﻿<%@ Page Title="Home Page" Language="VB" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="result.aspx.vb" Inherits="GoodDeals.result" %>

<%@ Register Src="~/Controls/ucGoomenaProducts.ascx" TagPrefix="uc1" TagName="ucGoomenaProducts" %>


<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="ProductsContainer">
        <div class="Transaction" data-status="<%:Me.status%>">
            <div class="Header">
                <asp:Literal ID="lblTrnsactionInfo" runat="server"></asp:Literal>
            </div>
            <div class="Secheader">
                <asp:Literal ID="lblSecHeader" runat="server"></asp:Literal>
            </div>

            <div class="btnContainer">
                <a href="http://www.goomena.com/Members/Default.aspx" class="linkToGoomena">
                    <asp:Literal ID="lblBacktoGoomena" runat="server"></asp:Literal></a>
            </div>

        </div>
        <div class="btnInvoiceContainer" runat="server" id="BtnContainer">
            <a href="javascript:void(0)" class="linkInvoice" onclick="showInvoiceForm()">
                <asp:Literal ID="lblInvoice" runat="server"></asp:Literal></a>
        </div>
        <asp:UpdatePanel ID="updBigPanel" runat="server" UpdateMode="Conditional" EnableViewState="false">
            <ContentTemplate>
                <asp:PlaceHolder ID="plForm" runat="server">
                    <div id="InvoiceForm" data-role="Form">
                        <div class="Header"><asp:Literal ID="lblFromHeader" runat="server"></asp:Literal></div>
                        <div class="row">
                            <asp:Label ID="lblLastName" runat="server" Text="Ονοματεπώνυμο:" AssociatedControlID="txtLastname"></asp:Label>
                            <asp:TextBox ID="txtLastname" runat="server"></asp:TextBox>
                        </div>
                        <div class="row">
                            <asp:Label ID="lblCompany" runat="server" Text="Επωνυμία:" AssociatedControlID="txtCompany"></asp:Label>
                            <asp:TextBox ID="txtCompany" runat="server"></asp:TextBox>
                        </div>
                        <div class="row">
                            <asp:Label ID="lblCountry" runat="server" Text="Χώρα:" AssociatedControlID="txtCountry"></asp:Label>
                            <asp:TextBox ID="txtCountry" runat="server"></asp:TextBox>
                        </div>
                        <div class="row">
                            <asp:Label ID="lblAddress" runat="server" Text="Διεύθυνση - Τ.Κ:" AssociatedControlID="txtAddress"></asp:Label>
                            <asp:TextBox ID="txtAddress" runat="server"></asp:TextBox>
                        </div>
                        <div class="row">
                            <asp:Label ID="lblVat" runat="server" Text="Α.Φ.Μ.:" AssociatedControlID="txtVat"></asp:Label>
                            <asp:TextBox ID="txtVat" runat="server"></asp:TextBox>
                        </div>
                        <div class="row">
                            <asp:Label ID="lblOccupation" runat="server" Text="Επάγγελμα:" AssociatedControlID="txtOccupation"></asp:Label>
                            <asp:TextBox ID="txtOccupation" runat="server"></asp:TextBox>
                        </div>
                        <div class="row">
                            <asp:Label ID="lblPhoneNumber" runat="server" Text="Τηλέφωνο:" AssociatedControlID="txtPhoneNumber"></asp:Label>
                            <asp:TextBox ID="txtPhoneNumber" runat="server"></asp:TextBox>
                        </div>
                        <div class="row">
                            <asp:Label ID="lblEmail" runat="server" Text="Email:" AssociatedControlID="txtEmail"></asp:Label>
                            <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                        </div>
                        <div id="SubmitContainer" class="form-group">
                                                        <asp:Button ID="submit" CssClass="btn btn-primary" OnClick="submit_Click" runat="server" Text="Ok" />
   

                        </div>

                    </div>
                </asp:PlaceHolder>
            </ContentTemplate>

        </asp:UpdatePanel>
    </div>
    <script type="text/javascript">
        var msgNotEmpty = '<%:Me.NotEmpty%>';
        var msgNotValid = '<%:Me.NotValid%>';;
        handlerPreventClickEvent = function PreventClickEvent(event) {
            event.preventDefault();
        };
        function AddFormValidation() {
            try {
                var validator = $('#InvoiceForm').data('formValidation');
                if (validator) {
                    validator.destroy();
                }
                $('#InvoiceForm').formValidation({
                    err: { container: 'tooltip' },
                    message: "This value is not valid",
                    icon: { valid: 'glyphicon glyphicon-ok', invalid: 'glyphicon glyphicon-remove', validating: 'glyphicon glyphicon-refresh' },
                    framework: 'bootstrap',
                    row: {
                        selector: '.row'
                    },
                    button: {
                        selector: '[type="submit"]:not([formnovalidate])',
                        disabled: 'disabled'
                    },
                    live:"enabled",
                    fields: {
                        Lastname: {
                            selector: $("#" + '<%:Me.txtLastname.ClientID%>'),
                            validators: {
                                notEmpty: { message: msgNotEmpty },
                                stringLength: {
                                    min: 6,
                                    message: msgNotValid
                                }

                            }
                        },
                        Company: { excluded: true },
                        Country: {
                            selector: $("#" + '<%:Me.txtCountry.ClientID%>'),
                            validators: {
                                notEmpty: { message: msgNotEmpty },
                                stringLength: {
                                    min: 2,
                                    message: msgNotValid
                                }
                            }
                        },
                        Address: {
                            selector: $("#" + '<%:Me.txtAddress.ClientID%>'),
                            validators: {
                                notEmpty: { message: msgNotEmpty },
                                stringLength: {
                                    min: 3,
                                    message: msgNotValid
                                }

                            }
                        },
                        Vat: {
                            selector: $("#" + '<%:Me.txtVat.ClientID%>'),
                            validators: {
                                notEmpty: { message: msgNotEmpty },
                                stringLength: {
                                    min: 6,
                                    message: msgNotValid
                                }

                            }
                        },
                        Occupation: {
                            selector: $("#" + '<%:Me.txtOccupation.ClientID%>'),
                            validators: {
                                notEmpty: { message: msgNotEmpty },
                                stringLength: {
                                    min: 3,
                                    message: msgNotValid
                                }

                            }
                        },
                        PhoneNumber: {
                            selector: $("#" + '<%:Me.txtPhoneNumber.ClientID%>'),
                            validators: {
                                notEmpty: { message: msgNotEmpty },
                                numeric: {
                                    message: msgNotValid,
                                    thousandsSeparator: '',
                                    decimalSeparator: ''
                                },
                                stringLength: {
                                    min: 10,
                                    message: msgNotValid
                                }
                            }
                        },
                        Email: {
                            selector: $("#" + '<%:Me.txtEmail.ClientID%>'),
                            validators: {
                                notEmpty: { message: msgNotEmpty },
                                emailAddress: {
                                    message: msgNotEmpty
                                },
                                stringLength: {
                                    min: 5,
                                    message: msgNotValid
                                }

                            }
                        }
                    }

                }).on('success.field.fv', function (e, data) {
                    var validator = $('#InvoiceForm').data('formValidation');
             
                    if (validator) {
                        if (validator.isValid()) {
                            $('.btn-primary').removeAttr("disabled")
                            $('.btn-primary').unbind("click", handlerPreventClickEvent);

                        } else {
                            $('.btn-primary').attr("disabled", "disabled")
                            $('.btn-primary').bind("click", handlerPreventClickEvent);
                        };
                    }
                }).on('err.field.fv', function (e, data) {
                    $('.btn-primary').attr("disabled", "disabled")
                    $('.btn-primary').bind("click", handlerPreventClickEvent);

                });
                $('#InvoiceForm').data('formValidation').validate();
                $('.btn-primary').attr("disabled", "disabled")
                $('.btn-primary').bind("click", handlerPreventClickEvent);
            } catch (e) { }

        }
    </script>
</asp:Content>

﻿Imports System.Web.Optimization
Imports System.Reflection
Imports Dating.Server.Core.DLL
Imports Dating.Server.Datasets.DLL

Public Class Global_asax
    Inherits HttpApplication

    Sub Application_Start(sender As Object, e As EventArgs)
        ' Fires when the application is started

        Dim getSyntax As MethodInfo = GetType(UriParser).GetMethod("GetSyntax", System.Reflection.BindingFlags.[Static] Or System.Reflection.BindingFlags.NonPublic)
        Dim flagsField As FieldInfo = GetType(UriParser).GetField("m_Flags", System.Reflection.BindingFlags.Instance Or System.Reflection.BindingFlags.NonPublic)
        If getSyntax IsNot Nothing AndAlso flagsField IsNot Nothing Then
            For Each scheme As String In New String() {"http", "https"}
                Dim parser As UriParser = DirectCast(getSyntax.Invoke(Nothing, New Object() {scheme}), UriParser)
                If parser IsNot Nothing Then
                    Dim flagsValue As Integer = CInt(flagsField.GetValue(parser))
                    ' Clear the CanonicalizeAsFilePath attribute
                    If (flagsValue And &H1000000) <> 0 Then
                        flagsField.SetValue(parser, flagsValue And Not &H1000000)
                    End If
                End If
            Next
        End If

        DataHelpers.SetConnectionString(System.Configuration.ConfigurationManager.ConnectionStrings("CMSDBconnectionString").ConnectionString)
        Dim css As System.Configuration.ConnectionStringSettings = System.Configuration.ConfigurationManager.ConnectionStrings("CMSDBconnectionString")
        If (css IsNot Nothing AndAlso Not String.IsNullOrEmpty(css.ConnectionString)) Then
            clsPageData.gLAG.Init(css.ConnectionString)
        Else
            clsPageData.gLAG.Init(ConfigurationManager.AppSettings("DB_ServerName").ToString(), 1433, ConfigurationManager.AppSettings("DB_Database").ToString(), False, ConfigurationManager.AppSettings("DB_Login").ToString(), ConfigurationManager.AppSettings("DB_Password").ToString())
        End If
        If clsMyMail.MySMTPSettings.gSMTPServerName Is Nothing OrElse Dating.Server.Core.DLL.clsMyMail.MySMTPSettings.gSMTPServerName.Length = 0 Then
            ReadSMTPSettings()
        End If
        System.Net.ServicePointManager.Expect100Continue = False
        System.Net.ServicePointManager.DefaultConnectionLimit = 10000
        RouteConfig.RegisterRoutes(RouteTable.Routes)
        BundleConfig.RegisterBundles(BundleTable.Bundles)
    End Sub

    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        '' Code that runs when an unhandled error occurs
        Dim retrying As Boolean = False
        Dim urlFixed As Boolean = False
        Dim siteUrl As String = ""
        Try
            Dim exSrv As Exception = Server.GetLastError()
            If (exSrv Is Nothing) Then Return

            Dim exSrv__ As String = exSrv.ToString()
            Dim exSrv__Message As String = exSrv.Message
            Dim isHttpExc As Boolean = (exSrv.GetType Is GetType(HttpException))

            'If (exSrv.GetType() Is GetType(System.Web.HttpException)) Then
            '    Server.ClearError()
            '    Return
            'End If

            If (exSrv.GetType() Is GetType(System.Threading.ThreadAbortException)) Then
                Server.ClearError()
                Return
            End If

            Try
                'use retry param to indicate that retry was done
                If (Context.Request.Url.Query = "?retry" OrElse Context.Request.Url.Query.Contains("&retry")) Then

                    siteUrl = Request.Url.AbsoluteUri
                    If (Request.Url.AbsolutePath.IndexOf(".aspx.aspx") > 0) Then
                        siteUrl = siteUrl.Replace(".aspx.aspx", ".aspx")
                        urlFixed = True

                    ElseIf (Request.Url.AbsolutePath.IndexOf("/gr/gr/") >= 0) Then
                        siteUrl = siteUrl.Replace("/gr/gr/", "/gr/")
                        urlFixed = True

                    ElseIf (Request.Url.AbsolutePath.IndexOf("/es/es/") >= 0) Then
                        siteUrl = siteUrl.Replace("/es/es/", "/es/")
                        urlFixed = True

                    ElseIf (Request.Url.AbsolutePath.IndexOf("/tr/tr/") >= 0) Then
                        siteUrl = siteUrl.Replace("/tr/tr/", "/tr/")
                        urlFixed = True

                    End If


                    If (urlFixed) Then
                        ' try to load again the same page one time
                        Server.ClearError()

                        Response.Redirect(siteUrl, False)
                        HttpContext.Current.ApplicationInstance.CompleteRequest()

                        retrying = True
                    End If
                Else
                    ' try to load again the same page one time
                    Server.ClearError()


                    siteUrl = Request.Url.AbsoluteUri
                    If (Request.Url.AbsolutePath.IndexOf(".aspx.aspx") > 0) Then
                        siteUrl = siteUrl.Replace(".aspx.aspx", ".aspx")
                        urlFixed = True

                    ElseIf (Request.Url.AbsolutePath.IndexOf("/gr/gr/") >= 0) Then
                        siteUrl = siteUrl.Replace("/gr/gr/", "/gr/")
                        urlFixed = True

                    ElseIf (Request.Url.AbsolutePath.IndexOf("/es/es/") >= 0) Then
                        siteUrl = siteUrl.Replace("/es/es/", "/es/")
                        urlFixed = True

                    ElseIf (Request.Url.AbsolutePath.IndexOf("/tr/tr/") >= 0) Then
                        siteUrl = siteUrl.Replace("/tr/tr/", "/tr/")
                        urlFixed = True

                    End If

                    If (Not urlFixed) Then
                        If (siteUrl.IndexOf("?") = -1 AndAlso siteUrl.IndexOf("&") > -1) OrElse
                           (siteUrl.IndexOf("?") > -1 AndAlso siteUrl.IndexOf("&") > -1 AndAlso siteUrl.IndexOf("?") > siteUrl.IndexOf("&")) Then

                            ' case-1: ? not found, but we have & ----> incorrect ulr
                            ' case-2: ? is after & ----> incorrect ulr
                            siteUrl = siteUrl.TrimEnd("&")
                            If (siteUrl.IndexOf("&") > -1) Then
                                Dim siteUrl1 As String = siteUrl.Remove(siteUrl.IndexOf("&"))
                                Dim siteUrl2 As String = siteUrl.Remove(0, siteUrl.IndexOf("&") + 1)
                                siteUrl = siteUrl1 & "?" & siteUrl2
                            End If
                            urlFixed = True

                        End If
                    End If


                    '         Dim redirected As Boolean = False
                    If (Not urlFixed) Then
                        Dim redirectPermanentUrl As String = ""
                        redirectPermanentUrl = UrlUtils.GetFixedURLForRedirection(siteUrl, isFromFileNotFoundPage:=False)
                        If (Not String.IsNullOrEmpty(redirectPermanentUrl)) Then
                            siteUrl = redirectPermanentUrl
                            urlFixed = True
                            'redirected = True
                        End If
                    End If


                    If (Not urlFixed) Then
                        If (isHttpExc AndAlso exSrv__.Contains("A potentially dangerous Request.Path value was detected from the client")) Then
                            siteUrl = HttpUtility.HtmlEncode(siteUrl)
                            'siteUrl = HttpUtility.UrlEncode(siteUrl)
                        End If
                        If siteUrl.Contains("http://www.goodeals.com/http:/cdn.") Then
                            siteUrl = siteUrl.Replace("http://www.goodeals.com/http:/cdn.", "http://cdn.")
                            urlFixed = True
                        Else
                            If (siteUrl.IndexOf("?") > -1) Then
                                siteUrl = siteUrl & "&retry"
                            Else
                                siteUrl = siteUrl & "?retry"
                            End If
                            urlFixed = True
                        End If


                    End If

                    If (urlFixed) Then
                        Response.Redirect(siteUrl, False)
                        HttpContext.Current.ApplicationInstance.CompleteRequest()
                    End If

                    retrying = True
                End If

            Catch
            End Try


            If (Not urlFixed) Then
                If (exSrv__Message = "The client disconnected.") Then
                    'Server.ClearError()

                ElseIf ((exSrv.GetType() Is GetType(System.ArgumentNullException) AndAlso exSrv__Message.Contains("Value cannot be null."))) Then
                    'Server.ClearError()

                ElseIf (isHttpExc AndAlso exSrv__Message = "Failed to load viewstate.") Then
                    'Server.ClearError()

                ElseIf (isHttpExc AndAlso exSrv__Message = "This is an invalid script resource request.") Then
                    'Server.ClearError()

                ElseIf (isHttpExc AndAlso exSrv__Message = "This is an invalid webresource request.") Then
                    'Server.ClearError()

                ElseIf (exSrv__Message = "The state information is invalid for this page and might be corrupted.") Then
                    'Server.ClearError()

                ElseIf (exSrv__Message.StartsWith("Validation of viewstate MAC failed.")) Then
                    'Server.ClearError()
                ElseIf (exSrv__Message.StartsWith("A potentially dangerous ")) Then
                    Try
                        If HttpContext.Current.Request.Url.ToString.Contains("http://www.goodeals.com/http:/cdn.goodeals.com/Images2/") Then
                            Debug.WriteLine("dsafsdad")
                        Else
                            Dating.Server.Core.DLL.clsSuspicious.AddSuspiciousIp(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), HttpContext.Current.Request.Url.ToString)
                        End If


                    Catch ex As Exception
                    Finally
                        WebErrorSendEmail(exSrv, "Application_Error (No reload)")
                    End Try
                Else

                    If (retrying) Then
                        WebErrorSendEmail(exSrv, "Application_Error (Retrying to reload the page...)")
                    Else
                        WebErrorSendEmail(exSrv, "Application_Error (No reload)")
                    End If
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when a new session is started
        'Session.Timeout = 960





        Session("StartDate") = DateTime.UtcNow
        Session("Agent") = Request.ServerVariables("HTTP_USER_AGENT") '-- the browser the user uses to visit your site
        Session("IP") = clsCurrentContext.GetCurrentIP()

        'Geo location IP Functions
        Try

            Session("GEO_COUNTRY_CODE") = "XX"
            '    Dim country As clsCountryByIP = clsCurrentContext.GetCountryByIP()

            'Dim country As clsCountryByIP = clsCountryByIP.GetCountryCodeFromIP(Session("IP"), ConfigurationManager.ConnectionStrings("GeoDBconnectionString").ConnectionString)
            'If (Len(country.CountryCode) = 2) Then
            '    Session("GEO_COUNTRY_CODE") = country.CountryCode
            '    Session("GEO_COUNTRY_CODE_BY_SITE") = "sql server"
            '    Session("GEO_COUNTRY_LATITUDE") = country.latitude
            '    Session("GEO_COUNTRY_LONGITUDE") = country.longitude
            '    Session("GEO_COUNTRY_POSTALCODE") = country.postalCode
            '    Session("GEO_COUNTRY_CITY") = country.city
            '    Session("GEO_COUNTRY_INFO") = country
            'End If

        Catch ex As Exception
            Try
                WebErrorSendEmail(ex, "GEO_COUNTRY_CODE")
            Catch : End Try
        End Try

        If Session("GEO_COUNTRY_CODE") = "XX" Then
            Session("GEO_COUNTRY_CODE") = "US"
        End If

        ' End GEO


        Try

            Dim lagCookie As HttpCookie = Request.Cookies("lagCookie")
            If lagCookie Is Nothing Then
                'Dim szGuid As String = DateTime.UtcNow.Ticks.ToString()                'System.Guid.NewGuid().ToString()
                Dim szGuid As String = System.Guid.NewGuid().ToString()
                lagCookie = New HttpCookie("lagCookie", szGuid)
                lagCookie.Expires = DateTime.Now.AddYears(2)
                lagCookie.Item("LagID") = Session("GEO_COUNTRY_CODE")
                Response.Cookies.Add(lagCookie)
                Session("lagCookie") = szGuid

                If (Session("LagID") Is Nothing) Then
                    Session("LagID") = lagCookie.Item("LagID")
                End If

                If Request("lag") IsNot Nothing AndAlso clsLanguageHelper.IsSupportedLAG(Request("lag"), Session("GEO_COUNTRY_CODE")) Then
                    'goomena.gr
                    Session("LagID") = gDomainGR_DefaultLAG
                Else

                    If Request.Url.Host.EndsWith(".com", StringComparison.OrdinalIgnoreCase) Then
                        'goodeals.com
                        If (Session("LagID") Is Nothing) Then Session("LagID") = "US"
                    End If
                    Session("LagID") = gDomainGR_DefaultLAG
                End If
            Else
                Session("lagCookie") = Server.HtmlEncode(lagCookie.Value)
                Session("LagID") = lagCookie.Item("LagID")
                If (lagCookie.Values.Count > 0) Then Session("lagCookie") = lagCookie.Values(0)
            End If

            If (Session("LagID") Is Nothing) Then

                ' ex	Request.Params("HTTP_ACCEPT_LANGUAGE") = "ru,en-US;q=0.8,en;q=0.6"	
                Dim browserLag As String = Request.Params("HTTP_ACCEPT_LANGUAGE")
                If (Not String.IsNullOrEmpty(browserLag)) Then
                    browserLag = browserLag.Split(","c, ";"c)(0)
                    Dim cultName As String() = browserLag.Split("-"c)
                    If (cultName.Length = 1) Then
                        ' ex "en"
                        If (clsLanguageHelper.IsSupportedLAG(cultName(0), Session("GEO_COUNTRY_CODE"))) Then Session("LagID") = cultName(0).ToUpper()
                    ElseIf (cultName.Length = 2) Then
                        ' ex "en-US"
                        If (clsLanguageHelper.IsSupportedLAG(cultName(1), Session("GEO_COUNTRY_CODE"))) Then Session("LagID") = cultName(1).ToUpper()
                    End If
                End If
            End If


            If (Session("LagID") Is Nothing) Then
                Session("LagID") = "US"
            End If

        Catch ex As Exception
            Session("LagID") = Session("GEO_COUNTRY_CODE")
        End Try




        If (Request.QueryString("persistlag") = "1") Then
            ' don't set SessionStarted_RedirectToCountryLAG flag
            ' here we stop redirection to country's default language
        Else
            Session("SessionStarted_RedirectToCountryLAG") = True
        End If
    End Sub


    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        Try

        Catch ex As Exception

        End Try
    End Sub
    Private Shared ClearCacheNextTime As DateTime
    Private Sub HandleNocache(Query As String)
        Dim ap As String = Context.Request.Url.AbsolutePath

        If (ap.StartsWith("/profile/", StringComparison.OrdinalIgnoreCase) OrElse
            ap.StartsWith("/cmspage/", StringComparison.OrdinalIgnoreCase) OrElse
            ap.StartsWith("/landing/", StringComparison.OrdinalIgnoreCase) OrElse
            ap.EndsWith(".aspx", StringComparison.OrdinalIgnoreCase)) Then
            If (Context.Request.Url.Query = "?nocache" OrElse Context.Request.Url.Query.Contains("&nocache")) Then

                ClearCache()
            Else
                ClearCacheWithCheck()
            End If
        End If
    End Sub
    Private Sub Application_PreRequestHandlerExecute(sender As Object, e As System.EventArgs) Handles Me.PreRequestHandlerExecute
        If (Not String.IsNullOrEmpty(Context.Request("country"))) Then
            Dim countrys As String() = New String() {"GR", "AL"}
            If (countrys.Contains(Context.Request("country").ToUpper())) Then
                Session("GEO_COUNTRY_CODE") = Context.Request("country").ToUpper()
            End If
        End If
        Dim re As Integer = 0
        Try
            If ConfigurationManager.AppSettings("CdnEnabled") <> "" AndAlso CInt(ConfigurationManager.AppSettings("CdnEnabled")) > 0 Then
                re = 1
            End If
            If re > 0 Then
                Dim application As HttpApplication = DirectCast(sender, HttpApplication)
                If (application.Context.Handler.GetType Is GetType(System.Web.UI.Page)) Then

                End If
            End If
        Catch ex As Exception
        End Try


    End Sub
    Private Shared Sub ClearCacheWithCheck()
        If (ClearCacheNextTime = Date.MinValue OrElse ClearCacheNextTime < Date.UtcNow) Then
            Dim IsNocacheOn As Boolean
            Try


                If (Not IsNocacheOn) Then
                    ''''''''''''''''''''
                    ' cms menu check scheduled menu items, 
                    ' if there was any update, reload cms menu
                    ''''''''''''''''''''


                    'If (Caching.Current.CheckSheduledItems()) Then
                    '    Caching.Current.Reload()
                    'End If
                End If

            Catch : End Try
            If (IsNocacheOn) Then
                ClearCache()
            End If
            ClearCacheNextTime = Date.UtcNow.AddMinutes(1)
        End If
    End Sub
    Public Shared Sub ReadSMTPSettings()
        Try
            Dim SYS_SMTPServer1 As SYS_SMTPServer = DataHelpers.SYS_SMTPServers_GetByNameLINQ("GOOMENA")

            If SYS_SMTPServer1 IsNot Nothing Then
                clsMyMail.MySMTPSettings.gSMTPOutPort = SYS_SMTPServer1.SMTPOutPort
                clsMyMail.MySMTPSettings.gSMTPServerName = SYS_SMTPServer1.SMTPServerName
                clsMyMail.MySMTPSettings.gSMTPLoginName = SYS_SMTPServer1.SMTPLoginName
                clsMyMail.MySMTPSettings.gSMTPPassword = SYS_SMTPServer1.SMTPLoginName
                clsMyMail.MySMTPSettings.gSMTPenableSsl = SYS_SMTPServer1.SmtpUseSsl
            End If
           

            Dim SYS_SMTPServer2 As SYS_SMTPServer = DataHelpers.SYS_SMTPServers_GetByNameLINQ("GOOSupport")

            If SYS_SMTPServer2 IsNot Nothing Then
                clsMyMail.MySMTPSettingsSupport.gSMTPOutPort = SYS_SMTPServer2.SMTPOutPort
                clsMyMail.MySMTPSettingsSupport.gSMTPServerName = SYS_SMTPServer2.SMTPServerName
                clsMyMail.MySMTPSettingsSupport.gSMTPLoginName = SYS_SMTPServer2.SMTPLoginName
                clsMyMail.MySMTPSettingsSupport.gSMTPPassword = SYS_SMTPServer2.SMTPLoginName
                clsMyMail.MySMTPSettingsSupport.gSMTPenableSsl = SYS_SMTPServer2.SmtpUseSsl
            End If
        Catch generatedExceptionName As Exception
        End Try
    End Sub
    Private Shared Sub ClearCache()
        clsPageData.ClearCache()
        Caching.Current.Reload()



    End Sub

End Class
﻿<%@ Page Title="Home Page" Language="VB" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.vb" Inherits="GoodDeals._Default" %>

<%@ Register Src="~/Controls/ucGoomenaProducts.ascx" TagPrefix="uc1" TagName="ucGoomenaProducts" %>


<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        var upd = '<%: Me.updBigPanel.UniqueID%>';
    </script>
    <div class="ProductsContainer">
         <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional" EnableViewState="false">
              <ContentTemplate>
                    <uc1:ucGoomenaProducts runat="server" ID="ucGoomenaProducts" />
                       </ContentTemplate>
         </asp:UpdatePanel>

      
    </div>
    <asp:UpdatePanel ID="updBigPanel" runat="server" UpdateMode="Conditional" EnableViewState="false">
        <ContentTemplate>
            <div class="UserNamePopUp">
                <div class="BackGround" >
                    <div class="Container">
                        <div class="Header">
                            <asp:Literal ID="lblHeader" runat="server"></asp:Literal>
                        </div>
                        
                        <div class="Content">
                            <div class="Error"> <asp:Literal ID="lblError" runat="server"></asp:Literal> </div>
                            <asp:TextBox ID="txtLoginname" runat="server"></asp:TextBox>
                        </div>
                        <div class="ButtonContainer">
                            <a href="javascript:void(0)" class="BuyNowbtn" onclick="BuyNow2('<%:Me.Package%>');">
                                <asp:Literal ID="lblContinue" runat="server"></asp:Literal>
                            </a>
                        </div>
                        <asp:HiddenField ID="txtPrid" runat="server" />
                          <asp:HiddenField ID="txtEmail" runat="server" />
                    </div>
                </div>
            </div>
        </ContentTemplate>

    </asp:UpdatePanel>

</asp:Content>

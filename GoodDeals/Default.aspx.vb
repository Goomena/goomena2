﻿Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Core.DLL
Imports Dating.Server
Imports GoodDeals.ucGoomenProducts

Public Class _Default
    Inherits BasePage
    Protected Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            ' If (_pageData Is Nothing) Then _pageData = New clsPageData("control.UCSelectProduct", Context)
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData("GoodelasDefault", Context, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property
    Private packages As String() = {"vv1000Credits", "vv3000Credits", "vv6000Credits", "vv10000Credits"}
    Public Property Package As String = ""
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load

    End Sub
    Dim isOldPackages As Boolean = False
    Private Sub SetOldPackages()
        isOldPackages = True
        ucGoomenaProducts.Products.Clear()
        ucGoomenaProducts.Products.Add(New GoomenaProduct With {.Amount = 15, .amountstr = "€15.00", .Credits = 1000, .ProductCode = "dd1000Credits"})
        ucGoomenaProducts.Products.Add(New GoomenaProduct With {.Amount = 36, .amountstr = "€36.00", .Credits = 3000, .ProductCode = "dd3000Credits"})
        ucGoomenaProducts.Products.Add(New GoomenaProduct With {.Amount = 66, .amountstr = "€66.00", .Credits = 6000, .ProductCode = "dd6000Credits"})
        ucGoomenaProducts.Products.Add(New GoomenaProduct With {.Amount = 99.9, .amountstr = "€99.90", .Credits = 10000, .ProductCode = "dd10000Credits"})
        ucGoomenaProducts.ReBind()
        UpdatePanel1.Update()
        packages = {"dd1000Credits", "dd3000Credits", "dd6000Credits", "dd10000Credits"}
    End Sub
    Private Sub Page_PreRender1(sender As Object, e As EventArgs) Handles Me.PreRender
        If Not Me.IsPostBack Then
            Dim LogName As String = Request("logname")
            Dim prid As String = Request("prid")
            Dim pc As String = Request("pck")
            Dim mail As String = Request("mail")

            If LogName IsNot Nothing AndAlso LogName.Length > 0 AndAlso prid IsNot Nothing AndAlso prid.Length > 0 AndAlso mail IsNot Nothing AndAlso mail.Length > 0 Then
                txtLoginname.Text = LogName
                Session("Loginname") = LogName
                txtPrid.Value = prid
                txtEmail.Value = mail
                Try
                    Dim pr As EUS_Profile = DataHelpers.GetEUS_ProfileByProfileID(prid)
                    If pr IsNot Nothing Then
                        Dim willseeNewPrices As Boolean = True
                        If pr.DateTimeToRegister < New Date(2016, 7, 6, 0, 0, 0) Then
                            If pc IsNot Nothing Then
                                pc = pc.Replace("vv", "dd")
                            End If

                            SetOldPackages()
                        End If
                    End If
                Catch ex As Exception
                End Try

            ElseIf Session("Loginname") IsNot Nothing AndAlso CStr(Session("Loginname")).Length > 0 Then
                txtLoginname.Text = CStr(Session("Loginname"))
            End If
            If pc IsNot Nothing AndAlso packages.Length > 0 AndAlso packages.Contains(pc) Then
                Package = pc
                ScriptManager.RegisterStartupScript(updBigPanel, updBigPanel.GetType(), "SelectPackage", "SelectProduct($('.GoomenaProduct[data-prid=""" & Package & """]'));", True)
            End If
        Else
            lblHeader.Text = CurrentPageData.GetCustomString("lblInserLoginname")
            lblContinue.Text = CurrentPageData.GetCustomString("lblContinue")
            txtLoginname.Attributes("Placeholder") = CurrentPageData.GetCustomString("PlLoginname")
            Dim prid As Integer = 0
            If Request.Params("__EVENTTARGET") = updBigPanel.UniqueID AndAlso Request.Params("__EVENTARGUMENT").Length > 0 Then
                Dim pc = Request.Params("__EVENTARGUMENT")
                If txtLoginname.Text.Length > 0 Then
                    Dim ds As DSMembers = DataHelpers.GetEUS_Profiles_ByLoginName(txtLoginname.Text)
                    Dim ok As Boolean = False
                    If ds IsNot Nothing AndAlso ds.EUS_Profiles.Rows.Count > 0 Then
                        For Each r As DSMembers.EUS_ProfilesRow In ds.EUS_Profiles.Rows
                            If r.IsMaster Then
                                txtPrid.Value = r.ProfileID
                                txtEmail.Value = r.eMail
                                If r.DateTimeToRegister < New Date(2016, 7, 5, 0, 0, 0) Then
                                    SetOldPackages()
                                    pc = pc.Replace("vv", "dd")
                                End If
                                Integer.TryParse(txtPrid.Value, prid)
                                ok = True
                                Exit For
                            End If
                        Next
                    End If
                    If Not ok Then
                        lblError.Text = CurrentPageData.GetCustomString("lblErrorProfileNotFound").Replace("###loginname###", txtLoginname.Text)
                        ScriptManager.RegisterStartupScript(updBigPanel, updBigPanel.GetType(), "SelectPackage", "GetUserName('.UserNamePopUp'); ", True)
                        Exit Sub
                    End If
                Else
                    ScriptManager.RegisterStartupScript(updBigPanel, updBigPanel.GetType(), "SelectPackage", "GetUserName('.UserNamePopUp');", True)
                    Exit Sub
                End If
                If pc IsNot Nothing AndAlso Me.packages.Contains(pc) Then
                    Package = pc
                Else
                    Exit Sub
                End If
                Dim pr As ucGoomenProducts.GoomenaProduct = (From itm In ucGoomenaProducts.Products
                                                            Where itm.ProductCode = Package
                                                            Select itm).FirstOrDefault
                clsCustomer.Viva_Payment(Math.Round(pr.Amount, 2).ToString, "Money", 1, txtLoginname.Text, prid, "http://www.goodeals.com", "", "viva", "", 5, "viva", Package)
            End If
        End If
        Try
            Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try
    End Sub



End Class
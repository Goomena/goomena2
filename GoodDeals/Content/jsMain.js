﻿SelectProduct = function (itm) {
    itm = $(itm).clone();
    var tmp = $('<div/>');
    $(tmp).addClass("PopUp");
    $(tmp).html('<div class="BackGround"  ><div class="Container"></div></div>');
    var imgContainer = $('<div/>');
    imgContainer.append($('.ImageContainer', $(itm)).html());
    imgContainer.addClass('ImageContainer');
    $('.ImageContainer', $(itm)).replaceWith(imgContainer);
    $('.Container', $(tmp)).html($(itm));
    $('.ButtonContainer', $(tmp)).appendTo($('.Container', $(tmp)));
    $('.BuyNowbtn', $(tmp)).click(function () {
        BuyNow(this);
        $('.BackGround', $(tmp)).click();
    });
  $('.BackGround', $(tmp)).click(function (e) { ClosePopup(e, this) });
    $('body').append($(tmp));

}
ClosePopup = function (e, itm) {
    var container = $(".Container",$(itm));
    if (!container.is(e.target) // if the target of the click isn't the container...
        && container.has(e.target).length === 0) // ... nor a descendant of the container
    {
        $(itm).hide(300).after(function () {
            $(itm).remove();
        })
    }
   
}
BuyNow = function (itm) {
    var tmp = $('.GoomenaProduct', $(itm).closest('.Container')).attr('data-prid');
    __doPostBack(upd, tmp);
}
BuyNow2 = function (itm) {
    __doPostBack(upd, itm);
}
GetUserName = function (itm) {
    $('body ' + itm).show(200);
    $('.BackGround', $('body ' + itm)).click(function (e) { ClosePopup(e, this) });
}
showInvoiceForm = function () {
    $('.ProductsContainer').addClass('Form');
    AddFormValidation();
}
RemoveForm = function (itm) {
    $('.ProductsContainer').removeClass('Form');
    $('#InvoiceForm').remove();
    $('.btnInvoiceContainer').remove();
    window.alert(itm);
}
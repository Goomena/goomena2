﻿Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Core.DLL
Imports System.Reflection

Public Class BaseMasterPagePublic
    Inherits BaseMasterPage


    Dim _RequestedLAGID As String
    Protected ReadOnly Property RequestedLAGID As String
        Get
            If (_RequestedLAGID Is Nothing) Then
                _RequestedLAGID = ""

                Dim currentUrl As String = Request.Url.ToString()
                Dim requestedLag As String = clsLanguageHelper.GetLAGFromURL(currentUrl)

                If (String.IsNullOrEmpty(requestedLag) AndAlso clsLanguageHelper.IsSupportedLAG(Request.QueryString("LAG"), Session("GEO_COUNTRY_CODE"))) Then
                    requestedLag = Request.QueryString("LAG")
                End If
                If (Not String.IsNullOrEmpty(requestedLag)) Then
                    _RequestedLAGID = requestedLag
                End If

                'If (Not String.IsNullOrEmpty(Request.QueryString("lag"))) Then
                '    _RequestedLAGID = Request.QueryString("lag")
                '    If (clsLanguage.IsSupportedLAG(_RequestedLAGID)) Then
                '        _RequestedLAGID = _RequestedLAGID.ToUpper()
                '    Else
                '        _RequestedLAGID = ""
                '    End If
                'End If
            End If
            Return _RequestedLAGID
        End Get
    End Property





    Private Sub controlCheck(ByVal ControlsList As UI.ControlCollection)
        For Each control As Control In ControlsList
            ' Now, using reflection, and get out all the properties.
            Dim properties As PropertyInfo() = control.[GetType]().GetProperties(BindingFlags.Instance Or BindingFlags.[Public])

            ' Loop through each one
            For i As Integer = 0 To properties.Length - 1
                ' Check if implements UrlPropertyAttribute, passing true to check 
                ' inheritance too. Also check we can read it.
                If properties(i).IsDefined(GetType(UrlPropertyAttribute), True) AndAlso properties(i).CanRead Then
                    ' It is implemented, so get the value out a as string
                    Dim url As String = TryCast(properties(i).GetValue(control, Nothing), String)

                    ' Double check we have a value. If we don't, we don't care
                    If Not [String].IsNullOrEmpty(url) Then
                        ' We do, so check it applies to a static resource on our site. 
                        ' For example, it points to another .aspx page, we don't 
                        ' want to rewrite it.
                        If Me.IsStaticResourceUrl(url) Then
                            ' It is, so rewrite it
                            Dim rewrittenUrl As String = Me.RewriteStaticResourceUrl(Page, url)

                            ' Set the value back
                            properties(i).SetValue(control, rewrittenUrl, Nothing)
                        End If
                    End If
                End If
            Next
            If control.Controls.Count > 0 Then
                controlCheck(control.Controls)
            End If
        Next

    End Sub
   

End Class

﻿Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Core.DLL

Public Class BaseUserControl
    Inherits System.Web.UI.UserControl
    Implements ILanguageDependableContent





    Public ReadOnly Property IsHTTPS As Boolean
        Get
            Return (Me.Request.Url.Scheme = "https")
        End Get
    End Property


    Protected ReadOnly Property LagID As String
        Get
            Return Me.Session("LagID")
        End Get
    End Property



    Private __Language As clsLanguageHelper
    Protected ReadOnly Property LanguageHelper As clsLanguageHelper
        Get
            If (__Language Is Nothing) Then __Language = New clsLanguageHelper(Context)
            Return __Language
        End Get
    End Property




    Private _globalStrings As clsPageData
    Public ReadOnly Property globalStrings As clsPageData
        Get
            If (_globalStrings Is Nothing) Then
                _globalStrings = New clsPageData("GoodealsGlobalStrings", HttpContext.Current)
                AddHandler _globalStrings.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _globalStrings
        End Get
    End Property

    Private Sub Page_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        Try
            If Me._pageData IsNot Nothing Then
                RemoveHandler Me._pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If

        Catch ex As Exception
        End Try
        Try
            If Me._globalStrings IsNot Nothing Then
                RemoveHandler Me._globalStrings.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If

        Catch ex As Exception
        End Try
        Try
            MyBase.Dispose()
        Catch ex As Exception

        End Try
    End Sub
    Protected _pageData As clsPageData
    Protected ReadOnly Property CurrentPageData As clsPageData
        Get
            ' let it read custom strings from container page
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData(Context, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property

    Public ReadOnly Property GetCMSDBDataContext As CMSDBDataContext
        Get
            Return New CMSDBDataContext(ModGlobals.ConnectionString)
        End Get
    End Property

    'Dim _CMSDBDataContext As CMSDBDataContext
    'Protected ReadOnly Property CMSDBDataContext As CMSDBDataContext
    '    Get
    '        Try
    '            If _CMSDBDataContext IsNot Nothing Then
    '                ' exception thrown when disposed
    '                Dim a As System.Data.Common.DbConnection = _CMSDBDataContext.Connection
    '                ' _CMSDBDataContext.

    '            End If
    '        Catch ex As Exception
    '            _CMSDBDataContext = Nothing
    '        End Try

    '        If (_CMSDBDataContext Is Nothing) Then 'OrElse _DataContext.Connection.State = ConnectionState.Broken OrElse _DataContext.Connection.State = ConnectionState.Closed) Then
    '            _CMSDBDataContext = New CMSDBDataContext(ModGlobals.ConnectionString)
    '        End If
    '        Return _CMSDBDataContext
    '    End Get
    'End Property








    Public Function GetLag() As String
        Dim str As String = Nothing
        Try
            If (clsLanguageHelper.IsSupportedLAG(HttpContext.Current.Session("LAGID"), Session("GEO_COUNTRY_CODE"))) Then
                str = HttpContext.Current.Session("LAGID")
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        If (str Is Nothing) Then
            If Request.Url.Host.EndsWith(".gr", StringComparison.OrdinalIgnoreCase) Then
                'goomena.gr
                str = gDomainGR_DefaultLAG
            Else

                If Request.Url.Host.EndsWith(".com", StringComparison.OrdinalIgnoreCase) Then
                    'goomena.com
                    If (str Is Nothing) Then str = gDomainCOM_DefaultLAG
                End If

            End If
        End If

        If (str Is Nothing) Then str = gDomainCOM_DefaultLAG

        Return str
    End Function




    Private _UnlimitedMsgID As Integer?
    Public ReadOnly Property UnlimitedMsgID As Integer
        Get
            If (Not _UnlimitedMsgID.HasValue) Then
                _UnlimitedMsgID = 0
                If (Not String.IsNullOrEmpty(Request.QueryString("unmsg"))) Then
                    Long.TryParse(Request.QueryString("unmsg"), _UnlimitedMsgID)
                End If
            End If
            Return _UnlimitedMsgID
        End Get
    End Property





    Public Sub Page_CustomStringRetrievalComplete(ByRef sender As clsPageData, ByRef Args As CustomStringRetrievalCompleteEventArgs)
        '    Dim __pageData As clsPageData = sender
        Dim result As String = Args.CustomString

        If (HttpContext.Current.Request.Url.Scheme.ToUpper() = "HTTPS") Then
            If (result.Contains("http://www.goodeals.")) Then
                result = result.Replace("http://www.goodeals.", "https://www.goodeals.")
                Args.CustomString = result
            End If
            If (result.Contains("http://cdn.goodeals.com")) Then
                result = result.Replace("http://cdn.goodeals.com", "https://cdn.goodeals.com")
                Args.CustomString = result
            End If
        End If

        If (Args.LagIDparam <> "US") Then
            Dim ancMatch As Match = clsWebFormHelper.AnchorRegex.Match(result, 0)
            If (ancMatch.Success) Then
                Args.CustomString = LanguageHelper.FixHTMLBodyAnchors(result, GetLag(), Page.RouteData)
            End If
        End If
    End Sub


    Protected Overrides Sub Finalize()
        '  If _CMSDBDataContext IsNot Nothing Then _CMSDBDataContext.Dispose()
        MyBase.Finalize()
    End Sub

    Public Sub Master_LanguageChanged() Implements ILanguageDependableContent.Master_LanguageChanged

    End Sub
End Class

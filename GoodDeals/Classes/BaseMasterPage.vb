﻿Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Core.DLL
Imports System.Reflection

Public Class BaseMasterPage
    Inherits System.Web.UI.MasterPage

    Protected _pageData As clsPageData
    Protected ReadOnly Property CurrentPageData As clsPageData
        Get
            'If (_pageData Is Nothing) Then _pageData = New clsPageData("master", Context)
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData("GoodDealsmaster", Context, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If

            Return _pageData
        End Get
    End Property

    Private Sub Page_Disposed1(sender As Object, e As EventArgs) Handles Me.Disposed
        Try
            If Me._pageData IsNot Nothing Then
                RemoveHandler Me._pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
        Catch ex As Exception
        End Try
        Try
            MyBase.Dispose()
        Catch ex As Exception
        End Try
    End Sub


    Private _globalStrings As clsPageData
    Public ReadOnly Property globalStrings As clsPageData
        Get
            If (_globalStrings Is Nothing) Then
                _globalStrings = New clsPageData("GoodDealsGlobalStrings", HttpContext.Current)
            End If
            Return _globalStrings
        End Get
    End Property

    Dim _sessVars As clsSessionVariables
    Protected ReadOnly Property SessionVariables As clsSessionVariables
        Get
            If (_sessVars Is Nothing) Then _sessVars = clsSessionVariables.GetCurrent()
            Return _sessVars
        End Get
    End Property


    Public ReadOnly Property IsHTTPS As Boolean
        Get
            Return (Me.Request.Url.Scheme = "https")
        End Get
    End Property



    Private __Language As clsLanguageHelper
    Protected ReadOnly Property LanguageHelper As clsLanguageHelper
        Get
            If (__Language Is Nothing) Then __Language = New clsLanguageHelper(Context)
            Return __Language
        End Get
    End Property


    Protected ReadOnly Property ProfileLoginName As String
        Get
            Return Me.SessionVariables.MemberData.LoginName
        End Get
    End Property

    Public ReadOnly Property GetCMSDBDataContext As CMSDBDataContext
        Get
            Return New CMSDBDataContext(ModGlobals.ConnectionString)
        End Get
    End Property
    'Dim _CMSDBDataContext As CMSDBDataContext
    'Protected ReadOnly Property CMSDBDataContext As CMSDBDataContext
    '    Get
    '        Try
    '            If _CMSDBDataContext IsNot Nothing Then
    '                ' exception thrown when disposed
    '                Dim a As System.Data.Common.DbConnection = _CMSDBDataContext.Connection


    '            End If
    '        Catch ex As Exception
    '            _CMSDBDataContext = Nothing
    '        End Try

    '        If (_CMSDBDataContext Is Nothing) Then 'OrElse _DataContext.Connection.State = ConnectionState.Broken OrElse _DataContext.Connection.State = ConnectionState.Closed) Then
    '            _CMSDBDataContext = New CMSDBDataContext(ModGlobals.ConnectionString)
    '        End If
    '        Return _CMSDBDataContext
    '    End Get
    'End Property


    Public Function GetLag() As String
        Dim str As String = Nothing
        Try
            If (clsLanguageHelper.IsSupportedLAG(HttpContext.Current.Session("LAGID"), Session("GEO_COUNTRY_CODE"))) Then
                str = HttpContext.Current.Session("LAGID")
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try


        If (str Is Nothing) Then str = gDomainCOM_DefaultLAG

        Return str
    End Function


    Public Shared Function GetGeoCountry() As String
        Return HttpContext.Current.Session("GEO_COUNTRY_CODE")
    End Function




    Private Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        Try
            If Not Me.IsPostBack Then
               
                Try
                    If Request("lag") IsNot Nothing AndAlso clsLanguageHelper.IsSupportedLAG(Request("lag").ToUpper, Session("GEO_COUNTRY_CODE")) Then
                        Session("LagID") = Request("lag").ToUpper
                        clsLanguageHelper.SetLagCookie(Session("LagID"))
                    End If
                Catch ex As Exception

                End Try


            End If
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub




    Public Sub Page_CustomStringRetrievalComplete(ByRef sender As clsPageData, ByRef Args As CustomStringRetrievalCompleteEventArgs)
        ' Dim __pageData As clsPageData = sender
        Dim result As String = Args.CustomString

        If (HttpContext.Current.Request.Url.Scheme.ToUpper() = "HTTPS") Then
            If (result.Contains("http://www.goodeals.")) Then
                result = result.Replace("http://www.goodeals.", "https://www.goodeals.")
                Args.CustomString = result
            End If
            If (result.Contains("http://cdn.goodeals.com")) Then
                result = result.Replace("http://cdn.goodeals.com", "https://cdn.goodeals.com")
                Args.CustomString = result
            End If
        End If

        If (Args.LagIDparam <> "US") Then
            Dim ancMatch As Match = clsWebFormHelper.AnchorRegex.Match(result, 0)
            If (ancMatch.Success) Then
                Args.CustomString = LanguageHelper.FixHTMLBodyAnchors(result, GetLag(), Page.RouteData)
            End If
        End If
    End Sub



    Protected Overrides Sub Finalize()
        '  If _CMSDBDataContext IsNot Nothing Then _CMSDBDataContext.Dispose()
        MyBase.Finalize()
    End Sub
    Private Sub controlCheck(ByVal ControlsList As UI.ControlCollection)
        For Each control As Control In ControlsList
            ' Now, using reflection, and get out all the properties.
            Dim properties As PropertyInfo() = control.[GetType]().GetProperties(BindingFlags.Instance Or BindingFlags.[Public])

            ' Loop through each one
            For i As Integer = 0 To properties.Length - 1
                ' Check if implements UrlPropertyAttribute, passing true to check 
                ' inheritance too. Also check we can read it.
                If properties(i).IsDefined(GetType(UrlPropertyAttribute), True) AndAlso properties(i).CanRead Then
                    ' It is implemented, so get the value out a as string
                    Dim url As String = TryCast(properties(i).GetValue(control, Nothing), String)

                    ' Double check we have a value. If we don't, we don't care
                    If Not [String].IsNullOrEmpty(url) Then
                        ' We do, so check it applies to a static resource on our site. 
                        ' For example, it points to another .aspx page, we don't 
                        ' want to rewrite it.
                        If Me.IsStaticResourceUrl(url) Then
                            ' It is, so rewrite it
                            Dim rewrittenUrl As String = Me.RewriteStaticResourceUrl(Page, url)

                            ' Set the value back
                            properties(i).SetValue(control, rewrittenUrl, Nothing)
                        End If
                    End If
                End If
            Next
            If control.Controls.Count > 0 Then
                controlCheck(control.Controls)
            End If
        Next

    End Sub

    Protected Function IsStaticResourceUrl(url As String) As Boolean
        Dim result As Boolean = False

        ' Clean up the URL
        Dim cleanUrl As String = url.ToLowerInvariant()

        ' Check it is relative to the site with a tilde ('~')
        If cleanUrl.StartsWith("~") Then
            ' Check points to a static URL
            If cleanUrl.EndsWith(".jpg") OrElse cleanUrl.EndsWith(".jpeg") OrElse cleanUrl.EndsWith(".gif") OrElse cleanUrl.EndsWith(".htm") OrElse cleanUrl.EndsWith(".html") OrElse cleanUrl.EndsWith(".css") OrElse cleanUrl.EndsWith(".js") Then
                ' TODO: Add more file types here
                ' It matches
                result = True
            End If

            Return result
        End If
    End Function
    Protected Function RewriteStaticResourceUrl(page As Page, url As String) As String
        ' Create it by combining the CDN URL with the resolved URL
        Dim result As String = "http://www.goodeals.com/" & page.ResolveUrl(url)

        Return result
    End Function
    Protected Function RewriteStaticResourceUrl(page As MasterPage, url As String) As String
        ' Create it by combining the CDN URL with the resolved URL
        Dim result As String = "http://www.goodeals.com/" & page.ResolveUrl(url)

        Return result
    End Function
End Class

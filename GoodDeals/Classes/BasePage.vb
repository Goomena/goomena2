Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Collections.Generic
Imports System.Configuration
Imports System.IO
Imports System.Xml
Imports System.Xml.Schema
Imports System.Globalization
Imports System.Text
Imports System.Text.RegularExpressions
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Reflection

Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Core.DLL
Imports Dating.Server


Partial Public Class BasePage
    Inherits System.Web.UI.Page
    Implements ILanguageDependableContent




    Protected ReadOnly Property LagID As String
        Get
            Return Me.Session("LagID")
        End Get
    End Property



    Public ReadOnly Property IsPublicRootMaster As Boolean
        Get
            Return (Me.Master IsNot Nothing AndAlso Me.Master.GetType().Name = "root_master")
        End Get
    End Property



    Public ReadOnly Property IsPublicRootMaster2 As Boolean
        Get
            Return (Me.Master IsNot Nothing AndAlso Me.Master.GetType().Name = "root2_master")
        End Get
    End Property




    Public ReadOnly Property IsLagChanged As Boolean
        Get


            Return False
        End Get
    End Property


    Private __Language As clsLanguageHelper
    Protected ReadOnly Property LanguageHelper As clsLanguageHelper
        Get
            If (__Language Is Nothing) Then __Language = New clsLanguageHelper(Context)
            Return __Language
        End Get
    End Property


    Public ReadOnly Property IsHTTPS As Boolean
        Get
            Return (Me.Request.Url.Scheme = "https")
        End Get
    End Property




    Private _globalStrings As clsPageData
    Public ReadOnly Property globalStrings As clsPageData
        Get
            If (_globalStrings Is Nothing) Then
                _globalStrings = New clsPageData("GlobalStrings", HttpContext.Current)
                AddHandler _globalStrings.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _globalStrings
        End Get
    End Property

    Private Sub Page_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        Try
            If Me._pageData IsNot Nothing Then
                RemoveHandler Me._pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If

        Catch ex As Exception
        End Try
        Try
            If Me._globalStrings IsNot Nothing Then
                RemoveHandler Me._globalStrings.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If

        Catch ex As Exception
        End Try
        Try
            MyBase.Dispose()
        Catch ex As Exception

        End Try
    End Sub
    Protected _pageData As clsPageData
    Protected ReadOnly Property CurrentPageData As clsPageData
        Get
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData(Context, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property

    ' Page PreInit 

    Public Overridable Sub Master_LanguageChanged() Implements ILanguageDependableContent.Master_LanguageChanged

    End Sub


    ' called from public default.aspx
    Protected Overridable Sub Page_PreInit(ByVal sender As Object, ByVal e As EventArgs)


    End Sub




    ' Page Load 
    Protected Overrides Sub OnLoad(ByVal e As EventArgs)
        Try


        Catch ex As System.Threading.ThreadAbortException
        End Try
    End Sub



    Public Sub Page_CustomStringRetrievalComplete(ByRef sender As clsPageData, ByRef Args As CustomStringRetrievalCompleteEventArgs)
        '   Dim __pageData As clsPageData = sender
        Dim result As String = Args.CustomString

        If (HttpContext.Current.Request.Url.Scheme.ToUpper() = "HTTPS") Then
            If (result.Contains("http://www.goodeals.")) Then
                result = result.Replace("http://www.goodeals.", "https://www.goodeals.")
                Args.CustomString = result
            End If
            If (result.Contains("http://cdn.goodeals.com")) Then
                result = result.Replace("http://cdn.goodeals.com", "https://cdn.goodeals.com")
                Args.CustomString = result
            End If
        End If

        If (Args.LagIDparam <> "US") Then
            Dim ancMatch As Match = clsWebFormHelper.AnchorRegex.Match(result, 0)
            If (ancMatch.Success) Then
                Args.CustomString = Me.FixHTMLBodyAnchors(result)
            End If
        End If

    End Sub

    Public Function FixHTMLBodyAnchors(html As String) As String
        html = LanguageHelper.FixHTMLBodyAnchors(html, GetLag(), Page.RouteData)
        Return html
    End Function


    ' Functions 
    Protected Overridable Function IsCurrentPage(ByVal oUrl As Object) As Boolean
        If oUrl Is Nothing Then
            Return False
        End If

        Dim result As Boolean = False
        Dim url As String = oUrl.ToString()
        If url.ToLower() = Page.Request.AppRelativeCurrentExecutionFilePath.ToLower() Then
            result = True
        End If
        Return result
    End Function

    Private Sub RegisterScript(ByVal key As String, ByVal url As String)
        Page.ClientScript.RegisterClientScriptInclude(key, Page.ResolveUrl(url))
    End Sub

    Private Sub RegisterCSSLink(ByVal url As String)
        Dim link As New HtmlLink()
        Page.Header.Controls.Add(link)
        link.EnableViewState = False
        link.Attributes.Add("type", "text/css")
        link.Attributes.Add("rel", "stylesheet")
        link.Href = url
    End Sub




    Private Sub Page_Error(sender As Object, e As System.EventArgs) Handles Me.Error
        '''''''''''''''''''''''''''''
        ' also check Application_Error
        ' in global.asax
        '''''''''''''''''''''''''''''
        Try
            Dim exSrv As Exception = Server.GetLastError()
            Dim exSrv__ As String = exSrv.ToString()
            Dim retrying As Boolean = False

            If (exSrv.GetType() Is GetType(System.Threading.ThreadAbortException)) Then
                Server.ClearError()
                Return
            End If

            Try
                'use retry param to indicate that retry was done
                If (Context.Request.Url.Query = "?retry" OrElse Context.Request.Url.Query.Contains("&retry")) Then

                Else
                    ' try to load again the same page one time
                    Server.ClearError()

                    If (Not Me.IsCallback) Then
                        Dim siteUrl As String = Request.Url.AbsoluteUri
                        If (siteUrl.IndexOf("?") > -1) Then
                            siteUrl = siteUrl & "&retry"
                        Else
                            siteUrl = siteUrl & "?retry"
                        End If
                        Response.Redirect(siteUrl, False)
                        HttpContext.Current.ApplicationInstance.CompleteRequest()
                    End If

                    retrying = True
                End If
            Catch
            End Try


            If (exSrv.Message = "The client disconnected.") Then
                'do nothing
            ElseIf (exSrv__.StartsWith("System.Web.HttpException (0x80004005): Failed to load viewstate.")) Then
                'do nothing
            ElseIf (exSrv__.StartsWith("System.Web.HttpException (0x80004005): The client disconnected.")) Then
                'do nothing
            ElseIf (exSrv.Message = "The state information is invalid for this page and might be corrupted.") Then
                'do nothing
            ElseIf (exSrv.Message.StartsWith("Validation of viewstate MAC failed.")) Then
                'do nothing
            Else
                If (retrying) Then
                    WebErrorMessageBox(Me, exSrv, "BasePage.Page_Error (Retrying to reload the page...)")
                Else
                    WebErrorMessageBox(Me, exSrv, "BasePage.Page_Error (No reload)")
                End If
            End If


        Catch ex As Exception
        End Try
    End Sub

    Private Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

    End Sub


    Protected Overridable Sub Page_PreLoad(sender As Object, e As System.EventArgs) Handles Me.PreLoad
        Try

            Session("CustomerAvailableCredits") = Nothing
            Session("GetMemberActionsCounters") = Nothing
            Session("EUS_Profile_IsProfileActive") = Nothing
            Session("IsAutorefresh") = Nothing

        Catch ex As Exception

        End Try
    End Sub
    Private Sub controlCheck(ByVal ControlsList As UI.ControlCollection)
        For Each control As Control In ControlsList
            ' Now, using reflection, and get out all the properties.
            Dim properties As PropertyInfo() = control.[GetType]().GetProperties(BindingFlags.Instance Or BindingFlags.[Public])

            ' Loop through each one
            For i As Integer = 0 To properties.Length - 1
                ' Check if implements UrlPropertyAttribute, passing true to check 
                ' inheritance too. Also check we can read it.
                If properties(i).IsDefined(GetType(UrlPropertyAttribute), True) AndAlso properties(i).CanRead Then
                    ' It is implemented, so get the value out a as string
                    Dim url As String = TryCast(properties(i).GetValue(control, Nothing), String)

                    ' Double check we have a value. If we don't, we don't care
                    If Not [String].IsNullOrEmpty(url) Then
                        ' We do, so check it applies to a static resource on our site. 
                        ' For example, it points to another .aspx page, we don't 
                        ' want to rewrite it.
                        If Me.IsStaticResourceUrl(url) Then
                            ' It is, so rewrite it
                            Dim rewrittenUrl As String = Me.RewriteStaticResourceUrl(Page, url)

                            ' Set the value back
                            properties(i).SetValue(control, rewrittenUrl, Nothing)
                        End If
                    End If
                End If
            Next
            If control.Controls.Count > 0 Then
                controlCheck(control.Controls)
            End If
        Next

    End Sub

    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        Try
            Dim re As Integer = 1
            Try
                If ConfigurationManager.AppSettings("CdnEnabled") <> "" AndAlso CInt(ConfigurationManager.AppSettings("CdnEnabled")) <> 1 Then
                    re = 0
                End If
                If re < 1 Then
                    Dim page As Page = DirectCast(sender, Page)
                    controlCheck(page.Controls)
                    For Each control As Control In page.Header.Controls
                        ' Check if a HTML link
                        If TypeOf control Is HtmlLink Then
                            ' It is, so get it
                            Dim link As HtmlLink = DirectCast(control, HtmlLink)

                            ' Check we can rewrite it
                            If Me.IsStaticResourceUrl(link.Href) Then
                                ' We can, so rewrite it
                                Dim rewrittenUrl As String = Me.RewriteStaticResourceUrl(page, link.Href)

                                ' Set it again
                                link.Href = rewrittenUrl
                            End If
                        End If
                    Next
                    Dim scriptManager__1 As ScriptManager = ScriptManager.GetCurrent(page)

                    ' Check we have one (otherwise do nothing)
                    If scriptManager__1 IsNot Nothing Then
                        ' Loop through each one
                        For Each script As ScriptReference In scriptManager__1.Scripts
                            ' Check we can rewrite it
                            If Me.IsStaticResourceUrl(script.Path) Then
                                ' We can, so rewrite it
                                Dim rewrittenUrl As String = Me.RewriteStaticResourceUrl(page, script.Path)

                                ' Set it again
                                script.Path = rewrittenUrl
                            End If
                        Next
                    End If
                End If
            Catch ex As Exception
            End Try
        Catch ex As Exception

        End Try
       

    End Sub
    Protected Function IsStaticResourceUrl(url As String) As Boolean
        Dim result As Boolean = False

        ' Clean up the URL
        Dim cleanUrl As String = url.ToLowerInvariant()

        ' Check it is relative to the site with a tilde ('~')
        If cleanUrl.StartsWith("~") Then
            ' Check points to a static URL
            If cleanUrl.EndsWith(".jpg") OrElse cleanUrl.EndsWith(".jpeg") OrElse cleanUrl.EndsWith(".gif") OrElse cleanUrl.EndsWith(".htm") OrElse cleanUrl.EndsWith(".html") OrElse cleanUrl.EndsWith(".css") OrElse cleanUrl.EndsWith(".js") Then
                ' TODO: Add more file types here
                ' It matches
                result = True
            End If

        End If

        Return result
    End Function
    Protected Function RewriteStaticResourceUrl(page As Page, url As String) As String
        ' Create it by combining the CDN URL with the resolved URL
        Dim result As String = "http://www.goodeals.com/" & page.ResolveUrl(url)

        Return result
    End Function
    Private Sub Page_PreRenderComplete(sender As Object, e As System.EventArgs) Handles Me.PreRenderComplete

        Try
            Dim X_UA_Compatible As New HtmlMeta()
            X_UA_Compatible.Attributes.Add("http-equiv", "X-UA-Compatible")
            X_UA_Compatible.Attributes.Add("content", "IE=edge,chrome=1")
            Me.Header.Controls.AddAt(0, X_UA_Compatible)
        Catch
        End Try

    End Sub



    Protected Overrides Sub OnUnload(e As System.EventArgs)
        Try
            MyBase.OnUnload(e)
        Catch ex As Exception
            WebErrorSendEmail(ex, "BasePage->Page_Unload")
        End Try



    End Sub




    Public Function GetLag() As String
        Dim str As String = Nothing
        Try
            If (clsLanguageHelper.IsSupportedLAG(HttpContext.Current.Session("LAGID"), Session("GEO_COUNTRY_CODE"))) Then
                str = HttpContext.Current.Session("LAGID")
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        If (str Is Nothing) Then
            If Request("lag") IsNot Nothing AndAlso clsLanguageHelper.IsSupportedLAG(Request("lag"), Session("GEO_COUNTRY_CODE")) Then
                'goodeals.gr
                str = Request("lag")
            Else
                str = gDomainGR_DefaultLAG


            End If
        End If

        If (str Is Nothing) Then str = gDomainGR_DefaultLAG

        Return str
    End Function


    Public Shared Function GetGeoCountry() As String
        Return HttpContext.Current.Session("GEO_COUNTRY_CODE")
    End Function



    Public Shared Function ReplaceCommonTookens(Input As String, Optional LoginName As String = "")
        If (Not String.IsNullOrEmpty(Input)) Then

            If (Input.IndexOf("###LOGINNAME###") > -1) Then Input = Input.Replace("###LOGINNAME###", LoginName)
            If (Input.IndexOf("###UNLOCK_MESSAGE_SEND_CRD###") > -1) Then Input = Input.Replace("###UNLOCK_MESSAGE_SEND_CRD###", ProfileHelper.Config_UNLOCK_MESSAGE_SEND_CREDITS)
            If (Input.IndexOf("###UNLOCK_MESSAGE_READ_CRD###") > -1) Then Input = Input.Replace("###UNLOCK_MESSAGE_READ_CRD###", ProfileHelper.Config_UNLOCK_MESSAGE_READ_CREDITS)
            If (Input.IndexOf("###UNLOCK_CONVERSATIONCRD###") > -1) Then Input = Input.Replace("###UNLOCK_CONVERSATIONCRD###", ProfileHelper.Config_UNLOCK_CONVERSATION_CREDITS)

        End If
        Return Input
    End Function




    Protected Sub Page_PreInit()



    End Sub



    Public Sub CreateCanonicalLink(canonicalHref As String)
        ' create canonical link
        'Render: <link rel="canonical" href="http://www.goodeals.com/es/cmspage/227" />
        ' Dim canonicalHref As String = "/landing/" & CurrentPageData.SitePageID

        If (Not String.IsNullOrEmpty(canonicalHref)) Then
            canonicalHref = LanguageHelper.GetPublicURL(canonicalHref, GetLag())
            canonicalHref = If(canonicalHref.IndexOf("?"c) > -1, canonicalHref.Remove(canonicalHref.IndexOf("?"c)), canonicalHref)
            If (canonicalHref.IndexOf("https://", StringComparison.OrdinalIgnoreCase) = 0) Then
                canonicalHref = canonicalHref.Remove(0, "https://".Length)
                canonicalHref = "http://" & canonicalHref
            End If

            ' Define an HtmlLink control.
            Dim lnkCanonical = New HtmlLink()
            lnkCanonical.Href = canonicalHref
            lnkCanonical.Attributes.Add("rel", "canonical")
            ' Add the HtmlLink to the Head section of the page.
            Page.Header.Controls.Add(lnkCanonical)
        End If
    End Sub



    Protected Overrides Sub Finalize()
        '  If _CMSDBDataContext IsNot Nothing Then _CMSDBDataContext.Dispose()

        MyBase.Finalize()
    End Sub
End Class





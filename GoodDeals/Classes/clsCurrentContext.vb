﻿Imports System.Web.SessionState
Imports Library.Public
Imports System.Reflection
Imports Dating.Server.Core.DLL

Public Class clsCurrentContext

    



    Public Shared Function GetCountryByIP() As clsCountryByIP
        Dim country As clsCountryByIP = Nothing
        Dim Session As HttpSessionState = HttpContext.Current.Session

        If (Session("GEO_COUNTRY_INFO") IsNot Nothing) Then country = Session("GEO_COUNTRY_INFO")

        If (country Is Nothing) Then
            If (Session("IP") Is Nothing) Then Session("IP") = clsCurrentContext.GetCurrentIP()

            country = clsCountryByIP.GetCountryCodeFromIP(Session("IP"), ConfigurationManager.ConnectionStrings("GeoDBconnectionString").ConnectionString)

            If (Len(country.CountryCode) = 2) Then
                Session("GEO_COUNTRY_CODE") = country.CountryCode
                Session("GEO_COUNTRY_CODE_BY_SITE") = "GeoDB"
                Session("GEO_COUNTRY_LATITUDE") = country.latitude
                Session("GEO_COUNTRY_LONGITUDE") = country.longitude
                Session("GEO_COUNTRY_POSTALCODE") = country.postalCode
                Session("GEO_COUNTRY_CITY") = country.city
                Session("GEO_COUNTRY_INFO") = country
            End If

        End If

        Return country
    End Function


    Public Shared Function IsLocalhost() As Boolean
        Try
            If (HttpContext.Current IsNot Nothing) Then
                Dim Request As HttpRequest = HttpContext.Current.Request
                Dim ip As String = Request.ServerVariables("REMOTE_ADDR") '-- the IP address of the visitor

                If (ip.StartsWith("127.") OrElse ip.Equals("::1")) Then
                    Return True
                End If
            End If
        Catch
        End Try

        Return False
    End Function










    Public Shared ReadOnly Property ExceptionsEmail As String
        Get
            Return ConfigurationManager.AppSettings("ExceptionsEmail")
        End Get
    End Property

    Public Shared ReadOnly Property gToEmail As String
        Get
            Return ConfigurationManager.AppSettings("gToEmail")
        End Get
    End Property


    Public Shared Function GetCurrentIP() As String
        Dim ip As String = ""

        Try
            Dim Request As HttpRequest = HttpContext.Current.Request
            ip = Request.ServerVariables("REMOTE_ADDR") '-- the IP address of the visitor

            If (clsCurrentContext.IsLocalhost()) Then
                ip = "77.49.94.48" 'GR
                'ip = "130.204.137.177" 'BG
                'ip = "66.220.152.114" ' US
                'ip = "81.61.207.139" 'es
                'ip = "151.250.122.99" ' TR
                'ip = "188.4.80.226" ' GR
                'ip = "5.135.134.124" ' FR
                'ip = "213.181.73.145" ' ES
                ' Session("IP") = "5.11.128.22" ' TR
                'Session("IP") = "188.165.13.64" ' FR
                'ip = "46.246.144.52" '"79.130.17.211"
            End If
        Catch
        End Try

        Return ip
    End Function



    Public Shared Function GetCookieID() As String
        Dim ip As String = ""

        Try
            Dim Session As HttpSessionState = HttpContext.Current.Session
            ip = Session("lagCookie") '-- lag cookie set by site
        Catch
        End Try

        Return ip
    End Function

    Private Shared _IsHTTPSAvailable As Boolean?
    Public Shared Function IsHTTPSAvailable() As Boolean
        If (_IsHTTPSAvailable Is Nothing) Then
            _IsHTTPSAvailable = False
            Try

                Dim url As String = "https://goodeals.com/index.html".ToLower()
                Try
                    Dim rp As String = clsWebPageProcessor.GetPageContent(url, 5000)
                    _IsHTTPSAvailable = True
                Catch
                End Try
            Catch
            End Try
        End If

        Return _IsHTTPSAvailable.Value
    End Function


    Public Shared Function FormatRedirectUrl(redirectUrl As String) As String
        Dim c As HttpContext = HttpContext.Current
        'Don’t append the forms auth ticket for unauthenticated users 
        '   or
        'for users authenticated with a different mechanism
        If Not c.User.Identity.IsAuthenticated OrElse Not (c.User.Identity.AuthenticationType = "Forms") Then
            Return redirectUrl
        End If

        'Determine if we need to append to an existing query-string or       not
        Dim qsSpacer As String
        qsSpacer = If(redirectUrl.IndexOf("?") > -1, "&", "?")

        'Build the new redirect URL. Assuming that currently using
        'forms authentication. Change the below FormsIdentity if required.

        Dim fi As FormsIdentity = DirectCast(c.User.Identity, FormsIdentity)
        Dim newRedirectUrl As String = (redirectUrl & qsSpacer) + FormsAuthentication.FormsCookieName & "=" & FormsAuthentication.Encrypt(fi.Ticket)
        Return newRedirectUrl
    End Function

    Public Shared Sub RedirectToMembersDefault(Optional useThreadAbort As Boolean = True)
        Dim Response As HttpResponse = HttpContext.Current.Response
        Dim url As String = "/Members/Default.aspx"
        url = clsCurrentContext.FormatRedirectUrl(url)

        If (useThreadAbort) Then
            Response.Redirect(url)
        Else
            Response.Redirect(url, False)
            HttpContext.Current.ApplicationInstance.CompleteRequest()
        End If
    End Sub



    Public Shared Function GetRefreshURL() As String
        Dim Request As HttpRequest = HttpContext.Current.Request
        ' Dim Response As HttpResponse = HttpContext.Current.Response

        Dim url As String = Request.Url.ToString()
        If (url.IndexOf("?") > -1) Then
            url = url.Remove(url.IndexOf("?") + 1)
            For Each itm As String In Request.QueryString.AllKeys
                If (Not String.IsNullOrEmpty(itm) AndAlso itm.ToUpper() <> "RFSH") Then
                    url = url & itm & "=" & HttpUtility.UrlEncode(Request.QueryString(itm)) & "&"
                End If
            Next
            url = url.TrimEnd({"&"c, "?"c})
        End If
        url = If(url IsNot Nothing, url, "") & If(url.IndexOf("?") > -1, "&", "?") & "rfsh=" & DateTime.UtcNow.Ticks
        Return url
    End Function


End Class

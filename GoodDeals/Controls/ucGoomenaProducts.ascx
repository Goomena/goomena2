﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucGoomenaProducts.ascx.vb" Inherits="GoodDeals.ucGoomenProducts" %>
<%@ Register Src="~/Controls/ucGoomenaProduct.ascx" TagPrefix="uc1" TagName="ucGoomenaProduct" %>

<div class="GoomenProductsContainer">
    <asp:Repeater ID="rptProductsContainer" runat="server">
        <ItemTemplate >
            <uc1:ucGoomenaProduct runat="server" id="ucGoomenaProduct1"
                 amount='<%# Eval("Amount")%>'
                AmountStr='<%# Eval("amountstr")%>'
                ProductCode='<%# Eval("ProductCode")%>'
                  Credits='<%# Eval("Credits")%>'
                 />
        </ItemTemplate>
    </asp:Repeater>
</div>
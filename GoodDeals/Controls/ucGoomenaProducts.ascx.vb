﻿Public Class ucGoomenProducts
    Inherits BaseUserControl
    Public ReadOnly Property Products As List(Of GoomenaProduct)
        Get
            If _list Is Nothing Then
                _list = New List(Of GoomenaProduct)
                _list.Add(New GoomenaProduct With {.Amount = 19, .amountstr = "€19.00", .Credits = 1000, .ProductCode = "vv1000Credits"})
                _list.Add(New GoomenaProduct With {.Amount = 45, .amountstr = "€45.00", .Credits = 3000, .ProductCode = "vv3000Credits"})
                _list.Add(New GoomenaProduct With {.Amount = 85, .amountstr = "€85.00", .Credits = 6000, .ProductCode = "vv6000Credits"})
                _list.Add(New GoomenaProduct With {.Amount = 125, .amountstr = "€125.00", .Credits = 10000, .ProductCode = "vv10000Credits"})
            End If
            Return _list
        End Get
       
    End Property
    Private _list As List(Of GoomenaProduct)
    Public Sub ReBind()
        rptProductsContainer.DataSource = Products
        rptProductsContainer.DataBind()
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


    End Sub
    Public Class GoomenaProduct
        Public Property Amount As Decimal
        Public Property amountstr As String
        Public Property ProductCode As String
        Public Property Credits As String
    End Class

    Private Sub Page_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        ReBind()
    End Sub
End Class
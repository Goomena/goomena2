﻿Public Class ucGoomenaProduct
    Inherits BaseUserControl
    Public Property amount As Decimal = 0
    Public Property AmountStr As String = ""
    Public Property Credits As Integer = 0
    Public Property ProductCode As String = ""
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub
    Protected Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            ' If (_pageData Is Nothing) Then _pageData = New clsPageData("control.UCSelectProduct", Context)
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData("goodealsGoomenaProduct", Context, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property

    Public Sub LoadLag()
        lblBuyNow.Text = CurrentPageData.GetCustomString("btnBuyNow")
        lblTitle.Text = CurrentPageData.GetCustomString("lblTitle").Replace("####Credits####", Credits.ToString)
        lblProductDescription.Text = CurrentPageData.GetCustomString("lblBody").Replace("####Credits####", Credits.ToString)
        lblMoney.Text = CurrentPageData.GetCustomString("lblPrice") & AmountStr
        img.ImageUrl = ResolveUrl("~/images/" & Credits.ToString & "-c.jpg")
    End Sub

    Private Sub Page_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        LoadLag()
    End Sub
End Class
﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucGoomenaProduct.ascx.vb" Inherits="GoodDeals.ucGoomenaProduct" %>
<div class="GoomenaProduct" data-prid="<%:Me.ProductCode%>" data-amount="<%:Me.amount%>" >
    <a class="ImageContainer" href="javascript:void(0);" onclick="SelectProduct($(this).parent());">
        <asp:Image ID="img" runat="server" />
    </a>

    <div class="RightPart">
            <div class="SmallDescription">
                  <asp:Literal ID="lblTitle" runat="server"></asp:Literal>
    </div>
        <div class="FromCompany">By:<a href="http://goomena.com/" class="CompanyLink">Goomena.com</a></div>
        <div class="MoneyContainer">
            <asp:Literal ID="lblMoney" runat="server"></asp:Literal>
        </div>
        <div class="ProductDescription">
            <asp:Literal ID="lblProductDescription" runat="server"></asp:Literal>
        </div>
    </div>
    <div class="clear"></div>
    <div class="ButtonContainer">
        <a href ="javascript:void(0)" class="BuyNowbtn" ><asp:Literal ID="lblBuyNow" runat="server"></asp:Literal></a> 
    </div>
</div>

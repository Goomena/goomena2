﻿Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Core.DLL
Imports Dating.Server

Public Class result
    Inherits BasePage
    Protected Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            ' If (_pageData Is Nothing) Then _pageData = New clsPageData("control.UCSelectProduct", Context)
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData("Goodealsresult", Context, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property
    Private packages As String() = {"dd1000Credits", "dd3000Credits", "dd6000Credits", "dd10000Credits"}
    Public Property Package As String = ""
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load

    End Sub
    Public Property Status As String = "success"
    Public Property NotEmpty As String = ""
    Public Property NotValid As String = ""
    Private Sub Page_PreRender1(sender As Object, e As EventArgs) Handles Me.PreRender
        If Not Me.IsPostBack Then
            If Request("failed") IsNot Nothing Then
                Status = "failed"
                lblTrnsactionInfo.Text = CurrentPageData.GetCustomString("lblFail")
                lblSecHeader.Text = CurrentPageData.GetCustomString("lblFailSecHeader")
                BtnContainer.Visible = False
            Else
                Status = "success"
                lblTrnsactionInfo.Text = CurrentPageData.GetCustomString("lblSuccessHeader")
                lblSecHeader.Text = CurrentPageData.GetCustomString("lblSecheader")
                If Request("s") IsNot Nothing AndAlso CStr(Request("s")).Length > 0 AndAlso Not IsSentedEmail Then
                    lblInvoice.Text = CurrentPageData.GetCustomString("btnInvoice")
                    lblLastName.Text = CurrentPageData.GetCustomString("lblLastName")
                    lblCompany.Text = CurrentPageData.GetCustomString("lblCompany")
                    lblCountry.Text = CurrentPageData.GetCustomString("lblCountry")
                    lblAddress.Text = CurrentPageData.GetCustomString("lblAddress")
                    lblVat.Text = CurrentPageData.GetCustomString("lblVat")
                    lblOccupation.Text = CurrentPageData.GetCustomString("lblOccupation")
                    lblPhoneNumber.Text = CurrentPageData.GetCustomString("lblPhoneNumber")
                    lblFromHeader.Text = CurrentPageData.GetCustomString("lblInvoice")
                    lblEmail.Text = CurrentPageData.GetCustomString("lblEmail")
                    Me.NotEmpty = CurrentPageData.GetCustomStringJS("lblNotEmpty")
                    Me.NotValid = CurrentPageData.GetCustomStringJS("NotValid")
                    BtnContainer.Visible = True
                    plForm.Visible = True
                    '    ScriptManager.RegisterStartupScript(updBigPanel, updBigPanel.GetType(), "Form", "AddFormValidation();", True)
                Else
                    BtnContainer.Visible = False
                    plForm.Visible = False
                End If
              
            End If
            lblBacktoGoomena.Text = CurrentPageData.GetCustomString("lblBackTOGoomena")
        End If
        Try
            Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try
    End Sub


    Public Property IsSentedEmail As Boolean = False
    Protected Sub submit_Click(sender As Object, e As EventArgs) Handles submit.Click
        Dim sb As New StringBuilder
        Dim i As String = ""
        If Request("s") IsNot Nothing AndAlso CStr(Request("s")).Length > 0 Then
            i = Request("s")
        End If
        sb.AppendLine("New application for invoice for transaction order:" & i)
        sb.AppendLine("Application Date(utc-dd/MM/yyyy HH:mm):" & DateTime.UtcNow.ToString("dd/MM/yyyy HH:mm"))
        If Session("Loginname") IsNot Nothing AndAlso CStr(Session("Loginname")).Length > 0 Then
            sb.AppendLine("LoginName:" & CStr(Session("Loginname")))
        End If
        sb.AppendLine("Full Name:" & txtLastname.Text)
        sb.AppendLine("Company Details:" & txtCompany.Text)
        sb.AppendLine("Country:" & txtCountry.Text)
        sb.AppendLine("Address - P.C.:" & txtAddress.Text)
        sb.AppendLine("VAT. Num. or Reg. Num.:" & txtVat.Text)
        sb.AppendLine("Occupation:" & txtOccupation.Text)
        sb.AppendLine("Phone Num.:" & txtPhoneNumber.Text)
        sb.AppendLine("Email:" & txtEmail.Text)
        Try
            Dim t As New Threading.Tasks.Task(Sub()
                                                  Dating.Server.Core.DLL.clsMyMail.TrySendMailSupport(ConfigurationManager.AppSettings("InvoiceApplicationEmail"), "New InvoiceApplication", sb.ToString)
                                              End Sub)
            t.Start()
        Catch ex As Exception

        End Try
        IsSentedEmail = True
        ScriptManager.RegisterStartupScript(updBigPanel, updBigPanel.GetType(), "RemoveForm", "RemoveForm('" & CurrentPageData.GetCustomStringJS("lblOkReceived") & "');", True)
    End Sub
End Class
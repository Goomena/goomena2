﻿Imports Dating.Server.Core.DLL

Public Class DatingDealsPoints


    Private Shared ReadOnly Property ConnectionString As String
        Get
            Return System.Configuration.ConfigurationManager.ConnectionStrings("DatingDealsPoints.My.MySettings.AppDBconnectionString").ConnectionString
        End Get
    End Property

    Private Sub DatingDealsPoints_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

    End Sub


    Private Sub WriteStatus(msg As String)
        memoLog.Text = DateTime.Now.ToString("hh:mm:ss.fff") & " - " & msg & vbCrLf & memoLog.Text
    End Sub


    Public Sub SetCustomerPoints()
        Dim dc As New CMSDBDataContext(ConnectionString)
        Dim conn As New SqlClient.SqlConnection(ConnectionString)

        Dim cmd As New SqlClient.SqlCommand()
        cmd.Parameters.Add(New SqlClient.SqlParameter("customerid", 0))


        Try
            conn.Open()

            ' generous account type id
            Dim generousAccId As Integer = (From itm In dc.EUS_LISTS_AccountTypes
                                           Where itm.ConstantName = "GENEROUS"
                                           Select itm.AccountTypeId).First()

            ' all profiles that should be affected
            Dim profileIds = (From itm In dc.EUS_Profiles
                            Where itm.Status = ProfileStatusEnum.Approved AndAlso itm.AccountTypeId = generousAccId
                            Select itm.ProfileID).GetEnumerator()


            While (profileIds.MoveNext())


                Dim sql As String = <sql><![CDATA[

declare @creditsofyear int, @unlocksofyear int;
  
select @creditsofyear=SUM(CREDITS) 
from [EUS_CustomerCredits]
where customerid= @customerid
and DateTimeCreated  between dateadd(year, -1, getutcdate()) and  getutcdate()
and isnull(validfordays ,0)<>0

select @unlocksofyear=COUNT(*)
from EUS_UnlockedConversations
where FromProfileId = @customerid
and DateTimeCreated  between dateadd(year, -1, getutcdate()) and  getutcdate()

if(@creditsofyear > 0 and @unlocksofyear > 0)
begin
    update EUS_Profiles 
    set 
	    PointsCredits = @creditsofyear,
	    PointsUnlocks = @unlocksofyear
    where profileid= @customerid or MirrorProfileID=@customerid
end
]]></sql>.Value

                cmd.CommandText = sql
                cmd.Connection = conn
                cmd.Parameters("customerid").Value = profileIds.Current
                Dim rowsAffected = cmd.ExecuteNonQuery()

                Application.DoEvents()

            End While


        Catch ex As Exception
            WriteStatus(ex.ToString())
        Finally
            dc.Dispose()
            conn.Close()
        End Try

    End Sub

    Private Sub DatingDealsPoints_Shown(sender As Object, e As System.EventArgs) Handles Me.Shown

        Try

            WriteStatus("Starting credit and unlock points update...")
            SetCustomerPoints()
            WriteStatus("Points update complete")
        Catch ex As Exception
            WriteStatus(ex.ToString())
        End Try

    End Sub
End Class
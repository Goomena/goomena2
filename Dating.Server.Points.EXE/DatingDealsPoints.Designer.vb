﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class DatingDealsPoints
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblStatus = New DevExpress.XtraEditors.LabelControl()
        Me.memoLog = New DevExpress.XtraEditors.MemoEdit()
        CType(Me.memoLog.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblStatus
        '
        Me.lblStatus.Location = New System.Drawing.Point(14, 15)
        Me.lblStatus.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(21, 13)
        Me.lblStatus.TabIndex = 1
        Me.lblStatus.Text = "Log:"
        '
        'memoLog
        '
        Me.memoLog.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.memoLog.Location = New System.Drawing.Point(14, 39)
        Me.memoLog.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.memoLog.Name = "memoLog"
        Me.memoLog.Properties.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.memoLog.Properties.WordWrap = False
        Me.memoLog.Size = New System.Drawing.Size(313, 282)
        Me.memoLog.TabIndex = 2
        '
        'DatingDealsPoints
        '
        Me.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(341, 336)
        Me.Controls.Add(Me.memoLog)
        Me.Controls.Add(Me.lblStatus)
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Name = "DatingDealsPoints"
        Me.Text = "DatingDealsPoints"
        CType(Me.memoLog.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblStatus As DevExpress.XtraEditors.LabelControl
    Friend WithEvents memoLog As DevExpress.XtraEditors.MemoEdit
End Class

﻿Imports Library.Public
Imports DevExpress.Web.ASPxEditors
Imports Dating.Server.Core.DLL

Public Class Login2
    Inherits BasePage


    Protected Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            'If (_pageData Is Nothing) Then _pageData = New clsPageData("Login.aspx", Context)
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData("Login.aspx", Context, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property

    Private Sub Page_Disposed1(sender As Object, e As EventArgs) Handles Me.Disposed
        Try
            If Me._pageData IsNot Nothing Then
                RemoveHandler Me._pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete

            End If
       
        Catch ex As Exception
        End Try
        Try
            If Me.Password IsNot Nothing Then
                RemoveHandler Me.Password.PreRender, AddressOf Password_PreRender
            End If
        Catch ex As Exception

        End Try
        Try
            MyBase.Dispose()
        Catch ex As Exception
        End Try
    End Sub

    Protected Overrides Sub OnPreInit(ByVal e As EventArgs)
        Try
            Dim check As Boolean = (Request.QueryString("check") = "1")
            If (check) Then
                Return
            End If

            If (Not Page.IsPostBack) Then

                '' redirect user to HTTPS url
                'If (Not Me.IsHTTPS AndAlso
                '    clsCurrentContext.UseHTTPSLogin()) Then
                '    Dim newUrl As String = UrlUtils.GetHTTPSUrl(Request.Url)
                '    Response.Redirect(newUrl)
                'End If

                If (clsCurrentContext.VerifyLogin() = True) Then
                    clsCurrentContext.RedirectToMembersDefault()
                Else
                    If (HttpContext.Current.User.Identity.IsAuthenticated) Then
                        'clear session
                        FormsAuthentication.SignOut()
                        Session.Abandon()

                    End If
                End If

                MyBase.OnPreInit(e)
            End If

        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "OnPreInit")
        End Try
    End Sub

    Private Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        Try
            If (Session("LoginRedirectToDefault") = 1) Then
                Session("LoginRedirectToDefault") = Nothing

                Dim url As String = ResolveUrl("~/Default.aspx")
                url = LanguageHelper.GetNewLAGUrl(Request.Url, GetLag(), url)
                Response.Redirect(url)
            End If

            If (Session("MAX_LOGIN_RETRIES") = 1 OrElse Session("MAX_LOGIN_RETRIES") = 2) Then

                Dim recaptcha As New Recaptcha.RecaptchaControl()
                recaptcha.ID = "recaptcha"
                recaptcha.PublicKey = "6LeqLOsSAAAAACbrctSmamfQ20Jc8BJrBynH-jKt"
                recaptcha.PrivateKey = "6LeqLOsSAAAAAM4HuHzL6W4zmNtKYCOn1iS0AwK7"

                Dim MainLogin As System.Web.UI.WebControls.Login = ucLogin1.LoginControl
                Dim phRecaptcha As PlaceHolder = MainLogin.FindControl("phRecaptcha")
                If (phRecaptcha IsNot Nothing) Then
                    phRecaptcha.Controls.Add(recaptcha)
                End If

                Dim lblFillRecaptcha As Label = MainLogin.FindControl("lblFillRecaptcha")
                If (lblFillRecaptcha IsNot Nothing) Then
                    lblFillRecaptcha.Text = CurrentPageData.GetCustomString("lblFillRecaptcha")
                End If

            End If
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "OnPreInit")
        End Try
    End Sub

    Dim Password As ASPxTextBox = Nothing
    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If (CurrentPageData Is Nothing) Then Throw New PageDataException()

        Try


            If clsCurrentContext.VerifyLogin() = True Then
                clsCurrentContext.RedirectToMembersDefault(True)

                'Dim url As String = Page.ResolveUrl("~/Members/default.aspx")
                'url = clsCurrentContext.FormatRedirectUrl(url)
                'Response.Redirect(url)

            End If


            Try
                Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
                AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "Page_Load")
            End Try


            If (Not Me.IsPostBack) Then
                LoadLAG()
            End If

            Dim MainLogin As System.Web.UI.WebControls.Login = ucLogin1.LoginControl
            Password = MainLogin.FindControl("Password")
            If (Password IsNot Nothing) Then
                AddHandler Password.PreRender, AddressOf Password_PreRender
            End If

        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

        Try
            If (Me.IsPostBack) Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "HideLoading", "HideLoading();", True)
            End If

            Dim lblErrorUC1 As Control = ucLogin1.FindControl("lblError")
            If (lblErrorUC1.GetType() Is GetType(ASPxLabel)) Then

                Dim lblErrorUC As ASPxLabel = lblErrorUC1
                If (lblErrorUC IsNot Nothing AndAlso lblErrorUC.Text.Length > 0) Then
                    Dim MainLogin As System.Web.UI.WebControls.Login = ucLogin1.LoginControl
                    Dim lblError As ASPxLabel = MainLogin.FindControl("lblError")
                    If (lblError IsNot Nothing) Then
                        lblError.Text = lblErrorUC.Text
                        lblError.Visible = True
                        lblErrorUC.Text = ""
                        lblErrorUC.Visible = False
                    End If
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "validateFields", "validateFieldsReload();", True)
                End If

            ElseIf (lblErrorUC1.GetType() Is GetType(Label)) Then

                Dim lblErrorUC As Label = lblErrorUC1
                If (lblErrorUC IsNot Nothing AndAlso lblErrorUC.Text.Length > 0) Then
                    Dim MainLogin As System.Web.UI.WebControls.Login = ucLogin1.LoginControl
                    Dim lblError As ASPxLabel = MainLogin.FindControl("lblError")
                    If (lblError IsNot Nothing) Then
                        lblError.Text = lblErrorUC.Text
                        lblError.Visible = True
                        lblErrorUC.Text = ""
                        lblErrorUC.Visible = False
                    End If
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "validateFields", "validateFieldsReload();", True)
                End If

            End If


        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

        Try
            LoadPhotos()
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "PreRender->LoadPhotos")
        End Try

    End Sub



    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
    End Sub


    Protected Sub LoadLAG()
        Try
            Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic

            lblHeader.Text = CurrentPageData.GetCustomString("lblHeader")
            lnkRestore.Text = CurrentPageData.GetCustomString("lbForgotPassword")
            lnkCreateAccount.Text = CurrentPageData.GetCustomString("lnkCreateAccount2")

            lblWellcomeMsg.Text = CurrentPageData.GetCustomString("lblWellcomeMsg")
            lblMembersCount.Text = CurrentPageData.GetCustomString("lblMembersCount")
            lblRegFooter.Text = CurrentPageData.GetCustomString("lblRegFooter")
            lblHeaderText.Text = CurrentPageData.GetCustomString("lblHeaderText")
            lblLoginWith.Text = CurrentPageData.GetCustomString("lblLoginWith")

            lnkCreateAccount.NavigateUrl = "~/Register2.aspx"
            If (Not String.IsNullOrEmpty(Request.QueryString("ReturnUrl"))) Then
                lnkCreateAccount.NavigateUrl = lnkCreateAccount.NavigateUrl & "?ReturnUrl=" & HttpUtility.UrlEncode(Request.QueryString("ReturnUrl"))
            End If


            LoadLAG_LoginControl()

            AppUtils.setNaviLabel(Me, "lbPublicNavi2Page", cPageBasic.PageTitle)

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Protected Sub LoadLAG_LoginControl()
        Try
            '    Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            Dim MainLogin As System.Web.UI.WebControls.Login = ucLogin1.LoginControl
            'MainLogin.UserNameLabelText = CurrentPageData.GetCustomString("txtUserNameLabelText")
            MainLogin.PasswordLabelText = CurrentPageData.GetCustomString("txtPasswordLabelText")
            MainLogin.RememberMeText = CurrentPageData.GetCustomString("txtRememberMeText")
            MainLogin.LoginButtonText = CurrentPageData.GetCustomString("txtLoginButtonText")
            MainLogin.UserNameRequiredErrorMessage = CurrentPageData.GetCustomString("txtUserNameRequiredErrorMessage")
            MainLogin.TitleText = CurrentPageData.GetCustomString("txtLoginTitleText")

            Dim lbForgotPassword As ASPxHyperLink = MainLogin.FindControl("lbForgotPassword")
            If (lbForgotPassword IsNot Nothing) Then
                lbForgotPassword.Text = CurrentPageData.GetCustomString("lbForgotPassword")
            End If

            Dim lbLoginFB As ASPxHyperLink = MainLogin.FindControl("lbLoginFB")
            If (lbLoginFB IsNot Nothing) Then
                lbLoginFB.Text = CurrentPageData.GetCustomString("lbLoginFB")
            End If

            Dim btnJoinToday As ASPxButton = MainLogin.FindControl("btnJoinToday")
            If (btnJoinToday IsNot Nothing) Then
                btnJoinToday.Text = CurrentPageData.GetCustomString("btnJoinToday")
            End If

            Dim UserName As ASPxTextBox = MainLogin.FindControl("UserName")
            If (UserName IsNot Nothing) Then
                'UserName.NullText = CurrentPageData.GetCustomString("UserName.NullText")
                UserName.ValidationSettings.RequiredField.ErrorText = CurrentPageData.GetCustomString("UserName.ValidationSettings.RequiredField.ErrorText")
                UserName.NullText = CurrentPageData.GetCustomString("lblUserName2")
                UserName.NullText = AppUtils.StripHTML(UserName.NullText)
            End If

            Dim Password As ASPxTextBox = MainLogin.FindControl("Password")
            If (Password IsNot Nothing) Then
                'Password.NullText = CurrentPageData.GetCustomString("Password.NullText")
                Password.ValidationSettings.RequiredField.ErrorText = CurrentPageData.GetCustomString("Password.ValidationSettings.RequiredField.ErrorText")
                'Password.NullText = CurrentPageData.GetCustomString("lblPassword")
            End If


            Dim btnLogin As ASPxButton = MainLogin.FindControl("btnLogin")
            If (btnLogin IsNot Nothing) Then
                btnLogin.Text = CurrentPageData.GetCustomString("btnLogin")
            End If

            Dim RememberMe As CheckBox = MainLogin.FindControl("RememberMe")
            If (RememberMe IsNot Nothing) Then
                RememberMe.Text = CurrentPageData.GetCustomString("RememberMe")
            End If

            Dim chkRememberMe As ASPxCheckBox = MainLogin.FindControl("chkRememberMe")
            If (chkRememberMe IsNot Nothing) Then
                chkRememberMe.Text = CurrentPageData.GetCustomString("RememberMe")
            End If

            '' controls found on login.aspx page
            Dim lblUserName As Label = MainLogin.FindControl("lblUserName")
            If (lblUserName IsNot Nothing) Then
                lblUserName.Text = CurrentPageData.GetCustomString("lblUserName")
            End If


            Dim lblPassword As Label = MainLogin.FindControl("lblPassword")
            If (lblPassword IsNot Nothing) Then
                lblPassword.Text = CurrentPageData.GetCustomString("lblPassword")
            End If

            Dim btnLoginNow As Button = MainLogin.FindControl("btnLoginNow")
            'Dim btnLoginNow As ASPxButton = MainLogin.FindControl("btnLoginNow")
            If (btnLoginNow IsNot Nothing) Then
                btnLoginNow.Text = CurrentPageData.GetCustomString("btnLoginNow")
                btnLoginNow.Text = btnLoginNow.Text.Replace("&nbsp;", " ")
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Protected Sub Password_PreRender(sender As Object, e As EventArgs)
        Dim Password As ASPxTextBox = sender
        Password.ClientSideEvents.Init = "function(s, e) {s.SetText('" + Password.Text + "');}"
    End Sub



    Private Sub LoadPhotos()

        Dim photoid As String
        Dim photoname As String
        Dim address As String = Me.Request.Url.Scheme & "://photos.goomena.com/"
        Dim address2 As String
        Dim address3 As String
        Dim i As Integer
        Dim picture As New HtmlGenericControl
        '  Dim imageforwomen As New HtmlImage
        Dim profilename As String
        Dim checkList As New List(Of Integer)
        '  Dim topControlsCount As Integer = pnlMosaic.Controls.Count

        If (clsCurrentContext.VerifyLogin()) Then
            registerform.Visible = False
        End If


        Dim latitudeIn As Double? = Nothing
        Dim longitudeIn As Double? = Nothing

        Try

            'If (Session("GEO_COUNTRY_INFO") IsNot Nothing) Then
            '    Dim country As clsCountryByIP = Session("GEO_COUNTRY_INFO")
            Dim country As clsCountryByIP = clsCurrentContext.GetCountryByIP()
            If (country IsNot Nothing) Then

                Try
                    Dim pos As New clsGlobalPositionWCF(country.latitude, country.longitude)
                    latitudeIn = pos.latitude
                    longitudeIn = pos.longitude

                    'If (Not String.IsNullOrEmpty(country.latitude)) Then
                    '    Dim __latitudeIn As Double = 0
                    '    'country.latitude = country.latitude.Replace(".", ",")
                    '    If (Double.TryParse(country.latitude, __latitudeIn)) Then latitudeIn = __latitudeIn
                    '    If (__latitudeIn > 180 OrElse __latitudeIn < -180) Then
                    '        country.latitude = country.latitude.Replace(".", ",")
                    '        If (Double.TryParse(country.latitude, __latitudeIn)) Then latitudeIn = __latitudeIn
                    '    End If
                    'End If
                    'If (Not String.IsNullOrEmpty(country.longitude)) Then
                    '    Dim __longitudeIn As Double = 0
                    '    'country.longitude = country.longitude.Replace(".", ",")
                    '    If (Double.TryParse(country.longitude, __longitudeIn)) Then longitudeIn = __longitudeIn
                    '    If (__longitudeIn > 180 OrElse __longitudeIn < -180) Then
                    '        country.longitude = country.longitude.Replace(".", ",")
                    '        If (Double.TryParse(country.longitude, __longitudeIn)) Then longitudeIn = __longitudeIn
                    '    End If
                    'End If
                Catch ex As Exception
                    WebErrorSendEmail(ex, "Get lat-lon from ip.")
                End Try

            End If
            ' End If


            If (Not latitudeIn.HasValue AndAlso Not longitudeIn.HasValue) Then
                Try

                    If (MasterProfileId > 0 AndAlso clsCurrentContext.VerifyLogin()) Then
                        If (SessionVariables.MemberData.latitude.HasValue) Then latitudeIn = SessionVariables.MemberData.latitude
                        If (SessionVariables.MemberData.longitude.HasValue) Then longitudeIn = SessionVariables.MemberData.longitude
                    End If
                Catch ex As Exception
                    WebErrorSendEmail(ex, "Get lat-lon from profile.")
                End Try
            End If

            If (Not latitudeIn.HasValue AndAlso Not longitudeIn.HasValue) Then
                Try

                    Dim pos As clsGlobalPositionWCF = clsGeoHelper.GetCountryMinPostcodeDataTable_Position(BasePage.GetGeoCountry(), Nothing, Nothing)
                    latitudeIn = pos.latitude
                    longitudeIn = pos.longitude

                    'Dim dtGEO As DataTable = clsGeoHelper.GetCountryMinPostcodeDataTable(MyBase.GetGeoCountry(), Nothing, Nothing)

                    'If (dtGEO.Rows.Count > 0 AndAlso Not IsDBNull(dtGEO.Rows(0)("latitude")) AndAlso Not IsDBNull(dtGEO.Rows(0)("longitude"))) Then
                    '    latitudeIn = clsNullable.DBNullToDecimal(dtGEO.Rows(0)("latitude"))
                    '    longitudeIn = clsNullable.DBNullToDecimal(dtGEO.Rows(0)("longitude"))
                    'End If

                Catch ex As Exception
                    WebErrorSendEmail(ex, "Get lat-lon from geo data.")
                End Try
            End If

        Catch ex As Exception
            WebErrorSendEmail(ex, "Get lat-lon info.")
        End Try
        Dim dt As DataTable
        Dim sql As String = "EXEC [CustomerPhotos_Grid2]  @latitudeIn=@latitudeIn, @longitudeIn=@longitudeIn,@Country=@Country"
        Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


            Using cmd As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(con, sql)


                cmd.Parameters.AddWithValue("@Country", BasePage.GetGeoCountry())
                Dim prm1 As SqlClient.SqlParameter = DataHelpers.GetSqlParam_LatitubeOrLongitude("@latitudeIn", latitudeIn)
                cmd.Parameters.Add(prm1)

                Dim prm2 As SqlClient.SqlParameter = DataHelpers.GetSqlParam_LatitubeOrLongitude("@longitudeIn", longitudeIn)
                cmd.Parameters.Add(prm2)

                'If (Not latitudeIn.HasValue AndAlso Not longitudeIn.HasValue) Then
                '    cmd.Parameters.AddWithValue("@latitudeIn", DBNull.Value)
                '    cmd.Parameters.AddWithValue("@longitudeIn", DBNull.Value)
                'Else
                '    cmd.Parameters.AddWithValue("@latitudeIn", latitudeIn.Value)
                '    cmd.Parameters.AddWithValue("@longitudeIn", longitudeIn.Value)
                'End If

                dt = DataHelpers.GetDataTable(cmd)
            End Using
        End Using
        dt.Columns.Add("InUse", GetType(Boolean))

        Try

            Dim women As New List(Of Integer)
            Dim men As New List(Of Integer)

            For cnt = 0 To dt.Rows.Count - 1
                Dim dr As DataRow = dt.Rows(cnt)

                Dim CustomerID As Integer = dr("CustomerID")
                If (checkList.Contains(CustomerID)) Then
                    Continue For
                End If
                Dim gender As Integer = dr("GenderId")
                If (gender = 2) Then
                    If (women.Count > 59) Then
                        Continue For
                    Else
                        women.Add(cnt)
                    End If
                ElseIf (gender = 1) Then
                    If (men.Count > 27) Then
                        Continue For
                    Else
                        men.Add(cnt)
                    End If
                End If

                checkList.Add(CustomerID)


                'stis 88 photos exit
                If (checkList.Count > 87) Then Exit For

            Next


            Dim photos_panel As Control = pnlMosaic
            Dim picSuffix As String = ""
            Dim currentPhotosCount As Integer = 0
            i = 0
            For cnt = 0 To checkList.Count - 1
                If (currentPhotosCount > 87) Then Exit For
                Dim drs As DataRow() = dt.Select("CustomerId=" & checkList(cnt) & "and (InUse is null)")
                If (drs.Length = 0) Then Continue For

                i += 1
                picture = photos_panel.FindControl("pic" & i & picSuffix)
                While (i < 87 AndAlso (picture Is Nothing OrElse Not picture.Visible) AndAlso (currentPhotosCount < 38))
                    i += 1
                    picture = photos_panel.FindControl("pic" & i & picSuffix)
                End While
                If (picture Is Nothing) Then Exit For


                Dim dr As DataRow = drs(0)
                dr("InUse") = True
                currentPhotosCount = currentPhotosCount + 1

                photoid = dr("CustomerID").ToString
                photoname = dr("FileName")
                profilename = dr("LoginName")
                '     Dim country As String = dr("Country")
                Dim region As String = dr("Region")
                Dim birthday As Date = dr("Birthday")
                Dim city As String = dr("City")
                Dim age As Integer = ProfileHelper.GetCurrentAge(birthday)
                Dim gender As Integer = dr("GenderId")
                address2 = address + photoid
                address3 = address2 + "/thumbs/" + photoname
                If Me.IsHTTPS AndAlso address3.StartsWith("http:") Then address3 = "https" & address3.Remove(0, "http".Length)

                picture.Attributes.Add("style", "background-image:url(" + address3 + ")")
                picture.Attributes.Add("data-URL", address3)
                picture.Attributes.Add("data-ID", photoid)
                picture.Attributes.Add("data-LoginName", profilename)
                picture.Attributes.Add("data-Country", dr("CountryName")) 'ProfileHelper.GetCountryName(country))
                picture.Attributes.Add("data-Region", region)
                picture.Attributes.Add("data-Age", age)
                picture.Attributes.Add("data-City", city)
                picture.Attributes.Add("data-Gender", gender)

                If (cnt = checkList.Count - 1 AndAlso currentPhotosCount < 87) Then
                    cnt = 0
                End If
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        End Try
        checkList.Clear()



    End Sub


End Class
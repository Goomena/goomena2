﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Root.Master" CodeBehind="RestorePassword.aspx.vb" Inherits="Dating.Server.Site.Web.RestorePassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/v1/css/main3.css?v=7" rel="stylesheet" type="text/css" />
    <style type="text/css">
    .recoverpassnewcontainer
    {
        background: url("//cdn.goomena.com/goomena-kostas/first-general/bg-reapet.jpg") ;
        box-shadow: 0px 5px 5px 1px #c7c5c8 ;
        padding-bottom:100px; 
        border-top:6px solid #be202f;
        margin-top:16px;
    }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
<div class="recoverpassnewcontainer">
    <div class="mid_bg pageContainer">
<div class="hp_cr" id="hp_top"></div>
<div id="hp_content">

	<div class="container_12">

		<div class="ab_content">
			<div class="ab_left">
				<div class="ab_block">
					<%--<h2><asp:Label ID="lblWelcome" runat="server" Text=""/></h2>--%>
 					<div class="pe_wrapper">
						<div class="pe_box">
							<h2 style="text-shadow:none; border:none; font-family:Lucida Grande,Lucida Sans Unicode,Verdana,Arial,sans-serif; ">
                            <asp:Label ID="lblHeader" runat="server" CssClass="loginHereTitle"/><span class="loginHereLink"><asp:HyperLink ID="lnkCreateAccount" runat="server" NavigateUrl="~/Register.aspx">Create an account</asp:HyperLink></span></h2>

                            <asp:Panel ID="pnlSubmitResult" class="pe_form pe_login" runat="server">
                                <asp:Label ID="lblSubmitResult" runat="server" Text=""/>
                            </asp:Panel>

                            <asp:Panel ID="pnlSubmitFormContainer" class="pe_form pe_login" runat="server" DefaultButton="btnLogin">
                                <asp:Label ID="lblRestoreInstructions" runat="server" Text="" Font-Italic="true" Font-Size="14px" />
                                <dx:ASPxValidationSummary ID="valSumm" runat="server" 
                                    ValidationGroup="RestorePasswordGroup" 
                                    ShowErrorsInEditors="true" RenderMode="BulletedList">
                                    <Paddings PaddingBottom="10px" />
                                    <LinkStyle>
                                        <Font Size="14px"></Font>
                                    </LinkStyle>
                                </dx:ASPxValidationSummary>
							    <ul class="defaultStyle">		
								    <li>
									    <div class="alert alert-danger hidden" id="id-error"></div>
                                        <div class="lfloat">
									        <asp:Label ID="lblUsernameText" runat="server" AssociatedControlID="txtEmail" CssClass="registerTableLabel" ></asp:Label>
                                        </div>
									    <div class="lfloat" style="margin-left:20px;">
                                            <dx:ASPxTextBox ID="txtEmail" runat="server" ClientInstanceName="RegisterEmailTextBox">
                                            <ValidationSettings ValidationGroup="RestorePasswordGroup" 
                                                    ErrorDisplayMode="ImageWithTooltip" ErrorTextPosition="Left" Display="Dynamic">
                                                <RequiredField IsRequired="True" ErrorText="" />
                                                <RegularExpression ValidationExpression="" 
                                                    ErrorText="" />
                                            </ValidationSettings>
                                            </dx:ASPxTextBox>
                                         </div>
								    </li>
								    <li class="clear"></li>
							    </ul>
							    <div class="clear"></div>
							    <div class="form-actions">
                                   <%-- <asp:Button ID="btnLogin" runat="server" Text="" CssClass="btn btn-large btn-primary" style="width:150px;" />--%>
                                    <dx:ASPxButton ID="btnLogin" runat="server" CausesValidation="True" CommandName="Insert"
                                        CssClass="btn btn-large btn-primary" Text="" 
                                        ValidationGroup="RestorePasswordGroup" Native="True"  EncodeHtml="False" />
                                </div>
                            </asp:Panel>
						</div>
					</div>
				</div>
						
			</div>
            <div class="ab_right">
                <div class="ab_block test_ab">
                    <div class="lists">
                        <h2><asp:Label ID="lblHeaderRight" runat="server" CssClass="rightColumnTitle" Text="" /></h2>
                        <asp:Label ID="lblTextRight" runat="server" CssClass="rightColumnContent" Text="" />
                    </div>
                </div>
            </div>
			<%--<div class="ab_right">
				<div class="ab_block test_ab">
                    <asp:Literal ID="lblHtmlBodyRight" runat="server" Text=""></asp:Literal>
				</div>		
			</div>	--%>
			<div class="clear"></div>
		</div>
	</div>
</div>
</div>
</div>
</asp:Content>

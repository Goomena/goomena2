﻿Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Core.DLL

Public Class Caching

    Private Shared _Current As Caching
    Public Shared ReadOnly Property Current As Caching
        Get
            If _Current Is Nothing Then _Current = New Caching()
            Return _Current
        End Get
    End Property

    Private Shared ReadOnly Property ConnectionString As String
        Get
            Return System.Configuration.ConfigurationManager.ConnectionStrings("CMSDBconnectionString").ConnectionString
        End Get
    End Property

#Region "Read-only properties to return data in application"
    Private _siteMenuItems As List(Of dsMenu.SiteMenuItemsRow)
    Public ReadOnly Property SiteMenuItems As List(Of dsMenu.SiteMenuItemsRow)
        Get
            If _siteMenuItems Is Nothing Then GetSiteMenuItems()
            Return _siteMenuItems
        End Get
    End Property
#End Region

#Region "Public methods to manipulate cache"
    Public Sub Reload()
        CheckSheduledItems()
        GetSiteMenuItems()
    End Sub


    Public Shared Function CheckSheduledItems() As Boolean
        Dim isAny As Boolean = False
        Dim sql As String = <sql><![CDATA[
declare @rows int=0

update SYS_SiteMenuItems set 
	IsActive=1
	,SceduleEnabled=0
	,DateTimeUploaded=getutcdate()
where SiteMenuItemID in (
	select SiteMenuItemID
	from SYS_SiteMenuItems
	where SceduleEnabled=1
	and DateTimeToUpload<=getutcdate()
)

set @rows=@@ROWCOUNT
select RowsUpdated=@rows
]]></sql>.Value
        Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


            Using cmd As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                cmd.Connection.ConnectionString = Caching.ConnectionString

                Using dt As DataTable = DataHelpers.GetDataTable(cmd)
                    If (dt.Rows.Count > 0) Then
                        isAny = (dt.Rows(0)(0) > 0)
                    End If
                End Using
            End Using
        End Using


        Return isAny
    End Function

#End Region

#Region "Private methods to fill cache"
    Private Sub GetSiteMenuItems()
        _siteMenuItems = New List(Of dsMenu.SiteMenuItemsRow)

        Using da As New dsMenuTableAdapters.SiteMenuItemsTableAdapter
            Using con As New SqlClient.SqlConnection(Caching.ConnectionString)
                da.Connection = con
                Using dt As dsMenu.SiteMenuItemsDataTable = da.GetAllActive()
                    For Each row As dsMenu.SiteMenuItemsRow In dt.Rows
                        _siteMenuItems.Add(row)
                    Next
                End Using
            End Using
        End Using

    End Sub
#End Region

End Class




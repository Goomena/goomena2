﻿Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Core.DLL
Imports Dating.Server.Datasets.DLL.DSMembers

Public Class clsProfileRows

    Public Property MasterProfileID As Integer
    Public Property MirrorProfileID As Integer
    Public Property MasterRowIndex As Integer
    Public Property MirrorRowIndex As Integer

    Private _profileId As Integer
    Private _ds As DSMembers
    Private _masterRow As EUS_ProfilesRow
    Private _mirrorRow As EUS_ProfilesRow

    Private _isDataLoaded As Boolean

    Public ReadOnly Property DSMembers As DSMembers
        Get
            Return _ds
        End Get
    End Property


    Public Sub New(profileId As Integer)
        _profileId = profileId
    End Sub


    Public Function GetMirrorRow() As DSMembers.EUS_ProfilesRow
        _LoadData()
        Return _mirrorRow
    End Function


    Public Function GetMasterRow() As DSMembers.EUS_ProfilesRow
        _LoadData()
        Return _masterRow
    End Function


    Private Sub _LoadData()
        If (Not _isDataLoaded) Then

            _ds = DataHelpers.GetEUS_Profiles_ByProfileOrMirrorID(_profileId)

            If (_ds.EUS_Profiles.Count > 0) Then

                Dim cnt As Integer = 0
                For cnt = 0 To _ds.EUS_Profiles.Rows.Count - 1
                    Dim dr As DataRow = _ds.EUS_Profiles.Rows(cnt)
                    If (dr("Status") = ProfileStatusEnum.NewProfile) Then
                        MasterProfileID = dr("ProfileID")
                        MasterRowIndex = cnt
                        MirrorProfileID = dr("ProfileID")
                        MirrorRowIndex = cnt
                    Else

                        If (dr("IsMaster") = True) Then
                            MasterProfileID = dr("ProfileID")
                            MasterRowIndex = cnt
                        ElseIf (dr("IsMaster") = False) Then
                            MirrorProfileID = dr("ProfileID")
                            MirrorRowIndex = cnt
                        End If

                    End If
                Next

                ' search for changed fields and show related indication buttons
                _masterRow = _ds.EUS_Profiles(MasterRowIndex)
                _mirrorRow = _ds.EUS_Profiles(MirrorRowIndex)
            End If

            _isDataLoaded = True
        End If
    End Sub


    Public Sub Update()
        If (_ds IsNot Nothing) Then DataHelpers.UpdateEUS_Profiles(_ds)
    End Sub

End Class




'Public Class clsProfileRowsForUser

'    Public Property MasterProfileID As Integer
'    Public Property MirrorProfileID As Integer
'    Public Property MasterRowIndex As Integer
'    Public Property MirrorRowIndex As Integer

'    Private _profileId As Integer
'    Private _ds As DSMembers
'    Private _masterRow As EUS_ProfilesForUserRow
'    Private _mirrorRow As EUS_ProfilesForUserRow

'    Private _isDataLoaded As Boolean

'    Public ReadOnly Property DSMembers As DSMembers
'        Get
'            Return _ds
'        End Get
'    End Property


'    Public Sub New(profileId As Integer)
'        _profileId = profileId
'    End Sub


'    Public Function GetMirrorRow() As DSMembers.EUS_ProfilesForUserRow
'        _LoadData()
'        Return _mirrorRow
'    End Function


'    Public Function GetMasterRow() As DSMembers.EUS_ProfilesForUserRow
'        _LoadData()
'        Return _masterRow
'    End Function


'    Private Sub _LoadData()
'        If (Not _isDataLoaded) Then

'            _ds = DataHelpers.GetEUS_ProfilesForUser_ByProfileOrMirrorID(_profileId)

'            If (_ds.EUS_Profiles.Count > 0) Then

'                Dim cnt As Integer = 0
'                For cnt = 0 To _ds.EUS_Profiles.Rows.Count - 1
'                    Dim dr As DataRow = _ds.EUS_Profiles.Rows(cnt)
'                    If (dr("Status") = ProfileStatusEnum.NewProfile) Then
'                        MasterProfileID = dr("ProfileID")
'                        MasterRowIndex = cnt
'                        MirrorProfileID = dr("ProfileID")
'                        MirrorRowIndex = cnt
'                    Else

'                        If (dr("IsMaster") = True) Then
'                            MasterProfileID = dr("ProfileID")
'                            MasterRowIndex = cnt
'                        ElseIf (dr("IsMaster") = False) Then
'                            MirrorProfileID = dr("ProfileID")
'                            MirrorRowIndex = cnt
'                        End If

'                    End If
'                Next

'                ' search for changed fields and show related indication buttons
'                _masterRow = _ds.EUS_ProfilesForUser(MasterRowIndex)
'                _mirrorRow = _ds.EUS_ProfilesForUser(MirrorRowIndex)
'            End If

'            _isDataLoaded = True
'        End If
'    End Sub


'    Public Sub Update()
'        If (_ds IsNot Nothing) Then DataHelpers.UpdateEUS_ProfilesForUser(_ds)
'    End Sub

'End Class





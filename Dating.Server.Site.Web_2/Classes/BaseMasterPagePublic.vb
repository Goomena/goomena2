﻿Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Core.DLL
Imports System.Reflection

Public Class BaseMasterPagePublic
    Inherits BaseMasterPage


    Dim _RequestedLAGID As String
    Protected ReadOnly Property RequestedLAGID As String
        Get
            If (_RequestedLAGID Is Nothing) Then
                _RequestedLAGID = ""

                Dim currentUrl As String = Request.Url.ToString()
                Dim requestedLag As String = clsLanguageHelper.GetLAGFromURL(currentUrl)

                If (String.IsNullOrEmpty(requestedLag) AndAlso clsLanguageHelper.IsSupportedLAG(Request.QueryString("LAG"), Session("GEO_COUNTRY_CODE"))) Then
                    requestedLag = Request.QueryString("LAG")
                End If
                If (Not String.IsNullOrEmpty(requestedLag)) Then
                    _RequestedLAGID = requestedLag
                End If

                'If (Not String.IsNullOrEmpty(Request.QueryString("lag"))) Then
                '    _RequestedLAGID = Request.QueryString("lag")
                '    If (clsLanguage.IsSupportedLAG(_RequestedLAGID)) Then
                '        _RequestedLAGID = _RequestedLAGID.ToUpper()
                '    Else
                '        _RequestedLAGID = ""
                '    End If
                'End If
            End If
            Return _RequestedLAGID
        End Get
    End Property


    Dim _mineMasterProfile As EUS_Profile
    Protected Function GetCurrentMasterProfile() As EUS_Profile
        Try

            If (_mineMasterProfile Is Nothing) AndAlso Me.MasterProfileId > 0 Then
                Using cmdb As CMSDBDataContext = Me.GetCMSDBDataContext
                    _mineMasterProfile = cmdb.EUS_Profiles.Where(Function(itm) (itm.ProfileID = Me.MirrorProfileId OrElse itm.ProfileID = Me.MasterProfileId) AndAlso itm.IsMaster = True).SingleOrDefault()
                End Using

            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Return _mineMasterProfile
    End Function


    Dim _mineMirrorProfile As EUS_Profile
    Protected Function GetCurrentMirrorProfile() As EUS_Profile
        Try

            If (_mineMirrorProfile Is Nothing) AndAlso Me.MirrorProfileId > 0 Then
                Using cmdb As CMSDBDataContext = Me.GetCMSDBDataContext
                    _mineMirrorProfile = cmdb.EUS_Profiles.Where(Function(itm) (itm.ProfileID = Me.MirrorProfileId OrElse itm.ProfileID = Me.MasterProfileId) AndAlso itm.IsMaster = False).SingleOrDefault()
                End Using

            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Return _mineMirrorProfile
    End Function




    Dim _currentProfile As EUS_Profile
    Protected Function GetCurrentProfile(Optional ByVal CheckOnly As Boolean = False) As EUS_Profile
        Try

            If (_currentProfile Is Nothing) Then
                _currentProfile = Me.GetCurrentMasterProfile()
                If (_currentProfile Is Nothing) Then
                    _currentProfile = Me.GetCurrentMirrorProfile()
                End If

                ' not approved member
                If (_currentProfile Is Nothing) Then
                    Using cmdb As CMSDBDataContext = Me.GetCMSDBDataContext
                        _currentProfile = cmdb.EUS_Profiles.Where(Function(itm) itm.ProfileID = Me.MasterProfileId AndAlso itm.IsMaster = False).SingleOrDefault()
                    End Using

                End If
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        'If (_currentProfile Is Nothing AndAlso Not CheckOnly) Then Throw New ProfileNotFoundException()
        Return _currentProfile
    End Function
    Private Sub controlCheck(ByVal ControlsList As UI.ControlCollection)
        For Each control As Control In ControlsList
            ' Now, using reflection, and get out all the properties.
            Dim properties As PropertyInfo() = control.[GetType]().GetProperties(BindingFlags.Instance Or BindingFlags.[Public])

            ' Loop through each one
            For i As Integer = 0 To properties.Length - 1
                ' Check if implements UrlPropertyAttribute, passing true to check 
                ' inheritance too. Also check we can read it.
                If properties(i).IsDefined(GetType(UrlPropertyAttribute), True) AndAlso properties(i).CanRead Then
                    ' It is implemented, so get the value out a as string
                    Dim url As String = TryCast(properties(i).GetValue(control, Nothing), String)

                    ' Double check we have a value. If we don't, we don't care
                    If Not [String].IsNullOrEmpty(url) Then
                        ' We do, so check it applies to a static resource on our site. 
                        ' For example, it points to another .aspx page, we don't 
                        ' want to rewrite it.
                        If Me.IsStaticResourceUrl(url) Then
                            ' It is, so rewrite it
                            Dim rewrittenUrl As String = Me.RewriteStaticResourceUrl(Page, url)

                            ' Set the value back
                            properties(i).SetValue(control, rewrittenUrl, Nothing)
                        End If
                    End If
                End If
            Next
            If control.Controls.Count > 0 Then
                controlCheck(control.Controls)
            End If
        Next

    End Sub
    'Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
    '    Try
    '        Dim re As Integer = 1
    '        Try
    '            If ConfigurationManager.AppSettings("CdnEnabled") <> "" AndAlso CInt(ConfigurationManager.AppSettings("CdnEnabled")) <> 1 Then
    '                re = 0
    '            End If
    '            If re < 1 Then
    '                Dim page As MasterPage = DirectCast(sender, MasterPage)
    '                controlCheck(page.Controls)
    '                For Each control As Control In page.Controls
    '                    ' Check if a HTML link
    '                    If TypeOf control Is HtmlLink Then
    '                        ' It is, so get it
    '                        Dim link As HtmlLink = DirectCast(control, HtmlLink)

    '                        ' Check we can rewrite it
    '                        If Me.IsStaticResourceUrl(link.Href) Then
    '                            ' We can, so rewrite it
    '                            Dim rewrittenUrl As String = Me.RewriteStaticResourceUrl(page, link.Href)

    '                            ' Set it again
    '                            link.Href = rewrittenUrl
    '                        End If
    '                    End If
    '                Next


    '            End If
    '        Catch ex As Exception
    '        End Try
    '    Catch ex As Exception

    '    End Try

    'End Sub
    'Protected Function IsStaticResourceUrl(url As String) As Boolean
    '    Dim result As Boolean = False

    '    ' Clean up the URL
    '    Dim cleanUrl As String = url.ToLowerInvariant()

    '    ' Check it is relative to the site with a tilde ('~')
    '    If cleanUrl.StartsWith("~") Then
    '        ' Check points to a static URL
    '        If cleanUrl.EndsWith(".jpg") OrElse cleanUrl.EndsWith(".jpeg") OrElse cleanUrl.EndsWith(".gif") OrElse cleanUrl.EndsWith(".htm") OrElse cleanUrl.EndsWith(".html") OrElse cleanUrl.EndsWith(".css") OrElse cleanUrl.EndsWith(".js") Then
    '            ' TODO: Add more file types here
    '            ' It matches
    '            result = True
    '        End If

    '        Return result
    '    End If
    'End Function
    'Protected Function RewriteStaticResourceUrl(page As Page, url As String) As String
    '    ' Create it by combining the CDN URL with the resolved URL
    '    Dim result As String = "http://www.goomena.com/" & page.ResolveUrl(url)

    '    Return result
    'End Function

End Class

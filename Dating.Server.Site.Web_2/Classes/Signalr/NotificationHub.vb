﻿
Imports System.Web
Imports Microsoft.AspNet.SignalR.Hubs
Imports Microsoft.AspNet.SignalR
Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Core.DLL
Imports System.Threading.Tasks
<HubName("NotificationHub")> _
Public Class NotificationHub
    Inherits Hub

#Region "PublicPart"
    'Public Async Function BroadcastMessage(message As String) As Task(Of Boolean)
    '    ' Call the broadcastMessage method to update clients.
    '    Return Await SendMessage(message)
    'End Function

    Public Overrides Function OnConnected() As Task
        Try
            Return Joined()
        Catch ex As Exception

        End Try
        Return MyBase.OnConnected()
    End Function
    Public Overrides Function OnReconnected() As Task
        Try
            Return Joined()
        Catch ex As Exception

        End Try
        Return MyBase.OnReconnected()
    End Function
    Public Overrides Function OnDisconnected(stopCalled As Boolean) As Task
        Try
            Return DisconnectMe()
        Catch ex As Exception
        End Try
        Return MyBase.OnDisconnected(stopCalled)
    End Function

    Public Async Function BroadcastMessageFromAdmin(ByVal _SignalType As SignarNotificationType, ByVal _message As String, ByVal _FrPrId As Integer, ByVal _PhotoId As Long, ByVal _count As Integer) As Task(Of Boolean)
        ' Call the broadcastMessage method to update clients.
        Return Await SendMessageFromAdmin(_SignalType, _message, _FrPrId, _PhotoId, _count)
    End Function
#End Region

#Region "PrivatePart"
    'Private Function SendMessage(_message As String) As Task(Of Boolean)
    '    Return Task.Run(Function() As Boolean
    '                        Dim re As Boolean = False
    '                        Try
    '                            If (Context Is Nothing OrElse Context.User Is Nothing) Then Return False
    '                            Dim LoginName As String = Context.User.Identity.Name
    '                            Dim id As System.Web.Security.FormsIdentity = Context.User.Identity
    '                            Dim ticket As System.Web.Security.FormsAuthenticationTicket = id.Ticket
    '                            If Context.User.IsInRole("AdminUserClientHub") AndAlso ticket.Version = 2 Then

    '                            Else
    '                                Dim result As New clsDataRecordLoginMemberReturn
    '                                result = clsSignalRLogin.PerfomRelogin(LoginName)
    '                                If result.HasErrors = False And result.IsValid Then
    '                                    Dim _ds As New dsConnectedUsers
    '                                    clsSignalrHelper.GetSignalRConnectionStrings(_ds, result.ProfileID, result.GenderId, result.PrivacySettings_HideMeFromSearchResults, result.Country)
    '                                    If _ds.Eus_ConnectionStrings.Count > 0 Then
    '                                        For Each r As dsConnectedUsers.Eus_ConnectionStringsRow In _ds.Eus_ConnectionStrings
    '                                            Clients.User(r.LoginName).appendNewMessage(_message)
    '                                        Next
    '                                    End If
    '                                    re = True
    '                                End If
    '                            End If
    '                        Catch ex As Exception

    '                        End Try
    '                        Return re
    '                    End Function
    '        )
    'End Function
    Private Function SendMessageFromAdmin(ByVal SignalType As SignarNotificationType, ByVal message As String, ByVal FrPrId As Integer, ByVal PhotoId As Long, ByVal Count As Integer) As Task(Of Boolean)
        Return Task.Run(Function() As Boolean
                            Dim re As Boolean = False
                            Try
                                If (Context Is Nothing OrElse Context.User Is Nothing) Then Return False
                                Dim LoginName As String = Context.User.Identity.Name
                                Dim id As System.Web.Security.FormsIdentity = Context.User.Identity
                                Dim ticket As System.Web.Security.FormsAuthenticationTicket = id.Ticket
                                If Context.User.IsInRole("AdminUserClientHub") AndAlso ticket.Version = 2 Then
                                    Dim dsm As New DSMembers
                                    If clsSignalrHelper.GetProfile(FrPrId, dsm) Then
                                        If dsm.EUS_Profiles.Rows.Count > 0 Then
                                            Dim r As DSMembers.EUS_ProfilesRow = dsm.EUS_Profiles.Rows(0)
                                            Dim gender As Integer = r.GenderId
                                            Dim country As String = If(r.IsCountryNull, "US", r.Country)
                                            Dim hidem As Boolean = If(r.IsPrivacySettings_HideMeFromSearchResultsNull, False, r.PrivacySettings_HideMeFromSearchResults)
                                            Dim _ds As New dsConnectedUsers
                                            clsSignalrHelper.GetSignalRConnectionStrings(_ds, FrPrId, gender, hidem, country)
                                            If _ds.Eus_ConnectionStrings.Count > 0 Then
                                                For Each r1 As dsConnectedUsers.Eus_ConnectionStringsRow In _ds.Eus_ConnectionStrings
                                                    Dim NotificationId As Long = clsSignalrHelper.AddNotificationInDatabase(FrPrId, r1.ProfileId, SignalType, message, PhotoId, Count)
                                                    Clients.User(r1.LoginName).appendNewMessage(clsEncryptDecryptDes.encrypt(NotificationId))
                                                Next
                                            End If
                                            _ds.Dispose()
                                        End If
                                    End If
                                    dsm.Dispose()
                                    re = True
                                End If
                            Catch ex As Exception

                            End Try
                            Return re
                        End Function
            )
    End Function
    Private Function Joined() As Task(Of Boolean)
        'Id = Context.ConnectionId,        
        Return Task.Run(Function() As Boolean
                            Dim re As Boolean = False
                            Try
                                If (Context Is Nothing OrElse Context.User Is Nothing) Then Return False
                                Dim LoginName As String = Context.User.Identity.Name
                                Dim id As System.Web.Security.FormsIdentity = Context.User.Identity
                                Dim ticket As System.Web.Security.FormsAuthenticationTicket = id.Ticket
                                If Context.User.IsInRole("AdminUserClientHub") AndAlso ticket.Version = 2 Then
                                Else
                                    Dim result As New clsDataRecordLoginMemberReturn
                                    result = clsSignalRLogin.PerfomRelogin(LoginName)
                                    If result.HasErrors = False And result.IsValid Then
                                        clsSignalrHelper.AddUser(result.ProfileID, result.MirrorProfileID, result.LoginName, result.GenderId, Process.GetCurrentProcess().Id, Context.ConnectionId)
                                    End If
                                End If
                                re = True
                            Catch ex As Exception
                            End Try
                            Return re
                        End Function)
    End Function



    Private Function DisconnectMe() As Task(Of Boolean)
        Return Task.Run(Function() As Boolean
                            Dim re As Boolean = False
                            Try
                                clsSignalrHelper.DeleteUser(Context.ConnectionId, Process.GetCurrentProcess().Id)
                                re = True
                            Catch ex As Exception
                            End Try
                            Return re
                        End Function
          )
    End Function

#End Region











End Class

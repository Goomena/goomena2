﻿Imports Dating.Server.Core.DLL
Imports Dating.Server.Datasets.DLL
Imports Microsoft.AspNet.SignalR.Hubs

Public Class clsSignalRLogin
   


    Public Shared Function PerfomRelogin(loginName As String) As clsDataRecordLoginMemberReturn
        Dim DataRecordLoginReturn As New clsDataRecordLoginMemberReturn
        DataRecordLoginReturn.IsValid = False
        DataRecordLoginReturn.HasErrors = False
        Dim pass As String = Nothing
        Try
            Using dc As New CMSDBDataContext(ModGlobals.ConnectionString)
                Try
                    ' retrieve user password
                    pass = (From itm In dc.EUS_Profiles
                            Where (itm.LoginName.ToUpper() = loginName.ToUpper() OrElse _
                                   itm.eMail.ToUpper() = loginName.ToUpper()) _
                            AndAlso _
                                  (itm.Status = ProfileStatusEnum.Approved OrElse _
                                     itm.Status = ProfileStatusEnum.NewProfile OrElse _
                                     itm.Status = ProfileStatusEnum.Rejected OrElse _
                                     itm.Status = ProfileStatusEnum.LIMITED) _
                            Select itm.Password).FirstOrDefault()
                Catch ex As Exception
                End Try
            End Using
            Dim IsMobileAccess As Boolean? = False
            Dim DataRecordLogin As New clsDataRecordLoginMember()
            DataRecordLogin.LoginName = loginName
            DataRecordLogin.Password = pass
            DataRecordLogin.RecordLastLogin = -11
            DataRecordLogin.IsMobile = clsNullable.NullTo(IsMobileAccess, False)
            DataRecordLoginReturn = PerfomLogin(DataRecordLogin)
        Catch ex As Exception
            DataRecordLoginReturn.HasErrors = True
            DataRecordLoginReturn.Message = ex.Message
        End Try
        Return DataRecordLoginReturn
    End Function
    Public Shared Function PerfomLogin(DataRecordLogin As clsDataRecordLoginMember) As clsDataRecordLoginMemberReturn
        Dim DataRecordLoginReturn As New clsDataRecordLoginMemberReturn
        DataRecordLoginReturn.IsValid = False
        DataRecordLoginReturn.HasErrors = False
        Try
            Using ds = DataHelpers.GetEUS_Profiles_ByLoginPass(DataRecordLogin.LoginName, DataRecordLogin.Password)
                Dim EUS_ProfilesDataTable As DSMembers.EUS_ProfilesDataTable = ds.EUS_Profiles
                If EUS_ProfilesDataTable.Rows.Count > 0 Then
                    Dim updateMember As Boolean = False
                    Dim rowIndex As Integer = 0
                    For rowIndex = 0 To (EUS_ProfilesDataTable.Rows.Count - 1)
                        'While rowIndex < EUS_ProfilesDataTable.Rows.Count
                        Dim EUS_MembersRow As DSMembers.EUS_ProfilesRow = EUS_ProfilesDataTable.Rows(rowIndex)
                        If ((EUS_ProfilesDataTable.Rows.Count = 2 AndAlso EUS_MembersRow.IsMaster = True) OrElse (EUS_ProfilesDataTable.Rows.Count = 1)) Then
                            If (EUS_MembersRow.Status <> ProfileStatusEnum.Deleted) Then
                                DataRecordLoginReturn.Fill(EUS_MembersRow)
                                EUS_MembersRow.LastLoginDateTime = DateTime.UtcNow
                            Else
                                DataRecordLoginReturn.HasErrors = True
                                DataRecordLoginReturn.Message = ProfileStatusEnum.Deleted.ToString()
                            End If
                        End If
                    Next
                Else
                    DataRecordLoginReturn.Message = "Error Login Or Password"
                End If
            End Using
        Catch ex As Exception
            DataRecordLoginReturn.HasErrors = True
            DataRecordLoginReturn.Message = ex.Message
        End Try
        Return DataRecordLoginReturn
    End Function
    Public Shared Function IsLocalhost(ByRef cont As HubCallerContext) As Boolean
        Try
            If (cont IsNot Nothing) Then
                Dim ip As String = ""
                If (cont.Request.Environment.TryGetValue("server.RemoteIpAddress", ip)) Then
                    ' ip = Request.ServerVariables("REMOTE_ADDR") '-- the IP address of the visitor

                    If (ip.StartsWith("127.") OrElse ip.Equals("::1")) Then
                        Return True
                    End If
                End If
            End If
        Catch
        End Try

        Return False
    End Function
End Class

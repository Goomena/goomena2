﻿Imports Microsoft.Owin
Imports Owin
Imports Microsoft.AspNet.SignalR
Imports Microsoft.AspNet.SignalR.SqlServer
<Assembly: OwinStartup(GetType(Startup))> 
Public Class Startup
    Public Sub Configuration(app As IAppBuilder)
        '' Any connection or hub wire up and configuration should go here
        'GlobalHost.Configuration.ConnectionTimeout = TimeSpan.FromSeconds(110)

        ''// Wait a maximum of 30 seconds after a transport connection is lost
        ''// before raising the Disconnected event to terminate the SignalR connection.
        'GlobalHost.Configuration.DisconnectTimeout = TimeSpan.FromSeconds(30)

        ''// For transports other than long polling, send a keepalive packet every
        ''// 10 seconds. 
        ''// This value must be no more than 1/3 of the DisconnectTimeout value.
        'GlobalHost.Configuration.KeepAlive = TimeSpan.FromSeconds(10)

        'GlobalHost.DependencyResolver.UseSqlServer(System.Configuration.ConfigurationManager.ConnectionStrings("SignalrDBconnectionString").ConnectionString)
        'app.MapSignalR("/NotificationHubs", New HubConfiguration)
    End Sub
End Class


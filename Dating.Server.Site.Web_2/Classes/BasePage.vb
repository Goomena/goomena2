Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Collections.Generic
Imports System.Configuration
Imports System.IO
Imports System.Xml
Imports System.Xml.Schema
Imports System.Globalization
Imports System.Text
Imports System.Text.RegularExpressions
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Reflection
Imports DevExpress.Web.ASPxHeadline
Imports DevExpress.Web.ASPxSiteMapControl
Imports DevExpress.Web.ASPxClasses
Imports DevExpress.Web.ASPxClasses.Internal
Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Core.DLL


Partial Public Class BasePage
    Inherits System.Web.UI.Page
    Implements ILanguageDependableContent
    Private webStatistics As New clsWebStatistics()
    Private _UnloadingTasks As PageOnUnloadTasksList
    Public ReadOnly Property UnloadingTasks As PageOnUnloadTasksList
        Get
            If (_UnloadingTasks Is Nothing) Then
                _UnloadingTasks = New PageOnUnloadTasksList()
            End If
            Return _UnloadingTasks
        End Get
    End Property


    Protected ReadOnly Property LagID As String
        Get
            Return Me.Session("LagID")
        End Get
    End Property

    'Private _IsProfileActive As Boolean?
    'Public ReadOnly Property IsProfileActive As Boolean
    '    Get
    '        Dim ProfileID As Integer? = HttpContext.Current.Session("ProfileID")
    '        If (Not _IsProfileActive.HasValue) Then
    '            _IsProfileActive = False
    '            If (clsNullable.NullTo(ProfileID) > 0) Then
    '                _IsProfileActive = DataHelpers.EUS_Profile_IsProfileActive(Me.CMSDBDataContext, ProfileID)
    '            End If
    '        End If
    '        Return _IsProfileActive
    '    End Get
    'End Property


    Private _IsMale As Boolean?
    Public ReadOnly Property IsMale As Boolean
        Get
            If (Not _IsMale.HasValue) Then
                _IsMale = (Me.SessionVariables.MemberData.GenderId = ProfileHelper.gMaleGender.GenderId)
            End If
            Return _IsMale
        End Get
    End Property


    Private _IsFemale As Boolean?
    Public ReadOnly Property IsFemale As Boolean
        Get
            If (Not _IsFemale.HasValue) Then
                '_IsFemale = (Me.GetCurrentProfile().GenderId = ProfileHelper.gFemaleGender.GenderId)
                _IsFemale = (Me.SessionVariables.MemberData.GenderId = ProfileHelper.gFemaleGender.GenderId)
            End If
            Return _IsFemale
        End Get
    End Property


    Private __isReferrer As Boolean?
    Protected ReadOnly Property IsReferrer As Boolean
        Get
            If (__isReferrer Is Nothing) Then
                __isReferrer = (Me.GetCurrentProfile().ReferrerParentId.HasValue AndAlso Me.GetCurrentProfile().ReferrerParentId > 0)
            End If
            Return __isReferrer
        End Get
    End Property


    Private __isVirtual As Boolean?
    Protected ReadOnly Property IsVirtual As Boolean
        Get
            If (__isVirtual Is Nothing) Then
                __isVirtual = clsProfilesPrivacySettings.GET_AdminSetting_IsVirtual(MasterProfileId)
            End If
            Return __isVirtual
        End Get
    End Property


    Public ReadOnly Property IsPublicRootMaster As Boolean
        Get
            Return (Me.Master IsNot Nothing AndAlso Me.Master.GetType().Name = "root_master")
        End Get
    End Property



    Public ReadOnly Property IsPublicRootMaster2 As Boolean
        Get
            Return (Me.Master IsNot Nothing AndAlso Me.Master.GetType().Name = "root2_master")
        End Get
    End Property




    Public ReadOnly Property IsLagChanged As Boolean
        Get
            If (Me.IsPublicRootMaster) Then
                Dim _master As _RootMaster1 = Me.Master
                Return _master.VerifyLagProps

            ElseIf (Me.IsPublicRootMaster2) Then
                Dim _master As _RootMaster140603 = Me.Master
                Return _master.VerifyLagProps

            End If
            'If (Me.Master IsNot Nothing AndAlso Me.Master.GetType().Name = "root_master") Then
            '    Dim _master As _RootMaster1 = Me.Master
            '    Return _master.VerifyLagProps

            'ElseIf (Me.Master IsNot Nothing AndAlso Me.Master.GetType().Name = "root2_master") Then
            '    Dim _master As _RootMaster140603 = Me.Master
            '    Return _master.VerifyLagProps

            'End If

            Return False
        End Get
    End Property


    Private __Language As clsLanguageHelper
    Protected ReadOnly Property LanguageHelper As clsLanguageHelper
        Get
            If (__Language Is Nothing) Then __Language = New clsLanguageHelper(Context)
            Return __Language
        End Get
    End Property


    Private __ProfileCountry As SYS_CountriesGEO
    Protected ReadOnly Property ProfileCountry As SYS_CountriesGEO
        Get
            If (__ProfileCountry Is Nothing) Then
                Using cmdb As CMSDBDataContext = Me.GetCMSDBDataContext


                    __ProfileCountry = SYS_CountriesGEOHelper.SYS_CountriesGEO_GetForCountry(cmdb, Me.SessionVariables.MemberData.Country)
                End Using
  End If
            Return __ProfileCountry
        End Get
    End Property


    Private __ProfileSettings As EUS_ProfilesPrivacySetting
    Protected ReadOnly Property ProfileSettings As EUS_ProfilesPrivacySetting
        Get
            If (__ProfileSettings Is Nothing) Then
                Using cmdb As CMSDBDataContext = Me.GetCMSDBDataContext


                    __ProfileSettings = (From itm In cmdb.EUS_ProfilesPrivacySettings
                                        Where itm.ProfileID = Me.MasterProfileId OrElse itm.MirrorProfileID = Me.MasterProfileId
                                        Select itm).FirstOrDefault()

                    If (__ProfileSettings Is Nothing) Then
                        clsProfilesPrivacySettings.CreateNew_EUS_ProfilesPrivacySetting(Me.MasterProfileId, Me.MirrorProfileId)

                        __ProfileSettings = (From itm In cmdb.EUS_ProfilesPrivacySettings
                               Where itm.ProfileID = Me.MasterProfileId
                               Select itm).FirstOrDefault()

                    End If
                End Using
            End If
            Return __ProfileSettings
        End Get
    End Property


    Private _UnlimitedMsgID As Integer?
    Public ReadOnly Property UnlimitedMsgID As Integer
        Get
            If (Not _UnlimitedMsgID.HasValue) Then
                _UnlimitedMsgID = 0
                If (Not String.IsNullOrEmpty(Request.QueryString("unmsg"))) Then
                    Long.TryParse(Request.QueryString("unmsg"), _UnlimitedMsgID)
                End If
            End If
            Return _UnlimitedMsgID
        End Get
    End Property



    Public ReadOnly Property IsHTTPS As Boolean
        Get
            Return (Me.Request.Url.Scheme = "https")
        End Get
    End Property



    Public ReadOnly Property MasterProfileId As Integer
        Get
            Return Me.Session("ProfileID")
        End Get
    End Property


    Protected ReadOnly Property MirrorProfileId As Integer
        Get
            Return Me.Session("MirrorProfileID")
        End Get
    End Property


    Protected ReadOnly Property BlockMessagesToBeReceivedByOther As Boolean
        Get
            If (ViewState("IsEnabled_BlockMessagesToBeReceivedByOther") Is Nothing) Then
                ViewState("IsEnabled_BlockMessagesToBeReceivedByOther") = clsProfilesPrivacySettings.GET_AdminSetting_BlockMessagesToBeReceivedByOther(Me.MasterProfileId)
            End If
            Return ViewState("IsEnabled_BlockMessagesToBeReceivedByOther")
        End Get
    End Property


    Dim _sessVars As clsSessionVariables
    Protected ReadOnly Property SessionVariables As clsSessionVariables
        Get
            If (_sessVars Is Nothing) Then
                _sessVars = clsSessionVariables.GetCurrent()
            End If
            If (_sessVars Is Nothing) Then
                _sessVars = New clsSessionVariables()
                HttpContext.Current.Session("SessionVariables") = _sessVars
            End If
            Return _sessVars
        End Get
    End Property


    Private _globalStrings As clsPageData
    Public ReadOnly Property globalStrings As clsPageData
        Get
            If (_globalStrings Is Nothing) Then
                _globalStrings = New clsPageData("GlobalStrings", HttpContext.Current)
                AddHandler _globalStrings.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _globalStrings
        End Get
    End Property

    Private Sub Page_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        Try
            If Me._pageData IsNot Nothing Then
                RemoveHandler Me._pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If

        Catch ex As Exception
        End Try
        Try
            If Me._globalStrings IsNot Nothing Then
                RemoveHandler Me._globalStrings.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If

        Catch ex As Exception
        End Try
        Try
            MyBase.Dispose()
        Catch ex As Exception

        End Try
    End Sub
    Protected _pageData As clsPageData
    Protected ReadOnly Property CurrentPageData As clsPageData
        Get
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData(Context, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property

    Public ReadOnly Property GetCMSDBDataContext As CMSDBDataContext
        Get
            Return New CMSDBDataContext(ModGlobals.ConnectionString)
        End Get
    End Property
       

    'Dim _CMSDBDataContext As CMSDBDataContext
    'Protected ReadOnly Property CMSDBDataContext As CMSDBDataContext
    '    Get
    '        Try
    '            If _CMSDBDataContext IsNot Nothing Then
    '                ' exception thrown when disposed
    '                Dim a As System.Data.Common.DbConnection = _CMSDBDataContext.Connection


    '            End If
    '        Catch ex As Exception
    '            _CMSDBDataContext = Nothing
    '        End Try

    '        If (_CMSDBDataContext Is Nothing) Then 'OrElse _DataContext.Connection.State = ConnectionState.Broken OrElse _DataContext.Connection.State = ConnectionState.Closed) Then
    '            _CMSDBDataContext = New CMSDBDataContext(ModGlobals.ConnectionString)
    '        End If
    '        Return _CMSDBDataContext
    '    End Get
    'End Property



    Dim _mineMasterProfile As vw_EusProfile_Light
    Protected Function GetCurrentMasterProfile() As vw_EusProfile_Light
        Try

            If (_mineMasterProfile Is Nothing) Then
                Using cmdb As CMSDBDataContext = Me.GetCMSDBDataContext
                    _mineMasterProfile = DataHelpers.GetCurrentMasterProfile(cmdb, Me.MasterProfileId, Me.MirrorProfileId)
                End Using

            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Return _mineMasterProfile
    End Function


    'Dim _mineMirrorProfile As vw_EusProfile_Light
    'Protected Function GetCurrentMirrorProfile() As vw_EusProfile_Light
    '    Try

    '        If (_mineMirrorProfile Is Nothing) Then
    '            _mineMirrorProfile = DataHelpers.GetCurrentMirrorProfile(Me.CMSDBDataContext, Me.MasterProfileId, Me.MirrorProfileId)
    '        End If

    '    Catch ex As Exception
    '        WebErrorMessageBox(Me, ex, "")
    '    End Try

    '    Return _mineMirrorProfile
    'End Function




    Dim _currentProfile As vw_EusProfile_Light
    Protected Function GetCurrentProfile(Optional ByVal CheckOnly As Boolean = False) As vw_EusProfile_Light
        Try

            If (_currentProfile Is Nothing) Then
                Using cmdb As CMSDBDataContext = Me.GetCMSDBDataContext
                    _currentProfile = DataHelpers.GetCurrentProfile(cmdb, Me.MasterProfileId, Me.MirrorProfileId)
                End Using
                
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        If (_currentProfile Is Nothing AndAlso Not CheckOnly) Then Throw New ProfileNotFoundException()
        Return _currentProfile
    End Function


    'Dim _minePhotosList As List(Of EUS_CustomerPhoto)
    'Protected Function GetPhotosList() As List(Of EUS_CustomerPhoto)
    '    Try

    '        If (_minePhotosList Is Nothing) AndAlso Me.MasterProfileId > 0 Then
    '            _minePhotosList = DataHelpers.GetPhotosList(Me.CMSDBDataContext, Me.MasterProfileId, Me.MirrorProfileId)
    '            '_minePhotosList = Me.CMSDBDataContext.EUS_CustomerPhotos.Where(Function(phot) phot.CustomerID = Me.MasterProfileId OrElse phot.CustomerID = Me.MirrorProfileId AndAlso (phot.IsDeleted Is Nothing Or phot.IsDeleted = 0)).ToList()
    '        End If

    '        If (_minePhotosList Is Nothing) Then _minePhotosList = New List(Of EUS_CustomerPhoto)()

    '    Catch ex As Exception
    '        WebErrorMessageBox(Me, ex, "")
    '    End Try

    '    Return _minePhotosList
    'End Function


    Dim _minePhotosCount As Integer?
    Protected Function GetPhotosCount() As Integer
        Try
            If (_minePhotosCount Is Nothing) AndAlso Me.MasterProfileId > 0 Then
                Using cmdb As CMSDBDataContext = Me.GetCMSDBDataContext
                    _minePhotosCount = DataHelpers.GetPhotosCount(cmdb, Me.MasterProfileId, Me.MirrorProfileId)
                End Using

            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        Finally
            If (_minePhotosCount Is Nothing) Then _minePhotosCount = 0
        End Try

        Return _minePhotosCount.Value
    End Function


    Protected Sub ResetEUS_ProfileObject()
        _currentProfile = Nothing
        '_mineMirrorProfile = Nothing
        _mineMasterProfile = Nothing
    End Sub

    Public Sub VerifyLagProps()
        If (Me.Master IsNot Nothing) Then
            If (Me.Master.GetType().Name = "root_master") Then
                Dim _master As _RootMaster1 = Me.Master
                _master.VerifyLagProps()
                'Return
            ElseIf (Me.Master.GetType().Name = "root2_master") Then
                Dim _master As _RootMaster140603 = Me.Master
                _master.VerifyLagProps()
                'Return
            ElseIf (Me.Master.GetType().Name = "rootlanding_master") Then
                Dim _master As _RootLandingMaster = Me.Master
                _master.VerifyLagProps()
                'Return
            End If
        End If
    End Sub

    ' Page PreInit 

    Public Overridable Sub Master_LanguageChanged() Implements ILanguageDependableContent.Master_LanguageChanged

    End Sub


    ' called from public default.aspx
    Protected Overridable Sub Page_PreInit(ByVal sender As Object, ByVal e As EventArgs)

        Try
            If clsCurrentContext.VerifyLogin() = True Then
                If (Request("master") = "inner") Then
                    Me.MasterPageFile = "~/Members/Members2Inner.Master"
                Else
                    'Me.MasterPageFile = "~/Members/Members2.Master"
                End If
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try


        Try

            If Not String.IsNullOrEmpty(Request.QueryString("theme")) Then
                Session("myTheme") = Request.QueryString("theme")
            End If

            Dim themeName = gDefaultThemeName

            If Not String.IsNullOrEmpty(Session("myTheme")) Then
                themeName = Session("myTheme")
            End If



            Dim clientScriptBlock As String = "var DXCurrentThemeCookieName = """ & themeName & """;"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "DXCurrentThemeCookieName", clientScriptBlock, True)
            'Me.Theme = themeName
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try


        Try
            If Not String.IsNullOrEmpty(SessionVariables.myCulture) Then
                SessionVariables.myCulture = gDomainCOM_DefaultLAG
                Dim aCookie As New HttpCookie("otmData")
                aCookie.Values("languagePref") = SessionVariables.myCulture
                aCookie.Expires = System.DateTime.Now.AddDays(21)
                Response.Cookies.Add(aCookie)
            End If
            'gLAG.SetLag(SessionVariables.myCulture)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub




    ' Page Load 
    Protected Overrides Sub OnLoad(ByVal e As EventArgs)
        Try

            '' check if page is public
            '' redirect public pages
            If (UrlUtils.GetPageScope(Request.Url) = PageScopeEnum.PublicPage AndAlso
                Not clsCurrentContext.VerifyLogin()) Then

                LanguageHelper.RedirectToNewLAGURL()

            End If

            clsLanguageHelper.CheckDomainAndRedirect(Request.Url.Host)
            Me.VerifyLagProps()
            MyBase.OnLoad(e)

            'If UsingIE7CompatibilityMode Then
            '    Dim ie7CompatModeMeta As LiteralControl = RenderUtils.CreateLiteralControl()
            '    'ie7CompatModeMeta.ID = RenderUtils.IE8CompatibilityMetaID
            '    'ie7CompatModeMeta.Text = RenderUtils.IE8CompatibilityMetaText
            '    Header.Controls.AddAt(0, ie7CompatModeMeta)
            'End If

            '' Scripts
            'ASPxWebControl.RegisterUtilsScript(Me)
            ''RegisterScript("Utilities", "~/Scripts/Utilities.js")
            '' CSS
            ''RegisterCSSLink("~/CSS/styles.css")
            ''RegisterCSSLink("~/CSS/ToolBars.css")
            'If (Not String.IsNullOrEmpty(Me.cssLink_Renamed)) Then
            '    RegisterCSSLink(Me.cssLink_Renamed)
            'End If

            'If Not IsPostBack AndAlso Not IsCallback Then
            Try
                Dim szURL As String = Request.Url.ToString()
                If Len(szURL) Then
                    Dim szPage As String = System.IO.Path.GetFileName(Request.Url.ToString())
                    Dim iStartParams As Integer = InStr(szPage, "?")
                    If iStartParams > 1 Then
                        szPage = Mid(szPage, 1, iStartParams - 1)
                    End If
                    SessionVariables.Page = szPage
                    SessionVariables.IdentityLoginName = HttpContext.Current.User.Identity.Name


                    'Dim gStat As New clsWebStatistics
                    'gStat.AddNew(Me)
                    Dim refferer As String = clsWebStatistics.GetReferrer(Page.Request("HTTP_REFERER"), Page.Request.Url.Host)
                    webStatistics.SessionID = Page.Session.SessionID
                    webStatistics.Referrer = refferer
                    webStatistics.CustomReferrer = Page.Session("CustomReferrer")
                    webStatistics.IP = Page.Session("IP")
                    webStatistics.Agent = Page.Session("Agent")
                    webStatistics.Page = SessionVariables.Page
                    webStatistics.LoginName = SessionVariables.IdentityLoginName
                    webStatistics.StatCookie = Page.Session("StatCookie")
                    webStatistics.CustomAction = Page.Session("StaticalCustomAction")
                    webStatistics.GEO_COUNTRY_CODE = Page.Session("GEO_COUNTRY_CODE")
                    webStatistics.RequestedUrl = Page.Request.Url.ToString()
                    webStatistics.ProfileID = Me.MasterProfileId

                End If
            Catch ex As Exception
            End Try
            'End If


            'Try
            '    If (SessionVariables.DateTimeToRegister IsNot Nothing AndAlso ConfigurationManager.AppSettings("OnRegisterNotifyUserAfterMinutes") > 0) Then
            '        If (Date.UtcNow - SessionVariables.DateTimeToRegister) > TimeSpan.FromMinutes(ConfigurationManager.AppSettings("OnRegisterNotifyUserAfterMinutes")) Then
            '            'Dim _profilerows As New clsProfileRows(Me.MasterProfileId)
            '            'Dim mirrorRow As DSMembers.EUS_ProfilesRow = _profilerows.GetMirrorRow()
            '            'SendNotificationEmailToMember_ToUpdateProfileInfo(mirrorRow)
            '            SessionVariables.DateTimeToRegister = Nothing
            '        End If
            '    End If
            'Catch ex As Exception
            'End Try


        Catch ex As System.Threading.ThreadAbortException
        End Try
    End Sub



    Public Sub Page_CustomStringRetrievalComplete(ByRef sender As clsPageData, ByRef Args As CustomStringRetrievalCompleteEventArgs)
        '   Dim __pageData As clsPageData = sender
        Dim result As String = Args.CustomString

        If (HttpContext.Current.Request.Url.Scheme.ToUpper() = "HTTPS") Then
            If (result.Contains("http://www.goomena.")) Then
                result = result.Replace("http://www.goomena.", "https://www.goomena.")
                Args.CustomString = result
            End If
            If (result.Contains("http://cdn.goomena.com")) Then
                result = result.Replace("http://cdn.goomena.com", "https://cdn.goomena.com")
                Args.CustomString = result
            End If
        End If

        If (Args.LagIDparam <> "US") Then
            Dim ancMatch As Match = clsWebFormHelper.AnchorRegex.Match(result, 0)
            If (ancMatch.Success) Then
                Args.CustomString = Me.FixHTMLBodyAnchors(result)
            End If
        End If
    End Sub

    Public Function FixHTMLBodyAnchors(html As String) As String
        html = LanguageHelper.FixHTMLBodyAnchors(html, GetLag(), Page.RouteData)
        Return html
    End Function


    ' Functions 
    Protected Overridable Function IsCurrentPage(ByVal oUrl As Object) As Boolean
        If oUrl Is Nothing Then
            Return False
        End If

        Dim result As Boolean = False
        Dim url As String = oUrl.ToString()
        If url.ToLower() = Page.Request.AppRelativeCurrentExecutionFilePath.ToLower() Then
            result = True
        End If
        Return result
    End Function

    Private Sub RegisterScript(ByVal key As String, ByVal url As String)
        Page.ClientScript.RegisterClientScriptInclude(key, Page.ResolveUrl(url))
    End Sub

    Private Sub RegisterCSSLink(ByVal url As String)
        Dim link As New HtmlLink()
        Page.Header.Controls.Add(link)
        link.EnableViewState = False
        link.Attributes.Add("type", "text/css")
        link.Attributes.Add("rel", "stylesheet")
        link.Href = url
    End Sub

 


    Private Sub Page_Error(sender As Object, e As System.EventArgs) Handles Me.Error
        '''''''''''''''''''''''''''''
        ' also check Application_Error
        ' in global.asax
        '''''''''''''''''''''''''''''
        Try
            Dim exSrv As Exception = Server.GetLastError()
            Dim exSrv__ As String = exSrv.ToString()
            Dim retrying As Boolean = False

            If (exSrv.GetType() Is GetType(System.Threading.ThreadAbortException)) Then
                Server.ClearError()
                Return
            End If

            Try
                'use retry param to indicate that retry was done
                If (Context.Request.Url.Query = "?retry" OrElse Context.Request.Url.Query.Contains("&retry")) Then

                Else
                    ' try to load again the same page one time
                    Server.ClearError()

                    If (Not Me.IsCallback) Then
                        Dim siteUrl As String = Request.Url.AbsoluteUri
                        If (siteUrl.IndexOf("?") > -1) Then
                            siteUrl = siteUrl & "&retry"
                        Else
                            siteUrl = siteUrl & "?retry"
                        End If
                        Response.Redirect(siteUrl, False)
                        HttpContext.Current.ApplicationInstance.CompleteRequest()
                    End If

                    retrying = True
                End If
            Catch
            End Try


            If (exSrv.Message = "The client disconnected.") Then
                'do nothing
            ElseIf (exSrv__.StartsWith("System.Web.HttpException (0x80004005): Failed to load viewstate.")) Then
                'do nothing
            ElseIf (exSrv__.StartsWith("System.Web.HttpException (0x80004005): The client disconnected.")) Then
                'do nothing
            ElseIf (exSrv.Message = "The state information is invalid for this page and might be corrupted.") Then
                'do nothing
            ElseIf (exSrv.Message.StartsWith("Validation of viewstate MAC failed.")) Then
                'do nothing
            Else
                If (retrying) Then
                    WebErrorMessageBox(Me, exSrv, "BasePage.Page_Error (Retrying to reload the page...)")
                Else
                    WebErrorMessageBox(Me, exSrv, "BasePage.Page_Error (No reload)")
                End If
            End If


        Catch ex As Exception
        End Try
    End Sub

    Private Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

    End Sub


    Protected Overridable Sub Page_PreLoad(sender As Object, e As System.EventArgs) Handles Me.PreLoad
        Try

            Session("CustomerAvailableCredits") = Nothing
            Session("GetMemberActionsCounters") = Nothing
            Session("EUS_Profile_IsProfileActive") = Nothing
            Session("IsAutorefresh") = Nothing

        Catch ex As Exception

        End Try
    End Sub
    Private Sub controlCheck(ByVal ControlsList As UI.ControlCollection)
        For Each control As Control In ControlsList
            ' Now, using reflection, and get out all the properties.
            Dim properties As PropertyInfo() = control.[GetType]().GetProperties(BindingFlags.Instance Or BindingFlags.[Public])

            ' Loop through each one
            For i As Integer = 0 To properties.Length - 1
                ' Check if implements UrlPropertyAttribute, passing true to check 
                ' inheritance too. Also check we can read it.
                If properties(i).IsDefined(GetType(UrlPropertyAttribute), True) AndAlso properties(i).CanRead Then
                    ' It is implemented, so get the value out a as string
                    Dim url As String = TryCast(properties(i).GetValue(control, Nothing), String)

                    ' Double check we have a value. If we don't, we don't care
                    If Not [String].IsNullOrEmpty(url) Then
                        ' We do, so check it applies to a static resource on our site. 
                        ' For example, it points to another .aspx page, we don't 
                        ' want to rewrite it.
                        If Me.IsStaticResourceUrl(url) Then
                            ' It is, so rewrite it
                            Dim rewrittenUrl As String = Me.RewriteStaticResourceUrl(Page, url)

                            ' Set the value back
                            properties(i).SetValue(control, rewrittenUrl, Nothing)
                        End If
                    End If
                End If
            Next
            If control.Controls.Count > 0 Then
                controlCheck(control.Controls)
            End If
        Next

    End Sub

    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        Try
            Dim re As Integer = 1
            Try
                If ConfigurationManager.AppSettings("CdnEnabled") <> "" AndAlso CInt(ConfigurationManager.AppSettings("CdnEnabled")) <> 1 Then
                    re = 0
                End If
                If re < 1 Then
                    Dim page As Page = DirectCast(sender, Page)
                    controlCheck(page.Controls)
                    For Each control As Control In page.Header.Controls
                        ' Check if a HTML link
                        If TypeOf control Is HtmlLink Then
                            ' It is, so get it
                            Dim link As HtmlLink = DirectCast(control, HtmlLink)

                            ' Check we can rewrite it
                            If Me.IsStaticResourceUrl(link.Href) Then
                                ' We can, so rewrite it
                                Dim rewrittenUrl As String = Me.RewriteStaticResourceUrl(page, link.Href)

                                ' Set it again
                                link.Href = rewrittenUrl
                            End If
                        End If
                    Next
                    Dim scriptManager__1 As ScriptManager = ScriptManager.GetCurrent(page)

                    ' Check we have one (otherwise do nothing)
                    If scriptManager__1 IsNot Nothing Then
                        ' Loop through each one
                        For Each script As ScriptReference In scriptManager__1.Scripts
                            ' Check we can rewrite it
                            If Me.IsStaticResourceUrl(script.Path) Then
                                ' We can, so rewrite it
                                Dim rewrittenUrl As String = Me.RewriteStaticResourceUrl(page, script.Path)

                                ' Set it again
                                script.Path = rewrittenUrl
                            End If
                        Next
                    End If
                End If
            Catch ex As Exception
            End Try
        Catch ex As Exception

        End Try

    End Sub
    Protected Function IsStaticResourceUrl(url As String) As Boolean
        Dim result As Boolean = False

        ' Clean up the URL
        Dim cleanUrl As String = url.ToLowerInvariant()

        ' Check it is relative to the site with a tilde ('~')
        If cleanUrl.StartsWith("~") Then
            ' Check points to a static URL
            If cleanUrl.EndsWith(".jpg") OrElse cleanUrl.EndsWith(".jpeg") OrElse cleanUrl.EndsWith(".gif") OrElse cleanUrl.EndsWith(".htm") OrElse cleanUrl.EndsWith(".html") OrElse cleanUrl.EndsWith(".css") OrElse cleanUrl.EndsWith(".js") Then
                ' TODO: Add more file types here
                ' It matches
                result = True
            End If

            Return result
        End If
    End Function
    Protected Function RewriteStaticResourceUrl(page As Page, url As String) As String
        ' Create it by combining the CDN URL with the resolved URL
        Dim result As String = "http://www.goomena.com/" & page.ResolveUrl(url)

        Return result
    End Function
    Private Sub Page_PreRenderComplete(sender As Object, e As System.EventArgs) Handles Me.PreRenderComplete

        Try
            Dim X_UA_Compatible As New HtmlMeta()
            X_UA_Compatible.Attributes.Add("http-equiv", "X-UA-Compatible")
            X_UA_Compatible.Attributes.Add("content", "IE=edge,chrome=1")
            Me.Header.Controls.AddAt(0, X_UA_Compatible)
        Catch
        End Try

        If (Not Page.IsPostBack) Then
            Try
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "js-global-my-gender", "setGlobalsGender(" & Me.SessionVariables.MemberData.GenderId & ");", True)
            Catch
            End Try
        End If
    End Sub



    Protected Overrides Sub OnUnload(e As System.EventArgs)
        Try
            MyBase.OnUnload(e)
        Catch ex As Exception
            WebErrorSendEmail(ex, "BasePage->Page_Unload")
        End Try


        Try

            If (clsCurrentContext.VerifyLogin()) Then
                Dim Status As Long = DataHelpers.EUS_Profiles_GetMasterStatus(Me.MasterProfileId)
                Me.SessionVariables.MemberData.Status = Status

                If ((Session("IsAdminLogin") Is Nothing OrElse Session("IsAdminLogin") = False) AndAlso (Session("IsAutorefresh") Is Nothing)) Then
                    If (Session("IsMobileAccess") = True) Then
                        DataHelpers.UpdateEUS_Profiles_Activity(Me.MasterProfileId, Session("IP"), True, True)
                    Else
                        DataHelpers.UpdateEUS_Profiles_Activity(Me.MasterProfileId, Session("IP"), True)
                    End If
                End If
            End If


            'If (_CMSDBDataContext IsNot Nothing) Then
            '    _CMSDBDataContext.Dispose()
            'End If

            'If (_minePhotosList IsNot Nothing) Then
            '    _minePhotosList.Clear()
            'End If

        Catch ex As Exception
            WebErrorSendEmail(ex, "BasePage->Page_Unload")
        End Try

        Try

            ' when year changed, update [CelebratingBirth] column of [EUS_Profiles] table
            ' check profiles for which CelebratingBirth not set, after registration

            If (Session("REGISTER_EUS_Profiles_UpdateCelebratingBirth") = 1 OrElse (DateTime.UtcNow.Month = 1 AndAlso DateTime.UtcNow.Day = 1)) Then
                DataHelpers.EUS_Profiles_UpdateCelebratingBirth()
                Session("REGISTER_EUS_Profiles_UpdateCelebratingBirth") = Nothing
            End If

        Catch ex As Exception
            WebErrorSendEmail(ex, "BasePage->Page_Unload")
        End Try

        If (Me._UnloadingTasks IsNot Nothing) Then
            Try
                'exec SendEmailNotification
                Me._UnloadingTasks.RunTasks()
            Catch ex As Exception
                WebErrorSendEmail(ex, "BasePage->executing UnloadingTasks")
            Finally
            End Try
        End If

        If (webStatistics.SessionID IsNot Nothing) Then
            Try
                ' update clsWebStatistics
                Page.Application.Lock()
                webStatistics.AddNew()
                Page.Application.UnLock()

            Catch ex As Exception
                Page.Application.UnLock()
                WebErrorSendEmail(ex, "BasePage->Page_Unload")
            Finally
            End Try
        End If

    End Sub




    Public Function GetLag() As String
        Dim str As String = Nothing
        Try
            If (clsLanguageHelper.IsSupportedLAG(HttpContext.Current.Session("LAGID"), Session("GEO_COUNTRY_CODE"))) Then
                str = HttpContext.Current.Session("LAGID")
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        If (str Is Nothing) Then
            If Request.Url.Host.EndsWith(".gr", StringComparison.OrdinalIgnoreCase) Then
                'goomena.gr
                str = gDomainGR_DefaultLAG
            Else

                If Request.Url.Host.EndsWith(".com", StringComparison.OrdinalIgnoreCase) Then
                    'goomena.com
                    If (str Is Nothing) Then str = gDomainCOM_DefaultLAG
                End If

            End If
        End If

        If (str Is Nothing) Then str = gDomainCOM_DefaultLAG

        Return str
    End Function


    Public Shared Function GetGeoCountry() As String
        Return HttpContext.Current.Session("GEO_COUNTRY_CODE")
    End Function




    Public Function GetHasCommunication(OtherProfileID As Integer) As Boolean
        Dim _HasCommunication As Boolean = False
        If (OtherProfileID = 1 OrElse Me.IsFemale) Then
            _HasCommunication = True
        Else
            _HasCommunication = clsUserDoes.HasCommunication(OtherProfileID, Me.MasterProfileId)
        End If
        Return _HasCommunication
    End Function


    Public Shared Function ReplaceCommonTookens(Input As String, Optional LoginName As String = "")
        If (Not String.IsNullOrEmpty(Input)) Then

            If (Input.IndexOf("###LOGINNAME###") > -1) Then Input = Input.Replace("###LOGINNAME###", LoginName)
            If (Input.IndexOf("###UNLOCK_MESSAGE_SEND_CRD###") > -1) Then Input = Input.Replace("###UNLOCK_MESSAGE_SEND_CRD###", ProfileHelper.Config_UNLOCK_MESSAGE_SEND_CREDITS)
            If (Input.IndexOf("###UNLOCK_MESSAGE_READ_CRD###") > -1) Then Input = Input.Replace("###UNLOCK_MESSAGE_READ_CRD###", ProfileHelper.Config_UNLOCK_MESSAGE_READ_CREDITS)
            If (Input.IndexOf("###UNLOCK_CONVERSATIONCRD###") > -1) Then Input = Input.Replace("###UNLOCK_CONVERSATIONCRD###", ProfileHelper.Config_UNLOCK_CONVERSATION_CREDITS)

        End If
        Return Input
    End Function



    Public Shared Function OnMoreInfoClickFunc(input As String, contentUrl As String, headerText As String) As String
        input = WhatIsIt.OnMoreInfoClickFunc(input, contentUrl, headerText)
        Return input
    End Function


    Protected Sub Page_PreInit()

        Try

            If clsCurrentContext.VerifyLogin() = True Then
                Me.MasterPageFile = "~/Members/Members2.Master"
                'If (Request("master") = "inner") Then
                '    Me.MasterPageFile = "~/Members/Members2Inner.Master"
                'Else
                'End If
            Else
                clsCurrentContext.ClearSession(Session)
                If (Not Me.IsCallback) Then
                    Response.Redirect(ResolveUrl("~/Login.aspx"), False)
                End If
            End If

        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub



    Private _CustomerAvailableCredits As clsCustomerAvailableCredits
    Protected Function HasRequiredCredits(type As UnlockType, Optional forceRead As Boolean = False) As Boolean
        Dim success = False

        If (_CustomerAvailableCredits Is Nothing) Then
            _CustomerAvailableCredits = BasePage.GetCustomerAvailableCredits(Me.MasterProfileId, forceRead)
        End If

        Try
            Dim creditsRequired As Integer = 10000000

            If (type = UnlockType.UNLOCK_CONVERSATION) Then
                creditsRequired = ProfileHelper.Config_UNLOCK_CONVERSATION_CREDITS

            ElseIf (type = UnlockType.UNLOCK_MESSAGE_READ) Then
                creditsRequired = ProfileHelper.Config_UNLOCK_MESSAGE_READ_CREDITS

            ElseIf (type = UnlockType.UNLOCK_MESSAGE_SEND) Then
                creditsRequired = ProfileHelper.Config_UNLOCK_MESSAGE_SEND_CREDITS
            End If

            If (_CustomerAvailableCredits.AvailableCredits >= creditsRequired) Then success = True
        Catch ex As Exception

        End Try


        Return success
    End Function


    Public Shared Function GetCustomerAvailableCredits(MasterProfileId As Integer, Optional forceRead As Boolean = False) As clsCustomerAvailableCredits
        Dim _CustomerAvailableCredits As clsCustomerAvailableCredits = Nothing
        Dim Session As System.Web.SessionState.HttpSessionState = HttpContext.Current.Session

        If (forceRead) Then Session("CustomerAvailableCredits") = Nothing

        If (Session("CustomerAvailableCredits") Is Nothing) Then
            _CustomerAvailableCredits = clsUserDoes.GetCustomerAvailableCredits(MasterProfileId)
            Session("CustomerAvailableCredits") = _CustomerAvailableCredits

            If (_CustomerAvailableCredits.CreditsRecordsCount > 0 AndAlso
                (Session("CustomerAvailableCredits_PREV") Is Nothing OrElse Session("CustomerAvailableCredits_PREV") <> _CustomerAvailableCredits.AvailableCredits)
            ) Then

                DataHelpers.Update_EUS_Profiles_UpdateAvailableCredits(MasterProfileId, _CustomerAvailableCredits.AvailableCredits)
                Session("CustomerAvailableCredits_PREV") = _CustomerAvailableCredits.AvailableCredits
            End If
        Else
            _CustomerAvailableCredits = Session("CustomerAvailableCredits")
        End If

        Return _CustomerAvailableCredits
    End Function

    Public Shared Function GetMemberActionsCounters(MasterProfileId As Integer) As clsGetMemberActionsCounters
        Dim _GetMemberActionsCounters As clsGetMemberActionsCounters = Nothing

        If (HttpContext.Current.Session("GetMemberActionsCounters") Is Nothing) Then
            _GetMemberActionsCounters = clsUserDoes.GetMemberActionsCounters(MasterProfileId)
            HttpContext.Current.Session("GetMemberActionsCounters") = _GetMemberActionsCounters
        Else
            _GetMemberActionsCounters = HttpContext.Current.Session("GetMemberActionsCounters")
        End If

        Return _GetMemberActionsCounters
    End Function

    Protected Sub SetMasterPage(CurrentPageData As clsPageData)
        Try
            Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            If (Not String.IsNullOrEmpty(cPageBasic.MasterPage_FileName)) Then
                Me.MasterPageFile = cPageBasic.MasterPage_FileName
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_PreInit")
        End Try
    End Sub


    Public Function GetCurrency() As String
        Dim currency As String = Nothing
        If (Me.MasterProfileId > 1 AndAlso Me.GetCurrentProfile(True) IsNot Nothing) Then
            currency = clsPricing.GetCurrency(Me.GetCurrentProfile().Country)
        Else
            currency = clsPricing.GetCurrency(HttpContext.Current.Session("GEO_COUNTRY_CODE"))
        End If
        Return currency
    End Function


    Public Sub CreateCanonicalLink(canonicalHref As String)
        ' create canonical link
        'Render: <link rel="canonical" href="http://www.goomena.com/es/cmspage/227" />
        ' Dim canonicalHref As String = "/landing/" & CurrentPageData.SitePageID

        If (Not String.IsNullOrEmpty(canonicalHref)) Then
            canonicalHref = LanguageHelper.GetPublicURL(canonicalHref, GetLag())
            canonicalHref = If(canonicalHref.IndexOf("?"c) > -1, canonicalHref.Remove(canonicalHref.IndexOf("?"c)), canonicalHref)
            If (canonicalHref.IndexOf("https://", StringComparison.OrdinalIgnoreCase) = 0) Then
                canonicalHref = canonicalHref.Remove(0, "https://".Length)
                canonicalHref = "http://" & canonicalHref
            End If

            ' Define an HtmlLink control.
            Dim lnkCanonical = New HtmlLink()
            lnkCanonical.Href = canonicalHref
            lnkCanonical.Attributes.Add("rel", "canonical")
            ' Add the HtmlLink to the Head section of the page.
            Page.Header.Controls.Add(lnkCanonical)
        End If
    End Sub


    Public Sub FillPublicSearchParams(ByRef prms As clsSearchHelperParameters)

        Try
            Dim countryCodeTmp As String = Session("GEO_COUNTRY_CODE")
            Dim PostcodeTmp As String = Nothing
            Dim LAGIDTmp As String = Me.GetLag()
            Using dt As DataTable = clsGeoHelper.GetCountryMinPostcodeDataTable(countryCodeTmp, Nothing, Nothing, LAGIDTmp)


                If (dt IsNot Nothing) AndAlso (dt.Rows.Count > 0) Then
                    If (Not IsDBNull(dt.Rows(0)("MinPostcode"))) Then PostcodeTmp = dt.Rows(0)("MinPostcode")
                    If (Not IsDBNull(dt.Rows(0)("countryCode"))) Then countryCodeTmp = dt.Rows(0)("countryCode")

                    Using dt2 = clsGeoHelper.GetGEOByZip(countryCodeTmp, PostcodeTmp, LAGIDTmp)



                        prms.zipstr = PostcodeTmp
                        Dim latitudeIn As Double = dt2.Rows(0)("latitude")
                        prms.latitudeIn = latitudeIn
                        Dim longitudeIn As Double = dt2.Rows(0)("longitude")
                        prms.longitudeIn = longitudeIn
                    End Using
                End If
            End Using
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        prms.CurrentProfileId = Me.MasterProfileId
        prms.ReturnRecordsWithStatus = ProfileStatusEnum.Approved

        If (Request("show") = "1") Then
            prms.LookingFor_ToMeetMaleID = True
            prms.LookingFor_ToMeetFemaleID = False
        ElseIf (Request("show") = "2") Then
            prms.LookingFor_ToMeetMaleID = False
            prms.LookingFor_ToMeetFemaleID = True
        Else
            prms.LookingFor_ToMeetMaleID = Me.SessionVariables.MemberData.LookingFor_ToMeetMaleID
            prms.LookingFor_ToMeetFemaleID = Me.SessionVariables.MemberData.LookingFor_ToMeetFemaleID
        End If
        prms.NumberOfRecordsToReturn = 0

        If (Not String.IsNullOrEmpty(Me.SessionVariables.MemberData.Country)) Then
            prms.Country = Me.SessionVariables.MemberData.Country
        Else
            prms.Country = Session("GEO_COUNTRY_CODE")
        End If

    End Sub


    Protected Overrides Sub Finalize()
        '  If _CMSDBDataContext IsNot Nothing Then _CMSDBDataContext.Dispose()

        MyBase.Finalize()
    End Sub
End Class



#Region "Comments"

'Protected Sub Render1(writer As HtmlTextWriter)
'    'Protected Overrides Sub Render(writer As HtmlTextWriter)

'    Dim currentLag As String = GetLag()
'    Dim currentLagUrl As String = LanguageHelper.GetURLAvailableForLAG(currentLag)
'    Dim RequestUrl As System.Uri = Request.Url
'    Dim pageScope As PageScopeEnum = UrlUtils.GetPageScope(RequestUrl, Page.RouteData)
'    Dim currentHost As String = UrlUtils.GetCurrentHostURL(RequestUrl)

'    Dim processHtml As Boolean =
'        currentLag <> "US" AndAlso
'        Not String.IsNullOrEmpty(currentLagUrl) AndAlso
'        Not currentLagUrl.ToLower().Equals(clsLanguageHelper.GetURLForLAG("US").ToLower()) AndAlso
'        (pageScope = PageScopeEnum.MembersPage OrElse pageScope = PageScopeEnum.PublicPage)

'    If (False AndAlso processHtml) Then

'        ' setup a TextWriter to capture the markup
'        'Dim sb As New StringBuilder()
'        Dim tw As TextWriter = New StringWriter()
'        Dim htw As HtmlTextWriter = New HtmlTextWriter(tw)

'        ' render the markup into our surrogate TextWriter
'        MyBase.Render(htw)

'        ' get the captured markup as a string
'        Dim html As String = tw.ToString()
'        Dim RequestUrlAbsoluteUri As String = Request.Url.AbsoluteUri

'        If (pageScope = PageScopeEnum.MembersPage) Then

'            Dim matchStartIndex As Integer = 0
'            Dim ancMatch As Match = clsWebFormHelper.AnchorRegex.Match(html, matchStartIndex)

'            While ancMatch.Success
'                Try

'                    Dim hrefGroup As Group = ancMatch.Groups(1)
'                    Dim href As String = hrefGroup.Value
'                    matchStartIndex = hrefGroup.Index + hrefGroup.Length

'                    If (href.StartsWith("/")) Then

'                        Dim _uri As System.Uri = UrlUtils.GetFullURI(currentHost, href)
'                        Dim newHref As String = LanguageHelper.GetNewLAGUrl2(RequestUrl, currentLag, _uri.ToString())
'                        html = html.Remove(hrefGroup.Index, hrefGroup.Length)
'                        html = html.Insert(hrefGroup.Index, newHref)
'                        matchStartIndex = hrefGroup.Index + newHref.Length

'                    ElseIf (Not UrlUtils.IsPathInCurrentFolder(RequestUrl, href)) Then

'                        Dim _uri As System.Uri = UrlUtils.GetFullURI(currentHost, href)
'                        Dim scope As PageScopeEnum = UrlUtils.GetPageScope(_uri)
'                        If (scope = PageScopeEnum.PublicPage) Then
'                            Dim newHref As String = LanguageHelper.GetNewLAGUrl(RequestUrl, currentLag, _uri.ToString())
'                            html = html.Remove(hrefGroup.Index, hrefGroup.Length)
'                            html = html.Insert(hrefGroup.Index, newHref)
'                            matchStartIndex = hrefGroup.Index + newHref.Length
'                        End If

'                    End If

'                Catch
'                End Try
'                ancMatch = clsWebFormHelper.AnchorRegex.Match(html, matchStartIndex)
'            End While

'        ElseIf (pageScope = PageScopeEnum.PublicPage) Then

'            Dim matchStartIndex As Integer = 0
'            Dim ancMatch As Match = clsWebFormHelper.AnchorRegex.Match(html, matchStartIndex)

'            While ancMatch.Success

'                Try

'                    Dim hrefGroup As Group = ancMatch.Groups(1)
'                    Dim href As String = hrefGroup.Value
'                    matchStartIndex = hrefGroup.Index + hrefGroup.Length

'                    'If (Not UrlUtils.IsPathInCurrentFolder(RequestUrl, href)) Then
'                    If (href.StartsWith("/")) Then
'                        Dim _uri As System.Uri = UrlUtils.GetFullURI(currentHost, href)
'                        Dim newHref As String = LanguageHelper.GetNewLAGUrl2(RequestUrl, currentLag, _uri.ToString())
'                        html = html.Remove(hrefGroup.Index, hrefGroup.Length)
'                        html = html.Insert(hrefGroup.Index, newHref)
'                        matchStartIndex = hrefGroup.Index + newHref.Length
'                    End If
'                    'End If

'                Catch
'                End Try

'                ancMatch = clsWebFormHelper.AnchorRegex.Match(html, matchStartIndex)
'            End While

'        End If

'        ' render the markup into the output stream verbatim
'        writer.Write(html)

'    Else
'        MyBase.Render(writer)
'    End If

'End Sub



'Sub SendNotificationEmailToMember_ToUpdateProfileInfo(mirrorRow As DSMembers.EUS_ProfilesRow)
'    Try

'        Dim toEmail As String = ConfigurationManager.AppSettings("gToEmail")
'        Dim Content As String = globalStrings.GetCustomString("EmailSendToSupport_MemberNew", "US")

'        If (mirrorRow.IsAutoApproved) Then
'            Content = Content.Replace("###YESNO###", "YES")
'        Else
'            Content = Content.Replace("###YESNO###", "NO")
'        End If

'        Content = Content.Replace("###LOGINNAME###", mirrorRow.LoginName)
'        Content = Content.Replace("###EMAIL###", mirrorRow.eMail)
'        Content = Content.Replace("###GENDER###", ProfileHelper.GetGenderString(mirrorRow.GenderId, "US"))


'        Dim approveUrl As String = ConfigurationManager.AppSettings("gApproveProfileURL")
'        Dim rejectUrl As String
'        approveUrl = String.Format(approveUrl, mirrorRow.LoginName)
'        rejectUrl = approveUrl & "&reject=1"

'        Content = Content.Replace("###APPROVEPROFILEURL###", approveUrl)
'        Content = Content.Replace("###REJECTPROFILEURL###", rejectUrl)

'        clsMyMail.SendMail(toEmail, "New user REGISTRATION on GOOMENA.", Content, True)

'    Catch ex As Exception
'        WebErrorMessageBox(Me, ex, "")
'    End Try

'End Sub


#End Region



﻿Imports FilesProxy.DownloadSites.Web
Imports System.Net
Imports System.IO
Imports System.Text.RegularExpressions



''' <summary>
''' WebFormHelper v 1.6.3
''' </summary>
''' <remarks></remarks>
Public Class clsWebFormHelper


    'Shared Sub New()
    '    clsWebFormHelper.FixUriParser()
    'End Sub



    Public Class FormData

        ''' <summary>
        ''' Url of action attribute
        ''' </summary>
        ''' <value>string</value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property ActionURL As String

        ''' <summary>
        ''' List of fields inside form. Each item contains key and value, where key stands for name attribute and value stands for value attrinute
        ''' </summary>
        ''' <value>List of KeyValuePair</value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property Fields As New List(Of KeyValuePair(Of String, String))()


        ''' <summary>
        ''' Indicates if instance has any number of fields
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property HasFields As Boolean
            Get
                Return (Me.Fields.Count > 0)
            End Get
        End Property

    End Class


    Public Class ElementData

        Public Property SrcURL As String
        Public Property AltenativeText As String
        Public Property TitleText As String
        Public Property Id As String
        Public Property Name As String

        Public Sub LoadFromNode(ByVal node As String)
            Dim attributes As List(Of KeyValuePair(Of String, String)) = GetNodeAttributes(node)
            For Each kvp As KeyValuePair(Of String, String) In attributes
                Select Case kvp.Key.ToLower()
                    Case "src"
                        Me.SrcURL = kvp.Value
                    Case "alt"
                        Me.AltenativeText = kvp.Value
                    Case "title"
                        Me.TitleText = kvp.Value
                    Case "id"
                        Me.Id = kvp.Value
                    Case "name"
                        Me.Name = kvp.Value
                End Select
            Next
        End Sub

    End Class


    Public Shared ReadOnly Property JavaScriptCurrentTime As String
        Get
            Return Decimal.Round((DateTime.Now - New DateTime(1970, 1, 1)).TotalMilliseconds).ToString()
        End Get
    End Property



    Shared _AnchorRegex As Regex
    Public Shared ReadOnly Property AnchorRegex As Regex
        Get
            If (_AnchorRegex Is Nothing) Then
                _AnchorRegex = New Regex("<a.*?href=[""'](?<url>.*?)[""'].*?>(?<name>.*?)</a>", RegexOptions.IgnoreCase Or RegexOptions.Multiline Or RegexOptions.Singleline Or RegexOptions.Compiled)
            End If

            Return _AnchorRegex
        End Get
    End Property



    Shared _FormRegex As Regex
    Public Shared ReadOnly Property FormRegex As Regex
        Get
            If (_FormRegex Is Nothing) Then
                _FormRegex = New Regex("<form[^<]*?>(?<content>.*?)</form>", RegexOptions.IgnoreCase Or RegexOptions.Multiline Or RegexOptions.Singleline Or RegexOptions.Compiled)
            End If

            Return _FormRegex
        End Get
    End Property



    Shared _FormActionRegex As Regex
    Public Shared Function GetFormActionAttribute(ByVal value As String) As String
        If (_FormActionRegex Is Nothing) Then
            _FormActionRegex = New Regex("<form[^<]*?\s+action=[""'](?<url>.*?)[""'].*?>", RegexOptions.IgnoreCase Or RegexOptions.Multiline Or RegexOptions.Singleline Or RegexOptions.Compiled Or RegexOptions.ExplicitCapture)
        End If


        Dim action As String = _FormActionRegex.Match(value).Groups(1).Value
        Return action
    End Function



    Shared _IframeRegex As Regex
    Public Shared ReadOnly Property IframeRegex As Regex
        Get
            If (_IframeRegex Is Nothing) Then
                _IframeRegex = New Regex("<iframe.*?src=[""'](?<url>.*?)[""'].*?>", RegexOptions.IgnoreCase Or RegexOptions.Multiline Or RegexOptions.Singleline Or RegexOptions.Compiled)
            End If

            Return _IframeRegex
        End Get
    End Property


    Shared _ImageRegex As Regex
    Public Shared ReadOnly Property ImageRegex As Regex
        Get
            If (_ImageRegex Is Nothing) Then
                _ImageRegex = New Regex("<img.*?src=[""'](?<url>.*?)[""'].*?>", RegexOptions.IgnoreCase Or RegexOptions.Multiline Or RegexOptions.Singleline Or RegexOptions.Compiled)
            End If

            Return _ImageRegex
        End Get
    End Property


    Shared _ScriptRegex As Regex
    Public Shared ReadOnly Property ScriptRegex As Regex
        Get
            If (_ScriptRegex Is Nothing) Then
                _ScriptRegex = New Regex("<script.*?>(?<content>.*?)</script>", RegexOptions.IgnoreCase Or RegexOptions.Multiline Or RegexOptions.Singleline Or RegexOptions.Compiled)
            End If

            Return _ScriptRegex
        End Get
    End Property


    Shared _InputRegex As Regex
    Public Shared ReadOnly Property InputRegex As Regex
        Get
            If (_InputRegex Is Nothing) Then
                _InputRegex = New Regex("(<input[^>](.*?)>)", RegexOptions.IgnoreCase Or RegexOptions.Multiline Or RegexOptions.Singleline Or RegexOptions.Compiled)
            End If

            Return _InputRegex
        End Get
    End Property


    Shared _NodeAttributesRegex As Regex
    Public Shared ReadOnly Property NodeAttributesRegex As Regex
        Get
            If (_NodeAttributesRegex Is Nothing) Then
                Dim pattern As String = vbCrLf & _
"(?:<)(?<Tag>[^\s/>]+)       # Extract the tag name." & vbCrLf & _
"(?![/>])                    # Stop if /> is found" & vbCrLf & _
"                     # -- Extract Attributes Key Value Pairs  --" & vbCrLf & _
" " & vbCrLf & _
"((?:\s+)             # One to many spaces start the attribute" & vbCrLf & _
" (?<Key>[^=]+)       # Name/key of the attribute" & vbCrLf & _
" (?:=)               # Equals sign needs to be matched, but not captured." & vbCrLf & _
" " & vbCrLf & _
"(?([\x22\x27])              # If quotes are found" & vbCrLf & _
"  (?:[\x22\x27])" & vbCrLf & _
"  (?<Value>[^\x22\x27]+)    # Place the value into named Capture" & vbCrLf & _
"  (?:[\x22\x27])" & vbCrLf & _
" |                          # Else no quotes" & vbCrLf & _
"   (?<Value>[^\s/>]*)       # Place the value into named Capture" & vbCrLf & _
" )" & vbCrLf & _
")+                  # -- One to many attributes found!"


                _NodeAttributesRegex = New Regex(pattern, RegexOptions.IgnoreCase Or RegexOptions.Multiline Or RegexOptions.Singleline Or RegexOptions.Compiled Or RegexOptions.IgnorePatternWhitespace)
            End If

            Return _NodeAttributesRegex
        End Get
    End Property



    Public Shared Function GetDictionaryForJSON(ByVal json As String) As Dictionary(Of String, String)
        Dim _values As Dictionary(Of String, String)

        If Not String.IsNullOrEmpty(json) Then
            Dim serializer As New Script.Serialization.JavaScriptSerializer()
            _values = serializer.Deserialize(Of Dictionary(Of String, String))(json)
        Else
            ' no client-side dictionary was present, so create a new blank one
            _values = New Dictionary(Of String, String)()
        End If

        Return _values
    End Function



    ''' <summary>
    ''' Searches inside provided html string for available anchor tags and tries to find an anchor tag containing the string specified in second parameter.
    ''' </summary>
    ''' <param name="html">Html content</param>
    ''' <param name="stringInsideAnchor">A string that should be found inside an anchor tag</param>
    ''' <returns>HREF attribute of anchor tag, if second string is found in it. Otherwise returns Nothing</returns>
    ''' <remarks></remarks>
    Public Shared Function GetUrlFromAnchor(ByVal html As String, ByVal stringInsideAnchor As String) As String
        Dim _href As String = Nothing

        If (Not String.IsNullOrEmpty(html) AndAlso Not String.IsNullOrEmpty(stringInsideAnchor)) Then
            Dim ancMatch As Match = clsWebFormHelper.AnchorRegex.Match(html)

            While ancMatch.Success
                If ancMatch.Value.IndexOf(stringInsideAnchor, StringComparison.OrdinalIgnoreCase) > -1 Then
                    _href = ancMatch.Groups(1).Value
                    Exit While
                Else
                    ancMatch = ancMatch.NextMatch()
                End If
            End While
        End If

        Return _href
    End Function


    ''' <summary>
    ''' Searches inside provided html string for available anchor tags and tries to find an anchor tag containing the string specified in second parameter.
    ''' </summary>
    ''' <param name="html">Html content</param>
    ''' <param name="stringInsideAnchor">A string that should be found inside an anchor tag</param>
    ''' <returns>List of HREF attributes of anchor tags, if second string is found within them. Otherwise returns empty list.</returns>
    ''' <remarks></remarks>
    Public Shared Function GetUrlListFromAnchors(ByVal html As String, ByVal stringInsideAnchor As String) As List(Of String)
        Dim _href As New List(Of String)

        If (Not String.IsNullOrEmpty(html) AndAlso Not String.IsNullOrEmpty(stringInsideAnchor)) Then
            Dim ancMatch As Match = clsWebFormHelper.AnchorRegex.Match(html)

            While ancMatch.Success

                If ancMatch.Value.IndexOf(stringInsideAnchor, StringComparison.OrdinalIgnoreCase) > -1 Then
                    _href.Add(ancMatch.Groups(1).Value)
                End If

                ancMatch = ancMatch.NextMatch()
            End While
        End If

        Return _href
    End Function


    ''' <summary>
    ''' Searches inside provided html string for available anchor tags and tries to find an anchor tag containing the string specified in second parameter.
    ''' </summary>
    ''' <param name="html">Html content</param>
    ''' <returns>List of HREF attributes of anchor tags, if second string is found within them. Otherwise returns empty list.</returns>
    ''' <remarks></remarks>
    Public Shared Function GetUrlListFromAnchors(ByVal html As String) As List(Of String)
        Dim _href As New List(Of String)

        If (Not String.IsNullOrEmpty(html)) Then
            Dim ancMatch As Match = clsWebFormHelper.AnchorRegex.Match(html)

            While ancMatch.Success
                _href.Add(ancMatch.Groups(1).Value)
                ancMatch = ancMatch.NextMatch()
            End While
        End If

        Return _href
    End Function



    ''' <summary>
    ''' Searches inside provided html string for available form tags and tries to find an form tag containing the string specified in second parameter.
    ''' </summary>
    ''' <param name="html">Html content</param>
    ''' <param name="stringInsideForm">A string that should be found inside a form tag</param>
    ''' <returns>Aa FormData object, containing data from found form</returns>
    ''' <remarks></remarks>
    Public Shared Function GetFormFields(ByVal html As String, ByVal stringInsideForm As String) As FormData

        Dim form As New FormData()
        Dim matches As MatchCollection = clsWebFormHelper.FormRegex.Matches(html)
        For Each m As Match In matches
            'For Each m As Match In clsWebFormHelper.FormRegex.Matches(html)

            If m.Value.IndexOf(stringInsideForm, StringComparison.OrdinalIgnoreCase) > -1 Then

                form.ActionURL = clsWebFormHelper.GetFormActionAttribute(m.Value)

                ' content
                Dim formTag As String = m.Value

                For Each inputMatch As Match In clsWebFormHelper.InputRegex.Matches(formTag)
                    Dim attributes As List(Of KeyValuePair(Of String, String)) = GetNodeAttributes(inputMatch.Value)
                    Dim key As String = "", value As String = ""

                    For Each kvp As KeyValuePair(Of String, String) In attributes

                        If kvp.Key = "name" Then
                            key = kvp.Value
                        ElseIf kvp.Key = "value" Then
                            value = kvp.Value
                        End If

                    Next

                    Dim pair As New KeyValuePair(Of String, String)(key, value)
                    form.Fields.Add(pair)
                Next

                Exit For
            End If
        Next

        Return form
    End Function


    Public Shared Function GetSourceFromIframe(ByVal html As String, ByVal stringInsideIframe As String) As String
        Dim _href As String = Nothing

        If (Not String.IsNullOrEmpty(html) AndAlso Not String.IsNullOrEmpty(stringInsideIframe)) Then
            Dim ifrMatch As Match = clsWebFormHelper.IframeRegex.Match(html)

            While ifrMatch.Success
                If ifrMatch.Value.IndexOf(stringInsideIframe, StringComparison.OrdinalIgnoreCase) > -1 Then
                    _href = ifrMatch.Groups(1).Value
                    Exit While
                Else
                    ifrMatch = ifrMatch.NextMatch()
                End If
            End While
        End If

        Return _href
    End Function


    Public Shared Function RemoveAllScriptTags(ByVal html As String) As String
        Dim _newHtml As String = html

        If (Not String.IsNullOrEmpty(html)) Then
            _newHtml = clsWebFormHelper.ScriptRegex.Replace(html, String.Empty)
        End If

        Return _newHtml
    End Function


    Public Shared Function GetImages(ByVal html As String, ByVal indexStart As Integer, Optional ByVal indexEnd As Integer = -1) As List(Of ElementData)

        If (indexEnd = -1) Then
            indexEnd = html.Length - 1
        End If

        Dim images As New List(Of ElementData)
        For Each m As Match In clsWebFormHelper.ImageRegex.Matches(html, indexStart)
            If (m.Index + m.Length <= indexEnd) Then
                Dim img As New ElementData()
                img.LoadFromNode(m.Value)
                images.Add(img)
            Else
                Exit For
            End If
        Next

        Return images
    End Function


    ''' <summary>
    ''' Reads a provided html node and extracts it's attributes using regular expression.
    ''' Ex. for tag &lt;input type="hidden" name="act" value="logout" /&gt;
    ''' Returns a list with type-hidden, name-act, value-logout
    ''' </summary>
    ''' <param name="node">Html node string</param>
    ''' <returns>List of KeyValuePair objects, each containing a pair of name-value for each attribute</returns>
    ''' <remarks></remarks>
    Public Shared Function GetNodeAttributes(ByVal node As String) As List(Of KeyValuePair(Of String, String))
        Dim attributes = (From mt In clsWebFormHelper.NodeAttributesRegex.Matches(node) _
                          Select New With {
                              Key .Name = mt.Groups("Tag"), _
                              Key .Attrs = (From cpKey In CType(mt, Match).Groups("Key").Captures.Cast(Of Capture)().Select(Function(a, i) New With {a.Value, i})
                                            Join cpValue In CType(mt, Match).Groups("Value").Captures.Cast(Of Capture)().Select(Function(b, i) New With {b.Value, i}) On cpKey.i Equals cpValue.i
                                            Select New KeyValuePair(Of String, String)(cpKey.Value, cpValue.Value)).ToList() _
                          }).First().Attrs

        Return attributes
    End Function



    ' calculate the MD5 hash of a given string 
    ' the string is first converted to a byte array
    Public Shared Function GetMD5Hash(ByVal strData As String) As String

        Using objMD5 As New System.Security.Cryptography.MD5CryptoServiceProvider


            Dim arrData() As Byte
            Dim arrHash() As Byte

            ' first convert the string to bytes (using UTF8 encoding for unicode characters)
            arrData = System.Text.Encoding.UTF8.GetBytes(strData)

            ' hash contents of this byte array
            arrHash = objMD5.ComputeHash(arrData)


            ' return formatted hash
            Return ByteArrayToString(arrHash)
        End Using
    End Function


    ' utility function to convert a byte array into a hex string
    Private Shared Function ByteArrayToString(ByVal arrInput() As Byte) As String
        Dim strOutput As New System.Text.StringBuilder(arrInput.Length)
        For i As Integer = 0 To arrInput.Length - 1
            strOutput.Append(arrInput(i).ToString("X2"))
        Next
        Return strOutput.ToString().ToLower
    End Function




    Public Shared Function ReadStream(ByRef _stream As Stream) As Byte()

        Dim buffer As Byte() = New Byte(16 * 1024) {}
        Using ms As MemoryStream = New MemoryStream()
            Dim read As Int32
            read = _stream.Read(buffer, 0, buffer.Length)
            ' the content of stream is more than buffer's length
            If read = buffer.Length Then
                ms.Write(buffer, 0, read)

                While (read = buffer.Length)
                    read = _stream.Read(buffer, 0, buffer.Length)
                    ms.Write(buffer, 0, read)
                End While

                ms.Dispose()
                Return ms.ToArray()
            Else
                Return buffer
            End If
        End Using
    End Function


    Public Shared Sub FixUriParser()
        Dim getSyntax As System.Reflection.MethodInfo = GetType(UriParser).GetMethod("GetSyntax", System.Reflection.BindingFlags.[Static] Or System.Reflection.BindingFlags.NonPublic)
        Dim flagsField As System.Reflection.FieldInfo = GetType(UriParser).GetField("m_Flags", System.Reflection.BindingFlags.Instance Or System.Reflection.BindingFlags.NonPublic)
        If getSyntax IsNot Nothing AndAlso flagsField IsNot Nothing Then
            For Each scheme As String In New String() {"http", "https"}
                Dim parser As UriParser = DirectCast(getSyntax.Invoke(Nothing, New Object() {scheme}), UriParser)
                If parser IsNot Nothing Then
                    Dim flagsValue As Integer = CInt(flagsField.GetValue(parser))
                    ' Clear the CanonicalizeAsFilePath attribute
                    If (flagsValue And &H1000000) <> 0 Then
                        flagsField.SetValue(parser, flagsValue And Not &H1000000)
                    End If
                End If
            Next
        End If
    End Sub



    ' not tested
    'Public Shared Function base64Encode(ByVal data As String) As String
    '    Try
    '        Dim encData_byte As Byte() = New Byte(data.Length - 1) {}
    '        encData_byte = System.Text.Encoding.UTF8.GetBytes(data)
    '        Dim encodedData As String = Convert.ToBase64String(encData_byte)
    '        Return encodedData
    '    Catch e As Exception
    '        Throw New Exception("Error in base64Encode" & e.Message)
    '    End Try
    'End Function


    Public Shared Function base64Decode(ByVal data As String) As String
        Try
            Dim encoder As New System.Text.UTF8Encoding()
            Dim utf8Decode As System.Text.Decoder = encoder.GetDecoder()

            Dim todecode_byte As Byte() = Convert.FromBase64String(data)
            Dim charCount As Integer = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length)
            Dim decoded_char As Char() = New Char(charCount - 1) {}
            utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0)
            Dim result As String = New String(decoded_char)
            Return result
        Catch e As Exception
            Throw New Exception("Error in base64Decode" & e.Message)
        End Try
    End Function


    Public Shared Function GetSubstring(ByVal input As String, ByVal stringBefore As String, Optional ByVal stringAfter As String = "") As String
        Dim result As String = String.Empty

        If (Not String.IsNullOrEmpty(stringBefore)) Then
            Dim ndxStart As Integer = input.IndexOf(stringBefore) + stringBefore.Length

            If (Not String.IsNullOrEmpty(stringAfter)) Then
                Dim ndxEnd As Integer = input.IndexOf(stringAfter, ndxStart)
                result = input.Substring(ndxStart, ndxEnd - ndxStart)
            Else
                result = input.Substring(ndxStart)
            End If
        End If

        Return result
    End Function



    ''' <summary>
    ''' Strip HTML tags.
    ''' </summary>
    Public Shared Function StripTags(ByVal html As String) As String
        ' Remove HTML tags.
        If (Not String.IsNullOrEmpty(html)) Then
            Dim str As String = Regex.Replace(html, "&nbsp;", "")
            str = Regex.Replace(str, "<.*?>", "")
            Return str
        End If
        Return html
    End Function


End Class


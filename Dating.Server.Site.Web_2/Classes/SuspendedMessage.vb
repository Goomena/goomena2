﻿Public Class SuspendedMessage

    Public Shared Sub AddMessageID(messageID As Long)
        Dim Session As HttpSessionState = HttpContext.Current.Session
        Dim SuspendedMessageIDList As List(Of Long) = Nothing
        If (Session("SuspendedMessageIDList") Is Nothing) Then
            SuspendedMessageIDList = New List(Of Long)()
        Else
            SuspendedMessageIDList = Session("SuspendedMessageIDList")
        End If
        SuspendedMessageIDList.Add(messageID)
        Session("SuspendedMessageIDList") = SuspendedMessageIDList
    End Sub


    Public Shared Function IsSuspended(messageID As Long) As Boolean
        Dim isSuspended1 As Boolean = False
        Dim Session As HttpSessionState = HttpContext.Current.Session
        If (Session("SuspendedMessageIDList") IsNot Nothing) Then
            Dim SuspendedMessageIDList As List(Of Long) = Nothing
            SuspendedMessageIDList = Session("SuspendedMessageIDList")
            isSuspended1 = SuspendedMessageIDList.Contains(messageID)
        End If
        Return isSuspended1
    End Function

    Public Shared Sub RemoveMessageID(messageID As Long)
        '  Dim isSuspended As Boolean = False
        Dim Session As HttpSessionState = HttpContext.Current.Session
        If (Session("SuspendedMessageIDList") IsNot Nothing) Then
            Dim SuspendedMessageIDList As List(Of Long) = Nothing
            SuspendedMessageIDList = Session("SuspendedMessageIDList")
            If (SuspendedMessageIDList.Contains(messageID)) Then
                SuspendedMessageIDList.Remove(messageID)
            End If
        End If
    End Sub


End Class

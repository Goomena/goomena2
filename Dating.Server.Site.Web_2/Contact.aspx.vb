﻿Imports Dating.Server.Core.DLL

Public Class Contact
    Inherits BasePage



    Protected Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            'If (_pageData Is Nothing) Then _pageData = New clsPageData("Contact.aspx", Context)
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData("Contact.aspx", Context, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property
    Private Sub Page_Disposed1(sender As Object, e As EventArgs) Handles Me.Disposed
        Try
            If Me._pageData IsNot Nothing Then
                RemoveHandler Me._pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
        Catch ex As Exception
        End Try
        Try
            MyBase.Dispose()
        Catch ex As Exception
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try


            Try
                Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
                AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "Page_Load")
            End Try


            If (Not Me.IsPostBack) Then
                LoadLAG()
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try
    End Sub



    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
    End Sub


    Protected Sub LoadLAG()
        Try
            '   Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = Me.CurrentPageData.cPageBasic
            Dim c As clsCountryByIP = Nothing
            Try
                c = clsCurrentContext.GetCountryByIP()

            Catch ex As Exception

            End Try
            If c IsNot Nothing AndAlso c.CountryCode.ToLower <> "gr" AndAlso c.CountryCode.ToLower <> "cy" Then
                lbContactTopDescription.Text = Me.CurrentPageData.GetCustomString("lbContactTopDescription")
            Else
                lbContactTopDescription.Text = Me.CurrentPageData.GetCustomString("lbContactTopDescriptionGRCY")
            End If

            lbHeader.Text = Me.CurrentPageData.GetCustomString("lbHeader")
            lbHeaderZilla.Text = Me.CurrentPageData.GetCustomString("lbHeaderZilla")
            lblTextlivezilla.Text = Me.CurrentPageData.GetCustomString("lblTextlivezilla")
            lbContactFormInfo.Text = Me.CurrentPageData.GetCustomString("lbContactFormInfo")

            If (TypeOf Page.Master Is INavigation) Then
                CType(Page.Master, INavigation).AddNaviLink(Me.CurrentPageData.Title, "")
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender

        Try
            Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

        If (Session("LiveZillaClosed") <> "1") Then

            Try
                Dim url As String = "http://chat.goomena.com/onoffline.php".ToLower()
                Dim rp As String = clsWebPageProcessor.GetPageContent(url, 15000)
                If (rp.ToLower().Contains("offline")) Then
                    milisemas.Visible = False
                Else
                    milisemas.Visible = True
                End If
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "chat.goomena.com -- Page_Load")
                Session("LiveZillaClosed") = "1"
                milisemas.Visible = False
            End Try

        End If

    End Sub

End Class
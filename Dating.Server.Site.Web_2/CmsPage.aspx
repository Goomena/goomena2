﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Root.Master" CodeBehind="CmsPage.aspx.vb" 
    Inherits="Dating.Server.Site.Web.CmsPage" %>
<%@ Register src="~/UserControls/ContactControl.ascx" tagname="ContactControl" tagprefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<style type="text/css">
    /*p{margin:0; padding:0; }*/
    body {font-family: "Segoe UI","Arial";font-weight:lighter;}
    
    .cc-wrapper {width: 900px;margin: 0 auto 0;font-size: 14px; }
    .cc-wrapper .inner-box{ width: 530px; display:block;margin: 10px auto 0;font-size: 14px;position:relative; }
    .cc-wrapper .inner-box .p276{ width: 530px; position:absolute;top:-200px;left: -200px; }
    .cc-wrapper div.form-actions {margin-top:20px;position:relative;}
    .cc-wrapper div.form-actions table{margin:0 auto 0;}
    .cc-wrapper div.form-actions input.linkbutton{position:absolute;top: -15px;right: -1px;}
    .cc-wrapper ul li { list-style: none; clear: both; display: block; height: 30px; margin-bottom: 10px; }
    .cc-wrapper ul li:after { clear: both; }
    .cc-wrapper ul li div.form_right { float: right; }
    #pc276{margin-bottom:300px;}
    .cc-wrapper   div.form-actions .cmdSend{cursor:pointer;}

    .pe_box a,
    .pe_box a:visited{color:#000;text-decoration:none;}
    .pe_box a:hover{color:#00aeef;}
    .pe_box .losetime a:hover{color:#fff;}
    
    .line{ border-bottom: 1px solid #c7c7c7;margin:10px 0;}
    
    a.ginemelosnowman,a.ginemelosnowman:visited,
    .koubi a,.koubi a:visited,
    .givemoneymid a,.givemoneymid a:visited,
    .getmoneymid a,.getmoneymid a:visited,
    .gineaffiliate a,.gineaffiliate a:visited{color:#000;text-decoration:none;}
    a.ginemelosnowman:hover,
    .koubi a:hover,
    .givemoneymid a:hover,
    .getmoneymid a:hover,
    .gineaffiliate a:hover{color: #fff;}
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="insidemosaiccontainer" runat="server">
    <asp:Label ID="lblinside" runat="server" Text=""></asp:Label>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="content" runat="server">
    <div class="mid_bg pageContainer">
        <div class="hp_cr" id="hp_top">
        </div>
        <div id="hp_content">
            <div class="container_12">
                <div class="cms_page">
                    <div class="cms_content">
                        <div class="ab_block">
                            <div class="pe_wrapper">
                                <div class="pe_box" id="<%= MyBase.PageContentID %>">
                                    <h2 style="text-shadow: none; border: none; font-family: Lucida Grande,Lucida Sans Unicode,Verdana,Arial,sans-serif;">
                                        <asp:Label ID="lblHeader" runat="server" EnableViewState="False" /><span class="loginHereLink"></span></h2>
                                    <span style="font-size:12px;">
                                        <asp:Literal ID="lbHTMLBody" runat="server" EnableViewState="False">
                                        </asp:Literal>
                                    </span>
                                    <asp:PlaceHolder ID="phContact" runat="server"></asp:PlaceHolder>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="pageContainer" style="margin-top: 20px; display: none;">
        <div></div>
    </div>
<%--
        <dx:ASPxRoundPanel ID="pnl" runat="server" ClientIDMode="AutoID" CssFilePath="~/App_Themes/Office2010Silver/{0}/styles.css"
            CssPostfix="Office2010Silver" EnableDefaultAppearance="False" GroupBoxCaptionOffsetX="6px"
            GroupBoxCaptionOffsetY="-19px" HeaderText="" SpriteCssFilePath="~/App_Themes/Office2010Silver/{0}/sprite.css" Width="100%">
            <ContentPaddings PaddingBottom="10px" PaddingLeft="9px" PaddingRight="11px" PaddingTop="10px" />
            < %--<HeaderStyle Font-Bold="True" Font-Size="Medium">
                <Paddings PaddingBottom="6px" PaddingLeft="9px" PaddingRight="11px" PaddingTop="3px" />
            </HeaderStyle>--% >
            <PanelCollection>
                <dx:PanelContent runat="server" SupportsDisabledAttribute="True">
                    <dxe:ASPxLabel ID="lbHTMLBody" runat="server" ClientIDMode="AutoID" EncodeHtml="False">
                    </dxe:ASPxLabel>
                </dx:PanelContent>
            </PanelCollection>
        </dx:ASPxRoundPanel>
--%>
 <dx:ASPxPopupControl SkinID="None" EncodeHtml="False" ID="popupWindows" runat="server"
        EnableViewState="False" EnableHotTrack="False" PopupHorizontalAlign="RightSides"
        PopupVerticalAlign="Above" PopupHorizontalOffset="1" PopupVerticalOffset="-4"
        EnableHierarchyRecreation="True" CloseAction="MouseOut" HeaderText="" 
        PopupAction="MouseOver" RenderMode="Lightweight">
        <ClientSideEvents />
        <Windows>
        </Windows>
    </dx:ASPxPopupControl>
</asp:Content>

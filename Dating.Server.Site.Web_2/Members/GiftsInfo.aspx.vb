﻿Imports Dating.Server.Core.DLL

Public Class GiftsInfo
    Inherits BasePage

    Protected Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            ' If (_pageData Is Nothing) Then _pageData = New clsPageData("~/Members/control.ProfileEdit", Context)
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData(Context, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property

    Private Sub Page_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        Try
            If Me._pageData IsNot Nothing Then
                RemoveHandler Me._pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If

        Catch ex As Exception
        End Try
        Try
            MyBase.Dispose()
        Catch ex As Exception

        End Try
    End Sub

    Protected Overrides Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        SetMasterPage(CurrentPageData)

        Try
            If clsCurrentContext.VerifyLogin() = True Then
                ''''  Me.MasterPageFile = "~/Members/Members2.Master"
            Else
                clsCurrentContext.ClearSession(Session)
                If (Not Me.IsCallback) Then
                    Response.Redirect(ResolveUrl("~/Login.aspx"), False)
                End If
                HttpContext.Current.ApplicationInstance.CompleteRequest()
            End If
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try


    End Sub


    Private Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        Try

            If (clsCurrentContext.VerifyLogin() = False) Then
                FormsAuthentication.SignOut()
                Response.Redirect(Page.ResolveUrl("~/Login.aspx"))
            End If


            'Try
            '    Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            '    AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
            'Catch ex As Exception
            '    WebErrorMessageBox(Me, ex, "Page_Load")
            'End Try


            If (Not Me.IsPostBack) Then
                Dim rpp As Boolean = ProfileHelper.GetRewardProgramParticipate(Me.MasterProfileId)
                If (rpp) Then
                    pnlBottom.Visible = False
                    '  Return
                End If

                LoadLAG()
            End If

        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try
    End Sub



    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender

        Try
            Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub



    Protected Sub LoadLAG()
        Try
            If (CurrentPageData.LagID <> GetLag()) Then
                Me._pageData = Nothing
            End If

            '      Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            lblHeader.Text = CurrentPageData.GetCustomString("lblHeader")
            lblContent.Text = CurrentPageData.GetCustomString("lblContent")
            lnkEnter.Text = CurrentPageData.GetCustomString("lnkEnter")
            chkAgree.Text = CurrentPageData.VerifyCustomString("chkAgree").Replace("[TOS-LINK]", "javascript:void(1);"" onclick=""openPopupTOS();return false;")
            lnkGetOut.Text = CurrentPageData.GetCustomString("lnkGetOut")
            lblMustCheckAgree.Text = CurrentPageData.GetCustomString("lblMustCheckAgree")
            Dim ctl2 As ucPopupOnlyTerms = Me.LoadControl("~/UserControls/ucPopupOnlyTerms.ascx")
            Me.placeCtl.Controls.Add(ctl2)

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Protected Sub lnkEnter_Click(sender As Object, e As EventArgs) Handles lnkEnter.Click
        Try

            ProfileHelper.SetRewardProgramParticipate(Me.MasterProfileId, True)
            '' ProfileHelper.ApproveUserNewTOS(Me.MasterProfileId, Me.MirrorProfileId, True, 
            Response.Redirect("Coupon.aspx")
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Protected Sub lnkGetOut_Click(sender As Object, e As EventArgs) Handles lnkGetOut.Click
        Try

            ProfileHelper.SetRewardProgramParticipate(Me.MasterProfileId, False)
            Response.Redirect("~/Members/")
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub



End Class
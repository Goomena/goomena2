﻿Imports System.Drawing
Imports DevExpress.Web.ASPxEditors
Imports Dating.Server.Core.DLL
Imports Dating.Server.Core.DLL.clsUserDoes
Imports Dating.Server.Datasets.DLL

Public Class ConversationUnlockMessage
    Inherits MessagesBasePage

#Region "Props"

    Public Property ScrollToElem As String
    Public Property CurrentLoginName As String
    Public Property WarningPopup_HeaderText As String


    Protected Property OfferId As Integer
        Get
            Return ViewState("OfferId")
        End Get
        Set(value As Integer)
            ViewState("OfferId") = value
        End Set
    End Property


    Protected Property UserIdInView As Integer
        Get
            Return ViewState("UserIdInView")
        End Get
        Set(value As Integer)
            ViewState("UserIdInView") = value
        End Set
    End Property


    Protected Property SubjectInView As String
        Get
            Return ViewState("SubjectInView")
        End Get
        Set(value As String)
            ViewState("SubjectInView") = value
        End Set
    End Property


    Protected Property MessageIdInView As Long
        Get
            Return ViewState("MessageIdInView")
        End Get
        Set(value As Long)
            ViewState("MessageIdInView") = value
        End Set
    End Property

    ' used with delete message action
    Protected Property MessageIdListInView As List(Of Long)
        Get
            Return ViewState("MessageIdListInView")
        End Get
        Set(value As List(Of Long))
            ViewState("MessageIdListInView") = value
        End Set
    End Property


    Protected Property BackUrl As String
        Get
            Return ViewState("BackUrl")
        End Get
        Set(value As String)
            ViewState("BackUrl") = value
        End Set
    End Property


    Protected Property SendAnotherMessage As Boolean
        Get
            Return ViewState("SendAnotherMessage")
        End Get
        Set(value As Boolean)
            ViewState("SendAnotherMessage") = value
        End Set
    End Property


    ''' <summary>
    ''' Recipient id.
    ''' Used when current user opens it's sent message and wants to send another one.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected Property SendAnotherMessageToRecipient As Integer
        Get
            Return ViewState("SendAnotherMessageToRecipient")
        End Get
        Set(value As Integer)
            ViewState("SendAnotherMessageToRecipient") = value
        End Set
    End Property

    Protected Property StopExecution As Boolean

#End Region


    Protected Overrides Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            If (Me.SessionVariables.MemberData.Status = ProfileStatusEnum.LIMITED) Then
                Response.Write("{""redirect"":""" & ResolveUrl("~/Members/Default.aspx") & """}")
                HttpContext.Current.ApplicationInstance.CompleteRequest()
                StopExecution = True
            End If

        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        Try

            '    Dim viewLoaded As Boolean = False

            Dim unlockMessage As Boolean = MyBase.UnlimitedMsgID > 0 AndAlso Not String.IsNullOrEmpty(Request.QueryString("vw"))
            '   Dim viewMessage As Boolean = Not MyBase.UnlimitedMsgID > 0 AndAlso (Not String.IsNullOrEmpty(Request.QueryString("vw")) OrElse Not String.IsNullOrEmpty(Request.QueryString("msg")))
            '  Dim sendMessageTo As Boolean = Not String.IsNullOrEmpty(Request.QueryString("p"))
            '      Dim tabChanged As Boolean = Not String.IsNullOrEmpty(Request.QueryString("t"))


            If (Not Page.IsPostBack AndAlso unlockMessage AndAlso Not StopExecution) Then
                PerformMessageUnlock()
            End If



        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub
    'Private Function GetMembersFreeMessagesAvailableToREAD(fromProfileID As Integer) As Integer
    '    Dim myFreeMessages As Integer = 0
    '    Dim fromProfileIDFreeMessages As Integer = 0
    '    If (ConfigurationManager.AppSettings("AllowFreeMessagesToRead") = "1") Then
    '        If (Not String.IsNullOrEmpty(Request.QueryString("free")) AndAlso Request.QueryString("free") = "1") Then
    '            If (freeMessagesHelper.CreditsPackagesBonusEnabled) Then

    '                Dim cls As clsFreeMessagesHelper = clsFreeMessagesHelper.GetData(Me.MasterProfileId, fromProfileID, Me.SessionVariables.MemberData.Country)
    '                If (Not cls.IsProfilesReadCountLimit OrElse cls.HasProfileConversation) Then
    '                    myFreeMessages = cls.Credits_FreeMessagesReadFromProfile - cls.MessagesReadFromProfile
    '                End If

    '            End If

    '        End If
    '    End If
    '    Return myFreeMessages
    'End Function
    Private __clsFreeMessagesHelper As CustomerFreeMessagesAvailableResult
    Private Function GetMembersFreeMessagesAvailableToREAD(manProfileId As Integer?, womanProfileId As Integer?, manCountry As String) As Integer
        Dim freeMessagesCount As Integer = 0
        If (__clsFreeMessagesHelper Is Nothing) Then
            If (ConfigurationManager.AppSettings("AllowFreeMessagesToRead") = "1" AndAlso
                freeMessagesHelper.CreditsPackagesBonusEnabled) Then
                If (Not String.IsNullOrEmpty(Request.QueryString("free")) AndAlso Request.QueryString("free") = "1") Then
                    Using cmdb As CMSDBDataContext = Me.GetCMSDBDataContext
                        __clsFreeMessagesHelper = cmdb.CustomerFreeMessagesAvailable(manProfileId, womanProfileId, manCountry).FirstOrDefault()
                    End Using

                    ' __clsFreeMessagesHelper = clsFreeMessagesHelper.GetData(Me.MasterProfileId, fromProfileID, Me.SessionVariables.MemberData.Country)
                End If
            End If
        End If
        If (__clsFreeMessagesHelper IsNot Nothing) Then
            If (Not __clsFreeMessagesHelper.IsProfilesReadCountLimit OrElse __clsFreeMessagesHelper.HasProfileConversation) Then
                freeMessagesCount = __clsFreeMessagesHelper.Credits_FreeMessagesReadFromProfile - __clsFreeMessagesHelper.MessagesReadFromProfile
            End If
        End If
        Return freeMessagesCount
    End Function

    'Private Function GetSubsriptionAvailableMessages(fromProfileID As Integer) As Integer
    '    Dim mySubscriptionMessages As Integer = 0
    '    Dim fromProfileIDFreeMessages As Integer = 0
    '    If (ConfigurationManager.AppSettings("AllowSubscription") = "1") Then
    '        If (Not String.IsNullOrEmpty(Request.QueryString("subscription")) AndAlso Request.QueryString("subscription") = "1") Then
    '            If (freeMessagesHelper.HasSubscription) Then

    '                Dim tmp As Integer = 0
    '                Dim countrySys As SYS_CountriesGEO = SYS_CountriesGEOHelper.SYS_CountriesGEO_GetForCountry(Me.SessionVariables.MemberData.Country)
    '                If (countrySys.MonthlySubscriptionEnabled = True) Then
    '                    If (countrySys.MaxMessagesToProfilePerDay > 0) Then tmp = tmp + countrySys.MaxMessagesToProfilePerDay
    '                    If (countrySys.MaxMessagesToDifferentProfilesPerDay > 0) Then tmp = tmp + countrySys.MaxMessagesToDifferentProfilesPerDay
    '                End If


    '                If (tmp > 0) Then
    '                    Dim cls As clsSubscriptionMessagesHelper = clsSubscriptionMessagesHelper.GetData(Me.MasterProfileId, fromProfileID, Me.SessionVariables.MemberData.Country)
    '                    If (Not cls.IsProfilesCountLimit OrElse cls.HasProfileConversationToday) Then
    '                        mySubscriptionMessages = cls.TotalMessagesAvailableForProfile
    '                    End If
    '                End If

    '            End If
    '        End If
    '    End If
    '    Return mySubscriptionMessages
    'End Function

    Private __SubscriptionMessagesHelper As CustomerSubscriptionAvailableMessages2Result
    'Private __SubscriptionMessagesHelper As clsSubscriptionMessagesHelper
    Private Function GetSubsriptionAvailableMessages(manProfileId As Integer?,
                                                     womanProfileId As Integer?,
                                                     manCountry As String,
                                                     Optional dateFrom As Date? = Nothing,
                                                     Optional dateTo As Date? = Nothing) As Integer
        Dim subsriptionMessagesCount As Integer = 0

        If (__SubscriptionMessagesHelper Is Nothing) Then
            If (ConfigurationManager.AppSettings("AllowSubscription") = "1" AndAlso
                freeMessagesHelper.HasSubscription) Then
                If (Not String.IsNullOrEmpty(Request.QueryString("subscription")) AndAlso Request.QueryString("subscription") = "1") Then

                    Dim tmp As Integer = 0
                    'If (ProfileCountry.MaxMessagesToProfilePerDay > 0) Then tmp = tmp + ProfileCountry.MaxMessagesToProfilePerDay
                    'If (ProfileCountry.MaxMessagesToDifferentProfilesPerDay > 0) Then tmp = tmp + ProfileCountry.MaxMessagesToDifferentProfilesPerDay
                    If (ProfileSettings.MaxMessagesToProfilePerDay > 0) Then tmp = tmp + ProfileSettings.MaxMessagesToProfilePerDay
                    If (ProfileSettings.MaxMessagesToDifferentProfilesPerDay > 0) Then tmp = tmp + ProfileSettings.MaxMessagesToDifferentProfilesPerDay

                    If (tmp > 0) Then
                        Using cmdb As CMSDBDataContext = Me.GetCMSDBDataContext
                            __SubscriptionMessagesHelper = cmdb.CustomerSubscriptionAvailableMessages2(manProfileId, womanProfileId, manCountry, dateFrom, dateTo).FirstOrDefault()
                        End Using

                        '__SubscriptionMessagesHelper = clsSubscriptionMessagesHelper.GetData(Me.MasterProfileId, fromProfileID, Me.SessionVariables.MemberData.Country)
                    End If

                End If
            End If
        End If

        If (__SubscriptionMessagesHelper IsNot Nothing) Then
            If (Not __SubscriptionMessagesHelper.IsProfilesCountLimit OrElse __SubscriptionMessagesHelper.HasProfileConversationToday) Then
                subsriptionMessagesCount = __SubscriptionMessagesHelper.TotalMessagesAvailableForProfile
            End If
        End If
        Return subsriptionMessagesCount
    End Function

    Private Sub PerformMessageUnlock()
        Try
            '' unlock message
            Dim gotoBilling As Boolean = False
            Dim subscriptionUsed As Boolean = False

            Dim unmsgId As Long = MyBase.UnlimitedMsgID
            Dim message As EUS_Message = clsUserDoes.GetMessage(unmsgId)
            If (message Is Nothing) Then Throw New MessageNotFoundException()

            If (message.FromProfileID <> Me.MasterProfileId) Then
                Me.UserIdInView = message.FromProfileID
            Else
                Me.UserIdInView = message.ToProfileID
            End If


            '''''''''''''
            'reset session property to update available credits, when master.page loads
            Session("CustomerAvailableCredits") = Nothing
            '''''''''''''


            Dim HasCommunication As Boolean = GetHasCommunicationDict(Me.UserIdInView)
            If (Not HasCommunication) Then

                Dim ReadOnce As Boolean = (Request.QueryString("read") = "once")
                Dim ReadUnlimited As Boolean = (Request.QueryString("read") = "unl")
                Dim myFreeMessages As Integer = 0
                Dim mySubscriptionMessages As Integer = 0

                ' here read once specific message, and pay credits only for that
                If (Request.QueryString("onemsg") = "1" AndAlso message.StatusID = 0) Then

                    Try
                        gotoBilling = True
                        Dim HasRequiredCredits_UNLOCK_MESSAGE_READ As Boolean = HasRequiredCredits(UnlockType.UNLOCK_MESSAGE_READ)
                        If (Not HasRequiredCredits_UNLOCK_MESSAGE_READ) Then

                            mySubscriptionMessages = GetSubsriptionAvailableMessages(Me.MasterProfileId, Me.UserIdInView, Me.SessionVariables.MemberData.Country)
                            If (mySubscriptionMessages < 1) Then
                                myFreeMessages = Me.GetMembersFreeMessagesAvailableToREAD(Me.MasterProfileId, Me.UserIdInView, Me.SessionVariables.MemberData.Country)
                            End If

                        End If

                        If (HasRequiredCredits_UNLOCK_MESSAGE_READ OrElse
                            (mySubscriptionMessages > 0) OrElse
                            (myFreeMessages > 0)) Then


                            Dim _REF_CRD2EURO_Rate As Double = DataHelpers.GetEUS_Profiles_REF_CRD2EURO_Rate(Me.MasterProfileId)
                            Dim customerCreditsId As Long

                            If (HasRequiredCredits_UNLOCK_MESSAGE_READ) Then


                                ' get current unread mesages from UserIdInView to MasterProfileId
                                customerCreditsId = clsUserDoes.UnlockMessageOnce(Me.UserIdInView,
                                                                                Me.MasterProfileId,
                                                                                ProfileHelper.Config_UNLOCK_MESSAGE_READ_CREDITS,
                                                                                UnlockType.UNLOCK_MESSAGE_READ,
                                                                                message.EUS_MessageID,
                                                                                _REF_CRD2EURO_Rate,
                                                                  False)

                                Try
                                    ' write commissition for sender referrer
                                    Dim cost As Double = ProfileHelper.Config_UNLOCK_MESSAGE_READ_CREDITS * _REF_CRD2EURO_Rate
                                    clsReferrer.SetCommissionForMessage(message.CopyMessageID,
                                                                        message.FromProfileID,
                                                                        cost,
                                                                        ProfileHelper.Config_UNLOCK_MESSAGE_READ_CREDITS,
                                                                        UnlockType.UNLOCK_MESSAGE_READ,
                                                                        customerCreditsId)
                                Catch ex As Exception
                                    WebErrorSendEmail(ex, "")
                                End Try

                            ElseIf (mySubscriptionMessages > 0) Then
                                subscriptionUsed = True
                                Dim SubscriptionREF_CRD2EURO_Rate As Double = SYS_CountriesGEOHelper.SYS_CountriesGEO_GetSubscriptionREF_CRD2EURO_Rate(ProfileCountry)

                                Dim isSet As Boolean = False
                                Dim setCommission As Boolean = False

                                Dim costPerMessage As Double = 0
                                Dim subscriptionMessageRate As Double = 0
                                If (freeMessagesHelper.SubscriptionMessageReferralAmount IsNot Nothing) Then
                                    costPerMessage = freeMessagesHelper.SubscriptionMessageReferralAmount
                                    subscriptionMessageRate = SubscriptionREF_CRD2EURO_Rate
                                Else
                                    costPerMessage = ProfileHelper.Config_UNLOCK_MESSAGE_READ_CREDITS * _REF_CRD2EURO_Rate
                                    subscriptionMessageRate = _REF_CRD2EURO_Rate
                                End If

                                Try
                                    customerCreditsId = clsUserDoes.UnlockMessageOnce(Me.UserIdInView,
                                                                                    Me.MasterProfileId,
                                                                                    ProfileHelper.Config_UNLOCK_MESSAGE_READ_CREDITS,
                                                                                    UnlockType.UNLOCK_MESSAGE_READ,
                                                                                    message.EUS_MessageID,
                                                                                    subscriptionMessageRate,
                                                                                    True)

                                Catch ex As Exception
                                    WebErrorSendEmail(ex, "mySubscriptionMessages-->clsUserDoes.UnlockMessageOnce")
                                End Try


                                Try
                                    ' write commissition for receiving referrer from subscription message
                                    If (freeMessagesHelper.SubscriptionMessagesCountReferralPayed = -1) Then
                                        setCommission = True
                                    ElseIf (freeMessagesHelper.SubscriptionMessagesCountReferralPayed > 0) Then
                                        If (__SubscriptionMessagesHelper IsNot Nothing AndAlso
                                            __SubscriptionMessagesHelper.CommisionSetMessagesCountToday < freeMessagesHelper.SubscriptionMessagesCountReferralPayed) Then
                                            setCommission = True
                                        End If
                                    End If

                                    If (setCommission) Then
                                        Try
                                            isSet = clsReferrer.SetCommissionForMessage_Subscription(Me.MasterProfileId, costPerMessage, message.CopyMessageID, message.FromProfileID, customerCreditsId, UnlockType.UNLOCK_MESSAGE_READ)
                                        Catch ex As Exception
                                            WebErrorSendEmail(ex, "")
                                        End Try
                                        'isSet = clsReferrer.SetCommissionForMessage(message.CopyMessageID,
                                        '                                    message.FromProfileID,
                                        '                                    costPerMessage,
                                        '                                    ProfileHelper.Config_UNLOCK_MESSAGE_READ_CREDITS,
                                        '                                    UnlockType.UNLOCK_MESSAGE_READ,
                                        '                                    customerCreditsId)

                                    End If
                                Catch ex As Exception
                                    WebErrorSendEmail(ex, "mySubscriptionMessages-->clsReferrer.SetCommissionForMessage")
                                End Try


                                Try
                                    If (isSet) Then
                                        freeMessagesHelper.MessageSetCommission(message.EUS_MessageID, message.CopyMessageID, Me.MasterProfileId, Me.UserIdInView)
                                    End If
                                Catch ex As Exception
                                    WebErrorSendEmail(ex, "mySubscriptionMessages-->freeMessagesHelper.MessageSetCommission")
                                End Try

                                __SubscriptionMessagesHelper = Nothing


                                '--------------------------------
                                'subscriptionUsed = True
                                'Dim _REF_CRD2EURO_Rate As Double = DataHelpers.GetEUS_Profiles_REF_CRD2EURO_Rate(Me.MasterProfileId)

                                '' get current unread mesages from UserIdInView to MasterProfileId
                                'customerCreditsId = clsUserDoes.UnlockMessageOnce(Me.UserIdInView,
                                '                                                Me.MasterProfileId,
                                '                                                ProfileHelper.Config_UNLOCK_MESSAGE_READ_CREDITS,
                                '                                                UnlockType.UNLOCK_MESSAGE_READ,
                                '                                                message.EUS_MessageID,
                                '                                                _REF_CRD2EURO_Rate,
                                '                                  True)

                                'Try
                                '    ' write commissition for sender referrer
                                '    Dim cost As Double = ProfileHelper.Config_UNLOCK_MESSAGE_READ_CREDITS * _REF_CRD2EURO_Rate
                                '    clsReferrer.SetCommissionForMessage(message.CopyMessageID,
                                '                                        message.FromProfileID,
                                '                                        cost,
                                '                                        ProfileHelper.Config_UNLOCK_MESSAGE_READ_CREDITS,
                                '                                        UnlockType.UNLOCK_MESSAGE_READ,
                                '                                        customerCreditsId)
                                'Catch ex As Exception
                                '    WebErrorSendEmail(ex, "")
                                'End Try
                                '--------------------------------

                            ElseIf (myFreeMessages > 0) Then
                                Dim isSet As Boolean = False
                                Dim setCommission As Boolean = False

                                Dim costPerMessage As Double = 0
                                Dim freeMessageRate As Double = 0
                                If (freeMessagesHelper.CreditsFreeMessageReferralAmount IsNot Nothing) Then
                                    costPerMessage = freeMessagesHelper.CreditsFreeMessageReferralAmount
                                    freeMessageRate = freeMessagesHelper.CreditsFreeMessageReferralAmount / ProfileHelper.Config_UNLOCK_MESSAGE_READ_CREDITS
                                Else
                                    costPerMessage = ProfileHelper.Config_UNLOCK_MESSAGE_READ_CREDITS * _REF_CRD2EURO_Rate
                                    freeMessageRate = _REF_CRD2EURO_Rate
                                End If

                                Try
                                    customerCreditsId = clsUserDoes.UnlockMessageOnce(Me.UserIdInView,
                                                                                    Me.MasterProfileId,
                                                                                    ProfileHelper.Config_UNLOCK_MESSAGE_FREE_CREDITS,
                                                                                    UnlockType.UNLOCK_MESSAGE_READ_FREE,
                                                                                    message.EUS_MessageID,
                                                                                    freeMessageRate,
                                                                                    False) '_REF_CRD2EURO_Rate
                                Catch ex As Exception
                                    WebErrorSendEmail(ex, "myFreeMessages-->clsUserDoes.UnlockMessageOnce")
                                End Try


                                Try
                                    ' write commissition for receiving referrer from free message
                                    If (freeMessagesHelper.CreditsFreeMessagesCountReferralPayed = -1) Then
                                        setCommission = True
                                    ElseIf (freeMessagesHelper.CreditsFreeMessagesCountReferralPayed > 0) Then
                                        If (__clsFreeMessagesHelper IsNot Nothing AndAlso
                                            __clsFreeMessagesHelper.CommisionSetMessagesCount < freeMessagesHelper.CreditsFreeMessagesCountReferralPayed) Then
                                            setCommission = True
                                        End If
                                    End If

                                    If (setCommission) Then

                                        isSet = clsReferrer.SetCommissionForMessage(message.CopyMessageID,
                                                                            message.FromProfileID,
                                                                            costPerMessage,
                                                                            ProfileHelper.Config_UNLOCK_MESSAGE_READ_CREDITS,
                                                                            UnlockType.UNLOCK_MESSAGE_READ,
                                                                            customerCreditsId)
                                    End If
                                Catch ex As Exception
                                    WebErrorSendEmail(ex, "myFreeMessages-->clsReferrer.SetCommissionForMessage")
                                End Try


                                Try
                                    If (isSet) Then
                                        freeMessagesHelper.MessageSetCommission(message.EUS_MessageID, message.CopyMessageID, Me.MasterProfileId, Me.UserIdInView)
                                    End If
                                Catch ex As Exception
                                    WebErrorSendEmail(ex, "myFreeMessages-->freeMessagesHelper.MessageSetCommission")
                                End Try

                                __clsFreeMessagesHelper = Nothing


                                '--------------------------------
                                '' Read for free current message
                                '' get current unread mesages from UserIdInView to MasterProfileId
                                'customerCreditsId = clsUserDoes.UnlockMessageOnce(Me.UserIdInView,
                                '                                                Me.MasterProfileId,
                                '                                                ProfileHelper.Config_UNLOCK_MESSAGE_FREE_CREDITS,
                                '                                                UnlockType.UNLOCK_MESSAGE_READ_FREE,
                                '                                                message.EUS_MessageID,
                                '                                                0,
                                '                                  False)

                                'Try
                                '    ' write commissition for sender referrer
                                '    Dim cost As Double = freeMessagesHelper.CreditsFreeMessageReferralAmount
                                '    clsReferrer.SetCommissionForMessage(message.CopyMessageID,
                                '                                        message.FromProfileID,
                                '                                        cost,
                                '                                        ProfileHelper.Config_UNLOCK_MESSAGE_READ_CREDITS,
                                '                                        UnlockType.UNLOCK_MESSAGE_READ,
                                '                                        customerCreditsId)
                                'Catch ex As Exception
                                '    WebErrorSendEmail(ex, "")
                                'End Try
                                '--------------------------------

                            End If

                            clsUserDoes.MarkMessageRead(message.EUS_MessageID)
                            MakeNewDate(Me.MasterProfileId, Me.UserIdInView)

                            '''''''''''''
                            'reset session property to update available credits, when master.page loads
                            Session("CustomerAvailableCredits") = Nothing
                            '''''''''''''

                            gotoBilling = False
                        End If
                    Catch ex As Exception
                        WebErrorMessageBox(Me, ex, "")
                    End Try

                ElseIf (message.StatusID = 0) Then

                    ' check for credits amount required to unlock
                    If (ReadOnce) Then
                        Try
                            gotoBilling = True
                            Dim HasRequiredCredits_To_UnlockMessage As Boolean = HasRequiredCredits(UnlockType.UNLOCK_MESSAGE_READ)
                            'Dim HasRequiredCredits_To_UnlockMessage As Boolean = clsUserDoes.HasRequiredCredits(Me.MasterProfileId, UnlockType.UNLOCK_MESSAGE_READ)
                            If (HasRequiredCredits_To_UnlockMessage) Then
                                ' get all unread mesages from UserIdInView to MasterProfileId
                                Dim messages As List(Of EUS_Message) = clsUserDoes.GetAllUnreadMessages(Me.UserIdInView, Me.MasterProfileId)
                                If (messages.Count > 0) Then

                                    Dim _REF_CRD2EURO_Rate As Double = DataHelpers.GetEUS_Profiles_REF_CRD2EURO_Rate(Me.MasterProfileId)
                                    Dim costPerMessage As Double = ProfileHelper.Config_UNLOCK_MESSAGE_READ_CREDITS * _REF_CRD2EURO_Rate

                                    Dim customerCreditsId As Long = clsUserDoes.UnlockMessageOnce(Me.UserIdInView,
                                                                                                  Me.MasterProfileId,
                                                                                                  ProfileHelper.Config_UNLOCK_MESSAGE_READ_CREDITS * messages.Count,
                                                                                                  UnlockType.UNLOCK_MESSAGE_READ,
                                                                                                  0,
                                                                                                  Nothing, False)
                                    MakeNewDate(Me.MasterProfileId, Me.UserIdInView)

                                    '''''''''''''
                                    'reset session property to update available credits, when master.page loads
                                    Session("CustomerAvailableCredits") = Nothing
                                    '''''''''''''

                                    Dim cnt As Integer
                                    For cnt = 0 To messages.Count - 1
                                        clsUserDoes.MarkMessageRead(messages(cnt).EUS_MessageID)

                                        Try
                                            ' write commissition for recipient referrer
                                            clsReferrer.SetCommissionForMessage(messages(cnt).EUS_MessageID,
                                                                                Me.UserIdInView,
                                                                                costPerMessage,
                                                                                ProfileHelper.Config_UNLOCK_MESSAGE_READ_CREDITS,
                                                                                UnlockType.UNLOCK_MESSAGE_READ,
                                                                                customerCreditsId)
                                        Catch ex As Exception
                                            WebErrorSendEmail(ex, "")
                                        End Try
                                    Next
                                End If
                                gotoBilling = False
                            End If
                        Catch ex As Exception
                            WebErrorMessageBox(Me, ex, "")
                        End Try
                    End If

                    If (ReadUnlimited) Then
                        Try
                            gotoBilling = True
                            Dim HasRequiredCredits_To_UnlockConversation As Boolean = HasRequiredCredits(UnlockType.UNLOCK_CONVERSATION)
                            'Dim HasRequiredCredits_To_UnlockConversation As Boolean = clsUserDoes.HasRequiredCredits(Me.MasterProfileId, UnlockType.UNLOCK_CONVERSATION)
                            If (HasRequiredCredits_To_UnlockConversation) Then
                                Dim customerCreditsId As Long = clsUserDoes.UnlockMessageConversation(unmsgId,
                                                                      ProfileHelper.Config_UNLOCK_CONVERSATION_CREDITS,
                                                                      Me.MasterProfileId,
                                                                      clsUserDoes.GetRequiredCredits(UnlockType.UNLOCK_CONVERSATION).EUS_CreditsTypeID, False)
                                MakeNewDate(Me.MasterProfileId, Me.UserIdInView)
                                clsUserDoes.MarkMessageRead(unmsgId)

                                Try
                                    ' write commissition for recipient referrer
                                    Dim _REF_CRD2EURO_Rate As Double = DataHelpers.GetEUS_Profiles_REF_CRD2EURO_Rate(Me.MasterProfileId)
                                    Dim cost As Double = ProfileHelper.Config_UNLOCK_CONVERSATION_CREDITS * _REF_CRD2EURO_Rate
                                    clsReferrer.SetCommissionForMessage(unmsgId,
                                                                        Me.UserIdInView,
                                                                        cost,
                                                                        ProfileHelper.Config_UNLOCK_CONVERSATION_CREDITS,
                                                                        UnlockType.UNLOCK_CONVERSATION,
                                                                        customerCreditsId)
                                Catch ex As Exception
                                    WebErrorSendEmail(ex, "")
                                End Try

                                gotoBilling = False
                            End If
                        Catch ex As Exception
                            WebErrorMessageBox(Me, ex, "")
                        End Try
                    End If


                End If

                'ElseIf (Me.IsFemale) Then

                '    clsUserDoes.MarkMessageRead(message.EUS_MessageID)

            End If


            If (gotoBilling) Then
                SessionVariables.UserUnlockInfo.OfferId = 0
                SessionVariables.UserUnlockInfo.ReturnUrl = Request.Url.ToString()

                If (Page.IsCallback) Then

                Else
                    Dim billingurl As String = ResolveUrl("~/Members/SelectProduct.aspx?returnurl=" & HttpUtility.UrlEncode(Request.Url.ToString()))

                    Response.Write("{""redirect"":""" & billingurl & """}")
                End If
            Else
                Try
                    freeMessagesHelper.MessageReadByMan(message.EUS_MessageID,
                                                        message.CopyMessageID,
                                                        Me.MasterProfileId,
                                                        Me.UserIdInView,
                                                        clsCurrentContext.GetCurrentIP(),
                                                        clsCurrentContext.GetCookieID(),
                                                        If(subscriptionUsed, FreeMessagesArchiveEnum.SubscriptionMessage, FreeMessagesArchiveEnum.FreeMessage))

                Catch ex As Exception
                    WebErrorSendEmail(ex, "update FreeMessageArchive")
                End Try

                LoadUsersConversation(unmsgId)
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Private Sub BindMessagesConversation(messageId As Integer)
        Dim num As Integer = 0
        'If (Not String.IsNullOrEmpty(Request("rowFrom")) AndAlso Integer.TryParse(Request("rowFrom"), num)) Then
        '    convCtl.RowNumberMin = num
        'Else
        convCtl.RowNumberMin = 0
        'End If
        If (Not String.IsNullOrEmpty(Request("rowTo")) AndAlso Integer.TryParse(Request("rowTo"), num)) Then
            convCtl.RowNumberMax = num
        Else
            convCtl.RowNumberMax = 5
        End If
        If (Not String.IsNullOrEmpty(Request("olderMsgIdAvailable")) AndAlso Integer.TryParse(Request("olderMsgIdAvailable"), num)) Then
            convCtl.OlderMsgIdAvailable = num
        End If

        convCtl.OfferId = Me.OfferId
        convCtl.UserIdInView = Me.UserIdInView
        convCtl.SubjectInView = Me.SubjectInView
        convCtl.MessageIdInView = Me.MessageIdInView
        convCtl.MessageIdListInView = Me.MessageIdListInView
        convCtl.SendAnotherMessageToRecipient = Me.SendAnotherMessageToRecipient
        convCtl.CurrentLoginName = Me.CurrentLoginName
        convCtl.LoadMessageOnly = True

        convCtl.LoadUsersConversation(messageId)
        Me.MessageIdListInView = convCtl.MessageIdListInView
        Me.ScrollToElem = convCtl.ScrollToElem
    End Sub

    Private Sub LoadUsersConversation(messageId As Integer)
        Me.MessageIdInView = messageId

        If (messageId > 0) Then
            BindMessagesConversation(messageId)
        End If


        ' when message id specified, Me.UserIdInView set on  dvConversation DataBound
        Dim otherUser As Integer = Me.UserIdInView
        If (Me.UserIdInView > 1) OrElse (Me.SendAnotherMessageToRecipient > 0) Then
            otherUser = Me.UserIdInView
            If (otherUser = 0) Then otherUser = Me.SendAnotherMessageToRecipient
        End If

    End Sub




    Protected Function IsMessageHidden(DataItem As DataRowView) As String
        Dim class1 As String = ""
        Dim HasCommunication As Boolean = GetHasCommunicationDict(DataItem("ProfileID"))
        If (Not HasCommunication AndAlso DataItem("StatusID") = 0 AndAlso Me.IsMale) Then
            class1 = "closed"
        End If

        Return class1
    End Function

    Private Property HasCommunicationDict As Dictionary(Of Integer, Boolean)
    Protected Function GetHasCommunicationDict(otherProfileID As Integer, Optional force As Boolean = False) As Boolean
        Dim HasCommunication As Boolean

        If (HasCommunicationDict Is Nothing) Then HasCommunicationDict = New Dictionary(Of Integer, Boolean)
        If (Not HasCommunicationDict.ContainsKey(otherProfileID) OrElse force) Then

            HasCommunication = GetHasCommunication(otherProfileID)
            If (force) Then
                HasCommunicationDict(otherProfileID) = HasCommunication
            Else
                HasCommunicationDict.Add(otherProfileID, HasCommunication)
            End If

        ElseIf (HasCommunicationDict.ContainsKey(otherProfileID)) Then

            HasCommunication = HasCommunicationDict(otherProfileID)

        End If

        Return HasCommunication
    End Function

    Private Sub MakeNewDate(FromProfileId As Integer, ToProfileId As Integer)
        Dim anyOfferAccepted As Boolean = False

        Dim parms As New NewOfferParameters()
        If (Me.OfferId > 0) Then

            Dim _offer As EUS_Offer = clsUserDoes.GetOfferByOfferId(Me.MasterProfileId, Me.OfferId)
            If (_offer.OfferTypeID = ProfileHelper.OfferTypeID_POKE) Then
                clsUserDoes.AcceptPoke(Me.OfferId, OfferStatusEnum.POKE_ACCEEPTED_WITH_MESSAGE)
                anyOfferAccepted = True
            ElseIf (_offer.OfferTypeID = ProfileHelper.OfferTypeID_WINK) Then
                clsUserDoes.AcceptLike(Me.OfferId, OfferStatusEnum.LIKE_ACCEEPTED_WITH_MESSAGE)
                anyOfferAccepted = True
            ElseIf (_offer.OfferTypeID = ProfileHelper.OfferTypeID_OFFERNEW) OrElse (_offer.OfferTypeID = ProfileHelper.OfferTypeID_OFFERCOUNTER) Then
                clsUserDoes.AcceptOffer(Me.OfferId, OfferStatusEnum.OFFER_ACCEEPTED_WITH_MESSAGE)
                anyOfferAccepted = True
            End If


            'If (SendUnlimited) Then
            parms.messageText1 = ""
            parms.messageText2 = ""
            parms.offerAmount = _offer.Amount
            parms.parentOfferId = _offer.OfferID
            parms.userIdReceiver = _offer.ToProfileID
            parms.userIdWhoDid = _offer.FromProfileID
            'End If
        Else
            parms.messageText1 = ""
            parms.messageText2 = ""
            parms.offerAmount = 0
            'parms.parentOfferId = _offer.OfferID
            parms.userIdReceiver = ToProfileId
            parms.userIdWhoDid = FromProfileId
        End If

        If (Me.IsMale) Then
            If (Not clsUserDoes.HasDateOfferAlready(parms.userIdWhoDid, parms.userIdReceiver)) Then clsUserDoes.MakeNewDate(parms)
        End If


        If (Not anyOfferAccepted) Then

            Dim rec As EUS_Offer = clsUserDoes.GetAnyWinkPending(Me.MasterProfileId, Me.UserIdInView)
            If (rec IsNot Nothing) Then
                'like check succesfull
                clsUserDoes.AcceptLike(rec.OfferID, OfferStatusEnum.LIKE_ACCEEPTED_WITH_MESSAGE)
            Else
                rec = clsUserDoes.GetAnyPokePending(Me.MasterProfileId, Me.UserIdInView)
                If (rec IsNot Nothing) Then
                    'poke check succesfull
                    clsUserDoes.AcceptPoke(rec.OfferID, OfferStatusEnum.POKE_ACCEEPTED_WITH_MESSAGE)
                Else
                    rec = clsUserDoes.GetLastOffer(Me.MasterProfileId, Me.UserIdInView)
                    If (rec IsNot Nothing) Then
                        'offer check succesfull
                        clsUserDoes.AcceptOffer(rec.OfferID, OfferStatusEnum.OFFER_ACCEEPTED_WITH_MESSAGE)
                    End If
                End If
            End If

        End If

    End Sub

End Class




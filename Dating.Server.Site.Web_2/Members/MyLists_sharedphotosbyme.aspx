﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Members/Members2Inner.Master" 
CodeBehind="MyLists_sharedphotosbyme.aspx.vb" Inherits="Dating.Server.Site.Web.MyLists_sharedphotosbyme" %>
<%@ Register src="../UserControls/SearchControl.ascx" tagname="SearchControl" tagprefix="uc2" %>
<%@ Register src="../UserControls/MemberLeftPanel.ascx" tagname="MemberLeftPanel" tagprefix="uc1" %>
<%@ Register src="~/UserControls/WhatIsIt.ascx" tagname="WhatIsIt" tagprefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<style type="text/css">
#search-members {
margin-top: 0;
}
</style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
<div id="search-members">

    <div class="list-title">
        <asp:Literal ID="lblItemsName" runat="server" />
        <asp:LinkButton ID="lnkViewDescription" runat="server" CssClass="more_info2" OnClientClick="OnMoreInfoClick('[contentUrl]', this, '[headerText]');return false;" />
    </div>

    <div id="divFilter" runat="server" clientidmode="Static" class="likes-filters">
        <div class="lfloat cr-word">
            <dx:ASPxLabel ID="lblSortByText" runat="server" Text="" EncodeHtml="False">
            </dx:ASPxLabel>
        </div>
        <div class="lfloat">
            <dx:ASPxComboBox ID="cbSort" runat="server" class="update_uri" 
                AutoPostBack="True" EncodeHtml="False">
                <ClientSideEvents SelectedIndexChanged="ShowLoadingOnMenu" />
            </dx:ASPxComboBox>
        </div>
        <div class="rfloat">
            <dx:ASPxComboBox ID="cbPerPage" runat="server" class="update_uri input-small" style="width: 60px;" 
                AutoPostBack="True" EncodeHtml="False">
                <ClientSideEvents SelectedIndexChanged="ShowLoadingOnMenu" />
                <Items>
                    <dx:ListEditItem Text="10" Value="10" Selected="true"  />
                    <dx:ListEditItem Text="25" Value="25" />
                    <dx:ListEditItem Text="50" Value="50" />
                </Items>
            </dx:ASPxComboBox>
        </div>
        <div class="rfloat cr-word">
            <dx:ASPxLabel ID="lblResultsPerPageText" runat="server" Text="" EncodeHtml="False">
            </dx:ASPxLabel>
        </div>
        <div class="clear"></div>
    </div>
<asp:UpdatePanel ID="updOffers" runat="server">
<ContentTemplate>
    <div class="l_wrap">
        <uc2:SearchControl ID="sharedPhotosByMeList" runat="server" UsersListView="SharedPhotosByMeList" />
    </div>
    <div class="pagination">
        <table align="center">
            <tr><td><dx:ASPxPager ID="Pager" runat="server" ItemCount="3" ItemsPerPage="1" RenderMode="Lightweight"
                        CssFilePath="~/App_Themes/Aqua/{0}/styles.css" CssPostfix="Aqua" CurrentPageNumberFormat="{0}"
                        SpriteCssFilePath="~/App_Themes/Aqua/{0}/sprite.css" PageNumberFormat="<span onclick='ShowLoading();'>{0}</span>"
                        EncodeHtml="false" SeoFriendly="Enabled">
                        <LastPageButton Visible="True">
                        </LastPageButton>
                        <FirstPageButton Visible="True">
                        </FirstPageButton>
                        <Summary Position="Left" Text="Page {0} of {1} ({2} members)" Visible="false" />
                    </dx:ASPxPager></td></tr>
        </table>
    </div>
</ContentTemplate>
</asp:UpdatePanel>

    <script type="text/javascript">
        jQuery(function ($) {
            scrollToLogin();
        });
    </script>
	<uc2:whatisit ID="WhatIsIt1" runat="server" />

</div>
</asp:Content>



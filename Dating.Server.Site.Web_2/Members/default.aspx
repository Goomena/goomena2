﻿<%@ Page Title="" Language="vb" AutoEventWireup="true" MasterPageFile="~/Members/Members2Inner.Master"
    CodeBehind="default.aspx.vb" Inherits="Dating.Server.Site.Web._default1" %>


<%@ Register Src="../UserControls/MemberLeftPanel.ascx" TagName="MemberLeftPanel" TagPrefix="uc3" %>
<%@ Register Assembly="DevExpress.Web.v13.1, Version=13.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxDataView" TagPrefix="dx" %>
<%@ Register src="~/UserControls/WhatIsIt.ascx" tagname="WhatIsIt" tagprefix="uc2" %>
<%@ Register Src="~/UserControls/UcTopDashboardMembers.ascx" TagPrefix="uc2" TagName="UcTopDashboardMembers" %>
<%@ Register Src="~/UserControls/UcQuickSearch.ascx" TagPrefix="uc2" TagName="UcQuickSearch" %>
<%--<%@ Register Src="~/UserControls/Messaging/UcQuickMessaging.ascx" TagPrefix="uc2" TagName="UcQuickMessaging" %>--%>



<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .mobilebanner img {
            width: 689px;
        }
    </style>

    <script type="text/javascript">
        // <![CDATA[
        $(document).ready(configurePopup);

        function configurePopup() {
            $('.members-popup .dropdown-menu div, .members-popup .dropdown-menu h3').click(function (e) {
                e.stopPropagation();
            });

            $('.members-popup .dropdown-menu .popupSaveButton').click(function (e) {
                $(this).parent().parent().hide();
            });
            //if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
            //    $('.quick-links-big ').css('width', '699px');
            //    $('.quick-links-big ').css('height', '150px');
            //}
        }

        function showAjaxLoader(id) {
            $('#' + id).html("");

            if (id == "nearest") {

                $('#' + id).css("width", "680px");
                $('#' + id).css("height", "403px");
                $('#' + id).css("background", "url('//cdn.goomena.com/Images2/mbr2/loader.gif') no-repeat center #fff");
            }
            else if (id == "profilesYouHaveViewed") {
                $('#' + id).css("width", "680px");
                $('#' + id).css("height", "149px");
                $('#' + id).css("background", "url('//cdn.goomena.com/Images2/mbr2/loader.gif') no-repeat center #fff");
            }
        }
        // ]]>
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">

    <div class="m_wrap" id="divUploadPhoto" runat="server" visible="false">
        <div id="ph-edit" class="no-photo">
            <div class="items_none items_hard">
                <div class="items_none_wrap">
                    <div class="items_none_text">
                        <asp:Literal ID="msg_UploadPhotoText_ND" runat="server"></asp:Literal>
                        <div class="search_members_button_right">
                            <asp:HyperLink ID="lnkUploadPhoto" runat="server" CssClass="btn btn-danger"></asp:HyperLink></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="welcome-msg" id="hdrWelcome" runat="server">
        <div class="inner-box">
            <asp:Literal ID="lblWelcome" runat="server" />
        </div>
    </div>
    <div id="QuickMembersTop">
        <uc2:UcTopDashboardMembers runat="server" ID="UcTopDashboardMembers" />
    </div>
    <div id="QuickLinkBoxes">
        <div class="InnerBox">
            <a href="/Members/MyLists.aspx?vw=myfavoritelist" class="linkBox MyFavoriteList">
                <span class="iconHolder"></span>
                <asp:Label ID="lblMyFavoriteList" runat="server" Text="My favorite List" CssClass="LabelBox"></asp:Label>
            </a>
            <a href="/Members/MyLists.aspx?vw=myviewedlist" class="linkBox ProfilesIhaveSeen">
                <span class="iconHolder"></span>
                <asp:Label ID="lblProfilesIhaveSeen" runat="server" Text="Profiles i have seen" CssClass="LabelBox"></asp:Label>
            </a>
            <a href="/Members/MyLists.aspx?vw=whoviewedme" class="linkBox WhoHasSeenmyProfile">
                <span class="iconHolder"></span>
                <asp:Label ID="lblWhoHasSeenmyProfile" runat="server" Text="Who have seen my profile" CssClass="LabelBox"></asp:Label>
            </a>
            <a href="/Members/settings.aspx?vw=notifications" class="linkBox AutoNotification">
                <span class="iconHolder"></span>
                <asp:Label ID="lblAutoNotification" runat="server" Text="Auto notification" CssClass="LabelBox"></asp:Label>
            </a>
        </div>
    </div>
    <uc2:UcQuickSearch runat="server" id="UcQuickSearch" />
        <div class="mobilebanner" id="divMobileBanner" runat="server" visible="false">
        <a href="https://play.google.com/store/apps/details?id=com.goomena.app&hl=el" target="_blank">
            <img alt="" style="margin-left: auto; margin-right: auto; display: block;" src="//cdn.goomena.com/Images/MobileBanners/android-banner-member.png" id="iMobileBanner" runat="server" /></a>
    </div>
    <%--<uc2:UcQuickMessaging runat="server" id="UcQuickMessaging" />--%>
    <div class="d_block">
    
        <div class="dashboard_announce" style="">
            <div class="d_bottom" style="padding-top: 10px;">
                <dx:ASPxLabel ID="lblImportantAdvice" runat="server" Text=""
                    EncodeHtml="False">
                </dx:ASPxLabel>
            </div>
            <div class="clear"></div>
        </div>

    </div>
    <div class="clear"></div>
    	<uc2:whatisit ID="WhatIsIt1" runat="server" />
</asp:Content>



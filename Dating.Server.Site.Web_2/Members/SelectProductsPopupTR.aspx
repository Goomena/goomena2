﻿<%@ Page Title="" Language="vb" AutoEventWireup="true" MasterPageFile="~/Members/Members2Inner.Master" 
    CodeBehind="SelectProductsPopupTR.aspx.vb" 
    Inherits="Dating.Server.Site.Web.SelectProductsPopupTR" %>
<%@ Register src="~/UserControls/UCPrice2.ascx" tagname="UCPrice" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        body { background-image: none; }
        h2{padding:0;margin:5px 0;}
#payment_2{width:584px;height:620px;padding:20px;text-align:center;}    
#payment_2 #products_2 { position: relative; height: auto; width:550px;}
#payment_2 #products_2 .product-1 {background: url('//cdn.goomena.com/Images2/payment-popup/1000credits.png') no-repeat scroll 0 0; position: absolute; width: 169px; height: 200px; left: 0px; top:9px;}
#payment_2 #products_2 .product-2 {background: url('//cdn.goomena.com/Images2/payment-popup/3000credits.png') no-repeat scroll 0 0; position: absolute; width: 169px; height: 200px; left: 204px; top:9px;}
#payment_2 #products_2 .product-3 {background: url('//cdn.goomena.com/Images2/payment-popup/6000credtis.png') no-repeat scroll 0 0; position: absolute; width: 176px; height: 207px; left: 406px; top:0px;}
    
#payment_2 #products_2 .pricecol { position: absolute; top: 20px; left: 15px; background-repeat: no-repeat; width: 85px; height: 85px; }
#payment_2 #products_2 .product-1 .pricecol { }
#payment_2 #products_2 .product-2 .pricecol {}
#payment_2 #products_2 .product-3 .pricecol {}
    
#payment_2 #products_2 .pricecol-credits {display:none; position: absolute; top: 20px; left: 0; right: 0; font-size: 42px; color: #fff;text-align: center; }
#payment_2 #products_2 .pricecol-credits-text { position: absolute; top: 42px; left: 0;right:0;  font-size: 27px; color: #fff; text-align: center; }
#payment_2 #products_2 .product-3  .pricecol-credits {display:none; top: 29px;  }
#payment_2 #products_2 .product-3  .pricecol-credits-text { top: 45px;  }

#payment_2 #products_2 .pricecol-price {display:none; position: absolute; top: 145px; left: 0;right:0; font-size: 54px; color: #000; text-align: center;}
#payment_2 #products_2 .product-3 .pricecol-price { top: 154px;}
    
    
#payment_2 #products_2 .pricecol-expire-wrap { position: absolute; top: 51px; left: 0;right:0; font-size: 14px; color: #000; background: url('//cdn.goomena.com/Images2/payment/check-cycle.png') no-repeat scroll 0 50%; padding-left: 20px; vertical-align: middle; display:none;}
#payment_2 #products_2 .product-3 .pricecol-expire-wrap { top: 58px;}
#payment_2 #products_2 .pricecol-expire { position: absolute; top: 152px; left: 30px; font-size: 14px; color: #000; background: url('//cdn.goomena.com/Images2/payment/check-cycle.png') no-repeat scroll 0 50%; padding-left: 20px; vertical-align: middle; }
#payment_2 #products_2 .pricecol-expire-line2 { position: absolute; top: 176px; left: 30px; font-size: 14px; color: #000; background: url('//cdn.goomena.com/Images2/payment/check-cycle.png') no-repeat scroll 0 50%; padding-left: 20px; vertical-align: middle; }
#payment_2 #products_2 .pricecol-expire-line3 { position: absolute; top: 200px; left: 30px; font-size: 14px; color: #000; background: url('//cdn.goomena.com/Images2/payment/check-cycle.png') no-repeat scroll 0 50%; padding-left: 20px; vertical-align: middle; }
#payment_2 #products_2 .pricecol-order-btn { color: #fff; text-transform: uppercase; font-size: 15px; text-align: center; line-height: 32px; 
                                            border-width: 0px; width: 121px; height: 32px; position: absolute; bottom: 15px; left: 27px; 
                                            display: inline-block; 
                                            background: url('//cdn.goomena.com/Images2/payment-popup/order-button.png') no-repeat scroll 0 -7%;text-indent:-9999px; }
#payment_2 #products_2 .pricecol-order-btn:hover {background-position: 100% 100%;}
#payment_2 #products_2 .product-3 .pricecol-order-btn {bottom: 13px;}
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
<div id="payment_2" runat="server" clientidmode="Static">
    <asp:Label ID="lblInfo" runat="server" CssClass="info-content">
    </asp:Label>  

    <div id="products_2">
        <uc1:UCPrice ID="UCPrice1" runat="server" View="vwMember" CssClass="product-1"  />
        <uc1:UCPrice ID="UCPrice2" runat="server" View="vwMember" CssClass="product-2" Discount="30"  />
        <uc1:UCPrice ID="UCPrice3" runat="server" View="vwMember" CssClass="product-3" IsRecommended="true" Discount="45"   />
    </div>
</div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cntBodyBottom" runat="server">
</asp:Content>

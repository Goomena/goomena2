﻿Imports Dating.Server.Core.DLL

Public Class PopupNewTerms
    Inherits BasePage

    Protected Overrides Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        '  SetMasterPage(CurrentPageData)
        Try
            ' If (Me.SessionVariables.MemberData.Status = ProfileStatusEnum.LIMITED) Then
            '  Response.Redirect(ResolveUrl("~/Members/Default.aspx"))
            '     End If

            '  MyBase.Page_PreInit()
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Protected Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData(Context, Me.Page, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property
    Private Sub Page_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        Try
            If Me._pageData IsNot Nothing Then
                RemoveHandler Me._pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If

        Catch ex As Exception
        End Try
       
        Try
            MyBase.Dispose()
        Catch ex As Exception
        End Try
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            LoadLAG()
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try


    End Sub


    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender

        Try
            Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub

    Protected Sub LoadLAG()
        Try
            If (String.IsNullOrEmpty(CurrentPageData.GetCustomString("lnkNew"))) Then
                clsPageData.ClearCache()
            End If
            lblNewTermsBottom.Text = CurrentPageData.GetCustomString("lblNewTermsBottom")
            lblcbAgreements.Text = CurrentPageData.GetCustomString("lblnewTermsReadMoreText")
            lblnewTermsMessage.Text = CurrentPageData.GetCustomString("lblnewTermsMessage")
            '   lblnewTermsReadMoreText.Text = CurrentPageData.GetCustomString("lblnewTermsReadMoreText")
            lblnewTermsMessageHeader.Text = CurrentPageData.GetCustomString("lblnewTermsMessageHeader")
            btnNewTermsAccept.Text = CurrentPageData.GetCustomString("btnNewTermsAccept")

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub
End Class
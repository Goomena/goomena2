﻿Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Core.DLL
Imports System.Data.SqlClient
Imports System.Globalization
Imports Dating.Server.Datasets.DLL.DSMembers

Public Class LoveHate
    Inherits BasePage


#Region "Props"
    Public ReadOnly Property cmspageClass As String
        Get
            Return If(Me.IsMale, "male", "female")
        End Get
    End Property

    Protected Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            If (_pageData Is Nothing) Then
                '_pageData = New clsPageData("~/Members/Search.aspx", Context)
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData(Context, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property


    Private Property LastAdvancedSearchquery As String
        Get
            Return ViewState("LastAdvancedSearchquery")
        End Get
        Set(value As String)
            ViewState("LastAdvancedSearchquery") = value
        End Set
    End Property


    Private Property LastWhatToSearch As WhatToSearchEnum
        Get
            Return ViewState("LastWhatToSearch")
        End Get
        Set(value As WhatToSearchEnum)
            ViewState("LastWhatToSearch") = value
        End Set
    End Property

    Private Property OldPageIndex As Integer
        Get
            Return ViewState("OldPageIndex")
        End Get
        Set(value As Integer)
            ViewState("OldPageIndex") = value
        End Set
    End Property
    Dim perPageNumber As Integer = 25
    Public Property ItemsPerPage As Integer

        Get
            Return perPageNumber
        End Get

        Set(value As Integer)
            Dim index As Integer = 0
            perPageNumber = value
            If (value = 25) Then

                index = 1
            ElseIf (value = 50) Then
                index = 2
            Else
                index = 0
            End If

            cbPerPage.SelectedIndex = index
        End Set

    End Property





    Protected ReadOnly Property DontAskForSave As Boolean
        Get
            If (Session("AdvancedSearch_Dont_Save_Dont_Ask") Is Nothing) Then
                Return False
            ElseIf (Session("AdvancedSearch_Dont_Save_Dont_Ask") = True) Then
                Return True
            End If
            Return False
        End Get
    End Property


#End Region

    Private Sub Page_Disposed1(sender As Object, e As EventArgs) Handles Me.Disposed

        If _clsVotesHelper1 IsNot Nothing Then _clsVotesHelper1.Dispose()
    End Sub

    '  Private Property mvSearchMain As Object



    Protected Overrides Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        SetMasterPage(CurrentPageData)

        Try
            If clsCurrentContext.VerifyLogin() = True Then
                Me.MasterPageFile = "~/Members/Members2.Master"
            Else
                clsCurrentContext.ClearSession(Session)
                If (Not Me.IsCallback) Then
                    Response.Redirect(ResolveUrl("~/Login.aspx"), False)
                End If
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub
    Private Sub Page_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        Try
            If Me._pageData IsNot Nothing Then
                RemoveHandler Me._pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If

        Catch ex As Exception
        End Try
        Try
            If Me.searchL IsNot Nothing Then
                If Me.searchL.Repeater IsNot Nothing Then
                    RemoveHandler Me.searchL.Repeater.ItemCommand, AddressOf Me.offersRepeater_ItemCommand
                End If
            End If
        Catch ex As Exception
        End Try
        Try
            MyBase.Dispose()
        Catch ex As Exception
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            AddHandler Me.searchL.Repeater.ItemCommand, AddressOf Me.offersRepeater_ItemCommand
           

            If (Not Page.IsPostBack) Then
                'Session("RedirectedLoveHate") = True

                Try
                    Dim i As Integer = clsProfilesPrivacySettings.GET_ItemsPerPage(Me.MasterProfileId)
                    Me.ItemsPerPage = If(i <= 0, 25, i)
                    Me.Pager.ItemsPerPage = ItemsPerPage

                    'Me.ItemsPerPage = If(clsProfilesPrivacySettings.GET_ItemsPerPage(Me.MasterProfileId)
                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "GET_ItemsPerPage")
                End Try

                Dim currentPage As Integer
                Dim req As String = Request.QueryString("seo" & Me.Pager.ClientID)
                If (Not String.IsNullOrEmpty(req)) Then
                    Try
                        req = req.Replace("page", "")
                        Integer.TryParse(req, currentPage)
                        Me.Pager.ItemCount = Me.ItemsPerPage * currentPage
                        Me.Pager.PageIndex = currentPage - 1
                    Catch ex As Exception
                    End Try
                End If

            End If

            If (Not Me.IsPostBack) Then
                LoveHateCmsContentID.CssClass = "LoveHateCmsContent " & Me.cmspageClass
                LoadLAG()
                LoadSavedSearch()
            
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub

    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender

        Try
            Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try


        Try
            If (Me.Pager.PageIndex > 0) Then
                Me.Header.Title = Me.Header.Title & " -- LoveHate page " & (Me.Pager.PageIndex + 1)
            End If

            ' check if page is changed, if so scroll to top.
            ' not scrolling to top cause of updatePanel
            If (Page.IsPostBack) Then
                If (Me.OldPageIndex <> Me.Pager.PageIndex) Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "scrollToResultsTop", "scrollToResultsTop();", True)
                    Me.OldPageIndex = Me.Pager.PageIndex
                Else
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "HideLoading", "HideLoading();", True)
                End If
            End If

        Catch ex As Exception
            'WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub



    Public Overrides Sub Master_LanguageChanged()
        Try

            If (Me.Visible) Then
                Me._pageData = Nothing
                LoadLAG()
                LoadSavedSearch()
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub

    Private Property txtYouCannotVoteUntil As String
    Private Property txtHowManyVotes As String
    Private Property txtHowManyVotes2 As String
    Private Property txtVoteHasFinished As String
    Private Property txtVoteNoPicture As String
    Private Property txtnoResults As String
    Private Property txtYouHaveNotBeenVoted As String
    Private Property txtYouHaveHates As String
    Private Property txtYouHaveLoves As String
    Protected Sub LoadLAG()
        Try
            '      Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            SetControlsValue(Me, CurrentPageData)
            txtYouCannotVoteUntil = CurrentPageData.GetCustomString("lblYouCannotVoteUntil")
            txtHowManyVotes = CurrentPageData.GetCustomString("lblHowManyVotes")
            txtHowManyVotes2 = CurrentPageData.GetCustomString("lblHowManyVotes2")
            txtVoteHasFinished = CurrentPageData.GetCustomString("lblVoteHasFinished")


            lblContentHeader.Text = CurrentPageData.GetCustomString("lblCmsHeader")
            lblContentTXT.Text = If(Me.IsMale, CurrentPageData.GetCustomString("lblMaleText"), CurrentPageData.GetCustomString("lblFemaleText"))
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub
    Private Sub refreshStrings()
        txtYouCannotVoteUntil = CurrentPageData.GetCustomString("lblYouCannotVoteUntil")
        txtHowManyVotes = CurrentPageData.GetCustomString("lblHowManyVotes")
        txtHowManyVotes2 = CurrentPageData.GetCustomString("lblHowManyVotes2")
        txtVoteHasFinished = CurrentPageData.GetCustomString("lblVoteHasFinished")
        txtVoteNoPicture = CurrentPageData.GetCustomString("lblVoteNoPicture")
        txtUserNameQ.NullText = CurrentPageData.GetCustomString("lblnulltext")
        msg_ClearForm.Text = CurrentPageData.GetCustomString("lblcleartext")
        txtnoResults = CurrentPageData.GetCustomString("lblNoResults")
        txtYouHaveNotBeenVoted = CurrentPageData.GetCustomString("lblYouHaveNotBeenVoted")
        txtYouHaveHates = CurrentPageData.GetCustomString("lblYouHaveHates")
        txtYouHaveLoves = CurrentPageData.GetCustomString("lblYouHaveLoves")
    End Sub






    Protected Sub offersRepeater_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs)
        Dim success = False
        Try
            Dim currentUserHasPhotos As Boolean = clsUserDoes.HasPhotos(Me.MasterProfileId, Me.MirrorProfileId)
            If (currentUserHasPhotos) Then
                If (e.CommandName = LoveHateCommandEnum.Love.ToString) Then
                    Try
                        Dim ToProfileID As Integer
                        Integer.TryParse(e.CommandArgument.ToString(), ToProfileID)

                        Dim Love As Boolean = True
                        Dim Hate As Boolean = False
                        clsUserDoes.SendLoveHate(ToProfileID, Me.MasterProfileId, Love, Hate, _clsVotesHelper.DatetimeStarts)
                        'Dim cls As clsLoveHateListItem = (From itm In Me.UsersList
                        '                    Where itm.ProfileID = ToProfileID
                        '                    Select itm).SingleOrDefault()
                        'If cls IsNot Nothing Then
                        '    cls.VoteCount = cls.VoteCount + 1
                        '    cls.AllowVote = False
                        'End If
                        success = True

                    Catch ex As Exception
                        WebErrorMessageBox(Me, ex, "")
                    End Try


                ElseIf (e.CommandName = LoveHateCommandEnum.Hate.ToString) Then
                    Try


                        Dim ToProfileID As Integer
                        Integer.TryParse(e.CommandArgument.ToString(), ToProfileID)
                        'clsUserDoes.SendWink(ToProfileID, Me.MasterProfileId)

                        Dim Love As Boolean = False
                        Dim Hate As Boolean = True
                        clsUserDoes.SendLoveHate(ToProfileID, Me.MasterProfileId, Love, Hate, _clsVotesHelper.DatetimeStarts)
                        success = True
                    Catch ex As Exception
                        WebErrorMessageBox(Me, ex, "")
                    End Try

                End If
            Else
                Response.Redirect("~/Members/Photos.aspx", False)
                HttpContext.Current.ApplicationInstance.CompleteRequest()
            End If



        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try





        Try
            If (success) Then
                '  BindFilteredResults()
                'BindSearchResults()
                LoadSavedSearch()
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try


    End Sub

    Protected Sub ddlPerPage_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbPerPage.SelectedIndexChanged
        Try
            Dim i As Integer = 0
            Select Case cbPerPage.Value
                Case "10"
                    i = 10
                    ItemsPerPage = 10
                    Exit Select
                Case "25"
                    i = 25
                    ItemsPerPage = 25
                    Exit Select
                Case Else
                    i = 50
                    ItemsPerPage = 50
                    Exit Select
            End Select
            Me.Pager.ItemsPerPage = ItemsPerPage
            clsProfilesPrivacySettings.Update_ItemsPerPage(Me.MasterProfileId, i)
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorSendEmail(ex, "")
        End Try
        Try
            ' If Me.Pager.PageIndex = -1 Then

            '  Else
            Me.Pager.PageIndex = -1
            LoadSavedSearch()
            '  End If

            '  LoadSavedSearch("", -1)

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub



    Protected Sub Pager_PageIndexChanged(sender As Object, e As EventArgs) Handles Pager.PageIndexChanged
        Try
          
            LoadSavedSearch()
            'Pager.
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub



    Sub SetPager(itemsCount As Integer)
        Me.Pager.ItemCount = itemsCount
        Me.Pager.ItemsPerPage = Me.ItemsPerPage
        Me.Pager.Visible = (itemsCount > Me.ItemsPerPage)

        If (Me.Pager.PageIndex = -1) AndAlso (itemsCount > 0) Then
            Me.Pager.PageIndex = 0
        End If
    End Sub

    Function GetRecordsToRetrieve() As Integer
        Me.ItemsPerPage = Me.Pager.ItemsPerPage

        If (Me.Pager.PageIndex = -1) AndAlso (Me.Pager.ItemCount > 0) Then
            Me.Pager.PageIndex = 0
        End If

        If (Me.Pager.PageIndex > 0) Then
            Return (Me.Pager.ItemsPerPage * (Me.Pager.PageIndex + 1))
        End If
        Return Me.Pager.ItemsPerPage
    End Function


    Dim txtMaxVotesstr As String = ""
    Private Sub FillOfferControlList(ByRef dt As DataTable, ByRef offersControl As Dating.Server.Site.Web.LoveHateControl, ByVal cLoves As Integer, ByVal cHates As Integer)
        Try
            Dim drDefaultPhoto As DSMembers.EUS_CustomerPhotosRow = DataHelpers.GetProfilesDefaultPhoto(Me.MasterProfileId)
            Dim uliCurrentMember As New clsLoveHateListItem()
            Dim crpr As vw_EusProfile_Light = Me.GetCurrentProfile
            uliCurrentMember.ProfileID = Me.MasterProfileId ' 
            uliCurrentMember.MirrorProfileID = Me.MirrorProfileId
            uliCurrentMember.LoginName = crpr.LoginName
            uliCurrentMember.Genderid = crpr.GenderId
            uliCurrentMember.ProfileViewUrl = ResolveUrl("~/Members/Profile.aspx?p=" & HttpUtility.UrlEncode(crpr.LoginName))
            uliCurrentMember.VoteCount = crpr.VotesInLast24Hour
            uliCurrentMember.LastDateTimeVoted = If(crpr.LastVoteDateTime Is Nothing, Date.UtcNow.AddDays(-2), crpr.LastVoteDateTime)
            If cLoves = 0 And cHates = 0 Then
                lblUserHasVotes1.Text = txtYouHaveNotBeenVoted
            Else
                If cLoves > 0 And cHates > 0 Then
                    lblUserHasVotes1.Text = txtYouHaveLoves.Replace("[LoveNum]", cLoves) & " " & txtYouHaveHates.Replace("[HateNum]", cHates)
                ElseIf cLoves > 0 Then
                    lblUserHasVotes1.Text = txtYouHaveLoves.Replace("[LoveNum]", cLoves)
                Else
                    lblUserHasVotes1.Text = txtYouHaveHates.Replace("[HateNum]", cHates)
                End If
            End If
            If Date.UtcNow < _clsVotesHelper.DatetimeEnds Then
                Dim MaxVotes As Integer = CInt(clsConfigValues.GetSYS_ConfigValueDouble2("LoveHateMaxVotesPerDay"))
                If uliCurrentMember.LastDateTimeVoted.AddDays(1) > Date.UtcNow Then
                    Try
                        If uliCurrentMember.VoteCount >= MaxVotes Then
                            uliCurrentMember.AllowVote = False

                            Dim pl As String = txtYouCannotVoteUntil.Replace("{Date}", uliCurrentMember.LastDateTimeVoted.AddDays(1).ToLocalTime.ToString("dd/MM, yyyy H:mm"))
                            pl = pl.Replace("{Date}", "")
                            lblLoveHateViewHeader.Text = pl
                        Else
                            txtMaxVotesstr = If(MaxVotes - uliCurrentMember.VoteCount = 1, txtHowManyVotes2, txtHowManyVotes.Replace("{voteCount}", MaxVotes - uliCurrentMember.VoteCount))
                            uliCurrentMember.AllowVote = True
                            lblLoveHateViewHeader.Text = txtMaxVotesstr
                        End If
                        ''  uliCurrentMember.AllowVote = If() Then
                    Catch ex As Exception
                        uliCurrentMember.AllowVote = False
                        uliCurrentMember.AllowVote = False
                        lblLoveHateViewHeader.Text = txtVoteHasFinished
                    End Try
                Else
                    clsUserDoes.ResetLoveHate(Me.MasterProfileId)
                    txtMaxVotesstr = If(MaxVotes - uliCurrentMember.VoteCount = 1, txtHowManyVotes2, txtHowManyVotes.Replace("{voteCount}", MaxVotes - uliCurrentMember.VoteCount))
                    uliCurrentMember.AllowVote = True
                    lblLoveHateViewHeader.Text = txtMaxVotesstr
                End If
            Else
                uliCurrentMember.AllowVote = False
                lblLoveHateViewHeader.Text = txtVoteHasFinished
            End If
            If (drDefaultPhoto IsNot Nothing) Then
                uliCurrentMember.ImageFileName = drDefaultPhoto.FileName
                uliCurrentMember.ImageUploadDate = drDefaultPhoto.DateTimeToUploading.ToLocalTime().ToString("dd/MM, yyyy H:mm")
            End If

            uliCurrentMember.ImageUrl = ProfileHelper.GetProfileImageURL(Me.MasterProfileId, uliCurrentMember.ImageFileName, Me.SessionVariables.MemberData.GenderId, True, Me.IsHTTPS, PhotoSize.D150)
            uliCurrentMember.ImageThumbUrl = uliCurrentMember.ImageUrl



            Dim mins As Integer = clsConfigValues.Get__members_online_minutes()
            Dim LastActivityUTCDate As DateTime = Date.UtcNow.AddMinutes(-mins)
            Dim hours As Integer = clsConfigValues.Get__members_online_recently_hours()
            Dim LastActivityRecentlyUTCDate As DateTime = Date.UtcNow.AddHours(-hours)
            Dim rowsCount As Integer
            For rowsCount = 0 To dt.Rows.Count - 1
                'If (rowsCount > endIndex) Then Exit For
                Try
                    Dim dr As DataRow = dt.Rows(rowsCount)
                    Dim uli As New clsLoveHateListItem()
                    uli.LAGID = Session("LAGID")
                    uli.ProfileID = uliCurrentMember.ProfileID
                    uli.MirrorProfileID = uliCurrentMember.MirrorProfileID
                    uli.LoginName = uliCurrentMember.LoginName
                    uli.Genderid = uliCurrentMember.Genderid
                    uli.ProfileViewUrl = uliCurrentMember.ProfileViewUrl
                    uli.ImageFileName = uliCurrentMember.ImageFileName
                    uli.ImageUrl = uliCurrentMember.ImageUrl
                    uli.ImageThumbUrl = uliCurrentMember.ImageThumbUrl
                    uli.ImageUploadDate = uliCurrentMember.ImageUploadDate
                    uli.AllowVote = uliCurrentMember.AllowVote
                    uli.DissallowVoteText = lblLoveHateViewHeader.Text
                    uli.PhotoOverlayCssClass = "PhotoOverlay"
                    If dr("HasPhoto") = True Then
                        If dr("HasBeenVotedHateFromCurrentProfile") = 0 And dr("HasBeenVotedLoveFromCurrentProfile") = 0 Then
                            If uliCurrentMember.AllowVote = False Then
                                uli.btnlnkLoveCssClass = "btn lnkLove DisVote"
                                uli.btnlnkHateCssClass = "btn lnkHate DisVote"
                            Else
                                uli.btnlnkLoveCssClass = "btn lnkLove NoVoted"
                                uli.btnlnkHateCssClass = "btn lnkHate NoVoted"
                            End If
                        Else
                            uli.AllowVote = False
                            If dr("HasBeenVotedLoveFromCurrentProfile") > 0 Then
                                uli.btnlnkLoveCssClass = "btn lnkLove Voted Enabled"

                                uli.btnlnkHateCssClass = "btn lnkHate Voted"


                                uli.PhotoOverlayCssClass = "prmOverlayLoveEnabled"
                            Else
                                uli.btnlnkLoveCssClass = "btn lnkLove Voted"
                                uli.btnlnkHateCssClass = "btn lnkHate Voted Enabled"
                                uli.PhotoOverlayCssClass = "prmOverlayHateEnabled"
                            End If
                        End If
                    Else
                        uli.btnlnkLoveCssClass = "btn lnkLove DisVote"
                        uli.btnlnkHateCssClass = "btn lnkHate DisVote"
                        uli.DissallowVoteText = txtVoteNoPicture
                        uli.AllowVote = False

                    End If
                    uli.OtherMemberLoginName = dr("LoginName")
                    uli.OtherMemberProfileID = dr("ProfileID")
                    uli.OtherMemberMirrorProfileID = dr("MirrorProfileID")

                    uli.OtherMemberGenderid = dr("Genderid")
                    uli.OtherProfileLoves = dr("Loves")
                    uli.OtherProfileHates = dr("Hates")
                    uli.OtherMemberIsFemale = ProfileHelper.IsFemale(uli.OtherMemberGenderid)
                    uli.OtherMemberIsOnline = clsNullable.DBNullToBoolean(dr("IsOnline"))
                    Dim __LastActivityDateTime As DateTime? = clsNullable.DBNullToDateTimeNull(dr("LastActivityDateTime"))

                    If (dt.Columns.Contains("IsOnlineNow")) Then
                        uli.OtherMemberIsOnline = clsNullable.DBNullToBoolean(dr("IsOnlineNow"))
                    Else
                        If (uli.OtherMemberIsOnline) Then
                            If (__LastActivityDateTime.HasValue) Then
                                uli.OtherMemberIsOnline = __LastActivityDateTime >= LastActivityUTCDate
                            Else
                                uli.OtherMemberIsOnline = False
                            End If
                        End If
                    End If

                    If (Not uli.OtherMemberIsOnline) Then
                        If (dt.Columns.Contains("IsOnlineRecently")) Then
                            'uli.OtherMemberIsOnlineRecently = IIf(Not dr.IsNull("IsOnlineRecently"), dr("IsOnlineRecently"), False)
                            uli.OtherMemberIsOnlineRecently = clsNullable.DBNullToBoolean(dr("IsOnlineRecently"))
                        Else
                            If (uli.OtherMemberIsOnline) Then
                                If (__LastActivityDateTime.HasValue) Then
                                    uli.OtherMemberIsOnlineRecently = __LastActivityDateTime >= LastActivityRecentlyUTCDate
                                Else
                                    uli.OtherMemberIsOnlineRecently = False
                                End If
                            End If
                        End If
                    End If

                    'is man and has credits
                    If (uli.OtherMemberGenderid = 1) Then
                        uli.OtherMemberIsVip = (clsNullable.DBNullToInteger(dr("AvailableCredits")) >= ProfileHelper.Config_UNLOCK_MESSAGE_READ_CREDITS)
                        uli.OtherMemberIsVip = uli.OtherMemberIsVip OrElse clsNullable.DBNullToBoolean(dr("HasSubscription"))
                    End If

                    uli.PhotoCssClass = "tt_enabled_LoveHatePhoto"
                    uli.PhotosCountString = globalStrings.GetCustomString("Member.Has.Photos.Count", uli.LAGID)
                    Try
                        If (clsNullable.DBNullToInteger(dr("PhotosApproved")) > 0) Then
                            uli.PhotosCountString = uli.PhotosCountString.Replace("[PHOTOS_COUNT]", dr("PhotosApproved"))
                        Else
                            uli.PhotosCountString = uli.PhotosCountString.Replace("[PHOTOS_COUNT]", 0)
                        End If
                    Catch ex As Exception
                        WebErrorSendEmail(ex, "PhotosCountString")
                    End Try



                    If (Not dr.IsNull("Birthday")) Then
                        uli.OtherMemberAge = ProfileHelper.GetCurrentAge(dr("Birthday"))
                    Else
                        uli.OtherMemberAge = 20
                    End If


                    If (Not dr.IsNull("FileName")) Then
                        uli.OtherMemberImageFileName = dr("FileName")
                        'ElseIf (dr("HasPhoto") = 1) Then
                        '    uli.OtherMemberImageFileName = "Private"
                        '    uli.OtherMemberImageUrl = ProfileHelper.GetPrivateImageURL(uli.OtherMemberGenderid)
                        '    uli.OtherMemberImageThumbUrl = ProfileHelper.GetPrivateImageURL(uli.OtherMemberGenderid)
                        'Else
                        '    uli.OtherMemberImageUrl = ProfileHelper.GetDefaultImageURL(uli.OtherMemberGenderid)
                        '    uli.OtherMemberImageThumbUrl = uli.OtherMemberImageUrl
                        '    uli.OtherMemberImageFileName = System.IO.Path.GetFileName(uli.OtherMemberImageUrl)
                    End If
                    'uli.OtherMemberImageUrl = ProfileHelper.GetProfileImageURL(dr("ProfileID"), uli.OtherMemberImageFileName, uli.OtherMemberGenderid, True, Me.IsHTTPS, PhotoSize.D150)
                    If (dt.Columns.Contains("HasPhoto")) Then
                        If (Not dr.IsNull("HasPhoto") AndAlso (dr("HasPhoto").ToString() = "1" OrElse dr("HasPhoto").ToString() = "True")) Then
                            ' user has photo
                            If (uli.OtherMemberImageFileName IsNot Nothing) Then
                                'has public photos
                                uli.OtherMemberImageUrl = ProfileHelper.GetProfileImageURL(uli.OtherMemberProfileID, uli.OtherMemberImageFileName, uli.OtherMemberGenderid, True, Me.IsHTTPS, PhotoSize.D150)

                            Else
                                'has private photos
                                uli.OtherMemberImageUrl = ProfileHelper.GetPrivateImageURL(uli.OtherMemberGenderid)

                            End If
                        Else
                            ' has no photo
                            uli.OtherMemberImageUrl = ProfileHelper.GetDefaultImageURL(uli.OtherMemberGenderid)

                        End If
                        If IsHTTPS AndAlso uli.OtherMemberImageUrl.StartsWith("http:") Then uli.OtherMemberImageUrl = "https" & uli.OtherMemberImageUrl.Remove(0, "http".Length)
                    Else
                        uli.OtherMemberImageUrl = ProfileHelper.GetProfileImageURL(uli.OtherMemberProfileID, uli.OtherMemberImageFileName, uli.OtherMemberGenderid, True, Me.IsHTTPS, PhotoSize.D150)
                    End If
                    uli.OtherMemberImageThumbUrl = uli.OtherMemberImageUrl


                    uli.OtherMemberProfileViewUrl = ResolveUrl("~/Members/Profile.aspx?p=" & HttpUtility.UrlEncode(dr("LoginName")))

                    uli.Distance = dr("distance")

                    If (uli.Distance < gCarDistance) Then
                        uli.DistanceCss = "distance_car"
                    Else
                        uli.DistanceCss = "distance_plane"
                    End If

                    offersControl.UsersList.Add(uli)
                    'offersControl.UsersList.Add(uli)

                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "")
                End Try

            Next
            '  = Me.UsersList

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Private Sub FillWinners(ByRef dt As DataTable, ByRef offersControl As Dating.Server.Site.Web.LoveHateWinnersControl)
        Try
            Dim drDefaultPhoto As DSMembers.EUS_CustomerPhotosRow = DataHelpers.GetProfilesDefaultPhoto(Me.MasterProfileId)
            Dim uliCurrentMember As New clsLoveHateListItem()
            Dim crpr As vw_EusProfile_Light = Me.GetCurrentProfile
            uliCurrentMember.ProfileID = Me.MasterProfileId ' 
            uliCurrentMember.MirrorProfileID = Me.MirrorProfileId
            uliCurrentMember.LoginName = crpr.LoginName
            uliCurrentMember.Genderid = crpr.GenderId
            uliCurrentMember.ProfileViewUrl = ResolveUrl("~/Members/Profile.aspx?p=" & HttpUtility.UrlEncode(crpr.LoginName))
            uliCurrentMember.VoteCount = crpr.VotesInLast24Hour
            uliCurrentMember.LastDateTimeVoted = If(crpr.LastVoteDateTime Is Nothing, Date.UtcNow.AddDays(-2), crpr.LastVoteDateTime)


            If (drDefaultPhoto IsNot Nothing) Then
                uliCurrentMember.ImageFileName = drDefaultPhoto.FileName
                uliCurrentMember.ImageUploadDate = drDefaultPhoto.DateTimeToUploading.ToLocalTime().ToString("dd/MM, yyyy H:mm")
            End If

            uliCurrentMember.ImageUrl = ProfileHelper.GetProfileImageURL(Me.MasterProfileId, uliCurrentMember.ImageFileName, Me.SessionVariables.MemberData.GenderId, True, Me.IsHTTPS, PhotoSize.D150)
            uliCurrentMember.ImageThumbUrl = uliCurrentMember.ImageUrl



            Dim mins As Integer = clsConfigValues.Get__members_online_minutes()
            Dim LastActivityUTCDate As DateTime = Date.UtcNow.AddMinutes(-mins)
            Dim hours As Integer = clsConfigValues.Get__members_online_recently_hours()
            Dim LastActivityRecentlyUTCDate As DateTime = Date.UtcNow.AddHours(-hours)
            Dim rowsCount As Integer
            For rowsCount = 0 To dt.Rows.Count - 1
                'If (rowsCount > endIndex) Then Exit For
                Try
                    Dim dr As DataRow = dt.Rows(rowsCount)
                    Dim uli As New clsLoveHateListItem()
                    uli.LAGID = Session("LAGID")
                    uli.ProfileID = uliCurrentMember.ProfileID
                    uli.MirrorProfileID = uliCurrentMember.MirrorProfileID
                    uli.LoginName = uliCurrentMember.LoginName
                    uli.Genderid = uliCurrentMember.Genderid
                    uli.ProfileViewUrl = uliCurrentMember.ProfileViewUrl
                    uli.ImageFileName = uliCurrentMember.ImageFileName
                    uli.ImageUrl = uliCurrentMember.ImageUrl
                    uli.ImageThumbUrl = uliCurrentMember.ImageThumbUrl
                    uli.ImageUploadDate = uliCurrentMember.ImageUploadDate
                    uli.OtherMemberLoginName = dr("LoginName")
                    uli.OtherMemberProfileID = dr("ProfileID")
                    uli.OtherMemberMirrorProfileID = dr("MirrorProfileID")
                    Dim pl As String = rowsCount + 1
                    Select Case pl
                        Case 1
                            uli.Place = CurrentPageData.GetCustomString("lblfirst")
                            Exit Select
                        Case 2
                            uli.Place = CurrentPageData.GetCustomString("lblsecond")
                            Exit Select
                        Case Else
                            uli.Place = CurrentPageData.GetCustomString("lblthird")
                            Exit Select
                    End Select

                    uli.OtherMemberGenderid = dr("Genderid")

                    uli.OtherMemberIsFemale = ProfileHelper.IsFemale(uli.OtherMemberGenderid)
                    uli.OtherMemberIsOnline = clsNullable.DBNullToBoolean(dr("IsOnline"))
                    Dim __LastActivityDateTime As DateTime? = clsNullable.DBNullToDateTimeNull(dr("LastActivityDateTime"))

                    If (dt.Columns.Contains("IsOnlineNow")) Then
                        uli.OtherMemberIsOnline = clsNullable.DBNullToBoolean(dr("IsOnlineNow"))
                    Else
                        If (uli.OtherMemberIsOnline) Then
                            If (__LastActivityDateTime.HasValue) Then
                                uli.OtherMemberIsOnline = __LastActivityDateTime >= LastActivityUTCDate
                            Else
                                uli.OtherMemberIsOnline = False
                            End If
                        End If
                    End If

                    If (Not uli.OtherMemberIsOnline) Then
                        If (dt.Columns.Contains("IsOnlineRecently")) Then
                            'uli.OtherMemberIsOnlineRecently = IIf(Not dr.IsNull("IsOnlineRecently"), dr("IsOnlineRecently"), False)
                            uli.OtherMemberIsOnlineRecently = clsNullable.DBNullToBoolean(dr("IsOnlineRecently"))
                        Else
                            If (uli.OtherMemberIsOnline) Then
                                If (__LastActivityDateTime.HasValue) Then
                                    uli.OtherMemberIsOnlineRecently = __LastActivityDateTime >= LastActivityRecentlyUTCDate
                                Else
                                    uli.OtherMemberIsOnlineRecently = False
                                End If
                            End If
                        End If
                    End If

                    'is man and has credits
                    If (uli.OtherMemberGenderid = 1) Then
                        uli.OtherMemberIsVip = (clsNullable.DBNullToInteger(dr("AvailableCredits")) >= ProfileHelper.Config_UNLOCK_MESSAGE_READ_CREDITS)
                        uli.OtherMemberIsVip = uli.OtherMemberIsVip OrElse clsNullable.DBNullToBoolean(dr("HasSubscription"))
                    End If

                    uli.PhotoCssClass = "tt_enabled_LoveHatePhoto"
                    uli.PhotosCountString = globalStrings.GetCustomString("Member.Has.Photos.Count", uli.LAGID)
                    Try
                        If (clsNullable.DBNullToInteger(dr("PhotosApproved")) > 0) Then
                            uli.PhotosCountString = uli.PhotosCountString.Replace("[PHOTOS_COUNT]", dr("PhotosApproved"))
                        Else
                            uli.PhotosCountString = uli.PhotosCountString.Replace("[PHOTOS_COUNT]", 0)
                        End If
                    Catch ex As Exception
                        WebErrorSendEmail(ex, "PhotosCountString")
                    End Try



                    If (Not dr.IsNull("Birthday")) Then
                        uli.OtherMemberAge = ProfileHelper.GetCurrentAge(dr("Birthday"))
                    Else
                        uli.OtherMemberAge = 20
                    End If


                    If (Not dr.IsNull("FileName")) Then
                        uli.OtherMemberImageFileName = dr("FileName")
                    End If
                    'uli.OtherMemberImageUrl = ProfileHelper.GetProfileImageURL(dr("ProfileID"), uli.OtherMemberImageFileName, uli.OtherMemberGenderid, True, Me.IsHTTPS, PhotoSize.D150)
                    If (dt.Columns.Contains("HasPhoto")) Then
                        If (Not dr.IsNull("HasPhoto") AndAlso (dr("HasPhoto").ToString() = "1" OrElse dr("HasPhoto").ToString() = "True")) Then
                            ' user has photo
                            If (uli.OtherMemberImageFileName IsNot Nothing) Then
                                'has public photos
                                uli.OtherMemberImageUrl = ProfileHelper.GetProfileImageURL(uli.OtherMemberProfileID, uli.OtherMemberImageFileName, uli.OtherMemberGenderid, True, Me.IsHTTPS, PhotoSize.D150)

                            Else
                                'has private photos
                                uli.OtherMemberImageUrl = ProfileHelper.GetPrivateImageURL(uli.OtherMemberGenderid)

                            End If
                        Else
                            ' has no photo
                            uli.OtherMemberImageUrl = ProfileHelper.GetDefaultImageURL(uli.OtherMemberGenderid)

                        End If
                        If IsHTTPS AndAlso uli.OtherMemberImageUrl.StartsWith("http:") Then uli.OtherMemberImageUrl = "https" & uli.OtherMemberImageUrl.Remove(0, "http".Length)
                    Else
                        uli.OtherMemberImageUrl = ProfileHelper.GetProfileImageURL(uli.OtherMemberProfileID, uli.OtherMemberImageFileName, uli.OtherMemberGenderid, True, Me.IsHTTPS, PhotoSize.D150)
                    End If
                    uli.OtherMemberImageThumbUrl = uli.OtherMemberImageUrl


                    uli.OtherMemberProfileViewUrl = ResolveUrl("~/Members/Profile.aspx?p=" & HttpUtility.UrlEncode(dr("LoginName")))



                    If (uli.Distance < gCarDistance) Then
                        uli.DistanceCss = "distance_car"
                    Else
                        uli.DistanceCss = "distance_plane"
                    End If

                    offersControl.UsersList.Add(uli)
                    'offersControl.UsersList.Add(uli)

                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "")
                End Try

            Next
            '  = Me.UsersList

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Private Property isNewSearch As Boolean = False
    Private Sub LoadSavedSearch()
        Try
           
            Dim username As String = ""
            Try
                username = txtUserNameQ.Text
            Catch ex As Exception

            End Try
           
            If _clsVotesHelper Is Nothing Then Return
            Try
                If _clsVotesHelper.HasPrevious Then
                    Dim ds As DataSet = _clsVotesHelper.GetWinners
                    If ds IsNot Nothing OrElse ds.Tables.Count > 0 Then
                        Dim dt2 As DataTable = ds.Tables(0)
                        LoveHateWinners.Visible = True
                        LoveHateWinners._TopWinnersLabel = If(IsMale, CurrentPageData.GetCustomString("lblTopWinnersWomen"), CurrentPageData.GetCustomString("lblTopWinnersMen"))
                        FillWinners(dt2, LoveHateWinners)
                        LoveHateWinners.DataBind()
                    Else
                        LoveHateWinners.Visible = False
                    End If
                Else
                    LoveHateWinners.Visible = False
                End If
            Catch ex As Exception

            End Try

            If username <> "" Then
                Try
                    '   Me.LastAdvancedSearchquery = sql
                    Dim NumberOfRecordsToReturn As Integer = 0
                    If isNewSearch Then
                        Dim i As Integer = clsProfilesPrivacySettings.GET_ItemsPerPage(Me.MasterProfileId)
                        Me.ItemsPerPage = If(i <= 0, 25, i)
                        NumberOfRecordsToReturn = ItemsPerPage

                    Else
                        NumberOfRecordsToReturn = Me.GetRecordsToRetrieve()
                    End If

                    Dim prms As New clsLoveHelperParameters()
                    prms.UserName = username
                    prms.CurrentProfileId = Me.MasterProfileId
                    prms.ReturnRecordsWithStatus = ProfileStatusEnum.Approved
                    prms.SearchSort = SearchSortEnum.NewestNearMember
                    prms.zipstr = Me.SessionVariables.MemberData.Zip
                    prms.latitudeIn = Me.SessionVariables.MemberData.latitude
                    prms.longitudeIn = Me.SessionVariables.MemberData.longitude
                    '  prms.Distance=
                    prms.LookingFor_ToMeetMaleID = Me.SessionVariables.MemberData.LookingFor_ToMeetMaleID
                    prms.LookingFor_ToMeetFemaleID = Me.SessionVariables.MemberData.LookingFor_ToMeetFemaleID
                    prms.NumberOfRecordsToReturn = 0 'Me.GetRecordsToRetrieve()
                    'prms.AdditionalWhereClause = sql
                    prms.performCount = True
                    prms.rowNumberMin = NumberOfRecordsToReturn - Me.ItemsPerPage
                    prms.rowNumberMax = NumberOfRecordsToReturn
                    refreshStrings()
                    Dim countRows As Integer = 0
                    If _clsVotesHelper.HasCurrent = True Then
                        searchform2.Visible = True
                        prms.DayTimeStarts = _clsVotesHelper.DatetimeStarts
                        prms.DateTimeEnds = _clsVotesHelper.DatetimeEnds
                        Dim ds As DataSet = GetMembersToSearchforUserName(prms)
                        Dim loves As Integer = 0
                        Dim hates As Integer = 0
                        Try
                            loves = ds.Tables(2).Rows(0)(0)
                            hates = ds.Tables(2).Rows(0)(1)
                        Catch ex As Exception
                        End Try
                        If (prms.performCount) Then
                            countRows = ds.Tables(0).Rows(0)(0)
                            If (countRows > 0) Then
                                lblNoresults.Visible = False

                                Dim dt2 As DataTable = ds.Tables(1)
                                FillOfferControlList(dt2, searchL, loves, hates)
                            Else
                                lblNoresults.Visible = True
                                lblNoresults.Text = txtnoResults
                            End If
                        Else
                            countRows = ds.Tables(0).Rows.Count
                            If (countRows > 0) Then
                                Dim dt2 As DataTable = ds.Tables(0)
                                FillOfferControlList(dt2, searchL, loves, hates)
                            End If
                        End If
                    Else
                        searchform2.Visible = False
                        lblLoveHateViewHeader.Text = txtVoteHasFinished
                    End If
                    searchL.DataBind()
                    If countRows >= 200 Then
                        SetPager(200)
                    Else
                        SetPager(countRows)
                    End If



                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "")

                    SetPager(0)
                    searchL.UsersList = Nothing
                    searchL.DataBind()
                End Try
            Else


                Try
                    '   Me.LastAdvancedSearchquery = sql
                    Dim NumberOfRecordsToReturn As Integer = Me.GetRecordsToRetrieve()
                    Dim prms As New clsLoveHelperParameters()
                    prms.CurrentProfileId = Me.MasterProfileId
                    prms.ReturnRecordsWithStatus = ProfileStatusEnum.Approved
                    prms.SearchSort = SearchSortEnum.NewestNearMember
                    prms.zipstr = Me.SessionVariables.MemberData.Zip
                    prms.latitudeIn = Me.SessionVariables.MemberData.latitude
                    prms.longitudeIn = Me.SessionVariables.MemberData.longitude
                    '  prms.Distance=
                    prms.LookingFor_ToMeetMaleID = Me.SessionVariables.MemberData.LookingFor_ToMeetMaleID
                    prms.LookingFor_ToMeetFemaleID = Me.SessionVariables.MemberData.LookingFor_ToMeetFemaleID
                    prms.NumberOfRecordsToReturn = 0 'Me.GetRecordsToRetrieve()
                    'prms.AdditionalWhereClause = sql
                    prms.performCount = True
                    prms.rowNumberMin = NumberOfRecordsToReturn - Me.ItemsPerPage
                    prms.rowNumberMax = NumberOfRecordsToReturn
                    refreshStrings()
                    Dim countRows As Integer = 0

                    If _clsVotesHelper.HasCurrent = True Then
                        searchform2.Visible = True
                        prms.DayTimeStarts = _clsVotesHelper.DatetimeStarts
                        prms.DateTimeEnds = _clsVotesHelper.DatetimeEnds
                        Dim ds As DataSet = GetMembersToSearchDataTable1(prms)
                        Dim loves As Integer = 0
                        Dim hates As Integer = 0
                        Try
                            loves = ds.Tables(2).Rows(0)(0)
                            hates = ds.Tables(2).Rows(0)(1)
                        Catch ex As Exception
                        End Try
                        If (prms.performCount) Then
                            countRows = ds.Tables(0).Rows(0)(0)
                            If (countRows > 0) Then
                                lblNoresults.Visible = False
                                Dim dt2 As DataTable = ds.Tables(1)
                                FillOfferControlList(dt2, searchL, loves, hates)
                            Else
                                lblNoresults.Visible = True
                                lblNoresults.Text = txtnoResults
                            End If
                        Else
                            countRows = ds.Tables(0).Rows.Count
                            If (countRows > 0) Then
                                Dim dt2 As DataTable = ds.Tables(0)
                                FillOfferControlList(dt2, searchL, loves, hates)
                            End If
                        End If
                    Else
                        searchform2.Visible = False
                        lblLoveHateViewHeader.Text = txtVoteHasFinished
                    End If
                    searchL.DataBind()
                    If countRows >= 200 Then
                        SetPager(200)
                    Else
                        SetPager(countRows)
                    End If



                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "")

                    SetPager(0)
                    searchL.UsersList = Nothing
                    searchL.DataBind()
                End Try
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub
    Public Shared Function GetListSubquery(sql As String, cb As CheckBoxList, dbField As String) As String

        Dim cbItemsAll As Boolean = True
        Dim cbItems As String = ""
        For Each li As ListItem In cb.Items
            If (li.Selected) Then
                cbItems = cbItems & li.Value & ","
            End If
            If (Not li.Selected) Then cbItemsAll = False
        Next
        If (cbItemsAll) Then
            If (cbItems <> "") Then
                cbItems = cbItems.TrimEnd(",")
                sql = sql & vbCrLf & " and  (" & dbField & " in  (" & cbItems & ") or " & dbField & "=-1  or " & dbField & " is null)"
            End If
        Else
            If (cbItems <> "") Then
                cbItems = cbItems.TrimEnd(",")
                sql = sql & vbCrLf & " and  " & dbField & " in  (" & cbItems & ")"
            End If
        End If

        Return sql
    End Function

    Public Shared Function GetListSubquery(Of T)(cb As List(Of T)) As String
        Dim sql As String = ""
        For cnt = 0 To cb.Count - 1
            If (cnt = cb.Count - 1) Then
                sql = sql & cb(cnt).ToString()
            Else
                sql = sql & cb(cnt).ToString() & ","
            End If
        Next
        Return sql
    End Function




    Private Const DISTANCE_DEFAULT = 1000000
    Private _clsVotesHelper1 As clsVotesHelper
    Private ReadOnly Property _clsVotesHelper As clsVotesHelper
        Get
            If _clsVotesHelper1 Is Nothing Then
                Try
                    _clsVotesHelper1 = New clsVotesHelper
                    _clsVotesHelper1.IsMale = Me.IsMale
                    _clsVotesHelper1.CurrentProfileId = Me.MasterProfileId
                Catch ex As Exception
                End Try
            End If
            Return _clsVotesHelper1
        End Get
    End Property


    'Private _dt As DateTime?
    'Private _dt1 As DateTime?
    'Private ReadOnly Property DateTimeItStarts As DateTime
    '    Get
    '        Try


    '        If _dt Is Nothing Then
    '                _dt = DateTime.Parse(clsConfigValues.GetSYS_ConfigValue("LoveHateVoteStartDay"))
    '            End If
    '            _
    '        Catch ex As Exception
    '            _dt = DateTime.UtcNow.AddYears(1)
    '        End Try
    '        Return _dt
    '    End Get

    'End Property
    'Private ReadOnly Property DateTimeItEnds As DateTime
    '    Get
    '        Try
    '            If _dt1 Is Nothing Then
    '                _dt1 = DateTime.Parse(clsConfigValues.GetSYS_ConfigValue("LoveHateVoteEndDay"))
    '            End If
    '        Catch ex As Exception
    '            _dt1 = DateTime.UtcNow.AddYears(1)
    '        End Try
    '        Return _dt1
    '    End Get

    'End Property
   
    Private Shared Function GetMembersToSearchDataTable1(prms As clsLoveHelperParameters) As DataSet

        'clsLogger.InsertLog("GetMembersToSearchDataTable", prms.CurrentProfileId)
        Dim __logdate As DateTime = DateTime.UtcNow

        Dim CurrentProfileId As Integer = prms.CurrentProfileId
        'Dim SearchSort As SearchSortEnum = prms.SearchSort
        'Dim zipstr As String = prms.zipstr
        Dim Distance As Integer = prms.Distance
        '  Dim LookingFor_ToMeetMaleID As Boolean = prms.LookingFor_ToMeetMaleID
        Dim LookingFor_ToMeetFemaleID As Boolean = prms.LookingFor_ToMeetFemaleID
        Dim NumberOfRecordsToReturn As Integer = prms.NumberOfRecordsToReturn
        'Dim AdditionalWhereClause As String = prms.AdditionalWhereClause


        If (Distance = 0) Then
            Distance = DISTANCE_DEFAULT
        End If

        Dim sql As String = "", sqlCount As String = "", sqlResults As String = ""


        'This script is based on kilometer. If you use miles simply convert the radius to kilometer (radius=radius*1.609344)
        Try

            sqlCount = <sql><![CDATA[
if(@performCount=1)
begin

    select Count(*) from (
	    SELECT    
		    distance=case 
                when @latitudeIn is null or EUS_Profiles.latitude is null or @longitudeIn is null or EUS_Profiles.longitude is null then 1000
                else cast(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)) as int)
            end
	    FROM		
            dbo.EUS_Profiles AS EUS_Profiles 
		    with (nolock)
	    --LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerPhotosID = EUS_Profiles.DefPhotoID AND phot.CustomerID = EUS_Profiles.ProfileID
		--LEFT OUTER JOIN dbo.EUS_ProfilesPrivacySettings AS pps ON (pps.ProfileID=EUS_Profiles.ProfileID) and (pps.HideMeFromUsersInDifferentCountry=1)
        WHERE
		    EUS_Profiles.IsMaster=1
	    AND EUS_Profiles.Profileid>1
	    AND EUS_Profiles.ProfileID<>@CurrentProfileId --AND EUS_Profiles.ProfileID<>@MirrorProfileID
	    AND EUS_Profiles.Status = @ReturnRecordsWithStatus
	    AND (EUS_Profiles.PrivacySettings_HideMeFromSearchResults is null or EUS_Profiles.PrivacySettings_HideMeFromSearchResults=0)
	    AND (
			    (@LookingFor_ToMeetMaleID= 1    AND EUS_Profiles.GenderId = 1)
		    OR
			    (@LookingFor_ToMeetFemaleID = 1 AND EUS_Profiles.GenderId = 2)
	    )
	    AND  not exists(select * 
						from EUS_ProfilesBlocked bl 
                        with (nolock) 
						where (bl.FromProfileID =EUS_Profiles.ProfileID and bl.ToProfileID = @CurrentProfileId) or
							(bl.FromProfileID =@CurrentProfileId and bl.ToProfileID = EUS_Profiles.ProfileID)
	    )
	    AND (  
            EUS_Profiles.Country = @Country OR 
            (
                EUS_Profiles.Country <> @Country AND
                not exists(select * 
					        from EUS_ProfilesPrivacySettings bl 
                            with (nolock) 
					        where (bl.ProfileID =EUS_Profiles.ProfileID) and (HideMeFromUsersInDifferentCountry=1)
                )
            )
        )
[AdditionalWhereClause]
    ) as EUS_Profiles
    where 
      ((distance <= @Distance and @Distance <@DISTANCE_DEFAULT)  OR ((distance <= @DISTANCE_DEFAULT) and @Distance =@DISTANCE_DEFAULT))
end
]]></sql>.Value


            sqlResults = <sql><![CDATA[



select 
	tOutter.*
    ,CASE 
		/*WHEN ISNULL(tOutter.Birthday, '') = '' then null
		ELSE dbo.fn_GetAge(tOutter.Birthday, GETDATE())*/
        WHEN not tOutter.Birthday is null then dbo.fn_GetAge(tOutter.Birthday, GETDATE())
		ELSE null
	End as Age
    ,IsOnlineNow=CAST((CASE
            WHEN pps.[PrivacySettings_ShowMeOffline]=1 THEN 0
            WHEN not @LastActivityUTCDate is null and (tOutter.IsOnline=1 and tOutter.LastActivityDateTime>=@LastActivityUTCDate) THEN 1
            ELSE 0
        END) as bit)
    ,IsOnlineRecently=CAST((CASE
            WHEN pps.[PrivacySettings_ShowMeOffline]=1 THEN 0
            WHEN not @LastActivityUTCDate is null and (tOutter.IsOnline=1 and tOutter.LastActivityDateTime>=@LastActivityRecentlyUTCDate) THEN 1
            ELSE 0
        END) as bit)
  /*  ,HasFavoritedMe = case 
		when (exists (select *
						from 	dbo.EUS_ProfilesFavorite favMe
						with (nolock)
						where   favMe.ToProfileID = @CurrentProfileId AND favMe.FromProfileID = tOutter.ProfileID))
        then 1
        else 0
    end*/
   /* ,DidIFavorited = case 
		when (exists (select *
                        from 	dbo.EUS_ProfilesFavorite favMe
                        with (nolock)
                        where   favMe.ToProfileID = tOutter.ProfileID AND favMe.FromProfileID = @CurrentProfileId))
        then 1
        else 0
    end
	,CommunicationUnl = case 
		when (exists (
				select *
				from 	dbo.EUS_UnlockedConversations unl
				with (nolock)
				where  @CurrentProfileId<=8911
				and  tOutter.ProfileID<=8911
				and (( unl.FromProfileID = @CurrentProfileId AND unl.ToProfileID = tOutter.ProfileID) OR
					( unl.FromProfileID = tOutter.ProfileID  AND unl.ToProfileID = @CurrentProfileId))
		))
		then 1
		else 0
	end
    ,WantJob = case 
        when tOutter.GenderID=2 then (select top(1) lt.JobID  from dbo.EUS_ProfileJobs pj inner join dbo.EUS_LISTS_Job lt on lt.JobID=pj.JobID where ProfileID=tOutter.ProfileID)
        else null
    end
    ,BreastSizeID = case 
        when tOutter.GenderID=2 then (select top(1) b.BreastSizeID from dbo.EUS_ProfileBreastSize b where b.ProfileID=tOutter.ProfileID)
        else null
    end
    ,IsAnyWinkOrIsAnyOffer = (
        SELECT top(1) [EUS_Offers].OfferID 
		FROM [dbo].[EUS_Offers]
		WHERE (([ToProfileID]=tOutter.ProfileID AND [FromProfileID]=@CurrentProfileId) OR ([ToProfileID]=@CurrentProfileId  AND [FromProfileID]=tOutter.ProfileID))
		AND ([OfferTypeID]=1 OR [OfferTypeID]=10  OR [OfferTypeID]= 11)
		--WINK,OFFERNEW,OFFERCOUNTER
	)
    ,IsAnyLastOffer = (SELECT TOP (1) [OfferID]
        FROM [dbo].[EUS_Offers] with (nolock)
        WHERE (([ToProfileID]=tOutter.ProfileID AND [FromProfileID]=@CurrentProfileId) OR ([ToProfileID]=@CurrentProfileId AND [FromProfileID]=tOutter.ProfileID)) 
        AND ([OfferTypeID]=10 OR [OfferTypeID]=11) 
        AND ([ChildOfferID]=0)
        --10=OFFERNEW, 11=OFFERCOUNTER
	)
    ,ProfilesCommunication_IsAnySentMessage = (
		SELECT TOP (1) [SentMessageID]
		FROM [dbo].[EUS_ProfilesCommunication]
		WHERE not [SentMessageID] is null 
		and [SentMessageID] > 0 
		and (([FromProfileID] = tOutter.ProfileID AND [ToProfileID] = @CurrentProfileId) or 
				([FromProfileID] = @CurrentProfileId AND [ToProfileID] = tOutter.ProfileID))
	)
    ,ProfilesCommunication_IsAnySentLike = (
		SELECT TOP (1) [SentLikeID]
		FROM [dbo].[EUS_ProfilesCommunication]
		WHERE not [SentLikeID] is null 
		and [SentLikeID] > 0 
		and (([FromProfileID] = tOutter.ProfileID AND [ToProfileID] = @CurrentProfileId) or 
				([FromProfileID] = @CurrentProfileId AND [ToProfileID] = tOutter.ProfileID))
	)
    ,ProfilesCommunication_IsAnySentOffer = (
		SELECT TOP (1) [SentOfferID]
		FROM [dbo].[EUS_ProfilesCommunication]
		WHERE not [SentOfferID] is null 
		and [SentOfferID] > 0 
		and (([FromProfileID] = tOutter.ProfileID AND [ToProfileID] = @CurrentProfileId) or 
				([FromProfileID] = @CurrentProfileId AND [ToProfileID] = tOutter.ProfileID))
	)*/
    ,HasSubscription = CAST((case
        when tOutter.GenderId=1 and exists (select CustomerCreditsId from EUS_CustomerCredits  cc  where cc.CustomerId = tOutter.ProfileID and cc.IsSubscription=1 and cc.DateTimeExpiration>getutcdate()) 
        then 1
        else 0
    end) as bit),
Loves=isnull((select sum([Loves]) from (
SELECT  count([Love]) as Loves FROM [dbo].[EUS_ProfilesLoves] with (nolock) where (ToProfileId=tOutter.ProfileID 
or ToProfileID=tOutter.MirrorProfileID) and Love=1 and (ActionDatetime>=@ThisVoteDateStartTime and ActionDatetime<@ThisVoteDateEndTime)
group by ToProfileId) as t),0),
Hates= isnull((select sum([Hates]) from (
SELECT  count([Hate]) as Hates FROM [dbo].[EUS_ProfilesLoves] with (nolock) where (ToProfileId=tOutter.ProfileID 
or ToProfileID=tOutter.MirrorProfileID) and Hate=1 and (ActionDatetime>=@ThisVoteDateStartTime and ActionDatetime<@ThisVoteDateEndTime)
group by ToProfileId) as t1),0),
HasBeenVotedLoveFromCurrentProfile=isnull((Select count([ProfilesLovesID]) as AllowVote 
from [dbo].[EUS_ProfilesLoves] with (nolock)
where (FromProfileID=@CurrentProfileId or FromProfileID=@MirrorProfileID)	
		and  (ToProfileId=tOutter.ProfileID or ToProfileID=tOutter.MirrorProfileID)
		and [Love]=1 
		and [ActionDatetime]>=@ThisVoteDateStartTime and ActionDatetime<@ThisVoteDateEndTime),0),
HasBeenVotedHateFromCurrentProfile=isnull((Select count([ProfilesLovesID]) as AllowVote 
from [dbo].[EUS_ProfilesLoves] with (nolock)
where (FromProfileID=@CurrentProfileId or FromProfileID=@MirrorProfileID)	
		and  (ToProfileId=tOutter.ProfileID or ToProfileID=tOutter.MirrorProfileID)
		and [Hate]=1 
		and [ActionDatetime]>=@ThisVoteDateStartTime),0),
cast(case 
			        when (tOutter.[HasPhoto1] > 0  )  then 1
			        else 0  end as bit) as HasPhoto

from (
    select  [TOP]
	    RowNumber = Row_Number() over([QueryOrdering])
	    ,tInner.*
    from(
        select 
            [SELECT-LIST]
        from (
	        SELECT    
		        distance=case 
                    when @latitudeIn is null or EUS_Profiles.latitude is null or @longitudeIn is null or EUS_Profiles.longitude is null then 1000
                    else cast(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)) as int)
                end
  		        ,minutes1=datediff(minute,@NewestNearMemberDateStart,EUS_Profiles.DateTimeToRegister),
                EUS_Profiles.PointsBeauty,    
		        EUS_Profiles.PointsVerification,     
		        EUS_Profiles.PointsCredits,    
                EUS_Profiles.PointsUnlocks,
                EUS_Profiles.ProfileID, 
                EUS_Profiles.IsMaster, 
                EUS_Profiles.MirrorProfileID, 
                EUS_Profiles.Status,
                EUS_Profiles.LoginName, 
                EUS_Profiles.FirstName,
                EUS_Profiles.LastName, 
                EUS_Profiles.GenderId, 
               /* EUS_Profiles.Country, 
                EUS_Profiles.Region, 
                EUS_Profiles.City, 
                EUS_Profiles.Zip, 
                EUS_Profiles.CityArea, 
                EUS_Profiles.Address, 
                EUS_Profiles.Telephone, 
                EUS_Profiles.eMail, 
                EUS_Profiles.Cellular, 
                EUS_Profiles.AreYouWillingToTravel, 
                EUS_Profiles.AboutMe_Heading, 
                EUS_Profiles.AboutMe_DescribeYourself, 
                EUS_Profiles.AboutMe_DescribeAnIdealFirstDate, 
                EUS_Profiles.OtherDetails_EducationID, 
                EUS_Profiles.OtherDetails_AnnualIncomeID, 
                EUS_Profiles.OtherDetails_NetWorthID, 
                EUS_Profiles.OtherDetails_Occupation, 
                EUS_Profiles.PersonalInfo_HeightID, 
                EUS_Profiles.PersonalInfo_BodyTypeID, 
                EUS_Profiles.PersonalInfo_EyeColorID, 
                EUS_Profiles.PersonalInfo_HairColorID, 
                EUS_Profiles.PersonalInfo_ChildrenID, 
                EUS_Profiles.PersonalInfo_EthnicityID,
                EUS_Profiles.PersonalInfo_ReligionID, 
                EUS_Profiles.PersonalInfo_SmokingHabitID, 
                EUS_Profiles.PersonalInfo_DrinkingHabitID, 
                EUS_Profiles.LookingFor_ToMeetMaleID, 
                EUS_Profiles.LookingFor_ToMeetFemaleID, EUS_Profiles.LookingFor_RelationshipStatusID, 
                EUS_Profiles.LookingFor_TypeOfDating_ShortTermRelationship, 
                EUS_Profiles.LookingFor_TypeOfDating_Friendship,
                EUS_Profiles.LookingFor_TypeOfDating_LongTermRelationship, 
                EUS_Profiles.LookingFor_TypeOfDating_MutuallyBeneficialArrangements, 
                EUS_Profiles.LookingFor_TypeOfDating_MarriedDating, 
                EUS_Profiles.LookingFor_TypeOfDating_AdultDating_Casual, */
                EUS_Profiles.DateTimeToRegister, 
               /* EUS_Profiles.RegisterGEOInfos, 
                EUS_Profiles.LastLoginDateTime, 
                EUS_Profiles.LastLoginGEOInfos, */
                EUS_Profiles.LastUpdateProfileDateTime, 
                EUS_Profiles.LastUpdateProfileGEOInfo, 
                EUS_Profiles.LAGID, 
                EUS_Profiles.CustomReferrer, 
                EUS_Profiles.Birthday, 
                EUS_Profiles.IsOnline, 
                EUS_Profiles.LastActivityDateTime, 
                EUS_Profiles.AvailableCredits, 
                EUS_Profiles.DefPhotoID,
                EUS_Profiles.PhotosApproved, 
EUS_Profiles.[VotesInLast24Hour],
      EUS_Profiles.[LastVoteDateTime],
               /* EUS_Profiles.CelebratingBirth, */
 [HasPhoto1] = 	isnull((select count([customerphotosid])
					 from [dbo].[eus_customerphotos] with (nolock)
					where ([CustomerID] in (EUS_Profiles.MirrorProfileID ,EUS_Profiles.ProfileID))
						and [HasAproved]=1 and [IsDeleted]=0 and [DisplayLevel]=0),0),
                phot.CustomerPhotosID, phot.CustomerID, 
                phot.DateTimeToUploading, phot.FileName, 
                phot.DisplayLevel, phot.HasAproved, phot.HasDeclined, phot.IsDefault 
              		-- HasPhoto=isnull((SELECT count([CustomerPhotosID])
						  -- FROM [dbo].[EUS_CustomerPhotos] with (nolock)
						 -- where ([CustomerID]=EUS_Profiles.MirrorProfileID or [CustomerID]=EUS_Profiles.ProfileID)
							-- and [HasAproved]=1 and [IsDeleted]=0 and [DisplayLevel]=0),0)
	        FROM		
                dbo.EUS_Profiles AS EUS_Profiles 
		        with (nolock)
	        LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot WITH (NOLOCK) ON phot.CustomerPhotosID = EUS_Profiles.DefPhotoID AND  phot.CustomerID = EUS_Profiles.ProfileID
            WHERE

		        EUS_Profiles.IsMaster=1
	        AND EUS_Profiles.Profileid>1
	        AND EUS_Profiles.ProfileID<>@CurrentProfileId --AND EUS_Profiles.ProfileID<>@MirrorProfileID
	        AND EUS_Profiles.Status = @ReturnRecordsWithStatus
            

	        AND (EUS_Profiles.PrivacySettings_HideMeFromSearchResults is null or EUS_Profiles.PrivacySettings_HideMeFromSearchResults=0)
	        AND (
			        (@LookingFor_ToMeetMaleID= 1    AND EUS_Profiles.GenderId = 1)
		        OR
			        (@LookingFor_ToMeetFemaleID = 1 AND EUS_Profiles.GenderId = 2)
	        )
	        AND  not exists(select * 
						    from EUS_ProfilesBlocked bl 
                            with (nolock) 
						    where (bl.FromProfileID =EUS_Profiles.ProfileID and bl.ToProfileID = @CurrentProfileId) or
							    (bl.FromProfileID =@CurrentProfileId and bl.ToProfileID = EUS_Profiles.ProfileID)
	        )
	        AND (  
                EUS_Profiles.Country = @Country OR 
                (
                    EUS_Profiles.Country <> @Country AND
                    not exists(select * 
					            from EUS_ProfilesPrivacySettings bl 
                                with (nolock) 
					            where (bl.ProfileID =EUS_Profiles.ProfileID) and (HideMeFromUsersInDifferentCountry=1)
                    )
                )
            ) 
        [AdditionalWhereClause]
        ) as EUS_Profiles
        where 
          ((distance <= @Distance and @Distance <@DISTANCE_DEFAULT)  OR ((distance <= @DISTANCE_DEFAULT) and @Distance =@DISTANCE_DEFAULT)) and HasPhoto1>0
    ) as tInner
) as tOutter
left join [EUS_ProfilesPrivacySettings] pps WITH (NOLOCK) on (pps.ProfileID=tOutter.ProfileID OR pps.[MirrorProfileID]=tOutter.ProfileID)
where 
	(tOutter.RowNumber > @RowNumberMin) and
	(tOutter.RowNumber <= @RowNumberMax) 
order by tOutter.RowNumber

]]></sql>.Value


            sql = <sql><![CDATA[

--fn:GetMembersToSearchDataTable
	--@CurrentProfileId int
	--,@ReturnRecordsWithStatus int
	--,@NumberOfRecordsToReturn int =0
	--,@Distance int=0

	SET NOCOUNT ON;

declare @Country nvarchar(10),
@LookingFor_ToMeetMaleID bit,
@LookingFor_ToMeetFemaleID bit,
@MirrorProfileID int

select 
    @MirrorProfileID=MirrorProfileID,
    @Country=Country, 
    @LookingFor_ToMeetMaleID=LookingFor_ToMeetMaleID, 
    @LookingFor_ToMeetFemaleID=LookingFor_ToMeetFemaleID 
from Eus_Profiles WITH (NOLOCK)
where ProfileID=@CurrentProfileId

[SQL_COUNT]
[SQL_RESULTS]
[SQL_USERLoves]
]]></sql>.Value

            Dim SQL_USERLoves As String = <sql><![CDATA[

   select isnull(sum(cast([Love] as int)),0)as loves,isnull(sum(cast([Hate]as int)),0) as hates
 from [dbo].[EUS_ProfilesLoves]
 where ([ToProfileID] =@CurrentProfileId or [ToProfileID] =@MirrorProfileID) and 
 (ActionDatetime>=@ThisVoteDateStartTime and ActionDatetime<@ThisVoteDateEndTime)

]]></sql>.Value

            '2014-09-12 00:00:00.000
            sqlResults = sqlResults.Replace("@ThisVoteDateStartTime", "'" & prms.DayTimeStarts.ToString("yyyy-MM-dd HH:mm:ss.fff") & "'")
            sqlResults = sqlResults.Replace("@ThisVoteDateEndTime", "'" & prms.DateTimeEnds.ToString("yyyy-MM-dd HH:mm:ss.fff") & "'")
            SQL_USERLoves = SQL_USERLoves.Replace("@ThisVoteDateEndTime", "'" & prms.DateTimeEnds.ToString("yyyy-MM-dd HH:mm:ss.fff") & "'")
            SQL_USERLoves = SQL_USERLoves.Replace("@ThisVoteDateStartTime", "'" & prms.DayTimeStarts.ToString("yyyy-MM-dd HH:mm:ss.fff") & "'")
            sql = sql.Replace("[SQL_USERLoves]", SQL_USERLoves)
            If (prms.performCount) Then
                sql = sql.Replace("[SQL_COUNT]", sqlCount)
                sql = sql.Replace("--fn:GetMembersToSearchDataTable", "--fn:GetMembersToSearchDataTable, --COUNT")
            Else
                sql = sql.Replace("[SQL_COUNT]", "")
            End If


            If (prms.returnCountOnly) Then
                sql = sql.Replace("[SQL_RESULTS]", "")
            Else
                sql = sql.Replace("[SQL_RESULTS]", sqlResults)
                sql = sql.Replace("--fn:GetMembersToSearchDataTable", "--fn:GetMembersToSearchDataTable, --RESULTS")
            End If


            If (Not String.IsNullOrEmpty(prms.AdditionalWhereClause)) Then
                sql = sql.Replace("[AdditionalWhereClause]", vbCrLf & prms.AdditionalWhereClause)
            Else
                sql = sql.Replace("[AdditionalWhereClause]", "")
            End If



            Dim SELECT_LIST As String = "*"
            If (prms.SearchSort = SearchSortEnum.NewestNearMember) Then

                SELECT_LIST = <sql><![CDATA[
	NewestNearMemberQualifier =  
		minutes1 / case 
						when distance=0 then 30
						when distance<30 then 30
						else isnull(distance, 1)
					end
	,* 
]]></sql>

            End If

            If (prms.SearchSort <> SearchSortEnum.NewestNearMember) Then

                SELECT_LIST = <sql><![CDATA[
	CountryQualifier = case 
						    when Country=@Country then 1
						    else 0
					    end
	,* 
]]></sql>

            End If

            sql = sql.Replace("[SELECT-LIST]", SELECT_LIST)
            If (prms.rowNumberMax > 0) Then
                sql = sql.Replace("[TOP]", "TOP(@RowNumberMax)")
            Else
                sql = sql.Replace("[TOP]", "")
            End If


            Dim sqlOrderBy As String = ""
            If (LookingFor_ToMeetFemaleID) Then
                If (prms.SearchSort = SearchSortEnum.NewestMember) Then
                    sqlOrderBy = vbCrLf & _
                        "Order By HasPhoto1 desc, CountryQualifier desc, DateTimeToRegister Desc, distance asc, Birthday asc, PointsBeauty desc, PointsVerification desc, PointsUnlocks desc"
                ElseIf (prms.SearchSort = SearchSortEnum.NewestNearMember) Then
                    sqlOrderBy = vbCrLf & _
                        "Order By  NewestNearMemberQualifier desc, DateTimeToRegister Desc, distance asc, Birthday asc, PointsBeauty desc, PointsVerification desc, PointsUnlocks desc"
                ElseIf (prms.SearchSort = SearchSortEnum.OldestMember) Then
                    sqlOrderBy = vbCrLf & _
                        "Order By HasPhoto1 desc, CountryQualifier desc, DateTimeToRegister Asc, distance asc, Birthday asc, PointsBeauty desc, PointsVerification desc, PointsUnlocks desc"
                ElseIf (prms.SearchSort = SearchSortEnum.RecentlyLoggedIn) Then
                    sqlOrderBy = vbCrLf & _
                        "Order By HasPhoto1 desc, CountryQualifier desc, LastActivityDateTime Desc, distance asc, Birthday asc, PointsBeauty desc, PointsVerification desc, PointsUnlocks desc"
                ElseIf (prms.SearchSort = SearchSortEnum.Birthday) Then
                    sqlOrderBy = vbCrLf & _
                        "Order By CountryQualifier desc,1 desc,CelebratingBirth asc,LastLoginDateTime Desc, distance asc, Birthday asc, PointsBeauty desc, PointsVerification desc, PointsUnlocks desc"
                Else
                    'SearchSortEnum.NearestDistance
                    'SearchSortEnum.None
                    sqlOrderBy = vbCrLf & _
                        "order by HasPhoto1 desc, CountryQualifier desc, distance asc, Birthday asc, PointsBeauty desc, PointsVerification desc, PointsUnlocks desc"
                End If
            Else
                If (prms.SearchSort = SearchSortEnum.NewestMember) Then
                    sqlOrderBy = vbCrLf & _
                        "Order By HasPhoto1 desc, CountryQualifier desc, DateTimeToRegister Desc, distance asc, PointsVerification desc, Birthday asc, PointsCredits desc, PointsUnlocks desc"
                ElseIf (prms.SearchSort = SearchSortEnum.NewestNearMember) Then
                    sqlOrderBy = vbCrLf & _
                        "Order By  NewestNearMemberQualifier desc, DateTimeToRegister Desc, distance asc, PointsVerification desc, Birthday asc, PointsCredits desc, PointsUnlocks desc"
                ElseIf (prms.SearchSort = SearchSortEnum.OldestMember) Then
                    sqlOrderBy = vbCrLf & _
                        "Order By HasPhoto1 desc, CountryQualifier desc, DateTimeToRegister Asc, distance asc, PointsVerification desc, Birthday asc, PointsCredits desc, PointsUnlocks desc"
                ElseIf (prms.SearchSort = SearchSortEnum.RecentlyLoggedIn) Then
                    sqlOrderBy = vbCrLf & _
                        "Order By HasPhoto1 desc, CountryQualifier desc, LastActivityDateTime Desc, distance asc, PointsVerification desc, Birthday asc, PointsCredits desc, PointsUnlocks desc"
                ElseIf (prms.SearchSort = SearchSortEnum.Birthday) Then
                    sqlOrderBy = vbCrLf & _
                        "Order By  HasPhoto1 desc,CountryQualifier desc,HasPhoto1,CelebratingBirth asc,LastLoginDateTime Desc,distance asc, PointsVerification desc, Birthday asc, PointsCredits desc, PointsUnlocks desc"
                Else
                    'SearchSortEnum.NearestDistance
                    'SearchSortEnum.None
                    sqlOrderBy = vbCrLf & _
                        "order by HasPhoto1 desc, CountryQualifier desc, distance asc, PointsVerification desc, Birthday asc, PointsCredits desc, PointsUnlocks desc"
                End If
            End If


            sql = sql.Replace("[QueryOrdering]", sqlOrderBy)
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(con, sql)


                    command.Parameters.Add(New SqlClient.SqlParameter("@CurrentProfileId", CurrentProfileId))
                    command.Parameters.Add(New SqlClient.SqlParameter("@ReturnRecordsWithStatus", prms.ReturnRecordsWithStatus))
                    command.Parameters.Add(New SqlClient.SqlParameter("@NumberOfRecordsToReturn", NumberOfRecordsToReturn))
                    command.Parameters.Add(New SqlClient.SqlParameter("@Distance", Distance))
                    command.Parameters.Add(New SqlClient.SqlParameter("@DISTANCE_DEFAULT", DISTANCE_DEFAULT))
                    command.Parameters.Add(New SqlClient.SqlParameter("@performCount", prms.performCount))
                    command.Parameters.Add(New SqlClient.SqlParameter("@RowNumberMin", prms.rowNumberMin))
                    command.Parameters.Add(New SqlClient.SqlParameter("@RowNumberMax", prms.rowNumberMax))
                    command.Parameters.Add(New SqlClient.SqlParameter("@NewestNearMemberDateStart", Date.UtcNow.AddMonths(-1).Date))
                    ' command.Parameters.Add(New SqlClient.SqlParameter("@ThisVoteDateStartTime", prms.DayTimeStarts))
                    '  command.Parameters.Add(New SqlClient.SqlParameter("@ThisVoteDateEndTime", prms.DateTimeEnds))
                    If (String.IsNullOrEmpty(prms.zipstr)) Then
                        command.Parameters.Add(New SqlClient.SqlParameter("@zip", System.DBNull.Value))
                    Else
                        command.Parameters.Add(New SqlClient.SqlParameter("@zip", prms.zipstr))
                    End If

                    'If (prms.latitudeIn Is Nothing) Then
                    '    command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", System.DBNull.Value))
                    'Else
                    '    command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", prms.latitudeIn))
                    'End If

                    'If (prms.longitudeIn Is Nothing) Then
                    '    command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", System.DBNull.Value))
                    'Else
                    '    command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", prms.longitudeIn))
                    'End If

                    Dim prm1 As SqlClient.SqlParameter = DataHelpers.GetSqlParam_LatitubeOrLongitude("@latitudeIn", prms.latitudeIn)
                    command.Parameters.Add(prm1)

                    Dim prm2 As SqlClient.SqlParameter = DataHelpers.GetSqlParam_LatitubeOrLongitude("@longitudeIn", prms.longitudeIn)
                    command.Parameters.Add(prm2)

                    Try
                        Dim mins As Integer = clsConfigValues.Get__members_online_minutes()
                        Dim LastActivityUTCDate As DateTime = Date.UtcNow.AddMinutes(-mins)
                        command.Parameters.Add(New SqlClient.SqlParameter("@LastActivityUTCDate", LastActivityUTCDate))
                    Catch
                        command.Parameters.Add(New SqlClient.SqlParameter("@LastActivityUTCDate", System.DBNull.Value))
                    End Try

                    Try
                        Dim hours As Integer = clsConfigValues.Get__members_online_recently_hours()
                        Dim LastActivityUTCDate As DateTime = Date.UtcNow.AddHours(-hours)
                        command.Parameters.Add(New SqlClient.SqlParameter("@LastActivityRecentlyUTCDate", LastActivityUTCDate))
                    Catch
                        command.Parameters.Add(New SqlClient.SqlParameter("@LastActivityRecentlyUTCDate", System.DBNull.Value))
                    End Try
                    Using dt = DataHelpers.GetDataSet(command)
                        Return dt
                    End Using
                End Using
            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        Finally
            clsLogger.InsertLog("GetMembersToSearchDataTable", prms.CurrentProfileId, (DateTime.UtcNow - __logdate).TotalMilliseconds)
        End Try



    End Function
    Private Shared Function GetMembersToSearchforUserName(prms As clsLoveHelperParameters) As DataSet

        'clsLogger.InsertLog("GetMembersToSearchDataTable", prms.CurrentProfileId)
        Dim __logdate As DateTime = DateTime.UtcNow

        Dim CurrentProfileId As Integer = prms.CurrentProfileId
        'Dim SearchSort As SearchSortEnum = prms.SearchSort
        'Dim zipstr As String = prms.zipstr
        Dim Distance As Integer = prms.Distance
        '    Dim LookingFor_ToMeetMaleID As Boolean = prms.LookingFor_ToMeetMaleID
        Dim LookingFor_ToMeetFemaleID As Boolean = prms.LookingFor_ToMeetFemaleID
        Dim NumberOfRecordsToReturn As Integer = prms.NumberOfRecordsToReturn
        'Dim AdditionalWhereClause As String = prms.AdditionalWhereClause


        If (Distance = 0) Then
            Distance = DISTANCE_DEFAULT
        End If

        Dim sql As String = "", sqlCount As String = "", sqlResults As String = ""


        'This script is based on kilometer. If you use miles simply convert the radius to kilometer (radius=radius*1.609344)
        Try

            sqlCount = <sql><![CDATA[
if(@performCount=1)
begin

    select Count(*) from (
	    SELECT    top 200
		    distance=case 
                when @latitudeIn is null or EUS_Profiles.latitude is null or @longitudeIn is null or EUS_Profiles.longitude is null then 1000
                else cast(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)) as int)
            end
	    FROM		
            dbo.EUS_Profiles AS EUS_Profiles 
		    with (nolock)
	    --LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerPhotosID = EUS_Profiles.DefPhotoID AND phot.CustomerID = EUS_Profiles.ProfileID
		--LEFT OUTER JOIN dbo.EUS_ProfilesPrivacySettings AS pps ON (pps.ProfileID=EUS_Profiles.ProfileID) and (pps.HideMeFromUsersInDifferentCountry=1)
        WHERE
		    EUS_Profiles.IsMaster=1
	    AND EUS_Profiles.Profileid>1
	    AND EUS_Profiles.ProfileID<>@CurrentProfileId --AND EUS_Profiles.ProfileID<>@MirrorProfileID
	    AND EUS_Profiles.Status = @ReturnRecordsWithStatus
and (EUS_Profiles.[LoginName] like @Username or EUS_Profiles.[LoginName] like @UsernameEnd or EUS_Profiles.[LoginName] like @UsernameStartEnd)
	    AND (EUS_Profiles.PrivacySettings_HideMeFromSearchResults is null or EUS_Profiles.PrivacySettings_HideMeFromSearchResults=0)
	    AND (
			    (@LookingFor_ToMeetMaleID= 1    AND EUS_Profiles.GenderId = 1)
		    OR
			    (@LookingFor_ToMeetFemaleID = 1 AND EUS_Profiles.GenderId = 2)
	    )
	    AND  not exists(select * 
						from EUS_ProfilesBlocked bl 
                        with (nolock) 
						where (bl.FromProfileID =EUS_Profiles.ProfileID and bl.ToProfileID = @CurrentProfileId) or
							(bl.FromProfileID =@CurrentProfileId and bl.ToProfileID = EUS_Profiles.ProfileID)
	    )
	    AND (  
            EUS_Profiles.Country = @Country OR 
            (
                EUS_Profiles.Country <> @Country AND
                not exists(select * 
					        from EUS_ProfilesPrivacySettings bl 
                            with (nolock) 
					        where (bl.ProfileID =EUS_Profiles.ProfileID) and (HideMeFromUsersInDifferentCountry=1)
                )
            )
        )



    ) as EUS_Profiles
    where 
      ((distance <= @Distance and @Distance <@DISTANCE_DEFAULT)  OR ((distance <= @DISTANCE_DEFAULT) and @Distance =@DISTANCE_DEFAULT))
end
]]></sql>.Value


            sqlResults = <sql><![CDATA[



select 
	tOutter.*
    ,CASE 
		/*WHEN ISNULL(tOutter.Birthday, '') = '' then null
		ELSE dbo.fn_GetAge(tOutter.Birthday, GETDATE())*/
        WHEN not tOutter.Birthday is null then dbo.fn_GetAge(tOutter.Birthday, GETDATE())
		ELSE null
	End as Age
    ,IsOnlineNow=CAST((CASE
            WHEN pps.[PrivacySettings_ShowMeOffline]=1 THEN 0
            WHEN not @LastActivityUTCDate is null and (tOutter.IsOnline=1 and tOutter.LastActivityDateTime>=@LastActivityUTCDate) THEN 1
            ELSE 0
        END) as bit)
    ,IsOnlineRecently=CAST((CASE
            WHEN pps.[PrivacySettings_ShowMeOffline]=1 THEN 0
            WHEN not @LastActivityUTCDate is null and (tOutter.IsOnline=1 and tOutter.LastActivityDateTime>=@LastActivityRecentlyUTCDate) THEN 1
            ELSE 0
        END) as bit)
  /*  ,HasFavoritedMe = case 
		when (exists (select *
						from 	dbo.EUS_ProfilesFavorite favMe
						with (nolock)
						where   favMe.ToProfileID = @CurrentProfileId AND favMe.FromProfileID = tOutter.ProfileID))
        then 1
        else 0
    end*/
   /* ,DidIFavorited = case 
		when (exists (select *
                        from 	dbo.EUS_ProfilesFavorite favMe
                        with (nolock)
                        where   favMe.ToProfileID = tOutter.ProfileID AND favMe.FromProfileID = @CurrentProfileId))
        then 1
        else 0
    end
	,CommunicationUnl = case 
		when (exists (
				select *
				from 	dbo.EUS_UnlockedConversations unl
				with (nolock)
				where  @CurrentProfileId<=8911
				and  tOutter.ProfileID<=8911
				and (( unl.FromProfileID = @CurrentProfileId AND unl.ToProfileID = tOutter.ProfileID) OR
					( unl.FromProfileID = tOutter.ProfileID  AND unl.ToProfileID = @CurrentProfileId))
		))
		then 1
		else 0
	end
    ,WantJob = case 
        when tOutter.GenderID=2 then (select top(1) lt.JobID  from dbo.EUS_ProfileJobs pj inner join dbo.EUS_LISTS_Job lt on lt.JobID=pj.JobID where ProfileID=tOutter.ProfileID)
        else null
    end
    ,BreastSizeID = case 
        when tOutter.GenderID=2 then (select top(1) b.BreastSizeID from dbo.EUS_ProfileBreastSize b where b.ProfileID=tOutter.ProfileID)
        else null
    end
    ,IsAnyWinkOrIsAnyOffer = (
        SELECT top(1) [EUS_Offers].OfferID 
		FROM [dbo].[EUS_Offers]
		WHERE (([ToProfileID]=tOutter.ProfileID AND [FromProfileID]=@CurrentProfileId) OR ([ToProfileID]=@CurrentProfileId  AND [FromProfileID]=tOutter.ProfileID))
		AND ([OfferTypeID]=1 OR [OfferTypeID]=10  OR [OfferTypeID]= 11)
		--WINK,OFFERNEW,OFFERCOUNTER
	)
    ,IsAnyLastOffer = (SELECT TOP (1) [OfferID]
        FROM [dbo].[EUS_Offers] with (nolock)
        WHERE (([ToProfileID]=tOutter.ProfileID AND [FromProfileID]=@CurrentProfileId) OR ([ToProfileID]=@CurrentProfileId AND [FromProfileID]=tOutter.ProfileID)) 
        AND ([OfferTypeID]=10 OR [OfferTypeID]=11) 
        AND ([ChildOfferID]=0)
        --10=OFFERNEW, 11=OFFERCOUNTER
	)
    ,ProfilesCommunication_IsAnySentMessage = (
		SELECT TOP (1) [SentMessageID]
		FROM [dbo].[EUS_ProfilesCommunication]
		WHERE not [SentMessageID] is null 
		and [SentMessageID] > 0 
		and (([FromProfileID] = tOutter.ProfileID AND [ToProfileID] = @CurrentProfileId) or 
				([FromProfileID] = @CurrentProfileId AND [ToProfileID] = tOutter.ProfileID))
	)
    ,ProfilesCommunication_IsAnySentLike = (
		SELECT TOP (1) [SentLikeID]
		FROM [dbo].[EUS_ProfilesCommunication]
		WHERE not [SentLikeID] is null 
		and [SentLikeID] > 0 
		and (([FromProfileID] = tOutter.ProfileID AND [ToProfileID] = @CurrentProfileId) or 
				([FromProfileID] = @CurrentProfileId AND [ToProfileID] = tOutter.ProfileID))
	)
    ,ProfilesCommunication_IsAnySentOffer = (
		SELECT TOP (1) [SentOfferID]
		FROM [dbo].[EUS_ProfilesCommunication]
		WHERE not [SentOfferID] is null 
		and [SentOfferID] > 0 
		and (([FromProfileID] = tOutter.ProfileID AND [ToProfileID] = @CurrentProfileId) or 
				([FromProfileID] = @CurrentProfileId AND [ToProfileID] = tOutter.ProfileID))
	)*/
    ,HasSubscription = CAST((case
        when tOutter.GenderId=1 and exists (select CustomerCreditsId from EUS_CustomerCredits  cc  where cc.CustomerId = tOutter.ProfileID and cc.IsSubscription=1 and cc.DateTimeExpiration>getutcdate()) 
        then 1
        else 0
    end) as bit),
Loves=isnull((select sum([Loves]) from (
SELECT  count([Love]) as Loves FROM [dbo].[EUS_ProfilesLoves] with (nolock) where (ToProfileId=tOutter.ProfileID 
or ToProfileID=tOutter.MirrorProfileID) and Love=1 and ([ActionDatetime]>=@ThisVoteDateStartTime and ActionDatetime<@ThisVoteDateEndTime)
group by ToProfileId) as t),0),
Hates= isnull((select sum([Hates]) from (
SELECT  count([Hate]) as Hates FROM [dbo].[EUS_ProfilesLoves] with (nolock) where (ToProfileId=tOutter.ProfileID 
or ToProfileID=tOutter.MirrorProfileID) and Hate=1 and ([ActionDatetime]>=@ThisVoteDateStartTime and ActionDatetime<@ThisVoteDateEndTime)
group by ToProfileId) as t1),0),
HasBeenVotedLoveFromCurrentProfile=isnull((Select count([ProfilesLovesID]) as AllowVote 
from [dbo].[EUS_ProfilesLoves] with (nolock)
where (FromProfileID=@CurrentProfileId or FromProfileID=@MirrorProfileID)	
		and  (ToProfileId=tOutter.ProfileID or ToProfileID=tOutter.MirrorProfileID)
		and [Love]=1 
		and [ActionDatetime]>=@ThisVoteDateStartTime and ActionDatetime<@ThisVoteDateEndTime),0),
HasBeenVotedHateFromCurrentProfile=isnull((Select count([ProfilesLovesID]) as AllowVote 
from [dbo].[EUS_ProfilesLoves] with (nolock)
where (FromProfileID=@CurrentProfileId or FromProfileID=@MirrorProfileID)	
		and  (ToProfileId=tOutter.ProfileID or ToProfileID=tOutter.MirrorProfileID)
		and [Hate]=1 
		and [ActionDatetime]>=@ThisVoteDateStartTime and ActionDatetime<@ThisVoteDateEndTime),0),
cast(case 
			        when (tOutter.[HasPhoto1] > 0  )  then 1
			        else 0  end as bit) as HasPhoto

from (
    select  [TOP]
	    RowNumber = Row_Number() over([QueryOrdering])
	    ,tInner.*
    from(
        select 
            [SELECT-LIST]
        from (
 SELECT    
		        distance=case 
                    when @latitudeIn is null or EUS_Profiles.latitude is null or @longitudeIn is null or EUS_Profiles.longitude is null then 1000
                    else cast(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)) as int)
                end
  		        ,minutes1=datediff(minute,@NewestNearMemberDateStart,EUS_Profiles.DateTimeToRegister),
                EUS_Profiles.PointsBeauty,    
		        EUS_Profiles.PointsVerification,     
		        EUS_Profiles.PointsCredits,    
                EUS_Profiles.PointsUnlocks,
                EUS_Profiles.ProfileID, 
                EUS_Profiles.IsMaster, 
                EUS_Profiles.MirrorProfileID, 
                EUS_Profiles.Status,
                EUS_Profiles.LoginName, 
                EUS_Profiles.FirstName,
                EUS_Profiles.LastName, 
                EUS_Profiles.GenderId, 
               /* EUS_Profiles.Country, 
                EUS_Profiles.Region, 
                EUS_Profiles.City, 
                EUS_Profiles.Zip, 
                EUS_Profiles.CityArea, 
                EUS_Profiles.Address, 
                EUS_Profiles.Telephone, 
                EUS_Profiles.eMail, 
                EUS_Profiles.Cellular, 
                EUS_Profiles.AreYouWillingToTravel, 
                EUS_Profiles.AboutMe_Heading, 
                EUS_Profiles.AboutMe_DescribeYourself, 
                EUS_Profiles.AboutMe_DescribeAnIdealFirstDate, 
                EUS_Profiles.OtherDetails_EducationID, 
                EUS_Profiles.OtherDetails_AnnualIncomeID, 
                EUS_Profiles.OtherDetails_NetWorthID, 
                EUS_Profiles.OtherDetails_Occupation, 
                EUS_Profiles.PersonalInfo_HeightID, 
                EUS_Profiles.PersonalInfo_BodyTypeID, 
                EUS_Profiles.PersonalInfo_EyeColorID, 
                EUS_Profiles.PersonalInfo_HairColorID, 
                EUS_Profiles.PersonalInfo_ChildrenID, 
                EUS_Profiles.PersonalInfo_EthnicityID,
                EUS_Profiles.PersonalInfo_ReligionID, 
                EUS_Profiles.PersonalInfo_SmokingHabitID, 
                EUS_Profiles.PersonalInfo_DrinkingHabitID, 
                EUS_Profiles.LookingFor_ToMeetMaleID, 
                EUS_Profiles.LookingFor_ToMeetFemaleID, EUS_Profiles.LookingFor_RelationshipStatusID, 
                EUS_Profiles.LookingFor_TypeOfDating_ShortTermRelationship, 
                EUS_Profiles.LookingFor_TypeOfDating_Friendship,
                EUS_Profiles.LookingFor_TypeOfDating_LongTermRelationship, 
                EUS_Profiles.LookingFor_TypeOfDating_MutuallyBeneficialArrangements, 
                EUS_Profiles.LookingFor_TypeOfDating_MarriedDating, 
                EUS_Profiles.LookingFor_TypeOfDating_AdultDating_Casual, */
                EUS_Profiles.DateTimeToRegister, 
               /* EUS_Profiles.RegisterGEOInfos, 
                EUS_Profiles.LastLoginDateTime, 
                EUS_Profiles.LastLoginGEOInfos, */
                EUS_Profiles.LastUpdateProfileDateTime, 
                EUS_Profiles.LastUpdateProfileGEOInfo, 
                EUS_Profiles.LAGID, 
                EUS_Profiles.CustomReferrer, 
                EUS_Profiles.Birthday, 
                EUS_Profiles.IsOnline, 
                EUS_Profiles.LastActivityDateTime, 
                EUS_Profiles.AvailableCredits, 
                EUS_Profiles.DefPhotoID,
                EUS_Profiles.PhotosApproved, 
cast(0 as int) as CloserResult, 
EUS_Profiles.[VotesInLast24Hour],
      EUS_Profiles.[LastVoteDateTime],
               /* EUS_Profiles.CelebratingBirth, */
 [HasPhoto1] = 	isnull((select count([customerphotosid])
					 from [dbo].[eus_customerphotos] with (nolock)
					where ([CustomerID] in (EUS_Profiles.MirrorProfileID ,EUS_Profiles.ProfileID))
						and [HasAproved]=1 and [IsDeleted]=0 and [DisplayLevel]=0),0),
                phot.CustomerPhotosID, phot.CustomerID, 
                phot.DateTimeToUploading, phot.FileName, 
                phot.DisplayLevel, phot.HasAproved, phot.HasDeclined, phot.IsDefault 
              		-- HasPhoto=isnull((SELECT count([CustomerPhotosID])
						  -- FROM [dbo].[EUS_CustomerPhotos] with (nolock)
						 -- where ([CustomerID]=EUS_Profiles.MirrorProfileID or [CustomerID]=EUS_Profiles.ProfileID)
							-- and [HasAproved]=1 and [IsDeleted]=0 and [DisplayLevel]=0),0)
	        FROM		
                dbo.EUS_Profiles AS EUS_Profiles 
		        with (nolock)
	        LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot WITH (NOLOCK) ON phot.CustomerPhotosID = EUS_Profiles.DefPhotoID AND  phot.CustomerID = EUS_Profiles.ProfileID
            WHERE

		        EUS_Profiles.IsMaster=1
	        AND EUS_Profiles.Profileid>1
	        AND EUS_Profiles.ProfileID<>@CurrentProfileId --AND EUS_Profiles.ProfileID<>@MirrorProfileID
	        AND EUS_Profiles.Status = @ReturnRecordsWithStatus
            and EUS_Profiles.[LoginName] like @Username

	        AND (EUS_Profiles.PrivacySettings_HideMeFromSearchResults is null or EUS_Profiles.PrivacySettings_HideMeFromSearchResults=0)
	        AND (
			        (@LookingFor_ToMeetMaleID= 1    AND EUS_Profiles.GenderId = 1)
		        OR
			        (@LookingFor_ToMeetFemaleID = 1 AND EUS_Profiles.GenderId = 2)
	        )
	        AND  not exists(select * 
						    from EUS_ProfilesBlocked bl 
                            with (nolock) 
						    where (bl.FromProfileID =EUS_Profiles.ProfileID and bl.ToProfileID = @CurrentProfileId) or
							    (bl.FromProfileID =@CurrentProfileId and bl.ToProfileID = EUS_Profiles.ProfileID)
	        )
	        AND (  
                EUS_Profiles.Country = @Country OR 
                (
                    EUS_Profiles.Country <> @Country AND
                    not exists(select * 
					            from EUS_ProfilesPrivacySettings bl 
                                with (nolock) 
					            where (bl.ProfileID =EUS_Profiles.ProfileID) and (HideMeFromUsersInDifferentCountry=1)
                    )
                )
            ) 
     
        





union all
	        SELECT    
		        distance=case 
                    when @latitudeIn is null or EUS_Profiles.latitude is null or @longitudeIn is null or EUS_Profiles.longitude is null then 1000
                    else cast(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)) as int)
                end
  		        ,minutes1=datediff(minute,@NewestNearMemberDateStart,EUS_Profiles.DateTimeToRegister),
                EUS_Profiles.PointsBeauty,    
		        EUS_Profiles.PointsVerification,     
		        EUS_Profiles.PointsCredits,    
                EUS_Profiles.PointsUnlocks,
                EUS_Profiles.ProfileID, 
                EUS_Profiles.IsMaster, 
                EUS_Profiles.MirrorProfileID, 
                EUS_Profiles.Status,
                EUS_Profiles.LoginName, 
                EUS_Profiles.FirstName,
                EUS_Profiles.LastName, 
                EUS_Profiles.GenderId, 
               /* EUS_Profiles.Country, 
                EUS_Profiles.Region, 
                EUS_Profiles.City, 
                EUS_Profiles.Zip, 
                EUS_Profiles.CityArea, 
                EUS_Profiles.Address, 
                EUS_Profiles.Telephone, 
                EUS_Profiles.eMail, 
                EUS_Profiles.Cellular, 
                EUS_Profiles.AreYouWillingToTravel, 
                EUS_Profiles.AboutMe_Heading, 
                EUS_Profiles.AboutMe_DescribeYourself, 
                EUS_Profiles.AboutMe_DescribeAnIdealFirstDate, 
                EUS_Profiles.OtherDetails_EducationID, 
                EUS_Profiles.OtherDetails_AnnualIncomeID, 
                EUS_Profiles.OtherDetails_NetWorthID, 
                EUS_Profiles.OtherDetails_Occupation, 
                EUS_Profiles.PersonalInfo_HeightID, 
                EUS_Profiles.PersonalInfo_BodyTypeID, 
                EUS_Profiles.PersonalInfo_EyeColorID, 
                EUS_Profiles.PersonalInfo_HairColorID, 
                EUS_Profiles.PersonalInfo_ChildrenID, 
                EUS_Profiles.PersonalInfo_EthnicityID,
                EUS_Profiles.PersonalInfo_ReligionID, 
                EUS_Profiles.PersonalInfo_SmokingHabitID, 
                EUS_Profiles.PersonalInfo_DrinkingHabitID, 
                EUS_Profiles.LookingFor_ToMeetMaleID, 
                EUS_Profiles.LookingFor_ToMeetFemaleID, EUS_Profiles.LookingFor_RelationshipStatusID, 
                EUS_Profiles.LookingFor_TypeOfDating_ShortTermRelationship, 
                EUS_Profiles.LookingFor_TypeOfDating_Friendship,
                EUS_Profiles.LookingFor_TypeOfDating_LongTermRelationship, 
                EUS_Profiles.LookingFor_TypeOfDating_MutuallyBeneficialArrangements, 
                EUS_Profiles.LookingFor_TypeOfDating_MarriedDating, 
                EUS_Profiles.LookingFor_TypeOfDating_AdultDating_Casual, */
                EUS_Profiles.DateTimeToRegister, 
               /* EUS_Profiles.RegisterGEOInfos, 
                EUS_Profiles.LastLoginDateTime, 
                EUS_Profiles.LastLoginGEOInfos, */
                EUS_Profiles.LastUpdateProfileDateTime, 
                EUS_Profiles.LastUpdateProfileGEOInfo, 
                EUS_Profiles.LAGID, 
                EUS_Profiles.CustomReferrer, 
                EUS_Profiles.Birthday, 
                EUS_Profiles.IsOnline, 
                EUS_Profiles.LastActivityDateTime, 
                EUS_Profiles.AvailableCredits, 
                EUS_Profiles.DefPhotoID,
                EUS_Profiles.PhotosApproved, 
cast(1 as int) as CloserResult, 
EUS_Profiles.[VotesInLast24Hour],
      EUS_Profiles.[LastVoteDateTime],
               /* EUS_Profiles.CelebratingBirth, */
 [HasPhoto1] = 	isnull((select count([customerphotosid])
					 from [dbo].[eus_customerphotos] with (nolock)
					where ([CustomerID] in (EUS_Profiles.MirrorProfileID ,EUS_Profiles.ProfileID))
						and [HasAproved]=1 and [IsDeleted]=0 and [DisplayLevel]=0),0),
                phot.CustomerPhotosID, phot.CustomerID, 
                phot.DateTimeToUploading, phot.FileName, 
                phot.DisplayLevel, phot.HasAproved, phot.HasDeclined, phot.IsDefault 
              		-- HasPhoto=isnull((SELECT count([CustomerPhotosID])
						  -- FROM [dbo].[EUS_CustomerPhotos] with (nolock)
						 -- where ([CustomerID]=EUS_Profiles.MirrorProfileID or [CustomerID]=EUS_Profiles.ProfileID)
							-- and [HasAproved]=1 and [IsDeleted]=0 and [DisplayLevel]=0),0)
	        FROM		
                dbo.EUS_Profiles AS EUS_Profiles 
		        with (nolock)
	        LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot WITH (NOLOCK) ON phot.CustomerPhotosID = EUS_Profiles.DefPhotoID AND  phot.CustomerID = EUS_Profiles.ProfileID
            WHERE

		        EUS_Profiles.IsMaster=1
	        AND EUS_Profiles.Profileid>1
	        AND EUS_Profiles.ProfileID<>@CurrentProfileId --AND EUS_Profiles.ProfileID<>@MirrorProfileID
	        AND EUS_Profiles.Status = @ReturnRecordsWithStatus
            and EUS_Profiles.[LoginName] like @UsernameEnd

	        AND (EUS_Profiles.PrivacySettings_HideMeFromSearchResults is null or EUS_Profiles.PrivacySettings_HideMeFromSearchResults=0)
	        AND (
			        (@LookingFor_ToMeetMaleID= 1    AND EUS_Profiles.GenderId = 1)
		        OR
			        (@LookingFor_ToMeetFemaleID = 1 AND EUS_Profiles.GenderId = 2)
	        )
	        AND  not exists(select * 
						    from EUS_ProfilesBlocked bl 
                            with (nolock) 
						    where (bl.FromProfileID =EUS_Profiles.ProfileID and bl.ToProfileID = @CurrentProfileId) or
							    (bl.FromProfileID =@CurrentProfileId and bl.ToProfileID = EUS_Profiles.ProfileID)
	        )
	        AND (  
                EUS_Profiles.Country = @Country OR 
                (
                    EUS_Profiles.Country <> @Country AND
                    not exists(select * 
					            from EUS_ProfilesPrivacySettings bl 
                                with (nolock) 
					            where (bl.ProfileID =EUS_Profiles.ProfileID) and (HideMeFromUsersInDifferentCountry=1)
                    )
                )
            ) 
     
        

union all
 SELECT    
		        distance=case 
                    when @latitudeIn is null or EUS_Profiles.latitude is null or @longitudeIn is null or EUS_Profiles.longitude is null then 1000
                    else cast(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)) as int)
                end
  		        ,minutes1=datediff(minute,@NewestNearMemberDateStart,EUS_Profiles.DateTimeToRegister),
                EUS_Profiles.PointsBeauty,    
		        EUS_Profiles.PointsVerification,     
		        EUS_Profiles.PointsCredits,    
                EUS_Profiles.PointsUnlocks,
                EUS_Profiles.ProfileID, 
                EUS_Profiles.IsMaster, 
                EUS_Profiles.MirrorProfileID, 
                EUS_Profiles.Status,
                EUS_Profiles.LoginName, 
                EUS_Profiles.FirstName,
                EUS_Profiles.LastName, 
                EUS_Profiles.GenderId, 
               /* EUS_Profiles.Country, 
                EUS_Profiles.Region, 
                EUS_Profiles.City, 
                EUS_Profiles.Zip, 
                EUS_Profiles.CityArea, 
                EUS_Profiles.Address, 
                EUS_Profiles.Telephone, 
                EUS_Profiles.eMail, 
                EUS_Profiles.Cellular, 
                EUS_Profiles.AreYouWillingToTravel, 
                EUS_Profiles.AboutMe_Heading, 
                EUS_Profiles.AboutMe_DescribeYourself, 
                EUS_Profiles.AboutMe_DescribeAnIdealFirstDate, 
                EUS_Profiles.OtherDetails_EducationID, 
                EUS_Profiles.OtherDetails_AnnualIncomeID, 
                EUS_Profiles.OtherDetails_NetWorthID, 
                EUS_Profiles.OtherDetails_Occupation, 
                EUS_Profiles.PersonalInfo_HeightID, 
                EUS_Profiles.PersonalInfo_BodyTypeID, 
                EUS_Profiles.PersonalInfo_EyeColorID, 
                EUS_Profiles.PersonalInfo_HairColorID, 
                EUS_Profiles.PersonalInfo_ChildrenID, 
                EUS_Profiles.PersonalInfo_EthnicityID,
                EUS_Profiles.PersonalInfo_ReligionID, 
                EUS_Profiles.PersonalInfo_SmokingHabitID, 
                EUS_Profiles.PersonalInfo_DrinkingHabitID, 
                EUS_Profiles.LookingFor_ToMeetMaleID, 
                EUS_Profiles.LookingFor_ToMeetFemaleID, EUS_Profiles.LookingFor_RelationshipStatusID, 
                EUS_Profiles.LookingFor_TypeOfDating_ShortTermRelationship, 
                EUS_Profiles.LookingFor_TypeOfDating_Friendship,
                EUS_Profiles.LookingFor_TypeOfDating_LongTermRelationship, 
                EUS_Profiles.LookingFor_TypeOfDating_MutuallyBeneficialArrangements, 
                EUS_Profiles.LookingFor_TypeOfDating_MarriedDating, 
                EUS_Profiles.LookingFor_TypeOfDating_AdultDating_Casual, */
                EUS_Profiles.DateTimeToRegister, 
               /* EUS_Profiles.RegisterGEOInfos, 
                EUS_Profiles.LastLoginDateTime, 
                EUS_Profiles.LastLoginGEOInfos, */
                EUS_Profiles.LastUpdateProfileDateTime, 
                EUS_Profiles.LastUpdateProfileGEOInfo, 
                EUS_Profiles.LAGID, 
                EUS_Profiles.CustomReferrer, 
                EUS_Profiles.Birthday, 
                EUS_Profiles.IsOnline, 
                EUS_Profiles.LastActivityDateTime, 
                EUS_Profiles.AvailableCredits, 
                EUS_Profiles.DefPhotoID,
                EUS_Profiles.PhotosApproved,
cast(2 as int) as CloserResult, 
EUS_Profiles.[VotesInLast24Hour],
      EUS_Profiles.[LastVoteDateTime],
               /* EUS_Profiles.CelebratingBirth, */
 [HasPhoto1] = 	isnull((select count([customerphotosid])
					 from [dbo].[eus_customerphotos] with (nolock)
					where ([CustomerID] in (EUS_Profiles.MirrorProfileID ,EUS_Profiles.ProfileID))
						and [HasAproved]=1 and [IsDeleted]=0 and [DisplayLevel]=0),0),
                phot.CustomerPhotosID, phot.CustomerID, 
                phot.DateTimeToUploading, phot.FileName, 
                phot.DisplayLevel, phot.HasAproved, phot.HasDeclined, phot.IsDefault 
              		-- HasPhoto=isnull((SELECT count([CustomerPhotosID])
						  -- FROM [dbo].[EUS_CustomerPhotos] with (nolock)
						 -- where ([CustomerID]=EUS_Profiles.MirrorProfileID or [CustomerID]=EUS_Profiles.ProfileID)
							-- and [HasAproved]=1 and [IsDeleted]=0 and [DisplayLevel]=0),0)
	        FROM		
                dbo.EUS_Profiles AS EUS_Profiles 
		        with (nolock)
	        LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot WITH (NOLOCK) ON phot.CustomerPhotosID = EUS_Profiles.DefPhotoID AND  phot.CustomerID = EUS_Profiles.ProfileID
            WHERE

		        EUS_Profiles.IsMaster=1
	        AND EUS_Profiles.Profileid>1
	        AND EUS_Profiles.ProfileID<>@CurrentProfileId --AND EUS_Profiles.ProfileID<>@MirrorProfileID
	        AND EUS_Profiles.Status = @ReturnRecordsWithStatus
            
            and EUS_Profiles.[LoginName] like @UsernameStartEnd
	        AND (EUS_Profiles.PrivacySettings_HideMeFromSearchResults is null or EUS_Profiles.PrivacySettings_HideMeFromSearchResults=0)
	        AND (
			        (@LookingFor_ToMeetMaleID= 1    AND EUS_Profiles.GenderId = 1)
		        OR
			        (@LookingFor_ToMeetFemaleID = 1 AND EUS_Profiles.GenderId = 2)
	        )
	        AND  not exists(select * 
						    from EUS_ProfilesBlocked bl 
                            with (nolock) 
						    where (bl.FromProfileID =EUS_Profiles.ProfileID and bl.ToProfileID = @CurrentProfileId) or
							    (bl.FromProfileID =@CurrentProfileId and bl.ToProfileID = EUS_Profiles.ProfileID)
	        )
	        AND (  
                EUS_Profiles.Country = @Country OR 
                (
                    EUS_Profiles.Country <> @Country AND
                    not exists(select * 
					            from EUS_ProfilesPrivacySettings bl 
                                with (nolock) 
					            where (bl.ProfileID =EUS_Profiles.ProfileID) and (HideMeFromUsersInDifferentCountry=1)
                    )
                )
            ) 
        
        





) as EUS_Profiles
        where 
          ((distance <= @Distance and @Distance <@DISTANCE_DEFAULT)  OR ((distance <= @DISTANCE_DEFAULT) and @Distance =@DISTANCE_DEFAULT)) 
    ) as tInner
) as tOutter
left join [EUS_ProfilesPrivacySettings] pps WITH (NOLOCK) on (pps.ProfileID=tOutter.ProfileID OR pps.[MirrorProfileID]=tOutter.ProfileID)
where 
	(tOutter.RowNumber > @RowNumberMin) and
	(tOutter.RowNumber <= @RowNumberMax) 
order by tOutter.RowNumber

]]></sql>.Value


            sql = <sql><![CDATA[

--fn:GetMembersToSearchDataTable
	--@CurrentProfileId int
	--,@ReturnRecordsWithStatus int
	--,@NumberOfRecordsToReturn int =0
	--,@Distance int=0

	SET NOCOUNT ON;

declare @Country nvarchar(10),
@LookingFor_ToMeetMaleID bit,
@LookingFor_ToMeetFemaleID bit,
@MirrorProfileID int

select 
    @MirrorProfileID=MirrorProfileID,
    @Country=Country, 
    @LookingFor_ToMeetMaleID=LookingFor_ToMeetMaleID, 
    @LookingFor_ToMeetFemaleID=LookingFor_ToMeetFemaleID 
from Eus_Profiles 
where ProfileID=@CurrentProfileId


[SQL_COUNT]
[SQL_RESULTS]
[SQL_USERLoves]
]]></sql>.Value

            sql = <sql><![CDATA[

--fn:GetMembersToSearchDataTable
	--@CurrentProfileId int
	--,@ReturnRecordsWithStatus int
	--,@NumberOfRecordsToReturn int =0
	--,@Distance int=0

	SET NOCOUNT ON;

declare @Country nvarchar(10),
@LookingFor_ToMeetMaleID bit,
@LookingFor_ToMeetFemaleID bit,
@MirrorProfileID int

select 
    @MirrorProfileID=MirrorProfileID,
    @Country=Country, 
    @LookingFor_ToMeetMaleID=LookingFor_ToMeetMaleID, 
    @LookingFor_ToMeetFemaleID=LookingFor_ToMeetFemaleID 
from Eus_Profiles 
where ProfileID=@CurrentProfileId


[SQL_COUNT]
[SQL_RESULTS]
[SQL_USERLoves]
]]></sql>.Value

            Dim SQL_USERLoves = <sql><![CDATA[

 select isnull(sum(cast([Love] as int)),0)as loves,isnull(sum(cast([Hate]as int)),0) as hates
 from [dbo].[EUS_ProfilesLoves]
 where ([ToProfileID] =@CurrentProfileId or [ToProfileID] =@MirrorProfileID) and 
 (ActionDatetime>=@ThisVoteDateStartTime and ActionDatetime<@ThisVoteDateEndTime)

]]></sql>.Value

            '2014-09-12 00:00:00.000
            sqlResults = sqlResults.Replace("@ThisVoteDateStartTime", "'" & prms.DayTimeStarts.ToString("yyyy-MM-dd HH:mm:ss.fff") & "'")
            sqlResults = sqlResults.Replace("@ThisVoteDateEndTime", "'" & prms.DateTimeEnds.ToString("yyyy-MM-dd HH:mm:ss.fff") & "'")
            SQL_USERLoves = SQL_USERLoves.Replace("@ThisVoteDateEndTime", "'" & prms.DateTimeEnds.ToString("yyyy-MM-dd HH:mm:ss.fff") & "'")
            SQL_USERLoves = SQL_USERLoves.Replace("@ThisVoteDateStartTime", "'" & prms.DayTimeStarts.ToString("yyyy-MM-dd HH:mm:ss.fff") & "'")
            sql = sql.Replace("[SQL_COUNT]", sqlCount)
            sql = sql.Replace("[SQL_RESULTS]", sqlResults)
            sql = sql.Replace("[SQL_USERLoves]", SQL_USERLoves)

            Dim SELECT_LIST As String = "*"
            If (prms.SearchSort = SearchSortEnum.NewestNearMember) Then

                SELECT_LIST = <sql><![CDATA[
	NewestNearMemberQualifier =  
		minutes1 / case 
						when distance=0 then 30
						when distance<30 then 30
						else isnull(distance, 1)
					end
	,* 
]]></sql>

            End If

            If (prms.SearchSort <> SearchSortEnum.NewestNearMember) Then

                SELECT_LIST = <sql><![CDATA[
	CountryQualifier = case 
						    when Country=@Country then 1
						    else 0
					    end
	,* 
]]></sql>

            End If

            sql = sql.Replace("[SELECT-LIST]", SELECT_LIST)
            If (prms.rowNumberMax > 0) Then
                sql = sql.Replace("[TOP]", "TOP(@RowNumberMax)")
            Else
                sql = sql.Replace("[TOP]", "")
            End If


            Dim sqlOrderBy As String = ""
            If (LookingFor_ToMeetFemaleID) Then
                sqlOrderBy = vbCrLf & _
                    "Order By  CloserResult asc, NewestNearMemberQualifier desc, DateTimeToRegister Desc, distance asc, Birthday asc, PointsBeauty desc, PointsVerification desc, PointsUnlocks desc"

            Else
                sqlOrderBy = vbCrLf & _
                       "Order By  CloserResult asc, NewestNearMemberQualifier desc, DateTimeToRegister Desc, distance asc, PointsVerification desc, Birthday asc, PointsCredits desc, PointsUnlocks desc"

            End If


            sql = sql.Replace("[QueryOrdering]", sqlOrderBy)
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(con, sql)


                    command.Parameters.Add(New SqlClient.SqlParameter("@CurrentProfileId", CurrentProfileId))
                    command.Parameters.Add(New SqlClient.SqlParameter("@ReturnRecordsWithStatus", prms.ReturnRecordsWithStatus))
                    command.Parameters.Add(New SqlClient.SqlParameter("@NumberOfRecordsToReturn", NumberOfRecordsToReturn))
                    command.Parameters.Add(New SqlClient.SqlParameter("@Distance", Distance))
                    command.Parameters.Add(New SqlClient.SqlParameter("@DISTANCE_DEFAULT", DISTANCE_DEFAULT))
                    command.Parameters.Add(New SqlClient.SqlParameter("@performCount", prms.performCount))
                    command.Parameters.Add(New SqlClient.SqlParameter("@RowNumberMin", prms.rowNumberMin))
                    command.Parameters.Add(New SqlClient.SqlParameter("@RowNumberMax", prms.rowNumberMax))
                    command.Parameters.Add(New SqlClient.SqlParameter("@NewestNearMemberDateStart", Date.UtcNow.AddMonths(-1).Date))
                    command.Parameters.Add(New SqlClient.SqlParameter("@UsernameEnd", prms.UserName & "_%"))
                    command.Parameters.Add(New SqlClient.SqlParameter("@UsernameStartEnd", "%_" & prms.UserName & "%"))
                    command.Parameters.Add(New SqlClient.SqlParameter("@Username", prms.UserName))

                    If (String.IsNullOrEmpty(prms.zipstr)) Then
                        command.Parameters.Add(New SqlClient.SqlParameter("@zip", System.DBNull.Value))
                    Else
                        command.Parameters.Add(New SqlClient.SqlParameter("@zip", prms.zipstr))
                    End If



                    Dim prm1 As SqlClient.SqlParameter = DataHelpers.GetSqlParam_LatitubeOrLongitude("@latitudeIn", prms.latitudeIn)
                    command.Parameters.Add(prm1)

                    Dim prm2 As SqlClient.SqlParameter = DataHelpers.GetSqlParam_LatitubeOrLongitude("@longitudeIn", prms.longitudeIn)
                    command.Parameters.Add(prm2)

                    Try
                        Dim mins As Integer = clsConfigValues.Get__members_online_minutes()
                        Dim LastActivityUTCDate As DateTime = Date.UtcNow.AddMinutes(-mins)
                        command.Parameters.Add(New SqlClient.SqlParameter("@LastActivityUTCDate", LastActivityUTCDate))
                    Catch
                        command.Parameters.Add(New SqlClient.SqlParameter("@LastActivityUTCDate", System.DBNull.Value))
                    End Try

                    Try
                        Dim hours As Integer = clsConfigValues.Get__members_online_recently_hours()
                        Dim LastActivityUTCDate As DateTime = Date.UtcNow.AddHours(-hours)
                        command.Parameters.Add(New SqlClient.SqlParameter("@LastActivityRecentlyUTCDate", LastActivityUTCDate))
                    Catch
                        command.Parameters.Add(New SqlClient.SqlParameter("@LastActivityRecentlyUTCDate", System.DBNull.Value))
                    End Try
                    Using dt = DataHelpers.GetDataSet(command)
                        Return dt
                    End Using
                End Using
            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        Finally
            clsLogger.InsertLog("GetMembersToSearchDataTable", prms.CurrentProfileId, (DateTime.UtcNow - __logdate).TotalMilliseconds)
        End Try



    End Function
    Private Sub imgSearchByUserName_Click(sender As Object, e As ImageClickEventArgs) Handles imgSearchByUserName.Click
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "scrollToResultsTop", "ShowLoading({ delay: 10, text: ''}); scrollToResultsTop();", True)
        isNewSearch = True
        LoadSavedSearch()
    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

    Private Sub Page_Unload(sender As Object, e As EventArgs) Handles Me.Unload
        If _clsVotesHelper1 IsNot Nothing Then _clsVotesHelper1.Dispose()
    End Sub
End Class



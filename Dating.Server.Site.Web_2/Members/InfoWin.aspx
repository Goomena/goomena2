﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Members/Members2Inner.Master" 
CodeBehind="InfoWin.aspx.vb" Inherits="Dating.Server.Site.Web.InfoWin" %>

<%@ Register src="../UserControls/MemberLeftPanel.ascx" tagname="MemberLeftPanel" tagprefix="uc2" %>
<%@ Register src="../UserControls/MessageNoPhoto.ascx" tagname="MessageNoPhoto" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<style type="text/css">
    body{background-image:none;}
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <div class="info-win">
        <asp:Panel ID="pnlErr" runat="server" CssClass="alert alert-danger" 
            Visible="False" EnableViewState="False">
            <asp:Label ID="lblErr" runat="server"></asp:Label>
        </asp:Panel>
        
        <div>
            <asp:Label ID="lblInfoTitle" runat="server"></asp:Label>
        </div>
        <div style="margin-top:10px;">
            <asp:Label ID="lblInfo" runat="server"></asp:Label>
        </div>
        <div class="form-actions" style="width:20px auto;">
            <asp:Button ID="btnClose" runat="server" CssClass="btn btn-primary" Text="Close" OnClientClick="return false;" />
        </div>
    </div>
    <script type="text/javascript">
        var popupName = '<%= Request.QueryString("popup") %>';
        function resizePopup() {
            var $j = jQuery;
            var h = $('html').height();
            var w = $('html').width();
            try {
                //top.window[popupName].SetSize(w, h);
                //top.window[popupName].UpdatePosition();
            }
            catch (e) { }
        }

        jQuery(document).ready(function ($) {
            if (stringIsEmpty(popupName)) {
                $('#<%= btnClose.ClientID %>').hide();
            }
            else {
                $('#<%= btnClose.ClientID %>').click(function (e) {
                    closePopup(popupName);
                });
            }
            setTimeout(resizePopup, 300);
        });
    </script>
</asp:Content>

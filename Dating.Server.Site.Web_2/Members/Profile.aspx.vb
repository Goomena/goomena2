﻿Imports Dating.Server.Core.DLL
Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Datasets.DLL.DSMembers

Public Class Profile
    Inherits BasePage


    Protected Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            'If (_pageData Is Nothing) Then _pageData = New clsPageData("~/Members/control.ProfileView", Context)
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData("~/Members/control.ProfileView", Context, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property

    Private Sub Page_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        Try
            If Me._pageData IsNot Nothing Then
                RemoveHandler Me._pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If

        Catch ex As Exception
        End Try
        Try
            MyBase.Dispose()
        Catch ex As Exception
        End Try
    End Sub

    Protected Overrides Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        SetMasterPage(CurrentPageData)

        Try
            If clsCurrentContext.VerifyLogin() = True Then
                Me.MasterPageFile = "~/Members/Members2.Master"
            Else
                clsCurrentContext.ClearSession(Session)
                If (Not Me.IsCallback) Then
                    Response.Redirect(ResolveUrl("~/Login.aspx"), False)
                End If
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If (clsCurrentContext.VerifyLogin() = False) Then
                FormsAuthentication.SignOut()
                HttpContext.Current.ApplicationInstance.CompleteRequest()
                If (Not Me.IsCallback) Then
                    Response.Redirect(Page.ResolveUrl("~/Login.aspx"), False)
                End If
            End If


            'Try
            '    Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            '    AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
            'Catch ex As Exception
            '    WebErrorMessageBox(Me, ex, "Page_Load")
            'End Try


            If (Not Me.IsPostBack) Then
                msg_divProfileDeleted.Visible = False
                msg_divProfileLIMITED.Visible = False
                msg_divProfileNotFound.Visible = False

                LoadLAG()


                Try
                    Dim prof As EUS_Profile = Nothing
                    Dim _userId As Integer = 0
                    Try

                        If (Request.QueryString("p") <> "") Then
                            Using cmdb As CMSDBDataContext = Me.GetCMSDBDataContext
                                prof = DataHelpers.GetEUS_ProfileByLoginNameOrEmail_OrderByStatus(cmdb, Request.QueryString("p"), If(Me.IsMale, 1, 2))
                                If (prof IsNot Nothing) Then
                                    _userId = prof.ProfileID
                                    Dim bl As Boolean = clsUserDoes.IsBlocked(Me.MasterProfileId, _userId)
                                    If bl = True Then
                                        _userId = -1

                                    End If

                                End If
                            End Using
                           
                        End If
                    Catch ex As Exception
                        WebErrorMessageBox(Me, ex, "Page_Load")
                    End Try

                    Dim profileInactiveOrNotFound As Boolean = True
                    If (_userId > 0 AndAlso Me.MasterProfileId <> _userId) Then

                        If (DataHelpers.EUS_Profile_IsProfileActive(prof)) Then
                            mvProfileMain.SetActiveView(vwProfileView)

                            ProfileView1.Visible = True
                            ProfileView1.EnableViewState = True

                            ProfileEdit1.Visible = False
                            ProfileEdit1.EnableViewState = False

                            profileInactiveOrNotFound = False
                        End If

                    ElseIf (Me.MasterProfileId = _userId OrElse String.IsNullOrEmpty(Request.QueryString("p"))) Then

                        Dim view As String = Request.QueryString("do")
                        If (view = "edit") Then

                            mvProfileMain.SetActiveView(vwProfileEdit)
                            ''liEdit.Attributes.Add("class", "down")
                            ProfileView1.Visible = False
                            ProfileView1.EnableViewState = False

                            ProfileEdit1.Visible = True
                            ProfileEdit1.EnableViewState = True

                        Else
                            mvProfileMain.SetActiveView(vwProfileView)
                            ''liView.Attributes.Add("class", "down")
                            ProfileView1.Visible = True
                            ProfileView1.EnableViewState = True

                            ProfileEdit1.Visible = False
                            ProfileEdit1.EnableViewState = False
                        End If

                        profileInactiveOrNotFound = False
                    End If

                    If (profileInactiveOrNotFound) Then
                        ProfileView1.Visible = False
                        ProfileView1.EnableViewState = False

                        ProfileEdit1.Visible = False
                        ProfileEdit1.EnableViewState = False

                        mvProfileMain.SetActiveView(vwProfileDeleted)

                        If (_userId > 0 AndAlso prof.Status = ProfileStatusEnum.LIMITED) Then
                            msg_divProfileLIMITED.Visible = True
                        ElseIf (_userId > 0) Then
                            msg_divProfileDeleted.Visible = True
                        Else
                            msg_divProfileNotFound.Visible = True
                        End If
                    End If
                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "Page_Load")
                End Try

            End If


        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try
    End Sub


    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender

        Try
            Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub



    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
    End Sub


    Protected Sub LoadLAG()
        Try
            '   Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            msg_divProfileNotFound.Text = CurrentPageData.GetCustomString("msg_divProfileNotFound")
            msg_divProfileDeleted.Text = CurrentPageData.GetCustomString("msg_divProfileDeleted")
            msg_divProfileLIMITED.Text = CurrentPageData.GetCustomString("msg_divProfileLIMITED")

            msg_divProfileNotFound.Text = msg_divProfileNotFound.Text.
    Replace("[SEARCH_NEAR_MEMBERS_URL]", ResolveUrl("~/Members/Search.aspx?vw=NEAREST")).
    Replace("[SEARCH_NEW_MEMBERS_URL]", ResolveUrl("~/Members/Search.aspx?vw=NEWEST"))

            msg_divProfileDeleted.Text = msg_divProfileDeleted.Text.
    Replace("[SEARCH_NEAR_MEMBERS_URL]", ResolveUrl("~/Members/Search.aspx?vw=NEAREST")).
    Replace("[SEARCH_NEW_MEMBERS_URL]", ResolveUrl("~/Members/Search.aspx?vw=NEWEST"))

            msg_divProfileLIMITED.Text = msg_divProfileLIMITED.Text.
    Replace("[SEARCH_NEAR_MEMBERS_URL]", ResolveUrl("~/Members/Search.aspx?vw=NEAREST")).
    Replace("[SEARCH_NEW_MEMBERS_URL]", ResolveUrl("~/Members/Search.aspx?vw=NEWEST"))

            ''lnkViewProfileTab.Text = "<span></span>" & CurrentPageData.GetCustomString(lnkViewProfileTab.ID)
            ''lnkEditProfileTab.Text = "<span></span>" & CurrentPageData.GetCustomString(lnkEditProfileTab.ID)
            'lnkCreateAccount.Text = CurrentPageData.GetCustomString("lnkCreateAccount")
            'lblRestoreInstructions.Text = CurrentPageData.GetCustomString("lblRestoreInstructions")
            'lblUsernameText.Text = CurrentPageData.GetCustomString("lblUsernameText")
            'btnLogin.Text = CurrentPageData.GetCustomString("btnLogin")
            'lblHtmlBodyRight.Text = CurrentPageData.GetCustomString("lblHtmlBodyRight")


            'AppUtils.setNaviLabel(Me, "lbPublicNavi2Page", cPageBasic.PageTitle)


            'lblItemsName.Text = Me.CurrentPageData.GetCustomString("CartProfileView")
            'lnkViewDescription.Text = Me.CurrentPageData.GetCustomString("CartProfileWhatIsIt")
            'lnkViewDescription.OnClientClick = OnMoreInfoClickFunc(
            '    lnkViewDescription.OnClientClick,
            '    ResolveUrl("~/Members/InfoWin.aspx?info=profile"),
            '    Me.CurrentPageData.GetCustomString("CartProfileView"))
            WhatIsIt1.PopupControl.HeaderText = Me.CurrentPageData.GetCustomString("CartProfileView")

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub



    'Private Sub SetActiveView()

    'End Sub



End Class
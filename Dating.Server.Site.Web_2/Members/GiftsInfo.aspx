﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Members/Members2.master" CodeBehind="GiftsInfo.aspx.vb" Inherits="Dating.Server.Site.Web.GiftsInfo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
    </style>
    <link href="/v1/css/GiftsStyle.css?v105" type="text/css" rel="Stylesheet" />
    <script type="text/javascript">

        var lnkEnter_href = '';
        var lnkGetOut_href = '';
        function disableContinueUI() {
            $('#<%= lnkEnter.ClientID%>').removeClass('Showinactive');
               var href = $('#<%= lnkEnter.ClientID%>').attr('href');
            if (!stringIsEmpty(href)) lnkEnter_href = href;
            $('#<%= lnkEnter.ClientID%>').removeAttr('href').attr('disabled', true).addClass('opacity60');
          
         
           
            $('#<%= lnkGetOut.ClientID%>').removeAttr('disabled').removeClass('opacity60');
            if (!stringIsEmpty(lnkGetOut_href))
                $('#<%= lnkGetOut.ClientID%>').attr('href', lnkGetOut_href)
            
        }
        function enableContinueUI() {
            $('#<%= lnkEnter.ClientID%>').removeClass('Showinactive');
     
            $('#<%= lnkEnter.ClientID%>').removeAttr('disabled').removeClass('opacity60');
             if (!stringIsEmpty(lnkEnter_href))
            $('#<%= lnkEnter.ClientID%>').attr('href', lnkEnter_href)
            var href = $('#<%= lnkGetOut.ClientID%>').attr('href');
            if (!stringIsEmpty(href)) lnkGetOut_href = href;
            $('#<%= lnkGetOut.ClientID%>').removeAttr('href').attr('disabled', true).addClass('opacity60');
           
        }
    

        function chkinit() {
            var href = $('#<%= lnkEnter.ClientID%>').attr('href');
            if (!stringIsEmpty(href)) lnkEnter_href = href;
            $('#<%= lnkEnter.ClientID%>').removeAttr('href').attr('disabled', true).addClass('Showinactive');
            var href = $('#<%= lnkGetOut.ClientID%>').attr('href');
            if (!stringIsEmpty(href)) lnkGetOut_href = href;
         <%--//   $('#<%= lnkGetOut.ClientID%>').removeAttr('href').attr('disabled', true).addClass('Showinactive');--%>

            //disableContinueUI();
        }

        function chkAgree_Init(s, e) {
                          chkinit();
        }

        var chkAgree_newState = null;
        function chkAgree_CheckedChanged(s, e) {
            chkAgree_newState = s.GetChecked();
            if (chkAgree_newState)
                enableContinueUI();
            else
                disableContinueUI()
        }
        function openPopupTOS() {
            if (typeof popupOnlyTermsLoaded !== 'undefined')
                openPopupOnlyTermsDelayed({ scrollTo: 'reward' });
               }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="leftPanelUpperContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="content" runat="server">
    <div class="gifts-info-page">
        <div class="gifts-info-content">
            <div class="header-box">
                <img src="//cdn.goomena.com/images2/gifts/header-image.jpg?v6" alt="" />
                <div class="header-text">
                    <asp:Literal ID="lblHeader" runat="server"></asp:Literal>
                </div>
            </div>
            <div class="main-content">
                <asp:Literal ID="lblContent" runat="server">
                </asp:Literal>

                <div class="bottom-content">
                    <asp:Panel ID="pnlBottom" runat="server" Visible="true" >
                    <div class="check-agree">
                        <dx:ASPxCheckBox ID="chkAgree" runat="server" Text="Αποδέχομαι τους όρους χρήσης" EncodeHtml="False"
                                        EnableDefaultAppearance="True" ForeColor="Black">
                                        <CheckedImage Url="//cdn.goomena.com/images2/search/check.png" Height="19px"
                                            Width="18px">
                                        </CheckedImage>
                                        <UncheckedImage Url="//cdn.goomena.com/images2/search/uncheck.png" Height="19px"
                                            Width="18px">
                                        </UncheckedImage>
                                        <ClientSideEvents CheckedChanged="chkAgree_CheckedChanged" Init="chkAgree_Init" />
                                    </dx:ASPxCheckBox>
                    </div>
                    
                          <div class="bottom-link">
                        <div class="dvlblMustCheckAgree">
                            <asp:Label ID="lblMustCheckAgree"  CssClass="dvlblMustCheckAgree Showinactive" runat="server" Text="MustClickAgree"></asp:Label></div>
                        <table cellpadding="0" cellspacing="0" class="center-table">
                            <tr>
                                <td><asp:LinkButton ID="lnkEnter" runat="server" CssClass="enter-link">ΘΕΛΩ ΝΑ ΣΥΜΜΕΤΑΣΧΩ</asp:LinkButton></td>
                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                <td><asp:LinkButton ID="lnkGetOut" runat="server" CssClass="getout-link">ΟΧΙ ΕΥΧΑΡΙΣΤΩ</asp:LinkButton></td>
                            </tr>
                        </table>
                              <script type="text/javascript">

                                  $("body").on("mouseenter", ".enter-link.Showinactive", function () {
                                      $('.dvlblMustCheckAgree').removeClass('Showinactive');
                                  }).on("mouseleave", ".enter-link.Showinactive", function () {
                                      $('.dvlblMustCheckAgree').addClass('Showinactive');
                                  });

</script>
                    </div>
                    </asp:Panel>
                  
                </div>

            </div>

        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cntBodyBottom" runat="server">
    <asp:PlaceHolder ID="placeCtl" runat="server"></asp:PlaceHolder>
  
</asp:Content>

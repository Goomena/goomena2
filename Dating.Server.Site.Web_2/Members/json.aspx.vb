﻿Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Core.DLL
Imports System.Web.Script.Serialization
Imports System.IO
Imports System.Web.Script.Services

Public Class JSON
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If (Request("option") IsNot Nothing AndAlso
                Request("option").ToLower() = "getjobslist") Then
                Dim result As String = GetJobsList(Request("term"))
                Response.Clear()
                Response.Write(result)
                Response.End()
            End If

        Catch ex As System.Threading.ThreadAbortException
        End Try
    End Sub

    <System.Web.Services.WebMethod()> _
    Public Shared Sub SetOffsetTime(ByVal time As String)
        Dim TimeOffset As Double
        If (time.Trim() <> "" AndAlso
            Double.TryParse(time, TimeOffset) AndAlso
            HttpContext.Current.Session("TimeOffset") <> TimeOffset) Then

            HttpContext.Current.Session("TimeOffset") = TimeOffset

        End If
    End Sub


    <System.Web.Services.WebMethod(enablesession:=True)> _
    Public Shared Sub SetIsMobile(ByVal isMobile As String)
        If (Not String.IsNullOrEmpty(isMobile)) Then
            isMobile = isMobile.ToUpper()
            Dim Session As HttpSessionState = HttpContext.Current.Session

            If (isMobile = Boolean.TrueString.ToUpper()) Then
                Session("IsMobileAccess") = True

                Dim MasterProfileId As Integer = Session("ProfileID")
                UpdateEUS_Profiles_LoginData(MasterProfileId, True, True)

                If (Session("MobileAccessIDList") IsNot Nothing) Then
                    Dim MobileAccessIDList As List(Of Long) = Session("MobileAccessIDList")
                    DataHelpers.SYS_ProfilesAccess_Update_IsMobile(MobileAccessIDList)
                    Session("MobileAccessIDList") = Nothing
                End If

            ElseIf (isMobile = Boolean.FalseString.ToUpper()) Then
                Session("IsMobileAccess") = False
            End If

        End If
    End Sub

    <System.Web.Services.WebMethod()> _
    Public Shared Sub SuppressWarning_WhenReadingMessageFromDifferentCountry(ByVal SuppressWarning)
        Try

            Dim ProfileID As Integer = HttpContext.Current.Session("ProfileID")
            clsProfilesPrivacySettings.Update_SuppressWarning_WhenReadingMessageFromDifferentCountry(ProfileID, SuppressWarning)

        Catch ex As Exception
            WebErrorSendEmail(ex, ex.Message)
        End Try
    End Sub



    <System.Web.Services.WebMethod()> _
    Public Shared Function loadMemberActionCounters(ByVal data As String) As String
        Try
            Dim session As System.Web.SessionState.HttpSessionState = HttpContext.Current.Session
            Dim MasterProfileId As Integer = session("ProfileID")
            Dim leftPanelCounters As clsGetMemberActionsCounters = clsUserDoes.GetMemberActionsCounters(MasterProfileId)
            Dim counters As New MemberTotalActionCounters

            Dim AvailableCredits As Integer
            Dim __cac As clsCustomerAvailableCredits = BasePage.GetCustomerAvailableCredits(MasterProfileId, True)
            If (__cac.CreditsRecordsCount > 0) Then

                AvailableCredits = __cac.AvailableCredits
                'lnkCredits.Visible = True
                'lnkCredits.Text = CurrentPageData.GetCustomString("lnkCredits")
                If (__cac.AvailableCredits > 0) Then
                    'lnkCredits.Text = textPattern.Replace("#TEXT#", lnkCredits.Text).Replace("#COUNT#", __CustomerAvailableCredits.AvailableCredits)
                End If
                'lnkCredits.Attributes.Add("onclick", "ShowLoading();")
            Else
                'lnkCredits.Visible = False
                AvailableCredits = -1
            End If

            counters.HasSubscription = __cac.HasSubscription
            counters.CreditsCounter = AvailableCredits
            counters.WhoViewedMeCounter = leftPanelCounters.CountWhoViewedMe
            counters.WhoFavoritedMeCounter = leftPanelCounters.CountWhoFavoritedMe
            counters.WhoSharedPhotosCounter = leftPanelCounters.CountWhoSharedPhotos


            'Dim sqlProc As String = "EXEC GetNewCounters2 @CurrentProfileId= " & MasterProfileId
            'Dim dt As DataTable = DataHelpers.GetDataTable(sqlProc)
            'If (dt.Rows.Count > 0) Then
            '    If (dt.Rows(0)("NewDates2") > 0) Then counters.NewDates = dt.Rows(0)("NewDates2")
            '    If (dt.Rows(0)("NewWinks") > 0) Then counters.NewWinks = dt.Rows(0)("NewWinks")
            '    If (dt.Rows(0)("NewOffers") > 0) Then counters.NewOffers = dt.Rows(0)("NewOffers")
            '    If (dt.Rows(0)("NewMessages") > 0) Then counters.NewMessages = dt.Rows(0)("NewMessages")
            'End If

            Dim result As Dating.Server.Core.DLL.clsGetMemberActionsCounters =
                clsUserDoes.GetMemberActionsCounters(MasterProfileId)

            counters.NewDates = result.NewDates
            counters.NewWinks = result.NewLikes
            counters.NewOffers = result.NewOffers
            counters.NewMessages = result.NewMessages


            Dim serializer As New JavaScriptSerializer()
            Return serializer.Serialize(counters)

        Catch ex As Exception
            'WebErrorMessageBox(Me, ex, "")
            Return False
        End Try
    End Function




    <System.Web.Services.WebMethod()> _
    Public Shared Function GetMessagesList() As String
        Dim result As String = ""
        Try
            Dim ProfileID As Integer = HttpContext.Current.Session("ProfileID")
            If (ProfileID > 0) Then

                Using writer As New StringWriter


                    HttpContext.Current.Server.Execute("~/Members/jsonMessagesList.aspx", writer, False)
                    result = writer.ToString()
                End Using

                If (Not result.StartsWith("{""redirect""")) Then
                    Dim form_ndx = result.IndexOf("<form")
                    If (form_ndx > -1) Then
                        form_ndx = (result.IndexOf(">", form_ndx)) + 1
                        Dim form_close_ndx = result.IndexOf("</form>", form_ndx)
                        result = result.Substring(form_ndx, form_close_ndx - form_ndx)
                    End If

                    form_ndx = result.IndexOf("<input type=""hidden"" name=""__VIEWSTATE""")
                    If (form_ndx > -1) Then
                        Dim form_close_ndx = result.IndexOf(">", form_ndx) + 1
                        result = result.Remove(form_ndx, form_close_ndx - form_ndx)
                    End If
                Else
                    If (result.IndexOf(vbCrLf) > 0) Then
                        result = result.Remove(result.IndexOf(vbCrLf))
                    End If
                End If


            End If
        Catch ex As Exception
            WebErrorSendEmail(ex, ex.Message)
        End Try
        Return result
    End Function


    <System.Web.Services.WebMethod()> _
    Public Shared Function GetConversationMessagesList(otherProfile As String, rowFrom As String, rowTo As String, olderMsgIdAvailable As String, freeMessages As String) As String
        Dim result As String = ""
        result = GetConversationMessagesList2(otherProfile, rowFrom, rowTo, olderMsgIdAvailable, freeMessages, Nothing)
        Return result
    End Function


    <System.Web.Services.WebMethod()> _
    Public Shared Function GetConversationMessagesList2(otherProfile As String, rowFrom As String, rowTo As String, olderMsgIdAvailable As String, freeMessages As String, dateUntil As String) As String
        Dim result As String = ""
        Try
            Dim ProfileID As Integer = HttpContext.Current.Session("ProfileID")
            If (ProfileID > 0) Then

                If (otherProfile Is Nothing) Then otherProfile = ""
                If (rowFrom Is Nothing) Then rowFrom = ""
                If (rowTo Is Nothing) Then rowTo = ""
                If (olderMsgIdAvailable Is Nothing) Then olderMsgIdAvailable = ""
                If (dateUntil Is Nothing) Then dateUntil = ""

                Dim url As String = "~/Members/jsonConversationOlder.aspx" & "?vw=" & otherProfile & "&rowFrom=" & rowFrom & "&rowTo=" & rowTo & "&olderMsgIdAvailable=" & olderMsgIdAvailable & "&freeMessages=" & freeMessages & "&dateUntil=" & dateUntil
                Using writer As New StringWriter()
                    HttpContext.Current.Server.Execute(url, writer, False)
                    result = writer.ToString()
                End Using
             

                If (Not result.StartsWith("{""redirect""")) Then
                    Dim form_ndx = result.IndexOf("<form")
                    If (form_ndx > -1) Then
                        form_ndx = (result.IndexOf(">", form_ndx)) + 1
                        Dim form_close_ndx = result.IndexOf("</form>", form_ndx)
                        result = result.Substring(form_ndx, form_close_ndx - form_ndx)
                    End If

                    form_ndx = result.IndexOf("<input type=""hidden"" name=""__VIEWSTATE""")
                    If (form_ndx > -1) Then
                        Dim form_close_ndx = result.IndexOf(">", form_ndx) + 1
                        result = result.Remove(form_ndx, form_close_ndx - form_ndx)
                    End If
                Else
                    If (result.IndexOf(vbCrLf) > 0) Then
                        result = result.Remove(result.IndexOf(vbCrLf))
                    End If
                End If

            End If
        Catch ex As Exception
            WebErrorSendEmail(ex, ex.Message)
        End Try

        Return result
    End Function


    <System.Web.Services.WebMethod()> _
    Public Shared Function MessageUnlock2(otherProfile As String, read As String, unmsg As String, onemsg As String, free As String, subscription As String) As String
        Dim result As String = ""
        Try
            Dim ProfileID As Integer = HttpContext.Current.Session("ProfileID")
            If (ProfileID > 0) Then

                If (otherProfile Is Nothing) Then otherProfile = ""
                If (read Is Nothing) Then read = ""
                If (unmsg Is Nothing) Then unmsg = ""
                If (onemsg Is Nothing) Then onemsg = ""

                Dim url As String = "~/Members/jsonConversationUnlockMessage.aspx?free=" & free & "&read=" & read & "&onemsg=" & onemsg & "&vw=" & otherProfile & "&unmsg=" & unmsg & "&subscription=" & subscription
                Using writer As New StringWriter()
                    HttpContext.Current.Server.Execute(url, writer, False)
                    result = writer.ToString()
                End Using
             

                If (Not result.StartsWith("{""redirect""")) Then
                    Dim form_ndx = result.IndexOf("<form")
                    If (form_ndx > -1) Then
                        form_ndx = (result.IndexOf(">", form_ndx)) + 1
                        Dim form_close_ndx = result.IndexOf("</form>", form_ndx)
                        result = result.Substring(form_ndx, form_close_ndx - form_ndx)
                    End If

                    form_ndx = result.IndexOf("<input type=""hidden"" name=""__VIEWSTATE""")
                    If (form_ndx > -1) Then
                        Dim form_close_ndx = result.IndexOf(">", form_ndx) + 1
                        result = result.Remove(form_ndx, form_close_ndx - form_ndx)
                    End If
                Else
                    If (result.IndexOf(vbCrLf) > 0) Then
                        result = result.Remove(result.IndexOf(vbCrLf))
                    End If
                End If

            End If
        Catch ex As Exception
            WebErrorSendEmail(ex, ex.Message)
        End Try

        Return result
    End Function


    <System.Web.Services.WebMethod()> _
    Public Shared Function MessageUnlock(otherProfile As String, read As String, unmsg As String, onemsg As String, free As String) As String
        Dim result As String = ""
        result = MessageUnlock2(otherProfile, read, unmsg, onemsg, free, "")
        Return result
    End Function


    <System.Web.Services.WebMethod()> _
    Public Shared Function MessageDelete(otherProfile As String, msg As String, onemsg As String) As String
        Dim result As String = ""
        Try
            Dim ProfileID As Integer = HttpContext.Current.Session("ProfileID")
            If (ProfileID > 0) Then

                If (otherProfile Is Nothing) Then otherProfile = ""
                If (msg Is Nothing) Then msg = ""
                If (onemsg Is Nothing) Then onemsg = ""

                Dim MsgID As Integer
                Integer.TryParse(msg, MsgID)
                '     Dim prof As EUS_Profile = Nothing
                Dim currentprof As EUS_Profile = Nothing
                Using ctx As New CMSDBDataContext(ModGlobals.ConnectionString)
                    Try
                        currentprof = DataHelpers.GetEUS_ProfileByProfileID(ctx, ProfileID)
                        'prof = DataHelpers.GetEUS_ProfileMasterByLoginName(ctx, otherProfile, ProfileStatusEnum.Approved, ProfileStatusEnum.LIMITED)
                    Catch ex As Exception
                        Throw

                    End Try
                End Using
               

                If (currentprof IsNot Nothing) Then
                    If (clsNullable.NullTo(currentprof.ReferrerParentId) > 0) Then
                        clsUserDoes.DeleteMessage(ProfileID, MsgID, True)
                    Else
                        clsUserDoes.DeleteMessage(ProfileID, MsgID, False)
                    End If

                    Dim m As EUS_Message = clsUserDoes.GetMessage(MsgID)
                    If (m IsNot Nothing AndAlso m.IsDeleted = True) Then
                        result = "{""action"":""trash""}"
                    ElseIf (m Is Nothing) Then
                        result = "{""action"":""remove""}"
                    End If
                End If
            End If
        Catch ex As Exception
            WebErrorSendEmail(ex, ex.Message)
        End Try

        Return result
    End Function

    <System.Web.Services.WebMethod()> _
    Public Shared Function GetCredits() As String
        Dim result As String = ""
        Try
            Dim ProfileID As Integer = HttpContext.Current.Session("ProfileID")
            If (ProfileID > 0) Then
                Dim sesVars As clsSessionVariables = clsSessionVariables.GetCurrent()
                If (sesVars.MemberData.GenderId = 1) Then
                    Dim Session As System.Web.SessionState.HttpSessionState = HttpContext.Current.Session
                    Session("CustomerAvailableCredits") = Nothing
                    Dim _CustomerAvailableCredits As clsCustomerAvailableCredits = BasePage.GetCustomerAvailableCredits(ProfileID)
                    result = _CustomerAvailableCredits.AvailableCredits
                End If
            End If
        Catch ex As Exception
            WebErrorSendEmail(ex, ex.Message)
        End Try

        Return result
    End Function



    '<System.Web.Services.WebMethod()> _
    'Public Shared Function UserMessages_Check() As String
    '    '''''''''''''''
    '    '' check if any ready message available for user action made  recently
    '    '''''''''''''''
    '    Dim arrayJson As String = ""
    '    Try
    '        Dim ProfileID As Integer = HttpContext.Current.Session("ProfileID")
    '        If (ProfileID > 0) Then
    '            Dim sesVars As clsSessionVariables = clsSessionVariables.GetCurrent()
    '            If (sesVars.MemberData.GenderId = 1) Then
    '                Dim Session As System.Web.SessionState.HttpSessionState = HttpContext.Current.Session
    '                Dim dt As DataTable = clsMemberActionResponseHandler.GetReadyMessageForProfileID(ProfileID)
    '                arrayJson = AppUtils.DataTableToJSON(dt)
    '            End If
    '        End If
    '    Catch ex As Exception
    '        WebErrorSendEmail(ex, ex.Message)
    '    End Try

    '    Return arrayJson
    'End Function


    '<System.Web.Services.WebMethod()> _
    'Public Shared Function UserMessages_Read(itemid As String) As String
    '    '''''''''''''''
    '    '' check if any ready message available for user action made  recently
    '    '''''''''''''''
    '    Dim arrayJson As String = ""
    '    Try
    '        Dim QueueID As Long
    '        If (Not String.IsNullOrEmpty(itemid)) Then
    '            Long.TryParse(itemid, QueueID)
    '        End If

    '        Dim ProfileID As Integer = HttpContext.Current.Session("ProfileID")
    '        If (ProfileID > 0 AndAlso QueueID > 0) Then
    '            Dim sesVars As clsSessionVariables = clsSessionVariables.GetCurrent()
    '            If (sesVars.MemberData.GenderId = 1) Then
    '                Dim Session As System.Web.SessionState.HttpSessionState = HttpContext.Current.Session
    '                clsMemberActionResponseHandler.SetReadyMessageRead(QueueID, ProfileID)
    '            End If
    '        End If
    '    Catch ex As Exception
    '        WebErrorSendEmail(ex, ex.Message)
    '    End Try

    '    Return "OK"
    'End Function


    <System.Web.Services.WebMethod()> _
    Public Shared Function GetRandomPredefinedMessage() As String
        Dim result As String = ""
        Try
            Dim ProfileID As Integer = HttpContext.Current.Session("ProfileID")
            Dim LAGID As String = HttpContext.Current.Session("LAGID")
            If (ProfileID > 0) Then
                Dim sesVars As clsSessionVariables = clsSessionVariables.GetCurrent()
                If (sesVars.MemberData.GenderId = 1) Then
                    result = DataHelpers.GetRandomPredefinedMessage(2, LAGID)
                Else
                    result = DataHelpers.GetRandomPredefinedMessage(1, LAGID)
                End If
            End If
        Catch ex As Exception
            WebErrorSendEmail(ex, ex.Message)
        End Try

        Return result
    End Function


    <System.Web.Services.WebMethod()> _
    Public Shared Function GetAllPredefinedMessage() As String
        Dim arrayJson As String = ""
        Try
            Dim ProfileID As Integer = HttpContext.Current.Session("ProfileID")
            Dim LAGID As String = HttpContext.Current.Session("LAGID")
            If (ProfileID > 0) Then
                Dim sesVars As clsSessionVariables = clsSessionVariables.GetCurrent()
                If (sesVars.MemberData.GenderId = 1) Then
                    Using result = DataHelpers.GetAllPredefinedMessages(2, LAGID)
                        arrayJson = AppUtils.DataTableToJSON(result)
                    End Using
                Else
                    Using result = DataHelpers.GetAllPredefinedMessages(1, LAGID)
                        arrayJson = AppUtils.DataTableToJSON(result)
                    End Using
                End If
            End If
        Catch ex As Exception
            WebErrorSendEmail(ex, ex.Message)
        End Try

        Return arrayJson
    End Function


    <System.Web.Services.WebMethod()> _
    Public Shared Function WantVisitDelete(location_id As String) As String
        Dim arrayJson As String = ""
        Try
            Dim ProfileID As Integer = HttpContext.Current.Session("ProfileID")
            If (ProfileID > 0) Then

                Using ctx As New CMSDBDataContext(ConnectionString)


                    Try

                        ' check
                        Dim loc As EUS_ProfileLocation = (From itm In ctx.EUS_ProfileLocations
                                                         Where itm.ProfileID = ProfileID AndAlso itm.LocationID = location_id
                                                         Select itm).FirstOrDefault()

                        If (loc IsNot Nothing) Then
                            ctx.EUS_ProfileLocations.DeleteOnSubmit(loc)
                            ctx.SubmitChanges()
                        End If

                        Dim counter As Integer = (From itm In ctx.EUS_ProfileLocations
                                                    Where itm.ProfileID = ProfileID
                                                    Select itm).Count()

                        If (counter = 0) Then
                            DataHelpers.UpdateEUS_Profiles_AreYouWillingToTravel(ProfileID, False)
                        End If

                    Catch ex As Exception
                        Throw
                  
                    End Try
                End Using
            End If

        Catch ex As Exception
            WebErrorSendEmail(ex, ex.Message)
        End Try

        Return arrayJson
    End Function


    <System.Web.Services.WebMethod()> _
    Public Shared Function WantVisitSave(location_id As String, locality_type As String,
                                         desc As String, ref As String,
                                         loc_country As String, loc_country_short As String,
                                         loc_region As String, loc_subregion As String,
                                         loc_city As String, loc_subcity As String) As String
        Dim arrayJson As String = ""
        Try
            Dim ProfileID As Integer = HttpContext.Current.Session("ProfileID")
            If (ProfileID > 0) Then

                Using ctx As New CMSDBDataContext(ConnectionString)


                    Try

                        ' check
                        Dim loc As EUS_ProfileLocation = (From itm In ctx.EUS_ProfileLocations
                                                         Where itm.ProfileID = ProfileID AndAlso itm.LocationID = location_id
                                                         Select itm).FirstOrDefault()

                        If (loc Is Nothing) Then
                            loc = New EUS_ProfileLocation()
                            loc.LocationID = location_id
                            loc.LocalityType = locality_type
                            loc.Descr = desc
                            loc.Ref = ref
                            loc.Loc_country = loc_country
                            loc.Loc_country_short = loc_country_short
                            loc.Loc_region = loc_region
                            loc.Loc_subregion = loc_subregion
                            loc.Loc_city = loc_city
                            loc.Loc_subcity = loc_subcity
                            loc.ProfileID = ProfileID

                            ctx.EUS_ProfileLocations.InsertOnSubmit(loc)
                            ctx.SubmitChanges()
                        End If
                        Dim counter As Integer = (From itm In ctx.EUS_ProfileLocations
                                                         Where itm.ProfileID = ProfileID
                                                         Select itm).Count()

                        If (counter > 0) Then
                            DataHelpers.UpdateEUS_Profiles_AreYouWillingToTravel(ProfileID, True)
                        End If
                    Catch ex As Exception
                        Throw

                    End Try
                End Using
            End If

        Catch ex As Exception
            WebErrorSendEmail(ex, ex.Message)
        End Try

        Return arrayJson
    End Function



    <System.Web.Services.WebMethod()> _
    Public Shared Function GetJobsList(term As String) As String
        Dim arrayJson As String = ""
        Try
            Dim ProfileID As Integer = HttpContext.Current.Session("ProfileID")
            Dim LAGID As String = HttpContext.Current.Session("LAGID")
            If (ProfileID > 0 AndAlso Not String.IsNullOrEmpty(term)) Then

                Using ctx As New CMSDBDataContext(ConnectionString)


                    Try
                        term = term.ToLower()
                        ' check
                        Dim loc As New List(Of EUS_LISTS_Job)()
                        Select Case LAGID
                            Case "GR"
                                loc = (From itm In ctx.EUS_LISTS_Jobs
                                Where itm.GR.ToLower().Contains(term) OrElse itm.US.ToLower().Contains(term)
                                Select itm).ToList()
                            Case "ES"
                                loc = (From itm In ctx.EUS_LISTS_Jobs
                                Where itm.ES.ToLower().Contains(term) OrElse itm.US.ToLower().Contains(term)
                                Select itm).ToList()
                            Case "TR"
                                loc = (From itm In ctx.EUS_LISTS_Jobs
                                Where itm.TR.ToLower().Contains(term) OrElse itm.US.ToLower().Contains(term)
                                Select itm).ToList()
                            Case "HU"
                                loc = (From itm In ctx.EUS_LISTS_Jobs
                                Where itm.HU.ToLower().Contains(term) OrElse itm.US.ToLower().Contains(term)
                                Select itm).ToList()
                            Case "DE"
                                loc = (From itm In ctx.EUS_LISTS_Jobs
                                Where itm.DE.ToLower().Contains(term) OrElse itm.US.ToLower().Contains(term)
                                Select itm).ToList()
                            Case "AL"
                                loc = (From itm In ctx.EUS_LISTS_Jobs
                                Where itm.AL.ToLower().Contains(term) OrElse itm.US.ToLower().Contains(term)
                                Select itm).ToList()
                            Case "SR"
                                loc = (From itm In ctx.EUS_LISTS_Jobs
                                Where itm.SR.ToLower().Contains(term) OrElse itm.US.ToLower().Contains(term)
                                Select itm).ToList()
                            Case "RU"
                                loc = (From itm In ctx.EUS_LISTS_Jobs
                                Where itm.RU.ToLower().Contains(term) OrElse itm.US.ToLower().Contains(term)
                                Select itm).ToList()
                            Case "RO"
                                loc = (From itm In ctx.EUS_LISTS_Jobs
                                Where itm.RO.ToLower().Contains(term) OrElse itm.US.ToLower().Contains(term)
                                Select itm).ToList()
                            Case "BG"
                                loc = (From itm In ctx.EUS_LISTS_Jobs
                                Where itm.BG.ToLower().Contains(term) OrElse itm.US.ToLower().Contains(term)
                                Select itm).ToList()
                            Case "FR"
                                loc = (From itm In ctx.EUS_LISTS_Jobs
                                Where itm.FR.ToLower().Contains(term) OrElse itm.US.ToLower().Contains(term)
                                Select itm).ToList()
                            Case "IT"
                                loc = (From itm In ctx.EUS_LISTS_Jobs
                                Where itm.IT.ToLower().Contains(term) OrElse itm.US.ToLower().Contains(term)
                                Select itm).ToList()

                        End Select

                        If (loc.Count = 0) Then
                            loc = (From itm In ctx.EUS_LISTS_Jobs
                            Where itm.US.ToLower().Contains(term)
                            Select itm).ToList()
                        End If

                        If (loc.Count > 0) Then
                            Dim sb As New StringBuilder()
                            sb.Append("[")
                            For c = 0 To loc.Count - 1
                                Dim text As String = Nothing
                                Select Case LAGID
                                    Case "GR"
                                        text = loc(c).GR
                                    Case "ES"
                                        text = loc(c).ES
                                    Case "TR"
                                        text = loc(c).TR
                                    Case "HU"
                                        text = loc(c).HU
                                    Case "DE"
                                        text = loc(c).DE
                                    Case "AL"
                                        text = loc(c).AL
                                    Case "SR"
                                        text = loc(c).SR
                                    Case "RU"
                                        text = loc(c).RU
                                    Case "RO"
                                        text = loc(c).RO
                                    Case "BG"
                                        text = loc(c).BG
                                    Case "FR"
                                        text = loc(c).FR
                                    Case "IT"
                                        text = loc(c).IT
                                End Select
                                If (String.IsNullOrEmpty(text)) Then
                                    text = loc(c).US
                                End If
                                sb.Append("{""id"":""" & loc(c).JobID & """,""value"":""" & text & """}")

                                If (c < loc.Count - 1) Then sb.Append(",")
                            Next
                            sb.Append("]")
                            arrayJson = sb.ToString()
                        End If

                    Catch ex As Exception
                        Throw
                  
                    End Try
                End Using
            End If

        Catch ex As Exception
            WebErrorSendEmail(ex, ex.Message)
        End Try

        Return arrayJson
    End Function



    <System.Web.Services.WebMethod()> _
    Public Shared Function JobsSave(job_id As String) As String
        Dim arrayJson As String = ""
        Try
            Dim ProfileID As Integer = HttpContext.Current.Session("ProfileID")
            Dim ijob_id As Integer = 0
            If (ProfileID > 0 AndAlso job_id IsNot Nothing AndAlso Integer.TryParse(job_id, ijob_id) AndAlso ijob_id > 0) Then

                Using ctx As New CMSDBDataContext(ConnectionString)


                    Try

                        ' check
                        Dim loc As EUS_ProfileJob = (From itm In ctx.EUS_ProfileJobs
                                                         Where itm.ProfileID = ProfileID AndAlso itm.JobID = ijob_id
                                                         Select itm).FirstOrDefault()

                        If (loc Is Nothing) Then
                            loc = New EUS_ProfileJob()
                            loc.JobID = ijob_id
                            loc.ProfileID = ProfileID

                            ctx.EUS_ProfileJobs.InsertOnSubmit(loc)
                            ctx.SubmitChanges()
                        End If
                    Catch ex As Exception
                        Throw
                    End Try
                End Using
            End If

        Catch ex As Exception
            WebErrorSendEmail(ex, ex.Message)
        End Try

        Return arrayJson
    End Function


    <System.Web.Services.WebMethod()> _
    Public Shared Function JobsDelete(job_id As String) As String
        Dim arrayJson As String = ""
        Try
            Dim ProfileID As Integer = HttpContext.Current.Session("ProfileID")
            Dim ijob_id As Integer = 0
            If (ProfileID > 0 AndAlso job_id IsNot Nothing AndAlso Integer.TryParse(job_id, ijob_id) AndAlso ijob_id > 0) Then

                Using ctx As New CMSDBDataContext(ConnectionString)


                    Try

                        ' check
                        Dim loc As EUS_ProfileJob = (From itm In ctx.EUS_ProfileJobs
                                                         Where itm.ProfileID = ProfileID AndAlso itm.JobID = ijob_id
                                                         Select itm).FirstOrDefault()

                        If (loc IsNot Nothing) Then
                            ctx.EUS_ProfileJobs.DeleteOnSubmit(loc)
                            ctx.SubmitChanges()
                        End If
                    Catch ex As Exception
                        Throw
                    End Try
                End Using
            End If

        Catch ex As Exception
            WebErrorSendEmail(ex, ex.Message)
        End Try

        Return arrayJson
    End Function

    <System.Web.Services.WebMethod()> _
    Public Shared Function GiftsPopupShown(ByVal val As Boolean) As Boolean
        Dim re As Boolean = False
        Try
            Dim ProfileID As Integer = HttpContext.Current.Session("ProfileID")
            re = ProfileHelper.SetHasSeenPopup(ProfileID, val)
            HttpContext.Current.Session("HasSeenGiftPopUp") = val
        Catch ex As Exception
            WebErrorSendEmail(ex, ex.Message)
        End Try
        Return re
    End Function
    <System.Web.Services.WebMethod()> _
    Public Shared Function NewTermsAcceptedShown(ByVal val As Boolean) As Boolean
        Dim re As Boolean = False
        Try
            Dim ProfileID As Integer = HttpContext.Current.Session("ProfileID")
            Dim MirrorProfileID As Integer = HttpContext.Current.Session("MirrorProfileID")
            re = ProfileHelper.ApproveUserNewTOS(ProfileID, MirrorProfileID, val, clsCurrentContext.GetCurrentIP, HttpContext.Current.Session.SessionID)
            HttpContext.Current.Session("HasNewTermsPopUp") = True
        Catch ex As Exception
            WebErrorSendEmail(ex, ex.Message)
        End Try
        Return re
    End Function

    <System.Web.Services.WebMethod()> _
    Public Shared Function GetProfilesMatching(ByVal LastRowNum As Integer, ByVal IsRight As Boolean) As String
        Dim re As New GetProfilesMatchingJsonResult
        Try
            If clsCurrentContext.VerifyLogin() = True Then

                Dim ProfileID As Integer = HttpContext.Current.Session("ProfileID")
                Dim dt As DataTable = clsMatchingHelper.GetMatchingProfiles(ProfileID, LastRowNum)
                If dt IsNot Nothing Then
                    If dt.Rows.Count > 0 Then
                        Dim clsResult As New GetProfilesMatchingJsonResult
                        Dim str As New StringBuilder
                        Dim ControlContainer As String = <html><![CDATA[
    <div class="###IsLeft###Profile###IsFirst###">
            <div class="###IsLeftLine###Line"></div>
            <div class="ProfileImg">
                <a class="InnerProfileImg" onclick="ShowLoading();" href="###ProfileUrl###"> 
                        <div class="RoundImage175" style="background: url('###ImageUrl###') no-repeat 50% 50%;background-size:cover;"></div>
                 </a>
                <canvas class="Heart DrawCanvas" width=53 height=43 value="###PerCs###">
                 
                </canvas>
                <div class="ProfileDetails">
                    <a class="ProfileName" onclick="ShowLoading();" href="###ProfileUrl###">
                       ###ProfileName###
                    </a>
                    <div class="ProfileAge">###ProfileAge###</div>
                </div>
            </div>
        </div>
]]></html>.Value
                        '   <div class="MatchPercent">###Percentage###</div>
                        Dim tst As String = <html><![CDATA[
<script>
$(function(){
    imagesAllLoaded();
});
</script>
]]></html>.Value
                        If dt.Columns.Contains("RowSort") AndAlso dt.Columns.Contains("ProfileId") AndAlso dt.Columns.Contains("Percents") _
                             AndAlso dt.Columns.Contains("LoginName") AndAlso dt.Columns.Contains("Birthday") AndAlso dt.Columns.Contains("FileName") Then
                            Dim sesVars As clsSessionVariables = clsSessionVariables.GetCurrent()
                            Dim othergenderId As Integer = If(sesVars.MemberData.GenderId = 1, 2, 1)
                            Dim tmpisleft As Boolean = Not IsRight
                            Dim _pageData As New clsPageData("request.json", HttpContext.Current)
                            Dim tmpAgeStr As String = _pageData.GetCustomString("lblYears")
                            For Each r As DataRow In dt.Rows
                                Dim OtherProfileid As Integer = If(r.IsNull("ProfileId"), -1, r.Item("ProfileId"))
                                Dim OtherPercentage As Integer = If(r.IsNull("Percents"), -1, r.Item("Percents"))
                                Dim RowSort As Integer = If(r.IsNull("RowSort"), -1, r.Item("RowSort"))
                                Dim OtherProfileLoginName As String = If(r.IsNull("LoginName"), "", r.Item("LoginName"))
                                Dim ImageFileName As String = If(r.IsNull("FileName"), "", r.Item("FileName"))
                                Dim OtherProfileBirthday As DateTime? = Nothing
                                If Not r.IsNull("Birthday") Then
                                    OtherProfileBirthday = r.Item("Birthday")
                                End If


                                Dim OtherProfileImageUrl As String = ProfileHelper.GetProfileImageURL(OtherProfileid, ImageFileName, othergenderId, True, False, PhotoSize.D350)

                                Dim otherProfileAgeText As String = If(OtherProfileBirthday.HasValue, ProfileHelper.GetCurrentAge(OtherProfileBirthday), 20) & " " & tmpAgeStr
                                Dim otherProfileUrl As String = System.Web.VirtualPathUtility.ToAbsolute("~/Members/Profile.aspx?p=" & HttpUtility.UrlEncode(OtherProfileLoginName))
                                Dim tmp As String = ControlContainer.Replace("###ProfileAge###", otherProfileAgeText).Replace("###ProfileName###", OtherProfileLoginName)
                                tmp = tmp.Replace("###ImageUrl###", OtherProfileImageUrl).Replace("###ProfileUrl###", otherProfileUrl)
                                tmp = tmp.Replace("###IsLeft###", If(tmpisleft, "Left ", "Right ")).Replace("###IsFirst###", If(RowSort <= 2, " First", ""))
                                tmp = tmp.Replace("###IsLeftLine###", If(RowSort = 1, "none", If(tmpisleft, "Left ", "Right ")))
                                tmp = tmp.Replace("###Num###", If(tmpisleft, "1", "2")).Replace("###PerCs###", OtherPercentage)

                                str.Append(tmp)
                                tmpisleft = Not tmpisleft
                            Next
                            '  str.Append(tst)
                            re.Html = str.ToString
                            re.lastRowNum = CInt(dt.Rows(dt.Rows.Count - 1).Item("RowSort"))
                            If dt.Rows.Count < 40 Then
                                re.NoMoreRows = True
                            End If

                            re.IsLastLeft = Not tmpisleft
                        Else
                            re.lastRowNum = LastRowNum
                            re.NoMoreRows = False
                        End If
                    Else
                        re.lastRowNum = LastRowNum
                        re.NoMoreRows = True
                    End If
                    dt.Dispose()
                End If
            End If
        Catch ex As Exception
            WebErrorSendEmail(ex, ex.Message)
        End Try
        Dim json As New Script.Serialization.JavaScriptSerializer
        Return json.Serialize(re)
    End Function
    Private Function isRefferer(ByVal MasterProfileId As Integer, ByVal MirrorProfileId As Integer) As Boolean
        Dim re As Boolean = False
        Try
            Dim _currentProfile As vw_EusProfile_Light
            Using cmdb As CMSDBDataContext = New CMSDBDataContext(ModGlobals.ConnectionString)
                _currentProfile = DataHelpers.GetCurrentProfile(cmdb, MasterProfileId, MirrorProfileId)
                re = (_currentProfile.ReferrerParentId.HasValue AndAlso _currentProfile.ReferrerParentId > 0)
            End Using
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
        Return re
    End Function
    <System.Web.Services.WebMethod()> _
    Public Shared Function LikeAsync(ByVal ID As String) As String
        Dim re As New GetErrorInfoResult
        If ID IsNot Nothing AndAlso ID.Trim.Length > 0 Then
            Try
                If clsCurrentContext.VerifyLogin() = True Then
                    Dim uncryptId As String = ""
                    Try
                        uncryptId = clsEncryptDecryptDes.decrypt(HttpContext.Current.Server.HtmlDecode(ID))
                    Catch ex As Exception
                        Return ""
                    End Try
                    Dim OtherId As Integer = -1
                    Integer.TryParse(uncryptId, OtherId)
                    If OtherId > -1 Then
                        Dim MaySendWink As Boolean = True
                        Dim MaySendWink_CheckSetting As Boolean = True
                        Dim MyPrId As Integer = CInt(HttpContext.Current.Session("ProfileID"))
                        Dim MyMirPrId As Integer = CInt(HttpContext.Current.Session("MirrorProfileID"))
                        Dim currentUserHasPhotos As Boolean = clsUserDoes.HasPhotos(MyPrId, MyMirPrId)
                        If (currentUserHasPhotos) Then


                            Dim ses As clsSessionVariables = clsSessionVariables.GetCurrent()
                            If ses Is Nothing Then
                                ses = New clsSessionVariables()
                            End If

                            MaySendWink_CheckSetting = Not clsProfilesPrivacySettings.GET_DisableReceivingLIKESFromDifferentCountryFromTo(OtherId, MyPrId)
                            If (MaySendWink_CheckSetting = False) Then
                                MaySendWink = False
                            End If

                            If (MaySendWink AndAlso ses.MemberData.GenderId = ProfileHelper.gFemaleGender.GenderId) Then
                                If (ses.MemberData.ReferrerParentID > 0) Then
                                    MaySendWink = clsUserDoes.IsReferrerAllowedSendWink(OtherId, MyPrId)
                                End If
                            End If

                            If (MaySendWink) Then
                                re.HasError = False
                                clsUserDoes.SendWink(OtherId, MyPrId)
                            Else
                                If (MaySendWink_CheckSetting = False) Then
                                    re.HasError = True
                                    re.Url = System.Web.VirtualPathUtility.ToAbsolute("~/Members/InfoWin.aspx?info=Error_Sending_Receiver_Disabled_Likes_From_Diff_Country&popup=popupWhatIs")
                                    re.Title = "Error! Can't send"
                                    re.width = 650
                                    re.height = 350
                                    re.Redirect = False
                                Else
                                    re.HasError = True
                                    re.Url = System.Web.VirtualPathUtility.ToAbsolute("~/Members/InfoWin.aspx?info=Error_Sending_Receiver_DoNot_Have_Photo&popup=popupWhatIs")
                                    re.Title = "Error! Can't send"
                                    re.width = 650
                                    re.height = 350
                                    re.Redirect = False
                                End If
                            End If
                        Else
                            re.HasError = True
                            re.Redirect = True
                        End If
                    End If
                End If
            Catch ex As Exception
                WebErrorSendEmail(ex, ex.Message)
            End Try
        End If
        Dim json As New Script.Serialization.JavaScriptSerializer
        Return json.Serialize(re)
    End Function
    <System.Web.Services.WebMethod()> _
    Public Shared Function FavoriteAsync(ByVal ID As String) As String
        Dim re As New GetErrorInfoResult
        If ID IsNot Nothing AndAlso ID.Trim.Length > 0 Then
            Try
                If clsCurrentContext.VerifyLogin() = True Then
                    Dim uncryptId As String = ""
                    Try
                        uncryptId = clsEncryptDecryptDes.decrypt(HttpContext.Current.Server.HtmlDecode(ID))
                    Catch ex As Exception
                        Return ""
                    End Try
                    Dim OtherId As Integer = -1
                    Integer.TryParse(uncryptId, OtherId)
                    If OtherId > -1 Then
                        Dim MyPrId As Integer = CInt(HttpContext.Current.Session("ProfileID"))
                        Dim MyMirPrId As Integer = CInt(HttpContext.Current.Session("MirrorProfileID"))
                        Dim currentUserHasPhotos As Boolean = clsUserDoes.HasPhotos(MyPrId, MyMirPrId)
                        If (currentUserHasPhotos) Then
                            clsUserDoes.MarkAsFavorite(OtherId, MyPrId)
                            '    Dim sesVars As clsSessionVariables = clsSessionVariables.GetCurrent()
                            'If sesVars.MemberData.PrivacySettings_NotShowInOtherUsersFavoritedList = False AndAlso clsSignalrHelper.CanNotifyForNotificationType(SignarNotificationType.Favorite, MyPrId, OtherId) Then
                            '    clsSignalrBroadcaster.Instance.BroadCastFavorite(MyPrId, OtherId, sesVars.MemberData.Country)
                            'End If
                            re.HasError = False
                        Else
                            re.HasError = True
                            re.Redirect = True
                        End If
                    End If
                End If
            Catch ex As Exception
                WebErrorSendEmail(ex, ex.Message)
            End Try
        End If
        Dim json As New Script.Serialization.JavaScriptSerializer
        Return json.Serialize(re)
    End Function
    <System.Web.Services.WebMethod()> _
    Public Shared Function UnFavoriteAsync(ByVal ID As String) As String
        Dim re As New GetErrorInfoResult
        If ID IsNot Nothing AndAlso ID.Trim.Length > 0 Then
            Try
                If clsCurrentContext.VerifyLogin() = True Then
                    Dim uncryptId As String = ""
                    Try
                        uncryptId = clsEncryptDecryptDes.decrypt(HttpContext.Current.Server.HtmlDecode(ID))
                    Catch ex As Exception
                        Return ""
                    End Try
                    Dim OtherId As Integer = -1
                    Integer.TryParse(uncryptId, OtherId)
                    If OtherId > -1 Then
                        Dim MyPrId As Integer = CInt(HttpContext.Current.Session("ProfileID"))
                        Dim MyMirPrId As Integer = CInt(HttpContext.Current.Session("MirrorProfileID"))
                        Dim currentUserHasPhotos As Boolean = clsUserDoes.HasPhotos(MyPrId, MyMirPrId)
                        If (currentUserHasPhotos) Then
                            clsUserDoes.MarkAsUnfavorite(OtherId, MyPrId)
                            re.HasError = False
                        Else
                            re.HasError = True
                            re.Redirect = True
                        End If

                    End If
                End If
            Catch ex As Exception
                WebErrorSendEmail(ex, ex.Message)
            End Try
        End If
        Dim json As New Script.Serialization.JavaScriptSerializer
        Return json.Serialize(re)
    End Function
#Region "Types"

    Public Class MemberTotalActionCounters
        Private _whoViewedMeCounter As Integer
        Public Property WhoViewedMeCounter As Integer
            Get
                Return _whoViewedMeCounter
            End Get
            Set(ByVal value As Integer)
                _whoViewedMeCounter = value
            End Set
        End Property

        Private _whoFavoritedMeCounter As Integer
        Public Property WhoFavoritedMeCounter As Integer
            Get
                Return _whoFavoritedMeCounter
            End Get
            Set(ByVal value As Integer)
                _whoFavoritedMeCounter = value
            End Set
        End Property

        Private _whoSharedPhotosCounter As Integer
        Public Property WhoSharedPhotosCounter As Integer
            Get
                Return _whoSharedPhotosCounter
            End Get
            Set(ByVal value As Integer)
                _whoSharedPhotosCounter = value
            End Set
        End Property

        Public Property HasSubscription As Boolean

        Private _creditsCounter As Integer
        Public Property CreditsCounter As Integer
            Get
                Return _creditsCounter
            End Get
            Set(ByVal value As Integer)
                _creditsCounter = value
            End Set
        End Property

        Private _newDates As Integer
        Public Property NewDates As Integer
            Get
                Return _newDates
            End Get
            Set(ByVal value As Integer)
                _newDates = value
            End Set
        End Property


        Private _newMessages As Integer
        Public Property NewMessages As Integer
            Get
                Return _newMessages
            End Get
            Set(ByVal value As Integer)
                _newMessages = value
            End Set
        End Property



        Private _newOffers As Integer
        Public Property NewOffers As Integer
            Get
                Return _newOffers
            End Get
            Set(ByVal value As Integer)
                _newOffers = value
            End Set
        End Property


        Private _newWinks As Integer
        Public Property NewWinks As Integer
            Get
                Return _newWinks
            End Get
            Set(ByVal value As Integer)
                _newWinks = value
            End Set
        End Property

    End Class

    Public Class FormlessPage
        Inherits Page

        Public Overrides Sub VerifyRenderingInServerForm(control As Control)
        End Sub

    End Class

#End Region



#Region "SignalRFunctions"
    Private Shared _globalStrings As clsPageData
    Private Shared ReadOnly Property globalStrings As clsPageData
        Get
            If (_globalStrings Is Nothing) Then
                _globalStrings = New clsPageData("GlobalStrings", HttpContext.Current)
                AddHandler _globalStrings.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _globalStrings
        End Get
    End Property
    Private Shared Sub Page_CustomStringRetrievalComplete(ByRef sender As clsPageData, ByRef Args As CustomStringRetrievalCompleteEventArgs)
        '   Dim __pageData As clsPageData = sender
        Try
            Dim result As String = Args.CustomString
            If (HttpContext.Current.Request.Url.Scheme.ToUpper() = "HTTPS") Then
                If (result.Contains("http://www.goomena.")) Then
                    result = result.Replace("http://www.goomena.", "https://www.goomena.")
                    Args.CustomString = result
                End If
                If (result.Contains("http://cdn.goomena.com")) Then
                    result = result.Replace("http://cdn.goomena.com", "https://cdn.goomena.com")
                    Args.CustomString = result
                End If
            End If
            If (Args.LagIDparam <> "US") Then
                Dim ancMatch As Match = clsWebFormHelper.AnchorRegex.Match(result, 0)
                If (ancMatch.Success) Then
                    '   Args.CustomString = Me.FixHTMLBodyAnchors(result)
                End If
            End If
        Catch ex As Exception
            WebErrorSendEmail(ex, ex.Message)
        End Try
    End Sub
#Region "Template"
    Private Shared FavoriteTemplate As String = <html><![CDATA[
     <div id="###Id###" class="Notify Favorite">
                    <div class="NotifyContent">


                        <div class="Title">###Title###</div>
                        <div class="ProfileImg">
                            <a class="InnerProfileImg" onclick="RemoveFromNotifications('###Id###');ShowLoading();" href="###ProfileUrl###">
                                <span class="RoundImage175" style="background: url('###ImageUrl###') no-repeat 50% 50%; background-size: cover;"></span>
                            </a>
                        </div>
                        <div class="ProfileDetails">
                            <div class="InnerProfileDetails">
                                <a class="ProfileName" onclick="RemoveFromNotifications('###Id###');ShowLoading();" href="###ProfileUrl###">###ProfileName###
                                </a>
                                <div class="ProfileInfo">
                                   ###ProfileInfo###
                                </div>
                            </div>
                        </div>

                        <a class="CloseButton" onclick="RemoveFromNotifications('###Id###');" href="javascript:void(0);">
                            <span class="CloseButtonImage"></span>
                        </a>
                        <div class="Clear"></div>

                    </div>
                </div>
    ]]></html>.Value
    Private Shared PhotoTemplate As String = <html><![CDATA[
<div id="###Id###" class="Notify NewPhoto">
                    <div class="NotifyContent">
                        <div class="Title">
                            <a class="InnerProfileImg" onclick="RemoveFromNotifications('###Id###');ShowLoading();" href="###ProfileUrl###">
                                <span class="RoundImage50" style="background: url('###ImageUrl###') no-repeat 50% 50%; background-size: cover;"></span>
                            </a>
                            <div class="Profile">
                                <div class="InnerProfile">
                                    <a class="ProfileName" onclick="RemoveFromNotifications('###Id###');ShowLoading();" href="###ProfileUrl###">###ProfileName###
                                    </a>
                                    <span class="ProfileInfo">###ProfileInfoSmall###
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="PhotoDetails">
                            <div class="NotifyText">
                               ###Title###
                            <span class="PhotoLevel">###PhotoLevel###</span>
                            </div>
                            <div class="PhotoUploaded">
                                <a class="InnerUploadedImage" onclick="RemoveFromNotifications('###Id###');ShowLoading();" href="###ProfileUrl###">
                                    <span class="NewPhotoUploaded" style="background: url('###NewPhotoImageUrl###') no-repeat 50% 50%; background-size: cover;"></span>
                                </a>
                            </div>
                        </div>
                        <a class="CloseButton" onclick="RemoveFromNotifications('###Id###');" href="javascript:void(0);">
                            <span class="CloseButtonImage"></span>
                        </a>
                        <div class="Clear"></div>

                    </div>
                </div>
    ]]></html>.Value

    Private Shared Function GetMemberInfoBig(ByRef dataItem As dsConnectedUsers.GetSignalRProfileRow) As String
        Dim sb As New StringBuilder()
        Try
            sb.Append("<ul>")
            sb.Append("<li>")
            Dim lagId As String = "US"
            Try
                lagId = HttpContext.Current.Session("LagID")
            Catch ex As Exception
                lagId = "US"
            End Try
            If Not AppUtils.IsDBNullOrNothingOrEmpty(dataItem.Age) Then
                Dim otherMemberAge As Integer = 20
                Dim Zodiac As String = ""

                If (Not dataItem.IsBirthdayNull) Then
                    otherMemberAge = ProfileHelper.GetCurrentAge(dataItem.Birthday)

                    Zodiac = globalStrings.GetCustomString("Zodiac" & ProfileHelper.ZodiacName(dataItem.Birthday))
                End If
                sb.Append("<b>" & otherMemberAge)
                sb.Append(" " & globalStrings.GetCustomString("lblYearsOld") & " ")
                If (Not String.IsNullOrEmpty(Zodiac) AndAlso Not dataItem.IsBirthdayNull) Then
                    sb.Append("(" & dataItem.Birthday.ToString("d MMM, yyyy") & ")")
                End If
                sb.Append("</b>")
            End If
            sb.Append("<div class=""s_item_address"">")
            Dim city As String = "", region As String = "", country As String = ""
            If Not dataItem.IsCityNull Then
                city = dataItem.City
            End If
            sb.Append(IIf(Not String.IsNullOrEmpty(city), city, ""))
            If Not dataItem.Is_RegionNull Then
                region = dataItem._Region
            End If
            sb.Append(IIf(Not String.IsNullOrEmpty(region), IIf(Not String.IsNullOrEmpty(region), ", ", "") & region, ""))
            If Not dataItem.Is_RegionNull Then
                country = dataItem.Country
            End If
            sb.Append(IIf(Not String.IsNullOrEmpty(country), IIf(Not String.IsNullOrEmpty(city) OrElse Not String.IsNullOrEmpty(region), ", ", "") & ProfileHelper.GetCountryName(country), ""))
            sb.Replace(",  " & ",  ", ",  ")
            sb.Append("</div>")
            sb.Append("</li>")
            sb.Append("<li>")
            Dim height As String = "", BodyType As String = ""
            If Not dataItem.IsPersonalInfo_HeightIDNull Then
                height = ProfileHelper.GetHeightString(dataItem.PersonalInfo_HeightID, lagId)
            End If
            If Not AppUtils.IsDBNullOrNothingOrEmpty(height) Then
                sb.Append(height)
            End If
            If Not dataItem.IsPersonalInfo_BodyTypeIDNull Then
                BodyType = ProfileHelper.GetBodyTypeString(dataItem.PersonalInfo_BodyTypeID, lagId)
            End If
            If Not AppUtils.IsDBNullOrNothingOrEmpty(height) AndAlso Not AppUtils.IsDBNullOrNothingOrEmpty(BodyType) Then
                sb.Append(" - ")
            End If
            If Not AppUtils.IsDBNullOrNothingOrEmpty(BodyType) Then
                sb.Append(BodyType)
            End If
            sb.Append("</li>")
            sb.Append("</ul>")
        Catch ex As Exception
            WebErrorSendEmail(ex, ex.Message)
        End Try
        Return sb.ToString()
    End Function

    Private Shared Function GetMemberInfoSmall(ByRef dataItem As dsConnectedUsers.GetSignalRProfileRow) As String
        Dim sb As New StringBuilder()
        Try
            If Not AppUtils.IsDBNullOrNothingOrEmpty(dataItem.Age) Then
                Dim otherMemberAge As Integer = 20
                If (Not dataItem.IsBirthdayNull) Then
                    otherMemberAge = ProfileHelper.GetCurrentAge(dataItem.Birthday)
                    sb.Append("<strong>" & otherMemberAge & "</strong>")
                End If
                sb.Append("<strong>" & otherMemberAge & "</strong>")
            End If
            Dim city As String = ""
            If Not dataItem.IsCityNull Then
                city = dataItem.City
                If sb.Length > 0 Then
                    sb.Append(", " & city)
                Else
                    sb.Append(city)
                End If
            End If
        Catch ex As Exception
            WebErrorSendEmail(ex, ex.Message)
        End Try
        Return sb.ToString()
    End Function
    Private Shared Function AddFavoriteNotification(ByVal EcryptedId As String, ByVal row As dsConnectedUsers.Eus_CustomerSignalrReceivedNotificationsRow) As String
        Dim re As String = ""
        Try
            If row IsNot Nothing Then
                Dim ds As New dsConnectedUsers
                clsSignalrHelper.GetProfile(row.FromProfileId, ds)
                If ds.GetSignalRProfile.Rows.Count > 0 Then
                    Dim r As dsConnectedUsers.GetSignalRProfileRow = ds.GetSignalRProfile.Rows(0)
                    Dim Item As String = FavoriteTemplate
                    Dim title As String = ""
                    If r.IsGenderIdNull Then
                        title = "You have been favorited"
                    Else
                        If r.GenderId = 1 Then
                            title = globalStrings.GetCustomString("HerFavorite")
                        Else
                            title = globalStrings.GetCustomString("HisFavorite")
                        End If
                    End If
                    Dim ProfileUrl As String = System.Web.VirtualPathUtility.ToAbsolute("~/Members/Profile.aspx?p=" & HttpUtility.UrlEncode(r.LoginName))
                    Dim OtherProfileImageUrl As String = ProfileHelper.GetProfileImageURL(r.ProfileID, If(r.IsFileNameNull, "", r.FileName), r.GenderId, True, False, PhotoSize.D350)
                    Item = Item.Replace("###Id###", EcryptedId).Replace("###ProfileUrl###", ProfileUrl).Replace("###ImageUrl###", OtherProfileImageUrl).Replace("###ProfileInfo###", GetMemberInfoBig(r))
                    Item = Item.Replace("###ProfileName###", r.LoginName).Replace("###Title###", title)
                    Return Item
                End If
            End If
        Catch ex As Exception
            WebErrorSendEmail(ex, ex.Message)
        End Try
        Return re
    End Function
    Private Shared Function AddNewPhotoUpload(ByVal EcryptedId As String, ByVal row As dsConnectedUsers.Eus_CustomerSignalrReceivedNotificationsRow) As String
        Dim re As String = ""
        Try
            Dim lagId As String = "US"
            Try
                lagId = HttpContext.Current.Session("LagID")
            Catch ex As Exception
                lagId = "US"
            End Try
            If row IsNot Nothing AndAlso Not row.IsCustomerPhotoIdNull AndAlso row.CustomerPhotoId > -1 Then
                Dim ds As New dsConnectedUsers
                Dim dsPhoto As New dsConnectedUsers
                clsSignalrHelper.GetProfile(row.FromProfileId, ds)
                clsSignalrHelper.FillCustomerPhoto(dsPhoto, row.CustomerPhotoId, row.FromProfileId, row.ToProfileId)
                If ds.GetSignalRProfile.Rows.Count > 0 AndAlso dsPhoto.EUS_CustomerPhotos.Rows.Count > 0 Then
                    Dim r As dsConnectedUsers.GetSignalRProfileRow = ds.GetSignalRProfile.Rows(0)
                    Dim PhotoR As dsConnectedUsers.EUS_CustomerPhotosRow = dsPhoto.EUS_CustomerPhotos.Rows(0)
                    Dim Item As String = PhotoTemplate
                    Dim allowDisplayLevel = If(PhotoR.IsAllowedLevelNull, 0, PhotoR.AllowedLevel)
                    Dim NewPhotoimageUrl As String = ""
                    If PhotoR.DisplayLevel > allowDisplayLevel Then
                        If r.GenderId = 1 Then
                            NewPhotoimageUrl = System.Web.VirtualPathUtility.ToAbsolute("~/Images2/private/list-menlevel" & PhotoR.DisplayLevel & ".png")
                        Else
                            NewPhotoimageUrl = System.Web.VirtualPathUtility.ToAbsolute("~/Images2/private/list-womenlevel" & PhotoR.DisplayLevel & ".png")
                        End If
                    Else
                        NewPhotoimageUrl = ProfileHelper.GetProfileImageURL(r.ProfileID, If(PhotoR.IsFileNameNull, "", PhotoR.FileName), r.GenderId, True, False, PhotoSize.D350)
                    End If


                    Dim lvlString As String = ""
                    If row.Count < 2 Then
                        Dim tmpLvlStr As String = ProfileHelper.GetPhotosDisplayLevelString(PhotoR.DisplayLevel, lagId)
                        If PhotoR.DisplayLevel = 0 Or (PhotoR.DisplayLevel = 1 And lagId = "US") Then
                            lvlString &= tmpLvlStr & " " & globalStrings.GetCustomString("CustomPhotο")
                        Else
                            lvlString &= tmpLvlStr
                        End If
                    End If
                    Dim title As String = If(row.Count < 2, globalStrings.GetCustomString("UploadedPhoto"), globalStrings.GetCustomString("UploadedNewPhotos").Replace("###Number###", row.Count))
                    Dim ProfileUrl As String = System.Web.VirtualPathUtility.ToAbsolute("~/Members/Profile.aspx?p=" & HttpUtility.UrlEncode(r.LoginName))
                    Dim OtherProfileImageUrl As String = ProfileHelper.GetProfileImageURL(r.ProfileID, If(r.IsFileNameNull, "", r.FileName), r.GenderId, True, False, PhotoSize.D150)
                    Item = Item.Replace("###Id###", EcryptedId).Replace("###ProfileUrl###", ProfileUrl).Replace("###ImageUrl###", OtherProfileImageUrl).Replace("###ProfileInfoSmall###", GetMemberInfoSmall(r))
                    Item = Item.Replace("###ProfileName###", r.LoginName).Replace("###NewPhotoImageUrl###", NewPhotoimageUrl).Replace("###Title###", title).Replace("###PhotoLevel###", lvlString)
                    Return Item
                End If
            End If
        Catch ex As Exception
            WebErrorSendEmail(ex, ex.Message)
        End Try
        Return re
    End Function
#End Region

    <System.Web.Services.WebMethod()> _
    Public Shared Function GetNotification(ByVal Id As String) As String
        Dim re As New SignalRMessagesResult
        Dim ItemsList As New List(Of SignalRMessageItem)
        Dim result As New StringBuilder
        Try
            If clsCurrentContext.VerifyLogin() = True Then
                Dim decryptId As String = ""
                Dim RealId As Long = -1
                If Id.Length > 0 AndAlso clsEncryptDecryptDes.decrypt(Id, decryptId) Then
                    If decryptId.Length > 0 AndAlso Long.TryParse(decryptId, RealId) Then
                        If RealId > -1 Then

                            Dim myds As New dsConnectedUsers
                            Dim ProfileID As Integer = HttpContext.Current.Session("ProfileID")
                            clsSignalrHelper.GetNotificationMessage(myds, RealId, ProfileID)
                            If myds.Eus_CustomerSignalrReceivedNotifications.Rows.Count > 0 Then
                                For Each r As dsConnectedUsers.Eus_CustomerSignalrReceivedNotificationsRow In myds.Eus_CustomerSignalrReceivedNotifications.Rows
                                    Try
                                        Select Case r.SignalrNotificationType
                                            Case SignarNotificationType.Favorite
                                                Dim itm As New SignalRMessageItem With {.NotificationType = r.SignalrNotificationType, .ItemHtml = AddFavoriteNotification(Id, r), .ItemId = Id}
                                                If itm IsNot Nothing Then
                                                    ItemsList.Add(itm)
                                                End If
                                            Case SignarNotificationType.Message
                                            Case SignarNotificationType.NewPhoto
                                                Dim itm As New SignalRMessageItem With {.NotificationType = r.SignalrNotificationType, .ItemHtml = AddNewPhotoUpload(Id, r), .ItemId = Id}
                                                If itm IsNot Nothing Then
                                                    ItemsList.Add(itm)
                                                End If
                                            Case SignarNotificationType.Register
                                            Case SignarNotificationType.Birthday
                                            Case Else
                                        End Select
                                    Catch ex As Exception
                                        WebErrorSendEmail(ex, ex.Message)
                                    End Try
                                Next
                                re.HasErrors = False
                            End If
                            Try
                                myds.Dispose()
                            Catch ex As Exception
                            End Try
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            WebErrorSendEmail(ex, ex.Message)
        End Try
        re.Items = ItemsList.ToArray
        If ItemsList.Count = 0 Then
            MarkMessageAsRead(Id)
        End If
        Dim json As New Script.Serialization.JavaScriptSerializer
        Return json.Serialize(re)
    End Function
    <System.Web.Services.WebMethod()> _
    Public Shared Function MarkMessageAsRead(ByVal Id As String) As Boolean
        Dim re As Boolean = False
        Try
            If clsCurrentContext.VerifyLogin() = True Then
                Dim decryptId As String = ""
                Dim RealId As Long = -1
                If Id.Length > 0 AndAlso clsEncryptDecryptDes.decrypt(Id, decryptId) Then
                    If decryptId.Length > 0 AndAlso Long.TryParse(decryptId, RealId) Then
                        If RealId > -1 Then
                            '     Dim myds As New dsConnectedUsers
                            Dim sess As HttpSessionState = HttpContext.Current.Session
                            Dim lastId As Integer = -1
                            If sess("LastNotificationId") IsNot Nothing AndAlso Long.TryParse(sess("LastNotificationId"), lastId) Then
                                If lastId < RealId Then
                                    sess("LastNotificationId") = RealId
                                End If
                            End If
                            re = True
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            WebErrorSendEmail(ex, ex.Message)
        End Try
        Return re
    End Function
    <System.Web.Services.WebMethod()> _
    Public Shared Function GetMyUnreadNotification() As String
        Dim re As New SignalRMessagesResult
        Dim ItemsList As New List(Of SignalRMessageItem)
        Dim result As New StringBuilder
        Dim Id As String = ""
        Try
            Dim sess As HttpSessionState = HttpContext.Current.Session
            If clsCurrentContext.VerifyLogin() = True Then
                Dim decryptId As String = ""
                Dim RealId As Long = -1
                If sess("LastNotificationId") IsNot Nothing AndAlso sess("LastNotificationId") > -1 Then
                    RealId = CLng(sess("LastNotificationId"))
                    If RealId > -1 Then
                        Dim myds As New dsConnectedUsers
                        Dim ProfileID As Integer = sess("ProfileID")
                        clsSignalrHelper.FillNotificationsMessagesByLastId(myds, RealId, ProfileID)
                        If myds.Eus_CustomerSignalrReceivedNotifications.Rows.Count > 0 Then
                            For Each r As dsConnectedUsers.Eus_CustomerSignalrReceivedNotificationsRow In myds.Eus_CustomerSignalrReceivedNotifications.Rows
                                If clsEncryptDecryptDes.encrypt(r.Eus_CustomerSignalrReceivedNotificationId, Id) Then
                                    Try
                                        Select Case r.SignalrNotificationType
                                            Case SignarNotificationType.Favorite
                                                Dim itm As New SignalRMessageItem With {.NotificationType = r.SignalrNotificationType, .ItemHtml = AddFavoriteNotification(Id, r), .ItemId = Id}
                                                If itm IsNot Nothing Then
                                                    ItemsList.Add(itm)
                                                End If
                                            Case SignarNotificationType.Message
                                            Case SignarNotificationType.NewPhoto
                                                Dim itm As New SignalRMessageItem With {.NotificationType = r.SignalrNotificationType, .ItemHtml = AddNewPhotoUpload(Id, r), .ItemId = Id}
                                                If itm IsNot Nothing Then
                                                    ItemsList.Add(itm)
                                                End If
                                            Case SignarNotificationType.Register
                                            Case SignarNotificationType.Birthday
                                            Case Else
                                        End Select
                                    Catch ex As Exception
                                        WebErrorSendEmail(ex, ex.Message)
                                    End Try
                                End If
                            Next
                            re.HasErrors = False
                        End If
                        Try
                            myds.Dispose()
                        Catch ex As Exception
                        End Try
                    End If
                Else
                    Dim ProfileID As Integer = sess("ProfileID")
                    Dim LastId As Long = clsSignalrHelper.GetLastNotificationMessageId(ProfileID)
                    sess("LastNotificationId") = LastId
                    re.HasErrors = False
                End If
            End If
        Catch ex As Exception
            WebErrorSendEmail(ex, ex.Message)
        End Try
        re.Items = ItemsList.ToArray
        Dim json As New Script.Serialization.JavaScriptSerializer
        Return json.Serialize(re)
    End Function
#End Region


End Class
Public Class GetProfilesMatchingJsonResult
    Public Property Html As String
    Public Property lastRowNum As Integer
    Public Property IsLastLeft As Boolean
    Public Property NoMoreRows As Boolean
End Class
Public Class GetErrorInfoResult
    Public Property HasError As Boolean = False
    Public Property Url As String
    Public Property Title As String
    Public Property width As Integer
    Public Property height As Integer
    Public Property Redirect As Boolean = False
End Class
Public Class SignalRMessagesResult
    Public Property HasErrors As Boolean = True
    Public Property Items As SignalRMessageItem()

End Class
Public Class SignalRMessageItem
    Public Property NotificationType As Integer
    Public Property ItemHtml As String
    Public Property ItemId As String
    Public Property FromProfileId As String
End Class
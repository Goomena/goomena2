﻿<%@ Page Title="" Language="vb" AutoEventWireup="true" MasterPageFile="~/Members/Members2Inner.Master"
    CodeBehind="CreateOffer.aspx.vb" Inherits="Dating.Server.Site.Web.CreateOffer" %>

<%@ Register Src="~/UserControls/MemberLeftPanel.ascx" TagName="MemberLeftPanel"
    TagPrefix="uc2" %>
<%@ Register Src="~/UserControls/MessageNoPhoto.ascx" TagName="MessageNoPhoto" TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/WhatIsIt.ascx" TagName="WhatIsIt" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
<div id="create-offer" class="simple-form">

    <h2 class="back-link">
        <asp:HyperLink ID="lnkBack" runat="server" Text="Back" CssClass="btn">
        </asp:HyperLink>
    </h2>
    <div class="clear"></div>

    <asp:MultiView ID="mvMainCreateOffer" runat="server" ActiveViewIndex="0">
        <asp:View ID="vwCreateOffer" runat="server">
        <div class="top-box">
            <div class="your-offer-title">
                <h2><asp:Label ID="lblCreateOfferTitle" runat="server" />
                    <asp:LinkButton ID="lnkViewDescription" runat="server" CssClass="more_info2" OnClientClick="OnMoreInfoClick('[contentUrl]', this, '[headerText]');return false;"/></h2>
                <div class="white-box-top">
                    <div class="photo pr-photo-81-noshad">
                        <asp:HyperLink ID="lnkProfImg" runat="server"><asp:Image ID="imgProf" runat="server" CssClass="round-img-81" /></asp:HyperLink>
                    </div>
                    <div class="photo-text">
                        <asp:Label ID="lblFirstDateAmountTitle" runat="server" Text="" />
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="grey-box">
                <asp:Panel ID="pnlAmountErr" runat="server" CssClass="alert alert-danger" Visible="False">
                    <asp:Label ID="lblAmountErr" runat="server" Text=""></asp:Label>
                </asp:Panel>
                <div class="grey-box-content">
                    <div class="photo">
                        <div class="pr-photo-97-noshad">
                            <asp:HyperLink ID="lnkOtherProfImg" runat="server"><asp:Image ID="imgOtherProf" runat="server" CssClass="round-img-97" /></asp:HyperLink>
                        </div>
                    </div>
                    <div class="offer_input">
                        <div class="money-box">
                            <div class="in-euro">&euro;</div>
                            <div class="in-money">
                                <dx:ASPxTextBox ID="txtAmount" runat="server" Width="70px">
                                </dx:ASPxTextBox>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="in-description">
                            <asp:Label ID="lblFirstDateEnterAmount" runat="server" Text="">
                            </asp:Label>
                        </div>
                        <div class="clear"></div>
                        <div class="in-info">
                            <asp:Label ID="lblZoneInfo" runat="server" Text="">
                            </asp:Label>
                        </div>
                        <div class="in-info">
                            <asp:Label ID="lblLastOfferAmount" runat="server" Text="">
                            </asp:Label>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="white-box-bottom">
                <div class="center">
                    <asp:Button ID="btnCreateOffer" runat="server" CssClass="btn" Text="Create Offer" />
                </div>
            </div>
        </div>
        
        <div class="bottom-box">
            <div class="offer-notes">
                <div class="top-arrow"></div>
                <asp:Label ID="lblFirstDateInfoText" runat="server" Text="">
                </asp:Label>
            </div>

            <div id="divFirstDateWarning" runat="server" class="alert alert-info" style="margin-top: 15px;"
                visible="false">
                <asp:Label ID="lblFirstDateAmountNote" runat="server" Text="">
                </asp:Label>
            </div>
            <%--<div class="poffer" id="divDatingAmount" runat="server">
                <asp:Label ID="lblDatingAmount" runat="server" Text="" />
            </div>--%>
        </div>

        </asp:View>

        <asp:View ID="vwNoPhoto" runat="server">
            <uc1:MessageNoPhoto ID="MessageNoPhoto1" runat="server" />
        </asp:View>

        <asp:View ID="vwEmpty" runat="server">
                
            <div class="m_wrap">
                <div class="items_none">
                    <div id="offers_icon" class="middle_icon"></div>
                    <div class="items_none_text">
                        <asp:Literal ID="lblActionError" runat="server"></asp:Literal>
                    </div>
                    <div class="search_members_button">
                        <asp:HyperLink ID="lnkSearchMembers" runat="server" CssClass="btn lighter" NavigateUrl="~/Members/Search.aspx"
                            onclick="ShowLoading();">
                            <asp:Literal ID="msg_SearchOurMembersText" runat="server"></asp:Literal>
                        </asp:HyperLink>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>

            <%--

            <div class="m_wrap" id="divInfo" runat="server">
                <div class="clearboth padding">
                </div>
                <div class="items_hard">
                    <asp:Literal ID="lblActionError" runat="server"></asp:Literal>
                    <p>
                        <asp:HyperLink ID="lnkSearchMembers" runat="server" CssClass="btn lighter" NavigateUrl="~/Members/Search.aspx"></asp:HyperLink></p>
                    <div class="clear">
                    </div>
                </div>
            </div>
                --%>
        </asp:View>
    </asp:MultiView>
</div>
<%--
<div class="container_12" id="content">
    <div class="grid_3 top_sidebar" id="sidebar-outter">
        <div class="left_tab" id="left_tab" runat="server">
            <div class="top_info">
            </div>
            <div class="middle" id="search">
            </div>
            <div class="bottom">
                &nbsp;</div>
        </div>
    </div>

    <div class="grid_9 body">
        <div class="middle" id="content-outter">

        </div>
		<div class="bottom"></div>
	
	</div>
    <div class="clear">
    </div>
</div>
--%>
<uc2:WhatIsIt ID="WhatIsIt1" runat="server" />
<script type="text/javascript">
    setAlertHandlers();
    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function () {
        setAlertHandlers();
    });
</script>
</asp:Content>

﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Members/Members2Inner.master" CodeBehind="VerificationDocs.aspx.vb" Inherits="Dating.Server.Site.Web.VerificationDocs" %>

<%@ Register Assembly="DevExpress.Web.v13.1, Version=13.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxUploadControl" TagPrefix="dx" %>

<%@ Register src="../UserControls/MemberLeftPanel.ascx" tagname="MemberLeftPanel" tagprefix="uc3" %>
<%@ Register src="../UserControls/VerificationDocsEdit.ascx" tagname="VerificationDocsEdit" tagprefix="uc2" %>
<%@ Register src="../UserControls/WhatIsIt.ascx" tagname="WhatIsIt" tagprefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
		<div class="container_12" id="verificatio_docs_page_wrapper">
            <div class="grid_9 body">
                <div class="middle" id="content-outter">
                    <h3 class="page-title" id="verification_page_title"><asp:Literal ID="lblItemsName" runat="server" /></h3>
                    <asp:LinkButton ID="lnkViewDescription" runat="server" CssClass="more_info2" OnClientClick="OnMoreInfoClick('[contentUrl]', this, '[headerText]');return false;" />
			        <uc2:VerificationDocsEdit ID="vde1" runat="server" Visible="true" />
			        <div class="clear"></div>
                </div>
                <div class="bottom"></div>
		    </div>
		    <div class="clear"></div>
	    </div>
	<uc2:whatisit ID="WhatIsIt1" runat="server" />
    
</asp:Content>


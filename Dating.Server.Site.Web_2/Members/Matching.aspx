﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Members/Members2Inner.Master" MaintainScrollPositionOnPostback="false"
    CodeBehind="Matching.aspx.vb" EnableEventValidation="true" Inherits="Dating.Server.Site.Web.Matching" %>

<%@ Register Assembly="DevExpress.Web.v13.1, Version=13.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>

<%--<%@ Register src="../UserControls/ProfilePreview.ascx" tagname="ProfilePreview" tagprefix="uc1" %>--%>
<%@ Register Src="~/UserControls/MemberLeftPanel.ascx" TagName="MemberLeftPanel" TagPrefix="uc3" %>
<%@ Register Src="~/UserControls/Matching/UcMatchingResults.ascx" TagPrefix="uc2" TagName="UcMatchingResults" %>






<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../v1/CSS/matching.css?44" rel="stylesheet" />
    <%-- <script src="//cdn.goomena.com/Scripts/jquery.object-fit.js" type="text/javascript"></script>--%>
      <script type="text/javascript">

        // <![CDATA[

        function onhi() {
            HideLoading();
        }



    </script>

</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">

            <uc2:UcMatchingResults runat="server" ID="UcMatchingResults" />
      
</asp:Content>


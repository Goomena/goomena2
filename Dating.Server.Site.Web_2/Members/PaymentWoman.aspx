﻿<%@ Page Title="" Language="vb" AutoEventWireup="true" MasterPageFile="~/Members/Members2Inner.Master" 
    CodeBehind="PaymentWoman.aspx.vb" Inherits="Dating.Server.Site.Web.PaymentWoman" %>
<%@ Register src="../UserControls/UsersList.ascx" tagname="UsersList" tagprefix="uc2" %>
<%@ Register src="../UserControls/MemberLeftPanel.ascx" tagname="MemberLeftPanel" tagprefix="uc3" %>
<%@ Register assembly="DevExpress.Web.v13.1, Version=13.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxDataView" tagprefix="dx" %>
<%@ Register src="../UserControls/WhatIsIt.ascx" tagname="WhatIsIt" tagprefix="uc2" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">

    <div class="main_container">
        <h1 class="main_header">
            <asp:Label ID="lblHeader" runat="server" Text=""></asp:Label>
        </h1>

        <div class="main_image_container"></div>

        <div class="main_content">
            <asp:Label ID="lblContent" runat="server" Text=""></asp:Label>
        </div>
    </div>

    <div class="men_search_link_container">
        <asp:HyperLink ID="btnSearchMen" runat="server" CssClass="men_search_link" NavigateUrl="~/Members/Search.aspx" onclick="ShowLoading();"></asp:HyperLink>
    </div>

<uc2:whatisit ID="WhatIsIt1" runat="server" />
</asp:Content>



﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Members/Members2Inner.Master"
    CodeBehind="DatesHistory.aspx.vb" Inherits="Dating.Server.Site.Web.DatesHistory" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<style type="text/css">
    body{background-image:none;}
    .dxgvHeader{font-family:'Segoe UI',Helvetica,Arial; font-size: 13px;font-weight:bold;border-bottom-width: 0px;padding: 5px 6px 7px;}
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">

    <asp:Panel ID="pnlErr" runat="server" CssClass="alert alert-danger" 
        Visible="False" EnableViewState="False">
        <asp:Label ID="lblErr" runat="server"></asp:Label>
    </asp:Panel>
                    
    <dx:ASPxGridView ID="gvHistory" runat="server" 
        AutoGenerateColumns="False" 
        DataSourceID="sdsCommunicationHistory" 
        Width="100%"
        EnableViewState="false">
        <Columns>
            <dx:GridViewDataDateColumn Caption="Date" FieldName="DateTimeToCreate" 
                ReadOnly="True" VisibleIndex="1" Width="10%">
                <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy HH:mm" 
                    EditFormat="DateTime" EditFormatString="dd/MM/yyyy HH:mm">
                </PropertiesDateEdit>
                <CellStyle Wrap="False">
                </CellStyle>
            </dx:GridViewDataDateColumn>
            <dx:GridViewDataTextColumn Caption="Action" FieldName="TypeTitle" 
                ReadOnly="True" VisibleIndex="2" Width="50">
                <DataItemTemplate>
                    <asp:Image ID="i1" runat="server" 
                        ImageUrl="~/Images2/mbr2/favorite-history.png" 
                        Visible='<%# Eval("TypeTitle")="Make favorite" %>'   
                        AlternateText='<%# Eval("TypeTitle") %>' 
                        ToolTip='<%# Eval("TypeTitle") %>' />
                    <asp:Image ID="i2" runat="server" 
                        ImageUrl="~/Images2/mbr2/like-history.png" 
                        Visible='<%# Eval("TypeTitle")="Like" %>'   
                        AlternateText='<%# Eval("TypeTitle") %>' 
                        ToolTip='<%# Eval("TypeTitle") %>' />
                    <asp:Image ID="i3" runat="server" 
                        ImageUrl="~/Images2/mbr2/message-history.png" 
                        Visible='<%# Eval("TypeTitle")="Message" %>'   
                        AlternateText='<%# Eval("TypeTitle") %>' 
                        ToolTip='<%# Eval("TypeTitle") %>' />
                    <asp:Image ID="i4" runat="server" 
                        ImageUrl="~/Images2/mbr2/offer-history.png" 
                        Visible='<%# (Eval("TypeTitle")="Offer New" OrElse Eval("TypeTitle")="Offer Counter") %>'   
                        AlternateText='<%# Eval("TypeTitle") %>' 
                        ToolTip='<%# Eval("TypeTitle") %>' />
                    <asp:Image ID="i5" runat="server" 
                        ImageUrl="~/Images2/mbr2/poke-history.png" 
                        Visible='<%# Eval("TypeTitle")="Poke" %>'   
                        AlternateText='<%# Eval("TypeTitle") %>' 
                        ToolTip='<%# Eval("TypeTitle") %>' />
                </DataItemTemplate>
                <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                </CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Direction" ReadOnly="True" 
                VisibleIndex="3" Width="50">
                <DataItemTemplate>
                    <asp:Image ID="img1" runat="server" Visible='<%# Eval("Direction")="Sent" %>' 
                        ImageUrl="~/Images2/mbr2/right-arrow.png" AlternateText='<%# Eval("Direction") %>' ToolTip='<%# Eval("Direction") %>' />
                    <asp:Image ID="img2" runat="server" Visible='<%# Eval("Direction")="Received" %>' 
                        ImageUrl="~/Images2/mbr2/left-arrow.png" AlternateText='<%# Eval("Direction") %>' ToolTip='<%# Eval("Direction") %>' />
                </DataItemTemplate>
                <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                </CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Description" ReadOnly="True" 
                VisibleIndex="4">
                <Settings AllowSort="False" />
            </dx:GridViewDataTextColumn>
        </Columns>
        <SettingsPager PageSize="10" Mode="ShowAllRecords">
        </SettingsPager>
        <Styles>
            <Header BackColor="#F0F0F0">
            </Header>
            <HeaderPanel BackColor="#F0F0F0">
            </HeaderPanel>
        </Styles>
    </dx:ASPxGridView>
    <asp:SqlDataSource ID="sdsCommunicationHistory" runat="server" ConnectionString="<%$ ConnectionStrings:AppDBconnectionString %>"
        SelectCommand="CustomerCommunicationHistory2" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:Parameter Name="MasterProfileId" Type="Int32" />
            <asp:Parameter Name="OtherProfileId" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>



        <%--<div class="container_12" id="content" style="width:100%;background-image:none;margin-top:10px;margin-bottom:20px;">
            <div class="grid_9 body" style="width:100%;margin:0;">
                <div class="middle" id="content-outter" style="padding-left:0;padding-right:0;" >
                </div>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
        </div>
        
        
        <dx:GridViewDataTextColumn FieldName="OfferOrMessageID" ReadOnly="True" 
                                Visible="False" VisibleIndex="0">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="FromProfileStatus" ReadOnly="True" 
                                Visible="False" VisibleIndex="5">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="FromLoginName" ReadOnly="True" 
                                Visible="False" VisibleIndex="6">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="ToProfileStatus" ReadOnly="True" 
                                Visible="False" VisibleIndex="7">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="ToLoginName" ReadOnly="True" 
                                Visible="False" VisibleIndex="8">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="StatusTitle" ReadOnly="True" 
                                Visible="False" VisibleIndex="9">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="OfferTypeID" ReadOnly="True" 
                                Visible="False" VisibleIndex="10">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="FromProfileID" ReadOnly="True" 
                                Visible="False" VisibleIndex="11">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="ToProfileID" ReadOnly="True" 
                                Visible="False" VisibleIndex="12">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Amount" ReadOnly="True" Visible="False" 
                                VisibleIndex="13">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="StatusID" ReadOnly="True" Visible="False" 
                                VisibleIndex="14">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="ParrentOfferID" ReadOnly="True" 
                                Visible="False" VisibleIndex="15">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Body" ReadOnly="True" Visible="False" 
                                VisibleIndex="17">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="ChildOfferID" ReadOnly="True" 
                                Visible="False" VisibleIndex="18">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Subject" ReadOnly="True" Visible="False" 
                                VisibleIndex="16">
                            </dx:GridViewDataTextColumn>--%>
</asp:Content>

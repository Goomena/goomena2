﻿
<%@ Page Title="" Language="vb" AutoEventWireup="true" MasterPageFile="~/Members/Members2Inner.Master" 
    CodeBehind="SelectPaymentTR.aspx.vb" Inherits="Dating.Server.Site.Web.SelectPaymentTR" %>
<%@ Register assembly="DevExpress.Web.v13.1, Version=13.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%--<%@ Register assembly="SpiceLogicPayPalStd" namespace="SpiceLogicPayPalStandard.Controls.BuyNowButton" tagprefix="cc1" %>
--%>
<%@ Register assembly="DevExpress.Web.v13.1, Version=13.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxPanel" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v13.1, Version=13.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxPopupControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v13.1, Version=13.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxRoundPanel" tagprefix="dx" %>
<%@ Register src="~/UserControls/UCSelectPayment2.ascx" tagname="UCSelectPayment" tagprefix="uc1" %>
<%@ Register src="~/UserControls/UCSelectPayment_TR.ascx" tagname="UCSelectPayment_TR" tagprefix="uc1" %>
<%@ Register src="~/UserControls/MemberLeftPanel.ascx" tagname="MemberLeftPanel" tagprefix="uc1" %>
<%@ Register src="~/UserControls/WhatIsIt.ascx" tagname="WhatIsIt" tagprefix="uc2" %>
<%@ Register src="~/UserControls/LiveZillaPublic.ascx" tagname="LiveZillaPublic" tagprefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<style type="text/css">
.right-side {
width: 910px;
padding-left:0px;
}
.headerTable .iframePayment2-wrap {
width: 910px;
}
.headerTable .iframePayment2 {
width: 910px;
}
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
<script type="text/javascript">
    // <![CDATA[
    (function () {
        $('.left-side-wrap').css("display", "none");
        $('#column-shadow').css("display", "none");
        $('.right-side').removeClass("lfloat").css("margin-left", "auto").css("margin-right", "auto");
    })();
    // ]]>
</script>

    <div class="clear"></div>
    <div style="margin-top:60px;">
        <%--<div style="padding:20px;">
            <div style="padding:10px;" class="blueBox"><dx:ASPxLabel ID="lblPaymentDescription" runat="server" Text="lblPaymentDescription" EncodeHtml="False"/></div>
            <div style="padding:10px;"><dx:ASPxLabel ID="lbPaymentExtraInfo" runat="server" Text="lbPaymentExtraInfo" EncodeHtml="False"/></div>
        </div>--%>
        <div runat="server" id="iframePayment_Wrap">
            <uc1:UCSelectPayment_TR ID="UCSelectPayment_TR1" runat="server" />
        </div>
    </div>


    <%--<div class="container_12" id="tabs-outter">
        <div class="grid_12 top_tabs">
            <div>
            </div>
        </div></div><div class="clear"></div>
	<div class="container_12" id="content_top"></div>
	<div class="container_12" id="content">
        <div class="grid_3 top_sidebar" id="sidebar-outter">
            <div class="left_tab" id="left_tab" runat="server">
                <div class="top_info">
                </div>
                <div class="middle" id="search">
                    <div class="submit">
                        <h3><asp:Literal ID="lblItemsName" runat="server" /></h3>
                        <div class="clear"></div>
                        <asp:LinkButton ID="lnkViewDescription" runat="server" CssClass="more_info2" OnClientClick="OnMoreInfoClick('[contentUrl]', this, '[headerText]');return false;" />
                    </div>
                </div>
                <div class="bottom">
                    &nbsp;</div></div>
                <uc1:memberleftpanel ID="leftPanel" runat="server" ShowBottomPanel="false" />
            </div>
            <div class="grid_9 body">
                <div class="middle" id="content-outter">

            <div id="filter" class="m_filter t_filter msg_filter">
	            <div class="lfloat">
		            <h2><asp:Label ID="lblPaymentMethodTitle" runat="server" Text="Report"/></h2>
	            </div>
	            <div class="rfloat">
                    <dx:ASPxHyperLink ID="lnkBack" runat="server" Text="Back" CssClass="btn">
                    </dx:ASPxHyperLink>
	            </div>

	            <div class="clear"></div>
            </div>

            <div class="t_wrap">
	            <div style="background-color:#fff">
        <uc1:UCSelectPayment ID="UCSelectPayment1" runat="server" />
        <uc1:UCSelectPayment_TR ID="UCSelectPayment_TR1" runat="server" visible="false" />
                </div>
            </div>
			    </div>
				<div class="bottom"></div>
            </div>
            <div class="clear">
            </div>
        </div>



	<uc2:whatisit ID="WhatIsIt1" runat="server" />--%>
    
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cntBodyBottom" runat="server">
    <uc3:LiveZillaPublic ID="ctlLiveZillaPublic" runat="server" />
</asp:Content>

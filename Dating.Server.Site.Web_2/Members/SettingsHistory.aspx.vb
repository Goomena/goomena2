﻿Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Core.DLL

Public Class SettingsHistory
    Inherits BasePage

#Region "Props"

    Protected Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData("~/Members/Settings.aspx", Context, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property
    Private Sub Page_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        Try
            If Me._pageData IsNot Nothing Then
                RemoveHandler Me._pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If

        Catch ex As Exception
        End Try
      
        Try
            MyBase.Dispose()
        Catch ex As Exception
        End Try
    End Sub
#End Region


    Protected Overrides Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        SetMasterPage(CurrentPageData)

        Try
            If clsCurrentContext.VerifyLogin() = True Then
                Me.MasterPageFile = "~/Members/Members2.Master"
                'If (Request("master") = "inner") Then
                '    Me.MasterPageFile = "~/Members/Members2Inner.Master"
                'Else
                'End If
            Else
                clsCurrentContext.ClearSession(Session)
                If (Not Me.IsCallback) Then
                    Response.Redirect(ResolveUrl("~/Login.aspx"), False)
                End If
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            pnlPSUserMessage.Visible = False
            'pnlNS_NotifyNewMembersErr.Visible = False

            'If (Not Page.IsPostBack) Then
            LoadLAG()
            LoadViews()
            'LoadFacebookStatus()
            'End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try
    End Sub

    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender

        Try
            Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub



    Private Sub LoadViews()

        Try
            Dim currentDate As DateTime = DateTime.UtcNow.Date.AddDays(1)
            '    Dim dateFormatted As String = currentDate.ToString("dd, MMMM yyyy")
            Try

                '   Dim DayOfMonthFullName As String = globalStrings.GetCustomString("DayOfMonthFullName", GetLag())
                ' Dim DayOfMonthFullNameArr As String() = DayOfMonthFullName.Trim().Split(vbCrLf)

                Dim curr_dateS As String = currentDate.Day
                If (GetLag() = "US") Then
                    Select Case (currentDate.Day)
                        Case 1 : curr_dateS = curr_dateS & "st"
                        Case 2 : curr_dateS = curr_dateS & "nd"
                        Case 3 : curr_dateS = curr_dateS & "rd"
                        Case Else : curr_dateS = curr_dateS & "th"
                    End Select
                End If

                '    dateFormatted = DayOfMonthFullNameArr(currentDate.Month - 1).Replace("[DAY]", curr_dateS).Replace("[YEAR]", currentDate.Year)
            Catch ex As Exception

            End Try

            Try

                Dim result As EUS_Messages_DeleteMessagesInView2_CountResult = clsMessagesHelper.COUNT_DeleteMessages_ClearHistory(Me.MasterProfileId, (IsReferrer = True), True, Nothing, currentDate)
                Dim itemsCount As String = msg_PSHistoryDeleteIncomingMessages_Count.Text
                itemsCount = itemsCount.Replace("[COUNT]", result.CountDeleted + result.CountHidden)
                'itemsCount = itemsCount.Replace("[DATE_UNTIL]", dateFormatted)
                msg_PSHistoryDeleteIncomingMessages_Count.Text = itemsCount

                If (result.CountDeleted + result.CountHidden <= 0) Then
                    btn_PSHistoryDeleteIncomingMessages.Attributes.Add("onclick", "return false;")
                End If

                result = clsMessagesHelper.COUNT_DeleteMessages_ClearHistory(Me.MasterProfileId, (IsReferrer = True), Nothing, True, currentDate)
                itemsCount = msg_PSHistoryDeleteOutgoingMessages_Count.Text
                itemsCount = itemsCount.Replace("[COUNT]", result.CountDeleted + result.CountHidden)
                msg_PSHistoryDeleteOutgoingMessages_Count.Text = itemsCount

                If (result.CountDeleted + result.CountHidden <= 0) Then
                    btn_PSHistoryDeleteOutgoingMessages.Attributes.Add("onclick", "return false;")
                End If
            Catch ex As Exception
                Throw New Exception(ex.Message, ex)
            End Try

            Try
                Dim result As EUS_Offers_DeleteLikes_CountResult = clsUserDoes.COUNT_DeleteLikes_ClearHistory(Me.MasterProfileId, currentDate)
                Dim itemsCount As String = msg_PSHistoryDeleteLIKES_Count.Text
                itemsCount = itemsCount.Replace("[COUNT]", result.CountHidden)
                msg_PSHistoryDeleteLIKES_Count.Text = itemsCount

                If (result.CountHidden <= 0) Then
                    btn_PSHistoryDeleteLIKES.Attributes.Add("onclick", "return false;")
                End If
            Catch ex As Exception
                Throw New Exception(ex.Message, ex)
            End Try


            Try
                Dim result As EUS_Offers_DeleteOffers_CountResult = clsUserDoes.COUNT_DeleteOffers_ClearHistory(Me.MasterProfileId, currentDate)
                Dim itemsCount As String = msg_PSHistoryDeleteOffers_Count.Text
                itemsCount = itemsCount.Replace("[COUNT]", result.CountHidden)
                msg_PSHistoryDeleteOffers_Count.Text = itemsCount

                If (result.CountHidden <= 0) Then
                    btn_PSHistoryDeleteOffers.Attributes.Add("onclick", "return false;")
                End If
            Catch ex As Exception
                Throw New Exception(ex.Message, ex)
            End Try

            Try
                Dim result As EUS_Offers_DeleteDates_CountResult = clsUserDoes.COUNT_DeleteDates_ClearHistory(Me.MasterProfileId, currentDate)
                Dim itemsCount As String = msg_PSHistoryDeleteDates_Count.Text
                itemsCount = itemsCount.Replace("[COUNT]", result.CountHidden)
                msg_PSHistoryDeleteDates_Count.Text = itemsCount

                If (result.CountHidden <= 0) Then
                    btn_PSHistoryDeleteDates.Attributes.Add("onclick", "return false;")
                End If
            Catch ex As Exception
                Throw New Exception(ex.Message, ex)
            End Try


            Try
                Dim result As EUS_ProfilePhotosLevel_DeletePhotosShares_CountResult = clsUserDoes.COUNT_DeletePhotosShares_ClearHistory(Me.MasterProfileId, Nothing, True, currentDate)
                Dim itemsCount As String = msg_PSHistoryDeleteMySharedPhotos_Count.Text
                itemsCount = itemsCount.Replace("[COUNT]", result.CountDeleted)
                msg_PSHistoryDeleteMySharedPhotos_Count.Text = itemsCount

                If (result.CountDeleted <= 0) Then
                    btn_PSHistoryDeleteMySharedPhotos.Attributes.Add("onclick", "return false;")
                End If
            Catch ex As Exception
                Throw New Exception(ex.Message, ex)
            End Try


            Try
                Dim result As EUS_ProfilePhotosLevel_DeletePhotosShares_CountResult = clsUserDoes.COUNT_DeletePhotosShares_ClearHistory(Me.MasterProfileId, True, Nothing, currentDate)
                Dim itemsCount As String = msg_PSHistoryDeleteSharedPhotosByOther_Count.Text
                itemsCount = itemsCount.Replace("[COUNT]", result.CountDeleted)
                msg_PSHistoryDeleteSharedPhotosByOther_Count.Text = itemsCount

                If (result.CountDeleted <= 0) Then
                    btn_PSHistoryDeleteSharedPhotosByOther.Attributes.Add("onclick", "return false;")
                End If
            Catch ex As Exception
                Throw New Exception(ex.Message, ex)
            End Try


            Try
                Dim result As EUS_ProfilesViewed_DeleteViews_CountResult = clsUserDoes.COUNT_DeleteProfilesViewed_ClearHistory(Me.MasterProfileId, Nothing, True, currentDate)
                Dim itemsCount As String = msg_PSHistoryDeleteProfilesViewedByME_Count.Text
                itemsCount = itemsCount.Replace("[COUNT]", result.CountHidden)
                msg_PSHistoryDeleteProfilesViewedByME_Count.Text = itemsCount

                If (result.CountHidden <= 0) Then
                    btn_PSHistoryDeleteProfilesViewedByME.Attributes.Add("onclick", "return false;")
                End If
            Catch ex As Exception
                Throw New Exception(ex.Message, ex)
            End Try


            Try
                Dim result As EUS_ProfilesViewed_DeleteViews_CountResult = clsUserDoes.COUNT_DeleteProfilesViewed_ClearHistory(Me.MasterProfileId, True, Nothing, currentDate)
                Dim itemsCount As String = msg_PSHistoryDeleteProfilesViewedByOther_Count.Text
                itemsCount = itemsCount.Replace("[COUNT]", result.CountHidden)
                msg_PSHistoryDeleteProfilesViewedByOther_Count.Text = itemsCount

                If (result.CountHidden <= 0) Then
                    btn_PSHistoryDeleteProfilesViewedByOther.Attributes.Add("onclick", "return false;")
                End If
            Catch ex As Exception
                Throw New Exception(ex.Message, ex)
            End Try


            ' ''vw=CREDITS
            ''txtEmail.ValidationSettings.RegularExpression.ValidationExpression = gEmailAddressValidationRegex
            'If (clsConfigValues.Get__validate_email_address_normal()) Then
            '    txtEmail.ValidationSettings.RegularExpression.ValidationExpression = gEmailAddressValidationRegex
            'Else
            '    txtEmail.ValidationSettings.RegularExpression.ValidationExpression = gEmailAddressValidationRegex_Simple
            'End If

            ''Dim sqlProc As String = "EXEC [CustomerAvailableCredits] @CustomerId = " & MasterProfileId
            ''Dim dt As DataTable = DataHelpers.GetDataTable(sqlProc)

            'Dim __CustomerAvailableCredits As clsCustomerAvailableCredits = BasePage.GetCustomerAvailableCredits(Me.MasterProfileId)
            'If (__CustomerAvailableCredits.CreditsRecordsCount > 0) Then
            '    liCredits.Visible = True

            '    If (Request.QueryString("vw") IsNot Nothing) Then
            '        If (Request.QueryString("vw").ToUpper() = "CREDITS") Then
            '            mvSettings.SetActiveView(vwCredits)
            '        End If
            '    End If

            'Else
            '    liCredits.Visible = False
            'End If

            'If (Request.QueryString("vw") IsNot Nothing AndAlso Request.QueryString("vw").ToUpper() = "ACCOUNT") Then
            '    mvSettings.SetActiveView(vwAccountSettings)
            'End If

            'If (Request.QueryString("vw") IsNot Nothing AndAlso Request.QueryString("vw").ToUpper() = "PRIVACY") Then
            '    mvSettings.SetActiveView(vwPrivacy)
            'End If

            'If (Request.QueryString("vw") IsNot Nothing AndAlso Request.QueryString("vw").ToUpper() = "NOTIFICATIONS") Then
            '    mvSettings.SetActiveView(vwNotifications)
            'End If

            'If (mvSettings.ActiveViewIndex = -1) Then
            '    mvSettings.ActiveViewIndex = 0
            'End If

            'ShowCriteriasPopUp.Windows(0).ContentUrl = "~/Members/AutoNotificationsSettings.aspx?popup=" & ShowCriteriasPopUp.ClientID
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
    End Sub



    Protected Sub LoadLAG()
        Try

            SetControlsValue(Me, CurrentPageData)

            btn_PSHistoryDeleteIncomingMessages.Text = Me.CurrentPageData.GetCustomString("btn_PSHistoryDeleteItem")
            btn_PSHistoryDeleteOutgoingMessages.Text = btn_PSHistoryDeleteIncomingMessages.Text
            btn_PSHistoryDeleteLIKES.Text = btn_PSHistoryDeleteIncomingMessages.Text
            btn_PSHistoryDeleteOffers.Text = btn_PSHistoryDeleteIncomingMessages.Text
            btn_PSHistoryDeleteDates.Text = btn_PSHistoryDeleteIncomingMessages.Text
            btn_PSHistoryDeleteMySharedPhotos.Text = btn_PSHistoryDeleteIncomingMessages.Text
            btn_PSHistoryDeleteSharedPhotosByOther.Text = btn_PSHistoryDeleteIncomingMessages.Text
            btn_PSHistoryDeleteProfilesViewedByME.Text = btn_PSHistoryDeleteIncomingMessages.Text
            btn_PSHistoryDeleteProfilesViewedByOther.Text = btn_PSHistoryDeleteIncomingMessages.Text
            lnkHistoryBack.Text = Me.CurrentPageData.GetCustomString("lnkHistoryBack")

            msg_PSHistoryDeleteIncomingMessages_Count.Text = Me.CurrentPageData.GetCustomString("Incoming.Messages.Delete.Count")
            msg_PSHistoryDeleteOutgoingMessages_Count.Text = Me.CurrentPageData.GetCustomString("Outgoing.Messages.Delete.Count")
            msg_PSHistoryDeleteLIKES_Count.Text = Me.CurrentPageData.GetCustomString("Likes.Delete.Count")
            msg_PSHistoryDeleteOffers_Count.Text = Me.CurrentPageData.GetCustomString("Offers.Delete.Count")
            msg_PSHistoryDeleteDates_Count.Text = Me.CurrentPageData.GetCustomString("Dates.Delete.Count")
            msg_PSHistoryDeleteMySharedPhotos_Count.Text = Me.CurrentPageData.GetCustomString("My.Shared.Photos.Delete.Count")
            msg_PSHistoryDeleteSharedPhotosByOther_Count.Text = Me.CurrentPageData.GetCustomString("Other.Shared.Photos.Delete.Count")
            msg_PSHistoryDeleteProfilesViewedByME_Count.Text = Me.CurrentPageData.GetCustomString("My.Viewed.Profiles.Delete.Count")
            msg_PSHistoryDeleteProfilesViewedByOther_Count.Text = Me.CurrentPageData.GetCustomString("Other.Viewed.Profiles.Delete.Count")


        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    'Protected Sub btnPrivacySettingsUpd_Click(sender As Object, e As EventArgs) Handles btnPrivacySettingsUpd.Click
    '    SaveProfile_PrivacySettings(Me.MasterProfileId)
    '    LoadProfile_PrivacySettings(Me.MasterProfileId)
    'End Sub



    'Protected Sub btnNotificationsUpdate_Click(sender As Object, e As EventArgs) Handles btnNotificationsUpdate.Click
    '    SaveProfile_NotificationSettings(Me.MasterProfileId)
    '    LoadProfile_NotificationSettings(Me.MasterProfileId)

    'End Sub


    'Function ValidateInput_EUS_AutoNotificationSettings(ByVal profileId As Integer)
    '    Dim success As Boolean

    '    Try

    '        Dim settings As EUS_AutoNotificationSetting = (From itm In Me.CMSDBDataContext.EUS_AutoNotificationSettings
    '                                          Where itm.CustomerID = Me.MasterProfileId
    '                                          Select itm).FirstOrDefault()
    '        If (settings Is Nothing) Then
    '            success = False
    '        Else
    '            success = True
    '        End If

    '    Catch ex As Exception

    '    End Try


    '    Return success
    'End Function


    'Sub SaveProfile_NotificationSettings(ByVal profileId As Integer)


    '    Try
    '        Dim messageSet As Boolean = False
    '        Dim _profilerows As New clsProfileRows(profileId)

    '        _profilerows.GetMasterRow().NotificationsSettings_WhenLikeReceived = chkNS_NotifyNewLikes.Checked
    '        _profilerows.GetMasterRow().NotificationsSettings_WhenNewMessageReceived = chkNS_NotifyOnNewMessage.Checked
    '        _profilerows.GetMasterRow().NotificationsSettings_WhenNewOfferReceived = chkNS_NotifyNewUpdatedOffers.Checked

    '        _profilerows.GetMasterRow().LastUpdateProfileDateTime = DateTime.UtcNow
    '        _profilerows.GetMasterRow().LastUpdateProfileIP = Request.Params("REMOTE_ADDR")
    '        _profilerows.GetMasterRow().LastUpdateProfileGEOInfo = Session("GEO_COUNTRY_CODE")


    '        _profilerows.GetMirrorRow().NotificationsSettings_WhenLikeReceived = chkNS_NotifyNewLikes.Checked
    '        _profilerows.GetMirrorRow().NotificationsSettings_WhenNewMessageReceived = chkNS_NotifyOnNewMessage.Checked
    '        _profilerows.GetMirrorRow().NotificationsSettings_WhenNewOfferReceived = chkNS_NotifyNewUpdatedOffers.Checked

    '        _profilerows.GetMirrorRow().LastUpdateProfileDateTime = DateTime.UtcNow
    '        _profilerows.GetMirrorRow().LastUpdateProfileIP = Request.Params("REMOTE_ADDR")
    '        _profilerows.GetMirrorRow().LastUpdateProfileGEOInfo = Session("GEO_COUNTRY_CODE")

    '        If (chkNS_NotifyNewMembers.Checked) Then
    '            If (Not ValidateInput_EUS_AutoNotificationSettings(Me.MasterProfileId)) Then
    '                'lblNS_NotifyNewMembersErr.Text = Me.CurrentPageData.GetCustomString("ErrorNotificationSettingsNotDefined")
    '                'pnlNS_NotifyNewMembersErr.Visible = True
    '                'pnlNS_NotifyNewMembersErr.CssClass = "alert"
    '                chkNS_NotifyNewMembers.Checked = False

    '                lblNSUserMessage.Text = Me.CurrentPageData.GetCustomString("ErrorNotificationSettingsNotDefined")
    '                pnlNSUserMessage.Visible = True
    '                pnlNSUserMessage.CssClass = "alert"

    '                messageSet = True
    '            End If
    '        End If

    '        _profilerows.GetMasterRow().NotificationsSettings_WhenNewMembersNearMe = chkNS_NotifyNewMembers.Checked
    '        _profilerows.GetMirrorRow().NotificationsSettings_WhenNewMembersNearMe = chkNS_NotifyNewMembers.Checked


    '        _profilerows.Update()

    '        If (Not messageSet) Then
    '            lblNSUserMessage.Text = Me.CurrentPageData.GetCustomString("UserMessageNotificationSettingsUpdated")
    '            pnlNSUserMessage.Visible = True
    '            pnlNSUserMessage.CssClass = "alert alert-success blue"
    '        End If
    '    Catch ex As Exception
    '        WebErrorMessageBox(Me, ex, "")
    '        lblNSUserMessage.Text = Me.CurrentPageData.GetCustomString("ErrorNotificationSettingsUpdateFailed")
    '        pnlNSUserMessage.Visible = True
    '        pnlNSUserMessage.CssClass = "alert alert-danger"
    '    End Try
    'End Sub


    '    Sub LoadProfile_NotificationSettings(ByVal profileId As Integer)
    '        Try

    '            Dim _profilerows As New clsProfileRows(profileId)
    '            Dim mirrorRow As EUS_ProfilesRow = _profilerows.GetMirrorRow()

    '            If (Not mirrorRow.IsNotificationsSettings_WhenLikeReceivedNull()) Then chkNS_NotifyNewLikes.Checked = mirrorRow.NotificationsSettings_WhenLikeReceived
    '            If (Not mirrorRow.IsNotificationsSettings_WhenNewMembersNearMeNull()) Then
    '                chkNS_NotifyNewMembers.Checked = mirrorRow.NotificationsSettings_WhenNewMembersNearMe
    '            End If
    '            If (Not mirrorRow.IsNotificationsSettings_WhenNewMessageReceivedNull()) Then chkNS_NotifyOnNewMessage.Checked = mirrorRow.NotificationsSettings_WhenNewMessageReceived
    '            If (Not mirrorRow.IsNotificationsSettings_WhenNewOfferReceivedNull()) Then chkNS_NotifyNewUpdatedOffers.Checked = mirrorRow.NotificationsSettings_WhenNewOfferReceived

    '            Dim settings As EUS_AutoNotificationSetting = (From itm In Me.CMSDBDataContext.EUS_AutoNotificationSettings
    '                                              Where itm.CustomerID = Me.MasterProfileId
    '                                              Select itm).FirstOrDefault()

    '            ' when setting checkbox is checked show criterias 
    '            If (settings Is Nothing) Then
    '                chkNS_NotifyNewMembers.ClientSideEvents.CheckedChanged = <js><![CDATA[
    'function(s, e) {
    '    if(s.GetChecked()==true){
    '        SetPCVisible(true, '#SHOWCRITERIASPOPUP#', #INDEX#)

    '		//window['#SHOWCRITERIASPOPUP#'].ShowWindow(0);
    '		//window['#SHOWCRITERIASPOPUP#'].Show();
    '    }
    '}
    ']]></js>
    '                'function(s, e) {
    '                '	if(s.GetChecked()==true){
    '                '		//window['#SHOWCRITERIASPOPUP#'].ShowWindow(0);
    '                '		window['#SHOWCRITERIASPOPUP#'].Show();
    '                '	}winAutoNotificationsSettings
    '                '}
    '                chkNS_NotifyNewMembers.ClientSideEvents.CheckedChanged = chkNS_NotifyNewMembers.ClientSideEvents.CheckedChanged.Replace("#SHOWCRITERIASPOPUP#", ShowCriteriasPopUp.ClientID)
    '                chkNS_NotifyNewMembers.ClientSideEvents.CheckedChanged = chkNS_NotifyNewMembers.ClientSideEvents.CheckedChanged.Replace("#INDEX#", 0)
    '            End If

    '            lnkShowMembersThatFillsCriteria.Visible = False
    '            If (chkNS_NotifyNewMembers.Checked) Then

    '                If (settings IsNot Nothing) Then
    '                    Dim dt As DataTable = clsSearchHelper.GetMembersThatShouldBeNotifiedForNewMember(Me.MasterProfileId)

    '                    Dim textPattern As String = <html><![CDATA[
    '#TEXT# <span id="notificationsCountWrapper" class="jewelCount"><span class="countValue">#COUNT#</span></span>
    ']]></html>.Value
    '                    lnkShowMembersThatFillsCriteria.Text = CurrentPageData.GetCustomString("lnkShowMembersThatFillsCriteria")
    '                    If (dt.Rows.Count > 0) Then
    '                        lnkShowMembersThatFillsCriteria.Text = textPattern.Replace("#TEXT#", lnkShowMembersThatFillsCriteria.Text).Replace("#COUNT#", dt.Rows.Count)
    '                        lnkShowMembersThatFillsCriteria.NavigateUrl = "~/Members/Search.aspx?result=autonotif"
    '                        lnkShowMembersThatFillsCriteria.Visible = True
    '                    End If
    '                End If
    '            End If


    '        Catch ex As Exception
    '            WebErrorMessageBox(Me, ex, "")
    '        End Try
    '    End Sub


    'Protected Sub mvSettings_ActiveViewChanged(sender As Object, e As EventArgs) Handles mvSettings.ActiveViewChanged


    '    If (mvSettings.GetActiveView().Equals(vwAccountSettings)) Then

    '        liAccSett.Attributes.Add("class", "down")

    '        liPrivacy.Attributes.Remove("class")
    '        liNotifications.Attributes.Remove("class")
    '        liCredits.Attributes.Remove("class")


    '    ElseIf (mvSettings.GetActiveView().Equals(vwPrivacy)) Then

    '        liPrivacy.Attributes.Add("class", "down")

    '        liAccSett.Attributes.Remove("class")
    '        liNotifications.Attributes.Remove("class")
    '        liCredits.Attributes.Remove("class")

    '        LoadProfile_PrivacySettings(Me.MasterProfileId)

    '    ElseIf (mvSettings.GetActiveView().Equals(vwNotifications)) Then

    '        liNotifications.Attributes.Add("class", "down")

    '        liAccSett.Attributes.Remove("class")
    '        liPrivacy.Attributes.Remove("class")
    '        liCredits.Attributes.Remove("class")

    '        LoadProfile_NotificationSettings(Me.MasterProfileId)

    '    ElseIf (mvSettings.GetActiveView().Equals(vwCredits)) Then

    '        liCredits.Attributes.Add("class", "down")

    '        liAccSett.Attributes.Remove("class")
    '        liPrivacy.Attributes.Remove("class")
    '        liNotifications.Attributes.Remove("class")

    '        gvHistory.Bind()
    '    End If

    'End Sub



End Class
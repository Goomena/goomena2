﻿Imports Dating.Server.Core.DLL
Public Class ReAdminLogin
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim crypt As New Library.Public.clsCrypoPassword()
        If Request("UserName") IsNot Nothing AndAlso Request("Password") IsNot Nothing Then


            Dim username As String = Request("UserName")
            Dim password As String = Request("Password")
            username = crypt.DeCrypPassword(username)
            password = crypt.DeCrypPassword(password)
            Try
                Dim result As Boolean = clsSignalrHelper.PerformAdminSiteLogin(username, password)
                If result Then
                    Dim ticket As New FormsAuthenticationTicket(2, username, DateTime.Now, DateTime.Now.AddDays(5), True, "AdminUserClientHub", FormsAuthentication.FormsCookiePath)
                    Dim hashCookies As String = FormsAuthentication.Encrypt(ticket)
                    Dim cookie As New HttpCookie(FormsAuthentication.FormsCookieName, hashCookies)
                    cookie.Path = FormsAuthentication.FormsCookiePath()
                    cookie.Expires = ticket.Expiration
                    cookie.Domain = Nothing

                    ' Context.Response.Cookies.
                    HttpContext.Current.Response.Cookies.Remove(cookie.Name)
                    HttpContext.Current.Response.Cookies.Add(cookie)
                    '    FormsAuthentication.SetAuthCookie(username, False)
                End If

            Catch ex As Exception
                WebErrorSendEmail(ex, ex.Message)
            End Try
        End If
    End Sub

End Class
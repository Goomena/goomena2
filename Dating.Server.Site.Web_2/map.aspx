﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="map.aspx.vb" Inherits="Dating.Server.Site.Web.map" %>


<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    <style type="text/css">
      html { height: 100% }
      body { height: 100%; margin: 0; padding: 0 }
      #map_canvas { height: 100% }
    </style>
      <%
          If (Request.Url.Scheme = "https") Then
      %><script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<%= System.Configuration.ConfigurationManager.AppSettings("googlemaps.api.key") %>&sensor=false">
      </script><%
               Else
      %><script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=<%= System.Configuration.ConfigurationManager.AppSettings("googlemaps.api.key") %>&sensor=false">
      </script><%
               End If
      %>

    
  </head>
  <body>
    <div id="map_canvas" style="width:100%; height:100%"></div>
    <form id="form1" runat="server">
    </form>
    <script type="text/javascript">

//        function initialize() {
//            var myOptions = {
//                center: new google.maps.LatLng(38.01321562, 23.75522601),
//                zoom: 8,
//                mapTypeId: google.maps.MapTypeId.ROADMAP
//            };
//            var map = new google.maps.Map(document.getElementById("map_canvas"),
//            myOptions);
//    }

        /**
        * Called on the initial page load.
        */
        function initialize() {
            var _zoom = <%= GetZoom() %>;
            var _lat = <%= GetLat() %>;
            var _lng = <%= GetLng() %>;
            var _radius = <%= GetRadius() %>;

            var mapCenter = new google.maps.LatLng(_lat, _lng);
            var map = new google.maps.Map(document.getElementById('map_canvas'), {
                'zoom': _zoom,
                'center': mapCenter,
                'mapTypeId': google.maps.MapTypeId.ROADMAP
            });

            // Create a draggable marker which will later on be binded to a
            // Circle overlay.
            var marker = new google.maps.Marker({
                map: map,
                position: new google.maps.LatLng(_lat, _lng),
                //position: new google.maps.LatLng(38.01321562, 23.75522601),
                draggable: true
            });

            // Add a Circle overlay to the map.
            var circle = new google.maps.Circle({
                map: map,
                radius: _radius // km
                //radius: 10000 // 100 km
            });

            // Since Circle and Marker both extend MVCObject, you can bind them
            // together using MVCObject's bindTo() method.  Here, we're binding
            // the Circle's center to the Marker's position.
            // http://code.google.com/apis/maps/documentation/v3/reference.html#MVCObject
            circle.bindTo('center', marker, 'position');
        }

        // Register an event listener to fire when the page finishes loading.
        google.maps.event.addDomListener(window, 'load', initialize);

    </script>
  </body>
</html>


<%--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    </div>
    </form>
</body>
</html>
--%>
<%--
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Postcode Radius - Map</title>

<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;sensor=true&amp;key=ABQIAAAAfD_vh-Lw9j7933bRx6W-CRTwKGP8wSRiCKGH60OogBkQKirshhT3W4Yf0nsUrl8uLTLbeunql-8ErA" type="text/javascript"></script>


</head>

<body style="margin: 0 0 0 0;">
    <form id="form1" runat="server">
    <div>
    
    </div>
    </form>

<div id="map" style="width:314px; height:183px; border: solid 1px #193333;" ></div>

<script type="text/javascript">
//<![CDATA[

    _mPreferMetric = true;

    var map = new GMap3(document.getElementById("map"));
    var start = new GLatLng(38.01321562, 23.75522601);
    map.setCenter(start, 10);
    //map.addControl(new GMapTypeControl(1));
    //map.addControl(new GLargeMapControl());
    //map.addControl(new GScaleControl(256));
    new GKeyboardHandler(map);
    map.enableContinuousZoom();
    map.enableDoubleClickZoom();

    ////pan and zoom to fit

    var bounds = new GLatLngBounds();
    function fit() {
        map.panTo(bounds.getCenter());
        map.setZoom(map.getBoundsZoomLevel(bounds));
    }

    ///Geo

    var geocoder = new GClientGeocoder();

    function showAddress(address) {
        geocoder.getLatLng(address, function (point) {
            if (!point) {
                document.getElementById('haku').style.color = 'red';
            }
            else {
                draw(point);
                map.panTo(point);
                document.getElementById('haku').style.color = 'black';
            } 
        })
    }

    // Esa's Auto ZoomOut

    function esasZoomOut() {
        var paragraphs = map.getContainer().getElementsByTagName('p').length;
        //GLog.write(paragraphs);
        if (paragraphs > 6) {
            map.zoomOut();
        } 
    }
    var interval = setInterval("esasZoomOut()", 500);

    //calling circle drawing function
    function draw(pnt) {
        map.clearOverlays();
        bounds = new GLatLngBounds();
        var givenRad = 16.1;
        var givenQuality = 40;
        var centre = pnt || map.getCenter()
        drawCircle(centre, givenRad, givenQuality);
        fit();
    }

    ////////////////////////// circle///////////////////////////////

    function drawCircle(center, radius, nodes, liColor, liWidth, liOpa, fillColor, fillOpa) {
        // Esa 2006
        //calculating km/degree
        var latConv = center.distanceFrom(new GLatLng(center.lat() + 0.1, center.lng())) / 100;
        var lngConv = center.distanceFrom(new GLatLng(center.lat(), center.lng() + 0.1)) / 100;

        //Loop 
        var points = [];
        var step = parseInt(360 / nodes) || 10;
        for (var i = 0; i <= 360; i += step) {
            var pint = new GLatLng(center.lat() + (radius / latConv * Math.cos(i * Math.PI / 180)), center.lng() +
	(radius / lngConv * Math.sin(i * Math.PI / 180)));
            points.push(pint);
            bounds.extend(pint); //this is for fit function
        }
        points.push(points[0]); // Closes the circle, thanks Martin
        fillColor = fillColor || liColor || "#00CCCC";
        liWidth = liWidth || 2;
        var poly = new GPolygon(points, liColor, liWidth, liOpa, fillColor, fillOpa);
        map.addOverlay(poly);
        //marker = new GMarker(center);
        //map.addOverlay(marker);
        //marker.openInfoWindowHtml("Text");
        //marker.show();	
    }
    /////////////////////////////////////////////////////////////////////

    draw();

//]]>

</script>

</body>

</html>--%>
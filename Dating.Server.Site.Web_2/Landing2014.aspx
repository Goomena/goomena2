﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Root2.Master" CodeBehind="Landing2014.aspx.vb" Inherits="Dating.Server.Site.Web.Landing2014" %>
<%@ Register src="UserControls/ucLandingProfiles2014.ascx" tagname="ucLandingProfiles2014" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link type="text/css" rel="Stylesheet" href="/v1/CSS/landing2014.css?v=3.3"/>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="insidemosaiccontainer" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="content" runat="server">

    <asp:Panel id="all_sections" runat="server" EnableViewState="false">
        <div class="section-1-ext">
            <div class="section-1 <%= LagID%>">
                <asp:Literal ID="lblTopImage" runat="server"></asp:Literal>
            </div>
        </div>

        <div class="section-2">
            <div class="landingcontainer <%= LagID%>">
                <asp:Label ID="lblHeader" runat="server" Text="" CssClass="landing-content"></asp:Label>
            </div>
        </div>

        <div class="section-3-ext">
            <div class="section-3">
                <div class="members-photos">
                    <asp:Panel ID="pnlMosaic" CssClass="mosaic" runat="server">
		                <div runat="server" class="pic" id="pic1"></div>
		                <div runat="server" class="pic" id="pic2"></div>
		                <div runat="server" class="pic" id="pic3"></div>
		                <div runat="server" class="pic" id="pic4"></div>
		                <div runat="server" class="pic" id="pic5"></div>
		                <div runat="server" class="pic" id="pic6"></div>
		                <div runat="server" class="pic" id="pic7"></div>
		                <div runat="server" class="pic" id="pic8"></div>
		                <div runat="server" class="pic" id="pic9"></div>
		                <div runat="server" class="pic" id="pic10"></div>
		                <div runat="server" class="pic" id="pic11"></div>
		                <div runat="server" class="pic" id="pic12"></div>
		                <div runat="server" class="pic" id="pic13"></div>
		                <div runat="server" class="pic" id="pic14"></div>
		                <div runat="server" class="pic" id="pic15"></div>
		                <div runat="server" class="pic" id="pic16"></div>
		                <div runat="server" class="pic" id="pic17"></div>
		                <div runat="server" class="pic" id="pic18"></div>
		                <div runat="server" class="pic" id="pic19"></div>
                        <div class="clear"></div>
                     </asp:Panel> 
                </div>
            </div>
        </div> 

        <div class="section-2">
            <div class="landingcontainer <%= LagID%>">
                <asp:Label ID="lblBody" runat="server" Text="" CssClass="landing-content"></asp:Label>
            </div>
        </div>

                          

    <div class="PhotoInfoWrap hidden">
        <div id="PhotoInfos" class="PhotoInfos">
            <img class="InfoImg" alt="" src="" />
        </div>
        <p class="ProfileID"></p>
        <p class="ProfileCity"></p>
        <div class="ProfileAgeWrap">
            <p class="ProfileAgeText"></p>
            <p class="ProfileAge"></p>
        </div>
    </div>

    <script type="text/javascript">
        var profileBaseUrl = '<%= MyBase.LanguageHelper.GetPublicURL("/profile/", MyBase.GetLag()) %>';

        if (!stringIsEmpty(profileBaseUrl)) {
            if (profileBaseUrl.indexOf("?") > -1) profileBaseUrl = profileBaseUrl.substring(0, profileBaseUrl.indexOf("?"));
        }

        function checkBorders(e) {
            var itmsExt = ['.section-1-ext','.section-3-ext'];
            var itms = ['.section-1', '.section-3'];
            var widthCheck = [1424 + 20, 2004 + 20];

            for (var i = 0; i < itmsExt.length; i++) {
                var itmExt = $(itmsExt[i]);
                var itm = $(itms[i], itmsExt[i]);
                var w = itmExt.width();
                if (w >= widthCheck[i] && !itm.is('.ext-borders')) {
                    itm.addClass('ext-borders');
                }
                else if (w < widthCheck[i] && itm.is('.ext-borders')) {
                    itm.removeClass('ext-borders');
                }
            }
        }

        $(checkBorders);
        $(window).on('resize', checkBorders);
    </script>  
    <script type="text/javascript" src="/v1/Scripts/mosaic.js"></script>  

    <uc1:ucLandingProfiles2014 ID="ucLP2014" runat="server" />
</asp:Panel> 

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cntBodyBottom" runat="server">
</asp:Content>

﻿'Imports Sites.SharedSiteCode
Imports Library.Public
Imports Dating.Server.Core.DLL


Public Class UCSelectProductTR
    Inherits BaseUserControl

    Protected Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            'If (_pageData Is Nothing) Then _pageData = New clsPageData("control.UCSelectProduct", Context)
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData("control.UCSelectProduct", Context, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property

    Private Sub Page_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        Try
            If Me._pageData IsNot Nothing Then
                RemoveHandler Me._pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If

        Catch ex As Exception
        End Try

        Try
            MyBase.Dispose()
        Catch ex As Exception
        End Try
    End Sub

    Dim _control As String = ""
    Public Property control As String
        Get
            Return _control
        End Get
        Set(ByVal value As String)
            _control = value
        End Set
    End Property

    Private Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        Try
            If (Not Me.IsPostBack AndAlso Me.Visible) Then
                LoadLAG()
                'LoadView()
                Dim currency As String = Me.GetCurrency()
                UCPrice1.SetPricing(clsPricing.GetPriceForDisplayIndex(1, currency))
                UCPrice2.SetPricing(clsPricing.GetPriceForDisplayIndex(2, currency))
                UCPrice3.SetPricing(clsPricing.GetPriceForDisplayIndex(3, currency))

            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub



    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
        'LoadView()
    End Sub


    Public Sub LoadLAG() 'ByVal LAGID As String, ByVal NoCache As Boolean
        Try

            PanelSelectProduct.Text = Me.CurrentPageData.GetCustomString("PanelSelectProduct")
            'lbFlashText.Text = Me.CurrentPageData.GetCustomString("lbFlashText")
            lbfootText.Text = Me.CurrentPageData.GetCustomString("lbfootText")
            'lbConfusedText.Text = Me.CurrentPageData.GetCustomString("lbconfusedText")
            'lbConfusedMore.Text = Me.CurrentPageData.GetCustomString("lbconfusedMore")
            'lblTopTitle.Text = Me.CurrentPageData.GetCustomString("lblTopTitle")
            lblPaymentsGuarantee.Text = Me.CurrentPageData.GetCustomString("lblPaymentsGuarantee")
            'lblAdvices.Text = Me.CurrentPageData.GetCustomString("lblAdvices")
            'lnkRedeemVoucher.Text = Me.CurrentPageData.GetCustomString("lnkRedeemVoucher")
            If (clsPricing.VATCountries.Contains(Session("GEO_COUNTRY_CODE"))) Then
                lblPaymentsGuarantee.Text = lblPaymentsGuarantee.Text.Replace("/1.png", "/1_2.png")
            End If
        Catch ex As Exception
            AppUtils.ErrorMsgBox(ex, "")
        End Try

    End Sub


    'Private Sub img30Days_Command(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs) Handles img1000Credits.Command, img3000Credits.Command, img6000Credits.Command, img10000Credits.Command, img30000Credits.Command, img50000Credits.Command ', img999Days.Command ', img50GB.Command, img150GB.Command, img300GB.Command, img600GB.Command, img5000GB.Command
    Private Sub img30Days_Command(ByVal sender As Object, ByVal e As PriceSelectedEventArgs) Handles UCPrice1.PriceSelected, UCPrice2.PriceSelected, UCPrice3.PriceSelected
        Dim amount As String = ""
        Dim days As String = ""
        Dim credits As String = ""

        credits = e.Credits
        days = e.Duration
        amount = e.Price

        Session("credits") = credits.Replace("Credits", "")
        Session("purDesc") = days
        Session("amount") = amount

        Dim product As String = credits
        Dim url As String = ResolveUrl("~/Members/selectPayment.aspx?choice=" & product)
        If (Me.SessionVariables.MemberData.Country = "TR") Then
            url = ResolveUrl("~/Members/selectPaymentTR.aspx?choice=" & product)
        End If

        If _control = "new" Then
            url = HttpUtility.UrlEncode(url)
            Dim registerUrl As String = ResolveUrl("~/register.aspx?ReturnUrl=" & url)
            Response.Redirect(registerUrl)

        ElseIf Me.SessionVariables.MemberData Is Nothing OrElse Me.Session("ProfileID") = 0 Then
            url = HttpUtility.UrlEncode(url)
            Dim loginUrl As String = ResolveUrl("~/login.aspx?ReturnUrl=" & url)
            Response.Redirect(loginUrl)
            'Response.Redirect("http://" & gSiteName & "/login.aspx")

        Else
            'Response.Redirect("selectPayment2.aspx?choice=" & product)
            Response.Redirect(url)

        End If

    End Sub

End Class
﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UcMatchingCriteriaMan.ascx.vb"
    Inherits="Dating.Server.Site.Web.UcMatchingCriteriaMan" %>

<script type="text/javascript">
    function TrackBar_Init(s, e) {
        if (s == window["trcHeight"]) {
        }
        if (s == window["trcDistance"]) {
            //            s.SetPosition(<%= trcDistance.Items.Count - 1%>)
        }
        if (s == window["trcAge"]) {
        }
        if (trackBarInfoList && trackBarInfoList != null &&
        trackBarInfoList.List != null && trackBarInfoList.List.length > 0) {
            SetTextForPositions(s);
        }
    }
    function TrackBar_PositionChanged(s, e) {
        SetTextForPositions(s);
    }
    function SetTextForPositions(s) {
        var txt = "", ti;
        try {
            //s.GetItemText(s.GetPosition())
            //s.GetPositionStart()
            if (s == window["trcHeight"]) {
                ti = trackBarInfoList.GetInfoByPos("trcHeight", "DefaultTextMin", s.GetPositionStart());
                if (ti != null) { txt = ti.DefaultTextMin; } else {
                    txt = s.GetItemText(s.GetPositionStart());
                    if (stringIsEmpty(txt)) txt = s.GetPositionStart();
                }
                $('#height-min', '#criteria_height').html(txt);

                ti = trackBarInfoList.GetInfoByPos("trcHeight", "DefaultTextMax", s.GetPositionEnd());
                if (ti != null) { txt = ti.DefaultTextMax; } else {
                    txt = s.GetItemText(s.GetPositionEnd());
                    if (stringIsEmpty(txt)) txt = s.GetPositionEnd();
                }
                $('#height-max', '#criteria_height').html(txt);
            }
            else if (s == window["trcDistance"]) {
                ti = trackBarInfoList.GetInfoByPos("trcDistance", "DefaultText", s.GetPosition());
                if (ti != null) { txt = ti.DefaultText; } else {
                    txt = s.GetItemText(s.GetPosition());
                    if (stringIsEmpty(txt)) txt = s.GetPosition();
                }
                $('.trackbar-pos-val', '#criteria_distance').html(txt);
            }
            else if (s == window["trcAge"]) {
                ti = trackBarInfoList.GetInfoByPos("trcAge", "DefaultTextMin", s.GetPositionStart());
                if (ti != null) { txt = ti.DefaultTextMin; } else {
                    txt = s.GetItemText(s.GetPositionStart());
                    if (stringIsEmpty(txt)) txt = s.GetPositionStart();
                }
                $('#age-min', '#criteria_age').html(txt);

                ti = trackBarInfoList.GetInfoByPos("trcAge", "DefaultTextMax", s.GetPositionEnd());
                if (ti != null) { txt = ti.DefaultTextMax; }
                else {
                    txt = s.GetItemText(s.GetPositionEnd());
                    if (stringIsEmpty(txt)) txt = s.GetPositionEnd();
                }
                $('#age-max', '#criteria_age').html(txt);
            }
        }
        catch (e) { }
    }
    function SaveDefaultText2() {
        try {
            trackBarInfoList.AddInfo("trcHeight", "DefaultTextMin", "&lt;152cm", 0);
            trackBarInfoList.AddInfo("trcHeight", "DefaultTextMax", "&gt;213cm", <%= trcHeight.Items.Count - 1%>);

            trackBarInfoList.AddInfo("trcDistance", "DefaultText", '<%= msg_DistanceVal.Text%>', <%= trcDistance.Items.Count - 1%>);

            trackBarInfoList.AddInfo("trcAge", "DefaultTextMin", '18', 0);
            trackBarInfoList.AddInfo("trcAge", "DefaultTextMax", '120', <%= trcAge.Items.Count - 1%>);
        }
        catch (e) { }
    }
    $(function(){
        SaveDefaultText2();
    
    });

    

    var textSeparator = ";";
    function OnListBoxSelectionChanged(listBox, args) {
        if (args.index == 0)
            args.isSelected ? listBox.SelectAll() : listBox.UnselectAll();
        UpdateSelectAllItemState(listBox);
        UpdateText(listBox);

       
    }
    function UpdateSelectAllItemState(clst) {
        IsAllSelected(clst) ? clst.SelectIndices([0]) : clst.UnselectIndices([0]);
    }
    function IsAllSelected(clst) {
        var selectedDataItemCount = clst.GetItemCount() - (clst.GetItem(0).selected ? 0 : 1);
        return clst.GetSelectedItems().length == selectedDataItemCount;
    }
    function UpdateText(clst) {
        var selectedItems = clst.GetSelectedItems();
        if (window['clstSpokenLang'] == clst) {
            ddeSpokenLangWrap.SetText(GetSelectedItemsText(selectedItems));
        }
        else if (window['clstTypeOfDating'] == clst) {
            ddeTypeOfDatingWrap.SetText(GetSelectedItemsText(selectedItems));
        }
        else if (window['clstJobs'] == clst) {
            ddeJobsWrap.SetText(GetSelectedItemsText(selectedItems));
        }
    }

    function SynchronizeListBoxValues(dropDown, args) {
        var clst = null;
        if (window['ddeSpokenLangWrap'] == dropDown) {
            clst = clstSpokenLang;
        }
        else if (window['ddeTypeOfDatingWrap'] == dropDown) {
            clst = clstTypeOfDating;
        }
        else if (window['ddeJobsWrap'] == dropDown) {
            clst = clstJobs;
        }
        clst.UnselectAll();
        var texts = dropDown.GetText().split(textSeparator);
        var values = GetValuesByTexts(clst, texts);
        clst.SelectValues(values);
        UpdateSelectAllItemState(clst);
        UpdateText(clst); // for remove non-existing texts
    }

    function GetSelectedItemsText(items) {
        var texts = [];
        for (var i = 0; i < items.length; i++)
            if (items[i].index != 0)
                texts.push(items[i].text);
        return texts.join(textSeparator);
    }
    function GetValuesByTexts(clst, texts) {
        var actualValues = [];
        var item;
        for (var i = 0; i < texts.length; i++) {
            item = clst.FindItemByText(texts[i]);
            if (item != null)
                actualValues.push(item.value);
        }
        return actualValues;
    }
    
    function CheckBoxList_SelectedIndexChanged(s, e) {
        try {
            var itm =s.GetItem(e.index);
            if(itm.value == "-1"){
                s.UnselectAll();
                s.SelectValues(["-1"]);
            }
            else
                s.UnselectValues(["-1"]);
        }
        catch (e) { }
    }
    function SelectdxCheckBoxes(scope){
        var s = window[scope];
        if (s!=null){
            s.SelectAll();
        }
    } 
    function CleardxCheckBoxes(scope){
        var s = window[scope];
        if (s!=null){
            s.UnselectAll();
        }
    }
        
    function SelectCheckBoxes(scope) {
        $('input[type="checkbox"]', $(scope)).prop("checked", true);
    }
    function ClearCheckBoxes(scope) {
        $('input[type="checkbox"]', $(scope)).prop("checked", false);
    }
    function TrackBar_PositionChanging(s, e) {
        //SaveDefaultText(s, e);
    }
       
  

</script>
  <script type="text/javascript">
      $(function(){
          alert('i am here');
          OnMoreInfoClick('/Members/InfoWin.aspx?info=Error_Sending_Receiver_DoNot_Have_Photo&popup=popupWhatIs', this, 'Error! Can\'t send',650,350,null);
          return false;});</script>
<div class="FrameArroundBox">

    <div id="MatchingCriteria">
        <div id="CriteriaTopView">
            <asp:Literal ID="lblCriteriaHeader" runat="server" Text=""></asp:Literal>
            <div class="clear"></div>
        </div>
        <div id="Criteria">
            <div class="top-trackbars">
                <div id="criteria_age" class="trcBox">
                    <div class="criteria trackbar tb-age">
                        <div class="trackbar-title">
                            <asp:Literal ID="msg_AgeText" runat="server" Text="Ηλικία" />
                        </div>
                        <dx:ASPxTrackBar runat="server" ID="trcAge" MinValue="18" MaxValue="120" Step="1"
                            LargeTickInterval="16" SmallTickFrequency="6" Width="170" AllowRangeSelection="true"
                            PositionStart="18" PositionEnd="120" ClientInstanceName="trcAge"
                            EnableTheming="False" ScaleLabelHighlightMode="HandlePosition"
                            ShowChangeButtons="False">
                            <ClientSideEvents Init="TrackBar_Init" PositionChanging="TrackBar_PositionChanging" PositionChanged="TrackBar_PositionChanged" />
                        </dx:ASPxTrackBar>
                        <div>
                            <div id="age-min" class="trackbar-pos-val lfloat">
                                <asp:Literal ID="msg_AgeMin" runat="server" Text="18" />
                            </div>
                            <div id="age-max" class="trackbar-pos-val rfloat">
                                <asp:Literal ID="msg_AgeMax" runat="server" Text="120" />
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
                <div id="criteria_height" class="trcBox">
                    <div class="criteria trackbar tb-height">
                        <div class="trackbar-title">
                            <asp:Literal ID="msg_PersonalHeight" runat="server" Text="Ύψος" />
                        </div>
                        <dx:ASPxTrackBar runat="server" ID="trcHeight" MinValue="0" MaxValue="26" Step="1"
                            LargeTickInterval="4" SmallTickFrequency="4"
                            Width="170" AllowRangeSelection="true"
                            PositionStart="0" PositionEnd="26"
                            ClientInstanceName="trcHeight"
                            EnableTheming="False" ScaleLabelHighlightMode="HandlePosition"
                            ShowChangeButtons="False">
                            <Items>
                                <dx:TrackBarItem Text="&lt;152cm" ToolTip="&lt;152cm" Value="29" />
                                <dx:TrackBarItem Text="152cm" ToolTip="152cm" Value="30" />
                                <dx:TrackBarItem Text="155cm" ToolTip="155cm" Value="31" />
                                <dx:TrackBarItem Text="157cm" ToolTip="157cm" Value="32" />
                                <dx:TrackBarItem Text="160cm" ToolTip="160cm" Value="33" />
                                <dx:TrackBarItem Text="163cm" ToolTip="163cm" Value="34" />
                                <dx:TrackBarItem Text="165cm" ToolTip="165cm" Value="35" />
                                <dx:TrackBarItem Text="168cm" ToolTip="168cm" Value="36" />
                                <dx:TrackBarItem Text="170cm" ToolTip="170cm" Value="37" />
                                <dx:TrackBarItem Text="173cm" ToolTip="173cm" Value="38" />
                                <dx:TrackBarItem Text="175cm" ToolTip="175cm" Value="39" />
                                <dx:TrackBarItem Text="178cm" ToolTip="178cm" Value="40" />
                                <dx:TrackBarItem Text="180cm" ToolTip="180cm" Value="41" />
                                <dx:TrackBarItem Text="183cm" ToolTip="183cm" Value="42" />
                                <dx:TrackBarItem Text="185cm" ToolTip="185cm" Value="43" />
                                <dx:TrackBarItem Text="188cm" ToolTip="188cm" Value="44" />
                                <dx:TrackBarItem Text="191cm" ToolTip="191cm" Value="45" />
                                <dx:TrackBarItem Text="193cm" ToolTip="193cm" Value="46" />
                                <dx:TrackBarItem Text="196cm" ToolTip="196cm" Value="47" />
                                <dx:TrackBarItem Text="198cm" ToolTip="198cm" Value="48" />
                                <dx:TrackBarItem Text="201cm" ToolTip="201cm" Value="49" />
                                <dx:TrackBarItem Text="203cm" ToolTip="203cm" Value="50" />
                                <dx:TrackBarItem Text="206cm" ToolTip="206cm" Value="51" />
                                <dx:TrackBarItem Text="208cm" ToolTip="208cm" Value="52" />
                                <dx:TrackBarItem Text="211cm" ToolTip="211cm" Value="53" />
                                <dx:TrackBarItem Text="213cm" ToolTip="213cm" Value="54" />
                                <dx:TrackBarItem Text="&gt; 213cm" ToolTip="&gt; 213cm" Value="55" />
                            </Items>
                            <ClientSideEvents Init="TrackBar_Init" PositionChanging="TrackBar_PositionChanging" PositionChanged="TrackBar_PositionChanged" />
                        </dx:ASPxTrackBar>
                        <div>
                            <div id="height-min" class="trackbar-pos-val lfloat">
                                <asp:Literal ID="msg_HeightMin" runat="server" Text="&lt;152cm" />
                            </div>
                            <div id="height-max" class="trackbar-pos-val rfloat">
                                <asp:Literal ID="msg_HeightMax" runat="server" Text="&gt;213cm" />
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
                <div id="criteria_distance" class="trcBox">
                    <div class="criteria trackbar tb-distance">
                        <div class="trackbar-title">
                            <asp:Literal ID="msg_DistanceFromMe_ND" runat="server" Text="Απόσταση απο εμένα" />
                        </div>
                        <dx:ASPxTrackBar runat="server" ID="trcDistance" Step="1"
                            MinValue="20"
                            MaxValue="10000"
                            Position="10000"
                            PositionStart="0" PositionEnd="30"
                            LargeTickInterval="4" LargeTickEndValue="1" Width="200"
                            ClientInstanceName="trcDistance" EnableTheming="False" ScaleLabelHighlightMode="HandlePosition"
                            ShowChangeButtons="False">
                            <Items>
                                <dx:TrackBarItem Text="20km" ToolTip="20km" Value="20" />
                                <dx:TrackBarItem Text="30km" ToolTip="30km" Value="30" />
                                <dx:TrackBarItem Text="40km" ToolTip="40km" Value="40" />
                                <dx:TrackBarItem Text="50km" ToolTip="50km" Value="50" />
                                <dx:TrackBarItem Text="60km" ToolTip="60km" Value="60" />
                                <dx:TrackBarItem Text="70km" ToolTip="70km" Value="70" />
                                <dx:TrackBarItem Text="80km" ToolTip="80km" Value="80" />
                                <dx:TrackBarItem Text="90km" ToolTip="90km" Value="90" />
                                <dx:TrackBarItem Text="100km" ToolTip="100km" Value="100" />
                                <dx:TrackBarItem Text="120km" ToolTip="120km" Value="120" />
                                <dx:TrackBarItem Text="150km" ToolTip="150km" Value="150" />
                                <dx:TrackBarItem Text="200km" ToolTip="200km" Value="200" />
                                <dx:TrackBarItem Text="300km" ToolTip="300km" Value="300" />
                                <dx:TrackBarItem Text="400km" ToolTip="400km" Value="400" />
                                <dx:TrackBarItem Text="500km" ToolTip="500km" Value="500" />
                                <dx:TrackBarItem Text="600km" ToolTip="600km" Value="600" />
                                <dx:TrackBarItem Text="700km" ToolTip="700km" Value="700" />
                                <dx:TrackBarItem Text="800km" ToolTip="800km" Value="800" />
                                <dx:TrackBarItem Text="900km" ToolTip="900km" Value="900" />
                                <dx:TrackBarItem Text="1000km" ToolTip="1000km" Value="1000" />
                                <dx:TrackBarItem Text="1200km" ToolTip="1200km" Value="1200" />
                                <dx:TrackBarItem Text="1400km" ToolTip="1400km" Value="1400" />
                                <dx:TrackBarItem Text="1600km" ToolTip="1600km" Value="1600" />
                                <dx:TrackBarItem Text="2000km" ToolTip="2000km" Value="2000" />
                                <dx:TrackBarItem Text="3000km" ToolTip="3000km" Value="3000" />
                                <dx:TrackBarItem Text="4000km" ToolTip="4000km" Value="4000" />
                                <dx:TrackBarItem Text="5000km" ToolTip="5000km" Value="5000" />
                                <dx:TrackBarItem Text="6000km" ToolTip="6000km" Value="6000" />
                                <dx:TrackBarItem Text="7000km" ToolTip="7000km" Value="7000" />
                                <dx:TrackBarItem Text="8000km" ToolTip="8000km" Value="8000" />
                                <dx:TrackBarItem Text="9000km" ToolTip="9000km" Value="9000" />
                                <dx:TrackBarItem Text="10000km" ToolTip="10000km" Value="10000" />
                            </Items>
                            <ClientSideEvents Init="TrackBar_Init" PositionChanging="TrackBar_PositionChanging" PositionChanged="TrackBar_PositionChanged" />
                        </dx:ASPxTrackBar>
                        <div>
                            <div class="trackbar-pos-val">
                                <asp:Literal ID="msg_DistanceVal" runat="server" Text="Any Distance" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="crt-lists">
                <div class="TopRowList">
                    <div class="TopRowListInner">
                        <!-- hair color -->
                        <div class="crt-list4 lfloat PersonalHairColor">
                            <h6>
                                <asp:Label ID="msg_PersonalHairClr" runat="server" Text="" CssClass="psLabel1" />
                            </h6>
                            <div class="crt-list-content">
                                <dx:ASPxCheckBoxList ID="cbHairColor" runat="server" ValueType="System.String" CssPostfix="ci" ClientInstanceName="cbHairColor">
                                    <CheckedImage Url="//cdn.goomena.com/Images2/Matching/check.png" Height="19px"
                                        Width="18px">
                                    </CheckedImage>
                                    <UncheckedImage Url="//cdn.goomena.com/Images2/Matching/uncheck.png" Height="19px"
                                        Width="18px">
                                    </UncheckedImage>
                                    <ClientSideEvents SelectedIndexChanged="CheckBoxList_SelectedIndexChanged" />
                                </dx:ASPxCheckBoxList>
                            </div>

                        </div>
                        <!-- breast size -->
                        <div class="crt-list4 lfloat breast">
                            <h6>
                                <asp:Label ID="msg_BreastSize" runat="server" Text="Μέγεθος Στήθους" CssClass="psLabel1"></asp:Label></h6>
                            <div class="crt-list-content">

                                <dx:ASPxCheckBoxList ID="clstBreast" runat="server" ValueType="System.String" CssPostfix="ci" ClientInstanceName="clstBreast">
                                    <CheckedImage Url="//cdn.goomena.com/Images2/Matching/check.png" Height="19px"
                                        Width="18px">
                                    </CheckedImage>
                                    <UncheckedImage Url="//cdn.goomena.com/Images2/Matching/uncheck.png" Height="19px"
                                        Width="18px">
                                    </UncheckedImage>
                                    <ClientSideEvents SelectedIndexChanged="CheckBoxList_SelectedIndexChanged" />
                                </dx:ASPxCheckBoxList>
                            </div>
                        </div>
                        <!-- eye color -->
                        <div class="crt-list4 lfloat eyecolor">
                            <h6>
                                <asp:Label ID="msg_PersonalEyeClr" runat="server" Text="" CssClass="psLabel1" /></h6>
                            <div class="crt-list-content">
                                <dx:ASPxCheckBoxList ID="cbEyeColor" runat="server" ValueType="System.String" CssPostfix="ci" ClientInstanceName="cbEyeColor">
                                    <CheckedImage Url="//cdn.goomena.com/Images2/Matching/check.png" Height="19px"
                                        Width="18px">
                                    </CheckedImage>
                                    <UncheckedImage Url="//cdn.goomena.com/Images2/Matching/uncheck.png" Height="19px"
                                        Width="18px">
                                    </UncheckedImage>
                                    <ClientSideEvents SelectedIndexChanged="CheckBoxList_SelectedIndexChanged" />
                                </dx:ASPxCheckBoxList>
                            </div>

                        </div>
                        <!-- body type -->
                        <div class="crt-list4 lfloat">
                            <h6>
                                <asp:Label ID="msg_PersonalBodyType" runat="server" Text="" CssClass="psLabel1" /></h6>
                            <div class="crt-list-content">
                                <dx:ASPxCheckBoxList ID="cbBodyType" runat="server" ValueType="System.String" CssPostfix="ci" ClientInstanceName="cbBodyType">
                                    <CheckedImage Url="//cdn.goomena.com/Images2/Matching/check.png" Height="19px"
                                        Width="18px">
                                    </CheckedImage>
                                    <UncheckedImage Url="//cdn.goomena.com/Images2/Matching/uncheck.png" Height="19px"
                                        Width="18px">
                                    </UncheckedImage>
                                    <ClientSideEvents SelectedIndexChanged="CheckBoxList_SelectedIndexChanged" />
                                </dx:ASPxCheckBoxList>
                            </div>
                        </div>

                        <div class="clear"></div>
                    </div>
                </div>
            </div>
            <div class="MiddleBoxButtons">
                <div class="MiddleBoxButtonsInline">

                    <div id="criteria_travel" class="lfloat checkbox-wrap">
                        <div class="criteria checkbox">
                            <dx:ASPxCheckBox ID="chkTravel" runat="server" Text="Willing to travel" EncodeHtml="False"
                                CssPostfix="ci">
                                <CheckedImage Url="//cdn.goomena.com/Images2/Matching/check.png" Height="19px"
                                    Width="18px">
                                </CheckedImage>
                                <UncheckedImage Url="//cdn.goomena.com/Images2/Matching/uncheck.png" Height="19px"
                                    Width="18px">
                                </UncheckedImage>
                            </dx:ASPxCheckBox>
                        </div>
                    </div>
                    <div id="criteria_SpokenLang" class="lfloat checkbox-wrap">
                        <div class="criteria checkbox">
                            <div class="LangContainer">
                                <asp:Label ID="msg_SpokenLang" runat="server" Text="" CssClass="criteria-title"></asp:Label>
                            </div>
                            <div class="LandDropDownContainer">
                                <dx:ASPxDropDownEdit ClientInstanceName="ddeSpokenLangWrap" ID="ddeSpokenLangWrap"
                                    Width="170px" runat="server" AnimationType="None" NullText="Any Language" ClientSideEvents-ButtonClick="function(s, e) {s.ShowDropDown();}" ClientSideEvents-Init="function(s, e) {s.GetInputElement().disabled = true;}">
                                    <DropDownWindowTemplate>
                                        <dx:ASPxListBox Width="100%" ID="clstSpokenLang" ClientInstanceName="clstSpokenLang"
                                            SelectionMode="CheckColumn" runat="server" Height="300px">
                                            <CheckBoxCheckedImage Url="//cdn.goomena.com/images2/Matching/check.png" Height="19px"
                                                Width="18px">
                                            </CheckBoxCheckedImage>
                                            <CheckBoxUncheckedImage Url="//cdn.goomena.com/search/Matching/uncheck.png?v2" Height="19px"
                                                Width="18px">
                                            </CheckBoxUncheckedImage>
                                            <ClientSideEvents SelectedIndexChanged="OnListBoxSelectionChanged" />
                                        </dx:ASPxListBox>
                                    </DropDownWindowTemplate>
                                    <ClientSideEvents TextChanged="SynchronizeListBoxValues" DropDown="SynchronizeListBoxValues" />
                                </dx:ASPxDropDownEdit>
                            </div>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>

            <div class="crt-lists">
                <div class="SecondRowList">
                    <div class="SecondRowListInner">
                        <!-- RelationShipStatus -->
                        <div class="crt-list2 lfloat RelationshipStatus">
                            <h6>
                                <asp:Label ID="msg_RelationshipStatus" runat="server" Text="" CssClass="psLabel1" />
                            </h6>
                            <div class="crt-list-content">
                                <dx:ASPxCheckBoxList ID="cbRelationshipStatus" runat="server" ValueType="System.String" CssPostfix="ci" ClientInstanceName="cbRelationshipStatus">
                                    <CheckedImage Url="//cdn.goomena.com/Images2/Matching/check.png" Height="19px"
                                        Width="18px">
                                    </CheckedImage>
                                    <UncheckedImage Url="//cdn.goomena.com/Images2/Matching/uncheck.png" Height="19px"
                                        Width="18px">
                                    </UncheckedImage>
                                    <ClientSideEvents SelectedIndexChanged="CheckBoxList_SelectedIndexChanged" />
                                </dx:ASPxCheckBoxList>
                            </div>

                        </div>
                        <!-- Type Of dating -->
                        <div class="crt-list2 lfloat TypeOfdating">
                            <h6>
                                <asp:Label ID="msg_TypeOfdating" runat="server" Text="" CssClass="psLabel1" />
                            </h6>
                            <div class="crt-list-content">
                                <dx:ASPxCheckBoxList ID="cbDatingType" runat="server" ValueType="System.String" CssPostfix="ci" ClientInstanceName="cbDatingType">
                                    <CheckedImage Url="//cdn.goomena.com/Images2/Matching/check.png" Height="19px"
                                        Width="18px">
                                    </CheckedImage>
                                    <UncheckedImage Url="//cdn.goomena.com/Images2/Matching/uncheck.png" Height="19px"
                                        Width="18px">
                                    </UncheckedImage>
                                    <ClientSideEvents SelectedIndexChanged="CheckBoxList_SelectedIndexChanged" />
                                </dx:ASPxCheckBoxList>
                            </div>

                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
            <div class="crt-lists">
                <div class="ThirdRowList">
                    <div class="ThirdRowListInner">
                        <!-- Education -->
                        <div class="crt-list2 lfloat Education">
                            <h6>
                                <asp:Label ID="msg_OtherEducat" runat="server" Text="" CssClass="psLabel1" />
                            </h6>
                            <div class="crt-list-content">
                                <dx:ASPxCheckBoxList ID="cbEducation" runat="server" ValueType="System.String" CssPostfix="ci" ClientInstanceName="cbEducation">
                                    <CheckedImage Url="//cdn.goomena.com/Images2/Matching/check.png" Height="19px"
                                        Width="18px">
                                    </CheckedImage>
                                    <UncheckedImage Url="//cdn.goomena.com/Images2/Matching/uncheck.png" Height="19px"
                                        Width="18px">
                                    </UncheckedImage>
                                    <ClientSideEvents SelectedIndexChanged="CheckBoxList_SelectedIndexChanged" />
                                </dx:ASPxCheckBoxList>
                            </div>

                        </div>
                        <!-- Children -->
                        <div class="crt-list2 lfloat Children">
                            <h6>
                                <asp:Label ID="msg_PersonalChildren" runat="server" Text="" CssClass="psLabel1" />
                            </h6>
                            <div class="crt-list-content">
                                <dx:ASPxCheckBoxList ID="cbChildren" runat="server" ValueType="System.String" CssPostfix="ci" ClientInstanceName="cbChildren">
                                    <CheckedImage Url="//cdn.goomena.com/Images2/Matching/check.png" Height="19px"
                                        Width="18px">
                                    </CheckedImage>
                                    <UncheckedImage Url="//cdn.goomena.com/Images2/Matching/uncheck.png" Height="19px"
                                        Width="18px">
                                    </UncheckedImage>
                                    <ClientSideEvents SelectedIndexChanged="CheckBoxList_SelectedIndexChanged" />
                                </dx:ASPxCheckBoxList>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
            <div class="crt-lists">
                <div class="ForthRowList">
                    <div class="ForthRowListInner">
                        <!-- Ethnicity -->
                        <div class="crt-list2 lfloat Ethnicity">
                            <h6>
                                <asp:Label ID="msg_PersonalEthnicity" runat="server" Text="" CssClass="psLabel1" />
                            </h6>
                            <div class="crt-list-content">
                                <dx:ASPxCheckBoxList ID="cbEthnicity" runat="server" ValueType="System.String" CssPostfix="ci" ClientInstanceName="cbEthnicity">
                                    <CheckedImage Url="//cdn.goomena.com/Images2/Matching/check.png" Height="19px"
                                        Width="18px">
                                    </CheckedImage>
                                    <UncheckedImage Url="//cdn.goomena.com/Images2/Matching/uncheck.png" Height="19px"
                                        Width="18px">
                                    </UncheckedImage>
                                    <ClientSideEvents SelectedIndexChanged="CheckBoxList_SelectedIndexChanged" />
                                </dx:ASPxCheckBoxList>
                            </div>
                        </div>
                        <!-- Religion -->
                        <div class="crt-list2 lfloat Religion">
                            <h6>
                                <asp:Label ID="msg_PersonalReligion" runat="server" Text="" CssClass="psLabel1" />
                            </h6>
                            <div class="crt-list-content">
                                <dx:ASPxCheckBoxList ID="cbReligion" runat="server" ValueType="System.String" CssPostfix="ci" ClientInstanceName="cbReligion">
                                    <CheckedImage Url="//cdn.goomena.com/Images2/Matching/check.png" Height="19px"
                                        Width="18px">
                                    </CheckedImage>
                                    <UncheckedImage Url="//cdn.goomena.com/Images2/Matching/uncheck.png" Height="19px"
                                        Width="18px">
                                    </UncheckedImage>
                                    <ClientSideEvents SelectedIndexChanged="CheckBoxList_SelectedIndexChanged" />
                                </dx:ASPxCheckBoxList>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="crt-lists">
                <div class="FifthRowList">
                    <div class="FifthRowListInner">
                        <!-- SmokingHabit -->
                        <div class="crt-list2 lfloat SmokingHabit">
                            <h6>
                                <asp:Label ID="msg_PersonalSmokingHabit" runat="server" Text="" CssClass="psLabel1" />
                            </h6>
                            <div class="crt-list-content">
                                <dx:ASPxCheckBoxList ID="cbSmoking" runat="server" ValueType="System.String" CssPostfix="ci" ClientInstanceName="cbSmoking">
                                    <CheckedImage Url="//cdn.goomena.com/Images2/Matching/check.png" Height="19px"
                                        Width="18px">
                                    </CheckedImage>
                                    <UncheckedImage Url="//cdn.goomena.com/Images2/Matching/uncheck.png" Height="19px"
                                        Width="18px">
                                    </UncheckedImage>
                                    <ClientSideEvents SelectedIndexChanged="CheckBoxList_SelectedIndexChanged" />
                                </dx:ASPxCheckBoxList>
                            </div>
                        </div>
                        <!-- DrinkingHabit -->
                        <div class="crt-list2 lfloat Drinking">
                            <h6>
                                <asp:Label ID="msg_PersonalDrinkingHabit" runat="server" Text="" CssClass="psLabel1" />
                            </h6>
                            <div class="crt-list-content">
                                <dx:ASPxCheckBoxList ID="cbDrinking" runat="server" ValueType="System.String" CssPostfix="ci" ClientInstanceName="cbDrinking">
                                    <CheckedImage Url="//cdn.goomena.com/Images2/Matching/check.png" Height="19px"
                                        Width="18px">
                                    </CheckedImage>
                                    <UncheckedImage Url="//cdn.goomena.com/Images2/Matching/uncheck.png" Height="19px"
                                        Width="18px">
                                    </UncheckedImage>
                                    <ClientSideEvents SelectedIndexChanged="CheckBoxList_SelectedIndexChanged" />
                                </dx:ASPxCheckBoxList>
                            </div>
                        </div>

                    </div>
                </div>
            </div>


            <%-- <div class="crt-lists">
        <div class="crt-list2 lfloat" id="divIncome" runat="server">
            <h6>
                <asp:Label ID="msg_OtherAnnual" runat="server" Text="" CssClass="psLabel1" /></h6>
            <div class="controls">
                <asp:CheckBoxList ID="cbIncome" runat="server"></asp:CheckBoxList>
            </div>
            <table class="left-table">
                <tr>
                    <td>
                        <asp:HyperLink ID="lnkIncomeClr" runat="server" NavigateUrl="javascript:void(0);">Clear</asp:HyperLink></td>
                    <td>&nbsp;&nbsp;&nbsp;</td>
                    <td>
                        <asp:HyperLink ID="lnkIncomeAll" runat="server" NavigateUrl="javascript:void(0);">Select All</asp:HyperLink></td>
                </tr>
            </table>
            <script type="text/javascript">
                $("#<%= lnkIncomeClr.ClientID%>").click(function () { ClearCheckBoxes("#<%= cbIncome.ClientID%>") });
                $("#<%= lnkIncomeAll.ClientID%>").click(function () { SelectCheckBoxes("#<%= cbIncome.ClientID%>") });
            </script>
        </div>
        <div class="clear"></div>
    </div>--%>


            <div class="form-actions">
                <asp:Button ID="btnSaveAndFind" runat="server" CssClass="btn btn-large btn-primary" Text="Save Search and Show Results" />

            </div>

        </div>
    </div>
</div>

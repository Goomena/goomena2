﻿Imports DevExpress.Web.ASPxUploadControl
Imports System.IO
Imports System.Drawing
Imports Dating.Server.Core.DLL
Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Datasets.DLL.DSMembers
Imports DevExpress.Web.ASPxEditors
Imports DevExpress.Web.ASPxPopupControl

Public Class MembersBarRight
    Inherits BaseUserControl

    Protected Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            'If (_pageData Is Nothing) Then _pageData = New clsPageData("master", Context)
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData("master", Context, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property
    Private Sub Page_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        Try
            If Me._pageData IsNot Nothing Then
                RemoveHandler Me._pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If

        Catch ex As Exception
        End Try

        Try
            MyBase.Dispose()
        Catch ex As Exception
        End Try
    End Sub
    Public ReadOnly Property LoginStatusControl As System.Web.UI.WebControls.LoginStatus
        Get
            Return LoginStatus1
        End Get
    End Property


    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try

            LoadLAG()

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Public Sub LoadLAG()
        Try

            btnSettings.Text = CurrentPageData.VerifyCustomString("btnSettings")

            Dim lnkAccSettings As HyperLink = pnlSettingsPopup.FindControl("lnkAccSettings")
            If (lnkAccSettings IsNot Nothing) Then
                lnkAccSettings.Text = CurrentPageData.VerifyCustomString("lnkAccSettings")
            End If

            Dim lnkAccDelete As HyperLink = pnlSettingsPopup.FindControl("lnkAccDelete")
            If (lnkAccDelete IsNot Nothing) Then
                lnkAccDelete.Text = CurrentPageData.VerifyCustomString("lnkAccDelete")
            End If


            Dim LoginStatus1 As LoginStatus = pnlSettingsPopup.FindControl("LoginStatus1")
            If (LoginStatus1 IsNot Nothing) Then
                LoginStatus1.LogoutText = CurrentPageData.VerifyCustomString("lnkLogOut")
                LoginStatus1.LoginText = LoginStatus1.LogoutText
            End If

            Dim btnHelp As HyperLink = pnlSettingsPopup.FindControl("btnHelp")
            If (btnHelp IsNot Nothing) Then
                btnHelp.Text = CurrentPageData.VerifyCustomString("btnHelp")
                btnHelp.NavigateUrl = CurrentPageData.VerifyCustomString("btnHelp.NavigateUrl")
            End If

            If (Not Page.IsPostBack) Then
                Dim href As String = ResolveUrl(lnkPubDefault.NavigateUrl)
                lnkPubDefault.NavigateUrl = LanguageHelper.GetPublicURL(href, GetLag())

                href = ResolveUrl(btnHelp.NavigateUrl)
                btnHelp.NavigateUrl = LanguageHelper.GetPublicURL(href, GetLag())
            End If


        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


End Class
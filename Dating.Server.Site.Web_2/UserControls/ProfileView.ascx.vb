﻿Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Datasets.DLL.DSMembers
Imports Dating.Server.Core.DLL
Imports System.Linq

Public Class ProfileView
    Inherits BaseUserControl

#Region "Props"

    Protected Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            'If (_pageData Is Nothing) Then _pageData = New clsPageData("~/Members/control.ProfileView", Context)
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData("~/Members/control.ProfileView", Context, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property

    Private Sub Page_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        Try
            If Me._pageData IsNot Nothing Then
                RemoveHandler Me._pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If

        Catch ex As Exception
        End Try

        Try
            MyBase.Dispose()
        Catch ex As Exception
        End Try
    End Sub
    Protected Property UserIdInView As Integer
        Get
            Return ViewState("UserIdInView")
        End Get
        Set(value As Integer)
            ViewState("UserIdInView") = value
        End Set
    End Property

    Private _VIPTooltip As String
    Protected Property VIPTooltip As String
        Get
            Return _VIPTooltip
        End Get
        Set(value As String)
            _VIPTooltip = value
        End Set
    End Property

    Protected Property UserLoginNameInView As String
        Get
            Return ViewState("UserLoginNameInView")
        End Get
        Set(value As String)
            ViewState("UserLoginNameInView") = value
        End Set
    End Property

    Public Property ifrUserMap_src As String


    Protected Property ControlLoaded As Boolean
        Get
            Return ViewState("ControlLoaded")
        End Get
        Set(value As Boolean)
            ViewState("ControlLoaded") = value
        End Set
    End Property

    Protected Property UserInView_IsFemale As Boolean
        Get
            Return ViewState("UserInView_IsFemale")
        End Get
        Set(value As Boolean)
            ViewState("UserInView_IsFemale") = value
        End Set
    End Property


    Protected Property UserInView_IsMale As Boolean
        Get
            Return ViewState("UserInView_IsMale")
        End Get
        Set(value As Boolean)
            ViewState("UserInView_IsMale") = value
        End Set
    End Property

#End Region


    Protected Overrides Sub OnInit(e As System.EventArgs)
        MyBase.OnInit(e)
        'ModGlobals.AddScriptResourceToHeader(Me.Page, "~/Scripts/bootstrap-dropdown.js")
    End Sub


    Private Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        '''''''''''''''''''''''''''''
        ' !!!   visibility is set by profile.aspx, if set to false do not load control
        '''''''''''''''''''''''''''''
        If (Not Me.Visible) Then Return

        Try

            If (Not Me.IsPostBack OrElse Not Me.ControlLoaded) Then

                Try
                    Me.UserIdInView = 0
                    If (Request.QueryString("p") <> "") Then
                        Using cmdb As CMSDBDataContext = Me.GetCMSDBDataContext


                            Dim prof As EUS_Profile = DataHelpers.GetEUS_ProfileMasterByLoginName(cmdb, Request.QueryString("p"), ProfileStatusEnum.Approved, ProfileStatusEnum.LIMITED)
                            If (prof IsNot Nothing) Then
                                Me.UserIdInView = prof.ProfileID
                                Me.UserLoginNameInView = prof.LoginName

                                If (ProfileHelper.IsFemale(prof.GenderId)) Then
                                    imgPhoto.ImageUrl = ProfileHelper.Female_DefaultImage
                                    Me.UserInView_IsFemale = True
                                Else
                                    imgPhoto.ImageUrl = ProfileHelper.Male_DefaultImage
                                    Me.UserInView_IsMale = True
                                End If
                            End If
                        End Using
                    End If
                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "")
                End Try

                Try
                    If (Me.UserIdInView = 0) Then
                        If (ProfileHelper.IsFemale(Me.SessionVariables.MemberData.GenderId)) Then
                            imgPhoto.ImageUrl = ProfileHelper.Female_DefaultImage
                        Else
                            imgPhoto.ImageUrl = ProfileHelper.Male_DefaultImage
                        End If
                    End If
                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "")
                End Try

                LoadLAG()
                LoadView()
                Me.ControlLoaded = True
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub
    Public ReadOnly Property OtherId As String
        Get
            Return Me.GetProfileIdString(UserIdInView)
        End Get
    End Property
    Public Property lnkWink As String = ""
    Public Property lnkFavorite As String = ""
    Public Property lnkUnFavorite As String = ""
    Public Function GetProfileIdString(ByVal profileId As Integer) As String
        Dim re As String = ""
        Try
            re = HttpContext.Current.Server.HtmlEncode(clsEncryptDecryptDes.encrypt(profileId))
        Catch ex As Exception

        End Try
        Return re
    End Function
    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        If (Not Me.Visible) Then Return

        Try
            Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try
    End Sub


    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
        LoadView()
    End Sub


    Protected Sub LoadLAG()
        Try


            SetControlsValue(Me, CurrentPageData)

            lnkEditProf.Text = CurrentPageData.GetCustomString("lnkEditProf")


            lnkConversationM.Text = CurrentPageData.GetCustomString("ConversationText")
            lnkConversationF.Text = lnkConversationM.Text

            lnkMessageF.Text = CurrentPageData.GetCustomString("lnkMessage")
            '   lnkFavoriteF.Text = CurrentPageData.GetCustomString("lnkFavorite")
            '   lnkUnFavoriteF.Text = CurrentPageData.GetCustomString("lnkUnFavorite")
            lnkMakeOfferF.Text = CurrentPageData.GetCustomString("lnkMakeOffer")
            lnkUnlockOfferF.Text = CurrentPageData.GetCustomString("OfferAcceptedUnlockText")
            '    lnkWinkF.Text = CurrentPageData.GetCustomString("lnkWink")
            '     lnkUnWinkF.Text = CurrentPageData.GetCustomString("lnkWink")
            lnkWink = CurrentPageData.GetCustomString("lnkWink")
            lnkFavorite = CurrentPageData.GetCustomString("lnkFavorite")
            lnkUnFavorite = CurrentPageData.GetCustomString("lnkUnFavorite")
            lnkMessageM.Text = CurrentPageData.GetCustomString("lnkMessage")
            '     lnkFavoriteM.Text = CurrentPageData.GetCustomString("lnkFavorite")
            '   lnkUnFavoriteM.Text = CurrentPageData.GetCustomString("lnkUnFavorite")
            lnkMakeOfferM.Text = CurrentPageData.GetCustomString("lnkMakeOffer")
            lnkUnlockOfferM.Text = CurrentPageData.GetCustomString("OfferAcceptedUnlockText")
            '  lnkWinkM.Text = CurrentPageData.GetCustomString("lnkWink")
            '   lnkUnWinkM.Text = CurrentPageData.GetCustomString("lnkWink")
            msg_ActionsText.Text = CurrentPageData.GetCustomString("lnkMessage")


            lnkReport.Text = CurrentPageData.GetCustomString("lnkReport")
            lnkBlock.Text = CurrentPageData.GetCustomString("lnkBlock")
    

            setLevelPopUp.HeaderText = CurrentPageData.GetCustomString("popupWindows.PhotoLevelTitle")

            msg_LastUpdatedDT.Text = CurrentPageData.GetCustomString(msg_LastUpdatedDT.ID)
            lnkMyPhoto.Text = CurrentPageData.GetCustomString("lnkMyPhoto")
            lnkBilling.Text = CurrentPageData.GetCustomString("StandardMember")

            lblItemsName.Text = Me.CurrentPageData.GetCustomString("CartProfileView")
            lnkViewDescription.Text = Me.CurrentPageData.GetCustomString("CartProfileWhatIsIt")
            lnkViewDescription.OnClientClick = OnMoreInfoClickFunc(
                lnkViewDescription.OnClientClick,
                ResolveUrl("~/Members/InfoWin.aspx?info=profile"),
                Me.CurrentPageData.GetCustomString("CartProfileView"))



            lnkSendMessageTravel.Text = Me.CurrentPageData.GetCustomString("lnkSendMessageTravel")

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Private Sub LoadView()

        Try

            If (Me.UserIdInView > 1 AndAlso Me.UserIdInView <> Me.MasterProfileId) Then

                If (Me.IsFemale) Then
                    mvGender.SetActiveView(vwFemale)
                Else
                    mvGender.SetActiveView(vwMale)
                End If

                Me.LoadProfile(Me.UserIdInView)

                If (Me.IsMale) Then
                    Dim currentUserHasPhotos As Boolean = clsUserDoes.HasPhotos(Me.MasterProfileId, Me.MirrorProfileId)
                    If (Not currentUserHasPhotos) Then
                        UploadPhotoPopUp.ShowOnPageLoad = True
                        Dim _pageData2 As clsPageData = New clsPageData("~/Members/control.PhotosEdit", Context)
                        msg_HasNoPhotosText.Text = _pageData2.GetCustomString("msg_HasNoPhotosText")
                        lnkUploadPhoto.Text = CurrentPageData.GetCustomString("lnkUploadPhoto")
                    End If
                End If

                Dim hasCommunication As Boolean = clsUserDoes.HasCommunication(Me.UserIdInView, Me.MasterProfileId)
                Dim hasAnyOffer As Boolean = clsUserDoes.HasAnyOffer(Me.UserIdInView, Me.MasterProfileId)
                'Dim acceptedOffer As EUS_Offer =
                clsUserDoes.GetAcceptedOrUnlockedOffer(Me.UserIdInView, Me.MasterProfileId)
                Dim isMyWinkExists As Boolean = (clsUserDoes.GetMyPendingWink(Me.UserIdInView, Me.MasterProfileId) IsNot Nothing)
                Dim IsAnyMessageExchanged As Boolean = clsUserDoes.IsAnyMessageExchanged(Me.UserIdInView, Me.MasterProfileId)


                divWinkF.Visible = True
                divUnWinkF.Visible = True
                divMessageF.Visible = True
                divMakeOfferF.Visible = True
                divFavoriteF.Visible = True
                divUnlockOfferF.Visible = True
                divUnFavoriteF.Visible = True
                divConversationF.Visible = True

                divWinkM.Visible = True
                divUnWinkM.Visible = True
                divMessageM.Visible = True
                divMakeOfferM.Visible = True
                divFavoriteM.Visible = True
                divUnlockOfferM.Visible = True
                divUnFavoriteM.Visible = True
                divConversationM.Visible = True

                If (Me.IsFemale) Then
                    Dim IsFavorited As Boolean = clsUserDoes.IsFavorited(Me.UserIdInView, Me.MasterProfileId)
                    divFavoriteF.Visible = (Not IsFavorited)
                    '   lnkFavoriteF.CommandArgument = Me.UserIdInView

                    divUnFavoriteF.Visible = IsFavorited
                    '   lnkUnFavoriteF.CommandArgument = Me.UserIdInView

                    lnkMakeOfferF.Visible = (Not HasOffer(Me.UserIdInView, Me.MasterProfileId) AndAlso Not clsUserDoes.HasCommunication(Me.UserIdInView, Me.MasterProfileId))
                    lnkMakeOfferF.CommandArgument = Me.UserIdInView

                    lnkReport.NavigateUrl = "~/Members/Report.aspx?p=" & HttpUtility.UrlEncode(Me.UserLoginNameInView)

                    lnkBlock.Visible = (Not clsUserDoes.IsBlocked(Me.UserIdInView, Me.MasterProfileId)) '
                    lnkBlock.CommandArgument = Me.UserIdInView
                    If (Not lnkBlock.Visible) Then
                        ltrSeparator.Visible = False
                    Else
                        ltrSeparator.Visible = True
                    End If


                    lnkMessageF.Visible = True
                    lnkMessageF.NavigateUrl = "~/Members/Conversation.aspx?p=" & HttpUtility.UrlEncode(Me.UserLoginNameInView)

                    If (IsAnyMessageExchanged) Then
                        lnkConversationF.Visible = True
                        lnkConversationF.NavigateUrl = "~/Members/Conversation.aspx?vw=" & HttpUtility.UrlEncode(Me.UserLoginNameInView)
                    End If
                   

                    lnkUnlockOfferF.Visible = False
                    '     divTooltipholderF.Visible = False
                   


                    ' no offer, no communication -> allow wink
                    divWinkF.Visible = (hasAnyOffer = False AndAlso hasCommunication = False AndAlso IsAnyMessageExchanged = False)
                    ' lnkWinkF.CommandArgument = Me.UserIdInView

                    divUnWinkF.Visible = (Not divWinkF.Visible AndAlso isMyWinkExists)
                    '    lnkUnWinkF.CommandArgument = Me.UserIdInView


                    If (Me.SessionVariables.MemberData.Status = ProfileStatusEnum.LIMITED) Then
                        divWinkF.Disabled = True ' = False
                        divUnWinkF.Disabled = True ' = False
                        lnkMessageF.Enabled = False
                        lnkMakeOfferF.Enabled = False
                        divFavoriteF.Disabled = True ' = False
                        lnkUnlockOfferF.Enabled = False
                        divUnFavoriteF.Disabled = True ' = False
                    End If

                ElseIf (Me.IsMale) Then

                    divFavoriteM.Visible = (Not clsUserDoes.IsFavorited(Me.UserIdInView, Me.MasterProfileId))
                    ' lnkFavoriteM.CommandArgument = Me.UserIdInView

                    divUnFavoriteM.Visible = Not divFavoriteM.Visible
                    '(clsUserDoes.IsFavorited(Me.UserIdInView, Me.MasterProfileId))
                    '  lnkUnFavoriteM.CommandArgument = Me.UserIdInView

                    lnkMakeOfferM.Visible = (Not HasOffer(Me.UserIdInView, Me.MasterProfileId) AndAlso Not clsUserDoes.HasCommunication(Me.UserIdInView, Me.MasterProfileId))
                    lnkMakeOfferM.CommandArgument = Me.UserIdInView

                    lnkReport.NavigateUrl = "~/Members/Report.aspx?p=" & HttpUtility.UrlEncode(Me.UserLoginNameInView)

                    lnkBlock.Visible = (Not clsUserDoes.IsBlocked(Me.UserIdInView, Me.MasterProfileId)) '
                    lnkBlock.CommandArgument = Me.UserIdInView
                    If (Not lnkBlock.Visible) Then
                        ltrSeparator.Visible = False
                    Else
                        ltrSeparator.Visible = True
                    End If

                    If (IsAnyMessageExchanged) Then
                        lnkConversationM.Visible = True
                        lnkConversationM.NavigateUrl = "~/Members/Conversation.aspx?vw=" & Me.UserLoginNameInView
                    End If


                    lnkUnlockOfferM.Visible = False
                   




                    ' no offer, no communication -> allow wink
                    divWinkM.Visible = (hasAnyOffer = False AndAlso hasCommunication = False AndAlso IsAnyMessageExchanged = False)
                    '     lnkWinkM.CommandArgument = Me.UserIdInView

                    divUnWinkM.Visible = (Not divWinkM.Visible AndAlso isMyWinkExists)
                    '  lnkUnWinkM.CommandArgument = Me.UserIdInView


                    'If (Not hasCommunication) Then
                    '    'divActionsMenu.Visible = True

                    '    'lnkSendMsgOnce.Text = BasePage.ReplaceCommonTookens(CurrentPageData.GetCustomString("SendMessageOnceText"), Me.UserLoginNameInView)
                    '    'lnkSendMsgMany.Text = BasePage.ReplaceCommonTookens(CurrentPageData.GetCustomString("SendMessageManyText"), Me.UserLoginNameInView)
                    '    'lnkSendMsgOnce.NavigateUrl = ResolveUrl("~/Members/Conversation.aspx?send=once&p=" & HttpUtility.UrlEncode(Me.UserLoginNameInView))
                    '    'lnkSendMsgMany.NavigateUrl = ResolveUrl("~/Members/Conversation.aspx?send=unl&p=" & HttpUtility.UrlEncode(Me.UserLoginNameInView))
                    'Else
                    lnkMessageM.Visible = True
                    lnkMessageM.NavigateUrl = "~/Members/Conversation.aspx?p=" & HttpUtility.UrlEncode(Me.UserLoginNameInView)
                    'End If


                    If (Me.SessionVariables.MemberData.Status = ProfileStatusEnum.LIMITED) Then
                        divWinkM.Disabled = True  ' = False
                        divUnWinkM.Disabled = True '= False
                        lnkMessageM.Enabled = False
                        lnkSendMsgOnce.Enabled = False
                        lnkSendMsgMany.Enabled = False
                        lnkMakeOfferM.Enabled = False
                        divFavoriteM.Disabled = True ' = False
                        lnkUnlockOfferM.Enabled = False
                        divUnFavoriteM.Disabled = True '= False
                        divActionsMenu.Disabled = True
                    End If

                End If

                lnkPhoto.NavigateUrl = "javascript:void(1)"
                lnkPhoto.Attributes("onClick") = "return false;"

                ShowPhotosLevel()
                pPhotosLevel.Visible = True

                pReport.Visible = True

                '     divWinkF.Visible = lnkWinkF.Visible
                ' divUnWinkF.Visible = lnkUnWinkF.Visible
                divMessageF.Visible = lnkMessageF.Visible
                divMakeOfferF.Visible = lnkMakeOfferF.Visible
                '  divFavoriteF.Visible = lnkFavoriteF.Visible
                divUnlockOfferF.Visible = lnkUnlockOfferF.Visible
                '  divUnFavoriteF.Visible = lnkUnFavoriteF.Visible
                divConversationF.Visible = lnkConversationF.Visible

                ' divWinkM.Visible = lnkWinkM.Visible
                '  divUnWinkM.Visible = lnkUnWinkM.Visible
                divMessageM.Visible = lnkMessageM.Visible
                divMakeOfferM.Visible = lnkMakeOfferM.Visible
                ' divFavoriteM.Visible = lnkFavoriteM.Visible
                divUnlockOfferM.Visible = lnkUnlockOfferM.Visible
                '  divUnFavoriteM.Visible = lnkUnFavoriteM.Visible
                divConversationM.Visible = lnkConversationM.Visible


            Else
                Me.LoadProfile(Me.MasterProfileId)
                mvGender.ActiveViewIndex = 0
                'lnkEditProfF.Visible = True
                'lnkMessageF.Visible = False
                'lnkFavoriteF.Visible = False
                'lnkUnFavoriteF.Visible = False
                'lnkMakeOfferF.Visible = False
                'lnkUnWinkF.Visible = False
                'lnkWinkF.Visible = False
                'lnkUnlockOfferF.Visible = False
                pReport.Visible = False
                pPhotosLevel.Visible = False

                lnkEditProf.Visible = True
                lnkEditProf1.Visible = True
                lnkEditProf2.Visible = True
                lnkEditProf3.Visible = True
                lnkEditProf4.Visible = True
                lnkEditProf5.Visible = True
                lnkEditProf6.Visible = True
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


    Private Sub ShowPhotosLevel()
        Try

            Try

                Web.ClsCombos.FillComboUsingDatatable(Lists.gEUS_LISTS_PhotosDisplayLevel, Session("LagID"), "PhotosDisplayLevelId", cbPhotosLevel, True, False, "US")
                cbPhotosLevel.Items(0).Selected = True

            Catch ex As System.Threading.ThreadAbortException
            Catch ex As System.ArgumentOutOfRangeException
                Lists.gEUS_LISTS_PhotosDisplayLevel.Clear()
                Web.ClsCombos.FillComboUsingDatatable(Lists.gEUS_LISTS_PhotosDisplayLevel, Session("LagID"), "PhotosDisplayLevelId", cbPhotosLevel, True, False, "US")
                cbPhotosLevel.Items(0).Selected = True
            End Try

            Dim phtLvl As EUS_ProfilePhotosLevel = GetEUS_ProfilePhotosLevel()
            If (phtLvl IsNot Nothing) Then
                ClsCombos.SelectComboItem(cbPhotosLevel, phtLvl.PhotoLevelID)
            End If

        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub



    Sub LoadProfile(ByVal profileId As Integer)
        Try

            Dim allowDisplayLevel = 0

            'Dim masterRow As EUS_ProfilesRow = Nothing
            Dim _profilerows As New clsProfileRows(profileId)
            'Dim currentRow As EUS_ProfilesRow = _profilerows.GetMirrorRow()

            '   Dim usersHasCommunication As Boolean = False
            Dim currentRow As EUS_ProfilesRow = Nothing
            Dim masterRow As EUS_ProfilesRow = Nothing
            If (Me.MasterProfileId = profileId) Then
                currentRow = _profilerows.GetMirrorRow()
                masterRow = _profilerows.GetMasterRow()
                '   usersHasCommunication = True

            Else
                currentRow = _profilerows.GetMasterRow()
                masterRow = _profilerows.GetMasterRow()
                '   usersHasCommunication = 
                'clsUserDoes.HasCommunication(profileId, Me.MasterProfileId)
                Using cmdb As CMSDBDataContext = Me.GetCMSDBDataContext


                    Dim phtLvl As EUS_ProfilePhotosLevel = (From itm In cmdb.EUS_ProfilePhotosLevels
                                                           Where itm.FromProfileID = Me.UserIdInView AndAlso itm.ToProfileID = Me.MasterProfileId
                                                           Select itm).FirstOrDefault()


                    If (phtLvl IsNot Nothing) Then
                        allowDisplayLevel = phtLvl.PhotoLevelID
                    End If
                End Using
            End If


            If (currentRow Is Nothing) Then
                Throw New ProfileNotFoundException()
            End If

            '''''''''''''''''''''''''''''''''''''
            ' load profile photos
            '''''''''''''''''''''''''''''''''''''
            Dim dtPhotos As EUS_CustomerPhotosDataTable = DataHelpers.GetEUS_CustomerPhotos_ByProfileOrMirrorID(profileId).EUS_CustomerPhotos
            Dim otherUserPhotos1 As New List(Of clsUserListItem)
            Dim otherUserPhotosLocks As New List(Of clsUserListItem)
            'Dim otherUserPhotos2 As New List(Of clsUserListItem)

            Dim defaultPhoto As New clsUserListItem()
            defaultPhoto.ImageUrlOnClick = ""
            defaultPhoto.LoginName = currentRow.LoginName
            defaultPhoto.ProfileViewUrl = ResolveUrl("~/Members/Profile.aspx?p=" & HttpUtility.UrlEncode(currentRow.LoginName))
            Dim isMainImageSet As Boolean = False

            Dim listOfPrivateAndDefault As New List(Of Long)
            Dim hasPrivatePhotos As Boolean

            For Each dr As EUS_CustomerPhotosRow In dtPhotos

                'If (Me.MasterProfileId <> profileId AndAlso dr.DisplayLevel > allowDisplayLevel) Then
                '    Continue For
                'End If

                Dim isDeclined As Boolean = (Not dr.IsHasDeclinedNull() AndAlso dr.HasDeclined = True)
                Dim isDeleted As Boolean = (Not dr.IsIsDeletedNull() AndAlso dr.IsDeleted = True)

                If (dr.HasAproved = True AndAlso Not isDeclined AndAlso Not isDeleted) Then
                    Dim isLocked As Boolean = False
                    Dim uli As New clsUserListItem()
                    'uli.AddNewPhoto = CurrentPageData.GetCustomString("Add.New.Photo")
                    uli.ImageUrlOnClick = ""
                    uli.LoginName = currentRow.LoginName
                    uli.ProfileViewUrl = ResolveUrl("~/Members/Profile.aspx?p=" & HttpUtility.UrlEncode(currentRow.LoginName))
                    uli.ImageUploadDate = dr.DateTimeToUploading.ToLocalTime().ToString("d MMM, yyyy H:mm")

                    If (Me.MasterProfileId <> profileId AndAlso dr.DisplayLevel > allowDisplayLevel) Then

                        ' view private photo of other

                        If (Me.IsFemale) Then
                            'uli.ImageThumbUrl = ResolveUrl("~/Images2/men_level.png")
                            uli.ImageThumbUrl = ResolveUrl("~/Images2/private/list-menlevel" & dr.DisplayLevel & ".png")
                        Else
                            'uli.ImageThumbUrl = ResolveUrl("~/Images2/woman_level.png")
                            uli.ImageThumbUrl = ResolveUrl("~/Images2/private/list-womenlevel" & dr.DisplayLevel & ".png")
                        End If

                        uli.ImageUrl = "javascript:void(0);"


                        'Dim url As String = ResolveUrl("~/Members/InfoWin.aspx?info=Blocked_Private_Photo_LevelDesc2&lvl=[PHOTO_LEVEL]&g=[GENDER]&p=[LOGIN]")
                        Dim url As String = ResolveUrl("~/Members/SharePrivatePhoto.aspx?info=Blocked_Private_Photo_LevelDesc2&lvl=[PHOTO_LEVEL]&g=[GENDER]&p=[LOGIN]&id=[LOGINID]") '&popup=popupWhatIs
                        url = url.Replace("[PHOTO_LEVEL]", dr.DisplayLevel)
                        url = url.Replace("[LOGIN]", HttpUtility.UrlEncode(currentRow.LoginName))
                        url = url.Replace("[LOGINID]", currentRow.ProfileID)
                        If (Me.IsFemale) Then
                            url = url.Replace("[GENDER]", "1")
                        Else
                            url = url.Replace("[GENDER]", "2")
                        End If
                        Dim popupTitle = CurrentPageData.VerifyCustomString("Private.Photo.Level.of.Levels")
                        popupTitle = popupTitle.Replace("[LEVEL]", dr.DisplayLevel)

                        'Dim txt As String = WhatIsIt.OnMoreInfoClickFunc(url, popupTitle, 740, 450)
                        'txt = txt.Replace("OnMoreInfoClick", "openPopupSharePhoto")
                        'uli.ImageUrlOnClick = txt

                        'Dim txt As String = WhatIsIt.OnMoreInfoClickFunc(url, popupTitle, 740, 450)
                        'txt = txt.Replace("OnMoreInfoClick", "openPopupSharePhoto")
                        uli.ImageUrl = url
                        uli.ImageUrlOnClick = "openPopupSharePhoto2(this,'" & AppUtils.JS_PrepareString(popupTitle) & "');return false;"

                        'uli.PhotoLevelText = CurrentPageData.GetCustomString("Blocked.Private.Photo.View.Text")
                        'uli.PhotoLevelText = uli.PhotoLevelText.Replace("###PHOTO_LEVEL###", dr.DisplayLevel)
                        'uli.PhotoLevelText = "<div class=""photo-level-text""><div class=""photo-level-text-top"">" & _
                        '    CurrentPageData.GetCustomString("Blocked.Private.Photo.View.Text").Replace("###PHOTO_LEVEL###", "") & _
                        '    "</div><div class=""photo-level-text-number"">" & dr.DisplayLevel & "</div></div>"
                        'If (Me.IsMale) Then
                        uli.PhotoLevelText = "<div class=""photo-level-text""><div>&nbsp;</div></div>"
                        'End If
                        isLocked = True
                    Else
                        uli.PhotoLevelText = "<div class=""photo-level-text""><div>&nbsp;</div></div>"
                        uli.ImageUrl = ProfileHelper.GetProfileImageURL(dr.CustomerID, dr.FileName, currentRow.GenderId, False, Me.IsHTTPS)
                        uli.ImageThumbUrl = ProfileHelper.GetProfileImageURL(dr.CustomerID, dr.FileName, currentRow.GenderId, True, Me.IsHTTPS)
                    End If

                    'uli.PhotoLevelText = "<div class=""photo-level-text""><div>&nbsp;</div></div>"
                    uli.DisplayLevel = dr.DisplayLevel
                    uli.DisplayLevelText = ProfileHelper.GetPhotosDisplayLevelString(dr.DisplayLevel, Me.GetLag())

                    If (dr.IsDefault) Then
                        uli.DisplayLevelText = uli.DisplayLevelText & " (" & CurrentPageData.GetCustomString("Photo.Level.Default.Text") & ")"
                        imgPhoto.ImageUrl = uli.ImageThumbUrl
                        isMainImageSet = True
                    End If

                    If (isLocked) Then
                        otherUserPhotosLocks.Add(uli)
                    Else
                        otherUserPhotos1.Add(uli)
                        If (uli.DisplayLevel > 0) Then hasPrivatePhotos = True
                    End If

                    If (Not isMainImageSet AndAlso uli.DisplayLevel > 0) Then
                        imgPhoto.ImageUrl = ProfileHelper.GetPrivateImageURL(currentRow.GenderId)
                    End If

                    Try
                        'fix for private photos, that was default before 
                        If (dr.DisplayLevel > 0 AndAlso dr.IsDefault) Then
                            listOfPrivateAndDefault.Add(dr.CustomerPhotosID)
                        End If
                    Catch
                    End Try
                End If
            Next

            If (listOfPrivateAndDefault.Count > 0) Then
                'fix for private photos, that was default before 
                Try
                    Dim sql As String = "update EUS_CustomerPhotos set isdefault=0 where isdefault=1 and displaylevel>0 and isdeleted=0 and customerid=" & currentRow.ProfileID
                    DataHelpers.ExecuteNonQuery(sql)
                Catch ex As Exception
                    WebErrorSendEmail(ex, "fixing private-default photos")
                End Try
            End If

            For cnt = 0 To otherUserPhotosLocks.Count - 1
                Dim o As clsUserListItem = otherUserPhotosLocks(cnt)
                otherUserPhotos1.Add(o)
            Next


            If (otherUserPhotos1.Count = 0) Then

                'Dim uli As New clsUserListItem()
                'uli.ImageUrlOnClick = ""
                'uli.LoginName = currentRow.LoginName
                'uli.ProfileViewUrl = ResolveUrl("~/Members/Profile.aspx?p=" & HttpUtility.UrlEncode(currentRow.LoginName))
                'uli.ImageUrl = ProfileHelper.GetDefaultImageURL(currentRow.GenderId)
                'uli.ImageThumbUrl = ProfileHelper.GetDefaultImageURL(currentRow.GenderId)
                ''uli.AddNewPhoto = CurrentPageData.GetCustomString("Add.New.Photo")
                ''uli.ImageUploadDate = dr.DateTimeToUploading.ToLocalTime().ToString("d MMM, yyyy H:mm")
                'otherUserPhotos1.Add(uli)
            End If

            ' set photo's list 
            lvPhotos.DataSource = otherUserPhotos1
            lvPhotos.DataBind()

            Dim ctl As Label = lvPhotos.FindControl("lblPhotoNew")
            If (ctl IsNot Nothing) Then ctl.Text = CurrentPageData.GetCustomString("Add.New.Photo")



            ' load profile text content
            '''''''''''''''''''''''''''''''''''''

            If (Me.MasterProfileId = profileId) Then
                ' the viewing profile is owned by member it self
                '''''''''''''''''''''''''''''''''''''''''''''

                If (Not currentRow.IsLastUpdateProfileDateTimeNull()) Then
                    lblLastUpdated.Text = AppUtils.GetDateTimeString(currentRow.LastUpdateProfileDateTime, "", "dd/MM/yyyy H:mm")
                ElseIf (Not currentRow.IsDateTimeToRegisterNull()) Then
                    lblLastUpdated.Text = AppUtils.GetDateTimeString(currentRow.DateTimeToRegister, "", "dd/MM/yyyy H:mm")
                End If

                'If (Not currentRow.IsStatusNull()) Then
                '    Dim profileStatus = "(<strong>###STATUS###</strong>)"
                '    Dim statusText As String = globalStrings.GetCustomString("ProfileStatusEnum." & CType(currentRow.Status, ProfileStatusEnum).ToString(), Session("LAGID"))
                '    lblStatus.Text = profileStatus.Replace("###STATUS###", statusText)

                '    If (currentRow.Status = ProfileStatusEnum.Approved) Then
                '        lblStatus.CssClass = "approved"
                '    Else
                '        lblStatus.CssClass = "warning"
                '    End If
                'End If

                'LoadWhoViewedAndFavoritedCurrentProfile()

                profileOwner.Visible = True
                'profileOwner2.Visible = True

                If (Me.IsMale) Then
                    divLineVert2.Visible = True
                    divBilling.Visible = True

                    Try

                        'Dim sql As String = "EXEC [CustomerAvailableCredits] @CustomerId = " & Me.MasterProfileId & ""
                        'Dim dt As DataTable = DataHelpers.GetDataTable(sql)

                        'If (dt.Rows(0)("CreditsRecordsCount") > 0) Then

                        Dim __cac As clsCustomerAvailableCredits = BasePage.GetCustomerAvailableCredits(Me.MasterProfileId)
                        If (__cac.CreditsRecordsCount > 0) Then
                            lnkBilling.NavigateUrl = "~/Members/SelectProduct.aspx"
                            lnkBilling.Text = CurrentPageData.GetCustomString("Credits")
                            lnkBilling.Text = lnkBilling.Text & "(" & __cac.AvailableCredits & ")"
                        ElseIf (__cac.HasSubscription) Then
                            lnkBilling.NavigateUrl = "~/Members/SelectProduct.aspx"
                            lnkBilling.Text = CurrentPageData.GetCustomString("Credits")
                        Else
                            lnkBilling.NavigateUrl = "~/Members/SelectProduct.aspx"
                            lnkBilling.Text = CurrentPageData.GetCustomString("StandardMember")
                        End If

                    Catch ex As Exception
                        WebErrorMessageBox(Me, ex, "")
                    End Try

                Else
                    divLineVert2.Visible = False
                    divBilling.Visible = False
                End If

            Else
                ' the viewing profile is owned by another member
                '''''''''''''''''''''''''''''''''''''''''''''

                Dim ctlDiv As Control = lvPhotos.FindControl("divAddNew")
                If (ctlDiv IsNot Nothing) Then ctlDiv.Visible = False
                profileOwner.Visible = False

                'If ProfileHelper.IsMale(currentRow.GenderId) Then
                SetPhotoLevelText(currentRow.LoginName)

                lblAllowAccessPhotosLevel.Text = CurrentPageData.VerifyCustomString("lblAllowAccessPhotosLevel").Replace("###LOGINNAME###", currentRow.LoginName)
                lblAllowAccessPhotosLevelBottom.Text = CurrentPageData.GetCustomString("lblAllowAccessPhotosLevelBottom")

                btnSave.Text = CurrentPageData.GetCustomString("btnSave")
            End If

            If (Me.MasterProfileId = profileId) Then
                pLastLoggedInDT.Visible = True
            Else
                If (currentRow.IsPrivacySettings_HideMyLastLoginDateTimeNull() OrElse currentRow.PrivacySettings_HideMyLastLoginDateTime = False) Then
                    pLastLoggedInDT.Visible = True
                Else
                    pLastLoggedInDT.Visible = False
                End If
            End If

            Dim MemberConnectDescript As MemberConnectDescriptionEnum
            Dim recentlyActiveHours As Integer = clsConfigValues.Get__members_online_recently_hours()
            If (pLastLoggedInDT.Visible) Then
                If (Not currentRow.IsLastActivityDateTimeNull()) Then
                    MemberConnectDescript = AppUtils.GetDateTimeDescription_MemberConnect(currentRow.LastActivityDateTime, recentlyActiveHours)
                    Dim descrLogin As String = AppUtils.GetLoginDescription(MemberConnectDescript, CurrentPageData)
                    lblLastLoginDT.Text = descrLogin

                ElseIf (Not currentRow.IsLastLoginDateTimeNull()) Then
                    MemberConnectDescript = AppUtils.GetDateTimeDescription_MemberConnect(currentRow.LastLoginDateTime, recentlyActiveHours)
                    Dim descrLogin As String = AppUtils.GetLoginDescription(MemberConnectDescript, CurrentPageData)
                    lblLastLoginDT.Text = descrLogin

                    '& " (" & AppUtils.GetDateTimeString(currentRow.LastLoginDateTime, "", "dd/MM/yyyy H:mm") & ")"
                    ''lblLastLoginDT.Text = AppUtils.GetDateTimeString(currentRow.LastLoginDateTime, "", "dd/MM/yyyy H:mm")

                ElseIf (Not currentRow.IsDateTimeToRegisterNull()) Then
                    MemberConnectDescript = AppUtils.GetDateTimeDescription_MemberConnect(currentRow.DateTimeToRegister, recentlyActiveHours)
                    Dim descrLogin As String = AppUtils.GetLoginDescription(MemberConnectDescript, CurrentPageData)
                    lblLastLoginDT.Text = descrLogin

                    '& " (" & AppUtils.GetDateTimeString(currentRow.DateTimeToRegister, "", "dd/MM/yyyy H:mm") & ")"
                    ''lblLastLoginDT.Text = AppUtils.GetDateTimeString(currentRow.DateTimeToRegister, "", "dd/MM/yyyy H:mm")
                End If
            End If


            Dim mins As Integer = 20
            Dim config As New clsConfigValues()
            If (Not Integer.TryParse(config.members_online_minutes, mins)) Then mins = 20
            Dim LastActivityUTCDate As DateTime = Date.UtcNow.AddMinutes(-mins)
            '   Dim offlineGetLastActivity As Boolean

            If (Not currentRow.IsLastActivityDateTimeNull() AndAlso Not currentRow.IsIsOnlineNull()) Then
                '   offlineGetLastActivity = True
                If (currentRow.LastActivityDateTime >= LastActivityUTCDate AndAlso currentRow.IsOnline) Then
                    divIsOnline.Visible = True
                    If (clsProfilesPrivacySettings.GET_PrivacySettings_ShowMeOffline(currentRow.ProfileID)) Then
                        divIsOnline.Visible = False
                    End If
                End If
            End If

            If (Not divIsOnline.Visible AndAlso (MemberConnectDescriptionEnum.MemberConnect_Recently = MemberConnectDescript)) Then

                Dim onlineCls As String = divIsOnline.Attributes("class")
                If (onlineCls Is Nothing) Then
                    onlineCls = ""
                    divIsOnline.Attributes.Add("class", "")
                End If
                onlineCls = onlineCls & " online-recent"
                divIsOnline.Attributes("class") = onlineCls
                divIsOnline.Visible = True

            End If

            If (Not currentRow.IsGenderIdNull() AndAlso Not currentRow.IsAvailableCreditsNull()) Then
                If (currentRow.GenderId = 1 AndAlso currentRow.AvailableCredits >= ProfileHelper.Config_UNLOCK_MESSAGE_READ_CREDITS) Then
                    imgVip.Visible = True
                End If
            End If


            lblLogin.Text = currentRow.LoginName

            If ProfileHelper.IsMale(currentRow.GenderId) Then
                liAnnualIncome.Visible = True
            ElseIf ProfileHelper.IsFemale(currentRow.GenderId) Then
                liAnnualIncome.Visible = False
            End If

            ShowBreastSize(masterRow)
            ShowHairColor(masterRow)
            ShowSmoking(masterRow)
            ShowDrinking(masterRow)

            'lblAccountType.Text = "(" & lblAccountType.Text & ")"


            ShowWantVisit(currentRow, profileId)


            lblAboutMeHeading.Visible = False
            If (Not currentRow.IsAboutMe_HeadingNull()) Then
                lblAboutMeHeading.Text = currentRow.AboutMe_Heading
                lblAboutMeHeading.Visible = True
            End If


            If (Not currentRow.IsAboutMe_DescribeYourselfNull()) Then
                lblAboutMeDescr.Text = currentRow.AboutMe_DescribeYourself
            End If
            'msg_AboutMeDescrTitle.Visible = (lblAboutMeDescr.Text.Trim() <> "")


            If (Not currentRow.IsAboutMe_DescribeAnIdealFirstDateNull()) Then
                lblFirstDateExpectationDescr.Text = currentRow.AboutMe_DescribeAnIdealFirstDate
            End If
            'msg_FirstDateExpectationDescrTitle.Visible = (lblFirstDateExpectationDescr.Text.Trim() <> "")


            SetLocationText(currentRow)
            If (Not currentRow.IsBirthdayNull()) Then
                ShowAgeAndZodiacs(currentRow)
            End If

            If (Not currentRow.IsPersonalInfo_HeightIDNull()) Then
                msg_Height.Text = Me.CurrentPageData.GetCustomString("msg_Height") & ": "
                lblHeight_Top.Text = ProfileHelper.GetHeightString(currentRow.PersonalInfo_HeightID, Me.Session("LagID"))
                lblHeight.Text = ProfileHelper.GetHeightString(currentRow.PersonalInfo_HeightID, Me.Session("LagID"))
                lblHeight_Num.Text = lblHeight_Top.Text '.Replace("cm", "").Trim()
            End If


            If (Not currentRow.IsPersonalInfo_BodyTypeIDNull()) Then
                lblBodyType.Text = ProfileHelper.GetBodyTypeString(currentRow.PersonalInfo_BodyTypeID, Me.Session("LagID"))
            End If


            If (Not currentRow.IsPersonalInfo_HairColorIDNull()) Then
                lblHairClr.Text = ProfileHelper.GetHairColorString(currentRow.PersonalInfo_HairColorID, Me.Session("LagID"))
            End If


            If (Not currentRow.IsPersonalInfo_EyeColorIDNull()) Then
                lblEyeClr.Text = ProfileHelper.GetEyeColorString(currentRow.PersonalInfo_EyeColorID, Me.Session("LagID"))
            End If


            If (Not currentRow.IsOtherDetails_EducationIDNull()) Then
                lblEducation.Text = ProfileHelper.GetEducationString(currentRow.OtherDetails_EducationID, Me.Session("LagID"))
            End If


            If (Not currentRow.IsPersonalInfo_ChildrenIDNull()) Then
                lblChildren.Text = ProfileHelper.GetChildrenNumberString(currentRow.PersonalInfo_ChildrenID, Me.Session("LagID"))
            End If


            If (Not currentRow.IsPersonalInfo_EthnicityIDNull()) Then
                lblEthnicity.Text = ProfileHelper.GetEthnicityString(currentRow.PersonalInfo_EthnicityID, Me.Session("LagID"))
            End If


            If (Not currentRow.IsPersonalInfo_ReligionIDNull()) Then
                lblReligion.Text = ProfileHelper.GetReligionString(currentRow.PersonalInfo_ReligionID, Me.Session("LagID"))
            End If


            If (Not currentRow.IsPersonalInfo_SmokingHabitIDNull()) Then
                lblSmoking.Text = ProfileHelper.GetSmokingString(currentRow.PersonalInfo_SmokingHabitID, Me.Session("LagID"))
            End If


            If (Not currentRow.IsPersonalInfo_DrinkingHabitIDNull()) Then
                lblDrinking.Text = ProfileHelper.GetDrinkingString(currentRow.PersonalInfo_DrinkingHabitID, Me.Session("LagID"))
            End If


            If (Not currentRow.IsOtherDetails_OccupationNull()) Then
                lblOccupation.Text = currentRow.OtherDetails_Occupation
            End If


            If (Not currentRow.IsOtherDetails_AnnualIncomeIDNull()) Then
                lblIncomeLevel.Text = ProfileHelper.GetIncomeString(currentRow.OtherDetails_AnnualIncomeID, Me.Session("LagID"))
            End If


            If (Not currentRow.IsOtherDetails_NetWorthIDNull()) Then
                lblNetWorth.Text = ProfileHelper.GetNetWorthString(currentRow.OtherDetails_NetWorthID, Me.Session("LagID"))
            End If

            If (Not currentRow.IsLookingFor_RelationshipStatusIDNull()) Then
                ShowRelationshipStatus(currentRow, profileId)
            End If


            lblLookingToMeet.Text = ""
            If (Not currentRow.IsLookingFor_ToMeetMaleIDNull() AndAlso currentRow.LookingFor_ToMeetMaleID) Then
                lblLookingToMeet.Text = lblLookingToMeet.Text & ProfileHelper.GetLookingToMeet_Male_String(Me.LagID) & ", "
            End If

            If (Not currentRow.IsLookingFor_ToMeetFemaleIDNull() AndAlso currentRow.LookingFor_ToMeetFemaleID) Then
                lblLookingToMeet.Text = lblLookingToMeet.Text & ProfileHelper.GetLookingToMeet_Female_String(Me.LagID) & ", "
            End If

            lblLookingToMeet.Text = lblLookingToMeet.Text.TrimEnd(" "c, ","c)

            ShowTypeOfDate(currentRow, profileId)

            If (Me.IsFemale) Then
                VIPTooltip = Me.CurrentPageData.GetCustomString("lbTooltip.MenVIP.ForWomen")
            Else
                VIPTooltip = Me.CurrentPageData.GetCustomString("lbTooltip.MenVIP.ForMen")
            End If

            ShowLangs(currentRow, profileId)
            ShowWantJob(currentRow, profileId)
            ShowHappyBirthday(currentRow)
            If (Me.MasterProfileId <> profileId) Then

                ShowUserMap(currentRow, profileId)
                UpdateMembersData(profileId, hasPrivatePhotos)

            End If

        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Private Sub UpdateMembersData(profileId As Integer, hasPrivatePhotos As Boolean)

        ' update EUS_ProfilesViewed records set IsToProfileIDViewed to true, where  ToProfileID is current member
        Using ctx As New CMSDBDataContext(ModGlobals.ConnectionString)


            Try

                'clsUserDoes.MarkAsViewed(Me.UserIdInView, Me.MasterProfileId)
                Dim notifyMember As Boolean = Not (TypeOf Page Is BasePage)
                Dim done As Boolean = clsUserDoes.MarkAsViewed(ctx, profileId, Me.MasterProfileId, notifyMember)
                If (done AndAlso Not notifyMember) Then
                    Dim task As New PageOnUnloadTask()
                    task.FromProfileID = Me.MasterProfileId
                    task.ToProfileID = profileId
                    task.NotificationType = NotificationType.ProfileViewed
                    DirectCast(Page, BasePage).UnloadingTasks.Add(task)
                End If
                clsUserDoes.MarkDateOfferAsViewed(ctx, profileId, Me.MasterProfileId)
                clsUserDoes.MarkProfileDataAsViewed(ctx, profileId, Me.MasterProfileId, hasPrivatePhotos)

            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "updating my profile viewed record")
            
            End Try
        End Using
    End Sub

    Protected Sub lvPhotos_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ListViewItemEventArgs) Handles lvPhotos.ItemDataBound, lvPhotos.ItemCreated
        If (e.Item.ItemType = ListViewItemType.EmptyItem) Then
            Dim lblNoPhotos As Label = e.Item.FindControl("lblNoPhotos")
            If (lblNoPhotos IsNot Nothing) Then
                lblNoPhotos.Text = CurrentPageData.VerifyCustomString("Member.Has.No.Photos")
                lblNoPhotos.Text = lblNoPhotos.Text.Replace("[LOGIN-NAME]", Me.UserLoginNameInView)
            End If
        End If
    End Sub


    Private Sub SetLocationText(currentRow As EUS_ProfilesRow)
        Dim _locationStrCity = ""
        Dim _locationStrRegion = ""
        Dim _locationStrCountry = ""
        Dim _birthDay = ""

        lblFrom.Text = ""
        lblBirth.Text = ""

        If (Not currentRow.IsCityNull()) Then
            _locationStrCity = currentRow.City
        End If
        If (Not currentRow.Is_RegionNull()) Then
            _locationStrRegion = currentRow._Region
        End If
        If (Not currentRow.IsCountryNull()) Then
            _locationStrCountry = ProfileHelper.GetCountryName(currentRow.Country)
        End If
        If (Not currentRow.IsBirthdayNull()) Then
            'Dim bornon As String = Me.CurrentPageData.GetCustomString("BornOn")
            'bornon = bornon.Replace("###DATE###", currentRow.Birthday.ToString("d MMM, yyyy"))
            '_birthDay = bornon
            _birthDay = Me.CurrentPageData.VerifyCustomString("lblAgeYearsOld").Replace("###AGE###", ProfileHelper.GetCurrentAge(currentRow.Birthday))
        End If

        If (_locationStrCity <> "") Then
            lblFrom.Text = "<strong>" & _locationStrCity & "</strong>"
        End If
        If (_locationStrRegion <> "" AndAlso _locationStrCity <> "") Then
            lblFrom.Text = lblFrom.Text & ", "
        End If
        If (_locationStrRegion <> "") Then
            lblFrom.Text = lblFrom.Text & _locationStrRegion
        End If
        If (_locationStrRegion <> "" AndAlso _locationStrCountry <> "") Then
            lblFrom.Text = lblFrom.Text & ", "
        End If
        If (_locationStrCountry <> "") Then
            Dim fromStr = globalStrings.GetCustomString("FromWord")
            lblFrom.Text = fromStr & ":&nbsp;" & lblFrom.Text & _locationStrCountry
        End If


        If (_birthDay <> "") Then
            lblBirth.Text = _birthDay
            lblBirthDay.Text = "(" & currentRow.Birthday.ToString("d MMM, yyyy") & ")"
        End If
    End Sub


    Public Function HasOffer(userIdOfferReceiver As Integer, userIdWhoDid As Integer) As Boolean
        Dim val As Boolean
        Try

            If (clsUserDoes.GetLastOffer(userIdOfferReceiver, userIdWhoDid) IsNot Nothing) Then
                val = True
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
        Return val
    End Function


    Protected Sub lnkFavorite_Command(sender As Object, e As System.Web.UI.WebControls.CommandEventArgs) 'Handles lnkFavoriteF.Command, lnkFavoriteM.Command
        Try
            Dim _userId As Integer = CType(e.CommandArgument, Integer)
            If (_userId > 0) Then
                clsUserDoes.MarkAsFavorite(_userId, Me.MasterProfileId)
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Try
            LoadView()
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Protected Sub lnkMakeOffer_Command(sender As Object, e As System.Web.UI.WebControls.CommandEventArgs) Handles lnkMakeOfferF.Command, lnkMakeOfferM.Command
        Try
            Dim _userId As Integer = CType(e.CommandArgument, Integer)
            If (_userId > 0) Then

                Dim currentUserHasPhotos As Boolean = clsUserDoes.HasPhotos(Me.MasterProfileId, Me.MirrorProfileId)
                If (currentUserHasPhotos) Then

                    Dim CreateOfferUrl As String = ""

                    Dim rec As EUS_Offer = clsUserDoes.GetAnyWinkPending(_userId, Me.MasterProfileId)
                    Dim offerId As Integer = 0
                    If (rec IsNot Nothing) Then
                        offerId = rec.OfferID
                        CreateOfferUrl = ResolveUrl("~/Members/CreateOffer.aspx?offer=" & offerId)
                    Else
                        Using cmdb As CMSDBDataContext = Me.GetCMSDBDataContext


                            Dim prof As EUS_Profile = (From itm In cmdb.EUS_Profiles
                                                      Where itm.ProfileID = _userId AndAlso _
                                                            itm.IsMaster = True
                                                      Select itm).FirstOrDefault()


                            CreateOfferUrl = ResolveUrl("~/Members/CreateOffer.aspx?offerto=" & prof.LoginName)
                        End Using
  End If


                    Response.Redirect(CreateOfferUrl, False)
                    HttpContext.Current.ApplicationInstance.CompleteRequest()
                    'Dim clsUserDoes As New clsUserDoes
                    'clsUserDoes.Make(_userId, Me.MasterProfileId)
                    'lnkMakeOffer.Visible = False
                Else
                    Response.Redirect(ResolveUrl("~/Members/Photos.aspx"), False)
                    HttpContext.Current.ApplicationInstance.CompleteRequest()
                End If
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Protected Sub lnkBlock_Command(sender As Object, e As System.Web.UI.WebControls.CommandEventArgs) Handles lnkBlock.Command
        Try
            Dim _userId As Integer = CType(e.CommandArgument, Integer)
            If (_userId > 0) Then
                clsUserDoes.MarkAsBlocked(_userId, Me.MasterProfileId)
                lnkBlock.Visible = False
                ltrSeparator.Visible = False
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Try
            LoadView()
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Protected Sub lnkUnFavorite_Command(sender As Object, e As System.Web.UI.WebControls.CommandEventArgs) ' Handles lnkUnFavoriteF.Command, lnkUnFavoriteM.Command
        Try
            Dim _userId As Integer = CType(e.CommandArgument, Integer)
            If (_userId > 0) Then
                clsUserDoes.MarkAsUnfavorite(_userId, Me.MasterProfileId)
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Try
            LoadView()
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub




    Protected Sub lnkWink_Command(sender As Object, e As System.Web.UI.WebControls.CommandEventArgs) 'Handles lnkWinkF.Command, lnkWinkM.Command
        Try

            If (Not String.IsNullOrEmpty(e.CommandArgument)) Then
                Dim currentUserHasPhotos As Boolean = clsUserDoes.HasPhotos(Me.MasterProfileId, Me.MirrorProfileId)
                If (currentUserHasPhotos) Then

                    Dim ToProfileID As Integer
                    Integer.TryParse(e.CommandArgument.ToString(), ToProfileID)

                    Dim MaySendWink As Boolean = True
                    Dim MaySendWink_CheckSetting As Boolean = True


                    MaySendWink_CheckSetting = Not clsProfilesPrivacySettings.GET_DisableReceivingLIKESFromDifferentCountryFromTo(ToProfileID, Me.MasterProfileId)
                    If (MaySendWink_CheckSetting = False) Then
                        MaySendWink = False
                    End If


                    If (MaySendWink AndAlso Me.IsFemale) Then
                        If (Me.IsReferrer) Then
                            MaySendWink = clsUserDoes.IsReferrerAllowedSendWink(ToProfileID, Me.MasterProfileId)
                        End If
                    End If

                    If (MaySendWink) Then
                        clsUserDoes.SendWink(ToProfileID, Me.MasterProfileId)
                    Else
                        If (MaySendWink_CheckSetting = False) Then
                            Dim popupMessage As String = WhatIsIt.OnMoreInfoClickFunc(ResolveUrl("~/Members/InfoWin.aspx?info=Error_Sending_Receiver_Disabled_Likes_From_Diff_Country&popup=popupWhatIs"), "Error! Can't send", 650, 350)
                            popupMessage = "function fnError_Sending_Receiver_DoNot_Have_Photo(){" & popupMessage & "};"
                            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Error_Sending_Receiver_DoNot_Have_Photo", popupMessage, True)
                        Else
                            Dim popupMessage As String = WhatIsIt.OnMoreInfoClickFunc(ResolveUrl("~/Members/InfoWin.aspx?info=Error_Sending_Receiver_DoNot_Have_Photo&popup=popupWhatIs"), "Error! Can't send", 650, 350)
                            popupMessage = "function fnError_Sending_Receiver_DoNot_Have_Photo(){" & popupMessage & "};"
                            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Error_Sending_Receiver_DoNot_Have_Photo", popupMessage, True)
                        End If
                    End If

                Else
                    Response.Redirect(ResolveUrl("~/Members/Photos.aspx"), False)
                    HttpContext.Current.ApplicationInstance.CompleteRequest()
                End If
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Try
            LoadView()
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Protected Sub lnkUnWink_Command(sender As Object, e As System.Web.UI.WebControls.CommandEventArgs) 'Handles lnkUnWinkF.Command, lnkUnWinkM.Command
        Try
            If (Not String.IsNullOrEmpty(e.CommandArgument)) Then

                Dim ToProfileID As Integer
                Integer.TryParse(e.CommandArgument.ToString(), ToProfileID)

                clsUserDoes.CancelPendingWink(ToProfileID, Me.MasterProfileId)
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Try
            LoadView()
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Function WriteActionTopRowStyle() As String
        If (pPhotosLevel.Visible = True) Then Return "style='border-bottom:none;'"

        Return ""
    End Function

    Function WriteUserInsightClass() As String
        If (pPhotosLevel.Visible = True) Then Return "p_actions_insight" Else Return "p_actions_insight_onerow"

        Return ""
    End Function

    Protected Sub cbpnlPhotos_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cbpnlPhotos.Callback

        Try
            Dim lvl As Integer = cbPhotosLevel.SelectedItem.Value 'e.Parameter.Replace("lvl_", "")
            Try
                clsUserDoes.SharePhoto(Me.MasterProfileId, Me.UserIdInView, lvl)
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try
            LoadLAG()
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub

    Protected Sub cbpnlPhotosLevelText_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cbpnlPhotosLevelText.Callback

        Try
            SetPhotoLevelText(Me.UserLoginNameInView)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


    Private Function GetEUS_ProfilePhotosLevel() As EUS_ProfilePhotosLevel
        Dim phtLvl As EUS_ProfilePhotosLevel = Nothing
        Try
            Using cmdb As CMSDBDataContext = Me.GetCMSDBDataContext


                phtLvl = (From itm In cmdb.EUS_ProfilePhotosLevels
                                                       Where itm.FromProfileID = Me.MasterProfileId AndAlso itm.ToProfileID = Me.UserIdInView
                                                       Select itm).FirstOrDefault()
            End Using

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
        Return phtLvl
    End Function



    Protected Sub lnkUnlockOffer_Command(sender As Object, e As System.Web.UI.WebControls.CommandEventArgs) Handles lnkUnlockOfferF.Command, lnkUnlockOfferM.Command
        Try
            If (e.CommandName = OfferControlCommandEnum.UNLOCKOFFER) Then

                Dim offerID As Integer
                Integer.TryParse(e.CommandArgument.ToString(), offerID)
                Dim success As Boolean
                If (AllowUnlimited) Then
                    success = clsUserDoes.PerformOfferUnlock(offerID, Me.MasterProfileId)
                Else
                    success = True
                End If

                If (success) Then
                    LoadView()
                End If
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub



    Public Sub ShowUserMap(lat As Double, lng As Double, radius As Integer, Optional zoom As Integer = 8)

        Try
            Dim mapPath As String = System.Web.VirtualPathUtility.ToAbsolute("~/map.aspx?lat=" & lat & "&lng=" & lng & "&radius=" & radius & "&zoom=" & zoom)

            divMapContainer.Visible = True
            ifrUserMap_src = mapPath
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


    Public Function GetGenderClass() As String
        Dim cls As String = If(MyBase.IsMale AndAlso UserIdInView = 0, "me-male", "")
        cls = cls & If(MyBase.IsFemale AndAlso UserIdInView = 0, "me-female", "")
        cls = cls & If(UserInView_IsMale, "other-male", "")
        cls = cls & If(UserInView_IsFemale, "other-female", "")
        Return cls
    End Function


    Private Sub ShowBreastSize(currentRow As EUS_ProfilesRow)
        Try

            imgBreastInfo.Visible = False
            If ProfileHelper.IsFemale(currentRow.GenderId) Then

                Dim pbs As EUS_ProfileBreastSize = Nothing
                Using cmdb As CMSDBDataContext = Me.GetCMSDBDataContext
                    pbs = (From itm In cmdb.EUS_ProfileBreastSizes
                             Where itm.ProfileID = currentRow.ProfileID
                                     Select itm).FirstOrDefault()
                End Using
                          

                If (pbs IsNot Nothing AndAlso pbs.BreastSizeID > 1) Then
                    Dim str As String = ProfileHelper.GetBreastSizeString(pbs.BreastSizeID, "US")
                    If (Not String.IsNullOrEmpty(str)) Then
                        str = " size-" & str.ToLower().Replace("+", "plus")
                        If (imgBreastInfo.Attributes("class") Is Nothing) Then
                            imgBreastInfo.Attributes.Add("class", str)
                        Else
                            imgBreastInfo.Attributes("class") = imgBreastInfo.Attributes("class") & " " & str
                        End If

                        imgBreastInfo.Visible = True
                        lblBreastInfo.Text = Me.CurrentPageData.GetCustomString("BreastSize.Tooltip") & "&nbsp;" & ProfileHelper.GetBreastSizeString(pbs.BreastSizeID, Session("LAGID"))

                    End If
                End If

            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try


    End Sub

    Private Sub ShowHairColor(currentRow As EUS_ProfilesRow)
        Try

            imgHairInfo.Visible = False

            If Not currentRow.IsPersonalInfo_HairColorIDNull() AndAlso
                ProfileHelper.IsFemale(currentRow.GenderId) Then

                If (currentRow.PersonalInfo_HairColorID > 10) Then
                    Dim str As String = ProfileHelper.GetHairColorString(currentRow.PersonalInfo_HairColorID, "US")
                    If (Not String.IsNullOrEmpty(str)) Then
                        str = " clr-" & str.ToLower().Replace(" ", "-")
                        If (imgHairInfo.Attributes("class") Is Nothing) Then
                            imgHairInfo.Attributes.Add("class", str)
                        Else
                            imgHairInfo.Attributes("class") = imgHairInfo.Attributes("class") & " " & str
                        End If
                        imgHairInfo.Visible = True
                    End If
                End If

            End If

            If (imgHairInfo.Visible) Then
                lblHairInfo.Text = Me.CurrentPageData.GetCustomString("HairColor.Tooltip") & "&nbsp;" & ProfileHelper.GetHairColorString(currentRow.PersonalInfo_HairColorID, Session("LAGID"))
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


    Private Sub ShowSmoking(currentRow As EUS_ProfilesRow)
        Try

            imgSmokingInfo.Visible = False

            If Not currentRow.IsPersonalInfo_SmokingHabitIDNull() AndAlso
                ProfileHelper.IsMale(currentRow.GenderId) Then

                If (currentRow.PersonalInfo_SmokingHabitID > 1) Then imgSmokingInfo.Visible = True

                If (currentRow.PersonalInfo_SmokingHabitID > 2) Then
                    If (imgSmokingInfo.Attributes("class") Is Nothing) Then
                        imgSmokingInfo.Attributes.Add("class", "smokes")
                    Else
                        imgSmokingInfo.Attributes("class") = imgSmokingInfo.Attributes("class") & " " & "smokes"
                    End If
                End If

            End If

            If (imgSmokingInfo.Visible) Then
                lblSmokingInfo.Text = ProfileHelper.GetSmokingString(currentRow.PersonalInfo_SmokingHabitID, Session("LAGID"))
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


    Private Sub ShowDrinking(currentRow As EUS_ProfilesRow)
        Try

            imgDrinkingInfo.Visible = False

            If Not currentRow.IsPersonalInfo_DrinkingHabitIDNull() AndAlso
                ProfileHelper.IsMale(currentRow.GenderId) Then

                If (currentRow.PersonalInfo_DrinkingHabitID > 1) Then imgDrinkingInfo.Visible = True

                If (currentRow.PersonalInfo_DrinkingHabitID > 2) Then
                    If (imgDrinkingInfo.Attributes("class") Is Nothing) Then
                        imgDrinkingInfo.Attributes.Add("class", "drinks")
                    Else
                        imgDrinkingInfo.Attributes("class") = imgDrinkingInfo.Attributes("class") & " " & "drinks"
                    End If
                End If
            End If

            If (imgDrinkingInfo.Visible) Then
                lblDrinkingInfo.Text = ProfileHelper.GetDrinkingString(currentRow.PersonalInfo_DrinkingHabitID, Session("LAGID"))
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


    Private Sub ShowAgeAndZodiacs(currentRow As EUS_ProfilesRow)
        Try

            msg_Age_Top.Text = Me.CurrentPageData.GetCustomString("msg_Age") & ": "
            lblAge.Text = Me.CurrentPageData.GetCustomString("lblAgeYearsOld")
            lblAge.Text = lblAge.Text.Replace("###AGE###", ProfileHelper.GetCurrentAge(currentRow.Birthday)) & " "


            Dim zodiacName As String = "Zodiac" & ProfileHelper.ZodiacName(currentRow.Birthday)
            Dim zodiac As String = globalStrings.GetCustomString(zodiacName, Session("LAGID"))
            lblZwdio.Text = String.Format("({0})", zodiac)
            lblZwdio_Top.Text = zodiac
            imgZodiac.Alt = zodiac
            If (imgZodiac.Attributes("class") Is Nothing) Then
                imgZodiac.Attributes.Add("class", zodiacName)
            Else
                imgZodiac.Attributes("class") = imgZodiac.Attributes("class") & " " & zodiacName
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub

    Private Sub ShowWantVisit(currentRow As EUS_ProfilesRow, profileId As Integer)
        Try

            'lblWillTravel.Visible = False
            want_to_visit_wrap.Visible = False
            lvWantVisit.Visible = False
            pnlWantVisitInfo.Visible = False
            Dim countriesLoaded As Boolean
            Dim isMyprofileInView As Boolean = (Me.MasterProfileId = profileId)

            If (isMyprofileInView) Then

                want_to_visit_wrap.Visible = True
                lvWantVisit.Visible = True
                pnlWantVisitInfo.Visible = True

            ElseIf (Not currentRow.IsAreYouWillingToTravelNull()) Then
                If (currentRow.AreYouWillingToTravel) Then
                    want_to_visit_wrap.Visible = True
                    lvWantVisit.Visible = True
                    pnlWantVisitInfo.Visible = True
                End If
            End If



            If (lvWantVisit.Visible) Then
                Try
                    Dim loc As List(Of EUS_ProfileLocation) = Nothing
                    Using cmdb As CMSDBDataContext = Me.GetCMSDBDataContext


                        loc = (From itm In cmdb.EUS_ProfileLocations
                                                         Where itm.ProfileID = profileId
                                                         Select itm).ToList()
                    End Using
                    If loc IsNot Nothing Then


                        For c1 = 0 To loc.Count - 1
                            If (loc(c1).LocalityType = "country") Then
                                loc(c1).Descr = loc(c1).Loc_country
                            ElseIf (Not String.IsNullOrEmpty(loc(c1).Loc_city)) Then
                                loc(c1).Descr = loc(c1).Loc_country & ", " & loc(c1).Loc_city
                            ElseIf (Not String.IsNullOrEmpty(loc(c1).Loc_region)) Then
                                loc(c1).Descr = loc(c1).Loc_country & ", " & loc(c1).Loc_region
                            End If
                            loc(c1).Descr = loc(c1).Descr.Trim(New Char() {" "c, ","})
                        Next
                    End If
                    lvWantVisit.DataSource = loc
                    lvWantVisit.DataBind()

                    If (loc.Count > 0) Then countriesLoaded = True

                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "Loading want to visit places")
                End Try


                If (Not isMyprofileInView) Then

                    If Me.UserInView_IsMale Then
                        lblWantVisitInfo.Text = Me.CurrentPageData.VerifyCustomString("msg_WantToVisitTitle.Male.Tooltip")
                        msg_WantToVisitTitle.Text = Me.CurrentPageData.VerifyCustomString("lblWillTravel_ViewOtherProfile_MALE")
                    ElseIf Me.UserInView_IsFemale Then
                        lblWantVisitInfo.Text = Me.CurrentPageData.VerifyCustomString("msg_WantToVisitTitle.Female.Tooltip")
                        msg_WantToVisitTitle.Text = Me.CurrentPageData.VerifyCustomString("lblWillTravel_ViewOtherProfile_FEMALE")
                    End If

                    msg_WantToVisitTitle.Text = msg_WantToVisitTitle.Text.Replace("[LOGIN-NAME]", currentRow.LoginName)
                    lnkSendMessageTravel.NavigateUrl = lnkSendMessageTravel.NavigateUrl.Replace("[LOGIN-NAME]", currentRow.LoginName)
                Else

                    If (Me.IsFemale) Then
                        lblWantVisitInfo.Text = Me.CurrentPageData.VerifyCustomString("msg_WantToVisitTitle.Female.Tooltip")
                    Else
                        lblWantVisitInfo.Text = Me.CurrentPageData.VerifyCustomString("msg_WantToVisitTitle.Male.Tooltip")
                    End If

                    lnkSendMessageTravel.NavigateUrl = "/Members/Profile.aspx?do=edit&scr=travel"
                    lnkSendMessageTravel.Text = Me.CurrentPageData.VerifyCustomString("msg_WantToVisitTitle.Add.Places")
                End If

                If (Not countriesLoaded AndAlso Not isMyprofileInView) Then
                    ' when members views other profile and not countriesLoaded

                    want_to_visit_wrap.Visible = False
                    lvWantVisit.Visible = False
                    pnlWantVisitInfo.Visible = False

                End If

            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub

    Private Sub ShowRelationshipStatus(currentRow As EUS_ProfilesRow, profileId As Integer)
        Try
            If Not currentRow.IsLookingFor_RelationshipStatusIDNull() Then

                Dim Tooltip1 As String = ""
                If (ProfileHelper.IsFemale(currentRow.GenderId)) Then
                    If currentRow.LookingFor_RelationshipStatusID = 6 Then

                        Tooltip1 = Me.CurrentPageData.GetCustomString("RelationshipStatus.Married.Female.Tooltip")
                    Else
                        Tooltip1 = Me.CurrentPageData.GetCustomString("RelationshipStatus.Single.Female.Tooltip")
                    End If
                End If
                If (String.IsNullOrEmpty(Tooltip1)) Then Tooltip1 = ProfileHelper.GetRelationshipStatusString(currentRow.LookingFor_RelationshipStatusID, Me.Session("LagID"))

                lblRelationshipStatus.Text = Tooltip1
                lblRelationshipStatus_Top.Text = Tooltip1

                If (Me.MasterProfileId <> profileId) Then
                    If Me.UserInView_IsFemale Then
                        If currentRow.LookingFor_RelationshipStatusID = 6 Then
                            pnlStatusInfo.CssClass += " woman-icon married"
                        Else
                            pnlStatusInfo.CssClass += " woman-icon female_single"
                        End If
                    Else
                        If currentRow.LookingFor_RelationshipStatusID = 6 Then
                            pnlStatusInfo.CssClass += " man-icon married"
                        Else
                            pnlStatusInfo.CssClass += " man-icon male_single"
                        End If
                    End If
                Else
                    If Me.IsFemale Then
                        If currentRow.LookingFor_RelationshipStatusID = 6 Then
                            pnlStatusInfo.CssClass += " woman-icon married"
                        Else
                            pnlStatusInfo.CssClass += " woman-icon female_single"
                        End If
                    Else
                        If currentRow.LookingFor_RelationshipStatusID = 6 Then
                            pnlStatusInfo.CssClass += " man-icon married"
                        Else
                            pnlStatusInfo.CssClass += " man-icon male_single"
                        End If
                    End If
                End If


            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


    Private Sub ShowLangs(currentRow As EUS_ProfilesRow, profileId As Integer)
        Dim isMyprofileInView As Boolean = (Me.MasterProfileId = profileId)
        Try
            Dim oldList As List(Of EUS_LISTS_SpokenLanguage) = Nothing
            Using cmdb As CMSDBDataContext = Me.GetCMSDBDataContext
                oldList = (From itm In cmdb.EUS_ProfileSpokenLangs
                                    Join itm2 In cmdb.EUS_LISTS_SpokenLanguages On itm.SpokenLangId Equals itm2.SpokenLangId
                                    Where (itm.ProfileID = profileId)
                                    Select itm2).ToList()

            End Using
            If oldList IsNot Nothing Then
                Dim langs As New List(Of String)(oldList.Count)
                For c1 = 0 To oldList.Count - 1
                    Dim lang As String = oldList(c1).LangName
                    If (Not String.IsNullOrEmpty(lang)) Then
                        If (c1 < oldList.Count - 1) Then lang = lang & ", "
                        langs.Add(lang)
                    End If
                Next
                If (langs.Count > 0) Then
                    lvSpokenLangs.DataSource = langs
                    lvSpokenLangs.DataBind()
                    divMyLanguages.Visible = True
                Else
                    divMyLanguages.Visible = False
                End If
            End If

            

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Loading spoken langs")
        End Try

        If (isMyprofileInView) Then
            divMyLanguages.Visible = True
        End If
    End Sub

    Private Sub ShowWantJob(currentRow As EUS_ProfilesRow, profileId As Integer)
        pnlWantJobInfo.Visible = False
        Dim isMyprofileInView As Boolean = (Me.MasterProfileId = profileId)

        Try

            Try
                Dim sql As String = <sql><![CDATA[
select lt.* 
from dbo.EUS_ProfileJobs pj
inner join dbo.EUS_LISTS_Job lt on lt.JobID=pj.JobID
where ProfileID=@Profileid
]]></sql>
                sql = sql.Replace("@Profileid", profileId)
                Dim dt As DataTable = DataHelpers.GetDataTable(sql)
                dt.Columns.Add("Descr", GetType(String))

                For c1 = 0 To dt.Rows.Count - 1
                    Dim dr As DataRow = dt.Rows(c1)
                    If (dt.Columns.Contains(Session("LAGID")) AndAlso dr(Session("LAGID")).ToString().Trim().Length > 0) Then
                        dr("Descr") = dr(Session("LAGID"))
                    Else
                        dr("Descr") = dr("US")
                    End If
                Next
                lvJobs.DataSource = dt
                lvJobs.DataBind()

                If (dt.Rows.Count > 0) Then
                    jobs_wrap.Visible = True
                    pnlWantJobInfo.Visible = True
                Else

                End If
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "Loading want to visit places")
            End Try

            'If (Me.IsFemale) Then
            '    msg_WantJob.Text = CurrentPageData.VerifyCustomString("msg_WantJob_ViewMyProfile_FEMALE")
            '    lblWantJobInfo.Text = CurrentPageData.VerifyCustomString("msg_WantJob_FEMALE.Tooltip")
            'Else
            '    pnlWantJobInfo.Visible = False
            '    msg_WantJob.Text = CurrentPageData.VerifyCustomString("msg_WantJob_ViewMyProfile_MALE")
            '    'lblWantJobInfo.Text = CurrentPageData.VerifyCustomString("msg_WantJob_ViewMyProfile_MALE.Tooltip")
            'End If

            If (isMyprofileInView) Then
                jobs_wrap.Visible = True
                pnlWantJobInfo.Visible = True
            End If

            If (jobs_wrap.Visible) Then

                If (Not isMyprofileInView) Then

                    If Me.UserInView_IsMale Then
                        pnlWantJobInfo.Visible = False
                        msg_WantJob.Text = CurrentPageData.VerifyCustomString("msg_WantJob_ViewOtherProfile_MALE")
                        lnkSendMessageWorkSuggest.Text = CurrentPageData.VerifyCustomString("lnkSendMessageWorkSuggest_MALE")

                    ElseIf Me.UserInView_IsFemale Then
                        msg_WantJob.Text = CurrentPageData.VerifyCustomString("msg_WantJob_ViewOtherProfile_FEMALE")
                        lblWantJobInfo.Text = CurrentPageData.VerifyCustomString("msg_WantJob_FEMALE.Tooltip")
                        lnkSendMessageWorkSuggest.Text = CurrentPageData.VerifyCustomString("lnkSendMessageWorkSuggest_FEMALE")

                    End If
                    msg_WantJob.Text = msg_WantJob.Text.Replace("[LOGIN-NAME]", currentRow.LoginName)
                    lnkSendMessageWorkSuggest.Text = lnkSendMessageWorkSuggest.Text.Replace("[LOGIN-NAME]", currentRow.LoginName)
                    lnkSendMessageWorkSuggest.NavigateUrl = lnkSendMessageWorkSuggest.NavigateUrl.Replace("[LOGIN-NAME]", currentRow.LoginName)

                Else

                    If (Me.IsFemale) Then
                        msg_WantJob.Text = CurrentPageData.VerifyCustomString("msg_WantJob_ViewMyProfile_FEMALE")
                        lblWantJobInfo.Text = CurrentPageData.VerifyCustomString("msg_WantJob_FEMALE.Tooltip")
                        lnkSendMessageWorkSuggest.Text = CurrentPageData.VerifyCustomString("msg_WantJob_ViewMyProfile_FEMALE.Add.Jobs")
                    Else
                        pnlWantJobInfo.Visible = False
                        msg_WantJob.Text = CurrentPageData.VerifyCustomString("msg_WantJob_ViewMyProfile_MALE")
                        lnkSendMessageWorkSuggest.Text = CurrentPageData.VerifyCustomString("msg_WantJob_ViewMyProfile_MALE.Add.Jobs")
                    End If

                    lnkSendMessageWorkSuggest.NavigateUrl = "/Members/Profile.aspx?do=edit&scr=job"
                End If

            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


    Private Sub ShowUserMap(currentRow As EUS_ProfilesRow, profileId As Integer)
        Try
            Dim dt As DataTable = Nothing
            Dim zoom As Integer = 8
            Dim radius As Integer = 10
            Dim lat As Double?
            Dim lng As Double?

            If (Not currentRow.IslatitudeNull()) Then lat = currentRow.latitude
            If (Not currentRow.IslongitudeNull()) Then lng = currentRow.longitude

            If (lat Is Nothing OrElse lng Is Nothing) Then
                Try
                    If (Not currentRow.IsZipNull() AndAlso Not String.IsNullOrEmpty(currentRow.Zip)) Then
                        dt = clsGeoHelper.GetGEOByZip(currentRow.Country, currentRow.Zip, Session("LAGID"))
                    End If

                    If (dt Is Nothing OrElse
                        dt.Rows.Count = 0 OrElse
                        (dt.Rows.Count > 0 AndAlso (IsDBNull(dt.Rows(0)("latitude")) OrElse IsDBNull(dt.Rows(0)("longitude"))))) Then

                        dt = clsGeoHelper.GetCountryMinPostcodeDataTable(currentRow.Country, currentRow._Region, currentRow.City, Session("LAGID"))
                        radius = 120
                    End If

                    If (dt IsNot Nothing AndAlso
                        dt.Rows.Count > 0 AndAlso
                        Not IsDBNull(dt.Rows(0)("latitude")) AndAlso
                        Not IsDBNull(dt.Rows(0)("longitude"))) Then

                        lat = clsNullable.DBNullToDecimal(dt.Rows(0)("latitude"))
                        lng = clsNullable.DBNullToDecimal(dt.Rows(0)("longitude"))
                    End If
                Catch ex As Exception
                    WebErrorSendEmail(ex, "Setting map position, using lat & long from Zip")
                End Try
            Else
                zoom = 12
            End If


            If (lat IsNot Nothing AndAlso lng IsNot Nothing) Then
                ShowUserMap(lat, lng, radius, zoom)
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub
    Private Sub ShowHappyBirthday(currentRow As EUS_ProfilesRow)
        Dim dt As DateTime = AppUtils.GetDateTimeWithUserOffset(DateTime.Now)
        Dim dt1 As DateTime = AppUtils.GetDateTimeWithUserOffset(currentRow.Birthday)
        If dt.Month = dt1.Month AndAlso dt.Day = dt1.Day Then
            pnlHappyBirthday.Visible = True
            Dim tm As String = CurrentPageData.VerifyCustomString("lblHappyBirthday")
            tm = tm.Replace("###LoginName###", currentRow.LoginName).Replace("###Gender###", If(currentRow.GenderId = 1, "ο", "η"))
            lblHappyBirthday.Text = tm
        Else
            pnlHappyBirthday.Visible = False
        End If
    End Sub

    Private Sub ShowTypeOfDate(currentRow As EUS_ProfilesRow, profileId As Integer)
        Try

            Using _dt As New DataTable


                _dt.Columns.Add("TypeOfDateText")

                Dim i As Integer = 0
                For i = 0 To (Lists.gDSLists.EUS_LISTS_TypeOfDating.Rows.Count - 1)

                    'While i < Lists.gDSLists.EUS_LISTS_TypeOfDating.Rows.Count
                    Try
                        Dim txt As String = Lists.gDSLists.EUS_LISTS_TypeOfDating.Rows(i)(GetLag()).ToString()
                        If (String.IsNullOrEmpty(txt)) Then txt = Lists.gDSLists.EUS_LISTS_TypeOfDating.Rows(i)("US")

                        Select Case Lists.gDSLists.EUS_LISTS_TypeOfDating.Rows(i)("TypeOfDatingId")
                            Case TypeOfDatingEnum.AdultDating_Casual
                                If (Not currentRow.IsLookingFor_TypeOfDating_AdultDating_CasualNull() AndAlso currentRow.LookingFor_TypeOfDating_AdultDating_Casual = True) Then
                                    Dim dr As DataRow = _dt.NewRow()
                                    dr("TypeOfDateText") = txt
                                    _dt.Rows.Add(dr)
                                End If

                            Case TypeOfDatingEnum.Friendship
                                If (Not currentRow.IsLookingFor_TypeOfDating_FriendshipNull() AndAlso currentRow.LookingFor_TypeOfDating_Friendship = True) Then
                                    Dim dr As DataRow = _dt.NewRow()
                                    dr("TypeOfDateText") = txt
                                    _dt.Rows.Add(dr)
                                End If

                            Case TypeOfDatingEnum.LongTermRelationship
                                If (Not currentRow.IsLookingFor_TypeOfDating_LongTermRelationshipNull() AndAlso currentRow.LookingFor_TypeOfDating_LongTermRelationship = True) Then
                                    Dim dr As DataRow = _dt.NewRow()
                                    dr("TypeOfDateText") = txt
                                    _dt.Rows.Add(dr)
                                End If

                            Case TypeOfDatingEnum.MarriedDating
                                If (Not currentRow.IsLookingFor_TypeOfDating_MarriedDatingNull() AndAlso currentRow.LookingFor_TypeOfDating_MarriedDating = True) Then
                                    Dim dr As DataRow = _dt.NewRow()
                                    dr("TypeOfDateText") = txt
                                    _dt.Rows.Add(dr)
                                End If

                            Case TypeOfDatingEnum.MutuallyBeneficialArrangements
                                If (Not currentRow.IsLookingFor_TypeOfDating_MutuallyBeneficialArrangementsNull() AndAlso currentRow.LookingFor_TypeOfDating_MutuallyBeneficialArrangements = True) Then
                                    Dim dr As DataRow = _dt.NewRow()
                                    dr("TypeOfDateText") = txt
                                    _dt.Rows.Add(dr)
                                End If

                            Case TypeOfDatingEnum.ShortTermRelationship
                                If (Not currentRow.IsLookingFor_TypeOfDating_ShortTermRelationshipNull() AndAlso currentRow.LookingFor_TypeOfDating_ShortTermRelationship = True) Then
                                    Dim dr As DataRow = _dt.NewRow()
                                    dr("TypeOfDateText") = txt
                                    _dt.Rows.Add(dr)
                                End If

                        End Select

                    Catch ex As Exception
                        WebErrorMessageBox(Me, ex, "")
                    End Try

                    '    i = i + 1
                    'End While
                Next

                lvTypeOfDate.DataSource = _dt
                lvTypeOfDate.DataBind()
            End Using
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        'If (lvTypeOfDate.Items.Count = 0) Then
        '    msg_InterestedIn.Visible = False
        'Else
        '    msg_InterestedIn.Visible = True
        'End If

    End Sub

    Private Sub SetPhotoLevelText(LoginName As String)
        Dim photoLevelSet As Integer = 0
        Dim phtLvl As EUS_ProfilePhotosLevel = GetEUS_ProfilePhotosLevel()
        If (phtLvl IsNot Nothing AndAlso phtLvl.PhotoLevelID > 0) Then
            photoLevelSet = phtLvl.PhotoLevelID
        End If

        If (photoLevelSet > 0) Then
            If Me.UserInView_IsMale Then
                lblPhotoLevel.Text = CurrentPageData.VerifyCustomString("MALE_lblPhotoLevelTitle.Private")
            Else
                lblPhotoLevel.Text = CurrentPageData.VerifyCustomString("FEMALE_lblPhotoLevelTitle.Private")
            End If
        Else
            If Me.UserInView_IsMale Then
                lblPhotoLevel.Text = CurrentPageData.VerifyCustomString("MALE_lblPhotoLevelTitle.Public")
            Else
                lblPhotoLevel.Text = CurrentPageData.VerifyCustomString("FEMALE_lblPhotoLevelTitle.Public")
            End If
        End If
        If (lblPhotoLevel.Text.Trim() = "") Then
            If Me.UserInView_IsMale Then
                lblPhotoLevel.Text = CurrentPageData.VerifyCustomString("MALE_lblPhotoLevelTitle_ND")
            Else
                lblPhotoLevel.Text = CurrentPageData.VerifyCustomString("FEMALE_lblPhotoLevelTitle_ND")
            End If
        End If

        lblPhotoLevel.Text = lblPhotoLevel.Text.
            Replace("[LEVEL]", photoLevelSet).
            Replace("###LOGINNAME###", LoginName).
            Replace(" ", "&nbsp;")

    End Sub


End Class

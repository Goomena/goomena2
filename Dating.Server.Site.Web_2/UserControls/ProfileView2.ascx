﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ProfileView.ascx.vb" Inherits="Dating.Server.Site.Web.ProfileView" %>
<style type="text/css">
    #profile_info2 .descr-text p.places {
        width: 404px;
        padding: 0 0 5px 0;
        margin: 0;
    }
</style>
<script type="text/javascript" src="/v1/Scripts/profile-view.js?v2"></script>

<script type="text/javascript">
    // <![CDATA[
    loading_htm_path = '<%= ResolveUrl("~/loading.htm") %>';
    profileViewLoad("<%= imgVip.ClientID %>", "<%= Dating.Server.Site.Web.AppUtils.JS_PrepareString(Me.VIPTooltip) %>");
    setLevelPopUp_ClientID = '<%= setLevelPopUp.ClientID %>';
    lblPhotoLevel_ClientID = '<%= lblPhotoLevel.ClientID%>';
    // ]]> 
</script>

<%--<div class="lfloat btn-wrap" ID="divWinkF" runat="server">
        <a id="lnkWink" class="btn lnkWink lfloat"  href="javascript:void(0);"  onclick='<%= "ReplaceElementLik(""divWinkF"",""" & me.OtherId & """); LikeProfile(""" & me.OtherId & """,ReplaceElementUnLik,""divUnWinkF"",""" & me.OtherId & """,OnNoPhoto);return false;"%>'   visible='<%# Eval("AllowWink") %>'><%= Me.lnkWink%></a>
       <div class="line-vert3"></div>
</div>
<div class="lfloat btn-wrap" ID="divUnWinkF" runat="server">
      <div id="lnkUnWink" class="btn lnkUnWink disabled lfloat" visible='<%# Eval("AllowUnWink") %>'> <%= Me.lnkWink%> </div> 
        <div class="line-vert3"></div>
</div>--%>
<script type="text/javascript">
    var liketext = '<%= Me.lnkWink%>';
    var Favoritetext = '<%= Me.lnkFavorite%>';
    var UnFavoritetext = '<%= Me.lnkUnFavorite%>';
    function ReplaceElementLik(id, Newid) {
        var str = '<div class="lfloat btn-wrap" ID="<%=divUnWinkF.ClientID%>"><div  class="btn lnkUnWink disabled lfloat">' + liketext + '</div> <div class="line-vert3"></div></div>';
        var Obj = document.getElementById(id); //any element to be fully replaced
        if (Obj.outerHTML) { //if outerHTML is supported
            Obj.outerHTML = str;
        } else { //if outerHTML is not supported, there is a weird but crossbrowsered trick
            var tmpObj = document.createElement("div");
            tmpObj.innerHTML = '<!--REPLACE-->';
            ObjParent = Obj.parentNode; //Okey, element should be parented
            ObjParent.replaceChild(tmpObj, Obj); //here we placing our temporary data instead of our target, so we can find it then and replace it into whatever we want to replace to
            ObjParent.innerHTML = ObjParent.innerHTML.replace('<div><!--REPLACE--></div>', str);
        }
    }
    function OnNoPhoto() {
                location.assign('<%= System.Web.VirtualPathUtility.ToAbsolute("~/Members/Photos.aspx") %>');
    }
    function ReplaceElementUnLik(id, Newid) {
        var Stid = "'" + Newid + "'";
        var NewStid = "'<%=divUnWinkF.ClientID%>'";
        var NewStid2 = "'<%=divWinkF.ClientID%>'";
        var str = '<div class="lfloat btn-wrap" ID="<%=divWinkF.ClientID%>"><a  class="btn lnkWink" href="javascript:void(0);" onclick="ReplaceElementLik(' + NewStid2 + ',' + Stid + ');LikeProfile(' + Stid + ',ReplaceElementUnLik,' + NewStid + ',' + Stid + ',OnNoPhoto);return false;" >' + liketext + '</a><div class="line-vert3"></div></div>';
        var Obj = document.getElementById(id); //any element to be fully replaced
        if (Obj.outerHTML) { //if outerHTML is supported
            Obj.outerHTML = str;
        } else { //if outerHTML is not supported, there is a weird but crossbrowsered trick
            var tmpObj = document.createElement("div");
            tmpObj.innerHTML = '<!--REPLACE-->';
            ObjParent = Obj.parentNode; //Okey, element should be parented
            ObjParent.replaceChild(tmpObj, Obj); //here we placing our temporary data instead of our target, so we can find it then and replace it into whatever we want to replace to
            ObjParent.innerHTML = ObjParent.innerHTML.replace('<div><!--REPLACE--></div>', str);
        }
    }
    <%--<div class="lfloat btn-wrap" ID="divFavoriteF" runat="server">
    <a ID="lnkFavoriteF" class="btn lnkFavorite lfloat" href="javascript:void(0);"  
         onclick='<%= "ReplaceElementFav(""divFavoriteF"",""" & me.OtherId & """); FavoriteProfile(""" & me.OtherId & """,OnNoPhoto);return false;"%>'   
         ><%= Me.lnkFavorite%></a>
    <div class="line-vert3"></div>
</div>
--%>
    function ReplaceElementFav(id, Newid) {
        var Stid = "'" + Newid + "'";
        var NewStid2 = "'<%=divUnFavoriteF.ClientID%>'";
        var str = '<div class="lfloat btn-wrap" ID="<%=divUnFavoriteF.ClientID%>" ><a  class="btn lnkUnFavorite lfloat" href="javascript:void(0);" onclick="ReplaceElementUnFav(' + NewStid2 + ',' + Stid + '); UnFavoriteProfile(' + Stid + ',OnNoPhoto);return false;" >' + UnFavoritetext + '</a><div class="line-vert3"></div></div>';
        var Obj = document.getElementById(id); //any element to be fully replaced
        if (Obj.outerHTML) { //if outerHTML is supported
            Obj.outerHTML = str;
        } else { //if outerHTML is not supported, there is a weird but crossbrowsered trick
            var tmpObj = document.createElement("div");
            tmpObj.innerHTML = '<!--REPLACE-->';
            ObjParent = Obj.parentNode; //Okey, element should be parented
            ObjParent.replaceChild(tmpObj, Obj); //here we placing our temporary data instead of our target, so we can find it then and replace it into whatever we want to replace to
            ObjParent.innerHTML = ObjParent.innerHTML.replace('<div><!--REPLACE--></div>', str);
        }
    }
    function ReplaceElementUnFav(id, Newid) {
        var Stid = "'" + Newid + "'";
        var NewStid2 = "'<%=divFavoriteF.ClientID%>'";
        var str = '<div class="lfloat btn-wrap" ID="<%=divFavoriteF.ClientID%>"><a  class="btn lnkFavorite lfloat" href="javascript:void(0);" onclick="ReplaceElementFav(' + NewStid2 + ',' + Stid + '); FavoriteProfile(' + Stid + ',OnNoPhoto);return false;" >' + Favoritetext + '</a><div class="line-vert3"></div></div>';
        var Obj = document.getElementById(id); //any element to be fully replaced
        if (Obj.outerHTML) { //if outerHTML is supported
            Obj.outerHTML = str;
        } else { //if outerHTML is not supported, there is a weird but crossbrowsered trick
            var tmpObj = document.createElement("div");
            tmpObj.innerHTML = '<!--REPLACE-->';
            ObjParent = Obj.parentNode; //Okey, element should be parented
            ObjParent.replaceChild(tmpObj, Obj); //here we placing our temporary data instead of our target, so we can find it then and replace it into whatever we want to replace to
            ObjParent.innerHTML = ObjParent.innerHTML.replace('<div><!--REPLACE--></div>', str);
        }
    }
</script>

<style type="text/css">
    #profile_view .fancybox-popup img.fancybox-popup-close {
        right: -35px !important;
        top: -34px;
    }
</style>
<div id="profile_view" class="<%= MyBase.GetGenderClass() %>">

    <h3 id="h3Dashboard" class="page-title">
        <asp:Literal ID="lblItemsName" runat="server" />
        <asp:LinkButton ID="lnkViewDescription" runat="server" CssClass="more_info2" OnClientClick="OnMoreInfoClick('[contentUrl]', this, '[headerText]');return false;" />
    </h3>
    <div class="clear"></div>

    <div class="left-side-inn-wrap">
        <div class="left-side-inn">
            <div id="profileOwner" runat="server">
                <asp:Literal ID="msg_LastUpdatedDT" runat="server" />&nbsp;<asp:Literal ID="lblLastUpdated" runat="server" />
            </div>
            <div id="pLastLoggedInDT" runat="server">
                <asp:Literal ID="msg_LastLoggedInDT2" runat="server" />&nbsp;<asp:Label ID="lblLastLoginDT" runat="server" Style="white-space: nowrap;" />
            </div>
        </div>
    </div>
    <div id="profile_info">
        <div class="lfloat">
            <div class="login-info">
                <h1 class="login-name lfloat">
                    <asp:Literal ID="lblLogin" runat="server" /></h1>
                <div id="divIsOnline" runat="server" class="online-indicator lfloat"
                    visible="false">
                    <img src="//cdn.goomena.com/Images/spacer10.png" alt="online" title="online" /><asp:Label
                        ID="lblOnline" runat="server" Text="online"></asp:Label>
                    <script type="text/javascript">
                        function animateImg() {
                            var a = $('img', '#<%= divIsOnline.ClientID %>')
        if (a.is(".slide1"))
            a.removeClass("slide1");
        else
            a.addClass("slide1");
    }
    setInterval(animateImg, 500);
                    </script>
                </div>
                <div class="clear"></div>
            </div>
            <div class="user-details">
                <div class="from-info">
                    <asp:Label ID="lblFrom" runat="server" Text=""></asp:Label></div>
                <div class="birth-info">
                    <asp:Literal ID="msg_Age_Top" runat="server" Text="" /><strong><asp:Literal ID="lblBirth" runat="server" Text="" /></strong>&nbsp;<asp:Literal ID="lblBirthDay" runat="server" Text="" /></div>
                <div class="profile-details">
                    <div class="lfloat height_info info-item">
                        <div class="tt_enabled">
                            <asp:Label ID="lblHeight_Num" runat="server" CssClass="height-num" />
                            <img id="imgHeightInfo" alt="" src="//cdn.goomena.com/Images/spacer10.png" />
                        </div>
                        <div class="tooltip tooltip_height_info">
                            <div class="tooltip_text right-arrow">
                                <asp:Literal ID="msg_Height" runat="server" /><asp:Literal ID="lblHeight_Top" runat="server" /></div>
                        </div>
                    </div>
                    <asp:Panel ID="pnlStatusInfo" runat="server" CssClass="lfloat status_info info-item">
                        <img id="imgStatusInfo" alt="" src="//cdn.goomena.com/Images/spacer10.png" class="tt_enabled" />
                        <div class="tooltip tooltip_status_info">
                            <div class="tooltip_text right-arrow">
                                <asp:Label ID="lblRelationshipStatus_Top" runat="server" Text="" /></div>
                        </div>
                    </asp:Panel>
                    <div class="lfloat zodiac_info info-item">
                        <img id="imgZodiac" runat="server" alt="" src="//cdn.goomena.com/Images/spacer10.png" class="tt_enabled" />
                        <div class="tooltip tooltip_zodiac">
                            <div class="tooltip_text right-arrow">
                                <asp:Label ID="lblZwdio_Top" runat="server" Text="" /></div>
                        </div>
                    </div>
                    <div id="pnlWantVisitInfo" runat="server" class="lfloat want-visit info-item">
                        <img id="imgWantVisitInfo" runat="server" alt="" src="//cdn.goomena.com/Images/spacer10.png" class="tt_enabled" />
                        <div class="tooltip tooltip_want_visit">
                            <div class="tooltip_text right-arrow">
                                <asp:Label ID="lblWantVisitInfo" runat="server" Text="" /></div>
                        </div>
                    </div>
                    <div id="pnlWantJobInfo" runat="server" class="lfloat want-job info-item">
                        <img id="imgWantJobInfo" runat="server" alt="" src="//cdn.goomena.com/Images/spacer10.png" class="tt_enabled" />
                        <div class="tooltip tooltip_want_job">
                            <div class="tooltip_text right-arrow">
                                <asp:Label ID="lblWantJobInfo" runat="server" Text="" /></div>
                        </div>
                    </div>
                    <div id="pnlBreastInfo" class="lfloat breast info-item">
                        <img id="imgBreastInfo" runat="server" alt="" src="//cdn.goomena.com/Images/spacer10.png" class="tt_enabled" />
                        <div class="tooltip tooltip_breast">
                            <div class="tooltip_text right-arrow">
                                <asp:Label ID="lblBreastInfo" runat="server" Text="" /></div>
                        </div>
                    </div>
                    <div id="pnlHairInfo" class="lfloat hair info-item">
                        <img id="imgHairInfo" runat="server" alt="" src="//cdn.goomena.com/Images/spacer10.png" class="tt_enabled" />
                        <div class="tooltip tooltip_hair">
                            <div class="tooltip_text right-arrow">
                                <asp:Label ID="lblHairInfo" runat="server" Text="" /></div>
                        </div>
                    </div>
                    <div id="pnlDrinkingInfo" class="lfloat drinking info-item">
                        <img id="imgDrinkingInfo" runat="server" alt="" src="//cdn.goomena.com/Images/spacer10.png" class="tt_enabled" />
                        <div class="tooltip tooltip_drinking">
                            <div class="tooltip_text right-arrow">
                                <asp:Label ID="lblDrinkingInfo" runat="server" Text="" /></div>
                        </div>
                    </div>
                    <div id="pnlSmokingInfo" class="lfloat smoking info-item">
                        <img id="imgSmokingInfo" runat="server" alt="" src="//cdn.goomena.com/Images/spacer10.png" class="tt_enabled" />
                        <div class="tooltip tooltip_smoking">
                            <div class="tooltip_text right-arrow">
                                <asp:Label ID="lblSmokingInfo" runat="server" Text="" /></div>
                        </div>
                    </div>
                    <div id="pnlHappyBirthday" runat="server" class="lfloat birthday info-item">
                        <img id="imgBirthday" runat="server" alt="" src="//cdn.goomena.com/Images/spacer10.png" class="tt_enabled" />
                        <div class="tooltip tooltip_Birthday">
                            <div class="tooltip_text right-arrow">
                                <asp:Label ID="lblHappyBirthday" runat="server" Text="" /></div>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="vip-info" id="divVip" runat="server">
                    <asp:Image ID="imgVip" runat="server" ImageUrl="//cdn.goomena.com/images2/mbr2/vip.png" Visible="false" Style="vertical-align: middle" title="" /></div>
            </div>
        </div>
        <div class="rfloat">
            <div class="pr-photo-125-noshad">
                <img id="imgPhotoOverlM" src="//cdn.goomena.com/images2/profile/men-hat-small.png" class="view-male-mask" alt="" />
                <img id="imgPhotoOverlF" src="//cdn.goomena.com/images2/profile/mask-small.png" class="view-female-mask" alt="" />
                <asp:HyperLink ID="lnkPhoto" NavigateUrl="~/Members/Photos.aspx" runat="server" CssClass="profile-picture" onClick="ShowLoading();">
                    <img id="imgPhotoOverlFlowerF" src="//cdn.goomena.com/images2/profile/flower-small.png" class="view-mayday-female" alt="" />
                    <asp:Image ID="imgPhoto" runat="server" ImageUrl="//cdn.goomena.com/Images/guy.jpg" CssClass="round-img-125" />
                </asp:HyperLink>
            </div>
        </div>
        <div class="clear"></div>
    </div>

    <div id="profile_buttons">

        <asp:MultiView ID="mvGender" runat="server" ActiveViewIndex="0">

            <asp:View ID="vwMyProfile" runat="server">
                <div class="my-buttons">
                    <div class="lfloat btn-wrap">
                        <asp:HyperLink ID="lnkEditProf" CssClass="lnkEditProf btn" runat="server" NavigateUrl="~/Members/Profile.aspx?do=edit" onclick="ShowLoading();" Visible="false" />
                    </div>
                    <div class="line-vert1"></div>
                    <div class="lfloat btn-wrap">
                        <asp:HyperLink ID="lnkMyPhoto" CssClass="lnkMyPhoto btn" runat="server" NavigateUrl="~/Members/Photos.aspx" onclick="ShowLoading();" Text="lnkMyPhoto" />
                    </div>
                    <div class="line-vert2" id="divLineVert2" runat="server" visible="false"></div>
                    <div class="lfloat btn-wrap" id="divBilling" runat="server" visible="false">
                        <asp:HyperLink ID="lnkBilling" CssClass="lnkBilling btn" runat="server" NavigateUrl="~/Members/Settings.aspx?vw=CREDITS" onclick="ShowLoading();" Text="lnkMyCredits" />
                    </div>
                    <div class="clear"></div>
                </div>
            </asp:View>


            <asp:View ID="vwFemale" runat="server">
                <div class="female-buttons">
                    <table class="center-table">
                        <tr>
                            <td>
                                <div class="lfloat btn-wrap" id="divWinkF" runat="server">
                                    <a class="btn lnkWink lfloat" href="javascript:void(0);" onclick='<%= "ReplaceElementLik(""" & divWinkF.ClientID & """,""" & Me.OtherId & """); LikeProfile(""" & Me.OtherId & """,ReplaceElementUnLik,""" & divUnWinkF.ClientID & """,""" & Me.OtherId & """,OnNoPhoto);return false;"%>' visible='<%# Eval("AllowWink") %>'><%= Me.lnkWink%></a>
                                    <div class="line-vert3"></div>
                                </div>
                                <div class="lfloat btn-wrap" id="divUnWinkF" runat="server">
                                    <div class="btn lnkUnWink disabled lfloat" visible='<%# Eval("AllowUnWink") %>'><%= Me.lnkWink%> </div>
                                    <div class="line-vert3"></div>
                                </div>
                                <div class="lfloat btn-wrap" id="divMessageF" runat="server">
                                    <asp:HyperLink ID="lnkMessageF" CssClass="btn lnkMessage lfloat" runat="server" NavigateUrl="#">[lnkMessageF]</asp:HyperLink>
                                    <div class="line-vert3"></div>
                                </div>
                                <div class="lfloat btn-wrap" id="divMakeOfferF" runat="server">
                                    <asp:LinkButton ID="lnkMakeOfferF" CssClass="btn lnkMakeOffer lfloat" runat="server" />
                                    <div class="line-vert3"></div>
                                </div>
                                <div class="lfloat btn-wrap" id="divFavoriteF" runat="server">
                                    <a class="btn lnkFavorite lfloat" href="javascript:void(0);"
                                        onclick='<%= "ReplaceElementFav(""" & divFavoriteF.ClientID & """,""" & Me.OtherId & """); FavoriteProfile(""" & Me.OtherId & """,OnNoPhoto);return false;"%>'><%= Me.lnkFavorite%></a>
                                    <div class="line-vert3"></div>
                                </div>
                                <div class="lfloat btn-wrap" id="divUnlockOfferF" runat="server">
                                    <asp:LinkButton ID="lnkUnlockOfferF" CssClass="btn lnkUnlockOffer btn-primary lfloat" runat="server" />
                                    <div class="line-vert3"></div>
                                </div>
                                <div class="lfloat btn-wrap" id="divUnFavoriteF" runat="server">
                                    <a class="btn lnkUnFavorite lfloat" href="javascript:void(0);"
                                        onclick='<%= "ReplaceElementUnFav(""" & divUnFavoriteF.ClientID & """,""" & Me.OtherId & """); UnFavoriteProfile(""" & Me.OtherId & """,OnNoPhoto);return false;"%>'><%= Me.lnkUnFavorite%></a>
                                    <div class="line-vert3"></div>
                                </div>
                                <div class="lfloat btn-wrap" id="divConversationF" runat="server">
                                    <asp:HyperLink ID="lnkConversationF" CssClass="btn lnkConversation lfloat" runat="server" NavigateUrl="#" Visible="false" />
                                    <div class="line-vert3"></div>
                                </div>
                                <div class="clear"></div>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:View>

            <asp:View ID="vwMale" runat="server">
                <div class="male-buttons">
                    <table class="center-table">
                        <tr>
                            <td>
                                <div class="lfloat btn-wrap" id="div1" runat="server">
                                    <a class="btn lnkWink lfloat" href="javascript:void(0);" onclick='<%= "ReplaceElementLik(""divWinkF"",""" & me.OtherId & """); LikeProfile(""" & me.OtherId & """,ReplaceElementUnLik,""divUnWinkF"",""" & me.OtherId & """,OnNoPhoto);return false;"%>' visible='<%# Eval("AllowWink") %>'><%= Me.lnkWink%></a>
                                    <div class="line-vert3"></div>
                                </div>
                                <div class="lfloat btn-wrap" id="div2" runat="server">
                                    <div class="btn lnkUnWink disabled lfloat" visible='<%# Eval("AllowUnWink") %>'><%= Me.lnkWink%> </div>
                                    <div class="line-vert3"></div>
                                </div>

                                <div class="lfloat btn-wrap" id="divMessageM" runat="server">
                                    <asp:HyperLink ID="lnkMessageM" CssClass="btn lnkMessage lfloat" runat="server" NavigateUrl="#" />
                                    <div class="btn-group lfloat hidden" id="divActionsMenu" runat="server" visible="false">
                                        <a class="btn btn-small dropdown-toggle" data-toggle="dropdown" href="#">
                                            <asp:Literal ID="msg_ActionsText" runat="server" />
                                            <span class="caret"></span></a>
                                        <ul class="dropdown-menu">
                                            <li id="liActsSendMsgOnce" runat="server">
                                                <asp:HyperLink ID="lnkSendMsgOnce" runat="server" NavigateUrl="">SendMessageOnceText</asp:HyperLink></li>
                                            <li id="liActsSendMsgMany" runat="server" visible="false">
                                                <asp:HyperLink ID="lnkSendMsgMany" runat="server" NavigateUrl="">SendMessageManyText</asp:HyperLink></li>
                                        </ul>
                                    </div>
                                    <div class="line-vert3"></div>
                                </div>
                                <div class="lfloat btn-wrap" id="divMakeOfferM" runat="server">
                                    <asp:LinkButton ID="lnkMakeOfferM" CssClass="btn lnkMakeOffer lfloat" runat="server" />
                                    <div class="line-vert3"></div>
                                </div>
                              
                                <div class="lfloat btn-wrap" id="divUnlockOfferM" runat="server">
                                    <asp:LinkButton ID="lnkUnlockOfferM" CssClass="btn lnkUnlockOffer btn-primary lfloat" runat="server" />
                                    <div class="line-vert3"></div>
                                </div>
                                <div class="lfloat btn-wrap" id="div4" runat="server">
                                    <a class="btn lnkUnFavorite lfloat" href="javascript:void(0);"
                                        onclick='<%= "ReplaceElementUnFav(""divUnFavoriteF"",""" & me.OtherId & """); UnFavoriteProfile(""" & me.OtherId & """,OnNoPhoto);return false;"%>'><%= Me.lnkUnFavorite%></a>
                                    <div class="line-vert3"></div>
                                </div>
                                <div class="lfloat btn-wrap" id="divConversationM" runat="server">
                                    <asp:HyperLink ID="lnkConversationM" CssClass="btn lnkConversation lfloat" runat="server" NavigateUrl="#" Visible="false" />
                                    <div class="line-vert3"></div>
                                </div>
                                <div class="clear"></div>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:View>
        </asp:MultiView>
    </div>

    <div class="profile-photos-levels" id="pPhotosLevel" runat="server">
        <div class="inner-box">
            <dx:ASPxCallbackPanel ID="cbpnlPhotosLevelText" runat="server" ShowLoadingPanel="False"
                ShowLoadingPanelImage="False" ClientInstanceName="cbpnlPhotosLevelText">
                <ClientSideEvents EndCallback="cbpnlPhotosLevelText_EndCallback" CallbackError="function(s, e) {
	HideLoading();
}"></ClientSideEvents>
                <PanelCollection>
                    <dx:PanelContent ID="pnlCont2" runat="server" SupportsDisabledAttribute="True">
                        <asp:HyperLink ID="lblPhotoLevel" runat="server" Text="Επίπεδο φωτογραφίας" CssClass="lblPhotoLevel"
                            NavigateUrl="javascript: void(0);" />
                    </dx:PanelContent>
                </PanelCollection>
            </dx:ASPxCallbackPanel>
        </div>
    </div>

    <div class="photos-list">
        <asp:ListView ID="lvPhotos" runat="server">
            <LayoutTemplate>
                <div class="profile_gallery">
                    <div runat="server" id="itemPlaceholder"></div>
                    <div id="divAddNew" runat="server">
                        <asp:HyperLink ID="lnkFancy" runat="server" class="btn d_big_photo new-photo" NavigateUrl="~/Members/Photos.aspx" onclick="ShowLoading();">
                            <img src="//cdn.goomena.com/Images/spacer10.png" />
                            <asp:Label ID="lblPhotoNew" runat="server"></asp:Label>
                        </asp:HyperLink>
                    </div>
                    <div class="clear"></div>
                </div>
            </LayoutTemplate>
            <ItemTemplate>
                <div class="d_big_photo">
                    <asp:HyperLink ID="lnkFancy0" runat="server" rel="prof_group" CssClass="js fancybox" NavigateUrl='<%# Eval("ImageUrl") %>' onclick='<%# Eval("ImageUrlOnClick") %>' Visible='<%# Eval("ImageUrlOnClick").toString().length=0 %>'>
                        <img src="<%# Eval("ImageThumbUrl") %>" />
                        <asp:Label ID="lblLevel" runat="server" Text='<%# Eval("PhotoLevelText") %>' Visible='<%# LEN(Eval("PhotoLevelText")) > 0 %>'></asp:Label>
                        <asp:Label ID="lblPhotoInf0" runat="server" Text='<%# Eval("DisplayLevelText") %>' Visible='<%# LEN(Eval("DisplayLevelText")) > 0 %>' CssClass="photo-text"></asp:Label>
                    </asp:HyperLink>
                    <asp:HyperLink ID="lnkFancy1" runat="server" NavigateUrl='<%# Eval("ImageUrl") %>' onclick='<%# Eval("ImageUrlOnClick") %>' Visible='<%# Eval("ImageUrlOnClick").toString().length>0 %>'>
                        <img src="<%# Eval("ImageThumbUrl") %>" class="private-photo" />
                        <asp:Label ID="lblLevel1" runat="server" Text='<%# Eval("PhotoLevelText") %>' Visible='<%# LEN(Eval("PhotoLevelText")) > 0 %>'></asp:Label>
                        <asp:Label ID="lblPhotoInf1" runat="server" Text='<%# Eval("DisplayLevelText") %>' Visible='<%# LEN(Eval("DisplayLevelText")) > 0 %>' CssClass="photo-text"></asp:Label>
                    </asp:HyperLink>
                </div>
            </ItemTemplate>
            <EmptyDataTemplate>
                <div class="no-photos" style="font-size: 15px; padding: 20px; text-align: center;">
                    <asp:Label ID="lblNoPhotos" runat="server" />
                </div>
            </EmptyDataTemplate>
        </asp:ListView>
    </div>

    <div id="profile_info2">
        <div class="info-inner">
            <asp:HyperLink ID="lnkEditProf1" CssClass="profile-edit-img btn" runat="server" NavigateUrl="~/Members/Profile.aspx?do=edit&scr=other" onclick="ShowLoading();" Visible="false" />
            <table style="width: 100%;">
                <tr>
                    <td valign="top">
                        <ul>
                            <li>
                                <asp:Label ID="lblAboutMeHeading" runat="server" CssClass="heading" /></li>
                            <li><span class="heading">
                                <asp:Literal ID="msg_Age" runat="server" />:</span>&nbsp;<asp:Literal ID="lblAge" runat="server" />
                                <asp:Literal ID="lblZwdio" runat="server" Text="" /></li>
                            <li><span class="heading">
                                <asp:Literal ID="msg_BodyType" runat="server" />:</span>&nbsp;<asp:Literal ID="lblHeight" runat="server" />
                                -
                                <asp:Literal ID="lblBodyType" runat="server" /></li>
                            <li><span class="heading">
                                <asp:Literal ID="msg_HairEyes" runat="server" />:</span>&nbsp;<asp:Literal ID="lblHairClr" runat="server" />
                                /
                                <asp:Literal ID="lblEyeClr" runat="server" /></li>
                            <li><span class="heading">
                                <asp:Literal ID="msg_Education" runat="server" />:</span>&nbsp;<asp:Literal ID="lblEducation" runat="server" /></li>
                            <li><span class="heading">
                                <asp:Literal ID="msg_Children" runat="server" />:</span>&nbsp;<asp:Literal ID="lblChildren" runat="server" /></li>
                            <li><span class="heading">
                                <asp:Literal ID="msg_Ethnicity" runat="server" />:</span>&nbsp;<asp:Literal ID="lblEthnicity" runat="server" /></li>
                            <li><span class="heading">
                                <asp:Literal ID="msg_Religion" runat="server" />:</span>&nbsp;<asp:Literal ID="lblReligion" runat="server" /></li>
                            <li><span class="heading">
                                <asp:Literal ID="msg_SmokingDrinking" runat="server" />:</span>&nbsp;<asp:Literal ID="lblSmoking" runat="server" />
                                /
                                <asp:Literal ID="lblDrinking" runat="server" /></li>
                        </ul>
                    </td>
                    <td valign="top" style="width: 10px;">
                        <div style="width: 10px;">&nbsp;</div>
                    </td>
                    <td valign="top">
                        <ul>
                            <li><span class="heading">
                                <asp:Literal ID="msg_Occupation" runat="server" />:</span>&nbsp;<asp:Literal ID="lblOccupation" runat="server" /></li>
                            <li runat="server" id="liAnnualIncome"><span class="heading">
                                <asp:Literal ID="msg_IncomeLevel" runat="server" />:</span>&nbsp;<asp:Literal ID="lblIncomeLevel" runat="server" /></li>
                            <li runat="server" id="liNetWorth" visible="false"><span class="heading">
                                <asp:Literal ID="msg_NetWorth" runat="server" />:</span>&nbsp;<asp:Literal ID="lblNetWorth" runat="server" /></li>
                            <li><span class="heading">
                                <asp:Literal ID="msg_RelationshipStatus" runat="server" />:</span>&nbsp;<asp:Literal ID="lblRelationshipStatus" runat="server" /></li>
                            <li><span class="heading">
                                <asp:Literal ID="msg_LookingToMeet" runat="server" />:</span>&nbsp;<asp:Literal ID="lblLookingToMeet" runat="server" /></li>
                            <li><span class="heading">
                                <asp:Literal ID="msg_InterestedIn" runat="server" />:</span>
                                <asp:ListView ID="lvTypeOfDate" runat="server">
                                    <LayoutTemplate>
                                        <ul>
                                            <li runat="server" id="itemPlaceholder"></li>
                                        </ul>
                                    </LayoutTemplate>
                                    <ItemTemplate>
                                        <li><%# Eval("TypeOfDateText") %></li>
                                    </ItemTemplate>
                                </asp:ListView>
                            </li>
                        </ul>
                        <p class="p_report" id="pReport" runat="server">
                            <asp:HyperLink ID="lnkReport" runat="server">Report Abuse</asp:HyperLink><asp:Literal
                                ID="ltrSeparator" runat="server">&nbsp;/&nbsp;</asp:Literal><asp:LinkButton
                                    ID="lnkBlock" runat="server">Block</asp:LinkButton>
                        </p>
                    </td>
                </tr>
            </table>
        </div>

        <div id="want_to_visit_wrap" runat="server" clientidmode="Static" visible="false" class="list-box-wrap">
            <div class="lfloat list-box-icon">&nbsp;</div>
            <div class="lfloat list-box-descr">
                <asp:Literal ID="msg_WantToVisitTitle" runat="server" />
                <asp:HyperLink ID="lnkEditProf5" CssClass="profile-edit-img btn" runat="server" NavigateUrl="~/Members/Profile.aspx?do=edit&scr=travel" onclick="ShowLoading();" Visible="false" />
            </div>
            <div class="clear"></div>

            <asp:ListView ID="lvWantVisit" runat="server">
                <LayoutTemplate>
                    <div class="list-box-items">
                        <div runat="server" id="itemPlaceholder"></div>
                        <div class="clear"></div>
                    </div>
                </LayoutTemplate>
                <ItemTemplate>
                    <div id="flag_<%# if(Eval("Loc_country_short") isnot nothing, Eval("Loc_country_short").ToLower(), "empty") %>_21" class="item"><span><%# Eval("Descr")%></span></div>
                </ItemTemplate>
            </asp:ListView>

            <div class="bottom-box" id="travel_send_message" runat="server" clientidmode="Static">
                <asp:HyperLink ID="lnkSendMessageTravel" runat="server" Text="" CssClass="center-link" NavigateUrl="/Members/Conversation.aspx?send=once&p=[LOGIN-NAME]&text=traveltogether" />
            </div>
        </div>


        <div id="jobs_wrap" runat="server" clientidmode="Static" visible="false" class="list-box-wrap">
            <div class="lfloat list-box-icon">&nbsp;</div>
            <div class="lfloat list-box-descr">
                <asp:Literal ID="msg_WantJob" runat="server" />
                <asp:HyperLink ID="lnkEditProf6" CssClass="profile-edit-img btn" runat="server" NavigateUrl="~/Members/Profile.aspx?do=edit&scr=job" onclick="ShowLoading();" Visible="false" />
            </div>
            <div class="clear"></div>

            <asp:ListView ID="lvJobs" runat="server">
                <LayoutTemplate>
                    <div class="list-box-items">
                        <div runat="server" id="itemPlaceholder"></div>
                        <div class="clear"></div>
                    </div>
                </LayoutTemplate>
                <ItemTemplate>
                    <div class="item"><span><%# Eval("Descr")%></span></div>
                </ItemTemplate>
            </asp:ListView>

            <div class="bottom-box" id="jobs_send_message" runat="server" clientidmode="Static">
                <asp:HyperLink ID="lnkSendMessageWorkSuggest" runat="server" Text="" CssClass="center-link" NavigateUrl="/Members/Conversation.aspx?send=once&p=[LOGIN-NAME]&text=worksuggest" />
            </div>
        </div>


        <h2 class="descr-title">
            <asp:Literal ID="msg_AboutMeDescrTitle" runat="server" />
            <asp:HyperLink ID="lnkEditProf2" CssClass="profile-edit-img btn" runat="server" NavigateUrl="~/Members/Profile.aspx?do=edit&scr=aboutdesc" onclick="ShowLoading();" Visible="false" />
            <div class="btm-arrow">&nbsp;</div>
        </h2>
        <p class="descr-text">
            <asp:Literal ID="lblAboutMeDescr" runat="server" /></p>

        <h2 class="descr-title">
            <asp:Literal ID="msg_FirstDateExpectationDescrTitle" runat="server" />
            <asp:HyperLink ID="lnkEditProf3" CssClass="profile-edit-img btn" runat="server" NavigateUrl="~/Members/Profile.aspx?do=edit&scr=aboutexpect" onclick="ShowLoading();" Visible="false" />
            <div class="btm-arrow">&nbsp;</div>
        </h2>
        <p class="descr-text">
            <asp:Literal ID="lblFirstDateExpectationDescr" runat="server" /></p>

        <div id="divMyLanguages" runat="server" visible="false">
            <h2 class="descr-title">
                <asp:Literal ID="msg_MyLanguagesTitle" runat="server" />
                <asp:HyperLink ID="lnkEditProf4" CssClass="profile-edit-img btn" runat="server" NavigateUrl="~/Members/Profile.aspx?do=edit&scr=langs" onclick="ShowLoading();" Visible="false" />
                <div class="btm-arrow">&nbsp;</div>
            </h2>
            <p class="descr-text">
                <asp:ListView ID="lvSpokenLangs" runat="server" EnableViewState="false">
                    <ItemTemplate>
                        <span class="spoken-lang"><%# Container.DataItem %></span><%--Eval("LangName")--%>
                    </ItemTemplate>
                </asp:ListView>
            </p>
        </div>


        <div class="module mod-line deepest" style="min-height: 254px;" id="divMapContainer" runat="server" visible="false">
            <div id="MapAreaScroll">
                <h3 class="module-title">
                    <asp:Label ID="msg_MemberPosition" runat="server" Text="" /><%--CssClass="member-position" style="display:inline;" --%>
                </h3>
                <div id="MapArea" style="display: none; opacity: 1;">

                    <script type="text/javascript">
                        loadUserMap("<%= ifrUserMap_src %>", '#<%= msg_MemberPosition.ClientID %>');
                    </script>



                    <%--<script type="text/javascript">
    function runExtLinks_PV() {
        var mapIframe = '<iframe id="ifrUserMap" style="width:693px; height:250px;" src="<%= ifrUserMap_src %>"></iframe>'
        $('#<%= divMapContainer.ClientID %>').append(mapIframe);
        $('#<%= msg_MemberPosition.ClientID %>').show();
    }
</script>
<!--[if gte IE 6]>
<script type="text/javascript">
    function runExtLinks_PV_Helper(){
        runExtLinks_PV();
    }
</script>
<![endif]>
<script type="text/javascript">
    $(function () {
        if (typeof runExtLinks_PV_Helper === 'function') {
            runExtLinks_PV_Helper()
        }
        else {
            setTimeout(runExtLinks_PV, 1500);
        }
    });
</script>--%>
                </div>
            </div>
        </div>
    </div>

    <%--<div class="p_wrap hidden">
        <table cellpadding="0" cellspacing="0" width="100%" style="background:#f9f9f9;">
        <tr>
            <td>
                <div class="p_actions" <%= WriteActionTopRowStyle() %>>
                    <div class="">
                    
                    <div class="clear"></div>
                    </div>

	            </div>
                <div class="<%= WriteUserInsightClass() %> hidden">
			        <h3 style="font-size:12px;"><asp:Literal ID="msg_UserInsightTitle" runat="server" /></h3>
			        <p id="pLastLoggedInDT" runat="server" ><asp:Literal ID="msg_LastLoggedInDT" runat="server" /> <asp:Label ID="lblLastLoginDT" runat="server" style="white-space:nowrap;" /></p>							
                
		        </div>
		        <div class="clear"></div>
            </td>
        </tr>
        
          </table>
  
	
	    	    <div class="clear"></div>
    </div>--%>

    <dx:ASPxPopupControl ID="UploadPhotoPopUp" runat="server"
        ClientInstanceName="UploadPhotoPopUp"
        AllowResize="True"
        PopupHorizontalAlign="WindowCenter"
        PopupVerticalAlign="WindowCenter"
        ShowSizeGrip="True"
        CloseAction="CloseButton"
        AllowDragging="True"
        Width="600px"
        HeaderText="Photo upload notification"
        Modal="True"
        ShowShadow="False">
        <CloseButtonImage Url="//cdn.goomena.com/Images/spacer10.png" Height="43px" Width="43px">
        </CloseButtonImage>
        <SizeGripImage Height="40px" Url="//cdn.goomena.com/Images/resize.png" Width="40px">
        </SizeGripImage>
        <CloseButtonStyle BackColor="White">
            <HoverStyle>
                <BackgroundImage ImageUrl="//cdn.goomena.com/Images2/popup-wrn/close.png" Repeat="NoRepeat" HorizontalPosition="center"
                    VerticalPosition="center"></BackgroundImage>
            </HoverStyle>
            <BackgroundImage ImageUrl="//cdn.goomena.com/Images2/popup-wrn/close.png" Repeat="NoRepeat" HorizontalPosition="center"
                VerticalPosition="center"></BackgroundImage>
            <BorderLeft BorderColor="White" BorderStyle="Solid" BorderWidth="10px" />
        </CloseButtonStyle>
        <ContentStyle HorizontalAlign="Left" VerticalAlign="Middle">
            <Paddings Padding="0px" PaddingTop="25px" />
            <Paddings Padding="0px" PaddingTop="25px"></Paddings>
        </ContentStyle>
        <HeaderStyle>
            <Paddings Padding="0px" PaddingLeft="0px" />
            <Paddings PaddingTop="0px" PaddingBottom="0px"></Paddings>
            <BorderTop BorderColor="White" BorderStyle="Solid" BorderWidth="5px" />
            <BorderRight BorderColor="White" BorderStyle="Solid" BorderWidth="5px" />
            <BorderBottom BorderColor="White" BorderStyle="Solid" BorderWidth="5px" />
        </HeaderStyle>
        <ModalBackgroundStyle BackColor="Black" Opacity="70"></ModalBackgroundStyle>
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server" SupportsDisabledAttribute="True"
                HeaderText="Photo upload notification" Modal="True"
                Name="winAutoNotificationsSettings">
                <div class="items_none2" style="margin: 10px;">
                    <div style="padding: 20px 0 0; margin-bottom: 40px; text-align: left;">
                        <asp:Literal ID="msg_HasNoPhotosText" runat="server" />
                    </div>
                    <asp:HyperLink ID="lnkUploadPhoto" CssClass="btn btn-danger" ForeColor="#ffffff" runat="server" NavigateUrl="~/Members/Photos.aspx" onclick="closePopup('UploadPhotoPopUp');ShowLoading();">Upload a Photo</asp:HyperLink>
                </div>
                <div style="margin: 0 10px 5px 0">
                    <table style="width: 100%;">
                        <tr>
                            <td align="right"><a href="javascript:closePopup('UploadPhotoPopUp');">Close</a></td>
                        </tr>
                    </table>
                </div>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>

    <dx:ASPxCallbackPanel ID="cbpnlPhotos" runat="server" ShowLoadingPanel="False"
        ShowLoadingPanelImage="False" ClientInstanceName="cbpnlPhotos">
        <ClientSideEvents EndCallback="cbpnlPhotos_EndCallback" CallbackError="function(s, e) {
	HideLoading();
    setPopupWindowUI();
}"></ClientSideEvents>
        <PanelCollection>
            <dx:PanelContent ID="pnlCont1" runat="server" SupportsDisabledAttribute="True">
                <dx:ASPxPopupControl runat="server"
                    ID="setLevelPopUp"
                    ClientInstanceName="setLevelPopUp"
                    ClientIDMode="AutoID"
                    PopupVerticalAlign="WindowCenter"
                    PopupHorizontalAlign="WindowCenter"
                    AllowDragging="false"
                    AllowResize="True"
                    ShowSizeGrip="True"
                    Modal="false"
                    CloseAction="OuterMouseClick"
                    ShowShadow="False">
                    <Windows>
                        <dx:PopupWindow Modal="false" Name="setLevelPopUpWin"
                            Height="150px" Width="400px" PopupElementID="lblPhotoLevel">
                            <ContentStyle HorizontalAlign="Left" VerticalAlign="Middle">
                                <Paddings Padding="0px" PaddingTop="25px" />
                                <Paddings Padding="0px" PaddingTop="25px"></Paddings>
                            </ContentStyle>
                            <ContentCollection>
                                <dx:PopupControlContentControl ID="PopupControlContentControl3" runat="server" SupportsDisabledAttribute="True">
                                    <asp:Label ID="lblAllowAccessPhotosLevel" runat="server" Text="" /><br />
                                    <br />

                                    <div align="center" class=" mediumCombo" style="margin: 5px auto;">
                                        <dx:ASPxRadioButtonList ID="cbPhotosLevel" runat="server" CssClass="levels-radio-list">
                                            <RadioButtonStyle CssClass="levels-radio-list-item">
                                            </RadioButtonStyle>
                                            <Border BorderWidth="0px" BorderStyle="None"></Border>
                                        </dx:ASPxRadioButtonList>

                                    </div>
                                    <div align="center" style="margin: 0 auto;">
                                        <dx:ASPxButton ID="btnSave" runat="server" Text="Save" EncodeHtml="False" CssClass="button-blue-new"
                                            Native="True">
                                            <ClientSideEvents Click="function(s, e) {
    ShowLoading();  
    closePopup('setLevelPopUp');
    OnPhotosDisplayLevelChanged(s,e); 
    e.processOnServer=false;
}"></ClientSideEvents>
                                        </dx:ASPxButton>

                                        <asp:Label ID="lblAllowAccessPhotosLevelBottom" runat="server" Text="" />
                                    </div>
                                </dx:PopupControlContentControl>
                            </ContentCollection>
                        </dx:PopupWindow>
                    </Windows>
                    <SizeGripImage Height="40px" Url="//cdn.goomena.com/Images/resize.png" Width="40px">
                    </SizeGripImage>
                    <CloseButtonStyle BackColor="White">
                        <HoverStyle>
                            <BackgroundImage ImageUrl="//cdn.goomena.com/Images2/popup-wrn/close.png" Repeat="NoRepeat" HorizontalPosition="center"
                                VerticalPosition="center"></BackgroundImage>
                        </HoverStyle>
                        <BackgroundImage ImageUrl="//cdn.goomena.com/Images2/popup-wrn/close.png" Repeat="NoRepeat" HorizontalPosition="center"
                            VerticalPosition="center"></BackgroundImage>
                        <BorderLeft BorderColor="White" BorderStyle="Solid" BorderWidth="10px" />
                    </CloseButtonStyle>
                    <CloseButtonImage Url="//cdn.goomena.com/Images/spacer10.png" Height="43px" Width="43px">
                    </CloseButtonImage>
                    <ContentStyle HorizontalAlign="Left" VerticalAlign="Middle">
                        <Paddings Padding="0px" PaddingTop="25px" />
                        <Paddings Padding="0px" PaddingTop="25px"></Paddings>
                    </ContentStyle>
                    <HeaderStyle>
                        <Paddings Padding="0px" PaddingLeft="0px" />
                        <Paddings PaddingTop="0px" PaddingBottom="0px"></Paddings>
                        <BorderTop BorderColor="White" BorderStyle="Solid" BorderWidth="5px" />
                        <BorderRight BorderColor="White" BorderStyle="Solid" BorderWidth="5px" />
                        <BorderBottom BorderColor="White" BorderStyle="Solid" BorderWidth="5px" />
                    </HeaderStyle>
                    <ModalBackgroundStyle CssClass="modalPopup">
                    </ModalBackgroundStyle>
                    <ContentCollection>
                        <dx:PopupControlContentControl ID="PopupControlContentControl4" runat="server" SupportsDisabledAttribute="True">
                        </dx:PopupControlContentControl>
                    </ContentCollection>
                </dx:ASPxPopupControl>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>




    <script type="text/javascript">

        setPopupWindowUI();
        $(function () {
            checkProfileButtons();
            setLevelPopUp.SetWindowPopupElementID(setLevelPopUp.GetWindow(0), '<%= lblPhotoLevel.ClientID %>');
    })
    </script>

</div>

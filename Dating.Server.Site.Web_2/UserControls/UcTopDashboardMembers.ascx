﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UcTopDashboardMembers.ascx.vb"
    Inherits="Dating.Server.Site.Web.UcTopDashboardMembers" %>
<script type="text/javascript">
    function ActivateThisTab(sender) {
        if ($(sender).hasClass("active") == false) {
            var terms = sender.hash.replace('/', '');
            $(".TabBoxControl .TabLink").removeClass("active");
            $(sender).addClass("active");
            $("#QuickMembersShow").find('.TabContent').hide();
            $(terms).fadeIn(200);
        }
    }

    function AddHovered(sender) {
        if ($(sender).parent().hasClass("hovered") == false) {
            $(sender).parent().addClass("hovered");
        }
    }
    function RemoveHovered(sender) {

        $(sender).parent().removeClass("hovered");

    }
    function AddHovered2(sender) {
        var itm = $(sender).parent().parent();
        var itm2 = itm.find('.pic-round-img-123')
        if (itm2.hasClass("hovered") == false) {
            itm2.addClass("hovered");
        }
    }
    function RemoveHovered2(sender) {
        var itm = $(sender).parent().parent();
        var itm2 = itm.find('.pic-round-img-123')
     itm2.removeClass("hovered");

    }
</script>

<div class="TabBoxControl">
    <div class="wrap" runat="server" visible="true">
        <ul class="tabs group">
            <li class="NewMembers">
                <a class="TabLink NewMembersContainer active" href="#/NewMembersContainer" onclick="ActivateThisTab(this)">
                    <asp:Label ID="lblNewMembers" runat="server" Text="New Members"></asp:Label>
                    <div class="Hideorder"></div>
                </a>

            </li>
            <li class="WhoHasBirthday">
                <a class="TabLink WhoHasBirthdayContainer" href="#/WhoHasBirthdayContainer" onclick="ActivateThisTab(this)">
                    <asp:Label ID="lblWhoHasBirthDay" runat="server" Text="Who has Birthday"></asp:Label>
                    <div class="Hideorder"></div>
                </a>

            </li>
        </ul>
        <div id="QuickMembersShow">
            <div id="NewMembersContainer" class="TabContent">
                <div class="ProfilesContainer">
                    <asp:ListView ID="lvNewMembers" runat="server">
                        <ItemTemplate>
                            <div class="Profile">
                                <div class="pic-round-img-123">
                                    <asp:HyperLink ID="HyperLink1" runat="server"
                                        NavigateUrl='<%# System.Web.VirtualPathUtility.ToAbsolute("~/Members/Profile.aspx?p=") &  HttpUtility.UrlEncode(Eval("LoginName").Tostring()) %>'
                                        ImageUrl='<%# Dating.Server.Site.Web.AppUtils.GetImage(Eval("ProfileID").Tostring(),Eval("FileName").Tostring(), Eval("GenderId").Tostring(), False, Me.IsHTTPS,Dating.Server.Core.DLL.PhotoSize.D150) %>'
                                        onclick="ShowLoading();"  onmouseover="AddHovered(this);"  onmouseleave="RemoveHovered(this);"></asp:HyperLink>
                                           <div runat="server" class='<%# If(Eval("IsOnlineNow").ToString() = "True", "online-indicator", "online-indicator offline")%>' > </div>
                                </div>
                            <div class="login-link">

                                    <asp:HyperLink ID="HyperLink2" runat="server"
                                        Text='<%# Eval("LoginName") %>'
                                        NavigateUrl='<%# System.Web.VirtualPathUtility.ToAbsolute("~/Members/Profile.aspx?p=") &  HttpUtility.UrlEncode(Eval("LoginName").Tostring()) %>'
                                        CssClass="login-name"
                                        onclick="ShowLoading();"  onmouseover="AddHovered2(this);"  onmouseleave="RemoveHovered2(this);"/>
                                    <span class="Moreinfo"><%# evaluateAgeAndCity(Eval("Birthday"),Eval("City")) %> </span>

                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:ListView>
                    <div class="clear"></div>
                </div>
                <div class="BtnMoreContainer">
                    <asp:HyperLink ID="btnMoreNewMembers" runat="server" CssClass="btnMore" NavigateUrl="~/Members/Search.aspx?vw=NEWEST" Text="More Profiles"></asp:HyperLink>
                </div>
            </div>
            <div id="WhoHasBirthdayContainer" class="TabContent">
                <div class="ProfilesContainer">
                    <asp:ListView ID="lvWhoHasBirthday" runat="server">
                        <ItemTemplate>
                            <div class="Profile">
                                <div class="pic-round-img-123">
                                    <asp:HyperLink ID="lnkProfileView" runat="server"
                                        NavigateUrl='<%# System.Web.VirtualPathUtility.ToAbsolute("~/Members/Profile.aspx?p=") &  HttpUtility.UrlEncode(Eval("LoginName").Tostring()) %>'
                                        ImageUrl='<%# Dating.Server.Site.Web.AppUtils.GetImage(Eval("ProfileID").Tostring(),Eval("FileName").Tostring(), Eval("GenderId").Tostring(), False, Me.IsHTTPS,Dating.Server.Core.DLL.PhotoSize.D150) %>'
                                        onclick="ShowLoading();" onmouseover="AddHovered(this);"  onmouseleave="RemoveHovered(this);"></asp:HyperLink>
                                      <div runat="server" class='<%# If(Eval("IsOnlineNow").ToString() = "True", "online-indicator", "online-indicator offline")%>' > </div>
                                </div>
                                <div class="login-link">
                                    <asp:HyperLink ID="lnkProfileView2" runat="server"
                                        Text='<%# Eval("LoginName") %>'
                                        NavigateUrl='<%# System.Web.VirtualPathUtility.ToAbsolute("~/Members/Profile.aspx?p=") &  HttpUtility.UrlEncode(Eval("LoginName").Tostring()) %>'
                                        CssClass="login-name"
                                        onclick="ShowLoading();" onmouseover="AddHovered2(this);"  onmouseleave="RemoveHovered2(this);"/>
                                    <span class="Moreinfo"><%# evaluateAgeAndCity(Eval("Birthday"),Eval("City")) %> </span>

                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:ListView>
                    <div class="clear"></div>
                </div>
                <div class="BtnMoreContainer">
                    <asp:HyperLink ID="lnkMoreProfilesWithBirthDay" runat="server" CssClass="btnMore" NavigateUrl="~/Members/Search.aspx?vw=birthdaytoday" Text="More Profiles"></asp:HyperLink>
                </div>
            </div>
        </div>
    </div>
</div>

﻿Imports Dating.Server.Datasets.DLL
'Imports Sites.SharedSiteCode
'Imports SpiceLogicPayPalStandard
Imports Library.Public
Imports Dating.Server.Core.DLL

Public Class UCSelectPayment2
    Inherits BaseUserControl

    Protected Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            'If (_pageData Is Nothing) Then _pageData = New clsPageData("control.UCSelectPayment", Context)
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData("control.UCSelectPayment", Context, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property

    Private Sub Page_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        Try
            If Me._pageData IsNot Nothing Then
                RemoveHandler Me._pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If

        Catch ex As Exception
        End Try

        Try
            MyBase.Dispose()
        Catch ex As Exception
        End Try
    End Sub

    'Protected ReadOnly Property PriceData As EUS_Price
    '    Get
    '        If (eus_PriceRec Is Nothing) Then
    '            calculateTransactionData()
    '        End If
    '        Return eus_PriceRec
    '    End Get
    'End Property

    Private eus_PriceRec As EUS_Price
    Dim amount As String = ""
    Dim type As String = ""
    Dim g_credits As String = ""
    Dim choise As String = ""

    Private Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        ' get customer last transaction
        Try
            If (False AndAlso Not Page.IsPostBack) Then

                'Dim tran As EUS_CustomerTransaction = (From itm In MyBase.CMSDBDataContext.EUS_CustomerTransactions
                '                                      Where itm.CustomerID = Me.MasterProfileId
                '                                      Order By itm.Date_Time Descending
                '                                      Select itm).FirstOrDefault()

                'If (tran IsNot Nothing) Then
                '    If ((Not String.IsNullOrEmpty(tran.MemberID) AndAlso tran.PaymentMethods = "CreditCard") OrElse
                '        tran.PaymentMethods = "CreditCard") Then

                '        Dim url As String = ResolveUrl("~/Members/SelectPaymentCC.aspx") '&popup=popupWhatIs
                '        Dim popupTitle = CurrentPageData.GetCustomString("Credit.Cards.Charge.Again.Title")
                '        btnCreditCards.OnClientClick = "openPopupCreditCard(this,'" & AppUtils.JS_PrepareString(popupTitle) & "', '" & AppUtils.JS_PrepareString(url) & "');return false;"

                '        'Dim popupMessage As String = WhatIsIt.OnMoreInfoClickFunc(ResolveUrl("~/Members/InfoWin.aspx?info=SelectPayment_Charge_Same_Credit_Card&popup=popupWhatIs"), "Credit Card", 650, 350)
                '        'popupMessage = "function fnError_Sending_Receiver_DoNot_Have_Photo(){" & popupMessage & "};"
                '        'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "SelectPayment_Charge_Same_Credit_Card", popupMessage, True)

                '    End If
                'End If

            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Searching transactions")
        End Try


        Try
            calculateTransactionData()
            Dim promoCode As String = txPromoCode.Text
            If Session("PromoCode") Is Nothing Or Session("PromoCode") <> promoCode Or Session("PromoCode") = "" Then
                Session("PromoCode") = promoCode
            End If
            Dim product As String = g_credits & type.ToLower()
            amount = "1.00"
            '   Response.Redirect("pwSetPayment.aspx?amount=" & amount & "&product=" & product)
            'Dim url = ConfigurationManager.AppSettings("PaymentSendPage") & "?method=pp&price=" & amount & "&product=" & product
            'iframePayment.Attributes.Add("src", url)
            Dim url As String = ConfigurationManager.AppSettings("PaymentSendPage") & "?method=" & If(Me.MasterProfileId = 165078, "PW", "PWO") & "&type=" & type & "&price=" & amount & "&product=" & product
            iframePaymentPw.Attributes.Add("src", url)
            'Response.Redirect(url)
            'HttpContext.Current.ApplicationInstance.CompleteRequest()
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

        Try
            'If (Not Me.IsPostBack) Then
            LoadLAG()
            'LoadView()
            'End If
            Try
                Dim i As Integer = DataHelpers.CountTransactionByProfileId(Me.MasterProfileId)
                ''If i = 0 Then
                'plPayPal.Visible = False
                'End If

                Dim cl As String = Me.GetCurrentProfile().Country
                If cl <> "GR" Then
                    plReseller.Visible = False
                    TrResellers.Visible = False
                    plGoodDeals.Visible = False
                    Tr1CreditCard.Visible = True
                Else
                    Tr1CreditCard.Visible = False
                End If
            Catch ex As Exception

            End Try

            'If (Me.MasterProfileId = 308 OrElse _
            '    Me.MasterProfileId = 295 OrElse _
            '    Me.MasterProfileId = 269 OrElse _
            '    Me.MasterProfileId = 272 OrElse _
            '    Me.MasterProfileId = 371 ) Then
            If Me.SessionVariables.MemberData.RegisterDate >= New Date(2016, 9, 16, 0, 0, 0) Then
                plGoodDeals.Visible = False
                NewPaysafe.Visible = True
                Tr3.Visible = False
                plReseller.Visible = True
                Tr1CreditCard.Visible = True
                plPayPal.Visible = True
            Else
                plGoodDeals.Visible = False
                NewPaysafe.Visible = True
                Tr1CreditCard.Visible = True
                Tr3.Visible = False
                plReseller.Visible = True
                plPayPal.Visible = True
            End If
            'If Me.MasterProfileId = 269 OrElse Me.MasterProfileId = 183730 OrElse Me.MasterProfileId = 185408 Then
            '    NewPaysafe.Visible = True
            '    Tr3.Visible = True
            '    plReseller.Visible = True
            '    Tr1CreditCard.Visible = True
            ' Else
            '    plGoodDeals.Visible = False
            '    NewPaysafe.Visible = False
            '    Tr1CreditCard.Visible = True
            'End If
            If (clsConfigValues.Get__test_payment_for_customers().Contains(Me.MasterProfileId)) Then
                trNoPayment.Visible = True
                pntTest.Visible = True
                btnPayTestUser.Text = btnPayTestUser.Text.Replace("#USER#", Me.GetCurrentProfile.LoginName)
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

        Try
            calculateTransactionData()
            BuyNowButton1.ItemName = "Goomena.com " & g_credits & " Credits"
            BuyNowButton1.Amount = amount
            '  BuyNowButton1.
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "BuyNow")
        End Try
    End Sub



    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
        'LoadView()
    End Sub





    Public Sub LoadLAG() 'ByVal LAGID As String, ByVal NoCache As Boolean
        Try

            Dim isGreek As Boolean = False
            If (Me.MasterProfileId > 1 AndAlso Me.GetCurrentProfile(True) IsNot Nothing) Then
                Dim cl As String = Me.GetCurrentProfile().Country
                If cl = "GR" OrElse cl = "CY" Then
                    isGreek = True
                    lbPaySafeHeadline.Text = Me.CurrentPageData.GetCustomString("lbPaySafeHeadline2")
                Else
                    lbPaySafeHeadline.Text = Me.CurrentPageData.GetCustomString("lbPaySafeHeadline")
                End If
            Else

                Dim cl As String = HttpContext.Current.Session("GEO_COUNTRY_CODE")
                If cl = "GR" OrElse cl = "CY" Then
                    isGreek = True
                    lbPaySafeHeadline.Text = Me.CurrentPageData.GetCustomString("lbPaySafeHeadline2")
                Else
                    lbPaySafeHeadline.Text = Me.CurrentPageData.GetCustomString("lbPaySafeHeadline")
                End If
            End If


            lbPaypalHeadline.Text = Me.CurrentPageData.GetCustomString("lbPaypalHeadline")

            lbCreditCardsHeadline.Text = Me.CurrentPageData.GetCustomString("lbCreditCardsHeadline")
            lbPayByPhoneHeadline.Text = Me.CurrentPageData.GetCustomString("lbPayByPhoneHeadline")
            lbAlertPayHeadline.Text = Me.CurrentPageData.GetCustomString("lbAlertPayHeadline")
            lbBitPayHeadline.Text = Me.CurrentPageData.GetCustomString("lbBitPayHeadline")
            lblReseller.Text = Me.CurrentPageData.GetCustomString("lblReseller")
            lbAlertPayMore.Text = Me.CurrentPageData.GetCustomString("AlertPayTIP")
            lbPaypalMore.Text = Me.CurrentPageData.GetCustomString("PaypalTIP")
            lbPaysafeMore.Text = Me.CurrentPageData.GetCustomString("PaysafeTIP")
            lbPayByPhoneMore.Text = Me.CurrentPageData.GetCustomString("PayByPhoneTIP")
            lbCreditCardsMore.Text = Me.CurrentPageData.GetCustomString("CreditCardsTIP")
            lnkPaymentWall.Text = CurrentPageData.GetCustomString("lnkOtherMethodsHeadline")
            lblRese.Text = CurrentPageData.GetCustomString("lblRese")
            lbPaymentWallMore.Text = Me.CurrentPageData.GetCustomString("PaymentWallTIP")
            lblPaymentWithPaypal.Text = Me.CurrentPageData.GetCustomString("lblPaymentWithPaypal")
            lblOthersHeadline.Text = CurrentPageData.GetCustomString("lnkOtherMethodsHeadline")
            lbOthersTIP.Text = Me.CurrentPageData.GetCustomString("OthersTIP")
            lbBitPayMore.Text = Me.CurrentPageData.GetCustomString("lbBitPayMore")
            lblResselerInform.Text = Me.CurrentPageData.GetCustomString("lblResselerInform")
            lblResselerInform.Text = Me.CurrentPageData.GetCustomString("lblResselerInform")
            btnRedirect.Text = Me.CurrentPageData.GetCustomString("btReselerOk")
            lblNewPaysafe.Text = lbPaySafeHeadline.Text
            If (Not Me.IsPostBack) Then

                'Dim cPageBasic As CMS.LAG.Site.Core.clsSiteLAG.clsPageBasicReturn
                ''Dim LagID As String = Session("LagID")
                'Dim PageName As String = "UCSelectPayment.ascx" 'AppUtils.getPageFileNameFromURL(Request.Url.ToString())
                'cPageBasic = gLAG.GetPageBasics(LAGID, PageName)
                PanelSelectPayment.HeaderText = Me.CurrentPageData.GetCustomString("PanelSelectPayment")
                lbPaymentHeader.Text = Me.CurrentPageData.GetCustomString("lbPaymentHeader")
                lbPaymentExtraInfo.Text = Me.CurrentPageData.GetCustomString("lbPaymentExtraInfo3")
                Dim product As String = g_credits & type.ToLower()
                Dim url As String = "http://digitadiko.com/el/goomena/465-goomena-1000-.html"
                Select Case g_credits
                    Case 1000
                        url = "http://digitadiko.com/el/goomena/463-goomena-1000-.html"
                        Exit Select
                    Case 3000
                        url = "http://digitadiko.com/el/goomena/464-goomena-1000-.html"
                        Exit Select
                    Case 6000
                        url = "http://digitadiko.com/el/goomena/465-goomena-1000-.html"
                        Exit Select
                    Case Else
                        url = "http://digitadiko.com/el/goomena/466-goomena-1000-.html"
                End Select



              


                '   Dim url As String = "http://www.emediashop.gr/goomena?product=" & product & "&usrname=" & HttpUtility.UrlEncode(Me.ProfileLoginName)
                '   btnReseller.NavigateUrl = url
                'AlertPayTIP.Text = Me.CurrentPageData.GetCustomString( "AlertPayTIP")
                btnRedirect.NavigateUrl = url
                If isGreek Then
                    btnPaySafe.PostBackUrl = url
                End If

                ASPxPopupControl2.Windows.FindByName("Win_PaypalTIP").HeaderText = Me.CurrentPageData.GetCustomString("PaypalTIP_HeaderText")
                ASPxPopupControl2.Windows.FindByName("Win_PaysafeTIP").HeaderText = Me.CurrentPageData.GetCustomString("PaysafeTIP_HeaderText")
                ASPxPopupControl2.Windows.FindByName("Win_PayByPhoneTIP").HeaderText = Me.CurrentPageData.GetCustomString("PayByPhoneTIP_HeaderText")
                ASPxPopupControl2.Windows.FindByName("Win_MoneyBookersTIP").HeaderText = Me.CurrentPageData.GetCustomString("MoneyBookersTIP_HeaderText")
                ASPxPopupControl2.Windows.FindByName("Win_PayZaTIP").HeaderText = Me.CurrentPageData.GetCustomString("PayZaTIP_HeaderText")
                ASPxPopupControl2.Windows.FindByName("Win_CreditCardsTIP").HeaderText = Me.CurrentPageData.GetCustomString("CreditCardsTIP_HeaderText")
                ASPxPopupControl2.Windows.FindByName("Win_PaymentWallTIP").HeaderText = Me.CurrentPageData.GetCustomString("PaymentWallTIP_HeaderText")
                ASPxPopupControl2.Windows.FindByName("Win_BitPayTIP").HeaderText = Me.CurrentPageData.GetCustomString("BitPayTIP_HeaderText")

                ASPxPopupControl2.Windows.FindByName("Win_OthersTIP").HeaderText = Me.CurrentPageData.GetCustomString("OthersTIP_HeaderText")


                Try
                    If (clsPricing.VATCountries.Contains(Session("GEO_COUNTRY_CODE"))) Then
                        plPayPal.Visible = True
                    Else
                        plPayPal.Visible = False
                    End If
                    'btnPaypalPW.Visible = False
                    'btnPaypalPW.Visible = False
                    'Dim rnd As New ModRandom
                    'Dim a As Integer = rnd.GetRandom(0, 1000)
                    'If a > 500 Then
                    'btnPaypalPW.Visible = True
                    'Else
                    'btnPayPal.Visible = True
                    'End If

                Catch ex As Exception

                End Try

                calculateTransactionData()

                lblVatInfo.Text = Me.CurrentPageData.GetCustomString("lblVatInfo")
                If (eus_PriceRec.IsSubscription) Then
                    lblPaymentDescription.Text = CurrentPageData.GetCustomString("lbProductInfo.Subscription")
                    lblPaymentDescription.Text = lblPaymentDescription.Text.Replace("###CREDITSAMOUNT###", eus_PriceRec.Credits)
                    Dim durStr As String = clsCustomer.GetDurationString(eus_PriceRec.duration, GetLag())
                    lblPaymentDescription.Text = lblPaymentDescription.Text.Replace("###DURATION###", durStr)
                    lblPaymentDescription.Text = lblPaymentDescription.Text.Replace("###PAYMENTAMOUNT###", eus_PriceRec.Amount) ' & "*"
                    lblPaymentDescription.Text = lblPaymentDescription.Text.Replace("###CURRENCY###", Me.GetCurrency())
                Else
                    lblPaymentDescription.Text = CurrentPageData.GetCustomString("lbProductInfo2")
                    lblPaymentDescription.Text = lblPaymentDescription.Text.Replace("###CREDITSAMOUNT###", eus_PriceRec.Credits)
                    Dim durStr As String = clsCustomer.GetDurationString(eus_PriceRec.duration, GetLag())
                    lblPaymentDescription.Text = lblPaymentDescription.Text.Replace("###DURATION###", durStr)
                    lblPaymentDescription.Text = lblPaymentDescription.Text.Replace("###PAYMENTAMOUNT###", eus_PriceRec.Amount) ' & "*"
                    lblPaymentDescription.Text = lblPaymentDescription.Text.Replace("###CURRENCY###", Me.GetCurrency())
                End If
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Protected Sub btnPayByPhone_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPayByPhone.Click
        If Not clsCurrentContext.VerifyLogin() Then
            ' Session("CustomerID") = 13
            Response.Redirect("~/Login.aspx", True)
        Else
            calculateTransactionData()
            Dim promoCode As String = txPromoCode.Text
            If Session("PromoCode") Is Nothing Or Session("PromoCode") <> promoCode Or Session("PromoCode") = "" Then
                Session("PromoCode") = promoCode
            End If
            Dim product As String = g_credits & type.ToLower()

            Response.Redirect("payment.aspx?method=phone&price=" & amount & "&product=" & product)
        End If


    End Sub

    Protected Sub btnAlertPay_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAlertPay.Click
        If Not clsCurrentContext.VerifyLogin() Then
            ' Session("CustomerID") = 13
            Response.Redirect("~/Login.aspx", True)
        Else
            calculateTransactionData()
            Dim promoCode As String = txPromoCode.Text
            If Session("PromoCode") Is Nothing Or Session("PromoCode") <> promoCode Or Session("PromoCode") = "" Then
                Session("PromoCode") = promoCode
            End If
            Dim product As String = g_credits & type.ToLower()

            Response.Redirect("payment.aspx?method=alertpay&price=" & amount & "&product=" & product)
        End If

    End Sub

    Sub calculateTransactionData()
        If (String.IsNullOrEmpty(Request.QueryString("choice"))) Then
            Throw New ProductNotSelectedException()
        End If

        choise = Request.QueryString("choice").ToLower
        If choise.Contains("credits") Then
            type = "Credits"
            g_credits = choise.Replace("credits", "")
        End If
        If choise.Contains("days") Then
            type = "days"
            g_credits = choise.Replace("days", "")
        End If
        Dim willseeNewPrices As Boolean = False
        If Me.SessionVariables.MemberData.RegisterDate >= New Date(2016, 9, 16, 0, 0, 0) Then
            willseeNewPrices = True
        End If
        Dim productCode As String = clsPricing.GetProductCode(g_credits, type, Me.SessionVariables.MemberData.Country, willseeNewPrices)

        'Dim productCode As String = "dd" & g_credits & type
        'Select Case (Session("GEO_COUNTRY_CODE"))
        '    Case "AL" : productCode = "dd" & g_credits & type & "LEK" '"EUR" '"LEK"
        'End Select

        eus_PriceRec = clsPricing.GetPriceForProductCode(productCode)
        amount = eus_PriceRec.Amount.ToString("0.00")
    End Sub

    Protected Sub btnPaySafe_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPaySafe.Click
        If Not clsCurrentContext.VerifyLogin() Then
            Response.Redirect("~/Login.aspx", True)
        Else
            calculateTransactionData()
            Dim promoCode As String = txPromoCode.Text
            If Session("PromoCode") Is Nothing Or Session("PromoCode") <> promoCode Or Session("PromoCode") = "" Then
                Session("PromoCode") = promoCode
            End If
            Dim product As String = g_credits & type.ToLower()

            Dim url As String = "https://www.goomena.com/Members/pwSetPayment.aspx?type=PV&amount=" & amount & "&product=" & product
            If (Not String.IsNullOrEmpty(ConfigurationManager.AppSettings("PaymentRedirectPage"))) Then
                url = ConfigurationManager.AppSettings("PaymentRedirectPage") & "?type=PV&amount=" & amount & "&product=" & product
            End If
            Response.Redirect(url)
        End If

    End Sub
    Protected Sub lnkNewPaysafe_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkNewPaysafe.Click
        If Not clsCurrentContext.VerifyLogin() Then
            Response.Redirect("~/Login.aspx", True)
        Else
            calculateTransactionData()
            Dim promoCode As String = txPromoCode.Text
            If Session("PromoCode") Is Nothing Or Session("PromoCode") <> promoCode Or Session("PromoCode") = "" Then
                Session("PromoCode") = promoCode
            End If
            Dim product As String = g_credits & type.ToLower()

            Dim url As String = "https://www.goomena.com/Members/pwSetPayment.aspx?type=PV&amount=" & amount & "&product=" & product
            If (Not String.IsNullOrEmpty(ConfigurationManager.AppSettings("PaymentRedirectPage"))) Then
                url = ConfigurationManager.AppSettings("PaymentRedirectPage") & "?type=paysafecard&amount=" & amount & "&product=" & product
            End If
            Response.Redirect(url)
        End If

    End Sub
    Protected Sub btnCreditCards_Click(sender As Object, e As System.EventArgs) Handles btnCreditCards.Click
        If Not clsCurrentContext.VerifyLogin() Then
            ' Session("CustomerID") = 13
            Response.Redirect("~/Login.aspx", True)
        Else
            calculateTransactionData()
            Dim promoCode As String = txPromoCode.Text
            If Session("PromoCode") Is Nothing Or Session("PromoCode") <> promoCode Or Session("PromoCode") = "" Then
                Session("PromoCode") = promoCode
            End If
            Dim product As String = g_credits & type.ToLower()
            amount = "1.00"

            ''Response.Redirect("pwSetPayment.aspx?type=clickandbuy&amount=" & amount & "&product=" & product)
            'Response.Redirect("https://www.goomena.com/Members/pwSetPayment.aspx?type=CC&amount=" & amount & "&product=" & product)
            ''Response.Redirect("/Members/pwSetPayment.aspx?type=CC&amount=" & amount & "&product=" & product)

            Dim url As String = "https://www.goomena.com/Members/pwSetPayment.aspx?type=CC&amount=" & amount & "&product=" & product
            If (Not String.IsNullOrEmpty(ConfigurationManager.AppSettings("PaymentRedirectPage"))) Then
                url = ConfigurationManager.AppSettings("PaymentRedirectPage") & "?type=CC&amount=" & amount & "&product=" & product
            End If
            If Me.MasterProfileId = 269 OrElse Me.MasterProfileId = 183730 Then
                url = ConfigurationManager.AppSettings("PaymentRedirectPage") & "?type=payweb&amount=" & amount & "&product=" & product
            End If
            If (hdfCreditCardsSame.Value = "1") Then url = url & "&MemberId=1"
            Response.Redirect(url)
        End If

    End Sub
    Protected Sub btnPayPalDir_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPayPalDir.Click

        If Not clsCurrentContext.VerifyLogin() Then
            Response.Redirect("~/Login.aspx", True)
        Else
            calculateTransactionData()
            Dim promoCode As String = txPromoCode.Text
            If Session("PromoCode") Is Nothing Or Session("PromoCode") <> promoCode Or Session("PromoCode") = "" Then
                Session("PromoCode") = promoCode
            End If


            Dim product As String = g_credits & type.ToLower()
            '    Dim productCode As String = "dd" & g_credits & type
            '   Dim ProductDescription As String = g_credits & type & " PayPal"
            '  Dim productId As Integer = GetProductId_RegNow(g_credits)
            Dim url As String = ""
            If (ConfigurationManager.AppSettings("spPaypalOnlyForTest") = "True" And Me.MasterProfileId = 269) OrElse ConfigurationManager.AppSettings("spPaypalOnlyForTest") <> "True" Then

                url = "https://www.goomena.com/Members/pwSetPayment.aspx?type=PP&amount=" & amount & "&product=" & product
                If (Not String.IsNullOrEmpty(ConfigurationManager.AppSettings("PaymentRedirectPage"))) Then
                    url = ConfigurationManager.AppSettings("PaymentRedirectPage") & "?type=PP&amount=" & amount & "&product=" & product
                End If
            Else
                url = "https://www.goomena.com/Members/pwSetPayment.aspx?type=PP&amount=" & amount & "&product=" & product
                If (Not String.IsNullOrEmpty(ConfigurationManager.AppSettings("PaymentRedirectPage"))) Then
                    url = ConfigurationManager.AppSettings("PaymentRedirectPage") & "?type=PP&amount=" & amount & "&product=" & product
                End If
            End If

            Response.Redirect(url)
        End If

    End Sub
    Protected Sub btnBitPayDir_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBitPayDir.Click

        If Not clsCurrentContext.VerifyLogin() Then
            Response.Redirect("~/Login.aspx", True)
        Else
            calculateTransactionData()
            Dim promoCode As String = txPromoCode.Text
            If Session("PromoCode") Is Nothing Or Session("PromoCode") <> promoCode Or Session("PromoCode") = "" Then
                Session("PromoCode") = promoCode
            End If


            Dim product As String = g_credits & type.ToLower()
            '    Dim productCode As String = "dd" & g_credits & type
            '   Dim ProductDescription As String = g_credits & type & " PayPal"
            '  Dim productId As Integer = GetProductId_RegNow(g_credits)

            Dim url As String = "https://www.goomena.com/Members/pwSetPayment.aspx?type=BitPay&amount=" & amount & "&product=" & product
            If (Not String.IsNullOrEmpty(ConfigurationManager.AppSettings("PaymentRedirectPage"))) Then
                url = ConfigurationManager.AppSettings("PaymentRedirectPage") & "?type=BitPay&amount=" & amount & "&product=" & product
            End If
            Response.Redirect(url)
        End If

    End Sub

#Region "test functions"


    Protected Sub img_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) 'Handles img.Click
        Try
            calculateTransactionData()
            Dim promoCode As String = txPromoCode.Text
            If Session("PromoCode") Is Nothing Or Session("PromoCode") <> promoCode Or Session("PromoCode") = "" Then
                Session("PromoCode") = promoCode
            End If

            clsCustomer.AddTransaction("Add Credits with Epoch. Reference: 1111. ProductCode:[dd1000Credits]. ProductCode:[dd1000Credits]",
                           1.0,
                           1111,
                           "",
                           g_credits,
                           "127.0.0.1",
                           Request.Params("HTTP_USER_AGENT"),
                           Session("Referrer"),
                           Me.MasterProfileId,
                           Session("CustomReferrer"),
                           Me.GetCurrentMasterProfile().eMail,
                           PaymentMethods.PayPal,
                           promoCode,
                           1,
                           Nothing,
                           "EUR",
                           New clsDataRecordIPN())
            'Request.Params("HTTP_REFERER"),
            Response.Redirect(ResolveUrl("~/members/paymentok.aspx"))
            'If (Not String.IsNullOrEmpty(Request.QueryString("returnurl"))) Then
            '    lnkCancelCreditsCharging.NavigateUrl = Server.UrlDecode(Request.QueryString("returnurl"))
            'Else
            '    ' hacked

            'End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub



    Protected Sub btnPayTestUser_Click(sender As Object, e As EventArgs) Handles btnPayTestUser.Click

        Dim ipnData As New clsDataRecordIPN()
        ipnData.BuyerInfo = New clsDataRecordBuyerInfo With {
            .PayerID = "PayerID", _
            .FirstName = Me.GetCurrentProfile.FirstName, _
            .LastName = Me.GetCurrentProfile.LastName, _
            .PayerEmail = Me.GetCurrentProfile.eMail _
        }
        ipnData.CustomerID = Me.MasterProfileId
        ipnData.CustomReferrer = Me.GetCurrentProfile().CustomReferrer
        ipnData.custopmerIP = Session("IP")
        ipnData.PaymentDateTime = Date.UtcNow
        ipnData.PayProviderAmount = System.Convert.ToInt32(eus_PriceRec.Amount)
        ipnData.PayProviderID = 2222
        ipnData.PayProviderTransactionID = 3333
        ipnData.PayTransactionID = 4444
        ipnData.PromoCode = "PromoCode"
        'cDataRecordIPN.SaleDescription = quantity & type.ToLower()
        'cDataRecordIPN.SaleDescription = "Reseller Add Money"
        'cDataRecordIPN.SaleDescription = quantity & type
        ipnData.SaleDescription = "Add Credits with Test. Reference: 1111. ProductCode:[" & eus_PriceRec.ProductCode & "]. PaymentType:[pp]. MemberID:[1111]"
        '"Add Credits with Epoch. Reference: 1573111153. ProductCode:[dd3000Credits]. PaymentType:[PP]. MemberID:[1573111153]" '
        ipnData.SaleQuantity = eus_PriceRec.Credits
        ipnData.SalesSiteID = ConfigurationManager.AppSettings("siteID")
        ipnData.TransactionTypeID = 1111
        ipnData.Currency = eus_PriceRec.Currency


        Dim sData As String
        sData = ipnData.PayProviderAmount & "+" & ipnData.PaymentDateTime & "+" & ipnData.PayTransactionID & "+" & ipnData.CustomerID & "+" & ipnData.SalesSiteProductID & "Extr@Ded0men@"
        '   Dim h As New Library.Public.clsHash
        Dim Code As String = Library.Public.clsHash.ComputeHash(sData, "SHA512")
        ipnData.VerifyHASH = Code

        Using srvc As New UniPAYIPN
            Dim cDataRecordIPNReturn As clsDataRecordIPNReturn = srvc.IPNEventRaised(ipnData)
            txtOutput.Text = txtOutput.Text & "ErrorCode : " & cDataRecordIPNReturn.ErrorCode & vbCrLf
            txtOutput.Text = txtOutput.Text & "HasErrors : " & cDataRecordIPNReturn.HasErrors & vbCrLf
            txtOutput.Text = txtOutput.Text & "Message : " & cDataRecordIPNReturn.Message & vbCrLf
            txtOutput.Text = txtOutput.Text & "ExtraMessage : " & cDataRecordIPNReturn.ExtraMessage & vbCrLf
        End Using
  
    End Sub

#End Region




    Protected Sub lnkOtherMethods_Click(sender As Object, e As EventArgs) Handles btnPaymentWall.Click, btnOther.Click

        If Not clsCurrentContext.VerifyLogin() Then
            ' Session("CustomerID") = 13
            Response.Redirect("~/Login.aspx", True)
        Else
            calculateTransactionData()
            Dim promoCode As String = txPromoCode.Text
            If Session("PromoCode") Is Nothing Or Session("PromoCode") <> promoCode Or Session("PromoCode") = "" Then
                Session("PromoCode") = promoCode
            End If
            Dim product As String = g_credits & type.ToLower()
            amount = "1.00"
            Response.Redirect("pwSetPayment.aspx?amount=" & amount & "&product=" & product)
        End If

    End Sub

    'Protected Sub btnCreditCardsDir_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles btnCreditCardsDir.Click

    '    If Not clsCurrentContext.VerifyLogin() Then
    '        ' Session("CustomerID") = 13
    '        Response.Redirect("~/Login.aspx", True)
    '    Else
    '        calculateTransactionData()
    '        Dim promoCode As String = txPromoCode.Text
    '        If Session("PromoCode") Is Nothing Or Session("PromoCode") <> promoCode Or Session("PromoCode") = "" Then
    '            Session("PromoCode") = promoCode
    '        End If

    '        Dim product As String = g_credits & type.ToLower()
    '        Dim productCode As String = "dd" & g_credits & type
    '        Dim ProductDescription As String = g_credits & type & " CreditCard"
    '        Dim productId As Integer = GetProductId_RegNow(g_credits)

    '        Dim transId As String = SaveToPayTransactions(PaymentMethods.CreditCard, Me.MasterProfileId, amount, ProductDescription, g_credits, Request.UserHostAddress)
    '        Dim path As String = ResolveUrl("~/IPN/RegNowSetPayment.aspx?type=CC&itemName=" & productId & "&amount=" & amount & "&TransID=" & transId & "&currency=EUR")
    '        Response.Redirect(path)

    '        'Dim ProductDescription As String = "Payed with 2CO using " & PaymentMethods.CreditCard.ToString() & " for " & quantity & " " & type & "."
    '        'path = ResolveUrl("~/IPN/2coSetPayment.aspx?type=CC&itemName=" & 1 & "&amount=" & amount & "&TransID=" & transId & "&currency=EUR")
    '        'Response.Redirect(path)
    '    End If

    'End Sub



    Private Shared Function SaveToPayTransactions(PayProviderID As Integer,
                                                 CustomerID As Integer,
                                                 Amount As Object,
                                                 ProductDescription As String,
                                                 g_credits As Integer,
                                                 UserHostAddress As String) As String

        '   Dim SalesSiteID As Integer = ConfigurationManager.AppSettings("siteID")
        Dim transId As String = (DateTime.UtcNow.ToFileTime() & "_" & (New Random()).Next(0, 1000)).ToString()

        Dim sql As String = <sql><![CDATA[
INSERT INTO [UNI_PayTransactions]
           ([PayProviderID]
           ,[SalesSiteID]
           ,[SalesSiteProductID]
           ,[CustomerID]
           ,[CustomerIP]
           ,[PaymentDateTime]
            ,[Currency]
            ,[Amount]
            ,UniqueID
            ,SiteProductCode
            ,SiteProductDescription)
     VALUES
           (@PayProviderID
           ,@SalesSiteID
           ,1 --SalesSiteProductID
           ,@CustomerID
           ,@CustomerIP
           ,@PaymentDateTime 
           ,@Currency
           ,@Amount
           ,@UniqueID
           ,@GoomenaCredits
           ,@ProductDescription);
SELECT SCOPE_IDENTITY();
]]></sql>
        Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


            Using cmd As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(con, sql)


                cmd.Parameters.AddWithValue("PayProviderID", PayProviderID)
                cmd.Parameters.AddWithValue("SalesSiteID", ConfigurationManager.AppSettings("siteID"))
                cmd.Parameters.AddWithValue("CustomerID", CustomerID)
                cmd.Parameters.AddWithValue("CustomerIP", UserHostAddress)
                cmd.Parameters.AddWithValue("PaymentDateTime", DateTime.UtcNow)
                cmd.Parameters.AddWithValue("Currency", "EUR")
                cmd.Parameters.AddWithValue("Amount", Amount)
                cmd.Parameters.AddWithValue("UniqueID", transId)
                cmd.Parameters.AddWithValue("ProductDescription", ProductDescription)
                cmd.Parameters.AddWithValue("GoomenaCredits", g_credits)

                DataHelpers.ExecuteScalar(cmd)
            End Using
        End Using
        Return transId
    End Function



    Public Shared Function GetProductId_RegNow(credits As Integer)
        Dim id As Integer

        Select Case credits
            Case 1000 : id = 1
            Case 3000 : id = 2
            Case 6000 : id = 3
            Case 9000 : id = 6
            Case 10000 : id = 4
            Case 30000 : id = 5
            Case 50000 : id = 6
        End Select

        Return id
    End Function


    'Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
    '    hdfCreditCardsSame.Value = ""
    'End Sub

    Private Sub lnkGoodDeals_Click(sender As Object, e As EventArgs) Handles lnkGoodDeals.Click

        Dim urls As String = "http://goodeals.com"
        Dim oPost As New RemotePost()
        oPost.Url = "http://goodeals.com/?vv" & g_credits & "&lagid=" & GetLag()
        oPost.Add("logname", Me.ProfileLoginName)
        oPost.Add("prid", Me.MasterProfileId)
        oPost.Add("mail", Me.SessionVariables.MemberData.Email)

        Dim product As String = "vv" & g_credits & "Credits"
        oPost.Add("pck", product)
        oPost.Post()
     
    End Sub
End Class
﻿<%@ Control Language="vb" AutoEventWireup="false" 
    CodeBehind="LikesControl.ascx.vb" 
    Inherits="Dating.Server.Site.Web.LikesControl" %>
<script type="text/javascript">
    var animating_elemnts_list = new Array();
</script>
<asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">

    <asp:View ID="View1" runat="server">
    </asp:View>
    
    
    <asp:View ID="vwNoPhoto" runat="server">
        <asp:FormView ID="fvNoPhoto" runat="server" EnableViewState="false">
        <ItemTemplate>
<div id="ph-edit" class="no-photo">
    <div class="items_none items_hard">
    <div class="items_none_wrap">
        <div class="items_none_text">
            <%= GetKeys().HasNoPhotosText%>
        </div>
        <div class="search_members_button_right"><asp:HyperLink ID="lnk" runat="server" CssClass="btn lighter" NavigateUrl="~/Members/Photos.aspx" onclick="ShowLoading();"><%= GetKeys().AddPhotosText%><i class="icon-chevron-right"></i></asp:HyperLink></div>
        <div class="clear"></div>
    </div>
    </div>
</div>
<%--<div class="items_hard">
    <%# Eval("HasNoPhotosText")%>
	<p><asp:HyperLink ID="lnk" runat="server" CssClass="btn lighter" NavigateUrl="~/Members/Photos.aspx" onclick="ShowLoading();"><%# Eval("AddPhotosText")%><i class="icon-chevron-right"></i></asp:HyperLink></p>
	<div class="clear"></div>
</div>--%>
        </ItemTemplate>
        </asp:FormView>
    </asp:View>
    
    
    <asp:View ID="vwNoOffers" runat="server">
        <asp:FormView ID="fvNoOffers" runat="server" Width="69px" EnableViewState="false">
        <ItemTemplate>
<div class="items_none">
    <div id="likes_icon" class="middle_icon"></div>
    <div class="items_none_text">
        <%# Eval("NoOfferText")%>
	    <div class="clear"></div>
        <div class="search_members_button">
            <asp:HyperLink ID="lnk4" runat="server" CssClass="btn lighter" NavigateUrl="~/Members/Search.aspx" onclick="ShowLoading();"><%= GetKeys().SearchOurMembersText%><i class="icon-chevron-right"></i></asp:HyperLink>
        </div>
    </div>
</div>
        </ItemTemplate>
        </asp:FormView>
    </asp:View>


    <asp:View ID="vwNewOffers" runat="server">
<asp:Repeater ID="rptNew" runat="server">
<ItemTemplate>
<div class="l_item l_new <%# If(Eval("IsPoke").ToString()="True", "poke", "") %>">
    <div class="left">
        <div class="info">
            <div ID="rptNewDescr" runat="server" enableviewstate="false">
                <h2 class="wink"><%# Eval("YouReceivedWinkOfferTextNew")%></h2>
                <p id="pWink" runat="server" visible='<%# Eval("IsWink") %>'><%# Eval("WantToKnowFirstDatePriceTextNew")%></p>
                <p id="pPoke" runat="server" visible='<%# Eval("IsPoke") %>'><%# Eval("YouReceivedPokeDescriptionText")%></p>
                <p id="pAmount" runat="server" visible='<%# Eval("IsOffer") %>'><%# Eval("WillYouAcceptDateWithForAmountText")%></p>
            </div>
 			<div class="bot_actions lfloat">
                <div class="lfloat btn-group" ID="ActionsMenu" runat="server" Visible='<%# Eval("AllowActionsMenu") %>'>
                    <a class="btn btn-small dropdown-toggle" data-toggle="dropdown" href="#"><%= GetKeys().ActionsText%> <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li ID="liActsSendMsgOnce" runat="server" visible='<%# Eval("AllowActionsSendMessageOnce")%>' EnableViewState="false"><asp:HyperLink 
                                ID="lnkSendMsgOnce" runat="server" NavigateUrl='<%# Eval("SendMessageUrlOnce")%>' 
                                onclick="ShowLoading();"
                                CssClass="btn"><%# Eval("SendMessageOnceText")%></asp:HyperLink></li>

                        <li ID="liActsSendMsgMany" runat="server" visible="false" EnableViewState="false"><%--visible='<%# Eval("AllowActionsSendMessageMany")%>'--%><asp:HyperLink 
                                ID="lnkSendMsgMany" runat="server" NavigateUrl='<%# Eval("SendMessageUrlMany")%>' 
                                onclick="ShowLoading();"><%# Eval("SendMessageManyText")%></asp:HyperLink></li>

                        <li ID="liActsSendMsg" runat="server" visible='<%# Eval("AllowActionsSendMessage")%>' EnableViewState="false"><asp:HyperLink 
                                ID="lnkSendMsg2" runat="server" NavigateUrl='<%# Eval("SendMessageUrl")%>' 
                                onclick="ShowLoading();"
                                CssClass="btn"><%= GetKeys().SendMessageText%></asp:HyperLink></li><%--<i class="icon-chevron-right"></i>--%>

                        <li ID="liActsUnlock" runat="server" visible='<%# Eval("AllowActionsUnlock")%>'><asp:LinkButton 
                                ID="lnkActsUnlock" runat="server" CommandName="UNLOCKOFFER" 
                                CommandArgument='<%# Eval("OfferId") %>'><%# Eval("ActionsUnlockText")%></asp:LinkButton></li><%--<i class="icon-chevron-right icon-white"></i>--%>

                        <li ID="liActsMakeOffer" runat="server" visible='<%# Eval("AllowActionsCreateOffer") %>' EnableViewState="false"><asp:HyperLink 
                                ID="lnkNewMakeOffer2" runat="server" NavigateUrl='<%# Eval("CreateOfferUrl") %>' 
                                onclick="ShowLoading();"
                                CssClass="btn"><span><%# Eval("ActionsMakeOfferText")%></span></asp:HyperLink></li>

                        <li ID="liActsPoke" runat="server" visible='<%# Eval("AllowActionsPoke")%>'><asp:LinkButton ID="lnkPoke" runat="server" 
                                CommandName="POKE" CommandArgument='<%# "offr_" & Eval("OfferID") & "-prof_" & Eval("OtherMemberProfileID") %>'
                                CssClass="btn"><%= GetKeys().PokeText %></asp:LinkButton></li>
                    
                        <li ID="liActsDelOffr" runat="server" visible='<%# Eval("AllowActionsDeleteOffer")%>'><asp:LinkButton 
                                ID="lnkDelOffr" runat="server" CommandName="DELETEOFFER" 
                                CommandArgument='<%# Eval("OfferId") %>' OnClientClick='<%#  Eval("OfferDeleteConfirmMessage")  %>'
                                CssClass="btn"><%= GetKeys().DeleteOfferText%></asp:LinkButton></li>
                    </ul>
                </div>
                <div class="lfloat btn-group" ID="RejectsMenu" runat="server" Visible='<%# Eval("AllowRejectsMenu") %>'>
                    <a class="btn btn-small dropdown-toggle" data-toggle="dropdown" href="#"><%= GetKeys().RejectText%> <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><asp:LinkButton ID="lnkRejectType" runat="server" 
                            CommandName="REJECTTYPE" CommandArgument='<%# Eval("OfferID")%>'
                            CssClass="btn"><i class="icon-chevron-right"></i><%= GetKeys().NotInterestedText%></asp:LinkButton></li>
                        <li><asp:LinkButton ID="lnkRejectFar" runat="server" 
                            CommandName="REJECTFAR" CommandArgument='<%# Eval("OfferID")%>'
                            CssClass="btn"><i class="icon-chevron-right"></i><%= GetKeys().TooFarAwayText%></asp:LinkButton></li>
                        <li><asp:LinkButton ID="lnkRejectBad" runat="server" 
                            CommandName="REJECTBAD" CommandArgument='<%# Eval("OfferID")%>'
                            CssClass="btn"><i class="icon-chevron-right"></i><%= GetKeys().NotEnoughInfoText%></asp:LinkButton></li>
                        <li><asp:LinkButton ID="lnkRejectExp" runat="server" 
                            CommandName="REJECTEXPECTATIONS" CommandArgument='<%# Eval("OfferID")%>'
                            CssClass="btn"><i class="icon-chevron-right"></i><%= GetKeys().DifferentExpectationsText%></asp:LinkButton></li>
                    </ul>
                </div>
                <div class="lfloat">
                <asp:LinkButton ID="lnkNewCounter" runat="server" Visible='<%# Eval("AllowCounter") %>' CommandName="OFFERCOUNTER" CommandArgument='<%# Eval("OfferID")%>' 
                    class="btn btn-small"><span><%= GetKeys().CounterText%></span></asp:LinkButton>
                </div>
                <div class="lfloat">
                <asp:LinkButton ID="lnkNewAccept" runat="server" Visible='<%# Eval("AllowAccept") %>' CommandName="OFFERACCEPT" CommandArgument='<%# Eval("OfferID")%>' 
                    class="btn btn-small"><span><%= GetKeys().AcceptText%></span></asp:LinkButton>
                </div>
                <div class="lfloat">
                <asp:HyperLink ID="lnkNewMakeOffer" runat="server" 
                    EnableViewState="false"
                    visible='<%# Eval("AllowCreateOffer") %>' NavigateUrl='<%# Eval("CreateOfferUrl") %>'
                    class="btn btn-small" onclick="ShowLoading();"><span><%= GetKeys().MakeOfferText%></span></asp:HyperLink>
                </div>
                <asp:Panel runat="server" ID="divTooltipholder"
                    EnableViewState="false"
                    CssClass="lfloat icon_tooltipholder" 
                    Visible='<%# Eval("AllowTooltipPopupLikes")%>'>
                    <asp:LinkButton ID="lnkWhatIs" runat="server" OnClientClick="OnMoreInfoClick('[contentUrl]',this,'[headerText]');return false;" CssClass="more_info2"><%--<img runat="server" id="icon_tip" src="~/images2/mbr2/lern-icon.png" alt=""/>--%></asp:LinkButton>
                </asp:Panel>
            </div>
        </div>
    </div>
    <div class="right" ID="dRight" runat="server" EnableViewState="false">
        <div class="photo">
            <div class="pr-photo-116-noshad">
                <asp:HyperLink ID="hl2" runat="server" NavigateUrl='<%# Eval("OtherMemberProfileViewUrl") %>' onclick="ShowLoading();"><img src="<%# Eval("OtherMemberImageUrl") %>" class="round-img-116" /></asp:HyperLink>
            </div>
        </div>
        <div class="info">
            <h2><asp:HyperLink ID="lnk2" runat="server" NavigateUrl='<%# Eval("OtherMemberProfileViewUrl")%>' onclick="ShowLoading();"><%# Eval("OtherMemberLoginName")%></asp:HyperLink></h2>
            <%--<p><%# Eval("OtherMemberHeading")%></p>--%>
            <div class="stats">
                <%# MyBase.WriteOffer_MemberInfo(Container.DataItem)%>
                <div class="tooltip_offer" style="display: none;">
                    <div class="tooltip_text"><%# Eval("MilesAwayText")%></div>
                </div>
            </div>
            <div class="tt_enabled_distance distance_absolute <%# Eval("DistanceCss")%>">
            </div>
            <asp:Panel runat="server" ID="pnlIsOnline" Visible='<%# Eval("OtherMemberIsOnline") %>' class="rfloat">
                <div id="pnlIsOnline<%# Eval("OtherMemberProfileID") %>" 
                    class="online-indicator"><asp:Image ID="imgOn" runat="server" ImageUrl="//cdn.goomena.com/Images/spacer10.png" AlternateText="online" ToolTip="online" /><asp:Label
                                    ID="lblOnline" runat="server" Text="online"></asp:Label>
                </div>
                <script type="text/javascript">
                    if ('<%# Eval("OtherMemberIsOnline").ToString() %>' == 'True') animating_elemnts_list.push('pnlIsOnline<%# Eval("OtherMemberProfileID") %>');
                </script>
            </asp:Panel>
        </div>
    </div>
    <div class="clear"></div>
</div>
</ItemTemplate>
</asp:Repeater>
    </asp:View>


    <asp:View ID="vwPendingOffers" runat="server">
<asp:Repeater ID="rptPending" runat="server">
<ItemTemplate>

<div class="l_item l_pending <%# If(Eval("IsPoke").ToString()="True", "poke", "") %>" login="<%# Eval("OtherMemberLoginName")%>">
    <div class="left">
        <div class="info">
            <div ID="rptPendingDescr" runat="server" enableviewstate="false">
                <h2><%# Eval("YourWinkSentTextNew")%></h2>
                <p><%= GetKeys().AwaitingResponseText %></p>
            </div>
 			<div class="bot_actions lfloat">
                <div class="lfloat">
                    <asp:LinkButton ID="lnkCancelWink" runat="server" CssClass="btn" 
                        Visible='<%# Eval("AllowCancelWink") %>' 
                        CommandName="CANCELWINK" CommandArgument='<%# Eval("OfferID")%>'><span><%= GetKeys().CancelWinkText%></span></asp:LinkButton>
                </div>
                <div class="lfloat">
                    <asp:LinkButton ID="lnkCancelOffer" runat="server" CssClass="btn" 
                        Visible='<%# Eval("AllowCancelPendingOffer")%>' 
                        CommandName="CANCELOFFER" CommandArgument='<%# Eval("OfferID")%>'><span><%= GetKeys().CancelOfferText%></span></asp:LinkButton>
                </div>
                <div class="lfloat">
                    <asp:LinkButton ID="lnkCancelPoke" runat="server" CssClass="btn" 
                        Visible='<%# Eval("AllowCancelPoke")%>' 
                        CommandName="CANCELPOKE" CommandArgument='<%# Eval("OfferID")%>'><span><%= GetKeys().CancelPokeText%></span></asp:LinkButton>
                </div>
                <asp:Panel runat="server" ID="divTooltipholder"
                    EnableViewState="false"
                    CssClass="lfloat icon_tooltipholder" 
                    Visible='<%# Eval("AllowTooltipPopupLikes")%>'>
                    <asp:LinkButton ID="lnkWhatIs" runat="server" OnClientClick="OnMoreInfoClick('[contentUrl]',this,'[headerText]');return false;" CssClass="more_info2"><%--<img runat="server" id="icon_tip" src="~/images2/mbr2/lern-icon.png" alt=""/>--%></asp:LinkButton>
                </asp:Panel>
            </div>
        </div>
    </div>
    <div class="right car " ID="dRight" runat="server" EnableViewState="false">
        <div class="photo">
            <div class="pr-photo-116-noshad">
                <asp:HyperLink ID="lnk1" runat="server" NavigateUrl='<%# Eval("OtherMemberProfileViewUrl") %>' onclick="ShowLoading();"><img src="<%# Eval("OtherMemberImageUrl") %>" class="round-img-116" /></asp:HyperLink></div>
        </div>
        <div class="info">
            <h2><asp:HyperLink ID="lnk2" runat="server" NavigateUrl='<%# Eval("OtherMemberProfileViewUrl") %>' Text='<%# Eval("OtherMemberLoginName")%>'  onclick="ShowLoading();"/></h2>
            <%--<p><%# Eval("OtherMemberHeading")%></p>--%>
            <div class="stats">
                <%# MyBase.WriteOffer_MemberInfo(Container.DataItem)%>
                <div class="tt_enabled_distance distance_absolute <%# Eval("DistanceCss")%>">
                </div>
                <div class="tooltip_offer" style="display: none;">
                    <div class="tooltip_text"><%# Eval("MilesAwayText")%></div>
                </div>
            </div>
            <asp:Panel runat="server" ID="pnlIsOnline" Visible='<%# Eval("OtherMemberIsOnline") %>' class="rfloat">
                <div id="pnlIsOnline<%# Eval("OtherMemberProfileID") %>" 
                    class="online-indicator"><asp:Image ID="imgOn" runat="server" ImageUrl="//cdn.goomena.com/Images/spacer10.png" AlternateText="online" ToolTip="online" /><asp:Label
                                    ID="lblOnline" runat="server" Text="online"></asp:Label>
                </div>
                <script type="text/javascript">
                    if ('<%# Eval("OtherMemberIsOnline").ToString() %>' == 'True') animating_elemnts_list.push('pnlIsOnline<%# Eval("OtherMemberProfileID") %>');
                </script>
            </asp:Panel>
        </div>
    </div>
    <div class="clear">
    </div>
</div>
</ItemTemplate>
</asp:Repeater>
    </asp:View>


    <asp:View ID="vwRejectedOffers" runat="server">
<asp:Repeater ID="rptRejected" runat="server">
<ItemTemplate>

<div class="l_item l_rejected <%# Eval("RejectCssClass")  %> <%# If(Eval("IsPoke").ToString()="True", "poke", "") %> <%# If(Eval("OtherMemberGenderid").ToString()="1", "male", "female") %>"  login="<%# Eval("OtherMemberLoginName")%>">	
	<div class="left">
		<div class="info">
            <div ID="rptRejectedDescr" runat="server" enableviewstate="false">
			    <h2 class="rejected"><%# Eval("CancelledRejectedTitleTextNew")%></h2>
			    <p class="rejected"><%# Eval("CancelledRejectedDescriptionText")%></p>
            </div>
 			<div class="bot_actions lfloat">
                <%--<asp:Panel runat="server" ID="divTooltipholder" Visible="false"
                    CssClass="lfloat icon_tooltipholder">
                    <asp:LinkButton ID="lnkWhatIs" runat="server" OnClientClick="OnMoreInfoClick('[contentUrl]',this,'[headerText]');return false;"><img runat="server" id="icon_tip" src="~/images2/mbr2/lern-icon.png" alt=""/></asp:LinkButton>
                </asp:Panel>--%>
                <div class="lfloat">
                    <asp:LinkButton ID="lnkTryWink" runat="server" Visible='<%# Eval("AllowTryWink")%>' CommandName="TRYWINK" CommandArgument='<%# Eval("OfferId") %>' CssClass="btn"><%= GetKeys().TryWinkText%></asp:LinkButton>
                </div>
                <div class="lfloat">
                    <asp:LinkButton ID="lnkDeleteWink" runat="server" Visible='<%# Eval("AllowDeleteWink")%>' CommandName="DELETEOFFER" CommandArgument='<%# Eval("OfferId") %>' CssClass="btn"><%= GetKeys().DeleteWinkText%></asp:LinkButton>
                </div>
                <div class="lfloat">
                    <asp:LinkButton ID="lnkCreateOffer" runat="server" Visible='<%# Eval("AllowCreateOffer")%>' CommandName="TRYCREATEOFFER" CommandArgument='<%# Eval("OfferId") %>' CssClass="btn"><%= GetKeys().MakeNewOfferText%></asp:LinkButton>
                </div>
                <div class="lfloat">
                    <asp:LinkButton ID="lnkDeleteOffer" runat="server" Visible='<%# Eval("AllowDeleteOffer")%>' CommandName="DELETEOFFER" CommandArgument='<%# Eval("OfferId") %>' CssClass="btn"><%= GetKeys().DeleteOfferText%></asp:LinkButton>
                </div>
			</div>
		</div>
	</div>
	<div class="right car " ID="dRight" runat="server" EnableViewState="false">
        <div class="photo">
            <div class="pr-photo-116-noshad">
                <asp:HyperLink ID="lnk2" runat="server" NavigateUrl='<%# Eval("OtherMemberProfileViewUrl") %>' onclick="ShowLoading();"><img src="<%# Eval("OtherMemberImageUrl") %>" class="round-img-116" /></asp:HyperLink>
            </div>
        </div>
		<div class="info">
            <h2><asp:HyperLink ID="lnk3" runat="server" NavigateUrl='<%# Eval("OtherMemberProfileViewUrl") %>' Text='<%# Eval("OtherMemberLoginName")%>'  onclick="ShowLoading();"/></h2>
			<%--<p><%# Eval("OtherMemberHeading")%></p>--%>
			<div class="stats">
                <%# MyBase.WriteOffer_MemberInfo(Container.DataItem)%>
				<div class="tooltip_offer" style="display: none;">
					<div class="tooltip_text"><%# Eval("MilesAwayText")%></div>
				</div>
			</div>
			<div class="tt_enabled_distance distance_absolute <%# Eval("DistanceCss")%>"></div>
		</div>
	</div>
	<div class="clear"></div>
</div>
</ItemTemplate>
</asp:Repeater>
    </asp:View>


    <asp:View ID="vwAcceptedOffers" runat="server">
<asp:Repeater ID="rptAccepted" runat="server">
<ItemTemplate>

<div class="l_item l_accepted new_date" login="<%# Eval("OtherMemberLoginName")%>">
	<div class="left accepted_left">
		<div class="info">
            <div ID="rptAcceptedDescr" runat="server" enableviewstate="false">
			    <h2><%# Eval("OfferAcceptedWithAmountText") %></h2>
			    <p class="padding"><%# Eval("OfferAcceptedHowToContinueText") %></p>
            </div>
 			<div class="bot_actions lfloat">
                <div class="rfloat">
                    <asp:HyperLink ID="lnk1" runat="server" EnableViewState="false" Visible='<%# Eval("AllowSendMessage")%>' CssClass="btn" NavigateUrl='<%# Eval("SendMessageUrl")%>' onclick="ShowLoading();"><%= GetKeys().SendMessageText%><i class="icon-chevron-right"></i></asp:HyperLink>
                </div>
                <div class="rfloat">
                    <asp:LinkButton ID="lnk2" runat="server" Visible='<%# Eval("AllowDeleteAcceptedOffer")%>' CommandName="DELETEOFFER" CommandArgument='<%# Eval("OfferId") %>' CssClass="btn" OnClientClick='<%#  Eval("OfferDeleteConfirmMessage")  %>'><%= GetKeys().DeleteOfferText%></asp:LinkButton>
                </div>
                <div class="rfloat">
                    <asp:LinkButton ID="lnk3" runat="server" Visible='<%# Eval("AllowOfferAcceptedUnlock")%>' CssClass="btn" CommandName="UNLOCKOFFER" CommandArgument='<%# Eval("OfferId") %>'><%# Eval("OfferAcceptedUnlockText")%><i class="icon-chevron-right icon-white"></i></asp:LinkButton>
                </div>
                <div class="btn-group" ID="ActionsMenu" runat="server" Visible='<%# Eval("AllowActionsMenu") %>' EnableViewState="false">
                    <a class="btn btn-small dropdown-toggle" data-toggle="dropdown" href="#"><%= GetKeys().ActionsText%> <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li ID="liActsSendMsgOnce" runat="server" visible='<%# Eval("AllowActionsSendMessageOnce")%>'><asp:HyperLink 
                                ID="lnkSendMsgOnce" runat="server" NavigateUrl='<%# Eval("SendMessageUrlOnce")%>' onclick="ShowLoading();" CssClass="btn"><%# Eval("SendMessageOnceText")%></asp:HyperLink></li>

                        <li ID="liActsSendMsgMany" runat="server" visible="false"><%-- visible='<%# Eval("AllowActionsSendMessageMany")%>'--%><asp:HyperLink 
                                ID="lnkSendMsgMany" runat="server" NavigateUrl='<%# Eval("SendMessageUrlMany")%>' onclick="ShowLoading();" CssClass="btn"><%# Eval("SendMessageManyText")%></asp:HyperLink></li>
                    </ul>
                </div>
			</div>
            <asp:Panel CssClass="rfloat" runat="server" ID="pnlMsgSent" Visible='<%# Eval("IsMessageSent")%>' style="margin-left:5px;padding-top:10px;" EnableViewState="false">
                <asp:Image ID="imgCheck" runat="server" Imageurl="~/Images2/mbr2/message-send.png" ImageAlign="AbsMiddle" /><asp:Label ID="lblMsgSent" runat="server" ><%= GetKeys().MessageSentNotice %></asp:Label>
            </asp:Panel>
	        <div class="clear"></div>
		</div>
	    <div class="clear"></div>
    </div>

	<div class="right accepted_right " ID="dRight" runat="server" EnableViewState="false">
        <div class="photo">
            <div class="pr-photo-116-noshad">
                <asp:HyperLink ID="lnk11" runat="server" NavigateUrl='<%# Eval("OtherMemberProfileViewUrl") %>' onclick="ShowLoading();"><img src="<%# Eval("OtherMemberImageUrl") %>" class="round-img-116" /></asp:HyperLink>
            </div>
        </div>
		<div class="info">
            <h2><asp:HyperLink ID="lnk21" runat="server" NavigateUrl='<%# Eval("OtherMemberProfileViewUrl") %>' Text='<%# Eval("OtherMemberLoginName")%>'  onclick="ShowLoading();"/></h2>
			<%--<p><%# Eval("OtherMemberHeading")%></p>--%>
			<div class="stats">
                <%# MyBase.WriteOffer_MemberInfo(Container.DataItem)%>
				<div class="tooltip_offer" style="display: none;">
					<div class="tooltip_text"><%# Eval("MilesAwayText")%></div>
				</div>
			</div>
			<div class="tt_enabled_distance distance_absolute <%# Eval("DistanceCss")%>"></div>
		</div>
	</div>
	<div class="clear"></div>
</div>
</ItemTemplate>
</asp:Repeater>
    </asp:View>



</asp:MultiView>

<script type="text/javascript">

    function animateImg(elid) {
        var a = $('img', '#' + elid);
        if (a.is(".slide1"))
            a.removeClass("slide1");
        else
            a.addClass("slide1");
        if (a.length > 0)
            setTimeout("animateImg('" + elid + "')", 500);
    }
    function startAnimating() {
        for (c = 0; c < animating_elemnts_list.length; c++) {
            animateImg(animating_elemnts_list[c]);
        }
    }
    function _runScripts_LikesControl() {

        if (animating_elemnts_list.length > 0) { setTimeout(startAnimating, 500); }

        $("div.bot_actions", ".l_new .left .info").click(function () {
            var t = this;
            $(t).addClass("selected");
        }).mouseenter(function () {
            var t = this;
            $(t).addClass("selected");
        }).mouseleave(function () {
            var t = this;
            $(t).removeClass("selected");
            /*if ($(".btn-group.open", t).length > 0) {$(t).addClass("selected");} else {$(t).removeClass("selected");}*/
        });


        var scope = "#likes-list .l_new";
        scope = (($(".l_pending", "#likes-list").length > 0) ? "#likes-list .l_pending" : scope);
        scope = (($(".l_rejected", "#likes-list").length > 0) ? "#likes-list .l_rejected" : scope);
        scope = (($(".l_accepted", "#likes-list").length > 0) ? "#likes-list .l_accepted" : scope);
        var itms = $("a.btn", scope + " .left").css("width", "auto");
        for (var c = 0; c < itms.length; c++) {
            var w = $(itms[c]).width();
            $(itms[c]).css("width", "").removeClass("lnk130").removeClass("lnk180");
            if (w < 120)
                $(itms[c]).addClass("lnk130");
            else
                $(itms[c]).addClass("lnk180");
        }
        itms = $("a.dropdown-toggle", scope + " .left .btn-group").css("width", "auto");
        for (c = 0; c < itms.length; c++) {
            var w = $(itms[c]).width();
            $(itms[c]).css("width", "").removeClass("lnk130").removeClass("lnk180").removeClass("lnk107").removeClass("lnk116");
            if (w < 83)
                $(itms[c]).addClass("lnk107");
            else
                $(itms[c]).addClass("lnk116");
        }
    }

</script>












﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="MembersBarLeft.ascx.vb" Inherits="Dating.Server.Site.Web.MembersBarLeft" %>
<%@ Register Src="~/UserControls/ucNewWinksQuick.ascx" TagName="ucNewWinksQuick" TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/ucNewOffersQuick.ascx" TagName="ucNewOffersQuick" TagPrefix="uc2" %>
<%@ Register Src="~/UserControls/ucNewMessagesQuick.ascx" TagName="ucNewMessagesQuick" TagPrefix="uc4" %>
<%@ Register Src="~/UserControls/ucNewDatesQuick.ascx" TagName="ucNewDatesQuick" TagPrefix="uc5" %>

<script type="text/javascript">
    var quick_lists_popups = new Array();
</script>

<div class="lfloat membersBar">
<div class="lfloat mbr-menu-item" id="WinksPopupContent">
    <asp:HyperLink ID="btnWinks" runat="server" 
        CssClass="hdr-link btnWinks dropdown-toggle" 
        ViewStateMode="Disabled" data-toggle="dropdown">Winks</asp:HyperLink>

    <div class="mbr-menu-item-inner"></div>

    <asp:Panel ID="pnlWinksPopup" runat="server" style="position:relative;">
        <div id="divListQLk" class="dropdown-menu">
            <div class="popup-arrow"></div>
            <div class="dxpcControl" style="width:280px;min-height:90px;">
                <uc1:ucNewWinksQuick ID="ucNewWinks2" runat="server" ItemsPerPage="5" />
            </div>
            <div class="dxpcFooter">
                <div class="jewelFooter">
                    <asp:HyperLink ID="lnkWinksAll" runat="server" 
                        Text="<span>View All Winks</span>" 
                        NavigateUrl="~/Members/Likes.aspx?vw=likes" CssClass="seeMore" onClick="ShowLoading();">
                    </asp:HyperLink>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            quick_lists_popups.push('#WinksPopupContent');
            $("#<%= btnWinks.ClientID %>").dblclick(function () {
                var href = $("#<%= lnkWinksAll.ClientID %>").attr("href");
                jsLoad(href)
            });
        </script>
    </asp:Panel>
</div>


<div class="lfloat mbr-menu-item" id="MessagesPopupContent">
    <asp:HyperLink ID="btnMessages" runat="server" 
        CssClass="hdr-link btnMessages dropdown-toggle" 
        ViewStateMode="Disabled" data-toggle="dropdown">Messages</asp:HyperLink>

    <div class="mbr-menu-item-inner"></div>

    <asp:Panel ID="pnlMessagesPopup" runat="server" style="position:relative;">
        <div id="divListQMs" class="dropdown-menu">
            <div class="popup-arrow"></div>
            <div class="dxpcControl" style="width:280px;min-height:90px;">
                <uc4:ucNewMessagesQuick ID="ucNewMessagesQuick1" runat="server" 
                ItemsPerPage="5" />
            </div>
            <div class="dxpcFooter">
                <div class="jewelFooter">
                    <asp:HyperLink ID="lnkMessagesAll" runat="server" 
                        Text="<span>View All Messages</span>" 
                        NavigateUrl="~/Members/Messages.aspx" CssClass="seeMore" onClick="ShowLoading();">
                    </asp:HyperLink>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            quick_lists_popups.push('#MessagesPopupContent');
            $("#<%= btnMessages.ClientID %>").dblclick(function () {
                var href = $("#<%= lnkMessagesAll.ClientID %>").attr("href");
                jsLoad(href)
            });
        </script>
    </asp:Panel>
</div>
    <asp:PlaceHolder ID="PlaceHolder1" runat="server" Visible="false" >
<div class="lfloat mbr-menu-item" id="OffersPopupContent">
    <asp:HyperLink ID="btnOffers" runat="server" 
        CssClass="hdr-link btnOffers dropdown-toggle" 
        ViewStateMode="Disabled" data-toggle="dropdown">Offers</asp:HyperLink>

    <div class="mbr-menu-item-inner"></div>

    <asp:Panel ID="pnlOffersPopup" runat="server" style="position:relative;">
        <div id="divListQOf" class="dropdown-menu">
            <div class="popup-arrow"></div>
            <div class="dxpcControl" style="width:280px;min-height:90px;">
                <uc2:ucNewOffersQuick ID="ucNewOffers1" runat="server" ItemsPerPage="5" />
            </div>
            <div class="dxpcFooter">
                <div class="jewelFooter">
                    <asp:HyperLink ID="lnkOffersAll" runat="server" 
                        Text="<span>View All Offers</span>" 
                        NavigateUrl="~/Members/Offers3.aspx?vw=newoffers" CssClass="seeMore" onClick="ShowLoading();">
                    </asp:HyperLink>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            quick_lists_popups.push('#OffersPopupContent');
            $("#<%= btnOffers.ClientID %>").dblclick(function () {
                var href = $("#<%= lnkOffersAll.ClientID %>").attr("href");
                jsLoad(href)
            });
        </script>
    </asp:Panel>
</div>

    </asp:PlaceHolder>



<div class="lfloat mbr-menu-item" id="DatesPopupContent">
    <asp:HyperLink ID="btnDates" runat="server" 
        CssClass="hdr-link btnDates dropdown-toggle" 
        ViewStateMode="Disabled" data-toggle="dropdown">Dates</asp:HyperLink>

    <div class="mbr-menu-item-inner"></div>

    <asp:Panel ID="pnlDatesPopup" runat="server" style="position:relative;">
        <div id="divListQDt" class="dropdown-menu">
            <div class="popup-arrow"></div>
            <div class="dxpcControl" style="width:280px;min-height:90px;">
                <uc5:ucNewDatesQuick ID="ucNewDatesQuick1" runat="server" ItemsPerPage="5" />
            </div>
            <div class="dxpcFooter">
                <div class="jewelFooter">
                    <asp:HyperLink ID="lnkDatesAll" runat="server" 
                        Text="<span>View All Dates</span>" 
                        NavigateUrl="~/Members/Dates.aspx?vw=ACCEPTED" CssClass="seeMore" onClick="ShowLoading();">
                    </asp:HyperLink>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            quick_lists_popups.push('#DatesPopupContent');
            $("#<%= btnDates.ClientID %>").dblclick(function () {
                var href = $("#<%= lnkDatesAll.ClientID %>").attr("href");
                jsLoad(href)
            });
        </script>
    </asp:Panel>
</div>

<div class="lfloat mbr-menu-item" id="CreditsPopupContent">
    <asp:HyperLink ID="lnkCredits" runat="server" NavigateUrl="~/Members/Settings.aspx?vw=CREDITS" CssClass="hdr-link lnkCredits" onClick="ShowLoading();">Credits</asp:HyperLink>
    <div class="mbr-menu-item-inner"></div>
</div>
<div class="lfloat mbr-menu-item mbr-menu-item_last" id="GPolls">
    <asp:HyperLink ID="lnkLoveHate" runat="server" Visible="false"   NavigateUrl="~/Members/LoveHate.aspx" CssClass="hdr-link lnkLoveHate"  onClick="ShowLoading();"></asp:HyperLink>
    <div class="mbr-menu-item-inner"></div>
</div>
<div class="lfloat mbr-menu-item mbr-menu-item_last" id="GooMatch"   >
    <asp:HyperLink ID="lnkGooMatch" runat="server" Visible="true"   NavigateUrl="~/Members/Matching.aspx" CssClass="hdr-link lnkGooMatch"  onClick="ShowLoading();"></asp:HyperLink>
    <div class="mbr-menu-item-inner"></div>
</div>
<div class="clear"></div>
</div>

    <%--
    <dx:ASPxPopupControl ID="WinksPopup" runat="server" PopupElementID="btnWinks" PopupVerticalAlign="Below"
        PopupHorizontalAlign="LeftSides" ShowFooter="True" HeaderText="Winks" 
        ClientInstanceName="WinksPopupControl" Width="280" MinHeight="120px" 
        CssClass="" 
        EnableViewState="false">
        <HeaderTemplate>
            <div class="uiHeader uiHeaderBottomBorder jewelHeader">
                <div class="clearfix uiHeaderTop">
                    <div><h3 class="uiHeaderTitle"><asp:Literal ID="msg_WinksHeader" runat="server"/></h3></div>
                </div>
            </div>
        </HeaderTemplate>
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl5" runat="server">
                <uc1:ucNewWinksQuick ID="ucNewWinks1" runat="server" ItemsPerPage="5" />
            </dx:PopupControlContentControl>
        </ContentCollection>
        <ClientSideEvents 
PopUp="function(s, e) {

// correction for chrome-opera -top psoition
var hgt = s.GetCurrentTop()
jQuery(function($){
hgt = $('#topTable').height();
//hgt =hgt+1
});
var index= -1;
s.SetWindowPos(index, s.GetWindowElement(index), s.GetCurrentLeft(), hgt);
}"
EndCallback="function(s, e) {
bindClicktoNewWinks('#WinksPopupContent');
}" 
Closing="function(s, e) {
jQuery('#' + s.popupElementID).toggleClass('menuOpen');
}" 
Shown="function(s, e) {
jQuery('#' + s.popupElementID).toggleClass('menuOpen');
bindClicktoNewWinks('#WinksPopupContent');
}" />
        <ContentStyle>
            <Paddings Padding="0px" />
        </ContentStyle>
        <HeaderStyle>
        <Paddings Padding="0px" />
        </HeaderStyle>
        <FooterTemplate>
            <div class="jewelFooter">
<asp:HyperLink ID="lnkWinksAll"  ClientInstanceName="lnkWinksAll" runat="server" Text="<span>View All Winks</span>" NavigateUrl="~/Members/Likes.aspx?vw=likes" CssClass="seeMore" EncodeHtml="False">
                            <ClientSideEvents Click="ShowLoadingOnMenu" />
                        </asp:HyperLink>
            </div>
        </FooterTemplate>
    </dx:ASPxPopupControl>


<div class="lfloat" id="MessagesPopupContent">
    <asp:HyperLink ID="btnMessages" runat="server" 
        NavigateUrl="javascript:void(0);" CssClass="headerLink btnMessages" 
        ViewStateMode="Disabled">Messages</asp:HyperLink>
    <dx:ASPxPopupControl ID="MessagesPopup" runat="server" 
        PopupElementID="btnMessages" PopupVerticalAlign="Below"
        PopupHorizontalAlign="LeftSides" ShowFooter="True" HeaderText="Winks" 
        ClientInstanceName="MessagesPopupControl" Width="280" MinHeight="120px" 
        CssClass=""
        EnableViewState="false">
        <HeaderTemplate>
            <div class="uiHeader uiHeaderBottomBorder jewelHeader">
                <div class="clearfix uiHeaderTop">
                    <div><h3 class="uiHeaderTitle"><asp:Literal ID="msg_MessagesHeader" runat="server"/></h3></div>
                </div>
            </div>
        </HeaderTemplate>
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                <uc4:ucNewMessagesQuick ID="ucNewMessagesQuick1" runat="server" 
                    ItemsPerPage="5" />
            </dx:PopupControlContentControl>
        </ContentCollection>
        <ClientSideEvents  
PopUp="function(s, e) {

// correction for chrome-opera -top psoition
var hgt = s.GetCurrentTop()
jQuery(function($){
hgt = $('#topTable').height();
//hgt =hgt+1
});
var index= -1;
s.SetWindowPos(index, s.GetWindowElement(index), s.GetCurrentLeft(), hgt);
}"
EndCallback="function(s, e) {
bindClicktoNewWinks('#MessagesPopupContent');
}"  
Closing="function(s, e) {
jQuery('#' + s.popupElementID).toggleClass('menuOpen');
}" 
Shown="function(s, e) {
jQuery('#' + s.popupElementID).toggleClass('menuOpen');
bindClicktoNewWinks('#MessagesPopupContent');
}" />
        <ContentStyle>
            <Paddings Padding="0px" />
        </ContentStyle>
        <HeaderStyle>
        <Paddings Padding="0px" />
        </HeaderStyle>
        <FooterTemplate>
            <div class="jewelFooter">
<asp:HyperLink ID="lnkMessagesAll" runat="server" Text="<span>View All Messages</span>" 
NavigateUrl="~/Members/Messages.aspx" CssClass="seeMore" onClick="ShowLoading();">
                        </asp:HyperLink>
            </div>
        </FooterTemplate>
    </dx:ASPxPopupControl>
</div>


<div class="lfloat" id="OffersPopupContent">
    <asp:HyperLink ID="btnOffers" runat="server" NavigateUrl="javascript:void(0);" 
        CssClass="headerLink btnOffers" ViewStateMode="Disabled">Offers</asp:HyperLink>
    <dx:ASPxPopupControl ID="OffersPopup" runat="server" PopupElementID="btnOffers" PopupVerticalAlign="Below"
        PopupHorizontalAlign="LeftSides" ShowFooter="True" HeaderText="Offers" 
        ClientInstanceName="OffersPopupControl" Width="280" MinHeight="120px" 
        CssClass=""
        EnableViewState="false">
        <HeaderTemplate>
            <div class="uiHeader uiHeaderBottomBorder jewelHeader">
                <div class="clearfix uiHeaderTop">
                    <div><h3 class="uiHeaderTitle"><asp:Literal ID="msg_OffersHeader" runat="server"/></h3></div>
                </div>
            </div>
        </HeaderTemplate>
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl10" runat="server">
                <uc2:ucNewOffersQuick ID="ucNewOffers1" runat="server" ItemsPerPage="5" />
            </dx:PopupControlContentControl>
        </ContentCollection>
        <ClientSideEvents 
PopUp="function(s, e) {

// correction for chrome-opera -top psoition
var hgt = s.GetCurrentTop()
jQuery(function($){
hgt = $('#topTable').height();
//hgt =hgt+1
});
var index= -1;
s.SetWindowPos(index, s.GetWindowElement(index), s.GetCurrentLeft(), hgt);
}"
EndCallback="function(s, e) {
bindClicktoNewWinks('#OffersPopupContent');
}"  
Closing="function(s, e) {
jQuery('#' + s.popupElementID).toggleClass('menuOpen');
}" 
Shown="function(s, e) {
jQuery('#' + s.popupElementID).toggleClass('menuOpen');
bindClicktoNewWinks('#OffersPopupContent');
}"/>
        <ContentStyle>
            <Paddings Padding="0px" />
        </ContentStyle>
        <HeaderStyle>
        <Paddings Padding="0px" />
        </HeaderStyle>
        <FooterTemplate>
            <div class="jewelFooter">
<asp:HyperLink ID="lnkOffersAll" runat="server" Text="<span>View All Offers</span>" 
NavigateUrl="~/Members/Offers3.aspx?vw=newoffers" CssClass="seeMore" onClick="ShowLoading();">
                        </asp:HyperLink>
            </div>
        </FooterTemplate>
    </dx:ASPxPopupControl>
</div>


<div class="lfloat" id="DatesPopupContent">
    <asp:HyperLink ID="btnDates" runat="server" NavigateUrl="javascript:void(0);" 
        CssClass="headerLink btnDates" ViewStateMode="Disabled">Dates</asp:HyperLink>
    <dx:ASPxPopupControl ID="DatesPopup" runat="server" PopupElementID="btnDates" PopupVerticalAlign="Below"
        PopupHorizontalAlign="LeftSides" ShowFooter="True" HeaderText="Winks" 
        ClientInstanceName="DatesPopupControl" Width="280" MinHeight="120px" 
        CssClass=""
        EnableViewState="false">
        <HeaderTemplate>
            <div class="uiHeader uiHeaderBottomBorder jewelHeader">
                <div class="clearfix uiHeaderTop">
                    <div><h3 class="uiHeaderTitle"><asp:Literal ID="msg_DatesHeader" runat="server"/></h3></div>
                </div>
            </div>
        </HeaderTemplate>
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl4" runat="server">
                <uc5:ucNewDatesQuick ID="ucNewDatesQuick1" runat="server" ItemsPerPage="5" />
            </dx:PopupControlContentControl>
        </ContentCollection>
        <ClientSideEvents 
PopUp="function(s, e) {

// correction for chrome-opera -top psoition
var hgt = s.GetCurrentTop()
jQuery(function($){
hgt = $('#topTable').height();
//hgt =hgt+1
});
var index= -1;
s.SetWindowPos(index, s.GetWindowElement(index), s.GetCurrentLeft(), hgt);
}"
EndCallback="function(s, e) {
bindClicktoNewWinks('#DatesPopupContent');
}"  Closing="function(s, e) {
jQuery('#' + s.popupElementID).toggleClass('menuOpen');
}" Shown="function(s, e) {
jQuery('#' + s.popupElementID).toggleClass('menuOpen');
bindClicktoNewWinks('#DatesPopupContent');
}" />
        <ContentStyle>
            <Paddings Padding="0px" />
        </ContentStyle>
        <HeaderStyle>
        <Paddings Padding="0px" />
        </HeaderStyle>
        <FooterTemplate>
            <div class="jewelFooter">
<asp:HyperLink ID="lnkDatesAll" ClientInstanceName="lnkDatesAll" runat="server" Text="<span>View All Dates</span>" 
NavigateUrl="~/Members/Dates.aspx?vw=ACCEPTED" CssClass="seeMore" onClick="ShowLoading();">
                        </asp:HyperLink>
            </div>
        </FooterTemplate>
    </dx:ASPxPopupControl>
</div>


<div class="rfloat" id="SettingsPopupContent">
    <asp:HyperLink ID="btnSettings" runat="server" NavigateUrl="javascript:void(0);" CssClass="menuPulldown headerLink">More</asp:HyperLink>
    <dx:ASPxPopupControl ID="SettingsPopup" runat="server"
        PopupElementID="btnSettings" PopupVerticalAlign="Below" 
        PopupHorizontalAlign="RightSides" HeaderText="Account Settings" ClientInstanceName="ClientPopupControl"
        ShowHeader="False"
        EnableViewState="false">
        <ClientSideEvents 
PopUp="function(s, e) {

// correction for chrome-opera -top psoition
var hgt = s.GetCurrentTop()
jQuery(function($){
hgt = $('#topTable').height();
//hgt =hgt+1
});
var index= -1;
s.SetWindowPos(index, s.GetWindowElement(index), s.GetCurrentLeft(), hgt);
}"
Closing="function(s, e) {
jQuery('#' + s.popupElementID).toggleClass('menuOpen');
}" Shown="function(s, e) {
jQuery('#' + s.popupElementID).toggleClass('menuOpen');
}" />
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl3" runat="server">
                <ul id="userNavigation" class="navigation defaultStyle">
                    <li>
                        <asp:HyperLink ID="lnkAccSettings" runat="server" Text="Account Settings" CssClass="navSubmenu lnkAccSettings"
                            NavigateUrl="~/Members/Settings.aspx" onClick="ShowLoading();">
                        </asp:HyperLink>
                    </li>
                    <li>
                        <asp:HyperLink ID="lnkPubDefault" runat="server" Text="Public default" CssClass="navSubmenu lnkPubDefault"
                            NavigateUrl="~/Default.aspx" onClick="ShowLoading();">
                        </asp:HyperLink>
                    </li>
                    <li>
                        <asp:LoginStatus ID="LoginStatus1" runat="server" CssClass="navSubmenu LoginStatus1" />
                    </li>
                    <li class="menuDivider"></li>
                    <li>
                        <asp:HyperLink ID="lnkAccDelete" runat="server" Text="Delete Account" CssClass="navSubmenu lnkAccDelete"
                            NavigateUrl="~/Members/Settings.aspx?account=remove" onClick="ShowLoading();">
                        </asp:HyperLink>
                    </li>
                    <li>
                        <asp:HyperLink ID="btnHelp" runat="server" Text="Help" CssClass="navSubmenu btnHelp"
                            NavigateUrl="~/CmsPage.aspx?PageId=158" onClick="ShowLoading();">
                        </asp:HyperLink>
                    </li>
                </ul>
            </dx:PopupControlContentControl>
        </ContentCollection>
        <ContentStyle>
            <Paddings Padding="0px" />
        </ContentStyle>
        <HeaderStyle>
        <Paddings Padding="0px" />
        </HeaderStyle>
    </dx:ASPxPopupControl>
</div>
--%>


<%--

    top menu (likes, messages, search, settings ...)


    <dx:ASPxCallbackPanel runat="server" ID="ASPxCallbackPanel1" ClientInstanceName="CallbackPanel" RenderMode="Div" ShowLoadingPanel="False">
            <ClientSideEvents EndCallback="cbpTopBar_OnEndCallback"></ClientSideEvents>
            <PanelCollection>
                <dx:PanelContent ID="PanelContent3" runat="server">
                </dx:PanelContent>
            </PanelCollection>
        </dx:ASPxCallbackPanel>



        <div class="lfloat mbr-menu-item" id="WinksPopupContent">
        <asp:HyperLink ID="btnWinks" runat="server" NavigateUrl="javascript:void(0);" 
            CssClass="hdr-link btnWinks" ViewStateMode="Disabled">Winks</asp:HyperLink>

            <div class="mbr-menu-item-inner"></div>

        <dx:ASPxPopupControl ID="WinksPopup" runat="server" PopupElementID="btnWinks" 
            PopupVerticalAlign="Below"
            PopupHorizontalAlign="LeftSides" 
            ShowFooter="True" HeaderText="Winks" 
            ClientInstanceName="WinksPopupControl" 
            Width="270"
            MinHeight="120"
            CssClass="" 
            EnableViewState="false" 
            ShowHeader="False" RenderMode="Lightweight" 
            EnableDefaultAppearance="true" PopupHorizontalOffset="-40"
            Border-BorderStyle="None">
            <HeaderTemplate>
                <div class="uiHeader uiHeaderBottomBorder jewelHeader">
                    <div class="clearfix uiHeaderTop">
                        <div><h3 class="uiHeaderTitle"><asp:Literal ID="msg_WinksHeader" runat="server"/></h3></div>
                    </div>
                </div>
            </HeaderTemplate>
            <ContentCollection>
                <dx:PopupControlContentControl ID="PopupControlContentControl5" runat="server">
                    <div class="popup-arrow"></div>
                    <uc1:ucNewWinksQuick ID="ucNewWinks1" runat="server" ItemsPerPage="5" />
                </dx:PopupControlContentControl>
            </ContentCollection>
            <ClientSideEvents 
PopUp="DevExPopup_setPos"
EndCallback="function(s, e) {
bindClicktoNewWinks('#WinksPopupContent');
}" 
Closing="DevExPopup_Toggle" 
Shown="function(s, e) {
jQuery('#' + s.popupElementID).toggleClass('menuOpen');
bindClicktoNewWinks('#WinksPopupContent');
}" />
            <ContentStyle>
                <Paddings Padding="0px" />
            </ContentStyle>
            <HeaderStyle>
            <Paddings Padding="0px" />
            </HeaderStyle>
            <FooterTemplate>
                <div class="jewelFooter">
<dx:ASPxHyperLink ID="lnkWinksAll"  ClientInstanceName="lnkWinksAll" runat="server" Text="<span>View All Winks</span>" NavigateUrl="~/Members/Likes.aspx?vw=likes" CssClass="seeMore" EncodeHtml="False">
                                <ClientSideEvents Click="ShowLoadingOnMenu" />
                            </dx:ASPxHyperLink>
                </div>
            </FooterTemplate>
        </dx:ASPxPopupControl>
    </div>



    <div class="lfloat mbr-menu-item" id="MessagesPopupContent">
        <asp:HyperLink ID="btnMessages" runat="server" 
            NavigateUrl="javascript:void(0);" CssClass="hdr-link btnMessages" 
            ViewStateMode="Disabled">Messages</asp:HyperLink>

        <div class="mbr-menu-item-inner"></div>
                        
        <dx:ASPxPopupControl ID="MessagesPopup" runat="server" 
            PopupElementID="btnMessages" PopupVerticalAlign="Below"
            PopupHorizontalAlign="LeftSides" ShowFooter="True" HeaderText="Winks" 
            ClientInstanceName="MessagesPopupControl"
            Width="270"
            MinHeight="120" 
            CssClass=""
            EnableViewState="false" 
            ShowHeader="False" RenderMode="Lightweight" 
            EnableDefaultAppearance="true" PopupHorizontalOffset="-30"
            Border-BorderStyle="None">
            <HeaderTemplate>
                <div class="uiHeader uiHeaderBottomBorder jewelHeader">
                    <div class="clearfix uiHeaderTop">
                        <div><h3 class="uiHeaderTitle"><asp:Literal ID="msg_MessagesHeader" runat="server"/></h3></div>
                    </div>
                </div>
            </HeaderTemplate>
            <ContentCollection>
                <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                <div class="popup-wrapper opacity96">
                    <div class="popup-arrow"></div>
                    <div class="popup-content">
                        <uc4:ucNewMessagesQuick ID="ucNewMessagesQuick1" runat="server" 
                            ItemsPerPage="5" />
                    </div>
                </div>
                </dx:PopupControlContentControl>
            </ContentCollection>
            <ClientSideEvents  
PopUp="DevExPopup_setPos"
EndCallback="function(s, e) {
bindClicktoNewWinks('#MessagesPopupContent');
}"  
Closing="DevExPopup_Toggle" 
Shown="function(s, e) {
jQuery('#' + s.popupElementID).toggleClass('menuOpen');
bindClicktoNewWinks('#MessagesPopupContent');
}" />
            <ContentStyle>
                <Paddings Padding="0px" />
            </ContentStyle>
            <HeaderStyle>
            <Paddings Padding="0px" />
            </HeaderStyle>
            <FooterTemplate>
                <div class="jewelFooter">
<dx:ASPxHyperLink ID="lnkMessagesAll" ClientInstanceName="lnkMessagesAll"  runat="server" Text="<span>View All Messages</span>" NavigateUrl="~/Members/Messages.aspx" CssClass="seeMore" EncodeHtml="False">
                                <ClientSideEvents Click="ShowLoadingOnMenu" />
                            </dx:ASPxHyperLink>
                </div>
            </FooterTemplate>
        </dx:ASPxPopupControl>
    </div>


    <div class="lfloat mbr-menu-item" id="OffersPopupContent">
        <asp:HyperLink ID="btnOffers" runat="server" NavigateUrl="javascript:void(0);" 
            CssClass="hdr-link btnOffers" ViewStateMode="Disabled">Offers</asp:HyperLink>

        <div class="mbr-menu-item-inner"></div>

        <dx:ASPxPopupControl ID="OffersPopup" runat="server" PopupElementID="btnOffers" PopupVerticalAlign="Below"
            PopupHorizontalAlign="LeftSides" ShowFooter="True" HeaderText="Offers" 
            ClientInstanceName="OffersPopupControl"
            Width="270"
            MinHeight="120"
            CssClass=""
            EnableViewState="false" 
            ShowHeader="False" RenderMode="Lightweight" 
            EnableDefaultAppearance="true" PopupHorizontalOffset="-40"
            Border-BorderStyle="None">
            <HeaderTemplate>
                <div class="uiHeader uiHeaderBottomBorder jewelHeader">
                    <div class="clearfix uiHeaderTop">
                        <div><h3 class="uiHeaderTitle"><asp:Literal ID="msg_OffersHeader" runat="server"/></h3></div>
                    </div>
                </div>
            </HeaderTemplate>
            <ContentCollection>
                <dx:PopupControlContentControl ID="PopupControlContentControl10" runat="server">
                <div class="popup-wrapper opacity96">
                    <div class="popup-arrow"></div>
                    <div class="popup-content">
                        <uc2:ucNewOffersQuick ID="ucNewOffers1" runat="server" ItemsPerPage="5" />
                    </div>
                </div>
                </dx:PopupControlContentControl>
            </ContentCollection>
            <ClientSideEvents 
PopUp="DevExPopup_setPos"
EndCallback="function(s, e) {
bindClicktoNewWinks('#OffersPopupContent');
}"
Closing="DevExPopup_Toggle" 
Shown="function(s, e) {
jQuery('#' + s.popupElementID).toggleClass('menuOpen');
bindClicktoNewWinks('#OffersPopupContent');
}"/>
            <ContentStyle>
                <Paddings Padding="0px" />
            </ContentStyle>
            <HeaderStyle>
            <Paddings Padding="0px" />
            </HeaderStyle>
            <FooterTemplate>
                <div class="jewelFooter">
<dx:ASPxHyperLink ID="lnkOffersAll" ClientInstanceName="lnkOffersAll" runat="server" Text="<span>View All Offers</span>" NavigateUrl="~/Members/Offers3.aspx?vw=newoffers" CssClass="seeMore" EncodeHtml="False">
                                <ClientSideEvents Click="ShowLoadingOnMenu" />
                            </dx:ASPxHyperLink>
                </div>
            </FooterTemplate>
        </dx:ASPxPopupControl>
    </div>

    <div class="lfloat mbr-menu-item" id="DatesPopupContent">
        <asp:HyperLink ID="btnDates" runat="server" NavigateUrl="javascript:void(0);" 
            CssClass="hdr-link btnDates" ViewStateMode="Disabled">Dates</asp:HyperLink>

        <div class="mbr-menu-item-inner"></div>

        <dx:ASPxPopupControl ID="DatesPopup" runat="server" 
            PopupElementID="btnDates" PopupVerticalAlign="Below"
            PopupHorizontalAlign="LeftSides" ShowFooter="True" HeaderText="Winks" 
            ClientInstanceName="DatesPopupControl" Width="280" MinHeight="120px" 
            CssClass=""
            EnableViewState="false" 
            ShowHeader="False" RenderMode="Lightweight" 
            EnableDefaultAppearance="true" PopupHorizontalOffset="-40"
            Border-BorderStyle="None">
            <HeaderTemplate>
                <div class="uiHeader uiHeaderBottomBorder jewelHeader">
                    <div class="clearfix uiHeaderTop">
                        <div><h3 class="uiHeaderTitle"><asp:Literal ID="msg_DatesHeader" runat="server"/></h3></div>
                    </div>
                </div>
            </HeaderTemplate>
            <ContentCollection>
                <dx:PopupControlContentControl ID="PopupControlContentControl4" runat="server">
                <div class="popup-wrapper opacity96">
                    <div class="popup-arrow"></div>
                    <div class="popup-content">
                        <uc5:ucNewDatesQuick ID="ucNewDatesQuick1" runat="server" ItemsPerPage="5" />
                    </div>
                </div>
                </dx:PopupControlContentControl>
            </ContentCollection>
            <ClientSideEvents 
PopUp="DevExPopup_setPos"
EndCallback="function(s, e) {
bindClicktoNewWinks('#DatesPopupContent');
}"  
Closing="DevExPopup_Toggle" 
Shown="function(s, e) {
jQuery('#' + s.popupElementID).toggleClass('menuOpen');
bindClicktoNewWinks('#DatesPopupContent');
}" />
            <ContentStyle>
                <Paddings Padding="0px" />
            </ContentStyle>
            <HeaderStyle>
            <Paddings Padding="0px" />
            </HeaderStyle>
            <FooterTemplate>
                <div class="jewelFooter">
<dx:ASPxHyperLink ID="lnkDatesAll" ClientInstanceName="lnkDatesAll" runat="server" Text="<span>View All Dates</span>" NavigateUrl="~/Members/Dates.aspx?vw=ACCEPTED" CssClass="seeMore" EncodeHtml="False">
                                <ClientSideEvents Click="ShowLoadingOnMenu" />
                            </dx:ASPxHyperLink>
                </div>
            </FooterTemplate>
        </dx:ASPxPopupControl>
    </div>


                                                                                                                                                                                                                                                                                                
    <div class="lfloat mbr-menu-item" id="CreditsPopupContent">
        <asp:HyperLink ID="lnkCredits" runat="server" NavigateUrl="~/Members/Settings.aspx?vw=CREDITS" CssClass="hdr-link lnkCredits">Credits</asp:HyperLink>

        <div class="mbr-menu-item-inner"></div>
    </div>
    <div class="clear"></div>
</div>
<div class="rfloat membersBar">

    <div class="rfloat mbr-menu-item" id="SettingsPopupContent">
        <asp:HyperLink ID="btnSettings" runat="server" NavigateUrl="javascript:void(0);" CssClass="menuPulldown hdr-link">More</asp:HyperLink>

        <div class="mbr-menu-item-inner"></div>
                    
        <dx:ASPxPopupControl ID="SettingsPopup" runat="server"
            PopupElementID="btnSettings" PopupVerticalAlign="Below" 
            PopupHorizontalAlign="RightSides" HeaderText="Account Settings" 
            ClientInstanceName="ClientPopupControl"
            ShowHeader="False"
            EnableViewState="false"
            Border-BorderStyle="None">
            <ClientSideEvents 
PopUp="DevExPopup_setPos"
Closing="DevExPopup_Toggle" 
Shown="DevExPopup_Toggle" />
            <ContentCollection>
                <dx:PopupControlContentControl ID="PopupControlContentControl3" runat="server">
                    <ul id="userNavigation" class="navigation defaultStyle">
                        <li>
                            <dx:ASPxHyperLink ID="lnkAccSettings" runat="server" Text="Account Settings" 
                                CssClass="navSubmenu lnkAccSettings"
                                NavigateUrl="~/Members/Settings.aspx" >
                                <ClientSideEvents Click="ShowLoadingOnMenu" />
                            </dx:ASPxHyperLink>
                        </li>
                        <li>
                            <dx:ASPxHyperLink ID="lnkPubDefault" runat="server" Text="Public default" 
                                CssClass="navSubmenu lnkPubDefault"
                                NavigateUrl="~/Default.aspx" >
                                <ClientSideEvents Click="ShowLoadingOnMenu" />
                            </dx:ASPxHyperLink>
                        </li>
                        <li>
                            <asp:LoginStatus ID="LoginStatus1" runat="server" CssClass="navSubmenu LoginStatus1" />
                        </li>
                        <li class="menuDivider"></li>
                        <li>
                            <dx:ASPxHyperLink ID="lnkAccDelete" runat="server" Text="Delete Account" CssClass="navSubmenu lnkAccDelete"
                                NavigateUrl="~/Members/Settings.aspx?account=remove" >
                                <ClientSideEvents Click="ShowLoadingOnMenu" />
                            </dx:ASPxHyperLink>
                        </li>
                        <li>
                            <dx:ASPxHyperLink ID="btnHelp" runat="server" Text="Help" CssClass="navSubmenu btnHelp"
                                NavigateUrl="~/CmsPage.aspx?PageId=158" >
                                <ClientSideEvents Click="ShowLoadingOnMenu" />
                            </dx:ASPxHyperLink>
                        </li>
                    </ul>
                </dx:PopupControlContentControl>
            </ContentCollection>
            <ContentStyle>
                <Paddings Padding="0px" />
            </ContentStyle>
            <HeaderStyle>
            <Paddings Padding="0px" />
            </HeaderStyle>
        </dx:ASPxPopupControl>
    </div>

</div>--%>


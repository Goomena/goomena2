﻿Imports DevExpress.Web.ASPxPager
Imports Dating.Server.Core.DLL
Imports Dating.Server.Site.Web.OfferControl3
Imports DevExpress.Web.ASPxEditors

Public Class MessagesControl
    Inherits BaseUserControl




    Public Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            'If (_pageData Is Nothing) Then _pageData = New clsPageData("~/Members/Messages.aspx", Context)
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData("~/Members/Messages.aspx", Context, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property
    Private Sub Page_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        Try
            If Me._pageData IsNot Nothing Then
                RemoveHandler Me._pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If

        Catch ex As Exception
        End Try

        Try
            MyBase.Dispose()
        Catch ex As Exception
        End Try
    End Sub

    Public Property ShowNoPhotoText As Boolean
    Public Property ShowEmptyListText As Boolean

    Public Property UsersListView As MessagesListViewEnum
        Get
            If (Me.ViewState("MessagesListViewEnum") IsNot Nothing) Then Return Me.ViewState("MessagesListViewEnum")
            Return MessagesListViewEnum.None
        End Get
        Set(value As MessagesListViewEnum)
            Me.ViewState("MessagesListViewEnum") = value
        End Set
    End Property


    Private _list As List(Of clsMessageUserListItem)
    Public Property UsersList As List(Of clsMessageUserListItem)
        Get
            If (Me._list Is Nothing) Then Me._list = New List(Of clsMessageUserListItem)
            Return Me._list
        End Get
        Set(value As List(Of clsMessageUserListItem))
            Me._list = value
        End Set
    End Property



    <PersistenceMode(PersistenceMode.InnerDefaultProperty)>
    Public ReadOnly Property Repeater As Repeater
        Get
            If (UsersListView = MessagesListViewEnum.AllMessages) Then
                Return Me.rptNew

            ElseIf (UsersListView = MessagesListViewEnum.Inbox) Then
                Return Me.rptNew

            ElseIf (UsersListView = MessagesListViewEnum.NewMessages) Then
                Return Me.rptNew

            ElseIf (UsersListView = MessagesListViewEnum.SentMessages) Then
                Return Me.rptNew

            ElseIf (UsersListView = MessagesListViewEnum.Trash) Then
                Return Me.rptNew
            Else
                Return Nothing
            End If
        End Get
    End Property


    Protected Overrides Sub OnInit(e As System.EventArgs)
        MyBase.OnInit(e)
        'ModGlobals.AddScriptResourceToHeader(Me.Page, "~/Scripts/jquery-1.7.min.js")
        'ModGlobals.AddScriptResourceToHeader(Me.Page, "~/Scripts/bootstrap-dropdown.js")
    End Sub



    Public Overrides Sub DataBind()

        If (Me.ShowNoPhotoText) Then

            Dim oNoPhoto As New clsMessageUserListItem()
            oNoPhoto.HasNoPhotosText = Me.CurrentPageData.GetCustomString("HasNoPhotosText")
            oNoPhoto.AddPhotosText = Me.CurrentPageData.GetCustomString("AddPhotosText")

            Dim os As New List(Of clsMessageUserListItem)
            os.Add(oNoPhoto)

            fvNoPhoto.DataSource = os
            fvNoPhoto.DataBind()

            Me.MultiView1.SetActiveView(vwNoPhoto)


        ElseIf (Me.ShowEmptyListText) Then

            Dim oNoOffer As New clsMessageUserListItem()
            If (UsersListView = MessagesListViewEnum.AllMessages) Then
                oNoOffer.NoOfferText = Me.CurrentPageData.GetCustomString("AcceptedOffersListEmptyText_ND")

            ElseIf (UsersListView = MessagesListViewEnum.Inbox) Then
                oNoOffer.NoOfferText = Me.CurrentPageData.GetCustomString("AcceptedOffersListEmptyText_ND")

            ElseIf (UsersListView = MessagesListViewEnum.NewMessages) Then
                oNoOffer.NoOfferText = Me.CurrentPageData.GetCustomString("NewOffersListEmptyText_ND")

            ElseIf (UsersListView = MessagesListViewEnum.SentMessages) Then
                oNoOffer.NoOfferText = Me.CurrentPageData.GetCustomString("WinksListEmptyText")

            ElseIf (UsersListView = MessagesListViewEnum.Trash) Then
                oNoOffer.NoOfferText = Me.CurrentPageData.GetCustomString("PokesListEmptyText")
            End If

            oNoOffer.SearchOurMembersText = Me.CurrentPageData.GetCustomString("SearchOurMembersText")

            Dim os As New List(Of clsMessageUserListItem)
            os.Add(oNoOffer)

            fvNoOffers.DataSource = os
            fvNoOffers.DataBind()

            Me.MultiView1.SetActiveView(vwNoOffers)

        Else

            If (UsersListView = MessagesListViewEnum.AllMessages) Then
                Me.MultiView1.SetActiveView(vwMessages)

            ElseIf (UsersListView = MessagesListViewEnum.Inbox) Then
                Me.MultiView1.SetActiveView(vwMessages)

            ElseIf (UsersListView = MessagesListViewEnum.NewMessages) Then
                Me.MultiView1.SetActiveView(vwMessages)

            ElseIf (UsersListView = MessagesListViewEnum.SentMessages) Then
                Me.MultiView1.SetActiveView(vwMessages)

            ElseIf (UsersListView = MessagesListViewEnum.Trash) Then
                Me.MultiView1.SetActiveView(vwMessages)

            Else
                Me.MultiView1.SetActiveView(View1)

            End If
        End If




        If (Me.Repeater IsNot Nothing) Then

            Try
                For Each itm As clsMessageUserListItem In Me.UsersList
                    itm.FillTextResourceProperties(Me.CurrentPageData)
                Next
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try

            Me.Repeater.DataSource = Me.UsersList
            Me.Repeater.DataBind()
        End If

    End Sub



    Protected Function WriteOffer_MemberInfo(DataItem As Dating.Server.Site.Web.clsMessageUserListItem) As String
        If (DataItem.OtherMemberProfileID < 2) Then
            Return ""
        End If

        Dim sb As New StringBuilder()
        Try

            sb.Append("<ul class=""info2"">")

            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberAge) Then
                sb.Append("<li class=""age1"">")
                sb.Append("<b>")
                sb.Append(DataItem.OtherMemberAge)
                sb.Append(" " & DataItem.YearsOldText & " ")
                sb.Append("</b>")
                sb.Append("</li>")
            End If


            If (Not String.IsNullOrEmpty(DataItem.OtherMemberCity)) Then
                sb.Append("<li>")
                sb.Append(IIf(Not String.IsNullOrEmpty(DataItem.OtherMemberCity), DataItem.OtherMemberCity, ""))
                If (Not String.IsNullOrEmpty(DataItem.OtherMemberRegion)) Then sb.Append(",  ")
                sb.Append("</li>")
            End If

            sb.Append("<li>")
            If (Not String.IsNullOrEmpty(DataItem.OtherMemberRegion)) Then
                sb.Append(DataItem.OtherMemberRegion)
                If (Not String.IsNullOrEmpty(DataItem.OtherMemberCountry)) Then sb.Append(",  ")
            End If
            If (Not String.IsNullOrEmpty(DataItem.OtherMemberCountry)) Then
                sb.Append(ProfileHelper.GetCountryName(DataItem.OtherMemberCountry))
            End If
            sb.Append("</li>")


            sb.Replace(",  " & ",  ", ",  ")
            sb.Append("</ul>")

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Return sb.ToString()
    End Function


    Protected Function WriteSearch_MemberInfo(DataItem As Dating.Server.Site.Web.clsMessageUserListItem) As Object


        Dim sb As New StringBuilder()
        Try

            sb.Append("<ul>")

            sb.Append("<li>")

            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberAge) Then
                sb.Append("<b>" & DataItem.OtherMemberAge & "</b>")
                sb.Append(" " & DataItem.YearsOldText & " ")
            End If

            sb.Append("<b>")

            sb.Append(IIf(Not String.IsNullOrEmpty(DataItem.OtherMemberCity), DataItem.OtherMemberCity, ""))
            sb.Append(",  ")
            sb.Append(IIf(Not String.IsNullOrEmpty(DataItem.OtherMemberRegion), DataItem.OtherMemberRegion, ""))
            sb.Append(",  ")
            sb.Append(IIf(Not String.IsNullOrEmpty(DataItem.OtherMemberCountry), ProfileHelper.GetCountryName(DataItem.OtherMemberCountry), ""))
            sb.Replace(",  " & ",  ", ",  ")


            sb.Append("</b>")
            sb.Append("</li>")


            sb.Append("<li>")
            sb.Append("<b>")

            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberHeight) Then
                sb.Append(DataItem.OtherMemberHeight)
            End If

            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberHeight) AndAlso Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberPersonType) Then
                sb.Append(" - ")
            End If

            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberPersonType) Then
                sb.Append(DataItem.OtherMemberPersonType)
            End If

            sb.Append("</b>")
            sb.Append("</li>")


            sb.Append("<li>")

            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberHair) Then
                sb.Append(DataItem.OtherMemberHair)
            End If

            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberEyes) Then
                If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberHair) Then sb.Append(", ")
                sb.Append(DataItem.OtherMemberEyes)
            End If

            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberEthnicity) Then
                If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberEyes) OrElse Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberHair) Then sb.Append(", ")
                sb.Append(DataItem.OtherMemberEthnicity)
            End If

            sb.Append("</li>")
            sb.Append("</ul>")

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Return sb.ToString()
    End Function


    Protected Sub lvMsgs_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptNew.ItemDataBound

        Try

            Dim itm As clsMessageUserListItem = e.Item.DataItem
            Dim hdfProfileID As HiddenField = CType(e.Item.FindControl("hdfProfileID"), HiddenField)

            '      Dim divTooltipholder As Panel = CType(e.Item.FindControl("divTooltipholder"), Panel)
            '     Dim lnkTooltip As HyperLink = CType(e.Item.FindControl("lnkTooltip"), HyperLink)

            Dim lblCommStat As Label = CType(e.Item.FindControl("lblCommStat"), Label)
            Dim comStat As Long = 0
            Dim comIsOpen = False
            If (Not String.IsNullOrEmpty(lblCommStat.Text)) Then
                If (Long.TryParse(lblCommStat.Text, comStat)) Then
                    If (comStat > 0) Then comIsOpen = True
                End If
            End If

            If (itm.MessagesView = MessagesViewEnum.NEWMESSAGES) Then
                If (Me.IsMale) Then

                    If (hdfProfileID.Value = "1") Then
                        lblCommStat.Text = ""
                    ElseIf (comIsOpen) Then
                        lblCommStat.Text = CurrentPageData.GetCustomString("CommunicationOpen")
                        lblCommStat.CssClass = lblCommStat.CssClass.Replace("closed", "")
                        lblCommStat.CssClass = lblCommStat.CssClass & " open"
                    Else
                        lblCommStat.Text = CurrentPageData.GetCustomString("CommunicationLocked")
                        lblCommStat.CssClass = lblCommStat.CssClass.Replace("open", "")
                        lblCommStat.CssClass = lblCommStat.CssClass & " closed"
                    End If
                ElseIf (Me.IsFemale) Then

                    lblCommStat.Text = ""
                    If (comIsOpen) Then
                        lblCommStat.Text = CurrentPageData.GetCustomString("CommunicationOpen")
                        lblCommStat.CssClass = lblCommStat.CssClass.Replace("closed", "")
                        lblCommStat.CssClass = lblCommStat.CssClass & " open"
                        'lblCommStat.ForeColor = Color.Green
                    End If

                End If

            ElseIf (itm.MessagesView = MessagesViewEnum.INBOX) Then
                If (Me.IsMale) Then

                    'messages are read, don't display status of communication
                    lblCommStat.Text = ""

                ElseIf (Me.IsFemale) Then

                    lblCommStat.Text = ""
                    If (comIsOpen) Then
                        lblCommStat.Text = CurrentPageData.GetCustomString("CommunicationOpen")
                        lblCommStat.CssClass = lblCommStat.CssClass.Replace("closed", "")
                        lblCommStat.CssClass = lblCommStat.CssClass & " open"
                    End If

                End If

            ElseIf (itm.MessagesView = MessagesViewEnum.TRASH) Then
                If (Me.IsMale) Then

                    'messages are read, don't display status of communication
                    lblCommStat.Text = ""

                ElseIf (Me.IsFemale) Then

                    lblCommStat.Text = ""
                    If (comIsOpen) Then
                        lblCommStat.Text = CurrentPageData.GetCustomString("CommunicationOpen")
                        lblCommStat.CssClass = lblCommStat.CssClass.Replace("closed", "")
                        lblCommStat.CssClass = lblCommStat.CssClass & " open"
                    End If

                End If

            Else

                lblCommStat.Text = ""

                If (hdfProfileID.Value = "1") Then
                    lblCommStat.Text = ""
                    'lblCommStat.ForeColor = Color.Green
                ElseIf (comIsOpen) Then
                    lblCommStat.Text = CurrentPageData.GetCustomString("CommunicationOpen")
                    lblCommStat.CssClass = lblCommStat.CssClass.Replace("closed", "")
                    lblCommStat.CssClass = lblCommStat.CssClass & " open"
                    'lblCommStat.ForeColor = Color.Green
                End If
            End If

            Dim lblOfferAmount As Label = CType(e.Item.FindControl("lblOfferAmount"), Label)
            If (Not String.IsNullOrEmpty(lblCommStat.Text) AndAlso Not String.IsNullOrEmpty(lblOfferAmount.Text)) Then
                lblOfferAmount.Text = lblOfferAmount.Text & " / "
            End If

            Dim lblReadStat As Label = CType(e.Item.FindControl("lblReadStat"), Label)
            Dim lblSentDate As Label = CType(e.Item.FindControl("lblSentDate"), Label)

            Dim dateDescrpt As String = AppUtils.GetMessageDateDescription(itm.MAXDateTimeToCreateUtc, Me.globalStrings)
            Dim sentText As String = Me.CurrentPageData.GetCustomString("SentDateText")
            sentText = sentText.Replace("###DATE###", dateDescrpt)
            lblSentDate.Text = sentText

            If (itm.MessagesView = MessagesViewEnum.INBOX) Then
                Dim hdfMessageRead As HiddenField = CType(e.Item.FindControl("hdfMessageRead"), HiddenField)
                If (hdfMessageRead.Value = "read") Then
                    lblReadStat.CssClass = "open"
                Else
                    lblReadStat.CssClass = "closed"
                End If
            Else
                lblReadStat.Visible = False
                Dim lblSeparatorReadDate = e.Item.FindControl("lblSeparatorReadDate")
                lblSeparatorReadDate.Visible = False
            End If

            Dim warntext As String = CurrentPageData.VerifyCustomString("MessagesAllDeleteConfirmationText")
            Select Case itm.MessagesView
                Case MessagesViewEnum.INBOX
                    warntext = Regex.Replace(warntext, "###VIEW###", CurrentPageData.VerifyCustomString("lnkInbox"))

                Case MessagesViewEnum.NEWMESSAGES
                    warntext = Regex.Replace(warntext, "###VIEW###", CurrentPageData.VerifyCustomString("lnkNew"))

                Case MessagesViewEnum.SENT
                    warntext = Regex.Replace(warntext, "###VIEW###", CurrentPageData.VerifyCustomString("lnkSent"))

                Case MessagesViewEnum.TRASH
                    warntext = CurrentPageData.GetCustomString("MessagesAllDeletePermanentlyConfirmationText")
                    warntext = Regex.Replace(warntext, "###VIEW###", CurrentPageData.VerifyCustomString("lnkTrash"))

            End Select

            warntext = BasePage.ReplaceCommonTookens(warntext, itm.OtherMemberLoginName)
            warntext = AppUtils.JS_PrepareAlertString(warntext)

            Dim btnDelete As LinkButton = e.Item.FindControl("btnDeleteAllInConversation")
            btnDelete.OnClientClick = String.Format(<js><![CDATA[return confirm('{0}');]]></js>.Value, warntext)
            btnDelete.ToolTip = Me.CurrentPageData.GetCustomString("ListItemDeleteButtonText")
            btnDelete.Text = Me.CurrentPageData.GetCustomString("ListItemDeleteButtonText")


        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Protected Shared Function GetCountMessagesString(ItemsCount As Object) As String

        If (Not AppUtils.IsDBNullOrNothingOrEmpty(ItemsCount)) Then
            Try
                If (ItemsCount > 0) Then Return "(" & ItemsCount & ")"
            Catch ex As Exception

            End Try
        End If

        Return ""
    End Function


    Protected Shared Function IsVisible(ItemsCount As Object) As Boolean

        If (Not AppUtils.IsDBNullOrNothingOrEmpty(ItemsCount)) Then
            Try
                If (ItemsCount > 1) Then Return True
            Catch ex As Exception

            End Try
        End If

        Return False
    End Function



End Class
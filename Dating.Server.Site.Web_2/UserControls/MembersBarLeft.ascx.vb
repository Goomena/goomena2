﻿Imports DevExpress.Web.ASPxUploadControl
Imports System.IO
Imports System.Drawing
Imports Dating.Server.Core.DLL
Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Datasets.DLL.DSMembers
Imports DevExpress.Web.ASPxEditors
Imports DevExpress.Web.ASPxPopupControl

Public Class MembersBarLeft
    Inherits BaseUserControl

    Protected Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            'If (_pageData Is Nothing) Then _pageData = New clsPageData("master", Context)
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData("master", Context, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property

    Private Sub Page_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        Try
            If Me._pageData IsNot Nothing Then
                RemoveHandler Me._pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If

        Catch ex As Exception
        End Try
        
        Try
            MyBase.Dispose()
        Catch ex As Exception
        End Try
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try

            RefreshControl()

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Public Sub RefreshControl()
        LoadLAG()
        Dim records_ucNewWinks2 As Integer = ucNewWinks2.BindControlData()
        Dim records_ucNewMessagesQuick1 As Integer = ucNewMessagesQuick1.BindControlData()
        Dim records_ucNewOffers1 As Integer = ucNewOffers1.BindControlData()
        Dim records_ucNewDatesQuick1 As Integer = ucNewDatesQuick1.BindControlData()
        Dim tuple As Tuple(Of Integer, Integer, Integer, Integer) = New Tuple(Of Integer, Integer, Integer, Integer)(records_ucNewWinks2, records_ucNewMessagesQuick1, records_ucNewOffers1, records_ucNewDatesQuick1)

        BindMembersList(tuple)
    End Sub

    Private Sub BindMembersList(tuple As Tuple(Of Integer, Integer, Integer, Integer))
        'Tuple (records_ucNewWinks2, records_ucNewMessagesQuick1, records_ucNewOffers1, records_ucNewDatesQuick1)

        ' set links
        Dim __logdate As DateTime = DateTime.UtcNow
        Dim MasterProfileId As Integer
        Try
            MasterProfileId = CType(Me.Page, BasePage).MasterProfileId

            'Dim sqlProc As String = "EXEC GetNewCounters2 @CurrentProfileId= " & MasterProfileId
            'Dim dt As DataTable = DataHelpers.GetDataTable(sqlProc)
            'Dim textPattern As String = "#TEXT# (#COUNT#)"


            Dim result As Dating.Server.Core.DLL.clsGetMemberActionsCounters =
    clsUserDoes.GetMemberActionsCounters(MasterProfileId)

            If ((tuple.Item1 < 5 AndAlso tuple.Item1 <> result.NewLikes) OrElse
                (tuple.Item2 < 5 AndAlso tuple.Item2 <> result.NewMessages) OrElse
                (tuple.Item3 < 5 AndAlso tuple.Item3 <> result.NewOffers) OrElse
                (tuple.Item4 < 5 AndAlso tuple.Item4 <> result.NewDates)) Then
                clsUserDoes.ResetMemberCounter(MemberCountersEnum.WhoViewedMe Or
                                               MemberCountersEnum.WhoFavoritedMe Or
                                               MemberCountersEnum.WhoSharedPhotos Or
                                               MemberCountersEnum.NewDates Or
                                               MemberCountersEnum.NewLikes Or
                                               MemberCountersEnum.NewOffers Or
                                               MemberCountersEnum.NewMessages, Me.MasterProfileId)

                result = clsUserDoes.GetMemberActionsCounters(MasterProfileId)
            End If

            Dim textPattern As String = <html><![CDATA[
#TEXT# <span id="notificationsCountWrapper" class="jewelCount"><span class="countValue">#COUNT#</span></span>
]]></html>.Value

            '   Dim btnUrl As String = ""


            btnDates.Text = CurrentPageData.VerifyCustomString("btnDates")
            If (Me.SessionVariables.MemberData.Status = ProfileStatusEnum.LIMITED) Then

                btnDates.Attributes("href") = "#" ' "javascript:void(0);"
                btnDates.CssClass = btnDates.CssClass.Replace(" limited", "") & " limited"
                If (result.NewDates > 0) Then
                    btnDates.Text = textPattern.Replace("#TEXT#", btnDates.Text).Replace("#COUNT#", result.NewDates)
                    pnlDatesPopup.Visible = True
                Else
                    pnlDatesPopup.Visible = False
                End If

                Dim lnkDatesAll As HyperLink = pnlDatesPopup.FindControl("lnkDatesAll")
                If (lnkDatesAll IsNot Nothing) Then
                    lnkDatesAll.Text = "<span>" & CurrentPageData.VerifyCustomString("lnkDatesAll") & "</span>"
                    lnkDatesAll.NavigateUrl = "javascript:void(0);"
                    lnkDatesAll.CssClass = lnkDatesAll.CssClass.Replace(" limited", "") & " limited"
                    lnkDatesAll.Attributes("onclick") = ""
                End If

            Else

                If (result.NewDates > 0) Then
                    btnDates.Text = textPattern.Replace("#TEXT#", btnDates.Text).Replace("#COUNT#", result.NewDates)
                    btnDates.Attributes("href") = "#" '  "javascript:void(0);"
                    pnlDatesPopup.Visible = True

                    'Dim lnkDatesAll As HyperLink = WinksPopup.FindControl("lnkDatesAll")
                    'If (lnkDatesAll IsNot Nothing) Then
                    '    lnkDatesAll.ClientSideEvents.Click = "function(s,e){ShowLoading();}"
                    'End If
                Else
                    btnDates.Attributes.Add("onclick", "ShowLoading();")
                    btnDates.NavigateUrl = "~/Members/Dates.aspx?vw=ACCEPTED"
                    btnDates.CssClass = btnDates.CssClass.Replace("dropdown-toggle", "")
                    btnDates.Attributes.Remove("data-toggle")
                    pnlDatesPopup.Visible = False
                End If

            End If


            btnWinks.Text = CurrentPageData.VerifyCustomString("btnWinks")
            If (Me.SessionVariables.MemberData IsNot Nothing AndAlso Me.SessionVariables.MemberData.Status = ProfileStatusEnum.LIMITED) Then

                btnWinks.Attributes("href") = "#" ' "javascript:void(0);"
                btnWinks.CssClass = btnWinks.CssClass.Replace(" limited", "") & " limited"
                If (result.NewLikes > 0) Then
                    btnWinks.Text = textPattern.Replace("#TEXT#", btnWinks.Text).Replace("#COUNT#", result.NewLikes)
                    pnlWinksPopup.Visible = True
                Else
                    pnlWinksPopup.Visible = False
                End If

                Dim lnkWinksAll As HyperLink = pnlWinksPopup.FindControl("lnkWinksAll")
                If (lnkWinksAll IsNot Nothing) Then
                    lnkWinksAll.Text = "<span>" & CurrentPageData.VerifyCustomString("lnkWinksAll") & "</span>"
                    lnkWinksAll.NavigateUrl = "javascript:void(0);"
                    lnkWinksAll.CssClass = lnkWinksAll.CssClass.Replace(" limited", "") & " limited"
                    lnkWinksAll.Attributes("onclick") = ""
                End If

            Else

                If (result.NewLikes > 0) Then
                    btnWinks.Text = textPattern.Replace("#TEXT#", btnWinks.Text).Replace("#COUNT#", result.NewLikes)
                    btnWinks.Attributes("href") = "#" ' "javascript:void(0);"
                    pnlWinksPopup.Visible = True

                    'Dim lnkWinksAll As HyperLink = WinksPopup.FindControl("lnkWinksAll")
                    'If (lnkWinksAll IsNot Nothing) Then
                    '    lnkWinksAll.ClientSideEvents.Click = "function(s,e){ShowLoading();}"
                    'End If
                Else
                    btnWinks.Attributes.Add("onclick", "ShowLoading();")
                    btnWinks.NavigateUrl = "~/Members/Likes.aspx?vw=likes"
                    btnWinks.CssClass = btnWinks.CssClass.Replace("dropdown-toggle", "")
                    btnWinks.Attributes.Remove("data-toggle")
                    pnlWinksPopup.Visible = False
                End If

            End If


            btnOffers.Text = CurrentPageData.VerifyCustomString("btnOffers")
            If (Me.SessionVariables.MemberData IsNot Nothing AndAlso Me.SessionVariables.MemberData.Status = ProfileStatusEnum.LIMITED) Then

                btnOffers.Attributes("href") = "#" '  "javascript:void(0);"
                btnOffers.CssClass = btnOffers.CssClass.Replace(" limited", "") & " limited"
                If (result.NewOffers > 0) Then
                    btnOffers.Text = textPattern.Replace("#TEXT#", btnOffers.Text).Replace("#COUNT#", result.NewOffers)
                    pnlOffersPopup.Visible = True
                Else
                    pnlOffersPopup.Visible = False
                End If

                Dim lnkOffersAll As HyperLink = pnlOffersPopup.FindControl("lnkOffersAll")
                If (lnkOffersAll IsNot Nothing) Then
                    lnkOffersAll.Text = "<span>" & CurrentPageData.VerifyCustomString("lnkOffersAll") & "</span>"
                    lnkOffersAll.NavigateUrl = "javascript:void(0);"
                    lnkOffersAll.CssClass = lnkOffersAll.CssClass.Replace(" limited", "") & " limited"
                    lnkOffersAll.Attributes("onclick") = ""
                End If

            Else
                If (result.NewOffers > 0) Then
                    btnOffers.Text = textPattern.Replace("#TEXT#", btnOffers.Text).Replace("#COUNT#", result.NewOffers)
                    btnOffers.Attributes("href") = "#" ' "javascript:void(0);"
                    pnlOffersPopup.Visible = True

                    'Dim lnkOffersAll As HyperLink = WinksPopup.FindControl("lnkOffersAll")
                    'If (lnkOffersAll IsNot Nothing) Then
                    '    lnkOffersAll.ClientSideEvents.Click = "function(s,e){ShowLoading();}"
                    'End If
                Else
                    ' find navigate url from footer link button
                    btnOffers.Attributes.Add("onclick", "ShowLoading();")
                    btnOffers.NavigateUrl = "~/Members/Offers3.aspx?vw=newoffers"
                    btnOffers.CssClass = btnOffers.CssClass.Replace("dropdown-toggle", "")
                    btnOffers.Attributes.Remove("data-toggle")
                    pnlOffersPopup.Visible = False
                End If
            End If


            btnMessages.Text = CurrentPageData.VerifyCustomString("btnMessages")
            If (Me.SessionVariables.MemberData IsNot Nothing AndAlso Me.SessionVariables.MemberData.Status = ProfileStatusEnum.LIMITED) Then

                btnMessages.Attributes("href") = "#" ' "javascript:void(0);"
                btnMessages.CssClass = btnMessages.CssClass.Replace(" limited", "") & " limited"
                If (result.NewMessages > 0) Then
                    btnMessages.Text = textPattern.Replace("#TEXT#", btnMessages.Text).Replace("#COUNT#", result.NewMessages)
                    pnlMessagesPopup.Visible = True
                Else
                    pnlMessagesPopup.Visible = False
                End If

                Dim lnkMessagesAll As HyperLink = pnlMessagesPopup.FindControl("lnkMessagesAll")
                If (lnkMessagesAll IsNot Nothing) Then
                    lnkMessagesAll.Text = "<span>" & CurrentPageData.VerifyCustomString("lnkMessagesAll") & "</span>"
                    lnkMessagesAll.NavigateUrl = "javascript:void(0);"
                    lnkMessagesAll.CssClass = lnkMessagesAll.CssClass.Replace(" limited", "") & " limited"
                    lnkMessagesAll.Attributes("onclick") = ""
                End If

            Else

                If (result.NewMessages > 0) Then
                    btnMessages.Text = textPattern.Replace("#TEXT#", btnMessages.Text).Replace("#COUNT#", result.NewMessages)
                    btnMessages.Attributes("href") = "#" ' "javascript:void(0);"
                    pnlMessagesPopup.Visible = True

                    'Dim lnkMessagesAll As HyperLink = WinksPopup.FindControl("lnkMessagesAll")
                    'If (lnkMessagesAll IsNot Nothing) Then
                    '    lnkMessagesAll.ClientSideEvents.Click = "function(s,e){ShowLoading();}"
                    'End If
                Else
                    btnMessages.Attributes.Add("onclick", "ShowLoading();")
                    btnMessages.NavigateUrl = "~/Members/Messages.aspx"
                    btnMessages.CssClass = btnMessages.CssClass.Replace("dropdown-toggle", "")
                    btnMessages.Attributes.Remove("data-toggle")
                    pnlMessagesPopup.Visible = False
                End If

            End If

            'btnUrl = btnMessages.NavigateUrl.TrimStart("~"c).ToUpper()
            'If (btnUrl.StartsWith(Request.Url.PathAndQuery.ToUpper())) Then
            '    btnMessages.CssClass = btnMessages.CssClass & " selectedBtn"
            'Else
            '    btnMessages.CssClass = btnMessages.CssClass.Replace(" selectedBtn", "")
            'End If



            lnkCredits.Visible = False
            If (Me.IsMale) Then

                '   Dim AvailableCredits As Integer
                lnkGooMatch.Visible = True
                Dim __cac As clsCustomerAvailableCredits = BasePage.GetCustomerAvailableCredits(MasterProfileId)
                If (__cac.CreditsRecordsCount > 0) Then

                    '      AvailableCredits = __cac.AvailableCredits
                    lnkCredits.Visible = True

                    lnkCredits.Text = CurrentPageData.VerifyCustomString("lnkCredits")
                    If (__cac.AvailableCredits > 0) Then
                        lnkCredits.Text = textPattern.Replace("#TEXT#", lnkCredits.Text).Replace("#COUNT#", __cac.AvailableCredits)
                    End If

                    'If (Session("CustomerAvailableCredits") Is Nothing) Then
                    '    DataHelpers.Update_EUS_Profiles_UpdateAvailableCredits(MasterProfileId, AvailableCredits)
                    'End If
                    lnkCredits.Text = lnkCredits.Text.Replace(" class=""jewelCount""", "")
                    lnkCredits.Attributes.Add("onclick", "ShowLoading();")

                ElseIf (__cac.HasSubscription) Then

                    lnkCredits.Visible = True
                    lnkCredits.Attributes.Add("onclick", "ShowLoading();")

                    'Else
                    '    lnkCredits.Visible = False
                End If


                'btnUrl = lnkCredits.NavigateUrl.TrimStart("~"c).ToUpper()
                'If (btnUrl.StartsWith(Request.Url.PathAndQuery.ToUpper())) Then
                '    lnkCredits.CssClass = lnkCredits.CssClass & " selectedBtn"
                'Else
                '    lnkCredits.CssClass = lnkCredits.CssClass.Replace(" selectedBtn", "")
                'End If
            Else
                lnkGooMatch.Visible = False
            End If




        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        Finally
            clsLogger.InsertLog("Members-Master-->BindMembersList", MasterProfileId, (DateTime.UtcNow - __logdate).TotalMilliseconds)
        End Try

    End Sub

    Private Sub LoadLAG()
        Try

            Dim lnkOffersAll As HyperLink = pnlOffersPopup.FindControl("lnkOffersAll")
            If (lnkOffersAll IsNot Nothing) Then
                lnkOffersAll.Text = "<span>" & CurrentPageData.VerifyCustomString("lnkOffersAll") & "</span>"
            End If

            Dim msg_OffersHeader As Literal = pnlOffersPopup.FindControl("msg_OffersHeader")
            If (msg_OffersHeader IsNot Nothing) Then
                msg_OffersHeader.Text = CurrentPageData.VerifyCustomString("msg_OffersHeader")
            End If

            Dim lnkMessagesAll As HyperLink = pnlMessagesPopup.FindControl("lnkMessagesAll")
            If (lnkMessagesAll IsNot Nothing) Then
                lnkMessagesAll.Text = "<span>" & CurrentPageData.VerifyCustomString("lnkMessagesAll") & "</span>"
            End If

            Dim msg_MessagesHeader As Literal = pnlMessagesPopup.FindControl("msg_MessagesHeader")
            If (msg_MessagesHeader IsNot Nothing) Then
                msg_MessagesHeader.Text = CurrentPageData.VerifyCustomString("msg_MessagesHeader")
            End If

            Dim lnkDatesAll As HyperLink = pnlDatesPopup.FindControl("lnkDatesAll")
            If (lnkDatesAll IsNot Nothing) Then
                lnkDatesAll.Text = "<span>" & CurrentPageData.VerifyCustomString("lnkDatesAll") & "</span>"
            End If

            Dim msg_DatesHeader As Literal = pnlDatesPopup.FindControl("msg_DatesHeader")
            If (msg_DatesHeader IsNot Nothing) Then
                msg_DatesHeader.Text = CurrentPageData.VerifyCustomString("msg_DatesHeader")
            End If


            Dim msg_WinksHeader As Literal = pnlWinksPopup.FindControl("msg_WinksHeader")
            If (msg_WinksHeader IsNot Nothing) Then
                msg_WinksHeader.Text = CurrentPageData.VerifyCustomString("msg_WinksHeader")
            End If

            Dim lnkWinksAll As HyperLink = pnlWinksPopup.FindControl("lnkWinksAll")
            If (lnkWinksAll IsNot Nothing) Then
                lnkWinksAll.Text = "<span>" & CurrentPageData.VerifyCustomString("lnkWinksAll") & "</span>"
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


End Class
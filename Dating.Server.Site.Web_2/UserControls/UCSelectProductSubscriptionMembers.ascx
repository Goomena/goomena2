﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UCSelectProductSubscriptionMembers.ascx.vb" 
    Inherits="Dating.Server.Site.Web.UCSelectProductSubscriptionMembers" %>
<%@ Register src="UCPriceSubscription.ascx" tagname="UCPriceSubscription" tagprefix="uc1" %>
<%@ Register src="UCPriceCredits.ascx" tagname="UCPriceCredits" tagprefix="uc1" %>

<%--<script src="//cdn.goomena.com/Scripts/hvreffcts/js/modernizr.custom.js" type="text/javascript"></script>
<script src="//cdn.goomena.com/Scripts/hvreffcts/js/toucheffects.js" type="text/javascript"></script>--%>
<%--<script src="/Scripts/hvreffcts/js/modernizr.custom.js" type="text/javascript"></script>
<script src="/Scripts/hvreffcts/js/toucheffects.js" type="text/javascript"></script>--%>
<link rel="stylesheet" href="/v1/css/payments3.css?v=1" />
<style type="text/css">
    .vat-info { font-size:12px;font-weight:bold;margin:10px 0;
                border-bottom: 1px solid #bababc;
                padding-bottom: 3px;}
</style>
<script type="text/javascript">
    $(function () {
        $('.product-1.product-tab', '#products_sub').click(function () { $('.pricecol-order-btn', '#products_sub .product-1')[0].click(); })
        $('.product-2.product-tab', '#products_sub').click(function () { $('.pricecol-order-btn', '#products_sub .product-2')[0].click(); })
        $('.product-3.product-tab', '#products_sub').click(function () { $('.pricecol-order-btn', '#products_sub .product-3')[0].click(); })
        $('.product-4.product-tab', '#products_sub').click(function () { $('.pricecol-order-btn', '#products_sub .product-4')[0].click(); })

        $('.product-1.product-tab', '#products_credits').click(function () { $('.pricecol-order-btn', '#products_credits .product-1')[0].click(); })
        $('.product-2.product-tab', '#products_credits').click(function () { $('.pricecol-order-btn', '#products_credits .product-2')[0].click(); })
        $('.product-3.product-tab', '#products_credits').click(function () { $('.pricecol-order-btn', '#products_credits .product-3')[0].click(); })
        $('.product-5.product-tab', '#products_credits').click(function () { $('.pricecol-order-btn', '#products_credits .product-5')[0].click(); })

        setTimeout(function () {
            new Image().src = '//cdn.goomena.com/images2/payment3/30days.png';
            new Image().src = '//cdn.goomena.com/images2/payment3/90days.png';
            new Image().src = '//cdn.goomena.com/images2/payment3/180days.png';
            new Image().src = '//cdn.goomena.com/images2/payment3/360days.png';
        }, 500);

        $('.product-1', '#products_sub')
            .mouseover(function (e) { $('figcaption', '#products_sub').addClass('ph-1'); })
            .mouseleave(function () { $('figcaption', '#products_sub').removeClass('ph-1'); });
        $('.product-2', '#products_sub')
            .mouseover(function (e) { $('figcaption', '#products_sub').addClass('ph-2'); })
            .mouseleave(function () { $('figcaption', '#products_sub').removeClass('ph-2'); });
        $('.product-3', '#products_sub')
            .mouseover(function (e) { $('figcaption', '#products_sub').addClass('ph-3'); })
            .mouseleave(function () { $('figcaption', '#products_sub').removeClass('ph-3'); });
        $('.product-4', '#products_sub')
            .mouseover(function (e) { $('figcaption', '#products_sub').addClass('ph-4'); })
            .mouseleave(function () { $('figcaption', '#products_sub').removeClass('ph-4'); });


        $('.product-1', '#products_credits')
            .mouseover(function (e) { $('figcaption', '#products_credits').addClass('ph-1'); })
            .mouseleave(function () { $('figcaption', '#products_credits').removeClass('ph-1'); });
        $('.product-2', '#products_credits')
            .mouseover(function (e) { $('figcaption', '#products_credits').addClass('ph-2'); })
            .mouseleave(function () { $('figcaption', '#products_credits').removeClass('ph-2'); });
        $('.product-3', '#products_credits')
            .mouseover(function (e) { $('figcaption', '#products_credits').addClass('ph-3'); })
            .mouseleave(function () { $('figcaption', '#products_credits').removeClass('ph-3'); });
        $('.product-5', '#products_credits')
            .mouseover(function (e) { $('figcaption', '#products_credits').addClass('ph-5'); })
            .mouseleave(function () { $('figcaption', '#products_credits').removeClass('ph-5'); });
    })
</script>
<div id="payment_2" runat="server" clientidmode="Static">
    <div class="info-content">
        <div>
            <asp:Label ID="lblPaymentsGuarantee" runat="server" Text=""></asp:Label>
        </div>
        <asp:Panel ID="pnlBonus" runat="server" CssClass="bonus-code">
            <div align="center">
                <asp:Label ID="lblBonusCode" runat="server" Text="" style="color:#25AAE1;"/>
                <asp:Label ID="lblBonusCodeSuccess" runat="server" ForeColor="Green" 
                    ViewStateMode="Disabled"/>
                <div style="height:20px"></div>
                <asp:TextBox ID="txtBonusCode" runat="server" CssClass="textbox"></asp:TextBox>
                <asp:Button ID="btnBonusCredits" runat="server" Text="Υποβολή κωδικού" CssClass="button" />
                <div style="height:20px"></div>
                <asp:Label ID="lblBonusCodeError" runat="server" Text="" ForeColor="red" 
                    ViewStateMode="Disabled"/>
            </div>
        </asp:Panel>
    </div>


    <div id="products_2">

        <div class="sub-content">
            <asp:Label ID="lblSubscrHeader" runat="server" Text="" CssClass="product-header"></asp:Label>

            <div id="products_sub">
                <%--<div class="product-hover"></div>--%>
                <figure class="product-hover">
	                <figcaption>
	                </figcaption>
                </figure>
                <div class="product-prices">
                    <uc1:UCPriceSubscription ID="UCPrice1Sub" runat="server" CssClass="product-1"  />
                    <uc1:UCPriceSubscription ID="UCPrice2Sub" runat="server" CssClass="product-2"  />
                    <uc1:UCPriceSubscription ID="UCPrice3Sub" runat="server" CssClass="product-3"  />
                    <uc1:UCPriceSubscription ID="UCPrice4Sub" runat="server" CssClass="product-4"  />
                </div>
            </div>
            <asp:Label ID="lblSubscrInfo" runat="server" Text="" CssClass="sub-info"></asp:Label>
        </div>

        <div class="credits-content">
            <asp:Label ID="lblCreditsHeader" runat="server" Text="" CssClass="product-header"></asp:Label>
            <div id="products_credits">
                <%--<div class="product-hover"></div>--%>
                <figure class="product-hover">
	                <figcaption>
	                </figcaption>
                </figure>
                <div class="product-prices">
                    <uc1:UCPriceCredits ID="UCPrice1Crd" runat="server" View="vwMember" CssClass="product-1"  />
                    <uc1:UCPriceCredits ID="UCPrice2Crd" runat="server" View="vwMember" CssClass="product-2" Discount="30"  />
                    <uc1:UCPriceCredits ID="UCPrice3Crd" runat="server" View="vwMember" CssClass="product-3" IsRecommended="true" Discount="45"   />
                    <uc1:UCPriceCredits ID="UCPrice5Crd" runat="server" View="vwMember" CssClass="product-5" />
                </div>
            </div>
        </div>

        <div class="paymentsimgs info-content">
            <img ID="imgPaymentSigns" runat="server" src="//cdn.goomena.com/goomena-kostas/pricing-members/payment2.png?v2" alt="Logos" />
        </div>
    </div>

    <div class="info-content">
        <asp:Literal runat="server" ID="lbfootText">
        </asp:Literal>
    </div>

</div>


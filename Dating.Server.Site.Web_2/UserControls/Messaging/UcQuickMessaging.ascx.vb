﻿Imports DevExpress.Web.ASPxPopupControl

Public Class UcQuickMessaging
    Inherits System.Web.UI.UserControl


    Public ReadOnly Property PopupControl As ASPxPopupControl
        Get
            Return popupConversation
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub


    Public Shared Function OnShowConversationFunc(contentUrl As String, headerText As String,
                                               Optional width As Integer? = Nothing,
                                               Optional height As Integer? = Nothing,
                                               Optional params As String = Nothing) As String

        Dim input As String =
            "OnShowConversation('[contentUrl]', this, '[headerText]'," &
            If(width.HasValue, width.Value, "null") & "," &
            If(height.HasValue, height.Value, "null") & "," &
            If(Not String.IsNullOrEmpty(params), "'" & params & "'", "null") & "); return false;"

        contentUrl = contentUrl.Replace("'", "\'")
        If (Not String.IsNullOrEmpty(headerText)) Then headerText = headerText.Replace("'", "\'")
        input = input.Replace("[contentUrl]", contentUrl)
        input = input.Replace("[headerText]", headerText)
        Return input
    End Function

    Public Shared Function OnShowConversationFunc(input As String, contentUrl As String, headerText As String) As String
        contentUrl = contentUrl.Replace("'", "\'")
        If (Not String.IsNullOrEmpty(headerText)) Then headerText = headerText.Replace("'", "\'")
        input = input.Replace("[contentUrl]", contentUrl)
        input = input.Replace("[headerText]", headerText)
        Return input
    End Function


End Class
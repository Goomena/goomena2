﻿Imports DevExpress.Web.ASPxEditors

Public Class AgeDropDown
    Inherits System.Web.UI.UserControl


    'Public Property Style As String
    '    Get
    '        If (Not ViewState("Style") Is Nothing) Then
    '            Return ViewState("Style").ToString()
    '        End If
    '        Return ""
    '    End Get
    '    Set(ByVal value As String)
    '        ViewState("Style") = value
    '    End Set
    'End Property


    Public Property CssClass As String
        Get
            If (ViewState("CssClass") IsNot Nothing) Then
                Return ViewState("CssClass").ToString()
            End If
            Return ""
        End Get
        Set(ByVal value As String)
            ViewState("CssClass") = value
        End Set
    End Property


    Public Property DefaultValue As String
        Get
            If (Not ViewState("DefaultValue") Is Nothing) Then
                Return ViewState("DefaultValue").ToString()
            End If
            Return Nothing
        End Get
        Set(ByVal value As String)

            If (ViewState("DefaultValue") <> value) Then

                ViewState("DefaultValue") = value
                fillDdl()
                If (value IsNot Nothing) Then
                    Dim li As DevExpress.Web.ASPxEditors.ListEditItem = Me.ddl.Items.FindByValue(value.ToString())
                    If (li IsNot Nothing) Then
                        Me.ddl.SelectedIndex = -1
                        Me.ddl.SelectedItem = li
                    ElseIf Me.ddl.Items(0).Value <> "-1" Then
                        Me.ddl.Items.Insert(0, New ListEditItem(value, "-1"))
                        Me.ddl.SelectedIndex = 0
                    End If

                End If

            End If

        End Set
    End Property


    Public Property SelectedValue As String
        Get

            If (ddl.SelectedIndex > -1) Then
                Return ddl.Items(ddl.SelectedIndex).Value
            End If
            Return Nothing

        End Get
        Set(ByVal value As String)

            If (Not value Is Nothing) Then
                Dim itm As ListEditItem = Me.ddl.Items.FindByValue(value)
                If (Not itm Is Nothing) Then
                    itm.Selected = True
                End If
            End If

        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If (Not Me.IsPostBack) Then
                fillDdl()
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try
    End Sub

    Protected Overrides Sub OnPreRender(ByVal e As System.EventArgs)
        MyBase.OnPreRender(e)

        'If (Not String.IsNullOrEmpty(Me.Style)) Then
        '    Me.ddl.Attributes.Add("style", Me.Style)
        'End If

        Me.ddl.CssClass = Me.CssClass
    End Sub

    Sub fillDdl()
        If (ddl.Items.Count = 0) Then

            Dim startDate As Integer = 18
            Dim endDate As Integer = 120
            Dim y As Integer

            For y = startDate To endDate
                ddl.Items.Add(y, y)
            Next
        End If
    End Sub
End Class
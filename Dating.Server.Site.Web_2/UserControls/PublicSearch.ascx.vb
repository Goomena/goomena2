﻿Imports DevExpress.Web.ASPxPager
Imports Dating.Server.Core.DLL

Public Class PublicSearch
    Inherits BaseUserControl




    Public Property InfoWinUrl As String
    Public Property InfoWinHeaderText As String

    Public Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            'If (_pageData Is Nothing) Then _pageData = New clsPageData("control.OfferControl", Context)
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData("control.OfferControl", Context, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property
    Private Sub Page_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        Try
            If Me._pageData IsNot Nothing Then
                RemoveHandler Me._pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If

        Catch ex As Exception
        End Try

        Try
            MyBase.Dispose()
        Catch ex As Exception
        End Try
    End Sub

    Public Property ShowNoPhotoText As Boolean
    Public Property ShowEmptyListText As Boolean
    Private IsUserAuthenicated As Boolean


    Private _list As List(Of clsWinkUserListItem)
    Public Property UsersList As List(Of clsWinkUserListItem)
        Get
            If (Me._list Is Nothing) Then Me._list = New List(Of clsWinkUserListItem)
            Return Me._list
        End Get
        Set(value As List(Of clsWinkUserListItem))
            Me._list = value
        End Set
    End Property



    <PersistenceMode(PersistenceMode.InnerDefaultProperty)>
    Public ReadOnly Property Repeater As Repeater
        Get
            Return rptPubSrch
        End Get
    End Property


    Protected Overrides Sub OnInit(e As System.EventArgs)
        MyBase.OnInit(e)
    End Sub


    Public Property MykeyStrings As New KeyStrings
    Public Function GetKeys() As KeyStrings
        If MykeyStrings Is Nothing Then
            MykeyStrings = New KeyStrings
        End If
        Return MykeyStrings
    End Function
    Public Overrides Sub DataBind()

        If (Me.Repeater IsNot Nothing) Then

            IsUserAuthenicated = clsCurrentContext.VerifyLogin()

            Try
                FillKeyStrings()
                For Each itm As clsWinkUserListItem In Me.UsersList
                    itm.FillTextResourceProperties(MykeyStrings)
                    If (itm.ZodiacString Is Nothing AndAlso itm.OtherMemberBirthday.HasValue AndAlso itm.OtherMemberBirthday.Value > DateTime.MinValue) Then
                        Dim tmpKeystr As String = "Zodiac" & ProfileHelper.ZodiacName(itm.OtherMemberBirthday)
                        If MykeyStrings.ZodiacNames.ContainsKey(tmpKeystr & itm.LAGID) Then
                            itm.ZodiacString = MykeyStrings.ZodiacNames(tmpKeystr & itm.LAGID)
                        Else
                            itm.ZodiacString = globalStrings.GetCustomString(tmpKeystr, itm.LAGID)
                            MykeyStrings.ZodiacNames.Add(tmpKeystr & itm.LAGID, itm.ZodiacString)
                        End If
                    End If
                Next
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try

            Me.Repeater.DataSource = Me.UsersList
            Me.Repeater.DataBind()
        End If

    End Sub
    Public Function FillKeyStrings(Optional ByVal Strings As KeyStrings = Nothing, Optional ByVal pageData As clsPageData = Nothing) As Boolean

        If pageData Is Nothing Then
            pageData = CurrentPageData
        End If
        If Strings Is Nothing Then
            Strings = MykeyStrings
        End If
        If (String.IsNullOrEmpty(Strings.SendMessageOnceText)) Then Strings.SendMessageOnceText = pageData.GetCustomString("SendMessageOnceText")
        If (String.IsNullOrEmpty(Strings.SendMessageManyText)) Then Strings.SendMessageManyText = pageData.GetCustomString("SendMessageManyText")
        If (String.IsNullOrEmpty(Strings.UnlockNotice)) Then Strings.UnlockNotice = pageData.GetCustomString("UnlockNotice")
        If (String.IsNullOrEmpty(Strings.ActionsUnlockText)) Then Strings.ActionsUnlockText = pageData.GetCustomString("ActionsUnlockText")
        If (String.IsNullOrEmpty(Strings.OfferAcceptedUnlockText)) Then Strings.OfferAcceptedUnlockText = pageData.GetCustomString("OfferAcceptedUnlockText")
        If (String.IsNullOrEmpty(Strings.OfferAcceptedHowToContinueText)) Then Strings.OfferAcceptedHowToContinueText = pageData.GetCustomString("OfferAcceptedHowToContinueText")
        If (String.IsNullOrEmpty(Strings.WillYouAcceptDateWithForAmountText)) Then Strings.WillYouAcceptDateWithForAmountText = pageData.GetCustomString("WillYouAcceptDateWithForAmountText")
        If (String.IsNullOrEmpty(Strings.YouReceivedPokeDescriptionText)) Then Strings.YouReceivedPokeDescriptionText = pageData.GetCustomString("YouReceivedPokeDescriptionText")
        If (String.IsNullOrEmpty(Strings.OfferDeleteConfirmMessage)) Then Strings.OfferDeleteConfirmMessage = pageData.GetCustomString("OfferDeleteConfirmMessage")
        If (String.IsNullOrEmpty(Strings.MilesAwayText)) Then Strings.MilesAwayText = pageData.GetCustomString("MilesAwayText")
        If (String.IsNullOrEmpty(Strings.CancelledRejectedDescriptionText)) Then Strings.CancelledRejectedDescriptionText = pageData.GetCustomString("YouCancelledWinkToText")
        If (String.IsNullOrEmpty(Strings.RejectBlockText)) Then Strings.RejectBlockText = pageData.GetCustomString("RejectBlockText")
        If (String.IsNullOrEmpty(Strings.RejectDeleteConversationText)) Then Strings.RejectDeleteConversationText = pageData.GetCustomString("RejectDeleteConversationText")
        If (String.IsNullOrEmpty(Strings.RejectDeleteConversationText)) Then Strings.RejectDeleteConversationText = pageData.GetCustomString("RejectDeleteConversationText")
        If (String.IsNullOrEmpty(Strings.IsOnlinetext)) Then Strings.IsOnlinetext = pageData.GetCustomString("Online.Now")
        If (String.IsNullOrEmpty(Strings.IsOnlineRecentlyText)) Then Strings.IsOnlineRecentlyText = pageData.GetCustomString("Online.Recently")
        'If (String.IsNullOrEmpty(Strings.HasNoPhotosText)) Then Strings.HasNoPhotosText = pageData.GetCustomString("HasNoPhotosText")
        'If (String.IsNullOrEmpty(Strings.SearchOurMembersText)) Then Strings.SearchOurMembersText = pageData.GetCustomString("SearchOurMembersText")
        'If (String.IsNullOrEmpty(Strings.YouReceivedWinkOfferText)) Then Strings.YouReceivedWinkOfferText = pageData.GetCustomString("YouReceivedWinkText")
        'If (String.IsNullOrEmpty(Strings.YearsOldText)) Then Strings.YearsOldText = pageData.GetCustomString("YearsOldText")
        'If (String.IsNullOrEmpty(Strings.WantToKnowFirstDatePriceText)) Then Strings.WantToKnowFirstDatePriceText = pageData.GetCustomString("WantToKnowFirstDatePriceText")
        'If (String.IsNullOrEmpty(Strings.MakeOfferText)) Then Strings.MakeOfferText = pageData.GetCustomString("MakeOfferText")
        'If (String.IsNullOrEmpty(Strings.AcceptText)) Then Strings.AcceptText = pageData.GetCustomString("AcceptText")
        'If (String.IsNullOrEmpty(Strings.CounterText)) Then Strings.CounterText = pageData.GetCustomString("CounterText")
        'If (String.IsNullOrEmpty(Strings.NotInterestedText)) Then Strings.NotInterestedText = pageData.GetCustomString("NotInterestedText")
        'If (String.IsNullOrEmpty(Strings.TooFarAwayText)) Then Strings.TooFarAwayText = pageData.GetCustomString("TooFarAwayText")
        'If (String.IsNullOrEmpty(Strings.NotEnoughInfoText)) Then Strings.NotEnoughInfoText = pageData.GetCustomString("NotEnoughInfoText")
        'If (String.IsNullOrEmpty(Strings.DifferentExpectationsText)) Then Strings.DifferentExpectationsText = pageData.GetCustomString("DifferentExpectationsText")
        'If (String.IsNullOrEmpty(Strings.DeleteOfferText)) Then Strings.DeleteOfferText = pageData.GetCustomString("DeleteOfferText_ND")
        'If (String.IsNullOrEmpty(Strings.DeleteWinkText)) Then Strings.DeleteWinkText = pageData.GetCustomString("DeleteWinkText")
        'If (String.IsNullOrEmpty(Strings.SendMessageText)) Then Strings.SendMessageText = pageData.GetCustomString("SendMessageText_ND")
        If (String.IsNullOrEmpty(Strings.WinkText)) Then Strings.WinkText = pageData.GetCustomString("WinkText")
        ' If (String.IsNullOrEmpty(Strings.UnWinkText)) Then Strings.UnWinkText = pageData.GetCustomString("UnWinkText")
        If (String.IsNullOrEmpty(Strings.FavoriteText)) Then Strings.FavoriteText = pageData.GetCustomString("FavoriteText")
        'If (String.IsNullOrEmpty(Strings.UnfavoriteText)) Then Strings.UnfavoriteText = pageData.GetCustomString("UnfavoriteText")
        'If (String.IsNullOrEmpty(Strings.YearsOldFromText)) Then Strings.YearsOldFromText = pageData.GetCustomString("YearsOldFromText")
        'If (String.IsNullOrEmpty(Strings.TryWinkText)) Then Strings.TryWinkText = pageData.GetCustomString("TryWinkText")
        'If (String.IsNullOrEmpty(Strings.MakeNewOfferText)) Then Strings.MakeNewOfferText = pageData.GetCustomString("MakeNewOfferText")
        'If (String.IsNullOrEmpty(Strings.YourWinkSentText)) Then Strings.YourWinkSentText = pageData.GetCustomString("YourWinkSentText")
        'If (String.IsNullOrEmpty(Strings.AwaitingResponseText)) Then Strings.AwaitingResponseText = pageData.GetCustomString("AwaitingResponseText")
        'If (String.IsNullOrEmpty(Strings.AddPhotosText)) Then Strings.AddPhotosText = pageData.GetCustomString("AddPhotosText")
        'If (String.IsNullOrEmpty(Strings.UnblockText)) Then Strings.UnblockText = pageData.GetCustomString("UnblockText")
        ' If (String.IsNullOrEmpty(Strings.PokeText)) Then Strings.PokeText = pageData.GetCustomString("PokeText")
        If (String.IsNullOrEmpty(Strings.ActionsText)) Then Strings.ActionsText = pageData.GetCustomString("ActionsText")
        'If (String.IsNullOrEmpty(Strings.CancelPokeText)) Then Strings.CancelPokeText = pageData.GetCustomString("CancelPokeText")
        'If (String.IsNullOrEmpty(Strings.CancelWinkText)) Then Strings.CancelWinkText = pageData.GetCustomString("CancelWinkText_ND")
        'If (String.IsNullOrEmpty(Strings.CancelOfferText)) Then Strings.CancelOfferText = pageData.GetCustomString("CancelOfferText_ND")
        'If (String.IsNullOrEmpty(Strings.RejectText)) Then Strings.RejectText = pageData.GetCustomString("RejectText")
        'If (String.IsNullOrEmpty(Strings.CancelledRejectedTitleText)) Then Strings.CancelledRejectedTitleText = pageData.GetCustomString("WinkCancelledText")
        'If (String.IsNullOrEmpty(Strings.MessageSentNotice)) Then Strings.MessageSentNotice = pageData.GetCustomString("MessageSentNotice")
        'If (String.IsNullOrEmpty(Strings.ConversationText)) Then Strings.ConversationText = pageData.GetCustomString("ConversationText")
        'If (String.IsNullOrEmpty(Strings.HistoryText)) Then Strings.HistoryText = pageData.GetCustomString("HistoryText")
        'If (String.IsNullOrEmpty(Strings.CommunicationStatusText)) Then Strings.CommunicationStatusText = pageData.GetCustomString("CommunicationStatusText")
        'If (String.IsNullOrEmpty(Strings.WhatIsText)) Then Strings.WhatIsText = pageData.GetCustomString("WhatIsText")
    End Function

    Protected Function WriteOffer_MemberInfo(DataItem As Dating.Server.Site.Web.clsWinkUserListItem) As Object

        Dim sb As New StringBuilder()
        Try
            sb.Append("<ul>")
            sb.Append("<li>")

            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberAge) Then
                sb.Append("<div class=""pr-age"">")
                sb.Append(DataItem.OtherMemberAge)
                sb.Append(" " & Me.MykeyStrings.YearsOldText & " ")
                sb.Append("</div>")
            End If

            sb.Append("</li>")


            sb.Append("<li>")
            sb.Append("<b>")

            'If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberCity) Then
            '    sb.Append(DataItem.OtherMemberCity)
            'End If

            'If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberCity) AndAlso Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberRegion) Then
            '    sb.Append(",  ")
            'End If

            'If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberRegion) Then
            '    sb.Append(DataItem.OtherMemberRegion)
            'End If
            sb.Append(IIf(Not String.IsNullOrEmpty(DataItem.OtherMemberCity), DataItem.OtherMemberCity, ""))
            sb.Append(",  ")
            sb.Append(IIf(Not String.IsNullOrEmpty(DataItem.OtherMemberRegion), DataItem.OtherMemberRegion, ""))
            sb.Append(",  ")
            sb.Append(IIf(Not String.IsNullOrEmpty(DataItem.OtherMemberCountry), ProfileHelper.GetCountryName(DataItem.OtherMemberCountry), ""))
            sb.Replace(",  " & ",  ", ",  ")

            sb.Append("</b>")
            sb.Append("</li>")


            sb.Append("</ul>")

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Return sb.ToString()
    End Function


    Protected Function WriteSearch_MemberInfo(DataItem As Dating.Server.Site.Web.clsWinkUserListItem) As Object

        'Dim str As String = "<ul>" & vbCrLf & _
        '    "<li><b>" & DataItem.OtherMemberAge & "</b> " & DataItem.YearsOldText & " <b>" & Eval("OtherMemberCity & ", " & Eval("OtherMemberRegion & "</b></li>" & vbCrLf & _
        '    "<li><b>" & DataItem.OtherMemberHeight & " - " & DataItem.OtherMemberPersonType & "</b></li>" & vbCrLf & _
        '    "<li>" & DataItem.OtherMemberHair & ", " & DataItem.OtherMemberEyes & ", " & DataItem.OtherMemberEthnicity & "</li>" & vbCrLf & _
        '    "</ul>" & vbCrLf

        Dim sb As New StringBuilder()
        Try

            sb.Append("<ul>")

            sb.Append("<li>")

            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberAge) Then
                sb.Append("<div class=""pr-age"">")
                sb.Append(DataItem.OtherMemberAge)
                sb.Append(" " & Me.MykeyStrings.YearsOldText & " ")
                sb.Append("</div>")
            End If

            'If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberCity) Then
            '    sb.Append(DataItem.OtherMemberCity)
            'End If

            'If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberCity) AndAlso Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberRegion) Then
            '    sb.Append(",  ")
            'End If

            'If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberRegion) Then
            '    sb.Append(DataItem.OtherMemberRegion)
            'End If

            sb.Append(IIf(Not String.IsNullOrEmpty(DataItem.OtherMemberCity), DataItem.OtherMemberCity, ""))
            sb.Append(",  ")
            sb.Append(IIf(Not String.IsNullOrEmpty(DataItem.OtherMemberRegion), DataItem.OtherMemberRegion, ""))
            sb.Append(",  ")
            sb.Append(IIf(Not String.IsNullOrEmpty(DataItem.OtherMemberCountry), ProfileHelper.GetCountryName(DataItem.OtherMemberCountry), ""))
            sb.Replace(",  " & ",  ", ",  ")

            sb.Append("</li>")


            sb.Append("<li>")

            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberHeight) Then
                sb.Append(DataItem.OtherMemberHeight)
            End If

            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberHeight) AndAlso Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberPersonType) Then
                sb.Append(" - ")
            End If

            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberPersonType) Then
                sb.Append(DataItem.OtherMemberPersonType)
            End If

            sb.Append("</li>")


            sb.Append("<li>")

            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberHair) Then
                sb.Append(DataItem.OtherMemberHair)
            End If

            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberEyes) Then
                If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberHair) Then sb.Append(", ")
                sb.Append(DataItem.OtherMemberEyes)
            End If

            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberEthnicity) Then
                If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberEyes) OrElse Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberHair) Then sb.Append(", ")
                sb.Append(DataItem.OtherMemberEthnicity)
            End If

            sb.Append("</li>")
            sb.Append("</ul>")

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Return sb.ToString()
    End Function


    Protected Sub rptPubSrch_ItemDataBound(source As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptPubSrch.ItemDataBound
        Try
            If (IsUserAuthenicated) Then

                Dim lnkOther As HyperLink = e.Item.FindControl("lnkOther")
                Dim lnkFav As HyperLink = e.Item.FindControl("lnkFav")
                Dim lnkLike As HyperLink = e.Item.FindControl("lnkLike")

                If (e.Item.DataItem IsNot Nothing) Then
                    Dim o As clsWinkUserListItem = DirectCast(e.Item.DataItem, Dating.Server.Site.Web.clsWinkUserListItem)
                    lnkOther.NavigateUrl = ResolveUrl("~/Members/Profile.aspx?p=" & HttpUtility.UrlEncode(o.OtherMemberLoginName))
                    lnkFav.NavigateUrl = ResolveUrl("~/Members/Profile.aspx?p=" & HttpUtility.UrlEncode(o.OtherMemberLoginName))
                    lnkLike.NavigateUrl = ResolveUrl("~/Members/Profile.aspx?p=" & HttpUtility.UrlEncode(o.OtherMemberLoginName))
                End If

            End If

            'Dim lnkWhatIs As LinkButton = e.Item.FindControl("lnkWhatIs")
            'If (lnkWhatIs IsNot Nothing) Then
            '    lnkWhatIs.ToolTip = Me.CurrentPageData.GetCustomString("WhatIsText")

            '    If (String.IsNullOrEmpty(InfoWinUrl)) Then InfoWinUrl = ResolveUrl("~/InfoWin.aspx?info=searchcommands")
            '    If (String.IsNullOrEmpty(InfoWinHeaderText)) Then InfoWinHeaderText = Me.CurrentPageData.GetCustomString("Search_WhatIsPopup_HeaderText")

            '    lnkWhatIs.OnClientClick = WhatIsIt.OnMoreInfoClickFunc(lnkWhatIs.OnClientClick,
            '                                 InfoWinUrl,
            '                                 InfoWinHeaderText)
            'End If

        Catch ex As Exception

        End Try
    End Sub


End Class
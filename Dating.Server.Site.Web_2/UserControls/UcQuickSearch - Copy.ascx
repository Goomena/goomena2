﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UcQuickSearch.ascx.vb"
    Inherits="Dating.Server.Site.Web.UcQuickSearch" %>

<script type="text/javascript">
    function TrackBar_Init(s, e) {
   
        if (s == window["trcDistance"]) {
        }
        if (s == window["trcAge"]) {
        }
        if (trackBarInfoList && trackBarInfoList != null &&
        trackBarInfoList.List != null && trackBarInfoList.List.length > 0) {
            SetTextForPositions(s);
        }
    }
    function TrackBar_PositionChanged(s, e) {
        SetTextForPositions(s);
    }
    function SetTextForPositions(s) {
        var txt = "", ti;
        try {
            //s.GetItemText(s.GetPosition())
            //s.GetPositionStart()
            if (s == window["trcDistance"]) {
                ti = trackBarInfoList.GetInfoByPos("trcDistance", "DefaultText", s.GetPosition());
                if (ti != null) { txt = ti.DefaultText; } else {
                    txt = s.GetItemText(s.GetPosition());
                    if (stringIsEmpty(txt)) txt = s.GetPosition();
                }
                $('#QuickSearchContainer #QuickSearchFilters .DistanceContainerControl .criteria .trackbar-pos-val').html(txt);
            }
            else if (s == window["trcAge"]) {
                ti = trackBarInfoList.GetInfoByPos("trcAge", "DefaultTextMin", s.GetPositionStart());
                if (ti != null) { txt = ti.DefaultTextMin; } else {
                    txt = s.GetItemText(s.GetPositionStart());
                    if (stringIsEmpty(txt)) txt = s.GetPositionStart();
                }
                $('#QuickSearchContainer #QuickSearchFilters .AgeContainerControl #age-min').html(txt);

                ti = trackBarInfoList.GetInfoByPos("trcAge", "DefaultTextMax", s.GetPositionEnd());
                if (ti != null) { txt = ti.DefaultTextMax; }
                else {
                    txt = s.GetItemText(s.GetPositionEnd());
                    if (stringIsEmpty(txt)) txt = s.GetPositionEnd();
                }
                $('#QuickSearchContainer #QuickSearchFilters .AgeContainerControl #age-max').html(txt);
            }
        }
        catch (e) { }
    }
    function SaveDefaultText2() {
        try {
            trackBarInfoList.AddInfo("trcHeight", "DefaultTextMin", "&lt;152cm", 0);
          

            trackBarInfoList.AddInfo("trcDistance", "DefaultText", '<%= msg_DistanceVal.Text%>', <%= trcDistance.Items.Count - 1%>);

            trackBarInfoList.AddInfo("trcAge", "DefaultTextMin", '18', 0);
            trackBarInfoList.AddInfo("trcAge", "DefaultTextMax", '120', <%= trcAge.Items.Count - 1%>);
        }
        catch (e) { }
    }
    $(function(){
        SaveDefaultText2();
    
    });
    function CheckBoxList_SelectedIndexChanged(s, e) {
        try {
            var itm =s.GetItem(e.index);
            if(itm.value == "-1"){
                s.UnselectAll();
                s.SelectValues(["-1"]);
            }
            else
                s.UnselectValues(["-1"]);
        }
        catch (e) { }
    }
    var textSeparator = ";";
    function OnListBoxSelectionChanged(listBox, args) {
        if (args.index == 0)
            args.isSelected ? listBox.SelectAll() : listBox.UnselectAll();
        UpdateSelectAllItemState(listBox);
        UpdateText(listBox);
    }
    function UpdateSelectAllItemState(clst) {
        IsAllSelected(clst) ? clst.SelectIndices([0]) : clst.UnselectIndices([0]);
    }
    function IsAllSelected(clst) {
        var selectedDataItemCount = clst.GetItemCount() - (clst.GetItem(0).selected ? 0 : 1);
        return clst.GetSelectedItems().length == selectedDataItemCount;
    }
    function UpdateText(clst) {
        var selectedItems = clst.GetSelectedItems();
        if (window['clstHairColor'] == clst) {
            ddeHairColorWrap.SetText(GetSelectedItemsText(selectedItems));
        }
        
    }
    function SynchronizeListBoxValues(dropDown, args) {
        var clst = null;
        if (window['ddeHairColorWrap'] == dropDown) {
            clst = clstHairColor;
        }
        clst.UnselectAll();
        var texts = dropDown.GetText().split(textSeparator);
        var values = GetValuesByTexts(clst, texts);
        clst.SelectValues(values);
        UpdateSelectAllItemState(clst);
        UpdateText(clst); // for remove non-existing texts
    }
    function TrackBar_PositionChanging(s, e) {
        //SaveDefaultText(s, e);
    }
    function GetSelectedItemsText(items) {
        var texts = [];
        for (var i = 0; i < items.length; i++)
            if (items[i].index != 0)
                texts.push(items[i].text);
        return texts.join(textSeparator);
    }
    function GetValuesByTexts(clst, texts) {
        var actualValues = [];
        var item;
        for (var i = 0; i < texts.length; i++) {
            item = clst.FindItemByText(texts[i]);
            if (item != null)
                actualValues.push(item.value);
        }
        return actualValues;
    }
</script>

<script type="text/javascript" >
    var liketext = '<%= Me.WinkText%>';
    var Favoritetext = '<%= Me.Favorite%>';
    var UnFavoritetext = '<%= Me.Unfavorite%>';
    function ReplaceElementLik(id,Newid) {
        var str = '<div id="Unlike'+Newid +'" class="btn lnkUnWink"><span class="Icon">' +liketext +'</span></div>'
        var Obj = document.getElementById(id); //any element to be fully replaced
        if (Obj.outerHTML) { //if outerHTML is supported
            Obj.outerHTML = str;
        } else { //if outerHTML is not supported, there is a weird but crossbrowsered trick
            var tmpObj = document.createElement("div");
            tmpObj.innerHTML = '<!--REPLACE-->';
            ObjParent = Obj.parentNode; //Okey, element should be parented
            ObjParent.replaceChild(tmpObj, Obj); //here we placing our temporary data instead of our target, so we can find it then and replace it into whatever we want to replace to
            ObjParent.innerHTML = ObjParent.innerHTML.replace('<div><!--REPLACE--></div>', str);
        }
    }
    function OnNoPhoto() {
        location.assign('<%= System.Web.VirtualPathUtility.ToAbsolute("~/Members/Photos.aspx") %>');
    }
    function ReplaceElementUnLik(id, Newid) {
        var Stid = "'" + Newid + "'";
        var NewStid = "'Unlike" + Newid + "'";
        var str = '<a id="lnkWink' + Newid + '" class="btn lnkWink" href="javascript:void(0);" onclick="ReplaceElementLik(this.id,' + Stid + ');LikeProfile(' + Stid + ',ReplaceElementUnLik,' + NewStid + ',' + Stid + ',OnNoPhoto);return false;" ><span class="Icon">' + liketext + '</span></a>';
        var Obj = document.getElementById(id); //any element to be fully replaced
        if (Obj.outerHTML) { //if outerHTML is supported
            Obj.outerHTML = str;
        } else { //if outerHTML is not supported, there is a weird but crossbrowsered trick
            var tmpObj = document.createElement("div");
            tmpObj.innerHTML = '<!--REPLACE-->';
            ObjParent = Obj.parentNode; //Okey, element should be parented
            ObjParent.replaceChild(tmpObj, Obj); //here we placing our temporary data instead of our target, so we can find it then and replace it into whatever we want to replace to
            ObjParent.innerHTML = ObjParent.innerHTML.replace('<div><!--REPLACE--></div>', str);
        }
    }
    function ReplaceElementFav(id, Newid) {
        var Stid="'"+Newid+"'"
        var str = '<a id="UnFavorite' + Newid + '" class="btn lnkUnFavorite" href="javascript:void(0);" onclick="ReplaceElementUnFav(this.id,' + Stid + '); UnFavoriteProfile(' + Stid + ');return false;" ><span class="Icon">' + UnFavoritetext + '</span></a>'
        var Obj = document.getElementById(id); //any element to be fully replaced
        if (Obj.outerHTML) { //if outerHTML is supported
            Obj.outerHTML = str;
        } else { //if outerHTML is not supported, there is a weird but crossbrowsered trick
            var tmpObj = document.createElement("div");
            tmpObj.innerHTML = '<!--REPLACE-->';
            ObjParent = Obj.parentNode; //Okey, element should be parented
            ObjParent.replaceChild(tmpObj, Obj); //here we placing our temporary data instead of our target, so we can find it then and replace it into whatever we want to replace to
            ObjParent.innerHTML = ObjParent.innerHTML.replace('<div><!--REPLACE--></div>', str);
        }
    }
    function ReplaceElementUnFav(id, Newid) {
        var Stid = "'" + Newid + "'"
        var str = '<a id="Favorite' + Newid + '" class="btn lnkFavorite" href="javascript:void(0);" onclick="ReplaceElementFav(this.id,' + Stid + '); FavoriteProfile(' + Stid + ');return false;" ><span class="Icon">' + Favoritetext + '</span></a>'
        var Obj = document.getElementById(id); //any element to be fully replaced
        if (Obj.outerHTML) { //if outerHTML is supported
            Obj.outerHTML = str;
        } else { //if outerHTML is not supported, there is a weird but crossbrowsered trick
            var tmpObj = document.createElement("div");
            tmpObj.innerHTML = '<!--REPLACE-->';
            ObjParent = Obj.parentNode; //Okey, element should be parented
            ObjParent.replaceChild(tmpObj, Obj); //here we placing our temporary data instead of our target, so we can find it then and replace it into whatever we want to replace to
            ObjParent.innerHTML = ObjParent.innerHTML.replace('<div><!--REPLACE--></div>', str);
        }
    }
</script>

<div id="QuickSearchContainer">
    <div id="QuickSearchFilters">
        <div class="Header">
            <asp:Label ID="lblHeader" runat="server" Text="Quick Search" CssClass="lblHeader"></asp:Label>
        </div>
        <div class="AgeContainerControl">
            <div class="criteria trackbar tb-age">
                <div class="trackbar-title">
                    <asp:Label ID="msg_AgeText" runat="server" Text="Ηλικία" CssClass="lblHeader2" />
                </div>
                <dx:ASPxTrackBar runat="server" ID="trcAge" MinValue="18" MaxValue="120" Step="1"
                    LargeTickInterval="16" SmallTickFrequency="6" Width="169" AllowRangeSelection="true"
                    PositionStart="18" PositionEnd="120" ClientInstanceName="trcAge" CssClass="trcAge"
                    EnableTheming="False" ScaleLabelHighlightMode="HandlePosition"
                    ShowChangeButtons="False">
                    <ClientSideEvents Init="TrackBar_Init" PositionChanging="TrackBar_PositionChanging" PositionChanged="TrackBar_PositionChanged" />
                </dx:ASPxTrackBar>
                <div>
                    <div id="age-min" class="trackbar-pos-val lfloat">
                        <asp:Literal ID="msg_AgeMin" runat="server" Text="18" />
                    </div>
                    <div id="age-max" class="trackbar-pos-val rfloat">
                        <asp:Literal ID="msg_AgeMax" runat="server" Text="120" />
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
        <div class="DistanceContainerControl">
            <div class="criteria trackbar tb-distance">
                <div class="trackbar-title">
                    <asp:Label ID="msg_DistanceFromMe_ND" runat="server" Text="Απόσταση απο εμένα" CssClass="lblHeader2" />
                </div>
                <dx:ASPxTrackBar runat="server" ID="trcDistance" Step="1"
                    MinValue="20"
                    MaxValue="10000"
                    PositionStart="0" PositionEnd="30"
                    LargeTickInterval="4" LargeTickEndValue="1" Width="169"
                    ClientInstanceName="trcDistance" EnableTheming="False" ScaleLabelHighlightMode="HandlePosition"
                    ShowChangeButtons="False" CssClass="trcDistance">
                    <Items>
                        <dx:TrackBarItem Text="20km" ToolTip="20km" Value="20" />
                        <dx:TrackBarItem Text="30km" ToolTip="30km" Value="30" />
                        <dx:TrackBarItem Text="40km" ToolTip="40km" Value="40" />
                        <dx:TrackBarItem Text="50km" ToolTip="50km" Value="50" />
                        <dx:TrackBarItem Text="60km" ToolTip="60km" Value="60" />
                        <dx:TrackBarItem Text="70km" ToolTip="70km" Value="70" />
                        <dx:TrackBarItem Text="80km" ToolTip="80km" Value="80" />
                        <dx:TrackBarItem Text="90km" ToolTip="90km" Value="90" />
                        <dx:TrackBarItem Text="100km" ToolTip="100km" Value="100" />
                        <dx:TrackBarItem Text="120km" ToolTip="120km" Value="120" />
                        <dx:TrackBarItem Text="150km" ToolTip="150km" Value="150" />
                        <dx:TrackBarItem Text="200km" ToolTip="200km" Value="200" />
                        <dx:TrackBarItem Text="300km" ToolTip="300km" Value="300" />
                        <dx:TrackBarItem Text="400km" ToolTip="400km" Value="400" />
                        <dx:TrackBarItem Text="500km" ToolTip="500km" Value="500" />
                        <dx:TrackBarItem Text="600km" ToolTip="600km" Value="600" />
                        <dx:TrackBarItem Text="700km" ToolTip="700km" Value="700" />
                        <dx:TrackBarItem Text="800km" ToolTip="800km" Value="800" />
                        <dx:TrackBarItem Text="900km" ToolTip="900km" Value="900" />
                        <dx:TrackBarItem Text="1000km" ToolTip="1000km" Value="1000" />
                        <dx:TrackBarItem Text="1200km" ToolTip="1200km" Value="1200" />
                        <dx:TrackBarItem Text="1400km" ToolTip="1400km" Value="1400" />
                        <dx:TrackBarItem Text="1600km" ToolTip="1600km" Value="1600" />
                        <dx:TrackBarItem Text="2000km" ToolTip="2000km" Value="2000" />
                        <dx:TrackBarItem Text="3000km" ToolTip="3000km" Value="3000" />
                        <dx:TrackBarItem Text="4000km" ToolTip="4000km" Value="4000" />
                        <dx:TrackBarItem Text="5000km" ToolTip="5000km" Value="5000" />
                        <dx:TrackBarItem Text="6000km" ToolTip="6000km" Value="6000" />
                        <dx:TrackBarItem Text="7000km" ToolTip="7000km" Value="7000" />
                        <dx:TrackBarItem Text="8000km" ToolTip="8000km" Value="8000" />
                        <dx:TrackBarItem Text="9000km" ToolTip="9000km" Value="9000" />
                        <dx:TrackBarItem Text="10000km" ToolTip="10000km" Value="10000" />
                    </Items>
                    <ClientSideEvents Init="TrackBar_Init" PositionChanging="TrackBar_PositionChanging" PositionChanged="TrackBar_PositionChanged" />
                </dx:ASPxTrackBar>
                <div>
                    <div class="trackbar-pos-val">
                        <asp:Literal ID="msg_DistanceVal" runat="server" Text="Any Distance" />
                    </div>
                </div>
            </div>
        </div>
        <div class="BodyTypeContainerControl">
            <div class="trackbar-title">
                <asp:Label ID="msg_BodyType" runat="server" Text="Body Type" CssClass="lblHeader2" />
            </div>
            <div class="crt-list-content">
                <dx:ASPxCheckBoxList ID="cbBodyType" CssClass="cbBodyType" runat="server" ValueType="System.String" CssPostfix="ci" ClientInstanceName="cbBodyType">
                    <CheckedImage Url="//cdn.goomena.com/Images/memberNewDesign2015/check-box.png?v2" Height="15px"
                        Width="15px">
                    </CheckedImage>
                    <UncheckedImage Url="//cdn.goomena.com/Images/memberNewDesign2015/Uncheck-box.png" Height="15px"
                        Width="15px">
                    </UncheckedImage>
                    <ClientSideEvents SelectedIndexChanged="CheckBoxList_SelectedIndexChanged" />
                </dx:ASPxCheckBoxList>
            </div>
        </div>
        <div class="HairColorContainerControl">

            <div class="HairColorContainer">
                <asp:Label ID="msg_HairColor" runat="server" Text="Hair Color" CssClass="lblHeader2"></asp:Label>
            </div>
            <div class="HairColorDropDownContainer">
                <dx:ASPxDropDownEdit CssClass="cbHairColor" ClientInstanceName="ddeHairColorWrap" ID="ddeHairColorWrap"
                    Width="169px" runat="server" AnimationType="None" NullText="Any Hair Color" ClientSideEvents-ButtonClick="function(s, e) {s.ShowDropDown();}" ClientSideEvents-Init="function(s, e) {s.GetInputElement().disabled = true;}">
                    <DropDownWindowTemplate>
                        <dx:ASPxListBox Width="100%" ID="clstHairColor" ClientInstanceName="clstHairColor"
                            SelectionMode="CheckColumn" runat="server" Height="300px">
                            <CheckBoxCheckedImage Url="//cdn.goomena.com/Images/memberNewDesign2015/check-box.png?v2" Height="15px"
                                Width="15px">
                            </CheckBoxCheckedImage>
                            <CheckBoxUncheckedImage Url="//cdn.goomena.com/Images/memberNewDesign2015/Uncheck-box.png" Height="15px"
                                Width="15px">
                            </CheckBoxUncheckedImage>
                            <ClientSideEvents SelectedIndexChanged="OnListBoxSelectionChanged" />
                        </dx:ASPxListBox>
                    </DropDownWindowTemplate>
                    <ClientSideEvents TextChanged="SynchronizeListBoxValues" DropDown="SynchronizeListBoxValues" />
                </dx:ASPxDropDownEdit>
            </div>
        </div>
        <div class="HasPhotoContainerControl">
            <dx:ASPxCheckBox ID="chkHasPhoto" runat="server" Text="HasPhoto" EncodeHtml="False" CssClass="chk"
                CssPostfix="ci" Checked="true">
                <CheckedImage Url="//cdn.goomena.com/Images/memberNewDesign2015/check-box.png?v2" Height="15px"
                    Width="15px">
                </CheckedImage>
                <UncheckedImage Url="//cdn.goomena.com/Images/memberNewDesign2015/Uncheck-box.png" Height="15px"
                    Width="15px">
                </UncheckedImage>
            </dx:ASPxCheckBox>
        </div>
        <div class="BeOnlineContainerControl">
            <dx:ASPxCheckBox ID="chkBeOnline" runat="server" Text="HasPhoto" EncodeHtml="False" CssClass="chk"
                CssPostfix="ci" Checked="true">
                <CheckedImage Url="//cdn.goomena.com/Images/memberNewDesign2015/check-box.png?v2" Height="15px"
                    Width="15px">
                </CheckedImage>
                <UncheckedImage Url="//cdn.goomena.com/Images/memberNewDesign2015/Uncheck-box.png" Height="15px"
                    Width="15px">
                </UncheckedImage>
            </dx:ASPxCheckBox>
        </div>
        <div class="btnContainer">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <asp:LinkButton ID="btnSearch" runat="server" CssClass="btnSearch" Text="Refresh"></asp:LinkButton>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <asp:UpdatePanel ID="quickSearchUpdatePanel" runat="server">
        <ContentTemplate>
            <div id="SearchResults">
                <div class="NoQuickResults" id="divNoResultsChangeCriteria" runat="server" visible="false">
                    <asp:Literal ID="msg_divNoResultsTryChangeCriteria_ND" runat="server" />
                    <div class="clear"></div>
                </div>
                <asp:Repeater ID="rptQuickSearch" runat="server">
                    <ItemTemplate>
                        <div class='<%# getProfileClass(Eval("RowNumber"))%>'>
                                <span class="Vip" runat="server" visible='<%# Eval("OtherMemberIsVip")%>'></span>
                            <div class="PhotoContainer">
                                <a class="ProfileLinkOnPhoto" href='<%# Eval("OtherMemberProfileViewUrl")%>'>
                                    <%# getPhotoStyle(Eval("OtherMemberImageUrl"))%>
                                
                                     <a ID="lilnkFavorite"
                                              class="btn lnkFavorite" 
                                              runat="server" 
                                              href="javascript:void(0);"  
                                              onclick='<%# "ReplaceElementFav(this.id,""" & Me.GetProfileIdString(Eval("OtherMemberProfileID")) & """); FavoriteProfile(""" & Me.GetProfileIdString(Eval("OtherMemberProfileID")) & """,OnNoPhoto);return false;"%>'   
                                              visible='<%# Eval("AllowFavorite") %>'><span class="Icon"><%= Me.Favorite%></a>
                                 
                                     <a ID="lilnkAllowUnFavorite"
                                              class="btn lnkUnFavorite" 
                                              runat="server" 
                                              href="javascript:void(0);"  
                                              onclick='<%# "ReplaceElementUnFav(this.id,""" & Me.GetProfileIdString(Eval("OtherMemberProfileID")) & """); UnFavoriteProfile(""" & Me.GetProfileIdString(Eval("OtherMemberProfileID")) & """,OnNoPhoto);return false;"%>'   
                                              visible='<%# Eval("AllowUnfavorite") %>'><span class="Icon"><%= Me.Unfavorite%></a>
                                </a>
                            </div>
                            <div class="LoginNameAndPhotos">
                                <a class="OtherLoginName" href="<%# Eval("OtherMemberProfileViewUrl")%>"><%# Eval("OtherMemberLoginName") %></a>
                                <div class="PhotoIconHolder"></div>
                                <div class="PhotoCounter"><%# Eval("PhotosCountString")%></div>
                                <div class="clear"></div>
                            </div>
                            <div class="MoreInfo">
                                <div class="AgeAndCity">
                                    <%# evaluateAgeAndCity(Eval("OtherMemberAge"),Eval("OtherMemberCity")) %>
                                </div>
                                <div class="COuntryAndRegion">
                                    <%# evaluateRegionAndCountry(Eval("OtherMemberRegion"),Eval("OtherMemberCountry")) %>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="ExtraInfo">
                                <div class="InnerCenterbox">
                                    <div class="lfloat status_info info-item" runat="server" visible='<%# if(not string.isnullOrEmpty(Eval("OtherMemberRelationshipTooltip")),true,false) %>'>
                                        <img alt="" src="//cdn.goomena.com/Images/spacer10.png" class="tt_enabled <%# Eval("OtherMemberRelationshipClass")%>">
                                        <div class="tooltip tooltip_status_info" style="display: none;">
                                            <div class="tooltip_text right-arrow"><%# Eval("OtherMemberRelationshipTooltip")%></div>
                                        </div>
                                    </div>
                                    <div class="lfloat zodiac_info">
                                        <img alt="" src="//cdn.goomena.com/Images/spacer10.png" class="tt_enabled <%# Eval("ZodiacName")%>">
                                        <div class="tooltip tooltip_zodiac" style="display: none;">
                                            <div class="tooltip_text right-arrow"><%# Eval("ZodiacString")%></div>
                                        </div>
                                    </div>


                                    <div class="lfloat drinking info-item" runat="server" visible='<%# If(Eval("OtherMemberIsMale")=True,if(not string.isnullOrEmpty(Eval("OtherMemberDrinkingText")),true,false),False) %>'>
                                        <img class="tt_enabled  <%# Eval("OtherMemberDrinkingClass")%>" src="//cdn.goomena.com/Images/spacer10.png" alt="">
                                        <div class="tooltip tooltip_drinking" style="display: none;">
                                            <div class="tooltip_text right-arrow"><%# Eval("OtherMemberDrinkingText")%></div>
                                        </div>
                                    </div>
                                    <div class="lfloat smoking info-item" runat="server" visible='<%# If(Eval("OtherMemberIsMale")=True,if(not string.isnullOrEmpty(Eval("OtherMemberSmokingText")),true,false),False) %>'>
                                        <img class="tt_enabled  <%# Eval("OtherMemberSmokingClass")%>" src="//cdn.goomena.com/Images/spacer10.png" alt="">
                                        <div class="tooltip tooltip_smoking" style="display: none;">
                                            <div class="tooltip_text right-arrow"><%# Eval("OtherMemberSmokingText")%></div>
                                        </div>
                                    </div>
                                    <div class="lfloat breast info-item" runat="server" visible='<%# If(Eval("OtherMemberIsMale")=false,if(not string.isnullOrEmpty(Eval("OtherMemberBreastSizeTooltip")),true,false),False) %>'>
                                        <img alt="" src="//cdn.goomena.com/Images/spacer10.png" class="tt_enabled <%# Eval("OtherMemberBreastSizeClass")%>">
                                        <div class="tooltip tooltip_breast" style="display: none;">
                                            <div class="tooltip_text right-arrow"><%# Eval("OtherMemberBreastSizeTooltip")%></div>
                                        </div>
                                    </div>
                                    <div class="lfloat hair info-item" runat="server" visible='<%# If(Eval("OtherMemberIsMale")=false,if(not string.isnullOrEmpty(Eval("OtherMemberHairTooltip")),true,false),False) %>'>
                                        <img alt="" src="//cdn.goomena.com/Images/spacer10.png" class="tt_enabled <%# Eval("OtherMemberHairClass")%>">
                                        <div class="tooltip tooltip_hair" style="display: none;">
                                            <div class="tooltip_text right-arrow"><%# Eval("OtherMemberHairTooltip")%></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="ButtonArea">
                                <div class="btnGroupContainer" id="ActionsMenu" runat="server" visible='true'>
                                     <div class="btn lnkUnWink" runat="server" visible='<%# Not Eval("AllowWink")%>'>
                                        <span class="Icon"><%= Me.WinkText%></span>
                                     </div>
                                        <a id="lnkWink" class="btn lnkWink" runat="server" href="javascript:void(0);"  onclick='<%# "ReplaceElementLik(this.id,""" & Me.GetProfileIdString(Eval("OtherMemberProfileID") )&"""); LikeProfile(""" & Me.GetProfileIdString(Eval("OtherMemberProfileID")) & """,ReplaceElementUnLik,""Unlike" & Me.GetProfileIdString(Eval("OtherMemberProfileID")) &""","""& Me.GetProfileIdString(Eval("OtherMemberProfileID")) & """,OnNoPhoto);return false;"%>'   visible='<%# Eval("AllowWink") %>'><span class="Icon"><%= Me.WinkText%></span></a>
                                 <a id="lnkSendMessage" class="btn lnkSendMessage" runat="server" href="javascript:void(0);"  
                                     onclick='<%# Eval("SendMessageUrl") & "return false;"%>'  ><span class="Icon"><%= Me.SendMessageText%></span></a>
                                       <%--<asp:HyperLink ID="lnkSendMessage" runat="server" EnableViewState="true" Visible='true'
                                                CssClass="btn lnkSendMessage" CommandArgument='<%# Eval("OtherMemberProfileID") %>'
                                                NavigateUrl='<%# Eval("SendMessageUrl")%>' Text=""><span class="Icon"><%= Me.SendMessageText%></span></asp:HyperLink>--%>
                                  <asp:HyperLink
                                                ID="lnkNewMakeOffer2" runat="server" NavigateUrl='<%# Eval("CreateOfferUrl") %>' Visible='<%# Eval("AllowActionsCreateOffer")%>'
                                                CssClass="btn MakeOffer"><span class="Icon"><%= Me.PlaceBid%></span></asp:HyperLink>
                                        <div class="btn MakeOfferPending" runat="server" visible=<%# not Eval("AllowActionsCreateOffer")%>>
                                            <span class="Icon"><%= Me.PlaceBid%>

                                            </span>

                                        </div>
                                    <div class="clear"></div>
                                                                </div>
                            </div>
                            <div class="Online<%#If((Eval("OtherMemberIsOnline")), " visible", " hidden")%>"></div>
                        </div>

                    </ItemTemplate>
                </asp:Repeater>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
            <div id="buttonResults">
                <div class="InnerCenterBox">
                    <asp:LinkButton ID="lnkPreviousPageResults" CssClass="lnkMoreResults lnkPreviousPageResults" runat="server">
                        <div class="btnIcon" runat="server"></div>
                        <asp:Label ID="lblPreviousPageResults" runat="server" Text="Previous Profiles"></asp:Label>
                    </asp:LinkButton>
                    <asp:LinkButton ID="lnkNextPageResults" CssClass="lnkMoreResults lnkNextPageResults" runat="server">
                        <asp:Label ID="lblNextPageResults" runat="server" Text="Next Profiles"></asp:Label>
                        <div class="btnIcon" runat="server"></div>
                    </asp:LinkButton>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="clear"></div>
</div>

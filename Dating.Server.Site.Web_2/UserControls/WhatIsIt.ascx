﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="WhatIsIt.ascx.vb" Inherits="Dating.Server.Site.Web.WhatIsIt" %>

<dx:ASPxPopupControl runat="server"
    Height="594px" Width="1000px"
    ID="popupWhatIs"
    ClientInstanceName="popupWhatIs" 
    ClientIDMode="AutoID" 
    AllowDragging="True" 
    PopupVerticalAlign="WindowCenter" 
    PopupHorizontalAlign="WindowCenter" 
    PopupElementID="lnkViewDescription" 
    Modal="True" 
    AllowResize="True" 
    AutoUpdatePosition="True"
    ShowShadow="False" >
    <ContentStyle HorizontalAlign="Left" VerticalAlign="Middle">
        <Paddings Padding="0px" PaddingTop="25px" />
        <Paddings Padding="0px" PaddingTop="25px" ></Paddings>
    </ContentStyle>
    <CloseButtonStyle BackColor="White">
        <HoverStyle>
            <BackgroundImage ImageUrl="//cdn.goomena.com/Images2/popup-wrn/close.png" Repeat="NoRepeat" HorizontalPosition="center"
                VerticalPosition="center"></BackgroundImage>
        </HoverStyle>
        <BackgroundImage ImageUrl="//cdn.goomena.com/Images2/popup-wrn/close.png" Repeat="NoRepeat" HorizontalPosition="center"
            VerticalPosition="center"></BackgroundImage>
        <BorderLeft BorderColor="White" BorderStyle="Solid" BorderWidth="10px" />
    </CloseButtonStyle>
    <CloseButtonImage Url="//cdn.goomena.com/Images/spacer10.png" Height="43px" Width="43px">
    </CloseButtonImage>
    <SizeGripImage Height="40px" Url="//cdn.goomena.com/Images/resize.png" Width="40px">
    </SizeGripImage>
    <HeaderStyle>
        <Paddings Padding="0px" PaddingLeft="0px" />
        <Paddings PaddingTop="0px" PaddingBottom="0px"></Paddings>
        <BorderTop BorderColor="White" BorderStyle="Solid" BorderWidth="5px" />
        <BorderRight BorderColor="White" BorderStyle="Solid" BorderWidth="5px" />
        <BorderBottom BorderColor="White" BorderStyle="Solid" BorderWidth="5px" />
    </HeaderStyle>
    <ModalBackgroundStyle BackColor="Black" Opacity="70" CssClass="WhatIs-ModalBG"></ModalBackgroundStyle>
    <ContentCollection>
        <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server" SupportsDisabledAttribute="True">
        </dx:PopupControlContentControl>
    </ContentCollection>
</dx:ASPxPopupControl>


<script type="text/javascript">
    function OnMoreInfoClick(contentUrl, obj, headerText, wdt, hgt, params) {
        var popup = window["popupWhatIs"]
        popup.SetContentUrl('<%= ResolveUrl("~/loading.htm") %>');

        if (headerText != null) popup.SetHeaderText(headerText);
        if (obj != null && typeof obj !== 'undefined' && obj["id"]!=null) popup.SetPopupElementID(obj.id);
        popup.Show();

        if (typeof wdt === 'undefined' || wdt == null) wdt = 750
        if (typeof hgt === 'undefined' || hgt == null) hgt = 594
        var win_wdt = $(window).width();
        var win_hgt = $(window).height();

        if ((win_wdt - 100) < wdt) { wdt = win_wdt - 100; }
        if ((win_hgt - 100) < hgt) { hgt = win_hgt - 100; }

        popup.SetSize(wdt, hgt);
        popup.UpdatePosition();
        setTimeout(function () {
            if (stringIsEmpty(gupWithUrl(contentUrl,'popup'))) {
                contentUrl = contentUrl + (contentUrl.indexOf('?') > -1 ? '&' : '?') + 'popup=popupWhatIs';
            }
            popup.SetContentUrl(contentUrl);
        }, 500);
    }

    $(function () {
        function m__setPopupClass() {
            var popup = popupWhatIs
            var divId = popup.name + '_PW-1';
            $('#' + divId)
            .addClass('fancybox-popup')
            .prepend($('<div class="header-line"></div>'));
            var closeId = popup.name + '_HCB-1Img';
            $('#' + closeId).addClass('fancybox-popup-close');
            var hdrId = popup.name + '_PWH-1T';
            $('#' + hdrId).addClass('fancybox-popup-header');
            //var frmId = popup.name + '_CSD-1';
            //$('#' + frmId).addClass('iframe-container');
        }

        setTimeout(m__setPopupClass, 300);
    });
    
</script>



<%--

<dx:ASPxPopupControl runat="server"
    Height="594px" Width="1000px"
    ID="popupWhatIs"
    ClientInstanceName="popupWhatIs" 
    ClientIDMode="AutoID" AllowDragging="True" PopupVerticalAlign="Below" 
        PopupHorizontalAlign="WindowCenter" PopupElementID="lnkViewDescription" 
        Modal="True" AllowResize="True" AutoUpdatePosition="True">
    <ContentStyle HorizontalAlign="Left" VerticalAlign="Middle">
        <Paddings Padding="10px" />
        <Paddings Padding="10px"></Paddings>
    </ContentStyle>
    <CloseButtonStyle BackColor="White">
        <HoverStyle>
            <BackgroundImage ImageUrl="//cdn.goomena.com/Images2/popup/close.png" Repeat="NoRepeat" HorizontalPosition="center"
                VerticalPosition="center"></BackgroundImage>
        </HoverStyle>
        <BackgroundImage ImageUrl="//cdn.goomena.com/Images2/popup/close.png" Repeat="NoRepeat" HorizontalPosition="center"
            VerticalPosition="center"></BackgroundImage>
        <BorderLeft BorderColor="White" BorderStyle="Solid" BorderWidth="10px" />
    </CloseButtonStyle>
    <CloseButtonImage Url="//cdn.goomena.com/Images/spacer10.png" Height="28px" Width="28px">
    </CloseButtonImage>
    <SizeGripImage Height="40px" Url="//cdn.goomena.com/Images/resize.png" Width="40px">
    </SizeGripImage>
    <HeaderStyle BackColor="#0198CF" Font-Bold="False" ForeColor="White" 
        Font-Size="16px">
        <Paddings Padding="0px" PaddingLeft="12px" />
        <Paddings PaddingTop="0px" PaddingBottom="0px"></Paddings>
        <BorderTop BorderColor="White" BorderStyle="Solid" BorderWidth="5px" />
        <BorderRight BorderColor="White" BorderStyle="Solid" BorderWidth="5px" />
        <BorderBottom BorderColor="White" BorderStyle="Solid" BorderWidth="5px" />
    </HeaderStyle>
    <ModalBackgroundStyle BackColor="Black" Opacity="70"></ModalBackgroundStyle>
    <ContentCollection>
        <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server" SupportsDisabledAttribute="True">
        </dx:PopupControlContentControl>
    </ContentCollection>
</dx:ASPxPopupControl>
--%>
﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucPopupNewTerms.ascx.vb" 
    Inherits="Dating.Server.Site.Web.ucPopupNewTerms" %>
<%--<script type="text/javascript">
    var StartPopup = null;
    function sd() { StartPopup = 10; }
    function InitPopup() {
        if (StartPopup != null) { openPopupNewTerms(); }
    }
</script>--%>
<%--<asp:UpdatePanel ID="UpdatePanel1" runat="server">--%>
    
<%--</asp:UpdatePanel>--%>

 <script type="text/javascript">
     function EndRequestHandler1(sender, args) {
         try {
             HideLoading();
         }
         catch (e) { }
         try {
             setActions();
           //  InitPopup();
             //m__setPopupClass();
         }
         catch (e) { }
     }
     function HidePopupAndShowInfo() {
         PopupNewTerms.Hide();
         performRequest_AcceptedTerms();
       
     }
     function ShowTerms() {
        // var popup = window["PopupNewTerms"]
      
         PopupNewTerms.SetSize(729, 584);
         $('#' + PopupNewTerms.name + '_CIF-1').css({ 'height': '100%' });
         $('#' + PopupNewTerms.name + '_PW-1 .dxpc-mainDiv.dxpc-shadow').css({ 'height': '584px' });
         PopupNewTerms.UpdatePosition();
     }

   
     var popupNewTermsLoaded = false;
     var acceptedTerms = false;
     function PopupNewTerms_Init(s, e) {
         popupNewTermsLoaded = true;
         }

     function openPopupNewTermsDelayed() {
         while (!popupNewTermsLoaded) {
             setTimeout(function () { openPopupNewTermsDelayed() }, 1400);
             return;
         }
         openPopupNewTerms();
     }


 

     function openPopupNewTerms() {
                 var contentUrl = '<%= ResolveUrl("~/Members/PopupNewTerms.aspx")%>', headerText = null;
   
         var popup = window["PopupNewTerms"]
             popup.SetContentUrl('<%= ResolveUrl("~/loading.htm")%>');
             popup.SetContentUrl(contentUrl);
             if (headerText != null) popup.SetHeaderText(headerText);
             var hdrId = popup.name + '_PW-1';
             var headerTop = hdrId + ' .dxpc-mainDiv.dxpc-shadow'
             $('#' + headerTop).css({ 'border-radius': '7px', 'height': '125px' });
             $('#' + headerTop + ' .dxpc-contentWrapper .dxpc-content').css({ 'height': '125px' });
             $('#' + popup.name + '_PWC-1').css({ 'height': '125px' });
             $('#' + popup.name + '_CIF-1').css({ 'border-radius': '7px' });
         //  $('#' + popup.name + '_PWC-1').css({ 'height': '125px!important' });dxpc-contentWrapper
                 popup.Show();
                 popup.SetSize(729, 93);
           
          
                 //$('#' + popup.name + '_PWC-1').css({ 'height': '125px!important' });

             popup.UpdatePosition();
             popup.Closing.AddHandler(__Closing_Remove);
             popup.CloseUp.AddHandler(__CloseUp_Remove);
          //   $('#' + popup.name + '_PWC-1').css({ 'height': '125px!important' });

             function __Closing_Remove(s, e) {
                 try {
                     if (__RefreshPage == true) {
                     <%--   eval($('#<%= lnkRefresh.ClientID %>').attr("href").replace("javascript:",""));--%>
                    }
                    s.SetContentUrl('about:blank');
                    s.Closing.RemoveHandler(__Closing_Remove);
                }
                catch (e) { }
            }
            function __CloseUp_Remove(s, e) {
                try {
                    s.CloseUp.RemoveHandler(__CloseUp_Remove);
                }
                catch (e) { }
            }


        }

        var __RefreshPage = false;
        function SetRefreshPage() {
            __RefreshPage = true;
        }
        function setActions() {
            var wdt = 728, hgt = 609;
            __RefreshPage = false;
        
        }

        $(function () { setActions();  });
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler1);




        function performRequest_AcceptedTerms() {
            try {
                var param = $.toJSON({ 'val': true });
                $.ajax({
                    type: "POST",
                    url: "json.aspx/NewTermsAcceptedShown",
                    data: param,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: true,
                    error: function (ar1, ar2, ar3) {
                    },
                    success: function (msg) {},
                    beforeSend: function () { },
                    complete: function () {}
                });
            }
            catch (e) { }
        }

    </script>

     <dx:ASPxPopupControl runat="server" 
        Height="93px"
       
        ID="PopupNewTerms"
        ClientInstanceName="PopupNewTerms" 
        ClientIDMode="AutoID" 
        PopupVerticalAlign="WindowCenter" 
        PopupHorizontalAlign="WindowCenter" 
        Modal="True" 
        
         Action="CloseButton" Width="729px" ShowSizeGrip="False"  DragElement="Window" PopupAction="None" RenderMode="Lightweight" ShowHeader="False" CloseAction="None" LoadingPanelDelay="10" MinHeight="10px" MinWidth="10px" AutoUpdatePosition="True">
    <ContentStyle HorizontalAlign="Left" VerticalAlign="Middle">
        <Paddings Padding="0px" />
        <Paddings Padding="0px" ></Paddings>
    </ContentStyle>
         
         <ClientSideEvents Closing="function(s, e) {
	performRequest_AcceptedTerms();
}" />
    <CloseButtonStyle>
        <HoverStyle>
            <BackgroundImage ImageUrl="~/Images/spacer10.png" Repeat="NoRepeat" HorizontalPosition="center"
                VerticalPosition="center"></BackgroundImage>
        </HoverStyle>
        <BackgroundImage ImageUrl="~/Images/spacer10.png" Repeat="NoRepeat" HorizontalPosition="center"
            VerticalPosition="center"></BackgroundImage>
        <BorderLeft BorderColor="White" BorderStyle="None" BorderWidth="0px" />
    </CloseButtonStyle>
    <CloseButtonImage Url="~/Images2/gifts/Popup/close-button20141127.png" Height="30px" Width="30px">
    </CloseButtonImage>
        <SizeGripImage Url="~/Images/resize.png">
        </SizeGripImage>
    <HeaderStyle>
        <Paddings Padding="0px" PaddingLeft="0px" />
        <Paddings PaddingTop="0px" PaddingBottom="0px"></Paddings>
        <BorderTop BorderColor="White" BorderStyle="None" BorderWidth="0px" />
        <BorderRight BorderColor="White" BorderStyle="None" BorderWidth="0px" />
        <BorderBottom BorderColor="White" BorderStyle="None" BorderWidth="0px" />
            </HeaderStyle>
<ModalBackgroundStyle BackColor="Black" Opacity="70"></ModalBackgroundStyle>
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server" SupportsDisabledAttribute="True" Height="93px" Width="729px">
            </dx:PopupControlContentControl>
        </ContentCollection>
         <ClientSideEvents Init="PopupNewTerms_Init" />
    </dx:ASPxPopupControl>







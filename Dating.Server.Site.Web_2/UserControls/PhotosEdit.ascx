﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PhotosEdit.ascx.vb" Inherits="Dating.Server.Site.Web.PhotosEdit" %>
<%@ Register Assembly="DevExpress.Web.v13.1, Version=13.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxUploadControl" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.v13.1, Version=13.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxCallbackPanel" tagprefix="dx" %>
<script type="text/javascript" src="/v1/Scripts/photos-edit.js?v1"></script>
<script type="text/javascript">
    msg_ClickToExpand = '<%= globalStrings.GetCustomString("msg_ClickToExpand", GetLag() )  %>';
    msg_ClickToCollapse = '<%= globalStrings.GetCustomString("msg_ClickToCollapse", GetLag())%>';
    cancelHtml = '<%= MyBase.CurrentPageData.GetCustomString("btnCancelEditDefault") %>';
    editHtml = '<%= MyBase.CurrentPageData.GetCustomString("btnEditDefault") %>';
    hdfDefaultPath = '<%= hdfDefaultPath.ClientID %>';
</script>
<style type="text/css">
.dxucErrorCell{width:210px;word-wrap: break-word;white-space: normal;}
.progress-wrap{width:210px;}
td.dxePBMainCell table{margin-top:0px !important;}
.ImageUpload{}
</style>


<div id="mp_photo">
    <div class="two-boxes">

        <div class="left-box">
            <div class="one-box">
		        <div class="mp_title">
                    <asp:Label ID="lblProfilePhotoTitle" runat="server" Text="Profile Photo"/>
		        </div>
                <div class="pr-photo-125-noshad">
                    <asp:Image ID="imgProfilePhoto" runat="server" ImageUrl="~/Images/guy.jpg" CssClass="round-img-125" alt="avatar" />
                </div>
                <div class="actions">
                    <div class="lfloat"><asp:LinkButton ID="btnEditDefault" runat="server" CssClass="btnEditDefault" OnClientClick="ChangeDefaultImg(this);return false;" Text="btnEditDefault" /></div>
                    <div class="line-vert1"></div>
                    <div class="rfloat"><asp:Button ID="btnDeletePhoto" runat="server" CssClass="btnDeletePhoto" CommandName="DELETE" OnClientClick="ShowLoading();" /></div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
       <div class="right-box">
            <div class="one-box">
                <div class="mp_title">
                    <asp:Literal ID="msg_UploadNewPhoto" runat="server"/>
		        </div>
                <asp:FileUpload ID="uplImage" runat="server" multiple="multiple"  CssClass="ImageUpload multi" />

                <div class="clear"></div>
                <div class="progress-wrap" style="display:none;">
                    <div style="display:none;">
                    <br />
                        <div class="clear"></div>
                        <dx:ASPxProgressBar ID="ASPxProgressBar2" runat="server" Width="100%"
                                        ClientInstanceName="progress2" 
                            CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx"></dx:ASPxProgressBar>
                    <br />
                    </div>
                </div>
                <div class="clear"></div>
                <div class="allowed-text">
                    <asp:Label ID="lblAllowed" runat="server"></asp:Label>
                </div>

                <table class="center-table" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                <div style="margin:0 0 15px 0;">
                    <div class="lblPhotoLevel-wrap">
                        <asp:Label ID="lblPhotoLevel" runat="server" Text="Επίπεδο φωτογραφίας" CssClass="lblPhotoLevel"/>&nbsp;<asp:Image
                            ID="imgPhotoLevelInfo" runat="server" ImageUrl="~/Images/icon_tip.png" />
                    </div>
                    <dx:ASPxPopupControl SkinID="None" EncodeHtml="False" ID="popupWindows" runat="server"
                        EnableViewState="False" EnableHotTrack="False" PopupHorizontalAlign="RightSides"
                        PopupVerticalAlign="Above" PopupHorizontalOffset="1" PopupVerticalOffset="-4"
                        EnableHierarchyRecreation="True" CloseAction="MouseOut" HeaderText="Επίπεδο φωτογραφίας" 
                        PopupAction="MouseOver" RenderMode="Lightweight">
                        <ClientSideEvents />
                        <Windows>
                            <dx:PopupWindow PopupElementID="lblPhotoLevel" CloseAction="MouseOut" PopupAction="MouseOver">
                                <ContentCollection>
                                    <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server" SupportsDisabledAttribute="True">
                                            <asp:Label ID="lblAllowAccessPhotosLevel" runat="server" Text=""/>
                                    </dx:PopupControlContentControl>
                                </ContentCollection>
                            </dx:PopupWindow>
                            <dx:PopupWindow PopupElementID="imgPhotoLevelInfo" CloseAction="MouseOut" PopupAction="MouseOver">
                                <ContentCollection>
                                    <dx:PopupControlContentControl ID="PopupControlContentControl2" runat="server" SupportsDisabledAttribute="True">
                                            <asp:Label ID="lblAllowAccessPhotosLevelIcon" runat="server" Text=""/>
                                    </dx:PopupControlContentControl>
                                </ContentCollection>
                            </dx:PopupWindow>
                        </Windows>
                        <ContentCollection>
                            <dx:PopupControlContentControl ID="PopupControlContentControl10" runat="server" SupportsDisabledAttribute="True">
                            </dx:PopupControlContentControl>
                        </ContentCollection>
                    </dx:ASPxPopupControl>

                    <dx:ASPxComboBox ID="cbPhotosLevel" runat="server" SelectedIndex="0">
                        <Items>
                            <dx:ListEditItem Selected="True" Text="Public" Value="0" />
                            <dx:ListEditItem Text="Private level 1" Value="1" />
                            <dx:ListEditItem Text="Private level 2" Value="2" />
                            <dx:ListEditItem Text="Private level 3" Value="3" />
                            <dx:ListEditItem Text="Private level 4" Value="4" />
                            <dx:ListEditItem Text="Private level 5" Value="5" />
                            <dx:ListEditItem Text="Private level 6" Value="6" />
                            <dx:ListEditItem Text="Private level 7" Value="7" />
                            <dx:ListEditItem Text="Private level 8" Value="8" />
                            <dx:ListEditItem Text="Private level 9" Value="9" />
                            <dx:ListEditItem Text="Private level 10" Value="10" />
                        </Items>
                    </dx:ASPxComboBox>
                    <%--<dx:ASPxCheckBox ID="chkPrivePhoto" runat="server" EncodeHtml="False" CheckBoxStyle-HorizontalAlign="Left" Visible="False"></dx:ASPxCheckBox>--%>
                </div></td>
                        <td><div style="width:5px;"></div></td>
                        <td>
                <asp:LinkButton ID="btnUpload1" runat="server" Text="" CssClass="btn btnUpload" OnClientClick="ShowLoading();">
                    <asp:Literal ID="btnUpload" runat="server" Text=""/>
                </asp:LinkButton>
                        </td>
                    </tr>
                </table>
                <asp:Label ID="uploadStatus" runat="server" Text="" style="display:block;color:Red;"></asp:Label>
            </div>
            <p class="approvale-note" style="margin:10px 0;"><asp:Literal ID="msg_ApprovalNote" runat="server"/></p>
        </div>

        <div class="clear"></div>
    </div>
    <div class="clear"></div>


        <dx:ASPxCallbackPanel ID="cbpnlPhotos" runat="server" 
            ShowLoadingPanel="False" 
            ShowLoadingPanelImage="False" 
            ClientInstanceName="cbpnlPhotos">
            <ClientSideEvents EndCallback="cbpnlPhotos_EndCallback" CallbackError="cbpnlPhotos_CallbackError"></ClientSideEvents>
            <PanelCollection>
<dx:PanelContent runat="server" SupportsDisabledAttribute="True">
    <asp:HiddenField ID="hdfDefaultPath" runat="server" />
    <div id="public-photos-wrap">
    <div class="public-photos">
        <img id="btnClose" src="//cdn.goomena.com/Images/spacer10.png" alt="[Close]" class="close-btn"/>
        <div class="page-title"><asp:Literal ID="msg_PubPhotos" runat="server"/></div>
        <div class="photos-list">
    <asp:ListView ID="lvPubPhotos" runat="server" GroupItemCount="4">
        <ItemTemplate>
            <li>
                <div class="d_big_photo">
                    <a class="js fancybox myphoto" href='<%# Eval("ImageUrl") %>' rel="public">
                        <img src="<%# Eval("ImageThumbUrl") %>" alt="" title="" class="<%# Eval("ImageCss") %>" />
                        <asp:Label ID="lblInfo" runat="server"
                            CssClass='<%# "photo-text " & Eval("InfoTextClass").Tostring() %>' 
                            Text='<%# Eval("NotApprovedPhotoInfoText") %>' 
                            Visible='<%# Not string.IsNullOrEmpty(Eval("NotApprovedPhotoInfoText").ToString()) %>'></asp:Label>
                    </a>
                    <div class="clear"></div>
                    <div class="mp_links">
                        <asp:HyperLink ID="lnkEdit" 
                            NavigateUrl="javascript:void(0);" runat="server" 
                            Text='<%# Eval("EditButtonText") %>' 
                            ToolTip='<%# Eval("EditButtonText") %>' 
                            Visible='<%# Eval("EditButtonVisible") %>'
                            CssClass="btn lnkEdit" />
                        <asp:LinkButton ID="btnDelete" runat="server" 
                            CommandArgument='<%# Eval("ImageId") %>' CommandName="DELETE" 
                            OnClientClick='<%# Eval("DeleteButtonClientClickText") %>' 
                            Text='<%# Eval("DeleteButtonText") %>' 
                            ToolTip='<%# Eval("DeleteButtonText") %>' 
                            CssClass="btn btnDelete" />
                        <asp:LinkButton ID="btnDefault" runat="server" 
                            CommandArgument='<%# Eval("ImageId") %>' CommandName="DEFAULT" 
                            Text='<%# Eval("DefaultButtonText") %>' 
                            Visible='<%# Eval("DefaultVisible") %>' OnClientClick="ShowLoading();"
                            ToolTip='<%# Eval("DefaultButtonText") %>' 
                            CssClass="btn btnDefault"></asp:LinkButton>
                        <asp:HiddenField ID="hdfDisplayLevel" runat="server" 
                            Value='<%# Eval("DisplayLevel") %>' Visible="False" />
                        <asp:HiddenField ID="hdfImageID" runat="server" 
                            Value='<%# Eval("ImageId") %>' Visible="False" />
                    </div>
<dx:ASPxPopupControl SkinID="None" EncodeHtml="False" ID="editPhotoWindow" runat="server"
        EnableViewState="False" 
        EnableHotTrack="False" 
        PopupHorizontalAlign="Center"
        PopupVerticalAlign="Above" 
        PopupHorizontalOffset="1" 
        PopupVerticalOffset="-4"
        EnableHierarchyRecreation="True" 
        CloseAction="MouseOut" 
        HeaderText="Επίπεδο φωτογραφίας" 
        PopupAction="MouseOver" 
        RenderMode="Lightweight">
        <ClientSideEvents />
        <ClientSideEvents />
        <Windows>
            <dx:PopupWindow PopupElementID="lnkEdit" CloseAction="OuterMouseClick" PopupAction="LeftMouseClick">
                <ContentCollection>
                    <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server" SupportsDisabledAttribute="True">
                        <asp:Label ID="lblInfoInsidePopup" runat="server" Text=""/>
                        <dx:ASPxComboBox ID="cbPhotosLevel" runat="server" 
                            PopupHorizontalAlign="LeftSides" SelectedIndex="0" Width="150px">
    <ClientSideEvents SelectedIndexChanged="function(s, e) {  OnPubPhotoDisplayLevelChanged(s,e); }" EndCallback="function(s, e) {HideLoading();}" />
    <Items>
        <dx:ListEditItem Selected="True" Text="Public" Value="0" />
        <dx:ListEditItem Text="Private level 1" Value="1" />
        <dx:ListEditItem Text="Private level 2" Value="2" />
        <dx:ListEditItem Text="Private level 3" Value="3" />
        <dx:ListEditItem Text="Private level 4" Value="4" />
        <dx:ListEditItem Text="Private level 5" Value="5" />
        <dx:ListEditItem Text="Private level 6" Value="6" />
        <dx:ListEditItem Text="Private level 7" Value="7" />
        <dx:ListEditItem Text="Private level 8" Value="8" />
        <dx:ListEditItem Text="Private level 9" Value="9" />
        <dx:ListEditItem Text="Private level 10" Value="10" />
    </Items>
                        </dx:ASPxComboBox>
                    </dx:PopupControlContentControl>
                </ContentCollection>
            </dx:PopupWindow>
        </Windows>
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl10" runat="server" SupportsDisabledAttribute="True">
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
                </div>
            </li>
        </ItemTemplate>
        <GroupSeparatorTemplate>
            <div class="clear"></div>
        </GroupSeparatorTemplate>
        <GroupTemplate>
            <ul>
                <li ID="itemPlaceholder" runat="server"></li>
            </ul>
        </GroupTemplate>
        <ItemSeparatorTemplate></ItemSeparatorTemplate>
        <LayoutTemplate>
        <div class="profile_gallery">
            <div ID="groupPlaceholder" runat="server"></div>
            <div class="clear"></div>
        </div>
        </LayoutTemplate>
    </asp:ListView>
        </div>  
    </div>
    </div>
	<!-- end public / begin private -->

    <div class="private-photos">
        <div class="page-title"><asp:Literal ID="msg_PrivatePhotos" runat="server"/></div>
        <div class="photos-list">
    <asp:ListView ID="lvPrivePhotos" runat="server" GroupItemCount="4">
        <ItemTemplate>
            <li>
                <div class="d_big_photo">
                    <a class="js fancybox myphoto" href='<%# Eval("ImageUrl") %>' rel="prive">
                        <img src="<%# Eval("ImageThumbUrl") %>" alt="" title="" class="<%# Eval("ImageCss") %>" />
                        <span id="notificationsCountWrapper" class="jewelCount level<%# Eval("DisplayLevel") %>" title="<%= lblPhotoLevel.Text %><%# Eval("DisplayLevel") %>"><span class="countValue"><%# Eval("DisplayLevel") %></span></span>
                        <asp:Label ID="lblInfo" runat="server" 
                            CssClass='<%# "photo-text " & Eval("InfoTextClass").Tostring() %>' 
                            Text='<%# Eval("NotApprovedPhotoInfoText") %>'
                            Visible='<%# Not string.IsNullOrEmpty(Eval("NotApprovedPhotoInfoText").ToString()) %>' />
                    </a>
                    <div class="clear"></div>
                    <div class="mp_links">
                        <asp:HyperLink ID="lnkEdit" 
                            NavigateUrl="javascript:void(0);" runat="server" 
                            Text='<%# Eval("EditButtonText") %>' 
                            ToolTip='<%# Eval("EditButtonText") %>' 
                            Visible='<%# Eval("EditButtonVisible") %>'
                            CssClass="btn lnkEdit" />
                        <asp:LinkButton ID="btnDelete" runat="server" 
                            CommandArgument='<%# Eval("ImageId") %>' CommandName="DELETE" 
                            OnClientClick='<%# Eval("DeleteButtonClientClickText") %>' 
                            Text='<%# Eval("DeleteButtonText") %>'
                            ToolTip='<%# Eval("DeleteButtonText") %>' 
                            CssClass="btn btnDelete"></asp:LinkButton>
                        <asp:HiddenField ID="hdfDisplayLevel" runat="server" 
                            Value='<%# Eval("DisplayLevel") %>' Visible="False" />
                        <asp:HiddenField ID="hdfImageID" runat="server" 
                            Value='<%# Eval("ImageId") %>' Visible="False" />
                        <div class="clear">
                        </div>
                    </div>
<dx:ASPxPopupControl SkinID="None" EncodeHtml="False" ID="editPhotoWindow" runat="server"
        EnableViewState="False" EnableHotTrack="False" 
        PopupHorizontalAlign="Center"
        PopupVerticalAlign="Above" 
        PopupHorizontalOffset="1" 
        PopupVerticalOffset="-4"
        EnableHierarchyRecreation="True" 
        CloseAction="MouseOut" 
        HeaderText="Επίπεδο φωτογραφίας" 
        PopupAction="MouseOver" 
        RenderMode="Lightweight">
        <ClientSideEvents />
        <Windows>
            <dx:PopupWindow PopupElementID="lnkEdit" CloseAction="OuterMouseClick" PopupAction="LeftMouseClick">
                <ContentCollection>
                    <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server" SupportsDisabledAttribute="True">
                        <asp:Label ID="lblInfoInsidePopup" runat="server" Text=""/>
                        <dx:ASPxComboBox ID="cbPhotosLevel" runat="server" 
                            PopupHorizontalAlign="LeftSides" SelectedIndex="1" Width="150px">
    <ClientSideEvents SelectedIndexChanged="function(s, e) {  OnPrivatePhotoDisplayLevelChanged(s, e); }" EndCallback="function(s, e) {HideLoading();}" />
    <Items>
        <dx:ListEditItem Selected="True" Text="Public" Value="0" />
        <dx:ListEditItem Text="Private level 1" Value="1" />
        <dx:ListEditItem Text="Private level 2" Value="2" />
        <dx:ListEditItem Text="Private level 3" Value="3" />
        <dx:ListEditItem Text="Private level 4" Value="4" />
        <dx:ListEditItem Text="Private level 5" Value="5" />
        <dx:ListEditItem Text="Private level 6" Value="6" />
        <dx:ListEditItem Text="Private level 7" Value="7" />
        <dx:ListEditItem Text="Private level 8" Value="8" />
        <dx:ListEditItem Text="Private level 9" Value="9" />
        <dx:ListEditItem Text="Private level 10" Value="10" />
    </Items>
                        </dx:ASPxComboBox>
                    </dx:PopupControlContentControl>
                </ContentCollection>
            </dx:PopupWindow>
        </Windows>
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl10" runat="server" SupportsDisabledAttribute="True">
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
                </div>
            </li>
        </ItemTemplate>
        <GroupSeparatorTemplate>
            <div class="clear"></div>
        </GroupSeparatorTemplate>
        <GroupTemplate>
            <ul>
                <li ID="itemPlaceholder" runat="server"></li>
            </ul>
        </GroupTemplate>
        <ItemSeparatorTemplate></ItemSeparatorTemplate>
        <LayoutTemplate>
        <div class="profile_gallery">
            <div ID="groupPlaceholder" runat="server"></div>
            <div class="clear"></div>
        </div>
        </LayoutTemplate>
    </asp:ListView>
            <div class="clear"></div>
        </div>
    </div>
                </dx:PanelContent>
</PanelCollection>
            </dx:ASPxCallbackPanel>
            
    <div class="important-note">
        <asp:Literal ID="msg_PrivatePhotosInfo" runat="server"/>
    </div>

	<div class="mp_info_container">
		<h2 runat="server" id="h2InfoTitle" class="collapsibleHeader" title="">
            <asp:HyperLink ID="lnkInfoExpColl" runat="server" NavigateUrl="javascript:void(0);" class="togglebutton">+</asp:HyperLink> <asp:Literal ID="msg_ManagePhotos_ND" runat="server"/></h2>
        <div class="mp_info" runat="server" id="divmp_info" >
	        <div class="arrow-up"></div>
            <asp:Literal ID="msg_ManagePhotosInfo" runat="server" />
        </div>
	</div>
		
	<div class="clear"></div>
</div>
			

﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class UcQuickSearch

    '''<summary>
    '''lblHeader control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblHeader As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''msg_AgeText control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents msg_AgeText As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''trcAge control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents trcAge As Global.DevExpress.Web.ASPxEditors.ASPxTrackBar

    '''<summary>
    '''msg_AgeMin control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents msg_AgeMin As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''msg_AgeMax control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents msg_AgeMax As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''msg_DistanceFromMe_ND control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents msg_DistanceFromMe_ND As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''trcDistance control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents trcDistance As Global.DevExpress.Web.ASPxEditors.ASPxTrackBar

    '''<summary>
    '''msg_DistanceVal control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents msg_DistanceVal As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''msg_BodyType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents msg_BodyType As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''cbBodyType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cbBodyType As Global.DevExpress.Web.ASPxEditors.ASPxCheckBoxList

    '''<summary>
    '''msg_HairColor control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents msg_HairColor As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''ddeHairColorWrap control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddeHairColorWrap As Global.DevExpress.Web.ASPxEditors.ASPxDropDownEdit

    '''<summary>
    '''chkHasPhoto control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkHasPhoto As Global.DevExpress.Web.ASPxEditors.ASPxCheckBox

    '''<summary>
    '''chkBeOnline control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkBeOnline As Global.DevExpress.Web.ASPxEditors.ASPxCheckBox

    '''<summary>
    '''UpdatePanel1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UpdatePanel1 As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''btnSearch control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnSearch As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''quickSearchUpdatePanel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents quickSearchUpdatePanel As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''divNoResultsChangeCriteria control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents divNoResultsChangeCriteria As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''msg_divNoResultsTryChangeCriteria_ND control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents msg_divNoResultsTryChangeCriteria_ND As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''rptQuickSearch control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rptQuickSearch As Global.System.Web.UI.WebControls.Repeater

    '''<summary>
    '''lnkPreviousPageResults control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkPreviousPageResults As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''lblPreviousPageResults control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPreviousPageResults As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lnkNextPageResults control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkNextPageResults As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''lblNextPageResults control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblNextPageResults As Global.System.Web.UI.WebControls.Label
End Class

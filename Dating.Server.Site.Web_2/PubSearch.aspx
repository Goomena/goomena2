﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Root.Master" CodeBehind="PubSearch.aspx.vb" 
    Inherits="Dating.Server.Site.Web.PubSearch" %>
<%@ Register src="~/UserControls/PublicSearch.ascx" tagname="PublicSearch" tagprefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        /*p{margin:0; padding:0; }*/
        body { font-family: "Segoe UI" , "Arial"; font-weight: lighter; }
        #hp_content { width: 100%; background-color: #EEEEEE; padding-top: 1px; padding-bottom: 12px; border-top: 4px solid #BF1D2A; margin-top: 20px; }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="insidemosaiccontainer" runat="server">
    <asp:Label ID="lblinside" runat="server" Text=""></asp:Label>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="content" runat="server">
    <div class="mid_bg pageContainer">
        <div class="hp_cr" id="hp_top">
        </div>
        <div id="hp_content">
            <div class="search-top">
                <div class="lfloat" style="white-space:nowrap;margin-right:10px;">
                    <asp:Label ID="msg_SortBy" runat="server" Text="Sort By" AssociatedControlID="cbSort"/>
                </div>
                <div class="lfloat"><dx:ASPxComboBox ID="cbSort" runat="server"
                                AutoPostBack="True" EncodeHtml="False">
                                <Items>
                                    <dx:ListEditItem Text="10" Value="10" Selected="true"  />
                                    <dx:ListEditItem Text="25" Value="25" />
                                    <dx:ListEditItem Text="50" Value="50" />
                                </Items>
<ClientSideEvents BeginCallback="ShowLoadingOnMenu" CallbackError="HideLoading" EndCallback="HideLoading" />
                            </dx:ASPxComboBox></div>


                <div class="rfloat"><dx:ASPxComboBox ID="cbPerPage" runat="server" style="width: 60px;" 
                                AutoPostBack="True">
                                <Items>
                                    <dx:ListEditItem Text="10" Value="10" Selected="true"  />
                                    <dx:ListEditItem Text="25" Value="25" />
                                    <dx:ListEditItem Text="50" Value="50" />
                                </Items>
<ClientSideEvents BeginCallback="ShowLoadingOnMenu" CallbackError="HideLoading" EndCallback="HideLoading" />
                            </dx:ASPxComboBox></div>
                <div class="rfloat" style="white-space:nowrap;margin-right:10px;">
                    <asp:Literal ID="msgResultsPerPage" runat="server">Results per page</asp:Literal>
                </div>
                <div class="clear"></div>
            </div>
            
            <div class="results-list">
                <asp:UpdatePanel ID="updOffers" runat="server">
                    <ContentTemplate>
                        <uc2:PublicSearch ID="ctlSearch" runat="server" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div class="pagination">
                <table align="center">
                    <tr>
                        <td><dx:ASPxPager ID="Pager" runat="server" ItemCount="3" ItemsPerPage="1" 
                                RenderMode="Lightweight" CssFilePath="~/App_Themes/Aqua/{0}/styles.css" 
                                CssPostfix="Aqua" CurrentPageNumberFormat="{0}" 
                                SpriteCssFilePath="~/App_Themes/Aqua/{0}/sprite.css" 
                                SeoFriendly="Enabled">
                                <lastpagebutton visible="True">
                                </lastpagebutton>
                                <firstpagebutton visible="True">
                                </firstpagebutton>
                                <Summary Position="Left" Text="Page {0} of {1} ({2} members)" Visible="false" />
                            </dx:ASPxPager></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

 
</asp:Content>

<%--<div class="results-list">
                
    <div class="result-item">
        <div class="pr-photo">
            <img src="http://photos.goomena.com/269/thumbs/31669dc3c42b4ada83158218a0969ae3.jpg" class="image-1" alt=""/>
        </div>
        <div class="result-inner">
            <div class="pr-details">
                <div class="pr-login"><asp:Literal ID="lblLogin" runat="server">Savva82</asp:Literal></div>
                <div class="pr-details-bottom">
                    <div class="pr-age"><asp:Literal ID="lblAge" runat="server">29 ετών</asp:Literal></div>
                    <div class="pr-other"><asp:Literal ID="lblDetails" runat="server">Γαλάτσι, Αττική<br />170cm - Λίγα κιλά παραπάνω<br />Μαύρα, Καστανά, Λευκός</asp:Literal></div>
                </div>
            </div>
            <div class="pr-actions">
                <div class="act-help"><asp:LinkButton ID="lnkHelp" runat="server">LinkButton</asp:LinkButton></div>
                <div class="act-other"><asp:LinkButton ID="lnkOther" runat="server" CssClass="shadow">LinkButton</asp:LinkButton></div>
                <div class="act-favore"><asp:LinkButton ID="lnkFav" runat="server" CssClass="shadow">LinkButton</asp:LinkButton></div>
                <div class="act-like"><asp:LinkButton ID="lnkLike" runat="server" CssClass="shadow">LinkButton</asp:LinkButton></div>
                <div class="act-distance"><img src="Images2/pub-search/car.png" alt=""/></div>
                <div class="clear"></div>
            </div>
        </div>
    </div>

</div>--%>


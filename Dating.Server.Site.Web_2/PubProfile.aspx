﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Root.Master" CodeBehind="PubProfile.aspx.vb" 
    Inherits="Dating.Server.Site.Web.PubProfile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<script type="text/javascript" src="http://cdn.goomena.com/Scripts/fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>
	<script type="text/javascript" src="http://cdn.goomena.com/Scripts/fancybox/source/jquery.fancybox.pack.js?v=2.1.4"></script>
	<script type="text/javascript" src="http://cdn.goomena.com/Scripts/fancybox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
	<script type="text/javascript" src="http://cdn.goomena.com/Scripts/fancybox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
	<script type="text/javascript" src="http://cdn.goomena.com/Scripts/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.5"></script>
	<link rel="stylesheet" type="text/css" href="http://cdn.goomena.com/Scripts/fancybox/source/jquery.fancybox.css?v=2.1.4" media="screen" />
	<link rel="stylesheet" type="text/css" href="http://cdn.goomena.com/Scripts/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" />
	<link rel="stylesheet" type="text/css" href="http://cdn.goomena.com/Scripts/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />
    <style type="text/css">
body { font-family: "Segoe UI" , "Arial"; font-weight: lighter; }
#hp_content { width: 100%; background-color: #EEEEEE; padding-top: 1px; padding-bottom: 12px; border-top: 4px solid #BF1D2A; margin-top: 20px; }
#hp_content.vpr{width: 1008px; margin:0 auto 0;}
.vpr .top-info{background: url("http://cdn.goomena.com/Images2/pub/bg-profile.png") no-repeat scroll 0 0; width: 1008px; height: 104px; position:relative;margin-top:50px;}
.vpr .top-photo{background: url("http://cdn.goomena.com/Images2/pub/cycle.png") no-repeat scroll 0 0; width: 176px; height: 177px; position: absolute; z-index: 10;top:-25px;left:15px; }
.vpr .image-1 { -webkit-border-radius: 50%; -moz-border-radius: 50%; border-radius: 50%; display: block; width: 147px; height: 147px; 
                position:relative;top:5px;left:8px;}
.vpr .top-details{color:#fff; position: absolute;top:5px;left:200px;right:200px;}
.vpr .top-indicator{font-size:16px;position: absolute;top:12px;left:900px;bottom:5px;width:200px;}
.vpr .top-login{font-size:30px;}
.vpr .top-may-trip{font-size:13px;}
.vpr .top-my-header{font-size:13px;color:#24A3D6;}
.vpr .top-detail1{font-size:16px;}
.vpr .bottom{width:800px;margin:20px auto;}
.vpr .bottom-header{color:#000;background-color:#fff;font-size:16px;padding:10px 5px;position:relative;margin:20px 0 13px;}
.vpr .bottom-header .arrow{background:url("http://cdn.goomena.com/Images2/pub/arrow.png") no-repeat scroll 0% 100%;width:63px;height:14px;position:absolute;bottom:-11px;left:0px;}
.vpr .bottom-text{color:#000;background-color:#CFCFCF;font-size:13px;padding:10px 5px;}
.vpr .image-online{background:transparent url("http://cdn.goomena.com/Images2/pub/online.png") no-repeat scroll 0% 100%;width: 82px; height: 82px;}
.vpr .image-offline{background:transparent url("http://cdn.goomena.com/Images2/pub/offline.png") no-repeat scroll 0% 100%;width: 82px; height: 82px;}
.vpr .statistics{font-size:13px;white-space:nowrap;float:right;color:#000;position: absolute;top:-25px;right:10px;}
.vpr .photos-list{margin-top:50px;margin-left:10px;float:left;}
.vpr .d_big_photo {float:left;margin-right:5px;margin-bottom:8px;-webkit-box-shadow: 2px 2px 3px rgba(50, 50, 50, 0.25);
        -moz-box-shadow:    2px 2px 3px rgba(50, 50, 50, 0.25);
        box-shadow:         2px 2px 3px rgba(50, 50, 50, 0.25);border: 5px solid #fff;}
.vpr .d_big_photo img{width:127px;height:127px;border: 0px;padding:1px;background:#fff}
.vpr .d_big_photo .text_content{margin:0 auto;text-align:center;}
ul.defaultStyle{list-style: none outside none;margin:0;padding:0;}
.vpr .pr-actions {bottom: -45px;}
.vpr .details-info {width:500px;margin:60px 20px 20px 10px;padding:20px;background-color:#fff;float:right;font-size:15px;}
.vpr .details-info .details-title {font-weight:bold;}
.vpr .pr-actions a.shadow{background-color:#F2F2F2;}
.vpr .act-other a {display: block;background: none;padding: 3px 10px 4px 10px;}

.items_none { font-family:'Segoe UI',Helvetica,Arial; box-sizing: border-box; background: url("http://cdn.goomena.com/Images2/mbr2/like-box.png") no-repeat; width: 602px; height: 435px; margin: 35px 0px 0px 50px; padding: 50px 45px 0px 45px; position: relative; }
.items_none h2 { font-family: 'Segoe UI Semibold' ,Helvetica,Arial; font-size: 20px; color: #26A5DA; font-weight: normal; letter-spacing: 0; margin: 0 0 25px; text-align: center; }
.items_none p { font-size: 14px; color: #000; text-align: center; padding: 0 0 0 5px; margin: 20px 0 0; }
.items_none h3 { font-size: 18px; text-align: center; margin: 30px 0 0; }
.items_none { padding: 0; }
.items_none_text { padding: 50px 45px 0px 45px; }
.vpr .items_none.no-results{background-image:none;text-align:center;padding:25px 0 20px;height:auto;margin-left:0;width:auto;margin:80px 0 100px;}
.vpr .items_none.no-results h3{font-weight:bold;font-size:25px;margin:0;}
.vpr .items_none.no-results h4{font-weight:normal;font-size:25px;margin:0;}
.vpr .items_none.no-results h5{font-weight:bold;font-size:25px;margin:7px 0;}
.vpr .items_none.no-results a{color: #26A5DA;font-size:16px;margin:0 0 7px;font-family:'Segoe UI Semibold';text-decoration:underline;display:inline-block;}

    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="content" runat="server">
    <div class="mid_bg pageContainer">
        <div class="hp_cr" id="hp_top">
        </div>

        <div id="hp_content" class="vpr">

            <div class="items_none no-results" id="divNoResultsChangeCriteria" runat="server" visible="false">
                <asp:Literal ID="msg_divNoResultsTryChangeCriteria_ND" runat="server"/>
	            <div class="clear"></div>
                <script type="text/javascript">
                    setTimeout(function () {
                        jsLoad('<%= ResolveUrl("~/Default.aspx") %>');
                    }, 3000);
                </script>
            </div>

            <asp:Panel id="pnlResults" runat="server">
            <div class="top-info" style="white-space:nowrap;">
                <div class="statistics">
			        <asp:Label ID="msg_UserInsightTitle" runat="server" />&nbsp;:&nbsp;<asp:Literal ID="msg_LastLoggedInDT" runat="server" />&nbsp;<asp:Label ID="lblLastLoginDT" runat="server" style="white-space:nowrap;" />							
		        </div>
                <div class="top-photo"><asp:HyperLink ID="lnkPhoto" runat="server" NavigateUrl="~/Register.aspx" onclick="ShowLoading();"><asp:Image runat="server" ID="imgProfile" AlternateText="" ImageUrl="//cdn.goomena.com/Images/spacer10.png" CssClass="image-1"/></asp:HyperLink></div>
                <div class="top-details">
                    <asp:Label ID="lblLogin" runat="server" CssClass="top-login" Text="login"/>
                    <asp:Label ID="lblWillTravel" runat="server" CssClass="top-may-trip" Text="may-trip"/>
                    <asp:Label ID="lblAboutMeHeading" runat="server" CssClass="top-my-header" Text="my-header"/><br />
                    <asp:Label ID="lblLocation" runat="server" CssClass="top-detail1" Text="detail1"/><br />
                    <asp:Label ID="lblBirth" runat="server" CssClass="top-detail1" Text="detail1"/>
                </div>
                <div class="top-indicator"><asp:Image runat="server" ID="imgOnline" AlternateText="" 
                    ImageUrl="//cdn.goomena.com/Images/spacer10.png" CssClass="image-offline"/></div>
                <div class="pr-actions">
                    <div class="act-other"><asp:HyperLink ID="lnkFav" runat="server" CssClass="shadow" Text="Favorite" NavigateUrl="~/Register.aspx" onclick="ShowLoading();"/></div>
                    <asp:PlaceHolder ID="PlaceHolder1" runat="server" Visible ="false" >
                      <div class="act-other"><asp:HyperLink ID="lnkOffer" runat="server" CssClass="shadow" Text="Offer" NavigateUrl="~/Register.aspx" onclick="ShowLoading();"/></div>
                   </asp:PlaceHolder>
                         <div class="act-favore"><asp:HyperLink ID="lnkMsg" runat="server" CssClass="shadow" Text="Message" NavigateUrl="~/Register.aspx" onclick="ShowLoading();"/></div>
                    <div class="act-like"><asp:HyperLink ID="lnkLike" runat="server" CssClass="shadow" Text="Like" NavigateUrl="~/Register.aspx" onclick="ShowLoading();"/></div>
                    <div class="clear"></div>
                </div>
            </div>

            <div class="photos-list" style="width:350px;">
<asp:ListView ID="lvPhotos" runat="server">
    <LayoutTemplate>
<div class="profile_gallery p_filter">
    <div runat="server" id="itemPlaceholder"></div>
	<div class="clear"></div>
</div>
    </LayoutTemplate>
    <ItemTemplate>
    <div class="d_big_photo">
        <a rel="prof_group" class="js fancybox" href="<%# Eval("ImageUrl") %>">
            <img src="<%# Eval("ImageThumbUrl") %>" /></a></div>
    </ItemTemplate>
</asp:ListView>
            </div> 
                       
            <div class="details-info">
            <table style="width:100%;">
            <tr>
                <td valign="top"><ul class="defaultStyle">
				    <li><span class="details-title"><asp:Literal ID="msg_Age" runat="server" /></span>&nbsp;:&nbsp;<asp:Literal ID="lblAge" runat="server" /> <asp:Literal ID="lblZwdio" runat="server" Text="" /></li>
				    <li><span class="details-title"><asp:Literal ID="msg_BodyType" runat="server" /></span>&nbsp;:&nbsp;<asp:Literal ID="lblHeight" runat="server" /> - <asp:Literal ID="lblBodyType" runat="server" /></li>
				    <li><span class="details-title"><asp:Literal ID="msg_HairEyes" runat="server" /></span>&nbsp;:&nbsp;<asp:Literal ID="lblHairClr" runat="server" /> / <asp:Literal ID="lblEyeClr" runat="server" /></li>
				    <li><span class="details-title"><asp:Literal ID="msg_Education" runat="server" /></span>&nbsp;:&nbsp;<asp:Literal ID="lblEducation" runat="server"  /></li>
				    <li><span class="details-title"><asp:Literal ID="msg_Children" runat="server" /></span>&nbsp;:&nbsp;<asp:Literal ID="lblChildren" runat="server"  /></li>
				    <li><span class="details-title"><asp:Literal ID="msg_Ethnicity" runat="server" /></span>&nbsp;:&nbsp;<asp:Literal ID="lblEthnicity" runat="server"  /></li>
				    <li><span class="details-title"><asp:Literal ID="msg_Religion" runat="server" /></span>&nbsp;:&nbsp;<asp:Literal ID="lblReligion" runat="server"  /></li>
				    <li><span class="details-title"><asp:Literal ID="msg_SmokingDrinking" runat="server" /></span>&nbsp;:&nbsp;<asp:Literal ID="lblSmoking" runat="server"  /> / <asp:Literal ID="lblDrinking" runat="server"  /></li>
			        <li><span class="details-title"><asp:Literal ID="msg_Occupation" runat="server" /></span>&nbsp;:&nbsp;<asp:Literal ID="lblOccupation" runat="server" /></li>
			        <li runat="server" id="liAnnualIncome" visible="false"><span class="details-title"><asp:Literal ID="msg_IncomeLevel" runat="server" /></span>&nbsp;:&nbsp;<asp:Literal ID="lblIncomeLevel" runat="server" /></li>								
			        <li runat="server" id="liNetWorth" visible="false"><span class="details-title"><asp:Literal ID="msg_NetWorth" runat="server" /></span>&nbsp;:&nbsp;<asp:Literal ID="lblNetWorth" runat="server" /></li>
			        <li><span class="details-title"><asp:Literal ID="msg_RelationshipStatus" runat="server" /></span>&nbsp;:&nbsp;<asp:Literal ID="lblRelationshipStatus" runat="server" /></li>
			        <li><span class="details-title"><asp:Literal ID="msg_LookingToMeet" runat="server" /></span>&nbsp;:&nbsp;<asp:Literal ID="lblLookingToMeet" runat="server" /></li>
			        <li><span class="details-title"><asp:Literal ID="msg_InterestedIn" runat="server" /></span>&nbsp;:
                    <asp:ListView ID="lvTypeOfDate" runat="server">
                        <LayoutTemplate>
                        <ul style="padding-left:20px;" class="defaultStyle">
                            <li runat="server" id="itemPlaceholder"></li>
                        </ul>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <li><%# Eval("TypeOfDateText") %></li>
                        </ItemTemplate>
                    </asp:ListView>
			        </li>
		        </ul></td>
            </tr>
            </table>
            </div>
            <div class="clear"></div>

            <div class="bottom">
                <div ID="divAboutMe" runat="server">
                    <div class="bottom-header"><asp:Literal ID="msg_AboutMeDescrTitle" runat="server" /><div class="arrow"></div></div>
                    <div class="bottom-text"><asp:Literal ID="lblAboutMeDescr" runat="server" /></div>
                </div>
                <div ID="divFirstDateExpectation" runat="server">
                    <div class="bottom-header"><asp:Literal ID="msg_FirstDateExpectationDescrTitle" runat="server" /><div class="arrow"></div></div>
                    <div class="bottom-text" ><asp:Literal ID="lblFirstDateExpectationDescr" runat="server" /></div>
                </div>
            </div>
            </asp:Panel>
        </div>
    </div>

    <script type="text/javascript">
// <![CDATA[
        function loadFancybox() {
            $("a[rel=prof_group]").fancybox({
                'transitionIn': 'elastic',
                'transitionOut': 'elastic',
                'titlePosition': 'over',
                'overlayColor': '#000'
            });
        }

        function pageLoad(sender, args) {
            loadFancybox();
        }
// ]]> 
    </script>

</asp:Content>



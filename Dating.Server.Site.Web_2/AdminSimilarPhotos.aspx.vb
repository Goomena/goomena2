﻿Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Core.DLL

Public Class AdminSimilarPhotos
    Inherits BasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            Dim photoid As Long = 0
            If (Not String.IsNullOrEmpty(Request("photoid"))) Then
                Long.TryParse(Request("photoid"), photoid)
            End If
            lblContent.Text = ""
            If (photoid > 0) Then
                GetPhotoFromSYS_EmailsQueue(photoid)
                If (lblContent.Text = "") Then lblContent.Text = "data not found"
            Else
                lblContent.Text = "Please specify photo id"
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "AdminSimilarPhotos-->Page_Load")
        End Try
    End Sub


    Private Sub GetPhotoFromSYS_EmailsQueue(photoid As Long)
        Try
            Using cmdb As CMSDBDataContext = Me.GetCMSDBDataContext


                Dim ph As EUS_CustomerPhoto = (From itm In cmdb.EUS_CustomerPhotos
                                                Where itm.CustomerPhotosID = photoid
                                                Select itm).FirstOrDefault()




                If (clsNullable.NullTo(ph.PhotoSimilarity) > 0) Then

                    Dim a As SYS_EmailsQueue = (From itm In cmdb.SYS_EmailsQueues
                                                Where itm.EmailAction = "New.Photo.Google.Check.ID-" & photoid
                                                Select itm).FirstOrDefault()


                    If (a IsNot Nothing AndAlso Not String.IsNullOrEmpty(a.Body)) Then
                        lblContent.Text = a.Body
                    Else

                        Dim phList As List(Of EUS_CustomerPhotosSimilar) = (From itm In cmdb.EUS_CustomerPhotosSimilars
                                        Where itm.CustomerPhotosID = photoid
                                        Select itm).ToList()


                        Dim Content_Similar As String = <h><![CDATA[
<div>Similar [SIMILARITY]%</div>
<div><img src="[PHOTO-URL]"  style="width:250px" width="250"/></div>
<div><a href="[PHOTO-URL]" target="_blank">[PHOTO-URL]</a></div>
<br/> 
<br/>
]]></h>

                        Dim htmlTable As New StringBuilder()
                        '    Dim prevLogin As String = ""

                        For f1 = 0 To phList.Count - 1
                            Dim itm = phList(f1)
                            Dim simPhoto As String = Content_Similar.Replace("[PHOTO-URL]", itm.PhotoURL)
                            simPhoto = simPhoto.Replace("[SIMILARITY]", Math.Round(System.Convert.ToDecimal(itm.PhotoSimilarity) * 100, 2))
                            htmlTable.AppendLine(simPhoto)
                        Next

                        lblContent.Text = htmlTable.ToString()


                    End If

                End If
            End Using
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "AdminSimilarPhotos-->GetPhotoFromSYS_EmailsQueue")
        End Try
    End Sub


End Class
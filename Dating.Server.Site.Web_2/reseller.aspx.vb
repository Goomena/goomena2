﻿Imports System.IO
Imports System.Security.Cryptography
Imports Newtonsoft.Json
Imports Dating.Server.Datasets.DLL
Imports System.Globalization
Imports Dating.Server.Core.DLL.clsCustomer
Imports Dating.Server.Core.DLL

Public Class reseller
    Inherits BasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try


            Dim json As String = ""
            Using reader As New StreamReader(Request.InputStream)
                json = reader.ReadToEnd()

                If json.Length > 0 Then
                    Dim o As Input = JsonConvert.DeserializeObject(Of Input)(json)
                    Dim tmp As String = "6UU5-T5ZK-D8PH" & JsonConvert.SerializeObject(o.Data)
                    Dim myhash As String = GetHash(tmp)
                    If myhash = o.Hash Then
                        If Not CheckIfOrderExists(o.Data) Then
                            Dim order As Eus_ResellerIpn = GetnewOrder(o)
                            If order IsNot Nothing Then
                                order.Json = json
                                Using cont = GetCMSDBDataContext


                                    Dim productionIndx As Integer = 0
                                    For Each pr As Products In o.Data.Products
                                        productionIndx += 1
                                        Dim profile As EUS_Profile = (From itm In cont.EUS_Profiles
                                                                      Where itm.LoginName.ToLower = pr.GooUserName.ToLower _
                                                                      AndAlso itm.GenderId = 1 AndAlso itm.IsMaster = True).FirstOrDefault
                                        If profile IsNot Nothing Then
                                            Dim indofchr As Integer = pr.Product_name.IndexOf("-")
                                            Dim credits As Integer = Num(If(indofchr > 0, pr.Product_name.Substring(0, indofchr), pr.Product_name))
                                            Dim prm As New AddTransactionParams
                                            prm.PromoCode = "ReselerPromo"
                                            prm.Currency = "EUR"
                                            prm.paymethod = PaymentMethods.Reseller
                                            prm.CustomReferrer = profile.CustomReferrer
                                            prm.CustomerID = profile.ProfileID
                                            prm.PayerEmail = profile.eMail
                                            Select Case credits
                                                Case 1000
                                                    prm.CreditsPurchased = 1000
                                                    prm.Money = 15
                                                    prm.durationDays = 45
                                                Case 3000
                                                    prm.CreditsPurchased = 3000
                                                    prm.Money = 36
                                                    prm.durationDays = 90
                                                Case 6000
                                                    prm.CreditsPurchased = 6000
                                                    prm.Money = 66
                                                    prm.durationDays = 180
                                                Case 10000
                                                    prm.CreditsPurchased = 10000
                                                    prm.Money = 99.9
                                                    prm.durationDays = 180
                                            End Select
                                            Dim Reference As String = "RModule "
                                            Reference &= " Credits: " & prm.CreditsPurchased ''& vbNewLine & json
                                            prm.Description = Reference

                                            prm.REMOTE_ADDR = profile.LastLoginIP
                                            Dim qua As Integer = CInt(pr.Quantity)
                                            If qua > 0 Then

                                                For i As Integer = 1 To qua
                                                    Try
                                                        prm.TransactionInfo = Reference & " - PrI: " & productionIndx & " - QId: " & i & " - OId: " & o.Data.OrderId & " - CId: " & o.Data.CartId
                                                        Dim Transactionid As Integer = clsCustomer.AddTransaction(prm)
                                                        Try

                                                            Using IPNNotifier As New IPNNotifier
                                                                IPNNotifier.UniPayTransactionID = Transactionid
                                                                IPNNotifier.CustomerEmaiAddress = If(prm.PayerEmail IsNot Nothing, prm.PayerEmail, "")
                                                                IPNNotifier.RefereceNumber = i
                                                                IPNNotifier.amount = prm.Money
                                                                IPNNotifier.status = "YConfirmed"
                                                                IPNNotifier.custFirstName = ""
                                                                IPNNotifier.custLastName = ""
                                                                IPNNotifier.custAddress = ""
                                                                IPNNotifier.custCity = ""
                                                                IPNNotifier.custState = ""
                                                                IPNNotifier.custCountry = ""
                                                                IPNNotifier.custZip = ""
                                                                IPNNotifier.customerIP = If(prm.REMOTE_ADDR IsNot Nothing, prm.REMOTE_ADDR, "")
                                                                IPNNotifier.feeAmount = 0
                                                                IPNNotifier.netAmount = IPNNotifier.amount
                                                                IPNNotifier.currency = If(prm.Currency IsNot Nothing, prm.Currency, "EUR")
                                                                IPNNotifier.memberID = If(prm.MemberID IsNot Nothing, prm.MemberID, "")
                                                                Select Case prm.CreditsPurchased
                                                                    Case 1000
                                                                        IPNNotifier.STRproductCode = "dd1000Credits"
                                                                    Case 3000
                                                                        IPNNotifier.STRproductCode = "dd3000Credits"
                                                                    Case 6000
                                                                        IPNNotifier.STRproductCode = "dd6000Credits"
                                                                    Case 10000
                                                                        IPNNotifier.STRproductCode = "dd10000Credits"

                                                                End Select
                                                                IPNNotifier.paymentSubType = ""
                                                                IPNNotifier.siteid = 5
                                                                IPNNotifier.loginname = If(prm.PayerEmail IsNot Nothing, prm.PayerEmail, "")
                                                                IPNNotifier.customerid = prm.CustomerID
                                                                IPNNotifier.customReferrer = If(prm.HTTP_REFERER IsNot Nothing, prm.HTTP_REFERER, "")
                                                                Dim count As Integer = 0
                                                                Try
                                                                    count = (From itm As EUS_CustomerTransaction In cont.EUS_CustomerTransactions
                         Where itm.CustomerTransactionID <> Transactionid AndAlso itm.CustomerID = profile.ProfileID AndAlso itm.PaymentMethods.ToLower = prm.paymethod.ToString.ToLower
                                                                     Select itm).Count
                                                                Catch ex As Exception

                                                                End Try

                                                                IPNNotifier.isRebill = (count > 0)
                                                                IPNNotifier.paymentMethod = "Reseller"
                                                                Dim s As String = IPNNotifier.SendResults("http://goomena.com/pay/epochipn.asmx")
                                                                Dim tra As EUS_CustomerTransaction = (From itm In cont.EUS_CustomerTransactions
                                                                                                     Where itm.CustomerTransactionID = Transactionid
                                                                                                     Select itm).FirstOrDefault
                                                                tra.Notes = "CentralIpn:" & s
                                                                cont.SubmitChanges()

                                                            End Using
                                                        Catch ex As Exception

                                                        End Try

                                                    Catch ex As Exception
                                                    End Try

                                                Next
                                            Else
                                                Response.ClearContent()
                                                Response.Write("ErrorInQuantity")
                                                Response.End()
                                            End If
                                        Else
                                            Response.ClearContent()
                                            Response.Write("ErrorMatchingTheProfile")
                                            Response.End()

                                        End If
                                    Next
                                    cont.Eus_ResellerIpns.InsertOnSubmit(order)
                                    cont.SubmitChanges()
                                    Response.ClearContent()
                                    Response.Write("OK")
                                    Response.End()
                                End Using
                            Else
                                Response.ClearContent()
                                Response.Write("ErrorOnCreatingOrder")
                                Response.End()
                            End If
                        Else
                            Response.ClearContent()
                            Response.Write("OrderExists")
                            Response.End()
                        End If

                    Else
                        Response.ClearContent()
                        Response.Write("Bad Hash")
                        Response.End()
                    End If
                Else
                    Response.Redirect(Page.ResolveUrl("~/default.aspx"), True)
                End If

            End Using
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception

        End Try
    End Sub
    Private Shared Function Num(ByVal value As String) As Integer
        Dim returnVal As String = String.Empty
        Dim collection As MatchCollection = Regex.Matches(value, "\d+")
        For Each m As Match In collection
            returnVal += m.ToString()
        Next
        Return Convert.ToInt32(returnVal)
    End Function
    Protected Function GetnewOrder(ByVal o As Input) As Eus_ResellerIpn

        Dim order As New Eus_ResellerIpn
        Try

            order.OrderId = CLng(o.Data.OrderId)
            order.CartId = CLng(o.Data.CartId)
            order.CustomerId = CLng(o.Data.CustomerId)
            order.Payment = o.Data.Payment

            Dim provider As CultureInfo = CultureInfo.InvariantCulture
            order.Date = DateTime.ParseExact(o.Data.Date, "yyyy-MM-dd HH:mm:ss", provider)
            order.DateUpdated = DateTime.ParseExact(o.Data.DateUpdated, "yyyy-MM-dd HH:mm:ss", provider)
            order.hash = o.Hash
        Catch ex As Exception
            order = Nothing
        End Try
        Return order
    End Function
    Protected Function CheckIfOrderExists(ByVal OrderData As OrderData) As Boolean
        Using cont = GetCMSDBDataContext
            Dim order As Eus_ResellerIpn = (From itm In cont.Eus_ResellerIpns
                                         Where itm.CartId = OrderData.CartId AndAlso itm.OrderId = OrderData.OrderId
                                         Select itm).FirstOrDefault
            If order Is Nothing Then
                Return False
            Else
                Return True
            End If
        End Using
        Return False
    End Function

    Shared Function GetHash(theInput As String) As String

        Using hasher As MD5 = MD5.Create()    ' create hash object

            ' Convert to byte array and get hash
            Dim dbytes As Byte() =
                 hasher.ComputeHash(Encoding.UTF8.GetBytes(theInput))

            ' sb to create string from bytes


            Return Convert.ToBase64String(dbytes)
        End Using

    End Function
    Public Class Input
        Public Hash As String
        Public Data As OrderData

    End Class
    Public Class OrderData
        <JsonProperty(Order:=1)>
        Public OrderId As String
        <JsonProperty(Order:=2)>
        Public CartId As String
        <JsonProperty(Order:=3)>
        Public CustomerId As String
        <JsonProperty(Order:=4)>
        Public Payment As String
        <JsonProperty(Order:=5)>
        Public [Date] As String
        <JsonProperty(Order:=6)>
        Public DateUpdated As String
        <JsonProperty(Order:=7)>
        Public Products As Products()
    End Class
    Public Class Products
        <JsonProperty(Order:=1)>
        Public ProductId As String
        <JsonProperty(Order:=2)>
        Public Quantity As String
        <JsonProperty(Order:=3)>
        Public GooUserName As String
        <JsonProperty(Order:=4)>
        Public Product_name As String
        <JsonProperty(Order:=5)>
        Public Cart As String
    End Class
End Class

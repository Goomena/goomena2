﻿Imports Library.Public
Imports Dating.Server.Core.DLL

Module ModGlobals

    Public Const Referrer_CODE_PATTERN As String = "GOM-[123]-[LOGIN]"
    Public Const Referrer_CODE_PATTERN_Check As String = "(GOM-(?<customer>\d+)-(?<login>(.{3})))"

    Public Const AllowUnlimited As Boolean = False
    'Public gSiteName As String = "www.dating-deals.com"
    Public gSiteName As String = ConfigurationManager.AppSettings("gSiteName")
    Public gLog As New Library.Public.ClsLOG("DatingDealsCom" & DateTime.Now.ToString("MMddyy-HHmm") & ".txt", True, "C:\WebSitesLogs")
    Public gER As New Library.Public.clsErrors(False, True, True, True)

    'Public gIsAdminLogin As Boolean = False
    Public gCarDistance As Integer = 600
    Public gDomainGR_DefaultLAG As String = "GR"
    Public gDomainCOM_DefaultLAG As String = "US"
    Public gLogin As New clsLogin()

    Public Const gDefaultThemeName As String = "Aqua"



    Public ReadOnly Property ConnectionString As String
        Get
            Return System.Configuration.ConfigurationManager.ConnectionStrings("AppDBconnectionString").ConnectionString
        End Get
    End Property

    Public Sub WebErrorMessageBox(ByVal WebPage As Object, ByVal ExtraMessage As String)
        ' try to show error with scriptmanager
        '   Dim sentToclient = False
        Try
            If (Not String.IsNullOrEmpty(ExtraMessage)) Then

                Dim jsmsg As String = ExtraMessage.Replace("'", "\'")
                If (jsmsg.Length > 70) Then jsmsg = jsmsg.Remove(70 & "...")

                jsmsg = vbCrLf & "writeUserErrorMsg('" & jsmsg & "');" & vbCrLf
                ScriptManager.RegisterClientScriptBlock(WebPage, WebPage.GetType(), "registerMessage", jsmsg, True)
            End If
        Catch exc As Exception
            gLog.Write(exc.Message)
        End Try
    End Sub

    Public Sub WebErrorMessageBox(ByVal WebPage As Object, ByVal ex As Exception, ByVal ExtraMessage As String)
        Try
            ' try to show error with scriptmanager
            Dim sentToclient = False
            Try
                If (Not String.IsNullOrEmpty(WebPage.ID) AndAlso WebPage.ID <> "__Page") Then
                    If (Not String.IsNullOrEmpty(ExtraMessage)) Then
                        ExtraMessage = ExtraMessage & vbCrLf & "Control ID: " & WebPage.ID
                    Else
                        ExtraMessage = "Control ID: " & WebPage.ID
                    End If
                End If

                Dim jsmsg As String = ex.Message
                If (ex.GetType() Is GetType(System.Data.SqlClient.SqlException)) Then
                    jsmsg = "Data retrieval exception occured. Please retry."
                ElseIf (ex.InnerException IsNot Nothing) Then
                    If (ex.InnerException.InnerException IsNot Nothing AndAlso (TypeOf ex.InnerException.InnerException Is System.Data.SqlClient.SqlException)) Then
                        jsmsg = "Data retrieval exception occured. Please retry."
                    ElseIf (TypeOf ex.InnerException Is System.Data.SqlClient.SqlException) Then
                        jsmsg = "Data retrieval exception occured. Please retry."
                    End If
                End If

                If (Not String.IsNullOrEmpty(ExtraMessage)) Then jsmsg = ExtraMessage & "---" & ex.Message
                If (jsmsg.Length > 70) Then jsmsg = jsmsg.Remove(70 & "...")

                jsmsg = jsmsg.Replace("'", "\'")
                jsmsg = vbCrLf & "writeUserErrorMsg('" & jsmsg & "');" & vbCrLf

                ScriptManager.RegisterClientScriptBlock(WebPage, WebPage.GetType(), "registerError", jsmsg, True)
                sentToclient = True
            Catch exc As Exception
                gLog.Write(exc.Message)
            End Try



            If (Not sentToclient) Then
                WebPage.Response.Write(ex.Message & " " & ExtraMessage)
            End If

            'Dim sz As String = "ERROR: " & ex.Message & " " & ExtraMessage & ex.TargetSite.ToString & " " & ex.Source.ToString
            'gLog.Write(sz)

            WebErrorSendEmail(ex, ExtraMessage)
        Catch exc As Exception
            gLog.Write(exc.Message)
        End Try
    End Sub


    Public Sub WebErrorSendEmail(ByVal ex As Exception, ByVal ExtraMessage As String)
        Dim sendException As Boolean = True
        If (HttpContext.Current.Request.Url.Host = "localhost") Then
            sendException = False
        End If
        If (sendException) Then
            Dim msg As String = ex.Message
            If (msg.Length > 200) Then msg = msg.Remove(197) & "..."
            msg = HttpUtility.UrlEncode(msg).
                Replace("+", " ")
            Dim subject As String = "Exception on " & gSiteName & " (" & msg & ")"
            Dim contetn As String = ""
            If (Not String.IsNullOrEmpty(ExtraMessage)) Then contetn = ExtraMessage & vbCrLf & vbCrLf


            If (HttpContext.Current IsNot Nothing) Then
                If (HttpContext.Current.User IsNot Nothing AndAlso Not String.IsNullOrEmpty(HttpContext.Current.User.Identity.Name)) Then
                    contetn = contetn & "Login name: " & HttpContext.Current.User.Identity.Name & vbCrLf & vbCrLf
                ElseIf (HttpContext.Current.Session IsNot Nothing AndAlso Not String.IsNullOrEmpty(HttpContext.Current.Session("LoginName"))) Then
                    contetn = contetn & "Login name: " & HttpContext.Current.Session("LoginName") & vbCrLf & vbCrLf
                End If
                contetn = contetn & ex.ToString() & vbCrLf
                contetn = contetn & "URL: " & HttpContext.Current.Request.Url.ToString() & vbCrLf
                contetn = contetn & "IP: " & HttpContext.Current.Request.ServerVariables("REMOTE_ADDR") & vbCrLf

                If (HttpContext.Current.Session IsNot Nothing) Then
                    contetn = contetn & "IP GEO: " & HttpContext.Current.Session("GEO_COUNTRY_CODE") & vbCrLf
                    contetn = contetn & "LAGID: " & HttpContext.Current.Session("LAGID") & vbCrLf
                End If
                Dim ref As String = HttpContext.Current.Request.ServerVariables("HTTP_REFERER")
                If (Not String.IsNullOrEmpty(ref)) Then
                    contetn = contetn & vbCrLf & "HTTP_REFERER: " & ref & vbCrLf
                End If
            Else
                contetn = contetn & ex.ToString()
            End If

            Dating.Server.Core.DLL.clsMyMail.SendMailSupport(ConfigurationManager.AppSettings("ExceptionsEmail"), subject, contetn)
        End If
    End Sub


    Public Sub WebInfoSendEmail(ByVal ExtraMessage As String)
        Dim sendException As Boolean = True
        If (HttpContext.Current.Request.Url.Host = "localhost") Then
            sendException = False
        End If
        If (sendException) Then
            Dim subject As String = "info Email from site " & gSiteName
            Dim contetn As String = ""
            If (Not String.IsNullOrEmpty(ExtraMessage)) Then contetn = ExtraMessage & vbCrLf & vbCrLf


            If (HttpContext.Current IsNot Nothing) Then
                If (HttpContext.Current.User IsNot Nothing AndAlso Not String.IsNullOrEmpty(HttpContext.Current.User.Identity.Name)) Then
                    contetn = contetn & "Login name: " & HttpContext.Current.User.Identity.Name & vbCrLf & vbCrLf
                ElseIf (HttpContext.Current.Session IsNot Nothing AndAlso Not String.IsNullOrEmpty(HttpContext.Current.Session("LoginName"))) Then
                    contetn = contetn & "Login name: " & HttpContext.Current.Session("LoginName") & vbCrLf & vbCrLf
                End If

                contetn = contetn & "URL: " & HttpContext.Current.Request.Url.ToString() & vbCrLf
                contetn = contetn & "IP: " & HttpContext.Current.Request.ServerVariables("REMOTE_ADDR") & vbCrLf

                If (HttpContext.Current.Session IsNot Nothing) Then
                    contetn = contetn & "IP GEO: " & HttpContext.Current.Session("GEO_COUNTRY_CODE") & vbCrLf
                    contetn = contetn & "LAGID: " & HttpContext.Current.Session("LAGID") & vbCrLf
                End If
                Dim ref As String = HttpContext.Current.Request.ServerVariables("HTTP_REFERER")
                If (Not String.IsNullOrEmpty(ref)) Then
                    contetn = contetn & vbCrLf & "HTTP_REFERER: " & ref & vbCrLf
                End If

            End If
            If (contetn.Trim().Length > 0) Then
                Dating.Server.Core.DLL.clsMyMail.SendMailSupport(ConfigurationManager.AppSettings("ExceptionsEmail"), subject, contetn)
            End If
        End If
    End Sub


    Public Sub UpdateUserControls(_control As Control, Optional checkLanguageChanged As Boolean = False)

        If (checkLanguageChanged) Then
            If (TypeOf _control Is ILanguageDependableContent) Then
                DirectCast(_control, ILanguageDependableContent).Master_LanguageChanged()
            End If
        End If

        For Each ctl As Control In _control.Controls
            If (ctl.HasControls()) Then
                UpdateUserControls(ctl, True)
            End If
        Next
    End Sub


    ''' <summary>
    ''' Set Text property of of basic controls. Skips devExpress controls, controls which full type name starts with "DevExpress.Web"
    ''' </summary>
    ''' <param name="_control"></param>
    ''' <param name="_pageData"></param>
    ''' <remarks></remarks>
    Public Sub SetControlsValue(ByRef _control As Control, ByRef _pageData As clsPageData)

        Dim checkChildrenControls = True

        If (_control.GetType().FullName.StartsWith("DevExpress.Web")) Then
            checkChildrenControls = False
        Else

            Dim valueString As String
            If (_control.ID IsNot Nothing) Then

                Dim ctlId As String = _control.ID
                If (ctlId.StartsWith("msg_")) Then
                    valueString = _pageData.GetCustomString(ctlId)

                    If (valueString IsNot Nothing) Then
                        If (TypeOf _control Is LinkButton) Then
                            CType(_control, LinkButton).Text = valueString
                            checkChildrenControls = False

                        ElseIf (TypeOf _control Is Label) Then
                            CType(_control, Label).Text = valueString
                            checkChildrenControls = False

                        ElseIf (TypeOf _control Is Literal) Then
                            CType(_control, Literal).Text = valueString
                            checkChildrenControls = False

                        ElseIf (TypeOf _control Is LiteralControl) Then
                            CType(_control, LiteralControl).Text = valueString
                            checkChildrenControls = False

                        ElseIf (TypeOf _control Is TextBox) Then
                            CType(_control, TextBox).Text = valueString
                            checkChildrenControls = False

                        ElseIf (TypeOf _control Is Button) Then
                            CType(_control, Button).Text = valueString
                            checkChildrenControls = False

                        ElseIf (TypeOf _control Is HyperLink) Then
                            CType(_control, HyperLink).Text = valueString
                            checkChildrenControls = False

                        End If

                    End If
                End If

            End If

        End If


        ' check children controls
        If (checkChildrenControls) Then

            For Each ctl As Control In _control.Controls
                ' skip UserControl controls
                If (TypeOf ctl Is UserControl AndAlso Not (TypeOf ctl Is MasterPage)) Then Continue For
                SetControlsValue(ctl, _pageData)
            Next

        End If

    End Sub



    Public Sub AddScriptResourceToHeader(page As Page, src As String)
        ' src = page.ResolveUrl(src)
        Dim found = False
        For Each ctl As Control In page.Header.Controls
            If (TypeOf ctl Is IAttributeAccessor) Then
                Dim oldSrc As String = CType(ctl, IAttributeAccessor).GetAttribute("src")
                Dim tmpOldSrc As String = HttpContext.Current.Server.MapPath(oldSrc)
                Dim tmpSrc As String = HttpContext.Current.Server.MapPath(src)
                If (tmpOldSrc = tmpSrc) Then
                    found = True
                End If
            End If
        Next

        If (Not found) Then
            Dim script As New HtmlGenericControl("script")
            script.Attributes.Add("src", src)
            page.Header.Controls.Add(script)
        End If

    End Sub



    Public Function ParseUrlEncodedQueryString(requestQueryString As NameValueCollection) As NameValueCollection
        Dim origQS As New NameValueCollection()

        If (Not String.IsNullOrEmpty(requestQueryString("enc"))) Then
            Dim enc As Integer
            Integer.TryParse(requestQueryString("enc"), enc)

            Dim queryString As String = requestQueryString.ToString()
            While (enc > 0)
                queryString = HttpContext.Current.Server.UrlDecode(queryString)
                enc -= 1
            End While

            origQS = HttpUtility.ParseQueryString(queryString)
        End If

        Return origQS
    End Function


    Public Function ShowUserErrorMsg(userMessage As String) As String
        Return "<div style='background-color:red;padding:10px;'>" & userMessage & "</div>"
    End Function

    ''' <summary>
    ''' Strip HTML tags.
    ''' </summary>
    Public Function StripTags(ByVal html As String) As String
        ' Remove HTML tags.
        Dim str As String = Regex.Replace(html, "&nbsp;", "")
        str = Regex.Replace(str, "<.*?>", "")
        Return str
    End Function

    Public Sub SelectAllCheckBoxes(ByRef CheckBoxList As System.Web.UI.WebControls.CheckBoxList, selectItems As Boolean)

        Try
            Dim cnt As Integer = 0
            For cnt = 0 To CheckBoxList.Items.Count - 1
                CheckBoxList.Items(cnt).Selected = True
            Next

        Catch ex As Exception

        End Try

    End Sub
    Public Sub SelectAllCheckBoxes(ByRef CheckBoxList As DevExpress.Web.ASPxEditors.ASPxCheckBoxList, selectItems As Boolean)
        Try
            Dim cnt As Integer = 0
            For cnt = 0 To CheckBoxList.Items.Count - 1
                CheckBoxList.Items(cnt).Selected = True
            Next
        Catch ex As Exception

        End Try

    End Sub



    Public Sub RefreshProfilePreviewControl(_control As Control)

        'If (TypeOf _control Is ProfilePreview) Then
        '    DirectCast(_control, ProfilePreview).RefreshThumb()
        '    Return
        'Else
        If (TypeOf _control Is MemberLeftPanel) Then
            DirectCast(_control, MemberLeftPanel).RefreshThumb()
            Return
        ElseIf (TypeOf _control Is ProfileIconCurrent) Then
            DirectCast(_control, ProfileIconCurrent).RefreshThumb()
            Return
        Else
            For Each ctl As Control In _control.Controls
                If (ctl.HasControls()) Then
                    RefreshProfilePreviewControl(ctl)
                End If
            Next
        End If

    End Sub


    Public Sub SetReferer(context As HttpContext)

        Dim Session As HttpSessionState = context.Session
        Dim Request As HttpRequest = context.Request
        Dim Response As HttpResponse = context.Response
        Dim Server As HttpServerUtility = context.Server


        Dim referrer2 As String = ""
        Try
            If (Request.AppRelativeCurrentExecutionFilePath.Contains("default.aspx") AndAlso Not String.IsNullOrEmpty(Request.QueryString("ref"))) Then
                ' needed  to process url after loader
                referrer2 = Request.QueryString("ref")
            Else
                referrer2 = Request.ServerVariables("HTTP_REFERER")
            End If
            Session("SearchEngineKeywords") = clsWebStatistics.GetKeywords(referrer2)
        Catch ex As Exception

        End Try


        ' Ini Session Vars for Staticals
        ' REFERRER --------------------------------------------------------
        '-- where the user was before he/she came to your site
        Dim Referrer As String = ""
        If (Request.AppRelativeCurrentExecutionFilePath.Contains("default.aspx") AndAlso Not String.IsNullOrEmpty(Request.QueryString("ref"))) Then
            ' needed  to process url after loader
            Referrer = Request.QueryString("ref")
        Else
            Referrer = Request.ServerVariables("HTTP_REFERER")
        End If
        Try
            If Not String.IsNullOrEmpty(Referrer) Then
                Referrer = Referrer.Replace("http://", "")
                Referrer = Referrer.Replace("https://", "")
                Referrer = Referrer.Substring(0, Referrer.IndexOf("/"))
            End If
        Catch ex As Exception
        End Try

        If Not Referrer Is Nothing AndAlso Not String.IsNullOrEmpty(gSiteName) Then
            If Not Referrer.Contains(Request.Url.Host) Then
                Session("Referrer") = Referrer
                Session("HTTP_REFERER") = referrer2
            End If
        End If

        'If Len(Session("Referrer")) = 0 Then Session("Referrer") = "none"
        ' REFERRER --------------------------------------------------------

        Session("CustomReferrer") = Trim(Request("cref"))
        If Len(Session("CustomReferrer")) = 0 Then
            Session("CustomReferrer") = Trim(Request("c"))
        End If

        If Len(Session("CustomReferrer")) = 0 Then
            If Len(Session("Referrer")) > 0 Then
                Dim ref As String = Session("Referrer")
                If (ref.IndexOf("www.") = 0) Then ref = ref.Remove(0, "www.".Length)
                Session("CustomReferrer") = DataHelpers.GetAffiliateBy_SiteName(ref)
            End If
        End If

        'If Len(Session("CustomReferrer")) > 0 Then
        Session("LandingPage") = Request.Url.ToString()
        'End If

        'If Len(Session("CustomReferrer")) = 0 Then
        '    If Len(Referrer) Then
        '        Try
        '            'Dim rs As New ClsMyADODB2
        '            'If rs.Open("SELECT * FROM EUS_Customers WHERE AffiliateSiteURL1 like '%" & Referrer & "%' OR AffiliateSiteURL2 like '%" & Referrer & "%' OR AffiliateSiteURL3 like '%" & Referrer & "%' ", ClsMyADODB2.OpenMode.ReadOnlyMode) Then
        '            '    If rs.RecordCount >= 1 Then
        '            '        Session("CustomReferrer") = rs.GetFieldValue("AffiliateCode", "none")
        '            '    End If
        '            'End If
        '            'rs.Close()
        '        Catch ex As Exception
        '        End Try
        '    End If
        'End If


        Dim refCookie As HttpCookie = Request.Cookies("refCookie")
        Try

            If Len(Session("CustomReferrer")) > 0 Then
                Dim szGuid As String = System.Guid.NewGuid().ToString()
                refCookie = New HttpCookie("refCookie", szGuid)
                refCookie.Expires = DateTime.Now.AddDays(40)
                refCookie.Item("customReferrer") = Session("CustomReferrer")
                Response.Cookies.Add(refCookie)
                Session("refCookie") = szGuid
            End If

        Catch ex As Exception
        End Try


        Try
            If Len(Session("CustomReferrer")) = 0 Then
                If Not refCookie Is Nothing Then
                    Session("refCookie") = Server.HtmlEncode(refCookie.Value)
                    Session("CustomReferrer") = refCookie.Item("customReferrer")
                End If
            End If
        Catch ex As Exception
        End Try

        Try
            If Len(Session("CustomReferrer")) = 0 Then Session("CustomReferrer") = "none"
        Catch ex As Exception

        End Try


        Try
            'Dim sql = "update EUS_Customers set AFF_Visitors=ISNULL(AFF_Visitors,0) + 1 where AFF_Code=@AFF_Code"
            Dim sql = <sql><![CDATA[
INSERT INTO [AFF_AffiliateVisitors]
        ([IP]
        ,[CustomReferrer]
        ,[refCookieVal]
        ,[SessionID])
    VALUES
        (@IP
        ,@CustomReferrer
        ,@refCookie
        ,@SessionID)
]]></sql>
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(con, sql)


                    If (Session("CustomReferrer") Is Nothing) Then
                        cmd.Parameters.AddWithValue("@CustomReferrer", System.DBNull.Value)
                    Else
                        cmd.Parameters.AddWithValue("@CustomReferrer", Session("CustomReferrer"))
                    End If
                    If (Session("refCookie") Is Nothing) Then
                        cmd.Parameters.AddWithValue("@refCookie", System.DBNull.Value)
                    Else
                        cmd.Parameters.AddWithValue("@refCookie", Session("refCookie"))
                    End If
                    cmd.Parameters.AddWithValue("@IP", Request.ServerVariables("REMOTE_ADDR"))
                    cmd.Parameters.AddWithValue("@SessionID", Session.SessionID)
                    DataHelpers.ExecuteNonQuery(cmd)
                End Using
            End Using
        Catch ex As Exception
            WebErrorSendEmail(ex, "")
        End Try

    End Sub


End Module

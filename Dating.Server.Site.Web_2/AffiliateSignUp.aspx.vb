﻿Imports Dating.Server.Core.DLL

Public Class AffiliateSignUp
    Inherits BasePage


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            'Try
            '    Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            '    AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
            'Catch ex As Exception
            '    WebErrorMessageBox(Me, ex, "Page_Load")
            'End Try


            If (Not Me.IsPostBack) Then
                LoadLAG()

                If (Me.MasterProfileId > 0) Then
                    lblYouremailRead.Text = Me.GetCurrentProfile().eMail
                    lblYouremailRead.Visible = True
                    txtYouremail.Visible = False
                End If
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try
    End Sub


    'Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
    '    Try

    '        If Not Page.IsPostBack Then
    '        End If
    '    Catch ex As Exception
    '        WebErrorMessageBox(Me, ex, "")
    '    End Try

    'End Sub


    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
    End Sub


    Protected Sub LoadLAG()
        Try
            '   Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = Me.CurrentPageData.cPageBasic

            lbHeader.Text = Me.CurrentPageData.GetCustomString(lbHeader.ID)
            'lbHeaderZilla.Text = Me.CurrentPageData.GetCustomString(lbHeaderZilla.ID)
            'lbHTMLBody.Text = cPageBasic.BodyHTM

            lbYouremail.Text = Me.CurrentPageData.GetCustomString("lbYouremail")
            lbYourWebSite.Text = Me.CurrentPageData.GetCustomString("lbYourWebSite")
            'lbSubject.Text = Me.CurrentPageData.GetCustomString("lbSubject")
            lbMessage.Text = Me.CurrentPageData.GetCustomString("lbMessage")
            lblMonthlyTraffic.Text = Me.CurrentPageData.GetCustomString("lblMonthlyTraffic")
            cmdSend.Text = Me.CurrentPageData.GetCustomString("cmdSend")
            cmdReset.Value = Me.CurrentPageData.GetCustomString("cmdReset")
            'linkReportAbuse.Text = Me.CurrentPageData.GetCustomString("linkReportAbuse")

            'lblTextlivezilla.Text = Me.CurrentPageData.GetCustomString("lblTextlivezilla")

            'AppUtils.setNaviLabel(Me, "lbPublicNavi2Page", cPageBasic.PageTitle)
            If (TypeOf Page.Master Is INavigation) Then
                CType(Page.Master, INavigation).AddNaviLink(Me.CurrentPageData.Title, "")
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Private Function ValidateInput() As Boolean
        Dim isvalid = True
        Try
            If (String.IsNullOrEmpty(lblYouremailRead.Text)) Then
                pnlYouremailErr.Visible = False
                Dim regMatch As Boolean
                If (clsConfigValues.Get__validate_email_address_normal()) Then
                    regMatch = Regex.IsMatch(txtYouremail.Text, AppUtils.gEmailAddressValidationRegex)
                Else
                    regMatch = Regex.IsMatch(txtYouremail.Text, AppUtils.gEmailAddressValidationRegex_Simple)
                End If

                If (txtYouremail.Text.Trim() = "" OrElse Not regMatch) Then
                    pnlYouremailErr.Visible = True
                    lbYouremailErr.Text = CurrentPageData.GetCustomString("txtYouremail_Validation_ErrorText")
                    txtYouremail.Focus()
                    isvalid = False
                End If
            End If


            pnlYourWebSiteErr.Visible = False
            If (txtYourWebSite.Text.Trim() = "") Then
                pnlYourWebSiteErr.Visible = True
                lbYourWebSiteErr.Text = CurrentPageData.GetCustomString("txtYourWebSite_Validation_ErrorText")
                txtYourWebSite.Focus()
                isvalid = False
            End If

            pnlMonthlyTrafficErr.Visible = False
            If (txtMonthlyTraffic.Text.Trim() = "") Then
                pnlMonthlyTrafficErr.Visible = True
                lblMonthlyTrafficErr.Text = CurrentPageData.GetCustomString("txtMonthlyTraffic_Validation_ErrorText")
                txtMonthlyTraffic.Focus()
                isvalid = False
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Return isvalid
    End Function


    Protected Sub cmdSend_Click(sender As Object, e As EventArgs) Handles cmdSend.Click
        If (Not ValidateInput()) Then Return

        Try
            Dim messageText As String = <message><![CDATA[
Contact message from [SITE_NAME]

From: ###SENDEREMAIL###
Website: ###SENDERNAME###
Monthly traffic: ###TRAFFIC###
Text: ###MESSAGEBODY###
]]></message>.Value



            messageText = messageText.Replace("[SITE_NAME]", ConfigurationManager.AppSettings("gSiteName"))

            If (Me.MasterProfileId > 0) Then
                messageText = messageText.Replace("###SENDEREMAIL###", lblYouremailRead.Text)
            Else
                messageText = messageText.Replace("###SENDEREMAIL###", txtYouremail.Text)
            End If

            messageText = messageText.Replace("###SENDERNAME###", txtYourWebSite.Text)
            messageText = messageText.Replace("###TRAFFIC###", txtMonthlyTraffic.Text)
            messageText = messageText.Replace("###MESSAGEBODY###", txtMessage.Text)

            Dim sendToEmailAddress As String = CurrentPageData.GetCustomString("SentToEmailAddress")
            If (String.IsNullOrEmpty(sendToEmailAddress)) Then
                sendToEmailAddress = ConfigurationManager.AppSettings("gToEmail")
            End If
            clsMyMail.SendMailSupport(sendToEmailAddress, "New affiliate sign up", messageText)

            pnlSendMessageForm.Visible = False
            pnlSendMessageWrap.Visible = True
            lblMessageSent.Text = CurrentPageData.GetCustomString("MessageSentSuccessfully")
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
            lblMessageSent.Text = ShowUserErrorMsg(CurrentPageData.GetCustomString("MessageSendFailed"))
        End Try
    End Sub


    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender

        Try
            Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub


End Class
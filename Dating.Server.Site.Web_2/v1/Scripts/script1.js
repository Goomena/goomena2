﻿
var Globals = new function () {
    var t = this;
    t.myGender = -1;
}

function setGlobalsGender(g) {
    try {
        Globals.myGender = g;
    }
    catch (e) { }
}
// When the DOM is ready, initialize the scripts.
jQuery(function ($) {
    /*
    script found at 
    http://www.bennadel.com/blog/1810-Creating-A-Sometimes-Fixed-Position-Element-With-jQuery.htm
    */
    // Get a reference to the placeholder. This element
    // will take up visual space when the message is
    // moved into a fixed position.
    var placeholder = $("#topFrameHolder");

    // Get a reference to the message whose position
    // we want to "fix" on window-scroll.
    var message = $("#m-top-container");

    // Get a reference to the window object; we will use
    // this several time, so cache the jQuery wrapper.
    var view = $(window);
    var doc = $(".main");

    // Bind to the window scroll and resize events.
    // Remember, resizing can also change the scroll
    // of the page. 
    view.bind("scroll resize",
				function () {
				    try {
				        // Get the current offset of the placeholder.
				        // Since the message might be in fixed
				        // position, it is the plcaeholder that will
				        // give us reliable offsets.
				        var placeholderTop = placeholder.offset().top;

				        // Get the current scroll of the window.
				        var viewTop = view.scrollTop();

				        // Check to see if the view had scroll down
				        // past the top of the placeholder AND that
				        // the message is not yet fixed.
				        if ((viewTop > placeholderTop) && !message.is(".m-top-container-fixed")) {

				            // The message needs to be fixed. Before
				            // we change its positon, we need to re-
				            // adjust the placeholder height to keep
				            // the same space as the message.
				            //
				            // NOTE: All we're doing here is going
				            // from auto height to explicit height.
				            placeholder.height(placeholder.height());

				            // Make the message fixed.
				            message.addClass("m-top-container-fixed");


				            // Check to see if the view has scroll back up
				            // above the message AND that the message is
				            // currently fixed.
				        } else if ((viewTop <= placeholderTop) && message.is(".m-top-container-fixed")) {

				            // Make the placeholder height auto again.
				            placeholder.css("height", "auto");

				            // Remove the fixed position class on the
				            // message. This will pop it back into its
				            // static position.
				            message.removeClass("m-top-container-fixed");

				            if (doc.length > 0) set_main_top($, placeholder, message); // scroll up
				        }
				    }
				    catch (e) { }
				}
			);
    if (doc.length > 0) set_main_top($, placeholder, message); // on load
});


function set_main_top($, placeholder, message) {
    try {
        var doc = $(".main");
        // style="margin-top:15px;"
        if (message.height() > placeholder.height()) {
            doc.offset({ top: message.height() });
        }
        else {
            doc.offset({ top: placeholder.height() });
        }
        //alert(message.height())
        //alert(doc.offset().top)
    }
    catch (e) { /* alert(e.message)*/ }
}

jQuery.fn.positionOn = function (element, align) {
    return this.each(function () {
        var target = jQuery(this);
        var position = element.position();

        var x = position.left;
        var y = position.top;

        if (align == 'right') {
            x -= (target.outerWidth() - element.outerWidth());
        } else if (align == 'center') {
            x -= target.outerWidth() / 2 - element.outerWidth() / 2;
        }

        target.css({
            position: 'absolute',
            zIndex: 5000,
            top: y,
            left: x
        });
    });
};



function bindClicktoNewWinks(searchWithInSelector) {
    jQuery(function ($) {

        $(".linkNotifWrapper", searchWithInSelector).each(function (index, elem) {
            var loc = $(elem).attr("location");
            $(elem).click(function () {
                window.location = $(this).attr("location");
                //window.location.reload();
            })
        })

    });
}

function scrollWin(scrollToSelector, offsetTop, scrollTime) {
    try {
        if (scrollTime == null || isNaN(scrollTime)) scrollTime = 2000;
        var offTop = $(scrollToSelector).offset();
        if (typeof offTop !== 'undefined') {
            if (!isNaN(offsetTop)) {
                $('html,body').animate({
                    scrollTop: ($(scrollToSelector).offset().top + offsetTop)
                }, scrollTime);
            }
            else {
                $('html,body').animate({
                    scrollTop: $(scrollToSelector).offset().top
                }, scrollTime);
            }
        }
    }
    catch (e) { }
}

function scrollContainer(scrollToSelector, scrollContainerSelector, offsetTop, scrollTime) {
    if (scrollTime == null || isNaN(scrollTime)) scrollTime = 2000;
    if (!isNaN(offsetTop)) {
        $(scrollContainerSelector).animate({
            scrollTop: ($(scrollToSelector).offset().top + offsetTop)
        }, scrollTime);
    }
    else {
        $(scrollContainerSelector).animate({
            scrollTop: $(scrollToSelector).offset().top
        }, scrollTime);
    }
}


function scrollToElem(el, topBar) {// top bar + paddings
    try {
        // jquery object
        if (el.length > 0) {
            //for (var c in el) {
            var top = el.offset().top;
            var left = el.offset().left;
            if (topBar > 0 || topBar < 0)
                top = top - topBar;
            if (top > 0)
                window.scrollTo(left, top);
            //}
        }
    }
    catch (e) { }
}

function scrollToLogin() {
    try {
        jQuery(function ($) {
            var strs = window.location.href.split('#');
            if (strs.length > 1) {
                var login = strs[1];
                var objs = $('div[name~="' + window.location.href.split('#')[1] + '"]');
                scrollToElem(objs, 120);
                //$('html, body').animate({
                //    scrollTop: $('div[name~="' + window.location.href.split('#')[1] + '"]').offset().top
                //}, 800);
            }
        });
    }
    catch (e) { }
}

function writeUserErrorMsg(msg) {
    try {
        setTimeout((function (msg) {
            if (msg != null) {
                var txt = document.createTextNode(msg);
                var o = document.getElementById("userErrorMsg");
                if (!o) {
                    o = document.createElement('div');
                    o.id = "userErrorMsg";
                    o.className = "userErrorMsg"
                    var topFrame = document.getElementById("m-top-container");
                    if (topFrame) {
                        topFrame.appendChild(o);
                    }
                    else {
                        var body = document.getElementsByTagName("body")[0];
                        body.insertBefore(o, body.firstChild);
                    }
                }
                try {
                    if (o.hasChildNodes())
                        while (o.childNodes.length >= 1)
                            o.removeChild(o.firstChild);
                }
                catch (e) { }
                o.appendChild(txt);
            }
            if (hideErrorTimer != null) clearTimeout(hideErrorTimer);
            hideError();
        })(msg), 1000);

        var hideErrorTimer;
        function hideError() {
            hideErrorTimer = setTimeout(function () {
                var o = document.getElementById("userErrorMsg");
                if (o) {
                    o.parentElement.removeChild(o);
                }
            }, 150000)
        }
    }
    catch (e) { }
}
function changeColorUsr(obj) {
    var color;
    if (obj.focused || obj.GetValue() != null) {
        color = "black";
    }
    else {
        color = "silver";
    }
    obj.GetInputElement().style.color = color;
}

function changeColorPwd(obj) {
    return;
    var color;
    var type;
    if (obj.focused || obj.GetValue() != null) {
        type = "password";
        color = "black";
    }
    else {
        type = "text";
        color = "silver";
        value = "**********";
    }
    obj.GetInputElement().style.color = color;
    obj.GetInputElement().type = type;
}




function facebook_fnc() {
    try {
        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_GB/all.js#xfbml=1&appId=425026417519142";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    }
    catch (e) { }

}


function SetPCVisible(value, popupControlId, windowIndex) {
    try {
        var popupcontrol = window[popupControlId];
        var wind = function () {
            this.index = -1;
        };
        var obj = new wind();
        obj.index = windowIndex;
        //obj["index"] = windowIndex;
        if (value)
            popupcontrol.ShowWindow(obj);
        else
            popupControl.HideWindow(obj);
    }
    catch (e) { }
}




function deleteCookies() {
    var cookies = document.cookie.split(";");
    document.cookie = "";
    for (var i = 0; i < cookies.length; i++) {
        var cookie = cookies[i];
        var eqPos = cookie.indexOf("=");
        var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
        //var d = new Date(1970, 1, 1)
        //Set_Cookie(name, '', d, '', '', '');
        document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
    }
}

function Set_Cookie(name, value, expires, path, domain, secure) {
    //var today = new Date();
    //today.setTime(today.getTime());
    //if (expires) {
    //    expires = expires * 1000 * 60 * 60 * 24;
    //}
    //var expires_date = new Date(today.getTime() + (expires));     
    var today = new Date();
    today.setTime(today.getTime());

    var expires_date = null;
    if (expires)
        expires_date = expires;
    else
        expires_date = today;

    document.cookie = name + "=" + escape(value) +
((expires) ? ";expires=" + expires_date.toGMTString() : "") +
((path) ? ";path=" + path : "") +
((domain) ? ";domain=" + domain : "") +
((secure) ? ";secure" : "");
}

function Get_Cookie(name) {
    var start = document.cookie.indexOf(name + "=");
    var len = start + name.length + 1;
    if ((!start) &&
(name != document.cookie.substring(0, name.length)))
    { return null; }
    if (start == -1) return null;
    var end = document.cookie.indexOf(";", len);
    if (end == -1) end = document.cookie.length;
    return unescape(document.cookie.substring(len, end));
}

jQuery(function ($) {
    $("body").on("mouseenter", ".tt_enabled", function () {
        $(".tooltip").hide();
        $(this).siblings(".tooltip").fadeIn(200)
    }).on("mouseleave", ".tt_enabled", function () {
        $(".tooltip").fadeOut(100)
    });

    $("body").on("mouseenter", ".tt_enabled_distance", function () {
        $(".tooltip_offer").hide();
        $(this).siblings(".tooltip_offer").fadeIn(200)
    }).on("mouseleave", ".tt_enabled_distance", function () {
        $(".tooltip_offer").fadeOut(100)
    });

    $("body").on("mouseenter", ".Zodiacs", function () {
        $(".tooltip_search_ZodiacName").hide();
        p = $(this).parent();
        while (!p.is('.stats')) { p = $(p).parent(); }
        $(".tooltip_search_ZodiacName", $(p)).fadeIn(100)
    }).on("mouseleave", ".Zodiacs", function () {
        $(".tooltip_search_ZodiacName").fadeOut(50)
    });
});


jQuery(function ($) {
    try {
        $('.tooltiptipsy-n').tipsy({ gravity: 'n', html: true, delayIn: 500, delayOut: 500, offset: 5 });
        $('.tooltiptipsy-w').tipsy({ gravity: 'w', html: true, delayIn: 500, delayOut: 500, offset: 5 });
        $('.tooltiptipsy-e').tipsy({ gravity: 'e', html: true, delayIn: 500, delayOut: 500, offset: 5 });
        $('.tooltiptipsy-s').tipsy({ gravity: 's', html: true, delayIn: 500, delayOut: 500, offset: 5 });
    }
    catch (e) { }
});

function runColapse(ClickToExpandString, ClickToCollapseString) {
    jQuery(function ($) {
        $('.pe_box').each(function () {
            var pe_box = this;

            $('.collapsibleHeader', pe_box).click(function () {
                $('.pe_form', pe_box).toggle('fast', function () {
                    // Animation complete.
                    var isVisible = ($('.pe_form', pe_box).css('display') == 'none');
                    if (isVisible) {
                        $('.togglebutton', pe_box).text("+");
                        $('.collapsibleHeader', pe_box).attr("title", ClickToExpandString);
                    }
                    else {
                        $('.togglebutton', pe_box).text("-");
                        $('.collapsibleHeader', pe_box).attr("title", ClickToCollapseString);
                    }
                });

            });
        });
    });
}






function ShowDevExPopUpByURL(popupName, contentUrl, popupElement, headerText) {
    if (headerText != null)
        window[popupName].SetHeaderText(headerText);

    if (popupElement != null) window[popupName].SetPopupElementID(popupElement.id);
    window[popupName].SetContentUrl(contentUrl);
    window[popupName].Show();
}

function ShowDevExPopUp(popupName, headerText) {
    if (headerText != null) window[popupName].SetHeaderText(headerText);
    window[popupName].Show();
}

function closePopup(popupName) {
    try {
        top.window[popupName].Hide();
    }
    catch (e) { }
    try {
        top.window[popupName].DoHideWindow(0);
    }
    catch (e) { }
}



function closeLiveZilla(elSelector) {
    var cookieName = "LiveZillaClosed"
    var cv = readCookie(cookieName)
    if (cv != null && cv.length > 0)
        eraseCookie(cookieName)

    createCookie(cookieName, '1', 5);
    $(elSelector).hide();
}

function createCookie(name, value, days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
    }
    else var expires = "";
    document.cookie = name + "=" + value + expires + "; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name, "", -1);
}


function closePopupAndShowLoading(popupName) {
    setTimeout(function () {
        top.ShowLoading();
        closePopup(popupName);
    }, 500);
}


var tmrShowLoading, LAGLoadingText;
function ShowLoading(opts) {
    if (stringIsEmpty(LAGLoadingText)) LAGLoadingText = ''; // '&nbsp;&nbsp;' +  'Loading&hellip;' + '&nbsp;&nbsp;'
    var _opts = { delay: 500, text: LAGLoadingText };

    if (typeof opts === 'undefined' || opts == null)
        opts = _opts;
    else {
        for (var p in _opts)
            if (opts[p] == null) opts[p] = _opts[p];
    }
    tmrShowLoading = setTimeout(function () {
        if (tmrShowLoading != null) {
            var z = $('.dxpcModalBackground').css('z-index');
            if (!stringIsEmpty(z)) $('.dxlpLoadingPanel_DevEx').css('z-index', parseInt(z) - 1);
            //LoadingPanel.Show();
            showLoadingFrame(opts.gender);
            $(document).keyup(escKey);
            //if (!stringIsEmpty(opts.text))
            //    LoadingPanel.SetText('&nbsp;&nbsp;' + opts.text + '&nbsp;&nbsp;');
        }
    }, opts.delay);

    function escKey(e) {
        if (e.keyCode == 27) { _HideLoading(); }   // esc
    }
    function _HideLoading() {
        $(document).unbind("keyup", escKey);
        HideLoading();
    }
}

function HideLoading() {
    try {
        if (tmrShowLoading != null) clearTimeout(tmrShowLoading);
        tmrShowLoading = null;
    }
    catch (e) { }
    try {
        //LoadingPanel.Hide();
        hideLoadingFrame();
    }
    catch (e) { }
}


function stringIsEmpty(s) {
    var b = (s == null || typeof s === 'undefined' || s == ''); return b;
}



function jsLoad(url) {
    ShowLoading();
    window.location.href = url;
}



function ShowLoadingOnMenu(s, e) {
    var isBlank = false;
    try {
        if (e.htmlElement.innerHTML.indexOf('_blank') > 0) isBlank = true;
        if (e.htmlElement.innerHTML.indexOf('javascript:') > 0) isBlank = true;
    }
    catch (e1) { }
    if (!isBlank) ShowLoading();
}


function topMenu_setHandlers(selector) {
    $(selector).click(function () {
        if ($(selector).attr('toggling') != "1") {
            $(selector).dropdown_mbr('toggle');
            $(selector).attr('toggling', '1');
        }
    });
}


function gup(name) {
    return gupWithUrl(window.location.href, name)
}

function gupWithUrl(url, name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS, 'i');
    var results = regex.exec(url);
    if (results == null)
        return "";
    else
        return results[1];
}




function performRequest_UpdateCredits() {
    try {
        var param = {};
        $.ajax({
            type: "POST",
            url: "json.aspx/GetCredits",
            data: param,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            error: function (ar1, ar2, ar3) {
            },
            success: function (data) {
                var d = data.d;
                if (stringIsEmpty(d)) { return; }
                $('.countValue', 'div#CreditsPopupContent a.hdr-link #notificationsCountWrapper').html(d);
                $('.countValue', '#left-side-inner .profile-self ul.info li a.lnkBilling').html('(' + d + ')');
            },
            beforeSend: function () { },
            complete: function () { HideLoading(); }
        });
    }
    catch (e) { }
}


function __focusAndShowError(msgBoxSelector, focusSelector, errMsg) {
    if ($(focusSelector).length > 0)
        __show();
    else
        $(function ($) {
            __show();
        });

    function __show() {
        HideLoading();
        $(focusSelector).focus();
        ShowMessageBox(msgBoxSelector, "Red", errMsg);
    }
}


function ShowMessageBox(msgBoxSelector, cls, text, noHide) {
    if (typeof noHide === undefined || noHide == null) noHide = false;
    var a = $(msgBoxSelector);
    if ($(msgBoxSelector + ':visible').length == 0) {
        a.html("<span>" + text + "</span>").addClass(cls);
        a.show("fast", function () {
            // allow hide
            if (noHide == false)
                $(a).delay(9000).fadeOut(800);
        });
    }
}


function setAlertHandlers() {
    var ui = $('.alert-error:visible');
    if (ui.length > 0) {
        ui.delay(20000).fadeOut(800);
    }
    var ui = $('.alert-danger:visible');
    if (ui.length > 0) {
        ui.delay(20000).fadeOut(800);
    }
}

function animateOnlineImg(elid) {
    var a = $('img', '#' + elid);
    if (a.is(".slide1"))
        a.removeClass("slide1");
    else
        a.addClass("slide1");
    if (a.length > 0)
        setTimeout("animateOnlineImg('" + elid + "')", 500);
}

function startAnimateOnlineImg() {
    for (c = 0; c < animating_elemnts_list.length; c++) {
        animateOnlineImg(animating_elemnts_list[c]);
    }
}


function forceRefresh() {
    // new method, refresh whole page
    var url = window.location.href;
    var param = gup("rfsh")
    var d = new Date();
    var n = d.getTime();
    if (!stringIsEmpty(param))
        url = url.replace("rfsh=" + param, "rfsh=" + n);
    else
        url = url + (url.indexOf("?") > -1 ? "&" : "?") + "rfsh=" + n;
    jsLoad(url);
}



function clearHTMLTags(d) {
    try {
        d = d.replace(/(<([^>]+)>)/ig, "");
        d = d.replace(/(&([^&]+);)/ig, "");
        d = $.trim(d)
    }
    catch (e) { }
    return d;
}


function selectedLagChanged(s, e) {
    var txt = s.GetText();
    txt = clearHTMLTags(txt);
    s.SetText(txt)
    ShowLoadingOnMenu(s, e);
    e.processOnServer = false;
}




var trackBarInfo = function () {
    var t = this;
    t.Name = t.DefaultText = t.DefaultTextPos = t.DefaultTextMin = t.DefaultTextMinPos = t.DefaultTextMax = t.DefaultTextMaxPos = null;
}

var trackBarInfoList = new function () {
    var t = this;
    t.List = new Array();

    t.GetInfoByPos = function (item, field, pos) {
        var i = 0;
        var ndx = -1;
        // find item
        for (; i < t.List.length; i++) {
            if (t.List[i].Name == item) {
                if (t.List[i][field + "Pos"] == pos) return t.List[i];
            }
        }
        return null;
    }

    t.AddInfo = function (item, field, value, pos) {
        var i = 0;
        var ndx = -1;
        // find item
        for (; i < t.List.length; i++) {
            if (t.List[i].Name == item) {
                ndx = i;
                break;
            }
        }
        if (ndx == -1) {
            var ti = new trackBarInfo();
            ti.Name = item;
            ti[field] = value;
            ti[field + "Pos"] = pos;
            t.List.push(ti);
        }
        else {
            var ti = t.List[i];
            if (ti[field] == null) {
                ti[field] = value;
                ti[field + "Pos"] = pos;
            }
        }
    }

}


function ProfileEdit_scrollToEditPosition() {
    function hideSection(scope_selector) {
        $('.pe_form', scope_selector).hide();
        collapsibleHeader_toggled($('.pe_box', scope_selector));
    }

    var scrpos = gup("scr");
    if (!stringIsEmpty(scrpos)) {
        scrpos = scrpos.toLowerCase();
        switch (scrpos) {
            case "other": {
                hideSection('#section_your_location')
                hideSection('#section_personal')
                hideSection('#section_about')
                scrollWin($('#section_other_details'), -200, 500);
                break;
            }
            case "aboutdesc":
            case "aboutexpect": {
                hideSection('#section_your_location')
                hideSection('#section_personal')
                hideSection('#section_other_details')
                hideSection('#section_personal_info')
                hideSection('#section_looking_for')
                if (scrpos == "aboutdesc") {
                    scrollWin($('#section_about'), 0, 500);
                } else if (scrpos == "aboutexpect") {
                    scrollWin($('#section_about'), 150, 500);
                }
                break;
            }
            case "travel":
            case "langs":
            case "job": {
                hideSection('#section_personal')
                hideSection('#section_about')
                hideSection('#section_other_details')
                hideSection('#section_personal_info')
                hideSection('#section_looking_for')
                if (scrpos == "job") {
                    scrollWin($('#liJobs'), -150, 500);
                } else if (scrpos == "travel") {
                    scrollWin($('#section_traveling'), -150, 500);
                } else if (scrpos == "langs") {
                    scrollWin($('#liSpokenLangs'), -150, 500);
                }
                break;
            }

        }
    }
}


function showLoadingFrame() {
    var position = { top: 0, left: 0 };
    var src = (Globals.myGender == 2 ? "/loading2.htm?woman" : "/loading2.htm?1");
    var ifrm = document.getElementById('_loading_frm');
    if (!ifrm) {
        var ifrm = document.createElement("IFRAME");
        ifrm.setAttribute("id", '_loading_frm');
        document.body.appendChild(ifrm);
    }
    ifrm.setAttribute("src", src);
    var win = $(window)
    var body = $(body)
    var w = $(window).width();
    var h = $(window).height();
    var css1 = {
        top: ((h / 2) - (165 / 2)) + 'px',
        left: ((w / 2) - (165 / 2)) + 'px',
        width: 165 + "px",
        height: 165 + "px",
        border: "0px",
        position: "absolute",
        'z-index': "10010",
        display: ''
    }
    if (win.scrollLeft() > 0) {
        try {
            css1.left = parseInt(css1.left) + win.scrollLeft();
            css1.left = css1.left + "px";
        }
        catch (e) { }
    }
    if (win.scrollTop() > 0) {
        try {
            css1.top = parseInt(css1.top) + win.scrollTop();
            css1.top = css1.top + "px";
        }
        catch (e) { }
    }
    $(ifrm).css(css1);
};

function hideLoadingFrame() {
    var ifrm = document.getElementById('_loading_frm');
    if (ifrm) {
        ifrm.setAttribute("src", "about:blank");
        ifrm.style.display = "none";
    }
};

var htmlOverflowStatus;
var bodyOverflowStatus;
function centerAdditionalInfo(frmwidth, frmheight) {

    var html = $('html');
    var body = $('body');
    htmlOverflowStatus = html.css("overflow");
    bodyOverflowStatus = body.css("overflow");

    var ifrm = document.getElementById('frmAddInfo');
    var win = $(window)

    var w = $(window).width();
    var h = $(window).height();
    var top = ((h / 2) - (frmheight / 2));
    var left = ((w / 2) - (frmwidth / 2));
    if (top < 190 && h >= frmheight + 190) {
        top = 190;
    }
    else if (top < 106) {
        top = 106
    }

    if (left < 300 && w >= frmwidth + 300) {
        left = 300;
    }
    else if (left < 50) {
        left = 50
    }


    var css1 = {
        top: top + 'px',
        left: left + 'px',
        width: frmwidth + "px",
        height: frmheight + "px",
        border: "0px",
        position: "absolute",
        'z-index': "10001"
    }
    if (win.scrollLeft() > 0) {
        try {
            css1.left = parseInt(css1.left) + win.scrollLeft();
            css1.left = css1.left + "px";
        }
        catch (e) { }
    }
    if (win.scrollTop() > 0) {
        try {
            css1.top = parseInt(css1.top) + win.scrollTop();
            css1.top = css1.top + "px";
        }
        catch (e) { }
    }
    $(ifrm).css(css1);
    html.css("overflow", "hidden");
    body.css("overflow", "hidden");
}


function showVeil() {
    var veil = $("#divAddInfoVeil");
    if (veil.length > 0) {
        var win = $(window)
        var body = $('body')
        var w = body.width();
        var h = body.height();

        var css1 = {
            top: -100 + 'px',
            left: -100 + 'px',
            right: w + 100 + 'px',
            bottom: h + 100 + 'px',
            width: w + 200 + 'px',
            height: h + 200 + 'px',
            'z-index': "10000"
        }
        veil.show().css(css1);
    }
}


function closeAdditionalInfo() {
    var css1 = {
        top: 'auto',
        left: 'auto',
        right: 'auto',
        bottom: 'auto',
        width: '0px',
        height: '0px',
        'z-index': "0"
    }
    $("#divAddInfoVeil").remove();

    var ifrm = document.getElementById('frmAddInfo');
    $(ifrm).hide();

    if (htmlOverflowStatus != null) {
        var html = $('html');
        html.css("overflow", htmlOverflowStatus);
    }
    if (bodyOverflowStatus != null) {
        var body = $('body');
        body.css("overflow", bodyOverflowStatus);
    }
}



function defaultRegisterClick() {
    try {
        if (window[ctlRdioMale].GetChecked() == true || window[ctlRdioFemale].GetChecked() == true) {
            ShowLoading();
            return true;
        }
        else {
            $('#' + ctlmainregdivctr)
                .attr("data-hint", defaultReg_data_hint)
                .addClass("hint--error hint--always mainregdivctr");
            return false;
        }
    }
    catch (e) { }
    return true;
}

function gender_CheckedChanged(s, e) {
    if (s.GetChecked()) {
        $('#' + ctlmainregdivctr)
            .removeClass("hint--error")
            .removeClass("hint--always")
            .removeAttr("data-hint");
    }
}



function isMobileDevice() {
    //https://coderwall.com/p/i817wa
    return (typeof window.orientation !== "undefined") || (navigator.userAgent.indexOf('IEMobile') !== -1);
};

var jsonConnect = new function () {
    var t = this;
    t.SetOffsetTimeUrl = '';
    t.hdfTimeOffsetID = '',
    t.SetIsMobileUrl = '';
    t.SetClientDataUrl = '';

    t.setClientData = function (reties) {

        if (stringIsEmpty(t.SetClientDataUrl)) {
            if (reties == null) reties = 3;
            if (reties > 0) {
                setTimeout(function () {
                    t.setClientData(reties - 1)
                }, 200);
            }
            return;
        }

        var d = new Date()
        var n = d.getTimezoneOffset();
        $('#' + t.hdfTimeOffsetID).val(n);

        try {
            var isMobile = isMobileDevice();
            var param = $.toJSON({ time: n, isMobile: isMobile });
            $.ajax({
                type: "POST",
                url: t.SetClientDataUrl,
                data: param,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                error: function (ar1, ar2, ar3) { },
                success: function (msg) { },
                beforeSend: function () { },
                complete: function () { }
            });
        }
        catch (e) { }
    }



    t.setTimeOffset = function (reties) {

        if (stringIsEmpty(t.SetOffsetTimeUrl)) {
            if (reties == null) reties = 3;
            if (reties > 0) {
                setTimeout(function () {
                    t.setTimeOffset(reties - 1)
                }, 200);
            }
            return;
        }

        var d = new Date()
        var n = d.getTimezoneOffset();
        $('#' + t.hdfTimeOffsetID).val(n);

        try {
            var param = $.toJSON({ time: n });
            $.ajax({
                type: "POST",
                url: t.SetOffsetTimeUrl,
                data: param,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                error: function (ar1, ar2, ar3) { },
                success: function (msg) { },
                beforeSend: function () { },
                complete: function () { }
            });
        }
        catch (e) { }
    }


    t.setIsMobile = function (reties) {

        if (stringIsEmpty(t.SetIsMobileUrl)) {
            if (reties == null) reties = 3;
            if (reties > 0) {
                setTimeout(function () {
                    t.setIsMobile(reties - 1)
                }, 200);
            }
            return;
        }


        try {
            var param = $.toJSON({ isMobile: isMobileDevice() });
            $.ajax({
                type: "POST",
                url: t.SetIsMobileUrl,
                data: param,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                error: function (ar1, ar2, ar3) { },
                success: function (msg) { },
                beforeSend: function () { },
                complete: function () { }
            });
        }
        catch (e) { }
    }


}

var __popupSMSInfo__ = new function () {
    var t = this;
    t.viewed = false;
    t.productsPage = false;
    t.headerText = '';

    t.onLoaded = function () {
        function m__setPopupClass() {
            var popup = popupSMSInfo
            var divId = popup.name + '_PW-1';
            $('#' + divId)
            .addClass('fancybox-popup')
            .prepend($('<div class="header-line"></div>'));
            $('.header-line', '#' + divId).append($('<div class="header-text">' + __popupSMSInfo__.headerText + '</div>'));
            //var closeId = popup.name + '_HCB-1Img';
            //$('#' + closeId).addClass('fancybox-popup-close');
            var hdrId = popup.name + '_PWH-1T';
            $('#' + hdrId).addClass('fancybox-popup-header');
            //var frmId = popup.name + '_CSD-1';
            //$('#' + frmId).addClass('iframe-container');
        }

        setTimeout(function () {
            m__setPopupClass();
        }, 300);

    }
}

function Show_popupSMSInfo() {
    if (window["popupSMSInfo"] != null && __popupSMSInfo__.viewed == false) {
        ShowDevExPopUp("popupSMSInfo", null)
        __popupSMSInfo__.viewed = true;
        return false;
    }
    return true;
}
function JustCloseWindow() {
    closePopup("popupSMSInfo");
}
function loadProducts() {
    JustCloseWindow();
    var productsPage = __popupSMSInfo__.productsPage;
    top.jsLoad(productsPage);
}

//Like-Favorite-Unfavorite
var likeRetries = 3;
function LikeProfile(ProfileId, onErrorFunc, Param1, Param2, OnRedirect) {
    try {
        var param = $.toJSON({ "ID": ProfileId });
        // ShowLoading()
        $.ajax({
            type: "POST",
            url: "json.aspx/LikeAsync",
            data: param,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            error: function () {
                if (likeRetries > 0) {
                    setTimeout(function () { LikeProfile(ProfileId); }, 3000);
                    likeRetries--;
                }
                else {

                    likeRetries = 3;
                }
            },
            success: function (obj) {
                try {
                    likeRetries = 3;
                    if (obj.d != "") {

                        var Result = eval("(" + obj.d + ")");
                        if (Result) {
                            if (Result.HasError == true) {
                                if (Result.Redirect == true) {
                                    OnRedirect();
                                } else {
                                    onErrorFunc(Param1, Param2);
                                    OnMoreInfoClick(Result.Url, this, Result.Title, Result.width, Result.height, null);
                                    return false;
                                }
                            }

                        }
                    }

                } catch (err) { }
            },
            beforeSend: function () { },
            complete: function () { }
        });
    }
    catch (e) { }
}
var favoriteRetries = 3;
function FavoriteProfile(ProfileId, OnRedirect) {
    try {
        var param = $.toJSON({ "ID": ProfileId });
        // ShowLoading()
        $.ajax({
            type: "POST",
            url: "json.aspx/FavoriteAsync",
            data: param,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            error: function () {
                if (favoriteRetries > 0) {
                    setTimeout(function () { FavoriteProfile(ProfileId); }, 3000);
                    favoriteRetries--;
                }
                else {

                    favoriteRetries = 3;
                }
            },
            success: function (obj) {
                try {
                    favoriteRetries = 3;
                    if (obj.d != "") {

                        var Result = eval("(" + obj.d + ")");
                        if (Result) {
                            if (Result.HasError == true) {
                                if (Result.Redirect == true) {
                                    OnRedirect();
                                } else {
                                                                        return false;
                                }
                            }

                        }
                    }

                } catch (err) { }
            },
            beforeSend: function () { },
            complete: function () { }
        });
    }
    catch (e) { }
}
var unFavoriteRetries = 3;
function UnFavoriteProfile(ProfileId, OnRedirect) {
    try {
        var param = $.toJSON({ "ID": ProfileId });
        // ShowLoading()
        $.ajax({
            type: "POST",
            url: "json.aspx/UnFavoriteAsync",
            data: param,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            error: function () {
                if (unFavoriteRetries > 0) {
                    setTimeout(function () { UnFavoriteProfile(ProfileId); }, 3000);
                    unFavoriteRetries--;
                }
                else {

                    unFavoriteRetries = 3;
                }
            },
            success:function (obj) {
                try {
                    unFavoriteRetries = 3;
                    if (obj.d != "") {

                        var Result = eval("(" + obj.d + ")");
                        if (Result) {
                            if (Result.HasError == true) {
                                if (Result.Redirect == true) {
                                    OnRedirect();
                                } else {
                                    return false;
                                }
                            }

                        }
                    }

                } catch (err) { }
            },
            beforeSend: function () { },
            complete: function () { }
        });
    }
    catch (e) { }
}
//End
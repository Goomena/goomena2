var btnRegisterClicked = false;
function OnCountryChanged(cbCountry) {
    HideLoading();
    cbpnlZip.PerformCallback("country_" + cbCountry.GetValue().toString());
    ShowLoading();
}
function OnRegionChanged(cbRegion) {
    HideLoading();
    cbpnlZip.PerformCallback("region_" + cbRegion.GetValue().toString());
    ShowLoading();
}
function OnCityChanged(cbCity) {
    HideLoading();
    cbpnlZip.PerformCallback("city_" + cbCity.GetValue().toString() + "_region_" + cbRegion.GetValue().toString());
    ShowLoading();
}

function valSumm_VisibilityChanged(s, e) {
    jQuery(function ($) {
        //scrollToElem($(), 160);
    });
    btnRegisterClicked = false;
    HideLoading();
}

function cbCountry_SelectedIndexChanged(s, e) { OnCountryChanged(s); }
function cbCountry_EndCallback(s, e) { HideLoading(); }
function cbpnlZip_EndCallback(s, e) { HideLoading(); }
function cbpnlZip_CallbackError(s, e) { HideLoading(); }
function cbpnlZip_BeginCallback(s, e) { ShowLoading(); }
function cbRegion_SelectedIndexChanged(s, e) { OnRegionChanged(s); }
function cbRegion_EndCallback(s, e) { HideLoading(); }
function cbCity_SelectedIndexChanged(s, e) { OnCityChanged(s); }

function field_Validation(s, e) {
    s.ValidateWithPatterns();
    e.isValid = s.GetIsValid();

    if (s == window["RegisterGenderRadioButtonList"]) {
        if (!e.isValid) {
            $('.RegisterGenderRadioButtonList-error', '#TrGender').show();
            $('.RegisterGenderRadioButtonList-ok', '#TrGender').hide();

            
            s.SetIsValid(false, false);
        }
        else {
            $('.RegisterGenderRadioButtonList-error', '#TrGender').hide();
            $('.RegisterGenderRadioButtonList-ok', '#TrGender').show();

            s.SetIsValid(true, false);
        }
    }
    else if (s == window["RegisterEmailTextBox"]) {
        isValid_Email(e.isValid)
    }
    else if (s == window["RegisterLoginTextBox"]) {
        isValid_Login(e.isValid)
    }
    else if (s == window["RegisterPassword"]) {
        if (!e.isValid) {
            $('.RegisterPassword-error', '#TrMyPassword').show();
            $('.RegisterPassword-ok', '#TrMyPassword').hide();

            
            s.SetIsValid(false, false);
        }
        else {
            $('.RegisterPassword-error', '#TrMyPassword').hide();
            $('.RegisterPassword-ok', '#TrMyPassword').show();

            s.SetIsValid(true, false);
        }
    }
    else if (s == window["RegisterPasswordConfirm"]) {
        window["RegisterPassword"].ValidateWithPatterns();
        var passValid = window["RegisterPassword"].GetIsValid();
        if (passValid) {

            e.isValid = (s.GetText() == RegisterPassword.GetText());

            if (!e.isValid) {
                $('.RegisterPasswordConfirm-error', '#TrMyPasswordConfirm').show();
                $('.RegisterPasswordConfirm-ok', '#TrMyPasswordConfirm').hide();

                
                s.SetIsValid(false, false);
            }
            else {
                $('.RegisterPasswordConfirm-error', '#TrMyPasswordConfirm').hide();
                $('.RegisterPasswordConfirm-ok', '#TrMyPasswordConfirm').show();

                s.SetIsValid(true, false);
            }
        }
        else {
            $('.RegisterPasswordConfirm-error', '#TrMyPasswordConfirm').hide();
            $('.RegisterPasswordConfirm-ok', '#TrMyPasswordConfirm').hide();
        }
    }
    else if (s == window["RegisterAgreementsCheckBox"]) {
        e.isValid = (s.GetChecked() == true);
        if (!e.isValid) {
            $('.RegisterAgreementsCheckBox-error', '#TrAgreements').show();
            $('.RegisterAgreementsCheckBox-ok', '#TrAgreements').hide();

            
            s.SetIsValid(false, false);
        }
        else {
            $('.RegisterAgreementsCheckBox-error', '#TrAgreements').hide();
            $('.RegisterAgreementsCheckBox-ok', '#TrAgreements').show();

            s.SetIsValid(true, false);
        }
    }
}

var isUserNameAvailable = true;
var isEmailAvailable = true;
function isValid_Login(isValid) {
    if (!isValid || !isUserNameAvailable) {
        $('.RegisterLoginTextBox-error', '#TrMyUsername').show();
        $('.RegisterLoginTextBox-ok', '#TrMyUsername').hide();
        window["RegisterLoginTextBox"].SetIsValid(false, false);
    }
    else {
        $('.RegisterLoginTextBox-error', '#TrMyUsername').hide();
        $('.RegisterLoginTextBox-ok', '#TrMyUsername').show();

        window["RegisterLoginTextBox"].SetIsValid(true, false);
    }

}

function isValid_Email(isValid) {
    if (!isValid || !isEmailAvailable) {
        $('.RegisterEmailTextBox-error', '#TrMyEmail').show();
        $('.RegisterEmailTextBox-ok', '#TrMyEmail').hide();
        window["RegisterEmailTextBox"].SetIsValid(false, false);
    }
    else {
        $('.RegisterEmailTextBox-error', '#TrMyEmail').hide();
        $('.RegisterEmailTextBox-ok', '#TrMyEmail').show();

        window["RegisterEmailTextBox"].SetIsValid(true, false);
    }
}


function showWaterMark5(s, e) {
    if (s == window["RegisterPassword"]) {
        if (stringIsEmpty($.trim(s.GetValue()))) {
            $('.text-watermark:hidden', '.RegisterPassword-wrap').show();
        }
    }
    else if (s == window["RegisterPasswordConfirm"]) {
        if (stringIsEmpty($.trim(s.GetValue()))) {
            $('.text-watermark:hidden', '.RegisterPasswordConfirm-wrap').show();
        }
    }
}
function hideWaterMark5(s, e) {
    if (s == window["RegisterPassword"]) {
        $('.text-watermark', '.RegisterPassword-wrap').hide();
    }
    else if (s == window["RegisterPasswordConfirm"]) {
        $('.text-watermark', '.RegisterPasswordConfirm-wrap').hide();
    }
}

function checkWaterMark5(s, e) {
    if (s == window["RegisterPassword"]) {
        if (stringIsEmpty($.trim(s.GetValue()))) {
            $('.text-watermark:hidden', '.RegisterPassword-wrap').show();
        }
        else {
            $('.text-watermark', '.RegisterPassword-wrap').hide();
        }
    }
    else if (s == window["RegisterPasswordConfirm"]) {
        if (stringIsEmpty($.trim(s.GetValue()))) {
            $('.text-watermark:hidden', '.RegisterPasswordConfirm-wrap').show();
        }
        else {
            $('.text-watermark', '.RegisterPasswordConfirm-wrap').hide();
        }
    }
}
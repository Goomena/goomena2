﻿
function ReadMessage(o) {
    ShowLoading();
    setTimeout(function () {
        $(o).
            attr('disabled', 'disabled').
            click(function (event) { event.preventDefault() });
    }, 100);
};

var __retries_messages_list_loading = 5,
__back_to_menu_left = 'Back to menu',
__back_to_menu_right = 'Show coversations list',
__ShowMainMenu_visible = false,
CurrentLoginName1 = null,
__retries_conv_older_list_loading = 5,
__retries_emoticons_list_loading = 5,
__setRowNumberMax = -1,
__setMessageIDMin = -1,
__freeMessagesOnView = -1;

var performRequest_LoadMoreMessages = loadMoreMessages = function (opts) {
    var optsDef = { updateInitial: false, dateUntil: null, callback: null, skipScroll: false }
    var updateInitial = (opts != null && opts['updateInitial'] != null ? opts.updateInitial : optsDef.updateInitial);
    var dateUntil = (opts != null && opts['dateUntil'] != null ? opts.dateUntil : optsDef.dateUntil);
    var callback = (opts != null && opts['callback'] != null ? opts.callback : optsDef.callback);
    var skipScroll = (opts != null && opts['skipScroll'] != null ? opts.skipScroll : optsDef.skipScroll);


    var expanded = false,
    context = '#loaded_messages',
    context2 = '#initial_messages';

    if (updateInitial == true) {
        context = context2;
    }
    else {
        /*
        // btnMore not exists or is hidden, then set expanded = true
        expanded = !btnsMoreLess.isVisibleMore();
        //((btnMore.length > 0) && $(btnMore).is(':hidden')) || (btnMore.length == 0);

        if (!expanded) {
            var itms = $(".messages-list-loaded:hidden", context);
            for (var c = 0; c < itms.length; c++) {
                if ($(itms[itms.length - 1]).css('display') == 'none') {
                    $(itms[itms.length - 1]).show('fast');
                    if (itms.length == 1) btnsMoreLess.hideMore();
                    if (!btnsMoreLess.isVisibleLess()) btnsMoreLess.showLess();
                    expanded = true;
                    break;
                }
            }
        }
        */
    }

    if (!expanded) {
        if ($('#loaded_messages_loading', '#all_messages').length == 0) {
            $('#all_messages').prepend('<div id="loaded_messages_loading" style="text-align:center;"><img src="/Images2/GOO-Loading.gif" /></div>')
        }
        try {
            var param = {}
            if (updateInitial == true)
                param = $.toJSON({ otherProfile: CurrentLoginName1, rowFrom: 0, rowTo: 5, olderMsgIdAvailable: __setMessageIDMin, freeMessages: __freeMessagesOnView, dateUntil: dateUntil });
            else {
                var cnv_itemLen = $('.cnv_item', '#all_messages').length;
                param = $.toJSON({ otherProfile: CurrentLoginName1, rowFrom: cnv_itemLen, rowTo: cnv_itemLen + 10, olderMsgIdAvailable: __setMessageIDMin, freeMessages: __freeMessagesOnView, dateUntil: dateUntil });
            }
            $.ajax({
                type: "POST",
                url: "json.aspx/GetConversationMessagesList2",
                data: param,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                error: function (ar1, ar2, ar3) {
                    if (__retries_conv_older_list_loading > 0) {
                        setTimeout(function () { loadMoreMessages(opts) }, 3000);
                        __retries_conv_older_list_loading--;
                    }
                    else {
                        performLoad_OlderMessages(null, updateInitial, skipScroll);
                        __retries_conv_older_list_loading = 0;
                    }
                },
                success: function (msg) {
                    performLoad_OlderMessages(msg, updateInitial, skipScroll);
                    if (callback != null)
                        callback();
                },
                beforeSend: function () { },
                complete: function () { }
            });
        }
        catch (e) { }
    }
}


function performRequest_MessageDelete(e, o) {
    if ($(o).attr("working") != "1") {
        $(o).attr("working", "1");
        var href = $(o).attr("href")
        _doReq($(o), href);
    }

    function _doReq(o, url) {
        try {
            var read, vw, unmsg, onemsg;
            vw = gupWithUrl(url, "vw");
            msg = gupWithUrl(url, "msg");
            onemsg = gupWithUrl(url, "onemsg");

            var param = $.toJSON({ "otherProfile": vw, "msg": msg, "onemsg": onemsg });
            $.ajax({
                type: "POST",
                url: "json.aspx/MessageDelete",
                data: param,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                error: function (ar1, ar2, ar3) {
                    if (__retries_messages_list_loading > 0) {
                        setTimeout(function () {
                            _doReq(o, url)
                        }, 2000);
                        __retries_messages_list_loading--;
                    }
                    else {
                        __retries_messages_list_loading = 5;
                    }
                },
                success: function (msg) {
                    messageDeleteSuccess(o, msg);
                },
                beforeSend: function () { },
                complete: function () { HideLoading(); }
            });
        }
        catch (e) { }
    }

    function messageDeleteSuccess(o, msg) {
        performLoad_MessageReplace(o, msg);
        var cnv_itemLen = $('.cnv_item', '#all_messages').length;
        if (cnv_itemLen == 0) {
            performRequest_LoadMoreMessages({ updateInitial: true, dateUntil: null });
        }
    }
}

function performRequest_MessageUnlock(o, url) {
    try {
        var read, vw, unmsg, onemsg;
        subscription = gupWithUrl(url, "subscription");
        free = gupWithUrl(url, "free");
        read = gupWithUrl(url, "read");
        vw = gupWithUrl(url, "vw");
        unmsg = gupWithUrl(url, "unmsg");
        onemsg = gupWithUrl(url, "onemsg");

        var param = $.toJSON({ "otherProfile": vw, "read": read, "unmsg": unmsg, "onemsg": onemsg, "free": free, "subscription": subscription });
        $.ajax({
            type: "POST",
            url: "json.aspx/MessageUnlock2",
            data: param,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            error: function (ar1, ar2, ar3) {
                if (__retries_messages_list_loading > 0) {
                    setTimeout(function () {
                        performRequest_MessageUnlock(o, url)
                    }, 2000);
                    __retries_messages_list_loading--;
                }
                else {
                    __retries_messages_list_loading = 5;
                }
            },
            success: function (msg) {
                performLoad_MessageReplace(o, msg);
                performRequest_UpdateCredits();
            },
            beforeSend: function () { },
            complete: function () { HideLoading(); }
        });
    }
    catch (e) { }
}

function performRequest_MessagesList() {
    try {
        var param = {}
        $.ajax({
            type: "POST",
            url: "json.aspx/GetMessagesList",
            data: param,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            error: function (ar1, ar2, ar3) {
                if (__retries_messages_list_loading > 0) {
                    setTimeout(performRequest_MessagesList, 3000);
                    __retries_messages_list_loading--;
                }
                else {
                    __retries_messages_list_loading = 5;
                }

            },
            success: function (msg) {
                performLoad_MessagesList(msg);
            },
            beforeSend: function () { },
            complete: function () { }
        });
    }
    catch (e) { }
}

var PredefinedMessage = new function () {
    var t = this;
    var strings = [];
    var _index = 0;

    t.performRequest_GetRandomMessage = function (e, o, fnComplete) {
        var __retries = 0;
        if (strings.length == 0) {
            if ($(o).attr("working") != "1") {
                $(o).attr("working", "1").attr("disabled", "disabled").addClass("opacity70");
                _doReq();
            }
        }
        else {
            if (_index >= strings.length) _index = 0;
            if (strings[_index] == null) _index = 0;
            var d = strings[_index++]['MessageText'];
            d = clearHTMLTags(d)
            fnComplete(d);
        }

        function _doReq() {
            try {
                var param = {};
                $.ajax({
                    type: "POST",
                    url: "json.aspx/GetAllPredefinedMessage",
                    data: param,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: true,
                    error: function (ar1, ar2, ar3) {
                        if (__retries > 0) {
                            setTimeout(function () {
                                _doReq();
                            }, 1000);
                            __retries--;
                        }
                        else
                            __retries = 5;
                    },
                    success: function (msg) {
                        _completeReq(msg);
                    },
                    beforeSend: function () { },
                    complete: function () { HideLoading(); }
                });
            }
            catch (e) { }
        }

        function _completeReq(data) {
            var d = data.d;
            if (stringIsEmpty(d)) { return; }
            eval("strings=" + d);
            $(o).removeAttr("working").removeAttr("disabled").removeClass("opacity70");

            if (strings.length > 0) {
                _index = 0;
                d = strings[_index++]['MessageText']
                d = clearHTMLTags(d)
                fnComplete(d);
            }
        }
    }

}
var performRequest_GetRandomPredefinedMessage = PredefinedMessage.performRequest_GetRandomMessage


function loadLessMessages() {
    var context = '#loaded_messages';
    var itms = $(".messages-list-loaded:visible", context);

    for (var c = 0; c < itms.length; c++) {
        if ($(itms[c]).css('display') != 'none') {
            $(itms[c]).hide('fast');
            btnsMoreLess.showMore();
            if (itms.length == 1)
                btnsMoreLess.hideLess();
            break;
        }
    }

}

function performStartup() {
    convList_updateButtonClasses();
    conversations_setHandlers();
    btnsMoreLess.startUp();
    messagesLoader.ShowPeriodButtons();
}

function performLoad_OlderMessages(data, updateInitial, skipScroll) {
    var context = '#loaded_messages';
    var context2 = '#initial_messages';
    if (data == null) {
        $('#loaded_messages_loading', '#all_messages').remove();
    }
    else if (updateInitial == true) {
        var d = data.d;
        if (stringIsEmpty(d)) { return; }

        $(context2).html(d);
        if ($('.cnv_item', context).length > 0) {
            $(context).html('');

            btnsMoreLess.showMore();
            btnsMoreLess.hideLess();
        }
        else if ($('.cnv_item', context2).length == 5) {
            btnsMoreLess.showMore();
        }

        $('.pnlMoreMsgs-remove', context2).remove();
        $('#loaded_messages_loading', '#all_messages').remove();
        var scrollTop = 0
        try {
            scrollTop = $('#EndOfAllMessages').offset().top ;
        }
        catch (e) { }
        $('#conv-item').animate({ scrollTop: scrollTop }, 500);
        convList_updateButtonClasses();
        conversations_setHandlers();
        messagesLoader.ShowPeriodButtons();
    }
    else {
        var d = data.d;
        if (stringIsEmpty(d)) { return; }


        if (d.indexOf("{\"redirect\"") == 0) {
            var action = {};
            eval("action=" + d);
            jsLoad(action["redirect"]);

            $('#loaded_messages_loading', '#all_messages').remove();
        }
        else {
            var hBefore = $(context).height();
            d = '<div id="messages-list-' + hBefore + '" style="visibility:hidden" class="messages-list-loaded">' + d + '</div>';
            $(context).prepend(d);

            if (!skipScroll) {
                var hght = $(context).height() - hBefore;
                $('#conv-item').animate({ scrollTop: hght }, 500, function () {
                    $("#messages-list-" + hBefore, context).
                        css("visibility", "visible").
                        fadeIn('fast');
                });
            }
            else {
                $("#messages-list-" + hBefore, context).css("visibility", "visible");
            }

            if (typeof resetInitialList === "function") $('#initial_messages', context).html("");

            if ($('.pnlMoreMsgs-remove', context).length == 0) btnsMoreLess.hideMore(); //btnMore.hide();
            if (!btnsMoreLess.isVisibleLess()) {
                btnsMoreLess.showLess();
                conversations_TopLinks();
            }

            $('.pnlMoreMsgs-remove', context).addClass("pnlMoreMsgs-point").removeClass("pnlMoreMsgs-remove");
            $('#loaded_messages_loading', '#all_messages').remove();
            convList_updateButtonClasses();
            conversations_setHandlers();
            messagesLoader.ShowPeriodButtons();
        }
    }
}

function ShowMainMenu() {
    $('.top-links', '#quick-list-messages').hide('fast');
    $('#scroll-bar-wrap', '#quick-list-messages').hide('fast');
    $('#list-container-wrap', '#quick-list-messages').hide('fast');
    $('#hr-separator', '#quick-list-messages').hide('fast');
    $('.back-to-menu', '#quick-list-messages').text(__back_to_menu_right);
    $('#quick-list-messages').addClass('collapsed');

    $('.my-photo', '#left-side-inner').show();
    $('.my-login', '#left-side-inner').show();
    $('.my-menu', '#left-side-inner').show();
    $('.my-quick-links', '#left-side-inner').show();
    __ShowMainMenu_visible = true;
}

function ShowQuickListMessages() {
    $('.my-photo', '#left-side-inner').hide();
    $('.my-login', '#left-side-inner').hide();
    $('.my-menu', '#left-side-inner').hide();
    $('.my-quick-links', '#left-side-inner').hide();

    $('.top-links', '#quick-list-messages').show('fast');
    if ($('#scroll-bar-wrap', '#quick-list-messages').attr('preserve') != '1') {
        $('#scroll-bar-wrap', '#quick-list-messages').show('fast');
    }
    $('#list-container-wrap', '#quick-list-messages').show('fast');
    $('#hr-separator', '#quick-list-messages').show('fast');
    $('.back-to-menu', '#quick-list-messages').text(__back_to_menu_left);
    $('#quick-list-messages').removeClass('collapsed');
    __ShowMainMenu_visible = false;
}

function FixLeftBarAfterPostBack() {
    var qmsl_items, qmsl_items_count, list_container, context = '#quick-list-messages';
    qmsl_items = $('.qmsl_item', context);
    qmsl_items_count = qmsl_items.length;

    if (qmsl_items_count > 0) {
        if (!__ShowMainMenu_visible) {
            $('.my-photo', '#left-side-inner').hide();
            $('.my-login', '#left-side-inner').hide();
            $('.my-menu', '#left-side-inner').hide();
            $('.my-quick-links', '#left-side-inner').hide();
        }
    }
}

function ToggleMainMenu() {
    if (__ShowMainMenu_visible) {
        ShowQuickListMessages();
    }
    else {
        ShowMainMenu();
    }
}

function performLoad_MessagesList(data) {
    var d = data.d;
    if (stringIsEmpty(d)) { return; }
    $('#list-container').html(d);

    var qmsl_items, qmsl_items_count, list_container, context = '#quick-list-messages';
    qmsl_items = $('.qmsl_item', context);
    qmsl_items_count = qmsl_items.length;

    if (qmsl_items_count > 0) {
        $('.my-photo', '#left-side-inner').hide('fast');
        $('.my-login', '#left-side-inner').hide('fast');
        $('.my-menu', '#left-side-inner').hide('fast');
        $('.my-quick-links', '#left-side-inner').hide('fast');
        $('#quick-list-messages').show('fast', function () {
            loadSlider();
        });
    }
}


function loadSlider() {
    var context = '#quick-list-messages', qmsl_items, qmsl_items_count,
    list_container, list_container_wrap, qmsl_items_selected, selected_ndx = -1,
    offset_top_1, qmsl_items_arr = new Array();

    qmsl_items = $('.qmsl_item', context);
    qmsl_items_count = qmsl_items.length;
    list_container = $('#list-container', context);
    list_container_wrap = $('#list-container-wrap', context);
    if (list_container_wrap.height() > list_container.height()) {
        $('#scroll-bar-wrap', context).hide().attr('preserve', '1');
        list_container_wrap.height(list_container.height() + 10);
        return;
    }

    offset_top_1 = $(qmsl_items[0]).offset().top;
    for (var c = 0; c < qmsl_items_count; c++) {
        var offset_top = $(qmsl_items[c]).offset().top;
        offset_top = offset_top - offset_top_1;
        qmsl_items_arr.push(offset_top);

        if ($(qmsl_items[c]).attr('login') == 'name-' + CurrentLoginName1) {
            $(qmsl_items[c]).addClass('selected');
            selected_ndx = c;
        }
    }

    if (selected_ndx >= 0) {
        var offset_top = qmsl_items_arr[selected_ndx];
        $('#list-container-wrap', context).scrollTop(offset_top);
        selected_ndx = qmsl_items_count - selected_ndx;
    }
    if (selected_ndx == -1) {
        selected_ndx = 0;
        selected_ndx = qmsl_items_count - selected_ndx;
    }

    //Store frequently elements in variables
    var slider1 = $('#slider', context),
		tooltip = $('#tooltip-slider', context);

    //Hide the Tooltip at first
    tooltip.hide();

    slider1.slider({
        //Config
        range: "max",
        min: 0,
        max: qmsl_items_count,
        value: selected_ndx,
        orientation: "vertical",

        start: function (event, ui) {
            //tooltip.fadeIn('fast');
        },

        //Slider Event
        slide: function (event, ui) { //When the slider is sliding
            var value = slider1.slider('value');
            //,volume = $('.volume', context);

            var ndx = qmsl_items_count - parseInt(ui.value);
            if (ndx < qmsl_items_count && ndx >= 0) {
                var offset_top = qmsl_items_arr[ndx];
                $('#list-container-wrap', context).scrollTop(offset_top);
                //var login = $(".login-name", qmsl_items[ndx]).text();
                //tooltip.css('bottom', ui.value + '%').text(offset_top + ":" + (ndx + 1) + ":" + login);
            }
        },

        stop: function (event, ui) {
            //tooltip.fadeOut('fast');
        }
    });

    list_container.bind("slide slidechange", function (e, ui) {
        var val = ui.value;
        // Handle the slider event here
    }).mousewheel(function (e, delta) {
        value = slider1.slider('value');
        value = (value + delta);
        var result = slider1.slider('option', 'slide').call(slider1, e, { value: value });
        slider1.slider('value', value);
        return false;
    });
}




function convList_updateButtonClasses() {
    var scope = "#conv-item .cnv_item";
    var itms = $("a.read-message", scope + " .middle").css("width", "auto");
    for (var c = 0; c < itms.length; c++) {
        var o = $(itms[c]);
        var w = o.width();
        o.css("width", "").removeClass("lnk100").removeClass("lnk174").removeClass("lnk212-green");
        if (w < 88)
            o.addClass("lnk100");
        else if (w < 160)
            o.addClass("lnk174");
        else
            o.addClass("lnk212-green");
        o.parent().width(o.width());
    }
}

function conversations_TopLinks() {
    var scope = "#conv-messages-list .pnlMoreMsgs";
    var itms = $("a.btn", scope).css("width", "auto");
    for (var c = 0; c < itms.length; c++) {
        var w = $(itms[c]).width();
        $(itms[c]).css("width", "").removeClass("lnk83").removeClass("lnk112").removeClass("lnk127").removeClass("lnk161")
        if (w < 73)
            $(itms[c]).addClass("lnk83");
        else if (w < 100)
            $(itms[c]).addClass("lnk112");
        else if (w < 115)
            $(itms[c]).addClass("lnk127");
        else
            $(itms[c]).addClass("lnk161");
    }
}



function conversations_setRowNumberMax(num) {
    if (__setRowNumberMax < num) __setRowNumberMax = num;
}

function conversations_setMessageIDMin(num) {
    if (__setMessageIDMin > num) __setMessageIDMin = num;
}

function conversations_setHandlers(o) {
    if (o) {
        $('a.read-message', $(o)).click(function (e) {
            return performRequest_ReadMessage(e, $(this));
        });
        //var o1 = $('div.delete', $(o));
        //$('a.btn', $(o1)).click(function (e) {
        //return performRequest_MessageDelete($(this));
        //})
        var bodys = $('.m-text .span', $(o));
        emoticonsHandler.SetEmoticonsOnElements(bodys);
    }
    else {
        $('a.read-message', '#conv-messages-list').click(function (e) {
            return performRequest_ReadMessage(e, $(this));
        });
        //$('a.btn', '#conv-messages-list .cnv_item .middle .delete').click(function (e) {
        //return performRequest_MessageDelete($(this));
        //})

        var bodys = $('.cnv_item .m-text .span', '#conv-messages-list');
        emoticonsHandler.SetEmoticonsOnElements(bodys);
    }

    __freeMessagesOnView = $('a.free-msg', '#conv-messages-list').length;
    //alert(__freeMessagesOnView)
}

function performRequest_ReadMessage(event, o) {
    if (_currentMessageOpenerObj == null) {
        if ($(o).attr("working") != "1") {
            $(o).attr("working", "1");
            var href = $(o).attr("href")
            var p = $(o).parent();
            $(o).replaceWith('<div style="text-align:center;"><img src="//cdn.goomena.com/Images2/ajax-loader.gif" atl="Loading..." /></div>');
            performRequest_MessageUnlock(p, href);
            event.stopPropagation();
            event.preventDefault();
            return false;
        }
    }
    return true;
}

function performLoad_MessageReplace(o, data) {
    var d = data.d;
    if (stringIsEmpty(d)) { return; }
    if (d.indexOf("{\"redirect\"") == 0) {
        var action = {};
        eval("action=" + d);
        jsLoad(action["redirect"]);
    }
    else if (d.indexOf("{\"action\"") == 0) {
        var action = {};
        eval("action=" + d);
        if (action["action"] == "trash") {
            var newOnclick = "return deleteMessage(this,event,1);";
            if ($(o).attr("class").indexOf("btn") > -1) {
                $(o).attr("onclick", newOnclick)
            }
            else {
                if ($('.delete', $(o)).length == 1) {
                    var a = $('.delete', $(o));
                    $('a.btn', $(a)).attr("onclick", newOnclick)
                }
                else
                    $('a.btn', $(o)).attr("onclick", newOnclick)
            }
        }
        else if (action["action"] == "remove") {
            var p = $(o).parent();
            while (!p.is('.cnv_item')) {
                p = $(p).parent();
                if (p == null || p.length == 0) break;
            }
            if (p.length > 0) {
                $(p).animate({ opacity: -1 }, 500, function () {
                    $(p).remove();
                });
                //$(p).remove();
                var cnv_itemLen = $('.cnv_item', '#initial_messages').length;
                if (cnv_itemLen == 0) {
                    btnsMoreLess.hideLess();
                    btnsMoreLess.hideMore();
                }
            }
        }
    }
    else {
        var p = $(o).parent();
        while (!p.is('.cnv_item')) { p = $(p).parent(); }
        if (p.length > 0) {
            var id = $(p)[0].id;
            $(p).replaceWith(d);
            var o1 = $('#' + id);
            conversations_setHandlers(o1);
        }
    }

    if ($(o).attr("working") == "1") $(o).attr("working", "0");

    if (typeof req_loadMemberActionCounters !== 'undefined')
        req_loadMemberActionCounters();
}


function writeMessage() {
    $('#send_buttons').hide('fast', function () {
        $('#write_message_wrap').show('fast');
    });
}

function clearMessage() {
    try {
        if (window["txtWriteMsgText"] != null) {
            txtWriteMsgText.SetText("");
        }
    } catch (e) { }
}


function fixMessageContent() {
    try {
        if (window["txtWriteMsgText"] != null) {
            var txt = txtWriteMsgText.GetText();
            txt = txt.replace("<", "&lt;").replace(">", "&gt;")
            txtWriteMsgText.SetText(txt);
        }
    } catch (e) { }
}

function hideWriteMessage() {
    var attr = $(this).attr('DisableHide');
    if (typeof attr !== typeof undefined && attr !== false) {
        $('#write_message_wrap').hide('fast', function () {
            $('#send_buttons').show('fast');
            clearMessage();
        });
    } else {
        if (attr == false) {
            $('#write_message_wrap').hide('fast', function () {
                $('#send_buttons').show('fast');
                clearMessage();
            });
        }

    }
}

function sendingNewMessage(o) {
    if ($(o).attr("working") != "1") {
        $(o).attr("working", "1").attr("disabled", "disabled").addClass("opacity70");
        fixMessageContent();
    }

    function EndRequestHandler1(sender, args) {
        HideLoading();
        $(o).removeAttr("working").removeAttr("disabled").removeClass("opacity70");
        try {
            var prms = null;
            var ui = $('.alert-error', '#write_message')
            if (ui.length > 0) {
                ui.delay(20000).fadeOut(800);
                prms = { updateInitial: true, dateUntil: null };
                writeMessage();
            }

            if (typeof fnError_Sending_Receiver_DoNot_Have_Photo !== 'undefined') {
                // do nothing
                prms = { updateInitial: true, dateUntil: null };
            }
            else if (ui.length == 0) {
                // message was sent, modify ui
                hideWriteMessage();
                prms = { updateInitial: true, dateUntil: null };
                performRequest_UpdateCredits();
            }

            if (prms != null) {
                performRequest_LoadMoreMessages(prms);
            }
            //if (SuspendedMessageInView == true) {
            //    SuspendedMessageInView = false;

            //    performRequest_MessagesList();
            //    if (typeof req_loadMemberActionCounters !== 'undefined')
            //        req_loadMemberActionCounters();
            //}
        }
        catch (e) { }
        Sys.WebForms.PageRequestManager.getInstance().remove_endRequest(EndRequestHandler1);
    }

    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler1);
}


//function notifyAboutNewMessage(messageId) {

//}
//var SuspendedMessageInView = false;
//function notifyAboutSuspendedMessage() {
//    SuspendedMessageInView = true;
//}


var _currentMessageOpenerObj = null;
var _fnSuppressWarning_WhenReadingMessageFromDifferentCountry = false;
function showWarning(event, openerObj) {
    if (_fnSuppressWarning_WhenReadingMessageFromDifferentCountry == false) {
        if (_currentMessageOpenerObj != openerObj) {
            _currentMessageOpenerObj = openerObj;
            openPopupConversationWarning();
            event.stopPropagation();
            event.preventDefault();
            return false;
        }
        else {
            _currentMessageOpenerObj = null;
            performRequest_ReadMessage(event, openerObj);
        }
    }
    return true;
}

function openPopupConversationWarning(headerText, wdt, hgt) {
    var popup = window["popupShowWarning"]
    if (headerText != null) popup.SetHeaderText(headerText);
   popup.Show();

    if (typeof wdt !== 'undefined' && wdt != null) {
        if (typeof hgt !== 'undefined' && hgt != null) {
            popup.SetSize(wdt, hgt);
        }
    }
   popup.UpdatePosition();
}

function continueReadingMessage() {
    _currentMessageOpenerObj.click()
    var popup = window["popupShowWarning"]
    popup.Hide();
    ShowLoading();
    /*
    setTimeout(function () {
    $(_currentMessageOpenerObj).
    attr('disabled', 'disabled').
    click(function (event) { event.preventDefault() });
    }, 100);
    */
}

function skipReadingMessage() {
    _currentMessageOpenerObj = null
    var popup = window["popupShowWarning"]
    popup.Hide();
}


function fnSuppressWarning_WhenReadingMessageFromDifferentCountry(suppressWarning) {
    try {
        _fnSuppressWarning_WhenReadingMessageFromDifferentCountry = suppressWarning;
        var param = $.toJSON({ SuppressWarning: suppressWarning });
        $.ajax({
            type: "POST",
            url: "json.aspx/SuppressWarning_WhenReadingMessageFromDifferentCountry",
            data: param,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            error: function (ar1, ar2, ar3) { },
            success: function (msg) { },
            beforeSend: function () { },
            complete: function () { }
        });
    }
    catch (e) { }
}




var btnsMoreLess = new function () {
    var t = this,
    btnsMoreLessContainer = $('.pnlMoreMsgs', '#conv-messages-list'),
    btnMore = $('.more-messages', '#conv-messages-list .pnlMoreMsgs'),
    btnLess = $('.less-messages', '#conv-messages-list .pnlMoreMsgs');


    t.showMore = function () {
        btnsMoreLessContainer.show();
        btnMore.show();
    };
    t.hideMore = function () {
        btnMore.hide();
        checkWrapper();
    };
    t.showLess = function () {
        btnsMoreLessContainer.show();
        btnLess.css("display", "block")
    };
    t.hideLess = function () {
        btnLess.hide();
        checkWrapper();
    };
    t.isVisibleLess = function () {
        var v = $('.less-messages:visible', '#conv-messages-list .pnlMoreMsgs');
        return (v.length > 0);
    };
    t.isVisibleMore = function () {
        var v = $('.more-messages:visible', '#conv-messages-list .pnlMoreMsgs');
        return (v.length > 0);
    };

    function checkWrapper(showMore, showLess) {
        //showMore, showLess: third value is null
        if (!t.isVisibleMore() && !t.isVisibleLess()) {
            btnsMoreLessContainer.hide();
        }
    }


    t.startUp = function () {
        btnsMoreLessContainer = $('.pnlMoreMsgs', '#conv-messages-list');
        btnMore = $('.more-messages', btnsMoreLessContainer);
        btnLess = $('.less-messages', btnsMoreLessContainer);

        if (btnsMoreLessContainer.attr('data') == 'more-messages') {
            btnsMoreLessContainer.show();
            btnMore.show();
            btnLess.hide();
        }
        else {
            btnsMoreLessContainer.hide();
            btnMore.hide();
            btnLess.hide();
        }
    }

}


function ins2pos(txt, selector) {
    var el = null;
    if (typeof txtWriteMsgText !== 'undefined') {
        if (txtWriteMsgText.GetText() == '') {
            var el1 = txtWriteMsgText.GetInputElement();
            var newText = storeCaret(el1, txt, 'smiley');
            el1.value = el1.value.replace(txtWriteMsgText.nullText, "");
            txtWriteMsgText.SetText(txt);
        }
        else
            el = txtWriteMsgText.GetInputElement();
    }
    else {
        el = $(selector)[0];
    }

    if (el != null) {
        var newText = storeCaret(el, txt, 'smiley');
        setTimeout(function () {
            if (typeof txtWriteMsgText !== 'undefined') {
                txtWriteMsgText.GetText();
                txtWriteMsgText.SetText(newText);
            }
        }, 10);
    }
}

var bd11 = {
    isMozilla: (navigator.userAgent.toLowerCase().indexOf('gecko') != -1) ? true : false,
    isOpera: (navigator.userAgent.toLowerCase().indexOf('opera') != -1) ? true : false,
    isIE: eval("/*@cc_on!@*/!1"),  // [Boolean]
    regexp: new RegExp("[\n]", "gi")
}

function storeCaret(oField, selec, type) {
    var newText = "";
    var isUnknown = !bd11.isIE && !bd11.isMozilla && !bd11.isOpera;

    function forIE() {
        // IE
        //oField = document.forms['news'].elements['newst'];
        var str = document.selection.createRange().text;
        if (str.length > 0) {// if we have selected some text,,   
            var sel = document.selection.createRange();
            if (type) // smiley
            {
                sel.text = " " + selec;
            }
            else {
                sel.text = "[" + selec + "]" + str + "[/" + selec + "]";
            }
            sel.collapse();
            sel.select();
        }
        else {
            oField.focus(oField.caretPos);
            oField.focus(oField.value.length);
            oField.caretPos = document.selection.createRange().duplicate();

            var bidon = "%~%"; // needed to catch the cursor position with IE
            var orig = oField.value;
            oField.caretPos.text = bidon;
            var i = oField.value.search(bidon);

            if (type) // smiley
            {
                oField.value = orig.substr(0, i) + " " + selec + orig.substr(i, oField.value.length);
            }
            else {
                oField.value = orig.substr(0, i) + "[" + selec + "][/" + selec + "]" + orig.substr(i, oField.value.length);
            }

            var r = 0;
            for (n = 0; n < i; n++) {
                if (bd11.regexp.test(oField.value.substr(n, 2)) == true)
                { r++; }
            };
            if (type) // smiley
            {
                pos = i + 1 + selec.length - r;
            }
            else {
                pos = i + 2 + selec.length - r;
            }

            // re-format the textarea & move the cursor to the correct position
            var r = oField.createTextRange();
            r.moveStart('character', pos);
            r.collapse();
            r.select();
        }
    }

    function forMozilla() {
        // Firefox
        //oField = document.forms['news'].elements['newst'];
        oField.focus();
        objectValue = oField.value;

        objectValueStart = objectValue.substring(0, oField.selectionStart);
        objectValueEnd = objectValue.substring(oField.selectionEnd, oField.textLength);
        objectSelected = objectValue.substring(oField.selectionStart, oField.selectionEnd);

        if (type) // smiley
        {
            oField.value = objectValueStart + " " + selec + objectSelected + objectValueEnd;
        }
        else {
            oField.value = objectValueStart + "[" + selec + "]" + objectSelected + "[/" + selec + "]" + objectValueEnd;
        }
        oField.focus();
        if (type) // smiley
        {
            oField.setSelectionRange(objectValueStart.length + selec.length + 2, objectValueStart.length + selec.length + 1);
        }
        else {
            oField.setSelectionRange(objectValueStart.length + selec.length + 2, objectValueStart.length + selec.length + 2);
        }
    }

    if (bd11.isIE) { forIE(); }

    if (bd11.isMozilla || bd11.isOpera) { forMozilla() }

    if (isUnknown) {
        try { forIE(); } catch (e) { }
        try { forMozilla(); } catch (e) { }
    }

    return oField.value;
}

var emoticonsHandler = new function () {
    var t = this;
    var emoticonsFilesDir = "//cdn.goomena.com/Images2/emoticons2/";
    var emoticonsFiles = {
        ":)": ["0100-smile.gif", "smile"], ":-(": ["0101-sadsmile.gif", "sadsmile"], ":-D": ["0102-bigsmile.gif", "bigsmile"], "8-)": ["0103-cool.gif", "cool"],
        ":-o": ["0104-surprised.gif", "surprised"], ";-)": ["0105-wink.gif", "wink"], ";-(": ["0106-crying.gif", "crying"], "(sweat)": ["0107-sweating.gif", "sweating"],
        ":-|": ["0108-speechless.gif", "speechless"], ":-*": ["0109-kiss.gif", "kiss"], ":-p": ["0110-tongueout.gif", "tongueout"], "(blush)": ["0111-blush.gif", "blush"],
        ":^)": ["0112-wondering.gif", "wondering"], "(snooze)": ["0113-sleepy.gif", "sleepy"], "|-(": ["0114-dull.gif", "dull"], "(inlove)": ["0115-inlove.gif", "inlove"],
        "(grin)": ["0116-evilgrin.gif", "evilgrin "], "(talk)": ["0117-talking.gif", "talking"], "(yawn)": ["0118-yawn.gif", "yawn "], "(puke)": ["0119-puke.gif", "puke "],
        "(doh)": ["0120-doh.gif", "doh "], ":-@": ["0121-angry.gif", "angry"], "(wasntme)": ["0122-itwasntme.gif", "it wasn't me "], "(party)": ["0123-party.gif", "party "],
        ":-s": ["0124-worried.gif", "worried "], "(mm)": ["0125-mmm.gif", "mmm "], "(nerd)": ["0126-nerd.gif", "nerd "], ":-x": ["0127-lipssealed.gif", "lipssealed "],
        "(hi)": ["0128-hi.gif", "hi"], "(call)": ["0129-call.gif", "call"], "(devil)": ["0130-devil.gif", "devil"], "(angel)": ["0131-angel.gif", "angel"],
        "(envy)": ["0132-envy.gif", "envy "], "(wait)": ["0133-wait.gif", "wait "], "(hug)": ["0134-bear.gif", "bear hug "], "(makeup)": ["0135-makeup.gif", "mail "],
        "(chuckle)": ["0136-giggle.gif", "giggle "], "(clap)": ["0137-clapping.gif", "clapping "], "(think)": ["0138-thinking.gif", "thinking "], "(bow)": ["0139-1-bow.gif", "bow "],
        "(ok)": ["0139-yes.gif", "yes "], "(N)": ["0140-no.gif", "no "], "(handshake)": ["0141-handshake.gif", "handshake "],
        "(h)": ["0143-heart.gif", "heart"], "(u)": ["0144-brokenheart.gif", "brokenheart "], "(m)": ["0145-mail.gif", "mail "], "(F)": ["0146-flower.gif", "flower "],
        "(rain)": ["0147-rain.gif", "raining "], "(sun)": ["0148-sun.gif", "sun "], "(time)": ["0149-time.gif", "time "], "(music)": ["0150-music.gif", "music "],
        "(movie)": ["0151-movie.gif", "movie"], "(ph)": ["0152-phone.gif", "phone "], "(coffee)": ["0153-coffee.gif", "coffee "], "(pizza)": ["0154-pizza.gif", "pizza "],
        "(cash)": ["0155-cash.gif", "cash "], "(flex)": ["0156-muscle.gif", "muscle "], "(cake)": ["0157-cake.gif", "cake "], "(beer)": ["0158-beer.gif", "beer"],
        "(d)": ["0159-drink.gif", "drink "], "(dance)": ["0160-dance.gif", "dance "], "(ninja)": ["0161-ninja.gif", "ninja"], "(*)": ["0162-star.gif", "star "],
        "(mooning)": ["0163-mooning.gif", "mooning "], "(finger)": ["0164-middlefinger.gif", "middlefinger "], "(bandit)": ["0165-bandit.gif", "bandit "], "(drunk)": ["0166-drunk.gif", "drunk "],
        "(smoking)": ["0167-smoke.gif", "smoke "], "(toivo)": ["0168-toivo.gif", "toivo "], "(rock)": ["0169-rock.gif", "rock"], "(headbang)": ["0170-headbang.gif", "headbang"]
    }
    //"(skype)": ["0142-skype.gif", "skype "],

    var emoticonsFiles_old = {
        "(angel)": ["angel.gif", "angel"], ":@": ["angry.gif", "angry"], "(hug)": ["bearhug.gif", "bear hug"], "(beer)": ["beer.gif", "beer"],
        "(blush)": ["blush.gif", "blushing"], "(bow)": ["bow.gif", "bowing"], "(punch)": ["boxing.gif", "boxing"], "(u)": ["brokenheart.gif", "broken heart"],
        "(^)": ["cake.gif", "cake"], "(call)": ["callme.gif", "call me"], "(cash)": ["cash.gif", "cash"], "(mp)": ["cellphone.gif", "mobile phone"],
        "(clap)": ["clapping.gif", "clapping"], "(coffee)": ["coffee.gif", "coffee"], "8-)": ["cool.gif", "cool"], ";(": ["crying.gif", "crying"],
        "(dance)": ["dance.gif", "dancin "], "(devil)": ["devil.gif", "devil"], "(doh)": ["doh.gif", "doh "], "(d)": ["drink.gif", "drink "],
        "|-(": ["dull.gif", "dull "], "(emo)": ["emo.gif", "secret"], "]:)": ["evilgrin.gif", "evil grin "], "(flex)": ["flex.gif", "flexing "],
        "(F)": ["flower.gif", "flower "], "(chuckle)": ["giggle.gif", "giggling "], "(handshake)": ["handshake.gif", "handshake "], "(happy)": ["happy.gif", "happy "],
        "(h)": ["heart.gif", "moon"], "(wave)": ["hi.gif", "hi "], "(inlove)": ["inlove.gif", "in love "], "(wasntme)": ["itwasntme.gif", "it wasn&#39;t me "],
        "(envy)": ["jealous.gif", "jealous "], ":*": ["kiss.gif", "kissing "], ":D": ["laughing.gif", "laughing "], "(e)": ["mail.gif", "mail "],
        "(makeup)": ["makeup.gif", "makeup "], "(mm)": ["mmm.gif", "mmmm "], "(~)": ["movie.gif", "movie "], "(music)": ["music.gif", "music "],
        "8-|": ["nerd.gif", "nerdy "], "(ninja)": ["ninja.gif", "ninja "], "(n)": ["no.gif", "no "], "(nod)": ["nod.gif", "nodding "],
        ":x": ["nospeak.gif", "speechless"], "(party)": ["party.gif", "party "], "(pi)": ["pizza.gif", "pizza "], "(puke)": ["puke.gif", "sick "],
        "(rain)": ["rain.gif", "raining "], "(rofl)": ["rofl.gif", "rofl "], ":(": ["sad.gif", "sad "], "(shake)": ["shakeno.gif", "no "],
        "|-)": ["sleepy.gif", "sleepy "], ":)": ["smile.gif", "smiling "], "(smirk)": ["smirk.gif", "smirking "],
        ":-|": ["speechless.gif", "speechless "], "(*)": ["star.gif", "star "], "(sun)": ["sun.gif", "sun "], "(sweat)": ["sweating.gif", "sweating "],
        "(talk)": ["talking.gif", "talking "], "(think)": ["thinking.gif", "thinking "], "(o)": ["time.gif", "clock "], "(yawn)": ["tired.gif", "tired "],
        ":p": ["tongue out.gif", "tongue out "], "(wait)": ["wait.gif", "wait "], "(whew)": ["whew.gif", "whew "], ";)": ["wink.gif", "winking "],
        ":^)": ["wondering.gif", "wondering "], ":S": ["worried.gif", "worried "], "(y)": ["yes.gif", "thumbs up "], "(bandit)": ["hiddenbandit.gif", "bandit"],
        "(bug)": ["hiddenbug.gif", "bug"], "(drunk)": ["hiddendrunk.gif", "drunk"], "(finger)": ["hiddenfinger.gif", "finger"], "(fubar)": ["hiddenfubar.gif", "fubar"],
        "(headbang)": ["hiddenheadbang.gif", "headbang"], "(mooning)": ["hiddenmooning.gif", "mooning"], "(poolparty)": ["hiddenpoolparty.gif", "pool party"], "(rock)": ["hiddenrockout.gif", "rock out"],
        "(smoking)": ["hiddensmoking.gif", "smoking"], "(heidy)": ["hiddensquirrell.gif", "squirell"], "(swear)": ["hiddenswear.gif", "swearing"], "(tmi)": ["hiddentmi.gif", "tmi"],
        "(toivo)": ["hiddentoivo.gif", "tovio"]

    }
    //"(skype)": ["skype.gif", "logo"],
    t.ShowEmoticonsHTML = function (txt) {
        if (stringIsEmpty(txt)) return txt;
        var img = '<img border="0" src="//cdn.goomena.com/Images2/emoticons2/{IMAGE}" alt="{TITLE}" title="{TITLE}" />';
        for (a in emoticonsFiles) {
            while (txt.indexOf(a) > -1) {
                var img1 = img.
                    replace("{IMAGE}", emoticonsFiles[a][0]).
                    replace("{TITLE}", emoticonsFiles[a][1]).
                    replace("{TITLE}", emoticonsFiles[a][1]);
                txt = txt.replace(a, img1);
            }
        }
        return txt;
    }

    t.GetEmoticonsListHTML = function () {
        var html = '<ul id="emoticons-list">{LIST}</ul>';
        var htmlItems = '<li><a href="#" title="{REPRESENT}"><img border="0" src="//cdn.goomena.com/Images2/emoticons2/{IMAGE}" alt="{TITLE}" title="{TITLE}" /></a></li>';
        var result = '';
        for (a in emoticonsFiles) {
            var htmlItems1 = htmlItems.
                replace("{REPRESENT}", a).
                replace("{IMAGE}", emoticonsFiles[a][0]).
                replace("{TITLE}", emoticonsFiles[a][1]).
                replace("{TITLE}", emoticonsFiles[a][1]);
            result = result + htmlItems1;
        }

        html = html.replace("{LIST}", result);
        return html;
    }


    t.SetEmoticonsOnElements = function (bodys) {
        for (c = 0; c < bodys.length; c++) {
            var html = $(bodys[c]).html();
            var html2 = t.ShowEmoticonsHTML(html);
            if (html != html2)
                $(bodys[c]).html(html2);
        }
    }

    /*
    t.test = function () {
        var txt = "asfasdfasd (toivo) asdfasdfasdf (heidy) asdfasdf";
        var txt2 = t.ShowEmoticonsHTML(txt);
        alert(txt2);
    }

    t.test();
    */
}

function LoadEmoticonsList() {
    if ($('ul', '#emoticons-box').length == 0) {
        var result = emoticonsHandler.GetEmoticonsListHTML();
        var msg = { d: result };
        performLoad_EmoticonsList(msg);
        //performRequest_EmoticonsList();
    }
}

function performRequest_EmoticonsList() {
    try {
        var param = {}
        $.ajax({
            type: "POST",
            url: "json.aspx/GetEmoticonsList",
            data: param,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            error: function (ar1, ar2, ar3) {
                if (__retries_emoticons_list_loading > 0) {
                    setTimeout(performLoad_EmoticonsList, 3000);
                    __retries_emoticons_list_loading--;
                }
                else {
                    __retries_emoticons_list_loading = 5;
                }
            },
            success: function (msg) {
                performLoad_EmoticonsList(msg);
            },
            beforeSend: function () { },
            complete: function () { }
        });
    }
    catch (e) { }
}



function performLoad_EmoticonsList(data) {
    var d = data.d;
    if (stringIsEmpty(d)) { return; }
    $('#emoticons-box').html(d);
    $('#emoticons-box').append("<div class=\"clear\"></div>");
    $('#emoticons-box a').attr("onclick", "return false;");
    $('#emoticons-box a').click(function (e) {
        var smiley = $(this).attr('title');
        ins2pos(smiley, '#write_message textarea');
        toggleEmoticons();
    });

}

var __Emoticons_visible;
function toggleEmoticons() {
    if (__Emoticons_visible == null) {
        if ($('#emoticons-box:visible').length > 0)
            __Emoticons_visible = true;
    }
    if (!__Emoticons_visible) {
        $(document).bind("click", hideEmoticons);
        $('#emoticons-box').show('fast');
    }
    else {
        $(document).unbind("click", hideEmoticons);
        $('#emoticons-box').hide('fast');
    }
    __Emoticons_visible = (!__Emoticons_visible);
}

function hideEmoticons(e) {
    var close = true;
    var o = e.target
    if ($(o).length > 0 && $(o)[0].id == 'msg-emoticons') { close = false; }
    else {
        var p = $(o).parent();
        if (p == null || p.length == 0) {
        }
        else {
            var depth = 10;
            while (p[0].id != 'msg-emoticons') {
                p = $(p).parent();
                if (--depth == 0) p = null;
                if (p == null || p.length == 0) break;
            }
        }
        if (p != null && p.length > 0)
            close = false;
    }
    if (close) toggleEmoticons();
}


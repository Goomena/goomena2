﻿function htmlEncode(value) {
    return $("<div/>").text(value).html();
}

var CurrentShowing = 0;
var ids = [];
function showAvaliableNotification() {
    try {
        if (CurrentShowing < 2 && ids.length>0) {
           try {
                var id = ids.shift();
                CurrentShowing++;
                setTimeout(function () { $("#NotificationContainer #Notifications div[id='" + id + "']").addClass("show"); }, 250);
                setTimeout(function () { RemoveFromNotifications(id); }, 4000);
                showAvaliableNotification();
            } catch (e) { }
        }
        } catch (e) { }
}
function RemoveFromNotifications(notificationId) {
    MarkMessageAsRead(notificationId);
    $("#NotificationContainer #Notifications div[id='" + notificationId + "']").removeClass("show");
    setTimeout(function () { $("#NotificationContainer #Notifications div[id='" + notificationId + "']").remove(); }, 300);
    CurrentShowing--;
    showAvaliableNotification();
}
function addNotification(msg, Id) {
    if ($("#NotificationContainer #Notifications div[id='" + Id + "']").length > 0) {
    } else {

        $("#NotificationContainer #Notifications").append(msg);
        ids.push(Id);
        showAvaliableNotification();
    }
    
}
function addNotificationTest(msg, Id) {
    $("#NotificationContainer #Notifications").append(msg);
  
}
$(function (){
    RetrieveUnreadNotifications();
    });
function ConnectToNotifyApp() {
    // Connect Hubs without the generated proxy
    //var timeHubProxy = $.connection.PascalCasedMyDateTimeHub;
    var chatHubProxy = $.connection.NotificationHub;

    // Register client function to be called by server
    chatHubProxy.client.appendNewMessage = function (notification) {
        RetrieveNotification(notification);
    }

    // Start the hub connection
   addNotificationTest("Connecting Hub...", 1);
    $.connection.hub.start().done(function () {
        addNotificationTest("Hub connected.", 1);
    }).fail(function () {
        addNotificationTest("Could not connect to Hub.", 1);
    });
    }


function WorkWithItems(list) {
    try {
        for (var i = 0; i < list.length; i++) {
            var itm = list[i];
            if (itm !== undefined && itm !== null) {
                if (itm.NotificationType && itm.ItemHtml && itm.ItemId) {
                    if (itm.ItemHtml != "") {
                        if (itm.NotificationType != 1) {
                            addNotification(itm.ItemHtml, itm.ItemId);
                        }
                    }
                }
            }
        }
    } catch (e) { }
    }

//---------------------------Json Functions----------------
var RetrieveNotificationTries = 3;
function RetrieveNotification(notificationId) {
    try {
        var param = $.toJSON({ "Id": notificationId });
        // ShowLoading()
        $.ajax({
            type: "POST",
            url: "json.aspx/GetNotification",
            data: param,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            error: function () {
                if (RetrieveNotificationTries > 0) {
                    setTimeout(function () { RetrieveNotification(notificationId); }, 3000);
                    RetrieveNotificationTries--;
                }
                else {
                    RetrieveNotificationTries = 3;
                }
            },
            success: function (obj) {
                try {
                    RetrieveNotificationTries = 3;
                    if (obj.d != "") {
                        var Result = eval("(" + obj.d + ")");
                        if (Result) {
                            if (Result.HasErrors == false) {
                                if (Result.Items)
                                    WorkWithItems(Result.Items);
                            }

                        }
                    }

                } catch (err) { }
            },
            beforeSend: function () { },
            complete: function () { }
        });
    }
    catch (e) { }
}
var MarkMessageAsReadTries = 3;
function MarkMessageAsRead(MessageId) {
    try {
        var param = $.toJSON({ "Id": MessageId });
        // ShowLoading()
        $.ajax({
            type: "POST",
            url: "json.aspx/MarkMessageAsRead",
            data: param,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            error: function () {
                if (MarkMessageAsReadTries > 0) {
                    setTimeout(function () { MarkMessageAsRead(MessageId); }, 3000);
                    MarkMessageAsReadTries--;
                }
                else {
                    MarkMessageAsReadTries = 3;
                }
            },
            success: function () {},
            beforeSend: function () {},
            complete: function () {}
        });
    }
    catch (e) { }
}
var RetrieveUnreadTries = 3;
function RetrieveUnreadNotifications() {
    try {
        var param = "";
        // ShowLoading()
        $.ajax({
            type: "POST",
            url: "json.aspx/GetMyUnreadNotification",
            data: param,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            error: function () {
                if (RetrieveUnreadTries > 0) {
                    setTimeout(function () { RetrieveUnreadNotifications(); }, 3000);
                    RetrieveUnreadTries--;
                }
                else {
                    RetrieveUnreadTries = 3;
                }
            },
            success: function (obj) {
                try {
                    RetrieveUnreadTries = 3;
                    if (obj.d != "") {
                        var Result = eval("(" + obj.d + ")");
                        if (Result) {
                            if (Result.HasErrors == false) {
                                if (Result.Items)
                                    WorkWithItems(Result.Items);
                            }

                        }
                    }

                } catch (err) { }
            },
            beforeSend: function () { },
            complete: function () { ConnectToNotifyApp();}
        });
    }
    catch (e) { }
}
//---------------------------End Json Functions----------------
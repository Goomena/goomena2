
var msg_HideAllLangs, msg_ShowAllLangs, msg_ClickToExpand, msg_ClickToCollapse;

function OnCountryChanged(cbCountry) {
    HideLoading();
    cbpnlZip.PerformCallback("country_" + cbCountry.GetValue().toString());
    ShowLoading();
}
function OnRegionChanged(cbRegion) {
    HideLoading();
    cbpnlZip.PerformCallback("region_" + cbRegion.GetValue().toString());
    ShowLoading();
}
function OnCityChanged(cbCity) {
    HideLoading();
    cbpnlZip.PerformCallback("city_" + cbCity.GetValue().toString() + "_region_" + cbRegion.GetValue().toString());
    ShowLoading();
}

function cbpnlZip_PerformCallback(s, e) {
    HideLoading();
    ToggleLanguages(true);
    $('.show-all', '#liSpokenLangs').click(function () {
        ToggleLanguages();
    });
}


function ToggleLanguages(showTopRow) {
    if (showTopRow) {
        var trs = $('tr', '#liSpokenLangs .spoken-langs-list');
        for (var i = 0; i < trs.length; i++) {
            if (i == 0) { $(trs[i]).addClass("langs-top").removeClass('hidden'); }
            else if (i == 1) { $(trs[i]).addClass("langs-top").addClass("line").removeClass('hidden'); }
            else { $(trs[i]).addClass("langs-next").removeClass('hidden'); }
        }
    }

    var isCollapsed = $('.show-all', '#liSpokenLangs').is('.collapsed');
    if (isCollapsed) {
        //$('tr.langs-next', '#liSpokenLangs').show();
        $('#spoken-langs-wrap', '#liSpokenLangs').addClass('list-open');
        $('.show-all', '#liSpokenLangs').removeClass('collapsed').addClass('expanded').html(msg_HideAllLangs);
    }
    else {
        //$('tr.langs-next', '#liSpokenLangs').hide();
        $('#spoken-langs-wrap', '#liSpokenLangs').removeClass('list-open');
        $('.show-all', '#liSpokenLangs').removeClass('expanded').addClass('collapsed').html(msg_ShowAllLangs);
    }
}


function collapsibleHeader_toggled(pe_box) {
    // Animation complete.
    var isVisible = ($('.pe_form', pe_box).css('display') == 'none');
    if (isVisible) {
        $('.togglebutton', pe_box).text("+");
        $('.collapsibleHeader', pe_box).attr("title", msg_ClickToExpand);
    }
    else {
        $('.togglebutton', pe_box).text("-");
        $('.collapsibleHeader', pe_box).attr("title", msg_ClickToCollapse);
    }
}


function toggleTravelingPlaces(s, e) {
    if (s.GetChecked() == true) {
        $('#liTravelingPlaces').removeClass('hidden');
    }
    else {
        $('#liTravelingPlaces').addClass('hidden');
    }
}


function checkAddressText(s, e) {
    $("#lnkAddressUrl").addClass("hidden");
    var source = s.GetText();
    var result = URI.withinString(source, function (url) {
        if ($("#lnkAddressUrl").is(".hidden")) {
            $("#lnkAddressUrl").removeClass("hidden").attr("href", url);
        }
        return url;
        //// callback needs to return a string
        //// feel free to URI(url).normalize().toString() or something
        //return "<a>" + url + "</a>";
    });
}

function profileEditLoad() {
    jQuery(function ($) {
        ToggleLanguages(true);
        //Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function () { ToggleLanguages(true) });
        $('.show-all', '#liSpokenLangs').click(function () {
            ToggleLanguages();
        });

        $('.pe_box').each(function () {
            var pe_box = this;
            $('.collapsibleHeader', pe_box).click(function () {
                $('.pe_form', pe_box).toggle('fast', function () {
                    collapsibleHeader_toggled(pe_box)
                });
            });
        });
        setTimeout(ProfileEdit_scrollToEditPosition, 500);
    });
}


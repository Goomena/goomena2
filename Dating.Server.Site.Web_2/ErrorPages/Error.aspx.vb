﻿Public Class _Error
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim errorInfo As String = ""
        Try
            Dim ctx As HttpContext = HttpContext.Current
            Dim exception As Exception = ctx.Server.GetLastError()
            If (exception IsNot Nothing) Then errorInfo = exception.ToString()
            '  "<br>Offending URL: " + ctx.Request.Url.ToString() +
            '  "<br>Source: " + exception.Source +
            '  "<br>Message: " + exception.Message

            ' label has not viewstate
            'lbHTMLBody.Text = lbHTMLBody.Text & errorInfo

        Catch
        End Try
        Try
            WebInfoSendEmail("Error page loaded." & vbCrLf & errorInfo)
        Catch
        End Try

    End Sub

End Class
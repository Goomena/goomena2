﻿Imports Library.Public

Public Class PubSearchSelect
    Inherits BasePage


#Region "Props"


    'Dim _pageData As clsPageData
    'Protected ReadOnly Property CurrentPageData As clsPageData
    '    Get
    '        If (_pageData Is Nothing) Then
    '            _pageData = New clsPageData(Context)
    '            AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
    '        End If
    '        Return _pageData
    '    End Get
    'End Property

#End Region


    Protected Overrides Sub Page_PreInit(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreInit
        SetMasterPage(CurrentPageData)
        MyBase.Page_PreInit(sender, e)

        'If clsCurrentContext.VerifyLogin() = True Then
        '    Me.MasterPageFile = "~/Members/Members2.Master"
        'End If

    End Sub



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub


    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        Try
            Me._pageData = Nothing
            LoadLAG()
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

        Try
            Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub



    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
    End Sub


    Protected Sub LoadLAG()
        Try
            Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = Me.CurrentPageData.cPageBasic
            lblHeader.Text = Me.CurrentPageData.GetCustomString(lblHeader.ID)
            lbHTMLBody.Text = Me.FixHTMLBodyAnchors(cPageBasic.BodyHTM)
            lblinside.Text = Me.CurrentPageData.GetCustomString("lblinside")
            lblWantToMeetWoman.Text = Me.CurrentPageData.GetCustomString("lblWantToMeetWoman")
            lblWantToMeetMan.Text = Me.CurrentPageData.GetCustomString("lblWantToMeetMan")

            'Dim _popupTrigger1_ As String = Me.CurrentPageData.GetCustomString("_popupTrigger1_")
            'If (Not String.IsNullOrEmpty(_popupTrigger1_) AndAlso lbHTMLBody.Text.Contains("_popupTrigger1_")) Then
            '    '<IMG id="_popupTrigger1_" src="http://www.dating-deals.com/images/icon_tip.png">
            '    Dim win As New DevExpress.Web.ASPxPopupControl.PopupWindow(_popupTrigger1_)
            '    win.PopupElementID = "_popupTrigger1_"
            '    popupWindows.Windows.Add(win)
            'End If


            ''AppUtils.setSEOPageData(Me, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
            ''AppUtils.setNaviLabel(Me, "lbPublicNavi2Page", cPageBasic.PageTitle)
            'If (TypeOf Page.Master Is INavigation) Then
            '    CType(Page.Master, INavigation).AddNaviLink(Me.CurrentPageData.Title, "")
            'End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

End Class
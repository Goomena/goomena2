﻿Public Class Login
    Inherits System.Web.UI.Page

    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If clsCurrentContext.VerifyLogin() = True Then
            Dim url As String = ResolveUrl("~/Members/Default.aspx")
            Response.Redirect(url)
        End If
    End Sub

End Class
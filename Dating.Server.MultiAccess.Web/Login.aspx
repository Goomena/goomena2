﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Login.aspx.vb" Inherits="Dating.Server.MultiAccess.Web.Login" %>
<%@ Register src="~/Security/UserControls/ucLogin.ascx" tagname="ucLogin" tagprefix="uc1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Login to MultiAccess</title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="sm1" runat="server">
        </asp:ScriptManager>
        <div style="height:150px;"></div>
        <h1 style="margin:10px auto;text-align:center;">Login to MultiAccess</h1>
        <div style="width:240px;margin:0 auto;"> 
            <%--<asp:UpdatePanel ID="updLog" runat="server">
                <ContentTemplate>--%>
                    <uc1:ucLogin ID="ucLogin1" runat="server">
                        <logincontrol>
                                    <LayoutTemplate>
                                        <asp:HiddenField ID="hdfHideRecaptcha" runat="server" Visible="false" />
                                 <table class="logintable" cellspacing="5" cellpadding="2">
                                              <tr>
                                                    <td colspan="2"></td>
                                              </tr>
                                              <tr>
                                                  <td id="lblUsernameTopTD" runat="server"><asp:Label ID="lblUsernameTop" runat="server" Text="User name"/> </td>
                                                  <td id="UserNameTD" runat="server"><div class="text-wrap"><dx:ASPxTextBox ID="UserName" runat="server">
                                                          <ValidationSettings ValidationGroup="QuickLoginGroup" ErrorTextPosition="Right" Display="Dynamic"
                                                              ErrorDisplayMode="Text">
                                                              <RequiredField ErrorText="Username or Email is required." IsRequired="true" />
                                                          </ValidationSettings>
                                                      </dx:ASPxTextBox></div></td>
                                              </tr>
                                              <tr>
                                                    <td colspan="2"></td>
                                              </tr>
                                              <tr>
                                                  <td ID="lblPasswordTopTD" runat="server"><asp:Label ID="lblPasswordTop" runat="server" Text="Password"/></td>
                                                  <td id="PasswordTD" runat="server"><div class="text-wrap"><dx:ASPxTextBox ID="Password" runat="server"  Password="True">
                                                          <ValidationSettings ValidationGroup="QuickLoginGroup" ErrorTextPosition="Right" Display="Dynamic"
                                                              ErrorDisplayMode="Text">
                                                              <RequiredField ErrorText="Password is required." IsRequired="true" />
                                                          </ValidationSettings>
                                                      </dx:ASPxTextBox></div></td>
                                              </tr>
                                              <tr>
                                                    <td colspan="2"></td>
                                              </tr>
                                              <tr id="login_bottom_row">
                                                  <td ID="btnLoginTD" runat="server" colspan="2" align="center"><div class="connect-wrap"><dx:ASPxButton ID="btnLogin" runat="server" Text="Login" 
                                                            CommandName="Login">
                                                        </dx:ASPxButton></div></td>
                                              </tr>
                                </table>
                                <div class="Errors" style="position:absolute; bottom:0px; right:0px;">
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" />
                                </div>
                                               </LayoutTemplate>
                                           </logincontrol>
                    </uc1:ucLogin>
               <%-- </ContentTemplate>
            </asp:UpdatePanel>--%>

        </div>
    </form>
</body>
</html>

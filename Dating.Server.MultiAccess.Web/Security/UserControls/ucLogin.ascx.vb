﻿Imports System.ComponentModel
Imports DevExpress.Web.ASPxEditors

Public Class ucLogin
    Inherits System.Web.UI.UserControl


    Public Event LoggingIn(e As System.Web.UI.WebControls.LoginCancelEventArgs)
    Public Event LoggingInFailed(ByRef sender As Object, ByRef e As LoggingInFailedEventArgs)

    Public Property LogginFailReason As LogginFailReasonEnum

    'Protected Overloads ReadOnly Property CurrentPageData As clsPageData
    '    Get
    '        'If (_pageData Is Nothing) Then _pageData = New clsPageData("Login.aspx", Context)
    '        If (_pageData Is Nothing) Then
    '            _pageData = New clsPageData("Login.aspx", Context)
    '            AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
    '        End If
    '        Return _pageData
    '    End Get
    'End Property



    Public Enum LoginViewEnum
        Standard = 1
        LoggedIn = 3
    End Enum


    Public Property LoginView As LoginViewEnum
        Get

            'If (MultiView1.Views(MultiView1.ActiveViewIndex) Is vwTemplated) Then
            '    Return LoginViewEnum.Templated
            'Else
            If (MultiView1.Views(MultiView1.ActiveViewIndex) Is vwLoggedInUser) Then
                Return LoginViewEnum.LoggedIn
            End If

            Return LoginViewEnum.Standard
        End Get
        Set(ByVal value As LoginViewEnum)

            'If (value = LoginViewEnum.Templated) Then
            '    MultiView1.SetActiveView(vwTemplated)
            'Else
            If (value = LoginViewEnum.Standard) Then
                MultiView1.SetActiveView(vwStandard)
            ElseIf (value = LoginViewEnum.LoggedIn) Then
                MultiView1.SetActiveView(vwLoggedInUser)
            End If

        End Set
    End Property


    <PersistenceMode(PersistenceMode.InnerDefaultProperty), _
    Browsable(True), _
Category("Behavior"), _
Editor("System.Web.UI.WebControls.Login", GetType(System.Web.UI.WebControls.Login))> _
    Public ReadOnly Property LoginControl As System.Web.UI.WebControls.Login
        Get
            Return MainLogin
        End Get
    End Property


    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If (clsCurrentContext.VerifyLogin()) Then
            LoginView = LoginViewEnum.LoggedIn
        Else
            LoginView = LoginViewEnum.Standard
        End If

    End Sub




    Private Sub Login1_LoggingIn(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.LoginCancelEventArgs) Handles MainLogin.LoggingIn
        Try

            e.Cancel = True
            Dim continueAuth As Boolean = True
            Dim hdfHideRecaptcha As HiddenField = MainLogin.FindControl("hdfHideRecaptcha")


            If (continueAuth) Then
                Dim userName As String = MainLogin.UserName
                Dim password As String = MainLogin.Password
                Dim rememberUserName As Boolean = MainLogin.RememberMeSet


                Dim ctrlUserName As ASPxTextBox = MainLogin.FindControl("UserName")
                If (ctrlUserName IsNot Nothing) Then
                    userName = ctrlUserName.Text
                End If

                Dim ctrlPassword As ASPxTextBox = MainLogin.FindControl("Password")
                If (ctrlPassword IsNot Nothing) Then
                    password = ctrlPassword.Text
                End If

                'Dim RememberMe As CheckBox = MainLogin.FindControl("RememberMe")
                'If (RememberMe IsNot Nothing) Then
                '    rememberUserName = RememberMe.Text
                'End If

                Dim RememberMe As ASPxCheckBox = MainLogin.FindControl("chkRememberMe")
                If (RememberMe IsNot Nothing) Then
                    rememberUserName = RememberMe.Checked
                End If


                PerformLogin(userName, password, rememberUserName)
            End If

            Dim check As Boolean = (Request.QueryString("check") = "1")
            If (Not check AndAlso Not String.IsNullOrEmpty(lblError.Text)) Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "failed-auth", "HideLoading();", True)
            End If

        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            ''WebErrorSendEmail(ex, "")
        End Try
    End Sub


    Private Function PerformLogin(ByVal userName As String, ByVal password As String, ByVal rememberUserName As Boolean) As String
        Dim returnUrl As String = "Login.aspx"
        Dim success As String = ""
        lblError.Text = ""

        'Fetch the role
        Dim role As String = String.Empty
        Dim check As Boolean = (Request.QueryString("check") = "1")

        If (Not String.IsNullOrEmpty(userName) AndAlso Not String.IsNullOrEmpty(password)) Then

            userName = userName.Trim()
            password = password.Trim()


            If (String.Compare(userName, "jim", True) = 0 AndAlso String.Compare(password, "jim2000", True) = 0) Then
                returnUrl = ResolveUrl("~/Members/Default.aspx")

                'Create Form Authentication ticket
                Dim ticket As New FormsAuthenticationTicket(1, userName, DateTime.Now, DateTime.Now.AddDays(5), False, "Member", FormsAuthentication.FormsCookiePath)


                'For security reasons we may hash the cookies
                Dim hashCookies As String = FormsAuthentication.Encrypt(ticket)
                Dim cookie As New HttpCookie(FormsAuthentication.FormsCookieName, hashCookies)

                cookie.Path = FormsAuthentication.FormsCookiePath()
                If rememberUserName Then
                    cookie.Expires = ticket.Expiration
                End If

                'cookie.Domain = Nothing

                'add the cookie to user browser
                HttpContext.Current.Response.Cookies.Remove(cookie.Name)
                HttpContext.Current.Response.Cookies.Add(cookie)

                Session("ProfileID") = 1
                Session("LoginName") = userName

            End If
          
        End If

        success = lblError.Text

        If (Not String.IsNullOrEmpty(returnUrl)) Then
            Response.Redirect(returnUrl)
        End If

        Return success
    End Function

    Protected Sub MainLogin_LoginError(sender As Object, e As EventArgs) Handles MainLogin.LoginError

    End Sub

    Protected Sub LN_Load(sender As Object, e As EventArgs) Handles LN.Load
        'Dim a = DirectCast(sender, System.Web.UI.WebControls.LoginName).ToString()
        '.UserName()
    End Sub

    Protected Sub MultiView1_ActiveViewChanged(ByVal sender As Object, ByVal e As EventArgs) Handles MultiView1.ActiveViewChanged

    End Sub
End Class
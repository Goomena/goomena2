﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucLogin.ascx.vb" Inherits="Dating.Server.MultiAccess.Web.ucLogin" %>


<asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">

    <asp:View ID="vwStandard" runat="server">
        <asp:Login ID="MainLogin" runat="server">
        </asp:Login>
        <div style="margin:10px;"><asp:Label ID="lblError" runat="server" Text="" ForeColor="red" Visible="false" EnableViewState="false" CssClass="login_error"></asp:Label></div>
    </asp:View>
    <asp:View ID="vwLoggedInUser" runat="server">
        <div class="pub-top-welcome">
            <asp:HyperLink ID="lblLoginName" runat="server" NavigateUrl="~/Members/Default.aspx"
                ForeColor="Black">
                <asp:LoginName ID="LN" runat="server" FormatString="Welcome, {0}" Font-Size="12pt"
                    Font-Bold="true" ForeColor="Black" />
            </asp:HyperLink>&nbsp;&nbsp;&nbsp;&nbsp;
            <div style="height: 8px;">&nbsp;</div>
        </div>

<div class="clear"></div>
    </asp:View>


</asp:MultiView>




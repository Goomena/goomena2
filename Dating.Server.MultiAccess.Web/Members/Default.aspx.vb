﻿Public Class _Default1
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not clsCurrentContext.VerifyLogin()) Then
            Response.Redirect(ResolveUrl("~/Default.aspx"))
        End If

    End Sub

    Protected Sub LoginStatus1_LoggingOut(sender As Object, e As LoginCancelEventArgs) Handles LoginStatus1.LoggingOut
        Try

            clsCurrentContext.ClearSession(Session)
            'Dim url As String = "http://www.admincash.net"
            'If (Not String.IsNullOrEmpty(ConfigurationManager.AppSettings("RedirectURLOnLogout"))) Then
            '    url = ConfigurationManager.AppSettings("RedirectURLOnLogout")
            'End If
            FormsAuthentication.SignOut()
            Response.Redirect(ResolveUrl("~/Login.aspx"))

        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            'WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub
End Class
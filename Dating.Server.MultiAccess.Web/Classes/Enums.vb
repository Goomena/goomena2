﻿



Public Enum LogginFailReasonEnum
    None = 0
    UserNameOrPasswordInvalid = 1
    UserDeleted = 2
    OtherErrorOcurredRetry = 4
    UserNameEmpty = 8
    PasswordEmpty = 16
End Enum



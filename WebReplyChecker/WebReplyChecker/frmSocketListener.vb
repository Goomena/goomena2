﻿
Imports System
Imports System.Xml
Imports System.Globalization
Imports System.Linq
Imports System.Data.SqlClient
Imports System.Data.Linq
Imports System.Xml.Linq
Imports System.Net
Imports System.Net.Sockets
Imports System.Text
Imports System.Threading
Imports System.IO


Public Class frmSocketListener

    Dim tmrTaskInterval As Integer


    Private Sub frmTasks_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        GroupControl1.Text = Me.Text
        BarStaticItemStatus.Caption = "Not running"
        bbiPortToListen.EditValue = 11599

        bbiIPListen.EditValue = "188.165.25.51"
        ' bbiIPListen.EditValue = "127.0.0.1"
    End Sub


    Private Sub frmGoomenaURLChecker_Shown(sender As Object, e As System.EventArgs) Handles Me.Shown
        tmrTask_Tick(tmrTask, Nothing)
    End Sub

    Private Sub frmTasks_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        OnClose()
    End Sub



    Private Sub frmTasks_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        OnClose()
    End Sub


    Private Sub OnClose()
        Application.Exit()
    End Sub



    Private Sub bw_DoWork(sender As System.Object, e As System.ComponentModel.DoWorkEventArgs) Handles bw.DoWork
        Try
            Dim listener As New AsynchronousSocketListener()
            AddHandler listener.WriteUserMessage, AddressOf listener_WriteUserMessage

            Dim continueDate As DateTime = DateTime.UtcNow.AddMinutes(-1)
            Dim port As Integer = bbiPortToListen.EditValue
            Dim ip As IPAddress = IPAddress.Loopback
            Dim ipHostInfo As IPHostEntry = Nothing
            IPAddress.TryParse(bbiIPListen.EditValue, ip)

            listener.Start(ip, port)
        Catch ex As Exception
            bw.ReportProgress(0, ex.Message)
        Finally

        End Try
    End Sub

    Private Sub AddMessage(message As String)
        If (message IsNot Nothing) Then
            Try
                If Me.InvokeRequired Then
                    ' calls itself, but on correct thread
                    Me.BeginInvoke(New Action(Of Object)(AddressOf txtOutput.MaskBox.AppendText), New Object() {"[" & Date.Now.ToLocalTime() & "] " & message & vbCrLf})
                Else
                    txtOutput.MaskBox.AppendText("[" & Date.Now.ToLocalTime() & "] " & message & vbCrLf)
                End If
            Catch ex As InvalidOperationException
            End Try
        End If
    End Sub


    Private Sub bw_RunWorkerCompleted(sender As System.Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bw.RunWorkerCompleted
        If e.Cancelled = True Then

            Me.BarStaticItemStatus.Caption = "Canceled"
            tmrTask.Enabled = False

        ElseIf e.Error IsNot Nothing Then
            Me.BarStaticItemStatus.Caption = "Error: " & e.Error.Message
        Else
            Me.BarStaticItemStatus.Caption = "Not running"
        End If
    End Sub



    Private Sub BarButtonItemClear_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemClear.ItemClick
        txtOutput.Text = ""
    End Sub


    Private Sub bw_ProgressChanged(sender As System.Object, e As System.ComponentModel.ProgressChangedEventArgs) Handles bw.ProgressChanged
        If (e.ProgressPercentage = -1) Then
            Me.BarStaticItemStatus.Caption = Mid(CStr(e.UserState), 1, 200) & "..."
        Else
            AddMessage(CStr(e.UserState))
        End If
    End Sub


    Private Sub listener_WriteUserMessage(sender As System.Object, e As SocketListenerMessageEventArgs)
        AddMessage(CStr(e.Message))
    End Sub


    Private Sub tmrTask_Tick(sender As System.Object, e As System.EventArgs) Handles tmrTask.Tick
        BarStaticItemStatus.Caption = "Starting"
        bw.RunWorkerAsync()
        BarStaticItemStatus.Caption = "Running"
        'BarButtonItemRun.Enabled = False
    End Sub

    Private Sub bbiForceRestart_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiForceRestart.ItemClick
        If (MsgBox("IIS server will restart. Continue?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes) Then
            AsynchronousSocketListener.RestartIIS()
        End If
    End Sub



End Class


' State object for reading client data asynchronously

Public Class StateObject
    ' Client  socket.
    Public workSocket As Socket = Nothing
    ' Size of receive buffer.
    Public Const BufferSize As Integer = 1024
    ' Receive buffer.
    Public buffer(BufferSize) As Byte
    ' Received data string.
    Public sb As New StringBuilder
End Class 'StateObject

Public Class SocketListenerMessageEventArgs
    Inherits EventArgs

    Public Property Message As String

    Public Sub New()
    End Sub

End Class

Public Class AsynchronousSocketListener
    ' Thread signal.
    Public allDone As New ManualResetEvent(False)
    Public Event WriteUserMessage(sender As Object, e As SocketListenerMessageEventArgs)

    Private AuthString As String = "D4FA901B3F51BEE64FD509560985D92952D4AE176E45B679E3013F6738D636F029B0E29820119BB5D50CFE9FEA363207FE6E827CC802F78AC7C766F485C1C2C7"
    Private CommandSeparator As String = "---"

    ' This server waits for a connection and then uses  asychronous operations to
    ' accept the connection, get data from the connected client, 
    ' echo that data back to the connected client.
    ' It then disconnects from the client and waits for another client. 
    Public Sub Start(ip As IPAddress, portToListen As Integer)
        ' Data buffer for incoming data.
        Dim bytes() As Byte = New [Byte](1023) {}


        ' Establish the local endpoint for the socket.
        'Dim ipHostInfo As IPHostEntry = Dns.GetHostEntry(Dns.GetHostName()) 'Dns.GetHostEntry(IPAddress.Any.ToString()) '
        'Dim ip As IPAddress = ipHostInfo.AddressList(0)
        Dim localEndPoint As New IPEndPoint(ip, portToListen)

        ' Create a TCP/IP socket.
        Dim listener As New Socket(ip.AddressFamily, SocketType.Stream, ProtocolType.Tcp)

        ' Bind the socket to the local endpoint and listen for incoming connections.
        listener.Bind(localEndPoint)
        listener.Listen(100)

        SendUserMessage("Bound to " & ip.ToString() & ":" & portToListen.ToString() & "")

        While True
            ' Set the event to nonsignaled state.
            allDone.Reset()

            ' Start an asynchronous socket to listen for connections.
            SendUserMessage("Waiting for a connection...")
            listener.BeginAccept(New AsyncCallback(AddressOf AcceptCallback), listener)

            ' Wait until a connection is made and processed before continuing.
            allDone.WaitOne()
        End While
    End Sub 'Main

    Private Sub SendUserMessage(msg As String)
        If (msg IsNot Nothing AndAlso msg.Length > 0) Then
            Dim e As New SocketListenerMessageEventArgs()
            e.Message = msg
            RaiseEvent WriteUserMessage(Me, e)
        End If
    End Sub

    Public Sub AcceptCallback(ByVal ar As IAsyncResult)
        ' Get the socket that handles the client request.
        Dim listener As Socket = CType(ar.AsyncState, Socket)
        ' End the operation.
        Dim handler As Socket = listener.EndAccept(ar)

        ' Create the state object for the async receive.
        Dim state As New StateObject
        state.workSocket = handler
        handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0, New AsyncCallback(AddressOf ReadCallback), state)
    End Sub 'AcceptCallback


    Public Sub ReadCallback(ByVal ar As IAsyncResult)
        Dim content As String = String.Empty

        ' Retrieve the state object and the handler socket
        ' from the asynchronous state object.
        Dim state As StateObject = CType(ar.AsyncState, StateObject)
        Dim handler As Socket = state.workSocket

        ' Read data from the client socket. 
        Dim bytesRead As Integer = handler.EndReceive(ar)

        If bytesRead > 0 Then
            ' There  might be more data, so store the data received so far.
            state.sb.Append(Encoding.ASCII.GetString(state.buffer, 0, bytesRead))

            ' Check for end-of-file tag. If it is not there, read 
            ' more data.
            content = state.sb.ToString()
            If content.IndexOf("<EOF>") > -1 Then
                ' All the data has been read from the 
                ' client. Display it on the console.
                Dim contentHlpr As String = content.Replace(AuthString, "")
                If (contentHlpr.Length < content.Length) Then contentHlpr = "[CONTENT REMOVED] " & contentHlpr
                SendUserMessage(String.Format("Read {0} bytes from socket. " + vbLf + " Data : {1}" + vbLf, content.Length, contentHlpr))

                Dim resultEnm As processRequestResultEnum = processRequest(handler, content)
                If (resultEnm = processRequestResultEnum.None) Then
                    ' Echo the data back to the client.
                    Send(handler, content)
                End If


            Else
                ' Not all data received. Get more.
                handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0, New AsyncCallback(AddressOf ReadCallback), state)
            End If
        End If
    End Sub 'ReadCallback

    Private Sub Send(ByVal handler As Socket, ByVal data As String)
        ' Convert the string data to byte data using ASCII encoding.
        Dim byteData As Byte() = Encoding.ASCII.GetBytes(data)

        ' Begin sending the data to the remote device.
        handler.BeginSend(byteData, 0, byteData.Length, 0, New AsyncCallback(AddressOf SendCallback), handler)
    End Sub 'Send


    Private Sub SendCallback(ByVal ar As IAsyncResult)
        ' Retrieve the socket from the state object.
        Dim handler As Socket = CType(ar.AsyncState, Socket)

        ' Complete sending the data to the remote device.
        Dim bytesSent As Integer = handler.EndSend(ar)
        SendUserMessage(String.Format("Sent {0} bytes to client.", bytesSent))
        ' SendUserMessage(String.Format("Read {0} bytes from socket. " + vbLf + " Data : {1}" + vbLf, bytesSent, contentHlpr))

        handler.Shutdown(SocketShutdown.Both)
        handler.Close()
        ' Signal the main thread to continue.
        allDone.Set()
    End Sub 'SendCallback


    Private Function processRequest(ByRef handler As Socket, requestContent As String) As processRequestResultEnum
        Dim resultEnm As processRequestResultEnum = processRequestResultEnum.None
        Dim result As String = ""

        If (Not String.IsNullOrEmpty(requestContent)) Then
            result = "IIS reset started"
            Try
                Dim loopC As Integer = 5
                requestContent = requestContent.Replace("<EOF>", "")
                Dim action As New List(Of String)() '= requestContent.Split(Me.CommandSeparator, StringSplitOptions.RemoveEmptyEntries)
                While (requestContent.IndexOf(Me.CommandSeparator) > -1) AndAlso loopC >= 0
                    If (requestContent.IndexOf(Me.CommandSeparator) = 0) Then
                        requestContent = requestContent.Remove(0, Me.CommandSeparator.Length)
                    End If
                    If (requestContent.IndexOf(Me.CommandSeparator) > -1) Then
                        Dim str As String = requestContent.Substring(0, requestContent.IndexOf(Me.CommandSeparator))
                        action.Add(str)
                        requestContent = requestContent.Remove(0, requestContent.IndexOf(Me.CommandSeparator))
                    Else
                        action.Add(requestContent)
                    End If
                    loopC = loopC - 1
                End While


                If (action(0) = AuthString) Then
                    '' user authenticated
                    If (action(1) = "iis-reset") Then
                        
                        RestartIIS()

                        resultEnm = processRequestResultEnum.Complete
                        result = result & vbCrLf & "IIS reset complete"

                    End If
                End If
            Catch ex As Exception
                SendUserMessage(ex.ToString())
                result = "IIS reset failed to start" & vbCrLf & ex.Message
                resultEnm = processRequestResultEnum.Failed
            End Try
        End If

        If (resultEnm <> processRequestResultEnum.None) Then
            result = result & "<EOF>"
            Send(handler, result)
        End If
        Return resultEnm
    End Function





    Public Shared Sub RestartIIS()
        Dim myProcess As New Process()
        myProcess.StartInfo.FileName = "cmd.exe"
        myProcess.StartInfo.UseShellExecute = False
        myProcess.StartInfo.RedirectStandardInput = True
        myProcess.StartInfo.RedirectStandardOutput = True
        myProcess.Start()
        Dim myStreamWriter As StreamWriter = myProcess.StandardInput
        Dim mystreamreader As StreamReader = myProcess.StandardOutput
        myStreamWriter.WriteLine("iisreset /restart" & Convert.ToChar(13))
        myStreamWriter.Close()
        myProcess.Close()

    End Sub



    Private Enum processRequestResultEnum
        None = 0
        Complete = 1
        Failed = 2
    End Enum
End Class 'AsynchronousSocketListener



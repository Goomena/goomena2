﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmConfig
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.txtURL = New DevExpress.XtraEditors.TextEdit()
        Me.txCommandURL = New DevExpress.XtraEditors.TextEdit()
        Me.txNotifyEmail = New DevExpress.XtraEditors.TextEdit()
        Me.dteInterval = New DevExpress.XtraEditors.DateEdit()
        Me.btnOK = New DevExpress.XtraEditors.SimpleButton()
        Me.btnCancel = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.spinTimeout = New DevExpress.XtraEditors.SpinEdit()
        Me.spinAllowedTimeouts = New DevExpress.XtraEditors.SpinEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        CType(Me.txtURL.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txCommandURL.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txNotifyEmail.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteInterval.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteInterval.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.spinTimeout.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.spinAllowedTimeouts.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(67, 13)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(55, 13)
        Me.LabelControl1.TabIndex = 0
        Me.LabelControl1.Text = "Check URL:"
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(55, 39)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(67, 13)
        Me.LabelControl2.TabIndex = 0
        Me.LabelControl2.Text = "Time Interval:"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(49, 65)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(73, 13)
        Me.LabelControl3.TabIndex = 0
        Me.LabelControl3.Text = "Command URL:"
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(62, 91)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(60, 13)
        Me.LabelControl4.TabIndex = 0
        Me.LabelControl4.Text = "Notify email:"
        '
        'txtURL
        '
        Me.txtURL.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtURL.Location = New System.Drawing.Point(130, 10)
        Me.txtURL.Name = "txtURL"
        Me.txtURL.Size = New System.Drawing.Size(299, 20)
        Me.txtURL.TabIndex = 1
        '
        'txCommandURL
        '
        Me.txCommandURL.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txCommandURL.Location = New System.Drawing.Point(130, 62)
        Me.txCommandURL.Name = "txCommandURL"
        Me.txCommandURL.Size = New System.Drawing.Size(299, 20)
        Me.txCommandURL.TabIndex = 1
        '
        'txNotifyEmail
        '
        Me.txNotifyEmail.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txNotifyEmail.Location = New System.Drawing.Point(130, 88)
        Me.txNotifyEmail.Name = "txNotifyEmail"
        Me.txNotifyEmail.Size = New System.Drawing.Size(299, 20)
        Me.txNotifyEmail.TabIndex = 1
        '
        'dteInterval
        '
        Me.dteInterval.EditValue = Nothing
        Me.dteInterval.Location = New System.Drawing.Point(130, 36)
        Me.dteInterval.Name = "dteInterval"
        Me.dteInterval.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteInterval.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dteInterval.Properties.CalendarTimeProperties.CloseUpKey = New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4)
        Me.dteInterval.Properties.CalendarTimeProperties.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.[Default]
        Me.dteInterval.Properties.DisplayFormat.FormatString = "HH:mm:ss"
        Me.dteInterval.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteInterval.Properties.EditFormat.FormatString = "HH:mm:ss"
        Me.dteInterval.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dteInterval.Properties.Mask.EditMask = "HH:mm:ss"
        Me.dteInterval.Size = New System.Drawing.Size(156, 20)
        Me.dteInterval.TabIndex = 2
        '
        'btnOK
        '
        Me.btnOK.Location = New System.Drawing.Point(130, 227)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(75, 23)
        Me.btnOK.TabIndex = 3
        Me.btnOK.Text = "OK"
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(211, 227)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 3
        Me.btnCancel.Text = "Cancel"
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(12, 117)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(110, 13)
        Me.LabelControl5.TabIndex = 0
        Me.LabelControl5.Text = "Request timeout (sec):"
        '
        'spinTimeout
        '
        Me.spinTimeout.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.spinTimeout.EditValue = New Decimal(New Integer() {10, 0, 0, 0})
        Me.spinTimeout.Location = New System.Drawing.Point(130, 114)
        Me.spinTimeout.Name = "spinTimeout"
        Me.spinTimeout.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.spinTimeout.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.[Default]
        Me.spinTimeout.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None
        Me.spinTimeout.Properties.MaxValue = New Decimal(New Integer() {1000, 0, 0, 0})
        Me.spinTimeout.Properties.MinValue = New Decimal(New Integer() {10, 0, 0, 0})
        Me.spinTimeout.Size = New System.Drawing.Size(96, 20)
        Me.spinTimeout.TabIndex = 1
        '
        'spinAllowedTimeouts
        '
        Me.spinAllowedTimeouts.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.spinAllowedTimeouts.EditValue = New Decimal(New Integer() {10, 0, 0, 0})
        Me.spinAllowedTimeouts.Location = New System.Drawing.Point(130, 149)
        Me.spinAllowedTimeouts.Name = "spinAllowedTimeouts"
        Me.spinAllowedTimeouts.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.spinAllowedTimeouts.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.[Default]
        Me.spinAllowedTimeouts.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None
        Me.spinAllowedTimeouts.Properties.MaxValue = New Decimal(New Integer() {1000, 0, 0, 0})
        Me.spinAllowedTimeouts.Properties.MinValue = New Decimal(New Integer() {10, 0, 0, 0})
        Me.spinAllowedTimeouts.Size = New System.Drawing.Size(96, 20)
        Me.spinAllowedTimeouts.TabIndex = 1
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(20, 143)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(102, 26)
        Me.LabelControl6.TabIndex = 0
        Me.LabelControl6.Text = "Allow Timeouts " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "count before restart:"
        '
        'frmConfig
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(441, 262)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.dteInterval)
        Me.Controls.Add(Me.txNotifyEmail)
        Me.Controls.Add(Me.txCommandURL)
        Me.Controls.Add(Me.LabelControl6)
        Me.Controls.Add(Me.LabelControl5)
        Me.Controls.Add(Me.txtURL)
        Me.Controls.Add(Me.LabelControl4)
        Me.Controls.Add(Me.LabelControl3)
        Me.Controls.Add(Me.LabelControl2)
        Me.Controls.Add(Me.LabelControl1)
        Me.Controls.Add(Me.spinAllowedTimeouts)
        Me.Controls.Add(Me.spinTimeout)
        Me.Name = "frmConfig"
        Me.Text = "Config"
        CType(Me.txtURL.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txCommandURL.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txNotifyEmail.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteInterval.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteInterval.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.spinTimeout.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.spinAllowedTimeouts.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtURL As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txCommandURL As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txNotifyEmail As DevExpress.XtraEditors.TextEdit
    Friend WithEvents dteInterval As DevExpress.XtraEditors.DateEdit
    Friend WithEvents btnOK As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents spinTimeout As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents spinAllowedTimeouts As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
End Class

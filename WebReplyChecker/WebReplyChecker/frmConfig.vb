﻿Public Class frmConfig

    Public Property CheckURL As String
    Public Property CheckInterval As DateTime
    Public Property CommandURL As String
    Public Property NotifyEmailAddress As String
    Public Property RequestTimeoutSeconds As Integer
    Public Property AllowedTimeoutsCount As Integer



    Private Sub frmConfig_Shown(sender As Object, e As System.EventArgs) Handles Me.Shown
        If (CheckURL Is Nothing) Then
            ' init control values 
            txtURL.Text = "http://www.goomena.com/Starter.aspx"
            dteInterval.DateTime = New DateTime(1, 1, 1, 0, 3, 0)
            txCommandURL.Text = "http://188.165.25.51:11599"
            txNotifyEmail.Text = "info@goomena.com"
            spinTimeout.Value = 15
            spinAllowedTimeouts.Value = 5
        End If
    End Sub

    Private Sub btnOK_Click(sender As System.Object, e As System.EventArgs) Handles btnOK.Click
        CheckURL = txtURL.Text
        CheckInterval = dteInterval.DateTime
        CommandURL = txCommandURL.Text
        NotifyEmailAddress = txNotifyEmail.Text
        RequestTimeoutSeconds = spinTimeout.Value
        AllowedTimeoutsCount = spinAllowedTimeouts.Value
        Me.Close()
    End Sub

    Private Sub btnCancel_Click(sender As System.Object, e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

End Class
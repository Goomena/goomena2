﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmGoomenaWebChecker
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.txtOutput = New DevExpress.XtraEditors.MemoEdit()
        Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl()
        Me.BarManager1 = New DevExpress.XtraBars.BarManager(Me.components)
        Me.Bar1 = New DevExpress.XtraBars.Bar()
        Me.BarButtonItemRun = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItemCancelJob = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItemClear = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiForceRestart = New DevExpress.XtraBars.BarButtonItem()
        Me.bbiConfig = New DevExpress.XtraBars.BarButtonItem()
        Me.Bar2 = New DevExpress.XtraBars.Bar()
        Me.BarStaticItemStatus = New DevExpress.XtraBars.BarStaticItem()
        Me.bw = New System.ComponentModel.BackgroundWorker()
        Me.tmrTask = New System.Windows.Forms.Timer(Me.components)
        Me.bbiTestSocket = New DevExpress.XtraBars.BarButtonItem()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.txtOutput.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupControl1
        '
        Me.GroupControl1.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 15.75!, System.Drawing.FontStyle.Bold)
        Me.GroupControl1.AppearanceCaption.Options.UseFont = True
        Me.GroupControl1.Controls.Add(Me.txtOutput)
        Me.GroupControl1.Controls.Add(Me.barDockControlLeft)
        Me.GroupControl1.Controls.Add(Me.barDockControlRight)
        Me.GroupControl1.Controls.Add(Me.barDockControlBottom)
        Me.GroupControl1.Controls.Add(Me.barDockControlTop)
        Me.GroupControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupControl1.Location = New System.Drawing.Point(0, 0)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(700, 521)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "Check Goomena web response"
        '
        'txtOutput
        '
        Me.txtOutput.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtOutput.Location = New System.Drawing.Point(5, 61)
        Me.txtOutput.Name = "txtOutput"
        Me.txtOutput.Size = New System.Drawing.Size(690, 434)
        Me.txtOutput.TabIndex = 0
        '
        'barDockControlLeft
        '
        Me.barDockControlLeft.CausesValidation = False
        Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
        Me.barDockControlLeft.Location = New System.Drawing.Point(2, 55)
        Me.barDockControlLeft.Size = New System.Drawing.Size(0, 439)
        '
        'barDockControlRight
        '
        Me.barDockControlRight.CausesValidation = False
        Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
        Me.barDockControlRight.Location = New System.Drawing.Point(698, 55)
        Me.barDockControlRight.Size = New System.Drawing.Size(0, 439)
        '
        'barDockControlBottom
        '
        Me.barDockControlBottom.CausesValidation = False
        Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.barDockControlBottom.Location = New System.Drawing.Point(2, 494)
        Me.barDockControlBottom.Size = New System.Drawing.Size(696, 25)
        '
        'barDockControlTop
        '
        Me.barDockControlTop.CausesValidation = False
        Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.barDockControlTop.Location = New System.Drawing.Point(2, 33)
        Me.barDockControlTop.Size = New System.Drawing.Size(696, 22)
        '
        'BarManager1
        '
        Me.BarManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar1, Me.Bar2})
        Me.BarManager1.DockControls.Add(Me.barDockControlTop)
        Me.BarManager1.DockControls.Add(Me.barDockControlBottom)
        Me.BarManager1.DockControls.Add(Me.barDockControlLeft)
        Me.BarManager1.DockControls.Add(Me.barDockControlRight)
        Me.BarManager1.Form = Me.GroupControl1
        Me.BarManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.BarButtonItemClear, Me.BarStaticItemStatus, Me.BarButtonItemRun, Me.BarButtonItemCancelJob, Me.bbiConfig, Me.bbiForceRestart, Me.bbiTestSocket})
        Me.BarManager1.MainMenu = Me.Bar1
        Me.BarManager1.MaxItemId = 8
        Me.BarManager1.StatusBar = Me.Bar2
        '
        'Bar1
        '
        Me.Bar1.BarName = "Custom 3"
        Me.Bar1.DockCol = 0
        Me.Bar1.DockRow = 0
        Me.Bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.Bar1.FloatLocation = New System.Drawing.Point(480, 227)
        Me.Bar1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemRun), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemCancelJob, True), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemClear, True), New DevExpress.XtraBars.LinkPersistInfo(Me.bbiForceRestart, True), New DevExpress.XtraBars.LinkPersistInfo(Me.bbiTestSocket, True), New DevExpress.XtraBars.LinkPersistInfo(Me.bbiConfig, True)})
        Me.Bar1.OptionsBar.MultiLine = True
        Me.Bar1.OptionsBar.UseWholeRow = True
        Me.Bar1.Text = "Custom 3"
        '
        'BarButtonItemRun
        '
        Me.BarButtonItemRun.Caption = "Run"
        Me.BarButtonItemRun.Id = 2
        Me.BarButtonItemRun.Name = "BarButtonItemRun"
        '
        'BarButtonItemCancelJob
        '
        Me.BarButtonItemCancelJob.Caption = "Cancel job"
        Me.BarButtonItemCancelJob.Id = 3
        Me.BarButtonItemCancelJob.Name = "BarButtonItemCancelJob"
        '
        'BarButtonItemClear
        '
        Me.BarButtonItemClear.Caption = "Clear"
        Me.BarButtonItemClear.Id = 0
        Me.BarButtonItemClear.Name = "BarButtonItemClear"
        '
        'bbiForceRestart
        '
        Me.bbiForceRestart.Caption = "Force Restart"
        Me.bbiForceRestart.Id = 5
        Me.bbiForceRestart.Name = "bbiForceRestart"
        '
        'bbiConfig
        '
        Me.bbiConfig.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right
        Me.bbiConfig.Caption = "Config"
        Me.bbiConfig.Id = 4
        Me.bbiConfig.Name = "bbiConfig"
        '
        'Bar2
        '
        Me.Bar2.BarName = "Custom 4"
        Me.Bar2.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom
        Me.Bar2.DockCol = 0
        Me.Bar2.DockRow = 0
        Me.Bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom
        Me.Bar2.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarStaticItemStatus)})
        Me.Bar2.OptionsBar.AllowQuickCustomization = False
        Me.Bar2.OptionsBar.DrawDragBorder = False
        Me.Bar2.OptionsBar.UseWholeRow = True
        Me.Bar2.Text = "Custom 4"
        '
        'BarStaticItemStatus
        '
        Me.BarStaticItemStatus.Caption = "[STATUS]"
        Me.BarStaticItemStatus.Id = 1
        Me.BarStaticItemStatus.Name = "BarStaticItemStatus"
        Me.BarStaticItemStatus.TextAlignment = System.Drawing.StringAlignment.Near
        '
        'bw
        '
        Me.bw.WorkerReportsProgress = True
        Me.bw.WorkerSupportsCancellation = True
        '
        'tmrTask
        '
        Me.tmrTask.Interval = 300000
        '
        'bbiTestSocket
        '
        Me.bbiTestSocket.Caption = "Test Socket"
        Me.bbiTestSocket.Id = 7
        Me.bbiTestSocket.Name = "bbiTestSocket"
        '
        'frmGoomenaWebChecker
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(700, 521)
        Me.Controls.Add(Me.GroupControl1)
        Me.Name = "frmGoomenaWebChecker"
        Me.Text = "Goomena Web Checker"
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        CType(Me.txtOutput.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtOutput As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
    Friend WithEvents BarManager1 As DevExpress.XtraBars.BarManager
    Friend WithEvents Bar1 As DevExpress.XtraBars.Bar
    Friend WithEvents BarButtonItemClear As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bw As System.ComponentModel.BackgroundWorker
    Friend WithEvents tmrTask As System.Windows.Forms.Timer
    Friend WithEvents Bar2 As DevExpress.XtraBars.Bar
    Friend WithEvents BarStaticItemStatus As DevExpress.XtraBars.BarStaticItem
    Friend WithEvents BarButtonItemRun As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItemCancelJob As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiConfig As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiForceRestart As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bbiTestSocket As DevExpress.XtraBars.BarButtonItem
End Class

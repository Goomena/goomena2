﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmStartup
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnStartChecker = New DevExpress.XtraEditors.SimpleButton()
        Me.btnStartListner = New DevExpress.XtraEditors.SimpleButton()
        Me.SuspendLayout()
        '
        'btnStartChecker
        '
        Me.btnStartChecker.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.btnStartChecker.Appearance.Options.UseFont = True
        Me.btnStartChecker.Image = Global.WebReplyChecker.My.Resources.Resources.servermonitor
        Me.btnStartChecker.Location = New System.Drawing.Point(91, 33)
        Me.btnStartChecker.Name = "btnStartChecker"
        Me.btnStartChecker.Size = New System.Drawing.Size(259, 145)
        Me.btnStartChecker.TabIndex = 0
        Me.btnStartChecker.Text = "Start Server " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Check"
        '
        'btnStartListner
        '
        Me.btnStartListner.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.btnStartListner.Appearance.Options.UseFont = True
        Me.btnStartListner.Image = Global.WebReplyChecker.My.Resources.Resources.satellite
        Me.btnStartListner.Location = New System.Drawing.Point(91, 196)
        Me.btnStartListner.Name = "btnStartListner"
        Me.btnStartListner.Size = New System.Drawing.Size(259, 141)
        Me.btnStartListner.TabIndex = 0
        Me.btnStartListner.Text = "Start Socket " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Listener"
        '
        'frmStartup
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(441, 370)
        Me.Controls.Add(Me.btnStartChecker)
        Me.Controls.Add(Me.btnStartListner)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmStartup"
        Me.Text = "Server check - Startup"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnStartListner As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnStartChecker As DevExpress.XtraEditors.SimpleButton
End Class

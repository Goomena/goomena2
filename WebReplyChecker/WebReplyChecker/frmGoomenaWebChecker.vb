﻿
Imports System.Xml
Imports System.Globalization
Imports System.Linq
Imports System.Data.SqlClient
Imports System.Data.Linq
Imports System.Xml.Linq
Imports System.Net
Imports System.Net.Sockets
Imports System.Text

Public Class frmGoomenaWebChecker

    Dim tmrTaskInterval As Integer
    Private TimeoutsCount As Integer
    Private ConfigForm As frmConfig

    Private Property CheckURL As String
    Private Property CheckInterval As DateTime
    Private Property CommandURL As String
    Private Property NotifyEmailAddress As String
    Private Property RequestTimeoutSeconds As Integer
    Private Property AllowedTimeoutsCount As Integer

    Private Sub frmTasks_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        GroupControl1.Text = Me.Text
        BarStaticItemStatus.Caption = "Not running"
        ConfigForm = New frmConfig()
    End Sub


    Private Sub frmGoomenaURLChecker_Shown(sender As Object, e As System.EventArgs) Handles Me.Shown

        If (CheckURL Is Nothing) Then
            ShowConfig()
        End If

        StartTimer()
    End Sub

    Private Sub frmTasks_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        OnClose()
    End Sub



    Private Sub frmTasks_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        OnClose()
    End Sub


    Private Sub OnClose()
        Try
            If (bw IsNot Nothing) Then
                bw.CancelAsync()
                bw.Dispose()
                bw = Nothing
            End If
            tmrTask.Enabled = False
        Catch ex As Exception
        End Try
        Application.Exit()
    End Sub

    Private Sub ShowConfig()
        ConfigForm.ShowDialog()
        CheckURL = ConfigForm.CheckURL
        CheckInterval = ConfigForm.CheckInterval
        CommandURL = ConfigForm.CommandURL
        NotifyEmailAddress = ConfigForm.NotifyEmailAddress
        RequestTimeoutSeconds = ConfigForm.RequestTimeoutSeconds
        AllowedTimeoutsCount = ConfigForm.AllowedTimeoutsCount
    End Sub



    Private Sub bw_DoWork(sender As System.Object, e As System.ComponentModel.DoWorkEventArgs) Handles bw.DoWork
        Try

            Dim continueDate As DateTime = DateTime.UtcNow.AddMinutes(-1)
            While True
                While (continueDate > DateTime.UtcNow)
                    System.Threading.Thread.Sleep(200)
                    If bw Is Nothing OrElse bw.CancellationPending = True Then
                        e.Cancel = True
                        Exit Sub
                    End If
                End While

                If bw.CancellationPending = True Then
                    e.Cancel = True
                    Exit Sub
                End If

                Dim response As System.Net.WebResponse = Nothing
                Try
                    bw.ReportProgress(0, " Calling " & Me.CheckURL & "")

                    Dim hwr As HttpWebRequest = DirectCast(System.Net.WebRequest.Create(Me.CheckURL), HttpWebRequest)
                    hwr.Method = "GET"
                    hwr.KeepAlive = True
                    hwr.UserAgent = "Mozilla/4.0 (compatible; Goo web checker)"
                    hwr.Timeout = Me.RequestTimeoutSeconds * 1000

                    response = hwr.GetResponse()
                    Dim sr As New System.IO.StreamReader(response.GetResponseStream())
                    Dim result As String = sr.ReadToEnd()
                    TimeoutsCount = 0

                Catch ex As System.Net.WebException
                    bw.ReportProgress(0, ex.Message)
                    If (ex.Message.Contains("The operation has timed out")) Then
                        TimeoutsCount = TimeoutsCount + 1
                    End If
                Catch ex As Exception
                    bw.ReportProgress(0, ex.Message)
                Finally
                    If (response IsNot Nothing) Then
                        response.Close()
                    End If
                End Try


                If (TimeoutsCount > Me.AllowedTimeoutsCount) Then
                    ' here we have to make server restart
                    SendCommand(SendCommandEnum.IISreset)
                End If

                Dim ts As New TimeSpan(Me.CheckInterval.Ticks)
                Dim interval As Long = ts.TotalSeconds

                bw.ReportProgress(0, " Waiting " & interval & " seconds")
                continueDate = DateTime.UtcNow.AddMilliseconds(interval * 1000)

            End While

        Catch ex As Exception
            bw.ReportProgress(0, ex.Message)
        Finally

        End Try
    End Sub

    Private Sub AddMessage(message As String)
        'txOutput.Text = "[" & Date.Now.ToLocalTime() & "] " & message & vbCrLf & txOutput.Text
        If (message IsNot Nothing) Then
            Try
                If Me.InvokeRequired Then
                    ' calls itself, but on correct thread
                    Me.BeginInvoke(New Action(Of Object)(AddressOf txtOutput.MaskBox.AppendText), New Object() {"[" & Date.Now.ToLocalTime() & "] " & message & vbCrLf})
                Else
                    txtOutput.MaskBox.AppendText("[" & Date.Now.ToLocalTime() & "] " & message & vbCrLf)
                End If
            Catch ex As InvalidOperationException
            End Try
        End If
    End Sub


    Private Sub bw_RunWorkerCompleted(sender As System.Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bw.RunWorkerCompleted
        If e.Cancelled = True Then

            Me.BarStaticItemStatus.Caption = "Canceled"
            tmrTask.Enabled = False

        ElseIf e.Error IsNot Nothing Then
            Me.BarStaticItemStatus.Caption = "Error: " & e.Error.Message
        Else
            Me.BarStaticItemStatus.Caption = "Not running"
        End If
    End Sub


    Private Enum SendCommandEnum
        none = 0
        IISreset = 1
        Test = 2
    End Enum


    Private AuthString As String = "D4FA901B3F51BEE64FD509560985D92952D4AE176E45B679E3013F6738D636F029B0E29820119BB5D50CFE9FEA363207FE6E827CC802F78AC7C766F485C1C2C7"
    Private CommandSeparator As String = "---"

    Private Sub SendCommand(enm As SendCommandEnum)
        Try
            If (enm = SendCommandEnum.none) Then
                AddMessage("Command not set. Returning...")
                Return
            End If

            Dim actionStr As String = "testing connection"
            If (enm = SendCommandEnum.IISreset) Then
                actionStr = "iis-reset"
                AddMessage("Sending iisreset command")
            End If
            If (enm = SendCommandEnum.Test) Then
                actionStr = "testing connection"
                AddMessage("Testing connection")
            End If

            Dim uriCommand As Uri = New Uri(Me.CommandURL)

            ' Data buffer for incoming data.
            Dim bytes(1024) As Byte

            ' Connect to a remote device.

            ' Establish the remote endpoint for the socket.
            ' This example uses port 11000 on the local computer.

            Dim ip As IPAddress = IPAddress.Loopback
            Dim ipHostInfo As IPHostEntry = Nothing
            IPAddress.TryParse(uriCommand.Host, ip)
            If (ip Is Nothing OrElse IPAddress.IsLoopback(ip)) Then
                ipHostInfo = Dns.GetHostEntry(uriCommand.Host)
                ip = ipHostInfo.AddressList(0)
            End If

            Dim remoteEP As New IPEndPoint(ip, uriCommand.Port)

            ' Create a TCP/IP socket.
            Dim sck As New Socket(ip.AddressFamily, SocketType.Stream, ProtocolType.Tcp)

            ' Connect the socket to the remote endpoint.
            sck.Connect(remoteEP)

            AddMessage(String.Format("Socket connected to {0}", sck.RemoteEndPoint.ToString()))

            ' Encode the data string into a byte array.
            Dim msg As Byte() = Encoding.ASCII.GetBytes(AuthString & Me.CommandSeparator & actionStr & "<EOF>")


            ' Send the data through the socket.
            Dim bytesSent As Integer = sck.Send(msg)

            ' Receive the response from the remote device.
            Dim bytesRec As Integer = sck.Receive(bytes)
            'Console.WriteLine("Echoed test = {0}", Encoding.ASCII.GetString(bytes, 0, bytesRec))
            'bw.ReportProgress(0, String.Format("Echoed test = {0}", Encoding.ASCII.GetString(bytes, 0, bytesRec)))
            Dim response As String = Encoding.ASCII.GetString(bytes, 0, bytesRec)
            AddMessage(String.Format("Server response: {0}", response))

            ' Release the socket.
            sck.Shutdown(SocketShutdown.Both)
            sck.Close()

        Catch ex As Exception
            AddMessage(Me.Name & " -> SendCommand" & vbCrLf & ex.Message)
        End Try
    End Sub


    Private Sub BarButtonItemClear_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemClear.ItemClick
        txtOutput.Text = ""
    End Sub

    Private Sub BarButtonItemRun_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemRun.ItemClick
        Try
            tmrTask.Enabled = False

            If Not bw.IsBusy = True Then
                BarStaticItemStatus.Caption = "Running"
                bw.RunWorkerAsync()
                BarButtonItemRun.Enabled = False
            Else
                Try
                    bw.CancelAsync()
                    bw.RunWorkerAsync()
                    BarStaticItemStatus.Caption = "Running"
                Catch ex As Exception
                    AddMessage(Me.Name & " -> BarButtonItemRun_ItemClick -> bw.IsBusy" & vbCrLf & ex.Message)
                End Try
            End If

        Catch ex As Exception
            AddMessage(Me.Name & " -> BarButtonItemRun_ItemClick" & vbCrLf & ex.Message)
        Finally
            tmrTask.Enabled = True
            tmrTask.Interval = tmrTaskInterval
        End Try
    End Sub


    Private Sub BarButtonItemCancelJob_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemCancelJob.ItemClick
        bw.CancelAsync()
        tmrTask.Enabled = False
        BarButtonItemRun.Enabled = True
    End Sub

    Private Sub bw_ProgressChanged(sender As System.Object, e As System.ComponentModel.ProgressChangedEventArgs) Handles bw.ProgressChanged
        If (e.ProgressPercentage = -1) Then
            Me.BarStaticItemStatus.Caption = Mid(CStr(e.UserState), 1, 200) & "..."
        Else
            AddMessage(CStr(e.UserState))
        End If
    End Sub



    Private Sub tmrTask_Tick(sender As System.Object, e As System.EventArgs) Handles tmrTask.Tick
        If Not bw.IsBusy = True Then
            BarStaticItemStatus.Caption = "Running"
            bw.RunWorkerAsync()
            BarButtonItemRun.Enabled = False
        End If
        tmrTask.Interval = tmrTaskInterval
    End Sub


    Private Sub StartTimer()
        Dim r As New Random(System.DateTime.Now.Millisecond)
        Dim interval As Integer = r.Next(30, 60)

        If (tmrTaskInterval = 0) Then tmrTaskInterval = tmrTask.Interval
        tmrTask.Interval = interval * 1000
        tmrTask.Enabled = True

        tmrTask.Enabled = True
        tmrTask.Start()

        BarStaticItemStatus.Caption = "First run in " & tmrTask.Interval / 1000 & " sec"
    End Sub


    Private Sub bbiConfig_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiConfig.ItemClick
        ShowConfig()
    End Sub


    Private Sub bbiForceRestart_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiForceRestart.ItemClick
        If (MsgBox("IIS server will restart. Continue?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes) Then
            SendCommand(SendCommandEnum.IISreset)
        End If
    End Sub

    Private Sub bbiTestSocket_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bbiTestSocket.ItemClick
        SendCommand(SendCommandEnum.Test)
    End Sub
End Class
﻿Imports System.Configuration
Imports System.Text
Imports Dating.Server.Datasets.DLL

Public Class clsMatchingHelper
    Public Shared Function HasFilledMatchingCriteria(ByVal MasterProfileId As Integer) As Boolean
        Dim re As Boolean = False
        Try
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection
                Dim sql As String = <sql><![CDATA[
                SELECT count([MatchingCriteriaId]) as count
                FROM [dbo].[EUS_MatchingCriteriaSearch]
                where profileid=@ProfileId
]]></sql>.Value
                Using command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                    command.Parameters.Add(New SqlClient.SqlParameter("@ProfileId", MasterProfileId))
                    Dim i As Integer = DataHelpers.ExecuteScalar(command)
                    If i > 0 Then
                        re = True
                    End If
                End Using
            End Using
        Catch ex As Exception
            clsMyMail.TrySendMailSupport(ConfigurationManager.AppSettings("ExceptionsEmail"), "On HasFilledMatchingCriteria prID:" & MasterProfileId, ex.ToString())
        End Try
        Return re
    End Function

    Public Shared Function GetMatcingCriteriaReturnFromDataRow(ByVal r As DSLists.EUS_MatchingCriteriaSearchRow) As MatcingCriteriaReturn
        Dim re As New MatcingCriteriaReturn
        If r IsNot Nothing Then
            re.SearchText = If(r.IsSearchTextNull, Nothing, r.SearchText)
            re.ageMinSelectedValue = If(r.IsageMinSelectedValueNull, Nothing, r.ageMinSelectedValue)
            re.ageMaxSelectedValue = If(r.IsageMaxSelectedValueNull, Nothing, r.ageMaxSelectedValue)
            re.HeightMinSelectedValue = If(r.IsHeightMinSelectedValueNull, Nothing, r.HeightMinSelectedValue)
            re.HeightMaxSelectedValue = If(r.IsHeightMaxSelectedValueNull, Nothing, r.HeightMaxSelectedValue)
            re.ddlDistanceSelectedValue = If(r.IsddlDistanceSelectedValueNull, Nothing, r.ddlDistanceSelectedValue)
            re.chkOnlineChecked = If(r.IschkOnlineCheckedNull, False, r.chkOnlineChecked)
            re.chkPhotosChecked = If(r.IschkPhotosCheckedNull, False, r.chkPhotosChecked)
            re.chkPhotosPrivateChecked = If(r.IschkPhotosPrivateCheckedNull, False, r.chkPhotosPrivateChecked)
            re.chkWillingToTravelChecked = If(r.IschkWillingToTravelCheckedNull, False, r.chkWillingToTravelChecked)
            re.SelectedHairColorList = New SearchValuesInfo With {
                                                               .Values = If(r.IsSelectedHairColorListNull, Nothing, r.SelectedHairColorList), _
                                                               .IsAllSelected = If(r.IsSelectedHairColorAllSelectedNull, False, r.SelectedHairColorAllSelected)
                                                                          }
            re.SelectedBreastSizeList = New SearchValuesInfo With {
                                                            .Values = If(r.IsSelectedBreastSizeListNull, Nothing, r.SelectedBreastSizeList), _
                                                            .IsAllSelected = If(r.IsSelectedBreastSizeAllSelectedNull, False, r.SelectedBreastSizeAllSelected)
                                                                       }
            re.SelectedEyeColorList = New SearchValuesInfo With {
                                                          .Values = If(r.IsSelectedEyeColorListNull, Nothing, r.SelectedEyeColorList), _
                                                          .IsAllSelected = If(r.IsSelectedEyeColorAllSelectedNull, False, r.SelectedEyeColorAllSelected)
                                                                     }
            re.SelectedBodyTypeList = New SearchValuesInfo With {
                                                          .Values = If(r.IsSelectedBodyTypeSizeListNull, Nothing, r.SelectedBodyTypeSizeList), _
                                                          .IsAllSelected = If(r.IsSelectedBodyTypeAllSelectedNull, False, r.SelectedBodyTypeAllSelected)
                                                                     }
            re.SelectedRelationShipStatusList = New SearchValuesInfo With {
                                                          .Values = If(r.IsSelectedRelationShipStatusListNull, Nothing, r.SelectedRelationShipStatusList), _
                                                          .IsAllSelected = If(r.IsSelectedRelationShipStatusAllSelectedNull, False, r.SelectedRelationShipStatusAllSelected)
                                                                     }
            re.SelectedTypeOfDatingList = New SearchValuesInfo With {
                                                          .Values = If(r.IsSelectedTypeOfDatingListNull, Nothing, r.SelectedTypeOfDatingList), _
                                                          .IsAllSelected = If(r.IsSelectedTypeOfDatingAllSelectedNull, False, r.SelectedBodyTypeAllSelected)
                                                                     }
            re.SelectedEducationList = New SearchValuesInfo With {
                                                          .Values = If(r.IsSelectedEducationSizeListNull, Nothing, r.SelectedEducationSizeList), _
                                                          .IsAllSelected = If(r.IsSelectedEducationAllSelectedNull, False, r.SelectedEducationAllSelected)
                                                                     }
            re.SelectedChildrenList = New SearchValuesInfo With {
                                                          .Values = If(r.IsSelectedChildrenListNull, Nothing, r.SelectedChildrenList), _
                                                          .IsAllSelected = If(r.IsSelectedChildrenAllSelectedNull, False, r.SelectedChildrenAllSelected)
                                                                     }
            re.SelectedEthnicityList = New SearchValuesInfo With {
                                                  .Values = If(r.IsSelectedEthnicityListNull, Nothing, r.SelectedEthnicityList), _
                                                  .IsAllSelected = If(r.IsSelectedEthnicityAllSelectedNull, False, r.SelectedEthnicityAllSelected)
                                                             }
            re.SelectedSmokingHabitsList = New SearchValuesInfo With {
                                                  .Values = If(r.IsSelectedSmokingHabitsListNull, Nothing, r.SelectedSmokingHabitsList), _
                                                  .IsAllSelected = If(r.IsSelectedSmokingHabitsAllSelectedNull, False, r.SelectedSmokingHabitsAllSelected)
                                                             }
            re.SelectedDrinkingHabitsList = New SearchValuesInfo With {
                                                  .Values = If(r.IsSelectedDrinkingHabitsListListNull, Nothing, r.SelectedDrinkingHabitsListList), _
                                                  .IsAllSelected = If(r.IsSelectedDrinkingHabitsListAllSelectedNull, False, r.SelectedDrinkingHabitsListAllSelected)
                                                             }
            re.SelectedReligionList = New SearchValuesInfo With {
                                                  .Values = If(r.IsSelectedReligionListNull, Nothing, r.SelectedReligionList), _
                                                  .IsAllSelected = If(r.IsSelectedReligionAllSelectedNull, False, r.SelectedReligionAllSelected)
                                              }
            re.SelectedLanguagesList = New SearchValuesInfo With {
                                                              .Values = If(r.IsSelectedSpokenLangsListNull, Nothing, r.SelectedSpokenLangsList), _
                                                              .IsAllSelected = If(r.IsSelectedSpokenLangsAllSelectedNull, False, r.SelectedSpokenLangsAllSelected)
                                                                         }

        End If
        Return re
    End Function

    Public Shared Function GetMatchingProfiles(ByVal MasterProfileId As Integer, ByVal LastRowNum As Integer, Optional ByVal r As DSLists.EUS_MatchingCriteriaSearchRow = Nothing) As DataTable
        Dim dt As DataTable = Nothing
        Try
            If r Is Nothing Then
                Dim re As GetMatchingCriteriaResult = GetMatchingCriteria(MasterProfileId)
                If re.HasErrors = True And re.HasRow = False Then
                    Return dt
                ElseIf re.Row Is Nothing Then
                    Return dt
                Else
                    r = re.Row
                End If
            End If
            dt = FillwithMatching(GetMatcingCriteriaReturnFromDataRow(r), MasterProfileId, 40, LastRowNum)
        Catch ex As Exception
            clsMyMail.TrySendMailSupport(ConfigurationManager.AppSettings("ExceptionsEmail"), "On GetMatchingProfiles prID:" & MasterProfileId, ex.ToString())
        End Try
        Return dt
    End Function
    Public Shared Function GetMatchingCriteria(ByVal _MasterProfileId As Integer) As GetMatchingCriteriaResult
        Dim re As New GetMatchingCriteriaResult
        Try
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection
                Using ds As New DSLists
                    Using ta As New DSListsTableAdapters.EUS_MatchingCriteriaSearchTableAdapter
                        ta.Connection = con
                        ta.FillById(ds.EUS_MatchingCriteriaSearch, _MasterProfileId)
                    End Using
                    If ds.EUS_MatchingCriteriaSearch.Rows.Count > 0 Then

                        re.Row = ds.EUS_MatchingCriteriaSearch.Rows(0)
                        re.HasErrors = False
                        re.HasRow = True
                    Else
                        re.HasErrors = False
                        re.HasRow = False
                    End If
                End Using
            End Using
        Catch ex As Exception
            re.HasErrors = True
            clsMyMail.TrySendMailSupport(ConfigurationManager.AppSettings("ExceptionsEmail"), "On GetMatchingCriteria prID:" & _MasterProfileId, ex.ToString())
        End Try
        Return re
    End Function
    Public Shared Sub InsertOrUpdateMatchingCriteria(ByRef MatchingCriteria As MatcingCriteriaReturn, ByVal MasterProfileId As Integer)
        Try
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection
                Dim sql As String = <sql><![CDATA[
 
EXECUTE [dbo].[EusMatchingCriteriaSearch_InsertOrUpdate] 
   @SearchText
  ,@ProfileId
  ,@ageMinSelectedValue
  ,@ageMaxSelectedValue
  ,@HeightMinSelectedValue
  ,@HeightMaxSelectedValue
  ,@ddlDistanceSelectedValue
  ,@chkOnlineChecked
  ,@chkPhotosChecked
  ,@chkPhotosPrivateChecked
  ,@chkWillingToTravelChecked
  ,@SelectedHairColorList
  ,@SelectedHairColorAllSelected
  ,@SelectedBreastSizeList
  ,@SelectedBreastSizeAllSelected
  ,@SelectedEyeColorList
  ,@SelectedEyeColorAllSelected
  ,@SelectedBodyTypeSizeList
  ,@SelectedBodyTypeAllSelected
  ,@SelectedRelationShipStatusList
  ,@SelectedRelationShipStatusAllSelected
  ,@SelectedTypeOfDatingList
  ,@SelectedTypeOfDatingAllSelected
  ,@SelectedEducationSizeList
  ,@SelectedEducationAllSelected
  ,@SelectedChildrenList
  ,@SelectedChildrenAllSelected
  ,@SelectedEthnicityList
  ,@SelectedEthnicityAllSelected
  ,@SelectedSmokingHabitsList
  ,@SelectedSmokingHabitsAllSelected
  ,@SelectedDrinkingHabitsListList
  ,@SelectedDrinkingHabitsListAllSelected
  ,@SelectedSpokenLangsList
  ,@SelectedSpokenLangsAllSelected
 ,@SelectedReligionList
  ,@SelectedReligionAllSelected
]]></sql>.Value

                Using command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(con, sql)

                    command.Parameters.Add(New SqlClient.SqlParameter("@SearchText", If(MatchingCriteria.SearchText = Nothing, DBNull.Value, MatchingCriteria.SearchText)))
                    command.Parameters.Add(New SqlClient.SqlParameter("@ddlDistanceSelectedValue", MatchingCriteria.ddlDistanceSelectedValue))
                    command.Parameters.Add(New SqlClient.SqlParameter("@ProfileId", MasterProfileId))
                    command.Parameters.Add(New SqlClient.SqlParameter("@ageMinSelectedValue", MatchingCriteria.ageMinSelectedValue))
                    command.Parameters.Add(New SqlClient.SqlParameter("@ageMaxSelectedValue", MatchingCriteria.ageMaxSelectedValue))
                    command.Parameters.Add(New SqlClient.SqlParameter("@HeightMinSelectedValue", MatchingCriteria.HeightMinSelectedValue))
                    command.Parameters.Add(New SqlClient.SqlParameter("@HeightMaxSelectedValue", MatchingCriteria.HeightMaxSelectedValue))
                    command.Parameters.Add(New SqlClient.SqlParameter("@chkOnlineChecked", False))
                    command.Parameters.Add(New SqlClient.SqlParameter("@chkPhotosChecked", False))
                    command.Parameters.Add(New SqlClient.SqlParameter("@chkPhotosPrivateChecked", False))
                    command.Parameters.Add(New SqlClient.SqlParameter("@chkWillingToTravelChecked", MatchingCriteria.chkWillingToTravelChecked))
                    command.Parameters.Add(New SqlClient.SqlParameter("@SelectedHairColorList", MatchingCriteria.SelectedHairColorList.getDbValue()))
                    command.Parameters.Add(New SqlClient.SqlParameter("@SelectedHairColorAllSelected", MatchingCriteria.SelectedHairColorList.IsAllSelected))
                    command.Parameters.Add(New SqlClient.SqlParameter("@SelectedBreastSizeList", MatchingCriteria.SelectedBreastSizeList.getDbValue()))
                    command.Parameters.Add(New SqlClient.SqlParameter("@SelectedBreastSizeAllSelected", MatchingCriteria.SelectedBreastSizeList.IsAllSelected))
                    command.Parameters.Add(New SqlClient.SqlParameter("@SelectedEyeColorList", MatchingCriteria.SelectedEyeColorList.getDbValue()))
                    command.Parameters.Add(New SqlClient.SqlParameter("@SelectedEyeColorAllSelected", MatchingCriteria.SelectedEyeColorList.IsAllSelected))
                    command.Parameters.Add(New SqlClient.SqlParameter("@SelectedBodyTypeSizeList", MatchingCriteria.SelectedBodyTypeList.getDbValue()))
                    command.Parameters.Add(New SqlClient.SqlParameter("@SelectedBodyTypeAllSelected", MatchingCriteria.SelectedBodyTypeList.IsAllSelected))
                    command.Parameters.Add(New SqlClient.SqlParameter("@SelectedRelationShipStatusList", MatchingCriteria.SelectedRelationShipStatusList.getDbValue()))
                    command.Parameters.Add(New SqlClient.SqlParameter("@SelectedRelationShipStatusAllSelected", MatchingCriteria.SelectedRelationShipStatusList.IsAllSelected))
                    command.Parameters.Add(New SqlClient.SqlParameter("@SelectedTypeOfDatingList", MatchingCriteria.SelectedTypeOfDatingList.getDbValue()))
                    command.Parameters.Add(New SqlClient.SqlParameter("@SelectedTypeOfDatingAllSelected", MatchingCriteria.SelectedTypeOfDatingList.IsAllSelected))
                    command.Parameters.Add(New SqlClient.SqlParameter("@SelectedEducationSizeList", MatchingCriteria.SelectedEducationList.getDbValue()))
                    command.Parameters.Add(New SqlClient.SqlParameter("@SelectedEducationAllSelected", MatchingCriteria.SelectedEducationList.IsAllSelected))
                    command.Parameters.Add(New SqlClient.SqlParameter("@SelectedChildrenList", MatchingCriteria.SelectedChildrenList.getDbValue()))
                    command.Parameters.Add(New SqlClient.SqlParameter("@SelectedChildrenAllSelected", MatchingCriteria.SelectedChildrenList.IsAllSelected))
                    command.Parameters.Add(New SqlClient.SqlParameter("@SelectedEthnicityList", MatchingCriteria.SelectedEthnicityList.getDbValue()))
                    command.Parameters.Add(New SqlClient.SqlParameter("@SelectedEthnicityAllSelected", MatchingCriteria.SelectedEthnicityList.IsAllSelected))
                    command.Parameters.Add(New SqlClient.SqlParameter("@SelectedSmokingHabitsList", MatchingCriteria.SelectedSmokingHabitsList.getDbValue()))
                    command.Parameters.Add(New SqlClient.SqlParameter("@SelectedSmokingHabitsAllSelected", MatchingCriteria.SelectedSmokingHabitsList.IsAllSelected))
                    command.Parameters.Add(New SqlClient.SqlParameter("@SelectedDrinkingHabitsListList", MatchingCriteria.SelectedDrinkingHabitsList.getDbValue()))
                    command.Parameters.Add(New SqlClient.SqlParameter("@SelectedDrinkingHabitsListAllSelected", MatchingCriteria.SelectedDrinkingHabitsList.IsAllSelected))
                    command.Parameters.Add(New SqlClient.SqlParameter("@SelectedSpokenLangsList", MatchingCriteria.SelectedLanguagesList.getDbValue()))
                    command.Parameters.Add(New SqlClient.SqlParameter("@SelectedSpokenLangsAllSelected", MatchingCriteria.SelectedLanguagesList.IsAllSelected))
                    command.Parameters.Add(New SqlClient.SqlParameter("@SelectedReligionList", MatchingCriteria.SelectedReligionList.getDbValue()))
                    command.Parameters.Add(New SqlClient.SqlParameter("@SelectedReligionAllSelected", MatchingCriteria.SelectedReligionList.IsAllSelected))
                    DataHelpers.ExecuteNonQuery(command)

                End Using
            End Using
        Catch ex As Exception
            clsMyMail.TrySendMailSupport(ConfigurationManager.AppSettings("ExceptionsEmail"), "On clsMatchingHelper.InsertOrUpdateMatchingCriteria prID:" & MasterProfileId, ex.ToString())
        End Try
    End Sub
    Public Shared Function FillwithMatching(ByRef _MatchingCriteria As MatcingCriteriaReturn, ByVal MasterProfileId As Integer, ByVal MaxRows As Integer, ByVal lastNum As Integer) As DataTable
        Dim dt As New DataTable
        Try
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection
                Using command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(con, CreateSqlSelect(_MatchingCriteria))
                    command.Parameters.Add(New SqlClient.SqlParameter("@ProfileId", MasterProfileId))
                    command.Parameters.Add(New SqlClient.SqlParameter("@MaxValue", GetMaxValue(_MatchingCriteria)))
                    command.Parameters.Add(New SqlClient.SqlParameter("@Rows", MaxRows))
                    command.Parameters.Add(New SqlClient.SqlParameter("@LastNum", lastNum))
                    DataHelpers.FillDataTable(command, dt)
                    Return dt
                End Using
            End Using
        Catch ex As Exception
            clsMyMail.TrySendMailSupport(ConfigurationManager.AppSettings("ExceptionsEmail"), "On FillwithMatching prID:" & MasterProfileId, ex.ToString())
        End Try
        Return dt
    End Function
    Public Shared Function CreateSqlSelect(ByRef MatchingCriteria As MatcingCriteriaReturn) As String
        ', ByVal genderId As String, ByVal longitudeIn? As Double, ByVal latitudeIn As Double?, ByVal MaxRows As Integer, ByVal lastNum As Integer
        Dim sql As String = <sql><![CDATA[
declare @latitudeIn decimal(13, 9)=null
declare @longitudeIn decimal(13, 9)=null
declare @Country nvarchar(50)=null
declare @LastActivityUTCDate datetime=DATEADD(day,-2,GETUTCDATE())
declare @GenderId int=2
select  @latitudeIn=[latitude],
        @longitudeIn=[longitude],
        @Country=[Country],
       @GenderId=( case when GenderId=1 then 2 else 1 end)
from [dbo].[EUS_Profiles] with (nolock)
where [ProfileID]=@ProfileId


select top (@Rows) p.*,pr.LoginName,pr.Birthday,(SELECT top 1  [FileName] FROM [dbo].[EUS_CustomerPhotos] with (nolock)
  where ([CustomerID]=pr.ProfileID or [CustomerID]=pr.MirrorProfileId) and [HasAproved]=1 and [HasDeclined]=0 and isnull([IsAutoApproved],0)=0 and [IsDeleted]=0 and [DisplayLevel]=0 and [IsDefault]=1) as FileName 
from (
select   Results.*,
        cast( (
            case 
                when ROUND((Results.Matching *100)/@MaxValue,0)>100 then 100 
            else 
                ROUND((Results.Matching *100)/@MaxValue,0) 
            end
         )  as int) as Percents,
        ROW_NUMBER() OVER(ORDER BY Results.Matching desc,Results.distance asc) AS RowSort
from (
        SELECT EUS_Profiles.[ProfileID]
              ,distance=(
                        case 
                            when (@latitudeIn is null or EUS_Profiles.latitude is null or @longitudeIn is null or EUS_Profiles.longitude is null) then 1000
                         else 
                            cast(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)) as int)
                         end
                        )
               ,3333 as Matching
         FROM [dbo].[EUS_Profiles] as EUS_Profiles with (nolock)
         left join [dbo].[EUS_ProfileBreastSize] as breast with (nolock) on EUS_Profiles.ProfileID =breast.ProfileID 
         where EUS_Profiles.ismaster=1  and 
               EUS_Profiles.GenderId=@GenderId  AND 
                EUS_Profiles.[Status] in (2,4) AND
                EUS_Profiles.[PrivacySettings_HideMeFromSearchResults]=0 AND
                not exists(
                            select * 
						    from EUS_ProfilesBlocked bl 
		                    with (nolock)
						    where (bl.FromProfileID =EUS_Profiles.ProfileID and bl.ToProfileID = @ProfileId) or
							    (bl.FromProfileID =@ProfileId and bl.ToProfileID = EUS_Profiles.ProfileID)
					    ) and
               ( EUS_Profiles.Country = @Country OR 
				(
					EUS_Profiles.Country <> @Country AND
					not exists(select * 
								from EUS_ProfilesPrivacySettings bl 
								with (nolock) 
								where (bl.ProfileID =EUS_Profiles.ProfileID) and (HideMeFromUsersInDifferentCountry=1)
					)
				))
     ) as Results
) as p
left join EUS_Profiles  as pr with (nolock) on p.ProfileID=pr.ProfileID 
where RowSort>@LastNum
order by RowSort asc 

]]></sql>.Value
        Try
            Dim MatcingColumn As New StringBuilder
            AddCaseToSumMinMax("EUS_Profiles.[PersonalInfo_HeightID]", CInt(MatchingCriteria.HeightMaxSelectedValue), CInt(MatchingCriteria.HeightMinSelectedValue), MatcingColumn, "1.65")
            AddCaseToSumAge(CInt(MatchingCriteria.ageMaxSelectedValue), CInt(MatchingCriteria.ageMinSelectedValue), MatcingColumn, "1.9")
            AddCaseInList("EUS_Profiles.[PersonalInfo_BodyTypeID]", MatchingCriteria.SelectedBodyTypeList, 15, MatcingColumn, "1.85")
            AddCaseInList("EUS_Profiles.[PersonalInfo_EthnicityID]", MatchingCriteria.SelectedEthnicityList, 15, MatcingColumn, "1.5")
            AddEyeColorId(MatcingColumn, MatchingCriteria.SelectedEyeColorList, "1.75")
            AddHairColorId(MatcingColumn, MatchingCriteria.SelectedHairColorList, "1.7")
            AddCaseDateTypeId(MatcingColumn, MatchingCriteria.SelectedTypeOfDatingList, "1.45")
            AddCaseInList("EUS_Profiles.[PersonalInfo_ChildrenID]", MatchingCriteria.SelectedChildrenList, 10, MatcingColumn, "1.25")
            AddCaseInList("EUS_Profiles.[LookingFor_RelationshipStatusID]", MatchingCriteria.SelectedRelationShipStatusList, 15, MatcingColumn, "1.4")
            AddCaseInList("EUS_Profiles.[OtherDetails_EducationID]", MatchingCriteria.SelectedEducationList, -1, MatcingColumn, "1.2")
            '   AddCaseInList("EUS_Profiles.OtherDetails_AnnualIncomeID", MatchingCriteria.SelectedEducationList, -1, MatcingColumn, "1.2")
            AddCaseInList("EUS_Profiles.[PersonalInfo_ReligionID]", MatchingCriteria.SelectedReligionList, -1, MatcingColumn, "1.55", True)
            AddCaseInList("EUS_Profiles.[PersonalInfo_SmokingHabitID]", MatchingCriteria.SelectedSmokingHabitsList, 10, MatcingColumn, "1.1", True)
            AddCaseInList("EUS_Profiles.[PersonalInfo_DrinkingHabitID]", MatchingCriteria.SelectedDrinkingHabitsList, 10, MatcingColumn, "1.15", True)
            AddCaseWillingTotravel(MatcingColumn, MatchingCriteria.chkWillingToTravelChecked, "1.3")
            AddBreastSizeId(MatcingColumn, MatchingCriteria.SelectedBreastSizeList, "1.8")
            AddSpokenLangs(MatcingColumn, MatchingCriteria.SelectedLanguagesList, "1.35")
            Adddistance(MatchingCriteria.ddlDistanceSelectedValue, MatcingColumn, "1.6")
            AddLastActivityDatetime(MatcingColumn)
            AddCustomerHasPhoto(MatcingColumn)
            sql = sql.Replace("3333", MatcingColumn.ToString)

        Catch ex As Exception
        End Try
        Return sql
    End Function
    Private Shared Sub AddLastActivityDatetime(ByRef Main As StringBuilder)
        AddToSum(Main, <sql><![CDATA[
CAST(
	  (CASE  
            WHEN  EUS_Profiles.LastActivityDateTime>=@LastActivityUTCDate  THEN 100
			WHEN EUS_Profiles.LastActivityMobileDateTime >=@LastActivityUTCDate THEN 100
       ELSE 0
       END
    ) as int
    )
]]></sql>.Value)
    End Sub
    Private Shared Sub AddCustomerHasPhoto(ByRef Main As StringBuilder)
        AddToSum(Main, <sql><![CDATA[
CAST(
	  case 
when (
	  SELECT top 1 Count([CustomerPhotosID])
	  FROM [dbo].[EUS_CustomerPhotos] with (nolock)
	  where CustomerID=EUS_Profiles.ProfileID and [HasAproved]=1 and [DisplayLevel]=0 and [IsDeleted]=0)>0 then 50
when (
	  SELECT top 1 Count([CustomerPhotosID])
	  FROM [dbo].[EUS_CustomerPhotos] with (nolock)
	  where CustomerID=EUS_Profiles.ProfileID and [HasAproved]=1 and [DisplayLevel]>0 and [IsDeleted]=0)>0 then 20
else
0
end

   as int ) 
   
]]></sql>.Value)
    End Sub

    Private Shared Function GetMaxValue(ByRef _MatchingCriteria As MatcingCriteriaReturn) As Decimal
        Dim i As Decimal = 0
        Dim tmp As Integer = 0
        If CInt(_MatchingCriteria.HeightMaxSelectedValue) > -1 OrElse CInt(_MatchingCriteria.HeightMinSelectedValue) > -1 Then
            tmp = CInt(_MatchingCriteria.HeightMaxSelectedValue)

            If tmp > -1 Then
                i += (tmp * 1.6499999999999999)
            Else
                tmp = 55 * 1.6499999999999999
            End If
        End If
        If CInt(_MatchingCriteria.ageMinSelectedValue) > -1 OrElse CInt(_MatchingCriteria.ageMaxSelectedValue) > -1 Then
            tmp = CInt(_MatchingCriteria.ageMinSelectedValue)
            If tmp > -1 Then
                i += ((130 - tmp) * 1.8999999999999999)
            Else
                i += ((130 - 18) * 1.8999999999999999)
            End If
        End If
     
        i += GetMaxValue(_MatchingCriteria.SelectedBodyTypeList, 1.8500000000000001, 15, False)
        i += GetMaxValue(_MatchingCriteria.SelectedEthnicityList, 1.5, 15, False)
        i += GetMaxValue(_MatchingCriteria.SelectedEyeColorList, -1.75, 0, True)
        i += GetMaxValue(_MatchingCriteria.SelectedHairColorList, -1.7, 0, True)
        i += GetMaxValue(_MatchingCriteria.SelectedChildrenList, 1.25, 10, False)
        i += GetMaxValue(_MatchingCriteria.SelectedRelationShipStatusList, 1.3999999999999999, 15, False)
        i += GetMaxValue(_MatchingCriteria.SelectedEducationList, 1.2, -1, True)
        i += GetMaxValue(_MatchingCriteria.SelectedReligionList, 1.55, -1, True)
        i += GetMaxValue(_MatchingCriteria.SelectedSmokingHabitsList, 1.1000000000000001, 10, False)
        i += GetMaxValue(_MatchingCriteria.SelectedDrinkingHabitsList, 1.1499999999999999, 10, False)
        i += If(_MatchingCriteria.chkWillingToTravelChecked, 15 * 1.3, 0)

        If _MatchingCriteria.SelectedBreastSizeList.IsAllSelected = False AndAlso _MatchingCriteria.SelectedBreastSizeList.Values IsNot Nothing AndAlso _MatchingCriteria.SelectedBreastSizeList.Values.Length > 0 Then
            Dim s() As String = _MatchingCriteria.SelectedBreastSizeList.Values.Split(","c)
            tmp = CInt(s(0))
            If s.Length > 1 Then
                For l As Integer = 1 To s.Length - 1
                    If CInt(s(l)) > tmp Then
                        tmp = CInt(s(l))
                    End If
                Next
            End If
            i += tmp * 1.8
        End If
        If _MatchingCriteria.SelectedLanguagesList.IsAllSelected = False AndAlso _MatchingCriteria.SelectedLanguagesList.Values IsNot Nothing AndAlso _MatchingCriteria.SelectedLanguagesList.Values.Length > 0 Then
            Dim s() As String = _MatchingCriteria.SelectedLanguagesList.Values.Split(","c)
            i += s.Length * 1.3500000000000001
        End If
        i += (14000 * 1.6000000000000001) / 1000
        If _MatchingCriteria.SelectedTypeOfDatingList.IsAllSelected = False AndAlso _MatchingCriteria.SelectedTypeOfDatingList.Values IsNot Nothing AndAlso _MatchingCriteria.SelectedTypeOfDatingList.Values.Length > 0 Then
            Dim s() As String = _MatchingCriteria.SelectedTypeOfDatingList.Values.Split(","c)
            i += 15 * 1.45
        End If
        i += 100 'lastActivity
        i += 50 'has public photo
        Return i
    End Function
    Private Shared Function getMaxValue(ByVal item As SearchValuesInfo, ByVal multiplier As Decimal, ByVal minus As Integer, ByVal IsBiger As Boolean) As Double
        Dim re As Double = 0
        If item.IsAllSelected = False AndAlso item.Values IsNot Nothing AndAlso item.Values.Length > 0 Then
            Dim s() As String = item.Values.Split(","c)
            Dim tmp As Integer = CInt(s(0))
            If s.Length > 1 Then
                For l As Integer = 1 To s.Length - 1
                    If IsBiger Then
                        If CInt(s(l)) > tmp Then
                            tmp = CInt(s(l))
                        End If
                    Else
                        If CInt(s(l)) < tmp Then
                            tmp = CInt(s(l))
                        End If
                    End If
                Next
            End If
            re = (minus - tmp) * multiplier
        End If
        Return re
    End Function
    Private Function GetbreastSize(ByVal i As Integer) As Integer
        Select Case i
            Case 10
                Return 3
            Case 3
                Return 4
            Case 4
                Return 5
            Case 5
                Return 6
            Case 6
                Return 7
            Case 7
                Return 8
            Case 8
                Return 9
            Case 9
                Return 10
            Case Else
                Return i
        End Select
    End Function
    Private Shared Sub AddSpokenLangs(ByRef Main As StringBuilder, ByVal item As SearchValuesInfo, ByVal multiplier As String)
        If item.IsAllSelected = False AndAlso item.Values IsNot Nothing AndAlso item.Values.Length > 0 Then
            AddToSum(Main, <sql><![CDATA[
((select count([ProfileSpokenLangID]) from [dbo].[EUS_ProfileSpokenLang] as  psl with (nolock) where psl.[ProfileID] = EUS_Profiles.ProfileId  and psl.SpokenLangId  in (###Values###))*####Multy####)
]]></sql>.Value.Replace("####Multy####", multiplier).Replace("###Values###", item.Values))
        End If
    End Sub
    Private Shared Sub Adddistance(ByVal Max As Integer, ByRef Main As StringBuilder, ByVal multiplier As String)

        If Max = -1 Then
            Max = 10000
        End If
        Dim DatabaseColumn As String = <sql><![CDATA[(case 
                when @latitudeIn is null or EUS_Profiles.latitude is null or @longitudeIn is null or EUS_Profiles.longitude is null then 1000
                else cast(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)) as int)
            end)]]></sql>.Value
        AddToSum(Main, <sql><![CDATA[(case when ###Col###<=###Max###  then ((15000-###Col###)*####Multy####)/1000
	when ###Col### is null then 0
	else (15000-###Col###)/1000 end) ]]></sql>.Value.Replace("###Max###", Max).Replace("###Col###", DatabaseColumn).Replace("####Multy####", multiplier))

    End Sub

    Private Shared Sub AddCaseWillingTotravel(ByRef Main As StringBuilder, ByVal WillingTOTravel As Boolean, ByVal multiplier As String)
        If WillingTOTravel = True Then

            AddToSum(Main, <sql><![CDATA[

(case when (EUS_Profiles.AreYouWillingToTravel= 1) then 15*####Multy####
    else 12 end)
]]></sql>.Value.Replace("####Multy####", multiplier))
        End If
    End Sub
    Private Shared Sub AddBreastSizeId(ByRef Main As StringBuilder, ByVal item As SearchValuesInfo, ByVal multiplier As String)
        If item.IsAllSelected = False AndAlso item.Values IsNot Nothing AndAlso item.Values.Length > 0 Then
            AddToSum(Main, <sql><![CDATA[
(case when breast.[BreastSizeID] in (###Values###) then (case when breast.[BreastSizeID]=10 then 2 else breast.[BreastSizeID] END)*####Multy####
    when breast.[BreastSizeID]= 10  then 3
     when breast.[BreastSizeID]= 3  then 4
     when breast.[BreastSizeID]= 4 then 5
     when breast.[BreastSizeID]= 5  then 6
     when breast.[BreastSizeID] =6  then 7
     when breast.[BreastSizeID] =7  then 8
    when breast.[BreastSizeID]= 8 then 9
    when breast.[BreastSizeID]= 9  then 10
    when breast.[BreastSizeID] is null  then 0
    else breast.[BreastSizeID] end)
]]></sql>.Value.Replace("####Multy####", multiplier).Replace("###Values###", item.Values))
        End If
    End Sub
    Private Shared Sub AddCaseDateTypeId(ByRef Main As StringBuilder, ByVal item As SearchValuesInfo, ByVal multiplier As String)
        If item.IsAllSelected = False AndAlso item.Values IsNot Nothing AndAlso item.Values.Length > 0 Then
            Dim vals As String() = item.Values.Split(","c)
            Dim sql As New StringBuilder
            For Each Val As String In vals
                Select Case CInt(Val)
                    Case TypeOfDatingEnum.AdultDating_Casual
                        If sql.Length = 0 Then
                            sql.Append("EUS_Profiles.LookingFor_TypeOfDating_AdultDating_Casual= 1")
                        Else
                            sql.Append(" or EUS_Profiles.LookingFor_TypeOfDating_AdultDating_Casual= 1")
                        End If
                    Case TypeOfDatingEnum.Friendship
                        If sql.Length = 0 Then
                            sql.Append("EUS_Profiles.LookingFor_TypeOfDating_Friendship= 1")
                        Else
                            sql.Append(" or EUS_Profiles.LookingFor_TypeOfDating_Friendship= 1")
                        End If
                    Case TypeOfDatingEnum.LongTermRelationship
                        If sql.Length = 0 Then
                            sql.Append("EUS_Profiles.LookingFor_TypeOfDating_LongTermRelationship= 1")
                        Else
                            sql.Append(" or EUS_Profiles.LookingFor_TypeOfDating_LongTermRelationship= 1")
                        End If
                    Case TypeOfDatingEnum.MarriedDating
                        If sql.Length = 0 Then
                            sql.Append("EUS_Profiles.LookingFor_TypeOfDating_MarriedDating= 1")
                        Else
                            sql.Append(" or EUS_Profiles.LookingFor_TypeOfDating_MarriedDating= 1")
                        End If
                    Case TypeOfDatingEnum.MutuallyBeneficialArrangements
                        If sql.Length = 0 Then
                            sql.Append("EUS_Profiles.LookingFor_TypeOfDating_MutuallyBeneficialArrangements= 1")
                        Else
                            sql.Append(" or EUS_Profiles.LookingFor_TypeOfDating_MutuallyBeneficialArrangements= 1")
                        End If
                    Case TypeOfDatingEnum.ShortTermRelationship
                        If sql.Length = 0 Then
                            sql.Append("EUS_Profiles.LookingFor_TypeOfDating_ShortTermRelationship= 1")
                        Else
                            sql.Append(" or EUS_Profiles.LookingFor_TypeOfDating_ShortTermRelationship= 1")
                        End If
                End Select
            Next

            AddToSum(Main, <sql><![CDATA[

(case when (####Cols###) then 15*####Multy####
    else 12 end)
]]></sql>.Value.Replace("####Cols###", sql.ToString).Replace("####Multy####", multiplier))
        End If
    End Sub
    Private Shared Sub AddHairColorId(ByRef Main As StringBuilder, ByVal item As SearchValuesInfo, ByVal multiplier As String)
        If item.IsAllSelected = False AndAlso item.Values IsNot Nothing AndAlso item.Values.Length > 0 Then
            AddToSum(Main, <sql><![CDATA[
(case when EUS_Profiles.[PersonalInfo_HairColorID] in (###Values###) then EUS_Profiles.[PersonalInfo_HairColorID]*####Multy####
	when EUS_Profiles.[PersonalInfo_HairColorID]= 13  then 19
    when EUS_Profiles.[PersonalInfo_HairColorID] =11  then 18
     when EUS_Profiles.[PersonalInfo_HairColorID]= 14  then 17
     when EUS_Profiles.[PersonalInfo_HairColorID]= 15  then 16
     when EUS_Profiles.[PersonalInfo_HairColorID]= 12  then 15
     when EUS_Profiles.[PersonalInfo_HairColorID]= 17  then 14
     when EUS_Profiles.[PersonalInfo_HairColorID]= 19  then 13
    when EUS_Profiles.[PersonalInfo_HairColorID]= 16  then 12
    when EUS_Profiles.[PersonalInfo_HairColorID]= 18  then 11
 when EUS_Profiles.[PersonalInfo_HairColorID] is null  then 0
    else EUS_Profiles.[PersonalInfo_HairColorID] end)
]]></sql>.Value.Replace("####Multy####", multiplier).Replace("###Values###", item.Values))
        End If
    End Sub
    Private Shared Sub AddEyeColorId(ByRef Main As StringBuilder, ByVal item As SearchValuesInfo, ByVal multiplier As String)
        If item.IsAllSelected = False AndAlso item.Values IsNot Nothing AndAlso item.Values.Length > 0 Then
            AddToSum(Main, <sql><![CDATA[
(case when EUS_Profiles.[PersonalInfo_EyeColorID] in (###Values###) then [PersonalInfo_BodyTypeID]*####Multy####
	when EUS_Profiles.[PersonalInfo_EyeColorID]= 13  then 15
    when EUS_Profiles.[PersonalInfo_EyeColorID]= 10  then 14
     when EUS_Profiles.[PersonalInfo_EyeColorID]= 12 then 13
     when EUS_Profiles.[PersonalInfo_EyeColorID] =14 then 12
     when EUS_Profiles.[PersonalInfo_EyeColorID]= 12  then 11
     when EUS_Profiles.[PersonalInfo_EyeColorID]= 9  then 10
     when EUS_Profiles.[PersonalInfo_EyeColorID]= 15 then 9
   when EUS_Profiles.[PersonalInfo_EyeColorID] is null then 0
    else EUS_Profiles.[PersonalInfo_EyeColorID] end)
]]></sql>.Value.Replace("####Multy####", multiplier).Replace("###Values###", item.Values))
        End If
    End Sub
    Private Shared Sub AddCaseToSumMinMax(ByVal DatabaseColumn As String, ByVal Max As Integer, ByVal Min As Integer, ByRef Main As StringBuilder, ByVal multiplier As String)
        If Max <> -1 OrElse Min <> -1 Then
            If Max <> -1 AndAlso Min <> -1 Then
                AddToSum(Main, <sql><![CDATA[(case when ###Col###>=###Min### and ###Col###<=###Max### then ###Col###*####Multy####
	when ###Col### is null then 0
	else ###Col### end) ]]></sql>.Value.Replace("###Min###", Min).Replace("###Max###", Max).Replace("###Col###", DatabaseColumn).Replace("####Multy####", multiplier))
            ElseIf Min <> -1 Then
                AddToSum(Main, <sql><![CDATA[(case when ###Col###>=###Min###  then ###Col###*####Multy#### 
	when ###Col### is null then 0
	else ###Col### end) ]]></sql>.Value.Replace("###Min###", Min).Replace("###Col###", DatabaseColumn).Replace("####Multy####", multiplier))
            Else
                AddToSum(Main, <sql><![CDATA[(case when  ###Col###<=###Max### then ###Col###*####Multy#### 
	when ###Col### is null then 0
	else ###Col### end) ]]></sql>.Value.Replace("###Max###", Max).Replace("###Col###", DatabaseColumn).Replace("####Multy####", multiplier))
            End If
        End If
    End Sub
    Private Shared Sub AddCaseToSumAge(ByVal Max As Integer, ByVal Min As Integer, ByRef Main As StringBuilder, ByVal multiplier As String)
        If Max <> -1 OrElse Min <> -1 Then
            If Max <> -1 AndAlso Min <> -1 Then
                AddToSum(Main, <sql><![CDATA[(case when DATEDIFF(year,EUS_Profiles.[Birthday],getutcdate())>=###Min### and DATEDIFF(year,EUS_Profiles.[Birthday],getutcdate())<=###Max### then (130-DATEDIFF(year,EUS_Profiles.[Birthday],getutcdate()))*####Multy####
	when EUS_Profiles.[Birthday] is null then 0
	else (130-DATEDIFF(year,EUS_Profiles.[Birthday],getutcdate())) end) ]]></sql>.Value.Replace("###Min###", Min).Replace("###Max###", Max).Replace("####Multy####", multiplier))
            ElseIf Min <> -1 Then
                AddToSum(Main, <sql><![CDATA[(case when DATEDIFF(year,EUS_Profiles.[Birthday],getutcdate())>=###Min###  then (130-DATEDIFF(year,EUS_Profiles.[Birthday],getutcdate()))*####Multy#### 
	when EUS_Profiles.[Birthday] is null then 0
	else (130-DATEDIFF(year,EUS_Profiles.[Birthday],getutcdate())) end) ]]></sql>.Value.Replace("###Min###", Min).Replace("####Multy####", multiplier))
            Else
                AddToSum(Main, <sql><![CDATA[(case when  DATEDIFF(year,EUS_Profiles.[Birthday],getutcdate())<=###Max### then (130-DATEDIFF(year,EUS_Profiles.[Birthday],getutcdate()))*####Multy#### 
	when EUS_Profiles.[Birthday] is null then 0
	else (130-DATEDIFF(year,EUS_Profiles.[Birthday],getutcdate())) end) ]]></sql>.Value.Replace("###Max###", Max).Replace("####Multy####", multiplier))
            End If
        End If
    End Sub
    Private Shared Sub AddCaseInList(ByVal databaseColumn As String, ByVal item As SearchValuesInfo, ByVal minus As Integer, ByRef Main As StringBuilder, ByVal multiplier As String, Optional DiscardOthers As Boolean = False)
        If item.IsAllSelected = False AndAlso item.Values IsNot Nothing AndAlso item.Values.Length > 0 Then
            If Not DiscardOthers Then
                AddToSum(Main, <sql><![CDATA[
    (case when ###Col### in (###Values###) then (###Minus###-###Col###)*####Multy####
	when ###Col### is null then 0
	else ###Minus###-###Col### end)]]></sql>.Value.Replace("###Col###", databaseColumn).Replace("###Values###", item.Values).Replace("####Multy####", multiplier).Replace("###Minus###", minus))
            Else
                AddToSum(Main, <sql><![CDATA[
    (case when ###Col### in (###Values###) then (###Minus###-###Col###)*####Multy####
	when ###Col### is null then 0
	else 0 end)]]></sql>.Value.Replace("###Col###", databaseColumn).Replace("###Values###", item.Values).Replace("####Multy####", multiplier).Replace("###Minus###", minus))
            End If

        End If
    End Sub
    Private Shared Sub AddToSum(ByRef str As StringBuilder, ByVal NewString As String)
        If str.Length = 0 Then
            str.Append(NewString)
        Else
            str.Append(" + " & NewString)
        End If
    End Sub
End Class
Public Class GetMatchingCriteriaResult
    Public Property HasErrors As Boolean
    Public Property Row As DSLists.EUS_MatchingCriteriaSearchRow
    Public Property HasRow As Boolean
End Class

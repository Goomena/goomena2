﻿Public Class clsSQLInjectionClear
    'Defines the set of characters that will be checked.
    'You can add to this list, or remove items from this list, as appropriate for your site
    Public Shared blackList As String() = {"--", ";--", "/*", "*/", "@@", _
                                           "@", "char", "nchar", "varchar", "nvarchar", "alter", _
                                           "begin", "cast", "create", "cursor", "declare", "delete", _
                                           "drop", "end", "exec", "execute", "fetch", "insert", _
                                           "kill", "open", "select", "sys", "sysobjects", "syscolumns", _
                                           "table", "update", "select"}

    'The utility method that performs the blacklist comparisons
    'You can change the error handling, and error redirect location to whatever makes sense for your site.
    Public Shared Sub ClearString(ByRef szInputString As String, ByVal MaxChar As Integer)
        szInputString = Mid(szInputString, 1, MaxChar)
        szInputString = mysql_escape(szInputString)
        For i As Integer = 0 To blackList.Length - 1
            If (szInputString.IndexOf(blackList(i), StringComparison.OrdinalIgnoreCase) >= 0) Then
                szInputString.Replace(blackList(i), "#")
            End If
        Next
    End Sub

    Shared Function mysql_escape(ByVal thisWord) As String
        If thisWord <> "" Then
            thisWord = Replace(thisWord, "/*", "")
            thisWord = Replace(thisWord, "*/", "")
            thisWord = Replace(thisWord, "UNION", "")
            thisWord = Replace(thisWord, ";", "\;")
            thisWord = Replace(thisWord, "'", "&amp;rsquo;")
            thisWord = Replace(thisWord, """", "&amp;quot;")
            thisWord = Replace(thisWord, "\", "\\")
        End If
        mysql_escape = thisWord
    End Function



End Class

﻿Public Class clsIndexesMaintenanceHelper

    Public Event TaskCompeleted(sender As Object, e As clsIndexesMaintenanceEventArgs)

    Public Sub PerformTask()
        Dim evArgs As New clsIndexesMaintenanceEventArgs()
        Try

            Dim continueTask As Boolean = True
            Dim sql As String = "exec [SYSDB_INDEXES_MAINTENANCE] @seconds_timeout=@seconds_timeout,@clear_interrupted=@clear_interrupted,@clear_table=@clear_table"

            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection

                Using cmd As Data.SqlClient.SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                    cmd.CommandTimeout = 240
                    cmd.Parameters.AddWithValue("@seconds_timeout", 15)
                    cmd.Parameters.AddWithValue("@clear_interrupted", False)
                    cmd.Parameters.AddWithValue("@clear_table", False)

                    While continueTask
                        continueTask = False
                        Try

                            Using dt As DataTable = DataHelpers.GetDataTable(cmd)
                                If (dt.Rows.Count > 0) Then
                                    If (CBool(dt.Rows(0)("continue_maintenance")) = True) Then
                                        continueTask = True
                                        cmd.Parameters("@clear_interrupted").Value = False
                                        System.Threading.Thread.Sleep(3000)
                                    End If
                                End If
                            End Using


                        Catch ex As System.Data.SqlClient.SqlException
                            continueTask = DataHelpers.CheckSqlException(ex.Message)
                            If (continueTask) Then
                                cmd.Parameters("@clear_interrupted").Value = True
                            Else
                                Throw
                            End If
                        End Try

                    End While
                End Using

            End Using

            evArgs.IsComplete = True
        Catch ex As Exception
            evArgs.CompleteWithError = True
            evArgs.ErrorMessage = ex.ToString()
        End Try

        Try
            RaiseEvent TaskCompeleted(Me, evArgs)
        Catch
        End Try

    End Sub

End Class

Public Class clsIndexesMaintenanceEventArgs
    Inherits EventArgs

    Public Property IsComplete As Boolean
    Public Property CompleteWithError As Boolean
    Public Property ErrorMessage As String

End Class

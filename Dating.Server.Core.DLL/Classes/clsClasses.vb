﻿Imports Dating.Server.Datasets.DLL
Imports System.Web

<Serializable()> _
Public Class clsDataRecordBuyerInfo
    Public PayerID As String
    Public BusinessName As String
    Public FirstName As String
    Public LastName As String
    Public PayerEmail As String
    Public Address As String
    Public ContactPhone As String
    Public ResidenceCountry As String
    Public PayerStatus As String
End Class

<Serializable()> _
Public Class clsCustomerAvailableCredits
    Public AvailableCredits As Integer
    Public CreditsRecordsCount As Integer
    Public HasSubscription As Boolean
End Class

<Serializable()> _
Public Class clsGetMemberActionsCounters
    Public CountWhoViewedMe As Integer
    Public CountWhoFavoritedMe As Integer
    Public CountWhoSharedPhotos As Integer
    Public NewDates As Integer
    Public NewLikes As Integer
    Public NewOffers As Integer
    Public NewMessages As Integer
End Class

<Serializable()> _
Public Class clsDataRecordIPN

    Public Property PayTransactionID As String
    Public Property PayProviderID As Long
    Public Property SalesSiteID As Long
    Public Property SalesSiteProductID As Long
    Public Property CustomerID As Long
    Public Property custopmerIP As String = ""
    Public Property PaymentDateTime As String ' Gia na min exoume thema me to HASH
    Public Property TransactionTypeID As Integer '1=Sale,2=Refund,3=Subscription,4=Cancel Sub
    Public Property PayProviderTransactionID As String
    Public Property PayProviderAmount As Double ' Gia na min exoume thema me to HASH
    Public Property PromoCode As String
    Public Property SaleDescription As String
    Public Property SaleQuantity As Integer
    Public Property CustomReferrer As String
    Public Property BuyerInfo As clsDataRecordBuyerInfo
    Public Property VerifyHASH As String
    Public Property Currency As String

End Class

<Serializable()> _
Public Class clsDataRecordIPNReturn

    Public HasErrors As Boolean
    Public ErrorCode As Integer
    Public Message As String
    Public ExtraMessage As String

End Class

<Serializable()> _
Public Class clsDataRecordJetCustomerDataReturn

    Public HasErrors As Boolean
    Public ErrorCode As Integer
    Public hosterName As String
    Public downloadedSoFar As Long
    Public DailyLimit As Long
    Public visualStatus As String
    Public status As Boolean
    Public ExtraNotes As String
    Public imagePath As String
    Public progress As Integer
    Public emptyText As List(Of String)
End Class

'<Serializable()> _
'Public Class clsDataRecordJetCustomerDataListReturn
'    Public Property RapidHosters As New List(Of clsDataRecordJetCustomerDataReturn)

'End Class




<Serializable()> _
Public Class clsUserUnlockInfo
    Public Property OtherLoginName As String
    Public Property OfferId As Integer
    Public Property ReturnUrl As String
    Public Property IsUnlocked As Boolean

    Public Sub Reset()
        OtherLoginName = Nothing
        OfferId = 0
        ReturnUrl = Nothing
        IsUnlocked = False
    End Sub

    Public Function IsValid() As Boolean
        Return (OfferId > 0)
    End Function
End Class


<Serializable()> _
Public Class EmailParams
    Public EmailAddress As String
    Public LagID As String
    Public LoginName As String
    Public ProfileID As Integer?
    Public SMTPServerCredentialsID As Integer?
    Public BCCRecipients As String
End Class



Public Class SendMessageToProfilesParams
    Public EmailsList As New List(Of EmailParams)
    Public ProfilesList As New List(Of Integer)
    Public DSUniCMSDB As UniCMSDB
    Public SendApplicationMessage As Boolean
    Public SendEmail As Boolean
    Public BCCRecipients As String
    'Public SMTPServerCredentialsID As Integer?

    Public UseProfilesList As Boolean = True

    'Public SubjectUS As String
    'Public TextUS As String
    'Public SubjectGR As String
    'Public TextGR As String

    'Public MessageAction As String
    'Public Sub SendMessageToProfilesParams()
    '    'ProfilesList = New List(Of Integer)
    '    'DSUniCMSDB = New UniCMSDB
    'End Sub

    Public Function GetMessageRow()
        If (DSUniCMSDB.SYS_EmailMessages.Rows.Count > 0) Then
            Return DirectCast(DSUniCMSDB.SYS_EmailMessages.Rows(0), UniCMSDB.SYS_EmailMessagesRow)
        End If
        Return Nothing
    End Function


    'Public Sub SetMessageRow(row As UniCMSDB.SYS_EmailMessagesRow)
    '    DSUniCMSDB = New UniCMSDB()
    '    DSUniCMSDB.SYS_EmailMessages.AddSYS_EmailMessagesRow(row)
    'End Sub

End Class



Public Class ActionResult

    Public HasErrors As Boolean
    Public ErrorCode As Integer
    Public Message As String
    Public ExtraMessage As String
    Public MailData As clsMyMail.clsMailData

End Class

Public Class DeleteAccountParams

    Public IP As String
    Public GEO_COUNTRY_CODE As String
    Public HTTP_USER_AGENT As String
    Public Referrer As String
    Public SupportTeamEmail As String

End Class




Public Class clsGlobalPosition
    Public Property latitude As Decimal
    Public Property longitude As Decimal
End Class


Public Class clsGlobalPositionWCF
    Public Property latitude As Double
    Public Property longitude As Double

    Public Sub New()

    End Sub

    Public Sub New(sLatitude As String, sLongitude As String)
        latitude = AppUtils.GetDoubleValue_FromStringWithPeriod(sLatitude)
        longitude = AppUtils.GetDoubleValue_FromStringWithPeriod(sLongitude)
    End Sub
End Class



Public Class clsApprovedProfileResult
    Public Property IsNewProfile As Boolean
    Public Property IsSuccessful As Boolean
End Class


Public Class clsRejectedProfileResult
    Public Property IsNewProfile As Boolean
    Public Property IsSuccessful As Boolean
End Class


Public Class clsSetDefaultPhotoResult
    Public Property PhotoID As Long
    Public Property IsSuccessful As Boolean
End Class




Public Class LoginWithFacebookSuccessParams
    Public Property LoginName As String
    Public Property Password As String
    'Public Property RememberUserName As Boolean
    'Public Property DataRecordLoginReturn As clsDataRecordLoginMemberReturn
    Public Property OnLoginSuccessSaveFBData As Boolean
End Class


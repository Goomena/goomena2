﻿Imports System.Text.RegularExpressions
Imports System.Text

Public Class clsGeoHelper

  

    Public Shared Function GetValidCountryCode(countryCode As String) As String
        Select Case countryCode
            Case "GB" : countryCode = "UK"
        End Select
        Return countryCode
    End Function

    Public Shared Function CheckCountryTable(countryCode As String) As Boolean
        Dim sql As String = ""
        Dim countRows As Integer = 0
        countryCode = clsGeoHelper.GetValidCountryCode(countryCode)

        Try

            sql = <sql><![CDATA[
SELECT COUNT(*) as countRows
        FROM INFORMATION_SCHEMA.TABLES 
        WHERE TABLE_SCHEMA = 'dbo' 
        AND  TABLE_NAME = 'SYS_GEO_[tblCountryCode]'
]]></sql>.Value

            '  Dim command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql)
            sql = sql.Replace("[tblCountryCode]", countryCode)
            countRows = DataHelpers.ExecuteScalar(sql)

        Catch ex As Exception

        End Try

        Return (countRows > 0)
    End Function

    Public Shared Function GetCountryRegions(countryCode As String, Optional lagid As String = Nothing) As DataTable
        Dim sql As String = ""


        countryCode = clsGeoHelper.GetValidCountryCode(countryCode)
        Try

            sql = <sql><![CDATA[
IF (EXISTS (SELECT * 
        FROM INFORMATION_SCHEMA.TABLES 
        WHERE TABLE_SCHEMA = 'dbo' 
        AND  TABLE_NAME = 'SYS_GEO_[tblCountryCode]'))
BEGIN

    if(EXISTS (select * from SYS_GEO_[tblCountryCode]
                where countryCode=@countryCode
                and (language=N'EN')))
    begin

        select distinct region1 
        from SYS_GEO_[tblCountryCode]
        where countryCode=@countryCode
        and (language=N'EN')
        order by region1
    end
    else  if(EXISTS (select * from SYS_GEO_[tblCountryCode]
                where countryCode=@countryCode
                and (language=@language or isnull(@language,'')='')))
    begin

        select distinct region1 
        from SYS_GEO_[tblCountryCode]
        where countryCode=@countryCode
        and (language=@language or isnull(@language,'')='')
        order by region1

    end
    else
    begin

        select distinct region1 
        from SYS_GEO_[tblCountryCode]
        where countryCode=@countryCode
        order by region1

    end

END

]]></sql>.Value


            sql = sql.Replace("[tblCountryCode]", countryCode)
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                    command.Parameters.Add(New SqlClient.SqlParameter("@countryCode", countryCode))

                    If (Not String.IsNullOrEmpty(lagid)) Then
                        If (lagid = "GR") Then
                            command.Parameters.Add(New SqlClient.SqlParameter("@language", "EL"))
                        Else
                            command.Parameters.Add(New SqlClient.SqlParameter("@language", "EN"))
                        End If
                    Else
                        command.Parameters.Add(New SqlClient.SqlParameter("@language", System.DBNull.Value))
                    End If
                    Using dt As DataTable = DataHelpers.GetDataTable(command)
                        Return dt
                    End Using
                End Using
            End Using
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Public Shared Function GetCountryRegionCities(countryCode As String, region1 As String, lagid As String) As DataTable
        Dim sql As String = ""
        countryCode = clsGeoHelper.GetValidCountryCode(countryCode)
        Try

            sql = <sql><![CDATA[
IF (EXISTS (SELECT * 
        FROM INFORMATION_SCHEMA.TABLES 
        WHERE TABLE_SCHEMA = 'dbo' 
        AND  TABLE_NAME = 'SYS_GEO_[tblCountryCode]'))
BEGIN

    if(EXISTS (select * from SYS_GEO_[tblCountryCode]
                where countryCode=@countryCode
                and region1=@region1
                and (language=N'EN')))
    begin

        select DISTINCT city 
        from SYS_GEO_[tblCountryCode]
        where countryCode=@countryCode
        and region1=@region1
        and (language=N'EN')
        order by city

    end
    else if(EXISTS (select * from SYS_GEO_[tblCountryCode]
                where countryCode=@countryCode
                and region1=@region1
                and (language=@language or isnull(@language,'')='')))
    begin

        select DISTINCT city 
        from SYS_GEO_[tblCountryCode]
        where countryCode=@countryCode
        and region1=@region1
        and (language=@language or isnull(@language,'')='')
        order by city
    end
    else
    begin

        select DISTINCT city 
        from SYS_GEO_[tblCountryCode]
        where countryCode=@countryCode
        and region1=@region1
        order by city
    end

END

]]></sql>.Value

            sql = sql.Replace("[tblCountryCode]", countryCode)
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                    command.Parameters.Add(New SqlClient.SqlParameter("@countryCode", countryCode))
                    command.Parameters.Add(New SqlClient.SqlParameter("@region1", region1))

                    If (Not String.IsNullOrEmpty(lagid)) Then
                        If (lagid = "GR") Then
                            command.Parameters.Add(New SqlClient.SqlParameter("@language", "EL"))
                        Else
                            command.Parameters.Add(New SqlClient.SqlParameter("@language", "EN"))
                        End If
                    Else
                        command.Parameters.Add(New SqlClient.SqlParameter("@language", System.DBNull.Value))
                    End If
                    Using dt As DataTable = DataHelpers.GetDataTable(command)
                        Return dt
                    End Using
                End Using
            End Using
        Catch ex As Exception

            Return Nothing
        End Try



    End Function

    Public Shared Function GetCountryCityPostalcodes(countryCode As String, region1 As String, city As String, lagid As String) As DataTable
        Dim sql As String = ""


        countryCode = clsGeoHelper.GetValidCountryCode(countryCode)
        Try

            sql = <sql><![CDATA[
IF (EXISTS (SELECT * 
        FROM INFORMATION_SCHEMA.TABLES 
        WHERE TABLE_SCHEMA = 'dbo' 
        AND  TABLE_NAME = 'SYS_GEO_[tblCountryCode]'))
BEGIN

    if(EXISTS (select * from SYS_GEO_[tblCountryCode]
                where countryCode=@countryCode
                and region1=@region1
                and city=@city
                and (language=N'EN')))
    begin

        select DISTINCT postcode 
        from SYS_GEO_[tblCountryCode]
        where countryCode=@countryCode
        and region1=@region1
        and city=@city
        and (language=N'EN')
        order by postcode
    end
    else if(EXISTS (select * from SYS_GEO_[tblCountryCode]
                where countryCode=@countryCode
                and region1=@region1
                and city=@city
                and (language=@language or isnull(@language,'')='')))
    begin

        select DISTINCT postcode 
        from SYS_GEO_[tblCountryCode]
        where countryCode=@countryCode
        and region1=@region1
        and city=@city
        and (language=@language or isnull(@language,'')='')
        order by postcode
    end
    else
    begin

        select DISTINCT postcode 
        from SYS_GEO_[tblCountryCode]
        where countryCode=@countryCode
        and region1=@region1
        and city=@city
        order by postcode

    end

END

]]></sql>.Value

            sql = sql.Replace("[tblCountryCode]", countryCode)
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                    command.Parameters.Add(New SqlClient.SqlParameter("@countryCode", countryCode))
                    command.Parameters.Add(New SqlClient.SqlParameter("@region1", region1))
                    command.Parameters.Add(New SqlClient.SqlParameter("@city", city))

                    If (Not String.IsNullOrEmpty(lagid)) Then
                        If (lagid = "GR") Then
                            command.Parameters.Add(New SqlClient.SqlParameter("@language", "EL"))
                        Else
                            command.Parameters.Add(New SqlClient.SqlParameter("@language", "EN"))
                        End If
                    Else
                        command.Parameters.Add(New SqlClient.SqlParameter("@language", System.DBNull.Value))
                    End If

                    Using dt As DataTable = DataHelpers.GetDataTable(command)
                        Return dt
                    End Using
                End Using
            End Using

        Catch ex As Exception

            Return Nothing
        End Try



    End Function



    Public Shared Function GetGEOByZip(countryCode As String, zipstr As String, Optional lagid As String = Nothing) As DataTable
        Dim sql As String = ""


        countryCode = clsGeoHelper.GetValidCountryCode(countryCode)
        Try
            '            sql = <sql><![CDATA[
            'EXEC	[GEO_GetByPostalCode] @countryCode=@countryCode, @zip=@zip, @language=@language
            '            ]]></sql>.Value

            sql = <sql><![CDATA[
IF (EXISTS (SELECT * 
        FROM INFORMATION_SCHEMA.TABLES 
        WHERE TABLE_SCHEMA = 'dbo' 
        AND  TABLE_NAME = 'SYS_GEO_[tblCountryCode]'))
BEGIN

if(EXISTS (select * from SYS_GEO_[tblCountryCode]
            where postcode=@zip  
            and countryCode=@countryCode
            and (language=N'EN')))
begin

    select * from SYS_GEO_[tblCountryCode]
    where postcode=@zip  
    and countryCode=@countryCode
    and (language=N'EN')

end
else if(EXISTS (select * from SYS_GEO_[tblCountryCode]
            where postcode=@zip  
            and countryCode=@countryCode
            and (language=@language or isnull(@language,'')='')))
begin

    select * from SYS_GEO_[tblCountryCode]
    where postcode=@zip  
    and countryCode=@countryCode
    and (language=@language or isnull(@language,'')='')

end
else
begin

    select * 
    from SYS_GEO_[tblCountryCode]
    where postcode=@zip  
    and countryCode=@countryCode

end

END
]]></sql>.Value


            sql = sql.Replace("[tblCountryCode]", countryCode)
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                    command.Parameters.Add(New SqlClient.SqlParameter("@zip", zipstr))
                    command.Parameters.Add(New SqlClient.SqlParameter("@countryCode", countryCode))

                    If (Not String.IsNullOrEmpty(lagid)) Then
                        If (lagid = "GR") Then
                            command.Parameters.Add(New SqlClient.SqlParameter("@language", "EL"))
                        Else
                            command.Parameters.Add(New SqlClient.SqlParameter("@language", "EN"))
                        End If
                    Else
                        command.Parameters.Add(New SqlClient.SqlParameter("@language", System.DBNull.Value))
                    End If
                    Using dt As DataTable = DataHelpers.GetDataTable(command)
                        Return dt
                    End Using
                End Using

            End Using


        Catch ex As Exception
            Return Nothing
        End Try


    End Function


    Public Shared Function GetPostalCodesNext(countryCode As String, zipInt As Integer, radius As Integer, lagid As String) As DataTable
        Dim sql As String = ""

        Try

            sql = <sql><![CDATA[
declare @lat as decimal(13,9),@lng as decimal(13,9)

SELECT  @lat = latitude,@lng = longitude
  FROM SYS_GEO_GR
  WHERE  postcode=@zip 
  
  
SELECT DISTINCT t.postcode, 
 SQRT(POWER((@lat-t.latitude)*110.7,2)+POWER((@lng - t.longitude)*75.6,2)) AS distance 
FROM SYS_GEO_GR as t
WHERE  SQRT(POWER((@lat-t.latitude)*110.7,2)+POWER(( @lng -t.longitude)*75.6,2)) <= @radius
ORDER BY SQRT(POWER((@lat-t.latitude)*110.7,2)+POWER(( @lng -t.longitude)*75.6,2)) ASC

]]></sql>.Value

            sql = sql.Replace("[countryCode]", countryCode)
            Dim zipstr As String = zipInt.ToString("### ##")
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                    command.Parameters.Add(New SqlClient.SqlParameter("@zip", zipstr))
                    command.Parameters.Add(New SqlClient.SqlParameter("@radius", radius))

                    Using dt As DataTable = DataHelpers.GetDataTable(command)
                        Return dt
                    End Using
                End Using
            End Using
        Catch ex As Exception
            Return Nothing
        End Try
    End Function



    Public Shared Function GetNearestPostalCodesCommaSeparatedString(countryCode As String, zipStr As String, radius As Integer, lagid As String) As String
        zipStr = Regex.Replace(zipStr, "[^\d]", "")
        Dim dt As DataTable = clsGeoHelper.GetPostalCodesNext(countryCode, zipStr, radius, lagid)
        Dim sb2 As New StringBuilder()
        For Each dr As DataRow In dt.Rows
            sb2.Append("'" & dr("postcode") & "',")
        Next
        If (sb2.Length > 1) Then
            sb2.Remove(sb2.Length - 1, 1)
        End If
        Return sb2.ToString()
    End Function


    Public Shared Function GetCityCenterPostcode(countryCode As String, region1 As String, city As String) As String
        Dim sql As String = ""
        Dim postCode = ""


        countryCode = clsGeoHelper.GetValidCountryCode(countryCode)
        Try
            If (Not String.IsNullOrEmpty(city)) Then
                sql = <sql><![CDATA[
IF (EXISTS (SELECT * 
        FROM INFORMATION_SCHEMA.TABLES 
        WHERE TABLE_SCHEMA = 'dbo' 
        AND  TABLE_NAME = 'SYS_GEO_[tblCountryCode]'))
BEGIN
    select  MIN([postcode])
    FROM SYS_GEO_[tblCountryCode]
    WHERE  city = @city and countryCode=@countryCode
END
]]></sql>.Value
            Else
                sql = <sql><![CDATA[
IF (EXISTS (SELECT * 
        FROM INFORMATION_SCHEMA.TABLES 
        WHERE TABLE_SCHEMA = 'dbo' 
        AND  TABLE_NAME = 'SYS_GEO_[tblCountryCode]'))
BEGIN
    select  MIN([postcode])
    FROM SYS_GEO_[tblCountryCode]
    WHERE  region1 = @region1 and countryCode=@countryCode
END
]]></sql>.Value
            End If

            sql = sql.Replace("[tblCountryCode]", countryCode)
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection

                Using command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                    command.Parameters.Add(New SqlClient.SqlParameter("@city", city))
                    command.Parameters.Add(New SqlClient.SqlParameter("@region1", region1))
                    command.Parameters.Add(New SqlClient.SqlParameter("@countryCode", countryCode))

                    Using dt As DataTable = DataHelpers.GetDataTable(command)
                        If (dt.Rows.Count = 1) Then
                            If (Not IsDBNull(dt.Rows(0)(0))) Then postCode = dt.Rows(0)(0)
                        End If
                    End Using
                End Using

            End Using

        Catch ex As Exception
        End Try

        Return postCode
    End Function


    Public Shared Function GetCountryMinPostcode(countryCode As String, Optional lagid As String = Nothing) As String
        '   Dim sql As String = ""
        Dim postCode = ""

        Try
            Using dt As DataTable = GetCountryMinPostcodeDataTable(countryCode, Nothing, Nothing, lagid)
                If (dt.Rows.Count > 0) Then
                    If (Not IsDBNull(dt.Rows(0)("countryCode"))) Then postCode = dt.Rows(0)("countryCode")
                End If
            End Using
           
        Catch ex As Exception
        End Try

        Return postCode
    End Function


    Public Shared Function GetCountryMinPostcodeDataTable(countryCode As String, region As String, city As String, Optional lagid As String = Nothing) As DataTable
        Dim sql As String = ""
        '  Dim postCode = ""


        countryCode = clsGeoHelper.GetValidCountryCode(countryCode)
        Try
            '            sql = <sql><![CDATA[
            'EXEC	[GEO_GetMinPostalCode] @countryCode=@countryCode, @region=@region, @city=@city, @language=@language
            ']]></sql>.Value
            sql = <sql><![CDATA[

declare @MinPostcode as varchar(20)
declare @latitude as nvarchar(50)
declare @longitude as nvarchar(50)

IF (EXISTS (SELECT * 
        FROM INFORMATION_SCHEMA.TABLES 
        WHERE TABLE_SCHEMA = 'dbo' 
        AND  TABLE_NAME = 'SYS_GEO_[tblCountryCode]'))
BEGIN


    if(EXISTS (select  *
				    FROM SYS_GEO_[tblCountryCode]
			    where countryCode=@countryCode 
			    and (language=N'EN')
			    and (region1=@region or region2=@region or isnull(@region,'')='')
			    and (city=@city or isnull(@city,'')='')))
    begin
		
		    select  @MinPostcode=MIN([postcode])
			    FROM SYS_GEO_[tblCountryCode]
		    where countryCode=@countryCode 
		    and (language=N'EN')
		    and (region1=@region or region2=@region or isnull(@region,'')='')
		    and (city=@city or isnull(@city,'')='')
			
            select  @latitude=latitude, @longitude=longitude
            FROM SYS_GEO_[tblCountryCode]
            where countryCode=@countryCode
            and [postcode]=@MinPostcode
    end
    else    if(EXISTS (select  *
				    FROM SYS_GEO_[tblCountryCode]
			    where countryCode=@countryCode 
			    and (language=@language or isnull(@language,'')='')
			    and (region1=@region or region2=@region or isnull(@region,'')='')
			    and (city=@city or isnull(@city,'')='')))
    begin
		
		    select  @MinPostcode=MIN([postcode])
			    FROM SYS_GEO_[tblCountryCode]
		    where countryCode=@countryCode 
		    and (language=@language or isnull(@language,'')='')
		    and (region1=@region or region2=@region or isnull(@region,'')='')
		    and (city=@city or isnull(@city,'')='')
			
            select  @latitude=latitude, @longitude=longitude
            FROM SYS_GEO_[tblCountryCode]
            where countryCode=@countryCode
            and [postcode]=@MinPostcode
    end
    else
    begin
		
		
		    if(EXISTS (select  *
				    FROM SYS_GEO_[tblCountryCode]
			    where countryCode=@countryCode 
			    and (region1=@region or region2=@region or isnull(@region,'')='')
			    and (city=@city or isnull(@city,'')='')))
		    begin
				
				    select  @MinPostcode=MIN([postcode])
					    FROM SYS_GEO_[tblCountryCode]
				    where countryCode=@countryCode 
				    and (region1=@region or region2=@region or isnull(@region,'')='')
				    and (city=@city or isnull(@city,'')='')
					
                    select  @latitude=latitude, @longitude=longitude
                    FROM SYS_GEO_[tblCountryCode]
                    where countryCode=@countryCode
                    and [postcode]=@MinPostcode
		    end
		    else
		    begin
				
				
				    if(EXISTS (select  *
						    FROM SYS_GEO_[tblCountryCode]
					    where countryCode=@countryCode 
					    and (region1=@region or region2=@region or isnull(@region,'')='')))
				    begin
						
					    select  @MinPostcode=MIN([postcode])
						    FROM SYS_GEO_[tblCountryCode]
					    where countryCode=@countryCode 
					    and (region1=@region or region2=@region or isnull(@region,'')='')
						
                        select  @latitude=latitude, @longitude=longitude
                        FROM SYS_GEO_[tblCountryCode]
                        where countryCode=@countryCode
                        and [postcode]=@MinPostcode
				    end
				    else
				    begin
						    if(EXISTS (select  *
								    FROM SYS_GEO_[tblCountryCode]
							    where countryCode=@countryCode))
						    begin
								
							    select  @MinPostcode=MIN([postcode])
								    FROM SYS_GEO_[tblCountryCode]
							    where countryCode=@countryCode 
								
                                select  @latitude=latitude, @longitude=longitude
                                FROM SYS_GEO_[tblCountryCode]
                                where countryCode=@countryCode
                                and [postcode]=@MinPostcode

						    end
				    end
		    end
		
    end
END

if(isnull(@MinPostcode,'') ='')
begin 
	select @countryCode=N'GR'
		
	select  @MinPostcode=MIN([postcode])
		FROM SYS_GEO_GR
	where countryCode=N'GR' 

    select  @latitude=latitude, @longitude=longitude
    FROM SYS_GEO_GR
    where countryCode=@countryCode
    and [postcode]=@MinPostcode

end

select @MinPostcode as MinPostcode, @countryCode as countryCode, @latitude as latitude, @longitude as longitude

	
]]></sql>.Value

            sql = sql.Replace("[tblCountryCode]", countryCode)
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(con, sql)


                    command.Parameters.Add(New SqlClient.SqlParameter("@countryCode", countryCode))

                    If (Not String.IsNullOrEmpty(lagid)) Then
                        If (lagid = "GR") Then
                            command.Parameters.Add(New SqlClient.SqlParameter("@language", "EL"))
                        Else
                            command.Parameters.Add(New SqlClient.SqlParameter("@language", "EN"))
                        End If
                    Else
                        command.Parameters.Add(New SqlClient.SqlParameter("@language", System.DBNull.Value))
                    End If

                    If (Not String.IsNullOrEmpty(region)) Then
                        command.Parameters.Add(New SqlClient.SqlParameter("@region", region))
                    Else
                        command.Parameters.Add(New SqlClient.SqlParameter("@region", System.DBNull.Value))
                    End If

                    If (Not String.IsNullOrEmpty(region)) Then
                        command.Parameters.Add(New SqlClient.SqlParameter("@city", city))
                    Else
                        command.Parameters.Add(New SqlClient.SqlParameter("@city", System.DBNull.Value))
                    End If

                    Using dt = DataHelpers.GetDataTable(command)
                        Return dt
                    End Using
                End Using
            End Using
        Catch ex As Exception
            Return Nothing
        End Try


    End Function

    Public Shared Function GetCountryMinPostcodeDataTable_Position(countryCode As String, region As String, city As String, Optional lagid As String = Nothing) As clsGlobalPositionWCF
        Using dtGEO As DataTable = clsGeoHelper.GetCountryMinPostcodeDataTable(countryCode, region, city, lagid)


            Dim pos As New clsGlobalPositionWCF()
            If (dtGEO.Rows.Count > 0 AndAlso Not IsDBNull(dtGEO.Rows(0)("latitude")) AndAlso Not IsDBNull(dtGEO.Rows(0)("longitude"))) Then
                pos.latitude = clsNullable.DBNullToDecimal(dtGEO.Rows(0)("latitude"))
                pos.longitude = clsNullable.DBNullToDecimal(dtGEO.Rows(0)("longitude"))
            End If
            Return pos
        End Using
    End Function


    Public Shared Function GetGEOWithLatitudeAndLongitude(countryCode As String, latitude As String, longitude As String, lagid As String) As DataTable
        Dim sql As String = ""
        '    Dim postCode = ""


        Try

            '                sql = <sql><![CDATA[
            'select *
            '  FROM SYS_GEO_[countryCode]
            '   WHERE  language='@language' and [latitude] like @latitude+'%' and [longitude] like @longitude+'%'
            ']]></sql>.Value
            sql = <sql><![CDATA[
IF (EXISTS (SELECT * 
        FROM INFORMATION_SCHEMA.TABLES 
        WHERE TABLE_SCHEMA = 'dbo' 
        AND  TABLE_NAME = 'SYS_GEO_[tblCountryCode]'))
BEGIN


    if(EXISTS (select * from SYS_GEO_[tblCountryCode]
                where countryCode=@countryCode
                and [latitude] like @latitude+'%' and [longitude] like @longitude+'%'
                and (language=N'EN')))
    begin

        select * 
        from SYS_GEO_[tblCountryCode]
        where countryCode=@countryCode
        and [latitude] like @latitude+'%' and [longitude] like @longitude+'%'
        and (language=N'EN')
        order by postcode

    end
    else if(EXISTS (select * from SYS_GEO_[tblCountryCode]
                where countryCode=@countryCode
                and [latitude] like @latitude+'%' and [longitude] like @longitude+'%'
                and (language=@language or isnull(@language,'')='')))
    begin

        select * 
        from SYS_GEO_[tblCountryCode]
        where countryCode=@countryCode
        and [latitude] like @latitude+'%' and [longitude] like @longitude+'%'
        and (language=@language or isnull(@language,'')='')
        order by postcode

    end
    else
    begin

        select * 
        from SYS_GEO_[tblCountryCode]
        where countryCode=@countryCode
        and [latitude] like @latitude+'%' and [longitude] like @longitude+'%'
        order by postcode

    end
END
]]></sql>.Value

            sql = sql.Replace("[tblCountryCode]", countryCode)
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                    command.Parameters.Add(New SqlClient.SqlParameter("@latitude", latitude))
                    command.Parameters.Add(New SqlClient.SqlParameter("@longitude", longitude))
                    command.Parameters.Add(New SqlClient.SqlParameter("@countryCode", countryCode))

                    If (Not String.IsNullOrEmpty(lagid)) Then
                        If (lagid = "GR") Then
                            command.Parameters.Add(New SqlClient.SqlParameter("@language", "EL"))
                        Else
                            command.Parameters.Add(New SqlClient.SqlParameter("@language", "EN"))
                        End If
                    Else
                        command.Parameters.Add(New SqlClient.SqlParameter("@language", System.DBNull.Value))
                    End If

                    Using dt = DataHelpers.GetDataTable(command)

                        Return dt
                    End Using
                End Using
            End Using
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Shared Function GetGEOWithCity(countryCode As String, city As String) As DataTable
        Dim sql As String = ""
        '  Dim postCode = ""


        If (countryCode.ToUpper() = "GR") Then
            Try

                '                sql = <sql><![CDATA[
                'select  *
                '  FROM SYS_GEO_[countryCode]
                '   WHERE  city=@city
                ']]></sql>.Value
                sql = <sql><![CDATA[
IF (EXISTS (SELECT * 
        FROM INFORMATION_SCHEMA.TABLES 
        WHERE TABLE_SCHEMA = 'dbo' 
        AND  TABLE_NAME = 'SYS_GEO_[tblCountryCode]'))
BEGIN

    select  *
    FROM SYS_GEO_[tblCountryCode]
    WHERE  city=@city
    order by postcode

END
]]></sql>.Value

                sql = sql.Replace("[tblCountryCode]", countryCode)
                Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                    Using command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                        command.Parameters.Add(New SqlClient.SqlParameter("@city", city))

                        Using dt = DataHelpers.GetDataTable(command)
                            Return dt
                        End Using
                    End Using
                End Using
            Catch ex As Exception
                Return Nothing
            End Try
        End If

        Return Nothing

    End Function


    Public Shared Function GetPosition(countryCode As String, region As String, city As String, zip As String, Optional lagid As String = Nothing) As clsGlobalPosition
        Dim pos As New clsGlobalPosition()
        Using dt As DataTable = clsGeoHelper.GetGEOByZip(countryCode, zip, lagid)



            If (dt.Rows.Count > 0) Then

                pos.latitude = dt.Rows(0)("latitude")
                pos.longitude = dt.Rows(0)("longitude")

            Else

                Using dt1 = clsGeoHelper.GetCountryMinPostcodeDataTable(countryCode, region, city, lagid)



                    If (dt1.Rows.Count > 0) Then
                        If (Not IsDBNull(dt1.Rows(0)("MinPostcode"))) Then zip = dt1.Rows(0)("MinPostcode")
                        If (Not IsDBNull(dt1.Rows(0)("countryCode"))) Then countryCode = dt1.Rows(0)("countryCode")

                        Using dt2 = clsGeoHelper.GetGEOByZip(countryCode, zip, lagid)
                            pos.latitude = dt2.Rows(0)("latitude")
                            pos.longitude = dt2.Rows(0)("longitude")
                        End Using
                    End If
                End Using
            End If
        End Using
        Return pos
    End Function



End Class

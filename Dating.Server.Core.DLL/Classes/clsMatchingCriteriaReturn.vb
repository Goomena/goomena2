﻿Public Class MatcingCriteriaReturn
    Property SearchText As String = Nothing
    Property ageMinSelectedValue As Integer?
    Property ageMaxSelectedValue As Integer?
    Property HeightMinSelectedValue As Integer?
    Property HeightMaxSelectedValue As Integer?
    Property ddlDistanceSelectedValue As Integer?
    Property chkOnlineChecked As Boolean?
    Property chkPhotosChecked As Boolean?
    Property chkPhotosPrivateChecked As Boolean?
    Property chkWillingToTravelChecked As Boolean?
    Property SelectedHairColorList As SearchValuesInfo
    Property SelectedBreastSizeList As SearchValuesInfo
    Property SelectedEyeColorList As SearchValuesInfo
    Property SelectedBodyTypeList As SearchValuesInfo
    Property SelectedRelationShipStatusList As SearchValuesInfo
    Property SelectedTypeOfDatingList As SearchValuesInfo
    Property SelectedEducationList As SearchValuesInfo
    Property SelectedChildrenList As SearchValuesInfo
    Property SelectedEthnicityList As SearchValuesInfo
    Property SelectedSmokingHabitsList As SearchValuesInfo
    Property SelectedDrinkingHabitsList As SearchValuesInfo
    Property SelectedReligionList As SearchValuesInfo
    Property SelectedLanguagesList As SearchValuesInfo
End Class
Public Class SearchValuesInfo
    Public Function getDbValue() As Object
        If Values <> "" AndAlso Values IsNot Nothing Then
            Return Values
        Else
            Return DBNull.Value
        End If
    End Function
    Public Property Values As String = Nothing
    Public Property IsAllSelected As Boolean = False
End Class
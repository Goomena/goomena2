﻿<Serializable()> _
Public Class clsCountryByIP
    Public Property CountryCode As String
    Public Property RegionCode As String
    Public Property city As String
    Public Property latitude As String
    Public Property longitude As String
    Public Property postalCode As String
    Public Property DataRecord As DataTable
    Public Shared Function GetCountryCodeFromIP(ip As String, connectionString As String) As clsCountryByIP
        Dim country As New clsCountryByIP()
        Dim output As String = Dot2LongIP(ip)
        Dim __tmp As String = "US"

        Dim sql As String = "EXEC GetCountryFromIP @ip=@ipIn"
        Using con As SqlClient.SqlConnection = New SqlClient.SqlConnection(connectionString)


            Using cmd As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                cmd.Parameters.AddWithValue("@ipIn", output)
                Using dt As DataTable = DataHelpers.GetDataTable(cmd)
                    If (dt.Rows.Count > 0) Then
                        Dim dr As DataRow = dt.Rows(0)
                        country.DataRecord = dt

                        __tmp = dr("country")
                        __tmp = __tmp.Trim("""")
                        country.CountryCode = __tmp

                        __tmp = dr("Region")
                        __tmp = __tmp.Trim("""")
                        country.RegionCode = __tmp

                        __tmp = dr("city")
                        __tmp = __tmp.Trim("""")
                        country.city = __tmp

                        __tmp = dr("latitude")
                        __tmp = __tmp.Trim("""")
                        country.latitude = __tmp

                        __tmp = dr("longitude")
                        __tmp = __tmp.Trim("""")
                        country.longitude = __tmp

                        __tmp = dr("postalCode")
                        __tmp = __tmp.Trim("""")
                        country.postalCode = __tmp
                    Else
                        Throw New IPNotFoundException("Requested IP (" & ip & ") address not found in GEO Locations.")
                    End If
                End Using
            End Using
        End Using






        Return country
    End Function

    Public Shared Function Dot2LongIP(ByVal DottedIP As String) As Double
        Dim arrDec() As String
        Dim i As Integer
        Dim intResult As Long
        If DottedIP = "" Then
            Dot2LongIP = 0
        Else
            arrDec = DottedIP.Split(".")
           
            For i = arrDec.Length - 1 To 0 Step -1
                If Not arrDec(i) = "::1" Then
                    intResult = intResult + ((Int(arrDec(i)) Mod 256) * Math.Pow(256, 3 - i))
                End If
            Next
            Dot2LongIP = intResult
        End If
    End Function

End Class

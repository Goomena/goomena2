﻿Imports System.Web
Imports System.Configuration
Imports System.Text

Public Class AppUtils

    Public Const gEmailAddressValidationRegex As String = "^[\w\.=-]+@[a-zA-Z0-9_-][\w\.-_]*[a-zA-Z0-9_-]\.[a-zA-Z_][a-zA-Z\._]*[a-zA-Z]$"
    Public Const gEmailAddressValidationRegex_Simple As String = "^.*@.*$"
    '"^[a-zA-Z0-9_-][\w\.-_]*[a-zA-Z0-9_-]@[a-zA-Z0-9_-][\w\.-_]*[a-zA-Z0-9_-]\.[a-zA-Z_][a-zA-Z\._]*[a-zA-Z]$"
    '"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
    '"^[\w\.=-]+@[\w\.-]+\.[\w]{2,3}$"


    Public Shared Function FormatFileSize(ByVal FileSizeBytes As Long) As String
        Dim sizeTypes() As String = {"B", "KB", "MB", "GB"}
        Dim Len As Decimal = FileSizeBytes
        Dim sizeType As Integer = 0
        Do While Len > 1024
            Len = Decimal.Round(Len / 1024, 2)
            sizeType += 1
            If sizeType >= sizeTypes.Length - 1 Then Exit Do
        Loop

        Dim Resp As String = Len.ToString & " " & sizeTypes(sizeType)
        Return Resp
    End Function


    Public Shared Function FormatFileSize2(ByVal Size As Long) As String
        Try
            Dim KB As Integer = 1024
            Dim MB As Integer = KB * KB
            ' Return size of file in kilobytes.
            If Size < KB Then
                Return (Size.ToString("D") & " bytes")
            Else
                Select Case Size / KB
                    Case Is < 1000
                        Return (Size / KB).ToString("N") & "KB"
                    Case Is < 1000000
                        Return (Size / MB).ToString("N") & "MB"
                    Case Is < 10000000
                        Return (Size / MB / KB).ToString("N") & "GB"
                End Select
            End If
        Catch ex As Exception
        End Try

        Return Size.ToString
    End Function

    Public Shared Function FormatFileSizeMB(ByVal Size As Long) As String
        Try
            Dim KB As Integer = 1024
            '  Dim MB As Integer = KB * KB
            ' Return size of file in kilobytes.
            Return (Size / KB).ToString("N") & "GB"
        Catch ex As Exception
        End Try

        Return Size.ToString
    End Function

    Public Shared Function IsNull_ToString(o As Object, Optional defaultValue As String = "") As String
        If (o IsNot System.DBNull.Value) Then
            Return o.ToString()
        Else
            Return defaultValue
        End If
    End Function

    ''' <summary>
    ''' Strip HTML tags.
    ''' </summary>
    Public Shared Function StripTags(ByVal html As String) As String
        ' Remove HTML tags.
        Dim str As String = Text.RegularExpressions.Regex.Replace(html, "&nbsp;", " ")
        str = Text.RegularExpressions.Regex.Replace(str, "&euro;", "EUR")
        str = Text.RegularExpressions.Regex.Replace(str, "<.*?>", "")
        'str = Text.RegularExpressions.Regex.Replace(str, "(&([^&]+);)", "")
        Return str
    End Function

    Public Shared Function StripHTML(ByVal html As String) As String
        ' Remove HTML tags.
        Dim str As String = Text.RegularExpressions.Regex.Replace(html, "&nbsp;", " ")
        str = Text.RegularExpressions.Regex.Replace(str, "&euro;", "EUR")
        str = Text.RegularExpressions.Regex.Replace(str, "<.*?>", "")
        str = Text.RegularExpressions.Regex.Replace(str, "(&([^&]+);)", "")
        Return str
    End Function



    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")> Public Shared Function StringToStream(ByVal str As String) As System.IO.MemoryStream
        Dim ms As New System.IO.MemoryStream
        Dim enc As New System.Text.UTF8Encoding
        Dim arrBytData() As Byte = enc.GetBytes(str)
        ms.Write(arrBytData, 0, arrBytData.Length)
        ms.Position = 0
        Return ms
    End Function

    Public Shared Function MakeMD5(ByVal str As String) As String
        Using o As New System.Security.Cryptography.MD5CryptoServiceProvider()
            str = (BitConverter.ToString(o.ComputeHash(AppUtils.StringToStream(str)))).Trim.ToUpper.Replace("-", "").Trim
        End Using
        Return str
    End Function

    Public Shared Function MD5_GetString(ByVal str As String) As String
        Using o As New System.Security.Cryptography.MD5CryptoServiceProvider()
            str = (BitConverter.ToString(o.ComputeHash(AppUtils.StringToStream(str)))).Trim.ToUpper.Replace("-", "").Trim
        End Using
        Return str
    End Function


    Public Shared Function MD5_GetGuid(ByVal str As String) As Guid
        'step 1, calculate MD5 hash from input
        Dim hash As Byte()
        Using md5 As System.Security.Cryptography.MD5 = System.Security.Cryptography.MD5.Create()


            Dim inputBytes As Byte() = System.Text.Encoding.ASCII.GetBytes(str)
            hash = md5.ComputeHash(inputBytes)
        End Using
        ' step 2, convert byte array to hex string
        Dim guid As New Guid(hash)
        Return guid
    End Function



    Public Shared Function StreamToBytesArray(ByRef stream As System.IO.Stream) As Byte()
        Dim streamLength As Integer = Convert.ToInt32(stream.Length)
        Dim fileData As Byte() = New Byte(streamLength) {}
        stream.Read(fileData, 0, streamLength)
        Return fileData
    End Function


    Public Shared Function GetZip(input As String) As String
        If (Not String.IsNullOrEmpty(input)) Then
            input = input.Trim()
            input = input.Replace(" ", "").Replace("-", "")
            input = input.ToUpper()
            Dating.Server.Core.DLL.ClsSQLInjectionClear.ClearString(input, 10)
        End If
        Return input
    End Function


    '    Imports System.Text
    'Imports System.Security.Cryptography

    '    Private Function GenerateHash(ByVal SourceText As String) As String
    '        'Create an encoding object to ensure the encoding standard for the source text
    '        Dim Ue As New UnicodeEncoding()
    '        'Retrieve a byte array based on the source text
    '        Dim ByteSourceText() As Byte = Ue.GetBytes(SourceTStext)
    '        'Instantiate an MD5 Provider object
    '        Dim Md5 As New MD5CryptoServiceProvider()
    '        'Compute the hash value from the source
    '        Dim ByteHash() As Byte = Md5.ComputeHash(ByteSourceText)
    '        'And convert it to String format for return
    '        Return Convert.ToBase64String(ByteHash)
    '    End Function



    ''' <summary>
    ''' Append to file
    ''' </summary>
    ''' <param name="file_name">file path</param>
    ''' <param name="message">string to add</param>
    ''' <remarks></remarks>
    Public Shared Sub Append_File(ByVal file_name As String, ByVal message As String)
        Dim numberofretrys As Integer = 5
        Dim trys As Integer = 0
retry:

        Try
            If Not IO.File.Exists(file_name) Then
                Try
                    IO.File.WriteAllText(file_name, "")
                Catch ex As Exception

                End Try
            End If
        Catch ex As Exception

        End Try

        Try

            If Not String.IsNullOrEmpty(file_name) Then
                Using objWriter As New System.IO.StreamWriter(file_name, True)
                    objWriter.WriteLine(message)
                    ' objWriter.Close()
                End Using
             
            End If
        Catch ex As Exception
            trys += 1
            If trys < numberofretrys Then
                Threading.Thread.Sleep(2000)
                GoTo retry
            End If
        End Try
    End Sub





    Public Shared Sub WebErrorSendEmail(ByVal ex As Exception, ByVal ExtraMessage As String)

        Dim gSiteName As String = ConfigurationManager.AppSettings("gSiteName")
        Dim gToEmail As String = ConfigurationManager.AppSettings("gToEmail")
        Dim sendException As Boolean = True

        If (String.IsNullOrEmpty(gSiteName)) Then
            If (HttpContext.Current IsNot Nothing) Then
                gSiteName = HttpContext.Current.Request.Url.Host

                If (HttpContext.Current.Request.Url.Host = "localhost") Then sendException = False
            Else
                gSiteName = ""
            End If
        End If

        If (sendException) Then
            Dim msg As String = ex.Message
            If (msg.Length > 200) Then msg = msg.Remove(197) & "..."
            msg = HttpUtility.UrlEncode(msg)
            Dim subject As String = "Exception on " & gSiteName & " (" & msg & ")"

            Dim contetn As String = ""
            If (HttpContext.Current IsNot Nothing AndAlso Not String.IsNullOrEmpty(HttpContext.Current.User.Identity.Name)) Then
                contetn = ExtraMessage & vbCrLf & "Login name: " & HttpContext.Current.User.Identity.Name & vbCrLf & ex.ToString()
            Else
                contetn = ExtraMessage & vbCrLf & vbCrLf & ex.ToString()
            End If

            If (HttpContext.Current IsNot Nothing) Then
                contetn = contetn & vbCrLf & vbCrLf & "URL: " & HttpContext.Current.Request.Url.ToString() & vbCrLf
                contetn = contetn & "IP: " & HttpContext.Current.Request.ServerVariables("REMOTE_ADDR") & vbCrLf

                If (HttpContext.Current.Session IsNot Nothing) Then
                    contetn = contetn & "LAGID: " & HttpContext.Current.Session("LAGID") & vbCrLf
                End If
            End If

            Dating.Server.Core.DLL.clsMyMail.SendMailSupport(gToEmail, subject, contetn)
        End If
    End Sub


    Public Shared Function GetDoubleValue_FromStringWithPeriod(Val As String) As Double
        Dim result As Double = 0
        If (Not String.IsNullOrEmpty(Val)) Then

            Dim cult As New System.Globalization.CultureInfo("en-US")
            Dim dblLat As Double = 0
            If (Double.TryParse(Val, System.Globalization.NumberStyles.AllowDecimalPoint Or System.Globalization.NumberStyles.AllowLeadingSign, cult.NumberFormat, dblLat)) Then
                result = dblLat
            Else
                Val = Val.Replace(".", ",")
                If (Double.TryParse(Val, System.Globalization.NumberStyles.AllowDecimalPoint Or System.Globalization.NumberStyles.AllowLeadingSign, cult.NumberFormat, dblLat)) Then
                    result = dblLat
                Else
                    Val = Val.Replace(",", ".")
                    If (Double.TryParse(Val, System.Globalization.NumberStyles.AllowDecimalPoint Or System.Globalization.NumberStyles.AllowLeadingSign, cult.NumberFormat, dblLat)) Then
                        result = dblLat
                    End If
                End If
            End If

        End If
        Return result
    End Function


    Public Shared Function CreateAlphaNumericCode(length As Integer, Optional retries As Integer = -1) As String
        Dim st As String = Guid.NewGuid().ToString("N")
        Dim com As String = ""

        While (com.Length < length)
            For x = 0 To st.Length - 1 Step 2
                Dim k As String = st.Substring(x, 2)
                Dim asciiNum As UInteger = System.Convert.ToUInt32(k, 16)
                If (asciiNum >= 65 AndAlso asciiNum <= 90) OrElse (asciiNum >= 97 AndAlso asciiNum <= 122) OrElse (asciiNum >= 48 AndAlso asciiNum <= 57) Then '
                    com &= System.Convert.ToChar(asciiNum).ToString()

                    If (com.Length = length) Then Exit For
                End If
            Next
            st = Guid.NewGuid().ToString("N")

            If (retries > -1) Then
                If (retries = 0) Then Exit While
                If (retries > 0) Then retries = retries - 1
            End If

        End While

        Return com.ToUpper()
    End Function


    Public Shared Function ConvertObjectToXML(item As Object, type As System.Type) As String
        Dim result As String = ""
        Try

            Using xmlresult As New System.IO.StringWriter()
                Dim x As New System.Xml.Serialization.XmlSerializer(type)
                x.Serialize(xmlresult, item)
                result = xmlresult.ToString()
            End Using
          

        Catch
        End Try

        Return result
    End Function



End Class

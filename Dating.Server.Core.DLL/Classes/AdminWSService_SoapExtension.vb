﻿Imports System
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.IO
Imports ICSharpCode.SharpZipLib.Core
Imports ICSharpCode.SharpZipLib.GZip
Imports System.IO.Compression


Public Class AdminWSService_SoapExtension
    Inherits SoapExtension

    Private OriginalStream As Stream
    Private NewStream As Stream

    Public Overloads Overrides Function GetInitializer(ByVal methodInfo As LogicalMethodInfo, ByVal attribute As SoapExtensionAttribute) As Object
        Return System.DBNull.Value
    End Function

    Public Overloads Overrides Function GetInitializer(ByVal WebServiceType As Type) As Object
        Return System.DBNull.Value
    End Function

    Public Overrides Sub Initialize(ByVal initializer As Object)
    End Sub


    ' Private ApplyComression As Boolean
    ' Private CurrentServiceURL As String

    'Private Shared ServicesCompressionEnabled As Dictionary(Of String, Boolean)

    'Shared Sub New()
    '    ServicesCompressionEnabled = New Dictionary(Of String, Boolean)
    'End Sub

    Public Overrides Sub ProcessMessage(ByVal message As SoapMessage)
        Select Case message.Stage

            Case SoapMessageStage.BeforeSerialize

            Case SoapMessageStage.AfterSerialize
                AfterSerialize(message)

            Case SoapMessageStage.BeforeDeserialize
                BeforeDeserialize(message)

            Case SoapMessageStage.AfterDeserialize
        End Select
    End Sub

    ' Save the stream representing the SOAP request or SOAP response into a
    ' local memory buffer.
    Public Overrides Function ChainStream(ByVal stream As Stream) As Stream
        OriginalStream = stream
        NewStream = New MemoryStream()
        Return NewStream
    End Function

    ' Write the compressed SOAP message out to a file at
    'the server's file system..
    Public Sub AfterSerialize(ByRef message As SoapMessage)
        Using ms As New MemoryStream
            NewStream.Position = 0
            Copy(NewStream, ms)
            NewStream.Position = 0
            ms.Position = 0
            Using ms2 = CompressData(ms)
                ms2.Position = 0
                NewStream.Position = 0
                Copy(ms2, OriginalStream)
                ms2.Position = 0
            End Using
        End Using
    End Sub

    ' Write the SOAP request message out to a file at the server's file system.
    Public Sub BeforeDeserialize(ByRef message As SoapMessage)
        Using ms As Stream = New MemoryStream
            Copy(OriginalStream, ms)
            ms.Position = 0
            Using ms2 = DeCompressData(ms)
                ms2.Position = 0
                NewStream.Position = 0
                Copy(ms2, NewStream)
                ms2.Position = 0
                NewStream.Position = 0
            End Using
        End Using
    End Sub

    Shared Sub Copy(ByRef fromStream As Stream, ByRef toStream As Stream)
        Dim bytesRead As Integer
        Dim buffer As Byte() = New Byte(1) {}
        Using reader As New BinaryReader(fromStream)
            Using writer As New BinaryWriter(toStream)
                Do
                    bytesRead = reader.Read(buffer, 0, buffer.Length)
                    writer.Write(buffer, 0, bytesRead)
                Loop While bytesRead > 0
                writer.Flush()
            End Using
        End Using
    End Sub

    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")> Public Shared Function CompressData(ByRef source As MemoryStream) As MemoryStream
        If source Is Nothing Then
            Return Nothing
        End If
        Dim ms As New MemoryStream()

        Try
            Dim buffer As Byte() = New Byte(source.Length - 1) {}
            source.Position = 0
            source.Read(buffer, 0, buffer.Length)
            Using compressedzipStream = New GZipStream(ms, CompressionMode.Compress, True)
                compressedzipStream.Write(buffer, 0, buffer.Length)
            End Using

        Finally
        End Try
        Return ms
    End Function



    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")> Public Shared Function DeCompressData(ByRef inputStream As Stream) As MemoryStream
        If inputStream Is Nothing Then
            Return Nothing
        End If
        inputStream.Position = 0
        Dim result As New MemoryStream()
        Using gs As New GZipStream(inputStream, CompressionMode.Decompress)
            Using reader As New BinaryReader(gs)
                Using writer As New BinaryWriter(result)
                    Dim bytesRead As Integer
                    Dim buffer As Byte() = New Byte(1) {}

                    Try
                        Do
                            bytesRead = reader.Read(buffer, 0, buffer.Length)
                            writer.Write(buffer, 0, bytesRead)
                        Loop While bytesRead > 0
                        writer.Flush()
                        result.Position = 0
                    Catch ex As System.IO.InvalidDataException
                        Return inputStream
                    Finally
                    End Try
                End Using
            End Using
        End Using
        Return result
    End Function


End Class



' Create a SoapExtensionAttribute for the SOAP extension that can be
' applied to an XML Web service method.
<AttributeUsage(AttributeTargets.Method)> _
Public Class AdminWSService_SoapExtensionAttribute
    Inherits SoapExtensionAttribute

    Public Overrides ReadOnly Property ExtensionType() As Type
        Get
            Return GetType(AdminWSService_SoapExtensionAttribute)
        End Get
    End Property

    Public Overrides Property Priority() As Integer
        Get
            Return 1
        End Get
        Set(ByVal Value As Integer)
        End Set
    End Property

End Class
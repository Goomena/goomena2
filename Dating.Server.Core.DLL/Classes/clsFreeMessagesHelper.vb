﻿Imports System.Data.SqlClient

Public Class clsFreeMessagesHelper

    Public WomanProfileId As Integer
    Public MessagesSentToProfile As Integer
    Public MessagesReadFromProfile As Integer
    Public HasProfileConversation As Boolean
    Public TotalMessagesSent As Integer
    Public TotalMessagesRead As Integer
    Public ProfilesSentCount As Integer
    Public ProfilesReadCount As Integer
    Public IsProfilesSentCountLimit As Boolean
    Public IsProfilesReadCountLimit As Boolean
    Public Credits_FreeMessagesSend As Integer
    Public Credits_FreeMessagesSendToProfile As Integer
    Public Credits_FreeMessagesRead As Integer
    Public Credits_FreeMessagesReadFromProfile As Integer
    Public Credits_MaxWomanProfilesSend As Integer
    Public Credits_MaxWomanProfilesRead As Integer

    '                      



    Public Shared Function GetData(MasterProfileId As Integer, WomanProfileId As Integer, Country As String) As clsFreeMessagesHelper
        Dim sql As String = <sql><![CDATA[exec [CustomerFreeMessagesAvailable] @ManProfileId=@ManProfileId, @WomanProfileId=@WomanProfileId, @ManCountry=@ManCountry]]></sql>
        Dim cmd As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql)
        cmd.Parameters.AddWithValue("@ManProfileId", MasterProfileId)
        cmd.Parameters.AddWithValue("@WomanProfileId", WomanProfileId)
        cmd.Parameters.AddWithValue("@ManCountry", Country)
        Dim dt As DataTable = DataHelpers.GetDataTable(cmd)
        Dim cls As New clsFreeMessagesHelper()
        If (dt.Rows.Count > 0) Then
            cls.WomanProfileId = WomanProfileId
            cls.MessagesSentToProfile = System.Convert.ToInt32(dt.Rows(0)("MessagesSentToProfile"))
            cls.MessagesReadFromProfile = System.Convert.ToInt32(dt.Rows(0)("MessagesReadFromProfile"))
            cls.HasProfileConversation = System.Convert.ToBoolean(dt.Rows(0)("HasProfileConversation"))
            cls.TotalMessagesSent = System.Convert.ToInt32(dt.Rows(0)("TotalMessagesSent"))
            cls.TotalMessagesRead = System.Convert.ToInt32(dt.Rows(0)("TotalMessagesRead"))
            cls.ProfilesSentCount = System.Convert.ToInt32(dt.Rows(0)("ProfilesSentCount"))
            cls.ProfilesReadCount = System.Convert.ToInt32(dt.Rows(0)("ProfilesReadCount"))
            cls.IsProfilesSentCountLimit = System.Convert.ToBoolean(dt.Rows(0)("IsProfilesSentCountLimit"))
            cls.IsProfilesReadCountLimit = System.Convert.ToBoolean(dt.Rows(0)("IsProfilesReadCountLimit"))
            cls.Credits_FreeMessagesSend = System.Convert.ToInt32(dt.Rows(0)("Credits_FreeMessagesSend"))
            cls.Credits_FreeMessagesSendToProfile = System.Convert.ToInt32(dt.Rows(0)("Credits_FreeMessagesSendToProfile"))
            cls.Credits_FreeMessagesRead = System.Convert.ToInt32(dt.Rows(0)("Credits_FreeMessagesRead"))
            cls.Credits_FreeMessagesReadFromProfile = System.Convert.ToInt32(dt.Rows(0)("Credits_FreeMessagesReadFromProfile"))
            cls.Credits_MaxWomanProfilesSend = System.Convert.ToInt32(dt.Rows(0)("Credits_MaxWomanProfilesSend"))
            cls.Credits_MaxWomanProfilesRead = System.Convert.ToInt32(dt.Rows(0)("Credits_MaxWomanProfilesRead"))
        End If

        Return cls
    End Function

End Class

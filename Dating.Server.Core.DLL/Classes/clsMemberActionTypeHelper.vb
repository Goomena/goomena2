﻿Public Class clsMemberActionTypeHelper

    Public Shared Function GetIntegerValues(actionType As MemberActionTypeEnum) As List(Of Integer)
        Dim ints As New List(Of Integer)
        If (actionType > MemberActionTypeEnum.None) Then
            If ((actionType And MemberActionTypeEnum.Dates) > 0) Then ints.Add(actionType)
            If ((actionType And MemberActionTypeEnum.Favorite) > 0) Then ints.Add(actionType)
            If ((actionType And MemberActionTypeEnum.Likes) > 0) Then ints.Add(actionType)
            If ((actionType And MemberActionTypeEnum.Message) > 0) Then ints.Add(actionType)
            If ((actionType And MemberActionTypeEnum.Offer) > 0) Then ints.Add(actionType)
            If ((actionType And MemberActionTypeEnum.Poke) > 0) Then ints.Add(actionType)
            If ((actionType And MemberActionTypeEnum.Unfavorite) > 0) Then ints.Add(actionType)
            If ((actionType And MemberActionTypeEnum.Viewed) > 0) Then ints.Add(actionType)
        End If
        Return ints
    End Function

End Class

﻿Imports System.Data.SqlClient

Public Class clsSubscriptionMessagesHelper

    Public WomanProfileId As Integer
    Public TotalMessagesAvailableForProfile As Integer
    Public TotalMessagesAvailable As Integer
    Public MessagesPerDayUsed As Integer
    Public MessagesToProfilePerDayUsed As Integer
    Public MessagesPerDayAvailable As Integer
    Public IsProfilesCountLimit As Integer
    Public HasProfileConversationToday As Boolean
    Public ProfilesCount As Boolean
    Public Subscription_MessagesToProfilePerDay As Integer
    Public Subscription_MessagesPerDay As Integer
    Public Subscription_MaxWomanProfiles As Integer
    Public DateFrom As DateTime
    Public DateTo As DateTime



    Public Shared Function GetData(MasterProfileId As Integer, WomanProfileId As Integer, Country As String) As clsSubscriptionMessagesHelper
        Dim sql As String = <sql><![CDATA[exec [CustomerSubscriptionAvailableMessages] @ManProfileId=@ManProfileId, @WomanProfileId=@WomanProfileId, @ManCountry=@ManCountry]]></sql>
        Dim cmd As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql)
        cmd.Parameters.AddWithValue("@ManProfileId", MasterProfileId)
        cmd.Parameters.AddWithValue("@WomanProfileId", WomanProfileId)
        cmd.Parameters.AddWithValue("@ManCountry", Country)
        Dim dt As DataTable = DataHelpers.GetDataTable(cmd)
        Dim cls As New clsSubscriptionMessagesHelper()
        If (dt.Rows.Count > 0) Then
            cls.WomanProfileId = WomanProfileId
            cls.TotalMessagesAvailableForProfile = System.Convert.ToInt32(dt.Rows(0)("TotalMessagesAvailableForProfile"))
            cls.TotalMessagesAvailable = System.Convert.ToInt32(dt.Rows(0)("TotalMessagesAvailable"))
            cls.MessagesPerDayUsed = System.Convert.ToInt32(dt.Rows(0)("MessagesPerDayUsed"))
            cls.MessagesToProfilePerDayUsed = System.Convert.ToInt32(dt.Rows(0)("MessagesToProfilePerDayUsed"))
            cls.MessagesPerDayAvailable = System.Convert.ToInt32(dt.Rows(0)("MessagesPerDayAvailable"))
            cls.HasProfileConversationToday = System.Convert.ToBoolean(dt.Rows(0)("HasProfileConversationToday"))
            cls.IsProfilesCountLimit = System.Convert.ToBoolean(dt.Rows(0)("IsProfilesCountLimit"))
            cls.ProfilesCount = System.Convert.ToInt32(dt.Rows(0)("ProfilesCount"))
            cls.Subscription_MessagesToProfilePerDay = System.Convert.ToInt32(dt.Rows(0)("Subscription_MessagesToProfilePerDay"))
            cls.Subscription_MessagesPerDay = System.Convert.ToInt32(dt.Rows(0)("Subscription_MessagesPerDay"))
            cls.Subscription_MaxWomanProfiles = System.Convert.ToInt32(dt.Rows(0)("Subscription_MaxWomanProfiles"))
            cls.DateFrom = System.Convert.ToDateTime(dt.Rows(0)("DateFrom"))
            cls.DateTo = System.Convert.ToDateTime(dt.Rows(0)("DateTo"))
        End If

        Return cls
    End Function

End Class

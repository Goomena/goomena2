﻿Imports FilesProxy.DownloadSites.Web
Imports System.Net
Imports System.IO
Imports System.Text.RegularExpressions



Public Class clsWebPageProcessor

   
    Public Shared Function GetPageContent(webPageUrl As String, Optional timeOut As Integer = 0) As String
        Dim br As SimpleBrowser = New SimpleBrowser()
        Dim page As WebPageUrl = New WebPageUrl(webPageUrl)
        If (timeOut > 0) Then br.Timeout = timeOut
        Using webresponse As HttpWebResponse = br.OpenResponse(page)
            Dim str As String
            Using sr As StreamReader = New StreamReader(webresponse.GetResponseStream())
                str = sr.ReadToEnd()
            End Using
            Return str
        End Using
    End Function


    Public Shared Function GetPageContentByPost(webPageUrl As String, Parameters As System.Collections.Generic.Dictionary(Of String, String)) As String
        Dim br As SimpleBrowser = New SimpleBrowser()

        'Dim Inputs As System.Collections.Specialized.NameValueCollection = ReadParametersFromURL(webPageUrl)
        'Dim newUrl As String = webPageUrl
        'If (newUrl.IndexOf("?") > -1) Then newUrl = newUrl.Remove(newUrl.IndexOf("?"))

        Dim page As WebPageUrl = New WebPageUrl(webPageUrl)
        page.UsePost = True

        For Each itm In Parameters.Keys
            page.Parameters.Add(itm, Parameters(itm))
        Next

        Using webresponse As HttpWebResponse = br.OpenResponse(page)
            Dim str As String
            Using sr As StreamReader = New StreamReader(webresponse.GetResponseStream())
                str = sr.ReadToEnd()
            End Using
            Return str
        End Using
    End Function

    Public Shared Function ReadParametersFromURL(url As String) As System.Collections.Specialized.NameValueCollection
        Dim Inputs As System.Collections.Specialized.NameValueCollection = New System.Collections.Specialized.NameValueCollection
        Dim matches As System.Text.RegularExpressions.MatchCollection = System.Text.RegularExpressions.Regex.Matches(url, "[\?&](?<name>[^&=]+)=(?<value>[^&=]+)")

        For Each m As System.Text.RegularExpressions.Match In matches
            Dim name As String = m.Groups(1).Value
            Dim value As String = m.Groups(2).Value
            value = System.Web.HttpUtility.UrlDecode(value)
            Inputs.Add(name, value)
        Next
        Return Inputs
    End Function


    Public Shared Function GetWebResponse(webPageUrl As String, Optional timeOut As Integer = 0) As HttpWebResponse
        Dim br As SimpleBrowser = New SimpleBrowser()

        Dim page As WebPageUrl = New WebPageUrl(webPageUrl)
        If (timeOut > 0) Then br.Timeout = timeOut
        Return br.OpenResponse(page)
    End Function



    Public Shared Function GetWebResponseHead(webPageUrl As String) As HttpWebResponse
        Dim request As HttpWebRequest = CType(WebRequest.Create(webPageUrl), HttpWebRequest)

        request.Method = WebRequestMethods.Http.Head

        Return request.GetResponse()

    End Function




End Class

﻿Public Class clsReturnSmsLimitations
    Private _PeriodInDays As Integer = 1
    Public Property PeriodInDays As Integer
        Get
            Return _PeriodInDays
        End Get
        Set(value As Integer)
            If value >= 1 Then
                _PeriodInDays = value
            End If
        End Set
    End Property
    Private _SmsCountLimitation As Integer = 1
    Public Property SmsCountLimitation As Integer
        Get
            Return _SmsCountLimitation
        End Get
        Set(value As Integer)
            If value >= 1 Then
                _SmsCountLimitation = value
            End If
        End Set
    End Property
    Public Enabled As Boolean = False
End Class

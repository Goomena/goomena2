﻿Imports System.Net.Mail
Imports System.Net



Public Class clsMyMail

    Public Class clsMailData
        Public ToAddress As String
        Public Subject As String
        Public Content As String
        Public IsHtmlBody As Boolean
        Public BCCRecipients As String
        Public IsSupportMail As Boolean = False
    End Class


    Public Class SMTPSettings
        Public gSMTPServerName As String
        Public gSMTPLoginName As String
        Public gSMTPPassword As String
        Public gSMTPOutPort As String
        Public gSMTPenableSsl As Boolean
    End Class

    Public Shared MySMTPSettings As New SMTPSettings
    Public Shared MySMTPSettingsSupport As New SMTPSettings
    Shared Sub New()
        MySMTPSettings = New clsMyMail.SMTPSettings()
        MySMTPSettingsSupport = New clsMyMail.SMTPSettings()
    End Sub

    ''' <summary>
    ''' Try  Send email using system.net/mailSettings configuration of web.config
    ''' If for any reason, email server is unavailable
    ''' </summary>
    ''' <remarks></remarks>
    Shared Function TrySendMail(mailData As clsMailData)

        Dim success As Boolean

        Try
            

            success = TrySendMail(mailData.ToAddress,
                         mailData.Subject,
                         mailData.Content,
                         mailData.IsHtmlBody,
                      mailData.BCCRecipients,
                      mailData.IsSupportMail
                     )
        Catch
            success = False
        End Try

        Return success
    End Function

    ''' <summary>
    ''' Try  Send email using system.net/mailSettings configuration of web.config
    ''' If for any reason, email server is unavailable
    ''' </summary>
    ''' <param name="toAddress"></param>
    ''' <param name="subject"></param>
    ''' <param name="content"></param>
    ''' <remarks></remarks>
    Shared Function TrySendMail(toAddress As String,
                       subject As String,
                       content As String,
                       Optional IsHtmlBody As Boolean = False,
                       Optional BCCRecipients As String = Nothing,
                         Optional IsSupportMail As Boolean = False)

        Dim success As Boolean

        Try
            If Not IsSupportMail Then
                SendMail(toAddress,
                                    subject,
                                    content,
                                     IsHtmlBody,
                                     BCCRecipients)
            Else
                SendMailSupport(toAddress,
                    subject,
                    content,
                     IsHtmlBody,
                     BCCRecipients)
            End If

            success = True
        Catch
            success = False
        End Try

        Return success
    End Function
    Shared Function TrySendMailSupport(toAddress As String,
                       subject As String,
                       content As String,
                       Optional IsHtmlBody As Boolean = False,
                       Optional BCCRecipients As String = Nothing)

        Dim success As Boolean

        Try


            SendMailSupport(toAddress,
                subject,
                content,
                 IsHtmlBody,
                 BCCRecipients)


            success = True
        Catch
            success = False
        End Try

        Return success
    End Function

    ''' <summary>
    '''  Sends email using system.net/mailSettings configuration of web.config
    ''' </summary>
    ''' <param name="toAddress"></param>
    ''' <param name="subject"></param>
    ''' <param name="content"></param>
    ''' <remarks></remarks>
    Shared Sub SendMail(toAddress As String,
                        subject As String,
                        content As String,
                        Optional IsHtmlBody As Boolean = False,
                        Optional BCCRecipients As String = Nothing)

        '(1) Create the MailMessage instance
        Using mm As New MailMessage()



            '(2) Assign the MailMessage's properties
            mm.Subject = subject
            mm.Body = content
            mm.IsBodyHtml = IsHtmlBody
            mm.To.Add(toAddress)
            mm.From = New System.Net.Mail.MailAddress(MySMTPSettings.gSMTPLoginName)
            If (Not String.IsNullOrEmpty(BCCRecipients)) Then
                If (BCCRecipients.Trim().Length > 5) Then
                    Dim bccArr As String() = BCCRecipients.Split(";")
                    For c1 = 0 To bccArr.Length - 1
                        mm.Bcc.Add(bccArr(c1))
                    Next
                End If
            End If
            '(3) Create the SmtpClient object
            Dim loginInfo As New NetworkCredential(MySMTPSettings.gSMTPLoginName, MySMTPSettings.gSMTPPassword)
            Using client As New Mail.SmtpClient()
                client.DeliveryMethod = Mail.SmtpDeliveryMethod.Network
                client.Host = MySMTPSettings.gSMTPServerName
                client.Port = MySMTPSettings.gSMTPOutPort
                client.EnableSsl = MySMTPSettings.gSMTPenableSsl
                client.Credentials = loginInfo
                client.Send(mm)
            End Using

            '(4) Send the MailMessage (will use the Web.config settings)

        End Using

    End Sub
    Shared Sub SendMailSupport(toAddress As String,
                      subject As String,
                      content As String,
                      Optional IsHtmlBody As Boolean = False,
                      Optional BCCRecipients As String = Nothing)

        '(1) Create the MailMessage instance
        Try


            Using mm As New MailMessage()



                '(2) Assign the MailMessage's properties
                mm.Subject = subject
                mm.Body = content
                mm.IsBodyHtml = IsHtmlBody
                mm.To.Add(toAddress)
                mm.From = New System.Net.Mail.MailAddress(MySMTPSettingsSupport.gSMTPLoginName)
                If (Not String.IsNullOrEmpty(BCCRecipients)) Then
                    If (BCCRecipients.Trim().Length > 5) Then
                        Dim bccArr As String() = BCCRecipients.Split(";")
                        For c1 = 0 To bccArr.Length - 1
                            mm.Bcc.Add(bccArr(c1))
                        Next
                    End If
                End If
                '(3) Create the SmtpClient object
                Dim loginInfo As New NetworkCredential(MySMTPSettingsSupport.gSMTPLoginName, MySMTPSettingsSupport.gSMTPPassword)
                Using client As New Mail.SmtpClient()
                    client.DeliveryMethod = Mail.SmtpDeliveryMethod.Network
                    client.Host = MySMTPSettingsSupport.gSMTPServerName
                    client.Port = MySMTPSettingsSupport.gSMTPOutPort
                    client.EnableSsl = MySMTPSettingsSupport.gSMTPenableSsl
                    client.Credentials = loginInfo
                    client.Send(mm)
                End Using

                '(4) Send the MailMessage (will use the Web.config settings)

            End Using
        Catch ex As Exception

        End Try
    End Sub

    Public Shared Function SendMail2(ByRef SMTPSettings As SMTPSettings,
                                     ByVal strFrom As String,
                                     ByVal strTo As String,
                                     ByVal strSubject As String,
                                     ByVal strBody As String,
                                     Optional IsBodyHtml As Boolean = False,
                                     Optional BCCRecipients As String = Nothing) As Boolean
        Try

            ' Dim mailMessage As New System.Net.Mail.MailMessage
            'Dim loginInfo As New NetworkCredential("viv.pisces@gmail.com", "vivekraj2484")
            'mailMessage.From = New Mail.MailAddress("viv.pisces@gmail.com")
            'mailMessage.To.Add(New Mail.MailAddress(MailTo))

            'mailMessage.Subject = Subject
            'mailMessage.Body = Body

            Dim loginInfo As New NetworkCredential(SMTPSettings.gSMTPLoginName, SMTPSettings.gSMTPPassword)
            Using client As New Mail.SmtpClient()
                client.DeliveryMethod = Mail.SmtpDeliveryMethod.Network
                client.Host = SMTPSettings.gSMTPServerName
                client.Port = SMTPSettings.gSMTPOutPort

                client.EnableSsl = SMTPSettings.gSMTPenableSsl
                client.Credentials = loginInfo




                Using mm As New MailMessage()



                    mm.From = New System.Net.Mail.MailAddress(strFrom)
                    mm.To.Add(strTo)
                    mm.Subject = strSubject
                    mm.Body = strBody
                    mm.IsBodyHtml = IsBodyHtml

                    If (Not String.IsNullOrEmpty(BCCRecipients)) Then
                        If (BCCRecipients.Trim().Length > 5) Then
                            Dim bccArr As String() = BCCRecipients.Split(";")
                            For c1 = 0 To bccArr.Length - 1
                                mm.Bcc.Add(bccArr(c1))
                            Next
                        End If
                    End If

                    client.Send(mm)
                End Using
            End Using
            Return True
        Catch ex As Exception
            Throw
            'gER.ErrorMsgBox(ex, "")
        End Try

        Return False
    End Function
    ''' <summary>
    ''' send using clsMyMail.MySMTPSettings object
    ''' </summary>
    ''' <param name="strFrom"></param>
    ''' <param name="strTo"></param>
    ''' <param name="strSubject"></param>
    ''' <param name="strBody"></param>
    ''' <param name="IsBodyHtml"></param>
    ''' <param name="BCCRecipients"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function SendMail2(ByVal strFrom As String,
                                     ByVal strTo As String,
                                     ByVal strSubject As String,
                                     ByVal strBody As String,
                                     Optional IsBodyHtml As Boolean = False,
                                     Optional BCCRecipients As String = Nothing) As Boolean
        Try

            strSubject = AppUtils.StripTags(strSubject)
            Return SendMail2(MySMTPSettings, strFrom, strTo, strSubject, strBody, IsBodyHtml, BCCRecipients)

        Catch ex As Exception
            Throw
            'gER.ErrorMsgBox(ex, "")
        End Try

        Return False
    End Function


    Shared Sub SendMailFromQueue(toAddress As String,
                                 subject As String,
                                 content As String,
                                 ProfileID As Integer,
                                 EmailAction As String,
                                 IsHtmlBody As Boolean,
                                 SMTPServerCredentialsID As Integer?,
                                 BCCRecipients As String)

        Dim hdl As New Core.DLL.clsSendingEmailsHandler()
        Try
            clsSendingEmailsHandler.AddEmail(toAddress, subject, content, ProfileID, IsHtmlBody, EmailAction, SMTPServerCredentialsID, BCCRecipients)
        Catch ex As Exception
            Throw
        Finally
            hdl.Dispose()
        End Try

    End Sub

End Class

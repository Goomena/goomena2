﻿Imports Dating.Server.Datasets.DLL
Imports System.Data.SqlClient

Public Class clsProfilesPrivacySettings


    Public Shared Function Update_SuppressWarning_WhenReadingMessageFromDifferentCountry(ProfileID As Integer, SuppressWarning As Boolean) As Integer
        Dim result As Integer = 0
        Try

            Dim sql As String = <sql><![CDATA[
update [EUS_ProfilesPrivacySettings]
set [SupressWarning_WhenReadingMessageFromDifferentCountry] = @SupressWarning_WhenReadingMessageFromDifferentCountry
    ,SupressWarning_WhenReadingMessageFromDifferentCountryDateSet=getutcdate()
where [ProfileID]=@ProfileID or [MirrorProfileID]=@ProfileID

if(@@ROWCOUNT = 0)
begin

    declare @MirrorProfileID int
    select @MirrorProfileID=MirrorProfileID from EUS_Profiles with (nolock) where ProfileID=@ProfileID
    
    Insert into [EUS_ProfilesPrivacySettings]([ProfileID],MirrorProfileID,[SupressWarning_WhenReadingMessageFromDifferentCountry],[SupressWarning_WhenReadingMessageFromDifferentCountryDateSet],[PrivacySettings_ShowMeOffline],[HideMeFromUsersInDifferentCountry])
    values (@ProfileID,@MirrorProfileID, @SupressWarning_WhenReadingMessageFromDifferentCountry,getutcdate(),0,0)

end

]]></sql>
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd As SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@ProfileID", ProfileID)
                    cmd.Parameters.AddWithValue("@SupressWarning_WhenReadingMessageFromDifferentCountry", SuppressWarning)
                    result = DataHelpers.ExecuteNonQuery(cmd)
                End Using
            End Using

        Catch ex As Exception
            Throw
        Finally

        End Try
        Return result
    End Function


    Public Shared Function Update_PrivacySettings_ShowMeOffline(ProfileID As Integer, ShowMeOffline As Boolean) As Integer
        Dim result As Integer = 0
        Try

            Dim sql As String = <sql><![CDATA[
update [EUS_ProfilesPrivacySettings]
set [PrivacySettings_ShowMeOffline] = @PrivacySettings_ShowMeOffline
    ,PrivacySettings_ShowMeOfflineDateSet=getutcdate()
where [ProfileID]=@ProfileID or 
    [MirrorProfileID]=@ProfileID

if(@@ROWCOUNT = 0)
begin

    declare @MirrorProfileID int
    select @MirrorProfileID=MirrorProfileID from EUS_Profiles with (nolock) where ProfileID=@ProfileID
    
    Insert into [EUS_ProfilesPrivacySettings]([ProfileID],MirrorProfileID,[PrivacySettings_ShowMeOffline],[PrivacySettings_ShowMeOfflineDateSet],[HideMeFromUsersInDifferentCountry])
    values (@ProfileID,@MirrorProfileID, @PrivacySettings_ShowMeOffline,getutcdate(),0)

end

]]></sql>
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd As SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@ProfileID", ProfileID)
                    cmd.Parameters.AddWithValue("@PrivacySettings_ShowMeOffline", ShowMeOffline)
                    result = DataHelpers.ExecuteNonQuery(cmd)
                End Using
            End Using

        Catch ex As Exception
            Throw
        Finally

        End Try
        Return result
    End Function



    Public Shared Function Update_ItemsPerPage(ProfileID As Integer, ItemsPerPage As Integer) As Integer
        Dim result As Integer = 0
        Try

            Dim sql As String = <sql><![CDATA[
update [EUS_ProfilesPrivacySettings]
set [ItemsPerPage] = @ItemsPerPage
    ,ItemsPerPageDateSet=getutcdate()
where [ProfileID]=@ProfileID or 
    [MirrorProfileID]=@ProfileID

if(@@ROWCOUNT = 0)
begin

    declare @MirrorProfileID int
    select @MirrorProfileID=MirrorProfileID from EUS_Profiles with (nolock) where ProfileID=@ProfileID
    
    Insert into [EUS_ProfilesPrivacySettings]([ProfileID],MirrorProfileID,[ItemsPerPage],[ItemsPerPageDateSet],[PrivacySettings_ShowMeOffline],[HideMeFromUsersInDifferentCountry])
    values (@ProfileID,@MirrorProfileID, @ItemsPerPage,getutcdate(),0,0)

end

]]></sql>
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd As SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@ProfileID", ProfileID)
                    cmd.Parameters.AddWithValue("@ItemsPerPage", ItemsPerPage)
                    result = DataHelpers.ExecuteNonQuery(cmd)
                End Using
            End Using

        Catch ex As Exception
            Throw
        Finally
        End Try
        Return result
    End Function


    Public Shared Function Update_RegisterAdditionalInfo(ProfileID As Integer, RegisterAdditionalInfo As Boolean) As Integer
        Dim result As Integer = 0
        Try

            Dim sql As String = <sql><![CDATA[
update [EUS_ProfilesPrivacySettings]
set [RegisterAdditionalInfo] = @RegisterAdditionalInfo
where [ProfileID]=@ProfileID or 
    [MirrorProfileID]=@ProfileID

if(@@ROWCOUNT = 0)
begin

    declare @MirrorProfileID int
    select @MirrorProfileID=MirrorProfileID from EUS_Profiles with (nolock) where ProfileID=@ProfileID
    
    Insert into [EUS_ProfilesPrivacySettings]([ProfileID],MirrorProfileID,[RegisterAdditionalInfo],[PrivacySettings_ShowMeOffline],[HideMeFromUsersInDifferentCountry])
    values (@ProfileID,@MirrorProfileID, @RegisterAdditionalInfo,0,0)

end

]]></sql>
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection
                Using cmd As SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@ProfileID", ProfileID)
                    cmd.Parameters.AddWithValue("@RegisterAdditionalInfo", RegisterAdditionalInfo)
                    result = DataHelpers.ExecuteNonQuery(cmd)
                End Using
            End Using
           
   

        Catch ex As Exception
            Throw
        Finally

        End Try
        Return result
    End Function


    Public Shared Function Update_SMSWarningViewed(ProfileID As Integer, SMSWarningViewed As Boolean) As Integer
        Dim result As Integer = 0
        Try

            Dim sql As String = <sql><![CDATA[
update [EUS_ProfilesPrivacySettings]
set [SMSWarningViewed] = @SMSWarningViewed
where [ProfileID]=@ProfileID or 
    [MirrorProfileID]=@ProfileID

if(@@ROWCOUNT = 0)
begin

    declare @MirrorProfileID int
    select @MirrorProfileID=MirrorProfileID from EUS_Profiles with (nolock) where ProfileID=@ProfileID
    
    Insert into [EUS_ProfilesPrivacySettings]([ProfileID],MirrorProfileID,[SMSWarningViewed],[PrivacySettings_ShowMeOffline],[HideMeFromUsersInDifferentCountry])
    values (@ProfileID,@MirrorProfileID, @SMSWarningViewed,0,0)

end

]]></sql>
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd As SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@ProfileID", ProfileID)
                    cmd.Parameters.AddWithValue("@SMSWarningViewed", SMSWarningViewed)
                    result = DataHelpers.ExecuteNonQuery(cmd)
                End Using
            End Using

        Catch ex As Exception
            Throw
        Finally

        End Try
        Return result
    End Function
    Public Shared Function Update_HasSendedMobileBonus(ProfileID As Integer, HasSendedMobileBonus As Boolean) As Integer
        Dim result As Integer = 0
        Try

            Dim sql As String = <sql><![CDATA[
update [EUS_ProfilesPrivacySettings]
set [SendedMobileBonus] = @HasSendedMobileBonus,SendedMobileBonusDate=GetUtcDate()
where [ProfileID]=@ProfileID or 
    [MirrorProfileID]=@ProfileID

if(@@ROWCOUNT = 0)
begin

    declare @MirrorProfileID int
    select @MirrorProfileID=MirrorProfileID from EUS_Profiles with (nolock) where ProfileID=@ProfileID
    
    Insert into [EUS_ProfilesPrivacySettings]([ProfileID],MirrorProfileID,[SendedMobileBonus],[SendedMobileBonusDate],[PrivacySettings_ShowMeOffline],[HideMeFromUsersInDifferentCountry])
    values (@ProfileID,@MirrorProfileID, @HasSendedMobileBonus,GetUtcDate(),0,0)

end

]]></sql>
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd As SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@ProfileID", ProfileID)
                    cmd.Parameters.AddWithValue("@HasSendedMobileBonus", HasSendedMobileBonus)
                    result = DataHelpers.ExecuteNonQuery(cmd)
                End Using
            End Using

        Catch ex As Exception
            Throw
        Finally

        End Try
        Return result
    End Function
    Public Shared Function get_HasSendedMobileBonus(ProfileID As Integer) As Boolean
        Dim HasSendedMobileBonus As Boolean
        Try

            Dim sql As String = <sql><![CDATA[
select SendedMobileBonus = cast ( ISNULL((
    select top(1) SendedMobileBonus 
    from [EUS_ProfilesPrivacySettings] with (nolock)
    where [ProfileID]=@ProfileID or [MirrorProfileID]=@ProfileID)
    ,0) as bit)

]]></sql>
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd As SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@ProfileID", ProfileID)
                    Dim result As DataTable = DataHelpers.GetDataTable(cmd)
                    If (result.Rows.Count > 0) Then
                        HasSendedMobileBonus = result.Rows(0)("SendedMobileBonus")
                    End If
                End Using
            End Using

        Catch ex As Exception
            Throw
        Finally

        End Try
        Return HasSendedMobileBonus
    End Function
    Public Shared Function Update_Eus_BirthdaysForCheck(ProfileID As Integer) As Integer
        Dim result As Integer = 0
        Try

            Dim sql As String = <sql><![CDATA[
update [EUS_ProfilesPrivacySettings]
set AccessedForHappyBirthday=GetUtcDate()
where [ProfileID]=@ProfileID or 
    [MirrorProfileID]=@ProfileID

if(@@ROWCOUNT = 0)
begin

    declare @MirrorProfileID int
    select @MirrorProfileID=MirrorProfileID from EUS_Profiles with (nolock) where ProfileID=@ProfileID
    
    Insert into [EUS_ProfilesPrivacySettings]([ProfileID],MirrorProfileID,[AccessedForHappyBirthday],[PrivacySettings_ShowMeOffline],[HideMeFromUsersInDifferentCountry])
    values (@ProfileID,@MirrorProfileID,GetUtcDate(),0,0)
end


]]></sql>
            Dim sql2 As String = <sql><![CDATA[
update [Eus_BirthdaysForCheck]
set DateTimeAdded=GetUtcDate()
where [ProfileID]=@ProfileID or 
    [MirrorProfileID]=@ProfileID

if(@@ROWCOUNT = 0)
begin

    declare @MirrorProfileID int
    select @MirrorProfileID=MirrorProfileID from EUS_Profiles with (nolock) where ProfileID=@ProfileID
    
    Insert into [Eus_BirthdaysForCheck]([ProfileID],[MirrorProfileID],[DateTimeAdded])
    values (@ProfileID,@MirrorProfileID,GetUtcDate())
end


]]></sql>
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd As SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@ProfileID", ProfileID)
                    result = DataHelpers.ExecuteNonQuery(cmd)
                End Using

                'cmd.Parameters.AddWithValue("@ProfileID", ProfileID)

                Using cmd2 As SqlCommand = DataHelpers.GetSqlCommand(con, sql2)
                    cmd2.Parameters.AddWithValue("@ProfileID", ProfileID)
                    'cmd.Parameters.AddWithValue("@ProfileID", ProfileID)
                    result = DataHelpers.ExecuteNonQuery(cmd2)
                End Using
            End Using
        Catch ex As Exception
            Throw
        Finally

        End Try
        Return result
    End Function

    Public Shared Function Update_LastBirthdayBonusSendToNow(ProfileID As Integer) As Integer
        Dim result As Integer = 0
        Try

            Dim sql As String = <sql><![CDATA[
update [EUS_ProfilesPrivacySettings]
set AccessedForHappyBirthday=GetUtcDate(),
SendedHappyBirthdayBonus = GetUtcDate()
where [ProfileID]=@ProfileID or 
    [MirrorProfileID]=@ProfileID

if(@@ROWCOUNT = 0)
begin

    declare @MirrorProfileID int
    select @MirrorProfileID=MirrorProfileID from EUS_Profiles  with (nolock) where ProfileID=@ProfileID
    
    Insert into [EUS_ProfilesPrivacySettings]([ProfileID],MirrorProfileID,[AccessedForHappyBirthday],[SendedHappyBirthdayBonus],[PrivacySettings_ShowMeOffline],[HideMeFromUsersInDifferentCountry])
    values (@ProfileID,@MirrorProfileID,GetUtcDate(),GetUtcDate(),0,0)
end

]]></sql>
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd As SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@ProfileID", ProfileID)
                    result = DataHelpers.ExecuteNonQuery(cmd)

                End Using
            End Using

        Catch ex As Exception
            Throw
        Finally

        End Try
        Return result
    End Function
    
    '    Public Shared Function get_LastBirthdayBonusSend(ProfileID As Integer) As Date
    '        Dim LastBirthdayBonusSend As Date
    '        Try

    '            Dim sql As String = <sql><![CDATA[
    'select LastBirthdayBonusSend = cast(isnull((SELECT top 1
    '	  LastBirthdayBonusSend
    '  FROM [dbo].[EUS_ProfilesPrivacySettings] with (nolock)
    '    where [ProfileID]=@ProfileID or [MirrorProfileID]=@ProfileID),DATEADD(year,-2,getutcdate())) as datetime)


    ']]></sql>
    '            Dim cmd As SqlCommand = DataHelpers.GetSqlCommand(sql)
    '            cmd.Parameters.AddWithValue("@ProfileID", ProfileID)
    '            Dim result As DataTable = DataHelpers.GetDataTable(cmd)
    '            If (result.Rows.Count > 0) Then
    '                LastBirthdayBonusSend = CDate(result.Rows(0)("LastBirthdayBonusSend"))
    '            End If

    '        Catch ex As Exception
    '            Throw
    '        Finally

    '        End Try
    '        Return LastBirthdayBonusSend
    '    End Function

    Public Shared Function GET_SMSWarningViewed(ProfileID As Integer) As Boolean
        Dim SMSWarningViewed As Boolean
        Try

            Dim sql As String = <sql><![CDATA[
select SMSWarningViewed = cast ( ISNULL((
    select top(1) SMSWarningViewed 
    from [EUS_ProfilesPrivacySettings]  with (nolock)
    where [ProfileID]=@ProfileID or [MirrorProfileID]=@ProfileID)
    ,0) as bit)

]]></sql>

            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd As SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@ProfileID", ProfileID)
                    Dim result As DataTable = DataHelpers.GetDataTable(cmd)
                    If (result.Rows.Count > 0) Then
                        SMSWarningViewed = result.Rows(0)("SMSWarningViewed")
                    End If
                End Using

            End Using
        Catch ex As Exception
            Throw
        Finally

        End Try
        Return SMSWarningViewed
    End Function


    Public Shared Function GET_RegisterAdditionalInfo(ProfileID As Integer) As Boolean
        Dim RegisterAdditionalInfo As Boolean
        Try

            Dim sql As String = <sql><![CDATA[
select RegisterAdditionalInfo = cast ( ISNULL((
    select top(1) RegisterAdditionalInfo 
    from [EUS_ProfilesPrivacySettings]  with (nolock)
    where [ProfileID]=@ProfileID or [MirrorProfileID]=@ProfileID)
    ,0) as bit)

]]></sql>

            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection

                Using cmd As SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@ProfileID", ProfileID)
                    Dim result As DataTable = DataHelpers.GetDataTable(cmd)
                    If (result.Rows.Count > 0) Then
                        RegisterAdditionalInfo = result.Rows(0)("RegisterAdditionalInfo")
                    End If
                End Using

            End Using

        Catch ex As Exception
            Throw
        Finally

        End Try
        Return RegisterAdditionalInfo
    End Function


    Public Shared Function GET_SuppressWarning_WhenReadingMessageFromDifferentCountry(ProfileID As Integer) As Boolean
        Dim SuppressWarning As Boolean
        Try

            Dim sql As String = <sql><![CDATA[
select SuppressWarning = cast ( ISNULL((
    select top(1) SupressWarning_WhenReadingMessageFromDifferentCountry 
    from [EUS_ProfilesPrivacySettings]  with (nolock)
    where [ProfileID]=@ProfileID or [MirrorProfileID]=@ProfileID)
    ,0) as bit)

]]></sql>
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd As SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@ProfileID", ProfileID)
                    'cmd.Parameters.AddWithValue("@SupressWarning_WhenReadingMessageFromDifferentCountry", SuppressWarning)
                    Dim result As DataTable = DataHelpers.GetDataTable(cmd)
                    If (result.Rows.Count > 0) Then
                        SuppressWarning = result.Rows(0)("SuppressWarning")
                    End If
                End Using
            End Using

        Catch ex As Exception
            Throw
        Finally

        End Try
        Return SuppressWarning
    End Function

    Public Shared Function GET_DisableReceivingMessagesFromDifferentCountry(ProfileID As Integer) As Boolean
        Dim DisableReceivingMessagesFromDifferentCountry As Boolean
        Try

            Dim sql As String = <sql><![CDATA[
select DisableReceivingMessagesFromDifferentCountry = cast ( ISNULL((
    select top(1) DisableReceivingMessagesFromDifferentCountry 
    from [EUS_ProfilesPrivacySettings]  with (nolock)
    where [ProfileID]=@ProfileID or [MirrorProfileID]=@ProfileID)
    ,0) as bit)

]]></sql>

            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd As SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@ProfileID", ProfileID)
                    Dim result As DataTable = DataHelpers.GetDataTable(cmd)
                    If (result.Rows.Count > 0) Then
                        DisableReceivingMessagesFromDifferentCountry = result.Rows(0)("DisableReceivingMessagesFromDifferentCountry")
                    End If
                End Using

            End Using
        Catch ex As Exception
            Throw
        Finally

        End Try
        Return DisableReceivingMessagesFromDifferentCountry
    End Function

    Public Shared Function GET_DisableReceivingLikesFromDifferentCountry(ProfileID As Integer) As Boolean
        Dim DisableReceivingLikesFromDifferentCountry As Boolean
        Try

            Dim sql As String = <sql><![CDATA[
select DisableReceivingLikesFromDifferentCountry = cast ( ISNULL((
    select top(1) DisableReceivingLikesFromDifferentCountry 
    from [EUS_ProfilesPrivacySettings]  with (nolock)
    where [ProfileID]=@ProfileID or [MirrorProfileID]=@ProfileID)
    ,0) as bit)

]]></sql>
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd As SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@ProfileID", ProfileID)
                    Dim result As DataTable = DataHelpers.GetDataTable(cmd)
                    If (result.Rows.Count > 0) Then
                        DisableReceivingLikesFromDifferentCountry = result.Rows(0)("DisableReceivingLikesFromDifferentCountry")
                    End If
                End Using
            End Using

        Catch ex As Exception
            Throw
        Finally

        End Try
        Return DisableReceivingLikesFromDifferentCountry
    End Function


    Public Shared Function GET_DisableReceivingLIKESFromDifferentCountryFromTo(ToProfileID As Integer, FromProfileID As Integer) As Boolean
        Dim DisableReceivingLikesFromDifferentCountry As Boolean
        Try

            Dim sql As String = <sql><![CDATA[
declare @DisableReceivingLikesFromDifferentCountry bit


select @DisableReceivingLikesFromDifferentCountry = cast ( ISNULL((
    select top(1) DisableReceivingLikesFromDifferentCountry 
    from [EUS_ProfilesPrivacySettings]  with (nolock)
    where [ProfileID]=@ToProfileID or [MirrorProfileID]=@ToProfileID)
    ,0) as bit)


-- if receiving of likes from different country is disabled
-- then check profiles' countries

if(@DisableReceivingLikesFromDifferentCountry=1)
begin

    select @DisableReceivingLikesFromDifferentCountry =case
				when count(*)>0 then 0
				else 1
			end
    from (
        select Country
        from dbo.EUS_Profiles   with (nolock)
        where [ProfileID]=@ToProfileID
    ) as t1,
    (
        select Country
        from dbo.EUS_Profiles   with (nolock)
        where [ProfileID]=@FromProfileID
    ) as t2
    where t1.Country = t2.Country
    
end

select @DisableReceivingLikesFromDifferentCountry as DisableReceivingLikesFromDifferentCountry

]]></sql>
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd As SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@ToProfileID", ToProfileID)
                    cmd.Parameters.AddWithValue("@FromProfileID", FromProfileID)
                    Dim result As DataTable = DataHelpers.GetDataTable(cmd)
                    If (result.Rows.Count > 0) Then
                        DisableReceivingLikesFromDifferentCountry = result.Rows(0)("DisableReceivingLikesFromDifferentCountry")
                    End If
                End Using
            End Using

        Catch ex As Exception
            Throw
        Finally

        End Try
        Return DisableReceivingLikesFromDifferentCountry
    End Function



    Public Shared Function GET_DisableReceivingOFFERSFromDifferentCountryFromTo(ToProfileID As Integer, FromProfileID As Integer) As Boolean
        Dim DisableReceivingOffersFromDifferentCountry As Boolean
        Try

            Dim sql As String = <sql><![CDATA[
declare @DisableReceivingOffersFromDifferentCountry bit


select @DisableReceivingOffersFromDifferentCountry = cast ( ISNULL((
    select top(1) DisableReceivingOffersFromDifferentCountry 
    from [EUS_ProfilesPrivacySettings]  with (nolock)
    where [ProfileID]=@ToProfileID or [MirrorProfileID]=@ToProfileID)
    ,0) as bit)


-- if receiving of offers from different country is disabled
-- then check profiles' countries

if(@DisableReceivingOffersFromDifferentCountry=1)
begin

    select @DisableReceivingOffersFromDifferentCountry =case
				when count(*)>0 then 0
				else 1
			end
    from (
        select Country
        from dbo.EUS_Profiles  with (nolock)
        where [ProfileID]=@ToProfileID
    ) as t1,
    (
        select Country
        from dbo.EUS_Profiles  with (nolock)
        where [ProfileID]=@FromProfileID
    ) as t2
    where t1.Country = t2.Country
    
end

select @DisableReceivingOffersFromDifferentCountry as DisableReceivingOffersFromDifferentCountry

]]></sql>
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd As SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@ToProfileID", ToProfileID)
                    cmd.Parameters.AddWithValue("@FromProfileID", FromProfileID)
                    Dim result As DataTable = DataHelpers.GetDataTable(cmd)
                    If (result.Rows.Count > 0) Then
                        DisableReceivingOffersFromDifferentCountry = result.Rows(0)("DisableReceivingOffersFromDifferentCountry")
                    End If
                End Using
            End Using

        Catch ex As Exception
            Throw
        Finally

        End Try
        Return DisableReceivingOffersFromDifferentCountry
    End Function


    Public Shared Function GET_DisableReceivingMESSAGESFromDifferentCountryFromTo(ToProfileID As Integer, FromProfileID As Integer) As Boolean
        Dim DisableReceivingMessagesFromDifferentCountry As Boolean
        Try

            Dim sql As String = <sql><![CDATA[
declare @DisableReceivingMessagesFromDifferentCountry bit


select @DisableReceivingMessagesFromDifferentCountry = cast ( ISNULL((
    select top(1) DisableReceivingMessagesFromDifferentCountry 
    from [EUS_ProfilesPrivacySettings]  with (nolock)
	where [ProfileID]=@ToProfileID or [MirrorProfileID]=@ToProfileID)
    ,0) as bit)


-- if receiving of offers from different country is disabled
-- then check profiles' countries

if(@DisableReceivingMessagesFromDifferentCountry=1)
begin

    select @DisableReceivingMessagesFromDifferentCountry =case
				when count(*)>0 then 0
				else 1
			end
    from (
        select Country
        from dbo.EUS_Profiles  with (nolock)
        where [ProfileID]=@ToProfileID
    ) as t1,
    (
        select Country
        from dbo.EUS_Profiles  with (nolock)
        where [ProfileID]=@FromProfileID
    ) as t2
    where t1.Country = t2.Country
    
end

select @DisableReceivingMessagesFromDifferentCountry as DisableReceivingMessagesFromDifferentCountry

]]></sql>

            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection

                Using cmd As SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@ToProfileID", ToProfileID)
                    cmd.Parameters.AddWithValue("@FromProfileID", FromProfileID)
                    Dim result As DataTable = DataHelpers.GetDataTable(cmd)
                    If (result.Rows.Count > 0) Then
                        DisableReceivingMessagesFromDifferentCountry = result.Rows(0)("DisableReceivingMessagesFromDifferentCountry")
                    End If
                End Using

            End Using

        Catch ex As Exception
            Throw
        Finally

        End Try
        Return DisableReceivingMessagesFromDifferentCountry
    End Function




    Public Shared Sub Update_AdminSetting_BlockMessagesToBeReceivedByOther(ProfileID As Integer, MirrorProfileID As Integer, Enable As Boolean)

        Using cmdb As New CMSDBDataContext(DataHelpers.ConnectionString)




            Try

                Dim asstt As EUS_ProfilesPrivacySetting = Nothing

                asstt = (From itm In cmdb.EUS_ProfilesPrivacySettings
                         Where itm.ProfileID = ProfileID OrElse itm.MirrorProfileID = ProfileID
                         Select itm).FirstOrDefault()

                If (asstt Is Nothing) Then
                    asstt = New EUS_ProfilesPrivacySetting()
                    cmdb.EUS_ProfilesPrivacySettings.InsertOnSubmit(asstt)
                End If

                asstt.ProfileID = ProfileID
                asstt.MirrorProfileID = MirrorProfileID
                asstt.AdminSetting_BlockMessagesToBeReceivedByOther = Enable
                asstt.AdminSetting_BlockMessagesToBeReceivedByOtherDateSet = DateTime.UtcNow

                cmdb.SubmitChanges()

            Catch ex As Exception
                Throw
            
            End Try
        End Using
    End Sub



    Public Shared Function GET_AdminSetting_BlockMessagesToBeReceivedByOther(ProfileID As Integer) As Boolean
        Dim AdminSetting_BlockMessagesToBeReceivedByOther As Boolean
        Try

            Dim sql As String = <sql><![CDATA[
select AdminSetting_BlockMessagesToBeReceivedByOther = cast (ISNULL((
    select top(1) AdminSetting_BlockMessagesToBeReceivedByOther 
    from [EUS_ProfilesPrivacySettings]  with (nolock)
    where [ProfileID]=@ProfileID or [MirrorProfileID]=@ProfileID)
    ,0) as bit)

]]></sql>

            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd As SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@ProfileID", ProfileID)
                    Dim result As DataTable = DataHelpers.GetDataTable(cmd)
                    If (result.Rows.Count > 0) Then
                        AdminSetting_BlockMessagesToBeReceivedByOther = result.Rows(0)("AdminSetting_BlockMessagesToBeReceivedByOther")
                    End If
                End Using

            End Using
        Catch ex As Exception
            Throw
        Finally

        End Try
        Return AdminSetting_BlockMessagesToBeReceivedByOther
    End Function



    Public Shared Function GET_AdminSetting_IsVirtual(ProfileID As Integer, Optional CheckSpanishWoman As Boolean = False) As Boolean
        Dim AdminSetting_IsVirtual As Boolean
        Try
            Dim sql As String = ""
            If (CheckSpanishWoman) Then

                '                sql = <sql><![CDATA[
                'declare @AdminSetting_IsVirtual bit;

                'select @AdminSetting_IsVirtual=AdminSetting_IsVirtual 
                'from [EUS_ProfilesPrivacySettings]
                'where [ProfileID]=@ProfileID or [MirrorProfileID]=@ProfileID);

                'if(@AdminSetting_IsVirtual is null)
                'begin
                '    select @AdminSetting_IsVirtual=() 
                '    from [EUS_ProfilesPrivacySettings]
                '    where [ProfileID]=@ProfileID or [MirrorProfileID]=@ProfileID);
                'end

                'select AdminSetting_IsVirtual = cast (ISNULL((@AdminSetting_IsVirtual),0) as bit);
                ']]></sql>

                sql = <sql><![CDATA[
select AdminSetting_IsVirtual = cast (ISNULL((
    select top(1) AdminSetting_IsVirtual 
    from [EUS_ProfilesPrivacySettings]  with (nolock)
    where [ProfileID]=@ProfileID or [MirrorProfileID]=@ProfileID)
    ,0) as bit)
]]></sql>

            Else

                sql = <sql><![CDATA[
select AdminSetting_IsVirtual = cast (ISNULL((
    select top(1) AdminSetting_IsVirtual 
    from [EUS_ProfilesPrivacySettings]  with (nolock)
    where [ProfileID]=@ProfileID or [MirrorProfileID]=@ProfileID)
    ,0) as bit)
]]></sql>

            End If


            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd As SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@ProfileID", ProfileID)
                    Dim result As DataTable = DataHelpers.GetDataTable(cmd)
                    If (result.Rows.Count > 0) Then
                        AdminSetting_IsVirtual = result.Rows(0)("AdminSetting_IsVirtual")
                    End If
                End Using
            End Using

        Catch ex As Exception
            Throw
        Finally

        End Try
        Return AdminSetting_IsVirtual
    End Function



    Public Shared Function GET_PrivacySettings_ShowMeOffline(ProfileID As Integer) As Boolean
        Dim PrivacySettings_ShowMeOffline As Boolean
        Try

            Dim sql As String = <sql><![CDATA[
select PrivacySettings_ShowMeOffline = cast (ISNULL((
    select top(1) PrivacySettings_ShowMeOffline 
    from [EUS_ProfilesPrivacySettings]  with (nolock)
    where (ProfileID=@ProfileID OR [MirrorProfileID]=@ProfileID)
	and [PrivacySettings_ShowMeOffline]=1)
,0) as bit)
]]></sql>
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd As SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@ProfileID", ProfileID)
                    Dim result As DataTable = DataHelpers.GetDataTable(cmd)
                    If (result.Rows.Count > 0) Then
                        PrivacySettings_ShowMeOffline = result.Rows(0)("PrivacySettings_ShowMeOffline")
                    End If
                End Using
            End Using

        Catch ex As Exception
            Throw
        Finally

        End Try
        Return PrivacySettings_ShowMeOffline
    End Function



    Public Shared Function Update_NearestMembersOptions(ProfileID As Integer, NearestMembersOptions As Integer) As Integer
        Dim result As Integer = 0
        Try

            Dim sql As String = <sql><![CDATA[
    update [EUS_ProfilesPrivacySettings]
    set [NearestMembersOptions] = @NearestMembersOptions
        ,NearestMembersOptionsDateSet=getutcdate()
    where [ProfileID]=@ProfileID or 
        [MirrorProfileID]=@ProfileID

    if(@@ROWCOUNT = 0)
    begin

        declare @MirrorProfileID int
        select @MirrorProfileID=MirrorProfileID from EUS_Profiles where ProfileID=@ProfileID

        Insert into [EUS_ProfilesPrivacySettings]([ProfileID],MirrorProfileID,[NearestMembersOptions],[NearestMembersOptionsDateSet],[PrivacySettings_ShowMeOffline],[HideMeFromUsersInDifferentCountry])
        values (@ProfileID,@MirrorProfileID, @NearestMembersOptions,getutcdate(),0,0)

    end

    ]]></sql>

            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd As SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@ProfileID", ProfileID)
                    cmd.Parameters.AddWithValue("@NearestMembersOptions", NearestMembersOptions)
                    result = DataHelpers.ExecuteNonQuery(cmd)
                End Using
            End Using

        Catch ex As Exception
            Throw
        Finally

        End Try
        Return result
    End Function

    Public Shared Function GET_NearestMembersOptions(ProfileID As Integer) As Integer
        Dim NearestMembersOptions As Integer
        Try

            Dim sql As String = <sql><![CDATA[
select NearestMembersOptions = cast (ISNULL((
    select top(1) NearestMembersOptions 
    from [EUS_ProfilesPrivacySettings]  with (nolock)
    where (ProfileID=@ProfileID OR [MirrorProfileID]=@ProfileID)),0) as int)
]]></sql>
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd As SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@ProfileID", ProfileID)
                    Dim result As DataTable = DataHelpers.GetDataTable(cmd)
                    If (result.Rows.Count > 0) Then
                        NearestMembersOptions = result.Rows(0)("NearestMembersOptions")
                    End If
                End Using
            End Using

        Catch ex As Exception
            Throw
        Finally

        End Try
        Return NearestMembersOptions
    End Function


    Public Shared Function Update_ProfilesViewedOptions(ProfileID As Integer, ProfilesViewedOptions As Integer) As Integer
        Dim result As Integer = 0
        Try

            Dim sql As String = <sql><![CDATA[
    update [EUS_ProfilesPrivacySettings]
    set [ProfilesViewedOptions] = @ProfilesViewedOptions
        ,ProfilesViewedOptionsDateSet=getutcdate()
    where [ProfileID]=@ProfileID or 
        [MirrorProfileID]=@ProfileID

    if(@@ROWCOUNT = 0)
    begin

        declare @MirrorProfileID int
        select @MirrorProfileID=MirrorProfileID from EUS_Profiles where ProfileID=@ProfileID

        Insert into [EUS_ProfilesPrivacySettings]([ProfileID],MirrorProfileID,[ProfilesViewedOptions],[ProfilesViewedOptionsDateSet],[PrivacySettings_ShowMeOffline],[HideMeFromUsersInDifferentCountry])
        values (@ProfileID,@MirrorProfileID, @ProfilesViewedOptions,getutcdate(),0,0)

    end

    ]]></sql>

            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd As SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@ProfileID", ProfileID)
                    cmd.Parameters.AddWithValue("@ProfilesViewedOptions", ProfilesViewedOptions)
                    result = DataHelpers.ExecuteNonQuery(cmd)
                End Using
            End Using

        Catch ex As Exception
            Throw
        Finally

        End Try
        Return result
    End Function

    Public Shared Function GET_ProfilesViewedOptions(ProfileID As Integer) As Integer
        Dim ProfilesViewedOptions As Integer
        Try

            Dim sql As String = <sql><![CDATA[
select ProfilesViewedOptions = cast (ISNULL((
    select top(1) ProfilesViewedOptions 
    from [EUS_ProfilesPrivacySettings]  with (nolock)
    where (ProfileID=@ProfileID OR [MirrorProfileID]=@ProfileID)),0) as int)
]]></sql>

            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd As SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@ProfileID", ProfileID)
                    Dim result As DataTable = DataHelpers.GetDataTable(cmd)
                    If (result.Rows.Count > 0) Then
                        ProfilesViewedOptions = result.Rows(0)("ProfilesViewedOptions")
                    End If
                End Using
            End Using

        Catch ex As Exception
            Throw
        Finally

        End Try
        Return ProfilesViewedOptions
    End Function

    Public Shared Function GET_ItemsPerPage(ProfileID As Integer) As Integer
        Dim ItemsPerPage As Integer
        Try

            Dim sql As String = <sql><![CDATA[
select ItemsPerPage = cast (ISNULL((
    select top(1) ItemsPerPage 
    from [EUS_ProfilesPrivacySettings]  with (nolock)
    where (ProfileID=@ProfileID OR [MirrorProfileID]=@ProfileID)),0) as int)
]]></sql>
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd As SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@ProfileID", ProfileID)
                    Dim result As DataTable = DataHelpers.GetDataTable(cmd)
                    If (result.Rows.Count > 0) Then
                        ItemsPerPage = result.Rows(0)("ItemsPerPage")
                    End If
                End Using
            End Using

        Catch ex As Exception
            Throw
        Finally

        End Try
        Return ItemsPerPage
    End Function


    Public Shared Function GET_NotificationsSettings_WhenMyProfileVisited(ProfileID As Integer) As Boolean
        Dim result As Boolean
        Try

            ' default value is TRUE, 
            ' NULL value => TRUE

            Dim sql As String = <sql><![CDATA[
select v = cast (ISNULL((
    select top(1) NotificationsSettings_WhenMyProfileVisited 
    from [EUS_ProfilesPrivacySettings]  with (nolock)
    where (ProfileID=@ProfileID OR [MirrorProfileID]=@ProfileID))
,1) as bit)
]]></sql>
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd As SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@ProfileID", ProfileID)
                    Dim dt As DataTable = DataHelpers.GetDataTable(cmd)
                    If (dt.Rows.Count > 0) Then
                        result = dt.Rows(0)("v")
                    End If
                End Using

            End Using
        Catch ex As Exception
            Throw
        Finally

        End Try
        Return result
    End Function


    Public Shared Function GET_NotificationsSettings_SendMeAboutUnvisitedProfiles(ProfileID As Integer) As Boolean
        Dim result As Boolean
        Try

            ' default value is TRUE, 
            ' NULL value => TRUE

            Dim sql As String = <sql><![CDATA[
select v = cast (ISNULL((
    select top(1) NotificationsSettings_SendMeAboutUnvisitedProfiles 
    from [EUS_ProfilesPrivacySettings]  with (nolock)
    where (ProfileID=@ProfileID OR [MirrorProfileID]=@ProfileID))
,1) as bit)
]]></sql>
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd As SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@ProfileID", ProfileID)
                    Dim dt As DataTable = DataHelpers.GetDataTable(cmd)
                    If (dt.Rows.Count > 0) Then
                        result = dt.Rows(0)("v")
                    End If
                End Using
            End Using

        Catch ex As Exception
            Throw
        Finally

        End Try
        Return result
    End Function


    Public Shared Function Update_FreeCreditsAvailableToRead(ProfileID As Integer, FreeCreditsAvailableToRead As Integer?) As Integer
        Dim result As Integer = 0
        Try

            Dim sql As String = <sql><![CDATA[
update [EUS_ProfilesPrivacySettings]
set [FreeCreditsAvailableToRead] = @FreeCreditsAvailableToRead
where [ProfileID]=@ProfileID or 
    [MirrorProfileID]=@ProfileID

if(@@ROWCOUNT = 0)
begin

    declare @MirrorProfileID int
    select @MirrorProfileID=MirrorProfileID from EUS_Profiles where ProfileID=@ProfileID

    Insert into [EUS_ProfilesPrivacySettings]([ProfileID],MirrorProfileID,[FreeCreditsAvailableToRead],[PrivacySettings_ShowMeOffline],[HideMeFromUsersInDifferentCountry])
    values (@ProfileID,@MirrorProfileID, @FreeCreditsAvailableToRead,0,0)

end

]]></sql>
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd As SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@ProfileID", ProfileID)
                    If (FreeCreditsAvailableToRead.HasValue) Then
                        cmd.Parameters.AddWithValue("@FreeCreditsAvailableToRead", FreeCreditsAvailableToRead)
                    Else
                        cmd.Parameters.AddWithValue("@FreeCreditsAvailableToRead", System.DBNull.Value)
                    End If
                    result = DataHelpers.ExecuteNonQuery(cmd)
                End Using
            End Using

        Catch ex As Exception
            Throw
        Finally

        End Try
        Return result
    End Function

    Public Shared Function GET_FreeCreditsAvailableToRead(ProfileID As Integer) As Integer?
        Dim FreeCreditsAvailableToRead As Integer? = Nothing
        Try

            Dim sql As String = <sql><![CDATA[
select top(1) FreeCreditsAvailableToRead 
from [EUS_ProfilesPrivacySettings]  with (nolock)
where (ProfileID=@ProfileID OR [MirrorProfileID]=@ProfileID)
]]></sql>
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd As SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@ProfileID", ProfileID)
                    Dim result As DataTable = DataHelpers.GetDataTable(cmd)
                    If (result.Rows.Count > 0) Then
                        If (Not result.Rows(0).IsNull("FreeCreditsAvailableToRead")) Then
                            FreeCreditsAvailableToRead = result.Rows(0)("FreeCreditsAvailableToRead")
                        End If
                    End If
                End Using
            End Using

        Catch ex As Exception
            Throw
        Finally

        End Try
        Return FreeCreditsAvailableToRead
    End Function


    Public Shared Function Update_FreeCreditsAvailableToSend(ProfileID As Integer, FreeCreditsAvailableToSend As Integer?) As Integer
        Dim result As Integer = 0
        Try

            Dim sql As String = <sql><![CDATA[
update [EUS_ProfilesPrivacySettings]
set [FreeCreditsAvailableToSend] = @FreeCreditsAvailableToSend
where [ProfileID]=@ProfileID or 
    [MirrorProfileID]=@ProfileID

if(@@ROWCOUNT = 0)
begin

    declare @MirrorProfileID int
    select @MirrorProfileID=MirrorProfileID from EUS_Profiles where ProfileID=@ProfileID

    Insert into [EUS_ProfilesPrivacySettings]([ProfileID],MirrorProfileID,[FreeCreditsAvailableToSend],[PrivacySettings_ShowMeOffline],[HideMeFromUsersInDifferentCountry])
    values (@ProfileID,@MirrorProfileID, @FreeCreditsAvailableToSend,0,0)

end

]]></sql>
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd As SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@ProfileID", ProfileID)
                    If (FreeCreditsAvailableToSend.HasValue) Then
                        cmd.Parameters.AddWithValue("@FreeCreditsAvailableToSend", FreeCreditsAvailableToSend)
                    Else
                        cmd.Parameters.AddWithValue("@FreeCreditsAvailableToSend", System.DBNull.Value)
                    End If
                    result = DataHelpers.ExecuteNonQuery(cmd)
                End Using
            End Using

        Catch ex As Exception
            Throw
        Finally

        End Try
        Return result
    End Function

    Public Shared Function GET_FreeCreditsAvailableToSend(ProfileID As Integer) As Integer?
        Dim FreeCreditsAvailableToSend As Integer? = Nothing
        Try

            Dim sql As String = <sql><![CDATA[
select top(1) FreeCreditsAvailableToSend 
from [EUS_ProfilesPrivacySettings]  with (nolock)
where (ProfileID=@ProfileID OR [MirrorProfileID]=@ProfileID)
]]></sql>
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd As SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@ProfileID", ProfileID)
                    Dim result As DataTable = DataHelpers.GetDataTable(cmd)
                    If (result.Rows.Count > 0) Then
                        If (Not result.Rows(0).IsNull("FreeCreditsAvailableToSend")) Then
                            FreeCreditsAvailableToSend = result.Rows(0)("FreeCreditsAvailableToSend")
                        End If
                    End If
                End Using
            End Using

        Catch ex As Exception
            Throw
        Finally

        End Try
        Return FreeCreditsAvailableToSend
    End Function


    Public Shared Function CreateNew_EUS_ProfilesPrivacySetting(ProfileID As Integer, MirrorProfileID As Integer) As EUS_ProfilesPrivacySetting
        Using ctx As New CMSDBDataContext(DataHelpers.ConnectionString)


            Dim _EUS_Pps As EUS_ProfilesPrivacySetting = Nothing
            Try

                _EUS_Pps = (From itm In ctx.EUS_ProfilesPrivacySettings
                            Where (itm.ProfileID = ProfileID AndAlso itm.MirrorProfileID = MirrorProfileID) OrElse (itm.ProfileID = MirrorProfileID AndAlso itm.MirrorProfileID = ProfileID)
                            Select itm).FirstOrDefault()

                If (_EUS_Pps Is Nothing) Then
                    _EUS_Pps = New EUS_ProfilesPrivacySetting()
                    _EUS_Pps.ProfileID = ProfileID
                    _EUS_Pps.MirrorProfileID = MirrorProfileID
                    _EUS_Pps.SMSWarningViewed = False
                    _EUS_Pps.RegisterAdditionalInfo = False
                    _EUS_Pps.HideMeFromUsersInDifferentCountry = False
                    _EUS_Pps.PrivacySettings_ShowMeOffline = False
                    ctx.EUS_ProfilesPrivacySettings.InsertOnSubmit(_EUS_Pps)
                    ctx.SubmitChanges()
                End If
                Return _EUS_Pps
            Catch ex As Exception
                Throw
          
            End Try
        End Using

    End Function

End Class

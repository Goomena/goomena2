﻿Imports Dating.Server.Datasets.DLL

Public Class clsReferrerCommissions

    Public Property REF_CommissionLevel0 As Double
    Public Property REF_CommissionLevel1 As Double
    Public Property REF_CommissionLevel2 As Double
    Public Property REF_CommissionLevel3 As Double
    Public Property REF_CommissionLevel4 As Double
    Public Property REF_CommissionLevel5 As Double
    Public Property REF_CommissionConvRate As Double
    Public Property REF_CommissionPendingPercent As Double
    Public Property REF_CommissionPendingPeriod As Integer

    Private _ReferrerID As Integer
    Public ReadOnly Property ReferrerID As Integer
        Get
            Return _ReferrerID
        End Get
    End Property



    Sub New()
        '_ReferrerID = affID
    End Sub

    Sub New(affID As Integer)
        _ReferrerID = affID
    End Sub


    Private _config As clsConfigValues
    Private Function GetConfig() As clsConfigValues
        If (_config Is Nothing) Then _config = New clsConfigValues()
        Return _config
    End Function


    Public Sub Load(affID As Integer)
        _ReferrerID = affID
        Dim profileRow As vw_EusProfile_Light = Nothing
        'Dim profileRow As DSMembers.EUS_ProfilesRow = DataHelpers.GetEUS_Profiles_ByProfileIDRow(_ReferrerID)
        Using ctx As New CMSDBDataContext(DataHelpers.ConnectionString)


            profileRow = DataHelpers.GetCurrentProfile(ctx, _ReferrerID, 0)
        End Using
        If (clsNullable.NullTo(profileRow.REF_CommissionLevel0) > 0) Then
            Me.REF_CommissionLevel0 = profileRow.REF_CommissionLevel0
        Else
            Me.REF_CommissionLevel0 = GetConfig().referrer_commission_level_0
        End If

        If (clsNullable.NullTo(profileRow.REF_CommissionLevel1) > 0) Then
            Me.REF_CommissionLevel1 = profileRow.REF_CommissionLevel1
        Else
            Me.REF_CommissionLevel1 = GetConfig().referrer_commission_level_1
        End If

        If (clsNullable.NullTo(profileRow.REF_CommissionLevel2) > 0) Then
            Me.REF_CommissionLevel2 = profileRow.REF_CommissionLevel2
        Else
            Me.REF_CommissionLevel2 = GetConfig().referrer_commission_level_2
        End If

        If (clsNullable.NullTo(profileRow.REF_CommissionLevel3) > 0) Then
            Me.REF_CommissionLevel3 = profileRow.REF_CommissionLevel3
        Else
            Me.REF_CommissionLevel3 = GetConfig().referrer_commission_level_3
        End If

        If (clsNullable.NullTo(profileRow.REF_CommissionLevel4) > 0) Then
            Me.REF_CommissionLevel4 = profileRow.REF_CommissionLevel4
        Else
            Me.REF_CommissionLevel4 = GetConfig().referrer_commission_level_4
        End If

        If (clsNullable.NullTo(profileRow.REF_CommissionLevel5) > 0) Then
            Me.REF_CommissionLevel5 = profileRow.REF_CommissionLevel5
        Else
            Me.REF_CommissionLevel5 = GetConfig().referrer_commission_level_5
        End If

        If (clsNullable.NullTo(profileRow.REF_CommissionConvRate) > 0) Then
            Me.REF_CommissionConvRate = profileRow.REF_CommissionConvRate
        Else
            Me.REF_CommissionConvRate = GetConfig().referrer_commission_conv_rate
        End If

        If (clsNullable.NullTo(profileRow.REF_CommissionPendingPercent) > 0) Then
            Me.REF_CommissionPendingPercent = profileRow.REF_CommissionPendingPercent
        Else
            Me.REF_CommissionPendingPercent = GetConfig().referrer_commission_pending_percent
        End If

        If (clsNullable.NullTo(profileRow.REF_CommissionPendingPeriod) > 0) Then
            Me.REF_CommissionPendingPeriod = profileRow.REF_CommissionPendingPeriod
        Else
            Me.REF_CommissionPendingPeriod = GetConfig().referrer_commission_pending_period
        End If

    End Sub


    Public Shared Function Load(affID As Integer, ByRef dt As DSReferrers.GetReferrerCommissions_AdminDataTable) As clsReferrerCommissions
        Dim drs As DataRow() = dt.Select("ProfileID = " & affID)
        Dim affComm As New clsReferrerCommissions(affID)
        'affComm.Load(affID)

        If (drs.Length > 0) Then
            Dim row As DSReferrers.GetReferrerCommissions_AdminRow = drs(0)
            affComm.REF_CommissionLevel0 = row.REF_CommissionLevel0
            affComm.REF_CommissionLevel1 = row.REF_CommissionLevel1
            affComm.REF_CommissionLevel2 = row.REF_CommissionLevel2
            affComm.REF_CommissionLevel3 = row.REF_CommissionLevel3
            affComm.REF_CommissionLevel4 = row.REF_CommissionLevel4
            affComm.REF_CommissionLevel5 = row.REF_CommissionLevel5
            affComm.REF_CommissionConvRate = row.REF_CommissionConvRate
            affComm.REF_CommissionPendingPercent = row.REF_CommissionPendingPercent
            affComm.REF_CommissionPendingPeriod = row.REF_CommissionPendingPeriod
        End If

        Return affComm
    End Function


    Public Function CalcCommissionForLevel(creditsSum As Double, level As Integer) As Double
        Dim result As Double

        Select Case level
            Case 0
                result = creditsSum * Me.REF_CommissionLevel0
            Case 1
                result = creditsSum * Me.REF_CommissionLevel1
            Case 2
                result = creditsSum * Me.REF_CommissionLevel2
            Case 3
                result = creditsSum * Me.REF_CommissionLevel3
            Case 4
                result = creditsSum * Me.REF_CommissionLevel4
            Case 5
                result = creditsSum * Me.REF_CommissionLevel5
        End Select

        Return result
    End Function

End Class

﻿Imports System.Drawing
Imports System.Drawing.Drawing2D
Imports System.Drawing.Imaging
Imports System.IO


Public NotInheritable Class PhotoUtils

    Private Sub New()
    End Sub

    ''' <summary>
    ''' Resizes picture proportionally to specified dimensions
    ''' </summary>
    ''' <param name="img"></param>
    ''' <param name="MaxWidth"></param>
    ''' <param name="MaxHeight"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")> Public Shared Function ResizeBitmap(ByRef img As System.Drawing.Image, MaxWidth As Integer, MaxHeight As Integer) As Bitmap

        If img.Width > MaxWidth OrElse img.Height > MaxHeight Then

            Dim widthRatio As Double = CDbl(img.Width) / CDbl(MaxWidth)
            Dim heightRatio As Double = CDbl(img.Height) / CDbl(MaxHeight)
            Dim ratio As Double = Math.Max(widthRatio, heightRatio)
            Dim newWidth As Integer = CInt(img.Width / ratio)
            Dim newHeight As Integer = CInt(img.Height / ratio)
            Dim tmpimg = New Bitmap(newWidth, newHeight)
            Dim rect As RectangleF = New RectangleF(0, 0, newWidth, newHeight)
            Using g As Graphics = Graphics.FromImage(tmpimg)
                g.DrawImage(img, rect)
            End Using
            Return tmpimg
            ' End Using
        Else
            'Using tmpimg = New Bitmap(img)
            Return New Bitmap(img)
            'End Using

        End If
    End Function


    Public Shared Function Inscribe(ByVal image As Image, ByVal size As Integer) As Image
        Return Inscribe(image, size, size)
    End Function

    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")> Public Shared Function Inscribe(ByVal image As Image, ByVal width As Integer, ByVal height As Integer) As Image
        Dim result As New Bitmap(width, height)
        Using graphics As Graphics = graphics.FromImage(result)
            Dim factor As Double = 1.0 * width / image.Width
            If image.Height * factor < height Then
                factor = 1.0 * height / image.Height
            End If
            Dim size As New Size(CInt(Fix(width / factor)), CInt(Fix(height / factor)))
            Dim sourceLocation As New Point((image.Width - size.Width) / 2, (image.Height - size.Height) / 2)

            SmoothGraphics(graphics)
            Dim destRect As New Rectangle(0, 0, width, height)
            Dim srcRect As New Rectangle(sourceLocation, size)
            graphics.DrawImage(image, destRect, srcRect, GraphicsUnit.Pixel)
        End Using
        Return result
        '  End Using
    End Function


    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")> Public Shared Function Fill(ByVal image As Image, ByVal width As Integer, ByVal height As Integer) As Image
        Dim result As New Bitmap(width, height)
        Using graphics As Graphics = graphics.FromImage(result)
            Dim factor As Double = 1.0 * width / image.Width
            If image.Height * factor < height Then
                factor = 1.0 * height / image.Height
            End If
            Dim size As New Size(width, height)
            Dim sourceLocation As New Point((image.Width - size.Width) / 2, (image.Height - size.Height) / 2)

            SmoothGraphics(graphics)
            graphics.DrawImage(image, New Rectangle(0, 0, width, height), New Rectangle(sourceLocation, size), GraphicsUnit.Pixel)
        End Using
        Return result
        ' End Using
    End Function

    Private Shared Sub SmoothGraphics(ByVal g As Graphics)
        g.SmoothingMode = SmoothingMode.AntiAlias
        g.InterpolationMode = InterpolationMode.HighQualityBicubic
        g.PixelOffsetMode = PixelOffsetMode.HighQuality
    End Sub

    Public Shared Sub SaveToJpeg(ByVal image As Image, ByVal output As Stream)
        image.Save(output, ImageFormat.Jpeg)
    End Sub

    Public Shared Sub SaveToJpeg(ByVal image As Image, ByVal fileName As String)
        image.Save(fileName, ImageFormat.Jpeg)
    End Sub



    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")> Public Shared Function AutosizeImage(ByVal image As Image,
                                  ByVal targetWidth As Integer,
                                  ByVal targetHeight As Integer) As Image


        Try
            If image IsNot Nothing Then
                Using imgOrg As Bitmap = image


                    Dim g As Graphics
                    Dim divideBy, divideByH, divideByW As Double
                    'imgOrg = DirectCast(Bitmap.FromFile(ImagePath), Bitmap)

                    divideByW = imgOrg.Width / targetWidth
                    divideByH = imgOrg.Height / targetHeight
                    If divideByW > 1 Or divideByH > 1 Then
                        If divideByW > divideByH Then
                            divideBy = divideByW
                        Else
                            divideBy = divideByH
                        End If

                        Dim imgShow = New Bitmap(CInt(CDbl(imgOrg.Width) / divideBy), CInt(CDbl(imgOrg.Height) / divideBy))


                        imgShow.SetResolution(imgOrg.HorizontalResolution, imgOrg.VerticalResolution)
                        g = Graphics.FromImage(imgShow)
                        g.InterpolationMode = Drawing2D.InterpolationMode.HighQualityBicubic
                        Dim rect As New Rectangle(0, 0, CInt(CDbl(imgOrg.Width) / divideBy), CInt(CDbl(imgOrg.Height) / divideBy))
                        g.DrawImage(imgOrg, rect, 0, 0, imgOrg.Width, imgOrg.Height, GraphicsUnit.Pixel)
                        g.Dispose()
                        Return imgShow
                        '   End Using
                    Else
                        Dim imgShow = New Bitmap(imgOrg.Width, imgOrg.Height)
                        imgShow.SetResolution(imgOrg.HorizontalResolution, imgOrg.VerticalResolution)
                        g = Graphics.FromImage(imgShow)
                        g.InterpolationMode = Drawing2D.InterpolationMode.HighQualityBicubic
                        Dim rect As New Rectangle(0, 0, imgOrg.Width, imgOrg.Height)
                        g.DrawImage(imgOrg, rect, 0, 0, imgOrg.Width, imgOrg.Height, GraphicsUnit.Pixel)
                        g.Dispose()
                        Return imgShow
                        '     End Using

                    End If

                End Using
            Else
                Return Nothing
            End If

        Catch ex As Exception
            Throw
        End Try


    End Function




    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")> Public Shared Function AutosizeImageAndInscribe(ByVal image As Image,
                                  ByVal targetWidth As Integer,
                                  ByVal targetHeight As Integer) As Image

        '    Dim imgShow As Bitmap = Nothing
        Dim result As New Bitmap(targetWidth, targetHeight)




        Try
            If image IsNot Nothing Then
                Using imgOrg As Bitmap = image


                    Dim divideBy, divideByH, divideByW As Double
                    'imgOrg = DirectCast(Bitmap.FromFile(ImagePath), Bitmap)

                    divideByW = imgOrg.Width / targetWidth
                    divideByH = imgOrg.Height / targetHeight
                    If divideByW > 1 Or divideByH > 1 Then
                        If divideByW > divideByH Then
                            divideBy = divideByW
                        Else
                            divideBy = divideByH
                        End If
                        Using imgShow = New Bitmap(CInt(CDbl(imgOrg.Width) / divideBy), CInt(CDbl(imgOrg.Height) / divideBy))
                            imgShow.SetResolution(imgOrg.HorizontalResolution, imgOrg.VerticalResolution)
                            Using g = Graphics.FromImage(imgShow)
                                g.InterpolationMode = Drawing2D.InterpolationMode.HighQualityBicubic
                                Dim rect As New Rectangle(0, 0, CInt(CDbl(imgOrg.Width) / divideBy), CInt(CDbl(imgOrg.Height) / divideBy))
                                g.DrawImage(imgOrg, rect, 0, 0, imgOrg.Width, imgOrg.Height, GraphicsUnit.Pixel)
                            End Using
                            Using graphics As Graphics = graphics.FromImage(result)
                                Dim factor As Double = 1.0 * targetWidth / imgShow.Width
                                If imgShow.Height * factor < targetHeight Then
                                    factor = 1.0 * targetHeight / imgShow.Height
                                End If
                                Dim size As New Size(CInt(Fix(targetWidth / factor)), CInt(Fix(targetHeight / factor)))
                                Dim sourceLocation As New Point((imgShow.Width - size.Width) / 2, (imgShow.Height - size.Height) / 2)

                                SmoothGraphics(graphics)
                                Dim destRect As New Rectangle(0, 0, targetWidth, targetHeight)
                                Dim srcRect As New Rectangle(sourceLocation, size)
                                graphics.DrawImage(imgShow, destRect, srcRect, GraphicsUnit.Pixel)
                                graphics.Dispose()
                            End Using
                        End Using

                    Else
                        Using imgShow = New Bitmap(imgOrg.Width, imgOrg.Height)
                            imgShow.SetResolution(imgOrg.HorizontalResolution, imgOrg.VerticalResolution)
                            Using g = Graphics.FromImage(imgShow)
                                g.InterpolationMode = Drawing2D.InterpolationMode.HighQualityBicubic
                                Dim rect As New Rectangle(0, 0, imgOrg.Width, imgOrg.Height)
                                g.DrawImage(imgOrg, rect, 0, 0, imgOrg.Width, imgOrg.Height, GraphicsUnit.Pixel)
                            End Using
                            Using graphics As Graphics = graphics.FromImage(result)
                                Dim factor As Double = 1.0 * targetWidth / imgShow.Width
                                If imgShow.Height * factor < targetHeight Then
                                    factor = 1.0 * targetHeight / imgShow.Height
                                End If
                                Dim size As New Size(CInt(Fix(targetWidth / factor)), CInt(Fix(targetHeight / factor)))
                                Dim sourceLocation As New Point((imgShow.Width - size.Width) / 2, (imgShow.Height - size.Height) / 2)

                                SmoothGraphics(graphics)
                                Dim destRect As New Rectangle(0, 0, targetWidth, targetHeight)
                                Dim srcRect As New Rectangle(sourceLocation, size)
                                graphics.DrawImage(imgShow, destRect, srcRect, GraphicsUnit.Pixel)
                                graphics.Dispose()
                            End Using
                        End Using

                    End If


                End Using
            End If

        Catch ex As Exception
            Throw
        End Try
        Return result
        ' End Using

    End Function





    Public Shared Function GetEmptyImage() As Image
        Using B As Bitmap = New Bitmap(1, 1)
            Using G As Graphics = Graphics.FromImage(B)
                G.FillRectangle(Brushes.White, 0, 0, 1, 1)
                Dim Bresult As Bitmap = New Bitmap(1, 1, G)
                Return Bresult

            End Using
        End Using
    End Function


    ' Private Sub ZoomOut(ByVal pic As PictureBox)
    '     If (pic.Width > (500 / MINMAX)) AndAlso (pic.Height > (500 / MINMAX)) Then

    '         pic.Width = Convert.ToInt32(pic.Width / ZOOMFACTOR)
    '         pic.Height = Convert.ToInt32(pic.Height / ZOOMFACTOR)
    '         pic.SizeMode = PictureBoxSizeMode.StretchImage
    '         'pbSourceImage.Location = New Point(0, 0)
    '     End If
    'End SubPrivate Sub ZoomIn(ByVal pic As Image, ByVal picbox As PictureBox)

    '     If (pic.Width < (MINMAX * picbox.Width)) AndAlso (pic.Height < (MINMAX * picbox.Height)) Then

    '         picbox.Width = Convert.ToInt32(pic.Width * ZOOMFACTOR)
    '         picbox.Height = Convert.ToInt32(pic.Height * ZOOMFACTOR)
    '         picbox.SizeMode = PictureBoxSizeMode.StretchImage
    '     End If
    ' End Sub

    ' Private ZOOMFACTOR As Double = 1.25    ' = 25% smaller or larger
    ' Private MINMAX As Integer = 5





    'Public Shared Function ImageToByteArray(ByRef imageIn As System.Drawing.Image) As Byte()
    '    Dim ImageData As Byte() = Nothing
    '    Dim converter As New ImageConverter
    '    ImageData = converter.ConvertTo(Args.BinaryData, GetType(Byte()))

    '    Dim ms As New MemoryStream()
    '    imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Gif)
    '    Return ms.ToArray()
    'End Function

    'Public Shared Function ByteArrayToImage(ByRef byteArrayIn As Byte()) As Image
    '    Dim ms As New MemoryStream(byteArrayIn)
    '    Dim returnImage As Image = Image.FromStream(ms)
    '    Return returnImage
    'End Function



End Class




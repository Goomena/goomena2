﻿Imports Dating.Server.Datasets.DLL

Public Class clsSendingMobileNotificationsHandler
    Implements IDisposable

    Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)

    Public Function GetNextNotification() As SYS_MobileNotificationsQueue
        Dim file As SYS_MobileNotificationsQueue
        Try
            file = (From itm In _CMSDBDataContext.SYS_MobileNotificationsQueues
                   Select itm).FirstOrDefault()

            _CMSDBDataContext.SubmitChanges()

        Catch ex As Exception
            Throw
        Finally
        End Try

        Return file
    End Function


    Public Sub DeleteSentNotification(itemId As Long)
        Try
            Dim email = From itm In _CMSDBDataContext.SYS_MobileNotificationsQueues
                   Where itm.QueueID = itemId
                   Select itm


            _CMSDBDataContext.SYS_MobileNotificationsQueues.DeleteAllOnSubmit(email)
            _CMSDBDataContext.SubmitChanges()
        Catch ex As Exception
            Throw
        Finally
        End Try

    End Sub


    Public Sub Dispose() Implements IDisposable.Dispose
        If (_CMSDBDataContext IsNot Nothing) Then _CMSDBDataContext.Dispose()
    End Sub

End Class

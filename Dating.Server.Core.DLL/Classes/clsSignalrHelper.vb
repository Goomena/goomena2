﻿Imports Dating.Server.Datasets.DLL
Public Class clsSignalrHelper

    Public Shared Sub GetSignalRConnectionStringsByProfileId(ByVal ds As dsConnectedUsers, ByVal _currentId As Integer, ByVal OtherProfileId As Integer, ByVal _country As String)
        Using con As New SqlClient.SqlConnection(DataHelpers.ConnectionString)
            Using ta As dsConnectedUsersTableAdapters.Eus_ConnectionStringsTableAdapter = New dsConnectedUsersTableAdapters.Eus_ConnectionStringsTableAdapter With {.Connection = con}
                ta.FillConnectedForSpam(ds.Eus_ConnectionStrings, _currentId, False, _country, 1, OtherProfileId)
            End Using
        End Using
    End Sub
    Public Shared Sub GetSignalRConnectionStrings(ByVal ds As dsConnectedUsers, ByVal _currentId As Integer, MyGender As Integer, _HideMeFromUsersInDifferentCountry As Boolean, ByVal _country As String)
        Dim gender As Integer = If(MyGender = 1, 2, 1)
        Using con As New SqlClient.SqlConnection(DataHelpers.ConnectionString)
            Using ta As dsConnectedUsersTableAdapters.Eus_ConnectionStringsTableAdapter = New dsConnectedUsersTableAdapters.Eus_ConnectionStringsTableAdapter With {.Connection = con}
                ta.FillConnectedForSpam(ds.Eus_ConnectionStrings, _currentId, _HideMeFromUsersInDifferentCountry, _country, gender, -1)
            End Using
        End Using
    End Sub
    Public Shared Function DeleteUserByConnectionId(ByVal ConnectionId As String, ByVal ProcessId As Integer) As dsConnectedUsers.EUS_ConnectedUsersRow()
        Dim re As New List(Of dsConnectedUsers.EUS_ConnectedUsersRow)

        Dim ds As New dsConnectedUsers
        Using con As New SqlClient.SqlConnection(DataHelpers.ConnectionString)
            Using cmd As New dsConnectedUsersTableAdapters.EUS_ConnectedUsersTableAdapter
                cmd.FillByProccesIdAndConnectionId(ds.EUS_ConnectedUsers, ConnectionId, ProcessId)
                cmd.DeleteProccessIdAndConnetionId(ConnectionId, ProcessId)
            End Using
        End Using
        For Each r As dsConnectedUsers.EUS_ConnectedUsersRow In ds.EUS_ConnectedUsers.Rows
            re.Add(r)
        Next

        Return re.ToArray
    End Function
    Public Shared Sub DeleteUser(ByVal ConnectionId As String, ByVal ProcessId As Integer)
        Dim re As New List(Of dsConnectedUsers.EUS_ConnectedUsersRow)
        Dim ds As New dsConnectedUsers
        Using con As New SqlClient.SqlConnection(DataHelpers.ConnectionString)
            Using cmd As New dsConnectedUsersTableAdapters.EUS_ConnectedUsersTableAdapter
                cmd.FillByProccesIdAndConnectionId(ds.EUS_ConnectedUsers, ConnectionId, ProcessId)
                cmd.DeleteProccessIdAndConnetionId(ConnectionId, ProcessId)
            End Using
        End Using
        For Each r As dsConnectedUsers.EUS_ConnectedUsersRow In ds.EUS_ConnectedUsers.Rows
            re.Add(r)
        Next
    End Sub
    Public Shared Sub AddUser(ByVal ProfileId As Integer, ByVal MirrorProfileId As Integer, LoginName As String, _GrenderId As Integer, ByVal processId As Integer, ByVal ConnectionId As String)
        Dim ds As New dsConnectedUsers
        Dim r As dsConnectedUsers.EUS_ConnectedUsersRow = ds.EUS_ConnectedUsers.NewRow
        r.ProfileId = ProfileId
        r.MirrorProfileId = MirrorProfileId
        r.LoginName = LoginName
        r.GenderId = _GrenderId
        r.DateTime = Now.ToUniversalTime
        r.ConnectionId = ConnectionId
        r.ProcessId = Process.GetCurrentProcess().Id
        ds.EUS_ConnectedUsers.Rows.Add(r)
        Using con As New SqlClient.SqlConnection(DataHelpers.ConnectionString)
            Using cmd As New dsConnectedUsersTableAdapters.EUS_ConnectedUsersTableAdapter With {.Connection = con}
                Try
                    cmd.Update(ds.EUS_ConnectedUsers)
                Catch ex As Exception
                    If Not ex.Message.Contains("PK_ConnectedUsers") Then
                        Throw
                    End If
                End Try

            End Using
        End Using
    End Sub
    Public Shared Function AddNotificationInDatabase(ByVal _FromProfileId As Integer, ByVal _ToProfileId As Integer, ByVal NotificationType As SignarNotificationType, ByVal Message As String, ByVal _CustomerPhotoId As Long?, Optional ByVal _Count As Integer = 0) As Long
        Dim Id As Long = -1
        Using con As New SqlClient.SqlConnection(DataHelpers.ConnectionString)
            Using cmd As New dsConnectedUsersTableAdapters.Eus_CustomerSignalrReceivedNotificationsTableAdapter With {.Connection = con}
                Dim ds As New dsConnectedUsers
                Dim dr As dsConnectedUsers.Eus_CustomerSignalrReceivedNotificationsRow = ds.Eus_CustomerSignalrReceivedNotifications.NewRow
                dr.SignalrNotificationType = NotificationType
                dr.FromProfileId = _FromProfileId
                dr.ToProfileId = _ToProfileId
                dr.DatetimeReceived = Now.ToUniversalTime
                dr.HasReadIt = False
                dr.Message = Message
                dr.CustomerPhotoId = _CustomerPhotoId
                dr.Count = _Count
                ds.Eus_CustomerSignalrReceivedNotifications.Rows.Add(dr)
                cmd.Update(ds)
                Id = dr.Eus_CustomerSignalrReceivedNotificationId
            End Using
        End Using
        Return Id
    End Function

    Public Shared Function GetLastNotificationMessageId(ByVal ProfileId As Integer) As Long
        Dim re As Long? = 0
        Using con As New SqlClient.SqlConnection(DataHelpers.ConnectionString)
            Using cmd As New dsConnectedUsersTableAdapters.Eus_CustomerSignalrReceivedNotificationsTableAdapter With {.Connection = con}
                re = cmd.GetlastId(ProfileId)
            End Using
        End Using
        If Not re.HasValue Then
            re = 0
        End If
        Return re
    End Function
    Public Shared Sub GetNotificationMessage(ByRef Ds As dsConnectedUsers, ByVal NotificationId As Long, ByVal ProfileId As Integer)
        Using con As New SqlClient.SqlConnection(DataHelpers.ConnectionString)
            Using cmd As New dsConnectedUsersTableAdapters.Eus_CustomerSignalrReceivedNotificationsTableAdapter With {.Connection = con}
                cmd.FillidAndProfileId(Ds.Eus_CustomerSignalrReceivedNotifications, NotificationId, ProfileId)
            End Using
        End Using
    End Sub
    Public Shared Sub FillNotificationsMessagesByLastId(ByRef Ds As dsConnectedUsers, ByVal NotificationId As Long, ByVal ProfileId As Integer)
        Using con As New SqlClient.SqlConnection(DataHelpers.ConnectionString)
            Using cmd As New dsConnectedUsersTableAdapters.Eus_CustomerSignalrReceivedNotificationsTableAdapter With {.Connection = con}
                cmd.FillByLastId(Ds.Eus_CustomerSignalrReceivedNotifications, NotificationId, ProfileId)
            End Using
        End Using
    End Sub
    Public Shared Sub FillCustomerPhoto(ByRef Ds As dsConnectedUsers, ByVal _PhotoId As Long, ByVal ProfileId As Integer, ByVal _MyprofileId As Integer)
        Using con As New SqlClient.SqlConnection(DataHelpers.ConnectionString)
            Using cmd As New dsConnectedUsersTableAdapters.EUS_CustomerPhotosTableAdapter With {.Connection = con}
                cmd.FillByPhotoIdAndCustomerId2(Ds.EUS_CustomerPhotos, _PhotoId, ProfileId, _MyprofileId)
            End Using
        End Using
    End Sub
    Public Shared Function CanNotifyForNotificationType(ByVal Type As SignarNotificationType, ByVal FromPrId As Integer, ByVal ToPrId As Integer) As Boolean
        Dim re As Boolean = False
        Dim i As Integer? = 0
        Using con As New SqlClient.SqlConnection(DataHelpers.ConnectionString)
            Using cmd As New dsConnectedUsersTableAdapters.Eus_CustomerSignalrReceivedNotificationsTableAdapter With {.Connection = con}
                Integer.TryParse(cmd.CountNotificationsTypeFromProfile(Type, FromPrId, ToPrId), i)
            End Using
        End Using
        If Type = SignarNotificationType.Favorite Then
            If i.HasValue AndAlso i > 0 Then
                re = False
            Else
                re = True
            End If
        End If
        Return True
    End Function

    Public Shared Sub GetProfile(ByVal ProfileId As Integer, ByRef ds As dsConnectedUsers)
        Dim LastActivityUTCDate As DateTime = Date.UtcNow
        Dim LastActivityRecentlyUTCDate As DateTime = Date.UtcNow
        Try
            Dim mins As Integer = clsConfigValues.Get__members_online_minutes()
            LastActivityUTCDate = Date.UtcNow.AddMinutes(-mins)
        Catch
        End Try
        Try
            Dim hours As Integer = clsConfigValues.Get__members_online_recently_hours()
            LastActivityRecentlyUTCDate = Date.UtcNow.AddHours(-hours)
        Catch
        End Try
        Using con As New SqlClient.SqlConnection(DataHelpers.ConnectionString)
            Using cmd As New dsConnectedUsersTableAdapters.GetSignalRProfileTableAdapter With {.Connection = con}
                cmd.Fill(ds.GetSignalRProfile, LastActivityUTCDate, LastActivityRecentlyUTCDate, ProfileId)
            End Using
        End Using
    End Sub
    Public Shared Function GetProfile(ByVal ProfileId As Integer, ByRef ds As DSMembers) As Boolean
        Dim re As Boolean = False
        Using con As New SqlClient.SqlConnection(DataHelpers.ConnectionString)
            Using cmd As New DSMembersTableAdapters.EUS_ProfilesTableAdapter With {.Connection = con}
                cmd.FillForBroadCastById(ds.EUS_Profiles, ProfileId)
                re = True
            End Using
        End Using
        Return re
    End Function



    Public Shared Function PerformAdminSiteLogin(ByVal LoginName As String, ByVal PassWord As String) As Boolean
        Dim re As Boolean = False
        Using con As New SqlClient.SqlConnection(DataHelpers.ConnectionString)
            Using SYS_UsersDataTable As New DSSYSData.SYS_UsersDataTable
                Using ta As New DSSYSDataTableAdapters.SYS_UsersTableAdapter With {.Connection = con}
                    ta.FillBy_SYS_Users_GetUsers(SYS_UsersDataTable, Nothing, LoginName, PassWord)
                    If SYS_UsersDataTable.Rows.Count > 0 Then
                        Dim SYS_UsersRow As DSSYSData.SYS_UsersRow = SYS_UsersDataTable.Rows(0)
                        If SYS_UsersRow.IsActive Then
                            re = True
                        End If
                    End If
                End Using
            End Using
        End Using
        Return re
    End Function

End Class

Public Class PrivacySettings
    Public Property HideMeFromUsersInDifferentCountry As Boolean = False
  
End Class
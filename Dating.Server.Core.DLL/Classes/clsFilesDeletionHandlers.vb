﻿Imports Dating.Server.Datasets.DLL

Public Class clsFilesDeletionHandlers


    'Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


    Public Sub AddFile(filePath As String)
        Try
            Dim file As New SYS_FilesForDeletion
            file.Datecreated = Date.UtcNow
            file.Path = filePath
            Using cmdb As New CMSDBDataContext(DataHelpers.ConnectionString)
                cmdb.SYS_FilesForDeletions.InsertOnSubmit(file)
                cmdb.SubmitChanges()
            End Using
         

        Catch ex As Exception
            Throw
        Finally
        End Try
    End Sub


    Public Function GetNextFile() As SYS_FilesForDeletion
        Dim file As SYS_FilesForDeletion
        Try
            Using cmdb As New CMSDBDataContext(DataHelpers.ConnectionString)
                file = (From itm In cmdb.SYS_FilesForDeletions
                  Where itm.Datedeleted Is Nothing
                  Select itm).FirstOrDefault()

                cmdb.SubmitChanges()
            End Using
           

        Catch ex As Exception
            Throw
        Finally
        End Try

        Return file
    End Function


    Public Sub SetFileDeleted(file1 As SYS_FilesForDeletion)
        Try
            Using cmdb As New CMSDBDataContext(DataHelpers.ConnectionString)


                Dim file As SYS_FilesForDeletion = (From itm In cmdb.SYS_FilesForDeletions
                       Where itm.Id = file1.Id
                       Select itm).FirstOrDefault()
                file.Datedeleted = Date.UtcNow

                If (Len(file1.Exception) > 1023) Then file1.Exception = file1.Exception.Remove(1023)
                file.Exception = file1.Exception

                cmdb.SubmitChanges()
            End Using
        Catch ex As Exception
            Throw
        Finally
        End Try

    End Sub




End Class

﻿Imports System.Security.Cryptography
Imports System.Text

Public Class clsEncryptData
    Private Shared Function fillSpace(ByRef txt As String, ByVal length As Integer) As String
        Dim tmp As String = txt
        For i As Integer = 0 To (length - txt.Length)
            tmp = "0" & tmp
        Next
        Return tmp
    End Function
    Private Shared Function getRandoms(ByVal fromNumber As Integer, ByVal toNumber As Integer, ByRef notin As HashSet(Of Integer)) As Integer
        Dim RandomClass As New Random()
        Dim RememberSet As HashSet(Of Integer) = If(notin Is Nothing, New HashSet(Of Integer), notin)
        Dim re As Integer
        Dim RandomNumber As Integer
        Dim count As Integer = RememberSet.Count + 1
        While RememberSet.Count < count
            RandomNumber = RandomClass.Next(fromNumber, toNumber)
            If RememberSet.Add(RandomNumber) Then
                re = RandomNumber
            End If
        End While
        Return re
    End Function

    Public Shared Function getCouponCode(ByVal id As Long) As String
        Dim re As String = ""
        Dim reIntegers As New HashSet(Of Integer)
        re = getRandoms(11, 99, reIntegers).ToString & (((DateTime.UtcNow.Year - 2000) + 60) - DateTime.UtcNow.Second).ToString & "-" & getRandoms(1, 9, reIntegers) & ((DateTime.UtcNow.Day + 61) - DateTime.UtcNow.Second) & "-" & getRandoms(1, 9, reIntegers) & ((DateTime.UtcNow.Month + 87) - DateTime.UtcNow.Second).ToString & "-" & fillSpace(id, 5)
        '    Dim str As String = r.ProfileId.ToString & "," & r.LoginName & "$" & r.CouponCode & "#" & r.DatetimeCreated.ToString("yyyyMMddHHmmss") & r.SessionID
        Return re
    End Function

    Public Shared Function CreateMd5hash(ByVal PrId As Integer, ByVal Lname As String, ByVal CouponCode As String, ByVal dt As DateTime, ByVal sesId As String)
        Dim str As String = PrId & "," & Lname & "$" & CouponCode & "#" & dt.ToString("yyyyMMddHHmmss") & sesId
        str = CreateMd5hash(str)
        Return str
    End Function
    Public Shared Function CreateMd5hash(ByVal tmp As String)
        Dim str As String = tmp
        Using DES As New TripleDESCryptoServiceProvider
            Using MD5 As New MD5CryptoServiceProvider
                DES.Key = MD5.ComputeHash(ASCIIEncoding.ASCII.GetBytes("#$%Goomena!@"))
                DES.Mode = CipherMode.ECB
                Dim Buffer As Byte() = ASCIIEncoding.ASCII.GetBytes(str)
                str = Convert.ToBase64String(DES.CreateEncryptor().TransformFinalBlock(Buffer, 0, Buffer.Length))
            End Using
        End Using
        Return str
    End Function
End Class

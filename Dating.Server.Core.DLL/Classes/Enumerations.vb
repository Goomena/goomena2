﻿
Public Enum NotificationType
    None = 0
    Likes = 1
    Offers = 2
    Message = 4
    NewMembersNear = 5
    Unlock = 6
    AutoNotification = 7
    AutoNotificationForPhoto = 8
    CreditsExpired = 10
    CreditsExpiring = 11
    ProfileViewed = 12
End Enum
Public Enum SignarNotificationType
    Message = 1
    Birthday = 2
    Register = 3
    NewPhoto = 4
    Favorite = 5
End Enum

Public Enum SearchSortEnum
    None = 0
    NewestMember = 1
    OldestMember = 2
    RecentlyLoggedIn = 3
    NearestDistance = 4
    NewestNearMember = 8
    NearMemberNewest = 10
    Birthday = 16
End Enum



Public Enum MyListsSortEnum
    None = 0
    Recent = 3
    Oldest = 4
End Enum



Public Enum OffersSortEnum
    None = 0
    OfferAmountHighest = 1
    OfferAmountLowest = 2
    RecentOffers = 3
    OldestOffers = 4
End Enum


Public Enum DatesSortEnum
    None = 0
    NewDates = 2
    ByDate = 4
    ByStatus = 5
    ByLastDating = 6
End Enum



Public Enum MessagesSortEnum
    None = 0
    Recent = 1
    Oldest = 2
End Enum




''' <summary>
''' Match to EUS_LISTS_AccountType database table,
''' Integer value maps to AccountTypeId column
''' </summary>
''' <remarks></remarks>
Public Enum GenerousAttractiveEnum
    None = 1
    Generous = 2
    Attractive = 3
End Enum



Public Enum PhotosFilterEnum As Integer
    None = 0
    AllPhotos = 1
    NewPhotos = 2
    RejectedPhotos = 4
    ApprovedPhotos = 8
    DeletedPhotos = 16
    AutoApprovedPhotos = 32
    CustomerDefaultPhoto = 64
    ViewPhotosOnFrontPage = 128
    ViewPhotosOnGrid = 256
    ViewMonitor = 512
End Enum




Public Enum ProfileStatusEnum As Integer
    None = 0
    NewProfile = 1
    Updating = 2
    Approved = 4
    Rejected = 8
    Deleted = 16
    DeletedByUser = 18
    LIMITED = 99
    AutoApprovedProfile = -100
    ApprovedProfileWithPhotos = -200
    ApprovedProfileWithoutPhotos = -300
    MembersOnline = -400
    ApprovedOrUpdating = -500
    ApprovedOrUpdatingProfileWithPhotos = -510
    ApprovedOrUpdatingProfileWithoutPhotos = -520
    ReferrerWithBonus = -530
    ProfilesOnQuarantine = -540
    VirtualProfiles = -550
    RegisteredFromMobile = -560
    SearchProfile = -570
    SpammerProfiles = 30
    'ReferralUpdating = -530
    'ReferralNew = -540
    'ReferralRejected = -550
    'ReferralDeleted = -560
    'ReferralApproved = -570
End Enum



Public Enum ProfileModifiedEnum
    None = 0
    UpdatingPhotos = 1
    UpdatingDocs = 2
End Enum



Public Enum TypeOfDatingEnum
    None = 0
    ShortTermRelationship = 1
    Friendship = 2
    LongTermRelationship = 3
    MutuallyBeneficialArrangements = 4
    MarriedDating = 5
    AdultDating_Casual = 6
End Enum



Public Enum PaymentMethods
    None = 0
    PayPal = 1
    DaoPay = 2
    MoneyBookers = 3
    AlertPay = 4
    PaySafe = 5
    CreditCard = 6
    WebMoney = 7
    fastspring = 8

    PaymentWall = 9
    BitPay = 10
    DirectPaypal = 11
    Reseller = 12
    Viva = 13

    'TwoCO = 101
    'Regnow = 102
End Enum

'Public Enum PaymentMethods
'    None = 0
'    AlertPay = 1
'    PaySafe = 2
'    CreditCard = 3
'    WebMoney = 4
'    PayPal = 5
'    TwoCO = 6
'    Regnow = 7
'End Enum


Public Enum UnlockType
    None = 0
    UNLOCK_CONVERSATION = 1
    UNLOCK_MESSAGE_READ = 2
    UNLOCK_MESSAGE_SEND = 3
    UNLOCK_MESSAGE_READ_FREE = 8
    UNLOCK_MESSAGE_SEND_FREE = 9
End Enum


Public Enum MessagesViewEnum
    None = 0
    INBOX = 1
    SENT = 2
    CONVERSATION = 4
    NEWMESSAGES = 8
    TRASH = 16
    SEND_NEW_MESSAGE = 32
End Enum






Public Enum OfferControlCommandEnum
    None = 0
    WINK = 1
    FAVORITE = 2
    OFFERACCEPT = 3
    OFFERCOUNTER = 4
    REJECTTYPE = 5
    REJECTFAR = 6
    REJECTBAD = 7
    REJECTEXPECTATIONS = 8
    CANCELWINK = 10
    CANCELOFFER = 11
    DELETEOFFER = 20
    POKE = 90
    CANCELPOKE = 91
    UNLOCKOFFER = 200
    UNBLOCK = 300
    UNFAVORITE = 400
    TRYWINK = 500
    UNWINK = 550
    TRYCREATEOFFER = 600
End Enum



Public Enum ReferrerStopSendingMessageEnum
    None = 0
    RecipientHasNoPhoto = 1
    MaxAllowedMessagesSent = 2
End Enum

Public Enum NearestMembersOptions
    NearestMembers = 0
    NewMembers = 1
End Enum


Public Enum ProfilesYouHaveViewedOptions
    ProfilesYouHaveViewed = 0
    WhoViewedMyProfile = 1
    MyFavoriteList = 2
    ProfilesHaveBirthdayToday = 4
End Enum



Public Enum PhotoSize
    Thumb = 0
    Original = 1
    D150 = 2
    D350 = 4
End Enum


Public Enum MemberCountersEnum
    None = 0
    WhoViewedMe = 1
    WhoFavoritedMe = 2
    WhoSharedPhotos = 4
    NewDates = 8
    NewLikes = 16
    NewOffers = 32
    NewMessages = 64
End Enum



Public Enum MemberActionTypeEnum
    None = 0
    Offer = 1
    Likes = 2
    Message = 4
    Poke = 8
    Dates = 16
    Favorite = 32
    Unfavorite = 64
    Viewed = 128
    DefaultPhotoCanged = 256
End Enum


Public Enum PerformMessageSendResultEnum
    none = 0
    success = 1
    fail_CreditsUNLOCK_CONVERSATION = 2
    fail_CreditsUNLOCK_MESSAGE_SEND = 3
End Enum




Public Enum ReadyMessagesActionsEnum
    None = 0
    After_Man_Like = 1
    After_profile_view = 2
End Enum


Public Enum FreeMessagesArchiveEnum
    None = 0
    FreeMessage = 1
    SubscriptionMessage = 2
End Enum



Public Enum OfferStatusEnum
    None = 0
    PENDING = 1
    COUNTER = 10
    ACCEPTED = 50
    CANCELED = 60
    REPLACINGBYCOUNTER = 80
    REJECTED = 90
    REJECTBAD = 100
    REJECTEXPECTATIONS = 101
    REJECTFAR = 102
    REJECTTYPE = 103
    UNLOCKED = 200

    LIKE_ACCEEPTED_WITH_OFFER = 300
    LIKE_ACCEEPTED_WITH_MESSAGE = 301
    LIKE_ACCEEPTED_WITH_POKE = 302

    POKE_ACCEEPTED_WITH_OFFER = 400
    POKE_ACCEEPTED_WITH_MESSAGE = 401

    OFFER_ACCEEPTED_WITH_MESSAGE = 500
End Enum



Public Enum OfferTypeEnum
    None = 0
    WINK = 1
    OFFERNEW = 10
    OFFERCOUNTER = 11
    POKE = 90
    NEWDATE = 100
End Enum


Public Enum ActionsOnProfileStatusEnum
    None = 0
    ProfileNotFound = 1
    Exception = 2
End Enum



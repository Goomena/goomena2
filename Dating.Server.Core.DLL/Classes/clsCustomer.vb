﻿Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Core.DLL
Imports System.Configuration
Imports System.Web
Imports System.Text.RegularExpressions
Imports System.Globalization

Public Class clsCustomer
    Public Shared Function RecordThatTheUserHasBeenInformedForTheSMSLimit(ByVal ProfileId As Integer) As Boolean
        Dim re As Boolean = False
        Try
            Dim sql As String = ""
            '   Using dt As New DataTable()
            sql = <sql><![CDATA[
  Update [dbo].[EUS_ProfilesPrivacySettings]
  set [SendedSmsLimitDate]=@DateTime
  where ProfileID =@ProfileId 
  or MirrorProfileID =@ProfileId
]]></sql>.Value
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                    command.Parameters.Add(New SqlClient.SqlParameter("@ProfileId", clsNullable.DBNullToInteger(ProfileId, -1)))
                    command.Parameters.Add(New SqlClient.SqlParameter("@DateTime", DateTime.UtcNow))
                    Dim i As Integer = DataHelpers.ExecuteNonQuery(command)
                    If i > 0 Then
                        re = True
                    Else
                        re = False
                    End If
                End Using
            End Using

        Catch ex As Exception
            clsMyMail.TrySendMailSupport(ConfigurationManager.AppSettings("ExceptionsEmail"), "On RecordThatTheUserHasBeenInformedForTheSMSLimit prID:" & ProfileId, ex.ToString())
        End Try
        Return re
    End Function
    Public Shared Function GetSmsReloadLimitationsByCountryCode(CountryCode As String) As clsReturnSmsLimitations
        Dim re As New clsReturnSmsLimitations
        Try
            Dim sql As String = ""



            sql = <sql><![CDATA[
SELECT Iso
      ,Isnull([HasSmsLimit],0) as [HasSmsLimit]
      ,Isnull([SmsMoneyLimit],0) as [SmsMoneyLimit]
      ,Isnull([SmsPeriodLimitInDays],0) as [SmsPeriodLimitInDays]
  FROM [dbo].[SYS_CountriesGEO]
  where Iso=@Iso
]]></sql>.Value
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                    command.Parameters.Add(New SqlClient.SqlParameter("@Iso", CountryCode))
                    Using dt As DataTable = DataHelpers.GetDataTable(command)
                        If dt.Rows.Count > 0 Then
                            re.Enabled = clsNullable.DBNullToBoolean(dt.Rows(0).Item("HasSmsLimit"), False)
                            re.PeriodInDays = clsNullable.DBNullToInteger(dt.Rows(0).Item("SmsPeriodLimitInDays"), 0)
                            re.SmsCountLimitation = clsNullable.DBNullToInteger(dt.Rows(0).Item("SmsMoneyLimit"), 0)
                        End If
                    End Using
                End Using
            End Using
        Catch ex As Exception
            clsMyMail.TrySendMailSupport(ConfigurationManager.AppSettings("ExceptionsEmail"), "On Getting SmsLimitations with Country Code:" & CountryCode, ex.ToString())
        End Try
        Return re
    End Function
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")> Public Shared Function GetProfileSmsReloadLimitations(ByVal _CountryCode As String, ByVal ProfileId As Integer, ByVal MirrorProfileId As Integer, ByVal dtNow As DateTime) As SmsReloadReturnAction
        Dim re As New SmsReloadReturnAction
        Try
            Dim CountryLimitations As clsReturnSmsLimitations = GetSmsReloadLimitationsByCountryCode(_CountryCode)
            If CountryLimitations.Enabled Then
                Dim TransactionsCount As Integer = 0
                Dim MinDateTime As DateTime = Now
                Dim sql As String = ""

                sql = <sql><![CDATA[
SELECT Count( [CustomerTransactionID]) as smsTransactions
      ,Min([Date_Time]) as MinDateTime
      FROM [dbo].[EUS_CustomerTransaction]  with (nolock)
  where CustomerID in (@ProfileId,@MirrorProfileId) and [PaymentMethods] ='SMS' and [Date_Time]>=@DateTime
]]></sql>.Value
                Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                    Using command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                        command.Parameters.Add(New SqlClient.SqlParameter("@ProfileId", clsNullable.DBNullToInteger(ProfileId, -1)))
                        command.Parameters.Add(New SqlClient.SqlParameter("@MirrorProfileId", clsNullable.DBNullToInteger(MirrorProfileId, -1)))
                        command.Parameters.Add(New SqlClient.SqlParameter("@DateTime", dtNow.AddDays(CountryLimitations.PeriodInDays * (-1))))
                        Using dt As DataTable = DataHelpers.GetDataTable(command)
                            If dt.Rows.Count > 0 Then
                                TransactionsCount = clsNullable.DBNullToInteger(dt.Rows(0).Item("smsTransactions"), 0)
                                MinDateTime = clsNullable.DBNullToDateTimeNull(dt.Rows(0).Item("MinDateTime"), Now.AddDays(1))
                                If TransactionsCount < CountryLimitations.SmsCountLimitation Then
                                    re.WhatToDo = SmsTransactionNextAction.DoNothing
                                ElseIf TransactionsCount = CountryLimitations.SmsCountLimitation Then
                                    Dim dif As Integer = DateDiff(DateInterval.Day, MinDateTime, dtNow, FirstDayOfWeek.Monday, FirstWeekOfYear.Jan1)
                                    If dif <= CountryLimitations.PeriodInDays Then
                                        dif = (CountryLimitations.PeriodInDays - dif) + 1
                                        re.DateSmsLimitIsOff = dtNow.AddDays(dif)
                                    End If
                                    re.WhatToDo = SmsTransactionNextAction.WarnHimAboutTheLimit
                                Else

                                    sql = <sql><![CDATA[
SELECT 
      [SendedSmsLimitDate]
  FROM [dbo].[EUS_ProfilesPrivacySettings] with (nolock)
  where ProfileID =@ProfileId 
  or MirrorProfileID =@ProfileId
]]></sql>.Value

                                    Try
                                        Using command1 As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                                            command1.Parameters.Add(New SqlClient.SqlParameter("@ProfileId", clsNullable.DBNullToInteger(ProfileId, -1)))
                                            Using dt2 = DataHelpers.GetDataTable(command1)
                                                If dt2.Rows.Count > 0 Then
                                                    If dt2.Rows(0).IsNull("SendedSmsLimitDate") Then
                                                        re.WhatToDo = SmsTransactionNextAction.WarnHimAboutTheLimit
                                                    Else
                                                        Try
                                                            Dim dif As Integer = DateDiff(DateInterval.Day, MinDateTime, dtNow, FirstDayOfWeek.Monday, FirstWeekOfYear.Jan1)
                                                            If dif <= CountryLimitations.PeriodInDays Then
                                                                dif = (CountryLimitations.PeriodInDays - dif) + 1
                                                                re.DateSmsLimitIsOff = dtNow.AddDays(dif)
                                                            End If
                                                            re.WhatToDo = SmsTransactionNextAction.DoNotReloadHisCredits
                                                        Catch ex As Exception
                                                            re.WhatToDo = SmsTransactionNextAction.DoNotReloadHisCredits
                                                        End Try
                                                    End If
                                                Else
                                                    re.WhatToDo = SmsTransactionNextAction.DoNothing
                                                End If
                                            End Using
                                        End Using

                                    Catch ex As Exception
                                        clsMyMail.TrySendMailSupport(ConfigurationManager.AppSettings("ExceptionsEmail"), "On Getting SendedSmsLimitDate from PrivacySettings PrId:" & ProfileId, ex.ToString())
                                    End Try

                                End If
                            Else
                                re.WhatToDo = SmsTransactionNextAction.DoNothing
                            End If
                        End Using

                    End Using
                End Using
            Else
                re.WhatToDo = SmsTransactionNextAction.DoNothing
            End If
        Catch ex As Exception
            clsMyMail.TrySendMailSupport(ConfigurationManager.AppSettings("ExceptionsEmail"), "On GetProfileSmsReloadLimitations PrId:" & ProfileId, ex.ToString())
        End Try
        Return re
    End Function

    Public Shared Sub sendPayment(ByVal amount As String,
                           ByVal type As String,
                           ByVal quantity As String,
                           ByVal Email As String,
                           ByVal CustomerID As Integer,
                           ByVal CustomReferrer As String,
                           ByVal oURL As String,
                           ByVal paymentMethod As String,
                           ByVal promoCode As String,
                           ByVal SalesSiteID As Integer,
                           ByVal paymentProviderType As String,
                           ByVal productCode As String,
                           ByVal RegisterCountry As String, Optional ByVal _currency As String = "EUR")

        If (String.IsNullOrEmpty(productCode)) Then
            productCode = "dd" & quantity & type
        End If

        If (Not String.IsNullOrEmpty(paymentProviderType)) Then paymentProviderType = paymentProviderType.ToUpper()
        Dim useCase1 As Boolean = ((HttpContext.Current.Session("GEO_COUNTRY_CODE") = "GR" OrElse HttpContext.Current.Session("GEO_COUNTRY_CODE") = "AL") AndAlso paymentProviderType = "CC")
        Dim useCase_TR As Boolean = (paymentMethod.ToUpper() = "PW" OrElse paymentMethod.ToUpper() = "PAYMENTWALL")
        Dim useCase_PWO As Boolean = (paymentMethod.ToUpper() = "PWO")
        If (Not String.IsNullOrEmpty(productCode)) Then
            If (Not productCode.EndsWith("TRY")) Then useCase_TR = False
        End If


        If (useCase_TR) Then

            clsCustomer.PaymentWall_TR_Payment(amount,
                            type,
                            quantity,
                            Email,
                            CustomerID,
                            CustomReferrer,
                            oURL,
                            paymentMethod,
                            promoCode,
                            SalesSiteID,
                            paymentProviderType,
                            productCode, If(CustomerID = 269 OrElse CustomerID = 165078, True, False))
        ElseIf (useCase_PWO) Then

            clsCustomer.PaymentWall_PWO_Payment(amount,
                            type,
                            quantity,
                            Email,
                            CustomerID,
                            CustomReferrer,
                            oURL,
                            paymentMethod,
                            promoCode,
                            SalesSiteID,
                            paymentProviderType,
                            productCode, If(CustomerID = 269 OrElse CustomerID = 165078, True, False))
        ElseIf (paymentMethod = "PW") Then
            clsCustomer.PaymentWall_Payment(amount,
                       type,
                       quantity,
                       Email,
                       CustomerID,
                       CustomReferrer,
                       oURL,
                       paymentMethod,
                       promoCode,
                       SalesSiteID,
                       paymentProviderType,
                       productCode, If(CustomerID = 269 OrElse CustomerID = 165078, True, False))
        ElseIf (paymentMethod = "epoch" OrElse paymentMethod.ToLower = "paypal" OrElse paymentMethod.ToLower = "paysafecard" OrElse paymentMethod.ToLower = "payweb") Then

            clsCustomer.Epoch_Payment(amount,
                           type,
                           quantity,
                           Email,
                           CustomerID,
                           CustomReferrer,
                           oURL,
                           paymentMethod,
                           promoCode,
                           SalesSiteID,
                           paymentProviderType,
                           productCode)
        ElseIf (paymentMethod = "bitpay") Then

            clsCustomer.BitPay_Payment(amount,
                           type,
                           quantity,
                           Email,
                           CustomerID,
                           CustomReferrer,
                           oURL,
                           paymentMethod,
                           promoCode,
                           SalesSiteID,
                           paymentProviderType,
                           productCode, _currency)
     
        Else
            '   Dim qsInfo As String = ""
            Dim prof As EUS_Profile = Nothing
            Try
                prof = DataHelpers.GetEUS_ProfileByProfileID(CustomerID)
                '    qsInfo = clsCustomer.GetProfileInfoParameters(prof)
            Catch
            End Try

            Try
                Dim oRemotePost As New RemotePost()
                Dim postURL As String = ""
                Dim flag As Boolean = True

                If (useCase1 AndAlso False) Then

                    postURL = "https://www.putdrive.com/pay/loader.aspx"
                    paymentMethod = "adyen"

                Else


                    If paymentMethod = "AlertPay" Or paymentMethod = "paysafecard" Or paymentMethod = "epoch" Then
                        flag = False
                    End If
                    If flag Then
                        postURL = "http://www.paymix.net/pay/loader.aspx"
                    End If

                    If postURL = "" Then postURL = "http://www.soundsoft.com/pay/initPayment.ashx"
                    If productCode.ToLower = "zfreemoney" Then
                        '  postURL = "http://www.digi-store.net/pay/initPayment.ashx"
                    End If
                    If paymentMethod.ToLower = "paypal" Then
                        postURL = "http://www.digi-store.net/pay/initPayment.ashx"
                    End If
                    If paymentMethod.ToLower = "bitpay" Then
                        postURL = "http://www.goomena.com/IPN/initPayment.ashx"
                    End If

                End If
                oRemotePost.Url = postURL
                'oRemotePost.Url = "http://localhost:15837/initPayment.ashx"

                oRemotePost.Add("PaymentMethod", paymentMethod)
                If (paymentProviderType Is Nothing) Then
                    If (paymentMethod.ToUpper() = "PAYMENTWALL") Then
                        oRemotePost.Add("Widget", "p4_1")
                    End If
                End If
                oRemotePost.Add("ProductCode", productCode)
                oRemotePost.Add("CustomerID", CustomerID)
                oRemotePost.Add("LoginName", Email)
                oRemotePost.Add("promoCode", promoCode)
                oRemotePost.Add("CustomReferrer", CustomReferrer)
                oRemotePost.Add("downloadURL", oURL)
                oRemotePost.Add("Amount", amount)
                oRemotePost.Add("type", paymentProviderType)
                oRemotePost.Add("SalesSiteID", SalesSiteID)
                If (prof IsNot Nothing) Then
                    Try
                        oRemotePost.Add("fn", prof.FirstName & " " & prof.LastName)
                        oRemotePost.Add("zip", prof.Zip)
                        oRemotePost.Add("cit", prof.City)
                        oRemotePost.Add("reg", prof.Region)
                        oRemotePost.Add("cntr", prof.Country)
                        oRemotePost.Add("eml", prof.eMail)
                        oRemotePost.Add("mob", prof.Cellular)
                        oRemotePost.Add("rDate", prof.DateTimeToRegister)
                    Catch ex As Exception

                    End Try
                End If
                oRemotePost.Post()
            Catch ex As Exception
                If (useCase1 AndAlso False) Then

                    Dim postURL As String = "https://www.putdrive.com/pay/loader.aspx"
                    postURL = postURL & _
                        "?PaymentMethod=" & paymentMethod & _
                        "&ProductCode=" & productCode & _
                        "&CustomerID=" & CustomerID & _
                        "&LoginName=" & Email & _
                        "&promoCode=" & promoCode & _
                        "&CustomReferrer=" & CustomReferrer & _
                        "&downloadURL=" & oURL & _
                        "&amount=" & amount & _
                        "&SalesSiteID=" & SalesSiteID & _
                        "&type=" & paymentProviderType

                    HttpContext.Current.Response.Redirect(postURL)

                Else

                    Dim flag As Boolean = True
                    If paymentMethod.ToLower() = "alertpay" Or paymentMethod.ToLower() = "paysafecard" Or paymentMethod.ToLower() = "epoch" Then
                        flag = False
                    End If
                    Dim Widget As String = ""
                    If (paymentProviderType Is Nothing) Then
                        If (paymentMethod.ToUpper() = "PAYMENTWALL") Then
                            Widget = "&Widget=p4_1"
                        End If
                    End If

                    If productCode.ToLower = "zfreemoney" Then
                        '  HttpContext.Current.Response.Redirect("http://www.digi-store.net/pay/initPayment.ashx?PaymentMethod=" & paymentMethod & "&ProductCode=" & productCode & "&CustomerID=" & CustomerID & "&LoginName=" & loginName & "&promoCode=" & promoCode & "&CustomReferrer=" & CustomReferrer & "&downloadURL=" & oURL & "&amount=" & amount & "&SalesSiteID=" & SalesSiteID & "&type=" & paymentProviderType)

                    End If
                    If paymentMethod.ToLower = "paypal" Then
                        Dim url As String = "http://www.digi-store.net/pay/initPayment.ashx?PaymentMethod=" & paymentMethod & "&ProductCode=" & productCode & "&CustomerID=" & CustomerID & "&LoginName=" & Email & "&promoCode=" & promoCode & "&CustomReferrer=" & CustomReferrer & "&downloadURL=" & oURL & "&amount=" & amount & "&SalesSiteID=" & SalesSiteID & "&type=" & paymentProviderType
                        HttpContext.Current.Response.Redirect(url)

                    End If
                    If paymentMethod.ToLower = "bitpay" Then
                        Dim url As String = "http://www.goomena.com/IPN/initPayment.ashx?PaymentMethod=" & paymentMethod & "&ProductCode=" & productCode & "&CustomerID=" & CustomerID & "&LoginName=" & Email & "&promoCode=" & promoCode & "&CustomReferrer=" & CustomReferrer & "&downloadURL=" & oURL & "&amount=" & amount & "&SalesSiteID=" & SalesSiteID & "&type=" & paymentProviderType & Widget
                        HttpContext.Current.Response.Redirect(url)
                    End If
                    If flag Then
                        Dim url As String = "http://www.paymix.net/pay/loader.aspx?PaymentMethod=" & paymentMethod & "&ProductCode=" & productCode & "&CustomerID=" & CustomerID & "&LoginName=" & Email & "&promoCode=" & promoCode & "&CustomReferrer=" & CustomReferrer & "&downloadURL=" & oURL & "&amount=" & amount & "&SalesSiteID=" & SalesSiteID & "&type=" & paymentProviderType & Widget
                        HttpContext.Current.Response.Redirect(url)

                    End If

                    'If paymentMethod.ToLower = "epoch" Then
                    '    Dim postURL As String = "https://www.goomena.com/pay/initPayment.ashx"

                    '    ' randomly send to dating-deals
                    '    Dim rnd As New Random(Date.UtcNow.Millisecond)
                    '    Dim rndVal = rnd.Next(100)
                    '    If (rndVal > 80) Then
                    '        postURL = "http://www.dating-deals.com/pay/initPayment.ashx"
                    '    End If

                    '    Dim url2 As String = postURL & "?PaymentMethod=" & paymentMethod & "&ProductCode=" & productCode & "&CustomerID=" & CustomerID & "&LoginName=" & loginName & "&promoCode=" & promoCode & "&CustomReferrer=" & CustomReferrer & "&downloadURL=" & oURL & "&amount=" & amount & "&SalesSiteID=" & SalesSiteID & "&type=" & paymentProviderType & "" & Widget & qsInfo
                    '    HttpContext.Current.Response.Redirect(url2)
                    'End If
                    Dim url1 As String = "http://www.soundsoft.com/pay/initPayment.ashx?PaymentMethod=" & paymentMethod & "&ProductCode=" & productCode & "&CustomerID=" & CustomerID & "&LoginName=" & Email & "&promoCode=" & promoCode & "&CustomReferrer=" & CustomReferrer & "&downloadURL=" & oURL & "&amount=" & amount & "&SalesSiteID=" & SalesSiteID & "" & Widget
                    HttpContext.Current.Response.Redirect(url1)
                End If

                'HttpContext.Current.Response.Redirect("http://localhost:15837/initPayment.ashx?PaymentMethod=" & paymentMethod & "&ProductCode=" & productCode & "&CustomerID=" & CustomerID & "&LoginName=" & loginName & "&promoCode=" & promoCode & "&CustomReferrer=" & CustomReferrer & "&downloadURL=" & oURL & "&amount=" & amount & "&SalesSiteID=" & SalesSiteID & "")
            End Try


        End If

    End Sub

    Public Shared Sub Epoch_Payment(ByVal amount As String,
                           ByVal type As String,
                           ByVal quantity As String,
                           ByVal loginName As String,
                           ByVal CustomerID As Integer,
                           ByVal CustomReferrer As String,
                           ByVal oURL As String,
                           ByVal paymentMethod As String,
                           ByVal promoCode As String,
                           ByVal SalesSiteID As Integer,
                           ByVal paymentProviderType As String,
                           ByVal productCode As String)

        Dim camcharge As String = ""
        'If (paymentProviderType = "CC" AndAlso
        '    clsConfigValues.Get__autorebill_for_customers().Contains(CustomerID)) Then
        '''''''''''''''''''''''
        '' autorebill functionality
        ''  295=savvas, 269=savva82, 32128=dooma, 
        ''  4534=jim2000, 31492=dimkaravel, 23322=eyelove, 28974=john1976
        '' {295, 269, 32128, 4534, 31492, 23322, 28974}
        '''''''''''''''''''''''

        If paymentProviderType = "CC" Then
            Dim meter As Integer = clsConfigValues.Get_PaymentProviderMeter
            If meter > 0 Then
                Dim rnd As Integer = 0
                Try
                    Dim r As New Random(System.DateTime.Now.Millisecond)
                    rnd = r.Next(1, 100)

                Catch ex As Exception
                    rnd = 0
                End Try
                If rnd <= meter Then
                    paymentMethod = "mes"
                End If
            End If

        End If


        'If (CustomerID = 269 AndAlso paymentProviderType = "CC") Then
        '    paymentMethod = "mes"
        'End If
        If (paymentProviderType = "CC") Then
            Using ctx As New CMSDBDataContext(DataHelpers.ConnectionString)


                Dim tran As EUS_CustomerTransaction = Nothing
                Try

                    tran = (From itm In ctx.EUS_CustomerTransactions
                            Where itm.CustomerID = CustomerID AndAlso itm.PaymentMethods IsNot Nothing AndAlso itm.PaymentMethods.Length > 0 AndAlso itm.PaymentMethods <> "None"
                            Order By itm.Date_Time Descending
                            Select itm).FirstOrDefault()


                    If (tran IsNot Nothing) Then
                        If (tran.PaymentMethods = "CreditCard") Then
                            paymentProviderType = tran.TransactionInfo
                            If (Not String.IsNullOrEmpty(tran.MemberID)) Then paymentProviderType = tran.MemberID
                            camcharge = "&camcharge=1"
                        End If
                    End If

                Catch ex As Exception
                Finally
                End Try
            End Using
        End If

        Dim qsInfo As String = ""
        Dim prof As EUS_Profile = Nothing
        Try
            prof = DataHelpers.GetEUS_ProfileByProfileID(CustomerID)
            qsInfo = clsCustomer.GetProfileInfoParameters(prof)
        Catch
        End Try


        Dim count As Integer = 0
        Try
            count = clsCreditsHelper.EUS_CustomerTransaction_CountTransactions(CustomerID)
        Catch
        End Try
        Dim isRebill As Boolean = (count > 1)


        Dim postURL As String = "https://www.goomena.com/pay/initPayment.ashx"
        If (Not String.IsNullOrEmpty(ConfigurationManager.AppSettings("TEST_initPayment"))) Then
            postURL = ConfigurationManager.AppSettings("TEST_initPayment")
        End If

        ' if it's euro then allow it to be sent from dating-deals
        If (Not productCode.EndsWith("TRY")) Then

            '' randomly send to dating-deals
            'Dim rnd As New Random(Date.UtcNow.Millisecond)
            'Dim rndVal = rnd.Next(100)
            'If (rndVal > 80) Then
            '    postURL = "http://www.dating-deals.com/pay/initPayment.ashx"
            '    paymentMethod = "epochzt"
            '    productCode = productCode & "zt"
            'End If

        End If


        Try
            Dim oRemotePost As New RemotePost()
            oRemotePost.Url = postURL
            oRemotePost.Add("PaymentMethod", paymentMethod)
            oRemotePost.Add("ProductCode", productCode)
            oRemotePost.Add("CustomerID", CustomerID)
            oRemotePost.Add("LoginName", loginName)
            oRemotePost.Add("promoCode", promoCode)
            oRemotePost.Add("CustomReferrer", CustomReferrer)
            oRemotePost.Add("downloadURL", oURL)
            oRemotePost.Add("Amount", amount)
            oRemotePost.Add("type", paymentProviderType)
            oRemotePost.Add("SalesSiteID", SalesSiteID)
            oRemotePost.Add("isRebill1", If(isRebill, 1, 0))
            oRemotePost.Add("currency", "EUR")
            If (prof IsNot Nothing) Then
                Try
                    oRemotePost.Add("fn", prof.FirstName & " " & prof.LastName)
                    oRemotePost.Add("zip", prof.Zip)
                    oRemotePost.Add("cit", prof.City)
                    oRemotePost.Add("reg", prof.Region)
                    oRemotePost.Add("cntr", prof.Country)
                    oRemotePost.Add("eml", prof.eMail)
                    oRemotePost.Add("mob", prof.Cellular)
                Catch
                End Try
            End If
            oRemotePost.Post()
        Catch ex As Exception

            Dim Widget As String = ""

            Dim url2 As String = postURL & _
                "?PaymentMethod=" & paymentMethod & _
                "&ProductCode=" & productCode & _
                "&CustomerID=" & CustomerID & _
                "&LoginName=" & loginName & _
                "&promoCode=" & promoCode & _
                "&CustomReferrer=" & CustomReferrer & _
                "&downloadURL=" & oURL & _
                "&amount=" & amount & _
                "&SalesSiteID=" & SalesSiteID & _
                "&isRebill1=" & If(isRebill, 1, 0) & _
                "&type=" & paymentProviderType & _
                 "" & Widget & _
                qsInfo & _
                camcharge
            '"&currency=EUR" & _
            HttpContext.Current.Response.Redirect(url2)

        End Try


    End Sub

    Public Shared Sub Viva_Payment(ByVal amount As String,
                          ByVal type As String,
                          ByVal quantity As String,
                          ByVal loginName As String,
                          ByVal CustomerID As Integer,
                          ByVal CustomReferrer As String,
                          ByVal oURL As String,
                          ByVal paymentMethod As String,
                          ByVal promoCode As String,
                          ByVal SalesSiteID As Integer,
                          ByVal paymentProviderType As String,
                          ByVal productCode As String)

        Dim camcharge As String = ""
        Dim qsInfo As String = ""
        Dim prof As EUS_Profile = Nothing
        Try
            prof = DataHelpers.GetEUS_ProfileByProfileID(CustomerID)
            qsInfo = clsCustomer.GetProfileInfoParameters(prof)
        Catch
        End Try


        Dim count As Integer = 0
        Try
            count = clsCreditsHelper.EUS_CustomerTransaction_CountTransactions(CustomerID)
        Catch
        End Try
        Dim isRebill As Boolean = (count > 1)


        Dim postURL As String = "https://www.goodeals.com/pay/initPayment.ashx"
        If (Not String.IsNullOrEmpty(ConfigurationManager.AppSettings("TEST_initPayment"))) Then
            postURL = ConfigurationManager.AppSettings("TEST_initPayment")
        End If

        Try
            Dim oRemotePost As New RemotePost()
            oRemotePost.Url = postURL
            oRemotePost.Add("PaymentMethod", paymentMethod)
            oRemotePost.Add("ProductCode", productCode)
            oRemotePost.Add("CustomerID", CustomerID)
            oRemotePost.Add("LoginName", loginName)
            oRemotePost.Add("promoCode", promoCode)
            oRemotePost.Add("CustomReferrer", CustomReferrer)
            oRemotePost.Add("downloadURL", oURL)
            oRemotePost.Add("Amount", amount)
            oRemotePost.Add("type", paymentProviderType)
            oRemotePost.Add("SalesSiteID", SalesSiteID)
            oRemotePost.Add("isRebill1", If(isRebill, 1, 0))
            oRemotePost.Add("currency", "EUR")
            If (prof IsNot Nothing) Then
                Try
                    oRemotePost.Add("fn", prof.FirstName & " " & prof.LastName)
                    oRemotePost.Add("zip", prof.Zip)
                    oRemotePost.Add("cit", prof.City)
                    oRemotePost.Add("reg", prof.Region)
                    oRemotePost.Add("cntr", prof.Country)
                    oRemotePost.Add("eml", prof.eMail)
                    oRemotePost.Add("mob", prof.Cellular)
                Catch
                End Try
            End If
            oRemotePost.Post()
        Catch ex As Exception

            Dim Widget As String = ""

            Dim url2 As String = postURL & _
                "?PaymentMethod=" & paymentMethod & _
                "&ProductCode=" & productCode & _
                "&CustomerID=" & CustomerID & _
                "&LoginName=" & loginName & _
                "&promoCode=" & promoCode & _
                "&CustomReferrer=" & CustomReferrer & _
                "&downloadURL=" & oURL & _
                "&amount=" & amount & _
                "&SalesSiteID=" & SalesSiteID & _
                "&isRebill1=" & If(isRebill, 1, 0) & _
                "&type=" & paymentProviderType & _
                 "" & Widget & _
                qsInfo & _
                camcharge
            '"&currency=EUR" & _
            HttpContext.Current.Response.Redirect(url2)

        End Try


    End Sub
    Public Shared Sub BitPay_Payment(ByVal amount As String,
                           ByVal type As String,
                           ByVal quantity As String,
                           ByVal loginName As String,
                           ByVal CustomerID As Integer,
                           ByVal CustomReferrer As String,
                           ByVal oURL As String,
                           ByVal paymentMethod As String,
                           ByVal promoCode As String,
                           ByVal SalesSiteID As Integer,
                           ByVal paymentProviderType As String,
                           ByVal productCode As String, ByVal Currency As String)

        '   Dim camcharge As String = ""
        'If (paymentProviderType = "CC" AndAlso
        '    clsConfigValues.Get__autorebill_for_customers().Contains(CustomerID)) Then
        '''''''''''''''''''''''
        '' autorebill functionality
        ''  295=savvas, 269=savva82, 32128=dooma, 
        ''  4534=jim2000, 31492=dimkaravel, 23322=eyelove, 28974=john1976
        '' {295, 269, 32128, 4534, 31492, 23322, 28974}
        '''''''''''''''''''''''
        'If (paymentProviderType = "bitpay") Then
        '    Using ctx As New CMSDBDataContext(DataHelpers.ConnectionString)


        '        Dim tran As EUS_CustomerTransaction = Nothing
        '        Try

        '            tran = (From itm In ctx.EUS_CustomerTransactions
        '                    Where itm.CustomerID = CustomerID AndAlso itm.PaymentMethods IsNot Nothing AndAlso itm.PaymentMethods.Length > 0 AndAlso itm.PaymentMethods <> "None"
        '                    Order By itm.Date_Time Descending
        '                    Select itm).FirstOrDefault()


        '            If (tran IsNot Nothing) Then
        '                '   If (tran.PaymentMethods = "CreditCard") Then
        '                camcharge = "&camcharge=1"
        '                'End If
        '            End If

        '        Catch ex As Exception
        'Finally
        'End Try
        '    End Using
        'End If

        Dim qsInfo As String = ""
        Dim prof As EUS_Profile = Nothing
        Try
            prof = DataHelpers.GetEUS_ProfileByProfileID(CustomerID)
            qsInfo = clsCustomer.GetProfileInfoParameters(prof)
        Catch
        End Try


        Dim count As Integer = 0
        Try
            count = clsCreditsHelper.EUS_CustomerTransaction_CountTransactions(CustomerID)
        Catch
        End Try
        Dim isRebill As Boolean = (count > 1)


        Dim postURL As String = "https://www.goomena.com/IPN/initPayment.ashx"
        'If (Not String.IsNullOrEmpty(ConfigurationManager.AppSettings("TEST_initPayment"))) Then
        '    postURL = ConfigurationManager.AppSettings("TEST_initPayment")
        'End If

        ' if it's euro then allow it to be sent from dating-deals
        'If (Not productCode.EndsWith("TRY")) Then

        '    '' randomly send to dating-deals
        '    'Dim rnd As New Random(Date.UtcNow.Millisecond)
        '    'Dim rndVal = rnd.Next(100)
        '    'If (rndVal > 80) Then
        '    '    postURL = "http://www.dating-deals.com/pay/initPayment.ashx"
        '    '    paymentMethod = "epochzt"
        '    '    productCode = productCode & "zt"
        '    'End If

        'End If


        Try
            Dim oRemotePost As New RemotePost()
            oRemotePost.Url = postURL
            oRemotePost.Add("PaymentMethod", paymentMethod)
            oRemotePost.Add("ProductCode", productCode)
            oRemotePost.Add("CustomerID", CustomerID)
            oRemotePost.Add("LoginName", loginName)
            oRemotePost.Add("promoCode", promoCode)
            oRemotePost.Add("CustomReferrer", CustomReferrer)
            oRemotePost.Add("downloadURL", oURL)
            oRemotePost.Add("Amount", amount)
            oRemotePost.Add("SalesSiteID", SalesSiteID)
            oRemotePost.Add("isRebill1", If(isRebill, 1, 0)) 'currency=USD
            oRemotePost.Add("currency", Currency)
            oRemotePost.Add("type", "cc")
            If (prof IsNot Nothing) Then
                Try
                    oRemotePost.Add("fn", prof.FirstName & " " & prof.LastName)
                    oRemotePost.Add("zip", prof.Zip)
                    oRemotePost.Add("cit", prof.City)
                    oRemotePost.Add("reg", prof.Region)
                    oRemotePost.Add("cntr", prof.Country)
                    oRemotePost.Add("eml", prof.eMail)
                    oRemotePost.Add("mob", prof.Cellular)
                Catch
                End Try
            End If
            oRemotePost.Post()
        Catch ex As Exception

            Dim Widget As String = ""

            Dim url2 As String = postURL & _
                "?PaymentMethod=" & paymentMethod & _
                "&ProductCode=" & productCode & _
                "&CustomerID=" & CustomerID & _
                "&LoginName=" & loginName & _
                "&promoCode=" & promoCode & _
                "&CustomReferrer=" & CustomReferrer & _
                "&downloadURL=" & oURL & _
                "&amount=" & amount & _
                "&SalesSiteID=" & SalesSiteID & _
                "&isRebill1=" & If(isRebill, 1, 0) & _
                        "&currency=" & Currency & _
                "&type=CC" & _
                 qsInfo
            '"&currency=EUR" & _
            HttpContext.Current.Response.Redirect(url2)

        End Try


    End Sub

    Public Shared Sub RegNowPayment(productID As String, amount As String, payTransID As String)

        Dim pgUrl As String = "https://www.regnow.com/checkout/cart/new/42221-"

        'Dim productID As String = Request("itemName")
        'Dim amount As Decimal = Request("amount")
        'Dim payTransID As String = Request("TransID")

        Dim oRemotePost As New RemotePost()
        oRemotePost.Url = pgUrl & productID

        oRemotePost.Add("linkid", payTransID)
        oRemotePost.Add("currency", "EUR")
        oRemotePost.Post()

    End Sub


    Public Shared Sub PaymentWall_TR_Payment(ByVal amount As String,
                           ByVal type As String,
                           ByVal quantity As String,
                           ByVal loginName As String,
                           ByVal CustomerID As Integer,
                           ByVal CustomReferrer As String,
                           ByVal oURL As String,
                           ByVal paymentMethod As String,
                           ByVal promoCode As String,
                           ByVal SalesSiteID As Integer,
                           ByVal paymentProviderType As String,
                           ByVal productCode As String,
                                Optional ByVal IsTEst As Boolean = False)

        If (String.IsNullOrEmpty(productCode)) Then productCode = "dd" & quantity & type
        If (Not String.IsNullOrEmpty(paymentProviderType)) Then paymentProviderType = paymentProviderType.ToUpper()

        Dim qsInfo As String = ""


        Dim prof As EUS_Profile = Nothing
        Try
            prof = DataHelpers.GetEUS_ProfileByProfileID(CustomerID)
            qsInfo = clsCustomer.GetProfileInfoParameters(prof)
        Catch
        End Try

        Try
            Dim oRemotePost As New RemotePost()
            Dim postURL As String = ""
            '     Dim flag As Boolean = True

#If DEBUG Then
            postURL = "http://localhost:19306/pweb/initPayment.ashx"    'http://localhost:19306/pweb
#Else

            postURL = "https://www.goomena.com/pay/initPayment.ashx"
#End If


            paymentMethod = "pw"
            If IsTEst = True Then oRemotePost.Add("test_mode", 1)
            oRemotePost.Url = postURL
            oRemotePost.Add("PaymentMethod", paymentMethod)
            If (paymentProviderType Is Nothing) Then
                If (paymentMethod.ToUpper() = "PAYMENTWALL") Then
                    oRemotePost.Add("Widget", "p4_1")
                End If
            End If
            oRemotePost.Add("ProductCode", productCode)
            oRemotePost.Add("CustomerID", CustomerID)
            oRemotePost.Add("LoginName", loginName)
            oRemotePost.Add("promoCode", promoCode)
            oRemotePost.Add("CustomReferrer", CustomReferrer)
            oRemotePost.Add("downloadURL", oURL)
            oRemotePost.Add("Amount", amount)
            oRemotePost.Add("type", paymentProviderType)
            oRemotePost.Add("SalesSiteID", SalesSiteID)
            If (prof IsNot Nothing) Then
                Try
                    oRemotePost.Add("fn", prof.FirstName)
                    oRemotePost.Add("ln", prof.LastName)
                    oRemotePost.Add("rDate", prof.DateTimeToRegister)
                    oRemotePost.Add("zip", prof.Zip)
                    oRemotePost.Add("cit", prof.City)
                    oRemotePost.Add("reg", prof.Region)
                    oRemotePost.Add("cntr", prof.Country)
                    oRemotePost.Add("eml", prof.eMail)
                    oRemotePost.Add("mob", prof.Cellular)
                Catch ex As Exception

                End Try
            End If
            oRemotePost.Post()
        Catch ex1 As Threading.ThreadAbortException
        Catch ex As Exception
            Dim Widget As String = ""
            If (paymentProviderType Is Nothing) Then
                If (paymentMethod.ToUpper() = "PAYMENTWALL" OrElse paymentMethod.ToUpper() = "PW") Then
                    Widget = "&Widget=p4_1"
                End If
            End If



            Dim postURL As String = "http://www.dating-deals.com/pay/initPayment.ashx"
            postURL = postURL & _
                "?PaymentMethod=" & HttpUtility.UrlEncode(paymentMethod) & _
                "&ProductCode=" & HttpUtility.UrlEncode(productCode) & _
                "&CustomerID=" & CustomerID & _
                "&LoginName=" & HttpUtility.UrlEncode(loginName) & _
                "&promoCode=" & HttpUtility.UrlEncode(promoCode) & _
                "&CustomReferrer=" & HttpUtility.UrlEncode(CustomReferrer) & _
                "&downloadURL=" & oURL & _
                "&amount=" & amount & _
                "&SalesSiteID=" & SalesSiteID & _
                "&type=" & paymentProviderType & _
                qsInfo & _
                Widget

            'Notify_SentToPaymentWall(postURL)
            HttpContext.Current.Response.Redirect(postURL)
        End Try
    End Sub

    Public Shared Sub PaymentWall_PWO_Payment(ByVal amount As String,
                           ByVal type As String,
                           ByVal quantity As String,
                           ByVal loginName As String,
                           ByVal CustomerID As Integer,
                           ByVal CustomReferrer As String,
                           ByVal oURL As String,
                           ByVal paymentMethod As String,
                           ByVal promoCode As String,
                           ByVal SalesSiteID As Integer,
                           ByVal paymentProviderType As String,
                           ByVal productCode As String,
                                Optional ByVal IsTEst As Boolean = False)

        If (String.IsNullOrEmpty(productCode)) Then productCode = "dd" & quantity & type
        If (Not String.IsNullOrEmpty(paymentProviderType)) Then paymentProviderType = paymentProviderType.ToUpper()

        Dim qsInfo As String = ""


        Dim prof As EUS_Profile = Nothing
        Try
            prof = DataHelpers.GetEUS_ProfileByProfileID(CustomerID)
            qsInfo = clsCustomer.GetProfileInfoParameters(prof)
        Catch
        End Try

        Try
            Dim oRemotePost As New RemotePost()
            Dim postURL As String = ""
            '     Dim flag As Boolean = True   '\dating-deals.com\newpay

#If DEBUG Then
            postURL = "http://localhost:19306/pweb/initPayment.ashx"    'http://localhost:19306/pweb

#Else

            postURL = "http://dating-deals.com/newpay/initPayment.ashx"
#End If


            paymentMethod = "pwo"
            If IsTEst = True Then oRemotePost.Add("test_mode", 1)
            oRemotePost.Url = postURL
            oRemotePost.Add("PaymentMethod", paymentMethod)
            If (paymentProviderType Is Nothing) Then
                If (paymentMethod.ToUpper() = "PAYMENTWALL") Then
                    oRemotePost.Add("Widget", "p4_1")
                End If
            End If
            oRemotePost.Add("ProductCode", productCode)
            oRemotePost.Add("CustomerID", CustomerID)
            oRemotePost.Add("LoginName", loginName)
            oRemotePost.Add("promoCode", promoCode)
            oRemotePost.Add("CustomReferrer", CustomReferrer)
            oRemotePost.Add("downloadURL", oURL)
            oRemotePost.Add("Amount", amount)
            oRemotePost.Add("type", paymentProviderType)
            oRemotePost.Add("SalesSiteID", SalesSiteID)
            If (prof IsNot Nothing) Then
                Try
                    oRemotePost.Add("fn", prof.FirstName)
                    oRemotePost.Add("ln", prof.LastName)
                    oRemotePost.Add("rDate", prof.DateTimeToRegister)
                    oRemotePost.Add("zip", prof.Zip)
                    oRemotePost.Add("cit", prof.City)
                    oRemotePost.Add("reg", prof.Region)
                    oRemotePost.Add("cntr", prof.Country)
                    oRemotePost.Add("eml", prof.eMail)
                    oRemotePost.Add("mob", prof.Cellular)
                Catch ex As Exception
                End Try
            End If
            oRemotePost.Post()
        Catch ex As Exception
        End Try
    End Sub

    Public Shared Sub PaymentWall_Payment(ByVal amount As String,
                           ByVal type As String,
                           ByVal quantity As String,
                           ByVal loginName As String,
                           ByVal CustomerID As Integer,
                           ByVal CustomReferrer As String,
                           ByVal oURL As String,
                           ByVal paymentMethod As String,
                           ByVal promoCode As String,
                           ByVal SalesSiteID As Integer,
                           ByVal paymentProviderType As String,
                           ByVal productCode As String,
                                Optional ByVal IsTEst As Boolean = False)

        If (String.IsNullOrEmpty(productCode)) Then productCode = "dd" & quantity & type
        If (Not String.IsNullOrEmpty(paymentProviderType)) Then paymentProviderType = paymentProviderType.ToUpper()

        Dim qsInfo As String = ""


        Dim prof As EUS_Profile = Nothing
        Try
            prof = DataHelpers.GetEUS_ProfileByProfileID(CustomerID)
            qsInfo = clsCustomer.GetProfileInfoParameters(prof)
        Catch
        End Try

        Try
            Dim oRemotePost As New RemotePost()
            Dim postURL As String = ""
            '     Dim flag As Boolean = True



#If DEBUG Then
            postURL = "http://localhost:19306/pweb/initPayment.ashx"    'http://localhost:19306/pweb
#Else

            postURL = "https://www.goomena.com/ipn/initPayment.ashx"
#End If

            paymentMethod = "pw"
            If IsTEst = True Then oRemotePost.Add("test_mode", 1)
            oRemotePost.Url = postURL
            oRemotePost.Add("PaymentMethod", paymentMethod)
            If (paymentProviderType Is Nothing) Then
                If (paymentMethod.ToUpper() = "PAYMENTWALL") Then
                    oRemotePost.Add("Widget", "p4_1")
                End If
            End If
            oRemotePost.Add("ProductCode", productCode)
            oRemotePost.Add("CustomerID", CustomerID)
            oRemotePost.Add("LoginName", loginName)
            oRemotePost.Add("promoCode", promoCode)
            oRemotePost.Add("CustomReferrer", CustomReferrer)
            oRemotePost.Add("downloadURL", oURL)
            oRemotePost.Add("Amount", amount)
            oRemotePost.Add("type", paymentProviderType)
            oRemotePost.Add("SalesSiteID", SalesSiteID)
            If (prof IsNot Nothing) Then
                Try
                    oRemotePost.Add("fn", prof.FirstName)
                    oRemotePost.Add("ln", prof.LastName)
                    oRemotePost.Add("rDate", prof.DateTimeToRegister)
                    oRemotePost.Add("zip", prof.Zip)
                    oRemotePost.Add("cit", prof.City)
                    oRemotePost.Add("reg", prof.Region)
                    oRemotePost.Add("cntr", prof.Country)
                    oRemotePost.Add("eml", prof.eMail)
                    oRemotePost.Add("mob", prof.Cellular)
                    oRemotePost.Add("rDate", prof.DateTimeToRegister)
                Catch ex As Exception
                End Try
            End If
            oRemotePost.Post()
        Catch ex As Exception
            'Dim Widget As String = ""
            'If (paymentProviderType Is Nothing) Then
            '    If (paymentMethod.ToUpper() = "PAYMENTWALL" OrElse paymentMethod.ToUpper() = "PW") Then
            '        Widget = "&Widget=p4_1"
            '    End If
            'End If



            'Dim postURL As String = "http://www.dating-deals.com/pay/initPayment.ashx"
            'postURL = postURL & _
            '    "?PaymentMethod=" & HttpUtility.UrlEncode(paymentMethod) & _
            '    "&ProductCode=" & HttpUtility.UrlEncode(productCode) & _
            '    "&CustomerID=" & CustomerID & _
            '    "&LoginName=" & HttpUtility.UrlEncode(loginName) & _
            '    "&promoCode=" & HttpUtility.UrlEncode(promoCode) & _
            '    "&CustomReferrer=" & HttpUtility.UrlEncode(CustomReferrer) & _
            '    "&downloadURL=" & oURL & _
            '    "&amount=" & amount & _
            '    "&SalesSiteID=" & SalesSiteID & _
            '    "&type=" & paymentProviderType & _
            '    qsInfo & _
            '    Widget

            ''Notify_SentToPaymentWall(postURL)
            'HttpContext.Current.Response.Redirect(postURL)
        End Try
    End Sub

    Shared Function ResellerAddFunds(Money As Double, CustomerID As Integer) As Boolean
        Return True
    End Function


    Public Class AddTransactionParams
        Public Property Description As String
        Public Property Money As Double
        Public Property TransactionInfo As String
        Public Property p4 As String
        Public Property CreditsPurchased As Integer?
        Public Property REMOTE_ADDR As String
        Public Property HTTP_USER_AGENT As String
        Public Property HTTP_REFERER As String
        Public Property CustomerID As Integer
        Public Property CustomReferrer As String
        Public Property PayerEmail As String
        Public Property paymethod As PaymentMethods
        Public Property PromoCode As String
        Public Property TransactionTypeID As Integer
        Public Property productCode As String
        Public Property Currency As String
        Public Property cDataRecordIPN As clsDataRecordIPN
        Public Property freeUnlocks As Integer = 0
        Public Property useVoucherCredits As Boolean = False
        Public Property durationDays As Integer = 45
        Public Property voucherCredits As Integer = 0
        Public Property MemberID As String
        Public Property ReceiptID As String = ""
    End Class



    Shared Function AddTransaction(Description As String,
                                   Money As Double,
                                   TransactionInfo As String,
                                   p4 As String,
                                   CreditsPurchased As Integer?,
                                   REMOTE_ADDR As String,
                                   HTTP_USER_AGENT As String,
                                   HTTP_REFERER As String,
                                   CustomerID As Integer,
                                   CustomReferrer As String,
                                   PayerEmail As String,
                                   paymethod As PaymentMethods,
                                   PromoCode As String,
                                   TransactionTypeID As Integer,
                                   productCode As String,
                                   Currency As String,
                                   cDataRecordIPN As clsDataRecordIPN,
                                   Optional freeUnlocks As Integer = 0,
                                   Optional useVoucherCredits As Boolean = False,
                                   Optional durationDays As Integer = 45,
                                   Optional voucherCredits As Integer = 0) As Integer



        Dim prms As New clsCustomer.AddTransactionParams()
        prms.Description = Description
        prms.Money = Money
        prms.TransactionInfo = TransactionInfo
        prms.p4 = p4
        prms.CreditsPurchased = CreditsPurchased
        prms.REMOTE_ADDR = REMOTE_ADDR
        prms.HTTP_USER_AGENT = HTTP_USER_AGENT
        prms.HTTP_REFERER = HTTP_REFERER
        prms.CustomerID = CustomerID
        prms.CustomReferrer = CustomReferrer
        prms.PayerEmail = PayerEmail
        prms.paymethod = paymethod
        prms.PromoCode = PromoCode
        prms.TransactionTypeID = TransactionTypeID
        prms.productCode = productCode
        prms.Currency = Currency
        prms.cDataRecordIPN = cDataRecordIPN
        prms.freeUnlocks = freeUnlocks
        prms.useVoucherCredits = useVoucherCredits
        prms.durationDays = durationDays
        prms.voucherCredits = voucherCredits
        prms.ReceiptID = ""
        Return AddTransaction(prms)
    End Function

    Shared Function AddTransaction(prms As AddTransactionParams) As Integer

        Dim Description As String = prms.Description
        Dim Money As Double = prms.Money
        Dim TransactionInfo As String = prms.TransactionInfo
        Dim p4 As String = prms.p4
        Dim CreditsPurchased As Integer? = prms.CreditsPurchased
        Dim REMOTE_ADDR As String = prms.REMOTE_ADDR
        Dim HTTP_USER_AGENT As String = prms.HTTP_USER_AGENT
        Dim HTTP_REFERER As String = prms.HTTP_REFERER
        Dim CustomerID As Integer = prms.CustomerID
        Dim CustomReferrer As String = prms.CustomReferrer
        Dim PayerEmail As String = prms.PayerEmail
        Dim paymethod As PaymentMethods = prms.paymethod
        Dim PromoCode As String = prms.PromoCode
        Dim TransactionTypeID As Integer = prms.TransactionTypeID
        Dim productCode As String = prms.productCode
        Dim Currency As String = prms.Currency
        Dim cDataRecordIPN As clsDataRecordIPN = prms.cDataRecordIPN
        Dim freeUnlocks As Integer = 0 = prms.freeUnlocks
        Dim useVoucherCredits As Boolean = prms.useVoucherCredits
        Dim durationDays As Integer = prms.durationDays
        Dim voucherCredits As Integer = prms.voucherCredits
        Dim CustomerTransactionID As Integer = -1

        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)



            Dim credit As New EUS_CustomerCredit()
            Dim profile As DSMembers.EUS_ProfilesRow = Nothing
            Try
                ' the transction is from payment provider 
                'Not String.IsNullOrEmpty(TransactionInfo) AndAlso
                If (Not {"-1", "3333", "1111"}.Contains(TransactionInfo) AndAlso
                    PromoCode <> "1000_CREDITS_GIFT" AndAlso PromoCode <> "10000_CREDITS_GIFT" AndAlso PromoCode <> "5000_XMAS_2016_GIFT") Then
                    If prms.TransactionTypeID = -100 Then
                        CustomerTransactionID = clsCreditsHelper.EUS_CustomerTransaction_IsTransactionExistsPW(CustomerID, TransactionInfo, TransactionTypeID)
                    Else
                        CustomerTransactionID = clsCreditsHelper.EUS_CustomerTransaction_IsTransactionExists(CustomerID, TransactionInfo)
                    End If
                    If (CustomerTransactionID > -1) Then
                        clsMyMail.TrySendMailSupport(ConfigurationManager.AppSettings("PaymentLogsEmail"),
                                               "Payment service (Payment exists)",
                                               "Payment already done for specified customer," & vbCrLf & _
                                               " TransactionID: " & TransactionInfo & vbCrLf & _
                                               " PayerEmail: " & PayerEmail & vbCrLf & _
                                               " CustomerID: " & CustomerID & vbCrLf & _
                                               " EUS_CustomerTransaction.CustomerTransactionID: " & CustomerTransactionID & vbCrLf & vbCrLf & _
                                               "Exiting...",
                                               False)
                        Return CustomerTransactionID
                    Else
                        If prms.TransactionTypeID = -100 Then
                            CustomerTransactionID = clsCreditsHelper.EUS_CustomerTransaction_IsTransactionExists(CustomerID, TransactionInfo)
                        End If
                    End If
                End If


                If (Not String.IsNullOrEmpty(HTTP_REFERER)) Then
                    If (HTTP_REFERER.Length > 1020) Then
                        HTTP_REFERER = HTTP_REFERER.Remove(1020)
                    End If
                End If
                If (String.IsNullOrEmpty(Currency)) Then Currency = "EUR"

                profile = DataHelpers.GetEUS_Profiles_ByProfileIDRow(CustomerID)
                Try

                    If (productCode IsNot Nothing AndAlso productCode.ToUpper().EndsWith("LEK") AndAlso profile.Country = "TR") Then
                        productCode = productCode.Replace("LEK", "TRY")
                    End If
                Catch
                End Try

                Dim payment_provider_value As Double = clsPricing.GetRandomPaymentProviderPercent(profile.Country)
                Dim payment_provider_ratio As Double = payment_provider_value / 100


                Dim transaction As New EUS_CustomerTransaction()
                transaction.CreditAmount = CreditsPurchased
                transaction.CustomerID = CustomerID
                transaction.CustomReferrer = CustomReferrer
                transaction.Date_Time = DateTime.UtcNow
                transaction.DebitReducePercent = payment_provider_value
                transaction.Description = Description
                transaction.IP = REMOTE_ADDR
                transaction.Referrer = HTTP_REFERER
                transaction.TransactionInfo = TransactionInfo
                transaction.TransactionType = TransactionTypeID
                transaction.Notes = ""
                transaction.ReceiptID = prms.ReceiptID
                transaction.PaymentMethods = paymethod.ToString()
                transaction.MemberID = prms.MemberID


                Dim freeCredits As Integer
                If (freeUnlocks > 0) Then
                    Dim creditsRequired As EUS_CreditsType = clsUserDoes.GetRequiredCredits(UnlockType.UNLOCK_CONVERSATION)
                    freeCredits = freeUnlocks * creditsRequired.CreditsAmount
                End If


                Dim _Price As New EUS_Price()
                _Price.Currency = "EUR"

                If (Not String.IsNullOrEmpty(productCode)) Then
                    _Price = clsPricing.GetPriceForProductCode_CopyResult(productCode)
                    _Price.Credits = (_Price.Credits + freeCredits)

                ElseIf (Not String.IsNullOrEmpty(PromoCode)) Then

                    _Price.Credits = CreditsPurchased
                    _Price.duration = durationDays
                    _Price.Amount = Money

                ElseIf (useVoucherCredits) Then
                    _Price.Credits = voucherCredits + freeCredits
                    _Price.duration = durationDays
                    _Price.Amount = Money

                Else
                    _Price = clsPricing.GetPriceFromCredits(CreditsPurchased)
                    _Price.Credits = (CreditsPurchased + freeCredits)

                End If

                If TransactionTypeID = -100 And paymethod = PaymentMethods.PaymentWall Then
                    If CustomerTransactionID > -1 Then
                        transaction.DebitAmountOriginal = Money
                        transaction.DebitProductPrice = If(_Price.Amount < 0, _Price.Amount * -1, _Price.Amount)
                        transaction.DebitAmountOriginal = Money
                        transaction.DebitAmount = Money
                        transaction.DebitAmountReceived = Money
                        transaction.CreditAmount = 0
                        Dim itm As EUS_CustomerCredit = (From cr As EUS_CustomerCredit In _CMSDBDataContext.EUS_CustomerCredits
                                                          Where cr.CustomerTransactionID = CustomerTransactionID
                                                          Select cr).FirstOrDefault
                        If itm IsNot Nothing Then
                            Dim cr1 As New EUS_CustomerCredit()
                            cr1.Credits = itm.CreditstoBeExpired * -1
                            cr1.CreditstoBeExpired = Nothing
                            cr1.CreditsCalculated = 1
                            cr1.CustomerId = itm.CustomerId
                            cr1.DateTimeCreated = DateTime.UtcNow
                            cr1.DateTimeExpiration = Nothing
                            cr1.ValidForDays = Nothing
                            cr1.EuroAmount = Money
                            cr1.Currency = Currency
                            cr1.IsSubscription = 0
                            itm.CreditstoBeExpired = 0
                            transaction.EUS_CustomerCredits.Add(cr1)

                            _CMSDBDataContext.EUS_CustomerTransactions.InsertOnSubmit(transaction)
                            _CMSDBDataContext.EUS_CustomerCredits.InsertOnSubmit(cr1)

                            _CMSDBDataContext.SubmitChanges()
                        End If

                    End If
                Else
                    If paymethod = PaymentMethods.PaymentWall Then transaction.DebitAmountOriginal = _Price.Amount
                    transaction.DebitAmountOriginal = Money
                    transaction.DebitProductPrice = _Price.Amount


                    ' currency defined by product package
                    If (Currency = "EUR" AndAlso _Price.Currency <> "EUR") Then
                        transaction.DebitAmount = Money - (Money * payment_provider_ratio)
                        transaction.DebitAmountReceived = Money
                    Else
                        '' If (_Price.Currency = "EUR") Then
                        transaction.DebitAmount = _Price.Amount - (_Price.Amount * payment_provider_ratio)
                        transaction.DebitAmountReceived = _Price.Amount
                    End If

                    Try
                        If (Currency <> "EUR") Then
                            ' convert to EURO
                            Dim cur As SYS_Currency = (From itm In _CMSDBDataContext.SYS_Currencies
                                                       Where itm.CurrencyName = Currency
                                                       Select itm).FirstOrDefault()

                            If (cur IsNot Nothing AndAlso cur.Rate > 0) Then
                                transaction.CurrencyRateToEuro = cur.Rate
                            End If


                            ' currecny defined by payment provider
                            If (_Price.Currency <> "EUR") Then

                                If (cur IsNot Nothing AndAlso cur.Rate > 0) Then
                                    Dim newMoney = Money / cur.Rate
                                    transaction.DebitAmount = newMoney - (newMoney * payment_provider_ratio)
                                    transaction.DebitAmountReceived = newMoney
                                End If

                            End If

                        End If

                    Catch ex As Exception
                        Try
                            Dim body As String = clsCustomer.GetMessageBody(Description, Money, TransactionInfo, p4,
                                                         CreditsPurchased, REMOTE_ADDR,
                                                         HTTP_USER_AGENT, HTTP_REFERER, CustomerID, CustomReferrer,
                                                         PayerEmail, paymethod, PromoCode, TransactionTypeID, Currency)

                            body = ex.ToString & vbCrLf & vbCrLf & body
                            'Dim toAddress As String = ConfigurationManager.AppSettings("gToEmail")
                            clsMyMail.TrySendMailSupport(ConfigurationManager.AppSettings("PaymentLogsEmail"), "Payment service (CurrencyRateToEuro calc error, payment continues)", body, True)
                        Catch
                        End Try
                    End Try
                    Dim SubscriptionCredits As Integer = 0
                    Dim OldSubscriptionExpiration As DateTime = Date.MinValue
                    Dim SubscriptionREF_CRD2EURO_Rate As Double = 0
                    If (_Price.IsSubscription) Then
                        ' try to find prev subscription
                        Try
                            Dim geo As SYS_CountriesGEO = SYS_CountriesGEOHelper.SYS_CountriesGEO_GetForCountry(_CMSDBDataContext, profile.Country)

                            Dim _EUS_Pps As EUS_ProfilesPrivacySetting = (From itm In _CMSDBDataContext.EUS_ProfilesPrivacySettings
                                                                                             Where itm.ProfileID = CustomerID OrElse itm.MirrorProfileID = CustomerID
                                                                                             Select itm).FirstOrDefault()
                            If (_EUS_Pps Is Nothing) Then
                                _EUS_Pps = New EUS_ProfilesPrivacySetting()
                                _EUS_Pps.HideMeFromUsersInDifferentCountry = False
                                _EUS_Pps.PrivacySettings_ShowMeOffline = False
                                _CMSDBDataContext.EUS_ProfilesPrivacySettings.InsertOnSubmit(_EUS_Pps)
                            End If

                            _EUS_Pps.ProfileID = CustomerID
                            _EUS_Pps.MirrorProfileID = profile.MirrorProfileID
                            _EUS_Pps.MaxMessagesToProfilePerDay = geo.MaxMessagesToProfilePerDay
                            _EUS_Pps.SubscriptionMaxWomanProfiles = geo.SubscriptionMaxWomanProfiles
                            _EUS_Pps.MaxMessagesToDifferentProfilesPerDay = geo.MaxMessagesToDifferentProfilesPerDay

                            SubscriptionCredits = geo.MaxMessagesToProfilePerDay * geo.SubscriptionMaxWomanProfiles * _Price.duration * ProfileHelper.Config_UNLOCK_MESSAGE_SEND_CREDITS
                            SubscriptionREF_CRD2EURO_Rate = SYS_CountriesGEOHelper.SYS_CountriesGEO_GetSubscriptionREF_CRD2EURO_Rate(geo)

                            Dim cur As EUS_CustomerCredit = (From itm In _CMSDBDataContext.EUS_CustomerCredits
                                                               Where itm.CustomerId = CustomerID AndAlso itm.IsSubscription = True AndAlso itm.DateTimeExpiration > DateTime.UtcNow
                                                               Order By itm.DateTimeExpiration Descending
                                                               Select itm).FirstOrDefault()

                            If (cur IsNot Nothing) Then
                                OldSubscriptionExpiration = cur.DateTimeExpiration
                            End If
                        Catch ex As Exception
                            Try
                                Dim body As String = clsCustomer.GetMessageBody(Description, Money, TransactionInfo, p4,
                                                             CreditsPurchased, REMOTE_ADDR,
                                                             HTTP_USER_AGENT, HTTP_REFERER, CustomerID, CustomReferrer,
                                                             PayerEmail, paymethod, PromoCode, TransactionTypeID, Currency)

                                body = ex.ToString & vbCrLf & vbCrLf & body
                                'Dim toAddress As String = ConfigurationManager.AppSettings("gToEmail")
                                clsMyMail.TrySendMailSupport(ConfigurationManager.AppSettings("PaymentLogsEmail"), "Payment service (checking of previous subscription failed)", body, True)
                            Catch
                            End Try
                        End Try
                    End If
                    Dim NewExpirationDate As DateTime = Date.MinValue
                    If (OldSubscriptionExpiration > Date.MinValue) Then
                        If (_Price.IsSubscription) Then
                            Select Case _Price.duration
                                Case 30
                                    NewExpirationDate = OldSubscriptionExpiration.AddMonths(1)
                                Case 90
                                    NewExpirationDate = OldSubscriptionExpiration.AddMonths(3)
                                Case 180
                                    NewExpirationDate = OldSubscriptionExpiration.AddMonths(6)
                                Case 360
                                    NewExpirationDate = OldSubscriptionExpiration.AddMonths(12)
                            End Select
                        Else
                            NewExpirationDate = OldSubscriptionExpiration.AddDays(_Price.duration)
                        End If
                    Else
                        If (_Price.IsSubscription) Then
                            Select Case _Price.duration
                                Case 30
                                    NewExpirationDate = DateTime.UtcNow.AddMonths(1)
                                Case 90
                                    NewExpirationDate = DateTime.UtcNow.AddMonths(3)
                                Case 180
                                    NewExpirationDate = DateTime.UtcNow.AddMonths(6)
                                Case 360
                                    NewExpirationDate = DateTime.UtcNow.AddMonths(12)
                            End Select
                        Else
                            NewExpirationDate = DateTime.UtcNow.AddDays(_Price.duration)
                        End If
                    End If
                    credit.Credits = If(SubscriptionCredits > 0, SubscriptionCredits, _Price.Credits)
                    credit.CreditstoBeExpired = credit.Credits
                    credit.CustomerId = CustomerID
                    credit.DateTimeCreated = DateTime.UtcNow
                    credit.DateTimeExpiration = NewExpirationDate
                    credit.ValidForDays = _Price.duration
                    credit.EuroAmount = transaction.DebitAmount
                    credit.Currency = Currency
                    credit.IsSubscription = _Price.IsSubscription
                    If (credit.IsSubscription = True) Then credit.SubscriptionAvailableAmount = credit.EuroAmount

                    transaction.Currency = Currency
                    transaction.IPNMetadata = GetIPNMetadata(prms)
                    transaction.IsSubscription = clsNullable.NullTo(_Price.IsSubscription, False)


                    transaction.EUS_CustomerCredits.Add(credit)

                    _CMSDBDataContext.EUS_CustomerTransactions.InsertOnSubmit(transaction)
                    _CMSDBDataContext.EUS_CustomerCredits.InsertOnSubmit(credit)

                    _CMSDBDataContext.SubmitChanges()

                    If (credit.IsSubscription) Then
                        UpdateEUS_Profiles_REF_CRD2EURO_Rate(CustomerID, SubscriptionREF_CRD2EURO_Rate)
                        'Dim REF_CRD2EURO_Rate As Double = clsConfigValues.Get__referrer_commission_conv_rate()
                        'UpdateEUS_Profiles_REF_CRD2EURO_Rate(CustomerID, REF_CRD2EURO_Rate)
                    Else
                        If credit.EuroAmount > 0 Then
                            Dim REF_CRD2EURO_Rate As Double = 0
                            '      Select case 

                            REF_CRD2EURO_Rate = ((credit.EuroAmount - (credit.EuroAmount * 0.23)) / (If(credit.Credits > 500, credit.Credits + 200, credit.Credits)))
                            If REF_CRD2EURO_Rate > 0 Then UpdateEUS_Profiles_REF_CRD2EURO_Rate(CustomerID, REF_CRD2EURO_Rate)
                        End If
                    End If
                    CustomerTransactionID = transaction.CustomerTransactionID
                    If (PromoCode = "REGISTRATION_GIFT") Then
                        SendNotification_FreeRegistrationBonus(profile, Money, _Price, PayerEmail, CustomerID)
                    ElseIf (PromoCode = "REGISTRATION_GIFTGreek") Then
                        SendNotification_FreeRegistrationBonusGreek(profile, Money, _Price, PayerEmail, CustomerID)
                    ElseIf (PromoCode = "1000_CREDITS_GIFT") Then
                        SendNotification_BonusCreditsGainedForPreviousBuy(profile, _Price, PayerEmail, CustomerID)
                    ElseIf (PromoCode = "5000_XMAS_2016_GIFT") Then
                        SendNotification_BonusCreditsGainedForPreviousXmasBuy(profile, _Price, PayerEmail, CustomerID)
                    ElseIf (PromoCode = "300_CREDITS_GIFT") Then
                        SendNotification_300BonusCreditsGainedForPreviousBuy(profile, _Price, PayerEmail, CustomerID)
                    ElseIf (PromoCode = "HappyBirthday_GIFT" Or PromoCode = "Mobile_GIFT") Then
                    ElseIf (Not String.IsNullOrEmpty(PromoCode) AndAlso PromoCode <> "ReselerPromo") Then
                        SendNotification_BonusCreditsObtained(profile, Money, _Price, PayerEmail, CustomerID)
                    Else
                        SendNotification_PaymentSuccessful(profile, Money, _Price, PayerEmail, CustomerID)
                    End If

                    Try

                        If (transaction.DebitAmountOriginal < transaction.DebitProductPrice) Then
                            Dim body As String = clsCustomer.GetMessageBody(Description, Money, TransactionInfo, p4,
                                           CreditsPurchased, REMOTE_ADDR,
                                           HTTP_USER_AGENT, HTTP_REFERER, CustomerID, CustomReferrer,
                                           PayerEmail, paymethod, PromoCode, TransactionTypeID, Currency)

                            body = vbCrLf & "<br/>" & _
                                    "The money amount sent to the UNIPAY service is lower than product's price" & "<br/>" & "<br/>" & vbCrLf & vbCrLf & _
                                    "---------------------------------" & "<br/>" & vbCrLf & _
                                    "Amoun sent to service: " & transaction.DebitAmountOriginal & "<br/>" & vbCrLf & _
                                    "Product price: " & transaction.DebitProductPrice & "<br/>" & vbCrLf & _
                                    "---------------------------------" & "<br/>" & vbCrLf & _
                                    vbCrLf & "<br/>" & body
                            clsMyMail.TrySendMailSupport(ConfigurationManager.AppSettings("PaymentLogsEmail"), "Lower money amount sent to the UNIPAY service.", body, True)

                        End If

                    Catch
                        ' ex As Exception
                        'clsMyMail.SendMail(ConfigurationManager.AppSettings("PaymentLogsEmail"), "Payment service (exc)", body, True)
                    End Try
                End If








            Catch ex As Exception
                Dim body As String = clsCustomer.GetMessageBody(Description, Money, TransactionInfo, p4,
                               CreditsPurchased, REMOTE_ADDR,
                               HTTP_USER_AGENT, HTTP_REFERER, CustomerID, CustomReferrer,
                               PayerEmail, paymethod, PromoCode, TransactionTypeID, Currency)

                body = ex.ToString & vbCrLf & vbCrLf & body
                'Dim toAddress As String = ConfigurationManager.AppSettings("gToEmail")
                clsMyMail.TrySendMailSupport(ConfigurationManager.AppSettings("PaymentLogsEmail"), "Payment service", body, True)

                Throw

            End Try

            '     TransactionInfo.

            Try
                If (CustomerTransactionID > 0 AndAlso credit.EuroAmount > 0 AndAlso TransactionTypeID > -100 AndAlso PromoCode <> "1000_CREDITS_GIFT" AndAlso PromoCode <> "10000_CREDITS_GIFT" AndAlso PromoCode <> "5000_XMAS_2016_GIFT") Then

                    Using dt As DataTable = clsCreditsHelper.EUS_CustomerCredits_GetLastCreditsObtained(CustomerID, CustomerTransactionID)
                        If (dt.Rows.Count > 0) Then
                            ' Dim dt1 As DateTime = Now
                            '     If DateTime.TryParse(CDate(dt.Rows(0)("DateTimeCreated")), dt1) Then
                            ' Dim dt2 As DateTime = New DateTime(2015, 2, 3, 0, 0, 0)
                            If (Not dt.Rows(0).IsNull("Credits") AndAlso dt.Rows(0)("Credits") = 10000) Then ' Then
                                ' add 1000 credits gift
                                ' TransactionTypeID: 4 = Bonus CustomerTransactionTypeID
                                Dim _prms2 As New clsCustomer.AddTransactionParams()
                                _prms2.Description = "Add 1000 credits gift (10000 credits was bought)"
                                _prms2.Money = 0
                                _prms2.TransactionInfo = "Add 1000 credits gift"
                                _prms2.CreditsPurchased = 1000
                                _prms2.REMOTE_ADDR = REMOTE_ADDR
                                _prms2.HTTP_USER_AGENT = HTTP_USER_AGENT
                                _prms2.HTTP_REFERER = HTTP_REFERER
                                _prms2.CustomerID = CustomerID
                                _prms2.CustomReferrer = CustomReferrer
                                _prms2.PayerEmail = PayerEmail
                                _prms2.paymethod = PaymentMethods.None
                                _prms2.PromoCode = "1000_CREDITS_GIFT"
                                _prms2.TransactionTypeID = 4
                                _prms2.productCode = Nothing
                                _prms2.Currency = "EUR"
                                _prms2.cDataRecordIPN = cDataRecordIPN
                                _prms2.freeUnlocks = 0
                                _prms2.useVoucherCredits = False
                                _prms2.durationDays = 45
                                _prms2.voucherCredits = 0

                                clsCustomer.AddTransaction(_prms2)
                            End If
                        End If
                        '    End If
                    End Using

                    'christmas

                    Using dt As DataTable = clsCreditsHelper.EUS_CustomerCredits_GetLastCreditsObtained(CustomerID, CustomerTransactionID)

                        Dim dt1 As DateTime = DateTime.UtcNow
                        Dim dt2 As DateTime = New DateTime(2015, 12, 17, 0, 0, 0)
                        Dim CheckOld As Boolean = False
                        If dt1 >= dt2 Then
                            If credit.Credits = 20000 Then
                                Dim dt3 As DataTable = clsCreditsHelper.EUS_CustomerCredits_GetLast20000CreditsObtainedAfterGift(CustomerID, CustomerTransactionID)
                                If dt3 IsNot Nothing AndAlso dt3.Rows.Count > 0 Then
                                    Dim prTradt As DateTime = Now.AddDays(-100)
                                    If DateTime.TryParse(dt3.Rows(0)("DateTimeCreated"), prTradt) Then
                                        If prTradt > dt2 Then
                                            If DateDiff(DateInterval.Hour, prTradt, DateTime.UtcNow, FirstDayOfWeek.Monday, FirstWeekOfYear.Jan1) < 120 Then
                                                Dim _prms2 As New clsCustomer.AddTransactionParams()
                                                _prms2.Description = "Add 5000 credits gift (20000 credits was bought) Christmas Offer"
                                                _prms2.Money = 0
                                                _prms2.TransactionInfo = "Add 5000 credits gift"
                                                _prms2.CreditsPurchased = 5000
                                                _prms2.REMOTE_ADDR = REMOTE_ADDR
                                                _prms2.HTTP_USER_AGENT = HTTP_USER_AGENT
                                                _prms2.HTTP_REFERER = HTTP_REFERER
                                                _prms2.CustomerID = CustomerID
                                                _prms2.CustomReferrer = CustomReferrer
                                                _prms2.PayerEmail = PayerEmail
                                                _prms2.paymethod = PaymentMethods.None
                                                _prms2.PromoCode = "5000_XMAS_2016_GIFT"
                                                _prms2.TransactionTypeID = 4
                                                _prms2.productCode = Nothing
                                                _prms2.Currency = "EUR"
                                                _prms2.cDataRecordIPN = cDataRecordIPN
                                                _prms2.freeUnlocks = 0
                                                _prms2.useVoucherCredits = False
                                                _prms2.durationDays = 90
                                                _prms2.voucherCredits = 0
                                                clsCustomer.AddTransaction(_prms2)
                                            End If
                                        Else
                                            CheckOld = True
                                        End If
                                    End If
                                End If
                            Else
                                CheckOld = True
                            End If
                        Else
                            CheckOld = True
                        End If



                        'If CheckOld Then
                        '    If (dt.Rows.Count > 0) Then
                        '        If DateTime.TryParse(CDate(dt.Rows(0)("DateTimeCreated")), dt1) Then
                        '            If (Not dt.Rows(0).IsNull("Credits") AndAlso dt.Rows(0)("Credits") = 10000) AndAlso dt1 < dt2 Then ' Then
                        '                ' add 1000 credits gift
                        '                ' TransactionTypeID: 4 = Bonus CustomerTransactionTypeID
                        '                Dim _prms2 As New clsCustomer.AddTransactionParams()
                        '                _prms2.Description = "Add 1000 credits gift (10000 credits was bought)"
                        '                _prms2.Money = 0
                        '                _prms2.TransactionInfo = "Add 1000 credits gift"
                        '                _prms2.CreditsPurchased = 1000
                        '                _prms2.REMOTE_ADDR = REMOTE_ADDR
                        '                _prms2.HTTP_USER_AGENT = HTTP_USER_AGENT
                        '                _prms2.HTTP_REFERER = HTTP_REFERER
                        '                _prms2.CustomerID = CustomerID
                        '                _prms2.CustomReferrer = CustomReferrer
                        '                _prms2.PayerEmail = PayerEmail
                        '                _prms2.paymethod = PaymentMethods.None
                        '                _prms2.PromoCode = "1000_CREDITS_GIFT"
                        '                _prms2.TransactionTypeID = 4
                        '                _prms2.productCode = Nothing
                        '                _prms2.Currency = "EUR"
                        '                _prms2.cDataRecordIPN = cDataRecordIPN
                        '                _prms2.freeUnlocks = 0
                        '                _prms2.useVoucherCredits = False
                        '                _prms2.durationDays = 45
                        '                _prms2.voucherCredits = 0

                        '                clsCustomer.AddTransaction(_prms2)
                        '                '   ElseIf dt Then
                        '            End If
                        '        End If
                        '    End If
                        'End If
                        'If (paymethod = PaymentMethods.CreditCard AndAlso profile IsNot Nothing AndAlso profile.Country = "GR" AndAlso TransactionTypeID > -100) Then
                        '    Dim _prms2 As New clsCustomer.AddTransactionParams()
                        '    _prms2.Description = "Add 300 credits gift (Payed with credit card)"
                        '    _prms2.Money = 0
                        '    _prms2.TransactionInfo = "Add 300 credits gift"
                        '    _prms2.CreditsPurchased = 300
                        '    _prms2.REMOTE_ADDR = REMOTE_ADDR
                        '    _prms2.HTTP_USER_AGENT = HTTP_USER_AGENT
                        '    _prms2.HTTP_REFERER = HTTP_REFERER
                        '    _prms2.CustomerID = CustomerID
                        '    _prms2.CustomReferrer = CustomReferrer
                        '    _prms2.PayerEmail = PayerEmail
                        '    _prms2.paymethod = PaymentMethods.None
                        '    _prms2.PromoCode = "300_CREDITS_GIFT"
                        '    _prms2.TransactionTypeID = 4
                        '    _prms2.productCode = Nothing
                        '    _prms2.Currency = "EUR"
                        '    _prms2.cDataRecordIPN = cDataRecordIPN
                        '    _prms2.freeUnlocks = 0
                        '    _prms2.useVoucherCredits = False
                        '    _prms2.durationDays = 45
                        '    _prms2.voucherCredits = 0
                        '    clsCustomer.AddTransaction(_prms2)
                        'End If
                        '    End If
                    End Using
                End If


                'If (CustomerTransactionID > 0 AndAlso credit.EuroAmount > 0 AndAlso credit.Credits = 10000 AndAlso PromoCode <> "1000_CREDITS_GIFT" AndAlso PromoCode <> "10000_CREDITS_GIFT") Then
                '    ''10000credits+1000gift
                '    ' Dim dt As DataTable = clsCreditsHelper.EUS_CustomerCredits_GetLastCreditsObtained(CustomerID, CustomerTransactionID)
                '    ''20000credits +10000gift in 5days

                '    Dim _prms1 As New clsCustomer.AddTransactionParams()
                '    _prms1.Description = "Add 10000 credits gift (10000 credits was bought)"
                '    _prms1.Money = 0
                '    _prms1.TransactionInfo = "Add 10000 credits gift"
                '    _prms1.CreditsPurchased = 10000
                '    _prms1.REMOTE_ADDR = REMOTE_ADDR
                '    _prms1.HTTP_USER_AGENT = HTTP_USER_AGENT
                '    _prms1.HTTP_REFERER = HTTP_REFERER
                '    _prms1.CustomerID = CustomerID
                '    _prms1.CustomReferrer = CustomReferrer
                '    _prms1.PayerEmail = PayerEmail
                '    _prms1.paymethod = PaymentMethods.None
                '    _prms1.PromoCode = "10000_CREDITS_GIFT"
                '    _prms1.TransactionTypeID = 4
                '    _prms1.productCode = Nothing
                '    _prms1.Currency = "EUR"
                '    _prms1.cDataRecordIPN = cDataRecordIPN
                '    _prms1.freeUnlocks = 0
                '    _prms1.useVoucherCredits = False
                '    _prms1.durationDays = 180
                '    _prms1.voucherCredits = 0
                '    clsCustomer.AddTransaction(_prms1)
                'End If

            Catch ex As Exception
                Try

                    Dim body As String = clsCustomer.GetMessageBody(Description, Money, TransactionInfo, p4,
                                   CreditsPurchased, REMOTE_ADDR,
                                   HTTP_USER_AGENT, HTTP_REFERER, CustomerID, CustomReferrer,
                                   PayerEmail, paymethod, PromoCode, TransactionTypeID, Currency)

                    body = ex.ToString & vbCrLf & vbCrLf & body
                    clsMyMail.TrySendMailSupport(ConfigurationManager.AppSettings("PaymentLogsEmail"), "Payment service (1000 credits gift error, payment continues)", body, True)
                Catch
                End Try
            End Try
        End Using
        Return CustomerTransactionID
    End Function



    Public Shared Sub SendNotification_PaymentSuccessful(profile As DSMembers.EUS_ProfilesRow, Money As Double, _Price As EUS_Price, PayerEmail As String, CustomerID As Integer)


        Dim toAddress As String = ConfigurationManager.AppSettings("PaymentLogsEmail")
        Dim subject As String = "Payment successfull"

        ' default body content
        Dim body As String = Nothing

        Try

            Dim lagid As String = "US"
            If (Not profile.IsLAGIDNull()) Then
                lagid = profile.LAGID
            End If
            Dim o As New clsSiteLAG()
            subject = o.GetCustomStringFromPage(lagid, "GlobalStrings", "PaymentMessageSubject", DataHelpers.ConnectionString)
            body = o.GetCustomStringFromPage(lagid, "GlobalStrings", "PaymentMessageText2", DataHelpers.ConnectionString)
            body = Regex.Replace(body, "###LOGINNAME###", profile.LoginName)


        Catch ex As Exception
            clsMyMail.TrySendMailSupport(ConfigurationManager.AppSettings("ExceptionsEmail"), "Exception while reading cms content", ex.ToString())
        End Try


        If (body IsNot Nothing) Then
            Try
                Dim dt As DataTable = GetRandomProfiles(profile)
                body = ModifyMessageContent(dt, body)
            Catch ex As Exception

                clsMyMail.TrySendMailSupport(ConfigurationManager.AppSettings("ExceptionsEmail"), "Exception while add random profiles to body", ex.ToString())
            Finally
                Try
                    body = body.
                        Replace("[PROFILE-URL1]", "#").
                        Replace("[PROFILE-URL2]", "#").
                        Replace("[PROFILE-URL3]", "#").
                        Replace("[PROFILE-URL4]", "#").
                        Replace("[PROFILE-LOGIN1]", "").
                        Replace("[PROFILE-LOGIN2]", "").
                        Replace("[PROFILE-LOGIN3]", "").
                        Replace("[PROFILE-LOGIN4]", "").
                        Replace("[PROFILE-PHOTO-URL1]", "").
                        Replace("[PROFILE-PHOTO-URL2]", "").
                        Replace("[PROFILE-PHOTO-URL3]", "").
                        Replace("[PROFILE-PHOTO-URL4]", "")
                Catch
                End Try
            End Try

            Try

                body = body.Replace("###MONEYAMOUNT###", Money & "euro")
                body = body.Replace("###CREDITS###", _Price.Credits)

                Dim durStr As String = GetDurationString(_Price.duration)
                body = body.Replace("###DURATION###", durStr)

                If (String.IsNullOrEmpty(PayerEmail)) Then PayerEmail = profile.eMail
                clsMyMail.TrySendMail(PayerEmail, subject, body, True)
                clsMyMail.TrySendMailSupport(toAddress, subject, body, True)

            Catch ex As Exception
                clsMyMail.TrySendMailSupport(ConfigurationManager.AppSettings("ExceptionsEmail"), "Exception while sending-" & subject, ex.ToString())
            End Try

            clsUserDoes.SendMessageFromAdministrator(subject, body, CustomerID)
        End If

    End Sub

    Private Shared Function ModifyMessageContent(dt As DataTable, Content As String) As String

        Dim view_profile_url As String = clsConfigValues.GetSYS_ConfigValue("view_profile_url")
        Dim image_thumb_url As String = clsConfigValues.GetSYS_ConfigValue("image_thumb_url")

        Dim checkList As New List(Of Integer)
        Dim counter As Integer = 1
        For cnt = 0 To dt.Rows.Count - 1

            Dim dr As DataRow = dt.Rows(cnt)
            If (dr.IsNull("FileName")) Then
                Continue For
            End If

            Dim CustomerID As Integer = dr(0)
            If (checkList.Contains(CustomerID)) Then
                Continue For
            End If
            checkList.Add(CustomerID)

            Dim PROFILE_LOGIN As String = dr("LoginName").ToString()
            Dim PROFILE_URL As String = view_profile_url.Replace("{0}", HttpUtility.UrlEncode(PROFILE_LOGIN))
            Dim PROFILE_PHOTO_URL As String = image_thumb_url.Replace("{0}", dr("ProfileID")).Replace("{1}", dr("FileName"))

            Content = Content.Replace("[PROFILE-URL" & counter & "]", PROFILE_URL)
            Content = Content.Replace("[PROFILE-LOGIN" & counter & "]", PROFILE_LOGIN)
            Content = Content.Replace("[PROFILE-PHOTO-URL" & counter & "]", PROFILE_PHOTO_URL)

            counter = counter + 1

            'stis 4 photos exit
            If (counter > 4) Then Exit For
        Next


        Return Content
    End Function


    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")> Private Shared Function GetRandomProfiles(profile As DSMembers.EUS_ProfilesRow) As DataTable


        Dim sql As String = <sql><![CDATA[
            and  EUS_Profiles.Profileid in (
select ProfileID 
from (
	select 
		RowNumber =ROW_NUMBER() over(order by NEWID()),
		ProfileID, LoginName, genderid, DateTimeToRegister
	from EUS_Profiles p 
	where ismaster=1
	and genderid=2
	and [status] in (2,4)
-- current user not seen the other one
	and  not exists(
		select  FromProfileID
		from  dbo.EUS_ProfilesViewed
		where  FromProfileID=@CurrentProfileId and ToProfileID=p.Profileid 
	)
-- has public photo
    and exists (select customerId 
				from EUS_CustomerPhotos phot1 
				where phot1.customerId = p.ProfileId 
				and phot1.HasAproved=1 
				and phot1.DisplayLevel=0
				and phot1.HasDeclined=0 --and ISNULL(phot1.HasDeclined,0)=0 
				and phot1.IsDeleted=0--and ISNULL(phot1.IsDeleted,0)=0)
    ) 
) as t 
where t.RowNumber<101
	        )
]]></sql>.Value



        Dim prms As New clsSearchHelperParameters()
        prms.CurrentProfileId = profile.ProfileID
        prms.ReturnRecordsWithStatus = ProfileStatusEnum.Approved
        prms.SearchSort = SearchSortEnum.None
        prms.zipstr = If(Not profile.IsZipNull(), profile.Zip, "")
        prms.latitudeIn = If(Not profile.IslatitudeNull(), profile.latitude, Nothing)
        prms.longitudeIn = If(Not profile.IslongitudeNull(), profile.longitude, Nothing)
        prms.LookingFor_ToMeetMaleID = If(Not profile.IsLookingFor_ToMeetMaleIDNull(), profile.LookingFor_ToMeetMaleID, Nothing)
        prms.LookingFor_ToMeetFemaleID = If(Not profile.IsLookingFor_ToMeetFemaleIDNull(), profile.LookingFor_ToMeetFemaleID, Nothing)
        prms.NumberOfRecordsToReturn = 0
        prms.AdditionalWhereClause = sql
        prms.performCount = False
        prms.rowNumberMin = 0
        prms.rowNumberMax = 5


        Dim countRows As Integer

        Using ds As DataSet = clsSearchHelper.GetMembersToSearchDataTable(prms)
            If (prms.performCount) Then
                countRows = ds.Tables(0).Rows(0)(0)
                If (countRows > 0) Then
                    Using dt = ds.Tables(1)
                        Return dt
                    End Using
                End If
            Else
                countRows = ds.Tables(0).Rows.Count
                If (countRows > 0) Then
                    Using dt = ds.Tables(0)

                        Return dt
                    End Using
                End If
            End If
        End Using
        Return Nothing
    End Function

    Public Shared Sub SendNotification_BonusCreditsObtained(profile As DSMembers.EUS_ProfilesRow, Money As Double, _Price As EUS_Price, PayerEmail As String, CustomerID As Integer)

        Dim toAddress As String = ConfigurationManager.AppSettings("PaymentLogsEmail")
        Dim subject As String = "Payment successfull"

        ' default body content
        Dim body As String = <body><![CDATA[
Your payment at goomena.com was successfull. <BR>You payed ###MONEYAMOUNT### and you gained an amount of ###CREDITS### credits. These credits are available during ###DURATION###. <BR><BR><BR>For any information don't hesitate to contact us at <A href="mailto:info@goomena.com">info@goomena.com</A>. <BR><BR>At your disposal, <BR>goomena.com website. 
]]></body>.Value

        Try
            Dim lagid As String = "US"
            If (Not profile.IsLAGIDNull()) Then
                lagid = profile.LAGID
            End If
            Dim o As New clsSiteLAG()
            subject = o.GetCustomStringFromPage(lagid, "GlobalStrings", "BonusCreditsObtainedSubject", DataHelpers.ConnectionString)
            body = o.GetCustomStringFromPage(lagid, "GlobalStrings", "BonusCreditsObtainedText", DataHelpers.ConnectionString)
            body = Regex.Replace(body, "###LOGINNAME###", profile.LoginName)
        Catch ex As Exception
        End Try


        If (body IsNot Nothing) Then
            body = body.Replace("###MONEYAMOUNT###", Money & "euro")
            body = body.Replace("###CREDITS###", _Price.Credits)

            Dim durStr As String = GetDurationString(_Price.duration)
            body = body.Replace("###DURATION###", durStr)
        End If

        Try
            clsMyMail.TrySendMail(PayerEmail, subject, body, True)
            clsMyMail.TrySendMailSupport(toAddress, subject, body, True)
        Catch
        End Try

        clsUserDoes.SendMessageFromAdministrator(subject, body, CustomerID)
    End Sub
    Public Shared Sub SendNotification_FreeRegistrationBonusGreek(profile As DSMembers.EUS_ProfilesRow, Money As Double, _Price As EUS_Price, PayerEmail As String, CustomerID As Integer)

        Dim toAddress As String = ConfigurationManager.AppSettings("PaymentLogsEmail")
        Dim subject As String = "Δωρεάν Credits"

        ' default body content
        Dim body As String = ""

        Try
            Dim lagid As String = "US"
            If (Not profile.IsLAGIDNull()) Then
                lagid = profile.LAGID
            End If
            Dim o As New clsSiteLAG()
            subject = o.GetCustomStringFromPage(lagid, "GlobalStrings", "NotificationEmail_FreeRegisterationCreditsSubject", DataHelpers.ConnectionString)
            body = o.GetCustomStringFromPage(lagid, "GlobalStrings", "NotificationEmail_FreeRegisterationCreditsTextGreek", DataHelpers.ConnectionString)
            body = Regex.Replace(body, "###CREDITS_GIFT###", _Price.Credits.ToString())
        Catch
        End Try


        If (body IsNot Nothing) Then
            body = body.Replace("###MONEYAMOUNT###", Money & "euro")
            body = body.Replace("###CREDITS###", _Price.Credits)

            Dim durStr As String = GetDurationString(_Price.duration)
            body = body.Replace("###DURATION###", durStr)
        End If

        clsMyMail.TrySendMail(PayerEmail, subject, body, True)
        clsMyMail.TrySendMailSupport(toAddress, subject, body, True)

        clsUserDoes.SendMessageFromAdministrator(subject, body, CustomerID)
    End Sub

    Public Shared Sub SendNotification_FreeRegistrationBonus(profile As DSMembers.EUS_ProfilesRow, Money As Double, _Price As EUS_Price, PayerEmail As String, CustomerID As Integer)

        Dim toAddress As String = ConfigurationManager.AppSettings("PaymentLogsEmail")
        Dim subject As String = "Payment successfull"

        ' default body content
        Dim body As String = ""

        Try
            Dim lagid As String = "US"
            If (Not profile.IsLAGIDNull()) Then
                lagid = profile.LAGID
            End If
            Dim o As New clsSiteLAG()
            subject = o.GetCustomStringFromPage(lagid, "GlobalStrings", "NotificationEmail_FreeRegisterationCreditsSubject", DataHelpers.ConnectionString)
            body = o.GetCustomStringFromPage(lagid, "GlobalStrings", "NotificationEmail_FreeRegisterationCreditsText", DataHelpers.ConnectionString)
            body = Regex.Replace(body, "###CREDITS_GIFT###", _Price.Credits.ToString())
        Catch
        End Try


        If (body IsNot Nothing) Then
            body = body.Replace("###MONEYAMOUNT###", Money & "euro")
            body = body.Replace("###CREDITS###", _Price.Credits)

            Dim durStr As String = GetDurationString(_Price.duration)
            body = body.Replace("###DURATION###", durStr)
        End If

        clsMyMail.TrySendMail(PayerEmail, subject, body, True)
        clsMyMail.TrySendMailSupport(toAddress, subject, body, True)

        clsUserDoes.SendMessageFromAdministrator(subject, body, CustomerID)
    End Sub


    Public Shared Sub SendNotification_BonusCreditsGainedForPreviousXmasBuy(profile As DSMembers.EUS_ProfilesRow, _Price As EUS_Price, PayerEmail As String, CustomerID As Integer)

        '  Dim toAddress As String = ConfigurationManager.AppSettings("PaymentLogsEmail")
        Dim subject As String = "Bonus Credits Gained"

        ' default body content
        Dim body As String = Nothing

        Try
            Dim lagid As String = "US"
            If (Not profile.IsLAGIDNull()) Then
                lagid = profile.LAGID
            End If
            Dim o As New clsSiteLAG()
            subject = o.GetCustomStringFromPage(lagid, "GlobalStrings", "NotificationEmail_BonusCreditsGainedForPreviousxmas2014Buy_Subject", DataHelpers.ConnectionString)
            body = o.GetCustomStringFromPage(lagid, "GlobalStrings", "NotificationEmail_BonusCreditsGainedForPreviousxmas2014Buy_Text", DataHelpers.ConnectionString)
        Catch
        End Try
        If (Not String.IsNullOrEmpty(body)) Then
            body = Regex.Replace(body, "###CREDITS_GIFT###", _Price.Credits.ToString())
            body = body.Replace("###MONEYAMOUNT###", 0 & "euro")
            body = body.Replace("###CREDITS###", _Price.Credits)

            Dim durStr As String = GetDurationString(_Price.duration)
            body = body.Replace("###DURATION###", durStr)

            clsUserDoes.SendMessageFromAdministrator(subject, body, CustomerID)
        End If
    End Sub

    Public Shared Sub SendNotification_BonusCreditsGainedForPreviousBuy(profile As DSMembers.EUS_ProfilesRow, _Price As EUS_Price, PayerEmail As String, CustomerID As Integer)

        '   Dim toAddress As String = ConfigurationManager.AppSettings("PaymentLogsEmail")
        Dim subject As String = "Bonus Credits Gained"

        ' default body content
        Dim body As String = Nothing

        Try
            Dim lagid As String = "US"
            If (Not profile.IsLAGIDNull()) Then
                lagid = profile.LAGID
            End If
            Dim o As New clsSiteLAG()
            subject = o.GetCustomStringFromPage(lagid, "GlobalStrings", "NotificationEmail_BonusCreditsGainedForPreviousBuy_Subject", DataHelpers.ConnectionString)
            body = o.GetCustomStringFromPage(lagid, "GlobalStrings", "NotificationEmail_BonusCreditsGainedForPreviousBuy_Text", DataHelpers.ConnectionString)
        Catch
        End Try
        If (Not String.IsNullOrEmpty(body)) Then
            body = Regex.Replace(body, "###CREDITS_GIFT###", _Price.Credits.ToString())
            body = body.Replace("###MONEYAMOUNT###", 0 & "euro")
            body = body.Replace("###CREDITS###", _Price.Credits)

            Dim durStr As String = GetDurationString(_Price.duration)
            body = body.Replace("###DURATION###", durStr)

            clsUserDoes.SendMessageFromAdministrator(subject, body, CustomerID)
        End If
    End Sub
    Public Shared Sub SendNotification_300BonusCreditsGainedForPreviousBuy(profile As DSMembers.EUS_ProfilesRow, _Price As EUS_Price, PayerEmail As String, CustomerID As Integer)

        '   Dim toAddress As String = ConfigurationManager.AppSettings("PaymentLogsEmail")
        Dim subject As String = "Bonus Credits Gained"

        ' default body content
        Dim body As String = Nothing

        Try
            Dim lagid As String = "US"
            If (Not profile.IsLAGIDNull()) Then
                lagid = profile.LAGID
            End If
            Dim o As New clsSiteLAG()
            subject = o.GetCustomStringFromPage(lagid, "GlobalStrings", "addFree300CreditsSubject", DataHelpers.ConnectionString)
            body = o.GetCustomStringFromPage(lagid, "GlobalStrings", "addFree300CreditsText", DataHelpers.ConnectionString)
        Catch
        End Try
        If (Not String.IsNullOrEmpty(body)) Then
            body = Regex.Replace(body, "###CREDITS_GIFT###", _Price.Credits.ToString())
            body = body.Replace("###MONEYAMOUNT###", 0 & "euro")
            body = body.Replace("###CREDITS###", _Price.Credits)

            Dim durStr As String = GetDurationString(_Price.duration)
            body = body.Replace("###DURATION###", durStr)

            clsUserDoes.SendMessageFromAdministrator(subject, body, CustomerID)
        End If
    End Sub

    Public Shared Function GetMessageBody(cDataRecordIPN As clsDataRecordIPN,
                                        HTTP_USER_AGENT As String,
                                        HTTP_REFERER As String,
                                        paymethod As PaymentMethods,
                                        productCode As String,
                                        MemberID As String)

        Dim body1 As String = <sql><![CDATA[
<table>
    <tr><td align="right"><strong>Login:</strong></td><td>[Login]</td></tr>
    <tr><td align="right"><strong>Money:</strong></td><td>[Money]</td></tr>
    <tr><td align="right"><strong>Currency:</strong></td><td>[Currency]</td></tr>
    <tr><td align="right"><strong>TransactionID:</strong></td><td>[TransactionID]</td></tr>
    <tr><td align="right"><strong>CreditsPurchased:</strong></td><td>[CreditsPurchased]</td></tr>
    <tr><td align="right"><strong>CustomerID:</strong></td><td>[CustomerID]</td></tr>
    <tr><td align="right"><strong>paymethod:</strong></td><td>[paymethod] ([paymethodCode])</td></tr>
    <tr><td align="right"><strong>TransactionTypeID:</strong></td><td>[TransactionTypeID]</td></tr>
    <tr><td align="right"><strong>CustomReferrer:</strong></td><td>[CustomReferrer]</td></tr>
    <tr><td align="right"><strong>Matched ProductCode:</strong></td><td>[ProductCode]</td></tr>
    <tr><td align="right"><strong>Matched MemberID:</strong></td><td>[MemberID]</td></tr>
    <tr><td colspan="2"><hr><h3>Goomena profile Info</h3></td></tr>
    <tr><td align="right"><strong>Country:</strong></td><td>[Country]</td></tr>
    <tr><td align="right"><strong>Region:</strong></td><td>[Region]</td></tr>
    <tr><td align="right"><strong>City:</strong></td><td>[City]</td></tr>
    <tr><td align="right"><strong>Zip:</strong></td><td>[Zip]</td></tr>
    <tr><td colspan="2"><hr><h3>Buyer Info</h3></td></tr>
    <tr><td align="right"><strong>PayerEmail:</strong></td><td>[BuyerInfoPayerEmail]</td></tr>
    <tr><td align="right"><strong>ResidenceCountry:</strong></td><td>[BuyerInfoResidenceCountry]</td></tr>
    <tr><td align="right"><strong>FirstName:</strong></td><td>[BuyerInfoFirstName]</td></tr>
    <tr><td align="right"><strong>LastName:</strong></td><td>[BuyerInfoLastName]</td></tr>
    <tr><td align="right"><strong>ContactPhone:</strong></td><td>[BuyerInfoContactPhone]</td></tr>
    <tr><td align="right"><strong>BusinessName:</strong></td><td>[BuyerInfoBusinessName]</td></tr>
    <tr><td align="right"><strong>Address:</strong></td><td>[BuyerInfoAddress]</td></tr>
    <tr><td align="right"><strong>PayerStatus:</strong></td><td>[BuyerInfoPayerStatus]</td></tr>
    <tr><td align="right"><strong>PayerID:</strong></td><td>[BuyerInfoPayerID]</td></tr>
    <tr><td colspan="2"><hr><h3>Metadata</h3></td></tr>
    <tr><td align="right"><strong>Remote Address:</strong></td><td>[REMOTE_ADDR]</td></tr>
    <tr><td align="right"><strong>User Agent:</strong></td><td>[HTTP_USER_AGENT]</td></tr>
    <tr><td align="right"><strong>HTTP Referer:</strong></td><td>[HTTP_REFERER]</td></tr>
    <tr><td colspan="2"><hr></td></tr>
    <tr><td align="right" font="small"><strong>Description:</strong></td><td>[Description]</td></tr>
    <tr><td colspan="2"><hr></td></tr>
    <tr><td align="right" font="small"><strong>Payment Started:</strong></td><td>[PaymentDateTime]</td></tr>
    <tr><td align="right" font="small"><strong>Payment Complete (Greece time):</strong></td><td>[PaymentCompleteDateTime]</td></tr>
    <tr><td align="right" font="small"><strong>Payment Complete (UTC time):</strong></td><td>[PaymentCompleteDateTimeUTC]</td></tr>
    <tr><td colspan="2"><hr><h3>Other info</h3></td></tr>
    <tr><td align="right" font="small"><strong>PayTransactionID:</strong></td><td>[PayTransactionID]</td></tr>
    <tr><td align="right" font="small"><strong>PayProviderID:</strong></td><td>[PayProviderID]</td></tr>
    <tr><td align="right" font="small"><strong>SalesSiteID:</strong></td><td>[SalesSiteID]</td></tr>
    <tr><td align="right" font="small"><strong>SalesSiteProductID:</strong></td><td>[SalesSiteProductID]</td></tr>
    <tr><td align="right" font="small"><strong>PromoCode:</strong></td><td>[PromoCode]</td></tr>
</table>
]]></sql>.Value

        body1 = body1.Replace("[Description]", IIf(cDataRecordIPN.SaleDescription IsNot Nothing, cDataRecordIPN.SaleDescription, "---"))
        body1 = body1.Replace("[Money]", cDataRecordIPN.PayProviderAmount)
        body1 = body1.Replace("[Currency]", cDataRecordIPN.Currency)
        body1 = body1.Replace("[TransactionID]", IIf(cDataRecordIPN.PayProviderTransactionID IsNot Nothing, cDataRecordIPN.PayProviderTransactionID, "---"))
        body1 = body1.Replace("[p4]", "---")
        body1 = body1.Replace("[CreditsPurchased]", cDataRecordIPN.SaleQuantity)
        body1 = body1.Replace("[CustomerID]", cDataRecordIPN.CustomerID)
        body1 = body1.Replace("[CustomReferrer]", IIf(cDataRecordIPN.CustomReferrer IsNot Nothing, cDataRecordIPN.CustomReferrer, "---"))
        body1 = body1.Replace("[paymethodCode]", CType(paymethod, Integer).ToString())
        body1 = body1.Replace("[paymethod]", paymethod.ToString())
        body1 = body1.Replace("[PromoCode]", IIf(cDataRecordIPN.PromoCode IsNot Nothing, cDataRecordIPN.PromoCode, "---"))
        body1 = body1.Replace("[TransactionTypeID]", cDataRecordIPN.TransactionTypeID)
        body1 = body1.Replace("[REMOTE_ADDR]", IIf(cDataRecordIPN.custopmerIP IsNot Nothing, cDataRecordIPN.custopmerIP, "---"))
        body1 = body1.Replace("[HTTP_USER_AGENT]", IIf(HTTP_USER_AGENT IsNot Nothing, HTTP_USER_AGENT, "---"))
        body1 = body1.Replace("[HTTP_REFERER]", IIf(HTTP_REFERER IsNot Nothing, HTTP_REFERER, "---"))
        body1 = body1.Replace("[PayTransactionID]", cDataRecordIPN.PayTransactionID)
        body1 = body1.Replace("[PayProviderID]", cDataRecordIPN.PayProviderID)
        body1 = body1.Replace("[SalesSiteID]", cDataRecordIPN.SalesSiteID)
        body1 = body1.Replace("[SalesSiteProductID]", cDataRecordIPN.SalesSiteProductID)

        body1 = body1.Replace("[ProductCode]", IIf(Not String.IsNullOrEmpty(productCode), productCode, "NOT MATCHED"))
        body1 = body1.Replace("[MemberID]", IIf(Not String.IsNullOrEmpty(MemberID), MemberID, "NOT MATCHED"))

        If (cDataRecordIPN.BuyerInfo IsNot Nothing) Then
            body1 = body1.Replace("[BuyerInfoAddress]", IIf(cDataRecordIPN.BuyerInfo.Address IsNot Nothing, cDataRecordIPN.BuyerInfo.Address, "---"))
            body1 = body1.Replace("[BuyerInfoBusinessName]", IIf(cDataRecordIPN.BuyerInfo.BusinessName IsNot Nothing, cDataRecordIPN.BuyerInfo.BusinessName, "---"))
            body1 = body1.Replace("[BuyerInfoContactPhone]", IIf(cDataRecordIPN.BuyerInfo.ContactPhone IsNot Nothing, cDataRecordIPN.BuyerInfo.ContactPhone, "---"))
            body1 = body1.Replace("[BuyerInfoFirstName]", IIf(cDataRecordIPN.BuyerInfo.FirstName IsNot Nothing, cDataRecordIPN.BuyerInfo.FirstName, "---"))
            body1 = body1.Replace("[BuyerInfoLastName]", IIf(cDataRecordIPN.BuyerInfo.LastName IsNot Nothing, cDataRecordIPN.BuyerInfo.LastName, "---"))
            body1 = body1.Replace("[BuyerInfoPayerEmail]", IIf(cDataRecordIPN.BuyerInfo.PayerEmail IsNot Nothing, cDataRecordIPN.BuyerInfo.PayerEmail, "---"))
            body1 = body1.Replace("[BuyerInfoPayerID]", IIf(cDataRecordIPN.BuyerInfo.PayerID IsNot Nothing, cDataRecordIPN.BuyerInfo.PayerID, "---"))
            body1 = body1.Replace("[BuyerInfoPayerStatus]", IIf(cDataRecordIPN.BuyerInfo.PayerStatus IsNot Nothing, cDataRecordIPN.BuyerInfo.PayerStatus, "---"))
            body1 = body1.Replace("[BuyerInfoResidenceCountry]", IIf(cDataRecordIPN.BuyerInfo.ResidenceCountry IsNot Nothing, cDataRecordIPN.BuyerInfo.ResidenceCountry, "---"))
        Else
            body1 = body1.Replace("[PayerEmail]", "---")
            body1 = body1.Replace("[BuyerInfoAddress]", "---")
            body1 = body1.Replace("[BuyerInfoBusinessName]", "---")
            body1 = body1.Replace("[BuyerInfoContactPhone]", "---")
            body1 = body1.Replace("[BuyerInfoFirstName]", "---")
            body1 = body1.Replace("[BuyerInfoLastName]", "---")
            body1 = body1.Replace("[BuyerInfoPayerEmail]", "---")
            body1 = body1.Replace("[BuyerInfoPayerID]", "---")
            body1 = body1.Replace("[BuyerInfoPayerStatus]", "---")
            body1 = body1.Replace("[BuyerInfoResidenceCountry]", "---")
        End If

        Try

            Dim UTCTime As DateTime = DateTime.UtcNow
            Dim GTBZone As TimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("GTB Standard Time")
            Dim GTBTime As DateTime = TimeZoneInfo.ConvertTime(UTCTime, TimeZoneInfo.Utc, GTBZone)
            '    Dim timeDiff As TimeSpan = GTBTime - UTCTime

            body1 = body1.Replace("[PaymentCompleteDateTimeUTC]", UTCTime.ToString("dd/MM/yyyy HH:mm:ss"))
            body1 = body1.Replace("[PaymentCompleteDateTime]", GTBTime.ToString("dd/MM/yyyy HH:mm:ss"))
            body1 = body1.Replace("[PaymentDateTime]", IIf(cDataRecordIPN.PaymentDateTime IsNot Nothing, cDataRecordIPN.PaymentDateTime, "---"))

        Catch
        End Try

        Try
            Dim cust As DSMembers.EUS_ProfilesRow = DataHelpers.GetEUS_Profiles_ByProfileIDRow(cDataRecordIPN.CustomerID)
            If (cust IsNot Nothing) Then
                body1 = body1.Replace("[Login]", cust.LoginName)

                Try
                    If (Not cust.IsCountryNull() AndAlso Not String.IsNullOrEmpty(cust.Country)) Then
                        body1 = body1.Replace("[Country]", ProfileHelper.GetCountryName(cust.Country))
                    Else
                        body1 = body1.Replace("[Country]", "Country not found in profile")
                    End If
                Catch ex As Exception
                End Try

                If (Not cust.Is_RegionNull() AndAlso Not String.IsNullOrEmpty(cust._Region)) Then
                    body1 = body1.Replace("[Region]", cust._Region)
                Else
                    body1 = body1.Replace("[Region]", "Region not found in profile")
                End If

                If (Not cust.IsCityNull() AndAlso Not String.IsNullOrEmpty(cust.City)) Then
                    body1 = body1.Replace("[City]", cust.City)
                Else
                    body1 = body1.Replace("[City]", "City not found in profile")
                End If

                If (Not cust.IsZipNull() AndAlso Not String.IsNullOrEmpty(cust.Zip)) Then
                    body1 = body1.Replace("[Zip]", cust.Zip)
                Else
                    body1 = body1.Replace("[Zip]", "Zip not found in profile")
                End If

            Else
                body1 = body1.Replace("[Login]", "Customer NOT FOUND")
                body1 = body1.Replace("[Country]", "Customer NOT FOUND")
                body1 = body1.Replace("[Region]", "Customer NOT FOUND")
                body1 = body1.Replace("[City]", "Customer NOT FOUND")
                body1 = body1.Replace("[Zip]", "Customer NOT FOUND")
            End If

        Catch ex As Exception
            body1 = body1.Replace("[Login]", ex.Message)
            body1 = body1.Replace("[Country]", "Exception")
            body1 = body1.Replace("[Region]", "Exception")
            body1 = body1.Replace("[City]", "Exception")
            body1 = body1.Replace("[Zip]", "Exception")
        End Try

        Return body1
    End Function


    Public Shared Function GetMessageBody(Description As String,
                                          Money As Double,
                                          TransactionID As String,
                                          p4 As String,
                                          CreditsPurchased As Integer,
                                          REMOTE_ADDR As String,
                                        HTTP_USER_AGENT As String,
                                        HTTP_REFERER As String,
                                        CustomerID As Integer,
                                        CustomReferrer As String,
                                        PayerEmail As Object,
                                        paymethod As PaymentMethods,
                                        PromoCode As Object,
                                        TransactionTypeID As Integer,
                                        Currency As String)

        Dim body1 As String = <sql><![CDATA[
<table>
    <tr><td align="right"><strong>Login:</strong></td><td>@Login</td></tr>
    <tr><td align="right"><strong>Money:</strong></td><td>@Money</td></tr>
    <tr><td align="right"><strong>Currency:</strong></td><td>[Currency]</td></tr>
    <tr><td align="right"><strong>TransactionID:</strong></td><td>@TransactionID</td></tr>
    <tr><td align="right"><strong>CreditsPurchased:</strong></td><td>@CreditsPurchased</td></tr>
    <tr><td align="right"><strong>CustomerID:</strong></td><td>@CustomerID</td></tr>
    <tr><td align="right"><strong>PayerEmail:</strong></td><td>@PayerEmail</td></tr>
    <tr><td align="right"><strong>paymethod:</strong></td><td>@paymethod (@paymethodCode)</td></tr>
    <tr><td align="right"><strong>PromoCode:</strong></td><td>@PromoCode</td></tr>
    <tr><td align="right"><strong>TransactionTypeID:</strong></td><td>@TransactionTypeID</td></tr>
    <tr><td align="right"><strong>CustomReferrer:</strong></td><td>@CustomReferrer</td></tr>
    <tr><td colspan="2"><hr></td></tr>
    <tr><td align="right"><strong>Country:</strong></td><td>[Country]</td></tr>
    <tr><td align="right"><strong>Region:</strong></td><td>[Region]</td></tr>
    <tr><td align="right"><strong>City:</strong></td><td>[City]</td></tr>
    <tr><td align="right"><strong>Zip:</strong></td><td>[Zip]</td></tr>
    <tr><td colspan="2"><hr></td></tr>
    <tr><td align="right"><strong>Remote Address:</strong></td><td>@REMOTE_ADDR</td></tr>
    <tr><td align="right"><strong>User Agent:</strong></td><td>@HTTP_USER_AGENT</td></tr>
    <tr><td align="right"><strong>HTTP Referer:</strong></td><td>@HTTP_REFERER</td></tr>
    <tr><td colspan="2"><hr></td></tr>
    <tr><td align="right" font="small"><strong>Description:</strong></td><td>@Description</td></tr>

    <!--<tr><td align="right" font="small"><strong>p4:</strong></td><td>@p4</td></tr>-->
</table>
]]></sql>.Value

        body1 = body1.Replace("[Currency]", Currency)
        body1 = body1.Replace("@Description", IIf(Description IsNot Nothing, Description, ""))
        body1 = body1.Replace("@Money", Money)
        body1 = body1.Replace("@TransactionID", IIf(TransactionID IsNot Nothing, TransactionID, ""))
        body1 = body1.Replace("@p4", IIf(p4 IsNot Nothing, p4, ""))
        body1 = body1.Replace("@CreditsPurchased", CreditsPurchased)
        body1 = body1.Replace("@CustomerID", CustomerID)
        body1 = body1.Replace("@CustomReferrer", IIf(CustomReferrer IsNot Nothing, CustomReferrer, ""))
        body1 = body1.Replace("@PayerEmail", IIf(PayerEmail IsNot Nothing, PayerEmail, ""))
        body1 = body1.Replace("@paymethodCode", CType(paymethod, Integer).ToString())
        body1 = body1.Replace("@paymethod", paymethod.ToString())
        body1 = body1.Replace("@PromoCode", IIf(PromoCode IsNot Nothing, PromoCode, ""))
        body1 = body1.Replace("@TransactionTypeID", TransactionTypeID)
        body1 = body1.Replace("@REMOTE_ADDR", IIf(REMOTE_ADDR IsNot Nothing, REMOTE_ADDR, ""))
        body1 = body1.Replace("@HTTP_USER_AGENT", IIf(HTTP_USER_AGENT IsNot Nothing, HTTP_USER_AGENT, ""))
        body1 = body1.Replace("@HTTP_REFERER", IIf(HTTP_REFERER IsNot Nothing, HTTP_REFERER, ""))


        Try
            Dim cust As DSMembers.EUS_ProfilesRow = DataHelpers.GetEUS_Profiles_ByProfileIDRow(CustomerID)
            If (cust IsNot Nothing) Then
                body1 = body1.Replace("@Login", cust.LoginName)

                Try
                    If (Not cust.IsCountryNull() AndAlso Not String.IsNullOrEmpty(cust.Country)) Then
                        body1 = body1.Replace("[Country]", ProfileHelper.GetCountryName(cust.Country))
                    Else
                        body1 = body1.Replace("[Country]", "Country not found in profile")
                    End If
                Catch ex As Exception
                End Try

                If (Not cust.Is_RegionNull() AndAlso Not String.IsNullOrEmpty(cust._Region)) Then
                    body1 = body1.Replace("[Region]", cust._Region)
                Else
                    body1 = body1.Replace("[Region]", "Region not found in profile")
                End If

                If (Not cust.IsCityNull() AndAlso Not String.IsNullOrEmpty(cust.City)) Then
                    body1 = body1.Replace("[City]", cust.City)
                Else
                    body1 = body1.Replace("[City]", "City not found in profile")
                End If

                If (Not cust.IsZipNull() AndAlso Not String.IsNullOrEmpty(cust.Zip)) Then
                    body1 = body1.Replace("[Zip]", cust.Zip)
                Else
                    body1 = body1.Replace("[Zip]", "Zip not found in profile")
                End If

            Else
                body1 = body1.Replace("@Login", "Customer NOT FOUND")
                body1 = body1.Replace("[Country]", "Customer NOT FOUND")
                body1 = body1.Replace("[Region]", "Customer NOT FOUND")
                body1 = body1.Replace("[City]", "Customer NOT FOUND")
                body1 = body1.Replace("[Zip]", "Customer NOT FOUND")
            End If

        Catch ex As Exception
            body1 = body1.Replace("@Login", ex.Message)
            body1 = body1.Replace("[Country]", "Exception")
            body1 = body1.Replace("[Region]", "Exception")
            body1 = body1.Replace("[City]", "Exception")
            body1 = body1.Replace("[Zip]", "Exception")
        End Try

        'Dim body As String = "Description:" & vbCrLf & Description & vbCrLf & vbCrLf & _
        '    "Money:" & vbCrLf & Money & vbCrLf & vbCrLf & _
        '    "TransactionID:" & vbCrLf & TransactionID & vbCrLf & vbCrLf & _
        '    "p4:" & vbCrLf & p4 & vbCrLf & vbCrLf & _
        '    "CreditsPurchased:" & vbCrLf & CreditsPurchased & vbCrLf & vbCrLf & _
        '    "REMOTE_ADDR:" & vbCrLf & REMOTE_ADDR & vbCrLf & vbCrLf & _
        '    "HTTP_USER_AGENT:" & vbCrLf & HTTP_USER_AGENT & vbCrLf & vbCrLf & _
        '    "HTTP_REFERER:" & vbCrLf & HTTP_REFERER & vbCrLf & vbCrLf & _
        '    "CustomerID:" & vbCrLf & CustomerID & vbCrLf & vbCrLf & _
        '    "CustomReferrer:" & vbCrLf & CustomReferrer & vbCrLf & vbCrLf & _
        '    "PayerEmail:" & vbCrLf & PayerEmail & vbCrLf & vbCrLf & _
        '    "paymethod:" & vbCrLf & paymethod & vbCrLf & vbCrLf & _
        '    "PromoCode:" & vbCrLf & PromoCode & vbCrLf & vbCrLf & _
        '    "TransactionTypeID:" & vbCrLf & TransactionTypeID

        Return body1
    End Function


    Public Shared Function GetDurationString(duration As Integer, Optional LAGID As String = "US") As String
        Dim text As String = ""
        Dim o As New clsSiteLAG()

        If (duration = 120) Then
            text = "3 " & o.GetCustomStringFromPage(LAGID, "GlobalStrings", "Months", DataHelpers.ConnectionString)
        ElseIf (duration = 180) Then
            text = "6 " & o.GetCustomStringFromPage(LAGID, "GlobalStrings", "Months", DataHelpers.ConnectionString)
        ElseIf (duration = 270) Then
            text = "9 " & o.GetCustomStringFromPage(LAGID, "GlobalStrings", "Months", DataHelpers.ConnectionString)
        ElseIf (duration = 365) Then
            text = "1 " & o.GetCustomStringFromPage(LAGID, "GlobalStrings", "Year", DataHelpers.ConnectionString)
        ElseIf (duration = 548) Then
            text = "18 " & o.GetCustomStringFromPage(LAGID, "GlobalStrings", "Months", DataHelpers.ConnectionString)
        ElseIf (duration = 730) Then
            text = "2 " & o.GetCustomStringFromPage(LAGID, "GlobalStrings", "Years", DataHelpers.ConnectionString)
        Else
            text = duration & " " & o.GetCustomStringFromPage(LAGID, "GlobalStrings", "Days", DataHelpers.ConnectionString)
        End If

        Return text
    End Function


    'Public Shared Function IsProfileActive(profileId As Integer) As Boolean
    '    Dim isactive As Boolean = DataHelpers.EUS_Profile_IsProfileActive(profileId)
    '    Return isactive
    'End Function




    Public Shared Sub AddFreeRegistrationCredits_WithCheck(profileid As Integer, REMOTE_ADDR As String, HTTP_USER_AGENT As String, HTTP_REFERER As String)
        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)




            Try

                Dim profile = (From itm In _CMSDBDataContext.EUS_Profiles
                               Where
                                   (itm.ProfileID = profileid OrElse itm.MirrorProfileID = profileid) AndAlso
                                   (itm.Status = 4 OrElse itm.Status = 2 OrElse itm.Status = 1) AndAlso
                                   (itm.StartingFreeCredits Is Nothing OrElse itm.StartingFreeCredits = False)
                               Select itm.GenderId, itm.eMail, itm.Country, itm.Region, itm.CustomReferrer, itm.StartingFreeCredits).FirstOrDefault()

                If ((profile Is Nothing) OrElse
                    (Not ProfileHelper.IsMale(profile.GenderId))) Then Exit Sub


                Dim ctr As SYS_CountriesGEO = SYS_CountriesGEOHelper.SYS_CountriesGEO_GetForCountry(_CMSDBDataContext, profile.Country)
                Dim freeCredits As Boolean = clsNullable.NullTo(ctr.MembersBonusCredits) > 0

                If (freeCredits AndAlso Not String.IsNullOrEmpty(ctr.MembersBonusRegionsOrStates)) Then
                    ctr.MembersBonusRegionsOrStates = ctr.MembersBonusRegionsOrStates.ToUpper()
                    Dim _region As String = profile.Region.ToUpper()
                    Dim regs As String() = ctr.MembersBonusRegionsOrStates.Split(vbCrLf)
                    For cnt = 0 To regs.Length - 1
                        regs(cnt) = regs(cnt).Trim()
                    Next
                    Dim reg As String = (From itm In regs
                                         Where itm = _region
                                         Select itm).FirstOrDefault()

                    If (reg IsNot Nothing) Then
                        freeCredits = True
                    Else
                        freeCredits = False
                    End If
                End If


                If (freeCredits) Then

                    ' TransactionTypeID: 4 = Bonus CustomerTransactionTypeID



                    clsCustomer.AddTransaction("Add free registration bonus credits",
                                                clsNullable.NullTo(ctr.MembersBonusCreditsAmount),
                                                "Free registration credits " & ctr.MembersBonusCredits,
                                                "",
                                                ctr.MembersBonusCredits,
                                                REMOTE_ADDR,
                                                HTTP_USER_AGENT,
                                                HTTP_REFERER,
                                                profileid,
                                                profile.CustomReferrer,
                                                profile.eMail,
                                                PaymentMethods.None,
                                                "REGISTRATION_GIFT",
                                                4,
                                                Nothing,
                                                "EUR",
                                                New clsDataRecordIPN(),
                                                0,
                                                False,
                                                45,
                                                0)

                    DataHelpers.EUS_Profiles_Update_StartingFreeCredits(profileid)
                End If

            Catch ex As Exception
                Throw New System.Exception(ex.Message, ex)
            End Try
        End Using
    End Sub

    Public Shared Sub AddFreeRegistrationCredits(ByRef profile As DSMembers.EUS_ProfilesRow, REMOTE_ADDR As String, HTTP_USER_AGENT As String, HTTP_REFERER As String)

        Dim ctr As SYS_CountriesGEO = SYS_CountriesGEOHelper.SYS_CountriesGEO_GetForCountry(profile.Country)
        Dim freeCredits As Boolean = clsNullable.DBNullToInteger(ctr.MembersBonusCredits) > 0

        If (freeCredits AndAlso Not String.IsNullOrEmpty(ctr.MembersBonusRegionsOrStates)) Then
            ctr.MembersBonusRegionsOrStates = ctr.MembersBonusRegionsOrStates.ToUpper()
            Dim _region As String = profile._Region.ToUpper()
            Dim regs As String() = ctr.MembersBonusRegionsOrStates.Split(vbCrLf)
            For cnt = 0 To regs.Length - 1
                regs(cnt) = regs(cnt).Trim()
            Next
            Dim reg As String = (From itm In regs
                                 Where itm = _region
                                 Select itm).FirstOrDefault()

            If (reg IsNot Nothing) Then
                freeCredits = True
            Else
                freeCredits = False
            End If
        End If


        If (freeCredits) Then

            ' TransactionTypeID: 4 = Bonus CustomerTransactionTypeID
            clsCustomer.AddTransaction("Add free registration bonus credits",
                                        clsNullable.NullTo(ctr.MembersBonusCreditsAmount),
                                        "Free registration credits " & ctr.MembersBonusCredits,
                                        "",
                                        ctr.MembersBonusCredits,
                                        REMOTE_ADDR,
                                        HTTP_USER_AGENT,
                                        HTTP_REFERER,
                                        profile.ProfileID,
                                        profile.CustomReferrer,
                                        profile.eMail,
                                        PaymentMethods.None,
                                        "REGISTRATION_GIFT",
                                        4,
                                        Nothing,
                                        "EUR",
                                        New clsDataRecordIPN(),
                                        0,
                                        False,
                                        45,
                                        0)

            DataHelpers.EUS_Profiles_Update_StartingFreeCredits(profile.ProfileID)

            Try
                profile("StartingFreeCredits") = True
            Catch ex As Exception
            End Try
        End If

    End Sub
    Public Shared Sub AddFreeRegistrationCreditsGreek(ByRef profile As DSMembers.EUS_ProfilesRow, REMOTE_ADDR As String, HTTP_USER_AGENT As String, HTTP_REFERER As String)



        Try

            If DataHelpers.GetCountSimilarProfiles(profile.ProfileID) = 0 Then


                Dim ref As String = If(profile.IsCustomReferrerNull, "", profile.CustomReferrer)

                clsCustomer.AddTransaction("Add free registration bonus credits",
                               0,
                                "Free registration credits " & 1000,
                                "",
                               1000,
                                REMOTE_ADDR,
                                HTTP_USER_AGENT,
                                HTTP_REFERER,
                                profile.ProfileID,
                                ref,
                                profile.eMail,
                                PaymentMethods.None,
                                "REGISTRATION_GIFTGreek",
                                4,
                                Nothing,
                                "EUR",
                                New clsDataRecordIPN(),
                                0,
                                False,
                                45,
                                0)

                DataHelpers.EUS_Profiles_Update_StartingFreeCredits(profile.ProfileID)
                profile("StartingFreeCredits") = True
            End If
        Catch ex As Exception
        End Try
        '    End If

    End Sub


    '    Private Shared Function Notify_SentToPaymentWall(postURL As String)

    '        Try

    '            Dim __Subject As String
    '            Dim __Content As String


    '            If (HttpContext.Current.Request.Url.Host <> "localhost") Then
    '                Dim SessionVariables As clsSessionVariables = clsSessionVariables.GetCurrent()
    '                If (SessionVariables.MemberData.Country = "TR") Then
    '                    __Subject = "Loading Payment wall (Country:" & SessionVariables.MemberData.Country & ")"
    '                    __Content = <html><![CDATA[
    'Loading Payment wall (Country:[COUNTRY])
    'LoginName: [LOGINNAME]
    'Redirect to: [REDIR-URL]


    'URL: [URL]
    'IP: [IP]

    ']]></html>
    '                    __Content = __Content.Replace("[REDIR-URL]", postURL)
    '                    __Content = __Content.Replace("[COUNTRY]", SessionVariables.MemberData.Country)
    '                    __Content = __Content.Replace("[LOGINNAME]", SessionVariables.MemberData.LoginName)
    '                    __Content = __Content.Replace("[URL]", HttpContext.Current.Request.Url.ToString())
    '                    __Content = __Content.Replace("[IP]", HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"))



    '                    Dating.Server.Core.DLL.clsMyMail.SendMail(ConfigurationManager.AppSettings("PaymentLogsEmail"), __Subject, __Content, False)
    '                End If
    '            End If

    '        Catch
    '        End Try

    '    End Function

    '    Private Shared Function Notify_SentToEpoch(postURL As String)

    '        Try

    '            Dim __Subject As String
    '            Dim __Content As String


    '            If (HttpContext.Current.Request.Url.Host <> "localhost") Then
    '                Dim SessionVariables As clsSessionVariables = clsSessionVariables.GetCurrent()
    '                If (SessionVariables.MemberData.Country = "TR") Then
    '                    __Subject = "Loading Epoch (Country:" & SessionVariables.MemberData.Country & ")"
    '                    __Content = <html><![CDATA[
    'Loading Epoch (Country:[COUNTRY])
    'LoginName: [LOGINNAME]
    'Redirect to: [REDIR-URL]

    'URL: [URL]
    'IP: [IP]

    ']]></html>
    '                    __Content = __Content.Replace("[REDIR-URL]", postURL)
    '                    __Content = __Content.Replace("[COUNTRY]", SessionVariables.MemberData.Country)
    '                    __Content = __Content.Replace("[LOGINNAME]", SessionVariables.MemberData.LoginName)
    '                    __Content = __Content.Replace("[URL]", HttpContext.Current.Request.Url.ToString())
    '                    __Content = __Content.Replace("[IP]", HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"))



    '                    Dating.Server.Core.DLL.clsMyMail.SendMail(ConfigurationManager.AppSettings("PaymentLogsEmail"), __Subject, __Content, False)
    '                End If
    '            End If

    '        Catch
    '        End Try

    '    End Function

    Private Shared Function GetIPNMetadata(prms As AddTransactionParams) As String
        Dim IPNParams As String = Nothing
        Try

            Dim cDataRecordIPN As clsDataRecordIPN = prms.cDataRecordIPN


            Dim dblBr As String = vbCrLf & vbCrLf

            IPNParams = "" & _
                "Money: " & vbCrLf & cDataRecordIPN.PayProviderAmount & dblBr & _
                "Currency: " & vbCrLf & IIf(Not String.IsNullOrEmpty(cDataRecordIPN.Currency), cDataRecordIPN.Currency, "-") & dblBr & _
                "Credits: " & vbCrLf & cDataRecordIPN.SaleQuantity & dblBr & _
                "TransactionID: " & vbCrLf & IIf(Not String.IsNullOrEmpty(cDataRecordIPN.PayProviderTransactionID), cDataRecordIPN.PayProviderTransactionID, "-") & dblBr & _
                "MemberID: " & vbCrLf & IIf(Not String.IsNullOrEmpty(prms.MemberID), prms.MemberID, "-") & dblBr & _
                "TransactionTypeID: " & vbCrLf & cDataRecordIPN.TransactionTypeID & dblBr & _
                "CustomerID: " & vbCrLf & cDataRecordIPN.CustomerID & dblBr & _
                "CustomerIP: " & vbCrLf & IIf(Not String.IsNullOrEmpty(cDataRecordIPN.custopmerIP), cDataRecordIPN.custopmerIP, "-") & dblBr & _
                "SaleDescription: " & vbCrLf & IIf(Not String.IsNullOrEmpty(cDataRecordIPN.SaleDescription), cDataRecordIPN.SaleDescription, "-") & dblBr & _
                "CustomReferrer: " & vbCrLf & IIf(Not String.IsNullOrEmpty(cDataRecordIPN.CustomReferrer), cDataRecordIPN.CustomReferrer, "-") & dblBr & _
                "SalesSiteID: " & vbCrLf & cDataRecordIPN.SalesSiteID & dblBr & _
                "SalesSiteProductID: " & vbCrLf & cDataRecordIPN.SalesSiteProductID & dblBr & _
                "PayTransactionID: " & vbCrLf & IIf(Not String.IsNullOrEmpty(cDataRecordIPN.PayTransactionID), cDataRecordIPN.PayTransactionID, "-") & dblBr & _
                "PayProviderID: " & vbCrLf & cDataRecordIPN.PayProviderID & dblBr & _
                "ProductCode: " & vbCrLf & IIf(Not String.IsNullOrEmpty(prms.productCode), prms.productCode, "-") & dblBr & _
                "PromoCode: " & vbCrLf & IIf(Not String.IsNullOrEmpty(cDataRecordIPN.PromoCode), cDataRecordIPN.PromoCode, "-") & dblBr

            Try

                Dim UTCTime As DateTime = DateTime.UtcNow
                Dim GTBZone As TimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("GTB Standard Time")
                Dim GTBTime As DateTime = TimeZoneInfo.ConvertTime(UTCTime, TimeZoneInfo.Utc, GTBZone)
                '  Dim timeDiff As TimeSpan = GTBTime - UTCTime

                IPNParams = IPNParams & vbCrLf & _
                    "Payment Started: " & vbCrLf & IIf(Not String.IsNullOrEmpty(cDataRecordIPN.PaymentDateTime), cDataRecordIPN.PaymentDateTime, "-") & dblBr & _
                    "Payment Complete (Greece time): " & vbCrLf & GTBTime.ToString("dd/MM/yyyy HH:mm:ss") & dblBr & _
                    "Payment Complete (UTC time): " & vbCrLf & UTCTime.ToString("dd/MM/yyyy HH:mm:ss") & dblBr
            Catch
            End Try


            If (cDataRecordIPN.BuyerInfo IsNot Nothing) Then
                IPNParams = IPNParams & vbCrLf & _
                    "Buyer Info PayerEmail: " & vbCrLf & IIf(cDataRecordIPN.BuyerInfo.PayerEmail IsNot Nothing, cDataRecordIPN.BuyerInfo.PayerEmail, "-") & dblBr & _
                    "Buyer Info ResidenceCountry: " & vbCrLf & IIf(cDataRecordIPN.BuyerInfo.ResidenceCountry IsNot Nothing, cDataRecordIPN.BuyerInfo.ResidenceCountry, "-") & dblBr & _
                    "Buyer Info Address: " & vbCrLf & IIf(cDataRecordIPN.BuyerInfo.Address IsNot Nothing, cDataRecordIPN.BuyerInfo.Address, "-") & dblBr & _
                    "Buyer Info BusinessName: " & vbCrLf & IIf(cDataRecordIPN.BuyerInfo.BusinessName IsNot Nothing, cDataRecordIPN.BuyerInfo.BusinessName, "-") & dblBr & _
                    "Buyer Info ContactPhone: " & vbCrLf & IIf(cDataRecordIPN.BuyerInfo.ContactPhone IsNot Nothing, cDataRecordIPN.BuyerInfo.ContactPhone, "-") & dblBr & _
                    "Buyer Info FirstName: " & vbCrLf & IIf(cDataRecordIPN.BuyerInfo.FirstName IsNot Nothing, cDataRecordIPN.BuyerInfo.FirstName, "-") & dblBr & _
                    "Buyer Info LastName: " & vbCrLf & IIf(cDataRecordIPN.BuyerInfo.LastName IsNot Nothing, cDataRecordIPN.BuyerInfo.LastName, "-") & dblBr & _
                    "Buyer Info PayerID: " & vbCrLf & IIf(cDataRecordIPN.BuyerInfo.PayerID IsNot Nothing, cDataRecordIPN.BuyerInfo.PayerID, "-") & dblBr & _
                    "Buyer Info PayerStatus: " & vbCrLf & IIf(cDataRecordIPN.BuyerInfo.PayerStatus IsNot Nothing, cDataRecordIPN.BuyerInfo.PayerStatus, "-") & dblBr
            Else
                IPNParams = IPNParams & dblBr & _
                    "Buyer Info not available "
            End If

            If (Len(IPNParams) > 4000) Then IPNParams = IPNParams.Remove(4000)
        Catch
        End Try
        Return IPNParams
    End Function


    Private Shared Function GetProfileInfoParameters(ByRef prof As EUS_Profile) As String
        Dim qsInfo As String = ""
        Try
            qsInfo = qsInfo & "&fn="
            If (Not String.IsNullOrEmpty(prof.FirstName) AndAlso Not String.IsNullOrEmpty(prof.LastName)) Then
                qsInfo = qsInfo & HttpUtility.UrlEncode(prof.FirstName & " " & prof.LastName)
            ElseIf (Not String.IsNullOrEmpty(prof.FirstName)) Then
                qsInfo = qsInfo & HttpUtility.UrlEncode(prof.FirstName)
            ElseIf (Not String.IsNullOrEmpty(prof.LastName)) Then
                qsInfo = qsInfo & HttpUtility.UrlEncode(prof.LastName)
            End If

            qsInfo = qsInfo & "&zip="
            If (Not String.IsNullOrEmpty(prof.Zip)) Then qsInfo = qsInfo & HttpUtility.UrlEncode(prof.Zip)

            qsInfo = qsInfo & "&cit="
            If (Not String.IsNullOrEmpty(prof.City)) Then qsInfo = qsInfo & HttpUtility.UrlEncode(prof.City)

            qsInfo = qsInfo & "&reg="
            If (Not String.IsNullOrEmpty(prof.Region)) Then qsInfo = qsInfo & HttpUtility.UrlEncode(prof.Region)

            qsInfo = qsInfo & "&cntr="
            If (Not String.IsNullOrEmpty(prof.Country)) Then qsInfo = qsInfo & HttpUtility.UrlEncode(prof.Country)

            qsInfo = qsInfo & "&eml="
            If (Not String.IsNullOrEmpty(prof.eMail)) Then qsInfo = qsInfo & HttpUtility.UrlEncode(prof.eMail)

            qsInfo = qsInfo & "&mob="
            If (Not String.IsNullOrEmpty(prof.Cellular)) Then qsInfo = qsInfo & HttpUtility.UrlEncode(prof.Cellular)
        Catch
        End Try
        Return qsInfo
    End Function


    Public Shared Function CreateNewSMSCode(ParametersUsed As String) As String
        Dim code As String = ""
        Dim codeCreated As Boolean = False
        Dim retries As Integer = 10

        While (Not codeCreated)
            If (retries = 0) Then codeCreated = True
            retries = retries - 1

            code = AppUtils.CreateAlphaNumericCode(10)
            Using ctx As New CMSDBDataContext(DataHelpers.ConnectionString)


                Try

                    Dim rec As New EUS_OneTimeCode()
                    rec.Code = code
                    rec.CreatedDate = Date.UtcNow
                    rec.IsRenewed = False

                    If (Not String.IsNullOrEmpty(ParametersUsed)) Then
                        If (ParametersUsed.Length > 250) Then ParametersUsed = ParametersUsed.Remove(249)
                        rec.ParametersUsed = ParametersUsed
                    End If

                    ctx.EUS_OneTimeCodes.InsertOnSubmit(rec)
                    ctx.SubmitChanges()
                    codeCreated = True

                Catch ex As Exception

                End Try
            End Using
        End While

        Return code
    End Function

    Public Shared Function AddTransactionWithSMS(Profile As EUS_Profile, transactionInfo As String, Description As String,
                                              REMOTE_ADDR As String, HTTP_REFERER As String, InputStream As String) As Boolean

        Dim success As Boolean = False
        Using ctx As New CMSDBDataContext(DataHelpers.ConnectionString)



            Try
                Dim priceO As EUS_Price = clsPricing.GetPriceForProductCode_AnyIndex("SMS150Credits")

                Dim payment_provider_value As Double = clsPricing.GetRandomPaymentProviderPercent(Profile.Country)
                Dim payment_provider_ratio As Double = payment_provider_value / 100

                Dim money As Double = priceO.Amount
                '    Dim credits As Double = priceO.Credits
                Dim Currency As String = priceO.Currency


                Dim transaction As New EUS_CustomerTransaction()
                transaction.CreditAmount = 0
                transaction.CustomerID = Profile.ProfileID
                transaction.CustomReferrer = Profile.CustomReferrer
                transaction.Date_Time = DateTime.UtcNow
                transaction.DebitReducePercent = payment_provider_value
                transaction.Description = Description
                transaction.IP = REMOTE_ADDR
                transaction.Referrer = HTTP_REFERER
                transaction.TransactionInfo = transactionInfo
                transaction.TransactionType = 1
                transaction.Notes = ""
                transaction.ReceiptID = ""
                transaction.PaymentMethods = "SMS"


                transaction.DebitAmountOriginal = money
                transaction.DebitProductPrice = money
                transaction.DebitAmount = money - (money * payment_provider_ratio)
                transaction.DebitAmountReceived = money

                transaction.Currency = Currency
                transaction.IPNMetadata = "SMS Payment" & " --- " & InputStream
                transaction.IsSubscription = False
                transaction.MemberID = transactionInfo



                'Dim credit As New EUS_CustomerCredit()
                'credit.Credits = credits
                'credit.CreditstoBeExpired = credits
                'credit.CustomerId = Profile.ProfileID
                'credit.DateTimeCreated = DateTime.UtcNow
                'credit.DateTimeExpiration = DateTime.UtcNow.AddDays(45)
                'credit.ValidForDays = 45
                'credit.EuroAmount = transaction.DebitAmount
                'credit.Currency = Currency
                'credit.IsSubscription = False

                'transaction.EUS_CustomerCredits.Add(credit)
                'ctx.EUS_CustomerTransactions.InsertOnSubmit(transaction)
                'ctx.EUS_CustomerCredits.InsertOnSubmit(credit)


                ctx.SubmitChanges()


                success = True

            Catch ex As Exception
                Throw New System.Exception(ex.Message, ex)

            End Try
        End Using
        Return success
    End Function

    Public Shared Function AddTransactionWithSMS(Profile As vw_EusProfile_Light, transactionInfo As String, Description As String,
                                              REMOTE_ADDR As String, HTTP_REFERER As String, InputStream As String) As Boolean

        Dim success As Boolean = False
        Using ctx As New CMSDBDataContext(DataHelpers.ConnectionString)



            Try
                Dim priceO As EUS_Price = clsPricing.GetPriceForProductCode_AnyIndex("SMS150Credits")

                Dim payment_provider_value As Double = clsPricing.GetRandomPaymentProviderPercent(Profile.Country)
                Dim payment_provider_ratio As Double = payment_provider_value / 100

                Dim money As Double = priceO.Amount
                '     Dim credits As Double = priceO.Credits
                Dim Currency As String = priceO.Currency


                Dim transaction As New EUS_CustomerTransaction()
                transaction.CreditAmount = 0
                transaction.CustomerID = Profile.ProfileID
                transaction.CustomReferrer = Profile.CustomReferrer
                transaction.Date_Time = DateTime.UtcNow
                transaction.DebitReducePercent = payment_provider_value
                transaction.Description = Description
                transaction.IP = REMOTE_ADDR
                transaction.Referrer = HTTP_REFERER
                transaction.TransactionInfo = transactionInfo
                transaction.TransactionType = 1
                transaction.Notes = ""
                transaction.ReceiptID = ""
                transaction.PaymentMethods = "SMS"


                transaction.DebitAmountOriginal = money
                transaction.DebitProductPrice = money
                transaction.DebitAmount = money - (money * payment_provider_ratio)
                transaction.DebitAmountReceived = money

                transaction.Currency = Currency
                transaction.IPNMetadata = "SMS Payment" & " --- " & InputStream
                transaction.IsSubscription = False
                transaction.MemberID = transactionInfo



                'Dim credit As New EUS_CustomerCredit()
                'credit.Credits = credits
                'credit.CreditstoBeExpired = credits
                'credit.CustomerId = Profile.ProfileID
                'credit.DateTimeCreated = DateTime.UtcNow
                'credit.DateTimeExpiration = DateTime.UtcNow.AddDays(45)
                'credit.ValidForDays = 45
                'credit.EuroAmount = transaction.DebitAmount
                'credit.Currency = Currency
                'credit.IsSubscription = False

                'transaction.EUS_CustomerCredits.Add(credit)
                'ctx.EUS_CustomerTransactions.InsertOnSubmit(transaction)
                'ctx.EUS_CustomerCredits.InsertOnSubmit(credit)


                ctx.SubmitChanges()


                success = True

            Catch ex As Exception
                Throw New System.Exception(ex.Message, ex)
            End Try
        End Using
        Return success
    End Function
    Public Shared Function RenewCreditsWithSMS(Profile As EUS_Profile, transactionInfo As String, Description As String,
                                               REMOTE_ADDR As String, HTTP_REFERER As String, InputStream As String) As Boolean

        Dim success As Boolean = False
        Using ctx As New CMSDBDataContext(DataHelpers.ConnectionString)
            Try
                Dim priceO As EUS_Price = clsPricing.GetPriceForProductCode_AnyIndex("SMS150Credits")

                Dim payment_provider_value As Double = clsPricing.GetRandomPaymentProviderPercent(Profile.Country)
                Dim payment_provider_ratio As Double = payment_provider_value / 100

                Dim money As Double = priceO.Amount
                Dim credits As Double = priceO.Credits
                Dim Currency As String = priceO.Currency


                Dim transaction As New EUS_CustomerTransaction()
                transaction.CreditAmount = 0
                transaction.CustomerID = Profile.ProfileID
                transaction.CustomReferrer = Profile.CustomReferrer
                transaction.Date_Time = DateTime.UtcNow
                transaction.DebitReducePercent = payment_provider_value
                transaction.Description = Description
                transaction.IP = REMOTE_ADDR
                transaction.Referrer = HTTP_REFERER
                transaction.TransactionInfo = transactionInfo
                transaction.TransactionType = 1
                transaction.Notes = ""
                transaction.ReceiptID = ""
                transaction.PaymentMethods = "SMS"


                transaction.DebitAmountOriginal = money
                transaction.DebitProductPrice = money
                transaction.DebitAmount = money - (money * payment_provider_ratio)
                transaction.DebitAmountReceived = money

                transaction.Currency = Currency
                transaction.IPNMetadata = "SMS Payment" & " --- " & InputStream
                transaction.IsSubscription = False
                transaction.MemberID = transactionInfo



                Dim credit As New EUS_CustomerCredit()
                credit.Credits = credits
                credit.CreditstoBeExpired = credits
                credit.CustomerId = Profile.ProfileID
                credit.DateTimeCreated = DateTime.UtcNow
                credit.DateTimeExpiration = DateTime.UtcNow.AddDays(45)
                credit.ValidForDays = 45
                credit.EuroAmount = transaction.DebitAmount
                credit.Currency = Currency
                credit.IsSubscription = False

                transaction.EUS_CustomerCredits.Add(credit)
                ctx.EUS_CustomerTransactions.InsertOnSubmit(transaction)
                ctx.EUS_CustomerCredits.InsertOnSubmit(credit)

              

                ctx.SubmitChanges()


                success = True

            Catch ex As Exception
                Throw New System.Exception(ex.Message, ex)

            End Try
        End Using
        Return success
    End Function
    Public Shared Function RenewCreditsWithSMS2(Profile As EUS_Profile, transactionInfo As String, Description As String,
                                               REMOTE_ADDR As String, HTTP_REFERER As String, InputStream As String) As EUS_CustomerTransaction

        Dim success As EUS_CustomerTransaction = Nothing
        Using ctx As New CMSDBDataContext(DataHelpers.ConnectionString)
            Try
                Dim priceO As EUS_Price = clsPricing.GetPriceForProductCode_AnyIndex("SMS150Credits")

                Dim payment_provider_value As Double = clsPricing.GetRandomPaymentProviderPercent(Profile.Country)
                Dim payment_provider_ratio As Double = payment_provider_value / 100

                Dim money As Double = priceO.Amount
                Dim credits As Double = priceO.Credits
                Dim Currency As String = priceO.Currency


                Dim transaction As New EUS_CustomerTransaction()
                transaction.CreditAmount = 0
                transaction.CustomerID = Profile.ProfileID
                transaction.CustomReferrer = Profile.CustomReferrer
                transaction.Date_Time = DateTime.UtcNow
                transaction.DebitReducePercent = payment_provider_value
                transaction.Description = Description
                transaction.IP = REMOTE_ADDR
                transaction.Referrer = HTTP_REFERER
                transaction.TransactionInfo = transactionInfo
                transaction.TransactionType = 1
                transaction.Notes = ""
                transaction.ReceiptID = ""
                transaction.PaymentMethods = "SMS"


                transaction.DebitAmountOriginal = money
                transaction.DebitProductPrice = money
                transaction.DebitAmount = money - (money * payment_provider_ratio)
                transaction.DebitAmountReceived = money

                transaction.Currency = Currency
                transaction.IPNMetadata = "SMS Payment" & " --- " & InputStream
                transaction.IsSubscription = False
                transaction.MemberID = transactionInfo
               

                Dim credit As New EUS_CustomerCredit()
                credit.Credits = credits
                credit.CreditstoBeExpired = credits
                credit.CustomerId = Profile.ProfileID
                credit.DateTimeCreated = DateTime.UtcNow
                credit.DateTimeExpiration = DateTime.UtcNow.AddDays(45)
                credit.ValidForDays = 45
                credit.EuroAmount = transaction.DebitAmount
                credit.Currency = Currency
                credit.IsSubscription = False

                transaction.EUS_CustomerCredits.Add(credit)
                ctx.EUS_CustomerTransactions.InsertOnSubmit(transaction)
                ctx.EUS_CustomerCredits.InsertOnSubmit(credit)





                ctx.SubmitChanges()


                success = transaction

            Catch ex As Exception
                Throw New System.Exception(ex.Message, ex)

            End Try
        End Using
        Return success
    End Function
    Public Shared Function RenewCreditsWithSMS3(Profile As EUS_Profile, transactionInfo As String, Description As String,
                                             REMOTE_ADDR As String, HTTP_REFERER As String, InputStream As String) As EUS_CustomerTransaction

        Dim success As EUS_CustomerTransaction = Nothing
        Using ctx As New CMSDBDataContext(DataHelpers.ConnectionString)
            Try
                Dim priceO As EUS_Price = clsPricing.GetPriceForProductCode_AnyIndex("SMS200Credits")

                Dim payment_provider_value As Double = clsPricing.GetRandomPaymentProviderPercent(Profile.Country)
                Dim payment_provider_ratio As Double = payment_provider_value / 100

                Dim money As Double = priceO.Amount
                Dim credits As Double = priceO.Credits
                Dim Currency As String = priceO.Currency


                Dim transaction As New EUS_CustomerTransaction()
                transaction.CreditAmount = 0
                transaction.CustomerID = Profile.ProfileID
                transaction.CustomReferrer = Profile.CustomReferrer
                transaction.Date_Time = DateTime.UtcNow
                transaction.DebitReducePercent = payment_provider_value
                transaction.Description = Description
                transaction.IP = REMOTE_ADDR
                transaction.Referrer = HTTP_REFERER
                transaction.TransactionInfo = transactionInfo
                transaction.TransactionType = 1
                transaction.Notes = ""
                transaction.ReceiptID = ""
                transaction.PaymentMethods = "SMS2"


                transaction.DebitAmountOriginal = money
                transaction.DebitProductPrice = money
                transaction.DebitAmount = money - (money * payment_provider_ratio)
                transaction.DebitAmountReceived = money

                transaction.Currency = Currency
                transaction.IPNMetadata = "SMS Payment" & " --- " & InputStream
                transaction.IsSubscription = False
                transaction.MemberID = transactionInfo


                Dim credit As New EUS_CustomerCredit()
                credit.Credits = credits
                credit.CreditstoBeExpired = credits
                credit.CustomerId = Profile.ProfileID
                credit.DateTimeCreated = DateTime.UtcNow
                credit.DateTimeExpiration = DateTime.UtcNow.AddDays(45)
                credit.ValidForDays = 45
                credit.EuroAmount = transaction.DebitAmount
                credit.Currency = Currency
                credit.IsSubscription = False

                transaction.EUS_CustomerCredits.Add(credit)
                ctx.EUS_CustomerTransactions.InsertOnSubmit(transaction)
                ctx.EUS_CustomerCredits.InsertOnSubmit(credit)





                ctx.SubmitChanges()


                success = transaction

            Catch ex As Exception
                Throw New System.Exception(ex.Message, ex)

            End Try
        End Using
        Return success
    End Function

    Public Shared Function RenewCreditsWithSMS(Profile As vw_EusProfile_Light, transactionInfo As String, Description As String,
                                               REMOTE_ADDR As String, HTTP_REFERER As String, InputStream As String) As Boolean
        Dim success As Boolean = False
        Using ctx As New CMSDBDataContext(DataHelpers.ConnectionString)



            Try
                Dim priceO As EUS_Price = clsPricing.GetPriceForProductCode_AnyIndex("SMS150Credits")

                Dim payment_provider_value As Double = clsPricing.GetRandomPaymentProviderPercent(Profile.Country)
                Dim payment_provider_ratio As Double = payment_provider_value / 100

                Dim money As Double = priceO.Amount
                Dim credits As Double = priceO.Credits
                Dim Currency As String = priceO.Currency


                Dim transaction As New EUS_CustomerTransaction()
                transaction.CreditAmount = 0
                transaction.CustomerID = Profile.ProfileID
                transaction.CustomReferrer = Profile.CustomReferrer
                transaction.Date_Time = DateTime.UtcNow
                transaction.DebitReducePercent = payment_provider_value
                transaction.Description = Description
                transaction.IP = REMOTE_ADDR
                transaction.Referrer = HTTP_REFERER
                transaction.TransactionInfo = transactionInfo
                transaction.TransactionType = 1
                transaction.Notes = ""
                transaction.ReceiptID = ""
                transaction.PaymentMethods = "SMS"


                transaction.DebitAmountOriginal = money
                transaction.DebitProductPrice = money
                transaction.DebitAmount = money - (money * payment_provider_ratio)
                transaction.DebitAmountReceived = money

                transaction.Currency = Currency
                transaction.IPNMetadata = "SMS Payment" & " --- " & InputStream
                transaction.IsSubscription = False
                transaction.MemberID = transactionInfo



                Dim credit As New EUS_CustomerCredit()
                credit.Credits = credits
                credit.CreditstoBeExpired = credits
                credit.CustomerId = Profile.ProfileID
                credit.DateTimeCreated = DateTime.UtcNow
                credit.DateTimeExpiration = DateTime.UtcNow.AddDays(45)
                credit.ValidForDays = 45
                credit.EuroAmount = transaction.DebitAmount
                credit.Currency = Currency
                credit.IsSubscription = False

                transaction.EUS_CustomerCredits.Add(credit)
                ctx.EUS_CustomerTransactions.InsertOnSubmit(transaction)
                ctx.EUS_CustomerCredits.InsertOnSubmit(credit)


                ctx.SubmitChanges()


                success = True

            Catch ex As Exception
                Throw New System.Exception(ex.Message, ex)
            End Try
        End Using
        Return success
    End Function
    Public Shared Function RenewCreditsWithSMS4(Profile As vw_EusProfile_Light, transactionInfo As String, Description As String,
                                               REMOTE_ADDR As String, HTTP_REFERER As String, InputStream As String) As Boolean
        Dim success As Boolean = False
        Using ctx As New CMSDBDataContext(DataHelpers.ConnectionString)



            Try
                Dim priceO As EUS_Price = clsPricing.GetPriceForProductCode_AnyIndex("SMS200Credits")

                Dim payment_provider_value As Double = clsPricing.GetRandomPaymentProviderPercent(Profile.Country)
                Dim payment_provider_ratio As Double = payment_provider_value / 100

                Dim money As Double = priceO.Amount
                Dim credits As Double = priceO.Credits
                Dim Currency As String = priceO.Currency


                Dim transaction As New EUS_CustomerTransaction()
                transaction.CreditAmount = 0
                transaction.CustomerID = Profile.ProfileID
                transaction.CustomReferrer = Profile.CustomReferrer
                transaction.Date_Time = DateTime.UtcNow
                transaction.DebitReducePercent = payment_provider_value
                transaction.Description = Description
                transaction.IP = REMOTE_ADDR
                transaction.Referrer = HTTP_REFERER
                transaction.TransactionInfo = transactionInfo
                transaction.TransactionType = 1
                transaction.Notes = ""
                transaction.ReceiptID = ""
                transaction.PaymentMethods = "SMS2"


                transaction.DebitAmountOriginal = money
                transaction.DebitProductPrice = money
                transaction.DebitAmount = money - (money * payment_provider_ratio)
                transaction.DebitAmountReceived = money

                transaction.Currency = Currency
                transaction.IPNMetadata = "SMS Payment" & " --- " & InputStream
                transaction.IsSubscription = False
                transaction.MemberID = transactionInfo



                Dim credit As New EUS_CustomerCredit()
                credit.Credits = credits
                credit.CreditstoBeExpired = credits
                credit.CustomerId = Profile.ProfileID
                credit.DateTimeCreated = DateTime.UtcNow
                credit.DateTimeExpiration = DateTime.UtcNow.AddDays(45)
                credit.ValidForDays = 45
                credit.EuroAmount = transaction.DebitAmount
                credit.Currency = Currency
                credit.IsSubscription = False

                transaction.EUS_CustomerCredits.Add(credit)
                ctx.EUS_CustomerTransactions.InsertOnSubmit(transaction)
                ctx.EUS_CustomerCredits.InsertOnSubmit(credit)


                ctx.SubmitChanges()


                success = True

            Catch ex As Exception
                Throw New System.Exception(ex.Message, ex)
            End Try
        End Using
        Return success
    End Function
End Class

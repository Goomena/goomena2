﻿Imports Dating.Server.Datasets.DLL
Imports System.Data.SqlClient
Imports Dating.Server.Datasets.DLL.DSMembers

Public Class AdminActions


    Public Shared Function ApproveProfile(ByRef members As DSMembers, Optional IsAutoApproved As Boolean = False) As clsApprovedProfileResult
        Dim result As New clsApprovedProfileResult()
        Try

            If (members.EUS_Profiles.Rows.Count = 1) Then
                result.IsNewProfile = True
                result.IsSuccessful = ApproveNewProfile(members, IsAutoApproved)
            Else
                result.IsSuccessful = ApproveExistingProfile(members, IsAutoApproved)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        End Try
        Return result
    End Function

    Public Shared Function ApproveProfile(loginName As String, Optional IsAutoApproved As Boolean = False) As clsApprovedProfileResult
        Dim result As New clsApprovedProfileResult()
        Try
            Using members As DSMembers = DataHelpers.GetEUS_Profiles_ByLoginName(loginName)
                If (members.EUS_Profiles.Rows.Count = 1) Then
                    result.IsNewProfile = True
                    result.IsSuccessful = ApproveNewProfile(members, IsAutoApproved)
                Else
                    result.IsSuccessful = ApproveExistingProfile(members, IsAutoApproved)
                End If
            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        End Try
        Return result
    End Function

    Public Shared Function ApproveProfile(profileID As Integer, Optional IsAutoApproved As Boolean = False) As clsApprovedProfileResult
        Dim result As New clsApprovedProfileResult()
        Try
            Using members As DSMembers = DataHelpers.GetEUS_Profiles_ByProfileOrMirrorID(profileID)
                If (members.EUS_Profiles.Rows.Count = 1) Then
                    result.IsNewProfile = True
                    result.IsSuccessful = ApproveNewProfile(members, IsAutoApproved)
                Else
                    result.IsSuccessful = ApproveExistingProfile(members, IsAutoApproved)
                End If
            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        End Try
        Return result
    End Function

    Public Shared Function ApproveNewProfile(members As DSMembers, Optional IsAutoApproved As Boolean = False) As Boolean
        Dim success As Boolean
        Try

            Dim masterRow As DSMembers.EUS_ProfilesRow = members.EUS_Profiles.Rows(0)

            Dim sql As String = "exec [EUS_Profiles_ApproveNewProfile] @CustomerId=@CustomerId, @IsAutoApproved=@IsAutoApproved"
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd As SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@CustomerId", masterRow.ProfileID)
                    cmd.Parameters.AddWithValue("@IsAutoApproved", IsAutoApproved)
                    DataHelpers.ExecuteNonQuery(cmd)
                End Using
            End Using
            success = True

        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        End Try
        Return success
    End Function

    Public Shared Function ApproveExistingProfile(members As DSMembers, Optional IsAutoApproved As Boolean = False) As Boolean
        Dim success As Boolean
        Try


            Dim mirrorRow As DSMembers.EUS_ProfilesRow = Nothing
            Dim masterRow As DSMembers.EUS_ProfilesRow = Nothing
            Dim mirrorRowOld As DSMembers.EUS_ProfilesRow = Nothing
            Dim masterRowOld As DSMembers.EUS_ProfilesRow = Nothing

            Dim _drs As EUS_ProfilesRow() = members.EUS_Profiles.Select("IsMaster=0")
            If (_drs.Length > 0) Then
                mirrorRow = _drs(0)
            End If
            _drs = members.EUS_Profiles.Select("IsMaster=1")
            If (_drs.Length > 0) Then
                masterRow = _drs(0)
            End If

            Dim clearUserMessage As Boolean = False
            Dim isvalid = True

            If (masterRow Is Nothing OrElse mirrorRow Is Nothing) Then
                isvalid = False
                Throw New System.Exception("Invalid profile data: " & vbCrLf & vbTab & "the mirror and master is same table record." & vbCrLf & vbTab & "Cannot approve.")
            End If

            If (isvalid) Then

                Dim dsOld As DSMembers = members.Copy()
                _drs = dsOld.EUS_Profiles.Select("IsMaster=0")
                If (_drs.Length > 0) Then
                    mirrorRowOld = _drs(0)
                End If
                _drs = dsOld.EUS_Profiles.Select("IsMaster=1")
                If (_drs.Length > 0) Then
                    masterRowOld = _drs(0)
                End If


                If (masterRow.Status = Dating.Server.Core.DLL.ProfileStatusEnum.LIMITED) Then
                    For Each dr As EUS_ProfilesRow In members.EUS_Profiles.Rows
                        dr.Status = Dating.Server.Core.DLL.ProfileStatusEnum.Approved
                        dr.LimitedOnDate = DateTime.UtcNow
                    Next
                    clearUserMessage = True
                End If

                For Each dr As EUS_ProfilesRow In members.EUS_Profiles.Rows
                    dr.UpdatingDocs = False
                    dr.UpdatingPhotos = False
                    dr.IsMobileUpdate = False
                Next

                ''''''''''''''''''''''''''''''''''''''''''''
                ' copy data from mirror record to master, overwrite old user data with new
                ''''''''''''''''''''''''''''''''''''''''''''

                CopyEUS_ProfilesRowsData(members.EUS_Profiles, masterRow, mirrorRow)
                '' copy data that is not read only
                'Dim cnt As Integer = 0
                'For cnt = 0 To members.EUS_Profiles.Columns.Count - 1
                '    If (Not members.EUS_Profiles.Columns(cnt).ReadOnly) Then
                '        masterRow(cnt) = mirrorRow(cnt)
                '    End If
                'Next


                mirrorRow.Status = ProfileStatusEnum.Approved
                masterRow.Status = ProfileStatusEnum.Approved

                mirrorRow.IsAutoApproved = IsAutoApproved
                masterRow.IsAutoApproved = IsAutoApproved

                If (masterRow IsNot Nothing) Then
                    DataHelpers.UpdateEUS_Profiles_ModifiedFields(masterRow, masterRowOld)
                End If
                DataHelpers.UpdateEUS_Profiles_ModifiedFields(mirrorRow, mirrorRowOld)

                '' update db
                'PergormReLogin()
                'CheckWebServiceCallResult(gAdminWS.UpdateEUS_Profiles(gLoginRec.Token, Me.DSMembers))
                '' send email to user
                '_SendEmailMessage(mirrorRow.eMail, "Your changes to " & My.MySettings.Default.SiteNameForCustomers & " profile are accepted.", "Your changes to " & My.MySettings.Default.SiteNameForCustomers & " profile are accepted.")
                ''DataHelpers.UpdateEUS_Profiles(members)
                'clsMyMail.SendMail(masterRow.eMail, "Your changes to Goomena.com profile are accepted.", "Your changes to Goomena.com profile are accepted.")

                success = True
            End If

            If (isvalid AndAlso clearUserMessage) Then
                ' update db
                DataHelpers.EUS_UserMessages_ClearByPosition(masterRow.ProfileID,
                                                             Position:="Info.Limited_Access")
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        End Try
        Return success
    End Function



    Public Shared Function RejectProfile(profileID As Integer) As clsRejectedProfileResult
        Dim result As New clsRejectedProfileResult()
        Try
            Using members As DSMembers = DataHelpers.GetEUS_Profiles_ByProfileOrMirrorID(profileID)
                If (members.EUS_Profiles.Rows.Count = 1) Then
                    result.IsNewProfile = True
                    result.IsSuccessful = RejectNewProfile(members)
                Else
                    result.IsSuccessful = RejectExistingProfile(members)
                End If
            End Using
          
        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        End Try
        Return result
    End Function


    Public Shared Function RejectProfile(ByRef members As DSMembers) As clsRejectedProfileResult
        Dim result As New clsRejectedProfileResult()
        Try
            If (members.EUS_Profiles.Rows.Count = 1) Then
                result.IsNewProfile = True
                result.IsSuccessful = RejectNewProfile(members)
            Else
                result.IsSuccessful = RejectExistingProfile(members)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        End Try
        Return result
    End Function


    Public Shared Function RejectProfile(loginName As String) As clsRejectedProfileResult
        Dim result As New clsRejectedProfileResult()
        Try
            Using members As DSMembers = DataHelpers.GetEUS_Profiles_ByLoginName(loginName)
                If (members.EUS_Profiles.Rows.Count = 1) Then
                    result.IsNewProfile = True
                    result = RejectNewProfile(members)
                Else
                    result = RejectExistingProfile(members)
                End If
            End Using

          
        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        End Try
        Return result
    End Function


    Public Shared Function RejectNewProfile(members As DSMembers)
        Dim success As Boolean
        Try

            Dim masterRow As DSMembers.EUS_ProfilesRow = members.EUS_Profiles.Rows(0)
            masterRow.Status = ProfileStatusEnum.Rejected

            DataHelpers.UpdateEUS_Profiles(members)

            success = True
        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        End Try
        Return success
    End Function


    Public Shared Function RejectExistingProfile(members As DSMembers)
        Dim success As Boolean
        Try

            Dim masterRow As DSMembers.EUS_ProfilesRow = members.EUS_Profiles.Rows(0)
            Dim mirrorRow As DSMembers.EUS_ProfilesRow = members.EUS_Profiles.Rows(1)

            If (masterRow.IsMaster = False) Then
                masterRow = members.EUS_Profiles.Rows(1)
                mirrorRow = members.EUS_Profiles.Rows(0)
            End If

            Dim isvalid = True
            If (masterRow.ProfileID = mirrorRow.ProfileID) Then
                isvalid = False
                Throw New System.Exception("Invalid profile data: " & vbCrLf & vbTab & "the mirror and master is same table record." & vbCrLf & vbTab & "Cannot approve.")
            End If

            If (isvalid) Then
                ''''''''''''''''''''''''''''''''''''''''''''
                ' copy data from master record to mirror, overwrite user changes
                ''''''''''''''''''''''''''''''''''''''''''''

                '' copy data that is not read only
                'Dim cnt As Integer = 0
                'For cnt = 0 To members.EUS_Profiles.Columns.Count - 1
                '    If (Not members.EUS_Profiles.Columns(cnt).ReadOnly) Then
                '        mirrorRow(cnt) = masterRow(cnt)
                '    End If
                'Next

                'mirrorRow.IsMaster = False
                'masterRow.IsMaster = True

                'mirrorRow.MirrorProfileID = masterRow.ProfileID
                'masterRow.MirrorProfileID = mirrorRow.ProfileID

                CopyEUS_ProfilesRowsData(members.EUS_Profiles, mirrorRow, masterRow)

                ' update db
                DataHelpers.UpdateEUS_Profiles(members)

                success = True
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        End Try
        Return success
    End Function



    Public Shared Function ApprovePhoto(photoId As Long,
                                IsUser As Boolean,
                                IsAdmin As Boolean, Optional IsAutoApproved As Boolean = False) As Boolean
        Dim IsDefaultPhotoSet As Long
        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
            Dim photo As EUS_CustomerPhoto = Nothing
            Try

                photo = (From itm In _CMSDBDataContext.EUS_CustomerPhotos
                        Where itm.CustomerPhotosID = photoId
                        Select itm).FirstOrDefault()


                If (photo IsNot Nothing) Then
                    photo.HasAproved = True
                    photo.HasDeclined = False
                    photo.IsAutoApproved = IsAutoApproved

                    _CMSDBDataContext.SubmitChanges()


                    IsDefaultPhotoSet = DataHelpers.EUS_CustomerPhotos_SetDefault(photo.CustomerID, Nothing, IsUser, IsAdmin)
                    If (IsDefaultPhotoSet > 0) Then
                        clsCustomer.AddFreeRegistrationCredits_WithCheck(photo.CustomerID, "", "", "")
                    End If
                End If

            Catch ex As Exception
                Throw New Exception(ex.Message, ex)
            Finally
                _CMSDBDataContext.Dispose()
            End Try

            If (photo Is Nothing) Then Throw New PhotoNotFoundException(photoId)
        End Using
        Return True
    End Function


    Public Shared Function RejectPhoto(photoId As Long) As Boolean
        '   Dim IsDefaultPhotoSet As Long
        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Dim photo As EUS_CustomerPhoto = Nothing
            Try


                photo = (From itm In _CMSDBDataContext.EUS_CustomerPhotos
                                                  Where itm.CustomerPhotosID = photoId
                                                  Select itm).FirstOrDefault()

                If (photo IsNot Nothing) Then
                    photo.HasAproved = False
                    photo.HasDeclined = True
                    photo.IsDefault = False
                    photo.IsAutoApproved = False

                    _CMSDBDataContext.SubmitChanges()

                    '  IsDefaultPhotoSet = DataHelpers.EUS_CustomerPhotos_SetDefault(photo.CustomerID, Nothing, False, False)
                    'If (IsDefaultPhotoSet > 0) Then
                    '    clsCustomer.AddFreeRegistrationCredits_WithCheck(photo.CustomerID, "", "", "")
                    'End If
                End If

            Catch ex As Exception
                Throw New Exception(ex.Message, ex)
            End Try
            If (photo Is Nothing) Then Throw New PhotoNotFoundException(photoId)
        End Using
        Return True
    End Function


    Public Shared Function SetDefaultEUS_CustomerPhotos(PhotoID As Long, CustomerID As Integer) As clsSetDefaultPhotoResult
        Dim result As New clsSetDefaultPhotoResult()
        If (PhotoID > 0) Then
            PhotoID = DataHelpers.EUS_CustomerPhotos_SetDefault(CustomerID, PhotoID, False, True)
        Else
            PhotoID = DataHelpers.EUS_CustomerPhotos_SetDefault(CustomerID, Nothing, False, True)
        End If

        If (PhotoID > 0) Then
            clsCustomer.AddFreeRegistrationCredits_WithCheck(CustomerID, "", "", "")
            result.IsSuccessful = True
        Else
            result.IsSuccessful = False
        End If

        result.PhotoID = PhotoID
        Return result
    End Function

    Public Shared Function DeleteAllCustomerPhotos(customerId As Integer) As clsDataRecordErrorsReturn
        Dim ReturnErrors As New clsDataRecordErrorsReturn

        Using ds As DSMembers = DataHelpers.GetEUS_CustomerPhotos_ByProfileOrMirrorID(customerId)


            If (ds.EUS_CustomerPhotos.Rows.Count > 0) Then customerId = ds.EUS_CustomerPhotos.Rows(0).Field(Of Integer)("CustomerID")

            Dim config As New clsConfigValues()

            '  Dim site_name As String = config.site_name
            Dim image_thumb_server_path As String = config.image_thumb_server_path
            Dim image_server_path As String = config.image_server_path
            Dim image_d150_server_path As String = config.image_d150_server_path
            Dim image_d350_server_path As String = config.image_d350_server_path

            Dim cnt As Integer = 0

            For cnt = 0 To ds.EUS_CustomerPhotos.Rows.Count - 1
                Dim row As DSMembers.EUS_CustomerPhotosRow = ds.EUS_CustomerPhotos.Rows(cnt)
               dim hdl As New Core.DLL.clsFilesDeletionHandlers()



                    Try

                        Dim fileName As String = row.FileName
                        ds.EUS_CustomerPhotos.Rows(cnt).Delete()

                        Dim path As String = String.Format(image_server_path, customerId, fileName)
                        hdl.AddFile(path)

                        Dim thumbPath As String = String.Format(image_thumb_server_path, customerId, fileName)
                        hdl.AddFile(thumbPath)

                        Dim d150Path As String = String.Format(image_d150_server_path, customerId, fileName)
                        hdl.AddFile(d150Path)

                        Dim d350Path As String = String.Format(image_d350_server_path, customerId, fileName)
                        hdl.AddFile(d350Path)

                        ReturnErrors.Message = ReturnErrors.Message & vbCrLf & path
                        ReturnErrors.Message = ReturnErrors.Message & vbCrLf & thumbPath

                    Catch ex As Exception
                        ReturnErrors.Message = ReturnErrors.Message & vbCrLf & ex.Message
                    End Try
                '   End Using
            Next

            'Try
            '    System.IO.Directory.Delete(pathCustomer, True)
            'Catch ex As Exception
            'End Try


            DataHelpers.UpdateEUS_CustomerPhotos(ds)
        End Using
        Return ReturnErrors
    End Function



    Public Shared Function DeletePhoto(customerId As Integer, photoId As Long) As clsDataRecordErrorsReturn
        Dim ReturnErrors As New clsDataRecordErrorsReturn
        Dim hdl As New Core.DLL.clsFilesDeletionHandlers()


        Try

            Using ds As DSMembers = DataHelpers.GetEUS_CustomerPhotosByCustomerPhotosID(photoId)


                Dim dr As DSMembers.EUS_CustomerPhotosRow = ds.EUS_CustomerPhotos.Rows(0)
                Dim fileName As String = dr("FileName")
                Dim isDefault As Boolean = False
                If (Not dr.IsNull("IsDefault")) Then
                    isDefault = dr("IsDefault")
                End If

                ds.EUS_CustomerPhotos.Rows(0).Delete()
                DataHelpers.UpdateEUS_CustomerPhotos(ds)

                Dim config As New clsConfigValues()
                '  Dim site_name As String = config.site_name
                Dim image_thumb_server_path As String = config.image_thumb_server_path
                Dim image_server_path As String = config.image_server_path
                Dim image_d150_server_path As String = config.image_d150_server_path
                Dim image_d350_server_path As String = config.image_d350_server_path


                Dim path As String = String.Format(image_server_path, customerId, fileName)
                hdl.AddFile(path)

                Dim thumbPath As String = String.Format(image_thumb_server_path, customerId, fileName)
                hdl.AddFile(thumbPath)

                Dim d150Path As String = String.Format(image_d150_server_path, customerId, fileName)
                hdl.AddFile(d150Path)

                Dim d350Path As String = String.Format(image_d350_server_path, customerId, fileName)
                hdl.AddFile(d350Path)

                ReturnErrors.Message = ReturnErrors.Message & vbCrLf & path
                ReturnErrors.Message = ReturnErrors.Message & vbCrLf & thumbPath
                If (isDefault) Then

                    ' set next approved public photo as default
                    Using _ds1 As DSMembers = DataHelpers.GetEUS_CustomerPhotos_ByProfileOrMirrorID(customerId)


                        Dim foundRows As DSMembers.EUS_CustomerPhotosRow() = _ds1.EUS_CustomerPhotos.Select("DisplayLevel=0")
                        For Each row As DSMembers.EUS_CustomerPhotosRow In foundRows
                            If (row.HasAproved AndAlso Not row.HasDeclined) Then
                                row.IsDefault = True
                                DataHelpers.UpdateEUS_CustomerPhotos(_ds1)
                                Exit For
                            End If
                        Next
                    End Using
                End If

            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        End Try
        '   End Using
        Return ReturnErrors
    End Function



End Class

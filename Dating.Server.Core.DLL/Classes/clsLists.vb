﻿Imports Dating.Server.Datasets.DLL


Public Class Lists

    '' Lazy loading
    Private Shared _gDSLists As DSLists
    Public Shared ReadOnly Property gDSLists As DSLists
        Get
            If (_gDSLists Is Nothing) Then _gDSLists = DataHelpers.GetEUS_LISTS()
            Return _gDSLists
        End Get
    End Property


    Private Shared _gEUS_LISTS_PhotosDisplayLevel As DSLists.EUS_LISTS_PhotosDisplayLevelDataTable
    Public Shared ReadOnly Property gEUS_LISTS_PhotosDisplayLevel As DSLists.EUS_LISTS_PhotosDisplayLevelDataTable
        Get
            If (_gEUS_LISTS_PhotosDisplayLevel Is Nothing OrElse _gEUS_LISTS_PhotosDisplayLevel.Rows.Count = 0) Then
                _gEUS_LISTS_PhotosDisplayLevel = DataHelpers.GetEUS_LISTS_PhotosDisplayLevel().EUS_LISTS_PhotosDisplayLevel
            End If
            Return _gEUS_LISTS_PhotosDisplayLevel
        End Get
    End Property


    'Private Shared _gDSListsGEO__IsEnabled_PrintableNameASC As DSGEO
    'Public Shared ReadOnly Property gDSListsGEO__IsEnabled_PrintableNameASC As DSGEO
    '    Get
    '        If (_gDSListsGEO__IsEnabled_PrintableNameASC Is Nothing) Then _gDSListsGEO__IsEnabled_PrintableNameASC = DataHelpers.GetSYS_CountriesGEO__IsEnabled_PrintableNameASC()
    '        Return _gDSListsGEO__IsEnabled_PrintableNameASC
    '    End Get
    'End Property


    Public Shared Sub ClearCache()
        If (_gDSLists IsNot Nothing) Then
            _gDSLists.Dispose()
            _gDSLists = Nothing
        End If
        If (_gEUS_LISTS_PhotosDisplayLevel IsNot Nothing) Then
            _gEUS_LISTS_PhotosDisplayLevel.Dispose()
            _gEUS_LISTS_PhotosDisplayLevel = Nothing
        End If
    End Sub


End Class

﻿Imports System.Configuration
Imports System.Web
Imports System.Data
Imports System.Data.Sql
Imports System.Data.SqlClient

Imports Library.Public

Public Class clsSiteLAG

    'Public Class clsPageCustomsReturn
    '    Public UniqueKey As String
    '    Public Message As String
    'End Class

    Public Class clsPageBasicReturn
        Public PageTitle As String
        Public MetaDesciption As String
        Public MetaKeywords As String
        Public BodyHTM As String
        Public SitePageID As Integer
        Public MasterPage_FileName As String
        Public MasterPage_DisplayName As String
        Public LastAccess As DateTime = Date.UtcNow

        Public ReadOnly Property BodyHTMSize As Integer
            Get
                If (BodyHTM Is Nothing) Then Return 0
                Return BodyHTM.Length
            End Get
        End Property


    End Class

    Dim m_ConnectionString As String
    Public ReadOnly Property ConnectionString As String
        Get
            Return m_ConnectionString
        End Get
    End Property


    Public Function Init(CMSconnectionString As String) As Boolean
        m_ConnectionString = CMSconnectionString
        Return True
    End Function


    Public Function Init(ByVal ServerName As String, ByVal Port As Integer, ByVal Database As String, ByVal UseNTSecurity As Boolean, ByVal Login As String, ByVal Password As String) As Boolean
        Try
            Dim sd As New Library.Public.SQLConnectionStringData


            sd.DB_Connect_Timeout = 120
            sd.DB_ServerName = ServerName
            sd.DB_SQLType = Library.Public.ProviderType.MsSQL
            sd.DB_DataBase = Database
            sd.DB_NTSecurity = UseNTSecurity
            sd.DB_Login = Login
            sd.DB_Password = Password
            m_ConnectionString = Library.Public.SQLFunctions.GetNETSQLConnectString(sd)
        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
            'gER.ErrorMsgBox(ex, "")
            Return False
        End Try

        Return True
    End Function

    Public Function GetPageBasics(ByVal LagID As String, ByVal SitePageID As Long) As clsPageBasicReturn
        Dim cPageBasicReturn As New clsPageBasicReturn
        Try

            'Dim szSQL As String = "SELECT * FROM SYS_SitePages WHERE SitePageID = " + SitePageID.ToString()
            Dim szSQL As String = <sql><![CDATA[
SELECT 
    SYS_SitePages.*,
    MasterDisplayName=SYS_SiteMasterPages.[DisplayName],
    MasterFileName=SYS_SiteMasterPages.[FileName]
FROM SYS_SitePages 
left join [SYS_SiteMasterPages] on SYS_SitePages.SYS_SiteMasterPageID=SYS_SiteMasterPages.SYS_SiteMasterPageID
WHERE SitePageID = @SitePageID
 ]]></sql>
            '  szSQL = szSQL.Replace("@SitePageID", SitePageID.ToString())
            '   Dim dtPage As DataTable = Nothing
            Using con As New SqlConnection(m_ConnectionString)
                Using cmd As SqlCommand = New SqlCommand(szSQL, con)
                    cmd.Parameters.AddWithValue("@SitePageID", SitePageID)
                    Using dtPage = DataHelpers.GetDataTable(cmd)
                        Try

                            For cnt = 0 To dtPage.Rows.Count - 1
                                ' while rsRead.Read()

                                Dim dr As DataRow = dtPage.Rows(cnt)
                                cPageBasicReturn.SitePageID = clsNullable.DBNullToInteger(dr("SitePageID"))

                                Dim FieldName As String = ("Title" & LagID)
                                If dtPage.Columns.Contains(FieldName.ToUpper) Then
                                    Try
                                        cPageBasicReturn.PageTitle = dr(FieldName).ToString()
                                    Catch
                                    End Try
                                    If "TITLEUS" <> FieldName.ToUpper AndAlso String.IsNullOrEmpty(cPageBasicReturn.PageTitle) Then
                                        cPageBasicReturn.PageTitle = dr("TitleUS").ToString()
                                    End If
                                End If


                                FieldName = ("metaDescription" & LagID)
                                If dtPage.Columns.Contains(FieldName.ToUpper) Then
                                    Try
                                        cPageBasicReturn.MetaDesciption = dr(FieldName).ToString()
                                    Catch
                                    End Try
                                    If "METADESCRIPTIONUS" <> FieldName.ToUpper AndAlso String.IsNullOrEmpty(cPageBasicReturn.MetaDesciption) Then
                                        cPageBasicReturn.MetaDesciption = dr("metaDescriptionUS").ToString()
                                    End If
                                End If


                                FieldName = ("Body" & LagID)
                                If dtPage.Columns.Contains(FieldName.ToUpper) Then
                                    Try
                                        cPageBasicReturn.BodyHTM = dr(FieldName).ToString()
                                    Catch
                                    End Try
                                    If "BODYUS" <> FieldName.ToUpper AndAlso String.IsNullOrEmpty(cPageBasicReturn.BodyHTM) Then
                                        cPageBasicReturn.BodyHTM = dr("BodyUS").ToString()
                                    End If
                                End If


                                FieldName = ("metaKeywords" & LagID)
                                If dtPage.Columns.Contains(FieldName.ToUpper) Then
                                    Try
                                        cPageBasicReturn.MetaKeywords = dr(FieldName).ToString()
                                    Catch
                                    End Try
                                    If "METAKEYWORDSUS" <> FieldName.ToUpper AndAlso String.IsNullOrEmpty(cPageBasicReturn.MetaKeywords) Then
                                        cPageBasicReturn.MetaKeywords = dr("metaKeywordsUS").ToString()
                                    End If
                                End If


                                FieldName = ("MasterFileName")
                                If dtPage.Columns.Contains(FieldName.ToUpper) Then
                                    Try
                                        cPageBasicReturn.MasterPage_FileName = dr(FieldName).ToString()
                                    Catch
                                    End Try
                                End If

                                FieldName = ("MasterDisplayName")
                                If dtPage.Columns.Contains(FieldName.ToUpper) Then
                                    Try
                                        cPageBasicReturn.MasterPage_DisplayName = dr(FieldName).ToString()
                                    Catch
                                    End Try
                                End If

                                ' End While
                            Next

                            dtPage.Clear()

                        Catch ex As Exception
                            Throw
                        End Try
                    End Using
                End Using
            End Using
            
             


            Return cPageBasicReturn
        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
            'gER.ErrorMsgBox(ex, "")
        End Try
        Return cPageBasicReturn
    End Function


    Public Function GetCustomStringFromPage(ByVal LagID As String, ByVal PageKey As String, ByVal LabelKey As String, ByVal connectionString As String) As String
        Dim text As String = ""


        Dim cPageBasicReturn As New clsPageBasicReturn
        Try
            m_ConnectionString = connectionString
            cPageBasicReturn = GetPageBasics(LagID, PageKey)
            text = GetCustomString(cPageBasicReturn.SitePageID, LagID, LabelKey, False)
        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
            'gER.ErrorMsgBox(ex, "")
        End Try

        Return text
    End Function

    Public Function GetPageBasics(ByVal LagID As String, ByVal PageKey As String) As clsPageBasicReturn
        Dim cPageBasicReturn As New clsPageBasicReturn

        'Dim rsRead As SqlDataReader = Nothing

        Try
            Dim szSQL As String = "EXEC [SYS_GetSitePages] @PageKey"
      
            Using con As New SqlConnection(m_ConnectionString)
                Using cmd As SqlCommand = New SqlCommand(szSQL, con)


                    cmd.Parameters.AddWithValue("@PageKey", PageKey)

                    Using dtPage = DataHelpers.GetDataTable(cmd)
                        If (dtPage.Rows.Count > 0) Then
                            Dim dr As DataRow = dtPage.Rows(0)

                            'While rsRead.Read()

                            cPageBasicReturn.SitePageID = clsNullable.DBNullToInteger(dr("SitePageID"))


                            Dim FieldName As String = ("Title" & LagID)
                            If dtPage.Columns.Contains(FieldName.ToUpper()) Then
                                Try
                                    cPageBasicReturn.PageTitle = dr(FieldName).ToString()
                                Catch ex As Exception
                                End Try
                                If "TITLEUS" <> FieldName.ToUpper AndAlso String.IsNullOrEmpty(cPageBasicReturn.PageTitle) Then
                                    cPageBasicReturn.PageTitle = dr("TitleUS").ToString()
                                End If
                            End If


                            FieldName = ("metaDescription" & LagID)
                            If dtPage.Columns.Contains(FieldName.ToUpper) Then
                                Try
                                    cPageBasicReturn.MetaDesciption = dr(FieldName).ToString()
                                Catch ex As Exception
                                End Try
                                If "METADESCRIPTIONUS" <> FieldName.ToUpper AndAlso String.IsNullOrEmpty(cPageBasicReturn.MetaDesciption) Then
                                    cPageBasicReturn.MetaDesciption = dr("metaDescriptionUS").ToString()
                                End If
                            End If


                            FieldName = ("Body" & LagID)
                            If dtPage.Columns.Contains(FieldName.ToUpper) Then
                                Try
                                    cPageBasicReturn.BodyHTM = dr(FieldName).ToString()
                                Catch ex As Exception
                                End Try
                                If "BODYUS" <> FieldName.ToUpper AndAlso String.IsNullOrEmpty(cPageBasicReturn.BodyHTM) Then
                                    cPageBasicReturn.BodyHTM = dr("BodyUS").ToString()
                                End If
                            End If


                            FieldName = ("metaKeywords" & LagID)
                            If dtPage.Columns.Contains(FieldName.ToUpper) Then
                                Try
                                    cPageBasicReturn.MetaKeywords = dr(FieldName).ToString()
                                Catch
                                End Try
                                If "METAKEYWORDSUS" <> FieldName.ToUpper AndAlso String.IsNullOrEmpty(cPageBasicReturn.MetaKeywords) Then
                                    cPageBasicReturn.MetaKeywords = dr("metaKeywordsUS").ToString()
                                End If
                            End If

                            ''= Library.Public.Common.CheckNULL(rsRead("Body") & LagID, "")
                            ''                If Len(cPageBasicReturn.MetaDesciption) Then cPageBasicReturn.MetaDesciption = Library.Public.Common.CheckNULL(rsRead("BodyUS"), "")

                            'Exit While 'if we get two pages with this name only read one and exit
                            'End While
                        End If

                        dtPage.Clear()
                    End Using
                End Using
            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        End Try
        Return cPageBasicReturn
    End Function

    Public Function GetCustomString(ByVal SitePageID As Integer, ByVal LagID As String, ByVal LabelKey As String, NoCache As Boolean) As String

        Dim Ret As String = ""


        Dim rsRead As SqlDataReader = Nothing

        Try
            'Dim cmd As SqlCommand
            'Dim szSQL As String = "SELECT * FROM SYS_Messages WHERE SitePageID=" & SitePageID & " AND MessageKey Like '" & LabelKey & "'"
            Dim szSQL As String = Nothing ' "SELECT * FROM SYS_Messages WHERE SitePageID=@SitePageId and not messagekey like '%***'"
            If avaliablelangString.Contains(LagID) Then
                szSQL = <sql><![CDATA[
 SELECT [MessageID]
      ,[SitePageID]
      ,[MessageKey]
	     ,( CASE 
      WHEN ([##lagID##] is null or [##lagID##]='')  THEN [US]
     else [##lagID##] 
   END ) as [##lagID##] FROM SYS_Messages WHERE SitePageID=@SitePageID 
	AND MessageKey=@MessageKey
]]></sql>.Value.Replace("##lagID##", LagID)
            Else
                szSQL = <sql><![CDATA[
 SELECT [MessageID]
      ,[SitePageID]
      ,[MessageKey]
         	     ,[US] FROM SYS_Messages WHERE SitePageID=@SitePageID 
	AND MessageKey=@MessageKey
]]></sql>.Value
            End If

            Try
                Using con As New SqlConnection(m_ConnectionString)
                    Using cmd = New SqlCommand(szSQL, con)
                        cmd.Parameters.AddWithValue("@SitePageID", SitePageID)
                        cmd.Parameters.AddWithValue("@MessageKey", LabelKey)
                        cmd.Connection.Open()

                        rsRead = cmd.ExecuteReader()
                        If rsRead.Read() Then
                            Dim FieldName As String = "" & LagID
                            Try
                                Ret = Library.Public.Common.CheckNULL(rsRead(FieldName), "")
                            Catch ex As Exception
                            End Try

                            If String.IsNullOrEmpty(Ret) Then
                                Ret = Common.CheckNULL(rsRead.Item("US"), "")
                            End If
                        End If
                    End Using
                End Using
               
           
           





                
            Catch ex As Exception

            Finally
                If (rsRead IsNot Nothing) Then
                    rsRead.Close()
                End If
            End Try

        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
            'gER.ErrorMsgBox(ex, "")
        End Try

        Return Ret
    End Function

    Public Function GetCustomStringDataTable(ByVal SitePageID As Integer, ByVal LabelKey As String, ByVal LagID As String) As DataTable
        Try

            Dim szSQL As String = Nothing ' "SELECT * FROM SYS_Messages WHERE SitePageID=@SitePageId and not messagekey like '%***'"
            If avaliablelangString.Contains(LagID) Then
                szSQL = <sql><![CDATA[
 SELECT [MessageID]
      ,[SitePageID]
      ,[MessageKey]
	     ,( CASE 
      WHEN ([##lagID##] is null or [##lagID##]='')  THEN [US]
     else [##lagID##] 
   END ) as [##lagID##] FROM SYS_Messages WHERE SitePageID=@SitePageID 
	AND MessageKey=@MessageKey
]]></sql>.Value.Replace("##lagID##", LagID)
            Else
                szSQL = <sql><![CDATA[
 SELECT [MessageID]
      ,[SitePageID]
      ,[MessageKey]
         	     ,[US] FROM SYS_Messages WHERE SitePageID=@SitePageID 
	AND MessageKey=@MessageKey
]]></sql>.Value
            End If
            Using con As New SqlConnection(m_ConnectionString)
                Using cmd = New SqlCommand(szSQL, con)
                    cmd.Parameters.AddWithValue("@SitePageID", SitePageID)
                    cmd.Parameters.AddWithValue("@MessageKey", LabelKey)
                    Using Ret = DataHelpers.GetDataTable(cmd)
                        Return Ret
                    End Using
                End Using
            End Using



        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
            'gER.ErrorMsgBox(ex, "")
        End Try


    End Function
    Private Shared Property avaliablelangString As String() = {"GR", "HU", "DE", "AL", "SR", "RU", "RO", "BG", "TR", "FR", "ES", "IT", "PT"}

    Public Function GetCustomStrings(ByVal SitePageID As Integer, ByVal LagID As String) As DataTable
        Try
            Dim szSQL As String = Nothing ' "SELECT * FROM SYS_Messages WHERE SitePageID=@SitePageId and not messagekey like '%***'"
            If avaliablelangString.Contains(LagID) Then
                szSQL = <sql><![CDATA[
 SELECT [MessageID]
      ,[SitePageID]
      ,[MessageKey]
	     ,( CASE 
      WHEN ([##lagID##] is null or [##lagID##]='')  THEN [US]
     else [##lagID##] 
   END ) as [##lagID##] FROM SYS_Messages WHERE SitePageID=@SitePageId and not messagekey like '%***'
]]></sql>.Value.Replace("##lagID##", LagID)
            Else
                szSQL = <sql><![CDATA[
 SELECT [MessageID]
      ,[SitePageID]
      ,[MessageKey]
         	     ,[US] FROM SYS_Messages WHERE SitePageID=@SitePageId and not messagekey like '%***'
]]></sql>.Value
            End If

            Using con As New SqlConnection(m_ConnectionString)
                Using cmd As New SqlCommand(szSQL, con)
                    cmd.Parameters.AddWithValue("@SitePageId", SitePageID)
                    cmd.Connection.Open()
                    Using da As New SqlDataAdapter(cmd)
                        Using dt As New DataTable
                            da.Fill(dt)
                            Return dt
                        End Using
                    End Using
                End Using
            End Using
        Catch ex As Exception
            Throw
            'gER.ErrorMsgBox(ex, "")
        End Try
    End Function



End Class

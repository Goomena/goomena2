﻿Imports Dating.Server.Datasets.DLL
Imports System.Data.SqlClient

Public Class clsCreditsHelper

    Private Shared _gEUS_CreditsType As New Dictionary(Of UnlockType, EUS_CreditsType)

    Public Shared Sub ClearCache()
        Try
            If (_gEUS_CreditsType IsNot Nothing) Then
                _gEUS_CreditsType.Clear()
            End If
        Catch
        End Try

    End Sub



    Public Shared Function GetRequiredCredits(type As UnlockType) As EUS_CreditsType
        Dim creditsRequired As EUS_CreditsType = Nothing

        Try
            If (_gEUS_CreditsType.ContainsKey(type)) Then
                creditsRequired = _gEUS_CreditsType(type)
            End If
        Catch
        End Try

        If (creditsRequired Is Nothing) Then


            Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


                Try

                    If (type = UnlockType.UNLOCK_CONVERSATION) Then
                        creditsRequired = (From itm In _CMSDBDataContext.EUS_CreditsTypes
                                     Where itm.CreditsType = "UNLOCK_CONVERSATION"
                                     Select itm).FirstOrDefault()

                    ElseIf (type = UnlockType.UNLOCK_MESSAGE_READ) Then
                        creditsRequired = (From itm In _CMSDBDataContext.EUS_CreditsTypes
                                     Where itm.CreditsType = "UNLOCK_MESSAGE_READ"
                                     Select itm).FirstOrDefault()

                    ElseIf (type = UnlockType.UNLOCK_MESSAGE_SEND) Then
                        creditsRequired = (From itm In _CMSDBDataContext.EUS_CreditsTypes
                                     Where itm.CreditsType = "UNLOCK_MESSAGE_SEND"
                                     Select itm).FirstOrDefault()

                    ElseIf (type = UnlockType.UNLOCK_MESSAGE_SEND_FREE) Then
                        creditsRequired = (From itm In _CMSDBDataContext.EUS_CreditsTypes
                                     Where itm.CreditsType = "UNLOCK_MESSAGE_SEND_FREE"
                                     Select itm).FirstOrDefault()

                    ElseIf (type = UnlockType.UNLOCK_MESSAGE_READ_FREE) Then
                        creditsRequired = (From itm In _CMSDBDataContext.EUS_CreditsTypes
                                     Where itm.CreditsType = "UNLOCK_MESSAGE_READ_FREE"
                                     Select itm).FirstOrDefault()
                    End If

                Catch ex As Exception
                    Throw
                End Try
            End Using
            Try
                _gEUS_CreditsType.Add(type, creditsRequired)
            Catch
            End Try
        End If


        Return creditsRequired
    End Function


    Public Shared Function EUS_CustomerTransaction_GetGroupedByPaymentMethods(customerID As Integer) As DataTable
        Dim sql As String = <sql><![CDATA[
select 
	SMSSent=count([CustomerTransactionID]), 
	StartDate=MIN(Date_Time), 
	LastDate=MAX(Date_Time),
	PaymentMethods
from [EUS_CustomerTransaction] 
where CustomerID=@CustomerID
and PaymentMethods<>'None'
and [DebitAmountReceived]>0
group by PaymentMethods
]]></sql>.Value


        Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


            Using cmd As SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                cmd.Parameters.AddWithValue("@CustomerID", customerID)
                Using dt As DataTable = DataHelpers.GetDataTable(cmd)

                    Return dt
                End Using
            End Using
        End Using


    End Function



    Public Shared Function EUS_CustomerCredits_GetSubscriptionAvailableAmount(customerID As Integer) As Double
        Dim sql As String = <sql><![CDATA[
declare @DateCreated [datetime];
select @DateCreated=getutcdate();

SELECT 
	[CustomerCreditsId],SubscriptionAvailableAmount
FROM [EUS_CustomerCredits] o
where not [DateTimeExpiration] is null
and [DateTimeCreated]<@DateCreated
and [DateTimeExpiration]>=@DateCreated
and IsSubscription = 1
and CustomerId=@CustomerId
and SubscriptionAvailableAmount>0;
]]></sql>.Value

        Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


            Using cmd As SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                cmd.Parameters.AddWithValue("@CustomerId", customerID)
                Using dt As DataTable = DataHelpers.GetDataTable(cmd)
                    Dim result As Double = 0
                    If (dt.Rows.Count > 0) Then
                        result = clsNullable.DBNullToDouble(dt.Rows(0)("SubscriptionAvailableAmount"))
                    End If
                    Return result
                End Using
            End Using
        End Using
    End Function


    Public Shared Function EUS_CustomerCredits_UpdateSubscriptionAvailableAmount(customerID As Integer, money As Double) As Integer
        Dim sql As String = <sql><![CDATA[
declare @DateCreated [datetime], @purchasedCreditsId bigint;
select @DateCreated=getutcdate();

SELECT 
	@purchasedCreditsId=[CustomerCreditsId]
FROM [EUS_CustomerCredits] o
where not [DateTimeExpiration] is null
and [DateTimeCreated]<@DateCreated
and [DateTimeExpiration]>=@DateCreated
and IsSubscription = 1
and CustomerId=@CustomerId
and SubscriptionAvailableAmount>0;

update [EUS_CustomerCredits]
set SubscriptionAvailableAmount=SubscriptionAvailableAmount-ABS(@EuroAmount)
where [CustomerCreditsId]=@purchasedCreditsId;
]]></sql>.Value

        Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


            Using cmd As SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                cmd.Parameters.AddWithValue("@CustomerId", customerID)
                cmd.Parameters.AddWithValue("@EuroAmount", money)
                Dim result As Integer = DataHelpers.ExecuteNonQuery(cmd)

                Return result
            End Using
        End Using

    End Function

    Public Shared Function EUS_CustomerCredits_GetLast10000CreditsObtained(CustomerID As Integer, Optional NewBuy_CustomerTransactionID As Integer = -1) As DataTable
        Dim result As DataTable
        Dim sql As String = ""

        ' If (NewBuy_CustomerTransactionID > 0) Then
        sql = <sql><![CDATA[
SELECT TOP 1 cre.[CustomerCreditsId]
      ,cre.[CustomerId]
      ,cre.[Credits]
      ,cre.[DateTimeCreated]
		,isnull(tra.[DebitAmountReceived],0) as Amount
  FROM [dbo].[EUS_CustomerCredits] as cre
  left join [dbo].[EUS_CustomerTransaction] as tra  on tra.CustomerTransactionID=cre.CustomerTransactionID
  where cre.[Credits]=10000 and cre.[CustomerId]=@CustomerId and cre.CustomerTransactionID<@TransactionID
    order by [DateTimeCreated] desc
]]></sql>.Value



        ' End If

        Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


            Using cmd As SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                cmd.Parameters.AddWithValue("@CustomerId", CustomerID)
                cmd.Parameters.AddWithValue("@TransactionID", NewBuy_CustomerTransactionID)
                result = DataHelpers.GetDataTable(cmd)

                Return result
            End Using
        End Using
    End Function

    Public Shared Function EUS_CustomerCredits_GetLast10000CreditsObtainedAfterGift(CustomerID As Integer, Optional NewBuy_CustomerTransactionID As Integer = -1) As DataTable
        Dim result As DataTable
        Dim sql As String = ""

        ' If (NewBuy_CustomerTransactionID > 0) Then
        sql = <sql><![CDATA[
SELECT TOP 1 cre.[CustomerCreditsId]
      ,cre.[CustomerId]
      ,cre.[Credits]
      ,cre.[DateTimeCreated]
		,isnull(tra.[DebitAmountReceived],0) as Amount,tra.CustomerTransactionID
  FROM [dbo].[EUS_CustomerCredits] as cre
  left join [dbo].[EUS_CustomerTransaction] as tra  on tra.CustomerTransactionID=cre.CustomerTransactionID
  where cre.[Credits]=10000 and cre.[CustomerId]=@CustomerID and cre.CustomerTransactionID<@traId 
  and tra.CustomerTransactionID>isnull((select top 1 CustomerTransactionID from [dbo].[EUS_CustomerTransaction]
								 where CustomerTransactionID<@traId and CreditAmount=10000 and DebitAmount=0 and CustomerId=@CustomerID
								 order by Date_Time desc),-1)

    order by [DateTimeCreated] desc
]]></sql>.Value



        ' End If

        Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


            Using cmd As SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                cmd.Parameters.AddWithValue("@CustomerID", CustomerID)
                cmd.Parameters.AddWithValue("@traId", NewBuy_CustomerTransactionID)
                result = DataHelpers.GetDataTable(cmd)

                Return result
            End Using
        End Using
    End Function
    Public Shared Function EUS_CustomerCredits_GetLast20000CreditsObtainedAfterGift(CustomerID As Integer, Optional NewBuy_CustomerTransactionID As Integer = -1) As DataTable
        Dim result As DataTable
        Dim sql As String = ""

        ' If (NewBuy_CustomerTransactionID > 0) Then
        sql = <sql><![CDATA[
SELECT TOP 1 cre.[CustomerCreditsId]
      ,cre.[CustomerId]
      ,cre.[Credits]
      ,cre.[DateTimeCreated]
		,isnull(tra.[DebitAmountReceived],0) as Amount,tra.CustomerTransactionID
  FROM [dbo].[EUS_CustomerCredits] as cre
  left join [dbo].[EUS_CustomerTransaction] as tra  on tra.CustomerTransactionID=cre.CustomerTransactionID
  where cre.[Credits]=20000 and cre.[CustomerId]=@CustomerID and cre.CustomerTransactionID<@traId 
  and tra.CustomerTransactionID>isnull((select top 1 CustomerTransactionID from [dbo].[EUS_CustomerTransaction]
								 where CustomerTransactionID<@traId and CreditAmount=20000 and DebitAmount=0 and CustomerId=@CustomerID
								 order by Date_Time desc),-1)

    order by [DateTimeCreated] desc
]]></sql>.Value



        ' End If

        Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


            Using cmd As SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                cmd.Parameters.AddWithValue("@CustomerID", CustomerID)
                cmd.Parameters.AddWithValue("@traId", NewBuy_CustomerTransactionID)
                result = DataHelpers.GetDataTable(cmd)

                Return result
            End Using
        End Using
    End Function
    Public Shared Function EUS_CustomerCredits_GetLastCreditsObtained(CustomerID As Integer, Optional NewBuy_CustomerTransactionID As Integer = -1) As DataTable
        Dim result As DataTable
        Dim sql As String = ""

        If (NewBuy_CustomerTransactionID > 0) Then
            sql = <sql><![CDATA[
SELECT TOP 1 
		[CustomerCreditsId]
		,[CustomerId]
		,[Credits]
		,[DateTimeCreated]
		,DateTimeExpiration
		,[CustomerTransactionID]

FROM [EUS_CustomerCredits]
where customerid=@CustomerID
and [Credits]>0
and not  [CustomerTransactionID] is null
and CustomerTransactionID<@NewBuy_CustomerTransactionID
and exists(
	select [CustomerTransactionID] 
	from [EUS_CustomerTransaction] 
	where [EUS_CustomerTransaction].[CustomerTransactionID]=[EUS_CustomerCredits].[CustomerTransactionID]
	and [DebitAmountReceived]>0
    and PaymentMethods<>'SMS'
)
--and DateTimeExpiration>GETUTCDATE()
order by [CustomerCreditsId] desc
]]></sql>.Value

        Else
            sql = <sql><![CDATA[
SELECT TOP 1 
        [CustomerCreditsId]
		,[CustomerId]
		,[Credits]
		,[DateTimeCreated]
		,DateTimeExpiration
		,[CustomerTransactionID]
FROM [EUS_CustomerCredits]
where customerid=@CustomerID
and [Credits]>0
and not  [CustomerTransactionID] is null
and exists(
	select [CustomerTransactionID] 
	from [EUS_CustomerTransaction] 
	where [EUS_CustomerTransaction].[CustomerTransactionID]=[EUS_CustomerCredits].[CustomerTransactionID]
	and [DebitAmountReceived]>0
    and PaymentMethods<>'SMS'
)
--and DateTimeExpiration>GETUTCDATE()
order by [CustomerCreditsId] desc
]]></sql>.Value

        End If

        Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


            Using cmd As SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                cmd.Parameters.AddWithValue("@CustomerID", CustomerID)
                cmd.Parameters.AddWithValue("@NewBuy_CustomerTransactionID", NewBuy_CustomerTransactionID)
                result = DataHelpers.GetDataTable(cmd)
            End Using
        End Using

        Return result
    End Function


    Public Shared Function GetCustomerTotalMoneyAndCredits(CustomerID As Integer) As DataTable
        Dim result As DataTable

        Dim sql As String = <sql><![CDATA[

select 
	isnull(Credits,0) as Credits, 
	isnull(DebitAmountReceived,0) as DebitAmountReceived
from (
	select SUM(Credits) as Credits
	from EUS_CustomerCredits
	where CustomerId=@CustomerID
	and Credits > 0
) as t1,
(
	select SUM(DebitAmountReceived) as DebitAmountReceived
	from EUS_CustomerTransaction
	where CustomerId=@CustomerID
	and TransactionType <> '-1'
	and TransactionType <> '1111'
	and TransactionInfo <> '1111'
	and TransactionInfo <> '3333'
	and TransactionInfo <> '-1'
	and TransactionInfo <> ''
) as t2


]]></sql>
        Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


            Using cmd As SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                cmd.Parameters.AddWithValue("@CustomerID", CustomerID)
                result = DataHelpers.GetDataTable(cmd)
            End Using
        End Using

        Return result
    End Function
    Public Shared Function EUS_CustomerTransaction_IsTransactionExistsPW(CustomerID As Integer, TransactionInfo As String, TransactionType As Integer) As Long
        Dim ROWCOUNT As Long

        Dim sql As String = <sql><![CDATA[
select ISNULL((select top(1) [CustomerTransactionID]
                from dbo.[EUS_CustomerTransaction]
                where CustomerId=@CustomerId 
                and [TransactionInfo] like @TransactionInfo   --'%'+@TransactionInfo+'%'
                and [DebitAmount]>0 and [TransactionType]=@TransactionType ),
      -1)
]]></sql>
        Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection

            Using cmd As SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                cmd.Parameters.AddWithValue("@CustomerID", CustomerID)
                cmd.Parameters.AddWithValue("@TransactionInfo", TransactionInfo)
                cmd.Parameters.AddWithValue("@TransactionType", TransactionType)
                ROWCOUNT = DataHelpers.ExecuteScalar(cmd)
            End Using

        End Using

        Return ROWCOUNT
    End Function

    Public Shared Function EUS_CustomerTransaction_IsTransactionExists(CustomerID As Integer, TransactionInfo As String) As Long
        Dim ROWCOUNT As Long

        Dim sql As String = <sql><![CDATA[
select ISNULL((select top(1) [CustomerTransactionID]
                from dbo.[EUS_CustomerTransaction]
                where CustomerId=@CustomerId 
                and [TransactionInfo] like @TransactionInfo   --'%'+@TransactionInfo+'%'
                and [DebitAmount]>0),
      -1)
]]></sql>
        Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection

            Using cmd As SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                cmd.Parameters.AddWithValue("@CustomerID", CustomerID)
                cmd.Parameters.AddWithValue("@TransactionInfo", TransactionInfo)
                ROWCOUNT = DataHelpers.ExecuteScalar(cmd)
            End Using

        End Using

        Return ROWCOUNT
    End Function



    Public Shared Function EUS_CustomerTransaction_CountTransactions(CustomerID As Integer) As Integer
        Using ctx As New CMSDBDataContext(DataHelpers.ConnectionString)
            Dim tran As Integer = 0
            Try

                tran = (From itm In ctx.EUS_CustomerTransactions
                        Where
                            itm.CustomerID = CustomerID AndAlso
                            itm.[DebitAmountReceived] > 0
                        Select itm).Count()

            Catch ex As Exception
                Throw
            End Try
            Return tran
        End Using
    End Function


End Class

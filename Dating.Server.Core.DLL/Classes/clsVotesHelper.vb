﻿Imports Dating.Server.Datasets.DLL
Imports System.Data.SqlClient

Public Class clsVotesHelper
    Implements IDisposable

    Public Current As Eus_ProfilesVoting = Nothing
    Public Previous As Eus_ProfilesVoting = Nothing
    Public Property HasCurrent As Boolean = False
    Public Property HasPrevious As Boolean = False
    Public Property IsMale As Boolean = False
    Public Property CurrentProfileId As Integer = -1
    Private _CMSDBDataContext As CMSDBDataContext
    Public Sub New()
        Try
            _CMSDBDataContext = New CMSDBDataContext(DataHelpers.ConnectionString)
            Current = (From itm In _CMSDBDataContext.Eus_ProfilesVotings
                                              Where itm.Enabled = True And itm.DateStart <= DateTime.UtcNow And itm.DateEnd > DateTime.UtcNow
                                              Order By itm.DateEnd Ascending
                                              Select itm).FirstOrDefault()
            If Current IsNot Nothing Then
                HasCurrent = True
            End If
        Catch ex As Exception
            Throw
        End Try
        Try
            Previous = (From itm In _CMSDBDataContext.Eus_ProfilesVotings
                                              Where itm.Enabled = True And itm.DateEnd < DateTime.UtcNow
                                              Order By itm.DateEnd Descending
                                              Select itm).FirstOrDefault()
            If Previous IsNot Nothing Then
                HasPrevious = True
                If Previous.ShowWinners = False OrElse Previous.ShowWinnersUntil <= DateTime.Now Then
                    HasPrevious = False
                Else
                    If Previous.Completed = False Then
                        FixWinners(Previous.DateStart, Previous.DateEnd)
                    End If
                End If


            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Public ReadOnly Property DatetimeStarts As DateTime
        Get
            Return If(Current IsNot Nothing, Current.DateStart, Nothing)

        End Get
    End Property
    Public ReadOnly Property DatetimeEnds As DateTime
        Get
            Return If(Current IsNot Nothing, Current.DateEnd, Nothing)
        End Get
    End Property
    Public Function GetPreviousWinners() As List(Of Integer)
        Dim re As New List(Of Integer)
        Try
            If HasPrevious Then
                Try
                    If Previous.ShowWinnersUntil Is Nothing OrElse
                        Previous.ShowWinnersUntil >= DateTime.UtcNow Then
                        If Previous.Completed = False Then
                            FixWinners(Previous.DateStart, Previous.DateEnd)
                        End If
                    End If
                Catch ex As Exception
                End Try
            End If
        Catch ex As Exception
        End Try
        Return re
    End Function
    Public Function GetWinners() As DataSet

        If HasPrevious Then
            If Previous.ShowWinnersUntil Is Nothing OrElse
                       Previous.ShowWinnersUntil >= DateTime.UtcNow Then
                If Previous.Completed = False Then
                    FixWinners(Previous.DateStart, Previous.DateEnd)

                End If
                Dim __logdate As DateTime = DateTime.UtcNow
                Dim sql As String = ""
                'This script is based on kilometer. If you use miles simply convert the radius to kilometer (radius=radius*1.609344)
                Try
                    sql = <sql><![CDATA[


select
                EUS_Profiles.PointsBeauty,    
		        EUS_Profiles.PointsVerification,     
		        EUS_Profiles.PointsCredits,    
                EUS_Profiles.PointsUnlocks,
                EUS_Profiles.ProfileID, 
                EUS_Profiles.IsMaster, 
                EUS_Profiles.MirrorProfileID, 
                EUS_Profiles.Status,
                EUS_Profiles.LoginName, 
                EUS_Profiles.FirstName,
                EUS_Profiles.LastName, 
                EUS_Profiles.GenderId, 
                EUS_Profiles.DateTimeToRegister, 
                       EUS_Profiles.LastUpdateProfileDateTime, 
                EUS_Profiles.LastUpdateProfileGEOInfo, 
                EUS_Profiles.LAGID, 
                EUS_Profiles.CustomReferrer, 
                EUS_Profiles.Birthday, 
                EUS_Profiles.IsOnline, 
                EUS_Profiles.LastActivityDateTime, 
                EUS_Profiles.AvailableCredits, 
                EUS_Profiles.DefPhotoID,
                EUS_Profiles.PhotosApproved, 
EUS_Profiles.[VotesInLast24Hour],
      EUS_Profiles.[LastVoteDateTime],
           cast(case 
			        when (isnull((select count([customerphotosid])
					 from [dbo].[eus_customerphotos] with (nolock)
					where ([CustomerID] in (EUS_Profiles.MirrorProfileID ,EUS_Profiles.ProfileID))
						and [HasAproved]=1 and [IsDeleted]=0 and [DisplayLevel]=0),0)> 0  )  then 1
			        else 0  end as bit) as HasPhoto  	,
                phot.CustomerPhotosID, phot.CustomerID, 
                phot.DateTimeToUploading, phot.FileName, 
                phot.DisplayLevel, phot.HasAproved, phot.HasDeclined, phot.IsDefault, CASE 
		/*WHEN ISNULL(Birthday, '') = '' then null
		ELSE dbo.fn_GetAge(Birthday, GETDATE())*/
        WHEN not Birthday is null then dbo.fn_GetAge(Birthday, GETDATE())
		ELSE null
	End as Age
    ,IsOnlineNow=CAST((CASE
            WHEN pps.[PrivacySettings_ShowMeOffline]=1 THEN 0
            WHEN not @LastActivityUTCDate is null and (IsOnline=1 and LastActivityDateTime>=@LastActivityUTCDate) THEN 1
            ELSE 0
        END) as bit)
    ,IsOnlineRecently=CAST((CASE
            WHEN pps.[PrivacySettings_ShowMeOffline]=1 THEN 0
            WHEN not @LastActivityUTCDate is null and (IsOnline=1 and LastActivityDateTime>=@LastActivityRecentlyUTCDate) THEN 1
            ELSE 0
        END) as bit)
 
    ,HasSubscription = CAST((case
        when GenderId=1 and exists (select CustomerCreditsId from EUS_CustomerCredits  cc  where cc.CustomerId = EUS_Profiles.ProfileID and cc.IsSubscription=1 and cc.DateTimeExpiration>getutcdate()) 
        then 1
        else 0
    end) as bit),
	cast((case  WHEN EUS_Profiles.Profileid=@pr1 or EUS_Profiles.MirrorProfileID=@pr1 then 1
				else 				
					case  WHEN EUS_Profiles.Profileid=@pr2 or EUS_Profiles.MirrorProfileID=@pr2 then 2
					else 3 end
				end) as int) as Place
              	 FROM		
                dbo.EUS_Profiles AS EUS_Profiles 
		        with (nolock)
	        LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot WITH (NOLOCK) ON phot.CustomerPhotosID = EUS_Profiles.DefPhotoID AND  phot.CustomerID = EUS_Profiles.ProfileID
           left join [EUS_ProfilesPrivacySettings] pps  WITH (NOLOCK) on (pps.ProfileID=EUS_Profiles.ProfileID OR pps.[MirrorProfileID]=EUS_Profiles.ProfileID)
		    WHERE  
 EXISTS (SELECT p.Profileid
              FROM EUS_Profiles as p WITH (NOLOCK)
              WHERE p.ProfileID>1 and (p.Profileid in (@pr1,@pr2,@pr3) or p.MirrorProfileID in (@pr1,@pr2,@pr3) ) AND
			   p.Status = @ReturnRecordsWithStatus and
			  p.ProfileID=EUS_Profiles.Profileid)  and IsMaster=1
 order by Place asc
]]></sql>.Value
                    '2014-09-12 00:00:00.000
                    Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                        Using command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(con, sql)


                            command.Parameters.Add(New SqlClient.SqlParameter("@ReturnRecordsWithStatus", 4))
                            If Previous IsNot Nothing Then
                                If IsMale Then

                                    Try
                                        If Previous.PrWinner1Female Is Nothing Then command.Parameters.Add(New SqlClient.SqlParameter("@pr1", DBNull.Value)) Else command.Parameters.Add(New SqlClient.SqlParameter("@pr1", Previous.PrWinner1Female))
                                        If Previous.PrWinner2Female Is Nothing Then command.Parameters.Add(New SqlClient.SqlParameter("@pr2", DBNull.Value)) Else command.Parameters.Add(New SqlClient.SqlParameter("@pr2", Previous.PrWinner2Female))
                                        If Previous.PrWinner3Female Is Nothing Then command.Parameters.Add(New SqlClient.SqlParameter("@pr3", DBNull.Value)) Else command.Parameters.Add(New SqlClient.SqlParameter("@pr3", Previous.PrWinner3Female))

                                    Catch ex As Exception
                                    End Try
                                Else
                                    Try
                                        If Previous.PrWinner1 Is Nothing Then command.Parameters.Add(New SqlClient.SqlParameter("@pr1", DBNull.Value)) Else command.Parameters.Add(New SqlClient.SqlParameter("@pr1", Previous.PrWinner1))
                                        If Previous.PrWinner2 Is Nothing Then command.Parameters.Add(New SqlClient.SqlParameter("@pr2", DBNull.Value)) Else command.Parameters.Add(New SqlClient.SqlParameter("@pr2", Previous.PrWinner2))
                                        If Previous.PrWinner3 Is Nothing Then command.Parameters.Add(New SqlClient.SqlParameter("@pr3", DBNull.Value)) Else command.Parameters.Add(New SqlClient.SqlParameter("@pr3", Previous.PrWinner3))

                                    Catch ex As Exception
                                    End Try
                                End If
                            End If

                            Try
                                Dim mins As Integer = clsConfigValues.Get__members_online_minutes()
                                Dim LastActivityUTCDate As DateTime = Date.UtcNow.AddMinutes(-mins)
                                command.Parameters.Add(New SqlClient.SqlParameter("@LastActivityUTCDate", LastActivityUTCDate))
                            Catch
                                command.Parameters.Add(New SqlClient.SqlParameter("@LastActivityUTCDate", System.DBNull.Value))
                            End Try

                            Try
                                Dim hours As Integer = clsConfigValues.Get__members_online_recently_hours()
                                Dim LastActivityUTCDate As DateTime = Date.UtcNow.AddHours(-hours)
                                command.Parameters.Add(New SqlClient.SqlParameter("@LastActivityRecentlyUTCDate", LastActivityUTCDate))
                            Catch
                                command.Parameters.Add(New SqlClient.SqlParameter("@LastActivityRecentlyUTCDate", System.DBNull.Value))
                            End Try
                            Using dt = DataHelpers.GetDataSet(command)
                                Return dt
                            End Using
                        End Using
                    End Using
                Catch ex As Exception
                    Throw New Exception(ex.Message, ex)
                Finally
                    clsLogger.InsertLog("GetWinners", CurrentProfileId, (DateTime.UtcNow - __logdate).TotalMilliseconds)
                End Try
            Else
                Return Nothing
            End If
        Else
            Return Nothing
        End If
    End Function
    Private Sub FixWinners(ByVal dts As DateTime, ByVal dte As DateTime)
        Dim ds As New DSMembersAdmin
        Dim __logdate As DateTime = DateTime.UtcNow
        Try
            If Previous IsNot Nothing Then


                Using ta As New DSMembersAdminTableAdapters.GetTop12LoveHateMaleFemaleTableAdapter
                    Using con As New SqlConnection(DataHelpers.ConnectionString)
                        ta.Connection = con
                        ta.Fill(ds.GetTop12LoveHateMaleFemale, dts, dte)
                    End Using
                

                End Using
                For Each r As DSMembersAdmin.GetTop12LoveHateMaleFemaleRow In ds.GetTop12LoveHateMaleFemale
                    If r.IsMale Then
                        If r.IsLove Then
                            Select Case r.Place
                                Case 1
                                    Previous.PrWinner1Female = r.Winner
                                    Exit Select
                                Case 2
                                    Previous.PrWinner2Female = r.Winner
                                    Exit Select
                                Case 3
                                    Previous.PrWinner3Female = r.Winner
                                    Exit Select
                            End Select
                        Else
                            Select Case r.Place
                                Case 1
                                    Previous.PrLosser1Female = r.Winner
                                    Exit Select
                                Case 2
                                    Previous.PrLosser2Female = r.Winner
                                    Exit Select
                                Case 3
                                    Previous.PrLosser3Female = r.Winner
                                    Exit Select
                            End Select
                        End If
                    Else
                        If r.IsLove Then
                            Select Case r.Place
                                Case 1
                                    Previous.PrWinner1 = r.Winner
                                    Exit Select
                                Case 2
                                    Previous.PrWinner2 = r.Winner
                                    Exit Select
                                Case 3
                                    Previous.PrWinner3 = r.Winner
                                    Exit Select
                            End Select
                        Else
                            Select Case r.Place
                                Case 1
                                    Previous.PrLosser1 = r.Winner
                                    Exit Select
                                Case 2
                                    Previous.PrLosser2 = r.Winner
                                    Exit Select
                                Case 3
                                    Previous.PrLosser3 = r.Winner
                                    Exit Select
                            End Select
                        End If



                    End If
                Next

            End If
        Catch ex As Exception

            Throw New Exception(ex.Message, ex)
        Finally
            ds.Dispose()
            clsLogger.InsertLog("FixWinners", CurrentProfileId, (DateTime.UtcNow - __logdate).TotalMilliseconds)
        End Try

        Try
            Previous.Completed = True
            ' _CMSDBDataContext.Eus_ProfilesVotings.InsertOnSubmit(Previous)
            _CMSDBDataContext.SubmitChanges()
        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        Finally
            _CMSDBDataContext.Dispose()
            clsLogger.InsertLog("FixWinners", CurrentProfileId, (DateTime.UtcNow - __logdate).TotalMilliseconds)
        End Try


    End Sub

#Region "IDisposable Support"
    Private disposedValue As Boolean ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                ' TODO: dispose managed state (managed objects).
            End If
            If _CMSDBDataContext IsNot Nothing Then
                Try
                    _CMSDBDataContext.Dispose()
                Catch ex As Exception

                End Try
            End If
            ' TODO: free unmanaged resources (unmanaged objects) and override Finalize() below.
            ' TODO: set large fields to null.
        End If
        Me.disposedValue = True
    End Sub

    ' TODO: override Finalize() only if Dispose(ByVal disposing As Boolean) above has code to free unmanaged resources.
    'Protected Overrides Sub Finalize()
    '    ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
    '    Dispose(False)
    '    MyBase.Finalize()
    'End Sub

    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.

        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class

﻿Imports Dating.Server.Datasets.DLL
Imports System.Data.SqlClient

Public Class SYS_CountriesGEOHelper

    Private Shared _gSYS_CountriesGEO As New Dictionary(Of String, SYS_CountriesGEO)

    Private Shared _gDSListsGEO__IsEnabled_PrintableNameASC As DSGEO
    Public Shared ReadOnly Property gDSListsGEO__IsEnabled_PrintableNameASC As DSGEO
        Get
            If (_gDSListsGEO__IsEnabled_PrintableNameASC Is Nothing) Then _gDSListsGEO__IsEnabled_PrintableNameASC = SYS_CountriesGEOHelper.GetSYS_CountriesGEO__IsEnabled_PrintableNameASC()
            Return _gDSListsGEO__IsEnabled_PrintableNameASC
        End Get
    End Property


    Public Shared Sub ClearCache()
        Try

            If (_gDSListsGEO__IsEnabled_PrintableNameASC IsNot Nothing) Then
                _gDSListsGEO__IsEnabled_PrintableNameASC.Dispose()
            End If
            _gDSListsGEO__IsEnabled_PrintableNameASC = Nothing
        Catch
        End Try

        Try
            If (_gSYS_CountriesGEO IsNot Nothing) Then
                _gSYS_CountriesGEO.Clear()
            End If
        Catch
        End Try

    End Sub


    'Public Shared Function SYS_CountriesGEO_GetForCountry(country As String) As SYS_CountriesGEO
    '    Dim ctr As SYS_CountriesGEO = Nothing
    '    Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
    '    Try
    '        ctr = (From itm In _CMSDBDataContext.SYS_CountriesGEOs
    '                                      Where itm.Iso = country.ToUpper()
    '                                      Select itm).FirstOrDefault()

    '    Catch ex As Exception
    '        Throw
    '    Finally
    '        _CMSDBDataContext.Dispose()
    '    End Try

    '    Return ctr
    'End Function

    'Public Shared Function SYS_CountriesGEO_GetForCountry(ByRef cntx As CMSDBDataContext, country As String) As SYS_CountriesGEO
    '    Dim ctr As SYS_CountriesGEO = Nothing
    '    Try
    '        ctr = (From itm In cntx.SYS_CountriesGEOs
    '                                      Where itm.Iso = country.ToUpper()
    '                                      Select itm).FirstOrDefault()

    '    Catch ex As Exception
    '        Throw
    '    Finally
    '    End Try

    '    Return ctr
    'End Function


    Public Shared Function SYS_CountriesGEO_CheckWomenOnline() As List(Of SYS_CountriesGEO)
        Dim ctr As New List(Of SYS_CountriesGEO)
        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try
                ctr = (From itm In _CMSDBDataContext.SYS_CountriesGEOs
                                              Where itm.WomenOnlinePercent > 0 AndAlso
                                              itm.IsEnabled = True
                                              Select itm).ToList()

            Catch ex As Exception
                Throw
         
            End Try
        End Using
        Return ctr
    End Function

    Public Shared Function SYS_CountriesGEO_CheckWomenOnline(ByRef cntx As CMSDBDataContext) As List(Of SYS_CountriesGEO)
        Dim ctr As New List(Of SYS_CountriesGEO)
        Try
            ctr = (From itm In cntx.SYS_CountriesGEOs
                                          Where itm.WomenOnlinePercent > 0
                                          Select itm).ToList()

        Catch ex As Exception
            Throw
        Finally
        End Try

        Return ctr
    End Function



    Public Shared Function GetSYS_CountriesGEO__IsEnabled_PrintableNameASC() As DSGEO
        Using ds As New DSGEO()
            Using ta As New DSGEOTableAdapters.TableAdapterManager
                Using connection As New SqlConnection(DataHelpers.ConnectionString)
                    ta.SYS_CountriesGEOTableAdapter = New DSGEOTableAdapters.SYS_CountriesGEOTableAdapter()
                    ta.SYS_CountriesGEOTableAdapter.Connection = connection
                    ta.SYS_CountriesGEOTableAdapter.FillBy_IsEnabled_PrintableNameASC(ds.SYS_CountriesGEO)
                    Return ds
                End Using
            End Using
        End Using
    End Function



    Public Shared Function SYS_CountriesGEO_GetForCountry(ByRef cntx As CMSDBDataContext, country As String) As SYS_CountriesGEO
        Dim ctr As SYS_CountriesGEO = Nothing
        If (String.IsNullOrEmpty(country)) Then Return ctr

        Try
            country = country.ToUpper()

            Try
                If (_gSYS_CountriesGEO.ContainsKey(country)) Then
                    ctr = _gSYS_CountriesGEO(country)
                End If
            Catch
            End Try

            If (ctr Is Nothing) Then
                ctr = (From itm In cntx.SYS_CountriesGEOs
                            Where itm.Iso = country.ToUpper()
                            Select itm).FirstOrDefault()

                Try
                    _gSYS_CountriesGEO.Add(country, ctr)
                Catch
                End Try
            End If


        Catch ex As Exception
            Throw
        Finally
        End Try

        Return ctr
    End Function


    Public Shared Function SYS_CountriesGEO_GetForCountry(country As String) As SYS_CountriesGEO
        Dim ctr As SYS_CountriesGEO = Nothing
        Try
            Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
                ctr = SYS_CountriesGEO_GetForCountry(_CMSDBDataContext, country)
            End Using

        Catch ex As Exception
            Throw
        End Try

        Return ctr
    End Function

    Public Shared Function SYS_CountriesGEO_GetSubscriptionREF_CRD2EURO_Rate(geo As SYS_CountriesGEO) As Double
        Dim SubscriptionREF_CRD2EURO_Rate As Double = 0

        If (geo.SubscriptionMessageReferralAmount IsNot Nothing) Then
            SubscriptionREF_CRD2EURO_Rate = (geo.SubscriptionMessageReferralAmount / ProfileHelper.Config_UNLOCK_MESSAGE_SEND_CREDITS +
                                         geo.SubscriptionMessageReferralAmount / ProfileHelper.Config_UNLOCK_MESSAGE_READ_CREDITS) / 2
        End If

        Return SubscriptionREF_CRD2EURO_Rate
    End Function


End Class

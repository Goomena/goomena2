﻿Imports Dating.Server.Datasets.DLL

Public Class clsConfigValues

    Public Property auto_approve_new_profiles As String
    Public Property auto_approve_photos As String
    Public Property auto_approve_updating_profiles As String
    Public Property site_name As String
    Public Property image_thumb_url As String
    Public Property image_url As String
    Public Property image_thumb_server_path As String
    Public Property image_server_path As String
    Public Property view_profile_url As String
    Public Property zone_a As String
    Public Property zone_b As String
    Public Property zone_c As String
    Public Property members_online_minutes As String
    Public Property ip_to_map As String
    Public Property documents_server_path As String

    Public Property image_d150_server_path As String
    Public Property image_d150_url As String

    Public Property image_d350_server_path As String
    Public Property image_d350_url As String

    Public Property referrer_commission_conv_rate As Double
    Public Property referrer_commission_level_0 As Double
    Public Property referrer_commission_level_1 As Double
    Public Property referrer_commission_level_2 As Double
    Public Property referrer_commission_level_3 As Double
    Public Property referrer_commission_level_4 As Double
    Public Property referrer_commission_level_5 As Double
    Public Property referrer_commission_pending_percent As Double
    Public Property referrer_commission_pending_period As Integer

    Public Property ratio_eur_usd As Double
    Public Property ratio_eur_lek As Double

    Public Property credits_bonus_codes As Integer
    Public Property validate_email_address_normal As Boolean
    Public Property photos_max_approved As Integer


    Public Property admin_controler_phone1 As String
    Public Property admin_controler_phone2 As String
    Public Property admin_controler_phone3 As String
    Public Property admin_controler_phone4 As String
    Public Property admin_controler_phone5 As String
    Public Property admin_controler_email As String

    Public Property referrer_calculate_currency As String
    Public Property gSMTPServerName As String = "mail.goomena.com"
    Public Property gSMTPLoginName As String = "support@goomena.com"
    Public Property gSMTPPassword As String = "Greece2012"
    Public Property gSMTPOutPort As Integer = 25
    Public Property gSMTPUseSSL As Boolean = False


    Public Property gSMTPServerNameSupport As String = "mail.goomena.com"
    Public Property gSMTPLoginNameSupport As String = "support@goomena.com"
    Public Property gSMTPPasswordSupport As String = "Greece2012"
    Public Property gSMTPOutPortSupport As Integer = 25
    Public Property gSMTPUseSSLSupport As Boolean = False

    Public Sub New()
        ' Dim configValue As String = Nothing


        Try
            Using _CMSDBDataContext = New CMSDBDataContext(DataHelpers.ConnectionString)


                Dim access As List(Of SYS_Config) = (From itm In _CMSDBDataContext.SYS_Configs
                                                     Select itm).ToList()

                For Each itm In access
                    Select Case itm.ConfigName
                        Case "members_online_minutes" : members_online_minutes = itm.ConfigValue
                        Case "ip_to_map" : ip_to_map = itm.ConfigValue
                        Case "auto_approve_new_profiles" : auto_approve_new_profiles = itm.ConfigValue
                        Case "auto_approve_photos" : auto_approve_photos = itm.ConfigValue
                        Case "auto_approve_updating_profiles" : auto_approve_updating_profiles = itm.ConfigValue
                        Case "image_thumb_url" : image_thumb_url = itm.ConfigValue
                        Case "image_url" : image_url = itm.ConfigValue
                        Case "image_thumb_server_path" : image_thumb_server_path = itm.ConfigValue
                        Case "image_server_path" : image_server_path = itm.ConfigValue
                        Case "site_name" : site_name = itm.ConfigValue
                        Case "view_profile_url" : view_profile_url = itm.ConfigValue
                        Case "zone_a" : zone_a = itm.ConfigValue
                        Case "zone_b" : zone_b = itm.ConfigValue
                        Case "zone_c" : zone_c = itm.ConfigValue

                        Case "image_d150_server_path" : image_d150_server_path = itm.ConfigValue
                        Case "image_d150_url" : image_d150_url = itm.ConfigValue

                        Case "image_d350_server_path" : image_d350_server_path = itm.ConfigValue
                        Case "image_d350_url" : image_d350_url = itm.ConfigValue

                        Case "documents_server_path" : documents_server_path = itm.ConfigValue

                        Case "ratio_eur_usd" : ratio_eur_usd = itm.ConfigValueDouble
                        Case "ratio_eur_lek" : ratio_eur_lek = itm.ConfigValueDouble
                        Case "credits_bonus_codes" : credits_bonus_codes = itm.ConfigValueDouble
                        Case "photos_max_approved" : photos_max_approved = itm.ConfigValueDouble

                        Case "validate_email_address_normal" : validate_email_address_normal = itm.ConfigValueDouble

                        Case "referrer_commission_conv_rate"
                            If (itm.ConfigValueDouble.HasValue) Then
                                referrer_commission_conv_rate = itm.ConfigValueDouble.Value
                            End If

                        Case "referrer_commission_level_0"
                            If (itm.ConfigValueDouble.HasValue) Then
                                referrer_commission_level_0 = itm.ConfigValueDouble.Value
                            End If

                        Case "referrer_commission_level_1"
                            If (itm.ConfigValueDouble.HasValue) Then
                                referrer_commission_level_1 = itm.ConfigValueDouble.Value
                            End If

                        Case "referrer_commission_level_2"
                            If (itm.ConfigValueDouble.HasValue) Then
                                referrer_commission_level_2 = itm.ConfigValueDouble.Value
                            End If

                        Case "referrer_commission_level_3"
                            If (itm.ConfigValueDouble.HasValue) Then
                                referrer_commission_level_3 = itm.ConfigValueDouble.Value
                            End If

                        Case "referrer_commission_level_4"
                            If (itm.ConfigValueDouble.HasValue) Then
                                referrer_commission_level_4 = itm.ConfigValueDouble.Value
                            End If

                        Case "referrer_commission_level_5"
                            If (itm.ConfigValueDouble.HasValue) Then
                                referrer_commission_level_5 = itm.ConfigValueDouble.Value
                            End If

                        Case "referrer_commission_pending_percent"
                            If (itm.ConfigValueDouble.HasValue) Then
                                referrer_commission_pending_percent = itm.ConfigValueDouble.Value
                            End If

                        Case "referrer_commission_pending_period"
                            If (itm.ConfigValueDouble.HasValue) Then
                                referrer_commission_pending_period = itm.ConfigValueDouble.Value
                            End If


                        Case "admin_controler_phone1" : If (Not String.IsNullOrEmpty(itm.ConfigValue)) Then admin_controler_phone1 = itm.ConfigValue
                        Case "admin_controler_phone2" : If (Not String.IsNullOrEmpty(itm.ConfigValue)) Then admin_controler_phone2 = itm.ConfigValue
                        Case "admin_controler_phone3" : If (Not String.IsNullOrEmpty(itm.ConfigValue)) Then admin_controler_phone3 = itm.ConfigValue
                        Case "admin_controler_phone4" : If (Not String.IsNullOrEmpty(itm.ConfigValue)) Then admin_controler_phone4 = itm.ConfigValue
                        Case "admin_controler_phone5" : If (Not String.IsNullOrEmpty(itm.ConfigValue)) Then admin_controler_phone5 = itm.ConfigValue
                        Case "admin_controler_email" : If (Not String.IsNullOrEmpty(itm.ConfigValue)) Then admin_controler_email = itm.ConfigValue

                        Case "referrer_calculate_currency" : If (Not String.IsNullOrEmpty(itm.ConfigValue)) Then referrer_calculate_currency = itm.ConfigValue


                        Case "gSMTPServerName" : If (Not String.IsNullOrEmpty(itm.ConfigValue)) Then gSMTPServerName = itm.ConfigValue
                        Case "gSMTPLoginName" : If (Not String.IsNullOrEmpty(itm.ConfigValue)) Then gSMTPLoginName = itm.ConfigValue
                        Case "gSMTPPassword" : If (Not String.IsNullOrEmpty(itm.ConfigValue)) Then gSMTPPassword = itm.ConfigValue
                        Case "gSMTPOutPort" : If (Not String.IsNullOrEmpty(itm.ConfigValue)) Then Integer.TryParse(itm.ConfigValue, gSMTPOutPort)
                        Case "gSMTPUseSSL" : If (Not String.IsNullOrEmpty(itm.ConfigValue)) Then gSMTPUseSSL = If(itm.ConfigValue.ToLower = "true", True, False)

                        Case "gSMTPServerNameSupport" : If (Not String.IsNullOrEmpty(itm.ConfigValue)) Then gSMTPServerNameSupport = itm.ConfigValue
                        Case "gSMTPLoginNameSupport" : If (Not String.IsNullOrEmpty(itm.ConfigValue)) Then gSMTPLoginNameSupport = itm.ConfigValue
                        Case "gSMTPPasswordSupport" : If (Not String.IsNullOrEmpty(itm.ConfigValue)) Then gSMTPPasswordSupport = itm.ConfigValue
                        Case "gSMTPOutPortSupport" : If (Not String.IsNullOrEmpty(itm.ConfigValue)) Then Integer.TryParse(itm.ConfigValue, gSMTPOutPortSupport)
                        Case "gSMTPUseSSLSupport" : If (Not String.IsNullOrEmpty(itm.ConfigValue)) Then gSMTPUseSSLSupport = If(itm.ConfigValue.ToLower = "true", True, False)
                    End Select
                Next
            End Using
        Catch ex As Exception
            Throw
        End Try
    End Sub

   

    Public Shared Function GetSYS_ConfigValue(key As String) As String
        Dim configValue As String = Nothing
        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try
                Dim access As SYS_Config = (From itm In _CMSDBDataContext.SYS_Configs
                                                     Where itm.ConfigName = key.ToLower()
                                                     Select itm).FirstOrDefault()

                configValue = access.ConfigValue
            Catch ex As Exception
                Throw
            End Try
        End Using
        Return configValue
    End Function

    Public Shared Function GetSYS_ConfigValueDouble(ByVal key As String) As String
        Dim configValue As String = Nothing
        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try
                Dim access As SYS_Config = (From itm In _CMSDBDataContext.SYS_Configs
                                                    Where itm.ConfigName = key.ToLower()
                                                    Select itm).FirstOrDefault()

                configValue = access.ConfigValueDouble
            Catch ex As Exception
                Throw
        
            End Try
        End Using
        Return configValue
    End Function


    Public Shared Function GetSYS_ConfigValueDouble2(ByVal key As String) As Double
        Dim configValue As Double? = Nothing
        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)

            Try
                configValue = (From itm In _CMSDBDataContext.SYS_Configs
                                Where itm.ConfigName = key.ToLower()
                                Select itm.ConfigValueDouble).FirstOrDefault()
            Catch ex As Exception
                Throw
            End Try

        End Using
        Return clsNullable.NullTo(configValue)
    End Function


    Private Shared __referrer_commission_conv_rate As Double = -1
    Public Shared Function Get__referrer_commission_conv_rate() As Double
        If (__referrer_commission_conv_rate = -1) Then
            __referrer_commission_conv_rate = clsConfigValues.GetSYS_ConfigValueDouble2("referrer_commission_conv_rate")
        End If
        Return __referrer_commission_conv_rate
    End Function


    Private Shared __ratio_eur_lek As Double = -1
    Public Shared Function Get__ratio_eur_lek() As Double
        If (__ratio_eur_lek = -1) Then
            __ratio_eur_lek = clsConfigValues.GetSYS_ConfigValueDouble2("ratio_eur_lek")
        End If
        Return __ratio_eur_lek
    End Function


    Private Shared __ratio_eur_try As Double = -1
    Public Shared Function Get__ratio_eur_try() As Double
        If (__ratio_eur_try = -1) Then
            __ratio_eur_try = clsConfigValues.GetSYS_ConfigValueDouble2("ratio_eur_try")
        End If
        Return __ratio_eur_try
    End Function


    Private Shared __credits_bonus_codes As Integer = -1
    Public Shared Function Get__credits_bonus_codes() As Integer
        If (__credits_bonus_codes = -1) Then
            __credits_bonus_codes = clsConfigValues.GetSYS_ConfigValueDouble2("credits_bonus_codes")
        End If
        Return __credits_bonus_codes
    End Function


    Private Shared __validate_email_address_normal As Boolean?
    Public Shared Function Get__validate_email_address_normal() As Boolean
        If (Not __validate_email_address_normal.HasValue) Then
            __validate_email_address_normal = clsConfigValues.GetSYS_ConfigValueDouble("validate_email_address_normal")
        End If
        Return clsNullable.NullTo(__validate_email_address_normal)
    End Function


    Private Shared __photos_max_approved As Integer = -1
    Public Shared Function Get__photos_max_approved() As Integer
        If (__photos_max_approved = -1) Then
            __photos_max_approved = clsConfigValues.GetSYS_ConfigValueDouble2("photos_max_approved")
        End If
        Return __photos_max_approved
    End Function


    Private Shared __members_online_minutes As Integer = -1
    Public Shared Function Get__members_online_minutes() As Integer
        If (__members_online_minutes = -1) Then
            Dim tmp As String = clsConfigValues.GetSYS_ConfigValue("members_online_minutes")
            If (Not Integer.TryParse(tmp, __members_online_minutes)) Then __members_online_minutes = 20
        End If
        Return __members_online_minutes
    End Function


    Private Shared __members_online_recently_hours As Integer = -1
    Public Shared Function Get__members_online_recently_hours() As Integer
        If (__members_online_recently_hours = -1) Then
            __members_online_recently_hours = clsConfigValues.GetSYS_ConfigValueDouble2("members_online_recently_hours")
        End If
        Return __members_online_recently_hours
    End Function


    'Private Shared __payment_provider_percents As Integer = -1
    'Public Shared Function Get__payment_provider_percents() As Integer
    '    If (__payment_provider_percents = -1) Then
    '        __payment_provider_percents = clsConfigValues.GetSYS_ConfigValueDouble("payment_provider_percents")
    '    End If
    '    Return __payment_provider_percents
    'End Function

    'Private Shared __payment_provider_percents_max As Integer = -1
    'Public Shared Function Get__payment_provider_percents_max() As Integer
    '    If (__payment_provider_percents_max = -1) Then
    '        __payment_provider_percents_max = clsConfigValues.GetSYS_ConfigValueDouble("payment_provider_percents_max")
    '    End If
    '    Return __payment_provider_percents_max
    'End Function


    'Public Shared Function GetRandomPaymentProviderPercent() As Integer
    '    Dim _min As Integer = clsConfigValues.Get__payment_provider_percents()
    '    Dim _max As Integer = clsConfigValues.Get__payment_provider_percents_max()

    '    Dim rnd As New Random(System.DateTime.Now.Millisecond)
    '    If _min > _max Then
    '        Dim t As Integer = _min
    '        _min = _max
    '        _max = t
    '    End If
    '    Dim payment_provider_ratio As Integer = rnd.Next(_min, _max)
    '    Return payment_provider_ratio
    'End Function


    Private Shared __message_check_forbidden_words_warning_threshold As Integer = -1
    Public Shared Function Get__message_check_forbidden_words_warning_threshold() As Integer
        If (__message_check_forbidden_words_warning_threshold = -1) Then
            __message_check_forbidden_words_warning_threshold = clsConfigValues.GetSYS_ConfigValueDouble2("message_check_forbidden_words_warning_threshold")
        End If
        Return __message_check_forbidden_words_warning_threshold
    End Function


    Private Shared __auto_approve_new_profiles As Boolean?
    Public Shared Function Get__auto_approve_new_profiles() As Boolean
        If (__auto_approve_new_profiles Is Nothing) Then
            __auto_approve_new_profiles = False
            If (clsConfigValues.GetSYS_ConfigValue("auto_approve_new_profiles") = "1") Then __auto_approve_new_profiles = True
        End If
        Return __auto_approve_new_profiles
    End Function


    Private Shared __autorebill_for_customers As List(Of Integer)
    Public Shared Function Get__autorebill_for_customers() As List(Of Integer)
        If (__autorebill_for_customers Is Nothing) Then

            __autorebill_for_customers = New List(Of Integer)
            Try

                Dim tmp As String = clsConfigValues.GetSYS_ConfigValue("autorebill_for_customers")
                If (Not String.IsNullOrEmpty(tmp)) Then
                    Dim tmpArr As String() = tmp.Split({","c}, System.StringSplitOptions.RemoveEmptyEntries)
                    For c = 0 To tmpArr.Length - 1
                        Dim i As Integer
                        Integer.TryParse(tmpArr(c), i)
                        If (i > 1) Then
                            __autorebill_for_customers.Add(i)
                        End If
                    Next
                End If

            Catch
            End Try

        End If
        Return __autorebill_for_customers
    End Function

    Private Shared __test_payment_for_customers As List(Of Integer)
    Public Shared Function Get__test_payment_for_customers() As List(Of Integer)
        If (__test_payment_for_customers Is Nothing) Then

            __test_payment_for_customers = New List(Of Integer)
            Try

                Dim tmp As String = clsConfigValues.GetSYS_ConfigValue("test_payment_for_customers")
                If (Not String.IsNullOrEmpty(tmp)) Then
                    Dim tmpArr As String() = tmp.Split({","c}, System.StringSplitOptions.RemoveEmptyEntries)
                    For c = 0 To tmpArr.Length - 1
                        Dim i As Integer
                        Integer.TryParse(tmpArr(c), i)
                        If (i > 1) Then
                            __test_payment_for_customers.Add(i)
                        End If
                    Next
                End If

            Catch
            End Try

        End If
        Return __test_payment_for_customers
    End Function


    Private Shared __products_page_show_sms As Boolean?
    Public Shared Function Get__products_page_show_sms() As Boolean
        If (__products_page_show_sms Is Nothing) Then
            __products_page_show_sms = False
            If (clsConfigValues.GetSYS_ConfigValueDouble2("products_page_show_sms") = 1) Then __products_page_show_sms = True
        End If
        Return __products_page_show_sms
    End Function



    Private Shared __messages_allow_links As Boolean?
    Public Shared Function Get__messages_allow_links() As Boolean
        If (__messages_allow_links Is Nothing) Then
            __messages_allow_links = False
            If (clsConfigValues.GetSYS_ConfigValueDouble2("messages_allow_links") = 1) Then __messages_allow_links = True
        End If
        Return __messages_allow_links
    End Function
    Public Shared Function Get_PaymentProviderMeter() As Integer
        Dim re As Integer = 0
        Try
            re = clsConfigValues.GetSYS_ConfigValueDouble2("PayMentProviderMeter")
        Catch ex As Exception
            re = 0
        End Try
        Return re
    End Function



    Private Shared __referrer_friend_gift_credits As Integer = -1
    Public Shared Function Get__referrer_friend_gift_credits() As Integer
        If (__referrer_friend_gift_credits = -1) Then
            __referrer_friend_gift_credits = clsConfigValues.GetSYS_ConfigValueDouble2("referrer_friend_gift_credits")
        End If
        Return __referrer_friend_gift_credits
    End Function

    Private Shared __referrer_payout_min_balance As Integer = -1
    Public Shared Function Get__referrer_payout_min_balance() As Integer
        If (__referrer_payout_min_balance = -1) Then
            __referrer_payout_min_balance = clsConfigValues.GetSYS_ConfigValueDouble2("referrer_payout_min_balance")
        End If
        Return __referrer_payout_min_balance
    End Function


    Public Shared Sub ClearCache()
        __ratio_eur_lek = -1
        __credits_bonus_codes = -1
        __validate_email_address_normal = Nothing
        __photos_max_approved = -1
        __members_online_minutes = -1
        '__payment_provider_percents = -1
        '__payment_provider_percents_max = -1
        __auto_approve_new_profiles = Nothing
        __autorebill_for_customers = Nothing
        __products_page_show_sms = Nothing
        __messages_allow_links = Nothing
        __referrer_friend_gift_credits = -1
        __referrer_payout_min_balance = -1
    End Sub

End Class

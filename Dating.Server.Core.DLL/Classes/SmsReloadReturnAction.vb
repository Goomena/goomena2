﻿Public Class SmsReloadReturnAction
    Public WhatToDo As SmsTransactionNextAction = SmsTransactionNextAction.DoNothing
    Public DateSmsLimitIsOff As DateTime = DateTime.UtcNow.AddDays(5)
End Class
Public Enum SmsTransactionNextAction As Integer
    DoNothing = 10
    WarnHimAboutTheLimit = 20
    DoNotReloadHisCredits = 30
End Enum

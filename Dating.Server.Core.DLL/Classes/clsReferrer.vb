﻿Imports Dating.Server.Datasets.DLL
Imports System.Text
Imports System.Transactions

Public Class clsReferrer

    Public Const StopLevel As Integer = 5
    Public Const DefaultDaysRange As Integer = 30
    Public Const Referrer_CODE_PATTERN As String = "GOM-[123]-[LOGIN]"
    Public Const Referrer_CODE_PATTERN_Check As String = "(GOM-(?<customer>\d+)-(?<login>.*))"
    Public Shared Function CreateReferrerCode(ByVal LoginName As String, ByVal MasterProfileId As Integer) As String
        If (LoginName.Length > 3) Then
            LoginName = LoginName.Remove(3)
        ElseIf (LoginName.Length = 2) Then
            LoginName = LoginName & "c"
        End If
        Dim code As String = Referrer_CODE_PATTERN.Replace("[123]", MasterProfileId).Replace("[LOGIN]", LoginName)
        Return code
    End Function

    Public Shared Sub GetProfilesReferrers(ByRef ds As DSMembers, targetLevel As Integer, profileId As Integer)
        ' get persons list for target level

        Dim mySubReferrers As New List(Of Integer)
        If (targetLevel > 0) Then
            mySubReferrers = GetPersonsId(targetLevel, profileId)
        Else
            mySubReferrers.Add(profileId)
        End If


        If (mySubReferrers.Count > 0) Then
            Dim sb As New StringBuilder()
            Dim c As Integer
            For c = 0 To mySubReferrers.Count - 1
                Dim _profileId As Integer = mySubReferrers(c)
                sb.Append(_profileId & ",")
            Next
            If (sb.Length > 0) Then
                sb.Remove(sb.Length - 1, 1)
            End If

            Dim sql As String = <sql><![CDATA[
select 
    *
from eus_profiles
where ProfileID in (@SubReferrers)
and IsMaster = 1
]]></sql>.Value '
            sql = sql.Replace("@SubReferrers", sb.ToString())
            DataHelpers.EUS_Profiles_LoadDataTable(ds, sql)

            'Dim cmd As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql)
            'Dim rsRead As SqlClient.SqlDataReader = Nothing
            'Try
            '    rsRead = DataHelpers.GetDataReader(cmd)
            '    ds.EUS_Profiles.Load(rsRead, LoadOption.OverwriteChanges)
            'Catch ex As Exception
            '    Throw
            'Finally
            '    If (rsRead IsNot Nothing) Then rsRead.Close()
            '    If (cmd IsNot Nothing) Then
            '        cmd.Connection.Close()
            '        cmd.Dispose()
            '    End If
            'End Try
        End If

        ' Return ds.EUS_Profiles
    End Sub



    Private Shared Function GetPersonsId(targetLevel As Integer, profileId As Integer) As List(Of Integer)
        Dim mySubReferrers As New List(Of Integer)

        Dim level As Integer = 0

        If (profileId > 1) Then
            level = 1
            mySubReferrers.Add(profileId)
        Else
            level = 0
            mySubReferrers.Add(profileId)
        End If


        '        ' level 1
        '        Try

        '            Dim sql As String = <sql><![CDATA[
        'select 
        '    --COUNT(ProfileID) as ProfilesCount
        '    ProfileID
        'from eus_profiles
        'where referrerparentid in (@ProfileID)
        'and IsMaster = 1
        ']]></sql>.Value '
        '            sql = sql.Replace("@ProfileID", profileId)
        '            Dim _dt As DataTable = DataHelpers.GetDataTable(sql)

        '            Dim c As Integer
        '            For c = 0 To _dt.Rows.Count - 1
        '                Dim _profileId As Integer = _dt.Rows(c)("ProfileID")
        '                mySubReferrers.Add(_profileId)
        '            Next

        '        Catch ex As Exception
        '            Throw
        '        End Try


        ' next levels
        While (mySubReferrers.Count > 0 AndAlso level < targetLevel AndAlso level < StopLevel)

            Try

                Dim sb As New StringBuilder()
                Dim c As Integer
                For c = 0 To mySubReferrers.Count - 1
                    Dim _profileId As Integer = mySubReferrers(c)
                    sb.Append(_profileId & ",")
                Next
                If (sb.Length > 0) Then
                    sb.Remove(sb.Length - 1, 1)
                End If
                mySubReferrers.Clear()


                Dim sql As String = <sql><![CDATA[
select 
    ProfileID
from eus_profiles
with(nolock)
where referrerparentid in (@SubReferrers)
and IsMaster = 1
]]></sql>.Value
                sql = sql.Replace("@SubReferrers", sb.ToString())
                Using _dt As DataTable = DataHelpers.GetDataTable(sql)
                    For c = 0 To _dt.Rows.Count - 1
                        Dim _profileId As Integer = _dt.Rows(c)("ProfileID")
                        mySubReferrers.Add(_profileId)
                    Next
                End Using



            Catch ex As Exception
                Throw
            End Try

            level = level + 1
        End While


        Return mySubReferrers
    End Function





    Public Shared Sub GetProfilesForReferrerParentIdList(ByRef ds As DSMembers, mySubReferrers As List(Of Integer))

        Dim mySubReferrersLevelDown As New List(Of Integer)

        Try
            ds.EUS_Profiles.Clear()

            Dim sb As New StringBuilder()
            Dim c As Integer
            For c = 0 To mySubReferrers.Count - 1
                Dim _profileId As Integer = mySubReferrers(c)
                sb.Append(_profileId & ",")
            Next
            If (sb.Length > 0) Then
                sb.Remove(sb.Length - 1, 1)
            End If
            mySubReferrers.Clear()


            Dim sql As String = <sql><![CDATA[
select 
    ProfileID
from eus_profiles
with(nolock)
where referrerparentid in (@SubReferrers)
and IsMaster = 1
]]></sql>.Value
            sql = sql.Replace("@SubReferrers", sb.ToString())
            Using _dt As DataTable = DataHelpers.GetDataTable(sql)
                For c = 0 To _dt.Rows.Count - 1
                    Dim _profileId As Integer = _dt.Rows(c)("ProfileID")
                    mySubReferrersLevelDown.Add(_profileId)
                Next
            End Using



        Catch ex As Exception
            Throw
        End Try


        If (mySubReferrersLevelDown.Count > 0) Then
            Dim sb As New StringBuilder()
            Dim c As Integer
            For c = 0 To mySubReferrersLevelDown.Count - 1
                Dim _profileId As Integer = mySubReferrersLevelDown(c)
                sb.Append(_profileId & ",")
            Next
            If (sb.Length > 0) Then
                sb.Remove(sb.Length - 1, 1)
            End If

            Dim sql As String = <sql><![CDATA[
select 
    *
from eus_profiles
where ProfileID in (@SubReferrers)
and IsMaster = 1
]]></sql>.Value '
            sql = sql.Replace("@SubReferrers", sb.ToString())
            DataHelpers.EUS_Profiles_LoadDataTable(ds, sql)

            ''Dim sdr As SqlClient.SqlDataReader = DataHelpers.GetDataReader(sql)
            ''ds.EUS_Profiles.Load(sdr, LoadOption.OverwriteChanges)
            'Dim sdr As SqlClient.SqlDataReader = Nothing
            'Try
            '    sdr = DataHelpers.GetDataReader(sql)
            '    ds.EUS_Profiles.Load(sdr, LoadOption.OverwriteChanges)
            'Catch ex As Exception
            '    Throw
            'Finally
            '    If (sdr IsNot Nothing) Then
            '        If (Not sdr.IsClosed) Then sdr.Close()
            '    End If
            'End Try
        End If

        'Return ds.EUS_Profiles
    End Sub


    Public Shared Function SetCommissionForMessage_Subscription(MasterProfileId As Integer, costPerMessage As Double, EUS_MessageID As Long, RefProfileID As Integer, customerCreditsId As Long, unlockTypeVar As UnlockType) As Boolean
        Dim isSet As Boolean = False

        Dim SubscriptionAvailableAmount As Double = clsCreditsHelper.EUS_CustomerCredits_GetSubscriptionAvailableAmount(MasterProfileId)
        If (SubscriptionAvailableAmount > 0) Then

            Dim moneySet As Double = 0

            ' write commissition for recipient referrer
            If (unlockTypeVar = UnlockType.UNLOCK_MESSAGE_SEND) Then

                moneySet = clsReferrer.SetCommissionForMessage(EUS_MessageID,
                                                    RefProfileID,
                                                    costPerMessage,
                                                    ProfileHelper.Config_UNLOCK_MESSAGE_SEND_CREDITS,
                                                    UnlockType.UNLOCK_MESSAGE_SEND,
                                                    customerCreditsId)

            ElseIf (unlockTypeVar = UnlockType.UNLOCK_MESSAGE_READ) Then

                moneySet = clsReferrer.SetCommissionForMessage(EUS_MessageID,
                                                    RefProfileID,
                                                    costPerMessage,
                                                    ProfileHelper.Config_UNLOCK_MESSAGE_READ_CREDITS,
                                                    UnlockType.UNLOCK_MESSAGE_READ,
                                                    customerCreditsId)

            End If

            If (moneySet > 0) Then
                isSet = True
                clsCreditsHelper.EUS_CustomerCredits_UpdateSubscriptionAvailableAmount(MasterProfileId, moneySet)
            End If
        End If

        Return isSet
    End Function

    Public Shared Function SetCommissionForMessage(MsgID As Integer, RefProfileID As Integer, cost As Double, creditsAmount As Integer, unlockType As UnlockType, CustomerCreditsId As Long) As Double
        Dim moneySet As Double = 0
        If (MsgID > 0 AndAlso RefProfileID > 0) Then
            'Using tx As New System.Transactions.TransactionScope(TransactionScopeOption.Required, New TransactionOptions With {.IsolationLevel = IsolationLevel.ReadUncommitted})
            Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
                Try
                    Dim parentRefId As Integer? = (From itm In _CMSDBDataContext.EUS_Profiles
                                                        Where itm.ProfileID = RefProfileID
                                                        Select itm.ReferrerParentId).SingleOrDefault()
                    Dim HasReduction As Boolean? = (From itm In _CMSDBDataContext.EUS_Profiles
                    Where itm.ProfileID = RefProfileID
                                                     Select itm.Ref_Reduction).SingleOrDefault()

                    If (parentRefId > 0) Then

                        Dim message As EUS_Message = (From itm In _CMSDBDataContext.EUS_Messages
                                                        Where itm.EUS_MessageID = MsgID AndAlso (itm.IsHidden Is Nothing OrElse itm.IsHidden = False)
                                                        Select itm).SingleOrDefault()


                        If (message IsNot Nothing) Then
                            message.Cost = cost
                            message.CreditsSpent = creditsAmount
                            message.CustomerCreditsId = CustomerCreditsId

                            Dim EUS_CreditsTypeID As Integer = clsUserDoes.GetRequiredCredits(unlockType).EUS_CreditsTypeID
                            message.EUS_CreditsTypeID = EUS_CreditsTypeID


                            Dim refComm As New clsReferrerCommissions()

                            If (RefProfileID > 0) Then
                                refComm.Load(RefProfileID)
                                Dim reduction As Double = 0
                                Try
                                    If HasReduction = True Then
                                        Dim max As Integer = CInt(clsConfigValues.GetSYS_ConfigValueDouble2("Ref_ReductionAmmountMax"))
                                        Dim min As Integer = CInt(clsConfigValues.GetSYS_ConfigValueDouble2("Ref_ReductionAmmountMin"))

                                        If min <> max And max > min Then
                                            Dim RandomClass As New Random()
                                            reduction = RandomClass.Next(min, max)
                                        ElseIf min = max Then
                                            reduction = min
                                        End If
                                        If reduction > 0 Then
                                            reduction = reduction / 100
                                        End If
                                    End If
                                Catch ex As Exception
                                End Try


                                message.REF_L1_Commission = refComm.REF_CommissionLevel1 - (reduction * refComm.REF_CommissionLevel1)
                                message.REF_L1_Money = (message.REF_L1_Commission) * cost
                            End If


                            Dim RefID2 As Integer, RefID3 As Integer, RefID4 As Integer, RefID5 As Integer
                            RefID2 = DataHelpers.GetEUS_Profiles_ReferrerParentId(RefProfileID)


                            If (RefID2 > 1) Then
                                refComm.Load(RefID2)
                                message.REF_L2_Commission = refComm.REF_CommissionLevel2
                                message.REF_L2_Money = refComm.REF_CommissionLevel2 * cost

                                RefID3 = DataHelpers.GetEUS_Profiles_ReferrerParentId(RefID2)
                            End If

                            If (RefID3 > 1) Then
                                refComm.Load(RefID3)
                                message.REF_L3_Commission = refComm.REF_CommissionLevel3
                                message.REF_L3_Money = refComm.REF_CommissionLevel3 * cost

                                RefID4 = DataHelpers.GetEUS_Profiles_ReferrerParentId(RefID3)
                            End If

                            If (RefID4 > 1) Then
                                refComm.Load(RefID4)
                                message.REF_L4_Commission = refComm.REF_CommissionLevel4
                                message.REF_L4_Money = refComm.REF_CommissionLevel4 * cost

                                RefID5 = DataHelpers.GetEUS_Profiles_ReferrerParentId(RefID4)
                            End If

                            If (RefID5 > 1) Then
                                ' initial referrer has parent  at level 5,  above of himself
                                refComm.Load(RefID5)
                                message.REF_L5_Commission = refComm.REF_CommissionLevel5
                                message.REF_L5_Money = refComm.REF_CommissionLevel5 * cost
                            End If


                            _CMSDBDataContext.SubmitChanges()

                            moneySet =
                                clsNullable.NullTo(message.REF_L1_Money) +
                                clsNullable.NullTo(message.REF_L2_Money) +
                                clsNullable.NullTo(message.REF_L3_Money) +
                                clsNullable.NullTo(message.REF_L4_Money) +
                                clsNullable.NullTo(message.REF_L5_Money)
                        End If


                    End If 'parentRefId > 0


                Catch ex As Exception
                    Throw

                End Try

            End Using
            'End Using
        End If
        Return moneySet
    End Function


End Class

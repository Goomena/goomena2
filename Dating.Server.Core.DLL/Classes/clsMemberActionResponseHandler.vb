﻿Imports Dating.Server.Datasets.DLL
Imports System.Data.SqlClient

Public Class clsMemberActionResponseHandler
    Implements IDisposable

    Public Event ReportProgress(sender As Object, e As TaskProgressEventArgs)
    Public Event SendingEmail(sender As Object, e As SendingEmailEventArgs)

    Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)

    Public Function GetNextEntry(minQueueid As Integer) As SYS_MemberActionPerformQueue
        Dim file As SYS_MemberActionPerformQueue
        Try
            file = (From itm In _CMSDBDataContext.SYS_MemberActionPerformQueues
                    Where itm.Exception Is Nothing AndAlso
                    itm.ReadyMessageID Is Nothing AndAlso
                    itm.QueueID > minQueueid
                    Order By itm.QueueID Ascending
                   Select itm).FirstOrDefault()

            _CMSDBDataContext.SubmitChanges()
        Catch ex As Exception
            Throw
        Finally
        End Try

        Return file
    End Function


    'Public Sub HandleCheckedEntry(file As SYS_MemberActionPerformQueue)
    '    Try
    '        If (file.RecipientProfileID Is Nothing) Then

    '            _CMSDBDataContext.SYS_MemberActionPerformQueues.DeleteOnSubmit(file)
    '            _CMSDBDataContext.SubmitChanges()

    '        Else

    '            Dim fileold As SYS_MemberActionPerformQueue =
    '                (From itm In _CMSDBDataContext.SYS_MemberActionPerformQueues
    '                    Select itm).FirstOrDefault()

    '            fileold.RecipientProfileID = file.RecipientProfileID
    '            fileold.ReadyMessageID = file.ReadyMessageID
    '            fileold.DateSent = file.DateSent

    '            _CMSDBDataContext.SubmitChanges()

    '        End If



    '    Catch ex As Exception
    '        Throw
    '    Finally
    '    End Try

    'End Sub

    Public Sub UpdateEntry(file As SYS_MemberActionPerformQueue)
        Try


            Dim fileold As SYS_MemberActionPerformQueue =
                (From itm In _CMSDBDataContext.SYS_MemberActionPerformQueues
                 Where itm.QueueID = file.QueueID
                    Select itm).FirstOrDefault()

            fileold.RecipientProfileID = file.RecipientProfileID
            fileold.ReadyMessageID = file.ReadyMessageID
            fileold.DateSent = file.DateSent
            fileold.Exception = file.Exception

            _CMSDBDataContext.SubmitChanges()


        Catch ex As Exception
            Throw
        Finally
        End Try
    End Sub


    'Public Sub SetEntryRead(itemId As Long)
    '    Try


    '        Dim fileold As SYS_MemberActionPerformQueue =
    '            (From itm In _CMSDBDataContext.SYS_MemberActionPerformQueues
    '             Where itm.QueueID = itemId
    '                Select itm).FirstOrDefault()

    '        fileold.IsRead = True
    '        _CMSDBDataContext.SubmitChanges()


    '    Catch ex As Exception
    '        Throw
    '    Finally
    '    End Try
    'End Sub

    Public Sub DeleteEntry(itemId As Long)
        Try
            Dim email = From itm In _CMSDBDataContext.SYS_MemberActionPerformQueues
                   Where itm.QueueID = itemId
                   Select itm


            _CMSDBDataContext.SYS_MemberActionPerformQueues.DeleteAllOnSubmit(email)
            _CMSDBDataContext.SubmitChanges()
        Catch ex As Exception
            Throw
        Finally
        End Try

    End Sub


    Public Sub Dispose() Implements IDisposable.Dispose
        If (_CMSDBDataContext IsNot Nothing) Then _CMSDBDataContext.Dispose()
    End Sub


    Public Shared Function GetReadyMessageForProfileID(profileId As Integer) As DataTable

        Dim sql As String = <sql><![CDATA[
select QueueID
from SYS_MemberActionPerformQueue q
inner join [EUS_ReadyMessages] rm on rm.[ReadyMessageID]=q.ReadyMessageID
where RecipientProfileID=@RecipientProfileID 
and q.DateSent is null
and q.exception is null
and isnull(q.IsRead,0)=0
]]></sql>
        Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


            Using cmd As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                cmd.Parameters.AddWithValue("@RecipientProfileID", profileId)
                Using dt As DataTable = DataHelpers.GetDataTable(cmd)
                    Return dt
                End Using


            End Using
        End Using
    End Function


    Public Shared Sub SetReadyMessageRead(QueueID As Long, profileId As Integer)

        Dim sql As String = <sql><![CDATA[
update SYS_MemberActionPerformQueue 
    set IsRead=1 
where QueueID=@QueueID
and RecipientProfileID=@RecipientProfileID 
]]></sql>
        Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


            Using cmd As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                cmd.Parameters.AddWithValue("@QueueID", QueueID)
                cmd.Parameters.AddWithValue("@RecipientProfileID", profileId)
                DataHelpers.ExecuteNonQuery(cmd)
            End Using
        End Using

    End Sub

    Private sqlReadyMessages As String = <sql><![CDATA[
EXECUTE [EUS_ReadyMessages_GetNext] 
   @ProfileID=@ProfileID
  ,@CurrentAge=@CurrentAge
  ,@CurrentCountry=@CurrentCountry
  ,@LAGID=@LAGID
  ,@ACTIONSID=@ACTIONSID
  ,@SendPeriodMonth=@SendPeriodMonth
  ,@SendPeriodDay=@SendPeriodDay
  ,@SendPeriodHour=@SendPeriodHour
  ,@DelayMinutes=@DelayMinutes
  ,@CheckDelayMinutesMin=0
    ]]></sql>.Value

    Private sqlReadyMessages_WithDelay As String = <sql><![CDATA[
    -- to keep current action, we have to check 
    -- if there is any message with big delay

EXECUTE [EUS_ReadyMessages_GetNext] 
   @ProfileID=@ProfileID
  ,@CurrentAge=@CurrentAge
  ,@CurrentCountry=@CurrentCountry
  ,@LAGID=@LAGID
  ,@ACTIONSID=@ACTIONSID
  ,@SendPeriodMonth=@SendPeriodMonth
  ,@SendPeriodDay=@SendPeriodDay
  ,@SendPeriodHour=@SendPeriodHour
  ,@DelayMinutes=@DelayMinutes
  ,@CheckDelayMinutesMin=1
    ]]></sql>.Value



    Public Sub MemberActionResponseHandler()
        Using ctx As New CMSDBDataContext(DataHelpers.ConnectionString)



            Try

                Dim minQueueid As Integer = 0
                Dim itemCount As Integer
                Dim allowSending As Double?
                Dim LastActivityUTCDate As DateTime

                While (itemCount < 11)

                    Dim item As SYS_MemberActionPerformQueue = Nothing
                    Try
                        item = Me.GetNextEntry(minQueueid)
                        If (item Is Nothing) Then
                            Exit While
                        End If

                        If (allowSending Is Nothing) Then
                            allowSending = clsConfigValues.GetSYS_ConfigValueDouble2("ready_messages_allow_sending")
                        End If
                        If (LastActivityUTCDate = DateTime.MinValue) Then
                            Dim mins As Integer = clsConfigValues.Get__members_online_minutes()
                            LastActivityUTCDate = Date.UtcNow.AddMinutes(-mins)
                        End If

                        Dim deleteEntry As Boolean = False
                        If (CInt(allowSending) <> 1) Then deleteEntry = True


                        Dim actionType As MemberActionTypeEnum = item.Type
                        Dim messageActionsString As String = ""
                        Dim profile = Nothing, profileWoman = Nothing

                        If (Not deleteEntry) Then

                            If ((actionType And MemberActionTypeEnum.Likes) = MemberActionTypeEnum.Likes) Then messageActionsString = messageActionsString & CInt(ReadyMessagesActionsEnum.After_Man_Like) & ","
                            If ((actionType And MemberActionTypeEnum.Viewed) = MemberActionTypeEnum.Viewed) Then messageActionsString = messageActionsString & CInt(ReadyMessagesActionsEnum.After_profile_view) & ","
                            messageActionsString = messageActionsString.Trim(","c)

                            If (String.IsNullOrEmpty(messageActionsString)) Then deleteEntry = True
                        End If


                        If (Not deleteEntry) Then
                            profile = (From itm In ctx.EUS_Profiles
                                        Where itm.ProfileID = item.FromProfileID AndAlso
                                        itm.IsMaster = True AndAlso
                                        itm.Status = ProfileStatusEnum.Approved AndAlso
                                        itm.GenderId = 1
                                        Select itm.ProfileID, itm.Birthday, itm.LoginName, itm.Country, itm.LAGID, itm.GenderId).FirstOrDefault()

                            profileWoman = (From itm In ctx.EUS_Profiles
                                        Where itm.ProfileID = item.ToProfileID AndAlso
                                        itm.IsMaster = True AndAlso
                                        itm.Status = ProfileStatusEnum.Approved AndAlso
                                        itm.GenderId = 2 AndAlso
                                        (itm.ReferrerParentId IsNot Nothing AndAlso itm.ReferrerParentId > 0)
                                        Select itm.ProfileID, itm.Birthday, itm.LoginName, itm.Country, itm.LAGID, itm.IsOnline, itm.LastActivityDateTime).FirstOrDefault()

                            If (profile Is Nothing) Then deleteEntry = True
                            If (profileWoman Is Nothing) Then deleteEntry = True

                            If (profileWoman IsNot Nothing) Then
                                Dim isOnline As Boolean = clsNullable.NullTo(profileWoman.IsOnline)
                                If (isOnline) Then
                                    Dim __LastActivityDateTime As DateTime? = profileWoman.LastActivityDateTime
                                    If (__LastActivityDateTime.HasValue) Then
                                        isOnline = __LastActivityDateTime >= LastActivityUTCDate
                                    Else
                                        isOnline = False
                                    End If
                                End If

                                If (Not isOnline) Then deleteEntry = True
                            End If

                        End If

                        If (Not deleteEntry) Then
                            deleteEntry = clsUserDoes.IsAnyMessageExchanged(item.ToProfileID, item.FromProfileID)
                        End If

                        If (deleteEntry) Then
                            Me.DeleteEntry(item.QueueID)
                            Continue While
                        End If


                        Dim SendPeriodDay As Integer = DateTime.UtcNow.Day
                        Dim SendPeriodMonth As Integer = DateTime.UtcNow.Month
                        Dim SendPeriodHour As Integer = DateTime.UtcNow.Hour
                        Dim SendPeriodMinute As Integer = DateTime.UtcNow.Minute
                        '     Dim SendPeriodSecond As Integer = DateTime.UtcNow.Second
                        Dim CurrentAge As Integer = ProfileHelper.GetCurrentAge(profile.Birthday)

                        'Dim newSql As String = sqlReadyMessages.Replace("[ACTIONSID]", messageActionsString)
                        Dim newSql As String = sqlReadyMessages
                        Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                            Using cmd As SqlCommand = DataHelpers.GetSqlCommand(con, newSql)


                                cmd.Parameters.AddWithValue("@SendPeriodDay", SendPeriodDay)
                                cmd.Parameters.AddWithValue("@SendPeriodMonth", SendPeriodMonth)
                                cmd.Parameters.AddWithValue("@SendPeriodHour", SendPeriodHour)
                                cmd.Parameters.AddWithValue("@SendPeriodMinute", SendPeriodMinute)
                                cmd.Parameters.AddWithValue("@CurrentAge", CurrentAge)
                                cmd.Parameters.AddWithValue("@LAGID", profile.LAGID)
                                cmd.Parameters.AddWithValue("@CurrentCountry", profile.Country)
                                cmd.Parameters.AddWithValue("@ProfileID", profile.ProfileID)
                                cmd.Parameters.AddWithValue("@ACTIONSID", messageActionsString)

                                Dim minsDiff As Double = ((Date.UtcNow - item.DateTimeToCreate.Value).TotalSeconds / 60)
                                cmd.Parameters.AddWithValue("@DelayMinutes", minsDiff)

                                Using dt As DataTable = DataHelpers.GetDataTable(cmd)


                                    If (dt.Rows.Count > 0 AndAlso CDbl(dt.Rows(0)("ReadyMessageID")) > 0) Then

                                        ' we have some results 

                                        ' 1. update queue item's values
                                        item.RecipientProfileID = profile.ProfileID
                                        item.ReadyMessageID = dt.Rows(0)("ReadyMessageID")
                                        item.DateSent = Date.UtcNow


                                        ' 2. send message to member man and create sent message for woman
                                        Dim msg As EUS_ReadyMessage = (From itm In ctx.EUS_ReadyMessages
                                                                      Where itm.ReadyMessageID = item.ReadyMessageID
                                                                      Select itm).FirstOrDefault()


                                        If (msg IsNot Nothing) Then
                                            Dim replyToMessageId As Integer = -1
                                            Dim blockReceiving As Boolean = False
                                            Dim IsAuto As Boolean = True

                                            Dim messageReceived As EUS_Message = clsUserDoes.SendMessage("", msg.Text, item.ToProfileID, profile.ProfileID, False, replyToMessageId, blockReceiving, IsAuto, Nothing)
                                            item.EUS_MessageID = messageReceived.EUS_MessageID

                                            Dim OfferID As Integer = -1
                                            If ((actionType And MemberActionTypeEnum.Likes) = MemberActionTypeEnum.Likes) Then
                                                Dim rec As EUS_Offer = clsUserDoes.GetAnyWinkPending(item.ToProfileID, item.FromProfileID) 'Me.MasterProfileId, Me.UserIdInView
                                                If (rec IsNot Nothing) Then
                                                    OfferID = rec.OfferID
                                                End If
                                            End If

                                            ' h gynaika pou apantaei sto minima, kleinei to like na min einai new
                                            clsUserDoes.MakeNewDate(item.ToProfileID, profile.ProfileID, OfferID, False)


                                            Me.UpdateEntry(item)
                                        End If


                                    Else
                                        ' is there any message, that have delay on it
                                        ' if such message exists, then keep current action

                                        newSql = sqlReadyMessages_WithDelay '.Replace("[ACTIONSID]", messageActionsString)
                                        cmd.CommandText = newSql
                                        Using dt2 = DataHelpers.GetDataTable(cmd)



                                            If (dt2.Rows.Count > 0) Then
                                                Dim DelayMinutesMin As Integer = CDbl(dt2.Rows(0)("DelayMinutesMin"))
                                                If (DelayMinutesMin > 0 AndAlso minsDiff < DelayMinutesMin - 1) Then
                                                    minQueueid = item.QueueID
                                                    RaiseEvent ReportProgress(Me, TaskProgressEventArgs.GetNew(0, " Current queue item  (SYS_MemberActionPerformQueue:QueueID " & minQueueid & ") will be checked in next iteration. Found message(s) that can be sent after some delay (EUS_ReadyMessages:DelayMinutesMin " & DelayMinutesMin & ")"))
                                                    ' bw.ReportProgress(0, " Current queue item  (SYS_MemberActionPerformQueue:QueueID " & minQueueid & ") will be checked in next iteration. Found message(s) that can be sent after some delay (EUS_ReadyMessages:DelayMinutesMin " & DelayMinutesMin & ")")
                                                    Continue While
                                                Else
                                                    deleteEntry = True
                                                End If
                                            Else
                                                deleteEntry = True
                                            End If
                                        End Using
                                    End If
                                End Using
                            End Using
                        End Using
                        If (deleteEntry) Then
                            Me.DeleteEntry(item.QueueID)
                            Continue While
                        End If


                    Catch ex As Exception
                        RaiseEvent ReportProgress(Me, TaskProgressEventArgs.GetNew(0, " Exc. " & ex.Message))
                        'bw.ReportProgress(0, " Exc. " & ex.Message)

                        Try
                            item.Exception = ex.ToString()
                            Me.UpdateEntry(item)
                        Catch
                        End Try

                        Try
                            Dim str As String = ""
                            str = vbCrLf &
                                "Checking ReadyMessages!" & vbCrLf & vbCrLf & _
                                 "FromProfileID: " & item.FromProfileID & vbCrLf & _
                                 "ToProfileID: " & item.ToProfileID & vbCrLf & _
                                 "Type: " & item.Type & vbCrLf & vbCrLf & _
                                 "Exception : " & ex.ToString()
                            'clsMyMail.SendMail2(My.Settings.gToEmail, My.Settings.gToEmail, "Exception, while checking ReadyMessages: " & ex.Message, str, False)
                            RaiseEvent SendingEmail(Me, SendingEmailEventArgs.GetNew("Exception, while checking ReadyMessages: " & ex.Message,
                                                                                     str,
                                                                                     False))


                            System.Threading.Thread.Sleep(120000)
                        Catch ex1 As Exception
                        End Try
                    End Try

                    itemCount = itemCount + 1
                    System.Threading.Thread.Sleep(500)
                End While

            Catch ex As Exception
                Throw New Exception(ex.Message, ex)
         
            End Try
        End Using

    End Sub



End Class

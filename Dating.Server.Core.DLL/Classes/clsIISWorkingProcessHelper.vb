﻿Imports Dating.Server.Datasets.DLL
Imports System.Data.SqlClient

Public Class clsIISWorkingProcessHelper

    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")> Private Shared Function GetDataContext() As CMSDBDataContext
        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString) With {.CommandTimeout = 5000}
        Return _CMSDBDataContext
    End Function


    Public Shared Sub AddProc(id As Long)
        Using _CMSDBDataContext As CMSDBDataContext = GetDataContext()


            Dim file As SYS_IISWorkingProcess
            Try
                file = (From itm In _CMSDBDataContext.SYS_IISWorkingProcesses
                        Where itm.ID = id
                       Select itm).FirstOrDefault()

                If (file Is Nothing) Then
                    file = New SYS_IISWorkingProcess()
                    file.ID = id
                    _CMSDBDataContext.SYS_IISWorkingProcesses.InsertOnSubmit(file)
                End If

                file.Nocache = False
                file.StartedDate = Date.UtcNow

                _CMSDBDataContext.SubmitChanges()
            Catch ex As Exception
                Throw
            End Try
        End Using
    End Sub

    Public Shared Sub SetNocacheOn(callingId As Long)
        Dim sql As String = "update SYS_IISWorkingProcesses set Nocache=1 where [ID]<>" & callingId
        DataHelpers.ExecuteNonQuery(sql)
    End Sub

    Public Shared Sub SetNocacheOff(callingId As Long)
        Dim sql As String = "update SYS_IISWorkingProcesses set Nocache=0 where [ID]=" & callingId
        DataHelpers.ExecuteNonQuery(sql)
    End Sub

    Public Shared Function IsNocacheOn(id As Long) As Boolean
        Using _CMSDBDataContext As CMSDBDataContext = GetDataContext()


            Try
                Dim file As SYS_IISWorkingProcess =
                    (From itm In _CMSDBDataContext.SYS_IISWorkingProcesses
                       Where itm.ID = id AndAlso itm.Nocache = True
                      Select itm).FirstOrDefault()

                If (file IsNot Nothing) Then
                    Return True
                End If

            Catch ex As Exception
                Throw
            End Try
        End Using
        Return False
    End Function


    Public Shared Sub DeleteProc(id As Long)
        Using _CMSDBDataContext As CMSDBDataContext = GetDataContext()


            Try
                Dim email = From itm In _CMSDBDataContext.SYS_IISWorkingProcesses
                       Where itm.ID = id
                       Select itm


                _CMSDBDataContext.SYS_IISWorkingProcesses.DeleteAllOnSubmit(email)
                _CMSDBDataContext.SubmitChanges()
            Catch ex As Exception
                Throw
            End Try
        End Using
    End Sub



End Class

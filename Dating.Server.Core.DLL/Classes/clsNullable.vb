﻿Public Class clsNullable

    Public Shared Function NullTo(val As Boolean?, Optional defValue As Boolean = False) As Boolean
        If (val Is Nothing) Then Return defValue
        Return val.Value
    End Function


    Public Shared Function NullTo(val As Integer?, Optional defValue As Integer = 0) As Integer
        If (val Is Nothing) Then Return defValue
        Return val.Value
    End Function


    Public Shared Function NullTo(val As Long?, Optional defValue As Long = 0) As Long
        If (val Is Nothing) Then Return defValue
        Return val.Value
    End Function


    Public Shared Function NullTo(val As Double?, Optional defValue As Double = 0) As Double
        If (val Is Nothing) Then Return defValue
        Return val.Value
    End Function


    Public Shared Function NullTo(val As Decimal?, Optional defValue As Decimal = 0) As Decimal
        If (val Is Nothing) Then Return defValue
        Return val.Value
    End Function

    Public Shared Function NullTo(val As DateTime?, defValue As DateTime) As DateTime
        If (val Is Nothing) Then Return defValue
        Return val.Value
    End Function


    Public Shared Function DBNullToInteger(val As Object, Optional defValue As Integer = 0) As Integer

        If (TypeOf val Is Integer) Then
            defValue = val
        ElseIf (val IsNot Nothing AndAlso Not IsDBNull(val)) Then
            defValue = Convert.ToInt32(val)
        End If

        Return defValue
    End Function

    Public Shared Function DBNullToLong(val As Object, Optional defValue As Long = 0) As Long

        If (TypeOf val Is Long) Then
            defValue = val
        ElseIf (val IsNot Nothing AndAlso Not IsDBNull(val)) Then
            defValue = Convert.ToInt64(val)
        End If

        Return defValue
    End Function

    Public Shared Function DBNullToDouble(val As Object, Optional defValue As Double = 0) As Double

        If (TypeOf val Is Double) Then
            defValue = val
        ElseIf (val IsNot Nothing AndAlso Not IsDBNull(val)) Then
            defValue = Convert.ToDouble(val)
        End If

        Return defValue
    End Function

    Public Shared Function DBNullToBoolean(val As Object, Optional defValue As Boolean = False) As Boolean
        If (TypeOf val Is Boolean) Then
            defValue = val
        ElseIf (val IsNot Nothing AndAlso Not IsDBNull(val)) Then
            defValue = Convert.ToBoolean(val)
        End If
        Return defValue
    End Function


    Public Shared Function DBNullToString(val As Object, Optional defValue As String = "") As String

        If (TypeOf val Is String) Then
            defValue = val
        ElseIf (val IsNot Nothing AndAlso Not IsDBNull(val)) Then
            defValue = val.ToString()
        End If

        Return defValue
    End Function


    Public Shared Function DBNullToDateTimeNull(val As Object, Optional defValue As DateTime? = Nothing) As DateTime?

        If (TypeOf val Is DateTime) Then
            defValue = val
        ElseIf (val IsNot Nothing AndAlso Not IsDBNull(val)) Then
            defValue = Convert.ToDateTime(val)
        End If

        Return defValue
    End Function
    Public Shared Function DBNullToDateTimeNull2(val As Object, defValue As DateTime) As DateTime

        If (TypeOf val Is DateTime) Then
            defValue = val
        ElseIf (val IsNot Nothing AndAlso Not IsDBNull(val)) Then
            defValue = Convert.ToDateTime(val)
        End If

        Return defValue
    End Function


    Public Shared Function DBNullToDecimal(val As Object, Optional defValue As Decimal = 0) As Decimal
        If (Not IsDBNull(val)) Then
            If (val.GetType() Is GetType(String)) Then
                Dim dbl As Double = 0
                Dim culture As System.Globalization.CultureInfo = System.Globalization.CultureInfo.CreateSpecificCulture("el-GR")
                Double.TryParse(val.ToString().Replace(".", ","), System.Globalization.NumberStyles.Any, culture, dbl)
                defValue = Convert.ToDecimal(dbl)
            Else
                defValue = Convert.ToDecimal(val)
            End If
        End If
        Return defValue
    End Function


End Class

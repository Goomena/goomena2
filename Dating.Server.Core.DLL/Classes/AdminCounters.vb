﻿Imports Dating.Server.Datasets.DLL
Imports System.Data.SqlClient
Imports Dating.Server.Datasets.DLL.DSMembers

Public Class AdminCounters


    Public Shared Sub GetAdminCountersPhotos(ByRef ds As DSMembersViews, customerId As Integer)
        Using con As New SqlConnection(DataHelpers.ConnectionString)
            Using ta As New DSMembersViewsTableAdapters.GetAdminCountersPhotosTableAdapter()
                ta.Connection = con
                ta.Fill(ds.GetAdminCountersPhotos, customerId)
            End Using
        End Using
     


    End Sub


End Class

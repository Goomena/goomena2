﻿Imports Dating.Server.Datasets.DLL
Imports System.Data.SqlClient

Public Class clsFreeAndSubscriptionMessagesHelper
    Implements IDisposable

    Private _CMSDBDataContext As CMSDBDataContext
    Public MasterProfileId As Integer
    Private CountryRec As SYS_CountriesGEO

    Public ReadOnly Property HasSubscription As Boolean
        Get
            Return CountryRec.MonthlySubscriptionEnabled
        End Get
    End Property


    Public ReadOnly Property CreditsPackagesBonusEnabled As Boolean
        Get
            Return CountryRec.CreditsPackagesBonusEnabled
        End Get
    End Property


    Public ReadOnly Property CreditsFreeMessageReferralAmount As Double?
        Get
            If (CountryRec.CreditsPackagesBonusEnabled) Then
                Return CountryRec.CreditsFreeMessageReferralAmount
            End If
            Return Nothing
        End Get
    End Property

    Public ReadOnly Property CreditsFreeMessagesCountReferralPayed As Integer
        Get
            If (CountryRec.CreditsPackagesBonusEnabled) Then
                Return CountryRec.CreditsFreeMessagesCountReferralPayed
            End If
            Return -1
        End Get
    End Property

    Public ReadOnly Property SubscriptionMessageReferralAmount As Double?
        Get
            If (CountryRec.MonthlySubscriptionEnabled) Then
                Return CountryRec.SubscriptionMessageReferralAmount
            End If
            Return Nothing
        End Get
    End Property

    Public ReadOnly Property SubscriptionMessagesCountReferralPayed As Integer
        Get
            If (CountryRec.MonthlySubscriptionEnabled) Then
                Return CountryRec.SubscriptionMessagesCountReferralPayed
            End If
            Return -1
        End Get
    End Property

    Private Shared Function GetDataContext() As CMSDBDataContext
        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Return _CMSDBDataContext
    End Function


    Public Sub New(profileId As Integer, profileCountry As String) ' profileGender As Integer, 
        _CMSDBDataContext = GetDataContext()
        MasterProfileId = profileId
        CountryRec = SYS_CountriesGEOHelper.SYS_CountriesGEO_GetForCountry(_CMSDBDataContext, profileCountry)
    End Sub


    Public Sub Dispose() Implements System.IDisposable.Dispose
        If (_CMSDBDataContext IsNot Nothing) Then _CMSDBDataContext.Dispose()
    End Sub




    Public Sub MessageReadByMan(MessageID As Long,
                                CopyMessageID As Long,
                                ManProfileID As Integer,
                                WomanProfileID As Integer,
                                ManProfileIP As String, ManProfileCookieID As String,
                                FreeMessagesArchive As FreeMessagesArchiveEnum)


        Dim rec As EUS_Messages_FreeMessagesArchive =
            (From itm1 In _CMSDBDataContext.EUS_Messages_FreeMessagesArchives
            Where (itm1.EUS_MessageID = MessageID OrElse itm1.EUS_MessageID = CopyMessageID) AndAlso itm1.ProfileIDOwner = Me.MasterProfileId
            Select itm1).FirstOrDefault()

        If (rec Is Nothing) Then
            If (CountryRec.CreditsPackagesBonusEnabled OrElse CountryRec.MonthlySubscriptionEnabled) Then
                rec = Me.MessageSentToMan(MessageID,
                                                CopyMessageID,
                                                WomanProfileID, ManProfileID,
                                                Nothing, Nothing)
            End If
        End If
        If (rec IsNot Nothing) Then

            If (CountryRec.CreditsPackagesBonusEnabled OrElse CountryRec.MonthlySubscriptionEnabled) Then

                rec.IsRead = True
                rec.ToProfileIP = ManProfileIP
                rec.ToProfileCookieID = ManProfileCookieID
                rec.DateTimeRead = Date.UtcNow
                If (FreeMessagesArchive = FreeMessagesArchiveEnum.FreeMessage) Then
                    rec.IsFree = True
                Else
                    rec.IsFree = False
                End If
                If (FreeMessagesArchive = FreeMessagesArchiveEnum.SubscriptionMessage) Then
                    rec.IsSubscription = True
                Else
                    rec.IsSubscription = False
                End If

                _CMSDBDataContext.SubmitChanges()

            Else

                _CMSDBDataContext.EUS_Messages_FreeMessagesArchives.DeleteOnSubmit(rec)
                _CMSDBDataContext.SubmitChanges()

            End If

        End If

    End Sub


    Public Function MessageSentToMan(MessageID As Long,
                                CopyMessageID As Long,
                                WomanProfileID As Integer, ManProfileID As Integer,
                                WomanProfileIP As String, WomanProfileCookieID As String) As EUS_Messages_FreeMessagesArchive

        'If (CountryRec.CreditsPackagesBonusEnabled OrElse
        '    CountryRec.MonthlySubscriptionEnabled) Then

        'Dim count As Integer = (From itm1 In _CMSDBDataContext.EUS_Messages_FreeMessagesArchives
        '            Where itm1.EUS_MessageID = MessageID
        '           Select itm1).Count()

        'If (count = 0) Then
        Dim itm As New EUS_Messages_FreeMessagesArchive()
        itm.DateTimeCreated = Date.UtcNow
        itm.EUS_MessageID = MessageID
        itm.CopyMessageID = CopyMessageID
        itm.FromProfileID = WomanProfileID
        itm.ToProfileID = ManProfileID
        itm.ProfileIDOwner = ManProfileID
        itm.IsRead = False
        itm.IsSent = False
        itm.FromProfileIP = WomanProfileIP
        itm.FromProfileCookieID = WomanProfileCookieID
        itm.IsSubscription = False
        itm.IsFree = False

        _CMSDBDataContext.EUS_Messages_FreeMessagesArchives.InsertOnSubmit(itm)
        _CMSDBDataContext.SubmitChanges()
        'End If

        'End If
        Return itm
    End Function


    Public Function MessageSentByMan(MessageID As Long,
                                      CopyMessageID As Long,
                                      WomanProfileID As Integer, ManProfileID As Integer,
                                      ManProfileIP As String, ManProfileCookieID As String,
                                FreeMessagesArchive As FreeMessagesArchiveEnum) As EUS_Messages_FreeMessagesArchive


        Dim itm As New EUS_Messages_FreeMessagesArchive()
        itm.DateTimeCreated = Date.UtcNow
        itm.EUS_MessageID = MessageID
        itm.CopyMessageID = CopyMessageID
        itm.FromProfileID = ManProfileID
        itm.ToProfileID = WomanProfileID
        itm.ProfileIDOwner = ManProfileID
        itm.IsRead = False
        itm.IsSent = True
        itm.FromProfileIP = ManProfileIP
        itm.FromProfileCookieID = ManProfileCookieID
        itm.DateTimeRead = itm.DateTimeCreated
        If (FreeMessagesArchive = FreeMessagesArchiveEnum.FreeMessage) Then
            itm.IsFree = True
        Else
            itm.IsFree = False
        End If
        If (FreeMessagesArchive = FreeMessagesArchiveEnum.SubscriptionMessage) Then
            itm.IsSubscription = True
        Else
            itm.IsSubscription = False
        End If

        _CMSDBDataContext.EUS_Messages_FreeMessagesArchives.InsertOnSubmit(itm)
        _CMSDBDataContext.SubmitChanges()

        Return itm
    End Function


    Public Sub MessageSetCommission(MessageID As Long,
                                    CopyMessageID As Long,
                                    ManProfileID As Integer,
                                    WomanProfileID As Integer)


        Dim rec As EUS_Messages_FreeMessagesArchive =
            (From itm1 In _CMSDBDataContext.EUS_Messages_FreeMessagesArchives
            Where (itm1.EUS_MessageID = MessageID OrElse itm1.EUS_MessageID = CopyMessageID) AndAlso itm1.ProfileIDOwner = Me.MasterProfileId
            Select itm1).FirstOrDefault()

        If (rec Is Nothing) Then
            If (CountryRec.CreditsPackagesBonusEnabled OrElse CountryRec.MonthlySubscriptionEnabled) Then
                rec = Me.MessageSentToMan(MessageID,
                                                CopyMessageID,
                                                WomanProfileID, ManProfileID,
                                                Nothing, Nothing)
            End If
        End If
        If (rec IsNot Nothing) Then

            If (CountryRec.CreditsPackagesBonusEnabled OrElse CountryRec.MonthlySubscriptionEnabled) Then

                rec.CommisionSet = 1
                _CMSDBDataContext.SubmitChanges()

            End If

        End If

    End Sub

End Class

﻿Imports System.Web

<Serializable()> _
Public Class clsSessionVariables

    Public Property MemberData As New clsDataRecordLoginMemberReturn()
    Public Property ReferrerCredits As clsReferrerCredits
    Public Property myTheme As String = ""
    Public Property myCulture As String = "US"
    Public Property Page As String = ""
    Public Property IdentityLoginName As String = ""

    'Public Property Referrer As String = ""
    'Public Property CustomReferrer As String = ""
    'Public Property IP As String = ""
    'Public Property Agent As String = ""
    'Public Property StatCookie As String = ""
    'Public Property StaticalCustomAction As String = ""
    'Public Property GEO_COUNTRY_CODE As String = ""
    'Public Property LagID As String
    'Public Property GlobalStrings As clsPageData

    Public Property WinkBeforeLogin As String
    Public Property FavoriteBeforeLogin As String

    Public Property UserUnlockInfo As New clsUserUnlockInfo()

    Public Property DateTimeToRegister As Date?


    Public Shared Function GetCurrent() As clsSessionVariables
        Dim _sessVars As clsSessionVariables
        _sessVars = HttpContext.Current.Session("SessionVariables")
        'If (_sessVars Is Nothing) Then _sessVars = New clsSessionVariables()
        Return _sessVars
    End Function

End Class


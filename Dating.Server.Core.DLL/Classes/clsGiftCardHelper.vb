﻿Imports System.Configuration
Imports System.Web

Public Class clsGiftCardHelper
    Public Shared Function ImageUrl(ByVal imgName As String) As String
        Dim re As String = ""
        Try
            re = "http://cdn.goomena.com/Images2/GiftCards/" & imgName & "?v" & Date.Now.ToShortTimeString
        Catch ex As Exception
            clsMyMail.TrySendMailSupport(ConfigurationManager.AppSettings("ExceptionsEmail"), "On clsGiftCardHelper.ImageUrl  ImgName:" & clsNullable.DBNullToString(imgName), ex.ToString())
        End Try
        Return re
    End Function
    Public Shared Function AvaliableImageUrl(ByVal imgName As String) As String
        Dim re As String = ""
        Try
            re = "http://cdn.goomena.com/Images2/GiftCards/Logos/" & imgName & "?v" & Date.Now.ToShortTimeString
        Catch ex As Exception
            clsMyMail.TrySendMailSupport(ConfigurationManager.AppSettings("ExceptionsEmail"), "On clsGiftCardHelper.AvaliableImageUrl  ImgName:" & clsNullable.DBNullToString(imgName), ex.ToString())
        End Try
        Return re
    End Function
    Public Shared Function ValueImageUrl(ByVal Name As String, ByVal Value As String) As String
        Dim re As String = ""
        Try
            re = "http://cdn.goomena.com/Images2/GiftCards/" & Name & "/" & Value & ".png" & "?v" & Date.Now.ToShortTimeString
        Catch ex As Exception
            clsMyMail.TrySendMailSupport(ConfigurationManager.AppSettings("ExceptionsEmail"), "On clsGiftCardHelper.ValueImageUrl  Name:" & clsNullable.DBNullToString(Name) & " Value:" & Value, ex.ToString())
        End Try
        Return re
    End Function
End Class

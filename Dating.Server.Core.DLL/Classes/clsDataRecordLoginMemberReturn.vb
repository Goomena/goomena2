﻿Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Datasets.DLL.DSMembers

<Serializable()> _
Public Class clsDataRecordLoginMemberReturn

    Public IsValid As Boolean
    Public HasErrors As Boolean
    Public ErrorCode As Integer
    Public Message As String
    Public ExtraMessage As String

    ' Public UsersRow As UniCMSDB.SYS_UsersRow

    ' Public AllowSiteManage As Boolean
    '  Public AllowCMS As Boolean
    Public ReferrerParentID As Integer

    Public Status As Integer
    Public ProfileID As Integer
    Public MirrorProfileID As Integer
    Public LoginName As String
    Public FirstName As String
    Public LastName As String
    Public GenderId As Integer
    Public Country As String
    Public Zip As String
    Public latitude As Double?
    Public longitude As Double?
    Public _Region As String
    Public Birthday As Date
    Public Role As String
    Public LAGID As String
    Public CustomReferrer As String
    Public LookingFor_ToMeetMaleID As Boolean
    Public LookingFor_ToMeetFemaleID As Boolean
    Public RegisterDate As DateTime
    Public Email As String
    Public PrivacySettings_HideMeFromSearchResults As Boolean
    Public PrivacySettings_NotShowInOtherUsersFavoritedList As Boolean



    'Public Token As String
    'Public HASH As String



    Public Sub Fill(EUS_MembersRow As DSMembers.EUS_ProfilesRow)

        Me.ProfileID = EUS_MembersRow.ProfileID
        Me.MirrorProfileID = EUS_MembersRow.MirrorProfileID
        Me.MirrorProfileID = IIf(Me.MirrorProfileID = 0, Me.ProfileID, Me.MirrorProfileID)
        Me.LoginName = EUS_MembersRow.LoginName
        Me.Email = EUS_MembersRow.eMail

        If (Not EUS_MembersRow.IsDateTimeToRegisterNull()) Then Me.RegisterDate = EUS_MembersRow.DateTimeToRegister
        If (Not EUS_MembersRow.IsStatusNull()) Then Me.Status = EUS_MembersRow.Status
        If (Not EUS_MembersRow.IsReferrerParentIdNull()) Then Me.ReferrerParentID = EUS_MembersRow.ReferrerParentId
        If (Not EUS_MembersRow.IsMirrorProfileIDNull()) Then Me.MirrorProfileID = EUS_MembersRow.MirrorProfileID
        If (Not EUS_MembersRow.IsFirstNameNull()) Then Me.FirstName = EUS_MembersRow.FirstName
        If (Not EUS_MembersRow.IsLastNameNull()) Then Me.LastName = EUS_MembersRow.LastName
        If (Not EUS_MembersRow.IsGenderIdNull()) Then Me.GenderId = EUS_MembersRow.GenderId
        If (Not EUS_MembersRow.IsCountryNull()) Then Me.Country = EUS_MembersRow.Country
        If (Not EUS_MembersRow.IsZipNull()) Then Me.Zip = EUS_MembersRow.Zip
        If (Not EUS_MembersRow.IslatitudeNull()) Then Me.latitude = EUS_MembersRow.latitude
        If (Not EUS_MembersRow.IslongitudeNull()) Then Me.longitude = EUS_MembersRow.longitude
        If (Not EUS_MembersRow.Is_RegionNull()) Then Me._Region = EUS_MembersRow._Region
        If (Not EUS_MembersRow.IsLAGIDNull()) Then Me.LAGID = EUS_MembersRow.LAGID
        If (Not EUS_MembersRow.IsCustomReferrerNull()) Then Me.CustomReferrer = EUS_MembersRow.CustomReferrer
        If (Not EUS_MembersRow.IsLookingFor_ToMeetMaleIDNull()) Then Me.LookingFor_ToMeetMaleID = EUS_MembersRow.LookingFor_ToMeetMaleID
        If (Not EUS_MembersRow.IsLookingFor_ToMeetFemaleIDNull()) Then Me.LookingFor_ToMeetFemaleID = EUS_MembersRow.LookingFor_ToMeetFemaleID
        Me.PrivacySettings_HideMeFromSearchResults = If(Not EUS_MembersRow.IsPrivacySettings_HideMeFromSearchResultsNull, EUS_MembersRow.PrivacySettings_HideMeFromSearchResults, False)
        Me.PrivacySettings_NotShowInOtherUsersFavoritedList = If(Not EUS_MembersRow.IsPrivacySettings_NotShowInOtherUsersFavoritedListNull, EUS_MembersRow.PrivacySettings_NotShowInOtherUsersFavoritedList, False)

        If (Not EUS_MembersRow.IsBirthdayNull()) Then
            If (EUS_MembersRow.Birthday.Year <> 1800) Then
                Me.Birthday = EUS_MembersRow.Birthday
            End If
        End If

        If (Not EUS_MembersRow.IsRoleNull()) Then
            If (Not String.IsNullOrEmpty(EUS_MembersRow.Role)) Then
                Me.Role = EUS_MembersRow.Role.ToUpper()
            Else
                Me.Role = "MEMBER"
            End If
        End If

        'Dim Token As String = Library.Public.clsHash.ComputeHash(Me.LoginName & "^Token$$$", "SHA1") 'DataRecordLogin.HardwareID & "^^" &
        'Me.Token = Token

        If EUS_MembersRow.Status > ProfileStatusEnum.None Then
            Me.HasErrors = False
            Me.Message = "OK"
            Me.IsValid = True
        Else
            Me.HasErrors = True
            Me.Message = "The user is not Active"
            Me.IsValid = False
        End If
    End Sub


End Class


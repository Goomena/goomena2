﻿
Public Class SendingEmailEventArgs
    Inherits System.EventArgs

    Public Property Subject As String
    Public Property Body As String
    Public Property IsHtml As Boolean

    Public Shared Function GetNew(subject As Integer, body As String, isHtml As Boolean) As SendingEmailEventArgs
        Dim t As New SendingEmailEventArgs()
        t.Subject = subject
        t.Body = body
        t.IsHtml = isHtml
        Return t
    End Function
End Class
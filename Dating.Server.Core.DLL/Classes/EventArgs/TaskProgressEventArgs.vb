﻿
Public Class TaskProgressEventArgs
    Inherits System.EventArgs

    Public Property Status As Integer
    Public Property ProgressInfo As String

    Public Shared Function GetNew(status As Integer, progressInfo As String) As TaskProgressEventArgs
        Dim t As New TaskProgressEventArgs()
        t.Status = status
        t.ProgressInfo = progressInfo
        Return t
    End Function
End Class


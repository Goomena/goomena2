﻿Imports System.Text.RegularExpressions
Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Datasets.DLL.DSMembers
Imports System.Text
Imports System.Web

Public Class clsUserNotifications


    Public Shared Function SendEmailNotification(notif As NotificationType, fromProfileID As Integer, toProfileId As Integer) As Boolean
        Dim success As Boolean = False
        If (notif = NotificationType.None) Then Return success


        Try
            Dim rowTo As EUS_ProfilesRow = Nothing
            Using memberTo As DSMembers = DataHelpers.GetEUS_Profiles_ByProfileID(toProfileId)
                rowTo = memberTo.EUS_Profiles.Rows(0)
            End Using

            ' If(rowTo)
            Dim rowFrom As EUS_ProfilesRow = Nothing
            Using memberFrom As DSMembers = DataHelpers.GetEUS_Profiles_ByProfileID(fromProfileID)
                rowFrom = memberFrom.EUS_Profiles.Rows(0)
            End Using



            If (rowTo.Status = ProfileStatusEnum.Approved) Then

                If (notif = NotificationType.Likes AndAlso rowTo.NotificationsSettings_WhenLikeReceived = True) Then
                    Dim o As New clsSiteLAG()
                    Dim subject As String = o.GetCustomStringFromPage(rowTo.LAGID, "GlobalStrings", "NotificationEmailForNewLikeSubject", DataHelpers.ConnectionString)
                    Dim body As String = o.GetCustomStringFromPage(rowTo.LAGID, "GlobalStrings", "NotificationEmailForNewLike2", DataHelpers.ConnectionString)

                    body = MessageBodyReplaceTokens(body, rowTo, rowFrom)
                    clsMyMail.SendMailFromQueue(rowTo.eMail, subject, body, rowTo.ProfileID, "NotificationType.Likes", True, Nothing, Nothing)
                    success = True

                ElseIf (notif = NotificationType.ProfileViewed) Then

                    If (clsProfilesPrivacySettings.GET_NotificationsSettings_WhenMyProfileVisited(toProfileId) = True) Then
                        Dim o As New clsSiteLAG()
                        Dim subject As String = o.GetCustomStringFromPage(rowTo.LAGID, "GlobalStrings", "NotificationEmailForProfileViewSubject2", DataHelpers.ConnectionString)

                        Dim ageWord As String = o.GetCustomStringFromPage(rowTo.LAGID, "~/Members/control.ProfileView", "lblAgeYearsOld", DataHelpers.ConnectionString)
                        subject = ReplaceProfileInfo(subject, rowTo, rowFrom, ageWord)
                        If (Not String.IsNullOrEmpty(subject)) Then
                            subject = subject.Replace("&", "-").Replace("?", "-").Replace("+", "-").Replace("@", "-")
                        End If

                        Dim body As String = ""
                        If (ProfileHelper.IsFemale(rowTo.GenderId)) Then
                            body = o.GetCustomStringFromPage(rowTo.LAGID, "GlobalStrings", "NotificationEmailForProfileView_Receive_FEMALE2", DataHelpers.ConnectionString)
                        Else
                            body = o.GetCustomStringFromPage(rowTo.LAGID, "GlobalStrings", "NotificationEmailForProfileView_Receive_MALE2", DataHelpers.ConnectionString)
                        End If
                        body = MessageBodyReplaceTokens(body, rowTo, rowFrom)
                        body = ReplaceProfileInfo(body, rowTo, rowFrom, ageWord)

                        'clsMyMail.SendMail(rowTo.eMail, subject, body, True)
                        clsMyMail.SendMailFromQueue(rowTo.eMail, subject, body, rowTo.ProfileID, "NotificationType.ProfileVisited", True, Nothing, Nothing)
                        success = True
                    End If

                ElseIf (notif = NotificationType.Message AndAlso rowTo.NotificationsSettings_WhenNewMessageReceived = True) Then
                    Dim o As New clsSiteLAG()


                    If (Not clsUserDoes.HasCommunication(rowTo.ProfileID, rowFrom.ProfileID) AndAlso ProfileHelper.IsMale(rowTo.GenderId)) Then

                        Dim subject As String = o.GetCustomStringFromPage(rowTo.LAGID, "GlobalStrings", "NotificationEmailForNewMessageSubject", DataHelpers.ConnectionString)
                        Dim body As String = o.GetCustomStringFromPage(rowTo.LAGID, "GlobalStrings", "MALE_NotificationEmailForNewMessage2", DataHelpers.ConnectionString)

                        body = MessageBodyReplaceTokens(body, rowTo, rowFrom)

                        'clsMyMail.SendMail(rowTo.eMail, subject, body, True)
                        clsMyMail.SendMailFromQueue(rowTo.eMail, subject, body, rowTo.ProfileID, "NotificationType.Message.ToMale", True, Nothing, Nothing)
                    Else

                        Dim subject As String = o.GetCustomStringFromPage(rowTo.LAGID, "GlobalStrings", "NotificationEmailForNewMessageSubject", DataHelpers.ConnectionString)
                        Dim body As String = o.GetCustomStringFromPage(rowTo.LAGID, "GlobalStrings", "NotificationEmailForNewMessage2", DataHelpers.ConnectionString)
                        body = MessageBodyReplaceTokens(body, rowTo, rowFrom)

                        'clsMyMail.SendMail(rowTo.eMail, subject, body, True)
                        clsMyMail.SendMailFromQueue(rowTo.eMail, subject, body, rowTo.ProfileID, "NotificationType.Message", True, Nothing, Nothing)
                    End If

                    success = True
                ElseIf (notif = NotificationType.Offers AndAlso rowTo.NotificationsSettings_WhenNewOfferReceived = True) Then
                    Dim o As New clsSiteLAG()
                    Dim subject As String = o.GetCustomStringFromPage(rowTo.LAGID, "GlobalStrings", "NotificationEmailForNewOfferSubject", DataHelpers.ConnectionString)
                    Dim body As String = o.GetCustomStringFromPage(rowTo.LAGID, "GlobalStrings", "NotificationEmailForNewOffer2", DataHelpers.ConnectionString)

                    body = MessageBodyReplaceTokens(body, rowTo, rowFrom)

                    'clsMyMail.SendMail(rowTo.eMail, subject, body, True)
                    clsMyMail.SendMailFromQueue(rowTo.eMail, subject, body, rowTo.ProfileID, "NotificationType.Offers", True, Nothing, Nothing)
                    success = True

                ElseIf (notif = NotificationType.Unlock AndAlso rowTo.NotificationsSettings_WhenNewOfferReceived = True) Then
                    Dim o As New clsSiteLAG()
                    Dim subject As String = o.GetCustomStringFromPage(rowTo.LAGID, "GlobalStrings", "NotificationEmailForUnlockSubject", DataHelpers.ConnectionString)
                    Dim body As String = o.GetCustomStringFromPage(rowTo.LAGID, "GlobalStrings", "NotificationEmailForUnlock", DataHelpers.ConnectionString)

                    body = Regex.Replace(body, "###RECIPIENTLOGINNAME###", rowTo.LoginName)
                    body = Regex.Replace(body, "###LOGINNAME###", rowFrom.LoginName)

                    'clsMyMail.SendMail(rowTo.eMail, subject, body, True)
                    clsMyMail.SendMailFromQueue(rowTo.eMail, subject, body, rowTo.ProfileID, "NotificationType.Unlock", True, Nothing, Nothing)
                    success = True

                ElseIf (notif = NotificationType.AutoNotification AndAlso rowTo.NotificationsSettings_WhenNewMembersNearMe = True) Then
                    success = clsUserNotifications.SendEmailNotification_AutoNotification(rowTo, rowFrom)

                ElseIf (notif = NotificationType.AutoNotificationForPhoto AndAlso rowTo.NotificationsSettings_WhenNewMembersNearMe = True) Then
                    success = clsUserNotifications.SendEmailNotification_AutoNotificationForPhoto(rowTo, rowFrom)

                End If

            End If 'rowTo.Status = ProfileStatusEnum.Approved

        Catch ex As Exception
            'Throw
        End Try

        Return success
    End Function


    Private Shared Function ReplaceProfileInfo(text As String, ByRef rowTo As EUS_ProfilesRow, ByRef rowFrom As EUS_ProfilesRow, ageWord As String) As String
        If (Not String.IsNullOrEmpty(text)) Then
            Dim login As String = rowFrom.LoginName

            Dim country As String = ""
            If (Not rowFrom.IsCountryNull()) Then country = rowFrom.Country

            Dim city As String = ""
            If (Not rowFrom.IsCityNull()) Then city = rowFrom.City

            Dim birthday As Date = Date.MinValue
            If (Not rowFrom.IsBirthdayNull()) Then birthday = rowFrom.Birthday

            Dim countryTo As String = ""
            If (Not rowTo.IsCountryNull()) Then countryTo = rowTo.Country

            Dim result As String = login
            If (birthday > Date.MinValue) Then
                If (Not String.IsNullOrEmpty(ageWord)) Then
                    result = result & ", " & ageWord.Replace("###AGE###", ProfileHelper.GetCurrentAge(birthday))
                Else
                    result = result & ", " & ProfileHelper.GetCurrentAge(birthday)
                End If
            End If
            If (countryTo <> country AndAlso country.Length > 0) Then
                result = result & ", " & ProfileHelper.GetCountryName(country)
            End If
            If (city.Length > 0) Then
                result = result & ", " & city
            End If
            text = text.Replace("[PROFILE-INFO]", result)
        End If
        Return text
    End Function


    Private Shared Function MessageBodyReplaceTokens(body As String, ByRef rowTo As EUS_ProfilesRow, ByRef rowFrom As EUS_ProfilesRow) As String

        body = Regex.Replace(body, "###RECIPIENTLOGINNAME###", rowTo.LoginName)
        body = Regex.Replace(body, "###LOGINNAME###", rowFrom.LoginName)
        body = Regex.Replace(body, "###LOGINNAME_ENC###", System.Web.HttpUtility.UrlEncode(rowFrom.LoginName))


        '   Dim photosGenderID As Integer = 2
        If (ProfileHelper.IsFemale(rowFrom.GenderId)) Then
            ' photosGenderID = 1
            body = body.Replace("[MY-GENDER]", "my-gender-female")
        Else
            '   photosGenderID = 2
            body = body.Replace("[MY-GENDER]", "my-gender-male")
        End If

        Try
            Dim view_profile_url As String = clsConfigValues.GetSYS_ConfigValue("view_profile_url")
            '     Dim image_thumb_url As String = clsConfigValues.GetSYS_ConfigValue("image_thumb_url")
            Dim site_name As String = clsConfigValues.GetSYS_ConfigValue("site_name")


            Dim PROFILE_LOGIN As String = rowFrom.LoginName
            Dim PROFILE_URL As String = view_profile_url.Replace("{0}", HttpUtility.UrlEncode(PROFILE_LOGIN))
            Dim PROFILE_PHOTO_URL As String

            Dim fileName As String = ""
            Dim dr As DSMembers.EUS_CustomerPhotosRow = DataHelpers.GetProfilesDefaultPhoto(rowFrom.ProfileID)
            If (dr IsNot Nothing) Then
                fileName = dr.FileName
                Dim src As String = ProfileHelper.GetProfileImageURL(rowFrom.ProfileID, fileName, rowFrom.GenderId, True, False, PhotoSize.Thumb)
                PROFILE_PHOTO_URL = src
            Else
                Dim src As String = ProfileHelper.GetProfileImageURL(rowFrom.ProfileID, fileName, rowFrom.GenderId, True, False, PhotoSize.Thumb)
                PROFILE_PHOTO_URL = (site_name & src.Trim("/"))
            End If

            body = body.Replace("[PROFILE-URL1]", PROFILE_URL)
            body = body.Replace("[PROFILE-LOGIN1]", PROFILE_LOGIN)
            body = body.Replace("[PROFILE-PHOTO-URL1]", PROFILE_PHOTO_URL)

        Catch ex As Exception
            'Throw New Exception(ex.Message, ex)
        Finally
        End Try
        Return body
    End Function


    Public Shared Function SendEmailNotification_AutoNotificationForPhoto(rowTo As EUS_ProfilesRow, rowFrom As EUS_ProfilesRow) As Boolean
        Dim success As Boolean = False
        Dim insertToEmailsQueue As Boolean = True
        Try

            Dim photo As DSMembers.EUS_CustomerPhotosRow = DataHelpers.GetProfilesDefaultPhoto(rowFrom.ProfileID)
            If (photo Is Nothing) Then
                Return success
            End If


            Dim o As New clsSiteLAG()
            Dim config As New clsConfigValues()
            '   Dim site_name As String = config.site_name
            Dim image_thumb_url As String = config.image_thumb_url
            '     Dim image_url As String = config.image_url

            Dim subject As String = o.GetCustomStringFromPage(rowTo.LAGID, "GlobalStrings", "NotificationEmailForAutoNotificationSubject", DataHelpers.ConnectionString)
            Dim body As String = o.GetCustomStringFromPage(rowTo.LAGID, "GlobalStrings", "NotificationEmailForAutoNotificationPhoto", DataHelpers.ConnectionString)

            body = Regex.Replace(body, "###RECIPIENTLOGINNAME###", rowTo.LoginName)
            body = Regex.Replace(body, "###LOGINNAME###", rowFrom.LoginName)


            ' get default photo
            Try
                Dim photoFileName As String = ""

                'Dim fileName As String = ""
                'fileName = photo.FileName
                'photoFileName = ProfileHelper.GetProfileImageURL(rowFrom.ProfileID, fileName, rowFrom.GenderId, True)

                photoFileName = String.Format(image_thumb_url, rowFrom.ProfileID, photo.FileName)

                body = Regex.Replace(body, "blank###DEFAULTPHOTO###", photoFileName) ' cms editor issue, adds word blank
                body = Regex.Replace(body, "###DEFAULTPHOTO###", photoFileName)
            Catch ex As Exception

            End Try

            ' get default photo
            Try
                Dim height As String = ProfileHelper.GetHeightString(rowFrom.PersonalInfo_HeightID, rowTo.LAGID)
                If (String.IsNullOrEmpty(height)) Then height = "-"
                body = Regex.Replace(body, "###HEIGHT###", height)
            Catch ex As Exception

            End Try

            ' GetBodyTypeString
            Try
                Dim str As String = ProfileHelper.GetBodyTypeString(rowFrom.PersonalInfo_BodyTypeID, rowTo.LAGID)
                If (String.IsNullOrEmpty(str)) Then str = "-"
                body = Regex.Replace(body, "###BODYTYPE###", str)
            Catch ex As Exception

            End Try

            ' GetEyeColorString
            Try
                Dim str As String = ProfileHelper.GetEyeColorString(rowFrom.PersonalInfo_EyeColorID, rowTo.LAGID)
                If (String.IsNullOrEmpty(str)) Then str = "-"
                body = Regex.Replace(body, "###EYECOLOR###", str)
            Catch ex As Exception

            End Try

            ' GetHairColorString
            Try
                Dim str As String = ProfileHelper.GetHairColorString(rowFrom.PersonalInfo_HairColorID, rowTo.LAGID)
                If (String.IsNullOrEmpty(str)) Then str = "-"
                body = Regex.Replace(body, "###HAIRCOLOR###", str)
            Catch ex As Exception

            End Try

            ' get default photo
            Try
                Dim age As Integer = ProfileHelper.GetCurrentAge(rowFrom.Birthday)
                body = Regex.Replace(body, "###AGE###", age.ToString())
            Catch ex As Exception

            End Try

            'clsMyMail.SendMail(rowTo.eMail, subject, body, True)
            Dim hdl As New Core.DLL.clsSendingEmailsHandler()
            Try
                If (insertToEmailsQueue) Then
                    'If (Not hdl.CheckEmailIfExists(rowTo.eMail, subject, rowTo.ProfileID, "AutoNotification")) Then
                    clsSendingEmailsHandler.AddEmail(rowTo.eMail, subject, body, rowTo.ProfileID, True, "AutoNotification", Nothing, Nothing)
                    'End If
                Else
                    clsMyMail.SendMail(rowTo.eMail, subject, body, True)
                End If
            Catch ex As Exception
                Throw
            Finally
                hdl.Dispose()
            End Try

            success = True
        Catch ex As Exception
            'WebErrorMessageBox(Me, ex, "")
        End Try

        Return success
    End Function



    Public Shared Function SendEmailNotification_AutoNotification(rowTo As EUS_ProfilesRow, rowFrom As EUS_ProfilesRow) As Boolean
        Dim success As Boolean = False
        Dim insertToEmailsQueue As Boolean = True
        Try
            Dim o As New clsSiteLAG()

            Dim subject As String = o.GetCustomStringFromPage(rowTo.LAGID, "GlobalStrings", "NotificationEmailForAutoNotificationSubject", DataHelpers.ConnectionString)
            Dim body As String = o.GetCustomStringFromPage(rowTo.LAGID, "GlobalStrings", "NotificationEmailForAutoNotification", DataHelpers.ConnectionString)

            body = Regex.Replace(body, "###RECIPIENTLOGINNAME###", rowTo.LoginName)
            body = Regex.Replace(body, "###LOGINNAME###", rowFrom.LoginName)


            ' get default photo
            Try
                Dim photoFileName As String = ""
                Dim fileName As String = ""
                Dim photo As DSMembers.EUS_CustomerPhotosRow = DataHelpers.GetProfilesDefaultPhoto(rowFrom.ProfileID)
                If (photo IsNot Nothing) Then
                    fileName = photo.FileName
                End If
                photoFileName = ProfileHelper.GetProfileImageURL(rowFrom.ProfileID, fileName, rowFrom.GenderId, True, False)
                body = Regex.Replace(body, "###DEFAULTPHOTO###", photoFileName)
            Catch ex As Exception

            End Try

            ' get default photo
            Try
                Dim height As String = ProfileHelper.GetHeightString(rowFrom.PersonalInfo_HeightID, rowTo.LAGID)
                If (String.IsNullOrEmpty(height)) Then height = "-"
                body = Regex.Replace(body, "###HEIGHT###", height)
            Catch ex As Exception

            End Try

            ' GetBodyTypeString
            Try
                Dim str As String = ProfileHelper.GetBodyTypeString(rowFrom.PersonalInfo_BodyTypeID, rowTo.LAGID)
                If (String.IsNullOrEmpty(str)) Then str = "-"
                body = Regex.Replace(body, "###BODYTYPE###", str)
            Catch ex As Exception

            End Try

            ' GetEyeColorString
            Try
                Dim str As String = ProfileHelper.GetEyeColorString(rowFrom.PersonalInfo_EyeColorID, rowTo.LAGID)
                If (String.IsNullOrEmpty(str)) Then str = "-"
                body = Regex.Replace(body, "###EYECOLOR###", str)
            Catch ex As Exception

            End Try

            ' GetHairColorString
            Try
                Dim str As String = ProfileHelper.GetHairColorString(rowFrom.PersonalInfo_HairColorID, rowTo.LAGID)
                If (String.IsNullOrEmpty(str)) Then str = "-"
                body = Regex.Replace(body, "###HAIRCOLOR###", str)
            Catch ex As Exception

            End Try

            ' get default photo
            Try
                Dim age As Integer = ProfileHelper.GetCurrentAge(rowFrom.Birthday)
                body = Regex.Replace(body, "###AGE###", age.ToString())
            Catch ex As Exception

            End Try

            'clsMyMail.SendMail(rowTo.eMail, subject, body, True)
            Using hdl As New Core.DLL.clsSendingEmailsHandler()


                Try
                    If (insertToEmailsQueue) Then
                        'If (Not hdl.CheckEmailIfExists(rowTo.eMail, subject, rowTo.ProfileID, "AutoNotification")) Then
                        clsSendingEmailsHandler.AddEmail(rowTo.eMail, subject, body, rowTo.ProfileID, True, "AutoNotification", Nothing, Nothing)
                        'End If
                    Else
                        clsMyMail.SendMail(rowTo.eMail, subject, body, True)
                    End If
                Catch ex As Exception
                    Throw
               
                End Try
            End Using
            success = True
        Catch ex As Exception
            'WebErrorMessageBox(Me, ex, "")
        End Try

        Return success
    End Function



    Public Shared Sub SendEmailNotification_OnAdminApprove(profile As EUS_ProfilesRow, lagid As String)
        Try
            Dim toEmail As String = profile.eMail
            Dim subject As String = ""
            Dim Content As String = ""
            Dim o As New clsSiteLAG()

            subject = o.GetCustomStringFromPage(lagid, "GlobalStrings", "RegistrationMessageTextSubject", DataHelpers.ConnectionString)
            Content = o.GetCustomStringFromPage(lagid, "GlobalStrings", "RegistrationMessageText2", DataHelpers.ConnectionString)
            Content = Content.Replace("[LOGINNAME]", profile.LoginName)
            Content = Content.Replace("[PASSWORD]", profile.Password)

            Dim photosGenderID As Integer = 2
            If (ProfileHelper.IsFemale(profile.GenderId)) Then
                photosGenderID = 1
                Content = Content.Replace("[MY-GENDER]", "my-gender-female")
            Else
                photosGenderID = 2
                Content = Content.Replace("[MY-GENDER]", "my-gender-male")
            End If

            Using dt As DataTable = DataHelpers.GetProofiles_ForRegistrationEmail(profile.Country, photosGenderID)



                Try
                    Dim view_profile_url As String = clsConfigValues.GetSYS_ConfigValue("view_profile_url")
                    Dim image_thumb_url As String = clsConfigValues.GetSYS_ConfigValue("image_thumb_url")

                    Dim checkList As New List(Of Integer)
                    Dim counter As Integer = 1
                    For cnt = 0 To dt.Rows.Count - 1

                        Dim dr As DataRow = dt.Rows(cnt)
                        'Do While myReader.Read()

                        Dim CustomerID As Integer = dr(0)
                        If (checkList.Contains(CustomerID)) Then
                            Continue For
                        End If
                        checkList.Add(CustomerID)

                        Dim PROFILE_LOGIN As String = dr("LoginName").ToString()
                        Dim PROFILE_URL As String = view_profile_url.Replace("{0}", HttpUtility.UrlEncode(PROFILE_LOGIN))
                        Dim PROFILE_PHOTO_URL As String = image_thumb_url.Replace("{0}", dr("CustomerID")).Replace("{1}", dr("FileName"))

                        Content = Content.Replace("[PROFILE-URL" & counter & "]", PROFILE_URL)
                        Content = Content.Replace("[PROFILE-LOGIN" & counter & "]", PROFILE_LOGIN)
                        Content = Content.Replace("[PROFILE-PHOTO-URL" & counter & "]", PROFILE_PHOTO_URL)

                        counter = counter + 1

                        'stis 4 photos exit
                        If (counter > 4) Then Exit For
                    Next

                Catch ex As Exception
                    'Throw New Exception(ex.Message, ex)
                Finally
                End Try
            End Using
            Dim topContent As String = <h><![CDATA[
<html>
<head>
    <title></title>
</head>
<body>
]]></h>
            Dim bottomContent As String = <h><![CDATA[
</body>
</html>
]]></h>


            Content = topContent & Content & bottomContent
            clsMyMail.SendMailFromQueue(toEmail, subject, Content, profile.ProfileID, "AdminApprove", True, Nothing, Nothing)

        Catch ex As Exception
            'WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Public Shared Sub SendEmailNotification_OnAdminRejectedNewProfile(profile As EUS_ProfilesRow, lagid As String, subject As String, content As String)
        Try
            Dim toEmail As String = profile.eMail
            '    Dim o As New clsSiteLAG()

            'subject = o.GetCustomStringFromPage(lagid, "GlobalStrings", "RegistrationMessageTextSubject", DataHelpers.ConnectionString)
            'content = o.GetCustomStringFromPage(lagid, "GlobalStrings", "RegistrationMessageText", DataHelpers.ConnectionString)
            'content = content.Replace("###LOGINNAME###", profile.LoginName)
            'content = content.Replace("###PASSWORD###", profile.Password)

            'clsMyMail.SendMail(toEmail, subject, content, True)
            clsMyMail.SendMailFromQueue(toEmail, subject, content, profile.ProfileID, "AdminRejectedNewProfile", True, Nothing, Nothing)

        Catch ex As Exception
            'WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Public Shared Sub SendEmailNotification_OnAdminRejectedProfileChanges(profile As EUS_ProfilesRow, lagid As String, subject As String, content As String)
        Try
            Dim toEmail As String = profile.eMail
            '   Dim o As New clsSiteLAG()

            'subject = o.GetCustomStringFromPage(lagid, "GlobalStrings", "RegistrationMessageTextSubject", DataHelpers.ConnectionString)
            'content = o.GetCustomStringFromPage(lagid, "GlobalStrings", "RegistrationMessageText", DataHelpers.ConnectionString)
            'content = content.Replace("###LOGINNAME###", profile.LoginName)
            'content = content.Replace("###PASSWORD###", profile.Password)

            'clsMyMail.SendMail(toEmail, subject, content, True)
            clsMyMail.SendMailFromQueue(toEmail, subject, content, profile.ProfileID, "AdminRejectedProfileChanges", True, Nothing, Nothing)

        Catch ex As Exception
            'WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub



    Public Shared Function SendNotificationsToMembers(sendMessageParams As SendMessageToProfilesParams, Optional insertToEmailsQueue As Boolean = False) As String

        Dim sb As New StringBuilder
        '   Dim o As New clsSiteLAG()
        Using hdl As New Core.DLL.clsSendingEmailsHandler()



            Dim resultCounter As Integer


            Try

                Using dc = New CMSDBDataContext(DataHelpers.ConnectionString)



                    Dim SYS_EmailMessagesRow As UniCMSDB.SYS_EmailMessagesRow = sendMessageParams.GetMessageRow()
                    '    Dim config As New clsConfigValues
                    Dim cnt As Integer


                    For cnt = 0 To sendMessageParams.ProfilesList.Count - 1
                        Try
                            Dim _EmailParams As New EmailParams()

                            Dim _profileid As Integer = sendMessageParams.ProfilesList(cnt)
                            Dim prof = (From itm In dc.EUS_Profiles
                                        Where itm.ProfileID = _profileid
                                        Select itm.eMail, itm.LAGID, itm.ProfileID, itm.LoginName).FirstOrDefault()

                            If (prof IsNot Nothing) Then

                                _EmailParams.EmailAddress = prof.eMail
                                _EmailParams.LagID = prof.LAGID
                                _EmailParams.ProfileID = prof.ProfileID
                                _EmailParams.LoginName = prof.LoginName
                                _EmailParams.BCCRecipients = sendMessageParams.BCCRecipients
                                sendMessageParams.EmailsList.Add(_EmailParams)

                            End If

                        Catch ex As Exception
                            sb.AppendLine(ex.ToString())
                        End Try
                    Next



                    For cnt = 0 To sendMessageParams.EmailsList.Count - 1
                        Try
                            Dim _EmailParams As EmailParams = sendMessageParams.EmailsList(cnt)

                            If (Not String.IsNullOrEmpty(_EmailParams.EmailAddress)) Then 'AndAlso row.eMail = "love@goomena.com"

                                Dim subject As String = ""
                                Dim body As String = ""
                                Dim MessageAction As String = ""

                                If (SYS_EmailMessagesRow.MessageAction.ToString() <> "") Then
                                    MessageAction = SYS_EmailMessagesRow.MessageAction
                                End If

                                If (SYS_EmailMessagesRow("Subject" & _EmailParams.LagID).ToString() <> "") Then
                                    subject = SYS_EmailMessagesRow("Subject" & _EmailParams.LagID)
                                Else
                                    subject = SYS_EmailMessagesRow("SubjectUS")
                                End If


                                If (SYS_EmailMessagesRow("MessageText" & _EmailParams.LagID).ToString() <> "") Then
                                    body = SYS_EmailMessagesRow("MessageText" & _EmailParams.LagID)
                                Else
                                    body = SYS_EmailMessagesRow("MessageTextUS")
                                End If

                                If (String.IsNullOrEmpty(MessageAction)) Then
                                    MessageAction = subject
                                End If


                                ' send email message
                                If (sendMessageParams.SendEmail) Then
                                    If (insertToEmailsQueue) Then
                                        clsSendingEmailsHandler.AddEmail(_EmailParams.EmailAddress,
                                                     subject,
                                                     body,
                                                     _EmailParams.ProfileID,
                                                     True,
                                                     MessageAction,
                                                     _EmailParams.SMTPServerCredentialsID,
                                                     _EmailParams.BCCRecipients)
                                        sb.AppendLine("Notification " & MessageAction & " added to queue for " & _EmailParams.EmailAddress & " - login """ & _EmailParams.LoginName & """")
                                        'If (MessageAction <> "Custom Message" AndAlso Not hdl.CheckEmailIfExists(_EmailParams.EmailAddress, subject, _EmailParams.ProfileID, MessageAction)) Then
                                        'Else
                                        '    sb.AppendLine("Notification " & MessageAction & " already exists for " & _EmailParams.EmailAddress & " - login """ & _EmailParams.LoginName & """")
                                        'End If
                                        resultCounter = resultCounter + 1
                                    Else
                                        clsMyMail.TrySendMail(_EmailParams.EmailAddress, subject, body, True, _EmailParams.BCCRecipients)
                                        sb.AppendLine("Notification " & MessageAction & " sent to " & _EmailParams.EmailAddress & " - login """ & _EmailParams.LoginName & """")
                                        resultCounter = resultCounter + 1
                                    End If
                                End If


                                ' send application message
                                If (sendMessageParams.SendApplicationMessage) Then
                                    clsUserDoes.SendMessageFromAdministrator(subject, body, _EmailParams.ProfileID)
                                End If

                            Else
                                sb.AppendLine("Email not specified for - login """ & _EmailParams.LoginName & """")
                            End If

                        Catch ex As Exception
                            sb.AppendLine(ex.ToString())
                        End Try
                    Next
                End Using
            Catch ex As Exception
                sb.AppendLine(ex.ToString())


            End Try

            sb.Insert(0, "Profiles processed :" & resultCounter.ToString() & vbCrLf & vbCrLf)
        End Using
        Return sb.ToString()
    End Function




    Public Shared Function SendNotificationsToMembers_OLD(sendMessageParams As SendMessageToProfilesParams, Optional insertToEmailsQueue As Boolean = False) As String

        Dim sb As New StringBuilder
        '   Dim o As New clsSiteLAG()
        Dim resultCounter As Integer
        Using hdl As New Core.DLL.clsSendingEmailsHandler()





            Try

                Using dc = New CMSDBDataContext(DataHelpers.ConnectionString)



                    Dim SYS_EmailMessagesRow As UniCMSDB.SYS_EmailMessagesRow = sendMessageParams.GetMessageRow()
                    '   Dim config As New clsConfigValues
                    Dim cnt As Integer

                    For cnt = 0 To sendMessageParams.ProfilesList.Count - 1
                        Try
                            Dim _profileid As Integer = sendMessageParams.ProfilesList(cnt)
                            Dim prof = (From itm In dc.EUS_Profiles
                                        Where itm.ProfileID = _profileid
                                        Select itm.eMail, itm.LAGID, itm.ProfileID, itm.LoginName).FirstOrDefault()

                            'OrElse itm.MirrorProfileID = profileid
                            'ta.FillByProfileID(dsMem.EUS_Profiles, sendMessageParams.ProfilesList(cnt))

                            'Dim row As DSMembers.EUS_ProfilesRow = dsMem.EUS_Profiles.Rows(0)

                            If (Not String.IsNullOrEmpty(prof.eMail)) Then 'AndAlso row.eMail = "love@goomena.com"

                                Dim subject As String = ""
                                Dim body As String = ""
                                Dim MessageAction As String = ""

                                If (SYS_EmailMessagesRow.MessageAction.ToString() <> "") Then
                                    MessageAction = SYS_EmailMessagesRow.MessageAction
                                End If

                                If (SYS_EmailMessagesRow("Subject" & prof.LAGID).ToString() <> "") Then
                                    subject = SYS_EmailMessagesRow("Subject" & prof.LAGID)
                                Else
                                    subject = SYS_EmailMessagesRow("SubjectUS")
                                End If


                                If (SYS_EmailMessagesRow("MessageText" & prof.LAGID).ToString() <> "") Then
                                    body = SYS_EmailMessagesRow("MessageText" & prof.LAGID)
                                Else
                                    body = SYS_EmailMessagesRow("MessageTextUS")
                                End If

                                If (String.IsNullOrEmpty(MessageAction)) Then
                                    MessageAction = subject
                                End If


                                ' send email message
                                If (sendMessageParams.SendEmail) Then
                                    If (insertToEmailsQueue) Then
                                        If (Not clsSendingEmailsHandler.CheckEmailIfExists(prof.eMail, subject, prof.ProfileID, MessageAction)) Then
                                            clsSendingEmailsHandler.AddEmail(prof.eMail, subject, body, prof.ProfileID, True, MessageAction, Nothing, sendMessageParams.BCCRecipients)
                                            sb.AppendLine("Notification " & MessageAction & " added to queue for " & prof.eMail & " - login """ & prof.LoginName & """")
                                        Else
                                            sb.AppendLine("Notification " & MessageAction & " already exists for " & prof.eMail & " - login """ & prof.LoginName & """")
                                        End If
                                        resultCounter = resultCounter + 1
                                    Else
                                        clsMyMail.TrySendMail(prof.eMail, subject, body, True)
                                        sb.AppendLine("Notification " & MessageAction & " sent to " & prof.eMail & " - login """ & prof.LoginName & """")
                                        resultCounter = resultCounter + 1
                                    End If
                                End If


                                ' send application message
                                If (sendMessageParams.SendApplicationMessage) Then
                                    clsUserDoes.SendMessageFromAdministrator(subject, body, prof.ProfileID)
                                End If

                            Else
                                sb.AppendLine("Email not specified for - login """ & prof.LoginName & """")
                            End If

                        Catch ex As Exception
                            sb.AppendLine(ex.ToString())
                        End Try
                    Next

                End Using
            Catch ex As Exception
                sb.AppendLine(ex.ToString())
           
            End Try
        End Using
        sb.Insert(0, "Profiles processed :" & resultCounter.ToString() & vbCrLf & vbCrLf)

        Return sb.ToString()
    End Function


    Public Shared Function SendNotificationsToMembers_WithoutPhoto(Optional insertToEmailsQueue As Boolean = False) As String

        Dim sb As New StringBuilder
        Dim o As New clsSiteLAG()

        Dim resultCounter As Integer
        Using dsMem As New DSMembers()


            Try


                Dim isreferrer As Boolean? = Nothing
                Using con As SqlClient.SqlConnection = New SqlClient.SqlConnection(DataHelpers.ConnectionString)
                    Using ta As New DSMembersTableAdapters.EUS_ProfilesTableAdapter
                        ta.Connection = con
                        ta.FillBy_Approved_HasOrNotPhotos(dsMem.EUS_Profiles, False, True, Nothing, Nothing, isreferrer)
                    End Using
                End Using



                '    Dim tbl As DSMembers.EUS_ProfilesDataTable = dsMem.EUS_Profiles
                Dim cnt As Integer

                Dim config As New clsConfigValues
                For cnt = 0 To dsMem.EUS_Profiles.Rows.Count - 1
                    Dim row As DSMembers.EUS_ProfilesRow = dsMem.EUS_Profiles.Rows(cnt)
                    If (Not row.IseMailNull()) Then 'AndAlso row.eMail = "love@goomena.com"
                        Dim subject As String = o.GetCustomStringFromPage(row.LAGID, "GlobalStrings", "NotificationEmailForMemberWithoutPhotoSubject", DataHelpers.ConnectionString)
                        Dim body As String = ""

                        If (ProfileHelper.IsFemale(row.GenderId)) Then
                            body = o.GetCustomStringFromPage(row.LAGID, "GlobalStrings", "NotificationEmailForMemberWithoutPhoto_FEMALE", DataHelpers.ConnectionString)
                        Else
                            body = o.GetCustomStringFromPage(row.LAGID, "GlobalStrings", "NotificationEmailForMemberWithoutPhoto_MALE", DataHelpers.ConnectionString)
                        End If
                        body = body.Replace("###LOGINNAME###", row.LoginName)


                        If (body.Contains("###GIRLPHOTOS###")) Then
                            Try
                                Dim photosListHtml As String = RenderPhotosHTML(row, config)
                                body = body.Replace("###GIRLPHOTOS###", photosListHtml)
                            Catch ex As Exception
                                sb.AppendLine(ex.ToString())
                            End Try

                        End If


                        If (insertToEmailsQueue) Then
                            Using hdl As New Core.DLL.clsSendingEmailsHandler
                                If (Not clsSendingEmailsHandler.CheckEmailIfExists(row.eMail, subject, row.ProfileID, "No Photo Notification")) Then
                                    clsSendingEmailsHandler.AddEmail(row.eMail, subject, body, row.ProfileID, True, "No Photo Notification", Nothing, Nothing)
                                End If
                            End Using
                            resultCounter = resultCounter + 1
                        Else
                            clsMyMail.TrySendMail(row.eMail, subject, body, True)
                            sb.AppendLine("Notification ""Without Photo"" sent to " & row.eMail & " - login """ & row.LoginName & """")
                            resultCounter = resultCounter + 1
                        End If

                    End If
                Next


            Catch ex As Exception
                'Throw
                sb.AppendLine(ex.ToString())

            End Try
        End Using
        sb.Insert(0, "Profiles processed :" & resultCounter.ToString() & vbCrLf & vbCrLf)

        Return sb.ToString()
    End Function

    Shared Function RenderPhotosHTML(row As DSMembers.EUS_ProfilesRow, config As clsConfigValues) As String

        Dim photosListHtml As New StringBuilder()
        photosListHtml.AppendLine("<div>")

        Dim prms As New clsSearchHelperParameters()
        prms.CurrentProfileId = row.ProfileID
        prms.ReturnRecordsWithStatus = ProfileStatusEnum.Approved
        prms.SearchSort = SearchSortEnum.NearestDistance
        prms.NumberOfRecordsToReturn = 10
        prms.rowNumberMin = 0
        prms.rowNumberMax = prms.NumberOfRecordsToReturn

        If (Not row.IsZipNull()) Then prms.zipstr = row.Zip
        If (Not row.IslatitudeNull()) Then prms.latitudeIn = row.latitude
        If (Not row.IslongitudeNull()) Then prms.longitudeIn = row.longitude
        If (Not row.IsLookingFor_ToMeetMaleIDNull()) Then prms.LookingFor_ToMeetMaleID = row.LookingFor_ToMeetMaleID
        If (Not row.IsLookingFor_ToMeetFemaleIDNull()) Then prms.LookingFor_ToMeetFemaleID = row.LookingFor_ToMeetFemaleID
        prms.performCount = False

        Dim count As Integer
        Using ds As DataSet = clsSearchHelper.GetMembersToSearchDataTable(prms)
            Using dt As DataTable = ds.Tables(0)

       
      
        If (dt.Rows.Count > 0) Then
            For Each dr As DataRow In dt.Rows

                count = count + 1
                If count > 5 Then
                    Exit For
                End If

                Dim photoUrl As String = ""
                Dim drDefaultPhoto As EUS_CustomerPhotosRow = DataHelpers.GetProfilesDefaultPhoto(dr("ProfileID"))
                If (drDefaultPhoto IsNot Nothing) Then
                    photoUrl = String.Format(config.image_thumb_url, drDefaultPhoto.CustomerID, drDefaultPhoto.FileName)
                Else
                    Continue For
                End If

                Dim atributes As New Dictionary(Of String, String)
                atributes.Add("border", "0")
                atributes.Add("alt", dr("LoginName"))
                atributes.Add("width", "100")
                atributes.Add("title", dr("LoginName"))
                Dim photoHtml As String = clsHTMLHelper.RenderImageHtml(photoUrl, atributes)

                Dim profileUrl As String = String.Format(config.view_profile_url, dr("LoginName"))
                Dim linkHtml As String = clsHTMLHelper.RenderLinkHtml(profileUrl, photoHtml)

                photosListHtml.AppendLine(linkHtml & "&nbsp;")
            Next
        End If
            End Using
        End Using
        photosListHtml.AppendLine("</div>")
        Return photosListHtml.ToString()
    End Function


    Public Shared Sub SendNotificationsToMembers_AboutNewMember(customerid As Integer, photoUploaded As Boolean)
        Try
            If (photoUploaded) Then

                Dim OnUpdateAutoNotificationSentForPhoto As Boolean = GetEUS_Profiles_OnUpdateAutoNotificationSentForPhoto(customerid)

                If (Not OnUpdateAutoNotificationSentForPhoto) Then
                    Using dt As DataTable = clsSearchHelper.GetMembersThatShouldBeNotifiedForNewMember(customerid)


                        If (dt.Rows.Count > 0) Then

                            Dim cnt As Integer
                            Dim isAnySent As Boolean = False


                            For cnt = 0 To dt.Rows.Count - 1
                                Dim success As Boolean = False
                                Dim toProfileId As Integer = dt.Rows(cnt)("CustomerID")

                                success = clsUserNotifications.SendEmailNotification(NotificationType.AutoNotificationForPhoto, customerid, toProfileId)
                                If (success) Then isAnySent = True
                            Next


                            If (isAnySent) Then
                                UpdateEUS_Profiles_OnUpdateAutoNotificationSentForPhoto(customerid, True)
                            End If

                        End If
                    End Using
                End If

            Else

                Dim OnUpdateAutoNotificationSent As Boolean = GetEUS_Profiles_OnUpdateAutoNotificationSent(customerid)

                If (Not OnUpdateAutoNotificationSent) Then
                    Using dt As DataTable = clsSearchHelper.GetMembersThatShouldBeNotifiedForNewMember(customerid)


                        If (dt.Rows.Count > 0) Then

                            Dim cnt As Integer
                            Dim isAnySent As Boolean = False


                            For cnt = 0 To dt.Rows.Count - 1
                                Dim success As Boolean = False
                                Dim toProfileId As Integer = dt.Rows(cnt)("CustomerID")

                                success = clsUserNotifications.SendEmailNotification(NotificationType.AutoNotification, customerid, toProfileId)

                                If (success) Then isAnySent = True
                            Next


                            If (isAnySent) Then
                                UpdateEUS_Profiles_OnUpdateAutoNotificationSent(customerid, True)
                            End If

                        End If
                    End Using
                End If

            End If


        Catch ex As Exception
            Throw
        End Try

    End Sub




    Public Shared Function EUS_UserMessages_Add(ByVal CustomerID As Integer, msg As String, Optional position As String = Nothing) As EUS_UserMessage
        Dim drv As New EUS_UserMessage
        Try
            Using ctx As New CMSDBDataContext(DataHelpers.ConnectionString)
                drv.CustomerID = CustomerID
                drv.MessageUS = msg
                drv.DateCreated = Date.UtcNow
                drv.Position = position
                ctx.EUS_UserMessages.InsertOnSubmit(drv)
                ctx.SubmitChanges()
            End Using
        Catch ex As Exception
            Throw
        End Try
        Return drv
    End Function

    Public Shared Sub EUS_UserMessages_Clear(CustomerID As Integer, Optional position As String = Nothing)
        'Dim ROWCOUNT As Integer
        'Dim sql As String = "delete from EUS_UserMessages where CustomerID=@CustomerID"
        'sql = sql.Replace("@CustomerID", CustomerID)
        'ROWCOUNT = DataHelpers.ExecuteNonQuery(sql)
        'Return ROWCOUNT

        Using ctx As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try
                Dim linq = From itm In ctx.EUS_UserMessages
                            Where itm.CustomerID = CustomerID AndAlso (itm.Position = position OrElse (itm.Position Is Nothing AndAlso position Is Nothing))
                            Select itm
                ctx.EUS_UserMessages.DeleteAllOnSubmit(linq)
                ctx.SubmitChanges()
            Catch ex As Exception
                Throw
        
            End Try
        End Using
    End Sub


    'Public Shared Function EUS_UserMessages_GetAll(CustomerID As Integer, Optional position As String = Nothing) As DataTable
    '    Dim ctx As New CMSDBDataContext(DataHelpers.ConnectionString)
    '    Dim linq As New DataTable()
    '    Try
    '        linq = (From itm In ctx.EUS_UserMessages
    '                    Where itm.CustomerID = CustomerID AndAlso (itm.Position = position OrElse (itm.Position Is Nothing AndAlso position Is Nothing))
    '                    Select itm).LINQToDataTable()
    '    Catch ex As Exception
    '        Throw
    '    Finally
    '        ctx.Dispose()
    '    End Try
    '    Return linq
    'End Function

    Public Shared Function EUS_UserMessages_IsExist(CustomerID As Integer, Optional position As String = Nothing) As Boolean
        Using ctx As New CMSDBDataContext(DataHelpers.ConnectionString)


            Dim linq As Integer
            Try
                linq = (From itm In ctx.EUS_UserMessages
                            Where itm.CustomerID = CustomerID AndAlso (itm.Position = position OrElse (itm.Position Is Nothing AndAlso position Is Nothing))
                            Select itm).Count()
            Catch ex As Exception
                Throw
           
            End Try
            Return (linq > 0)
        End Using

    End Function

    Public Shared Function EUS_UserMessages_GetByPosition(CustomerID As Integer, position As String) As EUS_UserMessage
        Using ctx As New CMSDBDataContext(DataHelpers.ConnectionString)


            Dim linq As EUS_UserMessage = Nothing
            Try
                linq = (From itm In ctx.EUS_UserMessages
                            Where itm.CustomerID = CustomerID AndAlso (itm.Position = position)
                            Select itm).FirstOrDefault()
            Catch ex As Exception
                Throw
         
            End Try
            Return linq
        End Using
    End Function


End Class

﻿Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Datasets.DLL.DSMembers
Imports Dating.Server.Core.DLL
Imports System.Web
Imports System.Text.RegularExpressions
Imports System.Configuration
Imports System.Transactions



Public Class clsUserDoes

#Region "Types"

    Public Class NewOfferParameters
        Public Property userIdReceiver As Integer
        Public Property userIdWhoDid As Integer
        Public Property offerAmount As Integer
        Public Property parentOfferId As Integer
        Public Property messageText1 As String
        Public Property messageText2 As String

        Public Property childOfferId As Integer

    End Class



    Public Class clsLoadProfilesViewsResult

        Property CurrentMemberProfileID As Integer
        Property CurrentMemberProfileViewUrl As String
        Property CurrentMemberLoginName As String
        Property CurrentMemberImageUrl As String

        Property OtherMemberProfileID As Integer
        Property OtherMemberProfileViewUrl As String
        Property OtherMemberLoginName As String
        Property OtherMemberImageUrl As String

    End Class

#End Region


    Public Shared Function MarkAsViewed(userIdViewed As Integer, userIdWhoDid As Integer, Optional notifyMember As Boolean = True) As Boolean
        Dim done As Boolean = False

        If (userIdViewed = 0 OrElse userIdWhoDid = 0 OrElse userIdViewed = userIdWhoDid) Then
            Return done
        End If


        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
            Try


                done = clsUserDoes.MarkAsViewed(_CMSDBDataContext, userIdViewed, userIdWhoDid, notifyMember)


            Catch ex As Exception
                Throw New Exception(ex.Message, ex)
            Finally
                _CMSDBDataContext.Dispose()
            End Try
        End Using



        Return done
    End Function


    Public Shared Function MarkAsViewed(ctx As CMSDBDataContext, userIdViewed As Integer, userIdWhoDid As Integer, Optional notifyMember As Boolean = True) As Boolean
        Dim done As Boolean = False

        If (userIdViewed = 0 OrElse userIdWhoDid = 0 OrElse userIdViewed = userIdWhoDid) Then
            Return done
        End If

        Try

            ' check
            Dim rec = ctx.EUS_ProfilesVieweds.Where(Function(itm) itm.FromProfileID = userIdWhoDid AndAlso itm.ToProfileID = userIdViewed).Count()

            If (rec = 0) Then
                Dim _EUS_ProfilesViewedRec As New EUS_ProfilesViewed()

                _EUS_ProfilesViewedRec.DateTimeToCreate = DateTime.UtcNow
                _EUS_ProfilesViewedRec.FromProfileID = userIdWhoDid
                _EUS_ProfilesViewedRec.ToProfileID = userIdViewed


                ctx.EUS_ProfilesVieweds.InsertOnSubmit(_EUS_ProfilesViewedRec)
                ctx.SubmitChanges()

                clsUserDoes.ResetMemberCounter(MemberCountersEnum.WhoViewedMe, userIdViewed)
                'Dim sql As String = "update [EUS_ProfileCounters] set WhoViewedMe=null where ProfileID=" & userIdViewed
                'DataHelpers.ExecuteNonQuery(sql)

                If (notifyMember) Then
                    clsUserNotifications.SendEmailNotification(NotificationType.ProfileViewed, userIdWhoDid, userIdViewed)
                End If

                done = True
                clsUserDoes.Update_ProfilesCommunication(userIdWhoDid, userIdViewed, MemberActionTypeEnum.Viewed, 0)
            End If


        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        Finally
        End Try


        Return done
    End Function


    Public Shared Sub MarkAsFavorite(userIdFavorited As Integer, userIdWhoDid As Integer)

        If (userIdFavorited = 0 OrElse userIdWhoDid = 0 OrElse userIdFavorited = userIdWhoDid) Then
            Return
        End If

        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try

                ' check
                Dim rec = _CMSDBDataContext.EUS_ProfilesFavorites.Where(Function(itm) itm.FromProfileID = userIdWhoDid AndAlso _
                                                                            itm.ToProfileID = userIdFavorited).Count()

                If (rec = 0) Then
                    'If (rec Is Nothing) Then
                    Dim _EUS_ProfilesFavoriteRec As New EUS_ProfilesFavorite()
                    _EUS_ProfilesFavoriteRec.DateTimeToCreate = DateTime.UtcNow
                    _EUS_ProfilesFavoriteRec.FromProfileID = userIdWhoDid
                    _EUS_ProfilesFavoriteRec.ToProfileID = userIdFavorited


                    _CMSDBDataContext.EUS_ProfilesFavorites.InsertOnSubmit(_EUS_ProfilesFavoriteRec)
                    _CMSDBDataContext.SubmitChanges()


                    clsUserDoes.ResetMemberCounter(MemberCountersEnum.WhoFavoritedMe, userIdFavorited)
                    'Dim sql As String = "update [EUS_ProfileCounters] set WhoFavoritedMe=null where ProfileID=" & userIdFavorited
                    'DataHelpers.ExecuteNonQuery(sql)

                    clsUserDoes.Update_ProfilesCommunication(userIdWhoDid, userIdFavorited, MemberActionTypeEnum.Favorite, 0)
                End If

            Catch ex As Exception
                Throw New Exception(ex.Message, ex)
            End Try
        End Using
    End Sub


    Public Shared Sub MarkAsUnfavorite(userIdUnfavorited As Integer, userIdWhoDid As Integer)

        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try

                ' check
                Dim rec As EUS_ProfilesFavorite = _CMSDBDataContext.EUS_ProfilesFavorites.Where(Function(itm) itm.FromProfileID = userIdWhoDid AndAlso _
                                                                                                    itm.ToProfileID = userIdUnfavorited).FirstOrDefault()

                If (rec IsNot Nothing) Then
                    _CMSDBDataContext.EUS_ProfilesFavorites.DeleteOnSubmit(rec)
                    _CMSDBDataContext.SubmitChanges()


                    'Dim sql As String = "update [EUS_ProfileCounters] set WhoFavoritedMe=null where ProfileID=" & userIdUnfavorited
                    'DataHelpers.ExecuteNonQuery(sql)

                    clsUserDoes.Update_ProfilesCommunication(userIdWhoDid, userIdUnfavorited, MemberActionTypeEnum.Unfavorite, 0)
                    clsUserDoes.ResetMemberCounter(MemberCountersEnum.WhoFavoritedMe, userIdUnfavorited)
                End If

            Catch ex As Exception
                Throw New Exception(ex.Message, ex)
            End Try
        End Using
    End Sub



    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")> Public Shared Sub SharePhoto(FromProfileID As Integer, ToProfileID As Integer, Level As Integer)
        ' Dim isactive As Boolean = False
        Dim rerunTran As Boolean = False
        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try

                _SharePhoto(_CMSDBDataContext, FromProfileID, ToProfileID, Level)

            Catch ex As System.Data.SqlClient.SqlException
                rerunTran = DataHelpers.CheckSqlException(ex.Message)
                If (Not rerunTran) Then Throw
            Catch ex As Exception
                Throw
            Finally
                If (Not rerunTran) Then
                    _CMSDBDataContext.Dispose()
                End If
            End Try

            If (rerunTran) Then
                Try
                    rerunTran = False
                    _SharePhoto(_CMSDBDataContext, FromProfileID, ToProfileID, Level)

                Catch ex As System.Data.SqlClient.SqlException
                    rerunTran = DataHelpers.CheckSqlException(ex.Message)
                    If (Not rerunTran) Then Throw
                Catch ex As Exception
                    Throw
                Finally
                    If (Not rerunTran) Then
                        _CMSDBDataContext.Dispose()
                    End If
                End Try
            End If

            If (rerunTran) Then
                Try

                    _SharePhoto(_CMSDBDataContext, FromProfileID, ToProfileID, Level)

                Catch ex As Exception
                    Throw
                Finally
                    _CMSDBDataContext.Dispose()
                End Try
            End If

            Try
                clsUserDoes.ResetMemberCounter(MemberCountersEnum.WhoSharedPhotos, ToProfileID)
            Catch ex As Exception
                Throw New Exception(ex.Message, ex)
            End Try
        End Using

    End Sub

    Public Shared Sub ResetLoveHate(userIdSending As Integer)
        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try



                Dim pr As EUS_Profile = (From itm In _CMSDBDataContext.EUS_Profiles
                                                Where itm.ProfileID = userIdSending
                                                Select itm).SingleOrDefault()
                Dim mir As EUS_Profile = (From itm In _CMSDBDataContext.EUS_Profiles
                                                Where itm.MirrorProfileID = userIdSending
                                                Select itm).SingleOrDefault()
                Try


                    If pr IsNot Nothing Then
                        pr.LastVoteDateTime = DateTime.UtcNow
                        pr.VotesInLast24Hour = 0
                        ' _CMSDBDataContext.EUS_Profiles.(newOffer)
                        _CMSDBDataContext.SubmitChanges()
                    End If
                Catch ex As Exception
                End Try
                Try
                    If mir IsNot Nothing Then
                        mir.LastVoteDateTime = DateTime.UtcNow
                        mir.VotesInLast24Hour = 0
                        _CMSDBDataContext.SubmitChanges()
                    End If
                Catch ex As Exception

                End Try



            Catch ex As Exception
                Throw New Exception(ex.Message, ex)

            End Try
        End Using
    End Sub

    Public Shared Sub SendLoveHate(userIdReceiving As Integer, userIdSending As Integer, ByVal IsLoving As Boolean, ByVal IsHating As Boolean, ByVal dtStart As DateTime)

        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try
                Dim ofr As EUS_ProfilesLove = (From itm In _CMSDBDataContext.EUS_ProfilesLoves
           Where itm.ToProfileID = userIdReceiving And itm.FromProfileID = userIdSending And itm.ActionDatetime >= dtStart
             Select itm).FirstOrDefault

                If ofr Is Nothing Then

                    Dim newOffer As New EUS_ProfilesLove
                    newOffer.ActionDatetime = DateTime.UtcNow
                    newOffer.FromProfileID = userIdSending
                    newOffer.Love = IsLoving
                    newOffer.Hate = IsHating
                    newOffer.ToProfileID = userIdReceiving


                    _CMSDBDataContext.EUS_ProfilesLoves.InsertOnSubmit(newOffer)
                    _CMSDBDataContext.SubmitChanges()
                    '    Or itm.MirrorProfileID = userIdSending
                    Dim b As Integer = -1
                    Try


                        Dim pr As EUS_Profile = (From itm In _CMSDBDataContext.EUS_Profiles
                                     Where itm.ProfileID = userIdSending
                                     Select itm).SingleOrDefault()
                        If pr IsNot Nothing Then
                            pr.LastVoteDateTime = DateTime.UtcNow
                            pr.VotesInLast24Hour = pr.VotesInLast24Hour + 1
                            b = pr.VotesInLast24Hour
                            ' _CMSDBDataContext.EUS_Profiles.(newOffer)
                            _CMSDBDataContext.SubmitChanges()
                        End If
                    Catch ex As Exception
                    End Try
                    Try
                        Dim mir As EUS_Profile = (From itm In _CMSDBDataContext.EUS_Profiles
                                                       Where itm.MirrorProfileID = userIdSending
                                                       Select itm).SingleOrDefault()
                        If mir IsNot Nothing Then
                            mir.LastVoteDateTime = DateTime.UtcNow
                            mir.VotesInLast24Hour = If(b < 0, mir.VotesInLast24Hour + 1, b)
                            _CMSDBDataContext.SubmitChanges()
                        End If
                    Catch ex As Exception

                    End Try


                End If


            Catch ex As Exception
                Throw New Exception(ex.Message, ex)

            End Try
        End Using
    End Sub
    Private Shared Sub _SharePhoto(_CMSDBDataContext As CMSDBDataContext, FromProfileID As Integer, ToProfileID As Integer, Level As Integer)

        Try

            Dim phtLvl As EUS_ProfilePhotosLevel = Nothing

            phtLvl = (From itm In _CMSDBDataContext.EUS_ProfilePhotosLevels
                    Where itm.FromProfileID = FromProfileID AndAlso itm.ToProfileID = ToProfileID
                    Select itm).FirstOrDefault()


            If (Level = 0 AndAlso phtLvl IsNot Nothing) Then
                _CMSDBDataContext.EUS_ProfilePhotosLevels.DeleteOnSubmit(phtLvl)

            ElseIf (Level > 0 AndAlso phtLvl IsNot Nothing) Then
                phtLvl.PhotoLevelID = Level
                phtLvl.IsToProfileIDViewed = False
                phtLvl.DateTimeCreated = Date.UtcNow

            ElseIf (Level > 0 AndAlso phtLvl Is Nothing) Then
                phtLvl = New EUS_ProfilePhotosLevel()
                phtLvl.FromProfileID = FromProfileID
                phtLvl.PhotoLevelID = Level
                phtLvl.ToProfileID = ToProfileID
                phtLvl.DateTimeCreated = Date.UtcNow
                _CMSDBDataContext.EUS_ProfilePhotosLevels.InsertOnSubmit(phtLvl)

            End If

            _CMSDBDataContext.SubmitChanges()
            'clsUserDoes.ResetMemberCounter(MemberCountersEnum.WhoSharedPhotos, ToProfileID)


        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        Finally
        End Try

    End Sub



    Public Shared Sub MarkAsBlocked(userIdBlocked As Integer, userIdWhoDid As Integer)

        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try

                ' check
                Dim rec = _CMSDBDataContext.EUS_ProfilesBlockeds.Where(Function(itm) itm.FromProfileID = userIdWhoDid AndAlso _
                                                                           itm.ToProfileID = userIdBlocked).Count()

                If (rec = 0) Then
                    'If (rec Is Nothing) Then
                    Dim _EUS_ProfilesBlockedRec As New EUS_ProfilesBlocked()
                    _EUS_ProfilesBlockedRec.DateTimeToCreate = DateTime.UtcNow
                    _EUS_ProfilesBlockedRec.FromProfileID = userIdWhoDid
                    _EUS_ProfilesBlockedRec.ToProfileID = userIdBlocked


                    _CMSDBDataContext.EUS_ProfilesBlockeds.InsertOnSubmit(_EUS_ProfilesBlockedRec)
                    _CMSDBDataContext.SubmitChanges()
                End If

            Catch ex As Exception
                Throw New Exception(ex.Message, ex)

            End Try
        End Using
    End Sub



    Public Shared Sub MarkAsUnblocked(userIdUnblocked As Integer, userIdWhoDid As Integer)

        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try

                ' check
                Dim rec As EUS_ProfilesBlocked = _CMSDBDataContext.EUS_ProfilesBlockeds.Where(Function(itm) itm.FromProfileID = userIdWhoDid AndAlso _
                                                                                                  itm.ToProfileID = userIdUnblocked).FirstOrDefault()

                If (rec IsNot Nothing) Then
                    _CMSDBDataContext.EUS_ProfilesBlockeds.DeleteOnSubmit(rec)
                    _CMSDBDataContext.SubmitChanges()
                End If

            Catch ex As Exception
                Throw New Exception(ex.Message, ex)
            End Try
        End Using
    End Sub



    Public Shared Function IsViewed(userIdViewed As Integer, userIdWhoDid As Integer) As Boolean
        Dim rec As Integer = 0

        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try
                rec = _CMSDBDataContext.EUS_ProfilesVieweds.Where(Function(itm) itm.FromProfileID = userIdWhoDid AndAlso itm.ToProfileID = userIdViewed).Count()
            Catch ex As Exception
                Throw

            End Try
        End Using
        Return (rec > 0)
    End Function


    Public Shared Function IsMessageSent(ToProfileId As Integer, FromProfileID As Integer) As Boolean
        Dim rec As Integer = 0

        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try
                rec = _CMSDBDataContext.EUS_Messages.Where(Function(itm) itm.FromProfileID = FromProfileID AndAlso itm.ToProfileID = ToProfileId AndAlso (itm.IsHidden Is Nothing OrElse itm.IsHidden = False)).Count()

                If (rec = 0) Then
                    Dim var As EUS_ProfilesCommunication = clsUserDoes.Get_ProfilesCommunication(_CMSDBDataContext, FromProfileID, ToProfileId)
                    If (var IsNot Nothing) Then rec = clsNullable.NullTo(var.SentMessageID)
                End If

            Catch ex As Exception
                Throw New Exception(ex.Message, ex)

            End Try
        End Using
        Return (rec > 0)
    End Function



    Public Shared Function HasCommunication(otherUserId As Integer, currentUserId As Integer) As Boolean
        Dim rec As Integer = 0

        'Dim maxProfileID_in_Table As Integer() = {8911, 8795}
        If (otherUserId > 8911 OrElse currentUserId > 8911 OrElse otherUserId = currentUserId) Then
            Return False
        End If

        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try

                rec = (From itm In _CMSDBDataContext.EUS_UnlockedConversations
                      Where (
                                (itm.ToProfileId = otherUserId AndAlso itm.FromProfileId = currentUserId) OrElse _
                                (itm.FromProfileId = otherUserId AndAlso itm.ToProfileId = currentUserId)
                            )
                      Select itm).Count()

                'ModGlobals.AllowUnlimited
                'AndAlso _
                '        itm.DateTimeCreated <= New DateTime(2013, 2, 8)
            Catch ex As Exception
                Throw New Exception(ex.Message, ex)

            End Try
        End Using

        Return (rec > 0)
    End Function




    Public Shared Function GetUnlockedConversation(otherUserId As Integer, currentUserId As Integer) As EUS_UnlockedConversation
        Dim rec As EUS_UnlockedConversation = Nothing
        If (otherUserId > 8911 OrElse currentUserId > 8911 OrElse otherUserId = currentUserId) Then
            Return rec
        End If

        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try
                rec = (From itm In _CMSDBDataContext.EUS_UnlockedConversations
                      Where (itm.ToProfileId = otherUserId AndAlso itm.FromProfileId = currentUserId) OrElse _
                                (itm.FromProfileId = otherUserId AndAlso itm.ToProfileId = currentUserId)
                      Order By itm.UnlockedConversationId Descending
                      Select itm).FirstOrDefault()
            Catch ex As Exception
                Throw New Exception(ex.Message, ex)

            End Try
        End Using
        Return rec
    End Function


    Public Shared Function IsFavorited(userIdFavorited As Integer, userIdWhoDid As Integer) As Boolean
        Dim rec As Integer = 0

        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try
                rec = _CMSDBDataContext.EUS_ProfilesFavorites.Where(Function(itm) itm.FromProfileID = userIdWhoDid AndAlso itm.ToProfileID = userIdFavorited).Count()
            Catch ex As Exception
                Throw

            End Try
        End Using
        'If (rec = 0) Then
        '    Dim var As EUS_ProfilesCommunication = clsUserDoes.Get_ProfilesCommunication(userIdWhoDid, userIdFavorited)
        '    If (var IsNot Nothing) Then rec = If(clsNullable.NullTo(var.MakeFavorite), 1, 0)
        'End If


        Return (rec > 0)
    End Function


    Public Shared Function IsBlocked(userIdBlocked As Integer, userIdWhoDid As Integer) As Boolean
        Dim rec As Integer = 0

        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try
                rec = _CMSDBDataContext.EUS_ProfilesBlockeds.Where(Function(itm) itm.FromProfileID = userIdWhoDid AndAlso itm.ToProfileID = userIdBlocked).Count()
            Catch ex As Exception
                Throw
            End Try
        End Using
        Return (rec > 0)
    End Function




    Public Shared Function GetLastOffer(userIdOfferReceiver As Integer, userIdWhoDid As Integer) As EUS_Offer
        Dim rec As EUS_Offer = Nothing

        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)

            Try

                Dim rerunTran As Boolean = False
                Try

                    rec = (From itm In _CMSDBDataContext.EUS_Offers
                          Where ((itm.ToProfileID = userIdOfferReceiver AndAlso itm.FromProfileID = userIdWhoDid) OrElse _
                                 (itm.ToProfileID = userIdWhoDid AndAlso itm.FromProfileID = userIdOfferReceiver)) AndAlso _
                                (itm.OfferTypeID = ProfileHelper.OfferTypeID_OFFERNEW OrElse itm.OfferTypeID = ProfileHelper.OfferTypeID_OFFERCOUNTER) AndAlso _
                                itm.ChildOfferID = 0
                          Select itm).FirstOrDefault()

                Catch ex As System.Data.SqlClient.SqlException
                    rerunTran = DataHelpers.CheckSqlException(ex.Message)
                    If (Not rerunTran) Then Throw
                End Try


                If (rerunTran) Then
                    rerunTran = False
                    Try

                        rec = (From itm In _CMSDBDataContext.EUS_Offers
                              Where ((itm.ToProfileID = userIdOfferReceiver AndAlso itm.FromProfileID = userIdWhoDid) OrElse _
                                     (itm.ToProfileID = userIdWhoDid AndAlso itm.FromProfileID = userIdOfferReceiver)) AndAlso _
                                    (itm.OfferTypeID = ProfileHelper.OfferTypeID_OFFERNEW OrElse itm.OfferTypeID = ProfileHelper.OfferTypeID_OFFERCOUNTER) AndAlso _
                                    itm.ChildOfferID = 0
                              Select itm).FirstOrDefault()

                    Catch ex As System.Data.SqlClient.SqlException
                        rerunTran = DataHelpers.CheckSqlException(ex.Message)
                        If (Not rerunTran) Then Throw
                    End Try
                End If


                If (rerunTran) Then
                    rerunTran = False

                    rec = (From itm In _CMSDBDataContext.EUS_Offers
                          Where ((itm.ToProfileID = userIdOfferReceiver AndAlso itm.FromProfileID = userIdWhoDid) OrElse _
                                 (itm.ToProfileID = userIdWhoDid AndAlso itm.FromProfileID = userIdOfferReceiver)) AndAlso _
                                (itm.OfferTypeID = ProfileHelper.OfferTypeID_OFFERNEW OrElse itm.OfferTypeID = ProfileHelper.OfferTypeID_OFFERCOUNTER) AndAlso _
                                itm.ChildOfferID = 0
                          Select itm).FirstOrDefault()
                End If

            Catch ex As Exception
                Throw New Exception(ex.Message, ex)
            End Try

        End Using
        Return rec
    End Function



    Public Shared Function GetOfferByOfferId(userId As Integer, offerId As Integer) As EUS_Offer
        Dim rec As EUS_Offer = Nothing

        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try
                rec = (From itm In _CMSDBDataContext.EUS_Offers
                      Where itm.OfferID = offerId AndAlso (itm.FromProfileID = userId OrElse itm.ToProfileID = userId)
                      Select itm).FirstOrDefault()
            Catch ex As Exception
                Throw New Exception(ex.Message, ex)

            End Try
        End Using
        Return rec
    End Function


    Public Shared Function GetLastOfferWithAmount(userIdOfferReceiver As Integer, userIdWhoDid As Integer) As EUS_Offer
        Dim rec As EUS_Offer = Nothing

        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try
                rec = (From itm In _CMSDBDataContext.EUS_Offers
                      Where ((itm.ToProfileID = userIdOfferReceiver AndAlso itm.FromProfileID = userIdWhoDid) OrElse _
                             (itm.ToProfileID = userIdWhoDid AndAlso itm.FromProfileID = userIdOfferReceiver)) AndAlso _
                             itm.Amount > 0
                      Order By itm.DateTimeToCreate Descending
                      Select itm).FirstOrDefault()
            Catch ex As Exception
                Throw New Exception(ex.Message, ex)

            End Try
        End Using
        Return rec
    End Function


    Public Shared Function GetLastOfferFromTo(ToProfileID As Integer, FromProfileID As Integer) As EUS_Offer
        Dim rec As EUS_Offer = Nothing

        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try
                rec = (From itm In _CMSDBDataContext.EUS_Offers
                      Where (itm.ToProfileID = ToProfileID AndAlso itm.FromProfileID = FromProfileID)
                      Order By itm.DateTimeToCreate Descending
                      Select itm).FirstOrDefault()
            Catch ex As Exception
                Throw New Exception(ex.Message, ex)

            End Try
        End Using
        Return rec
    End Function


    Public Shared Function GetLastOfferAny(userIdOfferReceiver As Integer, userIdWhoDid As Integer) As EUS_Offer
        Dim rec As EUS_Offer = Nothing

        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try
                rec = (From itm In _CMSDBDataContext.EUS_Offers
                      Where ((itm.ToProfileID = userIdOfferReceiver AndAlso itm.FromProfileID = userIdWhoDid) OrElse _
                             (itm.ToProfileID = userIdWhoDid AndAlso itm.FromProfileID = userIdOfferReceiver))
                      Order By itm.DateTimeToCreate Descending
                      Select itm).FirstOrDefault()
            Catch ex As Exception
                Throw New Exception(ex.Message, ex)

            End Try
        End Using
        Return rec
    End Function

    Public Shared Function GetAnyWinkPending(userIdOfferReceiver As Integer, userIdWhoDid As Integer) As EUS_Offer
        Dim rec As EUS_Offer = Nothing

        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try
                rec = (From itm In _CMSDBDataContext.EUS_Offers
                      Where ((itm.ToProfileID = userIdOfferReceiver AndAlso itm.FromProfileID = userIdWhoDid) OrElse _
                             (itm.ToProfileID = userIdWhoDid AndAlso itm.FromProfileID = userIdOfferReceiver)) AndAlso _
                             (itm.OfferTypeID = ProfileHelper.OfferTypeID_WINK) AndAlso _
                             itm.StatusID = ProfileHelper.OfferStatusID_PENDING
                      Select itm).FirstOrDefault()
            Catch ex As Exception
                Throw New Exception(ex.Message, ex)
            End Try
        End Using
        Return rec
    End Function


    Public Shared Function GetAnyPokePending(userIdOfferReceiver As Integer, userIdWhoDid As Integer) As EUS_Offer
        Dim rec As EUS_Offer = Nothing

        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try
                rec = (From itm In _CMSDBDataContext.EUS_Offers
                      Where ((itm.ToProfileID = userIdOfferReceiver AndAlso itm.FromProfileID = userIdWhoDid) OrElse _
                             (itm.ToProfileID = userIdWhoDid AndAlso itm.FromProfileID = userIdOfferReceiver)) AndAlso _
                             (itm.OfferTypeID = ProfileHelper.OfferTypeID_POKE) AndAlso _
                             itm.StatusID = ProfileHelper.OfferStatusID_PENDING
                      Select itm).FirstOrDefault()
            Catch ex As Exception
                Throw New Exception(ex.Message, ex)

            End Try
        End Using
        Return rec
    End Function


    Public Shared Function GetMyWink(userIdOfferReceiver As Integer, userIdWhoDid As Integer) As EUS_Offer
        Dim rec As EUS_Offer = Nothing

        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try
                rec = (From itm In _CMSDBDataContext.EUS_Offers
                      Where (itm.ToProfileID = userIdOfferReceiver AndAlso itm.FromProfileID = userIdWhoDid) AndAlso _
                            (itm.OfferTypeID = ProfileHelper.OfferTypeID_WINK)
                      Select itm).FirstOrDefault()
            Catch ex As Exception
                Throw New Exception(ex.Message, ex)

            End Try
        End Using
        Return rec
    End Function


    Public Shared Function GetMyPendingWink(userIdOfferReceiver As Integer, userIdWhoDid As Integer) As EUS_Offer
        Dim rec As EUS_Offer = Nothing

        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try
                rec = (From itm In _CMSDBDataContext.EUS_Offers
                      Where (itm.ToProfileID = userIdOfferReceiver AndAlso itm.FromProfileID = userIdWhoDid) AndAlso _
                            (itm.OfferTypeID = ProfileHelper.OfferTypeID_WINK) AndAlso _
                            (itm.StatusID = ProfileHelper.OfferStatusID_PENDING)
                      Select itm).FirstOrDefault()
            Catch ex As Exception
                Throw New Exception(ex.Message, ex)
            End Try
        End Using
        Return rec
    End Function


    Public Shared Function IsAnyWink(userIdOfferReceiver As Integer, userIdWhoDid As Integer) As Boolean
        Dim rec As Boolean

        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try
                rec = ((From itm In _CMSDBDataContext.EUS_Offers
                      Where ((itm.ToProfileID = userIdOfferReceiver AndAlso itm.FromProfileID = userIdWhoDid) OrElse _
                             (itm.ToProfileID = userIdWhoDid AndAlso itm.FromProfileID = userIdOfferReceiver)) AndAlso _
                            (itm.OfferTypeID = ProfileHelper.OfferTypeID_WINK)
                      Select itm).Count() > 0)


                If (rec = 0) Then
                    Dim var As EUS_ProfilesCommunication = clsUserDoes.Get_ProfilesCommunication(_CMSDBDataContext, userIdOfferReceiver, userIdWhoDid)
                    If (var IsNot Nothing) Then rec = clsNullable.NullTo(var.SentLikeID)
                    If (rec = 0) Then
                        var = clsUserDoes.Get_ProfilesCommunication(_CMSDBDataContext, userIdWhoDid, userIdOfferReceiver)
                        If (var IsNot Nothing) Then rec = clsNullable.NullTo(var.SentLikeID)
                    End If
                End If

            Catch ex As Exception
                Throw New Exception(ex.Message, ex)

            End Try
        End Using
        Return rec
    End Function


    Public Shared Function IsAnyWinkOrIsAnyOffer(userIdOfferReceiver As Integer, userIdWhoDid As Integer) As Boolean
        Dim rec As Boolean

        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try
                rec = ((From itm In _CMSDBDataContext.EUS_Offers
                      Where ((itm.ToProfileID = userIdOfferReceiver AndAlso itm.FromProfileID = userIdWhoDid) OrElse _
                             (itm.ToProfileID = userIdWhoDid AndAlso itm.FromProfileID = userIdOfferReceiver)) AndAlso _
                            (itm.OfferTypeID = ProfileHelper.OfferTypeID_WINK OrElse itm.OfferTypeID = ProfileHelper.OfferTypeID_OFFERNEW OrElse itm.OfferTypeID = ProfileHelper.OfferTypeID_OFFERCOUNTER)
                      Select itm).Count() > 0)


                If (rec = 0) Then
                    Dim var As EUS_ProfilesCommunication = clsUserDoes.Get_ProfilesCommunication(_CMSDBDataContext, userIdOfferReceiver, userIdWhoDid)
                    If (var IsNot Nothing) Then
                        rec = clsNullable.NullTo(var.SentLikeID)
                        If (rec = 0) Then rec = clsNullable.NullTo(var.SentOfferID)
                    End If
                    If (rec = 0) Then
                        var = clsUserDoes.Get_ProfilesCommunication(_CMSDBDataContext, userIdWhoDid, userIdOfferReceiver)
                        If (var IsNot Nothing) Then
                            rec = clsNullable.NullTo(var.SentLikeID)
                            If (rec = 0) Then rec = clsNullable.NullTo(var.SentOfferID)
                        End If
                    End If
                End If

            Catch ex As Exception
                Throw New Exception(ex.Message, ex)
            End Try
        End Using

        Return rec
    End Function


    Public Shared Function IsAnyWinkOrIsAnyOffer_FromProfilesCommunication(userIdOfferReceiver As Integer, userIdWhoDid As Integer) As Boolean
        Dim rec As Boolean

        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try

                Dim var As EUS_ProfilesCommunication = clsUserDoes.Get_ProfilesCommunication(_CMSDBDataContext, userIdOfferReceiver, userIdWhoDid)
                If (var IsNot Nothing) Then
                    rec = clsNullable.NullTo(var.SentLikeID)
                    If (rec = 0) Then rec = clsNullable.NullTo(var.SentOfferID)
                End If
                If (rec = 0) Then
                    var = clsUserDoes.Get_ProfilesCommunication(_CMSDBDataContext, userIdWhoDid, userIdOfferReceiver)
                    If (var IsNot Nothing) Then
                        rec = clsNullable.NullTo(var.SentLikeID)
                        If (rec = 0) Then rec = clsNullable.NullTo(var.SentOfferID)
                    End If
                End If

            Catch ex As Exception
                Throw New Exception(ex.Message, ex)

            End Try
        End Using

        Return rec
    End Function


    Public Shared Function IsAnyMessageExchanged(userId1 As Integer, userId2 As Integer) As Boolean
        Dim rec As Boolean
        Using tx As New System.Transactions.TransactionScope(TransactionScopeOption.Required, New TransactionOptions With {.IsolationLevel = IsolationLevel.ReadUncommitted})


            Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


                Try
                    ' get any message

                    rec = ((From itm In _CMSDBDataContext.EUS_Messages
                          Where ((itm.ToProfileID = userId1 AndAlso itm.FromProfileID = userId2) OrElse _
                                 (itm.ToProfileID = userId2 AndAlso itm.FromProfileID = userId1)) _
                                 AndAlso (itm.IsHidden Is Nothing OrElse itm.IsHidden = False)
                          Select itm).Count() > 0)


                    If (rec = False) Then
                        Dim var As EUS_ProfilesCommunication = clsUserDoes.Get_ProfilesCommunication(_CMSDBDataContext, userId1, userId2)
                        If (var IsNot Nothing) Then
                            rec = (clsNullable.NullTo(var.SentMessageID) > 0)
                        End If

                        If (rec = False) Then
                            var = clsUserDoes.Get_ProfilesCommunication(_CMSDBDataContext, userId2, userId1)
                            If (var IsNot Nothing) Then
                                rec = (clsNullable.NullTo(var.SentMessageID) > 0)
                            End If
                        End If
                    End If

                Catch ex As Exception
                    Throw New Exception(ex.Message, ex)
                End Try
            End Using
        End Using
        Return rec
    End Function


    Public Shared Function IsAnyMessageExchangedFromTo(ToProfileID As Integer, FromProfileID As Integer, OwnerID As Integer) As Boolean
        Dim rec As Boolean

        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try
                ' get any message
                rec = ((From itm In _CMSDBDataContext.EUS_Messages
                      Where ((itm.ToProfileID = ToProfileID AndAlso itm.FromProfileID = FromProfileID) AndAlso itm.ProfileIDOwner = OwnerID)
                      Select itm).Count() > 0)

                If (rec = 0) Then
                    Dim var As EUS_ProfilesCommunication = clsUserDoes.Get_ProfilesCommunication(_CMSDBDataContext, FromProfileID, ToProfileID)
                    If (var IsNot Nothing) Then
                        rec = clsNullable.NullTo(var.SentMessageID)
                    End If
                End If

            Catch ex As Exception
                Throw New Exception(ex.Message, ex)
            End Try
        End Using

        Return rec
    End Function


    Public Shared Function GetMyPoke(userIdOfferReceiver As Integer, userIdWhoDid As Integer) As EUS_Offer
        Dim rec As EUS_Offer = Nothing

        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
            Try
                rec = (From itm In _CMSDBDataContext.EUS_Offers
                      Where (itm.ToProfileID = userIdOfferReceiver AndAlso itm.FromProfileID = userIdWhoDid) AndAlso _
                            (itm.OfferTypeID = ProfileHelper.OfferTypeID_POKE)
                      Select itm).FirstOrDefault()
            Catch ex As Exception
                Throw New Exception(ex.Message, ex)

            End Try
        End Using
        Return rec
    End Function


    Public Shared Function GetMyPendingPoke(userIdOfferReceiver As Integer, userIdWhoDid As Integer) As EUS_Offer
        Dim rec As EUS_Offer = Nothing

        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try
                rec = (From itm In _CMSDBDataContext.EUS_Offers
                      Where (itm.ToProfileID = userIdOfferReceiver AndAlso itm.FromProfileID = userIdWhoDid) AndAlso _
                            (itm.OfferTypeID = ProfileHelper.OfferTypeID_POKE) AndAlso _
                            (itm.StatusID = ProfileHelper.OfferStatusID_PENDING)
                      Select itm).FirstOrDefault()
            Catch ex As Exception
                Throw New Exception(ex.Message, ex)
            End Try
        End Using
        Return rec
    End Function


    Public Shared Function HasAnyOffer(userIdOfferReceiver As Integer, userIdWhoDid As Integer) As Boolean
        Dim rec As Boolean

        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try
                rec = ((From itm In _CMSDBDataContext.EUS_Offers
                      Where ((itm.ToProfileID = userIdOfferReceiver AndAlso itm.FromProfileID = userIdWhoDid) OrElse _
                             (itm.ToProfileID = userIdWhoDid AndAlso itm.FromProfileID = userIdOfferReceiver))
                      Select itm).Count() > 0)


                If (rec = 0) Then
                    Dim var As EUS_ProfilesCommunication = clsUserDoes.Get_ProfilesCommunication(_CMSDBDataContext, userIdOfferReceiver, userIdWhoDid)
                    If (var IsNot Nothing) Then
                        rec = clsNullable.NullTo(var.SentLikeID)
                        If (rec = 0) Then rec = clsNullable.NullTo(var.SentOfferID)
                        If (rec = 0) Then rec = clsNullable.NullTo(var.SentPokeID)
                        If (rec = 0) Then rec = If(clsNullable.NullTo(var.IsDate), 1, 0)
                    End If
                    If (rec = 0) Then
                        var = clsUserDoes.Get_ProfilesCommunication(_CMSDBDataContext, userIdWhoDid, userIdOfferReceiver)
                        If (var IsNot Nothing) Then
                            rec = clsNullable.NullTo(var.SentLikeID)
                            If (rec = 0) Then rec = clsNullable.NullTo(var.SentOfferID)
                            If (rec = 0) Then rec = clsNullable.NullTo(var.SentPokeID)
                            If (rec = 0) Then rec = If(clsNullable.NullTo(var.IsDate), 1, 0)
                        End If
                    End If
                End If

            Catch ex As Exception
                Throw New Exception(ex.Message, ex)

            End Try
        End Using
        Return rec
    End Function

    Public Shared Function IsReferrerAllowedSendWink(userIdReceiving As Integer, userIdSending As Integer) As Boolean
        Dim MaySendWink As Boolean = False

        ' check if receiver has photo, visible to sending user
        'Dim VisiblePhotos As Integer = DataHelpers.EUS_CustomerPhotos_GetVisiblePhotosFromTo(userIdReceiving, userIdSending)
        Dim VisiblePhotos As Integer = DataHelpers.EUS_CustomerPhotos_GetVisiblePhotos(userIdReceiving)

        If (VisiblePhotos = 0) Then
            ' check if man has sent any offer or like to woman, 
            ' receiving user has sent anything to sending user?
            If (clsUserDoes.GetLastOfferFromTo(userIdSending, userIdReceiving) IsNot Nothing) Then
                MaySendWink = True
            ElseIf (clsUserDoes.IsAnyMessageExchangedFromTo(userIdReceiving, userIdSending, userIdSending)) Then
                MaySendWink = True
            End If
        Else
            MaySendWink = True
        End If
        Return MaySendWink
    End Function


    Public Shared Function IsReferrerAllowedSendMessage(userIdReceiving As Integer, userIdSending As Integer) As ReferrerStopSendingMessageEnum
        Dim ReferrerMaySendMessage As ReferrerStopSendingMessageEnum = ReferrerStopSendingMessageEnum.None

        ' check if receiver has photo, visible to sending user
        'Dim VisiblePhotos As Integer = DataHelpers.EUS_CustomerPhotos_GetVisiblePhotosFromTo(userIdReceiving, userIdSending)
        Dim VisiblePhotos As Integer = DataHelpers.EUS_CustomerPhotos_GetVisiblePhotos(userIdReceiving)

        If (VisiblePhotos = 0) Then

            ' check if man has sent any offer or like to referrer woman, 
            ' receiving user has sent anything to referrer?
            If (clsUserDoes.GetLastOfferFromTo(userIdSending, userIdReceiving) IsNot Nothing) Then
                'ReferrerMaySendMessage = ReferrerStopSendingMessageEnum.None

            ElseIf (clsUserDoes.IsAnyMessageExchangedFromTo(userIdSending, userIdReceiving, userIdSending)) Then
                'ReferrerMaySendMessage = ReferrerStopSendingMessageEnum.None

            Else
                ReferrerMaySendMessage = ReferrerStopSendingMessageEnum.RecipientHasNoPhoto
            End If

        Else


            ' find how many messages referrer sent to recipient and vice versa
            Using dt As DataTable = DataHelpers.EUS_Messages_CountMessagesExchangedFromTo_ByReferrer(userIdSending, userIdReceiving)



                Dim MessagesSent As Integer
                Dim MessagesSentAndOpened As Integer
                Dim MessagesReceivedAndOpened As Integer
                Dim MessagesSentAndOpened_Male As Integer
                Dim MessagesReceivedAndOpened_Male As Integer

                If (dt.Rows.Count > 0) Then
                    MessagesSent = dt.Rows(0)("MessagesSent")
                    MessagesSentAndOpened = dt.Rows(0)("MessagesSentAndOpened")
                    MessagesReceivedAndOpened = dt.Rows(0)("MessagesReceivedAndOpened")
                    MessagesSentAndOpened_Male = dt.Rows(0)("MessagesSentAndOpened_Male")
                    MessagesReceivedAndOpened_Male = dt.Rows(0)("MessagesReceivedAndOpened_Male")

                    If ((MessagesSent >= 2 AndAlso MessagesSentAndOpened = 0 AndAlso MessagesReceivedAndOpened = 0) AndAlso
                        (MessagesSent >= 2 AndAlso MessagesSentAndOpened_Male = 0 AndAlso MessagesReceivedAndOpened_Male = 0)) Then

                        ReferrerMaySendMessage = ReferrerStopSendingMessageEnum.MaxAllowedMessagesSent

                    End If
                End If
            End Using
        End If

        Return ReferrerMaySendMessage
    End Function

    Public Shared Sub SendWink(userIdReceiving As Integer, userIdSending As Integer)

        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try

                ' check
                'Dim rec = _CMSDBDataContext.EUS_Offers.Where(Function(itm) itm.ToProfileID = userIdReceiver AndAlso itm.FromProfileID = userIdWhoDid).Count()
                Dim rec As EUS_Offer = (From itm In _CMSDBDataContext.EUS_Offers
                                        Where itm.ToProfileID = userIdReceiving AndAlso itm.FromProfileID = userIdSending
                                        Select itm).FirstOrDefault()

                If (rec Is Nothing) Then

                    Dim newOffer As New EUS_Offer()
                    newOffer.DateTimeToCreate = DateTime.UtcNow
                    newOffer.FromProfileID = userIdSending
                    newOffer.OfferTypeID = ProfileHelper.OfferTypeID_WINK
                    newOffer.StatusID = ProfileHelper.OfferStatusID_PENDING
                    newOffer.ToProfileID = userIdReceiving
                    newOffer.Amount = 0
                    newOffer.ParrentOfferID = 0
                    newOffer.ChildOfferID = 0
                    newOffer.IsDate = False

                    _CMSDBDataContext.EUS_Offers.InsertOnSubmit(newOffer)
                    _CMSDBDataContext.SubmitChanges()

                    clsUserDoes.Update_ProfilesCommunication(userIdSending, userIdReceiving, MemberActionTypeEnum.Likes, newOffer.OfferID)
                    clsUserDoes.ResetMemberCounter(MemberCountersEnum.NewLikes, newOffer.ToProfileID)
                    clsUserNotifications.SendEmailNotification(NotificationType.Likes, newOffer.FromProfileID, newOffer.ToProfileID)
                End If

            Catch ex As Exception
                Throw New Exception(ex.Message, ex)

            End Try
        End Using
    End Sub


    Public Shared Function SendPoke(parms As NewOfferParameters) As Integer
        Dim newOfferID As Integer = 0

        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try

                Dim newOffer As New EUS_Offer()
                Dim parentOffer As EUS_Offer = (From itm In _CMSDBDataContext.EUS_Offers
                                                Where itm.OfferID = parms.parentOfferId
                                                Select itm).SingleOrDefault()



                newOffer.ToProfileID = parms.userIdReceiver
                newOffer.Amount = -1
                newOffer.FromProfileID = parms.userIdWhoDid
                newOffer.DateTimeToCreate = DateTime.UtcNow

                newOffer.Notes1 = parms.messageText1
                newOffer.Notes2 = parms.messageText2

                newOffer.OfferTypeID = ProfileHelper.OfferTypeID_POKE
                newOffer.StatusID = ProfileHelper.OfferStatusID_PENDING
                newOffer.IsDate = False

                If parentOffer IsNot Nothing Then
                    newOffer.ParrentOfferID = parms.parentOfferId
                    If (newOffer.ToProfileID <= 0) Then newOffer.ToProfileID = parentOffer.FromProfileID
                End If

                newOffer.ChildOfferID = 0


                _CMSDBDataContext.EUS_Offers.InsertOnSubmit(newOffer)
                _CMSDBDataContext.SubmitChanges()

                newOfferID = newOffer.OfferID

                'If parentOffer IsNot Nothing Then
                '    parentOffer.StatusID = ProfileHelper.OfferStatusID_REPLACINGBYCOUNTER
                '    parentOffer.ChildOfferID = newOffer.OfferID
                '    _CMSDBDataContext.SubmitChanges()
                'End If

                'clsUserNotifications.SendEmailNotification(NotificationType.Offers, newOffer.FromProfileID, newOffer.ToProfileID)
                clsUserDoes.ResetMemberCounter(MemberCountersEnum.NewLikes, newOffer.ToProfileID)
            Catch ex As Exception
                Throw New Exception(ex.Message, ex)

            End Try
        End Using
        Return newOfferID
    End Function



    Public Shared Sub DeleteOfferAndParents(offerId As Integer)

        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try

                If (offerId > 0) Then
                    Dim q As EUS_Offer = (From itm In _CMSDBDataContext.EUS_Offers
                            Where itm.OfferID = offerId
                            Select itm).SingleOrDefault()

                    If (q IsNot Nothing) Then
                        _CMSDBDataContext.EUS_Offers.DeleteOnSubmit(q)

                        Dim parentId As Integer = IIf(q.ParrentOfferID.HasValue, q.ParrentOfferID, 0)
                        While (parentId > 0)

                            Dim q2 As EUS_Offer = (From itm In _CMSDBDataContext.EUS_Offers
                                                    Where itm.OfferID = parentId
                                                    Select itm).SingleOrDefault()
                            parentId = -1

                            If (q2 IsNot Nothing) Then
                                _CMSDBDataContext.EUS_Offers.DeleteOnSubmit(q2)

                                If (q2.ParrentOfferID > 0) Then parentId = q2.ParrentOfferID
                            End If

                        End While

                        _CMSDBDataContext.SubmitChanges()

                        clsUserDoes.ResetMemberCounter(MemberCountersEnum.NewLikes Or MemberCountersEnum.NewDates Or MemberCountersEnum.NewOffers, q.ToProfileID)
                        clsUserDoes.ResetMemberCounter(MemberCountersEnum.NewLikes Or MemberCountersEnum.NewDates Or MemberCountersEnum.NewOffers, q.FromProfileID)

                    End If
                End If

            Catch ex As Exception
                Throw New Exception(ex.Message, ex)

            End Try
        End Using
    End Sub


    Public Shared Sub CancelWinkOrOffer(offerId As Integer)

        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try

                If (offerId > 0) Then
                    Dim q = (From itm In _CMSDBDataContext.EUS_Offers
                            Where itm.OfferID = offerId
                            Select itm).SingleOrDefault()

                    If (q IsNot Nothing) Then
                        _CMSDBDataContext.EUS_Offers.DeleteOnSubmit(q)
                        _CMSDBDataContext.SubmitChanges()

                        clsUserDoes.ResetMemberCounter(MemberCountersEnum.NewOffers Or MemberCountersEnum.NewDates Or MemberCountersEnum.NewLikes, q.ToProfileID)
                    End If

                    'If (q IsNot Nothing) Then
                    '    q.StatusID = ProfileHelper.OfferStatusID_CANCELED
                    '    _CMSDBDataContext.SubmitChanges()
                    'End If
                End If

            Catch ex As Exception
                Throw New Exception(ex.Message, ex)

            End Try
        End Using
    End Sub



    Public Shared Sub CancelPendingWink(userIdReceiver As Integer, userIdWhoDid As Integer)

        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try

                ' check
                Dim rec = (From itm In _CMSDBDataContext.EUS_Offers
                            Where (itm.ToProfileID = userIdReceiver AndAlso itm.FromProfileID = userIdWhoDid) AndAlso _
                                (itm.OfferTypeID = ProfileHelper.OfferTypeID_WINK) AndAlso _
                                (itm.StatusID = ProfileHelper.OfferStatusID_PENDING)
                            Select itm).FirstOrDefault()

                If (rec IsNot Nothing) Then
                    _CMSDBDataContext.EUS_Offers.DeleteOnSubmit(rec)
                    _CMSDBDataContext.SubmitChanges()

                    clsUserDoes.ResetMemberCounter(MemberCountersEnum.NewLikes, rec.ToProfileID)

                End If

            Catch ex As Exception
                Throw New Exception(ex.Message, ex)

            End Try
        End Using
    End Sub



    Public Shared Sub ResendWinkOrOffer(offerId As Integer)

        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try

                If (offerId > 0) Then
                    Dim q = (From itm In _CMSDBDataContext.EUS_Offers
                            Where itm.OfferID = offerId
                            Select itm).SingleOrDefault()

                    If (q IsNot Nothing) Then
                        q.StatusID = ProfileHelper.OfferStatusID_PENDING
                        _CMSDBDataContext.SubmitChanges()
                    End If
                End If

            Catch ex As Exception
                Throw New Exception(ex.Message, ex)

            End Try
        End Using
    End Sub



    Public Shared Sub AcceptOffer(offerId As Integer, AcceptedOfferStatus As OfferStatusEnum)

        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try

                Dim parentOffer As EUS_Offer = (From itm In _CMSDBDataContext.EUS_Offers
                                                Where itm.OfferID = offerId
                                                Select itm).SingleOrDefault()

                If (AcceptedOfferStatus = OfferStatusEnum.OFFER_ACCEEPTED_WITH_MESSAGE) Then
                    parentOffer.StatusID = ProfileHelper.OfferStatusID_OFFER_ACCEEPTED_WITH_MESSAGE

                ElseIf (AcceptedOfferStatus = OfferStatusEnum.ACCEPTED) Then
                    parentOffer.StatusID = ProfileHelper.OfferStatusID_ACCEPTED

                End If

                _CMSDBDataContext.SubmitChanges()

                clsUserDoes.ResetMemberCounter(MemberCountersEnum.NewOffers, parentOffer.ToProfileID)
                'clsUserNotifications.SendEmailNotification(NotificationType.Offers, newOffer.FromProfileID, newOffer.ToProfileID)
            Catch ex As Exception
                Throw New Exception(ex.Message, ex)

            End Try
        End Using
    End Sub


    Public Shared Sub RejectOffer(offerId As Integer, rejectionReason As String)

        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try

                If (offerId > 0) Then
                    Dim q = (From itm In _CMSDBDataContext.EUS_Offers
                            Where itm.OfferID = offerId
                            Select itm).SingleOrDefault()

                    If (q IsNot Nothing) Then
                        q.StatusID = Lists.gDSLists.EUS_OffersStatus.Where(Function(itm) itm.ConstantName = rejectionReason).FirstOrDefault().EUS_OffersStatusID
                        _CMSDBDataContext.SubmitChanges()

                        clsUserDoes.ResetMemberCounter(MemberCountersEnum.NewOffers Or MemberCountersEnum.NewDates Or MemberCountersEnum.NewLikes, q.ToProfileID)
                    End If
                End If

            Catch ex As Exception
                Throw New Exception(ex.Message, ex)
            End Try
        End Using
    End Sub




    Public Shared Function MakeOffer(parms As NewOfferParameters) As Integer
        Dim newOfferId As Integer


        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try

                Dim newOffer As New EUS_Offer()
                Dim parentOffer As EUS_Offer = (From itm In _CMSDBDataContext.EUS_Offers
                                                Where itm.OfferID = parms.parentOfferId
                                                Select itm).SingleOrDefault()



                newOffer.ToProfileID = parms.userIdReceiver
                newOffer.Amount = parms.offerAmount
                newOffer.FromProfileID = parms.userIdWhoDid
                newOffer.DateTimeToCreate = DateTime.UtcNow

                newOffer.Notes1 = parms.messageText1
                newOffer.Notes2 = parms.messageText2

                newOffer.OfferTypeID = ProfileHelper.OfferTypeID_OFFERNEW
                newOffer.StatusID = ProfileHelper.OfferStatusID_PENDING
                newOffer.IsDate = False

                If parentOffer IsNot Nothing Then
                    newOffer.ParrentOfferID = parms.parentOfferId
                End If
                newOffer.ChildOfferID = 0


                _CMSDBDataContext.EUS_Offers.InsertOnSubmit(newOffer)
                _CMSDBDataContext.SubmitChanges()


                clsUserDoes.Update_ProfilesCommunication(parms.userIdWhoDid, parms.userIdReceiver, MemberActionTypeEnum.Offer, newOffer.OfferID)
                clsUserDoes.ResetMemberCounter(MemberCountersEnum.NewOffers, newOffer.ToProfileID)

                newOfferId = newOffer.OfferID

                'If parentOffer IsNot Nothing Then
                '    parentOffer.StatusID = ProfileHelper.OfferStatusID_REPLACINGBYCOUNTER
                '    parentOffer.ChildOfferID = newOffer.OfferID
                '    _CMSDBDataContext.SubmitChanges()
                'End If

                clsUserNotifications.SendEmailNotification(NotificationType.Offers, newOffer.FromProfileID, newOffer.ToProfileID)
            Catch ex As Exception
                Throw New Exception(ex.Message, ex)

            End Try
        End Using
        Return newOfferId
    End Function



    Public Shared Sub MakeNewDate(FromProfileId As Integer, ToProfileId As Integer, OfferId As Integer, CurrentIsMale As Boolean)
        Dim anyOfferAccepted As Boolean = False

        Dim parms As New NewOfferParameters()
        If (OfferId > 0) Then

            Dim _offer As EUS_Offer = clsUserDoes.GetOfferByOfferId(FromProfileId, OfferId) 'Me.MasterProfileId, Me.OfferId
            If (_offer.OfferTypeID = ProfileHelper.OfferTypeID_POKE) Then
                clsUserDoes.AcceptPoke(OfferId, OfferStatusEnum.POKE_ACCEEPTED_WITH_MESSAGE)
                anyOfferAccepted = True
            ElseIf (_offer.OfferTypeID = ProfileHelper.OfferTypeID_WINK) Then
                clsUserDoes.AcceptLike(OfferId, OfferStatusEnum.LIKE_ACCEEPTED_WITH_MESSAGE)
                anyOfferAccepted = True
            ElseIf (_offer.OfferTypeID = ProfileHelper.OfferTypeID_OFFERNEW) OrElse (_offer.OfferTypeID = ProfileHelper.OfferTypeID_OFFERCOUNTER) Then
                clsUserDoes.AcceptOffer(OfferId, OfferStatusEnum.OFFER_ACCEEPTED_WITH_MESSAGE)
                anyOfferAccepted = True
            End If


            'If (SendUnlimited) Then
            parms.messageText1 = ""
            parms.messageText2 = ""
            parms.offerAmount = _offer.Amount
            parms.parentOfferId = _offer.OfferID
            parms.userIdReceiver = _offer.ToProfileID
            parms.userIdWhoDid = _offer.FromProfileID
            'End If
        Else
            parms.messageText1 = ""
            parms.messageText2 = ""
            parms.offerAmount = 0
            'parms.parentOfferId = _offer.OfferID
            parms.userIdReceiver = ToProfileId
            parms.userIdWhoDid = FromProfileId
        End If

        If (CurrentIsMale) Then 'Me.IsMale
            If (Not clsUserDoes.HasDateOfferAlready(parms.userIdWhoDid, parms.userIdReceiver)) Then clsUserDoes.MakeNewDate(parms)
        End If


        If (Not anyOfferAccepted) Then

            Dim rec As EUS_Offer = clsUserDoes.GetAnyWinkPending(FromProfileId, ToProfileId) 'Me.MasterProfileId, Me.UserIdInView
            If (rec IsNot Nothing) Then
                'like check succesfull
                clsUserDoes.AcceptLike(rec.OfferID, OfferStatusEnum.LIKE_ACCEEPTED_WITH_MESSAGE)
            End If

            rec = clsUserDoes.GetAnyPokePending(FromProfileId, ToProfileId)
            If (rec IsNot Nothing) Then
                'poke check succesfull
                clsUserDoes.AcceptPoke(rec.OfferID, OfferStatusEnum.POKE_ACCEEPTED_WITH_MESSAGE)
            End If

            rec = clsUserDoes.GetLastOffer(FromProfileId, ToProfileId)
            If (rec IsNot Nothing) Then
                'offer check succesfull
                clsUserDoes.AcceptOffer(rec.OfferID, OfferStatusEnum.OFFER_ACCEEPTED_WITH_MESSAGE)
            End If


            'Dim rec As EUS_Offer = clsUserDoes.GetAnyWinkPending(FromProfileId, ToProfileId) 'Me.MasterProfileId, Me.UserIdInView
            'If (rec IsNot Nothing) Then
            '    'like check succesfull
            '    clsUserDoes.AcceptLike(rec.OfferID, OfferStatusEnum.LIKE_ACCEEPTED_WITH_MESSAGE)
            'Else
            '    rec = clsUserDoes.GetAnyPokePending(FromProfileId, ToProfileId)
            '    If (rec IsNot Nothing) Then
            '        'poke check succesfull
            '        clsUserDoes.AcceptPoke(rec.OfferID, OfferStatusEnum.POKE_ACCEEPTED_WITH_MESSAGE)
            '    Else
            '        rec = clsUserDoes.GetLastOffer(FromProfileId, ToProfileId)
            '        If (rec IsNot Nothing) Then
            '            'offer check succesfull
            '            clsUserDoes.AcceptOffer(rec.OfferID, OfferStatusEnum.OFFER_ACCEEPTED_WITH_MESSAGE)
            '        End If
            '    End If
            'End If

        End If

    End Sub


    Public Shared Function MakeNewDate(parms As NewOfferParameters) As Integer
        Dim newOfferId As Integer


        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try
                Dim newOffer As New EUS_Offer()
                Dim parentOffer As EUS_Offer = (From itm In _CMSDBDataContext.EUS_Offers
                                                Where itm.OfferID = parms.parentOfferId
                                                Select itm).SingleOrDefault()



                newOffer.ToProfileID = parms.userIdReceiver
                newOffer.Amount = parms.offerAmount
                newOffer.FromProfileID = parms.userIdWhoDid
                newOffer.DateTimeToCreate = DateTime.UtcNow

                newOffer.Notes1 = parms.messageText1
                newOffer.Notes2 = parms.messageText2

                newOffer.OfferTypeID = ProfileHelper.OfferTypeID_NEWDATE
                newOffer.StatusID = ProfileHelper.OfferStatusID_UNLOCKED
                newOffer.IsDate = True

                If parentOffer IsNot Nothing Then
                    newOffer.ParrentOfferID = parms.parentOfferId
                End If
                newOffer.ChildOfferID = 0


                _CMSDBDataContext.EUS_Offers.InsertOnSubmit(newOffer)
                _CMSDBDataContext.SubmitChanges()

                newOfferId = newOffer.OfferID

                If parentOffer IsNot Nothing Then
                    parentOffer.ChildOfferID = newOffer.OfferID
                    _CMSDBDataContext.SubmitChanges()
                End If


                clsUserDoes.Update_ProfilesCommunication(parms.userIdWhoDid, parms.userIdReceiver, MemberActionTypeEnum.Dates, newOffer.OfferID)
                'clsUserNotifications.SendEmailNotification(NotificationType.Offers, newOffer.FromProfileID, newOffer.ToProfileID)
                clsUserDoes.ResetMemberCounter(MemberCountersEnum.NewDates, newOffer.ToProfileID)

            Catch ex As Exception
                Throw New Exception(ex.Message, ex)

            End Try
        End Using
        Return newOfferId
    End Function


    Public Shared Function HasDateOfferAlready(FromProfileID As Integer, ToProfileID As Integer) As Boolean
        Dim rec As Integer = 0

        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try
                rec = (From itm In _CMSDBDataContext.EUS_Offers
                      Where ((itm.FromProfileID = FromProfileID AndAlso itm.ToProfileID = ToProfileID) OrElse (itm.FromProfileID = ToProfileID AndAlso itm.ToProfileID = FromProfileID)) AndAlso _
                            itm.OfferTypeID = ProfileHelper.OfferTypeID_NEWDATE AndAlso _
                            itm.StatusID = ProfileHelper.OfferStatusID_UNLOCKED
                            Select itm).Count()


                If (rec = 0) Then
                    Dim var As EUS_ProfilesCommunication = clsUserDoes.Get_ProfilesCommunication(_CMSDBDataContext, FromProfileID, ToProfileID)
                    If (var IsNot Nothing) Then
                        rec = If(clsNullable.NullTo(var.IsDate), 1, 0)
                    End If
                    If (rec = 0) Then
                        var = clsUserDoes.Get_ProfilesCommunication(_CMSDBDataContext, ToProfileID, FromProfileID)
                        If (var IsNot Nothing) Then
                            rec = If(clsNullable.NullTo(var.IsDate), 1, 0)
                        End If
                    End If
                End If

            Catch ex As Exception
                Throw New Exception(ex.Message, ex)

            End Try
        End Using

        Return (rec > 0)
    End Function


    Public Shared Function GetDateOffer(FromProfileID As Integer, ToProfileID As Integer) As EUS_Offer
        Dim rec As EUS_Offer = Nothing

        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try
                rec = (From itm In _CMSDBDataContext.EUS_Offers
                      Where ((itm.FromProfileID = FromProfileID AndAlso itm.ToProfileID = ToProfileID) OrElse (itm.FromProfileID = ToProfileID AndAlso itm.ToProfileID = FromProfileID)) AndAlso _
                            itm.OfferTypeID = ProfileHelper.OfferTypeID_NEWDATE AndAlso _
                            itm.StatusID = ProfileHelper.OfferStatusID_UNLOCKED
                            Select itm).FirstOrDefault()
            Catch ex As Exception
                Throw

            End Try
        End Using
        Return rec
    End Function

    ''' <summary>
    ''' Marks EUS_ProfilesViewed table and EUS_ProfilePhotosLevel table, when one member is viewing other member's profile 
    ''' </summary>
    ''' <param name="ctx">CMSDBDataContext</param>
    ''' <param name="FromProfileID">Sender profile id</param>
    ''' <param name="ToProfileID">Current member, viewing other member's profile</param>
    ''' <param name="hasPrivatePhotos"></param>
    ''' <remarks></remarks>
    Public Shared Function MarkProfileDataAsViewed(ctx As CMSDBDataContext, FromProfileID As Integer, ToProfileID As Integer, hasPrivatePhotos As Boolean)
        Dim re As Boolean = False
        Dim rerunTran As Boolean = False
        Try

            For cnt = 0 To 2
                Try

                    Dim viewedMe As EUS_ProfilesViewed = (From itm In ctx.EUS_ProfilesVieweds
                                                        Where itm.FromProfileID = FromProfileID AndAlso
                                                        itm.ToProfileID = ToProfileID AndAlso
                                                        (itm.IsToProfileIDViewed Is Nothing OrElse itm.IsToProfileIDViewed = False)
                                                        Select itm).FirstOrDefault()

                    Dim sharedPhotosToMe As EUS_ProfilePhotosLevel = Nothing
                    If (hasPrivatePhotos) Then
                        sharedPhotosToMe = (From itm In ctx.EUS_ProfilePhotosLevels
                                            Where itm.FromProfileID = FromProfileID AndAlso
                                            itm.ToProfileID = ToProfileID AndAlso
                                            (itm.IsToProfileIDViewed Is Nothing OrElse itm.IsToProfileIDViewed = False)
                                            Select itm).FirstOrDefault()

                    End If

                    If (viewedMe IsNot Nothing OrElse sharedPhotosToMe IsNot Nothing) Then
                        If (viewedMe IsNot Nothing) Then viewedMe.IsToProfileIDViewed = True
                        If (sharedPhotosToMe IsNot Nothing) Then sharedPhotosToMe.IsToProfileIDViewed = True
                        If viewedMe IsNot Nothing OrElse sharedPhotosToMe IsNot Nothing Then
                            re = True
                        End If
                        ctx.SubmitChanges()
                        clsUserDoes.ResetMemberCounter(MemberCountersEnum.WhoViewedMe Or MemberCountersEnum.WhoSharedPhotos, ToProfileID)
                    End If

                    ' if qeury executed, exit for
                    Exit For

                Catch ex As System.Data.SqlClient.SqlException
                    rerunTran = DataHelpers.CheckSqlException(ex.Message)
                    If (Not rerunTran) Then Throw New Exception(ex.Message, ex)
                Catch ex As Exception
                    Throw New Exception(ex.Message, ex)
                Finally
                End Try


            Next
        Catch ex As Exception
        Finally
        End Try
        Return re
    End Function

    ''' <summary>
    ''' Mark the date EUS_Offers record as viewed by ToProfileID (set field IsToProfileIDViewedDate to true).
    ''' So this date, if exists, is not new any more for ToProfileID
    ''' </summary>
    ''' <param name="FromProfileID"></param>
    ''' <param name="ToProfileID"></param>
    ''' <remarks></remarks>
    Public Shared Sub MarkDateOfferAsViewed(FromProfileID As Integer, ToProfileID As Integer)

        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
            Try
                MarkDateOfferAsViewed(_CMSDBDataContext, FromProfileID, ToProfileID)
            Catch ex As Exception
                Throw New Exception(ex.Message, ex)

            End Try
        End Using


    End Sub
    ''' <summary>
    ''' Mark the date EUS_Offers record as viewed by ToProfileID (set field IsToProfileIDViewedDate to true).
    ''' So this date, if exists, is not new any more for ToProfileID
    ''' </summary>
    ''' <param name="FromProfileID"></param>
    ''' <param name="ToProfileID"></param>
    ''' <remarks></remarks>
    Public Shared Sub MarkDateOfferAsViewed(ctx As CMSDBDataContext, FromProfileID As Integer, ToProfileID As Integer)

        Dim rerunTran As Boolean = False
        Try

            For cnt = 0 To 2
                Try

                    Dim dateOffer As EUS_Offer = (From itm In ctx.EUS_Offers
                          Where ((itm.FromProfileID = FromProfileID AndAlso itm.ToProfileID = ToProfileID)) AndAlso _
                                itm.OfferTypeID = ProfileHelper.OfferTypeID_NEWDATE AndAlso _
                                itm.StatusID = ProfileHelper.OfferStatusID_UNLOCKED AndAlso
                                (itm.IsToProfileIDViewedDate Is Nothing OrElse itm.IsToProfileIDViewedDate = False)
                                Select itm).FirstOrDefault()

                    If (dateOffer IsNot Nothing) Then
                        dateOffer.IsToProfileIDViewedDate = True
                        ctx.SubmitChanges()

                        clsUserDoes.ResetMemberCounter(MemberCountersEnum.NewDates, ToProfileID)
                    End If

                    ' if qeury executed, exit for
                    Exit For

                Catch ex As System.Data.SqlClient.SqlException
                    rerunTran = DataHelpers.CheckSqlException(ex.Message)
                    If (Not rerunTran) Then Throw New Exception(ex.Message, ex)
                Catch ex As Exception
                    Throw New Exception(ex.Message, ex)
                Finally
                End Try


            Next
        Catch ex As Exception
            Throw New System.Exception(ex.Message, ex)
        Finally
        End Try

    End Sub


    Public Shared Sub AcceptLike(parms As NewOfferParameters, AcceptedOfferStatus As OfferStatusEnum)

        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try

                Dim parentOffer As EUS_Offer = (From itm In _CMSDBDataContext.EUS_Offers
                                                Where itm.OfferID = parms.parentOfferId
                                                Select itm).SingleOrDefault()

                If (AcceptedOfferStatus = OfferStatusEnum.LIKE_ACCEEPTED_WITH_OFFER) Then
                    parentOffer.StatusID = ProfileHelper.OfferStatusID_LIKE_ACCEEPTED_WITH_OFFER

                ElseIf (AcceptedOfferStatus = OfferStatusEnum.LIKE_ACCEEPTED_WITH_MESSAGE) Then
                    parentOffer.StatusID = ProfileHelper.OfferStatusID_LIKE_ACCEEPTED_WITH_MESSAGE

                ElseIf (AcceptedOfferStatus = OfferStatusEnum.LIKE_ACCEEPTED_WITH_POKE) Then
                    parentOffer.StatusID = ProfileHelper.OfferStatusID_LIKE_ACCEEPTED_WITH_POKE

                End If

                parentOffer.ChildOfferID = parms.childOfferId
                _CMSDBDataContext.SubmitChanges()

                'clsUserNotifications.SendEmailNotification(NotificationType.Offers, newOffer.FromProfileID, newOffer.ToProfileID)
                clsUserDoes.ResetMemberCounter(MemberCountersEnum.NewLikes, parentOffer.ToProfileID)
            Catch ex As Exception
                Throw New Exception(ex.Message, ex)

            End Try
        End Using
    End Sub


    Public Shared Sub AcceptLike(offerId As Integer, AcceptedOfferStatus As OfferStatusEnum)

        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try

                Dim parentOffer As EUS_Offer = (From itm In _CMSDBDataContext.EUS_Offers
                                                Where itm.OfferID = offerId
                                                Select itm).SingleOrDefault()

                If (AcceptedOfferStatus = OfferStatusEnum.LIKE_ACCEEPTED_WITH_OFFER) Then
                    parentOffer.StatusID = ProfileHelper.OfferStatusID_LIKE_ACCEEPTED_WITH_OFFER

                ElseIf (AcceptedOfferStatus = OfferStatusEnum.LIKE_ACCEEPTED_WITH_MESSAGE) Then
                    parentOffer.StatusID = ProfileHelper.OfferStatusID_LIKE_ACCEEPTED_WITH_MESSAGE

                ElseIf (AcceptedOfferStatus = OfferStatusEnum.LIKE_ACCEEPTED_WITH_POKE) Then
                    parentOffer.StatusID = ProfileHelper.OfferStatusID_LIKE_ACCEEPTED_WITH_POKE

                End If

                _CMSDBDataContext.SubmitChanges()
                clsUserDoes.ResetMemberCounter(MemberCountersEnum.NewLikes, parentOffer.ToProfileID)

                'clsUserNotifications.SendEmailNotification(NotificationType.Offers, newOffer.FromProfileID, newOffer.ToProfileID)
            Catch ex As Exception
                Throw New Exception(ex.Message, ex)

            End Try
        End Using
    End Sub


    Public Shared Sub AcceptPoke(parms As NewOfferParameters, AcceptedOfferStatus As OfferStatusEnum)

        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try

                Dim parentOffer As EUS_Offer = (From itm In _CMSDBDataContext.EUS_Offers
                                                Where itm.OfferID = parms.parentOfferId
                                                Select itm).SingleOrDefault()

                If (AcceptedOfferStatus = OfferStatusEnum.POKE_ACCEEPTED_WITH_OFFER) Then
                    parentOffer.StatusID = ProfileHelper.OfferStatusID_POKE_ACCEEPTED_WITH_OFFER

                ElseIf (AcceptedOfferStatus = OfferStatusEnum.POKE_ACCEEPTED_WITH_MESSAGE) Then
                    parentOffer.StatusID = ProfileHelper.OfferStatusID_POKE_ACCEEPTED_WITH_MESSAGE

                End If

                parentOffer.ChildOfferID = parms.childOfferId
                _CMSDBDataContext.SubmitChanges()

                clsUserDoes.ResetMemberCounter(MemberCountersEnum.NewLikes, parentOffer.ToProfileID)

                'clsUserNotifications.SendEmailNotification(NotificationType.Offers, newOffer.FromProfileID, newOffer.ToProfileID)
            Catch ex As Exception
                Throw New Exception(ex.Message, ex)

            End Try
        End Using
    End Sub



    Public Shared Sub AcceptPoke(offerId As Integer, AcceptedOfferStatus As OfferStatusEnum)

        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try

                Dim parentOffer As EUS_Offer = (From itm In _CMSDBDataContext.EUS_Offers
                                                Where itm.OfferID = offerId
                                                Select itm).SingleOrDefault()

                If (AcceptedOfferStatus = OfferStatusEnum.POKE_ACCEEPTED_WITH_OFFER) Then
                    parentOffer.StatusID = ProfileHelper.OfferStatusID_POKE_ACCEEPTED_WITH_OFFER

                ElseIf (AcceptedOfferStatus = OfferStatusEnum.POKE_ACCEEPTED_WITH_MESSAGE) Then
                    parentOffer.StatusID = ProfileHelper.OfferStatusID_POKE_ACCEEPTED_WITH_MESSAGE

                End If

                _CMSDBDataContext.SubmitChanges()

                clsUserDoes.ResetMemberCounter(MemberCountersEnum.NewLikes, parentOffer.ToProfileID)

                'clsUserNotifications.SendEmailNotification(NotificationType.Offers, newOffer.FromProfileID, newOffer.ToProfileID)
            Catch ex As Exception
                Throw New Exception(ex.Message, ex)

            End Try
        End Using
    End Sub


    Public Shared Sub CounterOffer(offerID As Integer, newAmount As Integer)

        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try

                If (offerID > 0) Then


                    Dim parentOffer As EUS_Offer = (From itm In _CMSDBDataContext.EUS_Offers
                                                    Where itm.OfferID = offerID
                                                    Select itm).SingleOrDefault()

                    Dim newOffer As New EUS_Offer()
                    newOffer.ToProfileID = parentOffer.FromProfileID
                    newOffer.Amount = newAmount
                    newOffer.FromProfileID = parentOffer.ToProfileID
                    newOffer.DateTimeToCreate = DateTime.UtcNow
                    newOffer.OfferTypeID = ProfileHelper.OfferTypeID_OFFERCOUNTER
                    newOffer.StatusID = ProfileHelper.OfferStatusID_COUNTER

                    newOffer.ParrentOfferID = offerID
                    newOffer.ChildOfferID = 0
                    newOffer.IsDate = False

                    _CMSDBDataContext.EUS_Offers.InsertOnSubmit(newOffer)
                    _CMSDBDataContext.SubmitChanges()


                    If (parentOffer IsNot Nothing) Then
                        parentOffer.StatusID = ProfileHelper.OfferStatusID_REPLACINGBYCOUNTER
                        parentOffer.ChildOfferID = newOffer.OfferID
                        _CMSDBDataContext.SubmitChanges()
                    End If

                    clsUserDoes.Update_ProfilesCommunication(parentOffer.FromProfileID, parentOffer.ToProfileID, MemberActionTypeEnum.Offer, newOffer.OfferID)

                    clsUserNotifications.SendEmailNotification(NotificationType.Offers, newOffer.FromProfileID, newOffer.ToProfileID)
                    clsUserDoes.ResetMemberCounter(MemberCountersEnum.NewOffers, newOffer.ToProfileID)
                End If

            Catch ex As Exception
                Throw New Exception(ex.Message, ex)

            End Try
        End Using
    End Sub


    'Public Sub MakeOffer(offerId As Integer)

    '    Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
    '    Try

    '        'If (offerId > 0) Then
    '        '    Dim q = (From itm In _CMSDBDataContext.EUS_Offers
    '        '            Where itm.OfferID = offerId
    '        '            Select itm).SingleOrDefault()

    '        '    If (q IsNot Nothing) Then
    '        '        q.StatusID = Lists.gDSLists.EUS_OffersStatus.Where(Function(itm) itm.ConstantName = OfferStatusEnum.ACCEPTED.ToString()).FirstOrDefault().EUS_OffersStatusID
    '        '        _CMSDBDataContext.SubmitChanges()
    '        '    End If
    '        'End If


    '        ' check
    '        Dim q = (From itm In _CMSDBDataContext.EUS_Offers
    '                Where itm.OfferID = offerId
    '                Select itm).SingleOrDefault()

    '        If (q IsNot Nothing) Then


    '            Dim newOffer As New EUS_Offer()
    '            newOffer.DateTimeToCreate = DateTime.UtcNow
    '            newOffer.FromProfileID = q.ToProfileID
    '            newOffer.OfferTypeID =  ProfileHelper.OfferTypeID_OFFERNEW
    '            newOffer.StatusID = OffersStatusID_PENDING
    '            newOffer.ToProfileID = q.FromProfileID
    '            newOffer.Amount = 0
    '            newOffer.ParrentOfferID = 0

    '            _CMSDBDataContext.EUS_Offers.InsertOnSubmit(newOffer)
    '            _CMSDBDataContext.SubmitChanges()

    '            q.ChildOfferID = newOffer.OfferID
    '        End If


    '    Catch ex As Exception
    '        Throw
    '    Finally
    '        _CMSDBDataContext.Dispose()
    '    End Try

    'End Sub




    Public Shared Function DeleteMessage(ProfileIdOwner As Integer, MessageID As Long, MakeHidden As Boolean, Optional deletePermanently As Boolean = False) As Integer
        Dim result As Integer = 0
        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try

                If (ProfileIdOwner > 0 AndAlso MessageID > 0) Then
                    If (Not deletePermanently) Then
                        result = _CMSDBDataContext.EUS_Messages_DeleteMessage(ProfileIdOwner, MessageID, MakeHidden)
                    Else
                        Dim message As EUS_Message = (From itm In _CMSDBDataContext.EUS_Messages
                                                        Where itm.EUS_MessageID = MessageID
                                                        Select itm).SingleOrDefault()

                        If (message IsNot Nothing) Then
                            _CMSDBDataContext.EUS_Messages.DeleteOnSubmit(message)
                            _CMSDBDataContext.SubmitChanges()
                        End If
                    End If

                    clsUserDoes.ResetMemberCounter(MemberCountersEnum.NewMessages, ProfileIdOwner)

                End If

            Catch ex As Exception
                Throw New Exception(ex.Message, ex)

            End Try
        End Using
        Return result
    End Function


    'Public Shared Function SendMessage(Subject As String, Body As String,
    '                               FromProfileID As Integer,
    '                               ToProfileID As Integer,
    '                               forceMessageCheck As Boolean,
    '                               replyToMessageId As Integer,
    '                               blockReceiving As Boolean) As EUS_Message

    '    Dim messageReceived As EUS_Message = SendMessage(Subject, Body,
    '                                                    FromProfileID,
    '                                                    ToProfileID,
    '                                                    forceMessageCheck,
    '                                                    replyToMessageId,
    '                                                    blockReceiving,
    '                                                    IsAuto:=False,
    '                                                    genderid:=Nothing)

    '    Return messageReceived
    'End Function


    Public Shared Function ReplaceLinksToDots(text As String) As String
        If (String.IsNullOrEmpty(text) OrElse text.Trim().Length = 0) Then
            Return text
        End If

        Dim substring1 As String = ""
        Dim httpNdxCount As Integer = 0
        Dim httpNdxEnd As Integer = -1
        Dim httpNdx As Integer = 0

        Try
            Do


                ' finde next 
                If (httpNdxEnd = -1) Then
                    httpNdx = text.IndexOf("http://", StringComparison.OrdinalIgnoreCase)
                Else
                    httpNdx = text.IndexOf("http://", httpNdxEnd, StringComparison.OrdinalIgnoreCase)
                End If
                If (httpNdx > -1) Then
                    httpNdxCount = "http://".Length
                    httpNdxEnd = httpNdx + httpNdxCount
                Else
                    httpNdxCount = 0
                    httpNdxEnd = -1
                End If

                If (httpNdx = -1) Then
                    If (httpNdxEnd = -1) Then
                        httpNdx = text.IndexOf("https://", StringComparison.OrdinalIgnoreCase)
                    Else
                        httpNdx = text.IndexOf("https://", httpNdxEnd, StringComparison.OrdinalIgnoreCase)
                    End If
                    If (httpNdx > -1) Then
                        httpNdxCount = "https://".Length
                        httpNdxEnd = httpNdx + httpNdxCount
                    Else
                        httpNdxCount = 0
                        httpNdxEnd = -1
                    End If
                End If

                If (httpNdx = -1) Then
                    If (httpNdxEnd = -1) Then
                        httpNdx = text.IndexOf("ftp://", StringComparison.OrdinalIgnoreCase)
                    Else
                        httpNdx = text.IndexOf("ftp://", httpNdxEnd, StringComparison.OrdinalIgnoreCase)
                    End If
                    If (httpNdx > -1) Then
                        httpNdxCount = "ftp://".Length
                        httpNdxEnd = httpNdx + httpNdxCount
                    Else
                        httpNdxCount = 0
                        httpNdxEnd = -1
                    End If
                End If

                If (httpNdx > -1) Then
                    substring1 = ""
                    ' find whole link
                    Dim nextNdx As Integer = text.IndexOfAny(New Char() {"<"c, ">"c, " "c, """"c, "'"c, System.Convert.ToChar(13), System.Convert.ToChar(10)}, httpNdxEnd)
                    If (nextNdx > -1) Then
                        substring1 = text.Substring(httpNdx, nextNdx - httpNdx)
                    Else
                        substring1 = text.Substring(httpNdx, text.Length - httpNdx)
                    End If

                    ' replace url
                    If (substring1.Length > 0) Then
                        text = text.Replace(substring1, New String("."c, substring1.Length))
                    End If
                End If

            Loop Until (httpNdx = -1)

        Catch
        End Try

        Return text
    End Function


    ''' <summary>
    ''' Returns EUS_Message object received by another user
    ''' </summary>
    ''' <param name="Subject"></param>
    ''' <param name="Body"></param>
    ''' <param name="FromProfileID"></param>
    ''' <param name="ToProfileID"></param>
    ''' <param name="replyToMessageId"></param>
    ''' <remarks></remarks>
    Public Shared Function SendMessage(Subject As String,
                                       Body As String,
                                       FromProfileID As Integer,
                                       ToProfileID As Integer,
                                       forceMessageCheck As Boolean,
                                       replyToMessageId As Integer,
                                       blockReceiving As Boolean,
                                       IsAuto As Boolean,
                                       genderid As Integer?) As EUS_Message


        Dim __logdate As DateTime = DateTime.UtcNow
        Dim messages_allow_links As Boolean = clsConfigValues.Get__messages_allow_links()
        If (Not messages_allow_links) Then
            If (FromProfileID <> 1 AndAlso ToProfileID <> 1) Then
                ' neither recipient, neither sender are administrator
                Body = ReplaceLinksToDots(Body)
            End If
        End If


        Dim messageReceived As New EUS_Message()
        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try
                Dim warnLvl As Integer?
                If (forceMessageCheck) Then
                    '  Dim checker As New ForbiddenWordsChecker()
                    Dim warnLvlSubject As Integer?, warnLvlBody As Integer?
                    warnLvlSubject = ForbiddenWordsChecker.CheckText(Subject)
                    warnLvlBody = ForbiddenWordsChecker.CheckText(Body)
                    warnLvl = warnLvlSubject + warnLvlBody
                End If

                Dim md5 As Guid = AppUtils.MD5_GetGuid(Body)

                Dim messageSent As New EUS_Message()
                messageSent.Body = Body
                messageSent.Subject = Subject
                messageSent.DateTimeToCreate = DateTime.UtcNow
                messageSent.FromProfileID = FromProfileID
                messageSent.ToProfileID = ToProfileID
                messageSent.StatusID = 1
                messageSent.ReplyToMessageId = replyToMessageId
                messageSent.IsSent = True
                messageSent.ProfileIDOwner = FromProfileID
                messageSent.MD5 = md5
                messageSent.IsAuto = IsAuto

                messageSent.IsDeleted = False
                messageSent.IsHidden = False
                messageSent.WarnLvl = 0
                messageSent.CopyMessageRead = False

                If (warnLvl.HasValue) Then
                    messageSent.WarnLvl = warnLvl
                End If
                If (blockReceiving) Then
                    messageSent.WarnLvl = clsNullable.NullTo(messageSent.WarnLvl, 1) * -1000
                    If (messageSent.WarnLvl = 0) Then messageSent.WarnLvl = -1000
                End If


                messageReceived.Body = Body
                messageReceived.Subject = Subject
                messageReceived.DateTimeToCreate = DateTime.UtcNow
                messageReceived.FromProfileID = FromProfileID
                messageReceived.ToProfileID = ToProfileID
                messageReceived.StatusID = 0
                messageReceived.ReplyToMessageId = replyToMessageId
                messageReceived.IsReceived = True
                messageReceived.ProfileIDOwner = ToProfileID
                messageReceived.IsDeleted = False
                messageReceived.IsHidden = False
                messageReceived.WarnLvl = messageSent.WarnLvl
                messageReceived.MD5 = md5
                messageReceived.IsAuto = IsAuto
                messageReceived.CopyMessageRead = False

                Dim isMessageBeingSent As Boolean =
                    clsNullable.NullTo(messageSent.WarnLvl, 0) < clsConfigValues.Get__message_check_forbidden_words_warning_threshold() AndAlso
                    Not blockReceiving

                If isMessageBeingSent Then
                    _CMSDBDataContext.EUS_Messages.InsertOnSubmit(messageReceived)
                End If

                _CMSDBDataContext.EUS_Messages.InsertOnSubmit(messageSent)
                _CMSDBDataContext.SubmitChanges()


                If isMessageBeingSent Then
                    messageSent.CopyMessageID = messageReceived.EUS_MessageID
                    messageReceived.CopyMessageID = messageSent.EUS_MessageID
                    _CMSDBDataContext.SubmitChanges()

                    clsUserDoes.ResetMemberCounter(MemberCountersEnum.NewMessages, messageReceived.ToProfileID)
                    clsUserNotifications.SendEmailNotification(NotificationType.Message, FromProfileID, ToProfileID)

                    clsUserDoes.Update_ProfilesCommunication(FromProfileID, ToProfileID, MemberActionTypeEnum.Message, messageSent.EUS_MessageID)

                    Try
                        If (clsProfilesPrivacySettings.GET_AdminSetting_IsVirtual(ToProfileID)) Then
                            Dim var1 As New SYS_ProfileReceivedMessage()
                            var1.CreatedDate = Date.UtcNow
                            var1.MessageID = messageReceived.EUS_MessageID
                            var1.ProfileID = ToProfileID
                            _CMSDBDataContext.SYS_ProfileReceivedMessages.InsertOnSubmit(var1)
                            _CMSDBDataContext.SubmitChanges()
                        End If
                    Catch
                    End Try

                    Try
                        If (genderid = 2) Then
                            DataHelpers.Check_IsSpammerProfile(FromProfileID, messageSent.MD5)
                        End If
                    Catch
                    End Try

                End If


                If (clsNullable.NullTo(messageSent.WarnLvl, 0) >= clsConfigValues.Get__message_check_forbidden_words_warning_threshold() / 2) Then
                    Dim fromLogin As String = DataHelpers.GetEUS_Profile_LoginName_ByProfileID(FromProfileID)
                    Dim toLogin As String = DataHelpers.GetEUS_Profile_LoginName_ByProfileID(ToProfileID)
                    Dim msgBody As String =
                        "Score: " & warnLvl & vbCrLf & _
                        "From: " & fromLogin & vbCrLf & _
                        "To: " & toLogin & vbCrLf & _
                        "Subject: " & Subject & vbCrLf & _
                        "Body:" & vbCrLf & Body

                    clsMyMail.SendMailSupport(ConfigurationManager.AppSettings("gToEmail"), "Forbidden words in message found", msgBody, False)
                End If


            Catch ex As Exception
                Throw New System.Exception(ex.Message, ex)
            Finally

                clsLogger.InsertLog("clsUserDoes-->SendMessage", FromProfileID, ToProfileID, (DateTime.UtcNow - __logdate).TotalMilliseconds)
            End Try
        End Using
        Return messageReceived
    End Function

    Public Shared Function SendMessageOnQuarantine(ProfileID As Integer, MessageId As Integer) As EUS_Message
        Dim messageReceived As EUS_Message = Nothing
        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try

                ' check the message, was not already sent 
                Dim isMessageSent As Integer = (From itm In _CMSDBDataContext.EUS_Messages
                                                 Where itm.CopyMessageID = MessageId
                                                 Select itm).Count()

                If (isMessageSent = 0) Then

                    Dim messageSent As EUS_Message = (From itm In _CMSDBDataContext.EUS_Messages
                                                     Where itm.EUS_MessageID = MessageId AndAlso itm.ProfileIDOwner = ProfileID
                                                     Select itm).FirstOrDefault()
                    If (messageSent IsNot Nothing) Then

                        If (messageSent.MD5 Is Nothing) Then
                            Dim md5 As Guid = AppUtils.MD5_GetGuid(messageSent.Body)
                            messageSent.MD5 = md5
                        End If

                        messageReceived = New EUS_Message()
                        messageReceived.Body = messageSent.Body
                        messageReceived.Subject = messageSent.Subject
                        messageReceived.DateTimeToCreate = messageSent.DateTimeToCreate
                        messageReceived.FromProfileID = messageSent.FromProfileID
                        messageReceived.ToProfileID = messageSent.ToProfileID
                        messageReceived.StatusID = 0
                        messageReceived.ReplyToMessageId = messageSent.ReplyToMessageId
                        messageReceived.IsReceived = True
                        messageReceived.ProfileIDOwner = messageSent.ToProfileID
                        messageReceived.IsDeleted = False
                        messageReceived.IsHidden = False
                        messageReceived.WarnLvl = messageSent.WarnLvl
                        messageReceived.MD5 = messageSent.MD5
                        messageReceived.IsAuto = messageSent.IsAuto
                        messageReceived.CopyMessageRead = False


                        _CMSDBDataContext.EUS_Messages.InsertOnSubmit(messageReceived)
                        _CMSDBDataContext.SubmitChanges()


                        If (clsNullable.NullTo(messageReceived.WarnLvl, 0) <= -1000) Then
                            messageSent.WarnLvl = messageSent.WarnLvl / -1000
                            messageReceived.WarnLvl = messageReceived.WarnLvl / -1000
                        End If

                        messageSent.CopyMessageID = messageReceived.EUS_MessageID
                        messageReceived.CopyMessageID = messageSent.EUS_MessageID
                        _CMSDBDataContext.SubmitChanges()

                        clsUserNotifications.SendEmailNotification(NotificationType.Message, messageSent.FromProfileID, messageSent.ToProfileID)
                        clsUserDoes.ResetMemberCounter(MemberCountersEnum.NewMessages, messageReceived.ToProfileID)
                        clsUserDoes.Update_ProfilesCommunication(messageReceived.FromProfileID, messageReceived.ToProfileID, MemberActionTypeEnum.Message, messageSent.EUS_MessageID)

                    End If


                End If

            Catch ex As Exception
                Throw New Exception(ex.Message, ex)

            End Try
        End Using
        Return messageReceived
    End Function


    Public Shared Sub SendMessageFromAdministrator(Subject As String, Body As String, ToProfileID As Integer)

        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try

                Dim messageReceived As New EUS_Message()
                messageReceived.Body = Body
                messageReceived.Subject = Subject
                messageReceived.DateTimeToCreate = DateTime.UtcNow
                messageReceived.ToProfileID = ToProfileID
                messageReceived.StatusID = 0
                messageReceived.IsReceived = True
                messageReceived.ProfileIDOwner = ToProfileID

                messageReceived.FromProfileID = 1
                messageReceived.ReplyToMessageId = 0
                messageReceived.CopyMessageID = 0
                messageReceived.IsDeleted = False
                messageReceived.IsHidden = False
                messageReceived.MD5 = New Guid()
                messageReceived.IsAuto = False
                messageReceived.WarnLvl = 0
                messageReceived.CopyMessageRead = False

                _CMSDBDataContext.EUS_Messages.InsertOnSubmit(messageReceived)
                _CMSDBDataContext.SubmitChanges()
                clsUserDoes.ResetMemberCounter(MemberCountersEnum.NewMessages, messageReceived.ToProfileID)

            Catch ex As Exception
                Throw New Exception(ex.Message, ex)

            End Try
        End Using
    End Sub



    Public Shared Sub MarkMessageRead(MsgID As Long)

        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try

                If (MsgID > 0) Then


                    Dim message As EUS_Message = (From itm In _CMSDBDataContext.EUS_Messages
                                                    Where itm.EUS_MessageID = MsgID AndAlso (itm.IsHidden Is Nothing OrElse itm.IsHidden = False)
                                                    Select itm).SingleOrDefault()


                    If (message IsNot Nothing) Then
                        message.StatusID = 1
                        message.DateRead = DateTime.UtcNow
                        _CMSDBDataContext.SubmitChanges()


                        Dim otherProfileid As Integer
                        If (message.ProfileIDOwner = message.FromProfileID) Then
                            otherProfileid = message.ToProfileID
                        Else
                            otherProfileid = message.FromProfileID
                        End If
                        If (otherProfileid > 1 AndAlso message.CopyMessageID IsNot Nothing) Then
                            DataHelpers.UpdateEUS_Messages_SetCopyMessageRead(otherProfileid, message.CopyMessageID, message.EUS_MessageID)
                        End If
                        clsUserDoes.ResetMemberCounter(MemberCountersEnum.NewMessages, message.ProfileIDOwner)
                    End If
                End If

            Catch ex As Exception
                Throw New Exception(ex.Message, ex)

            End Try
        End Using
    End Sub


    Public Shared Function GetLastMessageForProfiles(ProfileIDOwner As Integer, ProfileIDOther As Integer) As EUS_Message
        Dim message As EUS_Message = Nothing
        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try

                If (ProfileIDOwner > 0 AndAlso ProfileIDOther > 0) Then

                    message = (From itm In _CMSDBDataContext.EUS_Messages
                                Where ((itm.FromProfileID = ProfileIDOwner AndAlso itm.ToProfileID = ProfileIDOther) OrElse
                                       (itm.ToProfileID = ProfileIDOwner AndAlso itm.FromProfileID = ProfileIDOther)) AndAlso
                                      (itm.ProfileIDOwner = ProfileIDOwner) AndAlso
                                      (itm.IsHidden Is Nothing OrElse itm.IsHidden = False)
                                Order By itm.DateTimeToCreate Descending
                                Select itm).FirstOrDefault()

                End If

            Catch ex As Exception
                Throw New Exception(ex.Message, ex)

            End Try
        End Using
        Return message
    End Function


    Public Shared Function GetMessage(MsgID As Integer) As EUS_Message
        Dim message As EUS_Message = Nothing

        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try

                If (MsgID > 0) Then



                    Dim rerunTran As Boolean = False
                    Try

                        message = (From itm In _CMSDBDataContext.EUS_Messages
                                    Where itm.EUS_MessageID = MsgID AndAlso (itm.IsHidden Is Nothing OrElse itm.IsHidden = False)
                                    Select itm).SingleOrDefault()

                    Catch ex As System.Data.SqlClient.SqlException
                        rerunTran = DataHelpers.CheckSqlException(ex.Message)
                        If (Not rerunTran) Then Throw
                    End Try

                    If (rerunTran) Then
                        rerunTran = False
                        Try

                            message = (From itm In _CMSDBDataContext.EUS_Messages
                                        Where itm.EUS_MessageID = MsgID AndAlso (itm.IsHidden Is Nothing OrElse itm.IsHidden = False)
                                        Select itm).SingleOrDefault()

                        Catch ex As System.Data.SqlClient.SqlException
                            rerunTran = DataHelpers.CheckSqlException(ex.Message)
                            If (Not rerunTran) Then Throw
                        End Try
                    End If

                    If (rerunTran) Then
                        rerunTran = False

                        message = (From itm In _CMSDBDataContext.EUS_Messages
                                    Where itm.EUS_MessageID = MsgID AndAlso (itm.IsHidden Is Nothing OrElse itm.IsHidden = False)
                                    Select itm).SingleOrDefault()

                    End If

                End If

            Catch ex As Exception
                Throw New Exception(ex.Message, ex)

            End Try
        End Using
        Return message
    End Function


    Public Shared Function GetMessage_DateTimeToCreate(MsgID As Integer) As DateTime?
        Dim message As DateTime? = Nothing

        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try

                If (MsgID > 0) Then



                    Dim rerunTran As Boolean = False
                    Try

                        message = (From itm In _CMSDBDataContext.EUS_Messages
                                    Where itm.EUS_MessageID = MsgID AndAlso (itm.IsHidden Is Nothing OrElse itm.IsHidden = False)
                                    Select itm.DateTimeToCreate).SingleOrDefault()

                    Catch ex As System.Data.SqlClient.SqlException
                        rerunTran = DataHelpers.CheckSqlException(ex.Message)
                        If (Not rerunTran) Then Throw
                    End Try

                    If (rerunTran) Then
                        rerunTran = False
                        Try

                            message = (From itm In _CMSDBDataContext.EUS_Messages
                                        Where itm.EUS_MessageID = MsgID AndAlso (itm.IsHidden Is Nothing OrElse itm.IsHidden = False)
                                        Select itm.DateTimeToCreate).SingleOrDefault()

                        Catch ex As System.Data.SqlClient.SqlException
                            rerunTran = DataHelpers.CheckSqlException(ex.Message)
                            If (Not rerunTran) Then Throw
                        End Try
                    End If

                    If (rerunTran) Then
                        rerunTran = False

                        message = (From itm In _CMSDBDataContext.EUS_Messages
                                    Where itm.EUS_MessageID = MsgID AndAlso (itm.IsHidden Is Nothing OrElse itm.IsHidden = False)
                                    Select itm.DateTimeToCreate).SingleOrDefault()

                    End If

                End If

            Catch ex As Exception
                Throw New Exception(ex.Message, ex)

            End Try
        End Using
        Return message
    End Function



    Public Shared Function GetAllUnreadMessages(FromProfileID As Integer, ToProfileID As Integer) As List(Of EUS_Message)
        Dim messages As New List(Of EUS_Message)

        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try

                If (FromProfileID > 0 AndAlso ToProfileID > 0) Then


                    messages = (From itm In _CMSDBDataContext.EUS_Messages
                                Where itm.FromProfileID = FromProfileID AndAlso itm.ToProfileID = ToProfileID AndAlso itm.StatusID = 0 AndAlso (itm.IsHidden Is Nothing OrElse itm.IsHidden = False)
                                Select itm).ToList()

                End If

            Catch ex As Exception
                Throw New Exception(ex.Message, ex)

            End Try
        End Using
        Return messages
    End Function



    'Public Shared Function VerifyUserMaySendMessage(_CMSDBDataContext As CMSDBDataContext, messageSendToLoginName As String, currentUserProfileID As Integer)
    '    Dim result As New clsVerifyUserMaySendMessageResult()

    '    Dim prof As EUS_Profile = DataHelpers.GetEUS_ProfileByLoginName(_CMSDBDataContext, messageSendToLoginName, ProfileStatusEnum.Approved)
    '    If (prof IsNot Nothing) Then
    '        Dim acceptedOffer As EUS_Offer = (From ofr In _CMSDBDataContext.EUS_Offers
    '                                         Where ofr.StatusID = 50 AndAlso _
    '                                         ((ofr.ToProfileID = prof.ProfileID And ofr.FromProfileID = currentUserProfileID) OrElse _
    '                                          (ofr.ToProfileID = currentUserProfileID And ofr.FromProfileID = prof.ProfileID))
    '                                         Select ofr).FirstOrDefault()
    '        If (acceptedOffer IsNot Nothing) Then

    '            Dim defaultOtherPhoto As DSMembers.EUS_CustomerPhotosRow = DataHelpers.GetProfilesDefaultPhoto(currentUserProfileID)
    '            result.OtherMemberProfileViewUrl = System.Web.VirtualPathUtility.ToAbsolute("~/Members/Profile.aspx?p=" & prof.LoginName)
    '            result.OtherMemberLoginName = prof.LoginName

    '            If (Not defaultOtherPhoto.IsFileNameNull()) Then
    '                result.OtherMemberImageUrl = ProfileHelper.GetProfileImageURL(prof.ProfileID, defaultOtherPhoto.FileName, prof.GenderId, True)
    '            Else
    '                result.OtherMemberImageUrl = ProfileHelper.GetDefaultImageURL(prof.GenderId)
    '            End If


    '            If (acceptedOffer IsNot Nothing) Then
    '                result.DatingAmount = "&euro;" & acceptedOffer.Amount
    '            End If
    '        Else
    '            result.ErrorReason = clsVerifyUserMaySendMessageResult.ErrorReasonEnum.ErrorNoOfferCannotSendMessage
    '        End If
    '    Else
    '        result.ErrorReason = clsVerifyUserMaySendMessageResult.ErrorReasonEnum.ErrorMemberDoesNotExistCannotSendMessage
    '    End If

    '    Return result
    'End Function


    Public Shared Function GetProfilesThumbViews(fromProf As EUS_Profile,
                                                 fromProfPhoto As DSMembers.EUS_CustomerPhotosRow,
                                                 toProf As EUS_Profile,
                                                 toProfPhoto As DSMembers.EUS_CustomerPhotosRow,
                                                 isHTTPS As Boolean,
                                                 photoSize As PhotoSize) As clsLoadProfilesViewsResult
        Dim result As New clsLoadProfilesViewsResult()

        ' current user data
        result.CurrentMemberProfileID = fromProf.ProfileID
        result.CurrentMemberProfileViewUrl = System.Web.VirtualPathUtility.ToAbsolute("~/Members/Profile.aspx?p=" & HttpUtility.UrlEncode(fromProf.LoginName))
        result.CurrentMemberLoginName = fromProf.LoginName

        If (fromProfPhoto IsNot Nothing AndAlso Not fromProfPhoto.IsFileNameNull()) Then
            result.CurrentMemberImageUrl = ProfileHelper.GetProfileImageURL(fromProfPhoto.CustomerID, fromProfPhoto.FileName, fromProf.GenderId, True, isHTTPS, photoSize)
        Else
            result.CurrentMemberImageUrl = ProfileHelper.GetProfileImageURL(fromProf.ProfileID, Nothing, fromProf.GenderId, True, isHTTPS)
        End If


        ' other user data
        result.OtherMemberProfileID = toProf.ProfileID
        result.OtherMemberProfileViewUrl = System.Web.VirtualPathUtility.ToAbsolute("~/Members/Profile.aspx?p=" & HttpUtility.UrlEncode(toProf.LoginName))
        result.OtherMemberLoginName = toProf.LoginName

        If (toProfPhoto IsNot Nothing AndAlso toProfPhoto IsNot Nothing AndAlso Not toProfPhoto.IsFileNameNull()) Then
            result.OtherMemberImageUrl = ProfileHelper.GetProfileImageURL(toProfPhoto.CustomerID, toProfPhoto.FileName, toProf.GenderId, True, isHTTPS, photoSize)
        Else
            result.OtherMemberImageUrl = ProfileHelper.GetProfileImageURL(toProf.ProfileID, Nothing, toProf.GenderId, True, isHTTPS)
        End If

        Return result
    End Function


    Public Shared Function GetProfilesThumbViews(fromProf As vw_EusProfile_Light,
                                                 fromProfPhoto As DSMembers.EUS_CustomerPhotosRow,
                                                 toProf As EUS_Profile,
                                                 toProfPhoto As DSMembers.EUS_CustomerPhotosRow,
                                                 isHTTPS As Boolean,
                                                 photoSize As PhotoSize) As clsLoadProfilesViewsResult
        Dim result As New clsLoadProfilesViewsResult()

        ' current user data
        result.CurrentMemberProfileID = fromProf.ProfileID
        result.CurrentMemberProfileViewUrl = System.Web.VirtualPathUtility.ToAbsolute("~/Members/Profile.aspx?p=" & HttpUtility.UrlEncode(fromProf.LoginName))
        result.CurrentMemberLoginName = fromProf.LoginName

        If (fromProfPhoto IsNot Nothing AndAlso Not fromProfPhoto.IsFileNameNull()) Then
            result.CurrentMemberImageUrl = ProfileHelper.GetProfileImageURL(fromProfPhoto.CustomerID, fromProfPhoto.FileName, fromProf.GenderId, True, isHTTPS, photoSize)
        Else
            result.CurrentMemberImageUrl = ProfileHelper.GetProfileImageURL(fromProf.ProfileID, Nothing, fromProf.GenderId, True, isHTTPS)
        End If


        ' other user data
        result.OtherMemberProfileID = toProf.ProfileID
        result.OtherMemberProfileViewUrl = System.Web.VirtualPathUtility.ToAbsolute("~/Members/Profile.aspx?p=" & HttpUtility.UrlEncode(toProf.LoginName))
        result.OtherMemberLoginName = toProf.LoginName

        If (toProfPhoto IsNot Nothing AndAlso toProfPhoto IsNot Nothing AndAlso Not toProfPhoto.IsFileNameNull()) Then
            result.OtherMemberImageUrl = ProfileHelper.GetProfileImageURL(toProfPhoto.CustomerID, toProfPhoto.FileName, toProf.GenderId, True, isHTTPS, photoSize)
        Else
            result.OtherMemberImageUrl = ProfileHelper.GetProfileImageURL(toProf.ProfileID, Nothing, toProf.GenderId, True, isHTTPS)
        End If

        Return result
    End Function


    Public Shared Function GetProfilesThumbViews(fromProf As EUS_Profile, fromProfPhoto As EUS_CustomerPhoto, toProf As EUS_Profile, toProfPhoto As EUS_CustomerPhoto, isHTTPS As Boolean) As clsLoadProfilesViewsResult
        Dim result As New clsLoadProfilesViewsResult()

        ' current user data
        result.CurrentMemberProfileID = fromProf.ProfileID
        result.CurrentMemberProfileViewUrl = System.Web.VirtualPathUtility.ToAbsolute("~/Members/Profile.aspx?p=" & HttpUtility.UrlEncode(fromProf.LoginName))
        result.CurrentMemberLoginName = fromProf.LoginName

        If (Not String.IsNullOrEmpty(fromProfPhoto.FileName)) Then
            result.CurrentMemberImageUrl = ProfileHelper.GetProfileImageURL(fromProf.ProfileID, fromProfPhoto.FileName, fromProf.GenderId, True, isHTTPS)
        Else
            result.CurrentMemberImageUrl = ProfileHelper.GetDefaultImageURL(fromProf.GenderId)
        End If


        ' other user data
        result.OtherMemberProfileID = toProf.ProfileID
        result.OtherMemberProfileViewUrl = System.Web.VirtualPathUtility.ToAbsolute("~/Members/Profile.aspx?p=" & HttpUtility.UrlEncode(toProf.LoginName))
        result.OtherMemberLoginName = toProf.LoginName

        If (toProfPhoto IsNot Nothing AndAlso Not String.IsNullOrEmpty(toProfPhoto.FileName)) Then
            result.OtherMemberImageUrl = ProfileHelper.GetProfileImageURL(toProf.ProfileID, toProfPhoto.FileName, toProf.GenderId, True, isHTTPS)
        Else
            result.OtherMemberImageUrl = ProfileHelper.GetDefaultImageURL(toProf.GenderId)

        End If

        Return result
    End Function


    Shared Function UnlockOfferConversation(offerID As Integer, creditsAmount As Integer, userIdWhoUnlocks As Integer, EUS_CreditsTypeID As Integer, IsSubscription As Boolean)
        Dim UnlockedConversationId As Integer
        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try

                If (offerID > 0 AndAlso creditsAmount > 0) Then



                    Dim q = (From itm In _CMSDBDataContext.EUS_Offers
                            Where itm.OfferID = offerID
                            Select itm).SingleOrDefault()



                    If (q IsNot Nothing) Then
                        Dim conversationProfileid As Integer
                        If (userIdWhoUnlocks = q.FromProfileID) Then
                            conversationProfileid = q.ToProfileID
                        Else
                            conversationProfileid = q.FromProfileID
                        End If

                        Dim _HasCommunication As Boolean = clsUserDoes.HasCommunication(conversationProfileid, userIdWhoUnlocks)
                        If (Not _HasCommunication) Then


                            'Dim qEUS_CustomerCredit = Nothing
                            'If (userIdWhoUnlocks = q.FromProfileID) Then

                            '    qEUS_CustomerCredit = (From itm In _CMSDBDataContext.EUS_CustomerCredits
                            '       Where itm.CustomerId = userIdWhoUnlocks AndAlso itm.ConversationWithCustomerId = q.ToProfileID
                            '       Select itm).SingleOrDefault()

                            'Else
                            '    qEUS_CustomerCredit = (From itm In _CMSDBDataContext.EUS_CustomerCredits
                            '       Where itm.CustomerId = userIdWhoUnlocks AndAlso itm.ConversationWithCustomerId = q.FromProfileID
                            '       Select itm).SingleOrDefault()

                            'End If

                            'If (qEUS_CustomerCredit Is Nothing) Then

                            'End If
                            Dim _REF_CRD2EURO_Rate As Double = DataHelpers.GetEUS_Profiles_REF_CRD2EURO_Rate(userIdWhoUnlocks)

                            'Dim customerCreditsId As Integer =
                            _CMSDBDataContext.ConsumeCredits(userIdWhoUnlocks,
                                                         -(Math.Abs(creditsAmount)),
                                                         -(Math.Abs(creditsAmount)) * _REF_CRD2EURO_Rate,
                                                         EUS_CreditsTypeID,
                                                         conversationProfileid,
                                                         False,
                                                         0,
                                                         IsSubscription)

                            'Dim customerCredits As New EUS_CustomerCredit()
                            'customerCredits.Credits = -(creditsAmount)
                            'customerCredits.CustomerId = userIdWhoUnlocks
                            'customerCredits.DateTimeCreated = DateTime.UtcNow
                            'customerCredits.CreditsTypeId = EUS_CreditsTypeID
                            'customerCredits.ConversationWithCustomerId = conversationProfileid
                            'customerCredits.EuroAmount = customerCredits.Credits * _REF_CRD2EURO_Rate

                            ''q.StatusID = OfferStatusID_UNLOCKED
                            '_CMSDBDataContext.EUS_CustomerCredits.InsertOnSubmit(customerCredits)


                            Dim unlockedConvers As New EUS_UnlockedConversation()
                            If (Not _HasCommunication) Then
                                unlockedConvers.DateTimeCreated = DateTime.UtcNow
                                unlockedConvers.FromProfileId = userIdWhoUnlocks
                                unlockedConvers.ToProfileId = conversationProfileid
                                unlockedConvers.CurrentOfferId = q.OfferID
                                unlockedConvers.CurrentOfferAmount = q.Amount

                                _CMSDBDataContext.EUS_UnlockedConversations.InsertOnSubmit(unlockedConvers)

                                clsUserNotifications.SendEmailNotification(NotificationType.Unlock, userIdWhoUnlocks, conversationProfileid)
                            End If

                            _CMSDBDataContext.SubmitChanges()
                            UnlockedConversationId = unlockedConvers.UnlockedConversationId
                        End If



                        If (UnlockedConversationId > 0 AndAlso Not clsUserDoes.HasDateOfferAlready(q.FromProfileID, q.ToProfileID)) Then
                            Dim parms As New NewOfferParameters()
                            parms.messageText1 = ""
                            parms.messageText2 = ""
                            parms.offerAmount = q.Amount
                            parms.parentOfferId = q.OfferID
                            parms.userIdReceiver = q.ToProfileID
                            parms.userIdWhoDid = q.FromProfileID

                            clsUserDoes.MakeNewDate(parms)
                        End If

                    End If

                End If

            Catch ex As Exception
                Throw New Exception(ex.Message, ex)

            End Try
        End Using

        Return UnlockedConversationId
    End Function

    Shared Function UnlockMessageOnce(ToProfileID As Integer, FromProfileID As Integer, creditsAmount As Double, unlockType As UnlockType, messageId As Long, REF_CRD2EURO_Rate As Double?, IsSubscription As Boolean) As Long
        Dim customerCreditsId As Long = 0

        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)

            Try
                'Dim creditsAmount As Integer = ProfileHelper.Config_UNLOCK_MESSAGE
                Dim EUS_CreditsTypeID As Integer = clsUserDoes.GetRequiredCredits(unlockType).EUS_CreditsTypeID
                Dim _REF_CRD2EURO_Rate As Double = 0
                If (REF_CRD2EURO_Rate.HasValue) Then
                    _REF_CRD2EURO_Rate = REF_CRD2EURO_Rate
                Else
                    _REF_CRD2EURO_Rate = DataHelpers.GetEUS_Profiles_REF_CRD2EURO_Rate(FromProfileID)
                End If

                'Dim customerCredits As New EUS_CustomerCredit()
                'customerCredits.Credits = -(creditsAmount)
                'customerCredits.CustomerId = FromProfileID
                'customerCredits.DateTimeCreated = DateTime.UtcNow
                'customerCredits.CreditsTypeId = EUS_CreditsTypeID
                'customerCredits.EuroAmount = customerCredits.Credits * _REF_CRD2EURO_Rate
                'customerCredits.ConversationWithCustomerId = ToProfileID
                '_CMSDBDataContext.EUS_CustomerCredits.InsertOnSubmit(customerCredits)
                '_CMSDBDataContext.SubmitChanges()
                'customerCreditsId = customerCredits.CustomerCreditsId

                customerCreditsId = _CMSDBDataContext.ConsumeCredits(FromProfileID,
                                                                     -(Math.Abs(creditsAmount)),
                                                                     -(Math.Abs(creditsAmount)) * _REF_CRD2EURO_Rate,
                                                                     EUS_CreditsTypeID,
                                                                     ToProfileID,
                                                                     False,
                                                                     messageId,
                                                                     IsSubscription)

            Catch ex As Exception
                Throw New Exception(ex.Message, ex)
            End Try

        End Using
        Return customerCreditsId
    End Function



    Shared Function UnlockMessageUnlimited(ToProfileID As Integer, FromProfileID As Integer, OfferId As Integer, IsSubscription As Boolean) As Long
        Dim customerCreditsId As Long = 0

        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try

                If (Not clsUserDoes.HasCommunication(ToProfileID, FromProfileID)) Then

                    Dim creditsAmount As Integer = ProfileHelper.Config_UNLOCK_CONVERSATION_CREDITS
                    Dim EUS_CreditsTypeID As Integer = clsUserDoes.GetRequiredCredits(UnlockType.UNLOCK_CONVERSATION).EUS_CreditsTypeID
                    Dim _REF_CRD2EURO_Rate As Double = DataHelpers.GetEUS_Profiles_REF_CRD2EURO_Rate(FromProfileID)

                    'Dim customerCredits As New EUS_CustomerCredit()
                    'customerCredits.Credits = -(creditsAmount)
                    'customerCredits.CustomerId = FromProfileID
                    'customerCredits.DateTimeCreated = DateTime.UtcNow
                    'customerCredits.CreditsTypeId = EUS_CreditsTypeID
                    'customerCredits.EuroAmount = customerCredits.Credits * _REF_CRD2EURO_Rate
                    'customerCredits.ConversationWithCustomerId = ToProfileID
                    '_CMSDBDataContext.EUS_CustomerCredits.InsertOnSubmit(customerCredits)


                    customerCreditsId = _CMSDBDataContext.ConsumeCredits(FromProfileID,
                                                                         -(Math.Abs(creditsAmount)),
                                                                         -(Math.Abs(creditsAmount)) * _REF_CRD2EURO_Rate,
                                                                         EUS_CreditsTypeID,
                                                                         ToProfileID,
                                                                         False,
                                                                         0,
                                                                         IsSubscription)

                    Dim unlockedConvers As New EUS_UnlockedConversation()
                    unlockedConvers.DateTimeCreated = DateTime.UtcNow
                    unlockedConvers.FromProfileId = FromProfileID
                    unlockedConvers.ToProfileId = ToProfileID
                    unlockedConvers.CurrentOfferId = OfferId
                    _CMSDBDataContext.EUS_UnlockedConversations.InsertOnSubmit(unlockedConvers)


                    _CMSDBDataContext.SubmitChanges()
                    'customerCreditsId = customerCredits.CustomerCreditsId

                    clsUserNotifications.SendEmailNotification(NotificationType.Unlock, FromProfileID, ToProfileID)
                End If


            Catch ex As Exception
                Throw New Exception(ex.Message, ex)
            End Try
        End Using
        Return customerCreditsId
    End Function


    Shared Function UnlockMessageConversation(messageId As Integer, creditsAmount As Integer, userIdWhoUnlocks As Integer, EUS_CreditsTypeID As Integer, IsSubscription As Boolean) As Long
        Dim customerCreditsId As Long = 0

        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try

                If (messageId > 0 AndAlso creditsAmount > 0) Then



                    Dim q = (From itm In _CMSDBDataContext.EUS_Messages
                            Where itm.EUS_MessageID = messageId AndAlso (itm.IsHidden Is Nothing OrElse itm.IsHidden = False)
                            Select itm).SingleOrDefault()

                    If (q IsNot Nothing) Then

                        Dim _REF_CRD2EURO_Rate As Double = DataHelpers.GetEUS_Profiles_REF_CRD2EURO_Rate(userIdWhoUnlocks)
                        Dim ConversationWithCustomerId As Integer
                        If (userIdWhoUnlocks = q.FromProfileID) Then
                            ConversationWithCustomerId = q.ToProfileID
                        Else
                            ConversationWithCustomerId = q.FromProfileID
                        End If

                        customerCreditsId = _CMSDBDataContext.ConsumeCredits(userIdWhoUnlocks,
                                                                             -(Math.Abs(creditsAmount)),
                                                                             -(Math.Abs(creditsAmount)) * _REF_CRD2EURO_Rate,
                                                                             EUS_CreditsTypeID,
                                                                             ConversationWithCustomerId,
                                                                             False,
                                                                             messageId,
                                                                             IsSubscription)

                        'Dim customerCredits As New EUS_CustomerCredit()
                        'customerCredits.Credits = -(creditsAmount)
                        'customerCredits.CustomerId = userIdWhoUnlocks
                        'customerCredits.DateTimeCreated = DateTime.UtcNow
                        'customerCredits.CreditsTypeId = EUS_CreditsTypeID
                        'customerCredits.EuroAmount = customerCredits.Credits * _REF_CRD2EURO_Rate
                        'customerCredits.ConversationWithCustomerId=ConversationWithCustomerId


                        ''q.StatusID = OfferStatusID_UNLOCKED
                        '_CMSDBDataContext.EUS_CustomerCredits.InsertOnSubmit(customerCredits)


                        If (Not clsUserDoes.HasCommunication(ConversationWithCustomerId, userIdWhoUnlocks)) Then
                            Dim unlockedConvers As New EUS_UnlockedConversation()
                            unlockedConvers.DateTimeCreated = DateTime.UtcNow
                            unlockedConvers.FromProfileId = userIdWhoUnlocks
                            unlockedConvers.ToProfileId = ConversationWithCustomerId


                            Dim qoffer = (From itm In _CMSDBDataContext.EUS_Offers
               Where ((itm.FromProfileID = q.FromProfileID AndAlso itm.ToProfileID = q.ToProfileID) OrElse _
                    (itm.FromProfileID = q.ToProfileID AndAlso itm.ToProfileID = q.FromProfileID)) AndAlso _
                   itm.StatusID = ProfileHelper.OfferStatusID_ACCEPTED
               Select itm).SingleOrDefault()

                            If (qoffer IsNot Nothing) Then
                                unlockedConvers.CurrentOfferId = qoffer.OfferID
                                unlockedConvers.CurrentOfferAmount = qoffer.Amount
                            End If


                            _CMSDBDataContext.EUS_UnlockedConversations.InsertOnSubmit(unlockedConvers)

                            clsUserNotifications.SendEmailNotification(NotificationType.Unlock, userIdWhoUnlocks, ConversationWithCustomerId)
                        End If

                        _CMSDBDataContext.SubmitChanges()

                        'customerCreditsId = customerCredits.CustomerCreditsId

                    End If

                End If

            Catch ex As Exception
                Throw New Exception(ex.Message, ex)

            End Try
        End Using
        Return customerCreditsId
    End Function



    Shared Function GetAcceptedOrUnlockedOffer(otherUserId As Integer, currentUserId As Integer) As EUS_Offer
        Dim acceptedOffer As EUS_Offer = Nothing
        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)



            Dim rerunTran As Boolean = False
            Try

                For cnt = 0 To 2
                    Try

                        If (otherUserId > 0 AndAlso currentUserId > 0) Then
                            acceptedOffer = (From ofr In _CMSDBDataContext.EUS_Offers
                                                     Where (ofr.StatusID = ProfileHelper.OfferStatusID_ACCEPTED OrElse ofr.StatusID = ProfileHelper.OfferStatusID_LIKE_ACCEEPTED_WITH_POKE OrElse ofr.StatusID = ProfileHelper.OfferStatusID_LIKE_ACCEEPTED_WITH_MESSAGE OrElse ofr.StatusID = ProfileHelper.OfferStatusID_LIKE_ACCEEPTED_WITH_OFFER) AndAlso _
                                                     ((ofr.ToProfileID = otherUserId And ofr.FromProfileID = currentUserId) OrElse _
                                                      (ofr.ToProfileID = currentUserId And ofr.FromProfileID = otherUserId))
                                                     Select ofr).FirstOrDefault()
                            ' OrElse ofr.StatusID = OfferStatusID_UNLOCKED

                        End If


                        ' if qeury executed, exit for
                        Exit For


                    Catch ex As System.Data.SqlClient.SqlException
                        rerunTran = DataHelpers.CheckSqlException(ex.Message)
                        If (Not rerunTran) Then Throw New Exception(ex.Message, ex)
                    Catch ex As Exception
                        Throw New Exception(ex.Message, ex)
                    Finally
                    End Try


                Next
            Catch ex As Exception
                Throw New System.Exception(ex.Message, ex)

            End Try
        End Using
        Return acceptedOffer
    End Function


    Public Shared Function HasPhotos(userProfileId As Integer, userMirrorProfileId As Integer) As Boolean
        Dim rerunTran As Boolean = False
        Dim currentUserHasPhotos As Integer

        If (Not (userProfileId > 0 OrElse userMirrorProfileId > 0)) Then
            Return False
        End If

        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try

                Try

                    currentUserHasPhotos = (From phot In _CMSDBDataContext.EUS_CustomerPhotos
                                           Where (phot.CustomerID = userProfileId OrElse phot.CustomerID = userMirrorProfileId) AndAlso _
                                           phot.HasAproved = True AndAlso (phot.IsDeleted = 0)
                                           Select phot).Count()

                Catch ex As System.Data.SqlClient.SqlException
                    rerunTran = DataHelpers.CheckSqlException(ex.Message)
                    If (Not rerunTran) Then Throw
                End Try

                If (rerunTran) Then
                    rerunTran = False
                    Try

                        currentUserHasPhotos = (From phot In _CMSDBDataContext.EUS_CustomerPhotos
                                               Where (phot.CustomerID = userProfileId OrElse phot.CustomerID = userMirrorProfileId) AndAlso _
                                               phot.HasAproved = True AndAlso (phot.IsDeleted = 0)
                                               Select phot).Count()


                    Catch ex As System.Data.SqlClient.SqlException
                        rerunTran = DataHelpers.CheckSqlException(ex.Message)
                        If (Not rerunTran) Then Throw
                    End Try
                End If

                If (rerunTran) Then
                    rerunTran = False

                    currentUserHasPhotos = (From phot In _CMSDBDataContext.EUS_CustomerPhotos
                                           Where (phot.CustomerID = userProfileId OrElse phot.CustomerID = userMirrorProfileId) AndAlso _
                                           phot.HasAproved = True AndAlso (phot.IsDeleted = 0)
                                           Select phot).Count()

                End If

            Catch ex As Exception
                Throw New Exception(ex.Message, ex)

            End Try
        End Using
        Return (currentUserHasPhotos > 0)
    End Function


    'Public Shared Function GetRequiredCredits_UnlockConversation() As EUS_CreditsType
    '    Dim creditsRequired As EUS_CreditsType

    '    Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
    '    Try

    '        creditsRequired = (From itm In _CMSDBDataContext.EUS_CreditsTypes
    '                     Where itm.CreditsType = "UNLOCK_CONVERSATION"
    '                     Select itm).FirstOrDefault()


    '    Catch ex As Exception
    '        Throw
    '    Finally
    '        _CMSDBDataContext.Dispose()
    '    End Try

    '    Return creditsRequired
    'End Function

    'Public Shared Function GetRequiredCredits_UnlockMessage() As EUS_CreditsType
    '    Dim creditsRequired As EUS_CreditsType

    '    Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
    '    Try

    '        creditsRequired = (From itm In _CMSDBDataContext.EUS_CreditsTypes
    '                     Where itm.CreditsType = "UNLOCK_MESSAGE"
    '                     Select itm).FirstOrDefault()


    '    Catch ex As Exception
    '        Throw
    '    Finally
    '        _CMSDBDataContext.Dispose()
    '    End Try

    '    Return creditsRequired
    'End Function


    Public Shared Function GetRequiredCredits(type As UnlockType) As EUS_CreditsType
        Dim creditsRequired As EUS_CreditsType = clsCreditsHelper.GetRequiredCredits(type)
        Return creditsRequired
    End Function



    Public Shared Function HasRequiredCredits(MasterProfileId As Integer, type As UnlockType) As Boolean
        Dim success = False

        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try
                '   Dim gotoBilling = False

                Dim sqlProc As String = "EXEC [CustomerAvailableCredits] @CustomerId = " & MasterProfileId
                Dim dt As DataTable = DataHelpers.GetDataTable(sqlProc)

                Dim totalMemberCredits As Integer = 0
                If (Not dt.Rows(0).IsNull("AvailableCredits")) Then
                    totalMemberCredits = dt.Rows(0)("AvailableCredits")
                End If

                Try


                    Dim creditsRequired As Integer = 10000000

                    If (type = UnlockType.UNLOCK_CONVERSATION) Then
                        creditsRequired = ProfileHelper.Config_UNLOCK_CONVERSATION_CREDITS

                    ElseIf (type = UnlockType.UNLOCK_MESSAGE_READ) Then
                        creditsRequired = ProfileHelper.Config_UNLOCK_MESSAGE_READ_CREDITS

                    ElseIf (type = UnlockType.UNLOCK_MESSAGE_SEND) Then
                        creditsRequired = ProfileHelper.Config_UNLOCK_MESSAGE_SEND_CREDITS
                    End If



                    If (totalMemberCredits >= creditsRequired) Then success = True
                Catch ex As Exception

                End Try


            Catch ex As Exception
                Throw New Exception(ex.Message, ex)

            End Try
        End Using

        Return success
    End Function


    Public Shared Function GetCustomerAvailableCredits(MasterProfileId As Integer) As clsCustomerAvailableCredits
        Dim customerAvailableCredits As New clsCustomerAvailableCredits()

        Try
            ' Dim gotoBilling = False

            Dim sqlProc As String = "EXEC [CustomerAvailableCredits] @CustomerId = " & MasterProfileId
            Using dt As DataTable = DataHelpers.GetDataTable(sqlProc)
                customerAvailableCredits.AvailableCredits = clsNullable.DBNullToInteger(dt.Rows(0)("AvailableCredits"))
                customerAvailableCredits.CreditsRecordsCount = clsNullable.DBNullToInteger(dt.Rows(0)("CreditsRecordsCount"))
                customerAvailableCredits.HasSubscription = clsNullable.DBNullToBoolean(dt.Rows(0)("HasSubscription"))
            End Using



        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        Finally

        End Try


        Return customerAvailableCredits
    End Function


    Public Shared Function GetMemberActionsCounters(MasterProfileId As Integer) As clsGetMemberActionsCounters
        Dim memberActionsCounters As New clsGetMemberActionsCounters()

        Try
            'Dim sqlProc As String = "EXEC [GetMemberActionsCounters] @ProfileID=" & MasterProfileId
            Dim sqlProc As String = "EXEC [GetNewCounters2] @CurrentProfileId=" & MasterProfileId
            Using dt As DataTable = DataHelpers.GetDataTable(sqlProc)



                memberActionsCounters.CountWhoViewedMe = clsNullable.DBNullToInteger(dt.Rows(0)("CountWhoViewedMe"))
                memberActionsCounters.CountWhoFavoritedMe = clsNullable.DBNullToInteger(dt.Rows(0)("CountWhoFavoritedMe"))
                memberActionsCounters.CountWhoSharedPhotos = clsNullable.DBNullToInteger(dt.Rows(0)("CountWhoSharedPhotos"))
                memberActionsCounters.NewDates = clsNullable.DBNullToInteger(dt.Rows(0)("NewDates"))
                memberActionsCounters.NewLikes = clsNullable.DBNullToInteger(dt.Rows(0)("NewWinks"))
                memberActionsCounters.NewOffers = clsNullable.DBNullToInteger(dt.Rows(0)("NewOffers"))
                memberActionsCounters.NewMessages = clsNullable.DBNullToInteger(dt.Rows(0)("NewMessages"))

                'If (Not dt.Rows(0).IsNull("CountWhoViewedMe")) Then memberActionsCounters.CountWhoViewedMe = dt.Rows(0)("CountWhoViewedMe")
                'If (Not dt.Rows(0).IsNull("CountWhoFavoritedMe")) Then memberActionsCounters.CountWhoFavoritedMe = dt.Rows(0)("CountWhoFavoritedMe")
                'If (Not dt.Rows(0).IsNull("CountWhoSharedPhotos")) Then memberActionsCounters.CountWhoSharedPhotos = dt.Rows(0)("CountWhoSharedPhotos")
                'If (Not dt.Rows(0).IsNull("NewDates")) Then memberActionsCounters.NewDates = dt.Rows(0)("NewDates")
                'If (Not dt.Rows(0).IsNull("NewWinks")) Then memberActionsCounters.NewLikes = dt.Rows(0)("NewWinks")
                'If (Not dt.Rows(0).IsNull("NewOffers")) Then memberActionsCounters.NewOffers = dt.Rows(0)("NewOffers")
                'If (Not dt.Rows(0).IsNull("NewMessages")) Then memberActionsCounters.NewMessages = dt.Rows(0)("NewMessages")
            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        Finally

        End Try


        Return memberActionsCounters
    End Function



    'Public Shared Function HasRequiredCredits_To_UnlockConversation(MasterProfileId As Integer) As Boolean
    '    Dim success = False

    '    Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
    '    Try
    '        Dim gotoBilling = False

    '        Dim sqlProc As String = "EXEC [CustomerAvailableCredits] @CustomerId = " & MasterProfileId
    '        Dim dt As DataTable = DataHelpers.GetDataTable(sqlProc)

    '        Dim totalMemberCredits As Integer = 0
    '        If (Not dt.Rows(0).IsNull("AvailableCredits")) Then
    '            totalMemberCredits = dt.Rows(0)("AvailableCredits")
    '        End If

    '        Try

    '            Dim creditsRequired As EUS_CreditsType = (From itm In _CMSDBDataContext.EUS_CreditsTypes
    '                     Where itm.CreditsType = "UNLOCK_CONVERSATION"
    '                     Select itm).FirstOrDefault()


    '            If (totalMemberCredits >= creditsRequired.CreditsAmount) Then success = True
    '        Catch ex As Exception

    '        End Try


    '    Catch ex As Exception
    '        Throw
    '    Finally
    '        _CMSDBDataContext.Dispose()
    '    End Try


    '    Return success
    'End Function

    'Public Shared Function HasRequiredCredits_To_UnlockMessage(MasterProfileId As Integer) As Boolean
    '    Dim success = False

    '    Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
    '    Try
    '        Dim gotoBilling = False

    '        Dim sqlProc As String = "EXEC [CustomerAvailableCredits] @CustomerId = " & MasterProfileId
    '        Dim dt As DataTable = DataHelpers.GetDataTable(sqlProc)

    '        Dim totalMemberCredits As Integer = 0
    '        If (Not dt.Rows(0).IsNull("AvailableCredits")) Then
    '            totalMemberCredits = dt.Rows(0)("AvailableCredits")
    '        End If

    '        Try

    '            Dim creditsRequired As EUS_CreditsType = (From itm In _CMSDBDataContext.EUS_CreditsTypes
    '                     Where itm.CreditsType = "UNLOCK_MESSAGE"
    '                     Select itm).FirstOrDefault()


    '            If (totalMemberCredits >= creditsRequired.CreditsAmount) Then success = True
    '        Catch ex As Exception

    '        End Try


    '    Catch ex As Exception
    '        Throw
    '    Finally
    '        _CMSDBDataContext.Dispose()
    '    End Try


    '    Return success
    'End Function




    Public Shared Function PerformOfferUnlock(offerID As Integer, MasterProfileId As Integer) As Boolean
        Dim success = False

        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try
                Dim gotoBilling = False
                Dim totalMemberCredits As Integer = 0
                Dim sqlProc As String = "EXEC [CustomerAvailableCredits] @CustomerId = " & MasterProfileId
                Using dt As DataTable = DataHelpers.GetDataTable(sqlProc)




                    If (Not dt.Rows(0).IsNull("AvailableCredits")) Then
                        totalMemberCredits = dt.Rows(0)("AvailableCredits")
                    End If
                End Using
                Try
                    gotoBilling = True

                    Dim creditsRequired As EUS_CreditsType = (From itm In _CMSDBDataContext.EUS_CreditsTypes
                             Where itm.CreditsType = "UNLOCK_CONVERSATION"
                             Select itm).FirstOrDefault()


                    If (totalMemberCredits >= creditsRequired.CreditsAmount) Then
                        clsUserDoes.UnlockOfferConversation(offerID, creditsRequired.CreditsAmount, MasterProfileId, creditsRequired.EUS_CreditsTypeID, False)

                        gotoBilling = False
                    End If
                Catch ex As Exception

                End Try


                If (gotoBilling) Then
                    clsSessionVariables.GetCurrent().UserUnlockInfo.OfferId = offerID
                    clsSessionVariables.GetCurrent().UserUnlockInfo.ReturnUrl = HttpContext.Current.Request.Url.AbsoluteUri

                    Dim billingurl As String = System.Web.VirtualPathUtility.ToAbsolute("~/Members/SelectProduct.aspx") & _
                            ("?offer=" & offerID) & _
                            ("&returnurl=" & System.Web.HttpUtility.UrlEncode(HttpContext.Current.Request.Url.AbsoluteUri))

                    HttpContext.Current.Response.Redirect(billingurl, False)
                Else
                    success = True
                End If
            Catch ex As Exception
                Throw New Exception(ex.Message, ex)

            End Try
        End Using

        Return success
    End Function


    Public Shared Sub LogReporting(FromProfileId As Integer, ToProfileId As Integer, ReportingReason As Integer, OtherText As String)

        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try

                Dim report As New EUS_ReportingProfile()
                report.ToProfileId = ToProfileId
                report.FromProfileId = FromProfileId
                report.DateCreated = DateTime.UtcNow
                report.OtherText = OtherText
                report.ReportingReasonId = ReportingReason


                _CMSDBDataContext.EUS_ReportingProfiles.InsertOnSubmit(report)
                _CMSDBDataContext.SubmitChanges()

            Catch ex As Exception
                Throw New Exception(ex.Message, ex)

            End Try
        End Using
    End Sub




    Public Shared Function DeleteOffers_ClearHistory(ProfileIdOwner As Integer,
                                                       ClearDateUntil As DateTime) As Integer
        Dim result As Integer = 0
        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)

            Try

                If (ProfileIdOwner > 0) Then

                    result = _CMSDBDataContext.EUS_Offers_DeleteOffers(ProfileIdOwner,
                                                                    ClearDateUntil,
                                                                    Nothing)

                    clsUserDoes.ResetMemberCounter(MemberCountersEnum.NewOffers Or MemberCountersEnum.NewDates Or MemberCountersEnum.NewLikes, ProfileIdOwner)
                End If

            Catch ex As Exception
                Throw New Exception(ex.Message, ex)

            End Try

        End Using
        Return result
    End Function

    Public Shared Function COUNT_DeleteOffers_ClearHistory(ProfileIdOwner As Integer,
                                                       ClearDateUntil As DateTime) As EUS_Offers_DeleteOffers_CountResult
        Dim result As EUS_Offers_DeleteOffers_CountResult = Nothing
        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try

                If (ProfileIdOwner > 0) Then

                    result = _CMSDBDataContext.EUS_Offers_DeleteOffers_Count(ProfileIdOwner,
                                                                    ClearDateUntil,
                                                                    Nothing).FirstOrDefault()

                End If

            Catch ex As Exception
                Throw New Exception(ex.Message, ex)

            End Try
        End Using
        Return result
    End Function




    Public Shared Function DeleteLikes_ClearHistory(ProfileIdOwner As Integer,
                                                       ClearDateUntil As DateTime) As Integer
        Dim result As Integer = 0
        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try

                If (ProfileIdOwner > 0) Then

                    result = _CMSDBDataContext.EUS_Offers_DeleteLikes(ProfileIdOwner,
                                                                    ClearDateUntil,
                                                                    Nothing)

                    clsUserDoes.ResetMemberCounter(MemberCountersEnum.NewOffers Or MemberCountersEnum.NewDates Or MemberCountersEnum.NewLikes, ProfileIdOwner)
                End If

            Catch ex As Exception
                Throw New Exception(ex.Message, ex)

            End Try
        End Using
        Return result
    End Function

    Public Shared Function COUNT_DeleteLikes_ClearHistory(ProfileIdOwner As Integer,
                                                       ClearDateUntil As DateTime) As EUS_Offers_DeleteLikes_CountResult
        Dim result As EUS_Offers_DeleteLikes_CountResult = Nothing
        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try

                If (ProfileIdOwner > 0) Then

                    result = _CMSDBDataContext.EUS_Offers_DeleteLikes_Count(ProfileIdOwner,
                                                                    ClearDateUntil,
                                                                    Nothing).FirstOrDefault()

                End If

            Catch ex As Exception
                Throw New Exception(ex.Message, ex)

            End Try
        End Using
        Return result
    End Function


    Public Shared Function DeleteDates_ClearHistory(ProfileIdOwner As Integer,
                                                       ClearDateUntil As DateTime) As Integer
        Dim result As Integer = 0
        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try

                If (ProfileIdOwner > 0) Then

                    result = _CMSDBDataContext.EUS_Offers_DeleteDates(ProfileIdOwner,
                                                                    ClearDateUntil,
                                                                    Nothing)

                    clsUserDoes.ResetMemberCounter(MemberCountersEnum.NewOffers Or MemberCountersEnum.NewDates Or MemberCountersEnum.NewLikes, ProfileIdOwner)
                End If

            Catch ex As Exception
                Throw New Exception(ex.Message, ex)

            End Try
        End Using
        Return result
    End Function

    Public Shared Function COUNT_DeleteDates_ClearHistory(ProfileIdOwner As Integer,
                                                       ClearDateUntil As DateTime) As EUS_Offers_DeleteDates_CountResult
        Dim result As EUS_Offers_DeleteDates_CountResult = Nothing
        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try

                If (ProfileIdOwner > 0) Then

                    result = _CMSDBDataContext.EUS_Offers_DeleteDates_Count(ProfileIdOwner,
                                                                    ClearDateUntil,
                                                                    Nothing).FirstOrDefault()

                End If

            Catch ex As Exception
                Throw New Exception(ex.Message, ex)

            End Try
        End Using
        Return result
    End Function


    Public Shared Function DeletePhotosShares_ClearHistory(ProfileIdOwner As Integer,
                                                    ClearIncoming As Boolean?,
                                                    ClearOutgoing As Boolean?,
                                                    ClearDateUntil As DateTime) As Integer
        Dim result As Integer = 0
        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try

                If (ProfileIdOwner > 0) Then

                    result = _CMSDBDataContext.EUS_ProfilePhotosLevel_DeletePhotosShares(ProfileIdOwner,
                                                                                         ClearIncoming,
                                                                                         ClearOutgoing,
                                                                                            ClearDateUntil,
                                                                                            Nothing)

                    clsUserDoes.ResetMemberCounter(MemberCountersEnum.WhoSharedPhotos, ProfileIdOwner)
                End If

            Catch ex As Exception
                Throw New Exception(ex.Message, ex)

            End Try
        End Using
        Return result
    End Function

    Public Shared Function COUNT_DeletePhotosShares_ClearHistory(ProfileIdOwner As Integer,
                                                    ClearIncoming As Boolean?,
                                                    ClearOutgoing As Boolean?,
                                                       ClearDateUntil As DateTime) As EUS_ProfilePhotosLevel_DeletePhotosShares_CountResult
        Dim result As EUS_ProfilePhotosLevel_DeletePhotosShares_CountResult = Nothing
        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try

                If (ProfileIdOwner > 0) Then

                    result = _CMSDBDataContext.EUS_ProfilePhotosLevel_DeletePhotosShares_Count(ProfileIdOwner,
                                                                                                 ClearIncoming,
                                                                                                 ClearOutgoing,
                                                                                           ClearDateUntil,
                                                                                            Nothing).FirstOrDefault()

                End If

            Catch ex As Exception
                Throw New Exception(ex.Message, ex)

            End Try
        End Using
        Return result
    End Function


    Public Shared Function DeleteProfilesViewed_ClearHistory(ProfileIdOwner As Integer,
                                                    ClearIncoming As Boolean?,
                                                    ClearOutgoing As Boolean?,
                                                    ClearDateUntil As DateTime) As Integer
        Dim result As Integer = 0
        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try

                If (ProfileIdOwner > 0) Then

                    result = _CMSDBDataContext.EUS_ProfilesViewed_DeleteViews(ProfileIdOwner,
                                                                                         ClearIncoming,
                                                                                         ClearOutgoing,
                                                                                            ClearDateUntil,
                                                                                            Nothing)

                    clsUserDoes.ResetMemberCounter(MemberCountersEnum.WhoViewedMe, ProfileIdOwner)
                End If

            Catch ex As Exception
                Throw New Exception(ex.Message, ex)

            End Try
        End Using
        Return result
    End Function

    Public Shared Function COUNT_DeleteProfilesViewed_ClearHistory(ProfileIdOwner As Integer,
                                                    ClearIncoming As Boolean?,
                                                    ClearOutgoing As Boolean?,
                                                       ClearDateUntil As DateTime) As EUS_ProfilesViewed_DeleteViews_CountResult

        Dim result As EUS_ProfilesViewed_DeleteViews_CountResult = Nothing
        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try

                If (ProfileIdOwner > 0) Then

                    result = _CMSDBDataContext.EUS_ProfilesViewed_DeleteViews_Count(ProfileIdOwner,
                                                                                                 ClearIncoming,
                                                                                                 ClearOutgoing,
                                                                                           ClearDateUntil,
                                                                                            Nothing).FirstOrDefault()

                End If

            Catch ex As Exception
                Throw New Exception(ex.Message, ex)

            End Try
        End Using
        Return result
    End Function


    Public Shared Function GetLikesStatistics(ProfileIdOwner As Integer, dateFrom As DateTime, dateTo As DateTime) As EUS_Offers_LikesStatisticsResult
        Dim result As EUS_Offers_LikesStatisticsResult = Nothing
        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try

                If (ProfileIdOwner > 0) Then

                    result = _CMSDBDataContext.EUS_Offers_LikesStatistics(ProfileIdOwner, dateFrom, dateTo).FirstOrDefault()

                End If

            Catch ex As Exception
                Throw

            End Try
        End Using
        Return result
    End Function


    Public Shared Function GetMessagesStatistics(ProfileIdOwner As Integer, dateFrom As DateTime, dateTo As DateTime) As EUS_Messages_StatisticsResult
        Dim result As EUS_Messages_StatisticsResult = Nothing
        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try

                If (ProfileIdOwner > 0) Then

                    result = _CMSDBDataContext.EUS_Messages_Statistics(ProfileIdOwner, dateFrom, dateTo).FirstOrDefault()

                End If

            Catch ex As Exception
                Throw

            End Try
        End Using
        Return result
    End Function



    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")> Public Shared Sub Update_ProfilesCommunication(FromProfileId As Integer, ToProfileId As Integer, actionType As MemberActionTypeEnum, lastActionId As Long)

        Dim isMessage As Boolean?,
            isLike As Boolean?,
            isOffer As Boolean?,
            isPoke As Boolean?,
            isDate As Boolean?,
            isFavorite As Boolean?,
            isUnfavorite As Boolean? ',
        '  isViewed As Boolean?

        If ((actionType And MemberActionTypeEnum.Dates) > 0) Then isDate = True
        If ((actionType And MemberActionTypeEnum.Favorite) > 0) Then isFavorite = True
        If ((actionType And MemberActionTypeEnum.Likes) > 0) Then isLike = True
        If ((actionType And MemberActionTypeEnum.Message) > 0) Then isMessage = True
        If ((actionType And MemberActionTypeEnum.Offer) > 0) Then isOffer = True
        If ((actionType And MemberActionTypeEnum.Poke) > 0) Then isPoke = True
        If ((actionType And MemberActionTypeEnum.Unfavorite) > 0) Then isUnfavorite = True
        ' If ((actionType And MemberActionTypeEnum.Viewed) > 0) Then isViewed = True

        'End Sub


        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try

                Try

                    Dim var As EUS_ProfilesCommunication = (From itm In _CMSDBDataContext.EUS_ProfilesCommunications
                                                            Where itm.FromProfileID = FromProfileId AndAlso itm.ToProfileID = ToProfileId
                                                            Select itm).FirstOrDefault()

                    If (var Is Nothing) Then
                        var = New EUS_ProfilesCommunication()
                        _CMSDBDataContext.EUS_ProfilesCommunications.InsertOnSubmit(var)
                    End If

                    var.FromProfileID = FromProfileId
                    var.ToProfileID = ToProfileId

                    If (var.SentMessageID Is Nothing) Then var.SentMessageID = 0
                    If (var.SentMessage Is Nothing) Then var.SentMessage = 0
                    If (clsNullable.NullTo(isMessage) = True) Then
                        var.SentMessageID = lastActionId
                        var.SentMessage = 1
                        var.LastDateSentMessage = DateTime.UtcNow
                    End If

                    If (var.SentLikeID Is Nothing) Then var.SentLikeID = 0
                    If (var.SentLike Is Nothing) Then var.SentLike = 0
                    If (clsNullable.NullTo(isLike) = True) Then
                        var.SentLikeID = lastActionId
                        var.SentLike = 1
                        var.LastDateSentLike = DateTime.UtcNow
                    End If

                    If (var.SentOfferID Is Nothing) Then var.SentOfferID = 0
                    If (var.SentOffer Is Nothing) Then var.SentOffer = 0
                    If (clsNullable.NullTo(isOffer) = True) Then
                        var.SentOfferID = lastActionId
                        var.SentOffer = 1
                        var.LastDateSentOffer = DateTime.UtcNow
                    End If

                    If (var.SentPokeID Is Nothing) Then var.SentPokeID = 0
                    If (var.SentPoke Is Nothing) Then var.SentPoke = 0
                    If (clsNullable.NullTo(isPoke) = True) Then
                        var.SentPokeID = lastActionId
                        var.SentPoke = 1
                        var.LastDateSentPoke = DateTime.UtcNow
                    End If

                    var.IsDate = If(isDate.HasValue, isDate, If(var.IsDate IsNot Nothing, var.IsDate, False))
                    var.MakeFavorite = If(isFavorite.HasValue, isFavorite, If(var.MakeFavorite IsNot Nothing, var.MakeFavorite, False))
                    var.MakeUnfavorite = If(isUnfavorite.HasValue, isUnfavorite, If(var.MakeUnfavorite IsNot Nothing, var.MakeUnfavorite, False))

                    If (clsNullable.NullTo(isDate) = True) Then var.LastDateMakeDate = DateTime.UtcNow
                    If (clsNullable.NullTo(isFavorite) = True) Then var.LastDateMakeFavorite = DateTime.UtcNow
                    If (clsNullable.NullTo(isUnfavorite) = True) Then var.LastDateMakeUnfavorite = DateTime.UtcNow

                Catch ex As Exception
                    Throw New Exception(ex.Message, ex)
                End Try


                If (((actionType And MemberActionTypeEnum.Message) > 0) OrElse
                    ((actionType And MemberActionTypeEnum.Likes) > 0) OrElse
                    ((actionType And MemberActionTypeEnum.Offer) > 0) OrElse
                    ((actionType And MemberActionTypeEnum.DefaultPhotoCanged) > 0)) Then

                    Update_MobileNotifications(FromProfileId, ToProfileId, actionType, _CMSDBDataContext)

                End If


                Dim var2 As New SYS_MemberActionPerformQueue()
                _CMSDBDataContext.SYS_MemberActionPerformQueues.InsertOnSubmit(var2)

                var2.DateTimeToCreate = Date.UtcNow
                var2.FromProfileID = FromProfileId
                var2.ToProfileID = ToProfileId
                var2.Type = actionType

                _CMSDBDataContext.SubmitChanges()

            Catch ex As Exception
                Throw New Exception(ex.Message, ex)

            End Try
        End Using
    End Sub

    Public Shared Sub Update_MobileNotifications(FromProfileId As Integer, ToProfileId As Integer, actionType As MemberActionTypeEnum, ByRef _CMSDBDataContext As CMSDBDataContext)

        Dim var1 As New SYS_MobileNotificationsQueue()
        _CMSDBDataContext.SYS_MobileNotificationsQueues.InsertOnSubmit(var1)

        var1.DateTimeToCreate = Date.UtcNow
        var1.FromProfileID = FromProfileId
        var1.ToProfileID = ToProfileId
        If ((actionType And MemberActionTypeEnum.Message) > 0) Then var1.Type = 3
        If ((actionType And MemberActionTypeEnum.Likes) > 0) Then var1.Type = 2
        If ((actionType And MemberActionTypeEnum.Offer) > 0) Then var1.Type = 1
        If ((actionType And MemberActionTypeEnum.DefaultPhotoCanged) > 0) Then var1.Type = 4

    End Sub

    Public Shared Sub Update_MobileNotifications(FromProfileId As Integer, ToProfileId As Integer, actionType As MemberActionTypeEnum)
        Try
            Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
                Update_MobileNotifications(FromProfileId, ToProfileId, actionType, _CMSDBDataContext)
                _CMSDBDataContext.SubmitChanges()
            End Using

        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        End Try
    End Sub


    Public Shared Function Get_ProfilesCommunication(ctx As CMSDBDataContext, FromProfileId As Integer, ToProfileId As Integer) As EUS_ProfilesCommunication
        Dim var As EUS_ProfilesCommunication = Nothing
        'Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try
            var = (From itm In ctx.EUS_ProfilesCommunications
                Where itm.FromProfileID = FromProfileId AndAlso itm.ToProfileID = ToProfileId
                Select itm).FirstOrDefault()

            'If (var Is Nothing) Then
            '    var = New EUS_ProfilesCommunication()
            'End If

        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        Finally
            ' _CMSDBDataContext.Dispose()
        End Try
        Return var
    End Function



    Public Shared Function MaySendFreeMessage(ToProfileId As Integer, FromProfileID As Integer, FromCountry As String) As Boolean
        Dim SendFree = False
        'Return SendFree


        Dim ctr As SYS_CountriesGEO = SYS_CountriesGEOHelper.SYS_CountriesGEO_GetForCountry(FromCountry)
        If (clsNullable.NullTo(ctr.MonthlySubscriptionEnabled) = True) Then

        Else

            If (Not ctr.MalePayOnMessageSend) Then
                SendFree = True
            Else
                If (ctr.MaxFreeMessagesSend > 0) Then
                    ' count messages, user sent from his profile to other profile
                    Dim sql As String = <sql><![CDATA[
select  recs=count(*)
from (
	select distinct FromProfileID,ToProfileID,Body
	from  EUS_Messages
	where FromProfileID=@FromProfileID
	and ToProfileID=@ToProfileID
	and DateTimeToCreate>=@fromDate
	and DateTimeToCreate<@toDate
) as t
]]></sql>
                    Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                        Using cmd As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(con, sql)


                            cmd.Parameters.AddWithValue("@FromProfileID", FromProfileID)
                            cmd.Parameters.AddWithValue("@ToProfileID", ToProfileId)
                            cmd.Parameters.AddWithValue("@fromDate", DateTime.UtcNow.Date)
                            cmd.Parameters.AddWithValue("@toDate", DateTime.UtcNow.Date.AddDays(1))
                            Dim recs As Integer = DataHelpers.ExecuteScalar(cmd)

                            If (recs < ctr.MaxFreeMessagesSend) Then
                                SendFree = True
                            End If
                        End Using
                    End Using
                End If
            End If

        End If

        Return SendFree
    End Function



    Public Shared Function MayReadFreeMessage(ToProfileId As Integer, FromProfileID As Integer, FromCountry As String) As Boolean
        Dim ReadFree = False
        'Return ReadFree

        Dim ctr As SYS_CountriesGEO = SYS_CountriesGEOHelper.SYS_CountriesGEO_GetForCountry(FromCountry)


        If (clsNullable.NullTo(ctr.MonthlySubscriptionEnabled) = True) Then

        Else

            If (ctr.MalePayOnMessageRead) Then
                If (ctr.MaxFreeMessagesRead > 0) Then
                    ' count messages, user sent from his profile to other profile
                    Dim sql As String = <sql><![CDATA[
select  recs=count(*)
from (
	select distinct FromProfileID,ToProfileID,Body
	from  EUS_Messages
	where FromProfileID=@FromProfileID
	and ToProfileID=@ToProfileID
	and DateTimeToCreate>=@fromDate
	and DateTimeToCreate<@toDate
) as t
]]></sql>
                    Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                        Using cmd As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(con, sql)


                            cmd.Parameters.AddWithValue("@FromProfileID", FromProfileID)
                            cmd.Parameters.AddWithValue("@ToProfileID", ToProfileId)
                            cmd.Parameters.AddWithValue("@fromDate", DateTime.UtcNow.Date)
                            cmd.Parameters.AddWithValue("@toDate", DateTime.UtcNow.Date.AddDays(1))
                            Dim recs As Integer = DataHelpers.ExecuteScalar(cmd)

                            If (recs < ctr.MaxFreeMessagesRead) Then
                                ReadFree = True
                            End If
                        End Using
                    End Using
                End If
            Else
                ReadFree = True
            End If

        End If

        Return ReadFree
    End Function



    Public Shared Sub ResetMemberCounter(counter As MemberCountersEnum, profileId As Integer)
        If (profileId <= 1) Then Return
        Dim sql As String = ""

        If ((counter And MemberCountersEnum.WhoViewedMe) > 0) Then
            sql = sql & vbCrLf & "update [EUS_ProfileCounters] set  WhoViewedMe=null  where ProfileID=" & profileId
        End If

        If ((counter And MemberCountersEnum.WhoFavoritedMe) > 0) Then
            sql = sql & vbCrLf & "update [EUS_ProfileCounters] set  WhoFavoritedMe=null  where ProfileID=" & profileId
        End If

        If ((counter And MemberCountersEnum.WhoSharedPhotos) > 0) Then
            sql = sql & vbCrLf & "update [EUS_ProfileCounters] set  WhoSharedPhotos=null  where ProfileID=" & profileId
        End If

        If ((counter And MemberCountersEnum.NewDates) > 0) Then
            sql = sql & vbCrLf & "update [EUS_ProfileCounters] set  NewDates=null  where ProfileID=" & profileId
        End If

        If ((counter And MemberCountersEnum.NewLikes) > 0) Then
            sql = sql & vbCrLf & "update [EUS_ProfileCounters] set  NewWinks=null  where ProfileID=" & profileId
        End If

        If ((counter And MemberCountersEnum.NewOffers) > 0) Then
            sql = sql & vbCrLf & "update [EUS_ProfileCounters] set  NewOffers=null  where ProfileID=" & profileId
        End If

        If ((counter And MemberCountersEnum.NewMessages) > 0) Then
            sql = sql & vbCrLf & "update [EUS_ProfileCounters] set  NewMessages=null  where ProfileID=" & profileId
        End If

        If (Not String.IsNullOrEmpty(sql)) Then
            DataHelpers.ExecuteNonQuery(sql)
        End If

    End Sub


    Public Shared Sub ResetMemberCounter(counter As MemberCountersEnum, usersInList As List(Of Integer))

        Dim sql As New System.Text.StringBuilder()

        While (usersInList.Count > 0)
            For cnt As Integer = usersInList.Count - 1 To cnt = 0 Step -1
                Dim profileId As Integer = usersInList(cnt)

                If ((counter And MemberCountersEnum.WhoViewedMe) > 0) Then
                    sql.AppendLine("update [EUS_ProfileCounters] set  WhoViewedMe=null  where ProfileID=" & profileId)
                End If

                If ((counter And MemberCountersEnum.WhoFavoritedMe) > 0) Then
                    sql.AppendLine("update [EUS_ProfileCounters] set  WhoFavoritedMe=null  where ProfileID=" & profileId)
                End If

                If ((counter And MemberCountersEnum.WhoSharedPhotos) > 0) Then
                    sql.AppendLine("update [EUS_ProfileCounters] set  WhoSharedPhotos=null  where ProfileID=" & profileId)
                End If

                If ((counter And MemberCountersEnum.NewDates) > 0) Then
                    sql.AppendLine("update [EUS_ProfileCounters] set  NewDates=null  where ProfileID=" & profileId)
                End If

                If ((counter And MemberCountersEnum.NewLikes) > 0) Then
                    sql.AppendLine("update [EUS_ProfileCounters] set  NewWinks=null  where ProfileID=" & profileId)
                End If

                If ((counter And MemberCountersEnum.NewOffers) > 0) Then
                    sql.AppendLine("update [EUS_ProfileCounters] set  NewOffers=null  where ProfileID=" & profileId)
                End If

                If ((counter And MemberCountersEnum.NewMessages) > 0) Then
                    sql.AppendLine("update [EUS_ProfileCounters] set  NewMessages=null  where ProfileID=" & profileId)
                End If

                usersInList.RemoveAt(cnt)
                If (cnt = 100) Then Exit For
            Next

            If (sql.Length > 10) Then
                DataHelpers.ExecuteNonQuery(sql.ToString())
            End If
        End While

    End Sub


End Class

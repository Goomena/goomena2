﻿Imports System.Security.Cryptography
Imports System.Text

Public Class clsEncryptDecryptDes
    Private Shared ReadOnly iv() As Byte = {8, 7, 6, 5, 4, 3, 2, 1}
    Public Shared Function decrypt(encryptedQueryString As String) As String

        Dim buffer As Byte() = Convert.FromBase64String(encryptedQueryString)
        Using des As New TripleDESCryptoServiceProvider
            Using MD5 As New MD5CryptoServiceProvider
                des.Key = MD5.ComputeHash(ASCIIEncoding.ASCII.GetBytes("SignalRNotificationForGoomena"))
                des.IV = iv
                Return Encoding.ASCII.GetString(des.CreateDecryptor().TransformFinalBlock(buffer, 0, buffer.Length))
            End Using
        End Using
    End Function
    Public Shared Function decrypt(encryptedQueryString As String, ByRef Export As String) As Boolean
        Dim re As Boolean = False
        Try
            Dim buffer As Byte() = Convert.FromBase64String(encryptedQueryString)

            Using des As New TripleDESCryptoServiceProvider
                Using MD5 As New MD5CryptoServiceProvider
                    des.Key = MD5.ComputeHash(ASCIIEncoding.ASCII.GetBytes("SignalRNotificationForGoomena"))
                    des.IV = iv
                    Export = Encoding.ASCII.GetString(des.CreateDecryptor().TransformFinalBlock(buffer, 0, buffer.Length))
                    re = True
                End Using
            End Using
        Catch ex As Exception
        End Try
        Return re
    End Function
    Public Shared Function encrypt(serializedQueryString As String) As String
        Dim buffer As Byte() = Encoding.ASCII.GetBytes(serializedQueryString)
        Using des As New TripleDESCryptoServiceProvider
            Using MD5 As New MD5CryptoServiceProvider
                des.Key = MD5.ComputeHash(ASCIIEncoding.ASCII.GetBytes("SignalRNotificationForGoomena"))
                des.IV = iv
                Return Convert.ToBase64String(des.CreateEncryptor().TransformFinalBlock(buffer, 0, buffer.Length))
            End Using
        End Using
    End Function
    Public Shared Function encrypt(serializedQueryString As String, ByRef Export As String) As Boolean
        Dim re As Boolean = False
        Try
            Dim buffer As Byte() = Encoding.ASCII.GetBytes(serializedQueryString)
            Using des As New TripleDESCryptoServiceProvider
                Using MD5 As New MD5CryptoServiceProvider
                    des.Key = MD5.ComputeHash(ASCIIEncoding.ASCII.GetBytes("SignalRNotificationForGoomena"))
                    des.IV = iv
                    Export = Convert.ToBase64String(des.CreateEncryptor().TransformFinalBlock(buffer, 0, buffer.Length))
                    re = True
                End Using
            End Using
        Catch ex As Exception
        End Try
        Return re
    End Function
End Class

﻿Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Core.DLL
Imports System.Web
Imports System.Configuration
Imports System.Data.SqlClient

Public Class ProfileHelper

    Public Const gMemberPhotosDirectory As String = "~/Photos/{0}/"
    Public Const gMemberPhotosThumbsDirectory As String = "~/Photos/{0}/thumbs/"
    Public Const gMemberPhotosD150 As String = "~/Photos/{0}/d150/"
    Public Const gMemberPhotosD350 As String = "~/Photos/{0}/d350/"

    Public Const gMemberPhotosDirectoryView As String = "http://photos.goomena.com/{0}/"
    Public Const gMemberPhotosThumbsDirectoryView As String = "http://photos.goomena.com/{0}/thumbs/"
    Public Const gMemberPhotosD150View As String = "http://photos.goomena.com/{0}/d150/"
    Public Const gMemberPhotosD350View As String = "http://photos.goomena.com/{0}/d350/"
    Public Shared gMemberPhotosDefault As String = PhotoPath() & "{0}"
    Public Const gMemberDocsDirectory As String = "~/Documents/{0}/"
    Public Const gMemberDocsDirectoryView As String = "~/Documents/{0}/"


    Public Event OnExceptionThrowed(ByVal ex As Exception, ByVal ExtraMessage As String)
    'Public Event OnLoginWithFacebookSuccess(ByRef params As LoginWithFacebookSuccessParams)


    '' Lazy loading
    Private Shared _gMaleGender As DSLists.EUS_LISTS_GenderRow
    Public Shared ReadOnly Property gMaleGender As DSLists.EUS_LISTS_GenderRow
        Get
            If (_gMaleGender Is Nothing) Then
                _gMaleGender = Lists.gDSLists.EUS_LISTS_Gender.Single(Function(itm) itm.US = "Male")
            End If
            Return _gMaleGender
        End Get
    End Property


    Public Shared Sub gMaleGenderReset()
        _gMaleGender = Nothing
    End Sub



    '' Lazy loading
    Private Shared _gFemaleGender As DSLists.EUS_LISTS_GenderRow
    Public Shared ReadOnly Property gFemaleGender As DSLists.EUS_LISTS_GenderRow
        Get
            If (_gFemaleGender Is Nothing) Then
                _gFemaleGender = Lists.gDSLists.EUS_LISTS_Gender.Single(Function(itm) itm.US = "Female")
            End If
            Return _gFemaleGender
        End Get
    End Property


    Public Shared Sub gFemaleGenderReset()
        _gFemaleGender = Nothing
    End Sub


    Private Shared _gGenerousMember As DSLists.EUS_LISTS_AccountTypeRow
    Public Shared ReadOnly Property gGenerousMember As DSLists.EUS_LISTS_AccountTypeRow
        Get
            If (_gGenerousMember Is Nothing) Then
                _gGenerousMember = Lists.gDSLists.EUS_LISTS_AccountType.Single(Function(itm) itm.US.StartsWith("Generous:"))
            End If
            Return _gGenerousMember
        End Get
    End Property


    Public Shared Sub gGenerousMemberReset()
        _gGenerousMember = Nothing
    End Sub


    Private Shared _gAttractiveMember As DSLists.EUS_LISTS_AccountTypeRow
    Public Shared ReadOnly Property gAttractiveMember As DSLists.EUS_LISTS_AccountTypeRow
        Get
            If (_gAttractiveMember Is Nothing) Then
                _gAttractiveMember = Lists.gDSLists.EUS_LISTS_AccountType.Single(Function(itm) itm.US.StartsWith("Attractive:"))
            End If
            Return _gAttractiveMember
        End Get
    End Property


    Public Shared Sub gAttractiveMemberReset()
        _gAttractiveMember = Nothing
    End Sub



    Public Shared Function IsMale(GenderId As Integer) As Boolean
        If (GenderId = ProfileHelper.gMaleGender.GenderId) Then
            Return True
        End If
        Return False
    End Function


    Public Shared Function IsFemale(GenderId As Integer) As Boolean
        If (GenderId = ProfileHelper.gFemaleGender.GenderId) Then
            Return True
        End If
        Return False
    End Function
    Public Shared Function PhotoPath() As String
        Dim re As String = "http://cdn.goomena.com"
        Try
            If ConfigurationManager.AppSettings("CdnEnabled") <> "" AndAlso CInt(ConfigurationManager.AppSettings("CdnEnabled")) <> 1 Then
                re = "http://www.goomena.com/"
            End If
            Return re
        Catch ex As Exception

        End Try
        Return re
    End Function

    'Public Shared Function IsGenerous(AccountTypeId As Integer)
    '    If (AccountTypeId = Generous_AccountTypeId) Then
    '        Return True
    '    End If
    '    Return False
    'End Function


    'Public Shared Function IsAttractive(AccountTypeId As Integer)
    '    If (AccountTypeId = Attractive_AccountTypeId) Then
    '        Return True
    '    End If
    '    Return False
    'End Function


    'Private Shared _Generous_AccountTypeId As Integer
    'Public Shared ReadOnly Property Generous_AccountTypeId As Integer
    '    Get
    '        If (_Generous_AccountTypeId = 0) Then
    '            _Generous_AccountTypeId = (From itm In Lists.gDSLists.EUS_LISTS_AccountType
    '                                      Where Not itm.IsConstantNameNull AndAlso itm.ConstantName = "GENEROUS"
    '                                        Select itm.AccountTypeId).FirstOrDefault()
    '        End If
    '        Return _Generous_AccountTypeId
    '    End Get
    'End Property


    'Private Shared _Attractive_AccountTypeId As Integer
    'Public Shared ReadOnly Property Attractive_AccountTypeId As Integer
    '    Get
    '        If (_Attractive_AccountTypeId = 0) Then
    '            _Attractive_AccountTypeId = (From itm In Lists.gDSLists.EUS_LISTS_AccountType
    '                                        Where Not itm.IsConstantNameNull AndAlso itm.ConstantName = "ATTRACTIVE"
    '                                            Select itm.AccountTypeId).FirstOrDefault()
    '        End If
    '        Return _Attractive_AccountTypeId
    '    End Get
    'End Property


    Public Shared ReadOnly Property Male_DefaultImage As String
        Get
            Return "/Images/guy.jpg?v2"
        End Get
    End Property

    Public Shared ReadOnly Property Female_DefaultImage As String
        Get
            Return "/Images/girl.jpg?v2"
        End Get
    End Property


    Public Shared ReadOnly Property Male_DefaultImagePrivate As String
        Get
            Return "/Images2/men.png?v2"
        End Get
    End Property

    Public Shared ReadOnly Property Female_DefaultImagePrivate As String
        Get
            Return "/Images2/woman.png?v2"
        End Get
    End Property


    Public Shared ReadOnly Property Male_DefaultImagePrivate180 As String
        Get
            Return "/Images2/men180.jpg"
        End Get
    End Property

    Public Shared ReadOnly Property Female_DefaultImagePrivate180 As String
        Get
            Return "/Images2/woman180.jpg"
        End Get
    End Property



    Public Shared Function GetDefaultImageURL(GenderId As Integer) As String
        Dim photoPath As String = ""
        If ProfileHelper.IsMale(GenderId) Then

            photoPath = String.Format(gMemberPhotosDefault, ProfileHelper.Male_DefaultImage)  'System.Web.VirtualPathUtility.ToAbsolute(ProfileHelper.Male_DefaultImage)

        ElseIf ProfileHelper.IsFemale(GenderId) Then

            photoPath = String.Format(gMemberPhotosDefault, ProfileHelper.Female_DefaultImage) 'System.Web.VirtualPathUtility.ToAbsolute(ProfileHelper.Female_DefaultImage)

        End If
        Return photoPath
    End Function


    Public Shared Function GetPrivateImageURL(GenderId As Integer) As String
        Dim photoPath As String = ""
        If ProfileHelper.IsMale(GenderId) Then

            photoPath = String.Format(gMemberPhotosDefault, ProfileHelper.Male_DefaultImagePrivate) 'System.Web.VirtualPathUtility.ToAbsolute(ProfileHelper.Male_DefaultImagePrivate)

        ElseIf ProfileHelper.IsFemale(GenderId) Then

            photoPath = String.Format(gMemberPhotosDefault, ProfileHelper.Female_DefaultImagePrivate) 'System.Web.VirtualPathUtility.ToAbsolute(ProfileHelper.Female_DefaultImagePrivate)

        End If
        Return photoPath
    End Function

    Shared PhotoRefreshParam As String = Date.UtcNow.ToString("fff")
    Public Shared Function GetProfileImageURL(ProfileId As Integer?, FileName As String, GenderId As Integer, isThumb As Boolean, isHTTPS As Boolean) As String
        Dim photoPath As String = GetProfileImageURL(ProfileId, FileName, GenderId, isThumb, isHTTPS, PhotoSize.Thumb)
        Return photoPath
    End Function

    Public Shared Function GetProfileImageURL(ProfileId As Integer?, FileName As String, GenderId As Integer, isThumb As Boolean, isHTTPS As Boolean, photoSize As PhotoSize) As String
        Dim photoPath As String = ""

        If (ProfileId > 0 AndAlso Not String.IsNullOrEmpty(FileName)) Then
            If (isThumb AndAlso photoSize = DLL.PhotoSize.D150) Then
                photoPath = String.Format(gMemberPhotosD150View, ProfileId) & FileName

            ElseIf (isThumb AndAlso photoSize = DLL.PhotoSize.D350) Then
                photoPath = String.Format(gMemberPhotosD350View, ProfileId) & FileName

            ElseIf (isThumb) Then
                photoPath = String.Format(gMemberPhotosThumbsDirectoryView, ProfileId) & FileName

            Else
                photoPath = String.Format(gMemberPhotosDirectoryView, ProfileId) & FileName

            End If
        Else
            If (DataHelpers.EUS_CustomerPhotos_HasPrivatePhotos(ProfileId)) Then
                photoPath = ProfileHelper.GetPrivateImageURL(GenderId)
            Else
                photoPath = ProfileHelper.GetDefaultImageURL(GenderId)
            End If
        End If

        If isHTTPS AndAlso photoPath.StartsWith("http:") Then photoPath = "https" & photoPath.Remove(0, "http".Length)
        photoPath = photoPath & "?" & PhotoRefreshParam

        Return photoPath
    End Function



    Public Shared Function GetCurrentAge(birthdate As DateTime) As Integer
        Dim dateDiff As TimeSpan = (DateTime.Now - birthdate)
        Return Math.Floor((dateDiff.TotalDays / 365.255)).ToString()
    End Function


    Public Shared Function GetProfileNavigateUrl(profileLoginName As String) As String
        Return "/Members/Profile.aspx?p=" & HttpUtility.UrlEncode(profileLoginName)
        'Return System.Web.VirtualPathUtility.ToAbsolute("~/Members/Profile.aspx?p=" & HttpUtility.UrlEncode(profileLoginName))
    End Function



    'Public Shared Function CalculateDistance(ByVal currentProfile As EUS_Profile, ByVal othersProfile As DataRow) As Integer
    '    Return 111
    'End Function


    'Public Shared Function CalculateDistance(ByVal currentProfile As EUS_Profile, ByVal othersProfile As EUS_Profile) As Integer
    '    Return 111
    'End Function


    'Shared Function CalculateDistance(eUS_Profile As EUS_Profile, othersProfile As DataRowView) As Integer
    '    Return 111
    'End Function





    Private Shared _OfferTypeID_WINK As Integer
    Public Shared ReadOnly Property OfferTypeID_WINK As Integer
        Get
            If (_OfferTypeID_WINK = 0) Then
                _OfferTypeID_WINK = Lists.gDSLists.EUS_OffersTypes.Where(Function(itm) itm.ConstantName = OfferTypeEnum.WINK.ToString()).FirstOrDefault().EUS_OffersTypeID()
            End If
            Return _OfferTypeID_WINK
        End Get
    End Property


    Private Shared _OfferTypeID_OFFERNEW As Integer
    Public Shared ReadOnly Property OfferTypeID_OFFERNEW As Integer
        Get
            If (_OfferTypeID_OFFERNEW = 0) Then
                _OfferTypeID_OFFERNEW = Lists.gDSLists.EUS_OffersTypes.Where(Function(itm) itm.ConstantName = OfferTypeEnum.OFFERNEW.ToString()).FirstOrDefault().EUS_OffersTypeID()
            End If
            Return _OfferTypeID_OFFERNEW
        End Get
    End Property


    Private Shared _OfferTypeID_OFFERCOUNTER As Integer
    Public Shared ReadOnly Property OfferTypeID_OFFERCOUNTER As Integer
        Get
            If (_OfferTypeID_OFFERCOUNTER = 0) Then
                _OfferTypeID_OFFERCOUNTER = Lists.gDSLists.EUS_OffersTypes.Where(Function(itm) itm.ConstantName = OfferTypeEnum.OFFERCOUNTER.ToString()).FirstOrDefault().EUS_OffersTypeID()
            End If
            Return _OfferTypeID_OFFERCOUNTER
        End Get
    End Property


    Private Shared _OfferTypeID_POKE As Integer
    Public Shared ReadOnly Property OfferTypeID_POKE As Integer
        Get
            If (_OfferTypeID_POKE = 0) Then
                _OfferTypeID_POKE = Lists.gDSLists.EUS_OffersTypes.Where(Function(itm) itm.ConstantName = OfferTypeEnum.POKE.ToString()).FirstOrDefault().EUS_OffersTypeID()
            End If
            Return _OfferTypeID_POKE
        End Get
    End Property

    Private Shared _OfferTypeID_NEWDATE As Integer
    Public Shared ReadOnly Property OfferTypeID_NEWDATE As Integer
        Get
            If (_OfferTypeID_NEWDATE = 0) Then
                _OfferTypeID_NEWDATE = Lists.gDSLists.EUS_OffersTypes.Where(Function(itm) itm.ConstantName = OfferTypeEnum.NEWDATE.ToString()).FirstOrDefault().EUS_OffersTypeID()
            End If
            Return _OfferTypeID_NEWDATE
        End Get
    End Property



    Private Shared _OfferStatusID_ACCEPTED As Integer
    Public Shared ReadOnly Property OfferStatusID_ACCEPTED As Integer
        Get
            If (_OfferStatusID_ACCEPTED = 0) Then
                _OfferStatusID_ACCEPTED = Lists.gDSLists.EUS_OffersStatus.Where(Function(itm) itm.ConstantName = OfferStatusEnum.ACCEPTED.ToString()).FirstOrDefault().EUS_OffersStatusID
            End If
            Return _OfferStatusID_ACCEPTED
        End Get
    End Property


    Private Shared _OfferStatusID_OFFER_ACCEEPTED_WITH_MESSAGE As Integer
    Public Shared ReadOnly Property OfferStatusID_OFFER_ACCEEPTED_WITH_MESSAGE As Integer
        Get
            If (_OfferStatusID_OFFER_ACCEEPTED_WITH_MESSAGE = 0) Then
                _OfferStatusID_OFFER_ACCEEPTED_WITH_MESSAGE = Lists.gDSLists.EUS_OffersStatus.Where(Function(itm) itm.ConstantName = OfferStatusEnum.OFFER_ACCEEPTED_WITH_MESSAGE.ToString()).FirstOrDefault().EUS_OffersStatusID
            End If
            Return _OfferStatusID_OFFER_ACCEEPTED_WITH_MESSAGE
        End Get
    End Property


    Private Shared _OfferStatusID_LIKE_ACCEEPTED_WITH_OFFER As Integer
    Public Shared ReadOnly Property OfferStatusID_LIKE_ACCEEPTED_WITH_OFFER As Integer
        Get
            If (_OfferStatusID_LIKE_ACCEEPTED_WITH_OFFER = 0) Then
                _OfferStatusID_LIKE_ACCEEPTED_WITH_OFFER = Lists.gDSLists.EUS_OffersStatus.Where(Function(itm) itm.ConstantName = OfferStatusEnum.LIKE_ACCEEPTED_WITH_OFFER.ToString()).FirstOrDefault().EUS_OffersStatusID
            End If
            Return _OfferStatusID_LIKE_ACCEEPTED_WITH_OFFER
        End Get
    End Property


    Private Shared _OfferStatusID_LIKE_ACCEEPTED_WITH_MESSAGE As Integer
    Public Shared ReadOnly Property OfferStatusID_LIKE_ACCEEPTED_WITH_MESSAGE As Integer
        Get
            If (_OfferStatusID_LIKE_ACCEEPTED_WITH_MESSAGE = 0) Then
                _OfferStatusID_LIKE_ACCEEPTED_WITH_MESSAGE = Lists.gDSLists.EUS_OffersStatus.Where(Function(itm) itm.ConstantName = OfferStatusEnum.LIKE_ACCEEPTED_WITH_MESSAGE.ToString()).FirstOrDefault().EUS_OffersStatusID
            End If
            Return _OfferStatusID_LIKE_ACCEEPTED_WITH_MESSAGE
        End Get
    End Property


    Private Shared _OfferStatusID_LIKE_ACCEEPTED_WITH_POKE As Integer
    Public Shared ReadOnly Property OfferStatusID_LIKE_ACCEEPTED_WITH_POKE As Integer
        Get
            If (_OfferStatusID_LIKE_ACCEEPTED_WITH_POKE = 0) Then
                _OfferStatusID_LIKE_ACCEEPTED_WITH_POKE = Lists.gDSLists.EUS_OffersStatus.Where(Function(itm) itm.ConstantName = OfferStatusEnum.LIKE_ACCEEPTED_WITH_POKE.ToString()).FirstOrDefault().EUS_OffersStatusID
            End If
            Return _OfferStatusID_LIKE_ACCEEPTED_WITH_POKE
        End Get
    End Property


    Private Shared _OfferStatusID_POKE_ACCEEPTED_WITH_OFFER As Integer
    Public Shared ReadOnly Property OfferStatusID_POKE_ACCEEPTED_WITH_OFFER As Integer
        Get
            If (_OfferStatusID_POKE_ACCEEPTED_WITH_OFFER = 0) Then
                _OfferStatusID_POKE_ACCEEPTED_WITH_OFFER = Lists.gDSLists.EUS_OffersStatus.Where(Function(itm) itm.ConstantName = OfferStatusEnum.POKE_ACCEEPTED_WITH_OFFER.ToString()).FirstOrDefault().EUS_OffersStatusID
            End If
            Return _OfferStatusID_POKE_ACCEEPTED_WITH_OFFER
        End Get
    End Property


    Private Shared _OfferStatusID_POKE_ACCEEPTED_WITH_MESSAGE As Integer
    Public Shared ReadOnly Property OfferStatusID_POKE_ACCEEPTED_WITH_MESSAGE As Integer
        Get
            If (_OfferStatusID_POKE_ACCEEPTED_WITH_MESSAGE = 0) Then
                _OfferStatusID_POKE_ACCEEPTED_WITH_MESSAGE = Lists.gDSLists.EUS_OffersStatus.Where(Function(itm) itm.ConstantName = OfferStatusEnum.POKE_ACCEEPTED_WITH_MESSAGE.ToString()).FirstOrDefault().EUS_OffersStatusID
            End If
            Return _OfferStatusID_POKE_ACCEEPTED_WITH_MESSAGE
        End Get
    End Property


    Private Shared _OffersStatusID_COUNTER As Integer
    Public Shared ReadOnly Property OfferStatusID_COUNTER As Integer
        Get
            If (_OffersStatusID_COUNTER = 0) Then
                _OffersStatusID_COUNTER = Lists.gDSLists.EUS_OffersStatus.Where(Function(itm) itm.ConstantName = OfferStatusEnum.COUNTER.ToString()).FirstOrDefault().EUS_OffersStatusID
            End If
            Return _OffersStatusID_COUNTER
        End Get
    End Property

    Private Shared _OfferStatusID_PENDING As Integer
    Public Shared ReadOnly Property OfferStatusID_PENDING As Integer
        Get
            If (_OfferStatusID_PENDING = 0) Then
                _OfferStatusID_PENDING = Lists.gDSLists.EUS_OffersStatus.Where(Function(itm) itm.ConstantName = OfferStatusEnum.PENDING.ToString()).FirstOrDefault().EUS_OffersStatusID
            End If
            Return _OfferStatusID_PENDING
        End Get
    End Property


    Private Shared _OfferStatusID_REJECTBAD As Integer
    Public Shared ReadOnly Property OfferStatusID_REJECTBAD As Integer
        Get
            If (_OfferStatusID_REJECTBAD = 0) Then
                _OfferStatusID_REJECTBAD = Lists.gDSLists.EUS_OffersStatus.Where(Function(itm) itm.ConstantName = OfferStatusEnum.REJECTBAD.ToString()).FirstOrDefault().EUS_OffersStatusID
            End If
            Return _OfferStatusID_REJECTBAD
        End Get
    End Property


    Private Shared _OfferStatusID_REJECTEXPECTATIONS As Integer
    Public Shared ReadOnly Property OfferStatusID_REJECTEXPECTATIONS As Integer
        Get
            If (_OfferStatusID_REJECTEXPECTATIONS = 0) Then
                _OfferStatusID_REJECTEXPECTATIONS = Lists.gDSLists.EUS_OffersStatus.Where(Function(itm) itm.ConstantName = OfferStatusEnum.REJECTEXPECTATIONS.ToString()).FirstOrDefault().EUS_OffersStatusID
            End If
            Return _OfferStatusID_REJECTEXPECTATIONS
        End Get
    End Property


    Private Shared _OfferStatusID_REJECTED As Integer
    Public Shared ReadOnly Property OfferStatusID_REJECTED As Integer
        Get
            If (_OfferStatusID_REJECTED = 0) Then
                _OfferStatusID_REJECTED = Lists.gDSLists.EUS_OffersStatus.Where(Function(itm) itm.ConstantName = OfferStatusEnum.REJECTED.ToString()).FirstOrDefault().EUS_OffersStatusID
            End If
            Return _OfferStatusID_REJECTED
        End Get
    End Property

    Private Shared _OfferStatusID_REJECTFAR As Integer
    Public Shared ReadOnly Property OfferStatusID_REJECTFAR As Integer
        Get
            If (_OfferStatusID_REJECTFAR = 0) Then
                _OfferStatusID_REJECTFAR = Lists.gDSLists.EUS_OffersStatus.Where(Function(itm) itm.ConstantName = OfferStatusEnum.REJECTFAR.ToString()).FirstOrDefault().EUS_OffersStatusID
            End If
            Return _OfferStatusID_REJECTFAR
        End Get
    End Property


    Private Shared _OfferStatusID_REJECTTYPE As Integer
    Public Shared ReadOnly Property OfferStatusID_REJECTTYPE As Integer
        Get
            If (_OfferStatusID_REJECTTYPE = 0) Then
                _OfferStatusID_REJECTTYPE = Lists.gDSLists.EUS_OffersStatus.Where(Function(itm) itm.ConstantName = OfferStatusEnum.REJECTTYPE.ToString()).FirstOrDefault().EUS_OffersStatusID
            End If
            Return _OfferStatusID_REJECTTYPE
        End Get
    End Property

    Private Shared _OfferStatusID_REPLACINGBYCOUNTER As Integer
    Public Shared ReadOnly Property OfferStatusID_REPLACINGBYCOUNTER As Integer
        Get
            If (_OfferStatusID_REPLACINGBYCOUNTER = 0) Then
                _OfferStatusID_REPLACINGBYCOUNTER = Lists.gDSLists.EUS_OffersStatus.Where(Function(itm) itm.ConstantName = OfferStatusEnum.REPLACINGBYCOUNTER.ToString()).FirstOrDefault().EUS_OffersStatusID
            End If
            Return _OfferStatusID_REPLACINGBYCOUNTER
        End Get
    End Property


    'Private Shared _OfferStatusID_UNLOCKED As Integer
    'Public Shared ReadOnly Property OfferStatusID_UNLOCKED As Integer
    '    Get
    '        If (_OfferStatusID_UNLOCKED = 0) Then
    '            _OfferStatusID_UNLOCKED = Lists.gDSLists.EUS_OffersStatus.Where(Function(itm) itm.ConstantName = OfferStatusEnum.UNLOCKED.ToString()).FirstOrDefault().EUS_OffersStatusID
    '        End If
    '        Return _OfferStatusID_UNLOCKED
    '    End Get
    'End Property


    Private Shared _OfferStatusID_CANCELED As Integer
    Public Shared ReadOnly Property OfferStatusID_CANCELED As Integer
        Get
            If (_OfferStatusID_CANCELED = 0) Then
                _OfferStatusID_CANCELED = Lists.gDSLists.EUS_OffersStatus.Where(Function(itm) itm.ConstantName = OfferStatusEnum.CANCELED.ToString()).FirstOrDefault().EUS_OffersStatusID
            End If

            Return _OfferStatusID_CANCELED
        End Get
    End Property


    Private Shared _OfferStatusID_UNLOCKED As Integer
    Public Shared ReadOnly Property OfferStatusID_UNLOCKED As Integer
        Get
            If (_OfferStatusID_UNLOCKED = 0) Then
                _OfferStatusID_UNLOCKED = Lists.gDSLists.EUS_OffersStatus.Where(Function(itm) itm.ConstantName = OfferStatusEnum.UNLOCKED.ToString()).FirstOrDefault().EUS_OffersStatusID
            End If
            Return _OfferStatusID_UNLOCKED
        End Get
    End Property



    Public Shared Function GetBreastSizeString(BreastSizeId As Object, LagID As String) As String
        Dim txt As String = ""
        Dim var = BreastSizeId

        Try
            If (TypeOf var Is Integer AndAlso var > 1) Then
                Dim row As DSLists.EUS_LISTS_BreastSizeRow =
                             Lists.gDSLists.EUS_LISTS_BreastSize.Single(Function(itm As DSLists.EUS_LISTS_BreastSizeRow) itm.BreastSizeID = var)

                If (Lists.gDSLists.EUS_LISTS_BreastSize.Columns.Contains(LagID)) Then
                    txt = row(LagID).ToString()
                End If
                If (String.IsNullOrEmpty(txt)) Then txt = row.US

                'Select Case LagID
                '    Case "US"
                '        If (Not row.IsUSNull()) Then txt = row.US
                '    Case "GR"
                '        If (Not row.IsGRNull()) Then txt = row.GR
                '    Case "TR"
                '        If (Not row.IsTRNull()) Then txt = row.TR
                '    Case "ES"
                '        If (Not row.IsESNull()) Then txt = row.ES
                '    Case Else 'All other languages
                '        txt = row.US
                'End Select
                'If (String.IsNullOrEmpty(txt)) Then
                '    txt = row.US
                'End If

                'Select Case LagID
                '    Case "GR"
                '        txt = Lists.gDSLists.EUS_LISTS_Height.Single(Function(itm As DSLists.EUS_LISTS_HeightRow) itm.HeightId = var).GR
                '    Case "HU"
                '        txt = Lists.gDSLists.EUS_LISTS_Height.Single(Function(itm As DSLists.EUS_LISTS_HeightRow) itm.HeightId = var).HU
                '    Case "TR"
                '        txt = Lists.gDSLists.EUS_LISTS_Height.Single(Function(itm As DSLists.EUS_LISTS_HeightRow) itm.HeightId = var).TR
                '    Case "AL"
                '        txt = Lists.gDSLists.EUS_LISTS_Height.Single(Function(itm As DSLists.EUS_LISTS_HeightRow) itm.HeightId = var).AL
                '    Case Else '"US"
                '        txt = Lists.gDSLists.EUS_LISTS_Height.Single(Function(itm As DSLists.EUS_LISTS_HeightRow) itm.HeightId = var).US
                'End Select
            ElseIf var <= 1 Then
                txt = ""
            End If
        Catch ex As Exception

        End Try

        Return txt
    End Function


    Public Shared Function GetHeightString(heightId As Object, LagID As String) As String
        Dim txt As String = ""
        Dim var = heightId

        Try
            If (TypeOf var Is Integer AndAlso var > 28) Then
                Dim row As DSLists.EUS_LISTS_HeightRow =
                             Lists.gDSLists.EUS_LISTS_Height.Single(Function(itm As DSLists.EUS_LISTS_HeightRow) itm.HeightId = var)

                If (Lists.gDSLists.EUS_LISTS_Height.Columns.Contains(LagID)) Then
                    txt = row(LagID).ToString()
                End If
                If (String.IsNullOrEmpty(txt)) Then txt = row.US

                'Select Case LagID
                '    Case "US"
                '        If (Not row.IsUSNull()) Then txt = row.US
                '    Case "GR"
                '        If (Not row.IsGRNull()) Then txt = row.GR
                '    Case "TR"
                '        If (Not row.IsTRNull()) Then txt = row.TR
                '    Case "ES"
                '        If (Not row.IsESNull()) Then txt = row.ES
                '    Case Else 'All other languages
                '        txt = row.US
                'End Select
                'If (String.IsNullOrEmpty(txt)) Then
                '    txt = row.US
                'End If

                'Select Case LagID
                '    Case "GR"
                '        txt = Lists.gDSLists.EUS_LISTS_Height.Single(Function(itm As DSLists.EUS_LISTS_HeightRow) itm.HeightId = var).GR
                '    Case "HU"
                '        txt = Lists.gDSLists.EUS_LISTS_Height.Single(Function(itm As DSLists.EUS_LISTS_HeightRow) itm.HeightId = var).HU
                '    Case "TR"
                '        txt = Lists.gDSLists.EUS_LISTS_Height.Single(Function(itm As DSLists.EUS_LISTS_HeightRow) itm.HeightId = var).TR
                '    Case "AL"
                '        txt = Lists.gDSLists.EUS_LISTS_Height.Single(Function(itm As DSLists.EUS_LISTS_HeightRow) itm.HeightId = var).AL
                '    Case Else '"US"
                '        txt = Lists.gDSLists.EUS_LISTS_Height.Single(Function(itm As DSLists.EUS_LISTS_HeightRow) itm.HeightId = var).US
                'End Select
            ElseIf var <= 28 Then
                txt = ""
            End If
        Catch ex As Exception

        End Try

        Return txt
    End Function


    Public Shared Function GetBodyTypeString(BodyTypeId As Object, LagID As String) As String
        Dim txt As String = ""
        Dim var As Object = BodyTypeId

        Try
            If (TypeOf var Is Integer AndAlso var > 6) Then
                Dim row As DSLists.EUS_LISTS_BodyTypeRow = Lists.gDSLists.EUS_LISTS_BodyType.Single(Function(itm As DSLists.EUS_LISTS_BodyTypeRow) itm.BodyTypeId = var)

                If (Lists.gDSLists.EUS_LISTS_BodyType.Columns.Contains(LagID)) Then
                    txt = row(LagID).ToString()
                End If
                If (String.IsNullOrEmpty(txt)) Then txt = row.US

                'Select Case LagID
                '    Case "US"
                '        If (Not row.IsUSNull()) Then txt = row.US
                '    Case "GR"
                '        If (Not row.IsGRNull()) Then txt = row.GR
                '    Case "TR"
                '        If (Not row.IsTRNull()) Then txt = row.TR
                '    Case "ES"
                '        If (Not row.IsESNull()) Then txt = row.ES
                '    Case Else 'All other languages
                '        txt = row.US
                'End Select
                'If (String.IsNullOrEmpty(txt)) Then
                '    txt = row.US
                'End If

                'Select Case LagID
                '    Case "GR"
                '        txt = Lists.gDSLists.EUS_LISTS_BodyType.Single(Function(itm As DSLists.EUS_LISTS_BodyTypeRow) itm.BodyTypeId = var).GR
                '    Case "HU"
                '        txt = Lists.gDSLists.EUS_LISTS_BodyType.Single(Function(itm As DSLists.EUS_LISTS_BodyTypeRow) itm.BodyTypeId = var).HU
                '    Case "TR"
                '        txt = Lists.gDSLists.EUS_LISTS_BodyType.Single(Function(itm As DSLists.EUS_LISTS_BodyTypeRow) itm.BodyTypeId = var).TR
                '    Case "AL"
                '        txt = Lists.gDSLists.EUS_LISTS_BodyType.Single(Function(itm As DSLists.EUS_LISTS_BodyTypeRow) itm.BodyTypeId = var).AL
                '    Case Else '"US"
                '        txt = Lists.gDSLists.EUS_LISTS_BodyType.Single(Function(itm As DSLists.EUS_LISTS_BodyTypeRow) itm.BodyTypeId = var).US
                'End Select
            ElseIf var <= 6 Then
                txt = ""
            End If
        Catch ex As Exception

        End Try

        Return txt
    End Function


    Public Shared Function GetHairColorString(HairColorId As Object, LagID As String) As String
        Dim txt As String = ""
        Dim var As Object = HairColorId

        Try
            If (TypeOf var Is Integer AndAlso var > 10) Then
                Dim row As DSLists.EUS_LISTS_HairColorRow = Lists.gDSLists.EUS_LISTS_HairColor.Single(Function(itm As DSLists.EUS_LISTS_HairColorRow) itm.HairColorId = var)

                If (Lists.gDSLists.EUS_LISTS_HairColor.Columns.Contains(LagID)) Then
                    txt = row(LagID).ToString()
                End If
                If (String.IsNullOrEmpty(txt)) Then txt = row.US

                'Select Case LagID
                '    Case "US"
                '        If (Not row.IsUSNull()) Then txt = row.US
                '    Case "GR"
                '        If (Not row.IsGRNull()) Then txt = row.GR
                '    Case "TR"
                '        If (Not row.IsTRNull()) Then txt = row.TR
                '    Case "ES"
                '        If (Not row.IsESNull()) Then txt = row.ES
                '    Case Else 'All other languages
                '        txt = row.US
                'End Select
                'If (String.IsNullOrEmpty(txt)) Then
                '    txt = row.US
                'End If


                'Select Case LagID
                '    Case "GR"
                '        txt = Lists.gDSLists.EUS_LISTS_HairColor.Single(Function(itm As DSLists.EUS_LISTS_HairColorRow) itm.HairColorId = var).GR
                '    Case "HU"
                '        txt = Lists.gDSLists.EUS_LISTS_HairColor.Single(Function(itm As DSLists.EUS_LISTS_HairColorRow) itm.HairColorId = var).HU
                '    Case "TR"
                '        txt = Lists.gDSLists.EUS_LISTS_HairColor.Single(Function(itm As DSLists.EUS_LISTS_HairColorRow) itm.HairColorId = var).TR
                '    Case "AL"
                '        txt = Lists.gDSLists.EUS_LISTS_HairColor.Single(Function(itm As DSLists.EUS_LISTS_HairColorRow) itm.HairColorId = var).AL
                '    Case Else '"US"
                '        txt = Lists.gDSLists.EUS_LISTS_HairColor.Single(Function(itm As DSLists.EUS_LISTS_HairColorRow) itm.HairColorId = var).US
                'End Select
            ElseIf var <= 10 Then
                txt = ""
            End If
        Catch ex As Exception

        End Try

        Return txt
    End Function


    Public Shared Function GetEyeColorString(EyeColorId As Object, LagID As String) As String
        Dim txt As String = ""
        Dim var = EyeColorId

        Try
            If (TypeOf var Is Integer AndAlso var > 8) Then
                Dim row As DSLists.EUS_LISTS_EyeColorRow = Lists.gDSLists.EUS_LISTS_EyeColor.Single(Function(itm As DSLists.EUS_LISTS_EyeColorRow) itm.EyeColorId = var)

                If (Lists.gDSLists.EUS_LISTS_EyeColor.Columns.Contains(LagID)) Then
                    txt = row(LagID).ToString()
                End If
                If (String.IsNullOrEmpty(txt)) Then txt = row.US


                'Select Case LagID
                '    Case "US"
                '        If (Not row.IsUSNull()) Then txt = row.US
                '    Case "GR"
                '        If (Not row.IsGRNull()) Then txt = row.GR
                '    Case "TR"
                '        If (Not row.IsTRNull()) Then txt = row.TR
                '    Case "ES"
                '        If (Not row.IsESNull()) Then txt = row.ES
                '    Case Else 'All other languages
                '        txt = row.US
                'End Select
                'If (String.IsNullOrEmpty(txt)) Then
                '    txt = row.US
                'End If

                'Select Case LagID
                '    Case "GR"
                '        txt = Lists.gDSLists.EUS_LISTS_EyeColor.Single(Function(itm As DSLists.EUS_LISTS_EyeColorRow) itm.EyeColorId = var).GR
                '    Case "HU"
                '        txt = Lists.gDSLists.EUS_LISTS_EyeColor.Single(Function(itm As DSLists.EUS_LISTS_EyeColorRow) itm.EyeColorId = var).HU
                '    Case "TR"
                '        txt = Lists.gDSLists.EUS_LISTS_EyeColor.Single(Function(itm As DSLists.EUS_LISTS_EyeColorRow) itm.EyeColorId = var).TR
                '    Case "AL"
                '        txt = Lists.gDSLists.EUS_LISTS_EyeColor.Single(Function(itm As DSLists.EUS_LISTS_EyeColorRow) itm.EyeColorId = var).AL
                '    Case Else '"US"
                '        txt = Lists.gDSLists.EUS_LISTS_EyeColor.Single(Function(itm As DSLists.EUS_LISTS_EyeColorRow) itm.EyeColorId = var).US
                'End Select
            ElseIf var <= 8 Then
                txt = ""
            End If
        Catch ex As Exception

        End Try

        Return txt
    End Function
    Public Shared Function GetProfileHasApprovedNewTos(ByVal ProfileId As Integer) As Boolean
        Dim re As Boolean = False
        Try
            Dim dt As DateTime = Now.AddYears(-10)
            Try
                dt = Date.ParseExact(clsConfigValues.GetSYS_ConfigValue("NewTermsOfUseDate"), "dd-MM-yyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)
            Catch ex As Exception
                Try
                    dt = Date.Parse(clsConfigValues.GetSYS_ConfigValue("NewTermsOfUseDate"))
                Catch ex1 As Exception
                    Throw New Exception("Couldn't get date from ConfigValue: NewTermsOfUseDate")
                End Try
            End Try
            ' Dim dt As DateTime = Date.ParseExact((clsConfigValues.GetSYS_ConfigValue("NewTermsOfUseDate"),
            Dim sql2 = <sql><![CDATA[
EXEC  [dbo].[GetProfileHasAprovedNewTOS] 
   @ProfileID
  ,@Date
  ,@Responce
]]></sql>
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd2 As SqlCommand = DataHelpers.GetSqlCommand(con, sql2)
                    cmd2.Parameters.AddWithValue("@ProfileID", ProfileId)
                    cmd2.Parameters.Add("@Date", SqlDbType.DateTime)
                    cmd2.Parameters.Item("@Date").Value = dt
                    cmd2.Parameters.AddWithValue("@Responce", re)
                    re = DataHelpers.ExecuteScalar(cmd2)
                End Using
            End Using

        Catch ex As Exception
            clsMyMail.TrySendMailSupport(ConfigurationManager.AppSettings("ExceptionsEmail"), "On ApproveNewUserTOS for user PrID:" & ProfileId, ex.ToString())
        End Try
        Return re
    End Function
    Public Shared Function ApproveUserNewTOS(ByVal ProfileId As Integer, ByVal MirrorProfileId As Integer, Approved As Boolean, ip As String, SessionId As String) As Boolean
        Dim re As Boolean = False
        Try


            Dim sql2 = <sql><![CDATA[
EXEC  [dbo].[ApproveUserNewTOS] 
   @ProfileId
  ,@MirrorProfileId
  ,@Ip
  ,@SessionId
  ,@Aprroved
  ,@Responce OUTPUT
]]></sql>
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd2 As SqlCommand = DataHelpers.GetSqlCommand(con, sql2)
                    cmd2.Parameters.AddWithValue("@ProfileID", ProfileId)
                    cmd2.Parameters.AddWithValue("@MirrorProfileId", MirrorProfileId)
                    cmd2.Parameters.AddWithValue("@Ip", ip)
                    cmd2.Parameters.AddWithValue("@Aprroved", Approved)
                    cmd2.Parameters.AddWithValue("@SessionId", SessionId)
                    cmd2.Parameters.AddWithValue("@Responce", re)
                    DataHelpers.ExecuteNonQuery(cmd2)
                End Using
            End Using
        Catch ex As Exception
            clsMyMail.TrySendMailSupport(ConfigurationManager.AppSettings("ExceptionsEmail"), "On ApproveNewUserTOS for user PrID:" & ProfileId, ex.ToString())
        End Try
        Return re
    End Function


    Public Shared Function GetEthnicityString(EthnicityId As Object, LagID As String) As String
        Dim txt As String = ""
        Dim var = EthnicityId

        Try
            If (TypeOf var Is Integer AndAlso var > 1) Then
                Dim row As DSLists.EUS_LISTS_EthnicityRow = Lists.gDSLists.EUS_LISTS_Ethnicity.Single(Function(itm As DSLists.EUS_LISTS_EthnicityRow) itm.EthnicityId = var)

                If (Lists.gDSLists.EUS_LISTS_Ethnicity.Columns.Contains(LagID)) Then
                    txt = row(LagID).ToString()
                End If
                If (String.IsNullOrEmpty(txt)) Then txt = row.US


                'Select Case LagID
                '    Case "US"
                '        If (Not row.IsUSNull()) Then txt = row.US
                '    Case "GR"
                '        If (Not row.IsGRNull()) Then txt = row.GR
                '    Case "TR"
                '        If (Not row.IsTRNull()) Then txt = row.TR
                '    Case "ES"
                '        If (Not row.IsESNull()) Then txt = row.ES
                '    Case Else 'All other languages
                '        txt = row.US
                'End Select
                'If (String.IsNullOrEmpty(txt)) Then
                '    txt = row.US
                'End If
                'Select Case LagID
                '    Case "GR"
                '        txt = Lists.gDSLists.EUS_LISTS_Ethnicity.Single(Function(itm As DSLists.EUS_LISTS_EthnicityRow) itm.EthnicityId = var).GR
                '    Case "HU"
                '        txt = Lists.gDSLists.EUS_LISTS_Ethnicity.Single(Function(itm As DSLists.EUS_LISTS_EthnicityRow) itm.EthnicityId = var).HU
                '    Case "TR"
                '        txt = Lists.gDSLists.EUS_LISTS_Ethnicity.Single(Function(itm As DSLists.EUS_LISTS_EthnicityRow) itm.EthnicityId = var).TR
                '    Case "AL"
                '        txt = Lists.gDSLists.EUS_LISTS_Ethnicity.Single(Function(itm As DSLists.EUS_LISTS_EthnicityRow) itm.EthnicityId = var).AL
                '    Case Else '"US"
                '        txt = Lists.gDSLists.EUS_LISTS_Ethnicity.Single(Function(itm As DSLists.EUS_LISTS_EthnicityRow) itm.EthnicityId = var).US
                'End Select
            ElseIf var <= 1 Then
                txt = ""
            End If
        Catch ex As Exception

        End Try

        Return txt

    End Function


    Public Shared Function GetEducationString(EducationId As Object, LagID As String) As String
        Dim txt As String = ""
        Dim var = EducationId

        Try

            If (TypeOf var Is Integer AndAlso var > 1) Then
                Dim row As DSLists.EUS_LISTS_EducationRow = Lists.gDSLists.EUS_LISTS_Education.Single(Function(itm As DSLists.EUS_LISTS_EducationRow) itm.EducationId = var)

                If (Lists.gDSLists.EUS_LISTS_Education.Columns.Contains(LagID)) Then
                    txt = row(LagID).ToString()
                End If
                If (String.IsNullOrEmpty(txt)) Then txt = row.US

                'Select Case LagID
                '    Case "US"
                '        If (Not row.IsUSNull()) Then txt = row.US
                '    Case "GR"
                '        If (Not row.IsGRNull()) Then txt = row.GR
                '    Case "TR"
                '        If (Not row.IsTRNull()) Then txt = row.TR
                '    Case "ES"
                '        If (Not row.IsESNull()) Then txt = row.ES
                '    Case Else 'All other languages
                '        txt = row.US
                'End Select
                'If (String.IsNullOrEmpty(txt)) Then
                '    txt = row.US
                'End If
                'Select Case LagID
                '    Case "GR"
                '        txt = Lists.gDSLists.EUS_LISTS_Education.Single(Function(itm As DSLists.EUS_LISTS_EducationRow) itm.EducationId = var).GR
                '    Case "HU"
                '        txt = Lists.gDSLists.EUS_LISTS_Education.Single(Function(itm As DSLists.EUS_LISTS_EducationRow) itm.EducationId = var).HU
                '    Case "TR"
                '        txt = Lists.gDSLists.EUS_LISTS_Education.Single(Function(itm As DSLists.EUS_LISTS_EducationRow) itm.EducationId = var).TR
                '    Case "AL"
                '        txt = Lists.gDSLists.EUS_LISTS_Education.Single(Function(itm As DSLists.EUS_LISTS_EducationRow) itm.EducationId = var).AL
                '    Case Else '"US"
                '        txt = Lists.gDSLists.EUS_LISTS_Education.Single(Function(itm As DSLists.EUS_LISTS_EducationRow) itm.EducationId = var).US
                'End Select
            ElseIf var <= 1 Then
                txt = ""
            End If
        Catch ex As Exception

        End Try

        Return txt
    End Function


    Public Shared Function GetChildrenNumberString(ChildrenNumberId As Object, LagID As String) As String
        Dim txt As String = ""
        Dim var = ChildrenNumberId

        Try
            If (TypeOf var Is Integer AndAlso var > 1) Then
                Dim row As DSLists.EUS_LISTS_ChildrenNumberRow = Lists.gDSLists.EUS_LISTS_ChildrenNumber.Single(Function(itm As DSLists.EUS_LISTS_ChildrenNumberRow) itm.ChildrenNumberId = var)

                If (Lists.gDSLists.EUS_LISTS_ChildrenNumber.Columns.Contains(LagID)) Then
                    txt = row(LagID).ToString()
                End If
                If (String.IsNullOrEmpty(txt)) Then txt = row.US


                'Select Case LagID
                '    Case "US"
                '        If (Not row.IsUSNull()) Then txt = row.US
                '    Case "GR"
                '        If (Not row.IsGRNull()) Then txt = row.GR
                '    Case "TR"
                '        If (Not row.IsTRNull()) Then txt = row.TR
                '    Case "ES"
                '        If (Not row.IsESNull()) Then txt = row.ES
                '    Case Else 'All other languages
                '        txt = row.US
                'End Select
                'If (String.IsNullOrEmpty(txt)) Then
                '    txt = row.US
                'End If

                'Select Case LagID
                '    Case "GR"
                '        txt = Lists.gDSLists.EUS_LISTS_ChildrenNumber.Single(Function(itm As DSLists.EUS_LISTS_ChildrenNumberRow) itm.ChildrenNumberId = var).GR
                '    Case "HU"
                '        txt = Lists.gDSLists.EUS_LISTS_ChildrenNumber.Single(Function(itm As DSLists.EUS_LISTS_ChildrenNumberRow) itm.ChildrenNumberId = var).HU
                '    Case "TR"
                '        txt = Lists.gDSLists.EUS_LISTS_ChildrenNumber.Single(Function(itm As DSLists.EUS_LISTS_ChildrenNumberRow) itm.ChildrenNumberId = var).TR
                '    Case "AL"
                '        txt = Lists.gDSLists.EUS_LISTS_ChildrenNumber.Single(Function(itm As DSLists.EUS_LISTS_ChildrenNumberRow) itm.ChildrenNumberId = var).AL
                '    Case Else '"US"
                '        txt = Lists.gDSLists.EUS_LISTS_ChildrenNumber.Single(Function(itm As DSLists.EUS_LISTS_ChildrenNumberRow) itm.ChildrenNumberId = var).US
                'End Select
            ElseIf var <= 1 Then
                txt = ""
            End If
        Catch ex As Exception

        End Try

        Return txt
    End Function


    Public Shared Function GetReligionString(ReligionId As Object, LagID As String) As String
        Dim txt As String = ""
        Dim var = ReligionId

        Try
            If (TypeOf var Is Integer AndAlso var > 1) Then
                Dim row As DSLists.EUS_LISTS_ReligionRow = Lists.gDSLists.EUS_LISTS_Religion.Single(Function(itm As DSLists.EUS_LISTS_ReligionRow) itm.ReligionId = var)

                If (Lists.gDSLists.EUS_LISTS_Religion.Columns.Contains(LagID)) Then
                    txt = row(LagID).ToString()
                End If
                If (String.IsNullOrEmpty(txt)) Then txt = row.US



                'Select Case LagID
                '    Case "US"
                '        If (Not row.IsUSNull()) Then txt = row.US
                '    Case "GR"
                '        If (Not row.IsGRNull()) Then txt = row.GR
                '    Case "TR"
                '        If (Not row.IsTRNull()) Then txt = row.TR
                '    Case "ES"
                '        If (Not row.IsESNull()) Then txt = row.ES
                '    Case Else 'All other languages
                '        txt = row.US
                'End Select
                'If (String.IsNullOrEmpty(txt)) Then
                '    txt = row.US
                'End If

                'Select Case LagID
                '    Case "GR"
                '        txt = Lists.gDSLists.EUS_LISTS_Religion.Single(Function(itm As DSLists.EUS_LISTS_ReligionRow) itm.ReligionId = var).GR
                '    Case "HU"
                '        txt = Lists.gDSLists.EUS_LISTS_Religion.Single(Function(itm As DSLists.EUS_LISTS_ReligionRow) itm.ReligionId = var).HU
                '    Case "TR"
                '        txt = Lists.gDSLists.EUS_LISTS_Religion.Single(Function(itm As DSLists.EUS_LISTS_ReligionRow) itm.ReligionId = var).TR
                '    Case "AL"
                '        txt = Lists.gDSLists.EUS_LISTS_Religion.Single(Function(itm As DSLists.EUS_LISTS_ReligionRow) itm.ReligionId = var).AL
                '    Case Else '"US"
                '        txt = Lists.gDSLists.EUS_LISTS_Religion.Single(Function(itm As DSLists.EUS_LISTS_ReligionRow) itm.ReligionId = var).US
                'End Select
            ElseIf var <= 1 Then
                txt = ""
            End If
        Catch ex As Exception

        End Try

        Return txt
    End Function


    Public Shared Function GetSmokingString(SmokingId As Object, LagID As String) As String
        Dim txt As String = ""
        Dim var = SmokingId

        Try

            If (TypeOf var Is Integer AndAlso var > 1) Then
                Dim row As DSLists.EUS_LISTS_SmokingRow =
Lists.gDSLists.EUS_LISTS_Smoking.Single(Function(itm As DSLists.EUS_LISTS_SmokingRow) itm.SmokingId = var)

                If (Lists.gDSLists.EUS_LISTS_Smoking.Columns.Contains(LagID)) Then
                    txt = row(LagID).ToString()
                End If
                If (String.IsNullOrEmpty(txt)) Then txt = row.US

                'Select Case LagID
                '    Case "US"
                '        If (Not row.IsUSNull()) Then txt = row.US
                '    Case "GR"
                '        If (Not row.IsGRNull()) Then txt = row.GR
                '    Case "TR"
                '        If (Not row.IsTRNull()) Then txt = row.TR
                '    Case "ES"
                '        If (Not row.IsESNull()) Then txt = row.ES
                '    Case Else 'All other languages
                '        txt = row.US
                'End Select
                'If (String.IsNullOrEmpty(txt)) Then
                '    txt = row.US
                'End If

                'Select Case LagID
                '    Case "GR"
                '        txt = Lists.gDSLists.EUS_LISTS_Smoking.Single(Function(itm As DSLists.EUS_LISTS_SmokingRow) itm.SmokingId = var).GR
                '    Case "HU"
                '        txt = Lists.gDSLists.EUS_LISTS_Smoking.Single(Function(itm As DSLists.EUS_LISTS_SmokingRow) itm.SmokingId = var).HU
                '    Case "TR"
                '        txt = Lists.gDSLists.EUS_LISTS_Smoking.Single(Function(itm As DSLists.EUS_LISTS_SmokingRow) itm.SmokingId = var).TR
                '    Case "AL"
                '        txt = Lists.gDSLists.EUS_LISTS_Smoking.Single(Function(itm As DSLists.EUS_LISTS_SmokingRow) itm.SmokingId = var).AL
                '    Case Else '"US"
                '        txt = Lists.gDSLists.EUS_LISTS_Smoking.Single(Function(itm As DSLists.EUS_LISTS_SmokingRow) itm.SmokingId = var).US
                'End Select
            ElseIf var <= 1 Then
                txt = ""
            End If
        Catch ex As Exception

        End Try

        Return txt
    End Function


    Public Shared Function GetDrinkingString(DrinkingId As Object, LagID As String) As String
        Dim txt As String = ""
        Dim var = DrinkingId

        Try
            If (TypeOf var Is Integer AndAlso var > 1) Then
                Dim row As DSLists.EUS_LISTS_DrinkingRow =
Lists.gDSLists.EUS_LISTS_Drinking.Single(Function(itm As DSLists.EUS_LISTS_DrinkingRow) itm.DrinkingId = var)

                If (Lists.gDSLists.EUS_LISTS_Drinking.Columns.Contains(LagID)) Then
                    txt = row(LagID).ToString()
                End If
                If (String.IsNullOrEmpty(txt)) Then txt = row.US


                'Select Case LagID
                '    Case "US"
                '        If (Not row.IsUSNull()) Then txt = row.US
                '    Case "GR"
                '        If (Not row.IsGRNull()) Then txt = row.GR
                '    Case "TR"
                '        If (Not row.IsTRNull()) Then txt = row.TR
                '    Case "ES"
                '        If (Not row.IsESNull()) Then txt = row.ES
                '    Case Else 'All other languages
                '        txt = row.US
                'End Select
                'If (String.IsNullOrEmpty(txt)) Then
                '    txt = row.US
                'End If

                'Select Case LagID
                '    Case "GR"
                '        txt = Lists.gDSLists.EUS_LISTS_Drinking.Single(Function(itm As DSLists.EUS_LISTS_DrinkingRow) itm.DrinkingId = var).GR
                '    Case "HU"
                '        txt = Lists.gDSLists.EUS_LISTS_Drinking.Single(Function(itm As DSLists.EUS_LISTS_DrinkingRow) itm.DrinkingId = var).HU
                '    Case "TR"
                '        txt = Lists.gDSLists.EUS_LISTS_Drinking.Single(Function(itm As DSLists.EUS_LISTS_DrinkingRow) itm.DrinkingId = var).TR
                '    Case "AL"
                '        txt = Lists.gDSLists.EUS_LISTS_Drinking.Single(Function(itm As DSLists.EUS_LISTS_DrinkingRow) itm.DrinkingId = var).AL
                '    Case Else '"US"
                '        txt = Lists.gDSLists.EUS_LISTS_Drinking.Single(Function(itm As DSLists.EUS_LISTS_DrinkingRow) itm.DrinkingId = var).US
                'End Select
            ElseIf var <= 1 Then
                txt = ""
            End If
        Catch ex As Exception

        End Try

        Return txt
    End Function


    Public Shared Function GetIncomeString(IncomeId As Object, LagID As String) As String
        Dim txt As String = ""
        Dim var = IncomeId

        Try
            If (TypeOf var Is Integer AndAlso var > 20) Then
                Dim row As DSLists.EUS_LISTS_IncomeRow = Lists.gDSLists.EUS_LISTS_Income.Single(Function(itm As DSLists.EUS_LISTS_IncomeRow) itm.IncomeId = var)

                If (Lists.gDSLists.EUS_LISTS_Income.Columns.Contains(LagID)) Then
                    txt = row(LagID).ToString()
                End If
                If (String.IsNullOrEmpty(txt)) Then txt = row.US


                'Select Case LagID
                '    Case "US"
                '        If (Not row.IsUSNull()) Then txt = row.US
                '    Case "GR"
                '        If (Not row.IsGRNull()) Then txt = row.GR
                '    Case "TR"
                '        If (Not row.IsTRNull()) Then txt = row.TR
                '    Case "ES"
                '        If (Not row.IsESNull()) Then txt = row.ES
                '    Case Else 'All other languages
                '        txt = row.US
                'End Select
                'If (String.IsNullOrEmpty(txt)) Then
                '    txt = row.US
                'End If

                'Select Case LagID
                '    Case "GR"
                '        txt = Lists.gDSLists.EUS_LISTS_Income.Single(Function(itm As DSLists.EUS_LISTS_IncomeRow) itm.IncomeId = var).GR
                '    Case "HU"
                '        txt = Lists.gDSLists.EUS_LISTS_Income.Single(Function(itm As DSLists.EUS_LISTS_IncomeRow) itm.IncomeId = var).HU
                '    Case "TR"
                '        txt = Lists.gDSLists.EUS_LISTS_Income.Single(Function(itm As DSLists.EUS_LISTS_IncomeRow) itm.IncomeId = var).TR
                '    Case "AL"
                '        txt = Lists.gDSLists.EUS_LISTS_Income.Single(Function(itm As DSLists.EUS_LISTS_IncomeRow) itm.IncomeId = var).AL
                '    Case Else '"US"
                '        txt = Lists.gDSLists.EUS_LISTS_Income.Single(Function(itm As DSLists.EUS_LISTS_IncomeRow) itm.IncomeId = var).US
                'End Select
            ElseIf var <= 20 Then
                txt = ""
            End If
        Catch ex As Exception

        End Try

        Return txt
    End Function


    Public Shared Function GetNetWorthString(NetWorthId As Object, LagID As String) As String
        Dim txt As String = ""
        Dim var = NetWorthId

        Try
            If (TypeOf var Is Integer AndAlso var > 13) Then
                Dim row As DSLists.EUS_LISTS_NetWorthRow = Lists.gDSLists.EUS_LISTS_NetWorth.Single(Function(itm As DSLists.EUS_LISTS_NetWorthRow) itm.NetWorthId = var)

                If (Lists.gDSLists.EUS_LISTS_NetWorth.Columns.Contains(LagID)) Then
                    txt = row(LagID).ToString()
                End If
                If (String.IsNullOrEmpty(txt)) Then txt = row.US


                'Select Case LagID
                '    Case "US"
                '        If (Not row.IsUSNull()) Then txt = row.US
                '    Case "GR"
                '        If (Not row.IsGRNull()) Then txt = row.GR
                '    Case "TR"
                '        If (Not row.IsTRNull()) Then txt = row.TR
                '    Case "ES"
                '        If (Not row.IsESNull()) Then txt = row.ES
                '    Case Else 'All other languages
                '        txt = row.US
                'End Select
                'If (String.IsNullOrEmpty(txt)) Then
                '    txt = row.US
                'End If


                'Select Case LagID
                '    Case "GR"
                '        txt = Lists.gDSLists.EUS_LISTS_NetWorth.Single(Function(itm As DSLists.EUS_LISTS_NetWorthRow) itm.NetWorthId = var).GR
                '    Case "HU"
                '        txt = Lists.gDSLists.EUS_LISTS_NetWorth.Single(Function(itm As DSLists.EUS_LISTS_NetWorthRow) itm.NetWorthId = var).HU
                '    Case "TR"
                '        txt = Lists.gDSLists.EUS_LISTS_NetWorth.Single(Function(itm As DSLists.EUS_LISTS_NetWorthRow) itm.NetWorthId = var).TR
                '    Case "AL"
                '        txt = Lists.gDSLists.EUS_LISTS_NetWorth.Single(Function(itm As DSLists.EUS_LISTS_NetWorthRow) itm.NetWorthId = var).AL
                '    Case Else '"US"
                '        txt = Lists.gDSLists.EUS_LISTS_NetWorth.Single(Function(itm As DSLists.EUS_LISTS_NetWorthRow) itm.NetWorthId = var).US
                'End Select
            ElseIf var <= 13 Then
                txt = ""
            End If
        Catch ex As Exception

        End Try


        Return txt
    End Function


    Public Shared Function GetRelationshipStatusString(RelationshipStatusId As Object, LagID As String) As String
        Dim txt As String = ""
        Dim var = RelationshipStatusId

        Try
            If (TypeOf var Is Integer AndAlso var > 1) Then
                Dim row As DSLists.EUS_LISTS_RelationshipStatusRow = Lists.gDSLists.EUS_LISTS_RelationshipStatus.Single(Function(itm As DSLists.EUS_LISTS_RelationshipStatusRow) itm.RelationshipStatusId = var)

                If (Lists.gDSLists.EUS_LISTS_RelationshipStatus.Columns.Contains(LagID)) Then
                    txt = row(LagID).ToString()
                End If
                If (String.IsNullOrEmpty(txt)) Then txt = row.US

            ElseIf var <= 1 Then
                txt = ""
            End If
        Catch ex As Exception

        End Try

        Return txt
    End Function



    Public Shared Function GetJobString(JobId As Object, LagID As String) As String
        Dim txt As String = ""
        Dim var = JobId

        Try
            If (TypeOf var Is Integer AndAlso var <> -1) Then
                Dim row As DSLists.EUS_LISTS_JobRow = Lists.gDSLists.EUS_LISTS_Job.Single(Function(itm As DSLists.EUS_LISTS_JobRow) itm.JobID = var)

                If (Lists.gDSLists.EUS_LISTS_Job.Columns.Contains(LagID)) Then
                    txt = row(LagID).ToString()
                End If
                If (String.IsNullOrEmpty(txt)) Then txt = row.US


                'Select Case LagID
                '    Case "US"
                '        If (Not row.IsUSNull()) Then txt = row.US
                '    Case "GR"
                '        If (Not row.IsGRNull()) Then txt = row.GR
                '    Case "TR"
                '        If (Not row.IsTRNull()) Then txt = row.TR
                '    Case "ES"
                '        If (Not row.IsESNull()) Then txt = row.ES
                '    Case Else 'All other languages
                '        txt = row.US
                'End Select
                'If (String.IsNullOrEmpty(txt)) Then
                '    txt = row.US
                'End If

                'Select Case LagID
                '    Case "GR"
                '        txt = Lists.gDSLists.EUS_LISTS_RelationshipStatus.Single(Function(itm As DSLists.EUS_LISTS_RelationshipStatusRow) itm.RelationshipStatusId = var).GR
                '    Case "HU"
                '        txt = Lists.gDSLists.EUS_LISTS_RelationshipStatus.Single(Function(itm As DSLists.EUS_LISTS_RelationshipStatusRow) itm.RelationshipStatusId = var).HU
                '    Case "TR"
                '        txt = Lists.gDSLists.EUS_LISTS_RelationshipStatus.Single(Function(itm As DSLists.EUS_LISTS_RelationshipStatusRow) itm.RelationshipStatusId = var).TR
                '    Case "AL"
                '        txt = Lists.gDSLists.EUS_LISTS_RelationshipStatus.Single(Function(itm As DSLists.EUS_LISTS_RelationshipStatusRow) itm.RelationshipStatusId = var).AL
                '    Case Else '"US"
                '        txt = Lists.gDSLists.EUS_LISTS_RelationshipStatus.Single(Function(itm As DSLists.EUS_LISTS_RelationshipStatusRow) itm.RelationshipStatusId = var).US
                'End Select
            ElseIf var = -1 Then
                txt = ""
            End If
        Catch ex As Exception

        End Try

        Return txt
    End Function

    Public Shared Function GetLookingToMeet_Male_String(LagID As String) As String
        Dim txt As String = ""
        Dim row As DSLists.EUS_LISTS_GenderRow = ProfileHelper.gMaleGender

        If (Lists.gDSLists.EUS_LISTS_Gender.Columns.Contains(LagID)) Then
            txt = row(LagID).ToString()
        End If
        If (String.IsNullOrEmpty(txt)) Then txt = row.US


        'Select Case LagID
        '    Case "US"
        '        If (Not row.IsUSNull()) Then txt = row.US
        '    Case "GR"
        '        If (Not row.IsGRNull()) Then txt = row.GR
        '    Case "TR"
        '        If (Not row.IsTRNull()) Then txt = row.TR
        '    Case "ES"
        '        If (Not row.IsESNull()) Then txt = row.ES
        '    Case Else 'All other languages
        '        txt = row.US
        'End Select
        'If (String.IsNullOrEmpty(txt)) Then
        '    txt = row.US
        'End If

        'Select Case LagID
        '    Case "GR"
        '        txt = ProfileHelper.gMaleGender.GR
        '    Case "HU"
        '        txt = ProfileHelper.gMaleGender.HU
        '    Case "TR"
        '        txt = ProfileHelper.gMaleGender.TR
        '    Case "AL"
        '        txt = ProfileHelper.gMaleGender.AL
        '    Case Else '"US"
        '        txt = ProfileHelper.gMaleGender.US
        'End Select

        Return txt
    End Function


    Public Shared Function GetLookingToMeet_Female_String(LagID As String) As String
        Dim txt As String = ""
        Dim row As DSLists.EUS_LISTS_GenderRow = ProfileHelper.gFemaleGender

        If (Lists.gDSLists.EUS_LISTS_Gender.Columns.Contains(LagID)) Then
            txt = row(LagID).ToString()
        End If
        If (String.IsNullOrEmpty(txt)) Then txt = row.US


        'Select Case LagID
        '    Case "US"
        '        If (Not row.IsUSNull()) Then txt = row.US
        '    Case "GR"
        '        If (Not row.IsGRNull()) Then txt = row.GR
        '    Case "TR"
        '        If (Not row.IsTRNull()) Then txt = row.TR
        '    Case "ES"
        '        If (Not row.IsESNull()) Then txt = row.ES
        '    Case Else 'All other languages
        '        txt = row.US
        'End Select
        'If (String.IsNullOrEmpty(txt)) Then
        '    txt = row.US
        'End If


        'Select Case LagID
        '    Case "GR"
        '        txt = ProfileHelper.gFemaleGender.GR
        '    Case "HU"
        '        txt = ProfileHelper.gFemaleGender.HU
        '    Case "TR"
        '        txt = ProfileHelper.gFemaleGender.TR
        '    Case "AL"
        '        txt = ProfileHelper.gFemaleGender.AL
        '    Case Else '"US"
        '        txt = ProfileHelper.gFemaleGender.US
        'End Select

        Return txt
    End Function


    Public Shared Function GetGenderString(genderId As Integer?, LagID As String) As String
        Dim txt As String = ""
        If (IsMale(genderId)) Then

            Dim row As DSLists.EUS_LISTS_GenderRow = ProfileHelper.gMaleGender

            If (Lists.gDSLists.EUS_LISTS_Gender.Columns.Contains(LagID)) Then
                txt = row(LagID).ToString()
            End If
            If (String.IsNullOrEmpty(txt)) Then txt = row.US


            'Select Case LagID
            '    Case "US"
            '        If (Not row.IsUSNull()) Then txt = row.US
            '    Case "GR"
            '        If (Not row.IsGRNull()) Then txt = row.GR
            '    Case "TR"
            '        If (Not row.IsTRNull()) Then txt = row.TR
            '    Case "ES"
            '        If (Not row.IsESNull()) Then txt = row.ES
            '    Case Else 'All other languages
            '        txt = row.US
            'End Select
            'If (String.IsNullOrEmpty(txt)) Then
            '    txt = row.US
            'End If

            'Select Case LagID
            '    Case "GR"
            '        txt = ProfileHelper.gMaleGender.GR
            '    Case "HU"
            '        txt = ProfileHelper.gMaleGender.HU
            '    Case "TR"
            '        txt = ProfileHelper.gMaleGender.TR
            '    Case "AL"
            '        txt = ProfileHelper.gMaleGender.AL
            '    Case Else '"US"
            '        txt = ProfileHelper.gMaleGender.US
            'End Select

        Else

            Dim row As DSLists.EUS_LISTS_GenderRow = ProfileHelper.gFemaleGender

            If (Lists.gDSLists.EUS_LISTS_Gender.Columns.Contains(LagID)) Then
                txt = row(LagID).ToString()
            End If
            If (String.IsNullOrEmpty(txt)) Then txt = row.US


            'Select Case LagID
            '    Case "US"
            '        If (Not row.IsUSNull()) Then txt = row.US
            '    Case "GR"
            '        If (Not row.IsGRNull()) Then txt = row.GR
            '    Case "TR"
            '        If (Not row.IsTRNull()) Then txt = row.TR
            '    Case "ES"
            '        If (Not row.IsESNull()) Then txt = row.ES
            '    Case Else 'All other languages
            '        txt = row.US
            'End Select
            'If (String.IsNullOrEmpty(txt)) Then
            '    txt = row.US
            'End If

            'Select Case LagID
            '    Case "GR"
            '        txt = ProfileHelper.gFemaleGender.GR
            '    Case "HU"
            '        txt = ProfileHelper.gFemaleGender.HU
            '    Case "TR"
            '        txt = ProfileHelper.gFemaleGender.TR
            '    Case "AL"
            '        txt = ProfileHelper.gFemaleGender.AL
            '    Case Else '"US"
            '        txt = ProfileHelper.gFemaleGender.US
            'End Select

        End If

        Return txt
    End Function


    Public Shared Function GetPhotosDisplayLevelString(PhotosDisplayLevelId As Object, LagID As String) As String
        Dim txt As String = ""
        Dim var = PhotosDisplayLevelId

        Try
            If (TypeOf var Is Integer AndAlso var <> -1) Then
                Dim row As DSLists.EUS_LISTS_PhotosDisplayLevelRow = Lists.gDSLists.EUS_LISTS_PhotosDisplayLevel.Single(Function(itm As DSLists.EUS_LISTS_PhotosDisplayLevelRow) itm.PhotosDisplayLevelId = var)

                If (Lists.gDSLists.EUS_LISTS_PhotosDisplayLevel.Columns.Contains(LagID)) Then
                    txt = row(LagID).ToString()
                End If
                If (String.IsNullOrEmpty(txt)) Then txt = row.US


                'Select Case LagID
                '    Case "US"
                '        If (Not row.IsUSNull()) Then txt = row.US
                '    Case "GR"
                '        If (Not row.IsGRNull()) Then txt = row.GR
                '    Case "TR"
                '        If (Not row.IsTRNull()) Then txt = row.TR
                '    Case "ES"
                '        If (Not row.IsESNull()) Then txt = row.ES
                '    Case Else 'All other languages
                '        txt = row.US
                'End Select
                'If (String.IsNullOrEmpty(txt)) Then
                '    txt = row.US
                'End If
            ElseIf var = -1 Then

            End If
        Catch ex As Exception

        End Try

        Return txt
    End Function


    Public Shared Function GetCountryName(countryCode As String) As String
        Dim txt As String = ""
        If (Not String.IsNullOrEmpty(countryCode) AndAlso countryCode <> "-1") Then
            Try

                Dim row As DSGEO.SYS_CountriesGEORow = SYS_CountriesGEOHelper.gDSListsGEO__IsEnabled_PrintableNameASC.SYS_CountriesGEO.SingleOrDefault(Function(itm As DSGEO.SYS_CountriesGEORow) itm.Iso = countryCode)
                If (row IsNot Nothing) Then
                    txt = row.PrintableName
                End If
            Catch ex As Exception

            End Try
        End If
        Return txt
    End Function



    Public Shared Function GetCountryCode(countryName As String) As String
        Dim txt As String = ""
        If (Not String.IsNullOrEmpty(countryName)) Then
            Try
                Dim row As DSGEO.SYS_CountriesGEORow = SYS_CountriesGEOHelper.gDSListsGEO__IsEnabled_PrintableNameASC.SYS_CountriesGEO.Single(Function(itm As DSGEO.SYS_CountriesGEORow) itm.Name = countryName.ToUpper())
                If (row IsNot Nothing) Then
                    txt = row.Iso
                End If
            Catch ex As Exception

            End Try
        End If
        Return txt
    End Function


    'Shared Function ZodiacName2(ByVal Birthday As Date) As String
    '    Dim year As Integer = Birthday.Year
    '    Dim zodiacs = {
    '      New With {.From = New Date(year, 1, 1), .[To] = New Date(year, 1, 19), .Zodiac = "Capricorn"},
    '      New With {.From = New Date(year, 1, 20), .[To] = New Date(year, 2, 18), .Zodiac = "Aquarius"},
    '      New With {.From = New Date(year, 2, 19), .[To] = New Date(year, 3, 20), .Zodiac = "Pisces"},
    '      New With {.From = New Date(year, 3, 21), .[To] = New Date(year, 4, 19), .Zodiac = "Aries"},
    '      New With {.From = New Date(year, 4, 20), .[To] = New Date(year, 5, 20), .Zodiac = "Taurus"},
    '      New With {.From = New Date(year, 5, 21), .[To] = New Date(year, 6, 20), .Zodiac = "Gemini"},
    '      New With {.From = New Date(year, 6, 21), .[To] = New Date(year, 7, 22), .Zodiac = "Cancer"},
    '      New With {.From = New Date(year, 7, 23), .[To] = New Date(year, 8, 22), .Zodiac = "Leo"},
    '      New With {.From = New Date(year, 8, 23), .[To] = New Date(year, 9, 22), .Zodiac = "Virgo"},
    '      New With {.From = New Date(year, 9, 23), .[To] = New Date(year, 10, 22), .Zodiac = "Libra"},
    '      New With {.From = New Date(year, 10, 23), .[To] = New Date(year, 11, 21), .Zodiac = "Scorpio"},
    '      New With {.From = New Date(year, 11, 22), .[To] = New Date(year, 12, 21), .Zodiac = "Sagittarius"},
    '      New With {.From = New Date(year, 12, 22), .[To] = New Date(year, 12, 31), .Zodiac = "Capricorn"}}

    '    Dim str As String = (From z In zodiacs Where (z.From <= Birthday And Birthday <= z.[To])).Single.Zodiac
    '    Return str
    'End Function



    Public Shared Function ZodiacName(pBirthDate As Date) As String
        Dim pMonth As Integer
        Dim pDay As Integer
        pMonth = Month(pBirthDate)
        pDay = Day(pBirthDate)

        'Aries - March 21 - April 20
        'Taurus - April 21 - May 21
        'Gemini - May 22 - June 21
        'Cancer - June 22 - July 22
        'Leo - July 23 -August 21
        'Virgo - August 22 - September 23
        'Libra - September 24 - October 23
        'Scorpio - October 24 - November 22
        'Sagittarius - November 23 - December 22
        'Capricorn - December 23 - January 20
        'Aquarius - January 21 - February 19
        'Pisces - February 20- March 20


        If pMonth = 3 Then
            If pDay >= 21 Then
                ZodiacName = "Aries"
            Else
                ZodiacName = "Pisces"
            End If
        ElseIf pMonth = 4 Then
            If pDay >= 21 Then
                ZodiacName = "Taurus"
            Else
                ZodiacName = "Aries"
            End If
        ElseIf pMonth = 5 Then
            If pDay >= 22 Then
                ZodiacName = "Gemini"
            Else
                ZodiacName = "Taurus"
            End If
        ElseIf pMonth = 6 Then
            If pDay >= 22 Then
                ZodiacName = "Cancer"
            Else
                ZodiacName = "Gemini"
            End If
        ElseIf pMonth = 7 Then
            If pDay >= 23 Then
                ZodiacName = "Leo"
            Else
                ZodiacName = "Cancer"
            End If
        ElseIf pMonth = 8 Then
            If pDay >= 23 Then
                ZodiacName = "Virgo"
            Else
                ZodiacName = "Leo"
            End If
        ElseIf pMonth = 9 Then
            If pDay >= 24 Then
                ZodiacName = "Libra"
            Else
                ZodiacName = "Virgo"
            End If
        ElseIf pMonth = 10 Then
            If pDay >= 24 Then
                ZodiacName = "Scorpio"
            Else
                ZodiacName = "Libra"
            End If
        ElseIf pMonth = 11 Then
            If pDay >= 23 Then
                ZodiacName = "Sagittarius"
            Else
                ZodiacName = "Scorpio"
            End If
        ElseIf pMonth = 12 Then
            If pDay >= 23 Then
                ZodiacName = "Capricorn"
            Else
                ZodiacName = "Sagittarius"
            End If
        ElseIf pMonth = 1 Then
            If pDay >= 21 Then
                ZodiacName = "Aquarius"
            Else
                ZodiacName = "Capricorn"
            End If
        Else 'If pMonth = 2 Then
            If pDay >= 20 Then
                ZodiacName = "Pisces"
            Else
                ZodiacName = "Aquarius"
            End If
        End If
    End Function
    Public Shared Function CanHeTakeBirthdayBonus(ByVal ProfileId) As Boolean

        Return False

    End Function

    'Public Shared Function GetProfilesNextToPostalCode(countryCode As String, zip As Integer, radius As Integer, lagid As String)


    'End Function


    Shared _Config_UNLOCK_CONVERSATION As Double?
    Public Shared ReadOnly Property Config_UNLOCK_CONVERSATION_CREDITS As Double
        Get
            Try
                If (_Config_UNLOCK_CONVERSATION Is Nothing) Then _Config_UNLOCK_CONVERSATION = clsUserDoes.GetRequiredCredits(UnlockType.UNLOCK_CONVERSATION).CreditsAmount
            Catch ex As Exception
                _Config_UNLOCK_CONVERSATION = -1
            End Try

            Return _Config_UNLOCK_CONVERSATION
        End Get
    End Property


    Shared _Config_UNLOCK_MESSAGE_READ As Double?
    Public Shared ReadOnly Property Config_UNLOCK_MESSAGE_READ_CREDITS As Double
        Get
            Try
                If (_Config_UNLOCK_MESSAGE_READ Is Nothing) Then _Config_UNLOCK_MESSAGE_READ = clsUserDoes.GetRequiredCredits(UnlockType.UNLOCK_MESSAGE_READ).CreditsAmount
            Catch ex As Exception
                _Config_UNLOCK_MESSAGE_READ = -1
            End Try
            Return _Config_UNLOCK_MESSAGE_READ
        End Get
    End Property


    Shared _Config_UNLOCK_MESSAGE_SEND As Double?
    Public Shared ReadOnly Property Config_UNLOCK_MESSAGE_SEND_CREDITS As Double
        Get
            Try
                If (_Config_UNLOCK_MESSAGE_SEND Is Nothing) Then _Config_UNLOCK_MESSAGE_SEND = clsUserDoes.GetRequiredCredits(UnlockType.UNLOCK_MESSAGE_SEND).CreditsAmount
            Catch ex As Exception
                _Config_UNLOCK_MESSAGE_SEND = -1
            End Try
            Return _Config_UNLOCK_MESSAGE_SEND
        End Get
    End Property


    Public Shared ReadOnly Property Config_UNLOCK_MESSAGE_FREE_CREDITS As Double
        Get
            Return 0
        End Get
    End Property



    Public Shared Sub ClearCache()
        _Config_UNLOCK_MESSAGE_READ = Nothing
        _Config_UNLOCK_MESSAGE_SEND = Nothing
        _Config_UNLOCK_CONVERSATION = Nothing
    End Sub



    Public Shared Function DeleteAccount(MasterProfileId As Integer, MirrorProfileId As Integer, params As DeleteAccountParams, Optional sendEmailToSupport As Boolean = True) As ActionResult
        Dim ActionResult1 As New ActionResult()
        '    Dim result1 As ActionsOnProfileStatusEnum = ActionsOnProfileStatusEnum.None

        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)



            Try

                Dim _currentProfile As vw_EusProfile_Light = DataHelpers.GetCurrentProfile(_CMSDBDataContext, MasterProfileId, MirrorProfileId)



                If (_currentProfile Is Nothing) Then
                    ActionResult1.HasErrors = True
                    ActionResult1.ErrorCode = ActionsOnProfileStatusEnum.ProfileNotFound
                    ActionResult1.Message = "Profile not found"
                End If


                If (Not ActionResult1.HasErrors) Then

                    DataHelpers.EUS_Profiles_MarkDeleted(MasterProfileId)

                    Try
                        Dim body As String = <body><![CDATA[
User deleted profile. <br><br>
LoginName: <b>[LOGIN]</b>  <br>
Register date: <b>[REG_DATE]</b><br><br>
]]></body>
                        body = body.Replace("[LOGIN]", _currentProfile.LoginName)
                        body = body.Replace("[REG_DATE]", _currentProfile.DateTimeToRegister.Value.ToString("dd/MM/yyyy HH:mm"))

                        If (_currentProfile.GenderId = 1) Then
                            Try

                                Dim body2 As String = <body><![CDATA[
Total money sum: [MONEY]<br>
Total credits sum: [CREDITS]<br><br>
]]></body>

                                Using result As DataTable = clsCreditsHelper.GetCustomerTotalMoneyAndCredits(MasterProfileId)



                                    body2 = body2.Replace("[MONEY]", result.Rows(0)("DebitAmountReceived"))
                                    body2 = body2.Replace("[CREDITS]", result.Rows(0)("Credits"))
                                End Using
                                body = body & body2

                            Catch ex As Exception
                                ActionResult1.HasErrors = True
                                ActionResult1.ErrorCode = ActionsOnProfileStatusEnum.Exception
                                ActionResult1.Message = ex.ToString()
                            End Try

                        End If

                        body = body & <body><![CDATA[
--- <strong>System Details</strong> ------------------------------------<br>
IP: ###IP### GEO IP = ###GEOIP###<br>
Agent: ###AGENT###<br>
Referrer: ###REFERRER###<br>
<strong>Custom Referrer</strong>: ###CUSTOMREFERRER###<br>
CustomerID: ###CUSTOMERID###<br>
<strong>Keywords</strong>:###SEARCHENGINEKEYWORDS###<br>
<strong>Landing Page</strong>:###LANDINGPAGE###<br>
<br><br>
]]></body>.Value

                        body = body.Replace("###IP###", params.IP) 'Session("IP"))
                        body = body.Replace("###GEOIP###", params.GEO_COUNTRY_CODE) ' Session("GEO_COUNTRY_CODE"))

                        Try
                            body = body.Replace("###AGENT###", params.HTTP_USER_AGENT) ' Request.Params("HTTP_USER_AGENT"))
                        Catch ex As Exception
                        End Try

                        Try
                            body = body.Replace("###REFERRER###", clsHTMLHelper.CreateURLLink(params.Referrer)) ' clsHTMLHelper.CreateURLLink(IIf(Session("Referrer") IsNot Nothing, Session("Referrer"), "")))
                        Catch ex As Exception
                            body = body.Replace("###REFERRER###", "")
                        End Try

                        Try
                            body = body.Replace("###CUSTOMREFERRER###", _currentProfile.CustomReferrer)
                        Catch ex As Exception
                            body = body.Replace("###CUSTOMREFERRER###", "")
                        End Try

                        Try
                            body = body.Replace("###CUSTOMERID###", MasterProfileId)
                        Catch ex As Exception
                            body = body.Replace("###CUSTOMERID###", "")
                        End Try

                        Try
                            body = body.Replace("###SEARCHENGINEKEYWORDS###", _currentProfile.SearchKeywords)
                        Catch ex As Exception
                            body = body.Replace("###SEARCHENGINEKEYWORDS###", "")
                        End Try

                        Try
                            body = body.Replace("###LANDINGPAGE###", clsHTMLHelper.CreateURLLink(_currentProfile.LandingPage))
                        Catch ex As Exception
                            body = body.Replace("###LANDINGPAGE###", "")
                        End Try



                        '' last 3 received and last 3 sent messages
                        ''''''''''''
                        Try

                            Dim result As List(Of EUS_Messages_GetLastExchangedResult) = _CMSDBDataContext.EUS_Messages_GetLastExchanged(MasterProfileId).ToList()


                            'Dim ds As DataTable = clsMessagesHelper.GetMessages_LastExchanged(Me.MasterProfileId)
                            'Dim sb As New StringBuilder()
                            Dim htmlTable As String = <tbl><![CDATA[
    <div><u><b>Message [IsSentReceived] [COUNT]</b></u></div>
    <table>
        <tr>
            <th align="right" valign="top">From:<th>
            <td>[FromLoginName]<td>
        </tr>
        <tr>
            <th align="right" valign="top">To:<th>
            <td>[ToLoginName]<td>
        </tr>
        <tr>
            <th align="right" valign="top">Date:<th>
            <td>[DateTimeToCreate]<td>
        </tr>
        <tr>
            <th align="right" valign="top">Subject:<th>
            <td>[Subject]<td>
        </tr>
        <tr>
            <th align="right" valign="top">Body:<th>
            <td>[Body]<td>
        </tr>
        <tr>
            <th align="right" valign="top">MessageID:<th>
            <td>[EUS_MessageID]<td>
        </tr>
    </table>
    <br/>
]]></tbl>

                            Dim wrapper As String = <tbl><![CDATA[
<table border="1">
    <tr><td valign="top"><div style="width:350px">[Received1]</div></td><td valign="top"><div style="width:350px">[Sent1]</div></td></tr>
    <tr><td valign="top"><div style="width:350px">[Received2]</div></td><td valign="top"><div style="width:350px">[Sent2]</div></td></tr>
    <tr><td valign="top"><div style="width:350px">[Received3]</div></td><td valign="top"><div style="width:350px">[Sent3]</div></td></tr>
</table>
]]></tbl>
                            '    Dim cell As Integer = 0
                            Dim count1 As Integer = 0
                            Dim previousValue As String = ""
                            For cnt = 0 To result.Count - 1
                                Dim row1 As String = htmlTable
                                Dim dr As EUS_Messages_GetLastExchangedResult = result(cnt)

                                row1 = row1.Replace("[EUS_MessageID]", dr.EUS_MessageID)

                                If (Not String.IsNullOrEmpty(dr.IsSentReceived)) Then
                                    row1 = row1.Replace("[IsSentReceived]", dr.IsSentReceived)
                                Else
                                    row1 = row1.Replace("[IsSentReceived]", "")
                                End If

                                If ((previousValue = "") OrElse (dr.IsSentReceived <> previousValue)) Then
                                    count1 = 1
                                    previousValue = dr.IsSentReceived
                                ElseIf (dr.IsSentReceived = previousValue) Then
                                    count1 = count1 + 1
                                End If
                                row1 = row1.Replace("[COUNT]", count1)


                                If (Not String.IsNullOrEmpty(dr.FromLoginName)) Then
                                    row1 = row1.Replace("[FromLoginName]", dr.FromLoginName)
                                Else
                                    row1 = row1.Replace("[FromLoginName]", "")
                                End If

                                If (Not String.IsNullOrEmpty(dr.ToLoginName)) Then
                                    row1 = row1.Replace("[ToLoginName]", dr.ToLoginName)
                                Else
                                    row1 = row1.Replace("[ToLoginName]", "")
                                End If

                                If (dr.DateTimeToCreate.HasValue) Then
                                    row1 = row1.Replace("[DateTimeToCreate]", dr.DateTimeToCreate.Value.ToString("dd/MM/yyyy HH:mm"))
                                Else
                                    row1 = row1.Replace("[DateTimeToCreate]", "")
                                End If

                                If (Not String.IsNullOrEmpty(dr.Subject)) Then
                                    row1 = row1.Replace("[Subject]", dr.Subject)
                                Else
                                    row1 = row1.Replace("[Subject]", "")
                                End If

                                If (Not String.IsNullOrEmpty(dr.Body)) Then
                                    row1 = row1.Replace("[Body]", dr.Body)
                                Else
                                    row1 = row1.Replace("[Body]", "")
                                End If

                                If (Not String.IsNullOrEmpty(dr.IsSentReceived)) Then
                                    wrapper = wrapper.Replace("[" & dr.IsSentReceived & count1 & "]", row1)
                                End If
                            Next

                            If (result.Count > 0) Then
                                body = body & "<br/><br/>" & wrapper
                            End If


                        Catch ex As Exception
                            ActionResult1.HasErrors = True
                            ActionResult1.ErrorCode = ActionsOnProfileStatusEnum.Exception
                            ActionResult1.Message = ex.Message
                            ActionResult1.ExtraMessage = ex.ToString()
                        End Try

                        Dim MailData As New clsMyMail.clsMailData()



                        MailData.ToAddress = params.SupportTeamEmail
                        MailData.Subject = "User deleted profile on Goomena.com"
                        MailData.Content = body
                        MailData.IsHtmlBody = True
                        MailData.BCCRecipients = Nothing
                        MailData.IsSupportMail = True
                        ActionResult1.MailData = MailData
                        If (sendEmailToSupport) Then
                            clsMyMail.TrySendMail(MailData)
                        End If

                    Catch ex As Exception
                        ActionResult1.HasErrors = True
                        ActionResult1.ErrorCode = ActionsOnProfileStatusEnum.Exception
                        ActionResult1.Message = ex.Message
                        ActionResult1.ExtraMessage = ex.ToString()
                    End Try

                End If

            Catch ex As Exception
                ActionResult1.HasErrors = True
                ActionResult1.ErrorCode = ActionsOnProfileStatusEnum.Exception
                ActionResult1.Message = ex.Message
                ActionResult1.ExtraMessage = ex.ToString()

            End Try
        End Using
        Return ActionResult1
    End Function




    ''' <summary>
    ''' set next approved public photo as default
    ''' </summary>
    ''' <remarks></remarks>
    Public Shared Sub SetNextDefaultPhoto(MasterProfileId As Integer)
        Dim CustomerPhotosID As Long = DataHelpers.EUS_CustomerPhotos_SetDefault(MasterProfileId, Nothing, False, False)
        If (CustomerPhotosID > 0) Then
            DataHelpers.UpdateEUS_Profiles_MirrorSetStatusUpdating(MasterProfileId, ProfileStatusEnum.Updating, ProfileModifiedEnum.UpdatingPhotos)
            clsCustomer.AddFreeRegistrationCredits_WithCheck(MasterProfileId, "", "", "")
            clsUserDoes.Update_MobileNotifications(MasterProfileId, MasterProfileId, MemberActionTypeEnum.DefaultPhotoCanged)
            DataHelpers.UpdateEUS_CustomerPhotos_IsUpdating(MasterProfileId, CustomerPhotosID, True)
        End If
    End Sub

    Public Function SetPhotoLevel(MasterProfileId As Integer, CustomerPhotosID As Long, level As Integer) As Boolean
        Dim success As Boolean = False
        Dim allowAutoApproved As Boolean
        'query for photo auto approve setting
        Try
            Dim config As New clsConfigValues()
            allowAutoApproved = (config.auto_approve_photos = "1")
        Catch ex As Exception
            RaiseEvent OnExceptionThrowed(ex, "AutoApprovePhoto query failed.")
        End Try

        Dim isDefault As Boolean = False
        Dim isUpdating As Boolean = True

        Try
            Using ds As DSMembers = DataHelpers.GetEUS_CustomerPhotosByCustomerPhotosID(CustomerPhotosID)


                Dim photoRow As DSMembers.EUS_CustomerPhotosRow = ds.EUS_CustomerPhotos.Rows(0)

                If (Not photoRow.IsNull("IsDefault")) Then
                    isDefault = photoRow.IsDefault
                End If

                If (allowAutoApproved) Then
                    photoRow.HasAproved = True
                    photoRow.IsDefault = False
                Else
                    If (photoRow.DisplayLevel > 0 AndAlso level = 0) Then
                        photoRow.HasAproved = False
                        photoRow.IsDefault = False
                    ElseIf (photoRow.DisplayLevel > 0 AndAlso level > 0) Then
                        isUpdating = False
                    End If
                End If

                photoRow.DisplayLevel = level
                DataHelpers.UpdateEUS_CustomerPhotos(ds)
                If (isUpdating) Then
                    DataHelpers.UpdateEUS_Profiles_MirrorSetStatusUpdating(MasterProfileId, ProfileStatusEnum.Updating, ProfileModifiedEnum.UpdatingPhotos)
                    DataHelpers.UpdateEUS_CustomerPhotos_IsUpdating(MasterProfileId, photoRow.CustomerPhotosID, True)
                End If

            End Using
            success = True
        Catch ex As Exception
            RaiseEvent OnExceptionThrowed(ex, "")
        End Try

        If (isDefault) Then
            ProfileHelper.SetNextDefaultPhoto(MasterProfileId)
        End If
        Return success
    End Function


    'Public Function PerfomReloginWithFacebookId(fb_uid As String, fb_name As String, fb_username As String, fb_email As String, rememberUserName As Boolean) As clsDataRecordLoginMemberReturn
    '    Dim DataRecordLoginReturn As New clsDataRecordLoginMemberReturn
    '    DataRecordLoginReturn.IsValid = False
    '    DataRecordLoginReturn.HasErrors = False

    '    Dim dc As New CMSDBDataContext(DataHelpers.ConnectionString)
    '    Try
    '        Dim pass = Nothing

    '        ' retrieve user password
    '        pass = (From itm In dc.EUS_Profiles
    '                Where ((itm.FacebookUserId.ToString() = fb_uid) OrElse
    '                       (itm.FacebookName.ToString() = fb_name) OrElse
    '                       (itm.eMail.ToString() = fb_email) OrElse
    '                       (itm.FacebookUserName.ToString() = fb_username)) _
    '                AndAlso _
    '                      (itm.Status = ProfileStatusEnum.Approved OrElse _
    '                         itm.Status = ProfileStatusEnum.NewProfile OrElse _
    '                         itm.Status = ProfileStatusEnum.Rejected) _
    '                AndAlso itm.ProfileID > 1 _
    '                Select itm.LoginName, itm.Password, itm.eMail, itm.FacebookUserId, itm.FacebookName, itm.FacebookUserName).FirstOrDefault()


    '        If (pass IsNot Nothing) Then

    '            Dim prms As New LoginWithFacebookSuccessParams()
    '            prms.LoginName = pass.LoginName
    '            prms.Password = pass.Password
    '            prms.RememberUserName = rememberUserName
    '            prms.DataRecordLoginReturn = DataRecordLoginReturn

    '            RaiseEvent OnLoginWithFacebookSuccess(prms)
    '            DataRecordLoginReturn = prms.DataRecordLoginReturn

    '            'DataRecordLoginReturn = Me.PerfomSiteLogin(pass.LoginName, pass.Password, rememberUserName)
    '            If (Not DataRecordLoginReturn.HasErrors AndAlso
    '                DataRecordLoginReturn.IsValid AndAlso
    '                pass.eMail = fb_email AndAlso (pass.FacebookUserId <> fb_uid OrElse
    '                                               pass.FacebookName <> fb_name OrElse
    '                                               pass.FacebookUserName <> fb_username)) Then

    '                DataHelpers.UpdateEUS_Profiles_FacebookData(DataRecordLoginReturn.ProfileID, fb_uid, fb_name, fb_username)

    '            End If
    '        End If


    '    Catch ex As Exception
    '        DataRecordLoginReturn.HasErrors = True
    '        DataRecordLoginReturn.Message = ex.Message
    '    Finally
    '        dc.Dispose()
    '    End Try

    '    Return DataRecordLoginReturn
    'End Function


    Public Shared Function PerfomReloginWithFacebookId(fb_uid As String, fb_name As String, fb_username As String, fb_email As String) As LoginWithFacebookSuccessParams
        Dim prms As New LoginWithFacebookSuccessParams()

        Using dc As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try
                Dim pass = Nothing

                ' retrieve user password
                '(itm.eMail.ToString() = fb_email) OrElse
                pass = (From itm In dc.EUS_Profiles
                        Where ((itm.FacebookUserId.ToString() = fb_uid) OrElse
                               (itm.FacebookName.ToString() = fb_name) OrElse
                               (itm.FacebookUserName.ToString() = fb_username)) _
                        AndAlso _
                              (itm.Status = ProfileStatusEnum.Approved OrElse _
                                 itm.Status = ProfileStatusEnum.NewProfile OrElse _
                                 itm.Status = ProfileStatusEnum.Rejected) _
                        AndAlso itm.ProfileID > 1 _
                        Select itm.LoginName, itm.Password, itm.eMail, itm.FacebookUserId, itm.FacebookName, itm.FacebookUserName).FirstOrDefault()


                If (pass IsNot Nothing) Then

                    prms.LoginName = pass.LoginName
                    prms.Password = pass.Password
                    'prms.DataRecordLoginReturn = New clsDataRecordLoginMemberReturn()

                    If (pass.eMail = fb_email AndAlso (pass.FacebookUserId <> fb_uid OrElse
                                                       pass.FacebookName <> fb_name OrElse
                                                       pass.FacebookUserName <> fb_username)) Then

                        prms.OnLoginSuccessSaveFBData = True

                    End If
                End If


            Catch ex As Exception
                Throw New System.Exception(ex.Message, ex)
      
            End Try
        End Using
        Return prms
    End Function
    Public Shared Function getHasSeenPopup(ByVal ProfileId As Integer) As Boolean
        Dim re As Boolean = False
        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)

            Try

                '   Dim d As 
                Dim oa As Boolean? = (From f In _CMSDBDataContext.EUS_ProfilesPrivacySettings
                    Where f.ProfileID = ProfileId OrElse f.MirrorProfileID = ProfileId
                    Select f.HasSeenGiftPopup).FirstOrDefault
                If oa.HasValue Then
                    re = oa.Value
                End If
            Catch ex As Exception
                clsMyMail.TrySendMailSupport(ConfigurationManager.AppSettings("ExceptionsEmail"), "On ProfileHelper getHasSeenPopup prID:" & ProfileId, ex.ToString())
        
            End Try

        End Using
        Return re
    End Function
    Public Shared Function SetHasSeenPopup(ByVal ProfileId As Integer, ByVal Value As Boolean) As Boolean
        Dim re As Boolean = False
        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try

                '   Dim d As 
                Dim oa As EUS_ProfilesPrivacySetting() = (From f As EUS_ProfilesPrivacySetting In _CMSDBDataContext.EUS_ProfilesPrivacySettings
                    Where f.ProfileID = ProfileId OrElse f.MirrorProfileID = ProfileId
                    Select f).ToArray
                For Each r As EUS_ProfilesPrivacySetting In oa
                    r.HasSeenGiftPopup = Value
                    ' _CMSDBDataContext.EUS_ProfilesPrivacySettings.InsertAllOnSubmit() 'OnSubmit(r)
                Next
                If oa.Count > 0 Then
                    _CMSDBDataContext.SubmitChanges()
                End If

            Catch ex As Exception
                clsMyMail.TrySendMailSupport(ConfigurationManager.AppSettings("ExceptionsEmail"), "On ProfileHelper SetHasSeenPopup prID:" & ProfileId, ex.ToString())
            
            End Try
                End Using
        Return re
    End Function
   
    Public Shared Function GetRewardProgramParticipate(ByVal ProfileId As Integer) As Boolean
        Dim re As Boolean = False
        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try

                '   Dim d As 
                Dim oa As Boolean? = (From f In _CMSDBDataContext.EUS_ProfilesPrivacySettings
                    Where f.ProfileID = ProfileId OrElse f.MirrorProfileID = ProfileId
                    Select f.RewardProgramParticipate).FirstOrDefault
                If oa.HasValue Then
                    re = oa.Value
                End If

            Catch ex As Exception
                clsMyMail.TrySendMailSupport(ConfigurationManager.AppSettings("ExceptionsEmail"), "On ProfileHelper getRewardProgramParticipate prID:" & ProfileId, ex.ToString())
         
            End Try
        End Using
        Return re
    End Function
    Public Shared Function SetRewardProgramParticipate(ByVal ProfileId As Integer, ByVal Value As Boolean) As Boolean
        Dim re As Boolean = False
        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)

            Try

                '   Dim d As 
                Dim oa As EUS_ProfilesPrivacySetting() = (From f As EUS_ProfilesPrivacySetting In _CMSDBDataContext.EUS_ProfilesPrivacySettings
                    Where f.ProfileID = ProfileId OrElse f.MirrorProfileID = ProfileId
                    Select f).ToArray
                For Each r As EUS_ProfilesPrivacySetting In oa
                    r.RewardProgramParticipate = Value
                    r.RewardProgramParticipateDate = DateTime.UtcNow
                    ' _CMSDBDataContext.EUS_ProfilesPrivacySettings.InsertAllOnSubmit() 'OnSubmit(r)
                Next
                If oa.Count() > 0 Then
                    _CMSDBDataContext.SubmitChanges()
                    re = True
                End If

            Catch ex As Exception
                clsMyMail.TrySendMailSupport(ConfigurationManager.AppSettings("ExceptionsEmail"), "On ProfileHelper SetRewardProgramParticipate prID:" & ProfileId, ex.ToString())
           
            End Try

        End Using
        Return re
    End Function
    Public Shared Function HasCoupons(ByVal ProfileId As Integer, ByVal MirrorProfileId As Integer) As Boolean
        Dim re As Boolean = False




        Try

            Dim sql As String = <sql><![CDATA[
  SELECT count([RewardsGiftCardsOrdersId]) as c
     
  FROM [dbo].[EUS_RewardsGiftCardsOrders]  as ord with (nolock)  
  where [ProfileId]=@PrId or ProfileId =@MPrId

]]></sql>
            Dim dtResults As DataTable = Nothing
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection
                Using cmd As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@PrId", ProfileId)
                    cmd.Parameters.AddWithValue("@MPrId", MirrorProfileId)
                    dtResults = DataHelpers.GetDataTable(cmd)
                    If dtResults IsNot Nothing Then
                        If dtResults.Rows.Count > 0 Then
                            re = If(CInt(dtResults.Rows(0)(0)) > 0, True, False)
                        End If
                    End If
                End Using
            End Using
        Catch ex As Exception
            clsMyMail.TrySendMailSupport(ConfigurationManager.AppSettings("ExceptionsEmail"), "On ProfileHelper  HasCoupons prID:" & ProfileId, ex.ToString())
        End Try
        Return re
    End Function
End Class



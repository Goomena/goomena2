﻿Imports Dating.Server.Datasets.DLL
Imports System.Data.SqlClient
Imports System.Configuration

Public Class clsSuspicious
    Public Shared Function AddSuspiciousIp(ByVal ip As String, ByVal req As String)
        Dim re As Boolean = False
        Dim sql As String = ""
        'This script is based on kilometer. If you use miles simply convert the radius to kilometer (radius=radius*1.609344)
        Try
            sql = <sql><![CDATA[
INSERT INTO [dbo].[SYS_SuspiciousRequests]
           ([Ip]
           ,[UrlRequest]
           ,[DateTime])
     VALUES
           (@ip
           ,@request
           ,GETUTCDATE())
]]></sql>.Value
            Dim txt As String = ""

            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                    command.Parameters.Add(New SqlClient.SqlParameter("@ip", ip))
                    If req.Length > 500 Then
                        txt = req.Substring(0, 498)
                    Else
                        txt = req

                    End If
                    command.Parameters.Add(New SqlClient.SqlParameter("@request", txt))
                    command.Parameters.Add(New SqlClient.SqlParameter("@DateTime", txt))
                    Dim i As Integer = DataHelpers.ExecuteNonQuery(command)
                    If i = 1 Then
                        re = True
                    End If
                End Using
            End Using
        Catch ex As Exception
            clsMyMail.TrySendMailSupport(ConfigurationManager.AppSettings("ExceptionsEmail"), "clsSuspicious.AddSuspiciousIp:" & ip, "UrlRequest: " & req & vbNewLine & ex.ToString())
        End Try
        Return re
    End Function

End Class

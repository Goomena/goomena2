﻿<Serializable()>
Public Class ProfileNotFoundException
    Inherits Exception

    Public Sub New()
        MyBase.New("Specified profile not found. Please logout and login again.")
    End Sub

    Public Sub New(ProfileId As Integer)
        MyBase.New("Specified profile not found (ProfileId:" & ProfileId & ").")
    End Sub

    Public Sub New(ByVal message As String)
        MyBase.New(message)
    End Sub
    Public Sub New(ByVal message As String, ByVal e As Exception)
        MyBase.New(message, e)
    End Sub

    ' A constructor is needed for serialization when an 
    ' exception propagates from a remoting server to the client.  
    Protected Sub New(info As System.Runtime.Serialization.SerializationInfo, context As System.Runtime.Serialization.StreamingContext)

    End Sub
End Class

﻿Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Core.DLL
Imports System.Data.SqlClient
Imports System.Configuration

<Serializable()> _
Public Class clsReferrerCredits


    Private _referrerId As Integer
    Private _affData As ReferrersRepository
    Private _loginName As String


    Private _creditsAvailable As Double
    Public ReadOnly Property CreditsAvailable As Double
        Get
            Return _creditsAvailable
        End Get
    End Property

    Private _creditsPending As Double
    Public ReadOnly Property CreditsPending As Double
        Get
            Return _creditsPending
        End Get
    End Property

    Private _creditsTotal As Double
    Public ReadOnly Property CreditsTotal As Double
        Get
            Return _creditsTotal
        End Get
    End Property


    Private _commissionCreditsAvailable As Double
    Public ReadOnly Property CommissionCreditsAvailable As Double
        Get
            Return _commissionCreditsAvailable
        End Get
    End Property

    Private _commissionCreditsPending As Double
    Public ReadOnly Property CommissionCreditsPending As Double
        Get
            Return _commissionCreditsPending
        End Get
    End Property

    Private _commissionCreditsTotal As Double
    Public ReadOnly Property CommissionCreditsTotal As Double
        Get
            Return _commissionCreditsTotal
        End Get
    End Property



    Private _moneyAvailable As Double
    Public ReadOnly Property MoneyAvailable As Double
        Get
            Return _moneyAvailable
        End Get
    End Property

    Private _moneyPending As Double
    Public ReadOnly Property MoneyPending As Double
        Get
            Return _moneyPending
        End Get
    End Property

    Private _moneyTotal As Double
    Public ReadOnly Property MoneyTotal As Double
        Get
            Return _moneyTotal
        End Get
    End Property



    Public Property DateCreated As DateTime

    Private _convRate As Double
    Public ReadOnly Property ConversionRate As Double
        Get
            Return _convRate
        End Get
    End Property


    Private _familyMembers As Integer
    Public ReadOnly Property FamilyMembers As Integer
        Get
            Return _familyMembers
        End Get
    End Property

    Private _rootFamilyMembers As Integer
    Public ReadOnly Property RootFamilyMembers As Integer
        Get
            Return _rootFamilyMembers
        End Get
    End Property


    Private _config As clsConfigValues
    Private Function GetConfig() As clsConfigValues
        If (_config Is Nothing) Then _config = New clsConfigValues()
        Return _config
    End Function


    Private _referrerCommissions As clsReferrerCommissions
    Public Function GetCurrentReferrerCommissions() As clsReferrerCommissions
        If (_referrerCommissions Is Nothing) Then
            _referrerCommissions = New clsReferrerCommissions()
            _referrerCommissions.Load(Me._referrerId)
        End If
        Return _referrerCommissions
    End Function


    Private _ReferrerCommissions_Admin As DSReferrers.GetReferrerCommissions_AdminDataTable
    Public Function GetCommissionPercentsForAllReferrers() As DSReferrers.GetReferrerCommissions_AdminDataTable
        If (_ReferrerCommissions_Admin Is Nothing) Then
            _ReferrerCommissions_Admin = DataHelpers.GetReferrerCommissions_Admin().GetReferrerCommissions_Admin
        End If
        Return _ReferrerCommissions_Admin
    End Function

    Public ReadOnly Property Datatable As DSCustom.ReferrersRepositoryDataDataTable
        Get
            Return _affData.DataTable
        End Get
    End Property

    Private _dateFrom As DateTime?
    Public ReadOnly Property DateFrom As DateTime?
        Get
            Return _dateFrom
        End Get
    End Property


    Private _dateTo As DateTime?
    Public ReadOnly Property DateTo As DateTime?
        Get
            Return _dateTo
        End Get
    End Property


    'Sub New()

    'End Sub

    Sub New(referrerID As Integer, loginName As String, dateFrom As DateTime?, dateTo As DateTime?)
        Me._referrerId = referrerID
        Me._loginName = loginName
        Me._dateFrom = dateFrom
        Me._dateTo = dateTo
    End Sub


    'Public Sub Load(referrerID As Integer, loginName As String, dateFrom As DateTime?, dateTo As DateTime?)
    '    Me._referrerId = referrerID
    '    Me._loginName = loginName
    '    Me._dateFrom = dateFrom
    '    Me._dateTo = dateTo
    '    Me.Load()
    'End Sub


    Public Sub Load()
        _affData = New ReferrersRepository()

        If (_referrerId > 1) Then

            Me._convRate = GetCurrentReferrerCommissions().REF_CommissionConvRate
            LoadData()
            _affData.CalculateData(_referrerId)

            Me._creditsTotal = _affData.GetCreditsSumLevelsTotal(_referrerId)
            Me._commissionCreditsTotal = _affData.GetCommissionCreditsLevelsTotal(_referrerId)
            Me._moneyTotal = Me._commissionCreditsTotal

            Me._creditsPending = _affData.GetCreditsPendingLevelsTotal(_referrerId)
            Me._commissionCreditsPending = _affData.GetCommissionCreditsPendingLevelsTotal(_referrerId)
            Me._moneyPending = Me._commissionCreditsPending

            Me._creditsAvailable = _affData.GetCreditsAvailableLevelsTotal(_referrerId)
            Me._commissionCreditsAvailable = _affData.GetCommissionCreditsAvailableLevelsTotal(_referrerId)
            Me._moneyAvailable = Me._commissionCreditsAvailable

            Me._familyMembers = _affData.DataTable.Select("Level>0").Length


            ' calc pending and available
            Dim calcCreditsPending As Double = Me._creditsPending * GetCurrentReferrerCommissions().REF_CommissionPendingPercent
            Me._creditsAvailable = Me._creditsAvailable + (Me._creditsPending - calcCreditsPending)
            Me._creditsPending = calcCreditsPending

            Dim calcCommCreditsPending As Double = Me._commissionCreditsPending * GetCurrentReferrerCommissions().REF_CommissionPendingPercent
            Me._commissionCreditsAvailable = Me._commissionCreditsAvailable + (Me._commissionCreditsPending - calcCommCreditsPending)
            Me._commissionCreditsPending = calcCommCreditsPending

            Dim calcMoneyPending As Double = Me._moneyPending * GetCurrentReferrerCommissions().REF_CommissionPendingPercent
            Me._moneyAvailable = Me._moneyAvailable + (Me._moneyPending - calcMoneyPending)
            Me._moneyPending = calcMoneyPending
            ' calc pending and available END


            Me.DateCreated = DateTime.UtcNow
        End If

    End Sub


    Public Sub LoadRoot()
        _affData = New ReferrersRepository()

        If (_referrerId = 1) Then

            LoadData()
            _affData.CalculateData(_referrerId)
            Me._rootFamilyMembers = _affData.DataTable.Rows.Count

            Me.DateCreated = DateTime.UtcNow
        End If

    End Sub


    Public Shared Function CreateCoupon(ByVal RewardsGiftCardsAvaliableId As Integer, ByVal cur As Server.Datasets.DLL.vw_EusProfile_Light, ByVal ip As String, ByVal country As String, ByVal sessionId As String) As clsCreateCouponReturn
        Dim re As New clsCreateCouponReturn
        Using ds As New Server.Datasets.DLL.dsRewarding.EUS_RewardsGiftCardsOrdersDataTable
            Dim Ammount As Decimal = 0
            Dim Points As Decimal = 0
            Using con As New SqlConnection(DataHelpers.ConnectionString)
                Using tmp As New dsRewarding.EUS_RewardsGiftCardsAvaliableDataTable
                    Dim tries As Integer = 0
                    Do
                        Try
                            tries += 1
                            Using ta As New dsRewardingTableAdapters.EUS_RewardsGiftCardsAvaliableTableAdapter
                                ta.Connection = con
                                ta.FillById(tmp, RewardsGiftCardsAvaliableId)
                                If tmp.Rows.Count > 0 Then
                                    Dim r As dsRewarding.EUS_RewardsGiftCardsAvaliableRow = tmp.Rows(0)
                                    Ammount = r.Amount
                                    Points = r.Points
                                    tries = 6
                                End If
                            End Using
                        Catch ex As Exception
                            If tries > 3 Then
                                Throw New Exception("1")
                            End If
                        End Try
                    Loop Until tries > 3
                End Using
                Using cm As New Server.Datasets.DLL.CMSDBDataContext(DataHelpers.ConnectionString)

                    Using traDs As New Server.Datasets.DLL.DSReferrers.AFF_AffiliateTransactionDataTable


                        Try


                            Dim r As Server.Datasets.DLL.dsRewarding.EUS_RewardsGiftCardsOrdersRow = ds.NewRow




                            Dim traRow As Server.Datasets.DLL.DSReferrers.AFF_AffiliateTransactionRow = traDs.NewRow
                            Dim traType As Server.Datasets.DLL.EUS_CustomerTransactionType = (From trt As Server.Datasets.DLL.EUS_CustomerTransactionType In cm.EUS_CustomerTransactionTypes
                                                                                          Where trt.Desc = "Coupon").FirstOrDefault


                            Dim period As Server.Datasets.DLL.SYS_PaymentPeriod = (From prd As Server.Datasets.DLL.SYS_PaymentPeriod In cm.SYS_PaymentPeriods
                                                                                       Where prd.DateStart < DateTime.UtcNow And prd.DateEnd > DateTime.UtcNow).FirstOrDefault
                            Dim PayType As Server.Datasets.DLL.AFF_PayoutType = (From trt As Server.Datasets.DLL.AFF_PayoutType In cm.AFF_PayoutTypes
                                                                                       Where trt.Title = "Coupon").FirstOrDefault

                            traRow.AffiliateID = cur.ProfileID
                            traRow.CreditAmount = Points
                            traRow.DebitAmount = 0
                            traRow.Date_Time = DateTime.UtcNow
                            traRow.Currency = "EUR"
                            traRow.DateStart = If(period IsNot Nothing, period.DateStart, Nothing)
                            traRow.DateEnd = If(period IsNot Nothing, period.DateEnd, Now)
                            traRow.Description = "Coupon"
                            traRow.TransactionType = If(traType IsNot Nothing, traType.CustomerTransactionTypeID, -99)
                            traRow.PayoutTypeID = If(PayType IsNot Nothing, PayType.AFF_PayoutTypeID, -99)

                            traRow.IP = ip
                            traDs.Rows.Add(traRow)

                            'Using ta As New Server.Datasets.DLL.DSReferrersTableAdapters.AFF_AffiliateTransactionTableAdapter

                            '    ta.Connection = con
                            '    Dim rerunTran As Boolean = False
                            '    Try

                            '        Dim s As Integer = ta.Update(traDs)
                            '        Debug.WriteLine(s)
                            '    Catch ex As System.Data.SqlClient.SqlException
                            '        rerunTran = DataHelpers.CheckSqlException(ex.Message)
                            '        If (Not rerunTran) Then
                            '            clsMyMail.TrySendMail(ConfigurationManager.AppSettings("ExceptionsEmail"), "On Adding aafTransaction:" & cur.ProfileID, ex.ToString())
                            '            Throw
                            '        End If

                            '    End Try

                            '    If (rerunTran) Then
                            '        rerunTran = False
                            '        Try
                            '            ta.Update(traDs)

                            '        Catch ex As System.Data.SqlClient.SqlException
                            '            rerunTran = DataHelpers.CheckSqlException(ex.Message)
                            '            If (Not rerunTran) Then
                            '                clsMyMail.TrySendMail(ConfigurationManager.AppSettings("ExceptionsEmail"), "On Adding aafTransaction:" & cur.ProfileID, ex.ToString())
                            '                Throw
                            '            End If
                            '        End Try
                            '    End If

                            '    If (rerunTran) Then
                            '        rerunTran = False

                            '        Throw New Exception("Couldn't Create Coupon. Please contact the Administrator. Stage Failed 1.")

                            '    End If



                            'End Using
                            If traDs.Rows.Count > 0 Then
                                traRow = traDs.Rows(0)
                            End If
                            '    If traRow.AffiliateTransactionID > -1 Then

                            r.DateTimeCreated = DateTime.UtcNow
                            r.Points = Points
                            r.Amount = Ammount
                            r.RewardsGiftCardsAvaliableId = RewardsGiftCardsAvaliableId
                            r.StatusId = 1
                            r.Notes = ""
                            r.LastStatusChangeDateTime = DateTime.UtcNow
                            r.UserId = -1
                            If cur.IsMaster Then
                                r.ProfileId = cur.ProfileID
                            Else
                                If cur.MirrorProfileID Is Nothing Then
                                    r.ProfileId = cur.ProfileID
                                Else
                                    r.ProfileId = cur.MirrorProfileID
                                End If
                            End If


                            r.IP = ip



                            r.SessionID = sessionId
                            r.TransactionId = traRow.AffiliateTransactionID
                            '  Dim str As String = r.ProfileId & "," & traRow.AffiliateTransactionID & "," & DateTime.UtcNow.ToShortDateString & "," & RewardsGiftCardsAvaliableId & "," & sessionId
                            r.MD5 = Guid.NewGuid() '  Dim str As String = r.ProfileId.ToString & "," & r.LoginName & "$" & r.CouponCode & "#" & r.DatetimeCreated.ToString("yyyyMMddHHmmss") & r.SessionID
                            ds.Rows.Add(r)
                            Using ta As New Server.Datasets.DLL.dsRewardingTableAdapters.EUS_RewardsGiftCardsOrdersTableAdapter


                                ta.Connection = con
                                Dim tries As Integer = 0

                                Do

                                    tries += 1

                                    Try

                                        ta.Update(ds)
                                        tries = 6
                                    Catch ex As System.Data.SqlClient.SqlException
                                        If (tries > 3) Then
                                            ta.Dispose()
                                            clsMyMail.TrySendMailSupport(ConfigurationManager.AppSettings("ExceptionsEmail"), "On Adding Coupon:" & cur.ProfileID, ex.ToString())
                                            Throw New Exception("2")
                                        End If
                                    End Try
                                Loop Until tries > 3
                                re.Success = True
                                re.ErrorMessage = ""

                            End Using
                            ' End If
                        Catch ex As Exception
                            re.ErrorMessage = ex.Message
                            re.Success = False
                        End Try
                    End Using
                End Using
            End Using
        End Using
        Return re
    End Function
    Private Sub LoadData()
        ' Dim con As SqlClient.SqlConnection = DataHelpers.GetSqlCommand().Connection
        'Try


        'Dim CreditsSumForReferrers As DSReferrers = DataHelpers.GetReferrersCreditsSum_Admin3(Me._referrerId, True, DateFrom, DateTo)
        '  Dim CommissionsForReferrers As DSReferrers.GetReferrerCommissions_AdminDataTable = GetCommissionPercentsForAllReferrers()

        Dim level As Integer = 0
        Dim mySubReferrers As New List(Of Integer)
        Dim nodesCount As Integer
        Using ds As New DSMembers



            If (Me._referrerId > 1) Then
                level = 1
                'Dim dict As Dictionary(Of String, Double) = GetCreditsSum(Me._referrerId, CreditsSumForReferrers.GetReferrersCreditsSum_Admin3, GetCurrentReferrerCommissions().REF_CommissionPendingPeriod, level)
                Dim dict As Dictionary(Of String, Double) = GetCreditsSum(Me._referrerId, GetCurrentReferrerCommissions().REF_CommissionPendingPeriod, level)
                Using refBonus As DSReferrers = DataHelpers.GetReferrersBonus(DateFrom, DateTo, Me._referrerId)


                    Dim drsBonus As Object = refBonus.REF_Bonus.Compute("SUM(Amount)", "ProfileID=" & Me._referrerId)
                    If (drsBonus IsNot System.DBNull.Value) Then
                        dict("available") = dict("available") + CType(drsBonus, Double)
                        dict("total") = dict("total") + CType(drsBonus, Double)
                    End If
                End Using
                _affData.AddRecord(Me._referrerId, Me._loginName, 0, level, ProfileStatusEnum.Approved, dict("totalCost"), dict("total"), dict("availableCost"), dict("available"), dict("pendingCost"), dict("pending"))
                mySubReferrers.Add(Me._referrerId)
            Else
                level = 0
                _affData.AddRecord(Me._referrerId, Me._loginName, 0, level, ProfileStatusEnum.Approved, 0, 0, 0, 0, 0, 0)
                mySubReferrers.Add(Me._referrerId)
            End If


            ' next levels
            While (mySubReferrers.Count > 0 AndAlso (level < clsReferrer.StopLevel OrElse Me._referrerId = 1))
                level = level + 1

                Try

                    '     Dim aff As New clsReferrer
                    clsReferrer.GetProfilesForReferrerParentIdList(ds, mySubReferrers)

                    If (ds.EUS_Profiles.Rows.Count > 0) Then

                        mySubReferrers.Clear()
                        Using con As New SqlClient.SqlConnection(DataHelpers.ConnectionString)


                            Using cmd As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(con)


                                Dim sqlPattern As String = <sql><![CDATA[
exec [GetReferrersCreditsSum_ForLevel] 	@ReferrerID=@ReferrerID[COUNT], @ReferrerLevel=@ReferrerLevel[COUNT], @REF_CommissionPendingPeriod=@REF_CommissionPendingPeriod[COUNT], @dateFrom=@dateFrom, @dateTo=@dateTo;]]></sql>

                                Dim sb As New System.Text.StringBuilder()
                                Dim c As Integer
                                For c = 0 To ds.EUS_Profiles.Rows.Count - 1
                                    Dim dr As DSMembers.EUS_ProfilesRow = ds.EUS_Profiles.Rows(c)
                                    Dim profId As Integer = dr.ProfileID
                                    '          Dim affComm As clsReferrerCommissions = clsReferrerCommissions.Load(profId, CommissionsForReferrers)


                                    Dim sql2 As String = sqlPattern.Replace("[COUNT]", c.ToString())
                                    sb.Append(sql2)

                                    cmd.Parameters.AddWithValue("@ReferrerID" & c, profId)
                                    cmd.Parameters.AddWithValue("@ReferrerLevel" & c, level)
                                    cmd.Parameters.AddWithValue("@REF_CommissionPendingPeriod" & c, GetCurrentReferrerCommissions().REF_CommissionPendingPeriod)

                                    'If (DateFrom.HasValue) Then
                                    '    cmd.Parameters.AddWithValue("@dateFrom" & c, DateFrom.Value)
                                    'Else
                                    '    Dim dateFrom As New SqlParameter("@dateFrom" & c, SqlDbType.DateTime)
                                    '    dateFrom.Value = System.DBNull.Value
                                    '    cmd.Parameters.Add(dateFrom)
                                    'End If

                                    'If (DateTo.HasValue) Then
                                    '    cmd.Parameters.AddWithValue("@dateTo" & c, DateTo.Value)
                                    'Else
                                    '    Dim dateTo As New SqlParameter("@dateTo" & c, SqlDbType.DateTime)
                                    '    dateTo.Value = System.DBNull.Value
                                    '    cmd.Parameters.Add(dateTo)
                                    'End If

                                    ''Dim dict As Dictionary(Of String, Double) = GetCreditsSum(profId, CreditsSumForReferrers.GetReferrersCreditsSum_Admin3, affComm.REF_CommissionPendingPeriod, level)
                                    'Dim dict As Dictionary(Of String, Double) = GetCreditsSum(profId, GetCurrentReferrerCommissions().REF_CommissionPendingPeriod, level, DateFrom, DateTo)

                                    '_affData.AddRecord(profId, dr.LoginName, dr.ReferrerParentId, level, dr.Status, dict("totalCost"), dict("total"), dict("availableCost"), dict("available"), dict("pendingCost"), dict("pending"))

                                    'mySubReferrers.Add(dr.ProfileID)
                                    'nodesCount = nodesCount + 1
                                Next


                                If (DateFrom.HasValue) Then
                                    cmd.Parameters.AddWithValue("@dateFrom", DateFrom.Value)
                                Else
                                    Dim dateFrom As New SqlParameter("@dateFrom", SqlDbType.DateTime)
                                    dateFrom.Value = System.DBNull.Value
                                    cmd.Parameters.Add(dateFrom)
                                End If

                                If (DateTo.HasValue) Then
                                    cmd.Parameters.AddWithValue("@dateTo", DateTo.Value)
                                Else
                                    Dim dateTo As New SqlParameter("@dateTo", SqlDbType.DateTime)
                                    dateTo.Value = System.DBNull.Value
                                    cmd.Parameters.Add(dateTo)
                                End If

                                cmd.CommandText = sb.ToString()
                                Using ds2 As DataSet = DataHelpers.GetDataSet(cmd)
                                    For tbl = 0 To ds2.Tables.Count - 1
                                        Dim dt As DataTable = ds2.Tables(tbl)

                                        Dim dr As DSMembers.EUS_ProfilesRow = ds.EUS_Profiles.Select("Profileid=" & dt(0)("ReferrerID"))(0)
                                        Dim profId As Integer = dr.ProfileID

                                        Dim dict As New Dictionary(Of String, Double)
                                        dict.Add("pending", dt(0)("pending"))
                                        dict.Add("available", dt(0)("available"))
                                        dict.Add("total", dt(0)("pending") + dt(0)("available"))
                                        dict.Add("pendingCost", dt(0)("pendingCost"))
                                        dict.Add("availableCost", dt(0)("availableCost"))
                                        dict.Add("totalCost", dt(0)("pendingCost") + dt(0)("availableCost"))


                                        _affData.AddRecord(profId, dr.LoginName, dr.ReferrerParentId, level, dr.Status, dict("totalCost"), dict("total"), dict("availableCost"), dict("available"), dict("pendingCost"), dict("pending"))

                                        mySubReferrers.Add(dr.ProfileID)
                                        nodesCount = nodesCount + 1
                                    Next
                                End Using
                            End Using
                        End Using
                        'Dim ds2 As New DataSet()
                        'Dim adapter As New SqlDataAdapter(cmd)

                        'Try
                        '    adapter.Fill(ds)
                        'Catch ex As Exception
                        '    Throw
                        'Finally
                        '    adapter.Dispose()
                        '    cmd.Parameters.Clear()
                        'End Try






                        'For c = 0 To ds.EUS_Profiles.Rows.Count - 1
                        '    Dim dr As DSMembers.EUS_ProfilesRow = ds.EUS_Profiles.Rows(c)
                        '    Dim profId As Integer = dr.ProfileID
                        '    Dim affComm As clsReferrerCommissions = clsReferrerCommissions.Load(profId, CommissionsForReferrers)
                        '    'Dim dict As Dictionary(Of String, Double) = GetCreditsSum(profId, CreditsSumForReferrers.GetReferrersCreditsSum_Admin3, affComm.REF_CommissionPendingPeriod, level)
                        '    Dim dict As Dictionary(Of String, Double) = GetCreditsSum(profId, GetCurrentReferrerCommissions().REF_CommissionPendingPeriod, level, DateFrom, DateTo)

                        '    _affData.AddRecord(profId, dr.LoginName, dr.ReferrerParentId, level, dr.Status, dict("totalCost"), dict("total"), dict("availableCost"), dict("available"), dict("pendingCost"), dict("pending"))

                        '    mySubReferrers.Add(dr.ProfileID)
                        '    nodesCount = nodesCount + 1
                        'Next
                    End If

                Catch ex As Exception
                    Throw
                End Try

            End While
        End Using
        'Catch ex As Exception
        '    Throw
        'Finally
        '    If (con IsNot Nothing) Then con.Close()
        'End Try

    End Sub



    Private Shared Function GetCreditsSum(profileID As Integer, dt As DSReferrers.GetReferrersCreditsSum_Admin3DataTable, REF_CommissionPendingPeriod As Integer, level As Integer) As Dictionary(Of String, Double)

        Dim pending As Double = 0
        Dim available As Double = 0
        Dim pendingCost As Double = 0
        Dim availableCost As Double = 0

        Dim drs As DSReferrers.GetReferrersCreditsSum_Admin3Row() = dt.Select("ReferrerID = " & profileID)
        Dim cnt As Integer
        For cnt = 0 To drs.Length - 1
            Dim DateTimeCreated As DateTime = drs(cnt).DateTimeToCreate
            Dim money As Double
            Dim moneyCost As Double

            Select Case level
                Case 1
                    If (Not drs(cnt).IsREF_L1_MoneyNull()) Then money = drs(cnt).REF_L1_Money
                Case 2
                    If (Not drs(cnt).IsREF_L2_MoneyNull()) Then money = drs(cnt).REF_L2_Money
                Case 3
                    If (Not drs(cnt).IsREF_L3_MoneyNull()) Then money = drs(cnt).REF_L3_Money
                Case 4
                    If (Not drs(cnt).IsREF_L4_MoneyNull()) Then money = drs(cnt).REF_L4_Money
                Case 5
                    If (Not drs(cnt).IsREF_L5_MoneyNull()) Then money = drs(cnt).REF_L5_Money
            End Select
            If (Not drs(cnt).IsCostNull()) Then moneyCost = drs(cnt).Cost

            If (DateTimeCreated.AddDays(REF_CommissionPendingPeriod) > DateTime.UtcNow) Then
                pending = pending + money
                pendingCost = pendingCost + moneyCost
            Else
                available = available + money
                availableCost = availableCost + moneyCost
            End If

        Next


        Dim dict As New Dictionary(Of String, Double)
        dict.Add("pending", pending)
        dict.Add("available", available)
        dict.Add("total", pending + available)
        dict.Add("pendingCost", pendingCost)
        dict.Add("availableCost", availableCost)
        dict.Add("totalCost", pendingCost + availableCost)


        Return dict
    End Function



    Private Function GetCreditsSum(profileID As Integer, REF_CommissionPendingPeriod As Integer, level As Integer) As Dictionary(Of String, Double)
        'ByRef con As SqlClient.SqlConnection,

        Dim sql As String = <sql><![CDATA[
exec [GetReferrersCreditsSum_ForLevel] 	@ReferrerID=@ReferrerID, @ReferrerLevel=@ReferrerLevel, @REF_CommissionPendingPeriod=@REF_CommissionPendingPeriod, @dateFrom=@dateFrom, @dateTo=@dateTo;
]]></sql>
        Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


            Using cmd As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(con, sql)


                'cmd.Connection = con
                cmd.Parameters.AddWithValue("@ReferrerID", profileID)
                cmd.Parameters.AddWithValue("@ReferrerLevel", level)
                cmd.Parameters.AddWithValue("@REF_CommissionPendingPeriod", REF_CommissionPendingPeriod)

                If (DateFrom.HasValue) Then
                    cmd.Parameters.AddWithValue("@dateFrom", DateFrom.Value)
                Else
                    'cmd.Parameters.AddWithValue("@dateFrom", System.DBNull.Value)
                    Dim dateFrom As New SqlParameter("@dateFrom", SqlDbType.DateTime)
                    dateFrom.Value = System.DBNull.Value
                    cmd.Parameters.Add(dateFrom)
                End If

                If (DateTo.HasValue) Then
                    cmd.Parameters.AddWithValue("@dateTo", DateTo.Value)
                Else
                    'cmd.Parameters.AddWithValue("@dateTo", System.DBNull.Value)
                    Dim dateTo As New SqlParameter("@dateTo", SqlDbType.DateTime)
                    dateTo.Value = System.DBNull.Value
                    cmd.Parameters.Add(dateTo)
                End If


                'Dim dt As New DataTable()
                'Dim adapter As New SqlDataAdapter(cmd)

                'Try
                '    adapter.Fill(dt)
                'Catch ex As Exception
                '    Throw
                'Finally
                '    adapter.Dispose()
                '    cmd.Parameters.Clear()
                'End Try


                Dim dt As DataTable = DataHelpers.GetDataTable(cmd)

                Dim dict As New Dictionary(Of String, Double)
                dict.Add("pending", dt(0)("pending"))
                dict.Add("available", dt(0)("available"))
                dict.Add("total", dt(0)("pending") + dt(0)("available"))
                dict.Add("pendingCost", dt(0)("pendingCost"))
                dict.Add("availableCost", dt(0)("availableCost"))
                dict.Add("totalCost", dt(0)("pendingCost") + dt(0)("availableCost"))
                Return dict
            End Using

        End Using
    End Function



    Public Function GetReferrersForLevel(level As Integer) As DSCustom.ReferrersRepositoryDataRow()
        Return _affData.GetReferrersForLevel(level)
    End Function
    Public Function GetSumCreditsReferrersForLevel(level As Integer) As Decimal
        Return _affData.GetSumCreditsReferrersForLevel(level)
    End Function


    Public Function GetReferrersForLevel_SortMaxCredits(level As Integer) As DataRow()
        Return _affData.GetReferrersForLevel_SortMaxCredits(level)
    End Function

    Public Function GetReferrerSales(referrerID As Integer) As Integer
        Return _affData.GetCreditsSum(referrerID)
    End Function


    Public Function GetReferrerCommissions(referrerID As Integer) As Double
        Return _affData.GetCommissionCreditsLevelsTotal(referrerID)
    End Function


    Public Shared Function GetTotalPayment(referrerID As Integer) As Dictionary(Of String, Object)
        Dim data As New Dictionary(Of String, Object)
        data.Add("TotalDebitAmount", 0.0)
        data.Add("LastPaymentDate", DateTime.MinValue)
        data.Add("LastPaymentAmount", 0.0)

        Dim sql As String = <sql><![CDATA[
select * from (
			select 
				'TotalDebitAmount'=SUM(DebitAmount)
			from dbo.AFF_AffiliateTransaction  with (nolock) 
			where AffiliateID=@ReferrerID
) as t, (
			select top(1) 
					Date_Time as 'LastPaymentDate'
					,DebitAmount as 'LastPaymentAmount'
			from dbo.AFF_AffiliateTransaction  with (nolock) 
			where AffiliateID=@ReferrerID
			order by AffiliateTransactionID desc
) as t2
]]></sql>

        sql = sql.Replace("@ReferrerID", referrerID)
        Using dt As DataTable = DataHelpers.GetDataTable(sql)
            If (dt.Rows.Count > 0) Then

                If (Not dt(0).IsNull("TotalDebitAmount")) Then data("TotalDebitAmount") = dt(0)("TotalDebitAmount")
                If (Not dt(0).IsNull("LastPaymentDate")) Then data("LastPaymentDate") = dt(0)("LastPaymentDate")
                If (Not dt(0).IsNull("LastPaymentAmount")) Then data("LastPaymentAmount") = dt(0)("LastPaymentAmount")
            End If
        End Using
     

        Return data
    End Function



    Public Shared Function GetTotalPaymentDS(referrerID As Integer, Optional dateFrom As DateTime? = Nothing, Optional dateTo As DateTime? = Nothing) As DataTable

        Dim sql As String = <sql><![CDATA[
select * from (
	select 
		'TotalDebitAmount'=SUM(DebitAmount)
	from dbo.AFF_AffiliateTransaction  with (nolock) 
	where AffiliateID=@ReferrerID
	and	(Date_Time >= @dateFrom or @dateFrom is null)
	and (Date_Time <= @dateTo or @dateTo is null)
) as t, (
	select top(1) 
			Date_Time as 'LastPaymentDate'
			,DebitAmount as 'LastPaymentAmount'
	from dbo.AFF_AffiliateTransaction  with (nolock) 
	where AffiliateID=@ReferrerID
	and	(Date_Time >= @dateFrom or @dateFrom is null)
	and (Date_Time <= @dateTo or @dateTo is null)
	order by AffiliateTransactionID desc
) as t2
]]></sql>

        sql = sql.Replace("@ReferrerID", referrerID)
        Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


            Using command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(con, sql)


                If (dateFrom.HasValue) Then
                    command.Parameters.Add(New SqlClient.SqlParameter("@dateFrom", dateFrom))
                Else
                    command.Parameters.Add(New SqlClient.SqlParameter("@dateFrom", System.DBNull.Value))
                End If
                If (dateTo.HasValue) Then
                    command.Parameters.Add(New SqlClient.SqlParameter("@dateTo", dateTo))
                Else
                    command.Parameters.Add(New SqlClient.SqlParameter("@dateTo", System.DBNull.Value))
                End If

                Using dt As DataTable = DataHelpers.GetDataTable(command)
                    Return dt
                End Using
            End Using
        End Using
    End Function


End Class


<Serializable()> _
Public Class ReferrersRepository

    Dim dtReferrersData As DSCustom.ReferrersRepositoryDataDataTable
    Public ReadOnly Property DataTable As DSCustom.ReferrersRepositoryDataDataTable
        Get
            Return dtReferrersData
        End Get
    End Property

    Sub New()
        dtReferrersData = New DSCustom.ReferrersRepositoryDataDataTable()
    End Sub


    Public Function GetCreditsSum(profileID As Integer) As Double
        Dim crd As Double = 0
        Dim drs As DataRow() = dtReferrersData.Select("ProfileID=" & profileID)
        If (drs.Length > 0) Then crd = drs(0)("CreditsSum")
        Return crd
    End Function


    Public Function GetCreditsSumLevelsTotal(profileID As Integer) As Double
        Dim crd As Double = 0
        Dim drs As DataRow() = dtReferrersData.Select("ProfileID=" & profileID)
        If (drs.Length > 0) Then crd = drs(0)("CreditsSumLevelsTotal")
        Return crd
    End Function


    Public Function GetCommissionCreditsLevelsTotal(profileID As Integer) As Double
        Dim crd As Double = 0
        Dim drs As DataRow() = dtReferrersData.Select("ProfileID=" & profileID)
        If (drs.Length > 0) Then crd = drs(0)("CommissionCreditsLevelsTotal")
        Return crd
    End Function


    Public Function GetCreditsAvailableLevelsTotal(profileID As Integer) As Double
        Dim crd As Double = 0
        Dim drs As DataRow() = dtReferrersData.Select("ProfileID=" & profileID)
        If (drs.Length > 0) Then crd = drs(0)("CreditsAvailableLevelsTotal")
        Return crd
    End Function


    Public Function GetCommissionCreditsAvailableLevelsTotal(profileID As Integer) As Double
        Dim crd As Double = 0
        Dim drs As DataRow() = dtReferrersData.Select("ProfileID=" & profileID)
        If (drs.Length > 0) Then crd = drs(0)("CommissionCreditsAvailableLevelsTotal")
        Return crd
    End Function


    Public Function GetCreditsPendingLevelsTotal(profileID As Integer) As Double
        Dim crd As Double = 0
        Dim drs As DataRow() = dtReferrersData.Select("ProfileID=" & profileID)
        If (drs.Length > 0) Then crd = drs(0)("CreditsPendingLevelsTotal")
        Return crd
    End Function


    Public Function GetCommissionCreditsPendingLevelsTotal(profileID As Integer) As Double
        Dim crd As Double = 0
        Dim drs As DataRow() = dtReferrersData.Select("ProfileID=" & profileID)
        If (drs.Length > 0) Then crd = drs(0)("CommissionCreditsPendingLevelsTotal")
        Return crd
    End Function


    Public Sub AddRecord(ProfileID As Integer,
                        LoginName As String,
                        ParentId As Integer,
                        level As Integer,
                        Status As Integer,
                        CreditsSum As Double,
                        CommissionTotal As Double,
                        CreditsAvailable As Double,
                        CommissionAvailable As Double,
                        CreditsPending As Double,
                        CommissionPending As Double)

        Dim dr As DSCustom.ReferrersRepositoryDataRow = dtReferrersData.NewReferrersRepositoryDataRow()
        dr.ProfileID = ProfileID
        dr.LoginName = LoginName
        dr.ParentId = ParentId
        dr.ChildsCount = 0
        dr.Level = level
        dr.Status = Status

        dr.CreditsSum = CreditsSum
        dr.CreditsAvailable = CreditsAvailable
        dr.CreditsPending = CreditsPending

        dr.CreditsSumLevelsTotal = 0
        dr.CreditsAvailableLevelsTotal = 0
        dr.CreditsPendingLevelsTotal = 0

        dr.CommissionCredits = CommissionTotal
        dr.CommissionCreditsAvailable = CommissionAvailable
        dr.CommissionCreditsPending = CommissionPending

        dr.CommissionCreditsLevelsTotal = 0
        dr.CommissionCreditsAvailableLevelsTotal = 0
        dr.CommissionCreditsPendingLevelsTotal = 0
        dtReferrersData.AddReferrersRepositoryDataRow(dr)

        'Dim dr As DataRow = dtReferrersData.NewRow()
        'dr("ProfileID") = ProfileID
        'dr("LoginName") = LoginName

        'dr("CreditsSum") = CreditsSum
        'dr("CreditsSumLevelsTotal") = 0
        'dr("CommissionCredits") = CommissionTotal
        'dr("CommissionCreditsLevelsTotal") = 0

        'dr("ParentId") = ParentId
        'dr("ChildsCount") = 0
        'dr("Level") = level

        'dr("CreditsAvailable") = CreditsAvailable
        'dr("CreditsAvailableLevelsTotal") = 0
        'dr("CommissionCreditsAvailable") = CommissionAvailable
        'dr("CommissionCreditsAvailableLevelsTotal") = 0

        'dr("CreditsPending") = CreditsPending
        'dr("CreditsPendingLevelsTotal") = 0
        'dr("CommissionCreditsPending") = CommissionPending
        'dr("CommissionCreditsPendingLevelsTotal") = 0

        'dtReferrersData.Rows.Add(dr)
    End Sub


    Public Sub CalculateData(profileId As Integer)
        Dim drs As DataRow() = dtReferrersData.Select("ProfileID=" & profileId)
        CalculateDataInner(drs)
    End Sub


    Sub CalculateDataInner(drs As DataRow())


        For Each dr As DataRow In drs
            'Dim level As Integer = dr("Level")
            'dr("Level") = level + 1

            Dim drChildren As DataRow() = dtReferrersData.Select("ParentId=" & dr("ProfileID"))
            If (drChildren.Length > 0) Then
                CalculateDataInner(drChildren)
            End If
            Try

                Dim childCount As Integer = drChildren.Length
                For Each drInner As DataRow In drChildren
                    Dim currCount As Integer = drInner("ChildsCount")
                    childCount = childCount + currCount
                Next
                If (childCount > 0) Then dr("ChildsCount") = childCount

            Catch ex As Exception
                Throw
            End Try

            ' total credits
            Dim creditsCount As Double = dr("CreditsSum")
            For Each drInner As DataRow In drChildren
                Dim currCount As Double = drInner("CreditsSumLevelsTotal")
                creditsCount = creditsCount + currCount
            Next
            If (creditsCount > 0) Then dr("CreditsSumLevelsTotal") = creditsCount



            creditsCount = dr("CommissionCredits")
            For Each drInner As DataRow In drChildren
                Dim currCount As Double = drInner("CommissionCreditsLevelsTotal")
                creditsCount = creditsCount + currCount
            Next
            If (creditsCount > 0) Then dr("CommissionCreditsLevelsTotal") = creditsCount



            ' available credits
            creditsCount = dr("CreditsAvailable")
            For Each drInner As DataRow In drChildren
                Dim currCount As Double = drInner("CreditsAvailableLevelsTotal")
                creditsCount = creditsCount + currCount
            Next
            If (creditsCount > 0) Then dr("CreditsAvailableLevelsTotal") = creditsCount



            creditsCount = dr("CommissionCreditsAvailable")
            For Each drInner As DataRow In drChildren
                Dim currCount As Double = drInner("CommissionCreditsAvailableLevelsTotal")
                creditsCount = creditsCount + currCount
            Next
            If (creditsCount > 0) Then dr("CommissionCreditsAvailableLevelsTotal") = creditsCount



            ' pending credits
            creditsCount = dr("CreditsPending")
            For Each drInner As DataRow In drChildren
                Dim currCount As Double = drInner("CreditsPendingLevelsTotal")
                creditsCount = creditsCount + currCount
            Next
            If (creditsCount > 0) Then dr("CreditsPendingLevelsTotal") = creditsCount



            creditsCount = dr("CommissionCreditsPending")
            For Each drInner As DataRow In drChildren
                Dim currCount As Double = drInner("CommissionCreditsPendingLevelsTotal")
                creditsCount = creditsCount + currCount
            Next
            If (creditsCount > 0) Then dr("CommissionCreditsPendingLevelsTotal") = creditsCount

        Next

    End Sub
   
    Public Function GetReferrersForLevel(level As Integer) As DSCustom.ReferrersRepositoryDataRow()
        Dim drs As DSCustom.ReferrersRepositoryDataRow() = dtReferrersData.Select("Level=" & level, "CreditsSum DESC,Status asc")
        Return drs
    End Function
    Public Function GetSumCreditsReferrersForLevel(_level As Integer) As Decimal
        Dim i As Decimal = 0
        Try
            For Each r As DSCustom.ReferrersRepositoryDataRow In GetReferrersForLevel(_level)
                i += r.CommissionCredits
            Next
        Catch ex As Exception
        End Try
        Return i
    End Function


    Public Function GetReferrersForLevel_SortMaxCredits(level As Integer) As DataRow()
        Dim view As DataView = dtReferrersData.AsDataView()
        view.Sort = "CreditsSum DESC"
        Dim drs As DataRow() = view.ToTable().Select("Level=" & level)
        Return drs
    End Function
End Class
Public Class clsCreateCouponReturn
    Public Success As Boolean = False
    Public ErrorMessage As String = "Uknown"
End Class
﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Dating.Server.Core.DLL")> 
<Assembly: AssemblyDescription("Dating.Server.Core.DLL Core library")> 
<Assembly: AssemblyCompany("Goomena.com")> 
<Assembly: AssemblyProduct("Dating.Server.Core.DLL")> 
<Assembly: AssemblyCopyright("Copyright © Goomena.com 2011-2012")> 
<Assembly: AssemblyTrademark("Dating.Server.Core.DLL")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("40fbbcb5-a255-4f3f-92c6-a6ba32a49ae1")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.0.0")> 
<Assembly: AssemblyFileVersion("1.0.0.0")> 

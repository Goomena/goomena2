﻿Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Datasets.DLL.DSMembers
Imports Dating.Server.Core.DLL
Imports System.Data.SqlClient
Imports System.Xml
Imports System.Xml.Linq
Imports System.Configuration


Public Module DataHelpers

    Public Const WAIT_DB_RESTART_MILLISEC As Integer = 15000

    Private _ConnectionString As String
    Friend Property ConnectionString As String
        Get
            Return _ConnectionString
        End Get
        Set(value As String)
            _ConnectionString = value
        End Set
    End Property

    Public Sub SetConnectionString(connString)
        _ConnectionString = connString
    End Sub

#Region "EUS_Profiles"

    Public Function GetEUS_Profiles() As DSMembers
        Using ds As New DSMembers


            Try
                Using ta As New DSMembersTableAdapters.EUS_ProfilesTableAdapter
                    Using con As New SqlConnection(DataHelpers.ConnectionString)

                        ta.Connection = con
                        ta.Fill(ds.EUS_Profiles)

                    End Using
                End Using
            Catch ex As Exception
                Throw New Exception(ex.Message)
           
            End Try
            Return ds
        End Using
    End Function
    Public Function GetCountSimilarProfiles(ByVal ProfileId As Integer) As Integer
        Using ds As New DSWebStatistics
            Dim re As Integer = 0

            Try
                Using ta As New DSWebStatisticsTableAdapters.SYS_ProfilesAccess_GetByCookieTableAdapter
                    Using con As New SqlConnection(DataHelpers.ConnectionString)

                        ta.Connection = con
                        ta.FillBy_CustomerID(ds.SYS_ProfilesAccess_GetByCookie, ProfileId, False)
                        re = ds.SYS_ProfilesAccess_GetByCookie.Rows.Count
                    End Using
                End Using
            Catch ex As Exception
                re = 0
            End Try
            Return re
        End Using
    End Function

    Public Function GetEUS_Profiles_ByProfileID(profileId As Integer) As DSMembers
        Using ds As New DSMembers()


            Try
                Using ta As New DSMembersTableAdapters.EUS_ProfilesTableAdapter
                    Using con As New SqlConnection(DataHelpers.ConnectionString)
                        ta.Connection = con
                        ta.FillByProfileID(ds.EUS_Profiles, profileId)
                    End Using
                End Using
            Catch ex As Exception
                Throw New Exception(ex.Message)
            End Try

            Return ds
        End Using
    End Function


    Public Function GetEUS_Profiles_ByProfileIDRow(profileId As Integer) As DSMembers.EUS_ProfilesRow
        Dim row As DSMembers.EUS_ProfilesRow = Nothing
        Try

       

        Using ta As New DSMembersTableAdapters.EUS_ProfilesTableAdapter
            Using ds As New DSMembers
                Using con As New SqlConnection(DataHelpers.ConnectionString)
                        ta.Connection = con
                    ta.FillByProfileID(ds.EUS_Profiles, profileId)
                    row = ds.EUS_Profiles.Rows(0)
                End Using
            End Using
            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
        Return row
    End Function


    Public Function GetEUS_Profiles_ForReferrerBalance(profileId As Integer?, dateFrom As DateTime?, dateTo As DateTime?, Optional PreviousRowNumber As Integer? = Nothing, Optional RecordsToSelect As Integer? = Nothing) As DSReferrers
        Using ds As New DSReferrers()


            Try
                Using ta As New DSReferrersTableAdapters.GetReferrersBalance_ADMINTableAdapter
                    Using con As New SqlConnection(DataHelpers.ConnectionString)
                        ta.Connection = con
                        ta.Fill(ds.GetReferrersBalance_ADMIN, profileId, dateFrom, dateTo, PreviousRowNumber, RecordsToSelect)
                    End Using

                End Using
            Catch ex As Exception
                Throw New Exception(ex.Message)
            End Try

            Return ds
        End Using
    End Function


    Public Function GetEUS_Profiles_ByProfileOrMirrorID(profileId As Integer) As DSMembers
        Dim rerunTran As Boolean = False
        Using con As New SqlConnection(DataHelpers.ConnectionString)
            Using ta As New DSMembersTableAdapters.EUS_ProfilesTableAdapter()
                ta.Connection = con
                Using ds As New DSMembers()
                    Try
                        Try
                            ta.FillByProfileOrMirrorID(ds.EUS_Profiles, profileId)
                        Catch ex As System.Data.SqlClient.SqlException
                            rerunTran = DataHelpers.CheckSqlException(ex.Message)
                            If (Not rerunTran) Then Throw
                        End Try
                        If (rerunTran) Then
                            rerunTran = False
                            Try
                                ta.FillByProfileOrMirrorID(ds.EUS_Profiles, profileId)
                            Catch ex As System.Data.SqlClient.SqlException
                                rerunTran = DataHelpers.CheckSqlException(ex.Message)
                                If (Not rerunTran) Then Throw
                            End Try
                        End If
                        If (rerunTran) Then
                            rerunTran = False
                            ta.FillByProfileOrMirrorID(ds.EUS_Profiles, profileId)
                        End If
                    Catch ex As Exception
                        Throw New Exception(ex.Message)
                    End Try
                    Return ds
                End Using
            End Using
        End Using
       
    End Function

    Public Function GetEUS_Profiles_AdminUpdater_ByProfileOrMirrorID(profileId As Integer) As DSMembers
        Using ta As New DSMembersTableAdapters.EUS_Profiles_AdminUpdaterTableAdapter
            Try
                Using ds As New DSMembers()
                    Using con As New SqlConnection(DataHelpers.ConnectionString)
                        ta.Connection = con
                        ta.FillBy_ProfileIDOrMirrorProfileID(ds.EUS_Profiles_AdminUpdater, profileId)
                        Return ds
                    End Using
                End Using
               
            Catch ex As Exception
                Throw New Exception(ex.Message)
            End Try
        End Using
    End Function


    Public Function GetEUS_Profiles_ByLoginName(loginName As String) As DSMembers
        Using ta As New DSMembersTableAdapters.EUS_ProfilesTableAdapter
            Using ds As New DSMembers()
                Try
                    Using con As New SqlConnection(DataHelpers.ConnectionString)
                        ta.Connection = con
                        ta.FillBy_LoginName(ds.EUS_Profiles, loginName)
                        Return ds
                    End Using
                Catch ex As Exception
                    Throw New Exception(ex.Message)
                End Try
            End Using
        End Using
    End Function


    Public Function GetEUS_Profiles_ByLoginOrEmail(LoginOrEmail As String) As DSMembers
        Using ta As New DSMembersTableAdapters.EUS_ProfilesTableAdapter
            Using ds As New DSMembers()
                Try
                    Using con As New SqlConnection(DataHelpers.ConnectionString)
                        ta.Connection = con
                        ta.FillBy_LoginOrEmail(ds.EUS_Profiles, LoginOrEmail)
                        Return ds
                    End Using
                Catch ex As Exception
                    Throw New Exception(ex.Message)
                End Try
            End Using
        End Using
    End Function


    Public Function GetEUS_Profiles_ByStatus(profileStatus As Integer) As DSMembers
        Using ta As New DSMembersTableAdapters.EUS_ProfilesTableAdapter
            Using ds As New DSMembers()
                Try


                    Using con As New SqlConnection(DataHelpers.ConnectionString)
                        ta.Connection = con
                        ta.FillByStatus(ds.EUS_Profiles, profileStatus)
                        Return ds
                    End Using
                Catch ex As Exception
                    Throw New Exception(ex.Message)
                End Try
            End Using
        End Using
      
        

    End Function


    Public Function GetEUS_Profiles_ByLoginPass(ByVal LoginName As String, ByVal Password As String) As DSMembers
        Using ta As New DSMembersTableAdapters.EUS_ProfilesTableAdapter
            Using ds As New DSMembers()
                Try
                    Using con As New SqlConnection(DataHelpers.ConnectionString)
                        ta.Connection = con
                        ta.FillByLoginPass(ds.EUS_Profiles, LoginName, Password)
                        Return ds
                    End Using
                Catch ex As Exception
                    Throw New Exception(ex.Message)
                End Try
            End Using
        End Using
       
        

    End Function


    Public Function GetProfileStatistics(CustomerId As Integer, ByVal StartDaysBefore As Integer) As DSMembersViews
        Using ds As New DSMembersViews
            Using ta As New DSMembersViewsTableAdapters.GetProfileStatisticsTableAdapter
                Try
                    Using con As New SqlConnection(DataHelpers.ConnectionString)
                        ta.Connection = con
                        ta.Fill(ds.GetProfileStatistics, CustomerId, StartDaysBefore)
                        Return ds
                    End Using
                Catch ex As Exception
                    Throw New Exception(ex.Message)
                End Try
            End Using
        End Using
    End Function

    Public Function GetReferrersBonus(dateFrom As DateTime?, dateTo As DateTime?, profileID As Integer?) As DSReferrers
       
        Using ta As New DSReferrersTableAdapters.REF_BonusTableAdapter
            Using ds As New DSReferrers()
                Try
                    Using con As New SqlConnection(DataHelpers.ConnectionString)
                        ta.Connection = con
                        ta.FillBy_Params(ds.REF_Bonus, dateFrom, dateTo, profileID)
                        Return ds
                    End Using
                Catch ex As Exception
                    Throw New Exception(ex.Message)
                End Try
            End Using
        End Using
    End Function


    Public Function GetReferrerCommissions_Admin() As DSReferrers
        Using ta As New DSReferrersTableAdapters.GetReferrerCommissions_AdminTableAdapter
            Using ds As New DSReferrers()
                Try
                    Using con As New SqlConnection(DataHelpers.ConnectionString)
                        ta.Connection = con
                        ta.Fill(ds.GetReferrerCommissions_Admin)
                        Return ds
                    End Using
                Catch ex As Exception
                    Throw New Exception(ex.Message)
                End Try
            End Using
        End Using
    End Function


    Public Function GetReferrersTransactions(ByVal DateFrom As Date?, ByVal DateTo As Date?, ByVal AffiliateID As Integer?) As DSReferrers
       
        Using ta As New DSReferrersTableAdapters.GetReferrersTransactionsTableAdapter
            Using ds As New DSReferrers()
                Try
                    Using con As New SqlConnection(DataHelpers.ConnectionString)
                        ta.Connection = con
                        ta.Fill(ds.GetReferrersTransactions, AffiliateID, DateFrom, DateTo, True, False)
                        Return ds
                    End Using
                Catch ex As Exception
                    Throw New Exception(ex.Message)
                End Try
            End Using
        End Using
       
    End Function


    Public Function GetReferrerCreditsHistory(ByVal DateFrom As Date?, ByVal DateTo As Date?, ByVal AffiliateID As Integer?, ByVal LevelParam As Integer?) As DSReferrers
        Using ds As New DSReferrers()
            Using ta As New DSReferrersTableAdapters.GetReferrerCreditsHistory2TableAdapter
                Try
                    Using con As New SqlConnection(DataHelpers.ConnectionString)
                        ta.Connection = con
                        ta.Fill(ds.GetReferrerCreditsHistory2, AffiliateID, DateFrom, DateTo, LevelParam)
                        Return ds
                    End Using
                Catch ex As Exception
                    Throw New Exception(ex.Message)
                End Try
            End Using
        End Using
    End Function



    Public Sub UpdateEUS_Profiles(ByRef ds As DSMembers)
        Try
            Using ta As New DSMembersTableAdapters.EUS_ProfilesTableAdapter
                Using con As New SqlConnection(DataHelpers.ConnectionString)
                    ta.Connection = con
                    ta.Update(ds.EUS_Profiles)
                End Using
            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub


    Public Sub UpdateEUS_Profiles_AdminUpdater(ByRef ds As DSMembers)
        Try
            Using ta As New DSMembersTableAdapters.EUS_Profiles_AdminUpdaterTableAdapter
                Using con As New SqlConnection(DataHelpers.ConnectionString)
                    ta.Connection = con
                    ta.Update(ds.EUS_Profiles_AdminUpdater)

                End Using
            End Using

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub

    Public Function UpdateEUS_Profiles_ModifiedFields(drNew As DSMembers.EUS_ProfilesRow, drOld As DSMembers.EUS_ProfilesRow) As Integer
        Dim rowsAffected As Integer = 0
        If (drNew.RowState = DataRowState.Unchanged) Then Return rowsAffected

        Dim query As New System.Text.StringBuilder()
        Using con As New SqlClient.SqlConnection(DataHelpers.ConnectionString)

            Using cmd As SqlCommand = DataHelpers.GetSqlCommand(con)


                Try


                    ' copy data that is not read only
                    Dim cnt As Integer = 0
                    For cnt = 0 To drNew.Table.Columns.Count - 1
                        Dim dc As DataColumn = drNew.Table.Columns(cnt)
                        If (dc.ReadOnly) Then
                            'If (dc.ReadOnly OrElse dc.ColumnName = "IsMaster" OrElse dc.ColumnName = "MirrorProfileID") Then
                            Continue For
                        End If

                        Dim modified As Boolean = drNew.IsNull(cnt) AndAlso Not drOld.IsNull(cnt)
                        modified = modified OrElse Not drNew.IsNull(cnt) AndAlso drOld.IsNull(cnt)
                        modified = modified OrElse Not drNew.IsNull(cnt) AndAlso Not drOld.IsNull(cnt) AndAlso (drNew(cnt) <> drOld(cnt))

                        If (modified) Then
                            query.AppendLine("[" & dc.ColumnName & "]=@" & dc.ColumnName & ",")
                            If (drNew.IsNull(dc.ColumnName)) Then
                                cmd.Parameters.AddWithValue("@" & dc.ColumnName, System.DBNull.Value)
                            Else
                                cmd.Parameters.AddWithValue("@" & dc.ColumnName, drNew(cnt))
                            End If
                        End If

                    Next


                    If (cmd.Parameters.Count > 0) Then

                        If (query.Length > 1) Then
                            query.Remove(query.Length - 3, 3)
                            query.AppendLine("")
                        End If

                        query.Insert(0, "UPDATE EUS_Profiles SET")
                        query.AppendLine("WHERE ProfileID=@ProfileID")
                        cmd.Parameters.AddWithValue("@ProfileID", drNew("ProfileID"))

                        cmd.CommandText = query.ToString()
                        rowsAffected = DataHelpers.ExecuteNonQuery(cmd)
                    End If
                Catch ex As Exception
                    Throw New Exception(ex.Message)

                End Try
            End Using

        End Using
        Return rowsAffected
    End Function


    Public Function GetEUS_ProfileMasterByLoginName(ctx As CMSDBDataContext, loginNameOrEmail As String,
                                                    Optional status As ProfileStatusEnum = ProfileStatusEnum.None,
                                                    Optional status2 As ProfileStatusEnum = ProfileStatusEnum.None) As EUS_Profile
        Dim prof As EUS_Profile = Nothing
        'Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Dim rerunTran As Boolean = False
        Try

            For cnt = 0 To 2
                ' perform retry, in case when transaction was deadlocked

                Try
                    If (status > ProfileStatusEnum.None OrElse status2 > ProfileStatusEnum.None) Then
                        prof = (From itm In ctx.EUS_Profiles
                                Where (itm.LoginName.ToUpper() = loginNameOrEmail.ToUpper() OrElse itm.eMail.ToUpper() = loginNameOrEmail.ToUpper()) _
                                AndAlso itm.IsMaster = True _
                                AndAlso (itm.Status = status OrElse itm.Status = status2)
                                Select itm).SingleOrDefault()

                    ElseIf (status > ProfileStatusEnum.None) Then
                        prof = (From itm In ctx.EUS_Profiles
                                Where (itm.LoginName.ToUpper() = loginNameOrEmail.ToUpper() OrElse itm.eMail.ToUpper() = loginNameOrEmail.ToUpper()) _
                                AndAlso itm.IsMaster = True _
                                AndAlso itm.Status = status
                                Select itm).SingleOrDefault()

                    Else
                        prof = (From itm In ctx.EUS_Profiles
                                Where (itm.LoginName.ToUpper() = loginNameOrEmail.ToUpper() OrElse itm.eMail.ToUpper() = loginNameOrEmail.ToUpper()) _
                                AndAlso itm.IsMaster = True _
                                Select itm).SingleOrDefault()

                    End If

                    ' if qeury executed, exit for
                    Exit For

                Catch ex As System.Data.SqlClient.SqlException
                    rerunTran = DataHelpers.CheckSqlException(ex.Message)
                    If (Not rerunTran) Then Throw New Exception(ex.Message, ex)

                    'If (ex.Message.Contains("Rerun the transaction.")) Then
                    '    ' if there was deadlock, retry
                    'Else
                    '    Exit For
                    'End If
                End Try

            Next

        Catch ex As System.InvalidOperationException
            If (ex.Message = "Sequence contains more than one element") Then
                Throw New System.Exception("There should be only one profile with specified login name, but found more. Please check goomena profiles!", ex)
            Else
                Throw New System.InvalidOperationException(ex.Message, ex)
            End If
        Catch ex As Exception
            Throw New System.Exception(ex.Message, ex)
        Finally
            '_CMSDBDataContext.Dispose()
        End Try

        Return prof

    End Function


    Public Function GetEUS_Profile_ForPasswordRestore(ctx As CMSDBDataContext, loginNameOrEmail As String) As EUS_Profile
        Dim prof As EUS_Profile = Nothing
        Dim rerunTran As Boolean = False
        Try

            For cnt = 0 To 2
                ' perform retry, in case when transaction was deadlocked

                Try

                    prof = (From itm In ctx.EUS_Profiles
                        Where (itm.LoginName.ToUpper() = loginNameOrEmail.ToUpper() OrElse itm.eMail.ToUpper() = loginNameOrEmail.ToUpper()) AndAlso
                              (itm.Status = ProfileStatusEnum.Approved OrElse
                                 itm.Status = ProfileStatusEnum.NewProfile OrElse
                                 itm.Status = ProfileStatusEnum.Rejected)
                        Select itm).FirstOrDefault()


                    ' if qeury executed, exit for
                    Exit For

                Catch ex As System.Data.SqlClient.SqlException
                    rerunTran = DataHelpers.CheckSqlException(ex.Message)
                    If (Not rerunTran) Then Throw New Exception(ex.Message, ex)
                End Try

            Next

        Catch ex As Exception
            Throw New System.Exception(ex.Message, ex)
        Finally
        End Try

        Return prof
    End Function


    Public Function GetCurrentMirrorProfile(ctx As CMSDBDataContext, MasterProfileId As Integer, MirrorProfileId As Integer) As vw_EusProfile_Light
        Dim _mineMirrorProfile As vw_EusProfile_Light = Nothing

        If (MasterProfileId = 0 AndAlso MirrorProfileId = 0) Then
            _mineMirrorProfile = New vw_EusProfile_Light()
            Return _mineMirrorProfile
        End If

        Try

            Dim rerunTran As Boolean = False
            Try

                _mineMirrorProfile = ctx.vw_EusProfile_Lights.Where(Function(itm) (itm.ProfileID = MirrorProfileId OrElse itm.ProfileID = MasterProfileId) AndAlso itm.IsMaster = False).SingleOrDefault()

            Catch ex As System.Data.SqlClient.SqlException
                rerunTran = DataHelpers.CheckSqlException(ex.Message)
                If (Not rerunTran) Then Throw
            End Try

            If (rerunTran) Then
                rerunTran = False
                Try

                    _mineMirrorProfile = ctx.vw_EusProfile_Lights.Where(Function(itm) (itm.ProfileID = MirrorProfileId OrElse itm.ProfileID = MasterProfileId) AndAlso itm.IsMaster = False).SingleOrDefault()

                Catch ex As System.Data.SqlClient.SqlException
                    rerunTran = DataHelpers.CheckSqlException(ex.Message)
                    If (Not rerunTran) Then Throw
                End Try
            End If

            If (rerunTran) Then
                rerunTran = False

                _mineMirrorProfile = ctx.vw_EusProfile_Lights.Where(Function(itm) (itm.ProfileID = MirrorProfileId OrElse itm.ProfileID = MasterProfileId) AndAlso itm.IsMaster = False).SingleOrDefault()

            End If

        Catch ex As Exception
            Throw
        Finally
        End Try

        Return _mineMirrorProfile
    End Function



    Public Function GetCurrentMasterProfile(ctx As CMSDBDataContext, MasterProfileId As Integer, MirrorProfileId As Integer) As vw_EusProfile_Light
        Dim _mineMasterProfile As vw_EusProfile_Light = Nothing

        If (MasterProfileId = 0 AndAlso MirrorProfileId = 0) Then
            _mineMasterProfile = New vw_EusProfile_Light()
            Return _mineMasterProfile
        End If

        Try

            Dim rerunTran As Boolean = False
            Try

                _mineMasterProfile = ctx.vw_EusProfile_Lights.Where(Function(itm) (itm.ProfileID = MirrorProfileId OrElse itm.ProfileID = MasterProfileId) AndAlso itm.IsMaster = True).SingleOrDefault()

            Catch ex As System.Data.SqlClient.SqlException
                rerunTran = DataHelpers.CheckSqlException(ex.Message)
                If (Not rerunTran) Then Throw
            End Try

            If (rerunTran) Then
                rerunTran = False
                Try

                    _mineMasterProfile = ctx.vw_EusProfile_Lights.Where(Function(itm) (itm.ProfileID = MirrorProfileId OrElse itm.ProfileID = MasterProfileId) AndAlso itm.IsMaster = True).SingleOrDefault()

                Catch ex As System.Data.SqlClient.SqlException
                    rerunTran = DataHelpers.CheckSqlException(ex.Message)
                    If (Not rerunTran) Then Throw
                End Try
            End If

            If (rerunTran) Then
                rerunTran = False

                _mineMasterProfile = ctx.vw_EusProfile_Lights.Where(Function(itm) (itm.ProfileID = MirrorProfileId OrElse itm.ProfileID = MasterProfileId) AndAlso itm.IsMaster = True).SingleOrDefault()

            End If


        Catch ex As Exception
            Throw
        Finally
        End Try

        Return _mineMasterProfile
    End Function


    Public Function GetCurrentProfile(ctx As CMSDBDataContext, MasterProfileId As Integer, MirrorProfileId As Integer) As vw_EusProfile_Light
        Dim _currentProfile As vw_EusProfile_Light

        _currentProfile = GetCurrentMasterProfile(ctx, MasterProfileId, MirrorProfileId)
        If (_currentProfile Is Nothing) Then
            _currentProfile = GetCurrentMirrorProfile(ctx, MasterProfileId, MirrorProfileId)
        End If

        Return _currentProfile
    End Function


    Public Function GetPhotosCount(ctx As CMSDBDataContext, MasterProfileId As Integer, MirrorProfileId As Integer) As Integer
        Dim _minePhotosCount As Integer

        If (MasterProfileId = 0 AndAlso MirrorProfileId = 0) Then
            Return _minePhotosCount
        End If

        Try

            Dim rerunTran As Boolean = False
            Try


                _minePhotosCount = ctx.EUS_CustomerPhotos.Where(Function(phot) phot.CustomerID = MasterProfileId OrElse phot.CustomerID = MirrorProfileId AndAlso (phot.IsDeleted = 0)).Count()

            Catch ex As System.Data.SqlClient.SqlException
                rerunTran = DataHelpers.CheckSqlException(ex.Message)
                If (Not rerunTran) Then Throw
            End Try

            If (rerunTran) Then
                rerunTran = False
                Try

                    _minePhotosCount = ctx.EUS_CustomerPhotos.Where(Function(phot) phot.CustomerID = MasterProfileId OrElse phot.CustomerID = MirrorProfileId AndAlso (phot.IsDeleted = 0)).Count()

                Catch ex As System.Data.SqlClient.SqlException
                    rerunTran = DataHelpers.CheckSqlException(ex.Message)
                    If (Not rerunTran) Then Throw
                End Try
            End If

            If (rerunTran) Then
                rerunTran = False

                _minePhotosCount = ctx.EUS_CustomerPhotos.Where(Function(phot) phot.CustomerID = MasterProfileId OrElse phot.CustomerID = MirrorProfileId AndAlso (phot.IsDeleted = 0)).Count()

            End If


        Catch ex As Exception
            Throw
        Finally
        End Try

        Return _minePhotosCount
    End Function


    Public Function GetEUS_ProfileByLoginNameOrEmail(ctx As CMSDBDataContext, loginNameOrEmail As String) As EUS_Profile
        Dim prof As EUS_Profile = Nothing
        Try

            prof = (From itm In ctx.EUS_Profiles
                        Where (itm.LoginName.ToUpper() = loginNameOrEmail.ToUpper() OrElse itm.eMail.ToUpper() = loginNameOrEmail.ToUpper()) _
                        Select itm).FirstOrDefault()


        Catch ex As Exception
            Throw
        Finally
        End Try

        Return prof

    End Function

    Public Function GetEUS_ProfileByLoginNameOrEmail(loginNameOrEmail As String) As EUS_Profile
        Using ctx As New CMSDBDataContext(DataHelpers.ConnectionString)


            Dim prof As EUS_Profile = Nothing
            Try

                prof = GetEUS_ProfileByLoginNameOrEmail(ctx, loginNameOrEmail)


            Catch ex As Exception
                Throw
         
            End Try
            Return prof
        End Using

    End Function
    Public Function GetEUS_ProfileByLoginNameOrEmail_OrderByStatus(ctx As CMSDBDataContext, loginNameOrEmail As String, Optional ByVal genderId As Integer = -1) As EUS_Profile
        Dim prof As EUS_Profile = Nothing
        Try

            Dim rerunTran As Boolean = False
            Try

                prof = (From itm In ctx.EUS_Profiles
                               Where (itm.LoginName.ToUpper() = loginNameOrEmail.ToUpper() OrElse itm.eMail.ToUpper() = loginNameOrEmail.ToUpper()) AndAlso
                               itm.IsMaster = True AndAlso itm.GenderId <> genderId
                               Order By itm.Status Ascending
                            Select itm).FirstOrDefault()

            Catch ex As System.Data.SqlClient.SqlException
                rerunTran = DataHelpers.CheckSqlException(ex.Message)
                If (Not rerunTran) Then Throw
            End Try


            If (rerunTran) Then
                rerunTran = False
                Try

                    prof = (From itm In ctx.EUS_Profiles
                                       Where (itm.LoginName.ToUpper() = loginNameOrEmail.ToUpper() OrElse itm.eMail.ToUpper() = loginNameOrEmail.ToUpper()) AndAlso
                                       itm.IsMaster = True AndAlso itm.GenderId <> genderId
                                      Order By itm.Status Ascending
                                    Select itm).FirstOrDefault()

                Catch ex As System.Data.SqlClient.SqlException
                    rerunTran = DataHelpers.CheckSqlException(ex.Message)
                    If (Not rerunTran) Then Throw
                End Try
            End If


            If (rerunTran) Then
                rerunTran = False

                prof = (From itm In ctx.EUS_Profiles
                               Where (itm.LoginName.ToUpper() = loginNameOrEmail.ToUpper() OrElse itm.eMail.ToUpper() = loginNameOrEmail.ToUpper()) AndAlso
                               itm.IsMaster = True AndAlso itm.GenderId <> genderId
                              Order By itm.Status Ascending
                            Select itm).FirstOrDefault()

            End If

        Catch ex As Exception
            Throw
        Finally
        End Try

        Return prof

    End Function

    Public Function GetEUS_ProfileByProfileID(profileID As Integer) As EUS_Profile
        Using ctx As New CMSDBDataContext(DataHelpers.ConnectionString)

            Dim prof As EUS_Profile = Nothing
            Try

                prof = GetEUS_ProfileByProfileID(ctx, profileID)


            Catch ex As Exception
                Throw
           
            End Try
            Return prof
        End Using

    End Function

    Public Function EUS_Profile_CheckEmail(Email As String) As Boolean
        Using ctx As New CMSDBDataContext(DataHelpers.ConnectionString)


            Dim Count As Integer
            Try

                Count = (From itm In ctx.EUS_Profiles
                                    Where (itm.eMail.ToUpper() = Email.ToUpper() AndAlso itm.Status <> ProfileStatusEnum.DeletedByUser)
                                    Select itm).Count()


            Catch ex As Exception
                Throw
          
            End Try
            Return Count > 0
        End Using

    End Function

    Public Function EUS_Profile_CheckLoginName(LoginName As String) As Boolean
        Using ctx As New CMSDBDataContext(DataHelpers.ConnectionString)


            Dim Count As Integer
            Try

                Count = (From itm In ctx.EUS_Profiles
                        Where (itm.LoginName.ToUpper() = LoginName.ToUpper())
                        Select itm).Count()


            Catch ex As Exception
                Throw
         
            End Try
            Return Count > 0
        End Using

    End Function


    Public Function EUS_Profile_CheckLoginName_Register(username1 As String) As String

        Dim rerunTran As Boolean = False
        Dim profile As String = Nothing
        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try


                Try

                    'Dim profile As String = (From itm In Me.CMSDBDataContext.EUS_Profiles
                    '                        Where itm.LoginName.ToUpper() = username1.ToUpper() AndAlso _
                    '                           (itm.Status = ProfileStatusEnum.Updating OrElse
                    '                            itm.Status = ProfileStatusEnum.Approved OrElse
                    '                            itm.Status = ProfileStatusEnum.NewProfile OrElse
                    '                            itm.Status = ProfileStatusEnum.LIMITED)
                    '                        Select itm.LoginName).FirstOrDefault()

                    ' check user name
                    profile = (From itm In _CMSDBDataContext.EUS_Profiles
                                Where itm.LoginName.ToUpper() = username1.ToUpper() AndAlso _
                                    (itm.Status = ProfileStatusEnum.Updating OrElse
                                    itm.Status = ProfileStatusEnum.Approved OrElse
                                    itm.Status = ProfileStatusEnum.NewProfile OrElse
                                    itm.Status = ProfileStatusEnum.LIMITED)
                                Select itm.LoginName).FirstOrDefault()


                Catch ex As System.Data.SqlClient.SqlException
                    rerunTran = DataHelpers.CheckSqlException(ex.Message)
                    If (Not rerunTran) Then Throw
                Catch ex As Exception
                    Throw
                Finally

                End Try


                If (rerunTran) Then
                    Try
                        rerunTran = False
                        profile = (From itm In _CMSDBDataContext.EUS_Profiles
                                    Where itm.LoginName.ToUpper() = username1.ToUpper() AndAlso _
                                        (itm.Status = ProfileStatusEnum.Updating OrElse
                                        itm.Status = ProfileStatusEnum.Approved OrElse
                                        itm.Status = ProfileStatusEnum.NewProfile OrElse
                                        itm.Status = ProfileStatusEnum.LIMITED)
                                    Select itm.LoginName).FirstOrDefault()


                    Catch ex As System.Data.SqlClient.SqlException
                        rerunTran = DataHelpers.CheckSqlException(ex.Message)
                        If (Not rerunTran) Then Throw
                    Catch ex As Exception
                        Throw
                    Finally

                    End Try
                End If


                If (rerunTran) Then
                    Try

                        profile = (From itm In _CMSDBDataContext.EUS_Profiles
                                Where itm.LoginName.ToUpper() = username1.ToUpper() AndAlso _
                                    (itm.Status = ProfileStatusEnum.Updating OrElse
                                    itm.Status = ProfileStatusEnum.Approved OrElse
                                    itm.Status = ProfileStatusEnum.NewProfile OrElse
                                    itm.Status = ProfileStatusEnum.LIMITED)
                                Select itm.LoginName).FirstOrDefault()


                    Catch ex As Exception
                        Throw

                    End Try
                End If
            Catch ex As Exception
                Throw New Exception(ex.Message)
         
            End Try
        End Using
        Return profile
    End Function





    Public Function GetEUS_ProfileByProfileID(ctx As CMSDBDataContext, profileID As Integer) As EUS_Profile
        Dim prof As EUS_Profile = Nothing
        Try
            Dim rerunTran As Boolean = False
            Try

                prof = (From itm In ctx.EUS_Profiles
                            Where (itm.ProfileID = profileID) _
                            Select itm).SingleOrDefault()

            Catch ex As System.Data.SqlClient.SqlException
                rerunTran = DataHelpers.CheckSqlException(ex.Message)
                If (Not rerunTran) Then Throw
            End Try

            If (rerunTran) Then
                rerunTran = False
                Try

                    prof = (From itm In ctx.EUS_Profiles
                                Where (itm.ProfileID = profileID) _
                                Select itm).SingleOrDefault()

                Catch ex As System.Data.SqlClient.SqlException
                    rerunTran = DataHelpers.CheckSqlException(ex.Message)
                    If (Not rerunTran) Then Throw
                End Try
            End If

            If (rerunTran) Then
                rerunTran = False

                prof = (From itm In ctx.EUS_Profiles
                            Where (itm.ProfileID = profileID) _
                            Select itm).SingleOrDefault()

            End If



        Catch ex As Exception
            Throw
        Finally

        End Try

        Return prof
    End Function


    Public Function GetEUS_Profile_GenderId_ByProfileID(ctx As CMSDBDataContext, profileID As String) As Integer
        Dim GenderId1 As Integer
        Dim iProfileID As Integer = 0

        If (Integer.TryParse(profileID, iProfileID) AndAlso iProfileID > 0) Then
            Try

                GenderId1 = (From itm In ctx.EUS_Profiles
                            Where (itm.ProfileID = iProfileID) _
                            Select itm.GenderId).SingleOrDefault()

            Catch ex As Exception
                Throw
            End Try
        End If

        Return GenderId1
    End Function


    Public Function GetEUS_Profile_LoginName_ByProfileID(ctx As CMSDBDataContext, profileID As Integer) As String
        Dim LoginName1 As String = Nothing
        Dim iProfileID As Integer = 0

        If (Integer.TryParse(profileID, iProfileID) AndAlso iProfileID > 0) Then
            Try

                LoginName1 = (From itm In ctx.EUS_Profiles
                            Where (itm.ProfileID = iProfileID) _
                            Select itm.LoginName).SingleOrDefault()

            Catch ex As Exception
                Throw
            End Try
        End If

        Return LoginName1
    End Function



    Public Function GetEUS_Profile_LoginName_ByProfileID(profileID As Integer) As String
        Using ctx As New CMSDBDataContext(DataHelpers.ConnectionString)


            Dim LogName As String
            Try

                LogName = DataHelpers.GetEUS_Profile_LoginName_ByProfileID(ctx, profileID)

            Catch ex As Exception
                Throw
          
            End Try

            Return LogName
        End Using
    End Function


    Public Function EUS_Profile_GetIsReferrer(ctx As CMSDBDataContext, profileID As String) As Boolean
        Dim prof As Integer = 0
        Try

            prof = (From itm In ctx.EUS_Profiles
                        Where (itm.ProfileID = profileID AndAlso itm.ReferrerParentId > 0) _
                        Select itm).Count()


        Catch ex As Exception
            Throw
        Finally

        End Try

        Return prof > 0
    End Function



    Public Function SYS_ProfilesAccess_GetByCookie(Cookie As String) As DSWebStatistics.SYS_ProfilesAccess_GetByCookieDataTable
        Try
            Using ta As New DSWebStatisticsTableAdapters.SYS_ProfilesAccess_GetByCookieTableAdapter
                Using ds As New DSWebStatistics
                    Using con As New SqlConnection(DataHelpers.ConnectionString)
                        ta.Connection = con
                        ta.Fill(ds.SYS_ProfilesAccess_GetByCookie, Cookie)
                        Return ds.SYS_ProfilesAccess_GetByCookie
                    End Using
                End Using
            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function


    Public Sub UpdateEUS_Profiles_LoginData(profileid As Integer, REMOTE_ADDR As String, GEO_COUNTRY_CODE As String, IsOnline As Boolean, IsMobile As Boolean)

        Try
            Dim sql As String = <sql><![CDATA[
update EUS_Profiles set
    LastLoginDateTime=@LastLoginDateTime, 
    LastLoginIP=ISNULL(@LastLoginIP,LastLoginIP), 
    LastLoginGEOInfos=ISNULL(@LastLoginGEOInfos,LastLoginGEOInfos), 
    IsOnline=@IsOnline,
    LastLoginMobileDateTime=ISNULL(@LastLoginMobileDateTime,LastLoginMobileDateTime)
where 
    ProfileID=@ProfileID or 
    MirrorProfileID=@ProfileID
]]></sql>
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using command = GetSqlCommand(con, sql)


                    command.Parameters.AddWithValue("@ProfileID", profileid)
                    command.Parameters.AddWithValue("@LastLoginDateTime", DateTime.UtcNow)
                    command.Parameters.AddWithValue("@LastLoginIP", REMOTE_ADDR)
                    command.Parameters.AddWithValue("@IsOnline", IsOnline)

                    If (String.IsNullOrEmpty(GEO_COUNTRY_CODE)) Then
                        command.Parameters.AddWithValue("@LastLoginGEOInfos", DBNull.Value)
                    Else
                        command.Parameters.AddWithValue("@LastLoginGEOInfos", GEO_COUNTRY_CODE)
                    End If
                    If (IsMobile) Then
                        command.Parameters.AddWithValue("@LastLoginMobileDateTime", DateTime.UtcNow)
                    Else
                        command.Parameters.AddWithValue("@LastLoginMobileDateTime", DBNull.Value)
                    End If

                    DataHelpers.ExecuteNonQuery(command)
                End Using
            End Using
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Public Sub UpdateEUS_Profiles_LoginData(profileid As Integer, IsOnline As Boolean, isMobile As Boolean)

        Try
            Dim sql As String = ""

            sql = <sql><![CDATA[
update EUS_Profiles set
    IsOnline=@IsOnline,
    LastLoginMobileDateTime=ISNULL(@LastLoginMobileDateTime,LastLoginMobileDateTime)
where 
    ProfileID=@ProfileID or 
    MirrorProfileID=@ProfileID
]]></sql>
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using command = GetSqlCommand(con, sql)


                    command.Parameters.AddWithValue("@IsOnline", IsOnline)
                    command.Parameters.AddWithValue("@ProfileID", profileid)
                    command.Parameters.AddWithValue("@LastLoginMobileDateTime", DateTime.UtcNow)
                    DataHelpers.ExecuteNonQuery(command)
                End Using
            End Using
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Public Sub UpdateEUS_Profiles_GoogleUserData(profileid As Integer, goUid As String, goName As String, goEmail As String)
        Try
            Dim sql As String = "update EUS_Profiles set GoogleUserId=@GoogleUserId, GoogleName=@GoogleName, GoogleEmail=@GoogleEmail where ProfileID=@profileid Or MirrorProfileID=@profileid"
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd = GetSqlCommand(con, sql)


                    If (goUid IsNot Nothing) Then
                        cmd.Parameters.AddWithValue("@GoogleUserId", goUid)
                    Else
                        cmd.Parameters.AddWithValue("@GoogleUserId", System.DBNull.Value)
                    End If
                    If (goName IsNot Nothing) Then
                        cmd.Parameters.AddWithValue("@GoogleName", goName)
                    Else
                        cmd.Parameters.AddWithValue("@GoogleName", System.DBNull.Value)
                    End If
                    If (goEmail IsNot Nothing) Then
                        cmd.Parameters.AddWithValue("@GoogleEmail", goEmail)
                    Else
                        cmd.Parameters.AddWithValue("@GoogleEmail", System.DBNull.Value)
                    End If

                    cmd.Parameters.AddWithValue("@profileid", profileid)
                    DataHelpers.ExecuteNonQuery(cmd)
                End Using
            End Using
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Public Sub UpdateEUS_Profiles_FacebookData(profileid As Integer, fbUid As String, fbName As String, fbUserName As String)

        Try
            Dim sql As String = "update EUS_Profiles set FacebookUserId=@FacebookUserId, FacebookName=@FacebookName, FacebookUserName=@FacebookUserName where ProfileID=@profileid Or MirrorProfileID=@profileid"

            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd = GetSqlCommand(con, sql)


                    If (fbUid IsNot Nothing) Then
                        cmd.Parameters.AddWithValue("@FacebookUserId", fbUid)
                    Else
                        cmd.Parameters.AddWithValue("@FacebookUserId", System.DBNull.Value)
                    End If
                    If (fbName IsNot Nothing) Then
                        cmd.Parameters.AddWithValue("@FacebookName", fbName)
                    Else
                        cmd.Parameters.AddWithValue("@FacebookName", System.DBNull.Value)
                    End If
                    If (fbUserName IsNot Nothing) Then
                        cmd.Parameters.AddWithValue("@FacebookUserName", fbUserName)
                    Else
                        cmd.Parameters.AddWithValue("@FacebookUserName", System.DBNull.Value)
                    End If

                    cmd.Parameters.AddWithValue("@profileid", profileid)
                    DataHelpers.ExecuteNonQuery(cmd)
                End Using
            End Using
        Catch ex As Exception
            Throw
        End Try

    End Sub

    Public Sub UpdateEUS_Profiles_FacebookPhoto(profileid As Integer, FacebookPhoto As Boolean)
        Try
            Dim sb As String = "update EUS_Profiles set FacebookPhoto=@FacebookPhoto where ProfileID=@profileid Or MirrorProfileID=@profileid"
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd = GetSqlCommand(con, sb.ToString())
                    cmd.Parameters.AddWithValue("@FacebookPhoto", FacebookPhoto)
                    cmd.Parameters.AddWithValue("@profileid", profileid)
                    DataHelpers.ExecuteNonQuery(cmd)
                End Using
            End Using
        Catch ex As Exception
            Throw
        End Try
    End Sub



    Public Function UpdateEUS_Profiles_LogoutData(profileid As Integer) As Integer
        Dim ROWCOUNT As Integer = 0

        Try
            Dim sql As String = "update EUS_Profiles set IsOnline=0 where ProfileID=@profileid Or MirrorProfileID=@profileid"
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@profileid", profileid)
                    ROWCOUNT = DataHelpers.ExecuteNonQuery(cmd)
                    Return ROWCOUNT
                End Using
            End Using
        Catch ex As Exception
            Throw

        End Try

    End Function

    Public Function UpdateEUS_Profiles_MarkDeleted(profileid As Integer) As Integer
        Dim ROWCOUNT As Integer
        Try
            Dim sql As String = "update EUS_Profiles set Status=@Status,IsOnline=0,IsAutoApproved=0 where ProfileID=@profileid Or MirrorProfileID=@profileid"
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@Status", CInt(ProfileStatusEnum.Deleted))
                    cmd.Parameters.AddWithValue("@profileid", profileid)
                    ROWCOUNT = DataHelpers.ExecuteNonQuery(cmd)
                    Return ROWCOUNT
                End Using
            End Using
        Catch ex As Exception
            Throw
        End Try
    End Function


    Public Function UpdateEUS_Profiles_DeletePermanently(profileid As Integer) As Integer
        Dim ROWCOUNT As Integer
        Try
            Dim sql As String = "delete from EUS_Profiles where ProfileID=@profileid Or MirrorProfileID=@profileid"
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@profileid", profileid)
                    ROWCOUNT = DataHelpers.ExecuteNonQuery(cmd)
                    Return ROWCOUNT
                End Using
            End Using
        Catch ex As Exception
            Throw
        End Try


    End Function


    Public Function UpdateEUS_Profiles_AreYouWillingToTravel(profileid As Integer, willing As Boolean) As Integer
        Dim ROWCOUNT As Integer

        Try
            Dim sql As String = ""
            If (willing) Then
                sql = "update EUS_Profiles set AreYouWillingToTravel=1 where ProfileID=@profileid Or MirrorProfileID=@profileid;"
            Else
                sql = "update EUS_Profiles set AreYouWillingToTravel=0 where ProfileID=@profileid Or MirrorProfileID=@profileid;"
            End If
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection

                Using cmd = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@profileid", profileid)
                    ROWCOUNT = DataHelpers.ExecuteNonQuery(cmd)
                    DataHelpers.ExecuteNonQuery(cmd)
                    Return ROWCOUNT
                End Using

            End Using
        Catch ex As Exception
            Throw
        End Try

    End Function


    Public Sub Update_EUS_Profiles_UpdateAvailableCredits(profileid As Integer, AvailableCredits As Integer, Optional ReadAvailableCreditsFromDB As Boolean = False)

        Try
            Dim sql As String = "EXEC EUS_Profiles_UpdateAvailableCredits @customerid=@customerid, @AvailableCredits=@AvailableCredits, @ReadAvailableCredits=@ReadAvailableCredits"
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@customerid", profileid)
                    cmd.Parameters.AddWithValue("@AvailableCredits", AvailableCredits)
                    cmd.Parameters.AddWithValue("@ReadAvailableCredits", ReadAvailableCreditsFromDB)
                    DataHelpers.ExecuteNonQuery(cmd)
                End Using
            End Using
        Catch ex As Exception
            Throw
        End Try
    End Sub


    Public Function EUS_Profiles_CheckPassword(profileid As Integer, password As String) As Boolean
        Dim result As Long = 0
        Try
            Dim sql As String = "SELECT COUNT(*) as Rows FROM EUS_Profiles WHERE Password=@Password AND (ProfileID=@ProfileID OR MirrorProfileID=@ProfileID)"
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@ProfileID", profileid)
                    cmd.Parameters.AddWithValue("@Password", password)
                    result = DataHelpers.ExecuteScalar(cmd)
                    Return (result > 0)
                End Using
            End Using
        Catch ex As Exception
            Throw
        End Try
    End Function


    Public Sub UpdateEUS_Profiles_AffiliateParentId(profileid As Integer, AffiliateParentId As Integer)

        Try

            Dim sql As String = "update EUS_Profiles set AffiliateParentId=@AffiliateParentId where (ProfileID=@ProfileID OR MirrorProfileID=@ProfileID)"
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@AffiliateParentId", AffiliateParentId)
                    cmd.Parameters.AddWithValue("@ProfileID", profileid)
                    DataHelpers.ExecuteNonQuery(cmd)
                End Using
            End Using
        Catch ex As Exception
            Throw
        End Try
    End Sub


    Public Function GetEUS_Profiles_ReferrerParentId(profileid As Integer) As Integer
        Dim _ReferrerParentId As Integer?

        Try

            Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
                _ReferrerParentId = (From itm In _CMSDBDataContext.EUS_Profiles
                                   Where itm.ProfileID = profileid
                                   Select itm.ReferrerParentId).SingleOrDefault()
            End Using

        Catch ex As Exception
            Throw
        End Try
        If (Not _ReferrerParentId.HasValue) Then _ReferrerParentId = 0
        Return _ReferrerParentId
    End Function


    Public Sub UpdateEUS_Profiles_ReferrerParentId(profileid As Integer, ReferrerParentId As Integer)
        Try

            Dim sql As String = "update EUS_Profiles set ReferrerParentId=@ReferrerParentId where ProfileID=@ProfileID Or MirrorProfileID=@ProfileID"
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@ReferrerParentId", ReferrerParentId)
                    cmd.Parameters.AddWithValue("@ProfileID", profileid)
                    DataHelpers.ExecuteNonQuery(cmd)
                End Using
            End Using
        Catch ex As Exception
            Throw
        End Try
    End Sub


    Public Sub UpdateEUS_Profiles_LAGID(profileid As Integer, lagId As String)
        Try
            Dim sql As String = "update EUS_Profiles set LAGID=@lagId where ProfileID=@ProfileID Or MirrorProfileID=@ProfileID"
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@lagId", lagId)
                    cmd.Parameters.AddWithValue("@ProfileID", profileid)
                    DataHelpers.ExecuteNonQuery(cmd)
                End Using
            End Using
        Catch ex As Exception
            Throw
        End Try
    End Sub


    Public Function UpdateEUS_Profiles_REF_CRD2EURO_Rate(profileid As Integer, REF_CRD2EURO_Rate As Double) As Integer
        Dim ROWCOUNT As Integer
        Try
            Dim sql As String = "update EUS_Profiles set REF_CRD2EURO_Rate=@REF_CRD2EURO_Rate where ProfileID=@profileid Or MirrorProfileID=@profileid"

            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@profileid", profileid)
                    cmd.Parameters.Add(New SqlParameter("REF_CRD2EURO_Rate", REF_CRD2EURO_Rate))
                    ROWCOUNT = DataHelpers.ExecuteNonQuery(cmd)
                    Return ROWCOUNT
                End Using
            End Using
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function UpdateEUS_Profiles_ShowOnFirstPage(profileid As Integer, show As Boolean)
        Dim ROWCOUNT As Integer

        Try

            Dim sql As String = "update EUS_Profiles set ShowOnFrontPage=@ShowOnFrontPage where ProfileID=@profileid Or MirrorProfileID=@profileid"
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@profileid", profileid)
                    cmd.Parameters.Add(New SqlParameter("ShowOnFrontPage", show))
                    ROWCOUNT = DataHelpers.ExecuteNonQuery(cmd)
                    Return ROWCOUNT
                End Using
            End Using

        Catch ex As Exception
            Throw
        End Try

      
    End Function


    Public Sub UpdateEUS_Profiles_Activity(profileid As Integer, LastActivityIP As String, isOnline As Boolean?, Optional TimeUntil As DateTime? = Nothing)
        UpdateEUS_Profiles_Activity(profileid, LastActivityIP, isOnline, False, TimeUntil)
    End Sub


    Public Sub UpdateEUS_Profiles_Activity(profileid As Integer, LastActivityIP As String, isOnline As Boolean?, isMobile As Boolean, Optional TimeUntil As DateTime? = Nothing)
    
        Try
            'Dim sb As New System.Text.StringBuilder()
            'update EUS_Profiles set LastActivityDateTime='" & Date.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff") & "' where ProfileID=" & profileid & " or MirrorProfileID=" & profileid & "")
            'Dim sql As String = "exec [EUS_Profiles_LastActivityDateTime] @CustomerId=@CustomerId"
            'Dim sql As String = "update EUS_Profiles set LastActivityDateTime=@LastActivityDateTime, IsOnline=isnull(@IsOnline,IsOnline) where ProfileID=@CustomerId or MirrorProfileID=@CustomerId"
            Dim credits As Integer = If(isMobile, clsConfigValues.GetSYS_ConfigValueDouble2("MobileFirstUseBonus"), 0)
            Dim HasMobileActivity As Boolean = True
            Dim genderid As Integer = 2
            Dim Referrer As String = ""
            Dim eMail As String = ""
            Dim customref As String = ""
            Dim Country As String = ""
            Dim UserName As String = ""
            Dim LagId As String = ""
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection
                If credits > 0 Then

                    Dim sql2 = <sql><![CDATA[
select [LoginName],[LAGID],HasMobilyActivity=cast(isNull((select top(1) (case when [LastActivityMobileDateTime] is null then 0 else 1 end)),0) as bit),[Referrer]
      ,[CustomReferrer] ,
	 [eMail], GenderId, Country
 from [EUS_Profiles] 
where [ProfileID]=@ProfileID or [MirrorProfileID]=@ProfileID
]]></sql>



                    Using cmd2 = DataHelpers.GetSqlCommand(con, sql2)
                        cmd2.Parameters.AddWithValue("@ProfileID", profileid)
                        Dim result As DataTable = DataHelpers.GetDataTable(cmd2)
                        If (result.Rows.Count > 0) Then
                            eMail = If(result.Rows(0)("eMail") Is DBNull.Value, "", result.Rows(0)("eMail"))

                            HasMobileActivity = result.Rows(0)("HasMobilyActivity")
                            genderid = result.Rows(0)("GenderId")
                            Referrer = If(result.Rows(0)("Referrer") Is DBNull.Value, "", result.Rows(0)("Referrer"))
                            customref = If(result.Rows(0)("CustomReferrer") Is DBNull.Value, "", result.Rows(0)("CustomReferrer"))
                            Country = If(result.Rows(0)("Country") Is DBNull.Value, "", result.Rows(0)("Country"))
                            UserName = If(result.Rows(0)("LoginName") Is DBNull.Value, "member", result.Rows(0)("LoginName"))

                            LagId = If(result.Rows(0)("LAGID") Is DBNull.Value, "US", result.Rows(0)("LAGID"))
                        End If
                    End Using


                End If



                Dim sql As String = ""
                If (isMobile) Then
                    sql = <sql><![CDATA[
update EUS_Profiles set 
    LastActivityDateTime=@LastActivityDateTime, 
    IsOnline=isnull(@IsOnline,IsOnline), 
    LastActivityIP=isnull(@LastActivityIP,LastActivityIP), 
    LastLoginMobileDateTime=isnull(LastLoginMobileDateTime,LastActivityMobileDateTime),
    LastActivityMobileDateTime=isnull(@LastActivityMobileDateTime,LastActivityMobileDateTime) 
where ProfileID=@CustomerId or MirrorProfileID=@CustomerId
]]></sql>
                Else

                    sql = <sql><![CDATA[
update EUS_Profiles set 
    LastActivityDateTime=@LastActivityDateTime, 
    IsOnline=isnull(@IsOnline,IsOnline) , 
    LastActivityIP=isnull(@LastActivityIP,LastActivityIP) 
where ProfileID=@CustomerId or MirrorProfileID=@CustomerId
]]></sql>
                End If
                Using cmd = DataHelpers.GetSqlCommand(con, sql)


                    cmd.Parameters.Add(New SqlParameter("@CustomerId", profileid))

                    If (Not TimeUntil.HasValue) Then
                        cmd.Parameters.Add(New SqlParameter("@LastActivityDateTime", Date.UtcNow))
                    Else
                        cmd.Parameters.Add(New SqlParameter("@LastActivityDateTime", TimeUntil))
                    End If
                    If (isOnline.HasValue) Then
                        cmd.Parameters.Add(New SqlParameter("@IsOnline", isOnline))
                    Else
                        cmd.Parameters.Add(New SqlParameter("@IsOnline", System.DBNull.Value))
                    End If
                    If (Not String.IsNullOrEmpty(LastActivityIP)) Then
                        cmd.Parameters.Add(New SqlParameter("@LastActivityIP", LastActivityIP))
                    Else
                        cmd.Parameters.Add(New SqlParameter("@LastActivityIP", System.DBNull.Value))
                    End If
                    If (isMobile) Then
                        cmd.Parameters.Add(New SqlParameter("@LastActivityMobileDateTime", Date.UtcNow))
                    Else
                        cmd.Parameters.Add(New SqlParameter("@LastActivityMobileDateTime", System.DBNull.Value))
                    End If
                    DataHelpers.ExecuteNonQuery(cmd)
                End Using
                Dim HasCookie As Boolean = True
                Try
                    Using ta As New DSWebStatisticsTableAdapters.SYS_ProfilesAccess_GetByCookieTableAdapter
                        Using ds As New DSWebStatistics
                            '  Using con As New SqlConnection(DataHelpers.ConnectionString)
                            ta.Connection = con
                            ta.FillBy_CustomerID(ds.SYS_ProfilesAccess_GetByCookie, profileid, False)
                            If ds.SYS_ProfilesAccess_GetByCookie.Rows.Count = 0 Then
                                HasCookie = False
                            End If
                            '  End Using

                        End Using
                    End Using
        Catch ex As Exception
        End Try

                If HasMobileActivity = False AndAlso HasCookie = False AndAlso genderid = 1 AndAlso credits > 0 AndAlso customref <> "googleplay" AndAlso Country = "GR" Then
                    If clsProfilesPrivacySettings.get_HasSendedMobileBonus(profileid) = False Then
                        clsProfilesPrivacySettings.Update_HasSendedMobileBonus(profileid, True)
                        If (credits > 0) Then
                            ' TransactionTypeID: 4 = Bonus CustomerTransactionTypeID
                            Try
                                clsCustomer.AddTransaction("Add free Mobile bonus credits",
                                                                              0,
                                                                               "Free Mobile credits " & credits,
                                                                               "",
                                                                               credits,
                                                                              LastActivityIP,
                                                                               "Google App",
                                                                              Referrer,
                                                                               profileid,
                                                                              customref,
                                                                               eMail,
                                                                               PaymentMethods.None,
                                                                               "Mobile_GIFT",
                                                                               4,
                                                                               Nothing,
                                                                               "EUR",
                                                                               New clsDataRecordIPN(),
                                                                               0,
                                                                               False,
                                                                               45,
                                                                               0)
                            Catch ex As Exception
                                clsMyMail.TrySendMailSupport(ConfigurationManager.AppSettings("ExceptionsEmail"), "1010---- Exception adding free MobileCredits", ex.ToString())
                            Finally
                                SendEmail(profileid, UserName, eMail, LagId, credits)
                            End Try

                            '  DataHelpers.EUS_Profiles_Update_StartingFreeCredits(profileid)
                        End If

                    End If


                End If
            End Using


        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Function SendEmail(ByVal ProfileId As Integer, ByVal UserName As String, ByVal UserEmail As String, ByVal LagId As String, ByVal Credits As Integer) As Boolean
        Dim re As Boolean = False
        Try
            Dim subject As String = ""
            Dim body As String = ""

            Dim lagid2 As String = "US"
            If (Not String.IsNullOrEmpty(LagId)) Then lagid2 = LagId
            Dim o As New clsSiteLAG()
            subject = o.GetCustomStringFromPage(lagid2, "GlobalStrings", "Mobile_APP_RegistrationSubject", ConnectionString)
            body = o.GetCustomStringFromPage(lagid2, "GlobalStrings", "Mobile_APP_RegistrationText", ConnectionString)
            body = body.Replace("[###memberName###]", UserName)
            body = body.Replace("[###Credits###]", Credits)
            Try
                clsUserDoes.SendMessageFromAdministrator(subject, body, ProfileId)

            Catch ex As Exception
                clsMyMail.TrySendMailSupport(ConfigurationManager.AppSettings("ExceptionsEmail"), "1012---- Exception error on Sending Send TO administrator ", ex.ToString())
                re = False
            End Try

            clsMyMail.SendMailFromQueue(UserEmail, subject, body, ProfileId, "NotificationType.MobileRegistrationBonus", True, Nothing, Nothing)

            re = True

        Catch ex As Exception
            clsMyMail.TrySendMailSupport(ConfigurationManager.AppSettings("ExceptionsEmail"), "1011---- Exception error on Sending Send Mail From Queue ", ex.ToString())
            re = False
        End Try

        Return re
    End Function

    Public Function GetEUS_Profiles_REF_CRD2EURO_Rate(profileid As Integer) As Double
        Dim _REF_CRD2EURO_Rate As Double?
        Try

      
        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
                Dim check As Boolean = True
                Dim pr1 As EUS_Profile = Nothing
                Try
                    pr1 = (From itm In _CMSDBDataContext.EUS_Profiles
                                                     Where itm.ProfileID = profileid
                                                     Select itm).SingleOrDefault()
                Catch ex As Exception
                    Throw ex
                End Try
                If check AndAlso pr1 IsNot Nothing Then


                    Try
                        _REF_CRD2EURO_Rate = pr1.REF_CRD2EURO_Rate
                    Catch ex As Exception
                        Throw ex
                    End Try
                    If (Not _REF_CRD2EURO_Rate.HasValue) Then
                        Try
                            Dim pr As Integer? = pr1.MirrorProfileID
                            Dim lasttra As Decimal? = (From itm In _CMSDBDataContext.EUS_CustomerTransactions
                                                      Where (itm.DebitAmount > 0 AndAlso itm.TransactionType <> 4) AndAlso (itm.CustomerID = profileid OrElse itm.CustomerID = pr)
                                                     Order By itm.Date_Time Descending
                                                     Select itm.DebitAmount
                                                         ).FirstOrDefault()
                            If lasttra.HasValue Then
                                If lasttra.Value <= 3 Then
                                    _REF_CRD2EURO_Rate = lasttra / 150
                                ElseIf lasttra.Value <= 15 Then
                                    _REF_CRD2EURO_Rate = lasttra / 1200
                                ElseIf lasttra.Value <= 36 Then
                                    _REF_CRD2EURO_Rate = lasttra / 3200
                                ElseIf lasttra.Value <= 66 Then
                                    _REF_CRD2EURO_Rate = lasttra / 6200
                                Else
                                    _REF_CRD2EURO_Rate = lasttra / 10400
                                End If
                                If _REF_CRD2EURO_Rate.HasValue AndAlso _REF_CRD2EURO_Rate > 0 Then UpdateEUS_Profiles_REF_CRD2EURO_Rate(profileid, _REF_CRD2EURO_Rate)
                            Else
                                _REF_CRD2EURO_Rate = 0
                            End If
                        Catch ex As Exception
                        End Try
                    End If
                    If (Not _REF_CRD2EURO_Rate.HasValue) Then
                        _REF_CRD2EURO_Rate = clsConfigValues.Get__referrer_commission_conv_rate()
                    End If
                End If
            End Using
        Catch ex As Exception
            Throw ex
        End Try
        Return _REF_CRD2EURO_Rate.Value
    End Function


    Public Function GetEUS_Profiles_Status(profileid As Integer) As Integer
        Dim Status As Integer

        Try
            Dim Sql As String = "select top(1) Status from EUS_Profiles where ProfileID=@profileid Or MirrorProfileID=@profileid"
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd = DataHelpers.GetSqlCommand(con, Sql)
                    cmd.Parameters.AddWithValue("@profileid", profileid)
                    Dim o = DataHelpers.ExecuteScalar(cmd)
                    If (o IsNot Nothing AndAlso Not o Is System.DBNull.Value) Then Status = o
                End Using
            End Using
        Catch ex As Exception
            Throw


        End Try

        Return Status
        
    End Function



    Public Function UpdateEUS_Profiles_OnUpdateAutoNotificationSent(profileid As Integer, value As Boolean) As Integer
        Dim ROWCOUNT As Integer

        Try
            Dim sql As String = "update EUS_Profiles set OnUpdateAutoNotificationSent=@value where ProfileID=@profileid Or MirrorProfileID=@profileid"
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@profileid", profileid)
                    cmd.Parameters.AddWithValue("@value", value)
                    ROWCOUNT = DataHelpers.ExecuteNonQuery(cmd)
                End Using
            End Using
        Catch ex As Exception
            Throw
        Finally

        End Try
        Return ROWCOUNT
    
    End Function


    Public Function GetEUS_Profiles_OnUpdateAutoNotificationSent(profileid As Integer) As Boolean
        Dim value As Boolean

        Try
            Dim sql As String = "select top(1) ISNULL(OnUpdateAutoNotificationSent,0) as OnUpdateAutoNotificationSent from EUS_Profiles where ProfileID=@profileid Or MirrorProfileID=@profileid"
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@profileid", profileid)
                    value = DataHelpers.ExecuteScalar(cmd)
                End Using
            End Using

        Catch ex As Exception
            Throw
        Finally

        End Try
        Return value
       
    End Function



    Public Function UpdateEUS_Profiles_OnUpdateAutoNotificationSentForPhoto(profileid As Integer, value As Boolean) As Integer
        Dim ROWCOUNT As Integer

        Try

            Dim sql As String = "update EUS_Profiles set OnUpdateAutoNotificationSentForPhoto='" & value & "' where ProfileID=@profileid Or MirrorProfileID=@profileid"
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@profileid", profileid)
                    ROWCOUNT = DataHelpers.ExecuteNonQuery(cmd)
                End Using

            End Using
        Catch ex As Exception
            Throw
        Finally

        End Try
        Return ROWCOUNT

    End Function

    Public Function UpdateEUS_Profiles_OnApprove_SetLastUpdateProfileDateTime(profileid As Integer) As Integer
        Dim ROWCOUNT As Integer

        Try
            Dim sql As String = <sql><![CDATA[
-- set status
update EUS_Profiles set 
    LastUpdateProfileDateTime=(select MAX(LastUpdateProfileDateTime) from EUS_Profiles where (ProfileID=@ProfileID or MirrorProfileID=@ProfileID))
where 
    (ProfileID=@ProfileID or MirrorProfileID=@ProfileID)
]]></sql>
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd = GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@ProfileID", profileid)

                    ROWCOUNT = DataHelpers.ExecuteNonQuery(cmd)
                End Using
            End Using
        Catch ex As Exception
            Throw
        Finally

        End Try
        Return ROWCOUNT
    End Function

    Public Function UpdateEUS_Profiles_MirrorSetStatusUpdating(profileid As Integer, Status As ProfileStatusEnum, ProfileModified As ProfileModifiedEnum) As Integer
        Return UpdateEUS_Profiles_MirrorSetStatusUpdating(profileid, Status, ProfileModified, False)
    End Function

    Public Function UpdateEUS_Profiles_MirrorSetStatusUpdating(profileid As Integer, Status As ProfileStatusEnum, ProfileModified As ProfileModifiedEnum, IsMobileUpdate As Boolean) As Integer
        Dim ROWCOUNT As Integer

        Try
            Dim sql As String = <sql><![CDATA[

-- set status
update EUS_Profiles set 
    Status=@Status,
    LastUpdateProfileDateTime=@LastUpdateProfileDateTime
where 
    (ProfileID=@ProfileID or MirrorProfileID=@ProfileID)
    and IsMaster=0 
    and Status=@Approved



-- set UpdatingDocs or UpdatingPhotos

if(@@ROWCOUNT > 0 and @Status=2)
begin

    update EUS_Profiles set 
        UpdatingDocs=isnull(UpdatingDocs, @UpdatingDocs)
        ,UpdatingPhotos=isnull(UpdatingPhotos, @UpdatingPhotos)
        ,IsMobileUpdate=isnull(IsMobileUpdate, @IsMobileUpdate)
    where 
        (ProfileID=@ProfileID or MirrorProfileID=@ProfileID)

end


-- set remove  IsAutoApproved flag

if(@Status=2 or @Status=16 or @Status=18)
begin

    update EUS_Profiles 
        set IsAutoApproved=0
    where 
        (ProfileID=@ProfileID or MirrorProfileID=@ProfileID)

end


]]></sql>
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd = GetSqlCommand(con, sql)


                    cmd.Parameters.AddWithValue("@Status", Status)
                    cmd.Parameters.AddWithValue("@ProfileID", profileid)
                    cmd.Parameters.AddWithValue("@Approved", CInt(ProfileStatusEnum.Approved))

                    If (ProfileModified = ProfileModifiedEnum.UpdatingDocs) Then
                        cmd.Parameters.AddWithValue("@UpdatingDocs", True)
                    Else
                        cmd.Parameters.AddWithValue("@UpdatingDocs", System.DBNull.Value)
                    End If

                    If (ProfileModified = ProfileModifiedEnum.UpdatingPhotos) Then
                        cmd.Parameters.AddWithValue("@UpdatingPhotos", True)
                    Else
                        cmd.Parameters.AddWithValue("@UpdatingPhotos", System.DBNull.Value)
                    End If
                    cmd.Parameters.AddWithValue("@IsMobileUpdate", IsMobileUpdate)
                    cmd.Parameters.AddWithValue("@LastUpdateProfileDateTime", DateTime.UtcNow)

                    ROWCOUNT = DataHelpers.ExecuteNonQuery(cmd)
                End Using
            End Using
        Catch ex As Exception
            Throw


        End Try
        Return ROWCOUNT
    End Function

    Public Function GetEUS_Profiles_OnUpdateAutoNotificationSentForPhoto(profileid As Integer) As Boolean
        Dim value As Boolean

        Try
            Dim sql As String = "select top(1) ISNULL(OnUpdateAutoNotificationSentForPhoto,0) as OnUpdateAutoNotificationSentForPhoto from EUS_Profiles where ProfileID=@profileid Or MirrorProfileID=@profileid"
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@profileid", profileid)
                    value = DataHelpers.ExecuteScalar(cmd)
                End Using
            End Using

        Catch ex As Exception
            Throw


        End Try
        Return value
       
    End Function

#End Region

#Region "EUS_CustomerPhotos"

    Public Function GetEUS_CustomerPhotos() As DSMembers
        Try
            Using ta As New DSMembersTableAdapters.EUS_CustomerPhotosTableAdapter
                Using ds As New DSMembers()
                    Using con As New SqlConnection(DataHelpers.ConnectionString)
                        ta.Connection = con
                        ta.Fill(ds.EUS_CustomerPhotos)
                        Return ds
                    End Using
                End Using
            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function



    Public Function GetEUS_CustomerPhotos_ByProfileOrMirrorID(customerId As Integer) As DSMembers
        Dim rerunTran As Boolean = False
        Try
            Using con As New SqlConnection(DataHelpers.ConnectionString)
                Using ta As New DSMembersTableAdapters.EUS_CustomerPhotosTableAdapter
                    Using ds As New DSMembers()
                        ta.Connection = con
                        Try

                            ta.FillByProfileIDOrMirrorID(ds.EUS_CustomerPhotos, customerId)

                        Catch ex As System.Data.SqlClient.SqlException
                            rerunTran = DataHelpers.CheckSqlException(ex.Message)
                            If (Not rerunTran) Then Throw
                        End Try

                        If (rerunTran) Then
                            rerunTran = False
                            Try

                                ta.FillByProfileIDOrMirrorID(ds.EUS_CustomerPhotos, customerId)

                            Catch ex As System.Data.SqlClient.SqlException
                                rerunTran = DataHelpers.CheckSqlException(ex.Message)
                                If (Not rerunTran) Then Throw
                            End Try
                        End If

                        If (rerunTran) Then
                            rerunTran = False

                            ta.FillByProfileIDOrMirrorID(ds.EUS_CustomerPhotos, customerId)

                        End If
                        Return ds
                    End Using
                End Using
            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function GetEUS_CustomerPhotosByCustomerPhotosID(photoId As Integer) As DSMembers
        Try
            Using ta As New DSMembersTableAdapters.EUS_CustomerPhotosTableAdapter
                Using ds As New DSMembers()
                    Using con As New SqlConnection(DataHelpers.ConnectionString)
                        ta.Connection = con
                        ta.FillByCustomerPhotosID(ds.EUS_CustomerPhotos, photoId)
                        Return ds
                    End Using
                End Using
            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function


    Public Function GetEUS_CustomerVerificationDocsByID(CustomerVerificationDocsID As Integer) As DSCustomer
        Try
            Using ta As New DSCustomerTableAdapters.EUS_CustomerVerificationDocsTableAdapter
                Using ds As New DSCustomer
                    Using con As New SqlConnection(DataHelpers.ConnectionString)
                        ta.Connection = con
                        ta.FillBy_CustomerVerificationDocsID(ds.EUS_CustomerVerificationDocs, CustomerVerificationDocsID)

                        Return ds
                    End Using
                End Using
            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function


    Public Sub UpdateEUS_CustomerPhotos(ByRef ds As DSMembers)
        Try
            Using ta As New DSMembersTableAdapters.EUS_CustomerPhotosTableAdapter
                Using con As New SqlConnection(DataHelpers.ConnectionString)
                    ta.Connection = con
                    ta.Update(ds.EUS_CustomerPhotos)
                End Using
            End Using

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub

    Public Sub UpdateEUS_CustomerVerificationDocs(ByRef ds As DSCustomer)
        Try
            Using ta As New DSCustomerTableAdapters.EUS_CustomerVerificationDocsTableAdapter
                Using con As New SqlConnection(DataHelpers.ConnectionString)
                    ta.Connection = con
                    ta.Update(ds.EUS_CustomerVerificationDocs)
                End Using
            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub


    Public Function GetProfilesDefaultPhoto(profileId As Integer) As DSMembers.EUS_CustomerPhotosRow
        Try
            Using ta As New DSMembersTableAdapters.EUS_CustomerPhotosTableAdapter
                Using ds As New DSMembers
                    Using con As New SqlConnection(DataHelpers.ConnectionString)
                        ta.Connection = con
                        ta.FillBy_CustomerID_IsDefault(ds.EUS_CustomerPhotos, profileId, True)
                        If (ds.EUS_CustomerPhotos.Rows.Count > 0) Then
                            Return ds.EUS_CustomerPhotos.Rows(0)
                        Else
                            Return Nothing
                        End If
                    End Using
                End Using
            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function


    Public Function GetEUS_CustomerPhotosRow(photoId As Long) As DSMembers.EUS_CustomerPhotosRow
        Try
            Using ta As New DSMembersTableAdapters.EUS_CustomerPhotosTableAdapter
                Using ds As New DSMembers
                    Using con As New SqlConnection(DataHelpers.ConnectionString)
                        ta.Connection = con
                        ta.FillByCustomerPhotosID(ds.EUS_CustomerPhotos, photoId)
                        If (ds.EUS_CustomerPhotos.Rows.Count > 0) Then
                            Return ds.EUS_CustomerPhotos.Rows(0)
                        Else
                            Return Nothing
                        End If
                    End Using
                End Using
            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function



#End Region



    Public Function GetEUS_LISTS__OLD() As DSLists
        Using ds As New DSLists()


            Try

                Using ta As New DSListsTableAdapters.TableAdapterManager
                    Using connection As New SqlConnection(DataHelpers.ConnectionString)

                        ta.EUS_LISTS_BodyTypeTableAdapter = New DSListsTableAdapters.EUS_LISTS_BodyTypeTableAdapter()
                        ta.EUS_LISTS_BodyTypeTableAdapter.Connection = connection
                        ta.EUS_LISTS_BodyTypeTableAdapter.Fill(ds.EUS_LISTS_BodyType)

                        ta.EUS_LISTS_ChildrenNumberTableAdapter = New DSListsTableAdapters.EUS_LISTS_ChildrenNumberTableAdapter()
                        ta.EUS_LISTS_ChildrenNumberTableAdapter.Connection = connection
                        ta.EUS_LISTS_ChildrenNumberTableAdapter.Fill(ds.EUS_LISTS_ChildrenNumber)

                        ta.EUS_LISTS_DrinkingTableAdapter = New DSListsTableAdapters.EUS_LISTS_DrinkingTableAdapter()
                        ta.EUS_LISTS_DrinkingTableAdapter.Connection = connection
                        ta.EUS_LISTS_DrinkingTableAdapter.Fill(ds.EUS_LISTS_Drinking)

                        ta.EUS_LISTS_EducationTableAdapter = New DSListsTableAdapters.EUS_LISTS_EducationTableAdapter()
                        ta.EUS_LISTS_EducationTableAdapter.Connection = connection
                        ta.EUS_LISTS_EducationTableAdapter.Fill(ds.EUS_LISTS_Education)

                        ta.EUS_LISTS_EthnicityTableAdapter = New DSListsTableAdapters.EUS_LISTS_EthnicityTableAdapter()
                        ta.EUS_LISTS_EthnicityTableAdapter.Connection = connection
                        ta.EUS_LISTS_EthnicityTableAdapter.Fill(ds.EUS_LISTS_Ethnicity)

                        ta.EUS_LISTS_EyeColorTableAdapter = New DSListsTableAdapters.EUS_LISTS_EyeColorTableAdapter()
                        ta.EUS_LISTS_EyeColorTableAdapter.Connection = connection
                        ta.EUS_LISTS_EyeColorTableAdapter.Fill(ds.EUS_LISTS_EyeColor)

                        ta.EUS_LISTS_HairColorTableAdapter = New DSListsTableAdapters.EUS_LISTS_HairColorTableAdapter()
                        ta.EUS_LISTS_HairColorTableAdapter.Connection = connection
                        ta.EUS_LISTS_HairColorTableAdapter.Fill(ds.EUS_LISTS_HairColor)

                        ta.EUS_LISTS_HeightTableAdapter = New DSListsTableAdapters.EUS_LISTS_HeightTableAdapter()
                        ta.EUS_LISTS_HeightTableAdapter.Connection = connection
                        ta.EUS_LISTS_HeightTableAdapter.Fill(ds.EUS_LISTS_Height)

                        ta.EUS_LISTS_IncomeTableAdapter = New DSListsTableAdapters.EUS_LISTS_IncomeTableAdapter()
                        ta.EUS_LISTS_IncomeTableAdapter.Connection = connection
                        ta.EUS_LISTS_IncomeTableAdapter.Fill(ds.EUS_LISTS_Income)

                        ta.EUS_LISTS_NetWorthTableAdapter = New DSListsTableAdapters.EUS_LISTS_NetWorthTableAdapter()
                        ta.EUS_LISTS_NetWorthTableAdapter.Connection = connection
                        ta.EUS_LISTS_NetWorthTableAdapter.Fill(ds.EUS_LISTS_NetWorth)

                        ta.EUS_LISTS_RelationshipStatusTableAdapter = New DSListsTableAdapters.EUS_LISTS_RelationshipStatusTableAdapter()
                        ta.EUS_LISTS_RelationshipStatusTableAdapter.Connection = connection
                        ta.EUS_LISTS_RelationshipStatusTableAdapter.Fill(ds.EUS_LISTS_RelationshipStatus)

                        ta.EUS_LISTS_ReligionTableAdapter = New DSListsTableAdapters.EUS_LISTS_ReligionTableAdapter()
                        ta.EUS_LISTS_ReligionTableAdapter.Connection = connection
                        ta.EUS_LISTS_ReligionTableAdapter.Fill(ds.EUS_LISTS_Religion)

                        ta.EUS_LISTS_SmokingTableAdapter = New DSListsTableAdapters.EUS_LISTS_SmokingTableAdapter()
                        ta.EUS_LISTS_SmokingTableAdapter.Connection = connection
                        ta.EUS_LISTS_SmokingTableAdapter.Fill(ds.EUS_LISTS_Smoking)

                        ta.EUS_LISTS_RejectingReasonsTableAdapter = New DSListsTableAdapters.EUS_LISTS_RejectingReasonsTableAdapter()
                        ta.EUS_LISTS_RejectingReasonsTableAdapter.Connection = connection
                        ta.EUS_LISTS_RejectingReasonsTableAdapter.Fill(ds.EUS_LISTS_RejectingReasons)

                        ta.SYS_CountriesGEOTableAdapter = New DSListsTableAdapters.SYS_CountriesGEOTableAdapter()
                        ta.SYS_CountriesGEOTableAdapter.Connection = connection
                        ta.SYS_CountriesGEOTableAdapter.Fill(ds.SYS_CountriesGEO)

                        ta.EUS_LISTS_AccountTypeTableAdapter = New DSListsTableAdapters.EUS_LISTS_AccountTypeTableAdapter()
                        ta.EUS_LISTS_AccountTypeTableAdapter.Connection = connection
                        ta.EUS_LISTS_AccountTypeTableAdapter.Fill(ds.EUS_LISTS_AccountType)

                        ta.EUS_LISTS_GenderTableAdapter = New DSListsTableAdapters.EUS_LISTS_GenderTableAdapter()
                        ta.EUS_LISTS_GenderTableAdapter.Connection = connection
                        ta.EUS_LISTS_GenderTableAdapter.Fill(ds.EUS_LISTS_Gender)

                        ta.EUS_LISTS_PhotosDisplayLevelTableAdapter = New DSListsTableAdapters.EUS_LISTS_PhotosDisplayLevelTableAdapter()
                        ta.EUS_LISTS_PhotosDisplayLevelTableAdapter.Connection = connection
                        ta.EUS_LISTS_PhotosDisplayLevelTableAdapter.Fill(ds.EUS_LISTS_PhotosDisplayLevel)

                        ta.EUS_LISTS_TypeOfDatingTableAdapter = New DSListsTableAdapters.EUS_LISTS_TypeOfDatingTableAdapter()
                        ta.EUS_LISTS_TypeOfDatingTableAdapter.Connection = connection
                        ta.EUS_LISTS_TypeOfDatingTableAdapter.Fill(ds.EUS_LISTS_TypeOfDating)

                        ta.EUS_OffersStatusTableAdapter = New DSListsTableAdapters.EUS_OffersStatusTableAdapter()
                        ta.EUS_OffersStatusTableAdapter.Connection = connection
                        ta.EUS_OffersStatusTableAdapter.Fill(ds.EUS_OffersStatus)

                        ta.EUS_OffersTypesTableAdapter = New DSListsTableAdapters.EUS_OffersTypesTableAdapter()
                        ta.EUS_OffersTypesTableAdapter.Connection = connection
                        ta.EUS_OffersTypesTableAdapter.Fill(ds.EUS_OffersTypes)
                    End Using
                End Using

            Catch ex As Exception
                Throw New Exception(ex.Message)
           
            End Try
            Return ds
        End Using

    End Function


    Public Sub GetEUS_LISTS(ByRef ds As DSLists)

        Using dsFill As DSLists = DataHelpers.GetEUS_LISTS()


            ds.EUS_LISTS_BodyType.Merge(dsFill.EUS_LISTS_BodyType)
            ds.EUS_LISTS_ChildrenNumber.Merge(dsFill.EUS_LISTS_ChildrenNumber)
            ds.EUS_LISTS_Drinking.Merge(dsFill.EUS_LISTS_Drinking)
            ds.EUS_LISTS_Education.Merge(dsFill.EUS_LISTS_Education)
            ds.EUS_LISTS_Ethnicity.Merge(dsFill.EUS_LISTS_Ethnicity)
            ds.EUS_LISTS_EyeColor.Merge(dsFill.EUS_LISTS_EyeColor)
            ds.EUS_LISTS_HairColor.Merge(dsFill.EUS_LISTS_HairColor)
            ds.EUS_LISTS_Height.Merge(dsFill.EUS_LISTS_Height)
            ds.EUS_LISTS_Income.Merge(dsFill.EUS_LISTS_Income)
            ds.EUS_LISTS_NetWorth.Merge(dsFill.EUS_LISTS_NetWorth)
            ds.EUS_LISTS_RelationshipStatus.Merge(dsFill.EUS_LISTS_RelationshipStatus)
            ds.EUS_LISTS_Religion.Merge(dsFill.EUS_LISTS_Religion)
            ds.EUS_LISTS_Smoking.Merge(dsFill.EUS_LISTS_Smoking)
            ds.EUS_LISTS_RejectingReasons.Merge(dsFill.EUS_LISTS_RejectingReasons)
            ds.SYS_CountriesGEO.Merge(dsFill.SYS_CountriesGEO)
            ds.EUS_LISTS_AccountType.Merge(dsFill.EUS_LISTS_AccountType)
            ds.EUS_LISTS_Gender.Merge(dsFill.EUS_LISTS_Gender)
            ds.EUS_LISTS_PhotosDisplayLevel.Merge(dsFill.EUS_LISTS_PhotosDisplayLevel)
            ds.EUS_LISTS_TypeOfDating.Merge(dsFill.EUS_LISTS_TypeOfDating)
            ds.EUS_OffersStatus.Merge(dsFill.EUS_OffersStatus)
            ds.EUS_OffersTypes.Merge(dsFill.EUS_OffersTypes)
            ds.EUS_LISTS_Job.Merge(dsFill.EUS_LISTS_Job)
            ds.EUS_LISTS_BreastSize.Merge(dsFill.EUS_LISTS_BreastSize)
            ds.EUS_LISTS_SpokenLanguages.Merge(dsFill.EUS_LISTS_SpokenLanguages)
            ds.EUS_LISTS_Languages.Merge(dsFill.EUS_LISTS_Languages)
        End Using
    End Sub


    Public Function GetEUS_LISTS() As DSLists
    
        Dim sql As String = <sql><![CDATA[
select * from EUS_LISTS_BodyType; --0
select * from EUS_LISTS_ChildrenNumber; --1
select * from EUS_LISTS_Drinking; --2
select * from EUS_LISTS_Education; --3
select * from EUS_LISTS_Ethnicity; --4
select * from EUS_LISTS_EyeColor; --5 
select * from EUS_LISTS_HairColor; --6
select * from EUS_LISTS_Height; --7
select * from EUS_LISTS_Income; --8
select * from EUS_LISTS_NetWorth; --9
select * from EUS_LISTS_RelationshipStatus; --10
select * from EUS_LISTS_Religion; --11
select * from EUS_LISTS_Smoking; --12
select * from EUS_LISTS_RejectingReasons; --13
select * from SYS_CountriesGEO; --14
select * from EUS_LISTS_AccountType; --15
select * from EUS_LISTS_Gender; --16
select * from EUS_LISTS_PhotosDisplayLevel; --17
select * from EUS_LISTS_TypeOfDating; --18
select * from EUS_OffersStatus; --19
select * from EUS_OffersTypes; --20
select * from EUS_LISTS_Job; --21
select * from EUS_LISTS_BreastSize order by SortOrder asc; --22
select * from EUS_LISTS_SpokenLanguages; --23
select * from EUS_LISTS_Languages; --24
]]></sql>.Value

        Try

       

        'Dim ta As New DSListsTableAdapters.TableAdapterManager
        'Dim connection As New SqlConnection(DataHelpers.ConnectionString)
          
            Using dsFill As DataSet = DataHelpers.GetDataSet(sql)
                Using ds As New DSLists '=Nothing 
                    ds.EUS_LISTS_BodyType.Merge(dsFill.Tables(0))
                    ds.EUS_LISTS_ChildrenNumber.Merge(dsFill.Tables(1))
                    ds.EUS_LISTS_Drinking.Merge(dsFill.Tables(2))
                    ds.EUS_LISTS_Education.Merge(dsFill.Tables(3))
                    ds.EUS_LISTS_Ethnicity.Merge(dsFill.Tables(4))
                    ds.EUS_LISTS_EyeColor.Merge(dsFill.Tables(5))
                    ds.EUS_LISTS_HairColor.Merge(dsFill.Tables(6))
                    ds.EUS_LISTS_Height.Merge(dsFill.Tables(7))
                    ds.EUS_LISTS_Income.Merge(dsFill.Tables(8))
                    ds.EUS_LISTS_NetWorth.Merge(dsFill.Tables(9))
                    ds.EUS_LISTS_RelationshipStatus.Merge(dsFill.Tables(10))
                    ds.EUS_LISTS_Religion.Merge(dsFill.Tables(11))
                    ds.EUS_LISTS_Smoking.Merge(dsFill.Tables(12))
                    ds.EUS_LISTS_RejectingReasons.Merge(dsFill.Tables(13))
                    ds.SYS_CountriesGEO.Merge(dsFill.Tables(14))
                    ds.EUS_LISTS_AccountType.Merge(dsFill.Tables(15))
                    ds.EUS_LISTS_Gender.Merge(dsFill.Tables(16))
                    ds.EUS_LISTS_PhotosDisplayLevel.Merge(dsFill.Tables(17))
                    ds.EUS_LISTS_TypeOfDating.Merge(dsFill.Tables(18))
                    ds.EUS_OffersStatus.Merge(dsFill.Tables(19))
                    ds.EUS_OffersTypes.Merge(dsFill.Tables(20))
                    ds.EUS_LISTS_Job.Merge(dsFill.Tables(21))
                    ds.EUS_LISTS_BreastSize.Merge(dsFill.Tables(22))
                    ds.EUS_LISTS_SpokenLanguages.Merge(dsFill.Tables(23))
                    ds.EUS_LISTS_Languages.Merge(dsFill.Tables(24))
                    Return ds
                End Using
            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function



    Public Function GetEUS_LISTS_PhotosDisplayLevel() As DSLists
        Try
            Using ta As New DSListsTableAdapters.TableAdapterManager
                Using ds As New DSLists
                    Using connection As New SqlConnection(DataHelpers.ConnectionString)
                        ta.EUS_LISTS_PhotosDisplayLevelTableAdapter = New DSListsTableAdapters.EUS_LISTS_PhotosDisplayLevelTableAdapter()
                        ta.EUS_LISTS_PhotosDisplayLevelTableAdapter.Connection = connection
                        ta.EUS_LISTS_PhotosDisplayLevelTableAdapter.Fill(ds.EUS_LISTS_PhotosDisplayLevel)
                        Return ds
                    End Using
                End Using
            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function


    Public Function GetEUS_LISTS_ReportingReason() As DSLists
        Try
            Using ta As New DSListsTableAdapters.EUS_LISTS_ReportingReasonTableAdapter
                Using ds As New DSLists()
                    Using con As New SqlConnection(DataHelpers.ConnectionString)
                        ta.Connection = con
                        ta.Fill(ds.EUS_LISTS_ReportingReason)
                        Return ds
                    End Using
                End Using
            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function GetApprovedUsersDatatable(topRecords As Integer) As DataTable
        Dim sql As String = <sql><![CDATA[
SELECT TOP({0}) 
    [ProfileID], 
    [LoginName], 
    [Genderid],
    (Select top(1) [FileName] from EUS_CustomerPhotos where [CustomerID]=[ProfileID] and IsDefault=1 and HasAproved=1 and phot.IsDeleted=0) as ImageFileName
FROM [dbo].[EUS_Profiles]
WHERE [Status]= {1} and [IsMaster]=1 and 
Genderid = {2}
UNION
SELECT TOP({0}) 
    [ProfileID], 
    [LoginName], 
    [Genderid],
    (Select top(1) [FileName] from EUS_CustomerPhotos where [CustomerID]=[ProfileID] and IsDefault=1 and HasAproved=1 and phot.IsDeleted=0) as ImageFileName
FROM [dbo].[EUS_Profiles]
WHERE [Status]={1} and [IsMaster]=1 and  
Genderid = {3}
]]></sql>.Value


        sql = String.Format(sql, topRecords.ToString(), CType(ProfileStatusEnum.Approved, Integer).ToString(), ProfileHelper.gMaleGender.GenderId.ToString(), ProfileHelper.gFemaleGender.GenderId.ToString())
        Try
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using command As SqlCommand = GetSqlCommand(con, sql)
                    Using adapter As New SqlDataAdapter(command)
                        Using dataTbl = New DataTable()
                            adapter.Fill(dataTbl)
                            Return dataTbl
                        End Using
                    End Using
                End Using
            End Using
        Catch ex As Exception
            Throw
        End Try
    End Function


    Public Function GetSqlCommand(ByRef con As SqlConnection, Optional ByVal sqlStatement As String = "") As SqlClient.SqlCommand
        If (sqlStatement Is Nothing) Then sqlStatement = ""

        Return New SqlClient.SqlCommand(sqlStatement, con)
    End Function


    Public Function GetSqlConnection(Optional ByVal newConnectionString As String = "") As SqlClient.SqlConnection
        If String.IsNullOrEmpty(newConnectionString) Then newConnectionString = ConnectionString
        
        Return New SqlClient.SqlConnection(newConnectionString)
    End Function


    Public Sub EUS_Profiles_LoadDataTable(ByRef ds As DSMembers, sql As String)
        Using con As New SqlClient.SqlConnection(DataHelpers.ConnectionString)


            Using cmd As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                Try
                    Using rsRead = DataHelpers.GetDataReader(cmd)
                        ds.EUS_Profiles.Load(rsRead, LoadOption.OverwriteChanges)
                    End Using

                Catch ex As Exception
                    Throw
                Finally
                End Try
            End Using
        End Using
    End Sub




    Public Function GetDataReader(ByRef command As SqlClient.SqlCommand) As SqlDataReader

        Dim sdr As SqlDataReader
        Try
            command.Connection.Open()
            sdr = command.ExecuteReader()
        Catch ex As Exception
            Throw
        End Try

        Return sdr
    End Function

    Public Function CheckSqlException(exMessage As String) As Boolean
        Dim rerunTran As Boolean = False
        If (exMessage.Contains("Rerun the transaction.")) Then
            System.Threading.Thread.Sleep(600)
            rerunTran = True
        End If

        If (exMessage.Contains("Cannot open database") OrElse exMessage.Contains("A connection was successfully")) Then
            'System.Data.SqlClient.SqlException (0x80131904): A connection was successfully established with the server, but then an error occurred during the pre-login handshake. 
            'System.Data.SqlClient.SqlException (0x80131904): Cannot open database "CMSGoomena" requested by the login. The login failed.

            ' wait 15 seconds, while server is restarting
            System.Threading.Thread.Sleep(WAIT_DB_RESTART_MILLISEC)
            rerunTran = True
        End If
        Return rerunTran
    End Function


    Public Function GetDataSet(ByRef command As SqlClient.SqlCommand) As DataSet



        Try
            Dim rerunTran As Boolean = False
            Using ds As New DataSet
                Using adapter As New SqlDataAdapter(command)
                    Try
                        adapter.Fill(ds)
                    Catch ex As System.Data.SqlClient.SqlException
                        rerunTran = DataHelpers.CheckSqlException(ex.Message)
                        If (Not rerunTran) Then Throw
                    End Try
                    If (rerunTran) Then
                        rerunTran = False
                        Try
                            adapter.Fill(ds)
                        Catch ex As System.Data.SqlClient.SqlException
                            rerunTran = DataHelpers.CheckSqlException(ex.Message)
                            If (Not rerunTran) Then Throw
                        End Try
                    End If
                    If (rerunTran) Then
                        rerunTran = False
                        adapter.Fill(ds)
                    End If
                    Return ds
                End Using
            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        End Try
    End Function

    Public Function GetDataSet(sqlStatement As String) As DataSet

       
      

   
     

        Try
            Using connection As SqlClient.SqlConnection = New SqlClient.SqlConnection(DataHelpers.ConnectionString)
                Using command As SqlClient.SqlCommand = New SqlClient.SqlCommand(sqlStatement, connection)
                    Using adapter As New SqlDataAdapter(command)
                        Using ds As New DataSet()
                            Dim rerunTran As Boolean = False
                            Try
                                adapter.Fill(ds)
                            Catch ex As System.Data.SqlClient.SqlException
                                rerunTran = DataHelpers.CheckSqlException(ex.Message)
                                If (Not rerunTran) Then Throw
                            End Try
                            If (rerunTran) Then
                                rerunTran = False
                                Try
                                    adapter.Fill(ds)
                                Catch ex As System.Data.SqlClient.SqlException
                                    rerunTran = DataHelpers.CheckSqlException(ex.Message)
                                    If (Not rerunTran) Then Throw
                                End Try
                            End If
                            If (rerunTran) Then
                                rerunTran = False
                                adapter.Fill(ds)
                            End If
                            Return ds
                        End Using
                    End Using
                End Using
            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        End Try


    End Function


    Public Function GetDataTable(sqlStatement As String) As DataTable
        Try
            Using connection As SqlClient.SqlConnection = New SqlClient.SqlConnection(DataHelpers.ConnectionString)
                Using command As SqlClient.SqlCommand = New SqlClient.SqlCommand(sqlStatement, connection)
                    Using adapter As New SqlDataAdapter(command)
                        Using dt As New DataTable
                            Dim rerunTran As Boolean = False
                            Try
                                adapter.Fill(dt)
                            Catch ex As System.Data.SqlClient.SqlException
                                rerunTran = DataHelpers.CheckSqlException(ex.Message)
                                If (Not rerunTran) Then Throw
                            End Try

                            If (rerunTran) Then
                                rerunTran = False
                                adapter.Fill(dt)
                            End If
                            Return dt
                        End Using
                    End Using
                End Using
            End Using
        Catch ex As Exception
            Throw
        End Try
    End Function

    ''' <summary>
    ''' Return DataTable filled by sql command. Connection string is NOT modified.
    ''' </summary>
    ''' <param name="command"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetDataTable(command As SqlClient.SqlCommand) As DataTable
        Try
            Using adapter As New SqlDataAdapter(command)
                Using dt As New DataTable()
                    Dim rerunTran As Boolean = False
                    Try

                        adapter.Fill(dt)
                    Catch ex As System.Data.SqlClient.SqlException
                        rerunTran = DataHelpers.CheckSqlException(ex.Message)
                        If (Not rerunTran) Then Throw
                    End Try

                    If (rerunTran) Then
                        rerunTran = False
                        Try
                            adapter.Fill(dt)
                        Catch ex As System.Data.SqlClient.SqlException
                            rerunTran = DataHelpers.CheckSqlException(ex.Message)
                            If (Not rerunTran) Then Throw
                        End Try
                    End If
                    If (rerunTran) Then
                        rerunTran = False
                        adapter.Fill(dt)
                    End If
                    Return dt
                End Using
            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        End Try
    End Function


    Public Sub FillDataTable(sqlStatement As String, ByRef table As DataTable)
        Try
            Using connection As SqlClient.SqlConnection = New SqlClient.SqlConnection(DataHelpers.ConnectionString)
                Using command As SqlClient.SqlCommand = New SqlClient.SqlCommand(sqlStatement, connection)
                    Using adapter As New SqlDataAdapter(command)
                        Dim rerunTran As Boolean = False
                        Try
                            adapter.Fill(table)
                        Catch ex As System.Data.SqlClient.SqlException
                            rerunTran = DataHelpers.CheckSqlException(ex.Message)
                            If (Not rerunTran) Then Throw
                        End Try

                        If (rerunTran) Then
                            rerunTran = False
                            Try
                                adapter.Fill(table)
                            Catch ex As System.Data.SqlClient.SqlException
                                rerunTran = DataHelpers.CheckSqlException(ex.Message)
                                If (Not rerunTran) Then Throw
                            End Try
                        End If

                        If (rerunTran) Then
                            rerunTran = False
                            adapter.Fill(table)
                        End If
                    End Using
                End Using
            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        End Try

    End Sub

    Public Sub FillDataTable(command As SqlClient.SqlCommand, ByRef table As DataTable)
       
        Try
            Using adapter As New SqlDataAdapter(command)
                Dim rerunTran As Boolean = False
                Try
                    adapter.Fill(table)
                Catch ex As System.Data.SqlClient.SqlException
                    rerunTran = DataHelpers.CheckSqlException(ex.Message)
                    If (Not rerunTran) Then Throw
                End Try

                If (rerunTran) Then
                    rerunTran = False
                    Try
                        adapter.Fill(table)
                    Catch ex As System.Data.SqlClient.SqlException
                        rerunTran = DataHelpers.CheckSqlException(ex.Message)
                        If (Not rerunTran) Then Throw
                    End Try
                End If

                If (rerunTran) Then
                    rerunTran = False
                    adapter.Fill(table)
                End If
            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        End Try

    End Sub



    Public Function ExecuteNonQuery(command As SqlClient.SqlCommand) As Integer
        Dim result As Integer = -1
        Dim rerunTran As Boolean = False
        Try
            Using con As New SqlClient.SqlConnection(DataHelpers.ConnectionString)
                con.Open()
                command.Connection = con
                result = command.ExecuteNonQuery()
                con.Close()
            End Using


        Catch ex As System.Data.SqlClient.SqlException
            rerunTran = DataHelpers.CheckSqlException(ex.Message)
            If (Not rerunTran) Then Throw
        Catch ex As Exception
            Throw
        End Try
        If (rerunTran) Then
            Try
                Using con As New SqlClient.SqlConnection(DataHelpers.ConnectionString)
                    command.Connection = con
                    command.Connection.Open()
                    result = command.ExecuteNonQuery()
                End Using
            Catch ex As Exception
                Throw
            End Try
        End If
        Return result
    End Function

    Public Function ExecuteNonQuery(command As SqlClient.SqlCommand, newConnectionString As String) As Integer
        Dim result As Integer = -1
        Dim rerunTran As Boolean = False



        Try
            Using con As New SqlClient.SqlConnection(newConnectionString)
                command.Connection = con
                command.Connection.Open()
                result = command.ExecuteNonQuery()
            End Using
           

        Catch ex As System.Data.SqlClient.SqlException
            rerunTran = DataHelpers.CheckSqlException(ex.Message)
            If (Not rerunTran) Then Throw
        Catch ex As Exception
            Throw
        End Try


        If (rerunTran) Then
            Try
                Using con As New SqlClient.SqlConnection(newConnectionString)
                    command.Connection = con
                    command.Connection.Open()
                    result = command.ExecuteNonQuery()
                End Using
            Catch ex As Exception
                Throw
            End Try
        End If

        Return result
    End Function

    Public Function ExecuteNonQuery(sqlStatement As String) As Integer


       
       
        Dim result As Integer = -1

        Try
            Using connection As SqlClient.SqlConnection = New SqlClient.SqlConnection(DataHelpers.ConnectionString)
                Using command As SqlClient.SqlCommand = New SqlClient.SqlCommand(sqlStatement, connection)
                    command.Connection.Open()
                    Dim rerunTran As Boolean = False
                    Try
                        result = command.ExecuteNonQuery()
                    Catch ex As System.Data.SqlClient.SqlException
                        rerunTran = DataHelpers.CheckSqlException(ex.Message)
                        If (Not rerunTran) Then Throw
                    End Try

                    If (rerunTran) Then
                        rerunTran = False
                        Try
                            result = command.ExecuteNonQuery()
                        Catch ex As System.Data.SqlClient.SqlException
                            rerunTran = DataHelpers.CheckSqlException(ex.Message)
                            If (Not rerunTran) Then Throw
                        End Try
                    End If

                    If (rerunTran) Then
                        rerunTran = False
                        result = command.ExecuteNonQuery()
                    End If
                End Using
            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        End Try
        Return result
    End Function

    Public Function ExecuteNonQuery(sqlStatement As String, newConnectionString As String) As Integer
       
      
        Dim result As Integer = -1
        Try
            Using connection As SqlClient.SqlConnection = New SqlClient.SqlConnection(newConnectionString)
                Using command As SqlClient.SqlCommand = New SqlClient.SqlCommand(sqlStatement, connection)
                    command.Connection.Open()
                    Dim rerunTran As Boolean = False
                    Try
                        result = command.ExecuteNonQuery()
                    Catch ex As System.Data.SqlClient.SqlException
                        rerunTran = DataHelpers.CheckSqlException(ex.Message)
                        If (Not rerunTran) Then Throw
                    End Try

                    If (rerunTran) Then
                        rerunTran = False
                        Try
                            result = command.ExecuteNonQuery()
                        Catch ex As System.Data.SqlClient.SqlException
                            rerunTran = DataHelpers.CheckSqlException(ex.Message)
                            If (Not rerunTran) Then Throw
                        End Try
                    End If

                    If (rerunTran) Then
                        rerunTran = False
                        result = command.ExecuteNonQuery()
                    End If
                End Using
            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        End Try
        Return result
    End Function



    Public Function ExecuteScalar(command As SqlClient.SqlCommand) As Object
        Dim result As Long = -1
        Dim rerunTran As Boolean = False
        ' command.Connection = New SqlClient.SqlConnection(DataHelpers.ConnectionString)

        Try
            Using con As New SqlClient.SqlConnection(DataHelpers.ConnectionString)
                command.Connection = con
                command.Connection.Open()
                result = command.ExecuteScalar()
            End Using
         

        Catch ex As System.Data.SqlClient.SqlException
            rerunTran = DataHelpers.CheckSqlException(ex.Message)
            If (Not rerunTran) Then Throw
        Catch ex As Exception
            Throw
        End Try
        If (rerunTran) Then
            Try
                Using con As New SqlClient.SqlConnection(DataHelpers.ConnectionString)
                    command.Connection = con
                    command.Connection.Open()
                    result = command.ExecuteScalar()
                End Using
            Catch ex As Exception
                Throw
            End Try
        End If
        Return result
    End Function

    Public Function ExecuteScalar(sqlStatement As String) As Object

       
       
        Dim result As Integer = -1

        Try
          
            Using connection As SqlClient.SqlConnection = New SqlClient.SqlConnection(DataHelpers.ConnectionString)
                Using command As SqlClient.SqlCommand = New SqlClient.SqlCommand(sqlStatement, connection)
                    command.Connection.Open()

                    Dim rerunTran As Boolean = False
                    Try
                        result = command.ExecuteScalar()
                    Catch ex As System.Data.SqlClient.SqlException
                        rerunTran = DataHelpers.CheckSqlException(ex.Message)
                        If (Not rerunTran) Then Throw
                    End Try

                    If (rerunTran) Then
                        rerunTran = False
                        Try
                            result = command.ExecuteScalar()
                        Catch ex As System.Data.SqlClient.SqlException
                            rerunTran = DataHelpers.CheckSqlException(ex.Message)
                            If (Not rerunTran) Then Throw
                        End Try
                    End If

                    If (rerunTran) Then
                        rerunTran = False
                        result = command.ExecuteScalar()
                    End If
                End Using
            End Using
        Catch ex As Exception
            Throw
        End Try
        Return result
    End Function

    Public Function ExecuteScalar(sqlStatement As String, newConnectionString As String) As Object

      
    
        Dim result As Integer = -1

        Try
            Using connection As SqlClient.SqlConnection = New SqlClient.SqlConnection(newConnectionString)
                Using command As SqlClient.SqlCommand = New SqlClient.SqlCommand(sqlStatement, connection)
                    command.Connection.Open()

                    Dim rerunTran As Boolean = False
                    Try
                        result = command.ExecuteScalar()
                    Catch ex As System.Data.SqlClient.SqlException
                        rerunTran = DataHelpers.CheckSqlException(ex.Message)
                        If (Not rerunTran) Then Throw
                    End Try

                    If (rerunTran) Then
                        rerunTran = False
                        Try
                            result = command.ExecuteScalar()
                        Catch ex As System.Data.SqlClient.SqlException
                            rerunTran = DataHelpers.CheckSqlException(ex.Message)
                            If (Not rerunTran) Then Throw
                        End Try
                    End If

                    If (rerunTran) Then
                        rerunTran = False
                        result = command.ExecuteScalar()
                    End If
                End Using
            End Using

        Catch ex As Exception
            Throw
        End Try
        Return result
    End Function


    Public Function ToDataTable(ctx As System.Data.Linq.DataContext, query As Object) As DataTable
        If query Is Nothing Then
            Throw New ArgumentNullException("query")
        End If
        Try
            Using cmd As IDbCommand = ctx.GetCommand(TryCast(query, IQueryable))
                Using adapter As New SqlClient.SqlDataAdapter()
                    adapter.SelectCommand = DirectCast(cmd, SqlClient.SqlCommand)
                    Using dt As New DataTable("Generated")
                        cmd.Connection.Open()
                        adapter.FillSchema(dt, SchemaType.Source)
                        adapter.Fill(dt)
                        Return dt
                    End Using
                End Using
            End Using
        Finally
        End Try
    End Function



    Public Function GetExecutionStatement(cmd As SqlCommand)

        Dim quotedParameterTypes As DbType() = New DbType() {DbType.AnsiString, DbType.Date, DbType.DateTime, DbType.Guid, DbType.String, DbType.AnsiStringFixedLength, _
 DbType.StringFixedLength}
        Dim query As String = cmd.CommandText

        Dim arrParams = New SqlParameter(cmd.Parameters.Count - 1) {}
        cmd.Parameters.CopyTo(arrParams, 0)

        For Each prm As SqlParameter In arrParams.OrderByDescending(Function(p) p.ParameterName.Length)
            Dim value As String = prm.Value.ToString()

            If (prm.DbType = DbType.DateTime) Then
                value = "'" & DirectCast(prm.Value, DateTime).ToString("yyyy-MM-dd hh:mm:ss.mmm") & "'"
                'value = "'" & value & "'"
            ElseIf quotedParameterTypes.Contains(prm.DbType) Then
                value = "'" & value & "'"
            End If


            query = query.Replace("@" & prm.ParameterName, value)
        Next

        Return query
    End Function



    Public Function EUS_CustomerPhotos_SetDefault(CustomerID As Integer, CustomerPhotosID As Long?,
                                                  IsUser As Boolean,
                                                  IsAdmin As Boolean) As Long
        Dim success As Long

        Try
            Dim sql As String = <sql><![CDATA[
declare @result bigint=0
exec @result=[EUS_CustomerPhotos_SetDefault] @CustomerId=@CustomerId, @CustomerPhotosID=@CustomerPhotosID,@IsUser=@IsUser,@IsAdmin=@IsAdmin;
select @result as Result;
]]></sql>
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@CustomerId", CustomerID)
                    If (CustomerPhotosID.HasValue) Then
                        cmd.Parameters.AddWithValue("@CustomerPhotosID", CustomerPhotosID)
                    Else
                        cmd.Parameters.AddWithValue("@CustomerPhotosID", System.DBNull.Value)
                    End If
                    cmd.Parameters.AddWithValue("@IsUser", IsUser)
                    cmd.Parameters.AddWithValue("@IsAdmin", IsAdmin)
                    success = DataHelpers.ExecuteScalar(cmd)
                End Using
            End Using
        Catch ex As Exception
            Throw
        End Try

        Return success
    End Function


    Public Function EUS_CustomerPhotos_ApproveNewPhotos(CustomerID As Integer) As Long
        Dim success As Long
        Try

            Dim sql As String = "exec [EUS_CustomerPhotos_ApproveNewPhotos] @CustomerId=@CustomerId"
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@CustomerId", CustomerID)
                    success = DataHelpers.ExecuteNonQuery(cmd)
                End Using
            End Using
        Catch ex As Exception
            Throw
        End Try

        Return success
    End Function


    Public Sub CopyEUS_ProfilesRowsData(ByRef EUS_Profiles As DSMembers.EUS_ProfilesDataTable, ByRef drTarget As DSMembers.EUS_ProfilesRow, ByRef drSource As DSMembers.EUS_ProfilesRow)

        Dim cnt As Integer = 0
        For cnt = 0 To EUS_Profiles.Columns.Count - 1
            Dim dc As DataColumn = EUS_Profiles.Columns(cnt)
            If (dc.ReadOnly OrElse dc.ColumnName = "IsMaster" OrElse dc.ColumnName = "MirrorProfileID") Then
                Continue For
            End If

            If (drTarget.RowState = DataRowState.Unchanged) Then
                Dim modified As Boolean = drTarget.IsNull(cnt) AndAlso Not drSource.IsNull(cnt)
                modified = modified OrElse Not drTarget.IsNull(cnt) AndAlso drSource.IsNull(cnt)
                modified = modified OrElse Not drTarget.IsNull(cnt) AndAlso Not drSource.IsNull(cnt) AndAlso (drTarget(cnt) <> drSource(cnt))

                If modified Then
                    drTarget.SetModified()
                End If
            End If

            drTarget(cnt) = drSource(cnt)
        Next

    End Sub

    Public Sub EUS_UserMessages_ClearByPosition(ProfileId As Integer?, Position As String)
        If (Not String.IsNullOrEmpty(Position)) Then

            Try
                Using ds As New DSCustomer
                    Using ta As New DSCustomerTableAdapters.EUS_UserMessagesTableAdapter
                        Using con As New SqlConnection(DataHelpers.ConnectionString)
                            ta.Connection = con
                            ta.FillBy_CustomerID_Position(ds.EUS_UserMessages, Position, ProfileId)
                            For cnt = 0 To ds.EUS_UserMessages.Rows.Count - 1
                                ds.EUS_UserMessages.Rows(cnt).Delete()
                            Next
                            ta.Update(ds)
                        End Using
                    End Using
                End Using
            Catch ex As Exception
                Throw New Exception(ex.Message)
            End Try
        End If
    End Sub


    Public Function LogProfileAccess(ProfileID As Integer, KeybLoginName As String, keybPassword As String,
                                Success As Boolean,
                                Title As String,
                                Device As String,
                                Cookie As String,
                                IP As String,
                                isMobile As Boolean?) As Long
        Dim newId As Long = 0
        newId = LogProfileAccess(ProfileID, KeybLoginName, keybPassword,
                                Success,
                                Title,
                                Device,
                                Cookie,
                                IP,
                                isMobile,
                                Nothing)

        Return newId
    End Function


    Public Function LogProfileAccess(ProfileID As Integer, KeybLoginName As String, keybPassword As String,
                                Success As Boolean,
                                Title As String,
                                Device As String,
                                Cookie As String,
                                IP As String,
                                isMobile As Boolean?,
                                AdditionalInfo As String) As Long
        Dim newId As Long = 0
        
        Try
            Dim access As New SYS_ProfilesAccess

            access.DateTimeAccess = Date.UtcNow
            access.KeybLoginName = KeybLoginName
            access.keybPassword = keybPassword
            access.ProfileID = ProfileID
            access.Success = Success
            access.Title = Title
            access.Cookie = Mid(Cookie, 1, 50)
            access.IP = Mid(IP, 1, 30)

            If (Not String.IsNullOrEmpty(Device)) Then
                access.Device = Mid(Device, 1, 100)
                access.IsMobile = True
            End If
            If (clsNullable.NullTo(isMobile) = True) Then
                access.IsMobile = True
            End If

            If (Not String.IsNullOrEmpty(AdditionalInfo)) Then
                access.Info = Mid(AdditionalInfo, 1, 4000)
            End If

            Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
                _CMSDBDataContext.SYS_ProfilesAccesses.InsertOnSubmit(access)
                _CMSDBDataContext.SubmitChanges()
            End Using
            newId = access.ProfilesAccessID
        Catch ex As Exception
            Throw
        End Try
        Return newId
    End Function

    Public Function SYS_ProfilesAccess_Update_IsMobile(ProfilesAccessIDList As List(Of Long)) As Integer
        Dim result As Integer = 0
        Try
            If (ProfilesAccessIDList IsNot Nothing AndAlso ProfilesAccessIDList.Count > 0) Then
                Dim list As String() = New String(ProfilesAccessIDList.Count) {}
                For c = 0 To ProfilesAccessIDList.Count - 1
                    list(c) = ProfilesAccessIDList(c).ToString()
                Next

                Dim sql As String = "update dbo.SYS_ProfilesAccess set IsMobile=1 where ProfilesAccessID in ([ProfilesAccessID])"
                sql = sql.Replace("[ProfilesAccessID]", String.Join(",", list))
                result = DataHelpers.ExecuteNonQuery(sql)
            End If

        Catch ex As Exception
            Throw
        Finally

        End Try
        Return result
    End Function

    Public Function GetAvailable_AFF_PayoutTypes() As DataTable

        Try
            Using dt = DataHelpers.GetDataTable("SELECT * FROM AFF_PayoutTypes WHERE IsActive=1 ORDER BY Title")
                Return dt
            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

    End Function


    Public Function EUS_CreditsBonusCodes_IsCodeAvailable(ProfileId As Integer, Code As String) As Boolean
        Dim dt As Long = 0
        'Dim sql As String = "select isnull((SELECT case when CreditsCodeId > 0 then 0 else 1 end  FROM EUS_CreditsBonusCodes WHERE Code=@Code and ProfileID=@ProfileID), 1) as IsCodeAvailable"
        'Dim sql As String = "select isnull((SELECT case when SubmittedDate is null then 1 else 0 end FROM EUS_CreditsBonusCodes WHERE Code=@Code and ProfileID=@ProfileID), 0) as IsCodeAvailable"

        Dim sql As String = <sql><![CDATA[
if('Q5JKLRS4T'=@Code and not exists(SELECT  CreditsCodeId 
									FROM EUS_CreditsBonusCodes 
									WHERE Code=@Code and ProfileID=@ProfileID))	
	select 1 as IsCodeAvailable
else
	select 0 as IsCodeAvailable
]]></sql>

     
        Try
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using command As SqlClient.SqlCommand = GetSqlCommand(con, sql)
                    command.Parameters.AddWithValue("@Code", Code)
                    command.Parameters.AddWithValue("@ProfileID", ProfileId)
                    dt = DataHelpers.ExecuteScalar(command)
                End Using
            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
        Return (dt = 1)
    End Function

    Public Function EUS_CreditsBonusCodes_DisableCode(ProfileId As Integer, Code As String) As Boolean
        Dim dt As Long = 0
        Dim sql As String = <sql><![CDATA[
if(exists(SELECT * FROM EUS_CreditsBonusCodes WHERE Code=@Code and ProfileID=@ProfileID))
begin
    update EUS_CreditsBonusCodes	set SubmittedDate = GETUTCDATE(),ProfileID=@ProfileID	WHERE Code=@Code
end
else
begin
    INSERT INTO [EUS_CreditsBonusCodes]
           ([Code]
           ,[SubmittedDate]
           ,[ProfileID])
     VALUES
           (@Code
           ,GETUTCDATE()
           ,@ProfileID)
end
]]></sql>
        '""

     
        Try
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using command As SqlClient.SqlCommand = GetSqlCommand(con, sql)
                    command.Parameters.AddWithValue("@Code", Code)
                    command.Parameters.AddWithValue("@ProfileID", ProfileId)
                End Using
            End Using
            dt = DataHelpers.ExecuteNonQuery(Command)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try


        Return (dt > 0)
    End Function

    Public Function GetEUS_VoucherActivatedByVoucher(ctx As CMSDBDataContext, voucher As String) As EUS_VoucherActivated
        Dim prof As EUS_VoucherActivated = Nothing
        'Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try

            prof = (From itm In ctx.EUS_VoucherActivateds
                        Where (itm.Voucher.ToUpper() = voucher.ToUpper()) _
                        Select itm).FirstOrDefault()


        Catch ex As Exception
            Throw
        Finally
            '_CMSDBDataContext.Dispose()
        End Try

        Return prof
    End Function


    Public Sub InsertToEUS_VoucherActivated(ctx As CMSDBDataContext, voucher As String, profileid As Integer, CustomerTransactionID As Integer)
        Dim prof As EUS_VoucherActivated = Nothing
        'Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try

            prof = New EUS_VoucherActivated()
            prof.DateActivated = Date.UtcNow
            prof.ProfileID = profileid
            prof.Voucher = voucher
            prof.CustomerTransactionID = CustomerTransactionID

            ctx.EUS_VoucherActivateds.InsertOnSubmit(prof)
            ctx.SubmitChanges()

        Catch ex As Exception
            Throw
        Finally
            '_CMSDBDataContext.Dispose()
        End Try
    End Sub


    Public Function EUS_CustomerVerificationDocs_GetByCustomerID(CustomerID As Integer) As DSCustomer
     
       
        Try
            Using ta As New DSCustomerTableAdapters.EUS_CustomerVerificationDocsTableAdapter
                Using ds As New DSCustomer
                    Using con As New SqlConnection(DataHelpers.ConnectionString)
                        ta.Connection = con
                        ta.FillBy_CustomerID(ds.EUS_CustomerVerificationDocs, CustomerID)
                        Return ds
                    End Using
                End Using
            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function



    Public Function GetAffiliateBy_SiteName(SiteName As String) As String
        Dim result As String = ""


        Try
            Dim sql As String = <sql><![CDATA[
select dbo.EUS_Profiles.aff_code
from AFF_Sites 
inner join dbo.EUS_Profiles on AFF_Sites.CustomerID=EUS_Profiles.ProfileID 
where [Site] like @SiteName or [Site] like 'www.'+@SiteName
]]></sql>
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@SiteName", SiteName)
                    Using dt = DataHelpers.GetDataTable(cmd)
                        If (dt.Rows.Count > 0) Then
                            result = dt.Rows(0)("aff_code").ToString()
                        End If
                    End Using
                End Using
            End Using


        Catch ex As Exception
            Throw
        End Try
        Return result
    End Function


    Public Function EUS_CustomerPhotos_GetVisiblePhotos(CustomerID As Integer) As Integer
        Dim result As Integer = 0

        Try
            Dim sql As String = <sql><![CDATA[
select COUNT(*) as VisiblePhotos
from dbo.EUS_CustomerPhotos
where CustomerID=@CustomerID
and IsDeleted=0
and HasDeclined=0 
and HasAproved=1
]]></sql>
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@CustomerID", CustomerID)
                    result = DataHelpers.ExecuteScalar(cmd)
                End Using
            End Using
        Catch ex As Exception
            Throw
        End Try
        Return result
    End Function


    Public Function EUS_CustomerPhotos_HasPrivatePhotos(CustomerID As Integer) As Integer
        Dim result As Integer = 0
        Try
            Dim sql As String = <sql><![CDATA[
select COUNT(*) as VisiblePhotos
from dbo.EUS_CustomerPhotos
where CustomerID=@CustomerID
and IsDeleted=0
and HasDeclined=0 
and HasAproved=1
and DisplayLevel>0
]]></sql>
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@CustomerID", CustomerID)
                    result = DataHelpers.ExecuteScalar(cmd)
                End Using
            End Using
        Catch ex As Exception
            Throw
        End Try
        Return result
    End Function


    Public Function EUS_CustomerPhotos_GetVisiblePhotosFromTo(userWhoOwnsPhotos As Integer, userViewingPhotos As Integer) As Integer
        Dim result As Integer = 0
        Try

            Dim sql As String = <sql><![CDATA[

declare @VisiblePhotos int=0

select @VisiblePhotos=COUNT(*) 
from dbo.EUS_CustomerPhotos
where CustomerID=@userWhoOwnsPhotos
and IsDeleted=0
and HasDeclined=0 
and HasAproved=1
and DisplayLevel=0

if(@VisiblePhotos = 0)
begin
	select @VisiblePhotos=COUNT(*) 
	from dbo.EUS_CustomerPhotos
	where CustomerID=@userWhoOwnsPhotos
	and IsDeleted=0
	and HasDeclined=0 
	and HasAproved=1
	and DisplayLevel>0
	and exists(select * from EUS_ProfilePhotosLevel lvl where lvl.FromProfileID=@userWhoOwnsPhotos and lvl.ToProfileID=@userViewingPhotos)
end

select @VisiblePhotos as VisiblePhotos

]]></sql>
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@userWhoOwnsPhotos", userWhoOwnsPhotos)
                    cmd.Parameters.AddWithValue("@userViewingPhotos", userViewingPhotos)
                    result = DataHelpers.ExecuteScalar(cmd)
                End Using

            End Using
        Catch ex As Exception
            Throw
        End Try
        Return result
    End Function

    ''' <summary>
    '''  find how many messages the one member sent to other and vice versa
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function EUS_Messages_CountMessagesExchangedFromTo_ByReferrer(ReferrerID As Integer, CustomerID As Integer) As DataTable
  
        Try
            Dim sql As String = <sql><![CDATA[
select
		MessagesSent=isnull((Select COUNT(*) 
								from dbo.EUS_Messages
								where ProfileIDOwner=@female and FromProfileID=@female and ToProfileID=@male),0)
		,MessagesSentAndOpened=isnull((Select COUNT(*) 
								from dbo.EUS_Messages
								where ProfileIDOwner=@female and FromProfileID=@female and ToProfileID=@male and isnull(cost,0)>0),0)
		,MessagesSentAndOpened_Male=isnull((Select COUNT(*) 
								from dbo.EUS_Messages
								where ProfileIDOwner=@male and FromProfileID=@female and ToProfileID=@male and StatusID=1),0)
		,MessagesReceivedAndOpened=isnull((Select COUNT(*) 
									from dbo.EUS_Messages
									where ProfileIDOwner=@female and FromProfileID=@male and ToProfileID=@female and isnull(cost,0)>0),0)
		,MessagesReceivedAndOpened_Male=isnull((Select COUNT(*) 
									from dbo.EUS_Messages
									where ProfileIDOwner=@male and FromProfileID=@male and ToProfileID=@female and StatusID=1),0)
]]></sql>
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@female", ReferrerID)
                    cmd.Parameters.AddWithValue("@male", CustomerID)
                    Using dt = DataHelpers.GetDataTable(cmd)
                        Return dt
                    End Using
                End Using
            End Using
        Catch ex As Exception
            Throw
        End Try

    End Function



    Public Function EUS_Profiles_GetREF_RegStep(CustomerID As Integer) As Integer
        Dim result As Integer = 0

        Try
            Dim sql As String = <sql><![CDATA[
select isnull((
    select top (1) REF_RegStep
    from EUS_Profiles
    where profileid=@CustomerID
),0) as REF_RegStep
]]></sql>
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@CustomerID", CustomerID)
                    result = DataHelpers.ExecuteScalar(cmd)
                End Using
            End Using
        Catch ex As Exception
            Throw
        End Try
        Return result
    End Function

    Public Function EUS_Profiles_UpdateREF_RegStep(CustomerID As Integer, REF_RegStep As Integer) As Integer
        Dim result As Integer = 0

        Try
            Dim sql As String = <sql><![CDATA[
update dbo.EUS_Profiles set REF_RegStep=@REF_RegStep where referrerParentId>0 and profileid=@CustomerID and REF_RegStep<>@REF_RegStep
]]></sql>
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection

                Using cmd = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@CustomerID", CustomerID)
                    cmd.Parameters.AddWithValue("@REF_RegStep", REF_RegStep)
                    result = DataHelpers.ExecuteNonQuery(cmd)
                End Using

            End Using

        Catch ex As Exception
            Throw
        End Try
        Return result
    End Function

    Public Function EUS_Profiles_Update_StartingFreeCredits(CustomerID As Integer) As Integer
        Dim result As Integer = 0

        Try
            Dim sql As String = <sql><![CDATA[
update dbo.EUS_Profiles set StartingFreeCredits=@StartingFreeCredits where (ProfileID=@CustomerId or MirrorProfileID=@CustomerId) and StartingFreeCredits=0
]]></sql>
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@CustomerID", CustomerID)
                    cmd.Parameters.AddWithValue("@StartingFreeCredits", True)
                    result = DataHelpers.ExecuteNonQuery(cmd)
                End Using
            End Using
        Catch ex As Exception
            Throw
        End Try
        Return result
    End Function


    Public Function EUS_Profiles_GetMasterProfileID(CustomerID As Integer) As Integer
        Dim result As Integer = 0
        Try
            Dim sql As String = <sql><![CDATA[
declare @FoundProfileID int =0
select @FoundProfileID = 
		ISNULL((select profileID from dbo.EUS_Profiles 
				where (profileID=@profileID or mirrorprofileID=@profileID)
				and ismaster=1),0)
				
if(@FoundProfileID = 0)
	select @FoundProfileID=profileID from dbo.EUS_Profiles 
	where (profileID=@profileID or mirrorprofileID=@profileID)

select @FoundProfileID as ProfileID
]]></sql>
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@profileID", CustomerID)
                    result = DataHelpers.ExecuteScalar(cmd)
                End Using
            End Using
        Catch ex As Exception
            Throw
        End Try
        Return result
    End Function



    Public Function EUS_Profiles_GetMasterStatus(CustomerID As Integer) As Long
        Dim result As Long = 0
        Try
            Dim sql As String = <sql><![CDATA[
declare @FoundProfileID int =0
select @FoundProfileID = 
		ISNULL((select profileID from dbo.EUS_Profiles 
				where (profileID=@profileID or mirrorprofileID=@profileID)
				and ismaster=1),0)
				
if(@FoundProfileID = 0)
	select @FoundProfileID=profileID from dbo.EUS_Profiles 
	where (profileID=@profileID or mirrorprofileID=@profileID)

select Status from dbo.EUS_Profiles where ProfileID=@FoundProfileID
]]></sql>
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@profileID", CustomerID)
                    result = DataHelpers.ExecuteScalar(cmd)
                End Using
            End Using
        Catch ex As Exception
            Throw
        End Try
        Return result
    End Function


    Public Function SYS_SMTPServers_GetByName(name As String) As DataSet

        Try
            Dim sql As String = <sql><![CDATA[
SELECT [SMTPServerCredentialsID]
      ,[Name]
      ,[SMTPServerName]
      ,[SMTPLoginName]
      ,[SMTPPassword]
      ,[SMTPOutPort]
      ,[SMTPFromEmail]
  ,[SmtpUseSsl]
  FROM [SYS_SMTPServers]
WHERE [Name]=@Name
]]></sql>

            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@Name", name)
                    Using result = DataHelpers.GetDataSet(cmd)
                        Return result
                    End Using
                End Using
            End Using
        Catch ex As Exception
            Throw
        End Try

    End Function

    Public Function SYS_SMTPServers() As DataSet
        Try
            Dim sql As String = <sql><![CDATA[
SELECT [SMTPServerCredentialsID]
      ,[Name]
      ,[SMTPServerName]
      ,[SMTPLoginName]
      ,[SMTPPassword]
      ,[SMTPOutPort]
      ,[SMTPFromEmail]
  ,[SmtpUseSsl]
  FROM [SYS_SMTPServers]
]]></sql>
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd = DataHelpers.GetSqlCommand(con, sql)
                    Using result = DataHelpers.GetDataSet(cmd)
                        Return result
                    End Using
                End Using
            End Using
        Catch ex As Exception
            Throw
        End Try

    End Function

    Public Function SYS_SMTPServers_GetByID(SMTPServerCredentialsID As Integer) As SYS_SMTPServer
        Dim result As SYS_SMTPServer = Nothing
        Try
            Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


                result = (From itm In _CMSDBDataContext.SYS_SMTPServers
                        Where itm.SMTPServerCredentialsID = SMTPServerCredentialsID
                        Select itm).SingleOrDefault()
            End Using

        Catch ex As Exception
            Throw
        End Try
        Return result
    End Function




    Public Function SYS_SMTPServers_GetByNameLINQ(name As String) As SYS_SMTPServer
        Dim result As SYS_SMTPServer = Nothing
        Try
            Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
                result = (From itm In _CMSDBDataContext.SYS_SMTPServers
                        Where itm.Name.ToLower = name.ToLower
                        Select itm).SingleOrDefault()
            End Using

        Catch ex As Exception
            Throw
        End Try

        Return result
    End Function



    Public Function EUS_UserMessages_GetByPosition(ProfileID As Integer, position As String) As DSCustomer
       
      
        Try
            Using ta As New DSCustomerTableAdapters.EUS_UserMessagesTableAdapter
                Using ds As New DSCustomer
                    Using con As New SqlConnection(DataHelpers.ConnectionString)
                        ta.Connection = con
                        ta.FillBy_CustomerID_Position(ds.EUS_UserMessages, position, ProfileID)
                        Return ds
                    End Using
                End Using
            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function



    Public Sub UpdateEUS_UserMessages(ByRef ds As DSCustomer)
       
        Try
            Using ta As New DSCustomerTableAdapters.EUS_UserMessagesTableAdapter
                Using con As New SqlConnection(DataHelpers.ConnectionString)
                    ta.Connection = con
                    ta.Update(ds.EUS_UserMessages)
                End Using
            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub


    Public Function EUS_Profiles_SetLimitedOnDate(CustomerID As Integer, LimitedOnDate As DateTime) As Long
        Dim result As Long = 0

        Try
            Dim sql As String = <sql><![CDATA[
update   dbo.EUS_Profiles 
set LimitedOnDate=@LimitedOnDate
where (profileID=@profileID or mirrorprofileID=@profileID)
]]></sql>
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@profileID", CustomerID)
                    cmd.Parameters.AddWithValue("@LimitedOnDate", LimitedOnDate)
                    result = DataHelpers.ExecuteNonQuery(cmd)
                End Using
            End Using
        Catch ex As Exception
            Throw
        End Try
        Return result
    End Function


    Public Function EUS_Profiles_MarkDeleted(CustomerID As Integer) As Long
        Dim result As Long = 0

        Try
            Dim sql As String = <sql><![CDATA[
update   dbo.EUS_Profiles 
set 
    LoginName='deleted_' + LoginName
    ,eMail='deleted_' + eMail
    ,Status=@Status
    ,Isonline=0
    ,IsAutoApproved=0
where (profileID=@profileID or mirrorprofileID=@profileID)
]]></sql>
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@profileID", CustomerID)
                    cmd.Parameters.AddWithValue("@Status", ProfileStatusEnum.Deleted)
                    result = DataHelpers.ExecuteNonQuery(cmd)
                End Using
            End Using
        Catch ex As Exception
            Throw
        End Try
        Return result
    End Function


    Public Function EUS_Profiles_GetLimitedOnDate(CustomerID As Integer) As DateTime
        Dim LimitedOnDate As DateTime = DateTime.MinValue
   
        Try
            Dim sql As String = <sql><![CDATA[
select top(1) LimitedOnDate
from dbo.EUS_Profiles
where (profileID=@profileID or mirrorprofileID=@profileID)
]]></sql>
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection

                Using cmd = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@profileID", CustomerID)
                    cmd.Parameters.AddWithValue("@LimitedOnDate", LimitedOnDate)
                    Using dt = DataHelpers.GetDataTable(cmd)
                        If (dt.Rows.Count > 0) Then
                            If (Not dt.Rows(0)("LimitedOnDate").ToString() <> "") Then LimitedOnDate = dt.Rows(0)("LimitedOnDate")
                        End If
                    End Using
                End Using

            End Using
        Catch ex As Exception
            Throw
        End Try
        Return LimitedOnDate
    End Function


    Public Function SYS_UsersLogins_GetAll() As DSSYSData
        Try
            Using ds As New DSSYSData()
                Using ta As New DSSYSDataTableAdapters.SYS_UsersLoginsTableAdapter
                    Using con As New SqlConnection(DataHelpers.ConnectionString)
                        ta.Connection = con
                        ta.Fill(ds.SYS_UsersLogins)
                        Return ds
                    End Using
                End Using
            End Using

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function SYS_GetSuspiciousIps(ByVal dt As DateTime) As dsBlockSuspicious
        Try
            Using ds As New dsBlockSuspicious
                Using ta As New dsBlockSuspiciousTableAdapters.GetSuspiciousIpTableAdapter
                    Using con As New SqlConnection(DataHelpers.ConnectionString)
                        ta.Connection = con
                        ta.Fill(ds.GetSuspiciousIp, dt)
                        Return ds
                    End Using
                End Using
            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function SYS_FillIsInBlockedIps() As dsBlockSuspicious
        Try
            Using ds As New dsBlockSuspicious
                Using ta As New dsBlockSuspiciousTableAdapters.SYS_BlockedIpsTableAdapter
                    Using con As New SqlConnection(DataHelpers.ConnectionString)
                        ta.Connection = con
                        ta.FillIsInBlock(ds.SYS_BlockedIps)
                        Return ds
                    End Using
                End Using
            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function SYS_FillHistoryBlockecIps() As dsBlockSuspicious
        Try
            Using ds As New dsBlockSuspicious
                Using ta As New dsBlockSuspiciousTableAdapters.SYS_BlockedIpsTableAdapter
                    Using con As New SqlConnection(DataHelpers.ConnectionString)
                        ta.Connection = con
                        ta.FillByHistory(ds.SYS_BlockedIps)
                        Return ds
                    End Using
                End Using
            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function SYS_UsersLogins_GetByLoginIdentifier(computerName As String) As DSSYSData
        Try
            Using ds As New DSSYSData
                Using ta As New DSSYSDataTableAdapters.SYS_UsersLoginsTableAdapter
                    Using con As New SqlConnection(DataHelpers.ConnectionString)
                        ta.Connection = con
                        ta.FillBy_ComputerName(ds.SYS_UsersLogins, computerName)
                        Return ds
                    End Using
                End Using
            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function



    Public Function GetProofiles_ForRegistrationEmail(Country As String, GenderID As Integer) As DataTable

        Dim sql As String = <sql><![CDATA[

select * from (
	SELECT TOP 32
		EUS_CustomerPhotos.CustomerID, 
		EUS_CustomerPhotos.FileName, 
		EUS_Profiles.LoginName,
		EUS_Profiles.DateTimeToRegister,
		CountryQualifier = case 
			when @Country=Country then 1
			else 0
                End
	FROM EUS_CustomerPhotos 
	with (nolock)
	JOIN EUS_Profiles ON EUS_CustomerPhotos.CustomerID = EUS_Profiles.ProfileID 
	WHERE(
		EUS_CustomerPhotos.ShowOnGrid=1
		And DisplayLevel=0
		And HasAproved=1 
		And HasDeclined=0 
		AND IsDeleted=0  
		And GenderId=@GenderID  
		AND (EUS_Profiles.Status = 2 OR EUS_Profiles.Status = 4) 
		and EUS_Profiles.PrivacySettings_DontShowOnPhotosGrid=0 --ISNULL(EUS_Profiles.PrivacySettings_DontShowOnPhotosGrid,0)=0
		AND EUS_Profiles.PrivacySettings_HideMeFromSearchResults=0 --ISNULL(EUS_Profiles.PrivacySettings_HideMeFromSearchResults,0)=0
	) 
	ORDER BY CountryQualifier desc, DateTimeToRegister desc 
) as t1 
ORDER BY newid() 

]]></sql>.Value
        Try
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection

                Using cmd = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@Country", Country)
                    cmd.Parameters.AddWithValue("@GenderID", GenderID)
                    Using result = DataHelpers.GetDataTable(cmd)
                        Return result
                    End Using
                End Using

            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message)

        End Try

    End Function



    Public Sub EUS_Profile_SetOnlineManually(ProfileID As Integer, IP As String, Optional TimeUntil As DateTime? = Nothing)
        DataHelpers.UpdateEUS_Profiles_Activity(ProfileID, IP, True, TimeUntil)
        Dim loginName As String = DataHelpers.GetEUS_Profile_LoginName_ByProfileID(ProfileID)
        DataHelpers.LogProfileAccess(ProfileID, loginName, "[SET-ONLINE]", True, "[SET-ONLINE]", Nothing, Nothing, IP, Nothing)
    End Sub


    Public Sub EUS_Profile_SetOfflineManually(ProfileID As Integer, IP As String)
        DataHelpers.UpdateEUS_Profiles_LogoutData(ProfileID)
        Dim loginName As String = DataHelpers.GetEUS_Profile_LoginName_ByProfileID(ProfileID)
        DataHelpers.LogProfileAccess(ProfileID, loginName, "[SET-OFFLINE]", True, "[SET-OFFLINE]", Nothing, Nothing, IP, Nothing)
    End Sub


    Public Sub UpdateEUS_Messages_SetCopyMessageRead(profileid As Integer, MessageID As Long, CopyMessageID As Long, Optional isRead As Boolean = True)

        Try
            Dim sql As String = Nothing
            If (MessageID > 0) Then
                sql = "update dbo.EUS_Messages set CopyMessageRead=@CopyMessageRead where ProfileIDOwner=@ProfileIDOwner and EUS_MessageID=@MessageID"
            ElseIf (CopyMessageID > 0) Then
                sql = "update dbo.EUS_Messages set CopyMessageRead=@CopyMessageRead where ProfileIDOwner=@ProfileIDOwner and CopyMessageID=@CopyMessageID"
            End If
            If (sql IsNot Nothing) Then

                Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                    Using cmd = DataHelpers.GetSqlCommand(con, sql)
                        cmd.Parameters.AddWithValue("@CopyMessageRead", isRead)
                        cmd.Parameters.AddWithValue("@ProfileIDOwner", profileid)
                        cmd.Parameters.AddWithValue("@MessageID", MessageID)
                        cmd.Parameters.AddWithValue("@CopyMessageID", CopyMessageID)
                        DataHelpers.ExecuteNonQuery(cmd)
                    End Using
                End Using
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Public Function EUS_Profile_IsProfileActiveByDeviceID(deviceId As String) As Boolean
        Dim isactive As Boolean = False

        Dim rerunTran As Boolean = False
        Using ctx As New CMSDBDataContext(DataHelpers.ConnectionString)
            Try

                ' check


                Dim rec As Integer =
                    (From itm In ctx.EUS_Profiles
                     Join itm2 In ctx.EUS_GCMProfiles On itm.ProfileID Equals itm2.ProfileID
                    Where itm2.GCM_RegID = deviceId AndAlso _
                            (((itm.Status = ProfileStatusEnum.NewProfile OrElse itm.Status = ProfileStatusEnum.Updating) AndAlso itm.IsMaster = False) OrElse _
                            ((itm.Status = ProfileStatusEnum.Approved OrElse itm.Status = ProfileStatusEnum.LIMITED) AndAlso itm.IsMaster = True))
                    Select itm.LoginName).Count()

                isactive = rec > 0


            Catch ex As System.Data.SqlClient.SqlException
                rerunTran = DataHelpers.CheckSqlException(ex.Message)
                If (Not rerunTran) Then Throw
            Catch ex As Exception
                Throw
            End Try


            If (rerunTran) Then
                Try

                    Dim rec As Integer =
                        (From itm In ctx.EUS_Profiles
                         Join itm2 In ctx.EUS_GCMProfiles On itm.ProfileID Equals itm2.ProfileID
                        Where itm2.GCM_RegID = deviceId AndAlso _
                                (((itm.Status = ProfileStatusEnum.NewProfile OrElse itm.Status = ProfileStatusEnum.Updating) AndAlso itm.IsMaster = False) OrElse _
                                ((itm.Status = ProfileStatusEnum.Approved OrElse itm.Status = ProfileStatusEnum.LIMITED) AndAlso itm.IsMaster = True))
                        Select itm.LoginName).Count()

                    isactive = rec > 0

                Catch ex As Exception
                    Throw
                End Try
            End If
        End Using
        Return isactive
    End Function


    Public Function EUS_Profile_IsProfileActive(profileId As Integer) As Boolean
        Dim isactive As Boolean = False

        Dim rerunTran As Boolean = False
        Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


            Try

                isactive = EUS_Profile_IsProfileActive(_CMSDBDataContext, profileId)

            Catch ex As System.Data.SqlClient.SqlException
                rerunTran = DataHelpers.CheckSqlException(ex.Message)
                If (Not rerunTran) Then Throw
            Catch ex As Exception
                Throw
            Finally
                If (Not rerunTran) Then
                    _CMSDBDataContext.Dispose()
                End If
            End Try


            If (rerunTran) Then
                Try
                    rerunTran = False
                    isactive = EUS_Profile_IsProfileActive(_CMSDBDataContext, profileId)

                Catch ex As System.Data.SqlClient.SqlException
                    rerunTran = DataHelpers.CheckSqlException(ex.Message)
                    If (Not rerunTran) Then Throw
                Catch ex As Exception
                    Throw
                Finally
                    If (Not rerunTran) Then
                        _CMSDBDataContext.Dispose()
                    End If
                End Try
            End If


            If (rerunTran) Then
                Try

                    isactive = EUS_Profile_IsProfileActive(_CMSDBDataContext, profileId)

                Catch ex As Exception
                    Throw
                Finally
                    _CMSDBDataContext.Dispose()
                End Try
            End If
        End Using
        Return isactive
    End Function

    Public Function EUS_Profile_IsProfileActive(ctx As CMSDBDataContext, profileId As Integer) As Boolean
        Dim isactive As Boolean = False
        Try

            ' check
            Dim rec As Integer =
                (From itm In ctx.EUS_Profiles
                Where itm.ProfileID = profileId AndAlso _
                        (((itm.Status = ProfileStatusEnum.NewProfile OrElse itm.Status = ProfileStatusEnum.Updating) AndAlso itm.IsMaster = False) OrElse _
                        ((itm.Status = ProfileStatusEnum.Approved OrElse itm.Status = ProfileStatusEnum.LIMITED) AndAlso itm.IsMaster = True))
                Select itm.LoginName).Count()

            isactive = rec > 0
        Catch ex As Exception
            Throw
        Finally
        End Try

        Return isactive
    End Function


    Public Function EUS_Profile_IsProfileActive(prof As EUS_Profile) As Boolean
        Dim isactive As Boolean = False

        Try
            If (prof IsNot Nothing) Then

                ' check
                isactive = ((prof.Status = ProfileStatusEnum.NewProfile OrElse prof.Status = ProfileStatusEnum.Updating) AndAlso prof.IsMaster = False) OrElse _
                            ((prof.Status = ProfileStatusEnum.Approved OrElse prof.Status = ProfileStatusEnum.LIMITED) AndAlso prof.IsMaster = True)

            End If
        Catch ex As Exception
            Throw
        Finally
        End Try

        Return isactive
    End Function
    Public Function CountTransactionByProfileId(profileID As Integer) As Integer
        Dim sql As String = <sql><![CDATA[
SELECT count( [CustomerTransactionID])
 FROM [dbo].[EUS_CustomerTransaction]
  where [TransactionType]=1 and PaymentMethods<>'SMS' and CustomerID =@ProfileID

]]></sql>.Value
        Dim result As Integer = 0
        Try
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection

                Using cmd = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@ProfileID", profileID)
                    result = DataHelpers.ExecuteScalar(cmd)

                End Using
            End Using


        Catch ex As Exception
            Throw New Exception(ex.Message)

        End Try
        Return result
    End Function

    Public Function GetRandomPredefinedMessage(TargetGenderID As Integer, LagID As String) As String
        Dim MessageText As String = ""
        Try
            Dim sqlProc As String = "EXEC [GetRandomPredefinedMessage] @TargetGenderID=" & TargetGenderID & ",@CurrentLag='" & LagID & "'"
            Using dt As DataTable = DataHelpers.GetDataTable(sqlProc)
                If (dt.Rows.Count > 0) Then
                    If (Not dt.Rows(0).IsNull("MessageText")) Then MessageText = dt.Rows(0)("MessageText")
                End If
            End Using
        Catch ex As Exception
            Throw
        Finally

        End Try
        Return MessageText
    End Function


    Public Function GetAllPredefinedMessages(TargetGenderID As Integer, LagID As String) As DataTable
        Try
            Dim sqlProc As String = "EXEC [GetAllPredefinedMessages] @TargetGenderID=" & TargetGenderID & ",@CurrentLag='" & LagID & "'"
            Using MessageText = DataHelpers.GetDataTable(sqlProc)
                Return MessageText
            End Using

        Catch ex As Exception
            Throw
        End Try
    End Function



    Public Function GetEUS_Profiles_LAGID(profileid As Integer) As String
        Dim _lagId As String

        Try
            ' check
            Using _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
                _lagId = (From itm In _CMSDBDataContext.EUS_Profiles
                        Where itm.ProfileID = profileid
                        Select itm.LAGID).FirstOrDefault()
            End Using

        Catch ex As Exception
            Throw
        End Try

        Return _lagId
    End Function

    Public Sub EUS_Profiles_UpdateCelebratingBirth()
        '  Dim MessageText As String = ""
        Try
            Dim sqlProc As String = <sql><![CDATA[
if( exists(select top(1)[CelebratingBirth]  
			FROM [EUS_Profiles]
			where datepart(year,[CelebratingBirth])<datepart(year,getutcdate())
			or [CelebratingBirth] is null ))
begin

	EXEC [EUS_Profiles_UpdateCelebratingBirth]

end
]]></sql>
            DataHelpers.ExecuteNonQuery(sqlProc)
        Catch ex As Exception
            Throw
        Finally
        End Try
    End Sub


    Public Function GetEUS_LISTS_SpokenLanguages_Datatable() As DataTable
        Try
            Dim sqlProc As String = <sql><![CDATA[
select SpokenLangId,LangName from dbo.EUS_LISTS_SpokenLanguages order by Sorting ASC
]]></sql>
            Using dt = DataHelpers.GetDataTable(sqlProc)
                Return dt
            End Using
        Catch ex As Exception
            Throw
        End Try

    End Function


    Public Function GetEUS_LISTS_SpokenLanguages_Datatable(ProfileID As Integer) As DataTable


        Try
            Dim sql As String = <sql><![CDATA[
select 
	l.SpokenLangId, 
	l.LangName, 
	psl.Profileid,
	IsSelected=case
		when psl.Profileid>0 then 1
		else 0
	end
from dbo.EUS_LISTS_SpokenLanguages l
left join EUS_ProfileSpokenLang psl on  psl.SpokenLangId=l.SpokenLangId and psl.Profileid=@ProfileID
where isnull(l.IsEnabled,1)=1
order by Sorting ASC
]]></sql>
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@ProfileID", ProfileID)
                    Using dt = DataHelpers.GetDataTable(cmd)
                        Return dt
                    End Using
                End Using
            End Using
        Catch ex As Exception
            Throw
        End Try
    End Function



    Public Function GetSqlParam_LatitubeOrLongitude(paramName As String, LatLon As Double?) As SqlClient.SqlParameter
        Dim prm As SqlClient.SqlParameter
        prm = New SqlClient.SqlParameter(paramName, SqlDbType.Decimal)
        If (LatLon Is Nothing) Then
            prm.Value = System.DBNull.Value
        Else
            prm.Value = LatLon.Value
            prm.Precision = 13
            prm.Scale = 9
        End If
        Return prm
    End Function
    Public Sub RemoveFromQuarntineandSendMessage(ByVal ProfileId As Integer, ByVal MirrorProfileId As Integer)
        Try
            Using _ds As New Dating.Server.Datasets.DLL.DSCustomer()
                Dim profileIDMaster As Integer = DataHelpers.EUS_Profiles_GetMasterProfileID(ProfileId)
                Using ta As New DSCustomerTableAdapters.EUS_ProfilesPrivacySettingsTableAdapter
                    Using con As New SqlConnection(DataHelpers.ConnectionString)
                        ta.Connection = con

                        ta.FillBy_ProfileID(_ds.EUS_ProfilesPrivacySettings, profileIDMaster)

                        If (_ds.EUS_ProfilesPrivacySettings.Rows.Count > 0) Then

                            '    Dim drs As Dating.Server.Datasets.DLL.DSCustomer.EUS_ProfilesPrivacySettingsRow() = _ds.EUS_ProfilesPrivacySettings.Select("ProfileID=" & profileIDMaster & " or MirrorProfileID=" & profileIDMaster)
                            '   If (drs.Length > 0) Then
                            _ds.EUS_ProfilesPrivacySettings(0).AdminSetting_BlockMessagesToBeReceivedByOther = False
                            _ds.EUS_ProfilesPrivacySettings(0).AdminSetting_BlockMessagesToBeReceivedByOtherDateSet = Date.UtcNow

                            '  ShowProgressWin("Enabling profile " & GridViewProfiles.GetFocusedRowCellValue("LoginName"), 2 / 10)
                            ta.Update(_ds.EUS_ProfilesPrivacySettings)
                            'End If
                            Using ds2 As New Dating.Server.Datasets.DLL.DSMembersViews()
                                Using ta2 As New Dating.Server.Datasets.DLL.DSMembersViewsTableAdapters.GetAdminMessages2TableAdapter()


                                    ta2.Connection = con

                                    ta2.Fill(ds2.GetAdminMessages2,
                                           profileIDMaster,
                                             True,
                                           False,
                                            Nothing,
                                           Nothing,
                                            Nothing,
                                           Nothing,
                                           -1,
                                         1000000,
                                            Nothing,
                                            True)

                                    For cnt As Integer = 0 To ds2.GetAdminMessages2.Rows.Count - 1
                                        Dim dr As DSMembersViews.GetAdminMessagesRow = ds2.GetAdminMessages2.Rows(cnt)
                                        clsUserDoes.SendMessageOnQuarantine(profileIDMaster, dr.EUS_MessageID)
                                    Next
                                End Using
                            End Using

                        End If


                    End Using



                End Using




            End Using
            '  ShowProgressWin("Sending all messages for profile " & GridViewProfiles.GetFocusedRowCellValue("LoginName"), 3 / 10)
            ' MyAdminWS_ApproveQuarauntineMessages.ApproveAllMessagesOnQuarantine(GlbAdminWS.gHeaders, ProfileID, ProfileID)

        Catch ex As Exception

        End Try
    End Sub

    Public Sub UpdateEUS_CustomerPhotos_IsUpdating(CustomerID As Integer, CustomerPhotosID As Long, IsUpdating As Boolean)

        Try

            Dim sb As String = "UPDATE [EUS_CustomerPhotos] SET [IsUpdating] = @IsUpdating  WHERE [CustomerID] = @CustomerID AND [CustomerPhotosID]=@CustomerPhotosID AND isnull([IsUpdating],0)=0"
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd = DataHelpers.GetSqlCommand(con, sb)
                    cmd.Parameters.Add(New SqlParameter("@IsUpdating", IsUpdating))
                    cmd.Parameters.Add(New SqlParameter("@CustomerID", CustomerID))
                    cmd.Parameters.Add(New SqlParameter("@CustomerPhotosID", CustomerPhotosID))
                    DataHelpers.ExecuteNonQuery(cmd)
                End Using
            End Using
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Public Function Check_IsSpammerProfile(ProfileId As Integer, MD5 As Guid) As DataTable
     
        Try

            Dim sb As String = "EXEC [Check_IsSpammerProfile] @ProfileId=@ProfileId, @MD5=@MD5"
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd = DataHelpers.GetSqlCommand(con, sb)
                    cmd.Parameters.Add(New SqlParameter("@ProfileId", ProfileId))
                    cmd.Parameters.Add(New SqlParameter("@MD5", MD5))
                    Using dt = DataHelpers.GetDataTable(cmd)
                        Return dt
                    End Using
                End Using
            End Using
        Catch ex As Exception
            Throw
        End Try

    End Function

End Module




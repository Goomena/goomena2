﻿<%@ Page Title="" Language="vb" AutoEventWireup="true" MasterPageFile="~/Root2.Master" CodeBehind="Default2.aspx.vb" 
Inherits="Dating.Server.Site.Web._Default140603" %>

<%@ Register src="~/UserControls/UsersList.ascx" tagname="UsersList" tagprefix="uc1" %>
<%@ Register src="~/UserControls/LiveZillaPublic.ascx" tagname="LiveZillaPublic" tagprefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="//cdn.goomena.com/CSS/Hint.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="//cdn.goomena.com/css/slide/slide.css" />
    <link rel="stylesheet" href="//cdn.goomena.com/css/slide/custom.css" />
    <link rel="stylesheet" href="//cdn.goomena.com/balance/media/widgetkit/widgets/slideshow/css/slideshow.css" />
    <style type="text/css">
        .footernew {border-top: 0;margin-top: 70px;}
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
<div id="all_sections">
    <div class="section-1-ext">
        <div class="section-1">
            <div class="regmidcontainer" id="regmidcontainer" runat="server">
                <div class="reg-box">
                    <div class="registerform" id="registerform" runat="server">
                        <div class="mainregdiv">
                            <div class="notmember">
                                <asp:Label ID="lblNotAMember" runat="server" CssClass="header"></asp:Label>
                            </div>
                            <div runat="server" id="mainregdivctr" class="mainregdivctr" ><%--data-hint="Please select Gender before you continue!"--%>
                                <dx:ASPxRadioButton ID="RdioMale" runat="server" GroupName="selectgender" 
                                    Font-Size="16px" Text="">
                                    <CheckedImage Url="//cdn.goomena.com/images2/search/check.png" Height="19px"
                                        Width="18px">
                                    </CheckedImage>
                                    <UncheckedImage Url="//cdn.goomena.com/images2/search/uncheck.png" Height="19px"
                                        Width="18px">
                                    </UncheckedImage>
                                    <BackgroundImage HorizontalPosition="265px" ImageUrl="//cdn.goomena.com/images2/pub2/men-icon.png" Repeat="NoRepeat" VerticalPosition="50%" />
                                </dx:ASPxRadioButton>
                                <div style="height:4px;width:100%;"></div>
                                <dx:ASPxRadioButton ID="RdioFemale" runat="server" GroupName="selectgender" 
                                    Font-Size="16px" Text="">
                                    <CheckedImage Url="//cdn.goomena.com/images2/search/check.png" Height="19px"
                                        Width="18px">
                                    </CheckedImage>
                                    <UncheckedImage Url="//cdn.goomena.com/images2/search/uncheck.png" Height="19px"
                                        Width="18px">
                                    </UncheckedImage>
                                    <BackgroundImage HorizontalPosition="265px" ImageUrl="//cdn.goomena.com/images2/pub2/woman-icon.png" Repeat="NoRepeat" VerticalPosition="50%" />
                                </dx:ASPxRadioButton>
                            </div>
                            <div class="notmember2">
                                <asp:Label ID="lblBecomeMember2" runat="server" CssClass="sub-header font-semibold"></asp:Label>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="reg-buttons">
                            <a id="HyperRegister" runat="server" href="Register2.aspx" class="HyperRegister font-light" onclick="ShowLoading();">
                                <asp:Literal ID="lblRegister" runat="server"></asp:Literal>
                            </a>
                            <div class="reg-alternative">
                                <table class="center-table"><tr>
                                    <td><div class="hor-line"></div></td>
                                    <td>&nbsp;<asp:Literal ID="lblAlternativeReg" runat="server"></asp:Literal>&nbsp;</td>
                                    <td><div class="hor-line"></div></td>
                                       </tr></table>
                            </div>
                            <asp:HyperLink ID="lblFB" runat="server" Text="Login With Facebook"
                                NavigateUrl="~/Register2.aspx?log=fb" onclick="ShowLoading();" CssClass="main-fb-logindiv">
                                <img src="//cdn.goomena.com/images/spacer10.png"  alt="facebook" width="24" height="47"/>
                                <asp:Label ID="lblFBConnect" runat="server" class="fb-connect "></asp:Label>
                                <asp:Label ID="lbFBNote" runat="server" class="fb-connect-note"></asp:Label>
                            </asp:HyperLink>
                            <div class="reg-alternative-separator"></div>
                            <asp:HyperLink ID="lblGP" runat="server" Text="Login With Google"
                                NavigateUrl="~/Register2.aspx?log=google" onclick="ShowLoading();" CssClass="main-gp-logindiv">
                                <img src="//cdn.goomena.com/images/spacer10.png" alt="g+" width="25" height="39"/>
                                <asp:Label ID="lblGPConnect" runat="server" class="gp-connect "></asp:Label>
                            </asp:HyperLink>
                            <div class="reg-alternative-separator"></div>
                            <asp:HyperLink ID="lblGoo" runat="server" Text=""
                                NavigateUrl="~/Login2.aspx" onclick="ShowLoading();" CssClass="main-goo-logindiv">
                                <asp:Label ID="lblGooConnect" runat="server" class="goo-connect" Text="Are you registered already - Connect now"></asp:Label>
                            </asp:HyperLink>
                        </div>
                    </div>
                    <div class="reg-form-footer" id="registerform_footer" runat="server">
                        <strong><asp:Literal ID="lblRegFooter" runat="server"></asp:Literal></strong>
                    </div>
                    <h1 class="reg-wellcome"><asp:Literal ID="lblWellcomeMsg" runat="server"></asp:Literal></h1>
                </div>
            </div>
            <div class="gendermsg"></div> 
            <div id="top-pic"></div>
        </div>
    </div> 
    <div class="section-1">
        <div class="members-count">
            <asp:Literal ID="lblMembersCount" runat="server"></asp:Literal>
        </div>
    </div> 
    <div class="section-1-ext">
        <div class="section-1">
            <div class="members-photos">
                <asp:Panel ID="pnlMosaic" CssClass="mosaic" runat="server" EnableViewState="false">
		            <div runat="server" class="pic" id="pic1"></div>
		            <div runat="server" class="pic" id="pic2"></div>
		            <div runat="server" class="pic" id="pic3"></div>
		            <div runat="server" class="pic" id="pic4"></div>
		            <div runat="server" class="pic" id="pic5"></div>
		            <div runat="server" class="pic" id="pic6"></div>
		            <div runat="server" class="pic" id="pic7"></div>
		            <div runat="server" class="pic" id="pic8"></div>
		            <div runat="server" class="pic" id="pic9"></div>
		            <div runat="server" class="pic" id="pic10"></div>
		            <div runat="server" class="pic" id="pic11"></div>
		            <div runat="server" class="pic" id="pic12"></div>
		            <div runat="server" class="pic" id="pic13"></div>
		            <div runat="server" class="pic" id="pic14"></div>
		            <div runat="server" class="pic" id="pic15"></div>
		            <div runat="server" class="pic" id="pic16"></div>
		            <div runat="server" class="pic" id="pic17"></div>
		            <div runat="server" class="pic" id="pic18"></div>
		            <div runat="server" class="pic" id="pic19"></div>
                    <div class="clear"></div>
                 </asp:Panel> 
            </div>
        </div>
    </div> 
    <div class="section-2">
        <div class="inner-box" >
            <div class="left-content">
                <img src="//cdn.goomena.com/Images2/pub2/papigion.png" alt="" width="138" height="79" class="left-image"/>
                <p class="header"><asp:Literal ID="lblMenMessage4Hdr" runat="server"></asp:Literal></p>
            </div>
            <div class="right-content">
                <img src="//cdn.goomena.com/Images2/pub2/gova.png" alt="" width="121" height="102" class="right-image"/>
                <p class="header"><asp:Literal ID="lblLadiesMessage4Hdr" runat="server"></asp:Literal></p>
            </div>
            <div class="clear"></div>
            <div class="left-content">
                <div class="text"><asp:Literal ID="lblMenMessage4" runat="server"></asp:Literal></div>
            </div>
            <div class="right-content">
                <div class="text"><asp:Literal ID="lblLadiesMessage4" runat="server"></asp:Literal></div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="section-3">
        <div class="inner-box">
            <div class="content-box">
                <img src="//cdn.goomena.com/Images2/pub2/cycle.png?v=1" alt="" width="30" height="30" class="top-image" />
                <p class="text">
                <asp:Literal ID="lblFollowSteps" runat="server">
                </asp:Literal>
                </p>
                <img src="//cdn.goomena.com/Images2/pub2/cycle.png?v=1" alt="" width="30" height="30" class="bottom-image" />
            </div>
        </div>
    </div>
    <div class="section-4-ext">
        <div class="section-4">
            <div class="inner-box" >
                <div class="content-box">
                    <div class="left-content">
                        <img src="//cdn.goomena.com/Images2/pub2/papigion-small.png" alt="" width="45" height="26" />
                        <p class="text">
                            <asp:Literal ID="lblLeftCreate" runat="server"></asp:Literal>
                        </p>
                    </div>
                    <div class="middle-content">
                    </div>
                    <div class="right-content">
                        <img src="//cdn.goomena.com/Images2/pub2/gova-small.png" alt="" width="50" height="42" />
                        <p class="text">
                            <asp:Literal ID="lblRightCreate" runat="server"></asp:Literal>
                        </p>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="buttons-box">
                    <div class="left-content">
                        <asp:HyperLink ID="lnkLeftCreate" runat="server" CssClass="btn" NavigateUrl="~/Register2.aspx">Δημιουργία Προφίλ</asp:HyperLink>
                    </div>
                    <div class="right-content">
                        <asp:HyperLink ID="lnkRightCreate" runat="server" CssClass="btn" NavigateUrl="~/Register2.aspx">Δημιουργία Προφίλ</asp:HyperLink>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </div>


    <div class="section-5">
        <div class="inner-box" >
            <div class="content-box">
                <div class="left-content">
                    <img src="//cdn.goomena.com/Images2/pub2/papigion-small.png" alt="" width="45" height="26" />
                    <p class="text">
                        <asp:Literal ID="lblLeftLike" runat="server"></asp:Literal>
                    </p>
                </div>
                <div class="middle-content">
                    <div class="middle-content-back">
                    </div>
                </div>
               <div class="right-content">
                    <img src="//cdn.goomena.com/Images2/pub2/gova-small.png" alt="" width="50" height="42" />
                    <p class="text">
                        <asp:Literal ID="lblRightLike" runat="server"></asp:Literal>
                    </p>
                </div>
                <div class="clear"></div>
            </div>
            <div class="buttons-box">
                <div class="left-content">
                    <asp:HyperLink ID="lnkLeftLike" runat="server" CssClass="btn" NavigateUrl="~/Register2.aspx">Ξεκίνα τα Like απο τώρα</asp:HyperLink>
                </div>
                <div class="middle-content">
                </div>
                <div class="right-content">
                    <asp:HyperLink ID="lnkRightLike" runat="server" CssClass="btn" NavigateUrl="~/Register2.aspx">Ανταποκρίσου στα like τώρα</asp:HyperLink>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>


    <div class="section-6-ext">
        <div class="section-6">
            <div class="inner-box" style="position:relative;">
                <div class="content-box">
                    <div class="left-content">
                        <img src="//cdn.goomena.com/Images2/pub2/papigion-small.png" alt="" width="45" height="26" />
                        <p class="text">
                            <asp:Literal ID="lblLeftDate" runat="server">
                            </asp:Literal>
                        </p>
                    </div>
                    <div class="middle-content">
                    </div>
                    <div class="right-content">
                        <img src="//cdn.goomena.com/Images2/pub2/gova-small.png" alt="" width="50" height="42" />
                        <p class="text">
                            <asp:Literal ID="lblRightDate" runat="server"></asp:Literal>
                        </p>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="buttons-box">
                    <div class="left-content">
                        <asp:HyperLink ID="lnkLeftDate" runat="server" CssClass="btn" NavigateUrl="~/Register2.aspx"></asp:HyperLink>
                    </div>
                    <div class="right-content">
                        <asp:HyperLink ID="lnkRightDate" runat="server" CssClass="btn" NavigateUrl="~/Register2.aspx"></asp:HyperLink>
                    </div>
                    <div class="clear"></div>
                </div>
                <div style="position:absolute;bottom:0px;left:400px;right: 400px;">
                    <div style="width:30px;margin:0 auto;height:180px;background-color:#F3F3F3;">
                        <img src="//cdn.goomena.com/Images2/pub2/cycle.png?v=1" alt="" width="30" height="30" class="top-image" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    

    <div class="section-7-ext">
        <div class="section-7">
            <div class="inner-box">
                <%--<div class="content-box">--%>
                    <div style="position:absolute;bottom:0px;left:0px;right:0px;">
                        <table class="center-table">
                            <tr>
                                <td valign="top"><div class="left-content">
                                        <asp:Literal ID="lblLeftWaiting" runat="server"></asp:Literal>
                                    </div></td>
                                <td><div style="width:50px;height:10px;"></div></td>
                                <td valign="top"><div class="right-content">
                                        <asp:Literal ID="lblRightWaiting" runat="server"></asp:Literal>
                                    </div></td>
                            </tr>
                        </table>
                    </div>
               <%-- </div>--%>
            </div>
        </div>
    </div>


    <div class="section-8">
        <div class="inner-box" >
            <table class="center-table">
                <tr><td>
                <h5>
                    <span class="hor-dots-top"><img src="//cdn.goomena.com/images/spacer10.png" alt="" width="80" height="2" /></span>
                    <asp:Literal ID="lblYourExperience" runat="server">Ζήσε την δική σου περιπέτεια!</asp:Literal>
                    <span class="hor-dots-bottom"><img src="//cdn.goomena.com/images/spacer10.png" alt="" width="80" height="2" /></span>
                </h5>
                <h6><asp:Literal ID="lblBecomeMember" runat="server">Γίνε μέλος τώρα</asp:Literal></h6>
            </td></tr></table>
            <ul class="middle-content">
                <li>
                    <asp:HyperLink ID="lblRegBottom" runat="server"
                        NavigateUrl="~/Register2.aspx" onclick="ShowLoading();" CssClass="main-logindiv">
                        Κάνε εγγραφή τώρα
                    </asp:HyperLink>
                </li>
                <li class="reg-alternative-separator"></li>
                <li>
                    <asp:HyperLink ID="lblFBBottom" runat="server" Text="Login With Facebook"
                        NavigateUrl="~/Register2.aspx?log=fb" onclick="ShowLoading();" CssClass="main-fb-logindiv">
                        <span class="content">
                        <img src="//cdn.goomena.com/images/spacer10.png"  alt="facebook" width="24" height="47"/>
                        <asp:Label ID="lblFBConnectBottom" runat="server" class="fb-connect "></asp:Label>
                        <asp:Label ID="lbFBNoteBottom" runat="server" class="fb-connect-note"></asp:Label>
                        </span>
                    </asp:HyperLink>
                </li>
                <li class="reg-alternative-separator"></li>
                <li>
                    <asp:HyperLink ID="lblGPBottom" runat="server" Text="Login With Google"
                        NavigateUrl="~/Register2.aspx?log=google" onclick="ShowLoading();" CssClass="main-gp-logindiv">
                        <span class="content">
                        <img src="//cdn.goomena.com/images/spacer10.png" alt="g+" width="25" height="39"/>
                        <asp:Label ID="lblGPConnectBottom" runat="server" class="gp-connect "></asp:Label>
                        </span>
                    </asp:HyperLink>
                </li>
                <li class="reg-alternative-separator"></li>
                <li>
                    <asp:HyperLink ID="lblGooBottom" runat="server" Text=""
                        NavigateUrl="~/Login2.aspx" onclick="ShowLoading();" CssClass="main-goo-logindiv">
                        <span class="content">
                        <asp:Label ID="lblGooConnectBottom" runat="server" class="goo-connect" Text="Are you registered already - Connect now"></asp:Label>
                        </span>
                    </asp:HyperLink>
                </li>
             </ul>
        </div>
    </div>

    <div class="section-9-ext">
        <div class="section-9">
            <div class="members-photos">
                <asp:Panel ID="pnlMosaicBottom" CssClass="mosaic" runat="server" EnableViewState="false">
                <div runat="server" class="pic" id="pic1btm"></div>
                <div runat="server" class="pic" id="pic2btm"></div>
                <div runat="server" class="pic" id="pic3btm"></div>
                <div runat="server" class="pic" id="pic4btm"></div>
                <div runat="server" class="pic" id="pic5btm"></div>
                <div runat="server" class="pic" id="pic6btm"></div>
                <div runat="server" class="pic" id="pic7btm"></div>
                <div runat="server" class="pic" id="pic8btm"></div>
                <div runat="server" class="pic" id="pic9btm"></div>
                <div runat="server" class="pic" id="pic10btm"></div>
                <div runat="server" class="pic" id="pic11btm"></div>
                <div runat="server" class="pic" id="pic12btm"></div>
                <div runat="server" class="pic" id="pic13btm"></div>
                <div runat="server" class="pic" id="pic14btm"></div>
                <div runat="server" class="pic" id="pic15btm"></div>
                <div runat="server" class="pic" id="pic16btm"></div>
                <div runat="server" class="pic" id="pic17btm"></div>
                <div runat="server" class="pic" id="pic18btm"></div>
                <div runat="server" class="pic" id="pic19btm"></div>
                <div class="clear"></div>
                </asp:Panel> 
            </div>
        </div>
    </div>

</div>

    <%--<div style="display:none;">
        <a href="http://zizina.com" target="_blank">zizina</a>
    </div>--%>


        <%--<div class="maina">
        
    <asp:Panel ID="pnlStartingText" CssClass="manwomancontainer" runat="server" EnableViewState="false">
        <div class="top-text-wrap">
            <div class="woman"><asp:Literal ID="ltrMenMessage" runat="server"></asp:Literal></div>
            <div class="man"><asp:Literal ID="ltrLadiesMessage" runat="server"></asp:Literal></div>
            <div class="clear"></div>
        </div>
        <div class="collapsible-text-wrap">
            <div class="woman" style="margin-top:0px;">
                <asp:Literal ID="ltrMenMessageCollapse" runat="server"></asp:Literal>
            </div>
            <div class="man" style="margin-top:0px;">
                <asp:Literal ID="ltrLadiesMessageCollapse" runat="server"></asp:Literal>
            </div>
            <div class="clear"></div>
        </div>
        <div class="photos4-wrap">
            <div class="woman" style="margin-top:0px;">
                <div class="photosforwomen">
                    <asp:HyperLink ID="anchornum1" runat="server">
                        <div class="polaroid">
                            <p runat="server" id="imagename1" class="imagename1">
                            </p>
                            <img runat="server" id="imgtag1" class="imgtag1" />
                        </div>
                    </asp:HyperLink>
                    <asp:HyperLink ID="anchornum2" runat="server">
                        <div class="polaroid">
                            <p runat="server" id="imagename2" class="imagename2">
                            </p>
                            <img runat="server" id="imgtag2" class="imgtag2" />
                        </div>
                    </asp:HyperLink>
                    <asp:HyperLink ID="anchornum3" runat="server">
                        <div class="polaroid">
                            <p runat="server" id="imagename3" class="imagename3">
                            </p>
                            <img runat="server" id="imgtag3" class="imgtag3" />
                        </div>
                    </asp:HyperLink>
                    <asp:HyperLink ID="anchornum4" runat="server">
                        <div class="polaroid">
                            <p runat="server" id="imagename4" class="imagename4">
                            </p>
                            <img runat="server" id="imgtag4" class="imgtag4" />
                        </div>
                    </asp:HyperLink>
                </div>
                <div class="clear"></div>
            </div>
            <div class="man" style="margin-top:0px;">
                <div class="photosformen">
                    <asp:HyperLink ID="anchornum5" runat="server">
                        <div class="polaroid2">
                            <p runat="server" id="imagename5" class="imagename5">
                            </p>
                            <img runat="server" id="imgtag5" class="imgtag5" />
                        </div>
                    </asp:HyperLink>
                    <asp:HyperLink ID="anchornum6" runat="server">
                        <div class="polaroid2">
                            <p runat="server" id="imagename6" class="imagename6">
                            </p>
                            <img runat="server" id="imgtag6" class="imgtag6" />
                        </div>
                    </asp:HyperLink>
                    <asp:HyperLink ID="anchornum7" runat="server">
                        <div class="polaroid2">
                            <p runat="server" id="imagename7" class="imagename7">
                            </p>
                            <img runat="server" id="imgtag7" class="imgtag7" />
                        </div>
                    </asp:HyperLink>
                    <asp:HyperLink ID="anchornum8" runat="server">
                        <div class="polaroid2">
                            <p runat="server" id="imagename8" class="imagename8">
                            </p>
                            <img runat="server" id="imgtag8" class="imgtag8" />
                        </div>
                    </asp:HyperLink>
                </div>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </asp:Panel>
    </div>
    <div class="clear"></div>--%>

<div class="PhotoInfoWrap hidden">
    <div id="PhotoInfos" class="PhotoInfos">
        <img class="InfoImg" alt="" src="" />
    </div>
    <p class="ProfileID"></p>
    <p class="ProfileCity"></p>
    <div class="ProfileAgeWrap">
        <p class="ProfileAgeText"></p>
        <p class="ProfileAge"></p>
    </div>
</div>

                       
<script type="text/javascript">
    var profileBaseUrl='<%= MyBase.LanguageHelper.GetPublicURL("/profile/", MyBase.GetLag()) %>';
    
    if (!stringIsEmpty(profileBaseUrl)) {
        if (profileBaseUrl.indexOf("?") > -1) profileBaseUrl = profileBaseUrl.substring(0, profileBaseUrl.indexOf("?"));
    }

    function checkBorders(e) {
        var widthCheck = 2004 + 20;
        var itmsExt = ['.section-1-ext', '.section-9-ext'];
        var itms = ['.section-1', '.section-9'];
        for (var i = 0; i < itmsExt.length; i++) {
            var itmExt = $(itmsExt[i]);
            var itm = $(itms[i], itmsExt[i]);
            var w = itmExt.width();
            if (w >= widthCheck && !itm.is('.ext-borders')) {
                itm.addClass('ext-borders');
            }
            else if (w < widthCheck && itm.is('.ext-borders')) {
                itm.removeClass('ext-borders');
            }
        }
    }

    $(checkBorders);
    $(window).on('resize', checkBorders);
</script>  
<script type="text/javascript" src="/v1/Scripts/mosaic.js"></script>  
<script type="text/javascript">

    $("#lista1,#listaman1").toggle(function () {
        $("#lista2,#lista3,#lista4,#tiperimeneis,#listaman2,#listaman3,#listaman4,#tiperimeneisman").removeClass("hidden");
    },
    function () {
        $("#lista2,#lista3,#lista4,#tiperimeneis,#listaman2,#listaman3,#listaman4,#tiperimeneisman").addClass("hidden");
    });
    $("#lista1,#listaman1").toggle(function () {
        $("#lista1").css('background', 'url("https://cdn.goomena.com/goomena-kostas/first-general/red-meion.png") no-repeat scroll -10px 0 transparent');
        $("#listaman1").css('background', 'url("https://cdn.goomena.com/goomena-kostas/first-general/black-meion.png") no-repeat scroll -9px -1px transparent');
    }, function () {
        $("#lista1").css('background', 'url("https://cdn.goomena.com/goomena-kostas/first-general/plus-red.png") no-repeat scroll -10px 0 transparent');
        $("#listaman1").css('background', 'url("https://cdn.goomena.com/goomena-kostas/first-general/plus.png") no-repeat scroll -10px 0 transparent');
    });
    $("#listaman4").tooltip({ tooltipClass: "tooltipcustom" });
    var lag = document.cookie.split('LagID=');
    if (lag.length > 1) {
        if (lag[1].slice(0, 2) == "US") {
            $("#listaman4").tooltip({ content: "This offer represents the expenses a woman have to do in order to prepare herself for a date." });
        }
        else if (lag[1].slice(0, 2) == "GR") {
            $("#listaman4").tooltip({ content: "Η προσφορά αυτή , αντιστοιχεί στην ελάχιστη αμοιβή για την προετοιμασία της γυναίκας για να βγει ραντεβού." });
        }
        else if (lag[1].slice(0, 2) == "AL") {
            $("#listaman4").tooltip({ content: "Oferta ndikon për përgatitjen e vajzës për tu takuar." });
        }
    }
</script>
<%--<div class="mobilebanner" style=" width:1008px;margin-left:auto;margin-right:auto;display:block;margin-bottom:40px;"><a href="https://play.google.com/store/apps/details?id=com.goomena.app&hl=el" target="_blank"><img style="margin-left:auto;margin-right:auto;display:block;" src="../Images/MobileBanners/android-banner.png" id="iMobileBanner" runat="server" /></a></div>--%>

</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="cntBodyBottom" runat="server">
    <uc3:LiveZillaPublic ID="ctlLiveZillaPublic" runat="server" />
</asp:Content>

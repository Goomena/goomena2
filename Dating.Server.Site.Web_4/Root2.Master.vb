﻿Imports Library.Public
Imports DevExpress.Web.ASPxMenu
Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Core.DLL
Imports DevExpress.Web.ASPxEditors

Public Class _RootMaster140603
    Inherits BaseMasterPagePublic

    Public Property CssClassVerifyLogin As String = ""

    'Dim _pageData As clsPageData
    'Protected ReadOnly Property CurrentPageData As clsPageData
    '    Get
    '        'If (_pageData Is Nothing) Then _pageData = New clsPageData("master", Context)
    '        If (_pageData Is Nothing) Then
    '            _pageData = New clsPageData("master", Context)
    '            AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
    '        End If

    '        Return _pageData
    '    End Get
    'End Property

    Private Sub Page_Disposed1(sender As Object, e As EventArgs) Handles Me.Disposed
        Try
            If Me.ucLogin1 IsNot Nothing Then
                RemoveHandler Me.ucLogin1.LoggingInFailed, AddressOf ucLogin1_LoggingInFailed
            End If
        Catch ex As Exception
        End Try
        Try
            MyBase.Dispose()
        Catch ex As Exception
        End Try
    End Sub
    Private Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        Try
            AddHandler ucLogin1.LoggingInFailed, AddressOf ucLogin1_LoggingInFailed

            If Not Me.IsPostBack Then
                Dim lagIdOnStart As String = Session("LagID")
                If (Session("SessionStarted_RedirectToCountryLAG") = True) Then
                    Session("SessionStarted_RedirectToCountryLAG") = Nothing

                    Try

                        If (LanguageHelper.SessionStarted_RedirectToCountryLAG(True) = True) Then
                            Return
                        End If

                    Catch ex As System.Threading.ThreadAbortException
                    Catch ex As Exception
                        WebErrorMessageBox(Me, ex, "")
                    End Try
                End If

                'Session("LagID") = GetChangedLang()

                If (Not String.IsNullOrEmpty(Me.RequestedLAGID)) Then
                    Session("LagID") = Me.RequestedLAGID
                Else
                    Session("LagID") = clsLanguageHelper.GetLagCookie()
                End If

                If (Not clsLanguageHelper.IsSupportedLAG(Session("LagID"), Session("GEO_COUNTRY_CODE"))) Then
                    clsLanguageHelper.SetLAGID()
                End If

                If (clsLanguageHelper.IsSupportedLAG(Session("LagID"), Session("GEO_COUNTRY_CODE"))) Then
                    cmbLag.Items(clsLanguageHelper.GetIndexFromLagID(Session("LagID"))).Selected = True
                End If
                clsLanguageHelper.SetLagCookie(Session("LagID"))
                SetLanguageLinks()

                ' update lagid info
                Try
                    If (lagIdOnStart <> Session("LagID")) Then
                        If (Me.Session("ProfileID") > 0) Then
                            DataHelpers.UpdateEUS_Profiles_LAGID(Me.Session("ProfileID"), Me.Session("LagID"))
                        End If
                    End If
                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "")
                End Try
            End If


            If (cmbLag.SelectedItem Is Nothing) Then
                If (clsLanguageHelper.IsSupportedLAG(Session("LagID"), Session("GEO_COUNTRY_CODE"))) Then
                    cmbLag.Items(clsLanguageHelper.GetIndexFromLagID(Session("LagID"))).Selected = True
                End If
                clsLanguageHelper.SetLagCookie(Session("LagID"))
            End If


            Try
                ' otan to session exei diaforetiki glwsa apo tin epilegmeni glwssa
                If (clsLanguageHelper.GetIndexFromLagID(Session("LagID")) = 0 AndAlso Session("LagID") <> "US") Then
                    Session("LagID") = gDomainCOM_DefaultLAG
                    clsLanguageHelper.SetLagCookie(Session("LagID"))
                End If
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try


            If Not Me.IsPostBack OrElse Me.VerifyLagProps() Then
                LoadLAG()
            End If


            If (Session("MAX_LOGIN_RETRIES") = 1 OrElse Session("MAX_LOGIN_RETRIES") = 2) Then
                Dim MainLogin As System.Web.UI.WebControls.Login = ucLogin1.LoginControl

                Dim btnLogin As ASPxButton = MainLogin.FindControl("btnLogin")
                If (btnLogin IsNot Nothing) Then btnLogin.PostBackUrl = "~/Login2.aspx"

                Dim lblUsernameTopTD As HtmlControl = MainLogin.FindControl("lblUsernameTopTD")
                If (lblUsernameTopTD IsNot Nothing) Then lblUsernameTopTD.Visible = False

                Dim lblPasswordTopTD As HtmlControl = MainLogin.FindControl("lblPasswordTopTD")
                If (lblPasswordTopTD IsNot Nothing) Then lblPasswordTopTD.Visible = False

                Dim UserNameTD As HtmlControl = MainLogin.FindControl("UserNameTD")
                If (UserNameTD IsNot Nothing) Then UserNameTD.Visible = False

                Dim PasswordTD As HtmlControl = MainLogin.FindControl("PasswordTD")
                If (PasswordTD IsNot Nothing) Then PasswordTD.Visible = False

                Dim chkRememberMeTD As HtmlControl = MainLogin.FindControl("chkRememberMeTD")
                If (chkRememberMeTD IsNot Nothing) Then chkRememberMeTD.Visible = False

                Dim lbForgotPasswordTD As HtmlControl = MainLogin.FindControl("lbForgotPasswordTD")
                If (lbForgotPasswordTD IsNot Nothing) Then lbForgotPasswordTD.Visible = False

                If (Request.Url.LocalPath = "/Login2.aspx") Then
                    Dim btnLoginTD As HtmlControl = MainLogin.FindControl("btnLoginTD")
                    If (btnLoginTD IsNot Nothing) Then btnLoginTD.Visible = False

                    Dim btnLoginTopTD As HtmlControl = MainLogin.FindControl("btnLoginTopTD")
                    If (btnLoginTopTD IsNot Nothing) Then btnLoginTopTD.Visible = False

                    Dim btnLoginBotTD As HtmlControl = MainLogin.FindControl("btnLoginBotTD")
                    If (btnLoginBotTD IsNot Nothing) Then btnLoginBotTD.Visible = False
                End If
            End If

        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            ' ShowUserMap()

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Private Sub ModifyApplicationForLanguageChanges()
        Try
            If (Not Page.IsPostBack) Then
                If (Not String.IsNullOrEmpty(Me.RequestedLAGID)) Then
                    cmbLag.SelectedIndex = clsLanguageHelper.GetIndexFromLagID(Me.RequestedLAGID)
                End If
            End If

            'If (Session("LagID") <> cmbLag.SelectedItem.Value) Then
            clsLanguageHelper.SetLagCookie(cmbLag.SelectedItem.Value)
            'End If

        Catch ex As Exception
            Session("LagID") = cmbLag.SelectedItem.Value
        End Try
    End Sub



    Protected Sub cmbLag_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbLag.SelectedIndexChanged
        ModifyApplicationForLanguageChanges()
        Try

            Me._pageData = Nothing
            LoadLAG()

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try


        Try
            ModGlobals.UpdateUserControls(Me.Page, True)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


    Protected Sub LoadLAG()
        Try
            lnkLogo.NavigateUrl = MyBase.LanguageHelper.GetPublicURL("/Default2.aspx", MyBase.GetLag())
            imgLogo.Alt = CurrentPageData.GetCustomString("imgLogo.Alt.Text")
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Try
            'PopulateMenuData(mnuHeaderLeft, "topLeft")
            'PopulateMenuData(mnuHeaderRight, "topRight")
            'PopulateMenuData(mnuFooter, "bottom")
            'PopulateMenuData(mnuHeader, "bottom")

            clsMenuHelper.PopulateMenuData(mnuNavi, "topLeft")
            clsMenuHelper.CheckPublicMenuLinks(mnuNavi, GetLag())

            If (Me.Page.Master IsNot Nothing AndAlso
                Me.Page.Master.GetType().Name = "root2_master") Then

                Dim itm As DevExpress.Web.ASPxMenu.MenuItem = mnuNavi.Items.FindByName("111")
                If (itm IsNot Nothing) Then
                    itm.NavigateUrl = Regex.Replace(itm.NavigateUrl, "default.aspx", "default2.aspx", RegexOptions.IgnoreCase)
                    If (Request.Url.AbsolutePath.IndexOf("default2.aspx", StringComparison.OrdinalIgnoreCase) > -1) Then
                        itm.Selected = True
                    End If
                End If
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try


        'Try
        '    pmb1.LoadLAG()
        'Catch ex As Exception
        '    WebErrorMessageBox(Me, ex, "")
        'End Try


        Try
            LoadLAG_LoginControl()
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


    Protected Sub LoadLAG_LoginControl()
        Try
            '  Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            Dim MainLogin As System.Web.UI.WebControls.Login = ucLogin1.LoginControl
            'MainLogin.UserNameLabelText = CurrentPageData.GetCustomString("txtUserNameLabelText")
            'MainLogin.PasswordLabelText = CurrentPageData.GetCustomString("txtPasswordLabelText")
            'MainLogin.RememberMeText = CurrentPageData.GetCustomString("txtRememberMeText")
            'MainLogin.LoginButtonText = CurrentPageData.GetCustomString("txtLoginButtonText")
            'MainLogin.UserNameRequiredErrorMessage = CurrentPageData.GetCustomString("txtUserNameRequiredErrorMessage")
            MainLogin.TitleText = CurrentPageData.GetCustomString("txtLoginTitleText")

            Dim lbForgotPassword As ASPxHyperLink = MainLogin.FindControl("lbForgotPassword")
            If (lbForgotPassword IsNot Nothing) Then
                lbForgotPassword.Text = CurrentPageData.GetCustomString("lbForgotPassword")
            End If

            Dim lbLoginFB As ASPxHyperLink = MainLogin.FindControl("lbLoginFB")
            If (lbLoginFB IsNot Nothing) Then
                lbLoginFB.Text = CurrentPageData.GetCustomString("lbLoginFB")
            End If

            Dim btnJoinToday As ASPxButton = MainLogin.FindControl("btnJoinToday")
            If (btnJoinToday IsNot Nothing) Then
                btnJoinToday.Text = CurrentPageData.GetCustomString("btnJoinToday")
                btnJoinToday.PostBackUrl = "~/Register2.aspx"
                If (Not String.IsNullOrEmpty(Request.QueryString("ReturnUrl"))) Then
                    btnJoinToday.PostBackUrl = btnJoinToday.PostBackUrl & "?ReturnUrl=" & HttpUtility.UrlEncode(Request.QueryString("ReturnUrl"))
                End If
            End If

            Dim UserName As ASPxTextBox = MainLogin.FindControl("UserName")
            If (UserName IsNot Nothing) Then
                'UserName.NullText = CurrentPageData.GetCustomString("UserName.NullText")
                UserName.ValidationSettings.RequiredField.ErrorText = CurrentPageData.GetCustomString("UserName.ValidationSettings.RequiredField.ErrorText")
            End If

            Dim Password As ASPxTextBox = MainLogin.FindControl("Password")
            If (Password IsNot Nothing) Then
                'Password.NullText = CurrentPageData.GetCustomString("Password.NullText")
                Password.ValidationSettings.RequiredField.ErrorText = CurrentPageData.GetCustomString("Password.ValidationSettings.RequiredField.ErrorText")
            End If

            Dim lblUsernameTop As Label = MainLogin.FindControl("lblUsernameTop")
            If (lblUsernameTop IsNot Nothing) Then
                lblUsernameTop.Text = CurrentPageData.GetCustomString("UserName.NullText")
            End If

            Dim lblPasswordTop As Label = MainLogin.FindControl("lblPasswordTop")
            If (lblPasswordTop IsNot Nothing) Then
                lblPasswordTop.Text = CurrentPageData.GetCustomString("Password.NullText")
            End If


            Dim btnLogin As ASPxButton = MainLogin.FindControl("btnLogin")
            If (btnLogin IsNot Nothing) Then
                btnLogin.Text = CurrentPageData.GetCustomString("btnLogin")
            End If

            Dim RememberMe As CheckBox = MainLogin.FindControl("RememberMe")
            If (RememberMe IsNot Nothing) Then
                RememberMe.Text = CurrentPageData.GetCustomString("RememberMe")
            End If


            Dim chkRememberMe As ASPxCheckBox = MainLogin.FindControl("chkRememberMe")
            If (chkRememberMe IsNot Nothing) Then
                chkRememberMe.Text = CurrentPageData.GetCustomString("RememberMe")
            End If


            '' controls found on login.aspx page
            Dim lblUserName As Literal = MainLogin.FindControl("lblUserName")
            If (lblUserName IsNot Nothing) Then
                lblUserName.Text = CurrentPageData.GetCustomString("lblUserName")
            End If


            Dim lblPassword As Literal = MainLogin.FindControl("lblPassword")
            If (lblPassword IsNot Nothing) Then
                lblPassword.Text = CurrentPageData.GetCustomString("lblPassword")
            End If

            Dim btnLoginNow As ASPxButton = MainLogin.FindControl("btnLoginNow")
            If (btnLoginNow IsNot Nothing) Then
                btnLoginNow.Text = CurrentPageData.GetCustomString("btnLoginNow")
                btnLoginNow.Text = btnLoginNow.Text.Replace("&nbsp;", " ")
            End If


        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Public Sub PopulateMenuData(ByVal menu As ListView, ByVal position As String, Optional ByVal visibleOnAdmistratorUserRole As Boolean? = Nothing, Optional ByVal visibleOnAfficateUserRole As Boolean? = Nothing, Optional ByVal visibleOnMemberUserRole As Boolean? = Nothing, Optional ByVal visibleOnPublicArea As Boolean? = Nothing, Optional ByVal VisibleOnResellerUserRole As Boolean? = Nothing)
        If menu Is Nothing Then Return

        Dim _dt As DataTable = GetMenuData(menu.UniqueID, position, visibleOnAdmistratorUserRole, visibleOnAfficateUserRole, visibleOnMemberUserRole, visibleOnPublicArea, VisibleOnResellerUserRole)

        menu.Items.Clear()
        menu.DataSource = _dt
        menu.DataBind()
    End Sub

    Public Function GetMenuData(ByVal menuId As String, ByVal position As String, Optional ByVal visibleOnAdmistratorUserRole As Boolean? = Nothing, Optional ByVal visibleOnAfficateUserRole As Boolean? = Nothing, Optional ByVal visibleOnMemberUserRole As Boolean? = Nothing, Optional ByVal visibleOnPublicArea As Boolean? = Nothing, Optional ByVal VisibleOnResellerUserRole As Boolean? = Nothing) As DataTable

        If Session(menuId + Session("LagID") + position) Is Nothing Then
            Using da As New dsMenuTableAdapters.SiteMenuItemsTableAdapter
                Using con As New SqlClient.SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("AppDBconnectionString").ConnectionString)
                    da.Connection = con
                    Using dt As dsMenu.SiteMenuItemsDataTable = da.GetData(position, visibleOnAdmistratorUserRole, visibleOnAfficateUserRole, visibleOnMemberUserRole, visibleOnPublicArea, VisibleOnResellerUserRole)
                        Session(menuId + Session("LagID") + position) = dt
                    End Using
                End Using
            End Using
           
        End If


        Using _dt As New DataTable()

     
        For Each row As dsMenu.SiteMenuItemsRow In DirectCast(Session(menuId + Session("LagID") + position), dsMenu.SiteMenuItemsDataTable)

            If (_dt.Columns.Count = 0) Then
                _dt.Columns.Add("Text", String.Empty.GetType())
                _dt.Columns.Add("Name", row.SiteMenuItemID.GetType())
                _dt.Columns.Add("ImageUrl", String.Empty.GetType())
                _dt.Columns.Add("NavigateUrl", String.Empty.GetType())
                _dt.Columns.Add("ToolTip", String.Empty.GetType())
                _dt.Columns.Add("CssClass", String.Empty.GetType())
            End If


            Dim _text As String = ""
            Dim _toolTip As String = ""

            With row

                ' by default use english
                _text = .US
                _toolTip = .ToolTipUS

                Select Case Session("LagID")
                    Case "GR"
                        If .GR.Length > 0 Then
                            _text = .GR
                            _toolTip = .ToolTipGR
                        End If
                    Case "HU"
                        If .HU.Length > 0 Then
                            _text = .HU
                            _toolTip = .ToolTipHU
                        End If
                    Case "ES"
                        If .ES.Length > 0 Then
                            _text = .ES
                            _toolTip = .ToolTipES
                        End If
                    Case "DE"
                        If .DE.Length > 0 Then
                            _text = .DE
                            _toolTip = .ToolTipDE
                        End If
                    Case "RO"
                        If .RO.Length > 0 Then
                            _text = .RO
                            _toolTip = .ToolTipRO
                        End If
                    Case "TU"
                        If .TU.Length > 0 Then
                            _text = .TU
                            _toolTip = .ToolTipTU
                        End If
                    Case "IT"
                        If .IT.Length > 0 Then
                            _text = .IT
                            _toolTip = .ToolTipIT
                        End If
                    Case "IL"
                        If .IL.Length > 0 Then
                            _text = .IL
                            _toolTip = .ToolTipIL
                        End If
                    Case "FR"
                        If .FR.Length > 0 Then
                            _text = .FR
                            _toolTip = .ToolTipFR
                        End If
                    Case "AL"
                        If Not .IsALNull() AndAlso .AL.Length > 0 Then
                            _text = .AL
                            _toolTip = .ToolTipAL
                        End If
                    Case "TR"
                        If .TR.Length > 0 Then
                            _text = .TR
                            _toolTip = .ToolTipTR
                        End If
                End Select

            End With


            Dim _dr As DataRow = _dt.NewRow()

            _dr("Name") = row.SiteMenuItemID
            _dr("ImageUrl") = row.ImageURL
            _dr("NavigateUrl") = row.NavigateURL
            _dr("Text") = _text
            _dr("ToolTip") = _toolTip
            _dt.Rows.Add(_dr)
        Next

            Return _dt
        End Using

    End Function

    Private Function SetSessionCity(ByRef _dt As DataTable) As Boolean
        Dim re As Boolean = False
        If _dt.Rows.Count > 0 Then
            Dim lat As Double = _dt.Rows(0)("latitude")
            Dim lng As Double = _dt.Rows(0)("longitude")
            'ShowUserMap(lat, lng, radius, zoom)

            Session("GEO_COUNTRY_CITY") = _dt.Rows(0)("city")
            Session("GEO_COUNTRY_LATITUDE") = lat
            Session("GEO_COUNTRY_LONGITUDE") = lng
            Session("GEO_COUNTRY_POSTALCODE") = _dt.Rows(0)("postcode")
            re = True
        End If
        Return re
    End Function
    Public Sub ShowUserMap()
        Try


            '    Dim zoom As Integer = 9
            '     Dim radius As Integer = 10


            Dim b As Boolean = False
            If (Not String.IsNullOrEmpty(Session("GEO_COUNTRY_POSTALCODE"))) Then
                Using dt = clsGeoHelper.GetGEOByZip(Session("GEO_COUNTRY_CODE"), Session("GEO_COUNTRY_POSTALCODE"), Session("LAGID"))
                    b = SetSessionCity(dt)
                End Using
            ElseIf (Not String.IsNullOrEmpty(Session("GEO_COUNTRY_LATITUDE")) AndAlso Not String.IsNullOrEmpty(Session("GEO_COUNTRY_LONGITUDE"))) Then
                Dim sLAT As String = Session("GEO_COUNTRY_LATITUDE")
                Dim sLON As String = Session("GEO_COUNTRY_LONGITUDE")

                Dim periodLAT As Integer = sLAT.IndexOf(".")
                If (Len(sLAT) > periodLAT + 3) Then sLAT = sLAT.Remove(periodLAT + 3)

                Dim periodLON As Integer = sLON.IndexOf(".")
                If (Len(sLON) > periodLON + 3) Then sLON = sLON.Remove(periodLON + 3)

                Using dt = clsGeoHelper.GetGEOWithLatitudeAndLongitude(Session("GEO_COUNTRY_CODE"), sLAT, sLON, Session("LAGID"))
                    b = SetSessionCity(dt)
                End Using

            ElseIf (Not String.IsNullOrEmpty(Session("GEO_COUNTRY_CITY"))) Then
                Using dt = clsGeoHelper.GetGEOWithCity(Session("GEO_COUNTRY_CODE"), Session("GEO_COUNTRY_CITY"))
                    b = SetSessionCity(dt)
                End Using

            End If

            If Not b Then
                Using dt = clsGeoHelper.GetGEOByZip("GR", "10431", Session("LAGID"))
                    SetSessionCity(dt)
                End Using
            
            End If


        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        Finally
        End Try

    End Sub


    Private Sub SetLanguageLinks()
        Try
            If (clsLanguageHelper.HasCountryConfig(Session("GEO_COUNTRY_CODE"))) Then
                Dim cc As clsLanguageHelper.CountryConfig = clsLanguageHelper.GetCountryConfig(Session("GEO_COUNTRY_CODE"))
                For itms = cmbLag.Items.Count - 1 To 0 Step -1
                    If (cc.ShowLangs.Contains(cmbLag.Items(itms).Value)) Then
                        Continue For
                    Else
                        cmbLag.Items.RemoveAt(itms)
                    End If
                Next
            End If

            Dim url As String = Request.Url.AbsolutePath
            Dim attributes As New Dictionary(Of String, String)
            For Each itm As ListEditItem In cmbLag.Items
                Select Case itm.Value
                    Case "US"
                        If (Not itm.Text.StartsWith("<a")) Then
                            url = LanguageHelper.GetNewLAGUrl(itm.Index)
                            If (Not Regex.IsMatch(url, "[?&]lang=1", RegexOptions.IgnoreCase)) Then
                                url = If(url IsNot Nothing, url, "") & If(url.IndexOf("?") > -1, "&", "?") & "lang=1"
                            End If
                            attributes.Clear()
                            attributes.Add("class", itm.Value)
                            itm.Text = clsHTMLHelper.RenderLinkHtml(url, itm.Text, attributes)
                            itm.ImageUrl = "https://cdn.goomena.com/Images/spacer10.png"
                        End If
                    Case "ES"
                        If (Not itm.Text.StartsWith("<a")) Then
                            url = LanguageHelper.GetNewLAGUrl(itm.Index)
                            If (Not Regex.IsMatch(url, "[?&]lang=1", RegexOptions.IgnoreCase)) Then
                                url = If(url IsNot Nothing, url, "") & If(url.IndexOf("?") > -1, "&", "?") & "lang=1"
                            End If
                            attributes.Clear()
                            attributes.Add("class", itm.Value)
                            itm.Text = clsHTMLHelper.RenderLinkHtml(url, itm.Text, attributes)
                            itm.ImageUrl = "https://cdn.goomena.com/Images/spacer10.png"
                        End If
                    Case "GR"
                        If (Not itm.Text.StartsWith("<a")) Then
                            url = LanguageHelper.GetNewLAGUrl(itm.Index)
                            If (Not Regex.IsMatch(url, "[?&]lang=1", RegexOptions.IgnoreCase)) Then
                                url = If(url IsNot Nothing, url, "") & If(url.IndexOf("?") > -1, "&", "?") & "lang=1"
                            End If
                            attributes.Clear()
                            attributes.Add("class", itm.Value)
                            itm.Text = clsHTMLHelper.RenderLinkHtml(url, itm.Text, attributes)
                            itm.ImageUrl = "https://cdn.goomena.com/Images/spacer10.png"
                        End If
                    Case "TR"
                        If (Not itm.Text.StartsWith("<a")) Then
                            url = LanguageHelper.GetNewLAGUrl(itm.Index)
                            If (Not Regex.IsMatch(url, "[?&]lang=1", RegexOptions.IgnoreCase)) Then
                                url = If(url IsNot Nothing, url, "") & If(url.IndexOf("?") > -1, "&", "?") & "lang=1"
                            End If
                            attributes.Clear()
                            attributes.Add("class", itm.Value)
                            itm.Text = clsHTMLHelper.RenderLinkHtml(url, itm.Text, attributes)
                            itm.ImageUrl = "https://cdn.goomena.com/Images/spacer10.png"
                        End If
                End Select
            Next

            'url = url.TrimEnd("&"c)

            'lnkEnglish.NavigateUrl = url & "lag=US"
            'lnkGerman.NavigateUrl = url & "lag=DE"
            'lnkGreek.NavigateUrl = url & "lag=GR"
            'lnkHungarian.NavigateUrl = url & "lag=HU"
            'cmbLag.DisplayFormatString = "<a href=""/Default.aspx?lag=US"">English</a>"


        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        'Try

        '    'lnkEnglish()
        '    'lnkGerman
        '    'lnkGreek()
        '    'lnkHungarian()

        '    'lnkEnglish.Visible = AppUtils.IsSupportedLAG("US")
        '    'lnkGerman.Visible = AppUtils.IsSupportedLAG("DE")
        '    'lnkGreek.Visible = AppUtils.IsSupportedLAG("GR")
        '    'lnkHungarian.Visible = AppUtils.IsSupportedLAG("HU")

        'Catch ex As Exception
        '    WebErrorMessageBox(Me, ex, "")
        'End Try
    End Sub


    Private __IsLagChanged As Integer = -1
    Public Function VerifyLagProps() As Boolean
        If (Request.Form("__EVENTTARGET") = cmbLag.UniqueID) Then

            __IsLagChanged = IIf((Request.Form("__EVENTTARGET") = cmbLag.UniqueID), 1, 0)
            ModifyApplicationForLanguageChanges()

        ElseIf (Not Page.IsPostBack AndAlso Not String.IsNullOrEmpty(Me.RequestedLAGID)) Then

            __IsLagChanged = IIf((Me.RequestedLAGID <> Session("LAGID")), 1, 0)
            ModifyApplicationForLanguageChanges()

        End If
        Return (__IsLagChanged = 1)
    End Function

    Private Function GetChangedLang() As String
        Dim changedLag As String = Session("LagID")

        ' check if lang was changed, 
        ' 1. by user - lang=1. Check query param and url folder
        ' 2. by url folder - /es/, /gr/ etc
        ' 3. by query param - lag=es

        Dim getLagFromURL As String = clsLanguageHelper.GetLAGFromURL(Request.Url.ToString())
        getLagFromURL = If(String.IsNullOrEmpty(getLagFromURL), "", getLagFromURL.ToUpper())

        Dim getLagFromParam As String = clsLanguageHelper.GetLAGFromURLParam(Request.QueryString("lag"))
        getLagFromParam = If(String.IsNullOrEmpty(getLagFromParam), "", getLagFromParam.ToUpper())


        If (getLagFromURL = "US" AndAlso
            Request.QueryString("lang") = "1" AndAlso
            getLagFromURL <> changedLag) Then

            ' case 1.1: lang is US and defenetly lang was changed by user, with combobox
            changedLag = getLagFromURL

        ElseIf (String.IsNullOrEmpty(getLagFromURL) AndAlso
            Request.QueryString("lang") = "1" AndAlso
            getLagFromURL <> changedLag) Then

            ' case 1.2: lang differs from US and defenetly lang was changed by user, with combobox
            changedLag = getLagFromURL

        ElseIf (String.IsNullOrEmpty(getLagFromParam) AndAlso
                Request.QueryString("lang") = "1" AndAlso
                getLagFromParam <> changedLag) Then

            ' case 2.1: lang is from query string, defenetly lang was changed by user, with combobox
            changedLag = getLagFromParam

        ElseIf (Me.RequestedLAGID = "US" AndAlso
                            Not String.IsNullOrEmpty(Request.QueryString("lag")) AndAlso
                            clsLanguageHelper.IsSupportedLAG(Request.QueryString("LAG"), Session("GEO_COUNTRY_CODE")) AndAlso
                            Me.RequestedLAGID <> Session("LagID")) Then

            ' case 2:

        End If

        Return changedLag
    End Function



    Private Sub ucLogin1_LoggingInFailed(ByRef sender As Object, ByRef e As LoggingInFailedEventArgs)
        Dim loginctl As Dating.Server.Site.Web.ucLogin = sender
        If (loginctl.LogginFailReason = LogginFailReasonEnum.UserDeleted) Then
            e.Handled = True

            Dim MainLogin As System.Web.UI.WebControls.Login = ucLogin1.LoginControl

            Dim btnLogin As ASPxButton = MainLogin.FindControl("btnLogin")
            If (btnLogin IsNot Nothing) Then btnLogin.Visible = False

            Dim lblUsernameTopTD As HtmlControl = MainLogin.FindControl("lblUsernameTopTD")
            If (lblUsernameTopTD IsNot Nothing) Then lblUsernameTopTD.Visible = False

            Dim lblPasswordTopTD As HtmlControl = MainLogin.FindControl("lblPasswordTopTD")
            If (lblPasswordTopTD IsNot Nothing) Then lblPasswordTopTD.Visible = False

            Dim UserNameTD As HtmlControl = MainLogin.FindControl("UserNameTD")
            If (UserNameTD IsNot Nothing) Then UserNameTD.Visible = False

            Dim PasswordTD As HtmlControl = MainLogin.FindControl("PasswordTD")
            If (PasswordTD IsNot Nothing) Then PasswordTD.Visible = False

            Dim chkRememberMeTD As HtmlControl = MainLogin.FindControl("chkRememberMeTD")
            If (chkRememberMeTD IsNot Nothing) Then chkRememberMeTD.Visible = False

            Dim lbForgotPasswordTD As HtmlControl = MainLogin.FindControl("lbForgotPasswordTD")
            If (lbForgotPasswordTD IsNot Nothing) Then lbForgotPasswordTD.Visible = False

            Dim lblErrorUserDeleted As Label = MainLogin.FindControl("lblErrorUserDeleted")
            If (lblErrorUserDeleted IsNot Nothing) Then
                lblErrorUserDeleted.Text = e.ErrorMessage
                lblErrorUserDeleted.Visible = True
            End If

        End If
    End Sub

    Private Sub Page_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        If (clsCurrentContext.VerifyLogin()) Then
            CssClassVerifyLogin = CssClassVerifyLogin & "no-reg-box"
        End If
    End Sub



End Class
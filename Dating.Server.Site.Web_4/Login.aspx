﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Root.Master"
    CodeBehind="Login.aspx.vb" Inherits="Dating.Server.Site.Web.Login" %>
<%@ Register Src="Security/UserControls/ucLogin.ascx" TagName="ucLogin" TagPrefix="uc1" %>
<%@ Register TagPrefix="recaptcha" Namespace="Recaptcha" Assembly="Recaptcha" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/v1/css/main3.css?v=7" rel="stylesheet" type="text/css" />
<%--
    <!--[if lt IE 9]>
		<script src="Scripts/ie9.js">IE7_PNG_SUFFIX = ".png";</script>
	<![endif]-->
    <!--[if lt IE 8]>
	    <link rel="stylesheet" type="text/css" media="screen, projection" charset="utf-8" href="CSS/ie_fix.css">
	<![endif]-->
    <!--[if lte IE 7]>
	    <link rel="stylesheet" type="text/css" media="screen, projection" charset="utf-8" href="CSS/ie7_fix.css">
	<![endif]-->
--%>
    <style type="text/css">
        .pe_box { background: white; }
        .form-actions { background: white; }
        .lists { width: 440px; }
        .lists ul { list-style: none; padding-left: 0px; margin-left: 0px; }
        .lists ul li { list-style: none; }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
                    <%--<asp:UpdatePanel ID="updLogin" runat="server">
                        <ContentTemplate>--%>

    <div class="mid_bg pageContainer">
<div id="hp_top"><h2><asp:Literal ID="lblWelcome" runat="server">Login To Dating Deals</asp:Literal></h2></div>
<div id="hp_content">
    <div class="container_12">
        <div class="ab_content">
            <div class="ab_left">
                <div class="ab_block">
                    <div class="pe_wrapper">
                        <div class="pe_box">
                            <h2 style="text-shadow: none; border: none; font-family: Lucida Grande,Lucida Sans Unicode,Verdana,Arial,sans-serif;">
                                <asp:Label ID="lblHeader" runat="server" CssClass="loginHereTitle"/><span class="loginHereLink"><asp:HyperLink ID="lnkRestore" runat="server" NavigateUrl="~/RestorePassword.aspx" onclick="ShowLoading();">Restore Password</asp:HyperLink><b> | </b><asp:HyperLink ID="lnkCreateAccount" runat="server" NavigateUrl="~/Register.aspx" onclick="ShowLoading();">Create an account</asp:HyperLink></span></h2>

                            <div class="pe_form pe_login" style="background:white;">
                                <uc1:ucLogin ID="ucLogin1" runat="server">
                                    <LoginControl UserNameRequiredErrorMessage="Username or email is required." Width="100%">
                                        <LayoutTemplate>
                        <asp:Panel ID="Panel1" runat="server" class="pe_box" DefaultButton="btnLoginNow">
<dx:ASPxValidationSummary ID="valSumm" runat="server" 
    ValidationGroup="LoginGroup" RenderMode="BulletedList" 
    ShowErrorsInEditors="True">
    <Paddings PaddingBottom="10px" />
    <LinkStyle>
        <Font Size="14px"></Font>
    </LinkStyle>
</dx:ASPxValidationSummary>

<div style="margin:10px;text-align:center;"><dx:ASPxLabel ID="lblError" runat="server" Text="" 
        ForeColor="red" Visible="false" EnableViewState="false" EncodeHtml="False">
    </dx:ASPxLabel></div>

<table border="0" cellspacing="0" cellpadding="0" style="width:100%;">
<tr>
    <td colspan="2"><div style="height:7px;">&nbsp;</div></td>
</tr>
<tr>
    <td valign="middle"><asp:Label ID="lblUserName" runat="server" Text="Username or Email" CssClass="registerTableLabel" AssociatedControlID="UserName"/></td>
    <td valign="top">
<div class="form_right lfloat">
        <dx:ASPxTextBox ID="UserName" runat="server" CssClass="field" Width="310px" Font-Size="14px" Height="18px">
            <ValidationSettings ValidationGroup="LoginGroup"  ErrorDisplayMode="ImageWithTooltip" ErrorTextPosition="Left" ErrorText="" Display="Dynamic">
                <RequiredField ErrorText="Username or email is required." 
                    IsRequired="True" />
            </ValidationSettings>
            <Border BorderStyle="None" />
        </dx:ASPxTextBox>
</div>
        </td>
</tr>
<tr>
    <td colspan="2"><div style="height:7px;">&nbsp;</div></td>
</tr>
<tr>
<td valign="middle"><asp:Label ID="lblPassword" runat="server" Text="Password" CssClass="registerTableLabel" AssociatedControlID="Password"/></td>
<td valign="top">
    <dx:ASPxTextBox ID="Password" runat="server" CssClass="field" Width="310px" Password="True" Font-Size="14px" Height="18px">
        <ValidationSettings ValidationGroup="LoginGroup" ErrorDisplayMode="ImageWithTooltip" ErrorTextPosition="Left" ErrorText="" Display="Dynamic">
            <RequiredField ErrorText="Password is required." 
                IsRequired="True" />
        </ValidationSettings>
        <Border BorderStyle="None" />
    </dx:ASPxTextBox></td>
</tr>
<tr>
    <td colspan="2"><div style="height:7px;">&nbsp;</div></td>
</tr>
<tr>
<td valign="top" colspan="2" align="center">
    <dx:ASPxCheckBox ID="chkRememberMe" runat="server" Text="" EncodeHtml="False" Font-Size="14px">
                                            </dx:ASPxCheckBox></td>
</tr>
</table>

                                <div class="clear"></div>
                                <br />
                                <div style="margin: 15px auto;width: 318px;">
                                    <div style="text-align:center;margin-bottom:4px;">
                                        <asp:Label ID="lblFillRecaptcha" runat="server" Text=""  style="font-size: 14px;"/>
                                    </div>
                                    <div runat="server" id="pbTarget" visible="false"></div>  
                                    <asp:PlaceHolder ID="phRecaptcha" runat="server"></asp:PlaceHolder>
                                    <%--
                                    --- created from code ----
                                    
                                    <recaptcha:RecaptchaControl
                                        ID="recaptcha"
                                        runat="server"
                                        PublicKey="6LeqLOsSAAAAACbrctSmamfQ20Jc8BJrBynH-jKt"
                                        PrivateKey="6LeqLOsSAAAAAM4HuHzL6W4zmNtKYCOn1iS0AwK7"
                                        />--%>
                                </div>
                                <div class="form-actions">
                                    <asp:Button ID="btnLoginNow" runat="server" Text="Login now" 
                                        CausesValidation="True" CommandName="Login" ValidationGroup="LoginGroup" 
                                        BackColor="#74A554" ForeColor="White" Width="135px" Height="32px" Font-Size="16px"  />
                                    <%--
                                    <dx:ASPxButton ID="btnLoginNow" runat="server" CausesValidation="True" CommandName="Login"
                                        Text="Login now" ValidationGroup="LoginGroup" Native="True" EncodeHtml="False"
                                        EnableDefaultAppearance="False" BackColor="#74A554" ForeColor="White" Width="135px"
                                        EnableClientSideAPI="true"
                                        Height="32px" Font-Size="16px" />
                                        --%>
                                </div>
                                </asp:Panel>
                                        </LayoutTemplate>
                                    </LoginControl>
                                </uc1:ucLogin>
                            </div>

                        </div>
                    </div>
                </div>
				<div class="ab_block">
                    <dx:ASPxLabel ID="lbHTMLBody" runat="server" ClientIDMode="AutoID" EncodeHtml="False">
                    </dx:ASPxLabel>
				</div>
            </div>

            <div class="ab_right">
                <div class="ab_block test_ab">
                    <div class="lists">
                        <h2><asp:Label ID="lblHeaderRight" runat="server" CssClass="rightColumnTitle" Text="" /></h2>
                        <asp:Label ID="lblTextRight" runat="server" CssClass="rightColumnContent" Text="" />
                    </div>
                </div>
            </div>
			<%--<div class="ab_right">
				<div class="ab_block test_ab">
					<div class="lists">
					    <h2><asp:Literal ID="lblNotMemberQuestion" runat="server">Not a Member? Join Now</asp:Literal></h2>
					    <p style="margin: 0;"><asp:HyperLink ID="lnkCreateAccountRight" runat="server" NavigateUrl="~/Register.aspx" CssClass="btn btn-success btn-large" style="width: 270px; font-size: 20px;">Click Here To Join For Free</asp:HyperLink></p>
                        <asp:Literal ID="lblHtmlBodyRight" runat="server" />
					</div>
				</div>
			</div>--%>
			<div class="clear"></div>
		</div>
	</div>
</div>
</div>
                            <%--</ContentTemplate>
                    </asp:UpdatePanel>--%> 
</asp:Content>

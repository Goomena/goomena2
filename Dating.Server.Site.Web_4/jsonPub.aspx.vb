﻿Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Core.DLL
Imports System.Web.Script.Serialization
Imports System.IO
Imports System.Web.Script.Services

Public Class JSONPublic
    Inherits System.Web.UI.Page

    <System.Web.Services.WebMethod()> _
    Public Shared Sub SetOffsetTime(ByVal time As String)
        JSON.SetOffsetTime(time)
    End Sub


    <System.Web.Services.WebMethod()> _
    Public Shared Sub SetIsMobile(ByVal isMobile As String)
        JSON.SetIsMobile(isMobile)
    End Sub

    <System.Web.Services.WebMethod()> _
    Public Shared Sub SetClientData(ByVal time As String, ByVal isMobile As String)
        JSON.SetOffsetTime(time)
        JSON.SetIsMobile(isMobile)
    End Sub

End Class
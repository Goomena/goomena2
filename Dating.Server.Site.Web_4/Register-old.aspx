﻿<%@ Page Language="vb" AutoEventWireup="true" MasterPageFile="~/Root.Master" CodeBehind="Register-old.aspx.vb"
    Inherits="Dating.Server.Site.Web.Register" MaintainScrollPositionOnPostback="false" %>

<%@ Register Src="UserControls/DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<%@ Register Assembly="DevExpress.Web.v13.1, Version=13.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>
<%@ Register src="~/UserControls/LiveZillaPublic.ascx" tagname="LiveZillaPublic" tagprefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="/v1/Scripts/userAvailabilityCheck.js"></script>
    <style type="text/css">
        .rightsidediv { float: right; }
        .clear { clear: both; }
        .BulletedList { margin-left: 0px; padding-left: 25px; }
        li.BulletedListItem { }
        .BulletedListItem a { color: Red; text-decoration: none; border-bottom-color: #f70; border-bottom-width: 1px; border-bottom-style: dashed; }
        .lists { background: url(goomena-kostas/first-general/bg-reapet.jpg); }
        .registernewcontainer { background: white; width: 485px; position: relative; float: left; padding-left: 10px; padding-top: 0; padding-bottom: 55px; font-size: 13px; }
        .registernewcontainer li { list-style: none; }
        .registernewcontainer ul { padding-left: 0px; }
        .form-actions { position: absolute; bottom: 5px; right: 10px; }
        body { font-family: "Segoe UI" , "Arial"; font-weight: lighter; }
        .mainregistercon { background: url("//cdn.goomena.com/goomena-kostas/first-general/bg-reapet.jpg") repeat; border-top: 6px solid #be202f; padding-bottom: 100px; padding-top: 61px; }
        .regmidcont { margin-left: auto; margin-right: auto; width: 1080px; position: relative; }
        .lblform { float: left; margin-right: 5px; width: 135px; text-align: right; }
        .divform { margin-bottom: 3px; width: 480px; }
        .center { cursor: pointer; }
        .loadingIndicator { background: url('Images2/ajax-loader.gif') no-repeat right center; box-sizing: border-box; padding-right: 20px; }
        .mailValidation, .usernameValidation, .passwordValidation { text-align: left;width:100% !important; padding-left:20px;margin-bottom:20px;display:none;}
        ul.dxvsL{margin-left: 22px !important;}
        ul.dxvsL li.dxvsE{margin-bottom: 7px;}

        .pe_form .divform .dxeTextBox input.dxeEditArea {height: 17px !important; line-height:17px !important; }
        .pe_form .divform .dxeButtonEdit input.dxeEditArea {height: 17px !important; line-height:17px !important; }
        .pe_form .form_right .dxeButtonEdit input.dxeEditArea {height: 17px !important; line-height:17px !important; }
        .google-connect { display:none;}
    </style>
    <script type="text/javascript" src="/v1/Scripts/register.js?v=111"></script>
    <script type="text/javascript">
        // <![CDATA[
        //msg_ClickToExpand = "<%= globalStrings.GetCustomString("msg_ClickToExpand", GetLag() )  %>";
        //msg_ClickToCollapse = "<%= globalStrings.GetCustomString("msg_ClickToCollapse", GetLag()) %>";

        function showZip() {
            var zip = jQuery('#<%= TrShowRegion.ClientID %>');
            var region = jQuery('#<%= TrRegion.ClientID %>');
            var city = jQuery('#<%= TrCity.ClientID %>');

            //zip.show()
            region.hide()
            city.hide()

            var status = jQuery('#<%= hdfLocationStatus.ClientID %>');
            status.val("zip")
        }

        function showRegions() {
            //var zip = jQuery('#< %= liLocationsZip.ClientID %>');
            var zip = jQuery('#<%= TrShowRegion.ClientID %>');
            var region = jQuery('#<%= TrRegion.ClientID %>');
            var city = jQuery('#<%= TrCity.ClientID %>');

            zip.hide()
            region.show()
            city.show()
            var status = jQuery('#<%= hdfLocationStatus.ClientID %>');
            status.val("region")
        }

    //runColapse("<%= globalStrings.GetCustomString("msg_ClickToExpand", GetLag() ) %>",  "<%= globalStrings.GetCustomString("msg_ClickToCollapse", GetLag()) %>")
        jQuery(function ($) {
            $('.pe_box1').each(function () {
                var pe_box = this;

                $('.collapsibleHeader', pe_box).click(function () {
                    $('.pe_form', pe_box).toggle('fast', function () {
                        // Animation complete.
                        var isVisible = ($('.pe_form', pe_box).css('display') == 'none');
                        if (isVisible) {
                            $('.togglebutton', pe_box).text("+");
                            $('.collapsibleHeader', pe_box).attr("title", "<%= globalStrings.GetCustomString("msg_ClickToExpand", GetLag() )  %>");
                        }
                        else {
                            $('.togglebutton', pe_box).text("-");
                            $('.collapsibleHeader', pe_box).attr("title", "<%= globalStrings.GetCustomString("msg_ClickToCollapse", GetLag()) %>");
                        }
                    });

                });
            });
        });

        function OpenGoogleLoginPopup() {
            jsLoad('<%= ResolveUrl("~/LoginWithGoogle.aspx")%>');
        }
    // ]]> 
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
<script type="text/javascript" >
    passwordValidation_DiffThanLogin = '<%= Dating.Server.Site.Web.AppUtils.JS_PrepareString(Mybase.CurrentPageData.GetCustomString("Error.Password.DifferentThanUsername"))  %>';
</script>
                    <asp:UpdatePanel ID="updRegister" runat="server">
                        <ContentTemplate>

    <div class="mainregistercon">
        <div class="regmidcont">
            <div class="registernewcontainer">
                <div class="clear">
                </div>
                <asp:Panel ID="Panel1" runat="server" DefaultButton="btnRegister">
                    <h2 style="text-shadow: none; padding-top: 0px; padding-left: 30px; border: none;
                        font-family: Lucida Grande,Lucida Sans Unicode,Verdana,Arial,sans-serif;">
                        <asp:Label ID="msg_PrompToBegin" runat="server" CssClass="loginHereTitle" /></h2>
                    <div style="margin-bottom: 14px; margin-left: 257px; margin-top: -31px;">
                        <div class="rfloat">
                            <dx:ASPxHyperLink ID="lbLoginFB" runat="server" CssClass="js facebook-login" EncodeHtml="false"
                                Text="Login With Facebook" EnableDefaultAppearance="False" EnableTheming="False"
                                NavigateUrl="~/Register.aspx?log=fb" ImageUrl="images/signup_fb.png" ClientSideEvents-Click="function(){ShowLoading();}" />
                        </div>
                        <div class="clear">
                        </div>
                    </div>
                    <asp:BulletedList ID="serverValidationList" runat="server" Font-Size="14px" ForeColor="Red"
                        DisplayMode="HyperLink" CssClass="BulletedList">
                    </asp:BulletedList>
                    <dx:ASPxValidationSummary ID="valSumm" runat="server" ValidationGroup="RegisterGroup"
                        RenderMode="BulletedList" ShowErrorsInEditors="True" EncodeHtml="False">
                        <Paddings PaddingBottom="10px" />
                        <LinkStyle>
                            <Font Size="14px"></Font>
                        </LinkStyle>
                        <ClientSideEvents VisibilityChanged="valSumm_VisibilityChanged"/>
                    </dx:ASPxValidationSummary>
                    <asp:Panel ID="pnlContinueMessageCompleteForm" runat="server" CssClass="alert alert-success"
                        Visible="False">
                        <br /> 
                        <div style="padding-top:10px;padding-bottom:10px;">
                        <asp:Label ID="lblContinueMessageCompleteForm" runat="server" Text="" Font-Bold="true"></asp:Label></div>
                    </asp:Panel>

                    <div class="pe_form pe_login">
                        <div class="reglogintable">
                            <div runat="server" id="TrGender" style="margin-left: 135px;">
                                <dx:ASPxRadioButtonList ID="rblGender" runat="server" RepeatLayout="Flow" ClientInstanceName="RegisterGenderRadioButtonList">
                                    <Paddings PaddingLeft="0px" />
                                    <ValidationSettings ValidationGroup="RegisterGroup" ErrorDisplayMode="ImageWithTooltip"
                                        ErrorTextPosition="Left" Display="Dynamic">
                                        <RequiredField ErrorText="" IsRequired="True" />
                                    </ValidationSettings>
                                    <Border BorderStyle="None" />
                                    <RadioButtonStyle BackgroundImage-HorizontalPosition="center" HorizontalAlign="Center">
                                        <BackgroundImage HorizontalPosition="center"></BackgroundImage>
                                    </RadioButtonStyle>
                                </dx:ASPxRadioButtonList>
                            </div>
                            <div runat="server" id="TrAccountType" visible="False">
                                <asp:Label ID="msg_WhoIs" runat="server" Text="" class="registerTableLabel" AssociatedControlID="cbAccountType" />
                                <dx:ASPxComboBox ID="cbAccountType" runat="server" ClientInstanceName="RegisterAccountTypeComboBox"
                                    EncodeHtml="False">
                                    <ClientSideEvents Validation="function(s, e) {
if(s.GetSelectedIndex()==0) e.isValid=false;
}" />
                                    <ValidationSettings ValidationGroup="RegisterGroup" ErrorDisplayMode="ImageWithTooltip"
                                        ErrorTextPosition="Left" ErrorText="" Display="Dynamic">
                                        <RequiredField ErrorText="" IsRequired="True" />
                                    </ValidationSettings>
                                </dx:ASPxComboBox>
                            </div>
                            <div runat="server" id="TrMyEmail" class="divform">
                                <asp:Label ID="msg_MyEmail" runat="server" Text="" class="registerTableLabel" AssociatedControlID="txtEmail" CssClass="lblform" />
                                <dx:ASPxTextBox ID="txtEmail" runat="server" ClientInstanceName="RegisterEmailTextBox"
                                    Width="309px" CssClass="txtEmail">
                                    <ValidationSettings ValidationGroup="RegisterGroup" ErrorDisplayMode="ImageWithTooltip"
                                        ErrorTextPosition="Left" Display="Dynamic">
                                        <RequiredField IsRequired="True" ErrorText="" />
                                        <RegularExpression ValidationExpression="" ErrorText="" />
                                    </ValidationSettings>
                                </dx:ASPxTextBox>
                                <asp:Label ID="mailValidation" runat="server" Text="" CssClass="mailValidation"></asp:Label>
                            </div>
                            <div runat="server" id="TrMyUsername" class="divform">
                                <asp:Label ID="msg_MyUsername" runat="server" Text="" class="registerTableLabel"
                                    AssociatedControlID="cbAccountType" CssClass="lblform" />
                                <dx:ASPxTextBox ID="txtLogin" runat="server" ClientInstanceName="RegisterLoginTextBox"
                                    Width="309px" CssClass="txtLogin">
                                    <ValidationSettings ValidationGroup="RegisterGroup" ErrorDisplayMode="ImageWithTooltip"
                                        ErrorTextPosition="Left" ErrorText="" Display="Dynamic">
                                        <RequiredField ErrorText="" IsRequired="True" />
                                        <RegularExpression ValidationExpression="^(?=.*\d)(?=(.*[a-zA-Z]){2}).[^&:]{3,12}$" ErrorText="Το όνομα χρήστη πρέπει να αποτελείται απο 3 μέχρι 12 χαρακτήρες και να περιλαμβάνει τουλάχιστον ένα γράμμα και έναν αριθμό." />
                                    </ValidationSettings>
                                </dx:ASPxTextBox>
                                <asp:Label ID="usernameValidation" runat="server" Text="" CssClass="usernameValidation"></asp:Label>
                            </div>
                            <div runat="server" id="TrMyPassword" class="divform">
                                <asp:Label ID="msg_MyPassword" runat="server" Text="" class="registerTableLabel"
                                    AssociatedControlID="txtPasswrd" CssClass="lblform" />
                                <dx:ASPxTextBox ID="txtPasswrd" runat="server" Password="True" ClientInstanceName="RegisterPassword"
                                    Width="309px" CssClass="txtPasswrd">
                                    <ValidationSettings ValidationGroup="RegisterGroup" ErrorDisplayMode="ImageWithTooltip"
                                        ErrorText="" ErrorTextPosition="Left" Display="Dynamic">
                                        <RequiredField ErrorText="" IsRequired="True" />
                                        <RegularExpression ValidationExpression="^(?=.*\d)(?=.*[a-zA-Z]).{3,20}$" ErrorText="Ο κωδικός πρέπει να αποτελείται απο 3 μέχρι 20 χαρακτήρες και να περιλαμβάνει τουλάχιστον ένα γράμμα και έναν αριθμό." />
                                    </ValidationSettings>
                                </dx:ASPxTextBox>
                                <asp:Label ID="passwordValidation" runat="server" Text="" CssClass="passwordValidation"></asp:Label>
                            </div>
                            <div runat="server" id="TrMyPasswordConfirm" class="divform">
                                <asp:Label ID="msg_MyPasswordConfirm" runat="server" Text="" class="registerTableLabel"
                                    AssociatedControlID="txtPasswrd1Conf" CssClass="lblform" />
                                <dx:ASPxTextBox ID="txtPasswrd1Conf" runat="server" Password="True" ClientInstanceName="RegisterPasswordConfirm"
                                    Width="309px">
                                    <ClientSideEvents Validation="function(s, e) {e.isValid = (s.GetText() == RegisterPassword.GetText());}" />
                                    <ValidationSettings ValidationGroup="RegisterGroup" ErrorDisplayMode="ImageWithTooltip"
                                        ErrorTextPosition="Left" ErrorText="" Display="Dynamic">
                                    </ValidationSettings>
                                </dx:ASPxTextBox>
                            </div>
                            <div runat="server" id="TrMyBirthdate" class="divform">
                                <asp:Label ID="msg_MyBirthdate" runat="server" Text="" class="registerTableLabel" CssClass="lblform" />
                                <uc1:DateControl Height="28px" FontSize="14px" ID="ctlDate" runat="server" DateTime='<%# Bind("Birthday") %>' YearsFromNow="-18" YearsTillNow="100"  />
                            </div>
                        </div>
                        <dx:ASPxCallbackPanel Style="background: white;" ID="cbpnlZip" runat="server" ClientInstanceName="cbpnlZip"
                            CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" ShowLoadingPanel="False"
                            ShowLoadingPanelImage="False">
                            <ClientSideEvents EndCallback="function(s, e) {HideLoading();}" CallbackError="function(s, e) {HideLoading();}" BeginCallback="function(s, e) {ShowLoading();}" />

                            <LoadingPanelImage Url="~/App_Themes/DevEx/Web/Loading.gif">
                            </LoadingPanelImage>
                            <LoadingPanelStyle ImageSpacing="5px">
                            </LoadingPanelStyle>
                            <PanelCollection>
                                <dx:PanelContent ID="PanelContent1" runat="server" SupportsDisabledAttribute="True">
                                    <asp:HiddenField ID="hdfLocationStatus" runat="server" />
                                    <div>
                                        <div runat="server" id="TrCountry" class="divform">
                                            <asp:Label ID="msg_CountryText" runat="server" Text="" class="registerTableLabel"
                                                AssociatedControlID="cbCountry" CssClass="lblform" />
                                            <dx:ASPxComboBox ID="cbCountry" runat="server" Width="310px" Font-Size="14px" Height="18px"
                                                EncodeHtml="False" IncrementalFilteringMode="StartsWith">
                                                <ClientSideEvents SelectedIndexChanged="function(s, e) {  OnCountryChanged(s); }"
                                                    EndCallback="function(s, e) {
	LoadingPanel.Hide();
}" />
                                                <ValidationSettings CausesValidation="True" Display="Dynamic" ValidationGroup="RegisterGroup">
                                                </ValidationSettings>
                                            </dx:ASPxComboBox>
                                        </div>
                                        <div runat="server" id="TrShowRegion" style="display:none;">
                                            <span class="zip_list">
                                                <asp:LinkButton ID="msg_SelectRegion" runat="server" CssClass="js" OnClientClick="showRegions();return false;" /></span>
                                        </div>
                                        <div runat="server" id="TrRegion"  class="divform">
                                            <asp:Label ID="msg_RegionText" runat="server" Text="" class="registerTableLabel"
                                                AssociatedControlID="cbRegion" CssClass="lblform" />
                                            <dx:ASPxComboBox ID="cbRegion" runat="server" Width="310px" Font-Size="14px" Height="18px"
                                                EncodeHtml="False" EnableSynchronization="False" ClientInstanceName="cbRegion"
                                                IncrementalFilteringMode="StartsWith" DataSourceID="sdsRegion" TextField="region1"
                                                ValueField="region1">
                                                <ClientSideEvents SelectedIndexChanged="function(s, e) {
OnRegionChanged(s);
}" EndCallback="function(s, e) {
	LoadingPanel.Hide();
}" />
                                                <ValidationSettings CausesValidation="True" Display="Dynamic" ValidationGroup="RegisterGroup">
                                                </ValidationSettings>
                                            </dx:ASPxComboBox>
                                            <asp:SqlDataSource ID="sdsRegion" runat="server" ConnectionString="<%$ ConnectionStrings:AppDBconnectionString %>">
                                            </asp:SqlDataSource>
                                        </div>
                                        <div runat="server" id="TrCity"  class="divform">
                                            <asp:Label ID="msg_CityText" runat="server" Text="" CssClass="lblform"
                                                AssociatedControlID="cbCity" />
                                            <dx:ASPxComboBox ID="cbCity" runat="server" Width="310px" Font-Size="14px" Height="18px"
                                                EncodeHtml="False" EnableSynchronization="False" ClientInstanceName="cbCity"
                                                IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" DataSourceID="sdsCity"
                                                TextField="city" ValueField="city">
                                                <ClientSideEvents SelectedIndexChanged="function(s, e) {
  OnCityChanged(s);
}" />
                                                <ValidationSettings CausesValidation="True" Display="Dynamic" ValidationGroup="RegisterGroup">
                                                </ValidationSettings>
                                            </dx:ASPxComboBox>
                                            <asp:SqlDataSource ID="sdsCity" runat="server" ConnectionString="<%$ ConnectionStrings:AppDBconnectionString %>">
                                            </asp:SqlDataSource>
                                        </div>
                                        <div runat="server" id="TrZip" class="divform">
                                            <asp:Label ID="msg_ZipCodeText" runat="server" CssClass="lblform" Style="height: 30px"
                                                for="zip" />
                                            <dx:ASPxComboBox ID="txtZip" runat="server" DataSourceID="zip">
                                    <ValidationSettings Display="Dynamic">
                                    </ValidationSettings>
                                </dx:ASPxComboBox>
                                <asp:SqlDataSource ID="zip" runat="server" ConnectionString="<%$ ConnectionStrings:AppDBconnectionString %>">
                            </asp:SqlDataSource>
                                        </div>
                                    </div>
                                </dx:PanelContent>
                            </PanelCollection>
                        </dx:ASPxCallbackPanel>
                        <div class="pe_box1" style="background: white;">
                            <h2 runat="server" id="h2PersonalTitle" style="width: 185px; font-size: 13px; margin-left:125px; font-style: normal; font-weight: normal; padding-top: 7px; padding-bottom: 7px;
                                padding-left: 10px; background: black; color: White;" class="collapsibleHeader"
                                title="">
                                <asp:HyperLink ID="lnkExpColl" runat="server" NavigateUrl="javascript:void(0);" class="togglebutton"
                                    ForeColor="White">+</asp:HyperLink>
                                <asp:Literal ID="msg_PersonalTitle" runat="server" /></h2>
                            <div runat="server" id="pe_formPersonalInfo" class="pe_form">
                                <ul class="defaultStyle" style="list-style: none;">
                                    <li>
                                        <asp:Label ID="msg_PersonalHeight" runat="server" Text="" CssClass="lblform"
                                            AssociatedControlID="cbHeight" />
                                        <div class="form_right lfloat" style="margin-bottom: 4px;">
                                            <dx:ASPxComboBox ID="cbHeight" runat="server" EncodeHtml="False" Width="310px" Font-Size="14px"
                                                Height="18px" ClientInstanceName="cbHeight">
                                            </dx:ASPxComboBox>
                                        </div>
                                    </li>
                                    <li>
                                        <asp:Label ID="msg_PersonalBodyType" runat="server" Text="" CssClass="lblform"
                                            AssociatedControlID="cbBodyType" />
                                        <div class="form_right lfloat" style="margin-bottom: 4px;">
                                            <dx:ASPxComboBox ID="cbBodyType" runat="server" Width="310px" Font-Size="14px" Height="18px"
                                                EncodeHtml="False" ClientInstanceName="cbBodyType">
                                            </dx:ASPxComboBox>
                                        </div>
                                    </li>
                                    <li>
                                        <asp:Label ID="msg_PersonalEyeClr" runat="server" Text="" CssClass="lblform"
                                            AssociatedControlID="cbEyeColor" />
                                        <div class="form_right lfloat" style="margin-bottom: 4px;">
                                            <dx:ASPxComboBox ID="cbEyeColor" runat="server" Width="310px" Font-Size="14px" Height="18px"
                                                EncodeHtml="False">
                                            </dx:ASPxComboBox>
                                        </div>
                                    </li>
                                    <li>
                                        <asp:Label ID="msg_PersonalHairClr" runat="server" Text="" CssClass="lblform"
                                            AssociatedControlID="cbHairClr" />
                                        <div class="form_right lfloat" style="margin-bottom: 4px;">
                                            <dx:ASPxComboBox ID="cbHairClr" runat="server" Width="310px" Font-Size="14px" Height="18px"
                                                EncodeHtml="False" ClientInstanceName="cbHairClr">
                                            </dx:ASPxComboBox>
                                        </div>
                                    </li>
                                </ul>
                                <div class="clear">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div runat="server" id="TrAgreements" style="margin-top:15px;">
                            <div class="form_right form_tos" >
                                <dx:ASPxCheckBox ID="cbAgreements" runat="server" Text="" ClientInstanceName="RegisterAgreementsCheckBox"
                                    EncodeHtml="False" Checked="false">
                                    <ClientSideEvents Validation="function(s, e) {
                                                    e.isValid = (s.GetChecked() == true);
                                                }" />
                                    <ValidationSettings ValidationGroup="RegisterGroup" ErrorDisplayMode="ImageWithTooltip"
                                        ErrorTextPosition="Left">
                                        <RequiredField ErrorText="" IsRequired="True" />
                                    </ValidationSettings>
                                </dx:ASPxCheckBox>
                                <dx:ASPxHyperLink ID="lnkToS" runat="server" Text="" NavigateUrl="" EncodeHtml="False"
                                    Visible="False">
                                </dx:ASPxHyperLink>
                            </div>
                        </div>
                    </div>

                    <div class="form-actions">
                        <div style="width: 100px;">
                            
                            <dx:ASPxButton ID="btnRegister" runat="server" CommandName="Insert" CssClass="center"
                                ValidationGroup="RegisterGroup" 
                                EncodeHtml="False" 
                                Font-Bold="True" 
                                Height="32px"
                                Font-Size="16px" 
                                Width="100px" 
                                HorizontalAlign="Center" VerticalAlign="Middle"
                                BackColor="#74A554" EnableDefaultAppearance="False" ForeColor="White" 
                                Cursor="pointer"
                                EnableClientSideAPI="True">
                                <Border BorderWidth="1px"></Border>
                            </dx:ASPxButton>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    

                </asp:Panel>
                <div class="clear">
                </div>
            </div>
            <div class="rightsidediv">
                <div class="lists">
                    <h2>
                        <asp:Label ID="lblHeaderRight" runat="server" CssClass="rightColumnTitle" Text="" /></h2>
                    <asp:Label ID="lblTextRight" runat="server" CssClass="rightColumnContent" Text="" />
                </div>
            </div>
            <div class="clear">
            </div>
        </div>
    </div>
    <div class="clear">
    </div>


                            <asp:HyperLink ID="lnkLogGoogle" runat="server" onclick="OpenGoogleLoginPopup()" CssClass="google-connect">Login with google</asp:HyperLink>

                            </ContentTemplate>
                    </asp:UpdatePanel> 
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cntBodyBottom" runat="server">
    <uc3:LiveZillaPublic ID="ctlLiveZillaPublic" runat="server" />
    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function (sender, args) {
            RegisterForm_LoadAsyncValidation()
        });
        RegisterForm_LoadAsyncValidation();
    </script>
</asp:Content>

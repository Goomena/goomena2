﻿<%@ Page Title="" Language="vb" AutoEventWireup="true" MasterPageFile="~/Root.Master" CodeBehind="Contact.aspx.vb" Inherits="Dating.Server.Site.Web.Contact" %>
<%@ Register src="UserControls/LiveZilla.ascx" tagname="LiveZilla" tagprefix="uc1" %>
<%@ Register src="UserControls/ContactControl.ascx" tagname="ContactControl" tagprefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<%--
<!--[if lt IE 9]>
	<script src="Scripts/ie9.js">IE7_PNG_SUFFIX = ".png";</script>
<![endif]-->
<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" media="screen, projection" charset="utf-8" href="CSS/ie_fix.css">
<![endif]-->
<!--[if lte IE 7]>
	<link rel="stylesheet" type="text/css" media="screen, projection" charset="utf-8" href="CSS/ie7_fix.css">
<![endif]-->
--%>
<style type="text/css">
    td.header { width: 1%; white-space: nowrap; padding: 0 20px 0 0px; vertical-align: text-top; display: block; text-align: left; }
    .clear { clear: both; }
    .clearboth { clear: both; }
    ul li { list-style: none; clear: both; display: block; height: 30px; margin-bottom: 10px; }
    ul li:after { clear: both; }
    ul li div.form_right { float: right; }
    /*.mosaiccontact { position: relative; width: 1440px; height: 459px; background: url("http://cdn.goomena.com/goomena-kostas/contact-us/contact-us-big.jpg") no-repeat; background-position: center; margin-top: 16px; margin-left: auto; margin-right: auto;}*/
    .mosaiccontact { position: relative; width: 988px; margin:16px auto 0;}
    .mosaiccontact .left{width:480px;float:left;}
    .mosaiccontact .right{width:480px;float:right;}
    .maincontact {height: 590px;margin-bottom: 100px;}
    .contactuscontainer { margin-left: auto; margin-right: auto; width: 988px; height: 380px; position: relative; padding-top: 45px; }
    .contactuscontainer h1 { font-size: 24px; font-weight: lighter; width: 400px; margin: 0; border-bottom: 1px solid #c7c7c7; }
    .milisemasform { width: 530px; display: inline-block; }
    .milisemas { position: absolute; right: 50px; top: 45px; width: 256px; height: 375; }
    .milisemas h1 { width: 212px; border-bottom: 1px solid #c7c7c7; font-size: 24px; font-weight: lighter; }
    .milisemas p { width: 214px; text-align: left; font-size:83%; line-height: 18px; margin: 18px auto 24px; }
    div.form-actions {margin-top:20px;position:relative;}
    div.form-actions table{margin:0 auto 0;}
    div.form-actions input.linkbutton{position:absolute;top: -15px;right: -1px;}
    div.form-actions .cmdSend{cursor:pointer;}
    .mosaiccontact h1 {font-size: 24px;font-weight: lighter;color: #be202f;}
    .mosaiccontact .tblNoBg {width: 100%;border-spacing: 1px;}
    .mosaiccontact .tblNoBg td {padding: 8px;vertical-align: top;font-size:16px;}
    .mosaiccontact .bold{font-weight: bold;}
    .mosaiccontact p, .maincontact p{font-size:14px;}
    .line{ border-bottom: 1px solid #c7c7c7;margin:10px;}
    .maincontact .contact-form-info{width: 530px; }
    ul.providers{margin-left:0px;margin-right:0px;padding-left:10px}
    .contactuscontainer h1 {font-size: 24px;font-weight: lighter;color: #be202f;}
    a, .contactuscontainer a h1 {color:#24A9E1;text-decoration:none;}
    a:hover, .contactuscontainer a:hover h1{color: #007ada;}
    
    .tableRight ul.phones {padding: 0;margin: 0;}
    .tableRight ul.phones li {padding-left: 40px;padding-bottom:20px;}
    .tableRight ul.phones li, .tableRight ul li span, .tableRight ul.phones span.phone {display: block;height: 20px;line-height: 20px;}
    .tableRight ul li.us {background: url("/Images/flags/US.png") no-repeat scroll 0 20% transparent;}
    .tableRight ul li.tr {background: url("/Images/flags/turk.png") no-repeat scroll 0 20% transparent;}
    .tableRight ul.phones li span {float: left;width: 50%;}
    .tableRight ul.phones li span.phone {float: left;text-align: right;width: 50%;}
    
    #chat-online-wrap {display:block;position:relative;width:227px;height:227px;}
    #operator-online {position:absolute;top:0px;left:0px;}
    .milisemas .online-text {text-align:center;color:#000;font-size:16px;width:227px;margin-top:10px;}
</style>
    <script type="text/javascript" src="Scripts/jQueryRotateCompressed.js"></script>
    <script type="text/javascript">
        function startRotation() {
            var t = this;
            t.angle = 25;
            t.operatorImg = $('#chat-online-rot');
            var rotation = function () {
                if (t.angle < 180) 
                    t.angle += 180;
                else 
                    t.angle -= 180;
                
                t.operatorImg.rotate({
                    animateTo: t.angle,
                    duration:500,
                    callback: function () { setTimeout(rotation, 1500); }
                })
            }
            t.operatorImg.rotate({
                angle: t.angle
            });
            setTimeout(rotation, 1500);
        };
        $(function () {
            startRotation();
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <asp:Literal ID="lbContactTopDescription" runat="server" />
    <div class="maincontact">
        <div class="contactuscontainer">
            <asp:Literal ID="lbHeader" runat="server" />
            <asp:Literal ID="lbContactFormInfo" runat="server" />

            <div class="milisemasform">
                <uc2:ContactControl ID="ContactCtl" runat="server" />
            </div>

        <div class="milisemas" id="milisemas" runat="server" visible="false">
            <a href="javascript:void(window.open('<%= Request.Url.Scheme%>://chat.goomena.com/chat.php','','width=590,height=610,left=0,top=0,resizable=yes,menubar=no,location=no,status=yes,scrollbars=yes'))">
                <asp:Literal ID="lbHeaderZilla" runat="server"/>
                <span id="chat-online-wrap" style="margin-top: 14px;">
                   <img id="chat-online-rot" src="<%= Request.Url.Scheme%>://cdn.goomena.com/images2/contact/green.png" alt="<%= Dating.Server.Site.Web.clsWebFormHelper.StripTags(MyBase.CurrentPageData.GetCustomString("lbHeaderZilla"))%>" />
                   <img id="operator-online" src="<%= Request.Url.Scheme%>://cdn.goomena.com/images2/contact/support.png" alt="<%= Dating.Server.Site.Web.clsWebFormHelper.StripTags(MyBase.CurrentPageData.GetCustomString("lbHeaderZilla"))%>" />
                </span>
                <div class="online-text">online</div>
            </a>
            <asp:Literal ID="lblTextlivezilla" runat="server" />
        </div>
        </div>

    </div>
        
</asp:Content>

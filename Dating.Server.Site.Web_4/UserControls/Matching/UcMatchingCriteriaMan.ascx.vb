﻿Imports Dating.Server.Core.DLL
Imports DevExpress.Web.ASPxEditors
Imports Dating.Server.Datasets.DLL

''' <summary>
''' This control is loaded when a MAN view the search
''' </summary>
''' <remarks></remarks>
Public Class UcMatchingCriteriaMan
    Inherits BaseUserControl
    Implements IMatchingCriterias



    Protected Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData("control.UcMatchingCritiriaMan", Context, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property

    Private Sub Page_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        Try
            If Me._pageData IsNot Nothing Then
                RemoveHandler Me._pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If

        Catch ex As Exception
        End Try

        Try
            MyBase.Dispose()
        Catch ex As Exception
        End Try
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If (Not Me.IsPostBack) Then
                LoadLAG()
                If clsMatchingHelper.HasFilledMatchingCriteria(Me.MasterProfileId) Then
                    fillControlsWithSavedValues()
                Else
                    trcDistance.Value = 10000
                End If
            End If
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Private Sub fillControlsWithSavedValues()
        Dim re As GetMatchingCriteriaResult = clsMatchingHelper.GetMatchingCriteria(Me.MasterProfileId) ' clsMatchingHelper.GetMatcingCriteriaReturnFromDataRow()
        If re.HasRow AndAlso re.HasErrors = False Then
            Dim ma As MatcingCriteriaReturn = clsMatchingHelper.GetMatcingCriteriaReturnFromDataRow(re.Row)
            trcAge.PositionEnd = If(ma.ageMaxSelectedValue = -1, 120, ma.ageMaxSelectedValue)
            trcAge.PositionStart = If(ma.ageMinSelectedValue = -1, 18, ma.ageMinSelectedValue)
            trcHeight.PositionEnd = If(ma.HeightMaxSelectedValue = -1, 26, ma.HeightMaxSelectedValue - 29)
            trcHeight.PositionStart = If(ma.HeightMinSelectedValue = -1, 0, ma.HeightMinSelectedValue - 29)
            trcDistance.Value = If(ma.ddlDistanceSelectedValue = -1, 10000, ma.ddlDistanceSelectedValue)
            SelectValuesFromValues(cbHairColor, ma.SelectedHairColorList)
            SelectValuesFromValues(clstBreast, ma.SelectedBreastSizeList)
            SelectValuesFromValues(cbEyeColor, ma.SelectedEyeColorList)
            SelectValuesFromValues(cbBodyType, ma.SelectedBodyTypeList)
            SelectValuesFromValues(cbRelationshipStatus, ma.SelectedRelationShipStatusList)
            chkTravel.Checked = If(ma.chkWillingToTravelChecked.HasValue, ma.chkWillingToTravelChecked, False)


            SelectValuesFromValues(cbEducation, ma.SelectedEducationList)
            SelectValuesFromValues(cbDatingType, ma.SelectedTypeOfDatingList)
            SelectValuesFromValues(cbChildren, ma.SelectedChildrenList)
            SelectValuesFromValues(cbEthnicity, ma.SelectedEthnicityList)
            SelectValuesFromValues(cbSmoking, ma.SelectedSmokingHabitsList)
            SelectValuesFromValues(cbReligion, ma.SelectedReligionList)
            SelectValuesFromValues(cbDrinking, ma.SelectedDrinkingHabitsList)
            Dim clstSpokenLang As ASPxListBox = ddeSpokenLangWrap.FindControl("clstSpokenLang")
            SelectValuesFromValues(clstSpokenLang, ma.SelectedLanguagesList)

        End If
    End Sub

    Private Sub SelectValuesFromValues(ByRef cb As DevExpress.Web.ASPxEditors.ASPxListBox, ByRef values As SearchValuesInfo)
        If values.IsAllSelected = False Then
            If (cb.Items.Count > 0 AndAlso values.Values IsNot Nothing AndAlso values.Values.Length > 0) Then
                Dim itm As DevExpress.Web.ASPxEditors.ListEditItem = cb.Items(0)
                itm.Selected = False
                Dim vals As String() = values.Values.Split(","c)
                Dim count As Integer = 0
                Dim tmp As String = ""

                For Each item As DevExpress.Web.ASPxEditors.ListEditItem In cb.Items
                    For i As Integer = vals.Length - 1 To 0 Step -1
                        If item.Value = CInt(vals(i)) Then
                            If tmp = "" Then
                                tmp = item.Text
                            Else
                                tmp &= ";" & item.Text
                            End If

                            item.Selected = True
                            count += 1
                        End If
                        If count = vals.Length Then
                            Exit For
                        End If
                    Next
                    If count = vals.Length Then
                        Exit For
                    End If
                Next
                ddeSpokenLangWrap.Text = tmp
            End If
        End If
    End Sub
    Private Sub SelectValuesFromValues(ByRef cb As DevExpress.Web.ASPxEditors.ASPxCheckBoxList, ByRef values As SearchValuesInfo)
        If values.IsAllSelected = False Then
            If (cb.Items.Count > 0 AndAlso values.Values IsNot Nothing AndAlso values.Values.Length > 0) Then
                Dim itm As DevExpress.Web.ASPxEditors.ListEditItem = cb.Items(0)
                itm.Selected = False
                Dim vals As String() = values.Values.Split(","c)
                Dim count As Integer = 0
                For Each item As DevExpress.Web.ASPxEditors.ListEditItem In cb.Items
                    For i As Integer = vals.Length - 1 To 0 Step -1
                        If item.Value = CInt(vals(i)) Then
                            item.Selected = True
                            count += 1
                        End If
                        If count = vals.Length Then
                            Exit For
                        End If
                    Next
                    If count = vals.Length Then
                        Exit For
                    End If
                Next
            End If
        End If
    End Sub
    Protected Sub LoadLAG()
        Try

            lblCriteriaHeader.Text = CurrentPageData.GetCustomString("lblCriteriaTop")
            ddeSpokenLangWrap.NullText = CurrentPageData.GetCustomString("QS.SpokenLang.AnyLang")
            LoadAdvancedSearch()
            msg_DistanceVal.Text = CurrentPageData.GetCustomString("QS.AnyDistance")
            msg_AgeText.Text = CurrentPageData.GetCustomString("msg_AgeText")
            msg_PersonalHeight.Text = CurrentPageData.GetCustomString("msg_PersonalHeight")
            msg_BreastSize.Text = CurrentPageData.GetCustomString("QS.BreastSize") 'msg_BreastSize
            msg_RelationshipStatus.Text = CurrentPageData.GetCustomString("msg_RelationshipStatus") 'msg_RelationshipStatus
            msg_TypeOfdating.Text = CurrentPageData.GetCustomString("msg_TypeOfdating") 'msg_TypeOfdating
            msg_PersonalEyeClr.Text = CurrentPageData.GetCustomString("msg_PersonalEyeClr") 'msg_PersonalEyeClr
            msg_PersonalHairClr.Text = CurrentPageData.GetCustomString("msg_PersonalHairClr") 'msg_PersonalHairClr
            msg_PersonalBodyType.Text = CurrentPageData.GetCustomString("msg_PersonalBodyType") 'msg_PersonalBodyType
            'chkOnline.Text = CurrentPageData.GetCustomString("chkOnline") 'chkOnline
            'chkPhotos.Text = CurrentPageData.GetCustomString("chkPhotos") 'chkPhotos
            'chkPhotosPrivate.Text = CurrentPageData.GetCustomString("QS.Private.Photo")
            chkTravel.Text = CurrentPageData.GetCustomString("QS.WillingToTravel.FEMALE")
            msg_SpokenLang.Text = CurrentPageData.GetCustomString("QS.SpokenLang")
            msg_OtherEducat.Text = CurrentPageData.GetCustomString("msg_OtherEducat")
            msg_PersonalChildren.Text = CurrentPageData.GetCustomString("msg_PersonalChildren")
            msg_PersonalEthnicity.Text = CurrentPageData.GetCustomString("msg_PersonalEthnicity")
            msg_PersonalReligion.Text = CurrentPageData.GetCustomString("msg_PersonalReligion")
            msg_PersonalSmokingHabit.Text = CurrentPageData.GetCustomString("msg_PersonalSmokingHabit")
            msg_PersonalDrinkingHabit.Text = CurrentPageData.GetCustomString("msg_PersonalDrinkingHabit")

            btnSaveAndFind.Text = CurrentPageData.GetCustomString("btnSaveAndFind")
            btnSaveAndFind.Text = AppUtils.StripHTML(btnSaveAndFind.Text)

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Private Sub LoadAdvancedSearch()

        Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_BreastSize, Session("LAGID"), "BreastSizeID", clstBreast, True, False, "US")
        If (clstBreast.Items.Count > 0) Then
            Dim itm As DevExpress.Web.ASPxEditors.ListEditItem = clstBreast.Items(0)
            itm.Value = -1
            itm.Text = CurrentPageData.GetCustomString("QS.BreastSize.AnySize")
            itm.Selected = True
        End If
        Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_HairColor, Me.GetLag(), "HairColorId", cbHairColor, True)
        If (cbHairColor.Items.Count > 0) Then
            Dim itm As DevExpress.Web.ASPxEditors.ListEditItem = cbHairColor.Items(0)
            itm.Value = -1
            itm.Text = CurrentPageData.GetCustomString("QS.PersonalHair.Any")
            itm.Selected = True
        End If
        Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_EyeColor, Me.GetLag(), "EyeColorId", cbEyeColor, True)
        If (cbEyeColor.Items.Count > 0) Then
            Dim itm As DevExpress.Web.ASPxEditors.ListEditItem = cbEyeColor.Items(0)
            itm.Value = -1
            itm.Text = CurrentPageData.GetCustomString("QS.PersonalHair.Any")
            itm.Selected = True
        End If
        Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_BodyType, Me.GetLag(), "BodyTypeId", cbBodyType, True)
        If (cbBodyType.Items.Count > 0) Then
            Dim itm As DevExpress.Web.ASPxEditors.ListEditItem = cbBodyType.Items(0)
            itm.Value = -1
            itm.Text = CurrentPageData.GetCustomString("QS.BodyType.Any")
            itm.Selected = True
        End If
        Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_RelationshipStatus, Me.GetLag(), "RelationshipStatusId", cbRelationshipStatus, True)
        If (cbRelationshipStatus.Items.Count > 0) Then
            Dim itm As DevExpress.Web.ASPxEditors.ListEditItem = cbRelationshipStatus.Items(0)
            itm.Value = -1
            itm.Text = CurrentPageData.GetCustomString("QS.RelationshipStatus.Any")
            itm.Selected = True
        End If
        Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_TypeOfDating, Me.GetLag(), "TypeOfDatingId", cbDatingType, True)
        Try
            Dim itm As New DevExpress.Web.ASPxEditors.ListEditItem
            itm.Value = -1
            itm.Text = CurrentPageData.GetCustomString("QS.TypeOfDating.Any")
            itm.Selected = True
            cbDatingType.Items.Insert(0, itm)
        Catch ex As Exception
        End Try

        Dim clstSpokenLang As ASPxListBox = ddeSpokenLangWrap.FindControl("clstSpokenLang")
        Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_SpokenLanguages, "LangName", "SpokenLangId", clstSpokenLang, True)
        If (clstSpokenLang.Items.Count > 0) Then
            Dim itm As New DevExpress.Web.ASPxEditors.ListEditItem()
            itm.Value = -1
            itm.Text = CurrentPageData.GetCustomString("QS.SpokenLang.AnyLang")
            clstSpokenLang.Items.Insert(0, itm)
        End If
        Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_Education, Me.GetLag(), "EducationId", cbEducation, True)
        If (cbEducation.Items.Count > 0) Then
            Dim itm As DevExpress.Web.ASPxEditors.ListEditItem = cbEducation.Items(0)
            itm.Value = -1
            itm.Text = CurrentPageData.GetCustomString("QS.Education.Any")
            itm.Selected = True
        End If
        Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_ChildrenNumber, Me.GetLag(), "ChildrenNumberId", cbChildren, True)
        If (cbChildren.Items.Count > 0) Then
            Dim itm As DevExpress.Web.ASPxEditors.ListEditItem = cbChildren.Items(0)
            itm.Value = -1
            itm.Text = CurrentPageData.GetCustomString("QS.Children.Any")
            itm.Selected = True
        End If
        Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_Ethnicity, Me.GetLag(), "EthnicityId", cbEthnicity, True)
        If (cbEthnicity.Items.Count > 0) Then
            Dim itm As DevExpress.Web.ASPxEditors.ListEditItem = cbEthnicity.Items(0)
            itm.Value = -1
            itm.Text = CurrentPageData.GetCustomString("QS.Ethnicity.Any")
            itm.Selected = True
        End If
        Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_Religion, Me.GetLag(), "ReligionId", cbReligion, True)
        If (cbReligion.Items.Count > 0) Then
            Dim itm As DevExpress.Web.ASPxEditors.ListEditItem = cbReligion.Items(0)
            itm.Value = -1
            itm.Text = CurrentPageData.GetCustomString("QS.Religion.Any")
            itm.Selected = True
        End If
        Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_Smoking, Me.GetLag(), "SmokingId", cbSmoking, True)
        If (cbSmoking.Items.Count > 0) Then
            Dim itm As DevExpress.Web.ASPxEditors.ListEditItem = cbSmoking.Items(0)
            itm.Value = -1
            itm.Text = CurrentPageData.GetCustomString("QS.Smoking.Any")
            itm.Selected = True
        End If
        Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_Drinking, Me.GetLag(), "DrinkingId", cbDrinking, True)
        If (cbDrinking.Items.Count > 0) Then
            Dim itm As DevExpress.Web.ASPxEditors.ListEditItem = cbDrinking.Items(0)
            itm.Value = -1
            itm.Text = CurrentPageData.GetCustomString("QS.Drinking.Any")
            itm.Selected = True
        End If









        'Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_Income, Me.GetLag(), "IncomeId", cbIncome, True)
        'If (cbIncome.Items.Count > 0) Then
        '    cbIncome.Items.RemoveAt(0)
        '    SelectAllCheckBoxes(cbIncome, True)
        'End If









    End Sub


#Region "Properties"
    Public Property ageMaxSelectedValue As Integer Implements IMatchingCriterias.ageMaxSelectedValue
        Get
            Return trcAge.PositionEnd
        End Get
        Set(value As Integer)
            trcAge.PositionEnd = value
        End Set
    End Property
    Public Property ageMinSelectedValue As Integer Implements IMatchingCriterias.ageMinSelectedValue
        Get
            Return trcAge.PositionStart
        End Get
        Set(value As Integer)
            trcAge.PositionStart = value
        End Set
    End Property
    Public Property ddlDistanceSelectedValue As Integer Implements IMatchingCriterias.ddlDistanceSelectedValue
        Get
            If (trcDistance.Position = trcDistance.Items.Count - 1) Then
                Return clsSearchHelper.DISTANCE_DEFAULT
            End If
            Dim itm As DevExpress.Web.ASPxEditors.TrackBarItem = trcDistance.Items(trcDistance.Position)
            Return itm.Value
        End Get
        Set(value As Integer)
            ' ddlDistance.SelectedValue = value
        End Set
    End Property
    Public Property chkOnlineChecked As Boolean Implements IMatchingCriterias.chkOnlineChecked
    '    Get
    '        Return chkOnline.Checked
    '    End Get
    '    Set(value As Boolean)
    '        chkOnline.Checked = value
    '    End Set
    'End Property
    Public Property chkPhotosChecked As Boolean Implements IMatchingCriterias.chkPhotosChecked
    '    Get
    '        Return chkPhotos.Checked
    '    End Get
    '    Set(value As Boolean)
    '        chkPhotos.Checked = value
    '    End Set
    'End Property
    Public Property chkPhotosPrivateChecked As Boolean Implements IMatchingCriterias.chkPhotosPrivateChecked
    ''    Get
    ''        Return chkPhotosPrivate.Checked
    ''    End Get
    ''    Set(value As Boolean)
    ''        chkPhotosPrivate.Checked = value
    ''    End Set
    ''End Property
    Public Property chkWillingToTravelChecked As Boolean Implements IMatchingCriterias.chkWillingToTravelChecked
        Get
            Return chkTravel.Checked
        End Get
        Set(value As Boolean)
            chkTravel.Checked = value
        End Set
    End Property
    Public Property HeightMaxSelectedValue As Integer Implements IMatchingCriterias.HeightMaxSelectedValue
        Get
            Return trcHeight.PositionEnd
        End Get
        Set(value As Integer)
            trcHeight.PositionEnd = value
        End Set
    End Property
    Public Property HeightMinSelectedValue As Integer Implements IMatchingCriterias.HeightMinSelectedValue
        Get
            Return trcHeight.PositionStart
        End Get
        Set(value As Integer)
            trcHeight.PositionStart = value
        End Set
    End Property
    Public Property SelectedBreastSizeList As SearchListInfo Implements IMatchingCriterias.SelectedBreastSizeList
        Get
            Dim allSelected As Boolean = True
            Dim lst As New List(Of Integer)()
            For Each itm As DevExpress.Web.ASPxEditors.ListEditItem In clstBreast.Items
                If (itm.Selected AndAlso itm.Value > -1) Then lst.Add(itm.Value)
                If (Not itm.Selected AndAlso itm.Value > -1) Then allSelected = False
            Next

            Dim li As New SearchListInfo()
            li.List = lst
            li.IsAllSelected = allSelected
            Return li
        End Get
        Set(value As SearchListInfo)
        End Set
    End Property
    Public Property SelectedBodyTypeList As SearchListInfo Implements IMatchingCriterias.SelectedBodyTypeList
        Get
            Dim allSelected As Boolean = True
            Dim lst As New List(Of Integer)()
            For Each itm As DevExpress.Web.ASPxEditors.ListEditItem In cbBodyType.Items
                If (itm.Selected AndAlso itm.Value > -1) Then lst.Add(itm.Value)
                If (Not itm.Selected AndAlso itm.Value > -1) Then allSelected = False
            Next

            Dim li As New SearchListInfo()
            li.List = lst
            li.IsAllSelected = allSelected
            Return li
        End Get
        Set(value As SearchListInfo)
        End Set
    End Property
    Public Property SelectedChildrenList As SearchListInfo Implements IMatchingCriterias.SelectedChildrenList
        Get
            Dim allSelected As Boolean = True
            Dim lst As New List(Of Integer)()
            For Each itm As DevExpress.Web.ASPxEditors.ListEditItem In cbChildren.Items
                If (itm.Selected AndAlso itm.Value > -1) Then lst.Add(itm.Value)
                If (Not itm.Selected AndAlso itm.Value > -1) Then allSelected = False
            Next

            Dim li As New SearchListInfo()
            li.List = lst
            li.IsAllSelected = allSelected
            Return li
        End Get
        Set(value As SearchListInfo)
        End Set
    End Property
    Public Property SelectedDrinkingHabitsList As SearchListInfo Implements IMatchingCriterias.SelectedDrinkingHabitsList
        Get
            Dim allSelected As Boolean = True
            Dim lst As New List(Of Integer)()
            For Each itm As DevExpress.Web.ASPxEditors.ListEditItem In cbDrinking.Items
                If (itm.Selected AndAlso itm.Value > -1) Then lst.Add(itm.Value)
                If (Not itm.Selected AndAlso itm.Value > -1) Then allSelected = False
            Next

            Dim li As New SearchListInfo()
            li.List = lst
            li.IsAllSelected = allSelected
            Return li
        End Get
        Set(value As SearchListInfo)
        End Set
    End Property
    Public Property SelectedEducationList As SearchListInfo Implements IMatchingCriterias.SelectedEducationList
        Get
            Dim allSelected As Boolean = True
            Dim lst As New List(Of Integer)()
            For Each itm As DevExpress.Web.ASPxEditors.ListEditItem In cbEducation.Items
                If (itm.Selected AndAlso itm.Value > -1) Then lst.Add(itm.Value)
                If (Not itm.Selected AndAlso itm.Value > -1) Then allSelected = False
            Next

            Dim li As New SearchListInfo()
            li.List = lst
            li.IsAllSelected = allSelected
            Return li
        End Get
        Set(value As SearchListInfo)
        End Set
    End Property
    Public Property SelectedEthnicityList As SearchListInfo Implements IMatchingCriterias.SelectedEthnicityList
        Get
            Dim allSelected As Boolean = True
            Dim lst As New List(Of Integer)()
            For Each itm As DevExpress.Web.ASPxEditors.ListEditItem In cbEthnicity.Items
                If (itm.Selected AndAlso itm.Value > -1) Then lst.Add(itm.Value)
                If (Not itm.Selected AndAlso itm.Value > -1) Then allSelected = False
            Next

            Dim li As New SearchListInfo()
            li.List = lst
            li.IsAllSelected = allSelected
            Return li
        End Get
        Set(value As SearchListInfo)
        End Set
    End Property
    Public Property SelectedEyeColorList As SearchListInfo Implements IMatchingCriterias.SelectedEyeColorList
        Get
            Dim allSelected As Boolean = True
            Dim lst As New List(Of Integer)()
            For Each itm As DevExpress.Web.ASPxEditors.ListEditItem In cbEyeColor.Items
                If (itm.Selected AndAlso itm.Value > -1) Then lst.Add(itm.Value)
                If (Not itm.Selected AndAlso itm.Value > -1) Then allSelected = False
            Next

            Dim li As New SearchListInfo()
            li.List = lst
            li.IsAllSelected = allSelected
            Return li
        End Get
        Set(value As SearchListInfo)
        End Set
    End Property
    Public Property SelectedHairColorList As SearchListInfo Implements IMatchingCriterias.SelectedHairColorList
        Get
            Dim allSelected As Boolean = True
            Dim lst As New List(Of Integer)()
            For Each itm As DevExpress.Web.ASPxEditors.ListEditItem In cbHairColor.Items
                If (itm.Selected AndAlso itm.Value > -1) Then lst.Add(itm.Value)
                If (Not itm.Selected AndAlso itm.Value > -1) Then allSelected = False
            Next

            Dim li As New SearchListInfo()
            li.List = lst
            li.IsAllSelected = allSelected
            Return li
        End Get
        Set(value As SearchListInfo)
        End Set
    End Property
    Public Property SelectedRelationShipStatusList As SearchListInfo Implements IMatchingCriterias.SelectedRelationShipStatusList
        Get
            Dim allSelected As Boolean = True
            Dim lst As New List(Of Integer)()
            For Each itm As DevExpress.Web.ASPxEditors.ListEditItem In cbRelationshipStatus.Items
                If (itm.Selected AndAlso itm.Value > -1) Then lst.Add(itm.Value)
                If (Not itm.Selected AndAlso itm.Value > -1) Then allSelected = False
            Next

            Dim li As New SearchListInfo()
            li.List = lst
            li.IsAllSelected = allSelected
            Return li
        End Get
        Set(value As SearchListInfo)
        End Set
    End Property
    Public Property SelectedSmokingHabitsList As SearchListInfo Implements IMatchingCriterias.SelectedSmokingHabitsList
        Get
            Dim allSelected As Boolean = True
            Dim lst As New List(Of Integer)()
            For Each itm As DevExpress.Web.ASPxEditors.ListEditItem In cbSmoking.Items
                If (itm.Selected AndAlso itm.Value > -1) Then lst.Add(itm.Value)
                If (Not itm.Selected AndAlso itm.Value > -1) Then allSelected = False
            Next

            Dim li As New SearchListInfo()
            li.List = lst
            li.IsAllSelected = allSelected
            Return li
        End Get
        Set(value As SearchListInfo)
        End Set
    End Property
    Public Property SelectedTypeOfDatingList As SearchListInfo Implements IMatchingCriterias.SelectedTypeOfDatingList
        Get
            Dim allSelected As Boolean = True
            Dim lst As New List(Of Integer)()
            For Each itm As DevExpress.Web.ASPxEditors.ListEditItem In cbDatingType.Items
                If (itm.Selected AndAlso itm.Value > -1) Then lst.Add(itm.Value)
                If (Not itm.Selected AndAlso itm.Value > -1) Then allSelected = False
            Next
            Dim li As New SearchListInfo()
            li.List = lst
            li.IsAllSelected = allSelected
            Return li
        End Get
        Set(value As SearchListInfo)
        End Set
    End Property




    Private Function GetValueInfo(ByRef cb As DevExpress.Web.ASPxEditors.ASPxCheckBoxList) As SearchValuesInfo
        Dim re As New SearchValuesInfo
        Try
            If cb.Items.Count > 0 Then
                If cb.Items(0).Selected = True Then
                    re.IsAllSelected = True
                Else
                    For Each itm As DevExpress.Web.ASPxEditors.ListEditItem In cb.Items
                        If (itm.Selected AndAlso itm.Value > -1) Then
                            If re.Values Is Nothing OrElse re.Values.Length = 0 Then
                                re.Values = itm.Value
                            Else
                                re.Values &= "," & itm.Value
                            End If
                        End If
                        If (Not itm.Selected AndAlso itm.Value > -1) Then re.IsAllSelected = False
                    Next
                End If
            Else
                re.Values = Nothing
                re.IsAllSelected = False
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
        Return re
    End Function

    Private Function GetValueInfo(ByRef cb As DevExpress.Web.ASPxEditors.ASPxListBox) As SearchValuesInfo
        Dim re As New SearchValuesInfo
        Try
            If cb.Items.Count > 0 Then
                If cb.Items(0).Selected = True Then
                    re.IsAllSelected = True
                Else
                    For Each itm As DevExpress.Web.ASPxEditors.ListEditItem In cb.Items
                        If (itm.Selected AndAlso itm.Value > -1) Then
                            If re.Values Is Nothing OrElse re.Values.Length = 0 Then
                                re.Values = itm.Value
                            Else
                                re.Values &= "," & itm.Value
                            End If
                        End If
                        If (Not itm.Selected AndAlso itm.Value > -1) Then re.IsAllSelected = False
                    Next
                End If

           
            Else
                re.Values = Nothing
                re.IsAllSelected = False
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
        Return re
    End Function
    Public Function GetMatchingRow() As MatcingCriteriaReturn
        Dim ma As New MatcingCriteriaReturn
        ma.HeightMinSelectedValue = If(CInt(trcHeight.PositionStart) = 0, -1, CInt(trcHeight.PositionStart) + 29)
        ma.HeightMaxSelectedValue = If(CInt(trcHeight.PositionEnd) = 26, -1, CInt(trcHeight.PositionEnd) + 29)
        ma.ageMinSelectedValue = If(CInt(trcAge.PositionStart) = 18, -1, CInt(trcAge.PositionStart))
        ma.ageMaxSelectedValue = If(CInt(trcAge.PositionEnd) = 120, -1, CInt(trcAge.PositionEnd))
        ma.ddlDistanceSelectedValue = If(CInt(trcDistance.Value) = 10000, -1, CInt(trcDistance.Value))
        'ma.chkOnlineChecked = chkOnline.Checked
        'ma.chkPhotosChecked = chkPhotos.Checked
        'ma.chkPhotosPrivateChecked = chkPhotosPrivate.Checked
        ma.chkWillingToTravelChecked = chkTravel.Checked
        ma.SelectedBodyTypeList = GetValueInfo(cbBodyType)
        ma.SelectedBreastSizeList = GetValueInfo(clstBreast)
        ma.SelectedChildrenList = GetValueInfo(cbChildren)
        ma.SelectedDrinkingHabitsList = GetValueInfo(cbDrinking)
        ma.SelectedEducationList = GetValueInfo(cbEducation)
        ma.SelectedEthnicityList = GetValueInfo(cbEthnicity)
        ma.SelectedEyeColorList = GetValueInfo(cbEyeColor)
        ma.SelectedHairColorList = GetValueInfo(cbHairColor)
        ma.SelectedRelationShipStatusList = GetValueInfo(cbRelationshipStatus)
        ma.SelectedSmokingHabitsList = GetValueInfo(cbSmoking)
        ma.SelectedTypeOfDatingList = GetValueInfo(cbDatingType)
        ma.SelectedReligionList = GetValueInfo(cbReligion)
        Dim clstSpokenLang As ASPxListBox = ddeSpokenLangWrap.FindControl("clstSpokenLang")
        ma.SelectedLanguagesList = GetValueInfo(clstSpokenLang)
        If ma.SelectedLanguagesList.Values Is Nothing OrElse ma.SelectedLanguagesList.Values = "" Then
            If ma.SelectedLanguagesList.IsAllSelected = False Then
                If ddeSpokenLangWrap.Value = Nothing Then
                    ma.SelectedLanguagesList.IsAllSelected = True
                End If
            End If
        End If
        Return ma
    End Function
#End Region
    Protected Property ControlLoaded As Boolean
        Get
            Return ViewState("ControlLoaded")
        End Get
        Set(value As Boolean)
            ViewState("ControlLoaded") = value
        End Set
    End Property



    Public Event SaveSearchClicked(sender As Object, e As SaveAndSearchEventArgs) Implements IMatchingCriterias.SaveSearchClicked




    Public Sub VerifyControlLoaded() Implements IMatchingCriterias.VerifyControlLoaded
        Try
            '''''''''''''''''''''''''''''
            ' !!!   visibility is set by search.aspx, if set to false do not load control
            '''''''''''''''''''''''''''''
            If (Not Me.Visible) Then Return

            If (Not Me.ControlLoaded) Then
                LoadLAG()
                Me.ControlLoaded = True
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try
    End Sub



    Private Sub btnSaveAndFind_Click1(sender As Object, e As EventArgs) Handles btnSaveAndFind.Click
        Dim p As New SaveAndSearchEventArgs
        p.MatcingCriteria = GetMatchingRow()
        clsMatchingHelper.InsertOrUpdateMatchingCriteria(p.MatcingCriteria, Me.MasterProfileId)
        RaiseEvent SaveSearchClicked(sender, p)
    End Sub

    Public Property SelectedLangs As SearchListInfo Implements IMatchingCriterias.SelectedLangs
End Class
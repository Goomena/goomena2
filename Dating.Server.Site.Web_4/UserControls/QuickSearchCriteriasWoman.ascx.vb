﻿Imports Dating.Server.Core.DLL
Imports DevExpress.Web.ASPxEditors


''' <summary>
''' This control is loaded when a WOMAN view the search
''' </summary>
''' <remarks></remarks>
Public Class QuickSearchCriteriasWoman
    Inherits BaseUserControl
    Implements IQuickSearchCriterias


    Public Event SearchByUserNameClicked(sender As Object, e As System.EventArgs) Implements IQuickSearchCriterias.SearchByUserNameClicked
    Public Event SearchClicked(sender As Object, e As EventArgs) Implements IQuickSearchCriterias.SearchClicked
    Public Event AdvancedClicked(sender As Object, e As EventArgs) Implements IQuickSearchCriterias.AdvancedClicked

    Public Property SearchOnClientClicked As String
        Get
            Return ViewState("SearchOnClientClicked")
        End Get
        Set(value As String)
            ViewState("SearchOnClientClicked") = value
        End Set
    End Property

    Public Property SearchByUserNameOnClientClicked As String
        Get
            Return ViewState("SearchByUserNameOnClientClicked")
        End Get
        Set(value As String)
            ViewState("SearchByUserNameOnClientClicked") = value
        End Set
    End Property

    Protected Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData("~/Members/Search.aspx", Context, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property
    Private Sub Page_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        Try
            If Me._pageData IsNot Nothing Then
                RemoveHandler Me._pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If

        Catch ex As Exception
        End Try

        Try
            MyBase.Dispose()
        Catch ex As Exception
        End Try
    End Sub

    Public Property txtUserNameQText As String Implements IQuickSearchCriterias.txtUserNameQText
        Get
            Return txtUserNameQ.Text
        End Get
        Set(value As String)
            txtUserNameQ.Text = value
        End Set
    End Property



    Public Property ageMinSelectedValue As Integer Implements IQuickSearchCriterias.ageMinSelectedValue
        Get
            Return trcAge.PositionStart
        End Get
        Set(value As Integer)
            trcAge.PositionStart = value
        End Set
    End Property


    Public Property ageMaxSelectedValue As Integer Implements IQuickSearchCriterias.ageMaxSelectedValue
        Get
            Return trcAge.PositionEnd
        End Get
        Set(value As Integer)
            trcAge.PositionStart = value
        End Set
    End Property


    Public Property ddlDistanceSelectedValue As Integer Implements IQuickSearchCriterias.ddlDistanceSelectedValue
        Get
            If (trcDistance.Position = trcDistance.Items.Count - 1) Then
                Return clsSearchHelper.DISTANCE_DEFAULT
            End If
            Dim itm As DevExpress.Web.ASPxEditors.TrackBarItem = trcDistance.Items(trcDistance.Position)
            Return itm.Value
        End Get
        Set(value As Integer)
            ' ddlDistance.SelectedValue = value
        End Set
    End Property


    Public Property chkOnlineChecked As Boolean Implements IQuickSearchCriterias.chkOnlineChecked
        Get
            Return chkOnline.Checked
        End Get
        Set(value As Boolean)
            chkOnline.Checked = value
        End Set
    End Property


    Public Property chkPhotosChecked As Boolean Implements IQuickSearchCriterias.chkPhotosChecked
        Get
            Return chkPhotos.Checked
        End Get
        Set(value As Boolean)
            chkPhotos.Checked = value
        End Set
    End Property


    Public Property chkPhotosPrivateChecked As Boolean Implements IQuickSearchCriterias.chkPhotosPrivateChecked
        Get
            Return chkPhotosPrivate.Checked
        End Get
        Set(value As Boolean)
            chkPhotosPrivate.Checked = value
        End Set
    End Property


    Public Property chkVIPChecked As Boolean
        Get
            Return chkVIP.Checked
        End Get
        Set(value As Boolean)
            chkVIP.Checked = value
        End Set
    End Property


    Public Property chkWillingToTravelChecked As Boolean Implements IQuickSearchCriterias.chkWillingToTravelChecked
        Get
            Return chkTravel.Checked
        End Get
        Set(value As Boolean)
            chkTravel.Checked = value
        End Set
    End Property


    Public Property IncomeMin As Decimal?
        Get
            If (trcIncome.PositionStart <= 0) Then
                Return Nothing
            End If
            Dim itm As DevExpress.Web.ASPxEditors.TrackBarItem = trcIncome.Items(trcIncome.PositionStart)
            Return itm.Value
        End Get
        Set(value As Decimal?)
            If (value Is Nothing) Then
                trcIncome.PositionStart = 0
            Else
                Dim itm As DevExpress.Web.ASPxEditors.TrackBarItem = trcIncome.Items.FindByValue(value)
                If (itm IsNot Nothing) Then trcIncome.PositionStart = itm.Index
            End If
        End Set
    End Property


    Public Property IncomeMax As Decimal?
        Get
            If (trcIncome.PositionEnd = trcIncome.Items.Count - 1) Then
                Return Nothing
            End If
            Dim itm As DevExpress.Web.ASPxEditors.TrackBarItem = trcIncome.Items(trcIncome.PositionEnd)
            Return itm.Value
        End Get
        Set(value As Decimal?)
            If (value Is Nothing) Then
                trcIncome.PositionEnd = trcIncome.Items.Count - 1
            Else
                Dim itm As DevExpress.Web.ASPxEditors.TrackBarItem = trcIncome.Items.FindByValue(value)
                If (itm IsNot Nothing) Then trcIncome.PositionEnd = itm.Index
            End If
        End Set
    End Property


    Public Property chkWillingToWorkChecked As Boolean
        Get
            Return chkJob.Checked
        End Get
        Set(value As Boolean)
            chkJob.Checked = value
        End Set
    End Property


    Public Property SelectedJobsList As SearchListInfo
        Get
            Dim allSelected As Boolean = True
            Dim lst As New List(Of Integer)()
            Dim clstJobs As ASPxListBox = ddeJobsWrap.FindControl("clstJobs")
            For Each itm As DevExpress.Web.ASPxEditors.ListEditItem In clstJobs.Items
                If (itm.Selected AndAlso itm.Value > -1) Then lst.Add(itm.Value)
                If (Not itm.Selected AndAlso itm.Value > -1) Then allSelected = False
            Next

            Dim li As New SearchListInfo()
            li.List = lst
            li.IsAllSelected = allSelected
            Return li
        End Get
        Set(value As SearchListInfo)

        End Set
    End Property

    Public Property SelectedBodyTypesList As SearchListInfo Implements IQuickSearchCriterias.SelectedBodyTypesList
        Get
            Dim allSelected As Boolean = True
            Dim lst As New List(Of Integer)()
            For Each itm As DevExpress.Web.ASPxEditors.ListEditItem In clstBodyType.Items
                If (itm.Selected AndAlso itm.Value > -1) Then lst.Add(itm.Value)
                If (Not itm.Selected AndAlso itm.Value > -1) Then allSelected = False
            Next

            Dim li As New SearchListInfo()
            li.List = lst
            li.IsAllSelected = allSelected
            Return li
        End Get
        Set(value As SearchListInfo)
        End Set
    End Property


    Public Property SelectedSpokenLanguagesList As SearchListInfo Implements IQuickSearchCriterias.SelectedSpokenLanguagesList
        Get
            Dim allSelected As Boolean = True
            Dim lst As New List(Of Integer)()
            Dim clstSpokenLang As ASPxListBox = ddeSpokenLangWrap.FindControl("clstSpokenLang")
            For Each itm As DevExpress.Web.ASPxEditors.ListEditItem In clstSpokenLang.Items
                If (itm.Selected AndAlso itm.Value > -1) Then lst.Add(itm.Value)
                If (Not itm.Selected AndAlso itm.Value > -1) Then allSelected = False
            Next

            Dim li As New SearchListInfo()
            li.List = lst
            li.IsAllSelected = allSelected
            Return li
        End Get
        Set(value As SearchListInfo)
        End Set
    End Property


    Public Property SelectedDatingTypesList As SearchListInfo Implements IQuickSearchCriterias.SelectedDatingTypesList
        Get
            Dim allSelected As Boolean = True
            Dim lst As New List(Of Integer)()
            Dim clstTypeOfDating As ASPxListBox = ddeTypeOfDatingWrap.FindControl("clstTypeOfDating")
            For Each itm As DevExpress.Web.ASPxEditors.ListEditItem In clstTypeOfDating.Items
                If (itm.Selected AndAlso itm.Value > -1) Then lst.Add(itm.Value)
                If (Not itm.Selected AndAlso itm.Value > -1) Then allSelected = False
            Next

            Dim li As New SearchListInfo()
            li.List = lst
            li.IsAllSelected = allSelected
            Return li
        End Get
        Set(value As SearchListInfo)
        End Set
    End Property


    Public Property SelectedEducationTitlesList As SearchListInfo
        Get
            Dim allSelected As Boolean = True
            Dim lst As New List(Of Integer)()
            For Each itm As DevExpress.Web.ASPxEditors.ListEditItem In clstEducat.Items
                If (itm.Selected AndAlso itm.Value > -1) Then lst.Add(itm.Value)
                If (Not itm.Selected AndAlso itm.Value > -1) Then allSelected = False
            Next

            Dim li As New SearchListInfo()
            li.List = lst
            li.IsAllSelected = allSelected
            Return li
        End Get
        Set(value As SearchListInfo)
        End Set
    End Property


    Public Property SelectedSmokingHabitsList As SearchListInfo
        Get
            Dim allSelected As Boolean = True
            Dim lst As New List(Of Integer)()
            For Each itm As DevExpress.Web.ASPxEditors.ListEditItem In clstSmoking.Items
                If (itm.Selected AndAlso itm.Value > -1) Then lst.Add(itm.Value)
                If (Not itm.Selected AndAlso itm.Value > -1) Then allSelected = False
            Next

            Dim li As New SearchListInfo()
            li.List = lst
            li.IsAllSelected = allSelected
            Return li
        End Get
        Set(value As SearchListInfo)
        End Set
    End Property


    Public Property SelectedDrinkingHabitsList As SearchListInfo
        Get
            Dim allSelected As Boolean = True
            Dim lst As New List(Of Integer)()
            For Each itm As DevExpress.Web.ASPxEditors.ListEditItem In clstDrinking.Items
                If (itm.Selected AndAlso itm.Value > -1) Then lst.Add(itm.Value)
                If (Not itm.Selected AndAlso itm.Value > -1) Then allSelected = False
            Next

            Dim li As New SearchListInfo()
            li.List = lst
            li.IsAllSelected = allSelected
            Return li
        End Get
        Set(value As SearchListInfo)
        End Set
    End Property


    Protected Property ControlLoaded As Boolean
        Get
            Return ViewState("ControlLoaded")
        End Get
        Set(value As Boolean)
            ViewState("ControlLoaded") = value
        End Set
    End Property



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        VerifyControlLoaded()
    End Sub



    Public Sub VerifyControlLoaded() Implements IQuickSearchCriterias.VerifyControlLoaded
        Try
            '''''''''''''''''''''''''''''
            ' !!!   visibility is set by search.aspx, if set to false do not load control
            '''''''''''''''''''''''''''''
            If (Not Me.Visible) Then Return

            If (Not Me.ControlLoaded) Then
                LoadLAG()
                Me.ControlLoaded = True
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try
    End Sub



    Protected Sub LoadLAG()
        Try
            '   Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            'SetControlsValue(Me, CurrentPageData)

            txtUserNameQ.NullText = CurrentPageData.GetCustomString("msg_UserNameSearch")
            msg_BirthdayNote.Text = CurrentPageData.GetCustomString("msg_BirthdayNote")
            lnkAdvanced.Text = CurrentPageData.GetCustomString("lnkAdvanced_ND")
            chkPhotos.Text = CurrentPageData.GetCustomString("chkPhotos")
            chkOnline.Text = CurrentPageData.GetCustomString("chkOnline")
            chkVIP.Text = CurrentPageData.GetCustomString("chkVIP")
            'TODO: remove comments
            'If (Me.IsMale) Then
            '    chkVIP.Visible = False
            'Else
            '    chkVIP.Text = CurrentPageData.GetCustomString(chkVIP.ID)
            'End If


            btnSearch_ND.Text = CurrentPageData.GetCustomString("btnSearch_ND")
            chkPhotos.Text = CurrentPageData.GetCustomString("chkPhotos")
            chkPhotosPrivate.Text = CurrentPageData.GetCustomString("QS.Private.Photo")
            If (Me.IsMale) Then
                chkTravel.Text = CurrentPageData.GetCustomString("QS.WillingToTravel.FEMALE")
            Else
                chkTravel.Text = CurrentPageData.GetCustomString("QS.WillingToTravel.MALE")
            End If

            If (Me.IsMale) Then
                chkJob.Text = CurrentPageData.GetCustomString("QS.WillingToWork.FEMALE")
            ElseIf (Me.IsFemale) Then
                chkJob.Text = CurrentPageData.GetCustomString("QS.WillingToWork.MALE")
            End If

            msg_AgeText.Text = CurrentPageData.GetCustomString("msg_AgeText")
            msg_OtherAnnual.Text = CurrentPageData.GetCustomString("msg_OtherAnnual")
            msg_DistanceVal.Text = CurrentPageData.GetCustomString("QS.AnyDistance")
            msg_OtherEducat.Text = CurrentPageData.GetCustomString("msg_OtherEducat")
            msg_PersonalSmokingHabit.Text = CurrentPageData.GetCustomString("QS.SmokingHabit")
            msg_PersonalDrinkingHabit.Text = CurrentPageData.GetCustomString("QS.DrinkingHabit")
            msg_PersonalBodyType.Text = CurrentPageData.GetCustomString("msg_PersonalBodyType")
            msg_SpokenLang.Text = CurrentPageData.GetCustomString("QS.SpokenLang")
            msg_TypeOfDating.Text = CurrentPageData.GetCustomString("QS.DatingType")
            ddeSpokenLangWrap.NullText = CurrentPageData.GetCustomString("QS.SpokenLang.AnyLang")
            ddeTypeOfDatingWrap.NullText = CurrentPageData.GetCustomString("QS.DatingType.AnyType")
            ddeJobsWrap.NullText = CurrentPageData.GetCustomString("QS.WillingToWork.AllSpecialties")
            msg_MoreOptions.Text = CurrentPageData.GetCustomString("QS.More.Filters")
            msg_ClearForm.Text = CurrentPageData.GetCustomString("QS.ClearForm")


            'Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_BreastSize, Session("LAGID"), "BreastSizeID", clstBreast, True, False, "US")
            'If (clstBreast.Items.Count > 0) Then
            '    Dim itm As DevExpress.Web.ASPxEditors.ListEditItem = clstBreast.Items(0)
            '    itm.Value = -1
            '    itm.Text = CurrentPageData.GetCustomString("QS.BreastSize.AnySize")
            '    itm.Selected = True
            'End If

            Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_Education, Session("LAGID"), "EducationId", clstEducat, True, False, "US")
            If (clstEducat.Items.Count > 0) Then
                clstEducat.Items.RemoveAt(0)
            End If

            Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_Smoking, Session("LAGID"), "SmokingId", clstSmoking, True, False, "US")
            If (clstSmoking.Items.Count > 0) Then
                clstSmoking.Items.RemoveAt(0)
                For Each itm As DevExpress.Web.ASPxEditors.ListEditItem In clstSmoking.Items
                    If (itm.Value = 3) Then
                        ' Light Smoker
                        Dim txt As String = CurrentPageData.GetCustomString("QS.SmokingHabit.LightSmoker")
                        If (Not String.IsNullOrEmpty(txt)) Then itm.Text = txt
                    End If
                    If (itm.Value = 4) Then
                        ' Heavy Smoker
                        Dim txt As String = CurrentPageData.GetCustomString("QS.SmokingHabit.HeavySmoker")
                        If (Not String.IsNullOrEmpty(txt)) Then itm.Text = txt
                    End If
                Next
            End If

            Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_Drinking, Session("LAGID"), "DrinkingId", clstDrinking, True, False, "US")
            If (clstDrinking.Items.Count > 0) Then
                clstDrinking.Items.RemoveAt(0)
                For Each itm As DevExpress.Web.ASPxEditors.ListEditItem In clstDrinking.Items
                    If (itm.Value = 2) Then
                        ' Non-Drinker
                        Dim txt As String = CurrentPageData.GetCustomString("QS.DrinkingHabit.NonDrinker")
                        If (Not String.IsNullOrEmpty(txt)) Then itm.Text = txt
                    End If
                    If (itm.Value = 3) Then
                        ' Social Drinker
                        Dim txt As String = CurrentPageData.GetCustomString("QS.DrinkingHabit.Normal")
                        If (Not String.IsNullOrEmpty(txt)) Then itm.Text = txt
                    End If
                    If (itm.Value = 4) Then
                        ' Heavy Drinker
                        Dim txt As String = CurrentPageData.GetCustomString("QS.DrinkingHabit.Heavy")
                        If (Not String.IsNullOrEmpty(txt)) Then itm.Text = txt
                    End If
                Next
            End If

            Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_BodyType, Session("LAGID"), "BodyTypeId", clstBodyType, True, False, "US")
            If (clstBodyType.Items.Count > 0) Then
                clstBodyType.Items.RemoveAt(0)
            End If

            Dim clstSpokenLang As ASPxListBox = ddeSpokenLangWrap.FindControl("clstSpokenLang")
            Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_SpokenLanguages, "LangName", "SpokenLangId", clstSpokenLang, True)
            If (clstSpokenLang.Items.Count > 0) Then
                Dim itm As New DevExpress.Web.ASPxEditors.ListEditItem()
                itm.Value = -1
                itm.Text = CurrentPageData.GetCustomString("QS.SpokenLang.AnyLang")
                'itm.Selected = True
                clstSpokenLang.Items.Insert(0, itm)
            End If

            Dim clstTypeOfDating As ASPxListBox = ddeTypeOfDatingWrap.FindControl("clstTypeOfDating")
            Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_TypeOfDating, Session("LAGID"), "TypeOfDatingId", clstTypeOfDating, True, False, "US")
            If (clstTypeOfDating.Items.Count > 0) Then
                Dim itm As New DevExpress.Web.ASPxEditors.ListEditItem()
                itm.Value = -1
                itm.Text = CurrentPageData.GetCustomString("QS.DatingType.AnyType")
                'itm.Selected = True
                clstTypeOfDating.Items.Insert(0, itm)
            End If

            Dim clstJobs As ASPxListBox = ddeJobsWrap.FindControl("clstJobs")
            Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_Job, Session("LAGID"), "JobID", clstJobs, True, False, "US")
            If (clstJobs.Items.Count > 0) Then
                Dim itm As New DevExpress.Web.ASPxEditors.ListEditItem()
                itm.Value = -1
                itm.Text = CurrentPageData.GetCustomString("QS.WillingToWork.AllSpecialties")
                'itm.Selected = True
                clstJobs.Items.Insert(0, itm)
            End If


            Dim itm2 As DevExpress.Web.ASPxEditors.TrackBarItem = trcDistance.Items(trcDistance.Items.Count - 1)
            itm2.Text = CurrentPageData.GetCustomString("QS.AnyDistance")
            itm2.ToolTip = itm2.Text


            Dim incomeLess20 As String = ProfileHelper.GetIncomeString(22, Session("LAGID"))
            Dim incomeMore1M As String = ProfileHelper.GetIncomeString(36, Session("LAGID"))
            Dim itm3 As DevExpress.Web.ASPxEditors.TrackBarItem = trcIncome.Items(0)
            itm3.ToolTip = incomeLess20
            'msg_IncomeMin.Text = incomeLess20

            itm3 = trcIncome.Items(trcIncome.Items.Count - 1)
            itm3.ToolTip = incomeMore1M
            'msg_IncomeMax.Text = incomeMore1M

            If (Not String.IsNullOrEmpty(SearchOnClientClicked)) Then
                btnSearch_ND.OnClientClick = SearchOnClientClicked
            End If

            If (Not String.IsNullOrEmpty(SearchByUserNameOnClientClicked)) Then
                imgSearchByUserName.OnClientClick = SearchByUserNameOnClientClicked
                txtUserNameQ.ClientSideEvents.KeyPress = "fire_imgSearchByUserName"
            End If

            trcDistance.Position = trcDistance.Items.Count - 1

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Protected Sub imgSearchByUserName_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles imgSearchByUserName.Click
        RaiseEvent SearchByUserNameClicked(sender, e)
    End Sub

    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch_ND.Click
        RaiseEvent SearchClicked(sender, e)
    End Sub

    Protected Sub hlAdvanced_Click(sender As Object, e As EventArgs) Handles lnkAdvanced.Click
        RaiseEvent AdvancedClicked(sender, e)
    End Sub

    Public Sub ShowBirthdayNote() Implements IQuickSearchCriterias.ShowBirthdayNote
        msg_BirthdayNote.Visible = True
    End Sub


    Public Sub Distance_SelectComboItem(value As Integer) Implements IQuickSearchCriterias.Distance_SelectComboItem
        Try
            If (trcDistance.Items.FindByValue(value) IsNot Nothing) Then
                trcDistance.Position = value
            End If
        Catch
        End Try
        ' SelectComboItem(ddlDistance.ListControl, value)
    End Sub



End Class
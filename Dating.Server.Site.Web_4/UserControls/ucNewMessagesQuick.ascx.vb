﻿Imports Dating.Server.Core.DLL
Imports DevExpress.Web.ASPxEditors

Public Class ucNewMessagesQuick
    Inherits BaseUserControl

    Private _BindControlDataComplete As Boolean

    Protected Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            'If (_pageData Is Nothing) Then _pageData = New clsPageData("~/Members/Messages.aspx", Context)
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData("~/Members/Messages.aspx", Context, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property

    Private Sub Page_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        Try
            If Me._pageData IsNot Nothing Then
                RemoveHandler Me._pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If

        Catch ex As Exception
        End Try

        Try
            MyBase.Dispose()
        Catch ex As Exception
        End Try
    End Sub

    Public Property ItemsPerPage As Integer
        Get
            If (Me.ViewState("ItemsPerPage") IsNot Nothing) Then Return Me.ViewState("ItemsPerPage")
            Return 10
        End Get
        Set(value As Integer)
            Me.ViewState("ItemsPerPage") = value
        End Set
    End Property

    Public Property IsMessageFromAdmin As Boolean
        Get
            If (Me.ViewState("IsMessageFromAdmin") IsNot Nothing) Then Return Me.ViewState("IsMessageFromAdmin")
            Return False
        End Get
        Set(value As Boolean)
            Me.ViewState("IsMessageFromAdmin") = value
        End Set
    End Property


    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        BindControlData()
    End Sub

    Public Function BindControlData() As Integer
        Dim recordsFound As Integer = 0
        If (Not _BindControlDataComplete) Then

            Try
                Dim sql As String = <sql><![CDATA[
exec GetNewMessagesQuick2 
    @CurrentProfileId=@CurrentProfileId,
    @ReturnRecordsWithStatus=@ReturnRecordsWithStatus,
    @NumberOfRecordsToReturn=@NumberOfRecordsToReturn
]]></sql>
                Dim dtResults As DataTable
                Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                    Using cmd As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(con, sql)


                        cmd.Parameters.AddWithValue("@CurrentProfileId", Me.MasterProfileId)
                        cmd.Parameters.AddWithValue("@ReturnRecordsWithStatus", CInt(ProfileStatusEnum.Approved))
                        cmd.Parameters.AddWithValue("@NumberOfRecordsToReturn", Me.ItemsPerPage)
                        dtResults = DataHelpers.GetDataTable(cmd)
                    End Using
                End Using
                'Dim dc2 As New DataColumn()
                'dc2.ColumnName = "IsUnlimitedVisible"
                'dc2.DataType = GetType(Boolean)
                'dtResults.Columns.Add(dc2)

                'Dim dc1 As New DataColumn()
                'dc1.ColumnName = "IsLimitedVisible"
                'dc1.DataType = GetType(Boolean)
                'dtResults.Columns.Add(dc1)

                'For cnt = 0 To dtResults.Rows.Count - 1
                '    dtResults.Rows(cnt)("IsVisible") = True

                '    If (dtResults.Rows(cnt)("CommunicationUnl") > 0 OrElse dtResults.Rows(cnt)("ProfileID") = 1) Then
                '        dtResults.Rows(cnt)("IsUnlimitedVisible") = True

                '    ElseIf (dtResults.Rows(cnt)("CommunicationUnl") = 0) Then
                '        dtResults.Rows(cnt)("IsLimitedVisible") = True

                '    End If
                'Next

                dvMessages.DataSource = dtResults
                dvMessages.DataBind()

                If (dtResults IsNot Nothing) Then
                    recordsFound = dtResults.Rows.Count
                End If

            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try

            _BindControlDataComplete = True
        Else
            Dim dtResults As DataTable = dvMessages.DataSource
            If (dtResults IsNot Nothing) Then
                recordsFound = dtResults.Rows.Count
            End If
        End If
        Return recordsFound
    End Function

    'Protected Overrides Sub OnPreRender(e As System.EventArgs)
    '    dvWinks.RowPerPage = Me.ItemsPerPage
    '    MyBase.OnPreRender(e)
    'End Sub

    'Protected Sub sdsMessages_Selecting(sender As Object, e As System.Web.UI.WebControls.SqlDataSourceSelectingEventArgs) Handles sdsMessages.Selecting
    '    For Each prm As SqlClient.SqlParameter In e.Command.Parameters

    '        If (prm.ParameterName = "@CurrentProfileId") Then
    '            Dim MasterProfileId = Me.MasterProfileId
    '            prm.Value = MasterProfileId
    '        ElseIf (prm.ParameterName = "@ReturnRecordsWithStatus") Then
    '            prm.Value = ProfileStatusEnum.Approved
    '        ElseIf (prm.ParameterName = "@NumberOfRecordsToReturn") Then
    '            prm.Value = Me.ItemsPerPage
    '        End If

    '    Next
    'End Sub



    Protected Sub dvMessages_DataBound(sender As Object, e As EventArgs) Handles dvMessages.DataBound
        Try
            For Each dvi As DevExpress.Web.ASPxDataView.DataViewItem In dvMessages.Items
                Try

                    Dim dr As DataRowView = dvi.DataItem
                    If (dr("ProfileID") = 1) Then
                        IsMessageFromAdmin = True
                    End If

                    Dim lblSubject As Label = dvMessages.FindItemControl("lblSubject", dvi)
                    If (dr("CommunicationStatus") > 0) Then
                        If (dr("ProfileID") = 1) Then
                            lblSubject.Text = dr("Subject")
                        ElseIf (dr("CommunicationStatus") = 0) Then
                            lblSubject.Text = "You received a message from " & dr("LoginName")
                        Else
                            lblSubject.Text = dr("Subject")
                        End If
                    Else
                        lblSubject.Visible = False
                    End If

                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "")
                End Try
            Next

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Protected Function GetCountMessagesString(ItemsCount As Object) As String

        Try
            If (Not AppUtils.IsDBNullOrNothingOrEmpty(ItemsCount)) Then
                Try
                    If (ItemsCount > 0) Then Return "(" & ItemsCount & ")"
                Catch ex As Exception

                End Try
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Return ""
    End Function


    Protected Shared Function IsVisible(ItemsCount As Object) As Boolean

        If (Not AppUtils.IsDBNullOrNothingOrEmpty(ItemsCount)) Then
            Try
                If (ItemsCount > 1) Then Return True
            Catch ex As Exception

            End Try
        End If

        Return False
    End Function



    Protected Function GetMessageDateDescription(userDt As DateTime) As String
        Dim sentText As String = ""
        Try
            Dim dateDescrpt As String = AppUtils.GetMessageDateDescription(userDt, Me.globalStrings)
            sentText = Me.CurrentPageData.VerifyCustomString("SentDateText")
            sentText = sentText.Replace("###DATE###", dateDescrpt)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "GetMessageDateDescription (handled)")
        End Try
        Return sentText
    End Function




End Class
﻿Imports DevExpress.Web.ASPxUploadControl
Imports System.IO
Imports System.Drawing
Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Core.DLL
Imports DevExpress.Web.ASPxEditors
Imports DevExpress.Web.ASPxPopupControl

Public Class PhotosEdit
    Inherits BaseUserControl


    Public Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            'If (_pageData Is Nothing) Then _pageData = New clsPageData("~/Members/control.PhotosEdit", Context)
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData("~/Members/control.PhotosEdit", Context, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property
    Private Sub Page_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        Try
            If Me._pageData IsNot Nothing Then
                RemoveHandler Me._pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If

        Catch ex As Exception
        End Try

        Try
            MyBase.Dispose()
        Catch ex As Exception
        End Try
    End Sub

    Public Property NoPhotoAvailable As Boolean
        Get
            If (Me.ViewState("NoPhotoAvailable") IsNot Nothing) Then Return Me.ViewState("NoPhotoAvailable")
            Return True
        End Get
        Set(value As Boolean)
            Me.ViewState("NoPhotoAvailable") = value
        End Set
    End Property


    Private Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If (Me.Visible) Then

            Try

                'Try
                '    Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
                '    AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
                'Catch ex As Exception
                '    WebErrorMessageBox(Me, ex, "Page_Load")
                'End Try

                'If (Not Me.IsPostBack) Then
                LoadLAG()
                'End If

                If (Not Page.IsPostBack) Then
                    TogglePersonalCollapsible(False)
                End If

            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "Page_Load")
            End Try
        End If

    End Sub


    Private Sub TogglePersonalCollapsible(show As Boolean)
        If (show) Then
            lnkInfoExpColl.Text = "-"
            h2InfoTitle.Attributes("title") = globalStrings.GetCustomString("msg_ClickToCollapse", GetLag())
            divmp_info.Attributes.CssStyle.Remove("display")
        Else
            lnkInfoExpColl.Text = "+"
            h2InfoTitle.Attributes("title") = globalStrings.GetCustomString("msg_ClickToExpand", GetLag())
            divmp_info.Attributes.CssStyle("display") = "none"
        End If
    End Sub

    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        If (Me.Visible) Then
            Try

                LoadPhotos()

            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "Page_PreRender")
            End Try


            Try
                Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
                AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "Page_PreRender")
            End Try
        End If

    End Sub


    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
    End Sub


    Protected Sub LoadLAG()
        Try

            'lnkVerificationDocs.Text = CurrentPageData.GetCustomString("lnkVerificationDocs")

            'uplImage.BrowseButton.Text = CurrentPageData.GetCustomString("BrowseButton.Text")
            lblAllowed.Text = CurrentPageData.GetCustomString("lblAllowebMimeType_ND")
            'lblMaxFileSize.Text = CurrentPageData.GetCustomString(lblMaxFileSize.ID)
            'chkPrivePhoto.Text = CurrentPageData.GetCustomString(chkPrivePhoto.ID)
            btnUpload.Text = CurrentPageData.GetCustomString(btnUpload.ID)

            btnDeletePhoto.Text = CurrentPageData.GetCustomString("btnDeletePhoto_ND")
            If (Not Page.IsPostBack) Then
                Dim confirmDelete As String = String.Format(<js><![CDATA[return confirmDelete('{0}');]]></js>.Value,
                CurrentPageData.GetCustomString("btnDeleteClientClick").Replace("'", "\'"))
                btnDeletePhoto.OnClientClick = confirmDelete
            End If

            lblProfilePhotoTitle.Text = CurrentPageData.GetCustomString(lblProfilePhotoTitle.ID)


            'uplImage.ValidationSettings.GeneralErrorText = CurrentPageData.GetCustomString("Uploader.ValidationSettings.GeneralErrorText")
            'uplImage.ValidationSettings.MaxFileSizeErrorText = CurrentPageData.GetCustomString("Uploader.ValidationSettings.MaxFileSizeErrorText")
            'uplImage.ValidationSettings.NotAllowedFileExtensionErrorText = CurrentPageData.GetCustomString("Uploader.ValidationSettings.NotAllowedFileExtensionErrorText")

            SetControlsValue(Me, CurrentPageData)

            'msg_ProfileDefaultPicNote.Text = CurrentPageData.GetCustomString("msg_ProfileDefaultPicNote")
            msg_PubPhotos.Text = CurrentPageData.GetCustomString("msg_PubPhotos")
            msg_PrivatePhotos.Text = CurrentPageData.GetCustomString("msg_PrivatePhotos")
            msg_PrivatePhotosInfo.Text = CurrentPageData.GetCustomString("msg_PrivatePhotosInfo")


            lblAllowAccessPhotosLevel.Text = CurrentPageData.GetCustomString("lblAllowAccessPhotosLevel")
            lblAllowAccessPhotosLevelIcon.Text = CurrentPageData.GetCustomString("lblAllowAccessPhotosLevel")

            popupWindows.HeaderText = CurrentPageData.GetCustomString("popupWindows.PhotoLevelTitle")
            lblPhotoLevel.Text = CurrentPageData.GetCustomString("lblPhotoLevelTitle")
            btnEditDefault.Text = CurrentPageData.GetCustomString("btnEditDefault")


            'Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_PhotosDisplayLevel, Session("LagID"), "PhotosDisplayLevelId", cbPhotosLevel, True, "US")
            'cbPhotosLevel.Items(0).Selected = True


            'Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            'AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    'Protected Sub uplImage_FileUploadComplete(ByVal sender As Object, ByVal e As FileUploadCompleteEventArgs)
    '    SavePostedFile(e)
    'End Sub

    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpload1.Click
        'Page.Form.Enctype = "multipart/form-data"

        Try
            ' Get the HttpFileCollection
            Dim uploadedFilesCollection As HttpFileCollection = Request.Files
            For i As Integer = 0 To uploadedFilesCollection.Count - 1
                Dim uploadedFile As HttpPostedFile = uploadedFilesCollection(i)
                Dim fileName As String = uploadedFile.FileName
                If String.IsNullOrEmpty(fileName) Then
                    uploadStatus.Text = "Παρακαλώ, επιλέξτε φωτογραφίες για ανέβασμα"
                    Return
                End If
                Dim extension As String = uploadedFile.FileName.ToLower().Substring(uploadedFile.FileName.Length - 3)
                If (String.Compare(extension, "jpg", True) = 0) Or
                (String.Compare(extension, "jpeg", True) = 0) Or
                (String.Compare(extension, "gif", True) = 0) Or
                (String.Compare(extension, "png", True) = 0) Or
                (String.Compare(extension, "bmp", True) = 0) Then
                    If (uploadedFile.ContentLength > 0) And (uploadedFile.ContentLength <= 8388608) Then '  4194304 (4MB)

                        'Στο σημείο αυτό, το αρχείο μας έχει περάσει με επιτυχία το Validation...

                        If (clsConfigValues.Get__photos_max_approved() <= DataHelpers.EUS_CustomerPhotos_GetVisiblePhotos(Me.MasterProfileId)) Then
                            uploadStatus.Text = Me.CurrentPageData.GetCustomString("Error.Max.Photos.Limit.Reached")
                        Else
                            uploadStatus.Text = ""
                            SavePostedFile(uploadedFile.FileName, uploadedFile.InputStream)
                        End If
                    Else
                        uploadStatus.Text = CurrentPageData.GetCustomString("Uploader.ValidationSettings.MaxFileSizeErrorText")
                    End If
                Else
                    uploadStatus.Text = CurrentPageData.GetCustomString("Uploader.ValidationSettings.NotAllowedFileExtensionErrorText")
                End If
            Next i
        Catch ex As Exception
        End Try

        ' make redirect to same page to AVOID DOUBLE PHOTOS UPLOAD issue
        ' issue hapens when user makes refresh of page and selects resubmition of previous request
        Try
            Dim url As String = clsCurrentContext.GetRefreshURL()
            Response.Redirect(url)
        Catch ex As System.Threading.ThreadAbortException
        End Try
        ' ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Error_Sending_Receiver_DoNot_Have_Photo", "$(function(){forceRefresh();})", True)

    End Sub

    'Private Function SavePostedFile(ByRef e As FileUploadCompleteEventArgs) As String
    '    Dim uploadedFile As UploadedFile = e.UploadedFile()

    '    If (Not uploadedFile.IsValid) Then
    '        Return String.Empty
    '    End If
    '    If (clsConfigValues.Get__photos_max_approved() <= DataHelpers.EUS_CustomerPhotos_GetVisiblePhotos(Me.MasterProfileId)) Then
    '        e.ErrorText = Me.CurrentPageData.GetCustomString("Error.Max.Photos.Limit.Reached")
    '        Return e.ErrorText
    '    Else
    '        SavePostedFile(uploadedFile.FileName, uploadedFile.FileContent)
    '        e.CallbackData = "OK"
    '        Return e.CallbackData
    '    End If
    'End Function



    Private Function SavePostedFile(strFileName As String, streamFileContent As System.IO.Stream) As String

        Dim imgfilePath As String = ""

        Dim _guid As Guid = Guid.NewGuid()

        Dim ext As String = strFileName
        ext = ext.Substring(ext.LastIndexOf(".") + 1)

        Dim filePath As String = _guid.ToString("N") & "." & ext


        Try

            Dim imgDir As String = String.Format(ProfileHelper.gMemberPhotosDirectory, Me.MasterProfileId.ToString())
            imgDir = MapPath(imgDir)
            If Not System.IO.Directory.Exists(imgDir) Then
                System.IO.Directory.CreateDirectory(imgDir)
            End If
            imgfilePath = Path.Combine(imgDir, filePath)




            Dim original As Image = Nothing
            Try
                original = Image.FromStream(streamFileContent)
                If (original.Height > 1920 OrElse original.Width > 1920) Then
                    original = PhotoUtils.ResizeBitmap(original, 1920, 1920)
                End If
                PhotoUtils.SaveToJpeg(original, imgfilePath)


                Dim thumbfilePath As String = ""
                Dim thumbDir As String = String.Format(ProfileHelper.gMemberPhotosThumbsDirectory, Me.MasterProfileId.ToString())
                thumbDir = MapPath(thumbDir)
                If Not System.IO.Directory.Exists(thumbDir) Then
                    System.IO.Directory.CreateDirectory(thumbDir)
                End If
                thumbfilePath = Path.Combine(thumbDir, filePath)

                Using thumbnail As Image = PhotoUtils.AutosizeImageAndInscribe(original, 250, 250)
                    PhotoUtils.SaveToJpeg(thumbnail, thumbfilePath)
                    thumbnail.Dispose()
                End Using
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "SavePostedFile failed thumbnail 250.")
            Finally
                If (original IsNot Nothing) Then
                    original.Dispose()
                End If
            End Try


            Try

                Dim D150Path As String = ""
                Dim D150 As String = String.Format(ProfileHelper.gMemberPhotosD150, Me.MasterProfileId.ToString())
                D150 = MapPath(D150)
                If Not System.IO.Directory.Exists(D150) Then
                    System.IO.Directory.CreateDirectory(D150)
                End If
                D150Path = Path.Combine(D150, filePath)

                original = Image.FromFile(imgfilePath)
                ' fill image to avoid streching
                Using thumbnail As Image = PhotoUtils.AutosizeImageAndInscribe(original, 150, 150)
                    PhotoUtils.SaveToJpeg(thumbnail, D150Path)
                    thumbnail.Dispose()
                End Using
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "SavePostedFile failed thumbnail 150.")
            Finally
                If (original IsNot Nothing) Then
                    original.Dispose()
                End If
            End Try


            Try
                Dim D350Path As String = ""
                Dim D350 As String = String.Format(ProfileHelper.gMemberPhotosD350, Me.MasterProfileId.ToString())
                D350 = MapPath(D350)
                If Not System.IO.Directory.Exists(D350) Then
                    System.IO.Directory.CreateDirectory(D350)
                End If
                D350Path = Path.Combine(D350, filePath)


                original = Image.FromFile(imgfilePath)
                ' resize proportionally to 350
                Using thumbnail As Image = PhotoUtils.ResizeBitmap(original, 350, 350)
                    PhotoUtils.SaveToJpeg(thumbnail, D350Path)
                    thumbnail.Dispose()
                End Using
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "SavePostedFile failed thumbnail 350.")
            Finally
                If (original IsNot Nothing) Then
                    original.Dispose()
                End If
            End Try

            Try
                If (original IsNot Nothing) Then
                    original.Dispose()
                End If

                streamFileContent.Dispose()
            Catch ex As Exception
            End Try
            'Catch ex As Exception
            '    WebErrorMessageBox(Me, ex, "SavePostedFile failed (1).")
            'End Try


            '' save to database file reference
            'Try

            Dim displayLevel As Integer = 0 ' public
            If (cbPhotosLevel.SelectedItem IsNot Nothing) Then
                displayLevel = cbPhotosLevel.SelectedItem.Value
            End If

            'If (chkPrivePhoto.Checked) Then
            '    displayLevel = 1 'private
            '    chkPrivePhoto.Checked = False
            'End If

            Dim ds As DSMembers = DataHelpers.GetEUS_CustomerPhotos_ByProfileOrMirrorID(0)

            Dim newRow As DSMembers.EUS_CustomerPhotosRow = ds.EUS_CustomerPhotos.NewEUS_CustomerPhotosRow()
            newRow.CustomerPhotosID = -1
            newRow.CustomerID = Me.MasterProfileId
            newRow.DateTimeToUploading = DateTime.UtcNow
            newRow.DisplayLevel = displayLevel
            newRow.FileName = filePath
            newRow.HasAproved = False
            newRow.HasDeclined = False
            newRow.IsDefault = False
            newRow.IsDeleted = False
            newRow.IsAutoApproved = False
            newRow.ShowOnGrid = False
            newRow.ShowOnFrontPage = False
            'newRow.PhotoSimilarity = 0
            ds.EUS_CustomerPhotos.AddEUS_CustomerPhotosRow(newRow)

            DataHelpers.UpdateEUS_CustomerPhotos(ds)
            DataHelpers.UpdateEUS_CustomerPhotos_IsUpdating(Me.MasterProfileId, newRow.CustomerPhotosID, True)

            Dim isAutoApprovedPhoto = False
            'photo auto approve
            Try
                Dim config As New clsConfigValues()
                If (config.auto_approve_photos = "1") Then
                    isAutoApprovedPhoto = True
                    AdminActions.ApprovePhoto(newRow.CustomerPhotosID, False, False, isAutoApprovedPhoto)
                    RefreshProfilePreviewControl(Me.Page)
                End If
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "AutoApprovePhoto failed.")
            End Try


            DataHelpers.UpdateEUS_Profiles_MirrorSetStatusUpdating(Me.MasterProfileId, ProfileStatusEnum.Updating, ProfileModifiedEnum.UpdatingPhotos)

            Dim profileRows As New clsProfileRows(Me.MasterProfileId)
            If (isAutoApprovedPhoto) Then
                Try
                    clsUserNotifications.SendNotificationsToMembers_AboutNewMember(profileRows.GetMasterRow().ProfileID, True)
                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "")
                End Try
            End If

            Try
                SendNotificationEmailToSupport_NewPhoto(profileRows.GetMirrorRow(), newRow, isAutoApprovedPhoto)
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "SavePostedFile failed (2).")
        End Try

        Return imgfilePath
    End Function


    Sub SendNotificationEmailToSupport_NewPhoto(mirrorRow As DSMembers.EUS_ProfilesRow, newPhoto As DSMembers.EUS_CustomerPhotosRow, isAutoApproveOn As Boolean)
        Try

            '    Dim toEmail As String = ConfigurationManager.AppSettings("gToEmail")
            Dim Content As String = globalStrings.VerifyCustomString("EmailSendToSupport_MemberNewPhoto", "US")

            If (isAutoApproveOn) Then
                Content = Content.Replace("###YESNO###", "YES")
            Else
                Content = Content.Replace("###YESNO###", "NO")
            End If

            Content = Content.Replace("###LOGINNAME###", mirrorRow.LoginName)
            Content = Content.Replace("###EMAIL###", mirrorRow.eMail)

            Dim approveUrl As String = ConfigurationManager.AppSettings("gApprovePhotoURL")
            Dim rejectUrl As String
            approveUrl = String.Format(approveUrl, newPhoto.CustomerPhotosID)
            rejectUrl = approveUrl & "&reject=1"

            'If (isAutoApproveOn) Then
            '    Content = Content.Replace("###APPROVEPHOTOURL###", "")
            '    Content = Content.Replace("###REJECTPHOTOURL###", "")
            'Else
            Content = Content.Replace("###APPROVEPHOTOURL###", approveUrl)
            Content = Content.Replace("###REJECTPHOTOURL###", rejectUrl)
            'End If

            Try
                'Dim siteUrl As String = ConfigurationManager.AppSettings("gSiteURL")
                Dim photoUrl As String = ""
                photoUrl = ProfileHelper.GetProfileImageURL(newPhoto.CustomerID, newPhoto.FileName, mirrorRow.GenderId, True, Me.IsHTTPS, PhotoSize.D350)
                'photoUrl = (siteUrl & photoUrl)

                Content = Content.Replace("###FILEPATH###", photoUrl)
                Content = Content.Replace("blank###PHOTOURL###", photoUrl) ' cms editor issue, adds word blank
                Content = Content.Replace("###PHOTOURL###", photoUrl)
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "default photo")
            End Try


            Try
                Content = Content.Replace("###BIRTHDATE###", mirrorRow.Birthday)
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###AGE###", ProfileHelper.GetCurrentAge(mirrorRow.Birthday))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###GENDER###", ProfileHelper.GetGenderString(mirrorRow.GenderId, "US"))
                Content = Content.Replace("###SEX###", ProfileHelper.GetGenderString(mirrorRow.GenderId, "US"))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###COUNTRY###", ProfileHelper.GetCountryName(mirrorRow.Country))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###STATEREGION###", mirrorRow._Region)
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###CITY###", mirrorRow.City)
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###ZIP###", mirrorRow.Zip)
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###DATETOREGISTER###", mirrorRow.DateTimeToRegister)
            Catch ex As Exception
            End Try


            Try
                Content = Content.Replace("###PROFILEAPPROVEDYESNO###", IIf(mirrorRow.Status = 4, "YES", "NO"))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###APPROVEDPHOTOSYESNO###", "NO")
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###AUTOAPPROVEDPHOTOYESNO###", "NO")
            Catch ex As Exception
            End Try


            Try
                Content = Content.Replace("###IP###", clsHTMLHelper.CreateIPLookupLinks(Session("IP")))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###GEOIP###", Session("GEO_COUNTRY_CODE"))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###AGENT###", Request.Params("HTTP_USER_AGENT"))
            Catch ex As Exception
            End Try
            Try
                'Dim _uri As Uri = Nothing

                'Try
                '    _uri = New Uri(Request.Params("HTTP_REFERER"))
                'Catch ex As Exception
                'End Try
                'If (_uri IsNot Nothing) Then
                '    Content = Content.Replace("###REFERRER###", _uri.Host & ":" & _uri.Port)
                'Else
                Content = Content.Replace("###REFERRER###", clsHTMLHelper.CreateURLLink(IIf(Session("Referrer") IsNot Nothing, Session("Referrer"), "")))
                'End If
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###CUSTOMREFERRER###", Session("CustomReferrer"))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###CUSTOMERID###", mirrorRow.ProfileID)
            Catch ex As Exception
            End Try
            Try
                Dim link As String = ConfigurationManager.AppSettings("gSiteURL") & _
                    "?logon_customer=" & mirrorRow.MirrorProfileID & "_" & mirrorRow.ProfileID
                Content = Content.Replace("###LOGONCUSTOMER###", link)
            Catch ex As Exception
            End Try


            Try
                Dim SearchEngineKeywords As String = IIf(Session("SearchEngineKeywords") Is Nothing, "", Session("SearchEngineKeywords"))
                Content = Content.Replace("###SEARCHENGINEKEYWORDS###", SearchEngineKeywords)
            Catch ex As Exception
            End Try


            Try
                Dim LandingPage As String = ""
                If (Not mirrorRow.IsLandingPageNull()) Then
                    LandingPage = mirrorRow.LandingPage
                    If (LandingPage Is Nothing) Then LandingPage = ""
                End If
                Content = Content.Replace("###LANDINGPAGE###", clsHTMLHelper.CreateURLLink(LandingPage))
            Catch ex As Exception
            End Try

            If (SupportAppHelper.IsEmailSendRunning()) Then
                Dim EmailAction As String = "New.Photo.Google.Check.ID-" & newPhoto.CustomerPhotosID
                clsMyMail.SendMailFromQueue(ConfigurationManager.AppSettings("gToEmail"), "New photos uploaded on Goomena.com", Content, mirrorRow.ProfileID, EmailAction, True, Nothing, Nothing)
            Else
                clsMyMail.SendMail(ConfigurationManager.AppSettings("gToEmail"), "New photos uploaded on Goomena.com", Content, True)
            End If


        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


    Sub SendNotificationEmailToSupport_DeletedPhoto(mirrorRow As DSMembers.EUS_ProfilesRow, newPhoto As DSMembers.EUS_CustomerPhotosRow)
        Try

            '     Dim toEmail As String = ConfigurationManager.AppSettings("gToEmail")
            Dim Content As String = globalStrings.GetCustomString("EmailSendToSupport_MemberDeletedPhoto", "US")


            Content = Content.Replace("###LOGINNAME###", mirrorRow.LoginName)
            Content = Content.Replace("###EMAIL###", mirrorRow.eMail)


            Try
                Dim photoUrl As String = ""
                photoUrl = ProfileHelper.GetProfileImageURL(newPhoto.CustomerID, newPhoto.FileName, mirrorRow.GenderId, True, Me.IsHTTPS)

                Content = Content.Replace("###FILEPATH###", photoUrl)
                Content = Content.Replace("blank###PHOTOURL###", photoUrl) ' cms editor issue, adds word blank
                Content = Content.Replace("###PHOTOURL###", photoUrl)
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "default photo")
            End Try


            Try
                Content = Content.Replace("###BIRTHDATE###", mirrorRow.Birthday)
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###AGE###", ProfileHelper.GetCurrentAge(mirrorRow.Birthday))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###GENDER###", ProfileHelper.GetGenderString(mirrorRow.GenderId, "US"))
                Content = Content.Replace("###SEX###", ProfileHelper.GetGenderString(mirrorRow.GenderId, "US"))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###COUNTRY###", ProfileHelper.GetCountryName(mirrorRow.Country))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###STATEREGION###", mirrorRow._Region)
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###CITY###", mirrorRow.City)
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###ZIP###", mirrorRow.Zip)
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###DATETOREGISTER###", mirrorRow.DateTimeToRegister)
            Catch ex As Exception
            End Try

            Try
                Content = Content.Replace("###IP###", clsHTMLHelper.CreateIPLookupLinks(Session("IP")))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###GEOIP###", Session("GEO_COUNTRY_CODE"))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###AGENT###", Request.Params("HTTP_USER_AGENT"))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###REFERRER###", clsHTMLHelper.CreateURLLink(IIf(Session("Referrer") IsNot Nothing, Session("Referrer"), "")))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###CUSTOMREFERRER###", Session("CustomReferrer"))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###CUSTOMERID###", mirrorRow.ProfileID)
            Catch ex As Exception
            End Try
            Try
                Dim link As String = ConfigurationManager.AppSettings("gSiteURL") & _
                    "?logon_customer=" & mirrorRow.MirrorProfileID & "_" & mirrorRow.ProfileID
                Content = Content.Replace("###LOGONCUSTOMER###", link)
            Catch ex As Exception
            End Try


            Try
                Dim SearchEngineKeywords As String = IIf(Session("SearchEngineKeywords") Is Nothing, "", Session("SearchEngineKeywords"))
                Content = Content.Replace("###SEARCHENGINEKEYWORDS###", SearchEngineKeywords)
            Catch ex As Exception
            End Try


            Try
                Dim LandingPage As String = ""
                If (Not mirrorRow.IsLandingPageNull()) Then
                    LandingPage = mirrorRow.LandingPage
                    If (LandingPage Is Nothing) Then LandingPage = ""
                End If
                Content = Content.Replace("###LANDINGPAGE###", clsHTMLHelper.CreateURLLink(LandingPage))
            Catch ex As Exception
            End Try


            clsMyMail.SendMail(ConfigurationManager.AppSettings("gToEmail"), "Photo deleted on Goomena.com", Content, True)

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


    'Sub SendNotificationEmailToSupport_old(mirrorRow As DSMembers.EUS_ProfilesRow, newPhoto As DSMembers.EUS_CustomerPhotosRow, isAutoApproveOn As Boolean)
    '    Try

    '        Dim toEmail As String = ConfigurationManager.AppSettings("gToEmail")
    '        Dim Content As String = globalStrings.GetCustomString("EmailSendToSupport_MemberNewPhoto", "US")

    '        If (isAutoApproveOn) Then
    '            Content = Content.Replace("###YESNO###", "YES")
    '        Else
    '            Content = Content.Replace("###YESNO###", "NO")
    '        End If

    '        Content = Content.Replace("###LOGINNAME###", mirrorRow.LoginName)
    '        Content = Content.Replace("###EMAIL###", mirrorRow.eMail)
    '        Content = Content.Replace("###GENDER###", ProfileHelper.GetGenderString(mirrorRow.GenderId, "US"))

    '        Try
    '            Dim siteUrl As String = ConfigurationManager.AppSettings("gSiteURL")
    '            Dim photoUrl As String = ""
    '            photoUrl = ProfileHelper.GetProfileImageURL(newPhoto.CustomerID, newPhoto.FileName, mirrorRow.GenderId, True)
    '            photoUrl = (siteUrl & photoUrl)

    '            Content = Content.Replace("###FILEPATH###", photoUrl)
    '            Content = Content.Replace("blank###PHOTOURL###", photoUrl) ' cms editor issue, adds word blank
    '            Content = Content.Replace("###PHOTOURL###", photoUrl)
    '        Catch ex As Exception
    '            WebErrorMessageBox(Me, ex, "default photo")
    '        End Try



    '        Dim approveUrl As String = ConfigurationManager.AppSettings("gApprovePhotoURL")
    '        Dim rejectUrl As String
    '        approveUrl = String.Format(approveUrl, newPhoto.CustomerPhotosID)
    '        rejectUrl = approveUrl & "&reject=1"

    '        ' allow reject photo after auto approve
    '        If (isAutoApproveOn) Then
    '            Content = Content.Replace("###APPROVEPHOTOURL###", "")
    '            Content = Content.Replace("###REJECTPHOTOURL###", rejectUrl)
    '        Else
    '            Content = Content.Replace("###APPROVEPHOTOURL###", approveUrl)
    '            Content = Content.Replace("###REJECTPHOTOURL###", rejectUrl)
    '        End If


    '        '"New photos uploaded by member " & Me.SessionVariables.DataRecordLoginMemberReturn.LoginName & ". Filename " & filePath
    '        clsMyMail.SendMail(toEmail, "New photos uploaded on Goomena.com", Content, True)

    '    Catch ex As Exception
    '        WebErrorMessageBox(Me, ex, "")
    '    End Try
    'End Sub


    Private Sub LoadPhotos()


        Try

            ' true when user makes logoff on photos page
            If (Me.GetCurrentProfile(True) Is Nothing) Then Return


            imgProfilePhoto.ImageUrl = ""
            btnDeletePhoto.CommandArgument = 0
            btnDeletePhoto.Visible = True



            Dim DeleteButtonText As String = CurrentPageData.GetCustomString("btnDelete")
            Dim DeleteButtonClientClickText As String = "if(!confirm('" & CurrentPageData.GetCustomString("btnDeleteClientClick").Replace("'", "\'") & "')) {return false;}else{ShowLoading();}"
            Dim DefaultButtonText As String = CurrentPageData.GetCustomString("btnDefault")
            Dim NotApprovedPhotoInfoText As String = CurrentPageData.GetCustomString("NotApprovedPhotoInfoText")
            Dim DeclinedPhotoInfoText As String = CurrentPageData.GetCustomString("DeclinedPhotoInfoText")
            Dim NotApprovedPrivatePhotoInfoText As String = CurrentPageData.GetCustomString("NotApprovedPrivatePhotoInfoText")
            Dim EditButtonText As String = CurrentPageData.GetCustomString("EditButtonText")


            'Dim profileId As Integer = Me.Session("ProfileID")
            Dim ds As DSMembers = DataHelpers.GetEUS_CustomerPhotos_ByProfileOrMirrorID(Me.MasterProfileId)

            Dim FacebookUserName As String = Me.GetCurrentProfile().FacebookUserName
            Dim FacebookPhoto As Boolean = clsNullable.NullTo(Me.GetCurrentProfile().FacebookPhoto)
            If (Not String.IsNullOrEmpty(FacebookUserName)) Then FacebookUserName = FacebookUserName.Trim()

            If (ds.EUS_CustomerPhotos.Rows.Count = 0 AndAlso Not String.IsNullOrEmpty(FacebookUserName) AndAlso FacebookPhoto = False) Then
                Dim url As String = "https://graph.facebook.com/" & FacebookUserName & "/picture?width=1200&height=1200"
                Try

                    Using webresponse As System.Net.HttpWebResponse = clsWebPageProcessor.GetWebResponse(url)
                        If (webresponse.ContentType.StartsWith("image")) Then
                            Dim ext As String = webresponse.ContentType.Replace("image/", "")
                            SavePostedFile("fbImage." & ext, webresponse.GetResponseStream())
                            FacebookPhoto = True
                        End If
                    End Using
                Catch ex As Exception
                    WebErrorSendEmail(ex, "PhotosEdit->LoadPhotos->Read from facebook (url-->" & url & ")")
                End Try
            End If

            If (Not String.IsNullOrEmpty(FacebookUserName) AndAlso Me.GetCurrentProfile().FacebookPhoto Is Nothing) Then
                DataHelpers.UpdateEUS_Profiles_FacebookPhoto(Me.MasterProfileId, FacebookPhoto)
                Me.GetCurrentProfile().FacebookPhoto = FacebookPhoto
                ds = DataHelpers.GetEUS_CustomerPhotos_ByProfileOrMirrorID(Me.MasterProfileId)
            End If

            Dim approvedPhotosCount As Integer = 0

            Using dataTbl As New DataTable()


                dataTbl.Columns.Add("ImageId")
                dataTbl.Columns.Add("ImageUrl")
                dataTbl.Columns.Add("ImageThumbUrl")
                dataTbl.Columns.Add("IsDefault")
                dataTbl.Columns.Add("ImageCss")
                dataTbl.Columns.Add("DeleteButtonText")
                dataTbl.Columns.Add("DeleteButtonClientClickText")
                dataTbl.Columns.Add("DefaultButtonText")
                dataTbl.Columns.Add("DefaultVisible")
                dataTbl.Columns.Add("InfoVisible")
                dataTbl.Columns.Add("NotApprovedPhotoInfoText")
                dataTbl.Columns.Add("DisplayLevel")
                dataTbl.Columns.Add("EditButtonText")
                dataTbl.Columns.Add("EditButtonVisible")
                dataTbl.Columns.Add("InfoTextClass")


                ' bind public photos list
                Dim foundRows As DSMembers.EUS_CustomerPhotosRow() = ds.EUS_CustomerPhotos.Select("DisplayLevel = 0")
                For Each row As DSMembers.EUS_CustomerPhotosRow In foundRows
                    Try

                        Dim dr As DataRow = dataTbl.NewRow()
                        dr("EditButtonVisible") = True
                        dr("DisplayLevel") = row.DisplayLevel
                        dr("ImageId") = row.CustomerPhotosID

                        dr("ImageUrl") = ProfileHelper.GetProfileImageURL(Me.MasterProfileId, row.FileName, Me.SessionVariables.MemberData.GenderId, False, Me.IsHTTPS)
                        dr("ImageThumbUrl") = ProfileHelper.GetProfileImageURL(Me.MasterProfileId, row.FileName, Me.SessionVariables.MemberData.GenderId, True, Me.IsHTTPS)

                        Dim isDefault As Boolean =
                            Not row.IsIsDefaultNull() AndAlso
                            row.IsDefault AndAlso
                            Not row.IsHasAprovedNull() AndAlso
                            row.HasAproved AndAlso
                            (row.IsHasDeclinedNull() OrElse Not row.HasDeclined) AndAlso
                            (row.IsIsDeletedNull() OrElse Not row.IsDeleted)

                        If (isDefault) Then
                            dr("IsDefault") = isDefault
                            imgProfilePhoto.ImageUrl = ProfileHelper.GetProfileImageURL(Me.MasterProfileId, row.FileName, Me.SessionVariables.MemberData.GenderId, True, Me.IsHTTPS)
                            btnDeletePhoto.CommandArgument = dr("ImageId")
                        Else
                            dr("IsDefault") = False
                        End If



                        If (row.HasAproved) Then
                            dr("DefaultVisible") = (dr("IsDefault") = False)
                        Else
                            dr("DefaultVisible") = False
                        End If

                        If (row.HasDeclined) Then
                            dr("InfoVisible") = True
                            dr("NotApprovedPhotoInfoText") = DeclinedPhotoInfoText
                            dr("EditButtonVisible") = False
                            dr("InfoTextClass") = " photo-declined"
                            dr("ImageCss") = dr("ImageCss") & " photo-declined"
                        ElseIf (Not row.HasAproved) Then
                            dr("InfoVisible") = True
                            dr("NotApprovedPhotoInfoText") = NotApprovedPhotoInfoText
                            dr("InfoTextClass") = " waitng-approval"
                            dr("ImageCss") = dr("ImageCss") & " waitng-approval"
                        Else
                            dr("InfoVisible") = False
                            approvedPhotosCount = approvedPhotosCount + 1
                        End If


                        If (foundRows.Count > 1 AndAlso row.CustomerPhotosID = foundRows(foundRows.Count - 1).CustomerPhotosID) Then
                            dr("ImageCss") = dr("ImageCss") & " last"
                        End If

                        dr("DeleteButtonText") = DeleteButtonText
                        dr("DeleteButtonClientClickText") = DeleteButtonClientClickText
                        dr("DefaultButtonText") = DefaultButtonText
                        dr("EditButtonText") = EditButtonText

                        dataTbl.Rows.Add(dr)
                    Catch ex As Exception
                        WebErrorMessageBox(Me, ex, "")
                    End Try
                Next


                lvPubPhotos.DataSource = dataTbl
                lvPubPhotos.DataBind()

                If (dataTbl.Rows.Count = 0 OrElse approvedPhotosCount = 0) Then
                    NoPhotoAvailable = True
                Else
                    NoPhotoAvailable = False
                End If


                dataTbl.Clear()
                approvedPhotosCount = 0

                ' bind private photos list
                foundRows = ds.EUS_CustomerPhotos.Select("DisplayLevel > 0")
                For Each row As DSMembers.EUS_CustomerPhotosRow In foundRows
                    Try
                        Dim dr As DataRow = dataTbl.NewRow()
                        dr("EditButtonVisible") = True
                        dr("DisplayLevel") = row.DisplayLevel
                        dr("ImageId") = row.CustomerPhotosID

                        Dim path As String = String.Format(ProfileHelper.gMemberPhotosDirectoryView, Me.MasterProfileId)
                        If Me.IsHTTPS Then path = "https" & path.Remove(0, "http".Length)
                        dr("ImageUrl") = path & "/" & row.FileName

                        Dim thumbPath As String = String.Format(ProfileHelper.gMemberPhotosThumbsDirectoryView, Me.MasterProfileId)
                        If Me.IsHTTPS Then thumbPath = "https" & thumbPath.Remove(0, "http".Length)
                        dr("ImageThumbUrl") = thumbPath & "/" & row.FileName


                        dr("IsDefault") = False

                        If (row.HasDeclined) Then
                            dr("InfoVisible") = True
                            dr("NotApprovedPhotoInfoText") = DeclinedPhotoInfoText
                            dr("EditButtonVisible") = False
                            dr("InfoTextClass") = " photo-declined"
                        ElseIf (Not row.HasAproved) Then
                            dr("InfoVisible") = True
                            dr("NotApprovedPhotoInfoText") = NotApprovedPrivatePhotoInfoText
                            dr("InfoTextClass") = " waitng-approval"
                        Else
                            dr("InfoVisible") = False
                            approvedPhotosCount = approvedPhotosCount + 1
                        End If


                        If (foundRows.Count > 1 AndAlso row.CustomerPhotosID = foundRows(foundRows.Count - 1).CustomerPhotosID) Then
                            dr("ImageCss") = dr("ImageCss") & " last"
                        End If

                        dr("DeleteButtonText") = DeleteButtonText
                        dr("DeleteButtonClientClickText") = DeleteButtonClientClickText
                        dr("DefaultButtonText") = DefaultButtonText
                        dr("EditButtonText") = EditButtonText

                        dataTbl.Rows.Add(dr)
                    Catch ex As Exception
                        WebErrorMessageBox(Me, ex, "")
                    End Try
                Next


                lvPrivePhotos.DataSource = dataTbl
                lvPrivePhotos.DataBind()


                If (imgProfilePhoto.ImageUrl = "") Then
                    If ProfileHelper.IsMale(Me.SessionVariables.MemberData.GenderId) Then
                        imgProfilePhoto.ImageUrl = ProfileHelper.Male_DefaultImage
                    ElseIf ProfileHelper.IsFemale(Me.SessionVariables.MemberData.GenderId) Then
                        imgProfilePhoto.ImageUrl = ProfileHelper.Female_DefaultImage
                    End If
                    btnDeletePhoto.Visible = False
                End If

                If (NoPhotoAvailable) Then
                    If (dataTbl.Rows.Count = 0 OrElse approvedPhotosCount = 0) Then
                        NoPhotoAvailable = True
                    Else
                        NoPhotoAvailable = False
                    End If
                End If
            End Using
            'If (dataTbl.Rows.Count = 0) Then
            '    divNoPhoto.Visible = True
            'Else
            '    divNoPhoto.Visible = False
            'End If

            'If (divNoPhoto.Visible) Then
            '    If (dataTbl.Rows.Count = 0) Then
            '        divNoPhoto.Visible = True
            '    Else
            '        divNoPhoto.Visible = False
            '    End If
            'End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Failed to load photos")
        End Try

    End Sub



    Protected Sub lvPubPhotos_ItemCommand(sender As Object, e As System.Web.UI.WebControls.ListViewCommandEventArgs) Handles lvPubPhotos.ItemCommand
        Try
            Dim photoId As Integer
            Integer.TryParse(e.CommandArgument.ToString(), photoId)
            Dim commandName As String = e.CommandName.ToUpper()
            ExecCmd(commandName, photoId)

            Dim FileName As String = Nothing
            Dim dr As DSMembers.EUS_CustomerPhotosRow = DataHelpers.GetProfilesDefaultPhoto(Me.MasterProfileId)
            If (dr IsNot Nothing) Then FileName = dr.FileName

            Dim path As String = ProfileHelper.GetProfileImageURL(Me.MasterProfileId, FileName, Me.SessionVariables.MemberData.GenderId, True, Me.IsHTTPS, PhotoSize.Thumb)
            hdfDefaultPath.Value = path
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "lvPubPhotos_ItemCommand")
        End Try
    End Sub


    Protected Sub lvPrivePhotos_ItemCommand(sender As Object, e As System.Web.UI.WebControls.ListViewCommandEventArgs) Handles lvPrivePhotos.ItemCommand
        Try
            Dim photoId As Integer
            Integer.TryParse(e.CommandArgument.ToString(), photoId)
            Dim commandName As String = e.CommandName.ToUpper()
            ExecCmd(commandName, photoId)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "lvPrivePhotos_ItemCommand")
        End Try
    End Sub


    Protected Sub btnDeletePhoto_Command(sender As Object, e As System.Web.UI.WebControls.CommandEventArgs) Handles btnDeletePhoto.Command
        Try
            Dim photoId As Integer
            Integer.TryParse(e.CommandArgument.ToString(), photoId)
            Dim commandName As String = e.CommandName.ToUpper()
            ExecCmd(commandName, photoId)

            Dim FileName As String = Nothing
            Dim dr As DSMembers.EUS_CustomerPhotosRow = DataHelpers.GetProfilesDefaultPhoto(Me.MasterProfileId)
            If (dr IsNot Nothing) Then FileName = dr.FileName

            Dim path As String = ProfileHelper.GetProfileImageURL(Me.MasterProfileId, FileName, Me.SessionVariables.MemberData.GenderId, True, Me.IsHTTPS, PhotoSize.Thumb)
            hdfDefaultPath.Value = path

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "btnDeletePhoto_Command")
        End Try
    End Sub


    Private Sub ExecCmd(commandName As String, photoId As Integer)
        Try
            'Dim profileId As Integer = Me.Session("ProfileID")
            Select Case (commandName)

                Case "DELETE"

                    Dim ds As DSMembers = DataHelpers.GetEUS_CustomerPhotosByCustomerPhotosID(photoId)
                    Dim deletedPhoto As DSMembers.EUS_CustomerPhotosRow = ds.EUS_CustomerPhotos.Rows(0)
                    '     Dim fileName As String = ds.EUS_CustomerPhotos.Rows(0).Field(Of String)("FileName")
                    Dim isDefault As Boolean = False
                    If (Not ds.EUS_CustomerPhotos.Rows(0).IsNull("IsDefault")) Then
                        isDefault = ds.EUS_CustomerPhotos.Rows(0).Field(Of Boolean)("IsDefault")
                    End If

                    ds.EUS_CustomerPhotos.Rows(0).SetField("IsDeleted", True)
                    DataHelpers.UpdateEUS_CustomerPhotos(ds)

                    Try
                        'clsMyMail.SendMail(ConfigurationManager.AppSettings("gToEmail"), "Photo deleted on Goomena.com", "Photo is deleted. Filename " & fileName & ". Delete by " & Me.SessionVariables.MemberData.LoginName)
                        Dim profileRows As New clsProfileRows(Me.MasterProfileId)
                        SendNotificationEmailToSupport_DeletedPhoto(profileRows.GetMirrorRow(), deletedPhoto)
                    Catch ex As Exception
                        WebErrorMessageBox(Me, ex, "ExecCmd")
                    End Try


                    If (isDefault) Then
                        ' set next approved public photo as default
                        ProfileHelper.SetNextDefaultPhoto(Me.MasterProfileId)
                        RefreshProfilePreviewControl(Me.Page)
                    End If

                    ds.Dispose()

                Case "DEFAULT"
                    'Dim hasDefaultChanged As Boolean = False
                    'Dim ds As DSMembers = DataHelpers.GetEUS_CustomerPhotos_ByProfileOrMirrorID(Me.MasterProfileId)
                    'For Each rawRecord As DSMembers.EUS_CustomerPhotosRow In ds.EUS_CustomerPhotos.Rows
                    '    If (rawRecord.CustomerPhotosID = photoId AndAlso rawRecord.HasAproved = True) Then
                    '        rawRecord.IsDefault = True
                    '        hasDefaultChanged = True
                    '    Else
                    '        rawRecord.IsDefault = False
                    '    End If
                    'Next
                    'DataHelpers.UpdateEUS_CustomerPhotos(ds)
                    'ds.Dispose()

                    Dim CustomerPhotosID As Long = DataHelpers.EUS_CustomerPhotos_SetDefault(Me.MasterProfileId, photoId, True, False)
                    If (CustomerPhotosID > 0) Then
                        clsCustomer.AddFreeRegistrationCredits_WithCheck(Me.MasterProfileId, "", "", "")
                        clsUserDoes.Update_MobileNotifications(Me.MasterProfileId, Me.MasterProfileId, MemberActionTypeEnum.DefaultPhotoCanged)
                    End If

                    RefreshProfilePreviewControl(Me.Page)

                    If (CustomerPhotosID > 0) Then
                        DataHelpers.UpdateEUS_Profiles_MirrorSetStatusUpdating(Me.MasterProfileId, ProfileStatusEnum.Updating, ProfileModifiedEnum.UpdatingPhotos)
                        DataHelpers.UpdateEUS_CustomerPhotos_IsUpdating(Me.MasterProfileId, CustomerPhotosID, True)
                        'Dim profileRows As New clsProfileRows(Me.MasterProfileId)
                        'If (profileRows.GetMirrorRow().Status = ProfileStatusEnum.Approved) Then
                        '    profileRows.GetMirrorRow().Status = ProfileStatusEnum.Updating
                        '    profileRows.Update()
                        'End If
                    End If
            End Select

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "ExecCmd")
        End Try
    End Sub

    '!!! leave this handler to avoid error on scriptmanager, it used in it's event validation
    Protected Sub lvPubPhotos_ItemDeleting(sender As Object, e As System.Web.UI.WebControls.ListViewDeleteEventArgs) Handles lvPubPhotos.ItemDeleting

    End Sub

    '!!! leave this handler to avoid error on scriptmanager, it used in it's event validation
    Protected Sub lvPrivePhotos_ItemDeleting(sender As Object, e As System.Web.UI.WebControls.ListViewDeleteEventArgs) Handles lvPrivePhotos.ItemDeleting

    End Sub


    Protected Sub lvPrivePhotos_DataBound(sender As Object, e As EventArgs) Handles lvPrivePhotos.DataBound

        Try

            If (lvPrivePhotos.Items.Count > 0) Then
                For Each itm As ListViewDataItem In lvPrivePhotos.Items


                    Try
                        Dim hdfDisplayLevel As HiddenField = itm.FindControl("hdfDisplayLevel")
                        Dim hdfImageID As HiddenField = itm.FindControl("hdfImageID")

                        'Dim _cbPhotosLevel1 As ASPxComboBox = itm.FindControl("cbPhotosLevel")
                        ''Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_PhotosDisplayLevel, Session("LagID"), "PhotosDisplayLevelId", _cbPhotosLevel1, True, "US")

                        Dim editPhotoWindow As ASPxPopupControl = itm.FindControl("editPhotoWindow")
                        Dim _cbPhotosLevel1 As ASPxComboBox = editPhotoWindow.Windows(0).FindControl("cbPhotosLevel")
                        _cbPhotosLevel1.ID = "cbPhotosLevel_" & hdfImageID.Value
                        editPhotoWindow.ClientInstanceName = editPhotoWindow.ID & "_" & hdfImageID.Value

                        If (Not ClsCombos.SelectComboItem(_cbPhotosLevel1, hdfDisplayLevel.Value)) Then
                            For Each lei As ListEditItem In _cbPhotosLevel1.Items
                                If (lei.Value > 0) Then
                                    lei.Selected = True
                                    Exit For
                                End If
                            Next
                        End If

                        Dim lblInfoInsidePopup As Label = editPhotoWindow.Windows(0).FindControl("lblInfoInsidePopup")
                        lblInfoInsidePopup.Text = Me.CurrentPageData.GetCustomString("lblInfoInsidePopupChangingLevel")
                        editPhotoWindow.HeaderText = Me.CurrentPageData.GetCustomString("popupWindows.PhotoLevelTitle")
                    Catch ex As Exception
                        WebErrorMessageBox(Me, ex, "")
                    End Try


                Next
            End If

            LoadLAG()
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Protected Sub lvPubPhotos_DataBound(sender As Object, e As EventArgs) Handles lvPubPhotos.DataBound

        Try

            If (lvPubPhotos.Items.Count > 0) Then
                For Each itm As ListViewDataItem In lvPubPhotos.Items


                    Try
                        Dim hdfDisplayLevel As HiddenField = itm.FindControl("hdfDisplayLevel")
                        Dim hdfImageID As HiddenField = itm.FindControl("hdfImageID")

                        'Dim _cbPhotosLevel1 As ASPxComboBox = itm.FindControl("cbPhotosLevel")
                        ''Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_PhotosDisplayLevel, Session("LagID"), "PhotosDisplayLevelId", _cbPhotosLevel1, True, "US")
                        '

                        Dim editPhotoWindow As ASPxPopupControl = itm.FindControl("editPhotoWindow")
                        Dim _cbPhotosLevel1 As ASPxComboBox = editPhotoWindow.Windows(0).FindControl("cbPhotosLevel")
                        _cbPhotosLevel1.ID = "cbPhotosLevel_" & hdfImageID.Value
                        editPhotoWindow.ClientInstanceName = editPhotoWindow.ID & "_" & hdfImageID.Value

                        If (Not ClsCombos.SelectComboItem(_cbPhotosLevel1, hdfDisplayLevel.Value)) Then
                            For Each lei As ListEditItem In _cbPhotosLevel1.Items
                                If (lei.Value = 0) Then
                                    lei.Selected = True
                                    Exit For
                                End If
                            Next
                        End If

                        Dim lblInfoInsidePopup As Label = editPhotoWindow.Windows(0).FindControl("lblInfoInsidePopup")
                        lblInfoInsidePopup.Text = Me.CurrentPageData.GetCustomString("lblInfoInsidePopupChangingLevel")

                    Catch ex As Exception
                        WebErrorMessageBox(Me, ex, "")
                    End Try


                Next
            End If

            LoadLAG()
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Protected Sub cbpnlPhotos_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cbpnlPhotos.Callback
        Dim ph As New ProfileHelper()
        Try
            Dim ctlid As String = e.Parameter.Replace("ctlid_", "")
            ctlid = ctlid.Remove(ctlid.IndexOf("_lvl_"))
            ctlid = ctlid.Remove(0, ctlid.IndexOf("_") + 1)

            Dim lvl As String = e.Parameter.Substring(e.Parameter.IndexOf("_lvl_") + Len("_lvl_"))


            AddHandler ph.OnExceptionThrowed, AddressOf ProfileHelper_OnExceptionThrowed
            ph.SetPhotoLevel(Me.MasterProfileId, ctlid, lvl)
            LoadLAG()
            LoadPhotos()
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        Finally
            If ph IsNot Nothing Then
                RemoveHandler ph.OnExceptionThrowed, AddressOf ProfileHelper_OnExceptionThrowed
                ph = Nothing
            End If
        End Try

    End Sub

    Public Sub ProfileHelper_OnExceptionThrowed(ByVal ex As Exception, ByVal ExtraMessage As String)
        WebErrorMessageBox(Me, ex, ExtraMessage)
    End Sub


End Class
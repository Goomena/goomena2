﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class ucCouponPreview

    '''<summary>
    '''lblPreviewCouponTitle control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPreviewCouponTitle As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''lblPreviewCouponNotes1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPreviewCouponNotes1 As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''lblPreviewCouponNotes2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPreviewCouponNotes2 As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''lblCouponFirstLine control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblCouponFirstLine As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblPreviewCouponCredits control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPreviewCouponCredits As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblCouponCreditsΤext control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblCouponCreditsΤext As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblPreviewCouponName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPreviewCouponName As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblCouponBotomNotes control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblCouponBotomNotes As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblCouponMoney control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblCouponMoney As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblCouponCode control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblCouponCode As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''btnDownload control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnDownload As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''lblDownloadText control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblDownloadText As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''btnResend control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnResend As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''lblResendText control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblResendText As Global.System.Web.UI.WebControls.Label
End Class

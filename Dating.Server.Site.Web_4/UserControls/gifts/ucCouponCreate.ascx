﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucCouponCreate.ascx.vb" 
    Inherits="Dating.Server.Site.Web.ucCouponCreate" %>
               <script type="text/javascript">
                  lnkGetCreditsID2 = '<%= lblCouponMoney.ClientID%>';
                lnkGetCreditsText2 = '';

    </script>
<div class="createCouponContainer">
       <div class="createCouponTitle">
              <div class="Toprow">
                  <div class="lblcreateCouponTitle">
                      <asp:literal ID="lblcreateCouponTitle"  runat="server" Text="Βήμα1">

                   </asp:literal>
                       <asp:HyperLink ID="hpMoreInfoTitle" CssClass="btnhpMoreInfoTitle" runat="server">

                       </asp:HyperLink></div>
                </div>
           <div class="SecondRow">
                    <asp:Label ID="lblcreateCouponNotes" CssClass="lblcreateCouponNotes" runat="server" Text="Συμπληρωσε">

                           </asp:Label>
               </div>

          </div>

       <div class="CouponArea">
           <div class="lfloat Editable">
           <div class="Couponcredits">
               <div class="lfloat">
                         <asp:Label ID="lblCouponCredits"  CssClass="lblCouponCredits" runat="server" Text="140 Credits">

                           </asp:Label>

               </div>
                   <div class="lfloat">
                               
                       <div class="HorLineInButtons">

                       </div>
         
                         <dx:ASPxSpinEdit ID="spinCredits2" runat="server" Height="36px" Number="10" Width="150px"
                                Font-Bold="True"  CssClass="spin-control" DisplayFormatString="# ' Credits'" Increment="10" LargeIncrement="20" MinValue="10"  MaxValue="10000">
                                <SpinButtons>
                                    <IncrementImage Url="~/Images2/gifts/white-arrow-up.png">
                                    </IncrementImage>
                               
                                    <DecrementImage Url="~/Images2/gifts/white-arrow-down.png">
                                    </DecrementImage>
                                </SpinButtons>
                                <IncrementButtonStyle HorizontalAlign="Center" Width="26px">
                                    <Border BorderStyle="None" />
                                </IncrementButtonStyle>
                                <DecrementButtonStyle HorizontalAlign="Center" Width="26px">
                                    <Border BorderStyle="None" />
                                </DecrementButtonStyle>
                                <Border BorderStyle="None" />
                                <ClientSideEvents Init="spinCredits_Init2" ValueChanged="spinCredits_ValueChanged2" />
                                     
                          </dx:ASPxSpinEdit>
                       
                   </div>
                       <div class="lfloat"> 
                           <asp:HyperLink ID="CouponCreditsInfo"  CssClass ="CouponCreditsInfo" runat="server" >

                       </asp:HyperLink>

                       </div>
               <div class="clear"></div>
               </div>
            <div class="CouponName">
                <div class="lfloat">
                         <asp:Label ID="lblCouponName" CssClass="lblCouponName" runat="server" Text="140 Credits">

                           </asp:Label>

                </div>
                    <div class="lfloat">
                           
                        <dx:ASPxTextBox ID="txtCouponName" CssClass="txtCouponName" runat="server" Width="250px" Height="33px">
                            <Border BorderStyle="None" />
                        </dx:ASPxTextBox>
                    </div>
                <div class="clear"></div>
            </div>
           <div class="CreateCouponButton">
                    <asp:LinkButton ID="btnCouponCreate"  CssClass="btnCouponCreate" runat="server">
                            <asp:Label ID="btnCouponCreateLabel" CssClass="btnCouponCreateLabel" runat="server" Text="Δημιουργία">
                           
                           </asp:Label>
                    </asp:LinkButton>
            </div>
           </div>
           <div class="lfloat OnlyPrint">
               <div class="CouponMoney">
               <asp:Label ID="lblCouponMoney" CssClass="lblCouponMoney" runat="server" Text="0$"></asp:Label>
                   </div>
                <div class="CouponCode">
               <asp:Label ID="lblCouponCode" CssClass="lblCouponCode" runat="server" Text="XXXX-XXX-XXX-XXXXXX"></asp:Label>
                   </div>
           </div>
            <div class="clear"></div>
       </div>
       
   </div>





















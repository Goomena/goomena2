﻿Imports System.Data.SqlClient
Imports Dating.Server.Core.DLL

Public Class ucLandingProfiles
    Inherits BaseUserControl
    'Inherits System.Web.UI.Page

    Protected Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData(Context, Me.Page, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property
    Private Sub Page_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        Try
            If Me._pageData IsNot Nothing Then
                RemoveHandler Me._pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If

        Catch ex As Exception
        End Try

        Try
            MyBase.Dispose()
        Catch ex As Exception
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            lblCloseRdv.Text = CurrentPageData.GetCustomString("lblCloseRdv")
            btnRegister.Text = CurrentPageData.GetCustomString("btnRegister")
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

        Dim myCmd As SqlCommand = Nothing
        'Dim myConn As SqlConnection
        'Dim myReader As SqlDataReader
        Dim address As String = Me.Request.Url.Scheme & "://photos.goomena.com/"
        Dim htmlup As String = ""
        Dim htmldown As String = ""
        Dim counter As Integer
        Dim photoid As String
        Dim photoname As String
        Dim profilename As String
        Dim address2 As String
        Dim src As String

        Try
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection

                'myConn = New SqlConnection(ConfigurationManager.ConnectionStrings("AppDBconnectionString").ConnectionString)
                myCmd = DataHelpers.GetSqlCommand(con)
                myCmd.CommandText = <sql><![CDATA[exec [dbo].[CustomerPhotos_Landing] @Country=@Country1,@GenderID=@GenderID1]]></sql>.Value

                Dim country1 As String = Session("GEO_COUNTRY_CODE")
                If (Not String.IsNullOrEmpty(Me.SessionVariables.MemberData.Country)) Then
                    country1 = Me.SessionVariables.MemberData.Country
                End If
                myCmd.Parameters.AddWithValue("@Country1", country1)
                myCmd.Parameters.AddWithValue("@GenderID1", 2)
                Dim dt As DataTable = DataHelpers.GetDataTable(myCmd)



                Dim checkList As New List(Of Integer)
                counter = 0
                For cnt = 0 To dt.Rows.Count - 1

                    Dim dr As DataRow = dt.Rows(cnt)
                    'Do While myReader.Read()

                    Dim CustomerID As Integer = dr("CustomerID")
                    If (checkList.Contains(CustomerID)) Then
                        Continue For
                    End If
                    checkList.Add(CustomerID)

                    If counter < 4 Then
                        photoid = dr("CustomerID").ToString
                        photoname = dr("FileName")
                        profilename = dr("LoginName")
                        address2 = address + photoid
                        src = address2 + "/thumbs/" + photoname
                        Dim url As String = UrlUtils.GetValidRoutingProfileURI(HttpUtility.UrlEncode(profilename)) 'HttpUtility.UrlEncode(profilename).Replace("+", " ")
                        url = MyBase.LanguageHelper.GetPublicURL("/profile/" & url, MyBase.GetLag())
                        htmlup += "<div class=""landingFacesContainer""><a class=""landingimgborder"" href=""" & url & """ onclick=""ShowLoading();""><img class=""FaceImg"" src=""" & src & """/></a><p>" & profilename & "</p></div>"
                    Else
                        photoid = dr("CustomerID").ToString
                        photoname = dr("FileName")
                        profilename = dr("LoginName")
                        address2 = address + photoid
                        src = address2 + "/thumbs/" + photoname
                        Dim url As String = UrlUtils.GetValidRoutingProfileURI(HttpUtility.UrlEncode(profilename)) 'HttpUtility.UrlEncode(profilename).Replace("+", " ")
                        url = MyBase.LanguageHelper.GetPublicURL("/profile/" & url, MyBase.GetLag())
                        htmldown += "<div class=""landingFacesContainer""><a class=""landingimgborder"" href=""" & url & """ onclick=""ShowLoading();""><img class=""FaceImg"" src=""" & src & """/></a><p>" & profilename & "</p></div>"
                    End If
                    counter = counter + 1

                    'stis 8 photos exit
                    If (counter > 7) Then Exit For

                Next

                lblFacesUp.Text = htmlup
                lblFacesDown.Text = htmldown
            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        Finally

            'myReader.Close()
            'myConn.Dispose()
            If (myCmd IsNot Nothing) Then myCmd.Dispose()

        End Try


    End Sub



End Class
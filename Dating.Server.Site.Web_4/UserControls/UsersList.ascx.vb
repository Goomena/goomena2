﻿Public Class UsersList
    Inherits BaseUserControl


    Public Enum UsersListViewEnum
        None = 0
        MembersDefaultNear = 1
        MembersDefaultNewest = 2
        MembersDefaultMoreNear = 3
        MembersDefaultFavorited = 4
        MembersDefaultViewed = 5
        DefaultGenerous = 6
        DefaultAttractive = 7
        DefaultGenerous2 = 8
        DefaultAttractive2 = 9
    End Enum


    Public Property UsersListView As UsersListViewEnum
        Get
            If (Me.ViewState("UsersListView") IsNot Nothing) Then Return Me.ViewState("UsersListView")
            Return UsersListViewEnum.None
        End Get
        Set(value As UsersListViewEnum)
            Me.ViewState("UsersListView") = value
        End Set
    End Property


    Private _list As List(Of clsUserListItem)
    Public Property UsersList As List(Of clsUserListItem)
        Get
            If (Me._list Is Nothing) Then Me._list = New List(Of clsUserListItem)
            Return Me._list
        End Get
        Set(value As List(Of clsUserListItem))
            Me._list = value
        End Set
    End Property


    <PersistenceMode(PersistenceMode.InnerDefaultProperty)>
    Public ReadOnly Property ListView As Object
        Get
            If (UsersListView = UsersListViewEnum.MembersDefaultMoreNear Or UsersListView = UsersListViewEnum.MembersDefaultNear Or UsersListView = UsersListViewEnum.MembersDefaultNewest) Then
                Return Me.lv
            ElseIf (UsersListView = UsersListViewEnum.MembersDefaultFavorited Or UsersListView = UsersListViewEnum.MembersDefaultViewed) Then
                Return Me.lv2
            ElseIf (UsersListView = UsersListViewEnum.DefaultGenerous) Then
                Return Me.lvG
            ElseIf (UsersListView = UsersListViewEnum.DefaultAttractive) Then
                Return Me.lvA
            Else
                Return Nothing
            End If
        End Get
    End Property


    Public Overrides Sub DataBind()

        If (UsersListView = UsersListViewEnum.MembersDefaultMoreNear Or UsersListView = UsersListViewEnum.MembersDefaultNear Or UsersListView = UsersListViewEnum.MembersDefaultNewest) Then
            Me.MultiView1.SetActiveView(vwMembersDefaulMainList)
            Me.lv.DataSource = Me._list
            Me.lv.DataBind()

        ElseIf (UsersListView = UsersListViewEnum.MembersDefaultFavorited Or UsersListView = UsersListViewEnum.MembersDefaultViewed) Then
            Me.MultiView1.SetActiveView(vwMembersDefaultFavoritedOrViewed)
            Me.lv2.DataSource = Me._list
            Me.lv2.DataBind()

        ElseIf (UsersListView = UsersListViewEnum.DefaultGenerous) Then
            Me.MultiView1.SetActiveView(vwGenerous)
            Me.lvG.DataSource = Me._list
            Me.lvG.DataBind()

        ElseIf (UsersListView = UsersListViewEnum.DefaultAttractive) Then
            Me.MultiView1.SetActiveView(vwAttractives)
            Me.lvA.DataSource = Me._list
            Me.lvA.DataBind()


        ElseIf (UsersListView = UsersListViewEnum.DefaultGenerous2) Then
            Me.MultiView1.SetActiveView(vwGenerous2)
            Me.lvG2.DataSource = Me._list
            Me.lvG2.DataBind()

        ElseIf (UsersListView = UsersListViewEnum.DefaultAttractive2) Then
            Me.MultiView1.SetActiveView(vwAttractives2)
            Me.lvA2.DataSource = Me._list
            Me.lvA2.DataBind()

        Else
            Me.MultiView1.SetActiveView(View1)
        End If

    End Sub



    'Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    'End Sub

    'Public Property DataSource As Object
    '    Get
    '        Return Me.lv.DataSource
    '    End Get
    '    Set(value As Object)
    '        Me.lv.DataSource = value
    '    End Set
    'End Property

    'Public Function GetDataTable()

    '    Dim dt As New DataTable()
    '    dt.Columns.Add("ProfileID", GetType(Integer))
    '    dt.Columns.Add("LoginName")
    '    dt.Columns.Add("Genderid", GetType(Integer))
    '    dt.Columns.Add("ImageFileName")
    '    dt.Columns.Add("ProfileViewUrl")
    '    dt.Columns.Add("ImageUrl")
    '    dt.Columns.Add("SendWinkText")
    '    dt.Columns.Add("ShowCheckBox", GetType(Boolean))

    '    Return dt
    'End Function


End Class

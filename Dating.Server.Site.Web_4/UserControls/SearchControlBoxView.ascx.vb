﻿Imports DevExpress.Web.ASPxPager
Imports Dating.Server.Core.DLL

Public Class SearchControlBoxView
    Inherits BaseUserControl
    

    Private _VIPTooltip As String
    Protected Property VIPTooltip As String
        Get
            Return _VIPTooltip
        End Get
        Set(value As String)
            _VIPTooltip = value
        End Set
    End Property


    Public Enum WinkUsersListViewEnum
        None = 0

        LikesOffers = 12
        NewOffers = 1
        PendingOffers = 2
        RejectedOffers = 3
        AcceptedOffers = 4
        PokesOffers = 14

        SearchingOffers = 5

        WhoViewedMeList = 6
        WhoFavoritedMeList = 7
        WhoSharedPhotoList = 11
        SharedPhotosByMeList = 20
        MyFavoriteList = 8
        MyBlockedList = 9
        MyViewedList = 10

    End Enum

    Public Enum MessagesListViewEnum
        None = 0
        AllMessages = 24

        NewMessages = 12
        Inbox = 1
        SentMessages = 2
        Trash = 3
    End Enum

    Public Property InfoWinUrl As String
    Public Property InfoWinHeaderText As String

    Public Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            'If (_pageData Is Nothing) Then _pageData = New clsPageData("control.OfferControl", Context)
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData("control.OfferControl", Context, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property

    Private Sub Page_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        Try
            If Me._pageData IsNot Nothing Then
                RemoveHandler Me._pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If

        Catch ex As Exception
        End Try

        Try
            MyBase.Dispose()
        Catch ex As Exception
        End Try
    End Sub
    Public Property ShowNoPhotoText As Boolean
    Public Property ShowEmptyListText As Boolean

    Public Property UsersListView As WinkUsersListViewEnum
        Get
            If (Me.ViewState("WinkUsersListViewEnum") IsNot Nothing) Then Return Me.ViewState("WinkUsersListViewEnum")
            Return WinkUsersListViewEnum.None
        End Get
        Set(value As WinkUsersListViewEnum)
            Me.ViewState("WinkUsersListViewEnum") = value
        End Set
    End Property


    Private _list As List(Of clsWinkUserListItem)
    Public Property UsersList As List(Of clsWinkUserListItem)
        Get
            If (Me._list Is Nothing) Then Me._list = New List(Of clsWinkUserListItem)
            Return Me._list
        End Get
        Set(value As List(Of clsWinkUserListItem))
            Me._list = value
        End Set
    End Property



    <PersistenceMode(PersistenceMode.InnerDefaultProperty)>
    Public ReadOnly Property Repeater As Repeater
        Get
            If (UsersListView = WinkUsersListViewEnum.SearchingOffers _
                    OrElse UsersListView = WinkUsersListViewEnum.WhoViewedMeList _
                    OrElse UsersListView = WinkUsersListViewEnum.WhoFavoritedMeList _
                    OrElse UsersListView = WinkUsersListViewEnum.WhoSharedPhotoList _
                    OrElse UsersListView = WinkUsersListViewEnum.SharedPhotosByMeList _
                    OrElse UsersListView = WinkUsersListViewEnum.MyFavoriteList _
                    OrElse UsersListView = WinkUsersListViewEnum.MyBlockedList _
                    OrElse UsersListView = WinkUsersListViewEnum.MyViewedList) Then
                Return Me.rptSearch

            Else
                Return Nothing
            End If
        End Get
    End Property
    Protected Shared Function GetClosePhoto(ByVal Loginname As String) As String
        Dim txt As String = ""

        txt = "javascript:CloseBotom('#s_item" & ReturnAlphaCharacters(Loginname) & " .TopBox .RoundImgLink','#s_item" & ReturnAlphaCharacters(Loginname) & " .BottomBox');"

        Return txt
    End Function
    Protected Shared Function GetPhoto(ByVal Hasp As Boolean, NavigationUrl As String, ByVal Loginname As String) As String
        Dim txt As String = ""
        If Hasp Then
            txt = "javascript:ShowBotom('#s_item" & ReturnAlphaCharacters(Loginname) & " .TopBox .RoundImgLink','#s_item" & ReturnAlphaCharacters(Loginname) & " .BottomBox'); return false;"
        Else
            txt = "" 'NavigationUrl
        End If
        Return txt
    End Function
    Protected Shared Function GetPhoto2(ByVal Hasp As Boolean, ByVal ImgUrl As String, ByVal ImgUrlOnClick As String, ByVal imgThumb As String) As String
        Dim txt As String = ""
        If Hasp Then
            If ImgUrlOnClick Is Nothing OrElse ImgUrlOnClick.Length = 0 Then
                '    txt = "<a class='smallSquareImage' rel='prof_group" & ReturnAlphaCharacters(_loginname) & "' onclick='' href='" & ImgUrl & "'  >"
                txt = " <div class=""smallSquareImage"" style=""" & GetimageBackgroundStyle(imgThumb) & """></div>"
            Else
                txt = "<a class=""smallSquareImage private-photo""   href=""" & ImgUrl & "" & imgThumb & """ onclick=""" & ImgUrlOnClick & """ style=""" & GetimageBackgroundStyle(imgThumb) & """></a>"

            End If
            'If photoLevel IsNot Nothing AndAlso photoLevel.Length > 0 Then
            '    txt &= vbNewLine & "<span class='lblLevel'>" & photoLevel & "</span>"
            'End If
            'If DisplayLevel IsNot Nothing AndAlso DisplayLevel.Length > 0 Then
            '    txt &= vbNewLine & "<span class='photo-text'>" & DisplayLevel & "</span>"
            'End If

        End If
        Return txt
    End Function
    Protected Shared Function GetPhoto(ByVal Hasp As Boolean, ByVal ImgUrl As String, ByVal ImgUrlOnClick As String _
                                , ByVal imgThumb As String, ByVal photoLevel As String, ByVal _loginname As String, ByVal DisplayLevel As String) As String
        Dim txt As String = ""
        If Hasp Then
            If ImgUrlOnClick Is Nothing OrElse ImgUrlOnClick.Length = 0 Then
                '    txt = "<a class='smallSquareImage' rel='prof_group" & ReturnAlphaCharacters(_loginname) & "' onclick='' href='" & ImgUrl & "'  >"
                txt = " <div class=""smallSquareImage"" style=""" & GetimageBackgroundStyle(imgThumb) & """></div>"
            Else
                txt = "<a class=""smallSquareImage private-photo""   href=""" & ImgUrl & """ onclick=""" & ImgUrlOnClick & """ style=""" & GetimageBackgroundStyle(imgThumb) & """></a>"

            End If
            'If photoLevel IsNot Nothing AndAlso photoLevel.Length > 0 Then
            '    txt &= vbNewLine & "<span class='lblLevel'>" & photoLevel & "</span>"
            'End If
            'If DisplayLevel IsNot Nothing AndAlso DisplayLevel.Length > 0 Then
            '    txt &= vbNewLine & "<span class='photo-text'>" & DisplayLevel & "</span>"
            'End If

        End If
        Return txt
    End Function
    Shared Function ReturnAlphaCharacters(ByVal StringToCheck As String) As String
        Dim txt As String = ""
        For i = 0 To StringToCheck.Length - 1
            If Not Char.IsLetter(StringToCheck.Chars(i)) AndAlso Not Char.IsNumber(StringToCheck.Chars(i)) Then
                txt &= "p1"
            Else
                txt &= StringToCheck.Chars(i)
            End If
        Next

        Return txt
    End Function
    Protected Overrides Sub OnInit(e As System.EventArgs)
        MyBase.OnInit(e)
    End Sub



    Public Overrides Sub DataBind()

        If (Me.ShowNoPhotoText) Then

            Dim oNoPhoto As New clsWinkUserListItem()
            Dim os As New List(Of clsWinkUserListItem)
            os.Add(oNoPhoto)

            fvNoPhoto.DataSource = os
            fvNoPhoto.DataBind()

            Me.MultiView1.SetActiveView(vwNoPhoto)


        ElseIf (Me.ShowEmptyListText) Then

            Dim oNoOffer As New clsWinkUserListItem()
            If (UsersListView = WinkUsersListViewEnum.AcceptedOffers) Then
                oNoOffer.NoOfferText = Me.CurrentPageData.GetCustomString("AcceptedOffersListEmptyText_ND")

            ElseIf (UsersListView = WinkUsersListViewEnum.NewOffers) Then
                oNoOffer.NoOfferText = Me.CurrentPageData.GetCustomString("NewOffersListEmptyText_ND")

            ElseIf (UsersListView = WinkUsersListViewEnum.LikesOffers) Then
                oNoOffer.NoOfferText = Me.CurrentPageData.GetCustomString("WinksListEmptyText")

            ElseIf (UsersListView = WinkUsersListViewEnum.PokesOffers) Then
                oNoOffer.NoOfferText = Me.CurrentPageData.GetCustomString("PokesListEmptyText")

            ElseIf (UsersListView = WinkUsersListViewEnum.PendingOffers) Then
                oNoOffer.NoOfferText = Me.CurrentPageData.GetCustomString("PendingOffersListEmptyText_ND")

            ElseIf (UsersListView = WinkUsersListViewEnum.RejectedOffers) Then
                oNoOffer.NoOfferText = Me.CurrentPageData.GetCustomString("RejectedOffersListEmptyText_ND")


            ElseIf (UsersListView = WinkUsersListViewEnum.WhoViewedMeList) Then
                oNoOffer.NoOfferText = Me.CurrentPageData.GetCustomString("WhoViewedMeListEmptyText_ND")

            ElseIf (UsersListView = WinkUsersListViewEnum.WhoFavoritedMeList) Then
                oNoOffer.NoOfferText = Me.CurrentPageData.GetCustomString("WhoFavoritedMeListEmptyText_ND")

            ElseIf (UsersListView = WinkUsersListViewEnum.WhoSharedPhotoList) Then
                oNoOffer.NoOfferText = Me.CurrentPageData.GetCustomString("WhoSharedPhotoListEmptyText_ND")

            ElseIf (UsersListView = WinkUsersListViewEnum.SharedPhotosByMeList) Then
                oNoOffer.NoOfferText = Me.CurrentPageData.GetCustomString("SharedPhotosByMeListEmptyText_ND")

            ElseIf (UsersListView = WinkUsersListViewEnum.MyFavoriteList) Then
                oNoOffer.NoOfferText = Me.CurrentPageData.GetCustomString("MyFavoriteListEmptyText_ND")

            ElseIf (UsersListView = WinkUsersListViewEnum.MyBlockedList) Then
                oNoOffer.NoOfferText = Me.CurrentPageData.GetCustomString("MyBlockedListEmptyText_ND")

            ElseIf (UsersListView = WinkUsersListViewEnum.MyViewedList) Then
                oNoOffer.NoOfferText = Me.CurrentPageData.GetCustomString("MyViewedListEmptyText_ND")

            End If
            Dim os As New List(Of clsWinkUserListItem)
            os.Add(oNoOffer)
            fvNoOffers.DataSource = os
            fvNoOffers.DataBind()
            Me.MultiView1.SetActiveView(vwNoOffers)
        Else
            If (UsersListView = WinkUsersListViewEnum.SearchingOffers _
                OrElse UsersListView = WinkUsersListViewEnum.WhoViewedMeList _
                OrElse UsersListView = WinkUsersListViewEnum.WhoFavoritedMeList _
                OrElse UsersListView = WinkUsersListViewEnum.WhoSharedPhotoList _
                OrElse UsersListView = WinkUsersListViewEnum.SharedPhotosByMeList _
                OrElse UsersListView = WinkUsersListViewEnum.MyFavoriteList _
                OrElse UsersListView = WinkUsersListViewEnum.MyBlockedList _
                OrElse UsersListView = WinkUsersListViewEnum.MyViewedList) Then
                Me.MultiView1.SetActiveView(vwSearchingOffers)
            Else
                Me.MultiView1.SetActiveView(View1)
            End If
        End If




        If (Me.Repeater IsNot Nothing) Then
            '  FillKeyStrings()

            'Try
            '    Dim i As Integer = 0
            '    For Each itm As clsWinkUserListItem In Me.UsersList
            '        i += 1
            '        itm.FillTextResourceProperties(MykeyStrings)
            '        itm.isOdd = If(i Mod 2 = 0, False, True)
            '        If (itm.ZodiacString Is Nothing AndAlso itm.OtherMemberBirthday.HasValue AndAlso itm.OtherMemberBirthday.Value > DateTime.MinValue) Then
            '            Dim tmpKeystr As String = "Zodiac" & ProfileHelper.ZodiacName(itm.OtherMemberBirthday)
            '            If MykeyStrings.ZodiacNames.ContainsKey(tmpKeystr & itm.LAGID) Then
            '                itm.ZodiacString = MykeyStrings.ZodiacNames(tmpKeystr & itm.LAGID)
            '            Else
            '                itm.ZodiacString = globalStrings.GetCustomString(tmpKeystr, itm.LAGID)
            '                MykeyStrings.ZodiacNames.Add(tmpKeystr & itm.LAGID, itm.ZodiacString)
            '            End If
            '        End If
            '    Next
            'Catch ex As Exception
            '    WebErrorMessageBox(Me, ex, "")
            'End Try

            Me.Repeater.DataSource = Me.UsersList
            Me.Repeater.DataBind()
        End If


        If (Me.IsFemale) Then
            VIPTooltip = Me.CurrentPageData.GetCustomString("lbTooltip.MenVIP.ForWomen")
        Else
            VIPTooltip = Me.CurrentPageData.GetCustomString("lbTooltip.MenVIP.ForMen")
        End If

    End Sub

    Public Property MykeyStrings As KeyStrings
    Public Function GetKeys() As KeyStrings
        If MykeyStrings Is Nothing Then
            MykeyStrings = New KeyStrings
        End If
        Return MykeyStrings
    End Function
    Public Function FillKeyStrings(Optional ByVal Strings As KeyStrings = Nothing, Optional ByVal pageData As clsPageData = Nothing) As Boolean
        If MykeyStrings Is Nothing Then
            MykeyStrings = New KeyStrings
            If pageData Is Nothing Then
                pageData = CurrentPageData
            End If
            If Strings Is Nothing Then
                Strings = MykeyStrings
            End If
            If (String.IsNullOrEmpty(Strings.SendMessageOnceText)) Then Strings.SendMessageOnceText = pageData.GetCustomString("SendMessageOnceText")
            If (String.IsNullOrEmpty(Strings.SendMessageManyText)) Then Strings.SendMessageManyText = pageData.GetCustomString("SendMessageManyText")
            If (String.IsNullOrEmpty(Strings.UnlockNotice)) Then Strings.UnlockNotice = pageData.GetCustomString("UnlockNotice")
            If (String.IsNullOrEmpty(Strings.ActionsUnlockText)) Then Strings.ActionsUnlockText = pageData.GetCustomString("ActionsUnlockText")
            If (String.IsNullOrEmpty(Strings.OfferAcceptedUnlockText)) Then Strings.OfferAcceptedUnlockText = pageData.GetCustomString("OfferAcceptedUnlockText")
            If (String.IsNullOrEmpty(Strings.OfferAcceptedHowToContinueText)) Then Strings.OfferAcceptedHowToContinueText = pageData.GetCustomString("OfferAcceptedHowToContinueText")
            If (String.IsNullOrEmpty(Strings.WillYouAcceptDateWithForAmountText)) Then Strings.WillYouAcceptDateWithForAmountText = pageData.GetCustomString("WillYouAcceptDateWithForAmountText")
            If (String.IsNullOrEmpty(Strings.YouReceivedPokeDescriptionText)) Then Strings.YouReceivedPokeDescriptionText = pageData.GetCustomString("YouReceivedPokeDescriptionText")
            If (String.IsNullOrEmpty(Strings.OfferDeleteConfirmMessage)) Then Strings.OfferDeleteConfirmMessage = pageData.GetCustomString("OfferDeleteConfirmMessage")
            If (String.IsNullOrEmpty(Strings.MilesAwayText)) Then Strings.MilesAwayText = pageData.GetCustomString("MilesAwayText")
            If (String.IsNullOrEmpty(Strings.CancelledRejectedDescriptionText)) Then Strings.CancelledRejectedDescriptionText = pageData.GetCustomString("YouCancelledWinkToText")
            If (String.IsNullOrEmpty(Strings.RejectBlockText)) Then Strings.RejectBlockText = pageData.GetCustomString("RejectBlockText")
            If (String.IsNullOrEmpty(Strings.RejectDeleteConversationText)) Then Strings.RejectDeleteConversationText = pageData.GetCustomString("RejectDeleteConversationText")
            If (String.IsNullOrEmpty(Strings.RejectDeleteConversationText)) Then Strings.RejectDeleteConversationText = pageData.GetCustomString("RejectDeleteConversationText")
            If (String.IsNullOrEmpty(Strings.IsOnlinetext)) Then Strings.IsOnlinetext = pageData.GetCustomString("Online.Now")
            If (String.IsNullOrEmpty(Strings.IsOnlineRecentlyText)) Then Strings.IsOnlineRecentlyText = pageData.GetCustomString("Online.Recently")
            If (String.IsNullOrEmpty(Strings.HasNoPhotosText)) Then Strings.HasNoPhotosText = pageData.GetCustomString("HasNoPhotosText")
            If (String.IsNullOrEmpty(Strings.SearchOurMembersText)) Then Strings.SearchOurMembersText = pageData.GetCustomString("SearchOurMembersText")
            If (String.IsNullOrEmpty(Strings.YouReceivedWinkOfferText)) Then Strings.YouReceivedWinkOfferText = pageData.GetCustomString("YouReceivedWinkText")
            If (String.IsNullOrEmpty(Strings.YearsOldText)) Then Strings.YearsOldText = pageData.GetCustomString("YearsOldText")
            If (String.IsNullOrEmpty(Strings.DeleteOfferText)) Then Strings.DeleteOfferText = pageData.GetCustomString("DeleteOfferText_ND")
            If (String.IsNullOrEmpty(Strings.SendMessageText)) Then Strings.SendMessageText = pageData.GetCustomString("SendMessageText_ND")
            If (String.IsNullOrEmpty(Strings.WinkText)) Then Strings.WinkText = pageData.GetCustomString("WinkText")
            If (String.IsNullOrEmpty(Strings.UnWinkText)) Then Strings.UnWinkText = pageData.GetCustomString("UnWinkText")
            If (String.IsNullOrEmpty(Strings.FavoriteText)) Then Strings.FavoriteText = pageData.GetCustomString("FavoriteText")
            If (String.IsNullOrEmpty(Strings.UnfavoriteText)) Then Strings.UnfavoriteText = pageData.GetCustomString("UnfavoriteText")
            If (String.IsNullOrEmpty(Strings.YearsOldFromText)) Then Strings.YearsOldFromText = pageData.GetCustomString("YearsOldFromText")
            If (String.IsNullOrEmpty(Strings.AddPhotosText)) Then Strings.AddPhotosText = pageData.GetCustomString("AddPhotosText")
            If (String.IsNullOrEmpty(Strings.UnblockText)) Then Strings.UnblockText = pageData.GetCustomString("UnblockText")
            If (String.IsNullOrEmpty(Strings.ActionsText)) Then Strings.ActionsText = pageData.GetCustomString("ActionsText")
            If (String.IsNullOrEmpty(Strings.ActionsMakeOfferText)) Then Strings.ActionsMakeOfferText = pageData.GetCustomString("ActionsMakeOfferText")
        End If
    End Function


    Protected Function WriteOffer_MemberInfo(DataItem As Dating.Server.Site.Web.clsWinkUserListItem) As String
        Dim sb As New StringBuilder()
        Try
            sb.Append("<ul class=""info2"">")

            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberAge) Then
                sb.Append("<li class=""age1"">")
                sb.Append("<b>")
                sb.Append(DataItem.OtherMemberAge)
                sb.Append(" " & MykeyStrings.YearsOldText & " ")
                sb.Append("</b>")
                sb.Append("</li>")
            End If


            If (Not String.IsNullOrEmpty(DataItem.OtherMemberCity)) Then
                sb.Append("<li>")
                sb.Append(IIf(Not String.IsNullOrEmpty(DataItem.OtherMemberCity), DataItem.OtherMemberCity, ""))
                If (Not String.IsNullOrEmpty(DataItem.OtherMemberRegion)) Then sb.Append(",  ")
                sb.Append("</li>")
            End If

            sb.Append("<li>")
            sb.Append(IIf(Not String.IsNullOrEmpty(DataItem.OtherMemberRegion), DataItem.OtherMemberRegion, ""))
            sb.Append(",  ")
            sb.Append(IIf(Not String.IsNullOrEmpty(DataItem.OtherMemberCountry), ProfileHelper.GetCountryName(DataItem.OtherMemberCountry), ""))
            sb.Replace(",  " & ",  ", ",  ")
            sb.Append("</li>")


            sb.Append("</ul>")

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Return sb.ToString()
    End Function

    Protected Function WriteSearch_MemberInfo(DataItem As Dating.Server.Site.Web.clsWinkUserListItem) As String
        Dim sb As New StringBuilder()
        Try
            sb.Append("<ul>")
            sb.Append("<li>")
            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberAge) Then
                sb.Append("<b>" & DataItem.OtherMemberAge)
                sb.Append(" " & MykeyStrings.YearsOldText & " ")

                If (Not String.IsNullOrEmpty(DataItem.ZodiacString)) Then
                    sb.Append("(" & DataItem.OtherMemberBirthday.Value.ToString("d MMM, yyyy") & ")")
                End If
                sb.Append("</b>")
            End If

            sb.Append("<div class=""s_item_address"">")

            sb.Append(IIf(Not String.IsNullOrEmpty(DataItem.OtherMemberCity), DataItem.OtherMemberCity, ""))

            sb.Append(IIf(Not String.IsNullOrEmpty(DataItem.OtherMemberRegion), IIf(Not String.IsNullOrEmpty(DataItem.OtherMemberCity), ", ", "") & DataItem.OtherMemberRegion, ""))

            sb.Append(IIf(Not String.IsNullOrEmpty(DataItem.OtherMemberCountry), IIf(Not String.IsNullOrEmpty(DataItem.OtherMemberCity) OrElse Not String.IsNullOrEmpty(DataItem.OtherMemberRegion), ", ", "") & ProfileHelper.GetCountryName(DataItem.OtherMemberCountry), ""))
            sb.Replace(",  " & ",  ", ",  ")

            sb.Append("</div>")
            sb.Append("</li>")


            sb.Append("<li>")


            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberHeight) Then
                sb.Append(DataItem.OtherMemberHeight)
            End If

            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberHeight) AndAlso Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberPersonType) Then
                sb.Append(" - ")
            End If

            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberPersonType) Then
                sb.Append(DataItem.OtherMemberPersonType)
            End If

            sb.Append("</li>")


            sb.Append("<li>")
            sb.Append("</li>")
            sb.Append("</ul>")

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Return sb.ToString()
    End Function

    Protected Shared Function ShowIsOnline(DataItem As Dating.Server.Site.Web.clsWinkUserListItem) As Boolean
        If (DataItem.OtherMemberIsOnline OrElse DataItem.OtherMemberIsOnlineRecently) Then Return True
        Return False
    End Function
    Protected Sub rptSearch_ItemDataBound(source As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptSearch.ItemDataBound
        Try

            Dim lnkWhatIs As LinkButton = e.Item.FindControl("lnkWhatIs")
            If (lnkWhatIs IsNot Nothing) Then
                lnkWhatIs.ToolTip = Me.CurrentPageData.GetCustomString("WhatIsText")

                If (String.IsNullOrEmpty(InfoWinUrl)) Then InfoWinUrl = ResolveUrl("~/Members/InfoWin.aspx?info=searchcommands")
                If (String.IsNullOrEmpty(InfoWinHeaderText)) Then InfoWinHeaderText = Me.CurrentPageData.GetCustomString("Search_WhatIsPopup_HeaderText")

                lnkWhatIs.OnClientClick = WhatIsIt.OnMoreInfoClickFunc(lnkWhatIs.OnClientClick,
                                             InfoWinUrl,
                                             InfoWinHeaderText)
            End If

        Catch ex As Exception

        End Try
    End Sub
    Public Function ShowBirthday(ByVal bdt As Date, ByVal loginname As String, ByVal GenderId As String) As String
        Dim tmp As String = "<div  class=""rfloat birthday info-item""> " & vbNewLine & _
                        "<img src=""//cdn.goomena.com/Images/spacer10.png"" id=""ctl00_content_ProfileView1_imgBirthday"" class=""tt_enabled"">" & vbNewLine & _
                        "<div class=""tooltip tooltip_Birthday"" style=""display: none;"">" & vbNewLine & _
                          "  <div class=""tooltip_text right-arrow""><span>#####</span></div>" & vbNewLine & _
                       " </div></div>" & vbNewLine
        Dim dt As DateTime = AppUtils.GetDateTimeWithUserOffset(DateTime.Now)
        Dim dt1 As DateTime = AppUtils.GetDateTimeWithUserOffset(bdt)
        If dt.Month = dt1.Month AndAlso dt.Day = dt1.Day Then

            Dim tm As String = CurrentPageData.VerifyCustomString("lblHappyBirthday")
            tm = tm.Replace("###LoginName###", loginname).Replace("###Gender###", If(GenderId = 1, "ο", "η"))
            tmp = tmp.Replace("#####", tm)
        Else
            tmp = ""
        End If
        Return tmp
    End Function

    Public Shared Function GetimageBackgroundStyle(ByVal url As String) As String
        Return "background-image: url('" & url & "'); background-size: cover;"
    End Function
End Class
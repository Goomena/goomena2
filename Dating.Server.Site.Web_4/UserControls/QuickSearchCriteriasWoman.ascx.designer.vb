﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class QuickSearchCriteriasWoman

    '''<summary>
    '''pnlQUserName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlQUserName As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''txtUserNameQ control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtUserNameQ As Global.DevExpress.Web.ASPxEditors.ASPxTextBox

    '''<summary>
    '''imgSearchByUserName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imgSearchByUserName As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''msg_MoreOptions control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents msg_MoreOptions As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''msg_UserUserNameOrFiltersQ_ND control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents msg_UserUserNameOrFiltersQ_ND As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''chkOnline control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkOnline As Global.DevExpress.Web.ASPxEditors.ASPxCheckBox

    '''<summary>
    '''chkPhotos control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkPhotos As Global.DevExpress.Web.ASPxEditors.ASPxCheckBox

    '''<summary>
    '''chkPhotosPrivate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkPhotosPrivate As Global.DevExpress.Web.ASPxEditors.ASPxCheckBox

    '''<summary>
    '''chkVIP control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkVIP As Global.DevExpress.Web.ASPxEditors.ASPxCheckBox

    '''<summary>
    '''chkTravel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkTravel As Global.DevExpress.Web.ASPxEditors.ASPxCheckBox

    '''<summary>
    '''chkJob control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkJob As Global.DevExpress.Web.ASPxEditors.ASPxCheckBox

    '''<summary>
    '''ddeJobsWrap control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddeJobsWrap As Global.DevExpress.Web.ASPxEditors.ASPxDropDownEdit

    '''<summary>
    '''msg_AgeText control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents msg_AgeText As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''trcAge control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents trcAge As Global.DevExpress.Web.ASPxEditors.ASPxTrackBar

    '''<summary>
    '''msg_AgeMin control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents msg_AgeMin As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''msg_AgeMax control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents msg_AgeMax As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''msg_DistanceFromMe_ND control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents msg_DistanceFromMe_ND As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''trcDistance control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents trcDistance As Global.DevExpress.Web.ASPxEditors.ASPxTrackBar

    '''<summary>
    '''msg_DistanceVal control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents msg_DistanceVal As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''msg_OtherAnnual control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents msg_OtherAnnual As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''trcIncome control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents trcIncome As Global.DevExpress.Web.ASPxEditors.ASPxTrackBar

    '''<summary>
    '''msg_IncomeMin control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents msg_IncomeMin As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''msg_IncomeMax control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents msg_IncomeMax As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''msg_PersonalBodyType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents msg_PersonalBodyType As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''clstBodyType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents clstBodyType As Global.DevExpress.Web.ASPxEditors.ASPxCheckBoxList

    '''<summary>
    '''msg_OtherEducat control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents msg_OtherEducat As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''clstEducat control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents clstEducat As Global.DevExpress.Web.ASPxEditors.ASPxCheckBoxList

    '''<summary>
    '''msg_PersonalSmokingHabit control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents msg_PersonalSmokingHabit As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''clstSmoking control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents clstSmoking As Global.DevExpress.Web.ASPxEditors.ASPxCheckBoxList

    '''<summary>
    '''msg_PersonalDrinkingHabit control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents msg_PersonalDrinkingHabit As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''clstDrinking control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents clstDrinking As Global.DevExpress.Web.ASPxEditors.ASPxCheckBoxList

    '''<summary>
    '''msg_SpokenLang control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents msg_SpokenLang As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''ddeSpokenLangWrap control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddeSpokenLangWrap As Global.DevExpress.Web.ASPxEditors.ASPxDropDownEdit

    '''<summary>
    '''msg_TypeOfDating control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents msg_TypeOfDating As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''ddeTypeOfDatingWrap control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddeTypeOfDatingWrap As Global.DevExpress.Web.ASPxEditors.ASPxDropDownEdit

    '''<summary>
    '''btnSearch_ND control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnSearch_ND As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''msg_BirthdayNote control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents msg_BirthdayNote As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Label1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Label1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lnkAdvanced control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkAdvanced As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''msg_ClearForm control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents msg_ClearForm As Global.System.Web.UI.WebControls.Label
End Class

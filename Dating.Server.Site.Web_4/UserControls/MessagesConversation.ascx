﻿<%@ Control Language="vb" AutoEventWireup="false" 
    CodeBehind="MessagesConversation.ascx.vb" 
    Inherits="Dating.Server.Site.Web.MessagesConversation" %>

<%--<asp:HiddenField ID="hdfListData" runat="server" ClientIDMode="Static" ValidateRequestMode="Disabled" />--%>
<input type="hidden" id="hdfListData" value="<%= MyBase.ListData%>" />
<%--DataSourceID="sdsConversations"--%>
<asp:ListView ID="dvConversation" runat="server">
    <ItemTemplate>
        <div class="cnv_item" id="msg-<%# Eval("EUS_MessageID") %>">
            <asp:Label ID="lblScrollto" runat="server" Text="" Font-Italic="true" />
            <div class="left">
                <div class="photo" id="divFromLeft" runat="server" visible='<%# (Eval("ProfileID").ToString()<>MyBase.MasterProfileID) %>' >
                   <div class="pr-photo-88-noshad">
                        <div class="tt_enabled">
                       <%-- <asp:HyperLink ID="lnkFromLeft" runat="server" NavigateUrl='<%# "/Members/Profile.aspx?p=" & HttpUtility.UrlEncode(Eval("LoginName").ToString()) & "&t=2"%>' onclick="ShowLoading();"><img id="imgFromLeft" runat="server" class="round-img-88" alt=""/></asp:HyperLink>--%>
                                <asp:HyperLink ID="lnkFromLeft" runat="server" NavigateUrl='<%# "/Members/Profile.aspx?p=" & HttpUtility.UrlEncode(Eval("LoginName").ToString()) & "&t=2"%>' onclick="ShowLoading();">
                                   <img id="imgFromLeft" runat="server" class="round-img-88" alt=""/>
                                </asp:HyperLink>
                           </div>
                                  <div class="tooltip tooltip_PhotoLevelHover">
                                    <div class="tooltip_text left-arrow"><asp:Label ID="lblPhotoLevelHover" runat="server" Text="" /></div>
                                </div>
                          
                    </div>
                    <div class="m-arrow"></div>
                </div>
            </div>
            <div class="middle"><%--<%# If((Eval("ProfileID").ToString()=MyBase.MasterProfileID), "text-black", "") %>--%>
                <div class="copy-read" id="divCR" runat="server" visible='<%# ((Eval("ProfileID")=MyBase.MasterProfileID)  andalso (Eval("CopyMessageRead").ToString()="1" orelse Eval("CopyMessageRead").ToString()="True"))  %>' >
                    <div class="inner">
                        <img id="imgCR" runat="server" src="/Images2/mbr2/ckeck.png" alt=""/><br />
                        <span><%# MyBase.CurrentPageData.GetCustomString("lblCopyMessageRead")%></span>
                    </div>
                </div>
                <div class="inner-box <%# MyBase.IsMessageHidden(Container.DataItem)%> <%# MyBase.IsMessageNew(Container.DataItem)%>">
                    <div class="lfloat m-hdr"><asp:Literal ID="lblTextTitle" runat="server" Text="Body" /></div>
                    <div class="rfloat m-hdr-date"><asp:Literal ID="lblDateTime" runat="server" Text="" /><div class="flag-new"></div></div>
                    <div class="clear"></div>
                    <div class="m-subject"><asp:Label ID="lblSubject" runat="server" Text="" CssClass="span"/></div>
                    <div class="m-text"><asp:Label ID="lblText" runat="server" Text="" CssClass="span" /></div>
                    <div class="center"><asp:HyperLink ID="btnOpen" runat="server" Text="" CssClass="btn read-message" Visible="false"
                            onclick="ReadMessage(this);" /></div>
                    <div class="delete rfloat">
                        <asp:HyperLink ID="btnDelete" runat="server" Text="" CssClass="btn" />
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="right">
                <div class="photo" id="divFromRight" runat="server" visible='<%# (Eval("ProfileID").ToString()=MyBase.MasterProfileID) %>' >
                    <div class="pr-photo-88-noshad">
                        <div class="tt_enabled">

                        <asp:HyperLink ID="lnkFromRight" runat="server" NavigateUrl="#" CssClass="no-hand">
                            <img id="imgFromRight" runat="server" class="round-img-88" alt=""/>
                        </asp:HyperLink>
                             </div>
                   
                              <div class="tooltip tooltip_PhotoLevelHover">
                                    <div class="tooltip_text left-arrow"><asp:Label ID="lblPhotoLevelHover2" runat="server" Text="" /></div>
                                </div>
                             </div>
                    <div class="m-arrow"></div>
                </div>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
            <script type="text/javascript">
                conversations_setRowNumberMax(<%# Eval("RowNumber") %>);
                conversations_setMessageIDMin(<%# Eval("EUS_MessageID") %>);
            </script>
        </div>
    </ItemTemplate>
</asp:ListView>

<script type="text/javascript">
    function deleteMessage(o, e, permanent) {
        var msg = '';
        if (permanent == 1)
            msg = '<%= Dating.Server.Site.Web.AppUtils.JS_PrepareAlertString(CurrentPageData.GetCustomString("MessageDeleteConfirmationText_Permanently")) %>';
        else
            msg = '<%= Dating.Server.Site.Web.AppUtils.JS_PrepareAlertString(CurrentPageData.GetCustomString("MessageDeleteConfirmationText_ND")) %>';
        var r = confirm(msg);
        if (r) performRequest_MessageDelete(e, o);
        try {
            e.stopPropagation();
            e.preventDefault();
        }
        catch (e) { }
        return r;
    }
</script>



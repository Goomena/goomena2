﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucPopupGift.ascx.vb" 
    Inherits="Dating.Server.Site.Web.ucPopupGift" %>

 <script type="text/javascript">
     function EndRequestHandler1(sender, args) {
         try {
             LoadingPanel.Hide();
         }
         catch (e) { }
         try {
             setActions();
             //m__setPopupClass();
         }
         catch (e) { }
     }

     function openPopupGifts() {
         var contentUrl = '<%= ResolveUrl("~/Members/PopupGifts.aspx")%>', headerText = null;

         var popup = window["popupConfirmGift"]
         popup.SetContentUrl('<%= ResolveUrl("~/loading.htm")%>');
             popup.SetContentUrl(contentUrl);
             if (headerText != null) popup.SetHeaderText(headerText);
             var hdrId = popup.name + '_PW-1';
             var headerTop = hdrId + ' .dxpc-mainDiv.dxpc-shadow'
             $('#' + headerTop + ' .dxpc-header').css({ 'height': '0', 'width': '100%', 'position': 'absolute' });
             $('#' + headerTop + ' .dxpc-headerContent').css({ 'display': 'none' });
             $('#' + headerTop + ' .dxpc-closeBtn').css({ 'background-color': 'transparent', 'position': 'absolute', 'top': '10px', 'right': '10px' });
             popup.Show();
             popup.SetSize(672, 611);


             popup.UpdatePosition();
             popup.Closing.AddHandler(__Closing_Remove);
             popup.CloseUp.AddHandler(__CloseUp_Remove);


             function __Closing_Remove(s, e) {
                 try {
                     if (__RefreshPage == true) {
                     <%--   eval($('#<%= lnkRefresh.ClientID %>').attr("href").replace("javascript:",""));--%>
                     }
                     s.SetContentUrl('about:blank');
                     s.Closing.RemoveHandler(__Closing_Remove);
                 }
                 catch (e) { }
             }
             function __CloseUp_Remove(s, e) {
                 try {
                     s.CloseUp.RemoveHandler(__CloseUp_Remove);
                 }
                 catch (e) { }
             }


         }

         var __RefreshPage = false;
         function SetRefreshPage() {
             __RefreshPage = true;
         }
         function setActions() {
             var wdt = 670, hgt = 609;
             __RefreshPage = false;

         }

         $(function () { setActions(); });
         Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler1);


         var popupGiftsLoaded = false;
         function PopupGifts_Init(s, e) {
             popupGiftsLoaded = true;
         }
         function openPopupGiftsDelayed() {
             while (!popupGiftsLoaded) {
                 setTimeout(function () { openPopupGiftsDelayed() }, 1400);
                 return;
             }
             openPopupGifts();
         }
         function performRequest_PopUpGiftHasBeenSeen() {
             try {
                 var param = $.toJSON({ 'val': 'True' });
                 $.ajax({
                     type: "POST",
                     url: "json.aspx/GiftsPopupShown",
                     data: param,
                     contentType: "application/json; charset=utf-8",
                     dataType: "json",
                     async: true,
                     error: function (ar1, ar2, ar3) {
                     },
                     success: function (msg) { },
                     beforeSend: function () { },
                     complete: function () { }
                 });
             }
             catch (e) { }
         }

    </script>




<%--<asp:UpdatePanel ID="UpdatePanel1" runat="server">--%>
     <dx:ASPxPopupControl runat="server"
        Height="609px"
       
        ID="popupConfirmGift"
        ClientInstanceName="popupConfirmGift" 
        ClientIDMode="AutoID" 
        PopupVerticalAlign="WindowCenter" 
        PopupHorizontalAlign="WindowCenter" 
        Modal="True" 
        
         Action="CloseButton" Width="672px" ShowSizeGrip="False"  DragElement="Window" PopupAction="None" RenderMode="Lightweight" CloseAction="CloseButton">
    <ContentStyle HorizontalAlign="Left" VerticalAlign="Middle">
        <Paddings Padding="0px" />
        <Paddings Padding="0px" ></Paddings>
    </ContentStyle>
         <ClientSideEvents Closing="function(s, e) {
	performRequest_PopUpGiftHasBeenSeen();
}" />
    <CloseButtonStyle>
        <HoverStyle>
            <BackgroundImage ImageUrl="~/Images/spacer10.png" Repeat="NoRepeat" HorizontalPosition="center"
                VerticalPosition="center"></BackgroundImage>
        </HoverStyle>
        <BackgroundImage ImageUrl="~/Images/spacer10.png" Repeat="NoRepeat" HorizontalPosition="center"
            VerticalPosition="center"></BackgroundImage>
        <BorderLeft BorderColor="White" BorderStyle="None" BorderWidth="0px" />
    </CloseButtonStyle>
    <CloseButtonImage Url="~/Images2/gifts/Popup/close-button20141127.png" Height="30px" Width="30px">
    </CloseButtonImage>
        <SizeGripImage Url="~/Images/resize.png">
        </SizeGripImage>
    <HeaderStyle>
        <Paddings Padding="0px" PaddingLeft="0px" />
        <Paddings PaddingTop="0px" PaddingBottom="0px"></Paddings>
        <BorderTop BorderColor="White" BorderStyle="None" BorderWidth="0px" />
        <BorderRight BorderColor="White" BorderStyle="None" BorderWidth="0px" />
        <BorderBottom BorderColor="White" BorderStyle="None" BorderWidth="0px" />
            </HeaderStyle>
<ModalBackgroundStyle BackColor="Black" Opacity="70"></ModalBackgroundStyle>
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server" SupportsDisabledAttribute="True" Height="609px" Width="670px">
            </dx:PopupControlContentControl>
        </ContentCollection>
             <ClientSideEvents Init="PopupGifts_Init" />
    </dx:ASPxPopupControl>
    
<%--</asp:UpdatePanel>--%>





﻿Imports Dating.Server.Core.DLL
Imports DevExpress.Web.ASPxEditors
Imports Dating.Server.Datasets.DLL

''' <summary>
''' This control is loaded when a MAN view the search
''' </summary>
''' <remarks></remarks>
Public Class UcTopDashboardMembers
    Inherits BaseUserControl

    Protected Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData("control.UcTopDashboardMembers", Context, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property


    Private Sub Page_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        Try
            If Me._pageData IsNot Nothing Then
                RemoveHandler Me._pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If

        Catch ex As Exception
        End Try

        Try
            MyBase.Dispose()
        Catch ex As Exception
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If (Not Me.IsPostBack) Then
                LoadLAG()
               
            End If
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Protected Sub LoadLAG()
        Try

            'lblCriteriaHeader.Text = CurrentPageData.GetCustomString("lblCriteriaTop")
            lnkMoreProfilesWithBirthDay.Text = CurrentPageData.GetCustomString("btnMoreProfiles")
            btnMoreNewMembers.Text = lnkMoreProfilesWithBirthDay.Text
            lblNewMembers.Text = CurrentPageData.GetCustomString("lblNewMembers")
            lblWhoHasBirthDay.Text = CurrentPageData.GetCustomString("lblWhoHasBirthDay")
            FillContentNewMembers()
            FillContentWhoHasBirthday()
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub
    Private Sub FillContentNewMembers()
        Dim params As New clsSearchHelperParameters()
        params.CurrentProfileId = Me.MasterProfileId
        params.ReturnRecordsWithStatus = ProfileStatusEnum.Approved
        params.SearchSort = SearchSortEnum.NewestMember
        params.zipstr = Me.SessionVariables.MemberData.Zip
        params.latitudeIn = Me.SessionVariables.MemberData.latitude
        params.longitudeIn = Me.SessionVariables.MemberData.longitude
        params.Distance = 100000
        params.LookingFor_ToMeetMaleID = Me.SessionVariables.MemberData.LookingFor_ToMeetMaleID
        params.LookingFor_ToMeetFemaleID = Me.SessionVariables.MemberData.LookingFor_ToMeetFemaleID
        params.NumberOfRecordsToReturn = 4
        params.rowNumberMin = 0
        params.rowNumberMax = params.NumberOfRecordsToReturn
        params.AdditionalWhereClause = "and not exists(select * from EUS_ProfilesViewed where FromProfileID=@CurrentProfileId and ToProfileID=EUS_Profiles.ProfileID)"
        params.performCount = False
        lvNewMembers.DataSource = clsSearchHelper.GetMembersToSearchDataTableQuick(params)
        lvNewMembers.DataBind()

    End Sub
    Private Sub FillContentWhoHasBirthday()

        Dim params2 As New clsSearchHelperParameters()
        params2.CurrentProfileId = Me.MasterProfileId
        params2.ReturnRecordsWithStatus = ProfileStatusEnum.Approved
        params2.SearchSort = SearchSortEnum.Birthday
        params2.zipstr = Me.SessionVariables.MemberData.Zip
        params2.latitudeIn = Me.SessionVariables.MemberData.latitude
        params2.longitudeIn = Me.SessionVariables.MemberData.longitude
        params2.Distance = 100000
        params2.LookingFor_ToMeetMaleID = Me.SessionVariables.MemberData.LookingFor_ToMeetMaleID
        params2.LookingFor_ToMeetFemaleID = Me.SessionVariables.MemberData.LookingFor_ToMeetFemaleID
        params2.NumberOfRecordsToReturn = 4
        params2.rowNumberMin = 0
        params2.rowNumberMax = params2.NumberOfRecordsToReturn
        params2.performCount = False
        params2.AdditionalWhereClause = " and  (CelebratingBirth>=DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE())))"
        lvWhoHasBirthday.DataSource = clsSearchHelper.GetMembersToSearchDataTableQuick(params2)
        lvWhoHasBirthday.DataBind()
    End Sub
    Public Function evaluateAgeAndCity(birthday As Object, city As Object) As String
        Dim age As String = ""
        If birthday IsNot Nothing Then
            Try
                '  age = Me.CurrentPageData.VerifyCustomString("lblAgeYearsOld").Replace("###AGE###", ProfileHelper.GetCurrentAge(birthday))
                age = ProfileHelper.GetCurrentAge(birthday)

            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try
        End If
        If city IsNot Nothing Then
            Try
                '  age = Me.CurrentPageData.VerifyCustomString("lblAgeYearsOld").Replace("###AGE###", ProfileHelper.GetCurrentAge(birthday))
                If age <> "" Then
                    age &= ", "
                End If
                age &= CStr(city)

            Catch ex As Exception

                WebErrorMessageBox(Me, ex, "")
            End Try
        End If
        Return age

    End Function
End Class
﻿Imports DevExpress.Web.ASPxPager
Imports Dating.Server.Core.DLL

Public Class LoveHateControl
    Inherits BaseUserControl


    'Private _VIPTooltip As String
    'Protected Property VIPTooltip As String
    '    Get
    '        Return _VIPTooltip
    '    End Get
    '    Set(value As String)
    '        _VIPTooltip = value
    '    End Set
    'End Property


    Public Enum WinkUsersListViewEnum
        None = 0

        SearchingOffers = 5
        Winners
    End Enum

    Public Enum MessagesListViewEnum
        None = 0
        AllMessages = 24

        NewMessages = 12
        Inbox = 1
        SentMessages = 2
        Trash = 3
    End Enum

    Public Property InfoWinUrl As String
    Public Property InfoWinHeaderText As String

    Public Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            If (_pageData Is Nothing) Then _pageData = New clsPageData("control.OfferControl", Context)
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData("cms", Context, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property
    Private Sub Page_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        Try
            If Me._pageData IsNot Nothing Then
                RemoveHandler Me._pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If

        Catch ex As Exception
        End Try
       
        Try
            MyBase.Dispose()
        Catch ex As Exception
        End Try
    End Sub

    Public Property ShowNoPhotoText As Boolean
    Public Property ShowEmptyListText As Boolean

    Public Property UsersListView As WinkUsersListViewEnum
        Get
            If (Me.ViewState("WinkUsersListViewEnum") IsNot Nothing) Then Return Me.ViewState("WinkUsersListViewEnum")
            Return WinkUsersListViewEnum.None
        End Get
        Set(value As WinkUsersListViewEnum)
            Me.ViewState("WinkUsersListViewEnum") = value
        End Set
    End Property


    Private _list As New List(Of clsLoveHateListItem)
    Public Property UsersList As List(Of clsLoveHateListItem)
        Get
            If (Me._list Is Nothing) Then Me._list = New List(Of clsLoveHateListItem)
            Return Me._list
        End Get
        Set(value As List(Of clsLoveHateListItem))
            Me._list = value
        End Set
    End Property



    <PersistenceMode(PersistenceMode.InnerDefaultProperty)>
    Public ReadOnly Property Repeater As Repeater
        Get
      

            Return rptLoveHate

            
        End Get
    End Property


    Protected Overrides Sub OnInit(e As System.EventArgs)
        MyBase.OnInit(e)
        'ModGlobals.AddScriptResourceToHeader(Me.Page, "~/Scripts/jquery-1.7.min.js")
        'ModGlobals.AddScriptResourceToHeader(Me.Page, "~/Scripts/bootstrap-dropdown.js")
    End Sub




    Public Overrides Sub DataBind()


        '  Me.MultiView1.SetActiveView(vwLoveHateVote)
     


        If (Me.Repeater IsNot Nothing) Then

            Try
                For Each itm As clsLoveHateListItem In Me.UsersList
                    itm.FillTextResourceProperties(Me.CurrentPageData)
                Next
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try

            Me.Repeater.DataSource = Me.UsersList
            Me.Repeater.DataBind()
        End If


        'If (Me.IsFemale) Then
        '    VIPTooltip = Me.CurrentPageData.GetCustomString("lbTooltip.MenVIP.ForWomen")
        'Else
        '    VIPTooltip = Me.CurrentPageData.GetCustomString("lbTooltip.MenVIP.ForMen")
        'End If

    End Sub


    'Public Sub FillTextResourceProperties(itm As clsWinkUserListItem, pageData As clsPageData)

    '    If (String.IsNullOrEmpty(itm.HasNoPhotosText)) Then itm.HasNoPhotosText = pageData.GetCustomString("HasNoPhotosText")
    '    If (String.IsNullOrEmpty(itm.SearchOurMembersText)) Then itm.SearchOurMembersText = pageData.GetCustomString("SearchOurMembersText")
    '    If (String.IsNullOrEmpty(itm.YouReceivedWinkOfferText)) Then itm.YouReceivedWinkOfferText = pageData.GetCustomString("YouReceivedWinkText")
    '    If (String.IsNullOrEmpty(itm.WantToKnowFirstDatePriceText)) Then itm.WantToKnowFirstDatePriceText = pageData.GetCustomString("WantToKnowFirstDatePriceText")
    '    If (String.IsNullOrEmpty(itm.MakeOfferText)) Then itm.MakeOfferText = pageData.GetCustomString("MakeOfferText")
    '    If (String.IsNullOrEmpty(itm.AcceptText)) Then itm.AcceptText = pageData.GetCustomString("AcceptText")
    '    If (String.IsNullOrEmpty(itm.CounterText)) Then itm.CounterText = pageData.GetCustomString("CounterText")
    '    If (String.IsNullOrEmpty(itm.NotInterestedText)) Then itm.NotInterestedText = pageData.GetCustomString("NotInterestedText")
    '    If (String.IsNullOrEmpty(itm.TooFarAwayText)) Then itm.TooFarAwayText = pageData.GetCustomString("TooFarAwayText")
    '    If (String.IsNullOrEmpty(itm.NotEnoughInfoText)) Then itm.NotEnoughInfoText = pageData.GetCustomString("NotEnoughInfoText")
    '    If (String.IsNullOrEmpty(itm.DifferentExpectationsText)) Then itm.DifferentExpectationsText = pageData.GetCustomString("DifferentExpectationsText")
    '    If (String.IsNullOrEmpty(itm.YearsOldText)) Then itm.YearsOldText = pageData.GetCustomString("YearsOldText")

    '    If (String.IsNullOrEmpty(itm.DeleteOfferText)) Then itm.DeleteOfferText = pageData.GetCustomString("DeleteOfferText")
    '    If (String.IsNullOrEmpty(itm.DeleteWinkText)) Then itm.DeleteWinkText = pageData.GetCustomString("DeleteWinkText")

    '    If (String.IsNullOrEmpty(itm.SendMessageText)) Then itm.SendMessageText = pageData.GetCustomString("SendMessageText")

    '    If (String.IsNullOrEmpty(itm.WinkText)) Then itm.WinkText = pageData.GetCustomString("WinkText")
    '    If (String.IsNullOrEmpty(itm.UnWinkText)) Then itm.UnWinkText = pageData.GetCustomString("UnWinkText")

    '    If (String.IsNullOrEmpty(itm.FavoriteText)) Then itm.FavoriteText = pageData.GetCustomString("FavoriteText")
    '    If (String.IsNullOrEmpty(itm.UnfavoriteText)) Then itm.UnfavoriteText = pageData.GetCustomString("UnfavoriteText")

    '    If (String.IsNullOrEmpty(itm.YearsOldFromText)) Then itm.YearsOldFromText = pageData.GetCustomString("YearsOldFromText")
    '    If (String.IsNullOrEmpty(itm.TryWinkText)) Then itm.TryWinkText = pageData.GetCustomString("TryWinkText")
    '    If (String.IsNullOrEmpty(itm.MakeNewOfferText)) Then itm.MakeNewOfferText = pageData.GetCustomString("MakeNewOfferText")
    '    If (String.IsNullOrEmpty(itm.YourWinkSentText)) Then itm.YourWinkSentText = pageData.GetCustomString("YourWinkSentText")
    '    If (String.IsNullOrEmpty(itm.AwaitingResponseText)) Then itm.AwaitingResponseText = pageData.GetCustomString("AwaitingResponseText")
    '    If (String.IsNullOrEmpty(itm.AddPhotosText)) Then itm.AddPhotosText = pageData.GetCustomString("AddPhotosText")

    '    If (String.IsNullOrEmpty(itm.UnblockText)) Then itm.UnblockText = pageData.GetCustomString("UnblockText")
    '    If (String.IsNullOrEmpty(itm.PokeText)) Then itm.PokeText = pageData.GetCustomString("PokeText")
    '    If (String.IsNullOrEmpty(itm.ActionsText)) Then itm.ActionsText = pageData.GetCustomString("ActionsText")
    '    If (String.IsNullOrEmpty(itm.ActionsMakeOfferText)) Then itm.ActionsMakeOfferText = pageData.GetCustomString("ActionsMakeOfferText")
    '    If (String.IsNullOrEmpty(itm.CancelPokeText)) Then itm.CancelPokeText = pageData.GetCustomString("CancelPokeText")


    '    ''''''''''''''''''''''''''''
    '    'canceled/rejected
    '    ''''''''''''''''''''''''''''
    '    If (String.IsNullOrEmpty(itm.RejectText)) Then itm.RejectText = pageData.GetCustomString("RejectText")
    '    If (String.IsNullOrEmpty(itm.CancelWinkText)) Then itm.CancelWinkText = pageData.GetCustomString("CancelWinkText")
    '    If (String.IsNullOrEmpty(itm.CancelOfferText)) Then itm.CancelOfferText = pageData.GetCustomString("CancelOfferText")

    '    If (String.IsNullOrEmpty(itm.CancelledRejectedTitleText)) Then itm.CancelledRejectedTitleText = pageData.GetCustomString("WinkCancelledText")

    '    If (String.IsNullOrEmpty(itm.CancelledRejectedDescriptionText)) Then
    '        itm.CancelledRejectedDescriptionText = pageData.GetCustomString("YouCancelledWinkToText")
    '        itm.CancelledRejectedDescriptionText = itm.ReplaceCommonTookens(itm.CancelledRejectedDescriptionText)
    '    End If



    '    If (String.IsNullOrEmpty(itm.MilesAwayText)) Then
    '        itm.MilesAwayText = pageData.GetCustomString("MilesAwayText")
    '        itm.MilesAwayText = itm.ReplaceCommonTookens(itm.MilesAwayText)
    '    End If


    '    If (String.IsNullOrEmpty(itm.OfferDeleteConfirmMessage)) Then
    '        itm.OfferDeleteConfirmMessage = pageData.GetCustomString("OfferDeleteConfirmMessage")
    '        If (Not String.IsNullOrEmpty(itm.OfferDeleteConfirmMessage)) Then
    '            itm.OfferDeleteConfirmMessage = itm.OfferDeleteConfirmMessage.Replace("'", "\'")
    '            itm.OfferDeleteConfirmMessage = "return confirm('" & itm.OfferDeleteConfirmMessage & "');"
    '        End If
    '    End If


    '    If (String.IsNullOrEmpty(itm.YouReceivedPokeDescriptionText)) Then
    '        itm.YouReceivedPokeDescriptionText = pageData.GetCustomString("YouReceivedPokeDescriptionText")
    '        itm.YouReceivedPokeDescriptionText = itm.ReplaceCommonTookens(itm.YouReceivedPokeDescriptionText)
    '    End If


    '    If (String.IsNullOrEmpty(itm.WillYouAcceptDateWithForAmountText)) Then
    '        itm.WillYouAcceptDateWithForAmountText = pageData.GetCustomString("WillYouAcceptDateWithForAmountText")
    '        itm.WillYouAcceptDateWithForAmountText = itm.ReplaceCommonTookens(itm.WillYouAcceptDateWithForAmountText)
    '    End If


    '    If (String.IsNullOrEmpty(itm.OfferAcceptedHowToContinueText)) Then
    '        itm.OfferAcceptedHowToContinueText = pageData.GetCustomString("OfferAcceptedHowToContinueText")
    '        itm.OfferAcceptedHowToContinueText = itm.ReplaceCommonTookens(itm.OfferAcceptedHowToContinueText)
    '    End If

    '    If (String.IsNullOrEmpty(itm.OfferAcceptedUnlockText)) Then
    '        itm.OfferAcceptedUnlockText = pageData.GetCustomString("OfferAcceptedUnlockText")
    '        itm.OfferAcceptedUnlockText = itm.ReplaceCommonTookens(itm.OfferAcceptedUnlockText)
    '    End If

    '    If (String.IsNullOrEmpty(itm.ActionsUnlockText)) Then
    '        itm.ActionsUnlockText = pageData.GetCustomString("ActionsUnlockText")
    '        itm.ActionsUnlockText = itm.ReplaceCommonTookens(itm.ActionsUnlockText)
    '    End If

    '    If (String.IsNullOrEmpty(itm.UnlockNotice)) Then
    '        itm.UnlockNotice = pageData.GetCustomString("UnlockNotice")
    '        itm.UnlockNotice = itm.ReplaceCommonTookens(itm.UnlockNotice)
    '    End If

    '    If (itm.RejectedByProfileID = itm.OtherMemberProfileID) Then
    '        itm.RejectCssClass = "deal_right_rej"
    '    Else
    '        itm.RejectCssClass = "deal_left"
    '    End If


    '    ''''''''''''''''''''''''''''
    '    'accepted
    '    ''''''''''''''''''''''''''''
    '    If (String.IsNullOrEmpty(itm.MessageSentNotice)) Then itm.MessageSentNotice = pageData.GetCustomString("MessageSentNotice")

    '    If (String.IsNullOrEmpty(itm.WhatIsText)) Then itm.WhatIsText = pageData.GetCustomString("WhatIsText")
    '    itm.AllowTooltipPopupLikes = True

    '    If (String.IsNullOrEmpty(itm.SendMessageOnceText)) Then
    '        itm.SendMessageOnceText = pageData.GetCustomString("SendMessageOnceText")
    '        itm.SendMessageOnceText = itm.ReplaceCommonTookens(itm.SendMessageOnceText)
    '    End If


    '    If (String.IsNullOrEmpty(itm.SendMessageManyText)) Then
    '        itm.SendMessageManyText = pageData.GetCustomString("SendMessageManyText")
    '        itm.SendMessageManyText = itm.ReplaceCommonTookens(itm.SendMessageManyText)
    '    End If


    'End Sub

    'Protected Function WriteOffer_MemberInfo(DataItem As Dating.Server.Site.Web.clsWinkUserListItem) As Object
    '    'Dim str As String = "<ul>" & vbCrLf & _
    '    '    "<li><b>" & DataItem.OtherMemberAge") & "</b> " & DataItem.YearsOldText") & "</li>" & vbCrLf & _
    '    '    "<li><b>" & DataItem.OtherMemberCity") & ", " & DataItem.OtherMemberRegion") & "</b></li>" & vbCrLf & _
    '    '    "</ul>" & vbCrLf

    '    'Return str


    '    Dim sb As New StringBuilder()
    '    Try
    '        sb.Append("<ul>")
    '        sb.Append("<li>")

    '        If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberAge) Then
    '            sb.Append("<b>" & DataItem.OtherMemberAge & "</b>")
    '            sb.Append(" " & DataItem.YearsOldText & " ")
    '        End If

    '        sb.Append("</li>")


    '        sb.Append("<li>")
    '        sb.Append("<b>")

    '        'If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberCity) Then
    '        '    sb.Append(DataItem.OtherMemberCity)
    '        'End If

    '        'If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberCity) AndAlso Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberRegion) Then
    '        '    sb.Append(",  ")
    '        'End If

    '        'If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberRegion) Then
    '        '    sb.Append(DataItem.OtherMemberRegion)
    '        'End If

    '        sb.Append(IIf(Not String.IsNullOrEmpty(DataItem.OtherMemberCity), DataItem.OtherMemberCity, ""))
    '        sb.Append(",  ")
    '        sb.Append(IIf(Not String.IsNullOrEmpty(DataItem.OtherMemberRegion), DataItem.OtherMemberRegion, ""))
    '        sb.Append(",  ")
    '        sb.Append(IIf(Not String.IsNullOrEmpty(DataItem.OtherMemberCountry), ProfileHelper.GetCountryName(DataItem.OtherMemberCountry), ""))
    '        sb.Replace(",  " & ",  ", ",  ")
    '        sb.Append("</b>")
    '        sb.Append("</li>")


    '        sb.Append("</ul>")

    '    Catch ex As Exception
    '        WebErrorMessageBox(Me, ex, "")
    '    End Try

    '    Return sb.ToString()
    'End Function

    'Protected Function WriteOffer_MemberInfo(DataItem As Dating.Server.Site.Web.clsLoveHateListItem) As String
    '    Dim sb As New StringBuilder()
    '    Try
    '        sb.Append("<ul class=""info2"">")

    '        If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberAge) Then
    '            sb.Append("<li class=""age1"">")
    '            sb.Append("<b>")
    '            sb.Append(DataItem.OtherMemberAge)
    '            sb.Append(" " & DataItem.YearsOldText & " ")
    '            sb.Append("</b>")
    '            sb.Append("</li>")
    '        End If


    '        If (Not String.IsNullOrEmpty(DataItem.OtherMemberCity)) Then
    '            sb.Append("<li>")
    '            sb.Append(IIf(Not String.IsNullOrEmpty(DataItem.OtherMemberCity), DataItem.OtherMemberCity, ""))
    '            If (Not String.IsNullOrEmpty(DataItem.OtherMemberRegion)) Then sb.Append(",  ")
    '            sb.Append("</li>")
    '        End If

    '        sb.Append("<li>")
    '        sb.Append(IIf(Not String.IsNullOrEmpty(DataItem.OtherMemberRegion), DataItem.OtherMemberRegion, ""))
    '        sb.Append(",  ")
    '        sb.Append(IIf(Not String.IsNullOrEmpty(DataItem.OtherMemberCountry), ProfileHelper.GetCountryName(DataItem.OtherMemberCountry), ""))
    '        sb.Replace(",  " & ",  ", ",  ")
    '        sb.Append("</li>")


    '        sb.Append("</ul>")

    '    Catch ex As Exception
    '        WebErrorMessageBox(Me, ex, "")
    '    End Try

    '    Return sb.ToString()
    'End Function

    'Protected Function WriteSearch_MemberInfo(DataItem As Dating.Server.Site.Web.clsLoveHateListItem) As Object
    '    Dim sb As New StringBuilder()
    '    Try

    '        sb.Append("<ul>")
    '        sb.Append("<li>")

    '        If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberAge) Then
    '            sb.Append("<b>" & DataItem.OtherMemberAge & "</b>")
    '            sb.Append(" " & DataItem.YearsOldText & " ")

    '            If (Not String.IsNullOrEmpty(DataItem.ZodiacString)) Then
    '                sb.Append("(" & DataItem.OtherMemberBirthday.Value.ToString("d MMM, yyyy") & ")")
    '            End If
    '        End If

    '        sb.Append("<div class=""s_item_address"">")

    '        sb.Append(IIf(Not String.IsNullOrEmpty(DataItem.OtherMemberCity), DataItem.OtherMemberCity, ""))
    '        sb.Append(",  ")
    '        sb.Append(IIf(Not String.IsNullOrEmpty(DataItem.OtherMemberRegion), DataItem.OtherMemberRegion, ""))
    '        sb.Append(",  ")
    '        sb.Append(IIf(Not String.IsNullOrEmpty(DataItem.OtherMemberCountry), ProfileHelper.GetCountryName(DataItem.OtherMemberCountry), ""))
    '        sb.Replace(",  " & ",  ", ",  ")

    '        sb.Append("</div>")
    '        sb.Append("</li>")


    '        sb.Append("<li>")
    '        sb.Append("<b>")

    '        If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberHeight) Then
    '            sb.Append(DataItem.OtherMemberHeight)
    '        End If

    '        If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberHeight) AndAlso Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberPersonType) Then
    '            sb.Append(" - ")
    '        End If

    '        If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberPersonType) Then
    '            sb.Append(DataItem.OtherMemberPersonType)
    '        End If

    '        sb.Append("</b>")
    '        sb.Append("</li>")


    '        sb.Append("<li>")

    '        If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberHair) Then
    '            sb.Append(DataItem.OtherMemberHair)
    '        End If

    '        If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberEyes) Then
    '            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberHair) Then sb.Append(", ")
    '            sb.Append(DataItem.OtherMemberEyes)
    '        End If

    '        If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberEthnicity) Then
    '            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberEyes) OrElse Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberHair) Then sb.Append(", ")
    '            sb.Append(DataItem.OtherMemberEthnicity)
    '        End If

    '        sb.Append("</li>")
    '        sb.Append("</ul>")

    '    Catch ex As Exception
    '        WebErrorMessageBox(Me, ex, "")
    '    End Try

    '    Return sb.ToString()
    'End Function

    Protected Shared Function ShowIsOnline(DataItem As Dating.Server.Site.Web.clsLoveHateListItem) As Boolean
        If (DataItem.OtherMemberIsOnline OrElse DataItem.OtherMemberIsOnlineRecently) Then Return True
        Return False
    End Function


    'Protected Sub rptAccepted_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptAccepted.ItemDataBound
    '    Try

    '        Dim divTooltipholder As Control = e.Item.FindControl("divTooltipholder")
    '        If (divTooltipholder IsNot Nothing) Then
    '            Dim itm As clsWinkUserListItem = e.Item.DataItem
    '            If (ProfileHelper.IsMale(itm.Genderid)) Then
    '                divTooltipholder.Visible = True
    '            End If
    '        End If

    '    Catch ex As Exception

    '    End Try
    'End Sub



    Protected Sub rptSearch_ItemDataBound(source As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptLoveHate.ItemDataBound
        'Try

        '    Dim lnkWhatIs As LinkButton = e.Item.FindControl("lnkWhatIs")
        '    If (lnkWhatIs IsNot Nothing) Then
        '        lnkWhatIs.ToolTip = Me.CurrentPageData.GetCustomString("WhatIsText")

        '        If (String.IsNullOrEmpty(InfoWinUrl)) Then InfoWinUrl = ResolveUrl("~/Members/InfoWin.aspx?info=searchcommands")
        '        If (String.IsNullOrEmpty(InfoWinHeaderText)) Then InfoWinHeaderText = Me.CurrentPageData.GetCustomString("Search_WhatIsPopup_HeaderText")

        '        lnkWhatIs.OnClientClick = WhatIsIt.OnMoreInfoClickFunc(lnkWhatIs.OnClientClick,
        '                                     InfoWinUrl,
        '                                     InfoWinHeaderText)
        '    End If

        'Catch ex As Exception

        'End Try
    End Sub

End Class
﻿<%@ Control Language="vb" AutoEventWireup="false" 
    CodeBehind="PublicMasterBottom.ascx.vb" Inherits="Dating.Server.Site.Web.PublicMasterBottom"
    EnableViewState="false" %>
<%@ Register src="~/UserControls/TraceSrc.ascx" tagname="TraceSrc" tagprefix="uc1" %>

<div id="footer-content">
    <div class="footernew">
        <div class="footerbox">
        <div class="ul1">
            <asp:Literal ID="lblFooterMenuTitle1" runat="server" Text="Label" />
            <dx:ASPxMenu ID="mnuFooter1" runat="server" EnableDefaultAppearance="False" 
                Orientation="Vertical">
                <ClientSideEvents ItemClick="ShowLoadingOnMenu" />
            </dx:ASPxMenu>
                      <%--<div style="border:1px solid #000;"></div>
            <dx:ASPxMenu ID="mnuFooter1More" runat="server" EnableDefaultAppearance="False" 
                Orientation="Vertical">
                <ClientSideEvents ItemClick="ShowLoadingOnMenu" />
            </dx:ASPxMenu>--%>
        </div>
        <div class="ul2">
            <asp:Literal ID="lblFooterMenuTitle5" runat="server" Text="Label"/>
            <dx:ASPxMenu ID="mnuFooter5" runat="server" EnableDefaultAppearance="False" 
                EncodeHtml="False" Orientation="Vertical">
                <ClientSideEvents ItemClick="ShowLoadingOnMenu" />
            </dx:ASPxMenu>
        </div>
        <div class="ul3">
            <asp:Literal ID="lblFooterMenuTitle3" runat="server" Text="Label"/>
            <dx:ASPxMenu ID="mnuFooter3" runat="server" EnableDefaultAppearance="False" 
                Orientation="Vertical">
                <ClientSideEvents ItemClick="ShowLoadingOnMenu" />
            </dx:ASPxMenu>
        </div>
        <div class="ul4">
            <asp:Literal ID="lblFooterMenuTitle2" runat="server" Text="Label"/>
            <dx:ASPxMenu ID="mnuFooter2" runat="server" EnableDefaultAppearance="False" 
                Orientation="Vertical">
                <ClientSideEvents ItemClick="ShowLoadingOnMenu" />
            </dx:ASPxMenu>
        </div>
        <div class="ul5">
            <asp:Literal ID="lblFooterMenuTitle4" runat="server" Text="Label"/>
            <dx:ASPxMenu ID="mnuFooter4" runat="server" EnableDefaultAppearance="False" 
                Orientation="Vertical">
                <ClientSideEvents ItemClick="ShowLoadingOnMenu" />
            </dx:ASPxMenu>
       </div>
            <div class="socialbox">
                 <p>Social Media</p>
                 <div class="twcontainer">
                 <%--<a href="https://twitter.com/intent/user?screen_name=goomena"><img src="//cdn.goomena.com/goomena-kostas/first-general/tw.png" alt="twitter"/></a>--%>
                 <a href="https://twitter.com/intent/tweet?text=Goomena.com is definitely the Nr1 dating service! @Goomena beautiful women wait in line to be spoiled by you and spoil you back!"><img src="//cdn.goomena.com/goomena-kostas/first-general/tw.png" alt="twitter"/></a>
                 <p id="followers" style="display:none;"></p>
                 </div>
                 <div class="gpluscontainer">
                 <a href="https://plus.google.com/117771129858965427748/posts"><img src="//cdn.goomena.com/goomena-kostas/first-general/google-plus.png" alt="facebook"/></a>
                 <%--<p id="plusone"></p>--%>
                 </div>
                 <div class="fbcontainer">
                 <a href="https://www.facebook.com/goomena"><img src="//cdn.goomena.com/goomena-kostas/first-general/fb.png" alt="facebook"/></a>
                 <p id="fblikes"></p>
                 </div>
                 <div class="clear"></div>

            </div>
        <%--<asp:Label ID="lblEscortsWarning" runat="server"></asp:Label>--%>
            <div>
                <div class="lfloat"><img class="footerlogo" src="//cdn.goomena.com/goomena-kostas/first-general/logo-footer.png" alt="logo" /></div>
                <div class="rfloat"><img src="//cdn.goomena.com/images2/info-icons.png" alt="logo" /></div>
                <div class="clear"></div>
            </div>

            <div class="infobox">
                <h3><asp:Literal ID="lblFooterSiteTitle" runat="server" Text=""/></h3>
                <p class="info"><asp:Literal ID="lblFooterSiteDescription" runat="server" Text=""/>
                <br/><br/><br/>
        <div class="prosoxicontainer">
            <a class="prosoxi2" href="/cmspage.aspx?pageid=197&title=EscortsDisallow">
                <asp:Label ID="lblEscortsWarning" runat="server" Text="" CssClass="prosoxi2"></asp:Label></a>
                    <dx:ASPxPopupControl ID="lblEscortsWarningPopup" runat="server" 
                        CloseAction="MouseOut" PopupAction="MouseOver" 
                        PopupElementID="lblEscortsWarning" PopupVerticalAlign="Above" 
                        ShowHeader="False" Width="300px" CssClass="tooltip_Popup" ForeColor="White" BackColor="Black">                        
                            <ContentCollection>
                                <dx:PopupControlContentControl ID="PopupControlContentControl10" runat="server" BackColor="Black" ForeColor="White">
    <%--<asp:Label ID="lblEscortsWarningTooltip" runat="server" Text="" ForeColor="White"></asp:Label>--%>
                                </dx:PopupControlContentControl>
                            </ContentCollection>
                            <ContentStyle>
                                <Paddings Padding="10px" />
                            </ContentStyle>
                            <HeaderStyle>
                            <Paddings Padding="10px" />
                            </HeaderStyle>
                            <FooterStyle>
                            <Paddings Padding="10px" />
                            </FooterStyle>
                    </dx:ASPxPopupControl>
                    </div>
                                        
                </p>
            </div>


            <div class="siteinfo">
                <asp:Label ID="lblFooterCopyRight" runat="server" Text="" CssClass="copyrightParagraph" Visible="false"/> User IP: <%= Session("IP") & " - " & Session("GEO_COUNTRY_CODE")%>
                <asp:HyperLink ID="lnkSiteMap" runat="server" NavigateUrl="~/sitemap.aspx" onclick="ShowLoading();">Sitemap</asp:HyperLink>
            </div>

        </div>
    </div>
    <div style="background: white; min-width: 1024px;padding-top: 7px;">
        <div style="" class="footerlinks">
            <asp:Label ID="lblMoreInfo" runat="server" Text="Label"></asp:Label>
            <div style="float:left;width: 295px;font-size: 14px;">
                <dx:ASPxMenu ID="MenuLinks1" runat="server" EnableDefaultAppearance="False"
                    Orientation="Vertical" ForeColor="#14A8E6" ItemStyle-ForeColor="#14A8E6" CssClass="menulinks">
                    <ClientSideEvents ItemClick="ShowLoadingOnMenu" />
                </dx:ASPxMenu>
                <div id="WrapMenuLinks1More" style="display: none;">
                <dx:ASPxMenu ID="MenuLinks1More" runat="server" EnableDefaultAppearance="False"
                    Orientation="Vertical" ForeColor="#14A8E6" ItemStyle-ForeColor="#14A8E6" CssClass="menulinks">
                    <ClientSideEvents ItemClick="ShowLoadingOnMenu" />
                </dx:ASPxMenu>
                </div>
                <div class="link-more-articles">
                    <asp:HyperLink ID="lnkTogglMenuLinks1More" runat="server" NavigateUrl="javascript:void(1)" onclick="toggleMenuLinks(this,'#WrapMenuLinks1More');">More articles...</asp:HyperLink>
                </div>
            </div>
            <div style="float:left;margin-left:40px;width: 295px;font-size: 14px;">
                <dx:ASPxMenu ID="MenuLinks2" runat="server" EnableDefaultAppearance="False"
                    Orientation="Vertical" ForeColor="#14A8E6" ItemStyle-ForeColor="#14A8E6" CssClass="menulinks">
                    <ClientSideEvents ItemClick="ShowLoadingOnMenu" />
                </dx:ASPxMenu>
                <div id="WrapMenuLinks2More" style="display: none;">
                <dx:ASPxMenu ID="MenuLinks2More" runat="server" EnableDefaultAppearance="False"
                    Orientation="Vertical" ForeColor="#14A8E6" ItemStyle-ForeColor="#14A8E6" CssClass="menulinks">
                    <ClientSideEvents ItemClick="ShowLoadingOnMenu" />
                </dx:ASPxMenu>
                </div>
                <div class="link-more-articles">
                    <asp:HyperLink ID="lnkTogglMenuLinks2More" runat="server" NavigateUrl="javascript:void(2)" onclick="toggleMenuLinks(this,'#WrapMenuLinks2More');">More articles...</asp:HyperLink>
                </div>
            </div>
            <div style="float:left;margin-left:40px;width: 295px;font-size: 14px;">
                <dx:ASPxMenu ID="MenuLinks3" runat="server" EnableDefaultAppearance="False"
                    Orientation="Vertical" ForeColor="#14A8E6" ItemStyle-ForeColor="#14A8E6" CssClass="menulinks">
                    <ClientSideEvents ItemClick="ShowLoadingOnMenu" />
                </dx:ASPxMenu>
                <div id="WrapMenuLinks3More" style="display: none;">
                    <dx:ASPxMenu ID="MenuLinks3More" runat="server" EnableDefaultAppearance="False"
                        Orientation="Vertical" ForeColor="#14A8E6" ItemStyle-ForeColor="#14A8E6" CssClass="menulinks">
                        <ClientSideEvents ItemClick="ShowLoadingOnMenu" />
                    </dx:ASPxMenu>
                    <dx:ASPxMenu ID="MenuLinks4" runat="server" EnableDefaultAppearance="False"
                        Orientation="Vertical" ForeColor="#14A8E6" ItemStyle-ForeColor="#14A8E6" CssClass="menulinks">
                        <ClientSideEvents ItemClick="ShowLoadingOnMenu" />
                    </dx:ASPxMenu>
                </div>
                <div class="link-more-articles">
                    <asp:HyperLink ID="lnkTogglMenuLinks3More" runat="server" NavigateUrl="javascript:void(3)" onclick="toggleMenuLinks(this,'#WrapMenuLinks3More');">More articles...</asp:HyperLink>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>


<script type="text/javascript">
    function toggleMenuLinks(lnk, jqid) {
        var isHidden = ($(jqid).css('display') == 'none');
        if (isHidden) {
            $(lnk).attr("title", "<%= globalStrings.GetCustomString("msg_ClickToCollapse", GetLag())%>")
                .text("<%= CurrentPageData.GetCustomString("MenuLinksMore.Less")%>");
            $('.menulinks', jqid).css('visibility', 'hidden');
            $(jqid).show('slow', function () {
                $('.menulinks', jqid).css('visibility', '');
            });
        }
        else {
            $(lnk).attr("title", "<%= globalStrings.GetCustomString("msg_ClickToExpand", GetLag() )  %>")
                .text("<%= CurrentPageData.GetCustomString("MenuLinksMore.Show")%>");
            $('.menulinks', jqid).css('visibility', 'hidden');
            $(jqid).hide('slow', function () {
                $('.menulinks', jqid).css('visiblity', '');
            });
        }
    }

    var like;
    function setLikes(data) {
        like = data.likes;
        try {
            console.log('Got results', data.id, like);
        }
        catch (e) { };
        document.getElementById('fblikes').innerHTML = like;
    }

    var s = document.createElement('script');
    //s.src = 'https://graph.facebook.com/?id=http://www.goomena.com&callback=setLikes';
    s.src = 'https://graph.facebook.com/http://www.facebook.com/goomena?callback=setLikes';
    document.getElementsByTagName('head')[0].appendChild(s);
    $(function () {

        //var jqxhr = $.getJSON('https://api.twitter.com/1.1/users/show.json?screen_name=goomena&callback=?',
        //    function (data) {
        //        $('#followers').show().html(data.followers_count.toString().replace(/\B(?=(?:\d{3})+(?!\d))/g, ","));
        //    })
        //  .done(function () { })
        //  .fail(function (o, err, reason) { })
        //  .always(function () { });

        //$.getJSON('http://api.twitter.com/1/users/show.json?screen_name=goomena&callback=?', function (data) {
        //    alert(data)
        //        });

    });


    Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);
    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
    function BeginRequestHandler(sender, args) {
        ShowLoading();
    }
    function EndRequestHandler(sender, args) {
        try {
            HideLoading();
        }
        catch (e) { }
    }

</script>



<uc1:TraceSrc ID="trc" runat="server" />
<asp:HiddenField ID="hdfTimeOffset" runat="server" />
<asp:HiddenField ID="hdfIsMobile" runat="server" />
<script type="text/javascript">
    $(function () {
        var d = new Date()
        var n = d.getTimezoneOffset();
        $('#<%= hdfTimeOffset.ClientID %>').val(n);
        $('#<%= hdfIsMobile.ClientID%>').val(isMobileDevice());

        if ('<%= If(Session("IsMobileAccess") IsNot Nothing, Session("IsMobileAccess").ToString().ToLower(), "")%>' == '') {
            jsonConnect.hdfTimeOffsetID = '<%= hdfTimeOffset.ClientID %>';
            jsonConnect.SetClientDataUrl = '<%= ResolveClientUrl("~/jsonPub.aspx/SetClientData")%>';
            jsonConnect.setClientData();
        }

    });
</script>


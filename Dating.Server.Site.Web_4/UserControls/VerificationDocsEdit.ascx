﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="VerificationDocsEdit.ascx.vb" Inherits="Dating.Server.Site.Web.VerificationDocsEdit" %>
<%@ Register Assembly="DevExpress.Web.v13.1, Version=13.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxUploadControl" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v13.1, Version=13.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>

<div class="verification_controls_container">
    <asp:Literal ID="msg_HasNoPhotosText" runat="server" />
    <div class="verification_documents_main_controls_wrapper">
        <div class="file_uploader_title">
            <asp:Literal ID="msg_UploadNewPhoto" runat="server" />
        </div>
        <div id="fileUploader_container">
            <div id="BrowserVisible">
                <input type="text" id="fileTextField" readonly="readonly" />
                <label class="file_selection_button">
                    <asp:Label ID="BrowseButtonText" runat="server" Text="Select Document"></asp:Label><span><asp:FileUpload ID="fileUploader" runat="server" CssClass="fileUploader" onchange="document.getElementById('fileTextField').value = document.getElementsByClassName('fileUploader')[0].value;" /></span>
                </label>
            </div>
        </div>
        <div class="max_file_size_container">
            <dx:ASPxLabel ID="lblMaxFileSize" runat="server" EncodeHtml="false"></dx:ASPxLabel>
        </div>
        <div class="divUploadStatus_container">
            <div id="divUploadStatus" runat="server" class="divUploadStatus" visible="false">
                <asp:Image ID="imgResult" runat="server" CssClass="imgResult" />
                <asp:Literal ID="lblUploadResult" runat="server" />
            </div>
        </div>
        <div class="radiobuttons_container">
            <asp:Label ID="lblPhotoLevel" runat="server" Text="Select document type:" /><asp:Image
                ID="imgPhotoLevelInfo" runat="server" ImageUrl="~/Images/icon_tip.png" Visible="False" />
            <ul class="radiobuttons">
                <li>
                    <asp:RadioButton ID="DocumentIDRadioButton" runat="server" GroupName="documentsGroup" Checked="True" />
                    <label for="<%=DocumentIDRadioButton.ClientID%>"><span></span></label>
                    <asp:Label ID="DocumentIDLabel" runat="server" Text="ID" CssClass="radioButtonText"></asp:Label></li>

                <li>
                    <asp:RadioButton ID="DocumentPassportRadioButton" runat="server" GroupName="documentsGroup" />
                    <label for="<%=DocumentPassportRadioButton.ClientID%>"><span></span></label>
                    <asp:Label ID="DocumentPassportLabel" runat="server" Text="Passport" CssClass="radioButtonText"></asp:Label></li>

                <li>
                    <asp:RadioButton ID="DocumentPhoneBillRadioButton" runat="server" GroupName="documentsGroup" />
                    <label for="<%=DocumentPhoneBillRadioButton.ClientID%>"><span></span></label>
                    <asp:Label ID="DocumentPhoneBillLabel" runat="server" Text="Phone Bill" CssClass="radioButtonText"></asp:Label></li>

                <li>
                    <asp:RadioButton ID="DocumentOtherRadioButton" runat="server" GroupName="documentsGroup" />
                    <label for="<%=DocumentOtherRadioButton.ClientID%>"><span></span></label>
                    <asp:Label ID="DocumentOtherLabel" runat="server" Text="Other" CssClass="radioButtonText"></asp:Label></li>
            </ul>
        </div>
        <!--
        -->
        <div class="description_container">
            <asp:Label ID="msg_Description" runat="server" Text="Description (optional):" />
            <asp:TextBox ID="txtDescription" runat="server" TextMode="MultiLine"></asp:TextBox>
        </div>
        <div class="clear"></div>
        <asp:LinkButton ID="btnUpload" runat="server" Text="" CssClass="btnUploadDoc" OnClientClick="ShowLoading();">
            <asp:Literal ID="btnUploadText" runat="server" Text="" />
        </asp:LinkButton>
    </div>
</div>

<div class="documents_table_title_container">
    <asp:Literal ID="msg_PubPhotos" runat="server" />
</div>
<div class="documents_table_container">
    <dx:ASPxCallbackPanel ID="cbpnlPhotos" runat="server" ShowLoadingPanel="False"
        ShowLoadingPanelImage="False" ClientInstanceName="cbpnlPhotos">
        <ClientSideEvents EndCallback="function(s, e) {
	LoadingPanel.Hide();
}"
            CallbackError="function(s, e) {
	LoadingPanel.Hide();
}" />
        <ClientSideEvents EndCallback="function(s, e) {
	LoadingPanel.Hide();
}"
            CallbackError="function(s, e) {
	LoadingPanel.Hide();
}"></ClientSideEvents>
        <PanelCollection>
            <dx:PanelContent runat="server" SupportsDisabledAttribute="True">
                <dx:ASPxGridView ID="gvDocs" runat="server" DataSourceID="odsDocs"
                    AutoGenerateColumns="False" KeyFieldName="CustomerVerificationDocsID">
                    <Columns>
                        <dx:GridViewDataTextColumn FieldName="CustomerVerificationDocsID"
                            ReadOnly="True" ShowInCustomizationForm="True" Visible="False" VisibleIndex="0">
                            <EditFormSettings Visible="False" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="CustomerID"
                            ShowInCustomizationForm="True" Visible="False" VisibleIndex="1">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataDateColumn FieldName="DateTimeToUploading"
                            ShowInCustomizationForm="True" VisibleIndex="2" Caption="Uploaded On">
                            <PropertiesDateEdit DisplayFormatInEditMode="True"
                                DisplayFormatString="dd/MM/yyyy HH:mm" EditFormat="Custom"
                                EditFormatString="dd/MM/yyyy HH:mm">
                            </PropertiesDateEdit>
                        </dx:GridViewDataDateColumn>
                        <dx:GridViewDataTextColumn FieldName="FileName" ShowInCustomizationForm="True"
                            Visible="False" VisibleIndex="4">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="DocumentType"
                            ShowInCustomizationForm="True" VisibleIndex="5">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="Description"
                            ShowInCustomizationForm="True" VisibleIndex="6">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="OriginalName"
                            ShowInCustomizationForm="True" VisibleIndex="3">
                        </dx:GridViewDataTextColumn>
                    </Columns>
                    <Styles>
                        <Row BackColor="#f8f8f8"></Row>
                        <AlternatingRow BackColor="#ffffff" Enabled="True">
                        </AlternatingRow>
                        <Cell>
                            <Paddings PaddingBottom="5px" PaddingLeft="5px" PaddingTop="5px" />
                        </Cell>
                    </Styles>
                </dx:ASPxGridView>
                <asp:ObjectDataSource ID="odsDocs" runat="server"
                    TypeName="Dating.Server.Site.Web.VerificationDocsEdit"
                    OldValuesParameterFormatString="original_{0}"
                    SelectMethod="GetVerificationDocsForCustomer">
                    <SelectParameters>
                        <asp:Parameter Name="CustomerID" Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <div class="clear"></div>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</div>


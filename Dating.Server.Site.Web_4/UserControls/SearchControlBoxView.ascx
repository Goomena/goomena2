﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="SearchControlBoxView.ascx.vb" 
    Inherits="Dating.Server.Site.Web.SearchControlBoxView" %>



<div id="<%= Mybase.UsersListView.toString() %>">
    <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">

        <asp:View ID="View1" runat="server">
        </asp:View>
        <asp:View ID="vwNoPhoto" runat="server" EnableViewState="false">
            <asp:FormView ID="fvNoPhoto" runat="server">
                <ItemTemplate>
                    <div id="ph-edit" class="no-photo">
                        <div class="items_none items_hard">
                            <div class="items_none_wrap">
                                <div class="items_none_text">
                                    <%= GetKeys().HasNoPhotosText %>
                                </div>
                                <div class="search_members_button_right">
                                    <asp:HyperLink ID="HyperLink1" runat="server" CssClass="btn lighter" NavigateUrl="~/Members/Photos.aspx" onclick="ShowLoading();"><%# Eval("AddPhotosText")%><i class="icon-chevron-right"></i></asp:HyperLink>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>
                </ItemTemplate>
            </asp:FormView>
        </asp:View>


        <asp:View ID="vwNoOffers" runat="server">
            <asp:FormView ID="fvNoOffers" runat="server" EnableViewState="false">
                <ItemTemplate>
                    <div class="items_none">
                        <div id="offers_icon" class="middle_icon"></div>
                        <div class="items_none_text">
                            <%# Eval("NoOfferText")%>
                            <div class="clear"></div>
                            <div class="search_members_button">
                                <asp:HyperLink ID="lnk4" runat="server" NavigateUrl="~/Members/Search.aspx"><%= GetKeys().SearchOurMembersText %><i class="icon-chevron-right"></i></asp:HyperLink>
                            </div>
                        </div>
                    </div>
                </ItemTemplate>
            </asp:FormView>
        </asp:View>


        <asp:View ID="vwSearchingOffers" runat="server">
            <div id="search-results" class="BoxView">
                <asp:Repeater ID="rptSearch" runat="server">
                    <ItemTemplate>
                        <div class="s_item <%# Eval("AddItemCssClass")%><%# If(Eval("IsOdd"), " odd", "")%> " login="<%# ReturnAlphaCharacters(Eval("OtherMemberLoginName"))%>">
                             <span class="Vip" runat="server" visible='<%# Eval("OtherMemberIsVip") %>'></span>
                            <div class="TopBox">
                                <svg class="bgBigBluredImage" viewBox="0 0 342 225" width="342" height="225" style="position: absolute; left: 0; top: 0; opacity: 0.5; border-top-right-radius: 3px; border-top-left-radius: 3px;">
                                    <defs>
                                        <filter id="blur<%# ReturnAlphaCharacters(Eval("OtherMemberLoginName"))%>">
                                            <feGaussianBlur in="SourceGraphic" stdDeviation="4" />
                                        </filter>
                                    </defs>
                                    <image filter="url(#blur<%# ReturnAlphaCharacters(Eval("OtherMemberLoginName"))%>)" xlink:href="<%# Eval("OtherMemberImageUrl") %>" x="-13px" y="-8px" width="367" height="248"
                                        preserveAspectRatio="xMinYMin slice" />
                                </svg>
                                <div class="BigBoxContainer">

                                    <a class="lfloat bigSquareImage" href="<%# Eval("OtherMemberProfileViewUrl")%>" style="<%# GetimageBackgroundStyle(Eval("OtherMemberImageUrl")) %>"></a>
                                    <div class="rfloat smallRightArea">
                                        <%--<div class="smallSquareImage" runat="server" visible=<%# Eval("PhotosProperties(0).HasPhoto")%> >--%>
                                        <%# GetPhoto2(Eval("PhotosProperties(0).HasPhoto"),Eval("PhotosProperties(0).ImageUrl"), Eval("PhotosProperties(0).ImageUrlOnClick"), Eval("PhotosProperties(0).ImageThumbUrl"))%>
                                        <%--</div>--%>
                                        <a class="ImageCounterConatiner" href="<%# Eval("OtherMemberProfileViewUrl")%>">
                                            <div class="lblCounter">
                                                <%# Eval("PhotosCountString")%>
                                            </div>
                                        </a>
                                        <div class="OnlineRecentlyIcon" runat="server" visible='<%# (Not Eval("OtherMemberIsOnline") andalso Eval("OtherMemberIsOnlineRecently"))%>'><span class="lblRecentlyOnline"><%# Eval("OtherMemberIsOnlineText")%></span></div>
                                    </div>
                                    <div class="clear"></div>
                                    <div class="OnlineIcon" runat="server" visible='<%# Eval("OtherMemberIsOnline")%>'></div>
                                   
                                </div>

                            </div>
                            <div class="ProfileBottomBox">
                                <div class="lfloat infoBox">
                                    <div class="info">
                                        <h2>
                                            <asp:HyperLink ID="HyperLink2" runat="server" CssClass="login-name" NavigateUrl='<%# Eval("OtherMemberProfileViewUrl") %>' Text='<%# Eval("OtherMemberLoginName")%>' />

                                            <span class="clear"></span>
                                        </h2>
                                        <div class="stats" style="position: relative;">
                                            <%# MyBase.WriteSearch_MemberInfo(Container.DataItem)%>
                                        </div>
                                    </div>
                                    <div class="info_icons">
                                        <div class="InnerCenterbox">
                                            <div class="lfloat status_info info-item" runat="server" visible='<%# if(not string.isnullOrEmpty(Eval("OtherMemberRelationshipTooltip")),true,false) %>'>
                                                <img alt="" src="//cdn.goomena.com/Images/spacer10.png" class="tt_enabled <%# Eval("OtherMemberRelationshipClass")%>">
                                                <div class="tooltip tooltip_status_info" style="display: none;">
                                                    <div class="tooltip_text right-arrow"><%# Eval("OtherMemberRelationshipTooltip")%></div>
                                                </div>
                                            </div>
                                            <div class="lfloat zodiac_info">
                                                <img alt="" src="//cdn.goomena.com/Images/spacer10.png" class="tt_enabled <%# Eval("ZodiacName")%>">
                                                <div class="tooltip tooltip_zodiac" style="display: none;">
                                                    <div class="tooltip_text right-arrow"><%# Eval("ZodiacString")%></div>
                                                </div>
                                            </div>


                                            <div class="lfloat drinking info-item" runat="server" visible='<%# If(Eval("OtherMemberIsMale")=True,if(not string.isnullOrEmpty(Eval("OtherMemberDrinkingText")),true,false),False) %>'>
                                                <img class="tt_enabled  <%# Eval("OtherMemberDrinkingClass")%>" src="//cdn.goomena.com/Images/spacer10.png" alt="">
                                                <div class="tooltip tooltip_drinking" style="display: none;">
                                                    <div class="tooltip_text right-arrow"><%# Eval("OtherMemberDrinkingText")%></div>
                                                </div>
                                            </div>
                                            <div class="lfloat smoking info-item" runat="server" visible='<%# If(Eval("OtherMemberIsMale")=True,if(not string.isnullOrEmpty(Eval("OtherMemberSmokingText")),true,false),False) %>'>
                                                <img class="tt_enabled  <%# Eval("OtherMemberSmokingClass")%>" src="//cdn.goomena.com/Images/spacer10.png" alt="">
                                                <div class="tooltip tooltip_smoking" style="display: none;">
                                                    <div class="tooltip_text right-arrow"><%# Eval("OtherMemberSmokingText")%></div>
                                                </div>
                                            </div>
                                            <div class="lfloat breast info-item" runat="server" visible='<%# If(Eval("OtherMemberIsMale")=false,if(not string.isnullOrEmpty(Eval("OtherMemberBreastSizeTooltip")),true,false),False) %>'>
                                                <img alt="" src="//cdn.goomena.com/Images/spacer10.png" class="tt_enabled <%# Eval("OtherMemberBreastSizeClass")%>">
                                                <div class="tooltip tooltip_breast" style="display: none;">
                                                    <div class="tooltip_text right-arrow"><%# Eval("OtherMemberBreastSizeTooltip")%></div>
                                                </div>
                                            </div>
                                            <div class="lfloat hair info-item" runat="server" visible='<%# If(Eval("OtherMemberIsMale")=false,if(not string.isnullOrEmpty(Eval("OtherMemberHairTooltip")),true,false),False) %>'>
                                                <img alt="" src="//cdn.goomena.com/Images/spacer10.png" class="tt_enabled <%# Eval("OtherMemberHairClass")%>">
                                                <div class="tooltip tooltip_hair" style="display: none;">
                                                    <div class="tooltip_text right-arrow"><%# Eval("OtherMemberHairTooltip")%></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="rfloat ButtonBox">
                                    <div class="InnerBoxCenter">
                                        <div id="lnkUnWink" runat="server"
                                            class="btn lnkUnWink disabled "
                                            visible=<%# Eval("AllowUnWink") %>>
                                            <%= GetKeys().WinkText %>
                                        </div>
                                        <asp:LinkButton ID="lnkWink" runat="server"
                                            EnableViewState="true"
                                            CssClass="btn lnkWink" Style="margin: 0;"
                                            Visible='<%# Eval("AllowWink") %>' CommandArgument='<%# Eval("OtherMemberProfileID") %>' CommandName="WINK"><%= GetKeys().WinkText %></asp:LinkButton>

                                        <asp:HyperLink ID="lnkSendMessage" runat="server" EnableViewState="true" Visible='true'
                                            CssClass="btn lnkSendMessage" CommandArgument='<%# Eval("OtherMemberProfileID") %>'
                                            NavigateUrl='<%# Eval("SendMessageUrl")%>' Text=""> <%= GetKeys().SendMessageText  %></asp:HyperLink>
                                        <asp:LinkButton ID="lilnkAllowFavorite" runat="server" CssClass="btn lnkFavorite"
                                            EnableViewState="true"
                                            Visible='<%# Eval("AllowFavorite") %>' CommandArgument='<%# Eval("OtherMemberProfileID") %>'
                                            CommandName="FAVORITE"><%= GetKeys().FavoriteText%></asp:LinkButton>
                                        <asp:LinkButton ID="lilnkAllowUnFavorite" runat="server" CssClass="btn lnkUnFavorite"
                                            EnableViewState="true"
                                            Visible='<%# Eval("AllowUnfavorite") %>' CommandArgument='<%# Eval("OtherMemberProfileID") %>'
                                            CommandName="UNFAVORITE"><%= GetKeys().UnfavoriteText%></asp:LinkButton>
                                        <asp:HyperLink
                                            ID="lnkNewMakeOffer2" runat="server" NavigateUrl='<%# Eval("CreateOfferUrl") %>' Visible='<%# Eval("AllowActionsCreateOffer")%>'
                                            CssClass="btn MakeOffer"><%# Eval("ActionsMakeOfferText")%></asp:HyperLink>
                                        <div class="btn MakeOfferPending" runat="server" visible=<%# not Eval("AllowActionsCreateOffer")%>>
                                            <%# Eval("ActionsMakeOfferText")%>

                                            

                                        </div>
                                        <%--   <div class="btn-group" id="ActionsMenu" runat="server" visible='<%# If(Me.IsMale andalso Eval("AllowActionsCreateOffer") ,True,False) %>'>
                                            <a class="btn btn-small dropdown-toggle" data-toggle="dropdown" href="#">
                                                <%= GetKeys().ActionsText %>
                                                <span class="caret"></span>

                                            </a>
                                            <ul class="dropdown-menu right-menu">
                                                <li id="liAllowFavorite" runat="server" visible=<%# Eval("AllowFavorite") %>> 
                                                    <asp:LinkButton ID="lilnkAllowFavorite" runat="server" CssClass="btn lnkFavorite"
                                                        EnableViewState="true"
                                                        Visible='true' CommandArgument=<%# Eval("OtherMemberProfileID") %>
                                                        CommandName="FAVORITE"><%= GetKeys().FavoriteText%></asp:LinkButton>

                                                </li>
                                                <li id="liAllowUnFavorite" runat="server" visible=<%# Eval("AllowUnfavorite") %> >
                                                    <asp:LinkButton ID="lilnkAllowUnFavorite" runat="server" CssClass="btn lnkUnFavorite"
                                                        EnableViewState="true"
                                                        Visible='true' CommandArgument='<%# Eval("OtherMemberProfileID") %>'
                                                        CommandName="UNFAVORITE"><%= GetKeys().UnfavoriteText%></asp:LinkButton>

                                                </li>

                                                <li id="liActsMakeOffer" runat="server" visible=<%# Eval("AllowActionsCreateOffer") %> >
                                                    <asp:HyperLink
                                                        ID="lnkNewMakeOffer2" runat="server" NavigateUrl='<%# Eval("CreateOfferUrl") %>'
                                                        CssClass="btn"><span><%# Eval("ActionsMakeOfferText")%></span></asp:HyperLink></li>

                                            </ul>
                                        </div>
                                        <asp:LinkButton ID="lnkFavorite" runat="server" CssClass="btn lnkFavorite"
                                            EnableViewState="true"
                                             CommandArgument='<%# Eval("OtherMemberProfileID") %>'
                                            Visible=<%# If((not Eval("AllowActionsCreateOffer") orelse Not Me.IsMale) andalso Eval("AllowFavorite") ,True,False) %>
                                            CommandName="FAVORITE"><%= GetKeys().FavoriteText%></asp:LinkButton>
                                        <asp:LinkButton ID="lnkUnFavorite" runat="server" CssClass="btn lnkUnFavorite"
                                            EnableViewState="true"
                                                   Visible=<%# If((not Eval("AllowActionsCreateOffer") orelse Not Me.IsMale) andalso Eval("AllowUnfavorite") ,True,False) %>
                                           CommandArgument='<%# Eval("OtherMemberProfileID") %>'
                                            CommandName="UNFAVORITE"><%= GetKeys().UnfavoriteText%></asp:LinkButton>
                                    </div>--%>
                                        <div class="Clear"></div>
                                         </div>
                                    <div class="Clear"></div>
                                </div>
                            </div>
                            </div>
                    </ItemTemplate>
                </asp:Repeater>
                <div class="clear"></div>
            </div>
        </asp:View>


    </asp:MultiView>


    <script type="text/javascript">
        // <![CDATA[
        function loadFancybox() {
            $("a.fancybox").fancybox({
                'transitionIn': 'elastic',
                'transitionOut': 'elastic',
                'titlePosition': 'over',
                'overlayColor': '#000'
            });
        }

        function pageLoad(sender, args) {
            loadFancybox();
            if (typeof setPopupWindowUI !== 'undefined') setPopupWindowUI();
        }

        function closePopup(popupName) {
            try {
                top.window[popupName].Hide();
                top.window[popupName].DoHideWindow(0);
            }
            catch (e) { }
        }


        // ]]> 
    </script>


</div>












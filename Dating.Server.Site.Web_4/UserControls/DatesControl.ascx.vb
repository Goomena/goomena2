﻿Imports DevExpress.Web.ASPxPager
Imports Dating.Server.Core.DLL

Public Class DatesControl
    Inherits BaseUserControl


    Public Function GetKeys() As KeyStrings
        If MykeyStrings Is Nothing Then
            MykeyStrings = New KeyStrings
        End If
        Return MykeyStrings
    End Function

    Public Enum WinkUsersListViewEnum
        None = 0

        'LikesOffers = 12
        'NewOffers = 1
        'PendingOffers = 2
        'RejectedOffers = 3
        'AcceptedOffers = 4
        'PokesOffers = 14

        DatesOffers = 5

        'WhoViewedMeList = 6
        'WhoFavoritedMeList = 7
        'MyFavoriteList = 8
        'MyBlockedList = 9
        'MyViewedList = 10
    End Enum



    Public Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            'If (_pageData Is Nothing) Then _pageData = New clsPageData("control.DatesControl", Context)
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData("control.DatesControl", Context, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property
    Private Sub Page_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        Try
            If Me._pageData IsNot Nothing Then
                RemoveHandler Me._pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If

        Catch ex As Exception
        End Try
        Try
            MyBase.Dispose()
        Catch ex As Exception
        End Try
    End Sub

    Public Property ShowNoPhotoText As Boolean
    Public Property ShowEmptyListText As Boolean

    Public Property UsersListView As WinkUsersListViewEnum
        Get
            If (Me.ViewState("WinkUsersListViewEnum") IsNot Nothing) Then Return Me.ViewState("WinkUsersListViewEnum")
            Return WinkUsersListViewEnum.None
        End Get
        Set(value As WinkUsersListViewEnum)
            Me.ViewState("WinkUsersListViewEnum") = value
        End Set
    End Property


    Private _list As List(Of clsWinkUserListItem)
    Public Property UsersList As List(Of clsWinkUserListItem)
        Get
            If (Me._list Is Nothing) Then Me._list = New List(Of clsWinkUserListItem)
            Return Me._list
        End Get
        Set(value As List(Of clsWinkUserListItem))
            Me._list = value
        End Set
    End Property



    <PersistenceMode(PersistenceMode.InnerDefaultProperty)>
    Public ReadOnly Property Repeater As Repeater
        Get
            'If (UsersListView = WinkUsersListViewEnum.AcceptedOffers) Then
            '    Return Me.rptAccepted

            'ElseIf (UsersListView = WinkUsersListViewEnum.NewOffers) Then
            '    Return Me.rptNew

            'ElseIf (UsersListView = WinkUsersListViewEnum.LikesOffers) Then
            '    Return Me.rptNew

            'ElseIf (UsersListView = WinkUsersListViewEnum.PokesOffers) Then
            '    Return Me.rptNew

            'ElseIf (UsersListView = WinkUsersListViewEnum.PendingOffers) Then
            '    Return Me.rptPending

            'ElseIf (UsersListView = WinkUsersListViewEnum.RejectedOffers) Then
            '    Return Me.rptRejected

            'Else
            If (UsersListView = WinkUsersListViewEnum.DatesOffers) Then
                Return Me.rptDates

            Else
                Return Nothing
            End If
        End Get
    End Property


    Protected Overrides Sub OnInit(e As System.EventArgs)
        MyBase.OnInit(e)
        'ModGlobals.AddScriptResourceToHeader(Me.Page, "~/Scripts/jquery-1.7.min.js")
        'ModGlobals.AddScriptResourceToHeader(Me.Page, "~/Scripts/bootstrap-dropdown.js")
    End Sub



    Public Overrides Sub DataBind()

        If (Me.ShowNoPhotoText) Then

            Dim oNoPhoto As New clsWinkUserListItem()
          

            Dim os As New List(Of clsWinkUserListItem)
            os.Add(oNoPhoto)

            fvNoPhoto.DataSource = os
            fvNoPhoto.DataBind()

            Me.MultiView1.SetActiveView(vwNoPhoto)


        ElseIf (Me.ShowEmptyListText) Then

            Dim oNoOffer As New clsWinkUserListItem()
            If (UsersListView = WinkUsersListViewEnum.DatesOffers) Then
                oNoOffer.NoOfferText = Me.CurrentPageData.GetCustomString("AcceptedOffersListEmptyText_ND")
            End If



            Dim os As New List(Of clsWinkUserListItem)
            os.Add(oNoOffer)

            fvNoOffers.DataSource = os
            fvNoOffers.DataBind()

            Me.MultiView1.SetActiveView(vwNoOffers)

        Else

            'If (UsersListView = WinkUsersListViewEnum.AcceptedOffers) Then
            '    Me.MultiView1.SetActiveView(vwAcceptedOffers)

            'ElseIf (UsersListView = WinkUsersListViewEnum.NewOffers) Then
            '    Me.MultiView1.SetActiveView(vwNewOffers)

            'ElseIf (UsersListView = WinkUsersListViewEnum.LikesOffers) Then
            '    Me.MultiView1.SetActiveView(vwNewOffers)

            'ElseIf (UsersListView = WinkUsersListViewEnum.PokesOffers) Then
            '    Me.MultiView1.SetActiveView(vwNewOffers)

            'ElseIf (UsersListView = WinkUsersListViewEnum.PendingOffers) Then
            '    Me.MultiView1.SetActiveView(vwPendingOffers)

            'ElseIf (UsersListView = WinkUsersListViewEnum.RejectedOffers) Then
            '    Me.MultiView1.SetActiveView(vwRejectedOffers)

            'Else
            If (UsersListView = WinkUsersListViewEnum.DatesOffers) Then
                Me.MultiView1.SetActiveView(vwDatesOffers)

            Else
                Me.MultiView1.SetActiveView(View1)

            End If
        End If




        If (Me.Repeater IsNot Nothing) Then

            Try
                For Each itm As clsWinkUserListItem In Me.UsersList
                    itm.FillTextResourceProperties(MykeyStrings)
                    If (itm.ZodiacString Is Nothing AndAlso itm.OtherMemberBirthday.HasValue AndAlso itm.OtherMemberBirthday.Value > DateTime.MinValue) Then
                        Dim tmpKeystr As String = "Zodiac" & ProfileHelper.ZodiacName(itm.OtherMemberBirthday)
                        If MykeyStrings.ZodiacNames.ContainsKey(tmpKeystr & itm.LAGID) Then
                            itm.ZodiacString = MykeyStrings.ZodiacNames(tmpKeystr & itm.LAGID)
                        Else
                            itm.ZodiacString = globalStrings.GetCustomString(tmpKeystr, itm.LAGID)
                            MykeyStrings.ZodiacNames.Add(tmpKeystr & itm.LAGID, itm.ZodiacString)
                        End If
                    End If
                Next
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try

            Me.Repeater.DataSource = Me.UsersList
            Me.Repeater.DataBind()
        End If

    End Sub
    Public Property MykeyStrings As New KeyStrings
    Public Function FillKeyStrings(Optional ByVal Strings As KeyStrings = Nothing, Optional ByVal pageData As clsPageData = Nothing) As Boolean

        If pageData Is Nothing Then
            pageData = CurrentPageData
        End If
        If Strings Is Nothing Then
            Strings = MykeyStrings
        End If
        If (String.IsNullOrEmpty(Strings.SendMessageOnceText)) Then Strings.SendMessageOnceText = pageData.GetCustomString("SendMessageOnceText")
        If (String.IsNullOrEmpty(Strings.SendMessageManyText)) Then Strings.SendMessageManyText = pageData.GetCustomString("SendMessageManyText")
        If (String.IsNullOrEmpty(Strings.UnlockNotice)) Then Strings.UnlockNotice = pageData.GetCustomString("UnlockNotice")
        If (String.IsNullOrEmpty(Strings.ActionsUnlockText)) Then Strings.ActionsUnlockText = pageData.GetCustomString("ActionsUnlockText")
        If (String.IsNullOrEmpty(Strings.OfferAcceptedUnlockText)) Then Strings.OfferAcceptedUnlockText = pageData.GetCustomString("OfferAcceptedUnlockText")
        If (String.IsNullOrEmpty(Strings.OfferAcceptedHowToContinueText)) Then Strings.OfferAcceptedHowToContinueText = pageData.GetCustomString("OfferAcceptedHowToContinueText")
        If (String.IsNullOrEmpty(Strings.WillYouAcceptDateWithForAmountText)) Then Strings.WillYouAcceptDateWithForAmountText = pageData.GetCustomString("WillYouAcceptDateWithForAmountText")
        If (String.IsNullOrEmpty(Strings.YouReceivedPokeDescriptionText)) Then Strings.YouReceivedPokeDescriptionText = pageData.GetCustomString("YouReceivedPokeDescriptionText")
        If (String.IsNullOrEmpty(Strings.OfferDeleteConfirmMessage)) Then Strings.OfferDeleteConfirmMessage = pageData.GetCustomString("OfferDeleteConfirmMessage")
        If (String.IsNullOrEmpty(Strings.MilesAwayText)) Then Strings.MilesAwayText = pageData.GetCustomString("MilesAwayText")
        If (String.IsNullOrEmpty(Strings.CancelledRejectedDescriptionText)) Then Strings.CancelledRejectedDescriptionText = pageData.GetCustomString("YouCancelledWinkToText")
        If (String.IsNullOrEmpty(Strings.RejectBlockText)) Then Strings.RejectBlockText = pageData.GetCustomString("RejectBlockText")
        If (String.IsNullOrEmpty(Strings.RejectDeleteConversationText)) Then Strings.RejectDeleteConversationText = pageData.GetCustomString("RejectDeleteConversationText")
        If (String.IsNullOrEmpty(Strings.IsOnlinetext)) Then Strings.IsOnlinetext = pageData.GetCustomString("Online.Now")
        If (String.IsNullOrEmpty(Strings.IsOnlineRecentlyText)) Then Strings.IsOnlineRecentlyText = pageData.GetCustomString("Online.Recently")
        If (String.IsNullOrEmpty(Strings.HasNoPhotosText)) Then Strings.HasNoPhotosText = pageData.GetCustomString("HasNoPhotosText")
        If (String.IsNullOrEmpty(Strings.SearchOurMembersText)) Then Strings.SearchOurMembersText = pageData.GetCustomString("SearchOurMembersText")
        ' If (String.IsNullOrEmpty(Strings.YouReceivedWinkOfferText)) Then Strings.YouReceivedWinkOfferText = pageData.GetCustomString("YouReceivedWinkText")
        If (String.IsNullOrEmpty(Strings.YearsOldText)) Then Strings.YearsOldText = pageData.GetCustomString("YearsOldText")
        ' If (String.IsNullOrEmpty(Strings.WantToKnowFirstDatePriceText)) Then Strings.WantToKnowFirstDatePriceText = pageData.GetCustomString("WantToKnowFirstDatePriceText")
        ' If (String.IsNullOrEmpty(Strings.MakeOfferText)) Then Strings.MakeOfferText = pageData.GetCustomString("MakeOfferText")
        ' If (String.IsNullOrEmpty(Strings.AcceptText)) Then Strings.AcceptText = pageData.GetCustomString("AcceptText")
        ' If (String.IsNullOrEmpty(Strings.CounterText)) Then Strings.CounterText = pageData.GetCustomString("CounterText")
        ' If (String.IsNullOrEmpty(Strings.NotInterestedText)) Then Strings.NotInterestedText = pageData.GetCustomString("NotInterestedText")
        ' If (String.IsNullOrEmpty(Strings.TooFarAwayText)) Then Strings.TooFarAwayText = pageData.GetCustomString("TooFarAwayText")
        ' If (String.IsNullOrEmpty(Strings.NotEnoughInfoText)) Then Strings.NotEnoughInfoText = pageData.GetCustomString("NotEnoughInfoText")
        ' If (String.IsNullOrEmpty(Strings.DifferentExpectationsText)) Then Strings.DifferentExpectationsText = pageData.GetCustomString("DifferentExpectationsText")
        '  If (String.IsNullOrEmpty(Strings.DeleteOfferText)) Then Strings.DeleteOfferText = pageData.GetCustomString("DeleteOfferText_ND")
        '  If (String.IsNullOrEmpty(Strings.DeleteWinkText)) Then Strings.DeleteWinkText = pageData.GetCustomString("DeleteWinkText")
        If (String.IsNullOrEmpty(Strings.SendMessageText)) Then Strings.SendMessageText = pageData.GetCustomString("SendMessageText_ND")
        ' If (String.IsNullOrEmpty(Strings.WinkText)) Then Strings.WinkText = pageData.GetCustomString("WinkText")
        '   If (String.IsNullOrEmpty(Strings.UnWinkText)) Then Strings.UnWinkText = pageData.GetCustomString("UnWinkText")
        '   If (String.IsNullOrEmpty(Strings.FavoriteText)) Then Strings.FavoriteText = pageData.GetCustomString("FavoriteText")
        '   If (String.IsNullOrEmpty(Strings.UnfavoriteText)) Then Strings.UnfavoriteText = pageData.GetCustomString("UnfavoriteText")
        '  If (String.IsNullOrEmpty(Strings.YearsOldFromText)) Then Strings.YearsOldFromText = pageData.GetCustomString("YearsOldFromText")
        'If (String.IsNullOrEmpty(Strings.TryWinkText)) Then Strings.TryWinkText = pageData.GetCustomString("TryWinkText")
        ' If (String.IsNullOrEmpty(Strings.MakeNewOfferText)) Then Strings.MakeNewOfferText = pageData.GetCustomString("MakeNewOfferText")
        ''  If (String.IsNullOrEmpty(Strings.YourWinkSentText)) Then Strings.YourWinkSentText = pageData.GetCustomString("YourWinkSentText")
        '  If (String.IsNullOrEmpty(Strings.AwaitingResponseText)) Then Strings.AwaitingResponseText = pageData.GetCustomString("AwaitingResponseText")
        If (String.IsNullOrEmpty(Strings.AddPhotosText)) Then Strings.AddPhotosText = pageData.GetCustomString("AddPhotosText")
        '  If (String.IsNullOrEmpty(Strings.UnblockText)) Then Strings.UnblockText = pageData.GetCustomString("UnblockText")
        ' If (String.IsNullOrEmpty(Strings.PokeText)) Then Strings.PokeText = pageData.GetCustomString("PokeText")
        '   If (String.IsNullOrEmpty(Strings.ActionsText)) Then Strings.ActionsText = pageData.GetCustomString("ActionsText")
        ''  If (String.IsNullOrEmpty(Strings.CancelPokeText)) Then Strings.CancelPokeText = pageData.GetCustomString("CancelPokeText")
        ' If (String.IsNullOrEmpty(Strings.CancelWinkText)) Then Strings.CancelWinkText = pageData.GetCustomString("CancelWinkText_ND")
        '  If (String.IsNullOrEmpty(Strings.CancelOfferText)) Then Strings.CancelOfferText = pageData.GetCustomString("CancelOfferText_ND")
        If (String.IsNullOrEmpty(Strings.RejectText)) Then Strings.RejectText = pageData.GetCustomString("RejectText")
        '   If (String.IsNullOrEmpty(Strings.CancelledRejectedTitleText)) Then Strings.CancelledRejectedTitleText = pageData.GetCustomString("WinkCancelledText")
        '   If (String.IsNullOrEmpty(Strings.MessageSentNotice)) Then Strings.MessageSentNotice = pageData.GetCustomString("MessageSentNotice")
        If (String.IsNullOrEmpty(Strings.ConversationText)) Then Strings.ConversationText = pageData.GetCustomString("ConversationText")
        If (String.IsNullOrEmpty(Strings.HistoryText)) Then Strings.HistoryText = pageData.GetCustomString("HistoryText")
        '  If (String.IsNullOrEmpty(Strings.CommunicationStatusText)) Then Strings.CommunicationStatusText = pageData.GetCustomString("CommunicationStatusText")
        '  If (String.IsNullOrEmpty(Strings.WhatIsText)) Then Strings.WhatIsText = pageData.GetCustomString("WhatIsText")
        ' If (String.IsNullOrEmpty(Strings.OffersCommands_WhatIsPopup_HeaderText)) Then Strings.OffersCommands_WhatIsPopup_HeaderText = pageData.GetCustomString("OffersCommands_WhatIsPopup_HeaderText")
        '  If (String.IsNullOrEmpty(Strings.YourPokeSentText)) Then Strings.YourPokeSentText = pageData.GetCustomString("YourPokeSentText")
        '  If (String.IsNullOrEmpty(Strings.YouReceivedPokeText)) Then Strings.YouReceivedPokeText = pageData.GetCustomString("YouReceivedPokeText")
        '   If (String.IsNullOrEmpty(Strings.YourOfferSentText)) Then Strings.YourOfferSentText = pageData.GetCustomString("YourOfferSentText")
        '   If (String.IsNullOrEmpty(Strings.YouReceivedOfferText)) Then Strings.YouReceivedOfferText = pageData.GetCustomString("YouReceivedOfferText")
        ' If (String.IsNullOrEmpty(Strings.OtherMemberPokeAcceptedText)) Then Strings.OtherMemberPokeAcceptedText = pageData.GetCustomString("OtherMemberPokeAcceptedText")
        '  If (String.IsNullOrEmpty(Strings.CurrentMemberPokeAcceptedText)) Then Strings.CurrentMemberPokeAcceptedText = pageData.GetCustomString("CurrentMemberPokeAcceptedText")
        '  If (String.IsNullOrEmpty(Strings.OtherMemberOfferAcceptedWithAmountText)) Then Strings.OtherMemberOfferAcceptedWithAmountText = pageData.GetCustomString("OtherMemberOfferAcceptedWithAmountText")
        '  If (String.IsNullOrEmpty(Strings.CurrentMemberOfferAcceptedWithAmountText)) Then Strings.CurrentMemberOfferAcceptedWithAmountText = pageData.GetCustomString("CurrentMemberOfferAcceptedWithAmountText")
        '  If (String.IsNullOrEmpty(Strings.YouCancelledWinkToText)) Then Strings.YouCancelledWinkToText = pageData.GetCustomString("YouCancelledWinkToText")
        ' If (String.IsNullOrEmpty(Strings.WinkCancelledText)) Then Strings.WinkCancelledText = pageData.GetCustomString("WinkCancelledText")
        '   If (String.IsNullOrEmpty(Strings.OfferCancelledText)) Then Strings.OfferCancelledText = pageData.GetCustomString("OfferCancelledText")
        'If (String.IsNullOrEmpty(Strings.YouCancelledOfferToText)) Then Strings.YouCancelledOfferToText = pageData.GetCustomString("YouCancelledOfferToText")
        'If (String.IsNullOrEmpty(Strings.WinkREJECTEDText)) Then Strings.WinkREJECTEDText = pageData.GetCustomString("WinkREJECTEDText")
        'If (String.IsNullOrEmpty(Strings.YourWinkREJECTEDReasonText)) Then Strings.YourWinkREJECTEDReasonText = pageData.GetCustomString("YourWinkREJECTEDReasonText")
        'If (String.IsNullOrEmpty(Strings.PokeREJECTEDText)) Then Strings.PokeREJECTEDText = pageData.GetCustomString("PokeREJECTEDText")
        'If (String.IsNullOrEmpty(Strings.YourPokeREJECTEDReasonText)) Then Strings.YourPokeREJECTEDReasonText = pageData.GetCustomString("YourPokeREJECTEDReasonText")
        'If (String.IsNullOrEmpty(Strings.OfferREJECTEDText)) Then Strings.OfferREJECTEDText = pageData.GetCustomString("OfferREJECTEDText")
        'If (String.IsNullOrEmpty(Strings.YourOfferREJECTEDReasonText)) Then Strings.YourOfferREJECTEDReasonText = pageData.GetCustomString("YourOfferREJECTEDReasonText")

        'If (String.IsNullOrEmpty(Strings.YourWinkSentText_ND_ToFemale)) Then Strings.YourWinkSentText_ND_ToFemale = pageData.GetCustomString("YourWinkSentText_ND_ToFemale")
        'If (String.IsNullOrEmpty(Strings.YourWinkSentText_ND_ToMale)) Then Strings.YourWinkSentText_ND_ToMale = pageData.GetCustomString("YourWinkSentText_ND_ToMale")
        'If (String.IsNullOrEmpty(Strings.YouReceivedWinkText)) Then Strings.YouReceivedWinkText = pageData.GetCustomString("YouReceivedWinkText")
        'If (String.IsNullOrEmpty(Strings.YourPokeSentText_ND_ToFemale)) Then Strings.YourPokeSentText_ND_ToFemale = pageData.GetCustomString("YourPokeSentText_ND_ToFemale")
        'If (String.IsNullOrEmpty(Strings.YourPokeSentText_ND_ToMale)) Then Strings.YourPokeSentText_ND_ToMale = pageData.GetCustomString("YourPokeSentText_ND_ToMale")
        'If (String.IsNullOrEmpty(Strings.OtherMemberLikeAccepted_WithOffer_Text)) Then Strings.OtherMemberLikeAccepted_WithOffer_Text = pageData.GetCustomString("OtherMemberLikeAccepted_WithOffer_Text")
        'If (String.IsNullOrEmpty(Strings.CurrentMemberLikeAccepted_WithOffer_Text)) Then Strings.CurrentMemberLikeAccepted_WithOffer_Text = pageData.GetCustomString("CurrentMemberLikeAccepted_WithOffer_Text")
        'If (String.IsNullOrEmpty(Strings.OtherMemberLikeAccepted_WithMessage_Text)) Then Strings.OtherMemberLikeAccepted_WithMessage_Text = pageData.GetCustomString("OtherMemberLikeAccepted_WithMessage_Text")
        'If (String.IsNullOrEmpty(Strings.CurrentMemberLikeAccepted_WithMessage_Text)) Then Strings.CurrentMemberLikeAccepted_WithMessage_Text = pageData.GetCustomString("CurrentMemberLikeAccepted_WithMessage_Text")
        'If (String.IsNullOrEmpty(Strings.CurrentMemberLikeAccepted_WithPoke_Text)) Then Strings.CurrentMemberLikeAccepted_WithPoke_Text = pageData.GetCustomString("CurrentMemberLikeAccepted_WithPoke_Text")
        'If (String.IsNullOrEmpty(Strings.OtherMemberLikeAccepted_WithPoke_Text)) Then Strings.OtherMemberLikeAccepted_WithPoke_Text = pageData.GetCustomString("OtherMemberLikeAccepted_WithPoke_Text")
        'If (String.IsNullOrEmpty(Strings.OtherMemberPokeAccepted_WithOffer_Text)) Then Strings.OtherMemberPokeAccepted_WithOffer_Text = pageData.GetCustomString("OtherMemberPokeAccepted_WithOffer_Text")
        'If (String.IsNullOrEmpty(Strings.CurrentMemberPokeAccepted_WithOffer_Text)) Then Strings.CurrentMemberPokeAccepted_WithOffer_Text = pageData.GetCustomString("CurrentMemberPokeAccepted_WithOffer_Text")
        'If (String.IsNullOrEmpty(Strings.CurrentMemberPokeAccepted_WithMessage_Text)) Then Strings.CurrentMemberPokeAccepted_WithMessage_Text = pageData.GetCustomString("CurrentMemberPokeAccepted_WithMessage_Text")
        'If (String.IsNullOrEmpty(Strings.OtherMemberPokeAccepted_WithMessage_Text)) Then Strings.OtherMemberPokeAccepted_WithMessage_Text = pageData.GetCustomString("OtherMemberPokeAccepted_WithMessage_Text")

        If (String.IsNullOrEmpty(Strings.AcceptedOffersListEmptyText_ND)) Then Strings.AcceptedOffersListEmptyText_ND = pageData.GetCustomString("AcceptedOffersListEmptyText_ND")
        'If (String.IsNullOrEmpty(Strings.FEMALE_WantToKnowFirstDatePriceText)) Then Strings.FEMALE_WantToKnowFirstDatePriceText = pageData.GetCustomString("FEMALE_WantToKnowFirstDatePriceText")
        'If (String.IsNullOrEmpty(Strings.MALE_WantToKnowFirstDatePriceText)) Then Strings.MALE_WantToKnowFirstDatePriceText = pageData.GetCustomString("MALE_WantToKnowFirstDatePriceText")
    End Function
    Protected Function WriteOffer_MemberInfo(DataItem As Dating.Server.Site.Web.clsWinkUserListItem) As Object
        'Dim str As String = "<ul>" & vbCrLf & _
        '    "<li><b>" & DataItem.OtherMemberAge") & "</b> " & DataItem.YearsOldText") & "</li>" & vbCrLf & _
        '    "<li><b>" & DataItem.OtherMemberCity") & ", " & DataItem.OtherMemberRegion") & "</b></li>" & vbCrLf & _
        '    "</ul>" & vbCrLf

        'Return str


        Dim sb As New StringBuilder()
        Try
            sb.Append("<ul>")
            sb.Append("<li>")

            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberAge) Then
                sb.Append("<b>" & DataItem.OtherMemberAge & "</b>")
                sb.Append(" " & Me.MykeyStrings.YearsOldText & " ")
            End If

            sb.Append("</li>")


            sb.Append("<li>")
            sb.Append("<b>")
            sb.Append(IIf(Not String.IsNullOrEmpty(DataItem.OtherMemberCity), DataItem.OtherMemberCity, ""))
            sb.Append(",  ")
            sb.Append(IIf(Not String.IsNullOrEmpty(DataItem.OtherMemberRegion), DataItem.OtherMemberRegion, ""))
            sb.Append(",  ")
            sb.Append(IIf(Not String.IsNullOrEmpty(DataItem.OtherMemberCountry), ProfileHelper.GetCountryName(DataItem.OtherMemberCountry), ""))
            sb.Replace(",  " & ",  ", ",  ")

            'If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberCity) Then
            '    sb.Append(DataItem.OtherMemberCity)
            'End If

            'If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberCity) AndAlso Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberRegion) Then
            '    sb.Append(",  ")
            'End If

            'If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberRegion) Then
            '    sb.Append(DataItem.OtherMemberRegion)
            'End If

            'If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberRegion) AndAlso Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberCountry) Then
            '    sb.Append(",  ")
            'End If

            'If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberCountry) Then
            '    sb.Append(DataItem.OtherMemberCountry)
            'End If
            'sb.Replace(",  " & ",  ", ",  ")

            sb.Append("</b>")
            sb.Append("</li>")


            sb.Append("</ul>")

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Return sb.ToString()
    End Function


    Protected Function WriteSearch_MemberInfo(DataItem As Dating.Server.Site.Web.clsWinkUserListItem) As Object

        'Dim str As String = "<ul>" & vbCrLf & _
        '    "<li><b>" & DataItem.OtherMemberAge & "</b> " & DataItem.YearsOldText & " <b>" & Eval("OtherMemberCity & ", " & Eval("OtherMemberRegion & "</b></li>" & vbCrLf & _
        '    "<li><b>" & DataItem.OtherMemberHeight & " - " & DataItem.OtherMemberPersonType & "</b></li>" & vbCrLf & _
        '    "<li>" & DataItem.OtherMemberHair & ", " & DataItem.OtherMemberEyes & ", " & DataItem.OtherMemberEthnicity & "</li>" & vbCrLf & _
        '    "</ul>" & vbCrLf

        Dim sb As New StringBuilder()
        Try

            sb.Append("<ul>")

            sb.Append("<li>")

            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberAge) Then
                sb.Append("<b>" & DataItem.OtherMemberAge & "</b>")
                sb.Append(" " & Me.MykeyStrings.YearsOldText & " ")
            End If

            sb.Append("<b>")
            sb.Append(IIf(Not String.IsNullOrEmpty(DataItem.OtherMemberCity), DataItem.OtherMemberCity, ""))
            sb.Append(",  ")
            sb.Append(IIf(Not String.IsNullOrEmpty(DataItem.OtherMemberRegion), DataItem.OtherMemberRegion, ""))
            sb.Append(",  ")
            sb.Append(IIf(Not String.IsNullOrEmpty(DataItem.OtherMemberCountry), ProfileHelper.GetCountryName(DataItem.OtherMemberCountry), ""))
            sb.Replace(",  " & ",  ", ",  ")
            'If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberCity) Then
            '    sb.Append(DataItem.OtherMemberCity)
            'End If

            'If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberCity) AndAlso Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberRegion) Then
            '    sb.Append(",  ")
            'End If

            'If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberRegion) Then
            '    sb.Append(DataItem.OtherMemberRegion)
            'End If

            'If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberRegion) AndAlso Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberCountry) Then
            '    sb.Append(",  ")
            'End If

            'If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberCountry) Then
            '    sb.Append(DataItem.OtherMemberCountry)
            'End If
            'sb.Replace(",  " & ",  ", ",  ")

            sb.Append("</b>")
            sb.Append("</li>")


            sb.Append("<li>")
            sb.Append("<b>")

            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberHeight) Then
                sb.Append(DataItem.OtherMemberHeight)
            End If

            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberHeight) AndAlso Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberPersonType) Then
                sb.Append(" - ")
            End If

            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberPersonType) Then
                sb.Append(DataItem.OtherMemberPersonType)
            End If

            sb.Append("</b>")
            sb.Append("</li>")


            sb.Append("<li>")

            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberHair) Then
                sb.Append(DataItem.OtherMemberHair)
            End If

            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberEyes) Then
                If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberHair) Then sb.Append(", ")
                sb.Append(DataItem.OtherMemberEyes)
            End If

            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberEthnicity) Then
                If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberEyes) OrElse Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberHair) Then sb.Append(", ")
                sb.Append(DataItem.OtherMemberEthnicity)
            End If

            sb.Append("</li>")
            sb.Append("</ul>")

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Return sb.ToString()
    End Function


    Protected Sub rptDates_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptDates.ItemDataBound
        Try

            Dim lnkWhatIsUNL As LinkButton = e.Item.FindControl("lnkWhatIsUNL")
            If (lnkWhatIsUNL IsNot Nothing) Then
                lnkWhatIsUNL.ToolTip = Me.CurrentPageData.GetCustomString("WhatIsUnlimitedText")
                lnkWhatIsUNL.OnClientClick = WhatIsIt.OnMoreInfoClickFunc(lnkWhatIsUNL.OnClientClick,
                                             ResolveUrl("~/Members/InfoWin.aspx?info=datesunlimited"),
                                             Me.CurrentPageData.GetCustomString("WhatIsUnlimitedText"))
            End If


            Dim lnkWhatIsLTD As LinkButton = e.Item.FindControl("lnkWhatIsLTD")
            If (lnkWhatIsLTD IsNot Nothing) Then
                lnkWhatIsLTD.ToolTip = Me.CurrentPageData.GetCustomString("WhatIsLimitedText")
                lnkWhatIsLTD.OnClientClick = WhatIsIt.OnMoreInfoClickFunc(lnkWhatIsLTD.OnClientClick,
                                             ResolveUrl("~/Members/InfoWin.aspx?info=dateslimited"),
                                             Me.CurrentPageData.GetCustomString("WhatIsLimitedText"))
            End If

            Dim lnkDelConv As LinkButton = e.Item.FindControl("lnkDelConv")
            If (lnkDelConv IsNot Nothing) Then
                Dim wrd As String = AppUtils.JS_PrepareAlertString(Me.CurrentPageData.GetCustomString("Warning_RejectDeleteConversationText"))
                wrd = BasePage.ReplaceCommonTookens(wrd, DirectCast(e.Item.DataItem, Dating.Server.Site.Web.clsWinkUserListItem).OtherMemberLoginName)
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "key" & lnkDelConv.ClientID, "key" & lnkDelConv.ClientID & "='" & wrd & "';" & vbCrLf, True)
                lnkDelConv.OnClientClick = lnkDelConv.OnClientClick.Replace("{0}", "key" & lnkDelConv.ClientID)
            End If

        Catch ex As Exception

        End Try
    End Sub
End Class
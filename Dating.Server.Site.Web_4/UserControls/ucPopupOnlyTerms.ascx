﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucPopupOnlyTerms.ascx.vb" 
    Inherits="Dating.Server.Site.Web.ucPopupOnlyTerms" %>

 <script type="text/javascript">
     function EndRequestHandler1(sender, args) {
         try {
             LoadingPanel.Hide();
         }
         catch (e) { }
         try {
             setActions();
             //m__setPopupClass();
         }
         catch (e) { }
     }

     var popupOnlyTermsLoaded = false;
     function PopupOnlyTerms_Init(s, e) {
         popupOnlyTermsLoaded = true;
     }
     function openPopupOnlyTermsDelayed(opts) {
         while (!popupOnlyTermsLoaded) {
             setTimeout(function () { openPopupOnlyTermsDelayed(opts) }, 1400);
             return;
         }
         openPopupOnlyTerms(opts);
     }


     function openPopupOnlyTerms(opts) {
         var scrollTo = '';
         if (opts != null) {
             if (!stringIsEmpty(opts["scrollTo"])) scrollTo = opts["scrollTo"];
         }
         var contentUrl = '<%= ResolveUrl("~/Members/PopupOnlyTerms.aspx")%>', headerText = null;
         if (!stringIsEmpty(scrollTo)) contentUrl = contentUrl + (contentUrl.indexOf('?') > -1 ? '&' : '?') + 'scrollTo=' + scrollTo;
         var popup = window["popupOnlyTerms"]
         popup.SetContentUrl('<%= ResolveUrl("~/loading.htm")%>');
         popup.SetContentUrl(contentUrl);
       
         //if (headerText != null) popup.SetHeaderText(headerText);
         var hdrId = popup.name + '_PW-1';
         var headerTop = hdrId + ' .dxpc-mainDiv.dxpc-shadow'
         $('#' + headerTop + ' .dxpc-header').css({ 'height': '0', 'width': '100%', 'position': 'absolute' });
         $('#' + headerTop + ' .dxpc-headerContent').css({ 'display': 'none' });
         $('#' + headerTop + ' .dxpc-closeBtn').css({ 'background-color': 'transparent', 'position': 'absolute', 'top': '10px', 'right': '10px' });
      
         popup.Show();
         popup.SetSize(746, 454);


         popup.UpdatePosition();
         popup.Closing.AddHandler(__Closing_Remove);
         popup.CloseUp.AddHandler(__CloseUp_Remove);


         function __Closing_Remove(s, e) {
             try {
                 if (__RefreshPage == true) {
                     <%--   eval($('#<%= lnkRefresh.ClientID %>').attr("href").replace("javascript:",""));--%>
                     }
                     s.SetContentUrl('about:blank');
                     s.Closing.RemoveHandler(__Closing_Remove);
                 }
                 catch (e) { }
             }
             function __CloseUp_Remove(s, e) {
                 try {
                     s.CloseUp.RemoveHandler(__CloseUp_Remove);
                 }
                 catch (e) { }
             }
                  }

         var __RefreshPage = false;
         function SetRefreshPage() {
             __RefreshPage = true;
         }
         function setActions() {
             var wdt = 729, hgt = 452;
             __RefreshPage = false;

         }

         $(function () { setActions(); });
         Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler1);




    </script>

<%--<asp:UpdatePanel ID="UpdatePanel1" runat="server">--%>
     <dx:ASPxPopupControl runat="server"
        Height="609px"
       
        ID="popupOnlyTerms"
        ClientInstanceName="popupOnlyTerms" 
        ClientIDMode="AutoID" 
        PopupVerticalAlign="WindowCenter" 
        PopupHorizontalAlign="WindowCenter" 
        Modal="True" 
        
         Action="CloseButton" Width="672px" ShowSizeGrip="False"  DragElement="Window" PopupAction="None" RenderMode="Lightweight" CloseAction="CloseButton">
    <ContentStyle HorizontalAlign="Left" VerticalAlign="Middle">
        <Paddings Padding="0px" />
        <Paddings Padding="0px" ></Paddings>
    </ContentStyle>
   <%--      <ClientSideEvents Closing="function(s, e) {
	performRequest_PopUpGiftHasBeenSeen();
}" />--%>
    <CloseButtonStyle>
        <HoverStyle>
            <BackgroundImage ImageUrl="~/Images/spacer10.png" Repeat="NoRepeat" HorizontalPosition="center"
                VerticalPosition="center"></BackgroundImage>
        </HoverStyle>
        <BackgroundImage ImageUrl="~/Images/spacer10.png" Repeat="NoRepeat" HorizontalPosition="center"
            VerticalPosition="center"></BackgroundImage>
        <BorderLeft BorderColor="White" BorderStyle="None" BorderWidth="0px" />
    </CloseButtonStyle>
    <CloseButtonImage Url="~/Images2/mbr2/close-button201411217dark.png" Height="30px" Width="30px">
    </CloseButtonImage>
        <SizeGripImage Url="~/Images/resize.png">
        </SizeGripImage>
    <HeaderStyle>
        <Paddings Padding="0px" PaddingLeft="0px" />
        <Paddings PaddingTop="0px" PaddingBottom="0px"></Paddings>
        <BorderTop BorderColor="White" BorderStyle="None" BorderWidth="0px" />
        <BorderRight BorderColor="White" BorderStyle="None" BorderWidth="0px" />
        <BorderBottom BorderColor="White" BorderStyle="None" BorderWidth="0px" />
            </HeaderStyle>
<ModalBackgroundStyle BackColor="Black" Opacity="70"></ModalBackgroundStyle>
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server" SupportsDisabledAttribute="True" Height="452px" Width="729px">
            </dx:PopupControlContentControl>
        </ContentCollection>
          <ClientSideEvents Init="PopupOnlyTerms_Init" />
    </dx:ASPxPopupControl>
    
<%--</asp:UpdatePanel>--%>








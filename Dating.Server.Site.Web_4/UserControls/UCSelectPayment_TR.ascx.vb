﻿Imports Dating.Server.Datasets.DLL
'Imports Sites.SharedSiteCode
'Imports SpiceLogicPayPalStandard
Imports Library.Public
Imports Dating.Server.Core.DLL

Public Class UCSelectPayment_TR
    Inherits BaseUserControl

    Protected Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            ' If (_pageData Is Nothing) Then _pageData = New clsPageData("control.UCSelectPayment", Context)
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData("control.UCSelectPayment", Context, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property

    Private Sub Page_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        Try
            If Me._pageData IsNot Nothing Then
                RemoveHandler Me._pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If

        Catch ex As Exception
        End Try

        Try
            MyBase.Dispose()
        Catch ex As Exception
        End Try
    End Sub

    Dim eus_PriceRec As EUS_Price
    Dim amount As String = ""
    Dim type As String = ""
    Dim g_credits As String = ""
    Dim choise As String = ""

    Private Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Try

            If (Me.Visible) Then
                calculateTransactionData()

                Dim promoCode As String = txPromoCode.Text
                If Session("PromoCode") Is Nothing Or Session("PromoCode") <> promoCode Or Session("PromoCode") = "" Then
                    Session("PromoCode") = promoCode
                End If

                Dim product As String = g_credits & type.ToLower()
                amount = "1.00"


                Try
                    If (Me.SessionVariables.MemberData.Country = "TR") Then

                        Dim url = ResolveUrl("~/members/payment.aspx?method=paymentWall&price=" & amount & "&product=" & product)
                        iframePaymentPW.Attributes.Add("src", url)

                    Else
                        Dim url = ResolveUrl("~/members/payment.aspx?method=paymentWall&price=" & amount & "&product=" & product)
                        iframePaymentPW.Attributes.Add("src", url)
                        'Response.Redirect(url)
                        'HttpContext.Current.ApplicationInstance.CompleteRequest()
                    End If

                Catch ex As System.Threading.ThreadAbortException
                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "Page_Load")
                End Try

                Try

                    Dim type As String = "CC"
                    Dim url As String = "https://www.goomena.com/Members/payment.aspx?method=epoch&price=" & amount & "&product=" & product
                    If (Not String.IsNullOrEmpty(ConfigurationManager.AppSettings("PaymentSendPage"))) Then
                        url = ConfigurationManager.AppSettings("PaymentSendPage") & "?method=epoch&price=" & amount & "&product=" & product
                    End If


                    If (Not String.IsNullOrEmpty(type)) Then
                        If (type.ToLower() = "pp") Then
                            ' epoch with paypal - europe

                            url = "https://www.goomena.com/Members/payment.aspx?method=epoch&type=" & type & "&price=" & amount & "&product=" & product
                            If (Not String.IsNullOrEmpty(ConfigurationManager.AppSettings("PaymentSendPage"))) Then
                                url = ConfigurationManager.AppSettings("PaymentSendPage") & "?method=epoch&type=" & type & "&price=" & amount & "&product=" & product
                            End If


                        Else
                            If (type.ToLower() = "paypal") Then
                                url = "https://www.goomena.com/Members/payment.aspx?method=paymentWall&type=paypal&price=" & amount & "&product=" & product
                                If (Not String.IsNullOrEmpty(ConfigurationManager.AppSettings("PaymentSendPage"))) Then
                                    url = ConfigurationManager.AppSettings("PaymentSendPage") & "?method=paymentWall&type=paypal&price=" & amount & "&product=" & product
                                End If

                            ElseIf (type.ToLower() = "clickandbuy") Then
                                url = "https://www.goomena.com/Members/payment.aspx?method=paymentWall&type=clickandbuy&price=" & amount & "&product=" & product
                                If (Not String.IsNullOrEmpty(ConfigurationManager.AppSettings("PaymentSendPage"))) Then
                                    url = ConfigurationManager.AppSettings("PaymentSendPage") & "?method=paymentWall&type=clickandbuy&price=" & amount & "&product=" & product
                                End If
                            ElseIf (type.ToLower() = "pv" OrElse type.ToLower() = "cc") Then
                                url = "https://www.goomena.com/Members/payment.aspx?method=epoch&type=" & type & "&price=" & amount & "&product=" & product
                                If (Not String.IsNullOrEmpty(ConfigurationManager.AppSettings("PaymentSendPage"))) Then
                                    url = ConfigurationManager.AppSettings("PaymentSendPage") & "?method=epoch&type=" & type & "&price=" & amount & "&product=" & product
                                End If
                            End If
                        End If

                    End If

                    iframePaymentEpoch.Attributes.Add("src", url)

                Catch ex As System.Threading.ThreadAbortException
                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "Page_Load")
                End Try

                Try
                    If (Not Me.IsPostBack) Then
                        LoadLAG()
                        'LoadView()
                    End If


                    If (Me.MasterProfileId = 308 OrElse _
                        Me.MasterProfileId = 295 OrElse _
                        Me.MasterProfileId = 269 OrElse _
                        Me.MasterProfileId = 272 OrElse _
                        Me.MasterProfileId = 371 OrElse _
                        Me.MasterProfileId = 34224) Then
                        '34224 metu - TR

                        trNoPayment.Visible = True
                        pntTest.Visible = True
                        btnPayTestUser.Text = btnPayTestUser.Text.Replace("#USER#", Me.GetCurrentProfile.LoginName)
                    End If

                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "Page_Load")
                End Try

                Try
                    calculateTransactionData()
                    BuyNowButton1.ItemName = "Goomena.com " & g_credits & " Credits"
                    BuyNowButton1.Amount = amount
                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "BuyNow")
                End Try

            End If

        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub



    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
        'LoadView()
    End Sub





    Public Sub LoadLAG() 'ByVal LAGID As String, ByVal NoCache As Boolean
        Try

            'Dim cPageBasic As CMS.LAG.Site.Core.clsSiteLAG.clsPageBasicReturn
            ''Dim LagID As String = Session("LagID")
            'Dim PageName As String = "UCSelectPayment.ascx" 'AppUtils.getPageFileNameFromURL(Request.Url.ToString())
            'cPageBasic = gLAG.GetPageBasics(LAGID, PageName)

            PanelSelectPayment.HeaderText = Me.CurrentPageData.GetCustomString("PanelSelectPayment")

            'lbPaymentHeader.Text = Me.CurrentPageData.GetCustomString("lbPaymentHeader")

            lbPaypalHeadline.Text = Me.CurrentPageData.GetCustomString("lbPaypalHeadline")
            lbPaySafeHeadline.Text = Me.CurrentPageData.GetCustomString("lbPaySafeHeadline")

            lbPayByPhoneHeadline.Text = Me.CurrentPageData.GetCustomString("lbPayByPhoneHeadline")

            lbAlertPayHeadline.Text = Me.CurrentPageData.GetCustomString("lbAlertPayHeadline")
            lbCreditCardsHeadline.Text = Me.CurrentPageData.GetCustomString("lbCreditCardsHeadline")
            'AlertPayTIP.Text = Me.CurrentPageData.GetCustomString( "AlertPayTIP")

            lbAlertPayMore.Text = Me.CurrentPageData.GetCustomString("AlertPayTIP")
            lbPaypalMore.Text = Me.CurrentPageData.GetCustomString("PaypalTIP")
            lbPaysafeMore.Text = Me.CurrentPageData.GetCustomString("PaysafeTIP")
            lbPayByPhoneMore.Text = Me.CurrentPageData.GetCustomString("PayByPhoneTIP")
            lbCreditCardsMore.Text = Me.CurrentPageData.GetCustomString("CreditCardsTIP")

            lbPaymentExtraInfo.Text = Me.CurrentPageData.GetCustomString("lbPaymentExtraInfo")
            lblPaymentWithPaypal.Text = Me.CurrentPageData.GetCustomString("lblPaymentWithPaypal")

            lnkPaymentWall.Text = CurrentPageData.GetCustomString("lnkOtherMethodsHeadline")
            lbPaymentWallMore.Text = Me.CurrentPageData.GetCustomString("PaymentWallTIP")


            ASPxPopupControl2.Windows.FindByName("Win_PaypalTIP").HeaderText = Me.CurrentPageData.GetCustomString("PaypalTIP_HeaderText")
            ASPxPopupControl2.Windows.FindByName("Win_PaysafeTIP").HeaderText = Me.CurrentPageData.GetCustomString("PaysafeTIP_HeaderText")
            ASPxPopupControl2.Windows.FindByName("Win_PayByPhoneTIP").HeaderText = Me.CurrentPageData.GetCustomString("PayByPhoneTIP_HeaderText")
            ASPxPopupControl2.Windows.FindByName("Win_MoneyBookersTIP").HeaderText = Me.CurrentPageData.GetCustomString("MoneyBookersTIP_HeaderText")
            ASPxPopupControl2.Windows.FindByName("Win_PayZaTIP").HeaderText = Me.CurrentPageData.GetCustomString("PayZaTIP_HeaderText")
            ASPxPopupControl2.Windows.FindByName("Win_CreditCardsTIP").HeaderText = Me.CurrentPageData.GetCustomString("CreditCardsTIP_HeaderText")
            ASPxPopupControl2.Windows.FindByName("Win_PaymentWallTIP").HeaderText = Me.CurrentPageData.GetCustomString("PaymentWallTIP_HeaderText")


            lblOthersHeadline.Text = CurrentPageData.GetCustomString("lnkOtherMethodsHeadline")
            lbOthersTIP.Text = Me.CurrentPageData.GetCustomString("OthersTIP")
            ASPxPopupControl2.Windows.FindByName("Win_OthersTIP").HeaderText = Me.CurrentPageData.GetCustomString("OthersTIP_HeaderText")

            lblInernationalPayments.Text = Me.CurrentPageData.GetCustomString("lblInernationalPayments")
            lblOtherPaymentsTR.Text = Me.CurrentPageData.GetCustomString("lblLocalPayments")
            lbIntPaymentText.Text = Me.CurrentPageData.GetCustomString("lbIntPaymentText")
            lblTopInfo.Text = Me.CurrentPageData.GetCustomString("lblTopInfo")

            Try
                If (clsPricing.VATCountries.Contains(Session("GEO_COUNTRY_CODE"))) Then
                    TrPayPal.Visible = True
                Else
                    TrPayPal.Visible = False
                End If
                'btnPaypalPW.Visible = False
                'btnPaypalPW.Visible = False
                'Dim rnd As New ModRandom
                'Dim a As Integer = rnd.GetRandom(0, 1000)
                'If a > 500 Then
                'btnPaypalPW.Visible = True
                'Else
                'btnPayPal.Visible = True
                'End If

            Catch ex As Exception
            End Try

            calculateTransactionData()

            '    Dim description As String = ""
            'If g_credits = 999 Then
            '    description = " Lifetime"
            'ElseIf g_credits = 9999 Then
            '    description = " Unlimited"
            'Else
            '    description = g_credits & " " & type
            'End If

            ''You have selected the amount of ###CREDITSAMOUNT### credits for ###DURATION###.<br>A payment of ###PAYMENTAMOUNT### Euro  is required.
            'lbProductInfo.Text = Me.CurrentPageData.GetCustomString(lbProductInfo.ID)
            'lbProductInfo.Text = lbProductInfo.Text.Replace("###CREDITSAMOUNT###", eus_PriceRec.Credits)

            'Dim durStr As String = clsCustomer.GetDurationString(eus_PriceRec.duration, Session("LAGID"))
            'lbProductInfo.Text = lbProductInfo.Text.Replace("###DURATION###", durStr)

            'lbProductInfo.Text = lbProductInfo.Text.Replace("###PAYMENTAMOUNT###", eus_PriceRec.Amount)


            lblPaymentDescription.Text = CurrentPageData.GetCustomString("lbProductInfo2")
            lblPaymentDescription.Text = lblPaymentDescription.Text.Replace("###CREDITSAMOUNT###", eus_PriceRec.Credits)

            Dim durStr As String = clsCustomer.GetDurationString(eus_PriceRec.duration, GetLag())
            lblPaymentDescription.Text = lblPaymentDescription.Text.Replace("###DURATION###", durStr)

            lblPaymentDescription.Text = lblPaymentDescription.Text.Replace("###PAYMENTAMOUNT###", eus_PriceRec.Amount)
            lblPaymentDescription.Text = lblPaymentDescription.Text.Replace("###CURRENCY###", Me.GetCurrency())



            'If type.ToLower.Contains("days") Then
            '    If quantity = 999 Then
            '        lbProduct.Text = "Lifetime Unlimited Traffic Package"

            '    Else
            '        lbProduct.Text = description.ToLower().Replace("days", "") & " " & "Days Unlimited Traffic Package"

            '    End If

            'End If
            'If type.ToLower.Contains("giga") Then
            '    lbProduct.Text = description.Replace("gigabytes", "") & " " & "Gigabytes of Traffic Package"
            '    If type.ToLower.Contains("orongiga") Then
            '        lbProduct.Text = description.Replace("orongiga", "") & " " & "ORON Extra Gigabytes of Traffic Package"

            '    End If
            '    If type.ToLower.Contains("ulgiga") Then
            '        lbProduct.Text = description.Replace("ulgiga", "") & " " & "Uploaded.to Extra Gigabytes of Traffic Package"

            '    End If
            'End If
            'lbAmount.Text = amount & " Euro"
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Protected Sub btnPayByPhone_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPayByPhone.Click
        If Not clsCurrentContext.VerifyLogin() Then
            ' Session("CustomerID") = 13
            Response.Redirect("~/Login.aspx", True)
        Else
            calculateTransactionData()
            Dim promoCode As String = txPromoCode.Text
            If Session("PromoCode") Is Nothing Or Session("PromoCode") <> promoCode Or Session("PromoCode") = "" Then
                Session("PromoCode") = promoCode
            End If
            Dim product As String = g_credits & type.ToLower()

            Response.Redirect("payment.aspx?method=phone&price=" & amount & "&product=" & product)
        End If


    End Sub

    Protected Sub btnAlertPay_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAlertPay.Click
        If Not clsCurrentContext.VerifyLogin() Then
            ' Session("CustomerID") = 13
            Response.Redirect("~/Login.aspx", True)
        Else
            calculateTransactionData()
            Dim promoCode As String = txPromoCode.Text
            If Session("PromoCode") Is Nothing Or Session("PromoCode") <> promoCode Or Session("PromoCode") = "" Then
                Session("PromoCode") = promoCode
            End If
            Dim product As String = g_credits & type.ToLower()

            Response.Redirect("payment.aspx?method=alertpay&price=" & amount & "&product=" & product)
        End If

    End Sub

    Sub calculateTransactionData()
        If (String.IsNullOrEmpty(Request.QueryString("choice"))) Then
            Throw New ProductNotSelectedException()
        End If

        choise = Request.QueryString("choice").ToLower
        If choise.Contains("credits") Then
            type = "credits"
            g_credits = choise.Replace("credits", "")
        End If

        Dim productCode As String = clsPricing.GetProductCode(g_credits, type, Me.SessionVariables.MemberData.Country)
        'Dim productCode As String = "dd" & g_credits & type
        'Select Case (Session("GEO_COUNTRY_CODE"))
        '    Case "AL" : productCode = "dd" & g_credits & type & "LEK" '"EUR" '"LEK"
        'End Select

        eus_PriceRec = clsPricing.GetPriceForProductCode(productCode)
        amount = eus_PriceRec.Amount.ToString("0.00")

    End Sub

    Protected Sub btnPaySafe_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPaySafe.Click
        If Not clsCurrentContext.VerifyLogin() Then
            ' Session("CustomerID") = 13
            Response.Redirect("~/Login.aspx", True)
        Else
            calculateTransactionData()
            Dim promoCode As String = txPromoCode.Text
            If Session("PromoCode") Is Nothing Or Session("PromoCode") <> promoCode Or Session("PromoCode") = "" Then
                Session("PromoCode") = promoCode
            End If
            Dim product As String = g_credits & type.ToLower()

            'Response.Redirect("payment.aspx?method=paysafe&price=" & amount & "&product=" & product)
            Dim url As String = "https://www.goomena.com/Members/pwSetPayment.aspx?type=PV&amount=" & amount & "&product=" & product
            'Dim url As String = "/Members/pwSetPayment.aspx?type=PV&amount=" & amount & "&product=" & product
            If (Not String.IsNullOrEmpty(ConfigurationManager.AppSettings("PaymentRedirectPage"))) Then
                url = ConfigurationManager.AppSettings("PaymentRedirectPage") & "?type=PV&amount=" & amount & "&product=" & product
            End If
            Response.Redirect(url)

        End If

    End Sub
    Protected Sub btnPaypalPW_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPaypalPW.Click


        If Not clsCurrentContext.VerifyLogin() Then
            ' Session("CustomerID") = 13
            Response.Redirect("~/Login.aspx", True)
        Else
            calculateTransactionData()
            Dim promoCode As String = txPromoCode.Text
            If Session("PromoCode") Is Nothing Or Session("PromoCode") <> promoCode Or Session("PromoCode") = "" Then
                Session("PromoCode") = promoCode
            End If
            Dim product As String = g_credits & type.ToLower()
            amount = "1.00"
            Response.Redirect("pwSetPayment.aspx?type=paypal&amount=" & amount & "&product=" & product)
        End If

    End Sub


    Protected Sub btnCreditCards_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles btnCreditCards.Click


        If Not clsCurrentContext.VerifyLogin() Then
            ' Session("CustomerID") = 13
            Response.Redirect("~/Login.aspx", True)
        Else
            calculateTransactionData()
            Dim promoCode As String = txPromoCode.Text
            If Session("PromoCode") Is Nothing Or Session("PromoCode") <> promoCode Or Session("PromoCode") = "" Then
                Session("PromoCode") = promoCode
            End If
            Dim product As String = g_credits & type.ToLower()
            amount = "1.00"

            ''Response.Redirect("pwSetPayment.aspx?type=clickandbuy&amount=" & amount & "&product=" & product)
            'Response.Redirect("https://www.goomena.com/Members/pwSetPayment.aspx?type=CC&amount=" & amount & "&product=" & product)
            ''Response.Redirect("/Members/pwSetPayment.aspx?type=CC&amount=" & amount & "&product=" & product)

            Dim url As String = "https://www.goomena.com/Members/pwSetPayment.aspx?type=CC&amount=" & amount & "&product=" & product
            If (Not String.IsNullOrEmpty(ConfigurationManager.AppSettings("PaymentRedirectPage"))) Then
                url = ConfigurationManager.AppSettings("PaymentRedirectPage") & "?type=CC&amount=" & amount & "&product=" & product
            End If
            Response.Redirect(url)
        End If

    End Sub


    Protected Sub btnPayPalDir_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPayPalDir.Click

        If Not clsCurrentContext.VerifyLogin() Then
            ' Session("CustomerID") = 13
            Response.Redirect("~/Login.aspx", True)
        Else
            calculateTransactionData()
            Dim promoCode As String = txPromoCode.Text
            If Session("PromoCode") Is Nothing Or Session("PromoCode") <> promoCode Or Session("PromoCode") = "" Then
                Session("PromoCode") = promoCode
            End If


            Dim product As String = g_credits & type.ToLower()
            '   Dim productCode As String = "dd" & g_credits & type
            Dim ProductDescription As String = g_credits & type & " PayPal"
            ' Dim productId As Integer = GetProductId_RegNow(g_credits)

            '  Dim transId As String =
            SaveToPayTransactions(PaymentMethods.PayPal, Me.MasterProfileId, amount, ProductDescription, g_credits, Request.UserHostAddress)
            'Dim path As String = ResolveUrl("~/IPN/RegNowSetPayment.aspx?type=PP&itemName=" & productId & "&amount=" & amount & "&TransID=" & transId & "&currency=EUR")
            Dim url As String = "https://www.goomena.com/Members/pwSetPayment.aspx?type=PP&amount=" & amount & "&product=" & product
            'Dim url As String = "/Members/pwSetPayment.aspx?type=PV&amount=" & amount & "&product=" & product
            If (Not String.IsNullOrEmpty(ConfigurationManager.AppSettings("PaymentRedirectPage"))) Then
                url = ConfigurationManager.AppSettings("PaymentRedirectPage") & "?type=PP&amount=" & amount & "&product=" & product
            End If
            Response.Redirect(url)

            'Dim ProductDescription As String = "Payed with 2CO using " & PaymentMethods.PayPal.ToString() & " for " & quantity & " " & type & "."
            'path = ResolveUrl("~/IPN/2coSetPayment.aspx?type=PP&itemName=" & 1 & "&amount=" & amount & "&TransID=" & transId & "&currency=EUR")
            'Response.Redirect(path)
        End If

    End Sub


#Region "test functions"


    Protected Sub img_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) 'Handles img.Click
        Try
            calculateTransactionData()
            Dim promoCode As String = txPromoCode.Text
            If Session("PromoCode") Is Nothing Or Session("PromoCode") <> promoCode Or Session("PromoCode") = "" Then
                Session("PromoCode") = promoCode
            End If

            clsCustomer.AddTransaction("Add Credits with Epoch. Reference: 1111. ProductCode:[dd1000Credits]. ProductCode:[dd1000Credits]",
                           1.0,
                           1111,
                           "",
                           g_credits,
                           "127.0.0.1",
                           Request.Params("HTTP_USER_AGENT"),
                           Session("Referrer"),
                           Me.MasterProfileId,
                           Session("CustomReferrer"),
                           Me.GetCurrentMasterProfile().eMail,
                           PaymentMethods.PayPal,
                           promoCode,
                           1,
                           Nothing,
                          "EUR",
                           New clsDataRecordIPN())
            'Request.Params("HTTP_REFERER"),
            Response.Redirect(ResolveUrl("~/members/paymentok.aspx"))
            'If (Not String.IsNullOrEmpty(Request.QueryString("returnurl"))) Then
            '    lnkCancelCreditsCharging.NavigateUrl = Server.UrlDecode(Request.QueryString("returnurl"))
            'Else
            '    ' hacked

            'End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub



    Protected Sub btnPayTestUser_Click(sender As Object, e As EventArgs) Handles btnPayTestUser.Click

        Dim ipnData As New clsDataRecordIPN()
        ipnData.BuyerInfo = New clsDataRecordBuyerInfo With {
            .PayerID = "PayerID", _
            .FirstName = Me.GetCurrentProfile.FirstName, _
            .LastName = Me.GetCurrentProfile.LastName, _
            .PayerEmail = Me.GetCurrentProfile.eMail _
        }
        ipnData.CustomerID = Me.MasterProfileId
        ipnData.CustomReferrer = Me.GetCurrentProfile().CustomReferrer
        ipnData.custopmerIP = Session("IP")
        ipnData.PaymentDateTime = Date.UtcNow
        ipnData.PayProviderAmount = System.Convert.ToInt32(eus_PriceRec.Amount)
        ipnData.PayProviderID = 2222
        ipnData.PayProviderTransactionID = 3333
        ipnData.PayTransactionID = 4444
        ipnData.PromoCode = "PromoCode"
        'cDataRecordIPN.SaleDescription = quantity & type.ToLower()
        'cDataRecordIPN.SaleDescription = "Reseller Add Money"
        'cDataRecordIPN.SaleDescription = quantity & type
        ipnData.SaleDescription = "Add Credits with Test. Reference: 1111. ProductCode:[" & eus_PriceRec.ProductCode & "]. PaymentType:[pp]"
        ipnData.SaleQuantity = eus_PriceRec.Credits
        ipnData.SalesSiteID = ConfigurationManager.AppSettings("siteID")
        ipnData.TransactionTypeID = 1111
        ipnData.Currency = eus_PriceRec.Currency


        Dim sData As String
        sData = ipnData.PayProviderAmount & "+" & ipnData.PaymentDateTime & "+" & ipnData.PayTransactionID & "+" & ipnData.CustomerID & "+" & ipnData.SalesSiteProductID & "Extr@Ded0men@"
        '   Dim h As New Library.Public.clsHash
        Dim Code As String = Library.Public.clsHash.ComputeHash(sData, "SHA512")
        ipnData.VerifyHASH = Code

        Using srvc As New UniPAYIPN


            Dim cDataRecordIPNReturn As clsDataRecordIPNReturn = srvc.IPNEventRaised(ipnData)
            txtOutput.Text = txtOutput.Text & "ErrorCode : " & cDataRecordIPNReturn.ErrorCode & vbCrLf
            txtOutput.Text = txtOutput.Text & "HasErrors : " & cDataRecordIPNReturn.HasErrors & vbCrLf
            txtOutput.Text = txtOutput.Text & "Message : " & cDataRecordIPNReturn.Message & vbCrLf
            txtOutput.Text = txtOutput.Text & "ExtraMessage : " & cDataRecordIPNReturn.ExtraMessage & vbCrLf
        End Using
    End Sub

#End Region




    Protected Sub lnkOtherMethods_Click(sender As Object, e As EventArgs) Handles btnPaymentWall.Click, btnOther.Click
        calculateTransactionData()

        Dim promoCode As String = txPromoCode.Text
        If Session("PromoCode") Is Nothing Or Session("PromoCode") <> promoCode Or Session("PromoCode") = "" Then
            Session("PromoCode") = promoCode
        End If

        Dim product As String = g_credits & type.ToLower()
        amount = "1.00"


        Try
            If (Me.SessionVariables.MemberData.Country = "TR") Then

                Dim url = "pwSetPayment.aspx?type=paymentWall&amount=" & amount & "&product=" & product
                ' iframePayment.Attributes.Add("src", url)
                Response.Redirect(url)

            Else
                Dim url = "pwSetPayment.aspx?type=paymentWall&amount=" & amount & "&product=" & product
                'iframePayment.Attributes.Add("src", url)
                'Response.Redirect(url)
                'HttpContext.Current.ApplicationInstance.CompleteRequest()
                Response.Redirect(url)
            End If
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

        ''If Not clsCurrentContext.VerifyLogin() Then
        ''    ' Session("CustomerID") = 13
        ''    Response.Redirect("~/Login.aspx", True)
        ''Else
        ''    calculateTransactionData()
        ''    Dim promoCode As String = txPromoCode.Text
        ''    If Session("PromoCode") Is Nothing Or Session("PromoCode") <> promoCode Or Session("PromoCode") = "" Then
        ''        Session("PromoCode") = promoCode
        ''    End If
        ''    Dim product As String = g_credits & type.ToLower()
        ''    amount = "1.00"
        ''    Response.Redirect("pwSetPayment.aspx?amount=" & amount & "&product=" & product)
        ''End If


        'If Not clsCurrentContext.VerifyLogin() Then
        '    ' Session("CustomerID") = 13
        '    Response.Redirect("~/Login.aspx", True)
        'Else
        '    calculateTransactionData()
        '    Dim promoCode As String = txPromoCode.Text
        '    If Session("PromoCode") Is Nothing Or Session("PromoCode") <> promoCode Or Session("PromoCode") = "" Then
        '        Session("PromoCode") = promoCode
        '    End If
        '    Dim product As String = g_credits & type.ToLower()
        '    amount = "1.00"

        '    ''Response.Redirect("pwSetPayment.aspx?type=clickandbuy&amount=" & amount & "&product=" & product)
        '    'Response.Redirect("https://www.goomena.com/Members/pwSetPayment.aspx?type=CC&amount=" & amount & "&product=" & product)
        '    ''Response.Redirect("/Members/pwSetPayment.aspx?type=CC&amount=" & amount & "&product=" & product)

        '    Dim url As String = "https://www.goomena.com/Members/pwSetPayment.aspx?type=CC&amount=" & amount & "&product=" & product
        '    If (Not String.IsNullOrEmpty(ConfigurationManager.AppSettings("PaymentRedirectPage"))) Then
        '        url = ConfigurationManager.AppSettings("PaymentRedirectPage") & "?type=CC&amount=" & amount & "&product=" & product
        '    End If
        '    Response.Redirect(url)
        'End If

    End Sub

    Protected Sub btnCreditCardsDir_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles btnCreditCardsDir.Click

        If Not clsCurrentContext.VerifyLogin() Then
            ' Session("CustomerID") = 13
            Response.Redirect("~/Login.aspx", True)
        Else
            calculateTransactionData()
            Dim promoCode As String = txPromoCode.Text
            If Session("PromoCode") Is Nothing Or Session("PromoCode") <> promoCode Or Session("PromoCode") = "" Then
                Session("PromoCode") = promoCode
            End If

            '   Dim product As String = g_credits & type.ToLower()
            '  Dim productCode As String = "dd" & g_credits & type
            Dim ProductDescription As String = g_credits & type & " CreditCard"
            Dim productId As Integer = GetProductId_RegNow(g_credits)

            Dim transId As String = SaveToPayTransactions(PaymentMethods.CreditCard, Me.MasterProfileId, amount, ProductDescription, g_credits, Request.UserHostAddress)
            Dim path As String = ResolveUrl("~/IPN/RegNowSetPayment.aspx?type=CC&itemName=" & productId & "&amount=" & amount & "&TransID=" & transId & "&currency=EUR")
            Response.Redirect(path)

            'Dim ProductDescription As String = "Payed with 2CO using " & PaymentMethods.CreditCard.ToString() & " for " & quantity & " " & type & "."
            'path = ResolveUrl("~/IPN/2coSetPayment.aspx?type=CC&itemName=" & 1 & "&amount=" & amount & "&TransID=" & transId & "&currency=EUR")
            'Response.Redirect(path)
        End If

    End Sub



    Private Shared Function SaveToPayTransactions(
                                                 PayProviderID As Integer,
                                                 CustomerID As Integer,
                                                 Amount As Object,
                                                 ProductDescription As String,
                                                 g_credits As Integer,
                                                 UserHostAddress As String) As String

        Dim SalesSiteID As Integer = ConfigurationManager.AppSettings("siteID")
        Dim transId As String = (DateTime.UtcNow.ToFileTime() & "_" & (New Random()).Next(0, 1000)).ToString()

        Dim sql As String = <sql><![CDATA[
INSERT INTO [UNI_PayTransactions]
           ([PayProviderID]
           ,[SalesSiteID]
           ,[SalesSiteProductID]
           ,[CustomerID]
           ,[CustomerIP]
           ,[PaymentDateTime]
            ,[Currency]
            ,[Amount]
            ,UniqueID
            ,SiteProductCode
            ,SiteProductDescription)
     VALUES
           (@PayProviderID
           ,@SalesSiteID
           ,1 --SalesSiteProductID
           ,@CustomerID
           ,@CustomerIP
           ,@PaymentDateTime 
           ,@Currency
           ,@Amount
           ,@UniqueID
           ,@GoomenaCredits
           ,@ProductDescription);
SELECT SCOPE_IDENTITY();
]]></sql>
        Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


            Using cmd As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(con, sql)


                cmd.Parameters.AddWithValue("PayProviderID", PayProviderID)
                cmd.Parameters.AddWithValue("SalesSiteID", SalesSiteID)
                cmd.Parameters.AddWithValue("CustomerID", CustomerID)
                cmd.Parameters.AddWithValue("CustomerIP", UserHostAddress)
                cmd.Parameters.AddWithValue("PaymentDateTime", DateTime.UtcNow)
                cmd.Parameters.AddWithValue("Currency", "EUR")
                cmd.Parameters.AddWithValue("Amount", Amount)
                cmd.Parameters.AddWithValue("UniqueID", transId)
                cmd.Parameters.AddWithValue("ProductDescription", ProductDescription)
                cmd.Parameters.AddWithValue("GoomenaCredits", g_credits)

                DataHelpers.ExecuteScalar(cmd)
            End Using
        End Using
        Return transId
    End Function



    Public Shared Function GetProductId_RegNow(credits As Integer)
        Dim id As Integer

        Select Case credits
            Case 1000 : id = 1
            Case 3000 : id = 2
            Case 6000 : id = 3
            Case 10000 : id = 4
            Case 30000 : id = 5
            Case 50000 : id = 6
        End Select

        Return id
    End Function


    'Protected Sub lnkOtherMethods_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs)

    'End Sub

End Class
﻿<%@ Page Title="" Language="vb" AutoEventWireup="false"
 MasterPageFile="~/Members/Members2Inner.Master"
 CodeBehind="ConfirmPass.aspx.vb" Inherits="Dating.Server.Site.Web.ConfirmPass" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
    function notifyParentSuccess() {
        top.isOkPasswordConfirm = true;
        closePopup('<%= Request.QueryString("popup") %>')
    }
</script>
<style type="text/css">
    body{background-image:none;}
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <div id="confirm-pass">
    <asp:Label ID="lblTitle" runat="server" Text=""><h2>Provide your pasword to confirm action:</h2></asp:Label>
    <asp:Panel ID="pnlPasswordVerifyFailed" runat="server" CssClass="alert alert-danger"
        Visible="False" ViewStateMode="Disabled"><asp:Label ID="lblPasswordVerifyFailed" runat="server" Text=""/>
        </asp:Panel>

<asp:Panel ID="pnlSubmitAction" runat="server" DefaultButton="btnCheckPassword">
    <table class="center-table">
        <tr>
            <td class="lbl-right"><asp:Label ID="lblYourPassword" runat="server" Text="">Your Password:</asp:Label></td> 
            <td>&nbsp;</td>
            <td><dx:aspxtextbox id="txtPassword" runat="server" password="True" width="230px"
                    font-size="14px" height="18px" clientinstancename="txtPassword">
    <validationsettings errordisplaymode="ImageWithTooltip" errortext="" errortextposition="Left"><requiredfield errortext="" isrequired="True" /></validationsettings></dx:aspxtextbox>
            </td>
        </tr>
    </table>

    <div class="form-actions">
        <table class="center-table">
            <tr>
                <td valign="top"><dx:ASPxButton ID="btnCheckPassword" runat="server" CausesValidation="True"
                            CssClass="btn lnk391-blue" Text="Verify Password" 
                            Native="True"  EncodeHtml="False" />
                </td>
            </tr>
        </table>
    </div>
</asp:Panel>
    </div>

 <script type="text/javascript">
     (function () {
         var itms = $("input,a", "#confirm-pass");
         for (var c = 0; c < itms.length; c++) {
             if ($(itms[c]).is(".persist_css")) continue;
             if ($(itms[c]).is(".lnk172-blue") || $(itms[c]).is(".lnk391-blue")) {
                 $(itms[c]).css("width", "auto");
                 var w = $(itms[c]).width();
                 $(itms[c]).css("width", "").removeClass("lnk172-blue").removeClass("lnk391-blue")
                 if (w < 160)
                     $(itms[c]).addClass("lnk172-blue");
                 else
                     $(itms[c]).addClass("lnk391-blue");
             }
         }
     })();
    </script>
</asp:Content>

﻿'Imports Sites.SharedSiteCode
'Imports SpiceLogicPayPalStandard
Imports Library.Public
Imports Dating.Server.Core.DLL


Public Class SelectPaymentTR
    Inherits BasePage



    Protected Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            'If (_pageData Is Nothing) Then _pageData = New clsPageData("~/Members/SelectPayment.aspx", Context)
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData("~/Members/SelectPayment.aspx", Context, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property

    'Private __Subject As String
    'Private __Content As String

    Private Sub Page_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        Try
            If Me._pageData IsNot Nothing Then
                RemoveHandler Me._pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If

        Catch ex As Exception
        End Try
        Try
            MyBase.Dispose()
        Catch ex As Exception
        End Try
    End Sub

    Protected Overrides Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        SetMasterPage(CurrentPageData)

        Try
            If clsCurrentContext.VerifyLogin() = True Then
                Me.MasterPageFile = "~/Members/Members2.Master"
                'If (Request("master") = "inner") Then
                '    Me.MasterPageFile = "~/Members/Members2Inner.Master"
                'Else
                'End If
            Else
                clsCurrentContext.ClearSession(Session)
                If (Not Me.IsCallback) Then
                    Response.Redirect(ResolveUrl("~/Login.aspx"), False)
                End If
            End If
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If (Not Me.IsPostBack) Then
                DirectCast(Me.Master, Members2).HideBottom_MemberLeftPanel = True

                If (Me.SessionVariables.MemberData.Country = "TR") Then
                    'UCSelectPayment1.Visible = False
                    UCSelectPayment_TR1.Visible = True
                Else
                    Response.Redirect("SelectPayment.aspx?choice=" & Request.QueryString("choice"))
                End If

                LoadLAG()
                'If (Request.UrlReferrer IsNot Nothing) Then
                '    lnkBack.NavigateUrl = Request.UrlReferrer.ToString()
                'Else
                '    lnkBack.NavigateUrl = "~/Members/"
                'End If
            End If

        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try
    End Sub


    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender

        Try
            Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try


        '        Try
        '            If (Not Me.IsPostBack) Then
        '                If (HttpContext.Current.Request.Url.Host <> "localhost") Then
        '                    If (Me.SessionVariables.MemberData.Country = "TR") Then
        '                        __Subject = "Select Payment loaded (Country:" & Me.SessionVariables.MemberData.Country & ")"
        '                        __Content = <html><![CDATA[
        'Select Payment loaded (Country:[COUNTRY])
        'LoginName: [LOGINNAME]

        'URL: [URL]
        'IP: [IP]

        ']]></html>
        '                        __Content = __Content.Replace("[COUNTRY]", Me.SessionVariables.MemberData.Country)
        '                        __Content = __Content.Replace("[LOGINNAME]", Me.SessionVariables.MemberData.LoginName)
        '                        __Content = __Content.Replace("[URL]", HttpContext.Current.Request.Url.ToString())
        '                        __Content = __Content.Replace("[IP]", HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"))
        '                    End If
        '                End If
        '            End If
        '        Catch ex As Exception
        '            WebErrorMessageBox(Me, ex, "Page_Load")
        '        End Try

    End Sub



    'Private Sub Page_Unload1(sender As Object, e As System.EventArgs) Handles Me.Unload
    '    Try
    '        If (Not String.IsNullOrEmpty(__Content)) Then
    '            Dating.Server.Core.DLL.clsMyMail.SendMail(ConfigurationManager.AppSettings("PaymentLogsEmail"), __Subject, __Content, False)
    '        End If
    '    Catch
    '    End Try
    'End Sub


    Public Overrides Sub Master_LanguageChanged()

        If (Me.Visible) Then
            Me._pageData = Nothing
            LoadLAG()
        End If

    End Sub


    Protected Sub LoadLAG()
        Try

            '   Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic


            'lblPaymentMethodTitle.Text = CurrentPageData.GetCustomString(lblPaymentMethodTitle.ID)
            'lnkBack.Text = CurrentPageData.GetCustomString(lnkBack.ID)


            'lblItemsName.Text = Me.CurrentPageData.GetCustomString("CartSelectPaymentView")
            'lnkViewDescription.Text = Me.CurrentPageData.GetCustomString("CartSelectPaymentWhatIsIt")
            'lnkViewDescription.OnClientClick = OnMoreInfoClickFunc(
            '    lnkViewDescription.OnClientClick,
            '    ResolveUrl("~/Members/InfoWin.aspx?info=selectpayment"),
            '    Me.CurrentPageData.GetCustomString("CartSelectPaymentView"))
            'WhatIsIt1.PopupControl.HeaderText = Me.CurrentPageData.GetCustomString("CartSelectPaymentView")

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub
End Class
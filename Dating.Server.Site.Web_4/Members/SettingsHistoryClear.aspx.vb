﻿Imports System.Web.Services
Imports Dating.Server.Core.DLL

Public Class SettingsHistoryClear
    Inherits BasePage

#Region "Props"

    'Dim _pageData As clsPageData
    'Protected ReadOnly Property CurrentPageData As clsPageData
    '    Get
    '        If (_pageData Is Nothing) Then _pageData = New clsPageData(Context)
    '        Return _pageData
    '    End Get
    'End Property

    Public Property DayOfMonthFullName As String
#End Region


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If (Not Page.IsPostBack) Then
                Dim items As String = Request.QueryString("items")
                items = If(items Is Nothing, "", items)
                Session("HistoryClear") = items
            End If


            If (Not Page.IsPostBack) Then
                LoadLAG()
            End If

        Catch ex As Exception
            WebErrorSendEmail(ex, "")
        End Try
    End Sub


    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
    End Sub



    Protected Sub LoadLAG()
        Try
            DayOfMonthFullName = globalStrings.GetCustomString("DayOfMonthFullName", GetLag())
            DayOfMonthFullName = DayOfMonthFullName.Trim().Replace(vbCrLf, "','")

            SetControlsValue(Me, CurrentPageData)

            lblStepConfirmDeleteInsertPassword.Text = CurrentPageData.GetCustomString("lblStepConfirmDeleteInsertPassword")
            'lblYourPassword.Text = CurrentPageData.GetCustomString("lblYourPassword")
            lblStepConfirmDelete6.Text = CurrentPageData.GetCustomString("lblStepConfirmDelete6")
            lblStepConfirmDelete3Date.Text = CurrentPageData.GetCustomString("lblStepConfirmDelete3.Date")
            lblWatermark5.Text = CurrentPageData.GetCustomString("lblWatermark5")
            lnkContinueStep7.InnerHtml = CurrentPageData.GetCustomString("lnkFinish")
            lnkCancelStep1.InnerHtml = CurrentPageData.GetCustomString("lnkCancelStep")
            lnkContinueStep1.InnerHtml = CurrentPageData.GetCustomString("lnkContinueStep")

            lblTitle1.Text = CurrentPageData.GetCustomString("Incoming.Messages.Delete.Title")
            lblStepConfirmDelete1.Text = CurrentPageData.GetCustomString("Incoming.Messages.Delete.lblStepConfirmDelete1")
            lblStepConfirmDelete2.Text = CurrentPageData.GetCustomString("Incoming.Messages.Delete.lblStepConfirmDelete2")
            lblStepConfirmDelete3.Text = CurrentPageData.GetCustomString("Incoming.Messages.Delete.lblStepConfirmDelete3")
            lblStepConfirmDeleteUntil4.Text = CurrentPageData.GetCustomString("Incoming.Messages.Delete.lblStepConfirmDeleteUntil4")

            Select Case Session("HistoryClear")
                Case "incoming-messages"
                    lblTitle1.Text = CurrentPageData.GetCustomString("Incoming.Messages.Delete.Title")
                    lblStepConfirmDelete1.Text = CurrentPageData.GetCustomString("Incoming.Messages.Delete.lblStepConfirmDelete1")
                    lblStepConfirmDelete2.Text = CurrentPageData.GetCustomString("Incoming.Messages.Delete.lblStepConfirmDelete2")
                    lblStepConfirmDelete3.Text = CurrentPageData.GetCustomString("Incoming.Messages.Delete.lblStepConfirmDelete3")
                    lblStepConfirmDeleteUntil4.Text = CurrentPageData.GetCustomString("Incoming.Messages.Delete.lblStepConfirmDeleteUntil4")
                    lblStepConfirmDeleteInsertPassword.Text = CurrentPageData.GetCustomString("Incoming.Messages.Delete.lblStepConfirmDelete5.InsertPassword")

                Case "outgoing-messages"
                    lblTitle1.Text = CurrentPageData.GetCustomString("Outgoing.Messages.Delete.Title")
                    lblStepConfirmDelete1.Text = CurrentPageData.GetCustomString("Outgoing.Messages.Delete.lblStepConfirmDelete1")
                    lblStepConfirmDelete2.Text = CurrentPageData.GetCustomString("Outgoing.Messages.Delete.lblStepConfirmDelete2")
                    lblStepConfirmDelete3.Text = CurrentPageData.GetCustomString("Outgoing.Messages.Delete.lblStepConfirmDelete3")
                    lblStepConfirmDeleteUntil4.Text = CurrentPageData.GetCustomString("Outgoing.Messages.Delete.lblStepConfirmDeleteUntil4")
                    lblStepConfirmDeleteInsertPassword.Text = CurrentPageData.GetCustomString("Outgoing.Messages.Delete.lblStepConfirmDelete5.InsertPassword")

                Case "likes"
                    lblTitle1.Text = CurrentPageData.GetCustomString("Likes.Delete.Title")
                    lblStepConfirmDelete1.Text = CurrentPageData.GetCustomString("Likes.Delete.lblStepConfirmDelete1")
                    lblStepConfirmDelete2.Text = CurrentPageData.GetCustomString("Likes.Delete.lblStepConfirmDelete2")
                    lblStepConfirmDelete3.Text = CurrentPageData.GetCustomString("Likes.Delete.lblStepConfirmDelete3")
                    lblStepConfirmDeleteUntil4.Text = CurrentPageData.GetCustomString("Likes.Delete.lblStepConfirmDeleteUntil4")
                    lblStepConfirmDeleteInsertPassword.Text = CurrentPageData.GetCustomString("Likes.Delete.lblStepConfirmDelete5.InsertPassword")

                Case "offers"
                    lblTitle1.Text = CurrentPageData.GetCustomString("Offers.Delete.Title")
                    lblStepConfirmDelete1.Text = CurrentPageData.GetCustomString("Offers.Delete.lblStepConfirmDelete1")
                    lblStepConfirmDelete2.Text = CurrentPageData.GetCustomString("Offers.Delete.lblStepConfirmDelete2")
                    lblStepConfirmDelete3.Text = CurrentPageData.GetCustomString("Offers.Delete.lblStepConfirmDelete3")
                    lblStepConfirmDeleteUntil4.Text = CurrentPageData.GetCustomString("Offers.Delete.lblStepConfirmDeleteUntil4")
                    lblStepConfirmDeleteInsertPassword.Text = CurrentPageData.GetCustomString("Offers.Delete.lblStepConfirmDelete5.InsertPassword")

                Case "dates"
                    lblTitle1.Text = CurrentPageData.GetCustomString("Dates.Delete.Title")
                    lblStepConfirmDelete1.Text = CurrentPageData.GetCustomString("Dates.Delete.lblStepConfirmDelete1")
                    lblStepConfirmDelete2.Text = CurrentPageData.GetCustomString("Dates.Delete.lblStepConfirmDelete2")
                    lblStepConfirmDelete3.Text = CurrentPageData.GetCustomString("Dates.Delete.lblStepConfirmDelete3")
                    lblStepConfirmDeleteUntil4.Text = CurrentPageData.GetCustomString("Dates.Delete.lblStepConfirmDeleteUntil4")
                    lblStepConfirmDeleteInsertPassword.Text = CurrentPageData.GetCustomString("Dates.Messages.Delete.lblStepConfirmDelete5.InsertPassword")

                Case "my-shared-photos"
                    lblTitle1.Text = CurrentPageData.GetCustomString("My.Shared.Photos.Delete.Title")
                    lblStepConfirmDelete1.Text = CurrentPageData.GetCustomString("My.Shared.Photos.Delete.lblStepConfirmDelete1")
                    lblStepConfirmDelete2.Text = CurrentPageData.GetCustomString("My.Shared.Photos.Delete.lblStepConfirmDelete2")
                    lblStepConfirmDelete3.Text = CurrentPageData.GetCustomString("My.Shared.Photos.Delete.lblStepConfirmDelete3")
                    lblStepConfirmDeleteUntil4.Text = CurrentPageData.GetCustomString("My.Shared.Photos.Delete.lblStepConfirmDeleteUntil4")
                    lblStepConfirmDeleteInsertPassword.Text = CurrentPageData.GetCustomString("My.Shared.Photos.Delete.lblStepConfirmDelete5.InsertPassword")

                Case "other-shared-photos"
                    lblTitle1.Text = CurrentPageData.GetCustomString("Other.Shared.Photos.Delete.Title")
                    lblStepConfirmDelete1.Text = CurrentPageData.GetCustomString("Other.Shared.Photos.Delete.lblStepConfirmDelete1")
                    lblStepConfirmDelete2.Text = CurrentPageData.GetCustomString("Other.Shared.Photos.Delete.lblStepConfirmDelete2")
                    lblStepConfirmDelete3.Text = CurrentPageData.GetCustomString("Other.Shared.Photos.Delete.lblStepConfirmDelete3")
                    lblStepConfirmDeleteUntil4.Text = CurrentPageData.GetCustomString("Other.Shared.Photos.Delete.lblStepConfirmDeleteUntil4")
                    lblStepConfirmDeleteInsertPassword.Text = CurrentPageData.GetCustomString("Other.Shared.Photos.Delete.lblStepConfirmDelete5.InsertPassword")

                Case "my-viewed-profiles"
                    lblTitle1.Text = CurrentPageData.GetCustomString("My.Viewed.Profiles.Delete.Title")
                    lblStepConfirmDelete1.Text = CurrentPageData.GetCustomString("My.Viewed.Profiles.Delete.lblStepConfirmDelete1")
                    lblStepConfirmDelete2.Text = CurrentPageData.GetCustomString("My.Viewed.Profiles.Delete.lblStepConfirmDelete2")
                    lblStepConfirmDelete3.Text = CurrentPageData.GetCustomString("My.Viewed.Profiles.Delete.lblStepConfirmDelete3")
                    lblStepConfirmDeleteUntil4.Text = CurrentPageData.GetCustomString("My.Viewed.Profiles.Delete.lblStepConfirmDeleteUntil4")
                    lblStepConfirmDeleteInsertPassword.Text = CurrentPageData.GetCustomString("My.Viewed.Profiles.Delete.lblStepConfirmDelete5.InsertPassword")

                Case "other-viewed-profiles"
                    lblTitle1.Text = CurrentPageData.GetCustomString("Other.Viewed.Profiles.Delete.Title")
                    lblStepConfirmDelete1.Text = CurrentPageData.GetCustomString("Other.Viewed.Profiles.Delete.lblStepConfirmDelete1")
                    lblStepConfirmDelete2.Text = CurrentPageData.GetCustomString("Other.Viewed.Profiles.Delete.lblStepConfirmDelete2")
                    lblStepConfirmDelete3.Text = CurrentPageData.GetCustomString("Other.Viewed.Profiles.Delete.lblStepConfirmDelete3")
                    lblStepConfirmDeleteUntil4.Text = CurrentPageData.GetCustomString("Other.Viewed.Profiles.Delete.lblStepConfirmDeleteUntil4")
                    lblStepConfirmDeleteInsertPassword.Text = CurrentPageData.GetCustomString("Other.Viewed.Profiles.Delete.lblStepConfirmDelete5.InsertPassword")

                Case Else

            End Select

            'lblTitle2.Text = lblTitle1.Text
            'lblTitle3.Text = lblTitle1.Text
            'lblTitle4.Text = lblTitle1.Text
            'lblTitle5.Text = lblTitle1.Text
            'lblTitle6.Text = lblTitle1.Text
            'lblTitle7.Text = lblTitle1.Text

            lnkCancelStep2.InnerHtml = lnkCancelStep1.InnerHtml
            lnkContinueStep2.InnerHtml = lnkContinueStep1.InnerHtml
            lnkCancelStep3.InnerHtml = lnkCancelStep1.InnerHtml
            lnkContinueStep3.InnerHtml = lnkContinueStep1.InnerHtml
            lnkCancelStep4.InnerHtml = lnkCancelStep1.InnerHtml
            lnkContinueStep4.InnerHtml = lnkContinueStep1.InnerHtml
            lnkCancelStep5.InnerHtml = lnkCancelStep1.InnerHtml
            lnkContinueStep5.InnerHtml = lnkContinueStep1.InnerHtml


        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    <WebMethod()> _
    Public Shared Function PerformHistoryClear(dateUntil As String) As String
        Dim resultStr As String = ""
        Try
            Dim isTest As Boolean = False

            Dim provider As Globalization.CultureInfo = Globalization.CultureInfo.InvariantCulture
            Dim format As String = "dd/MM/yyyy"
            Dim dateTo As DateTime
            Try
                dateTo = DateTime.ParseExact(dateUntil, format, provider).AddSeconds(86399)
            Catch ex As FormatException
                WebErrorSendEmail(ex, "PerformHistoryClear")
            End Try

            Dim items As String = HttpContext.Current.Session("HistoryClear")
            Dim ProfileID As Integer = HttpContext.Current.Session("ProfileID")
            Dim LAGID As String = HttpContext.Current.Session("LAGID")

            items = If(items Is Nothing, "", items)
            Dim lag As New clsSiteLAG()


            Select Case items
                Case "incoming-messages"

                    Try
                        If (Not isTest) Then
                            Dim _ReferrerParentId As Integer = DataHelpers.GetEUS_Profiles_ReferrerParentId(ProfileID)
                            clsMessagesHelper.DeleteMessages_ClearHistory(ProfileID, (_ReferrerParentId > 0), True, Nothing, dateTo)
                            'ModGlobals.WebInfoSendEmail("DeleteMessages_ClearHistory: Params (with lag param), " & " newUrl:" & newUrl)
                        End If
                        resultStr = lag.GetCustomStringFromPage(LAGID, "~/Members/SettingsHistoryClear.aspx", "Incoming.Messages.Delete.lblActionSuccessfull", ModGlobals.ConnectionString)
                    Catch ex As Exception
                        resultStr = lag.GetCustomStringFromPage(LAGID, "~/Members/SettingsHistoryClear.aspx", "Incoming.Messages.Delete.lblActionFailed", ModGlobals.ConnectionString)
                    End Try


                Case "outgoing-messages"

                    Try
                        If (Not isTest) Then
                            Dim _ReferrerParentId As Integer = DataHelpers.GetEUS_Profiles_ReferrerParentId(ProfileID)
                            clsMessagesHelper.DeleteMessages_ClearHistory(ProfileID, (_ReferrerParentId > 0), Nothing, True, dateTo)
                        End If
                        resultStr = lag.GetCustomStringFromPage(LAGID, "~/Members/SettingsHistoryClear.aspx", "Outgoing.Messages.Delete.lblActionSuccessfull", ModGlobals.ConnectionString)
                    Catch ex As Exception
                        resultStr = lag.GetCustomStringFromPage(LAGID, "~/Members/SettingsHistoryClear.aspx", "Outgoing.Messages.Delete.lblActionFailed", ModGlobals.ConnectionString)
                    End Try

                Case "likes"

                    Try
                        If (Not isTest) Then
                            clsUserDoes.DeleteLikes_ClearHistory(ProfileID, dateTo)
                        End If
                        resultStr = lag.GetCustomStringFromPage(LAGID, "~/Members/SettingsHistoryClear.aspx", "Likes.Delete.lblActionSuccessfull", ModGlobals.ConnectionString)
                    Catch ex As Exception
                        resultStr = lag.GetCustomStringFromPage(LAGID, "~/Members/SettingsHistoryClear.aspx", "Likes.Delete.lblActionFailed", ModGlobals.ConnectionString)
                    End Try

                Case "offers"

                    Try
                        If (Not isTest) Then
                            clsUserDoes.DeleteOffers_ClearHistory(ProfileID, dateTo)
                        End If
                        resultStr = lag.GetCustomStringFromPage(LAGID, "~/Members/SettingsHistoryClear.aspx", "Offers.Delete.lblActionSuccessfull", ModGlobals.ConnectionString)
                    Catch ex As Exception
                        resultStr = lag.GetCustomStringFromPage(LAGID, "~/Members/SettingsHistoryClear.aspx", "Offers.Delete.lblActionFailed", ModGlobals.ConnectionString)
                    End Try

                Case "dates"

                    Try
                        If (Not isTest) Then
                            clsUserDoes.DeleteDates_ClearHistory(ProfileID, dateTo)
                        End If
                        resultStr = lag.GetCustomStringFromPage(LAGID, "~/Members/SettingsHistoryClear.aspx", "Dates.Delete.lblActionSuccessfull", ModGlobals.ConnectionString)
                    Catch ex As Exception
                        resultStr = lag.GetCustomStringFromPage(LAGID, "~/Members/SettingsHistoryClear.aspx", "Dates.Delete.lblActionFailed", ModGlobals.ConnectionString)
                    End Try

                Case "my-shared-photos"


                    Try
                        If (Not isTest) Then
                            clsUserDoes.DeletePhotosShares_ClearHistory(ProfileID, Nothing, True, dateTo)
                        End If
                        resultStr = lag.GetCustomStringFromPage(LAGID, "~/Members/SettingsHistoryClear.aspx", "My.Shared.Photos.Delete.lblActionSuccessfull", ModGlobals.ConnectionString)
                    Catch ex As Exception
                        resultStr = lag.GetCustomStringFromPage(LAGID, "~/Members/SettingsHistoryClear.aspx", "My.Shared.Photos.Delete.lblActionFailed", ModGlobals.ConnectionString)
                    End Try

                Case "other-shared-photos"

                    Try
                        If (Not isTest) Then
                            clsUserDoes.DeletePhotosShares_ClearHistory(ProfileID, True, Nothing, dateTo)
                        End If
                        resultStr = lag.GetCustomStringFromPage(LAGID, "~/Members/SettingsHistoryClear.aspx", "Other.Shared.Photos.Delete.lblActionSuccessfull", ModGlobals.ConnectionString)
                    Catch ex As Exception
                        resultStr = lag.GetCustomStringFromPage(LAGID, "~/Members/SettingsHistoryClear.aspx", "Other.Shared.Photos.Delete.lblActionFailed", ModGlobals.ConnectionString)
                    End Try

                Case "my-viewed-profiles"

                    Try
                        If (Not isTest) Then
                            clsUserDoes.DeleteProfilesViewed_ClearHistory(ProfileID, Nothing, True, dateTo)
                        End If
                        resultStr = lag.GetCustomStringFromPage(LAGID, "~/Members/SettingsHistoryClear.aspx", "My.Viewed.Profiles.Delete.lblActionSuccessfull", ModGlobals.ConnectionString)
                    Catch ex As Exception
                        resultStr = lag.GetCustomStringFromPage(LAGID, "~/Members/SettingsHistoryClear.aspx", "My.Viewed.Profiles.Delete.lblActionFailed", ModGlobals.ConnectionString)
                    End Try

                Case "other-viewed-profiles"

                    Try
                        If (Not isTest) Then
                            clsUserDoes.DeleteProfilesViewed_ClearHistory(ProfileID, True, Nothing, dateTo)
                        End If
                        resultStr = lag.GetCustomStringFromPage(LAGID, "~/Members/SettingsHistoryClear.aspx", "Other.Viewed.Profiles.Delete.lblActionSuccessfull", ModGlobals.ConnectionString)
                    Catch ex As Exception
                        resultStr = lag.GetCustomStringFromPage(LAGID, "~/Members/SettingsHistoryClear.aspx", "Other.Viewed.Profiles.Delete.lblActionFailed", ModGlobals.ConnectionString)
                    End Try

                Case Else

            End Select

        Catch ex As Exception
            WebErrorSendEmail(ex, "PerformHistoryClear")
        End Try

        Return resultStr
    End Function


    <WebMethod()> _
    Public Shared Function VerifyPassword(pass As String) As String
        Try
            Dim lagid As String = HttpContext.Current.Session("LAGID")
            Dim ProfileID As Integer = HttpContext.Current.Session("ProfileID")
            Dim b As Boolean = Dating.Server.Core.DLL.DataHelpers.EUS_Profiles_CheckPassword(ProfileID, pass)
            If (b) Then
                Return "true"
            Else
                Dim o As New clsSiteLAG()
                Return o.GetCustomStringFromPage(lagid, "~/Members/SettingsHistoryClear.aspx", "lblPasswordVerifyFailed", ConnectionString)
            End If
        Catch ex As Exception
            WebErrorSendEmail(ex, "")
        End Try
        Return "false"
    End Function



   

End Class
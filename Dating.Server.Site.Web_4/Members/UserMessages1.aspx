﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Members/Members2Inner.Master"
    CodeBehind="UserMessages1.aspx.vb" Inherits="Dating.Server.Site.Web.UserMessages1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Scripts/jquery.json-2.4.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.dateFormat-1.0.js" type="text/javascript"></script>
    <style type="text/css">
        body{background-image:none;}
        .text-wrap { margin:20px 10px;}
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <div class="text-wrap">
    <asp:Literal ID="lblMessageBody" runat="server"></asp:Literal>
        </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cntBodyBottom" runat="server">

    <script type="text/javascript">
        function req_setMessageRead() {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "json.aspx/UserMessages_Read",
                data: '{"itemid":"<%= Request.QueryString("itemid")%>"}',
                dataType: "json",
                success: function (msg) {  }
            });
        }

        $(function ($) {
            req_setMessageRead()
        });
    </script>

</asp:Content>

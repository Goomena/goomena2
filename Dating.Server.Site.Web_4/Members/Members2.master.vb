﻿Imports System.Data.SqlClient
Imports Dating.Server.Core.DLL
Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Datasets.DLL.DSMembers
Imports DevExpress.Web.ASPxEditors



Public Class Members2
    Inherits BaseMasterPageMembers


    Public Property HideBottom_MemberLeftPanel As Boolean
    Public Property ifrUserMap_src As String

    Private Sub Page_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        Try
            If Me._pageData IsNot Nothing Then
                RemoveHandler Me._pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If

        Catch ex As Exception
        End Try
        Try
            If Me.mbr IsNot Nothing Then
                If Me.mbr.LoginStatusControl IsNot Nothing Then
                    RemoveHandler mbr.LoginStatusControl.LoggingOut, AddressOf LoginStatus1_LoggingOut
                End If
            End If
        Catch ex As Exception

        End Try
        Try
            MyBase.Dispose()
        Catch ex As Exception
        End Try
    End Sub
    Private Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        Try
            ''If Not clsCurrentContext.VerifyLogin() OrElse Request.Form("__EVENTTARGET") = LoginStatus1.UniqueID & "$ctl00" Then
            ''    '"ctl00%24SettingsPopup%24LoginStatus1%24ctl00" Then
            ''    clsCurrentContext.ClearSession(Session)
            ''    Response.Redirect(ResolveUrl("~/Login.aspx"), False)
            ''    HttpContext.Current.ApplicationInstance.CompleteRequest()
            ''    Session("LoginRedirectToDefault") = 1
            ''End If


            AddHandler mbr.LoginStatusControl.LoggingOut, AddressOf LoginStatus1_LoggingOut

            If Not Me.IsPostBack Then

                Dim TimeOffset As Double
                If (hdfTimeOffset.Value.Trim() <> "" AndAlso Double.TryParse(hdfTimeOffset.Value, TimeOffset)) Then
                    Session("TimeOffset") = TimeOffset
                End If

                Dim lagIdOnStart As String = Session("LagID")
                Dim lagIdFromDB As String = Nothing
                Dim lagIdFromLAGParam As String = Nothing

                If (Session("NoRedirectToMembersDefault") Is Nothing) Then
                    Session("NoRedirectToMembersDefault") = True

                    ' read lagid from database, first time user enters members area
                    lagIdFromDB = DataHelpers.GetEUS_Profiles_LAGID(Me.Session("ProfileID"))
                End If

                If (clsLanguageHelper.IsSupportedLAG(Request.QueryString("lag"), Session("GEO_COUNTRY_CODE"))) Then
                    lagIdFromLAGParam = Request.QueryString("lag").ToUpper()
                End If

                ' on first time in members area, 
                ' redirect if the requested lag(lag param) is different from lag in DB
                If (Not String.IsNullOrEmpty(lagIdFromLAGParam) AndAlso Not String.IsNullOrEmpty(lagIdFromDB)) Then
                    If (lagIdFromLAGParam.ToUpper() <> lagIdFromDB.ToUpper() AndAlso
                        clsLanguageHelper.IsSupportedLAG(lagIdFromDB, GetGeoCountry())) Then

                        Dim newUrl As String = LanguageHelper.GetNewLAGUrl_WithLagParam(lagIdFromDB)
                        Response.Redirect(newUrl)
                    End If
                ElseIf (Not String.IsNullOrEmpty(lagIdFromLAGParam)) Then
                    Session("LagID") = lagIdFromLAGParam
                End If


                If (Not clsLanguageHelper.IsSupportedLAG(Session("LagID"), Session("GEO_COUNTRY_CODE"))) Then
                    clsLanguageHelper.SetLAGID()
                End If

                If (clsLanguageHelper.IsSupportedLAG(Session("LagID"), Session("GEO_COUNTRY_CODE"))) Then
                    cmbLag.Items(clsLanguageHelper.GetIndexFromLagID(Session("LagID"))).Selected = True
                End If
                clsLanguageHelper.SetLagCookie(Session("LagID"))
                SetLanguageLinks()

                ' update lagid info
                Try
                    If (lagIdOnStart <> Session("LagID")) Then
                        If (Me.Session("ProfileID") > 0) Then
                            DataHelpers.UpdateEUS_Profiles_LAGID(Me.Session("ProfileID"), Me.Session("LagID"))
                        End If
                    End If
                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "")
                End Try
            End If


            If (cmbLag.SelectedItem Is Nothing) Then
                If (clsLanguageHelper.IsSupportedLAG(Session("LagID"), Session("GEO_COUNTRY_CODE"))) Then
                    cmbLag.Items(clsLanguageHelper.GetIndexFromLagID(Session("LagID"))).Selected = True
                End If
                clsLanguageHelper.SetLagCookie(Session("LagID"))
            End If


            Try
                ' otan to session exei diaforetiki glwsa apo tin epilegmeni glwssa
                If (clsLanguageHelper.GetIndexFromLagID(Session("LagID")) = 0 AndAlso Session("LagID") <> "US") Then
                    Session("LagID") = gDomainCOM_DefaultLAG
                    clsLanguageHelper.SetLagCookie(Session("LagID"))
                End If
            Catch
            End Try

        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Dim a As New _RootMaster
        'If (a.IsFemale) AndAlso (Session("LagID") = "GR") Then
        '    Me.womenbanner.Attributes.Add("src", "//cdn.goomena.com/goomena-kostas/gifts/banner-home-page.jpg")
        '    Me.womenbannerhref.Attributes.Add("href", "http://www.goomena.com/cmspage.aspx?pageid=276&title=Gifts.aspx")
        'End If

        Try
            '    BindMembersList() 'set popup controls enabled/disabled first
            '    LoadLAG()
            '    ShowUserMap()

            '    If (Not Page.IsPostBack) Then
            '        content_bottom.Visible = False

            '        If (Me.Request.RawUrl.ToUpper().StartsWith("/MEMBERS/")) Then
            '            content_bottom.Visible = True
            '            'SetReferrerButtonVisible()
            '        End If

            '    End If
            Try
                'If Session("HasNewTermsPopUp") Is Nothing Then
                '    Dim r As Boolean = ProfileHelper.GetProfileHasApprovedNewTos(Me.MasterProfileId)
                '    Session("HasNewTermsPopUp") = r
                'End If
                'If Not Session("HasNewTermsPopUp") AndAlso Session("HasNewTermsPopUp") = False Then
                '    '     Session("HasNewTermsPopUp") = True
                '    Dim ctucNewTerms As ucPopupNewTerms = Me.LoadControl("~/UserControls/ucPopupNewTerms.ascx")
                '    Me.content.Controls.Add(ctucNewTerms)
                '    ctucNewTerms.ShowPopUp()
                'ElseIf Me.IsFemale Then

                '    If Session("HasSeenGiftPopUp") Is Nothing Then
                '        Dim r As Boolean = ProfileHelper.getHasSeenPopup(Me.MasterProfileId)
                '        Session("HasSeenGiftPopUp") = r
                '    End If
                '    If Not Session("HasSeenGiftPopUp") AndAlso Session("HasSeenGiftPopUp") = False Then
                '        Dim ctucPopupGift As ucPopupGift = Me.LoadControl("~/UserControls/ucPopupGift.ascx")
                '        Me.content.Controls.Add(ctucPopupGift)
                '        ctucPopupGift.ShowPopUp()
                '    End If
                'End If
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try

          

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        Try

            LoadLAG()
            leftPanel.Visible = Not HideBottom_MemberLeftPanel

            If Not Me.IsPostBack Then

                If (Session("SET_ADDITIONAL_INFO") Is Nothing) Then
                    Dim SET_ADDITIONAL_INFO As Boolean = clsProfilesPrivacySettings.GET_RegisterAdditionalInfo(Me.MasterProfileId)
                    Session("SET_ADDITIONAL_INFO") = SET_ADDITIONAL_INFO
                End If

                If (Session("SET_ADDITIONAL_INFO") = True OrElse
                    Request.QueryString("showadditional") = "1") Then

                    pnlAdditionalInfo.Visible = True

                End If
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Protected Sub cmbLag_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbLag.SelectedIndexChanged
        clsLanguageHelper.SetLagCookie(cmbLag.SelectedItem.Value)
        'Try
        '             Dim lagCookie As HttpCookie = Request.Cookies("lagCookie")
        '    If lagCookie Is Nothing Then
        '        Dim szGuid As String = System.Guid.NewGuid().ToString()
        '        lagCookie = New HttpCookie("lagCookie", szGuid)
        '        lagCookie.Expires = DateTime.Now.AddYears(2)

        '        lagCookie.Item("LagID") = cmbLag.SelectedItem.Value
        '        Response.Cookies.Add(lagCookie)
        '        Session("lagCookie") = szGuid
        '    Else
        '        Session("lagCookie") = Server.HtmlEncode(lagCookie.Value)
        '        lagCookie.Item("LagID") = cmbLag.SelectedItem.Value
        '        If (lagCookie.Values.Count > 0) Then Session("lagCookie") = lagCookie.Values(0)
        '    End If
        '    Session("LagID") = lagCookie.Item("LagID")
        '    Response.Cookies.Add(lagCookie)
        '    'Response.AddHeader("Refresh", "1")

        'Catch ex As Exception
        '    Session("LagID") = cmbLag.SelectedItem.Value
        'End Try

        ' update lagid info
        Try
            DataHelpers.UpdateEUS_Profiles_LAGID(Me.MasterProfileId, Me.Session("LagID"))
            If (Not Me.Page.IsCallback) Then
                LanguageHelper.RedirectToNewLAGForMembers(cmbLag.SelectedIndex)
            End If
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try



        Try

            Me._pageData = Nothing
            'LoadLAG()

        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try


        Try
            ModGlobals.UpdateUserControls(Me.Page, True)
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


    Protected Sub LoadLAG()
        Try
            clsMenuHelper.PopulateMenuData(mnuFooter1, "mnuFooter1")
            clsMenuHelper.PopulateMenuData(mnuFooter2, "mnuFooter2")
            clsMenuHelper.PopulateMenuData(mnuFooter3, "mnuFooter3")
            clsMenuHelper.PopulateMenuData(mnuFooter4, "mnuFooter4")
            clsMenuHelper.PopulateMenuData(mnuFooter5, "mnuFooter5")


            Dim currentLag As String = GetLag()
            clsMenuHelper.CheckPublicMenuLinks(mnuFooter1, currentLag)
            clsMenuHelper.CheckPublicMenuLinks(mnuFooter2, currentLag)
            clsMenuHelper.CheckPublicMenuLinks(mnuFooter3, currentLag)
            clsMenuHelper.CheckPublicMenuLinks(mnuFooter4, currentLag)
            clsMenuHelper.CheckPublicMenuLinks(mnuFooter5, currentLag)

            lblFooterMenuTitle1.Text = CurrentPageData.GetCustomString("lblFooterMenuTitle1")
            lblFooterMenuTitle2.Text = CurrentPageData.GetCustomString("lblFooterMenuTitle2")
            lblFooterMenuTitle3.Text = CurrentPageData.GetCustomString("lblFooterMenuTitle3")
            lblFooterMenuTitle4.Text = CurrentPageData.GetCustomString("lblFooterMenuTitle4")
            lblFooterMenuTitle5.Text = CurrentPageData.GetCustomString("lblFooterMenuTitle5")

        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try


        Try
            lblFooterSiteTitle.Text = CurrentPageData.GetCustomString("lblFooterSiteTitle")
            lblFooterSiteDescription.Text = CurrentPageData.GetCustomString("lblFooterSiteDescription")
            lblFooterCopyRight.Text = CurrentPageData.GetCustomString("lblFooterCopyRight")
            'lblMoreInfo.Text = CurrentPageData.GetCustomString("lblMoreInfo")
            lnkSiteMap.Text = CurrentPageData.GetCustomString("lnkSiteMap")

            imgLogo.AlternateText = If(Me.IsMale, CurrentPageData.GetCustomString("maleLogoText"), CurrentPageData.GetCustomString("femaleLogoText"))
            imgLogo.ToolTip = imgLogo.AlternateText
            ''Dim lnkOffersAll As ASPxHyperLink = OffersPopup.FindControl("lnkOffersAll")
            ''If (lnkOffersAll IsNot Nothing) Then
            ''    lnkOffersAll.Text = "<span>" & CurrentPageData.GetCustomString("lnkOffersAll") & "</span>"
            ''End If

            ''Dim msg_OffersHeader As Literal = OffersPopup.FindControl("msg_OffersHeader")
            ''If (msg_OffersHeader IsNot Nothing) Then
            ''    msg_OffersHeader.Text = CurrentPageData.GetCustomString("msg_OffersHeader")
            ''End If


            btnSearch.Text = CurrentPageData.GetCustomString("btnSearch")
            'Dim btnUrl As String = btnSearch.NavigateUrl.TrimStart("~"c).ToUpper()
            'If (btnUrl.StartsWith(Request.Url.PathAndQuery.ToUpper())) Then
            '    btnSearch.CssClass = btnSearch.CssClass & " selectedBtn"
            'Else
            '    btnSearch.CssClass = btnSearch.CssClass.Replace(" selectedBtn", "")
            'End If

            'Dim lnkSearchAll As ASPxHyperLink = SearchPopup.FindControl("lnkSearchAll")
            'If (lnkSearchAll IsNot Nothing) Then
            '    lnkSearchAll.Text = "<span>" & CurrentPageData.GetCustomString("lnkSearchAll") & "</span>"
            'End If

            'Dim msg_SearchHeader As Literal = SearchPopup.FindControl("msg_SearchHeader")
            'If (msg_SearchHeader IsNot Nothing) Then
            '    msg_SearchHeader.Text = CurrentPageData.GetCustomString("msg_SearchHeader")
            'End If



            ''Dim lnkMessagesAll As ASPxHyperLink = MessagesPopup.FindControl("lnkMessagesAll")
            ''If (lnkMessagesAll IsNot Nothing) Then
            ''    lnkMessagesAll.Text = "<span>" & CurrentPageData.GetCustomString("lnkMessagesAll") & "</span>"
            ''End If

            ''Dim msg_MessagesHeader As Literal = MessagesPopup.FindControl("msg_MessagesHeader")
            ''If (msg_MessagesHeader IsNot Nothing) Then
            ''    msg_MessagesHeader.Text = CurrentPageData.GetCustomString("msg_MessagesHeader")
            ''End If



            ''Dim lnkDatesAll As ASPxHyperLink = DatesPopup.FindControl("lnkDatesAll")
            ''If (lnkDatesAll IsNot Nothing) Then
            ''    lnkDatesAll.Text = "<span>" & CurrentPageData.GetCustomString("lnkDatesAll") & "</span>"
            ''End If

            ''Dim msg_DatesHeader As Literal = DatesPopup.FindControl("msg_DatesHeader")
            ''If (msg_DatesHeader IsNot Nothing) Then
            ''    msg_DatesHeader.Text = CurrentPageData.GetCustomString("msg_DatesHeader")
            ''End If


            ''btnSettings.Text = CurrentPageData.GetCustomString("btnSettings")

            ''Dim lnkAccSettings As ASPxHyperLink = SettingsPopup.FindControl("lnkAccSettings")
            ''If (lnkAccSettings IsNot Nothing) Then
            ''    lnkAccSettings.Text = CurrentPageData.GetCustomString("lnkAccSettings")
            ''End If

            ''Dim lnkAccDelete As ASPxHyperLink = SettingsPopup.FindControl("lnkAccDelete")
            ''If (lnkAccDelete IsNot Nothing) Then
            ''    lnkAccDelete.Text = CurrentPageData.GetCustomString("lnkAccDelete")
            ''End If

            'Dim lnkLogOut As ASPxHyperLink = SettingsPopup.FindControl("lnkLogOut")
            'If (lnkLogOut IsNot Nothing) Then
            '    lnkLogOut.Text = CurrentPageData.GetCustomString("lnkLogOut")
            'End If


            ''Dim LoginStatus1 As LoginStatus = SettingsPopup.FindControl("LoginStatus1")
            ''If (LoginStatus1 IsNot Nothing) Then
            ''    LoginStatus1.LogoutText = CurrentPageData.GetCustomString("lnkLogOut")
            ''End If

            ''Dim btnHelp As ASPxHyperLink = SettingsPopup.FindControl("btnHelp")
            ''If (btnHelp IsNot Nothing) Then
            ''    btnHelp.Text = CurrentPageData.GetCustomString("btnHelp")
            ''    btnHelp.NavigateUrl = CurrentPageData.GetCustomString("btnHelp.NavigateUrl")
            ''End If


            'btnDrinks.Text = CurrentPageData.GetCustomString("btnDrinks")

            'btnUrl = btnDrinks.NavigateUrl.TrimStart("~"c).ToUpper()
            'If (btnUrl.StartsWith(Request.Url.PathAndQuery.ToUpper())) Then
            '    btnDrinks.CssClass = btnDrinks.CssClass & " selectedBtn"
            'Else
            '    btnDrinks.CssClass = btnDrinks.CssClass.Replace(" selectedBtn", "")
            'End If




            ''Dim msg_WinksHeader As Literal = WinksPopup.FindControl("msg_WinksHeader")
            ''If (msg_WinksHeader IsNot Nothing) Then
            ''    msg_WinksHeader.Text = CurrentPageData.GetCustomString("msg_WinksHeader")
            ''End If

            ''Dim lnkWinksAll As ASPxHyperLink = WinksPopup.FindControl("lnkWinksAll")
            ''If (lnkWinksAll IsNot Nothing) Then
            ''    lnkWinksAll.Text = "<span>" & CurrentPageData.GetCustomString("lnkWinksAll") & "</span>"
            ''End If

            'lblChartTitle.Text = CurrentPageData.GetCustomString("lblChartTitle")


            lblEscortsWarning.Text = CurrentPageData.GetCustomString("lblEscortsWarning")


            Dim lblEscortsWarningTooltip As Label = lblEscortsWarningPopup.FindControl("lblEscortsWarningTooltip")
            If (lblEscortsWarningTooltip IsNot Nothing) Then
                lblEscortsWarningTooltip.Text = CurrentPageData.GetCustomString("lblEscortsWarningTooltip")
                lblEscortsWarningPopup.Enabled = True
            Else
                lblEscortsWarningPopup.Enabled = False
            End If

            ''lnkPubDefault.Text = CurrentPageData.GetCustomString("lnkPubDefault")
            'lnkAffiliate.Text = CurrentPageData.GetCustomString("lnkAffiliate")



            Dim btnUrl As String
            Dim currentUrl As String = Request.Url.PathAndQuery.ToUpper()

            btnUrl = lnkOnline.NavigateUrl.TrimStart("~"c).ToUpper()
            If (btnUrl.StartsWith(currentUrl)) Then
                lnkOnline.CssClass = lnkOnline.CssClass & " selectedLink"
            Else
                lnkOnline.CssClass = lnkOnline.CssClass.Replace(" selectedLink", "")
            End If

            If (Me.IsFemale) Then
                lblOnline.Text = CurrentPageData.GetCustomString("FEMALE_lnkOnline")
            Else
                lblOnline.Text = CurrentPageData.GetCustomString("MALE_lnkOnline")
            End If

        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub

    'Private Sub SetReferrerButtonVisible()
    '    Dim dc As New CMSDBDataContext(System.Configuration.ConfigurationManager.ConnectionStrings("AppDBconnectionString").ConnectionString)
    '    Try


    '        Dim MasterProfileId As Integer = Session("ProfileId")
    '        Dim profGenderId As Integer? = (From itm In dc.EUS_Profiles
    '                                        Where (itm.ProfileID = MasterProfileId AndAlso itm.IsMaster = True) OrElse _
    '                                        (itm.ProfileID = MasterProfileId AndAlso itm.MirrorProfileID = 0 AndAlso itm.IsMaster = False)
    '                                        Select itm.GenderId).FirstOrDefault()

    '        If (profGenderId IsNot Nothing AndAlso ProfileHelper.IsFemale(profGenderId)) Then
    '            pnlAffiliate.Visible = True
    '        End If
    '    Catch ex As Exception
    '        WebErrorMessageBox(Me, ex, "")
    '    Finally
    '        dc.Dispose()
    '    End Try

    'End Sub

    ''    Private Sub BindMembersList()
    ''        ' set links
    ''        Dim __logdate As DateTime = DateTime.UtcNow
    ''        Dim MasterProfileId As Integer
    ''        Try
    ''            MasterProfileId = CType(Me.Page, BasePage).MasterProfileId

    ''            Dim sqlProc As String = "EXEC GetNewCounters @CurrentProfileId= " & MasterProfileId
    ''            Dim dt As DataTable = DataHelpers.GetDataTable(sqlProc)
    ''            'Dim textPattern As String = "#TEXT# (#COUNT#)"
    ''            Dim textPattern As String = <html><![CDATA[
    ''#TEXT# <span id="notificationsCountWrapper" class="jewelCount"><span class="countValue">#COUNT#</span></span>
    '']]></html>.Value

    ''            Dim btnUrl As String = ""


    ''            btnDates.Text = CurrentPageData.GetCustomString("btnDates")
    ''            If (Me.SessionVariables.MemberData.Status = ProfileStatusEnum.LIMITED) Then

    ''                btnDates.NavigateUrl = "javascript:void(0);"
    ''                btnDates.CssClass = btnDates.CssClass.Replace(" limited", "") & " limited"
    ''                If (dt.Rows(0)("NewDates2") > 0) Then
    ''                    btnDates.Text = textPattern.Replace("#TEXT#", btnDates.Text).Replace("#COUNT#", dt.Rows(0)("NewDates2"))
    ''                    DatesPopup.Visible = True
    ''                Else
    ''                    DatesPopup.Visible = False
    ''                End If

    ''                Dim lnkDatesAll As ASPxHyperLink = DatesPopup.FindControl("lnkDatesAll")
    ''                If (lnkDatesAll IsNot Nothing) Then
    ''                    lnkDatesAll.Text = "<span>" & CurrentPageData.GetCustomString("lnkDatesAll") & "</span>"
    ''                    lnkDatesAll.NavigateUrl = "javascript:void(0);"
    ''                    lnkDatesAll.CssClass = lnkDatesAll.CssClass.Replace(" limited", "") & " limited"
    ''                    lnkDatesAll.ClientSideEvents.Click = ""
    ''                End If

    ''            Else

    ''                If (dt.Rows(0)("NewDates2") > 0) Then
    ''                    btnDates.Text = textPattern.Replace("#TEXT#", btnDates.Text).Replace("#COUNT#", dt.Rows(0)("NewDates2"))
    ''                    btnDates.NavigateUrl = "javascript:void(0);"
    ''                    DatesPopup.Visible = True

    ''                    'Dim lnkDatesAll As ASPxHyperLink = WinksPopup.FindControl("lnkDatesAll")
    ''                    'If (lnkDatesAll IsNot Nothing) Then
    ''                    '    lnkDatesAll.ClientSideEvents.Click = "function(s,e){ShowLoading();}"
    ''                    'End If
    ''                Else
    ''                    btnDates.Attributes.Add("onclick", "ShowLoading();")
    ''                    btnDates.NavigateUrl = "~/Members/Dates.aspx?vw=ACCEPTED"
    ''                    DatesPopup.Visible = False
    ''                End If

    ''            End If

    ''            'btnUrl = btnDates.NavigateUrl.TrimStart("~"c).ToUpper()
    ''            'If (btnUrl.StartsWith(Request.Url.PathAndQuery.ToUpper())) Then
    ''            '    btnDates.CssClass = btnDates.CssClass & " selectedBtn"
    ''            'Else
    ''            '    btnDates.CssClass = btnDates.CssClass.Replace(" selectedBtn", "")
    ''            'End If


    ''            btnWinks.Text = CurrentPageData.GetCustomString("btnWinks")
    ''            If (Me.SessionVariables.MemberData IsNot Nothing AndAlso Me.SessionVariables.MemberData.Status = ProfileStatusEnum.LIMITED) Then

    ''                btnWinks.NavigateUrl = "javascript:void(0);"
    ''                btnWinks.CssClass = btnWinks.CssClass.Replace(" limited", "") & " limited"
    ''                If (dt.Rows(0)("NewWinks") > 0) Then
    ''                    btnWinks.Text = textPattern.Replace("#TEXT#", btnWinks.Text).Replace("#COUNT#", dt.Rows(0)("NewWinks"))
    ''                    WinksPopup.Visible = True
    ''                Else
    ''                    WinksPopup.Visible = False
    ''                End If

    ''                Dim lnkWinksAll As ASPxHyperLink = WinksPopup.FindControl("lnkWinksAll")
    ''                If (lnkWinksAll IsNot Nothing) Then
    ''                    lnkWinksAll.Text = "<span>" & CurrentPageData.GetCustomString("lnkWinksAll") & "</span>"
    ''                    lnkWinksAll.NavigateUrl = "javascript:void(0);"
    ''                    lnkWinksAll.CssClass = lnkWinksAll.CssClass.Replace(" limited", "") & " limited"
    ''                    lnkWinksAll.ClientSideEvents.Click = ""
    ''                End If

    ''            Else

    ''                If (dt.Rows(0)("NewWinks") > 0) Then
    ''                    btnWinks.Text = textPattern.Replace("#TEXT#", btnWinks.Text).Replace("#COUNT#", dt.Rows(0)("NewWinks"))
    ''                    btnWinks.NavigateUrl = "javascript:void(0);"
    ''                    WinksPopup.Visible = True

    ''                    'Dim lnkWinksAll As ASPxHyperLink = WinksPopup.FindControl("lnkWinksAll")
    ''                    'If (lnkWinksAll IsNot Nothing) Then
    ''                    '    lnkWinksAll.ClientSideEvents.Click = "function(s,e){ShowLoading();}"
    ''                    'End If
    ''                Else
    ''                    btnWinks.Attributes.Add("onclick", "ShowLoading();")
    ''                    btnWinks.NavigateUrl = "~/Members/Likes.aspx?vw=likes"
    ''                    WinksPopup.Visible = False
    ''                End If

    ''            End If


    ''            'btnUrl = btnWinks.NavigateUrl.TrimStart("~"c).ToUpper()
    ''            'If (btnUrl.StartsWith(Request.Url.PathAndQuery.ToUpper())) Then
    ''            '    btnWinks.CssClass = btnWinks.CssClass & " selectedBtn"
    ''            'Else
    ''            '    btnWinks.CssClass = btnWinks.CssClass.Replace(" selectedBtn", "")
    ''            'End If



    ''            btnOffers.Text = CurrentPageData.GetCustomString("btnOffers")
    ''            If (Me.SessionVariables.MemberData IsNot Nothing AndAlso Me.SessionVariables.MemberData.Status = ProfileStatusEnum.LIMITED) Then

    ''                btnOffers.NavigateUrl = "javascript:void(0);"
    ''                btnOffers.CssClass = btnOffers.CssClass.Replace(" limited", "") & " limited"
    ''                If (dt.Rows(0)("NewOffers") > 0) Then
    ''                    btnOffers.Text = textPattern.Replace("#TEXT#", btnOffers.Text).Replace("#COUNT#", dt.Rows(0)("NewOffers"))
    ''                    OffersPopup.Visible = True
    ''                Else
    ''                    OffersPopup.Visible = False
    ''                End If

    ''                Dim lnkOffersAll As ASPxHyperLink = OffersPopup.FindControl("lnkOffersAll")
    ''                If (lnkOffersAll IsNot Nothing) Then
    ''                    lnkOffersAll.Text = "<span>" & CurrentPageData.GetCustomString("lnkOffersAll") & "</span>"
    ''                    lnkOffersAll.NavigateUrl = "javascript:void(0);"
    ''                    lnkOffersAll.CssClass = lnkOffersAll.CssClass.Replace(" limited", "") & " limited"
    ''                    lnkOffersAll.ClientSideEvents.Click = ""
    ''                End If

    ''            Else
    ''                If (dt.Rows(0)("NewOffers") > 0) Then
    ''                    btnOffers.Text = textPattern.Replace("#TEXT#", btnOffers.Text).Replace("#COUNT#", dt.Rows(0)("NewOffers"))
    ''                    btnOffers.NavigateUrl = "javascript:void(0);"
    ''                    OffersPopup.Visible = True

    ''                    'Dim lnkOffersAll As ASPxHyperLink = WinksPopup.FindControl("lnkOffersAll")
    ''                    'If (lnkOffersAll IsNot Nothing) Then
    ''                    '    lnkOffersAll.ClientSideEvents.Click = "function(s,e){ShowLoading();}"
    ''                    'End If
    ''                Else
    ''                    ' find navigate url from footer link button
    ''                    btnOffers.Attributes.Add("onclick", "ShowLoading();")
    ''                    btnOffers.NavigateUrl = "~/Members/Offers3.aspx?vw=newoffers"
    ''                    OffersPopup.Visible = False
    ''                End If
    ''            End If

    ''            'btnUrl = btnOffers.NavigateUrl.TrimStart("~"c).ToUpper()
    ''            'If (btnUrl.StartsWith(Request.Url.PathAndQuery.ToUpper())) Then
    ''            '    btnOffers.CssClass = btnOffers.CssClass & " selectedBtn"
    ''            'Else
    ''            '    btnOffers.CssClass = btnOffers.CssClass.Replace(" selectedBtn", "")
    ''            'End If




    ''            btnMessages.Text = CurrentPageData.GetCustomString("btnMessages")
    ''            If (Me.SessionVariables.MemberData IsNot Nothing AndAlso Me.SessionVariables.MemberData.Status = ProfileStatusEnum.LIMITED) Then

    ''                btnMessages.NavigateUrl = "javascript:void(0);"
    ''                btnMessages.CssClass = btnMessages.CssClass.Replace(" limited", "") & " limited"
    ''                If (dt.Rows(0)("NewMessages") > 0) Then
    ''                    btnMessages.Text = textPattern.Replace("#TEXT#", btnMessages.Text).Replace("#COUNT#", dt.Rows(0)("NewMessages"))
    ''                    MessagesPopup.Visible = True
    ''                Else
    ''                    MessagesPopup.Visible = False
    ''                End If

    ''                Dim lnkMessagesAll As ASPxHyperLink = MessagesPopup.FindControl("lnkMessagesAll")
    ''                If (lnkMessagesAll IsNot Nothing) Then
    ''                    lnkMessagesAll.Text = "<span>" & CurrentPageData.GetCustomString("lnkMessagesAll") & "</span>"
    ''                    lnkMessagesAll.NavigateUrl = "javascript:void(0);"
    ''                    lnkMessagesAll.CssClass = lnkMessagesAll.CssClass.Replace(" limited", "") & " limited"
    ''                    lnkMessagesAll.ClientSideEvents.Click = ""
    ''                End If

    ''            Else

    ''                If (dt.Rows(0)("NewMessages") > 0) Then
    ''                    btnMessages.Text = textPattern.Replace("#TEXT#", btnMessages.Text).Replace("#COUNT#", dt.Rows(0)("NewMessages"))
    ''                    btnMessages.NavigateUrl = "javascript:void(0);"
    ''                    MessagesPopup.Visible = True

    ''                    'Dim lnkMessagesAll As ASPxHyperLink = WinksPopup.FindControl("lnkMessagesAll")
    ''                    'If (lnkMessagesAll IsNot Nothing) Then
    ''                    '    lnkMessagesAll.ClientSideEvents.Click = "function(s,e){ShowLoading();}"
    ''                    'End If
    ''                Else
    ''                    btnMessages.Attributes.Add("onclick", "ShowLoading();")
    ''                    btnMessages.NavigateUrl = "~/Members/Messages.aspx"
    ''                    MessagesPopup.Visible = False
    ''                End If

    ''            End If

    ''            'btnUrl = btnMessages.NavigateUrl.TrimStart("~"c).ToUpper()
    ''            'If (btnUrl.StartsWith(Request.Url.PathAndQuery.ToUpper())) Then
    ''            '    btnMessages.CssClass = btnMessages.CssClass & " selectedBtn"
    ''            'Else
    ''            '    btnMessages.CssClass = btnMessages.CssClass.Replace(" selectedBtn", "")
    ''            'End If




    ''            Dim AvailableCredits As Integer

    ''            'sqlProc = "EXEC [CustomerAvailableCredits] @CustomerId = " & MasterProfileId
    ''            'dt = DataHelpers.GetDataTable(sqlProc)
    ''            'If (dt.Rows(0)("CreditsRecordsCount") > 0) Then

    ''            Dim __CustomerAvailableCredits As clsCustomerAvailableCredits = BasePage.GetCustomerAvailableCredits(MasterProfileId)
    ''            If (__CustomerAvailableCredits.CreditsRecordsCount > 0) Then

    ''                AvailableCredits = __CustomerAvailableCredits.AvailableCredits
    ''                lnkCredits.Visible = True

    ''                lnkCredits.Text = CurrentPageData.GetCustomString("lnkCredits")
    ''                If (__CustomerAvailableCredits.AvailableCredits > 0) Then
    ''                    lnkCredits.Text = textPattern.Replace("#TEXT#", lnkCredits.Text).Replace("#COUNT#", __CustomerAvailableCredits.AvailableCredits)
    ''                End If
    ''                lnkCredits.Text = lnkCredits.Text.Replace(" class=""jewelCount""", "")
    ''                lnkCredits.Attributes.Add("onclick", "ShowLoading();")
    ''            Else
    ''                lnkCredits.Visible = False
    ''            End If


    ''            'btnUrl = lnkCredits.NavigateUrl.TrimStart("~"c).ToUpper()
    ''            'If (btnUrl.StartsWith(Request.Url.PathAndQuery.ToUpper())) Then
    ''            '    lnkCredits.CssClass = lnkCredits.CssClass & " selectedBtn"
    ''            'Else
    ''            '    lnkCredits.CssClass = lnkCredits.CssClass.Replace(" selectedBtn", "")
    ''            'End If




    ''        Catch ex As Exception
    ''            WebErrorMessageBox(Me, ex, "")
    ''        Finally
    ''            clsLogger.InsertLog("Members-Master-->BindMembersList", MasterProfileId, (DateTime.UtcNow - __logdate).TotalMilliseconds)
    ''        End Try

    ''    End Sub


    'Public Sub ShowUserMap()
    '    Dim dc As New CMSDBDataContext(System.Configuration.ConfigurationManager.ConnectionStrings("AppDBconnectionString").ConnectionString)
    '    Try

    '        If (Session("ProfileId") Is Nothing OrElse Session("ProfileId") = 0) Then Return


    '        Dim MasterProfileId As Integer = Session("ProfileId")
    '        Dim profZip = (From itm In dc.EUS_Profiles
    '                        Where (itm.ProfileID = MasterProfileId AndAlso itm.IsMaster = True) OrElse _
    '                        (itm.ProfileID = MasterProfileId AndAlso itm.MirrorProfileID = 0 AndAlso itm.IsMaster = False)
    '                        Select itm.Zip, itm.Country, itm.Region, itm.City, itm.RegisterGEOInfos, itm.LastLoginGEOInfos).FirstOrDefault()


    '        Dim dt As DataTable = Nothing
    '        Dim zoom As Integer = 8
    '        Dim radius As Integer = 10
    '        If (profZip IsNot Nothing AndAlso Not String.IsNullOrEmpty(profZip.Zip)) Then

    '            'Dim country As String = Session("LAGID")
    '            'If (Not String.IsNullOrEmpty(profZip.Country)) Then country = profZip.Country
    '            dt = clsGeoHelper.GetGEOByZip(profZip.Country, profZip.Zip, Session("LAGID"))

    '        End If

    '        If (dt Is Nothing OrElse dt.Rows.Count = 0) Then
    '            dt = clsGeoHelper.GetCountryMinPostcodeDataTable(profZip.Country, profZip.Region, profZip.City, Session("LAGID"))
    '            radius = 120
    '        End If

    '        If (dt.Rows.Count > 0) Then
    '            Dim lat As Double = dt.Rows(0)("latitude")
    '            Dim lng As Double = dt.Rows(0)("longitude")
    '            ShowUserMap(lat, lng, radius, zoom)
    '        End If

    '    Catch ex As Exception
    '        WebErrorMessageBox(Me, ex, "")
    '    Finally
    '        dc.Dispose()
    '    End Try

    'End Sub

    'Public Sub ShowUserMap(lat As Double, lng As Double, radius As Integer, Optional zoom As Integer = 8)

    '    Try
    '        Dim mapPath As String = System.Web.VirtualPathUtility.ToAbsolute("~/map.aspx?lat=" & lat & "&lng=" & lng & "&radius=" & radius & "&zoom=" & zoom)

    '        divMapContainer.Visible = True
    '        'ifrUserMap.Attributes.Add("src", mapPath)
    '        ifrUserMap_src = mapPath
    '    Catch ex As Exception
    '        WebErrorMessageBox(Me, ex, "")
    '    End Try

    'End Sub



    'Protected Sub btnEnglish_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles btnEnglish.Click
    '    cmbLag.Items(GetIndexFromLagID("US")).Selected = True
    '    cmbLag_SelectedIndexChanged(cmbLag, New EventArgs())
    'End Sub

    'Protected Sub btnGerman_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles btnGerman.Click
    '    cmbLag.Items(GetIndexFromLagID("DE")).Selected = True
    '    cmbLag_SelectedIndexChanged(cmbLag, New EventArgs())
    'End Sub

    'Protected Sub btnGreek_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles btnGreek.Click
    '    cmbLag.Items(GetIndexFromLagID("GR")).Selected = True
    '    cmbLag_SelectedIndexChanged(cmbLag, New EventArgs())
    'End Sub

    'Protected Sub btnHungarian_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles btnHungarian.Click
    '    cmbLag.Items(GetIndexFromLagID("HU")).Selected = True
    '    cmbLag_SelectedIndexChanged(cmbLag, New EventArgs())
    'End Sub

    Private Sub SetLanguageLinks()
        Try

            If (clsLanguageHelper.HasCountryConfig(Session("GEO_COUNTRY_CODE"))) Then
                Dim cc As clsLanguageHelper.CountryConfig = clsLanguageHelper.GetCountryConfig(Session("GEO_COUNTRY_CODE"))
                For itms = cmbLag.Items.Count - 1 To 0 Step -1
                    If (cc.ShowLangs.Contains(cmbLag.Items(itms).Value)) Then
                        Continue For
                    Else
                        cmbLag.Items.RemoveAt(itms)
                    End If
                Next
            End If


        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub

    Protected Sub LoginStatus1_LoggingOut(sender As Object, e As System.Web.UI.WebControls.LoginCancelEventArgs) 'Handles LoginStatus1.LoggingOut
        Try

            Dim url As String = ResolveUrl("~/login.aspx")

            Dim currentHost As String = UrlUtils.GetCurrentHostURL(Request.Url)
            Dim _uri As System.Uri = UrlUtils.GetFullURI(currentHost, url)
            url = LanguageHelper.GetNewLAGUrl(Request.Url, GetLag(), _uri.ToString())

            FormsAuthentication.SignOut()
            clsCurrentContext.ClearSession(Session)

            Response.Redirect(url)

        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Protected Sub cbpTopBar_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cbpTopBar.Callback
        Try
            Session("IsAutorefresh") = True
            mbl.RefreshControl()
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub
End Class
﻿Imports System.Drawing
Imports DevExpress.Web.ASPxEditors
Imports Dating.Server.Core.DLL
Imports Dating.Server.Core.DLL.clsUserDoes
Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Datasets.DLL.DSMembers

Public Class ConversationOlder
    Inherits MessagesBasePage


#Region "Props"

    Public Property ScrollToElem As String
    Public Property CurrentLoginName As String
    Public Property WarningPopup_HeaderText As String

   


    Protected Property OfferId As Integer
        Get
            Return ViewState("OfferId")
        End Get
        Set(value As Integer)
            ViewState("OfferId") = value
        End Set
    End Property


    Protected Property UserIdInView As Integer
        Get
            Return ViewState("UserIdInView")
        End Get
        Set(value As Integer)
            ViewState("UserIdInView") = value
        End Set
    End Property


    Protected Property SubjectInView As String
        Get
            Return ViewState("SubjectInView")
        End Get
        Set(value As String)
            ViewState("SubjectInView") = value
        End Set
    End Property


    Protected Property MessageIdInView As Long
        Get
            Return ViewState("MessageIdInView")
        End Get
        Set(value As Long)
            ViewState("MessageIdInView") = value
        End Set
    End Property

    ' used with delete message action
    Protected Property MessageIdListInView As List(Of Long)
        Get
            Return ViewState("MessageIdListInView")
        End Get
        Set(value As List(Of Long))
            ViewState("MessageIdListInView") = value
        End Set
    End Property


    Protected Property BackUrl As String
        Get
            Return ViewState("BackUrl")
        End Get
        Set(value As String)
            ViewState("BackUrl") = value
        End Set
    End Property


    Protected Property SendAnotherMessage As Boolean
        Get
            Return ViewState("SendAnotherMessage")
        End Get
        Set(value As Boolean)
            ViewState("SendAnotherMessage") = value
        End Set
    End Property


    Protected ReadOnly Property FreeMessagesOnView As Integer
        Get
            If (ViewState("FreeMessagesOnView") Is Nothing) Then
                Dim freeMessages As Integer = 0
                If (Not String.IsNullOrEmpty(Request.QueryString("freeMessages")) AndAlso
                    Integer.TryParse(Request.QueryString("freeMessages"), freeMessages)) Then
                    ' do nothing
                End If
                ViewState("FreeMessagesOnView") = freeMessages
            End If
            Return ViewState("FreeMessagesOnView")
        End Get
    End Property


    ''' <summary>
    ''' Recipient id.
    ''' Used when current user opens it's sent message and wants to send another one.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected Property SendAnotherMessageToRecipient As Integer
        Get
            Return ViewState("SendAnotherMessageToRecipient")
        End Get
        Set(value As Integer)
            ViewState("SendAnotherMessageToRecipient") = value
        End Set
    End Property

    Protected Property StopExecution As Boolean

#End Region


    Protected Overrides Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            If (Me.SessionVariables.MemberData.Status = ProfileStatusEnum.LIMITED) Then
                Response.Write("{""redirect"":""" & ResolveUrl("~/Members/Default.aspx") & """}")
                HttpContext.Current.ApplicationInstance.CompleteRequest()
                StopExecution = True
            End If
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try

            If (StopExecution) Then Return


            '    Dim viewLoaded As Boolean = False

            '   Dim unlockMessage As Boolean = MyBase.UnlimitedMsgID > 0 AndAlso Not String.IsNullOrEmpty(Request.QueryString("vw"))
            Dim viewMessage As Boolean = Not MyBase.UnlimitedMsgID > 0 AndAlso (Not String.IsNullOrEmpty(Request.QueryString("vw")) OrElse Not String.IsNullOrEmpty(Request.QueryString("msg")))
            '    Dim sendMessageTo As Boolean = Not String.IsNullOrEmpty(Request.QueryString("p"))
            '   Dim tabChanged As Boolean = Not String.IsNullOrEmpty(Request.QueryString("t"))

            If (Not Page.IsPostBack) Then
                Me.BackUrl = Request.Params("HTTP_REFERER")
                If (Not String.IsNullOrEmpty(Request.Params("offerid"))) Then Integer.TryParse(Request.Params("offerid"), Me.OfferId)
                LoadLAG()

                If (viewMessage) Then
                    '' load conversation for requested and current profiles
                    'LoadViews()

                    If (Not String.IsNullOrEmpty(Request.QueryString("vw"))) Then
                        Dim otherLoginName = Request.QueryString("vw")
                        Using cmdb As CMSDBDataContext = Me.GetCMSDBDataContext
                            Dim memberReceivingMessage As EUS_Profile = DataHelpers.GetEUS_ProfileMasterByLoginName(cmdb, otherLoginName, ProfileStatusEnum.Approved, ProfileStatusEnum.LIMITED)
                            If (memberReceivingMessage IsNot Nothing) Then
                                CurrentLoginName = memberReceivingMessage.LoginName

                                '    Dim HasCommunication As Boolean = GetHasCommunicationDict(memberReceivingMessage.ProfileID)



                                Dim message As EUS_Message = clsUserDoes.GetLastMessageForProfiles(Me.MasterProfileId, memberReceivingMessage.ProfileID)
                                If (message IsNot Nothing) Then
                                    Me.UserIdInView = memberReceivingMessage.ProfileID
                                    Me.MessageIdInView = message.EUS_MessageID
                                Else
                                    ' there are no messages among users
                                    Me.UserIdInView = memberReceivingMessage.ProfileID
                                    Me.MessageIdInView = 0
                                End If

                                ExecCmd("VIEWMSG", Me.MessageIdInView)
                                '   viewLoaded = True

                            End If
                        End Using
                    End If



                    Dim messageID As Integer
                    If (Not String.IsNullOrEmpty(Request.QueryString("msg")) AndAlso Integer.TryParse(Request.QueryString("msg"), messageID)) Then
                        If (messageID > 0) Then
                            Dim message As EUS_Message = clsUserDoes.GetMessage(messageID)

                            If (message Is Nothing OrElse message.ProfileIDOwner <> Me.MasterProfileId) Then
                                ' message not found or message is owned by another user

                                '  viewLoaded = True

                            ElseIf (message.ProfileIDOwner = Me.MasterProfileId) Then
                                If (message.FromProfileID = Me.MasterProfileId) Then
                                    Me.UserIdInView = message.ToProfileID
                                Else
                                    Me.UserIdInView = message.FromProfileID
                                End If
                                Me.MessageIdInView = message.EUS_MessageID
                                ExecCmd("VIEWMSG", Me.MessageIdInView)
                                '  viewLoaded = True

                            End If
                        End If
                    End If

                End If

            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


    Private Sub BindMessagesConversation(messageId As Integer)
        Dim num As Integer = 0
        If (Not String.IsNullOrEmpty(Request("rowFrom")) AndAlso Integer.TryParse(Request("rowFrom"), num)) Then
            convCtl.RowNumberMin = num
        Else
            convCtl.RowNumberMin = 0
        End If
        If (Not String.IsNullOrEmpty(Request("rowTo")) AndAlso Integer.TryParse(Request("rowTo"), num)) Then
            convCtl.RowNumberMax = num
        Else
            convCtl.RowNumberMax = 0
        End If
        If (Not String.IsNullOrEmpty(Request("olderMsgIdAvailable")) AndAlso Integer.TryParse(Request("olderMsgIdAvailable"), num)) Then
            convCtl.OlderMsgIdAvailable = num
        End If

        Dim LoadAllMessages As Boolean = False
        Dim LoadUntilDate As DateTime? = Nothing
        If (Not String.IsNullOrEmpty(Request.QueryString("dateUntil"))) Then
            LoadAllMessages = (Request.QueryString("dateUntil").ToLower() = "all")
            Dim dateUntil As Integer = 0
            Integer.TryParse(Request.QueryString("dateUntil"), dateUntil)
            If (dateUntil > 0) Then
                LoadUntilDate = Date.UtcNow.AddDays(-dateUntil)
            End If
        End If


        convCtl.OfferId = Me.OfferId
        convCtl.UserIdInView = Me.UserIdInView
        convCtl.SubjectInView = Me.SubjectInView
        convCtl.MessageIdInView = Me.MessageIdInView
        convCtl.MessageIdListInView = Me.MessageIdListInView
        convCtl.SendAnotherMessageToRecipient = Me.SendAnotherMessageToRecipient
        convCtl.FreeMessagesOnView = Me.FreeMessagesOnView
        convCtl.LoadUntilDate = LoadUntilDate
        convCtl.LoadAllMessages = LoadAllMessages


        convCtl.LoadUsersConversation(messageId)

        pnlMoreMsgs.Visible = convCtl.IsThereMoreMessages

    End Sub

    Private Sub LoadUsersConversation(messageId As Long)
        Me.MessageIdInView = messageId

        If (messageId > 0) Then

            BindMessagesConversation(messageId)

        Else

            If (Me.UserIdInView > 0) Then
                convCtl.Visible = False
            End If

        End If


        convCtl.Visible = (convCtl.Count > 0)
    End Sub



    Protected Sub LoadLAG()
        Try
            'lnkMoreMsgs.Text = Me.CurrentPageData.GetCustomString("btn.Load.More.Messages")
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Private Sub ExecCmd(CommandName As String, CommandArgument As String)
        Try

            If (CommandName = "DELETEMSG") Then


                Dim MsgID As Integer
                Integer.TryParse(CommandArgument, MsgID)
                If (clsNullable.NullTo(Me.GetCurrentProfile().ReferrerParentId) > 0) Then
                    clsUserDoes.DeleteMessage(Me.MasterProfileId, MsgID, True)
                Else
                    clsUserDoes.DeleteMessage(Me.MasterProfileId, MsgID, False)
                End If
                Me.MessageIdListInView.Remove(MsgID)
                If (Me.MessageIdListInView.Count > 0) Then
                    MsgID = Me.MessageIdListInView(0)
                    LoadUsersConversation(MsgID)
                Else
                    'MessagesView = MessagesViewEnum.INBOX
                    'Me.MessageIdInView = 0
                    'LoadMessagesList()
                    LoadUsersConversation(0)
                    convCtl.Visible = (convCtl.Count > 0)
                End If
            End If


            If (CommandName = "VIEWMSG") Then

                Dim MsgID As Integer
                Integer.TryParse(CommandArgument, MsgID)
                LoadUsersConversation(MsgID)
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try


        If (CommandName = "REJECT_DELETECONV" OrElse CommandName = "REJECT_BLOCK") Then


            ' commands of Dates control, copied from dates.aspx
            Dim success = False
            Dim showNoPhotoNote = False

            Try
                Dim currentUserHasPhotos As Boolean = clsUserDoes.HasPhotos(Me.MasterProfileId, Me.MirrorProfileId)
                If (currentUserHasPhotos) Then

                    ''''''''''''''''''''''
                    'photo aware actions
                    ''''''''''''''''''''''
                    If (CommandName = "REJECT_DELETECONV") Then

                        ''''''''''''''''''''''
                        'photo aware action
                        ''''''''''''''''''''''

                        Dim OtherMemberProfileID As Integer
                        Integer.TryParse(CommandArgument.ToString(), OtherMemberProfileID)

                        If (OtherMemberProfileID > 0) Then
                            clsMessagesHelper.DeleteMessagesFromMemberInView(Me.MasterProfileId, Me.MasterProfileId, OtherMemberProfileID, MessagesViewEnum.SENT, (IsReferrer = True))
                            clsMessagesHelper.DeleteMessagesFromMemberInView(Me.MasterProfileId, OtherMemberProfileID, Me.MasterProfileId, MessagesViewEnum.INBOX, (IsReferrer = True))
                            clsMessagesHelper.DeleteMessagesFromMemberInView(Me.MasterProfileId, OtherMemberProfileID, Me.MasterProfileId, MessagesViewEnum.NEWMESSAGES, (IsReferrer = True))
                            clsMessagesHelper.DeleteMessagesFromMemberInView(Me.MasterProfileId, OtherMemberProfileID, Me.MasterProfileId, MessagesViewEnum.TRASH, (IsReferrer = True))

                            clsUserDoes.MarkDateOfferAsViewed(OtherMemberProfileID, Me.MasterProfileId)
                        End If

                        success = True

                    ElseIf (CommandName = "REJECT_BLOCK") Then

                        Dim OtherMemberProfileID As Integer
                        Integer.TryParse(CommandArgument.ToString(), OtherMemberProfileID)

                        If (OtherMemberProfileID > 0) Then
                            clsUserDoes.MarkAsBlocked(OtherMemberProfileID, Me.MasterProfileId)
                            clsUserDoes.MarkDateOfferAsViewed(OtherMemberProfileID, Me.MasterProfileId)
                        End If

                        success = True

                    End If

                Else
                    showNoPhotoNote = True
                End If

            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try



            Try

                If (showNoPhotoNote) Then
                 
                ElseIf (success) Then
                    LoadUsersConversation(Me.MessageIdInView)
                End If
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try

        End If


    End Sub




    Protected Function IsMessageHidden(DataItem As DataRowView) As String
        Dim class1 As String = ""
        Dim HasCommunication As Boolean = GetHasCommunicationDict(DataItem("ProfileID"))
        If (Not HasCommunication AndAlso DataItem("StatusID") = 0 AndAlso Me.IsMale) Then
            class1 = "closed"
        End If

        Return class1
    End Function

    Private Property HasCommunicationDict As Dictionary(Of Integer, Boolean)
    Protected Function GetHasCommunicationDict(otherProfileID As Integer, Optional force As Boolean = False) As Boolean
        Dim HasCommunication As Boolean

        If (HasCommunicationDict Is Nothing) Then HasCommunicationDict = New Dictionary(Of Integer, Boolean)
        If (Not HasCommunicationDict.ContainsKey(otherProfileID) OrElse force) Then

            HasCommunication = GetHasCommunication(otherProfileID)
            If (force) Then
                HasCommunicationDict(otherProfileID) = HasCommunication
            Else
                HasCommunicationDict.Add(otherProfileID, HasCommunication)
            End If

        ElseIf (HasCommunicationDict.ContainsKey(otherProfileID)) Then

            HasCommunication = HasCommunicationDict(otherProfileID)

        End If

        Return HasCommunication
    End Function

End Class






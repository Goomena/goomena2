﻿<%@ Page Title="" Language="vb" AutoEventWireup="true" MasterPageFile="~/Members/Members2Inner.Master" 
    CodeBehind="defaultold.aspx.vb" Inherits="Dating.Server.Site.Web._default1" %>
<%@ Register src="../UserControls/UsersList.ascx" tagname="UsersList" tagprefix="uc2" %>
<%@ Register src="../UserControls/MemberLeftPanel.ascx" tagname="MemberLeftPanel" tagprefix="uc3" %>
<%@ Register assembly="DevExpress.Web.v13.1, Version=13.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxDataView" tagprefix="dx" %>
<%@ Register src="../UserControls/WhatIsIt.ascx" tagname="WhatIsIt" tagprefix="uc2" %>
<%--<%@ Register Src="~/UserControls/ucPopupNewTerms.ascx" TagPrefix="uc3" TagName="ucPopupNewTerms" %>--%>



<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .mobilebanner img { width: 689px; }
    </style>

    <script type="text/javascript">
        // <![CDATA[
        $(document).ready(configurePopup);

        function configurePopup() {
            $('.members-popup .dropdown-menu div, .members-popup .dropdown-menu h3').click(function (e) {
                e.stopPropagation();
            });

            $('.members-popup .dropdown-menu .popupSaveButton').click(function (e) {
                $(this).parent().parent().hide();
            });
            //if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
            //    $('.quick-links-big ').css('width', '699px');
            //    $('.quick-links-big ').css('height', '150px');
            //}
        }

        function showAjaxLoader(id) {
            $('#' + id).html("");

            if (id == "nearest") {

                $('#' + id).css("width", "680px");
                $('#' + id).css("height", "403px");
                $('#' + id).css("background", "url('//cdn.goomena.com/Images2/mbr2/loader.gif') no-repeat center #fff");
            }
            else if (id == "profilesYouHaveViewed") {
                $('#' + id).css("width", "680px");
                $('#' + id).css("height", "149px");
                $('#' + id).css("background", "url('//cdn.goomena.com/Images2/mbr2/loader.gif') no-repeat center #fff");
            }
        }
        // ]]>
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">

<div class="m_wrap" id="divUploadPhoto" runat="server" visible="false">
    <div id="ph-edit" class="no-photo">
        <div class="items_none items_hard">
            <div class="items_none_wrap">
                <div class="items_none_text">
                    <asp:Literal ID="msg_UploadPhotoText_ND" runat="server"></asp:Literal>
                    <div class="search_members_button_right"><asp:HyperLink ID="lnkUploadPhoto" runat="server" CssClass="btn btn-danger"></asp:HyperLink></div>
                </div>
            </div>
        </div>
    </div>
</div>

 <div class="welcome-msg" ID="hdrWelcome" runat="server">
    <div class="welcome_middle_icon"></div>
    <div class="inner-box">
    <asp:Literal ID="lblWelcome" runat="server" />
    </div>
</div>
 
    
                     
<h3 id="h3Dashboard" class="page-title">
    <asp:Literal ID="lblItemsName" runat="server" />
    <asp:LinkButton ID="lnkViewDescription" runat="server" CssClass="more_info2" OnClientClick="OnMoreInfoClick('[contentUrl]', this, '[headerText]');return false;" />
</h3>
    <div class="quick-links-big">
        <div id="first_column" class="big-list-container">
            <ul class="big-list">
                <li>
                    <dx:ASPxHyperLink ID="lnkSearch" runat="server"
                        Text="Search our members"
                        NavigateUrl="~/Members/Search.aspx"
                        EnableDefaultAppearance="false" EnableTheming="false"
                        EncodeHtml="False"
                        CssClass="lnkSearch">
                        <ClientSideEvents Click="ShowLoadingOnMenu" />
                    </dx:ASPxHyperLink>
                </li>
                <li>
                    <dx:ASPxHyperLink ID="lnkWhoViewedMe" runat="server" Text="Who Viewed Me"
                        NavigateUrl="~/Members/MyLists.aspx?vw=whoviewedme"
                        EnableDefaultAppearance="false" EnableTheming="false"
                        EncodeHtml="False"
                        CssClass="lnkWhoViewedMe">
                        <ClientSideEvents Click="ShowLoadingOnMenu" />
                    </dx:ASPxHyperLink>
                </li>
                <li>
                    <dx:ASPxHyperLink ID="lnkWhoFavoritedMe" runat="server" Text="Who Favorited Me"
                        NavigateUrl="~/Members/MyLists.aspx?vw=whofavoritedme"
                        EnableDefaultAppearance="false"
                        EnableTheming="false" EncodeHtml="False"
                        CssClass="lnkWhoFavoritedMe">
                        <ClientSideEvents Click="ShowLoadingOnMenu" />
                    </dx:ASPxHyperLink>
                </li>
            </ul>
        </div>

        <div id="second_column" class="big-list-container">
            <ul class="big-list">
                <li>
                    <dx:ASPxHyperLink ID="lnkMyFavoriteList" runat="server" Text="My Favorite List" 
                        NavigateUrl="~/Members/MyLists.aspx?vw=myfavoritelist" 
                        EnableDefaultAppearance="false"
                        EnableTheming="false" EncodeHtml="False"
                        CssClass="lnkMyFavoriteList">
                        <ClientSideEvents Click="ShowLoadingOnMenu" />
                    </dx:ASPxHyperLink>
                </li>
                <li>
                    <dx:ASPxHyperLink ID="lnkMyBlockedList" runat="server" Text="My Blocked List" 
                        NavigateUrl="~/Members/MyLists.aspx?vw=myblockedlist"
                        EnableDefaultAppearance="false"
                        EnableTheming="false" EncodeHtml="False"
                        CssClass="lnkMyBlockedList">
                        <ClientSideEvents Click="ShowLoadingOnMenu" />
                    </dx:ASPxHyperLink>
                </li>
                <li>
                    <dx:ASPxHyperLink ID="lnkMyViewedList" runat="server" Text="My Viewed List" 
                        NavigateUrl="~/Members/MyLists.aspx?vw=myviewedlist" 
                        EnableDefaultAppearance="false"
                        EnableTheming="false" EncodeHtml="False"
                        CssClass="lnkMyViewedList">
                        <ClientSideEvents Click="ShowLoadingOnMenu" />
                    </dx:ASPxHyperLink>
                </li>
            </ul>
        </div>

        <div id="third_column" class="big-list-container">
            <ul class="big-list">
                <li>
                    <dx:ASPxHyperLink ID="lnkAddPhotos" runat="server" Text="Photos" 
                        NavigateUrl="~/Members/Photos.aspx"
                        EnableDefaultAppearance="false"
                        EnableTheming="false" EncodeHtml="False"
                        CssClass="lnkAddPhotos">
                        <ClientSideEvents Click="ShowLoadingOnMenu" />
                    </dx:ASPxHyperLink>
                </li>
                <li>
                    <dx:ASPxHyperLink ID="lnkNotifications" runat="server" Text="Notifications" 
                        NavigateUrl="~/Members/settings.aspx?vw=notifications"
                        EnableDefaultAppearance="false" EnableTheming="false"
                        EncodeHtml="False"
                        CssClass="lnkNotifications">
                        <ClientSideEvents Click="ShowLoadingOnMenu" />
                    </dx:ASPxHyperLink>
                </li>
                <li>
                    <dx:ASPxHyperLink ID="lnkHowWorks" runat="server" Text="How it Works"
                        NavigateUrl="~/CmsPage.aspx?PageId=158"
                        EnableDefaultAppearance="false" EnableTheming="false" 
                        EncodeHtml="False"
                        CssClass="lnkHowWorks">
                        <ClientSideEvents Click="ShowLoadingOnMenu" />
                    </dx:ASPxHyperLink>
                </li>
            </ul>
        </div>
        <div class="clear"></div>
    </div>

    <asp:UpdatePanel ID="nearestMembersUpdatePanel" runat="server">
        <ContentTemplate>
            <div id="titleNearest" class="members-popup-wrapper">
                <div class="members-popup">
                    <asp:HyperLink ID="lnkMembersNear" runat="server" CssClass="btn btn-danger dropdown-toggle" data-toggle="dropdown" NavigateUrl="javascript:void(0);"></asp:HyperLink>
                    <div id="nearest-popup-contents" class="dropdown-menu">
                        <h3 class="h3Popup">
                            <asp:Label ID="h3NearestOptionsPopup" runat="server" Text="Viewing Preferences"></asp:Label></h3>

                        <div class="popup_content_container">
                            <asp:RadioButton ID="nearestOptionsNearestMembersRadioButton" runat="server" GroupName="nearestOptionsGroup" Checked="True" />
                            <label for="<%=nearestOptionsNearestMembersRadioButton.ClientID%>"><span></span></label>
                            <asp:Label ID="nearestOptionsNearestMembersLabel" runat="server" Text="Members Near You" CssClass="popupTextContent"></asp:Label>
                        </div>

                        <div class="popup_content_container _last">
                            <asp:RadioButton ID="nearestOptionsNewMembersRadioButton" runat="server" GroupName="nearestOptionsGroup" />
                            <label for="<%=nearestOptionsNewMembersRadioButton.ClientID%>"><span></span></label>
                            <asp:Label ID="nearestOptionsNewMembersLabel" runat="server" Text="New Members" CssClass="popupTextContent"></asp:Label>
                        </div>

                        <div class="popupSaveButtonContainer">
                            <asp:LinkButton ID="nearestPopupSaveButton" runat="server" Text="Save" CssClass="popupSaveButton" OnClientClick="javascript:showAjaxLoader('nearest');"/>
                        </div>
                    </div>
                </div>
            </div>

            <div id="nearest" class="d_block">

                <div class="m_wrap" id="divUpdateProfileData" runat="server">
                    <div class="clearboth padding">
                    </div>
                    <div class="items_none">
                        <asp:Literal ID="lblUpdateProfileData" runat="server"></asp:Literal>
                        <p>
                            <asp:HyperLink ID="lnkEditProfile" runat="server" CssClass="btn btn-danger" NavigateUrl="~/Members/Profile.aspx?do=edit"  onclick="ShowLoading();"></asp:HyperLink></p>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="d_winks_profiles">
                    <asp:ListView ID="lvNearest" runat="server">
                        <ItemTemplate>
                            <div class="d_big_photo" onclick='<%# MyBase.GetOnClickJS(Eval("LoginName").Tostring()) %>'>
                                <div class="pr-photo-116-noshad">
                                    <asp:HyperLink ID="lnkPhoto" runat="server"
                                        NavigateUrl='<%# System.Web.VirtualPathUtility.ToAbsolute("~/Members/Profile.aspx?p=") &  HttpUtility.UrlEncode(Eval("LoginName").Tostring())%>'
                                        CssClass="profile-picture"
                                        onClick="ShowLoading();">
                                        <asp:Image ID="imgPhoto" runat="server"
                                            ImageUrl='<%# Dating.Server.Site.Web.AppUtils.GetImage(Eval("ProfileID").Tostring(), Eval("FileName").Tostring(), Eval("GenderId").Tostring(), False, Me.IsHTTPS) %>'
                                            CssClass="round-img-116" />
                                    </asp:HyperLink>
                                </div>

                                <div class="text_content">
                                    <dx:ASPxHyperLink ID="lnkLogin" runat="server"
                                        Text='<%# Eval("LoginName") %>'
                                        NavigateUrl='<%# System.Web.VirtualPathUtility.ToAbsolute("~/Members/Profile.aspx?p=") & HttpUtility.UrlEncode(Eval("LoginName").Tostring()) %>'
                                        CssClass="login-name">
                                        <ClientSideEvents Click="ShowLoadingOnMenu" />
                                    </dx:ASPxHyperLink>

                                    <div id="divIsOnline" runat="server" class="online-indicator"
                                        visible='<%# (Eval("IsOnlineNow").Tostring()="True") %>'>
                                        <img src="//cdn.goomena.com/Images2/mbr2/online-cycle-small.png" alt="online" title="online" /><asp:Label
                                            ID="lblOnline" runat="server" Text="online"></asp:Label>
                                    </div>

                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:ListView>

                    <div class="clear"></div>
                    <div class="bottomLinkViewAll">
                        <asp:HyperLink ID="lnkMembersNearAll" runat="server" NavigateUrl="~/Members/Search.aspx?vw=NEAREST" onclick="ShowLoading();"></asp:HyperLink></div>
                </div>
            </div>
       </ContentTemplate>
    </asp:UpdatePanel>



<asp:UpdatePanel ID="profilesViewedUpdatePanel" runat="server">
        <ContentTemplate>
            <div id="titleProfilesViewewd" class="members-popup-wrapper">
                <div class="members-popup">
                    <asp:HyperLink ID="lnkProfilesYouHaveViewed" runat="server" CssClass="btn btn-danger dropdown-toggle" data-toggle="dropdown" NavigateUrl="javascript:void(0);"></asp:HyperLink>
                    <div id="profiles-popup-contents" class="dropdown-menu">
                        <h3 class="h3Popup">
                            <asp:Label ID="h3ProfilesYouHaveViewedOptionsPopup" runat="server" Text="Viewing Preferences"></asp:Label></h3>
                        <div class="popup_content_container">
                            <asp:RadioButton ID="profilesYouHaveViewedRadioButton" runat="server" GroupName="profilesYouHaveViewedOptionsGroup" Checked="True" />
                            <label for="<%=profilesYouHaveViewedRadioButton.ClientID%>"><span></span></label>
                            <asp:Label ID="profilesYouHaveViewedLabel" runat="server" Text="My Viewed List" CssClass="popupTextContent"></asp:Label>
                        </div>

                        <div class="popup_content_container">
                            <asp:RadioButton ID="whoViewedMeRadioButton" runat="server" GroupName="profilesYouHaveViewedOptionsGroup" />
                            <label for="<%=whoViewedMeRadioButton.ClientID%>"><span></span></label>
                            <asp:Label ID="whoViewedMeLabel" runat="server" Text="Who Viewed Your Profile" CssClass="popupTextContent"></asp:Label>
                        </div>

                        <div class="popup_content_container">
                            <asp:RadioButton ID="myFavoriteListRadioButton" runat="server" GroupName="profilesYouHaveViewedOptionsGroup" />
                            <label for="<%=myFavoriteListRadioButton.ClientID%>"><span></span></label>
                            <asp:Label ID="myFavoriteListLabel" runat="server" Text="My Favorites List" CssClass="popupTextContent"></asp:Label>
                        </div>

                        <div class="popup_content_container _last">
                            <asp:RadioButton ID="rblProfilesHaveBirthday" runat="server" GroupName="profilesYouHaveViewedOptionsGroup" />
                            <label for="<%=rblProfilesHaveBirthday.ClientID%>"><span></span></label>
                            <asp:Label ID="lblProfilesHaveBirthday" runat="server" Text="Who has birthday today" CssClass="popupTextContent"></asp:Label>
                        </div>


                        <div class="popupSaveButtonContainer">
                            <asp:LinkButton ID="profilesYouHaveViewedPopupSaveButton" runat="server" Text="Save" CssClass="popupSaveButton" OnClientClick="javascript:showAjaxLoader('profilesYouHaveViewed');" />
                        </div>
                    </div>
                </div>
            </div>

            <div class="d_block you-have-viewed" id="pnlProfilesYouHaveViewed" runat="server">
                <div class="d_winks_profiles" id="profilesYouHaveViewed">

                    <asp:ListView ID="lvWhomIViewed" runat="server" DataSourceID="sdsWhomIViewed">
                        <ItemTemplate>
                            <div class="d_big_photo" onclick='<%# MyBase.GetOnClickJS(Eval("LoginName").Tostring()) %>'>
                                <div class="pr-photo-78-noshad">
                                    <asp:HyperLink ID="lnkPhoto" runat="server"
                                        NavigateUrl='<%# System.Web.VirtualPathUtility.ToAbsolute("~/Members/Profile.aspx?p=") & HttpUtility.UrlEncode(Eval("LoginName").Tostring()) %>'
                                        CssClass="profile-picture"
                                        onClick="ShowLoading();">
                                        <asp:Image ID="imgPhoto" runat="server"
                                            ImageUrl='<%# Dating.Server.Site.Web.AppUtils.GetImage(Eval("ProfileID").Tostring(), Eval("FileName").Tostring(), Eval("GenderId").Tostring(), False, Me.IsHTTPS,Dating.Server.Core.DLL.PhotoSize.D150) %>'
                                            CssClass="round-img-78" />
                                    </asp:HyperLink>


                                </div>
                                <%--
 <%# MyBase.IsLastRowCount(Container.DataItem) %>"
<dx:ASPxHyperLink ID="lnkPhoto" runat="server" 
    EnableDefaultAppearance="False" EnableTheming="False" EncodeHtml="false" 
    ImageUrl='<%# Dating.Server.Site.Web.AppUtils.GetImage(Eval("CustomerID").Tostring(), Eval("FileName").Tostring(), Eval("GenderId").Tostring(), False, Me.IsHTTPS) %>' 
    NavigateUrl='<%# System.Web.VirtualPathUtility.ToAbsolute("~/Members/Profile.aspx?p=") &    HttpUtility.UrlEncode(Eval("LoginName").Tostring()) %>'>
        <ClientSideEvents Click="ShowLoadingOnMenu" />
</dx:ASPxHyperLink>
                                --%>
                                <div class="text_content">
                                    <dx:ASPxHyperLink ID="lnkLogin" runat="server"
                                        Text='<%# Eval("LoginName") %>'
                                        NavigateUrl='<%# System.Web.VirtualPathUtility.ToAbsolute("~/Members/Profile.aspx?p=") &   HttpUtility.UrlEncode(Eval("LoginName").Tostring()) %>'
                                        CssClass="login-name">
                                        <ClientSideEvents Click="ShowLoadingOnMenu" />
                                    </dx:ASPxHyperLink>

                                    <div class="photo_hover_box"></div>
                                </div>
                            </div>
                        </ItemTemplate>
                        <ItemSeparatorTemplate>
                            <div class="line-vert">&nbsp;</div>
                        </ItemSeparatorTemplate>
                    </asp:ListView>
                    <asp:SqlDataSource ID="sdsWhomIViewed" runat="server"
                        ConnectionString="<%$ ConnectionStrings:AppDBconnectionString %>" SelectCommand="exec [GetMembersDashboardWhomIViewed] @CurrentProfileId=@CurrentProfileId, @NumberOfRecordsToReturn=@NumberOfRecordsToReturn">
                        <SelectParameters>
                            <asp:SessionParameter DefaultValue="0" Name="CurrentProfileId"
                                SessionField="ProfileId" />
                            <asp:Parameter Name="NumberOfRecordsToReturn" Type="Int32" DefaultValue="5" />
                        </SelectParameters>
                    </asp:SqlDataSource>


                    <div class="clear"></div>
                    <div class="bottomLinkViewAll">
                        <asp:HyperLink ID="lnkAllProfilesYouHaveViewed" runat="server" NavigateUrl="~/Members/MyLists.aspx?vw=myviewedlist" onclick="ShowLoading();"></asp:HyperLink></div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

		
<div class="mobilebanner" ID="divMobileBanner" runat="server" visible="false">
    <a href="https://play.google.com/store/apps/details?id=com.goomena.app&hl=el" target="_blank"><img style="margin-left:auto;margin-right:auto;display:block;" src="//cdn.goomena.com/Images/MobileBanners/android-banner-member.png" id="iMobileBanner" runat="server" /></a>
</div>  

<div class="d_block">
<%--
<div style="padding:5px 5px 0;">
    <dx:ASPxLabel ID="lblDashboardTop1" runat="server" Text="lblDashboardTop1" 
        EncodeHtml="False">
    </dx:ASPxLabel>
</div>
<div class="d_announce" id="divLinks" runat="server" style="padding-top:0px;">
    <div class="clear">
    </div>
    <div style="padding:0 5px 5px;">
        <dx:ASPxLabel ID="lblDashboardTop2" runat="server" Text="lblDashboardTop2" 
            EncodeHtml="False">
        </dx:ASPxLabel>
	</div>
</div>

<div class="d_announce" id="divImportantAnnouncement" runat="server" visible="false">
	<div class="grid_4 alpha" style="width:auto;padding:0 0 20px 10px;">
        <dx:ASPxLabel ID="lblImportantAnnouncement" runat="server" Text="" 
            EncodeHtml="False">
        </dx:ASPxLabel>
		<div class="clearboth padding"></div>
	</div>

</div>
--%>

        <div class="dashboard_announce" style="">
	        <div class="d_bottom" style="padding-top:10px;">
                <dx:ASPxLabel ID="lblImportantAdvice" runat="server" Text="" 
                    EncodeHtml="False">
                </dx:ASPxLabel>
	        </div>
	        <div class="clear"></div>
        </div>

    </div>
    <div class="clear"></div>
<%--   <uc3:ucPopupNewTerms runat="server" ID="ucPopupNewTerms" />--%>

<uc2:whatisit ID="WhatIsIt1" runat="server" />

</asp:Content>



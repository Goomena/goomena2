﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Members/Members2Inner.Master" MaintainScrollPositionOnPostback="true"
 CodeBehind="Profile.aspx.vb" Inherits="Dating.Server.Site.Web.Profile" %>

<%@ Register src="../UserControls/MemberLeftPanel.ascx" tagname="MemberLeftPanel" tagprefix="uc3" %>
<%@ Register src="../UserControls/ProfileEdit.ascx" tagname="ProfileEdit" tagprefix="uc2" %>
<%@ Register src="../UserControls/ProfileView.ascx" tagname="ProfileView" tagprefix="uc3" %>
<%@ Register src="../UserControls/WhatIsIt.ascx" tagname="WhatIsIt" tagprefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<script type="text/javascript" src="//cdn.goomena.com/Scripts/fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>
	<script type="text/javascript" src="//cdn.goomena.com/Scripts/fancybox/source/jquery.fancybox.pack.js?v=2.1.4"></script>
	<script type="text/javascript" src="//cdn.goomena.com/Scripts/fancybox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
	<script type="text/javascript" src="//cdn.goomena.com/Scripts/fancybox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
	<script type="text/javascript" src="//cdn.goomena.com/Scripts/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.5"></script>
	<link rel="stylesheet" type="text/css" href="//cdn.goomena.com/Scripts/fancybox/source/jquery.fancybox.css?v=2.1.4" media="screen" />
	<link rel="stylesheet" type="text/css" href="//cdn.goomena.com/Scripts/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" />
	<link rel="stylesheet" type="text/css" href="//cdn.goomena.com/Scripts/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
<asp:MultiView ID="mvProfileMain" runat="server" ActiveViewIndex="1">
    <asp:View ID="vwProfileDeleted" runat="server">
    <div class="s_wrap">
        <div class="items_none no-results" id="divNoResultsChangeCriteria" runat="server">
            <asp:Literal ID="msg_divProfileNotFound" runat="server"/>
            <asp:Literal ID="msg_divProfileDeleted" runat="server"/>
            <asp:Literal ID="msg_divProfileLIMITED" runat="server"/>
	        <div class="clear"></div>
        </div>
    </div>
    <script type="text/javascript">
        setTimeout(function () {
            jsLoad('<%= ResolveUrl("~/Members/Default.aspx") %>');
        }, 10000);
    </script>
    </asp:View>
    <asp:View ID="vwProfileView" runat="server">
        <uc3:ProfileView ID="ProfileView1" runat="server" />
    </asp:View>
    <asp:View ID="vwProfileEdit" runat="server">
        <uc2:ProfileEdit ID="ProfileEdit1" runat="server" />
    </asp:View>
</asp:MultiView>

        <%--<div class="clear"></div>
        <div class="container_12" id="tabs-outter">
            <div class="grid_12 top_tabs">
                <div id="divProfileTabs" runat="server">
                    <ul>
                        <li runat="server" id="liView">
                            <asp:HyperLink ID="lnkViewProfileTab" runat="server" NavigateUrl="~/Members/Profile.aspx" onclick="ShowLoading();"><span></span>My Profile</asp:HyperLink>
                        </li>
                        <li runat="server" id="liEdit">
                            <asp:HyperLink ID="lnkEditProfileTab" runat="server" NavigateUrl="~/Members/Profile.aspx?do=edit" onclick="ShowLoading();"><span></span>Edit Profile</asp:HyperLink>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="clear"></div>
		<div class="container_12" id="content_top">


		</div>
		<div class="container_12" id="content">
        --%>
        <%--
        <div class="grid_3 top_sidebar" id="sidebar-outter">
            <div class="left_tab" id="left_tab" runat="server">
                <div class="top_info">
                </div>
                <div class="middle" id="search">
                    <div class="submit">
                        <h3><asp:Literal ID="lblItemsName" runat="server" /></h3>
                        <div class="clear"></div>
                        <asp:LinkButton ID="lnkViewDescription" runat="server" CssClass="more_info2" OnClientClick="OnMoreInfoClick('[contentUrl]', this, '[headerText]');return false;" />
                    </div>
                </div>
                <div class="bottom">
                    &nbsp;</div></div>
		    </div>

            <div class="grid_9 body">
                <div class="middle" id="content-outter">
                </div>
                <div class="bottom"></div>
			</div>
			<div class="clear"></div>
		</div>--%>
    <script type="text/javascript">
// <![CDATA[
        function loadFancybox() {
            $("a[rel=prof_group]").fancybox({
                'transitionIn': 'elastic',
                'transitionOut': 'elastic',
                'titlePosition': 'over',
                'overlayColor': '#000'
            });
        }

        function pageLoad(sender, args) {
            loadFancybox();
            if (typeof setPopupWindowUI !== 'undefined') setPopupWindowUI();
        }

        function closePopup(popupName) {
            try {
                top.window[popupName].Hide();
                top.window[popupName].DoHideWindow(0);
            }
            catch (e) { }
        }


// ]]> 
    </script>

	<uc2:whatisit ID="WhatIsIt1" runat="server" />
    
</asp:Content>
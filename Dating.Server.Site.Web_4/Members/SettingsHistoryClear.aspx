﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Members/Members2Inner.Master"
    CodeBehind="SettingsHistoryClear.aspx.vb" Inherits="Dating.Server.Site.Web.SettingsHistoryClear" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Scripts/jquery.json-2.4.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.dateFormat-1.0.js" type="text/javascript"></script>
    <style type="text/css">
        body{background-image:none;}
        .steps{display:none;}
        a.steps-continue{width:112px;height:47px;background:url('//cdn.goomena.com/Images2/popup-history/conti-bg.png') no-repeat;color:#fff;font-size:14px;display:block;text-align:center;line-height:41px; cursor: pointer;}
        a.steps-continue:hover{background-position:100% 100%;}
        a.steps-cancel{width: 94px;height: 47px;background:url('//cdn.goomena.com/Images2/popup-history/can-bg1.png') no-repeat scroll 0 0;color:#fff;font-size:14px;display:block;text-align:center;line-height:41px; cursor: pointer;}
        a.steps-cancel:hover{background-position:100% 100%;}
        div.history-form-buttons{position:absolute;bottom:10px;left:10px;right:10px;}
        div.history-form-buttons a.steps-cancel{position:absolute;bottom:0px;left:0px;font-size:16px;}
        div.history-form-buttons a.steps-continue{position:absolute;bottom:0px;right:0px;font-size:16px;}

        #history_clear_steps{position: relative;height:320px;}
        .step-1
        {
            padding-top: 30px;
        }

        .step-1 h3{font-weight:normal;font-size:21px;}
        .step-2{padding-top:70px;padding-left:10px;}
        .step-2 h3{font-weight:normal;font-size:21px;margin-top: 5px;}
        .step-3 h3{font-weight:normal;font-size:21px;}
        .step-3{padding-top:10px;}
        .step-4{padding-top:50px;}
        .step-4 h3{font-weight:normal;font-size:21px;}
        .step-5{}
        .step-5 .dxeEditArea.dxeEditAreaSys  {height:20px;width:300px;padding:7px;font-size:16px;}
        .step-5 .left-img  {width:150px;text-align:center;padding-top:50px;}
        .step-5 .left-content  {width: 350px;padding-top:60px;}
        .step-5 .left-content.error{width: 350px;padding-top:30px;}
        .step-5 .left-content  .text-watermark{font-size:16px;display:none;padding:7px 0 0 10px;position:absolute;left:0px;right:34px;top:10px;bottom:0px;font-style:italic;color:#9A9A9A}
        .step-5 .left-content  .text-watermark span{display:block;}
        .step-6{padding-top:50px;text-align:center;}
        .step-7{padding-top:50px;}
    </style>
    <script type="text/javascript">
        function showWaterMark5(s, e) {
            if (stringIsEmpty($.trim(s.GetValue()))) {
                $('.text-watermark:hidden', '.step-5').show();
            }
        }
        function hideWaterMark5(s, e) {
                $('.text-watermark', '.step-5').hide();
        }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
<div id="history_clear_steps">


        <asp:Panel ID="pnlConfirm1" runat="server" CssClass="steps step-1">
            <div style="width:130px;text-align:center; display: inline-block;">
                <img src="~/Images2/popup-history/trush.png" runat="server" id="iTrush1" />
            </div>
            <div style="width: 65%; display: inline-block;">
                <div style="font-size:21px"><asp:Label ID="lblTitle1" runat="server" /></div>
                <div style="font-size:16px;line-height:25px;"><asp:Label ID="lblStepConfirmDelete1" runat="server" /></div>
            </div>
            <div class="clear"></div>
            <div class="history-form-buttons">
                <a ID="lnkCancelStep1" runat="server" class="steps-cancel">Cancel</a>
                <a ID="lnkContinueStep1" runat="server" class="steps-continue">Continue</a>
            </div>
        </asp:Panel>


        <asp:Panel ID="pnlConfirm2" runat="server" CssClass="steps step-2">
            <div class="lfloat" style="width:150px;text-align:center;">
                <img src="~/Images2/popup-history/warn.png" runat="server" id="iWarn1" />
            </div>
            <div class="lfloat" style="width: 350px;">
                <div style="font-size:21px"><asp:Label ID="lblTitle2" runat="server" /></div>
                <div style="font-size:16px;line-height:25px;"><asp:Label ID="lblStepConfirmDelete2" runat="server" /></div>
            </div>
            <div class="clear"></div>
            <div class="history-form-buttons">
                <a ID="lnkCancelStep2" runat="server" class="steps-cancel">Cancel</a>
                <a ID="lnkContinueStep2" runat="server" class="steps-continue">Continue</a>
            </div>
        </asp:Panel>



        <asp:Panel ID="pnlConfirm3" runat="server" CssClass="steps step-3">
            <div style="padding-left:10px;padding-right:10px;padding-top:15px;">
                <div style="font-size:21px;text-align:center;"><asp:Label ID="lblTitle3" runat="server" /></div>
                <div style="font-size:16px;line-height:25px;text-align:center;"><asp:Label ID="lblStepConfirmDelete3" runat="server" /></div>
            </div>
            <div style="padding-top:15px;">
                <table style="margin:0 auto;">
                    <tr>
                        <td><asp:Label ID="lblStepConfirmDelete3Date" runat="server" Text="Date: " Font-Size="17px" /></td>
                        <td>&nbsp;</td>
                        <td><dx:ASPxDateEdit ID="dateTo" runat="server" DisplayFormatString="dd/MM/yyyy" 
                                    Width="191px" EditFormatString="dd/MM/yyyy" ClientInstanceName="dateTo" PopupHorizontalAlign="NotSet" PopupVerticalAlign="NotSet">
                                </dx:ASPxDateEdit></td>
                    </tr>
                </table>
            </div>
            <div class="history-form-buttons">
                <a ID="lnkCancelStep3" runat="server" class="steps-cancel">Cancel</a>
                <a ID="lnkContinueStep3" runat="server" class="steps-continue">Continue</a>
            </div>

        </asp:Panel>



        <asp:panel ID="pnlConfirm4" runat="server" CssClass="steps step-4">
            <div class="lfloat" style="width:150px;text-align:center;">
                <img src="~/Images2/popup-history/calendar.png" runat="server" id="iCalendar1" />
            </div>
            <div class="lfloat" style="width: 350px;padding-top:1px;">
                <div style="font-size:21px"><asp:Label ID="lblTitle4" runat="server" /></div>
                <div style="font-size:16px;line-height:25px;"><asp:Label ID="lblStepConfirmDeleteUntil4" runat="server" CssClass="step-4-replace" /></div>
            </div>
            <div class="clear"></div>
            <div class="history-form-buttons">
                <a ID="lnkCancelStep4" runat="server" class="steps-cancel">Cancel</a>
                <a ID="lnkContinueStep4" runat="server" class="steps-continue">Continue</a>
            </div>
        </asp:Panel>



        <asp:Panel ID="pnlConfirm5" runat="server" CssClass="steps step-5">
            <div class="lfloat left-img" style="" >
                <img src="~/Images2/popup-history/lock.png" runat="server" id="iLock1" />
            </div>
            <div class="lfloat left-content"  style="">
                <div id="pnlPasswordVerifyFailed" class="alert alert-danger" style="display:none;"><asp:Label ID="lblPasswordVerifyFailed" runat="server" Text=""/></div>
                <div style="font-size:21px"><asp:Label ID="lblTitle5" runat="server" /></div>
                <div style="font-size:16px;line-height:25px;"><asp:Label ID="lblStepConfirmDeleteInsertPassword" runat="server" /></div>
                <div style="padding-top:10px;position:relative;">
                    <dx:ASPxTextBox ID="txtPassword" runat="server" Password="True"
                        ClientInstanceName="txtPassword">
                        <ClientSideEvents LostFocus="showWaterMark5" GotFocus="hideWaterMark5" />
                    </dx:ASPxTextBox>
                    <div class="text-watermark"><asp:Label ID="lblWatermark5" runat="server" /></div>
                </div>           
            </div>
            <div class="clear"></div>
            <div class="history-form-buttons">
                <a ID="lnkCancelStep5" runat="server" class="steps-cancel">Cancel</a>
                <a ID="lnkContinueStep5" runat="server" class="steps-continue">Continue</a>
            </div>           
        </asp:Panel>



        
        <asp:Panel ID="pnlConfirm6" runat="server" CssClass="steps step-6">
            <div style="font-size:21px"><asp:Label ID="lblTitle6" runat="server" /></div>
            <div style="font-size:16px;line-height:25px;"><asp:Label ID="lblStepConfirmDelete6" runat="server" /></div>
            <div style="padding-top:15px;text-align:center;">
                <img src="//cdn.goomena.com/images2/GOO-Loading.gif" runat="server" id="iGOO1" />
            </div>
        </asp:Panel>

       

        <asp:Panel ID="pnlConfirm7" runat="server" CssClass="steps step-7">
            <div class="lfloat" style="width:150px;text-align:center;">
                <img src="~/Images2/popup-history/ok.png" runat="server" id="iOk1" />
                <img src="~/Images/oops.png" runat="server" id="iFail1" width="79" style="display:none;" />
            </div>
            <div class="lfloat" style="width: 350px;">
                <div style="font-size:21px"><asp:Label ID="lblTitle7" runat="server" /></div>
                <div style="font-size:16px;line-height:25px;"><asp:Label ID="lblActionResults" runat="server" /></div>
            </div>
            <div class="clear"></div>
            <div class="history-form-buttons">
                <a ID="lnkContinueStep7" runat="server" class="steps-continue">Continue</a>
            </div>
        </asp:Panel>
        
        
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cntBodyBottom" runat="server">

    <script type="text/javascript">

        function showFirstStep() {
            var css = '.step-1';
            $('.steps', '#history_clear_steps').hide();
            $(css, '#history_clear_steps').show();

            var tomorrow = new Date();
            tomorrow.setDate(tomorrow.getDate() + 1);
            
            dateTo.SetDate(tomorrow);
            $('.steps-continue', '#history_clear_steps ' + css).click(function () {
                showNextStep(2)
            });
            $('.steps-cancel', '#history_clear_steps ' + css).click(function () {
                closeWindow();
            });
        }


        function showNextStep(step) {
            //HideLoading();
            var css = '.step-' + step;
            $('.steps', '#history_clear_steps').hide();
            $(css, '#history_clear_steps').show();

            $('.steps-continue', '#history_clear_steps ' + css).click(function () {
                if (step == 5) 
                    verifyPassword(txtPassword.GetText());
                else if (step == 7) 
                    closeWindow();
                else 
                    showNextStep(step + 1)
            });
            $('.steps-cancel', '#history_clear_steps ' + css).click(function () {
                closeWindow();
            });

            if (step == 3) {
                GetDateUntil();
            }
            else if (step == 4) {
                var html = $('.step-4-replace', '.step-4').html()
                //var date = GetDateUntil()
                //var fDate = ($.format.date(date, "dd, MMM yyyy"))
                var fDate = GetDateUntilFormatted(); 
                html = html.replace("[DATE_UNTIL]", fDate);
                $('.step-4-replace', '.step-4').html(html);
            }
            else if (step == 5) {
                txtPassword.SetValue('');
                showWaterMark5(txtPassword, null);
                if (!_text_watermarkHandlers_Set) {
                    var inp = $('input.dxeEditArea.dxeEditAreaSys', '.step-5');
                    $('.text-watermark', '.step-5').click(function (event) {
                        $('.text-watermark', '.step-5').hide();
                        inp.focus();
                        event.preventDefault()
                    });
                    $('.text-watermark span', '.step-5').show().click(function (event) {
                        $('.text-watermark', '.step-5').hide();
                        inp.focus();
                        event.preventDefault()
                    });
                    _text_watermarkHandlers_Set = true;
                }
            }
            else if (step == 6) {
                performHistoryClear();
            }
        }
        var _text_watermarkHandlers_Set = false;
        function closeWindow() {
            closePopup(popupName);
        }

        var IsPerformHistoryClear = false;
        function performHistoryClear() {
            if (IsPerformHistoryClear) {
                showNextStep(7);
                return;
            }
            IsPerformHistoryClear = true;

            try {
                var param = $.toJSON({ dateUntil: GetDateUntilStr() });
                $.ajax({
                    type: "POST",
                    url: "SettingsHistoryClear.aspx/PerformHistoryClear",
                    data: param,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: true,
                    error: function (ar1, ar2, ar3) {
                        try {
                            HideLoading();
                            var err = ''
                            try {
                                var err1;
                                err = ar1.responseText;
                                err = 'err1=' + ar1.responseText;
                                 eval(err);
                                err = err1["Message"];
                            } catch (e) { }
                            if (err == '')
                                try { err = ar1.statusText } catch (e) { }
                            if (err == '')
                                try { err = ar3 } catch (e) { }
                            if (err == '')
                                try { err = "Sorry, serious error occured!" } catch (e) { }

                            $('#<%= iOk1.ClientID %>').hide()
                            $('#<%= iFail1.ClientID %>').show()
                            $('#<%= lblActionResults.ClientID %>').html(err)
                            showNextStep(7)
                        }
                        catch (e) { }
                    },
                    success: function (msg) {
                        try {
                            HideLoading();
                            var d = msg.d;
                            var fDate = GetDateUntilFormatted();
                            d = d.replace("[DATE_UNTIL]", fDate);
                            $('#<%= lblActionResults.ClientID %>').html(d)
                            showNextStep(7)
                            window.top.SetRefreshPage();
                        } catch (e) { }
                    },
                    beforeSend: function () {
                        //ShowLoading();
                    },
                    complete: function () {
                        HideLoading();
                    }
                });
            }
            catch (e) { }
        }



        function verifyPassword(pass) {
            try {
                HideLoading();
                var param = $.toJSON({ pass: pass });
                $.ajax({
                    type: "POST",
                    url: "SettingsHistoryClear.aspx/VerifyPassword",
                    data: param,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: true,
                    error: function (ar1, ar2, ar3) {
                        HideLoading();
                        $('#<%= lblPasswordVerifyFailed.ClientID %>').html(ar3)
                        $('#pnlPasswordVerifyFailed').show();
                        showNextStep(5)
                    },
                    success: function (msg) {
                        HideLoading();
                        if (msg.d == 'true') {
                            showNextStep(6);
                        }
                        else {
                            $('#<%= lblPasswordVerifyFailed.ClientID %>').html(msg.d);
                            $('#pnlPasswordVerifyFailed').show();
                            $('.step-5 .left-content').addClass('error');
                            $('.step-5 .left-img').addClass('opacity50');
                            showNextStep(5)
                        }
                    },
                    beforeSend: function () {
                        $('#pnlPasswordVerifyFailed').hide();
                        ShowLoading({delay: 1000});
                    },
                    complete: function () {
                        HideLoading();
                    }
                });
                
            }
            catch (e) { }
        }


        function GetDateUntil() {
            return dateTo.GetDate()
        }


        function GetDateUntilFormatted() {
            var result = '';
            try {
                var d = dateTo.GetDate()
                var DayOfMonthFullNameArr = ['<%= DayOfMonthFullName %>'];
                for (var cnt = 0; cnt < DayOfMonthFullNameArr.length; cnt++) {
                    DayOfMonthFullNameArr[cnt] = $.trim(DayOfMonthFullNameArr[cnt]);
                }
                var curr_date = d.getDate();
                var curr_month = d.getMonth(); //Months are zero based
                var curr_year = d.getFullYear();
                var curr_dateS = curr_date;
                if ('<%= GetLag() %>' == 'US') {
                    switch (curr_date) {
                        case 1: curr_dateS=curr_dateS + 'st';
                        case 2: curr_dateS=curr_dateS + 'nd';
                        case 3: curr_dateS=curr_dateS + 'rd';
                        default: curr_dateS=curr_dateS + 'th';
                    }
                }
                result = DayOfMonthFullNameArr[curr_month].replace('[DAY]', curr_dateS).replace('[YEAR]', curr_year);
            }
            catch (e) {
                var date = GetDateUntil()
                result = ($.format.date(date, "dd, MMMM yyyy"))
            }
            return result;
        }

        function GetDateUntilStr() {
            var d = dateTo.GetDate();
            var curr_date = d.getDate();
            var curr_month = d.getMonth() + 1; //Months are zero based
            var curr_year = d.getFullYear();
            var result = '';
            if (curr_date < 10) curr_date = '0' + curr_date
            if (curr_month < 10) curr_month = '0' + curr_month
            result = curr_date + '/' + curr_month + '/' + curr_year;
            return result;
        }


        var popupName = '<%= Request.QueryString("popup") %>';
        function resizePopup() {
            var $j = jQuery;
            var h = $('html').height();
            var w = $('html').width();
            try {
                //top.window[popupName].SetSize(w + 50, h + 77);
                //top.window[popupName].UpdatePosition();
                top.window[popupName].SetHeaderText($('#<%= lblTitle1.ClientID %>').text());
            }
            catch (e) { }
        }
        jQuery(document).ready(function ($) {
            setTimeout(resizePopup, 300);
            //showNextStep(7)
            showFirstStep();
        });
    </script>

</asp:Content>

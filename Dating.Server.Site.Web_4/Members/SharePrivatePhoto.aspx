﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Members/Members2Inner.Master" 
    CodeBehind="SharePrivatePhoto.aspx.vb" Inherits="Dating.Server.Site.Web.SharePrivatePhoto" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        body { background-image: none; }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">

<div style="width:660px;height:500px;position:relative;">
    <asp:Label ID="lblInfoTitle" runat="server"></asp:Label>
    <asp:Label ID="lblInfo" runat="server" CssClass="info-content">
    </asp:Label>
    <%--<div class="info-content-footer">
        <asp:Label ID="lblInfoFooter" runat="server">
        </asp:Label>
    </div>--%>

    <div class="<%= iif(Mybase.IsMale=true, "female", "male") %>">
    <div id="private-footer">
        <%--<div class="private-footer-start"></div>--%>
        <div class="private-footer-level1"></div>
        <div class="private-footer-level2"></div>
        <div class="private-footer-level3"></div>
        <div class="private-footer-level4"></div>
        <div class="private-footer-level5"></div>
        <div class="private-footer-level6"></div>
        <div class="private-footer-level7"></div>
        <div class="private-footer-level8"></div>
        <div class="private-footer-level9"></div>
        <div class="private-footer-level10"></div>
        <div id="private-footer-top-line"></div>
    </div>
    </div>

    
</div>
<%--

    <table cellspacing="0" cellpadding="0" style="margin:0 auto;" ID="tblSharePhotoLevels" runat="server">
        <tr class="women-img" ID="pnlSharePhotoWomen" runat="server" Visible="false">
            <td>
                <div id="tdWmnLevel1" class="tdWmnLevel level1" runat="server"></div>
            </td>
            <td>
                <div id="tdWmnLevel2" class="tdWmnLevel level2" runat="server"></div>
            </td>
            <td>
                <div id="tdWmnLevel3" class="tdWmnLevel level3" runat="server"></div>
            </td>
            <td>
                <div id="tdWmnLevel4" class="tdWmnLevel level4" runat="server"></div>
            </td>
            <td>
                <div id="tdWmnLevel5" class="tdWmnLevel level5" runat="server"></div>
            </td>
            <td>
                <div id="tdWmnLevel6" class="tdWmnLevel level6" runat="server"></div>
            </td>
            <td>
                <div id="tdWmnLevel7" class="tdWmnLevel level7" runat="server"></div>
            </td>
            <td>
                <div id="tdWmnLevel8" class="tdWmnLevel level8" runat="server"></div>
            </td>
            <td>
                <div id="tdWmnLevel9" class="tdWmnLevel level9" runat="server"></div>
            </td>
            <td>
                <div id="tdWmnLevel10" class="tdWmnLevel level10" runat="server"></div>
            </td>
        </tr>
    </table>
--%>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cntBodyBottom" runat="server">
<%--    <div id="private-footer" style="">
        <div class="private-footer-level1"></div>
        <div class="private-footer-level2"></div>
        <div class="private-footer-level3"></div>
        <div class="private-footer-level4"></div>
        <div class="private-footer-level5"></div>
        <div class="private-footer-level6"></div>
        <div class="private-footer-level7"></div>
        <div class="private-footer-level8"></div>
        <div class="private-footer-level9"></div>
        <div class="private-footer-level10"></div>
    </div>

--%>   <script type="text/javascript">
       var popupName = '<%= Request.QueryString("popup") %>';
       function resizePopup1() {
           var $j = jQuery;
           var h = $('html').height();
           var w = $('html').width();
           try {
               top.window[popupName].SetSize(w + 50, h + 77);
               top.window[popupName].UpdatePosition();
               var title = '<%= Dating.Server.Site.Web.AppUtils.JS_PrepareString (PopupTitle) %>';
               if (title != null && title.length > 0)
                   top.window[popupName].SetHeaderText(title);
           }
           catch (e) { }
       }
       function setFancyboxTitle() {
           try {
               var title = '<%= Dating.Server.Site.Web.AppUtils.JS_PrepareString (PopupTitle) %>';
               if (title != null && title.length > 0) {
                   //alert(title + ' 1')
                   window.top.setFancyboxTitle(title);
                   //alert(title + ' 2')
               }
           }
           catch (e) { }
       }
       jQuery(document).ready(function ($) {

           (function () {
               var availableLevel = parseInt('<%= MyBase.AvailableLevel %>');
               var requestedLevel = parseInt('<%= MyBase.RequestedLevel %>');

               if (availableLevel == 0) {
                   $('div#private-footer-top-line').addClass('level1');
                   setInterval(toogleLevel1, 500)
               }
               else if (availableLevel > 0) {
                   $('div#private-footer-top-line').addClass('level' + availableLevel);
                   for (var i = 1; i <= availableLevel; i++)
                       $('.private-footer-level' + i, '#private-footer').addClass('on')
               }

               var isOn = false;
               function toogleLevel1() {
                   if (isOn) 
                       $('.private-footer-level1', '#private-footer').removeClass('on');
                   else 
                       $('.private-footer-level1', '#private-footer').addClass('on');
                   isOn = !isOn;
               }
           })();

           setTimeout(setFancyboxTitle, 1500);

       });
    </script>

</asp:Content>

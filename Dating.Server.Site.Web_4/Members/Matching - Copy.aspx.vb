﻿Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Core.DLL
Imports System.Data.SqlClient
Imports System.Globalization
Imports Dating.Server.Datasets.DLL.DSMembers

Public Class Matching
    Inherits BasePage

    Protected Overrides Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            If clsCurrentContext.VerifyLogin() = True Then
                Me.MasterPageFile = "~/Members/Members2.Master"
            Else
                clsCurrentContext.ClearSession(Session)
                If (Not Me.IsCallback) Then
                    Response.Redirect(ResolveUrl("~/Login.aspx"), False)
                End If
                HttpContext.Current.ApplicationInstance.CompleteRequest()
            End If
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub
    Protected Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData(Context, Me.Page, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property
    Private Sub Page_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        Try
            If Me._pageData IsNot Nothing Then
                RemoveHandler Me._pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If

        Catch ex As Exception
        End Try
       
        Try
            MyBase.Dispose()
        Catch ex As Exception
        End Try
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If (Not Me.IsPostBack) Then
                LoadLAG()
            End If
            If (ProfileHelper.IsMale(Me.GetCurrentProfile().GenderId)) Then
                divIncome.Visible = False
            Else
                divIncome.Visible = True
            End If
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try


    End Sub


#Region "Properties"
    Protected ReadOnly Property DontAskForSave As Boolean
        Get
            If (Session("AdvancedSearch_Dont_Save_Dont_Ask") Is Nothing) Then
                Return False
            ElseIf (Session("AdvancedSearch_Dont_Save_Dont_Ask") = True) Then
                Return True
            End If
            Return False
        End Get
    End Property
#End Region

    Protected Sub LoadLAG()
        Try
            SetControlsValue(Me, CurrentPageData)
            chkTravelOnDate.Text = CurrentPageData.GetCustomString(chkTravelOnDate.ID)



            msg_SelectCountry.Text = CurrentPageData.GetCustomString(msg_SelectCountry.ID)
            msg_SelectRegionText.Text = CurrentPageData.GetCustomString(msg_SelectRegionText.ID)
            msg_SelectCountry.Text = CurrentPageData.GetCustomString(msg_SelectCountry.ID)
            msg_SelectCity.Text = CurrentPageData.GetCustomString(msg_SelectCity.ID)


            msg_ZipCodeText.Text = CurrentPageData.GetCustomString(msg_ZipCodeText.ID)

            msg_ZipExample.Text = CurrentPageData.GetCustomString(msg_ZipExample.ID)



            msg_DistanceFromMeText.Text = CurrentPageData.GetCustomString("msg_DistanceFromMeText")
            msg_AgeToWord.Text = CurrentPageData.GetCustomString("msg_AgeToWord")
            'btnDeleteSS.Text = CurrentPageData.GetCustomString("btnDeleteSavedSearch")

            
            cbCountry.Items.Clear()
            LoadAdvancedSearch()




            '  lnkViewDescription2.Text = Me.CurrentPageData.GetCustomString("CartSearchWhatIsIt")
            ' lnkViewDescription2.OnClientClick = OnMoreInfoClickFunc(
            '   lnkViewDescription2.OnClientClick,
            '  ResolveUrl("~/Members/InfoWin.aspx?info=search"),
            '  Me.CurrentPageData.GetCustomString("CartSearchView"))

            WhatIsIt1.PopupControl.HeaderText = Me.CurrentPageData.GetCustomString("CartSearchView")
           

            btnSaveAndFind.Text = CurrentPageData.GetCustomString("btnSaveAndFind")
            btnSaveAndFind.Text = AppUtils.StripHTML(btnSaveAndFind.Text)

            If (Me.DontAskForSave) Then
                hdfNewName.Value = "#DO_NOT_SAVE#"
                hdfSelectedName.Value = "-1"
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Private Sub LoadAdvancedSearch()
        If (cbCountry.Items.Count = 0) Then


            Web.ClsCombos.FillComboUsingDatatable(SYS_CountriesGEOHelper.gDSListsGEO__IsEnabled_PrintableNameASC.SYS_CountriesGEO, "PrintableName", "Iso", cbCountry, True, "PrintableName")
            Dim def As DevExpress.Web.ASPxEditors.ListEditItem = Nothing 'cbCountry.Items.FindByValue(Session("GEO_COUNTRY_CODE"))
            If (def IsNot Nothing) Then
                def.Selected = True
            Else
                def = New DevExpress.Web.ASPxEditors.ListEditItem(CurrentPageData.GetCustomString("cbCountry_default"), "-1")
                def.Selected = True
                cbCountry.Items.Insert(0, def)
            End If


            Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_Height, Me.GetLag(), "HeightId", cbHeightMin, True, "US")
            If (cbHeightMin.Items.Count > 0) Then cbHeightMin.Items(0).Selected = True


            Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_Height, Me.GetLag(), "HeightId", cbHeightMax, True, "US")
            If (cbHeightMax.Items.Count > 0) Then cbHeightMax.Items(0).Selected = True


            Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_BodyType, Me.GetLag(), "BodyTypeId", cbBodyType, True)
            If (cbBodyType.Items.Count > 0) Then
                cbBodyType.Items.RemoveAt(0)
                SelectAllCheckBoxes(cbBodyType, True)
            End If

            Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_Ethnicity, Me.GetLag(), "EthnicityId", cbEthnicity, True)
            If (cbEthnicity.Items.Count > 0) Then
                cbEthnicity.Items.RemoveAt(0)
                SelectAllCheckBoxes(cbEthnicity, True)
            End If

            Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_EyeColor, Me.GetLag(), "EyeColorId", cbEyeColor, True)
            If (cbEyeColor.Items.Count > 0) Then
                cbEyeColor.Items.RemoveAt(0)
                SelectAllCheckBoxes(cbEyeColor, True)
            End If

            Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_HairColor, Me.GetLag(), "HairColorId", cbHairColor, True)
            If (cbHairColor.Items.Count > 0) Then
                cbHairColor.Items.RemoveAt(0)
                SelectAllCheckBoxes(cbHairColor, True)
            End If

            Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_RelationshipStatus, Me.GetLag(), "RelationshipStatusId", cbRelationshipStatus, True)
            If (cbRelationshipStatus.Items.Count > 0) Then
                cbRelationshipStatus.Items.RemoveAt(0)
                SelectAllCheckBoxes(cbRelationshipStatus, True)
            End If

            Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_ChildrenNumber, Me.GetLag(), "ChildrenNumberId", cbChildren, True)
            If (cbChildren.Items.Count > 0) Then
                cbChildren.Items.RemoveAt(0)
                SelectAllCheckBoxes(cbChildren, True)
            End If

            Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_TypeOfDating, Me.GetLag(), "TypeOfDatingId", cbDatingType, True)
            SelectAllCheckBoxes(cbDatingType, True)

            Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_Education, Me.GetLag(), "EducationId", cbEducation, True)
            If (cbEducation.Items.Count > 0) Then
                cbEducation.Items.RemoveAt(0)
                SelectAllCheckBoxes(cbEducation, True)
            End If

            Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_Income, Me.GetLag(), "IncomeId", cbIncome, True)
            If (cbIncome.Items.Count > 0) Then
                cbIncome.Items.RemoveAt(0)
                SelectAllCheckBoxes(cbIncome, True)
            End If

            

            Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_Religion, Me.GetLag(), "ReligionId", cbReligion, True)
            If (cbReligion.Items.Count > 0) Then
                cbReligion.Items.RemoveAt(0)
                SelectAllCheckBoxes(cbReligion, True)
            End If

            Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_Smoking, Me.GetLag(), "SmokingId", cbSmoking, True)
            If (cbSmoking.Items.Count > 0) Then
                cbSmoking.Items.RemoveAt(0)
                SelectAllCheckBoxes(cbSmoking, True)
            End If


            Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_Drinking, Me.GetLag(), "DrinkingId", cbDrinking, True)
            If (cbDrinking.Items.Count > 0) Then
                cbDrinking.Items.RemoveAt(0)
                SelectAllCheckBoxes(cbDrinking, True)
            End If

           
        End If


        If (cbCountry.SelectedItem IsNot Nothing) Then
            FillRegionCombo(cbCountry.SelectedItem.Value)
            cbCountry_SelectedIndexChanged(cbCountry, Nothing)
            ClsCombos.SelectComboItem(cbRegion, cbRegion.Text)

            If (cbRegion.SelectedItem IsNot Nothing) Then
                FillCityCombo(cbRegion.SelectedItem.Value)
                cbRegion_SelectedIndexChanged(cbRegion, Nothing)
                ClsCombos.SelectComboItem(cbCity, cbCity.Text)
            End If
        End If
        If (cbRegion.Items.Count = 0) Then cbRegion.Text = CurrentPageData.GetCustomString("msg_NotFound")
        If (cbCity.Items.Count = 0) Then cbCity.Text = CurrentPageData.GetCustomString("msg_NotFound")

    End Sub
    Public Sub SetControlsValue(ByRef _control As Control, ByRef _pageData As clsPageData)

        Dim checkChildrenControls = True

        If (_control.GetType().FullName.StartsWith("DevExpress.Web")) Then
            checkChildrenControls = False
        Else

            Dim valueString As String
            If (_control.ID IsNot Nothing) Then

                Dim ctlId As String = _control.ID
                If (ctlId.StartsWith("msg_")) Then
                    valueString = _pageData.GetCustomString(ctlId)

                    If (valueString IsNot Nothing) Then
                        If (TypeOf _control Is LinkButton) Then
                            CType(_control, LinkButton).Text = valueString
                            checkChildrenControls = False

                        ElseIf (TypeOf _control Is Label) Then
                            CType(_control, Label).Text = valueString
                            checkChildrenControls = False

                        ElseIf (TypeOf _control Is Literal) Then
                            CType(_control, Literal).Text = valueString
                            checkChildrenControls = False

                        ElseIf (TypeOf _control Is LiteralControl) Then
                            CType(_control, LiteralControl).Text = valueString
                            checkChildrenControls = False

                        ElseIf (TypeOf _control Is TextBox) Then
                            CType(_control, TextBox).Text = valueString
                            checkChildrenControls = False

                        ElseIf (TypeOf _control Is Button) Then
                            CType(_control, Button).Text = valueString
                            checkChildrenControls = False

                        ElseIf (TypeOf _control Is HyperLink) Then
                            CType(_control, HyperLink).Text = valueString
                            checkChildrenControls = False

                        End If

                    End If
                End If

            End If

        End If


        ' check children controls
        If (checkChildrenControls) Then

            For Each ctl As Control In _control.Controls
                ' skip UserControl controls
                If (TypeOf ctl Is UserControl AndAlso Not (TypeOf ctl Is MasterPage)) Then Continue For
                SetControlsValue(ctl, _pageData)
            Next

        End If

    End Sub

    Protected Sub sdsRegion_Selecting(sender As Object, e As System.Web.UI.WebControls.SqlDataSourceSelectingEventArgs) Handles sdsRegion.Selecting
        For Each prm As SqlClient.SqlParameter In e.Command.Parameters

            If (prm.ParameterName = "@language") Then
                If (GetLag() = "GR") Then
                    prm.Value = "EL"
                Else
                    prm.Value = "EN"
                End If
            End If

        Next

    End Sub

    Protected Sub sdsCity_Selecting(sender As Object, e As System.Web.UI.WebControls.SqlDataSourceSelectingEventArgs) Handles sdsCity.Selecting
        For Each prm As SqlClient.SqlParameter In e.Command.Parameters

            If (prm.ParameterName = "@language") Then
                If (GetLag() = "GR") Then
                    prm.Value = "EL"
                Else
                    prm.Value = "EN"
                End If
            End If

        Next

    End Sub

    Protected Sub FillRegionCombo(ByVal country As String)
        If String.IsNullOrEmpty(country) Then
            Return
        End If

        Try
            'ShowLocationControls()
            'cbRegion.DataBind()

            If (cbRegion.Items.Count = 0) Then
                cbRegion.Text = CurrentPageData.GetCustomString("msg_NotFound")
                cbCity.Text = CurrentPageData.GetCustomString("msg_NotFound")
            Else
                cbRegion.Text = CurrentPageData.GetCustomString(msg_SelectRegionText.ID)
                cbCity.Text = CurrentPageData.GetCustomString("msg_NotFound")
            End If


        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        Finally

        End Try
    End Sub

    Private Sub cbCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbCountry.SelectedIndexChanged
        If (cbCountry.SelectedItem IsNot Nothing AndAlso cbCountry.SelectedItem.Value <> "-1") Then
            cbRegion.DataSourceID = ""
            cbRegion.DataSource = clsGeoHelper.GetCountryRegions(cbCountry.SelectedItem.Value, Session("LAGID"))
            cbRegion.TextField = "region1"
            cbRegion.DataBind()

        End If
    End Sub

    Private Sub cbRegion_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbRegion.SelectedIndexChanged
        If (cbRegion.SelectedItem IsNot Nothing AndAlso cbRegion.SelectedItem.Value <> "-1") Then
            cbCity.DataSourceID = ""
            cbCity.DataSource = clsGeoHelper.GetCountryRegionCities(cbCountry.SelectedItem.Value, cbRegion.SelectedItem.Value, Session("LAGID"))
            cbCity.TextField = "city"
            cbCity.DataBind()
        End If
    End Sub


    Protected Sub FillCityCombo(ByVal region1 As String)
        If String.IsNullOrEmpty(region1) Then
            Return
        End If

        Try
            'ShowLocationControls()
            cbCity.Text = CurrentPageData.GetCustomString(msg_SelectCity.ID)


        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        Finally

        End Try
    End Sub


    Private Sub FillZipTextBox(city As String, Optional region As String = "")
        If String.IsNullOrEmpty(city) Then
            Return
        End If

        Try
            'ShowLocationControls()

            ' if there is a value in zip, skip
            'If (txtZip.Text.Trim() <> "") Then
            '    Return
            'End If


            Dim country As String = cbCountry.SelectedItem.Value

            If (String.IsNullOrEmpty(region)) Then
                region = cbRegion.SelectedItem.Value
            End If
            ' city = cbCity.SelectedItem.Value
            If (country = "GR") Then
                txtZip.Text = clsGeoHelper.GetCityCenterPostcode(country, region, city)
            End If
            'ShowRegion(True)
            'ShowCity(True)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        Finally

        End Try
    End Sub


End Class



﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Members/Members2Inner.Master" CodeBehind="MessagesTest.aspx.vb" 
    Inherits="Dating.Server.Site.Web.MessagesTest" %>

<%@ Register src="~/UserControls/MessagesControlLeft.ascx" tagname="MessagesControlLeft" tagprefix="uc2" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script src="../Scripts/jquery-ui-1.10.3.js" type="text/javascript"></script>
<style type="text/css">



#quick-list-messages { width: 292px; height: 560px; background: url('../../Images2/mbr2/left-bar.png'); overflow: hidden; margin: 20px; }
#quick-list-messages .qmsl_item { padding: 4px 0; }
#quick-list-messages .qmsl_item .photo { float: left; margin-left: 8px; }
#quick-list-messages .qmsl_item .info { float: left; margin-left: 10px; color: #fff; width: 140px; font-size: 14px; }
#quick-list-messages .qmsl_item .info h2 { margin: 0px; padding: 0px; line-height: 20px; margin-right: 8px; font-weight: normal; font-size: 14px; margin-bottom: 8px; }
#quick-list-messages .qmsl_item .msg-count { -webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px; background-color: #DA1E4E; display: block; padding: 0px 7px 1px; font-size: 14px; min-width: 16px; text-align: center; }
#quick-list-messages .qmsl_item .info2 { font-size: 11px; }
#quick-list-messages .qmsl_item-separator { background: url('../Images2/mbr2/sep.png') no-repeat scroll 0 50%; height: 8px; width: 100%; }
    
#quick-list-messages #scroll-bar-wrap { width: 0; height: 0; position: relative; left: 223px; top: 10px; }
#quick-list-messages #scroll-bar { width: 30px; height: 544px; position: relative; right: 0; top: 0; }
#quick-list-messages #slider { position: absolute; width: 10px; height: 544px; background-color: #8e8d8d; background: url('../../Images2/mbr2/scroll-bg.png') repeat top left; right: 0; top: 0; }
#quick-list-messages #tooltip-slider { z-index: 100; position: absolute; display: block; left: -29px; /*top: -25px;*/ width: 35px; height: 20px; color: #fff; text-align: center; font: 10pt Tahoma, Arial, sans-serif;border-radius: 3px; border: 1px solid #333; 
                  -webkit-box-shadow: 1px 1px 2px 0px rgba(0, 0, 0, .3); box-shadow: 1px 1px 2px 0px rgba(0, 0, 0, .3); -moz-box-sizing: border-box; -webkit-box-sizing: border-box; box-sizing: border-box; 
                  background: -moz-linear-gradient(top,  rgba(69,72,77,0.5) 0%, rgba(0,0,0,0.5) 100%); 
                  background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(69,72,77,0.5)), color-stop(100%,rgba(0,0,0,0.5))); 
                  background: -webkit-linear-gradient(top,  rgba(69,72,77,0.5) 0%,rgba(0,0,0,0.5) 100%); background: -o-linear-gradient(top,  rgba(69,72,77,0.5) 0%,rgba(0,0,0,0.5) 100%); 
                  background: -ms-linear-gradient(top,  rgba(69,72,77,0.5) 0%,rgba(0,0,0,0.5) 100%); background: linear-gradient(top,  rgba(69,72,77,0.5) 0%,rgba(0,0,0,0.5) 100%); 
                  filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#8045484d', endColorstr='#80000000',GradientType=0 ); }
#quick-list-messages .ui-slider { position: relative; text-align: left; }
#quick-list-messages .ui-slider .ui-slider-handle { position: absolute; z-index: 2; width: 1.2em; height: 1.2em; cursor: default; }
#quick-list-messages .ui-slider .ui-slider-range { position: absolute; z-index: 1; font-size: .7em; display: block; border: 0; background-position: 0 0; }
/* For IE8 - See #6727 */
#quick-list-messages .ui-slider.ui-state-disabled .ui-slider-handle, .ui-slider.ui-state-disabled .ui-slider-range { filter: inherit; }
#quick-list-messages .ui-slider-horizontal { height: .8em; }
#quick-list-messages .ui-slider-horizontal .ui-slider-handle { top: -.3em; margin-left: -.6em; }
#quick-list-messages .ui-slider-horizontal .ui-slider-range { top: 0; height: 100%; }
#quick-list-messages .ui-slider-horizontal .ui-slider-range-min { left: 0; }
#quick-list-messages .ui-slider-horizontal .ui-slider-range-max { right: 0; }
#quick-list-messages .ui-slider-vertical { width: .8em; height: 100px; }
#quick-list-messages .ui-slider-vertical .ui-slider-handle { left: -.2em; margin-left: 0; margin-bottom: -.6em; }
#quick-list-messages .ui-slider-vertical .ui-slider-range { left: 0; width: 100%; }
#quick-list-messages .ui-slider-vertical .ui-slider-range-min { bottom: 0; }
#quick-list-messages .ui-slider-vertical .ui-slider-range-max { top: 0; }
#quick-list-messages .ui-slider-handle { position: absolute; z-index: 2; width: 21px; height: 23px; cursor: pointer; background: url('../../Images2/mbr2/scroll-cycle.png') no-repeat 50% 50%; outline: none; margin-left: -6px; }
#quick-list-messages .ui-slider-range { background: #dc1f4e; position: absolute; border: 0; top: 0; width: 7px; left: 1px; -webkit-border-radius: 25px; -moz-border-radius: 25px; border-radius: 25px; -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=30)"; filter: alpha(opacity=30); -moz-opacity: 0.3; -khtml-opacity: 0.3; opacity: 0.3; height: 100%; }
#quick-list-messages #list-container-wrap {  width: 206px; height: 560px;overflow: hidden;}
</style>
  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <div id="quick-list-messages">
        <div id="scroll-bar-wrap">
            <div id="scroll-bar">
                <div id="tooltip-slider"></div> <!-- Tooltip -->
                <div id="slider"></div> <!-- the Slider -->
                <span class="volume"></span> <!-- Volume -->
            </div>
        </div>
        <div id="list-container-wrap">
            <div id="list-container">
                <uc2:MessagesControlLeft ID="msgsL" runat="server" UsersListView="AllMessages"/>
            </div>
        </div>
    </div>



	<script type="text/javascript">

		
        function loadSlider() {
            var qmsl_items , qmsl_items_count ,list_container;

            qmsl_items = $('.qmsl_item');
            qmsl_items_count = qmsl_items.length;
            list_container = $('#list-container');

            var offset_top_1=$(qmsl_items[0]).offset().top;
            var qmsl_items_arr = new Array();
            for(var c=0 ; c<qmsl_items_count; c++ ){
                    var offset_top =   $(qmsl_items[c]).offset().top;
                    offset_top = offset_top -offset_top_1;
                qmsl_items_arr.push(offset_top);
           }

			//Store frequently elements in variables
			var slider  = $('#slider'),
				tooltip = $('#tooltip-slider');

			//Hide the Tooltip at first
			//tooltip.hide();

			//Call the Slider
			slider.slider({
				//Config
				range: "max",
                min: 0,
                max: qmsl_items_count,
                value: qmsl_items_count,
                orientation:"vertical",

				start: function(event,ui) {
				    tooltip.fadeIn('fast');
				},

				//Slider Event
				slide: function(event, ui) { //When the slider is sliding
					var value  = slider.slider('value'),
						volume = $('.volume');

                    var ndx = qmsl_items_count - parseInt(ui.value);
                    if(ndx<qmsl_items_count && ndx>=0){
                        var offset_top =   qmsl_items_arr[ndx];
                        $('#list-container-wrap').scrollTop(offset_top);
                        var login =$(".login-name",qmsl_items[ndx]).text();
                        tooltip.css('bottom', ui.value + '%').text(offset_top + ":" + (ndx+1) + ":" + login);
                    }
				},

				stop: function(event,ui) {
				    //tooltip.fadeOut('fast');
				}
			});
		}
        
        loadSlider();

	</script>
        <%--<script>
            $(function () {
                //scrollpane parts
                var scrollPane = $(".scroll-pane"),
      scrollContent = $(".scroll-content");

                //build slider
                var scrollbar = $(".scroll-bar").slider({
                    slide: function (event, ui) {
                        if (scrollContent.width() > scrollPane.width()) {
                            scrollContent.css("margin-left", Math.round(
            ui.value / 100 * (scrollPane.width() - scrollContent.width())
          ) + "px");
                        } else {
                            scrollContent.css("margin-left", 0);
                        }
                    }
                });

                //append icon to handle
                var handleHelper = scrollbar.find(".ui-slider-handle")
    .mousedown(function () {
        scrollbar.width(handleHelper.width());
    })
    .mouseup(function () {
        scrollbar.width("100%");
    })
    .append("<span class='ui-icon ui-icon-grip-dotted-vertical'></span>")
    .wrap("<div class='ui-handle-helper-parent'></div>").parent();

                //change overflow to hidden now that slider handles the scrolling
                scrollPane.css("overflow", "hidden");

                //size scrollbar and handle proportionally to scroll distance
                function sizeScrollbar() {
                    var remainder = scrollContent.width() - scrollPane.width();
                    var proportion = remainder / scrollContent.width();
                    var handleSize = scrollPane.width() - (proportion * scrollPane.width());
                    scrollbar.find(".ui-slider-handle").css({
                        width: handleSize,
                        "margin-left": -handleSize / 2
                    });
                    handleHelper.width("").width(scrollbar.width() - handleSize);
                }

                //reset slider value based on scroll content position
                function resetValue() {
                    var remainder = scrollPane.width() - scrollContent.width();
                    var leftVal = scrollContent.css("margin-left") === "auto" ? 0 :
        parseInt(scrollContent.css("margin-left"));
                    var percentage = Math.round(leftVal / remainder * 100);
                    scrollbar.slider("value", percentage);
                }

                //if the slider is 100% and window gets larger, reveal content
                function reflowContent() {
                    var showing = scrollContent.width() + parseInt(scrollContent.css("margin-left"), 10);
                    var gap = scrollPane.width() - showing;
                    if (gap > 0) {
                        scrollContent.css("margin-left", parseInt(scrollContent.css("margin-left"), 10) + gap);
                    }
                }

                //change handle position on window resize
                $(window).resize(function () {
                    resetValue();
                    sizeScrollbar();
                    reflowContent();
                });
                //init scrollbar size
                setTimeout(sizeScrollbar, 10); //safari wants a timeout
            });
  </script>--%>

</asp:Content>






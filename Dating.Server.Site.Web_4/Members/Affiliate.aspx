﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Members/Members2Inner.Master" 
    CodeBehind="Affiliate.aspx.vb" Inherits="Dating.Server.Site.Web.Affiliate" %>
<%@ Register src="../UserControls/UsersList.ascx" tagname="UsersList" tagprefix="uc2" %>
<%@ Register src="../UserControls/MemberLeftPanel.ascx" tagname="MemberLeftPanel" tagprefix="uc3" %>
<%@ Register src="../UserControls/WhatIsIt.ascx" tagname="WhatIsIt" tagprefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script type="text/javascript">
    jQuery(function () {
        $('#select_link').click(function () {
            $('#select_link').select();
        })
    });
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <div class="clear"></div>
    <div class="container_12" id="tabs-outter">
        <div class="grid_12 top_tabs">
        </div>
    </div>
    <div class="clear"></div>
	<div class="container_12" id="content_top">


	</div>
    <div class="container_12" id="content">
        <div class="grid_3 top_sidebar" id="sidebar-outter">
            <div class="left_tab" id="left_tab" runat="server">
                <div class="top_info">
                </div>
                <div class="middle" id="search">
                    <div class="submit">
                        <h3><asp:Literal ID="lblItemsName" runat="server" Text="Affiliate Dashboard"/></h3>
                        <div class="clear"></div>
                        <asp:LinkButton ID="lnkViewDescription" runat="server" CssClass="more_info2" OnClientClick="OnMoreInfoClick('[contentUrl]', this, '[headerText]');return false;" Visible="False" />
                    </div>
                </div>
                <div class="bottom">
                    &nbsp;</div>
            </div>
		    <%--<uc3:memberleftpanel ID="leftPanel" runat="server" ShowBottomPanel="false" />--%>
		</div>
        <div class="grid_9 body">
            <div class="middle" id="content-outter">
<div id="affiliate_area" runat="server" visible="false">
            <h4>
                <asp:Literal ID="lblAffInfo" runat="server" Text=""></asp:Literal></h4><div class="link-wrap">
                <asp:Label ID="lblAffLink" runat="server" Text="">
                    <asp:Literal ID="msg_AffLink" runat="server">Affiliate link:</asp:Literal> 
                    <input type="text" value="http://www.goomena.com/?cref=[CODE]" id="select_link" title="click to select" style="width:400px;" />  <br></asp:Label>
            </div>
            <table cellpadding="1" cellspacing="6">
                <tr>
                    <td style="text-align: right">Days Report:</td><td style="text-align: right">Start:</td><td>
                        <dx:ASPxDateEdit ID="deFromDate" runat="server" DisplayFormatString="dd/MM/yy HH:mm"
                            EditFormat="DateTime" EditFormatString="dd/MM/yy HH:mm" EnableAnimation="False">
                        </dx:ASPxDateEdit>
                    </td>
                    <td style="text-align: right">To:</td><td>
                        <dx:ASPxDateEdit ID="deToDate" runat="server" DisplayFormatString="dd/MM/yy HH:mm"
                            EditFormat="DateTime" EditFormatString="dd/MM/yy HH:mm" EnableAnimation="False">
                        </dx:ASPxDateEdit>
                    </td>
                    <td>
                        <dx:ASPxButton ID="CmdRun" runat="server" Text="Run">
                        </dx:ASPxButton>
                    </td>
                    <td>&nbsp;</td></tr><tr>
                    <td colspan="7" style="text-align: right">&nbsp;</td></tr><tr>
                    <td colspan="7">
                        <table class="style1">
                            <tr>
                                <td class="style2">
                                    <asp:Literal ID="msg_UniqueByIPText" runat="server"></asp:Literal></td><td colspan="2">
                                    <dx:ASPxLabel ID="lbUniqueByIP" runat="server" Text="lbUniqueByIP" Font-Bold="True">
                                    </dx:ASPxLabel>
                                </td>
                            </tr>
                            <tr>
                                <td class="style2"><asp:Literal ID="msg_RegistrationText" runat="server"></asp:Literal></td><td colspan="2">
                                    <dx:ASPxLabel ID="lbRegistration" runat="server" Text="lbRegistration" Font-Bold="True">
                                    </dx:ASPxLabel>
                                </td>
                            </tr>
                            <tr>
                                <td class="style2"><asp:Literal ID="msg_Payments" runat="server"></asp:Literal></td><td colspan="2">
                                    <dx:ASPxLabel ID="lbPayments" runat="server" Text="lbPayments" Font-Bold="True">
                                    </dx:ASPxLabel>
                                    <b>Euro</b></td></tr><tr>
                                <td class="style2"><asp:Literal ID="msg_Earning" runat="server"></asp:Literal></td><td colspan="2">
                                    <dx:ASPxLabel ID="lbEarning" runat="server" Text="lbEarning" Font-Bold="True">
                                    </dx:ASPxLabel>
                                    <b>Euro</b></td></tr></table></td></tr><tr>
                    <td colspan="7">&nbsp;</td></tr></table></div>

			    <div class="clear"></div>
            </div>
            <div class="bottom"></div>
		</div>
		<div class="clear"></div>
	</div>

	<uc2:whatisit ID="WhatIsIt1" runat="server" />
</asp:Content>

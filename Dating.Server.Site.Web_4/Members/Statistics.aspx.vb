﻿Imports Dating.Server.Core.DLL
Imports Dating.Server.Core.DLL.clsUserDoes
Imports Dating.Server.Datasets.DLL


Public Class Statistics
    Inherits BasePage

    Private Enum DateRangeNameEnum As Integer
        None = 0
        Custom = 1
        Last7Days = 2
        ThisMonth = 3
        Last3Months = 4
        Last6Months = 5
    End Enum

#Region "Props"

    Protected Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData("~/Members/Statistics.aspx", Context, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property
    Private Sub Page_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        Try
            If Me._pageData IsNot Nothing Then
                RemoveHandler Me._pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If

        Catch ex As Exception
        End Try
        Try
            MyBase.Dispose()
        Catch ex As Exception
        End Try
    End Sub
#End Region


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If (Not Me.IsPostBack) Then
                dteFrom.Date = DateTime.UtcNow.Date.AddDays(-30)
                dteTo.Date = DateTime.UtcNow.Date.AddDays(1).AddSeconds(-1)
            End If

            LoadLAG()

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Protected Sub LoadLAG()
        Try

            SetControlsValue(Me, CurrentPageData)

            btnLast7Days.Text = Me.CurrentPageData.GetCustomString("btnLast7Days")
            btnThisMonth.Text = Me.CurrentPageData.GetCustomString("btnThisMonth")
            btn3Months.Text = Me.CurrentPageData.GetCustomString("btn3Months")
            btn6Months.Text = Me.CurrentPageData.GetCustomString("btn6Months")
            btnCustom.Text = Me.CurrentPageData.GetCustomString("btnCustom")

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Protected Sub btnLast7Days_Click(sender As Object, e As EventArgs) Handles btnLast7Days.Click
        Try
            Dim dateFrom As DateTime? = Nothing
            Dim dateTo As DateTime? = Nothing

            dateFrom = DateTime.UtcNow.Date.AddDays(-7)
            dateTo = DateTime.UtcNow.Date.AddDays(1).AddSeconds(-1)

            UpdateText(dateFrom, dateTo, DateRangeNameEnum.Last7Days)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Protected Sub btnThisMonth_Click(sender As Object, e As EventArgs) Handles btnThisMonth.Click
        Try
            Dim dateFrom As DateTime? = Nothing
            Dim dateTo As DateTime? = Nothing

            dateFrom = DateTime.UtcNow.Date.AddDays(-DateTime.UtcNow.Day)
            dateTo = DateTime.UtcNow.Date.AddDays(1).AddSeconds(-1)

            UpdateText(dateFrom, dateTo, DateRangeNameEnum.ThisMonth)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Protected Sub btn3Months_Click(sender As Object, e As EventArgs) Handles btn3Months.Click
        Try
            Dim dateFrom As DateTime? = Nothing
            Dim dateTo As DateTime? = Nothing

            dateFrom = DateTime.UtcNow.Date.AddMonths(-3)
            dateTo = DateTime.UtcNow.Date.AddDays(1).AddSeconds(-1)

            UpdateText(dateFrom, dateTo, DateRangeNameEnum.Last3Months)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Protected Sub btn6Months_Click(sender As Object, e As EventArgs) Handles btn6Months.Click
        Try
            Dim dateFrom As DateTime? = Nothing
            Dim dateTo As DateTime? = Nothing

            dateFrom = DateTime.UtcNow.Date.AddMonths(-6)
            dateTo = DateTime.UtcNow.Date.AddDays(1).AddSeconds(-1)

            UpdateText(dateFrom, dateTo, DateRangeNameEnum.Last6Months)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Protected Sub btnCustom_Click(sender As Object, e As EventArgs) Handles btnCustom.Click
        Try
            Dim dateFrom As DateTime? = Nothing
            Dim dateTo As DateTime? = Nothing

            If (dteFrom.Value IsNot Nothing) Then
                dateFrom = dteFrom.Date.Date
            End If
            If (dteTo.Value IsNot Nothing) Then
                dateTo = dteTo.Date.Date.AddDays(1).AddSeconds(-1)
            End If

            UpdateText(dateFrom, dateTo, DateRangeNameEnum.Custom)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


    Private Sub UpdateText(dateFrom As DateTime?, dateTo As DateTime?, dateRangeName As DateRangeNameEnum)

        Dim result1 As EUS_Offers_LikesStatisticsResult = clsUserDoes.GetLikesStatistics(Me.MasterProfileId, dateFrom, dateTo)
        Dim result2 As EUS_Messages_StatisticsResult = clsUserDoes.GetMessagesStatistics(Me.MasterProfileId, dateFrom, dateTo)

        If (result1.CountSentLikes = 0) Then
            msg_LikesSent.Text = Me.CurrentPageData.GetCustomString("msg_LikesSent.ZERO")
            msg_LikesSent.Text = msg_LikesSent.Text.
                Replace("[MY_SENT_LIKES_COUNT]", result1.CountSentLikes)
        Else
            msg_LikesSent.Text = msg_LikesSent.Text.
                Replace("[MY_SENT_LIKES_COUNT]", result1.CountSentLikes).
                Replace("[OTHER_ACCEPTED_LIKES_COUNT]", result1.CountSentAndAcceptedLikes)
        End If


        If (result1.CountReceivedLikes = 0) Then
            msg_LikesReceived.Text = Me.CurrentPageData.GetCustomString("msg_LikesReceived.ZERO")
            msg_LikesReceived.Text = msg_LikesReceived.Text.
                Replace("[MY_RECEIVED_LIKES_COUNT]", result1.CountReceivedLikes)
        Else
            msg_LikesReceived.Text = msg_LikesReceived.Text.
                Replace("[MY_RECEIVED_LIKES_COUNT]", result1.CountReceivedLikes).
                Replace("[I_ACCEPTED_LIKES_COUNT]", result1.CountReceivedAndAcceptedLikes)
        End If


        If (result2.CountSentMessages = 0) Then
            msg_MessagesSent.Text = Me.CurrentPageData.GetCustomString("msg_MessagesSent.ZERO")
            msg_MessagesSent.Text = msg_MessagesSent.Text.
                Replace("[MY_SENT_MESSAGES_COUNT]", result2.CountSentMessages)
        Else
            msg_MessagesSent.Text = msg_MessagesSent.Text.
                  Replace("[MY_SENT_MESSAGES_COUNT]", result2.CountSentMessages).
                  Replace("[OTHER_READ_MESSAGES_COUNT]", result2.CountSentAndReadMessages)
        End If



        If (result2.CountReceivedMessages = 0) Then
            msg_MessagesReceived.Text = Me.CurrentPageData.GetCustomString("msg_MessagesReceived.ZERO")
            msg_MessagesReceived.Text = msg_MessagesReceived.Text.
                Replace("[MY_RECEIVED_MESSAGES_COUNT]", result2.CountReceivedMessages)
        Else
            msg_MessagesReceived.Text = msg_MessagesReceived.Text.
                     Replace("[MY_RECEIVED_MESSAGES_COUNT]", result2.CountReceivedMessages)
        End If



        If (dateRangeName > DateRangeNameEnum.Custom) Then
            pnlResults.Visible = True
            msg_ResultsDates.Text = msg_ResultsDates.Text.
                Replace("[DATE_FROM]", dateFrom.Value.ToString("dd MMM, yyyy")).
                Replace("[DATE_ΤΟ]", dateTo.Value.ToString("dd MMM, yyyy"))

            Select Case dateRangeName
                Case DateRangeNameEnum.Last7Days
                    msg_ResultsDates.Text = msg_ResultsDates.Text.
                        Replace("[DATE_RANGE_NAME]", Me.CurrentPageData.GetCustomString("Last7Days_Descr"))

                Case DateRangeNameEnum.ThisMonth
                    msg_ResultsDates.Text = msg_ResultsDates.Text.
                        Replace("[DATE_RANGE_NAME]", Me.CurrentPageData.GetCustomString("ThisMonth_Descr"))

                Case DateRangeNameEnum.Last3Months
                    msg_ResultsDates.Text = msg_ResultsDates.Text.
                        Replace("[DATE_RANGE_NAME]", Me.CurrentPageData.GetCustomString("Last3Months_Descr"))

                Case DateRangeNameEnum.Last6Months
                    msg_ResultsDates.Text = msg_ResultsDates.Text.
                        Replace("[DATE_RANGE_NAME]", Me.CurrentPageData.GetCustomString("Last6Months_Descr"))

            End Select
        Else
            pnlResults.Visible = False
        End If


        pnlStart.Visible = False
        pnlLikes.Visible = True
        pnlMessages.Visible = True

    End Sub

End Class
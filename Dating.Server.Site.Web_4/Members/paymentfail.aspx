﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Members/Members2Inner.master"
    CodeBehind="paymentfail.aspx.vb" Inherits="Dating.Server.Site.Web.paymentfail" %>

<%@ Register Src="../UserControls/MemberLeftPanel.ascx" TagName="MemberLeftPanel"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        try{
            if (window.top != window.self) {
                window.top.location = window.self.location;
            }
        }
        catch (e) { }
    </script>
<style type="text/css">
    #payment_err {font-family:'Segoe UI', 'Arial';}
    #payment_err .main-wrap{background: url('//cdn.goomena.com/Images2/payment-finish/bg.png') no-repeat scroll 0 0;width:685px;height:570px;position:relative;}
    #payment_err .main-wrap .success-title{text-align:center;position:absolute;top:30px;left:0px;right:0px;}
    #payment_err .main-wrap .success-title .line1{font-size:25px;font-weight:bold;color:#f84600;}
    #payment_err .main-wrap .success-title .line2{font-size:19px;font-style:italic;color:#f84600;}
    #payment_err .main-wrap .main-content{text-align:center;position:absolute;top:250px;color:#333;}
    #payment_err .go-back { color: #fff;font-size: 16px;font-weight:bold; text-align: center; line-height: 42px; 
                                                 border-width: 0px; width: 174px; height: 47px;display: inline-block; 
                                                 background: url('//cdn.goomena.com/Images2/payment-finish/err-button-back.png') no-repeat scroll 0% 0%;
                                                 margin: 0 auto; }
    #payment_err .go-back:hover {background-position:100% 100%;}
    #payment_err ul.success-points {font-size:13px;text-align:left;}
    #payment_err ul.success-points li {margin-left:40px;margin-bottom:15px;color:#333;margin-right: 30px;}
    #payment_err div.grey-line {height:2px;margin:10px 20px;background-color:#7C8274;}
    #payment_err div.logo-holder {text-align:center;position:absolute;bottom:30px;left:0px;right:0px;}
    #payment_err div.link-holder {text-align:center;position:relative;margin-top:20px;}

</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <div class="clear">
    </div>
    <div class="container_12" id="tabs-outter">
        <div class="grid_12 top_tabs">
            <div>
            </div>
        </div>
    </div>
    <div class="clear">
    </div>
    <div class="container_12" id="content_top">
    </div>
    <div class="container_12" id="content">
        <div class="grid_3" id="sidebar-outter">
            <%--<uc1:MemberLeftPanel ID="leftPanel" runat="server" ShowBottomPanel="false" />--%>
        </div>
        <div class="grid_9 body" id="payment_err">
            <div class="middle" id="content-outter">
            
                <asp:Literal ID="lDescription2" runat="server">
                <div class="main-wrap">

                    <div class="success-title">
                        <img src="//cdn.goomena.com/Images2/payment-finish/error-icon.png" alt="Your Transaction was successful. Your account has been credited the amount purchased. Now start talking." title="Your Transaction was successful. Your account has been credited the amount purchased. Now start talking." /><br />
                        <span class="line1">Η συναλλαγή ακυρώθηκε</span><br />
                        <span class="line2">Η πιστωτική σου κάρτα ΔΕΝ χρεώθηκε</span>
                    </div>

                    <div class="main-content">
                        <div style="line-height:25px;">
                            <span style="font-size:14px;">Μπορείς να δοκιμάσεις ξανά, ή μπορείς να επιλέξεις άλλη μέθοδο πληρωμής εάν το επιθυμείς.</span><br />
                        </div>
                        <div class="link-holder">
        <a href="http://www.goomena.com/Members/Default.aspx" target="_self" class="go-back">Επιστροφή</a>
                        </div>
                        <div class="grey-line"></div>
<ul class="success-points">
    <li>Εάν καμία από τις μεθόδους πληρωμής που προσφέρουμε δεν&nbsp;σου ταιριάζει, 
        θα το εκτιμούσαμε αν επικοινωνήσεις μαζί μας&nbsp;να μας πεις ποια μέθοδο πληρωμής προτιμάς.</li>
    <li>Έτσι στο μέλλον θα μπορούσαμε να υποστηρίξουμε την&nbsp;προτεινόμενη αυτή μέθοδο πληρωμής, ή ακόμα, 
        θα μπορούσαμε να σου προτείνουμε μια άλλη λύση, όπου η συγκεκριμένη μέθοδος πληρωμής θα είναι διαθέσιμη.</li>
</ul>
                    </div>

                    <div class="logo-holder">
                        <img src="//cdn.goomena.com/Images2/payment-finish/logo-small.png" alt="logo-small.png" />
                    </div>

                </div>


                </asp:Literal>


<%--                <table id="ctl00_tblTitle" cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tbody>
                        <tr>
                            <td valign="middle" class="DetailsPageHeader">
                                <table cellpadding="0" cellspacing="0" border="0" class="Title" width="100%">
                                    <tbody>
                                        <tr>
                                            <td style="width: 1%;" rowspan="2">
                                                <dx:ASPxImage ID="iTitleImage" runat="server" Style="height: 50px; width: 45px; border-width: 0px;
                                                    border-width: 0px;" ImageUrl="//cdn.goomena.com/Images/IconImages/agt_update_critical.png" AlternateText="Transaction Cancelled - Your credit card was NOT charged">
                                                </dx:ASPxImage>
                                            </td>
                                            <td>
                                                &nbsp;&nbsp;<dx:ASPxLabel ID="lGroupName" runat="server" Text="Transaction Cancelled"
                                                    Style="font-size: X-Large; font-weight: bold;" EncodeHtml="False">
                                                </dx:ASPxLabel>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;&nbsp;<dx:ASPxLabel ID="lbSubTitle" runat="server" 
                                                    Text="Your credit card was NOT charged" EncodeHtml="False">
                                                </dx:ASPxLabel>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table id="ctl00_tblContent" cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tbody>
                        <tr>
                            <td valign="top">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tbody>
                                        <tr>
                                            <td class="DetailsPageLeftEdge">
                                                <div class="Spacer DetailsPageEdge">
                                                </div>
                                            </td>
                                            <td valign="top" class="DetailsPageContent">
                                                <div class="ContentMargin">
                                                    <dx:ASPxLabel ID="lDescription" runat="server" Text="" EncodeHtml="False">
                                                    </dx:ASPxLabel>
                                                </div>
                                            </td>
                                            <td class="DetailsPageRightEdge">
                                                <div class="Spacer DetailsPageEdge">
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table id="ctl00_tblFooter" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                        <tr>
                            <td class="DetailsPageFooter">
                                <div class="Spacer">
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
--%>            </div>
            <div class="bottom">
            </div>
        </div>
        <div class="clear">
        </div>
    </div>

</asp:Content>

﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Members/Members2Inner.Master" MaintainScrollPositionOnPostback="false"
    CodeBehind="Matching.aspx.vb" EnableEventValidation="true" Inherits="Dating.Server.Site.Web.Matching" %>

<%@ Register Assembly="DevExpress.Web.v13.1, Version=13.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>

<%--<%@ Register src="../UserControls/ProfilePreview.ascx" tagname="ProfilePreview" tagprefix="uc1" %>--%>
<%@ Register Src="~/UserControls/MemberLeftPanel.ascx" TagName="MemberLeftPanel" TagPrefix="uc3" %>
<%@ Register Src="~/UserControls/AgeDropDown.ascx" TagName="AgeDropDown" TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/DistanceDropDown.ascx" TagName="DistanceDropDown" TagPrefix="uc4" %>
<%@ Register Src="~/UserControls/WhatIsIt.ascx" TagName="WhatIsIt" TagPrefix="uc2" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../v1/CSS/matching.css" rel="stylesheet" />
    <style type="text/css">
    #criteria_distance { width: 212px; }
    #quick-search-form2 #criteria_distance .criteria { padding: 5px; }
</style>
    <script type="text/javascript">
       
        // <![CDATA[
        function OnCountryChanged(cbCountry) {
            LoadingPanel.Hide();
            cbpnlZip.PerformCallback("country_" + cbCountry.GetValue().toString());
            ShowLoading();
        }
        function OnRegionChanged(cbRegion) {
            LoadingPanel.Hide();
            cbpnlZip.PerformCallback("region_" + cbRegion.GetValue().toString());
            ShowLoading();
        }
        function onhi() {
            HideLoading();
        }
        function OnCityChanged(cbCity) {
        }
        function showZip() {
            region.hide();
            city.hide();
            var status = jQuery('#<%= hdfLocationStatus.ClientID %>');
            status.val("zip");
        }
        function showRegions() {
            zip.hide();
            region.show();
            city.show();
            var status = jQuery('#<%= hdfLocationStatus.ClientID %>');
        status.val("region");
    }

        function TrackBar_Init(s, e) {
            if (s == window["trcHeight"]) {
            }
            if (s == window["trcDistance"]) {
            }
            if (s == window["trcAge"]) {
            }
            if (trackBarInfoList && trackBarInfoList != null &&
            trackBarInfoList.List != null && trackBarInfoList.List.length > 0) {
                SetTextForPositions(s);
            }
        }

    function SelectCheckBoxes(scope) {
        $('input[type="checkbox"]', $(scope)).prop("checked", true);
    }
    function ClearCheckBoxes(scope) {
        $('input[type="checkbox"]', $(scope)).prop("checked", false);
    }
    function TrackBar_PositionChanging(s, e) {
        //SaveDefaultText(s, e);
    }
    function TrackBar_PositionChanged(s, e) {
        SetTextForPositions(s);
    }

    function SetTextForPositions(s) {
        var txt = "", ti;
        try {
            //s.GetItemText(s.GetPosition())
            //s.GetPositionStart()
            if (s == window["trcHeight"]) {
                ti = trackBarInfoList.GetInfoByPos("trcHeight", "DefaultTextMin", s.GetPositionStart());
                if (ti != null) { txt = ti.DefaultTextMin; } else {
                    txt = s.GetItemText(s.GetPositionStart());
                    if (stringIsEmpty(txt)) txt = s.GetPositionStart();
                }
                $('#height-min', '#criteria_height').html(txt);

                ti = trackBarInfoList.GetInfoByPos("trcHeight", "DefaultTextMax", s.GetPositionEnd());
                if (ti != null) { txt = ti.DefaultTextMax; } else {
                    txt = s.GetItemText(s.GetPositionEnd());
                    if (stringIsEmpty(txt)) txt = s.GetPositionEnd();
                }
                $('#height-max', '#criteria_height').html(txt);
            }
            else if (s == window["trcDistance"]) {
                ti = trackBarInfoList.GetInfoByPos("trcDistance", "DefaultText", s.GetPosition());
                if (ti != null) { txt = ti.DefaultText; } else {
                    txt = s.GetItemText(s.GetPosition());
                    if (stringIsEmpty(txt)) txt = s.GetPosition();
                }
                $('.trackbar-pos-val', '#criteria_distance').html(txt);
            }
            else if (s == window["trcAge"]) {
                ti = trackBarInfoList.GetInfoByPos("trcAge", "DefaultTextMin", s.GetPositionStart());
                if (ti != null) { txt = ti.DefaultTextMin; } else {
                    txt = s.GetItemText(s.GetPositionStart());
                    if (stringIsEmpty(txt)) txt = s.GetPositionStart();
                }
                $('#age-min', '#criteria_age').html(txt);

                ti = trackBarInfoList.GetInfoByPos("trcAge", "DefaultTextMax", s.GetPositionEnd());
                if (ti != null) { txt = ti.DefaultTextMax; }
                else {
                    txt = s.GetItemText(s.GetPositionEnd());
                    if (stringIsEmpty(txt)) txt = s.GetPositionEnd();
                }
                $('#age-max', '#criteria_age').html(txt);
            }
        }
        catch (e) { }
    }
        // ]]> 
    function SaveDefaultText2() {
        try {
            trackBarInfoList.AddInfo("trcHeight", "DefaultTextMin", "&lt;152cm", 0);
            trackBarInfoList.AddInfo("trcHeight", "DefaultTextMax", "&gt;213cm", <%= trcHeight.Items.Count - 1%>);

            trackBarInfoList.AddInfo("trcDistance", "DefaultText", '<%= msg_DistanceVal.Text%>', <%= trcDistance.Items.Count - 1%>);

            trackBarInfoList.AddInfo("trcAge", "DefaultTextMin", '18', 0);
            trackBarInfoList.AddInfo("trcAge", "DefaultTextMax", '120', <%= trcAge.Items.Count - 1%>);
        }
        catch (e) { }
    }
        $(function(){
            SaveDefaultText2();
            //initQuickOptions();
  
        });
    </script>

</asp:Content>


<asp:Content ID="Content2"  ContentPlaceHolderID="content" runat="server">
    <div id="MatchingCriteria">
        <div id="CriteriaTopView">
            <asp:Label ID="lblCriteriaHeader" runat="server" Text=""></asp:Label>
        </div>
        <div id="criteria_age" class="lfloat crt-wrap">
            <div class="criteria trackbar tb-age">
                <div class="trackbar-title"><asp:Literal ID="msg_AgeText" runat="server" Text="Ηλικία"/></div>
                <dx:ASPxTrackBar runat="server" ID="trcAge" MinValue="18" MaxValue="120" Step="1"
                    LargeTickInterval="16" SmallTickFrequency="6" Width="200" AllowRangeSelection="true"
                    PositionStart="18" PositionEnd="120" ClientInstanceName="trcAge" 
                    EnableTheming="False" ScaleLabelHighlightMode="HandlePosition" 
                    ShowChangeButtons="False">
                    <ClientSideEvents Init="TrackBar_Init" PositionChanging="TrackBar_PositionChanging" PositionChanged="TrackBar_PositionChanged" />
                </dx:ASPxTrackBar>
                <div>
                    <div id="age-min" class="trackbar-pos-val lfloat"><asp:Literal ID="msg_AgeMin" runat="server" Text="18"/></div>
                    <div id="age-max" class="trackbar-pos-val rfloat"><asp:Literal ID="msg_AgeMax" runat="server" Text="120"/></div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>

        <div id="criteria_distance" class="lfloat crt-wrap">
            <div class="criteria trackbar tb-distance">
                <div class="trackbar-title"><asp:Literal ID="msg_DistanceFromMe_ND" runat="server" Text="Απόσταση απο εμένα"/></div>
                <dx:ASPxTrackBar runat="server" ID="trcDistance" Step="1" 
                    MinValue="20"
                    MaxValue="10000"
                    PositionStart="0" PositionEnd="30"
                    LargeTickInterval="4" LargeTickEndValue="1" Width="200px" 
                    ClientInstanceName="trcDistance" EnableTheming="False" ScaleLabelHighlightMode="HandlePosition" 
                    ShowChangeButtons="False">
                    <Items>
                        <dx:TrackBarItem Text="20km" ToolTip="20km" Value="20" />
                        <dx:TrackBarItem Text="30km" ToolTip="30km" Value="30" />
                        <dx:TrackBarItem Text="40km" ToolTip="40km" Value="40" />
                        <dx:TrackBarItem Text="50km" ToolTip="50km" Value="50" />
                        <dx:TrackBarItem Text="60km" ToolTip="60km" Value="60" />
                        <dx:TrackBarItem Text="70km" ToolTip="70km" Value="70" />
                        <dx:TrackBarItem Text="80km" ToolTip="80km" Value="80" />
                        <dx:TrackBarItem Text="90km" ToolTip="90km" Value="90" />
                        <dx:TrackBarItem Text="100km" ToolTip="100km" Value="100" />
                        <dx:TrackBarItem Text="120km" ToolTip="120km" Value="120" />
                        <dx:TrackBarItem Text="150km" ToolTip="150km" Value="150" />
                        <dx:TrackBarItem Text="200km" ToolTip="200km" Value="200" />
                        <dx:TrackBarItem Text="300km" ToolTip="300km" Value="300" />
                        <dx:TrackBarItem Text="400km" ToolTip="400km" Value="400" />
                        <dx:TrackBarItem Text="500km" ToolTip="500km" Value="500" />
                        <dx:TrackBarItem Text="600km" ToolTip="600km" Value="600" />
                        <dx:TrackBarItem Text="700km" ToolTip="700km" Value="700" />
                        <dx:TrackBarItem Text="800km" ToolTip="800km" Value="800" />
                        <dx:TrackBarItem Text="900km" ToolTip="900km" Value="900" />
                        <dx:TrackBarItem Text="1000km" ToolTip="1000km" Value="1000" />
                        <dx:TrackBarItem Text="1200km" ToolTip="1200km" Value="1200" />
                        <dx:TrackBarItem Text="1400km" ToolTip="1400km" Value="1400" />
                        <dx:TrackBarItem Text="1600km" ToolTip="1600km" Value="1600" />
                        <dx:TrackBarItem Text="2000km" ToolTip="2000km" Value="2000" />
                        <dx:TrackBarItem Text="3000km" ToolTip="3000km" Value="3000" />
                        <dx:TrackBarItem Text="4000km" ToolTip="4000km" Value="4000" />
                        <dx:TrackBarItem Text="5000km" ToolTip="5000km" Value="5000" />
                        <dx:TrackBarItem Text="6000km" ToolTip="6000km" Value="6000" />
                        <dx:TrackBarItem Text="7000km" ToolTip="7000km" Value="7000" />
                        <dx:TrackBarItem Text="8000km" ToolTip="8000km" Value="8000" />
                        <dx:TrackBarItem Text="9000km" ToolTip="9000km" Value="9000" />
                        <dx:TrackBarItem Text="10000km" ToolTip="10000km" Value="10000" />
                    </Items>
                    <ClientSideEvents Init="TrackBar_Init" PositionChanging="TrackBar_PositionChanging" PositionChanged="TrackBar_PositionChanged" />
                </dx:ASPxTrackBar>
                <div>
                    <div class="trackbar-pos-val"><asp:Literal ID="msg_DistanceVal" runat="server" Text="Any Distance"/></div>
                </div>
            </div>
        </div>

        <div id="criteria_height" class="lfloat crt-wrap">
            <div class="criteria trackbar tb-height">
                <div class="trackbar-title"><asp:Literal ID="Literal1" runat="server" Text="Ύψος"/></div>
                <dx:ASPxTrackBar runat="server" ID="trcHeight" MinValue="0" MaxValue="26" Step="1"
                    LargeTickInterval="4" SmallTickFrequency="4" 
                    Width="200" AllowRangeSelection="true"
                    PositionStart="0" PositionEnd="26" 
                    ClientInstanceName="trcHeight" 
                    EnableTheming="False" ScaleLabelHighlightMode="HandlePosition" 
                    ShowChangeButtons="False">
                    <Items>
                        <dx:TrackBarItem Text="&lt;152cm" ToolTip="&lt;152cm" Value="29" />
                        <dx:TrackBarItem Text="152cm" ToolTip="152cm" Value="30" />
                        <dx:TrackBarItem Text="155cm" ToolTip="155cm" Value="31" />
                        <dx:TrackBarItem Text="157cm" ToolTip="157cm" Value="32" />
                        <dx:TrackBarItem Text="160cm" ToolTip="160cm" Value="33" />
                        <dx:TrackBarItem Text="163cm" ToolTip="163cm" Value="34" />
                        <dx:TrackBarItem Text="165cm" ToolTip="165cm" Value="35" />
                        <dx:TrackBarItem Text="168cm" ToolTip="168cm" Value="36" />
                        <dx:TrackBarItem Text="170cm" ToolTip="170cm" Value="37" />
                        <dx:TrackBarItem Text="173cm" ToolTip="173cm" Value="38" />
                        <dx:TrackBarItem Text="175cm" ToolTip="175cm" Value="39" />
                        <dx:TrackBarItem Text="178cm" ToolTip="178cm" Value="40" />
                        <dx:TrackBarItem Text="180cm" ToolTip="180cm" Value="41" />
                        <dx:TrackBarItem Text="183cm" ToolTip="183cm" Value="42" />
                        <dx:TrackBarItem Text="185cm" ToolTip="185cm" Value="43" />
                        <dx:TrackBarItem Text="188cm" ToolTip="188cm" Value="44" />
                        <dx:TrackBarItem Text="191cm" ToolTip="191cm" Value="45" />
                        <dx:TrackBarItem Text="193cm" ToolTip="193cm" Value="46" />
                        <dx:TrackBarItem Text="196cm" ToolTip="196cm" Value="47" />
                        <dx:TrackBarItem Text="198cm" ToolTip="198cm" Value="48" />
                        <dx:TrackBarItem Text="201cm" ToolTip="201cm" Value="49" />
                        <dx:TrackBarItem Text="203cm" ToolTip="203cm" Value="50" />
                        <dx:TrackBarItem Text="206cm" ToolTip="206cm" Value="51" />
                        <dx:TrackBarItem Text="208cm" ToolTip="208cm" Value="52" />
                        <dx:TrackBarItem Text="211cm" ToolTip="211cm" Value="53" />
                        <dx:TrackBarItem Text="213cm" ToolTip="213cm" Value="54" />
                        <dx:TrackBarItem Text="&gt; 213cm" ToolTip="&gt; 213cm" Value="55" />
                    </Items>
                    <ClientSideEvents Init="TrackBar_Init" PositionChanging="TrackBar_PositionChanging" PositionChanged="TrackBar_PositionChanged" />
                </dx:ASPxTrackBar>
                <div>
                    <div id="height-min" class="trackbar-pos-val lfloat"><asp:Literal ID="msg_HeightMin" runat="server" Text="&lt;152cm"/></div>
                    <div id="height-max" class="trackbar-pos-val rfloat"><asp:Literal ID="msg_HeightMax" runat="server" Text="&gt;213cm"/></div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>

        <div id="Criteria">
            <div class="cbp-content">
                <h3>
                    <asp:Literal ID="msg_LocationSectionTitle" runat="server" /></h3>
                <asp:HiddenField ID="hdfLocationStatus" runat="server" />
                <dx:ASPxCallbackPanel ID="cbpnlZip" runat="server" ClientInstanceName="cbpnlZip"
                    CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" ShowLoadingPanel="False"
                    ShowLoadingPanelImage="False">
                    <ClientSideEvents
                        EndCallback="onhi"
                        CallbackError="onhi" />
                    <LoadingPanelImage Url="~/App_Themes/DevEx/Web/Loading.gif">
                    </LoadingPanelImage>
                    <LoadingPanelStyle ImageSpacing="5px">
                    </LoadingPanelStyle>
                    <PanelCollection>
                       
                        <dx:PanelContent ID="PanelContent1" CssClass="CountryRegion" runat="server" SupportsDisabledAttribute="True">
                            <div class="Line">
                                <div class="lfloat crt-box">
                                    <h6>
                                        <asp:Label ID="msg_SelectCountry" runat="server" Text="" CssClass="psLabel1" /></h6>
                                    <dx:ASPxComboBox ID="cbCountry" runat="server" EncodeHtml="False" Height="18px" IncrementalFilteringMode="StartsWith">
                                        <ClientSideEvents BeginCallback="ShowLoadingOnMenu" SelectedIndexChanged="function(s, e) {  OnCountryChanged(s); }" />
                                    </dx:ASPxComboBox>
                                </div>

                                <div class="rfloat crt-box">
                                    <h6>
                                        <asp:Label ID="msg_SelectRegionText" runat="server" CssClass="psLabel1" Text="" /></h6>
                                    <dx:ASPxComboBox ID="cbRegion" runat="server" ClientInstanceName="cbRegion" DataSourceID="sdsRegion"
                                        EnableSynchronization="False" EncodeHtml="False" IncrementalFilteringMode="StartsWith"
                                        TextField="region1" ValueField="region1">
                                        <ClientSideEvents BeginCallback="ShowLoadingOnMenu" SelectedIndexChanged="function(s, e) { OnRegionChanged(s); }" />
                                    </dx:ASPxComboBox>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="Line">
                                <div class="lfloat crt-box">
                                    <h6>
                                        <asp:Label ID="msg_SelectCity" runat="server" CssClass="psLabel1" Text="" /></h6>
                                    <dx:ASPxComboBox ID="cbCity" runat="server" ClientInstanceName="cbCity" DataSourceID="sdsCity"
                                        EnableIncrementalFiltering="True" EnableSynchronization="False" EncodeHtml="False"
                                        Height="18px" IncrementalFilteringMode="StartsWith" TextField="city" ValueField="city">
                                        <ClientSideEvents SelectedIndexChanged="function(s, e) {  OnCityChanged(s); }" />
                                    </dx:ASPxComboBox>
                                </div>
                                <div class="rfloat crt-box">
                                    <h6>
                                        <asp:Label ID="msg_ZipCodeText" runat="server" CssClass="psLabel1" /></h6>
                                    <dx:ASPxTextBox ID="txtZip" CssClass="txtZip" runat="server" autocomplete="off" />
                                    <asp:Label ID="msg_ZipExample" CssClass="msg_ZipExample" runat="server" Style="font-size: 12px;" />
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="Line">
                                <div class="lfloat crt-box">
                                    <h6>
                                        <asp:Label ID="msg_DistanceFromMeText" runat="server" CssClass="psLabel1" /></h6>
                                    <uc4:DistanceDropDown ID="ddldistanceAdv" runat="server" AutoPostBack="false" />
                                </div>
                                <div class="rfloat crt-box">
                                    <h6>
                                        &nbsp;</h6>
                                     <dx:ASPxCheckBox ID="chkTravelOnDate" runat="server" CssClass="check-box TravelOnDate" Text="User is Willing To Travel"
                                    EncodeHtml="False">
                                </dx:ASPxCheckBox>
                                </div>
                                <div class="clear"></div>
                            </div>
                           <div>
                            </div>
                                                      

                            <asp:SqlDataSource ID="sdsRegion" runat="server"
                                ConnectionString="<%$ ConnectionStrings:AppDBconnectionString %>"
                                SelectCommand="SELECT DISTINCT region1 FROM SYS_GEO_GR WHERE (countrycode = @countrycode) AND (language = @language)">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="cbCountry" DefaultValue="GR"
                                        Name="countrycode" PropertyName="Value" />
                                    <asp:Parameter DefaultValue="EN" Name="language" />
                                </SelectParameters>
                            </asp:SqlDataSource>

                            <asp:SqlDataSource ID="sdsCity" runat="server" ConnectionString="<%$ ConnectionStrings:AppDBconnectionString %>"
                                SelectCommand="SELECT DISTINCT city FROM SYS_GEO_GR WHERE (region1 = @region1) AND (language = @language)">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="cbRegion" Name="region1" PropertyName="Value" />
                                    <asp:Parameter DefaultValue="EN" Name="language" />
                                </SelectParameters>
                            </asp:SqlDataSource>

                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxCallbackPanel>
                <div class="clear"></div>


            </div>

            <h3 class="paragraph">
                <asp:Literal ID="msg_PersonalInfoSectionTitle" runat="server" /></h3>

            <div class="crt-box">
                <div class="lfloat">
                    <!-- Age -->
                    <h6 class="lfloat cr-word">
                        <asp:Label ID="msg_AgeWord" class="psLabel1" runat="server" /></h6>
                    <%--<div class="lfloat cr-word"><asp:Label ID="msg_AgeFromWord" class="help-inline" runat="server"/></div>--%>
                    <div class="lfloat cr-control">
                        <uc1:AgeDropDown ID="ageMin2" runat="server" CssClass="input-small" DefaultValue="Minimum" />
                    </div>
                    <div class="lfloat cr-word" style="color: #909aa4">
                        <asp:Label ID="msg_AgeToWord" class="help-inline" runat="server" />
                    </div>
                    <div class="lfloat cr-control">
                        <uc1:AgeDropDown ID="ageMax2" runat="server" CssClass="input-small" DefaultValue="Maximum" />
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="rfloat">
                    <!-- height -->
                    <div class="rfloat cr-control">
                        <dx:ASPxComboBox ID="cbHeightMax" runat="server" EncodeHtml="False" CssClass="input-small" Width="90px"></dx:ASPxComboBox>
                    </div>
                    <div class="rfloat cr-word" style="color: #909aa4">
                        <span class="help-inline">
                            <asp:Literal ID="msg_HeightToWord" runat="server" /></span>
                    </div>
                    <div class="rfloat cr-control">
                        <dx:ASPxComboBox ID="cbHeightMin" runat="server" EncodeHtml="False" CssClass="input-small" Width="90px"></dx:ASPxComboBox>
                    </div>
                    <%--<div class="rfloat" style="margin-left:10px;"><asp:Label class="help-inline" ID="msg_FromWord" runat="server"/></div>--%>
                    <h6 class="rfloat cr-word">
                        <asp:Label ID="msg_PersonalHeight" runat="server" Text="" CssClass="psLabel1" /></h6>
                    <div class="clear"></div>
                </div>

                <div class="clear"></div>
            </div>


            <div class="crt-lists">

                <div class="crt-list3 lfloat">
                    <h6>
                        <asp:Label ID="msg_PersonalEyeClr" runat="server" Text="" CssClass="psLabel1" /></h6>
                    <div class="crt-list-content">
                        <asp:CheckBoxList ID="cbEyeColor" runat="server"></asp:CheckBoxList>
                    </div>
                    <table class="left-table">
                        <tr>
                            <td>
                                <asp:HyperLink ID="lnkEyeClr" runat="server" NavigateUrl="javascript:void(0);">Clear</asp:HyperLink></td>
                            <td>&nbsp;&nbsp;&nbsp;</td>
                            <td>
                                <asp:HyperLink ID="lnkEyeAll" runat="server" NavigateUrl="javascript:void(0);">Select All</asp:HyperLink></td>
                        </tr>
                    </table>
                    <script type="text/javascript">
                        $("#<%= lnkEyeClr.ClientID%>").click(function () { ClearCheckBoxes("#<%= cbEyeColor.ClientID%>") });
                        $("#<%= lnkEyeAll.ClientID%>").click(function () { SelectCheckBoxes("#<%= cbEyeColor.ClientID%>") });
                    </script>
                </div>

                <div class="crt-list3 lfloat">
                    <h6>
                        <asp:Label ID="msg_PersonalHairClr" runat="server" Text="" CssClass="psLabel1" /></h6>
                    <div class="crt-list-content">
                        <asp:CheckBoxList ID="cbHairColor" runat="server"></asp:CheckBoxList>
                    </div>
                    <table class="left-table">
                        <tr>
                            <td>
                                <asp:HyperLink ID="lnkHairClr" runat="server" NavigateUrl="javascript:void(0);">Clear</asp:HyperLink></td>
                            <td>&nbsp;&nbsp;&nbsp;</td>
                            <td>
                                <asp:HyperLink ID="lnkHairAll" runat="server" NavigateUrl="javascript:void(0);">Select All</asp:HyperLink></td>
                        </tr>
                    </table>
                    <script type="text/javascript">
                        $("#<%= lnkHairClr.ClientID%>").click(function () { ClearCheckBoxes("#<%= cbHairColor.ClientID%>") });
                        $("#<%= lnkHairAll.ClientID%>").click(function () { SelectCheckBoxes("#<%= cbHairColor.ClientID%>") });
                    </script>
                </div>


                <!-- body type -->
                <div class="crt-list3 lfloat">
                    <h6>
                        <asp:Label ID="msg_PersonalBodyType" runat="server" Text="" CssClass="psLabel1" /></h6>
                    <div class="crt-list-content">
                        <asp:CheckBoxList ID="cbBodyType" runat="server">
                        </asp:CheckBoxList>
                    </div>
                    <table class="left-table">
                        <tr>
                            <td>
                                <asp:HyperLink ID="lnkBodyClr" runat="server" NavigateUrl="javascript:void(0);">Clear</asp:HyperLink></td>
                            <td>&nbsp;&nbsp;&nbsp;</td>
                            <td>
                                <asp:HyperLink ID="lnkBodyAll" runat="server" NavigateUrl="javascript:void(0);">Select All</asp:HyperLink></td>
                        </tr>
                    </table>
                    <script type="text/javascript">
                        $("#<%= lnkBodyClr.ClientID%>").click(function () { ClearCheckBoxes("#<%= cbBodyType.ClientID%>") });
                        $("#<%= lnkBodyAll.ClientID%>").click(function () { SelectCheckBoxes("#<%= cbBodyType.ClientID%>") });
                    </script>
                </div>
                <div class="clear"></div>
            </div>


            <h3 class="paragraph">
                <asp:Literal ID="msg_OtherSectionTitle" runat="server" /></h3>

            <div class="crt-lists">
                <div class="crt-list2 lfloat">
                    <h6>
                        <asp:Label ID="msg_RelationshipStatus" runat="server" Text="" CssClass="psLabel1" /></h6>
                    <div class="crt-list-content">
                        <asp:CheckBoxList ID="cbRelationshipStatus" runat="server" Width="310px"></asp:CheckBoxList>
                    </div>
                    <table class="left-table">
                        <tr>
                            <td>
                                <asp:HyperLink ID="lnkRelStatClr" runat="server" NavigateUrl="javascript:void(0);">Clear</asp:HyperLink></td>
                            <td>&nbsp;&nbsp;&nbsp;</td>
                            <td>
                                <asp:HyperLink ID="lnkRelStatAll" runat="server" NavigateUrl="javascript:void(0);">Select All</asp:HyperLink></td>
                        </tr>
                    </table>
                    <script type="text/javascript">
                        $("#<%= lnkRelStatClr.ClientID%>").click(function () { ClearCheckBoxes("#<%= cbRelationshipStatus.ClientID%>") });
                        $("#<%= lnkRelStatAll.ClientID%>").click(function () { SelectCheckBoxes("#<%= cbRelationshipStatus.ClientID%>") });
                    </script>
                </div>

                <div class="crt-list2 lfloat">
                    <h6>
                        <asp:Label ID="msg_TypeOfdating" runat="server" Text="" CssClass="psLabel1" /></h6>
                    <div class="crt-list-content">
                        <asp:CheckBoxList ID="cbDatingType" runat="server"></asp:CheckBoxList>
                    </div>
                    <table class="left-table">
                        <tr>
                            <td>
                                <asp:HyperLink ID="lnkDatingTypeClr" runat="server" NavigateUrl="javascript:void(0);">Clear</asp:HyperLink></td>
                            <td>&nbsp;&nbsp;&nbsp;</td>
                            <td>
                                <asp:HyperLink ID="lnkDatingTypeAll" runat="server" NavigateUrl="javascript:void(0);">Select All</asp:HyperLink></td>
                        </tr>
                    </table>
                    <script type="text/javascript">
                        $("#<%= lnkDatingTypeClr.ClientID%>").click(function () { ClearCheckBoxes("#<%= cbDatingType.ClientID%>") });
                        $("#<%= lnkDatingTypeAll.ClientID%>").click(function () { SelectCheckBoxes("#<%= cbDatingType.ClientID%>") });
                    </script>
                </div>


                <div class="clear"></div>
            </div>

            <div class="crt-lists">
                <div class="crt-list2 lfloat">
                    <h6>
                        <asp:Label ID="msg_OtherEducat" runat="server" Text="" CssClass="psLabel1" /></h6>
                    <div class="crt-list-content">
                        <asp:CheckBoxList ID="cbEducation" runat="server"></asp:CheckBoxList>
                    </div>
                    <table class="left-table">
                        <tr>
                            <td>
                                <asp:HyperLink ID="lnkEducClr" runat="server" NavigateUrl="javascript:void(0);">Clear</asp:HyperLink></td>
                            <td>&nbsp;&nbsp;&nbsp;</td>
                            <td>
                                <asp:HyperLink ID="lnkEducAll" runat="server" NavigateUrl="javascript:void(0);">Select All</asp:HyperLink></td>
                        </tr>
                    </table>
                    <script type="text/javascript">
                        $("#<%= lnkEducClr.ClientID%>").click(function () { ClearCheckBoxes("#<%= cbEducation.ClientID%>") });
                        $("#<%= lnkEducAll.ClientID%>").click(function () { SelectCheckBoxes("#<%= cbEducation.ClientID%>") });
                    </script>
                </div>

                <div class="crt-list2 lfloat">
                    <h6>
                        <asp:Label ID="msg_PersonalChildren" runat="server" Text="" CssClass="psLabel1" /></h6>
                    <div class="crt-list-content">
                        <asp:CheckBoxList ID="cbChildren" runat="server"></asp:CheckBoxList>
                    </div>
                    <table class="left-table">
                        <tr>
                            <td>
                                <asp:HyperLink ID="lnkChildClr" runat="server" NavigateUrl="javascript:void(0);">Clear</asp:HyperLink></td>
                            <td>&nbsp;&nbsp;&nbsp;</td>
                            <td>
                                <asp:HyperLink ID="lnkChildAll" runat="server" NavigateUrl="javascript:void(0);">Select All</asp:HyperLink></td>
                        </tr>
                    </table>
                    <script type="text/javascript">
                        $("#<%= lnkChildClr.ClientID%>").click(function () { ClearCheckBoxes("#<%= cbChildren.ClientID%>") });
                        $("#<%= lnkChildAll.ClientID%>").click(function () { SelectCheckBoxes("#<%= cbChildren.ClientID%>") });
                    </script>
                </div>

                <div class="clear"></div>
            </div>

            <div class="crt-lists">

                <div class="crt-list2 lfloat">
                    <h6>
                        <asp:Label ID="msg_PersonalEthnicity" runat="server" Text="" CssClass="psLabel1" /></h6>
                    <div class="crt-list-content">
                        <asp:CheckBoxList ID="cbEthnicity" runat="server"></asp:CheckBoxList>
                    </div>
                    <table class="left-table">
                        <tr>
                            <td>
                                <asp:HyperLink ID="lnkEthnClr" runat="server" NavigateUrl="javascript:void(0);">Clear</asp:HyperLink></td>
                            <td>&nbsp;&nbsp;&nbsp;</td>
                            <td>
                                <asp:HyperLink ID="lnkEthnAll" runat="server" NavigateUrl="javascript:void(0);">Select All</asp:HyperLink></td>
                        </tr>
                    </table>
                    <script type="text/javascript">
                        $("#<%= lnkEthnClr.ClientID%>").click(function () { ClearCheckBoxes("#<%= cbEthnicity.ClientID%>") });
                        $("#<%= lnkEthnAll.ClientID%>").click(function () { SelectCheckBoxes("#<%= cbEthnicity.ClientID%>") });
                    </script>
                </div>

                <div class="crt-list2 lfloat">
                    <h6>
                        <asp:Label ID="msg_PersonalReligion" runat="server" Text="" CssClass="psLabel1" /></h6>
                    <div class="crt-list-content">
                        <asp:CheckBoxList ID="cbReligion" runat="server"></asp:CheckBoxList>
                    </div>
                    <table class="left-table">
                        <tr>
                            <td>
                                <asp:HyperLink ID="lnkReligClr" runat="server" NavigateUrl="javascript:void(0);">Clear</asp:HyperLink></td>
                            <td>&nbsp;&nbsp;&nbsp;</td>
                            <td>
                                <asp:HyperLink ID="lnkReligAll" runat="server" NavigateUrl="javascript:void(0);">Select All</asp:HyperLink></td>
                        </tr>
                    </table>
                    <script type="text/javascript">
                        $("#<%= lnkReligClr.ClientID%>").click(function () { ClearCheckBoxes("#<%= cbReligion.ClientID%>") });
                        $("#<%= lnkReligAll.ClientID%>").click(function () { SelectCheckBoxes("#<%= cbReligion.ClientID%>") });
                    </script>
                </div>

                <div class="clear"></div>
            </div>


            <div class="crt-lists">

                <div class="crt-list2 lfloat">
                    <h6>
                        <asp:Label ID="msg_PersonalSmokingHabit" runat="server" Text="" CssClass="psLabel1" /></h6>
                    <div class="crt-list-content">
                        <asp:CheckBoxList ID="cbSmoking" runat="server"></asp:CheckBoxList>
                    </div>
                    <table class="left-table">
                        <tr>
                            <td>
                                <asp:HyperLink ID="lnkSmokeClr" runat="server" NavigateUrl="javascript:void(0);">Clear</asp:HyperLink></td>
                            <td>&nbsp;&nbsp;&nbsp;</td>
                            <td>
                                <asp:HyperLink ID="lnkSmokeAll" runat="server" NavigateUrl="javascript:void(0);">Select All</asp:HyperLink></td>
                        </tr>
                    </table>
                    <script type="text/javascript">
                        $("#<%= lnkSmokeClr.ClientID%>").click(function () { ClearCheckBoxes("#<%= cbSmoking.ClientID%>") });
                        $("#<%= lnkSmokeAll.ClientID%>").click(function () { SelectCheckBoxes("#<%= cbSmoking.ClientID%>") });
                    </script>
                </div>

                <div class="crt-list2 lfloat">
                    <h6>
                        <asp:Label ID="msg_PersonalDrinkingHabit" runat="server" Text="" CssClass="psLabel1" /></h6>
                    <div class="crt-list-content">
                        <asp:CheckBoxList ID="cbDrinking" runat="server"></asp:CheckBoxList>
                    </div>
                    <table class="left-table">
                        <tr>
                            <td>
                                <asp:HyperLink ID="lnkDrinkClr" runat="server" NavigateUrl="javascript:void(0);">Clear</asp:HyperLink></td>
                            <td>&nbsp;&nbsp;&nbsp;</td>
                            <td>
                                <asp:HyperLink ID="lnkDrinkAll" runat="server" NavigateUrl="javascript:void(0);">Select All</asp:HyperLink></td>
                        </tr>
                    </table>
                    <script type="text/javascript">
                        $("#<%= lnkDrinkClr.ClientID%>").click(function () { ClearCheckBoxes("#<%= cbDrinking.ClientID%>") });
                        $("#<%= lnkDrinkAll.ClientID%>").click(function () { SelectCheckBoxes("#<%= cbDrinking.ClientID%>") });
                    </script>
                </div>

                <div class="clear"></div>
            </div>

            <div class="crt-lists">
                <div class="crt-list2 lfloat" id="divIncome" runat="server">
                    <h6>
                        <asp:Label ID="msg_OtherAnnual" runat="server" Text="" CssClass="psLabel1" /></h6>
                    <div class="controls">
                        <asp:CheckBoxList ID="cbIncome" runat="server"></asp:CheckBoxList>
                    </div>
                    <table class="left-table">
                        <tr>
                            <td>
                                <asp:HyperLink ID="lnkIncomeClr" runat="server" NavigateUrl="javascript:void(0);">Clear</asp:HyperLink></td>
                            <td>&nbsp;&nbsp;&nbsp;</td>
                            <td>
                                <asp:HyperLink ID="lnkIncomeAll" runat="server" NavigateUrl="javascript:void(0);">Select All</asp:HyperLink></td>
                        </tr>
                    </table>
                    <script type="text/javascript">
                        $("#<%= lnkIncomeClr.ClientID%>").click(function () { ClearCheckBoxes("#<%= cbIncome.ClientID%>") });
                        $("#<%= lnkIncomeAll.ClientID%>").click(function () { SelectCheckBoxes("#<%= cbIncome.ClientID%>") });
                    </script>
                </div>
                <div class="clear"></div>
            </div>


            <div class="form-actions">
                <dx:ASPxButton ID="btnSaveAndFind" runat="server" ClientInstanceName="btnSaveAndFind"
                    CssClass="btn btn-large btn-primary" Text="Save Search and Show Results"
                    Native="True" EncodeHtml="False"
                    UseSubmitBehavior="False">
               <%--     <ClientSideEvents Click="btnSaveAndFind_Clicked" />--%>
                </dx:ASPxButton>
            </div>
            <asp:HiddenField ID="hdfNewName" runat="server" />
            <asp:HiddenField ID="hdfSelectedName" runat="server" />
        </div>




    </div>

    <uc2:WhatIsIt ID="WhatIsIt1" runat="server" />


    <dx:ASPxPopupControl runat="server"
        Height="340px" Width="500px"
        ID="popupSearchName"
        ClientInstanceName="popupSearchName"
        ClientIDMode="AutoID"
        AllowDragging="True"
        PopupVerticalAlign="WindowCenter"
        PopupHorizontalAlign="WindowCenter"
        Modal="True"
        CloseAction="CloseButton"
        AllowResize="True"
        AutoUpdatePosition="True"
        ShowShadow="False">
        <ContentStyle HorizontalAlign="Left" VerticalAlign="Middle">
            <Paddings Padding="0px" PaddingTop="25px" />
            <Paddings Padding="0px" PaddingTop="25px"></Paddings>
        </ContentStyle>
        <CloseButtonStyle BackColor="White">
            <HoverStyle>
                <BackgroundImage ImageUrl="//cdn.goomena.com/Images2/popup-wrn/close.png" Repeat="NoRepeat" HorizontalPosition="center"
                    VerticalPosition="center"></BackgroundImage>
            </HoverStyle>
            <BackgroundImage ImageUrl="//cdn.goomena.com/Images2/popup-wrn/close.png" Repeat="NoRepeat" HorizontalPosition="center"
                VerticalPosition="center"></BackgroundImage>
            <BorderLeft BorderColor="White" BorderStyle="Solid" BorderWidth="10px" />
        </CloseButtonStyle>
        <CloseButtonImage Url="//cdn.goomena.com/Images/spacer10.png" Height="43px" Width="43px">
        </CloseButtonImage>
        <SizeGripImage Height="40px" Url="//cdn.goomena.com/Images/resize.png" Width="40px">
        </SizeGripImage>
        <HeaderStyle>
            <Paddings Padding="0px" PaddingLeft="0px" />
            <Paddings PaddingTop="0px" PaddingBottom="0px"></Paddings>
            <BorderTop BorderColor="White" BorderStyle="Solid" BorderWidth="5px" />
            <BorderRight BorderColor="White" BorderStyle="Solid" BorderWidth="5px" />
            <BorderBottom BorderColor="White" BorderStyle="Solid" BorderWidth="5px" />
        </HeaderStyle>
        <ModalBackgroundStyle BackColor="Black" Opacity="70" CssClass="WhatIs-ModalBG"></ModalBackgroundStyle>
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server" SupportsDisabledAttribute="True">
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>


</asp:Content>


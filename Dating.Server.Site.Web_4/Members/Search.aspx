﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Members/Members2Inner.Master" MaintainScrollPositionOnPostback="false"
     CodeBehind="Search.aspx.vb" EnableEventValidation="true" Inherits="Dating.Server.Site.Web.Search" %>

<%@ Register Assembly="DevExpress.Web.v13.1, Version=13.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>

<%--<%@ Register src="../UserControls/ProfilePreview.ascx" tagname="ProfilePreview" tagprefix="uc1" %>--%>

<%@ Register src="~/UserControls/MemberLeftPanel.ascx" tagname="MemberLeftPanel" tagprefix="uc3" %>
<%@ Register src="~/UserControls/AgeDropDown.ascx" tagname="AgeDropDown" tagprefix="uc1" %>
<%@ Register src="~/UserControls/DistanceDropDown.ascx" tagname="DistanceDropDown" tagprefix="uc4" %>
<%@ Register src="~/UserControls/WhatIsIt.ascx" tagname="WhatIsIt" tagprefix="uc2" %>
<%@ Register src="~/UserControls/QuickSearchCriteriasMan2.ascx" tagname="QuickSearchCriteriasMan2" tagprefix="uc2" %>
<%@ Register src="~/UserControls/QuickSearchCriteriasWoman.ascx" tagname="QuickSearchCriteriasWoman" tagprefix="uc2" %>
<%@ Register Src="~/UserControls/SearchControlBoxView.ascx" TagPrefix="uc2" TagName="SearchControl" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   	<script type="text/javascript" src="//cdn.goomena.com/Scripts/fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>
	<script type="text/javascript" src="//cdn.goomena.com/Scripts/fancybox/source/jquery.fancybox.pack.js?v=2.1.4"></script>
	<script type="text/javascript" src="//cdn.goomena.com/Scripts/fancybox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
	<script type="text/javascript" src="//cdn.goomena.com/Scripts/fancybox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
	<script type="text/javascript" src="//cdn.goomena.com/Scripts/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.5"></script>
	<link rel="stylesheet" type="text/css" href="//cdn.goomena.com/Scripts/fancybox/source/jquery.fancybox.css?v=2.1.4" media="screen" />
	<link rel="stylesheet" type="text/css" href="//cdn.goomena.com/Scripts/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" />
	<link rel="stylesheet" type="text/css" href="//cdn.goomena.com/Scripts/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />
    <script type="text/javascript" src="/v1/Scripts/profile-view.js?v2"></script>
<script type="text/javascript">
    // <![CDATA[
    function OnCountryChanged(cbCountry) {
        LoadingPanel.Hide();
        cbpnlZip.PerformCallback("country_" + cbCountry.GetValue().toString());
        ShowLoading();
    }
    function OnRegionChanged(cbRegion) {
        LoadingPanel.Hide();
        cbpnlZip.PerformCallback("region_" + cbRegion.GetValue().toString());
        ShowLoading();
    }
    function onhi() {
        HideLoading();
    }
    function OnCityChanged(cbCity) {
    }
    function showZip() {
        region.hide()
        city.hide()

        var status = jQuery('#<%= hdfLocationStatus.ClientID %>');
        status.val("zip")
    }
    function showRegions() {
        zip.hide()
        region.show()
        city.show()
        var status = jQuery('#<%= hdfLocationStatus.ClientID %>');
        status.val("region")
    }

    jQuery(function ($) {
        scrollToLogin();

        $('#colapsibleForm').each(function () {
            var pe_box = this;
            // $('.other-criterias-text', pe_box).click(function () {
            //$('.other-criterias', pe_box).toggle('fast', function () {
            //Animation complete.
            //var isVisible = ($('.other-criterias', pe_box).css('display') == 'none');
            //if (isVisible) 
            //      $('a', '.other-criterias-text .rfloat').attr("title", '<%= globalStrings.GetCustomString("msg_ClickToExpand", GetLag() )  %>').removeClass("dd-open");
            //    else 
            //          $('a', '.other-criterias-text .rfloat').attr("title", '<%= globalStrings.GetCustomString("msg_ClickToCollapse", GetLag()) %>').addClass("dd-open");
            //    });
            //  });
        });
    });


    function RefreshResults() {
        if ($('#<%= btnSearch_ND.ClientID %>').length > 0) {
            var name = $('#<%= btnSearch_ND.ClientID %>')[0].name;
            __doPostBack(name, '');
        }
    }

    function RefreshResultsByUserName() {
        if ($('#<%= imgSearchByUserName.ClientID%>').length > 0) {
            var name = $('#<%= imgSearchByUserName.ClientID%>')[0].name;
            __doPostBack(name, '');
        }
    }

    function SelectCheckBoxes(scope) {
        $('input[type="checkbox"]', $(scope)).prop("checked", true);
    }
    function ClearCheckBoxes(scope) {
        $('input[type="checkbox"]', $(scope)).prop("checked", false);
    }
    function scrollToResultsTop() {
        scrollWin('#<%= divFilter.ClientID%>', -200, 500);
    }

    var isOkSavedSearchName;
    function SetNewSearchName(txt) {
        $('#<%= hdfNewName.ClientID%>').val(txt);
    }
   function SearchNamePopupSetUI() {
        var popup = popupSearchName
        var divId = popup.name + '_PW-1';
        $('#' + divId)
        .addClass('fancybox-popup')
        .prepend($('<div class="header-line"></div>'));
        var closeId = popup.name + '_HCB-1Img';
        $('#' + closeId).addClass('fancybox-popup-close');
        var hdrId = popup.name + '_PWH-1T';
        $('#' + hdrId).addClass('fancybox-popup-header');
        //var frmId = popup.name + '_CSD-1';
        //$('#' + frmId).addClass('iframe-container');
    }
    function ShowSearchNamePopup(title) {
        isOkSavedSearchName = false;
        SearchNamePopupSetUI();
        ShowDevExPopUpByURL('popupSearchName', '<%= ResolveUrl("~/loading.htm") %>', null, title);
        window["popupSearchName"].Closing.AddHandler(__Closing_Remove);
        //window["popupSearchName"].CloseUp.AddHandler(__CloseUp_Remove);
        var popup = popupSearchName
        setTimeout(function () {
            popup.SetContentUrl('SearchAdvancedSave.aspx?popup=popupSearchName');
        }, 500);

        function __Closing_Remove(s, e) {
            try {
                s.SetContentUrl('about:blank');
                s.Closing.RemoveHandler(__Closing_Remove);
                if (isOkSavedSearchName) {
                    setTimeout(function () {
                        btnSaveAndFind.DoClick();
                    }, 300);
                }
            }
            catch (e) { }
        }
        function __CloseUp_Remove(s, e) {
            try {
                s.CloseUp.RemoveHandler(__CloseUp_Remove);
            }
            catch (e) { }
        }

    }

    function btnSaveAndFind_Clicked(s, e) {
        if ('<%= Me.DontAskForSave.ToString().ToLower()%>' == 'false') {
            if (isOkSavedSearchName ||
                !stringIsEmpty($('#<%= Me.hdfSelectedName.ClientID%>').val())) {
                e.processOnServer = true;
            }
            else {
                e.processOnServer = false;
                ShowSearchNamePopup('Enter name for your search');
            }
        }
    }

    function SaveAndFindComplete() {
        isOkSavedSearchName = false;
    }

// ]]> 
</script>
     <script type="text/javascript">
         // <![CDATA[
         function CloseBotom(Item, Name) {
             $(Name + ' .CloseBox').fadeOut(100);
             $(Name).fadeOut(200);
             $(Name).animate({ height: "0px", paddingTop: 0, paddingBottom: 0 }, 350, "swing");
         }

         function ShowBotom(Item,Name) {
           
             $(Name).fadeIn(200);
             $(Name).animate({ height: "116px", paddingTop: 19, paddingBottom: 19 }, 350, "swing");
             setTimeout(function () { $(Name + ' .CloseBox').fadeIn(200); }, 500);
                     }

         function loadFancybox() {
             $("a[rel=prof_group]").fancybox({
                 'transitionIn': 'elastic',
                 'transitionOut': 'elastic',
                 'titlePosition': 'over',
                 'overlayColor': '#000'
             });
         }

         function pageLoad(sender, args) {
             loadFancybox();
             if (typeof setPopupWindowUI !== 'undefined') setPopupWindowUI();
         }

         function closePopup(popupName) {
             try {
                 top.window[popupName].Hide();
                 top.window[popupName].DoHideWindow(0);
             }
             catch (e) { }
         }


         // ]]> 
    </script>
    <style type="text/css">
    
    </style>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <div id="search-members" class ="NewSearch">

        <asp:MultiView ID="mvSearchMain" runat="server" ActiveViewIndex="1">

                <asp:View ID="vwSearchAdvanced" runat="server">
<%--<asp:Panel ID="pnlAdvSearch" runat="server">--%>
<%--</asp:Panel>--%>

    <h3 id="h3Dashboard2" class="page-title">
        <asp:Literal ID="msg_AdvancedSearchTitle" runat="server" />
        <asp:LinkButton ID="lnkViewDescription2" runat="server" CssClass="more_info2" OnClientClick="OnMoreInfoClick('[contentUrl]', this, '[headerText]');return false;" />
    </h3>
<div id="adv-search">
    

    <div class="cbp-content">
        <h3><asp:Literal ID="msg_LocationSectionTitle" runat="server"/></h3>
        <asp:HiddenField ID="hdfLocationStatus" runat="server" />
        <dx:ASPxCallbackPanel ID="cbpnlZip" runat="server" ClientInstanceName="cbpnlZip"
            CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" ShowLoadingPanel="False"
            ShowLoadingPanelImage="False">
            <ClientSideEvents 
                EndCallback="onhi" 
                CallbackError="onhi" />
            <LoadingPanelImage Url="~/App_Themes/DevEx/Web/Loading.gif">
            </LoadingPanelImage>
            <LoadingPanelStyle ImageSpacing="5px">
            </LoadingPanelStyle>
            <PanelCollection>
                <dx:PanelContent ID="PanelContent1" runat="server" SupportsDisabledAttribute="True">
                    <div class="crt-box">
                        <h6>
                            <asp:Label ID="msg_SelectCountry" runat="server" Text="" CssClass="psLabel1" /></h6>
                        <dx:ASPxComboBox ID="cbCountry" runat="server" EncodeHtml="False" Height="18px" IncrementalFilteringMode="StartsWith">
                            <ClientSideEvents BeginCallback="ShowLoadingOnMenu" SelectedIndexChanged="function(s, e) {  OnCountryChanged(s); }" />
                        </dx:ASPxComboBox>
                    </div>

                    <div class="crt-box">
                        <h6>
                            <asp:Label ID="msg_SelectRegionText" runat="server" CssClass="psLabel1" Text="" /></h6>
                        <dx:ASPxComboBox ID="cbRegion" runat="server" ClientInstanceName="cbRegion" DataSourceID="sdsRegion"
                            EnableSynchronization="False" EncodeHtml="False" IncrementalFilteringMode="StartsWith"
                            TextField="region1" ValueField="region1">
                            <ClientSideEvents BeginCallback="ShowLoadingOnMenu" SelectedIndexChanged="function(s, e) { OnRegionChanged(s); }" />
                        </dx:ASPxComboBox>
                    </div>

                    <div class="crt-box">
                        <h6>
                            <asp:Label ID="msg_SelectCity" runat="server" CssClass="psLabel1" Text="" /></h6>
                        <dx:ASPxComboBox ID="cbCity" runat="server" ClientInstanceName="cbCity" DataSourceID="sdsCity"
                            EnableIncrementalFiltering="True" EnableSynchronization="False" EncodeHtml="False"
                            Height="18px" IncrementalFilteringMode="StartsWith" TextField="city" ValueField="city">
                            <ClientSideEvents SelectedIndexChanged="function(s, e) {  OnCityChanged(s); }" />
                        </dx:ASPxComboBox>
                    </div>
                    <div>
                        <div class="lfloat crt-box">
                            <h6>
                                <asp:Label ID="msg_ZipCodeText" runat="server" CssClass="psLabel1" /></h6>
                            <dx:ASPxTextBox ID="txtZip" runat="server" autocomplete="off" />
                            <asp:Label ID="msg_ZipExample" runat="server" Style="font-size: 12px;" />
                        </div>
                        <div class="rfloat crt-box">
                            <h6>
                                <asp:Label ID="msg_DistanceFromMeText" runat="server" CssClass="psLabel1" /></h6>
                                <uc4:DistanceDropDown ID="ddldistanceAdv" runat="server" AutoPostBack="false" />
                        </div>
                        <div class="clear"></div>
                    </div>

                    <div class="crt-box">
                        <dx:ASPxCheckBox ID="chkTravelOnDate" runat="server" CssClass="check-box" Text="User is Willing To Travel"
                            EncodeHtml="False">
                        </dx:ASPxCheckBox>
                    </div>
                    <asp:SqlDataSource ID="sdsRegion" runat="server" 
ConnectionString="<%$ ConnectionStrings:AppDBconnectionString %>" 
SelectCommand="SELECT DISTINCT region1 FROM SYS_GEO_GR WHERE (countrycode = @countrycode) AND (language = @language)">
<SelectParameters>
    <asp:ControlParameter ControlID="cbCountry" DefaultValue="GR" 
        Name="countrycode" PropertyName="Value" />
    <asp:Parameter DefaultValue="EN" Name="language" />
</SelectParameters>
</asp:SqlDataSource>

<asp:SqlDataSource ID="sdsCity" runat="server" ConnectionString="<%$ ConnectionStrings:AppDBconnectionString %>"
SelectCommand="SELECT DISTINCT city FROM SYS_GEO_GR WHERE (region1 = @region1) AND (language = @language)">
<SelectParameters>
    <asp:ControlParameter ControlID="cbRegion" Name="region1" PropertyName="Value" />
    <asp:Parameter DefaultValue="EN" Name="language" />
</SelectParameters>
</asp:SqlDataSource>
           </dx:PanelContent>
            </PanelCollection>
        </dx:ASPxCallbackPanel>
        <div class="clear"></div>


        <div class="saved-search">
		    <div class="as_info">
                <asp:Literal ID="msg_AdvancedSearchDescription_ND" runat="server"/>
		    </div>

            <div class="controls">
                <div class="lfloat">
    <dx:ASPxComboBox ID="cbSavedSrch" runat="server" 
        EncodeHtml="False" 
        DataSourceID="sdsSavedSearch" TextField="Name" ValueField="SavedSearchId">
    </dx:ASPxComboBox>
                    <div>
                        <asp:LinkButton ID="btnDeleteSS" runat="server" Text="Delete" ToolTip="Delete" CssClass="as_recent_del" />
                    </div>
                </div>
                <div class="rfloat">
                    <asp:ImageButton ID="ibtSavedSearchLoad" runat="server" ImageUrl="//cdn.goomena.com/Images/spacer10.png" CssClass="btn imgSearchByUserName" /></div>
                <div class="clear"></div>
            </div>
            <asp:SqlDataSource ID="sdsSavedSearch" runat="server" 
                ConnectionString="<%$ ConnectionStrings:AppDBconnectionString %>" 
                SelectCommand="SELECT [SavedSearchId], [SearchText], [CreatedDate], [Name], [Distance], [ProfileId] FROM [EUS_SavedSearch] WHERE ([ProfileId] = @ProfileId) ORDER BY CreatedDate desc">
                <SelectParameters>
                    <asp:SessionParameter DefaultValue="0" Name="ProfileId" 
                        SessionField="ProfileID" Type="Int32" />
                </SelectParameters>
            </asp:SqlDataSource>


            <%--
            <asp:ListView ID="lvSavedSearches" runat="server" DataKeyNames="SavedSearchId" 
                DataSourceID="sdsSavedSearch">
                <LayoutTemplate>
                    <div ID="itemPlaceholderContainer" runat="server" style="">
                        <span runat="server" id="itemPlaceholder" />
                    </div>
                    <div style="">
                    </div>
                </LayoutTemplate>
                <EmptyDataTemplate>
               
                </EmptyDataTemplate>
                <ItemTemplate>
                    <table width="100%">
                        <tr>
                            <td valign="middle" ><asp:LinkButton ID="btnName" runat="server" Text='<%# Eval("Name") %>' CssClass="tt_enabled"  CommandName="LOADSEARCH" CommandArgument='<%# Eval("SavedSearchId") %>' /></td>
                            <td width="90%">&nbsp;</td><td><asp:LinkButton ID="btnDelete" runat="server" Text='<%# Eval("Name") %>' ToolTip="Delete" CssClass="as_recent_del" CommandName="DELETESEARCH" CommandArgument='<%# Eval("SavedSearchId") %>' /></td>
                        </tr>
                    </table>
                </ItemTemplate>
            </asp:ListView>
            --%>
    
        </div>
    </div>

    <h3 class="paragraph"><asp:Literal ID="msg_PersonalInfoSectionTitle" runat="server"/></h3>
        
    <div class="crt-box">
        <div class="lfloat">
        <!-- Age -->
            <h6 class="lfloat cr-word"><asp:Label ID="msg_AgeWord" class="psLabel1" runat="server" /></h6>
            <%--<div class="lfloat cr-word"><asp:Label ID="msg_AgeFromWord" class="help-inline" runat="server"/></div>--%>
            <div class="lfloat cr-control"><uc1:AgeDropDown ID="ageMin2" runat="server" CssClass="input-small" DefaultValue="Minimum" /></div>
            <div class="lfloat cr-word" style="color:#909aa4"><asp:Label ID="msg_AgeToWord" class="help-inline" runat="server"/></div>
            <div class="lfloat cr-control"><uc1:AgeDropDown ID="ageMax2" runat="server" CssClass="input-small" DefaultValue="Maximum" /></div>
            <div class="clear"></div>
        </div>

        <div class="rfloat">
        <!-- height -->
            <div class="rfloat cr-control"><dx:ASPxComboBox ID="cbHeightMax" runat="server" EncodeHtml="False" CssClass="input-small" Width="90px"></dx:ASPxComboBox></div>
            <div class="rfloat cr-word" style="color:#909aa4"><span class="help-inline"><asp:Literal ID="msg_HeightToWord" runat="server"/></span></div>
            <div class="rfloat cr-control"><dx:ASPxComboBox ID="cbHeightMin" runat="server" EncodeHtml="False" CssClass="input-small" Width="90px"></dx:ASPxComboBox></div>
            <%--<div class="rfloat" style="margin-left:10px;"><asp:Label class="help-inline" ID="msg_FromWord" runat="server"/></div>--%>
	        <h6 class="rfloat cr-word"><asp:Label ID="msg_PersonalHeight" runat="server" Text="" CssClass="psLabel1" /></h6>
            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>


    <div class="crt-lists">
    
        <div class="crt-list3 lfloat">
	        <h6><asp:Label ID="msg_PersonalEyeClr" runat="server" Text="" CssClass="psLabel1"  /></h6>
	        <div class="crt-list-content">
                <asp:CheckBoxList ID="cbEyeColor" runat="server"></asp:CheckBoxList>
	        </div>
            <table class="left-table">
                <tr>
                    <td><asp:HyperLink ID="lnkEyeClr" runat="server" NavigateUrl="javascript:void(0);">Clear</asp:HyperLink></td>
                    <td>&nbsp;&nbsp;&nbsp;</td>
                    <td><asp:HyperLink ID="lnkEyeAll" runat="server" NavigateUrl="javascript:void(0);">Select All</asp:HyperLink></td>
                </tr>
            </table>
            <script type="text/javascript">
                $("#<%= lnkEyeClr.ClientID%>").click(function () { ClearCheckBoxes("#<%= cbEyeColor.ClientID%>") });
                $("#<%= lnkEyeAll.ClientID%>").click(function () { SelectCheckBoxes("#<%= cbEyeColor.ClientID%>") });
            </script>
        </div>

        <div class="crt-list3 lfloat">
	        <h6><asp:Label ID="msg_PersonalHairClr" runat="server" Text="" CssClass="psLabel1"  /></h6>
	        <div class="crt-list-content">
                <asp:CheckBoxList ID="cbHairColor" runat="server"></asp:CheckBoxList>
            </div>
            <table class="left-table">
                <tr>
                    <td><asp:HyperLink ID="lnkHairClr" runat="server" NavigateUrl="javascript:void(0);">Clear</asp:HyperLink></td>
                    <td>&nbsp;&nbsp;&nbsp;</td>
                    <td><asp:HyperLink ID="lnkHairAll" runat="server" NavigateUrl="javascript:void(0);">Select All</asp:HyperLink></td>
                </tr>
            </table>
            <script type="text/javascript">
                $("#<%= lnkHairClr.ClientID%>").click(function () { ClearCheckBoxes("#<%= cbHairColor.ClientID%>") });
                $("#<%= lnkHairAll.ClientID%>").click(function () { SelectCheckBoxes("#<%= cbHairColor.ClientID%>") });
            </script>
        </div>

		
        <!-- body type -->
        <div class="crt-list3 lfloat">
	        <h6><asp:Label ID="msg_PersonalBodyType" runat="server" Text="" CssClass="psLabel1" /></h6>
            <div class="crt-list-content">
                <asp:CheckBoxList  ID="cbBodyType" runat="server">
                </asp:CheckBoxList >	
            </div>
            <table class="left-table">
                <tr>
                    <td><asp:HyperLink ID="lnkBodyClr" runat="server" NavigateUrl="javascript:void(0);">Clear</asp:HyperLink></td>
                    <td>&nbsp;&nbsp;&nbsp;</td>
                    <td><asp:HyperLink ID="lnkBodyAll" runat="server" NavigateUrl="javascript:void(0);">Select All</asp:HyperLink></td>
                </tr>
            </table>
            <script type="text/javascript">
                $("#<%= lnkBodyClr.ClientID%>").click(function () { ClearCheckBoxes("#<%= cbBodyType.ClientID%>") });
                $("#<%= lnkBodyAll.ClientID%>").click(function () { SelectCheckBoxes("#<%= cbBodyType.ClientID%>") });
            </script>
        </div>
        <div class="clear"></div>
    </div>


    <h3 class="paragraph"><asp:Literal ID="msg_OtherSectionTitle" runat="server"/></h3>

    <div class="crt-lists">
        <div class="crt-list2 lfloat">
	        <h6><asp:Label ID="msg_RelationshipStatus" runat="server" Text="" CssClass="psLabel1" /></h6>
	        <div class="crt-list-content">
                <asp:CheckBoxList ID="cbRelationshipStatus" runat="server" Width="310px"></asp:CheckBoxList >
            </div>
            <table class="left-table">
                <tr>
                    <td><asp:HyperLink ID="lnkRelStatClr" runat="server" NavigateUrl="javascript:void(0);">Clear</asp:HyperLink></td>
                    <td>&nbsp;&nbsp;&nbsp;</td>
                    <td><asp:HyperLink ID="lnkRelStatAll" runat="server" NavigateUrl="javascript:void(0);">Select All</asp:HyperLink></td>
                </tr>
            </table>
            <script type="text/javascript">
                $("#<%= lnkRelStatClr.ClientID%>").click(function () { ClearCheckBoxes("#<%= cbRelationshipStatus.ClientID%>") });
                $("#<%= lnkRelStatAll.ClientID%>").click(function () { SelectCheckBoxes("#<%= cbRelationshipStatus.ClientID%>") });
            </script>
        </div>

        <div class="crt-list2 lfloat">
	        <h6><asp:Label ID="msg_TypeOfdating" runat="server" Text="" CssClass="psLabel1" /></h6>
	        <div class="crt-list-content">
                <asp:CheckBoxList  ID="cbDatingType" runat="server"></asp:CheckBoxList>
   	        </div>
            <table class="left-table">
                <tr>
                    <td><asp:HyperLink ID="lnkDatingTypeClr" runat="server" NavigateUrl="javascript:void(0);">Clear</asp:HyperLink></td>
                    <td>&nbsp;&nbsp;&nbsp;</td>
                    <td><asp:HyperLink ID="lnkDatingTypeAll" runat="server" NavigateUrl="javascript:void(0);">Select All</asp:HyperLink></td>
                </tr>
            </table>
            <script type="text/javascript">
                $("#<%= lnkDatingTypeClr.ClientID%>").click(function () { ClearCheckBoxes("#<%= cbDatingType.ClientID%>") });
                $("#<%= lnkDatingTypeAll.ClientID%>").click(function () { SelectCheckBoxes("#<%= cbDatingType.ClientID%>") });
            </script>
        </div>
            

        <div class="clear"></div>
    </div>

    <div class="crt-lists">
        <div class="crt-list2 lfloat">
	        <h6><asp:Label ID="msg_OtherEducat" runat="server" Text="" CssClass="psLabel1" /></h6>
	        <div class="crt-list-content">
                <asp:CheckBoxList  ID="cbEducation" runat="server"></asp:CheckBoxList>
            </div>
            <table class="left-table">
                <tr>
                    <td><asp:HyperLink ID="lnkEducClr" runat="server" NavigateUrl="javascript:void(0);">Clear</asp:HyperLink></td>
                    <td>&nbsp;&nbsp;&nbsp;</td>
                    <td><asp:HyperLink ID="lnkEducAll" runat="server" NavigateUrl="javascript:void(0);">Select All</asp:HyperLink></td>
                </tr>
            </table>
            <script type="text/javascript">
                $("#<%= lnkEducClr.ClientID%>").click(function () { ClearCheckBoxes("#<%= cbEducation.ClientID%>") });
                $("#<%= lnkEducAll.ClientID%>").click(function () { SelectCheckBoxes("#<%= cbEducation.ClientID%>") });
            </script>
        </div>

        <div class="crt-list2 lfloat">
	        <h6><asp:Label ID="msg_PersonalChildren" runat="server" Text="" CssClass="psLabel1" /></h6>
	        <div class="crt-list-content">
                <asp:CheckBoxList  ID="cbChildren" runat="server"></asp:CheckBoxList>
            </div>
            <table class="left-table">
                <tr>
                    <td><asp:HyperLink ID="lnkChildClr" runat="server" NavigateUrl="javascript:void(0);">Clear</asp:HyperLink></td>
                    <td>&nbsp;&nbsp;&nbsp;</td>
                    <td><asp:HyperLink ID="lnkChildAll" runat="server" NavigateUrl="javascript:void(0);">Select All</asp:HyperLink></td>
                </tr>
            </table>
            <script type="text/javascript">
                $("#<%= lnkChildClr.ClientID%>").click(function () { ClearCheckBoxes("#<%= cbChildren.ClientID%>") });
                $("#<%= lnkChildAll.ClientID%>").click(function () { SelectCheckBoxes("#<%= cbChildren.ClientID%>") });
            </script>
        </div>

        <div class="clear"></div>
    </div>

    <div class="crt-lists">

        <div class="crt-list2 lfloat">
		    <h6><asp:Label ID="msg_PersonalEthnicity" runat="server" Text="" CssClass="psLabel1" /></h6>
	        <div class="crt-list-content">
                <asp:CheckBoxList ID="cbEthnicity" runat="server"></asp:CheckBoxList>	
            </div>
            <table class="left-table">
                <tr>
                    <td><asp:HyperLink ID="lnkEthnClr" runat="server" NavigateUrl="javascript:void(0);">Clear</asp:HyperLink></td>
                    <td>&nbsp;&nbsp;&nbsp;</td>
                    <td><asp:HyperLink ID="lnkEthnAll" runat="server" NavigateUrl="javascript:void(0);">Select All</asp:HyperLink></td>
                </tr>
            </table>
            <script type="text/javascript">
                $("#<%= lnkEthnClr.ClientID%>").click(function () { ClearCheckBoxes("#<%= cbEthnicity.ClientID%>") });
                $("#<%= lnkEthnAll.ClientID%>").click(function () { SelectCheckBoxes("#<%= cbEthnicity.ClientID%>") });
            </script>
        </div>

        <div class="crt-list2 lfloat">
		    <h6><asp:Label ID="msg_PersonalReligion" runat="server" Text="" CssClass="psLabel1" /></h6>
	        <div class="crt-list-content">
                <asp:CheckBoxList  ID="cbReligion" runat="server"></asp:CheckBoxList >
	        </div>
            <table class="left-table">
                <tr>
                    <td><asp:HyperLink ID="lnkReligClr" runat="server" NavigateUrl="javascript:void(0);">Clear</asp:HyperLink></td>
                    <td>&nbsp;&nbsp;&nbsp;</td>
                    <td><asp:HyperLink ID="lnkReligAll" runat="server" NavigateUrl="javascript:void(0);">Select All</asp:HyperLink></td>
                </tr>
            </table>
            <script type="text/javascript">
                $("#<%= lnkReligClr.ClientID%>").click(function () { ClearCheckBoxes("#<%= cbReligion.ClientID%>") });
                $("#<%= lnkReligAll.ClientID%>").click(function () { SelectCheckBoxes("#<%= cbReligion.ClientID%>") });
            </script>
        </div>

        <div class="clear"></div>
    </div>


    <div class="crt-lists">

        <div class="crt-list2 lfloat">
	        <h6><asp:Label ID="msg_PersonalSmokingHabit" runat="server" Text="" CssClass="psLabel1" /></h6>
	        <div class="crt-list-content">
                <asp:CheckBoxList  ID="cbSmoking" runat="server"></asp:CheckBoxList>
	        </div>
            <table class="left-table">
                <tr>
                    <td><asp:HyperLink ID="lnkSmokeClr" runat="server" NavigateUrl="javascript:void(0);">Clear</asp:HyperLink></td>
                    <td>&nbsp;&nbsp;&nbsp;</td>
                    <td><asp:HyperLink ID="lnkSmokeAll" runat="server" NavigateUrl="javascript:void(0);">Select All</asp:HyperLink></td>
                </tr>
            </table>
            <script type="text/javascript">
                $("#<%= lnkSmokeClr.ClientID%>").click(function () { ClearCheckBoxes("#<%= cbSmoking.ClientID%>") });
                $("#<%= lnkSmokeAll.ClientID%>").click(function () { SelectCheckBoxes("#<%= cbSmoking.ClientID%>") });
            </script>
        </div>

        <div class="crt-list2 lfloat">
	        <h6><asp:Label ID="msg_PersonalDrinkingHabit" runat="server" Text="" CssClass="psLabel1" /></h6>
	        <div class="crt-list-content">
                <asp:CheckBoxList  ID="cbDrinking" runat="server"></asp:CheckBoxList>
	        </div>
            <table class="left-table">
                <tr>
                    <td><asp:HyperLink ID="lnkDrinkClr" runat="server" NavigateUrl="javascript:void(0);">Clear</asp:HyperLink></td>
                    <td>&nbsp;&nbsp;&nbsp;</td>
                    <td><asp:HyperLink ID="lnkDrinkAll" runat="server" NavigateUrl="javascript:void(0);">Select All</asp:HyperLink></td>
                </tr>
            </table>
            <script type="text/javascript">
                $("#<%= lnkDrinkClr.ClientID%>").click(function () { ClearCheckBoxes("#<%= cbDrinking.ClientID%>") });
                $("#<%= lnkDrinkAll.ClientID%>").click(function () { SelectCheckBoxes("#<%= cbDrinking.ClientID%>") });
            </script>
        </div>

        <div class="clear"></div>
    </div>

    <div class="crt-lists">
        <div class="crt-list2 lfloat" id="divIncome" runat="server">
	        <h6><asp:Label ID="msg_OtherAnnual" runat="server" Text="" CssClass="psLabel1" /></h6>
	        <div class="controls">
                <asp:CheckBoxList  ID="cbIncome" runat="server"></asp:CheckBoxList>
            </div>
            <table class="left-table">
                <tr>
                    <td><asp:HyperLink ID="lnkIncomeClr" runat="server" NavigateUrl="javascript:void(0);">Clear</asp:HyperLink></td>
                    <td>&nbsp;&nbsp;&nbsp;</td>
                    <td><asp:HyperLink ID="lnkIncomeAll" runat="server" NavigateUrl="javascript:void(0);">Select All</asp:HyperLink></td>
                </tr>
            </table>
            <script type="text/javascript">
                $("#<%= lnkIncomeClr.ClientID%>").click(function () { ClearCheckBoxes("#<%= cbIncome.ClientID%>") });
                $("#<%= lnkIncomeAll.ClientID%>").click(function () { SelectCheckBoxes("#<%= cbIncome.ClientID%>") });
            </script>
        </div>
        <div class="clear"></div>
    </div>


	<div class="form-actions">
        <dx:ASPxButton ID="btnSaveAndFind" runat="server" ClientInstanceName="btnSaveAndFind" 
            CssClass="btn btn-large btn-primary" Text="Save Search and Show Results" 
            Native="True" EncodeHtml="False" 
            UseSubmitBehavior="False">
            <ClientSideEvents Click="btnSaveAndFind_Clicked" />
        </dx:ASPxButton>
	</div>
    <asp:HiddenField ID="hdfNewName" runat="server" />
    <asp:HiddenField ID="hdfSelectedName" runat="server" />
</div>

	            </asp:View>

                <asp:View ID="vwSearchBrowse" runat="server">

<uc2:QuickSearchCriteriasMan2 ID="ctlQSCrM" runat="server" 
    OnAdvancedClicked="hlAdvanced_Click" 
    OnSearchClicked="btnSearch_Click" 
    OnSearchByUserNameClicked="imgSearchByUserName_Click"
    SearchOnClientClicked="RefreshResults();setTimeout(function() {toggleQuickOptions();},600);return false;"
    SearchByUserNameOnClientClicked="RefreshResultsByUserName();return false;"   />

<uc2:QuickSearchCriteriasWoman ID="ctlQSCrW" runat="server" 
    OnAdvancedClicked="hlAdvanced_Click" 
    OnSearchClicked="btnSearch_Click" 
    OnSearchByUserNameClicked="imgSearchByUserName_Click"
    SearchOnClientClicked="RefreshResults();setTimeout(function() {toggleQuickOptions();},600);return false;"
    SearchByUserNameOnClientClicked="RefreshResultsByUserName();return false;" />

<asp:UpdatePanel ID="updOffers" runat="server">
    <ContentTemplate>
<asp:Button ID="btnSearch_ND" runat="server" Text="Update Search" CssClass="hidden" OnClicked="btnSearch_Click" />
<asp:ImageButton ID="imgSearchByUserName" runat="server" ImageUrl="//cdn.goomena.com/Images/spacer10.png" CssClass="hidden" />

<%--<input id="updOffers_refresh" type="submit" value="submit" class="hidden" />--%>
<div class="ss-filters">
    <div id="divFilter" runat="server" clientidmode="Static">
        <div class="lfloat descr-wrap" >
            <asp:Label ID="msg_SortBy" runat="server" Text="Sort By" AssociatedControlID="cbSort" CssClass="description"></asp:Label></div>
        <div class="lfloat">
            <dx:ASPxComboBox ID="cbSort" runat="server" class="update_uri" 
                AutoPostBack="True" EncodeHtml="False">
                <ClientSideEvents SelectedIndexChanged="ShowLoadingOnMenu" />
<ClientSideEvents BeginCallback="function(s, e) {
ShowLoading();
}" CallbackError="function(s, e) {
HideLoading();
}" EndCallback="function(s, e) {
HideLoading();
}" />
            </dx:ASPxComboBox>
        </div>
        <div class="rfloat">
            <dx:ASPxComboBox ID="cbPerPage" runat="server" class="update_uri input-small" style="width: 60px;" 
                AutoPostBack="True">
                <ClientSideEvents SelectedIndexChanged="ShowLoadingOnMenu" />
                <Items>
                    <dx:ListEditItem Text="10" Value="10" Selected="true"  />
                    <dx:ListEditItem Text="26" Value="26" />
                    <dx:ListEditItem Text="50" Value="50" />
                </Items>
<ClientSideEvents BeginCallback="function(s, e) {
ShowLoading();
}" CallbackError="function(s, e) {
LoadingPanel.Hide();
}" EndCallback="function(s, e) {
LoadingPanel.Hide();
}" />
            </dx:ASPxComboBox>
        </div>
        <div class="rfloat descr-wrap">
            <asp:Label ID="msg_ResultsPerPage" runat="server" Text="Results Per Page" AssociatedControlID="cbPerPage" CssClass="description"></asp:Label></div><div class="clear"></div>
    </div>
    <div class="clear"></div>
</div>

<div class="s_wrap">
    <div class="items_none" id="divNoresultsToManyParams" runat="server">
        <asp:Literal ID="msg_divNoresultsToManyParamsSpecified" runat="server"/>
	    <div class="clear"></div>
    </div>
    <div class="items_none no-results" id="divNoResultsChangeCriteria" runat="server">
        <asp:Literal ID="msg_divNoResultsTryChangeCriteria_ND" runat="server"/>
	    <div class="clear"></div>
    </div>
    <div class="items_none" id="divUserNameAndFiltersDisabled" runat="server" visible="False" enableviewstate="False">
        <asp:Literal ID="msg_divUserNameAndFiltersDisabled" runat="server"/>
	    <div class="clear"></div>
    </div>
    
            <uc2:SearchControl ID="searchL" runat="server" UsersListView="SearchingOffers" />
</div>
<div class="pagination">
<table align="center">
    <tr>
        <td><dx:ASPxPager ID="Pager" runat="server" ItemCount="3" 
                ItemsPerPage="1" 
                EncodeHtml="false"
                RenderMode="Lightweight" CssFilePath="~/App_Themes/Aqua/{0}/styles.css" 
                CssPostfix="Aqua" 
                CurrentPageNumberFormat="{0}" 
                PageNumberFormat="<span onclick='ShowLoading();'>{0}</span>"
                SpriteCssFilePath="~/App_Themes/Aqua/{0}/sprite.css">
                <lastpagebutton visible="True">
                </lastpagebutton>
                <firstpagebutton visible="True">
                </firstpagebutton>
                <Summary Position="Left" Text="Page {0} of {1} ({2} members)" Visible="false" />
        </dx:ASPxPager></td>
    </tr>
</table>
</div>
    </ContentTemplate>
</asp:UpdatePanel>

                </asp:View>



            </asp:MultiView>

        <%--
        <div class="clear"></div>
        <div class="container_12" id="tabs-outter">
            <div class="grid_12 top_tabs">
                <div runat="server" id="divSearchTabs">
                    <ul style="display:block">
                        <li runat="server" id="liBrowse">
                            <asp:LinkButton ID="lnkBrowse" runat="server"><span></span>Browse</asp:LinkButton></li><li runat="server" id="liAdvanced">
                            </li></ul></div></div></div>
        <div class="clear"></div>
        <div class="container_12" id="content_top">
        </div>
        <div class="container_12" id="content">
            <div class="grid_3" id="sidebar-outter" style="position:relative;top:-11px;">
                <div class="left_tab" ID="left_tab" runat="server">
                    <!-- search tab -->
                    <div class="top_info">
                    </div>
                    <div class="middle" id="search">
                    </div>
                    <div class="bottom">&nbsp;</div>
               </div>
            </div>
            <div class="grid_9 body">
                <div class="middle" id="content-outter">
                    <div style="height:10px;">&nbsp;</div>
                </div>
                <div class="bottom"></div>
            </div>
            <div class="clear"></div>
        </div>
        --%>
</div>
     <script type="text/javascript">
         // <![CDATA[
         function loadFancybox() {
             $("a.fancybox").fancybox({
                 'transitionIn': 'elastic',
                 'transitionOut': 'elastic',
                 'titlePosition': 'over',
                 'overlayColor': '#000'
             });
         }

         function pageLoad(sender, args) {
             loadFancybox();
             if (typeof setPopupWindowUI !== 'undefined') setPopupWindowUI();
         }

         function closePopup(popupName) {
             try {
                 top.window[popupName].Hide();
                 top.window[popupName].DoHideWindow(0);
             }
             catch (e) { }
         }


         // ]]> 
    </script>
	<uc2:whatisit ID="WhatIsIt1" runat="server" />


    <dx:ASPxPopupControl runat="server"
        Height="340px" Width="500px"
        ID="popupSearchName"
        ClientInstanceName="popupSearchName" 
        ClientIDMode="AutoID" 
        AllowDragging="True" 
        PopupVerticalAlign="WindowCenter" 
        PopupHorizontalAlign="WindowCenter" 
        Modal="True" 
        CloseAction="CloseButton"
        AllowResize="True" 
        AutoUpdatePosition="True"
        ShowShadow="False">
    <ContentStyle HorizontalAlign="Left" VerticalAlign="Middle">
        <Paddings Padding="0px" PaddingTop="25px" />
        <Paddings Padding="0px" PaddingTop="25px" ></Paddings>
    </ContentStyle>
    <CloseButtonStyle BackColor="White">
        <HoverStyle>
            <BackgroundImage ImageUrl="//cdn.goomena.com/Images2/popup-wrn/close.png" Repeat="NoRepeat" HorizontalPosition="center"
                VerticalPosition="center"></BackgroundImage>
        </HoverStyle>
        <BackgroundImage ImageUrl="//cdn.goomena.com/Images2/popup-wrn/close.png" Repeat="NoRepeat" HorizontalPosition="center"
            VerticalPosition="center"></BackgroundImage>
        <BorderLeft BorderColor="White" BorderStyle="Solid" BorderWidth="10px" />
    </CloseButtonStyle>
    <CloseButtonImage Url="//cdn.goomena.com/Images/spacer10.png" Height="43px" Width="43px">
    </CloseButtonImage>
    <SizeGripImage Height="40px" Url="//cdn.goomena.com/Images/resize.png" Width="40px">
    </SizeGripImage>
    <HeaderStyle>
        <Paddings Padding="0px" PaddingLeft="0px" />
        <Paddings PaddingTop="0px" PaddingBottom="0px"></Paddings>
        <BorderTop BorderColor="White" BorderStyle="Solid" BorderWidth="5px" />
        <BorderRight BorderColor="White" BorderStyle="Solid" BorderWidth="5px" />
        <BorderBottom BorderColor="White" BorderStyle="Solid" BorderWidth="5px" />
    </HeaderStyle>
    <ModalBackgroundStyle BackColor="Black" Opacity="70" CssClass="WhatIs-ModalBG"></ModalBackgroundStyle>
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server" SupportsDisabledAttribute="True">
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>


</asp:Content>


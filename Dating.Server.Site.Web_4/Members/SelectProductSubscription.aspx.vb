﻿'Imports Sites.SharedSiteCode
Imports Library.Public
Imports Dating.Server.Core.DLL

Public Class SelectProductSubscription
    Inherits BasePage

    Dim isSubscriptionUser As Boolean


#Region "Props"

    'Dim _pageData As clsPageData
    Protected Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData("~/Members/SelectProduct.aspx", Context, coe)
            End If
            Return _pageData
        End Get
    End Property

    'Private __Subject As String
    'Private __Content As String

#End Region


    Protected Overrides Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        SetMasterPage(CurrentPageData)

        Try
            If clsCurrentContext.VerifyLogin() = True Then
                isSubscriptionUser = (Me.MasterProfileId.ToString() = ConfigurationManager.AppSettings("TEST_PROFILE_ID"))

                Me.MasterPageFile = "~/Members/Members2.Master"
                'If (Request("master") = "inner") Then
                '    Me.MasterPageFile = "~/Members/Members2Inner.Master"
                'Else
                'End If
                If Me.IsFemale = True Then
                    Response.Redirect(ResolveUrl("~/Members/PaymentWoman.aspx"), True)
                End If

                Try
                    If (ConfigurationManager.AppSettings("AllowSubscription") = "1" OrElse isSubscriptionUser) Then
                        If (Not ProfileCountry.MonthlySubscriptionEnabled AndAlso Not isSubscriptionUser) Then
                            Dim url As String = ResolveUrl("~/Members/SelectProduct.aspx")
                            Response.Redirect(url, True)
                        End If
                    End If
                Catch ex As Exception
                    WebErrorSendEmail(ex, "")
                End Try
            Else
                clsCurrentContext.ClearSession(Session)
                If (Not Me.IsCallback) Then
                    Response.Redirect(ResolveUrl("~/Login.aspx"), False)
                End If
            End If
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Dim a As Integer = 0
        'Dim b As Integer = 1111 / a
        Try
            If (Not Page.IsPostBack) Then
                LoadLAG()
            End If

            If Len(Request.QueryString("choice")) Then
                Response.Redirect("selectPayment2.aspx?choice=" & Request.QueryString("choice"))
            Else
                LoadLAG()
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try
    End Sub

    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try

            Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try
    End Sub



    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
    End Sub



    Protected Sub LoadLAG()
        Try

            lblItemsName.Text = Me.CurrentPageData.GetCustomString("CartSelectProductView")
            lnkViewDescription.Text = Me.CurrentPageData.GetCustomString("CartSelectProductWhatIsIt")
            lnkViewDescription.OnClientClick = OnMoreInfoClickFunc(
                lnkViewDescription.OnClientClick,
                ResolveUrl("~/Members/InfoWin.aspx?info=selectproduct"),
                Me.CurrentPageData.GetCustomString("CartSelectProductView"))
            WhatIsIt1.PopupControl.HeaderText = Me.CurrentPageData.GetCustomString("CartSelectProductView")

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    'Private Sub Page_Unload1(sender As Object, e As System.EventArgs) Handles Me.Unload
    '    Try
    '        If (Not String.IsNullOrEmpty(__Content)) Then
    '            Dating.Server.Core.DLL.clsMyMail.SendMail(ConfigurationManager.AppSettings("PaymentLogsEmail"), __Subject, __Content, False)
    '        End If
    '    Catch
    '    End Try
    'End Sub

End Class
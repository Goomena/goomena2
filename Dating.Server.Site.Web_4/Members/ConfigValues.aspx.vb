﻿Public Class WebForm1
    Inherits BasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Me.MasterProfileId.ToString() = ConfigurationManager.AppSettings("TEST_PROFILE_ID")) Then
            Label1.Text =
                "AllowSubscription" & " --- " & "<br>" & ConfigurationManager.AppSettings("AllowSubscription") & "<br><br>" & _
                "PaymentSendPage" & " --- " & "<br>" & ConfigurationManager.AppSettings("PaymentSendPage") & "<br><br>" & _
                "TEST_PROFILE_ID" & " --- " & "<br>" & ConfigurationManager.AppSettings("TEST_PROFILE_ID")

        Else
            Response.Redirect(ResolveUrl("~/Members/Default.aspx"))
        End If
    End Sub

End Class
﻿Imports System.Data.SqlClient
Imports Library.Public


Public Class ClsCombos

    Shared Sub FillCombo(ByVal ConnectionStringNET As String, ByVal TableName As String, _
ByVal TextFieldName As String, _
ByVal ValueIDFieldName As String, _
ByRef aSPxRadioButtonList As DevExpress.Web.ASPxEditors.ASPxRadioButtonList, _
ByVal ClearItems As Boolean,
Optional ByVal showAny As Boolean = False)

        Try
            Dim anySQL As String = ""
            If showAny Then
                ' Add The < any > option
                anySQL = "SELECT -1 AS " & ValueIDFieldName & ", '< any >' " & TextFieldName & " From " & TableName & " UNION "
            End If
            ' The table SQL
            Dim tableSQL As String = "Select " & ValueIDFieldName & "," & TextFieldName & " From " & TableName
            'Combined SQL 
            If ClearItems Then aSPxRadioButtonList.Items.Clear()

            Dim cmdSQL = anySQL + tableSQL
            Dim i As Integer = 0
            Using con As New SqlConnection(ConnectionStringNET)


                Using cmd = New SqlCommand(cmdSQL, con)


                    Dim rsRead As SqlDataReader
                    cmd.Connection.Open()
                    rsRead = cmd.ExecuteReader()
                    While rsRead.Read()
                        i = i + 1
                        If Len(Common.CheckNULL(rsRead(TextFieldName), "")) Then
                            Dim ic As New DevExpress.Web.ASPxEditors.ListEditItem
                            ic.Text = Common.CheckNULL(rsRead(TextFieldName), "")
                            ic.Value = Common.CheckNULL(rsRead(ValueIDFieldName), 0)
                            aSPxRadioButtonList.Items.Add(ic)
                        End If
                    End While
                    rsRead.Close()
                    '    cmd.Connection.Close()
                End Using
            End Using
        Catch ex As Exception
            gER.ErrorMsgBox(ex, "FillComboNET Table:" & TableName & "," & TextFieldName & "," & ValueIDFieldName)
        End Try

    End Sub


    Public Shared Sub FillCombo(ByVal ConnectionStringNET As String, ByVal TableName As String, _
ByVal TextFieldName As String, _
ByVal ValueIDFieldName As String, _
ByRef cbCompo As DevExpress.Web.ASPxEditors.ASPxComboBox, _
ByVal ClearItems As Boolean, Optional ByVal showAny As Boolean = False, _
Optional ByVal OrderByFieldName As String = Nothing, _
Optional ByVal OrderByASC_DESC As String = Nothing)

        Try
            Dim anySQL As String = ""
            If showAny Then
                ' Add The < any > option
                anySQL = "SELECT -1 AS " & ValueIDFieldName & ", '< any >' " & TextFieldName & " From " & TableName & " UNION "
            End If
            ' The table SQL
            Dim tableSQL As String = "Select " & ValueIDFieldName & "," & TextFieldName & " From " & TableName
            'Combined SQL 

            If (Not String.IsNullOrEmpty(OrderByFieldName)) Then
                tableSQL = tableSQL & vbCrLf & "order by " & OrderByFieldName

                If (Not String.IsNullOrEmpty(OrderByASC_DESC)) Then
                    tableSQL = tableSQL & " " & OrderByASC_DESC
                End If
            End If

            If ClearItems Then cbCompo.Items.Clear()

            Dim cmdSQL = anySQL + tableSQL
            Dim i As Integer = 0
            Using con As New SqlConnection(ConnectionStringNET)
                Using cmd = New SqlCommand(cmdSQL, con)

                    cmd.Connection.Open()
                    Using rsRead As SqlDataReader = cmd.ExecuteReader()


                        While rsRead.Read()
                            i = i + 1
                            If Len(Common.CheckNULL(rsRead(TextFieldName), "")) Then
                                Dim ic As New DevExpress.Web.ASPxEditors.ListEditItem
                                ic.Text = Common.CheckNULL(rsRead(TextFieldName), "")
                                ic.Value = Common.CheckNULL(rsRead(ValueIDFieldName), 0)
                                cbCompo.Items.Add(ic)
                            End If
                        End While
                    End Using
                  
                End Using
            End Using
        Catch ex As Exception
            gER.ErrorMsgBox(ex, "FillComboNET Table:" & TableName & "," & TextFieldName & "," & ValueIDFieldName)
        End Try

    End Sub



    Public Shared Sub FillComboUsingDatatable(ByRef dt As DataTable, _
ByVal TitleFieldName As String, _
ByVal ValueIDFieldName As String, _
ByRef lbListBox As DevExpress.Web.ASPxEditors.ASPxListBox, _
ByVal ClearItems As Boolean,
Optional ByVal showAny As Boolean = False, _
Optional ByVal DefaultTitleFieldName As String = "")


        'On Error GoTo er
        Dim i As Integer = 0
        If ClearItems Then lbListBox.Items.Clear()

        While i < dt.Rows.Count
            If Len(Common.CheckNULL(dt.Rows(i)(TitleFieldName), "")) Then
                'Public Sub Add(item As DevExpress.Web.ASPxEditors.ListEditItem)

                Dim ic As New DevExpress.Web.ASPxEditors.ListEditItem
                ic.Text = dt.Rows(i)(TitleFieldName)
                ic.Value = dt.Rows(i)(ValueIDFieldName)
                lbListBox.Items.Add(ic)

            ElseIf Not String.IsNullOrEmpty(DefaultTitleFieldName) AndAlso dt.Rows(i)(DefaultTitleFieldName).ToString().Length > 0 Then

                Dim ic As New DevExpress.Web.ASPxEditors.ListEditItem
                ic.Text = dt.Rows(i)(DefaultTitleFieldName)
                ic.Value = dt.Rows(i)(ValueIDFieldName)
                lbListBox.Items.Add(ic)

            End If
            i = i + 1
        End While
        Exit Sub
        'er:         gER.ErrorMsgBox("FillComboUsingDatatable Table:" & dt.TableName & "," & TitleFieldName & "," & ValueIDFieldName)
    End Sub



    Public Shared Sub FillComboUsingDatatable(ByRef dt As DataTable, _
ByVal TitleFieldName As String, _
ByVal ValueIDFieldName As String, _
ByRef lbListBox As DevExpress.Web.ASPxEditors.ASPxCheckBoxList, _
ByVal ClearItems As Boolean,
Optional ByVal showAny As Boolean = False, _
Optional ByVal DefaultTitleFieldName As String = "")


        'On Error GoTo er
        Dim i As Integer = 0
        If ClearItems Then lbListBox.Items.Clear()

        While i < dt.Rows.Count
            If Len(Common.CheckNULL(dt.Rows(i)(TitleFieldName), "")) Then
                'Public Sub Add(item As DevExpress.Web.ASPxEditors.ListEditItem)

                Dim ic As New DevExpress.Web.ASPxEditors.ListEditItem
                ic.Text = dt.Rows(i)(TitleFieldName)
                ic.Value = dt.Rows(i)(ValueIDFieldName)
                lbListBox.Items.Add(ic)
            ElseIf Not String.IsNullOrEmpty(DefaultTitleFieldName) AndAlso dt.Rows(i)(DefaultTitleFieldName).ToString().Length > 0 Then

                Dim ic As New DevExpress.Web.ASPxEditors.ListEditItem
                ic.Text = dt.Rows(i)(DefaultTitleFieldName)
                ic.Value = dt.Rows(i)(ValueIDFieldName)
                lbListBox.Items.Add(ic)

            End If
            i = i + 1
        End While
        Exit Sub
        'er:         gER.ErrorMsgBox("FillComboUsingDatatable Table:" & dt.TableName & "," & TitleFieldName & "," & ValueIDFieldName)
    End Sub



    Public Shared Sub FillComboUsingDatatable(ByRef dt As DataTable, _
ByVal TitleFieldName As String, _
ByVal ValueIDFieldName As String, _
ByRef RadioButtonList As DevExpress.Web.ASPxEditors.ASPxRadioButtonList, _
ByVal ClearItems As Boolean,
Optional ByVal showAny As Boolean = False, _
Optional ByVal DefaultTitleFieldName As String = "")


        On Error GoTo er
        Dim i As Integer = 0
        If ClearItems Then RadioButtonList.Items.Clear()

        While i < dt.Rows.Count
            'If Len(Common.CheckNULL(dt.Rows(i)(TitleFieldName), "")) Then
            '    Dim ic As New DevExpress.Web.ASPxEditors.ListEditItem
            '    ic.Text = dt.Rows(i)(TitleFieldName)
            '    ic.Value = dt.Rows(i)(ValueIDFieldName)
            '    RadioButtonList.Items.Add(ic)
            'End If
            If Len(Common.CheckNULL(dt.Rows(i)(TitleFieldName), "")) Then

                Dim ic As New DevExpress.Web.ASPxEditors.ListEditItem
                ic.Text = dt.Rows(i)(TitleFieldName)
                ic.Value = dt.Rows(i)(ValueIDFieldName)
                RadioButtonList.Items.Add(ic)
            ElseIf Not String.IsNullOrEmpty(DefaultTitleFieldName) AndAlso Len(Common.CheckNULL(dt.Rows(i)(DefaultTitleFieldName), "")) Then

                Dim ic As New DevExpress.Web.ASPxEditors.ListEditItem
                ic.Text = dt.Rows(i)(DefaultTitleFieldName)
                ic.Value = dt.Rows(i)(ValueIDFieldName)
                RadioButtonList.Items.Add(ic)

            End If

            i = i + 1
        End While
        Exit Sub

er:     gER.ErrorMsgBox("FillComboUsingDatatable Table:" & dt.TableName & "," & TitleFieldName & "," & ValueIDFieldName)
    End Sub



    Public Shared Sub FillComboUsingDatatable(ByRef dt As DataTable, _
ByVal TitleFieldName As String, _
ByVal ValueIDFieldName As String, _
ByRef CheckBoxList As System.Web.UI.WebControls.CheckBoxList, _
ByVal ClearItems As Boolean,
Optional ByVal showAny As Boolean = False, _
Optional ByVal DefaultTitleFieldName As String = "US")


        On Error GoTo er
        Dim i As Integer = 0
        If ClearItems Then CheckBoxList.Items.Clear()

        While i < dt.Rows.Count
            'If Len(Common.CheckNULL(dt.Rows(i)(TitleFieldName), "")) Then
            '    'Public Sub Add(item As DevExpress.Web.ASPxEditors.ListEditItem)

            '    Dim ic As New System.Web.UI.WebControls.ListItem
            '    ic.Text = dt.Rows(i)(TitleFieldName)
            '    ic.Value = dt.Rows(i)(ValueIDFieldName)
            '    CheckBoxList.Items.Add(ic)
            'End If
            If Len(Common.CheckNULL(dt.Rows(i)(TitleFieldName), "")) Then

                Dim ic As New System.Web.UI.WebControls.ListItem
                ic.Text = dt.Rows(i)(TitleFieldName)
                ic.Value = dt.Rows(i)(ValueIDFieldName)
                CheckBoxList.Items.Add(ic)
            ElseIf Not String.IsNullOrEmpty(DefaultTitleFieldName) AndAlso Len(Common.CheckNULL(dt.Rows(i)(DefaultTitleFieldName), "")) Then

                Dim ic As New System.Web.UI.WebControls.ListItem
                ic.Text = dt.Rows(i)(DefaultTitleFieldName)
                ic.Value = dt.Rows(i)(ValueIDFieldName)
                CheckBoxList.Items.Add(ic)

            End If

            i = i + 1
        End While
        Exit Sub
er:     gER.ErrorMsgBox("FillComboUsingDatatable Table:" & dt.TableName & "," & TitleFieldName & "," & ValueIDFieldName)
    End Sub




    Public Shared Sub FillComboUsingDatatable(ByRef dt As DataTable, _
ByVal TitleFieldName As String, _
ByVal ValueIDFieldName As String, _
ByRef cbCompo As DevExpress.Web.ASPxEditors.ASPxComboBox, _
ByVal ClearItems As Boolean,
Optional ByVal DefaultTitleFieldName As String = "", _
Optional ByVal showAny As Boolean = False)


        On Error GoTo er
        Dim i As Integer = 0
        If ClearItems Then cbCompo.Items.Clear()

        While i < dt.Rows.Count
            If Len(Common.CheckNULL(dt.Rows(i)(TitleFieldName), "")) Then

                Dim ic As New DevExpress.Web.ASPxEditors.ListEditItem
                ic.Text = dt.Rows(i)(TitleFieldName)
                ic.Value = dt.Rows(i)(ValueIDFieldName)
                cbCompo.Items.Add(ic)
            ElseIf Not String.IsNullOrEmpty(DefaultTitleFieldName) AndAlso Len(Common.CheckNULL(dt.Rows(i)(DefaultTitleFieldName), "")) Then

                Dim ic As New DevExpress.Web.ASPxEditors.ListEditItem
                ic.Text = dt.Rows(i)(DefaultTitleFieldName)
                ic.Value = dt.Rows(i)(ValueIDFieldName)
                cbCompo.Items.Add(ic)

            End If
            i = i + 1
        End While
        Exit Sub
er:     gER.ErrorMsgBox("FillComboUsingDatatable Table:" & dt.TableName & "," & TitleFieldName & "," & ValueIDFieldName)
    End Sub





    Public Shared Function SelectComboItem(ByRef cb As DevExpress.Web.ASPxEditors.ASPxComboBox, value As Object) As Boolean
        Dim isSelected = False

        If (value IsNot Nothing) Then
            Dim ic As DevExpress.Web.ASPxEditors.ListEditItem = cb.Items.FindByValue(value.ToString())
            If (ic IsNot Nothing) Then
                cb.SelectedItem = ic
                isSelected = True
            End If
        End If

        Return isSelected
    End Function



    Public Shared Function SelectComboItem(ByRef rbl As DevExpress.Web.ASPxEditors.ASPxRadioButtonList, value As Object) As Boolean
        Dim isSelected = False

        If (value IsNot Nothing) Then
            Dim ic As DevExpress.Web.ASPxEditors.ListEditItem = rbl.Items.FindByValue(value.ToString())
            If (ic IsNot Nothing) Then
                rbl.SelectedItem = ic
                isSelected = True
            End If
        End If

        Return isSelected
    End Function


    Public Shared Function SelectComboItem(ByRef cb As ListControl, value As Object) As Boolean
        Dim isSelected = False

        If (value IsNot Nothing) Then
            Dim ic As ListItem = cb.Items.FindByValue(value.ToString())
            If (ic IsNot Nothing) Then
                ic.Selected = True
                isSelected = True
            End If
        End If

        Return isSelected
    End Function



End Class



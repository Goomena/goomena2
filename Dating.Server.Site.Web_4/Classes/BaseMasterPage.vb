﻿Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Core.DLL

Public Class BaseMasterPage
    Inherits System.Web.UI.MasterPage

    Protected _pageData As clsPageData
    Protected ReadOnly Property CurrentPageData As clsPageData
        Get
            'If (_pageData Is Nothing) Then _pageData = New clsPageData("master", Context)
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData("master", Context, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If

            Return _pageData
        End Get
    End Property

    Private Sub Page_Disposed1(sender As Object, e As EventArgs) Handles Me.Disposed
        Try
            If Me._pageData IsNot Nothing Then
                RemoveHandler Me._pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
        Catch ex As Exception
        End Try
        Try
            MyBase.Dispose()
        Catch ex As Exception
        End Try
    End Sub


    Private _globalStrings As clsPageData
    Public ReadOnly Property globalStrings As clsPageData
        Get
            If (_globalStrings Is Nothing) Then
                _globalStrings = New clsPageData("GlobalStrings", HttpContext.Current)
            End If
            Return _globalStrings
        End Get
    End Property

    Dim _sessVars As clsSessionVariables
    Protected ReadOnly Property SessionVariables As clsSessionVariables
        Get
            If (_sessVars Is Nothing) Then _sessVars = clsSessionVariables.GetCurrent()
            Return _sessVars
        End Get
    End Property


    Public ReadOnly Property IsHTTPS As Boolean
        Get
            Return (Me.Request.Url.Scheme = "https")
        End Get
    End Property



    Private _IsMale As Boolean?
    Public ReadOnly Property IsMale As Boolean
        Get
            If (Not _IsMale.HasValue) Then
                '_IsMale = (Me.GetCurrentProfile().GenderId = ProfileHelper.gMaleGender.GenderId)
                _IsMale = (Me.SessionVariables.MemberData.GenderId = ProfileHelper.gMaleGender.GenderId)
            End If
            Return _IsMale
        End Get
    End Property


    Private _IsFemale As Boolean?
    Public ReadOnly Property IsFemale As Boolean
        Get
            If (Not _IsFemale.HasValue) Then
                '_IsFemale = (Me.GetCurrentProfile().GenderId = ProfileHelper.gFemaleGender.GenderId)
                _IsFemale = (Me.SessionVariables.MemberData.GenderId = ProfileHelper.gFemaleGender.GenderId)
            End If
            Return _IsFemale
        End Get
    End Property


    Protected ReadOnly Property MasterProfileId As Integer
        Get
            Return Me.Session("ProfileID")
        End Get
    End Property


    Protected ReadOnly Property MirrorProfileId As Integer
        Get
            Return Me.Session("MirrorProfileID")
        End Get
    End Property


    Private __Language As clsLanguageHelper
    Protected ReadOnly Property LanguageHelper As clsLanguageHelper
        Get
            If (__Language Is Nothing) Then __Language = New clsLanguageHelper(Context)
            Return __Language
        End Get
    End Property


    Protected ReadOnly Property ProfileLoginName As String
        Get
            Return Me.SessionVariables.MemberData.LoginName
        End Get
    End Property

    Public ReadOnly Property GetCMSDBDataContext As CMSDBDataContext
        Get
            Return New CMSDBDataContext(ModGlobals.ConnectionString)
        End Get
    End Property
    'Dim _CMSDBDataContext As CMSDBDataContext
    'Protected ReadOnly Property CMSDBDataContext As CMSDBDataContext
    '    Get
    '        Try
    '            If _CMSDBDataContext IsNot Nothing Then
    '                ' exception thrown when disposed
    '                Dim a As System.Data.Common.DbConnection = _CMSDBDataContext.Connection


    '            End If
    '        Catch ex As Exception
    '            _CMSDBDataContext = Nothing
    '        End Try

    '        If (_CMSDBDataContext Is Nothing) Then 'OrElse _DataContext.Connection.State = ConnectionState.Broken OrElse _DataContext.Connection.State = ConnectionState.Closed) Then
    '            _CMSDBDataContext = New CMSDBDataContext(ModGlobals.ConnectionString)
    '        End If
    '        Return _CMSDBDataContext
    '    End Get
    'End Property


    Public Function GetLag() As String
        Dim str As String = Nothing
        Try
            If (clsLanguageHelper.IsSupportedLAG(HttpContext.Current.Session("LAGID"), Session("GEO_COUNTRY_CODE"))) Then
                str = HttpContext.Current.Session("LAGID")
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        If (str Is Nothing) Then
            If Request.Url.Host.EndsWith(".gr", StringComparison.OrdinalIgnoreCase) Then
                'goomena.gr
                str = gDomainGR_DefaultLAG
            Else

                If Request.Url.Host.EndsWith(".com", StringComparison.OrdinalIgnoreCase) Then
                    'goomena.com
                    If (str Is Nothing) Then str = gDomainCOM_DefaultLAG
                End If

            End If
        End If

        If (str Is Nothing) Then str = gDomainCOM_DefaultLAG

        Return str
    End Function


    Public Shared Function GetGeoCountry() As String
        Return HttpContext.Current.Session("GEO_COUNTRY_CODE")
    End Function

  


    Private Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        Try
            If Not Me.IsPostBack Then
                Dim scope As PageScopeEnum = UrlUtils.GetPageScope(Request.Url)
                If (scope = PageScopeEnum.PublicPage) Then
                    LanguageHelper.CheckIfPublicPageShouldRedirect()

                    If (Me.IsHTTPS AndAlso
                        clsCurrentContext.ForceHTTPPublic()) Then

                        Dim newUrl As String = UrlUtils.GetHTTPUrl(Request.Url)
                        Response.Redirect(newUrl)

                    End If

                ElseIf (scope = PageScopeEnum.MembersPage) Then

                    LanguageHelper.CheckIfMemberPageShouldRedirectToMainURL()

                    ' redirect user to HTTPS url
                    If (Not Me.IsHTTPS AndAlso
                        Not clsCurrentContext.IsLocalhost() AndAlso
                        clsCurrentContext.UseHTTPSMembers() AndAlso
                        clsCurrentContext.IsHTTPSAvailable()) Then

                        Dim newUrl As String = UrlUtils.GetHTTPSUrl(Request.Url)
                        Response.Redirect(newUrl)

                    ElseIf (Me.IsHTTPS AndAlso
                            clsCurrentContext.ForceHTTPMembers()) Then

                        Dim redirToHttp As Boolean = True

                        Dim IsPayment As Boolean = False
                        Dim url1 As String = Request.Url.AbsolutePath.ToLower()
                        If (url1.IndexOf("/selectproduct.aspx") > -1 OrElse
                            url1.IndexOf("/selectpayment.aspx") > -1 OrElse
                            url1.IndexOf("/pwsetpayment.aspx") > -1 OrElse
                            url1.IndexOf("/payment.aspx") > -1 OrElse
                            url1.IndexOf("/paymentfail.aspx") > -1 OrElse
                            url1.IndexOf("/paymentok.aspx") > -1) Then

                            IsPayment = True

                        End If


                        If (IsPayment AndAlso
                            clsCurrentContext.UseHTTPSOnPayment()) Then
                            redirToHttp = False
                        End If

                        If (redirToHttp) Then
                            Dim newUrl As String = UrlUtils.GetHTTPUrl(Request.Url)
                            Response.Redirect(newUrl)
                        End If


                    End If
                End If

            End If
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub




    Public Sub Page_CustomStringRetrievalComplete(ByRef sender As clsPageData, ByRef Args As CustomStringRetrievalCompleteEventArgs)
        ' Dim __pageData As clsPageData = sender
        Dim result As String = Args.CustomString

        If (HttpContext.Current.Request.Url.Scheme.ToUpper() = "HTTPS") Then
            If (result.Contains("http://www.goomena.")) Then
                result = result.Replace("http://www.goomena.", "https://www.goomena.")
                Args.CustomString = result
            End If
            If (result.Contains("http://cdn.goomena.com")) Then
                result = result.Replace("http://cdn.goomena.com", "https://cdn.goomena.com")
                Args.CustomString = result
            End If
        End If

        If (Args.LagIDparam <> "US") Then
            Dim ancMatch As Match = clsWebFormHelper.AnchorRegex.Match(result, 0)
            If (ancMatch.Success) Then
                Args.CustomString = LanguageHelper.FixHTMLBodyAnchors(result, GetLag(), Page.RouteData)
            End If
        End If
    End Sub



    Protected Overrides Sub Finalize()
        '  If _CMSDBDataContext IsNot Nothing Then _CMSDBDataContext.Dispose()
        MyBase.Finalize()
    End Sub
End Class

Imports System.Data.SqlClient
Imports Library.Public

Public Class ClsCombos

    Public Shared Sub FillCombo(ByVal cMyConnectionString As ClsMyConnectionString, ByVal TableName As String, _
    ByVal TitleFieldName As String, _
    ByVal ValueIDFieldName As String, _
    ByVal cbCompo As DevExpress.XtraEditors.ImageComboBoxEdit, _
    ByVal ClearItems As Boolean, Optional ByVal showAny As Boolean = False)

        On Error GoTo er
        Dim anySQL As String = ""

        If showAny Then
            ' Add The < any > option
            anySQL = "SELECT -1 AS " & ValueIDFieldName & ", '< any >' " & TitleFieldName & " From " & TableName & " UNION "
        End If

        ' The table SQL
        Dim tableSQL As String = "Select " & ValueIDFieldName & "," & TitleFieldName & " From " & TableName

        Dim rs As New ClsMyADODB(cMyConnectionString)

        'Combined SQL 
        Dim cmdSQL = anySQL + tableSQL

        Dim i As Integer = 0
        If ClearItems Then cbCompo.Properties.Items.Clear()

        If rs.Open(cmdSQL, ClsMyADODB.OpenMode.ReadOnlyMode) Then
            While Not rs.EOF
                i = i + 1
                If Len(Common.CheckNULL(rs.GetFieldValue(TitleFieldName, ""), "")) Then
                    Dim ic As New DevExpress.XtraEditors.Controls.ImageComboBoxItem
                    ic.Description = rs.GetFieldValue(TitleFieldName, "")
                    ic.Value = rs.GetFieldValue(ValueIDFieldName, 0)
                    cbCompo.Properties.Items.Add(ic)
                End If
                rs.MoveNext()
            End While
        End If
        rs.Close()
        Exit Sub
er:     gER.ErrorMsgBox("FillCombo Table:" & TableName & "," & TitleFieldName & "," & ValueIDFieldName & "," & cmdSQL)
    End Sub

    Public Shared Sub FillComboWithParentID(ByVal cMyConnectionString As ClsMyConnectionString, ByVal TableName As String, _
    ByVal TitleFieldName As String, _
    ByVal ValueIDFieldName As String, _
    ByVal cbCompo As DevExpress.XtraEditors.ImageComboBoxEdit, _
    ByVal ParentFieldName As String, _
    ByVal ParentFieldValueID As Integer, ByVal ClearItems As Boolean, Optional ByVal showAny As Boolean = False)

        Dim cmdSQL As String
        Try

            Dim anySQL As String = ""
            If IsDBNull(ParentFieldValueID) Then Exit Sub
            If IsNothing(ParentFieldValueID) Then Exit Sub

            If showAny Then
                ' Add The < any > option
                anySQL = "SELECT -1 AS " & ValueIDFieldName & ", '< any >' " & TitleFieldName & " From " & TableName & " UNION "
            End If

            ' The table SQL
            Dim tableSQL As String = "Select " & ValueIDFieldName & "," & TitleFieldName & " From " & TableName & " WHERE " & ParentFieldName & "=" & ParentFieldValueID

            Dim rs As New ClsMyADODB(cMyConnectionString)

            'Combined SQL 
            cmdSQL = anySQL + tableSQL

            Dim i As Integer = 0
            If ClearItems Then cbCompo.Properties.Items.Clear()
            If rs.Open(cmdSQL, ClsMyADODB.OpenMode.ReadOnlyMode) Then
                While Not rs.EOF
                    i = i + 1
                    If Not Common.CheckNULL(rs.GetFieldValue(TitleFieldName, ""), "") Then
                        Dim ic As New DevExpress.XtraEditors.Controls.ImageComboBoxItem
                        ic.Description = rs.GetFieldValue(TitleFieldName, "")
                        ic.Value = rs.GetFieldValue(ValueIDFieldName, 0)
                        cbCompo.Properties.Items.Add(ic)
                    End If
                    rs.MoveNext()
                End While
            End If
            rs.Close()
            Exit Sub
        Catch ex As Exception
            gER.ErrorMsgBox(ex, "FillComboWithParentID Table:" & TableName & "," & TitleFieldName & "," & ValueIDFieldName & "," & cmdSQL)
        End Try
    End Sub




    'Repository
    Public Shared Sub FillCombo(ByVal cMyConnectionString As ClsMyConnectionString, ByVal TableName As String, _
    ByVal TitleFieldName As String, _
    ByVal ValueIDFieldName As String, _
    ByVal cbCompo As DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox, _
    ByVal ClearItems As Boolean, Optional ByVal showAny As Boolean = False)

        On Error GoTo er
        Dim anySQL As String = ""

        If showAny Then
            ' Add The < any > option
            anySQL = "SELECT -1 AS " & ValueIDFieldName & ", '< any >' " & TitleFieldName & " From " & TableName & " UNION "
        End If

        ' The table SQL
        Dim tableSQL As String = "Select " & ValueIDFieldName & "," & TitleFieldName & " From " & TableName

        Dim rs As New ClsMyADODB(cMyConnectionString)

        'Combined SQL 
        Dim cmdSQL = anySQL + tableSQL

        Dim i As Integer = 0
        If ClearItems Then cbCompo.Items.Clear()
        If rs.Open(cmdSQL, ClsMyADODB.OpenMode.ReadOnlyMode) Then
            While Not rs.EOF
                i = i + 1
                If Not Common.CheckNULL(rs.GetFieldValue(TitleFieldName, ""), "") Then
                    Dim ic As New DevExpress.XtraEditors.Controls.ImageComboBoxItem
                    ic.Description = rs.GetFieldValue(TitleFieldName, "")
                    ic.Value = rs.GetFieldValue(ValueIDFieldName, 0)
                    cbCompo.Items.Add(ic)
                End If
                rs.MoveNext()
            End While
        End If
        rs.Close()
        Exit Sub
er:     gER.ErrorMsgBox("FillCombo Table:" & TableName & "," & TitleFieldName & "," & ValueIDFieldName & "," & cmdSQL)
    End Sub

    'Repository
    Public Shared Sub FillComboWithParentID(ByVal cMyConnectionString As ClsMyConnectionString, ByVal TableName As String, _
    ByVal TitleFieldName As String, _
    ByVal ValueIDFieldName As String, _
    ByVal cbCompo As DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox, _
    ByVal ParentFieldName As String, _
    ByVal ParentFieldValueID As Integer, ByVal ClearItems As Boolean, Optional ByVal showAny As Boolean = False)

        Dim cmdSQL As String
        Try

            Dim anySQL As String = ""
            If IsDBNull(ParentFieldValueID) Then Exit Sub
            If IsNothing(ParentFieldValueID) Then Exit Sub

            If showAny Then
                ' Add The < any > option
                anySQL = "SELECT -1 AS " & ValueIDFieldName & ", '< any >' " & TitleFieldName & " From " & TableName & " UNION "
            End If

            ' The table SQL
            Dim tableSQL As String = "Select " & ValueIDFieldName & "," & TitleFieldName & " From " & TableName & " WHERE " & ParentFieldName & "=" & ParentFieldValueID

            Dim rs As New ClsMyADODB(cMyConnectionString)

            'Combined SQL 
            cmdSQL = anySQL + tableSQL

            Dim i As Integer = 0
            If ClearItems Then cbCompo.Items.Clear()
            If rs.Open(cmdSQL, ClsMyADODB.OpenMode.ReadOnlyMode) Then
                While Not rs.EOF
                    i = i + 1
                    If Not Common.CheckNULL(rs.GetFieldValue(TitleFieldName, ""), "") Then
                        Dim ic As New DevExpress.XtraEditors.Controls.ImageComboBoxItem
                        ic.Description = rs.GetFieldValue(TitleFieldName, "")
                        ic.Value = rs.GetFieldValue(ValueIDFieldName, 0)
                        cbCompo.Items.Add(ic)
                    End If
                    rs.MoveNext()
                End While
            End If
            rs.Close()
            Exit Sub
        Catch ex As Exception
            gER.ErrorMsgBox(ex, "FillComboWithParentID Table:" & TableName & "," & TitleFieldName & "," & ValueIDFieldName & "," & cmdSQL)
        End Try
    End Sub


    Public Shared Sub AddItemImageCombo(ByVal cbCompo As DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox, _
    ByVal ItemID As Integer, _
    ByVal ItemTitle As String)
        Try
            Dim ic As New DevExpress.XtraEditors.Controls.ImageComboBoxItem
            ic.Description = ItemTitle
            ic.Value = ItemID
            cbCompo.Properties.Items.Add(ic)
            Exit Sub
        Catch ex As Exception
            gER.ErrorMsgBox(ex, "AddItemImageCombo")
        End Try
    End Sub

    Public Shared Sub AddItemImageCombo(ByVal cbCompo As DevExpress.XtraEditors.ImageComboBoxEdit, _
    ByVal ItemTitle As String, _
    ByVal ItemID As Integer)
        Try
            Dim ic As New DevExpress.XtraEditors.Controls.ImageComboBoxItem
            ic.Description = ItemTitle
            ic.Value = ItemID
            cbCompo.Properties.Items.Add(ic)
            Exit Sub
        Catch ex As Exception
            gER.ErrorMsgBox(ex, "AddItemImageCombo2")
        End Try
    End Sub


    Public Shared Sub SetValueCombo(ByVal imageCompo As DevExpress.XtraEditors.ImageComboBoxEdit, ByVal ID As Integer)
        Try
            imageCompo.EditValue = ID
            Dim i As Integer
            imageCompo.SelectAll()
            Dim sum As Integer = imageCompo.SelectionLength
            For i = 0 To sum
                imageCompo.SelectedIndex = i
                If imageCompo.EditValue = ID Then Exit Sub
            Next

        Catch ex As Exception
            gER.ErrorMsgBox(ex, "")
        End Try
    End Sub

    Public Shared Sub SetValueCombo(ByVal imageCompo As DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox, ByVal ID As Integer)
        Try
            'imageCompo..EditValue = ID
            'Dim i As Integer
            'imageCompo.SelectAll()
            'Dim sum As Integer = imageCompo.SelectionLength
            'For i = 0 To sum
            '    imageCompo.SelectedIndex = i
            '    If imageCompo.EditValue = ID Then Exit Sub
            'Next

        Catch ex As Exception
            gER.ErrorMsgBox(ex, "")
        End Try
    End Sub

    Public Shared Sub FillComboNET(ByVal ConnectionStringNET As String, ByVal TableName As String, _
ByVal TitleFieldName As String, _
ByVal ValueIDFieldName As String, _
ByVal cbCompo As DevExpress.XtraEditors.ImageComboBoxEdit, _
ByVal ClearItems As Boolean, ByVal Value As Integer, Optional ByVal showAny As Boolean = False)
        Try
            FillComboNET(ConnectionStringNET, TableName, TitleFieldName, ValueIDFieldName, cbCompo, ClearItems, showAny)
            SetValueCombo(cbCompo, Value)

        Catch ex As Exception
            gER.ErrorMsgBox(ex, "FillComboNET Table:" & TableName & "," & TitleFieldName & "," & ValueIDFieldName)
        End Try
    End Sub

    Public Shared Sub FillComboNET(ByVal ConnectionStringNET As String, ByVal TableName As String, _
ByVal TitleFieldName As String, _
ByVal ValueIDFieldName As String, _
ByVal cbCompo As DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox, _
ByVal ClearItems As Boolean, ByVal Value As Integer, Optional ByVal showAny As Boolean = False)
        Try
            FillComboNET(ConnectionStringNET, TableName, TitleFieldName, ValueIDFieldName, cbCompo, ClearItems, showAny)
            SetValueCombo(cbCompo, Value)

        Catch ex As Exception
            gER.ErrorMsgBox(ex, "FillComboNET Table:" & TableName & "," & TitleFieldName & "," & ValueIDFieldName)
        End Try
    End Sub


    Public Shared Sub FillComboNET(ByVal ConnectionStringNET As String, ByVal TableName As String, _
ByVal TitleFieldName As String, _
ByVal ValueIDFieldName As String, _
ByVal cbCompo As DevExpress.XtraEditors.ImageComboBoxEdit, _
ByVal ClearItems As Boolean, Optional ByVal showAny As Boolean = False)
        Try
            Dim anySQL As String = ""
            If showAny Then
                ' Add The < any > option
                anySQL = "SELECT -1 AS " & ValueIDFieldName & ", '< any >' " & TitleFieldName & " From " & TableName & " UNION "
            End If
            ' The table SQL
            Dim tableSQL As String = "Select " & ValueIDFieldName & "," & TitleFieldName & " From " & TableName
            'Combined SQL 
            If ClearItems Then cbCompo.Properties.Items.Clear()

            Dim cmdSQL = anySQL + tableSQL
            Dim i As Integer = 0
            Dim cmd As SqlCommand
            cmd = New SqlCommand(cmdSQL, New SqlConnection(ConnectionStringNET))
            Dim rsRead As SqlDataReader
            cmd.Connection.Open()
            rsRead = cmd.ExecuteReader()
            While rsRead.Read()
                i = i + 1
                If Len(Common.CheckNULL(rsRead(TitleFieldName), "")) Then
                    Dim ic As New DevExpress.XtraEditors.Controls.ImageComboBoxItem
                    ic.Description = Common.CheckNULL(rsRead(TitleFieldName), "")
                    ic.Value = Common.CheckNULL(rsRead(ValueIDFieldName), 0)
                    cbCompo.Properties.Items.Add(ic)
                End If
            End While
            rsRead.Close()
            cmd.Connection.Close()
        Catch ex As Exception
            gER.ErrorMsgBox(ex, "FillComboNET Table:" & TableName & "," & TitleFieldName & "," & ValueIDFieldName)
        End Try
    End Sub



    Public Shared Sub FillComboNET(ByVal ConnectionStringNET As String, ByVal TableName As String, _
ByVal TitleFieldName As String, _
ByVal ValueIDFieldName As String, _
ByVal cbCompo As DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox, _
ByVal ClearItems As Boolean, Optional ByVal showAny As Boolean = False)
        Try
            Dim anySQL As String = ""
            If showAny Then
                ' Add The < any > option
                anySQL = "SELECT -1 AS " & ValueIDFieldName & ", '< any >' " & TitleFieldName & " From " & TableName & " UNION "
            End If
            ' The table SQL
            Dim tableSQL As String = "Select " & ValueIDFieldName & "," & TitleFieldName & " From " & TableName
            'Combined SQL 
            If ClearItems Then cbCompo.Properties.Items.Clear()

            Dim cmdSQL = anySQL + tableSQL
            Dim i As Integer = 0
            Dim cmd As SqlCommand
            cmd = New SqlCommand(cmdSQL, New SqlConnection(ConnectionStringNET))
            Dim rsRead As SqlDataReader
            cmd.Connection.Open()
            rsRead = cmd.ExecuteReader()
            While rsRead.Read()
                i = i + 1
                If Len(Common.CheckNULL(rsRead(TitleFieldName), "")) Then
                    Dim ic As New DevExpress.XtraEditors.Controls.ImageComboBoxItem
                    ic.Description = Common.CheckNULL(rsRead(TitleFieldName), "")
                    ic.Value = Common.CheckNULL(rsRead(ValueIDFieldName), 0)
                    cbCompo.Properties.Items.Add(ic)
                End If
            End While
            rsRead.Close()
            cmd.Connection.Close()
        Catch ex As Exception
            gER.ErrorMsgBox(ex, "FillComboNET Table:" & TableName & "," & TitleFieldName & "," & ValueIDFieldName)
        End Try
    End Sub



    '    Public Shared Sub FillComboRepositoryNET(ByVal ConnectionStringNET As String, ByVal TableName As String, _
    'ByVal TitleFieldName As String, _
    'ByVal ValueIDFieldName As String, _
    'ByVal cbCompo As DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox, _
    'ByVal ClearItems As Boolean, Optional ByVal showAny As Boolean = False)
    '        Try
    '            Dim anySQL As String = ""
    '            If showAny Then
    '                ' Add The < any > option
    '                anySQL = "SELECT -1 AS " & ValueIDFieldName & ", '< any >' " & TitleFieldName & " From " & TableName & " UNION "
    '            End If
    '            ' The table SQL
    '            Dim tableSQL As String = "Select " & ValueIDFieldName & "," & TitleFieldName & " From " & TableName
    '            'Combined SQL 
    '            If ClearItems Then cbCompo.Properties.Items.Clear()

    '            Dim cmdSQL = anySQL + tableSQL
    '            Dim i As Integer = 0
    '            Dim cmd As SqlCommand
    '            cmd = New SqlCommand(cmdSQL, New SqlConnection(ConnectionStringNET))
    '            Dim rsRead As SqlDataReader
    '            cmd.Connection.Open()
    '            rsRead = cmd.ExecuteReader()
    '            While rsRead.Read()
    '                i = i + 1
    '                If Len(Common.CheckNULL(rsRead(TitleFieldName), "")) Then
    '                    Dim ic As New DevExpress.XtraEditors.Controls.ImageComboBoxItem
    '                    ic.Description = Common.CheckNULL(rsRead(TitleFieldName), "")
    '                    ic.Value = Common.CheckNULL(rsRead(ValueIDFieldName), 0)
    '                    cbCompo.Properties.Items.Add(ic)
    '                End If
    '            End While
    '            rsRead.Close()
    '            cmd.Connection.Close()
    '        Catch ex As Exception
    '            gER.ErrorMsgBox(ex, "FillComboNET Table:" & TableName & "," & TitleFieldName & "," & ValueIDFieldName)
    '        End Try
    '    End Sub




End Class


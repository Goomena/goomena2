﻿

Public Class clsTemplatesCache

    Public Enum HtmlTemplate
        None = 0
        SocialButtons = 1
    End Enum


    Private Shared Property gTemplateList As New Dictionary(Of HtmlTemplate, String)
    Private Shared Property gTemplateList_JS As New Dictionary(Of HtmlTemplate, String)


    Public Shared Function GetTemplate(key As HtmlTemplate) As String

        'Dim LagIDparam As String = LagID
        Dim templateContent As String = Nothing

        Try

            If (gTemplateList Is Nothing) Then gTemplateList = New Dictionary(Of HtmlTemplate, String)

            If (Not gTemplateList.ContainsKey(key)) Then
                templateContent = _GetTemplate(key)
                If (Not String.IsNullOrEmpty(templateContent)) Then
                    Try
                        gTemplateList.Add(key, templateContent)
                    Catch
                    End Try
                End If
            End If

            If (String.IsNullOrEmpty(templateContent)) Then
                If (gTemplateList.ContainsKey(key)) Then
                    templateContent = gTemplateList(key)
                End If
            End If
        Catch ex As Exception

        End Try

        Return templateContent
    End Function

    Public Shared Function GetTemplate_JS(key As HtmlTemplate) As String

        'Dim LagIDparam As String = LagID
        Dim templateContent As String = Nothing

        Try
            templateContent = GetTemplate(key)
            templateContent = AppUtils.JS_PrepareString(templateContent)
        Catch ex As Exception

        End Try

        Return templateContent
    End Function


    Private Shared Function _GetTemplate(key As HtmlTemplate) As String

        'Dim LagIDparam As String = LagID
        Dim templateContent As String = Nothing

        Try

            Select Case key
                Case HtmlTemplate.SocialButtons
                    templateContent = clsTemplatesCache.GetTemplateContent("/Members/social_buttons.htm")
            End Select


        Catch ex As Exception

        End Try

        Return templateContent
    End Function


    Public Shared Function GetTemplateContent(filepath) As String
        Dim transfer_row_htm As String = ""

        Dim fi As New System.IO.FileInfo(System.Web.HttpContext.Current.Server.MapPath(filepath))
        Using sr As System.IO.StreamReader = fi.OpenText()
            transfer_row_htm = sr.ReadToEnd()
        End Using
       

        transfer_row_htm = transfer_row_htm.Replace(Chr(13), "").Replace(Chr(10), "").Replace("    ", "")
        Return transfer_row_htm
    End Function



    Public Shared Sub ClearCache()
        gTemplateList = New Dictionary(Of HtmlTemplate, String)()
        gTemplateList_JS = New Dictionary(Of HtmlTemplate, String)()
    End Sub

End Class

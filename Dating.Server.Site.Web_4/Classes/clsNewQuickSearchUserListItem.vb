﻿
Imports Dating.Server.Core.DLL



Public Class clsNewQuickSearchUserListItem
    Public Sub New()
        ItemIndex = DateTime.UtcNow.Ticks
    End Sub
    Public Property ItemIndex As Long
    Public Property AddItemCssClass As String
    Public Property ProfileID As Integer
    Public Property MirrorProfileID As Integer
    Public Property Genderid As Integer
    Public Property OtherMemberLoginName As String
    Public Property OtherMemberProfileID As Integer
    Public Property OtherMemberMirrorProfileID As Integer
    Public Property OtherMemberGenderid As Integer
    Public Property OtherMemberAge As Integer
    Public Property OtherMemberCity As String
    Public Property OtherMemberRegion As String
    Public Property OtherMemberCountry As String
    Public Property OtherMemberHair As String
    Public Property OtherMemberHairClass As String
    Public Property OtherMemberHairTooltip As String
    Public Property OtherMemberImageFileName As String
    Public Property OtherMemberImageUrl As String
    Public Property OtherMemberImageThumbUrl As String
    Public Property OtherMemberProfileViewUrl As String
    Public Property OtherMemberIsOnline As Boolean
    Public Property OtherMemberIsOnlineText As String
    Public Property OtherMemberDrinkingText As String
    Public Property OtherMemberDrinkingClass As String
    Public Property OtherMemberSmokingText As String
    Public Property OtherMemberSmokingClass As String
    Public Property OtherMemberRelationshipTooltip As String
    Public Property OtherMemberRelationshipClass As String
    Public Property OtherMemberBreastSizeClass As String
    Public Property OtherMemberBreastSizeTooltip As String
    Public Property OtherMemberBirthday As DateTime?
    Public Property OtherMemberIsMale As Boolean
    Public Property OtherMemberIsFemale As Boolean
    Public Property PhotosCountString As String
    Public Property RowNumber As Integer
    Public Property LAGID As String
    Public Property OtherMemberGenderClass As String
    Public Property OtherMemberIsOnlineRecently As Boolean
    Public Property AllowWink As Boolean
    Public Property AllowUnWink As Boolean
    Public Property AllowFavorite As Boolean
    Public Property AllowUnfavorite As Boolean
    Public Property AllowActionsCreateOffer As Boolean
    Public Property OtherMemberIsVip As Boolean
    Public Property SendMessageUrl As String
    Public Property CreateOfferUrl As String
    Public Property OfferID As Integer


    Public Sub FillTextResourceProperties(pageData As clsPageData)

    End Sub
    Public Property ZodiacString As String

    Private __ZodiacName As String
    Public ReadOnly Property ZodiacName As String
        Get
            If (Me.OtherMemberBirthday.HasValue AndAlso Me.OtherMemberBirthday.Value > DateTime.MinValue) Then
                If (__ZodiacName Is Nothing) Then __ZodiacName = "Zodiac" & ProfileHelper.ZodiacName(Me.OtherMemberBirthday)
                If (__ZodiacName Is Nothing) Then __ZodiacName = String.Empty
            End If
            Return __ZodiacName
        End Get
    End Property


End Class

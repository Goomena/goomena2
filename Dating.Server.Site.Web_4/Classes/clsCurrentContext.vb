﻿Imports System.Web.SessionState
Imports Library.Public
Imports System.Reflection
Imports Dating.Server.Core.DLL

Public Class clsCurrentContext

    'Private __context As HttpContext
    'Private ReadOnly Property Session As HttpSessionState
    '    Get
    '        Return __context.Session
    '    End Get
    'End Property
    'Private ReadOnly Property Request As HttpRequest
    '    Get
    '        Return __context.Request
    '    End Get
    'End Property
    'Private ReadOnly Property Response As HttpResponse
    '    Get
    '        Return __context.Response
    '    End Get
    'End Property
    'Private ReadOnly Property Server As HttpServerUtility
    '    Get
    '        Return __context.Server
    '    End Get
    'End Property

    'Public Sub New(ByRef context As HttpContext)
    '    __context = context
    'End Sub


    Public Shared Function VerifyLogin(ByRef cnt As HttpContextBase) As Boolean
        Dim Request As HttpRequestBase = cnt.Request
        Dim Session As HttpSessionStateBase = cnt.Session

        Dim isVerified As Boolean =
            Request.Params("AUTH_USER") <> "" AndAlso
            cnt.User.Identity.IsAuthenticated AndAlso
            Session("ProfileID") > 0 AndAlso
            Request.Params("AUTH_USER") = Session("LoginName")

        If (Not isVerified AndAlso
            Not String.IsNullOrEmpty(Request.QueryString(FormsAuthentication.FormsCookieName))) Then

            clsLogin.PerfomRelogin_ByAUTHCookies()

            isVerified =
            Request.Params("AUTH_USER") <> "" AndAlso
            cnt.User.Identity.IsAuthenticated AndAlso
            Session("ProfileID") > 0 AndAlso
            Request.Params("AUTH_USER") = Session("LoginName")

        End If

        If (isVerified AndAlso cnt.Session("EUS_Profile_IsProfileActive") Is Nothing) Then

            Dim ProfileID As Integer? = cnt.Session("ProfileID")
            If (clsNullable.NullTo(ProfileID) > 0) Then
                isVerified = DataHelpers.EUS_Profile_IsProfileActive(ProfileID)
                cnt.Session("EUS_Profile_IsProfileActive") = isVerified
            End If
        End If



        Return isVerified
    End Function

    Public Shared Function VerifyLogin() As Boolean
        Dim Request As HttpRequest = HttpContext.Current.Request
        Dim Session As HttpSessionState = HttpContext.Current.Session

        Dim isVerified As Boolean =
            Request.Params("AUTH_USER") <> "" AndAlso
            HttpContext.Current.User.Identity.IsAuthenticated AndAlso
            Session("ProfileID") > 0 AndAlso
            Request.Params("AUTH_USER") = Session("LoginName")

        If (Not isVerified AndAlso
            Not String.IsNullOrEmpty(Request.QueryString(FormsAuthentication.FormsCookieName))) Then

            clsLogin.PerfomRelogin_ByAUTHCookies()

            isVerified =
            Request.Params("AUTH_USER") <> "" AndAlso
            HttpContext.Current.User.Identity.IsAuthenticated AndAlso
            Session("ProfileID") > 0 AndAlso
            Request.Params("AUTH_USER") = Session("LoginName")

        End If

        If (isVerified AndAlso HttpContext.Current.Session("EUS_Profile_IsProfileActive") Is Nothing) Then

            Dim ProfileID As Integer? = HttpContext.Current.Session("ProfileID")
            If (clsNullable.NullTo(ProfileID) > 0) Then
                isVerified = DataHelpers.EUS_Profile_IsProfileActive(ProfileID)
                HttpContext.Current.Session("EUS_Profile_IsProfileActive") = isVerified
            End If
        End If
     
       

        Return isVerified
    End Function


    Public Shared Function IsAffiliate() As Boolean

        If (clsSessionVariables.GetCurrent().MemberData.Role = "AFFILIATE") Then
            Return True
        End If

        Return False
    End Function



    Public Shared Sub LogOff()
        FormsAuthentication.SignOut()
        HttpContext.Current.Session.Abandon()

        HttpContext.Current.Request.Cookies.Remove(".ASPNETAUTH")

        Dim cnt As Integer
        For cnt = 0 To HttpContext.Current.Request.Cookies.Count - 1
            HttpContext.Current.Request.Cookies(cnt).Expires = DateTime.Now.AddYears(-1)
            HttpContext.Current.Request.Cookies(cnt).Domain = Nothing
        Next

        For cnt = 0 To HttpContext.Current.Response.Cookies.Count - 1
            HttpContext.Current.Response.Cookies(cnt).Expires = DateTime.Now.AddYears(-1)
            HttpContext.Current.Response.Cookies(cnt).Domain = Nothing
        Next

        ' clear authentication cookie
        Dim cookie1 As HttpCookie = New HttpCookie(FormsAuthentication.FormsCookieName, "")
        cookie1.Expires = DateTime.Now.AddYears(-1)
        cookie1.Domain = Nothing
        HttpContext.Current.Response.Cookies.Add(cookie1)

        ' clear session cookie (not necessary for your current problem but i would recommend you do it anyway)
        Dim cookie2 As HttpCookie = New HttpCookie("ASP.NET_SessionId", "")
        cookie2.Expires = DateTime.Now.AddYears(-1)
        cookie2.Domain = Nothing
        HttpContext.Current.Response.Cookies.Add(cookie2)


    End Sub


    Public Shared Sub ClearSession(Session As HttpSessionState)
        Try
            'Dim Session As HttpSessionState = HttpContext.Current.Session

            If (Session("ProfileID") > 0) Then

                If (Session("IsAdminLogin") Is Nothing OrElse Session("IsAdminLogin") = False) Then
                    DataHelpers.UpdateEUS_Profiles_LogoutData(Session("ProfileID"))

                    'Try
                    '    DataHelpers.LogProfileAccess(Session("ProfileID"), userName, password, DataRecordLoginReturn.IsValid, "Login")
                    'Catch ex As Exception
                    'End Try
                End If


                Try
                    DataHelpers.Update_EUS_Profiles_UpdateAvailableCredits(Session("ProfileID"), 0, True)
                Catch ex As Exception
                    Try
                        WebErrorSendEmail(ex, "ClearSession->Update_EUS_Profiles_UpdateAvailableCredits")
                    Catch 
                    End Try
                End Try

                Try
                    Dim LogProfileAccessID As Long = 0
                    If (Session("IsAdminLogin") = True) Then
                        LogProfileAccessID = DataHelpers.LogProfileAccess(Session("ProfileID"),
                                                     Session("LoginName"), "",
                                                     True,
                                                     "Admin Logout", Nothing,
                                                     Session("lagCookie"),
                                                     Session("IP"),
                                                     Session("IsMobileAccess")) 'HttpContext.Current.Request.ServerVariables("REMOTE_ADDR")
                    Else
                        LogProfileAccessID = DataHelpers.LogProfileAccess(Session("ProfileID"),
                                                     Session("LoginName"), "",
                                                     True,
                                                     "Logout", Nothing,
                                                     Session("lagCookie"),
                                                     Session("IP"),
                                                     Session("IsMobileAccess"))
                    End If


                    If (Session("IsMobileAccess") Is Nothing) Then
                        clsCurrentContext.AddAccessID(LogProfileAccessID)
                    End If
                Catch
                End Try

            End If

            'Session("LagID") = Nothing
            Session("Logoff") = Nothing
            Session("ProfileID") = 0
            Session("IsAdminLogin") = Nothing
            Session("clsAffiliateCredits") = Nothing
            Session("NoRedirectToMembersDefault") = Nothing
            Session("clsSessionVariables") = Nothing
            Session("CustomerAvailableCredits") = Nothing
            Session("CustomerAvailableCredits_PREV") = Nothing
            Session("FBUser") = Nothing
            Session("TWUser") = Nothing
            Session("StartDate") = Nothing
            Session("SessionVariables") = New clsSessionVariables()
            Session("SelectProductsPopupTR") = Nothing
            Session("MAX_LOGIN_RETRIES") = Nothing
            Session("LOGIN_RETRIES") = Nothing
            Session("AdvancedSearch_Dont_Save_Dont_Ask") = Nothing
            Session("SET_ADDITIONAL_INFO") = Nothing
            Session("SMSInfoPopup") = Nothing
            Session("HasSeenGiftPopUp") = Nothing
            Session("HasNewTermsPopUp") = Nothing
            Session("LastNotificationId") = Nothing
            ' Session("RedirectedLoveHate") = Nothing 
            Dim ip As String = GetCurrentIP()
            If (Not String.IsNullOrEmpty(ip)) Then Session("IP") = ip

            'clsSessionVariables.GetCurrent().ReferrerCredits = Nothing
        Catch ex As Exception

        End Try
    End Sub

    Public Shared Sub AddAccessID(accessID As Long)
        Dim Session As HttpSessionState = HttpContext.Current.Session
        Dim MobileAccessIDList As List(Of Long) = Nothing
        If (Session("MobileAccessIDList") Is Nothing) Then
            MobileAccessIDList = New List(Of Long)()
        Else
            MobileAccessIDList = Session("MobileAccessIDList")
        End If
        MobileAccessIDList.Add(accessID)
        Session("MobileAccessIDList") = MobileAccessIDList
    End Sub


    Public Shared Sub AddSuspendedMessageID(messageID As Long)
        Dim Session As HttpSessionState = HttpContext.Current.Session
        Dim SuspendedMessageIDList As List(Of Long) = Nothing
        If (Session("SuspendedMessageIDList") Is Nothing) Then
            SuspendedMessageIDList = New List(Of Long)()
        Else
            SuspendedMessageIDList = Session("SuspendedMessageIDList")
        End If
        SuspendedMessageIDList.Add(messageID)
        Session("SuspendedMessageIDList") = SuspendedMessageIDList
    End Sub


    Public Shared Function IsAlreadySuspendedMessageID(messageID As Long) As Boolean
        Dim isSuspended As Boolean = False
        Dim Session As HttpSessionState = HttpContext.Current.Session
        If (Session("SuspendedMessageIDList") IsNot Nothing) Then
            Dim SuspendedMessageIDList As List(Of Long) = Nothing
            SuspendedMessageIDList = Session("SuspendedMessageIDList")
            isSuspended = SuspendedMessageIDList.Contains(messageID)
        End If
        Return isSuspended
    End Function

    Public Shared Sub RemoveSuspendedMessageID(messageID As Long)
        '  Dim isSuspended As Boolean = False
        Dim Session As HttpSessionState = HttpContext.Current.Session
        If (Session("SuspendedMessageIDList") IsNot Nothing) Then
            Dim SuspendedMessageIDList As List(Of Long) = Nothing
            SuspendedMessageIDList = Session("SuspendedMessageIDList")
            If (SuspendedMessageIDList.Contains(messageID)) Then
                SuspendedMessageIDList.Remove(messageID)
            End If
        End If
    End Sub


    Public Shared Function GetCountryByIP() As clsCountryByIP
        Dim country As clsCountryByIP = Nothing
        Dim Session As HttpSessionState = HttpContext.Current.Session

        If (Session("GEO_COUNTRY_INFO") IsNot Nothing) Then country = Session("GEO_COUNTRY_INFO")

        If (country Is Nothing) Then
            If (Session("IP") Is Nothing) Then Session("IP") = clsCurrentContext.GetCurrentIP()

            country = clsCountryByIP.GetCountryCodeFromIP(Session("IP"), ConfigurationManager.ConnectionStrings("GeoDBconnectionString").ConnectionString)

            If (Len(country.CountryCode) = 2) Then
                Session("GEO_COUNTRY_CODE") = country.CountryCode
                Session("GEO_COUNTRY_CODE_BY_SITE") = "GeoDB"
                Session("GEO_COUNTRY_LATITUDE") = country.latitude
                Session("GEO_COUNTRY_LONGITUDE") = country.longitude
                Session("GEO_COUNTRY_POSTALCODE") = country.postalCode
                Session("GEO_COUNTRY_CITY") = country.city
                Session("GEO_COUNTRY_INFO") = country
            End If

        End If

        Return country
    End Function


    Public Shared Function IsLocalhost() As Boolean
        Try
            If (HttpContext.Current IsNot Nothing) Then
                Dim Request As HttpRequest = HttpContext.Current.Request
                Dim ip As String = Request.ServerVariables("REMOTE_ADDR") '-- the IP address of the visitor

                If (ip.StartsWith("127.") OrElse ip.Equals("::1")) Then
                    Return True
                End If
            End If
        Catch
        End Try

        Return False
    End Function


    Public Shared Function UseHTTPSLogin() As Boolean
        Try
            ' redirect user to HTTPS url
            Dim UseHTTPS As String = ConfigurationManager.AppSettings("UseHTTPSLogin")
            If (Not clsCurrentContext.IsLocalhost() AndAlso
                Not String.IsNullOrEmpty(UseHTTPS) AndAlso
                UseHTTPS = "1") Then

                Return True

            End If

        Catch
        End Try

        Return False
    End Function


    Public Shared Function UseHTTPSRegister() As Boolean
        Try
            ' redirect user to HTTPS url
            Dim UseHTTPS As String = ConfigurationManager.AppSettings("UseHTTPSRegister")
            If (Not clsCurrentContext.IsLocalhost() AndAlso
                Not String.IsNullOrEmpty(UseHTTPS) AndAlso
                UseHTTPS = "1") Then

                Return True

            End If

        Catch
        End Try

        Return False
    End Function


    Public Shared Function UseHTTPSMembers() As Boolean
        Try
            ' redirect user to HTTPS url
            Dim UseHTTPS As String = ConfigurationManager.AppSettings("UseHTTPSMembers")
            If (Not String.IsNullOrEmpty(UseHTTPS) AndAlso UseHTTPS = "1") Then

                Return True

            End If

        Catch
        End Try

        Return False
    End Function


    Public Shared Function ForceHTTPMembers() As Boolean
        Try
            ' redirect user to HTTPS url
            Dim s As String = ConfigurationManager.AppSettings("ForceHTTPMembers")
            If (Not String.IsNullOrEmpty(s) AndAlso s = "1") Then

                Return True

            End If

        Catch
        End Try

        Return False
    End Function


    Public Shared Function ForceHTTPPublic() As Boolean
        Try
            ' redirect user to HTTPS url
            Dim s As String = ConfigurationManager.AppSettings("ForceHTTPPublic")
            If (Not String.IsNullOrEmpty(s) AndAlso s = "1") Then

                Return True

            End If

        Catch
        End Try

        Return False
    End Function


    Public Shared Function UseHTTPSOnPayment() As Boolean
        Try
            ' redirect user to HTTPS url
            Dim s As String = ConfigurationManager.AppSettings("UseHTTPSOnPayment")
            If (Not String.IsNullOrEmpty(s) AndAlso s = "1") Then

                Return True

            End If

        Catch
        End Try

        Return False
    End Function


    Public Shared ReadOnly Property ExceptionsEmail As String
        Get
            Return ConfigurationManager.AppSettings("ExceptionsEmail")
        End Get
    End Property

    Public Shared ReadOnly Property gToEmail As String
        Get
            Return ConfigurationManager.AppSettings("gToEmail")
        End Get
    End Property


    Public Shared Function GetCurrentIP() As String
        Dim ip As String = ""

        Try
            Dim Request As HttpRequest = HttpContext.Current.Request
            ip = Request.ServerVariables("REMOTE_ADDR") '-- the IP address of the visitor

            If (clsCurrentContext.IsLocalhost()) Then
                ip = "77.49.94.48" 'GR
                'ip = "130.204.137.177" 'BG
                'ip = "66.220.152.114" ' US
                'ip = "81.61.207.139" 'es
                'ip = "151.250.122.99" ' TR
                'ip = "188.4.80.226" ' GR
                'ip = "5.135.134.124" ' FR
                'ip = "213.181.73.145" ' ES
                ' Session("IP") = "5.11.128.22" ' TR
                'Session("IP") = "188.165.13.64" ' FR
                'ip = "46.246.144.52" '"79.130.17.211"
            End If
        Catch
        End Try

        Return ip
    End Function



    Public Shared Function GetCookieID() As String
        Dim ip As String = ""

        Try
            Dim Session As HttpSessionState = HttpContext.Current.Session
            ip = Session("lagCookie") '-- lag cookie set by site
        Catch
        End Try

        Return ip
    End Function

    Private Shared _IsHTTPSAvailable As Boolean?
    Public Shared Function IsHTTPSAvailable() As Boolean
        If (_IsHTTPSAvailable Is Nothing) Then
            _IsHTTPSAvailable = False
            Try

                Dim url As String = "https://goomena.com/index.html".ToLower()
                Try
                    Dim rp As String = clsWebPageProcessor.GetPageContent(url, 5000)
                    _IsHTTPSAvailable = True
                Catch
                End Try
            Catch
            End Try
        End If

        Return _IsHTTPSAvailable.Value
    End Function


    Public Shared Function FormatRedirectUrl(redirectUrl As String) As String
        Dim c As HttpContext = HttpContext.Current
        'Don’t append the forms auth ticket for unauthenticated users 
        '   or
        'for users authenticated with a different mechanism
        If Not c.User.Identity.IsAuthenticated OrElse Not (c.User.Identity.AuthenticationType = "Forms") Then
            Return redirectUrl
        End If

        'Determine if we need to append to an existing query-string or       not
        Dim qsSpacer As String
        qsSpacer = If(redirectUrl.IndexOf("?") > -1, "&", "?")

        'Build the new redirect URL. Assuming that currently using
        'forms authentication. Change the below FormsIdentity if required.

        Dim fi As FormsIdentity = DirectCast(c.User.Identity, FormsIdentity)
        Dim newRedirectUrl As String = (redirectUrl & qsSpacer) + FormsAuthentication.FormsCookieName & "=" & FormsAuthentication.Encrypt(fi.Ticket)
        Return newRedirectUrl
    End Function

    Public Shared Sub RedirectToMembersDefault(Optional useThreadAbort As Boolean = True)
        Dim Response As HttpResponse = HttpContext.Current.Response
        Dim url As String = "/Members/Default.aspx"
        url = clsCurrentContext.FormatRedirectUrl(url)

        If (useThreadAbort) Then
            Response.Redirect(url)
        Else
            Response.Redirect(url, False)
            HttpContext.Current.ApplicationInstance.CompleteRequest()
        End If
    End Sub



    Public Shared Function GetRefreshURL() As String
        Dim Request As HttpRequest = HttpContext.Current.Request
        ' Dim Response As HttpResponse = HttpContext.Current.Response

        Dim url As String = Request.Url.ToString()
        If (url.IndexOf("?") > -1) Then
            url = url.Remove(url.IndexOf("?") + 1)
            For Each itm As String In Request.QueryString.AllKeys
                If (Not String.IsNullOrEmpty(itm) AndAlso itm.ToUpper() <> "RFSH") Then
                    url = url & itm & "=" & HttpUtility.UrlEncode(Request.QueryString(itm)) & "&"
                End If
            Next
            url = url.TrimEnd({"&"c, "?"c})
        End If
        url = If(url IsNot Nothing, url, "") & If(url.IndexOf("?") > -1, "&", "?") & "rfsh=" & DateTime.UtcNow.Ticks
        Return url
    End Function


End Class

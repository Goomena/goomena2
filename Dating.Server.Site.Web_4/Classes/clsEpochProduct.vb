﻿
Public Class clsEpochProduct


    Public Property SalesSiteProductID As Long
    Public Property SiteID As Integer = 5
    Public Property Description As String
    Public Property SiteProductCode As String
    Public Property ProviderProductCode As String
    Public Property MoneyAmount As Single
    Public Property Currency As String = "EUR"
    Public Property Quantity As Integer



    Shared Products As List(Of clsEpochProduct)
    Shared Sub New()
        FillData()
    End Sub


    Public Shared Function GetProductBySiteCode(sitecode As String) As clsEpochProduct
        Dim ep As clsEpochProduct = (From itm In clsEpochProduct.Products
                                     Where itm.SiteProductCode = sitecode
                                     Select itm).FirstOrDefault()
        Return ep
    End Function


    Public Shared Function GetProductBySalesSiteProductID(SiteProductID As Long) As clsEpochProduct
        Dim ep As clsEpochProduct = (From itm In clsEpochProduct.Products
                                     Where itm.SalesSiteProductID = SiteProductID
                                     Select itm).FirstOrDefault()
        Return ep
    End Function


#Region "Fill products"
    Shared Sub FillData()
        Products = New List(Of clsEpochProduct)

        Dim a00 As New clsEpochProduct()
        Dim a01 As New clsEpochProduct()
        Dim a02 As New clsEpochProduct()
        Dim a03 As New clsEpochProduct()
        Dim a04 As New clsEpochProduct()
        Dim a05 As New clsEpochProduct()
        Dim a06 As New clsEpochProduct()
        Dim a07 As New clsEpochProduct()
        Dim a08 As New clsEpochProduct()
        Dim a09 As New clsEpochProduct()
        Dim a10 As New clsEpochProduct()
        Dim a11 As New clsEpochProduct()
        Dim a12 As New clsEpochProduct()
        Dim a13 As New clsEpochProduct()
        Dim a14 As New clsEpochProduct()
        Dim a15 As New clsEpochProduct()
        Dim a16 As New clsEpochProduct()
        Dim a17 As New clsEpochProduct()
        Dim a18 As New clsEpochProduct()
        Dim a19 As New clsEpochProduct()
        Dim a20 As New clsEpochProduct()

        a00.SalesSiteProductID = 39
        a01.SalesSiteProductID = 42
        a02.SalesSiteProductID = 43
        a03.SalesSiteProductID = 44
        a04.SalesSiteProductID = 45
        a05.SalesSiteProductID = 46
        a06.SalesSiteProductID = 61
        a07.SalesSiteProductID = 62
        a08.SalesSiteProductID = 63
        a09.SalesSiteProductID = 95
        a10.SalesSiteProductID = 96
        a11.SalesSiteProductID = 97
        a12.SalesSiteProductID = 98
        a13.SalesSiteProductID = 99
        a14.SalesSiteProductID = 100
        a15.SalesSiteProductID = 151
        a16.SalesSiteProductID = 152
        a17.SalesSiteProductID = 158
        a18.SalesSiteProductID = 159
        a19.SalesSiteProductID = 160
        a20.SalesSiteProductID = 161

        a00.Description = "Goomena - 1000 Credits"
        a01.Description = "Goomena - 3000 Credits"
        a02.Description = "Goomena - 6000 Credits"
        a03.Description = "Goomena - 10000 Credits"
        a04.Description = "Goomena - 30000 Credits"
        a05.Description = "Goomena - 50000 Credits"
        a06.Description = "Goomena - 500 Credits"
        a07.Description = "Goomena - 1200 Credits"
        a08.Description = "Goomena - 3000 Credits"
        a09.Description = "Goomena - 3000 Credits"
        a10.Description = "Goomena - 6000 Credits"
        a11.Description = "Goomena - 1000 Credits"
        a12.Description = "Goomena - 1000 Credits"
        a13.Description = "Goomena - 3000 Credits"
        a14.Description = "Goomena - 6000 Credits"
        a15.Description = "Goomena - 5000 Credits"
        a16.Description = "Goomena - 5000 Credits"
        a17.Description = "Goomena - 30 Days"
        a18.Description = "Goomena - 90 Days"
        a19.Description = "Goomena - 180 Days"
        a20.Description = "Goomena - 360 Days"


        a00.SiteProductCode = "dd1000Credits"
        a01.SiteProductCode = "dd3000Credits"
        a02.SiteProductCode = "dd6000Credits"
        a03.SiteProductCode = "dd10000Credits"
        a04.SiteProductCode = "dd30000Credits"
        a05.SiteProductCode = "dd50000Credits"
        a06.SiteProductCode = "dd500CreditsLEK"
        a07.SiteProductCode = "dd1200CreditsLEK"
        a08.SiteProductCode = "dd3000CreditsLEK"
        a09.SiteProductCode = "dd3000CreditsTRY"
        a10.SiteProductCode = "dd6000CreditsTRY"
        a11.SiteProductCode = "dd1000CreditsTRY"
        a12.SiteProductCode = "dd1000Creditszt"
        a13.SiteProductCode = "dd3000Creditszt"
        a14.SiteProductCode = "dd6000Creditszt"
        a15.SiteProductCode = "dd5000Credits"
        a16.SiteProductCode = "dd5000Creditszt"
        a17.SiteProductCode = "dd30Days"
        a18.SiteProductCode = "dd90Days"
        a19.SiteProductCode = "dd180Days"
        a20.SiteProductCode = "dd360Days"


        a00.ProviderProductCode = "cvjoja1p125314"
        a01.ProviderProductCode = "cvjoja2p125316"
        a02.ProviderProductCode = "cvjoja3p125317"
        a03.ProviderProductCode = "cvjoja14p429266"
        a04.ProviderProductCode = Nothing
        a05.ProviderProductCode = Nothing
        a06.ProviderProductCode = "cvjoja7p196953"
        a07.ProviderProductCode = "cvjoja8p196954"
        a08.ProviderProductCode = "cvjoja9p196955"
        a09.ProviderProductCode = "cvjoja11p340396"
        a10.ProviderProductCode = "cvjoja12p340397"
        a11.ProviderProductCode = "cvjoja10p340395"
        a12.ProviderProductCode = "cicace1p276817"
        a13.ProviderProductCode = "cicace2p276818"
        a14.ProviderProductCode = "cicace3p276819"
        a15.ProviderProductCode = "cvjoja13p392652"
        a16.ProviderProductCode = "cvjoja13p392652"
        a17.ProviderProductCode = "cvjoja15p606713"
        a18.ProviderProductCode = "cvjoja16p606714"
        a19.ProviderProductCode = "cvjoja17p606871"
        a20.ProviderProductCode = "cvjoja18p606872"


        a00.MoneyAmount = 15
        a01.MoneyAmount = 36
        a02.MoneyAmount = 66
        a03.MoneyAmount = 99.9
        a04.MoneyAmount = 240
        a05.MoneyAmount = 360
        a06.MoneyAmount = 7
        a07.MoneyAmount = 14
        a08.MoneyAmount = 32
        a09.MoneyAmount = 36
        a10.MoneyAmount = 66
        a11.MoneyAmount = 15
        a12.MoneyAmount = 15
        a13.MoneyAmount = 36
        a14.MoneyAmount = 66
        a15.MoneyAmount = 49
        a16.MoneyAmount = 49
        a17.MoneyAmount = 30
        a18.MoneyAmount = 69
        a19.MoneyAmount = 119
        a20.MoneyAmount = 159


        a00.Quantity = 1000
        a01.Quantity = 3000
        a02.Quantity = 6000
        a03.Quantity = 10000
        a04.Quantity = 30000
        a05.Quantity = 50000
        a06.Quantity = 500
        a07.Quantity = 1200
        a08.Quantity = 3000
        a09.Quantity = 3000
        a10.Quantity = 6000
        a11.Quantity = 1000
        a12.Quantity = 1000
        a13.Quantity = 3000
        a14.Quantity = 6000
        a15.Quantity = 5000
        a16.Quantity = 5000
        a17.Quantity = 30
        a18.Quantity = 90
        a19.Quantity = 180
        a20.Quantity = 360


        Products.Add(a00)
        Products.Add(a01)
        Products.Add(a02)
        Products.Add(a03)
        Products.Add(a04)
        Products.Add(a05)
        Products.Add(a06)
        Products.Add(a07)
        Products.Add(a08)
        Products.Add(a09)
        Products.Add(a10)
        Products.Add(a11)
        Products.Add(a12)
        Products.Add(a13)
        Products.Add(a14)
        Products.Add(a15)
        Products.Add(a16)
        Products.Add(a17)
        Products.Add(a18)
        Products.Add(a19)
        Products.Add(a20)
    End Sub
#End Region


End Class
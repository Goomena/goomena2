﻿
Public Class clsVerifyUserMaySendMessageResult
    Public Enum ErrorReasonEnum
        none = 0
        ErrorNoOfferCannotSendMessage = 1
        ErrorMemberDoesNotExistCannotSendMessage = 2
    End Enum

    Public Property HasErrors As Boolean
    Public Property ErrorReason As ErrorReasonEnum


    Public Property OtherMemberImageUrl As String
    Public Property OtherMemberLoginName As String
    Property OtherMemberProfileViewUrl As String
    Public Property DatingAmount As String

End Class


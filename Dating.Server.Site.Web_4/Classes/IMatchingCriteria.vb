﻿Imports Dating.Server.Core.DLL

Public Interface IMatchingCriterias

#Region "Events"
    Event SaveSearchClicked(sender As Object, e As SaveAndSearchEventArgs)
#End Region

#Region "Properties"
    Property ageMinSelectedValue As Integer
    Property ageMaxSelectedValue As Integer
    Property HeightMinSelectedValue As Integer
    Property HeightMaxSelectedValue As Integer
    Property ddlDistanceSelectedValue As Integer
    Property chkOnlineChecked As Boolean
    Property chkPhotosChecked As Boolean
    Property chkPhotosPrivateChecked As Boolean
    Property chkWillingToTravelChecked As Boolean
    Property SelectedHairColorList As SearchListInfo
    Property SelectedBreastSizeList As SearchListInfo
    Property SelectedEyeColorList As SearchListInfo
    Property SelectedBodyTypeList As SearchListInfo
    Property SelectedRelationShipStatusList As SearchListInfo
    Property SelectedTypeOfDatingList As SearchListInfo
    Property SelectedEducationList As SearchListInfo
    Property SelectedChildrenList As SearchListInfo
    Property SelectedEthnicityList As SearchListInfo
    Property SelectedSmokingHabitsList As SearchListInfo
    Property SelectedDrinkingHabitsList As SearchListInfo
    Property SelectedLangs As SearchListInfo

#End Region
    Sub VerifyControlLoaded()
End Interface



Public Class SaveAndSearchEventArgs
    Inherits EventArgs
    Public MatcingCriteria As MatcingCriteriaReturn


End Class
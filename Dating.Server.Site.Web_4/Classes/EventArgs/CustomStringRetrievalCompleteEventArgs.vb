﻿
Public Class CustomStringRetrievalCompleteEventArgs
    Inherits System.EventArgs

    Public Property CustomStringChanged As Boolean

    Private __CustomString As String
    Public Property CustomString As String
        Get
            Return __CustomString
        End Get
        Set(value As String)
            If (value <> __CustomString) Then
                CustomStringChanged = True
            End If
            __CustomString = value
        End Set
    End Property

    Public Property LagIDparam As String

    Public Sub New(data As String, lag As String)
        MyBase.New()
        __CustomString = data
        LagIDparam = lag
    End Sub

End Class

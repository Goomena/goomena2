﻿Imports System.Collections.Concurrent
Imports System.Collections.Generic
Imports System.Threading
Imports Microsoft.AspNet.SignalR.Hubs
Imports Microsoft.AspNet.SignalR
Imports Dating.Server.Core.DLL
Imports Dating.Server.Datasets.DLL


Public Class clsSignalrBroadcaster
    ' Singleton instance
    Private Shared ReadOnly _instance As New Lazy(Of clsSignalrBroadcaster)(Function() As clsSignalrBroadcaster
                                                                                Return New clsSignalrBroadcaster(GlobalHost.ConnectionManager.GetHubContext(Of ChatHub)().Clients)
                                                                            End Function)


    Private Sub New(clients__1 As IHubConnectionContext(Of Object))
        Clients = clients__1
    End Sub

    Public Shared ReadOnly Property Instance() As clsSignalrBroadcaster
        Get
            Return _instance.Value
        End Get
    End Property

    Private Property Clients() As IHubConnectionContext(Of Object)
        Get
            Return m_Clients
        End Get
        Set(value As IHubConnectionContext(Of Object))
            m_Clients = value
        End Set
    End Property
    Private m_Clients As IHubConnectionContext(Of Object)

    Shared ReadOnly favorite As String = <html><![CDATA[
        <div Id=####Id#### class="Message ####NotificationType####">
                <div class="####MessageType####"></div>
                <div class="ProfileImg">
                    <a class="InnerProfileImg" onclick="RemoveFromNotifications(####Id####);ShowLoading();" href="###ProfileUrl###"> 
                            <div class="RoundImage175" style="background: url('###ImageUrl###') no-repeat 50% 50%;background-size:cover;"></div>
                     </a>
                    <canvas class="Heart DrawCanvas" width=53 height=43 value="###PerCs###">

                    </canvas>
                    <div class="ProfileDetails">
                        <a class="ProfileName" onclick="RemoveFromNotifications(####Id####);ShowLoading();" href="###ProfileUrl###">
                           ###ProfileName###
                        </a>
                        <div class="ProfileAge">###ProfileAge###</div>
                    </div>
                </div>
                    <a class="CloseButton" onclick="RemoveFromNotifications(####Id####);" href="#"> 

                     </a>
            </div>
    ]]></html>.Value


    Public Sub BroadCastFavorite(ByVal FromProfileId As Integer, ByVal ToProfileId As Integer, ByVal Country As String)

        Dim _ds As New dsConnectedUsers
        clsSignalrHelper.GetSignalRConnectionStringsByProfileId(_ds, FromProfileId, ToProfileId, Country)

        If _ds.Eus_ConnectionStrings.Count > 0 Then
            '   Dim r As dsConnectedUsers.Eus_CustomerSignalrReceivedNotificationsRow =
            Dim Id As Long = clsSignalrHelper.AddNotificationInDatabase(FromProfileId, ToProfileId, SignarNotificationType.Favorite, "", -1)
            For Each r As dsConnectedUsers.Eus_ConnectionStringsRow In _ds.Eus_ConnectionStrings
                Clients.User(r.LoginName).appendNewMessage(clsEncryptDecryptDes.encrypt(Id))
            Next
        End If




    End Sub


End Class


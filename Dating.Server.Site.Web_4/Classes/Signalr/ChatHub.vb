﻿
Imports System.Web
Imports Microsoft.AspNet.SignalR.Hubs
Imports Microsoft.AspNet.SignalR
Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Core.DLL
Imports System.Threading.Tasks
<HubName("ChatHub")> _
Public Class ChatHub
    Inherits Hub

#Region "PublicPart"
    Public Async Function BroadcastMessage(message As String) As Task(Of Boolean)
        ' Call the broadcastMessage method to update clients.
        Return Await SendMessage(message)
    End Function

    Public Overrides Function OnConnected() As Task
        Try
            Return Joined()
        Catch ex As Exception

        End Try
        Return MyBase.OnConnected()
    End Function
    Public Overrides Function OnReconnected() As Task
        Try
            Return Joined()
        Catch ex As Exception

        End Try
        Return MyBase.OnReconnected()
    End Function
    Public Overrides Function OnDisconnected(stopCalled As Boolean) As Task
        Try
            Return DisconnectMe()
        Catch ex As Exception
        End Try
        Return MyBase.OnDisconnected(stopCalled)
    End Function
#End Region

#Region "PrivatePart"
    Private Function SendMessage(_message As String) As Task(Of Boolean)
        Return Task.Run(Function() As Boolean
                            Dim re As Boolean = False
                            Try
                                If (Context Is Nothing OrElse Context.User Is Nothing) Then Return False
                                Dim LoginName As String = Context.User.Identity.Name
                                Dim id As System.Web.Security.FormsIdentity = Context.User.Identity
                                Dim ticket As System.Web.Security.FormsAuthenticationTicket = id.Ticket
                                Dim result As New clsDataRecordLoginMemberReturn
                                result = clsSignalRLogin.PerfomRelogin(LoginName)
                                If result.HasErrors = False And result.IsValid Then
                                    Dim _ds As New dsConnectedUsers
                                    clsSignalrHelper.GetSignalRConnectionStrings(_ds, result.ProfileID, result.GenderId, result.PrivacySettings_HideMeFromSearchResults, result.Country)
                                    If _ds.Eus_ConnectionStrings.Count > 0 Then
                                        For Each r As dsConnectedUsers.Eus_ConnectionStringsRow In _ds.Eus_ConnectionStrings
                                            Clients.User(r.LoginName).appendNewMessage(_message)
                                        Next
                                    End If
                                    re = True
                                End If
                            Catch ex As Exception

                            End Try
                            Return re
                        End Function
            )
    End Function
    Private Function Joined() As Task(Of Boolean)
        'Id = Context.ConnectionId,        
        Return Task.Run(Function() As Boolean
                            Dim re As Boolean = False
                            Try
                                If (Context Is Nothing OrElse Context.User Is Nothing) Then Return False
                                Dim LoginName As String = Context.User.Identity.Name
                                Dim id As System.Web.Security.FormsIdentity = Context.User.Identity
                                Dim ticket As System.Web.Security.FormsAuthenticationTicket = id.Ticket
                                Dim result As New clsDataRecordLoginMemberReturn
                                result = clsSignalRLogin.PerfomRelogin(LoginName)
                                If result.HasErrors = False And result.IsValid Then
                                    clsSignalrHelper.AddUser(result.ProfileID, result.MirrorProfileID, result.LoginName, result.GenderId, Process.GetCurrentProcess().Id, Context.ConnectionId)
                                End If
                                re = True
                            Catch ex As Exception

                            End Try
                            Return re
                        End Function)

      
    End Function



    Private Function DisconnectMe() As Task(Of Boolean)
        Return Task.Run(Function() As Boolean
                            Dim re As Boolean = False
                            Try
                                clsSignalrHelper.DeleteUser(Context.ConnectionId, Process.GetCurrentProcess().Id)
                                re = True
                            Catch ex As Exception
                            End Try
                            Return re
                        End Function
          )
    End Function

#End Region











End Class

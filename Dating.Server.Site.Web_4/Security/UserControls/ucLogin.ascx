﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucLogin.ascx.vb" Inherits="Dating.Server.Site.Web.ucLogin" %>


<asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">

    <asp:View ID="vwStandard" runat="server">
        <asp:Login ID="MainLogin" runat="server">
        </asp:Login>
        <div style="margin:10px;"><asp:Label ID="lblError" runat="server" Text="" ForeColor="red" Visible="false" EnableViewState="false" CssClass="login_error"></asp:Label></div>
    </asp:View>
    <asp:View ID="vwLoggedInUser" runat="server">
        <div class="pub-top-welcome">
            <asp:HyperLink ID="lblLoginName" runat="server" NavigateUrl="~/Members/default.aspx"
                ForeColor="Black">
                <asp:LoginName ID="LN" runat="server" FormatString="Welcome, {0}" Font-Size="12pt"
                    Font-Bold="true" ForeColor="Black" />
            </asp:HyperLink>&nbsp;&nbsp;&nbsp;&nbsp;<span style="">
                <dx:ASPxLabel ID="lblLoginDate" runat="server" Text="Login: ###LOGINDATE###" EncodeHtml="False"
                    ForeColor="Black">
                </dx:ASPxLabel></span>
            <div style="height: 8px;">&nbsp;</div>
        </div>

<div style="position:absolute;right:0px;bottom:45px;">
    <dx:ASPxButton ID="lblMembersArea" runat="server" Text="Members&nbsp;Area"
                                            EncodeHtml="False" Height="32px" Font-Bold="True" 
                                            PostBackUrl="~/Members/default.aspx" 
            UseSubmitBehavior="false" BackColor="#26A9E1" Cursor="pointer" 
                EnableDefaultAppearance="False" Font-Size="13px" ForeColor="White" 
                Width="150px">
                                            <Border BorderWidth="1px"></Border>
                                            <ClientSideEvents Click="ShowLoadingOnMenu" />
                                        </dx:ASPxButton>
</div>
<div class="clear"></div>

        
    </asp:View>


</asp:MultiView>




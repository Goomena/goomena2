﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Root.Master" CodeBehind="Landing.aspx.vb" Inherits="Dating.Server.Site.Web.Landing" %>
<%@ Register src="UserControls/ucLandingProfiles.ascx" tagname="ucLandingProfiles" tagprefix="uc1" %>
<%@ Register src="UserControls/ucLandingProfiles2014.ascx" tagname="ucLandingProfiles2014" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<style type="text/css">
    .landing-content a, .landing-content a h1,.landing-content a:visited,.landing-content a:visited h1 {color:#24A9E1;text-decoration:none;}
    .landing-content a:hover,.landing-content a:hover h1{color: #007ada;}
    .pe_box a,
    .pe_box a:visited{color:#000;text-decoration:none;}
    .pe_box a:hover{color:#00aeef;}
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="insidemosaiccontainer" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="content" runat="server">
    <div class="landingcontainer">
        <asp:Label ID="lblHeader" runat="server" Text="" CssClass="landing-content" EnableViewState="false"></asp:Label>
    </div>
    <uc1:ucLandingProfiles ID="ucLP1" runat="server" EnableViewState="false" />
    <uc1:ucLandingProfiles2014 ID="ucLP2014" runat="server" Visible="false" EnableViewState="false" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cntBodyBottom" runat="server">
</asp:Content>

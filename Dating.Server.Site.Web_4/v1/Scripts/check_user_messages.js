
var checkUserMessagesTimer;

function checkUserMessages_poll() {
    if (checkUserMessagesTimer == null) {
        checkUserMessagesTimer = setTimeout(req_checkUserMessages, 10000);
    }
    else {
        clearTimeout(checkUserMessagesTimer);
        checkUserMessagesTimer = setTimeout(req_checkUserMessages, 30000);
    }
}


function req_checkUserMessages() {
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "../Members/json.aspx/UserMessages_Check",
        //data: '{ "data": "data" }',
        dataType: "json",
        success: function (msg) { req_success_checkUserMessages(msg) }
    });

}


function req_success_checkUserMessages(msg) {
    var data = "";
    eval("data=" + msg.d + "");

    if (typeof openPopupCheckMessage !== undefined && data != null && data.length > 1) {
        var QueueID = data[0].QueueID
        openPopupCheckMessageByID(QueueID);
    }
    checkUserMessages_poll();
}


jQuery(function ($) {
    checkUserMessages_poll();
});



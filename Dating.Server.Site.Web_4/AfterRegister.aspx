﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Root.Master" CodeBehind="AfterRegister.aspx.vb" Inherits="Dating.Server.Site.Web.AfterRegister" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   <%--<meta http-equiv="refresh" content="4; url=<%= ResolveUrl("~/Members/Photos.aspx") %>"/>--%>
    <style type="text/css">
        .afterregister {margin-top: 100px;}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <div class="mid_bg pageContainer">
        <div class="hp_cr" id="hp_top">
        </div>
        <div id="hp_content">
            <div class="container_12">
                <div class="cms_page">
                    <div class="cms_content">
                        <div class="ab_block">
                            <div class="pe_wrapper">
                                <div class="pe_box">
                                    <dx:ASPxLabel ID="lbHTMLBody" runat="server" ClientIDMode="AutoID" EncodeHtml="False">
                                    </dx:ASPxLabel>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="pageContainer" style="margin-top: 20px; display: none;">
        <div>
        </div>
    </div>

    <script type="text/javascript">
        setTimeout(function () {
            jsLoad('<%= ResolveUrl("~/Members/Photos.aspx") %>');
        }, 4000);
    </script>
</asp:Content>

﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="LoginWithGoogleCB.aspx.vb" Inherits="Dating.Server.Site.Web.LoginWithGoogleCB" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
     <script type="text/javascript">
         try {
             // First, parse the query string
             var params = {}, queryString = location.hash.substring(1), regex = /([^&=]+)=([^&]*)/g, m;
             while (m = regex.exec(queryString)) {
                 params[decodeURIComponent(m[1])] = decodeURIComponent(m[2]);
             }
             var ss = queryString.split("&")
             window.location = "LoginWithGoogle.aspx?" + ss[1];
         } catch (exp) {
             window.location = "Register.aspx?ref=google&result=cancel";
             //alert(exp.message + " here 1");
         }
        </script>
    </div>
    </form>
</body>
</html>

﻿Imports Library.Public

Public Class CmsPage
    Inherits LandingBasePage


#Region "Props"


    'Dim _pageData As clsPageData
    'Protected ReadOnly Property CurrentPageData As clsPageData
    '    Get
    '        If (_pageData Is Nothing) Then _pageData = New clsPageData(Context, Me.Page)
    '        Return _pageData
    '    End Get
    'End Property

    Protected Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData(Context, Me.Page, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property


    Protected Property PageContentID As String
#End Region

    Private Sub Page_Disposed1(sender As Object, e As EventArgs) Handles Me.Disposed
        Try
            If Me._pageData IsNot Nothing Then
                RemoveHandler Me._pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
        Catch ex As Exception
        End Try
        Try
            MyBase.Dispose()
        Catch ex As Exception
        End Try
    End Sub


    Protected Overrides Sub Page_PreInit(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreInit
        Try

            CheckMasterPageAndRedirect(CurrentPageData)
            SetMasterPage(CurrentPageData)


            If (Not String.IsNullOrEmpty(Request.QueryString("popup"))) Then
                Me.MasterPageFile = "~/RootInner.Master"
            End If

            MyBase.Page_PreInit(sender, e)
            'If clsCurrentContext.VerifyLogin() = True Then
            '    Me.MasterPageFile = "~/Members/Members2.Master"
            'End If
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try
    End Sub

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Try
            ' if lag changed, clear page's cache
            Me.VerifyLagProps()
            If (_pageData IsNot Nothing AndAlso _pageData.LagID <> GetLag()) Then
                _pageData = Nothing
            End If

            If (Me.CurrentPageData.SitePageID = 276) Then
                ' /cmspage.aspx?pageid=276&title=Gifts.aspx
                ' einai programma antamoibis, prosti8etai contact form

                'Dim cc As New ContactControl()
                'Dim cc As ContactControl = Page.LoadControl(GetType(ContactControl), {})
                Dim ccWrap As New HtmlGenericControl("div")
                ccWrap.ID = "divContactWrap"
                ccWrap.Attributes.Add("class", "cc-wrapper")

                Dim ccWrapInner As New HtmlGenericControl("div")
                ccWrapInner.ID = "divContactWrapInner"
                ccWrapInner.Attributes.Add("class", "inner-box p276")

                Dim p276 As New HtmlGenericControl("div")
                p276.ID = "p276"
                p276.Attributes.Add("class", "p276")

                Dim cc As ContactControl = Page.LoadControl("~/UserControls/ContactControl.ascx")
                cc.ID = "ctlContact"
                cc.EmailSubject = Me.CurrentPageData.GetCustomString("Contact.Form.Email.Subject")


                p276.Controls.Add(cc)
                ccWrapInner.Controls.Add(p276)
                ccWrap.Controls.Add(ccWrapInner)

                phContact.Controls.Add(ccWrap)

                PageContentID = "pc276"
                'divContent.
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'If (Not Me.IsPostBack OrElse MyBase.IsLagChanged()) Then
            LoadLAG()
            'End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try
    End Sub


    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender

        Try
            Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "PreRender->SEO data")
        End Try


        Try
            If (CurrentPageData.IsQueryStringParamDetected) Then
                ' create canonical link only when pageid param is present

                Dim canonicalHref As String = "/cmspage/" & CurrentPageData.SitePageID
                MyBase.CreateCanonicalLink(canonicalHref)

            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "PreRender->canonical link")
        End Try
    End Sub



    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
    End Sub


    Protected Sub LoadLAG()
        Try
            Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = Me.CurrentPageData.cPageBasic
            lblHeader.Text = Me.CurrentPageData.GetCustomString(lblHeader.ID)
            lbHTMLBody.Text = Me.FixHTMLBodyAnchors(cPageBasic.BodyHTM)
            lblinside.Text = Me.CurrentPageData.GetCustomString("lblinside")

            Dim _popupTrigger1_ As String = Me.CurrentPageData.GetCustomString("_popupTrigger1_")
            If (Not String.IsNullOrEmpty(_popupTrigger1_) AndAlso lbHTMLBody.Text.Contains("_popupTrigger1_")) Then
                Dim win As New DevExpress.Web.ASPxPopupControl.PopupWindow(_popupTrigger1_)
                win.PopupElementID = "_popupTrigger1_"
                popupWindows.Windows.Add(win)
            End If


            'AppUtils.setSEOPageData(Me, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
            'AppUtils.setNaviLabel(Me, "lbPublicNavi2Page", cPageBasic.PageTitle)
            If (TypeOf Page.Master Is INavigation) Then
                CType(Page.Master, INavigation).AddNaviLink(Me.CurrentPageData.Title, "")
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

End Class
﻿Imports Dating.Server.Core.DLL

Public Class AdminHandler
    Inherits System.Web.UI.Page

    Public Shared ReadOnly Property GetHash As Integer
        Get
            Return DateTime.UtcNow.Date.ToString().GetHashCode()
        End Get
    End Property


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'If (Request.QueryString("code") = GetHash().ToString()) Then
        If (Request.QueryString("source") = "@goomena.com") Then

            If (Not String.IsNullOrEmpty(Request.QueryString("makefirstpage"))) Then
                Dim profileid As Integer
                Integer.TryParse(Request.QueryString("makefirstpage"), profileid)
                ShowProfileOnFirstPage(profileid)
            Else

            End If

        End If
    End Sub

    Private Sub ShowProfileOnFirstPage(profileId As Integer)
        If (profileId > 1) Then
            Try
                DataHelpers.UpdateEUS_Profiles_ShowOnFirstPage(profileId, True)
                Response.Write("User with profileID  " & profileId & " set succesfully on first page<br>")
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "ShowProfileOnFirstPage")
            End Try
        Else
            Response.Write("Invalid argument " & profileId & "<br>")
        End If
    End Sub
End Class
﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Root2.Master"
    CodeBehind="Login2.aspx.vb" Inherits="Dating.Server.Site.Web.Login2" %>
<%@ Register Src="Security/UserControls/ucLogin.ascx" TagName="ucLogin" TagPrefix="uc1" %>
<%@ Register TagPrefix="recaptcha" Namespace="Recaptcha" Assembly="Recaptcha" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .pe_box {background: white;}
        .form-actions {background: white;}
        .lists {width: 440px;}
        .lists ul {list-style:none;padding-left:0px;margin-left:0px;}
        .lists ul li {list-style: none;}
    </style>
    <script type="text/javascript">
        function showWaterMark5(s, e) {
            if (stringIsEmpty($.trim(s.GetValue()))) {
                $('.text-watermark:hidden', '.login-input-wrap').show();
            }
        }
        function hideWaterMark5(s, e) {
            $('.text-watermark', '.login-input-wrap').hide();
        }
        function checkWaterMark5(s, e) {
            if (stringIsEmpty($.trim(s.GetValue()))) {
                $('.text-watermark:hidden', '.login-input-wrap').show();
            }
            else {
                $('.text-watermark', '.login-input-wrap').hide();
            }
        }
        function validateFields(err) {
            var result = true;
            try{
                var passFail = false,userFail= false;
                if (jsPassword) passFail = stringIsEmpty($.trim(jsPassword.GetValue()));
                if (jsUserName) userFail = stringIsEmpty($.trim(jsUserName.GetValue()));
                if (err) { userFail = passFail = true; }

                if (userFail) {
                    $('.jsUserName-error', '.login-input-wrap').show();
                    $('.jsUserName-ok', '.login-input-wrap').hide();
                }
                else {
                    $('.jsUserName-error', '.login-input-wrap').hide();
                    $('.jsUserName-ok', '.login-input-wrap').show();
                }
                if (passFail) {
                    $('.jsPassword-error', '.login-input-wrap').show();
                    $('.jsPassword-ok', '.login-input-wrap').hide();
                }
                else {
                    $('.jsPassword-error', '.login-input-wrap').hide();
                    $('.jsPassword-ok', '.login-input-wrap').show();
                }
                result = !userFail && !passFail;
            }
            catch (e) { }
            return result;
        }

        function validateFieldsReload() {
            setTimeout(function () { validateFields(1) }, 300);
        }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">

<div id="login-sections">
<div id="all_sections">
    <div class="section-1-ext">
        <div class="section-1">
            <div class="regmidcontainer" id="regmidcontainer" runat="server">
                <div class="reg-box">
                    <div class="registerform" id="registerform" runat="server">
                        <div class="mainregdiv">
                            <div class="notmember">
                                <h1 class="login-title"><asp:Label ID="lblHeader" runat="server"/></h1>
                                <asp:Label ID="lblHeaderText" runat="server" CssClass="select-item"></asp:Label>
                            </div>
                            <uc1:ucLogin ID="ucLogin1" runat="server">
                                <LoginControl UserNameRequiredErrorMessage="Username or email is required." Width="100%">
                                    <LayoutTemplate>
<asp:Panel ID="Panel1" runat="server" class="login_box" DefaultButton="btnLoginNow">
    <dx:ASPxValidationSummary ID="valSumm" runat="server" 
        ValidationGroup="LoginGroup" RenderMode="BulletedList" 
        ShowErrorsInEditors="True">
    <Paddings PaddingBottom="10px" />
    <LinkStyle>
        <Font Size="14px"></Font>
    </LinkStyle>
    </dx:ASPxValidationSummary>

    <div style="margin:10px;text-align:center;"><dx:ASPxLabel ID="lblError" runat="server" Text="" 
        ForeColor="red" Visible="false" EnableViewState="false" EncodeHtml="False">
    </dx:ASPxLabel></div>

    <div class="login-input-wrap">
        <dx:ASPxTextBox ID="UserName" runat="server" ClientInstanceName="jsUserName">
            <ValidationSettings ValidationGroup="LoginGroup"  ErrorDisplayMode="ImageWithTooltip" ErrorTextPosition="Left" ErrorText="" Display="Dynamic">
                <RequiredField ErrorText="Username or email is required." IsRequired="True" />
            </ValidationSettings>
            <Border BorderStyle="None" />
            <BackgroundImage HorizontalPosition="0px" ImageUrl="//cdn.goomena.com/images2/pub2/username-icon.png" Repeat="NoRepeat" VerticalPosition="50%" />
            <NullTextStyle CssClass="null-text"></NullTextStyle>
        </dx:ASPxTextBox>
        <img src="//cdn.goomena.com/images2/pub2/check.png" alt="check ok" width="18" height="18" class="check jsUserName-ok hidden"/>
        <img src="//cdn.goomena.com/images2/pub2/error.png" alt="check error" width="18" height="18" class="check jsUserName-error hidden"/>
    </div>
    <div style="height:5px;width:100%;"></div>
    <div class="login-input-wrap">
        <dx:ASPxTextBox ID="Password" runat="server" Password="True" ClientInstanceName="jsPassword">
            <ValidationSettings ValidationGroup="LoginGroup" ErrorDisplayMode="ImageWithTooltip" ErrorTextPosition="Left" ErrorText="" Display="Dynamic">
                <RequiredField ErrorText="Password is required." IsRequired="True" />
            </ValidationSettings>
            <Border BorderStyle="None" />
            <BackgroundImage HorizontalPosition="0px" ImageUrl="//cdn.goomena.com/images2/pub2/password-icon.png" Repeat="NoRepeat" VerticalPosition="50%" />
            <ClientSideEvents LostFocus="showWaterMark5" GotFocus="hideWaterMark5" />
        </dx:ASPxTextBox>
        <div class="text-watermark"><asp:Label ID="lblPassword" runat="server" Text="Password"/></div>
        <img src="//cdn.goomena.com/images2/pub2/check.png" alt="check ok" width="18" height="18" class="check jsPassword-ok hidden"/>
        <img src="//cdn.goomena.com/images2/pub2/error.png" alt="check error" width="18" height="18" class="check jsPassword-error hidden"/>
    </div>

    <div class="check-wrap">
        <dx:ASPxCheckBox ID="chkRememberMe" runat="server" Text="" EncodeHtml="False">
            <CheckedImage Url="//cdn.goomena.com/images2/search/check.png" Height="19px"
                Width="18px">
            </CheckedImage>
            <UncheckedImage Url="//cdn.goomena.com/images2/search/uncheck.png" Height="19px"
                Width="18px">
            </UncheckedImage>
        </dx:ASPxCheckBox>
    </div>
    <div class="clear"></div>
    <br />
    <div style="margin: 15px auto;width: 318px;">
        <div style="text-align:center;margin-bottom:4px;">
            <asp:Label ID="lblFillRecaptcha" runat="server" Text=""  style="font-size: 14px;"/>
        </div>
        <div runat="server" id="pbTarget" visible="false"></div>  
        <asp:PlaceHolder ID="phRecaptcha" runat="server"></asp:PlaceHolder>
        <%--
        --- created from code ----
                                    
        <recaptcha:RecaptchaControl
            ID="recaptcha"
            runat="server"
            PublicKey="6LeqLOsSAAAAACbrctSmamfQ20Jc8BJrBynH-jKt"
            PrivateKey="6LeqLOsSAAAAAM4HuHzL6W4zmNtKYCOn1iS0AwK7"
            />--%>
    </div>
    <div class="form-actions">
        <asp:Button ID="btnLoginNow" runat="server" Text="Login now" 
            CausesValidation="True" CommandName="Login" ValidationGroup="LoginGroup" OnClientClick="return validateFields();" />
    </div>
</asp:Panel>
                                    </LayoutTemplate>
                                </LoginControl>
                            </uc1:ucLogin>
                            <div class="clear"></div>
                        </div>
                        <div class="restore-wrap">
                            <asp:HyperLink ID="lnkRestore" runat="server" NavigateUrl="~/RestorePassword.aspx" onclick="ShowLoading();">Restore Password</asp:HyperLink>
                        </div>
                        <div class="reg-form-footer">
                            <asp:Label ID="lblRegFooter" runat="server" CssClass="footer-text"></asp:Label>
                            <asp:HyperLink ID="lnkCreateAccount" runat="server" NavigateUrl="~/Register2.aspx" onclick="ShowLoading();">Create an account</asp:HyperLink>
                        </div>
                    </div>
                    <div class="login-buttons">
                        <div class="login-with-wrap font-semibold">
                            <asp:Label ID="lblLoginWith" runat="server" CssClass="login-with">Login with</asp:Label>
                            <%--<canvas id="cnvLoginWith" width="auto" height="30">
                                </canvas>--%>
                        </div>
                        <%--<script type="text/javascript">
                            function drawLoginWith() {
                                try{
                                    var html=$('.login-with').html();
                                    var canvas = $('#cnvLoginWith')[0];
                                    c = canvas.getContext('2d');

                                    //c.class = 'login-with';
                                    //c.font = "15px Arial,sans-serif";
                                    c.textAlign = "center";
                                    c.fillText(html, 0, 0);
                                }
                                catch (e) { }
                            }
                            drawLoginWith();
                        </script>--%>

                        <asp:HyperLink ID="lblFB" runat="server" Text="Login With Facebook"
                            NavigateUrl="~/Register2.aspx?log=fb" onclick="ShowLoading();" CssClass="main-fb-logindiv">
                            <img src="//cdn.goomena.com/images/spacer10.png"  alt="facebook" width="24" height="47"/>
                        </asp:HyperLink>
                        <asp:HyperLink ID="lblGP" runat="server" Text="Login With Google"
                            NavigateUrl="~/Register2.aspx?log=google" onclick="ShowLoading();" CssClass="main-gp-logindiv">
                            <img src="//cdn.goomena.com/images/spacer10.png" alt="g+" width="25" height="39"/>
                        </asp:HyperLink>
                    </div>
                    <h1 class="reg-wellcome"><asp:Literal ID="lblWellcomeMsg" runat="server"></asp:Literal></h1>
                </div>
            </div>
            <div class="gendermsg"></div> 
            <div id="top-pic">
            </div>
        </div>
    </div> 
    <div class="section-1">
        <div class="members-count">
            <asp:Literal ID="lblMembersCount" runat="server"></asp:Literal>
        </div>
    </div> 
    <div class="section-1-ext">
        <div class="section-1">
            <div class="members-photos">
                <asp:Panel ID="pnlMosaic" CssClass="mosaic" runat="server" EnableViewState="false">
		            <div runat="server" class="pic" id="pic1"></div>
		            <div runat="server" class="pic" id="pic2"></div>
		            <div runat="server" class="pic" id="pic3"></div>
		            <div runat="server" class="pic" id="pic4"></div>
		            <div runat="server" class="pic" id="pic5"></div>
		            <div runat="server" class="pic" id="pic6"></div>
		            <div runat="server" class="pic" id="pic7"></div>
		            <div runat="server" class="pic" id="pic8"></div>
		            <div runat="server" class="pic" id="pic9"></div>
		            <div runat="server" class="pic" id="pic10"></div>
		            <div runat="server" class="pic" id="pic11"></div>
		            <div runat="server" class="pic" id="pic12"></div>
		            <div runat="server" class="pic" id="pic13"></div>
		            <div runat="server" class="pic" id="pic14"></div>
		            <div runat="server" class="pic" id="pic15"></div>
		            <div runat="server" class="pic" id="pic16"></div>
		            <div runat="server" class="pic" id="pic17"></div>
		            <div runat="server" class="pic" id="pic18"></div>
		            <div runat="server" class="pic" id="pic19"></div>
                    <div class="clear"></div>
                 </asp:Panel> 
            </div>
        </div>
    </div> 
</div> 
</div>

<div class="PhotoInfoWrap hidden">
    <div id="PhotoInfos" class="PhotoInfos">
        <img class="InfoImg" alt="" src="" />
    </div>
    <p class="ProfileID"></p>
    <p class="ProfileCity"></p>
    <div class="ProfileAgeWrap">
        <p class="ProfileAgeText"></p>
        <p class="ProfileAge"></p>
    </div>
</div>
                       
<script type="text/javascript">
    var profileBaseUrl = '<%= MyBase.LanguageHelper.GetPublicURL("/profile/", MyBase.GetLag()) %>';

    if (!stringIsEmpty(profileBaseUrl)) {
        if (profileBaseUrl.indexOf("?") > -1) profileBaseUrl = profileBaseUrl.substring(0, profileBaseUrl.indexOf("?"));
    }

    function checkBorders(e) {
        var widthCheck = 2004 + 20;
        var itmsExt = ['.section-1-ext'];
        var itms = ['.section-1'];
        for (var i = 0; i < itmsExt.length; i++) {
            var itmExt = $(itmsExt[i]);
            var itm = $(itms[i], itmsExt[i]);
            var w = itmExt.width();
            if (w >= widthCheck && !itm.is('.ext-borders')) {
                itm.addClass('ext-borders');
            }
            else if (w < widthCheck && itm.is('.ext-borders')) {
                itm.removeClass('ext-borders');
            }
        }
    }

    $(checkBorders);
    $(window).on('resize', checkBorders);


    function setWaterMarkHandlers() {
        try {
            $(".text-watermark", ".login-input-wrap").click(function () { jsPassword.SetFocus(); });
        }
        catch (e) { }
        try {
            showWaterMark5(window["jsPassword"], null);
        }
        catch (e) { }
    }
    setWaterMarkHandlers();
</script>  
<script type="text/javascript" src="/v1/Scripts/mosaic.js"></script>  
                         
</asp:Content>

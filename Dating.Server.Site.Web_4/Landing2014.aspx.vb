﻿Imports System.Data.SqlClient
Imports Dating.Server.Core.DLL

Public Class Landing2014
    Inherits LandingBasePage

    Protected Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData(Context, Me.Page, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property
    Private Sub Page_Disposed1(sender As Object, e As EventArgs) Handles Me.Disposed
        Try
            If Me._pageData IsNot Nothing Then
                RemoveHandler Me._pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
        Catch ex As Exception
        End Try
        Try
            MyBase.Dispose()
        Catch ex As Exception
        End Try
    End Sub

    Protected Overrides Sub Page_PreInit(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreInit
        Try
            CheckMasterPageAndRedirect(CurrentPageData)
            SetMasterPage(CurrentPageData)

        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "PreInit")
        End Try
    End Sub


    Private Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init

    End Sub


    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Dim cPageBasic As clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try


        Try
            LoadPhotos()

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "page_prerender->LoadPhotos")
        End Try


        Try
            If (CurrentPageData.IsQueryStringParamDetected) Then
                ' create canonical link only when pageid param is present

                Dim canonicalHref As String = "/landing/y2014/" & CurrentPageData.SitePageID
                MyBase.CreateCanonicalLink(canonicalHref)

            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "PreRender->canonical link")
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
           
            LoadLAG()

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub

    Protected Sub LoadLAG()
        Try
            '  Dim cPageBasic As clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            lblHeader.Text = CurrentPageData.GetCustomString("lblHeader")
            lblBody.Text = CurrentPageData.GetCustomString("lblBody")
            lblTopImage.Text = CurrentPageData.GetCustomString("lblTopImage")
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub



    Private Sub LoadPhotos()

        Dim photoid As String
        Dim photoname As String
        Dim address As String = Me.Request.Url.Scheme & "://photos.goomena.com/"
        Dim address2 As String
        Dim address3 As String
        Dim i As Integer
        Dim picture As New HtmlGenericControl
        '  Dim imageforwomen As New HtmlImage
        Dim profilename As String
        Dim checkList As New List(Of Integer)
        '    Dim pnlBottomPhotos1 As Control = Master.FindControl("Content")
        '    Dim topControlsCount As Integer = pnlMosaic.Controls.Count


        Dim latitudeIn As Double? = Nothing
        Dim longitudeIn As Double? = Nothing

        Try

            'If (Session("GEO_COUNTRY_INFO") IsNot Nothing) Then
            '    Dim country As clsCountryByIP = Session("GEO_COUNTRY_INFO")
            Dim country As clsCountryByIP = clsCurrentContext.GetCountryByIP()
            If (country IsNot Nothing) Then

                Try
                    Dim pos As New clsGlobalPositionWCF(country.latitude, country.longitude)
                    latitudeIn = pos.latitude
                    longitudeIn = pos.longitude

                    'If (Not String.IsNullOrEmpty(country.latitude)) Then
                    '    Dim __latitudeIn As Double = 0
                    '    'country.latitude = country.latitude.Replace(".", ",")
                    '    If (Double.TryParse(country.latitude, __latitudeIn)) Then latitudeIn = __latitudeIn
                    '    If (__latitudeIn > 180 OrElse __latitudeIn < -180) Then
                    '        country.latitude = country.latitude.Replace(".", ",")
                    '        If (Double.TryParse(country.latitude, __latitudeIn)) Then latitudeIn = __latitudeIn
                    '    End If
                    'End If
                    'If (Not String.IsNullOrEmpty(country.longitude)) Then
                    '    Dim __longitudeIn As Double = 0
                    '    'country.longitude = country.longitude.Replace(".", ",")
                    '    If (Double.TryParse(country.longitude, __longitudeIn)) Then longitudeIn = __longitudeIn
                    '    If (__longitudeIn > 180 OrElse __longitudeIn < -180) Then
                    '        country.longitude = country.longitude.Replace(".", ",")
                    '        If (Double.TryParse(country.longitude, __longitudeIn)) Then longitudeIn = __longitudeIn
                    '    End If
                    'End If
                Catch ex As Exception
                    WebErrorSendEmail(ex, "Get lat-lon from ip.")
                End Try

            End If
            'End If


            If (Not latitudeIn.HasValue AndAlso Not longitudeIn.HasValue) Then
                Try

                    If (MasterProfileId > 0 AndAlso clsCurrentContext.VerifyLogin()) Then
                        If (SessionVariables.MemberData.latitude.HasValue) Then latitudeIn = SessionVariables.MemberData.latitude
                        If (SessionVariables.MemberData.longitude.HasValue) Then longitudeIn = SessionVariables.MemberData.longitude
                    End If
                Catch ex As Exception
                    WebErrorSendEmail(ex, "Get lat-lon from profile.")
                End Try
            End If

            If (Not latitudeIn.HasValue AndAlso Not longitudeIn.HasValue) Then
                Try

                    Dim pos As clsGlobalPositionWCF = clsGeoHelper.GetCountryMinPostcodeDataTable_Position(BasePage.GetGeoCountry(), Nothing, Nothing)
                    latitudeIn = pos.latitude
                    longitudeIn = pos.longitude

                    'Dim dtGEO As DataTable = clsGeoHelper.GetCountryMinPostcodeDataTable(MyBase.GetGeoCountry(), Nothing, Nothing)

                    'If (dtGEO.Rows.Count > 0 AndAlso Not IsDBNull(dtGEO.Rows(0)("latitude")) AndAlso Not IsDBNull(dtGEO.Rows(0)("longitude"))) Then
                    '    latitudeIn = clsNullable.DBNullToDecimal(dtGEO.Rows(0)("latitude"))
                    '    longitudeIn = clsNullable.DBNullToDecimal(dtGEO.Rows(0)("longitude"))
                    'End If

                Catch ex As Exception
                    WebErrorSendEmail(ex, "Get lat-lon from geo data.")
                End Try
            End If

        Catch ex As Exception
            WebErrorSendEmail(ex, "Get lat-lon info.")
        End Try
        Dim dt As DataTable
        Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


            Dim sql As String = "EXEC [CustomerPhotos_Grid3]  @latitudeIn=@latitudeIn, @longitudeIn=@longitudeIn,@Country=@Country,@showWomanOnly=1,@showManOnly=0"
            Using cmd As SqlCommand = DataHelpers.GetSqlCommand(con, sql)


                cmd.Parameters.AddWithValue("@Country", BasePage.GetGeoCountry())
                Dim prm1 As SqlClient.SqlParameter = DataHelpers.GetSqlParam_LatitubeOrLongitude("@latitudeIn", latitudeIn)
                cmd.Parameters.Add(prm1)

                Dim prm2 As SqlClient.SqlParameter = DataHelpers.GetSqlParam_LatitubeOrLongitude("@longitudeIn", longitudeIn)
                cmd.Parameters.Add(prm2)

                'If (Not latitudeIn.HasValue AndAlso Not longitudeIn.HasValue) Then
                '    cmd.Parameters.AddWithValue("@latitudeIn", DBNull.Value)
                '    cmd.Parameters.AddWithValue("@longitudeIn", DBNull.Value)
                'Else
                '    cmd.Parameters.AddWithValue("@latitudeIn", latitudeIn.Value)
                '    cmd.Parameters.AddWithValue("@longitudeIn", longitudeIn.Value)
                'End If

                dt = DataHelpers.GetDataTable(cmd)
            End Using
        End Using
        dt.Columns.Add("InUse", GetType(Boolean))

        Try

            Dim women As New List(Of Integer)
            Dim men As New List(Of Integer)

            For cnt = 0 To dt.Rows.Count - 1
                Dim dr As DataRow = dt.Rows(cnt)

                Dim CustomerID As Integer = dr("CustomerID")
                If (checkList.Contains(CustomerID)) Then
                    Continue For
                End If
                Dim gender As Integer = dr("GenderId")
                If (gender = 2) Then
                    If (women.Count > 59) Then
                        Continue For
                    Else
                        women.Add(cnt)
                    End If
                ElseIf (gender = 1) Then
                    If (men.Count > 27) Then
                        Continue For
                    Else
                        men.Add(cnt)
                    End If
                End If

                checkList.Add(CustomerID)


                'stis 88 photos exit
                If (checkList.Count > 87) Then Exit For

            Next


            Dim photos_panel As Control = pnlMosaic
            Dim picSuffix As String = ""
            Dim currentPhotosCount As Integer = 0
            i = 0
            For cnt = 0 To checkList.Count - 1
                If (currentPhotosCount > 87) Then Exit For
                Dim drs As DataRow() = dt.Select("CustomerId=" & checkList(cnt) & "and (InUse is null)")
                If (drs.Length = 0) Then Continue For

                i += 1
                picture = photos_panel.FindControl("pic" & i & picSuffix)
                While (i < 87 AndAlso (picture Is Nothing OrElse Not picture.Visible) AndAlso (currentPhotosCount < 38))
                    i += 1
                    picture = photos_panel.FindControl("pic" & i & picSuffix)
                End While
                If (picture Is Nothing) Then Exit For


                Dim dr As DataRow = drs(0)
                dr("InUse") = True
                currentPhotosCount = currentPhotosCount + 1

                photoid = dr("CustomerID").ToString
                photoname = dr("FileName")
                profilename = dr("LoginName")
                '   Dim country As String = dr("Country")
                Dim region As String = dr("Region")
                Dim birthday As Date = dr("Birthday")
                Dim city As String = dr("City")
                Dim age As Integer = ProfileHelper.GetCurrentAge(birthday)
                Dim gender As Integer = dr("GenderId")
                address2 = address + photoid
                address3 = address2 + "/thumbs/" + photoname
                If Me.IsHTTPS AndAlso address3.StartsWith("http:") Then address3 = "https" & address3.Remove(0, "http".Length)

                picture.Attributes.Add("style", "background-image:url(" + address3 + ")")
                picture.Attributes.Add("data-URL", address3)
                picture.Attributes.Add("data-ID", photoid)
                picture.Attributes.Add("data-LoginName", profilename)
                picture.Attributes.Add("data-Country", dr("CountryName")) 'ProfileHelper.GetCountryName(country))
                picture.Attributes.Add("data-Region", region)
                picture.Attributes.Add("data-Age", age)
                picture.Attributes.Add("data-City", city)
                picture.Attributes.Add("data-Gender", gender)

                If (cnt = checkList.Count - 1 AndAlso currentPhotosCount < 87) Then
                    cnt = 0
                End If
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
  
        End Try
        checkList.Clear()



    End Sub


End Class
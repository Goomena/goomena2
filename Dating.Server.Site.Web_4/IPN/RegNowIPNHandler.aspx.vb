﻿Imports System.IO
Imports System.Xml
Imports System.Data.SqlClient
Imports Dating.Server.Core.DLL


Public Class RegNowIPNHandler
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ReadRegNowResponse()
    End Sub



    Sub ReadRegNowResponse()
        Dim reader As StreamReader = New StreamReader(Page.Request.InputStream)
        Dim UniPayTransactionID As String = ""
        Dim readerContent As String = ""
        Try
            Dim siteIPN As Dating.Server.Site.Web.UniPAYIPN = New Dating.Server.Site.Web.UniPAYIPN()
            Dim ipnSender As New clsCore
            Dim buyer As New clsDataRecordBuyerInfo
            Dim ipnData As New clsDataRecordIPN

            readerContent = reader.ReadToEnd()

            Try
                WebErrorSendEmail(New System.Exception("TESTING ReadRegNowResponse"), "ReadRegNowResponse XML Content ---- " & vbCrLf & readerContent)
            Catch ex As Exception
            End Try

            Dim m_xmld As XmlDocument
            Dim m_node As XmlNode
            'Create the XML Document
            m_xmld = New XmlDocument()
            'Load the Xml file
            m_xmld.LoadXml(readerContent)
            'Get the list of name nodes 

            m_node = m_xmld.SelectSingleNode("/order-notification/order/order-status")
            Dim status As String = m_node.InnerText

            m_node = m_xmld.SelectSingleNode("/order-notification/order/order-item")
            Dim ReferenceNumber As String = m_node.Attributes("id").Value
            Dim feeAmount As String = 0
            Try
                m_node = m_xmld.SelectSingleNode("/order-notification/order/order-item/tax")
                feeAmount = m_node.InnerText

            Catch ex As Exception

            End Try
            Dim netAmount As String = 0
            Dim amount As String = 0

            Try
                m_node = m_xmld.SelectSingleNode("/order-notification/order/order-item/total")
                netAmount = m_node.InnerText
                amount = m_node.InnerText
            Catch ex As Exception

            End Try
            m_node = m_xmld.SelectSingleNode("/order-notification/order/customer/address/first-name")
            Dim custFirstName As String = m_node.InnerText
            m_node = m_xmld.SelectSingleNode("/order-notification/order/customer/address/last-name")
            Dim custLastName As String = m_node.InnerText
            m_node = m_xmld.SelectSingleNode("/order-notification/order/customer/address/address1")
            Dim custAddress As String = m_node.InnerText
            m_node = m_xmld.SelectSingleNode("/order-notification/order/customer/address/address2")
            custAddress = custAddress & " " & m_node.InnerText
            m_node = m_xmld.SelectSingleNode("/order-notification/order/customer/address/city")
            Dim custCity As String = m_node.InnerText
            Dim custState As String = ""
            Try
                m_node = m_xmld.SelectSingleNode("/order-notification/order/customer/address/region")
                custState = m_node.InnerText

            Catch ex As Exception

            End Try
            m_node = m_xmld.SelectSingleNode("/order-notification/order/customer/address/postal-code")
            Dim custZip As String = m_node.InnerText
            m_node = m_xmld.SelectSingleNode("/order-notification/order/customer/address/country")
            Dim custCountry As String = m_node.InnerText
            m_node = m_xmld.SelectSingleNode("/order-notification/order/customer/address/email")
            Dim CustomerEmaiAddress As String = m_node.InnerText
            m_node = m_xmld.SelectSingleNode("/order-notification/order/custom-referrer")
            UniPayTransactionID = m_node.InnerText
            m_node = m_xmld.SelectSingleNode("/order-notification/order/ip")
            Dim customerIP As String = m_node.InnerText
            Dim currency As String = "USD"

            If status = "Completed" Then

                '  If Trim(Securitycode) = Trim(GenerateHash(UrlDecode(Request("sale_id")) & Merchant & ReferenceNumber & "@Ek0R1G!aL21")) Then
                Dim sqlStr As String = "select * from PayTransactions WHERE IsSendingToSalesSiteIPN = 0 and UniqueID = '" & UniPayTransactionID & "'"
                Dim cmd As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sqlStr)
                Dim rs As SqlDataReader = DataHelpers.GetDataReader(cmd)

                Try

                    While rs.Read

                        siteIPN = New Dating.Server.Site.Web.UniPAYIPN()
                        ipnSender = New clsCore
                        ipnData.PayProviderTransactionID = ReferenceNumber
                        ipnData.PayProviderAmount = amount

                        buyer.PayerEmail = CustomerEmaiAddress
                        ipnData.CustomerID = rs("CustomerID")
                        ipnData.PaymentDateTime = rs("PaymentDateTime")
                        ipnData.PayTransactionID = UniPayTransactionID
                        ipnData.SalesSiteID = rs("SalesSiteID")
                        ipnData.SalesSiteProductID = rs("SalesSiteProductID")
                        ipnData.TransactionTypeID = rs("SaleTypeID")
                        ipnData.Currency = currency
                        ipnData.custopmerIP = customerIP
                        'ipnData.message_type = message_type
                        'ipnData.fraud_status = fraud_status

                        Dim extraData As String = rs("SalesSiteExtraData").ToString()
                        For Each Data As String In extraData.Split(";")
                            If Data.Contains("customReferrer") Then
                                ipnData.CustomReferrer = Data.Split("=")(1)
                            End If
                            If Data.Contains("promoCode") Then
                                ipnData.PromoCode = Data.Split("=")(1)
                            End If

                        Next
                        Dim ProductDescription As String = rs("ProductDescription").ToString().ToUpper()
                        If (ProductDescription.Contains("CREDITS")) Then
                            If (ProductDescription.Contains("CREDITCARD")) Then
                                ipnData.SaleDescription = "Add credits with Regnow - 2CO. Reference: " & ReferenceNumber
                            Else
                                ipnData.SaleDescription = "Add credits with Regnow - PayPal. Reference: " & ReferenceNumber
                            End If
                        Else
                            If rs("ProductDescription").ToString.Contains("Days") Or rs("ProductDescription").ToString.Contains("Lifetime") Or rs("ProductDescription").ToString.Contains("Unlimited") Then

                                ipnData.SaleDescription = "Add Days with RegNow. Reference: " & ReferenceNumber
                            ElseIf rs("ProductDescription").ToString.Contains("Giga") Then

                                If rs("ProductDescription").ToString.Contains("Oron") Then
                                    ipnData.SaleDescription = "Add oronGiga with RegNow. Reference: " & ReferenceNumber
                                ElseIf rs("ProductDescription").ToString.Contains("Uploaded.to") Then
                                    ipnData.SaleDescription = "Add ulGiga with RegNow. Reference: " & ReferenceNumber
                                Else

                                    ipnData.SaleDescription = "Add Giga with RegNow. Reference: " & ReferenceNumber
                                End If

                            Else
                                ipnData.SaleDescription = "Reseller Add Money"

                            End If
                        End If

                        ipnData.SaleQuantity = rs("GoomenaCredits")
                    End While
                Catch ex As Exception
                    Throw
                Finally
                    If (rs IsNot Nothing) Then rs.Close()
                    If (cmd IsNot Nothing) Then
                        cmd.Connection.Close()
                        cmd.Dispose()
                    End If
                End Try

                Try

                    sqlStr = "UPDATE PayTransactions SET IsCompleted = 1, Currency = '" & currency & "', BuyerInfo_PayerEmail = '" & buyer.PayerEmail & "', BuyerInfo_FirstName = '" & custFirstName & "', BuyerInfo_LastName = '" & custLastName & "', BuyerInfo_Address = '" & custAddress & "', BuyerInfo_ResidenceCountry = '" & custCountry & "', IPN_NotifierIP = '" & Request.ServerVariables("REMOTE_ADDR") & "', IPN_TransactionID = '" & ReferenceNumber & "', Amount = " & amount & ", Fee = " & feeAmount & ", Gross = " & amount - feeAmount & " WHERE UniqueID = '" & UniPayTransactionID & "'"
                    DataHelpers.ExecuteNonQuery(sqlStr)
                Catch ex As Exception

                End Try

                Dim sData As String
                sData = ipnData.PayProviderAmount & "+" & ipnData.PaymentDateTime & "+" & ipnData.PayTransactionID & "+" & ipnData.CustomerID & "+" & ipnData.SalesSiteProductID & "Extr@Ded0men@"
                Dim h As New Library.Public.clsHash
                Dim Code As String = Library.Public.clsHash.ComputeHash(sData, "SHA512")
                ipnData.VerifyHASH = Code
                ipnData.BuyerInfo = buyer
                Dim ipnReturn As clsDataRecordIPNReturn = UniPAYIPN.SendIPN(ipnData)

                Try
                    If Not ipnReturn.HasErrors Then
                        sqlStr = "UPDATE PayTransactions SET IsSendingToSalesSiteIPN = 1, ResponceFromSalesSite = 'Customer Updated' WHERE UniqueID = '" & UniPayTransactionID & "'"
                        DataHelpers.ExecuteNonQuery(sqlStr)
                    Else
                        sqlStr = "UPDATE PayTransactions SET IsSendingToSalesSiteIPN = 0, ResponceFromSalesSite = '" & ipnReturn.Message.Replace("'", "_") & "' WHERE UniqueID = '" & UniPayTransactionID & "'"
                        DataHelpers.ExecuteNonQuery(sqlStr)
                    End If
                Catch ex As Exception
                    sqlStr = "UPDATE PayTransactions SET IsSendingToSalesSiteIPN = 1, ResponceFromSalesSite = 'Customer Updated' WHERE UniqueID = '" & UniPayTransactionID & "'"
                    DataHelpers.ExecuteNonQuery(sqlStr)
                End Try

            End If

        Catch ex As Exception
            Dim sqlStr As String = "UPDATE PayTransactions SET IPN_OriginalCleanData = '" & ex.Message.ToString & "' WHERE UniqueID = '" & UniPayTransactionID & "'"
            DataHelpers.ExecuteNonQuery(sqlStr)

            WebErrorSendEmail(ex, "ReadRegNowResponse ---- " & vbCrLf & readerContent)
        Finally
            reader.Close()
        End Try
        Response.Write("OK")

        Response.Clear()
    End Sub


End Class
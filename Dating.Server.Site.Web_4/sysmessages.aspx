﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="sysmessages.aspx.vb" Inherits="Dating.Server.Site.Web.sysmessages" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    </div>
    <dx:ASPxGridView ID="gv1" runat="server"
        AutoGenerateColumns="False" 
        DataSourceID="SqlDataSource1" 
        KeyFieldName="SitePageID" 
        EnableViewState="false">
        <Columns>
            <dx:GridViewCommandColumn VisibleIndex="0" Visible="false">
                <EditButton Visible="True">
                </EditButton>
                <CellStyle VerticalAlign="Top">
                </CellStyle>
            </dx:GridViewCommandColumn>
            <dx:GridViewDataTextColumn FieldName="myTitle" VisibleIndex="1" Visible="false">
                <PropertiesTextEdit EncodeHtml="False">
                </PropertiesTextEdit>
                <CellStyle VerticalAlign="Top">
                </CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="PageFileName" VisibleIndex="2">
                <PropertiesTextEdit EncodeHtml="False">
                </PropertiesTextEdit>
                <CellStyle VerticalAlign="Top">
                </CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="MessageKey" VisibleIndex="3">
                <PropertiesTextEdit EncodeHtml="False">
                </PropertiesTextEdit>
                <CellStyle VerticalAlign="Top">
                </CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="US" VisibleIndex="4" Width="300px">
                <PropertiesTextEdit EncodeHtml="False">
                </PropertiesTextEdit>
                <CellStyle VerticalAlign="Top">
                </CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="GR" VisibleIndex="5" Width="300px">
                <PropertiesTextEdit EncodeHtml="False">
                </PropertiesTextEdit>
                <CellStyle VerticalAlign="Top">
                </CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="TR" VisibleIndex="6" Width="300px">
                <PropertiesTextEdit EncodeHtml="False">
                </PropertiesTextEdit>
                <CellStyle VerticalAlign="Top">
                </CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="SitePageID" ReadOnly="True" 
                VisibleIndex="7">
                <EditFormSettings Visible="False" />
                <CellStyle VerticalAlign="Top">
                </CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="MessageID" ReadOnly="True" 
                VisibleIndex="8">
                <EditFormSettings Visible="False" />
                <CellStyle VerticalAlign="Top">
                </CellStyle>
            </dx:GridViewDataTextColumn>
        </Columns>
        <SettingsPager Visible="False" AlwaysShowPager="True" PageSize="10" Mode="ShowAllRecords">
        </SettingsPager>
    </dx:ASPxGridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:AppDBconnectionString %>" 
        SelectCommand="SELECT SYS_SitePages.myTitle, SYS_SitePages.PageFileName, SYS_Messages.MessageKey, SYS_Messages.US, SYS_Messages.GR,  SYS_Messages.TR, SYS_SitePages.SitePageID, SYS_Messages.MessageID FROM SYS_SitePages INNER JOIN SYS_Messages ON SYS_SitePages.SitePageID = SYS_Messages.SitePageID ORDER BY SYS_SitePages.PageFileName">
    </asp:SqlDataSource>
    <br />
    <br />
    <dx:ASPxGridView ID="gv2" runat="server" 
        AutoGenerateColumns="False" 
        DataSourceID="SqlDataSource2" 
        KeyFieldName="SitePageID" 
        EnableViewState="false">
        <Columns>
            <dx:GridViewDataTextColumn FieldName="myTitle" VisibleIndex="0" Visible="false">
                <CellStyle VerticalAlign="Top">
                </CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="PageFileName" VisibleIndex="1">
                <PropertiesTextEdit EncodeHtml="False">
                </PropertiesTextEdit>
                <CellStyle VerticalAlign="Top">
                </CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="SitePageID" VisibleIndex="2" 
                ReadOnly="True">
                <PropertiesTextEdit EncodeHtml="False">
                </PropertiesTextEdit>
                <EditFormSettings Visible="False" />
                <CellStyle VerticalAlign="Top">
                </CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="TitleUS" VisibleIndex="4" Width="300px">
                <CellStyle VerticalAlign="Top">
                </CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="BodyUS" 
                VisibleIndex="5">
                <PropertiesTextEdit EncodeHtml="False">
                </PropertiesTextEdit>
                <CellStyle VerticalAlign="Top">
                </CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="TitleGR" VisibleIndex="6" Width="300px">
                <CellStyle VerticalAlign="Top">
                </CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="BodyGR" 
                VisibleIndex="7" Width="300px">
                <PropertiesTextEdit EncodeHtml="False">
                </PropertiesTextEdit>
                <CellStyle VerticalAlign="Top">
                </CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="TitleTR" VisibleIndex="8" Width="300px">
                <CellStyle VerticalAlign="Top">
                </CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="BodyTR" 
                VisibleIndex="9" Width="300px">
                <PropertiesTextEdit EncodeHtml="False">
                </PropertiesTextEdit>
                <CellStyle VerticalAlign="Top">
                </CellStyle>
            </dx:GridViewDataTextColumn>
        </Columns>
        <SettingsPager Visible="False" AlwaysShowPager="True" PageSize="10" Mode="ShowAllRecords">
        </SettingsPager>
    </dx:ASPxGridView>
    <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
        ConnectionString="<%$ ConnectionStrings:AppDBconnectionString %>" 
        
        SelectCommand="SELECT myTitle, PageFileName, SitePageID, TitleUS, TitleGR, BodyUS, BodyGR ,TitleTR, BodyTR FROM SYS_SitePages ORDER BY PageFileName">
    </asp:SqlDataSource>
    </form>
</body>
</html>

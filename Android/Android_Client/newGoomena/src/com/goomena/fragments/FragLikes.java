package com.goomena.fragments;

import com.goomena.app.ActHome;
import com.goomena.app.ActionBarChange;
import com.goomena.app.R;
import com.goomena.app.adapters.LikesAdapters;
import com.goomena.app.tools.Log;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

@SuppressLint("NewApi") 
public class FragLikes extends Fragment
{
	
	private LikesAdapters adapter;
	private ViewPager viewPager;
	private RelativeLayout fr_likes_new_relativeLayout,fr_likes_inbox_relativeLayout,fr_likes_sent_relativeLayout,fr_likes_trash_relativeLayout;
	private TextView fr_likes_new_text,fr_likes_inbox_text,fr_likes_sent_text,fr_likes_trash_text;
	
	private View fr_likes_viewNew,fr_likes_viewInbox,fr_likes_viewSent,fr_likes_viewTrash;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup group, Bundle saved)
	{
		return inflater.inflate(R.layout.fr_likes, group, false);
	}
	
	@Override
	public void onActivityCreated (Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);
		
		//ActionBarChange.changeActionBar(getActivity(), 1);
		
		fr_likes_new_relativeLayout = (RelativeLayout) getActivity().findViewById(R.id.fr_likes_new_relativeLayout);
		fr_likes_inbox_relativeLayout = (RelativeLayout) getActivity().findViewById(R.id.fr_likes_inbox_relativeLayout);
		fr_likes_sent_relativeLayout = (RelativeLayout) getActivity().findViewById(R.id.fr_likes_sent_relativeLayout);
		fr_likes_trash_relativeLayout = (RelativeLayout) getActivity().findViewById(R.id.fr_likes_trash_relativeLayout);
		fr_likes_new_text = (TextView) getActivity().findViewById(R.id.fr_likes_new_text);
		fr_likes_inbox_text = (TextView) getActivity().findViewById(R.id.fr_likes_inbox_text);
		fr_likes_sent_text = (TextView) getActivity().findViewById(R.id.fr_likes_sent_text);
		fr_likes_trash_text = (TextView) getActivity().findViewById(R.id.fr_likes_trash_text);
		fr_likes_viewNew = (View) getActivity().findViewById(R.id.fr_likes_viewNew);
		fr_likes_viewInbox = (View) getActivity().findViewById(R.id.fr_likes_viewInbox);
		fr_likes_viewSent = (View) getActivity().findViewById(R.id.fr_likes_viewSent);
		fr_likes_viewTrash = (View) getActivity().findViewById(R.id.fr_likes_viewTrash);
		viewPager = (ViewPager) getActivity().findViewById(R.id.view_pager_likes);
		viewPager.setPageMargin(50);
		
		fr_likes_new_relativeLayout.setOnClickListener(clickListener);
		fr_likes_inbox_relativeLayout.setOnClickListener(clickListener);
		fr_likes_sent_relativeLayout.setOnClickListener(clickListener);
		fr_likes_trash_relativeLayout.setOnClickListener(clickListener);
		
		
		if(adapter==null)
		{
			adapter = new LikesAdapters(getActivity());
		}
		
		viewPager.setAdapter(adapter);
		
		
		viewPager.setOnPageChangeListener(pageChangeListener);
		
				
		setTab(0);
		viewPager.setCurrentItem(0);
				
		
				
		
		
		
	}
	
	OnClickListener clickListener = new OnClickListener() 
	{
		public void onClick(View v) 
		{
			if(v == fr_likes_new_relativeLayout)
			{
				setTab(0);
				viewPager.setCurrentItem(0);
			}
			else if(v == fr_likes_inbox_relativeLayout)
			{
				setTab(1);
				viewPager.setCurrentItem(1);
			}
			else if(v == fr_likes_sent_relativeLayout)
			{
				setTab(2);
				viewPager.setCurrentItem(2);
			}
			else if(v == fr_likes_trash_relativeLayout)
			{
				setTab(3);
				viewPager.setCurrentItem(3);
			}
		}
	};
	OnPageChangeListener pageChangeListener = new OnPageChangeListener()
	{
		@Override
		public void onPageScrollStateChanged(int arg0) {}
		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2) {}
		@Override
		public void onPageSelected(int arg0) { setTab(arg0); }
	};
//	Handler handler_likes = new Handler()
//	{
//		public void handleMessage(Message msg) 
//  		{	
//			setTab(Integer.parseInt(msg.obj.toString()));
//  		}
//	};
	
	private void setTab(int select)
	{
		if(select==0)
		{
			fr_likes_new_text.setTextColor(getActivity().getResources().getColor(R.color.colorPanelLine));
			fr_likes_inbox_text.setTextColor(getActivity().getResources().getColor(R.color.white));
			fr_likes_sent_text.setTextColor(getActivity().getResources().getColor(R.color.white));
			fr_likes_trash_text.setTextColor(getActivity().getResources().getColor(R.color.white));
			
			fr_likes_viewNew.setBackgroundColor(getActivity().getResources().getColor(R.color.colorPanelLine));
			fr_likes_viewInbox.setBackgroundColor(getActivity().getResources().getColor(R.color.white));
			fr_likes_viewSent.setBackgroundColor(getActivity().getResources().getColor(R.color.white));
			fr_likes_viewTrash.setBackgroundColor(getActivity().getResources().getColor(R.color.white));
		}
		else if(select==1)
		{
			fr_likes_new_text.setTextColor(getActivity().getResources().getColor(R.color.white));
			fr_likes_inbox_text.setTextColor(getActivity().getResources().getColor(R.color.colorPanelLine));
			fr_likes_sent_text.setTextColor(getActivity().getResources().getColor(R.color.white));
			fr_likes_trash_text.setTextColor(getActivity().getResources().getColor(R.color.white));
			
			fr_likes_viewNew.setBackgroundColor(getActivity().getResources().getColor(R.color.white));
			fr_likes_viewInbox.setBackgroundColor(getActivity().getResources().getColor(R.color.colorPanelLine));
			fr_likes_viewSent.setBackgroundColor(getActivity().getResources().getColor(R.color.white));
			fr_likes_viewTrash.setBackgroundColor(getActivity().getResources().getColor(R.color.white));
			
		}
		else if(select==2)
		{
			fr_likes_new_text.setTextColor(getActivity().getResources().getColor(R.color.white));
			fr_likes_inbox_text.setTextColor(getActivity().getResources().getColor(R.color.white));
			fr_likes_sent_text.setTextColor(getActivity().getResources().getColor(R.color.colorPanelLine));
			fr_likes_trash_text.setTextColor(getActivity().getResources().getColor(R.color.white));
			
			fr_likes_viewNew.setBackgroundColor(getActivity().getResources().getColor(R.color.white));
			fr_likes_viewInbox.setBackgroundColor(getActivity().getResources().getColor(R.color.white));
			fr_likes_viewSent.setBackgroundColor(getActivity().getResources().getColor(R.color.colorPanelLine));
			fr_likes_viewTrash.setBackgroundColor(getActivity().getResources().getColor(R.color.white));
			
		}
		else if(select==3)
		{
			fr_likes_new_text.setTextColor(getActivity().getResources().getColor(R.color.white));
			fr_likes_inbox_text.setTextColor(getActivity().getResources().getColor(R.color.white));
			fr_likes_sent_text.setTextColor(getActivity().getResources().getColor(R.color.white));
			fr_likes_trash_text.setTextColor(getActivity().getResources().getColor(R.color.colorPanelLine));
			
			fr_likes_viewNew.setBackgroundColor(getActivity().getResources().getColor(R.color.white));
			fr_likes_viewInbox.setBackgroundColor(getActivity().getResources().getColor(R.color.white));
			fr_likes_viewSent.setBackgroundColor(getActivity().getResources().getColor(R.color.white));
			fr_likes_viewTrash.setBackgroundColor(getActivity().getResources().getColor(R.color.colorPanelLine));
		}
	}

}

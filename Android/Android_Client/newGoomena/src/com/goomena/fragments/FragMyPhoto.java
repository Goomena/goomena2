package com.goomena.fragments;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import com.goomena.app.ActHome;
import com.goomena.app.ActShowPhoto;
import com.goomena.app.ActionBarChange;
import com.goomena.app.R;
import com.goomena.app.adapters.ExpandableHeightGridView;
import com.goomena.app.adapters.ImageAdapter;
import com.goomena.app.adapters.ObservableScrollView;
import com.goomena.app.adapters.ObservableScrollView.ScrollViewListener;
import com.goomena.app.data.ServiceClient;
import com.goomena.app.models.ImagePath;
import com.goomena.app.models.ImagePathList;
import com.goomena.app.models.UserProfile;
import com.goomena.app.tools.Log;
import com.goomena.app.tools.Utils;
import com.goomena.fragments.FragAddPhoto.uploadImagesTask;

@SuppressLint("NewApi") 
public class FragMyPhoto extends Fragment
{	
	ObservableScrollView fr_my_photo_scroll;
	RelativeLayout fr_my_photo_Change,fr_my_photo_Delete;
	LinearLayout fr_my_photo_linearMenu;
	RelativeLayout fr_my_photo_relativeAddphoto,fr_my_photo_relativeMyprofile,fr_my_photo_relativeEditProfile;
	ImageView fr_my_photo_imageProfile;
	ExpandableHeightGridView fr_my_photo_imageGridPublic,fr_my_photo_imageGridPrivate;
	TextView fr_my_photo_Edit;
	
	FragLoading fragLoading;
	Animation animFadein;
	private final static String TAG_FRAGMENT = "TAG_FRAGMENT";
	
	MyImageAdapter adapterpublic;
	MyImageAdapter adapterprivate;
	Dialog d;
	int levelFinal;
	
	ImagePathList listAll;
	
	public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState) 
	{
		return inflater.inflate(R.layout.fr_my_photo, container, false);
	}
	
	public void onActivityCreated (Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);
		
		//ActionBarChange.changeActionBar(getActivity(), 1);
		
		fr_my_photo_scroll = (ObservableScrollView) getActivity().findViewById(R.id.fr_my_photo_scroll);
		fr_my_photo_Change = (RelativeLayout) getActivity().findViewById(R.id.fr_my_photo_Change);
		fr_my_photo_Delete = (RelativeLayout) getActivity().findViewById(R.id.fr_my_photo_Delete);
		fr_my_photo_linearMenu = (LinearLayout) getActivity().findViewById(R.id.fr_my_photo_linearMenu);
		fr_my_photo_relativeAddphoto = (RelativeLayout) getActivity().findViewById(R.id.fr_my_photo_relativeAddphoto);
		fr_my_photo_relativeMyprofile = (RelativeLayout) getActivity().findViewById(R.id.fr_my_photo_relativeMyprofile);
		fr_my_photo_relativeEditProfile = (RelativeLayout) getActivity().findViewById(R.id.fr_my_photo_relativeEditProfile);
		
		fr_my_photo_imageProfile = (ImageView) getActivity().findViewById(R.id.fr_my_photo_imageProfile);
		fr_my_photo_imageGridPublic = new ExpandableHeightGridView(getActivity());
		fr_my_photo_imageGridPrivate = new ExpandableHeightGridView(getActivity());
		
		fr_my_photo_imageGridPublic = (ExpandableHeightGridView) getActivity().findViewById(R.id.fr_my_photo_imageGridPublic);
		fr_my_photo_imageGridPrivate = (ExpandableHeightGridView) getActivity().findViewById(R.id.fr_my_photo_imageGridPrivate);
		
		fr_my_photo_Edit = (TextView) getActivity().findViewById(R.id.fr_my_photo_Edit);
		
		fragLoading = new FragLoading(getActivity());
		
		fr_my_photo_imageGridPublic.setNumColumns(3);
		fr_my_photo_imageGridPublic.setFocusable(false);
		fr_my_photo_imageGridPublic.setExpanded(true);
        
		fr_my_photo_imageGridPrivate.setNumColumns(3);
		fr_my_photo_imageGridPrivate.setFocusable(false);
		fr_my_photo_imageGridPrivate.setExpanded(true);
		
		
		listAll = new ImagePathList();
		listAll.PathList = new ArrayList<ImagePath>();
		fr_my_photo_Delete.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				for(int i=0; i<listAll.PathList.size(); i++)
				{
					if(listAll.PathList.get(i).Path.equals(Utils.getSharedPreferences(getActivity()).getString("imageurl", "").replace("/d150", "")))
					{
						new getDeletePhotosTask(listAll.PathList.get(i).photoId).execute();
						FragmentManager fm = getFragmentManager();
						fm.popBackStack();
					}
				}
			}
		});
		
		fr_my_photo_scroll.setScrollViewListener(new ScrollViewListener() {
        	int t=0,u=0;
			@Override
			public void onScrollStopped() {}
			@SuppressLint("NewApi") 
			@Override
			public void onScrollChanged(ScrollView scrollView, int x, int y, int oldx,int oldy) {
				// TODO Auto-generated method stub
				if(y==0)
				{
					fr_my_photo_linearMenu.setTranslationY(0);
					t=0;
					u=0;
				}
				else
				{
					if(y > oldy)
					{
						u = u + (y-oldy);
						fr_my_photo_linearMenu.setTranslationY(u);
						t=200;
					}
					else if(oldy > y)
					{
						t = t - (oldy-y) ;
						if(t>0)
						{
							fr_my_photo_linearMenu.setTranslationY(t);
						}
						else
						{
							t=0;
							fr_my_photo_linearMenu.setTranslationY(0);
						}
						u=t;
					}
				}
			}
		});
		
		Utils.getImageLoader(getActivity()).displayImage(Utils.getSharedPreferences(getActivity()).getString("imageurl", "").replace("/d150", ""), fr_my_photo_imageProfile, Utils.getImageOptions());
		new getPhotosTask(fr_my_photo_imageGridPublic,fr_my_photo_imageGridPrivate,fr_my_photo_Change).execute();
		
		
		fr_my_photo_relativeAddphoto.setOnClickListener(clickListener);
		fr_my_photo_relativeMyprofile.setOnClickListener(clickListener);
		fr_my_photo_relativeEditProfile.setOnClickListener(clickListener);
		fr_my_photo_Edit.setOnClickListener(clickListener);
		
		fr_my_photo_relativeMyprofile.setBackgroundColor(getActivity().getResources().getColor(R.color.alphaGrey));
	}

	
	
	
	
	class getPhotosTask extends AsyncTask<Void,Integer,ImagePathList>
	{
		ExpandableHeightGridView _imageGrid = new ExpandableHeightGridView(getActivity());
		ExpandableHeightGridView _imageGridPr = new ExpandableHeightGridView(getActivity());
		
		ImageView _imgDisplay;
		RelativeLayout _fr_my_photo_Change;
		
		public getPhotosTask(ExpandableHeightGridView imageGrid,ExpandableHeightGridView imageGridPr,RelativeLayout fr_my_photo_Change)
		{
			this._imageGrid = imageGrid;
			this._imageGridPr = imageGridPr;
			this._fr_my_photo_Change = fr_my_photo_Change;
		}
		@Override
		protected void onPreExecute()
		{
			fragLoading.Show();
		}
		@Override
		protected ImagePathList doInBackground(Void... params)
		{
			ImagePathList list;
			list = ServiceClient.getUserPhotosThumbs( Utils.getSharedPreferences(getActivity()).getString("userid", ""),
					Utils.getSharedPreferences(getActivity()).getString("userid", ""),Utils.getSharedPreferences(getActivity()).getString("genderid", ""));
			return list;
		}
		 
		@Override
		protected void onPostExecute(final ImagePathList list)
		{
			fragLoading.Close();
			if(list!=null)
			{
				final ImagePathList listpublic = new ImagePathList();
				listpublic.PathList = new ArrayList<ImagePath>();
				
				final ImagePathList listprivate = new ImagePathList();
				listprivate.PathList = new ArrayList<ImagePath>();
				
				
				
				for(int i=0; i<list.PathList.size(); i++)
				{
					if(list.PathList.get(i).Level.equals("0"))
					{
						listpublic.PathList.add(list.PathList.get(i));
					}
					else
					{
						listprivate.PathList.add(list.PathList.get(i));
					}
				}
				
				
				listAll.PathList.addAll(listpublic.PathList);
				listAll.PathList.addAll(listprivate.PathList);
				
				ImagePath a = new ImagePath();
				a.Path = "drawable://" + R.drawable.add_big;
				a.Level = "0";
				a.photoType="-5";
				listpublic.PathList.add(0,a);
				
				ImagePath b = new ImagePath();
				b.Path = "drawable://" + R.drawable.add_big;
				b.Level = "0";
				b.photoType="-5";
				listprivate.PathList.add(0, b);
				
				
				adapterpublic = new MyImageAdapter(getActivity());
				adapterpublic.setItems(listpublic);
				_imageGrid.setAdapter(adapterpublic);
				
				
				adapterprivate = new MyImageAdapter(getActivity());
				adapterprivate.setItems(listprivate);
				_imageGridPr.setAdapter(adapterprivate);
				
				
				_imageGrid.setOnItemClickListener(new OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1, int index,long arg3) 
					{
						if(index==0)
						{
							Fragment fr = new FragAddPhoto();
							FragmentManager fm = getActivity().getFragmentManager();
							
					        FragmentTransaction fragmentTransaction = fm.beginTransaction();
					        fragmentTransaction.setCustomAnimations(R.anim.left_slidein_list_animator, R.anim.left_slideout_list_animator,R.anim.right_slidein_list_animator, R.anim.right_slideout_list_animator);
					        fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
					        fragmentTransaction.addToBackStack(null);
					        fragmentTransaction.commit();
						}
						else
						{
							if(listpublic!=null)
							{
								ArrayList<String> viewpagerImages = new ArrayList<String>();
								for(int i=1;i<listpublic.PathList.size();i++)
								{
									viewpagerImages.add(listpublic.PathList.get(i).Path);
								}
								Intent i = new Intent(getActivity(),ActShowPhoto.class);
								i.putExtra("ImageUrls", viewpagerImages);
								i.putExtra("Index", index-1);
								getActivity().startActivity(i);
							}
						}
					}
				});
				
				_imageGridPr.setOnItemClickListener(new OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1, int index,long arg3) 
					{
						if(index==0)
						{
							Fragment fr = new FragAddPhoto();
							FragmentManager fm = getActivity().getFragmentManager();
							
					        FragmentTransaction fragmentTransaction = fm.beginTransaction();
					        fragmentTransaction.setCustomAnimations(R.anim.left_slidein_list_animator, R.anim.left_slideout_list_animator,R.anim.right_slidein_list_animator, R.anim.right_slideout_list_animator);
					        fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
					        fragmentTransaction.addToBackStack(null);
					        fragmentTransaction.commit();
						}
						else
						{
							if(listprivate!=null)
							{
								ArrayList<String> viewpagerImages = new ArrayList<String>();
								for(int i=1;i<listprivate.PathList.size();i++)
								{
									viewpagerImages.add(listprivate.PathList.get(i).Path);
								}
								Intent i = new Intent(getActivity(),ActShowPhoto.class);
								i.putExtra("ImageUrls", viewpagerImages);
								i.putExtra("Index", index-1);
								getActivity().startActivity(i);
							}
						}
					}
				});
				
				_fr_my_photo_Change.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						showDialogPhotos(list);
					}
				});
			}
		}
	}
	
	
	
	
	
	
	OnClickListener clickListener = new OnClickListener() 
	{
		@Override
		public void onClick(View v) 
		{
			if(v == fr_my_photo_relativeAddphoto)
			{
				Fragment fr = new FragAddPhoto();
				FragmentManager fm = getActivity().getFragmentManager();
				
		        FragmentTransaction fragmentTransaction = fm.beginTransaction();
		        fragmentTransaction.setCustomAnimations(R.anim.left_slidein_list_animator, R.anim.left_slideout_list_animator,R.anim.right_slidein_list_animator, R.anim.right_slideout_list_animator);
		        fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
		        fragmentTransaction.addToBackStack(null);
		
		        fragmentTransaction.commit();
			}
			else if(v == fr_my_photo_relativeMyprofile)
			{
				/*
				Fragment fr = new FragMyProfile();
				FragmentManager fm = getActivity().getFragmentManager();
				
		        FragmentTransaction fragmentTransaction = fm.beginTransaction();
		        fragmentTransaction.setCustomAnimations(R.anim.left_slidein_list_animator, R.anim.left_slideout_list_animator,R.anim.right_slidein_list_animator, R.anim.right_slideout_list_animator);
		        fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
		        fragmentTransaction.addToBackStack(null);
		
		        fragmentTransaction.commit();
		        */
			}
			else if(v == fr_my_photo_relativeEditProfile)
			{
				Fragment fr = new FragEditProfile();
				FragmentManager fm = getActivity().getFragmentManager();
				
		        FragmentTransaction fragmentTransaction = fm.beginTransaction();
		        fragmentTransaction.setCustomAnimations(R.anim.left_slidein_list_animator, R.anim.left_slideout_list_animator,R.anim.right_slidein_list_animator, R.anim.right_slideout_list_animator);
		        fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
		        fragmentTransaction.addToBackStack(null);
		
		        fragmentTransaction.commit();
			}
			else if(v == fr_my_photo_Edit)
			{
				if(!adapterpublic.getviewEdit())
				{
					adapterpublic.viewEdit(true);
					adapterpublic.notifyDataSetChanged();
					adapterprivate.viewEdit(true);
					adapterprivate.notifyDataSetChanged();
				}
				else
				{
					adapterpublic.viewEdit(false);
					adapterpublic.notifyDataSetChanged();
					adapterprivate.viewEdit(false);
					adapterprivate.notifyDataSetChanged();
				}
			}
		}
	};
	
	
	
	
	
	
	
	public class MyImageAdapter extends BaseAdapter 
	{	
		private Activity _activity;
		private ImagePathList imageList;
		Boolean _vEdit=false;
		
		
		 
		    public MyImageAdapter(Activity activity)
		    {
		        this._activity = activity;
		    }
		    
		    public void setItems(ImagePathList list)
		    {
		    	this.imageList = list;
		    }
		    
		    public void viewEdit(Boolean vEdit)
		    {
		    	_vEdit = vEdit;
		    }
		    
		    public Boolean getviewEdit()
		    {
		    	return _vEdit;
		    }
		 
		    @Override
		    public int getCount() 
		    {
		        return imageList.PathList.size();
		    }
		 
		    @Override
		    public Object getItem(int position) 
		    {
		        return imageList.PathList.get(position);
		    }
		 
		    @Override
		    public long getItemId(int position) 
		    {
		        return 0;
		    }
		 
		    @Override
		    public View getView(final int position, View convertView, ViewGroup parent) 
		    {
		    	RelativeLayout layout = (RelativeLayout) _activity.getLayoutInflater().inflate(R.layout.item_my_image, null);		    	
		        ImageView imageView = (ImageView) layout.findViewById(R.id.image);
		        //final ImageView imageEdit = (ImageView) layout.findViewById(R.id.imageEdit);
		        final TextView item_image_imageDelete = (TextView) layout.findViewById(R.id.item_image_imageDelete);
		        final TextView item_image_imageEdit = (TextView) layout.findViewById(R.id.item_image_imageEdit);
		        RelativeLayout item_image_relativeDenied = (RelativeLayout) layout.findViewById(R.id.item_image_relativeDenied);
		        TextView item_image_textdecline = (TextView) layout.findViewById(R.id.item_image_textdecline);
		        RelativeLayout item_image_relativeLevel = (RelativeLayout) layout.findViewById(R.id.item_image_relativeLevel);
		        TextView item_image_textLevel = (TextView) layout.findViewById(R.id.item_image_textLevel);
		        
		        
		        if(_vEdit)
		        {
		        	
							
							
							if(imageList.PathList.get(position).photoType.contains("2"))
					        {
								item_image_imageDelete.setVisibility(View.VISIBLE);
								item_image_imageEdit.setVisibility(View.GONE);
					        }
							else
							{
								item_image_imageDelete.setVisibility(View.VISIBLE);
								item_image_imageEdit.setVisibility(View.VISIBLE);
							}
							
							item_image_imageDelete.setOnClickListener(new OnClickListener() {
								
								@Override
								public void onClick(View v) {
									// TODO Auto-generated method stub
									new getDeletePhotosTask(imageList.PathList.get(position).photoId).execute();
								}
							});
							
							item_image_imageEdit.setOnClickListener(new OnClickListener() {
								
								@Override
								public void onClick(View v) {
									// TODO Auto-generated method stub
									
									showCategory(imageList.PathList.get(position).photoId);
								}
							});
						
		        }
		        else
		        {
		        	///imageEdit.setVisibility(View.GONE);
		        	item_image_imageDelete.setVisibility(View.GONE);
					item_image_imageEdit.setVisibility(View.GONE);
		        }
		        
		        
		        
		        if(position==0)
		        {
		        	//imageEdit.setVisibility(View.GONE);
		        	item_image_imageDelete.setVisibility(View.GONE);
					item_image_imageEdit.setVisibility(View.GONE);
		        }
		        
		        Utils.getImageLoader(_activity).displayImage(imageList.PathList.get(position).Path, imageView, Utils.getImageOptions());
		        
		        if(imageList.PathList.get(position).photoType.contains("2"))
		        {
		        	item_image_relativeDenied.setVisibility(View.VISIBLE);
		        	item_image_textdecline.setText(R.string.photodecline);
		        }
		        
		       if(!imageList.PathList.get(position).Level.equals("0"))
		        {
		        	item_image_relativeLevel.setVisibility(View.VISIBLE);
		        	item_image_textLevel.setText(imageList.PathList.get(position).Level);
		        }
		        
		        if(imageList.PathList.get(position).photoType.contains("-5"))
		        {
		        	item_image_imageDelete.setVisibility(View.GONE);
		        }
		        
		        
		        
		        
		       
		        return layout;
		    }

	}
	
	
	Boolean Npublic1 = true;
	Boolean Npr1 = false;
	Boolean Npr2 = false;
	Boolean Npr3 = false;
	Boolean Npr4 = false;
	Boolean Npr5 = false;
	Boolean Npr6 = false;
	Boolean Npr7 = false;
	Boolean Npr8 = false;
    Boolean Npr9 = false;
	Boolean Npr10 = false;
	
	private void showCategory(final String photoid) 
	{

		final Dialog d = new Dialog(getActivity(), R.style.DialogSlideAnim);
		d.setContentView(R.layout.level_view_pro);
		Button cancelButton = (Button) d.findViewById(R.id.cancel_btn);
		Button okButton = (Button) d.findViewById(R.id.ok_btn);
		final LinearLayout public1 = (LinearLayout) d.findViewById(R.id.public1);
	    final LinearLayout pr1 = (LinearLayout) d.findViewById(R.id.private1);
	    final LinearLayout pr2 = (LinearLayout) d.findViewById(R.id.private2);
	    final LinearLayout pr3 = (LinearLayout) d.findViewById(R.id.private3);
		final LinearLayout pr4 = (LinearLayout) d.findViewById(R.id.private4);
	    final LinearLayout pr5 = (LinearLayout) d.findViewById(R.id.private5);
	    final LinearLayout pr6 = (LinearLayout) d.findViewById(R.id.private6);
		final LinearLayout pr7 = (LinearLayout) d.findViewById(R.id.private7);
	    final LinearLayout pr8 = (LinearLayout) d.findViewById(R.id.private8);
	    final LinearLayout pr9 = (LinearLayout) d.findViewById(R.id.private9);
		final LinearLayout pr10 = (LinearLayout) d.findViewById(R.id.private10);
		
		
		final ImageView impublic1 = (ImageView) d.findViewById(R.id.imageCheckpublic1);
	    final ImageView impr1 = (ImageView) d.findViewById(R.id.imageCheckprivate1);
	    final ImageView impr2 = (ImageView) d.findViewById(R.id.imageCheckprivate2);
	    final ImageView impr3 = (ImageView) d.findViewById(R.id.imageCheckprivate3);
		final ImageView impr4 = (ImageView) d.findViewById(R.id.imageCheckprivate4);
	    final ImageView impr5 = (ImageView) d.findViewById(R.id.imageCheckprivate5);
	    final ImageView impr6 = (ImageView) d.findViewById(R.id.imageCheckprivate6);
		final ImageView impr7 = (ImageView) d.findViewById(R.id.imageCheckprivate7);
	    final ImageView impr8 = (ImageView) d.findViewById(R.id.imageCheckprivate8);
	    final ImageView impr9 = (ImageView) d.findViewById(R.id.imageCheckprivate9);
		final ImageView impr10 = (ImageView) d.findViewById(R.id.imageCheckprivate10);
		
		Npublic1 = true;
		Npr1 = false;
		Npr2 = false;
		Npr3 = false;
		Npr4 = false;
		Npr5 = false;
		Npr6 = false;
		Npr7 = false;
		Npr8 = false;
	    Npr9 = false;
		Npr10 = false;
	    
		OnClickListener ffff = new OnClickListener()
		{

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				if(v == public1)
				{
					public1.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr1.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr2.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr3.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr4.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr5.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr6.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr7.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr8.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr9.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr10.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					
					impublic1.setBackgroundResource(R.drawable.search_check);
					impr1.setBackgroundResource(R.drawable.search_unckeck);
					impr2.setBackgroundResource(R.drawable.search_unckeck);
					impr3.setBackgroundResource(R.drawable.search_unckeck);
					impr4.setBackgroundResource(R.drawable.search_unckeck);
					impr5.setBackgroundResource(R.drawable.search_unckeck);
					impr6.setBackgroundResource(R.drawable.search_unckeck);
					impr7.setBackgroundResource(R.drawable.search_unckeck);
					impr8.setBackgroundResource(R.drawable.search_unckeck);
					impr9.setBackgroundResource(R.drawable.search_unckeck);
					impr10.setBackgroundResource(R.drawable.search_unckeck);
					
					Npublic1 = true;
					Npr1 = false;
					Npr2 = false;
					Npr3 = false;
					Npr4 = false;
					Npr5 = false;
					Npr6 = false;
					Npr7 = false;
					Npr8 = false;
				    Npr9 = false;
					Npr10 = false;
				}
				else if(v == pr1)
				{
					public1.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr1.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr2.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr3.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr4.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr5.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr6.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr7.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr8.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr9.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr10.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					
					impublic1.setBackgroundResource(R.drawable.search_unckeck);
					impr1.setBackgroundResource(R.drawable.search_check);
					impr2.setBackgroundResource(R.drawable.search_unckeck);
					impr3.setBackgroundResource(R.drawable.search_unckeck);
					impr4.setBackgroundResource(R.drawable.search_unckeck);
					impr5.setBackgroundResource(R.drawable.search_unckeck);
					impr6.setBackgroundResource(R.drawable.search_unckeck);
					impr7.setBackgroundResource(R.drawable.search_unckeck);
					impr8.setBackgroundResource(R.drawable.search_unckeck);
					impr9.setBackgroundResource(R.drawable.search_unckeck);
					impr10.setBackgroundResource(R.drawable.search_unckeck);
					
					Npublic1 = false;
					Npr1 = true;
					Npr2 = false;
					Npr3 = false;
					Npr4 = false;
					Npr5 = false;
					Npr6 = false;
					Npr7 = false;
					Npr8 = false;
				    Npr9 = false;
					Npr10 = false;
				}
				else if(v == pr2)
				{
					public1.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr1.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr2.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr3.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr4.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr5.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr6.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr7.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr8.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr9.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr10.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					
					impublic1.setBackgroundResource(R.drawable.search_unckeck);
					impr1.setBackgroundResource(R.drawable.search_unckeck);
					impr2.setBackgroundResource(R.drawable.search_check);
					impr3.setBackgroundResource(R.drawable.search_unckeck);
					impr4.setBackgroundResource(R.drawable.search_unckeck);
					impr5.setBackgroundResource(R.drawable.search_unckeck);
					impr6.setBackgroundResource(R.drawable.search_unckeck);
					impr7.setBackgroundResource(R.drawable.search_unckeck);
					impr8.setBackgroundResource(R.drawable.search_unckeck);
					impr9.setBackgroundResource(R.drawable.search_unckeck);
					impr10.setBackgroundResource(R.drawable.search_unckeck);
					
					Npublic1 = false;
					Npr1 = false;
					Npr2 = true;
					Npr3 = false;
					Npr4 = false;
					Npr5 = false;
					Npr6 = false;
					Npr7 = false;
					Npr8 = false;
				    Npr9 = false;
					Npr10 = false;
				}
				else if(v == pr3)
				{
					public1.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr1.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr2.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr3.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr4.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr5.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr6.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr7.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr8.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr9.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr10.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					
					impublic1.setBackgroundResource(R.drawable.search_unckeck);
					impr1.setBackgroundResource(R.drawable.search_unckeck);
					impr2.setBackgroundResource(R.drawable.search_unckeck);
					impr3.setBackgroundResource(R.drawable.search_check);
					impr4.setBackgroundResource(R.drawable.search_unckeck);
					impr5.setBackgroundResource(R.drawable.search_unckeck);
					impr6.setBackgroundResource(R.drawable.search_unckeck);
					impr7.setBackgroundResource(R.drawable.search_unckeck);
					impr8.setBackgroundResource(R.drawable.search_unckeck);
					impr9.setBackgroundResource(R.drawable.search_unckeck);
					impr10.setBackgroundResource(R.drawable.search_unckeck);
					
					Npublic1 = false;
					Npr1 = false;
					Npr2 = false;
					Npr3 = true;
					Npr4 = false;
					Npr5 = false;
					Npr6 = false;
					Npr7 = false;
					Npr8 = false;
				    Npr9 = false;
					Npr10 = false;
				}
				else if(v == pr4)
				{
					public1.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr1.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr2.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr3.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr4.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr5.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr6.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr7.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr8.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr9.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr10.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					
					impublic1.setBackgroundResource(R.drawable.search_unckeck);
					impr1.setBackgroundResource(R.drawable.search_unckeck);
					impr2.setBackgroundResource(R.drawable.search_unckeck);
					impr3.setBackgroundResource(R.drawable.search_unckeck);
					impr4.setBackgroundResource(R.drawable.search_check);
					impr5.setBackgroundResource(R.drawable.search_unckeck);
					impr6.setBackgroundResource(R.drawable.search_unckeck);
					impr7.setBackgroundResource(R.drawable.search_unckeck);
					impr8.setBackgroundResource(R.drawable.search_unckeck);
					impr9.setBackgroundResource(R.drawable.search_unckeck);
					impr10.setBackgroundResource(R.drawable.search_unckeck);
					
					Npublic1 = false;
					Npr1 = false;
					Npr2 = false;
					Npr3 = false;
					Npr4 = true;
					Npr5 = false;
					Npr6 = false;
					Npr7 = false;
					Npr8 = false;
				    Npr9 = false;
					Npr10 = false;
				}
				else if(v == pr5)
				{
					public1.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr1.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr2.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr3.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr4.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr5.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr6.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr7.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr8.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr9.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr10.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					
					impublic1.setBackgroundResource(R.drawable.search_unckeck);
					impr1.setBackgroundResource(R.drawable.search_unckeck);
					impr2.setBackgroundResource(R.drawable.search_unckeck);
					impr3.setBackgroundResource(R.drawable.search_unckeck);
					impr4.setBackgroundResource(R.drawable.search_unckeck);
					impr5.setBackgroundResource(R.drawable.search_check);
					impr6.setBackgroundResource(R.drawable.search_unckeck);
					impr7.setBackgroundResource(R.drawable.search_unckeck);
					impr8.setBackgroundResource(R.drawable.search_unckeck);
					impr9.setBackgroundResource(R.drawable.search_unckeck);
					impr10.setBackgroundResource(R.drawable.search_unckeck);
					
					Npublic1 = false;
					Npr1 = false;
					Npr2 = false;
					Npr3 = false;
					Npr4 = false;
					Npr5 = true;
					Npr6 = false;
					Npr7 = false;
					Npr8 = false;
				    Npr9 = false;
					Npr10 = false;
				}
				else if(v == pr6)
				{
					public1.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr1.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr2.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr3.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr4.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr5.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr6.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr7.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr8.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr9.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr10.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					
					impublic1.setBackgroundResource(R.drawable.search_unckeck);
					impr1.setBackgroundResource(R.drawable.search_unckeck);
					impr2.setBackgroundResource(R.drawable.search_unckeck);
					impr3.setBackgroundResource(R.drawable.search_unckeck);
					impr4.setBackgroundResource(R.drawable.search_unckeck);
					impr5.setBackgroundResource(R.drawable.search_unckeck);
					impr6.setBackgroundResource(R.drawable.search_check);
					impr7.setBackgroundResource(R.drawable.search_unckeck);
					impr8.setBackgroundResource(R.drawable.search_unckeck);
					impr9.setBackgroundResource(R.drawable.search_unckeck);
					impr10.setBackgroundResource(R.drawable.search_unckeck);
					
					Npublic1 = false;
					Npr1 = false;
					Npr2 = false;
					Npr3 = false;
					Npr4 = false;
					Npr5 = false;
					Npr6 = true;
					Npr7 = false;
					Npr8 = false;
				    Npr9 = false;
					Npr10 = false;
				}
				else if(v == pr7)
				{
					public1.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr1.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr2.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr3.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr4.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr5.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr6.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr7.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr8.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr9.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr10.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					
					impublic1.setBackgroundResource(R.drawable.search_unckeck);
					impr1.setBackgroundResource(R.drawable.search_unckeck);
					impr2.setBackgroundResource(R.drawable.search_unckeck);
					impr3.setBackgroundResource(R.drawable.search_unckeck);
					impr4.setBackgroundResource(R.drawable.search_unckeck);
					impr5.setBackgroundResource(R.drawable.search_unckeck);
					impr6.setBackgroundResource(R.drawable.search_unckeck);
					impr7.setBackgroundResource(R.drawable.search_check);
					impr8.setBackgroundResource(R.drawable.search_unckeck);
					impr9.setBackgroundResource(R.drawable.search_unckeck);
					impr10.setBackgroundResource(R.drawable.search_unckeck);
					
					Npublic1 = false;
					Npr1 = false;
					Npr2 = false;
					Npr3 = false;
					Npr4 = false;
					Npr5 = false;
					Npr6 = false;
					Npr7 = true;
					Npr8 = false;
				    Npr9 = false;
					Npr10 = false;
				}
				else if(v == pr8)
				{
					public1.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr1.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr2.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr3.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr4.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr5.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr6.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr7.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr8.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr9.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr10.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					
					impublic1.setBackgroundResource(R.drawable.search_unckeck);
					impr1.setBackgroundResource(R.drawable.search_unckeck);
					impr2.setBackgroundResource(R.drawable.search_unckeck);
					impr3.setBackgroundResource(R.drawable.search_unckeck);
					impr4.setBackgroundResource(R.drawable.search_unckeck);
					impr5.setBackgroundResource(R.drawable.search_unckeck);
					impr6.setBackgroundResource(R.drawable.search_unckeck);
					impr7.setBackgroundResource(R.drawable.search_unckeck);
					impr8.setBackgroundResource(R.drawable.search_check);
					impr9.setBackgroundResource(R.drawable.search_unckeck);
					impr10.setBackgroundResource(R.drawable.search_unckeck);
					
					Npublic1 = false;
					Npr1 = false;
					Npr2 = false;
					Npr3 = false;
					Npr4 = false;
					Npr5 = false;
					Npr6 = false;
					Npr7 = false;
					Npr8 = true;
				    Npr9 = false;
					Npr10 = false;
				}
				else if(v == pr9)
				{
					public1.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr1.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr2.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr3.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr4.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr5.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr6.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr7.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr8.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr9.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr10.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					
					impublic1.setBackgroundResource(R.drawable.search_unckeck);
					impr1.setBackgroundResource(R.drawable.search_unckeck);
					impr2.setBackgroundResource(R.drawable.search_unckeck);
					impr3.setBackgroundResource(R.drawable.search_unckeck);
					impr4.setBackgroundResource(R.drawable.search_unckeck);
					impr5.setBackgroundResource(R.drawable.search_unckeck);
					impr6.setBackgroundResource(R.drawable.search_unckeck);
					impr7.setBackgroundResource(R.drawable.search_unckeck);
					impr8.setBackgroundResource(R.drawable.search_unckeck);
					impr9.setBackgroundResource(R.drawable.search_check);
					impr10.setBackgroundResource(R.drawable.search_unckeck);
					
					Npublic1 = false;
					Npr1 = false;
					Npr2 = false;
					Npr3 = false;
					Npr4 = false;
					Npr5 = false;
					Npr6 = false;
					Npr7 = false;
					Npr8 = false;
				    Npr9 = true;
					Npr10 = false;
				}
				else if(v == pr10)
				{
					public1.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr1.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr2.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr3.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr4.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr5.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr6.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr7.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr8.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr9.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr10.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					
					impublic1.setBackgroundResource(R.drawable.search_unckeck);
					impr1.setBackgroundResource(R.drawable.search_unckeck);
					impr2.setBackgroundResource(R.drawable.search_unckeck);
					impr3.setBackgroundResource(R.drawable.search_unckeck);
					impr4.setBackgroundResource(R.drawable.search_unckeck);
					impr5.setBackgroundResource(R.drawable.search_unckeck);
					impr6.setBackgroundResource(R.drawable.search_unckeck);
					impr7.setBackgroundResource(R.drawable.search_unckeck);
					impr8.setBackgroundResource(R.drawable.search_unckeck);
					impr9.setBackgroundResource(R.drawable.search_unckeck);
					impr10.setBackgroundResource(R.drawable.search_check);
					
					Npublic1 = false;
					Npr1 = false;
					Npr2 = false;
					Npr3 = false;
					Npr4 = false;
					Npr5 = false;
					Npr6 = false;
					Npr7 = false;
					Npr8 = false;
				    Npr9 = false;
					Npr10 = true;
				}
				
			}
			
		};
		
		public1.setOnClickListener(ffff);
		pr1.setOnClickListener(ffff);
		pr2.setOnClickListener(ffff);
		pr3.setOnClickListener(ffff);
		pr4.setOnClickListener(ffff);
		pr5.setOnClickListener(ffff);
		pr6.setOnClickListener(ffff);
		pr7.setOnClickListener(ffff);
		pr8.setOnClickListener(ffff);
		pr9.setOnClickListener(ffff);
		pr10.setOnClickListener(ffff);
		
		
		
		cancelButton.setOnClickListener(new OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				d.cancel();
			}
		});
		
		okButton.setOnClickListener(new OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				 if (Npr10) {
                     levelFinal = 10;
					 //leveltext.setText(R.string.p10);
				 } else if (Npr1) {
					 levelFinal = 1;
					 //leveltext.setText(R.string.p1);
				 } else if (Npr2) {
					 levelFinal = 2;
					 //leveltext.setText(R.string.p2);
				 } else if (Npr3) {
					 levelFinal = 3;
					 //leveltext.setText(R.string.p3);
				 } else if (Npr4) {
					 levelFinal = 4;
					 //leveltext.setText(R.string.p4);
				 } else if (Npr5) {
					 levelFinal = 5;
					 //leveltext.setText(R.string.p5);
				 } else if (Npr6) {
					 levelFinal = 6;
					 //leveltext.setText(R.string.p6);
				 } else if (Npr7) {
					 levelFinal = 7;
					 //leveltext.setText(R.string.p7);
				 } else if (Npr8) {
					 levelFinal = 8;
					 //leveltext.setText(R.string.p8);
				 } else if (Npr9) {
					 levelFinal = 9;
					 //leveltext.setText(R.string.p9);
				 } else if (Npublic1) {
					 levelFinal = 0;
					 //leveltext.setText(R.string.p0);
				 } else {

				 }
				 
				new setPhotoLevelTask(photoid,String.valueOf(levelFinal)).execute();
				d.cancel();
			}
		});
		d.show();
	}
	
	class getDeletePhotosTask extends AsyncTask<Void,Integer,String>
	{
		String pos;
		public getDeletePhotosTask(String posit)
		{
			this.pos = posit;
		}
		 
		@Override
		protected void onPreExecute()
		{
			fragLoading.Show();
		}
		@Override
		protected String doInBackground(Void... params) 
		{
			SharedPreferences sp = Utils.getSharedPreferences(getActivity());
			String result1 = ServiceClient.deletePhoto(sp.getString("userid",""), pos);
			return result1;
		}
			 
		@Override
		protected void onPostExecute(String res)
		{		
			fragLoading.Close();
			if (res.indexOf("ok") != -1) 
			{
				fr_my_photo_imageGridPublic.clearChoices();
				new getPhotosTask(fr_my_photo_imageGridPublic,fr_my_photo_imageGridPrivate,fr_my_photo_Change).execute();
			}
			else
			{
				Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.dialoglevel), Toast.LENGTH_LONG).show();
			}
		}		 
	}
	
	
	
	class setPhotoLevelTask extends AsyncTask<Void,Integer,String>
	{
		String pos;
		String level;
		public setPhotoLevelTask(String posit,String level)
		{
			this.pos = posit;
			this.level = level;
		}
		 
		@Override
		protected void onPreExecute()
		{
			fragLoading.Show();
		}
		@Override
		protected String doInBackground(Void... params) 
		{
			SharedPreferences sp = Utils.getSharedPreferences(getActivity());
			String result1 = ServiceClient.setPhotoLevel(sp.getString("userid",""), pos,level);
			return result1;
		}
			 
		@Override
		protected void onPostExecute(String res)
		{		
			fragLoading.Close();
			if (res.indexOf("ok") != -1) 
			{
				fr_my_photo_imageGridPublic.clearChoices();
				new getPhotosTask(fr_my_photo_imageGridPublic,fr_my_photo_imageGridPrivate,fr_my_photo_Change).execute();
			}
			else
			{
				Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.dialoglevel), Toast.LENGTH_LONG).show();
			}
		}		 
	}
	
	class setDeafultImageTask extends AsyncTask<String,Void,Void>
	{
		@Override
		protected void onPreExecute()
		{
			fragLoading.Show();
		}
		@Override
		protected Void doInBackground(String... params) 
		{
			ServiceClient.setDeafultPhoto(Utils.getSharedPreferences(getActivity()).getString("userid", ""), params[0]);
			return null;
		}
		
		@Override
		protected void onPostExecute(Void v)
		{
			//new getProfileTask().execute();
			fragLoading.Close();
		}
	}
	
	
	
	
	private void showDialogPhotos(ImagePathList list) 
	{
		final Dialog dPhotos = new Dialog(getActivity(), R.style.DialogSlideAnim);
		dPhotos.setContentView(R.layout.dialog_photos);
		MyImageAdapter adapter = new MyImageAdapter(getActivity());
		final ImagePathList thisList = new ImagePathList();
		thisList.PathList = new ArrayList<ImagePath>();
		
		ExpandableHeightGridView dialog_photos_imageGrid = new ExpandableHeightGridView(getActivity());
		ProgressBar dialog_photos_ProgressBar = (ProgressBar) dPhotos.findViewById(R.id.dialog_photos_ProgressBar);
		ProgressBar dialog_photos_ProgressBar2 = (ProgressBar) dPhotos.findViewById(R.id.dialog_photos_ProgressBar2);
		dialog_photos_imageGrid = (ExpandableHeightGridView) dPhotos.findViewById(R.id.dialog_photos_imageGrid);
		RelativeLayout dialog_photos_relativeInfo = (RelativeLayout) dPhotos.findViewById(R.id.dialog_photos_relativeInfo);
		final RelativeLayout dialog_photos_relativeComplete = (RelativeLayout) dPhotos.findViewById(R.id.dialog_photos_relativeComplete);
		RelativeLayout dialog_photos_relativeCancel = (RelativeLayout) dPhotos.findViewById(R.id.dialog_photos_relativeCancel);
	
		dialog_photos_imageGrid.setNumColumns(3);
		dialog_photos_imageGrid.setFocusable(false);
		dialog_photos_imageGrid.setExpanded(true);
		
		//thisList.PathList.addAll(list.PathList);
		
		for(int i=0; i<list.PathList.size();i++)
		{
			if(list.PathList.get(i).photoType.contains("1"))
			{
				thisList.PathList.add(list.PathList.get(i));
			}
		}
		
		
		
		if(thisList.PathList.size()>0)
		{
			adapter.setItems(thisList);
			dialog_photos_imageGrid.setAdapter(adapter);
			
			dialog_photos_relativeInfo.setVisibility(View.GONE);
			dialog_photos_relativeComplete.setVisibility(View.GONE);
			dialog_photos_relativeCancel.setVisibility(View.GONE);
			dialog_photos_ProgressBar.setVisibility(View.GONE);
			dialog_photos_ProgressBar2.setVisibility(View.GONE);
			
			dialog_photos_imageGrid.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view,int position, long id) {
					new setDeafultImageTask().execute(thisList.PathList.get(position).photoId);
					dPhotos.cancel();
				}
			});
			dPhotos.show();
		}
		else
		{
			Toast.makeText(getActivity(), "Not public photos", Toast.LENGTH_LONG ).show();
		}
	}

	
	
	
	
	
}

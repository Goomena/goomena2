package com.goomena.fragments;

import com.goomena.app.R;
import com.goomena.app.tools.Utils;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

@SuppressLint({ "NewApi", "ValidFragment" }) 
public class FragModeSettings extends Fragment
{
	SharedPreferences settings;
	RelativeLayout modesettingsRelativeModeSave;
	ImageView modesettingsImageMode;
	TextView modesettingsTextTitle1,modesettingsTextTitle2;
	Boolean SoundOn = true;
	ImageView modesettingsImageOnOff;
	RelativeLayout modesettingsRelativeBall_1,modesettingsRelativeBall_2,modesettingsRelativeBall_3;
	ImageView modesettingsImageball_1,modesettingsImageball_2,modesettingsImageball_3;
	RelativeLayout modesettingsRelativeLogoG,modesettingsRelativeLogoT;
	ImageView modesettingsImageBallG,modesettingsImageBallT;
	
	
	int _mode;
	
	int Tone=-1;
	int Notif=-1;
	int Appl=-1;
	
	
	
	public FragModeSettings(int mode)
	{
		this._mode = mode;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState) 
	{
		return inflater.inflate(R.layout.fr_mode_settings, container, false);
	}

	public void onActivityCreated (Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);
		
		settings = getActivity().getSharedPreferences(Utils.SharedPrefsKey, 0);
		
		modesettingsRelativeModeSave = (RelativeLayout) getActivity().findViewById(R.id.modesettingsRelativeModeSave);
		modesettingsImageMode = (ImageView) getActivity().findViewById(R.id.modesettingsImageMode);
		modesettingsTextTitle1 = (TextView) getActivity().findViewById(R.id.modesettingsTextTitle1);
		modesettingsTextTitle2 = (TextView) getActivity().findViewById(R.id.modesettingsTextTitle2);
		modesettingsImageOnOff = (ImageView) getActivity().findViewById(R.id.modesettingsImageOnOff);
		modesettingsRelativeBall_1 = (RelativeLayout) getActivity().findViewById(R.id.modesettingsRelativeBall_1);
		modesettingsRelativeBall_2 = (RelativeLayout) getActivity().findViewById(R.id.modesettingsRelativeBall_2);
		modesettingsRelativeBall_3 = (RelativeLayout) getActivity().findViewById(R.id.modesettingsRelativeBall_3);
		modesettingsImageball_1 = (ImageView) getActivity().findViewById(R.id.modesettingsImageball_1);
		modesettingsImageball_2 = (ImageView) getActivity().findViewById(R.id.modesettingsImageball_2);
		modesettingsImageball_3 = (ImageView) getActivity().findViewById(R.id.modesettingsImageball_3);
		modesettingsRelativeLogoG = (RelativeLayout) getActivity().findViewById(R.id.modesettingsRelativeLogoG);
		modesettingsRelativeLogoT = (RelativeLayout) getActivity().findViewById(R.id.modesettingsRelativeLogoT);
		modesettingsImageBallG = (ImageView) getActivity().findViewById(R.id.modesettingsImageBallG);
		modesettingsImageBallT = (ImageView) getActivity().findViewById(R.id.modesettingsImageBallT);
		
		modesettingsRelativeModeSave.setOnClickListener(clickListener);
		modesettingsImageOnOff.setOnClickListener(clickListener);
		modesettingsRelativeBall_1.setOnClickListener(clickListener);
		modesettingsRelativeBall_2.setOnClickListener(clickListener);
		modesettingsRelativeBall_3.setOnClickListener(clickListener);
		modesettingsRelativeLogoG.setOnClickListener(clickListener);
		modesettingsRelativeLogoT.setOnClickListener(clickListener);
		
		
		LoadInfo(_mode);
		
	}
	
	
	
	
	OnClickListener clickListener = new OnClickListener() 
	{
		public void onClick(View v) 
		{
			if(v == modesettingsRelativeModeSave)
			{
				if(_mode==0)
				{
					if(Tone!=-1)
					{
						Editor edit = settings.edit();
						edit.putInt("Mode0Tone", Tone);
						edit.commit();
					}
					if(Notif!=-1)
					{
						Editor edit = settings.edit();
						edit.putInt("Mode0Notif", Notif);
						edit.commit();
					}
					if(Appl!=-1)
					{
						Editor edit = settings.edit();
						edit.putInt("Mode0Appl", Appl);
						edit.commit();
					}
				}
				else if(_mode==1)
				{
					if(Tone!=-1)
					{
						Editor edit = settings.edit();
						edit.putInt("Mode1Tone", Tone);
						edit.commit();
					}
					if(Notif!=-1)
					{
						Editor edit = settings.edit();
						edit.putInt("Mode1Notif", Notif);
						edit.commit();
					}
					if(Appl!=-1)
					{
						Editor edit = settings.edit();
						edit.putInt("Mode1Appl", Appl);
						edit.commit();
					}
				}
				else if(_mode==2)
				{
					if(Tone!=-1)
					{
						Editor edit = settings.edit();
						edit.putInt("Mode2Tone", Tone);
						edit.commit();
					}
					if(Notif!=-1)
					{
						Editor edit = settings.edit();
						edit.putInt("Mode2Notif", Notif);
						edit.commit();
					}
					if(Appl!=-1)
					{
						Editor edit = settings.edit();
						edit.putInt("Mode2Appl", Appl);
						edit.commit();
					}
				}
				else if(_mode==3)
				{
					if(Tone!=-1)
					{
						Editor edit = settings.edit();
						edit.putInt("Mode3Tone", Tone);
						edit.commit();
					}
					if(Notif!=-1)
					{
						Editor edit = settings.edit();
						edit.putInt("Mode3Notif", Notif);
						edit.commit();
					}
					if(Appl!=-1)
					{
						Editor edit = settings.edit();
						edit.putInt("Mode3Appl", Appl);
						edit.commit();
					}
				}
				FragmentManager fm = getFragmentManager();
				fm.popBackStack();
			}
			else if(v == modesettingsImageOnOff)
			{
				if(SoundOn)
				{
					modesettingsImageOnOff.setImageResource(R.drawable.off);
					SoundOn=false;
					Tone=2;
				}
				else
				{
					modesettingsImageOnOff.setImageResource(R.drawable.on);
					SoundOn = true;
					Tone=1;
				}
			}
			else if(v == modesettingsRelativeBall_1)
			{
				modesettingsImageball_1.setImageResource(R.drawable.ds_enable);
				modesettingsImageball_2.setImageResource(R.drawable.ds_disable);
				modesettingsImageball_3.setImageResource(R.drawable.ds_disable);
				Notif = 1;
			}
			else if(v == modesettingsRelativeBall_2)
			{
				modesettingsImageball_1.setImageResource(R.drawable.ds_disable);
				modesettingsImageball_2.setImageResource(R.drawable.ds_enable);
				modesettingsImageball_3.setImageResource(R.drawable.ds_disable);
				Notif = 2;
			}
			else if(v == modesettingsRelativeBall_3)
			{
				modesettingsImageball_1.setImageResource(R.drawable.ds_disable);
				modesettingsImageball_2.setImageResource(R.drawable.ds_disable);
				modesettingsImageball_3.setImageResource(R.drawable.ds_enable);
				Notif = 3;
			}
			else if(v == modesettingsRelativeLogoG)
			{
				modesettingsRelativeLogoG.setBackgroundResource(R.drawable.rectangle_search_white_corners);
				modesettingsRelativeLogoT.setBackgroundResource(R.drawable.rectangle_white_alpha_corners);
				modesettingsImageBallG.setImageResource(R.drawable.ds_enable);
				modesettingsImageBallT.setImageResource(R.drawable.ds_disable);
				Appl=1;
			}
			else if(v == modesettingsRelativeLogoT)
			{
				modesettingsRelativeLogoG.setBackgroundResource(R.drawable.rectangle_white_alpha_corners);
				modesettingsRelativeLogoT.setBackgroundResource(R.drawable.rectangle_search_white_corners);
				modesettingsImageBallG.setImageResource(R.drawable.ds_disable);
				modesettingsImageBallT.setImageResource(R.drawable.ds_enable);
				Appl=2;
			}	
		}
	};
	
	
	
	
	public void  LoadInfo(int mode_)
	{
		if(mode_==0)
		{	
			if(settings.getInt("Mode0Tone",1)==1)
			{
				modesettingsImageOnOff.setImageResource(R.drawable.on);
				SoundOn = true;
			}
			else
			{
				modesettingsImageOnOff.setImageResource(R.drawable.off);
				SoundOn=false;
			}
			
			if(settings.getInt("Mode0Notif",1)==1)
			{
				modesettingsImageball_1.setImageResource(R.drawable.ds_enable);
				modesettingsImageball_2.setImageResource(R.drawable.ds_disable);
				modesettingsImageball_3.setImageResource(R.drawable.ds_disable);
			}
			else if(settings.getInt("Mode0Notif",1)==2)
			{
				modesettingsImageball_1.setImageResource(R.drawable.ds_disable);
				modesettingsImageball_2.setImageResource(R.drawable.ds_enable);
				modesettingsImageball_3.setImageResource(R.drawable.ds_disable);
			}
			else if(settings.getInt("Mode0Notif",1)==3)
			{
				modesettingsImageball_1.setImageResource(R.drawable.ds_disable);
				modesettingsImageball_2.setImageResource(R.drawable.ds_disable);
				modesettingsImageball_3.setImageResource(R.drawable.ds_enable);
			}
			
			
			if(settings.getInt("Mode0Appl",1)==1)
			{
				modesettingsRelativeLogoG.setBackgroundResource(R.drawable.rectangle_search_white_corners);
				modesettingsRelativeLogoT.setBackgroundResource(R.drawable.rectangle_white_alpha_corners);
				modesettingsImageBallG.setImageResource(R.drawable.ds_enable);
				modesettingsImageBallT.setImageResource(R.drawable.ds_disable);
			}
			else
			{
				modesettingsRelativeLogoG.setBackgroundResource(R.drawable.rectangle_white_alpha_corners);
				modesettingsRelativeLogoT.setBackgroundResource(R.drawable.rectangle_search_white_corners);
				modesettingsImageBallG.setImageResource(R.drawable.ds_disable);
				modesettingsImageBallT.setImageResource(R.drawable.ds_enable);
			}
		}
		else if(mode_==1)
		{
			if(settings.getInt("Mode1Tone",1)==1)
			{
				modesettingsImageOnOff.setImageResource(R.drawable.on);
				SoundOn = true;
			}
			else
			{
				modesettingsImageOnOff.setImageResource(R.drawable.off);
				SoundOn=false;
			}
			
			
			if(settings.getInt("Mode1Notif",2)==1)
			{
				modesettingsImageball_1.setImageResource(R.drawable.ds_enable);
				modesettingsImageball_2.setImageResource(R.drawable.ds_disable);
				modesettingsImageball_3.setImageResource(R.drawable.ds_disable);
			}
			else if(settings.getInt("Mode1Notif",2)==2)
			{
				modesettingsImageball_1.setImageResource(R.drawable.ds_disable);
				modesettingsImageball_2.setImageResource(R.drawable.ds_enable);
				modesettingsImageball_3.setImageResource(R.drawable.ds_disable);
			}
			else if(settings.getInt("Mode1Notif",2)==3)
			{
				modesettingsImageball_1.setImageResource(R.drawable.ds_disable);
				modesettingsImageball_2.setImageResource(R.drawable.ds_disable);
				modesettingsImageball_3.setImageResource(R.drawable.ds_enable);
			}
			
			if(settings.getInt("Mode1Appl",1)==1)
			{
				modesettingsRelativeLogoG.setBackgroundResource(R.drawable.rectangle_search_white_corners);
				modesettingsRelativeLogoT.setBackgroundResource(R.drawable.rectangle_white_alpha_corners);
				modesettingsImageBallG.setImageResource(R.drawable.ds_enable);
				modesettingsImageBallT.setImageResource(R.drawable.ds_disable);
			}
			else
			{
				modesettingsRelativeLogoG.setBackgroundResource(R.drawable.rectangle_white_alpha_corners);
				modesettingsRelativeLogoT.setBackgroundResource(R.drawable.rectangle_search_white_corners);
				modesettingsImageBallG.setImageResource(R.drawable.ds_disable);
				modesettingsImageBallT.setImageResource(R.drawable.ds_enable);
			}
		}
		else if(mode_==2)
		{
			if(settings.getInt("Mode2Tone",0)==1)
			{
				modesettingsImageOnOff.setImageResource(R.drawable.on);
				SoundOn = true;
			}
			else
			{
				modesettingsImageOnOff.setImageResource(R.drawable.off);
				SoundOn=false;
			}
			
			
			if(settings.getInt("Mode2Notif",3)==1)
			{
				modesettingsImageball_1.setImageResource(R.drawable.ds_enable);
				modesettingsImageball_2.setImageResource(R.drawable.ds_disable);
				modesettingsImageball_3.setImageResource(R.drawable.ds_disable);
			}
			else if(settings.getInt("Mode2Notif",3)==2)
			{
				modesettingsImageball_1.setImageResource(R.drawable.ds_disable);
				modesettingsImageball_2.setImageResource(R.drawable.ds_enable);
				modesettingsImageball_3.setImageResource(R.drawable.ds_disable);
			}
			else if(settings.getInt("Mode2Notif",3)==3)
			{
				modesettingsImageball_1.setImageResource(R.drawable.ds_disable);
				modesettingsImageball_2.setImageResource(R.drawable.ds_disable);
				modesettingsImageball_3.setImageResource(R.drawable.ds_enable);
			}
			
			if(settings.getInt("Mode2Appl",2)==1)
			{
				modesettingsRelativeLogoG.setBackgroundResource(R.drawable.rectangle_search_white_corners);
				modesettingsRelativeLogoT.setBackgroundResource(R.drawable.rectangle_white_alpha_corners);
				modesettingsImageBallG.setImageResource(R.drawable.ds_enable);
				modesettingsImageBallT.setImageResource(R.drawable.ds_disable);
			}
			else
			{
				modesettingsRelativeLogoG.setBackgroundResource(R.drawable.rectangle_white_alpha_corners);
				modesettingsRelativeLogoT.setBackgroundResource(R.drawable.rectangle_search_white_corners);
				modesettingsImageBallG.setImageResource(R.drawable.ds_disable);
				modesettingsImageBallT.setImageResource(R.drawable.ds_enable);
			}
		}
		else if(mode_==3)
		{
			if(settings.getInt("Mode3Tone",0)==1)
			{
				modesettingsImageOnOff.setImageResource(R.drawable.on);
				SoundOn = true;
			}
			else
			{
				modesettingsImageOnOff.setImageResource(R.drawable.off);
				SoundOn=false;
			}
			
			if(settings.getInt("Mode3Notif",3)==1)
			{
				modesettingsImageball_1.setImageResource(R.drawable.ds_enable);
				modesettingsImageball_2.setImageResource(R.drawable.ds_disable);
				modesettingsImageball_3.setImageResource(R.drawable.ds_disable);
			}
			else if(settings.getInt("Mode3Notif",3)==2)
			{
				modesettingsImageball_1.setImageResource(R.drawable.ds_disable);
				modesettingsImageball_2.setImageResource(R.drawable.ds_enable);
				modesettingsImageball_3.setImageResource(R.drawable.ds_disable);
			}
			else if(settings.getInt("Mode3Notif",3)==3)
			{
				modesettingsImageball_1.setImageResource(R.drawable.ds_disable);
				modesettingsImageball_2.setImageResource(R.drawable.ds_disable);
				modesettingsImageball_3.setImageResource(R.drawable.ds_enable);
			}
			
			if(settings.getInt("Mode3Appl",2)==1)
			{
				modesettingsRelativeLogoG.setBackgroundResource(R.drawable.rectangle_search_white_corners);
				modesettingsRelativeLogoT.setBackgroundResource(R.drawable.rectangle_white_alpha_corners);
				modesettingsImageBallG.setImageResource(R.drawable.ds_enable);
				modesettingsImageBallT.setImageResource(R.drawable.ds_disable);
			}
			else
			{
				modesettingsRelativeLogoG.setBackgroundResource(R.drawable.rectangle_white_alpha_corners);
				modesettingsRelativeLogoT.setBackgroundResource(R.drawable.rectangle_search_white_corners);
				modesettingsImageBallG.setImageResource(R.drawable.ds_disable);
				modesettingsImageBallT.setImageResource(R.drawable.ds_enable);
			}
		}
	}
}

package com.goomena.fragments;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;



import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.goomena.app.ActHome;
import com.goomena.app.ActionBarChange;
import com.goomena.app.R;
import com.goomena.app.adapters.FullScreenProfileAdapter;
import com.goomena.app.data.ServiceClient;
import com.goomena.app.models.ImagePath;
import com.goomena.app.models.UserProfile;
import com.goomena.app.models.UserProfilesList;
import com.goomena.app.tools.Log;
import com.goomena.app.tools.Utils;

@SuppressLint({ "NewApi", "ValidFragment" }) 
public class FragProfile extends Fragment
{
	Activity act;
	ArrayList<String> imageUrls;
	int index;
	private FullScreenProfileAdapter adapter;
	private ViewPager viewPager;
	ArrayList<UserProfile> userProfiles;
	ArrayList<UserProfile> userProfilesFull;
	String _onString = "0";
	Dialog Dwaiting;
	Animation animFadein;
	int _flag = 0;
	int _CurrentItem;
	
	private final static String TAG_FRAGMENT = "TAG_FRAGMENT";
	
	public FragProfile(int flag,ArrayList<UserProfile> ListProfilesFull,int CurrentItem)
	{
		this._flag = flag;
		this.userProfilesFull = ListProfilesFull;
		this._CurrentItem = CurrentItem;
	}
	
	public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState) 
	{
		return inflater.inflate(R.layout.fr_profile, container, false);
	}
	
	@SuppressWarnings("deprecation")
	public void onActivityCreated (Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);
		act = getActivity();
	
	//	ActionBarChange.changeActionBar(getActivity(), 1);
		Log.v("APP", "dagadgwqerg Created");
		
		viewPager = (ViewPager) act.findViewById(R.id.view_pager);
		//viewPager.setAccessibilityLiveRegion(4);
		viewPager.setPageMargin(-(getActivity().getWindowManager().getDefaultDisplay().getWidth()/22)); //htan 50
		//viewPager.setHorizontalFadingEdgeEnabled(true);
		//viewPager.setFadingEdgeLength(30);
		
		
		
		if(_flag==1)
		{
			if(Utils.isNetworkConnected(getActivity()))
			{
				userProfilesFull = new ArrayList<UserProfile>();
				new getProfilesTask().execute();
				viewPager.setCurrentItem(0);
			}
			else
			{
				Utils.showNoInternet(getActivity());
			}
		}
		else if(_flag==2)
		{
			//userProfilesFull = new ArrayList<UserProfile>();
			//userProfilesFull.add(_userProfile);
			//adapter.setItems(userProfilesFull);	
			//adapter.notifyDataSetChanged();
			adapter = new FullScreenProfileAdapter(act,userProfilesFull);
			
			viewPager.setAdapter(adapter);
			
			
			viewPager.setCurrentItem(_CurrentItem);
		}
		
		
		
		
		
		
	}
	
	
	
	
	public ViewPager getViewPager()
	{
		return viewPager;
	}
	
	public void setViewPager(int i)
	{
		viewPager.setCurrentItem(i);
	}
	
	
	
	
	
	class getProfilesTask extends AsyncTask<Void, Void, Integer>
	{
		@Override
		protected void onPreExecute()
		{
			Dwaiting.show();
		}
	
		@Override
		protected Integer doInBackground(Void... params)
		{
			UserProfilesList list = ServiceClient.getNewMembers(Utils.getSharedPreferences(getActivity()).getString("userid", "") ,String.valueOf(userProfilesFull.size()),
			 "15",Utils.getSharedPreferences(getActivity()).getString("genderid",""), _onString, Utils.getSharedPreferences(getActivity()).getString("UserCountry",""));
	 
			if(list!=null && list.Profiles!=null)
			{
				userProfiles = list.Profiles;
				return 1;
			}
			return 0;
		}

		@Override
		protected void onPostExecute(Integer result)
		{
			if(isAdded())
			{
				if(result==1)
				{
					userProfilesFull.addAll(userProfiles);
					//adapter.setItems(userProfilesFull);	
					//adapter.notifyDataSetChanged();
					adapter = new FullScreenProfileAdapter(act,userProfilesFull);
					viewPager.setAdapter(adapter);
					
					viewPager.setCurrentItem(0);
					
					
					
				}
				Dwaiting.cancel();
			}
		}
	}

}
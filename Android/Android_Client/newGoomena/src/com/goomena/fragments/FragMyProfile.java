package com.goomena.fragments;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.goomena.app.ActGoogleMap;
import com.goomena.app.ActHome;
import com.goomena.app.ActionBarChange;
import com.goomena.app.R;
import com.goomena.app.adapters.ObservableScrollView;
import com.goomena.app.adapters.ObservableScrollView.ScrollViewListener;
import com.goomena.app.data.ServiceClient;
import com.goomena.app.global.Global;
import com.goomena.app.models.UserDetails;
import com.goomena.app.models.UserProfile;
import com.goomena.app.tools.Log;
import com.goomena.app.tools.Utils;

@SuppressLint("NewApi") 
public class FragMyProfile extends Fragment
{	
	private final static String TAG_FRAGMENT = "TAG_FRAGMENT";
	UserProfile _userProfiles;
	
	ObservableScrollView fr_my_profile_scroll;
	LinearLayout fr_my_profile_linearMenu;
	
	ImageView fr_my_profile_imageProfile;
	TextView fr_my_profile_textUsername,fr_my_profile_textAge,fr_my_profile_textLocation;
	
	ImageView fr_my_profile_imageVip,fr_my_profile_imageSmoke,fr_my_profile_imageDrink,fr_my_profile_imageZodio,fr_my_profile_imageTravel,fr_my_profile_imageStatus_small;
	
	TextView Profile_textabout,Profile_textage3,Profile_textzodiac3,Profile_textbodytype,Profile_texthaireyes,Profile_texteducation,Profile_textchilds,Profile_textnation,Profile_textreligion,Profile_textsmoke,Profile_textdrink,Profile_textjob;
    ImageView Profile_imageStatus,Profile_imageMeet;
    TextView Profile_textStatus,Profile_textMeet,Profile_textInterested1,Profile_textInterested2,Profile_textInterested3,Profile_textInterested4,Profile_textInterested5,Profile_textInterested6;
    TextView Profile_textTitleaboutme,Profile_textaboutme,Profile_textTitleaboutimaginefirst,Profile_textaboutimaginefirst;
    
    ImageView Profile_imageMap;
    TextView Profile_textMap;
    
    RelativeLayout fr_my_profile_relativeMyphoto,fr_my_profile_relativeAddphoto,fr_my_profile_relativeEditProfile;
    RelativeLayout fr_my_profile_relativeMyphotoUP,fr_my_profile_relativeAddphotoUP,fr_my_profile_relativeEditProfileUP;
	
    FragLoading fragLoading;
    
	public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState) 
	{
		return inflater.inflate(R.layout.fr_my_profile, container, false);
	}
	
	public void onActivityCreated (Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);
		
		
		//ActionBarChange.changeActionBar(getActivity(), 1);
		
		
		fragLoading = new FragLoading(getActivity()); 
		
		_userProfiles = new UserProfile();
		
		
		_userProfiles.UserName = Utils.getSharedPreferences(getActivity()).getString("username", "");
		_userProfiles.ProfileId = Utils.getSharedPreferences(getActivity()).getString("userid", "");
		_userProfiles.GenderId = Utils.getSharedPreferences(getActivity()).getString("genderid", "");
		
		_userProfiles.Latitude = Utils.getSharedPreferences(getActivity()).getString("lat", "");
		_userProfiles.Longtitude = Utils.getSharedPreferences(getActivity()).getString("lng", "");
		
		
		
		fr_my_profile_scroll = (ObservableScrollView) getActivity().findViewById(R.id.fr_my_profile_scroll);
		fr_my_profile_linearMenu = (LinearLayout) getActivity().findViewById(R.id.fr_my_profile_linearMenu);
        
		fr_my_profile_imageProfile = (ImageView) getActivity().findViewById(R.id.fr_my_profile_imageProfile);
		fr_my_profile_textUsername = (TextView) getActivity().findViewById(R.id.fr_my_profile_textUsername);
		fr_my_profile_textAge = (TextView) getActivity().findViewById(R.id.fr_my_profile_textAge);
		fr_my_profile_textLocation = (TextView) getActivity().findViewById(R.id.fr_my_profile_textLocation);
		
		fr_my_profile_imageVip = (ImageView) getActivity().findViewById(R.id.fr_my_profile_imageVip);
		fr_my_profile_imageSmoke = (ImageView) getActivity().findViewById(R.id.fr_my_profile_imageSmoke);
		fr_my_profile_imageDrink = (ImageView) getActivity().findViewById(R.id.fr_my_profile_imageDrink);
		fr_my_profile_imageZodio = (ImageView) getActivity().findViewById(R.id.fr_my_profile_imageZodio);
		fr_my_profile_imageTravel = (ImageView) getActivity().findViewById(R.id.fr_my_profile_imageTravel);
		fr_my_profile_imageStatus_small = (ImageView) getActivity().findViewById(R.id.fr_my_profile_imageStatus_small);
		
		Profile_textabout = (TextView) getActivity().findViewById(R.id.Profile_textabout);
        Profile_textage3 = (TextView) getActivity().findViewById(R.id.Profile_textage3);
        Profile_textzodiac3 = (TextView) getActivity().findViewById(R.id.Profile_textzodiac3);
        Profile_textbodytype = (TextView) getActivity().findViewById(R.id.Profile_textbodytype);
        Profile_texthaireyes = (TextView) getActivity().findViewById(R.id.Profile_texthaireyes);
        Profile_texteducation = (TextView) getActivity().findViewById(R.id.Profile_texteducation);
        Profile_textchilds = (TextView) getActivity().findViewById(R.id.Profile_textchilds);
        Profile_textnation = (TextView) getActivity().findViewById(R.id.Profile_textnation);
        Profile_textreligion = (TextView) getActivity().findViewById(R.id.Profile_textreligion);
        Profile_textsmoke = (TextView) getActivity().findViewById(R.id.Profile_textsmoke);
        Profile_textdrink = (TextView) getActivity().findViewById(R.id.Profile_textdrink);
        Profile_textjob = (TextView) getActivity().findViewById(R.id.Profile_textjob);
        
        Profile_imageStatus = (ImageView) getActivity().findViewById(R.id.Profile_imageStatus);
        Profile_imageMeet = (ImageView) getActivity().findViewById(R.id.Profile_imageMeet);
        Profile_textStatus = (TextView) getActivity().findViewById(R.id.Profile_textStatus);
        Profile_textMeet = (TextView) getActivity().findViewById(R.id.Profile_textMeet);
        Profile_textInterested1 = (TextView) getActivity().findViewById(R.id.Profile_textInterested1);
        Profile_textInterested2 = (TextView) getActivity().findViewById(R.id.Profile_textInterested2);
        Profile_textInterested3 = (TextView) getActivity().findViewById(R.id.Profile_textInterested3);
        Profile_textInterested4 = (TextView) getActivity().findViewById(R.id.Profile_textInterested4);
        Profile_textInterested5 = (TextView) getActivity().findViewById(R.id.Profile_textInterested5);
        Profile_textInterested6 = (TextView) getActivity().findViewById(R.id.Profile_textInterested6);
        Profile_textTitleaboutme = (TextView) getActivity().findViewById(R.id.Profile_textTitleaboutme);
        Profile_textaboutme = (TextView) getActivity().findViewById(R.id.Profile_textaboutme);
        Profile_textTitleaboutimaginefirst = (TextView) getActivity().findViewById(R.id.Profile_textTitleaboutimaginefirst);
        Profile_textaboutimaginefirst = (TextView) getActivity().findViewById(R.id.Profile_textaboutimaginefirst);
        Profile_imageMap = (ImageView) getActivity().findViewById(R.id.Profile_imageMap);
        Profile_textMap = (TextView) getActivity().findViewById(R.id.Profile_textMap);
        
        fr_my_profile_relativeMyphoto = (RelativeLayout) getActivity().findViewById(R.id.fr_my_profile_relativeMyphoto);
        fr_my_profile_relativeAddphoto = (RelativeLayout) getActivity().findViewById(R.id.fr_my_profile_relativeAddphoto);
        fr_my_profile_relativeEditProfile = (RelativeLayout) getActivity().findViewById(R.id.fr_my_profile_relativeEditProfile);
        
        fr_my_profile_relativeMyphotoUP = (RelativeLayout) getActivity().findViewById(R.id.fr_my_profile_relativeMyphotoUP);
        fr_my_profile_relativeAddphotoUP = (RelativeLayout) getActivity().findViewById(R.id.fr_my_profile_relativeAddphotoUP);
        fr_my_profile_relativeEditProfileUP = (RelativeLayout) getActivity().findViewById(R.id.fr_my_profile_relativeEditProfileUP);
        
     
        fr_my_profile_scroll.setScrollViewListener(new ScrollViewListener() {
        	int t=0,u=0;
			@Override
			public void onScrollStopped() {}
			@SuppressLint("NewApi") 
			@Override
			public void onScrollChanged(ScrollView scrollView, int x, int y, int oldx,int oldy) {
				// TODO Auto-generated method stub
				if(y==0)
				{
					fr_my_profile_linearMenu.setTranslationY(0);
					t=0;
					u=0;
				}
				else
				{
					if(y > oldy)
					{
						u = u + (y-oldy);
						fr_my_profile_linearMenu.setTranslationY(u);
						t=200;
					}
					else if(oldy > y)
					{
						t = t - (oldy-y) ;
						if(t>0)
						{
							fr_my_profile_linearMenu.setTranslationY(t);
						}
						else
						{
							t=0;
							fr_my_profile_linearMenu.setTranslationY(0);
						}
						u=t;
					}
				}
			}
		});
        
        
		
		
	//	Utils.getImageLoader(getActivity()).displayImage(Utils.getSharedPreferences(getActivity()).getString("imageurl", "").replace("/d150", ""), fr_my_profile_imageProfile, Utils.getImageOptions());
		//fr_my_profile_textUsername.setText(Utils.getSharedPreferences(getActivity()).getString("username", ""));
		
		//fr_my_profile_textAge.setText(getActivity().getResources().getText(R.string.age)+" "+Utils.getSharedPreferences(getActivity()).getString("userage", "")+" "+getActivity().getResources().getText(R.string.years));
		//fr_my_profile_textLocation.setText(getActivity().getResources().getText(R.string.from)+" "+Utils.getSharedPreferences(getActivity()).getString("usercity", "")+", "+Utils.getSharedPreferences(getActivity()).getString("userregion", "")+", "+Utils.getSharedPreferences(getActivity()).getString("UserCountry", ""));

		
        new getUserProfileTask().execute();
		new getDetailsTask(_userProfiles,Profile_textabout,Profile_textage3,Profile_textzodiac3,Profile_textbodytype,Profile_texthaireyes,Profile_texteducation,Profile_textchilds,Profile_textnation,Profile_textreligion,Profile_textsmoke,Profile_textdrink,Profile_textjob,
        		Profile_imageStatus,Profile_imageMeet,
				Profile_textStatus,Profile_textMeet,Profile_textInterested1,Profile_textInterested2,Profile_textInterested3,Profile_textInterested4,Profile_textInterested5,Profile_textInterested6,Profile_textaboutme,Profile_textaboutimaginefirst).execute();
		
		/*
		 Profile_imageMap.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent intent = new Intent();
					intent.setClass(getActivity(), ActGoogleMap.class);
					intent.putExtra("UserName", _userProfiles.UserName);
					intent.putExtra("Lat", Double.parseDouble(_userProfiles.Latitude));
					intent.putExtra("Lng",Double.parseDouble( _userProfiles.Longtitude));
					
					Log.v("APP", "yyyt "+_userProfiles.Latitude);
					Log.v("APP", "yyyt "+_userProfiles.Longtitude);
					getActivity().startActivity(intent);
					
					
				}
			});
			*/
		 
		 
		 
		 
		 fr_my_profile_relativeMyphoto.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Fragment fr = new FragMyPhoto();
				FragmentManager fm = getActivity().getFragmentManager();
				
		         FragmentTransaction fragmentTransaction = fm.beginTransaction();
		         fragmentTransaction.setCustomAnimations(R.anim.left_slidein_list_animator, R.anim.left_slideout_list_animator,R.anim.right_slidein_list_animator, R.anim.right_slideout_list_animator);
		         fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
		         fragmentTransaction.addToBackStack(null);
		
		         fragmentTransaction.commit();

			}
		});
		 
		 fr_my_profile_relativeAddphoto.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Fragment fr = new FragAddPhoto();
				FragmentManager fm = getActivity().getFragmentManager();
				
		         FragmentTransaction fragmentTransaction = fm.beginTransaction();
		         fragmentTransaction.setCustomAnimations(R.anim.left_slidein_list_animator, R.anim.left_slideout_list_animator,R.anim.right_slidein_list_animator, R.anim.right_slideout_list_animator);
		         fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
		         fragmentTransaction.addToBackStack(null);
		
		         fragmentTransaction.commit();
			}
		});
		
		 fr_my_profile_relativeEditProfile.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					
					Fragment fr = new FragEditProfile();
					FragmentManager fm = getActivity().getFragmentManager();
					
			         FragmentTransaction fragmentTransaction = fm.beginTransaction();
			         fragmentTransaction.setCustomAnimations(R.anim.left_slidein_list_animator, R.anim.left_slideout_list_animator,R.anim.right_slidein_list_animator, R.anim.right_slideout_list_animator);
			         fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
			         fragmentTransaction.addToBackStack(null);
			
			         fragmentTransaction.commit();
				}
			});
		 
		 
		 fr_my_profile_relativeMyphotoUP.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Fragment fr = new FragMyPhoto();
					FragmentManager fm = getActivity().getFragmentManager();
					
			         FragmentTransaction fragmentTransaction = fm.beginTransaction();
			         fragmentTransaction.setCustomAnimations(R.anim.left_slidein_list_animator, R.anim.left_slideout_list_animator,R.anim.right_slidein_list_animator, R.anim.right_slideout_list_animator);
			         fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
			         fragmentTransaction.addToBackStack(null);
			
			         fragmentTransaction.commit();

				}
			});
			 
			 fr_my_profile_relativeAddphotoUP.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					
					Fragment fr = new FragAddPhoto();
					FragmentManager fm = getActivity().getFragmentManager();
					
			         FragmentTransaction fragmentTransaction = fm.beginTransaction();
			         fragmentTransaction.setCustomAnimations(R.anim.left_slidein_list_animator, R.anim.left_slideout_list_animator,R.anim.right_slidein_list_animator, R.anim.right_slideout_list_animator);
			         fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
			         fragmentTransaction.addToBackStack(null);
			
			         fragmentTransaction.commit();
				}
			});
			
			 fr_my_profile_relativeEditProfileUP.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						
						Fragment fr = new FragEditProfile();
						FragmentManager fm = getActivity().getFragmentManager();
						
				         FragmentTransaction fragmentTransaction = fm.beginTransaction();
				         fragmentTransaction.setCustomAnimations(R.anim.left_slidein_list_animator, R.anim.left_slideout_list_animator,R.anim.right_slidein_list_animator, R.anim.right_slideout_list_animator);
				         fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
				         fragmentTransaction.addToBackStack(null);
				
				         fragmentTransaction.commit();
					}
				});
		 
		 
		 
		
	}
	
	
	class getUserProfileTask extends AsyncTask<Void, Integer, UserProfile>
	{
		@Override
		protected void onPreExecute()
		{
			fragLoading.Show();
		}

		@Override
		protected UserProfile doInBackground(Void... params) 
		{		// Global.Lang
			UserProfile details;
			details = ServiceClient.getProfile(_userProfiles.ProfileId);			
			return details;
		}
		@Override
		protected void onPostExecute(final UserProfile result)
		{
			if(isAdded())
			{
				Utils.getImageLoader(getActivity()).displayImage(result.ImageUrl.replace("/d150", ""), fr_my_profile_imageProfile, Utils.getImageOptions());
				Utils.getSharedPreferences(getActivity()).edit().putString("imageurl", result.ImageUrl).commit();
				
				fr_my_profile_textUsername.setText(result.UserName.toString());
				
				fr_my_profile_textAge.setText(getActivity().getResources().getText(R.string.age)+" "+result.UserAge.toString()+" "+getActivity().getResources().getText(R.string.years));
				fr_my_profile_textLocation.setText(getActivity().getResources().getText(R.string.from)+" "+result.UserCity.toString()+", "+result.UserRegion+", "+result.UserCountry);

				Utils.getSharedPreferences(getActivity()).edit().putString("lat", result.Latitude).commit();
				Utils.getSharedPreferences(getActivity()).edit().putString("lng", result.Longtitude).commit();
				Profile_textMap.setText(result.UserName + " " + getActivity().getResources().getString(R.string.on_map));
				Profile_imageMap.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						Intent intent = new Intent();
						intent.setClass(getActivity(), ActGoogleMap.class);
						intent.putExtra("UserName", result.UserName);
						intent.putExtra("Lat", Double.parseDouble(result.Latitude));
						intent.putExtra("Lng",Double.parseDouble( result.Longtitude));
						
						getActivity().startActivity(intent);
						
						
					}
				});
			}
		}
		
	}
	
	
	class getDetailsTask extends AsyncTask<Void, Integer, UserDetails>
	{
		UserProfile _userprofile;
		TextView Profile_textabout,Profile_textage3,Profile_textzodiac3;
		TextView Profile_textbodytype,Profile_texthaireyes,Profile_texteducation,Profile_textchilds,Profile_textnation,Profile_textreligion,Profile_textsmoke,Profile_textdrink,Profile_textjob;
		ImageView Profile_imageStatus,Profile_imageMeet;
        TextView Profile_textStatus,Profile_textMeet,Profile_textInterested1,Profile_textInterested2,Profile_textInterested3,Profile_textInterested4,Profile_textInterested5,Profile_textInterested6;
        TextView Profile_textaboutme;
        TextView Profile_textaboutimaginefirst;
        
        
		public getDetailsTask(UserProfile userprofile,TextView Profile_textabout,TextView Profile_textage3,TextView Profile_textzodiac3,TextView Profile_textbodytype,TextView Profile_texthaireyes,TextView Profile_texteducation,TextView Profile_textchilds,TextView Profile_textnation,TextView Profile_textreligion,TextView Profile_textsmoke,TextView Profile_textdrink,TextView Profile_textjob,
				ImageView Profile_imageStatus,ImageView Profile_imageMeet,
				TextView Profile_textStatus,TextView Profile_textMeet,TextView Profile_textInterested1,TextView Profile_textInterested2,TextView Profile_textInterested3,TextView Profile_textInterested4,TextView Profile_textInterested5,TextView Profile_textInterested6,
				TextView Profile_textaboutme,
				TextView Profile_textaboutimaginefirst)
		{
			this._userprofile = userprofile;
			this.Profile_textabout = Profile_textabout;
			this.Profile_textage3 = Profile_textage3;
			this.Profile_textzodiac3 = Profile_textzodiac3;
			this.Profile_textbodytype = Profile_textbodytype;
			this.Profile_texthaireyes = Profile_texthaireyes;
			this.Profile_texteducation = Profile_texteducation;
			this.Profile_textchilds = Profile_textchilds;
			this.Profile_textnation = Profile_textnation;
			this.Profile_textreligion = Profile_textreligion;
			this.Profile_textsmoke = Profile_textsmoke;
			this.Profile_textdrink = Profile_textdrink;
			this.Profile_textjob = Profile_textjob;
			
			this.Profile_imageStatus = Profile_imageStatus;
			this.Profile_imageMeet = Profile_imageMeet;
			this.Profile_textStatus = Profile_textStatus;
			this.Profile_textMeet = Profile_textMeet;
			this.Profile_textInterested1 = Profile_textInterested1;
			this.Profile_textInterested2 = Profile_textInterested2;
			this.Profile_textInterested3 = Profile_textInterested3;
			this.Profile_textInterested4 = Profile_textInterested4;
			this.Profile_textInterested5 = Profile_textInterested5;
			this.Profile_textInterested6 = Profile_textInterested6;
			this.Profile_textaboutme = Profile_textaboutme;
			this.Profile_textaboutimaginefirst = Profile_textaboutimaginefirst;
		}
		
		@Override
		protected void onPreExecute()
		{
		}

		@Override
		protected UserDetails doInBackground(Void... params) 
		{		// Global.Lang
			UserDetails details;
			details = ServiceClient.getUserDetails(_userprofile.ProfileId,Global.Lang);			
			return details;
		}
		@Override
		protected void onPostExecute(UserDetails details)
		{
			fragLoading.Close();
			if(details!=null)
			{
				try 
				{
					if(details.zodiac.contains("Gemini"))
					{
						fr_my_profile_imageZodio.setBackgroundResource(R.drawable.p_gemini);
					}
					else if(details.zodiac.contains("Aries"))
					{
						fr_my_profile_imageZodio.setBackgroundResource(R.drawable.p_aries);
					}
					else if(details.zodiac.contains("Aquarius"))
					{
						fr_my_profile_imageZodio.setBackgroundResource(R.drawable.p_aruarius);
					}
					else if(details.zodiac.contains("Cancer"))
					{
						fr_my_profile_imageZodio.setBackgroundResource(R.drawable.p_cancer);
					}
					else if(details.zodiac.contains("Capricorn"))
					{
						fr_my_profile_imageZodio.setBackgroundResource(R.drawable.p_capricorn);
					}
					else if(details.zodiac.contains("Leo"))
					{
						fr_my_profile_imageZodio.setBackgroundResource(R.drawable.p_leo);
					}
					else if(details.zodiac.contains("Libra"))
					{
						fr_my_profile_imageZodio.setBackgroundResource(R.drawable.p_libra);
					}
					else if(details.zodiac.contains("Pisces"))
					{
						fr_my_profile_imageZodio.setBackgroundResource(R.drawable.p_pisces);
					}
					else if(details.zodiac.contains("Sagittarius"))
					{
						fr_my_profile_imageZodio.setBackgroundResource(R.drawable.p_sagittarius);
					}
					else if(details.zodiac.contains("Scorpio"))
					{
						fr_my_profile_imageZodio.setBackgroundResource(R.drawable.p_scorpio);
					}
					else if(details.zodiac.contains("Taurus"))
					{
						fr_my_profile_imageZodio.setBackgroundResource(R.drawable.p_taurus);
					}
					else if(details.zodiac.contains("Virgo"))
					{
						fr_my_profile_imageZodio.setBackgroundResource(R.drawable.p_virgo);
					}
					else
					{
						fr_my_profile_imageZodio.setVisibility(View.GONE);
					}
					
					if(_userprofile.GenderId.equals("1"))
					{
						fr_my_profile_imageTravel.setBackgroundResource(R.drawable.p_men_travel);
						fr_my_profile_imageStatus_small.setBackgroundResource(R.drawable.p_men_single);
					}
					else
					{
						fr_my_profile_imageTravel.setBackgroundResource(R.drawable.p_woman_travel);
						fr_my_profile_imageStatus_small.setBackgroundResource(R.drawable.p_woman_single);
					}
					
					
					Profile_textabout.setText(_userprofile.UserName);
					Profile_textage3.setText(details.age);
					Profile_textzodiac3.setText("("+details.zodiac+")");	
					Profile_textbodytype.setText(details.bodytype);
					Profile_texthaireyes.setText(details.haireyes);
					Profile_texteducation.setText(details.education);
					Profile_textchilds.setText(details.childs);
					Profile_textnation.setText(details.nationality);
					Profile_textreligion.setText(details.religion);
					String[] result = details.smokedring.split("/");
					if(result.length > 0)
					{
						Profile_textsmoke.setText(result[0]);
						Profile_textdrink.setText(result[1]);
						
						if(result[0].equals("Μη καπνιστής") || result[0].equals("Non-Smoker") || result[0].equals("Sigara kullanmıyorum") || result[0].equals("No fuma"))
						{
							fr_my_profile_imageSmoke.setBackgroundResource(R.drawable.p_no_smoke);
						}
						else
						{
							fr_my_profile_imageSmoke.setBackgroundResource(R.drawable.p_smoke);
						}
						
						if(result[1].equals("Όχι") || result[1].equals("Non-Drinker") || result[1].equals("Alkol kullanmıyorum")|| result[1].equals("No bebedor"))
						{
							fr_my_profile_imageDrink.setBackgroundResource(R.drawable.p_no_drink);
						}
						else
						{
							fr_my_profile_imageDrink.setBackgroundResource(R.drawable.p_drink);
						}
					}
					Profile_textjob.setText(details.job);
					
					Profile_textStatus.setText(details.familystate);
					Profile_textMeet.setText(details.lookfor);
					
					if(details.familystate.equals("Μόνος/η") || details.familystate.equals("Single") || details.familystate.equals("Solo/a"))
					{
						if(_userprofile.GenderId.equals("1"))
						{
							Profile_imageStatus.setImageResource(R.drawable.single_men_big);
							fr_my_profile_imageStatus_small.setImageResource(R.drawable.p_men_single);
						}
						else
						{
							//na mou to steilei o andreas
							Profile_imageStatus.setImageResource(R.drawable.famele_icon);
							fr_my_profile_imageStatus_small.setImageResource(R.drawable.p_woman_single);
						}
					}
					else
					{
						Profile_imageStatus.setImageResource(R.drawable.couple_big);
						fr_my_profile_imageStatus_small.setImageResource(R.drawable.p_couple);
					}
					
					if(details.lookfor.equals("Άντρας") || details.lookfor.equals("Male") || details.lookfor.equals("Erkek") || details.lookfor.equals("Masculino"))
					{
						Profile_imageMeet.setImageResource(R.drawable.male_icon);
					}
					else
					{
						Profile_imageMeet.setImageResource(R.drawable.famele_icon);
					}
					String[] result2 = details.interest.split("</br>");
					Profile_textInterested1.setText(result2[1]);
					Profile_textInterested2.setText(result2[2]);
					Profile_textInterested3.setText(result2[3]);
					Profile_textInterested4.setText(result2[4]);
					Profile_textInterested5.setText(result2[5]);
					if(result2.length > 6)
					{
						Profile_textInterested6.setText(result2[6]);
					}
					
					
					
					if(details.aboutme_describeyourself.length()!=0)
					{
						Profile_textTitleaboutme.setVisibility(View.VISIBLE);
						Profile_textaboutme.setVisibility(View.VISIBLE);
						Profile_textaboutme.setText(details.aboutme_describeyourself);
					}
					
					
					if(details.aboutme_describedate.length()!=0)
					{
						Profile_textTitleaboutimaginefirst.setVisibility(View.VISIBLE);
						Profile_textaboutimaginefirst.setVisibility(View.VISIBLE);
						Profile_textaboutimaginefirst.setText(details.aboutme_describedate);
					}
					
				} catch (Exception e) {
					// TODO: handle exception
				}
				
				
				
			}	
		}
	}
}
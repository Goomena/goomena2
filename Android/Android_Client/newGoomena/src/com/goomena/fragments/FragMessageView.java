package com.goomena.fragments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.goomena.app.Controller;
import com.goomena.app.Panel;
import com.goomena.app.R;
import com.goomena.app.adapters.ConversationAdapter;
import com.goomena.app.data.DataFetcher;
import com.goomena.app.data.ServiceClient;
import com.goomena.app.models.Message;
import com.goomena.app.models.MessageInfo;
import com.goomena.app.models.MessageList;
import com.goomena.app.models.UserProfile;
import com.goomena.app.tools.Log;
import com.goomena.app.tools.Utils;


@SuppressLint({ "NewApi", "ValidFragment" }) 
public class FragMessageView  extends Fragment
{
	
	
			
	MessageInfo _minfo;
	UserProfile _userProfile = null;
	ConversationAdapter adapter;
	ListView convList;
	
	ImageView fr_message_view_imeage_profile,fr_message_view_imeage_imageOval_yellow;
	TextView fr_message_view_text_username,fr_message_view_text_online,fr_message_view_text_location,fr_message_view_text_age;
	RelativeLayout fr_message_view_relativeLayoutInfo;
	ImageView fr_message_view_image_checkDelete;
	TextView fr_message_view_text_checkDelete;
	EditText fr_message_view_editMessage;
	TextView fr_message_view_textSendMessage;
	
	Animation animFadein;
	
	int _offerid = 0;
	String _levelPrivate=null;
	
	FragLoading fragLoading;
	
	private final static String TAG_FRAGMENT = "TAG_FRAGMENT";
	
	
	private HashMap<String, Integer> emoticons = new HashMap<String, Integer>();
	private ArrayList<String> arrayListSmileys = new ArrayList<String>();
	ImageView imgbtn_show_smileys;
	
	private RelativeLayout emoticonsCover;
	private View popUpView;
	public static PopupWindow popupWindow = null;
	private int keyboardHeight;	
	private boolean isKeyBoardVisible;
	private LinearLayout parentLayout;
	
	ImageView grid_groupBackIcons;
	GridView groupIconsGrid;
	
	String hash = "";
	
	public FragMessageView(MessageInfo minfo,UserProfile userProfile,int offerid,String levelPrivate) 
	{
		// TODO Auto-generated constructor stub
		this._minfo = minfo;
		this._offerid = offerid;
		this._levelPrivate = levelPrivate;
		this._userProfile = userProfile;
	}

	public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState) 
	 {
		emoticons.put(":)", R.drawable.emotic_smile);
		emoticons.put(":-(", R.drawable.emotic_sadsmile);
		emoticons.put(":-D", R.drawable.emotic_bigsmile);
		emoticons.put("8-)", R.drawable.emotic_cool);
		emoticons.put(":-o", R.drawable.emotic_surprised);
		emoticons.put(";-)", R.drawable.emotic_wink);
		emoticons.put(";-(", R.drawable.emotic_crying);
		emoticons.put("(sweat)", R.drawable.emotic_sweating);
		emoticons.put(":-|", R.drawable.emotic_speechless);
		emoticons.put(":-*", R.drawable.emotic_kiss);
		emoticons.put(":-p", R.drawable.emotic_tongueout);
		emoticons.put("(blush)", R.drawable.emotic_blush);
		emoticons.put(":^)", R.drawable.emotic_wondering);
		emoticons.put("(snooze)", R.drawable.emotic_sleepy);
		emoticons.put("|-(", R.drawable.emotic_dull);
		emoticons.put("(inlove)", R.drawable.emotic_inlove);
		emoticons.put("(grin)", R.drawable.emotic_evilgrin);
		emoticons.put("(talk)", R.drawable.emotic_talking);
		emoticons.put("(yawn)", R.drawable.emotic_yawning);
		emoticons.put("(puke)", R.drawable.emotic_puke);
		emoticons.put("(doh)", R.drawable.emotic_doh);
		emoticons.put(":-@", R.drawable.emotic_angry);
		emoticons.put("(wasntme)", R.drawable.emotic_itwasntme);
		emoticons.put("(party)", R.drawable.emotic_party);
		emoticons.put(":-s", R.drawable.emotic_worried);
		emoticons.put("(mm)", R.drawable.emotic_mmm);
		emoticons.put("(nerd)", R.drawable.emotic_nerd);
		emoticons.put(":-x", R.drawable.emotic_lipssealed);
		emoticons.put("(hi)", R.drawable.emotic_hi);
		emoticons.put("(call)", R.drawable.emotic_call);
		emoticons.put("(devil)", R.drawable.emotic_devil);
		emoticons.put("(angel)", R.drawable.emotic_angel);
		emoticons.put("(envy)", R.drawable.emotic_envy);
		emoticons.put("(wait)", R.drawable.emotic_wait);
		emoticons.put("(hug)", R.drawable.emotic_hug);
		emoticons.put("(makeup)", R.drawable.emotic_makeup);
		emoticons.put("(chuckle)", R.drawable.emotic_giggle);
		emoticons.put("(clap)", R.drawable.emotic_clapping);
		emoticons.put("(think)", R.drawable.emotic_thinking);
		emoticons.put("(bow)", R.drawable.emotic_bow);
		emoticons.put("(ok)", R.drawable.emotic_yes);
		emoticons.put("(N)", R.drawable.emotic_no);
		emoticons.put("(handshake)", R.drawable.emotic_handshake);
		emoticons.put("(h)", R.drawable.emotic_heart);
		emoticons.put("(u)", R.drawable.emotic_brokenheart);
		emoticons.put("(m)", R.drawable.emotic_mail);
		emoticons.put("(F)", R.drawable.emotic_flower);
		emoticons.put("(rain)", R.drawable.emotic_rain);
		emoticons.put("(sun)", R.drawable.emotic_sunshine);
		emoticons.put("(time)", R.drawable.emotic_time);
		emoticons.put("(music)", R.drawable.emotic_music);
		emoticons.put("(movie)", R.drawable.emotic_movie);
		emoticons.put("(ph)", R.drawable.emotic_phone);
		emoticons.put("(coffee)", R.drawable.emotic_coffee);
		emoticons.put("(pizza)", R.drawable.emotic_pizza);
		emoticons.put("(cash)", R.drawable.emotic_cash);
		emoticons.put("(flex)", R.drawable.emotic_muscle);
		emoticons.put("(cake)", R.drawable.emotic_cake);
		emoticons.put("(beer)", R.drawable.emotic_beer);
		emoticons.put("(d)", R.drawable.emotic_drink);
		emoticons.put("(dance)", R.drawable.emotic_dancing);
		emoticons.put("(ninja)", R.drawable.emotic_ninja);
		emoticons.put("(*)", R.drawable.emotic_star);
		emoticons.put("(mooning)", R.drawable.emotic_mooning);
		emoticons.put("(finger)", R.drawable.emotic_finger);
		emoticons.put("(bandit)", R.drawable.emotic_bandit);
		emoticons.put("(drunk)", R.drawable.emotic_drunk);
		emoticons.put("(smoking)", R.drawable.emotic_smoking);
		emoticons.put("(toivo)", R.drawable.emotic_toivo);
		emoticons.put("(rock)", R.drawable.emotic_rock);
		emoticons.put("(headbang)", R.drawable.emotic_headbang);
		
		
		
		
		 return inflater.inflate(R.layout.fr_message_view, container, false);
	 }
	
	@SuppressLint("InflateParams")
	public void onActivityCreated (Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);
		
		
		fragLoading = new FragLoading(getActivity());
		
		fr_message_view_relativeLayoutInfo = (RelativeLayout) getActivity().findViewById(R.id.fr_message_view_relativeLayoutInfo);
		fr_message_view_imeage_profile = (ImageView) getActivity().findViewById(R.id.fr_message_view_imeage_profile);
		fr_message_view_imeage_imageOval_yellow = (ImageView) getActivity().findViewById(R.id.fr_message_view_imeage_imageOval_yellow);
		animFadein = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_in);
		fr_message_view_text_username = (TextView) getActivity().findViewById(R.id.fr_message_view_text_username);
		fr_message_view_text_online = (TextView) getActivity().findViewById(R.id.fr_message_view_text_online);
		fr_message_view_text_age = (TextView) getActivity().findViewById(R.id.fr_message_view_text_age);
		fr_message_view_text_location = (TextView) getActivity().findViewById(R.id.fr_message_view_text_location);
		
		fr_message_view_image_checkDelete = (ImageView) getActivity().findViewById(R.id.fr_message_view_image_checkDelete);
		fr_message_view_text_checkDelete = (TextView) getActivity().findViewById(R.id.fr_message_view_text_checkDelete);
		
		fr_message_view_editMessage = (EditText) getActivity().findViewById(R.id.fr_message_view_editMessage);
		fr_message_view_textSendMessage = (TextView) getActivity().findViewById(R.id.fr_message_view_textSendMessage);
		
		imgbtn_show_smileys = (ImageView) getActivity().findViewById(R.id.imgbtn_show_smileys);
		
		convList = (ListView) getActivity().findViewById(R.id.conversationList);
		
		
		emoticonsCover = (RelativeLayout) getActivity().findViewById(R.id.footer_for_emoticons);
		parentLayout = (LinearLayout) getActivity().findViewById(R.id.list_parent);
		
		popUpView = getActivity().getLayoutInflater().inflate(R.layout.group_icons_layout, null);
		grid_groupBackIcons = (ImageView) popUpView.findViewById(R.id.grid_groupBackIcons);
		groupIconsGrid = (GridView) popUpView.findViewById(R.id.grid_groupIcons);
		arrayListSmileys.clear();
		groupIconsGrid.setAdapter(new SmileysAdapter(arrayListSmileys, getActivity(), emoticons));
		
		
		emoticonsCover.setVisibility(RelativeLayout.GONE);
		
		if(_levelPrivate!=null)
		{
			fr_message_view_editMessage.setText(getActivity().getResources().getString(R.string.please_give_me_access_to_your_private)+" "+_levelPrivate+". "
					+ getActivity().getResources().getString(R.string.i_want_to_get_to_know_you_better));
		}
		
		if(_userProfile ==null)
		{
			new getProfileTask(_minfo.ProfileId).execute();
		}
		else
		{
			fr_message_view_imeage_profile.setOnClickListener(new OnClickListener() 
			{		
				@Override
				public void onClick(View v) 
				{
				    InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
				    inputManager.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
				    
				    if (popupWindow.isShowing())
				    {
				    	popupWindow.dismiss();	
				    }
				    
				    
					// TODO Auto-generated method stub
					ArrayList<UserProfile> _userProfilesFull_ = new ArrayList<UserProfile>();
					_userProfilesFull_.add(_userProfile);
					Fragment fr = new FragProfile(2,_userProfilesFull_,0);				
					FragmentManager fm = getActivity().getFragmentManager();
					FragmentTransaction fragmentTransaction = fm.beginTransaction();
					fragmentTransaction.setCustomAnimations(R.anim.left_slidein_list_animator, R.anim.left_slideout_list_animator,R.anim.right_slidein_list_animator, R.anim.right_slideout_list_animator);
					fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
					fragmentTransaction.addToBackStack(null);
					fragmentTransaction.commit();	
				}
			});
			
			
			
			if(_userProfile.ImageUrl.contains("Images2/woman"))
	        {
				_userProfile.ImageUrl = "drawable://" + R.drawable.private_for_woman;
	        }
	        else if(_userProfile.ImageUrl.contains("Images2/men.png"))
	        {
	        	_userProfile.ImageUrl = "drawable://" + R.drawable.private_for_men;
	        }
			
			Utils.getImageLoader(getActivity()).displayImage(_userProfile.ImageUrl,fr_message_view_imeage_profile, Utils.getImageOptions());
				
			fr_message_view_text_username.setText(_userProfile.UserName);
			
			if(_userProfile.IsOnline)
			{
				fr_message_view_text_online.setText("Online");
				fr_message_view_imeage_imageOval_yellow.setBackgroundResource(R.drawable.oval_yellow);
				fr_message_view_imeage_imageOval_yellow.startAnimation(animFadein);
			}
			else if(!_userProfile.IsOnline)
			{
				fr_message_view_text_online.setText("Offline");
				fr_message_view_imeage_imageOval_yellow.setBackgroundResource(R.drawable.oval_red);
				fr_message_view_imeage_imageOval_yellow.clearAnimation();
			}
			else
		    {
				new getUserStateTask(_userProfile.ProfileId,fr_message_view_text_online,fr_message_view_imeage_imageOval_yellow).execute();
			}
				
			
			
			fr_message_view_text_age.setText(" "+_userProfile.UserAge+" ");
			fr_message_view_text_location.setText(" "+_userProfile.UserCity+", "+_userProfile.UserRegion+", "+_userProfile.UserCountry);
			adapter = new ConversationAdapter(getActivity(),null,_userProfile);
			new getConversationWithProfileTask(Utils.getSharedPreferences(getActivity()).getString("userid", ""),_userProfile.ProfileId,adapter).execute();
		
			
		}
			

		if(Utils.getSharedPreferences(getActivity()).getString("genderid", "").equals("2"))
		{
			fr_message_view_editMessage.setHint(getActivity().getResources().getString(R.string.sendmessage));
		}
		
		fr_message_view_textSendMessage.setOnClickListener(new OnClickListener() 
		{	
			@Override
			public void onClick(View v) 
			{
				// TODO Auto-generated method stub
				new sendMessageTask().execute();
			}
		});

		convList.setOnTouchListener(new OnTouchListener() {
			
			@SuppressLint("ClickableViewAccessibility") 
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
			    inputManager.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
			    
			    if (popupWindow.isShowing())
			    {
			    	popupWindow.dismiss();	
			    }
			    
				return false;
			}
		});
		
		fr_message_view_relativeLayoutInfo.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
			    inputManager.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
			    
			    if (popupWindow.isShowing())
			    {
			    	popupWindow.dismiss();	
			    }
				
			}
		});
		
		fr_message_view_editMessage.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				if (popupWindow.isShowing()) {
					
					popupWindow.dismiss();
					
				}
			}
		});
		
		
		convList.setTranscriptMode(ListView.TRANSCRIPT_MODE_NORMAL);
		convList.setStackFromBottom(true);
		
		
		
		// Defining default height of keyboard which is equal to 230 dip
		final float popUpheight = getResources().getDimension(R.dimen.keyboard_height);
		changeKeyboardHeight((int) popUpheight);
		
				
		checkKeyboardHeight(parentLayout);
		fillArrayList();
		
		
		enablePopUpView();
		
		
		
		
		
		imgbtn_show_smileys.setOnClickListener(new View.OnClickListener() {

	        @Override
	        public void onClick(View arg0) 
	        {
	        	if (!popupWindow.isShowing()) 
	        	{
	        		popupWindow.setHeight((int) (keyboardHeight));

					if (isKeyBoardVisible) 
					{
						emoticonsCover.setVisibility(RelativeLayout.GONE);
					} 
					else
					{
						emoticonsCover.setVisibility(RelativeLayout.VISIBLE);
					}
					popupWindow.showAtLocation(parentLayout, Gravity.BOTTOM, 0, 0);
				} 
	        	else
	        	{
	        		popupWindow.dismiss();
				}

	        }
	    });
		
	}
		
	
	
	
	
	
	
	private void enablePopUpView() {

		// Creating a pop window for emoticons keyboard
		popupWindow = new PopupWindow(popUpView, LayoutParams.MATCH_PARENT, (int) keyboardHeight, false);
		
		
		
		grid_groupBackIcons.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				KeyEvent event = new KeyEvent(0, 0, 0, KeyEvent.KEYCODE_DEL, 0, 0, 0, 0, KeyEvent.KEYCODE_ENDCALL);
				fr_message_view_editMessage.dispatchKeyEvent(event);	
			}
		});

		popupWindow.setOnDismissListener(new OnDismissListener() {

			@Override
			public void onDismiss() {
				emoticonsCover.setVisibility(RelativeLayout.GONE);
			}
		});
	}
	
	
	private void fillArrayList() {
	    Iterator<Entry<String, Integer>> iterator = emoticons.entrySet().iterator();
	    while(iterator.hasNext()){
	        Entry<String, Integer> entry = iterator.next();
	        arrayListSmileys.add(entry.getKey());
	    }
	}
	
	public void getSmiledText(Context context, String text) {
        SpannableStringBuilder builder = new SpannableStringBuilder(text);
        int index;
        for (index = 0; index < builder.length(); index++) {
            for (Entry<String, Integer> entry : emoticons.entrySet()) {
                int length = entry.getKey().length();
                if (index + length > builder.length())
                    continue;
                if (builder.subSequence(index, index + length).toString().equals(entry.getKey())) {
                    builder.setSpan(new ImageSpan(context, entry.getValue()), index, index + length,
                            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    index += length - 1;
                    break;
                }
            }
        }
        
        int cursorPosition = fr_message_view_editMessage.getSelectionStart();		
        fr_message_view_editMessage.getText().insert(cursorPosition, builder);
    }
	
	public static void refresh()
	{
		
	}
	
	
	
	
	
	
	class getConversationWithProfileTask extends AsyncTask<Void,Integer,MessageList>
	{
		String _MyId;
		String _UserId;
		ConversationAdapter _adapter;
		MessageList _list;
		
		public getConversationWithProfileTask(String MyId,String UserId, ConversationAdapter adapter)
		{
			this._MyId = MyId;
			this._UserId = UserId;
			this._adapter = adapter;
		}

		@Override
		protected MessageList doInBackground(Void... params) 
		{			
			MessageList conversation = null;			
			conversation = ServiceClient.getConversation_with_profile(_MyId, _UserId);
			return conversation;
		}
		
		@Override
		protected void onPostExecute(MessageList list){
			if(list!=null)
			{
				if(list.list!=null)
				{
					_adapter.setItems(list);
					convList.setAdapter(_adapter);
					convList.setSelection(list.list.size()-1);	
					
					fr_message_view_image_checkDelete.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							if(_adapter.getOpenCheck())
							{
								fr_message_view_image_checkDelete.setVisibility(View.VISIBLE);
								fr_message_view_text_checkDelete.setVisibility(View.GONE);
								
								new deleteMessageTask(_adapter.getCheckedItemsIDs(),_adapter).execute();
								_adapter.setOpenCheck(false);
								_adapter.notifyDataSetChanged();
							}
							else
							{
								fr_message_view_image_checkDelete.setVisibility(View.GONE);
								fr_message_view_text_checkDelete.setVisibility(View.VISIBLE);
								
								_adapter.setOpenCheck(true);
								_adapter.notifyDataSetChanged();
							}
						}
					});
					
					fr_message_view_text_checkDelete.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							if(_adapter.getOpenCheck())
							{
								fr_message_view_image_checkDelete.setVisibility(View.VISIBLE);
								fr_message_view_text_checkDelete.setVisibility(View.GONE);
								
								new deleteMessageTask(_adapter.getCheckedItemsIDs(),_adapter).execute();
								_adapter.setOpenCheck(false);
								_adapter.notifyDataSetChanged();
							}
							else
							{
								fr_message_view_image_checkDelete.setVisibility(View.GONE);
								fr_message_view_text_checkDelete.setVisibility(View.VISIBLE);
								
								_adapter.setOpenCheck(true);
								_adapter.notifyDataSetChanged();
							}
						}
					});
					
				}
				else
				{
					list = new MessageList();
					list.list= new ArrayList<Message>();
					_adapter.setItems(list);
					convList.setAdapter(_adapter);
					convList.setSelection(list.list.size()-1);
				}
			}
		}
	}
	
	
	@SuppressLint("NewApi") 
	class getUserStateTask extends AsyncTask<Void, Integer, String>
	{
		String _userID;
		TextView _fr_message_view_textOnline;
		ImageView _fr_message_view_imageOval_yellow;
		
		public getUserStateTask(String userID,TextView fr_message_view_textOnline,ImageView fr_message_view_imageOval_yellow)
		{
			this._userID = userID;
			this._fr_message_view_textOnline = fr_message_view_textOnline;
			this._fr_message_view_imageOval_yellow = fr_message_view_imageOval_yellow;
		}
		@Override
		protected String doInBackground(Void... params)
		{
			String result = ServiceClient.getUserState(_userID);
			return result;
		}
	
		@Override
		protected void onPostExecute(String res)
		{
			if(res.contains("Online"))
			{
				_fr_message_view_textOnline.setText("Online");
				_fr_message_view_imageOval_yellow.setBackgroundResource(R.drawable.oval_yellow);
				_fr_message_view_imageOval_yellow.startAnimation(animFadein);
			}
			else
			{
				_fr_message_view_textOnline.setText("Offline");
				_fr_message_view_imageOval_yellow.setBackgroundResource(R.drawable.oval_red);
				_fr_message_view_imageOval_yellow.clearAnimation();
			}
		}
	}
	
	
	
	
	
	
	
	class sendMessageTask extends AsyncTask<Void,Integer,String>
	{
		String sedmessagetext;
		@Override
		protected void onPreExecute()
		{
			sedmessagetext = fr_message_view_editMessage.getText().toString();
			fr_message_view_editMessage.setText("");
		}
		
		@Override
		protected String doInBackground(Void... params) 
		{
			//Global.credits=Global.credits-50;
			String result = "";
			if(sedmessagetext.length()!=0)
			{
				//add subject!!!!	
				if(_userProfile.ProfileId!=null)
				{	
					String base_url = "http://188.165.13.64/testandroid2014/GoomenaLib.Service1.svc/json/";
					String url = base_url+"sendmessage";
					result =DataFetcher.postData(url,"-",sedmessagetext,
							Utils.getSharedPreferences(getActivity()).getString("userid", ""),
							_userProfile.ProfileId,
							Utils.getSharedPreferences(getActivity()).getString("genderid", ""),
							String.valueOf(_offerid)
							);
				}
			}
			return result;
		}
		
		@Override
		protected void onPostExecute(String s)
		{
			Log.v("APP", "no credits " +s);
			if(_userProfile.ProfileId!=null)
			{
				fr_message_view_editMessage.setText("");
				if(s.toString().contains("\"-1\""))
				{
					Toast.makeText(getActivity(),  getActivity().getResources().getString(R.string.errormsg) , Toast.LENGTH_LONG).show();
					new getHashTask().execute();
				}
				new getConversationWithProfileTask(Utils.getSharedPreferences(getActivity()).getString("userid", ""),_userProfile.ProfileId,adapter).execute();
			}
			else if(_minfo.ProfileId !=null )
			{
				
				Log.v("APP", "no credits " +s);
				
				if(s.toString().contains("\"-1\""))
				{
					Toast.makeText(getActivity(),  getActivity().getResources().getString(R.string.errormsg) , Toast.LENGTH_LONG).show();
				}
				else
				{
					Toast.makeText(getActivity(),  getActivity().getResources().getString(R.string.errormsg), Toast.LENGTH_LONG).show();
				}
				fr_message_view_editMessage.setText("");
			}
			else 
			{
				Toast.makeText(getActivity(),  getActivity().getResources().getString(R.string.errormsg), Toast.LENGTH_LONG).show();
				fr_message_view_editMessage.setText("");
			}
			
			Panel.refresh();
			Controller.refreshCredits(getActivity());
			Controller.refreshCounters(getActivity());
		}		
	}
	
	
	
	class getHashTask extends AsyncTask<Void,Void,Void>{

		@Override
		protected Void doInBackground(Void... params) {
			hash = ServiceClient.getHashTask(Utils.getSharedPreferences(getActivity()).getString("username",""));
			return null;
		}
		  
		@Override
		protected void onPostExecute(Void p){
			hash = hash.replace("\"", "");
			String uri = "http://www.goomena.com/Login.aspx?hash="+hash+"&login="+Utils.getSharedPreferences(getActivity()).getString("username","")+"&ReturnUrl=%2fMembers%2fSelectProduct.aspx";
			Log.v("APP", uri);
			Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
			getActivity().startActivity(browserIntent);
		}
		
	}
	
	
	class deleteMessageTask extends AsyncTask<String,Void,String>
	{
		ArrayList<String> _MessagesIDs;
		ConversationAdapter _adapter;
		public deleteMessageTask(ArrayList<String> MessagesIDs,ConversationAdapter adapter)
		{
			this._MessagesIDs = MessagesIDs;
			this._adapter = adapter;
		}
		
		@Override
		protected void onPreExecute() { fragLoading.Show(); }
		@Override
		protected String doInBackground(String... params) 
		{
			String refId = Utils.getSharedPreferences(getActivity()).getString("referrerid", "");
			if(refId.equals("")) { refId = "0"; }
			String result = null;
			if(!_MessagesIDs.isEmpty())
			{
				for(int i=0; i<_MessagesIDs.size(); i++)
				{
					result = ServiceClient.deleteMessage(Utils.getSharedPreferences(getActivity()).getString("userid", ""),_MessagesIDs.get(i),refId);
				}
				return result;
			}
			else
			{
				return result;
			}
		}
		
		@Override
		protected void onPostExecute(String v)
		{
			if(v!=null)
			{
				Toast.makeText(getActivity(), "The messages will be transfered to Trash or will be deleted", Toast.LENGTH_SHORT).show();
				//new getConversationTask().execute(minfo.MsgId);
				new getConversationWithProfileTask(Utils.getSharedPreferences(getActivity()).getString("userid", ""),_userProfile.ProfileId,_adapter).execute();
			
			
				Panel.refresh();
				Controller.refreshCredits(getActivity());
				Controller.refreshCounters(getActivity());
			}
			
			fragLoading.Close();
		}
	}
	
	
	
	class getProfileTask extends AsyncTask<Void,UserProfile,UserProfile>
	{
		String _userID;
		
		getProfileTask(String userID)
		{
			this._userID = userID;
		}
		
		@Override
		protected UserProfile doInBackground(Void... params) {
			// TODO Auto-generated method stub
			return ServiceClient.getProfile(_userID);
		}
		
		@Override
		protected void onPostExecute(final UserProfile userprofile)
		{
			if(userprofile!=null)
			{		
				_userProfile = userprofile;
				fr_message_view_imeage_profile.setOnClickListener(new OnClickListener() 
				{
					@Override
					public void onClick(View v) {
						
						InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
					    inputManager.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
					    
					    if (popupWindow.isShowing())
					    {
					    	popupWindow.dismiss();	
					    }
					    
					    
						// TODO Auto-generated method stub
						ArrayList<UserProfile> _userProfilesFull_ = new ArrayList<UserProfile>();
						_userProfilesFull_.add(_userProfile);
						Fragment fr = new FragProfile(2,_userProfilesFull_,0);			
						FragmentManager fm = getActivity().getFragmentManager();
					    FragmentTransaction fragmentTransaction = fm.beginTransaction();
					    fragmentTransaction.setCustomAnimations(R.anim.left_slidein_list_animator, R.anim.left_slideout_list_animator,R.anim.right_slidein_list_animator, R.anim.right_slideout_list_animator);
					    fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
					    fragmentTransaction.addToBackStack(null);
					    fragmentTransaction.commit();
						
					}
				});
				
				if(_userProfile.ImageUrl.contains("Images2/woman"))
		        {
					_userProfile.ImageUrl = "drawable://" + R.drawable.private_for_woman;
		        }
		        else if(_userProfile.ImageUrl.contains("Images2/men.png"))
		        {
		        	_userProfile.ImageUrl = "drawable://" + R.drawable.private_for_men;
		        }
				Utils.getImageLoader(getActivity()).displayImage(_userProfile.ImageUrl,fr_message_view_imeage_profile, Utils.getImageOptions());
				
				fr_message_view_text_username.setText(_userProfile.UserName);
			
				if(_userProfile.IsOnline)
				{
					fr_message_view_text_online.setText("Online");
					fr_message_view_imeage_imageOval_yellow.setBackgroundResource(R.drawable.oval_yellow);
					fr_message_view_imeage_imageOval_yellow.startAnimation(animFadein);
				}
				else if(!_userProfile.IsOnline)
				{
					fr_message_view_text_online.setText("Offline");
					fr_message_view_imeage_imageOval_yellow.setBackgroundResource(R.drawable.oval_red);
					fr_message_view_imeage_imageOval_yellow.clearAnimation();
				}
				else
			    {
					new getUserStateTask(_userProfile.ProfileId,fr_message_view_text_online,fr_message_view_imeage_imageOval_yellow).execute();
				}
			
			
				fr_message_view_text_age.setText(" "+_userProfile.UserAge+" ");
				fr_message_view_text_location.setText(" "+_userProfile.UserCity+", "+_userProfile.UserRegion+", "+_userProfile.UserCountry);
				adapter = new ConversationAdapter(getActivity(),null,_userProfile);
				new getConversationWithProfileTask(Utils.getSharedPreferences(getActivity()).getString("userid", ""),_userProfile.ProfileId,adapter).execute();
			}
		}
	}
	
	
	
	/**
	 * Checking keyboard height and keyboard visibility
	 */
	int previousHeightDiffrence = 0;
	private void checkKeyboardHeight(final View parentLayout) {

		parentLayout.getViewTreeObserver().addOnGlobalLayoutListener(
				new ViewTreeObserver.OnGlobalLayoutListener() {

					@Override
					public void onGlobalLayout() {
						
						Rect r = new Rect();
						parentLayout.getWindowVisibleDisplayFrame(r);
						
						int screenHeight = parentLayout.getRootView()
								.getHeight();
						int heightDifference = screenHeight - (r.bottom);
						
						int y = 0;
						if(heightDifference>=0)
						{
							y = previousHeightDiffrence - heightDifference ;
						}
						
						//if(y!=0)
						//{
						//	Log.v("APP", "adfadfadfdaf "+y);
						//}
						
						if (y > 50 || y < 0) {							
							popupWindow.dismiss();
						}
						
						if(heightDifference>=0)
						{
						previousHeightDiffrence = heightDifference;
						}
						if (heightDifference > 100) {

							isKeyBoardVisible = true;
							changeKeyboardHeight(heightDifference);

						} else {

							isKeyBoardVisible = false;
							
						}

					}
				});

	}

	/**
	 * change height of emoticons keyboard according to height of actual
	 * keyboard
	 * 
	 * @param height
	 *            minimum height by which we can make sure actual keyboard is
	 *            open or not
	 */
	private void changeKeyboardHeight(int height) {

		if (height > 100) {
			keyboardHeight = height;
			RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, keyboardHeight);
			emoticonsCover.setLayoutParams(params);
		}

	}
	
	
	
	public class SmileysAdapter extends BaseAdapter {

	    private ArrayList<String> arrayListSmileys = new ArrayList<String>();
	    private Context context;
	    private HashMap<String, Integer> emoticons = new HashMap<String, Integer>();
	    @SuppressWarnings("unused")
		private ArrayList<String> showListSmileys = new ArrayList<String>();


	    public SmileysAdapter(ArrayList<String> arraylistSmileys,Context context,HashMap<String, Integer> emoticons) {
	        // TODO Auto-generated constructor stub

	        this.arrayListSmileys = arraylistSmileys;
	        this.context = context;
	        this.emoticons = emoticons;
	    }

	    @Override
	    public int getCount() {
	        // TODO Auto-generated method stub
	        return arrayListSmileys.size();
	    }

	    @Override
	    public Object getItem(int position) {
	        // TODO Auto-generated method stub
	        return arrayListSmileys.get(position);
	    }

	    @Override
	    public long getItemId(int position) {
	        // TODO Auto-generated method stub
	        return position;
	    }

	    @SuppressLint({ "ViewHolder", "InflateParams" }) 
	    @Override
	    public View getView(final int position, View convertView, ViewGroup parent) {
	        convertView = LayoutInflater.from(context).inflate(R.layout.row, null);
	        ImageView imageView = (ImageView) convertView.findViewById(R.id.row_image);
	        imageView.setBackgroundResource(emoticons.get(arrayListSmileys.get(position)));
	        
	        convertView.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					 String value = groupIconsGrid.getAdapter().getItem(position).toString();
				        
				        
				        getSmiledText(getActivity(), value);
				}
			});
	        
	        
	        return convertView;
	        }
	}
	

}




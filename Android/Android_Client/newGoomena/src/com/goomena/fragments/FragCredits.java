package com.goomena.fragments;

import com.goomena.app.ActHome;
import com.goomena.app.ActionBarChange;
import com.goomena.app.R;
import com.goomena.app.data.ServiceClient;
import com.goomena.app.tools.Log;
import com.goomena.app.tools.Utils;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

@SuppressLint("NewApi") 
public class FragCredits extends Fragment
{
	RelativeLayout credits_layout_relativeLayoutGetMore,credits_layout_relativeLayoutGetIt;
	TextView credits_layout_textCredits;
	
	public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState) 
	{
		 return inflater.inflate(R.layout.credits_layout, container, false);
	}
	
	public void onActivityCreated (Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);
		
		
		
		credits_layout_relativeLayoutGetMore = (RelativeLayout) getActivity().findViewById(R.id.credits_layout_relativeLayoutGetMore); 
		credits_layout_relativeLayoutGetIt = (RelativeLayout) getActivity().findViewById(R.id.credits_layout_relativeLayoutGetIt);
		credits_layout_textCredits = (TextView) getActivity().findViewById(R.id.credits_layout_textCredits);
		
		new getCredits(credits_layout_textCredits).execute();
		
		credits_layout_relativeLayoutGetMore.setOnClickListener(clickListener);
		credits_layout_relativeLayoutGetIt.setOnClickListener(clickListener);
	}
	
	OnClickListener clickListener = new OnClickListener() 
	{
		@Override
		public void onClick(View v) 
		{
			if(v == credits_layout_relativeLayoutGetMore)
			{
				new getHashTask().execute();
			}
			else if(v ==credits_layout_relativeLayoutGetIt)
			{
				new getHashTask().execute();
			}
		}
	};
	
	
	class getCredits extends AsyncTask<Void,Void,String>
	{
		TextView _textCredits;
		public getCredits(TextView textCredits)
		{
			this._textCredits = textCredits;
		}
		@Override
		protected String doInBackground(Void... params) 
		{
			String Credits;
			Credits = ServiceClient.getCredits(Utils.getSharedPreferences(getActivity()).getString("userid",""));
			return Credits;
		}
		
		@Override
		protected void onPostExecute(String NCredits)
		{
			if(NCredits!=null)
			{
				_textCredits.setText(NCredits+" Credits");
			}			
		}
	}
	
	class getHashTask extends AsyncTask<Void,Void,String>{
		@Override
		protected String doInBackground(Void... params) {
			String hash;
			hash = ServiceClient.getHashTask(Utils.getSharedPreferences(getActivity()).getString("username",""));
			return hash;
		}
		@Override
		protected void onPostExecute(String p){
			p = p.replace("\"", "");
			String uri = "http://www.goomena.com/Login.aspx?hash="+p+"&login="+Utils.getSharedPreferences(getActivity()).getString("username","")+"&ReturnUrl=%2fMembers%2fSelectProduct.aspx";
			Log.v("APP", uri);
			Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
			getActivity().startActivity(browserIntent);
		}  
	}

}

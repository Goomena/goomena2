package com.goomena.fragments;

import java.util.ArrayList;

import com.edmodo.rangebar.RangeBar;
import com.goomena.app.ActHome;
import com.goomena.app.ActionBarChange;
import com.goomena.app.R;
import com.goomena.app.custom.StaggeredGridView;
import com.goomena.app.data.ServiceClient;
import com.goomena.app.models.Item;
import com.goomena.app.models.ItemList;
import com.goomena.app.models.Offer;
import com.goomena.app.models.OfferList;
import com.goomena.app.models.UserProfile;
import com.goomena.app.tools.Log;
import com.goomena.app.tools.Utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

@SuppressLint("NewApi") 
public class FragSearch extends Fragment
{
	EditText fr_search_EditSearchName;
	View fr_search_viewSearchIcon;
	LinearLayout fr_search_linearOnline,fr_search_linearPublicImage,fr_search_linearPrivateImage,fr_search_linearVip,fr_search_linearTravel;
	ImageView fr_search_imageOnline,fr_search_imagePublicImage,fr_search_imagePrivateImage,fr_search_imageVip,fr_search_imageTravel;
	
	ScrollView fr_search_scroll;
	
	private RangeBar rangebarAge;
	TextView fr_search_textAgeMin,fr_search_textAgeMax;
	private RangeBar rangebarheight;
	TextView fr_search_textheightMin,fr_search_textheightMax;
	
	Spinner fr_search_spinnerDistances;
	
	boolean fr_search_Online = false;
	boolean fr_search_PublicImage = false;
	boolean fr_search_PrivateImage = false;
	boolean fr_search_Travel = false;
	boolean fr_search_Vip = false;
	
	LinearLayout fr_search_linearMoreOptions,fr_search_linearFilterss;
	
	TextView fr_search_bodytype_title;
	StaggeredGridView fr_search_bodytype;
	ItemAdapter adapter_bodytpead;
	
	TextView fr_search_breastsize_title;
	StaggeredGridView fr_search_breastsize;
	ItemAdapter adapter_breastsize;
	
	TextView fr_search_haircolor_title;
	StaggeredGridView fr_search_haircolor;
	ItemAdapter adapter_haircolor;
	
	TextView fr_search_typeofdating_title;
	StaggeredGridView fr_search_typeofdating;
	ItemAdapter adapter_typeofdating;
	
	TextView fr_search_spokenlanguages_title;
	StaggeredGridView fr_search_spokenlanguages;
	ItemAdapter adapter_spokenlanguages;
	
	
	
	RelativeLayout fr_search_relativeSearch;
	
	private final static String TAG_FRAGMENT = "TAG_FRAGMENT";
	
	public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState) 
	{
		 return inflater.inflate(R.layout.fr_search, container, false);
	}
	
	public void onActivityCreated (Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);
		
		//ActionBarChange.changeActionBar(getActivity(), 1);
		
		fr_search_EditSearchName = (EditText) getActivity().findViewById(R.id.fr_search_EditSearchName);
		fr_search_viewSearchIcon = (View) getActivity().findViewById(R.id.fr_search_viewSearchIcon);
		
		rangebarAge = (RangeBar) getActivity().findViewById(R.id.fr_search_rangebarAge);
		rangebarAge.setThumbIndices(0, 85);
		
		rangebarheight = (RangeBar) getActivity().findViewById(R.id.fr_search_rangebarheight);
		rangebarheight.setThumbIndices(0, 45);
		
		fr_search_textAgeMin = (TextView) getActivity().findViewById(R.id.fr_search_textAgeMin);
		fr_search_textAgeMax = (TextView) getActivity().findViewById(R.id.fr_search_textAgeMax);
		fr_search_textheightMin = (TextView) getActivity().findViewById(R.id.fr_search_textheightMin);
		fr_search_textheightMax = (TextView) getActivity().findViewById(R.id.fr_search_textheightMax);
		
		fr_search_linearOnline = (LinearLayout) getActivity().findViewById(R.id.fr_search_linearOnline);
		fr_search_imageOnline = (ImageView) getActivity().findViewById(R.id.fr_search_imageOnline);
		fr_search_linearPublicImage = (LinearLayout) getActivity().findViewById(R.id.fr_search_linearPublicImage);
		fr_search_imagePublicImage = (ImageView) getActivity().findViewById(R.id.fr_search_imagePublicImage);
		fr_search_linearPrivateImage = (LinearLayout) getActivity().findViewById(R.id.fr_search_linearPrivateImage);
		fr_search_imagePrivateImage = (ImageView) getActivity().findViewById(R.id.fr_search_imagePrivateImage);
		fr_search_linearVip = (LinearLayout) getActivity().findViewById(R.id.fr_search_linearVip);
		fr_search_imageVip = (ImageView) getActivity().findViewById(R.id.fr_search_imageVip);
		if(Utils.getSharedPreferences(getActivity()).getString("genderid","").contains("1"))
		{
			fr_search_linearVip.setVisibility(View.GONE);
		}
		fr_search_linearTravel = (LinearLayout) getActivity().findViewById(R.id.fr_search_linearTravel);
		fr_search_imageTravel = (ImageView) getActivity().findViewById(R.id.fr_search_imageTravel);
		
		
		fr_search_scroll = (ScrollView) getActivity().findViewById(R.id.fr_search_scroll);
		fr_search_linearMoreOptions  = (LinearLayout) getActivity().findViewById(R.id.fr_search_linearMoreOptions);
		fr_search_linearFilterss  = (LinearLayout) getActivity().findViewById(R.id.fr_search_linearFilterss);
		
		
		
		
		fr_search_relativeSearch = (RelativeLayout) getActivity().findViewById(R.id.fr_search_relativeSearch);
		
		
		
		fr_search_textAgeMin.setText( Integer.toString(rangebarAge.getLeftIndex()+18) );
		fr_search_textAgeMax.setText(Integer.toString(rangebarAge.getRightIndex()+18) );
		
		rangebarAge.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onIndexChangeListener(RangeBar rangeBar, int leftThumbIndex, int rightThumbIndex) {

            	fr_search_textAgeMin.setText( Integer.toString(rangebarAge.getLeftIndex()+18) );
        		fr_search_textAgeMax.setText(Integer.toString(rangebarAge.getRightIndex()+18) );
            }
        });
		
		fr_search_textheightMin.setText( Integer.toString(rangebarheight.getLeftIndex()+152) );
		fr_search_textheightMax.setText(Integer.toString(rangebarheight.getRightIndex()+152) );
		
		rangebarheight.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onIndexChangeListener(RangeBar rangeBar, int leftThumbIndex, int rightThumbIndex) {

            	fr_search_textheightMin.setText( Integer.toString(rangebarheight.getLeftIndex()+152) );
        		fr_search_textheightMax.setText(Integer.toString(rangebarheight.getRightIndex()+152) );
            }
        });
		
		
		
		
		fr_search_spinnerDistances = (Spinner) getActivity().findViewById(R.id.fr_search_spinnerDistances);
		
		
		ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(getActivity(),
		        R.array.distances, android.R.layout.simple_spinner_item);
		
		adapter2.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
		
		fr_search_spinnerDistances.setAdapter(adapter2);
		
		
		fr_search_viewSearchIcon.setOnClickListener(clickListener);
		
		fr_search_linearOnline.setOnClickListener(clickListener);
		fr_search_linearPublicImage.setOnClickListener(clickListener);
		fr_search_linearPrivateImage.setOnClickListener(clickListener);
		fr_search_linearVip.setOnClickListener(clickListener);
		fr_search_linearTravel.setOnClickListener(clickListener);
		fr_search_relativeSearch.setOnClickListener(clickListener);
		fr_search_linearMoreOptions.setOnClickListener(clickListener);
	}

	
	
	
	
	
	
	OnClickListener clickListener = new OnClickListener() 
	{
		public void onClick(View v) 
		{
			if(v == fr_search_viewSearchIcon)
			{
				InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
			    inputManager.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
				FragGridProfile a = new FragGridProfile(21);
				
				if(fr_search_EditSearchName.getText().toString().equals(""))
				{
					a.searchByName("a");
				}
				else
				{
					a.searchByName(fr_search_EditSearchName.getText().toString());
				}
				
				Fragment fr = a;			
				FragmentManager fm = getActivity().getFragmentManager();
				
		        FragmentTransaction fragmentTransaction = fm.beginTransaction();
		        fragmentTransaction.setCustomAnimations(R.anim.left_slidein_list_animator, R.anim.left_slideout_list_animator,R.anim.right_slidein_list_animator, R.anim.right_slideout_list_animator);
		        fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
		        fragmentTransaction.addToBackStack(null);
		
		        fragmentTransaction.commit();
				
			}
			else if(v == fr_search_linearOnline)
			{
				if(fr_search_Online)
				{
					fr_search_linearOnline.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					fr_search_imageOnline.setBackgroundResource(R.drawable.search_unckeck);
					fr_search_Online = false;
				}
				else
				{
					fr_search_linearOnline.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					fr_search_imageOnline.setBackgroundResource(R.drawable.search_check);
					fr_search_Online = true;
				}
			}
			else if(v == fr_search_linearPublicImage)
			{
				if(fr_search_PublicImage)
				{
					fr_search_linearPublicImage.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					fr_search_imagePublicImage.setBackgroundResource(R.drawable.search_unckeck);
					fr_search_PublicImage = false;
				}
				else
				{
					fr_search_linearPublicImage.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					fr_search_imagePublicImage.setBackgroundResource(R.drawable.search_check);
					fr_search_PublicImage = true;
				}
			}
			else if(v == fr_search_linearPrivateImage)
			{
				if(fr_search_PrivateImage)
				{
					fr_search_linearPrivateImage.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					fr_search_imagePrivateImage.setBackgroundResource(R.drawable.search_unckeck);
					fr_search_PrivateImage = false;
				}
				else
				{
					fr_search_linearPrivateImage.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					fr_search_imagePrivateImage.setBackgroundResource(R.drawable.search_check);
					fr_search_PrivateImage = true;
				}
			}
			else if(v == fr_search_linearVip)
			{
				if(fr_search_Vip)
				{
					fr_search_linearVip.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					fr_search_imageVip.setBackgroundResource(R.drawable.search_unckeck);
					fr_search_Vip = false;
				}
				else
				{
					fr_search_linearVip.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					fr_search_imageVip.setBackgroundResource(R.drawable.search_check);
					fr_search_Vip = true;
				}
			}
			else if(v == fr_search_linearTravel)
			{
				if(fr_search_Travel)
				{
					fr_search_linearTravel.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					fr_search_imageTravel.setBackgroundResource(R.drawable.search_unckeck);
					fr_search_Travel = false;
				}
				else
				{
					fr_search_linearTravel.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					fr_search_imageTravel.setBackgroundResource(R.drawable.search_check);
					fr_search_Travel = true;
				}
			}
			else if(v == fr_search_linearMoreOptions)
			{
				fr_search_linearFilterss.setVisibility(View.VISIBLE);
				
				adapter_bodytpead = new ItemAdapter(getActivity());
				fr_search_bodytype = (StaggeredGridView) getActivity().findViewById(R.id.fr_search_bodytype);
				fr_search_bodytype_title = (TextView) getActivity().findViewById(R.id.fr_search_bodytype_title);
				fr_search_bodytype.setAdapter(adapter_bodytpead);
				new getItemListTask(fr_search_bodytype_title,adapter_bodytpead,"bodytype", "GR").execute();
				
				fr_search_breastsize = (StaggeredGridView) getActivity().findViewById(R.id.fr_search_breastsize);
				fr_search_breastsize_title = (TextView) getActivity().findViewById(R.id.fr_search_breastsize_title);
				adapter_breastsize = new ItemAdapter(getActivity());
				fr_search_breastsize.setAdapter(adapter_breastsize);
				new getItemListTask(fr_search_breastsize_title,adapter_breastsize,"breastsize", "US").execute();
				
				fr_search_haircolor = (StaggeredGridView) getActivity().findViewById(R.id.fr_search_haircolor);
				fr_search_haircolor_title = (TextView) getActivity().findViewById(R.id.fr_search_haircolor_title);
				adapter_haircolor = new ItemAdapter(getActivity());
				fr_search_haircolor.setAdapter(adapter_haircolor);
				new getItemListTask(fr_search_haircolor_title,adapter_haircolor,"haircolor", "GR").execute();
				
				fr_search_typeofdating = (StaggeredGridView) getActivity().findViewById(R.id.fr_search_typeofdating);
				fr_search_typeofdating.setColumnCount(1);
				fr_search_typeofdating_title = (TextView) getActivity().findViewById(R.id.fr_search_typeofdating_title);
				adapter_typeofdating = new ItemAdapter(getActivity());
				fr_search_typeofdating.setAdapter(adapter_typeofdating);
				new getItemListTask(fr_search_typeofdating_title,adapter_typeofdating,"typeofdating", "GR").execute();
				
				fr_search_spokenlanguages = (StaggeredGridView) getActivity().findViewById(R.id.fr_search_spokenlanguages);
				fr_search_spokenlanguages_title = (TextView) getActivity().findViewById(R.id.fr_search_spokenlanguages_title);
				adapter_spokenlanguages = new ItemAdapter(getActivity());
				fr_search_spokenlanguages.setAdapter(adapter_spokenlanguages);
				new getItemListTask(fr_search_spokenlanguages_title,adapter_spokenlanguages,"spokenlanguages", "GR").execute();
			}
			else if(v == fr_search_relativeSearch)
			{
				InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
			    inputManager.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
			    
				int bitPhoto=0,bitOnline=0,bitVip=0,bitPrivatePhoto=0,bitTravel=0;
				if(fr_search_Online)
					bitOnline = 1;
				
				
				if(fr_search_PublicImage)
					bitPhoto = 1;
				
				if(fr_search_Vip)
					bitVip = 1;
				
				if(fr_search_PrivateImage)
					bitPrivatePhoto = 1;
				
				if(fr_search_Travel)
					bitTravel = 1;
				
				String distance = (String)fr_search_spinnerDistances.getSelectedItem();
				if (distance !=null && isNumeric(distance)) 
				{
		            if ( distance.indexOf(" Km") > -1 )
		            {
		            	distance = distance.replace(" Km", "");
		            }
		            if (  distance.indexOf(" Χλμ.") > -1 )
		            {
		            	distance = distance.replace(" Χλμ.", "");
		            }
				} 
				else 
				{
					distance = "1200";
				}
				
				FragGridProfile a = new FragGridProfile(22);
				a.searchByFilter(fr_search_textAgeMin.getText().toString(),fr_search_textAgeMax.getText().toString(),bitOnline, bitPhoto,bitVip,bitPrivatePhoto,bitTravel, distance);
				
				Fragment fr = a;			
				FragmentManager fm = getActivity().getFragmentManager();
				
		        FragmentTransaction fragmentTransaction = fm.beginTransaction();
		        fragmentTransaction.setCustomAnimations(R.anim.left_slidein_list_animator, R.anim.left_slideout_list_animator,R.anim.right_slidein_list_animator, R.anim.right_slideout_list_animator);
		        fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
		        fragmentTransaction.addToBackStack(null);
		
		        fragmentTransaction.commit();
			}
		}
	};
	
	
	
	public static boolean isNumeric(String str)  
	{  
	  try  
	  {  
	    int d = Integer.parseInt(str);  
	  }  
	  catch(NumberFormatException nfe)  
	  {  
	    return false;  
	  }  
	  return true;  
	}
	
	
	
	class getItemListTask extends AsyncTask<Void,Integer,ItemList>
	{
		String _Mode;
		String _Lanquage;
		ItemAdapter _adapter;
		TextView _title;
		
		public getItemListTask(TextView title,ItemAdapter adapter,String Mode,String Lanquage)
		{
			this._title = title;
			this._adapter = adapter;
			this._Mode = Mode;
			this._Lanquage = Lanquage;
		}
		@Override
		protected void onPreExecute(){}
		
		@Override
		protected ItemList doInBackground(Void... params) 
		{
			
			ItemList itemList = null;
			
				itemList = ServiceClient.getItems(_Mode, _Lanquage);
			
			return itemList;
		}
		
		@Override
		protected void onPostExecute(ItemList result)
		{
			if(result.items!=null)
			{
				if(_Mode.contains("typeofdating") || _Mode.contains("spokenlanguages"))
				{
					_adapter.setItems(result.items);
					_adapter.notifyDataSetChanged();
				}
				else
				{
					_title.setText(result.items.get(0).title);
					result.items.remove(0);
					_adapter.setItems(result.items);
					_adapter.notifyDataSetChanged();	
				}
				fr_search_scroll.setScrollY(1000);
			}
		}
	}
	
	
	
	
	
	
	
	
	public class ItemAdapter extends BaseAdapter
	{
		Activity _activity;
		private ArrayList<Item> itemList;
		int _viewMode;
		
		public ItemAdapter(Activity activity)
		{
			this._activity = activity;
			
			itemList = new ArrayList<Item>();
		}
		
		public void setItems(ArrayList<Item> items){
			this.itemList = items;			
		}	

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return itemList.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return itemList.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			View rowView = null;
			LayoutInflater inflater = _activity.getLayoutInflater();
			
			if(rowView==null)
			{			
				rowView = inflater.inflate(R.layout.item_filter, null);
				ViewHolder vholder = new ViewHolder();
				vholder.item_filter_linear = (LinearLayout)rowView.findViewById(R.id.item_filter_linear);	
				vholder.item_filter_text = (TextView)rowView.findViewById(R.id.item_filter_text);	
				vholder.item_filter_image = (ImageView)rowView.findViewById(R.id.item_filter_image);						
				vholder.seleced = false;	
				rowView.setTag(vholder);
			}
			final ViewHolder viewHolder = (ViewHolder)rowView.getTag();
			
			viewHolder.item_filter_text.setText(itemList.get(position).title);
			
			viewHolder.item_filter_linear.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					
					if(viewHolder.seleced)
					{
						viewHolder.item_filter_linear.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
						viewHolder.item_filter_image.setBackgroundResource(R.drawable.search_unckeck);
						viewHolder.seleced = false;
					}
					else
					{
						viewHolder.item_filter_linear.setBackgroundResource(R.drawable.rectangle_search_white_corners);
						viewHolder.item_filter_image.setBackgroundResource(R.drawable.search_check);
						viewHolder.seleced = true;
					}
				}
			});
				
			return rowView;	
		}

		class ViewHolder 
		{
			public TextView item_filter_text;
			public ImageView item_filter_image;
			public LinearLayout item_filter_linear;
			public boolean seleced;

		}
	}
}

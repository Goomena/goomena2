package com.goomena.fragments;

import com.goomena.app.R;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class FragEmpty extends Fragment
{
	public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState) 
	 {
		 return inflater.inflate(R.layout.empty, container, false);
	 }

}

package com.goomena.app.tools;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;

import com.goomena.app.R;
import com.nostra13.universalimageloader.cache.disc.impl.TotalSizeLimitedDiscCache;
import com.nostra13.universalimageloader.cache.disc.naming.HashCodeFileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;
import com.nostra13.universalimageloader.utils.StorageUtils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.view.Display;
import android.widget.ImageView;
import android.widget.Toast;

public class Utils 
{
	public static final String SharedPrefsKey = "GoomenaPreferences";
	private static SharedPreferences settings;
	private static ImageLoader loader;
	
	public static class DownloadImageTask extends AsyncTask<String, Void, Bitmap> 
	{
		ImageView bmImage;
		public DownloadImageTask(ImageView bmImage) 
		{
			this.bmImage = bmImage;
		}

		protected Bitmap doInBackground(String... urls) 
		{
			String urldisplay = urls[0];
		    Bitmap mIcon11 = null;
		    try 
		    {
		    	InputStream in = new java.net.URL(urldisplay).openStream();
		        mIcon11 = BitmapFactory.decodeStream(in);
		    } 
		    catch (Exception e)
		    {
		    	Log.e("Error", e.getMessage());
		        e.printStackTrace();
		    }
		    return mIcon11;
		}

		protected void onPostExecute(Bitmap result) 
		{
			bmImage.setImageBitmap(result);
		}
	}
	
    public static void CopyStream(InputStream is, OutputStream os)
    {
        final int buffer_size=1024;
        try
        {
            byte[] bytes=new byte[buffer_size];
            for(;;)
            {
              int count=is.read(bytes, 0, buffer_size);
              if(count==-1)
                  break;
              os.write(bytes, 0, count);
            }
        }
        catch(Exception ex){}
    }
    
    public static boolean isNetworkConnected(Context c) 
    {
    	ConnectivityManager cm = (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
    	NetworkInfo ni = cm.getActiveNetworkInfo();
    	if (ni == null) 
    	{
    		// There are no active networks.
    	    return false;
    	} 
    	else
    	{
    		return true;
    	}
    }
    
    public static boolean isSdPresent()
    {
    	Boolean isSDPresent = Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);
    	return isSDPresent;
    }
    
    public static float convertDpToPixel(float dp, Context context)
    {
    	Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return px;
    }
    
    public static DisplayImageOptions getImageOptions()
    {
    	DisplayImageOptions options;
    	options = new DisplayImageOptions.Builder()
 		//.showStubImage(R.drawable.loading)
 		//.showImageForEmptyUri(R.drawable.loading)
 		//.showImageOnFail(R.drawable.loading)
 		.bitmapConfig(Bitmap.Config.RGB_565)
 		.imageScaleType(ImageScaleType.IN_SAMPLE_INT)
 		.cacheInMemory(true)
 		.cacheOnDisc(true)
 		//.displayer(new FadeInBitmapDisplayer(1000))
 		.build();
    	 
    	return options;
    }
    public static DisplayImageOptions getImageOptions1()
    {
    	DisplayImageOptions options;
    	options = new DisplayImageOptions.Builder()
 		//.showStubImage(R.drawable.loading)
 		//.showImageForEmptyUri(R.drawable.loading)
 		//.showImageOnFail(R.drawable.loading)
 		.bitmapConfig(Bitmap.Config.RGB_565)
 		.imageScaleType(ImageScaleType.IN_SAMPLE_INT)
 		.cacheInMemory(true)
 		.cacheOnDisc(true)
 		.displayer(new FadeInBitmapDisplayer(2000))
 		.build();
    	 
    	return options;
    }
    
    public static DisplayImageOptions getImageOptions2()
    {
    	DisplayImageOptions options;
    	options = new DisplayImageOptions.Builder()
    	.showImageOnLoading(R.drawable.loading)
 		.showImageForEmptyUri(R.drawable.loading)
 		.showImageOnFail(R.drawable.loading)
  		.bitmapConfig(Bitmap.Config.RGB_565)
  		.imageScaleType(ImageScaleType.IN_SAMPLE_INT)
  		.cacheInMemory(true)
  		.cacheOnDisc(true)
 		.displayer(new FadeInBitmapDisplayer(2000))
 		.build();
    	 
    	return options;
    }
    //Notification
    public static DisplayImageOptions getImageOptions3()
    {
    	DisplayImageOptions options;
    	options = new DisplayImageOptions.Builder()
    	.showImageOnLoading(R.drawable.loading)
 		.showImageForEmptyUri(R.drawable.loading)
 		.showImageOnFail(R.drawable.loading)
  		.bitmapConfig(Bitmap.Config.RGB_565)
  		.imageScaleType(ImageScaleType.EXACTLY)
  		.cacheInMemory(true)
  		.cacheOnDisc(true)
 		.displayer(new RoundedBitmapDisplayer(200))
 		.build();
    	 
    	return options;
    }
    
    public static ImageLoader getImageLoader(Activity act)
    {
    	if(loader==null)
    	{
    		loader = ImageLoader.getInstance();
    		
    		File cacheDir = StorageUtils.getCacheDirectory(act);

    		DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
    		    .cacheInMemory(true)
    		    .cacheOnDisc(true)
    		    .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
    		    .build();

    		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(act)
    		    .memoryCacheExtraOptions(480, 800)
    		    .discCacheExtraOptions(480, 800, CompressFormat.JPEG, 75 , null)
    		    .threadPoolSize(3)
    		    .threadPriority(Thread.MIN_PRIORITY - 1)
    		    .tasksProcessingOrder(QueueProcessingType.FIFO) // default
    		    //.offOutOfMemoryHandling()
    		    //.memoryCache(new UsingFreqLimitedMemoryCache(2 * 1024 * 1024))
    		    .memoryCache(new WeakMemoryCache())
    		    .denyCacheImageMultipleSizesInMemory()
    		    .discCache(new TotalSizeLimitedDiscCache(cacheDir, 30 * 1024 * 1024))
    		    
    		    .discCacheFileNameGenerator(new HashCodeFileNameGenerator())
    		    .imageDownloader(new BaseImageDownloader(act, 20 * 1000, 30 * 1000))
    		    .defaultDisplayImageOptions(defaultOptions)
    		    .build();
    		loader.init(config);
    	}
    	
    	return loader;
    }
    
    public static SharedPreferences getSharedPreferences(Activity context)
    {
    	if(settings==null)
    	{
    		settings = context.getSharedPreferences(SharedPrefsKey, 0);
    	}
    	return settings;
    }
    
    public static SharedPreferences getSharedPreferences(Context context)
    {
    	if(settings==null)
    	{
    		settings = context.getSharedPreferences(SharedPrefsKey, 0);
    	}
    	return settings;
    }
        
    public static void showNoInternet(Activity act)
    {
    	Toast.makeText(act,act.getResources().getString(R.string.nointernet), Toast.LENGTH_LONG).show();
    }
    
    @SuppressLint("NewApi")
	public static String getLoginDetails(Activity act)
    {
    	Display display = act.getWindowManager().getDefaultDisplay();
    	Point size = new Point();
    	try 
    	{
    		display.getSize(size);
    	}
    	catch (java.lang.NoSuchMethodError ignore) 
    	{
    		// Older device
    		size.x = display.getWidth();
    		size.y = display.getHeight();
    	}
    
    	int width = size.x;
    	int height = size.y;
    	
    	PackageInfo pInfo;
    	String version = "-";
		try 
		{
			pInfo = act.getPackageManager().getPackageInfo(act.getPackageName(), 0);
			version = pInfo.versionName;
		} 
		catch (NameNotFoundException e) 
		{
			e.printStackTrace();
		}    	
    	
    	return "Android App version "+ version + " OS version "+android.os.Build.VERSION.RELEASE + " Device Resolution w"+width +" h"+height + " Device Model "+android.os.Build.MODEL; 
    }
}

package com.goomena.app;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class ActionBarChange 
{ 
	static Boolean flagTran = true;
	@SuppressLint("NewApi") 
	public static void changeActionBar(Activity activity, int mode)
	{
		if(mode==0)
		{
			if(ActionBarChange.flagTran==false)
			{
				Animation fade_in = AnimationUtils.loadAnimation(activity, R.anim.fade_in_dialog_offer);
				LinearLayout actin_bar_bg_menu = ( LinearLayout ) activity.findViewById(R.id.actin_bar_bg_menu);
				ImageView actin_bar_imageGoo = ( ImageView ) activity.findViewById(R.id.actin_bar_imageGoo);
				ImageView action_barImageMessage = ( ImageView ) activity.findViewById(R.id.action_barImageMessage);
				ImageView action_barImageNotification = ( ImageView ) activity.findViewById(R.id.action_barImageNotification);
				ImageView action_barImageUser = ( ImageView ) activity.findViewById(R.id.action_barImageUser);
				
				actin_bar_bg_menu.setBackgroundResource(R.drawable.bg_menu_color);
				actin_bar_bg_menu.startAnimation(fade_in);
				
				actin_bar_imageGoo.setBackgroundResource(R.drawable.goo_black);
				action_barImageMessage.setImageResource(R.drawable.menumessage_top);
				action_barImageNotification.setImageResource(R.drawable.menunotification);
				action_barImageUser.setImageResource(R.drawable.menuuser_icon);
				ActionBarChange.flagTran = true;
			}
			ActionBarChange.flagTran = true;
		}
		else
		{
			if(ActionBarChange.flagTran==true)
			{
				Animation fade_in = AnimationUtils.loadAnimation(activity, R.anim.fade_in_dialog_offer);
				LinearLayout actin_bar_bg_menu = ( LinearLayout ) activity.findViewById(R.id.actin_bar_bg_menu);
				ImageView actin_bar_imageGoo = ( ImageView ) activity.findViewById(R.id.actin_bar_imageGoo);
				ImageView action_barImageMessage = ( ImageView ) activity.findViewById(R.id.action_barImageMessage);
				ImageView action_barImageNotification = ( ImageView ) activity.findViewById(R.id.action_barImageNotification);
				ImageView action_barImageUser = ( ImageView ) activity.findViewById(R.id.action_barImageUser);
				
				actin_bar_bg_menu.setBackgroundResource(R.drawable.bg_menu_color);
				actin_bar_bg_menu.startAnimation(fade_in);
				
				actin_bar_imageGoo.setBackgroundResource(R.drawable.goo_black);
				action_barImageMessage.setImageResource(R.drawable.menumessage_top_color);
				action_barImageNotification.setImageResource(R.drawable.notifacation_icon);
				action_barImageUser.setImageResource(R.drawable.profile);
				ActionBarChange.flagTran = false;
			}
			ActionBarChange.flagTran = false;
		}
	}

}

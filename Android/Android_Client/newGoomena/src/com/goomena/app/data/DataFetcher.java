package com.goomena.app.data;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONStringer;

import android.app.Activity;

import com.goomena.app.ActLogin;
import com.goomena.app.tools.Log;
import com.goomena.app.tools.Utils;

public class DataFetcher {


	public static String getData(String url){
		
		String jsonstr = "";
		
		 HttpParams httpParameters;
	     HttpGet httpGet;
	     HttpClient httpClient;
	        
	     httpParameters = new BasicHttpParams();	         
         int timeoutConnection = 30000;
         HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);	        
         int timeoutSocket = 50000;
         HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
         HttpClientParams.setRedirecting(httpParameters, true);
         httpClient = new DefaultHttpClient(httpParameters);	            	            
         httpGet = new HttpGet(url);
         Log.v("SERVER", "CALLING:\n"+url);
         try {
			HttpResponse response = httpClient.execute(httpGet);
			HttpEntity entity = response.getEntity();
			if(entity!=null){
				InputStream instream = entity.getContent();				
				StringWriter writer = new StringWriter();
				IOUtils.copy(instream, writer, "UTF-8");
				jsonstr = writer.toString();			
				
				Log.v("SERVER", "SERVER RESPONSE:\n"+jsonstr);	
				
			}
			
		} catch (ClientProtocolException e) {
			
			e.printStackTrace();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		return jsonstr;
	}
	
	public static String postData(String url,String subject,String body,String fromprofile,String toprofile,String genderid,String offerId){
		
        HttpPost request = new HttpPost(url);
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
       
        String result ="";
        HttpResponse response = null;
        
        // Build JSON string
        JSONStringer messageparams = null;
		try {
			messageparams = new JSONStringer()
			    .object() 
			        .key("subject").value(subject)
			        .key("body").value(body)
			        .key("fromprofileid").value(fromprofile)
			        .key("toprofileid").value(toprofile)
			        .key("genderid").value(genderid)
			        .key("offerId").value(offerId)
			    .endObject();
			    
			
			 StringEntity entity = new StringEntity(messageparams.toString(), HTTP.UTF_8);

		        request.setEntity(entity);

		        // Send request to WCF service
		        DefaultHttpClient httpClient = new DefaultHttpClient();
		        response = httpClient.execute(request);
			
		        if(response.getEntity()!=null){
					InputStream instream = response.getEntity().getContent();				
					StringWriter writer = new StringWriter();
					IOUtils.copy(instream, writer, "UTF-8");
					result = writer.toString();			
					
					Log.v("SERVER", "SERVER RESPONSE:\n"+result);			
				}
		        
		} catch (JSONException e) {
			
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}        
				
       return result;
		
	}
	
	public static String postImage(String filename, byte[] imgBytes, String profid,int pLevel){
		String base_url = "http://188.165.13.64/testwcf/GoomenaLib.PhotoService.svc/upload/";
		String url = base_url+"savephoto/"+filename+"/"+profid+"/"+pLevel;			    
	    	    
		// Log.v("SERVER", "Calling: "+url);
           DefaultHttpClient httpClient = new DefaultHttpClient();
           try {
        	  
      	     InputStream is = new ByteArrayInputStream(imgBytes);
         
             HttpPost request = new HttpPost(url);
             request.setHeader("Accept", "binary/octet-stream");
             request.setHeader("Content-type", "binary/octet-stream");
             InputStreamEntity reqEntity = new InputStreamEntity(is, -1);
             reqEntity.setChunked(false);
             request.setEntity(reqEntity);      	     
        	   
                         
             Log.v("SERVER", "Calling: "+url);
			HttpResponse response = httpClient.execute(request);
			
        	   
			String result;
			if(response.getEntity()!=null){
				InputStream instream = response.getEntity().getContent();				
				StringWriter writer = new StringWriter();
				IOUtils.copy(instream, writer, "UTF-8");
				result = writer.toString();			
				
				Log.v("SERVER", "SERVER RESPONSE: "+ result );	
				
				if ( (result.indexOf("ok") != -1) && (result.length()<5)) 
				{
					 
					return "Your Photo Was Uploades Successfully";
				} 
				else if ((result.indexOf("max") != -1) && (result.length()<5)) 
				{
					return "You Have Exceeded The Limit Of Available Photos";
				} 
				else 
				{
					return "Your Photo Not Uploading. Try Later";
				}
			} else {
				return "Your Photo Not Uploading. Try Later";
			}
			
			
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		  return "Your Photo Not Uploading. Try Later";
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
	
			e.printStackTrace();
			  return "Your Photo Not Uploading. Try Later";
		}
	  	 	        		
		//return "ok";
	}
	
	public static String postRegisterData(String email,String username,String password,String birthday,String gender,String region,String country,String city,String lag, String fbuid , String fbname , String fbusername){
		String result = "";
		String base_url = "http://188.165.13.64/testwcf/GoomenaLib.Version4.GoomenaServiceV4.svc/json/";
		String url = base_url+"register";
		
		DefaultHttpClient httpClient = new DefaultHttpClient();
		
		
		Log.v("SERVER","CALLING: \n"+url);
		HttpPost httpPost = new HttpPost(url);
		
		httpPost.setHeader("Accept", "application/json");
        httpPost.setHeader("Content-type", "application/json");
		
      //Build JSON string
        JSONStringer messageparams = null;
		try {
			messageparams = new JSONStringer()
			    .object() 
			        .key("email").value(email)
			        .key("username").value(username)
			        .key("password").value(password)
			        .key("birthday").value(birthday)
			        .key("gender").value(gender)
			        .key("region").value(region)
			        .key("country").value(country)
			        .key("city").value(city)
			        .key("lag").value(lag)
			        .key("fb_uid").value(fbuid)
			        .key("fb_name").value(fbname)
			        .key("fb_username").value(fbusername)
			    .endObject();
			    
			
			 StringEntity entity = new StringEntity(messageparams.toString(), HTTP.UTF_8);
		
                
			httpPost.setEntity(entity);
			HttpResponse response = httpClient.execute(httpPost);
			
			Log.v("SERVER","RESPONSE: \n"+response.toString());
			
			if(response.getEntity()!=null){
				InputStream instream = response.getEntity().getContent();				
				StringWriter writer = new StringWriter();
				IOUtils.copy(instream, writer, "UTF-8");
				result = writer.toString();			
				
				Log.v("SERVER", "aeaeae SERVER RESPONSE:\n"+result);			
			}
			
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {			
			e.printStackTrace();
		}
        
		return result;
	}
	
	public static String postLoginData(String email,String password, String logindetails, String fbuid , String fbname , String fbusername){
		String result = "";
		String base_url = "http://188.165.13.64/testwcf/GoomenaLib.Version4.GoomenaServiceV4.svc/json/";
		String url = base_url+"login_v2";
		
		DefaultHttpClient httpClient = new DefaultHttpClient();
		

		Log.v("SERVER","CALLING: \n"+url);
		HttpPost httpPost = new HttpPost(url);
		

		httpPost.setHeader("Accept", "application/json");
        httpPost.setHeader("Content-type", "application/json");
        
        //Build JSON string
        JSONStringer messageparams = null;
		try {
			messageparams = new JSONStringer()
			    .object() 
			        .key("email").value(email)
			        .key("password").value(password)
			        .key("logindetails").value(logindetails)
			        .key("fb_uid").value(fbuid)
			        .key("fb_name").value(fbname)
			        .key("fb_username").value(fbusername)
			    .endObject();
			
			 StringEntity entity = new StringEntity(messageparams.toString(), HTTP.UTF_8);
				
             
				httpPost.setEntity(entity);
				HttpResponse response = httpClient.execute(httpPost);
				
				Log.v("SERVER","RESPONSE: \n"+response.toString());
				
				if(response.getEntity()!=null){
					InputStream instream = response.getEntity().getContent();				
					StringWriter writer = new StringWriter();
					IOUtils.copy(instream, writer, "UTF-8");
					result = writer.toString();			
					
					Log.v("SERVER", "SERVER RESPONSE: "+result);			
				}
				
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (JSONException e) {			
				e.printStackTrace();
			}
	        
			return result;
			    
	}
	
	public static String postGCMUserData(String regid,String profileId,String username,String email){
		
		String result = "";
		String base_url = "http://188.165.13.64/testandroid2014/GoomenaLib.Service1.svc/json/";
		String url = base_url+"storeGCMUser";
		
		DefaultHttpClient httpClient = new DefaultHttpClient();
		

		Log.v("SERVER","CALLING: \n"+url);
		HttpPost httpPost = new HttpPost(url);
		

		httpPost.setHeader("Accept", "application/json");
        httpPost.setHeader("Content-type", "application/json");
        
        //Build JSON string
        JSONStringer messageparams = null;
		try {
			messageparams = new JSONStringer()
			    .object() 
			        .key("regId").value(regid)
			        .key("profileId").value(profileId)
			        .key("loginname").value(username)
			        .key("email").value(email)
			    .endObject();
			
			 StringEntity entity = new StringEntity(messageparams.toString(), HTTP.UTF_8);
				
             
				httpPost.setEntity(entity);
				HttpResponse response = httpClient.execute(httpPost);
				
				Log.v("SERVER","RESPONSE: \n"+response.toString());
				
				if(response.getEntity()!=null){
					InputStream instream = response.getEntity().getContent();				
					StringWriter writer = new StringWriter();
					IOUtils.copy(instream, writer, "UTF-8");
					result = writer.toString();			
					
					Log.v("SERVER", "SERVER RESPONSE:\n"+result);			
				}
				
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (JSONException e) {			
				e.printStackTrace();
			}
	        
			return result;
			    
	}	
}

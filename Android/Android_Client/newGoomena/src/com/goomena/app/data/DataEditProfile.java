package com.goomena.app.data;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONStringer;

import android.app.Activity;

import com.google.gson.Gson;
import com.goomena.app.models.UserDetails;
import com.goomena.app.models.GetEditProfile.ProfileEditLocation;
import com.goomena.app.models.GetEditProfile.ProfileEditLookingFor;
import com.goomena.app.models.GetEditProfile.ProfileEditOtherDetails;
import com.goomena.app.models.GetEditProfile.ProfileEditPersonal;
import com.goomena.app.models.GetEditProfile.ProfileEditPersonalInfoLists;
import com.goomena.app.tools.Log;
import com.goomena.app.tools.Utils;

public class DataEditProfile 
{
	
	static String base_url = "http://188.165.13.64/testwcf/GoomenaLib.Version4.GoomenaServiceV4.svc/json/";

	
	
	private static Gson gson;
	
	private static Gson getGson(){
		if(gson==null){
			gson = new Gson();
		}
		return gson;
	}
	
	
public static String postsetProfileEditAboutMe(String AboutMe_Heading,String AboutMe_DescribeYourself,String AboutMe_DescribeFirstDate,Activity a){
		
		String result = "";
		String base_url = "http://188.165.13.64/testwcf/GoomenaLib.Version4.GoomenaServiceV4.svc/json/";
		String url = base_url+"setProfileEditAboutMe";
		
		DefaultHttpClient httpClient = new DefaultHttpClient();
		

		Log.v("SERVER","CALLING: \n"+url);
		HttpPost httpPost = new HttpPost(url);
		

		httpPost.setHeader("Accept", "application/json");
        httpPost.setHeader("Content-type", "application/json");
        
        
        //Build JSON string
        JSONStringer messageparams = null;
		try {
			messageparams = new JSONStringer()
			    .object() 
			    	.key("email").value(Utils.getSharedPreferences(a).getString("Email", ""))
			        .key("password").value(Utils.getSharedPreferences(a).getString("Pass", ""))
			        .key("logindetails").value(Utils.getLoginDetails(a))
			        .key("AboutMe_Heading").value(AboutMe_Heading)
			        .key("AboutMe_DescribeYourself").value(AboutMe_DescribeYourself)
			        .key("AboutMe_DescribeFirstDate").value(AboutMe_DescribeFirstDate)
			    .endObject();
			
			 StringEntity entity = new StringEntity(messageparams.toString(),HTTP.UTF_8);
				
             
				httpPost.setEntity(entity);
				HttpResponse response = httpClient.execute(httpPost);
				
				Log.v("SERVER","RESPONSE: \n"+response.toString());
				
				if(response.getEntity()!=null){
					InputStream instream = response.getEntity().getContent();				
					StringWriter writer = new StringWriter();
					IOUtils.copy(instream, writer, "UTF-8");
					result = writer.toString();			
					
					Log.v("SERVER", "SERVER RESPONSE:\n"+result);			
				}
				
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (JSONException e) {			
				e.printStackTrace();
			}
	        
			return result;
			    
	}



public static ProfileEditPersonal postgetProfileEditPersonal(Activity a){
	
	String result = "";
	String url = base_url+"getProfileEditPersonal";
	
	DefaultHttpClient httpClient = new DefaultHttpClient();
	

	Log.v("SERVER","CALLING: \n"+url);
	HttpPost httpPost = new HttpPost(url);
	

	httpPost.setHeader("Accept", "application/json");
    httpPost.setHeader("Content-type", "application/json");
    
    
    //Build JSON string
    JSONStringer messageparams = null;
	try {
		messageparams = new JSONStringer()
		    .object() 
		    	.key("email").value(Utils.getSharedPreferences(a).getString("Email", ""))
			    .key("password").value(Utils.getSharedPreferences(a).getString("Pass", ""))
		        .key("logindetails").value(Utils.getLoginDetails(a))
		    .endObject();
		
		 StringEntity entity = new StringEntity(messageparams.toString(),HTTP.UTF_8);
			
         
			httpPost.setEntity(entity);
			HttpResponse response = httpClient.execute(httpPost);
			
			Log.v("SERVER","RESPONSE: \n"+response.toString());
			
			if(response.getEntity()!=null){
				InputStream instream = response.getEntity().getContent();				
				StringWriter writer = new StringWriter();
				IOUtils.copy(instream, writer, "UTF-8");
				result = writer.toString();			
				
				Log.v("SERVER", "SERVER RESPONSE:\n"+result);			
			}
			
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {			
			e.printStackTrace();
		}
	
	
	ProfileEditPersonal Personal = getGson().fromJson(result, ProfileEditPersonal.class);
	return Personal;
		    
}

public static String setProfileEditPersonal(String FirstName,String LastName,String Address,String EmailAddress,String PhoneNumber,String MobilePhone,Activity a){
	
	String result = "";
	String url = base_url+"setProfileEditPersonal";
	
	DefaultHttpClient httpClient = new DefaultHttpClient();
	

	Log.v("SERVER","CALLING: \n"+url);
	HttpPost httpPost = new HttpPost(url);
	

	httpPost.setHeader("Accept", "application/json");
    httpPost.setHeader("Content-type", "application/json");
    
    
    //Build JSON string
    JSONStringer messageparams = null;
	try {
		messageparams = new JSONStringer()
		    .object() 
		    	.key("email").value(Utils.getSharedPreferences(a).getString("Email", ""))
			    .key("password").value(Utils.getSharedPreferences(a).getString("Pass", ""))
		        .key("logindetails").value(Utils.getLoginDetails(a))
		        .key("FirstName").value(FirstName)
		        .key("LastName").value(LastName)
		        .key("EmailAddress").value(EmailAddress)
		        .key("Address").value(Address)
		        .key("PhoneNumber").value(PhoneNumber)
		        .key("MobilePhone").value(MobilePhone)
		    .endObject();
		
		 StringEntity entity = new StringEntity(messageparams.toString(),HTTP.UTF_8);
			
         
			httpPost.setEntity(entity);
			HttpResponse response = httpClient.execute(httpPost);
			
			Log.v("SERVER","RESPONSE: \n"+response.toString());
			
			if(response.getEntity()!=null){
				InputStream instream = response.getEntity().getContent();				
				StringWriter writer = new StringWriter();
				IOUtils.copy(instream, writer, "UTF-8");
				result = writer.toString();			
				
				Log.v("SERVER", "SERVER RESPONSE:\n"+result);			
			}
			
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {			
			e.printStackTrace();
		}
	
	
	
	return result;
		    
}


public static ProfileEditLookingFor getProfileEditLookingFor(Activity a){
	
	String result = "";
	String url = base_url+"getProfileEditLookingFor";
	
	DefaultHttpClient httpClient = new DefaultHttpClient();
	

	Log.v("SERVER","CALLING: \n"+url);
	HttpPost httpPost = new HttpPost(url);
	

	httpPost.setHeader("Accept", "application/json");
    httpPost.setHeader("Content-type", "application/json");
    
    
    //Build JSON string
    JSONStringer messageparams = null;
	try {
		messageparams = new JSONStringer()
		    .object() 
		    	.key("email").value(Utils.getSharedPreferences(a).getString("Email", ""))
			    .key("password").value(Utils.getSharedPreferences(a).getString("Pass", ""))
		        .key("logindetails").value(Utils.getLoginDetails(a))
		    .endObject();
		
		 StringEntity entity = new StringEntity(messageparams.toString(),HTTP.UTF_8);
			
         
			httpPost.setEntity(entity);
			HttpResponse response = httpClient.execute(httpPost);
			
			Log.v("SERVER","RESPONSE: \n"+response.toString());
			
			if(response.getEntity()!=null){
				InputStream instream = response.getEntity().getContent();				
				StringWriter writer = new StringWriter();
				IOUtils.copy(instream, writer, "UTF-8");
				result = writer.toString();			
				
				Log.v("SERVER", "SERVER RESPONSE:\n"+result);			
			}
			
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {			
			e.printStackTrace();
		}
	
	ProfileEditLookingFor Personal = getGson().fromJson(result, ProfileEditLookingFor.class);
	return Personal;    
}

public static String setProfileEditLookingFor(long l,boolean b,boolean c,boolean d,boolean f,boolean g,boolean h,Activity a){
	
	String result = "";
	String url = base_url+"setProfileEditLookingFor";
	
	DefaultHttpClient httpClient = new DefaultHttpClient();
	

	Log.v("SERVER","CALLING: \n"+url);
	HttpPost httpPost = new HttpPost(url);
	

	httpPost.setHeader("Accept", "application/json");
    httpPost.setHeader("Content-type", "application/json");
    
    
    //Build JSON string
    JSONStringer messageparams = null;
	try {
		messageparams = new JSONStringer()
		    .object() 
		    	.key("email").value(Utils.getSharedPreferences(a).getString("Email", ""))
			    .key("password").value(Utils.getSharedPreferences(a).getString("Pass", ""))
		        .key("logindetails").value(Utils.getLoginDetails(a))
		        .key("RelationshipStatus").value(l)
		        .key("TODShort").value(b)
		        .key("TODFriendship").value(c)
		        .key("TODLongTerm").value(d)
		        .key("TODMutually").value(f)
		        .key("TODMarried").value(g)
		        .key("TODCasual").value(h)
		    .endObject();
		
		 StringEntity entity = new StringEntity(messageparams.toString(),HTTP.UTF_8);
			
         
			httpPost.setEntity(entity);
			HttpResponse response = httpClient.execute(httpPost);
			
			Log.v("SERVER","RESPONSE: \n"+response.toString());
			
			if(response.getEntity()!=null){
				InputStream instream = response.getEntity().getContent();				
				StringWriter writer = new StringWriter();
				IOUtils.copy(instream, writer, "UTF-8");
				result = writer.toString();			
				
				Log.v("SERVER", "SERVER RESPONSE:\n"+result);			
			}
			
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {			
			e.printStackTrace();
		}
	
	
	
	return result;
		    
}


public static String setProfileEditPersonalInfoLists(long l,long b,long c,long d,long f,long g,long h,long j,long k,long m,Activity a){
	
	String result = "";
	String url = base_url+"setProfileEditPersonalInfoLists";
	
	DefaultHttpClient httpClient = new DefaultHttpClient();
	

	Log.v("SERVER","CALLING: \n"+url);
	HttpPost httpPost = new HttpPost(url);
	

	httpPost.setHeader("Accept", "application/json");
    httpPost.setHeader("Content-type", "application/json");
    
   //Log.v("APP", "fgfgfgfhh "+m);
    
    //Build JSON string
    JSONStringer messageparams = null;
	try {
		messageparams = new JSONStringer()
		    .object() 
		    	.key("email").value(Utils.getSharedPreferences(a).getString("Email", ""))
			    .key("password").value(Utils.getSharedPreferences(a).getString("Pass", ""))
		        .key("logindetails").value(Utils.getLoginDetails(a))
		        .key("Height").value(l)
		        .key("BodyType").value(b)
		        .key("EyeColor").value(c)
		        .key("HairColor").value(d)
		        .key("Children").value(f)
		        .key("Ethnicity").value(g)
		        .key("Religion").value(h)
		        .key("Smoking").value(j)
		        .key("Drinking").value(k)
		        .key("BreastSize").value(m)
		    .endObject();
		
		 StringEntity entity = new StringEntity(messageparams.toString(),HTTP.UTF_8);
			
         
			httpPost.setEntity(entity);
			HttpResponse response = httpClient.execute(httpPost);
			
			Log.v("SERVER","RESPONSE: \n"+response.toString());
			
			if(response.getEntity()!=null){
				InputStream instream = response.getEntity().getContent();				
				StringWriter writer = new StringWriter();
				IOUtils.copy(instream, writer, "UTF-8");
				result = writer.toString();			
				
				Log.v("SERVER", "SERVER RESPONSE:\n"+result);			
			}
			
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {			
			e.printStackTrace();
		}
	
	
	
	return result;
		    
}




public static String setProfileEditOtherDetails(long b,long c,String Occupation,Activity a){
	
	String result = "";
	String url = base_url+"setProfileEditOtherDetails";
	
	DefaultHttpClient httpClient = new DefaultHttpClient();
	

	Log.v("SERVER","CALLING: \n"+url);
	HttpPost httpPost = new HttpPost(url);
	

	httpPost.setHeader("Accept", "application/json");
    httpPost.setHeader("Content-type", "application/json");
    
    
    //Build JSON string
    JSONStringer messageparams = null;
	try {
		messageparams = new JSONStringer()
		    .object() 
		    	.key("email").value(Utils.getSharedPreferences(a).getString("Email", ""))
			    .key("password").value(Utils.getSharedPreferences(a).getString("Pass", ""))
		        .key("logindetails").value(Utils.getLoginDetails(a))
		        .key("Education").value(b)
		        .key("AnnualIncome").value(c)
		        .key("Occupation").value(Occupation)
		    .endObject();
		
		 StringEntity entity = new StringEntity(messageparams.toString(),HTTP.UTF_8);
			
         
			httpPost.setEntity(entity);
			HttpResponse response = httpClient.execute(httpPost);
			
			Log.v("SERVER","RESPONSE: \n"+response.toString());
			
			if(response.getEntity()!=null){
				InputStream instream = response.getEntity().getContent();				
				StringWriter writer = new StringWriter();
				IOUtils.copy(instream, writer, "UTF-8");
				result = writer.toString();			
				
				Log.v("SERVER", "SERVER RESPONSE:\n"+result);			
			}
			
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {			
			e.printStackTrace();
		}
	
	return result;
		    
}

public static String setProfileEditLocation(String Country,String Region,String City,String ZipCode,Activity a){
	
	String result = "";
	String url = base_url+"setProfileEditLocation";
	
	DefaultHttpClient httpClient = new DefaultHttpClient();
	

	Log.v("SERVER","CALLING: \n"+url);
	HttpPost httpPost = new HttpPost(url);
	

	httpPost.setHeader("Accept", "application/json");
    httpPost.setHeader("Content-type", "application/json");
    
    
    
    //Build JSON string
    JSONStringer messageparams = null;
	try {
		messageparams = new JSONStringer()
		    .object() 
		    	.key("email").value(Utils.getSharedPreferences(a).getString("Email", ""))
			    .key("password").value(Utils.getSharedPreferences(a).getString("Pass", ""))
		        .key("logindetails").value(Utils.getLoginDetails(a))
		        .key("Country").value(Country)
		        .key("Region").value(Region)
		        .key("City").value(City)
		        .key("ZipCode").value(ZipCode)
		    .endObject();
		
		 StringEntity entity = new StringEntity(messageparams.toString(),HTTP.UTF_8);
			
         
			httpPost.setEntity(entity);
			HttpResponse response = httpClient.execute(httpPost);
			
			Log.v("SERVER","RESPONSE: \n"+response.toString());
			
			if(response.getEntity()!=null){
				InputStream instream = response.getEntity().getContent();				
				StringWriter writer = new StringWriter();
				IOUtils.copy(instream, writer, "UTF-8");
				result = writer.toString();			
				
				Log.v("SERVER", "SERVER RESPONSE:\n"+result);			
			}
			
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {			
			e.printStackTrace();
		}
	
	return result;
		    
}

public static ProfileEditLocation getProfileEditLocation(Activity a){
	
	String result = "";
	String url = base_url+"getProfileEditLocation";
	
	DefaultHttpClient httpClient = new DefaultHttpClient();
	

	Log.v("SERVER","CALLING: \n"+url);
	HttpPost httpPost = new HttpPost(url);
	

	httpPost.setHeader("Accept", "application/json");
    httpPost.setHeader("Content-type", "application/json");
    
    
    //Build JSON string
    JSONStringer messageparams = null;
	try {
		messageparams = new JSONStringer()
		    .object() 
		    	.key("email").value(Utils.getSharedPreferences(a).getString("Email", ""))
			    .key("password").value(Utils.getSharedPreferences(a).getString("Pass", ""))
		        .key("logindetails").value(Utils.getLoginDetails(a))
		    .endObject();
		
		 StringEntity entity = new StringEntity(messageparams.toString(),HTTP.UTF_8);
			
         
			httpPost.setEntity(entity);
			HttpResponse response = httpClient.execute(httpPost);
			
			Log.v("SERVER","RESPONSE: \n"+response.toString());
			
			if(response.getEntity()!=null){
				InputStream instream = response.getEntity().getContent();				
				StringWriter writer = new StringWriter();
				IOUtils.copy(instream, writer, "UTF-8");
				result = writer.toString();			
				
				Log.v("SERVER", "SERVER RESPONSE:\n"+result);			
			}
			
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {			
			e.printStackTrace();
		}
	
	ProfileEditLocation Personal = getGson().fromJson(result, ProfileEditLocation.class);
	return Personal;    
}



public static ProfileEditPersonalInfoLists getProfileEditPersonalInfoLists(Activity a){
	
	String result = "";
	String url = base_url+"getProfileEditPersonalInfoLists";
	
	DefaultHttpClient httpClient = new DefaultHttpClient();
	

	Log.v("SERVER","CALLING: \n"+url);
	HttpPost httpPost = new HttpPost(url);
	

	httpPost.setHeader("Accept", "application/json");
    httpPost.setHeader("Content-type", "application/json");
    
    
    //Build JSON string
    JSONStringer messageparams = null;
	try {
		messageparams = new JSONStringer()
		    .object() 
		    	.key("email").value(Utils.getSharedPreferences(a).getString("Email", ""))
			    .key("password").value(Utils.getSharedPreferences(a).getString("Pass", ""))
		        .key("logindetails").value(Utils.getLoginDetails(a))
		    .endObject();
		
		 StringEntity entity = new StringEntity(messageparams.toString(),HTTP.UTF_8);
			
         
			httpPost.setEntity(entity);
			HttpResponse response = httpClient.execute(httpPost);
			
			Log.v("SERVER","RESPONSE: \n"+response.toString());
			
			if(response.getEntity()!=null){
				InputStream instream = response.getEntity().getContent();				
				StringWriter writer = new StringWriter();
				IOUtils.copy(instream, writer, "UTF-8");
				result = writer.toString();			
				
				Log.v("SERVER", "SERVER RESPONSE:\n"+result);			
			}
			
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {			
			e.printStackTrace();
		}
	
	ProfileEditPersonalInfoLists Personal = getGson().fromJson(result, ProfileEditPersonalInfoLists.class);
	return Personal;    
}


public static ProfileEditOtherDetails getProfileEditOtherDetails(Activity a){
	
	String result = "";
	String url = base_url+"getProfileEditOtherDetails";
	
	DefaultHttpClient httpClient = new DefaultHttpClient();
	

	Log.v("SERVER","CALLING: \n"+url);
	HttpPost httpPost = new HttpPost(url);
	

	httpPost.setHeader("Accept", "application/json");
    httpPost.setHeader("Content-type", "application/json");
    
    
    //Build JSON string
    JSONStringer messageparams = null;
	try {
		messageparams = new JSONStringer()
		    .object() 
		    	.key("email").value(Utils.getSharedPreferences(a).getString("Email", ""))
			    .key("password").value(Utils.getSharedPreferences(a).getString("Pass", ""))
		        .key("logindetails").value(Utils.getLoginDetails(a))
		    .endObject();
		
		 StringEntity entity = new StringEntity(messageparams.toString(),HTTP.UTF_8);
			
         
			httpPost.setEntity(entity);
			HttpResponse response = httpClient.execute(httpPost);
			
			Log.v("SERVER","RESPONSE: \n"+response.toString());
			
			if(response.getEntity()!=null){
				InputStream instream = response.getEntity().getContent();				
				StringWriter writer = new StringWriter();
				IOUtils.copy(instream, writer, "UTF-8");
				result = writer.toString();			
				
				Log.v("SERVER", "SERVER RESPONSE:\n"+result);			
			}
			
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {			
			e.printStackTrace();
		}
	
	ProfileEditOtherDetails Personal = getGson().fromJson(result, ProfileEditOtherDetails.class);
	return Personal;    
}

public static String changePassword_newPassword(String newPassword,Activity a)
{
	String result = "";
	String url = base_url+"changePassword/"+newPassword;
	DefaultHttpClient httpClient = new DefaultHttpClient();
	Log.v("SERVER","CALLING: \n"+url);
	HttpPost httpPost = new HttpPost(url);
	
	httpPost.setHeader("Accept", "application/json");
    httpPost.setHeader("Content-type", "application/json");
    
    //Build JSON string
    JSONStringer messageparams = null;
	try {
		messageparams = new JSONStringer()
		    .object() 
		    	.key("email").value(Utils.getSharedPreferences(a).getString("Email", ""))
			    .key("password").value(Utils.getSharedPreferences(a).getString("Pass", ""))
		        .key("logindetails").value(Utils.getLoginDetails(a))
		    .endObject();
		
		StringEntity entity = new StringEntity(messageparams.toString(),HTTP.UTF_8);
			
         
		httpPost.setEntity(entity);
		HttpResponse response = httpClient.execute(httpPost);
			
		Log.v("SERVER","RESPONSE: \n"+response.toString());
			
		if(response.getEntity()!=null)
		{
			InputStream instream = response.getEntity().getContent();				
			StringWriter writer = new StringWriter();
			IOUtils.copy(instream, writer, "UTF-8");
			result = writer.toString();			
				
			Log.v("SERVER", "SERVER RESPONSE:\n"+result);			
		}
			
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {			
			e.printStackTrace();
		}
	
	return result;
		    
}

public static String changePassword_deleteAccount(Activity a)
{
	String result = "";
	String url = base_url+"deleteAccount";
	DefaultHttpClient httpClient = new DefaultHttpClient();
	Log.v("SERVER","CALLING: \n"+url);
	HttpPost httpPost = new HttpPost(url);
	
	httpPost.setHeader("Accept", "application/json");
    httpPost.setHeader("Content-type", "application/json");
    
    //Build JSON string
    JSONStringer messageparams = null;
	try {
		messageparams = new JSONStringer()
		    .object() 
		    	.key("email").value(Utils.getSharedPreferences(a).getString("Email", ""))
			    .key("password").value(Utils.getSharedPreferences(a).getString("Pass", ""))
		        .key("logindetails").value(Utils.getLoginDetails(a))
		    .endObject();
		
		StringEntity entity = new StringEntity(messageparams.toString(),HTTP.UTF_8);
			
         
		httpPost.setEntity(entity);
		HttpResponse response = httpClient.execute(httpPost);
			
		Log.v("SERVER","RESPONSE: \n"+response.toString());
			
		if(response.getEntity()!=null)
		{
			InputStream instream = response.getEntity().getContent();				
			StringWriter writer = new StringWriter();
			IOUtils.copy(instream, writer, "UTF-8");
			result = writer.toString();			
				
			Log.v("SERVER", "SERVER RESPONSE:\n"+result);			
		}
			
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {			
			e.printStackTrace();
		}
	
	return result;
}



}

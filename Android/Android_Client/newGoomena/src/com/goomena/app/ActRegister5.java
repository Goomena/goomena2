package com.goomena.app;

import com.goomena.app.R;
import com.goomena.app.data.ProfileManager;
import com.goomena.app.models.UserProfile;
import com.goomena.app.tools.Log;
import com.goomena.app.tools.Utils;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ActRegister5 extends Activity
{
	RelativeLayout relativeLayout1,relativeLayout2,relativeLayout3,relativeLayout4,relativeLayout5,relativeLayout6,relativeLayout7;
	
	UserProfile myprofile;
	
	ImageView register5_imageProfile,l_close_icon;
	TextView register5_username,register5_age,register5_location;
	
	String Myimage;
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_register5);
		
		relativeLayout7 = (RelativeLayout) findViewById(R.id.relativeLayout7);
		
		l_close_icon = (ImageView) findViewById(R.id.l_close_icon);
		register5_imageProfile = (ImageView) findViewById(R.id.register5_imageProfile);
		register5_username = (TextView) findViewById(R.id.register5_username);
		register5_age = (TextView) findViewById(R.id.register5_age);
		register5_location = (TextView) findViewById(R.id.register5_location);
     	
     	relativeLayout7.setOnClickListener(clickListener);
     	l_close_icon.setOnClickListener(clickListener);
     	
     	myprofile = ProfileManager.loadProfile(ActRegister5.this);
     	
     	
     	Myimage = getIntent().getStringExtra("MYimage");
     	
     	Utils.getImageLoader(ActRegister5.this).displayImage(Myimage,register5_imageProfile, Utils.getImageOptions());
     	register5_username.setText(myprofile.UserName);
     	register5_location.setText(ActRegister5.this.getResources().getString(R.string.from)+" "+myprofile.UserCity+", "+myprofile.UserRegion+", " +myprofile.UserCountry);
     	register5_age.setText(ActRegister5.this.getResources().getText(R.string.age)+" "+myprofile.UserAge+" "+ActRegister5.this.getResources().getText(R.string.years));
		
	}
	
	
	OnClickListener clickListener = new OnClickListener() 
	{
		@Override
		public void onClick(View v) 
		{
			if(v == relativeLayout7)
			{
				Intent intent = new Intent();
				intent.setClass(ActRegister5.this, ActWelcome.class);
				intent.putExtra("MYimage", Myimage);
				startActivity(intent);
				overridePendingTransition(R.anim.right_in, R.anim.left_out);
			}
			else if(v == l_close_icon)
			{
				ActRegister5.this.finish();
			}
		}
	};

}
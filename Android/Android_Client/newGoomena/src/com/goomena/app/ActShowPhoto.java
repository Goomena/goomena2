package com.goomena.app;

import java.util.ArrayList;
import java.util.Locale;

import com.google.analytics.tracking.android.EasyTracker;
import com.goomena.app.adapters.FullScreenImageAdapter;
import com.goomena.app.models.UserProfile;
import com.goomena.app.tools.Log;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnGenericMotionListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Gallery;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;


public class ActShowPhoto extends Activity
{
	ArrayList<String> imageUrls;
	UserProfile userprofile;
	int index;
	private FullScreenImageAdapter adapter;
	private ViewPager viewPager;
	Button btnClose;
	
	Integer[] imageIDs;
	GridView gridView;
	RelativeLayout gallery_relative;
	
	@SuppressLint("NewApi") 
	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);

	//	SharedPreferences preferences = getSharedPreferences("Lengua", MODE_PRIVATE);
//		int oro = preferences.getInt("lan", 0);
//		if ( oro == 0 ) 
//		{
//			Locale locale = new Locale("en"); 
//			Locale.setDefault(locale);
//			Configuration config = new Configuration();
//			config.locale = locale;
//			getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
//			setContentView(R.layout.act_showphoto);
//		} 
//		else if ( oro == 1 ) 
//		{
//			Locale locale = new Locale("el"); 
//			Locale.setDefault(locale);
//			Configuration config = new Configuration();
//			config.locale = locale;
//			getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
//			setContentView(R.layout.act_showphoto);	
//		} 
//		else if ( oro == 2 ) 
//		{
//			Locale locale = new Locale("tr"); 
//			Locale.setDefault(locale);
//			Configuration config = new Configuration();
//			config.locale = locale;
//			getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
//			setContentView(R.layout.act_showphoto);
//		} 
//		else if ( oro == 3 ) 
//		{
// 			Locale locale = new Locale("es"); 
// 			Locale.setDefault(locale);
// 			Configuration config = new Configuration();
// 			config.locale = locale;
// 			getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
// 			setContentView(R.layout.act_showphoto);
// 		} 
//		else
//		{
//			setContentView(R.layout.act_showphoto);
//		}
		
		setContentView(R.layout.act_showphoto);
		
		//Log.v("APP", "rgergegegtr" +getWindowManager().getDefaultDisplay().getWidth());
		
		
		 btnClose = (Button) findViewById(R.id.btnClose);
	
		 btnClose.setOnClickListener(new View.OnClickListener() 
	        {			
				@Override
				public void onClick(View v) 
				{
					finish();
				}
			}); 
		
	
			
		imageUrls = (ArrayList<String>) getIntent().getExtras().getSerializable("ImageUrls");	
		index = (Integer) getIntent().getExtras().getSerializable("Index");
		userprofile = (UserProfile) getIntent().getExtras().getSerializable("ImageUserProfile");	
		
		viewPager = (ViewPager) findViewById(R.id.view_pager);
		
		Intent i = getIntent();
		int position = i.getIntExtra("position", index);
		
		adapter = new FullScreenImageAdapter(ActShowPhoto.this,imageUrls,userprofile);
		viewPager.setAdapter(adapter);
		
		// displaying selected image first
		viewPager.setCurrentItem(position);
		
		imageIDs = new Integer[imageUrls.size()];
		for(int g=0; g<imageUrls.size(); g++)
		{
			if(g==position)
			{
				imageIDs[position] = R.drawable.circle_white;
			}
			else
			{
				imageIDs[g] = R.drawable.circle_outline;
			}
		}
		
		//final Gallery gallery = (Gallery) findViewById(R.id.gallery1);
		final ImageAdapter yy = new ImageAdapter(this);
		//gallery.setAdapter(yy); 
		gridView = (GridView) findViewById(R.id.gallery1);
		gridView.setLayoutParams(new RelativeLayout.LayoutParams(imageUrls.size()*50, 60)); 
		gridView.setNumColumns(imageUrls.size());
		gridView.setAdapter(yy);
		//gallery.setSelection(position);
		
		
		
		
		
		viewPager.setOnPageChangeListener(new OnPageChangeListener() {
			
			@Override
			public void onPageSelected(int arg0) {
				// TODO Auto-generated method stub
				
				for(int g=0; g < imageUrls.size(); g++)
				{
					if(g==arg0)
					{
						imageIDs[arg0] = R.drawable.circle_white;
					}
					else
					{
						imageIDs[g] = R.drawable.circle_outline;
					}
					
				}
				
				yy.notifyDataSetChanged();
				gridView.setAdapter(yy);
				
			}
			
			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onPageScrollStateChanged(int arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		
		
		
		
		
		gridView.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				
				//Log.v("APP", "sssssssss "+position);
				viewPager.setCurrentItem(position);
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
				
			}
		});
//		gallery.setOnItemClickListener(new OnItemClickListener() 
//		{
//	
//			public void onItemClick(AdapterView parent, View v, int position, long id)
//			{
//			
//				Log.v("APP", "rtrtrtrtd");
//				viewPager.setCurrentItem(position);
//			} 
//		});
	}
	
	
	
	
	
	
	public class ImageAdapter extends BaseAdapter 
	{
		Context context;
		int itemBackground;
		
		public ImageAdapter(Context c) 
		{
			context = c;
			//---setting the style---
			TypedArray a = obtainStyledAttributes(R.styleable.Gallery1);
			a.recycle();
		}
	
		//---returns the number of images---
		public int getCount()
		{ 
			return imageIDs.length;
		}
	
		//---returns the item---
		public Object getItem(int position) 
		{ 
			return position;
		}
	
		//---returns the ID of an item---
		public long getItemId(int position) 
		{ 
			return position;
		}
		
		//---returns an ImageView view---
		@SuppressWarnings("deprecation")
		public View getView(int position, View convertView, ViewGroup parent) 
		{
			ImageView imageView;
			
			if (convertView == null) 
			{
				imageView = new ImageView(context); 
				imageView.setImageResource(imageIDs[position]); 
				
				//imageView.setScaleType(ImageView.ScaleType.FIT_XY); 
				imageView.setLayoutParams(new GridView.LayoutParams(Math.round(getWindowManager().getDefaultDisplay().getWidth()/36), Math.round(getWindowManager().getDefaultDisplay().getWidth()/36))); //htan 30 ,30
				
			} 
			else 
			{
				imageView = (ImageView) convertView; 
			}
			//imageView.setBackgroundResource(itemBackground);
			return imageView; 
		}
	}
	
	
	
	
	
	@Override
	public void onStart() 
	{
		super.onStart();
		// The rest of your onStart() code.
	    EasyTracker.getInstance(this).activityStart(this);  // Add this method.
	}

	  
	@Override
	public void onStop()
	{
		super.onStop();
	    // The rest of your onStop() code.
	    EasyTracker.getInstance(this).activityStop(this);  // Add this method.
	}
}
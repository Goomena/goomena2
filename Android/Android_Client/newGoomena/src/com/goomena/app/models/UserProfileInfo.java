package com.goomena.app.models;

public class UserProfileInfo
{     
    public String profileId;     
    public String userName;     
    public String firstName;     
    public String lastName;     
    public String password;     
    public String genderId;     
    public String zipCode;     
    public String latitude;     
    public String longtitude;

}
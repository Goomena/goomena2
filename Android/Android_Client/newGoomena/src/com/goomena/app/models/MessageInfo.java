package com.goomena.app.models;

import java.io.Serializable;

public class MessageInfo implements Serializable 
{     
    public String ProfileId;     
    public String MsgId;
    public String MsgCount;     
    public String UserName;
    public boolean IsOnline;
    public String Age;     
    public String City;
    public String Region;
    public String Country;     
    public String ProfileImg;
    public String DateTimeSent;
}
package com.goomena.app;

import static com.goomena.app.notifications.CommonUtilities.DISPLAY_MESSAGE_ACTION;
import static com.goomena.app.notifications.CommonUtilities.EXTRA_MESSAGE;
import static com.goomena.app.notifications.CommonUtilities.SENDER_ID;

import java.util.Locale;

import com.google.android.gcm.GCMRegistrar;
import com.goomena.app.adapters.ObservableScrollView;
import com.goomena.app.adapters.ObservableScrollView.ScrollViewListener;
import com.goomena.app.data.ProfileManager;
import com.goomena.app.data.ServiceClient;
import com.goomena.app.global.Global;
import com.goomena.app.models.UserProfile;
import com.goomena.app.notifications.ServerUtilities;
import com.goomena.app.notifications.WakeLocker;
import com.goomena.app.tools.Log;
import com.goomena.app.tools.Utils;
import com.goomena.fragments.FragCredits;
import com.goomena.fragments.FragEditProfile;
import com.goomena.fragments.FragGridProfile;
import com.goomena.fragments.FragLikes;
import com.goomena.fragments.FragLoading;
import com.goomena.fragments.FragMessageView;
import com.goomena.fragments.FragMessages;
import com.goomena.fragments.FragMode;
import com.goomena.fragments.FragMyProfile;
import com.goomena.fragments.FragOffers;
import com.goomena.fragments.FragPreSearch;
import com.qs.samsungbadger.Badge;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Display;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.CompoundButton.OnCheckedChangeListener;


public class ActHome extends Activity
{
	InputMethodManager imm;
	private final static String TAG_FRAGMENT = "TAG_FRAGMENT";
	RelativeLayout relativeLayout01,relativeLayout02,relativeLayout03,relativeLayout04;
	ImageView imageMode,imagetriangle1,imagetriangle2,imagetriangle3,imagetriangle4;
	ImageView actin_bar_imageGoo;
	
	RelativeLayout relativefragment_place;
	ImageView new_menuImageView,new_menuImagebar;
	ObservableScrollView new_menuScrollView;
	RelativeLayout new_menuRelativeUnder,new_menuRelativeUnderBlack;
	
	
	RelativeLayout gradient_drawable_top;
	
	
	LinearLayout new_menuItemSearch;
	ImageView new_menuItemImageSearch;
	LinearLayout new_menuItemOnline;
	ImageView new_menuItemImageOnline;
	LinearLayout new_menuItemMessages;
	TextView new_menuItemTextMessages;
	LinearLayout new_menuItemLikes;
	TextView new_menuItemTextLikes;
	LinearLayout new_menuItemBids;
	TextView new_menuItemTextBids;
	LinearLayout new_menuItemMode;
	ImageView new_menuItemImageMode;
	TextView new_menuItemTextMode;
	LinearLayout new_menuItemMyfavlist;
	LinearLayout new_menuItemAccessmephoto;
	LinearLayout new_menuItemFavme;
	LinearLayout new_menuItemAccesswhomphoto;
	LinearLayout new_menuItemSeenmyprofile;
	LinearLayout new_menuItemSaved_profiles;
	LinearLayout new_menuItemPeople_who_have_blocked;
	LinearLayout new_menuItemAccount_settings;
	LinearLayout new_menuItemCredits;
	TextView new_menuItemTextCredits;
	LinearLayout new_menuItemLogout;
	
	LinearLayout new_menuLinearBack;
	RelativeLayout homeBackBack;
	
	Display display;
	
	Panel panel;
	LiveNotification LiveNotific;
	
	TextView action_bar_text_notifications,action_bar_text_numMessages;
	
	
	SharedPreferences settings;
	Handler handler = new Handler();
	
	AlertDialog alertRate;
	
	FragLoading fragLoading;
	
	AsyncTask<String, Void, Void> mRegisterTask;
	
	UserProfile datauserprofile;
		
	@SuppressLint({ "NewApi", "HandlerLeak" })
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		
		
		
		
		
		SharedPreferences preferences = getSharedPreferences("Lengua", MODE_PRIVATE);
		int oro = preferences.getInt("lan", 0);
		if ( oro == 0 ) {
			Global.Lang = "US";
			Locale locale = new Locale("en"); 
			Locale.setDefault(locale);
			Configuration config = new Configuration();
			config.locale = locale;
			getBaseContext().getResources().updateConfiguration(config, 
				      getBaseContext().getResources().getDisplayMetrics());
			setContentView(R.layout.act_home);
		} else if ( oro == 1 ) {
			Global.Lang = "GR";
			Locale locale = new Locale("el"); 
			Locale.setDefault(locale);
			Configuration config = new Configuration();
			config.locale = locale;
			getBaseContext().getResources().updateConfiguration(config, 
				      getBaseContext().getResources().getDisplayMetrics());
			setContentView(R.layout.act_home);	
		} else if ( oro == 2 ) {
			Global.Lang = "TR";
			Locale locale = new Locale("tr"); 
			Locale.setDefault(locale);
			Configuration config = new Configuration();
			config.locale = locale;
			getBaseContext().getResources().updateConfiguration(config, 
				      getBaseContext().getResources().getDisplayMetrics());
			setContentView(R.layout.act_home);
		} else if ( oro == 3 ) {
			Global.Lang = "ES";
			Locale locale = new Locale("es"); 
			Locale.setDefault(locale);
			Configuration config = new Configuration();
			config.locale = locale;
			getBaseContext().getResources().updateConfiguration(config, 
				      getBaseContext().getResources().getDisplayMetrics());
			setContentView(R.layout.act_home);
		} else {
			setContentView(R.layout.act_home);
		}
		
		
		
		
		
		
		
        /*
        if(Locale.getDefault().getLanguage().equals("el"))
        {
        	Global.Lang = "GR";
        }
        else if(Locale.getDefault().getLanguage().equals("en"))
        {
        	Global.Lang = "US";
        }*/
        
		
		
		//gia na stelnei notification, alla prepei na to allaxw
		new saveSettingsTask("1","1","1").execute(Utils.getSharedPreferences(ActHome.this).getString("userid", ""));
        Global.MyId = Utils.getSharedPreferences(ActHome.this).getString("userid", "");
        
        //Fragment fr = new FragHome();
        Fragment fr = new FragGridProfile(0);
        FragmentManager fm = getFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.add(R.id.fragment_place, fr);
        fragmentTransaction.commit();
     	
        
        relativefragment_place = (RelativeLayout) findViewById(R.id.relativefragment_place);
        new_menuImageView = (ImageView) findViewById(R.id.new_menuImageView);
        new_menuImagebar = (ImageView) findViewById(R.id.new_menuImagebar);
    	new_menuScrollView = (ObservableScrollView) findViewById(R.id.new_menuScrollView);
    	new_menuRelativeUnder = (RelativeLayout) findViewById(R.id.new_menuRelativeUnder);
    	new_menuRelativeUnderBlack = (RelativeLayout) findViewById(R.id.new_menuRelativeUnderBlack);
    	
    	gradient_drawable_top = (RelativeLayout) findViewById(R.id.gradient_drawable_top);
    	new_menuItemSearch = (LinearLayout) findViewById(R.id.new_menuItemSearch);
    	new_menuItemImageSearch = (ImageView) findViewById(R.id.new_menuItemImageSearch);
    	new_menuItemOnline = (LinearLayout) findViewById(R.id.new_menuItemOnline);
    	new_menuItemImageOnline = (ImageView) findViewById(R.id.new_menuItemImageOnline);
    	new_menuItemMessages = (LinearLayout) findViewById(R.id.new_menuItemMessages);
    	new_menuItemTextMessages = (TextView) findViewById(R.id.new_menuItemTextMessages);
    	new_menuItemLikes = (LinearLayout) findViewById(R.id.new_menuItemLikes);
    	new_menuItemTextLikes = (TextView) findViewById(R.id.new_menuItemTextLikes);
    	new_menuItemBids = (LinearLayout) findViewById(R.id.new_menuItemBids);
    	new_menuItemTextBids = (TextView) findViewById(R.id.new_menuItemTextBids);
    	new_menuItemMode = (LinearLayout) findViewById(R.id.new_menuItemMode);
    	new_menuItemImageMode = (ImageView) findViewById(R.id.new_menuItemImageMode);
    	new_menuItemTextMode = (TextView) findViewById(R.id.new_menuItemTextMode);
    	new_menuItemMyfavlist = (LinearLayout) findViewById(R.id.new_menuItemMyfavlist);
    	new_menuItemAccessmephoto = (LinearLayout) findViewById(R.id.new_menuItemAccessmephoto);
    	new_menuItemFavme = (LinearLayout) findViewById(R.id.new_menuItemFavme);
    	new_menuItemAccesswhomphoto = (LinearLayout) findViewById(R.id.new_menuItemAccesswhomphoto);
    	new_menuItemSeenmyprofile = (LinearLayout) findViewById(R.id.new_menuItemSeenmyprofile);
    	new_menuItemSaved_profiles = (LinearLayout) findViewById(R.id.new_menuItemSaved_profiles);
    	new_menuItemPeople_who_have_blocked = (LinearLayout) findViewById(R.id.new_menuItemPeople_who_have_blocked);
    	new_menuItemAccount_settings = (LinearLayout) findViewById(R.id.new_menuItemAccount_settings);
    	new_menuItemCredits = (LinearLayout) findViewById(R.id.new_menuItemCredits);
    	new_menuItemLogout = (LinearLayout) findViewById(R.id.new_menuItemLogout);
    	new_menuLinearBack = (LinearLayout) findViewById(R.id.new_menuLinearBack);
    	new_menuItemTextCredits = (TextView) findViewById(R.id.new_menuItemTextCredits);
    	homeBackBack = (RelativeLayout) findViewById(R.id.homeBackBack);
    	
    	
    	
    	
    	//AppRater.showRateDialog(this, null);   //auto einai gia test
		AppRater.app_launched(this);
		
		
    	if(Utils.getSharedPreferences(this).getString("genderid","").contains("2"))
		{
    		new_menuItemImageSearch.setImageResource(R.drawable.nm_search_woman);
    		new_menuItemImageOnline.setImageResource(R.drawable.nm_online_woman);
    		new_menuItemMode.setVisibility(View.GONE);
    		new_menuItemCredits.setVisibility(View.GONE);
		}
    	new_menuItemSaved_profiles.setVisibility(View.GONE);
    	
    	
    	new_menuItemSearch.setOnClickListener(clickListenermenu);
    	new_menuItemOnline.setOnClickListener(clickListenermenu);
    	new_menuItemMessages.setOnClickListener(clickListenermenu);
    	new_menuItemLikes.setOnClickListener(clickListenermenu);
    	new_menuItemBids.setOnClickListener(clickListenermenu);
    	new_menuItemMode.setOnClickListener(clickListenermenu);
    	new_menuItemMyfavlist.setOnClickListener(clickListenermenu);
    	new_menuItemAccessmephoto.setOnClickListener(clickListenermenu);
    	new_menuItemFavme.setOnClickListener(clickListenermenu);
    	new_menuItemAccesswhomphoto.setOnClickListener(clickListenermenu);
    	new_menuItemSeenmyprofile.setOnClickListener(clickListenermenu);
    	new_menuItemSaved_profiles.setOnClickListener(clickListenermenu);
    	new_menuItemPeople_who_have_blocked.setOnClickListener(clickListenermenu);
    	new_menuItemAccount_settings.setOnClickListener(clickListenermenu);
    	new_menuItemCredits.setOnClickListener(clickListenermenu);
    	new_menuItemLogout.setOnClickListener(clickListenermenu);
    	new_menuLinearBack.setOnClickListener(clickListenermenu);
    	
    	display = getWindowManager().getDefaultDisplay();
    	//Log.v("APP", "aaaaaas " +display.getWidth()/1.6);
    	
    	new_menuScrollView.setScrollViewListener(new ScrollViewListener() {
			
			@Override
			public void onScrollStopped() {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onScrollChanged(ScrollView scrollView, int x, int y, int oldx, int oldy) {
				// TODO Auto-generated method stub
				new_menuImageView.setTranslationY(y+100);
				
				//Log.v("APP", "gwrgwregwerge "+new_menuScrollView.getVerticalScrollbarPosition());
				
				if(y<10)
				{
					gradient_drawable_top.setVisibility(View.GONE);
				}
				else
				{
					gradient_drawable_top.setVisibility(View.VISIBLE);
				}
			}
		});
    	

    	
    	
    	
    	new_menuImagebar.setOnTouchListener(new OnTouchListener() {
			
			@SuppressLint("ClickableViewAccessibility") 
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				
				switch(event.getAction())
				{
					case MotionEvent.ACTION_DOWN:
		                return true;
		           
		            case MotionEvent.ACTION_MOVE:
		            	new_menuScrollView.setScrollY(Math.round(event.getY()-100) );
		            	
		                return true;
		            case MotionEvent.ACTION_UP:
		                return true;
		            }
		            return false;
			}
		});
        
        
        action_bar_text_notifications = (TextView) findViewById(R.id.action_bar_text_notifications);
        action_bar_text_numMessages = (TextView) findViewById(R.id.action_bar_text_numMessages);
     	imagetriangle1 = (ImageView) findViewById(R.id.imagetriangle1);
     	imagetriangle2 = (ImageView) findViewById(R.id.imagetriangle2);
     	imagetriangle3 = (ImageView) findViewById(R.id.imagetriangle3);
     	imagetriangle4 = (ImageView) findViewById(R.id.imagetriangle4);
     	
     	
     	
     	relativeLayout01 = (RelativeLayout) findViewById(R.id.relativeLayout01);
     	relativeLayout02 = (RelativeLayout) findViewById(R.id.relativeLayout02);
     	relativeLayout03 = (RelativeLayout) findViewById(R.id.relativeLayout03);
     	relativeLayout04 = (RelativeLayout) findViewById(R.id.relativeLayout04);
     	relativeLayout01.setOnClickListener(clickListener);
     	relativeLayout02.setOnClickListener(clickListener);
     	relativeLayout03.setOnClickListener(clickListener);
     	relativeLayout04.setOnClickListener(clickListener);
     	
     	fragLoading = new FragLoading(this);
     	
     	settings = getSharedPreferences(Utils.SharedPrefsKey, 0);
     	
     	panel = new Panel(ActHome.this,imagetriangle3);
     	//LiveNotific = new LiveNotification(this);
     	
     	//serverThread();
     	
		int edit = settings.getInt("Mode",0);
		selectMode(edit);
		
		initAlertDialog();
		initMessageReceiver();
		
		Controller.refreshCredits(ActHome.this);
		Controller.refreshCounters(ActHome.this);
		
		Controller.handlerCounters = new Handler()
		{
			public void handleMessage(Message msg) 
	  		{	
				Controller.refreshCredits(ActHome.this);
				Controller.refreshCounters(ActHome.this);
				Panel.refresh();
	  		}
		};
		
		
		
		
		
		/*
		Intent intent = new Intent();

		intent.setAction("com.sonyericsson.home.action.UPDATE_BADGE");
		intent.putExtra("com.sonyericsson.home.intent.extra.badge.ACTIVITY_NAME", "com.newgoomena.app.ActSplash");
		intent.putExtra("com.sonyericsson.home.intent.extra.badge.SHOW_MESSAGE", true);
		intent.putExtra("com.sonyericsson.home.intent.extra.badge.MESSAGE", "99");
		intent.putExtra("com.sonyericsson.home.intent.extra.badge.PACKAGE_NAME", "com.newgoomena.app");

		getApplicationContext().sendBroadcast(intent);
		*/
		
		/*
		Intent myLauncherIntent = new Intent();
		myLauncherIntent.setClassName("com.newgoomena.app", "com.newgoomena.app.ActSplash");
		myLauncherIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		
		
		Intent intent = new Intent();
		intent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, myLauncherIntent);
		intent.putExtra(Intent.EXTRA_SHORTCUT_NAME, "newGoomena");
		intent.putExtra
		       (
		        Intent.EXTRA_SHORTCUT_ICON_RESOURCE,
		        Intent.ShortcutIconResource.fromContext
		                                    (
		                                         getApplicationContext(), 
		                                         R.drawable.bid
		                                    )
		       );
		intent.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
		
		getApplicationContext().sendBroadcast(intent);
		*/
		
		
		
		//gia na mhn einai adeio to Mail giati to xreiazomaste sto edit profile
		if(Utils.getSharedPreferences(this).getString("Email", "").equals(""))
		{
			new disconectGCMTask().execute();
		}
	}
	
	
	
	
	
	//FragMessageView
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) 
	{
		if(FragMessageView.popupWindow != null)
		{
			if (FragMessageView.popupWindow.isShowing()) 
			{
				FragMessageView.popupWindow.dismiss();
				return false;
			} 
			else 
			{
				return super.onKeyDown(keyCode, event);
			}
		}
		else
		{
			return super.onKeyDown(keyCode, event);
		}
	}
	
	
	
	
	
	@SuppressLint("NewApi") 
	public void onActivityResult(int requestCode, int resultCode, Intent data) 
	{
		super.onActivityResult(requestCode,resultCode,data);

		if(requestCode==1999) //gia tis private foto, gia na valei standar mnm
		{
			if(data!=null)
			{
				datauserprofile = (UserProfile) data.getExtras().get("result");
				String ttlevel = (String) data.getExtras().get("resultlevel");
				Fragment fr;
				fr = new FragMessageView(null,datauserprofile,0,ttlevel);
				FragmentManager fm = getFragmentManager();
			
				FragmentTransaction fragmentTransaction = fm.beginTransaction();
				fragmentTransaction.setCustomAnimations(R.anim.left_slidein_list_animator, R.anim.left_slideout_list_animator,R.anim.right_slidein_list_animator, R.anim.right_slideout_list_animator);
				fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
				fragmentTransaction.addToBackStack(null);
				fragmentTransaction.commit();
			}
		}
	}
	OnClickListener clickListenermenu = new OnClickListener() 
	{
		@SuppressLint("NewApi") 
		@Override
		public void onClick(View v) 
		{
			if(v == new_menuItemSearch)
			{
				nemuOn(false);
				
				Fragment fr = new FragPreSearch();
				FragmentManager fm = getFragmentManager();
				fm.popBackStack(); fm.popBackStack(); fm.popBackStack(); fm.popBackStack(); fm.popBackStack(); fm.popBackStack();
		        FragmentTransaction fragmentTransaction = fm.beginTransaction(); 
		        fragmentTransaction.setCustomAnimations(R.anim.left_slidein_list_animator, R.anim.left_slideout_list_animator,R.anim.right_slidein_list_animator, R.anim.right_slideout_list_animator);
		        fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
		        fragmentTransaction.addToBackStack(null);
		        fragmentTransaction.commit();
		        
			}
			else if(v == new_menuItemOnline)
			{
				nemuOn(false);
				
				Fragment fr = new FragGridProfile(0);
				FragmentManager fm = getFragmentManager();
				fm.popBackStack(); fm.popBackStack(); fm.popBackStack(); fm.popBackStack(); fm.popBackStack(); fm.popBackStack();
		        FragmentTransaction fragmentTransaction = fm.beginTransaction(); 
		        fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
		        fragmentTransaction.addToBackStack(null);
		        fragmentTransaction.commit();
			}
			else if(v == new_menuItemMessages)
			{
				nemuOn(false);
				
				Fragment fr = new FragMessages();
				FragmentManager fm = getFragmentManager();
				fm.popBackStack(); fm.popBackStack(); fm.popBackStack(); fm.popBackStack(); fm.popBackStack(); fm.popBackStack();
		        FragmentTransaction fragmentTransaction = fm.beginTransaction(); 
		        fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
		        fragmentTransaction.addToBackStack(null);
		        fragmentTransaction.commit();
			}
			else if(v == new_menuItemLikes)
			{
				nemuOn(false);
				
				Fragment fr = new FragLikes();
				FragmentManager fm = getFragmentManager();
				fm.popBackStack(); fm.popBackStack(); fm.popBackStack(); fm.popBackStack(); fm.popBackStack(); fm.popBackStack();
		        FragmentTransaction fragmentTransaction = fm.beginTransaction(); 
		        fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
		        fragmentTransaction.addToBackStack(null);
		        fragmentTransaction.commit();
			}
			else if(v == new_menuItemBids)
			{
				nemuOn(false);
				
				Fragment fr = new FragOffers();
				FragmentManager fm = getFragmentManager();
				fm.popBackStack(); fm.popBackStack(); fm.popBackStack(); fm.popBackStack(); fm.popBackStack(); fm.popBackStack();
		        FragmentTransaction fragmentTransaction = fm.beginTransaction(); 
		        fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
		        fragmentTransaction.addToBackStack(null);
		        fragmentTransaction.commit();
			}
			else if(v == new_menuItemMode)
			{
				nemuOn(false);
				
				Fragment fr = new FragMode();
				FragmentManager fm = getFragmentManager();
				fm.popBackStack(); fm.popBackStack(); fm.popBackStack(); fm.popBackStack(); fm.popBackStack(); fm.popBackStack();
		        FragmentTransaction fragmentTransaction = fm.beginTransaction(); 
		        fragmentTransaction.setCustomAnimations(R.anim.left_slidein_list_animator, R.anim.left_slideout_list_animator,R.anim.right_slidein_list_animator, R.anim.right_slideout_list_animator);
		        fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
		        fragmentTransaction.addToBackStack(null);
		        fragmentTransaction.commit();
			}
			else if(v == new_menuItemMyfavlist)
			{
				nemuOn(false);
				
				Fragment fr = new FragGridProfile(1);
				FragmentManager fm = getFragmentManager();
				fm.popBackStack(); fm.popBackStack(); fm.popBackStack(); fm.popBackStack(); fm.popBackStack(); fm.popBackStack();
		        FragmentTransaction fragmentTransaction = fm.beginTransaction(); 
		        fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
		        fragmentTransaction.addToBackStack(null);
		        fragmentTransaction.commit();
			}
			else if(v == new_menuItemAccessmephoto)
			{
				nemuOn(false);
				
				Fragment fr = new FragGridProfile(5);
				FragmentManager fm = getFragmentManager();
				fm.popBackStack(); fm.popBackStack(); fm.popBackStack(); fm.popBackStack(); fm.popBackStack(); fm.popBackStack();
		        FragmentTransaction fragmentTransaction = fm.beginTransaction(); 
		        fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
		        fragmentTransaction.addToBackStack(null);
		        fragmentTransaction.commit();
			}
			else if(v == new_menuItemFavme)
			{
				nemuOn(false);
				
				Fragment fr = new FragGridProfile(3);
				FragmentManager fm = getFragmentManager();
				fm.popBackStack(); fm.popBackStack(); fm.popBackStack(); fm.popBackStack(); fm.popBackStack(); fm.popBackStack();
		        FragmentTransaction fragmentTransaction = fm.beginTransaction(); 
		        fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
		        fragmentTransaction.addToBackStack(null);
		        fragmentTransaction.commit();
			}
			else if(v == new_menuItemAccesswhomphoto)
			{
				nemuOn(false);
				
				Fragment fr = new FragGridProfile(4);
				FragmentManager fm = getFragmentManager();
				fm.popBackStack(); fm.popBackStack(); fm.popBackStack(); fm.popBackStack(); fm.popBackStack(); fm.popBackStack();
		        FragmentTransaction fragmentTransaction = fm.beginTransaction(); 
		        fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
		        fragmentTransaction.addToBackStack(null);
		        fragmentTransaction.commit();
			}
			else if(v == new_menuItemSeenmyprofile)
			{
				nemuOn(false);
				
				Fragment fr = new FragGridProfile(2);
				FragmentManager fm = getFragmentManager();
				fm.popBackStack(); fm.popBackStack(); fm.popBackStack(); fm.popBackStack(); fm.popBackStack(); fm.popBackStack();
		        FragmentTransaction fragmentTransaction = fm.beginTransaction(); 
		        fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
		        fragmentTransaction.addToBackStack(null);
		        fragmentTransaction.commit();
			}
			else if(v == new_menuItemSaved_profiles)
			{
				nemuOn(false);
				
				Fragment fr = new FragGridProfile(5);
				FragmentManager fm = getFragmentManager();
				fm.popBackStack(); fm.popBackStack(); fm.popBackStack(); fm.popBackStack(); fm.popBackStack(); fm.popBackStack();
		        FragmentTransaction fragmentTransaction = fm.beginTransaction(); 
		        fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
		        fragmentTransaction.addToBackStack(null);
		        fragmentTransaction.commit();
			}
			else if(v == new_menuItemPeople_who_have_blocked)
			{
				nemuOn(false);
				
				Fragment fr = new FragGridProfile(6);
				FragmentManager fm = getFragmentManager();
				fm.popBackStack(); fm.popBackStack(); fm.popBackStack(); fm.popBackStack(); fm.popBackStack(); fm.popBackStack();
		        FragmentTransaction fragmentTransaction = fm.beginTransaction(); 
		        fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
		        fragmentTransaction.addToBackStack(null);
		        fragmentTransaction.commit();
			}
			else if(v == new_menuItemAccount_settings)
			{
				nemuOn(false);
				
				Fragment fr = new FragEditProfile();
				FragmentManager fm = getFragmentManager();
				fm.popBackStack(); fm.popBackStack(); fm.popBackStack(); fm.popBackStack(); fm.popBackStack(); fm.popBackStack();
		        FragmentTransaction fragmentTransaction = fm.beginTransaction(); 
		        fragmentTransaction.setCustomAnimations(R.anim.left_slidein_list_animator, R.anim.left_slideout_list_animator,R.anim.right_slidein_list_animator, R.anim.right_slideout_list_animator);
		        fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
		        fragmentTransaction.addToBackStack(null);
		        fragmentTransaction.commit();
			}
			else if(v == new_menuItemCredits)
			{
				nemuOn(false);
				
				Fragment fr = new FragCredits();
				FragmentManager fm = getFragmentManager();
				fm.popBackStack(); fm.popBackStack(); fm.popBackStack(); fm.popBackStack(); fm.popBackStack(); fm.popBackStack();
		        FragmentTransaction fragmentTransaction = fm.beginTransaction(); 
		        fragmentTransaction.setCustomAnimations(R.anim.left_slidein_list_animator, R.anim.left_slideout_list_animator,R.anim.right_slidein_list_animator, R.anim.right_slideout_list_animator);
		        fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
		        fragmentTransaction.addToBackStack(null);
		        fragmentTransaction.commit();
			}
			else if(v == new_menuItemLogout)
			{
				new disconectGCMTask().execute();
			}
			else if(v == new_menuLinearBack)
			{
				nemuOn(false);
			}
		}
	};
	
	@SuppressWarnings("deprecation")
	@SuppressLint("NewApi") 
	public void nemuOn(Boolean on)
	{
		if(on)
		{
			relativefragment_place.animate().setDuration(400).scaleX(0.8f).scaleY(0.8f).translationX((display.getWidth()*10)/16);
			new_menuRelativeUnderBlack.animate().setDuration(800).alpha(0.0f).setListener(new AnimatorListener() {
				@Override
				public void onAnimationStart(Animator animation) { new_menuRelativeUnder.setVisibility(View.VISIBLE); }
				@Override
				public void onAnimationRepeat(Animator animation) {}
				@Override
				public void onAnimationEnd(Animator animation) { new_menuRelativeUnder.setVisibility(View.VISIBLE); }
				@Override
				public void onAnimationCancel(Animator animation) {}
			});
			
			homeBackBack.setVisibility(RelativeLayout.VISIBLE);
			homeBackBack.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					nemuOn(false);
				}
			});
		}
		else
		{
			new_menuRelativeUnderBlack.animate().setDuration(250).alpha(0.8f).setListener(new AnimatorListener() {
				@Override
				public void onAnimationStart(Animator animation) {}
				@Override
				public void onAnimationRepeat(Animator animation) {}
				@Override
				public void onAnimationEnd(Animator animation) { new_menuRelativeUnder.setVisibility(View.GONE); }
				@Override
				public void onAnimationCancel(Animator animation) {}
			});
			relativefragment_place.animate().setDuration(400).scaleX(1.0f).scaleY(1.0f).translationX(0);
			homeBackBack.setOnClickListener(null);
			homeBackBack.setVisibility(RelativeLayout.GONE);
		}
	}
	
	
	OnClickListener clickListener = new OnClickListener() 
	{
		Fragment fr;

		@SuppressLint("NewApi")
		@Override
		public void onClick(View v) 
		{
			if(v == relativeLayout01)
			{
				fragLoading.Close();
				//linearlayout_act_include_home.setVisibility(View.VISIBLE);
				//linearlayout_act_include_mode.setVisibility(View.GONE);
				imagetriangle1.setVisibility(View.GONE);
				imagetriangle2.setVisibility(View.GONE);
				imagetriangle3.setVisibility(View.GONE);
				imagetriangle4.setVisibility(View.GONE);
				
				
				//relativefragment_place.startAnimation(AnimationUtils.loadAnimation(ActHome.this, R.anim.zoom_out));//////////////////////////////
				
				InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			    inputManager.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
			    if(FragMessageView.popupWindow != null)
				{
					if (FragMessageView.popupWindow.isShowing()) 
					{
						FragMessageView.popupWindow.dismiss();
					} 
				}
				

			    
				
//				Animation aa = AnimationUtils.loadAnimation(ActHome.this, R.anim.zoomoutnewmenu);
//				relativefragment_place.startAnimation(aa);
//				
//				aa.setAnimationListener(new AnimationListener() {
//					
//					@Override
//					public void onAnimationStart(Animation animation) {
//						// TODO Auto-generated method stub
//						
//					}
//					
//					@Override
//					public void onAnimationRepeat(Animation animation) {
//						// TODO Auto-generated method stub
//						
//					}
//					
//					@Override
//					public void onAnimationEnd(Animation animation) {
//						// TODO Auto-generated method stub
//						relativefragment_place.setScaleX(0.8f);
//						relativefragment_place.setScaleY(0.8f);
//						relativefragment_place.setTranslationX(670);
//					}
//				});
				if(relativefragment_place.getX()==0)
				{
					nemuOn(true);
				}
				else
				{
					nemuOn(false);
				}
				
				
				//relativefragment_place.setScaleX(0.8f);
				//relativefragment_place.setScaleY(0.8f);
				//relativefragment_place.setTranslationX(670);
				
				/*
				ActionBarChange.changeActionBar(ActHome.this, 0);
				
				fr = new FragHome();
				FragmentManager fm = getFragmentManager();
				fm.popBackStack(); fm.popBackStack(); fm.popBackStack(); fm.popBackStack(); fm.popBackStack(); fm.popBackStack();
		        FragmentTransaction fragmentTransaction = fm.beginTransaction();
		        fragmentTransaction.replace(R.id.fragment_place, fr);
		        fragmentTransaction.commit();
		        */
		        
		        if(panel.IsOpen()){ panel.Close(); }
			}
			else if(v == relativeLayout02)
			{
				InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			    inputManager.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
			    if(FragMessageView.popupWindow != null)
				{
					if (FragMessageView.popupWindow.isShowing()) 
					{
						FragMessageView.popupWindow.dismiss();
					} 
				}
			    
			    
				fragLoading.Close();
				//linearlayout_act_include_home.setVisibility(View.GONE);
				//linearlayout_act_include_mode.setVisibility(View.VISIBLE);
				imagetriangle1.setVisibility(View.GONE);
				imagetriangle2.setVisibility(View.GONE);
				imagetriangle3.setVisibility(View.GONE);
				imagetriangle4.setVisibility(View.GONE);
				
				
				fr = new FragMessages();
				FragmentManager fm = getFragmentManager();
				fm.popBackStack(); fm.popBackStack(); fm.popBackStack(); fm.popBackStack(); fm.popBackStack(); fm.popBackStack();
		        FragmentTransaction fragmentTransaction = fm.beginTransaction(); 
		        fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
		        fragmentTransaction.addToBackStack(null);
		        fragmentTransaction.commit();
		        
		        if(panel.IsOpen()){ panel.Close(); }
			}
			else if(v == relativeLayout03)
			{
				InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			    inputManager.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
			    if(FragMessageView.popupWindow != null)
				{
					if (FragMessageView.popupWindow.isShowing()) 
					{
						FragMessageView.popupWindow.dismiss();
					} 
				}
			    
			    
				fragLoading.Close();
				//linearlayout_act_include_home.setVisibility(View.GONE);
				//linearlayout_act_include_mode.setVisibility(View.VISIBLE);
				imagetriangle1.setVisibility(View.GONE);
				imagetriangle2.setVisibility(View.GONE);
				imagetriangle3.setVisibility(View.VISIBLE);
				imagetriangle4.setVisibility(View.GONE);
				
				if(panel.IsOpen()){ panel.Close(); }
				else { panel.Show(); }
			}
			else if(v == relativeLayout04)
			{
				InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			    inputManager.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
			    if(FragMessageView.popupWindow != null)
				{
					if (FragMessageView.popupWindow.isShowing()) 
					{
						FragMessageView.popupWindow.dismiss();
					} 
				}
			    
			    
				fragLoading.Close();
				//linearlayout_act_include_home.setVisibility(View.GONE);
				//linearlayout_act_include_mode.setVisibility(View.VISIBLE);
				imagetriangle1.setVisibility(View.GONE);
				imagetriangle2.setVisibility(View.GONE);
				imagetriangle3.setVisibility(View.GONE);
				imagetriangle4.setVisibility(View.VISIBLE);

				
				fr = new FragMyProfile();
				FragmentManager fm = getFragmentManager();
				fm.popBackStack(); fm.popBackStack(); fm.popBackStack(); fm.popBackStack(); fm.popBackStack(); fm.popBackStack();
		        FragmentTransaction fragmentTransaction = fm.beginTransaction(); 
		        fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
		        fragmentTransaction.addToBackStack(null);
		        fragmentTransaction.commit();
				
				if(panel.IsOpen()){ panel.Close(); }
			}
		}
	};
	
	class disconectGCMTask extends AsyncTask<String, Void, Void>
	{
		@Override
		protected void onPreExecute(){
			fragLoading.Show();
		}
		
		@Override
		protected Void doInBackground(String... params){
			String regId = Utils.getSharedPreferences(ActHome.this).getString("reg", "");
			Log.v("APP", "wergwg " +regId);
			ServiceClient.disconectGCM(regId);
			return null;
		}
		
		@SuppressLint("InlinedApi") 
		@Override
		protected void onPostExecute(Void v)
		{
			Context context = ActHome.this.getApplicationContext();
			if (Badge.isBadgingSupported(context)) 
			{
				Badge.deleteAllBadges(context);
			}
			
			new saveSettingsTask("0","0","0").execute(Utils.getSharedPreferences(ActHome.this).getString("userid", ""));
			
			fragLoading.Close();
			ProfileManager.disconnect(ActHome.this);
			GCMRegistrar.unregister(getApplicationContext());
		//	unregisterReceiver(mHandleMessageReceiver);//////////////////////
			//GCMRegistrar.onDestroy(getApplicationContext());////
			GCMRegistrar.setRegisteredOnServer(getApplicationContext(), false);
			Intent in = new Intent(ActHome.this,ActSplash.class);
			//in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
			ActHome.this.startActivity(in);
		}
	}
	
	
	
	@SuppressLint("NewApi") 
	@Override
	public void onBackPressed() 
	{
		if(panel.IsOpen() || relativefragment_place.getX()!=0)
		{ 
			if(panel.IsOpen())
			{
				panel.Close(); 
			}
			
			if(relativefragment_place.getX()!=0)
			{
				nemuOn(false);
			}
			
		}
		else 
		{ 
			super.onBackPressed();
		}
		
		
		
	    //final Fragment fragment = (Fragment) getFragmentManager().findFragmentByTag(TAG_FRAGMENT);
	    //if (fragment.allowBackPressed()) 
	    //{ 
	    	// and then you define a method allowBackPressed with the logic to allow back pressed or not
	      //  super.onBackPressed();
	    //}
		//ActHome.this.finish();
	}
	

	void selectMode(int mode)
	{
		if(mode == 1)
		{
			new_menuItemImageMode.setImageResource(R.drawable.nm_mode_nice_free);
			new_menuItemTextMode.setText("("+getResources().getString(R.string.normal)+")");
		}
		else if(mode == 2)
		{
			new_menuItemImageMode.setImageResource(R.drawable.nm_mode_bussiness);
			new_menuItemTextMode.setText("("+getResources().getString(R.string.discreet)+")");
		}
		else if(mode == 3)
		{
			new_menuItemImageMode.setImageResource(R.drawable.nm_mode_with_wife);
			new_menuItemTextMode.setText("("+getResources().getString(R.string.dangerous_situation)+")");
		}
		else if(mode == 4)
		{
			new_menuItemImageMode.setImageResource(R.drawable.nm_mode_spy);
			new_menuItemTextMode.setText("("+getResources().getString(R.string.masquerading)+")");
		}
	}
	
	
	class saveSettingsTask extends AsyncTask<String, Void, Void>{

		String notifMessages,notifLikes,notifOffers;
		
		public saveSettingsTask(String s1,String s2,String s3) {
			notifMessages = s1;
			notifLikes = s2;
			notifOffers = s3;
		}
		
		@Override
		protected void onPreExecute(){
			//Toast.makeText(ActSettings.this, getResources().getString(R.string.savesettings), Toast.LENGTH_LONG).show();
		}
		
		@Override
		protected Void doInBackground(String... params) {
			ServiceClient.saveGCMsettings(params[0], notifMessages, notifLikes, notifOffers);
			
			return null;
		}
		
		@Override
		protected void onPostExecute(Void v){
		}
		
	}
	
	/*
	public void serverThread()
	{
		
		
		GlobalOffers.getOffersCount(ActHome.this);
		GlobalLikes.getLikesCount();
		  GlobalMessages.getMessagesCount();
		  if(GlobalMessages.messageInfosFull==null)
		  {
		  GlobalMessages.getAllMessages();////////////////
		  }
		Thread thread = new Thread() 
	      { 
	          @Override
	          public void run()  
	          { 
	              try 
	              { 
	                  while(true) 
	                  {   
	                      handler.post(new Runnable() 
	                      { 
	                          public void run() 
	                          { 
	                        	  if(Utils.isNetworkConnected(ActHome.this))
	                        	  {
	                        		  //GlobalLikes.getLikesCount();
	                        		  //GlobalMessages.getMessagesCount();
	                        		  //GlobalMessages.getAllMessages();
	                        	  }
	                        	  else
	                        	  {
	                        		  Utils.showNoInternet(ActHome.this);
	                        	  }
	                          } 
	                      }); 
	                      sleep(40000);
	                  } 
	              }  
	              catch (InterruptedException e)  
	              { 
	                  e.printStackTrace(); 
	              } 
	          } 
	      }; 
	      thread.start();
	}
	*/
	
	public void initMessageReceiver()
	{
		
		//Make sure the device has the proper dependencies.
        GCMRegistrar.checkDevice(ActHome.this);
 
        // Make sure the manifest was properly set - comment out this line
        // while developing the app, then uncomment it when it's ready.
        GCMRegistrar.checkManifest(ActHome.this);
 
                 
        registerReceiver(mHandleMessageReceiver, new IntentFilter(DISPLAY_MESSAGE_ACTION));
        
        
       // GCMRegistrar.unregister(getApplicationContext());
       
        // Get GCM registration id
       final String regId = GCMRegistrar.getRegistrationId(ActHome.this);
    //   Toast.makeText(getApplicationContext(), regId, Toast.LENGTH_LONG).show();////////////////////////////////
       
       // Check if regid already presents
       if (regId.equals("")) 
       {
           // Registration is not present, register now with GCM       
       	Log.v("APP", "wwwr GCMRegistrar ok");
           GCMRegistrar.register(getApplicationContext(), SENDER_ID);
       }
       else 
       {
       	Log.v("APP", "wwwr GCMRegistrar NOT ok");
           // Device is already registered on GCM
           if (GCMRegistrar.isRegisteredOnServer(this)) 
           {
               // Skips registration.              
           	
              // Toast.makeText(getApplicationContext(), "Already registered with GCM", Toast.LENGTH_LONG).show();////////////////////////
           } 
           else 
           {
               // Try to register again, but not in the UI thread.
               // It's also necessary to cancel the thread onDestroy(),
               // hence the use of AsyncTask instead of a raw thread.
               final Context context = ActHome.this;
               
               
               mRegisterTask = new AsyncTask<String, Void, Void>() 
               {

                   @Override
                   protected Void doInBackground(String... params) 
                   {
                       // Register on our server
                       // On server creates a new user
                   	
                   	                    	
                   	 SharedPreferences prefs = Utils.getSharedPreferences((Activity)context);
                        String userid = prefs.getString("userid", "");
                       
                        String loginName = prefs.getString("username", "");
                       
                        prefs.edit().putString("reg", params[0]).commit();
                        
                        ServerUtilities.storeUser(context, userid, loginName, "", params[0]);
                   	Log.v("APP", "Stored user to database for notifications...");
                       return null;
                   }

                   @Override
                   protected void onPostExecute(Void result) {
                       mRegisterTask = null;
                   }

               };
             //  Utils.getSharedPreferences((Activity)context).edit().putString("reg", ""+regId).commit();
               mRegisterTask.execute(regId, null, null);
               
               
            
           }
       }
		
		
		
	}
	
	/**
     * Receiving push messages
     * */
    private final BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() 
    {
        @Override
        public void onReceive(Context context, Intent intent) 
        {
            String newMessage = intent.getExtras().getString(EXTRA_MESSAGE);
            // Waking up mobile if it is sleeping
            WakeLocker.acquire(getApplicationContext());
             
            /**
             * Take appropriate action on this message
             * depending upon your app requirement
             * For now i am just displaying it on the screen
             * */
             
            // Showing received message
            Toast.makeText(getApplicationContext(), newMessage, Toast.LENGTH_LONG).show();
             
            // Releasing wake lock
            WakeLocker.release();
        }
    };
    
    
	@SuppressLint("InflateParams") 
	private void initAlertDialog(){
		
		
		alertRate = new AlertDialog.Builder(this).create();
		alertRate.setIcon(R.drawable.iconpopup);
		alertRate.setTitle(getResources().getString(R.string.plzrateus));
		View v = getLayoutInflater().inflate(R.layout.menu_rate, null);
		alertRate.setView(v);
		
		TextView okButton = (TextView) v.findViewById(R.id.menuOk);
		TextView laterButton = (TextView)v.findViewById(R.id.menuLater);
		okButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Utils.getSharedPreferences(ActHome.this).edit().putBoolean("showRatePopup", false);
				Utils.getSharedPreferences(ActHome.this).edit().commit();
				
				//Uri uri = Uri.parse("market://details?id=" + getApplicationContext().getPackageName());
				Uri uri = Uri.parse("market://details?id=" + "com.zevera.app");
				Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
				try {
				  startActivity(goToMarket);
				} catch (ActivityNotFoundException e) {
				  Toast.makeText(ActHome.this, "Couldn't launch the market", Toast.LENGTH_LONG).show();
				}
				if(!Utils.getSharedPreferences(ActHome.this).getBoolean("rememberMe", false)){
					ProfileManager.disconnect(ActHome.this);
				}
				ActHome.this.finish();
			}
		});
		
		laterButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				alertRate.dismiss();
				if(!Utils.getSharedPreferences(ActHome.this).getBoolean("rememberMe", false)){
					ProfileManager.disconnect(ActHome.this);
				}
				ActHome.this.finish();
				
			}
		});
		
		CheckBox checkAllow = (CheckBox) v.findViewById(R.id.dontshow);
		checkAllow.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(isChecked){
					Utils.getSharedPreferences(ActHome.this).edit().putBoolean("showRatePopup", false);
				}
				else{
					Utils.getSharedPreferences(ActHome.this).edit().putBoolean("showRatePopup", true);
				}
				Utils.getSharedPreferences(ActHome.this).edit().commit();
				
			}
		});
		
		
	}
    
    @Override
	protected void onDestroy() 
    {
		if (mRegisterTask != null) 
		{
			mRegisterTask.cancel(true);
		}
		try 
		{
			unregisterReceiver(mHandleMessageReceiver);
			GCMRegistrar.onDestroy(getApplicationContext());
		} 
		catch (Exception e) 
		{
			Log.e("UnRegister Receiver Error", "> " + e.getMessage());
		}
		super.onDestroy();
	}
	
}

package com.goomena.app;

import com.goomena.app.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;

public class ActFirst extends Activity 
{
	Animation animZoomIn1,animZoomIn2,animZoomIn3,animZoomIn4,animZoomIn5;
	RelativeLayout relativeLayout1,relativeLayout2,relativeLayout3,relativeLayout4,relativeLayout5;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_first);
		
		animZoomIn1 = AnimationUtils.loadAnimation(this, R.anim.zoom_in1);
     	animZoomIn2 = AnimationUtils.loadAnimation(this, R.anim.zoom_in2);
     	animZoomIn3 = AnimationUtils.loadAnimation(this, R.anim.zoom_in3);
     	animZoomIn4 = AnimationUtils.loadAnimation(this, R.anim.zoom_in4);
     	animZoomIn5 = AnimationUtils.loadAnimation(this, R.anim.zoom_in5);
     	
		relativeLayout1 = (RelativeLayout) findViewById(R.id.relativeLayout1);
     	relativeLayout2 = (RelativeLayout) findViewById(R.id.relativeLayout2);
     	relativeLayout3 = (RelativeLayout) findViewById(R.id.relativeLayout3);
     	relativeLayout4 = (RelativeLayout) findViewById(R.id.relativeLayout4);
     	relativeLayout5 = (RelativeLayout) findViewById(R.id.relativeLayout5);
     	
     	relativeLayout1.startAnimation(animZoomIn1);
     	relativeLayout2.startAnimation(animZoomIn2);
     	relativeLayout3.startAnimation(animZoomIn3);
     	relativeLayout4.startAnimation(animZoomIn4);
     	relativeLayout5.startAnimation(animZoomIn5);
     	
     	relativeLayout2.setOnClickListener(clickListener);
     	relativeLayout3.setOnClickListener(clickListener);
     	relativeLayout5.setOnClickListener(clickListener);
	}
	
	
	OnClickListener clickListener = new OnClickListener() 
	{
		@Override
		public void onClick(View v) 
		{
			if(v == relativeLayout2)
			{
				Intent intent = new Intent();
				intent.setClass(ActFirst.this, ActMeet.class);
				startActivity(intent);
				overridePendingTransition(R.anim.right_in, R.anim.left_out);
			}
			else if(v == relativeLayout3)
			{
				// TODO Auto-generated method stub
				Intent intent = new Intent();
				intent.setClass(ActFirst.this, ActLogin.class);
				startActivity(intent);
				overridePendingTransition(R.anim.right_in, R.anim.left_out);
			}
			else if(v == relativeLayout5)
			{
				// TODO Auto-generated method stub
				Intent intent = new Intent();
				intent.setClass(ActFirst.this, ActRegister1.class);
				startActivity(intent);
				overridePendingTransition(R.anim.right_in, R.anim.left_out);
			}
		}
	};
}

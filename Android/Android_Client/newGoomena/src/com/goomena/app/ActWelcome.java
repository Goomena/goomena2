package com.goomena.app;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import com.goomena.app.data.DataFetcher;
import com.goomena.app.data.ProfileManager;
import com.goomena.app.models.ImagePath;
import com.goomena.app.models.UserProfile;
import com.goomena.app.tools.Utils;
import com.goomena.fragments.ManagerOfImage;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class ActWelcome extends Activity
{
	RelativeLayout relativeLayout0;
	UserProfile myprofile;
	
	ImageView welcome_imageProfile;
	TextView welcome_username;
	
	String notif;
	private String pathOfImage = null;
	private Bitmap bitmap;
	
	Dialog Dwaiting;
	
	String Myimage;
	
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_welcome);
		
		///-----+
		Dwaiting = new Dialog(ActWelcome.this, R.style.DialogSlideAnim);
		Dwaiting.setContentView(R.layout.dialog_waiting);
		Animation animFadein = AnimationUtils.loadAnimation(ActWelcome.this, R.anim.d_wait_fr_load);
		Animation animRotScal = AnimationUtils.loadAnimation(ActWelcome.this, R.anim.d_wait_bg_logo);
		final TextView dialog_waiting_textWaiting = (TextView) Dwaiting.findViewById(R.id.dialog_waiting_textWaiting);
		ImageView dialog_waiting_bgLogo = (ImageView) Dwaiting.findViewById(R.id.dialog_waiting_bgLogo);
		dialog_waiting_textWaiting.startAnimation(animFadein);
		dialog_waiting_bgLogo.startAnimation(animRotScal);
		/////----
		
		relativeLayout0 = (RelativeLayout) findViewById(R.id.relativeLayout0);
		welcome_imageProfile = (ImageView) findViewById(R.id.welcome_imageProfile);
		welcome_username = (TextView) findViewById(R.id.welcome_username);
     	
     	relativeLayout0.setOnClickListener(clickListener);
     	
     	if(Utils.getSharedPreferences(this).getString("genderid","").contains("2"))
		{
     		relativeLayout0.setBackgroundResource(R.drawable.w_men);
		}
     	
     	Myimage = getIntent().getStringExtra("MYimage");
     	
     	myprofile = ProfileManager.loadProfile(ActWelcome.this);
     	
     	if(!Myimage.equals("NOT"))
     	{
     		Utils.getImageLoader(ActWelcome.this).displayImage(Myimage,welcome_imageProfile, Utils.getImageOptions());
     	}
     	else
     	{
     		Utils.getImageLoader(ActWelcome.this).displayImage(myprofile.ImageUrl,welcome_imageProfile, Utils.getImageOptions());
     	}
     	
     	welcome_username.setText(myprofile.UserName);
	}

	
	OnClickListener clickListener = new OnClickListener() 
	{
		@Override
		public void onClick(View v) 
		{
			
			if(v == relativeLayout0)
			{
				if(!Myimage.equals("NOT"))
		     	{
					ImagePath a = new ImagePath();
					a.Path = Myimage;
					a.Level = "0";
					new uploadImageTask(a,0,1).execute();
				}
				else
				{
					Intent intent = new Intent();
					intent.setClass(ActWelcome.this, ActHome.class);
					startActivity(intent);
				}
			}
		}
	};
	
	
	
	class uploadImageTask extends AsyncTask<Void,Void,Void>
	{
		byte[] _byteArray;
		ImagePath _imPath;
		int _position;
		int _sizelist;
		
		uploadImageTask(ImagePath imPath,int position,int sizelist)
		{
			this._imPath = imPath;
			this._position = position;
			this._sizelist = sizelist;
		}
		@Override
		protected void onPreExecute()
		{
			Dwaiting.show();
			if(_imPath.Path.indexOf("file://") > -1)
			{
				pathOfImage = _imPath.Path.replace("file://", "");
			}
			else
			{
				pathOfImage = getPath(Uri.parse(_imPath.Path));
			}
			
			ManagerOfImage manIma = new ManagerOfImage(); 
			bitmap = manIma.down_scale(pathOfImage,  800, 480);
			
			int ROTATION_DEGREE =90;
		    Matrix matrix = new Matrix();
		    int orientation = 1;
		    try 
		    {
		    	ExifInterface exif = new ExifInterface(pathOfImage);
		        orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);                  
		    } 
		    catch (IOException e) 
		    {
		    	e.printStackTrace();
		    }
		                 
		    matrix.setRotate(0);
		                  
		    switch (orientation)
		    {
		    	case ExifInterface.ORIENTATION_ROTATE_90:
		    		matrix.setRotate(ROTATION_DEGREE);
		    		break;
		                  
		        case ExifInterface.ORIENTATION_ROTATE_180:
		        	matrix.setRotate(ROTATION_DEGREE * 2); 
		        	break;
		                  
		        case ExifInterface.ORIENTATION_ROTATE_270:
		        	matrix.setRotate(-ROTATION_DEGREE);
		        	break;
		                  
		        default:
		        	break;
		    }
		    int x = bitmap.getWidth();
		    int y = bitmap.getHeight();

		    bitmap = Bitmap.createBitmap(bitmap, 0, 0, x, y, matrix, true);
			
			ByteArrayOutputStream stream = new ByteArrayOutputStream();
	        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
	        _byteArray = stream.toByteArray();
		}
				
		@Override
		protected Void doInBackground(Void... params) 
		{
			notif = DataFetcher.postImage("profileimg.jpg",_byteArray ,Utils.getSharedPreferences(ActWelcome.this).getString("userid",""),Integer.parseInt(_imPath.Level) );
			return null;
		}
		
		@Override
		protected void onPostExecute(Void r)
		{
			Dwaiting.cancel();
			Intent intent = new Intent();
			intent.setClass(ActWelcome.this, ActHome.class);
			startActivity(intent);
			//if(isAdded())
			{
				Toast toast = Toast.makeText(ActWelcome.this, notif, Toast.LENGTH_LONG);
				toast.show();
				
				
			}
		}
	}
	
	public String getPath(Uri uri) 
	{
		Cursor cursor = ActWelcome.this.getContentResolver().query(uri, null, null, null, null); 
		if(cursor!=null)
		{
			cursor.moveToFirst(); 
			int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
			return cursor.getString(idx); 
		}
		else
		{
			return null;
		}
	}
}

package com.goomena.app;

import static com.goomena.app.notifications.CommonUtilities.SENDER_ID;
import static com.goomena.app.notifications.CommonUtilities.displayMessage;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.RemoteViews;

import com.google.android.gcm.GCMBaseIntentService;
import com.goomena.app.data.ServiceClient;
import com.goomena.app.notifications.ServerUtilities;
import com.goomena.app.tools.Utils;
import com.qs.samsungbadger.Badge;
 
public class GCMIntentService extends GCMBaseIntentService 
{
 
    private static final String TAG = "GCMIntentService";
 
    public GCMIntentService() 
    {
        super(SENDER_ID);
    }
 
    /**
     * Method called on device registered
     **/
    @Override
    protected void onRegistered(Context context, String registrationId) {
        Log.v("APP", "Device registered: regId = " + registrationId);
        //displayMessage(context, "Your device registered with GCM");
        Log.d("NAME", "activity name");
        
        //ServerUtilities.register(context,"name","email", registrationId);
        SharedPreferences prefs = Utils.getSharedPreferences(context);
        String userid = prefs.getString("userid", "");
        String loginName = prefs.getString("username", "");
        prefs.edit().putString("reg", registrationId).commit();
        ServerUtilities.storeUser(context, userid, loginName, "", registrationId);
    }
 
    /**
     * Method called on device un registred
     * */
    @Override
    protected void onUnregistered(Context context, String registrationId) {
        Log.i(TAG, "Device unregistered");
        //displayMessage(context, "device unregistered");
      //  new disconectGCMTask().execute(registrationId);
        ServerUtilities.unregister(context, registrationId);
    }
    

 
    /**
     * Method called on Receiving a new message
     * */
    @Override
    protected void onMessage(Context context, Intent intent) 
    {
        Log.v("APP", "Received message aaaa");
        String message = intent.getExtras().getString("message");
         
        String fromProfileId = null;
    	String msg = null;
    	String nlc = null;
    	String nmc = null;
    	String noc = null;
    	JSONObject receivedObject = null;
    	
    	try 
    	{
			receivedObject = new JSONObject(message);
			
			//Log.v("APP", "sfdgwshwsrh "+message);
			fromProfileId = receivedObject.getString("fromProfileId");
			msg = receivedObject.getString("message");
			nlc = receivedObject.getString("newlikescount");
			nmc = receivedObject.getString("newmessagescount");
			noc = receivedObject.getString("newofferscount");
		} 
    	catch (JSONException e) 
    	{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	if(msg!=null && fromProfileId!=null)
    	{
    		Controller.refreshWithHandler();
    		
    		int allNotification = Integer.parseInt(nlc)+Integer.parseInt(nmc)+Integer.parseInt(noc);
    		//Context context = getApplicationContext();
    		if (Badge.isBadgingSupported(context)) 
    		{
    		    Badge badge = new Badge();
    		    badge.mPackage = "com.goomena.app";
    		    badge.mClass = "com.goomena.app.ActSplash"; // This should point to Activity declared as android.intent.action.MAIN
    		    //badge.mIcon = R.drawable.bid;
    		    if(allNotification!=0)
			    {
			    	badge.mBadgeCount = allNotification;
			    }
			    else
			    {
			    	Badge.deleteAllBadges(context);
			    }
    		   
    		    badge.save(context);
    		}
    		displayMessage(context, msg);
    		new getImageTask(String.valueOf( 3-Integer.parseInt(Utils.getSharedPreferences(context).getString("genderid", ""))), context,msg).execute(fromProfileId);
    		
    	}
    	else
    	{
    		// notifies user
            generateSimpleNotification(context, message);
    	}
        
        
        
    }
 
    /**
     * Method called on receiving a deleted message
     * */
    @Override
    protected void onDeletedMessages(Context context, int total) {
        Log.i(TAG, "Received deleted messages notification");
        String message = "message";
        displayMessage(context, message);
        // notifies user
        //generateNotification(context, message);
    }
 
    /**
     * Method called on Error
     * */
    @Override
    public void onError(Context context, String errorId) {
        Log.i(TAG, "Received error: " + errorId);
       // displayMessage(context, "Error");
    }
 
    @Override
    protected boolean onRecoverableError(Context context, String errorId) {
        // log message
        Log.i(TAG, "Received recoverable error: " + errorId);
      //  displayMessage(context, "recoverable error");
        return super.onRecoverableError(context, errorId);
    }
 
    /**
     * Issues a notification to inform the user that server has sent a message.
     */

    
    private void generateSimpleNotification(final Context context, String message){
    	int icon = R.drawable.iconpopup;
    	long when = System.currentTimeMillis();
        NotificationManager notificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notification = new Notification(icon, message, when);
            
        String title = context.getString(R.string.app_name);
         
        Intent notificationIntent = new Intent(context, ActSplash.class);
        // set intent so it does not start a new activity
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent intent =
                PendingIntent.getActivity(context, 0, notificationIntent, 0);
        notification.setLatestEventInfo(context, title, message, intent);
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
         
        // Play default notification sound
        notification.defaults |= Notification.DEFAULT_SOUND;
         
        // Vibrate if vibrate is enabled
        notification.defaults |= Notification.DEFAULT_VIBRATE;
        notificationManager.notify(0, notification);	  
    }

	String profileImgUrl = null; 
    
	class getImageTask extends AsyncTask<String, Void,Void>
	{
		String genderId,message;
		Context context;
		public getImageTask(String genderId,Context context,String message){
			this.genderId = genderId;
			this.context = context;
			this.message = message;
		}
		
		@Override
		protected Void doInBackground(String... params) {
			profileImgUrl = ServiceClient.getUserProfileImage(params[0], genderId );
			return null;
		}
		
		@Override
		protected void onPostExecute(Void v){
			createNotificationAction(profileImgUrl,context,message);
		}
		
	}
     
	Bitmap image = null;
	
	private void createNotificationAction(String profileImgUrl,Context context,String message) {
		
		new getBitmap(context, message).execute(profileImgUrl);
		        		
	}
	
	class getBitmap extends AsyncTask<String, Void,Void>{

		Context context;
		String message;
		public getBitmap(Context c,String m){
			context = c;
			message = m;
		}
		
		@Override
		protected Void doInBackground(String... params) {
			try {
				image = BitmapFactory.decodeStream(new URL( params[0]).openConnection().getInputStream());
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(Void v){
			if(image!=null){
				showNotificatonAction(context, message);
			}
		}
	}
	
	@SuppressLint("NewApi")
	private void showNotificatonAction(Context context,String message){
		Notification.Builder builder = new Notification.Builder(this)
        .setSmallIcon(R.drawable.iconpopup)
        .setAutoCancel(true)
        .setTicker(getResources().getString(R.string.notifticker));
        
        RemoteViews layout = new RemoteViews(getPackageName(), R.layout.item_notification);
        
        
        //Log.v("APP", "sghwrhwrh "+message);
        
        
        
        SharedPreferences prefs = Utils.getSharedPreferences(context);
        Integer Mode = prefs.getInt("Mode",1);
        Integer ModeNotif = 1,ModeTone = 1,ModeAppl = 1;
        
        
        if(Mode == 1)
        {
        	ModeTone = prefs.getInt("Mode0Tone",1);
        	ModeNotif = prefs.getInt("Mode0Notif",1);
        	ModeAppl = prefs.getInt("Mode0Appl",1);
        }
        else if(Mode == 2)
        {
        	ModeTone = prefs.getInt("Mode1Tone",1);
        	ModeNotif = prefs.getInt("Mode1Notif",2);
        	ModeAppl = prefs.getInt("Mode1Appl",1);
        }
        else if(Mode == 3)
        {
        	ModeTone = prefs.getInt("Mode2Tone",0);
        	ModeNotif = prefs.getInt("Mode2Notif",3);
        	ModeAppl = prefs.getInt("Mode2Appl",1);
        }
        else if(Mode == 4)
        {
        	ModeTone = prefs.getInt("Mode3Tone",0);
        	ModeNotif = prefs.getInt("Mode3Notif",3);
        	ModeAppl = prefs.getInt("Mode3Appl",1);
        }
        
        builder.setDefaults(0);
        
        
        
        if(ModeTone == 1)
        {
        	builder.setDefaults(Notification.DEFAULT_ALL);
        }
        else
        {
        	builder.setDefaults(0);
        }
        
        
        if(ModeNotif == 1)
    	{
        	builder.setSmallIcon(R.drawable.iconpopup);
        	builder.setAutoCancel(true);
            builder.setTicker(getResources().getString(R.string.notifticker));
          
    		layout.setTextViewText(R.id.notifText, message);
    		layout.setImageViewBitmap(R.id.notifIcon,image);
    		layout.setImageViewResource(R.id.notifIconSmall, R.drawable.iconpopup);
    	}
    	if(ModeNotif == 2)
    	{
    		builder.setSmallIcon(R.drawable.notif_goo);
        	builder.setAutoCancel(true);
            builder.setTicker(getResources().getString(R.string.notifticker));
            
    		layout.setTextViewText(R.id.notifText, "1 New Event");
    		layout.setImageViewResource(R.id.notifIcon,R.drawable.notif_goo);
    		//layout.setImageViewResource(R.id.notifIconSmall, R.drawable.iconpopup);
    	}
    	if(ModeNotif == 3)
    	{
    		builder.setSmallIcon(R.drawable.notif_newspapper);
        	builder.setAutoCancel(true);
            builder.setTicker("Latest News");
            
    		layout.setTextViewText(R.id.notifText, "Latest News");
    		layout.setImageViewResource(R.id.notifIcon,R.drawable.notif_newspapper);
    		//layout.setImageViewResource(R.id.notifIconSmall, R.drawable.iconpopup);
    	}
        
        
        
        builder.setContent(layout); 
        Intent intent = null;
        if (message.indexOf("message") != -1) 
        { 
        	intent = new Intent(context, ActHome.class);
        } 
        else if (message.indexOf("offer") != -1) 
        {
        	intent = new Intent(context, ActHome.class);
        } 
        else if (message.indexOf("like") != -1) 
        {
        	intent = new Intent(context, ActHome.class);
        }   
        else 
        {   
        	intent = new Intent(context, ActHome.class);
		}
        PendingIntent pIntent = PendingIntent.getActivity(context, 0, intent, 0);
        builder.setContentIntent(pIntent);
        //builder.setDefaults(Notification.DEFAULT_ALL);
        
        NotificationManager notificationManager = (NotificationManager)
        context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(1, builder.build());
	}
}
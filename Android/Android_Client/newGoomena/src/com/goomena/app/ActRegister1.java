package com.goomena.app;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;

import org.json.JSONException;
import org.json.JSONObject;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.FacebookError;
import com.facebook.android.AsyncFacebookRunner.RequestListener;
import com.facebook.android.Facebook.DialogListener;
import com.facebook.android.Util;
import com.facebook.model.GraphUser;
import com.goomena.app.R;
import com.goomena.app.data.ProfileManager;
import com.goomena.app.data.ServiceClient;
import com.goomena.app.models.UserProfile;
import com.goomena.app.models.UserProfilesList;
import com.goomena.app.tools.Log;
import com.goomena.app.tools.Utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.SyncStateContract.Constants;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

@SuppressWarnings("deprecation")
public class ActRegister1 extends Activity
{
	RelativeLayout relativeLayout1,relativeLayout2,relativeLayout3,relativeLayout4,relativeLayout5,relativeLayout6;
	Animation move_down1,move_down2,move_down3,move_down4,move_down5;
	
	ImageView image_profile1,image_profile2,image_profile3,image_profile4;
	TextView text_profile1,text_profile2,text_profile3,text_profile4;
	
	SharedPreferences settings;
	boolean rememberMe = true;
	boolean remember = false;
	Dialog Dwaiting;
	
	// Your Facebook APP ID
	private static String APP_ID = "1458937241031502"; // Replace with your App ID
	
	// Instance of Facebook Class
	private Facebook facebook = new Facebook(APP_ID);
	private AsyncFacebookRunner mAsyncRunner;
	String FILENAME = "AndroidSSO_data";
	private SharedPreferences mPrefs;
	  
	  
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_register1);
		
		///-----+
		Dwaiting = new Dialog(ActRegister1.this, R.style.DialogSlideAnim);
		Dwaiting.setContentView(R.layout.dialog_waiting);
		Animation animFadein = AnimationUtils.loadAnimation(ActRegister1.this, R.anim.d_wait_fr_load);
		Animation animRotScal = AnimationUtils.loadAnimation(ActRegister1.this, R.anim.d_wait_bg_logo);
		final TextView dialog_waiting_textWaiting = (TextView) Dwaiting.findViewById(R.id.dialog_waiting_textWaiting);
		ImageView dialog_waiting_bgLogo = (ImageView) Dwaiting.findViewById(R.id.dialog_waiting_bgLogo);
		dialog_waiting_textWaiting.startAnimation(animFadein);
		dialog_waiting_bgLogo.startAnimation(animRotScal);
		/////----
		
		move_down1 = AnimationUtils.loadAnimation(this, R.anim.move_down1);
     	move_down2 = AnimationUtils.loadAnimation(this, R.anim.move_down2);
     	move_down3 = AnimationUtils.loadAnimation(this, R.anim.move_down3);
		
		relativeLayout1 = (RelativeLayout) findViewById(R.id.relativeLayout1);
     	relativeLayout2 = (RelativeLayout) findViewById(R.id.relativeLayout2);
     	relativeLayout3 = (RelativeLayout) findViewById(R.id.relativeLayout3);
     	relativeLayout4 = (RelativeLayout) findViewById(R.id.relativeLayout4);
     	relativeLayout5 = (RelativeLayout) findViewById(R.id.relativeLayout5);
     	relativeLayout6 = (RelativeLayout) findViewById(R.id.relativeLayout6);
     	
     	image_profile1 = (ImageView) findViewById(R.id.image_profile1);
     	image_profile2 = (ImageView) findViewById(R.id.image_profile2);
     	image_profile3 = (ImageView) findViewById(R.id.image_profile3);
     	image_profile4 = (ImageView) findViewById(R.id.image_profile4);
     	
     	text_profile1 = (TextView) findViewById(R.id.text_profile1);
     	text_profile2 = (TextView) findViewById(R.id.text_profile2);
     	text_profile3 = (TextView) findViewById(R.id.text_profile3);
     	text_profile4 = (TextView) findViewById(R.id.text_profile4);
     	
     	relativeLayout3.setOnClickListener(clickListener);
     	relativeLayout4.setOnClickListener(clickListener);
     	relativeLayout5.setOnClickListener(clickListener);
     	relativeLayout6.setOnClickListener(clickListener);
     	
     	relativeLayout3.startAnimation(move_down1);
     	relativeLayout4.startAnimation(move_down2);
     	relativeLayout5.startAnimation(move_down3);
     	
     
     	new getFavoritesTask().execute();
     	
     	settings = getSharedPreferences(Utils.SharedPrefsKey, 0);
		remember = settings.getBoolean("rememberMe",false);
     	mAsyncRunner = new AsyncFacebookRunner(facebook);
	}
	
	
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) 
	{
		super.onActivityResult(requestCode, resultCode, data);
		facebook.authorizeCallback(requestCode, resultCode, data);
	}
	
	
	
	OnClickListener clickListener = new OnClickListener() 
	{
		@Override
		public void onClick(View v) 
		{
			if(v == relativeLayout2)
			{
				
			}
			else if(v == relativeLayout3)
			{
				loginToFacebook();
			}
			else if(v == relativeLayout4)
			{
				Toast.makeText(ActRegister1.this, getResources().getString(R.string.under_construction), Toast.LENGTH_LONG).show();
			}
			else if(v == relativeLayout5)
			{
				Intent intent = new Intent();
				intent.setClass(ActRegister1.this, ActRegister2.class);
				startActivity(intent);
				//overridePendingTransition(R.anim.right_in, R.anim.left_out);
			}
			else if(v == relativeLayout6)
			{
				Intent intent = new Intent();
				intent.setClass(ActRegister1.this, ActLogin.class);
				startActivity(intent);
				
			}
		}
	};
	
	
	
	
	
	/**
	 * Function to login into facebook
	 * */
	public void loginToFacebook() 
	{
		mPrefs = getPreferences(MODE_PRIVATE);
		String access_token = mPrefs.getString("access_token", null);
		long expires = mPrefs.getLong("access_expires", 0);

		if (access_token != null) 
		{
			facebook.setAccessToken(access_token);
			Log.d("FB Sessions", "" + facebook.isSessionValid());
		}

		if (expires != 0) 
		{
			facebook.setAccessExpires(expires);
		}

		if (!facebook.isSessionValid()) 
		{
			facebook.authorize(this, new String[] { "email", "publish_stream" }, new DialogListener() 
			{
				@Override
				public void onCancel() 
				{
					// Function to handle cancel event
				}

				@Override
				public void onComplete(Bundle values) 
				{
					// Function to handle complete event
					// Edit Preferences and update facebook acess_token
					/*
					SharedPreferences.Editor editor = mPrefs.edit();
					editor.putString("access_token", facebook.getAccessToken());
					editor.putLong("access_expires", facebook.getAccessExpires());
					editor.commit();
					 */
					getProfileInformation();

				}

				@Override
				public void onError(DialogError error) 
				{
					// Function to handle error
				}

				@Override
				public void onFacebookError(FacebookError fberror) 
				{
					// Function to handle Facebook errors
				}

			});
		}
	}
	
	
	
	/**
	 * Get Profile information by making request to Facebook Graph API
	 * */
	public void getProfileInformation() 
	{
		mAsyncRunner.request("me", new RequestListener() 
		{
			@Override
			public void onComplete(String response, Object state) 
			{
				Log.d("Profile", response);
				String json = response;
				try 
				{
					// Facebook Profile JSON data
					JSONObject profile = new JSONObject(response);
					
					
					
					// getting email of the user
					final String email = profile.getString("email");
					
					
					final String id = profile.getString("id");
					
					
					
					// getting name of the user
					final String name = profile.getString("name");
					
					final String gender = profile.getString("gender");
					
					
					
					//final String birthday = profile.getString("birthday");
					
					//	Log.v("APP", "dgwsrgwrg "+birthday);
					
					runOnUiThread(new Runnable() 
					{
						@Override
						public void run() 
						{
							//Toast.makeText(getApplicationContext(), "Name: " + name + "\nEmail: " + email, Toast.LENGTH_LONG).show();
							new LoginTask(email,null,id,name,null,gender).execute();
						}

					});
				} 
				catch (JSONException e) 
				{
					e.printStackTrace();
				}
			}

			@Override
			public void onIOException(IOException e, Object state) { }

			@Override
			public void onFileNotFoundException(FileNotFoundException e, Object state) { }

			@Override
			public void onMalformedURLException(MalformedURLException e, Object state) { }

			@Override
			public void onFacebookError(FacebookError e, Object state) { }
		});
	}
	
	
	class LoginTask extends AsyncTask<Void, Integer, UserProfile>
	{
		int myProgressCount;
		String _email,_password,_fbid,_fbname,_fbusername,_gender;
		public LoginTask(String email, String password, String fbid, String fbname, String fbusername,String gender)
		{
			this._email = email;
			this._password = password;
			this._fbid = fbid;
			this._fbname = fbname;
			this._fbusername = fbusername;
			this._gender = gender;
		}
		
		@Override
		protected void onPreExecute()
		{	
			Dwaiting.show();
		}
		
		@Override
		protected UserProfile doInBackground(Void... params) 
		{
			UserProfile result = ServiceClient.loginUser_v2(_email, _password, Utils.getLoginDetails(ActRegister1.this),_fbid,_fbname,_fbusername);
			return result;
		}
		
		@SuppressLint("InlinedApi")
		@Override
		protected void onPostExecute(UserProfile result)
		{	
			if(result!=null && checkProfile(result))
			{				
				ProfileManager.saveProfile(ActRegister1.this, result);
				if(rememberMe)
				{
					Editor edit = settings.edit();
					edit.putBoolean("rememberMe", true);
					edit.putString("Email", result.UserName);
					edit.putString("Pass", result.Password);
					edit.commit();
				}
				else
				{
					Editor edit = settings.edit();
					edit.putBoolean("rememberMe", false);
					edit.commit();
				}
				Intent intent = new Intent(ActRegister1.this,ActHome.class);
		        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
		        ActRegister1.this.startActivity(intent);
				
				//ActLogin.this.finish();
			}
			else
			{
				Log.v("APP","Error in login!");
				//Toast.makeText(ActRegister1.this, getResources().getString(R.string.msg_err_login), Toast.LENGTH_LONG).show();
				Intent intent = new Intent();
				intent.setClass(ActRegister1.this, ActRegister2.class);
				intent.putExtra("fbMail", _email);
				intent.putExtra("fbId", _fbid);
				intent.putExtra("fbName", _fbname);
				intent.putExtra("fbUsername", _fbusername);
				intent.putExtra("fbGender", _gender);
				startActivity(intent);
				//overridePendingTransition(R.anim.right_in, R.anim.left_out);
			}
			Dwaiting.cancel();
			//imgLoading.setVisibility(View.GONE);
			//loadingAnimation.stop();
		}
	}
	
	private boolean checkProfile(UserProfile profile)
	{
		if(profile.ProfileId==null)
		{
			return false;
		}
		else if(profile.UserName==null)
		{
			return false;
		}
		return true;
	}
	
	
	
	class getFavoritesTask extends AsyncTask<Void,Integer,UserProfilesList>
	{
		@Override
		protected void onPreExecute(){}
		
		@Override
		protected UserProfilesList doInBackground(Void... params) 
		{
			UserProfilesList profilesList = null;
			profilesList = ServiceClient.getSearchResultsPublic("1", null, null, "100", "0", "0", "4" , "GR");
			//profilesList = ServiceClient.getNewMembers("67884","0", "4" ,"1", "0", "GR");
			return profilesList;
		}
		
		@Override
		protected void onPostExecute(UserProfilesList result){
			if(result!=null)
			{
				if(result.Profiles.size()>3)
				{
					Utils.getImageLoader(ActRegister1.this).displayImage(result.Profiles.get(0).ImageUrl.replace("d150", "thumbs"),image_profile1, Utils.getImageOptions());
					text_profile1.setText(result.Profiles.get(0).UserName);
				
					Utils.getImageLoader(ActRegister1.this).displayImage(result.Profiles.get(1).ImageUrl.replace("d150", "thumbs"),image_profile2, Utils.getImageOptions());
					text_profile2.setText(result.Profiles.get(1).UserName);
				
					Utils.getImageLoader(ActRegister1.this).displayImage(result.Profiles.get(2).ImageUrl.replace("d150", "thumbs"),image_profile3, Utils.getImageOptions());
					text_profile3.setText(result.Profiles.get(2).UserName);
				
					Utils.getImageLoader(ActRegister1.this).displayImage(result.Profiles.get(3).ImageUrl.replace("d150", "thumbs"),image_profile4, Utils.getImageOptions());
					text_profile4.setText(result.Profiles.get(3).UserName);
				}
			}
		}
	};

}

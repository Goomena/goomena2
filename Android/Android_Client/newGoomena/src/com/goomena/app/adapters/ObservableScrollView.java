package com.goomena.app.adapters;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ScrollView;

public class ObservableScrollView extends ScrollView {

	private int lastScrollPosition = 0;

	private int delay = 200;

	public interface ScrollViewListener {

		void onScrollChanged(ScrollView scrollView, int x, int y, int oldx, int oldy);

		void onScrollStopped();

	}

	private ScrollViewListener scrollViewListener = null;

	public ObservableScrollView(Context context)
	{
		super(context);
	}

	public ObservableScrollView(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);
	}

	public ObservableScrollView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
	}

	public void setScrollViewListener(ScrollViewListener scrollViewListener)
	{
		this.scrollViewListener = scrollViewListener;
	}

	@Override
	protected void onScrollChanged(int x, int y, int oldx, int oldy)
	{
		super.onScrollChanged(x, y, oldx, oldy);

		if (scrollViewListener != null)
		{
			lastScrollPosition = getScrollY();
			scrollViewListener.onScrollChanged(this, x, y, oldx, oldy);
			postDelayed(new ScrollStoppedTask(lastScrollPosition), delay);
		}

	}

	private class ScrollStoppedTask implements Runnable {

		private int lastPosition;

		public ScrollStoppedTask(int lastPosition)
		{
			this.lastPosition = lastPosition;
		}

		@Override
		public void run()
		{
			int currentScrollPosition = getScrollY();
			if (lastPosition == currentScrollPosition)
			{
				scrollViewListener.onScrollStopped();
			}
		}

	}

}
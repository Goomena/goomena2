package com.goomena.app.adapters;

import java.util.ArrayList;

import com.goomena.app.Controller;
import com.goomena.app.Panel;
import com.goomena.app.R;
import com.goomena.app.adapters.LikesAdapters.cancelLikeTask;
import com.goomena.app.adapters.LikesAdapters.deleteLikeTask;
import com.goomena.app.adapters.LikesAdapters.deleteLikeTask1;
import com.goomena.app.adapters.LikesAdapters.getLikesTask;
import com.goomena.app.adapters.LikesAdapters.rejectLikeTask;
import com.goomena.app.data.ServiceClient;
import com.goomena.app.models.Offer;
import com.goomena.app.models.OfferList;
import com.goomena.app.models.UserProfile;
import com.goomena.app.tools.Log;
import com.goomena.app.tools.Utils;
import com.goomena.fragments.FragLoading;
import com.goomena.fragments.FragMessageView;
import com.goomena.fragments.FragProfile;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView.OnItemClickListener;

public class OffersAdapters extends PagerAdapter
{
	private Activity _activity;
	private LayoutInflater inflater;
	
	String MyUserId,zip,lat,lng;
	
	String rejectReason;
	String currentOfferid;
	
	ListView offerListView;
	FragLoading fragLoading;
	Animation animFadein;
	
	OfferList profilesListFull_new,profilesListFull_pending,profilesListFull_rejected,profilesListFull_accepted;
	
	boolean allowCall_new = true;
	boolean allowCall_pending = true;
	boolean allowCall_rejected = true;
	boolean allowCall_accepted = true;
	
	ItemOfferAdapter adapter_new,adapter_pending,adapter_rejected,adapter_accepted;////
	
	private final static String TAG_FRAGMENT = "TAG_FRAGMENT";
	
	Handler handler_new,handler_pending,handler_rejected,handler_accepted;
	
	public OffersAdapters(Activity activity)
	{
		this._activity = activity;
		
		MyUserId = Utils.getSharedPreferences(activity).getString("userid", "");
		zip = Utils.getSharedPreferences(activity).getString("zipcode", "");
		lat = Utils.getSharedPreferences(activity).getString("lat", "");
		lng = Utils.getSharedPreferences(activity).getString("lng", "");
		
		profilesListFull_new = new OfferList();
		profilesListFull_new.offers = new ArrayList<Offer>();
		
		profilesListFull_pending = new OfferList();
		profilesListFull_pending.offers = new ArrayList<Offer>();
		
		profilesListFull_rejected = new OfferList();
		profilesListFull_rejected.offers = new ArrayList<Offer>();
		
		profilesListFull_accepted = new OfferList();
		profilesListFull_accepted.offers = new ArrayList<Offer>();
		
		adapter_new = new ItemOfferAdapter(_activity,0);
		adapter_pending = new ItemOfferAdapter(_activity,1);
		adapter_rejected = new ItemOfferAdapter(_activity,3);
		adapter_accepted = new ItemOfferAdapter(_activity,2);
		createRejectDialog();
		
		fragLoading = new FragLoading(activity);
		
		profilesListFull_new.offers.clear();
		new getOffersTask(0).execute();
		
		profilesListFull_pending.offers.clear();
		new getOffersTask(1).execute();
		
		profilesListFull_rejected.offers.clear();
		new getOffersTask(3).execute();
		
		profilesListFull_accepted.offers.clear();
		new getOffersTask(2).execute();
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return 4;
	}
	
	@Override
	public boolean isViewFromObject(View view, Object object) 
	{
		// TODO Auto-generated method stub
		return view == ((RelativeLayout) object);
	}
	
	@Override
	public void setPrimaryItem(ViewGroup container, int position, Object object) 
	{}
	
	public Object instantiateItem(ViewGroup container, int position) 
	{
		inflater = (LayoutInflater) _activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View viewLayout = inflater.inflate(R.layout.fr_offers_new, container, false);
		offerListView = (ListView)viewLayout.findViewById(R.id.offerList);
		
		final RelativeLayout include_view_no_result = (RelativeLayout) viewLayout.findViewById(R.id.include_view_no_result);
		final TextView view_noresult_textTitle = (TextView) viewLayout.findViewById(R.id.view_noresult_textTitle);
		
		if(Utils.isNetworkConnected(_activity))
		{
			if(position==0)
			{
				offerListView.setAdapter(adapter_new);
				offerListView.setOnScrollListener(myOnScrollListener_new);
				offerListView.setOnItemClickListener(myOnItemClickListener_new);
				
				if(profilesListFull_new.offers.size()==0)
				{
					view_noresult_textTitle.setText(_activity.getResources().getString(R.string.noresult_title_you_currently_have_no_offers));
					include_view_no_result.setVisibility(RelativeLayout.VISIBLE);
				}
				handler_new = new Handler()
				{
					public void handleMessage(Message msg) 
					{	
						if(msg.obj.toString().equals("0"))
						{
							view_noresult_textTitle.setText(_activity.getResources().getString(R.string.noresult_title_you_currently_have_no_offers));
							include_view_no_result.setVisibility(RelativeLayout.VISIBLE);
						}
						else
						{
							include_view_no_result.setVisibility(RelativeLayout.GONE);
						}
					}
				};
				
				//profilesListFull_new.offers.clear();
				//new getLikesTask(0).execute();
			}
			else if(position==1)
			{
				offerListView.setAdapter(adapter_pending);
				offerListView.setOnScrollListener(myOnScrollListener_pending);
				offerListView.setOnItemClickListener(myOnItemClickListener_pending);
				
				if(profilesListFull_pending.offers.size()==0)
				{
					view_noresult_textTitle.setText(_activity.getResources().getString(R.string.noresult_title_you_currently_have_no_offers));
					include_view_no_result.setVisibility(RelativeLayout.VISIBLE);
				}
				handler_pending = new Handler()
				{
					public void handleMessage(Message msg) 
					{	
						if(msg.obj.toString().equals("0"))
						{
							view_noresult_textTitle.setText(_activity.getResources().getString(R.string.noresult_title_you_currently_have_no_offers));
							include_view_no_result.setVisibility(RelativeLayout.VISIBLE);
						}
						else
						{
							include_view_no_result.setVisibility(RelativeLayout.GONE);
						}
					}
				};
				
				//profilesListFull_pending.offers.clear();
				//new getLikesTask(1).execute();
			}
			else if(position==2)
			{
				offerListView.setAdapter(adapter_rejected);
				offerListView.setOnScrollListener(myOnScrollListener_rejected);
				offerListView.setOnItemClickListener(myOnItemClickListener_rejected);
				
				if(profilesListFull_rejected.offers.size()==0)
				{
					view_noresult_textTitle.setText(_activity.getResources().getString(R.string.noresult_title_you_currently_have_no_offers));
					include_view_no_result.setVisibility(RelativeLayout.VISIBLE);
				}
				handler_rejected = new Handler()
				{
					public void handleMessage(Message msg) 
					{	
						if(msg.obj.toString().equals("0"))
						{
							view_noresult_textTitle.setText(_activity.getResources().getString(R.string.noresult_title_you_currently_have_no_offers));
							include_view_no_result.setVisibility(RelativeLayout.VISIBLE);
						}
						else
						{
							include_view_no_result.setVisibility(RelativeLayout.GONE);
						}
					}
				};
				
				//profilesListFull_rejected.offers.clear();
				//new getLikesTask(3).execute();
			}
			else if(position==3)
			{
				offerListView.setAdapter(adapter_accepted);
				offerListView.setOnScrollListener(myOnScrollListener_accepted);
				offerListView.setOnItemClickListener(myOnItemClickListener_accepted);
				
				if(profilesListFull_accepted.offers.size()==0)
				{
					view_noresult_textTitle.setText(_activity.getResources().getString(R.string.noresult_title_you_currently_have_no_offers));
					include_view_no_result.setVisibility(RelativeLayout.VISIBLE);
				}
				handler_accepted = new Handler()
				{
					public void handleMessage(Message msg) 
					{	
						if(msg.obj.toString().equals("0"))
						{
							view_noresult_textTitle.setText(_activity.getResources().getString(R.string.noresult_title_you_currently_have_no_offers));
							include_view_no_result.setVisibility(RelativeLayout.VISIBLE);
						}
						else
						{
							include_view_no_result.setVisibility(RelativeLayout.GONE);
						}
					}
				};
				
				//profilesListFull_accepted.offers.clear();
				//new getLikesTask(2).execute();
			}
		}
		else
		{
			Utils.showNoInternet(_activity);
		}

		((ViewPager) container).addView(viewLayout);
		 
        return viewLayout;
	}
	
	@Override
    public void destroyItem(ViewGroup container, int position, Object object) 
	{
        ((ViewPager) container).removeView((RelativeLayout) object);
    }
	
	
	class getOffersTask extends AsyncTask<Void,Integer,OfferList>
	{
		int _viewMode;
		
		public getOffersTask(int viewMode)
		{
			this._viewMode = viewMode;
		}
		@Override
		protected void onPreExecute(){ fragLoading.Show(); }
		
		@Override
		protected OfferList doInBackground(Void... params) 
		{
			Log.v("APP","cvcvcv "+MyUserId);
			OfferList profilesList = null;
			if(_viewMode==0)
			{
				profilesList = ServiceClient.getOffers(String.valueOf(_viewMode), MyUserId, zip, lat, lng, String.valueOf(profilesListFull_new.offers.size()));
			}
			else if(_viewMode==1)
			{
				profilesList = ServiceClient.getOffers(String.valueOf(_viewMode), MyUserId, zip, lat, lng, String.valueOf(profilesListFull_pending.offers.size()));
			}
			else if(_viewMode==2)
			{
				profilesList = ServiceClient.getOffers(String.valueOf(_viewMode), MyUserId, zip, lat, lng, String.valueOf(profilesListFull_accepted.offers.size()));
			}
			else if(_viewMode==3)
			{
				profilesList = ServiceClient.getOffers(String.valueOf(_viewMode), MyUserId, zip, lat, lng, String.valueOf(profilesListFull_rejected.offers.size()));
			}
			return profilesList;
		}
		
		@Override
		protected void onPostExecute(OfferList result)
		{
			if(result!=null)
			{
				if(_viewMode==0)
				{
					profilesListFull_new.offers.addAll(result.offers);
					adapter_new.setItems(profilesListFull_new.offers);
					adapter_new.notifyDataSetChanged();
					
					if(handler_new!=null)
					{
						if(profilesListFull_new.offers.size()==0)
						{
							Message msg = handler_new.obtainMessage();
							msg.obj = "0";
							handler_new.sendMessage(msg);
						}
						else
						{
							Message msg = handler_new.obtainMessage();
							msg.obj = "1";
							handler_new.sendMessage(msg);
						}
					}
				
					if(result.offers.size()<20) { allowCall_new = false; }
					else { allowCall_new = true; }
				}
				else if(_viewMode==1)
				{
					profilesListFull_pending.offers.addAll(result.offers);
					adapter_pending.setItems(profilesListFull_pending.offers);
					adapter_pending.notifyDataSetChanged();
					
					if(handler_pending!=null)
					{
						if(profilesListFull_pending.offers.size()==0)
						{
							Message msg = handler_pending.obtainMessage();
							msg.obj = "0";
							handler_pending.sendMessage(msg);
						}
						else
						{
							Message msg = handler_pending.obtainMessage();
							msg.obj = "1";
							handler_pending.sendMessage(msg);
						}
					}
				
					if(result.offers.size()<20) { allowCall_pending = false; }
					else { allowCall_pending = true; }
				}
				else if(_viewMode==2)
				{
					profilesListFull_accepted.offers.addAll(result.offers);
					adapter_accepted.setItems(profilesListFull_accepted.offers);
					adapter_accepted.notifyDataSetChanged();
					
					if(handler_accepted!=null)
					{
						if(profilesListFull_accepted.offers.size()==0)
						{
							Message msg = handler_accepted.obtainMessage();
							msg.obj = "0";
							handler_accepted.sendMessage(msg);
						}
						else
						{
							Message msg = handler_accepted.obtainMessage();
							msg.obj = "1";
							handler_accepted.sendMessage(msg);
						}
					}
				
					if(result.offers.size()<20) { allowCall_accepted = false; }
					else { allowCall_accepted = true; }
				}
				else if(_viewMode==3)
				{
					profilesListFull_rejected.offers.addAll(result.offers);
					adapter_rejected.setItems(profilesListFull_rejected.offers);
					adapter_rejected.notifyDataSetChanged();
					
					if(handler_rejected!=null)
					{
						if(profilesListFull_rejected.offers.size()==0)
						{
							Message msg = handler_rejected.obtainMessage();
							msg.obj = "0";
							handler_rejected.sendMessage(msg);
						}
						else
						{
							Message msg = handler_rejected.obtainMessage();
							msg.obj = "1";
							handler_rejected.sendMessage(msg);
						}
					}
				
					if(result.offers.size()<20) { allowCall_rejected = false; }
					else { allowCall_rejected = true; }
				}
			}	
			fragLoading.Close();
		}
	}
	
	
	
	
	
	OnScrollListener myOnScrollListener_new = new OnScrollListener() 
	{
		@Override
		public void onScrollStateChanged(AbsListView view, int scrollState) 
		{
			if(scrollState==1)
			{
				adapter_new.setAnim(true);
			}
			else if(scrollState==0)
			{
				adapter_new.setAnim(false);
			}
		}

		@Override
		public void onScroll(AbsListView view, int firstVisibleItem,int visibleItemCount, int totalItemCount) {
			if(firstVisibleItem+visibleItemCount==totalItemCount && visibleItemCount!=0 && totalItemCount!=0 && allowCall_new)
			{
				allowCall_new = false;
				Log.v("APP","----Executing get profiles from on scroll-----");				
				new getOffersTask(0).execute();
			}
		}
	};
	
	OnScrollListener myOnScrollListener_pending = new OnScrollListener() 
	{
		@Override
		public void onScrollStateChanged(AbsListView view, int scrollState) 
		{
			if(scrollState==1)
			{
				adapter_pending.setAnim(true);
			}
			else if(scrollState==0)
			{
				adapter_pending.setAnim(false);
			}
		}

		@Override
		public void onScroll(AbsListView view, int firstVisibleItem,int visibleItemCount, int totalItemCount) {
			if(firstVisibleItem+visibleItemCount==totalItemCount && visibleItemCount!=0 && totalItemCount!=0 && allowCall_pending)
			{
				allowCall_pending = false;
				Log.v("APP","----Executing get profiles from on scroll-----");				
				new getOffersTask(1).execute();
			}
		}
	};
	
	OnScrollListener myOnScrollListener_rejected = new OnScrollListener() 
	{
		@Override
		public void onScrollStateChanged(AbsListView view, int scrollState) 
		{
			if(scrollState==1)
			{
				adapter_rejected.setAnim(true);
			}
			else if(scrollState==0)
			{
				adapter_rejected.setAnim(false);
			}
		}

		@Override
		public void onScroll(AbsListView view, int firstVisibleItem,int visibleItemCount, int totalItemCount) {
			if(firstVisibleItem+visibleItemCount==totalItemCount && visibleItemCount!=0 && totalItemCount!=0 && allowCall_rejected)
			{
				allowCall_rejected = false;
				Log.v("APP","----Executing get profiles from on scroll-----");				
				new getOffersTask(3).execute();
			}
		}
	};
	
	OnScrollListener myOnScrollListener_accepted = new OnScrollListener() 
	{
		@Override
		public void onScrollStateChanged(AbsListView view, int scrollState) 
		{
			if(scrollState==1)
			{
				adapter_accepted.setAnim(true);
			}
			else if(scrollState==0)
			{
				adapter_accepted.setAnim(false);
			}
		}

		@Override
		public void onScroll(AbsListView view, int firstVisibleItem,int visibleItemCount, int totalItemCount) {
			if(firstVisibleItem+visibleItemCount==totalItemCount && visibleItemCount!=0 && totalItemCount!=0 && allowCall_accepted)
			{
				allowCall_accepted = false;
				Log.v("APP","----Executing get profiles from on scroll-----");				
				new getOffersTask(2).execute();
			}
		}
	};
	
	
	
	
	
	
	
	
	
	OnItemClickListener myOnItemClickListener_new = new OnItemClickListener() 
	{
		@Override
		public void onItemClick(AdapterView<?> parent, View view,final int position, long id) 
		{
			final Dialog d = new Dialog(_activity, R.style.DialogSlideAnim);
			d.setContentView(R.layout.dialog_like);
			
			TextView dialog_textTitle = (TextView) d.findViewById(R.id.dialog_textTitle);
			ImageView dialog_imageProfile = (ImageView) d.findViewById(R.id.dialog_imageProfile);
			TextView dialog_textUsername = (TextView) d.findViewById(R.id.dialog_textUsername);
			TextView dialog_textAge = (TextView) d.findViewById(R.id.dialog_textAge);
			TextView dialog_textLocation = (TextView) d.findViewById(R.id.dialog_textLocation);
			RelativeLayout dialog_relativeLayoutSendMessage = (RelativeLayout) d.findViewById(R.id.dialog_relativeLayoutSendMessage);
			RelativeLayout dialog_relativeLayoutBid = (RelativeLayout) d.findViewById(R.id.dialog_relativeLayoutBid);
			TextView dialog_textLayoutBid = (TextView) d.findViewById(R.id.dialog_textLayoutBid);
			RelativeLayout dialog_relativeLayoutReject = (RelativeLayout) d.findViewById(R.id.dialog_relativeLayoutReject);
			TextView dialog_textReject = (TextView) d.findViewById(R.id.dialog_textReject);
			RelativeLayout dialog_relativeLayoutProfile = (RelativeLayout) d.findViewById(R.id.dialog_relativeLayoutProfile);
			
			dialog_textTitle.setText(_activity.getResources().getString(R.string.you_received_a_offer_from));
			
			Utils.getImageLoader(_activity).displayImage(profilesListFull_new.offers.get(position).profile.ImageUrl.replace("d150", "thumbs"), dialog_imageProfile, Utils.getImageOptions()); 	    
			dialog_textUsername.setText(profilesListFull_new.offers.get(position).profile.UserName);
			dialog_textAge.setText(_activity.getResources().getString(R.string.age)+" "+profilesListFull_new.offers.get(position).profile.UserAge+" "+_activity.getResources().getString(R.string.years));
			dialog_textLocation.setText(profilesListFull_new.offers.get(position).profile.UserCity+", "+profilesListFull_new.offers.get(position).profile.UserRegion);
			dialog_textReject.setText(_activity.getResources().getString(R.string.reject_the_offer));
			dialog_textLayoutBid.setText(_activity.getResources().getString(R.string.accept));
			
			dialog_relativeLayoutSendMessage.setOnClickListener(new OnClickListener() {
				
				@SuppressLint("NewApi") 
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Fragment fr;
					fr = new FragMessageView(null,profilesListFull_new.offers.get(position).profile,Integer.parseInt(profilesListFull_new.offers.get(position).offerId),null);
					FragmentManager fm = _activity.getFragmentManager();
					
			        FragmentTransaction fragmentTransaction = fm.beginTransaction();
			        fragmentTransaction.setCustomAnimations(R.anim.left_slidein_list_animator, R.anim.left_slideout_list_animator,R.anim.right_slidein_list_animator, R.anim.right_slideout_list_animator);
			        fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
			        fragmentTransaction.addToBackStack(null);
			        fragmentTransaction.commit();
			        d.cancel();
				}
			});
			
			dialog_relativeLayoutBid.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					currentOfferid = profilesListFull_new.offers.get(position).offerId;
					new acceptOfferTask().execute(currentOfferid);
					d.cancel();
				}
			});
			
			
			dialog_imageProfile.setOnClickListener(new OnClickListener() {
				@SuppressLint("NewApi") 
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					ArrayList<UserProfile> _userProfilesFull_ = new ArrayList<UserProfile>();
					_userProfilesFull_.add(profilesListFull_new.offers.get(position).profile);
					
					Fragment fr = new FragProfile(2,_userProfilesFull_,0);				
					FragmentManager fm = _activity.getFragmentManager();
				    FragmentTransaction fragmentTransaction = fm.beginTransaction();
				    fragmentTransaction.setCustomAnimations(R.anim.left_slidein_list_animator, R.anim.left_slideout_list_animator,R.anim.right_slidein_list_animator, R.anim.right_slideout_list_animator);
				    fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
				    fragmentTransaction.addToBackStack(null);
				    fragmentTransaction.commit();
					d.cancel();
				}
			});
			
			//dialog_relativeLayoutBid.setVisibility(View.GONE);
			
			dialog_relativeLayoutProfile.setOnClickListener(new OnClickListener() {
				@SuppressLint("NewApi") 
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					ArrayList<UserProfile> _userProfilesFull_ = new ArrayList<UserProfile>();
					_userProfilesFull_.add(profilesListFull_new.offers.get(position).profile);
					
					Fragment fr = new FragProfile(2,_userProfilesFull_,0);				
					FragmentManager fm = _activity.getFragmentManager();
				    FragmentTransaction fragmentTransaction = fm.beginTransaction();
				    fragmentTransaction.setCustomAnimations(R.anim.left_slidein_list_animator, R.anim.left_slideout_list_animator,R.anim.right_slidein_list_animator, R.anim.right_slideout_list_animator);
				    fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
				    fragmentTransaction.addToBackStack(null);
				    fragmentTransaction.commit();
					d.cancel();
				}
			});
			
			dialog_relativeLayoutReject.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					rejectDialog.show();
					currentOfferid = profilesListFull_new.offers.get(position).offerId;
					d.cancel();
				}
			});
			d.show();
		}
	};
	
	
	
	 
	OnItemClickListener myOnItemClickListener_pending = new OnItemClickListener() 
	{
		@Override
		public void onItemClick(AdapterView<?> parent, View view,final int position, long id) 
		{
			final Dialog d = new Dialog(_activity, R.style.DialogSlideAnim);
			d.setContentView(R.layout.dialog_like);
			
			TextView dialog_textTitle = (TextView) d.findViewById(R.id.dialog_textTitle);
			ImageView dialog_imageProfile = (ImageView) d.findViewById(R.id.dialog_imageProfile);
			TextView dialog_textUsername = (TextView) d.findViewById(R.id.dialog_textUsername);
			TextView dialog_textAge = (TextView) d.findViewById(R.id.dialog_textAge);
			TextView dialog_textLocation = (TextView) d.findViewById(R.id.dialog_textLocation);
			RelativeLayout dialog_relativeLayoutSendMessage = (RelativeLayout) d.findViewById(R.id.dialog_relativeLayoutSendMessage);
			RelativeLayout dialog_relativeLayoutBid = (RelativeLayout) d.findViewById(R.id.dialog_relativeLayoutBid);
			RelativeLayout dialog_relativeLayoutReject = (RelativeLayout) d.findViewById(R.id.dialog_relativeLayoutReject);
			TextView dialog_textReject = (TextView) d.findViewById(R.id.dialog_textReject);
			RelativeLayout dialog_relativeLayoutProfile = (RelativeLayout) d.findViewById(R.id.dialog_relativeLayoutProfile);
			
			dialog_textTitle.setText(_activity.getResources().getString(R.string.waitreply));
			Utils.getImageLoader(_activity).displayImage(profilesListFull_pending.offers.get(position).profile.ImageUrl.replace("d150", "thumbs"), dialog_imageProfile, Utils.getImageOptions()); 	    
			dialog_textUsername.setText(profilesListFull_pending.offers.get(position).profile.UserName);
			dialog_textAge.setText(_activity.getResources().getString(R.string.age)+" "+profilesListFull_pending.offers.get(position).profile.UserAge+" "+_activity.getResources().getString(R.string.years));
			dialog_textLocation.setText(profilesListFull_pending.offers.get(position).profile.UserCity+", "+profilesListFull_pending.offers.get(position).profile.UserRegion);
			dialog_textReject.setText(_activity.getResources().getString(R.string.cancel));
			
			
			
			
			
			dialog_relativeLayoutSendMessage.setOnClickListener(new OnClickListener() {
				
				@SuppressLint("NewApi") 
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Fragment fr;
					fr = new FragMessageView(null,profilesListFull_pending.offers.get(position).profile,Integer.parseInt(profilesListFull_pending.offers.get(position).offerId),null);
					FragmentManager fm = _activity.getFragmentManager();
					
			        FragmentTransaction fragmentTransaction = fm.beginTransaction();
			        fragmentTransaction.setCustomAnimations(R.anim.left_slidein_list_animator, R.anim.left_slideout_list_animator,R.anim.right_slidein_list_animator, R.anim.right_slideout_list_animator);
			        fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
			        fragmentTransaction.addToBackStack(null);
			        fragmentTransaction.commit();
			        d.cancel();
				}
			});
//			dialog_relativeLayoutBid.setOnClickListener(new OnClickListener() {
//				
//				@Override
//				public void onClick(View v) {
//					// TODO Auto-generated method stub
//					createOfferDialog(profilesListFull_pending.offers.get(position).profile);
//					offerDialog.show();
//					d.cancel();
//				}
//			});
			
			
			dialog_imageProfile.setOnClickListener(new OnClickListener() {
				@SuppressLint("NewApi") 
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					ArrayList<UserProfile> _userProfilesFull_ = new ArrayList<UserProfile>();
					_userProfilesFull_.add(profilesListFull_pending.offers.get(position).profile);
					
					Fragment fr = new FragProfile(2,_userProfilesFull_,0);				
					FragmentManager fm = _activity.getFragmentManager();
				    FragmentTransaction fragmentTransaction = fm.beginTransaction();
				    fragmentTransaction.setCustomAnimations(R.anim.left_slidein_list_animator, R.anim.left_slideout_list_animator,R.anim.right_slidein_list_animator, R.anim.right_slideout_list_animator);
				    fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
				    fragmentTransaction.addToBackStack(null);
				    fragmentTransaction.commit();
					d.cancel();
				}
			});
			
			dialog_relativeLayoutBid.setVisibility(View.GONE);
			
			dialog_relativeLayoutProfile.setOnClickListener(new OnClickListener() {
				@SuppressLint("NewApi") 
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					ArrayList<UserProfile> _userProfilesFull_ = new ArrayList<UserProfile>();
					_userProfilesFull_.add(profilesListFull_pending.offers.get(position).profile);
					
					Fragment fr = new FragProfile(2,_userProfilesFull_,0);				
					FragmentManager fm = _activity.getFragmentManager();
				    FragmentTransaction fragmentTransaction = fm.beginTransaction();
				    fragmentTransaction.setCustomAnimations(R.anim.left_slidein_list_animator, R.anim.left_slideout_list_animator,R.anim.right_slidein_list_animator, R.anim.right_slideout_list_animator);
				    fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
				    fragmentTransaction.addToBackStack(null);
				    fragmentTransaction.commit();
					d.cancel();
				}
			});
			
			dialog_relativeLayoutReject.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					currentOfferid = profilesListFull_pending.offers.get(position).offerId;
					new cancelLikeTask().execute(currentOfferid);
					d.cancel();
				}
			});
			d.show();
		}
	};
	
	
	OnItemClickListener myOnItemClickListener_rejected = new OnItemClickListener() 
	{
		@Override
		public void onItemClick(AdapterView<?> parent, View view,final int position, long id) 
		{
			final Dialog d = new Dialog(_activity, R.style.DialogSlideAnim);
			d.setContentView(R.layout.dialog_like);
			
			TextView dialog_textTitle = (TextView) d.findViewById(R.id.dialog_textTitle);
			ImageView dialog_imageProfile = (ImageView) d.findViewById(R.id.dialog_imageProfile);
			TextView dialog_textUsername = (TextView) d.findViewById(R.id.dialog_textUsername);
			TextView dialog_textAge = (TextView) d.findViewById(R.id.dialog_textAge);
			TextView dialog_textLocation = (TextView) d.findViewById(R.id.dialog_textLocation);
			RelativeLayout dialog_relativeLayoutSendMessage = (RelativeLayout) d.findViewById(R.id.dialog_relativeLayoutSendMessage);
			RelativeLayout dialog_relativeLayoutBid = (RelativeLayout) d.findViewById(R.id.dialog_relativeLayoutBid);
			RelativeLayout dialog_relativeLayoutReject = (RelativeLayout) d.findViewById(R.id.dialog_relativeLayoutReject);
			TextView dialog_textReject = (TextView) d.findViewById(R.id.dialog_textReject);
			RelativeLayout dialog_relativeLayoutProfile = (RelativeLayout) d.findViewById(R.id.dialog_relativeLayoutProfile);
			
			dialog_textTitle.setText(_activity.getResources().getString(R.string.rejectoffer));
			Utils.getImageLoader(_activity).displayImage(profilesListFull_rejected.offers.get(position).profile.ImageUrl.replace("d150", "thumbs"), dialog_imageProfile, Utils.getImageOptions()); 	    
			dialog_textUsername.setText(profilesListFull_rejected.offers.get(position).profile.UserName);
			dialog_textAge.setText(_activity.getResources().getString(R.string.age)+" "+profilesListFull_rejected.offers.get(position).profile.UserAge+" "+_activity.getResources().getString(R.string.years));
			dialog_textLocation.setText(profilesListFull_rejected.offers.get(position).profile.UserCity+", "+profilesListFull_rejected.offers.get(position).profile.UserRegion);
			dialog_textReject.setText(_activity.getResources().getString(R.string.delete));
			
			
			
			
			dialog_relativeLayoutSendMessage.setOnClickListener(new OnClickListener() {
				
				@SuppressLint("NewApi") 
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Fragment fr;
					fr = new FragMessageView(null,profilesListFull_rejected.offers.get(position).profile,Integer.parseInt(profilesListFull_rejected.offers.get(position).offerId),null);
					FragmentManager fm = _activity.getFragmentManager();
					
			        FragmentTransaction fragmentTransaction = fm.beginTransaction();
			        fragmentTransaction.setCustomAnimations(R.anim.left_slidein_list_animator, R.anim.left_slideout_list_animator,R.anim.right_slidein_list_animator, R.anim.right_slideout_list_animator);
			        fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
			        fragmentTransaction.addToBackStack(null);
			        fragmentTransaction.commit();
			        d.cancel();
				}
			});
			
//			dialog_relativeLayoutBid.setOnClickListener(new OnClickListener() {
//				
//				@Override
//				public void onClick(View v) {
//					// TODO Auto-generated method stub
//					createOfferDialog(profilesListFull_rejected.offers.get(position).profile);
//					offerDialog.show();
//					d.cancel();
//				}
//			});
			
			dialog_imageProfile.setOnClickListener(new OnClickListener() {
				@SuppressLint("NewApi") 
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					ArrayList<UserProfile> _userProfilesFull_ = new ArrayList<UserProfile>();
					_userProfilesFull_.add(profilesListFull_rejected.offers.get(position).profile);
					
					Fragment fr = new FragProfile(2,_userProfilesFull_,0);				
					FragmentManager fm = _activity.getFragmentManager();
				    FragmentTransaction fragmentTransaction = fm.beginTransaction();
				    fragmentTransaction.setCustomAnimations(R.anim.left_slidein_list_animator, R.anim.left_slideout_list_animator,R.anim.right_slidein_list_animator, R.anim.right_slideout_list_animator);
				    fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
				    fragmentTransaction.addToBackStack(null);
				    fragmentTransaction.commit();
					d.cancel();
				}
			});
			
			dialog_relativeLayoutBid.setVisibility(View.GONE);
			
			dialog_relativeLayoutProfile.setOnClickListener(new OnClickListener() {
				@SuppressLint("NewApi") 
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					ArrayList<UserProfile> _userProfilesFull_ = new ArrayList<UserProfile>();
					_userProfilesFull_.add(profilesListFull_rejected.offers.get(position).profile);
					
					Fragment fr = new FragProfile(2,_userProfilesFull_,0);				
					FragmentManager fm = _activity.getFragmentManager();
				    FragmentTransaction fragmentTransaction = fm.beginTransaction();
				    fragmentTransaction.setCustomAnimations(R.anim.left_slidein_list_animator, R.anim.left_slideout_list_animator,R.anim.right_slidein_list_animator, R.anim.right_slideout_list_animator);
				    fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
				    fragmentTransaction.addToBackStack(null);
				    fragmentTransaction.commit();
					d.cancel();
				}
			});
			
			dialog_relativeLayoutReject.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					currentOfferid = profilesListFull_rejected.offers.get(position).offerId;
					new deleteLikeTask().execute(currentOfferid);
					d.cancel();
				}
			});
			d.show();
		}
	};
	
	
	
	OnItemClickListener myOnItemClickListener_accepted = new OnItemClickListener() 
	{
		@Override
		public void onItemClick(AdapterView<?> parent, View view,final int position, long id) 
		{
			final Dialog d = new Dialog(_activity, R.style.DialogSlideAnim);
			d.setContentView(R.layout.dialog_like);
			
			TextView dialog_textTitle = (TextView) d.findViewById(R.id.dialog_textTitle);
			ImageView dialog_imageProfile = (ImageView) d.findViewById(R.id.dialog_imageProfile);
			TextView dialog_textUsername = (TextView) d.findViewById(R.id.dialog_textUsername);
			TextView dialog_textAge = (TextView) d.findViewById(R.id.dialog_textAge);
			TextView dialog_textLocation = (TextView) d.findViewById(R.id.dialog_textLocation);
			RelativeLayout dialog_relativeLayoutSendMessage = (RelativeLayout) d.findViewById(R.id.dialog_relativeLayoutSendMessage);
			RelativeLayout dialog_relativeLayoutBid = (RelativeLayout) d.findViewById(R.id.dialog_relativeLayoutBid);
			RelativeLayout dialog_relativeLayoutReject = (RelativeLayout) d.findViewById(R.id.dialog_relativeLayoutReject);
			TextView dialog_textReject = (TextView) d.findViewById(R.id.dialog_textReject);
			RelativeLayout dialog_relativeLayoutProfile = (RelativeLayout) d.findViewById(R.id.dialog_relativeLayoutProfile);
			
			dialog_textTitle.setText(_activity.getResources().getString(R.string.acceptedoffers));
			Utils.getImageLoader(_activity).displayImage(profilesListFull_accepted.offers.get(position).profile.ImageUrl.replace("d150", "thumbs"), dialog_imageProfile, Utils.getImageOptions()); 	    
			dialog_textUsername.setText(profilesListFull_accepted.offers.get(position).profile.UserName);
			dialog_textAge.setText(_activity.getResources().getString(R.string.age)+" "+profilesListFull_accepted.offers.get(position).profile.UserAge+" "+_activity.getResources().getString(R.string.years));
			dialog_textLocation.setText(profilesListFull_accepted.offers.get(position).profile.UserCity+", "+profilesListFull_accepted.offers.get(position).profile.UserRegion);
			dialog_textReject.setText(_activity.getResources().getString(R.string.delete));
			
			
			
			dialog_relativeLayoutSendMessage.setOnClickListener(new OnClickListener() {
				
				@SuppressLint("NewApi") 
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Fragment fr;
					fr = new FragMessageView(null,profilesListFull_accepted.offers.get(position).profile,Integer.parseInt(profilesListFull_accepted.offers.get(position).offerId),null);
					FragmentManager fm = _activity.getFragmentManager();
					
			        FragmentTransaction fragmentTransaction = fm.beginTransaction();
			        fragmentTransaction.setCustomAnimations(R.anim.left_slidein_list_animator, R.anim.left_slideout_list_animator,R.anim.right_slidein_list_animator, R.anim.right_slideout_list_animator);
			        fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
			        fragmentTransaction.addToBackStack(null);
			        fragmentTransaction.commit();
			        d.cancel();
				}
			});
//			dialog_relativeLayoutBid.setOnClickListener(new OnClickListener() {
//				
//				@Override
//				public void onClick(View v) {
//					// TODO Auto-generated method stub
//					createOfferDialog(profilesListFull_accepted.offers.get(position).profile);
//					offerDialog.show();
//					d.cancel();
//				}
//			});
			
			dialog_imageProfile.setOnClickListener(new OnClickListener() {
				@SuppressLint("NewApi") 
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					ArrayList<UserProfile> _userProfilesFull_ = new ArrayList<UserProfile>();
					_userProfilesFull_.add(profilesListFull_accepted.offers.get(position).profile);
					
					Fragment fr = new FragProfile(2,_userProfilesFull_,0);				
					FragmentManager fm = _activity.getFragmentManager();
				    FragmentTransaction fragmentTransaction = fm.beginTransaction();
				    fragmentTransaction.setCustomAnimations(R.anim.left_slidein_list_animator, R.anim.left_slideout_list_animator,R.anim.right_slidein_list_animator, R.anim.right_slideout_list_animator);
				    fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
				    fragmentTransaction.addToBackStack(null);
				    fragmentTransaction.commit();
					d.cancel();
				}
			});
			
			dialog_relativeLayoutBid.setVisibility(View.GONE);
			
			dialog_relativeLayoutProfile.setOnClickListener(new OnClickListener() {
				@SuppressLint("NewApi") 
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					ArrayList<UserProfile> _userProfilesFull_ = new ArrayList<UserProfile>();
					_userProfilesFull_.add(profilesListFull_accepted.offers.get(position).profile);
					
					Fragment fr = new FragProfile(2,_userProfilesFull_,0);				
					FragmentManager fm = _activity.getFragmentManager();
				    FragmentTransaction fragmentTransaction = fm.beginTransaction();
				    fragmentTransaction.setCustomAnimations(R.anim.left_slidein_list_animator, R.anim.left_slideout_list_animator,R.anim.right_slidein_list_animator, R.anim.right_slideout_list_animator);
				    fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
				    fragmentTransaction.addToBackStack(null);
				    fragmentTransaction.commit();
					d.cancel();
				}
			});
			
			dialog_relativeLayoutReject.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					currentOfferid = profilesListFull_accepted.offers.get(position).offerId;
					new deleteLikeTask1().execute(currentOfferid);
					d.cancel();
				}
			});
			d.show();
		}
	};
	
	
	
	
	Dialog rejectDialog;
	
	private void createRejectDialog(){
		rejectDialog = new Dialog(_activity, R.style.DialogSlideAnim);
		rejectDialog.setContentView(R.layout.menu_reject);
		rejectDialog.setTitle(R.string.reject);
		
		final LinearLayout[] texts = new LinearLayout[4];
		texts[0] = (LinearLayout) rejectDialog.findViewById(R.id.txtReject1);
		texts[0].setTag("7");
		texts[1] = (LinearLayout) rejectDialog.findViewById(R.id.txtReject2);
		texts[1].setTag("6");
		texts[2] = (LinearLayout) rejectDialog.findViewById(R.id.txtReject3);
		texts[2].setTag("5");
		texts[3] = (LinearLayout) rejectDialog.findViewById(R.id.txtReject4);
		texts[3].setTag("8");
		
		final ImageView[] imagevs = new ImageView[4];
		imagevs[0] = (ImageView) rejectDialog.findViewById(R.id.imagetxtReject1);
		imagevs[1] = (ImageView) rejectDialog.findViewById(R.id.imagetxtReject2);
		imagevs[2] = (ImageView) rejectDialog.findViewById(R.id.imagetxtReject3);
		imagevs[3] = (ImageView) rejectDialog.findViewById(R.id.imagetxtReject4);
		
		OnClickListener clck = new OnClickListener() {
			@Override
			public void onClick(View v) {
				for(LinearLayout t:texts){
					t.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
				}
				for(ImageView t:imagevs){
					t.setBackgroundResource(R.drawable.search_unckeck);
				}
				v.setBackgroundResource(R.drawable.rectangle_search_white_corners);
				
				if(v.getTag().toString().equals("7"))
					imagevs[0].setBackgroundResource(R.drawable.search_check);
				if(v.getTag().toString().equals("6"))
					imagevs[1].setBackgroundResource(R.drawable.search_check);
				if(v.getTag().toString().equals("5"))
					imagevs[2].setBackgroundResource(R.drawable.search_check);
				if(v.getTag().toString().equals("8"))
					imagevs[3].setBackgroundResource(R.drawable.search_check);
				
				rejectReason = (String) v.getTag();
			}
		};
		
		for(LinearLayout t:texts){			
			t.setOnClickListener(clck);
		}
		
		TextView cancel = (TextView)rejectDialog.findViewById(R.id.cancel);
		TextView ok = (TextView)rejectDialog.findViewById(R.id.ok);

		cancel.setOnClickListener(new OnClickListener() {	
			@Override
			public void onClick(View v) {
				rejectDialog.dismiss();
			}
		});
		
		ok.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				rejectDialog.dismiss();
				new rejectLikeTask().execute(currentOfferid);				
			}
		});
	}
	
	
	
	class rejectLikeTask extends AsyncTask<String,Void,Void>{
		@Override
		protected Void doInBackground(String... params) {
			String res = ServiceClient.rejectLike(params[0], rejectReason);
			return null;
		}
		@Override
		protected void onPostExecute(Void p){
			profilesListFull_new.offers.clear();
			new getOffersTask(0).execute();
			
			Panel.refresh();
			Controller.refreshCounters(_activity);
		}
	}
	
	class acceptOfferTask extends AsyncTask<String,Void,Void>{

		@Override
		protected Void doInBackground(String... args) {			
			String result = ServiceClient.acceptOffer(MyUserId ,args[0]);
			return null;
		}
		
		@Override
		protected void onPostExecute(Void p){
			profilesListFull_new.offers.clear();
			new getOffersTask(0).execute();
			
			Panel.refresh();
			Controller.refreshCounters(_activity);
		}
		
	}
	
	class cancelLikeTask extends AsyncTask<String,Void,Void>{
		@Override
		protected Void doInBackground(String... args) {			
			String result = ServiceClient.likeActions(MyUserId ,"1", args[0]);			
			return null;
		}
		@Override
		protected void onPostExecute(Void p){
			profilesListFull_pending.offers.clear();
			new getOffersTask(1).execute();
			
			Panel.refresh();
			Controller.refreshCounters(_activity);
		}
		
	}
	
	class deleteLikeTask extends AsyncTask<String,Void,Void>{
		@Override
		protected Void doInBackground(String... args) {			
			String result = ServiceClient.likeActions( MyUserId, "2", args[0]);			
			return null;
		}
		@Override
		protected void onPostExecute(Void p){
			profilesListFull_rejected.offers.clear();
			new getOffersTask(3).execute();
			
			Panel.refresh();
			Controller.refreshCounters(_activity);
		}
	}
	
	class deleteLikeTask1 extends AsyncTask<String,Void,Void>{
		@Override
		protected Void doInBackground(String... args) {			
			String result = ServiceClient.likeActions( MyUserId, "2", args[0]);			
			return null;
		}
		@Override
		protected void onPostExecute(Void p){
			profilesListFull_accepted.offers.clear();
			new getOffersTask(2).execute();
		}
	}
	
	
	
}

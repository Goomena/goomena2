package com.goomena.app.adapters;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import com.google.android.gcm.GCMRegistrar;
import com.goomena.app.ActHome;
import com.goomena.app.ActRegister3;
import com.goomena.app.ActSplash;
import com.goomena.app.R;
import com.goomena.app.adapters.ObservableScrollView.ScrollViewListener;
import com.goomena.app.data.DataEditProfile;
import com.goomena.app.data.DataFetcher;
import com.goomena.app.data.ProfileManager;
import com.goomena.app.data.ServiceClient;
import com.goomena.app.global.Global;
import com.goomena.app.models.Country;
import com.goomena.app.models.CountryList;
import com.goomena.app.models.ItemList;
import com.goomena.app.models.UserDetails;
import com.goomena.app.models.UserProfile;
import com.goomena.app.models.GetEditProfile.ProfileEditLocation;
import com.goomena.app.models.GetEditProfile.ProfileEditLookingFor;
import com.goomena.app.models.GetEditProfile.ProfileEditOtherDetails;
import com.goomena.app.models.GetEditProfile.ProfileEditPersonal;
import com.goomena.app.models.GetEditProfile.ProfileEditPersonalInfoLists;
import com.goomena.app.tools.Log;
import com.goomena.app.tools.Utils;
import com.goomena.fragments.FragAddPhoto;
import com.goomena.fragments.FragEditProfile;
import com.goomena.fragments.FragLoading;
import com.goomena.fragments.FragMyPhoto;
import com.goomena.fragments.FragMyProfile;

import android.R.animator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class EditProfileAdapter extends PagerAdapter
{
	private Activity _activity;
	private LayoutInflater inflater;
	
	ObservableScrollView change_profile_scroll;
	LinearLayout change_profile_linearMenu;
	RelativeLayout change_profile_relativeAddphoto,change_profile_relativeMyphoto,change_profile_relativeMyProfile;
	RelativeLayout change_profile_R1;
	LinearLayout change_profile_L1;
	RelativeLayout change_profile_R2;
	LinearLayout change_profile_L2;
	RelativeLayout change_profile_R3;
	LinearLayout change_profile_L3;
	RelativeLayout change_profile_R4;
	LinearLayout change_profile_L4;
	RelativeLayout change_profile_R5;
	LinearLayout change_profile_L5;
	LinearLayout change_profile_linearL5_2;
	RelativeLayout change_profile_R6;
	LinearLayout change_profile_L6;
	RelativeLayout change_profile_UpDownN1,change_profile_UpDownN2,change_profile_UpDownN3,change_profile_UpDownN4,change_profile_UpDownN5,change_profile_UpDownN6;
	ProgressBar progressBarN1,progressBarN2,progressBarN3,progressBarN4,progressBarN5,progressBarN6;
	
	String region,country,city,zip;
	ArrayList<String> countries,regions,cities;
	HashMap<String, String> mapCountries;
	String countryIso;
	CountryList countryList;
	TextView change_profile_L1_1;
	Spinner change_profile_L1_2,change_profile_L1_3,change_profile_L1_4;
	EditText change_profile_L1_5;
	RelativeLayout change_profile_L1_save;
	
	EditText change_profile_L2_1,change_profile_L2_2,change_profile_L2_3,change_profile_L2_4,change_profile_L2_5,change_profile_L2_6;
	RelativeLayout change_profile_L2_save;
	
	Spinner change_profile_L3_1,change_profile_L3_2,change_profile_L3_3,change_profile_L3_4,change_profile_L3_5,change_profile_L3_6,change_profile_L3_7,change_profile_L3_8,change_profile_L3_9,change_profile_L3_10;
	LinearLayout change_profile_linearL3_10;
	RelativeLayout change_profile_L3_save;
	
	EditText change_profile_L4_1,change_profile_L4_2,change_profile_L4_3;
	RelativeLayout change_profile_L4_save;
	
	Spinner change_profile_L5_1,change_profile_L5_2;
	TextView change_profile_L5_3;
	RelativeLayout change_profile_L5_save;
	
	Spinner change_profile_L6_1;
	CheckBox change_profile_L6_2,change_profile_L6_3,change_profile_L6_4,change_profile_L6_5,change_profile_L6_6,change_profile_L6_7;
	RelativeLayout change_profile_L6_save;
	
	//-------------------------------------------------------------------------------------------------//
	
	ObservableScrollView settings_account_scroll;
	LinearLayout settings_account_linearMenu;
	RelativeLayout settings_account_relativeAddphoto,settings_account_relativeMyphoto,settings_account_relativeMyProfile;
	RelativeLayout settings_account_R0;
	LinearLayout settings_account_L0;
	RelativeLayout settings_account_R1;
	LinearLayout settings_account_L1;
	RelativeLayout settings_account_R2;
	LinearLayout settings_account_L2;
	RelativeLayout settings_account_R3;
	LinearLayout settings_account_L3;
	RelativeLayout settings_account_R4;
	LinearLayout settings_account_L4;
	RelativeLayout settings_account_UpDownN0,settings_account_UpDownN1,settings_account_UpDownN2,settings_account_UpDownN3,settings_account_UpDownN4;
	
	
	Spinner settings_account_L0_1;
	RelativeLayout settings_account_L0_save;
	
	EditText settings_account_L1_1,settings_account_L1_2,settings_account_L1_3;
	RelativeLayout settings_account_L1_save;
	
	RelativeLayout settings_account_L4_save;
	
	Animation animRotateUP;
	Animation animRotateDown;
	
	private final static String TAG_FRAGMENT = "TAG_FRAGMENT";
	UserProfile Myprofile = ProfileManager.loadProfile(_activity);
	
	FragLoading fragLoading;
	
	public EditProfileAdapter(Activity activity)
	{
		this._activity = activity;
		animRotateUP = AnimationUtils.loadAnimation(activity, R.anim.rotate_0_180);
		animRotateDown = AnimationUtils.loadAnimation(activity, R.anim.rotate_180_0);
		
		fragLoading = new FragLoading(activity);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return 2;
	}

	@Override
	public boolean isViewFromObject(View view, Object object) 
	{
		// TODO Auto-generated method stub
		return view == ((RelativeLayout) object);
	}
	
	@Override
	public void setPrimaryItem(ViewGroup container, int position, Object object) 
	{	
	
	}
	
	public Object instantiateItem(ViewGroup container, int position) 
	{
		inflater = (LayoutInflater) _activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		if(position==0)
		{
			View viewLayout = inflater.inflate(R.layout.change_profile, container, false);
			
			change_profile_scroll = (ObservableScrollView) viewLayout.findViewById(R.id.change_profile_scroll);
			change_profile_linearMenu = (LinearLayout) viewLayout.findViewById(R.id.change_profile_linearMenu);
			
			change_profile_relativeAddphoto = (RelativeLayout) viewLayout.findViewById(R.id.change_profile_relativeAddphoto);
			change_profile_relativeMyphoto = (RelativeLayout) viewLayout.findViewById(R.id.change_profile_relativeMyphoto);
			change_profile_relativeMyProfile = (RelativeLayout) viewLayout.findViewById(R.id.change_profile_relativeMyProfile);
			
			change_profile_R1 = (RelativeLayout) viewLayout.findViewById(R.id.change_profile_R1);
			change_profile_L1 = (LinearLayout) viewLayout.findViewById(R.id.change_profile_L1);
			change_profile_R2 = (RelativeLayout) viewLayout.findViewById(R.id.change_profile_R2);
			change_profile_L2 = (LinearLayout) viewLayout.findViewById(R.id.change_profile_L2);
			change_profile_R3 = (RelativeLayout) viewLayout.findViewById(R.id.change_profile_R3);
			change_profile_L3 = (LinearLayout) viewLayout.findViewById(R.id.change_profile_L3);
			change_profile_R4 = (RelativeLayout) viewLayout.findViewById(R.id.change_profile_R4);
			change_profile_L4 = (LinearLayout) viewLayout.findViewById(R.id.change_profile_L4);
			change_profile_R5 = (RelativeLayout) viewLayout.findViewById(R.id.change_profile_R5);
			change_profile_L5 = (LinearLayout) viewLayout.findViewById(R.id.change_profile_L5);
			change_profile_linearL5_2 = (LinearLayout) viewLayout.findViewById(R.id.change_profile_linearL5_2);
			if(Utils.getSharedPreferences(_activity).getString("genderid","").contains("2"))
			{
				change_profile_linearL5_2.setVisibility(View.GONE);
			}
			change_profile_R6 = (RelativeLayout) viewLayout.findViewById(R.id.change_profile_R6);
			change_profile_L6 = (LinearLayout) viewLayout.findViewById(R.id.change_profile_L6);
			progressBarN1 = (ProgressBar) viewLayout.findViewById(R.id.progressBarN1);
			progressBarN2 = (ProgressBar) viewLayout.findViewById(R.id.progressBarN2);
			progressBarN3 = (ProgressBar) viewLayout.findViewById(R.id.progressBarN3);
			progressBarN4 = (ProgressBar) viewLayout.findViewById(R.id.progressBarN4);
			progressBarN5 = (ProgressBar) viewLayout.findViewById(R.id.progressBarN5);
			progressBarN6 = (ProgressBar) viewLayout.findViewById(R.id.progressBarN6);
			change_profile_UpDownN1 = (RelativeLayout) viewLayout.findViewById(R.id.change_profile_UpDownN1);
			change_profile_UpDownN2 = (RelativeLayout) viewLayout.findViewById(R.id.change_profile_UpDownN2);
			change_profile_UpDownN3 = (RelativeLayout) viewLayout.findViewById(R.id.change_profile_UpDownN3);
			change_profile_UpDownN4 = (RelativeLayout) viewLayout.findViewById(R.id.change_profile_UpDownN4);
			change_profile_UpDownN5 = (RelativeLayout) viewLayout.findViewById(R.id.change_profile_UpDownN5);
			change_profile_UpDownN6 = (RelativeLayout) viewLayout.findViewById(R.id.change_profile_UpDownN6);
			
			change_profile_L1_1 = (TextView) viewLayout.findViewById(R.id.change_profile_L1_1);
			change_profile_L1_2 = (Spinner) viewLayout.findViewById(R.id.change_profile_L1_2);
			change_profile_L1_3 = (Spinner) viewLayout.findViewById(R.id.change_profile_L1_3);
			change_profile_L1_4 = (Spinner) viewLayout.findViewById(R.id.change_profile_L1_4);
			change_profile_L1_5 = (EditText) viewLayout.findViewById(R.id.change_profile_L1_5);
			change_profile_L1_save = (RelativeLayout) viewLayout.findViewById(R.id.change_profile_L1_save);
			//change_profile_L1_1.setText(Myprofile.UserCity+", "+Myprofile.UserRegion+", "+Myprofile.UserCountry);
			new getProfileEditLocation().execute();
			new getCountriesTask().execute();
			initSpinners();
			
			change_profile_L2_1 = (EditText) viewLayout.findViewById(R.id.change_profile_L2_1);
			change_profile_L2_2 = (EditText) viewLayout.findViewById(R.id.change_profile_L2_2);
			change_profile_L2_3 = (EditText) viewLayout.findViewById(R.id.change_profile_L2_3);
			change_profile_L2_4 = (EditText) viewLayout.findViewById(R.id.change_profile_L2_4);
			change_profile_L2_5 = (EditText) viewLayout.findViewById(R.id.change_profile_L2_5);
			change_profile_L2_6 = (EditText) viewLayout.findViewById(R.id.change_profile_L2_6);
			change_profile_L2_save = (RelativeLayout) viewLayout.findViewById(R.id.change_profile_L2_save);
			new getProfileEditPersonal().execute();
			
			
			change_profile_L3_1 = (Spinner) viewLayout.findViewById(R.id.change_profile_L3_1);
			change_profile_L3_2 = (Spinner) viewLayout.findViewById(R.id.change_profile_L3_2);
			change_profile_L3_3 = (Spinner) viewLayout.findViewById(R.id.change_profile_L3_3);
			change_profile_L3_4 = (Spinner) viewLayout.findViewById(R.id.change_profile_L3_4);
			change_profile_L3_5 = (Spinner) viewLayout.findViewById(R.id.change_profile_L3_5);
			change_profile_L3_6 = (Spinner) viewLayout.findViewById(R.id.change_profile_L3_6);
			change_profile_L3_7 = (Spinner) viewLayout.findViewById(R.id.change_profile_L3_7);
			change_profile_L3_8 = (Spinner) viewLayout.findViewById(R.id.change_profile_L3_8);
			change_profile_L3_9 = (Spinner) viewLayout.findViewById(R.id.change_profile_L3_9);
			change_profile_L3_10 = (Spinner) viewLayout.findViewById(R.id.change_profile_L3_10);
			change_profile_linearL3_10 = (LinearLayout) viewLayout.findViewById(R.id.change_profile_linearL3_10);
			if(Utils.getSharedPreferences(_activity).getString("genderid","").contains("1"))
			{
				change_profile_linearL3_10.setVisibility(View.GONE);
			}
			change_profile_L3_save = (RelativeLayout) viewLayout.findViewById(R.id.change_profile_L3_save);
			new getPersonalInfoLists(change_profile_L3_1,"height").execute();
			new getPersonalInfoLists(change_profile_L3_2,"bodytype").execute();
			new getPersonalInfoLists(change_profile_L3_3,"eyecolor").execute();
			new getPersonalInfoLists(change_profile_L3_4,"haircolor").execute();
			new getPersonalInfoLists(change_profile_L3_5,"childrennumber").execute();
			new getPersonalInfoLists(change_profile_L3_6,"ethnicity").execute();
			new getPersonalInfoLists(change_profile_L3_7,"religion").execute();
			new getPersonalInfoLists(change_profile_L3_8,"smoking").execute();
			new getPersonalInfoLists(change_profile_L3_10,"breastsize").execute();
			new getPersonalInfoLists(change_profile_L3_9,"drinking").execute();
			
			change_profile_L4_1 = (EditText) viewLayout.findViewById(R.id.change_profile_L4_1);
			change_profile_L4_2 = (EditText) viewLayout.findViewById(R.id.change_profile_L4_2);
			change_profile_L4_3 = (EditText) viewLayout.findViewById(R.id.change_profile_L4_3);
			change_profile_L4_save = (RelativeLayout) viewLayout.findViewById(R.id.change_profile_L4_save);
			new getProfileEditAboutMe().execute();
			
			change_profile_L5_1 = (Spinner) viewLayout.findViewById(R.id.change_profile_L5_1);
			change_profile_L5_2 = (Spinner) viewLayout.findViewById(R.id.change_profile_L5_2);
			change_profile_L5_3 = (TextView) viewLayout.findViewById(R.id.change_profile_L5_3);
			change_profile_L5_save = (RelativeLayout) viewLayout.findViewById(R.id.change_profile_L5_save);
			new getPersonalInfoLists(change_profile_L5_2,"income").execute();
			new getPersonalInfoLists(change_profile_L5_1,"education").execute();
			
			
			
			change_profile_L6_1 = (Spinner) viewLayout.findViewById(R.id.change_profile_L6_1);
			change_profile_L6_2 = (CheckBox) viewLayout.findViewById(R.id.change_profile_L6_2);
			change_profile_L6_3 = (CheckBox) viewLayout.findViewById(R.id.change_profile_L6_3);
			change_profile_L6_4 = (CheckBox) viewLayout.findViewById(R.id.change_profile_L6_4);
			change_profile_L6_5 = (CheckBox) viewLayout.findViewById(R.id.change_profile_L6_5);
			change_profile_L6_6 = (CheckBox) viewLayout.findViewById(R.id.change_profile_L6_6);
			change_profile_L6_7 = (CheckBox) viewLayout.findViewById(R.id.change_profile_L6_7);
			change_profile_L6_save = (RelativeLayout) viewLayout.findViewById(R.id.change_profile_L6_save);
			new getrelationshipstatus().execute();
			
			
			
			change_profile_scroll.setScrollViewListener(scrolllistener1);
			
			change_profile_relativeAddphoto.setOnClickListener(clickListener);
			change_profile_relativeMyphoto.setOnClickListener(clickListener);
			change_profile_relativeMyProfile.setOnClickListener(clickListener);
			change_profile_R1.setOnClickListener(clickListener);
			change_profile_R2.setOnClickListener(clickListener);
			change_profile_R3.setOnClickListener(clickListener);
			change_profile_R4.setOnClickListener(clickListener);
			change_profile_R5.setOnClickListener(clickListener);
			change_profile_R6.setOnClickListener(clickListener);
			
			change_profile_L1_save.setOnClickListener(SaveclickListener);
			change_profile_L2_save.setOnClickListener(SaveclickListener);
			change_profile_L3_save.setOnClickListener(SaveclickListener);
			change_profile_L4_save.setOnClickListener(SaveclickListener);
			change_profile_L5_save.setOnClickListener(SaveclickListener);
			change_profile_L6_save.setOnClickListener(SaveclickListener);
			
			change_profile_relativeMyProfile.setBackgroundColor(_activity.getResources().getColor(R.color.alphaGrey));
	
	
			((ViewPager) container).addView(viewLayout);
			return viewLayout;
		}
		else
		{
			View viewLayout = inflater.inflate(R.layout.settings_account, container, false);
			
			settings_account_scroll = (ObservableScrollView) viewLayout.findViewById(R.id.settings_account_scroll);
			settings_account_linearMenu = (LinearLayout) viewLayout.findViewById(R.id.settings_account_linearMenu);
			
			settings_account_relativeAddphoto = (RelativeLayout) viewLayout.findViewById(R.id.settings_account_relativeAddphoto);
			settings_account_relativeMyphoto = (RelativeLayout) viewLayout.findViewById(R.id.settings_account_relativeMyphoto);
			settings_account_relativeMyProfile = (RelativeLayout) viewLayout.findViewById(R.id.settings_account_relativeMyProfile);
			
			settings_account_R0 = (RelativeLayout) viewLayout.findViewById(R.id.settings_account_R0);
			settings_account_L0 = (LinearLayout) viewLayout.findViewById(R.id.settings_account_L0);
			settings_account_R1 = (RelativeLayout) viewLayout.findViewById(R.id.settings_account_R1);
			settings_account_L1 = (LinearLayout) viewLayout.findViewById(R.id.settings_account_L1);
			settings_account_R2 = (RelativeLayout) viewLayout.findViewById(R.id.settings_account_R2);
			settings_account_L2 = (LinearLayout) viewLayout.findViewById(R.id.settings_account_L2);
			settings_account_R3 = (RelativeLayout) viewLayout.findViewById(R.id.settings_account_R3);
			settings_account_L3 = (LinearLayout) viewLayout.findViewById(R.id.settings_account_L3);
			settings_account_R4 = (RelativeLayout) viewLayout.findViewById(R.id.settings_account_R4);
			settings_account_L4 = (LinearLayout) viewLayout.findViewById(R.id.settings_account_L4);
			settings_account_UpDownN0 = (RelativeLayout) viewLayout.findViewById(R.id.settings_account_UpDownN0);
			settings_account_UpDownN1 = (RelativeLayout) viewLayout.findViewById(R.id.settings_account_UpDownN1);
			settings_account_UpDownN2 = (RelativeLayout) viewLayout.findViewById(R.id.settings_account_UpDownN2);
			settings_account_UpDownN3 = (RelativeLayout) viewLayout.findViewById(R.id.settings_account_UpDownN3);
			settings_account_UpDownN4 = (RelativeLayout) viewLayout.findViewById(R.id.settings_account_UpDownN4);
			
			settings_account_L0_1 = (Spinner) viewLayout.findViewById(R.id.settings_account_L0_1);
			settings_account_L0_save = (RelativeLayout) viewLayout.findViewById(R.id.settings_account_L0_save);
			ArrayAdapter<CharSequence> adapter3 = ArrayAdapter.createFromResource(_activity,
			        R.array.Languages, R.layout.spinner_item);
			adapter3.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
			settings_account_L0_1.setAdapter(adapter3);
			
			
			settings_account_L1_1 = (EditText) viewLayout.findViewById(R.id.settings_account_L1_1);
			settings_account_L1_2 = (EditText) viewLayout.findViewById(R.id.settings_account_L1_2);
			settings_account_L1_3 = (EditText) viewLayout.findViewById(R.id.settings_account_L1_3);
			settings_account_L1_save = (RelativeLayout) viewLayout.findViewById(R.id.settings_account_L1_save);
			
			settings_account_L4_save = (RelativeLayout) viewLayout.findViewById(R.id.settings_account_L4_save);
			
			settings_account_scroll.setScrollViewListener(scrolllistener2);
			
			settings_account_relativeAddphoto.setOnClickListener(clickListener);
			settings_account_relativeMyphoto.setOnClickListener(clickListener);
			settings_account_relativeMyProfile.setOnClickListener(clickListener);
			settings_account_R0.setOnClickListener(clickListener);
			settings_account_R1.setOnClickListener(clickListener);
			settings_account_R2.setOnClickListener(clickListener);
			settings_account_R3.setOnClickListener(clickListener);
			settings_account_R4.setOnClickListener(clickListener);
			
			settings_account_L0_save.setOnClickListener(SaveclickListener);
			settings_account_L1_save.setOnClickListener(SaveclickListener);
			settings_account_L4_save.setOnClickListener(SaveclickListener);
			
			settings_account_relativeMyProfile.setBackgroundColor(_activity.getResources().getColor(R.color.alphaGrey));
			
			((ViewPager) container).addView(viewLayout); 
	        return viewLayout;
		}
	}
	
	

	@Override
    public void destroyItem(ViewGroup container, int position, Object object) 
	{
        ((ViewPager) container).removeView((RelativeLayout) object);
    }
	
	
	
	
	
	
	
	
	OnClickListener SaveclickListener = new OnClickListener() 
	{
		@SuppressLint("NewApi") public void onClick(View v) 
		{
			if(v == change_profile_L1_save)
			{
				if(change_profile_L1_2.getSelectedItem()!=null && change_profile_L1_3.getSelectedItem()!=null && change_profile_L1_4.getSelectedItem()!=null && change_profile_L1_5.getText()!=null)
				{
					new setProfileEditLocation().execute(mapCountries.get(change_profile_L1_2.getSelectedItem()),change_profile_L1_3.getSelectedItem().toString(),change_profile_L1_4.getSelectedItem().toString(),change_profile_L1_5.getText().toString());
				}
			}
			else if(v == change_profile_L2_save)
			{
				new setProfileEditPersonal().execute(change_profile_L2_1.getText().toString(),change_profile_L2_2.getText().toString(),change_profile_L2_3.getText().toString(),change_profile_L2_4.getText().toString(),change_profile_L2_5.getText().toString(),change_profile_L2_6.getText().toString());
			}
			else if(v == change_profile_L3_save)
			{
				new setProfileEditPersonalInfoLists().execute();
			}
			else if(v == change_profile_L4_save)
			{
				new postsetProfileEditAboutMe().execute(change_profile_L4_1.getText().toString(),change_profile_L4_2.getText().toString(),change_profile_L4_3.getText().toString());
			}
			else if(v == change_profile_L5_save)
			{
				new setProfileEditOtherDetails().execute();
			}
			else if(v == change_profile_L6_save)
			{
				new setProfileEditLookingFor().execute();
			}
			else if(v == settings_account_L0_save)
			{
				int glossa = (int)settings_account_L0_1.getSelectedItemId();
				SharedPreferences.Editor editor = _activity.getSharedPreferences("Lengua", _activity.MODE_PRIVATE).edit();
				
				if (glossa == 0) {
					
					editor.putInt("lan", 0); 
					editor.commit();
					Locale locale = new Locale("en"); 
					Locale.setDefault(locale);
					Configuration config = new Configuration();
					config.locale = locale;
					_activity.getBaseContext().getResources().updateConfiguration(config, 
						      _activity.getBaseContext().getResources().getDisplayMetrics());
					Global.Lang = "US";
						    
				} else if (glossa == 1 ) {
					
					editor.putInt("lan", 1); 
					editor.commit();
					Locale locale = new Locale("el"); 
					Locale.setDefault(locale);
					Configuration config = new Configuration();
					config.locale = locale;
					_activity.getBaseContext().getResources().updateConfiguration(config, 
							_activity.getBaseContext().getResources().getDisplayMetrics());
					Global.Lang = "GR";
					
				}
				else
				{
					editor.putInt("lan", 0); 
					editor.commit();
					Locale locale = new Locale("en"); 
					Locale.setDefault(locale);
					Configuration config = new Configuration();
					config.locale = locale;
					_activity.getBaseContext().getResources().updateConfiguration(config, 
						      _activity.getBaseContext().getResources().getDisplayMetrics());
					Global.Lang = "US";
				}
				
				Intent intent = new Intent(_activity, ActHome.class);
				 intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				 _activity.finish();
				 _activity.startActivity(intent);
			}
			else if(v == settings_account_L1_save) //---------------------------------------------------//
			{
				String oldpass = settings_account_L1_1.getText().toString();
				String newpass = settings_account_L1_2.getText().toString();
				String confpass = settings_account_L1_3.getText().toString();
				
				Log.v("APP", "tytytyty ");
				if(oldpass.equals(Utils.getSharedPreferences(_activity).getString("Pass", "")))
				{
					if(newpass.equals(confpass))
					{
						if(newpass.length()>2)
						{
							new changePassword_newPassword(newpass).execute();
						}
						else
						{
							Toast.makeText(_activity,_activity.getResources().getString(R.string.passtooshort), Toast.LENGTH_SHORT).show();
						}
					}
					else
					{
						Toast.makeText(_activity,"The password confirmation does not match", Toast.LENGTH_SHORT).show();
					}
				}
				else
				{
					Toast.makeText(_activity,"Error Current Password", Toast.LENGTH_SHORT).show();
				}
			}
			else if(v == settings_account_L4_save)
			{
				new changePassword_deleteAccount().execute();
			}
		}
	};
	
	
	
	
	private void initSpinners(){
		
		change_profile_L1_2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int index, long arg3) {
				if(countries!=null){
					country = countries.get(index);
					new getRegionsTask().execute();
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {				
			}
		});
		
		change_profile_L1_3.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int index, long arg3) {
				if(regions!=null){
					region = regions.get(index);
					new getCitiesTask().execute();
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
			}
			
		});
		
		change_profile_L1_4.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int index, long arg3) {
				if(cities!=null)
					city = cities.get(index);
					new getZipTask().execute();
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
			}
			
		});
	}
	
	
	class getCountriesTask extends AsyncTask<Void,Void,Void>
	{
		@Override
		protected void onPreExecute()
		{
			progressBarN1.setVisibility(View.VISIBLE);
		}
		@Override
		protected Void doInBackground(Void... params) {
			countryList = ServiceClient.getCountries();
			region = "";
			city = "";
			if(countryList!=null){
				mapCountries = new HashMap<String, String>();
				countries = new ArrayList<String>();
				for(Country c:countryList.countries){
					mapCountries.put(c.name, c.iso);
					countries.add(c.name);
				}
				
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(Void p){
			progressBarN1.setVisibility(View.GONE);
			if(countryList!=null){
				ArrayAdapter<String> adapter = new ArrayAdapter<String>(_activity, R.layout.spinner_item, countries);
				adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
				change_profile_L1_2.setAdapter(adapter);
				int po = 20;
				for(int i=0; i<countries.size(); i++)
				{
					if(mapCountries.get(countries.get(i)).contains(Myprofile.UserCountry))
					{
						po=i;
					}
				}
				change_profile_L1_2.setSelection(po);
				//change_profile_L1_2.setSelection(20); //20 = Greece
			
				new getRegionsTask().execute();
			}
		}
		
	}
	class getRegionsTask extends AsyncTask<Void,Void,Void>{
		@Override
		protected void onPreExecute()
		{
			progressBarN1.setVisibility(View.VISIBLE);
		}

		@Override
		protected Void doInBackground(Void... params) {
			countryIso = mapCountries.get( change_profile_L1_2.getSelectedItem() );
			String lagid = "EN";
			if(countryIso.equals("GR"))
				lagid = "GR";
			regions = ServiceClient.getCountryRegions(countryIso, lagid);
			
			return null;
		}
		
		@Override
		protected void onPostExecute(Void p){
			progressBarN1.setVisibility(View.GONE);
			if(regions!=null){
				ArrayAdapter<String> adapter = new ArrayAdapter<String>(_activity, R.layout.spinner_item, regions);
				adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
				change_profile_L1_3.setAdapter(adapter);
				int po = 0;
				for(int i=0; i<regions.size(); i++)
				{
					if(regions.get(i).contains(Myprofile.UserRegion))
					{
						po=i;
					}
				}
				change_profile_L1_3.setSelection(po);
				//change_profile_L1_3.setSelection(0);
			}
			city = "";
			new getCitiesTask().execute();
		}		
	}
	class getCitiesTask extends AsyncTask<Void,Void,Void>{

		@Override
		protected void onPreExecute()
		{
			progressBarN1.setVisibility(View.VISIBLE);
		}
		@Override
		protected Void doInBackground(Void... params) {
			String countryIso = mapCountries.get( change_profile_L1_2.getSelectedItem() );
			String lagid = "EN";
			if(countryIso.equals("GR"))
				lagid = "GR";
			region = region.replace(" ", "%20");
			cities = ServiceClient.getRegionCities(countryIso, region,lagid);
			
			region = region.replace("%20", " ");
			return null;
		}
		
		@Override
		protected void onPostExecute(Void p){
			progressBarN1.setVisibility(View.GONE);
			if(cities!=null){
				ArrayAdapter<String> adapter = new ArrayAdapter<String>(_activity, R.layout.spinner_item, cities);
				adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
				change_profile_L1_4.setAdapter(adapter);
				int po = 17;
				for(int i=0; i<cities.size(); i++)
				{
					if(cities.get(i).contains(Myprofile.UserCity))
					{
						po=i;
					}
				}
				change_profile_L1_4.setSelection(po);
				new getZipTask().execute();
			}
		}
		
	}
	
	class getZipTask extends AsyncTask<Void,Void,Void>
	{
		@Override
		protected void onPreExecute()
		{
			progressBarN1.setVisibility(View.VISIBLE);
		}
		@Override
		protected Void doInBackground(Void... params) {
			String countryIso = mapCountries.get( change_profile_L1_2.getSelectedItem() );
			String lagid = "EN";
			if(countryIso.equals("GR"))
				lagid = "GR";
			region = region.replace(" ", "%20");
			zip = ServiceClient.getRegionZip(countryIso, region,city,lagid);
			
			region = region.replace("%20", " ");
			return null;
		}
		
		@Override
		protected void onPostExecute(Void p){
			progressBarN1.setVisibility(View.GONE);
			if(cities!=null){
				
				zip = zip.replace("\"", "");
				change_profile_L1_5.setText(zip);
				//new getZipTask().execute();
			}
		}
		
	}
	
	class getProfileEditLocation extends AsyncTask<String,String,ProfileEditLocation>
	{
		
		@Override
		protected void onPreExecute()
		{
			progressBarN1.setVisibility(View.VISIBLE);
		}
		@Override
		protected ProfileEditLocation doInBackground(String... params) 
		{
			
			ProfileEditLocation result =DataEditProfile.getProfileEditLocation(_activity);
			return result;
		}
		
		@Override
		protected void onPostExecute(ProfileEditLocation s)
		{
			progressBarN1.setVisibility(View.GONE);
			if(s!=null)
			{
				change_profile_L1_1.setText(s.City+", "+s.Region+", "+s.Country.replace("GR", "Greece"));
				Utils.getSharedPreferences(_activity).edit().putString("UserCountry", s.Country).commit();
				Utils.getSharedPreferences(_activity).edit().putString("userregion", s.Region).commit();
				Utils.getSharedPreferences(_activity).edit().putString("usercity", s.City).commit();
				Utils.getSharedPreferences(_activity).edit().putString("zipcode", s.ZipCode).commit();
			}
		}		
	}
	
	class setProfileEditLocation extends AsyncTask<String,String,String>
	{
		@Override
		protected void onPreExecute() { fragLoading.Show(); }
		@Override
		protected String doInBackground(String... params) 
		{
			
			String result =DataEditProfile.setProfileEditLocation(params[0], params[1], params[2], params[3], _activity);
			return result;
		}
		
		@Override
		protected void onPostExecute(String s)
		{
			fragLoading.Close();
			Toast.makeText(_activity,s, Toast.LENGTH_SHORT).show();
			if(s.contains("ok"))
			{
				new getProfileEditLocation().execute();
			}
		}		
	}
	
	
	
	class getProfileEditPersonal extends AsyncTask<String,String,ProfileEditPersonal>
	{
		@Override
		protected void onPreExecute() {progressBarN2.setVisibility(View.VISIBLE);}
		@Override
		protected ProfileEditPersonal doInBackground(String... params) 
		{
			
			ProfileEditPersonal result =DataEditProfile.postgetProfileEditPersonal(_activity);
			return result;
		}
		
		@Override
		protected void onPostExecute(ProfileEditPersonal s)
		{
			progressBarN2.setVisibility(View.GONE);
			if(s!=null)
			{
				change_profile_L2_1.setText(s.FirstName);
				change_profile_L2_2.setText(s.LastName);
				change_profile_L2_3.setText(s.Address);
				change_profile_L2_4.setText(s.EmailAddress);
				change_profile_L2_5.setText(s.PhoneNumber);
				change_profile_L2_6.setText(s.PhoneNumber);
			}
		}		
	}
	
	class setProfileEditPersonal extends AsyncTask<String,String,String>
	{
		@Override
		protected void onPreExecute() { fragLoading.Show(); }
		@Override
		protected String doInBackground(String... params) 
		{
			String result =DataEditProfile.setProfileEditPersonal(params[0], params[1], params[2], params[3], params[4], params[5], _activity);
			return result;
		}
		
		@Override
		protected void onPostExecute(String s)
		{
			fragLoading.Close();
			Toast.makeText(_activity,s, Toast.LENGTH_SHORT).show();
		}		
	}
	
	
	class getPersonalInfoLists extends AsyncTask<String,String,ItemList>
	{
		Spinner _a;
		String _b;
		@Override
		protected void onPreExecute() {progressBarN3.setVisibility(View.VISIBLE);}
		public getPersonalInfoLists(Spinner a,String b)
		{
			this._a = a;
			this._b = b;
		}
		@Override
		protected ItemList doInBackground(String... params) 
		{
			if(_b.contains("breastsize"))
			{
				ItemList result = ServiceClient.getItems(_b, "US");
				return result;
			}
			else
			{
				ItemList result = ServiceClient.getItems(_b, Global.Lang);
				return result;
			}
		}
		
		@Override
		protected void onPostExecute(ItemList s)
		{
			progressBarN3.setVisibility(View.GONE);
			if(s!=null)
			{
				ArrayList<String> items = new ArrayList<String>();
				for (int i=0; i<s.items.size(); i++) {
					items.add( s.items.get(i).title.replace("&lt;", "<").replace("&gt;", ">").replace("&euro;", "€"));
				}
				ArrayAdapter <String> adapter2 = new ArrayAdapter(_activity,R.layout.spinner_item,items);
				adapter2.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
				_a.setAdapter(adapter2);
				
				if(_b.contains("drinking"))
				{
					new getProfileEditPersonalInfoLists().execute();
				}
				if(_b.contains("education"))
				{
					new getProfileEditOtherDetails().execute();//////////////////////////////////////////////////////////////
				}
			}
		}		
	}
	
	
	class getProfileEditPersonalInfoLists extends AsyncTask<String,String,ProfileEditPersonalInfoLists>
	{
		@Override
		protected void onPreExecute() {progressBarN3.setVisibility(View.VISIBLE);}
		@Override
		protected ProfileEditPersonalInfoLists doInBackground(String... params) 
		{
			
			ProfileEditPersonalInfoLists result = DataEditProfile.getProfileEditPersonalInfoLists(_activity);
			return result;
		}
		
		@Override
		protected void onPostExecute(ProfileEditPersonalInfoLists s)
		{
			progressBarN3.setVisibility(View.GONE);
			if(s!=null)
			{
				change_profile_L3_1.setSelection(s.Height-28);
				change_profile_L3_2.setSelection(s.BodyType-6);
				change_profile_L3_3.setSelection(s.EyeColor-8);
				change_profile_L3_4.setSelection(s.HairColor-10);
				change_profile_L3_5.setSelection(s.Children-1);
				change_profile_L3_6.setSelection(s.Ethnicity-1);
				change_profile_L3_7.setSelection(s.Religion-1);
				change_profile_L3_8.setSelection(s.Smoking-1);
				change_profile_L3_9.setSelection(s.Drinking-1);
				change_profile_L3_10.setSelection(s.BreastSize-1);
			}
		}		
	}
	
	class setProfileEditPersonalInfoLists extends AsyncTask<String,String,String>
	{
		@Override
		protected void onPreExecute() { fragLoading.Show(); }
		@Override
		protected String doInBackground(String... params) 
		{	
			String result = DataEditProfile.setProfileEditPersonalInfoLists(change_profile_L3_1.getSelectedItemId()+28, change_profile_L3_2.getSelectedItemId()+6,
					change_profile_L3_3.getSelectedItemId()+8, change_profile_L3_4.getSelectedItemId()+10, change_profile_L3_5.getSelectedItemId()+1,
					change_profile_L3_6.getSelectedItemId()+1, change_profile_L3_7.getSelectedItemId()+1, change_profile_L3_8.getSelectedItemId()+1,
					change_profile_L3_9.getSelectedItemId()+1, change_profile_L3_10.getSelectedItemId()+1, _activity);
			return result;
		}
		
		@Override
		protected void onPostExecute(String s)
		{
			if(s!=null)
			{
				
			}
			fragLoading.Close();
		}		
	}
	
	
	class getProfileEditAboutMe extends AsyncTask<String,String,UserDetails>
	{
		@Override
		protected void onPreExecute() {progressBarN4.setVisibility(View.VISIBLE);}
		@Override
		protected UserDetails doInBackground(String... params) 
		{
			
			UserDetails result =ServiceClient.getUserDetails(Myprofile.ProfileId, Global.Lang);
			return result;
		}
		
		@Override
		protected void onPostExecute(UserDetails s)
		{
			progressBarN4.setVisibility(View.GONE);
			if(s!=null)
			{
				change_profile_L4_1.setText(s.aboutme_heading);
				change_profile_L4_2.setText(s.aboutme_describeyourself);
				change_profile_L4_3.setText(s.aboutme_describedate);
			}
		}		
	}
	
	class postsetProfileEditAboutMe extends AsyncTask<String,String,String>
	{
		@Override
		protected void onPreExecute() { fragLoading.Show(); }
		@Override
		protected String doInBackground(String... params) 
		{
			
			String result =DataEditProfile.postsetProfileEditAboutMe(params[0], params[1],params[2],_activity);
			return result;
		}
		
		@Override
		protected void onPostExecute(String s)
		{
			fragLoading.Close();
			Log.v("APP", "tytytyu "+s);
		}		
	}
	
	class setProfileEditOtherDetails extends AsyncTask<String,String,String>
	{
		
		@Override
		protected void onPreExecute() { fragLoading.Show(); }
		@Override
		protected String doInBackground(String... params) 
		{	
			long t = 1;
			if(change_profile_L5_1.getSelectedItemId()==0)
			{
				t = 1;
			}
			else if(change_profile_L5_1.getSelectedItemId()==1)
			{
				t = 7;
			}
			else if(change_profile_L5_1.getSelectedItemId()==2)
			{
				t = 10;
			}
			else if(change_profile_L5_1.getSelectedItemId()==3)
			{
				t = 11;
			}
			else if(change_profile_L5_1.getSelectedItemId()==4)
			{
				t = 12;
			}
			
			String result = DataEditProfile.setProfileEditOtherDetails(t,
					change_profile_L5_2.getSelectedItemId()+20,change_profile_L5_3.getText().toString(), _activity);
			return result;
		}
		
		@Override
		protected void onPostExecute(String s)
		{
			if(s!=null)
			{
			}
			fragLoading.Close();
		}		
	}
	
	
	class getProfileEditOtherDetails extends AsyncTask<String,String,ProfileEditOtherDetails>
	{
		@Override
		protected void onPreExecute() {progressBarN5.setVisibility(View.VISIBLE);}
		@Override
		protected ProfileEditOtherDetails doInBackground(String... params) 
		{
			
			ProfileEditOtherDetails result = DataEditProfile.getProfileEditOtherDetails(_activity);
			return result;
		}
		
		@Override
		protected void onPostExecute(ProfileEditOtherDetails s)
		{
			progressBarN5.setVisibility(View.GONE);
			if(s!=null)
			{
				if(s.Education == 1)
				{
					change_profile_L5_1.setSelection(0);
				}
				else if(s.Education==7)
				{
					change_profile_L5_1.setSelection(1);
				}
				else if(s.Education==10)
				{
					change_profile_L5_1.setSelection(2);
				}
				else if(s.Education==11)
				{
					change_profile_L5_1.setSelection(3);
				}
				else if(s.Education==12)
				{
					change_profile_L5_1.setSelection(4);
				}
				change_profile_L5_2.setSelection(s.AnnualIncome-20);
				change_profile_L5_3.setText(s.Occupation);
			}
				
		}		
	}
	
	class getrelationshipstatus extends AsyncTask<String,String,ItemList>
	{
		@Override
		protected void onPreExecute() {progressBarN6.setVisibility(View.VISIBLE);}
		@Override
		protected ItemList doInBackground(String... params) 
		{
			
			ItemList result = ServiceClient.getItems("relationshipstatus", Global.Lang);
			return result;
		}
		
		@Override
		protected void onPostExecute(ItemList s)
		{
			progressBarN6.setVisibility(View.GONE);
			if(s!=null)
			{
				ArrayList<String> items = new ArrayList<String>();
				for (int i=0; i<s.items.size(); i++) {
					items.add( s.items.get(i).title);		    
				}
				ArrayAdapter <String> adapter2 = new ArrayAdapter(_activity,R.layout.spinner_item,items);
				adapter2.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
				change_profile_L6_1.setAdapter(adapter2);
				
				new gettypeofdating().execute();
			}
		}		
	}
	
	
	class gettypeofdating extends AsyncTask<String,String,ItemList>
	{
		@Override
		protected void onPreExecute() {progressBarN6.setVisibility(View.VISIBLE);}
		@Override
		protected ItemList doInBackground(String... params) 
		{
			
			ItemList result = ServiceClient.getItems("typeofdating", Global.Lang);
			return result;
		}
		
		@Override
		protected void onPostExecute(ItemList s)
		{
			progressBarN6.setVisibility(View.GONE);
			if(s!=null)
			{
				change_profile_L6_2.setText(s.items.get(0).title);
				change_profile_L6_3.setText(s.items.get(1).title);
				change_profile_L6_4.setText(s.items.get(2).title);
				change_profile_L6_5.setText(s.items.get(3).title);
				change_profile_L6_6.setText(s.items.get(4).title);
				change_profile_L6_7.setText(s.items.get(5).title);
				
				new getProfileEditLookingFor().execute();
			}
		}		
	}
	
	class getProfileEditLookingFor extends AsyncTask<String,String,ProfileEditLookingFor>
	{
		@Override
		protected void onPreExecute() {progressBarN6.setVisibility(View.VISIBLE);}
		@Override
		protected ProfileEditLookingFor doInBackground(String... params) 
		{
			
			ProfileEditLookingFor result = DataEditProfile.getProfileEditLookingFor(_activity);
			return result;
		}
		
		@Override
		protected void onPostExecute(ProfileEditLookingFor s)
		{
			progressBarN6.setVisibility(View.GONE);
			if(s!=null)
			{
				if(s.RelationshipStatus == 8)
				{
					change_profile_L6_1.setSelection(6);
				}
				else if(s.RelationshipStatus == 9)
				{
					change_profile_L6_1.setSelection(7);
				}
				else
				{
					change_profile_L6_1.setSelection(s.RelationshipStatus-1);
				}
				
				if(s.TODShort.contains("True")) { change_profile_L6_2.setChecked(true); }
				else { change_profile_L6_2.setChecked(false); }

				if(s.TODFriendship.contains("True")) { change_profile_L6_3.setChecked(true); }
				else { change_profile_L6_3.setChecked(false); }
				
				if(s.TODLongTerm.contains("True")) { change_profile_L6_4.setChecked(true); }
				else { change_profile_L6_4.setChecked(false); }

				if(s.TODMutually.contains("True")) { change_profile_L6_5.setChecked(true); }
				else { change_profile_L6_5.setChecked(false); }
				
				if(s.TODMarried.contains("True")) { change_profile_L6_6.setChecked(true); }
				else { change_profile_L6_6.setChecked(false); }
				
				if(s.TODCasual.contains("True")) { change_profile_L6_7.setChecked(true); }
				else { change_profile_L6_7.setChecked(false); }
			}
		}		
	}
	
	
	
	class setProfileEditLookingFor extends AsyncTask<String,String,String>
	{
		@Override
		protected void onPreExecute() { fragLoading.Show(); }
		@Override
		protected String doInBackground(String... params) 
		{
			long t;
			if(change_profile_L6_1.getSelectedItemId()==6)
			{
				t = change_profile_L6_1.getSelectedItemId()+2;
			}
			if(change_profile_L6_1.getSelectedItemId()==7)
			{
				t = change_profile_L6_1.getSelectedItemId()+2;
			}
			else
			{
				t = change_profile_L6_1.getSelectedItemId()+1;
			}
			
			String result = DataEditProfile.setProfileEditLookingFor(t, change_profile_L6_2.isChecked(), change_profile_L6_3.isChecked(), change_profile_L6_4.isChecked(),
					change_profile_L6_5.isChecked(), change_profile_L6_6.isChecked(), change_profile_L6_7.isChecked(), _activity);
			return result;
		}
		
		@Override
		protected void onPostExecute(String s)
		{
			if(s!=null)
			{
			}
			fragLoading.Close();
		}		
	}
	
	
	
	
	
	
	
	class changePassword_newPassword extends AsyncTask<String,String,String>
	{
		String _newPass;
		changePassword_newPassword(String newPass)
		{
			this._newPass = newPass;
		}
		@Override
		protected void onPreExecute() { fragLoading.Show(); }
		@Override
		protected String doInBackground(String... params) 
		{
			String result = DataEditProfile.changePassword_newPassword(_newPass, _activity);
			return result;
		}
		
		@Override
		protected void onPostExecute(String s)
		{
			if(s!=null)
			{
				if(s.equals("ok"))
				{
					Utils.getSharedPreferences(_activity).edit().putString("Pass", _newPass);
				}
				else
				{
					Toast.makeText(_activity,s, Toast.LENGTH_SHORT).show();
				}
			}
			fragLoading.Close();
		}		
	}
	
	class changePassword_deleteAccount extends AsyncTask<String,String,String>
	{
		@Override
		protected void onPreExecute() { fragLoading.Show();}
		@Override
		protected String doInBackground(String... params) 
		{
			String result = DataEditProfile.changePassword_deleteAccount(_activity);
			return result;
		}
		
		@Override
		protected void onPostExecute(String s)
		{
			if(s!=null)
			{
				if(s.contains("ok"))
				{
					Utils.getSharedPreferences(_activity).edit().putString("Email", "").commit();
					Utils.getSharedPreferences(_activity).edit().putString("Pass", "").commit();
					new disconectGCMTask().execute();
				}
				else
				{
					Toast.makeText(_activity,s, Toast.LENGTH_SHORT).show();
				}
			}
			fragLoading.Close();
		}		
	}
	
	class disconectGCMTask extends AsyncTask<String, Void, Void>
	{
		@Override
		protected Void doInBackground(String... params){
			String regId = Utils.getSharedPreferences(_activity).getString("reg", "");
			ServiceClient.disconectGCM(regId);
			return null;
		}
		
		@Override
		protected void onPostExecute(Void v){
			ProfileManager.disconnect(_activity);
			GCMRegistrar.unregister(_activity);
			GCMRegistrar.setRegisteredOnServer(_activity, false);
			Intent in = new Intent(_activity,ActSplash.class);
			//in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
			_activity.startActivity(in);
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	ScrollViewListener scrolllistener1 = new ScrollViewListener()
	{
		int t=0,u=0;
		@Override
		public void onScrollStopped() {}
		@SuppressLint("NewApi") 
		@Override
		public void onScrollChanged(ScrollView scrollView, int x, int y, int oldx,int oldy) {
			// TODO Auto-generated method stub
			if(y==0)
			{
				change_profile_linearMenu.setTranslationY(0);
				t=0;
				u=0;
			}
			else
			{
				if(y > oldy)
				{
					u = u + (y-oldy);
					change_profile_linearMenu.setTranslationY(u);
					t=200;
				}
				else if(oldy > y)
				{
					t = t - (oldy-y) ;
					if(t>0)
					{
						change_profile_linearMenu.setTranslationY(t);
					}
					else
					{
						t=0;
						change_profile_linearMenu.setTranslationY(0);
					}
					u=t;
				}
			}
		}
	};
	
	ScrollViewListener scrolllistener2 = new ScrollViewListener()
	{
		int t=0,u=0;
		@Override
		public void onScrollStopped() {}
		@SuppressLint("NewApi") 
		@Override
		public void onScrollChanged(ScrollView scrollView, int x, int y, int oldx,int oldy) {
			// TODO Auto-generated method stub
			if(y==0)
			{
				settings_account_linearMenu.setTranslationY(0);
				t=0;
				u=0;
			}
			else
			{
				if(y > oldy)
				{
					u = u + (y-oldy);
					settings_account_linearMenu.setTranslationY(u);
					t=200;
				}
				else if(oldy > y)
				{
					t = t - (oldy-y) ;
					if(t>0)
					{
						settings_account_linearMenu.setTranslationY(t);
					}
					else
					{
						t=0;
						settings_account_linearMenu.setTranslationY(0);
					}
					u=t;
				}
			}
		}
	};
	
	@SuppressLint("NewApi") 
	OnClickListener clickListener = new OnClickListener() 
	{
		public void onClick(View v) 
		{
			if(v == change_profile_R1)
			{
				Animation animBounce = AnimationUtils.loadAnimation(_activity, R.anim.bounce);
				
				if(change_profile_L1.getVisibility() == View.GONE)
				{ 
					change_profile_L1.setVisibility(View.VISIBLE);
					change_profile_L1.startAnimation(animBounce);
					change_profile_UpDownN1.startAnimation(animRotateUP);
				}
				else 
				{
					change_profile_L1.setVisibility(View.GONE);
					change_profile_L1.clearAnimation();
					change_profile_UpDownN1.startAnimation(animRotateDown);
				}
			}
			else if(v == change_profile_R2)
			{
				Animation animBounce = AnimationUtils.loadAnimation(_activity, R.anim.bounce);
				if(change_profile_L2.getVisibility() == View.GONE)
				{ 
					change_profile_L2.setVisibility(View.VISIBLE);
					change_profile_L2.startAnimation(animBounce);
					change_profile_UpDownN2.startAnimation(animRotateUP);
				}
				else 
				{
					change_profile_L2.setVisibility(View.GONE); 
					change_profile_L2.clearAnimation();
					change_profile_UpDownN2.startAnimation(animRotateDown);
				}
			}
			else if(v == change_profile_R3)
			{
				Animation animBounce = AnimationUtils.loadAnimation(_activity, R.anim.bounce);
				if(change_profile_L3.getVisibility() == View.GONE)
				{ 
					change_profile_L3.setVisibility(View.VISIBLE);
					change_profile_L3.startAnimation(animBounce);
					change_profile_UpDownN3.startAnimation(animRotateUP);
				}
				else 
				{ 
					change_profile_L3.setVisibility(View.GONE); 
					change_profile_L3.clearAnimation();
					change_profile_UpDownN3.startAnimation(animRotateDown);
				}
			}
			else if(v == change_profile_R4)
			{
				Animation animBounce = AnimationUtils.loadAnimation(_activity, R.anim.bounce);
				if(change_profile_L4.getVisibility() == View.GONE)
				{ 
					change_profile_L4.setVisibility(View.VISIBLE);
					change_profile_L4.startAnimation(animBounce);
					change_profile_UpDownN4.startAnimation(animRotateUP);
				}
				else 
				{
					change_profile_L4.setVisibility(View.GONE);
					change_profile_L4.clearAnimation();
					change_profile_UpDownN4.startAnimation(animRotateDown);
				}
			}
			else if(v == change_profile_R5)
			{
				Animation animBounce = AnimationUtils.loadAnimation(_activity, R.anim.bounce);
				if(change_profile_L5.getVisibility() == View.GONE)
				{ 
					change_profile_L5.setVisibility(View.VISIBLE);
					change_profile_L5.startAnimation(animBounce);
					change_profile_UpDownN5.startAnimation(animRotateUP);
				}
				else 
				{
					change_profile_L5.setVisibility(View.GONE);
					change_profile_L5.clearAnimation();
					change_profile_UpDownN5.startAnimation(animRotateDown);
				}
			}
			else if(v == change_profile_R6)
			{
				Animation animBounce = AnimationUtils.loadAnimation(_activity, R.anim.bounce);
				if(change_profile_L6.getVisibility() == View.GONE)
				{ 
					change_profile_L6.setVisibility(View.VISIBLE);
					change_profile_L6.startAnimation(animBounce);
					change_profile_UpDownN6.startAnimation(animRotateUP);
				}
				else
				{
					change_profile_L6.setVisibility(View.GONE);
					change_profile_L6.clearAnimation();
					change_profile_UpDownN6.startAnimation(animRotateDown);
				}
			}
			else if(v == settings_account_R0)
			{
				Animation animBounce = AnimationUtils.loadAnimation(_activity, R.anim.bounce);
				if(settings_account_L0.getVisibility() == View.GONE)
				{ 
					settings_account_L0.setVisibility(View.VISIBLE);
					settings_account_L0.startAnimation(animBounce);
					settings_account_UpDownN0.startAnimation(animRotateUP);
				}
				else
				{
					settings_account_L0.setVisibility(View.GONE);
					settings_account_L0.clearAnimation();
					settings_account_UpDownN0.startAnimation(animRotateDown);
				}
			}
			else if(v == settings_account_R1)
			{
				Animation animBounce = AnimationUtils.loadAnimation(_activity, R.anim.bounce);
				if(settings_account_L1.getVisibility() == View.GONE)
				{ 
					settings_account_L1.setVisibility(View.VISIBLE);
					settings_account_L1.startAnimation(animBounce);
					settings_account_UpDownN1.startAnimation(animRotateUP);
				}
				else
				{
					settings_account_L1.setVisibility(View.GONE);
					settings_account_L1.clearAnimation();
					settings_account_UpDownN1.startAnimation(animRotateDown);
				}
			}
			else if(v == settings_account_R2)
			{
				Animation animBounce = AnimationUtils.loadAnimation(_activity, R.anim.bounce);
				if(settings_account_L2.getVisibility() == View.GONE)
				{ 
					settings_account_L2.setVisibility(View.VISIBLE);
					settings_account_L2.startAnimation(animBounce);
					settings_account_UpDownN2.startAnimation(animRotateUP);
				}
				else
				{
					settings_account_L2.setVisibility(View.GONE);
					settings_account_L2.clearAnimation();
					settings_account_UpDownN2.startAnimation(animRotateDown);
				}
			}
			else if(v == settings_account_R4)
			{
				Animation animBounce = AnimationUtils.loadAnimation(_activity, R.anim.bounce);
				if(settings_account_L4.getVisibility() == View.GONE)
				{ 
					settings_account_L4.setVisibility(View.VISIBLE);
					settings_account_L4.startAnimation(animBounce);
					settings_account_UpDownN4.startAnimation(animRotateUP);
				}
				else
				{
					settings_account_L4.setVisibility(View.GONE);
					settings_account_L4.clearAnimation();
					settings_account_UpDownN4.startAnimation(animRotateDown);
				}
			}
			else if(v == change_profile_relativeAddphoto)
			{
				Fragment fr = new FragAddPhoto();
				FragmentManager fm = _activity.getFragmentManager();
				
		        FragmentTransaction fragmentTransaction = fm.beginTransaction();
		        fragmentTransaction.setCustomAnimations(R.anim.left_slidein_list_animator, R.anim.left_slideout_list_animator,R.anim.right_slidein_list_animator, R.anim.right_slideout_list_animator);
		        fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
		        fragmentTransaction.addToBackStack(null);
		
		        fragmentTransaction.commit();
			}
			else if(v == change_profile_relativeMyphoto)
			{
				Fragment fr = new FragMyPhoto();
				FragmentManager fm = _activity.getFragmentManager();
				
		        FragmentTransaction fragmentTransaction = fm.beginTransaction();
		        fragmentTransaction.setCustomAnimations(R.anim.left_slidein_list_animator, R.anim.left_slideout_list_animator,R.anim.right_slidein_list_animator, R.anim.right_slideout_list_animator);
		        fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
		        fragmentTransaction.addToBackStack(null);
		
		        fragmentTransaction.commit();
			}
			else if(v == change_profile_relativeMyProfile)
			{
				/*
				Fragment fr = new FragMyProfile();
				FragmentManager fm = _activity.getFragmentManager();
				
		        FragmentTransaction fragmentTransaction = fm.beginTransaction();
		        fragmentTransaction.setCustomAnimations(R.anim.left_slidein_list_animator, R.anim.left_slideout_list_animator,R.anim.right_slidein_list_animator, R.anim.right_slideout_list_animator);
		        fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
		        fragmentTransaction.addToBackStack(null);
		
		        fragmentTransaction.commit();
		        */
			}
			else if(v == settings_account_relativeAddphoto)
			{
				Fragment fr = new FragAddPhoto();
				FragmentManager fm = _activity.getFragmentManager();
				
		        FragmentTransaction fragmentTransaction = fm.beginTransaction();
		        fragmentTransaction.setCustomAnimations(R.anim.left_slidein_list_animator, R.anim.left_slideout_list_animator,R.anim.right_slidein_list_animator, R.anim.right_slideout_list_animator);
		        fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
		        fragmentTransaction.addToBackStack(null);
		
		        fragmentTransaction.commit();
			}
			else if(v == settings_account_relativeMyphoto)
			{
				Fragment fr = new FragMyPhoto();
				FragmentManager fm = _activity.getFragmentManager();
				
		        FragmentTransaction fragmentTransaction = fm.beginTransaction();
		        fragmentTransaction.setCustomAnimations(R.anim.left_slidein_list_animator, R.anim.left_slideout_list_animator,R.anim.right_slidein_list_animator, R.anim.right_slideout_list_animator);
		        fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
		        fragmentTransaction.addToBackStack(null);
		
		        fragmentTransaction.commit();
			}
			else if(v == settings_account_relativeMyProfile)
			{
				/*
				Fragment fr = new FragMyProfile();
				FragmentManager fm = _activity.getFragmentManager();
				
		        FragmentTransaction fragmentTransaction = fm.beginTransaction();
		        fragmentTransaction.setCustomAnimations(R.anim.left_slidein_list_animator, R.anim.left_slideout_list_animator,R.anim.right_slidein_list_animator, R.anim.right_slideout_list_animator);
		        fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
		        fragmentTransaction.addToBackStack(null);
		
		        fragmentTransaction.commit();
		        */
			}
		}
	};
	
	
}

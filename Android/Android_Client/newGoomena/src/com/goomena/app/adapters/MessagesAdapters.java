package com.goomena.app.adapters;

import java.util.ArrayList;

import com.goomena.app.Controller;
import com.goomena.app.R;
import com.goomena.app.adapters.OffersAdapters.getOffersTask;
import com.goomena.app.data.ServiceClient;
import com.goomena.app.global.Global;
import com.goomena.app.global.GlobalMessages;
import com.goomena.app.models.MessageInfo;
import com.goomena.app.models.MessageInfoList;
import com.goomena.app.models.Offer;
import com.goomena.app.models.OfferList;
import com.goomena.app.tools.Log;
import com.goomena.app.tools.Utils;
import com.goomena.fragments.FragLoading;
import com.goomena.fragments.FragMessageView;
import com.goomena.fragments.FragProfile;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView.OnItemClickListener;

@SuppressLint("NewApi") 
public class MessagesAdapters extends PagerAdapter
{
	private Activity _activity;
	private LayoutInflater inflater;
	
	public int viewMode=2;
	
	ListView messageListView;
	MessageInfoList profilesListFull_new,profilesListFull_inbox,profilesListFull_sent,profilesListFull_trash;
	
	FragLoading fragLoading;
	
	
	boolean allowCall_new = true;
	boolean allowCall_inbox = true;
	boolean allowCall_sent = true;
	boolean allowCall_trash = true;
	
	ItemMessageAdapter adapter_new,adapter_inbox,adapter_sent,adapter_trash;
	String userid,genderid;
	String MyUserId;
	
	//OnMessageSelectedListener callback;
	
	boolean allowCall = true;
	int mode;
	
	TextView textInactive,textWarn;
		
	boolean active = true;
	
	private final static String TAG_FRAGMENT = "TAG_FRAGMENT";
	
	Handler handler_new,handler_inbox,handler_sent,handler_trash;
	
	public MessagesAdapters(Activity activity)
	{
		this._activity = activity;
		
		SharedPreferences settings = Utils.getSharedPreferences(_activity);
		MyUserId = Utils.getSharedPreferences(activity).getString("userid", "");
		genderid = settings.getString("genderid", "");
		
		
		profilesListFull_new = new MessageInfoList();
		profilesListFull_new.msgList = new ArrayList<MessageInfo>();
		
		profilesListFull_inbox = new MessageInfoList();
		profilesListFull_inbox.msgList = new ArrayList<MessageInfo>();
		
		profilesListFull_sent = new MessageInfoList();
		profilesListFull_sent.msgList = new ArrayList<MessageInfo>();
		
		profilesListFull_trash = new MessageInfoList();
		profilesListFull_trash.msgList = new ArrayList<MessageInfo>();
		
		
		adapter_new = new ItemMessageAdapter(_activity,8);
		adapter_inbox = new ItemMessageAdapter(_activity,1);
		adapter_sent = new ItemMessageAdapter(_activity,2);
		adapter_trash = new ItemMessageAdapter(_activity,16);
		
		
		fragLoading = new FragLoading(activity);
		
		profilesListFull_new.msgList.clear();
		new getNewMessagesTask(8).execute();
		
		profilesListFull_inbox.msgList.clear();
		new getNewMessagesTask(1).execute();
		
		profilesListFull_sent.msgList.clear();
		new getNewMessagesTask(2).execute();
		
		profilesListFull_trash.msgList.clear();
		new getNewMessagesTask(16).execute();
		
	}
	
	@Override
	public int getCount() 
	{
		// TODO Auto-generated method stub
		return 4;
	}
	@Override
	public boolean isViewFromObject(View view, Object object) 
	{
		// TODO Auto-generated method stub
		return view == ((RelativeLayout) object);
	}
	
	@Override
	public void setPrimaryItem(ViewGroup container, int position, Object object) 
	{	
	
	}
	
    @SuppressLint("HandlerLeak") 
    public Object instantiateItem(ViewGroup container, int position) 
	{
		inflater = (LayoutInflater) _activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View viewLayout = inflater.inflate(R.layout.fr_messages_new, container, false);
		messageListView = (ListView)viewLayout.findViewById(R.id.messageList);
		textWarn = (TextView)viewLayout.findViewById(R.id.textWarn);
		textInactive = (TextView)viewLayout.findViewById(R.id.textInactive);
		
		final RelativeLayout include_view_no_result = (RelativeLayout) viewLayout.findViewById(R.id.include_view_no_result);
		final TextView view_noresult_textTitle = (TextView) viewLayout.findViewById(R.id.view_noresult_textTitle);
		
		
		if(position == 0)
		{
			messageListView.setAdapter(adapter_new);
			messageListView.setOnScrollListener(myOnScrollListener_new);
			messageListView.setOnItemClickListener(myOnItemClickListener_new);
			
			if(profilesListFull_new.msgList.size()==0)
			{
				view_noresult_textTitle.setText(_activity.getResources().getString(R.string.noresult_title_you_have_no_messages));
				include_view_no_result.setVisibility(RelativeLayout.VISIBLE);
			}
			handler_new = new Handler()
			{
				public void handleMessage(Message msg) 
				{	
					if(msg.obj.toString().equals("0"))
					{
						view_noresult_textTitle.setText(_activity.getResources().getString(R.string.noresult_title_you_have_no_messages));
						include_view_no_result.setVisibility(RelativeLayout.VISIBLE);
					}
					else
					{
						include_view_no_result.setVisibility(RelativeLayout.GONE);
					}
				}
			};
		}
		if(position == 1)
		{
			messageListView.setAdapter(adapter_inbox);
			messageListView.setOnScrollListener(myOnScrollListener_inbox);
			messageListView.setOnItemClickListener(myOnItemClickListener_inbox);
			
			if(profilesListFull_inbox.msgList.size()==0)
			{
				view_noresult_textTitle.setText(_activity.getResources().getString(R.string.noresult_title_you_have_no_messages));
				include_view_no_result.setVisibility(RelativeLayout.VISIBLE);
			}
			handler_inbox = new Handler()
			{
				public void handleMessage(Message msg) 
				{	
					if(msg.obj.toString().equals("0"))
					{
						view_noresult_textTitle.setText(_activity.getResources().getString(R.string.noresult_title_you_have_no_messages));
						include_view_no_result.setVisibility(RelativeLayout.VISIBLE);
					}
					else
					{
						include_view_no_result.setVisibility(RelativeLayout.GONE);
					}
				}
			};
		}
		if(position == 2)
		{
			messageListView.setAdapter(adapter_sent);
			messageListView.setOnScrollListener(myOnScrollListener_sent);
			messageListView.setOnItemClickListener(myOnItemClickListener_sent);
			
			if(profilesListFull_sent.msgList.size()==0)
			{
				view_noresult_textTitle.setText(_activity.getResources().getString(R.string.noresult_title_you_have_no_messages));
				include_view_no_result.setVisibility(RelativeLayout.VISIBLE);
			}
			handler_sent = new Handler()
			{
				public void handleMessage(Message msg) 
				{	
					if(msg.obj.toString().equals("0"))
					{
						view_noresult_textTitle.setText(_activity.getResources().getString(R.string.noresult_title_you_have_no_messages));
						include_view_no_result.setVisibility(RelativeLayout.VISIBLE);
					}
					else
					{
						include_view_no_result.setVisibility(RelativeLayout.GONE);
					}
				}
			};
		}
		if(position == 3)
		{
			messageListView.setAdapter(adapter_trash);
			messageListView.setOnScrollListener(myOnScrollListener_trash);
			messageListView.setOnItemClickListener(myOnItemClickListener_trash);
			
			if(profilesListFull_trash.msgList.size()==0)
			{
				view_noresult_textTitle.setText(_activity.getResources().getString(R.string.noresult_title_you_have_no_messages));
				include_view_no_result.setVisibility(RelativeLayout.VISIBLE);
			}
			handler_trash = new Handler()
			{
				public void handleMessage(Message msg) 
				{	
					if(msg.obj.toString().equals("0"))
					{
						view_noresult_textTitle.setText(_activity.getResources().getString(R.string.noresult_title_you_have_no_messages));
						include_view_no_result.setVisibility(RelativeLayout.VISIBLE);
					}
					else
					{
						include_view_no_result.setVisibility(RelativeLayout.GONE);
					}
				}
			};
		}
		
		
		
		
		((ViewPager) container).addView(viewLayout);
		 
        return viewLayout;
	}
	
	@Override
    public void destroyItem(ViewGroup container, int position, Object object) 
	{
        ((ViewPager) container).removeView((RelativeLayout) object);
    }
	
	
	
	
	
	
	
	
	
	class getNewMessagesTask extends AsyncTask<Void,Integer,MessageInfoList>
	{
		int _viewMode;
		getNewMessagesTask(int viewMode)
		{
			this._viewMode = viewMode;
		}
		
		@Override
		protected void onPreExecute() {  fragLoading.Show(); }
		
		@Override
		protected MessageInfoList doInBackground(Void... params)
		{
			MessageInfoList profilesList = null;
			
			if(_viewMode==8)
			{
				profilesList = ServiceClient.getMessagesList(MyUserId,String.valueOf(8),String.valueOf(profilesListFull_new.msgList.size()),"1");
			}
			else if(_viewMode==1)
			{
				profilesList = ServiceClient.getMessagesList(MyUserId,String.valueOf(1),String.valueOf(profilesListFull_inbox.msgList.size()),"1");
			}
			else if(_viewMode==2)
			{
				profilesList = ServiceClient.getMessagesList(MyUserId,String.valueOf(2),String.valueOf(profilesListFull_sent.msgList.size()),"1");
			}
			else if(_viewMode==16)
			{
				profilesList = ServiceClient.getMessagesList(MyUserId,String.valueOf(16),String.valueOf(profilesListFull_trash.msgList.size()),"1");
			}
			return profilesList;
		}
		
		@Override
		protected void onPostExecute(MessageInfoList result)
		{
			if(result!=null)
			{
				if(_viewMode==8)
				{
					profilesListFull_new.msgList.addAll(result.msgList);
					adapter_new.setItems(profilesListFull_new.msgList);
					adapter_new.notifyDataSetChanged();
					
					if(handler_new!=null)
					{
						if(profilesListFull_new.msgList.size()==0)
						{
							Message msg = handler_new.obtainMessage();
							msg.obj = "0";
							handler_new.sendMessage(msg);
						}
						else
						{
							Message msg = handler_new.obtainMessage();
							msg.obj = "1";
							handler_new.sendMessage(msg);
						}
					}
				
					if(result.msgList.size()<20) { allowCall_new = false; }
					else { allowCall_new = true; }
					
					Controller.refreshCounters(_activity);
				}
				else if(_viewMode==1)
				{
					profilesListFull_inbox.msgList.addAll(result.msgList);
					adapter_inbox.setItems(profilesListFull_inbox.msgList);
					adapter_inbox.notifyDataSetChanged();
					
					if(handler_inbox!=null)
					{
						if(profilesListFull_inbox.msgList.size()==0)
						{
							Message msg = handler_inbox.obtainMessage();
							msg.obj = "0";
							handler_inbox.sendMessage(msg);
						}
						else
						{
							Message msg = handler_inbox.obtainMessage();
							msg.obj = "1";
							handler_inbox.sendMessage(msg);
						}
					}
				
					if(result.msgList.size()<20) { allowCall_inbox = false; }
					else { allowCall_inbox = true; }
				}
				else if(_viewMode==2)
				{
					profilesListFull_sent.msgList.addAll(result.msgList);
					adapter_sent.setItems(profilesListFull_sent.msgList);
					adapter_sent.notifyDataSetChanged();
					
					if(handler_sent!=null)
					{
						if(profilesListFull_sent.msgList.size()==0)
						{
							Message msg = handler_sent.obtainMessage();
							msg.obj = "0";
							handler_sent.sendMessage(msg);
						}
						else
						{
							Message msg = handler_sent.obtainMessage();
							msg.obj = "1";
							handler_sent.sendMessage(msg);
						}
					}
				
					if(result.msgList.size()<20) { allowCall_sent = false; }
					else { allowCall_sent = true; }
				}
				else if(_viewMode==16)
				{
					profilesListFull_trash.msgList.addAll(result.msgList);
					adapter_trash.setItems(profilesListFull_trash.msgList);
					adapter_trash.notifyDataSetChanged();
					
					if(handler_trash!=null)
					{
						if(profilesListFull_trash.msgList.size()==0)
						{
							Message msg = handler_trash.obtainMessage();
							msg.obj = "0";
							handler_trash.sendMessage(msg);
						}
						else
						{
							Message msg = handler_trash.obtainMessage();
							msg.obj = "1";
							handler_trash.sendMessage(msg);
						}
					}
				
					if(result.msgList.size()<20) { allowCall_trash = false; }
					else { allowCall_trash = true; }
				}
			}	
			fragLoading.Close();
		}
		
	}
	
	OnScrollListener myOnScrollListener_new = new OnScrollListener() 
	{
		@Override
		public void onScrollStateChanged(AbsListView view, int scrollState) 
		{
			if(scrollState==1)
			{
				adapter_new.setAnim(true);
			}
			else if(scrollState==0)
			{
				adapter_new.setAnim(false);
			}
		}

		@Override
		public void onScroll(AbsListView view, int firstVisibleItem,int visibleItemCount, int totalItemCount) {
			if(firstVisibleItem+visibleItemCount==totalItemCount && visibleItemCount!=0 && totalItemCount!=0 && allowCall_new)
			{
				allowCall_new = false;
				Log.v("APP","----Executing get profiles from on scroll-----");				
				new getNewMessagesTask(8).execute();
			}
		}
	};
	
	OnScrollListener myOnScrollListener_inbox = new OnScrollListener() 
	{
		@Override
		public void onScrollStateChanged(AbsListView view, int scrollState) 
		{
			if(scrollState==1)
			{
				adapter_inbox.setAnim(true);
			}
			else if(scrollState==0)
			{
				adapter_inbox.setAnim(false);
			}
		}

		@Override
		public void onScroll(AbsListView view, int firstVisibleItem,int visibleItemCount, int totalItemCount) {
			if(firstVisibleItem+visibleItemCount==totalItemCount && visibleItemCount!=0 && totalItemCount!=0 && allowCall_inbox)
			{
				allowCall_inbox = false;
				Log.v("APP","----Executing get profiles from on scroll-----");				
				new getNewMessagesTask(1).execute();
			}
		}
	};
	
	OnScrollListener myOnScrollListener_sent = new OnScrollListener() 
	{
		@Override
		public void onScrollStateChanged(AbsListView view, int scrollState) 
		{
			if(scrollState==1)
			{
				adapter_sent.setAnim(true);
			}
			else if(scrollState==0)
			{
				adapter_sent.setAnim(false);
			}
		}

		@Override
		public void onScroll(AbsListView view, int firstVisibleItem,int visibleItemCount, int totalItemCount) {
			if(firstVisibleItem+visibleItemCount==totalItemCount && visibleItemCount!=0 && totalItemCount!=0 && allowCall_sent)
			{
				allowCall_sent = false;
				Log.v("APP","----Executing get profiles from on scroll-----");				
				new getNewMessagesTask(2).execute();
			}
		}
	};
	
	OnScrollListener myOnScrollListener_trash = new OnScrollListener() 
	{
		@Override
		public void onScrollStateChanged(AbsListView view, int scrollState) 
		{
			if(scrollState==1)
			{
				adapter_trash.setAnim(true);
			}
			else if(scrollState==0)
			{
				adapter_trash.setAnim(false);
			}
		}

		@Override
		public void onScroll(AbsListView view, int firstVisibleItem,int visibleItemCount, int totalItemCount) {
			if(firstVisibleItem+visibleItemCount==totalItemCount && visibleItemCount!=0 && totalItemCount!=0 && allowCall_trash)
			{
				allowCall_trash = false;
				Log.v("APP","----Executing get profiles from on scroll-----");				
				new getNewMessagesTask(16).execute();
			}
		}
	};
	
	
	
	
	
	
	
	
	
	
	
	
	
	OnItemClickListener myOnItemClickListener_new = new OnItemClickListener() 
	{
		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
			
			MessageInfo minfo = profilesListFull_new.msgList.get(position);
			
			Fragment fr;
			fr = new FragMessageView(minfo,null,0,null);
			FragmentManager fm = _activity.getFragmentManager();
			
	        FragmentTransaction fragmentTransaction = fm.beginTransaction();
	        fragmentTransaction.setCustomAnimations(R.anim.left_slidein_list_animator, R.anim.left_slideout_list_animator,R.anim.right_slidein_list_animator, R.anim.right_slideout_list_animator);
	        fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
	        fragmentTransaction.addToBackStack(null);
	        fragmentTransaction.commit();
	        
	        
		}
	};
	
	
	OnItemClickListener myOnItemClickListener_inbox = new OnItemClickListener() 
	{
		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
			
			MessageInfo minfo = profilesListFull_inbox.msgList.get(position);
			
			Fragment fr;
			fr = new FragMessageView(minfo,null,0,null);
			FragmentManager fm = _activity.getFragmentManager();
			
	        FragmentTransaction fragmentTransaction = fm.beginTransaction();
	        fragmentTransaction.setCustomAnimations(R.anim.left_slidein_list_animator, R.anim.left_slideout_list_animator,R.anim.right_slidein_list_animator, R.anim.right_slideout_list_animator);
	        fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
	        fragmentTransaction.addToBackStack(null);
	        fragmentTransaction.commit();
			
		}
	};
	
	OnItemClickListener myOnItemClickListener_sent = new OnItemClickListener() 
	{
		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
			
			MessageInfo minfo = profilesListFull_sent.msgList.get(position);
			
			Fragment fr;
			fr = new FragMessageView(minfo,null,0,null);
			FragmentManager fm = _activity.getFragmentManager();
			
	        FragmentTransaction fragmentTransaction = fm.beginTransaction();
	        fragmentTransaction.setCustomAnimations(R.anim.left_slidein_list_animator, R.anim.left_slideout_list_animator,R.anim.right_slidein_list_animator, R.anim.right_slideout_list_animator);
	        fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
	        fragmentTransaction.addToBackStack(null);
	        fragmentTransaction.commit();
			
		}
	};
	
	
	OnItemClickListener myOnItemClickListener_trash = new OnItemClickListener() 
	{
		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
			
			MessageInfo minfo = profilesListFull_trash.msgList.get(position);
			
			Fragment fr;
			fr = new FragMessageView(minfo,null,0,null);
			FragmentManager fm = _activity.getFragmentManager();
			
	        FragmentTransaction fragmentTransaction = fm.beginTransaction();
	        fragmentTransaction.setCustomAnimations(R.anim.left_slidein_list_animator, R.anim.left_slideout_list_animator,R.anim.right_slidein_list_animator, R.anim.right_slideout_list_animator);
	        fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
	        fragmentTransaction.addToBackStack(null);
	        fragmentTransaction.commit();
			
		}
	};
	
	
	
	
	
	

}

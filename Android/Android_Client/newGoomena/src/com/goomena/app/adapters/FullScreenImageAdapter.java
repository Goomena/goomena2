package com.goomena.app.adapters;

import java.util.ArrayList;

import com.goomena.app.R;
import com.goomena.app.TouchImageView;
import com.goomena.app.models.UserProfile;
import com.goomena.app.tools.Utils;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class FullScreenImageAdapter extends PagerAdapter
{
	private Activity _activity;
	private ArrayList<String> _imagePaths;
	UserProfile _userprofile;
	private LayoutInflater inflater;
	Animation animRotate;
	ImageView spinner;
	boolean fullscreen = false;
	TouchImageView imgDisplay = null;
	
	@SuppressWarnings("unused")
	private final static String TAG_FRAGMENT = "TAG_FRAGMENT";

	// constructor
	public FullScreenImageAdapter(Activity activity, ArrayList<String> imagePaths,UserProfile userprofile)
	{
		this._activity = activity;
		this._imagePaths = imagePaths;
		this._userprofile = userprofile;
		animRotate = AnimationUtils.loadAnimation(activity, R.anim.d_wait_bg_logo);
	}

	@Override
	public int getCount() 
	{
		return this._imagePaths.size();
	}

	@SuppressLint("NewApi") 
	public void setPrimaryItem(ViewGroup container, int position, Object object) 
	{	
		if(imgDisplay!=null)
		{
			if(fullscreen==true)
        	{
        		imgDisplay.animate().scaleYBy(1.0f).scaleXBy(1.0f).scaleY(1.6f).scaleX(1.6f);
        	}
        	else
        	{
        		imgDisplay.animate().scaleYBy(1.6f).scaleXBy(1.6f).scaleY(1.0f).scaleX(1.0f);
        	}
		}
	}
	@Override
    public boolean isViewFromObject(View view, Object object) 
	{
        return view == ((RelativeLayout) object);
    }
	
	@SuppressLint("NewApi") 
	@Override
    public Object instantiateItem(ViewGroup container, int position) 
	{
        final TouchImageView imgDisplay;
        final View viewLayout;
        
        
        
        RelativeLayout prIm_relativeHome;
        ImageView prIm_imagePrivate;
        final TextView prIm_textPrivate;
        
        //Log.v("APP", "srfhtsh sh "+_userprofile.UserName);
        
        if(_imagePaths.get(position).replace("/d150", "").contains("level"))
        {
        	inflater = (LayoutInflater) _activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        	viewLayout = inflater.inflate(R.layout.layout_fullscreen_privateimage, container, false);
 
        	prIm_relativeHome = (RelativeLayout) viewLayout.findViewById(R.id.prIm_relativeHome);
        	prIm_imagePrivate = (ImageView) viewLayout.findViewById(R.id.prIm_imagePrivate);
        	prIm_textPrivate = (TextView) viewLayout.findViewById(R.id.prIm_textPrivate);
        	
        	if(_imagePaths.get(position).replace("/d150", "").contains("womenlevel"))
            {
        		prIm_relativeHome.setBackgroundResource(R.drawable.pr_woman);
            }
        	
        	if(_imagePaths.get(position).replace("/d150", "").contains("level1"))
        	{
        		prIm_textPrivate.setBackgroundColor(Color.parseColor("#cc323b31"));
        		prIm_textPrivate.setTag("1");
        	}
        	if(_imagePaths.get(position).replace("/d150", "").contains("level2"))
        	{
        		prIm_textPrivate.setBackgroundColor(Color.parseColor("#cc6b9d08"));
        		prIm_textPrivate.setTag("2");
        	}
        	if(_imagePaths.get(position).replace("/d150", "").contains("level3"))
        	{
        		prIm_textPrivate.setBackgroundColor(Color.parseColor("#cceca900"));
        		prIm_textPrivate.setTag("3");
        	}
        	if(_imagePaths.get(position).replace("/d150", "").contains("level4"))
        	{
        		prIm_textPrivate.setBackgroundColor(Color.parseColor("#cc4a3d8c"));
        		prIm_textPrivate.setTag("4");
        	}
        	if(_imagePaths.get(position).replace("/d150", "").contains("level5"))
        	{
        		prIm_textPrivate.setBackgroundColor(Color.parseColor("#ccb76fb9"));
        		prIm_textPrivate.setTag("5");
        	}
        	if(_imagePaths.get(position).replace("/d150", "").contains("level6"))
        	{
        		prIm_textPrivate.setBackgroundColor(Color.parseColor("#ccd0314a"));
        		prIm_textPrivate.setTag("6");
        	}
        	if(_imagePaths.get(position).replace("/d150", "").contains("level7"))
        	{
        		prIm_textPrivate.setBackgroundColor(Color.parseColor("#ccd10066"));
        		prIm_textPrivate.setTag("7");
        	}
        	if(_imagePaths.get(position).replace("/d150", "").contains("level8"))
        	{
        		prIm_textPrivate.setBackgroundColor(Color.parseColor("#ccd60054"));
        		prIm_textPrivate.setTag("8");
        	}
        	if(_imagePaths.get(position).replace("/d150", "").contains("level9"))
        	{
        		prIm_textPrivate.setBackgroundColor(Color.parseColor("#cccb232f"));
        		prIm_textPrivate.setTag("9");
        	}
        	if(_imagePaths.get(position).replace("/d150", "").contains("level10"))
        	{
        		prIm_textPrivate.setBackgroundColor(Color.parseColor("#ccd00404"));
        		prIm_textPrivate.setTag("10");
        	}
        	
        	prIm_textPrivate.setText(_activity.getResources().getString(R.string.in_order_to_proceed)+" "+_userprofile.UserName+" "+_activity.getResources().getString(R.string.now));
        	
        	ImageLoader.getInstance().displayImage(_imagePaths.get(position).replace("/d150", "") , prIm_imagePrivate, Utils.getImageOptions1());
        	
        	
        	prIm_textPrivate.setOnClickListener(new OnClickListener() {
				
				
				@SuppressWarnings("static-access")
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent returnIntent = new Intent();
					returnIntent.putExtra("result",_userprofile);
					returnIntent.putExtra("resultlevel",prIm_textPrivate.getTag().toString());
					_activity.setResult(_activity.RESULT_OK,returnIntent);
					_activity.finish();
				}
			});
        
        }
        else
        {
        	inflater = (LayoutInflater) _activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            viewLayout = inflater.inflate(R.layout.layout_fullscreen_image, container, false);
     
            
            imgDisplay = (TouchImageView) viewLayout.findViewById(R.id.imgDisplay);
            imgDisplay.setMaxZoom(100);
          
            final ImageView spinner = (ImageView) viewLayout.findViewById(R.id.loading_rotate);
            final ImageView spinnerFr = (ImageView) viewLayout.findViewById(R.id.loading_rotateFR);
            if(Utils.getSharedPreferences(_activity).getString("genderid","").contains("2"))
    		{
            	spinnerFr.setImageResource(R.drawable.woman_loading);
    		}
            
            
            ImageLoader.getInstance().displayImage(_imagePaths.get(position).replace("/d150", "") , imgDisplay, Utils.getImageOptions1(),new SimpleImageLoadingListener() 
            {
    			public void onLoadingStarted(String imageUri, View view) 
    			{
    				spinner.setVisibility(View.VISIBLE);
    				spinnerFr.setVisibility(View.VISIBLE);
    				spinner.startAnimation(animRotate);
    			}

    			@SuppressWarnings("unused")
				public void onLoadingFailed(String imageUri, View view) 
    			{
    				spinner.setVisibility(View.GONE);
    				spinnerFr.setVisibility(View.GONE);
    			}

    			public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage)
    			{
    				spinner.setVisibility(View.GONE);
    				spinnerFr.setVisibility(View.GONE);
    				spinner.clearAnimation();
    			}
    		});
        }
        
  
        ((ViewPager) container).addView(viewLayout);
 
        return viewLayout;
	}
	
	@Override
    public void destroyItem(ViewGroup container, int position, Object object) 
	{
        ((ViewPager) container).removeView((RelativeLayout) object);
    }
}


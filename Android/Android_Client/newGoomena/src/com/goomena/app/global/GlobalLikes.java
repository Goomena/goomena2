package com.goomena.app.global;

import com.goomena.app.data.ServiceClient;
import com.goomena.app.models.OfferList;
import com.goomena.app.tools.Log;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Message;


public class GlobalLikes 
{
	public static String likescount = "0";

	OfferList offerList;
	
	
	public static void getLikesCount()
	{
		new getLikesCountTask().execute(Global.MyId);
	
	}
}


class getLikesCountTask extends AsyncTask<String,Void,String>
{
	@Override
	protected String doInBackground(String... params) 
	{
		
		String result = ServiceClient.getNumberLikes(params[0]);
			
		return result;
	}
	
	@Override
	protected void onPostExecute(String result)
	{
		if(!result.equals(""))
		{
			GlobalLikes.likescount = result;
			if(Global.handler1!=null && Global.handlerSumNotifications!=null)
  	  		{
				Message msg = Global.handler1.obtainMessage();
				Global.handler1.sendMessage(msg);
			
				Message msg1 = Global.handlerSumNotifications.obtainMessage();
				Global.handlerSumNotifications.sendMessage(msg1);
  	  		}
		}
	}
}
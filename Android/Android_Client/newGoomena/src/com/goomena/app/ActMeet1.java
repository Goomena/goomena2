package com.goomena.app;

import java.util.ArrayList;

import com.goomena.app.data.ServiceClient;
import com.goomena.app.global.Global;
import com.goomena.app.models.UserDetails;
import com.goomena.app.models.UserProfile;
import com.goomena.app.tools.Utils;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ActMeet1 extends PagerAdapter
{
	private Activity _activity;
	private ArrayList<UserProfile> _userProfiles;
	private LayoutInflater inflater;
	Animation animRotate,animFadein;
	
	public ActMeet1(Activity activity, ArrayList<UserProfile> userProfiles)
	{
		this._activity = activity;
		//this._imagePaths = imagePaths;
		this._userProfiles = userProfiles;
		animRotate = AnimationUtils.loadAnimation(_activity, R.anim.d_wait_bg_logo);
		animFadein = AnimationUtils.loadAnimation(activity, R.anim.fade_in);
	}
	
	@Override
	public int getCount() 
	{		
		//Log.v("APP","aaaaaaaaaa size "+_userProfiles.size());
		return _userProfiles.size();
	}
	
	@Override
    public boolean isViewFromObject(View view, Object object) 
	{
        return view == ((LinearLayout) object);
    }
	
	public void setPrimaryItem(ViewGroup container, int position, Object object) 
	{	
		
	}
	
	@SuppressLint("NewApi") 
	@Override
    public Object instantiateItem(ViewGroup container, final int position) 
	{
		RelativeLayout relativeViewProfile;
		ImageView Profile_imageOval_yellow;
		TextView Profile_textOnline,Profile_textNumPhotos;
        ImageView imgDisplay;
        
        ImageView p_imageView2,p_imageView3,p_imageView4,p_imageView5;
        
        TextView Profile_textName,Profile_textAge,Profile_textLocation;
		
		inflater = (LayoutInflater) _activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View viewLayout = inflater.inflate(R.layout.act_meet1, container, false);
        
        
        relativeViewProfile = (RelativeLayout) viewLayout.findViewById(R.id.relativeViewProfile);
        Profile_imageOval_yellow = (ImageView) viewLayout.findViewById(R.id.Profile_imageOval_yellow);
        Profile_textOnline = (TextView) viewLayout.findViewById(R.id.Profile_textOnline);
        //Profile_textNumPhotos = (TextView) viewLayout.findViewById(R.id.Profile_textNumPhotos);
        imgDisplay = (ImageView) viewLayout.findViewById(R.id.Profile_imageid);
        
        p_imageView2 = (ImageView) viewLayout.findViewById(R.id.p_imageView2);
        p_imageView3 = (ImageView) viewLayout.findViewById(R.id.p_imageView3);
        p_imageView4 = (ImageView) viewLayout.findViewById(R.id.p_imageView4);
        p_imageView5 = (ImageView) viewLayout.findViewById(R.id.p_imageView5);
        
        Profile_textName = (TextView) viewLayout.findViewById(R.id.Profile_textName);
        Profile_textAge = (TextView) viewLayout.findViewById(R.id.Profile_textAge);
        Profile_textLocation = (TextView) viewLayout.findViewById(R.id.Profile_textLocation);
        
        if(_userProfiles.get(position).GenderId.contains("1"))
        {
        	p_imageView3.setVisibility(View.GONE);
        	p_imageView4.setBackgroundResource(R.drawable.p_men_travel);
        	p_imageView5.setBackgroundResource(R.drawable.p_men_single);
        }
        else
        {
        	p_imageView3.setBackgroundResource(R.drawable.p_woman_work);
        	p_imageView4.setBackgroundResource(R.drawable.p_woman_travel);
        	p_imageView5.setBackgroundResource(R.drawable.p_woman_single);
        }
        
        if(_userProfiles.get(position).IsOnline == true)
        {
        	Profile_textOnline.setText("online");
			Profile_imageOval_yellow.setBackgroundResource(R.drawable.oval_yellow);
			Profile_imageOval_yellow.startAnimation(animFadein);
        }
        else if(_userProfiles.get(position).IsOnline == false)
        {
        	Profile_textOnline.setText("offline");
			Profile_imageOval_yellow.setBackgroundResource(R.drawable.oval_red);
			Profile_imageOval_yellow.clearAnimation();
        }
        /*
        if(_userProfiles.get(position).PhotosCount!=0)
        {
        	Profile_textNumPhotos.setText(""+_userProfiles.get(position).PhotosCount+" "+_activity.getResources().getString(R.string.photos));
        }
        */
        
        final ImageView spinner = (ImageView) viewLayout.findViewById(R.id.loading_rotate);
        final ImageView spinnerFr = (ImageView) viewLayout.findViewById(R.id.loading_rotateFR);
        if(_userProfiles.get(position).GenderId.contains("1"))
        {
        	spinnerFr.setImageResource(R.drawable.woman_loading);
        }
        _userProfiles.get(position).ImageUrl = _userProfiles.get(position).ImageUrl.replace("/d150", "");
        Utils.getImageLoader(_activity);
		ImageLoader.getInstance().displayImage(_userProfiles.get(position).ImageUrl, imgDisplay,Utils.getImageOptions(),new SimpleImageLoadingListener() {
			public void onLoadingStarted(String imageUri, View view) {
				spinner.setVisibility(View.VISIBLE);
				spinnerFr.setVisibility(View.VISIBLE);
				spinner.startAnimation(animRotate);
			}
			@SuppressWarnings("unused")
			public void onLoadingFailed(String imageUri, View view) {
				spinner.setVisibility(View.GONE);
				spinnerFr.setVisibility(View.GONE);
			}
			public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage){
				spinner.setVisibility(View.GONE);
				spinnerFr.setVisibility(View.GONE);
				spinner.clearAnimation();
			}
		});
        
        Profile_textName.setText(""+_userProfiles.get(position).UserName);
        Profile_textAge.setText(_activity.getResources().getText(R.string.age)+" "+_userProfiles.get(position).UserAge+" "+_activity.getResources().getText(R.string.years));
        Profile_textLocation.setText(_activity.getResources().getText(R.string.from)+" "+_userProfiles.get(position).UserCity+", "+_userProfiles.get(position).UserRegion+", "+_userProfiles.get(position).UserCountry);

        new getDetailsTask(_userProfiles.get(position),p_imageView2).execute();
		
        
        relativeViewProfile.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Intent intent = new Intent();
				intent.setClass(_activity, ActMeet3.class);
				intent.putExtra("Mimage",_userProfiles.get(position).ImageUrl);
				intent.putExtra("Musername",_userProfiles.get(position).UserName);
				intent.putExtra("Mage",_userProfiles.get(position).UserAge);
				intent.putExtra("Mlocation",_userProfiles.get(position).UserCity+", "+_userProfiles.get(position).UserRegion+", "+_userProfiles.get(position).UserCountry);
				_activity.startActivity(intent);
				_activity.overridePendingTransition(R.anim.right_in, R.anim.left_out);
				
			}
		});
        
        ((ViewPager) container).addView(viewLayout);
        return viewLayout;
	}
	
	@Override
    public void destroyItem(ViewGroup container, int position, Object object) 
	{
        ((ViewPager) container).removeView((LinearLayout) object);
    }
	
	
	
	class getDetailsTask extends AsyncTask<Void, Integer, UserDetails>
	{
		UserProfile _userprofile;
		ImageView _p_imageView2;
		
		public getDetailsTask(UserProfile userprofile,ImageView p_imageView2)
		{
			this._userprofile = userprofile;
			this._p_imageView2 = p_imageView2;
		}
		
		@Override
		protected void onPreExecute(){}

		@Override
		protected UserDetails doInBackground(Void... params) 
		{		// Global.Lang
			UserDetails details;
			details = ServiceClient.getUserDetails(_userprofile.ProfileId,"US");			
			return details;
		}
		@SuppressLint("NewApi") 
		@Override
		protected void onPostExecute(UserDetails details)
		{
			if(details!=null)
			{
				
					if(details.zodiac.contains("Gemini"))
					{
						_p_imageView2.setBackgroundResource(R.drawable.p_gemini);
					}
					else if(details.zodiac.contains("Aries"))
					{
						_p_imageView2.setBackgroundResource(R.drawable.p_aries);
					}
					else if(details.zodiac.contains("Aquarius"))
					{
						_p_imageView2.setBackgroundResource(R.drawable.p_aruarius);
					}
					else if(details.zodiac.contains("Cancer"))
					{
						_p_imageView2.setBackgroundResource(R.drawable.p_cancer);
					}
					else if(details.zodiac.contains("Capricorn"))
					{
						_p_imageView2.setBackgroundResource(R.drawable.p_capricorn);
					}
					else if(details.zodiac.contains("Leo"))
					{
						_p_imageView2.setBackgroundResource(R.drawable.p_leo);
					}
					else if(details.zodiac.contains("Libra"))
					{
						_p_imageView2.setBackgroundResource(R.drawable.p_libra);
					}
					else if(details.zodiac.contains("Pisces"))
					{
						_p_imageView2.setBackgroundResource(R.drawable.p_pisces);
					}
					else if(details.zodiac.contains("Sagittarius"))
					{
						_p_imageView2.setBackgroundResource(R.drawable.p_sagittarius);
					}
					else if(details.zodiac.contains("Scorpio"))
					{
						_p_imageView2.setBackgroundResource(R.drawable.p_scorpio);
					}
					else if(details.zodiac.contains("Taurus"))
					{
						_p_imageView2.setBackgroundResource(R.drawable.p_taurus);
					}
					else if(details.zodiac.contains("Virgo"))
					{
						_p_imageView2.setBackgroundResource(R.drawable.p_virgo);
					}
					else
					{
						_p_imageView2.setVisibility(View.GONE);
					}
			}	
		}
	}
	

}

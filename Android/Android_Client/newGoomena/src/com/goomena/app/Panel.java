package com.goomena.app;

import com.goomena.app.adapters.PanelAdapter;
import com.goomena.app.tools.Log;
import com.goomena.app.tools.Utils;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnAttachStateChangeListener;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SlidingDrawer;
import android.widget.SlidingDrawer.OnDrawerCloseListener;
import android.widget.SlidingDrawer.OnDrawerOpenListener;


public class Panel 
{
	Activity _activity;
	private SlidingDrawer slidingDrawer;
	RelativeLayout panel_relativeLayout;
	View panel_view;
	LinearLayout handle;
	ImageView _imagetriangle3;
	private ViewPager viewPager;
	private static PanelAdapter adapter;
	
	
	RelativeLayout panel_buddy_relativeLayoutLikes,
					panel_buddy_relativeLayoutMessages,panel_buddy_relativeLayoutOffers,
					panel_buddy_relativeLayoutDates,panel_buddy_relativeLayoutCredits;
	
	ImageView panel_buddy_imageLikes,panel_buddy_imageMessages,
				panel_buddy_imageOffers,panel_buddy_imageDates,
				panel_buddy_imageCredits;
	
	View panel_buddy_viewLikes,panel_buddy_viewMessages,panel_buddy_viewOffers,panel_buddy_viewDates,panel_buddy_viewCredits;
	
	@SuppressLint("NewApi") 
	@SuppressWarnings("deprecation")
	public Panel(final Activity activity, ImageView imagetriangle3)
	{
		this._activity = activity;
		this._imagetriangle3 = imagetriangle3;
		
		slidingDrawer = (SlidingDrawer) activity.findViewById(R.id.slidingDrawer);
		panel_relativeLayout = (RelativeLayout) activity.findViewById(R.id.panel_relativeLayout);
		panel_view = (View) activity.findViewById(R.id.panel_view);
		handle = (LinearLayout) activity.findViewById(R.id.handle);
		viewPager = (ViewPager) activity.findViewById(R.id.view_pager_panel);
		viewPager.setPageMargin(50);
		
		//panel_buddy_relativeLayoutNotification = ( RelativeLayout ) activity.findViewById(R.id.panel_buddy_relativeLayoutNotification);
		panel_buddy_relativeLayoutLikes = ( RelativeLayout ) activity.findViewById(R.id.panel_buddy_relativeLayoutLikes);
		panel_buddy_relativeLayoutMessages = ( RelativeLayout ) activity.findViewById(R.id.panel_buddy_relativeLayoutMessages);
		panel_buddy_relativeLayoutOffers = ( RelativeLayout ) activity.findViewById(R.id.panel_buddy_relativeLayoutOffers);
		panel_buddy_relativeLayoutDates = ( RelativeLayout ) activity.findViewById(R.id.panel_buddy_relativeLayoutDates);
		panel_buddy_relativeLayoutCredits  = ( RelativeLayout ) activity.findViewById(R.id.panel_buddy_relativeLayoutCredits);
		
		
		panel_buddy_imageLikes = (ImageView) activity.findViewById(R.id.panel_buddy_imageLikes);
		panel_buddy_imageMessages = (ImageView) activity.findViewById(R.id.panel_buddy_imageMessages);
		panel_buddy_imageOffers = (ImageView) activity.findViewById(R.id.panel_buddy_imageOffers);
		panel_buddy_imageDates = (ImageView) activity.findViewById(R.id.panel_buddy_imageDates);
		panel_buddy_imageCredits = (ImageView) activity.findViewById(R.id.panel_buddy_imageCredits);
		
		panel_buddy_viewLikes = (View) activity.findViewById(R.id.panel_buddy_viewLikes);
		panel_buddy_viewMessages = (View) activity.findViewById(R.id.panel_buddy_viewMessages);
		panel_buddy_viewOffers = (View) activity.findViewById(R.id.panel_buddy_viewOffers);
		panel_buddy_viewDates = (View) activity.findViewById(R.id.panel_buddy_viewDates);
		panel_buddy_viewCredits = (View) activity.findViewById(R.id.panel_buddy_viewCredits);
		
		if(Utils.getSharedPreferences(activity).getString("genderid","").contains("2"))
		{
			panel_buddy_relativeLayoutCredits.setVisibility(View.GONE);
			panel_buddy_viewCredits.setVisibility(View.GONE);
		}
		
		panel_buddy_relativeLayoutLikes.setOnClickListener(clickListener);
		panel_buddy_relativeLayoutMessages.setOnClickListener(clickListener);
		panel_buddy_relativeLayoutOffers.setOnClickListener(clickListener);
		panel_buddy_relativeLayoutDates.setOnClickListener(clickListener);
		panel_buddy_relativeLayoutCredits.setOnClickListener(clickListener);
		
		slidingDrawer.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
	            switch (event.getAction()) {

	            case MotionEvent.ACTION_DOWN:
	                panel_view.setBackgroundColor(activity.getResources().getColor(R.color.colorPanelLine));
	                break;

	            case MotionEvent.ACTION_MOVE:
	                break;

	            case MotionEvent.ACTION_UP:
	                panel_view.setBackgroundColor(activity.getResources().getColor(R.color.white));
	                break;               
	            }               
	            return false;
				
			}
		});
		
		viewPager.setOnPageChangeListener(pageChangeListener);
		
		
		slidingDrawer.setOnDrawerOpenListener(new OnDrawerOpenListener() {
			@Override
			public void onDrawerOpened() {
				// TODO Auto-generated method stub
				panel_relativeLayout.setVisibility(View.VISIBLE);
				_imagetriangle3.setVisibility(View.VISIBLE);
				panel_view.setBackgroundColor(activity.getResources().getColor(R.color.white));
			}
		});
		
		slidingDrawer.setOnDrawerCloseListener(new OnDrawerCloseListener() {
			@Override
			public void onDrawerClosed() {
				// TODO Auto-generated method stub
				panel_relativeLayout.setVisibility(View.GONE);
				_imagetriangle3.setVisibility(View.GONE);
			}
		});
		
		
		
		
		adapter = new PanelAdapter(activity,this);
		viewPager.setAdapter(adapter);
		
		
		
				
		setTab(0);
		viewPager.setCurrentItem(0);
	}
	public static void refresh()
	{
		adapter.refresh();
	}
	
	@SuppressWarnings("deprecation")
	public void Show()
	{
		slidingDrawer.animateOpen();
	}
	@SuppressWarnings("deprecation")
	public void Close()
	{
		slidingDrawer.animateToggle();
	}
	@SuppressWarnings("deprecation")
	public boolean IsOpen()
	{
		if(slidingDrawer.isOpened())
		{
			return true;
		}
		else
		{
			return false;
		}
	}

//	Handler handler_panel = new Handler()
//	{
//		public void handleMessage(Message msg) 
//  		{	
//			///setTab(Integer.parseInt(msg.obj.toString()));
//  		}
//	};
	
	OnClickListener clickListener = new OnClickListener() 
	{
		public void onClick(View v) 
		{
			if(v == panel_buddy_relativeLayoutLikes)
			{
				setTab(0);
				viewPager.setCurrentItem(0);
			}
			/*
			else if(v == panel_buddy_relativeLayoutMessages)
			{
				setTab(1);
				viewPager.setCurrentItem(1);
			}
			*/
			else if(v == panel_buddy_relativeLayoutOffers)
			{
				setTab(1);
				viewPager.setCurrentItem(1);
			}
			/*
			else if(v == panel_buddy_relativeLayoutDates)
			{
				setTab(3);
				viewPager.setCurrentItem(3);
			}
			*/
			else if(v == panel_buddy_relativeLayoutCredits)
			{
				setTab(2);
				viewPager.setCurrentItem(2);
			}
		}
	};
	
	OnPageChangeListener pageChangeListener = new OnPageChangeListener()
	{
		@Override
		public void onPageScrollStateChanged(int arg0) {}
		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2) {}
		@Override
		public void onPageSelected(int arg0) { setTab(arg0); }
	};
	
	
	@SuppressWarnings("deprecation")
	private void setTab(int select)
	{
		if(select==0)
		{
			panel_buddy_imageLikes.setBackgroundResource(R.drawable.panel_like_hover);
			//panel_buddy_imageMessages.setBackgroundResource(R.drawable.panel_message_icon);
			panel_buddy_imageOffers.setBackgroundResource(R.drawable.panel_bids_icon);
			//panel_buddy_imageDates.setBackgroundResource(R.drawable.panel_date_icon);
			panel_buddy_imageCredits.setBackgroundResource(R.drawable.panel_credit_icon);
			
			panel_buddy_viewLikes.setBackgroundColor(_activity.getResources().getColor(R.color.colorPanelLine));
			//panel_buddy_viewMessages.setBackgroundColor(_activity.getResources().getColor(R.color.white));
			panel_buddy_viewOffers.setBackgroundColor(_activity.getResources().getColor(R.color.white));
			//panel_buddy_viewDates.setBackgroundColor(_activity.getResources().getColor(R.color.white));
			panel_buddy_viewCredits.setBackgroundColor(_activity.getResources().getColor(R.color.white));
		}
		/*
		else if(select==1)
		{
			
			panel_buddy_imageLikes.setBackgroundResource(R.drawable.panel_like_icon);
			panel_buddy_imageMessages.setBackgroundResource(R.drawable.panel_message_hover);
			panel_buddy_imageOffers.setBackgroundResource(R.drawable.panel_bids_icon);
			panel_buddy_imageDates.setBackgroundResource(R.drawable.panel_date_icon);
			panel_buddy_imageCredits.setBackgroundResource(R.drawable.panel_credit_icon);
			
			panel_buddy_viewLikes.setBackgroundColor(_activity.getResources().getColor(R.color.white));
			panel_buddy_viewMessages.setBackgroundColor(_activity.getResources().getColor(R.color.colorPanelLine));
			panel_buddy_viewOffers.setBackgroundColor(_activity.getResources().getColor(R.color.white));
			panel_buddy_viewDates.setBackgroundColor(_activity.getResources().getColor(R.color.white));
			panel_buddy_viewCredits.setBackgroundColor(_activity.getResources().getColor(R.color.white));
			
		}
		*/
		else if(select==1)
		{
			panel_buddy_imageLikes.setBackgroundResource(R.drawable.panel_like_icon);
			//panel_buddy_imageMessages.setBackgroundResource(R.drawable.panel_message_icon);
			panel_buddy_imageOffers.setBackgroundResource(R.drawable.panel_bids_hover);
			//panel_buddy_imageDates.setBackgroundResource(R.drawable.panel_date_icon);
			panel_buddy_imageCredits.setBackgroundResource(R.drawable.panel_credit_icon);
			
			panel_buddy_viewLikes.setBackgroundColor(_activity.getResources().getColor(R.color.white));
			//panel_buddy_viewMessages.setBackgroundColor(_activity.getResources().getColor(R.color.white));
			panel_buddy_viewOffers.setBackgroundColor(_activity.getResources().getColor(R.color.colorPanelLine));
			//panel_buddy_viewDates.setBackgroundColor(_activity.getResources().getColor(R.color.white));
			panel_buddy_viewCredits.setBackgroundColor(_activity.getResources().getColor(R.color.white));
		}
		/*
		else if(select==3)
		{
			panel_buddy_imageLikes.setBackgroundResource(R.drawable.panel_like_icon);
			panel_buddy_imageMessages.setBackgroundResource(R.drawable.panel_message_icon);
			panel_buddy_imageOffers.setBackgroundResource(R.drawable.panel_bids_icon);
			panel_buddy_imageDates.setBackgroundResource(R.drawable.panel_date_hover);
			panel_buddy_imageCredits.setBackgroundResource(R.drawable.panel_credit_icon);
			
			panel_buddy_viewLikes.setBackgroundColor(_activity.getResources().getColor(R.color.white));
			panel_buddy_viewMessages.setBackgroundColor(_activity.getResources().getColor(R.color.white));
			panel_buddy_viewOffers.setBackgroundColor(_activity.getResources().getColor(R.color.white));
			panel_buddy_viewDates.setBackgroundColor(_activity.getResources().getColor(R.color.colorPanelLine));
			panel_buddy_viewCredits.setBackgroundColor(_activity.getResources().getColor(R.color.white));
		}
		 */
		else if(select==2)
		{
			panel_buddy_imageLikes.setBackgroundResource(R.drawable.panel_like_icon);
			//panel_buddy_imageMessages.setBackgroundResource(R.drawable.panel_message_icon);
			panel_buddy_imageOffers.setBackgroundResource(R.drawable.panel_bids_icon);
			panel_buddy_imageDates.setBackgroundResource(R.drawable.panel_date_icon);
			panel_buddy_imageCredits.setBackgroundResource(R.drawable.panel_credit_hover);
			
			panel_buddy_viewLikes.setBackgroundColor(_activity.getResources().getColor(R.color.white));
			panel_buddy_viewMessages.setBackgroundColor(_activity.getResources().getColor(R.color.white));
			panel_buddy_viewOffers.setBackgroundColor(_activity.getResources().getColor(R.color.white));
			//panel_buddy_viewDates.setBackgroundColor(_activity.getResources().getColor(R.color.white));
			panel_buddy_viewCredits.setBackgroundColor(_activity.getResources().getColor(R.color.colorPanelLine));
		}
	}
}

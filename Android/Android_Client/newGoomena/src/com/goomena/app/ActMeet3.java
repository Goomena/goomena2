package com.goomena.app;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;

import org.json.JSONException;
import org.json.JSONObject;

import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.FacebookError;
import com.facebook.android.AsyncFacebookRunner.RequestListener;
import com.facebook.android.Facebook.DialogListener;
import com.goomena.app.data.ProfileManager;
import com.goomena.app.data.ServiceClient;
import com.goomena.app.models.UserProfile;
import com.goomena.app.tools.Log;
import com.goomena.app.tools.Utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

@SuppressWarnings("deprecation")
public class ActMeet3 extends Activity
{
	RelativeLayout relativeLayout1,relativeLayout2,relativeLayout3,relativeLayout4,relativeLayout5,relativeLayout6;
	Animation move_down1,move_down2,move_down3,move_down4,move_down5;
	
	ImageView fr_message_view_imeage_profile,fr_message_view_imeage_imageOval_yellow;
	TextView fr_message_view_text_username,fr_message_view_text_online,fr_message_view_text_age,fr_message_view_text_location;
	TextView full_profile;
	
	
	SharedPreferences settings;
	boolean rememberMe = true;
	boolean remember = false;
	Dialog Dwaiting;
	
	
	// Your Facebook APP ID
	private static String APP_ID = "1458937241031502"; // Replace with your App ID
	
	// Instance of Facebook Class
	private Facebook facebook = new Facebook(APP_ID);
	private AsyncFacebookRunner mAsyncRunner;
	String FILENAME = "AndroidSSO_data";
	private SharedPreferences mPrefs;
		
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_meet3);
		
		///-----+
		Dwaiting = new Dialog(ActMeet3.this, R.style.DialogSlideAnim);
		Dwaiting.setContentView(R.layout.dialog_waiting);
		Animation animFadein = AnimationUtils.loadAnimation(ActMeet3.this, R.anim.d_wait_fr_load);
		Animation animRotScal = AnimationUtils.loadAnimation(ActMeet3.this, R.anim.d_wait_bg_logo);
		final TextView dialog_waiting_textWaiting = (TextView) Dwaiting.findViewById(R.id.dialog_waiting_textWaiting);
		ImageView dialog_waiting_bgLogo = (ImageView) Dwaiting.findViewById(R.id.dialog_waiting_bgLogo);
		dialog_waiting_textWaiting.startAnimation(animFadein);
		dialog_waiting_bgLogo.startAnimation(animRotScal);
		/////----
		
		move_down1 = AnimationUtils.loadAnimation(this, R.anim.move_down1);
     	move_down2 = AnimationUtils.loadAnimation(this, R.anim.move_down2);
     	move_down3 = AnimationUtils.loadAnimation(this, R.anim.move_down3);
		
     	relativeLayout3 = (RelativeLayout) findViewById(R.id.relativeLayout3);
     	relativeLayout4 = (RelativeLayout) findViewById(R.id.relativeLayout4);
     	relativeLayout5 = (RelativeLayout) findViewById(R.id.relativeLayout5);
     	
     	fr_message_view_imeage_profile = (ImageView) findViewById(R.id.fr_message_view_imeage_profile);
     	fr_message_view_imeage_imageOval_yellow = (ImageView) findViewById(R.id.fr_message_view_imeage_imageOval_yellow);
     	
    	fr_message_view_text_username = (TextView) findViewById(R.id.fr_message_view_text_username);
    	fr_message_view_text_online = (TextView) findViewById(R.id.fr_message_view_text_online);
    	fr_message_view_text_age = (TextView) findViewById(R.id.fr_message_view_text_age);
    	fr_message_view_text_location = (TextView) findViewById(R.id.fr_message_view_text_location);
    	full_profile = (TextView) findViewById(R.id.full_profile);
		
     	
    	
    	
    	Utils.getImageLoader(ActMeet3.this).displayImage(getIntent().getStringExtra("Mimage"),fr_message_view_imeage_profile, Utils.getImageOptions());
    	fr_message_view_text_username.setText(getIntent().getStringExtra("Musername"));
    	fr_message_view_text_age.setText(" "+getIntent().getStringExtra("Mage")+" ");
    	fr_message_view_text_location.setText(" "+getIntent().getStringExtra("Mlocation"));
    	
    	full_profile.setText("To view Full profile of "+getIntent().getStringExtra("Musername")+" Sing up");
    	
    	
    	
     	relativeLayout3.setOnClickListener(clickListener);
     	relativeLayout4.setOnClickListener(clickListener);
     	relativeLayout5.setOnClickListener(clickListener);
     	
     	relativeLayout3.startAnimation(move_down1);
     	relativeLayout4.startAnimation(move_down2);
     	relativeLayout5.startAnimation(move_down3);
     	
     	
     	settings = getSharedPreferences(Utils.SharedPrefsKey, 0);
		remember = settings.getBoolean("rememberMe",false);
     	mAsyncRunner = new AsyncFacebookRunner(facebook);
	}
	
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) 
	{
		super.onActivityResult(requestCode, resultCode, data);
		facebook.authorizeCallback(requestCode, resultCode, data);
	}
	
	
	
	OnClickListener clickListener = new OnClickListener() 
	{
		@Override
		public void onClick(View v) 
		{
			if(v == relativeLayout2) { }
			else if(v == relativeLayout3)
			{
				loginToFacebook();
			}
			else if(v == relativeLayout4)
			{
				Toast.makeText(ActMeet3.this, getResources().getString(R.string.under_construction), Toast.LENGTH_LONG).show();
			}
			else if(v == relativeLayout5)
			{
				Intent intent = new Intent();
				intent.setClass(ActMeet3.this, ActRegister2.class);
				startActivity(intent);
			}
		}
	};
	
	
	
	/**
	 * Function to login into facebook
	 * */
	public void loginToFacebook() 
	{
		mPrefs = getPreferences(MODE_PRIVATE);
		String access_token = mPrefs.getString("access_token", null);
		long expires = mPrefs.getLong("access_expires", 0);

		if (access_token != null) 
		{
			facebook.setAccessToken(access_token);
			Log.d("FB Sessions", "" + facebook.isSessionValid());
		}

		if (expires != 0) 
		{
			facebook.setAccessExpires(expires);
		}

		if (!facebook.isSessionValid()) 
		{
			facebook.authorize(this, new String[] { "email", "publish_stream" }, new DialogListener() 
			{
				@Override
				public void onCancel() { }

				@Override
				public void onComplete(Bundle values) 
				{
					getProfileInformation();
				}
				@Override
				public void onError(DialogError error) { }

				@Override
				public void onFacebookError(FacebookError fberror) { }
			});
		}
	}
	
	
	
	/**
	 * Get Profile information by making request to Facebook Graph API
	 * */
	public void getProfileInformation() 
	{
		mAsyncRunner.request("me", new RequestListener() 
		{
			@SuppressWarnings("unused")
			@Override
			public void onComplete(String response, Object state) 
			{
				Log.d("Profile", response);
				String json = response;
				try 
				{
					JSONObject profile = new JSONObject(response);
					final String email = profile.getString("email");
					final String id = profile.getString("id");
					final String name = profile.getString("name");
					
					runOnUiThread(new Runnable() 
					{
						@Override
						public void run() 
						{
							new LoginTask(email,null,id,name,null).execute();
						}

					});
				} 
				catch (JSONException e) 
				{
					e.printStackTrace();
				}
			}

			@Override
			public void onIOException(IOException e, Object state) { }
			@Override
			public void onFileNotFoundException(FileNotFoundException e, Object state) { }
			@Override
			public void onMalformedURLException(MalformedURLException e, Object state) { }
			@Override
			public void onFacebookError(FacebookError e, Object state) { }
		});
	}
	
	
	
	class LoginTask extends AsyncTask<Void, Integer, UserProfile>
	{
		int myProgressCount;
		String _email,_password,_fbid,_fbname,_fbusername;
		public LoginTask(String email, String password, String fbid, String fbname, String fbusername)
		{
			this._email = email;
			this._password = password;
			this._fbid = fbid;
			this._fbname = fbname;
			this._fbusername = fbusername;
		}
		
		@Override
		protected void onPreExecute()
		{	
			Dwaiting.show();
		}
		
		@Override
		protected UserProfile doInBackground(Void... params) 
		{
			UserProfile result = ServiceClient.loginUser_v2(_email, _password, Utils.getLoginDetails(ActMeet3.this),_fbid,_fbname,_fbusername);
			return result;
		}
		
		@SuppressLint("InlinedApi")
		@Override
		protected void onPostExecute(UserProfile result)
		{	
			if(result!=null && checkProfile(result))
			{				
				ProfileManager.saveProfile(ActMeet3.this, result);
				if(rememberMe)
				{
					Editor edit = settings.edit();
					edit.putBoolean("rememberMe", true);
					edit.putString("Email", result.UserName);
					edit.putString("Pass", result.Password);
					edit.commit();
				}
				else
				{
					Editor edit = settings.edit();
					edit.putBoolean("rememberMe", false);
					edit.commit();
				}
				Intent intent = new Intent(ActMeet3.this,ActHome.class);
		        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
		        ActMeet3.this.startActivity(intent);
			}
			else
			{
				Intent intent = new Intent();
				intent.setClass(ActMeet3.this, ActRegister2.class);
				startActivity(intent);
			}
			Dwaiting.cancel();
		}
	}
	
	private boolean checkProfile(UserProfile profile)
	{
		if(profile.ProfileId==null)
		{
			return false;
		}
		else if(profile.UserName==null)
		{
			return false;
		}
		return true;
	}

}

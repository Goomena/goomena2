﻿Public Class Login
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            Dim newUrl As String = Request.Url.ToString()
            newUrl = newUrl.ToLower().Replace("login.aspx", "smsservice.ashx")
            WebInfoSendEmail("Redirecting from Login.aspx." & vbCrLf & "New URL " & newUrl)

            Try

                Dim Url As New Uri(newUrl)
                Dim newUrl1 As String = Url.PathAndQuery.TrimStart("/")
                Server.Execute(newUrl1)

            Catch ex As Exception
                Response.RedirectPermanent(newUrl)
            End Try


        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorSendEmail(ex, "")
            Throw
        End Try
    End Sub

End Class
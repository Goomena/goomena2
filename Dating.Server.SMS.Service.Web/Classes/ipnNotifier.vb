﻿Public Class IPNNotifier
    Implements System.IDisposable

    Private ws As UniPay.epochIPN


    Public Property UniPayTransactionID As String
    Public Property CustomerEmaiAddress As String
    Public Property RefereceNumber As String
    Public Property amount As String
    Public Property status As String
    Public Property custFirstName As String
    Public Property custLastName As String
    Public Property custAddress As String
    Public Property custCity As String
    Public Property custState As String
    Public Property custCountry As String
    Public Property custZip As String
    Public Property customerIP As String
    Public Property feeAmount As String
    Public Property netAmount As String
    Public Property currency As String
    Public Property memberID As String
    Public Property STRproductCode As String
    Public Property paymentMethod As String
    Public Property paymentSubType As String
    Public Property siteid As Integer
    Public Property loginname As String
    Public Property customerid As Integer
    Public Property customReferrer As String
    Public Property isRebill As Boolean



    Public Function SendResults(url As String) As String
        If (ws Is Nothing) Then
            ws = New UniPay.epochIPN
        End If
        ws.Url = url
        Dim result As String = ""
        result =
           ws.IPN(
                    Me.UniPayTransactionID,
                    Me.CustomerEmaiAddress,
                    Me.RefereceNumber,
                    Me.amount,
                    Me.status,
                    Me.custFirstName,
                    Me.custLastName,
                    Me.custAddress,
                    Me.custCity,
                    Me.custState,
                    Me.custCountry,
                    Me.custZip,
                    Me.customerIP,
                    Me.feeAmount,
                    Me.netAmount,
                    Me.currency,
                    Me.memberID,
                    Me.STRproductCode,
                    Me.paymentMethod,
                    Me.paymentSubType,
                    Me.siteid,
                    Me.loginname,
                    Me.customerid,
                    Me.customReferrer,
                    Me.isRebill
                     )


        Return result
    End Function


    Public Sub Dispose() Implements IDisposable.Dispose
        If (ws IsNot Nothing) Then
            ws.Dispose()
            ws = Nothing
        End If
    End Sub

End Class

﻿Imports Dating.Server.Core.DLL
Imports Dating.Server.Core.DLL.clsSiteLAG
Imports Dating.Server
Imports System.Collections.Concurrent
Imports System.Web.SessionState
Imports System.Web

''' <summary>
''' v 0.4
''' </summary>
''' <remarks></remarks>
Public Class clsPageDataBase

    Public Enum CacheOptionsEnum
        None = 0
        DisabledCacheForPageBasics = 1
        DisabledCacheForContent = 2
    End Enum
#Region "caching related fuynctionality"
    Public Class CachedDatatable
        Public DtStrings As New DataTable()
        Public TableSize As Integer
        Public LastAccess As DateTime = Date.UtcNow
    End Class

    'Public Shared gLAG As New Core.DLL.clsSiteLAG()

    Protected Friend Shared Property gDataList_Pages As New ConcurrentDictionary(Of String, clsPageBasicReturn)
    Protected Friend Shared Property gDataList_PageCustoms As New ConcurrentDictionary(Of String, CachedDatatable)
    Protected Friend Shared Property RemovalExclutionFromCustoms As String() = {"GlobalStrings2", "Register_LoginApi", "control.UcMemberLeftBar", "AjaxContent"}
    Protected Friend Shared Sub RemoveOldPageCustoms()

        Try
            For cnt = 0 To gDataList_PageCustoms.Keys.Count - 1
                Dim key As String = gDataList_PageCustoms.Keys(cnt)
                If Not RemovalExclutionFromCustoms.Contains(key) Then
                    Dim mins As Integer = (DateTime.UtcNow - gDataList_PageCustoms(key).LastAccess).TotalMinutes
                    If (gDataList_PageCustoms(key).TableSize > 2048 AndAlso mins > 1) Then
                        'if the size of BodyHTM column is greater than 2kb
                        'remove that item
                        gDataList_PageCustoms.TryRemove(key, Nothing)
                        Exit For
                    ElseIf (mins > 3) Then
                        'remove one item at a time
                        gDataList_PageCustoms.TryRemove(key, Nothing)
                        Exit For
                    End If
                End If
            Next
        Catch ex As Exception
        End Try
    End Sub
    Public Shared Function GetCachedDataTable_PageCustoms(pageName As String) As DataTable
        Dim dtStrings As DataTable = Nothing
        Try
            If (Not String.IsNullOrEmpty(pageName) AndAlso gDataList_PageCustoms.ContainsKey(pageName)) Then

                Dim s As CachedDatatable = Nothing


                If gDataList_PageCustoms.TryGetValue(pageName, s) Then
                    dtStrings = s.DtStrings
                    s.LastAccess = DateTime.UtcNow
                    gDataList_PageCustoms.TryUpdate(pageName, s, Nothing)
                    gDataList_PageCustoms(pageName).LastAccess = DateTime.UtcNow

                End If

            End If
        Catch ex As Exception
        End Try
        Return dtStrings
    End Function

    Public Shared Sub SetCachedDataTable_PageCustoms(pageName As String, dtStrings As DataTable)
        Try
            If (Not String.IsNullOrEmpty(pageName) AndAlso Not gDataList_PageCustoms.ContainsKey(pageName)) Then
                Dim cch As New CachedDatatable()
                cch.DtStrings = dtStrings
                gDataList_PageCustoms.GetOrAdd(pageName, cch)
            End If
        Catch ex As Exception

        End Try
    End Sub
    Protected Friend Shared Sub RemoveOldPages()
        Try
            For cnt = 0 To gDataList_PageCustoms.Keys.Count - 1
                Dim key As String = gDataList_PageCustoms.Keys(cnt)
                Dim mins As Integer = (DateTime.UtcNow - gDataList_PageCustoms(key).LastAccess).TotalMinutes
                If (gDataList_PageCustoms(key).TableSize > 2048 AndAlso mins > 1) Then
                    'if the size of BodyHTM column is greater than 2kb
                    'remove that item
                    gDataList_PageCustoms.TryRemove(key, Nothing)
                    Exit For
                ElseIf (mins > 3) Then
                    'remove one item at a time
                    gDataList_PageCustoms.TryRemove(key, Nothing)
                    Exit For
                End If
            Next
        Catch ex As Exception
        End Try
    End Sub
    Public Shared Function GetCachedDataTable_Pages(pageNameAndLang As String) As clsPageBasicReturn
        Dim dtStrings As clsPageBasicReturn = Nothing

        Try
            If (Not String.IsNullOrEmpty(pageNameAndLang) AndAlso gDataList_Pages.ContainsKey(pageNameAndLang)) Then
                dtStrings = gDataList_Pages(pageNameAndLang)
                gDataList_Pages(pageNameAndLang).LastAccess = DateTime.UtcNow
            End If

            '   Threading.Tasks.Task.Run(Sub()
            '    RemoveOldPages()
            '  End Sub)
        Catch ex As Exception
        End Try

        Return dtStrings
    End Function


    Public Shared Sub SetCachedDataTable_Pages(pageName As String, dtStrings As clsPageBasicReturn)
        Try
            If (Not String.IsNullOrEmpty(pageName) AndAlso Not gDataList_Pages.ContainsKey(pageName)) Then
                gDataList_Pages.GetOrAdd(pageName, dtStrings)
            End If
        Catch ex As Exception

        End Try
    End Sub

    Public Shared Sub ClearCache()
        Try
            gDataList_PageCustoms.Clear()
        Catch
        End Try

        Try
            gDataList_Pages.Clear()
        Catch
        End Try

    End Sub


#End Region

    Public Property IsQueryStringParamDetected As Boolean
    Public Property CacheDisabledForPageBasics As Boolean
    Public Property CacheDisabledForContent As Boolean

    Protected Friend _SitePageID As Long = -1
    Public ReadOnly Property SitePageID As Long
        Get
            Return _SitePageID
        End Get
    End Property


    Protected Friend _PageName As String
    Public ReadOnly Property PageName As String
        Get
            If (_PageName Is Nothing) Then _PageName = String.Empty
            Return _PageName
        End Get
    End Property


    Protected Friend _Title As String
    Public ReadOnly Property Title As String
        Get
            If (_Title Is Nothing) Then _Title = String.Empty
            Return _Title
        End Get
    End Property
    Public Sub SetLagId(ByVal LagId As String)
        _LagID = LagId
    End Sub

    Protected Friend _LagID As String
    Public ReadOnly Property LagID As String
        Get
            If (_LagID Is Nothing) Then _LagID = String.Empty
            Return _LagID
        End Get
    End Property


    Protected Friend _cPageBasic As clsSiteLAG.clsPageBasicReturn
    Public ReadOnly Property cPageBasic As clsSiteLAG.clsPageBasicReturn
        Get
            Return _cPageBasic
        End Get
    End Property

    Protected Friend _dtStrings As DataTable
    Public Event CustomStringRetrievalComplete(ByRef sender As Object, ByRef Args As CustomStringRetrievalCompleteEventArgs)

    Public Sub New()

    End Sub
    Public Sub New(ByVal pageName As String, cacheOptions As CacheOptionsEnum, ByVal lagId As String)

        _PageName = pageName

        If ((cacheOptions And CacheOptionsEnum.DisabledCacheForPageBasics) > 0) Then
            Me.CacheDisabledForPageBasics = True
        End If
        If ((cacheOptions And CacheOptionsEnum.DisabledCacheForContent) > 0) Then
            Me.CacheDisabledForContent = True
        End If



        If lagId.Trim.Length = 0 Then
            _LagID = "US"
        Else
            _LagID = lagId
        End If
        _cPageBasic = GetPageBasics(_LagID)
        If (_cPageBasic Is Nothing) Then Throw New PageBasicNotIntializedException()

        _SitePageID = _cPageBasic.SitePageID
        If _Title Is Nothing OrElse _Title.Length = 0 Then
            _Title = _cPageBasic.PageTitle
        End If
    End Sub
   
 


    Public Function GetPageBasics(ByVal LagIDparam As String) As clsPageBasicReturn
        Dim dtStrings As New clsPageBasicReturn
        If (Not String.IsNullOrEmpty(_PageName)) Then
            _PageName = _PageName.ToLower()
        End If

        Dim _newPageName = _PageName
        If (_SitePageID > 0) Then _newPageName = _PageName & "_" & _SitePageID
        _newPageName = _newPageName & LagIDparam

        If (Me.CacheDisabledForPageBasics = True OrElse Me.CacheDisabledForContent = True) Then

            If (_SitePageID > 0) Then
                dtStrings = gLAG.GetPageBasics(LagIDparam, _SitePageID)
            Else
                dtStrings = gLAG.GetPageBasics(LagIDparam, _PageName)
            End If

        Else

            dtStrings = GetCachedDataTable_Pages(_newPageName)
            If (dtStrings Is Nothing) Then
                If (_SitePageID > 0) Then
                    dtStrings = gLAG.GetPageBasics(LagIDparam, _SitePageID)
                Else
                    dtStrings = gLAG.GetPageBasics(LagIDparam, _PageName)
                End If
                SetCachedDataTable_Pages(_newPageName, dtStrings)
            End If

        End If

        Return dtStrings
    End Function

    Public Function VerifyCustomString(ByVal key As String) As String
        Dim result As String = Me.VerifyCustomString(key, LagID)
        Return result
    End Function

    Public Function VerifyCustomString(ByVal key As String, LagIDparam As String) As String
        Dim result As String = Me.GetCustomString(key, LagIDparam)
        If (result Is Nothing) Then
            gDataList_PageCustoms.Clear()
            ' _keysDictionary = Nothing
            _dtStrings = Nothing
            result = Me.GetCustomString(key, LagIDparam)
        End If
        Return result
    End Function

    ''' <summary>
    ''' Returns NOTHING if specified key is not found.
    ''' </summary>
    ''' <param name="key">A key to be found in database</param>
    ''' <returns>value paired to spicified key, otherwise NOTHING</returns>
    ''' <remarks></remarks>
    Public Function GetCustomStringJS(ByVal key As String) As String
        Dim result As String = GetCustomString(key, LagID)
        If (result IsNot Nothing) Then AppUtils.JS_PrepareString(result)
        Return result
    End Function

    ''' <summary>
    ''' Returns NOTHING if specified key is not found.
    ''' </summary>
    ''' <param name="key">A key to be found in database</param>
    ''' <returns>value paired to spicified key, otherwise NOTHING</returns>
    ''' <remarks></remarks>
    Public Overloads Function GetCustomString(ByVal key As String) As String
        Dim result As String = GetCustomString(key, LagID)
        Return result
    End Function


    ''' <summary>
    ''' Returns NOTHING if specified key is not found.
    ''' </summary>
    ''' <param name="key">A key to be found in database</param>
    ''' <returns>value paired to spicified key, otherwise NOTHING</returns>
    ''' <remarks></remarks>
    Overloads Function GetCustomString(ByVal key As String, LagIDparam As String) As String
        Dim result As String = ""
        Try
            If (_dtStrings Is Nothing) Then

                '   _keysDictionary = New Dictionary(Of String, String)

                _dtStrings = New DataTable()

                If (Not String.IsNullOrEmpty(_PageName)) Then _PageName = _PageName.ToLower()

                Try
                    ' get sys_messages records for a page

                    Dim _newPageName = _PageName
                    If (cPageBasic.SitePageID > 0) Then _newPageName = _PageName & "_" & cPageBasic.SitePageID

                    If (Not Me.CacheDisabledForContent) Then
                        _dtStrings = GetCachedDataTable_PageCustoms(_newPageName & LagIDparam)
                    Else
                        Try
                            If (gDataList_PageCustoms.ContainsKey(_newPageName & LagIDparam)) Then
                                gDataList_PageCustoms.TryRemove(_newPageName & LagIDparam, Nothing)
                            End If
                            'If (gDataList_PageCustoms.ContainsKey(_newPageName)) Then
                            '    gDataList_PageCustoms.Remove(_newPageName)
                            'End If
                        Catch
                        End Try
                    End If

                    If (_dtStrings Is Nothing) Then
                        ' get dummy dt
                        _dtStrings = gLAG.GetCustomStrings(_SitePageID, LagIDparam)
                        SetCachedDataTable_PageCustoms(_newPageName & LagIDparam, _dtStrings)
                    End If
                Catch
                End Try
            End If

            ' load cache using sys_messages records 
            '     If (Not _keysDictionary.ContainsKey(key)) Then
            'Dim dr As DataRow = (From itm In _dtStrings.Rows
            '                      Where itm("MessageKey") = key
            '                      Select itm).FirstOrDefault()
            '   Dim sqlKey As String = key.Replace("'", "''")
            Dim dr As DataRow = Nothing
            Try

                If _dtStrings IsNot Nothing AndAlso _dtStrings.Columns.Contains("MessageKey") Then
                    Dim r As DataRow() = _dtStrings.Select("MessageKey='" & key & "'")
                    If r.Length > 0 Then
                        dr = r(0)
                    End If
                End If

                'For cnt = 0 To _dtStrings.Rows.Count - 1
                '    If (_dtStrings.Rows(cnt)("MessageKey") = key) Then
                '        dr = _dtStrings.Rows(cnt)
                '        Exit For
                '    End If
                'Next
            Catch ex As Exception

            End Try
            'Dim drs As DataRow() = {}
            'Try
            '    drs = _dtStrings.Select("MessageKey='" & sqlKey & "'")
            'Catch ex As System.InvalidOperationException
            '    Try
            '        drs = _dtStrings.Select("MessageKey='" & sqlKey & "'")
            '    Catch ex1 As System.InvalidOperationException
            '    End Try
            'End Try

            'If (drs.Length > 0) Then dr = drs(0)
            '   Dim removeit As Boolean = True
            If (dr Is Nothing) Then

                Using dt As DataTable = gLAG.GetCustomStringDataTable(cPageBasic.SitePageID, key, LagID)


                    If (dt.Rows.Count = 0) Then
                        Dim __dr1 As DataRow = dt.NewRow()
                        __dr1("MessageID") = -(_dtStrings.Rows.Count)
                        __dr1("SitePageID") = cPageBasic.SitePageID
                        __dr1("MessageKey") = key
                        dt.Rows.Add(__dr1)
                    End If

                    If (dt.Rows.Count > 0) Then
                        dr = dt.Rows(0)
                        'removeit = False
                        ' if the size of US column is greater than 2,5kb
                        'If (dr(LagID).ToString().Length < 2560) Then
                        '    Try
                        '        _dtStrings.Merge(dt)
                        '    Catch ex As System.InvalidOperationException
                        '        Try
                        '            _dtStrings.Merge(dt)
                        '        Catch ex1 As System.InvalidOperationException
                        '        End Try
                        '    End Try
                        'End If
                    End If
                End Using
            End If

            If (dr IsNot Nothing) Then
                Dim messageKey As String = dr("MessageKey").ToString()

                Dim valueLag As String = ""
                If (_dtStrings.Columns.Contains(LagIDparam)) Then valueLag = dr(LagIDparam).ToString()
                If (valueLag.Length > 0) Then
                    result = valueLag
                Else
                    Dim valueUS As String = dr(LagIDparam).ToString()
                    result = valueUS
                End If
                ' If removeit Then _dtStrings.Rows.Remove(dr)
            End If

            'End If


            'If (_keysDictionary.ContainsKey(key)) Then
            '    result = _keysDictionary(key)
            'End If


            If (Not String.IsNullOrEmpty(result)) Then

                Dim args As New CustomStringRetrievalCompleteEventArgs(result, LagIDparam)
                RaiseEvent CustomStringRetrievalComplete(Me, args)
                result = args.CustomString

            End If
        Catch ex As Exception

        End Try

        Return result
    End Function

    Public Overrides Function ToString() As String
        Dim info As String = String.Empty

        'info = info & "_affCode: " & _affCode & vbCrLf
        info = info & "_SitePageID: " & _SitePageID & vbCrLf
        info = info & "_PageName: " & _PageName & vbCrLf
        info = info & "_Title: " & _Title & vbCrLf
        info = info & "_LagID: " & _LagID & vbCrLf

        Return info
    End Function

End Class



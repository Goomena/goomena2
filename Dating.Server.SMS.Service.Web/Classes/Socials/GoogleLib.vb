﻿Imports System.Net
Imports System.IO

Public Class GoogleLib

    ''' <summary>
    ''' Get Facebook login Url
    ''' </summary>
    ''' <returns>Facebook login URL(including AppID,AppSECREt,RedirectLink)</returns>
    Public Shared Function GetLoginURL(ByVal Client_ID As String, ByVal ReplyURL As String) As String
        Try
            Dim strOut As String = "https://accounts.google.com/o/oauth2/auth?state=%2Fprofile&redirect_uri=" + ReplyURL + "&response_type=token&client_id=" + Client_ID +
                "&scope=https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email"
            Return strOut
        Catch ex As Exception
            Return Nothing
        End Try
    End Function


    Public Shared Function GetUser(access_token As String) As GoogleUserInfo
        Dim tw As New GoogleUserInfo()

        Dim URI As [String] = "https://www.googleapis.com/oauth2/v1/userinfo?access_token=" & access_token
        Dim b As String
        Using webClient As New WebClient()
            Using stream As Stream = webClient.OpenRead(URI)
                Using br As New StreamReader(stream)
                    b = br.ReadToEnd()
                End Using
            End Using
        End Using
       


        'I have not used any JSON parser because I do not want to use any extra dll/3rd party dll

        

        b = b.Replace("id", "").Replace("email", "")
        b = b.Replace("given_name", "")
        b = b.Replace("family_name", "").Replace("link", "").Replace("picture", "")
        b = b.Replace("gender", "").Replace("locale", "").Replace(":", "")
        b = b.Replace("""", "").Replace("name", "").Replace("{", "").Replace("}", "")

        '
        '                 
        '                "id": "109124950535374******"
        '                  "email": "usernamil@gmail.com"
        '                  "verified_email": true
        '                  "name": "firstname lastname"
        '                  "given_name": "firstname"
        '                  "family_name": "lastname"
        '                  "link": "https://plus.google.com/10912495053537********"
        '                  "picture": "https://lh3.googleusercontent.com/......./photo.jpg"
        '                  "gender": "male"
        '                  "locale": "en" } 
        '               


        Dim ar As Array = b.Split(",".ToCharArray())
        For p As Integer = 0 To ar.Length - 1
            ar.SetValue(ar.GetValue(p).ToString().Trim(), p)
        Next

        tw.email = ar.GetValue(1).ToString()
        tw.uid = ar.GetValue(0).ToString()
        tw.first_name = ar.GetValue(4).ToString()
        tw.last_name = ar.GetValue(5).ToString()
        tw.name = ar.GetValue(3).ToString()
        tw.link = ar.GetValue(6).ToString()
        tw.picture = ar.GetValue(7).ToString()
        tw.gender = ar.GetValue(8).ToString()
        tw.locale = ar.GetValue(9).ToString()


        Return tw
    End Function


End Class

<Serializable()>
Public Class GoogleUserInfo

    Public Property uid As String
    Public Property email As String
    Public Property first_name As String
    Public Property last_name As String
    Public Property name As String
    Public Property link As String
    Public Property picture As String
    Public Property gender As String
    Public Property locale As String
    Public Property birthday_date As String

End Class

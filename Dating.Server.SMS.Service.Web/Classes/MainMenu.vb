Imports DevExpress.Web.ASPxMenu

Namespace ServerControls
    ''' <summary>
    ''' I wanted a way to display a bulleted list of items, bound from a strongly-typed list of strings.
    ''' The .net "BulletedList" control seemed promising, but it insisted on HTML-endcoding the content of the string, so HTML could not
    ''' be displayed onto the page within the bullet. This control was created to be clean, and be easy to build from a List(Of String).
    ''' However I realise that it may be useful to sometimes have HTML encoded, so I also provide the "EncodeHtml" property to do this.
    ''' </summary>
    ''' <remarks>Ben Sloan, 11 July 2008</remarks>
    Public Class MainMenu
        Inherits ASPxMenu



        ' ''' <summary>
        ' ''' Gets or sets the list of strings to be rendered by the control.
        ' ''' </summary>
        ' ''' <value>A strongly typed list of strings</value>
        ' ''' <returns>A strongly typed list of strings</returns>
        'Public Property Items() As DevExpress.Web.ASPxMenu.MenuItemCollection
        '    Get
        '        If IsNothing(ViewState("Items")) Then
        '            ' Instantiate the list of items to a new list; which will allow us to do ".Items.Add" from elsewhere in code without 
        '            ' having to worry about null values checks.
        '            ViewState("Items") = New DevExpress.Web.ASPxMenu.MenuItemCollection()
        '        End If
        '        Return DirectCast(ViewState("Items"), DevExpress.Web.ASPxMenu.MenuItemCollection)
        '    End Get
        '    Set(ByVal value As DevExpress.Web.ASPxMenu.MenuItemCollection)
        '        ViewState("Items") = value
        '    End Set
        'End Property

        ' ''' <summary>
        ' ''' Determines whether HTML should be encoded before it is rendered to the output stream.
        ' ''' For example, should a "&gt;" symbol be rendered as the &gt;, or as &amp;gt;?
        ' ''' </summary>
        ' ''' <value></value>
        ' ''' <returns></returns>
        ' ''' <remarks></remarks>
        'Public Property EncodeHtml() As Boolean
        '    Get
        '        If IsNothing(ViewState("EncodeHtml")) Then
        '            Return False
        '        Else
        '            Return DirectCast(ViewState("EncodeHtml"), Boolean)
        '        End If
        '    End Get
        '    Set(ByVal value As Boolean)
        '        ViewState("EncodeHtml") = value
        '    End Set
        'End Property



        ''' <summary>
        ''' Determines how the control should render itself.
        ''' </summary>
        Protected Overrides Sub Render(writer As System.Web.UI.HtmlTextWriter)
            ' Only output something if there are items in the collection:
            If Items.Count > 0 Then

                'writer.Write("<div id=""menubar"" class=""grid-block"">")
                writer.Write("<ul class=""menu menu-dropdown"">")

                For Each itm In Me.Items
                    _RenderLevel1(writer, itm)
                Next

                writer.Write("</ul>")
                'writer.Write("<div id=""menubar-r""><div></div></div>")
                'writer.Write("<div id=""search""></div>")
                'writer.Write("</div>")
            End If

            'MyBase.Render(writer)
        End Sub

        Private ItemCounter As Integer = 0



        Private Sub _RenderLevel1(writer As System.Web.UI.HtmlTextWriter, _item As DevExpress.Web.ASPxMenu.MenuItem)
            ItemCounter = ItemCounter + 1

            System.Diagnostics.Debug.Print("Selected " & _item.Selected)
            System.Diagnostics.Debug.Print("HasChildren " & _item.HasChildren)

            Dim txt As String = ""

            If (_item.HasChildren) Then


                If (_item.Image IsNot Nothing AndAlso Not _item.Image.IsEmpty) Then
                    txt = <h><![CDATA[
<li class="level1 item#COUNTER# parent #SELECTED#">
    <a href="#NAVIGATEURL#" class="level1 parent #SELECTED#">
        <span>
            <span class="icon" style="background-image: url('#IMAGEURL#');"></span>
            <span class="title">#TEXT#</span> 
        </span></a>
    <div class="dropdown columns1">
        <div class="dropdown-bg">
            <div>
                <div class="width100 column">
                    <ul class="level2">
]]></h>.Value

                    txt = Regex.Replace(txt, "#COUNTER#", ItemCounter.ToString())
                    txt = Regex.Replace(txt, "#NAVIGATEURL#", _item.NavigateUrl)
                    txt = Regex.Replace(txt, "#IMAGEURL#", _item.Image.Url)
                    txt = Regex.Replace(txt, "#TEXT#", _item.Text)

                    If (_item.Selected) Then
                        txt = Regex.Replace(txt, "#SELECTED#", "active current")
                    Else
                        txt = Regex.Replace(txt, "#SELECTED#", "")
                    End If

                    writer.Write(txt)

                    ' render children
                    For Each itm In _item.Items
                        _RenderLevel2(writer, itm)
                    Next

                    txt = <h><![CDATA[
                    </ul>
                </div>
            </div>
        </div>
    </div>
</li>
]]></h>.Value

                    writer.Write(txt)
                Else
                    txt = <h><![CDATA[
<li class="level1 item#COUNTER# parent #SELECTED#">
    <a href="#NAVIGATEURL#" class="level1 parent #SELECTED#"><span>#TEXT#</span></a>
    <div class="dropdown columns1">
        <div class="dropdown-bg">
            <div>
                <div class="width100 column">
                    <ul class="level2">
]]></h>.Value

                    txt = Regex.Replace(txt, "#COUNTER#", ItemCounter.ToString())
                    txt = Regex.Replace(txt, "#NAVIGATEURL#", _item.NavigateUrl)
                    txt = Regex.Replace(txt, "#TEXT#", _item.Text)

                    If (_item.Selected) Then
                        txt = Regex.Replace(txt, "#SELECTED#", "active current")
                    Else
                        txt = Regex.Replace(txt, "#SELECTED#", "")
                    End If

                    writer.Write(txt)

                    For Each itm In _item.Items
                        _RenderLevel2(writer, itm)
                    Next

                    txt = <h><![CDATA[
                    </ul>
                </div>
            </div>
        </div>
    </div>
</li>
]]></h>.Value

                    writer.Write(txt)
                End If
            Else
                ' no children

                If (_item.Image IsNot Nothing AndAlso Not _item.Image.IsEmpty) Then
                    txt = <h><![CDATA[
<li class="level1 item#COUNTER# #SELECTED#">
    <a href="#NAVIGATEURL#" class="level1 #SELECTED#">
        <span>
            <span class="icon" style="background-image: url('#IMAGEURL#');"></span>
            <span class="title">#TEXT#</span> 
        </span></a>
</li>
]]></h>
                    txt = Regex.Replace(txt, "#COUNTER#", ItemCounter.ToString())
                    txt = Regex.Replace(txt, "#NAVIGATEURL#", _item.NavigateUrl)
                    txt = Regex.Replace(txt, "#IMAGEURL#", _item.Image.Url)
                    txt = Regex.Replace(txt, "#TEXT#", _item.Text)

                    If (_item.Selected) Then
                        txt = Regex.Replace(txt, "#SELECTED#", "active current")
                    Else
                        txt = Regex.Replace(txt, "#SELECTED#", "")
                    End If

                    writer.Write(txt)
                Else
                    txt = <h><![CDATA[
<li class="level1 item#COUNTER# #SELECTED#">
    <a href="#NAVIGATEURL#" class="level1 #SELECTED#"><span>#TEXT#</span></a>
</li>
]]></h>
                    txt = Regex.Replace(txt, "#COUNTER#", ItemCounter.ToString())
                    txt = Regex.Replace(txt, "#NAVIGATEURL#", _item.NavigateUrl)
                    txt = Regex.Replace(txt, "#IMAGEURL#", _item.Image.Url)
                    txt = Regex.Replace(txt, "#TEXT#", _item.Text)

                    If (_item.Selected) Then
                        txt = Regex.Replace(txt, "#SELECTED#", "active current")
                    Else
                        txt = Regex.Replace(txt, "#SELECTED#", "")
                    End If

                    writer.Write(txt)
                End If
            End If

        End Sub


        Private Sub _RenderLevel2(writer As System.Web.UI.HtmlTextWriter, _item As DevExpress.Web.ASPxMenu.MenuItem)
            ItemCounter = ItemCounter + 1

            System.Diagnostics.Debug.Print("Selected " & _item.Selected)
            System.Diagnostics.Debug.Print("HasChildren " & _item.HasChildren)

            Dim txt As String = ""

            If (_item.Image IsNot Nothing AndAlso Not _item.Image.IsEmpty) Then
                txt = <h><![CDATA[
    <li class="level2 item#COUNTER# #SELECTED#"><a href="#NAVIGATEURL#" class="level2 #SELECTED#">
        <span>
            <span class="icon" style="background-image: url('#IMAGEURL#');"></span>
            <span class="title">#TEXT#</span></a>
    </li>
]]></h>.Value

                txt = Regex.Replace(txt, "#COUNTER#", ItemCounter.ToString())
                txt = Regex.Replace(txt, "#NAVIGATEURL#", _item.NavigateUrl)
                txt = Regex.Replace(txt, "#IMAGEURL#", _item.Image.Url)
                txt = Regex.Replace(txt, "#TEXT#", _item.Text)

                If (_item.Selected) Then
                    txt = Regex.Replace(txt, "#SELECTED#", "active current")
                Else
                    txt = Regex.Replace(txt, "#SELECTED#", "")
                End If

                writer.Write(txt)
            Else

                txt = <h><![CDATA[
    <li class="level2 item#COUNTER#  #SELECTED#">
        <a href="#NAVIGATEURL#" class="level2  #SELECTED#"><span>#TEXT#</span></a>
    </li>
]]></h>
                txt = Regex.Replace(txt, "#COUNTER#", ItemCounter.ToString())
                txt = Regex.Replace(txt, "#NAVIGATEURL#", _item.NavigateUrl)
                txt = Regex.Replace(txt, "#TEXT#", _item.Text)

                If (_item.Selected) Then
                    txt = Regex.Replace(txt, "#SELECTED#", "active current")
                Else
                    txt = Regex.Replace(txt, "#SELECTED#", "")
                End If

                writer.Write(txt)
            End If

            If (_item.HasChildren) Then
                For Each itm In _item.Items
                    _RenderLevel2(writer, itm)
                Next
            End If

        End Sub


    End Class
End Namespace



﻿Imports System.Diagnostics
Imports Microsoft.VisualBasic
Imports System
Imports System.Drawing
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.IO
Imports DevExpress.XtraEditors
Imports DevExpress.Skins
Imports DevExpress.Utils.Drawing
Imports DevExpress.Utils
Imports System.Text.RegularExpressions
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports System.Web.UI
Imports System.Text
Imports System.Web.UI.HtmlControls
Imports System.Web
Imports Library.Public
Imports Dating.Server.Core.DLL

Public Class AppUtils
    Inherits Dating.Server.Core.DLL.AppUtils


    Public Const gLoginNameValidationRegex As String = "^(?=.*\d)(?=(.*[a-zA-Z]){2}).[^&:<>]{3,12}$"

    Public Shared Function mySQLDate(ByVal D As DateTime) As String
        On Error GoTo er
        Dim sz As String
        sz = Microsoft.VisualBasic.Year(D) & "/" & Microsoft.VisualBasic.Month(D) & "/" & Microsoft.VisualBasic.DateAndTime.Day(D) ' d.Year & "/" & d.Month & "/" & d.Day
        mySQLDate = " CONVERT(DATETIME, '" & sz & "  00:00:00', 102)" '"'" & sZ & "'"
        Exit Function
er:     ErrorMsgBox("")
    End Function

    Public Shared Function mySQLDateTime(ByVal D As DateTime) As String
        On Error GoTo er
        Dim sz As String
        sz = Microsoft.VisualBasic.Year(D) _
            & "/" _
            & Microsoft.VisualBasic.Month(D) _
            & "/" _
            & Microsoft.VisualBasic.DateAndTime.Day(D) _
            & " " _
            & Microsoft.VisualBasic.Format(Microsoft.VisualBasic.DateAndTime.Hour(D), "#0") _
            & ":" _
            & Microsoft.VisualBasic.Format(Microsoft.VisualBasic.DateAndTime.Minute(D), "#0") _
            & ":" _
            & Microsoft.VisualBasic.Format(Microsoft.VisualBasic.DateAndTime.Second(D), "#0")
        mySQLDateTime = " CONVERT(DATETIME, '" & sz & "', 102)" '"'" & sZ & "'"
        Exit Function
er:     ErrorMsgBox("")
    End Function

    Public Shared Function mySQLDateTimeCustom(ByVal D As DateTime, ByVal szTime As String) As String
        On Error GoTo er
        Dim sz As String
        sz = Microsoft.VisualBasic.Year(D) & "/" & Microsoft.VisualBasic.Month(D) & "/" & Microsoft.VisualBasic.DateAndTime.Day(D) ' d.Year & "/" & d.Month & "/" & d.Day
        mySQLDateTimeCustom = " CONVERT(DATETIME, '" & sz & "  " & szTime & "', 102)" '"'" & sZ & "'"
        Exit Function
er:     ErrorMsgBox("")
    End Function

    Public Shared Function mySQLDateOnly(ByVal D As DateTime) As String
        On Error GoTo er
        Dim sz As String
        sz = Microsoft.VisualBasic.Year(D) & "/" & Microsoft.VisualBasic.Month(D) & "/" & Microsoft.VisualBasic.DateAndTime.Day(D) ' d.Year & "/" & d.Month & "/" & d.Day
        mySQLDateOnly = sz & "  00:00:00" '"'" & sZ & "'"
        Exit Function
er:     ErrorMsgBox("")
    End Function


    Public Shared Function FindControlIterative(ByVal myRoot As Control, ByVal myIDOfControlToFind As String) As Control

        Dim myRootControl As Control = New Control
        myRootControl = myRoot
        Dim setOfChildControls As LinkedList(Of Control) = New LinkedList(Of Control)

        Do While (myRootControl IsNot Nothing)
            If myRootControl.ID = myIDOfControlToFind Then
                Return myRootControl
            End If
            For Each childControl As Control In myRootControl.Controls
                If childControl.ID = myIDOfControlToFind Then
                    Return childControl
                End If
                If childControl.HasControls() Then
                    setOfChildControls.AddLast(childControl)
                End If
            Next
            myRootControl = setOfChildControls.First.Value
            setOfChildControls.Remove(myRootControl)
        Loop

        Return Nothing

    End Function


    Public Shared Sub ErrorMsgBox(ByVal sExtaMessage As String)
        'On Error GoTo er ' **** na min enegorpithi katastrefete to error
        Dim s As String = Err.Description & vbCrLf & "Error Code: " & Err.Number & vbCrLf & "Source: " & Err.Source & vbCrLf & "Function:" & sExtaMessage
        HttpContext.Current.Response.Write(s)
        ' Err.Clear()
        Exit Sub
er:
        MsgBox("Error on ErrorMsgBox->" & sExtaMessage)
    End Sub

    Public Shared Sub ErrorMsgBox(ByVal ex As Exception, ByVal sExtaMessage As String)
        Try
            Dim s As String = ex.Message & "<br>" & "Source: " & ex.Source & " <br>[Function:" & sExtaMessage & "<br>] Stack: " & ex.StackTrace & "<br>"
            'MsgBox(s, "Error", MessageBoxButtons.OK)
            HttpContext.Current.Response.Write(s)
        Catch inex As Exception
            'MsgBox(inex.Message & vbCrLf & "Error on ErrorMsgBox->" & sExtaMessage)
        End Try
    End Sub


    Public Shared Function CheckNULL(ByVal nValue As Object, ByVal DefaultValue As String) As String
        Try
            If IsDBNull(nValue) Then nValue = DefaultValue
            If Len(DefaultValue) > 0 And Len(nValue) = 0 Then Return DefaultValue
            Return nValue
        Catch ex As Exception
            ErrorMsgBox(ex, "")
            Return DefaultValue
        End Try
    End Function


  

    Public Shared Function GetParamValueFromUrl(ByVal szURL As String, ByVal szParamName As String, ByVal DefaultValue As String) As String
        Try

            Dim StartPos As Integer = InStr(szURL, szParamName & "=")
            If StartPos = 0 Then Return ""
            Dim NewString As String = Mid(szURL, StartPos + 2)
            Dim EndPos As Integer = InStr(NewString, "&")
            If EndPos = 0 Then EndPos = Len(NewString) - StartPos
            Dim Value As String = Mid(szURL, StartPos + 2, EndPos - 1)
            Return Value

        Catch ex As Exception
            'ErrorMsgBox(ex, "")
            Return DefaultValue
        End Try
    End Function



    Public Shared Function GetImage(CustomerID As String, FileName As String, GenderId As String, isThumb As Boolean, isHTTPS As Boolean) As String
        Dim photoPath As String = ""
        If (isThumb) Then
            photoPath = GetImage(CustomerID, FileName, GenderId, isThumb, isHTTPS, PhotoSize.Thumb)
        Else
            photoPath = GetImage(CustomerID, FileName, GenderId, isThumb, isHTTPS, PhotoSize.Original)
        End If
        Return photoPath
    End Function

    Public Shared Function GetImage(CustomerID As String, FileName As String, GenderId As String, isThumb As Boolean, isHTTPS As Boolean, photoSize As PhotoSize) As String
        Dim photoPath As String = ""
        Try

            Dim _CustomerID As Integer = 0
            Dim _GenderId As Integer = 0

            If (Not String.IsNullOrEmpty(CustomerID)) Then
                Integer.TryParse(CustomerID, _CustomerID)
            End If

            If (Not String.IsNullOrEmpty(GenderId)) Then
                Integer.TryParse(GenderId, _GenderId)
            End If

            photoPath = ProfileHelper.GetProfileImageURL(_CustomerID, FileName, _GenderId, True, isHTTPS, photoSize)
            'If (HttpContext.Current.Session("ProfileID") = 269) Then
            '    If (HttpContext.Current.Request.Url.Scheme = "https") Then
            '        photoPath = photoPath.Replace("http://", "https://")
            '    End If
            'End If
        Catch ex As Exception

        End Try
        Return photoPath
    End Function

   
    Public Shared Function GetDateTimeString(dateTimeValue As Object, Optional tookenString As String = "", Optional formatString As String = "d MMM, yyyy H:mm") As String
        Dim txt = ""
        Try
            If (HttpContext.Current.Session("TimeOffset") IsNot Nothing AndAlso HttpContext.Current.Session("TimeOffset").GetType() = GetType(Double)) Then
                Dim dt As DateTime = CType(dateTimeValue, DateTime)
                dt = dt.AddMinutes(HttpContext.Current.Session("TimeOffset") * -1)
                txt = dt.ToString(formatString)
            Else
                txt = CType(dateTimeValue, DateTime).ToLocalTime().ToString(formatString)
            End If

            If (Not String.IsNullOrEmpty(tookenString)) Then txt = tookenString.Replace("###DATETIME###", txt)
        Catch
        End Try
        Return txt
    End Function


    Public Shared Function GetDateTimeWithUserOffset(dateTimeValue As DateTime) As DateTime
        Try
            If (HttpContext.Current.Session("TimeOffset") IsNot Nothing AndAlso HttpContext.Current.Session("TimeOffset").GetType() = GetType(Double)) Then
                Dim dt As DateTime = dateTimeValue
                dt = dt.AddMinutes(HttpContext.Current.Session("TimeOffset") * -1)
                dateTimeValue = dt
            End If
        Catch
        End Try
        Return dateTimeValue
    End Function

    'Public Shared Sub GetDateTimeDescription_MemberConnectTEST(ByRef PageData As clsPageData)
    '    Dim MemberConnect_Recently As String = GetDateTimeDescription_MemberConnect(DateTime.UtcNow.AddHours(-3), PageData)
    '    Dim MemberConnect_Today As String = GetDateTimeDescription_MemberConnect(DateTime.UtcNow.AddHours(-7), PageData)
    'End Sub

    Public Shared Function GetDateTimeDescription_MemberConnect(dateTimeValue As DateTime, recentlyActiveHours As Integer) As MemberConnectDescriptionEnum
        Dim result As MemberConnectDescriptionEnum = MemberConnectDescriptionEnum.None
        Dim userDt As DateTime = dateTimeValue
        Dim currentDt As DateTime = DateTime.Now
        Try
            If (HttpContext.Current.Session("TimeOffset") IsNot Nothing AndAlso HttpContext.Current.Session("TimeOffset").GetType() = GetType(Double)) Then
                userDt = dateTimeValue.AddMinutes(HttpContext.Current.Session("TimeOffset") * -1)
                currentDt = DateTime.UtcNow.AddMinutes(HttpContext.Current.Session("TimeOffset") * -1)
            Else
                userDt = dateTimeValue.ToLocalTime()
            End If

            Dim timeDiff As TimeSpan = (currentDt - userDt)
            Dim isSameDate As Boolean = (currentDt.Date = userDt.Date)
            ' Dim tsSameDateDiff As TimeSpan = currentDt.Date.AddDays(1) - userDt.Date
            Dim monthsDiff As Integer = (currentDt.Month - userDt.Month)

            Dim calendar As System.Globalization.Calendar = System.Globalization.CultureInfo.CurrentCulture.Calendar
            Dim currentWeekOfYear As Integer = calendar.GetWeekOfYear(currentDt, Globalization.CalendarWeekRule.FirstDay, DayOfWeek.Sunday)
            Dim userWeekOfYear As Integer = calendar.GetWeekOfYear(userDt, Globalization.CalendarWeekRule.FirstDay, DayOfWeek.Sunday)



            If (timeDiff.TotalHours <= recentlyActiveHours) Then
                result = MemberConnectDescriptionEnum.MemberConnect_Recently
                'result = PageData.GetCustomString("MemberConnect.Recently")

            ElseIf (timeDiff.TotalHours < 24 AndAlso isSameDate) Then
                result = MemberConnectDescriptionEnum.MemberConnect_Today
                ' result = PageData.GetCustomString("MemberConnect.Today")

            ElseIf (timeDiff.TotalHours < 24 AndAlso Not isSameDate) Then
                result = MemberConnectDescriptionEnum.MemberConnect_Yesterday
                '  result = PageData.GetCustomString("MemberConnect.Yesterday")

            ElseIf (timeDiff.TotalDays < 2) Then
                result = MemberConnectDescriptionEnum.MemberConnect_Yesterday
                '  result = PageData.GetCustomString("MemberConnect.Yesterday")

            ElseIf (timeDiff.TotalDays = 2) Then
                result = MemberConnectDescriptionEnum.MemberConnect_2DaysAgo
                '  result = PageData.GetCustomString("MemberConnect.2DaysAgo")

            ElseIf (currentWeekOfYear = userWeekOfYear) Then
                result = MemberConnectDescriptionEnum.MemberConnect_ThisWeek
                ' result = PageData.GetCustomString("MemberConnect.ThisWeek")

            ElseIf ((currentWeekOfYear - userWeekOfYear) = 1) Then
                result = MemberConnectDescriptionEnum.MemberConnect_PreviousWeek
                '  result = PageData.GetCustomString("MemberConnect.PreviousWeek")

            ElseIf (timeDiff.TotalDays <= 30) Then
                result = MemberConnectDescriptionEnum.MemberConnect_PreviousFortnight
                ' result = PageData.GetCustomString("MemberConnect.PreviousFortnight")

            ElseIf (monthsDiff >= 1 AndAlso monthsDiff < 3) Then
                result = MemberConnectDescriptionEnum.MemberConnect_MoreThenMonth
                '  result = PageData.GetCustomString("MemberConnect.MoreThenMonth")

            ElseIf (monthsDiff >= 3 AndAlso monthsDiff < 6) Then
                result = MemberConnectDescriptionEnum.MemberConnect_MoreThen3Moths
                'result = PageData.GetCustomString("MemberConnect.MoreThen3Moths")

            Else
                result = MemberConnectDescriptionEnum.MemberConnect_MoreThen6Moths
                'result = PageData.GetCustomString("MemberConnect.MoreThen6Moths")

            End If

            If (isSameDate) Then


            End If
            'If (timeDiff) Then
        Catch
        End Try


        Return result
    End Function


    Public Shared Function GetDateTimeDescription_MessageDate(dateTimeValue As DateTime) As MessageDateDescriptionEnum
        Dim result As MessageDateDescriptionEnum = MessageDateDescriptionEnum.None
        Dim userDt As DateTime = dateTimeValue
        Dim currentDt As DateTime = DateTime.Now
        Try
            If (HttpContext.Current.Session("TimeOffset") IsNot Nothing AndAlso HttpContext.Current.Session("TimeOffset").GetType() = GetType(Double)) Then
                userDt = dateTimeValue.AddMinutes(HttpContext.Current.Session("TimeOffset") * -1)
                currentDt = DateTime.UtcNow.AddMinutes(HttpContext.Current.Session("TimeOffset") * -1)
            Else
                userDt = dateTimeValue.ToLocalTime()
            End If

            Dim timeDiff As TimeSpan = (currentDt - userDt)
            If (timeDiff.TotalHours <= 1) Then
                result = MessageDateDescriptionEnum.MessageDate_Now

            ElseIf (timeDiff.TotalHours < 2) Then
                result = MessageDateDescriptionEnum.MessageDate_AnHourAgo

            ElseIf (timeDiff.TotalHours < 20) Then
                result = MessageDateDescriptionEnum.MessageDate_HoursAgo

            ElseIf (timeDiff.TotalDays < 2) Then
                result = MessageDateDescriptionEnum.MessageDate_Yesterday

            ElseIf (timeDiff.TotalDays = 2) Then
                result = MessageDateDescriptionEnum.MessageDate_2DaysAgo

            Else
                result = MessageDateDescriptionEnum.MessageDate_ShowDate

            End If

        Catch
        End Try


        Return result
    End Function



    Public Shared Function GetLoginDescription(descrLoginenm As MemberConnectDescriptionEnum, ByRef pageData As clsPageData) As String
        Dim result As String = ""

        Select Case (descrLoginenm)
            Case MemberConnectDescriptionEnum.MemberConnect_Recently
                result = pageData.GetCustomString("MemberConnect.Recently")

            Case MemberConnectDescriptionEnum.MemberConnect_Today
                result = pageData.GetCustomString("MemberConnect.Today")

            Case MemberConnectDescriptionEnum.MemberConnect_Yesterday
                result = pageData.GetCustomString("MemberConnect.Yesterday")

            Case MemberConnectDescriptionEnum.MemberConnect_2DaysAgo
                result = pageData.GetCustomString("MemberConnect.2DaysAgo")

            Case MemberConnectDescriptionEnum.MemberConnect_ThisWeek
                result = pageData.GetCustomString("MemberConnect.ThisWeek")

            Case MemberConnectDescriptionEnum.MemberConnect_PreviousWeek
                result = pageData.GetCustomString("MemberConnect.PreviousWeek")

            Case MemberConnectDescriptionEnum.MemberConnect_PreviousFortnight
                result = pageData.GetCustomString("MemberConnect.PreviousFortnight")

            Case MemberConnectDescriptionEnum.MemberConnect_MoreThenMonth
                result = pageData.GetCustomString("MemberConnect.MoreThenMonth")

            Case MemberConnectDescriptionEnum.MemberConnect_MoreThen3Moths
                result = pageData.GetCustomString("MemberConnect.MoreThen3Moths")

            Case MemberConnectDescriptionEnum.MemberConnect_MoreThen6Moths
                result = pageData.GetCustomString("MemberConnect.MoreThen6Moths")

        End Select

        Return result
    End Function

    Public Shared Function GetMessageDateDescription(dateToDescribe As DateTime, ByRef pageData As clsPageData) As String
        Dim descrptEnm As MessageDateDescriptionEnum = AppUtils.GetDateTimeDescription_MessageDate(dateToDescribe)
        Dim result As String = ""

        Select Case (descrptEnm)
            Case MessageDateDescriptionEnum.MessageDate_Now
                result = pageData.GetCustomString("Event.WithinHour")

            Case MessageDateDescriptionEnum.MessageDate_AnHourAgo
                result = pageData.GetCustomString("Event.Before1Hour")

            Case MessageDateDescriptionEnum.MessageDate_HoursAgo
                result = pageData.GetCustomString("Event.Before1-19Hours")
                Dim usertime As DateTime = AppUtils.GetDateTimeWithUserOffset(dateToDescribe)
                Dim currenttime As DateTime = AppUtils.GetDateTimeWithUserOffset(Date.UtcNow)
                result = result.Replace("[HOUR]", CInt((currenttime - usertime).TotalHours))

            Case MessageDateDescriptionEnum.MessageDate_Yesterday
                result = pageData.GetCustomString("Event.Yesterday")

            Case MessageDateDescriptionEnum.MessageDate_2DaysAgo
                result = pageData.GetCustomString("Event.2DaysAgo")

            Case MessageDateDescriptionEnum.MessageDate_ShowDate
                result = AppUtils.GetDateTimeString(dateToDescribe, "", "dd/MM/yyyy")

        End Select

        Return result
    End Function



    Public Shared Function IsDBNullOrNothingOrEmpty(obj As Object) As Boolean
        If (obj Is System.DBNull.Value) Then Return True
        If (obj Is Nothing) Then Return True
        If (obj.GetType() Is GetType(String) AndAlso obj.ToString() = String.Empty) Then Return True

        Return False
    End Function


    Public Shared Function JS_PrepareAlertString(input As String) As String
        If (Not String.IsNullOrEmpty(input)) Then
            input = input.Replace(vbCrLf, "\r\n").Replace(vbCr, "\r\n").Replace(vbLf, "\r\n").Replace("'", "\'")
        End If
        Return input
    End Function

    Public Shared Function JS_PrepareString(input As String) As String
        If (Not String.IsNullOrEmpty(input)) Then
            input = input.Replace("'", "\'")
            input = StringClearNewLines(input)
        End If
        Return input
    End Function


    Public Shared Function StringClearNewLines(str As String) As String
        str = str.Replace(Chr(13), "").Replace(Chr(10), "").Replace("    ", "")
        Return str
    End Function


    Public Shared Function DataTableToJSON(dt As DataTable) As String
        'Dim dict As New Dictionary(Of String, Object)

        Dim arr(dt.Rows.Count) As Object

        For i As Integer = 0 To dt.Rows.Count - 1
            'arr(i) = dt.Rows(i).ItemArray
            Dim dict As New Dictionary(Of String, Object)
            For Each dc As DataColumn In dt.Columns
                dict.Add(dc.ColumnName, dt.Rows(i)(dc.ColumnName))
            Next
            arr(i) = dict
        Next

        'dict.Add(dt.TableName, arr)

        Dim json As New Script.Serialization.JavaScriptSerializer
        Return json.Serialize(arr)
    End Function


    Public Shared Sub SetControlAttribute_Class(ByRef ctl As HtmlGenericControl, attValue As String)
        If (ctl.Attributes("class") IsNot Nothing) Then
            If (ctl.Attributes("class").Trim() = "") Then
                ctl.Attributes("class") = attValue
            Else
                ctl.Attributes("class") = ctl.Attributes("class") & " " & attValue
            End If
        Else
            ctl.Attributes.Add("class", attValue)
        End If
    End Sub

End Class

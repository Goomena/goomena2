﻿Imports System.Web
Imports System.Web.Services
Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Core.DLL
Imports System.Globalization

Public Class SmsService
    Implements System.Web.IHttpHandler, System.Web.SessionState.IReadOnlySessionState

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest

        Dim ctx As New CMSDBDataContext(ModGlobals.ConnectionString)
        '  gLAG.Init(ModGlobals.CMSConnectionString)
        Try

            Dim Response As HttpResponse = context.Response
            Dim Request As HttpRequest = context.Request
            '  Dim Session As HttpSessionState = context.Session

            Dim sr As New System.IO.StreamReader(Request.InputStream)
            Dim InputStream As String = sr.ReadToEnd()

            If (Not String.IsNullOrEmpty(InputStream) OrElse
                Not String.IsNullOrEmpty(Request.QueryString.ToString())) Then

                InputStream = "query::" & vbCrLf & Request.QueryString.ToString() & vbCrLf & vbCrLf & "stream::" & vbCrLf & InputStream

                Dating.Server.Core.DLL.clsMyMail.TrySendMail(ConfigurationManager.AppSettings("ExceptionsEmail"), "sms-service", InputStream)
            End If

            Dim success As Boolean = False
            Dim continue1 As Boolean = False
            Dim Profile As EUS_Profile = Nothing
            '  Dim SmsReload As SmsReloadReturnAction = New SmsReloadReturnAction With {.WhatToDo = SmsTransactionNextAction.DoNothing}
            Dim lagid As String = "US"
            continue1 = (
                Request("op") = "smsredirect" AndAlso
                (Request("to") = "54345" OrElse Request("to") = "54030") AndAlso
                Not String.IsNullOrEmpty(Request("from"))
                )

            If (continue1 AndAlso Not String.IsNullOrEmpty(Request("text"))) Then

                Dim loginName As String = Request("text")

                Try

                    ' retrieve user password
                    Profile = (From itm In ctx.EUS_Profiles
                            Where (itm.LoginName.ToUpper() = loginName.ToUpper() OrElse itm.eMail.ToUpper() = loginName.ToUpper()) AndAlso
                                  (itm.Status = ProfileStatusEnum.Approved OrElse itm.Status = ProfileStatusEnum.LIMITED) AndAlso
                                  itm.IsMaster = True
                            Select itm).FirstOrDefault()


                    If (Profile IsNot Nothing AndAlso Profile.ProfileID > 1) Then
                        lagid = Profile.LAGID
                        '  SmsReload = clsCustomer.GetProfileSmsReloadLimitations(If(Profile.Country Is DBNull.Value, "US", Profile.Country), Profile.ProfileID, Profile.MirrorProfileID, DateTime.UtcNow)
                        Dim transactionInfo As String = Request("from")
                        Dim Description As String = "SMS Payment to " & Request("to")
                        Dim tra As EUS_CustomerTransaction = Nothing
                        Dim STRproductCode As String = "dd150CreditsSMS"
                        If Request("to") = "54030" Then
                            STRproductCode = "dd200CreditsSMS"
                            tra = clsCustomer.RenewCreditsWithSMS3(Profile, transactionInfo, Description, Request.ServerVariables("REMOTE_ADDR"), Request.ServerVariables("HTTP_REFERER"), InputStream)
                        Else
                            tra = clsCustomer.RenewCreditsWithSMS2(Profile, transactionInfo, Description, Request.ServerVariables("REMOTE_ADDR"), Request.ServerVariables("HTTP_REFERER"), InputStream)
                        End If
                        '   If SmsReload.WhatToDo = SmsTransactionNextAction.DoNothing Then


                        If tra IsNot Nothing Then
                            Try
                                Dim count As Integer = 0
                                count = (From tra2 As EUS_CustomerTransaction In ctx.EUS_CustomerTransactions
                               Where tra2.CustomerTransactionID <> tra.CustomerTransactionID AndAlso tra2.CustomerID = tra.CustomerID AndAlso (tra2.PaymentMethods.ToLower = "sms" OrElse tra2.PaymentMethods.ToLower = "sms2")
                                                                           Select tra2).Count
                                Dim us As New CultureInfo("en-US")
                                Dim ipnNotifier As New IPNNotifier
                                ipnNotifier.UniPayTransactionID = tra.CustomerTransactionID
                                ipnNotifier.CustomerEmaiAddress = If(Profile.eMail IsNot Nothing, Profile.eMail, "")
                                ipnNotifier.RefereceNumber = tra.CustomerTransactionID
                                ipnNotifier.amount = tra.DebitAmountReceived.Value.ToString("0.00", us)
                                ipnNotifier.status = "YConfirmed"
                                ipnNotifier.custFirstName = ""
                                ipnNotifier.custLastName = ""
                                ipnNotifier.custAddress = ""
                                ipnNotifier.custCity = ""
                                ipnNotifier.custState = ""
                                ipnNotifier.custCountry = ""
                                ipnNotifier.custZip = ""
                                ipnNotifier.customerIP = Request.ServerVariables("REMOTE_ADDR")
                                ipnNotifier.feeAmount = 0
                                ipnNotifier.netAmount = ipnNotifier.amount
                                ipnNotifier.currency = If(tra.Currency IsNot Nothing, tra.Currency, "EUR")
                                ipnNotifier.memberID = If(tra.MemberID IsNot Nothing, tra.MemberID, "")
                                ipnNotifier.STRproductCode = STRproductCode
                                ipnNotifier.paymentSubType = ""
                                ipnNotifier.siteid = 5
                                ipnNotifier.loginname = If(Profile.eMail IsNot Nothing, Profile.eMail, "")
                                ipnNotifier.customerid = tra.CustomerID
                                ipnNotifier.customReferrer = Request.ServerVariables("HTTP_REFERER")
                                ipnNotifier.isRebill = (count > 0)
                                ipnNotifier.paymentMethod = "sms"
                                Dim s As String = ipnNotifier.SendResults("http://goomena.com/pay/epochipn.asmx")
                            Catch ex As Exception
                                WebErrorSendEmail(ex, "Error on Informing Unipay from SMS")
                            End Try

                            success = True
                        End If




                    End If

                Catch ex As Exception
                    WebErrorSendEmail(ex, "")
                End Try
            End If




            If (continue1) Then

                Dim _globalStrings As New clsPageDataBase("GlobalStrings", clsPageDataBase.CacheOptionsEnum.None, lagid)
                If (success) Then
                    'If SmsReload.WhatToDo = SmsTransactionNextAction.DoNothing Then
                    Dim responseMessage As String = _globalStrings.GetCustomString("SMS.Redirect.Response")
                  
                    '    If (String.IsNullOrEmpty(responseMessage)) Then responseMessage = "OK"
                    '    Response.Write(responseMessage)
                    'ElseIf SmsReload.WhatToDo = SmsTransactionNextAction.WarnHimAboutTheLimit Then
                    '    Dim responseMessage As String = _globalStrings.GetCustomString("SMS.Redirect.ResponseWithWarning", "GR")
                    '    If (String.IsNullOrEmpty(responseMessage)) Then
                    '        responseMessage = _globalStrings.GetCustomString("SMS.Redirect.ResponseWithWarning", "US")
                    '    End If
                    '    responseMessage = responseMessage.Replace("[###Date###]", SmsReload.DateSmsLimitIsOff.ToString("dd/MM/yyyy"))
                    '    Try
                    '        Dim adminMessageText As String = _globalStrings.GetCustomString("NotificationSMSWarningText", Profile.LAGID)
                    '        Dim adminMessageSubject As String = _globalStrings.GetCustomString("NotificationSMSWarningSubject", Profile.LAGID)
                    '        If (String.IsNullOrEmpty(adminMessageText)) Then
                    '            adminMessageText = _globalStrings.GetCustomString("NotificationSMSWarningText", "US")
                    '            adminMessageSubject = _globalStrings.GetCustomString("NotificationSMSWarningSubject", "US")
                    '        End If
                    '        adminMessageText = adminMessageText.Replace("[###Date###]", SmsReload.DateSmsLimitIsOff.ToString("dd/MM/yyyy")).Replace("###LOGINNAME###", Profile.LoginName)
                    '        clsUserDoes.SendMessageFromAdministrator(adminMessageSubject, adminMessageText, Profile.ProfileID)
                    '    Catch ex As Exception
                    '        WebErrorSendEmail(ex, "")
                    '    End Try

                    'NotificationSMSWarningSubject
                    'NotificationSMSWarningText
                    '[###Date###].
                    '###LOGINNAME###
                    If (String.IsNullOrEmpty(responseMessage)) Then responseMessage = "Your credits were successfully renewed! Now you can talk to any hot girls you like!"
                    Response.Write(responseMessage) 'SMS.Redirect.Response.NotReload
                    'Else
                    '    'NotificationSMSNotReloadSubject
                    '    'NotificationSMSNotReloadText
                    '    Dim responseMessage As String = _globalStrings.GetCustomString("SMS.Redirect.Response.NotReload", "GR")
                    '    If (String.IsNullOrEmpty(responseMessage)) Then
                    '        responseMessage = _globalStrings.GetCustomString("SMS.Redirect.Response.NotReload", "US")
                    '    End If
                    '    responseMessage = responseMessage.Replace("[###Date###]", SmsReload.DateSmsLimitIsOff.ToString("dd/MM/yyyy"))
                    '    Try
                    '        Dim adminMessageText As String = _globalStrings.GetCustomString("NotificationSMSNotReloadText", Profile.LAGID)
                    '        Dim adminMessageSubject As String = _globalStrings.GetCustomString("NotificationSMSNotReloadSubject", Profile.LAGID)
                    '        If (String.IsNullOrEmpty(adminMessageText)) Then
                    '            adminMessageText = _globalStrings.GetCustomString("NotificationSMSNotReloadText", "US")
                    '            adminMessageSubject = _globalStrings.GetCustomString("NotificationSMSNotReloadSubject", "US")
                    '        End If
                    '        adminMessageText = adminMessageText.Replace("[###Date###]", SmsReload.DateSmsLimitIsOff.ToString("dd/MM/yyyy")).Replace("###LOGINNAME###", Profile.LoginName)
                    '        clsUserDoes.SendMessageFromAdministrator(adminMessageSubject, adminMessageText, Profile.ProfileID)
                    '    Catch ex As Exception
                    '        WebErrorSendEmail(ex, "")
                    '    End Try
                    '    '[###Date###].
                    '    If (String.IsNullOrEmpty(responseMessage)) Then responseMessage = "OK"
                    '    Response.Write(responseMessage) 'SMS.Redirect.Response.NotReload
                    'End If

                Else
                    Dim responseMessage As String = _globalStrings.GetCustomString("SMS.Redirect.Response.CODEEmail")
                    If (String.IsNullOrEmpty(responseMessage)) Then responseMessage = "Wrong LOGIN input. Send us by e-mail [CODE] and your LOGIN in info@goomena.com and we will renew your credits."

                    Dim code As String = clsCustomer.CreateNewSMSCode(InputStream)
                    responseMessage = responseMessage.Replace("[CODE]", code)

                    Response.Write(responseMessage)
                End If

            Else
                WebInfoSendEmail("incorrect login")

                Response.Write("incorrect login")
                Response.StatusCode = 401
                Response.StatusDescription = "Unauthorized"

            End If


        Catch ex As Exception
            WebErrorSendEmail(ex, "")
        Finally
            ctx.Dispose()

        End Try

    End Sub

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class

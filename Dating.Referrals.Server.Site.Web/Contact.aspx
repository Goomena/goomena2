﻿<%@ Page Title="" Language="vb" AutoEventWireup="true" MasterPageFile="~/Root.Master" CodeBehind="Contact.aspx.vb" Inherits="Dating.Referrals.Server.Site.Web.Contact" %>
<%@ Register src="UserControls/LiveZilla.ascx" tagname="LiveZilla" tagprefix="uc1" %>
<%@ Register src="UserControls/ContactControl.ascx" tagname="ContactControl" tagprefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        td.header { width: 1%; white-space: nowrap; padding: 0 20px 0 0px; vertical-align: text-top; display: block; text-align: left; }
        .clear { clear: both; }
        ul li { list-style: none; }
        .mosaiccontact { position: relative; width: 1440px; height: 459px; background: url("Images2/contact-us/contact-us-big.jpg") no-repeat; background-position: center; margin-top: 16px; margin-left: auto; margin-right: auto; /*border-top:4px solid #be202f;
    box-shadow: 0px 5px 5px 1px #c7c5c8 ;*/ }
        .maincontact { /*background: url("Images2/first-general/bg-reapet.jpg") ;
    box-shadow: 0px 5px 5px 1px #c7c5c8 ;*/ height: 590px; }
        .contactuscontainer { margin-left: auto; margin-right: auto; width: 988px; height: 380px; position: relative; padding-top: 45px; }
        .contactuscontainer h1 { font-size: 24px; font-weight: lighter; width: 400px; margin: 0; border-bottom: 1px solid #c7c7c7; }
        .contactusform { width: 530px; display: inline-block; }
        .contactus { position: absolute; right: 50px; top: 41px; width: 256px; height: 375; }
        .contactus h1 { width: 212px; border-bottom: 1px solid #c7c7c7; font-size: 24px; font-weight: lighter; }
        .contactus p { width: 214px; text-align: left; font-size: 83%; line-height: 18px; margin: 18px auto 24px; }
        .peLabel-reg{font-size:14px;}

        .form-title{font-size:22px;margin:0;margin-bottom:10px;padding-left:20px;font-weight:normal;}
        .form_right{border-left:5px solid #FFCC33;margin-bottom:10px;}
        .form_right .dxeTextBox, .form_right .dxeMemo{border:none;}
        .form_right .dxeTextBox .dxeEditArea{height:22px !important; width:300px;}
        .check-box label{margin-left:5px;vertical-align:middle;}
        .check-box input{vertical-align:middle;}
        .contact_form li {
            clear: both;
            padding: 0 0;
        }
        .contactLabel {
            font-family: Segoe UI;
            width: auto;
            text-align: right;
            display: inline-block;
            font-size: 14px;
            padding: 0 0 0 0;
            padding-right: 0;
            white-space: nowrap;
        }
        .contactuscontainer h1 {
            font-family: Segoe UI;
            font-size: 22px;
            font-weight: normal;
            width: 400px;
            margin: 0;
            border-bottom: none;
            margin-bottom: 10px;
            padding-left: 20px;
        }
        .contactuscontainer ul{
font-size:14px;background-color:#DAD8D9;padding:20px;width:325px;
            }
        .contactuscontainer .form-actions{
font-size:14px;padding:20px;width:325px;margin:0 0;
            }
            .contact_form{
                font-size:14px;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <div class="contactuscontainer">
        <asp:Label ID="lbHeader" runat="server" />

        <table>
            <tr>
                <td valign="top"><uc2:ContactControl ID="ContactCtl" runat="server" SendButton="DefaultApperance" /></td>
                <td>&nbsp;</td>
                <td valign="top">
                    <asp:Label ID="lblContactText" runat="server" Text=""></asp:Label>
                </td>
            </tr>
        </table>
        
    </div>
</asp:Content>

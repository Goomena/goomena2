﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Root.Master"
    CodeBehind="Login.aspx.vb" Inherits="Dating.Referrals.Server.Site.Web.Login" %>
<%@ Register Src="Security/UserControls/ucLogin.ascx" TagName="ucLogin" TagPrefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
<div class="pe_form pe_login" style="background:#F3F1F2;">
                                <uc1:ucLogin ID="ucLogin1" runat="server">
                                    <LoginControl UserNameRequiredErrorMessage="Username or email is required." Width="100%">
                                        <LayoutTemplate>
                        <asp:Panel ID="Panel1" runat="server" class="pe_box" DefaultButton="btnLoginNow">
<dx:ASPxValidationSummary ID="valSumm" runat="server" 
    ValidationGroup="LoginGroup" RenderMode="BulletedList" 
    ShowErrorsInEditors="True">
    <Paddings PaddingBottom="10px" />
    <LinkStyle>
        <Font Size="14px"></Font>
    </LinkStyle>
</dx:ASPxValidationSummary>

<div align="center">
<table border="0" cellspacing="0" cellpadding="0">
<tr>
    <td colspan="3"><div style="height:7px;">&nbsp;</div></td>
</tr>
<tr>
    <td valign="middle"><asp:Label ID="lblUserName" runat="server" Text="Username or Email" CssClass="registerTableLabel" AssociatedControlID="UserName"/></td>
    <td>&nbsp;&nbsp;&nbsp;</td>
    <td valign="top">
<div class="form_right floatLeft">
        <dx:ASPxTextBox ID="UserName" runat="server" CssClass="field" Width="310px" Font-Size="14px" Height="18px">
            <ValidationSettings ValidationGroup="LoginGroup"  ErrorDisplayMode="ImageWithTooltip" ErrorTextPosition="Left" ErrorText="" Display="Dynamic">
                <RequiredField ErrorText="Username or email is required." 
                    IsRequired="True" />
            </ValidationSettings>
            <Border BorderStyle="None" />
        </dx:ASPxTextBox>
</div>
        </td>
</tr>
<tr>
    <td colspan="3"><div style="height:7px;">&nbsp;</div></td>
</tr>
<tr>
<td valign="middle"><asp:Label ID="lblPassword" runat="server" Text="Password" CssClass="registerTableLabel" AssociatedControlID="Password"/></td>
    <td>&nbsp;&nbsp;&nbsp;</td>
<td valign="top">
    <dx:ASPxTextBox ID="Password" runat="server" CssClass="field" Width="310px" Password="True" Font-Size="14px" Height="18px">
        <ValidationSettings ValidationGroup="LoginGroup" ErrorDisplayMode="ImageWithTooltip" ErrorTextPosition="Left" ErrorText="" Display="Dynamic">
            <RequiredField ErrorText="Password is required." 
                IsRequired="True" />
        </ValidationSettings>
        <Border BorderStyle="None" />
    </dx:ASPxTextBox></td>
</tr>
<tr>
    <td colspan="3"><div style="height:7px;">&nbsp;</div></td>
</tr>
<tr>
<td valign="top" colspan="3" align="center">
    <dx:ASPxCheckBox ID="chkRememberMe" runat="server" Text="" EncodeHtml="False" Font-Size="14px">
                                            </dx:ASPxCheckBox></td>
</tr>
</table>
</div>

                                <div class="clear"></div>
                                <br />
                                <div class="form-actions">
                                    <dx:ASPxButton ID="btnLoginNow" runat="server" CausesValidation="True" CommandName="Login"
                                     Text="Login now" ValidationGroup="LoginGroup" Native="True"  
                                    EncodeHtml="False" EnableDefaultAppearance="False" 
                                    BackColor="#000000" ForeColor="White" 
                                    Width="135px" Height="32px" Font-Size="16px" />
                                </div>
                                </asp:Panel>
                                        </LayoutTemplate>
                                    </LoginControl>
                                </uc1:ucLogin>
                            </div>

</asp:Content>

﻿Public Class Contact
    Inherits BasePage



    Dim _pageData As clsPageData
    Protected ReadOnly Property CurrentPageData As clsPageData
        Get
            If (_pageData Is Nothing) Then _pageData = New clsPageData("Referrer.Contact.aspx", Context)
            Return _pageData
        End Get
    End Property


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            AddHandler ContactCtl.SendSuccesfull, AddressOf ContactCtl_SendSuccesfull

            Try
                Dim cPageBasic As Dating.Server.Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
                AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "Page_Load")
            End Try


            If (Not Me.IsPostBack) Then
                LoadLAG()
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try
    End Sub



    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
    End Sub


    Protected Sub LoadLAG()
        Try
            Dim cPageBasic As Dating.Server.Core.DLL.clsSiteLAG.clsPageBasicReturn = Me.CurrentPageData.cPageBasic

            lbHeader.Text = Me.CurrentPageData.GetCustomString(lbHeader.ID)
            lblContactText.Text = Me.CurrentPageData.GetCustomString("lblContactText")
            'lblTextlivezilla.Text = Me.CurrentPageData.GetCustomString("lblTextlivezilla")

            If (TypeOf Page.Master Is INavigation) Then
                CType(Page.Master, INavigation).AddNaviLink(Me.CurrentPageData.Title, "")
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender

        Try
            Dim cPageBasic As Dating.Server.Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub

    Private Sub ContactCtl_SendSuccesfull(sender As Object, e As EventArgs)
        lblContactText.Visible = False
    End Sub

End Class
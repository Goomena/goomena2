﻿

// When the DOM is ready, initialize the scripts.
jQuery(function ($) {
    /*
    script found at 
    http://www.bennadel.com/blog/1810-Creating-A-Sometimes-Fixed-Position-Element-With-jQuery.htm
    */
    // Get a reference to the placeholder. This element
    // will take up visual space when the message is
    // moved into a fixed position.
    var placeholder = $("#topFrameHolder");

    // Get a reference to the message whose position
    // we want to "fix" on window-scroll.
    var message = $("#topFrame");

    // Get a reference to the window object; we will use
    // this several time, so cache the jQuery wrapper.
    var view = $(window);


    // Bind to the window scroll and resize events.
    // Remember, resizing can also change the scroll
    // of the page.
    view.bind(
				"scroll resize",
				function () {
				    try {
				        // Get the current offset of the placeholder.
				        // Since the message might be in fixed
				        // position, it is the plcaeholder that will
				        // give us reliable offsets.
				        var placeholderTop = placeholder.offset().top;

				        // Get the current scroll of the window.
				        var viewTop = view.scrollTop();

				        // Check to see if the view had scroll down
				        // past the top of the placeholder AND that
				        // the message is not yet fixed.
				        if (
						(viewTop > placeholderTop) &&
						!message.is(".topFrame-fixed")
						) {

				            // The message needs to be fixed. Before
				            // we change its positon, we need to re-
				            // adjust the placeholder height to keep
				            // the same space as the message.
				            //
				            // NOTE: All we're doing here is going
				            // from auto height to explicit height.
				            placeholder.height(
							placeholder.height()
						);

				            // Make the message fixed.
				            message.addClass("topFrame-fixed");


				            // Check to see if the view has scroll back up
				            // above the message AND that the message is
				            // currently fixed.
				        } else if (
						(viewTop <= placeholderTop) &&
						message.is(".topFrame-fixed")
						) {

				            // Make the placeholder height auto again.
				            placeholder.css("height", "auto");

				            // Remove the fixed position class on the
				            // message. This will pop it back into its
				            // static position.
				            message.removeClass("topFrame-fixed");

				            set_main_top($, placeholder, message); // scroll up
				        }
				    }
				    catch (e) { }
				}
			);
    set_main_top($, placeholder, message); // on load

});

function set_main_top($, placeholder, message) {
    try {
        var doc = $(".main");
        // style="margin-top:15px;"
        if (message.height() > placeholder.height()) {
            doc.offset({ top: message.height() });
        }
        else {
            doc.offset({ top: placeholder.height() });
        }
        //alert(message.height())
        //alert(doc.offset().top)
    }
    catch (e) { /* alert(e.message)*/ }
}

jQuery.fn.positionOn = function (element, align) {
    return this.each(function () {
        var target = jQuery(this);
        var position = element.position();

        var x = position.left;
        var y = position.top;

        if (align == 'right') {
            x -= (target.outerWidth() - element.outerWidth());
        } else if (align == 'center') {
            x -= target.outerWidth() / 2 - element.outerWidth() / 2;
        }

        target.css({
            position: 'absolute',
            zIndex: 5000,
            top: y,
            left: x
        });
    });
};

function bindClicktoNewWinks(searchWithInSelector) {
    jQuery(function ($) {

        $(".linkNotifWrapper", searchWithInSelector).each(function (index, elem) {
            var loc = $(elem).attr("location");
            $(elem).click(function () {
                window.location = $(this).attr("location");
                //window.location.reload();
            })
        })

    });
}


function scrollToElem(el, topBar) {// top bar + paddings
    try {
        // jquery object
        if (el.length > 0) {
            //for (var c in el) {
            var top = el.offset().top;
            var left = el.offset().left;
            if (topBar > 0 || topBar < 0)
                top = top - topBar;
            if (top > 0)
                window.scrollTo(left, top);
            //}
        }
    }
    catch (e) { }
}

function scrollToLogin() {
    try {
        jQuery(function ($) {
            var strs = window.location.href.split('#');
            if (strs.length > 1) {
                var login = strs[1];
                var objs = $('div[name~="' + window.location.href.split('#')[1] + '"]');
                scrollToElem(objs, 120);
                //$('html, body').animate({
                //    scrollTop: $('div[name~="' + window.location.href.split('#')[1] + '"]').offset().top
                //}, 800);
            }
        });
    }
    catch (e) { }
}

function writeUserErrorMsg(msg) {
    try {
        setTimeout((function (msg) {
            if (msg != null) {
                var txt = document.createTextNode(msg);
                var o = document.getElementById("userErrorMsg");
                if (!o) {
                    o = document.createElement('div');
                    o.id = "userErrorMsg";
                    o.className = "userErrorMsg"
                    var topFrame = document.getElementById("topFrame");
                    if (topFrame) {
                        topFrame.appendChild(o);
                    }
                    else {
                        var body = document.getElementsByTagName("body")[0];
                        body.insertBefore(o, body.firstChild);
                    }
                }
                try {
                    if (o.hasChildNodes())
                        while (o.childNodes.length >= 1)
                            o.removeChild(o.firstChild);
                }
                catch (e) { }
                o.appendChild(txt);
            }
            if (hideErrorTimer != null) clearTimeout(hideErrorTimer);
            hideError();
        })(msg), 1000);

        var hideErrorTimer;
        function hideError() {
            hideErrorTimer = setTimeout(function () {
                var o = document.getElementById("userErrorMsg");
                if (o) {
                    o.parentElement.removeChild(o);
                }
            }, 150000)
        }
    }
    catch (e) { }
}
function changeColorUsr(obj) {
    var color;
    if (obj.focused || obj.GetValue() != null) {
        color = "black";
    }
    else {
        color = "silver";
    }
    obj.GetInputElement().style.color = color;
}

function changeColorPwd(obj) {
    return;
    var color;
    var type;
    if (obj.focused || obj.GetValue() != null) {
        type = "password";
        color = "black";
    }
    else {
        type = "text";
        color = "silver";
        value = "**********";
    }
    obj.GetInputElement().style.color = color;
    obj.GetInputElement().type = type;
}




function facebook_fnc() {
    try {
        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_GB/all.js#xfbml=1&appId=425026417519142";
            fjs.parentNode.insertBefore(js, fjs);
        } (document, 'script', 'facebook-jssdk'));
    }
    catch (e) { }

}


function SetPCVisible(value, popupControlId, windowIndex) {
    try {
        var popupcontrol = window[popupControlId];
        var wind = function () {
            this.index = -1;
        };
        var obj = new wind();
        obj.index = windowIndex;
        //obj["index"] = windowIndex;
        if (value)
            popupcontrol.ShowWindow(obj);
        else
            popupControl.HideWindow(obj);
    }
    catch (e) { }
}




function deleteCookies() {
    var cookies = document.cookie.split(";");
    document.cookie = "";
    for (var i = 0; i < cookies.length; i++) {
        var cookie = cookies[i];
        var eqPos = cookie.indexOf("=");
        var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
        //var d = new Date(1970, 1, 1)
        //Set_Cookie(name, '', d, '', '', '');
        document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
    }
}

function Set_Cookie(name, value, expires, path, domain, secure) {
    //var today = new Date();
    //today.setTime(today.getTime());
    //if (expires) {
    //    expires = expires * 1000 * 60 * 60 * 24;
    //}
    //var expires_date = new Date(today.getTime() + (expires));     
    var today = new Date();
    today.setTime(today.getTime());

    var expires_date = null;
    if (expires)
        expires_date = expires;
    else
        expires_date = today;

    document.cookie = name + "=" + escape(value) +
((expires) ? ";expires=" + expires_date.toGMTString() : "") +
((path) ? ";path=" + path : "") +
((domain) ? ";domain=" + domain : "") +
((secure) ? ";secure" : "");
}

function Get_Cookie(name) {
    var start = document.cookie.indexOf(name + "=");
    var len = start + name.length + 1;
    if ((!start) &&
(name != document.cookie.substring(0, name.length)))
    { return null; }
    if (start == -1) return null;
    var end = document.cookie.indexOf(";", len);
    if (end == -1) end = document.cookie.length;
    return unescape(document.cookie.substring(len, end));
}

jQuery(function ($) {
    $("body").on("mouseenter",".tt_enabled", function () {
        $(".tooltip").hide();
        $(this).siblings(".tooltip").fadeIn(200)
    }).on("mouseleave", ".tt_enabled", function () {
        $(".tooltip").fadeOut(100)
    });

    $("body").on("mouseenter", ".tt_enabled_distance", function () {
        $(".tooltip_offer").hide();
        $(this).siblings(".tooltip_offer").fadeIn(200)
    }).on("mouseleave", ".tt_enabled_distance", function () {
        $(".tooltip_offer").fadeOut(100)
    });
});


jQuery(function ($) {
    try {
        $('.tooltiptipsy-n').tipsy({ gravity: 'n', html: true, delayIn: 500, delayOut: 500, offset: 5 });
        $('.tooltiptipsy-w').tipsy({ gravity: 'w', html: true, delayIn: 500, delayOut: 500, offset: 5 });
        $('.tooltiptipsy-e').tipsy({ gravity: 'e', html: true, delayIn: 500, delayOut: 500, offset: 5 });
        $('.tooltiptipsy-s').tipsy({ gravity: 's', html: true, delayIn: 500, delayOut: 500, offset: 5 });
    }
    catch (e) { }
});

function runColapse(ClickToExpandString, ClickToCollapseString) {
    jQuery(function ($) {
        $('.pe_box').each(function () {
            var pe_box = this;

            $('.collapsibleHeader', pe_box).click(function () {
                $('.pe_form', pe_box).toggle('fast', function () {
                    // Animation complete.
                    var isVisible = ($('.pe_form', pe_box).css('display') == 'none');
                    if (isVisible) {
                        $('.togglebutton', pe_box).text("+");
                        $('.collapsibleHeader', pe_box).attr("title", ClickToExpandString);
                    }
                    else {
                        $('.togglebutton', pe_box).text("-");
                        $('.collapsibleHeader', pe_box).attr("title", ClickToCollapseString);
                    }
                });

            });
        });
    });
}



function closePopup(popupName) {
    try {
        top.window[popupName].Hide();
        top.window[popupName].DoHideWindow(0);
    }
    catch (e) { }
}



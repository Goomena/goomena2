Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Collections.Generic
Imports System.Configuration
Imports System.IO
Imports System.Xml
Imports System.Xml.Schema
Imports System.Globalization
Imports System.Text
Imports System.Text.RegularExpressions
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Reflection
Imports DevExpress.Web.ASPxHeadline
Imports DevExpress.Web.ASPxSiteMapControl
Imports DevExpress.Web.ASPxClasses
Imports DevExpress.Web.ASPxClasses.Internal
Imports Dating.Server.Core.DLL
Imports ReferralsDLL = Dating.Referrals.Server.Core.DLL
Imports Dating.Server.Datasets.DLL


Partial Public Class BasePage
    Inherits System.Web.UI.Page
    Implements ILanguageDependableContent

    Private webStatistics As New clsWebStatistics()

    Private _IsMale As Boolean?
    Public ReadOnly Property IsMale As Boolean
        Get
            If (Not _IsMale.HasValue) Then
                '_IsMale = (Me.GetCurrentProfile().GenderId = ProfileHelper.gMaleGender.GenderId)
                _IsMale = (Me.SessionVariables.MemberData.GenderId = ProfileHelper.gMaleGender.GenderId)
            End If
            Return _IsMale
        End Get
    End Property


    Private _IsFemale As Boolean?
    Public ReadOnly Property IsFemale As Boolean
        Get
            If (Not _IsFemale.HasValue) Then
                '_IsFemale = (Me.GetCurrentProfile().GenderId = ProfileHelper.gFemaleGender.GenderId)
                _IsFemale = (Me.SessionVariables.MemberData.GenderId = ProfileHelper.gFemaleGender.GenderId)
            End If
            Return _IsFemale
        End Get
    End Property


    Private _UnlimitedMsgID As Integer?
    Public ReadOnly Property UnlimitedMsgID As Integer
        Get
            If (Not _UnlimitedMsgID.HasValue) Then
                _UnlimitedMsgID = 0
                If (Not String.IsNullOrEmpty(Request.QueryString("unmsg"))) Then
                    Long.TryParse(Request.QueryString("unmsg"), _UnlimitedMsgID)
                End If
            End If
            Return _UnlimitedMsgID
        End Get
    End Property




    Public ReadOnly Property IsHTTPS As Boolean
        Get
            Return (Me.Request.Url.Scheme = "https")
        End Get
    End Property

    Public ReadOnly Property ReferrerCustomerID As Integer
        Get
            Return Me.Session("ReferrerCustomerID")
        End Get
    End Property



    Public ReadOnly Property MasterProfileId As Integer
        Get
            Return Me.Session("ProfileID")
        End Get
    End Property


    Protected ReadOnly Property MirrorProfileId As Integer
        Get
            Return Me.Session("MirrorProfileID")
        End Get
    End Property


    Dim _sessVars As clsSessionVariables
    Protected ReadOnly Property SessionVariables As clsSessionVariables
        Get
            If (_sessVars Is Nothing) Then _sessVars = clsSessionVariables.GetCurrent()
            Return _sessVars
        End Get
    End Property


    Private _globalStrings As clsPageData
    Public ReadOnly Property globalStrings As clsPageData
        Get
            If (_globalStrings Is Nothing) Then
                _globalStrings = New clsPageData("GlobalStrings", HttpContext.Current)
            End If
            Return _globalStrings
        End Get
    End Property



    Dim _CMSDBDataContext As CMSDBDataContext
    Protected ReadOnly Property CMSDBDataContext As CMSDBDataContext
        Get
            If (_CMSDBDataContext Is Nothing) Then
                _CMSDBDataContext = New CMSDBDataContext(ModGlobals.ConnectionString)
            End If
            Return _CMSDBDataContext
        End Get
    End Property


    Dim _REF_CMSDBDataContext As ReferralsDLL.CMSDBDataContext
    Protected ReadOnly Property REF_CMSDBDataContext As ReferralsDLL.CMSDBDataContext
        Get
            If (_REF_CMSDBDataContext Is Nothing) Then
                _REF_CMSDBDataContext = New ReferralsDLL.CMSDBDataContext(ModGlobals.ConnectionString)
            End If
            Return _REF_CMSDBDataContext
        End Get
    End Property




    Dim _mineMasterProfile As EUS_Profile
    Protected Function GetCurrentMasterProfile() As EUS_Profile
        Try

            If (_mineMasterProfile Is Nothing) Then
                _mineMasterProfile = Me.CMSDBDataContext.EUS_Profiles.Where(Function(itm) (itm.ProfileID = Me.MirrorProfileId OrElse itm.ProfileID = Me.MasterProfileId) AndAlso itm.IsMaster = True).SingleOrDefault()
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Return _mineMasterProfile
    End Function


    Dim _mineMirrorProfile As EUS_Profile
    Protected Function GetCurrentMirrorProfile() As EUS_Profile
        Try

            If (_mineMirrorProfile Is Nothing) Then
                _mineMirrorProfile = Me.CMSDBDataContext.EUS_Profiles.Where(Function(itm) (itm.ProfileID = Me.MirrorProfileId OrElse itm.ProfileID = Me.MasterProfileId) AndAlso itm.IsMaster = False).SingleOrDefault()
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Return _mineMirrorProfile
    End Function




    Dim _currentProfile As EUS_Profile
    Protected Function GetCurrentProfile(Optional ByVal CheckOnly As Boolean = False) As EUS_Profile
        Try

            If (_currentProfile Is Nothing) Then
                _currentProfile = Me.GetCurrentMasterProfile()
                If (_currentProfile Is Nothing) Then
                    _currentProfile = Me.GetCurrentMirrorProfile()
                End If
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        If (_currentProfile Is Nothing AndAlso Not CheckOnly) Then Throw New ProfileNotFoundException()
        Return _currentProfile
    End Function

    Protected Function IsReferrer() As Boolean
        Dim result As Boolean = (Me.MirrorProfileId > 0 AndAlso
                                Me.MasterProfileId > 0 AndAlso
                                clsNullable.NullTo(Me.GetCurrentProfile().ReferrerParentId) > 0 AndAlso
                                (Me.SessionVariables.MemberData.Status = ProfileStatusEnum.Approved OrElse
                                 Me.SessionVariables.MemberData.Status = ProfileStatusEnum.Updating))

        Return result
    End Function

    Protected Sub ResetEUS_ProfileObject()
        _currentProfile = Nothing
        _mineMirrorProfile = Nothing
        _mineMasterProfile = Nothing
    End Sub

    ' Page PreInit 

    Public Overridable Sub Master_LanguageChanged() Implements ILanguageDependableContent.Master_LanguageChanged

    End Sub


    ' called from public default.aspx
    Protected Overridable Sub Page_PreInit(ByVal sender As Object, ByVal e As EventArgs)

        Try
            If clsCurrentContext.VerifyLogin() = True Then
                If (Request("master") = "inner") Then
                    Me.MasterPageFile = "~/Members/Members2Inner.Master"
                Else
                    'Me.MasterPageFile = "~/Members/Members2.Master"
                End If
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try


        Try

            If Not String.IsNullOrEmpty(Request.QueryString("theme")) Then
                Session("myTheme") = Request.QueryString("theme")
            End If

            Dim themeName = gDefaultThemeName

            If Not String.IsNullOrEmpty(Session("myTheme")) Then
                themeName = Session("myTheme")
            End If



            Dim clientScriptBlock As String = "var DXCurrentThemeCookieName = """ & themeName & """;"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "DXCurrentThemeCookieName", clientScriptBlock, True)
            'Me.Theme = themeName
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try


        Try
            If Not String.IsNullOrEmpty(SessionVariables.myCulture) Then
                SessionVariables.myCulture = gDomainCOM_DefaultLAG
                Dim aCookie As New HttpCookie("otmData")
                aCookie.Values("languagePref") = SessionVariables.myCulture
                aCookie.Expires = System.DateTime.Now.AddDays(21)
                Response.Cookies.Add(aCookie)
            End If
            'gLAG.SetLag(SessionVariables.myCulture)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub



    ' Page Load 
    Protected Overrides Sub OnLoad(ByVal e As EventArgs)

        AppUtils.CheckDomainAndRedirect(Request.Url.Host)

        MyBase.OnLoad(e)

        'If UsingIE7CompatibilityMode Then
        '    Dim ie7CompatModeMeta As LiteralControl = RenderUtils.CreateLiteralControl()
        '    'ie7CompatModeMeta.ID = RenderUtils.IE8CompatibilityMetaID
        '    'ie7CompatModeMeta.Text = RenderUtils.IE8CompatibilityMetaText
        '    Header.Controls.AddAt(0, ie7CompatModeMeta)
        'End If

        '' Scripts
        'ASPxWebControl.RegisterUtilsScript(Me)
        ''RegisterScript("Utilities", "~/Scripts/Utilities.js")
        '' CSS
        ''RegisterCSSLink("~/CSS/styles.css")
        ''RegisterCSSLink("~/CSS/ToolBars.css")
        'If (Not String.IsNullOrEmpty(Me.cssLink_Renamed)) Then
        '    RegisterCSSLink(Me.cssLink_Renamed)
        'End If

        If Not IsPostBack Or Not IsCallback Then
            Dim szURL As String
            Try
                szURL = Request.Url.ToString()
                If Len(szURL) Then
                    Dim szPage As String = System.IO.Path.GetFileName(Request.Url.ToString())
                    Dim iStartParams As Integer = InStr(szPage, "?")
                    If iStartParams > 1 Then
                        szPage = Mid(szPage, 1, iStartParams - 1)
                    End If
                    SessionVariables.Page = szPage
                    SessionVariables.IdentityLoginName = HttpContext.Current.User.Identity.Name
                    'Dim gStat As New clsWebStatistics
                    'gStat.AddNew(Me)
                    Dim refferer As String = webStatistics.GetReferrer(Page.Request("HTTP_REFERER"), Page.Request.Url.Host)
                    webStatistics.SessionID = Page.Session.SessionID
                    webStatistics.Referrer = refferer
                    webStatistics.CustomReferrer = Page.Session("CustomReferrer")
                    webStatistics.IP = Page.Session("IP")
                    webStatistics.Agent = Page.Session("Agent")
                    webStatistics.Page = SessionVariables.Page
                    webStatistics.LoginName = SessionVariables.IdentityLoginName
                    webStatistics.StatCookie = Page.Session("StatCookie")
                    webStatistics.CustomAction = Page.Session("StaticalCustomAction")
                    webStatistics.GEO_COUNTRY_CODE = Page.Session("GEO_COUNTRY_CODE")
                    webStatistics.RequestedUrl = Page.Request.Url.ToString()
                    webStatistics.ProfileID = Me.MasterProfileId

                End If
            Catch ex As Exception
            End Try
        End If


        'Try
        '    If (SessionVariables.DateTimeToRegister IsNot Nothing AndAlso ConfigurationManager.AppSettings("OnRegisterNotifyUserAfterMinutes") > 0) Then
        '        If (Date.UtcNow - SessionVariables.DateTimeToRegister) > TimeSpan.FromMinutes(ConfigurationManager.AppSettings("OnRegisterNotifyUserAfterMinutes")) Then
        '            'Dim _profilerows As New clsProfileRows(Me.MasterProfileId)
        '            'Dim mirrorRow As DSMembers.EUS_ProfilesRow = _profilerows.GetMirrorRow()
        '            'SendNotificationEmailToMember_ToUpdateProfileInfo(mirrorRow)
        '            SessionVariables.DateTimeToRegister = Nothing
        '        End If
        '    End If
        'Catch ex As Exception
        'End Try

        Try
            If (Session("IsAdminLogin") Is Nothing OrElse Session("IsAdminLogin") = False) Then
                If (Me.MasterProfileId > 0) Then DataHelpers.UpdateEUS_Profiles_Activity(Me.MasterProfileId, Session("IP"), True)
            End If
        Catch ex As Exception
        End Try

    End Sub

    ' Functions 
    Protected Overridable Function IsCurrentPage(ByVal oUrl As Object) As Boolean
        If oUrl Is Nothing Then
            Return False
        End If

        Dim result As Boolean = False
        Dim url As String = oUrl.ToString()
        If url.ToLower() = Page.Request.AppRelativeCurrentExecutionFilePath.ToLower() Then
            result = True
        End If
        Return result
    End Function

    Private Sub RegisterScript(ByVal key As String, ByVal url As String)
        Page.ClientScript.RegisterClientScriptInclude(key, Page.ResolveUrl(url))
    End Sub

    Private Sub RegisterCSSLink(ByVal url As String)
        Dim link As New HtmlLink()
        Page.Header.Controls.Add(link)
        link.EnableViewState = False
        link.Attributes.Add("type", "text/css")
        link.Attributes.Add("rel", "stylesheet")
        link.Href = url
    End Sub


    Private Sub Page_Error(sender As Object, e As System.EventArgs) Handles Me.Error
        Try
            Dim exSrv As Exception = Server.GetLastError()
            Dim exSrv2 As String = exSrv.ToString()
            Dim retrying As Boolean = False

            Try
                'use retry param to indicate that retry was done
                If (Context.Request.Url.Query = "?retry" OrElse Context.Request.Url.Query.Contains("&retry")) Then

                Else
                    ' try to load again the same page one time
                    Server.ClearError()

                    Dim siteUrl As String = Request.Url.AbsoluteUri
                    If (siteUrl.IndexOf("?") > -1) Then
                        siteUrl = siteUrl & "&retry"
                    Else
                        siteUrl = siteUrl & "?retry"
                    End If
                    Response.Redirect(siteUrl, False)
                    HttpContext.Current.ApplicationInstance.CompleteRequest()

                    retrying = True
                End If
            Catch ex As Exception
            End Try


            If (exSrv.Message = "The client disconnected.") Then
                'do nothing
            ElseIf (exSrv2.StartsWith("System.Web.HttpException (0x80004005): Failed to load viewstate.")) Then
                'do nothing
            ElseIf (exSrv.Message = "The state information is invalid for this page and might be corrupted.") Then
                'do nothing
            ElseIf (exSrv.Message.StartsWith("Validation of viewstate MAC failed.")) Then
                'do nothing
            Else
                If (retrying) Then
                    WebErrorMessageBox(Me, exSrv, "BasePage.Page_Error (Retrying to reload the page...)")
                Else
                    WebErrorMessageBox(Me, exSrv, "BasePage.Page_Error (No reload)")
                End If
            End If


        Catch ex As Exception
        End Try
    End Sub


    Protected Overridable Sub Page_Unload(sender As Object, e As System.EventArgs) Handles Me.Unload
        If (_CMSDBDataContext IsNot Nothing) Then
            _CMSDBDataContext.Dispose()
        End If

        If (_REF_CMSDBDataContext IsNot Nothing) Then
            _REF_CMSDBDataContext.Dispose()
        End If


        If (webStatistics.SessionID IsNot Nothing) Then
            Try
                ' update clsWebStatistics
                Page.Application.Lock()
                webStatistics.AddNew()
                Page.Application.UnLock()

            Catch ex As Exception
                Page.Application.UnLock()
                WebErrorSendEmail(ex, "BasePage->Page_Unload")
            Finally
            End Try
        End If


    End Sub





    'Sub SendNotificationEmailToMember_ToUpdateProfileInfo(mirrorRow As DSMembers.EUS_ProfilesRow)
    '    Try

    '        Dim toEmail As String = ConfigurationManager.AppSettings("gToEmail")
    '        Dim Content As String = globalStrings.GetCustomString("EmailSendToSupport_MemberNew", "US")

    '        If (mirrorRow.IsAutoApproved) Then
    '            Content = Content.Replace("###YESNO###", "YES")
    '        Else
    '            Content = Content.Replace("###YESNO###", "NO")
    '        End If

    '        Content = Content.Replace("###LOGINNAME###", mirrorRow.LoginName)
    '        Content = Content.Replace("###EMAIL###", mirrorRow.eMail)
    '        Content = Content.Replace("###GENDER###", ProfileHelper.GetGenderString(mirrorRow.GenderId, "US"))


    '        Dim approveUrl As String = ConfigurationManager.AppSettings("gApproveProfileURL")
    '        Dim rejectUrl As String
    '        approveUrl = String.Format(approveUrl, mirrorRow.LoginName)
    '        rejectUrl = approveUrl & "&reject=1"

    '        Content = Content.Replace("###APPROVEPROFILEURL###", approveUrl)
    '        Content = Content.Replace("###REJECTPROFILEURL###", rejectUrl)

    '        clsMyMail.SendMail(toEmail, "New user REGISTRATION on GOOMENA.", Content, True)

    '    Catch ex As Exception
    '        WebErrorMessageBox(Me, ex, "")
    '    End Try

    'End Sub



    Public Function GetLag() As String
        Dim str As String = Nothing
        Try
            If (AppUtils.IsSupportedLAG(HttpContext.Current.Session("LAGID"))) Then
                str = HttpContext.Current.Session("LAGID")
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        If (str Is Nothing) Then
            If Request.Url.Host.EndsWith(".gr", StringComparison.OrdinalIgnoreCase) Then
                'goomena.gr
                str = gDomainGR_DefaultLAG
            Else

                If Request.Url.Host.EndsWith(".com", StringComparison.OrdinalIgnoreCase) Then
                    'goomena.com
                    If (str Is Nothing) Then str = gDomainCOM_DefaultLAG
                End If

            End If
        End If

        If (str Is Nothing) Then str = gDomainCOM_DefaultLAG

        Return str
    End Function




    Public Function GetHasCommunication(OtherProfileID As Integer) As Boolean
        Dim _HasCommunication As Boolean = False
        If (OtherProfileID = 1 OrElse Me.IsFemale) Then
            _HasCommunication = True
        Else
            _HasCommunication = clsUserDoes.HasCommunication(OtherProfileID, Me.MasterProfileId)
        End If
        Return _HasCommunication
    End Function


    Public Shared Function ReplaceCommonTookens(Input As String, Optional LoginName As String = "")
        If (Not String.IsNullOrEmpty(Input)) Then

            If (Input.IndexOf("###LOGINNAME###") > -1) Then
                Input = Input.Replace("###LOGINNAME###", LoginName)
            End If


            If (Input.IndexOf("###UNLOCK_MESSAGE_SEND_CRD###") > -1) Then
                Input = Input.Replace("###UNLOCK_MESSAGE_SEND_CRD###", ProfileHelper.Config_UNLOCK_MESSAGE_SEND_CREDITS)
            End If

            If (Input.IndexOf("###UNLOCK_MESSAGE_READ_CRD###") > -1) Then
                Input = Input.Replace("###UNLOCK_MESSAGE_READ_CRD###", ProfileHelper.Config_UNLOCK_MESSAGE_READ_CREDITS)
            End If

            If (Input.IndexOf("###UNLOCK_CONVERSATIONCRD###") > -1) Then
                Input = Input.Replace("###UNLOCK_CONVERSATIONCRD###", ProfileHelper.Config_UNLOCK_CONVERSATION_CREDITS)
            End If

        End If
        Return Input
    End Function



    Public Shared Function OnMoreInfoClickFunc(input As String, contentUrl As String, headerText As String) As String
        input = WhatIsIt.OnMoreInfoClickFunc(input, contentUrl, headerText)
        Return input
    End Function

End Class


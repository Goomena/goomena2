﻿Imports Dating.Server.Core.DLL
Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Datasets.DLL.DSMembers

Public Class BaseUserControl
    Inherits System.Web.UI.UserControl
    Implements ILanguageDependableContent


    Private _IsMale As Boolean?
    Public ReadOnly Property IsMale As Boolean
        Get
            If (Not _IsMale.HasValue) Then
                '_IsMale = (Me.GetCurrentProfile().GenderId = ProfileHelper.gMaleGender.GenderId)
                _IsMale = (Me.SessionVariables.MemberData.GenderId = ProfileHelper.gMaleGender.GenderId)
            End If
            Return _IsMale
        End Get
    End Property


    Private _IsFemale As Boolean?
    Public ReadOnly Property IsFemale As Boolean
        Get
            If (Not _IsFemale.HasValue) Then
                '_IsFemale = (Me.GetCurrentProfile().GenderId = ProfileHelper.gFemaleGender.GenderId)
                _IsFemale = (Me.SessionVariables.MemberData.GenderId = ProfileHelper.gFemaleGender.GenderId)
            End If
            Return _IsFemale
        End Get
    End Property



    Public ReadOnly Property IsHTTPS As Boolean
        Get
            Return (Me.Request.Url.Scheme = "https")
        End Get
    End Property

    Protected ReadOnly Property MasterProfileId As Integer
        Get
            Return Me.Session("ProfileID")
        End Get
    End Property


    Protected ReadOnly Property MirrorProfileId As Integer
        Get
            Return Me.Session("MirrorProfileID")
        End Get
    End Property


    Protected ReadOnly Property ProfileLoginName As String
        Get
            Return Me.SessionVariables.MemberData.LoginName
        End Get
    End Property

    Dim _sessVars As clsSessionVariables
    Protected ReadOnly Property SessionVariables As clsSessionVariables
        Get
            If (_sessVars Is Nothing) Then _sessVars = clsSessionVariables.GetCurrent()
            Return _sessVars
        End Get
    End Property


    Private _globalStrings As clsPageData
    Public ReadOnly Property globalStrings As clsPageData
        Get
            If (_globalStrings Is Nothing) Then
                _globalStrings = New clsPageData("GlobalStrings", HttpContext.Current)
            End If
            Return _globalStrings
        End Get
    End Property



    Dim _CMSDBDataContext As CMSDBDataContext
    Protected ReadOnly Property CMSDBDataContext As CMSDBDataContext
        Get
            If (_CMSDBDataContext Is Nothing) Then
                _CMSDBDataContext = New CMSDBDataContext(ConfigurationManager.ConnectionStrings("AppDBconnectionString").ConnectionString)
            End If
            Return _CMSDBDataContext
        End Get
    End Property


    Dim _mineMasterProfile As EUS_Profile
    Protected Function GetCurrentMasterProfile() As EUS_Profile

        If (_mineMasterProfile Is Nothing) And Me.MasterProfileId > 0 Then
            _mineMasterProfile = Me.CMSDBDataContext.EUS_Profiles.Where(Function(itm) itm.ProfileID = Me.MasterProfileId AndAlso itm.IsMaster = True).SingleOrDefault()
        End If

        Return _mineMasterProfile
    End Function


    Dim _mineMirrorProfile As EUS_Profile
    Protected Function GetCurrentMirrorProfile() As EUS_Profile

        If (_mineMirrorProfile Is Nothing) And Me.MirrorProfileId > 0 Then
            _mineMirrorProfile = Me.CMSDBDataContext.EUS_Profiles.Where(Function(itm) itm.ProfileID = Me.MirrorProfileId AndAlso itm.IsMaster = False).SingleOrDefault()
        End If

        Return _mineMirrorProfile
    End Function


    Dim _currentProfile As EUS_Profile
    Protected Function GetCurrentProfile(Optional ByVal CheckOnly As Boolean = False) As EUS_Profile
        Try

            If (_currentProfile Is Nothing) Then
                _currentProfile = Me.GetCurrentMasterProfile()
                If (_currentProfile Is Nothing) Then
                    _currentProfile = Me.GetCurrentMirrorProfile()
                End If

                ' not approved member
                If (_currentProfile Is Nothing) Then
                    _currentProfile = Me.CMSDBDataContext.EUS_Profiles.Where(Function(itm) itm.ProfileID = Me.MasterProfileId AndAlso itm.IsMaster = False).SingleOrDefault()
                End If
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        If (_currentProfile Is Nothing AndAlso Not CheckOnly) Then Throw New ProfileNotFoundException()
        Return _currentProfile
    End Function

    Public Function GetCurrency() As String
        Dim currency As String = Nothing
        If (Me.MasterProfileId > 1 AndAlso Me.GetCurrentProfile(True) IsNot Nothing) Then
            currency = clsPricing.GetCurrency(Me.GetCurrentProfile().Country)
        Else
            currency = clsPricing.GetCurrency(HttpContext.Current.Session("GEO_COUNTRY_CODE"))
        End If
        Return currency
    End Function

    Public Overridable Sub Master_LanguageChanged() Implements ILanguageDependableContent.Master_LanguageChanged

    End Sub


    Protected Overridable Sub Page_Unload(sender As Object, e As System.EventArgs) Handles Me.Unload
        If (Not _CMSDBDataContext Is Nothing) Then
            _CMSDBDataContext.Dispose()
        End If
    End Sub



    Public Function GetLag() As String
        Dim str As String = Nothing
        Try
            If (AppUtils.IsSupportedLAG(HttpContext.Current.Session("LAGID"))) Then
                str = HttpContext.Current.Session("LAGID")
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        If (str Is Nothing) Then
            If Request.Url.Host.EndsWith(".gr", StringComparison.OrdinalIgnoreCase) Then
                'goomena.gr
                str = gDomainGR_DefaultLAG
            Else

                If Request.Url.Host.EndsWith(".com", StringComparison.OrdinalIgnoreCase) Then
                    'goomena.com
                    If (str Is Nothing) Then str = gDomainCOM_DefaultLAG
                End If

            End If
        End If

        If (str Is Nothing) Then str = gDomainCOM_DefaultLAG

        Return str
    End Function


End Class

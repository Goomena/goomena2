﻿
Public Class clsUserListItem
    Public Property ProfileID As Integer
    Public Property MirrorProfileID As Integer
    Public Property LoginName As String
    Public Property Genderid As Integer
    Public Property ProfileViewUrl As String

    Public Property ImageFileName As String
    Public Property ImageUrl As String
    Public Property ImageThumbUrl As String
    Public Property ImageUploadDate As String

    Public Property CheckBoxText As String
    Public Property ShowCheckBox As Boolean
End Class


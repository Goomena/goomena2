﻿Imports DevExpress.Web.ASPxMenu
Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Core.DLL

Public Class clsMenuHelper



    Private Shared Function GetMenuItemsList(menu As ASPxMenu,
                                       position As String,
                                       itemsCount As Integer) As List(Of dsMenu.SiteMenuItemsRow)

        Dim visibleOnAdmistratorUserRole As Boolean?,
            visibleOnAfficateUserRole As Boolean?,
            visibleOnMemberUserRole As Boolean?,
            visibleOnPublicArea As Boolean?,
            VisibleOnResellerUserRole As Boolean?

        Dim q As List(Of dsMenu.SiteMenuItemsRow) = Nothing

        Try
            Dim cacheInst As Caching = Caching.Current
            

                q = (From i In cacheInst.SiteMenuItems Where _
                     i.Position.ToLower = position.ToLower AndAlso _
                     (i.VisibleOnAdmistratorUserRole = visibleOnAdmistratorUserRole OrElse visibleOnAdmistratorUserRole Is Nothing) AndAlso _
                     (i.VisibleOnAfficateUserRole = visibleOnAfficateUserRole OrElse visibleOnAfficateUserRole Is Nothing) AndAlso _
                     (i.VisibleOnMemberUserRole = visibleOnMemberUserRole OrElse visibleOnMemberUserRole Is Nothing) AndAlso _
                     (i.VisibleOnPublicArea = visibleOnPublicArea OrElse visibleOnPublicArea Is Nothing) AndAlso _
                     (i.VisibleOnResellerUserRole = VisibleOnResellerUserRole OrElse VisibleOnResellerUserRole Is Nothing) _
                     Where i.ParrentSiteMenuItemID = 0 AndAlso i.IsActive = True _
                     Order By i.SortNumber _
                     Select i).ToList()


        Catch ex As System.Exception
            WebErrorSendEmail(ex, "Creating menu " & menu.ID & ". Retrying...")
        End Try

        If (q Is Nothing) Then
            Try
                Caching.Current.Reload()
            Catch
            End Try

            Try
                Dim cacheInst As Caching = New Caching()

                q = (From i In cacheInst.SiteMenuItems Where _
                     i.Position.ToLower = position.ToLower AndAlso _
                     (i.VisibleOnAdmistratorUserRole = visibleOnAdmistratorUserRole OrElse visibleOnAdmistratorUserRole Is Nothing) AndAlso _
                     (i.VisibleOnAfficateUserRole = visibleOnAfficateUserRole OrElse visibleOnAfficateUserRole Is Nothing) AndAlso _
                     (i.VisibleOnMemberUserRole = visibleOnMemberUserRole OrElse visibleOnMemberUserRole Is Nothing) AndAlso _
                     (i.VisibleOnPublicArea = visibleOnPublicArea OrElse visibleOnPublicArea Is Nothing) AndAlso _
                     (i.VisibleOnResellerUserRole = VisibleOnResellerUserRole OrElse VisibleOnResellerUserRole Is Nothing) _
                     Where i.ParrentSiteMenuItemID = 0 AndAlso i.IsActive = True _
                     Order By i.SortNumber _
                     Select i).ToList()


            Catch ex As System.Exception
                WebErrorSendEmail(ex, "Creating menu " & menu.ID & ". Failed second time.")
            End Try
        End If

        Return q
    End Function


    Public Shared Sub PopulateMenuData(menu As ASPxMenu,
                                       position As String)
        PopulateMenuData(menu, position, -1)
    End Sub

    Public Shared Sub PopulateMenuData(menu As ASPxMenu,
                                       position As String,
                                       itemsCount As Integer)

        If menu Is Nothing Then Return
        Dim visibleOnAdmistratorUserRole As Boolean?,
            visibleOnAfficateUserRole As Boolean?,
            visibleOnMemberUserRole As Boolean?,
            visibleOnPublicArea As Boolean?,
            VisibleOnResellerUserRole As Boolean?

        Try

            menu.Items.Clear()
            Dim q As List(Of dsMenu.SiteMenuItemsRow) = GetMenuItemsList(menu, position, itemsCount)

            Dim cnt As Integer
            For cnt = 0 To q.Count - 1

                Dim _text As String = ""
                Dim _toolTip As String = ""

                With q(cnt)

                    ' check the item is in list already
                    If menu.Items.FindByName(.SiteMenuItemID) IsNot Nothing Then
                        Continue For
                    End If


                    ' by default use english
                    _text = .US
                    _toolTip = .ToolTipUS

                    Select Case HttpContext.Current.Session("LagID")
                        Case "GR"
                            If Not .IsGRNull() AndAlso .GR.Length > 0 Then
                                _text = .GR
                                _toolTip = .ToolTipGR
                            End If
                        Case "HU"
                            If Not .IsHUNull() AndAlso .HU.Length > 0 Then
                                _text = .HU
                                _toolTip = .ToolTipHU
                            End If
                        Case "ES"
                            If Not .IsESNull() AndAlso .ES.Length > 0 Then
                                _text = .ES
                                _toolTip = .ToolTipES
                            End If
                        Case "DE"
                            If Not .IsDENull() AndAlso .DE.Length > 0 Then
                                _text = .DE
                                _toolTip = .ToolTipDE
                            End If
                        Case "RO"
                            If Not .IsRONull() AndAlso .RO.Length > 0 Then
                                _text = .RO
                                _toolTip = .ToolTipRO
                            End If
                        Case "TU"
                            If Not .IsTUNull() AndAlso .TU.Length > 0 Then
                                _text = .TU
                                _toolTip = .ToolTipTU
                            End If
                        Case "IT"
                            If Not .IsITNull() AndAlso .IT.Length > 0 Then
                                _text = .IT
                                _toolTip = .ToolTipIT
                            End If
                        Case "IL"
                            If Not .IsILNull() AndAlso .IL.Length > 0 Then
                                _text = .IL
                                _toolTip = .ToolTipIL
                            End If
                        Case "FR"
                            If Not .IsFRNull() AndAlso .FR.Length > 0 Then
                                _text = .FR
                                _toolTip = .ToolTipFR
                            End If
                        Case "AL"
                            If Not .IsALNull() AndAlso .AL.Length > 0 Then
                                _text = .AL
                                _toolTip = .ToolTipAL
                            End If
                        Case "TR"
                            If Not .IsTRNull() AndAlso .TR.Length > 0 Then
                                _text = .TR
                                _toolTip = .ToolTipTR
                            End If
                    End Select

                    If _text.Length > 0 OrElse (Not String.IsNullOrEmpty(.ImageURL)) Then
                        Dim mi As New DevExpress.Web.ASPxMenu.MenuItem(_text, .SiteMenuItemID, .ImageURL, .NavigateURL)
                        mi.ToolTip = _toolTip
                        Dim isVisible As Boolean = True

                        Try

                            If (.US.Contains("[BLANK]")) Then
                                mi.Target = "_blank"
                                mi.Text = mi.Text.Replace("[BLANK]", "")
                            End If

                            Dim sesVars As clsSessionVariables = clsSessionVariables.GetCurrent()
                            Dim memberCountry As String = ""
                            If (sesVars IsNot Nothing AndAlso sesVars.MemberData IsNot Nothing AndAlso sesVars.MemberData.Country IsNot Nothing) Then
                                memberCountry = sesVars.MemberData.Country
                            End If
                            Dim GEO_COUNTRY_CODE As String = HttpContext.Current.Session("GEO_COUNTRY_CODE")

                            If (.US.Contains("[BLOG]")) Then
                                mi.Text = mi.Text.Replace("[BLOG]", "")
                                isVisible = False
                                If ("GR" = memberCountry) Then
                                    isVisible = True
                                ElseIf (String.IsNullOrEmpty(memberCountry) AndAlso "GR" = GEO_COUNTRY_CODE) Then
                                    isVisible = True
                                End If
                            End If

                            If (.US.Contains("[BLOG-TR]")) Then
                                mi.Text = mi.Text.Replace("[BLOG-TR]", "")
                                isVisible = False
                                If ("TR" = memberCountry) Then
                                    isVisible = True
                                ElseIf (String.IsNullOrEmpty(memberCountry) AndAlso "TR" = GEO_COUNTRY_CODE) Then
                                    isVisible = True
                                End If
                            End If

                            If (.US.Contains("[BLOG-ES]")) Then
                                mi.Text = mi.Text.Replace("[BLOG-ES]", "")
                                isVisible = False
                                If ("ES" = memberCountry) Then
                                    isVisible = True
                                ElseIf (String.IsNullOrEmpty(memberCountry) AndAlso "ES" = GEO_COUNTRY_CODE) Then
                                    isVisible = True
                                End If
                            End If

                        Catch ex As Exception
                            WebErrorMessageBox(menu.Page, ex, "")
                        End Try

                        If (isVisible) Then
                            Dim url As String = .NavigateURL 'AdoptLanguageURL(.NavigateURL, _text)
                            mi.NavigateUrl = url
                            menu.Items.Add(mi)
                        End If
                    End If


                End With
            Next

            'now load all sub menus for loaded root menus
            Dim qSub As List(Of dsMenu.SiteMenuItemsRow) = (From i In Caching.Current.SiteMenuItems Where _
                    i.Position.ToLower = position.ToLower AndAlso _
                    (i.VisibleOnAdmistratorUserRole = visibleOnAdmistratorUserRole OrElse visibleOnAdmistratorUserRole Is Nothing) AndAlso _
                    (i.VisibleOnAfficateUserRole = visibleOnAfficateUserRole OrElse visibleOnAfficateUserRole Is Nothing) AndAlso _
                    (i.VisibleOnMemberUserRole = visibleOnMemberUserRole OrElse visibleOnMemberUserRole Is Nothing) AndAlso _
                    (i.VisibleOnPublicArea = visibleOnPublicArea OrElse visibleOnPublicArea Is Nothing) AndAlso _
                    (i.VisibleOnResellerUserRole = VisibleOnResellerUserRole OrElse VisibleOnResellerUserRole Is Nothing) _
                    Where i.ParrentSiteMenuItemID <> 0 AndAlso i.IsActive = True _
                    Order By i.SortNumber _
                    Select i).ToList()


            'For Each row As dsMenu.SiteMenuItemsRow In qSub
            For cnt = 0 To qSub.Count - 1
                With qSub(cnt)

                    Dim menuItem As MenuItem = menu.Items.FindByName(.ParrentSiteMenuItemID)
                    If menuItem IsNot Nothing Then

                        ' check the item is in list already
                        If menuItem.Items.FindByName(.SiteMenuItemID) IsNot Nothing Then
                            Continue For
                        End If

                        Select Case HttpContext.Current.Session("LagID")
                            Case "US"
                                If .US.Length > 0 Then
                                    menuItem.Items.Add(.US, .SiteMenuItemID, .ImageURL, .NavigateURL)
                                    menuItem.Items.FindByName(.SiteMenuItemID).ToolTip = .ToolTipUS
                                Else
                                    GoTo defSub
                                End If
                            Case "GR"
                                If .GR.Length > 0 Then
                                    menuItem.Items.Add(.GR, .SiteMenuItemID, .ImageURL, .NavigateURL)
                                    menuItem.Items.FindByName(.SiteMenuItemID).ToolTip = .ToolTipGR
                                Else
                                    GoTo defSub
                                End If
                            Case "HU"
                                If .HU.Length > 0 Then
                                    menuItem.Items.Add(.HU, .SiteMenuItemID, .ImageURL, .NavigateURL)
                                    menuItem.Items.FindByName(.SiteMenuItemID).ToolTip = .ToolTipHU
                                Else
                                    GoTo defSub
                                End If
                            Case "ES"
                                If .ES.Length > 0 Then
                                    menuItem.Items.Add(.ES, .SiteMenuItemID, .ImageURL, .NavigateURL)
                                    menuItem.Items.FindByName(.SiteMenuItemID).ToolTip = .ToolTipES
                                Else
                                    GoTo defSub
                                End If
                            Case "DE"
                                If .DE.Length > 0 Then
                                    menuItem.Items.Add(.DE, .SiteMenuItemID, .ImageURL, .NavigateURL)
                                    menuItem.Items.FindByName(.SiteMenuItemID).ToolTip = .ToolTipDE
                                Else
                                    GoTo defSub
                                End If
                            Case "RO"
                                If .RO.Length > 0 Then
                                    menuItem.Items.Add(.RO, .SiteMenuItemID, .ImageURL, .NavigateURL)
                                    menuItem.Items.FindByName(.SiteMenuItemID).ToolTip = .ToolTipRO
                                Else
                                    GoTo defSub
                                End If
                            Case "TU"
                                If .TU.Length > 0 Then
                                    menuItem.Items.Add(.TU, .SiteMenuItemID, .ImageURL, .NavigateURL)
                                    menuItem.Items.FindByName(.SiteMenuItemID).ToolTip = .ToolTipTU
                                Else
                                    GoTo defSub
                                End If
                            Case "IT"
                                If .IT.Length > 0 Then
                                    menuItem.Items.Add(.IT, .SiteMenuItemID, .ImageURL, .NavigateURL)
                                    menuItem.Items.FindByName(.SiteMenuItemID).ToolTip = .ToolTipIT
                                Else
                                    GoTo defSub
                                End If
                            Case "IL"
                                If .IL.Length > 0 Then
                                    menuItem.Items.Add(.IL, .SiteMenuItemID, .ImageURL, .NavigateURL)
                                    menuItem.Items.FindByName(.SiteMenuItemID).ToolTip = .ToolTipIL
                                Else
                                    GoTo defSub
                                End If
                            Case "FR"
                                If .FR.Length > 0 Then
                                    menuItem.Items.Add(.FR, .SiteMenuItemID, .ImageURL, .NavigateURL)
                                    menuItem.Items.FindByName(.SiteMenuItemID).ToolTip = .ToolTipFR
                                Else
                                    GoTo defSub
                                End If
                            Case "AL"
                                If Not .IsALNull() AndAlso .AL.Length > 0 Then
                                    menuItem.Items.Add(.AL, .SiteMenuItemID, .ImageURL, .NavigateURL)
                                    menuItem.Items.FindByName(.SiteMenuItemID).ToolTip = .ToolTipAL
                                Else
                                    GoTo defSub
                                End If
                            Case "TR"
                                If .TR.Length > 0 Then
                                    menuItem.Items.Add(.TR, .SiteMenuItemID, .ImageURL, .NavigateURL)
                                    menuItem.Items.FindByName(.SiteMenuItemID).ToolTip = .ToolTipTR
                                Else
                                    GoTo defSub
                                End If
                            Case Else 'default to US
                                GoTo defSub
                        End Select

                        GoTo skipSub 'skip default load for US
defSub:
                        If .US.Length > 0 Then
                            menuItem.Items.Add(.US, .SiteMenuItemID, .ImageURL, .NavigateURL)
                            menuItem.Items.FindByName(.SiteMenuItemID).ToolTip = .ToolTipUS
                        End If
skipSub:
                        'do nothing
                    End If
                End With
            Next

        Catch ex As System.NullReferenceException
            Try
                Caching.Current.Reload()
                HttpContext.Current.Response.Redirect(HttpContext.Current.Request.Url.AbsoluteUri, False)
            Catch
            End Try

            WebErrorMessageBox(menu.Page, ex, "")
        Catch ex As Exception
            WebErrorMessageBox(menu.Page, ex, "")
        End Try

    End Sub





End Class

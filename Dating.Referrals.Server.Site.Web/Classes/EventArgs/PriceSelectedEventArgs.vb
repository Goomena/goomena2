﻿Public Class PriceSelectedEventArgs
    Inherits System.EventArgs

    Public Property Credits As String
    Public Property Price As Integer
    Public Property Duration As Integer

    Public Sub New()
        MyBase.New()
    End Sub

    Public Sub New(ByVal _Credits As Integer, ByVal _Price As Integer, ByVal _Duration As Integer)
        MyBase.New()
        Credits = _Credits & "Credits"
        Price = _Price
        Duration = _Duration
    End Sub

End Class

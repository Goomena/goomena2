﻿Imports Dating.Referrals.Server.Core.DLL
Imports ReferralsDLL = Dating.Referrals.Server.Core.DLL

Public Class clsLogin

    Public Function PerfomLogin(DataRecordLogin As clsDataRecordLoginMember) As clsDataRecordLoginMemberReturn
        Dim DataRecordLoginReturn As New clsDataRecordLoginMemberReturn
        DataRecordLoginReturn.IsValid = False
        DataRecordLoginReturn.HasErrors = False

        Try
            Dim ds As DSReferrers = DataHelpers.GetREF_ReferrerProfiles_ByLoginPass(DataRecordLogin.LoginName, DataRecordLogin.Password)
            Dim REF_ReferrerProfilesDataTable As DSReferrers.REF_ReferrerProfilesDataTable = ds.REF_ReferrerProfiles
            If REF_ReferrerProfilesDataTable.Rows.Count > 0 Then

                Dim updateMember As Boolean = False
                Dim rowIndex As Integer = 0
                For rowIndex = 0 To (REF_ReferrerProfilesDataTable.Rows.Count - 1)
                    'While rowIndex < REF_ReferrerProfilesDataTable.Rows.Count


                    Dim EUS_MembersRow As DSReferrers.REF_ReferrerProfilesRow = REF_ReferrerProfilesDataTable.Rows(rowIndex)
                    If (DataRecordLogin.AllowReferral) Then
                        If (EUS_MembersRow.IsReferrerParentIdNull() OrElse EUS_MembersRow.ReferrerParentId < 1) Then
                            DataRecordLoginReturn.Message = "Error Login Or Password"
                            DataRecordLoginReturn.HasErrors = True

                            Return DataRecordLoginReturn
                        End If
                    End If


                    If (EUS_MembersRow.Status <> ProfileStatusEnum.Deleted) Then
                        DataRecordLoginReturn.Fill(EUS_MembersRow)

                        EUS_MembersRow.LastLoginDateTime = DateTime.UtcNow
                        EUS_MembersRow.LastLoginIP = HttpContext.Current.Request.Params("REMOTE_ADDR")
                        ' EUS_MembersRow.LastLoginGEOInfos, 
                    Else
                        DataRecordLoginReturn.HasErrors = True
                        DataRecordLoginReturn.Message = ProfileStatusEnum.Deleted.ToString()

                    End If
                Next

                If (updateMember) Then
                    DataHelpers.UpdateREF_ReferrerProfiles(ds)
                End If

            Else
                DataRecordLoginReturn.Message = "Error Login Or Password"
            End If

            ' log last login data
            If (DataRecordLoginReturn.ProfileID > 0 AndAlso DataRecordLogin.RecordLastLogin <> -11) Then
                DataHelpers.UpdateREF_ReferrerProfiles_LoginData(DataRecordLoginReturn.ProfileID, HttpContext.Current.Request.Params("REMOTE_ADDR"), HttpContext.Current.Session("GEO_COUNTRY_CODE"))
            End If

        Catch ex As Exception
            DataRecordLoginReturn.HasErrors = True
            DataRecordLoginReturn.Message = ex.Message
        End Try
        Return DataRecordLoginReturn
    End Function



    Public Function PerfomSiteLogin(loginName As String, pass As String, ByVal rememberUserName As Boolean, AllowReferral As Boolean) As clsDataRecordLoginMemberReturn
        Return PerfomSiteLogin(loginName, pass, rememberUserName, 0, AllowReferral)
    End Function



    Public Function PerfomSiteLogin(loginName As String, pass As String, ByVal rememberUserName As Boolean, recordLogin As Integer, AllowReferral As Boolean) As clsDataRecordLoginMemberReturn
        Dim DataRecordLoginReturn As New clsDataRecordLoginMemberReturn
        DataRecordLoginReturn.IsValid = False
        DataRecordLoginReturn.HasErrors = False

        Try
            'Dim core As New clsCore

            Dim DataRecordLogin As New clsDataRecordLoginMember()
            DataRecordLogin.LoginName = loginName
            DataRecordLogin.Password = pass
            DataRecordLogin.RecordLastLogin = recordLogin
            DataRecordLogin.AllowReferral = AllowReferral

            DataRecordLoginReturn = Me.PerfomLogin(DataRecordLogin)

            If (HttpContext.Current.Session("SessionVariables") Is Nothing) Then
                HttpContext.Current.Session("SessionVariables") = New Dating.Server.Core.DLL.clsSessionVariables()
            End If

            Dim sesVars As Dating.Server.Core.DLL.clsSessionVariables = DirectCast(HttpContext.Current.Session("SessionVariables"), Dating.Server.Core.DLL.clsSessionVariables)
            sesVars.MemberData.Status = DataRecordLoginReturn.Status
            sesVars.MemberData.ReferrerParentID = DataRecordLoginReturn.ReferrerParentID
            sesVars.MemberData.Birthday = DataRecordLoginReturn.Birthday
            sesVars.MemberData.CustomReferrer = DataRecordLoginReturn.CustomReferrer
            sesVars.MemberData.ErrorCode = DataRecordLoginReturn.ErrorCode
            sesVars.MemberData.ExtraMessage = DataRecordLoginReturn.ExtraMessage
            sesVars.MemberData.FirstName = DataRecordLoginReturn.FirstName
            sesVars.MemberData.GenderId = DataRecordLoginReturn.GenderId
            sesVars.MemberData.HasErrors = DataRecordLoginReturn.HasErrors
            'sesVars.MemberData.HASH = DataRecordLoginReturn.HASH
            sesVars.MemberData.IsValid = DataRecordLoginReturn.IsValid
            sesVars.MemberData.LAGID = DataRecordLoginReturn.LAGID
            sesVars.MemberData.LastName = DataRecordLoginReturn.LastName
            sesVars.MemberData.latitude = DataRecordLoginReturn.latitude
            sesVars.MemberData.LoginName = DataRecordLoginReturn.LoginName
            sesVars.MemberData.longitude = DataRecordLoginReturn.longitude
            sesVars.MemberData.LookingFor_ToMeetFemaleID = DataRecordLoginReturn.LookingFor_ToMeetFemaleID
            sesVars.MemberData.LookingFor_ToMeetMaleID = DataRecordLoginReturn.LookingFor_ToMeetMaleID
            sesVars.MemberData.Message = DataRecordLoginReturn.Message
            sesVars.MemberData.MirrorProfileID = DataRecordLoginReturn.MirrorProfileID
            sesVars.MemberData.ProfileID = DataRecordLoginReturn.ProfileID
            sesVars.MemberData.Role = DataRecordLoginReturn.Role
            'sesVars.MemberData.Token = DataRecordLoginReturn.Token
            sesVars.MemberData.Zip = DataRecordLoginReturn.Zip
            sesVars.MemberData._Region = DataRecordLoginReturn._Region


            HttpContext.Current.Session("ReferrerCustomerID") = DataRecordLoginReturn.ReferrerCustomerID
            HttpContext.Current.Session("LagID") = DataRecordLoginReturn.LAGID
            HttpContext.Current.Session("ProfileID") = DataRecordLoginReturn.ProfileID
            HttpContext.Current.Session("MirrorProfileID") = DataRecordLoginReturn.MirrorProfileID
            HttpContext.Current.Session("LoginName") = DataRecordLoginReturn.LoginName


            If (HttpContext.Current.Session("LagID") Is Nothing) Then
                clsCurrentContext.SetLAGID()
            End If

            If DataRecordLoginReturn.IsValid Then

                'Create Form Authentication ticket
                Dim ticket As New FormsAuthenticationTicket(1, DataRecordLoginReturn.LoginName, DateTime.Now, DateTime.Now.AddDays(5), rememberUserName, DataRecordLoginReturn.Role, FormsAuthentication.FormsCookiePath)


                'For security reasons we may hash the cookies
                Dim hashCookies As String = FormsAuthentication.Encrypt(ticket)
                Dim cookie As New HttpCookie(FormsAuthentication.FormsCookieName, hashCookies)

                cookie.Path = FormsAuthentication.FormsCookiePath()
                If rememberUserName Then
                    cookie.Expires = ticket.Expiration
                End If

                cookie.Domain = Nothing

                'Dim CookiesDomain1 As String = ConfigurationManager.AppSettings("CookiesDomain1")
                'Dim CookiesDomain2 As String = ConfigurationManager.AppSettings("CookiesDomain2")

                'If (HttpContext.Current.Request.Url.Host.EndsWith(CookiesDomain1.TrimStart("."c))) Then
                '    cookie.Domain = CookiesDomain1
                'ElseIf (HttpContext.Current.Request.Url.Host.EndsWith(CookiesDomain2.TrimStart("."c))) Then
                '    cookie.Domain = CookiesDomain2
                'End If


                'add the cookie to user browser
                HttpContext.Current.Response.Cookies.Remove(cookie.Name)
                HttpContext.Current.Response.Cookies.Add(cookie)

                HttpContext.Current.Session("DataRecordLoginReturn") = DataRecordLoginReturn

            End If

        Catch ex As Exception
            DataRecordLoginReturn.HasErrors = True
            DataRecordLoginReturn.Message = ex.Message
        End Try


        Return DataRecordLoginReturn
    End Function


    Public Function PerfomAdminLogin(logon_customerParam As String) As clsDataRecordLoginMemberReturn
        Dim dc As CMSDBDataContext = Nothing
        Try
            dc = New CMSDBDataContext(ModGlobals.ConnectionString)
            Dim customerId As String() = logon_customerParam.Split("_"c)
            Dim name As String = (From itm In dc.REF_ReferrerProfiles
                                         Where itm.CustomerID = customerId(1)
                                         Select itm.LoginName).FirstOrDefault()

            Return PerfomRelogin(name, False, -11)
        Catch ex As Exception
        Finally
            If (dc IsNot Nothing) Then dc.Dispose()
        End Try

        Return (Nothing)
    End Function


    Public Function PerfomRelogin(loginName As String, ByVal rememberUserName As Boolean) As clsDataRecordLoginMemberReturn
        Return PerfomRelogin(loginName, rememberUserName, 0)
    End Function


    Public Function PerfomRelogin(loginName As String, ByVal rememberUserName As Boolean, recordLogin As Integer) As clsDataRecordLoginMemberReturn
        Dim DataRecordLoginReturn As New clsDataRecordLoginMemberReturn
        DataRecordLoginReturn.IsValid = False
        DataRecordLoginReturn.HasErrors = False

        Try
            Dim dc As New CMSDBDataContext(ModGlobals.ConnectionString)
            Dim pass As String = Nothing

            Try
                ' retrieve user password
                pass = (From itm In dc.REF_ReferrerProfiles
                        Where (itm.LoginName.ToUpper() = loginName.ToUpper() OrElse _
                               itm.eMail.ToUpper() = loginName.ToUpper()) _
                        AndAlso _
                              (itm.Status = ProfileStatusEnum.Approved OrElse _
                                 itm.Status = ProfileStatusEnum.NewProfile OrElse _
                                 itm.Status = ProfileStatusEnum.Rejected) _
                        Select itm.Password).FirstOrDefault()
            Catch ex As Exception

            Finally
                dc.Dispose()
            End Try

            DataRecordLoginReturn = Me.PerfomSiteLogin(loginName, pass, rememberUserName, recordLogin)

            Try
                If (recordLogin = -11) Then
                    DataHelpers.LogProfileAccess(DataRecordLoginReturn.ProfileID, loginName, pass, DataRecordLoginReturn.IsValid, "Admin Login")
                Else
                    DataHelpers.LogProfileAccess(DataRecordLoginReturn.ProfileID, loginName, pass, DataRecordLoginReturn.IsValid, "Auto Login")
                End If
            Catch ex As Exception
            End Try


        Catch ex As Exception
            DataRecordLoginReturn.HasErrors = True
            DataRecordLoginReturn.Message = ex.Message
        End Try

        Return DataRecordLoginReturn
    End Function

    Function PerfomReloginWithTwitterId(twUserId As String, rememberUserName As Boolean, AllowReferral As Boolean) As clsDataRecordLoginMemberReturn
        Dim DataRecordLoginReturn As New clsDataRecordLoginMemberReturn
        DataRecordLoginReturn.IsValid = False
        DataRecordLoginReturn.HasErrors = False

        Dim dc As New CMSDBDataContext(ModGlobals.ConnectionString)
        Try
            Dim pass = Nothing

            ' retrieve user password
            pass = (From itm In dc.REF_ReferrerProfiles
                    Where (itm.TwitterUserId.ToString() = twUserId) _
                    AndAlso _
                          (itm.Status = ProfileStatusEnum.Approved OrElse _
                             itm.Status = ProfileStatusEnum.NewProfile OrElse _
                             itm.Status = ProfileStatusEnum.Rejected) _
                    Select itm.LoginName, itm.Password).FirstOrDefault()


            If (pass IsNot Nothing) Then
                DataRecordLoginReturn = Me.PerfomSiteLogin(pass.LoginName, pass.Password, rememberUserName, AllowReferral)
            End If

        Catch ex As Exception
            DataRecordLoginReturn.HasErrors = True
            DataRecordLoginReturn.Message = ex.Message
        Finally
            dc.Dispose()
        End Try

        Return DataRecordLoginReturn
    End Function

    Function PerfomReloginWithFacebookId(fbUserId As String, rememberUserName As Boolean, AllowReferral As Boolean) As clsDataRecordLoginMemberReturn
        Dim DataRecordLoginReturn As New clsDataRecordLoginMemberReturn
        DataRecordLoginReturn.IsValid = False
        DataRecordLoginReturn.HasErrors = False

        Dim dc As New CMSDBDataContext(ModGlobals.ConnectionString)
        Try
            Dim pass = Nothing

            ' retrieve user password
            pass = (From itm In dc.REF_ReferrerProfiles
                    Where (itm.FacebookUserId.ToString() = fbUserId) _
                    AndAlso _
                          (itm.Status = ProfileStatusEnum.Approved OrElse _
                             itm.Status = ProfileStatusEnum.NewProfile OrElse _
                             itm.Status = ProfileStatusEnum.Rejected) _
                    Select itm.LoginName, itm.Password).FirstOrDefault()


            If (pass IsNot Nothing) Then
                DataRecordLoginReturn = Me.PerfomSiteLogin(pass.LoginName, pass.Password, rememberUserName, AllowReferral)
            End If


        Catch ex As Exception
            DataRecordLoginReturn.HasErrors = True
            DataRecordLoginReturn.Message = ex.Message
        Finally
            dc.Dispose()
        End Try

        Return DataRecordLoginReturn
    End Function


End Class

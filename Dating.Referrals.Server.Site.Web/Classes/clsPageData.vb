﻿Imports Dating.Server.Core.DLL

''' <summary>
''' v 0.4
''' </summary>
''' <remarks></remarks>
Public Class clsPageData


#Region "caching related fuynctionality"

    Private Shared Property gPageDataList As New Dictionary(Of String, DataTable)

    Public Shared Function GetCachedDataTable(pageName As String)
        Dim dtStrings As DataTable = Nothing

        Try
            If (Not String.IsNullOrEmpty(pageName) AndAlso gPageDataList.ContainsKey(pageName)) Then
                dtStrings = gPageDataList(pageName)
            End If
        Catch ex As Exception

        End Try

        Return dtStrings
    End Function


    Public Shared Sub SetCachedDataTable(pageName As String, dtStrings As DataTable)
        Try
            If (Not String.IsNullOrEmpty(pageName) AndAlso Not gPageDataList.ContainsKey(pageName)) Then
                gPageDataList.Add(pageName, dtStrings)
            End If
        Catch ex As Exception

        End Try
    End Sub


    Public Shared Sub ClearCache()
        Try
            gPageDataList.Clear()
        Catch ex As Exception

        End Try
    End Sub


#End Region



    Dim _affCode As String
    Public ReadOnly Property affCode As String
        Get
            If (_affCode Is Nothing) Then _affCode = String.Empty
            Return _affCode
        End Get
    End Property


    Dim _SitePageID As Long = -1
    Public ReadOnly Property SitePageID As String
        Get
            Return _SitePageID
        End Get
    End Property


    Dim _PageName As String
    Public ReadOnly Property PageName As String
        Get
            If (_PageName Is Nothing) Then _PageName = String.Empty
            Return _PageName
        End Get
    End Property


    Dim _Title As String
    Public ReadOnly Property Title As String
        Get
            If (_Title Is Nothing) Then _Title = String.Empty
            Return _Title
        End Get
    End Property


    Dim _LagID As String
    Public ReadOnly Property LagID As String
        Get
            If (_LagID Is Nothing) Then _LagID = String.Empty
            Return _LagID
        End Get
    End Property


    Dim _cPageBasic As Dating.Server.Core.DLL.clsSiteLAG.clsPageBasicReturn
    Public ReadOnly Property cPageBasic As Dating.Server.Core.DLL.clsSiteLAG.clsPageBasicReturn
        Get
            Return _cPageBasic
        End Get
    End Property


    Dim _keysDictionary As Dictionary(Of String, String)
    Dim _keysDictionaryHTTPS As Dictionary(Of String, String)

    Public Sub New(ByRef context As HttpContext)

        If Not context.Request("affCode") Is Nothing Then
            _affCode = Trim(context.Request("affCode"))
        End If

        If Not context.Request("PageId") Is Nothing Then
            _SitePageID = If(IsNumeric(Trim(context.Request("PageId"))), Trim(context.Request("PageId")), -1)
        End If

        If Not context.Request("PageName") Is Nothing Then
            _PageName = Trim(context.Request("PageName"))
        End If

        Dim sessVars As clsSessionVariables = clsSessionVariables.GetCurrent()
        If Not HttpContext.Current.Session("LagID") Is Nothing Then
            _LagID = HttpContext.Current.Session("LagID")
        Else
            _LagID = "US"
        End If



        If _PageName Is Nothing OrElse _PageName.Length = 0 Then
            _PageName = context.Request.AppRelativeCurrentExecutionFilePath
            _PageName = _PageName.TrimStart("~"c).TrimStart("/"c)
        End If

        'If _PageName Is Nothing OrElse _PageName.Length = 0 Then
        '    _PageName = context.Request.Url.LocalPath.TrimStart("/"c)
        'End If

        'If _PageName Is Nothing OrElse _PageName.Length = 0 Then
        '    _PageName = GeneralFunctions.getPageFileNameFromURL(context.Request.Url.AbsoluteUri)
        'End If


        If _SitePageID < 1 Then
            _cPageBasic = gLAG.GetPageBasics(_LagID, _PageName)
            _SitePageID = _cPageBasic.SitePageID
        Else
            'we get the content by SitePageID
            _cPageBasic = gLAG.GetPageBasics(_LagID, _SitePageID)
        End If

        _Title = _cPageBasic.PageTitle
        If _Title Is Nothing OrElse _Title.Length = 0 Then
            If Not context.Request("Title") Is Nothing Then
                _Title = Trim(context.Request("Title"))
            End If
        End If

    End Sub


    Public Sub New(ByVal pageName As String, ByRef context As HttpContext)
        _PageName = pageName

        If Not context.Request("affCode") Is Nothing Then
            _affCode = Trim(context.Request("affCode"))
        End If

        If Not context.Request("Title") Is Nothing Then
            _Title = Trim(context.Request("Title"))
        End If

        Dim sessVars As clsSessionVariables = clsSessionVariables.GetCurrent()
        If Not HttpContext.Current.Session("LagID") Is Nothing Then
            _LagID = HttpContext.Current.Session("LagID")
        Else
            _LagID = "US"
        End If

        _cPageBasic = gLAG.GetPageBasics(_LagID, _PageName)
        _SitePageID = _cPageBasic.SitePageID

        If _Title Is Nothing OrElse _Title.Length = 0 Then
            _Title = _cPageBasic.PageTitle
        End If

    End Sub

    Public Sub New(ByVal pageName As String, ByVal affCode As String, ByVal title As String, ByVal lagID As String)
        _PageName = pageName
        _affCode = affCode
        _Title = title
        _LagID = lagID

        _cPageBasic = gLAG.GetPageBasics(_LagID, _PageName)
        _SitePageID = _cPageBasic.SitePageID

        If _Title Is Nothing OrElse _Title.Length = 0 Then
            _Title = _cPageBasic.PageTitle
        End If

    End Sub


    ''' <summary>
    ''' Returns NOTHING if specified key is not found.
    ''' </summary>
    ''' <param name="key">A key to be found in database</param>
    ''' <returns>value paired to spicified key, otherwise NOTHING</returns>
    ''' <remarks></remarks>
    Public Function GetCustomString(ByVal key As String) As String
        Dim result As String = GetCustomString(key, LagID)
        Return result
    End Function


    ''' <summary>
    ''' Returns NOTHING if specified key is not found.
    ''' </summary>
    ''' <param name="key">A key to be found in database</param>
    ''' <returns>value paired to spicified key, otherwise NOTHING</returns>
    ''' <remarks></remarks>
    Function GetCustomString(ByVal key As String, LagIDparam As String) As String

        'Dim LagIDparam As String = LagID
        Dim result As String = Nothing

        Try

            If (_keysDictionary Is Nothing) Then

                _keysDictionary = New Dictionary(Of String, String)
                Dim dtStrings As New DataTable()
                Try
                    Try
                        ' get sys_messages records for a page
                        If (_PageName = "cmspage.aspx") Then

                            Dim _newPageName = "cmspage.aspx_" & cPageBasic.SitePageID
                            dtStrings = clsPageData.GetCachedDataTable(_newPageName)
                            If (dtStrings Is Nothing) Then
                                dtStrings = gLAG.GetCustomStrings(cPageBasic.SitePageID, LagIDparam)
                                clsPageData.SetCachedDataTable(_newPageName, dtStrings)
                            End If

                        Else

                            dtStrings = clsPageData.GetCachedDataTable(_PageName)
                            If (dtStrings Is Nothing) Then
                                dtStrings = gLAG.GetCustomStrings(cPageBasic.SitePageID, LagIDparam)
                                clsPageData.SetCachedDataTable(_PageName, dtStrings)
                            End If

                        End If

                        'If (Not String.IsNullOrEmpty(_PageName) AndAlso gPageDataList.ContainsKey(_PageName)) Then
                        '    dtStrings = gPageDataList(_PageName)
                        'Else
                        '    dtStrings = gLAG.GetCustomStrings(cPageBasic.SitePageID, LagIDparam)
                        '    gPageDataList.Add(_PageName, dtStrings)
                        'End If
                    Catch ex As Exception

                    End Try


                ' load cache using sys_messages records 
                For Each dr As DataRow In dtStrings.Rows
                    Dim messageKey As String = dr("MessageKey").ToString()
                    Dim valueLag As String = ""
                    If (dtStrings.Columns.Contains(LagIDparam)) Then
                        valueLag = dr(LagIDparam).ToString()
                    End If
                    Dim valueUS As String = dr("US").ToString()

                    If (Not _keysDictionary.ContainsKey(messageKey)) Then
                        If (valueLag.Length > 0) Then
                            _keysDictionary.Add(messageKey, valueLag)
                        ElseIf (valueUS.Length > 0) Then
                            _keysDictionary.Add(messageKey, valueUS)
                        End If
                    End If
                    Next
                Catch ex As Exception
                    Throw New Exception(ex.Message)
                Finally
                    dtStrings.Dispose()
                End Try
            End If


            If (_keysDictionary.ContainsKey(key)) Then
                result = _keysDictionary(key)

                If (HttpContext.Current.Request.Url.Scheme = "https" AndAlso Not String.IsNullOrEmpty(result)) Then
                    If (_keysDictionaryHTTPS Is Nothing) Then _keysDictionaryHTTPS = New Dictionary(Of String, String)

                    If (_keysDictionaryHTTPS.ContainsKey(key)) Then
                        Dim t As String = _keysDictionaryHTTPS(key)
                        If (t <> "NOCHANGE") Then result = t
                    Else
                        If (result.Contains("http://www.goomena.")) Then
                            result = result.Replace("http://www.goomena.", "https://www.goomena.")
                            _keysDictionaryHTTPS.Add(key, result)
                        Else
                            _keysDictionaryHTTPS.Add(key, "NOCHANGE")
                        End If
                    End If
                End If


            End If


        Catch ex As Exception

        End Try

        Return result
    End Function



    Public Overrides Function ToString() As String
        Dim info As String = String.Empty

        info = info & "_affCode: " & _affCode & vbCrLf
        info = info & "_SitePageID: " & _SitePageID & vbCrLf
        info = info & "_PageName: " & _PageName & vbCrLf
        info = info & "_Title: " & _Title & vbCrLf
        info = info & "_LagID: " & _LagID & vbCrLf

        Return info
    End Function
End Class



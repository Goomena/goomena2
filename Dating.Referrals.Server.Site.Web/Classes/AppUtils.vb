﻿Imports System.Diagnostics
Imports Microsoft.VisualBasic
Imports System
Imports System.Drawing
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.IO
Imports DevExpress.XtraEditors
Imports DevExpress.Skins
Imports DevExpress.Utils.Drawing
Imports DevExpress.Utils
Imports System.Text.RegularExpressions
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports System.Web.UI
Imports System.Text
Imports System.Web.UI.HtmlControls
Imports System.Web
Imports Library.Public
Imports Dating.Server.Core.DLL

Public Class AppUtils
    Inherits Dating.Server.Core.DLL.AppUtils

    Public Shared Function mySQLDate(ByVal D As DateTime) As String
        On Error GoTo er
        Dim sz As String
        sz = Microsoft.VisualBasic.Year(D) & "/" & Microsoft.VisualBasic.Month(D) & "/" & Microsoft.VisualBasic.DateAndTime.Day(D) ' d.Year & "/" & d.Month & "/" & d.Day
        mySQLDate = " CONVERT(DATETIME, '" & sz & "  00:00:00', 102)" '"'" & sZ & "'"
        Exit Function
er:     ErrorMsgBox("")
    End Function

    Public Shared Function mySQLDateTime(ByVal D As DateTime) As String
        On Error GoTo er
        Dim sz As String
        sz = Microsoft.VisualBasic.Year(D) _
            & "/" _
            & Microsoft.VisualBasic.Month(D) _
            & "/" _
            & Microsoft.VisualBasic.DateAndTime.Day(D) _
            & " " _
            & Microsoft.VisualBasic.Format(Microsoft.VisualBasic.DateAndTime.Hour(D), "#0") _
            & ":" _
            & Microsoft.VisualBasic.Format(Microsoft.VisualBasic.DateAndTime.Minute(D), "#0") _
            & ":" _
            & Microsoft.VisualBasic.Format(Microsoft.VisualBasic.DateAndTime.Second(D), "#0")
        mySQLDateTime = " CONVERT(DATETIME, '" & sz & "', 102)" '"'" & sZ & "'"
        Exit Function
er:     ErrorMsgBox("")
    End Function

    Public Shared Function mySQLDateTimeCustom(ByVal D As DateTime, ByVal szTime As String) As String
        On Error GoTo er
        Dim sz As String
        sz = Microsoft.VisualBasic.Year(D) & "/" & Microsoft.VisualBasic.Month(D) & "/" & Microsoft.VisualBasic.DateAndTime.Day(D) ' d.Year & "/" & d.Month & "/" & d.Day
        mySQLDateTimeCustom = " CONVERT(DATETIME, '" & sz & "  " & szTime & "', 102)" '"'" & sZ & "'"
        Exit Function
er:     ErrorMsgBox("")
    End Function

    Public Shared Function mySQLDateOnly(ByVal D As DateTime) As String
        On Error GoTo er
        Dim sz As String
        sz = Microsoft.VisualBasic.Year(D) & "/" & Microsoft.VisualBasic.Month(D) & "/" & Microsoft.VisualBasic.DateAndTime.Day(D) ' d.Year & "/" & d.Month & "/" & d.Day
        mySQLDateOnly = sz & "  00:00:00" '"'" & sZ & "'"
        Exit Function
er:     ErrorMsgBox("")
    End Function


    Public Shared Sub setSEOPageData(ByVal page As BasePage, ByVal title As String, Optional ByVal meta_description_content As String = "", Optional ByVal meta_keywords_content As String = "", Optional ByVal meta_copyright_content As String = "", Optional ByVal meta_name_content As String = "")
        'SEO '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        page.Header.Title = title

        'Render: <meta itemprop="name" content="Some words listed here"
        If (Not String.IsNullOrEmpty(meta_name_content)) Then
            Dim meta_name As New HtmlMeta
            meta_name.Name = "name"
            meta_name.Content = meta_name_content
            meta_name.Attributes.Add("itemprop", "name")
            page.Header.Controls.Add(meta_name)
        End If

        'Render: <meta name="keywords" content="Some words listed here" />
        Dim meta_description As New HtmlMeta
        meta_description.Name = "description"
        meta_description.Content = meta_description_content
        'meta_description.Attributes.Add("itemprop", "description")
        page.Header.Controls.Add(meta_description)

        'Render: <meta name="keywords" content="Some words listed here" />
        Dim meta_keywords As New HtmlMeta
        meta_keywords.Name = "keywords"
        meta_keywords.Content = meta_keywords_content
        page.Header.Controls.Add(meta_keywords)

        'Render: <meta name="copyright" content="Some words listed here" />
        Dim meta_copyright As New HtmlMeta
        meta_copyright.Name = "copyright"
        meta_copyright.Content = meta_copyright_content
        page.Header.Controls.Add(meta_copyright)


        ' Render: <meta name="robots" content="noindex" />
        '<META NAME="ROBOTS" CONTENT="NOINDEX, FOLLOW">
        '<META NAME="ROBOTS" CONTENT="INDEX, NOFOLLOW">
        '<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">

        Dim meta_robots = New HtmlMeta
        meta_robots.Name = "robots"
        meta_robots.Content = "INDEX, FOLLOW"
        page.Header.Controls.Add(meta_robots)

        ' Render: <meta name="date" content="2006-03-25" scheme="YYYY-MM-DD" />
        Dim meta_date = New HtmlMeta
        meta_date.Name = "date"
        meta_date.Content = DateTime.Now.ToString("yyyy-MM-dd")
        meta_date.Scheme = "YYYY-MM-DD" & vbCrLf
        page.Header.Controls.Add(meta_date)

        Dim verify_robots = New HtmlMeta
        verify_robots.Name = "verify-v1"
        verify_robots.Content = "verify Google"
        page.Header.Controls.Add(verify_robots)



        'END SEO '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    End Sub

    Public Shared Sub initPageData(ByVal page As BasePage, ByVal labelName As String, ByVal pageURL As String, ByVal lagID As String)
        ' Try

        'Dim PageData As Dictionary(Of String, String) = GetPageMainMessages(getPageFileNameFromURL(pageURL), siteID, lagID)
        'setSEOPageData(page, PageData("Title"), PageData("MetaDescription"))
        'If labelName <> "" Then
        '    Dim label As DevExpress.Web.ASPxEditors.ASPxLabel = page.Controls.Item(labelName)
        '    label.Text = PageData("Body")
        'End If
        'Catch ex As Exception

        'End Try
    End Sub
    Public Shared Sub setNaviLabel(ByVal page As BasePage, ByVal labelName As String, ByVal labelText As String)
        Dim label As DevExpress.Web.ASPxEditors.ASPxLabel = page.Master.FindControl(labelName)
        If (Not label Is Nothing) Then
            label.Text = labelText
        End If
    End Sub
    Public Shared Function ErrorMsgBox(ByVal sExtaMessage As String)
        'On Error GoTo er ' **** na min enegorpithi katastrefete to error
        Dim s As String = Err.Description & vbCrLf & "Error Code: " & Err.Number & vbCrLf & "Source: " & Err.Source & vbCrLf & "Function:" & sExtaMessage
        HttpContext.Current.Response.Write(s)
        ' Err.Clear()
        Exit Function
er:
        MsgBox("Error on ErrorMsgBox->" & sExtaMessage)
    End Function

    Public Shared Function ErrorMsgBox(ByVal ex As Exception, ByVal sExtaMessage As String)
        Try
            Dim s As String = ex.Message & "<br>" & "Source: " & ex.Source & " <br>[Function:" & sExtaMessage & "<br>] Stack: " & ex.StackTrace & "<br>"
            'MsgBox(s, "Error", MessageBoxButtons.OK)
            HttpContext.Current.Response.Write(s)
        Catch inex As Exception
            'MsgBox(inex.Message & vbCrLf & "Error on ErrorMsgBox->" & sExtaMessage)
        End Try
    End Function


    Public Shared Function CheckNULL(ByVal nValue As Object, ByVal DefaultValue As String) As String
        Try
            If IsDBNull(nValue) Then nValue = DefaultValue
            If Len(DefaultValue) > 0 And Len(nValue) = 0 Then Return DefaultValue
            Return nValue
        Catch ex As Exception
            ErrorMsgBox(ex, "")
            Return DefaultValue
        End Try
    End Function



    Public Shared Function GetParamValueFromUrl(ByVal szURL As String, ByVal szParamName As String, ByVal DefaultValue As String) As String
        Try

            Dim StartPos As Integer = InStr(szURL, szParamName & "=")
            If StartPos = 0 Then Return ""
            Dim NewString As String = Mid(szURL, StartPos + 2)
            Dim EndPos As Integer = InStr(NewString, "&")
            If EndPos = 0 Then EndPos = Len(NewString) - StartPos
            Dim Value As String = Mid(szURL, StartPos + 2, EndPos - 1)
            Return Value

        Catch ex As Exception
            'ErrorMsgBox(ex, "")
            Return DefaultValue
        End Try
    End Function



    Public Shared Function GetImage(CustomerID As String, FileName As String, GenderId As String, isThumb As Boolean, isHTTPS As Boolean) As String
        Dim photoPath As String = ""
        Try

            Dim _CustomerID As Integer = 0
            Dim _GenderId As Integer = 0

            If (Not String.IsNullOrEmpty(CustomerID)) Then
                Integer.TryParse(CustomerID, _CustomerID)
            End If

            If (Not String.IsNullOrEmpty(GenderId)) Then
                Integer.TryParse(GenderId, _GenderId)
            End If

            photoPath = ProfileHelper.GetProfileImageURL(_CustomerID, FileName, _GenderId, True, isHTTPS)
            If (HttpContext.Current.Session("ProfileID") = 269) Then
                If (HttpContext.Current.Request.Url.Scheme = "https") Then
                    photoPath = photoPath.Replace("http://", "https://")
                End If
            End If
        Catch ex As Exception

        End Try
        Return photoPath
    End Function

    Public Shared Function GetDateTimeString(dateTimeValue As Object, Optional tookenString As String = "", Optional formatString As String = "d MMM, yyyy H:mm") As String
        Dim txt = ""
        Try
            If (HttpContext.Current.Session("TimeOffset") IsNot Nothing AndAlso HttpContext.Current.Session("TimeOffset").GetType() = GetType(Double)) Then
                Dim dt As DateTime = CType(dateTimeValue, DateTime)
                dt = dt.AddMinutes(HttpContext.Current.Session("TimeOffset") * -1)
                txt = dt.ToString(formatString)
            Else
                txt = CType(dateTimeValue, DateTime).ToLocalTime().ToString(formatString)
            End If

            If (Not String.IsNullOrEmpty(tookenString)) Then txt = tookenString.Replace("###DATETIME###", txt)
        Catch ex As Exception

        End Try
        Return txt
    End Function


    Public Shared Function IsDBNullOrNothingOrEmpty(obj As Object) As Boolean
        If (obj Is System.DBNull.Value) Then Return True
        If (obj Is Nothing) Then Return True
        If (obj.GetType() Is GetType(String) AndAlso obj.ToString() = String.Empty) Then Return True

        Return False
    End Function

    Private Shared _alllags() As String
    Public Shared Function IsSupportedLAG(lag As String) As Boolean
        Dim success As Boolean

        If (_alllags Is Nothing) Then
            Dim SupportedLAGS As String = ConfigurationManager.AppSettings("SupportedLAGS")
            If (String.IsNullOrEmpty(SupportedLAGS)) Then SupportedLAGS = "GR,US,DE,FR,HU,IN,RO"
            _alllags = SupportedLAGS.Split(",")
        End If

        If (Not String.IsNullOrEmpty(lag)) Then
            Dim reqLag As String = lag.ToUpper()
            If (_alllags.Contains(reqLag)) Then
                success = True
            End If
        End If

        Return success
    End Function


    Public Shared Sub CheckDomainAndRedirect(host As String)
        Try
            host = host.ToUpper()
            Dim config As String = ConfigurationManager.AppSettings("RedirectFromTo")
            Dim redirections As String() = config.Split(","c)
            Dim cnt As Integer
            For cnt = 0 To redirections.Length - 1
                Dim domains As String() = (redirections(cnt).Replace("[", "").Replace("]", "")).Split(" "c)
                If (domains.Length = 2) Then
                    If (host.ToUpper() = domains(0).ToUpper()) Then
                        Dim newUrl As String = HttpContext.Current.Request.Url.ToString()
                        newUrl = newUrl.Replace(HttpContext.Current.Request.Url.Host, domains(1))
                        HttpContext.Current.Response.RedirectPermanent(newUrl)
                    End If
                End If
            Next
            'If(request)

        Catch ex As Exception

        End Try
    End Sub


   
End Class

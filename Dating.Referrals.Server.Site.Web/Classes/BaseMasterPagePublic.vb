﻿Imports Dating.Server.Core.DLL
Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Datasets.DLL.DSMembers

Public Class BaseMasterPagePublic
    Inherits System.Web.UI.MasterPage
    'Implements ILanguageDependableContent



    Private _IsMale As Boolean?
    Public ReadOnly Property IsMale As Boolean
        Get
            If (Not _IsMale.HasValue) Then
                '_IsMale = (Me.GetCurrentProfile().GenderId = ProfileHelper.gMaleGender.GenderId)
                _IsMale = (Me.SessionVariables.MemberData.GenderId = ProfileHelper.gMaleGender.GenderId)
            End If
            Return _IsMale
        End Get
    End Property


    Private _IsFemale As Boolean?
    Public ReadOnly Property IsFemale As Boolean
        Get
            If (Not _IsFemale.HasValue) Then
                '_IsFemale = (Me.GetCurrentProfile().GenderId = ProfileHelper.gFemaleGender.GenderId)
                _IsFemale = (Me.SessionVariables.MemberData.GenderId = ProfileHelper.gFemaleGender.GenderId)
            End If
            Return _IsFemale
        End Get
    End Property



    Public ReadOnly Property MasterProfileId As Integer
        Get
            Return Me.Session("ProfileID")
        End Get
    End Property


    Protected ReadOnly Property MirrorProfileId As Integer
        Get
            Return Me.Session("MirrorProfileID")
        End Get
    End Property


    Dim _sessVars As clsSessionVariables
    Protected ReadOnly Property SessionVariables As clsSessionVariables
        Get
            If (_sessVars Is Nothing) Then _sessVars = clsSessionVariables.GetCurrent()
            Return _sessVars
        End Get
    End Property


    Private _globalStrings As clsPageData
    Public ReadOnly Property globalStrings As clsPageData
        Get
            If (_globalStrings Is Nothing) Then
                _globalStrings = New clsPageData("GlobalStrings", HttpContext.Current)
            End If
            Return _globalStrings
        End Get
    End Property



    Dim _CMSDBDataContext As CMSDBDataContext
    Protected ReadOnly Property CMSDBDataContext As CMSDBDataContext
        Get
            If (_CMSDBDataContext Is Nothing) Then
                _CMSDBDataContext = New CMSDBDataContext(ConfigurationManager.ConnectionStrings("AppDBconnectionString").ConnectionString)
            End If
            Return _CMSDBDataContext
        End Get
    End Property



    Dim _mineMasterProfile As EUS_Profile
    Protected Function GetCurrentMasterProfile() As EUS_Profile
        Try

            If (_mineMasterProfile Is Nothing) Then
                _mineMasterProfile = Me.CMSDBDataContext.EUS_Profiles.Where(Function(itm) (itm.ProfileID = Me.MirrorProfileId OrElse itm.ProfileID = Me.MasterProfileId) AndAlso itm.IsMaster = True).SingleOrDefault()
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Return _mineMasterProfile
    End Function


    Dim _mineMirrorProfile As EUS_Profile
    Protected Function GetCurrentMirrorProfile() As EUS_Profile
        Try

            If (_mineMirrorProfile Is Nothing) Then
                _mineMirrorProfile = Me.CMSDBDataContext.EUS_Profiles.Where(Function(itm) (itm.ProfileID = Me.MirrorProfileId OrElse itm.ProfileID = Me.MasterProfileId) AndAlso itm.IsMaster = False).SingleOrDefault()
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Return _mineMirrorProfile
    End Function




    Dim _currentProfile As EUS_Profile
    Protected Function GetCurrentProfile() As EUS_Profile
        Try

            If (_currentProfile Is Nothing) Then
                _currentProfile = Me.GetCurrentMasterProfile()
                If (_currentProfile Is Nothing) Then
                    _currentProfile = Me.GetCurrentMirrorProfile()
                End If
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Return _currentProfile
    End Function



End Class

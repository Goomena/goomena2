﻿Public Class clsCrypoPassword

    Public Function CrypPassword(ByVal szPass As String) As String
        Try
            If Not gIsReadGlobals Then ReadGlobals()
            Dim szPassFull = szPass '& Plus2Pass
            Return Library.Public.Crypto.EncryptTripleDES(szPassFull, EnCrypPass)
        Catch ex As Exception
            gER.ErrorMsgBox(ex, "")
        End Try
    End Function

    Public Function DeCrypPassword(ByVal szPassCrypto As String) As String
        Try
            If Not gIsReadGlobals Then ReadGlobals()
            Return Library.Public.Crypto.DecryptTripleDES(szPassCrypto, EnCrypPass)
        Catch ex As Exception
            gER.ErrorMsgBox(ex, "")
        End Try
    End Function


End Class

﻿Imports System.Web.SessionState
Imports Library.Public
Imports System.Reflection
Imports Dating.Server.Core.DLL

Public Class clsCurrentContext



    Public Shared Function VerifyLogin() As Boolean

        If (HttpContext.Current.Request.Params("AUTH_USER") <> "" AndAlso
            HttpContext.Current.User.Identity.IsAuthenticated AndAlso
            (HttpContext.Current.Session("ProfileID") > 0 OrElse
             HttpContext.Current.Session("ReferrerCustomerID") > 0) AndAlso
            HttpContext.Current.Request.Params("AUTH_USER") = HttpContext.Current.Session("LoginName")) Then

            Return True

        End If

        Return False
    End Function


    Public Shared Function IsAffiliate() As Boolean

        If (clsSessionVariables.GetCurrent().MemberData.Role = "AFFILIATE") Then
            Return True
        End If

        Return False
    End Function


    Public Shared Sub SetLAGID()
        If (HttpContext.Current.Session("LagID") Is Nothing) Then
            If (HttpContext.Current.Session("GEO_COUNTRY_CODE") = "US" OrElse HttpContext.Current.Session("GEO_COUNTRY_CODE") = "GR" OrElse HttpContext.Current.Session("GEO_COUNTRY_CODE") = "HU") Then
                HttpContext.Current.Session("LagID") = HttpContext.Current.Session("GEO_COUNTRY_CODE")
            Else
                HttpContext.Current.Session("LagID") = "US"
            End If
        End If
    End Sub


    Public Shared Sub LogOff()
        FormsAuthentication.SignOut()
        HttpContext.Current.Session.Abandon()

        HttpContext.Current.Request.Cookies.Remove(".ASPNETAUTH")

        Dim cnt As Integer
        For cnt = 0 To HttpContext.Current.Request.Cookies.Count - 1
            HttpContext.Current.Request.Cookies(cnt).Expires = DateTime.Now.AddYears(-1)
            HttpContext.Current.Request.Cookies(cnt).Domain = Nothing
        Next

        For cnt = 0 To HttpContext.Current.Response.Cookies.Count - 1
            HttpContext.Current.Response.Cookies(cnt).Expires = DateTime.Now.AddYears(-1)
            HttpContext.Current.Response.Cookies(cnt).Domain = Nothing
        Next

        ' clear authentication cookie
        Dim cookie1 As HttpCookie = New HttpCookie(FormsAuthentication.FormsCookieName, "")
        cookie1.Expires = DateTime.Now.AddYears(-1)
        cookie1.Domain = Nothing
        HttpContext.Current.Response.Cookies.Add(cookie1)

        ' clear session cookie (not necessary for your current problem but i would recommend you do it anyway)
        Dim cookie2 As HttpCookie = New HttpCookie("ASP.NET_SessionId", "")
        cookie2.Expires = DateTime.Now.AddYears(-1)
        cookie2.Domain = Nothing
        HttpContext.Current.Response.Cookies.Add(cookie2)


    End Sub


    Public Shared Sub ClearSession(Session As HttpSessionState)
        Try
            'Dim Session As HttpSessionState = HttpContext.Current.Session

            If (Session("ProfileID") > 0 AndAlso (Session("IsAdminLogin") Is Nothing OrElse Session("IsAdminLogin") = False)) Then
                DataHelpers.UpdateEUS_Profiles_LogoutData(Session("ProfileID"))

                'Try
                '    DataHelpers.LogProfileAccess(Session("ProfileID"), userName, password, DataRecordLoginReturn.IsValid, "Login")
                'Catch ex As Exception
                'End Try
            End If

            If (Session("ProfileID") > 0) Then
                Try
                    DataHelpers.Update_EUS_Profiles_UpdateAvailableCredits(Session("ProfileID"), 0, True)
                Catch ex As Exception
                End Try

                Try
                    If (Session("IsAdminLogin") = True) Then
                        DataHelpers.LogProfileAccess(Session("ProfileID"), Session("LoginName"), "", True, "Admin Logout", Nothing, Session("lagCookie"),
                                                     Session("IP"), False)
                    Else
                        DataHelpers.LogProfileAccess(Session("ProfileID"), Session("LoginName"), "", True, "Logout", Nothing, Session("lagCookie"),
                                                     Session("IP"), False)
                    End If
                Catch ex As Exception
                End Try
            End If

            Session("ReferrerCustomerID") = 0
            Session("SiteLoginName") = Nothing

            Session("Logoff") = Nothing
            Session("ReferrerCustomerID") = 0
            Session("ProfileID") = 0
            Session("MirrorProfileID") = 0
            Session("IsAdminLogin") = Nothing
            Session("clsAffiliateCredits") = Nothing
            Session("NoRedirectToMembersDefault") = Nothing
            Session("clsSessionVariables") = Nothing
            Session("Update_EUS_Profiles_UpdateAvailableCredits") = Nothing
            Session("FBUser") = Nothing
            Session("TWUser") = Nothing
            Dim ip As String = GetCurrentIP()
            If (Not String.IsNullOrEmpty(ip)) Then Session("IP") = ip
            'clsSessionVariables.GetCurrent().ReferrerCredits = Nothing
        Catch ex As Exception

        End Try
    End Sub



    Public Shared Function GetCurrentIP() As String
        Dim ip As String = ""

        Try
            If (HttpContext.Current IsNot Nothing) Then
                Dim Request As HttpRequest = HttpContext.Current.Request
                ip = Request.ServerVariables("REMOTE_ADDR") '-- the IP address of the visitor
                If (ip.ToString().StartsWith("127.") OrElse ip.ToString().Equals("::1")) Then
                    ip = "46.246.144.52" '"79.130.17.211"
                    ' Session("IP") = "5.11.128.22" ' TR
                    ip = "188.165.13.64" ' FR
                    'ip = "188.4.80.226" ' GR
                End If
            End If
        Catch
        End Try

        Return ip
    End Function






End Class

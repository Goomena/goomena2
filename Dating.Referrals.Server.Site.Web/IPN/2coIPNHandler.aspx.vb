﻿Imports System.Data.SqlClient
Imports System.Security.Cryptography
Imports Dating.Server.Core.DLL

Public Class _2coIPNHandler
    Inherits System.Web.UI.Page

    Shared cmd As New SqlCommand
    Shared sqlcon As New SqlConnection(ModGlobals.SqlConnectionString)
    Shared sqlStr As String = ""


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim siteIPN As UniPAYIPN = New UniPAYIPN() '"http://www.zevera.com/unipayipn.asmx"
        Dim ipnSender As New clsCore
        Dim buyer As New clsDataRecordBuyerInfo
        Dim ipnData As New clsDataRecordIPN
        Dim UniPayTransactionID As String = Server.UrlDecode(Request("vendor_order_id"))
        Dim Merchant As String = Server.UrlDecode(Request("vendor_id"))
        Dim Securitycode As String = Server.UrlDecode(Request("md5_hash"))
        Dim CustomerEmaiAddress As String = Server.UrlDecode(Request("customer_email"))
        Dim ReferenceNumber As String = Server.UrlDecode(Request("invoice_id"))
        Dim amount As String = Server.UrlDecode(Request("invoice_list_amount"))
        Dim status As String = Server.UrlDecode(Request("invoice_status"))
        Dim custFirstName As String = Server.UrlDecode(Request("customer_first_name"))
        Dim custLastName As String = Server.UrlDecode(Request("customer_last_name"))
        Dim custAddress As String = Server.UrlDecode(Request("bill_street_address"))
        Dim custCity As String = Server.UrlDecode(Request("bill_city"))
        Dim custState As String = Server.UrlDecode(Request("bill_state"))
        Dim custCountry As String = Server.UrlDecode(Request("bill_country"))
        Dim custZip As String = Server.UrlDecode(Request("bill_postal_code"))
        Dim customerIP As String = Server.UrlDecode(Request("customer_ip"))
        Dim feeAmount As String = 0 'UrlDecode(Request("ap_feeamount"))
        Dim netAmount As String = Server.UrlDecode(Request("invoice_list_amount")) 'UrlDecode(Request("ap_netamount"))
        Dim currency As String = "EUR"
        'Dim message_type As String = UrlDecode(Request("message_type"))
        'Dim fraud_status As String = UrlDecode(Request("fraud_status"))
        Try

            If Trim(Merchant.ToLower()) = "1832556" Then

                '  If Trim(Securitycode) = Trim(GenerateHash(UrlDecode(Request("sale_id")) & Merchant & ReferenceNumber & "@Ek0R1G!aL21")) Then
                sqlStr = "select * from PayTransactions INNER JOIN SalesSitesProducts ON SalesSitesProducts.SalesSiteProductID =  PayTransactions.SalesSiteProductID inner join salessites on salessites.SalesSiteID = PayTransactions.SalesSiteID WHERE UniqueID = '" & UniPayTransactionID & "'"
                Dim rs As SqlDataReader
                cmd = New SqlCommand(sqlStr, sqlcon)

                cmd.Connection.Open()
                rs = cmd.ExecuteReader()

                While rs.Read

                    siteIPN = New UniPAYIPN() 'rs("WebServiceIPNURL")
                    ipnSender = New clsCore
                    ipnData.PayProviderTransactionID = ReferenceNumber
                    ipnData.PayProviderAmount = amount

                    buyer.PayerEmail = CustomerEmaiAddress
                    ipnData.CustomerID = rs("CustomerID")
                    ipnData.PaymentDateTime = rs("PaymentDateTime")
                    ipnData.PayTransactionID = UniPayTransactionID
                    ipnData.SalesSiteID = rs("SalesSiteID")
                    ipnData.SalesSiteProductID = rs("SalesSiteProductID")
                    ipnData.TransactionTypeID = rs("SaleTypeID")
                    ipnData.Currency = currency
                    ipnData.custopmerIP = customerIP
                    'ipnData.message_type = message_type
                    'ipnData.fraud_status = fraud_status

                    Dim extraData As String = rs("SalesSiteExtraData")
                    For Each Data As String In extraData.Split(";")
                        If Data.Contains("customReferrer") Then
                            ipnData.CustomReferrer = Data.Split("=")(1)
                        End If
                        If Data.Contains("promoCode") Then
                            ipnData.PromoCode = Data.Split("=")(1)
                        End If

                    Next
                    ipnData.SaleDescription = "Add Credits with 2CO. Reference: " & ReferenceNumber

                    'If rs("ProductDescription").ToString.Contains("Days") Or rs("ProductDescription").ToString.Contains("Lifetime") Or rs("ProductDescription").ToString.Contains("Unlimited") Then

                    '    ipnData.SaleDescription = "Add Days with 2CO. Reference: " & ReferenceNumber
                    'ElseIf rs("ProductDescription").ToString.Contains("Giga") Then

                    '    If rs("ProductDescription").ToString.Contains("Oron") Then
                    '        ipnData.SaleDescription = "Add oronGiga with 2CO. Reference: " & ReferenceNumber
                    '    ElseIf rs("ProductDescription").ToString.Contains("Uploaded.to") Then
                    '        ipnData.SaleDescription = "Add ulGiga with 2CO. Reference: " & ReferenceNumber
                    '    Else

                    '        ipnData.SaleDescription = "Add Giga with 2CO. Reference: " & ReferenceNumber
                    '    End If

                    'Else
                    '    ipnData.SaleDescription = "Reseller Add Money"

                    'End If

                    ipnData.SaleQuantity = rs("Quantity")
                End While
                rs.Close()
                cmd.Connection.Close()
                Try

                    sqlStr = "UPDATE PayTransactions SET IsCompleted = 1, Currency = '" & currency & "', BuyerInfo_PayerEmail = '" & buyer.PayerEmail & "', BuyerInfo_FirstName = '" & custFirstName & "', BuyerInfo_LastName = '" & custLastName & "', BuyerInfo_Address = '" & custAddress & "', BuyerInfo_ResidenceCountry = '" & custCountry & "', IPN_NotifierIP = '" & Request.ServerVariables("REMOTE_ADDR") & "', IPN_TransactionID = '" & ReferenceNumber & "', Amount = " & amount & ", Fee = " & feeAmount & ", Gross = " & amount - feeAmount & " WHERE UniqueID = '" & UniPayTransactionID & "'"
                    cmd = New SqlCommand(sqlStr, sqlcon)
                    cmd.Connection.Open()
                    cmd.ExecuteNonQuery()
                    cmd.Connection.Close()
                Catch ex As Exception

                End Try
                Dim sData As String
                sData = ipnData.PayProviderAmount & "+" & ipnData.PaymentDateTime & "+" & ipnData.PayTransactionID & "+" & ipnData.CustomerID & "+" & ipnData.SalesSiteProductID & "Extr@Ded0men@"
                Dim h As New Library.Public.clsHash
                Dim Code As String = Library.Public.clsHash.ComputeHash(sData, "SHA512")
                ipnData.VerifyHASH = Code
                ipnData.BuyerInfo = buyer
                Dim ipnReturn As New clsDataRecordIPNReturn
                ipnReturn = clsCore.SendIPN(ipnData) ', siteIPN.Url
                Try
                    If Not ipnReturn.HasErrors Then
                        sqlStr = "UPDATE PayTransactions SET IsSendingToSalesSiteIPN = 1, ResponceFromSalesSite = 'Customer Updated' WHERE UniqueID = '" & UniPayTransactionID & "'"
                        cmd = New SqlCommand(sqlStr, sqlcon)
                        cmd.Connection.Open()
                        cmd.ExecuteNonQuery()
                        cmd.Connection.Close()
                    Else
                        sqlStr = "UPDATE PayTransactions SET IsSendingToSalesSiteIPN = 0, ResponceFromSalesSite = '" & ipnReturn.Message.Replace("'", "_") & "' WHERE UniqueID = '" & UniPayTransactionID & "'"
                        cmd = New SqlCommand(sqlStr, sqlcon)
                        cmd.Connection.Open()
                        cmd.ExecuteNonQuery()
                        cmd.Connection.Close()
                    End If
                Catch ex As Exception
                    cmd.Connection.Close()
                    sqlStr = "UPDATE PayTransactions SET IsSendingToSalesSiteIPN = 1, ResponceFromSalesSite = 'Customer Updated' WHERE UniqueID = '" & UniPayTransactionID & "'"
                    cmd = New SqlCommand(sqlStr, sqlcon)
                    cmd.Connection.Open()
                    cmd.ExecuteNonQuery()
                    cmd.Connection.Close()
                End Try

                ' Else
                '    sqlcon = New SqlConnection(ModGlobals.SqlConnectionString)

                '   sqlStr = "UPDATE PayTransactions SET IPN_OriginalCleanData = 'Error On Security Code' WHERE UniqueID = '" & UniPayTransactionID & "'"
                '  cmd = New SqlCommand(sqlStr, sqlcon)
                ' cmd.Connection.Open()
                'cmd.ExecuteNonQuery()
                'cmd.Connection.Close()
                ' End If
            Else


            End If
        Catch ex As Exception
            sqlcon = New SqlConnection(ModGlobals.SqlConnectionString)

            sqlStr = "UPDATE PayTransactions SET IPN_OriginalCleanData = '" & ex.Message.ToString & "' WHERE UniqueID = '" & UniPayTransactionID & "'"
            cmd = New SqlCommand(sqlStr, sqlcon)
            cmd.Connection.Open()
            cmd.ExecuteNonQuery()
            cmd.Connection.Close()

        End Try
        Response.Clear()

    End Sub


    Private Function GenerateHash(ByVal SourceText As String) As String
        'Create an encoding object to ensure the encoding standard for the source text
        Dim Ue As New UnicodeEncoding()
        'Retrieve a byte array based on the source text
        Dim ByteSourceText() As Byte = Ue.GetBytes(SourceText)
        'Instantiate an MD5 Provider object
        Dim Md5 As New MD5CryptoServiceProvider()
        'Compute the hash value from the source
        Dim ByteHash() As Byte = Md5.ComputeHash(ByteSourceText)
        'And convert it to String format for return
        Return Convert.ToBase64String(ByteHash)
    End Function

End Class
﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Root.Master" CodeBehind="CmsPage.aspx.vb" 
    Inherits="Dating.Referrals.Server.Site.Web.CmsPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
    /*p{margin:0; padding:0; }*/
    body {
    font-family: "Segoe UI","Arial";
    font-weight:lighter;
    }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="insidemosaiccontainer" runat="server">
    <asp:Label ID="lblinside" runat="server" Text=""></asp:Label>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="content" runat="server">
    <div class="mid_bg pageContainer">
        <div class="hp_cr" id="hp_top">
        </div>
        <div id="hp_content">
            <div class="container_12">
                <div class="cms_page">
                    <div class="cms_content">
                        <div class="ab_block">
                            <div class="pe_wrapper">
                                <div class="pe_box">
                                    <h2 style="text-shadow: none; border: none; font-family: Lucida Grande,Lucida Sans Unicode,Verdana,Arial,sans-serif;">
                                        <asp:Label ID="lblHeader" runat="server" /><span class="loginHereLink"></span></h2>
                                    <dx:ASPxLabel ID="lbHTMLBody" runat="server" ClientIDMode="AutoID" EncodeHtml="False">
                                    </dx:ASPxLabel>
                                </div>
                            </div>
                        </div>
                    </div>
                    <%--
            <div class="ab_right">
                <div class="ab_block test_ab">
                    <div class="lists">
                    </div>
                    <h2>Talk to us</h2>
                    <uc1:LiveZilla ID="LiveZilla1" runat="server" />
                </div>
            </div>
            <div class="ab_right">
                <div class="ab_block test_ab">
                    <div class="lists">
                        <h2><asp:Literal ID="msg_JoinTwitterPanel" runat="server"/></h2>
                        <a class="js twitter-login" id="lnkJoinTwitter" runat="server"
                            href="https://www.twitter.com/"><asp:Literal ID="msg_JoinTwitterLink" runat="server"/></a>
                        <asp:Literal ID="msg_JoinTwitterDescr" runat="server"/>
                    </div>
                </div>
            </div>
                    --%>
                    <div class="clear">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="pageContainer" style="margin-top: 20px; display: none;">
        <div>
        </div>
    </div>
<%--
        <dx:ASPxRoundPanel ID="pnl" runat="server" ClientIDMode="AutoID" CssFilePath="~/App_Themes/Office2010Silver/{0}/styles.css"
            CssPostfix="Office2010Silver" EnableDefaultAppearance="False" GroupBoxCaptionOffsetX="6px"
            GroupBoxCaptionOffsetY="-19px" HeaderText="" SpriteCssFilePath="~/App_Themes/Office2010Silver/{0}/sprite.css" Width="100%">
            <ContentPaddings PaddingBottom="10px" PaddingLeft="9px" PaddingRight="11px" PaddingTop="10px" />
            < %--<HeaderStyle Font-Bold="True" Font-Size="Medium">
                <Paddings PaddingBottom="6px" PaddingLeft="9px" PaddingRight="11px" PaddingTop="3px" />
            </HeaderStyle>--% >
            <PanelCollection>
                <dx:PanelContent runat="server" SupportsDisabledAttribute="True">
                    <dxe:ASPxLabel ID="lbHTMLBody" runat="server" ClientIDMode="AutoID" EncodeHtml="False">
                    </dxe:ASPxLabel>
                </dx:PanelContent>
            </PanelCollection>
        </dx:ASPxRoundPanel>
--%>
 <dx:ASPxPopupControl SkinID="None" EncodeHtml="False" ID="popupWindows" runat="server"
        EnableViewState="False" EnableHotTrack="False" PopupHorizontalAlign="RightSides"
        PopupVerticalAlign="Above" PopupHorizontalOffset="1" PopupVerticalOffset="-4"
        EnableHierarchyRecreation="True" CloseAction="MouseOut" HeaderText="" 
        PopupAction="MouseOver" RenderMode="Lightweight">
        <ClientSideEvents />
        <Windows>
        </Windows>
    </dx:ASPxPopupControl>
</asp:Content>

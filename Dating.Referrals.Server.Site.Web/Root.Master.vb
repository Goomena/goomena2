﻿Imports Library.Public
Imports DevExpress.Web.ASPxMenu
Imports Dating.Server.Core.DLL
Imports DevExpress.Web.ASPxEditors
Imports Dating.Server.Datasets.DLL

Public Class _RootMaster
    Inherits BaseMasterPagePublic
    'Implements INavigation


    Dim _pageData As clsPageData
    Protected ReadOnly Property CurrentPageData As clsPageData
        Get
            If (_pageData Is Nothing) Then _pageData = New clsPageData("master", Context)
            Return _pageData
        End Get
    End Property



    Private Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        Try


            If Not Me.IsPostBack Then

                If (Not Page.IsPostBack) Then
                    Dim TimeOffset As Double
                    If (hdfTimeOffset.Value.Trim() <> "" AndAlso Double.TryParse(hdfTimeOffset.Value, TimeOffset)) Then
                        Session("TimeOffset") = TimeOffset
                    End If
                End If


                'If (GeneralFunctions.IsSupportedLAG(Request.QueryString("lag"))) Then
                If (Not String.IsNullOrEmpty(Request.QueryString("lag")) AndAlso (Request.QueryString("lag").ToUpper() = "US" OrElse Request.QueryString("lag").ToUpper() = "GR")) Then
                    Session("LagID") = Request.QueryString("lag").ToUpper()

                    '' update lagid info
                    'Try
                    '    If (Me.Session("ProfileID") > 0) Then
                    '        DataHelpers.UpdateEUS_Profiles_LAGID(Me.Session("ProfileID"), Me.Session("LagID"))
                    '    End If
                    'Catch ex As Exception
                    '    WebErrorMessageBox(Me, ex, "")
                    'End Try
                End If

                If Session("LagID") Is Nothing Then
                    If (Session("GEO_COUNTRY_CODE") = "GR") Then
                        Session("LagID") = "GR"
                    Else
                        Session("LagID") = gDomainCOM_DefaultLAG
                    End If
                End If

                cmbLag.Items(GetIndexFromLagID(Session("LagID"))).Selected = True

                SetLanguageLinks()
            End If


            If (cmbLag.SelectedItem Is Nothing) Then
                cmbLag.Items(GetIndexFromLagID(Session("LagID"))).Selected = True
            End If


            Try
                ' otan to session exei diaforetiki glwsa apo tin epilegmeni glwssa
                If (GetIndexFromLagID(Session("LagID")) = 0 AndAlso Session("LagID") <> "US") Then
                    Session("LagID") = gDomainCOM_DefaultLAG
                End If
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try


            If Not Me.IsPostBack Then
                LoadLAG()
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            ' ShowUserMap()

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Protected Sub cmbLag_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbLag.SelectedIndexChanged
        Try

            Dim lagCookie As HttpCookie = Request.Cookies("lagCookie")
            If lagCookie Is Nothing Then
                Dim szGuid As String = System.Guid.NewGuid().ToString()
                lagCookie = New HttpCookie("lagCookie", szGuid)
                lagCookie.Expires = DateTime.Now.AddYears(2)
                lagCookie.Item("LagID") = cmbLag.SelectedItem.Value
                Response.Cookies.Add(lagCookie)
                Session("lagCookie") = szGuid
            Else
                Session("lagCookie") = Server.HtmlEncode(lagCookie.Value)
                lagCookie.Item("LagID") = cmbLag.SelectedItem.Value
                If (lagCookie.Values.Count > 0) Then Session("lagCookie") = lagCookie.Values(0)
            End If
            Session("LagID") = lagCookie.Item("LagID")
            Response.Cookies.Add(lagCookie)

        Catch ex As Exception
            Session("LagID") = cmbLag.SelectedItem.Value
        End Try


        Try

            Me._pageData = Nothing
            LoadLAG()

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try


        Try
            ModGlobals.UpdateUserControls(Me.Page, True)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub

    Private Function GetIndexFromLagID(ByVal LagID As String) As Integer
        Select Case LagID
            Case "US"
                Return 0
                'Case "GR"
                '    Return 4
            Case "GR"
                Return 1
                'Case "DE"
                '    Return 3
                'Case "FR"
                '    Return 2
                'Case "HU"
                '    Return 5
                'Case "IT"
                '    Return 6
                'Case "RO"
                '    Return 7
            Case "AL"
                Return 2
                'Case "TR"
                '    Return 8
            Case Else
                Return 0

        End Select
    End Function


    Protected Sub LoadLAG()
        Try
            'PopulateMenuData(mnuHeaderLeft, "topLeft")
            'PopulateMenuData(mnuHeaderRight, "topRight")
            'PopulateMenuData(mnuFooter, "bottom")
            'PopulateMenuData(mnuHeader, "bottom")

            '_RootMaster.PopulateMenuData(mnuNavi, "topLeft")
            '_RootMaster.PopulateMenuData(mnuFooter1, "mnuFooter1")
            '_RootMaster.PopulateMenuData(mnuFooter2, "mnuFooter2")
            '_RootMaster.PopulateMenuData(mnuFooter3, "mnuFooter3")
            '_RootMaster.PopulateMenuData(mnuFooter4, "mnuFooter4")
            '_RootMaster.PopulateMenuData(mnuFooter5, "mnuFooter5")

            'For Each item As DevExpress.Web.ASPxMenu.MenuItem In mnuFooter2.Items
            '    If item.NavigateUrl.Contains("epoch.com") Then
            '        item.Target = "_blank"
            '    End If
            '    If item.NavigateUrl.Contains("Prices") Then
            '        If (clsCurrentContext.VerifyLogin) Then
            '            If (Me.IsFemale) Then
            '                item.NavigateUrl = "#"
            '            End If
            '        End If
            '    End If
            'Next


            'lblFooterMenuTitle1.Text = CurrentPageData.GetCustomString("lblFooterMenuTitle1")
            'lblFooterMenuTitle2.Text = CurrentPageData.GetCustomString("lblFooterMenuTitle2")
            'lblFooterMenuTitle3.Text = CurrentPageData.GetCustomString("lblFooterMenuTitle3")
            'lblFooterMenuTitle4.Text = CurrentPageData.GetCustomString("lblFooterMenuTitle4")
            'lblFooterMenuTitle5.Text = CurrentPageData.GetCustomString("lblFooterMenuTitle5")

            'tdFooterMenu1.Visible = (mnuFooter1.Items.Count > 0)
            'tdFooterMenu2.Visible = (mnuFooter2.Items.Count > 0)
            'tdFooterMenu3.Visible = (mnuFooter3.Items.Count > 0)
            'tdFooterMenu4.Visible = (mnuFooter4.Items.Count > 0)
            'tdFooterMenu5.Visible = (mnuFooter5.Items.Count > 0)



        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        'Try
        '    'lblFooterMenuHeader.Text = CurrentPageData.GetCustomString("lblFooterMenuHeader")
        '    lblFooterSiteTitle.Text = CurrentPageData.GetCustomString("lblFooterSiteTitle")
        '    lblFooterSiteDescription.Text = CurrentPageData.GetCustomString("lblFooterSiteDescription")
        '    lblFooterCopyRight.Text = CurrentPageData.GetCustomString("lblFooterCopyRight")
        '    'lblChartTitle.Text = CurrentPageData.GetCustomString("lblChartTitle")

        'Catch ex As Exception
        '    WebErrorMessageBox(Me, ex, "")
        'End Try

        'Try
        '    lblEscortsWarning.Text = CurrentPageData.GetCustomString("lblEscortsWarning")


        '    Dim lblEscortsWarningTooltip As Label = lblEscortsWarningPopup.FindControl("lblEscortsWarningTooltip")
        '    If (lblEscortsWarningTooltip IsNot Nothing) Then
        '        lblEscortsWarningTooltip.Text = CurrentPageData.GetCustomString("lblEscortsWarningTooltip")
        '        lblEscortsWarningPopup.Enabled = True
        '    Else
        '        lblEscortsWarningPopup.Enabled = False
        '    End If
        'Catch ex As Exception
        '    WebErrorMessageBox(Me, ex, "")
        'End Try


        'Try
        '    LoadLAG_LoginControl()
        'Catch ex As Exception
        '    WebErrorMessageBox(Me, ex, "")
        'End Try


    End Sub


    'Protected Sub LoadLAG_LoginControl()
    '    Try
    '        Dim cPageBasic As Dating.Server.Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
    '        Dim MainLogin As System.Web.UI.WebControls.Login = ucLogin1.LoginControl
    '        'MainLogin.UserNameLabelText = CurrentPageData.GetCustomString("txtUserNameLabelText")
    '        'MainLogin.PasswordLabelText = CurrentPageData.GetCustomString("txtPasswordLabelText")
    '        'MainLogin.RememberMeText = CurrentPageData.GetCustomString("txtRememberMeText")
    '        'MainLogin.LoginButtonText = CurrentPageData.GetCustomString("txtLoginButtonText")
    '        'MainLogin.UserNameRequiredErrorMessage = CurrentPageData.GetCustomString("txtUserNameRequiredErrorMessage")
    '        MainLogin.TitleText = CurrentPageData.GetCustomString("txtLoginTitleText")

    '        Dim lbForgotPassword As ASPxHyperLink = MainLogin.FindControl("lbForgotPassword")
    '        If (lbForgotPassword IsNot Nothing) Then
    '            lbForgotPassword.Text = CurrentPageData.GetCustomString("lbForgotPassword")
    '        End If

    '        Dim lbLoginFB As ASPxHyperLink = MainLogin.FindControl("lbLoginFB")
    '        If (lbLoginFB IsNot Nothing) Then
    '            lbLoginFB.Text = CurrentPageData.GetCustomString("lbLoginFB")
    '        End If

    '        Dim btnJoinToday As ASPxButton = MainLogin.FindControl("btnJoinToday")
    '        If (btnJoinToday IsNot Nothing) Then
    '            btnJoinToday.Text = CurrentPageData.GetCustomString("btnJoinToday")
    '            btnJoinToday.PostBackUrl = "~/Register.aspx"
    '            If (Not String.IsNullOrEmpty(Request.QueryString("ReturnUrl"))) Then
    '                btnJoinToday.PostBackUrl = btnJoinToday.PostBackUrl & "?ReturnUrl=" & Server.UrlEncode(Request.QueryString("ReturnUrl"))
    '            End If
    '        End If

    '        Dim UserName As ASPxTextBox = MainLogin.FindControl("UserName")
    '        If (UserName IsNot Nothing) Then
    '            'UserName.NullText = CurrentPageData.GetCustomString("UserName.NullText")
    '            UserName.ValidationSettings.RequiredField.ErrorText = CurrentPageData.GetCustomString("UserName.ValidationSettings.RequiredField.ErrorText")
    '        End If

    '        Dim Password As ASPxTextBox = MainLogin.FindControl("Password")
    '        If (Password IsNot Nothing) Then
    '            'Password.NullText = CurrentPageData.GetCustomString("Password.NullText")
    '            Password.ValidationSettings.RequiredField.ErrorText = CurrentPageData.GetCustomString("Password.ValidationSettings.RequiredField.ErrorText")
    '        End If

    '        Dim lblUsernameTop As Label = MainLogin.FindControl("lblUsernameTop")
    '        If (lblUsernameTop IsNot Nothing) Then
    '            lblUsernameTop.Text = CurrentPageData.GetCustomString("UserName.NullText")
    '        End If

    '        Dim lblPasswordTop As Label = MainLogin.FindControl("lblPasswordTop")
    '        If (lblPasswordTop IsNot Nothing) Then
    '            lblPasswordTop.Text = CurrentPageData.GetCustomString("Password.NullText")
    '        End If


    '        Dim btnLogin As ASPxButton = MainLogin.FindControl("btnLogin")
    '        If (btnLogin IsNot Nothing) Then
    '            btnLogin.Text = CurrentPageData.GetCustomString("btnLogin")
    '        End If

    '        Dim RememberMe As CheckBox = MainLogin.FindControl("RememberMe")
    '        If (RememberMe IsNot Nothing) Then
    '            RememberMe.Text = CurrentPageData.GetCustomString("RememberMe")
    '        End If


    '        Dim chkRememberMe As ASPxCheckBox = MainLogin.FindControl("chkRememberMe")
    '        If (chkRememberMe IsNot Nothing) Then
    '            chkRememberMe.Text = CurrentPageData.GetCustomString("RememberMe")
    '        End If


    '        '' controls found on login.aspx page
    '        Dim lblUserName As Literal = MainLogin.FindControl("lblUserName")
    '        If (lblUserName IsNot Nothing) Then
    '            lblUserName.Text = CurrentPageData.GetCustomString("lblUserName")
    '        End If


    '        Dim lblPassword As Literal = MainLogin.FindControl("lblPassword")
    '        If (lblPassword IsNot Nothing) Then
    '            lblPassword.Text = CurrentPageData.GetCustomString("lblPassword")
    '        End If

    '        Dim btnLoginNow As ASPxButton = MainLogin.FindControl("btnLoginNow")
    '        If (btnLoginNow IsNot Nothing) Then
    '            btnLoginNow.Text = CurrentPageData.GetCustomString("btnLoginNow")
    '            btnLoginNow.Text = btnLoginNow.Text.Replace("&nbsp;", " ")
    '        End If

    '    Catch ex As Exception
    '        WebErrorMessageBox(Me, ex, "")
    '    End Try
    'End Sub


    '    Public Shared Sub PopulateMenuData(menu As ASPxMenu, position As String, Optional visibleOnAdmistratorUserRole As Boolean? = Nothing, Optional visibleOnAfficateUserRole As Boolean? = Nothing, Optional visibleOnMemberUserRole As Boolean? = Nothing, Optional visibleOnPublicArea As Boolean? = Nothing, Optional VisibleOnResellerUserRole As Boolean? = Nothing)
    '        If menu Is Nothing Then Return

    '        Try

    '            menu.Items.Clear()

    '            Dim q As List(Of dsMenu.SiteMenuItemsRow) =
    '                (From i In Caching.SiteMenuItems Where _
    '                    i.Position.ToLower = position.ToLower AndAlso _
    '                    (i.VisibleOnAdmistratorUserRole = visibleOnAdmistratorUserRole OrElse visibleOnAdmistratorUserRole Is Nothing) AndAlso _
    '                    (i.VisibleOnAfficateUserRole = visibleOnAfficateUserRole OrElse visibleOnAfficateUserRole Is Nothing) AndAlso _
    '                    (i.VisibleOnMemberUserRole = visibleOnMemberUserRole OrElse visibleOnMemberUserRole Is Nothing) AndAlso _
    '                    (i.VisibleOnPublicArea = visibleOnPublicArea OrElse visibleOnPublicArea Is Nothing) AndAlso _
    '                    (i.VisibleOnResellerUserRole = VisibleOnResellerUserRole OrElse VisibleOnResellerUserRole Is Nothing) _
    '                    Where i.ParrentSiteMenuItemID = 0 AndAlso i.IsActive = True _
    '                    Order By i.SortNumber _
    '                    Select i).ToList()


    '            Dim cnt As Integer
    '            For cnt = 0 To q.Count - 1
    '                With q(cnt)

    '                    ' check the item is in list already
    '                    If menu.Items.FindByName(.SiteMenuItemID) IsNot Nothing Then
    '                        Continue For
    '                    End If

    '                    Select Case HttpContext.Current.Session("LagID")
    '                        Case "US"
    '                            If .US.Length > 0 Then
    '                                Dim mi As New DevExpress.Web.ASPxMenu.MenuItem(.US, .SiteMenuItemID, .ImageURL, .NavigateURL)
    '                                mi.ToolTip = .ToolTipUS
    '                                menu.Items.Add(mi)
    '                            Else
    '                                GoTo def
    '                            End If
    '                        Case "GR"
    '                            If .GR.Length > 0 Then
    '                                Dim mi As New DevExpress.Web.ASPxMenu.MenuItem(.GR, .SiteMenuItemID, .ImageURL, .NavigateURL)
    '                                mi.ToolTip = .ToolTipGR
    '                                menu.Items.Add(mi)
    '                            Else
    '                                GoTo def
    '                            End If
    '                        Case "HU"
    '                            If .HU.Length > 0 Then
    '                                menu.Items.Add(.HU, .SiteMenuItemID, .ImageURL, .NavigateURL)
    '                                menu.Items.FindByName(.SiteMenuItemID).ToolTip = .ToolTipHU
    '                            Else
    '                                GoTo def
    '                            End If
    '                        Case "ES"
    '                            If .ES.Length > 0 Then
    '                                menu.Items.Add(.ES, .SiteMenuItemID, .ImageURL, .NavigateURL)
    '                                menu.Items.FindByName(.SiteMenuItemID).ToolTip = .ToolTipES
    '                            Else
    '                                GoTo def
    '                            End If
    '                        Case "DE"
    '                            If .DE.Length > 0 Then
    '                                menu.Items.Add(.DE, .SiteMenuItemID, .ImageURL, .NavigateURL)
    '                                menu.Items.FindByName(.SiteMenuItemID).ToolTip = .ToolTipDE
    '                            Else
    '                                GoTo def
    '                            End If
    '                        Case "RO"
    '                            If .RO.Length > 0 Then
    '                                menu.Items.Add(.RO, .SiteMenuItemID, .ImageURL, .NavigateURL)
    '                                menu.Items.FindByName(.SiteMenuItemID).ToolTip = .ToolTipRO
    '                            Else
    '                                GoTo def
    '                            End If
    '                        Case "TU"
    '                            If .TU.Length > 0 Then
    '                                menu.Items.Add(.TU, .SiteMenuItemID, .ImageURL, .NavigateURL)
    '                                menu.Items.FindByName(.SiteMenuItemID).ToolTip = .ToolTipTU
    '                            Else
    '                                GoTo def
    '                            End If
    '                        Case "IT"
    '                            If .IT.Length > 0 Then
    '                                menu.Items.Add(.IT, .SiteMenuItemID, .ImageURL, .NavigateURL)
    '                                menu.Items.FindByName(.SiteMenuItemID).ToolTip = .ToolTipIT
    '                            Else
    '                                GoTo def
    '                            End If
    '                        Case "IL"
    '                            If .IL.Length > 0 Then
    '                                menu.Items.Add(.IL, .SiteMenuItemID, .ImageURL, .NavigateURL)
    '                                menu.Items.FindByName(.SiteMenuItemID).ToolTip = .ToolTipIL
    '                            Else
    '                                GoTo def
    '                            End If
    '                        Case "FR"
    '                            If .FR.Length > 0 Then
    '                                menu.Items.Add(.FR, .SiteMenuItemID, .ImageURL, .NavigateURL)
    '                                menu.Items.FindByName(.SiteMenuItemID).ToolTip = .ToolTipFR
    '                            Else
    '                                GoTo def
    '                            End If
    '                        Case "AL"
    '                            If Not .IsALNull() AndAlso .AL.Length > 0 Then
    '                                menu.Items.Add(.AL, .SiteMenuItemID, .ImageURL, .NavigateURL)
    '                                menu.Items.FindByName(.SiteMenuItemID).ToolTip = .ToolTipAL
    '                            Else
    '                                GoTo def
    '                            End If
    '                        Case "TR"
    '                            If .TR.Length > 0 Then
    '                                menu.Items.Add(.TR, .SiteMenuItemID, .ImageURL, .NavigateURL)
    '                                menu.Items.FindByName(.SiteMenuItemID).ToolTip = .ToolTipTR
    '                            Else
    '                                GoTo def
    '                            End If
    '                        Case Else 'default to US
    '                            GoTo def
    '                    End Select

    '                    GoTo skip 'skip default load for US
    'def:
    '                    If .US.Length > 0 OrElse (Not String.IsNullOrEmpty(.ImageURL)) Then
    '                        'menu.Items.Add(.US, .SiteMenuItemID, .ImageURL, .NavigateURL)
    '                        'menu.Items.FindByName(.SiteMenuItemID).ToolTip = .ToolTipUS
    '                        Dim mi As New DevExpress.Web.ASPxMenu.MenuItem(.US, .SiteMenuItemID, .ImageURL, .NavigateURL)
    '                        mi.ToolTip = .ToolTipUS
    '                        menu.Items.Add(mi)
    '                    End If
    'skip:
    '                    'do nothing
    '                End With
    '            Next

    '            'now load all sub menus for loaded root menus
    '            Dim qSub As List(Of dsMenu.SiteMenuItemsRow) = (From i In Caching.SiteMenuItems Where _
    '                    i.Position.ToLower = position.ToLower AndAlso _
    '                    (i.VisibleOnAdmistratorUserRole = visibleOnAdmistratorUserRole OrElse visibleOnAdmistratorUserRole Is Nothing) AndAlso _
    '                    (i.VisibleOnAfficateUserRole = visibleOnAfficateUserRole OrElse visibleOnAfficateUserRole Is Nothing) AndAlso _
    '                    (i.VisibleOnMemberUserRole = visibleOnMemberUserRole OrElse visibleOnMemberUserRole Is Nothing) AndAlso _
    '                    (i.VisibleOnPublicArea = visibleOnPublicArea OrElse visibleOnPublicArea Is Nothing) AndAlso _
    '                    (i.VisibleOnResellerUserRole = VisibleOnResellerUserRole OrElse VisibleOnResellerUserRole Is Nothing) _
    '                    Where i.ParrentSiteMenuItemID <> 0 AndAlso i.IsActive = True _
    '                    Order By i.SortNumber _
    '                    Select i).ToList()


    '            'For Each row As dsMenu.SiteMenuItemsRow In qSub
    '            For cnt = 0 To qSub.Count - 1
    '                With qSub(cnt)

    '                    Dim menuItem As MenuItem = menu.Items.FindByName(.ParrentSiteMenuItemID)
    '                    If menuItem IsNot Nothing Then

    '                        ' check the item is in list already
    '                        If menuItem.Items.FindByName(.SiteMenuItemID) IsNot Nothing Then
    '                            Continue For
    '                        End If

    '                        Select Case HttpContext.Current.Session("LagID")
    '                            Case "US"
    '                                If .US.Length > 0 Then
    '                                    menuItem.Items.Add(.US, .SiteMenuItemID, .ImageURL, .NavigateURL)
    '                                    menuItem.Items.FindByName(.SiteMenuItemID).ToolTip = .ToolTipUS
    '                                Else
    '                                    GoTo defSub
    '                                End If
    '                            Case "GR"
    '                                If .GR.Length > 0 Then
    '                                    menuItem.Items.Add(.GR, .SiteMenuItemID, .ImageURL, .NavigateURL)
    '                                    menuItem.Items.FindByName(.SiteMenuItemID).ToolTip = .ToolTipGR
    '                                Else
    '                                    GoTo defSub
    '                                End If
    '                            Case "HU"
    '                                If .HU.Length > 0 Then
    '                                    menuItem.Items.Add(.HU, .SiteMenuItemID, .ImageURL, .NavigateURL)
    '                                    menuItem.Items.FindByName(.SiteMenuItemID).ToolTip = .ToolTipHU
    '                                Else
    '                                    GoTo defSub
    '                                End If
    '                            Case "ES"
    '                                If .ES.Length > 0 Then
    '                                    menuItem.Items.Add(.ES, .SiteMenuItemID, .ImageURL, .NavigateURL)
    '                                    menuItem.Items.FindByName(.SiteMenuItemID).ToolTip = .ToolTipES
    '                                Else
    '                                    GoTo defSub
    '                                End If
    '                            Case "DE"
    '                                If .DE.Length > 0 Then
    '                                    menuItem.Items.Add(.DE, .SiteMenuItemID, .ImageURL, .NavigateURL)
    '                                    menuItem.Items.FindByName(.SiteMenuItemID).ToolTip = .ToolTipDE
    '                                Else
    '                                    GoTo defSub
    '                                End If
    '                            Case "RO"
    '                                If .RO.Length > 0 Then
    '                                    menuItem.Items.Add(.RO, .SiteMenuItemID, .ImageURL, .NavigateURL)
    '                                    menuItem.Items.FindByName(.SiteMenuItemID).ToolTip = .ToolTipRO
    '                                Else
    '                                    GoTo defSub
    '                                End If
    '                            Case "TU"
    '                                If .TU.Length > 0 Then
    '                                    menuItem.Items.Add(.TU, .SiteMenuItemID, .ImageURL, .NavigateURL)
    '                                    menuItem.Items.FindByName(.SiteMenuItemID).ToolTip = .ToolTipTU
    '                                Else
    '                                    GoTo defSub
    '                                End If
    '                            Case "IT"
    '                                If .IT.Length > 0 Then
    '                                    menuItem.Items.Add(.IT, .SiteMenuItemID, .ImageURL, .NavigateURL)
    '                                    menuItem.Items.FindByName(.SiteMenuItemID).ToolTip = .ToolTipIT
    '                                Else
    '                                    GoTo defSub
    '                                End If
    '                            Case "IL"
    '                                If .IL.Length > 0 Then
    '                                    menuItem.Items.Add(.IL, .SiteMenuItemID, .ImageURL, .NavigateURL)
    '                                    menuItem.Items.FindByName(.SiteMenuItemID).ToolTip = .ToolTipIL
    '                                Else
    '                                    GoTo defSub
    '                                End If
    '                            Case "FR"
    '                                If .FR.Length > 0 Then
    '                                    menuItem.Items.Add(.FR, .SiteMenuItemID, .ImageURL, .NavigateURL)
    '                                    menuItem.Items.FindByName(.SiteMenuItemID).ToolTip = .ToolTipFR
    '                                Else
    '                                    GoTo defSub
    '                                End If
    '                            Case "AL"
    '                                If Not .IsALNull() AndAlso .AL.Length > 0 Then
    '                                    menuItem.Items.Add(.AL, .SiteMenuItemID, .ImageURL, .NavigateURL)
    '                                    menuItem.Items.FindByName(.SiteMenuItemID).ToolTip = .ToolTipAL
    '                                Else
    '                                    GoTo defSub
    '                                End If
    '                            Case "TR"
    '                                If .TR.Length > 0 Then
    '                                    menuItem.Items.Add(.TR, .SiteMenuItemID, .ImageURL, .NavigateURL)
    '                                    menuItem.Items.FindByName(.SiteMenuItemID).ToolTip = .ToolTipTR
    '                                Else
    '                                    GoTo defSub
    '                                End If
    '                            Case Else 'default to US
    '                                GoTo defSub
    '                        End Select

    '                        GoTo skipSub 'skip default load for US
    'defSub:
    '                        If .US.Length > 0 Then
    '                            menuItem.Items.Add(.US, .SiteMenuItemID, .ImageURL, .NavigateURL)
    '                            menuItem.Items.FindByName(.SiteMenuItemID).ToolTip = .ToolTipUS
    '                        End If
    'skipSub:
    '                        'do nothing
    '                    End If
    '                End With
    '            Next

    '        Catch ex As System.NullReferenceException
    '            Try
    '                Caching.Reload()
    '            Catch ex1 As Exception

    '            End Try
    '            WebErrorMessageBox(menu.Page, ex, "")
    '        Catch ex As Exception
    '            WebErrorMessageBox(menu.Page, ex, "")
    '        End Try

    '    End Sub


    Public Sub PopulateMenuData(ByVal menu As ListView, ByVal position As String, Optional ByVal visibleOnAdmistratorUserRole As Boolean? = Nothing, Optional ByVal visibleOnAfficateUserRole As Boolean? = Nothing, Optional ByVal visibleOnMemberUserRole As Boolean? = Nothing, Optional ByVal visibleOnPublicArea As Boolean? = Nothing, Optional ByVal VisibleOnResellerUserRole As Boolean? = Nothing)
        If menu Is Nothing Then Return

        Dim _dt As DataTable = GetMenuData(menu.UniqueID, position, visibleOnAdmistratorUserRole, visibleOnAfficateUserRole, visibleOnMemberUserRole, visibleOnPublicArea, VisibleOnResellerUserRole)

        menu.Items.Clear()
        menu.DataSource = _dt
        menu.DataBind()
    End Sub



    Public Function GetMenuData(ByVal menuId As String, ByVal position As String, Optional ByVal visibleOnAdmistratorUserRole As Boolean? = Nothing, Optional ByVal visibleOnAfficateUserRole As Boolean? = Nothing, Optional ByVal visibleOnMemberUserRole As Boolean? = Nothing, Optional ByVal visibleOnPublicArea As Boolean? = Nothing, Optional ByVal VisibleOnResellerUserRole As Boolean? = Nothing) As DataTable

        If Session(menuId + Session("LagID") + position) Is Nothing Then
            Using da As New dsMenuTableAdapters.SiteMenuItemsTableAdapter
                Using con As New SqlClient.SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("AppDBconnectionString").ConnectionString)
                    da.Connection = con
                    Using dt As dsMenu.SiteMenuItemsDataTable = da.GetData(position, visibleOnAdmistratorUserRole, visibleOnAfficateUserRole, visibleOnMemberUserRole, visibleOnPublicArea, VisibleOnResellerUserRole)


                        Session(menuId + Session("LagID") + position) = dt
                    End Using
                End Using
            End Using
           
       
        End If


        Using _dt As New DataTable()



            For Each row As dsMenu.SiteMenuItemsRow In DirectCast(Session(menuId + Session("LagID") + position), dsMenu.SiteMenuItemsDataTable)

                If (_dt.Columns.Count = 0) Then
                    _dt.Columns.Add("Text", String.Empty.GetType())
                    _dt.Columns.Add("Name", row.SiteMenuItemID.GetType())
                    _dt.Columns.Add("ImageUrl", String.Empty.GetType())
                    _dt.Columns.Add("NavigateUrl", String.Empty.GetType())
                    _dt.Columns.Add("ToolTip", String.Empty.GetType())
                    _dt.Columns.Add("CssClass", String.Empty.GetType())
                End If


                Dim _text As String = ""
                Dim _toolTip As String = ""

                With row

                    ' by default use english
                    _text = .US
                    _toolTip = .ToolTipUS

                    Select Case Session("LagID")
                        Case "GR"
                            If .GR.Length > 0 Then
                                _text = .GR
                                _toolTip = .ToolTipGR
                            End If
                        Case "HU"
                            If .HU.Length > 0 Then
                                _text = .HU
                                _toolTip = .ToolTipHU
                            End If
                        Case "ES"
                            If .ES.Length > 0 Then
                                _text = .ES
                                _toolTip = .ToolTipES
                            End If
                        Case "DE"
                            If .DE.Length > 0 Then
                                _text = .DE
                                _toolTip = .ToolTipDE
                            End If
                        Case "RO"
                            If .RO.Length > 0 Then
                                _text = .RO
                                _toolTip = .ToolTipRO
                            End If
                        Case "TU"
                            If .TU.Length > 0 Then
                                _text = .TU
                                _toolTip = .ToolTipTU
                            End If
                        Case "IT"
                            If .IT.Length > 0 Then
                                _text = .IT
                                _toolTip = .ToolTipIT
                            End If
                        Case "IL"
                            If .IL.Length > 0 Then
                                _text = .IL
                                _toolTip = .ToolTipIL
                            End If
                        Case "FR"
                            If .FR.Length > 0 Then
                                _text = .FR
                                _toolTip = .ToolTipFR
                            End If
                        Case "AL"
                            If Not .IsALNull() AndAlso .AL.Length > 0 Then
                                _text = .AL
                                _toolTip = .ToolTipAL
                            End If
                        Case "TR"
                            If .TR.Length > 0 Then
                                _text = .TR
                                _toolTip = .ToolTipTR
                            End If
                    End Select

                End With


                Dim _dr As DataRow = _dt.NewRow()

                _dr("Name") = row.SiteMenuItemID
                _dr("ImageUrl") = row.ImageURL
                _dr("NavigateUrl") = row.NavigateURL
                _dr("Text") = _text
                _dr("ToolTip") = _toolTip
                _dt.Rows.Add(_dr)
            Next

            Return _dt
        End Using
    End Function

    'Public Sub AddNaviLink(ByVal name As String, ByVal link As String) Implements INavigation.AddNaviLink
    '    If (mnuNavi.Items.Count = 0) Then
    '        mnuNavi.Items.Add("Home", "Home", "", "~/Default.aspx")
    '    End If
    '    mnuNavi.Items.Add(name, name, "", link)
    'End Sub



    Private Function SetSessionCity(ByRef _dt As DataTable) As Boolean
        Dim re As Boolean = False
        If _dt.Rows.Count > 0 Then
            Dim lat As Double = _dt.Rows(0)("latitude")
            Dim lng As Double = _dt.Rows(0)("longitude")
            'ShowUserMap(lat, lng, radius, zoom)

            Session("GEO_COUNTRY_CITY") = _dt.Rows(0)("city")
            Session("GEO_COUNTRY_LATITUDE") = lat
            Session("GEO_COUNTRY_LONGITUDE") = lng
            Session("GEO_COUNTRY_POSTALCODE") = _dt.Rows(0)("postcode")
            re = True
        End If
        Return re
    End Function
    Public Sub ShowUserMap()
        Try




            Dim zoom As Integer = 9
            Dim radius As Integer = 10


            Dim b As Boolean = False
            If (Not String.IsNullOrEmpty(Session("GEO_COUNTRY_POSTALCODE"))) Then
                Using dt = clsGeoHelper.GetGEOByZip(Session("GEO_COUNTRY_CODE"), Session("GEO_COUNTRY_POSTALCODE"), Session("LAGID"))
                    b = SetSessionCity(dt)
                End Using


            ElseIf (Not String.IsNullOrEmpty(Session("GEO_COUNTRY_LATITUDE")) AndAlso Not String.IsNullOrEmpty(Session("GEO_COUNTRY_LONGITUDE"))) Then
                Dim sLAT As String = Session("GEO_COUNTRY_LATITUDE")
                Dim sLON As String = Session("GEO_COUNTRY_LONGITUDE")

                Dim periodLAT As Integer = sLAT.IndexOf(".")
                If (Len(sLAT) > periodLAT + 3) Then sLAT = sLAT.Remove(periodLAT + 3)

                Dim periodLON As Integer = sLON.IndexOf(".")
                If (Len(sLON) > periodLON + 3) Then sLON = sLON.Remove(periodLON + 3)

                Using dt = clsGeoHelper.GetGEOWithLatitudeAndLongitude(Session("GEO_COUNTRY_CODE"), sLAT, sLON, Session("LAGID"))
                    b = SetSessionCity(dt)
                End Using

            ElseIf (Not String.IsNullOrEmpty(Session("GEO_COUNTRY_CITY"))) Then
                Using dt = clsGeoHelper.GetGEOWithCity(Session("GEO_COUNTRY_CODE"), Session("GEO_COUNTRY_CITY"))
                    b = SetSessionCity(dt)
                End Using


            End If

            If (Not b) Then
                Using dt = clsGeoHelper.GetGEOByZip("GR", "10431", Session("LAGID"))
                    SetSessionCity(dt)
                End Using
              
            End If

         
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        Finally
        End Try

    End Sub

    'Public Sub ShowUserMap(ByVal lat As Double, ByVal lng As Double, ByVal radius As Integer, Optional ByVal zoom As Integer = 8)

    '    Try
    '        Dim lang As String = ""
    '        If (Session("GR")) Then lang = "&language=el"

    '        Dim mapPath As String = System.Web.VirtualPathUtility.ToAbsolute("~/map.aspx?lat=" & lat & "&lng=" & lng & "&radius=" & radius & "&zoom=" & zoom & lang)

    '        divMapContainer.Visible = True
    '        ifrUserMap.Attributes.Add("src", mapPath)

    '    Catch ex As Exception
    '        WebErrorMessageBox(Me, ex, "")
    '    End Try

    'End Sub



    'Protected Sub btnEnglish_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnEnglish.Click
    '    cmbLag.Items(GetIndexFromLagID("US")).Selected = True
    '    cmbLag_SelectedIndexChanged(cmbLag, New EventArgs())
    'End Sub

    'Protected Sub btnGerman_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnGerman.Click
    '    cmbLag.Items(GetIndexFromLagID("DE")).Selected = True
    '    cmbLag_SelectedIndexChanged(cmbLag, New EventArgs())
    'End Sub

    'Protected Sub btnGreek_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnGreek.Click
    '    cmbLag.Items(GetIndexFromLagID("GR")).Selected = True
    '    cmbLag_SelectedIndexChanged(cmbLag, New EventArgs())
    'End Sub

    'Protected Sub btnHungarian_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnHungarian.Click
    '    cmbLag.Items(GetIndexFromLagID("HU")).Selected = True
    '    cmbLag_SelectedIndexChanged(cmbLag, New EventArgs())
    'End Sub

    Private Sub SetLanguageLinks()
        Try


            Dim url As String = Request.Url.AbsolutePath
            url = url & "?"

            For Each itm As String In Request.QueryString.AllKeys
                If (Not String.IsNullOrEmpty(itm) AndAlso itm.ToUpper() <> "LAG") Then
                    url = url & itm & "=" & Request.QueryString(itm) & "&"
                End If
            Next



            lnkEn.NavigateUrl = url & "lag=US"
            lnkGR.NavigateUrl = url & "lag=GR"
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub

End Class
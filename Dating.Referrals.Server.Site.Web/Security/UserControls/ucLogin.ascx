﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucLogin.ascx.vb" Inherits="Dating.Referrals.Server.Site.Web.ucLogin" %>


<asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">

    <asp:View ID="vwStandard" runat="server">
        <asp:Login ID="MainLogin" runat="server">
        </asp:Login>
        <div style="margin:10px;"><dx:ASPxLabel ID="lblError" runat="server" Text="" 
                ForeColor="red" Visible="false" EnableViewState="false" EncodeHtml="False">
            </dx:ASPxLabel></div>
    </asp:View>
    <asp:View ID="vwLoggedInUser" runat="server">
<div style="position:absolute; right:0px;">
    <asp:HyperLink ID="lblLoginName" runat="server" NavigateUrl="~/Members/default.aspx" ForeColor="Black">
    <asp:LoginName ID="LN" runat="server" FormatString="Welcome, {0}" Font-Size="12pt" Font-Bold="true" ForeColor="Black"/>
            </asp:HyperLink>&nbsp;&nbsp;&nbsp;&nbsp;

<dx:ASPxLabel ID="lblLoginDate" runat="server" Text="Login: ###LOGINDATE###" EncodeHtml="False" ForeColor="Black">
            </dx:ASPxLabel>

<div style="height:8px;">&nbsp;</div>
</div>
<%--<asp:HyperLink ID="lblMembersArea" runat="server" Text="Members Area" NavigateUrl="~/Members/default.aspx" ForeColor="White" Font-Size="11pt">
            </asp:HyperLink >
--%>
<%--<div style="margin-bottom:4px;" class="rfloat">--%><div style="position:absolute;right:0px;bottom:45px;">
    <dx:ASPxButton ID="lblMembersArea" runat="server" Text="Members&nbsp;Area"
                                            EncodeHtml="False" Height="32px" Font-Bold="True" 
                                            PostBackUrl="~/Members/default.aspx" 
            UseSubmitBehavior="false" BackColor="#000000" Cursor="pointer" 
                EnableDefaultAppearance="False" Font-Size="13px" ForeColor="White" 
                Width="150px">
                                            <Border BorderWidth="1px"></Border>
                                            <ClientSideEvents Click="function(s, e) { LoadingPanel.Show(); }" />
                                        </dx:ASPxButton>
</div>
<div class="clear"></div>
<%--
            <table cellspacing="0" cellpadding="0">
                <tr>
                    <td><div class="welcomeUser" style="font-size:18px;margin:2px 15px 2px 5px;"><asp:LoginName ID="LN" runat="server" FormatString="Welcome, {0}" /></div></td>
                    <td><dx:ASPxButton ID="btnLogout" runat="server" Text="Logout" PostBackUrl="~/Logout.aspx"
                            EncodeHtml="False" Border-BorderWidth="1px" Paddings-Padding="0"
                            Height="31px" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                            CssPostfix="Office2003Blue" Font-Bold="True" 
                            SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"></dx:ASPxButton>
                    </td>
                </tr>
            </table>
--%>
        
    </asp:View>


</asp:MultiView>


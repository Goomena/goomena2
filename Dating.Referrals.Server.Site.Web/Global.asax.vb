﻿Imports System.Web.SessionState
Imports Library.Public
Imports System.Reflection
Imports Dating.Server.Core.DLL

Public Class Global_asax
    Inherits System.Web.HttpApplication

    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        Dim getSyntax As MethodInfo = GetType(UriParser).GetMethod("GetSyntax", System.Reflection.BindingFlags.[Static] Or System.Reflection.BindingFlags.NonPublic)
        Dim flagsField As FieldInfo = GetType(UriParser).GetField("m_Flags", System.Reflection.BindingFlags.Instance Or System.Reflection.BindingFlags.NonPublic)
        If getSyntax IsNot Nothing AndAlso flagsField IsNot Nothing Then
            For Each scheme As String In New String() {"http", "https"}
                Dim parser As UriParser = DirectCast(getSyntax.Invoke(Nothing, New Object() {scheme}), UriParser)
                If parser IsNot Nothing Then
                    Dim flagsValue As Integer = CInt(flagsField.GetValue(parser))
                    ' Clear the CanonicalizeAsFilePath attribute
                    If (flagsValue And &H1000000) <> 0 Then
                        flagsField.SetValue(parser, flagsValue And Not &H1000000)
                    End If
                End If
            Next
        End If

        Dating.Server.Core.DLL.DataHelpers.SetConnectionString(System.Configuration.ConfigurationManager.ConnectionStrings("AppDBconnectionString").ConnectionString)
        Dating.Referrals.Server.Core.DLL.DataHelpers.SetConnectionString(System.Configuration.ConfigurationManager.ConnectionStrings("AppDBconnectionString").ConnectionString)
        gLAG.Init(ConfigurationManager.AppSettings("DB_ServerName").ToString(), 1433, ConfigurationManager.AppSettings("DB_Database").ToString(), False, ConfigurationManager.AppSettings("DB_Login").ToString(), ConfigurationManager.AppSettings("DB_Password").ToString())

        'Dim cd As New Library.Public.SQLConnectionStringData
        'cd.DB_DataBase = ConfigurationManager.AppSettings("DB_ServerName").ToString()
        'cd.DB_Login = ConfigurationManager.AppSettings("DB_Login").ToString()
        'cd.DB_Password = ConfigurationManager.AppSettings("DB_Password").ToString()
        'cd.DB_DataBase = ConfigurationManager.AppSettings("DB_Database").ToString()

        'cCnn.m_ConnectionString = Library.Public.SQLFunctions.GetADOConnectString(cd)
        'cCnn.m_UsingProviderType = ProviderType.MsSQL



        System.Net.ServicePointManager.Expect100Continue = False
        System.Net.ServicePointManager.DefaultConnectionLimit = 10000

    End Sub


    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        '  Code that runs on application shutdown

    End Sub

    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when an unhandled error occurs

    End Sub

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)

        ' Code that runs when a new session is started
        'Session.Timeout = 960

        Try
            SetReferer(Context)
        Catch ex As Exception

        End Try

        If (Session("ProfileID") Is Nothing) Then
            Session("ProfileID") = 0
            Session("MirrorProfileID") = 0
        End If

        If HttpContext.Current.User IsNot Nothing Then
            If HttpContext.Current.User.Identity.IsAuthenticated AndAlso Not Request("logout") = "1" Then
                Dim LoginName As String = HttpContext.Current.User.Identity.Name
                Dim id As System.Web.Security.FormsIdentity = HttpContext.Current.User.Identity
                Dim ticket As System.Web.Security.FormsAuthenticationTicket = id.Ticket
                Session("UserRole") = ticket.UserData
                '!!Session("UserInfo") = ticket.UserData

                Try
                    If (Not String.IsNullOrEmpty(Request("logon_customer")) AndAlso (Session("IsAdminLogin") Is Nothing OrElse Session("IsAdminLogin") = False)) Then
                        Try
                            Dim result As Dating.Referrals.Server.Core.DLL.clsDataRecordLoginMemberReturn = gLogin.PerfomAdminLogin(Request("logon_customer"))
                            If (result IsNot Nothing AndAlso result.IsValid) Then
                                Session("IsAdminLogin") = True
                            End If
                        Catch ex As Exception
                        End Try
                    Else
                        gLogin.PerfomRelogin(LoginName, True)
                    End If
                Catch ex As Exception

                End Try
            End If
        End If

        If (Session("SessionVariables") Is Nothing) Then
            Session("SessionVariables") = New clsSessionVariables()
        End If



        Session("Agent") = Request.ServerVariables("HTTP_USER_AGENT") '-- the browser the user uses to visit your site

        Session("IP") = clsCurrentContext.GetCurrentIP()
        'Session("IP") = Request.ServerVariables("REMOTE_ADDR") '-- the IP address of the visitor
        'If (Session("IP").ToString().StartsWith("127.") OrElse Session("IP").ToString().Equals("::1")) Then
        '    Session("IP") = "46.246.144.52" '"79.130.17.211"
        'End If

        'Geo location IP Functions
        Try
            'Dim GeoLocationSite As String = "http://api.hostip.info/country.php?ip="
            ''Dim GeoLocationSite2 As String = "http://api.wipmania.com/"
            '' second site for backup :"http://api.wipmania.com/145.145.67.121"

            ' api.hostip.info OFF
            'Dim cDownloads As New Library.Public.ClsDownloadViaHTTP
            'Dim GeoSiteULR As String = GeoLocationSite & Session("IP")
            'Dim ReturnData As String = String.Empty
            'If cDownloads.DownloadData(GeoSiteULR, ReturnData) Then
            '    Session("GEO_COUNTRY_CODE") = Mid(ReturnData, 1, 2)
            '    Session("GEO_COUNTRY_CODE_BY_SITE") = GeoLocationSite
            'Else
            '    Session("GEO_COUNTRY_CODE") = "XX"
            'End If

            ' wipmania OFF
            'If Session("GEO_COUNTRY_CODE") = "XX" Then
            '    GeoSiteULR = GeoLocationSite2 & Session("IP")
            '    If cDownloads.DownloadData(GeoSiteULR, ReturnData) Then
            '        Session("GEO_COUNTRY_CODE") = Right(ReturnData, 2)
            '        Session("GEO_COUNTRY_CODE_BY_SITE") = GeoLocationSite2
            '    Else
            '        Session("GEO_COUNTRY_CODE") = "US"
            '    End If
            'End If

            ''If Session("GEO_COUNTRY_CODE") = "XX" Then
            'Session("GEO_COUNTRY_CODE") = "XX"
            'Dim ws As New GeoService.GeoService()
            'ws.Url = ConfigurationManager.AppSettings("WSGeoServiceURL")
            'ws.UseDefaultCredentials = True
            'Dim ipData As DataSet = ws.GetAllDataFromIP(Session("IP"), generateToken())

            'Dim countryCode As String = ipData.Tables(0).Rows(0)("Country").ToString().TrimStart("""").TrimEnd("""")
            'Dim postalCode As String = ipData.Tables(0).Rows(0)("PostalCode").ToString().TrimStart("""").TrimEnd("""")
            'Dim city As String = ipData.Tables(0).Rows(0)("City").ToString().TrimStart("""").TrimEnd("""")
            'Dim latitude As String = ipData.Tables(0).Rows(0)("Latitude").ToString().TrimStart("""").TrimEnd("""")
            'Dim longitude As String = ipData.Tables(0).Rows(0)("Longitude").ToString().TrimStart("""").TrimEnd("""")

            ''= ws.GetCountryCodeFromIP(Session("IP"), generateToken())

            'ws.Dispose()

            'If (Len(countryCode) = 2) Then
            '    Session("GEO_COUNTRY_CODE") = countryCode
            '    Session("GEO_COUNTRY_CODE_BY_SITE") = ConfigurationManager.AppSettings("WSGeoServiceURL")
            '    Session("GEO_COUNTRY_LATITUDE") = latitude
            '    Session("GEO_COUNTRY_LONGITUDE") = longitude
            '    Session("GEO_COUNTRY_POSTALCODE") = postalCode
            '    Session("GEO_COUNTRY_CITY") = city
            'End If
            ''End If

            Dim country As clsCountryByIP = clsCountryByIP.GetCountryCodeFromIP(Session("IP"), ConfigurationManager.ConnectionStrings("GeoDBconnectionString").ConnectionString)

            If (Len(country.CountryCode) = 2) Then
                Session("GEO_COUNTRY_CODE") = country.CountryCode
                Session("GEO_COUNTRY_CODE_BY_SITE") = "sql server" 'ConfigurationManager.AppSettings("WSGeoServiceURL")
                Session("GEO_COUNTRY_LATITUDE") = country.latitude
                Session("GEO_COUNTRY_LONGITUDE") = country.longitude
                Session("GEO_COUNTRY_POSTALCODE") = country.postalCode
                Session("GEO_COUNTRY_CITY") = country.city
            End If


        Catch ex As Exception
            Try
                WebErrorSendEmail(ex, "GEO_COUNTRY_CODE")
            Catch : End Try
            Session("GEO_COUNTRY_CODE") = "US"
        End Try

        If Session("GEO_COUNTRY_CODE") = "XX" Then
            Session("GEO_COUNTRY_CODE") = "US"
        End If

        ' End GEO


        Try
            'Session("LagID") = gDefaultLAG

            Dim lagCookie As HttpCookie = Request.Cookies("lagCookie")
            If lagCookie Is Nothing Then
                Dim szGuid As String = System.Guid.NewGuid().ToString()
                lagCookie = New HttpCookie("lagCookie", szGuid)
                lagCookie.Expires = DateTime.Now.AddYears(2)
                lagCookie.Item("LagID") = Session("GEO_COUNTRY_CODE")
                Response.Cookies.Add(lagCookie)
                Session("lagCookie") = szGuid

                If (Session("LagID") Is Nothing) Then
                    Session("LagID") = lagCookie.Item("LagID")
                End If

                If Request.Url.Host.EndsWith(".gr", StringComparison.OrdinalIgnoreCase) Then
                    'goomena.gr
                    Session("LagID") = "GR"
                Else

                    If Request.Url.Host.EndsWith(".com", StringComparison.OrdinalIgnoreCase) Then
                        'goomena.com
                        If (Session("LagID") Is Nothing) Then Session("LagID") = "US"
                    End If

                End If
            Else
                Session("lagCookie") = Server.HtmlEncode(lagCookie.Value)
                Session("LagID") = lagCookie.Item("LagID")
            End If


            If (Session("LagID") Is Nothing) Then
                ' ex	Request.Params("HTTP_ACCEPT_LANGUAGE") = "ru,en-US;q=0.8,en;q=0.6"	
                Dim browserLag As String = Request.Params("HTTP_ACCEPT_LANGUAGE")
                If (Not String.IsNullOrEmpty(browserLag)) Then
                    browserLag = browserLag.Split(","c, ";"c)(0)
                    Dim cultName As String() = browserLag.Split("-"c)
                    If (cultName.Length = 1) Then
                        ' ex "en"
                        If (AppUtils.IsSupportedLAG(cultName(0))) Then Session("LagID") = cultName(0).ToUpper()
                    ElseIf (cultName.Length = 2) Then
                        ' ex "en-US"
                        If (AppUtils.IsSupportedLAG(cultName(1))) Then Session("LagID") = cultName(1).ToUpper()
                    End If
                End If
            End If


            If (Session("LagID") Is Nothing) Then
                Session("LagID") = "US"
            End If

        Catch ex As Exception
            Session("LagID") = Session("GEO_COUNTRY_CODE")
        End Try

    End Sub

    Private Function generateToken() As String
        Dim secretKey As String = "@!$@#%#$^&^%TYTUYTUYTDSAW#$%#@$%"
        'Create an encoding object to ensure the encoding standard for the source text
        Dim Ue As New UnicodeEncoding()
        'Retrieve a byte array based on the source text
        Dim ByteSourceText() As Byte = Ue.GetBytes("GeoLocation1234" & secretKey)
        'Instantiate an MD5 Provider object
        Using Md5 As New System.Security.Cryptography.MD5CryptoServiceProvider()
            'Compute the hash value from the source
            Dim ByteHash() As Byte = Md5.ComputeHash(ByteSourceText)
            'And convert it to String format for return
            Return Convert.ToBase64String(ByteHash)
        End Using
      

    End Function


    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        Try
            clsCurrentContext.ClearSession(Session)
            'If (Session("ProfileID") > 0 AndAlso (Session("IsAdminLogin") Is Nothing OrElse Session("IsAdminLogin") = False)) Then
            '    DataHelpers.UpdateEUS_Profiles_LogoutData(Session("ProfileID"))

            '    'Try
            '    '    DataHelpers.LogProfileAccess(Session("ProfileID"), userName, password, DataRecordLoginReturn.IsValid, "Login")
            '    'Catch ex As Exception
            '    'End Try
            'End If

            'If (Session("ProfileID") > 0) Then
            '    Try
            '        'Dim sesVars As clsSessionVariables = DirectCast(HttpContext.Current.Session("SessionVariables"), clsSessionVariables)
            '        'Dim DataRecordLoginReturn As clsDataRecordLoginMemberReturn = sesVars.DataRecordLoginMemberReturn
            '        If (Session("IsAdminLogin") = True) Then
            '            DataHelpers.LogProfileAccess(Session("ProfileID"), Session("LoginName"), "", True, "Admin Logout - Session End")
            '        Else
            '            DataHelpers.LogProfileAccess(Session("ProfileID"), Session("LoginName"), "", True, "Logout - Session End")
            '        End If
            '    Catch ex As Exception
            '    End Try
            'End If

            'Session("Logoff") = Nothing
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub Application_AuthenticateRequest(sender As Object, e As EventArgs)
        'Get authentication cookie
        Dim sCookieName As String = FormsAuthentication.FormsCookieName
        Dim authCookie As HttpCookie = Request.Cookies.Get(sCookieName)

        'If the cookie cannot be found then do not issue the ticket
        If authCookie Is Nothing Then
            Exit Sub
        End If

        If authCookie.Value IsNot Nothing AndAlso authCookie.Value.Length > 0 Then
            'Get the ticket and rebuild the principal and identity
            Dim authTicket As FormsAuthenticationTicket = FormsAuthentication.Decrypt(authCookie.Value)

            'Get the Roles
            Dim sRoles() As String = authTicket.UserData.Split(",")
            'Build the identity
            Dim userIdentity As New System.Web.Security.FormsIdentity(authTicket)

            'Build the Principal
            Dim userPrincipal As New System.Security.Principal.GenericPrincipal(userIdentity, sRoles)

            HttpContext.Current.User = userPrincipal
        End If
    End Sub



    Private Sub Global_asax_BeginRequest(sender As Object, e As System.EventArgs) Handles Me.BeginRequest
        ' no chache for testing purposes
        If (Context.Request.Url.Query = "?nocache" OrElse Context.Request.Url.Query.Contains("&nocache")) Then
            ClearCache()
        End If

        'If (Session("CustomReferrer") = "none" AndAlso (Not String.IsNullOrEmpty(Request.QueryString("c")) OrElse Not String.IsNullOrEmpty(Request.QueryString("cref")))) Then
        '    SetReferer(Context)
        'End If
    End Sub

    Private Sub Application_PreRequestHandlerExecute(sender As Object, e As System.EventArgs) Handles Me.PreRequestHandlerExecute
        If (Not String.IsNullOrEmpty(Context.Request("country"))) Then
            Dim countrys As String() = New String() {"GR", "AL"}
            If (countrys.Contains(Context.Request("country").ToUpper())) Then
                Session("GEO_COUNTRY_CODE") = Context.Request("country").ToUpper()
            End If
        End If
    End Sub


    Private Sub ClearCache()
        clsPageData.ClearCache()
        Caching.Current.Reload()
        Lists.ClearCache()
        ProfileHelper.ClearCache()
        clsPricing.ClearCache()
        clsConfigValues.ClearCache()
    End Sub

End Class


'Sub Application_AuthenticateRequest(ByVal sender As Object, ByVal e As EventArgs)
'    If (Not (HttpContext.Current.User Is Nothing)) Then
'        If (HttpContext.Current.User.Identity.IsAuthenticated) Then
'            If HttpContext.Current.User.Identity.AuthenticationType = "Forms" Then
'                Dim id As System.Web.Security.FormsIdentity = HttpContext.Current.User.Identity
'                Dim ticket As System.Web.Security.FormsAuthenticationTicket = id.Ticket
'                Dim myRoles(1) As String
'                myRoles(0) = ticket.UserData
'                HttpContext.Current.User = New System.Security.Principal.GenericPrincipal(id, myRoles)

'            End If
'        End If
'    End If
'End Sub


'Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
'    ' Code that runs when a new session is started
'    Session.Timeout = 960

'    ' Ini Session Vars for Staticals
'    ' REFERRER --------------------------------------------------------
'    Dim Referrer As String = ""
'    Referrer = Request.ServerVariables("HTTP_REFERER") '-- where the user was before he/she came to your site
'    Try
'        If Not Referrer Is Nothing And Len(Referrer > 0) Then
'            Referrer = Referrer.Replace("http://", "")
'            Referrer = Referrer.Substring(0, Referrer.IndexOf("/"))
'        End If
'    Catch ex As Exception
'    End Try

'    If Not Referrer Is Nothing Then
'        If Not Referrer.Contains(gSiteName) Then
'            Session("Referrer") = Referrer
'        End If
'    End If
'    If Len(Session("Referrer")) = 0 Then Session("Referrer") = "none"
'    ' REFERRER --------------------------------------------------------

'    Session("CustomReferrer") = Trim(Request("cref"))
'    If Len(Session("CustomReferrer")) = 0 Then
'        Session("CustomReferrer") = Trim(Request("c"))
'    End If
'    Dim refCookie As HttpCookie = Request.Cookies("refCookie")



'    If Len(Session("CustomReferrer")) = 0 Then
'        If Len(Referrer) Then
'            Try
'                'Dim rs As New ClsMyADODB2
'                'If rs.Open("SELECT * FROM EUS_Customers WHERE AffiliateSiteURL1 like '%" & Referrer & "%' OR AffiliateSiteURL2 like '%" & Referrer & "%' OR AffiliateSiteURL3 like '%" & Referrer & "%' ", ClsMyADODB2.OpenMode.ReadOnlyMode) Then
'                '    If rs.RecordCount >= 1 Then
'                '        Session("CustomReferrer") = rs.GetFieldValue("AffiliateCode", "none")
'                '    End If
'                'End If
'                'rs.Close()
'            Catch ex As Exception
'            End Try
'        End If
'    End If
'    Try


'        If Len(Session("CustomReferrer")) > 0 Then
'            Dim szGuid As String = System.Guid.NewGuid().ToString()
'            refCookie = New HttpCookie("refCookie", szGuid)
'            refCookie.Expires = DateTime.Now.AddDays(40)
'            refCookie.Item("customReferrer") = Session("CustomReferrer")
'            Response.Cookies.Add(refCookie)
'            Session("refCookie") = szGuid
'        End If
'    Catch ex As Exception
'    End Try

'    Try
'        If Len(Session("CustomReferrer")) = 0 Then
'            If Not refCookie Is Nothing Then
'                Session("refCookie") = Server.HtmlEncode(refCookie.Value)
'                Session("CustomReferrer") = refCookie.Item("customReferrer")
'            End If
'        End If
'    Catch ex As Exception
'    End Try
'    Try


'        If Len(Session("CustomReferrer")) = 0 Then Session("CustomReferrer") = "none"


'    Catch ex As Exception

'    End Try

'    Session("IP") = Request.ServerVariables("REMOTE_ADDR") '-- the IP address of the visitor
'    Session("Agent") = Request.ServerVariables("HTTP_USER_AGENT") '-- the browser the user uses to visit your site
'    Try


'        Dim FirstTimeCookie As HttpCookie = Request.Cookies("FirstTimeCookie")
'        If FirstTimeCookie Is Nothing Then
'            Dim szGuid As String = System.Guid.NewGuid().ToString()
'            FirstTimeCookie = New HttpCookie("FirstTimeCookie", szGuid)
'            FirstTimeCookie.Item("FirstTimeVisit") = DateTime.Now()
'            FirstTimeCookie.Expires = DateTime.Now.AddYears(18)
'            FirstTimeCookie.Item("FirstReferrer") = Referrer
'            Response.Cookies.Add(FirstTimeCookie)
'            Session("FirstTimeCookie") = szGuid
'        Else
'            Session("FirstTimeCookie") = Server.HtmlEncode(FirstTimeCookie.Value)
'        End If


'        Dim statCookie As HttpCookie = Request.Cookies("StatCookie")
'        If statCookie Is Nothing Then
'            Dim szGuid As String = System.Guid.NewGuid().ToString()
'            statCookie = New HttpCookie("StatCookie", szGuid)
'            statCookie.Expires = DateTime.Now.AddYears(2)
'            Response.Cookies.Add(statCookie)
'            Session("StatCookie") = szGuid
'        Else
'            Session("StatCookie") = Server.HtmlEncode(statCookie.Value)
'        End If

'    Catch ex As Exception

'    End Try

'    'Geo location IP Functions
'    Try
'        Dim GeoLocationSite As String = "http://api.hostip.info/country.php?ip="
'        Dim GeoLocationSite2 As String = "http://api.wipmania.com/"
'        ' second site for backup :"http://api.wipmania.com/145.145.67.121"
'        Dim cDownloads As New Library.Public.ClsDownloadViaHTTP
'        Dim GeoSiteULR As String = GeoLocationSite & Session("IP")
'        Dim ReturnData As String
'        If cDownloads.DownloadData(GeoSiteULR, ReturnData) Then
'            Session("GEO_COUNTRY_CODE") = Mid(ReturnData, 1, 2)
'            Session("GEO_COUNTRY_CODE_BY_SITE") = GeoLocationSite
'        Else
'            Session("GEO_COUNTRY_CODE") = "XX"
'        End If
'        If Session("GEO_COUNTRY_CODE") = "XX" Then
'            GeoSiteULR = GeoLocationSite2 & Session("IP")
'            If cDownloads.DownloadData(GeoSiteULR, ReturnData) Then
'                Session("GEO_COUNTRY_CODE") = Mid(ReturnData, 1, 2)
'                Session("GEO_COUNTRY_CODE_BY_SITE") = GeoLocationSite2
'            Else
'                Session("GEO_COUNTRY_CODE") = "US"
'            End If
'        End If


'    Catch ex As Exception
'        Session("GEO_COUNTRY_CODE") = "US"
'    End Try
'    ' End GEO
'    Try

'        Dim lagCookie As HttpCookie = Request.Cookies("lagCookie")
'        If lagCookie Is Nothing Then
'            Dim szGuid As String = System.Guid.NewGuid().ToString()
'            lagCookie = New HttpCookie("lagCookie", szGuid)
'            lagCookie.Expires = DateTime.Now.AddYears(2)
'            lagCookie.Item("LagID") = Session("GEO_COUNTRY_CODE")
'            Response.Cookies.Add(lagCookie)
'            Session("lagCookie") = szGuid
'        Else
'            Session("lagCookie") = Server.HtmlEncode(lagCookie.Value)
'        End If
'        Session("LagID") = lagCookie.Item("LagID")
'    Catch ex As Exception
'        Session("LagID") = Session("GEO_COUNTRY_CODE")
'    End Try
'    ' Automatic Login if same user
'    If (Not (HttpContext.Current.User Is Nothing)) Then
'        If (HttpContext.Current.User.Identity.IsAuthenticated) Then
'            'Dim cLogins As New clsCustomer
'            'Dim LoginName As String = HttpContext.Current.User.Identity.Name
'            'If cLogins.ValidateUser_Global(Me, LoginName) Then
'            '    Dim id As System.Web.Security.FormsIdentity = HttpContext.Current.User.Identity
'            '    Dim ticket As System.Web.Security.FormsAuthenticationTicket = id.Ticket
'            '    Session("UserRole") = ticket.UserData

'            'Else
'            '    FormsAuthentication.SignOut()
'            'End If
'        End If
'    End If


'End Sub

'Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
'    ' Code that runs when a session ends. 
'    ' Note: The Session_End event is raised only when the sessionstate mode
'    ' is set to InProc in the Web.config file. If session mode is set to StateServer 
'    ' or SQLServer, the event is not raised.
'    If Len(Session("CustomerID")) > 0 Then
'        Dim szBuffer As String
'        'Dim cCustomer As New clsCustomer
'        'szBuffer = "Logout (Session_End) Using SC=" & Session("StatCookie") & " CustomReferrer=" & Session("CustomReferrer")
'        'cCustomer.AddEUS_LOG_Record(szBuffer, Session("CustomerID"), Session("IP"), Session("Referrer"))
'    End If
'End Sub


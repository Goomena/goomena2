﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PhotosEdit.ascx.vb" Inherits="Dating.Referrals.Server.Site.Web.PhotosEdit" %>
<%@ Register Assembly="DevExpress.Web.v13.1, Version=13.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxUploadControl" TagPrefix="dx" %>

<%@ Register assembly="DevExpress.Web.v13.1, Version=13.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxCallbackPanel" tagprefix="dx" %>
<script type="text/javascript">
     // <![CDATA[
    function OnPubPhotoDisplayLevelChanged(s, e) {
        try {
            var imgid = s.uniqueID.split("_")[1];
            window["editPhotoWindow_" + imgid].DoHideWindow(0);
        }
        catch (e) { }
        LoadingPanel.Hide();
        cbpnlPhotos.PerformCallback("ctlid_" + s.uniqueID + "_lvl_" + s.GetValue());
        LoadingPanel.Show();
    }
    function OnPrivatePhotoDisplayLevelChanged(s, e) {
        try {
            var imgid = s.uniqueID.split("_")[1];
            window["editPhotoWindow_" + imgid].DoHideWindow(0);
        }
        catch (e) { }
        LoadingPanel.Hide();
        cbpnlPhotos.PerformCallback("ctlid_" + s.uniqueID + "_lvl_" + s.GetValue());
        LoadingPanel.Show();
    }

    function runPhotosCollapsible(){
        jQuery(function ($) {
	        $('.mp_info_container').each(function () {
	            var pe_box = this;

	            $('.collapsibleHeader', pe_box).click(function () {
	                $('.mp_info', pe_box).toggle('fast', function () {
	                    // Animation complete.
	                    var isVisible = ($('.mp_info', pe_box).css('display') == 'none');
	                    if (isVisible) {
	                        $('.togglebutton', pe_box).text("+");
	                        $('.collapsibleHeader', pe_box).attr("title", "<%= globalStrings.GetCustomString("msg_ClickToExpand", GetLag() )  %>");
	                    }
	                    else {
	                        $('.togglebutton', pe_box).text("-");
	                        $('.collapsibleHeader', pe_box).attr("title", "<%= globalStrings.GetCustomString("msg_ClickToCollapse", GetLag()) %>");
	                    }
	                });

	            });
	        });
    	});
    }
    //runPhotosCollapsible();
    // ]]> 
</script>
<div class="m_wrap" id="divNoPhoto" runat="server">
	<div class="clearboth padding"></div>
	<div class="items_none items_hard">
        <asp:Literal ID="msg_HasNoPhotosText" runat="server"/>
	</div>
</div>
<div class="mp_photo">
	<div class="mp_left grid_3 alpha">
		<%--<h1><asp:Literal ID="msg_ManagePhotos" runat="server" /></h1>--%>
		<%--<h2 style="padding-bottom:0px;"><small></small></h2>--%>
        <div class="mp_title">
            <asp:Literal ID="msg_UploadNewPhoto" runat="server"/>
		</div>
        <div class="mp_info" style="overflow:visible;">
            <dx:ASPxUploadControl ID="uplImage" runat="server" 
                ClientInstanceName="uploader"
                OnFileUploadComplete="uplImage_FileUploadComplete" 
                CssClass="uploadCtlText" FileUploadMode="OnPageLoad" EncodeHtml="False">
                <ClientSideEvents 
                    FileUploadComplete="function(s, e) {Uploader_OnFileUploadComplete(s, e); }"
                    FilesUploadComplete="function(s, e) { Uploader_OnFilesUploadComplete(s, e); }"
                    FileUploadStart="function(s, e) { Uploader_OnUploadStart(s, e); }"
                    TextChanged="function(s, e) { UpdateUploadButton(); }"></ClientSideEvents>
                <ValidationSettings MaxFileSize="4194304" AllowedFileExtensions=".jpg,.jpeg,.jpe,.gif,.bmp,.png">
                </ValidationSettings>
                <BrowseButton Text="Select Image">
                </BrowseButton>
                <TextBoxStyle >
                <Paddings Padding="0px" />
                </TextBoxStyle>
            </dx:ASPxUploadControl>
            <div style="margin:10px 0;">
                <dx:ASPxLabel ID="lblAllowebMimeType" runat="server" EncodeHtml="false"></dx:ASPxLabel>
            </div>
            <div style="margin:5px 0;">
                <dx:ASPxLabel ID="lblMaxFileSize" runat="server" EncodeHtml="false"></dx:ASPxLabel>
            </div>
            <dx:ASPxButton ID="btnUpload" runat="server" 
                AutoPostBack="False" Text="Upload Your Photo" ClientInstanceName="btnUpload"
                    ClientEnabled="False" CssClass="btn btn-primary" 
                EnableDefaultAppearance="false" EncodeHtml="False">
                <ClientSideEvents Click="function(s, e) { uploader.Upload(); }" />
            </dx:ASPxButton>

            <div style="margin:15px 0;">
                         <asp:Label ID="lblPhotoLevel" runat="server" Text="Επίπεδο φωτογραφίας"/><asp:Image
                             ID="imgPhotoLevelInfo" runat="server" ImageUrl="~/Images/icon_tip.png" />
 <dx:ASPxPopupControl SkinID="None" EncodeHtml="False" ID="popupWindows" runat="server"
        EnableViewState="False" EnableHotTrack="False" PopupHorizontalAlign="RightSides"
        PopupVerticalAlign="Above" PopupHorizontalOffset="1" PopupVerticalOffset="-4"
        EnableHierarchyRecreation="True" CloseAction="MouseOut" HeaderText="Επίπεδο φωτογραφίας" 
        PopupAction="MouseOver" RenderMode="Lightweight">
        <ClientSideEvents />
        <Windows>
            <dx:PopupWindow PopupElementID="lblPhotoLevel" CloseAction="MouseOut" PopupAction="MouseOver">
                <ContentCollection>
                    <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server" SupportsDisabledAttribute="True">
                         <asp:Label ID="lblAllowAccessPhotosLevel" runat="server" Text=""/>
                    </dx:PopupControlContentControl>
                </ContentCollection>
            </dx:PopupWindow>
            <dx:PopupWindow PopupElementID="imgPhotoLevelInfo" CloseAction="MouseOut" PopupAction="MouseOver">
                <ContentCollection>
                    <dx:PopupControlContentControl ID="PopupControlContentControl2" runat="server" SupportsDisabledAttribute="True">
                         <asp:Label ID="lblAllowAccessPhotosLevelIcon" runat="server" Text=""/>
                    </dx:PopupControlContentControl>
                </ContentCollection>
            </dx:PopupWindow>
        </Windows>
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl10" runat="server" SupportsDisabledAttribute="True">
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>

<dx:ASPxComboBox ID="cbPhotosLevel" runat="server" SelectedIndex="0">
    <Items>
        <dx:ListEditItem Selected="True" Text="Public" Value="0" />
        <dx:ListEditItem Text="Private level 1" Value="1" />
        <dx:ListEditItem Text="Private level 2" Value="2" />
        <dx:ListEditItem Text="Private level 3" Value="3" />
        <dx:ListEditItem Text="Private level 4" Value="4" />
        <dx:ListEditItem Text="Private level 5" Value="5" />
        <dx:ListEditItem Text="Private level 6" Value="6" />
        <dx:ListEditItem Text="Private level 7" Value="7" />
        <dx:ListEditItem Text="Private level 8" Value="8" />
        <dx:ListEditItem Text="Private level 9" Value="9" />
        <dx:ListEditItem Text="Private level 10" Value="10" />
    </Items>
                        </dx:ASPxComboBox>
                <dx:ASPxCheckBox ID="chkPrivePhoto" runat="server" EncodeHtml="False" CheckBoxStyle-HorizontalAlign="Left" Visible="False"></dx:ASPxCheckBox>
            </div>
            <p class="not" style="margin:10px 0;"><asp:Literal ID="msg_ApprovalNote" runat="server"/></p>
		</div>

		
        <div class="mp_title" style="margin-top:20px">
            <asp:Literal ID="msg_VerificationDocs" runat="server"/>
		</div>
        <div class="mp_info" style="overflow:visible;">
            <asp:HyperLink ID="lnkVerificationDocs" runat="server" NavigateUrl="~/Members/VerificationDocs.aspx">Upload verification documents</asp:HyperLink>
            <%--<p class="not" style="margin:10px 0;"><asp:Literal ID="msg_VerificationDocsNote" runat="server"/></p>--%>
		</div>

	</div>


        <dx:ASPxCallbackPanel ID="cbpnlPhotos" runat="server" ShowLoadingPanel="False" 
                ShowLoadingPanelImage="False" ClientInstanceName="cbpnlPhotos">
                                <ClientSideEvents EndCallback="function(s, e) {
	LoadingPanel.Hide();
}" CallbackError="function(s, e) {
	LoadingPanel.Hide();
}" />
<ClientSideEvents EndCallback="function(s, e) {
	LoadingPanel.Hide();
}" CallbackError="function(s, e) {
	LoadingPanel.Hide();
}"></ClientSideEvents>
            <PanelCollection>
<dx:PanelContent runat="server" SupportsDisabledAttribute="True">

    <div class="mp_right grid_6 omega" style="padding-top:0px;">
		<div class="mp_title">
            <asp:Label ID="lblProfilePhotoTitle" runat="server" Text="Profile Photo"/>
		</div>
        <div class="mp_main">
            <div class="left">
                <asp:Image ID="imgProfilePhoto" runat="server" alt="avatar" />
            </div>
            <div class="right">
                <p><asp:Literal ID="msg_ProfileDefaultPicNote" runat="server"/></p>
                <div class=""><dx:ASPxButton ID="btnDeletePhoto" runat="server" CssClass="btn" 
                        EnableDefaultAppearance="False" CommandName="DELETE" EncodeHtml="False"></dx:ASPxButton></div>
            </div>
            <div class="clear"></div>
        </div>

        <div class="mp_title"><asp:Literal ID="msg_PubPhotos" runat="server"/></div>
        <div class="mp_body">
    <asp:ListView ID="lvPubPhotos" runat="server">
        <ItemTemplate>
            <li>
                <div class="mp_holder">
                    <a class="js fancybox myphoto" href='<%# Eval("ImageUrl") %>' rel="public">
                        <img src="<%# Eval("ImageThumbUrl") %>" alt="" title="" class="<%# Eval("ImageCss") %>" />
                    </a>
                    <div class="clear">
                    </div>
                    <div class="mp_links" style="margin-bottom:5px;">
<dx:ASPxPopupControl SkinID="None" EncodeHtml="False" ID="editPhotoWindow" runat="server"
        EnableViewState="False" EnableHotTrack="False" PopupHorizontalAlign="RightSides"
        PopupVerticalAlign="Above" PopupHorizontalOffset="1" PopupVerticalOffset="-4"
        EnableHierarchyRecreation="True" CloseAction="MouseOut" HeaderText="Επίπεδο φωτογραφίας" 
        PopupAction="MouseOver" RenderMode="Lightweight">
        <ClientSideEvents />
        <ClientSideEvents />
        <Windows>
            <dx:PopupWindow PopupElementID="lnkEdit" CloseAction="OuterMouseClick" PopupAction="LeftMouseClick">
                <ContentCollection>
                    <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server" SupportsDisabledAttribute="True">
                        <asp:Label ID="lblInfoInsidePopup" runat="server" Text=""/>
                        <dx:ASPxComboBox ID="cbPhotosLevel" runat="server" 
                            PopupHorizontalAlign="LeftSides" SelectedIndex="0" Width="150px">
<ClientSideEvents SelectedIndexChanged="function(s, e) {  OnPubPhotoDisplayLevelChanged(s,e); }" EndCallback="function(s, e) {
	LoadingPanel.Hide();
}" />

<ClientSideEvents SelectedIndexChanged="function(s, e) {  OnPubPhotoDisplayLevelChanged(s,e); }" EndCallback="function(s, e) {
	LoadingPanel.Hide();
}" />

    <Items>
        <dx:ListEditItem Selected="True" Text="Public" Value="0" />
        <dx:ListEditItem Text="Private level 1" Value="1" />
        <dx:ListEditItem Text="Private level 2" Value="2" />
        <dx:ListEditItem Text="Private level 3" Value="3" />
        <dx:ListEditItem Text="Private level 4" Value="4" />
        <dx:ListEditItem Text="Private level 5" Value="5" />
        <dx:ListEditItem Text="Private level 6" Value="6" />
        <dx:ListEditItem Text="Private level 7" Value="7" />
        <dx:ListEditItem Text="Private level 8" Value="8" />
        <dx:ListEditItem Text="Private level 9" Value="9" />
        <dx:ListEditItem Text="Private level 10" Value="10" />
    </Items>
                        </dx:ASPxComboBox>
                    </dx:PopupControlContentControl>
                </ContentCollection>
            </dx:PopupWindow>
        </Windows>
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl10" runat="server" SupportsDisabledAttribute="True">
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
                        <asp:HyperLink ID="lnkEdit" 
                            NavigateUrl="javascript:void(0);" runat="server" 
                            Text='<%# Eval("EditButtonText") %>' style="display:block;" />
                        <asp:HiddenField ID="hdfDisplayLevel" runat="server" 
                            Value='<%# Eval("DisplayLevel") %>' Visible="False" />
                        <asp:LinkButton ID="btnDelete" runat="server" 
                            CommandArgument='<%# Eval("ImageId") %>' CommandName="DELETE" 
                            OnClientClick='<%# Eval("DeleteButtonClientClickText") %>' 
                            Text='<%# Eval("DeleteButtonText") %>' style="display:block;" />
                        <asp:LinkButton ID="btnDefault" runat="server" 
                            CommandArgument='<%# Eval("ImageId") %>' CommandName="DEFAULT" 
                            style="display:inline-block;padding-bottom:17px;" 
                            Text='<%# Eval("DefaultButtonText") %>' Visible='<%# Eval("DefaultVisible") %>'></asp:LinkButton>
                        <asp:Label ID="lblInfo" runat="server" CssClass="not"
                            Text='<%# Eval("NotApprovedPhotoInfoText") %>' 
                            Visible='<%# Eval("InfoVisible") %>'></asp:Label>
                        <asp:HiddenField ID="hdfImageID" runat="server" 
                            Value='<%# Eval("ImageId") %>' Visible="False" />
                        <div class="clear">
                        </div>
                    </div>
                </div>
            </li>
        </ItemTemplate>
        <LayoutTemplate>
            <ul>
                <li ID="itemPlaceholder" runat="server"></li>
            </ul>
        </LayoutTemplate>
    </asp:ListView>
      
            <div class="clear"></div>
        </div>
		<!-- end public / begin private -->
        <div class="mp_title"><asp:Literal ID="msg_PrivatePhotos" runat="server"/></div>
        
        <div class="mp_body">
    <asp:ListView ID="lvPrivePhotos" runat="server">
        <ItemTemplate>
            <li>
                <div class="mp_holder">
                    <a class="js fancybox myphoto" href='<%# Eval("ImageUrl") %>' rel="prive">
                        <img src="<%# Eval("ImageThumbUrl") %>" alt="" title="" class="<%# Eval("ImageCss") %>" />
                        <span id="notificationsCountWrapper" class="jewelCount" title="<%= lblPhotoLevel.Text %><%# Eval("DisplayLevel") %>"><span class="countValue"><%# Eval("DisplayLevel") %></span></span>
                    </a>
                    <div class="clear">
                    </div>
                    <div class="mp_links">
<dx:ASPxPopupControl SkinID="None" EncodeHtml="False" ID="editPhotoWindow" runat="server"
        EnableViewState="False" EnableHotTrack="False" PopupHorizontalAlign="RightSides"
        PopupVerticalAlign="Above" PopupHorizontalOffset="1" PopupVerticalOffset="-4"
        EnableHierarchyRecreation="True" CloseAction="MouseOut" HeaderText="Επίπεδο φωτογραφίας" 
        PopupAction="MouseOver" RenderMode="Lightweight">
        <ClientSideEvents />
        <ClientSideEvents />
        <Windows>
            <dx:PopupWindow PopupElementID="lnkEdit" CloseAction="OuterMouseClick" PopupAction="LeftMouseClick">
                <ContentCollection>
                    <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server" SupportsDisabledAttribute="True">
                        <asp:Label ID="lblInfoInsidePopup" runat="server" Text=""/>
                        <dx:ASPxComboBox ID="cbPhotosLevel" runat="server" 
                            PopupHorizontalAlign="LeftSides" SelectedIndex="1" Width="150px">
<ClientSideEvents SelectedIndexChanged="function(s, e) {  OnPrivatePhotoDisplayLevelChanged(s, e); }" EndCallback="function(s, e) {
	LoadingPanel.Hide();
}" />
<ClientSideEvents SelectedIndexChanged="function(s, e) {  OnPrivatePhotoDisplayLevelChanged(s, e); }" EndCallback="function(s, e) {
	LoadingPanel.Hide();
}" />
    <Items>
        <dx:ListEditItem Selected="True" Text="Public" Value="0" />
        <dx:ListEditItem Text="Private level 1" Value="1" />
        <dx:ListEditItem Text="Private level 2" Value="2" />
        <dx:ListEditItem Text="Private level 3" Value="3" />
        <dx:ListEditItem Text="Private level 4" Value="4" />
        <dx:ListEditItem Text="Private level 5" Value="5" />
        <dx:ListEditItem Text="Private level 6" Value="6" />
        <dx:ListEditItem Text="Private level 7" Value="7" />
        <dx:ListEditItem Text="Private level 8" Value="8" />
        <dx:ListEditItem Text="Private level 9" Value="9" />
        <dx:ListEditItem Text="Private level 10" Value="10" />
    </Items>
                        </dx:ASPxComboBox>
                    </dx:PopupControlContentControl>
                </ContentCollection>
            </dx:PopupWindow>
        </Windows>
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl10" runat="server" SupportsDisabledAttribute="True">
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
                        <asp:HyperLink ID="lnkEdit" 
                            NavigateUrl="javascript:void(0);" runat="server" 
                            Text='<%# Eval("EditButtonText") %>' style="display:block;" />
                        <asp:LinkButton ID="btnDelete" runat="server" 
                            CommandArgument='<%# Eval("ImageId") %>' CommandName="DELETE" 
                            OnClientClick='<%# Eval("DeleteButtonClientClickText") %>' 
                            Text='<%# Eval("DeleteButtonText") %>'></asp:LinkButton>
                        <asp:Label ID="lblInfo" runat="server" CssClass="not" 
                            Text='<%# Eval("NotApprovedPhotoInfoText") %>' 
                            Visible="False"/>
                        <asp:HiddenField ID="hdfDisplayLevel" runat="server" 
                            Value='<%# Eval("DisplayLevel") %>' Visible="False" />
                        <asp:HiddenField ID="hdfImageID" runat="server" 
                            Value='<%# Eval("ImageId") %>' Visible="False" />
                        <div class="clear">
                        </div>
                    </div>
                </div>
            </li>
        </ItemTemplate>
        <LayoutTemplate>
            <ul>
                <li ID="itemPlaceholder" runat="server"></li>
            </ul>
        </LayoutTemplate>
    </asp:ListView>
            <div class="clear"></div>
            <div class="alert alert-info">
                <asp:Literal ID="msg_PrivatePhotosInfo" runat="server"/>
            </div>
        </div>

    </div>
                </dx:PanelContent>
</PanelCollection>
            </dx:ASPxCallbackPanel>
            
	<div class="mp_info_container">
		<h2 runat="server" id="h2InfoTitle" style="font-style:normal;font-weight:normal;padding-bottom:5px; " class="collapsibleHeader" title="">
            <asp:HyperLink ID="lnkInfoExpColl" runat="server" NavigateUrl="javascript:void(0);" class="togglebutton">+</asp:HyperLink> <asp:Literal ID="msg_ManagePhotos" runat="server"/></h2>
        <div class="mp_info" runat="server" id="divmp_info" >
            <asp:Literal ID="msg_ManagePhotosInfo" runat="server" />
        </div>
	</div>
		
	<div class="clear"></div>
</div>
			

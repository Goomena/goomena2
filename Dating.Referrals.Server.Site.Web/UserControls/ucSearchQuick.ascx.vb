﻿Imports Dating.Server.Core.DLL
Imports DevExpress.Web.ASPxEditors

Public Class ucSearchQuick
    Inherits BaseUserControl


    Dim _pageData As clsPageData
    Public ReadOnly Property CurrentPageData As clsPageData
        Get
            If (_pageData Is Nothing) Then _pageData = New clsPageData("control.OfferControl", Context)
            Return _pageData
        End Get
    End Property


    Public Property ItemsPerPage As Integer
        Get
            If (Me.ViewState("ItemsPerPage") IsNot Nothing) Then Return Me.ViewState("ItemsPerPage")
            Return 10
        End Get
        Set(value As Integer)
            Me.ViewState("ItemsPerPage") = value
        End Set
    End Property



    Private Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        dvWinks.DataBind()
    End Sub

    'Protected Overrides Sub OnPreRender(e As System.EventArgs)
    '    dvWinks.RowPerPage = Me.ItemsPerPage
    '    MyBase.OnPreRender(e)
    'End Sub



    Protected Sub sdsWinks_Selecting(sender As Object, e As System.Web.UI.WebControls.SqlDataSourceSelectingEventArgs) Handles sdsWinks.Selecting
        For Each prm As SqlClient.SqlParameter In e.Command.Parameters

            If (prm.ParameterName = "@CurrentProfileId") Then
                Dim MasterProfileId = Me.MasterProfileId
                prm.Value = MasterProfileId
            ElseIf (prm.ParameterName = "@ReturnRecordsWithStatus") Then
                prm.Value = ProfileStatusEnum.Approved
            ElseIf (prm.ParameterName = "@NumberOfRecordsToReturn") Then
                prm.Value = Me.ItemsPerPage
            End If

        Next
    End Sub



    Protected Sub dvWinks_DataBound(sender As Object, e As EventArgs) Handles dvWinks.DataBound
        Try
            For Each dvi As DevExpress.Web.ASPxDataView.DataViewItem In dvWinks.Items
                Try

                    Dim dr As DataRowView = dvi.DataItem

                    Dim lblOtherMemberHeading As Literal = dvWinks.FindItemControl("lblOtherMemberHeading", dvi)
                    lblOtherMemberHeading.Text = IIf(dr("AboutMe_Heading") IsNot Nothing, dr("AboutMe_Heading"), String.Empty)

                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "")
                End Try
            Next

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub



    Protected Function WriteSearch_MemberInfo(DataItem As DataRowView) As Object
        '<ul>
        '    <li><b><%# Dating.Server.Core.DLL.ProfileHelper.GetCurrentAge(Eval("Birthday"))%></b> <%# MyBase.CurrentPageData.GetCustomString("YearsOldFromText") %> 
        '<b><%# Eval("City")%>, <%# Eval("Region")%></b></li>
        '    <li><b><%# Dating.Server.Core.DLL.ProfileHelper.GetHeightString(Eval("PersonalInfo_HeightID"), MyBase.Session("LagID"))%></b> - 
        '        <b><%# Dating.Server.Core.DLL.ProfileHelper.GetBodyTypeString(Eval("PersonalInfo_BodyTypeID"), MyBase.Session("LagID"))%></b></li>
        '    <li><%# Dating.Server.Core.DLL.ProfileHelper.GetHairColorString(Eval("PersonalInfo_HairColorID"), Me.Session("LagID"))%>, 
        '        <%# Dating.Server.Core.DLL.ProfileHelper.GetEyeColorString(Eval("PersonalInfo_EyeColorID"), Me.Session("LagID"))%>, 
        '        <%# Dating.Server.Core.DLL.ProfileHelper.GetEthnicityString(Eval("PersonalInfo_EthnicityID"), Me.Session("LagID"))%></li>
        '</ul>

        Dim sb As New StringBuilder()
        Try
            sb.Append("<ul>")

            sb.Append("<li>")

            If Not GeneralFunctions.IsDBNullOrNothingOrEmpty(DataItem("Birthday")) Then
                sb.Append("<b>" & ProfileHelper.GetCurrentAge(DataItem("Birthday")) & "</b>")
                sb.Append(" " & CurrentPageData.GetCustomString("YearsOldFromText") & " ")
            End If

            sb.Append("<b>")
            If (Not GeneralFunctions.IsDBNullOrNothingOrEmpty(DataItem("City"))) Then
                sb.Append(DataItem("City"))
            End If

            If (Not GeneralFunctions.IsDBNullOrNothingOrEmpty(DataItem("City")) AndAlso Not GeneralFunctions.IsDBNullOrNothingOrEmpty(DataItem("Region"))) Then
                sb.Append(",  ")
            End If

            If Not GeneralFunctions.IsDBNullOrNothingOrEmpty(DataItem("Region")) Then
                sb.Append(DataItem("Region"))
            End If
            sb.Append("</b>")
            sb.Append("</li>")


            sb.Append("<li>")
            sb.Append("<b>")

            If Not GeneralFunctions.IsDBNullOrNothingOrEmpty(DataItem("PersonalInfo_HeightID").ToString()) Then
                sb.Append(ProfileHelper.GetHeightString(Eval("PersonalInfo_HeightID"), MyBase.Session("LagID")))
            End If

            If Not GeneralFunctions.IsDBNullOrNothingOrEmpty(DataItem("PersonalInfo_HeightID")) AndAlso Not GeneralFunctions.IsDBNullOrNothingOrEmpty(DataItem("PersonalInfo_BodyTypeID")) Then
                sb.Append(" - ")
            End If

            If (Not GeneralFunctions.IsDBNullOrNothingOrEmpty(DataItem("PersonalInfo_BodyTypeID"))) Then
                sb.Append(ProfileHelper.GetBodyTypeString(Eval("PersonalInfo_BodyTypeID"), MyBase.Session("LagID")))
            End If

            sb.Append("</b>")
            sb.Append("</li>")


            sb.Append("<li>")

            If Not GeneralFunctions.IsDBNullOrNothingOrEmpty(DataItem("PersonalInfo_HairColorID")) Then
                sb.Append(ProfileHelper.GetHairColorString(Eval("PersonalInfo_HairColorID"), Me.Session("LagID")))
            End If

            If Not GeneralFunctions.IsDBNullOrNothingOrEmpty(DataItem("PersonalInfo_HairColorID")) AndAlso Not GeneralFunctions.IsDBNullOrNothingOrEmpty(DataItem("PersonalInfo_EyeColorID")) Then
                sb.Append(", ")
            End If

            If Not GeneralFunctions.IsDBNullOrNothingOrEmpty(DataItem("PersonalInfo_EyeColorID")) Then
                sb.Append(ProfileHelper.GetEyeColorString(Eval("PersonalInfo_EyeColorID"), Me.Session("LagID")))
            End If

            If Not GeneralFunctions.IsDBNullOrNothingOrEmpty(DataItem("PersonalInfo_EyeColorID")) AndAlso Not GeneralFunctions.IsDBNullOrNothingOrEmpty(DataItem("PersonalInfo_EthnicityID")) Then
                sb.Append(", ")
            End If

            If Not GeneralFunctions.IsDBNullOrNothingOrEmpty(DataItem("PersonalInfo_EthnicityID")) Then
                sb.Append(ProfileHelper.GetEthnicityString(Eval("PersonalInfo_EthnicityID"), Me.Session("LagID")))
            End If

            sb.Append("</li>")
            sb.Append("</ul>")

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Return sb.ToString()
    End Function


End Class
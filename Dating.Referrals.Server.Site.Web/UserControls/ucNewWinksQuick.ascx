﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucNewWinksQuick.ascx.vb" Inherits="Dating.Referrals.Server.Site.Web.ucNewWinksQuick" %>
<%@ Register assembly="DevExpress.Web.v13.1, Version=13.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxDataView" tagprefix="dx" %>

<dx:ASPxDataView ID="dvWinks" runat="server" ColumnCount="1" EnableDefaultAppearance="false" EnableTheming="False" 
    ItemSpacing="0px" RowPerPage="10" Width="100%" 
    EmptyDataText="Can't find any new wink.">
    <ItemTemplate>
        <div class="quick_list m_item newest">
        <a class="linkNotifWrapper" href="javascript:void(0);" location="<%# System.Web.VirtualPathUtility.ToAbsolute("~/Members/Likes.aspx#") &  Eval("LoginName") %>">
        <table cellpadding="0" cellspacing="0">
            <tr>
                <td><div class="pic">
                        <dx:ASPxHyperLink ID="lnkFromImage" runat="server" 
                            EnableDefaultAppearance="False" EnableTheming="False" EncodeHtml="false" 
                            ImageUrl='<%# Dating.Referrals.Server.Site.Web.AppUtils.GetImage(Eval("CustomerID").Tostring(), Eval("FileName").Tostring(), Eval("GenderId").Tostring(), False,Me.IsHTTPS) %>' 
                            NavigateUrl='<%# System.Web.VirtualPathUtility.ToAbsolute("~/Members/Likes.aspx#") &  Eval("LoginName") %>'>
                        </dx:ASPxHyperLink>
                    </div></td>
                <td><div>
                        <dx:ASPxHyperLink ID="lnkFromLogin" runat="server" 
                            EnableDefaultAppearance="False" EnableTheming="False" EncodeHtml="false" 
                            NavigateUrl='<%# System.Web.VirtualPathUtility.ToAbsolute("~/Members/Likes.aspx#") &  Eval("LoginName") %>' 
                            Text='<%# Eval("LoginName") %>' style="margin-top:0px;padding-top:0px;" CssClass="loginName">
                        </dx:ASPxHyperLink>
                        <asp:Literal ID="lblYouReceivedWinkOfferText" runat="server"/>
                        <p id="pWink"><asp:Literal ID="lblWantToKnowFirstDatePriceText" runat="server" Visible="false" /></p>
                    </div></td>
            </tr>
        </table>
        </a>
        </div>
        <div class="clear">
        </div>
    </ItemTemplate>
    <Paddings Padding="0px" />
    <ItemStyle>
    <Paddings Padding="0px" />
    </ItemStyle>
</dx:ASPxDataView>
<%--
<asp:SqlDataSource ID="sdsWinks" runat="server" 
    ConnectionString="<%$ ConnectionStrings:AppDBconnectionString %>" 
    SelectCommand="GetNewWinksQuick" SelectCommandType="StoredProcedure">
    <SelectParameters>
        <asp:Parameter DefaultValue="0" Name="CurrentProfileId" Type="Int32" />
        <asp:Parameter DefaultValue="4" Name="ReturnRecordsWithStatus" Type="Int32" />
        <asp:Parameter DefaultValue="5" Name="NumberOfRecordsToReturn" Type="Int32" />
    </SelectParameters>
</asp:SqlDataSource>

<script type="text/javascript">
    jQuery('.m_item.newest').positionOn($('m_item.newest.linkNotifWrapper'))
</script>

--%>
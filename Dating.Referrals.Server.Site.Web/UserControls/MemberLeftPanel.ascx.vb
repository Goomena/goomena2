﻿Imports Dating.Server.Core.DLL
Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Datasets.DLL.DSMembers

Public Class MemberLeftPanel
    Inherits BaseUserControl

    Dim _pageData As clsPageData
    Protected ReadOnly Property CurrentPageData As clsPageData
        Get
            If (_pageData Is Nothing) Then _pageData = New clsPageData("~/Members/control.ProfileView", Context)
            Return _pageData
        End Get
    End Property


    Public Property CssClass As String
        Get
            Return pnlProfile.CssClass
        End Get
        Set(value As String)
            pnlProfile.CssClass = value
            pnlProfile2.CssClass = value
        End Set
    End Property

    Public Property ShowMembersNewest As Boolean
        Get
            Return divMembersNewestAll.Visible
        End Get
        Set(value As Boolean)
            divMembersNewestAll.Visible = value
        End Set
    End Property


    Public Property ShowMembersNear As Boolean
        Get
            Return divMembersNearAll.Visible
        End Get
        Set(value As Boolean)
            divMembersNearAll.Visible = value
        End Set
    End Property


    Public Property ShowQuickLinks As Boolean
        Get
            Return pnlQuickLinks.Visible
        End Get
        Set(value As Boolean)
            pnlQuickLinks.Visible = value
        End Set
    End Property


    Public Property ShowLiveZilla As Boolean
        Get
            Return pnlLiveZilla.Visible
        End Get
        Set(value As Boolean)
            pnlLiveZilla.Visible = value
        End Set
    End Property


    Public Property ShowBottomPanel As Boolean
        Get
            Return pnlBottom.Visible
        End Get
        Set(value As Boolean)
            pnlBottom.Visible = value
        End Set
    End Property


    Public Property ShowDefaultView As Boolean
        Get
            Return pnlProfile.Visible
        End Get
        Set(value As Boolean)
            pnlProfile.Visible = value
            pnlPhoto.Visible = value
            pnlProfile2.Visible = value
        End Set
    End Property


    'Public Property CssClass As String
    '    Get
    '        If (ViewState("CssClass") IsNot Nothing) Then
    '            Return ViewState("CssClass")
    '        End If
    '        Return ""
    '    End Get
    '    Set(value As String)

    '        If (ViewState("CssClass") <> value) Then

    '            Dim oldStyle As String = ViewState("CssClass")

    '            If (Not String.IsNullOrEmpty(oldStyle)) Then
    '                pnlProfile.CssClass.Replace(oldStyle, "")
    '            End If

    '            If (Not String.IsNullOrEmpty(value)) Then
    '                pnlProfile.CssClass = pnlProfile.CssClass & " " & value
    '            End If

    '            ViewState("CssClass") = value
    '        End If

    '    End Set
    'End Property



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            If (Not Me.IsPostBack) Then
                LoadLAG()
                LoadProfile(Me.MasterProfileId)


                'Dim prms As New clsSearchHelperParameters()
                'prms.CurrentProfileId = Me.MasterProfileId
                'prms.ReturnRecordsWithStatus = ProfileStatusEnum.Approved
                'prms.SearchSort = SearchSortEnum.NearestDistance
                'prms.zipstr = Me.GetCurrentProfile().Zip
                'prms.Distance = 1000
                'prms.LookingFor_ToMeetMaleID = Me.GetCurrentProfile().LookingFor_ToMeetMaleID
                'prms.LookingFor_ToMeetFemaleID = Me.GetCurrentProfile().LookingFor_ToMeetFemaleID
                'prms.NumberOfRecordsToReturn = 5
                'prms.AdditionalWhereClause = ""
                'dvNearest.DataSource = clsSearchHelper.GetMembersToSearchDataTable(prms)

                ''dvNearest.DataSource = clsSearchHelper.GetMembersToSearchDataTable(Me.MasterProfileId, ProfileStatusEnum.Approved, SearchSortEnum.NearestDistance, Me.GetCurrentProfile().Zip, 1000, 5)
                'dvNearest.DataBind()
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

        If (Not Page.IsPostBack) Then
        End If

    End Sub

    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        Try

            If (Not Me.IsPostBack) Then LoadProfile(Me.MasterProfileId)

            Dim btnUrl As String
            Dim currentUrl As String = Request.Url.PathAndQuery.ToUpper()

            btnUrl = lnkDashboard.NavigateUrl.TrimStart("~"c).ToUpper()
            If (btnUrl.StartsWith(currentUrl)) Then
                lnkDashboard.CssClass = lnkDashboard.CssClass & " selectedLink"
            Else
                lnkDashboard.CssClass = lnkDashboard.CssClass.Replace(" selectedLink", "")
            End If

            btnUrl = lnkProfile.NavigateUrl.TrimStart("~"c).ToUpper()
            If (btnUrl.StartsWith(currentUrl)) Then
                lnkProfile.CssClass = lnkProfile.CssClass & " selectedLink"
            Else
                lnkProfile.CssClass = lnkProfile.CssClass.Replace(" selectedLink", "")
            End If

            btnUrl = lnkEditPhotos.NavigateUrl.TrimStart("~"c).ToUpper()
            If (btnUrl.StartsWith(currentUrl)) Then
                lnkEditPhotos.CssClass = lnkEditPhotos.CssClass & " selectedLink"
            Else
                lnkEditPhotos.CssClass = lnkEditPhotos.CssClass.Replace(" selectedLink", "")
            End If

            btnUrl = lnkProfile.NavigateUrl.TrimStart("~"c).ToUpper()
            If (btnUrl.StartsWith(currentUrl)) Then
                lnkProfile.CssClass = lnkProfile.CssClass & " selectedLink"
            Else
                lnkProfile.CssClass = lnkProfile.CssClass.Replace(" selectedLink", "")
            End If

            btnUrl = lnkLikes.NavigateUrl.TrimStart("~"c).ToUpper()
            If (btnUrl.StartsWith(currentUrl)) Then
                lnkLikes.CssClass = lnkLikes.CssClass & " selectedLink"
            Else
                lnkLikes.CssClass = lnkLikes.CssClass.Replace(" selectedLink", "")
            End If

            btnUrl = lnkOffers.NavigateUrl.TrimStart("~"c).ToUpper()
            If (btnUrl.StartsWith(currentUrl)) Then
                lnkOffers.CssClass = lnkOffers.CssClass & " selectedLink"
            Else
                lnkOffers.CssClass = lnkOffers.CssClass.Replace(" selectedLink", "")
            End If


            btnUrl = lnkMessages.NavigateUrl.TrimStart("~"c).ToUpper()
            If (btnUrl.StartsWith(currentUrl)) Then
                lnkMessages.CssClass = lnkMessages.CssClass & " selectedLink"
            Else
                lnkMessages.CssClass = lnkMessages.CssClass.Replace(" selectedLink", "")
            End If


            btnUrl = lnkDates.NavigateUrl.TrimStart("~"c).ToUpper()
            If (btnUrl.StartsWith(currentUrl)) Then
                lnkDates.CssClass = lnkDates.CssClass & " selectedLink"
            Else
                lnkDates.CssClass = lnkDates.CssClass.Replace(" selectedLink", "")
            End If

            btnUrl = lnkBilling.NavigateUrl.TrimStart("~"c).ToUpper()
            If (btnUrl.StartsWith(currentUrl)) Then
                lnkBilling.CssClass = lnkBilling.CssClass & " selectedLink"
            Else
                lnkBilling.CssClass = lnkBilling.CssClass.Replace(" selectedLink", "")
            End If

            btnUrl = lnkWhoViewedMe.NavigateUrl.TrimStart("~"c).ToUpper()
            If (btnUrl.StartsWith(currentUrl)) Then
                lnkWhoViewedMe.CssClass = lnkWhoViewedMe.CssClass & " selectedLink"
            Else
                lnkWhoViewedMe.CssClass = lnkWhoViewedMe.CssClass.Replace(" selectedLink", "")
            End If

            btnUrl = lnkWhoFavoritedMe.NavigateUrl.TrimStart("~"c).ToUpper()
            If (btnUrl.StartsWith(currentUrl)) Then
                lnkWhoFavoritedMe.CssClass = lnkWhoFavoritedMe.CssClass & " selectedLink"
            Else
                lnkWhoFavoritedMe.CssClass = lnkWhoFavoritedMe.CssClass.Replace(" selectedLink", "")
            End If

            btnUrl = lnkWhoSharedPhotos.NavigateUrl.TrimStart("~"c).ToUpper()
            If (btnUrl.StartsWith(currentUrl)) Then
                lnkWhoSharedPhotos.CssClass = lnkWhoSharedPhotos.CssClass & " selectedLink"
            Else
                lnkWhoSharedPhotos.CssClass = lnkWhoSharedPhotos.CssClass.Replace(" selectedLink", "")
            End If

            btnUrl = lnkMyFavoriteList.NavigateUrl.TrimStart("~"c).ToUpper()
            If (btnUrl.StartsWith(currentUrl)) Then
                lnkMyFavoriteList.CssClass = lnkMyFavoriteList.CssClass & " selectedLink"
            Else
                lnkMyFavoriteList.CssClass = lnkMyFavoriteList.CssClass.Replace(" selectedLink", "")
            End If

            btnUrl = lnkMyBlockedList.NavigateUrl.TrimStart("~"c).ToUpper()
            If (btnUrl.StartsWith(currentUrl)) Then
                lnkMyBlockedList.CssClass = lnkMyBlockedList.CssClass & " selectedLink"
            Else
                lnkMyBlockedList.CssClass = lnkMyBlockedList.CssClass.Replace(" selectedLink", "")
            End If

            btnUrl = lnkMyViewedList.NavigateUrl.TrimStart("~"c).ToUpper()
            If (btnUrl.StartsWith(currentUrl)) Then
                lnkMyViewedList.CssClass = lnkMyViewedList.CssClass & " selectedLink"
            Else
                lnkMyViewedList.CssClass = lnkMyViewedList.CssClass.Replace(" selectedLink", "")
            End If

            btnUrl = lnkNotifications.NavigateUrl.TrimStart("~"c).ToUpper()
            If (btnUrl.StartsWith(currentUrl)) Then
                lnkNotifications.CssClass = lnkNotifications.CssClass & " selectedLink"
            Else
                lnkNotifications.CssClass = lnkNotifications.CssClass.Replace(" selectedLink", "")
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try
    End Sub


    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
        LoadProfile(Me.MasterProfileId)
    End Sub


    Protected Sub LoadLAG()
        Try
            Dim cPageBasic As Dating.Server.Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            lnkProfile.Text = globalStrings.GetCustomString("MyProfile")
            lnkEditPhotos.Text = globalStrings.GetCustomString("Photos")
            lnkBilling.Text = globalStrings.GetCustomString("StandardMember")
            lnkTestemonials.Text = globalStrings.GetCustomString("Testemonials")
            lnkProfile.Text = globalStrings.GetCustomString("MyProfile")

            lnkMembersNewestAll.Text = CurrentPageData.GetCustomString(lnkMembersNewestAll.ID)
            lnkMembersNearAll.Text = CurrentPageData.GetCustomString(lnkMembersNearAll.ID)
            'lnkMembersNewest.Text = CurrentPageData.GetCustomString(lnkMembersNewest.ID)
            'lnkMembersNear.Text = CurrentPageData.GetCustomString(lnkMembersNear.ID)

            lnkWhoViewedMe.Text = CurrentPageData.GetCustomString("lnkWhoViewedMe")
            lnkWhoFavoritedMe.Text = CurrentPageData.GetCustomString("lnkWhoFavoritedMe")
            lnkWhoSharedPhotos.Text = CurrentPageData.GetCustomString("lnkWhoSharedPhotos")
            lnkMyFavoriteList.Text = CurrentPageData.GetCustomString("lnkMyFavoriteList")
            lnkMyBlockedList.Text = CurrentPageData.GetCustomString("lnkMyBlockedList")
            lnkMyViewedList.Text = CurrentPageData.GetCustomString("lnkMyViewedList")

            lnkDashboard.Text = CurrentPageData.GetCustomString("lnkDashboard")
            lnkLikes.Text = CurrentPageData.GetCustomString("lnkLikes")
            lnkOffers.Text = CurrentPageData.GetCustomString("lnkOffers")
            lnkMessages.Text = CurrentPageData.GetCustomString("lnkMessages")
            lnkDates.Text = CurrentPageData.GetCustomString("lnkDates")
            lnkNotifications.Text = CurrentPageData.GetCustomString("lnkNotifications")

            lblHeaderQuickLinks.Text = CurrentPageData.GetCustomString(lblHeaderQuickLinks.ID)



            lnkHowWorksMale.Text = CurrentPageData.GetCustomString("lnkHowWorks")
            lnkHowWorksFemale.Text = CurrentPageData.GetCustomString("lnkHowWorks")
            lnkReferrer.Text = CurrentPageData.GetCustomString("lnkAffiliate")
            lnkAffiliate.Text = CurrentPageData.GetCustomString("lnkAffiliate2")

            If (Me.IsFemale) Then
                liHowWorksMale.Visible = False
                liHowWorksFemale.Visible = True
                liReferrer.Visible = True
                liAffiliate.Visible = False
            ElseIf (Me.IsMale AndAlso clsCurrentContext.IsAffiliate()) Then
                liHowWorksMale.Visible = False
                liHowWorksFemale.Visible = True
                liAffiliate.Visible = True
                liReferrer.Visible = False
            Else
                liHowWorksMale.Visible = True
                liHowWorksFemale.Visible = False

                liAffiliate.Visible = False
                liReferrer.Visible = False
            End If

            lnkViewDescription.Text = Me.CurrentPageData.GetCustomString("QuickLinksWhatIsIt")
            lnkViewDescription.OnClientClick = WhatIsIt.OnMoreInfoClickFunc(
                lnkViewDescription.OnClientClick,
                ResolveUrl("~/Members/InfoWin.aspx?info=quicklinks"),
                Me.CurrentPageData.GetCustomString("lblHeaderQuickLinks"))


        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Sub LoadProfile(ByVal profileId As Integer)
        Dim textPattern As String = <html><![CDATA[
#TEXT# <span id="notificationsCountWrapper" class="jewelCount"><span class="countValue">#COUNT#</span></span>
]]></html>.Value


        Try

            'Dim _profilerows As New clsProfileRows(profileId)
            'Dim masterRow As EUS_ProfilesRow = _profilerows.GetMasterRow()

            ltrLogin.Text = Me.SessionVariables.MemberData.LoginName

            If Me.IsMale Then

                'If (ProfileHelper.IsGenerous(Me.GetCurrentProfile().AccountTypeId)) Then
                ltrInfo.Text = globalStrings.GetCustomString("msg_GenerousMale")
                'ElseIf (ProfileHelper.IsAttractive(Me.GetCurrentProfile().AccountTypeId)) Then
                '    ltrInfo.Text = globalStrings.GetCustomString("msg_AttractiveMale")
                'End If

            ElseIf Me.IsFemale Then

                'If (ProfileHelper.IsGenerous(Me.GetCurrentProfile().AccountTypeId)) Then
                '    ltrInfo.Text = globalStrings.GetCustomString("msg_GenerousFemale")
                'ElseIf (ProfileHelper.IsAttractive(Me.GetCurrentProfile().AccountTypeId)) Then
                ltrInfo.Text = globalStrings.GetCustomString("msg_AttractiveFemale")
                'End If

            End If



            'Dim totalMemberCredits As Integer?
            'Dim memberCreditsQuery = From itm In Me.CMSDBDataContext.EUS_CustomerCredits
            '                       Where itm.CustomerId = Me.MasterProfileId
            '                       Select itm.Credits

            'If (memberCreditsQuery IsNot Nothing) Then
            '    totalMemberCredits = memberCreditsQuery.Sum()
            'End If




            If (Me.IsMale) Then
                liBilling.Visible = True

                Try


                    Dim sql As String = "EXEC [CustomerAvailableCredits] @CustomerId = " & Me.MasterProfileId & ""
                    Dim dt As DataTable = DataHelpers.GetDataTable(sql)

                    If (dt.Rows(0)("CreditsRecordsCount") > 0) Then
                        lnkBilling.NavigateUrl = "~/Members/SelectProduct.aspx"
                        lnkBilling.Text = globalStrings.GetCustomString("Credits")
                        lnkBilling.Text = textPattern.Replace("#TEXT#", lnkBilling.Text).Replace("#COUNT#", dt.Rows(0)("AvailableCredits"))
                    Else
                        lnkBilling.NavigateUrl = "~/Members/SelectProduct.aspx"
                        lnkBilling.Text = globalStrings.GetCustomString("StandardMember")
                    End If

                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "")
                End Try

            Else
                liBilling.Visible = False
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try


        Try
            ' view icon of current user, that has logged in 

            Dim fileName As String = Nothing
            Dim photo As DSMembers.EUS_CustomerPhotosRow = DataHelpers.GetProfilesDefaultPhoto(profileId)
            If (photo IsNot Nothing) Then
                imgPhoto.ImageUrl = ProfileHelper.GetProfileImageURL(photo.CustomerID, photo.FileName, Me.SessionVariables.MemberData.GenderId, True, Me.IsHTTPS)
            Else
                imgPhoto.ImageUrl = ProfileHelper.GetDefaultImageURL(Me.SessionVariables.MemberData.GenderId)
            End If
            If (profileId = 269) Then
                If (Request.Url.Scheme = "https") Then
                    imgPhoto.ImageUrl = imgPhoto.ImageUrl.Replace("http://", "https://")
                End If
            End If
            lnkPhoto.NavigateUrl = ResolveUrl("~/Members/Photos.aspx")

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try


        Try
            ' set numbers to Who viewed me, who favorited me

            '            Dim sql As String = <sql><![CDATA[

            'select 
            '	CountWhoViewedMe = (select COUNT(*) 
            '							from EUS_ProfilesViewed v
            '							where ToProfileID=@ProfileID
            '                            and ISNULL(IsToProfileIDViewed, 0) = 0
            '							and exists(
            '								select profileid 
            '								from EUS_Profiles p
            '								where p.ProfileID = v.FromProfileID 
            '								and p.IsMaster=1
            '								and p.Status=4
            '								and isnull(p.PrivacySettings_NotShowInOtherUsersViewedList,0)=0
            '							)
            '							AND  not exists(select * 
            '								from EUS_ProfilesBlocked bl 
            '								where (bl.FromProfileID =v.FromProfileID and bl.ToProfileID = @ProfileID) or
            '									(bl.FromProfileID =@ProfileID and bl.ToProfileID = v.FromProfileID)
            '							)	
            '    ),
            '	CountWhoFavoritedMe = (select COUNT(*) 
            '							from EUS_ProfilesFavorite v
            '							where ToProfileID=@ProfileID
            '                            and ISNULL(IsToProfileIDViewed, 0) = 0
            '							and exists(
            '								select profileid 
            '								from EUS_Profiles p
            '								where p.ProfileID = v.FromProfileID 
            '								and p.IsMaster=1
            '								and p.Status=4
            '							)
            '							AND  not exists(select * 
            '								from EUS_ProfilesBlocked bl 
            '								where (bl.FromProfileID =v.FromProfileID and bl.ToProfileID = @ProfileID) or
            '									(bl.FromProfileID =@ProfileID and bl.ToProfileID = v.FromProfileID)
            '							)	
            '	),
            '	CountWhoSharedPhotos = (select COUNT(*) 
            '							from EUS_ProfilePhotosLevel v
            '							where ToProfileID=@ProfileID
            '                            and ISNULL(IsToProfileIDViewed, 0) = 0
            '							and exists(
            '								select profileid 
            '								from EUS_Profiles p
            '								where p.ProfileID = v.FromProfileID 
            '								and p.IsMaster=1
            '								and p.Status=4
            '							)
            '							and exists(
            '								select CustomerPhotosID 
            '								from EUS_CustomerPhotos p
            '								where p.CustomerID = v.FromProfileID 
            '								and p.DisplayLevel>0
            '								and p.HasAproved=1
            '								and ISNULL(p.IsDeleted,0)=0
            '							)
            '							AND  not exists(select * 
            '								from EUS_ProfilesBlocked bl 
            '								where (bl.FromProfileID =v.FromProfileID and bl.ToProfileID = @ProfileID) or
            '									(bl.FromProfileID =@ProfileID and bl.ToProfileID = v.FromProfileID)
            '							)	
            '	)
            ']]></sql>.Value

            Dim sql As String = "EXEC [GetMemberActionsCounters] @ProfileID=" & Me.MasterProfileId
            Dim dt As DataTable = DataHelpers.GetDataTable(sql)


            If (dt.Rows(0)("CountWhoViewedMe") > 0) Then
                lnkWhoViewedMe.Text = CurrentPageData.GetCustomString("lnkWhoViewedMe")
                lnkWhoViewedMe.Text = textPattern.Replace("#TEXT#", lnkWhoViewedMe.Text).Replace("#COUNT#", dt.Rows(0)("CountWhoViewedMe"))
            Else
                lnkWhoViewedMe.Text = CurrentPageData.GetCustomString("lnkWhoViewedMe")
            End If

            If (dt.Rows(0)("CountWhoFavoritedMe") > 0) Then
                lnkWhoFavoritedMe.Text = CurrentPageData.GetCustomString("lnkWhoFavoritedMe")
                lnkWhoFavoritedMe.Text = textPattern.Replace("#TEXT#", lnkWhoFavoritedMe.Text).Replace("#COUNT#", dt.Rows(0)("CountWhoFavoritedMe"))
            Else
                lnkWhoFavoritedMe.Text = CurrentPageData.GetCustomString("lnkWhoFavoritedMe")
            End If

            If (dt.Rows(0)("CountWhoSharedPhotos") > 0) Then
                lnkWhoSharedPhotos.Text = CurrentPageData.GetCustomString("lnkWhoSharedPhotos")
                lnkWhoSharedPhotos.Text = textPattern.Replace("#TEXT#", lnkWhoSharedPhotos.Text).Replace("#COUNT#", dt.Rows(0)("CountWhoSharedPhotos"))
            Else
                lnkWhoSharedPhotos.Text = CurrentPageData.GetCustomString("lnkWhoSharedPhotos")
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try


    End Sub


    Public Sub RefreshThumb()
        LoadProfile(Me.MasterProfileId)
    End Sub


    Protected Sub sdsNewest_Selecting(sender As Object, e As System.Web.UI.WebControls.SqlDataSourceSelectingEventArgs) Handles sdsNewest.Selecting
        For Each prm As SqlClient.SqlParameter In e.Command.Parameters

            If (prm.ParameterName = "@CurrentProfileId") Then
                prm.Value = Me.MasterProfileId
            ElseIf (prm.ParameterName = "@ReturnRecordsWithStatus") Then
                prm.Value = ProfileStatusEnum.Approved
            ElseIf (prm.ParameterName = "@NumberOfRecordsToReturn") Then
                prm.Value = 5
            End If

        Next
    End Sub



    'Protected Sub sdsNearest_Selecting(sender As Object, e As System.Web.UI.WebControls.SqlDataSourceSelectingEventArgs) Handles sdsNearest.Selecting
    '    For Each prm As SqlClient.SqlParameter In e.Command.Parameters

    '        If (prm.ParameterName = "@CurrentProfileId") Then
    '            prm.Value = Me.MasterProfileId
    '        ElseIf (prm.ParameterName = "@ReturnRecordsWithStatus") Then
    '            prm.Value = ProfileStatusEnum.Approved
    '        ElseIf (prm.ParameterName = "@NumberOfRecordsToReturn") Then
    '            prm.Value = 5
    '        End If

    '    Next
    'End Sub

End Class
﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="DateControl.ascx.vb" Inherits="Dating.Referrals.Server.Site.Web.DateControl" %>
<table cellpadding="0" cellspacing="0">
<tr>
<td><dx:ASPxComboBox ID="ddlday" runat="server" EncodeHtml="False" Width="100px">
<Items>
    <dx:ListEditItem Text="Day" Value="" />
    <dx:ListEditItem Text="1" Value="1" />
    <dx:ListEditItem Text="2" Value="2" />
    <dx:ListEditItem Text="3" Value="3" />
    <dx:ListEditItem Text="4" Value="4" />
    <dx:ListEditItem Text="5" Value="5" />
    <dx:ListEditItem Text="6" Value="6" />
    <dx:ListEditItem Text="7" Value="7" />
    <dx:ListEditItem Text="8" Value="8" />
    <dx:ListEditItem Text="9" Value="9" />
    <dx:ListEditItem Text="10" Value="10" />
    <dx:ListEditItem Text="11" Value="11" />
    <dx:ListEditItem Text="12" Value="12" />
    <dx:ListEditItem Text="13" Value="13" />
    <dx:ListEditItem Text="14" Value="14" />
    <dx:ListEditItem Text="15" Value="15" />
    <dx:ListEditItem Text="16" Value="16" />
    <dx:ListEditItem Text="17" Value="17" />
    <dx:ListEditItem Text="18" Value="18" />
    <dx:ListEditItem Text="19" Value="19" />
    <dx:ListEditItem Text="20" Value="20" />
    <dx:ListEditItem Text="21" Value="21" />
    <dx:ListEditItem Text="22" Value="22" />
    <dx:ListEditItem Text="23" Value="23" />
    <dx:ListEditItem Text="24" Value="24" />
    <dx:ListEditItem Text="25" Value="25" />
    <dx:ListEditItem Text="26" Value="26" />
    <dx:ListEditItem Text="27" Value="27" />
    <dx:ListEditItem Text="28" Value="28" />
    <dx:ListEditItem Text="29" Value="29" />
    <dx:ListEditItem Text="30" Value="30" />
    <dx:ListEditItem Text="31" Value="31" />
</Items>
</dx:ASPxComboBox></td>
<td>&nbsp;</td>
<td><dx:ASPxComboBox ID="ddlmonth" runat="server" EncodeHtml="False" Width="100px">
<Items>
    <dx:ListEditItem Text="Month" Value="" />
    <dx:ListEditItem Text="1" Value="1" />
    <dx:ListEditItem Text="2" Value="2" />
    <dx:ListEditItem Text="3" Value="3" />
    <dx:ListEditItem Text="4" Value="4" />
    <dx:ListEditItem Text="5" Value="5" />
    <dx:ListEditItem Text="6" Value="6" />
    <dx:ListEditItem Text="7" Value="7" />
    <dx:ListEditItem Text="8" Value="8" />
    <dx:ListEditItem Text="9" Value="9" />
    <dx:ListEditItem Text="10" Value="10" />
    <dx:ListEditItem Text="11" Value="11" />
    <dx:ListEditItem Text="12" Value="12" />
</Items>
</dx:ASPxComboBox></td>
<td>&nbsp;</td>
<td><dx:ASPxComboBox ID="ddlyear" runat="server" EncodeHtml="False" Width="100px">
<Items>
    <dx:ListEditItem Text="Year" Value="" />
</Items>
</dx:ASPxComboBox></td>
    </tr>
</table>



<%--
<select style="width: 100px" id="ddlday" runat="server">
<option value="">Day</option>
<option value="1">1</option>
<option value="2">2</option>
<option value="3">3</option>
<option value="4">4</option>
<option value="5">5</option>
<option value="6">6</option>
<option value="7">7</option>
<option value="8">8</option>
<option value="9">9</option>
<option value="10">10</option>
<option value="11">11</option>
<option value="12">12</option>
<option value="13">13</option>
<option value="14">14</option>
<option value="15">15</option>
<option value="16">16</option>
<option value="17">17</option>
<option value="18">18</option>
<option value="19">19</option>
<option value="20">20</option>
<option value="21">21</option>
<option value="22">22</option>
<option value="23">23</option>
<option value="24">24</option>
<option value="25">25</option>
<option value="26">26</option>
<option value="27">27</option>
<option value="28">28</option>
<option value="29">29</option>
<option value="30">30</option>
<option value="31">31</option>
</select>
--%>
<%--
<select style="width: 100px" id="ddlmonth" runat="server">
<option value="">Month</option>
<option value="1">1</option>
<option value="2">2</option>
<option value="3">3</option>
<option value="4">4</option>
<option value="5">5</option>
<option value="6">6</option>
<option value="7">7</option>
<option value="8">8</option>
<option value="9">9</option>
<option value="10">10</option>
<option value="11">11</option>
<option value="12">12</option>
</select>
--%>
<%--
<select style="width: 100px" id="ddlyear" runat="server">
<option value="">Year</option>
<option value="2000">2000</option>
<option value="1999">1999</option>
<option value="1998">1998</option>
<option value="1997">1997</option>
<option value="1996">1996</option>
<option value="1995">1995</option>
<option value="1994">1994</option>
<option value="1993">1993</option>
<option value="1992">1992</option>
<option value="1991">1991</option>
<option value="1990">1990</option>
<option value="1989">1989</option>
<option value="1988">1988</option>
<option value="1987">1987</option>
<option value="1986">1986</option>
<option value="1985">1985</option>
<option value="1984">1984</option>
<option value="1983">1983</option>
<option value="1982">1982</option>
<option value="1981">1981</option>
<option value="1980">1980</option>
<option value="1979">1979</option>
<option value="1978">1978</option>
<option value="1977">1977</option>
<option value="1976">1976</option>
<option value="1975">1975</option>
<option value="1974">1974</option>
<option value="1973">1973</option>
<option value="1972">1972</option>
<option value="1971">1971</option>
<option value="1970">1970</option>
<option value="1969">1969</option>
<option value="1968">1968</option>
<option value="1967">1967</option>
<option value="1966">1966</option>
<option value="1965">1965</option>
<option value="1964">1964</option>
<option value="1963">1963</option>
<option value="1962">1962</option>
<option value="1961">1961</option>
<option value="1960">1960</option>
<option value="1959">1959</option>
<option value="1958">1958</option>
<option value="1957">1957</option>
<option value="1956">1956</option>
<option value="1955">1955</option>
<option value="1954">1954</option>
<option value="1953">1953</option>
<option value="1952">1952</option>
<option value="1951">1951</option>
<option value="1950">1950</option>
<option value="1949">1949</option>
<option value="1948">1948</option>
<option value="1947">1947</option>
<option value="1946">1946</option>
<option value="1945">1945</option>
<option value="1944">1944</option>
<option value="1943">1943</option>
<option value="1942">1942</option>
<option value="1941">1941</option>
<option value="1940">1940</option>
<option value="1939">1939</option>
<option value="1938">1938</option>
<option value="1937">1937</option>
<option value="1936">1936</option>
<option value="1935">1935</option>
<option value="1934">1934</option>
<option value="1933">1933</option>
<option value="1932">1932</option>
<option value="1931">1931</option>
<option value="1930">1930</option>
<option value="1929">1929</option>
<option value="1928">1928</option>
<option value="1927">1927</option>
<option value="1926">1926</option>
<option value="1925">1925</option>
<option value="1924">1924</option>
<option value="1923">1923</option>
<option value="1922">1922</option>
<option value="1921">1921</option>
<option value="1920">1920</option>
</select>--%>
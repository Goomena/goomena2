﻿<%@ Control Language="vb" AutoEventWireup="false" 
    CodeBehind="LikesControl.ascx.vb" 
    Inherits="Dating.Referrals.Server.Site.Web.LikesControl" %>
<asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">

    <asp:View ID="View1" runat="server">
    </asp:View>
    
    
    <asp:View ID="vwNoPhoto" runat="server">
        <asp:FormView ID="fvNoPhoto" runat="server">
        <ItemTemplate>
<div class="items_hard">
    <%# Eval("HasNoPhotosText")%>
	<p><asp:HyperLink ID="lnk" runat="server" CssClass="btn lighter" NavigateUrl="~/Members/Photos.aspx"><%# Eval("AddPhotosText")%><i class="icon-chevron-right"></i></asp:HyperLink></p>
	<div class="clear"></div>
</div>
        </ItemTemplate>
        </asp:FormView>
    </asp:View>
    
    
    <asp:View ID="vwNoOffers" runat="server">
        <asp:FormView ID="fvNoOffers" runat="server" Width="69px">
        <ItemTemplate>
<div class="items_none">
    <%# Eval("NoOfferText")%>
	<p><asp:HyperLink ID="lnk4" runat="server" CssClass="btn lighter" NavigateUrl="~/Members/Search.aspx"><%# Eval("SearchOurMembersText")%><i class="icon-chevron-right"></i></asp:HyperLink></p>
	<div class="clear"></div>
</div>
        </ItemTemplate>
        </asp:FormView>
    </asp:View>


    <asp:View ID="vwNewOffers" runat="server">
<asp:Repeater ID="rptNew" runat="server">
<ItemTemplate>
<div class="l_item deal_right">
    <div class="left">
    
        <div class="photo">
            <div class="thumb">
                <asp:HyperLink ID="hl2" runat="server" NavigateUrl='<%# Eval("ProfileViewUrl") %>'><img src="<%# Eval("ImageUrl") %>" /></asp:HyperLink></div>
        </div>
        <div class="info">
            <h2 class="wink"><%# Eval("YouReceivedWinkOfferText")%></h2>
            <p id="pWink" runat="server" visible='<%# Eval("IsWink") %>'><%# Eval("WantToKnowFirstDatePriceText")%></p>
            <p id="pPoke" runat="server" visible='<%# Eval("IsPoke") %>'><%# Eval("YouReceivedPokeDescriptionText")%></p>
            <p id="pAmount" runat="server" visible='<%# Eval("IsOffer") %>'><%# Eval("WillYouAcceptDateWithForAmountText")%></p>
 			<div class="bot_actions lfloat">
                <asp:Panel CssClass="rfloat icon_tooltipholder" runat="server" ID="divTooltipholder" Visible='<%# Eval("AllowTooltipPopupLikes")%>'>
                    <asp:LinkButton ID="lnkWhatIs" runat="server" OnClientClick="OnMoreInfoClick('[contentUrl]',this,'[headerText]');return false;">
                        <img runat="server" id="icon_tip" src="~/Images/icon_tip16.png" alt=""/></asp:LinkButton>
                </asp:Panel>
                <div class="lfloat">
                <asp:HyperLink ID="lnkNewMakeOffer" runat="server" visible='<%# Eval("AllowCreateOffer") %>' NavigateUrl='<%# Eval("CreateOfferUrl") %>'
                    class="btn btn-small"><span><%# Eval("MakeOfferText")%></span></asp:HyperLink>
                </div>
                <div class="lfloat">
                <asp:LinkButton ID="lnkNewAccept" runat="server" Visible='<%# Eval("AllowAccept") %>' CommandName="OFFERACCEPT" CommandArgument='<%# Eval("OfferID")%>' 
                    class="btn btn-small"><span><%# Eval("AcceptText")%></span></asp:LinkButton>
                </div>
                <div class="lfloat">
                <asp:LinkButton ID="lnkNewCounter" runat="server" Visible='<%# Eval("AllowCounter") %>' CommandName="OFFERCOUNTER" CommandArgument='<%# Eval("OfferID")%>' 
                    class="btn btn-small"><span><%# Eval("CounterText")%></span></asp:LinkButton>
                </div>
                <div class="btn-group" ID="RejectsMenu" runat="server" Visible='<%# Eval("AllowRejectsMenu") %>'>
                    <a class="btn btn-small dropdown-toggle" data-toggle="dropdown" href="#"><%# Eval("RejectText")%> <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><asp:LinkButton ID="lnkRejectType" runat="server" CommandName="REJECTTYPE" CommandArgument='<%# Eval("OfferID")%>'><i class="icon-chevron-right"></i><%# Eval("NotInterestedText")%></asp:LinkButton></li>
                        <li><asp:LinkButton ID="lnkRejectFar" runat="server" CommandName="REJECTFAR" CommandArgument='<%# Eval("OfferID")%>'><i class="icon-chevron-right"></i><%# Eval("TooFarAwayText")%></asp:LinkButton></li>
                        <li><asp:LinkButton ID="lnkRejectBad" runat="server" CommandName="REJECTBAD" CommandArgument='<%# Eval("OfferID")%>'><i class="icon-chevron-right"></i><%# Eval("NotEnoughInfoText")%></asp:LinkButton></li>
                        <li><asp:LinkButton ID="lnkRejectExp" runat="server" CommandName="REJECTEXPECTATIONS" CommandArgument='<%# Eval("OfferID")%>'><i class="icon-chevron-right"></i><%# Eval("DifferentExpectationsText")%></asp:LinkButton></li>
                    </ul>
                </div>
                <div class="btn-group" ID="ActionsMenu" runat="server" Visible='<%# Eval("AllowActionsMenu") %>'>
                    <a class="btn btn-small dropdown-toggle" data-toggle="dropdown" href="#"><%# Eval("ActionsText")%> <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li ID="liActsSendMsgOnce" runat="server" visible='<%# Eval("AllowActionsSendMessageOnce")%>'><asp:HyperLink 
                                ID="lnkSendMsgOnce" runat="server" NavigateUrl='<%# Eval("SendMessageUrlOnce")%>'><%# Eval("SendMessageOnceText")%></asp:HyperLink></li>

                        <li ID="liActsSendMsgMany" runat="server" visible="false"><%--visible='<%# Eval("AllowActionsSendMessageMany")%>'--%><asp:HyperLink 
                                ID="lnkSendMsgMany" runat="server" NavigateUrl='<%# Eval("SendMessageUrlMany")%>'><%# Eval("SendMessageManyText")%></asp:HyperLink></li>

                        <li ID="liActsSendMsg" runat="server" visible='<%# Eval("AllowActionsSendMessage")%>'><asp:HyperLink 
                                ID="lnkSendMsg2" runat="server" NavigateUrl='<%# Eval("SendMessageUrl")%>'><%# Eval("SendMessageText")%></asp:HyperLink></li><%--<i class="icon-chevron-right"></i>--%>

                        <li ID="liActsUnlock" runat="server" visible='<%# Eval("AllowActionsUnlock")%>'><asp:LinkButton 
                                ID="lnkActsUnlock" runat="server" CommandName="UNLOCKOFFER" CommandArgument='<%# Eval("OfferId") %>'><%# Eval("ActionsUnlockText")%></asp:LinkButton></li><%--<i class="icon-chevron-right icon-white"></i>--%>

                        <li ID="liActsMakeOffer" runat="server" visible='<%# Eval("AllowActionsCreateOffer") %>' ><asp:HyperLink 
                                ID="lnkNewMakeOffer2" runat="server" NavigateUrl='<%# Eval("CreateOfferUrl") %>'><span><%# Eval("ActionsMakeOfferText")%></span></asp:HyperLink></li>

                        <li ID="liActsPoke" runat="server" visible='<%# Eval("AllowActionsPoke")%>'><asp:LinkButton ID="lnkPoke" runat="server" 
                                CommandName="POKE" CommandArgument='<%# "offr_" & Eval("OfferID") & "-prof_" & Eval("OtherMemberProfileID") %>'><%# Eval("PokeText")%></asp:LinkButton></li>
                    
                        <li ID="liActsDelOffr" runat="server" visible='<%# Eval("AllowActionsDeleteOffer")%>'><asp:LinkButton 
                                ID="lnkDelOffr" runat="server" CommandName="DELETEOFFER" CommandArgument='<%# Eval("OfferId") %>' OnClientClick='<%#  Eval("OfferDeleteConfirmMessage")  %>'><%# Eval("DeleteOfferText")%></asp:LinkButton></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="right plane ">
        <div class="photo">
            <div class="thumb">
                <asp:HyperLink ID="lnk1" runat="server" NavigateUrl='<%# Eval("OtherMemberProfileViewUrl")%>'><img alt="" src="<%# Eval("OtherMemberImageUrl")%>"/></asp:HyperLink>
            </div>
        </div>
        <div class="info">
            <h2><asp:HyperLink ID="lnk2" runat="server" NavigateUrl='<%# Eval("OtherMemberProfileViewUrl")%>'><%# Eval("OtherMemberLoginName")%></asp:HyperLink></h2>
            <p><%# Eval("OtherMemberHeading")%></p>
            <div class="stats">
                <%# MyBase.WriteOffer_MemberInfo(Container.DataItem)%>
                <div class="tooltip_offer" style="display: none;">
                    <div class="tooltip_text"><%# Eval("MilesAwayText")%></div>
                </div>
            </div>
            <div class="tt_enabled_distance distance_absolute <%# Eval("DistanceCss")%>">
            </div>
        </div>
    </div>
    <div class="clear"></div>
</div>
</ItemTemplate>
</asp:Repeater>
    </asp:View>


    <asp:View ID="vwPendingOffers" runat="server">
<asp:Repeater ID="rptPending" runat="server">
<ItemTemplate>

<div class="l_item deal_left"  login="<%# Eval("OtherMemberLoginName")%>">
    <div class="left">
        <div class="photo">
            <div class="thumb">
                <asp:HyperLink ID="hl2" runat="server" NavigateUrl='<%# Eval("ProfileViewUrl") %>'><img src="<%# Eval("ImageUrl") %>" alt="" /></asp:HyperLink></div>
        </div>
        <div class="info">
            <h2><%# Eval("AwaitingResponseText")%></h2>
            <p><%# Eval("YourWinkSentText")%></p>
 			<div class="bot_actions lfloat">
                <asp:Panel CssClass="rfloat icon_tooltipholder" runat="server" ID="divTooltipholder" Visible='<%# Eval("AllowTooltipPopupLikes")%>'>
                    <asp:LinkButton ID="lnkWhatIs" runat="server" OnClientClick="OnMoreInfoClick('[contentUrl]',this,'[headerText]');return false;">
                        <img runat="server" id="icon_tip" src="~/Images/icon_tip16.png" alt=""/></asp:LinkButton>
                </asp:Panel>
                <div class="lfloat">
                    <asp:LinkButton ID="lnkCancelWink" runat="server" CssClass="btn btn-small" Visible='<%# Eval("AllowCancelWink") %>' CommandName="CANCELWINK" CommandArgument='<%# Eval("OfferID")%>'><span><%# Eval("CancelWinkText")%></span></asp:LinkButton>
                </div>
                <div class="lfloat">
                    <asp:LinkButton ID="lnkCancelOffer" runat="server" CssClass="btn btn-small" Visible='<%# Eval("AllowCancelPendingOffer")%>' CommandName="CANCELOFFER" CommandArgument='<%# Eval("OfferID")%>'><span><%# Eval("CancelOfferText")%></span></asp:LinkButton>
                </div>
                <div class="lfloat">
                    <asp:LinkButton ID="lnkCancelPoke" runat="server" CssClass="btn btn-small" Visible='<%# Eval("AllowCancelPoke")%>' CommandName="CANCELPOKE" CommandArgument='<%# Eval("OfferID")%>'><span><%# Eval("CancelPokeText")%></span></asp:LinkButton>
                </div>
            </div>
        </div>
    </div>
    <div class="right car ">
        <div class="photo">
            <div class="thumb">
                <asp:HyperLink ID="lnk1" runat="server" NavigateUrl='<%# Eval("OtherMemberProfileViewUrl") %>'><img src="<%# Eval("OtherMemberImageUrl") %>" /></asp:HyperLink></div>
        </div>
        <div class="info">
            <h2><asp:HyperLink ID="lnk2" runat="server" NavigateUrl='<%# Eval("OtherMemberProfileViewUrl") %>' Text='<%# Eval("OtherMemberLoginName")%>' /></h2>
            <p><%# Eval("OtherMemberHeading")%></p>
            <div class="stats">
                <%# MyBase.WriteOffer_MemberInfo(Container.DataItem)%>
                <div class="tt_enabled_distance distance_absolute <%# Eval("DistanceCss")%>">
                </div>
                <div class="tooltip_offer" style="display: none;">
                    <div class="tooltip_text"><%# Eval("MilesAwayText")%></div>
                </div>
            </div>
        </div>
    </div>
    <div class="clear">
    </div>
</div>
</ItemTemplate>
</asp:Repeater>
    </asp:View>


    <asp:View ID="vwRejectedOffers" runat="server">
<asp:Repeater ID="rptRejected" runat="server">
<ItemTemplate>

<div class="l_item <%# Eval("RejectCssClass")  %>"  login="<%# Eval("OtherMemberLoginName")%>">	
	<div class="left">
        <div class="photo">
            <div class="thumb">
                <asp:HyperLink ID="hl2" runat="server" NavigateUrl='<%# Eval("ProfileViewUrl") %>'><img src="<%# Eval("ImageUrl") %>" alt="" /></asp:HyperLink></div>
        </div>
		<div class="info">
			<h2 class="rejected"><%# Eval("CancelledRejectedTitleText")%></h2>
			<p style="padding: 5px 0 0; position: relative; width: 300px;"><%# Eval("CancelledRejectedDescriptionText")%></p>
 			<div class="bot_actions lfloat">
                <asp:Panel CssClass="rfloat icon_tooltipholder" runat="server" ID="divTooltipholder" Visible="false"><%--Visible='<%# Eval("AllowTooltipPopupLikes")%>'--%>
                    <asp:LinkButton ID="lnkWhatIs" runat="server" OnClientClick="OnMoreInfoClick('[contentUrl]',this,'[headerText]');return false;">
                        <img runat="server" id="icon_tip" src="~/Images/icon_tip16.png" alt=""/></asp:LinkButton>
                </asp:Panel>
                <div class="lfloat">
                    <asp:LinkButton ID="lnkTryWink" runat="server" Visible='<%# Eval("AllowTryWink")%>' CommandName="TRYWINK" CommandArgument='<%# Eval("OfferId") %>' CssClass="btn btn-small"><%# Eval("TryWinkText")%></asp:LinkButton>
                </div>
                <div class="lfloat">
                    <asp:LinkButton ID="lnkDeleteWink" runat="server" Visible='<%# Eval("AllowDeleteWink")%>' CommandName="DELETEOFFER" CommandArgument='<%# Eval("OfferId") %>' CssClass="btn btn-small"><%# Eval("DeleteWinkText")%></asp:LinkButton>
                </div>
                <div class="lfloat">
                    <asp:LinkButton ID="lnkCreateOffer" runat="server" Visible='<%# Eval("AllowCreateOffer")%>' CommandName="TRYCREATEOFFER" CommandArgument='<%# Eval("OfferId") %>' CssClass="btn btn-small"><%# Eval("MakeNewOfferText")%></asp:LinkButton>
                </div>
                <div class="lfloat">
                    <asp:LinkButton ID="lnkDeleteOffer" runat="server" Visible='<%# Eval("AllowDeleteOffer")%>' CommandName="DELETEOFFER" CommandArgument='<%# Eval("OfferId") %>' CssClass="btn btn-small"><%# Eval("DeleteOfferText")%></asp:LinkButton>
                </div>
			</div>
		</div>
	</div>
	<div class="right car ">
        <div class="photo">
            <div class="thumb">
                <asp:HyperLink ID="lnk2" runat="server" NavigateUrl='<%# Eval("OtherMemberProfileViewUrl") %>'><img src="<%# Eval("OtherMemberImageUrl") %>" /></asp:HyperLink></div>
        </div>
		<div class="info">
            <h2><asp:HyperLink ID="lnk3" runat="server" NavigateUrl='<%# Eval("OtherMemberProfileViewUrl") %>' Text='<%# Eval("OtherMemberLoginName")%>' /></h2>
			<p><%# Eval("OtherMemberHeading")%></p>
			<div class="stats">
                <%# MyBase.WriteOffer_MemberInfo(Container.DataItem)%>
				<div class="tooltip_offer" style="display: none;">
					<div class="tooltip_text"><%# Eval("MilesAwayText")%></div>
				</div>
			</div>
			<div class="tt_enabled_distance distance_absolute <%# Eval("DistanceCss")%>"></div>
		</div>
	</div>
	<div class="clear"></div>
</div>
</ItemTemplate>
</asp:Repeater>
    </asp:View>


    <asp:View ID="vwAcceptedOffers" runat="server">
<asp:Repeater ID="rptAccepted" runat="server">
<ItemTemplate>

<div class="l_item deal_accepted new_date" login="<%# Eval("OtherMemberLoginName")%>">
	<div class="left accepted_left">
        <div class="photo">
            <div class="thumb">
                <asp:HyperLink ID="hl2" runat="server" NavigateUrl='<%# Eval("ProfileViewUrl") %>'><img src="<%# Eval("ImageUrl") %>" alt="" /></asp:HyperLink></div>
        </div>
		<div class="info">
			<h2><%# Eval("OfferAcceptedWithAmountText") %></h2>
			<p class="padding"><%# Eval("OfferAcceptedHowToContinueText") %></p>
 			<div class="bot_actions lfloat">
                <asp:Panel CssClass="rfloat icon_tooltipholder" runat="server" ID="divTooltipholder" Visible="false"><%--Visible='<%# Eval("AllowTooltipPopupLikes")%>'>--%>
                    <asp:LinkButton ID="lnkWhatIs" runat="server" OnClientClick="OnMoreInfoClick('[contentUrl]',this,'[headerText]');return false;">
                        <img runat="server" id="Img1" src="~/Images/icon_tip16.png" alt=""/></asp:LinkButton>
                </asp:Panel>
                <div class="rfloat">
                    <asp:HyperLink ID="lnk1" runat="server" Visible='<%# Eval("AllowSendMessage")%>' CssClass="btn btn-small lighter" NavigateUrl='<%# Eval("SendMessageUrl")%>'><%# Eval("SendMessageText")%><i class="icon-chevron-right"></i></asp:HyperLink>
                </div>
                <div class="rfloat">
                    <asp:LinkButton ID="lnk2" runat="server" Visible='<%# Eval("AllowDeleteAcceptedOffer")%>' CommandName="DELETEOFFER" CommandArgument='<%# Eval("OfferId") %>' CssClass="btn btn-small" OnClientClick='<%#  Eval("OfferDeleteConfirmMessage")  %>'><%# Eval("DeleteOfferText")%></asp:LinkButton>
                </div>
                <div class="rfloat">
                    <asp:LinkButton ID="lnk3" runat="server" Visible='<%# Eval("AllowOfferAcceptedUnlock")%>' CssClass="btn btn-primary" CommandName="UNLOCKOFFER" CommandArgument='<%# Eval("OfferId") %>'><%# Eval("OfferAcceptedUnlockText")%><i class="icon-chevron-right icon-white"></i></asp:LinkButton>
                </div>
                <div class="btn-group" ID="ActionsMenu" runat="server" Visible='<%# Eval("AllowActionsMenu") %>'>
                    <a class="btn btn-small dropdown-toggle" data-toggle="dropdown" href="#"><%# Eval("ActionsText")%> <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li ID="liActsSendMsgOnce" runat="server" visible='<%# Eval("AllowActionsSendMessageOnce")%>'><asp:HyperLink 
                                ID="lnkSendMsgOnce" runat="server" NavigateUrl='<%# Eval("SendMessageUrlOnce")%>'><%# Eval("SendMessageOnceText")%></asp:HyperLink></li>

                        <li ID="liActsSendMsgMany" runat="server" visible="false"><%-- visible='<%# Eval("AllowActionsSendMessageMany")%>'--%><asp:HyperLink 
                                ID="lnkSendMsgMany" runat="server" NavigateUrl='<%# Eval("SendMessageUrlMany")%>'><%# Eval("SendMessageManyText")%></asp:HyperLink></li>
                    </ul>
                </div>
                <%--<asp:Panel CssClass="lfloat icon_tooltipholder" runat="server" ID="divTooltipholder" Visible='<%# Eval("AllowTooltipUnlockNotice")%>'>
                    <a href="#" class="tooltiptipsy-n" title="<%# Eval("UnlockNotice") %>">
                        <img runat="server" id="icon_tip" src="~/Images/icon_tip16.png" alt=""/></a>
                </asp:Panel>--%>
			</div>
            <asp:Panel CssClass="rfloat" runat="server" ID="pnlMsgSent" Visible='<%# Eval("IsMessageSentForOffer")%>' style="margin-left:5px;padding-top:15px;">
                <asp:Image ID="imgCheck" runat="server" Imageurl="~/Images/green.check.png" ImageAlign="AbsMiddle" /><asp:Label ID="lblMsgSent" runat="server" Text='<%# Eval("MessageSentNotice") %>'></asp:Label>
            </asp:Panel>
	        <div class="clear"></div>
		</div>
	    <div class="clear"></div>
    </div>

	<div class="right accepted_right ">
        <div class="photo">
            <div class="thumb">
                <asp:HyperLink ID="hl3" runat="server" NavigateUrl='<%# Eval("OtherMemberProfileViewUrl") %>'><img src="<%# Eval("OtherMemberImageUrl") %>" /></asp:HyperLink></div>
        </div>
	</div>
	<div class="clear"></div>
</div>
</ItemTemplate>
</asp:Repeater>
    </asp:View>



</asp:MultiView>














﻿Imports Dating.Server.Core.DLL
Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Datasets.DLL.DSMembers

Public Class ProfilePreview
    Inherits BaseUserControl



    Dim _pageData As clsPageData
    Protected ReadOnly Property CurrentPageData As clsPageData
        Get
            If (_pageData Is Nothing) Then _pageData = New clsPageData("~/Members/control.ProfileView", Context)
            Return _pageData
        End Get
    End Property




    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            If (Not Me.IsPostBack) Then
                LoadLAG()

                'Dim profileId As Integer = Me.Session("ProfileID")
                Me.LoadProfile(Me.MasterProfileId)
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

        If (Not Page.IsPostBack) Then
        End If

    End Sub


    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
    End Sub


    Protected Sub LoadLAG()
        Try
            Dim cPageBasic As Dating.Server.Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic


        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Sub LoadProfile(ByVal profileId As Integer)
        Try

            Dim _profilerows As New clsProfileRows(profileId)
            Dim mirrorRow As EUS_ProfilesRow = _profilerows.GetMirrorRow()

            ltrName.Text = mirrorRow.LoginName


            If ProfileHelper.IsMale(mirrorRow.GenderId) Then

                'If (ProfileHelper.IsGenerous(mirrorRow.AccountTypeId)) Then
                ltrInfo.Text = globalStrings.GetCustomString("msg_GenerousMale")
                'ElseIf (ProfileHelper.IsAttractive(mirrorRow.AccountTypeId)) Then
                '    ltrInfo.Text = globalStrings.GetCustomString("msg_AttractiveMale")
                'End If

            ElseIf ProfileHelper.IsFemale(mirrorRow.GenderId) Then

                'If (ProfileHelper.IsGenerous(mirrorRow.AccountTypeId)) Then
                '    ltrInfo.Text = globalStrings.GetCustomString("msg_GenerousFemale")
                'ElseIf (ProfileHelper.IsAttractive(mirrorRow.AccountTypeId)) Then
                ltrInfo.Text = globalStrings.GetCustomString("msg_AttractiveFemale")
                'End If

            End If



            Dim fileName As String = Nothing
            Dim photo As DSMembers.EUS_CustomerPhotosRow = DataHelpers.GetProfilesDefaultPhoto(profileId)
            If (photo IsNot Nothing) Then
                imgProfile.Src = ProfileHelper.GetProfileImageURL(photo.CustomerID, photo.FileName, mirrorRow.GenderId, True, Me.IsHTTPS)
            Else
                imgProfile.Src = ProfileHelper.GetDefaultImageURL(mirrorRow.GenderId)
            End If
            If (profileId = 269) Then
                If (Request.Url.Scheme = "https") Then
                    imgProfile.Src = imgProfile.Src.Replace("http://", "https://")
                End If
            End If


            Dim totalMemberCredits As Integer?
            Dim memberCreditsQuery = From itm In Me.CMSDBDataContext.EUS_CustomerCredits
                                   Where itm.CustomerId = Me.MasterProfileId
                                   Select itm.Credits

            If (memberCreditsQuery IsNot Nothing) Then
                totalMemberCredits = memberCreditsQuery.Sum()
            End If

            Try
                Dim sql As String = <sql><![CDATA[
select 
	(select 	SUM(o.Credits) from EUS_CustomerCredits o where (o.CustomerId={0})) 
	as AvailableCredits
	,(select 	Count(*) from EUS_CustomerCredits o where (o.CustomerId={0})) 
	as RecordsCount
]]></sql>.Value
                sql = String.Format(sql, Me.MasterProfileId)
                Dim dt As DataTable = DataHelpers.GetDataTable(sql)

                If (dt.Rows(0)("RecordsCount") > 0) Then
                    lnkBilling.NavigateUrl = "~/Members/SelectProduct.aspx"
                    lnkBilling.Text = "Credits: " & dt.Rows(0)("AvailableCredits")
                Else
                    lnkBilling.NavigateUrl = "~/Members/SelectProduct.aspx"
                    lnkBilling.Text = "Standard Member"
                End If

                'If (dt.Rows(0)("NewDate") = 0) Then
                '    lnkNewDates.CssClass = "dates no_bg"
                'End If
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try


        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Public Sub RefreshThumb()

        'Dim profileId As Integer = Me.Session("ProfileID")
        Me.LoadProfile(Me.MasterProfileId)

    End Sub


End Class
﻿Public Class MessageNoPhoto
    Inherits BaseUserControl

    Dim _pageData As clsPageData
    Protected ReadOnly Property CurrentPageData As clsPageData
        Get
            If (_pageData Is Nothing) Then _pageData = New clsPageData("~/Members/Information.aspx", Context)
            Return _pageData
        End Get
    End Property



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            If (Not Me.IsPostBack) Then
                LoadLAG()
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try
    End Sub

    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
    End Sub


    Protected Sub LoadLAG()
        Try
            '  SetControlsValue(Me, CurrentPageData)
            msg_AddPhotosText.Text = CurrentPageData.GetCustomString("msg_AddPhotosText")
            msg_HasNoPhotosText.Text = CurrentPageData.GetCustomString("msg_HasNoPhotosText")
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

End Class
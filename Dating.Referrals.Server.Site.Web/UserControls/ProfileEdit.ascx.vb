﻿Imports Dating.Server.Core.DLL
Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Datasets.DLL.DSMembers
Imports DevExpress.Web.ASPxEditors

Public Class ProfileEdit
    Inherits BaseUserControl


    'Private EditMasterProfileID As Integer
    'Private EditMirrorProfileID As Integer
    'Private EditMasterRowIndex As Integer
    'Private EditMirrorRowIndex As Integer


    Protected Property PageHasErrors As Boolean
    Protected Property HasAgeIssue As Boolean

    Dim _pageData As clsPageData
    Protected ReadOnly Property CurrentPageData As clsPageData
        Get
            If (_pageData Is Nothing) Then _pageData = New clsPageData("~/Members/control.ProfileEdit", Context)
            Return _pageData
        End Get
    End Property


    Private Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        Try

            '''''''''''''''''''''''''''''
            ' !!!   visibility is set by profile.aspx, if set to false do not load control
            '''''''''''''''''''''''''''''
            If (Me.Visible) Then

                'Try
                '    Dim cPageBasic As Dating.Server.Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
                '    GeneralFunctions.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
                'Catch ex As Exception
                '    WebErrorMessageBox(Me, ex, "Page_Load")
                'End Try

                If (Not Me.IsPostBack) Then
                    LoadLAG()

                    'Dim profileId As Integer = Me.Session("ProfileID")
                    Me.LoadProfile(Me.MasterProfileId)
                End If

            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub


    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender

        Try
            Dim cPageBasic As Dating.Server.Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub


    Public Overrides Sub Master_LanguageChanged()

        If (Me.Visible) Then
            Me._pageData = Nothing
            LoadLAG()
            Me.LoadProfile(Me.MasterProfileId)
        End If

    End Sub


    Protected Sub LoadLAG()
        Try
            Dim cPageBasic As Dating.Server.Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic


            Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_Gender, Me.Session("LagID"), "GenderId", rblGender, True)
            'Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_AccountType, Me.Session("LagID"), "AccountTypeId", cbAccountType, True, "US")
            'cbAccountType.Items(0).Selected = True


            'Web.ClsCombos.FillCombo(gLAG.ConnectionString, "SYS_CountriesGEO", "PrintableName", "Iso", cbCountry, True, False, "PrintableName", "ASC")
            Web.ClsCombos.FillComboUsingDatatable(SYS_CountriesGEOHelper.gDSListsGEO__IsEnabled_PrintableNameASC.SYS_CountriesGEO, "PrintableName", "Iso", cbCountry, True, "PrintableName")
            Dim def As DevExpress.Web.ASPxEditors.ListEditItem = Nothing 'cbCountry.Items.FindByValue(Session("GEO_COUNTRY_CODE"))
            If (def IsNot Nothing) Then
                def.Selected = True
            Else
                def = New DevExpress.Web.ASPxEditors.ListEditItem(CurrentPageData.GetCustomString("cbCountry_default"), "-1")
                def.Selected = True
                cbCountry.Items.Insert(0, def)
            End If


            Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_Education, Session("LagID"), "EducationId", cbEducation, True, "US")
            cbEducation.Items(0).Selected = True

            Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_Income, Session("LagID"), "IncomeId", cbIncome, True, "US")
            cbIncome.Items(0).Selected = True

            'Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_NetWorth, Session("LagID"), "NetWorthId", cbNetWorth, True, "US")
            'cbNetWorth.Items(0).Selected = True

            Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_Height, Session("LagID"), "HeightId", cbHeight, True, "US")
            cbHeight.Items(0).Selected = True

            Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_BodyType, Session("LagID"), "BodyTypeId", cbBodyType, True, "US")
            cbBodyType.Items(0).Selected = True

            Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_EyeColor, Session("LagID"), "EyeColorId", cbEyeColor, True, "US")
            cbEyeColor.Items(0).Selected = True

            Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_HairColor, Session("LagID"), "HairColorId", cbHairClr, True, "US")
            cbHairClr.Items(0).Selected = True

            Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_ChildrenNumber, Session("LagID"), "ChildrenNumberId", cbChildren, True, "US")
            cbChildren.Items(0).Selected = True

            Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_Ethnicity, Session("LagID"), "EthnicityId", cbEthnicity, True, "US")
            cbEthnicity.Items(0).Selected = True

            Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_Religion, Session("LagID"), "ReligionId", cbReligion, True, "US")
            cbReligion.Items(0).Selected = True

            Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_Smoking, Session("LagID"), "SmokingId", cbSmoking, True, "US")
            cbSmoking.Items(0).Selected = True

            Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_Drinking, Session("LagID"), "DrinkingId", cbDrinking, True, "US")
            cbDrinking.Items(0).Selected = True

            Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_RelationshipStatus, Session("LagID"), "RelationshipStatusId", cbRelationshipStatus, True, "US")
            cbRelationshipStatus.Items(0).Selected = True

            'Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_TypeOfDating, getlag(), "TypeOfDatingId", lbTypeOfDate, True)


            SetControlsValue(Me, CurrentPageData)

            chkTravelOnDate.Text = CurrentPageData.GetCustomString(chkTravelOnDate.ID)
            msg_LocationText.Text = CurrentPageData.GetCustomString(msg_LocationText.ID)
            msg_CountryText.Text = CurrentPageData.GetCustomString(msg_CountryText.ID)
            msg_RegionText.Text = CurrentPageData.GetCustomString(msg_RegionText.ID)
            msg_SelectZip.Text = CurrentPageData.GetCustomString(msg_SelectZip.ID)
            msg_CityText.Text = CurrentPageData.GetCustomString(msg_CityText.ID)
            msg_ZipCodeText.Text = CurrentPageData.GetCustomString(msg_ZipCodeText.ID)
            msg_SelectRegion.Text = CurrentPageData.GetCustomString(msg_SelectRegion.ID)
            msg_ZipExample.Text = CurrentPageData.GetCustomString(msg_ZipExample.ID)


            'If (Me.Session("LagID") = "HU") Then
            '    chkWAYLF_TypeDateShort.Text = _
            '        Lists.gDSLists.EUS_LISTS_TypeOfDating.Where(Function(row As DSLists.EUS_LISTS_TypeOfDatingRow) row.TypeOfDatingId = TypeOfDatingEnum.ShortTermRelationship).FirstOrDefault().HU

            'ElseIf (Me.Session("LagID") = "GR") Then
            '    chkWAYLF_TypeDateShort.Text = _
            '        Lists.gDSLists.EUS_LISTS_TypeOfDating.Where(Function(row As DSLists.EUS_LISTS_TypeOfDatingRow) row.TypeOfDatingId = TypeOfDatingEnum.ShortTermRelationship).FirstOrDefault().GR

            'Else ' take us 'If (Me.Session("LagID") = "US") Then
            '    chkWAYLF_TypeDateShort.Text = _
            '        Lists.gDSLists.EUS_LISTS_TypeOfDating.Where(Function(row As DSLists.EUS_LISTS_TypeOfDatingRow) row.TypeOfDatingId = TypeOfDatingEnum.ShortTermRelationship).FirstOrDefault().US

            'End If
            ''CurrentPageData.GetCustomString(chkWAYLF_TypeDateShort.ID)
            'chkWAYLF_TypeDateFriendship.Text = 'CurrentPageData.GetCustomString(chkWAYLF_TypeDateFriendship.ID)
            'chkWAYLF_TypeDateLong.Text = 'CurrentPageData.GetCustomString(chkWAYLF_TypeDateLong.ID)
            'chkWAYLF_TypeDateMutually.Text = 'CurrentPageData.GetCustomString(chkWAYLF_TypeDateMutually.ID)
            'chkWAYLF_TypeDateMarried.Text = 'CurrentPageData.GetCustomString(chkWAYLF_TypeDateMarried.ID)
            'chkWAYLF_TypeDateCasual.Text = 'CurrentPageData.GetCustomString(chkWAYLF_TypeDateCasual.ID)
            'chkPS_HideMeFromSearchResults.Text = CurrentPageData.GetCustomString(chkPS_HideMeFromSearchResults.ID)
            'chkPS_HideMeFromMembersIHaveBlocked.Text = CurrentPageData.GetCustomString(chkPS_HideMeFromMembersIHaveBlocked.ID)
            'chkPS_NotShowInOtherUsersFavoritedList.Text = CurrentPageData.GetCustomString(chkPS_NotShowInOtherUsersFavoritedList.ID)
            'chkPS_NotShowInOtherUsersViewedList.Text = CurrentPageData.GetCustomString(chkPS_NotShowInOtherUsersViewedList.ID)
            'chkPS_NotShowMyGifts.Text = CurrentPageData.GetCustomString(chkPS_NotShowMyGifts.ID)
            'chkPS_HideMyLastLoginDateTime.Text = CurrentPageData.GetCustomString(chkPS_HideMyLastLoginDateTime.ID)

            btnProfileUpd.Text = CurrentPageData.GetCustomString(btnProfileUpd.ID)

            'GeneralFunctions.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub



    Protected Sub btnProfileUpd_Click(sender As Object, e As EventArgs) Handles btnProfileUpd.Click
        Try
            'Dim profileId As Integer = Me.Session("ProfileID")
            ValidateInput()
            If (Page.IsValid AndAlso Not PageHasErrors) Then
                SaveProfile(Me.MasterProfileId)
                SaveProfile_ManuallyApprovingFields(Me.MasterProfileId)
            End If

            If (Not PageHasErrors AndAlso Not HasAgeIssue) Then
                'Dim defaultPhoto As DSMembers.EUS_CustomerPhotosRow = DataHelpers.GetProfilesDefaultPhoto(Me.MirrorProfileId)
                'If (defaultPhoto Is Nothing) Then

                Dim currentUserHasPhotos As Boolean = clsUserDoes.HasPhotos(Me.MasterProfileId, Me.MirrorProfileId)
                If (Not currentUserHasPhotos) Then
                    Response.Redirect(ResolveUrl("~/Members/Photos.aspx"), False)
                    HttpContext.Current.ApplicationInstance.CompleteRequest()
                Else
                    LoadProfile(Me.MasterProfileId)
                End If
            End If

            If (HasAgeIssue) Then
                Response.Redirect(ResolveUrl("~/Members/"), False)
                HttpContext.Current.ApplicationInstance.CompleteRequest()
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Private Sub ValidateInput()

        PageHasErrors = False
        Try
            Dim regMatch As Boolean
            If (clsConfigValues.Get__validate_email_address_normal()) Then
                regMatch = Regex.IsMatch(txteMail.Text, gEmailAddressValidationRegex)
            Else
                regMatch = Regex.IsMatch(txteMail.Text, gEmailAddressValidationRegex_Simple)
            End If

            pnleMailErr.Visible = False
            If (txteMail.Text.Trim() = "" OrElse Not regMatch) Then
                pnleMailErr.Visible = True
                lbleMailErr.Text = CurrentPageData.GetCustomString("txteMail_Required_ErrorText")
                If (Not regMatch) Then
                    lbleMailErr.Text = CurrentPageData.GetCustomString("txteMail_Invalid_ErrorText")
                End If
                txteMail.Focus()
                PageHasErrors = True
            End If

            pnlBirthdateErr.Visible = False
            If (liBirthdayContainer.Visible = True AndAlso ctlDate.DateTime Is Nothing) Then
                pnlBirthdateErr.Visible = True
                lblBirthdateErr.Text = CurrentPageData.GetCustomString("ctlBirthday_Required_ErrorText")
                ctlDate.Focus()
                PageHasErrors = True
            End If


            pnlErrorSelectLookingForMaleFemale.Visible = False
            If (Not PageHasErrors) Then
                If (Not chkLookingFemale.Checked AndAlso Not chkLookingMale.Checked) Then
                    pnlErrorSelectLookingForMaleFemale.Visible = True
                    lblErrorSelectLookingForMaleFemale.Text = CurrentPageData.GetCustomString(lblErrorSelectLookingForMaleFemale.ID)
                    chkLookingMale.Focus()
                    PageHasErrors = True
                End If
            End If


            pnlCountryErr.Visible = False
            If (Not PageHasErrors) Then
                If (cbCountry.SelectedItem Is Nothing OrElse cbCountry.SelectedIndex = 0) Then
                    pnlCountryErr.Visible = True
                    lblCountryErr.Text = CurrentPageData.GetCustomString("lblCountryErr")
                    cbCountry.Focus()
                    PageHasErrors = True
                End If
            End If


            pnlRegionErr.Visible = False
            pnlCityErr.Visible = False
            pnlZipErr.Visible = False


            txtZip.Text = Regex.Replace(txtZip.Text, "[^\d]", "")
            If (txtZip.Text <> "" AndAlso txtZip.Text.Length > 9) Then
                txtZip.Focus()
                pnlZipErr.Visible = True
                PageHasErrors = True
                lblZipErr.Text = CurrentPageData.GetCustomString("lblZipErr_InvalidZip")
            End If

            If (cbCountry.SelectedItem IsNot Nothing AndAlso
                cbCountry.SelectedItem.Value <> "-1" AndAlso
                clsGeoHelper.CheckCountryTable(cbCountry.SelectedItem.Value)) Then

                If (Not PageHasErrors) Then
                    txtZip.Text = AppUtils.GetZip(txtZip.Text)
                    If (txtZip.Text.Trim() <> "") Then 'AndAlso hdfLocationStatus.Value = "zip"
                        Try

                            Dim dt As DataTable = clsGeoHelper.GetGEOByZip(cbCountry.SelectedItem.Value, txtZip.Text, Me.Session("LAGID"))
                            If (dt.Rows.Count > 0) Then
                                If (Not SelectComboItem(cbRegion, dt.Rows(0)("region1"))) Then
                                    cbRegion.Items.Add(dt.Rows(0)("region1"))
                                    cbRegion.Items(cbRegion.Items.Count - 1).Selected = True
                                End If
                                If (Not SelectComboItem(cbCity, dt.Rows(0)("city"))) Then
                                    cbCity.Items.Add(dt.Rows(0)("city"))
                                    cbCity.Items(cbCity.Items.Count - 1).Selected = True
                                End If
                            Else
                                txtZip.Focus()
                                pnlZipErr.Visible = True
                                PageHasErrors = True
                            End If

                        Catch ex As Exception
                            WebErrorMessageBox(Me, ex, "")

                            txtZip.Focus()
                            pnlZipErr.Visible = True
                            PageHasErrors = True
                        Finally
                        End Try
                    End If
                    lblZipErr.Text = CurrentPageData.GetCustomString(lblZipErr.ID)
                End If


                If (Not PageHasErrors) Then
                    If (cbRegion.SelectedItem Is Nothing OrElse cbRegion.SelectedIndex = -1) Then
                        pnlRegionErr.Visible = True
                        lblRegionErr.Text = CurrentPageData.GetCustomString("lblRegionErr")
                        cbRegion.Focus()
                        PageHasErrors = True
                    End If
                End If


                If (Not PageHasErrors) Then
                    If (cbCity.SelectedItem Is Nothing OrElse cbCity.SelectedIndex = -1) Then
                        pnlCityErr.Visible = True
                        lblCityErr.Text = CurrentPageData.GetCustomString("lblCityErr")
                        cbCity.Focus()
                        PageHasErrors = True
                    End If
                End If
            End If


        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


    Sub SaveProfile(ByVal profileId As Integer)
        Try

            Dim _profilerows As New clsProfileRows(profileId)
            Dim mirrorRow As EUS_ProfilesRow = _profilerows.GetMirrorRow() 'ds.EUS_Profiles(_profilerows.MirrorRowIndex)
            Dim ds As DSMembers = _profilerows.DSMembers


            If (mirrorRow.IsGenderIdNull()) Then
                mirrorRow.GenderId = rblGender.SelectedItem.Value
            End If

            'If (mirrorRow.IsAccountTypeIdNull()) Then
            '    mirrorRow.AccountTypeId = cbAccountType.SelectedItem.Value
            'End If

            If (mirrorRow.IsBirthdayNull()) Then
                mirrorRow.Birthday = ctlDate.DateTime
            End If



            '' Location
            mirrorRow.Country = cbCountry.SelectedItem.Value
            If (cbRegion.SelectedItem IsNot Nothing) Then
                mirrorRow._Region = cbRegion.SelectedItem.Value
            Else
                mirrorRow._Region = ""
            End If

            If (cbCity.SelectedItem IsNot Nothing) Then
                mirrorRow.City = cbCity.SelectedItem.Value
            Else
                mirrorRow.City = ""
            End If


            txtZip.Text = AppUtils.GetZip(txtZip.Text)
            If (txtZip.Text <> "") Then
                mirrorRow.Zip = txtZip.Text
            Else
                mirrorRow.Zip = clsGeoHelper.GetCityCenterPostcode(mirrorRow.Country, mirrorRow._Region, mirrorRow.City)
            End If

            If (Not mirrorRow.IsZipNull()) Then
                ' set textbox zip value
                txtZip.Text = mirrorRow.Zip
            End If




            Dim countryCodeTmp As String = mirrorRow.Country
            Dim PostcodeTmp As String = mirrorRow.Zip
            Dim LAGIDTmp As String = mirrorRow.LAGID

            Try

                Dim dt As DataTable = clsGeoHelper.GetGEOByZip(countryCodeTmp, PostcodeTmp, LAGIDTmp)

                If (dt.Rows.Count > 0) Then

                    mirrorRow.latitude = dt.Rows(0)("latitude")
                    mirrorRow.longitude = dt.Rows(0)("longitude")

                Else

                    dt = clsGeoHelper.GetCountryMinPostcodeDataTable(mirrorRow.Country, mirrorRow._Region, mirrorRow.City, mirrorRow.LAGID)

                    If (dt.Rows.Count > 0) Then
                        If (Not IsDBNull(dt.Rows(0)("MinPostcode"))) Then PostcodeTmp = dt.Rows(0)("MinPostcode")
                        If (Not IsDBNull(dt.Rows(0)("countryCode"))) Then countryCodeTmp = dt.Rows(0)("countryCode")

                        dt = clsGeoHelper.GetGEOByZip(countryCodeTmp, PostcodeTmp, mirrorRow.LAGID)

                        mirrorRow.latitude = dt.Rows(0)("latitude")
                        mirrorRow.longitude = dt.Rows(0)("longitude")
                    End If

                End If

            Catch ex As Exception
                WebErrorSendEmail(ex, "Updating Profile: Failed to retrieve GEO info for specified zip, params (zip:" & mirrorRow.Zip & ")")
            End Try


            'mirrorRow.CityArea = txtCityArea.Text
            'mirrorRow.Address = txtAddress.Text

            SetLocationText()

            'mirrorRow.FirstName = txtFirstName.Text
            'mirrorRow.LastName = txtLastName.Text
            'mirrorRow.Telephone = txtTelephone.Text
            'mirrorRow.eMail = txteMail.Text
            'mirrorRow.Cellular = txtCellular.Text

            mirrorRow.AreYouWillingToTravel = chkTravelOnDate.Checked

            'mirrorRow.AboutMe_Heading = txtYrSlfHeading.Text
            'mirrorRow.AboutMe_DescribeYourself = txtYrSlfDesc.Text
            'mirrorRow.AboutMe_DescribeAnIdealFirstDate = txtYrSlfSeeking.Text
            'mirrorRow.OtherDetails_Occupation = txtOccupation.Text

            If (cbEducation.Items(0).Selected) Then
                mirrorRow.OtherDetails_EducationID = -1
            Else
                mirrorRow.OtherDetails_EducationID = cbEducation.SelectedItem.Value
            End If

            If (cbIncome.Items(0).Selected) Then
                mirrorRow.OtherDetails_AnnualIncomeID = -1
            Else
                mirrorRow.OtherDetails_AnnualIncomeID = cbIncome.SelectedItem.Value
            End If

            'If (cbNetWorth.Items(0).Selected) Then
            '    mirrorRow.OtherDetails_NetWorthID = -1
            'Else
            '    mirrorRow.OtherDetails_NetWorthID = cbNetWorth.SelectedItem.Value
            'End If

            If (cbHeight.Items(0).Selected) Then
                mirrorRow.PersonalInfo_HeightID = -1
            Else
                mirrorRow.PersonalInfo_HeightID = cbHeight.SelectedItem.Value
            End If

            If (cbBodyType.Items(0).Selected) Then
                mirrorRow.PersonalInfo_BodyTypeID = -1
            Else
                mirrorRow.PersonalInfo_BodyTypeID = cbBodyType.SelectedItem.Value
            End If

            If (cbEyeColor.Items(0).Selected) Then
                mirrorRow.PersonalInfo_EyeColorID = -1
            Else
                mirrorRow.PersonalInfo_EyeColorID = cbEyeColor.SelectedItem.Value
            End If

            If (cbHairClr.Items(0).Selected) Then
                mirrorRow.PersonalInfo_HairColorID = -1
            Else
                mirrorRow.PersonalInfo_HairColorID = cbHairClr.SelectedItem.Value
            End If

            If (cbChildren.Items(0).Selected) Then
                mirrorRow.PersonalInfo_ChildrenID = -1
            Else
                mirrorRow.PersonalInfo_ChildrenID = cbChildren.SelectedItem.Value
            End If

            If (cbEthnicity.Items(0).Selected) Then
                mirrorRow.PersonalInfo_EthnicityID = -1
            Else
                mirrorRow.PersonalInfo_EthnicityID = cbEthnicity.SelectedItem.Value
            End If

            If (cbReligion.Items(0).Selected) Then
                mirrorRow.PersonalInfo_ReligionID = -1
            Else
                mirrorRow.PersonalInfo_ReligionID = cbReligion.SelectedItem.Value
            End If

            If (cbSmoking.Items(0).Selected) Then
                mirrorRow.PersonalInfo_SmokingHabitID = -1
            Else
                mirrorRow.PersonalInfo_SmokingHabitID = cbSmoking.SelectedItem.Value
            End If

            If (cbDrinking.Items(0).Selected) Then
                mirrorRow.PersonalInfo_DrinkingHabitID = -1
            Else
                mirrorRow.PersonalInfo_DrinkingHabitID = cbDrinking.SelectedItem.Value
            End If

            If (cbRelationshipStatus.Items(0).Selected) Then
                mirrorRow.LookingFor_RelationshipStatusID = -1
            Else
                mirrorRow.LookingFor_RelationshipStatusID = cbRelationshipStatus.SelectedItem.Value
            End If


            mirrorRow.LookingFor_ToMeetMaleID = chkLookingMale.Checked
            mirrorRow.LookingFor_ToMeetFemaleID = chkLookingFemale.Checked

            ' bind listview of
            Try
                For Each itm As ListViewDataItem In lvTypeOfDate.Items

                    Try

                        Dim hdf As HiddenField = DirectCast(itm.FindControl("hdf"), HiddenField)
                        Dim chk As DevExpress.Web.ASPxEditors.ASPxCheckBox = DirectCast(itm.FindControl("chk"), DevExpress.Web.ASPxEditors.ASPxCheckBox)

                        Dim tmpTypeOfDating As TypeOfDatingEnum = DirectCast(System.Enum.Parse(GetType(TypeOfDatingEnum), hdf.Value), TypeOfDatingEnum)
                        Select Case tmpTypeOfDating
                            Case TypeOfDatingEnum.AdultDating_Casual
                                mirrorRow.LookingFor_TypeOfDating_AdultDating_Casual = chk.Checked

                            Case TypeOfDatingEnum.Friendship
                                mirrorRow.LookingFor_TypeOfDating_Friendship = chk.Checked

                            Case TypeOfDatingEnum.LongTermRelationship
                                mirrorRow.LookingFor_TypeOfDating_LongTermRelationship = chk.Checked

                            Case TypeOfDatingEnum.MarriedDating
                                mirrorRow.LookingFor_TypeOfDating_MarriedDating = chk.Checked

                            Case TypeOfDatingEnum.MutuallyBeneficialArrangements
                                mirrorRow.LookingFor_TypeOfDating_MutuallyBeneficialArrangements = chk.Checked

                            Case TypeOfDatingEnum.ShortTermRelationship
                                mirrorRow.LookingFor_TypeOfDating_ShortTermRelationship = chk.Checked

                        End Select
                    Catch ex As Exception
                        WebErrorMessageBox(Me, ex, "")
                    End Try
                Next

            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try

            'If (mirrorRow.Status <> ProfileStatusEnum.NewProfile AndAlso mirrorRow.Status <> ProfileStatusEnum.None) Then
            '    mirrorRow.Status = ProfileStatusEnum.Updating
            'End If

            mirrorRow.LastUpdateProfileDateTime = DateTime.UtcNow
            mirrorRow.LastUpdateProfileIP = Request.Params("REMOTE_ADDR")
            mirrorRow.LastUpdateProfileGEOInfo = Session("GEO_COUNTRY_CODE")
            mirrorRow.LAGID = Session("LagID")

            Dim masterRow As EUS_ProfilesRow = _profilerows.GetMasterRow()
            If (mirrorRow.ProfileID <> masterRow.ProfileID) Then

                ' copy from mirror to master
                ' copy data that is not read only
                Dim columns As System.Data.DataColumnCollection = ds.EUS_Profiles.Columns
                Dim cnt As Integer = 0
                For cnt = 0 To columns.Count - 1
                    If (columns(cnt).ReadOnly) Then Continue For ' skip profileid column

                    ' skip columns, needs manual approvement
                    If (columns(cnt).ColumnName = "CityArea") Then Continue For
                    If (columns(cnt).ColumnName = "Address") Then Continue For ' skip column
                    If (columns(cnt).ColumnName = "FirstName") Then Continue For ' skip column
                    If (columns(cnt).ColumnName = "LastName") Then Continue For ' skip column
                    If (columns(cnt).ColumnName = "Telephone") Then Continue For ' skip column
                    If (columns(cnt).ColumnName = "eMail") Then Continue For ' skip column
                    If (columns(cnt).ColumnName = "Cellular") Then Continue For ' skip column
                    If (columns(cnt).ColumnName = "AboutMe_Heading") Then Continue For ' skip column
                    If (columns(cnt).ColumnName = "AboutMe_DescribeYourself") Then Continue For ' skip column
                    If (columns(cnt).ColumnName = "AboutMe_DescribeAnIdealFirstDate") Then Continue For ' skip column
                    If (columns(cnt).ColumnName = "OtherDetails_Occupation") Then Continue For ' skip column

                    If (columns(cnt).ColumnName = "IsMaster") Then Continue For ' skip column
                    If (columns(cnt).ColumnName = "MirrorProfileID") Then Continue For ' skip column


                    masterRow(cnt) = mirrorRow(cnt)
                Next

                ' set specific columns data
                masterRow.Status = ProfileStatusEnum.Approved

                'mirrorRow.IsMaster = False
                'masterRow.IsMaster = True

                'mirrorRow.MirrorProfileID = masterRow.ProfileID
                'masterRow.MirrorProfileID = mirrorRow.ProfileID

            End If

            Me.SessionVariables.MemberData.Fill(masterRow)

            DataHelpers.UpdateEUS_Profiles(ds)
            HasAgeIssue = (Me.GetCurrentProfile().Birthday IsNot Nothing AndAlso ProfileHelper.GetCurrentAge(Me.GetCurrentProfile().Birthday) < 18)

            'Try
            '    clsUserNotifications.SendNotificationsToMembers_AboutNewMember(masterRow.ProfileID, False)
            'Catch ex As Exception
            '    WebErrorMessageBox(Me, ex, "")
            'End Try

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Sub SaveProfile_ManuallyApprovingFields(ByVal profileId As Integer)
        Try
            Dim _DataChanged As Boolean

            Dim _profilerows As New clsProfileRows(profileId)
            Dim mirrorRow As EUS_ProfilesRow = _profilerows.GetMirrorRow() 'ds.EUS_Profiles(_profilerows.MirrorRowIndex)
            Dim ds As DSMembers = _profilerows.DSMembers

            If (mirrorRow.CityArea <> txtCityArea.Text) Then _DataChanged = True
            mirrorRow.CityArea = txtCityArea.Text

            If (mirrorRow.Address <> txtAddress.Text) Then _DataChanged = True
            mirrorRow.Address = txtAddress.Text

            If (mirrorRow.FirstName <> txtFirstName.Text) Then _DataChanged = True
            mirrorRow.FirstName = txtFirstName.Text

            If (mirrorRow.LastName <> txtLastName.Text) Then _DataChanged = True
            mirrorRow.LastName = txtLastName.Text

            If (mirrorRow.Telephone <> txtTelephone.Text) Then _DataChanged = True
            mirrorRow.Telephone = txtTelephone.Text

            If (mirrorRow.eMail <> txteMail.Text) Then _DataChanged = True
            mirrorRow.eMail = txteMail.Text

            If (mirrorRow.Cellular <> txtCellular.Text) Then _DataChanged = True
            mirrorRow.Cellular = txtCellular.Text

            If (mirrorRow.AboutMe_Heading <> txtYrSlfHeading.Text) Then _DataChanged = True
            mirrorRow.AboutMe_Heading = txtYrSlfHeading.Text
            If (Not String.IsNullOrEmpty(mirrorRow.AboutMe_Heading)) Then
                If (mirrorRow.AboutMe_Heading.Length > 199) Then
                    mirrorRow.AboutMe_Heading = mirrorRow.AboutMe_Heading.Remove(199)
                End If
            End If

            If (mirrorRow.AboutMe_DescribeYourself <> txtYrSlfDesc.Text) Then _DataChanged = True
            mirrorRow.AboutMe_DescribeYourself = txtYrSlfDesc.Text

            If (mirrorRow.AboutMe_DescribeAnIdealFirstDate <> txtYrSlfSeeking.Text) Then _DataChanged = True
            mirrorRow.AboutMe_DescribeAnIdealFirstDate = txtYrSlfSeeking.Text

            If (mirrorRow.OtherDetails_Occupation <> txtOccupation.Text) Then _DataChanged = True
            mirrorRow.OtherDetails_Occupation = txtOccupation.Text
            If (Not String.IsNullOrEmpty(mirrorRow.OtherDetails_Occupation)) Then
                If (mirrorRow.OtherDetails_Occupation.Length > 199) Then
                    mirrorRow.OtherDetails_Occupation = mirrorRow.OtherDetails_Occupation.Remove(199)
                End If
            End If

            If (_DataChanged) Then

                If (mirrorRow.Status <> ProfileStatusEnum.NewProfile AndAlso mirrorRow.Status <> ProfileStatusEnum.None) Then
                    mirrorRow.Status = ProfileStatusEnum.Updating
                End If

                DataHelpers.UpdateEUS_Profiles(ds)
            End If


            'existing profile auto approve
            Dim isAutoApproveOn As Boolean
            Try
                Dim dt As DataTable = DataHelpers.GetDataTable("   select ISNULL(ConfigValue,0) as ConfigValue from SYS_Config where ConfigName='auto_approve_updating_profiles' ")
                If (dt.Rows.Count > 0 AndAlso dt.Rows(0)("ConfigValue") = "1") Then
                    AdminActions.ApproveProfile(mirrorRow.LoginName, True)
                    isAutoApproveOn = True
                End If
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "AutoApprovePhoto failed.")
            End Try


            If (_DataChanged) Then
                SendNotificationEmailToSupport(mirrorRow, isAutoApproveOn)
            End If



        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Sub LoadProfile(ByVal profileId As Integer)
        Try

            Dim _profilerows As New clsProfileRows(profileId)
            Dim currentRow As EUS_ProfilesRow = _profilerows.GetMirrorRow()
            Dim ds As DSMembers = _profilerows.DSMembers

            If (Not currentRow.IsGenderIdNull()) Then
                liGenderContainer.Visible = False
            End If

            'If (Not currentRow.IsAccountTypeIdNull()) Then
            '    liWhoIsContainer.Visible = False
            'End If

            If (Not currentRow.IsBirthdayNull()) Then
                liBirthdayContainer.Visible = False
            End If

            If (Not currentRow.IsFirstNameNull()) Then txtFirstName.Text = currentRow.FirstName
            If (Not currentRow.IsLastNameNull()) Then txtLastName.Text = currentRow.LastName

            If (Not currentRow.IsTelephoneNull()) Then txtTelephone.Text = currentRow.Telephone
            If (Not currentRow.IseMailNull()) Then txteMail.Text = currentRow.eMail
            If (Not currentRow.IsCellularNull()) Then txtCellular.Text = currentRow.Cellular

            If (Not currentRow.IsAreYouWillingToTravelNull()) Then chkTravelOnDate.Checked = currentRow.AreYouWillingToTravel

            If (Not currentRow.IsAboutMe_HeadingNull()) Then txtYrSlfHeading.Text = currentRow.AboutMe_Heading
            If (Not currentRow.IsAboutMe_DescribeYourselfNull()) Then txtYrSlfDesc.Text = currentRow.AboutMe_DescribeYourself
            If (Not currentRow.IsAboutMe_DescribeAnIdealFirstDateNull()) Then txtYrSlfSeeking.Text = currentRow.AboutMe_DescribeAnIdealFirstDate

            If (Not currentRow.IsOtherDetails_EducationIDNull()) Then SelectComboItem(cbEducation, currentRow.OtherDetails_EducationID)
            If (Not currentRow.IsOtherDetails_AnnualIncomeIDNull()) Then SelectComboItem(cbIncome, currentRow.OtherDetails_AnnualIncomeID)
            'If (Not currentRow.IsOtherDetails_NetWorthIDNull()) Then SelectComboItem(cbNetWorth, currentRow.OtherDetails_NetWorthID)
            If (Not currentRow.IsOtherDetails_OccupationNull()) Then txtOccupation.Text = currentRow.OtherDetails_Occupation

            If (Not currentRow.IsPersonalInfo_HeightIDNull()) Then SelectComboItem(cbHeight, currentRow.PersonalInfo_HeightID)
            If (Not currentRow.IsPersonalInfo_BodyTypeIDNull()) Then SelectComboItem(cbBodyType, currentRow.PersonalInfo_BodyTypeID)
            If (Not currentRow.IsPersonalInfo_EyeColorIDNull()) Then SelectComboItem(cbEyeColor, currentRow.PersonalInfo_EyeColorID)
            If (Not currentRow.IsPersonalInfo_HairColorIDNull()) Then SelectComboItem(cbHairClr, currentRow.PersonalInfo_HairColorID)
            If (Not currentRow.IsPersonalInfo_ChildrenIDNull()) Then SelectComboItem(cbChildren, currentRow.PersonalInfo_ChildrenID)
            If (Not currentRow.IsPersonalInfo_EthnicityIDNull()) Then SelectComboItem(cbEthnicity, currentRow.PersonalInfo_EthnicityID)
            If (Not currentRow.IsPersonalInfo_ReligionIDNull()) Then SelectComboItem(cbReligion, currentRow.PersonalInfo_ReligionID)
            If (Not currentRow.IsPersonalInfo_SmokingHabitIDNull()) Then SelectComboItem(cbSmoking, currentRow.PersonalInfo_SmokingHabitID)
            If (Not currentRow.IsPersonalInfo_DrinkingHabitIDNull()) Then SelectComboItem(cbDrinking, currentRow.PersonalInfo_DrinkingHabitID)

            If (Not currentRow.IsLookingFor_RelationshipStatusIDNull()) Then SelectComboItem(cbRelationshipStatus, currentRow.LookingFor_RelationshipStatusID)


            '' Location
            If (Not currentRow.IsCountryNull()) Then
                SelectComboItem(cbCountry, currentRow.Country)
            End If

            If (Not currentRow.Is_RegionNull()) Then
                FillRegionCombo(currentRow.Country)
                cbCountry_SelectedIndexChanged(cbCountry, New System.EventArgs())
                If (Not SelectComboItem(cbRegion, currentRow._Region)) Then
                    cbRegion.Items.Add(currentRow._Region)
                    cbRegion.Items(cbRegion.Items.Count - 1).Selected = True
                End If


                If (Not currentRow.IsCityNull()) Then
                    cbRegion_SelectedIndexChanged(cbRegion, New System.EventArgs())
                    FillCityCombo(cbRegion.SelectedItem.Value)
                End If
            End If

            If (Not currentRow.IsCityNull()) Then
                If (Not currentRow.Is_RegionNull()) Then
                    cbRegion.Value = currentRow._Region
                    cbRegion_SelectedIndexChanged(cbRegion, New System.EventArgs())
                    FillCityCombo(currentRow._Region)
                End If

                txtCity.Text = currentRow.City
                If (Not SelectComboItem(cbCity, currentRow.City)) Then
                    cbCity.Items.Add(currentRow.City)
                    cbCity.Items(cbCity.Items.Count - 1).Selected = True
                End If
            End If


            If (hdfLocationStatus.Value = "") Then
                If (Not currentRow.IsZipNull()) Then
                    txtZip.Text = currentRow.Zip
                    hdfLocationStatus.Value = "zip"

                    liLocationsZip.Attributes.CssStyle.Remove("display")
                    liLocationsRegion.Attributes.CssStyle.Add("display", "none")
                    liLocationsCity.Attributes.CssStyle.Add("display", "none")
                    msg_SelectRegion.Attributes.CssStyle.Remove("display")

                Else
                    hdfLocationStatus.Value = "region"

                    liLocationsZip.Attributes.CssStyle.Add("display", "none")
                    liLocationsRegion.Attributes.CssStyle.Remove("display")
                    liLocationsCity.Attributes.CssStyle.Remove("display")
                    msg_SelectRegion.Attributes.CssStyle.Add("display", "none")
                End If
            End If

            If (Not currentRow.IsCityAreaNull()) Then txtCityArea.Text = currentRow.CityArea
            If (Not currentRow.IsAddressNull()) Then txtAddress.Text = currentRow.Address

            SetLocationText()
            '' Location end


            'If (Not mirrorRow.IsAccountTypeIdNull()) Then
            '    chkLookingFemale.Checked =  ProfileHelper.IsMale(mirrorRow.GenderId)
            '    chkLookingMale.Checked =  ProfileHelper.IsFemale(mirrorRow.GenderId)
            'End If

            If (Not currentRow.IsLookingFor_ToMeetMaleIDNull()) Then chkLookingMale.Checked = currentRow.LookingFor_ToMeetMaleID
            If (Not currentRow.IsLookingFor_ToMeetFemaleIDNull()) Then chkLookingFemale.Checked = currentRow.LookingFor_ToMeetFemaleID


            If ProfileHelper.IsMale(currentRow.GenderId) Then
                liAnnualIncome.Visible = True

            ElseIf ProfileHelper.IsFemale(currentRow.GenderId) Then
                liAnnualIncome.Visible = False

            End If


            Try

                Using _dt As New DataTable


                    _dt.Columns.Add("TypeOfDatingId")
                    _dt.Columns.Add("TypeOfDateChecked")
                    _dt.Columns.Add("TypeOfDateText")

                    Dim i As Integer = 0
                    For i = 0 To (Lists.gDSLists.EUS_LISTS_TypeOfDating.Rows.Count - 1)

                        'While i < Lists.gDSLists.EUS_LISTS_TypeOfDating.Rows.Count
                        Try
                            Dim dr As DataRow = _dt.NewRow()

                            dr("TypeOfDateText") = Lists.gDSLists.EUS_LISTS_TypeOfDating.Rows(i)(GetLag())

                            Select Case Lists.gDSLists.EUS_LISTS_TypeOfDating.Rows(i)("TypeOfDatingId")
                                Case TypeOfDatingEnum.AdultDating_Casual
                                    dr("TypeOfDatingId") = TypeOfDatingEnum.AdultDating_Casual.ToString()
                                    If (currentRow.IsLookingFor_TypeOfDating_AdultDating_CasualNull()) Then
                                        dr("TypeOfDateChecked") = False
                                    Else
                                        dr("TypeOfDateChecked") = currentRow.LookingFor_TypeOfDating_AdultDating_Casual
                                    End If


                                Case TypeOfDatingEnum.Friendship
                                    dr("TypeOfDatingId") = TypeOfDatingEnum.Friendship.ToString()
                                    If (currentRow.IsLookingFor_TypeOfDating_FriendshipNull()) Then
                                        dr("TypeOfDateChecked") = False
                                    Else
                                        dr("TypeOfDateChecked") = currentRow.LookingFor_TypeOfDating_Friendship
                                    End If


                                Case TypeOfDatingEnum.LongTermRelationship
                                    dr("TypeOfDatingId") = TypeOfDatingEnum.LongTermRelationship.ToString()
                                    If (currentRow.IsLookingFor_TypeOfDating_LongTermRelationshipNull()) Then
                                        dr("TypeOfDateChecked") = False
                                    Else
                                        dr("TypeOfDateChecked") = currentRow.LookingFor_TypeOfDating_LongTermRelationship
                                    End If


                                Case TypeOfDatingEnum.MarriedDating
                                    dr("TypeOfDatingId") = TypeOfDatingEnum.MarriedDating.ToString()
                                    If (currentRow.IsLookingFor_TypeOfDating_MarriedDatingNull()) Then
                                        dr("TypeOfDateChecked") = False
                                    Else
                                        dr("TypeOfDateChecked") = currentRow.LookingFor_TypeOfDating_MarriedDating
                                    End If


                                Case TypeOfDatingEnum.MutuallyBeneficialArrangements
                                    dr("TypeOfDatingId") = TypeOfDatingEnum.MutuallyBeneficialArrangements.ToString()
                                    If (currentRow.IsLookingFor_TypeOfDating_MutuallyBeneficialArrangementsNull()) Then
                                        dr("TypeOfDateChecked") = False
                                    Else
                                        dr("TypeOfDateChecked") = currentRow.LookingFor_TypeOfDating_MutuallyBeneficialArrangements
                                    End If


                                Case TypeOfDatingEnum.ShortTermRelationship
                                    dr("TypeOfDatingId") = TypeOfDatingEnum.ShortTermRelationship.ToString()
                                    If (currentRow.IsLookingFor_TypeOfDating_ShortTermRelationshipNull()) Then
                                        dr("TypeOfDateChecked") = False
                                    Else
                                        dr("TypeOfDateChecked") = currentRow.LookingFor_TypeOfDating_ShortTermRelationship
                                    End If

                            End Select

                            _dt.Rows.Add(dr)
                        Catch ex As Exception
                            WebErrorMessageBox(Me, ex, "")
                        End Try

                        'i = i + 1
                        'End While
                    Next

                    lvTypeOfDate.DataSource = _dt
                    lvTypeOfDate.DataBind()
                End Using
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try


            DataHelpers.UpdateEUS_Profiles(ds)

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Protected Sub cbpnlZip_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cbpnlZip.Callback
        If (e.Parameter.StartsWith("country_")) Then
            FillRegionCombo(e.Parameter.Replace("country_", ""))

            If (cbCountry.SelectedItem Is Nothing OrElse cbCountry.SelectedItem.Value <> "GR") Then
                txtZip.Text = ""
            End If

        ElseIf (e.Parameter.StartsWith("region_")) Then
            FillCityCombo(e.Parameter.Replace("region_", ""))

        ElseIf (e.Parameter.StartsWith("city_")) Then
            Dim city As String = e.Parameter.Replace("city_", "")
            city = city.Remove(city.IndexOf("_region_"))

            Dim region As String = e.Parameter.Substring(e.Parameter.IndexOf("_region_") + Len("_region_"))

            FillZipTextBox(city, region)
        End If
        SetLocationText()
    End Sub


    Private Sub SetLocationText()
        lblLocation.Text = ""

        Dim _locationStrCity = ""
        Dim _locationStrRegion = ""
        Dim _locationStrCountry = ""
        If (cbCountry.SelectedItem IsNot Nothing AndAlso cbCountry.SelectedIndex > 0) Then
            _locationStrCountry = cbCountry.SelectedItem.Text
        End If
        If (cbRegion.SelectedItem IsNot Nothing) Then
            _locationStrRegion = cbRegion.SelectedItem.Text
        End If
        If (cbCity.SelectedItem IsNot Nothing) Then
            _locationStrCity = cbCity.SelectedItem.Text
        End If

        If (_locationStrCity <> "") Then
            lblLocation.Text = _locationStrCity
        End If
        If (_locationStrRegion <> "" AndAlso _locationStrCity <> "") Then
            lblLocation.Text = lblLocation.Text & ", "
        End If
        If (_locationStrRegion <> "") Then
            lblLocation.Text = lblLocation.Text & _locationStrRegion
        End If
        If (_locationStrRegion <> "" AndAlso _locationStrCountry <> "") Then
            lblLocation.Text = lblLocation.Text & ", "
        End If
        If (_locationStrCountry <> "") Then
            lblLocation.Text = lblLocation.Text & _locationStrCountry
        End If
        If (lblLocation.Text.Trim() = "") Then liLocationString.Visible = False
    End Sub


    Protected Sub cbRegion_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cbRegion.Callback
        FillRegionCombo(e.Parameter)
    End Sub
    Protected Sub cbCity_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cbCity.Callback
        FillCityCombo(e.Parameter)
    End Sub

    Protected Sub FillRegionCombo(ByVal country As String)
        If String.IsNullOrEmpty(country) Then
            Return
        End If

        Try
            ShowLocationControls()
            'cbRegion.DataBind()

            If (cbRegion.Items.Count = 0) Then
                cbRegion.Text = CurrentPageData.GetCustomString("msg_NotFound")
                cbCity.Text = CurrentPageData.GetCustomString("msg_NotFound")
            Else
                cbRegion.Text = CurrentPageData.GetCustomString(msg_RegionText.ID)
                cbCity.Text = CurrentPageData.GetCustomString("msg_NotFound")
            End If

            'Return

            'cbRegion.Items.Clear()
            'cbCity.Items.Clear()

            ''ShowRegion(False)
            ''ShowCity(False)

            'If (country = "GR") Then
            '    Dim dt As DataTable = clsGeoHelper.GetCountryRegions(country, Me.Session("LAGID"))
            '    cbRegion.DataSource = dt
            '    cbRegion.TextField = "region1"
            '    cbRegion.ValueField = "region1"
            '    cbRegion.DataBind()

            '    'ShowRegion(True)
            '    'ShowCity(True)
            'End If

            'If (cbRegion.Items.Count = 0) Then
            '    cbRegion.DataSource = Nothing
            '    cbRegion.DataBind()

            '    cbCity.DataSource = Nothing
            '    cbCity.DataBind()
            '    'ShowRegion(False)
            '    'ShowCity(False)

            '    cbRegion.Items.Clear()
            '    cbCity.Items.Clear()

            '    cbRegion.Items.Insert(0, New ListEditItem("Not found"))
            '    cbCity.Items.Insert(0, New ListEditItem("Not found"))

            '    'cbRegion.Enabled = False
            '    'cbCity.Enabled = False
            'End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Protected Sub FillCityCombo(ByVal region1 As String)
        If String.IsNullOrEmpty(region1) Then
            Return
        End If

        Try
            ShowLocationControls()
            cbCity.Text = CurrentPageData.GetCustomString(msg_CityText.ID)
            'cbCity.DataBind()

            'Return

            'Dim country As String = cbCountry.SelectedItem.Value
            'cbCity.Items.Clear()
            ''liLocationsCity.Attributes.CssStyle.Add("display", "none")

            'Dim dt As DataTable = clsGeoHelper.GetCountryRegionCities(country, region1, Me.Session("LAGID"))
            'cbCity.DataSource = dt
            'cbCity.TextField = "city"
            'cbCity.ValueField = "city"
            'cbCity.DataBind()

            ''ShowCity(True)

            'If (cbCity.Items.Count = 0) Then
            '    cbCity.Items.Insert(0, New ListEditItem("Not found"))
            '    cbCity.Items.Clear()
            '    'cbCity.Enabled = False
            'End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Private Sub FillZipTextBox(city As String, Optional region As String = "")
        If String.IsNullOrEmpty(city) Then
            Return
        End If

        Try
            ShowLocationControls()

            ' if there is a value in zip, skip
            'If (txtZip.Text.Trim() <> "") Then
            '    Return
            'End If


            Dim country As String = cbCountry.SelectedItem.Value

            If (String.IsNullOrEmpty(region)) Then
                region = cbRegion.SelectedItem.Value
            End If
            ' city = cbCity.SelectedItem.Value
            'If (country = "GR") Then
            txtZip.Text = clsGeoHelper.GetCityCenterPostcode(country, region, city)
            'End If
            'ShowRegion(True)
            'ShowCity(True)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub



    Private Sub cbCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbCountry.SelectedIndexChanged
        If (cbCountry.SelectedItem IsNot Nothing) Then
            cbRegion.DataSourceID = ""
            cbRegion.DataSource = clsGeoHelper.GetCountryRegions(cbCountry.SelectedItem.Value, Session("LAGID"))
            cbRegion.TextField = "region1"
            cbRegion.DataBind()
        End If
    End Sub

    Private Sub cbRegion_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbRegion.SelectedIndexChanged
        If (cbRegion.SelectedItem IsNot Nothing) Then
            cbCity.DataSourceID = ""
            cbCity.DataSource = clsGeoHelper.GetCountryRegionCities(cbCountry.SelectedItem.Value, cbRegion.SelectedItem.Value, Session("LAGID"))
            cbCity.TextField = "city"
            cbCity.DataBind()
        End If
    End Sub

   

    Private Sub ShowLocationControls()

        liLocationsRegion.Attributes.CssStyle.Remove("display")
        liLocationsCity.Attributes.CssStyle.Remove("display")
        liLocationsZip.Attributes.CssStyle.Remove("display")
        If (hdfLocationStatus.Value = "region") Then
            msg_SelectRegion.Attributes.CssStyle.Add("display", "none")
        Else
            msg_SelectRegion.Attributes.CssStyle.Remove("display")
        End If
    End Sub


    Private Sub ShowRegion(show As Boolean)

        If (show) Then
            If (hdfLocationStatus.Value <> "zip") Then
                liLocationsRegion.Attributes.CssStyle.Remove("display")
            End If
        Else
            liLocationsRegion.Attributes.CssStyle.Add("display", "none")
        End If

    End Sub

    Private Sub ShowCity(show As Boolean)

        If (show) Then
            If (hdfLocationStatus.Value <> "zip") Then
                liLocationsCity.Attributes.CssStyle.Remove("display")
            End If
        Else
            liLocationsCity.Attributes.CssStyle.Add("display", "none")
        End If

    End Sub

    Private Sub ShowZip(show As Boolean)

        If (show) Then
            If (hdfLocationStatus.Value = "zip") Then
                liLocationsZip.Attributes.CssStyle.Remove("display")
            End If
        Else
            liLocationsZip.Attributes.CssStyle.Add("display", "none")
        End If

    End Sub

    Protected Sub sdsRegion_Selecting(sender As Object, e As System.Web.UI.WebControls.SqlDataSourceSelectingEventArgs) Handles sdsRegion.Selecting
        For Each prm As SqlClient.SqlParameter In e.Command.Parameters

            If (prm.ParameterName = "@language") Then
                If (Session("LAGID") = "GR") Then
                    prm.Value = "EL"
                Else
                    prm.Value = "EN"
                End If
            End If

        Next
    End Sub

    Protected Sub sdsCity_Selecting(sender As Object, e As System.Web.UI.WebControls.SqlDataSourceSelectingEventArgs) Handles sdsCity.Selecting
        For Each prm As SqlClient.SqlParameter In e.Command.Parameters

            If (prm.ParameterName = "@language") Then
                If (Session("LAGID") = "GR") Then
                    prm.Value = "EL"
                Else
                    prm.Value = "EN"
                End If
            End If

        Next
    End Sub





    Sub SendNotificationEmailToSupport(mirrorRow As EUS_ProfilesRow, isAutoApproveOn As Boolean)
        Try

            Dim toEmail As String = ConfigurationManager.AppSettings("gToEmail")
            Dim Content As String = globalStrings.GetCustomString("EmailSendToSupport_MemberUpdating", "US")

            If (isAutoApproveOn) Then
                Content = Content.Replace("###YESNO###", "YES")
            Else
                Content = Content.Replace("###YESNO###", "NO")
            End If

            Content = Content.Replace("###LOGINNAME###", mirrorRow.LoginName)
            Content = Content.Replace("###EMAIL###", mirrorRow.eMail)

            Dim approveUrl As String = ConfigurationManager.AppSettings("gApproveProfileURL")
            Dim rejectUrl As String
            approveUrl = String.Format(approveUrl, mirrorRow.LoginName)
            rejectUrl = approveUrl & "&reject=1"

            'If (isAutoApproveOn) Then
            '    Content = Content.Replace("###APPROVEPROFILEURL###", "")
            '    Content = Content.Replace("###REJECTPROFILEURL###", "")
            'Else
            Content = Content.Replace("###APPROVEPROFILEURL###", approveUrl)
            Content = Content.Replace("###REJECTPROFILEURL###", rejectUrl)
            'End If

            Try
                If (Not mirrorRow.IslongitudeNull()) Then
                    Content = Content.Replace("###LONGITUDE###", mirrorRow.longitude)
                Else
                    Content = Content.Replace("###LONGITUDE###", "[Longitude not set]")
                End If
            Catch ex As Exception
            End Try

            Try
                If (Not mirrorRow.IslatitudeNull()) Then
                    Content = Content.Replace("###LATITUDE###", mirrorRow.latitude)
                Else
                    Content = Content.Replace("###LATITUDE###", "[Latitude not set]")
                End If
            Catch ex As Exception
            End Try

            Try

                'Dim siteUrl As String = ConfigurationManager.AppSettings("gSiteURL")
                Dim photoUrl As String = ""
                Dim drDefaultPhoto As DSMembers.EUS_CustomerPhotosRow = DataHelpers.GetProfilesDefaultPhoto(Me.MasterProfileId)
                If (drDefaultPhoto IsNot Nothing) Then
                    photoUrl = ProfileHelper.GetProfileImageURL(drDefaultPhoto.CustomerID, drDefaultPhoto.FileName, mirrorRow.GenderId, True, Me.IsHTTPS)
                Else
                    photoUrl = ProfileHelper.GetDefaultImageURL(mirrorRow.GenderId)
                    photoUrl = "http://" & ConfigurationManager.AppSettings("gSiteName") & IIf(photoUrl.StartsWith("/"), photoUrl, "/" & photoUrl)
                End If
                'photoUrl = (siteUrl & photoUrl)

                Content = Content.Replace("###FILEPATH###", photoUrl)
                Content = Content.Replace("blank###DEFAULTPHOTOURL###", photoUrl) ' cms editor issue, adds word blank
                Content = Content.Replace("###DEFAULTPHOTOURL###", photoUrl)
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "default photo")
            End Try



            Content = Content.Replace("###ABOUTME_HEADING###", mirrorRow.AboutMe_Heading)
            Content = Content.Replace("###ABOUTME_DESCRIBEYOURSELF###", mirrorRow.AboutMe_DescribeYourself)
            Content = Content.Replace("###ABOUTME_DESCRIBEANIDEALFIRSTDATE###", mirrorRow.AboutMe_DescribeAnIdealFirstDate)
            Content = Content.Replace("###OTHERDETAILS_OCCUPATION###", mirrorRow.OtherDetails_Occupation)

            Try
                Content = Content.Replace("###BIRTHDATE###", mirrorRow.Birthday)
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###AGE###", ProfileHelper.GetCurrentAge(mirrorRow.Birthday))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###GENDER###", ProfileHelper.GetGenderString(mirrorRow.GenderId, "US"))
                Content = Content.Replace("###SEX###", ProfileHelper.GetGenderString(mirrorRow.GenderId, "US"))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###COUNTRY###", ProfileHelper.GetCountryName(mirrorRow.Country))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###STATEREGION###", mirrorRow._Region)
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###CITY###", mirrorRow.City)
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###ZIP###", mirrorRow.Zip)
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###DATETOREGISTER###", mirrorRow.DateTimeToRegister)
            Catch ex As Exception
            End Try


            Try
                Content = Content.Replace("###PROFILEAPPROVEDYESNO###", IIf(mirrorRow.Status = 4, "YES", "NO"))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###APPROVEDPHOTOSYESNO###", "NO")
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###AUTOAPPROVEDPHOTOYESNO###", "NO")
            Catch ex As Exception
            End Try


            Try
                Content = Content.Replace("###IP###", Dating.Server.Core.DLL.clsHTMLHelper.CreateIPLookupLinks(Session("IP")))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###GEOIP###", Session("GEO_COUNTRY_CODE"))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###AGENT###", Request.Params("HTTP_USER_AGENT"))
            Catch ex As Exception
            End Try
            Try
                'Dim _uri As Uri = Nothing

                'Try
                '    _uri = New Uri(Request.Params("HTTP_REFERER"))
                'Catch ex As Exception
                'End Try
                'If (_uri IsNot Nothing) Then
                '    Content = Content.Replace("###REFERRER###", _uri.Host & ":" & _uri.Port)
                'Else
                Content = Content.Replace("###REFERRER###", Dating.Server.Core.DLL.clsHTMLHelper.CreateURLLink(IIf(Session("Referrer") IsNot Nothing, Session("Referrer"), "")))
                'End If
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###CUSTOMREFERRER###", Session("CustomReferrer"))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###CUSTOMERID###", mirrorRow.ProfileID)
            Catch ex As Exception
            End Try
            Try
                Dim link As String = ConfigurationManager.AppSettings("gSiteURL") & _
                    "?logon_customer=" & mirrorRow.MirrorProfileID & "_" & mirrorRow.ProfileID
                Content = Content.Replace("###LOGONCUSTOMER###", link)
            Catch ex As Exception
            End Try


            Try
                Dim LandingPage As String = ""
                If (Not mirrorRow.IsLandingPageNull()) Then
                    LandingPage = mirrorRow.LandingPage
                    If (LandingPage Is Nothing) Then LandingPage = ""
                End If
                Content = Content.Replace("###LANDINGPAGE###", Dating.Server.Core.DLL.clsHTMLHelper.CreateURLLink(LandingPage))
            Catch ex As Exception
            End Try

            clsMyMail.SendMail(ConfigurationManager.AppSettings("gToEmail"), "User updated profile information on GOOMENA.", Content, True)

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


    'Sub SendNotificationEmailToSupport_old(mirrorRow As EUS_ProfilesRow, isAutoApproveOn As Boolean)
    '    Try

    '        Dim toEmail As String = ConfigurationManager.AppSettings("gToEmail")
    '        Dim Content As String = globalStrings.GetCustomString("EmailSendToSupport_MemberUpdating", "US")

    '        If (isAutoApproveOn) Then
    '            Content = Content.Replace("###YESNO###", "YES")
    '        Else
    '            Content = Content.Replace("###YESNO###", "NO")
    '        End If

    '        Content = Content.Replace("###LOGINNAME###", mirrorRow.LoginName)
    '        Content = Content.Replace("###EMAIL###", mirrorRow.eMail)
    '        Content = Content.Replace("###GENDER###", ProfileHelper.GetGenderString(mirrorRow.GenderId, "US"))

    '        Try

    '            Dim siteUrl As String = ConfigurationManager.AppSettings("gSiteURL")
    '            Dim photoUrl As String = ""
    '            Dim drDefaultPhoto As DSMembers.EUS_CustomerPhotosRow = DataHelpers.GetProfilesDefaultPhoto(Me.MasterProfileId)
    '            If (drDefaultPhoto IsNot Nothing) Then
    '                photoUrl = ProfileHelper.GetProfileImageURL(drDefaultPhoto.CustomerID, drDefaultPhoto.FileName, mirrorRow.GenderId, True)
    '            Else
    '                photoUrl = ProfileHelper.GetDefaultImageURL(mirrorRow.GenderId)
    '            End If
    '            photoUrl = (siteUrl & photoUrl)

    '            Content = Content.Replace("###FILEPATH###", photoUrl)
    '            Content = Content.Replace("blank###DEFAULTPHOTOURL###", photoUrl) ' cms editor issue, adds word blank
    '            Content = Content.Replace("###DEFAULTPHOTOURL###", photoUrl)
    '        Catch ex As Exception
    '            WebErrorMessageBox(Me, ex, "default photo")
    '        End Try

    '        Content = Content.Replace("###ABOUTME_HEADING###", mirrorRow.AboutMe_Heading)
    '        Content = Content.Replace("###ABOUTME_DESCRIBEYOURSELF###", mirrorRow.AboutMe_DescribeYourself)
    '        Content = Content.Replace("###ABOUTME_DESCRIBEANIDEALFIRSTDATE###", mirrorRow.AboutMe_DescribeAnIdealFirstDate)
    '        Content = Content.Replace("###OTHERDETAILS_OCCUPATION###", mirrorRow.OtherDetails_Occupation)




    '        Dim approveUrl As String = ConfigurationManager.AppSettings("gApproveProfileURL")
    '        Dim rejectUrl As String
    '        approveUrl = String.Format(approveUrl, mirrorRow.LoginName)
    '        rejectUrl = approveUrl & "&reject=1"
    '        If (isAutoApproveOn) Then
    '            Content = Content.Replace("###APPROVEPROFILEURL###", "")
    '            Content = Content.Replace("###REJECTPROFILEURL###", "")
    '        Else
    '            Content = Content.Replace("###APPROVEPROFILEURL###", approveUrl)
    '            Content = Content.Replace("###REJECTPROFILEURL###", rejectUrl)
    '        End If


    '        clsMyMail.SendMail(toEmail, "User updated profile information on GOOMENA.", Content, True)

    '    Catch ex As Exception
    '        WebErrorMessageBox(Me, ex, "")
    '    End Try
    'End Sub


End Class

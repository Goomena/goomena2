﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UCPrice.ascx.vb" Inherits="Dating.Referrals.Server.Site.Web.Price" %>

<asp:MultiView ID="mv" runat="server" ActiveViewIndex="0">

    <asp:View ID="vwPublic" runat="server">
<div class="pricecol" style="background-image: url('../Images2/pricing/for-public.png'); background-repeat: no-repeat;width:251px;height:338px;" 
    align="center">
    <div class="pricecol-credits"><%= MyBase.Credits%><div class="pricecol-credits-text">Credits</div></div>
    <div class="pricecol-price<%= MyBase.SmallClass%>"><div class="pricecol-price-text"><%= MyBase.MoneySymb%><span style="font-size:14px;">&nbsp;</span><%= MyBase.Price%></div></div>
    <div class="pricecol-expire<%= MyBase.SmallClass%>"><div class="pricecol-expire-text">Expire after <%= MyBase.Duration%> Days</div></div>
&nbsp;
    <asp:ImageButton ID="imgCreditsP" runat="server" align="center"
        ImageUrl="~/Images2/pricing/order-button.png" CssClass="ordbut" />
</div>
    </asp:View>


    <asp:View ID="vwPublicRecom" runat="server">
<div class="pricecol" style="background-image: url('../Images2/pricing/for-public-recommend.png'); background-repeat: no-repeat;width:287px;height:338px;" 
    align="center">
    <div class="pricecol-credits"><%= MyBase.Credits%><div class="pricecol-credits-text">Credits</div></div>
    <div class="pricecol-price<%= MyBase.SmallClass%>"><div class="pricecol-price-text"><%= MyBase.MoneySymb%><span style="font-size:14px;">&nbsp;</span><%= MyBase.Price%></div></div>
    <div class="pricecol-expire<%= MyBase.SmallClass%>"><div class="pricecol-expire-text">Expire after <%= MyBase.Duration%> Days</div></div>
&nbsp;
    <asp:ImageButton ID="imgCreditsPR" runat="server" align="center"
        ImageUrl="~/Images2/pricing/order-button.png" CssClass="ordbut"  />
</div>
    </asp:View>


    <asp:View ID="vwMember" runat="server">
<div class="pricecol" style="background-image: url('../Images2/pricing/for-member.png'); background-repeat: no-repeat;width:195px;height:263px;" 
    align="center">
    <div class="pricecol-credits"><%= MyBase.Credits%><div class="pricecol-credits-text">Credits</div></div>
    <div class="pricecol-price<%= MyBase.SmallClass%>"><div class="pricecol-price-text"><%= MyBase.MoneySymb%><span style="font-size:14px;">&nbsp;</span><%= MyBase.Price%></div></div>
    <div class="pricecol-expire<%= MyBase.SmallClass%>"><div class="pricecol-expire-text">Expire after <%= MyBase.Duration%> Days</div></div>
&nbsp;
    <asp:ImageButton ID="imgCreditsM" runat="server" align="center"
        ImageUrl="~/Images2/pricing/order-button.png" CssClass="ordbut" />
</div>
    </asp:View>


    <asp:View ID="vwMemberRecom" runat="server">
<div class="pricecol" style="background-image: url('../Images2/pricing/for-member-recommend.png'); background-repeat: no-repeat;width:224px;height:263px;" 
    align="center">
    <div class="pricecol-credits"><%= MyBase.Credits%><div class="pricecol-credits-text">Credits</div></div>
    <div class="pricecol-price<%= MyBase.SmallClass%>"><div class="pricecol-price-text"><%= MyBase.MoneySymb%><span style="font-size:14px;">&nbsp;</span><%= MyBase.Price%></div></div>
    <div class="pricecol-expire<%= MyBase.SmallClass%>"><div class="pricecol-expire-text">Expire after <%= MyBase.Duration%> Days</div></div>
&nbsp;
    <asp:ImageButton ID="imgCreditsMR" runat="server" align="center"
        ImageUrl="~/Images2/pricing/order-button.png" CssClass="ordbut"  />
</div>
    </asp:View>

</asp:MultiView>

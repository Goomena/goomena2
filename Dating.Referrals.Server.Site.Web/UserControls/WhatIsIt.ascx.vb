﻿Imports DevExpress.Web.ASPxPopupControl

Public Class WhatIsIt
    Inherits System.Web.UI.UserControl


    Public ReadOnly Property PopupControl As ASPxPopupControl
        Get
            Return popupWhatIs
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub


    Public Shared Function OnMoreInfoClickFunc(input As String, contentUrl As String, headerText As String) As String
        contentUrl = contentUrl.Replace("'", "\'")
        If (Not String.IsNullOrEmpty(headerText)) Then headerText = headerText.Replace("'", "\'")
        input = input.Replace("[contentUrl]", contentUrl)
        input = input.Replace("[headerText]", headerText)
        Return input
    End Function


End Class
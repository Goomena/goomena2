﻿Imports DevExpress.Web.ASPxPager
Imports Dating.Server.Core.DLL
Imports Dating.Referrals.Server.Site.Web.OfferControl3

Public Class LikesControl
    Inherits BaseUserControl




    Dim _pageData As clsPageData
    Public ReadOnly Property CurrentPageData As clsPageData
        Get
            If (_pageData Is Nothing) Then _pageData = New clsPageData("control.LikesControl", Context)
            Return _pageData
        End Get
    End Property


    Public Property ShowNoPhotoText As Boolean
    Public Property ShowEmptyListText As Boolean

    Public Property UsersListView As WinkUsersListViewEnum
        Get
            If (Me.ViewState("WinkUsersListViewEnum") IsNot Nothing) Then Return Me.ViewState("WinkUsersListViewEnum")
            Return WinkUsersListViewEnum.None
        End Get
        Set(value As WinkUsersListViewEnum)
            Me.ViewState("WinkUsersListViewEnum") = value
        End Set
    End Property


    Private _list As List(Of clsWinkUserListItem)
    Public Property UsersList As List(Of clsWinkUserListItem)
        Get
            If (Me._list Is Nothing) Then Me._list = New List(Of clsWinkUserListItem)
            Return Me._list
        End Get
        Set(value As List(Of clsWinkUserListItem))
            Me._list = value
        End Set
    End Property



    <PersistenceMode(PersistenceMode.InnerDefaultProperty)>
    Public ReadOnly Property Repeater As Repeater
        Get
            If (UsersListView = WinkUsersListViewEnum.AcceptedOffers) Then
                Return Me.rptAccepted

            ElseIf (UsersListView = WinkUsersListViewEnum.NewOffers) Then
                Return Me.rptNew

            ElseIf (UsersListView = WinkUsersListViewEnum.LikesOffers) Then
                Return Me.rptNew

            ElseIf (UsersListView = WinkUsersListViewEnum.PokesOffers) Then
                Return Me.rptNew

            ElseIf (UsersListView = WinkUsersListViewEnum.PendingOffers) Then
                Return Me.rptPending

            ElseIf (UsersListView = WinkUsersListViewEnum.RejectedOffers) Then
                Return Me.rptRejected

            Else
                Return Nothing
            End If
        End Get
    End Property


    Protected Overrides Sub OnInit(e As System.EventArgs)
        MyBase.OnInit(e)
        'ModGlobals.AddScriptResourceToHeader(Me.Page, "~/Scripts/jquery-1.7.min.js")
        ModGlobals.AddScriptResourceToHeader(Me.Page, "~/Scripts/bootstrap-dropdown.js")
    End Sub



    Public Overrides Sub DataBind()

        If (Me.ShowNoPhotoText) Then

            Dim oNoPhoto As New clsWinkUserListItem()
            oNoPhoto.HasNoPhotosText = Me.CurrentPageData.GetCustomString("HasNoPhotosText")
            oNoPhoto.AddPhotosText = Me.CurrentPageData.GetCustomString("AddPhotosText")

            Dim os As New List(Of clsWinkUserListItem)
            os.Add(oNoPhoto)

            fvNoPhoto.DataSource = os
            fvNoPhoto.DataBind()

            Me.MultiView1.SetActiveView(vwNoPhoto)


        ElseIf (Me.ShowEmptyListText) Then

            Dim oNoOffer As New clsWinkUserListItem()
            If (UsersListView = WinkUsersListViewEnum.AcceptedOffers) Then
                oNoOffer.NoOfferText = Me.CurrentPageData.GetCustomString("AcceptedOffersListEmptyText")

            ElseIf (UsersListView = WinkUsersListViewEnum.NewOffers) Then
                oNoOffer.NoOfferText = Me.CurrentPageData.GetCustomString("NewOffersListEmptyText")

            ElseIf (UsersListView = WinkUsersListViewEnum.LikesOffers) Then
                oNoOffer.NoOfferText = Me.CurrentPageData.GetCustomString("WinksListEmptyText")

            ElseIf (UsersListView = WinkUsersListViewEnum.PokesOffers) Then
                oNoOffer.NoOfferText = Me.CurrentPageData.GetCustomString("PokesListEmptyText")

            ElseIf (UsersListView = WinkUsersListViewEnum.PendingOffers) Then
                oNoOffer.NoOfferText = Me.CurrentPageData.GetCustomString("PendingOffersListEmptyText")

            ElseIf (UsersListView = WinkUsersListViewEnum.RejectedOffers) Then
                oNoOffer.NoOfferText = Me.CurrentPageData.GetCustomString("RejectedOffersListEmptyText")


            ElseIf (UsersListView = WinkUsersListViewEnum.WhoViewedMeList) Then
                oNoOffer.NoOfferText = Me.CurrentPageData.GetCustomString("WhoViewedMeListEmptyText")

            ElseIf (UsersListView = WinkUsersListViewEnum.WhoFavoritedMeList) Then
                oNoOffer.NoOfferText = Me.CurrentPageData.GetCustomString("WhoFavoritedMeListEmptyText")

            ElseIf (UsersListView = WinkUsersListViewEnum.MyFavoriteList) Then
                oNoOffer.NoOfferText = Me.CurrentPageData.GetCustomString("MyFavoriteListEmptyText")

            ElseIf (UsersListView = WinkUsersListViewEnum.MyBlockedList) Then
                oNoOffer.NoOfferText = Me.CurrentPageData.GetCustomString("MyBlockedListEmptyText")

            ElseIf (UsersListView = WinkUsersListViewEnum.MyViewedList) Then
                oNoOffer.NoOfferText = Me.CurrentPageData.GetCustomString("MyViewedListEmptyText")

            End If

            oNoOffer.SearchOurMembersText = Me.CurrentPageData.GetCustomString("SearchOurMembersText")

            Dim os As New List(Of clsWinkUserListItem)
            os.Add(oNoOffer)

            fvNoOffers.DataSource = os
            fvNoOffers.DataBind()

            Me.MultiView1.SetActiveView(vwNoOffers)

        Else

            If (UsersListView = WinkUsersListViewEnum.AcceptedOffers) Then
                Me.MultiView1.SetActiveView(vwAcceptedOffers)

            ElseIf (UsersListView = WinkUsersListViewEnum.NewOffers) Then
                Me.MultiView1.SetActiveView(vwNewOffers)

            ElseIf (UsersListView = WinkUsersListViewEnum.LikesOffers) Then
                Me.MultiView1.SetActiveView(vwNewOffers)

            ElseIf (UsersListView = WinkUsersListViewEnum.PokesOffers) Then
                Me.MultiView1.SetActiveView(vwNewOffers)

            ElseIf (UsersListView = WinkUsersListViewEnum.PendingOffers) Then
                Me.MultiView1.SetActiveView(vwPendingOffers)

            ElseIf (UsersListView = WinkUsersListViewEnum.RejectedOffers) Then
                Me.MultiView1.SetActiveView(vwRejectedOffers)

            Else
                Me.MultiView1.SetActiveView(View1)

            End If
        End If




        If (Me.Repeater IsNot Nothing) Then

            Try
                For Each itm As clsWinkUserListItem In Me.UsersList
                    itm.FillTextResourceProperties(Me.CurrentPageData)
                Next
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try

            Me.Repeater.DataSource = Me.UsersList
            Me.Repeater.DataBind()
        End If

    End Sub



    Protected Function WriteOffer_MemberInfo(DataItem As Dating.Referrals.Server.Site.Web.clsWinkUserListItem) As Object
        'Dim str As String = "<ul>" & vbCrLf & _
        '    "<li><b>" & DataItem.OtherMemberAge") & "</b> " & DataItem.YearsOldText") & "</li>" & vbCrLf & _
        '    "<li><b>" & DataItem.OtherMemberCity") & ", " & DataItem.OtherMemberRegion") & "</b></li>" & vbCrLf & _
        '    "</ul>" & vbCrLf

        'Return str


        Dim sb As New StringBuilder()
        Try
            sb.Append("<ul>")
            sb.Append("<li>")

            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberAge) Then
                sb.Append("<b>" & DataItem.OtherMemberAge & "</b>")
                sb.Append(" " & DataItem.YearsOldText & " ")
            End If

            sb.Append("</li>")


            sb.Append("<li>")
            sb.Append("<b>")

            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberCity) Then
                sb.Append(DataItem.OtherMemberCity)
            End If

            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberCity) AndAlso Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberRegion) Then
                sb.Append(",  ")
            End If

            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberRegion) Then
                sb.Append(DataItem.OtherMemberRegion)
            End If

            sb.Append("</b>")
            sb.Append("</li>")


            sb.Append("</ul>")

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Return sb.ToString()
    End Function


    Protected Function WriteSearch_MemberInfo(DataItem As Dating.Referrals.Server.Site.Web.clsWinkUserListItem) As Object

        'Dim str As String = "<ul>" & vbCrLf & _
        '    "<li><b>" & DataItem.OtherMemberAge & "</b> " & DataItem.YearsOldText & " <b>" & Eval("OtherMemberCity & ", " & Eval("OtherMemberRegion & "</b></li>" & vbCrLf & _
        '    "<li><b>" & DataItem.OtherMemberHeight & " - " & DataItem.OtherMemberPersonType & "</b></li>" & vbCrLf & _
        '    "<li>" & DataItem.OtherMemberHair & ", " & DataItem.OtherMemberEyes & ", " & DataItem.OtherMemberEthnicity & "</li>" & vbCrLf & _
        '    "</ul>" & vbCrLf

        Dim sb As New StringBuilder()
        Try

            sb.Append("<ul>")

            sb.Append("<li>")

            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberAge) Then
                sb.Append("<b>" & DataItem.OtherMemberAge & "</b>")
                sb.Append(" " & DataItem.YearsOldText & " ")
            End If

            sb.Append("<b>")
            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberCity) Then
                sb.Append(DataItem.OtherMemberCity)
            End If

            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberCity) AndAlso Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberRegion) Then
                sb.Append(",  ")
            End If

            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberRegion) Then
                sb.Append(DataItem.OtherMemberRegion)
            End If

            sb.Append("</b>")
            sb.Append("</li>")


            sb.Append("<li>")
            sb.Append("<b>")

            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberHeight) Then
                sb.Append(DataItem.OtherMemberHeight)
            End If

            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberHeight) AndAlso Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberPersonType) Then
                sb.Append(" - ")
            End If

            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberPersonType) Then
                sb.Append(DataItem.OtherMemberPersonType)
            End If

            sb.Append("</b>")
            sb.Append("</li>")


            sb.Append("<li>")

            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberHair) Then
                sb.Append(DataItem.OtherMemberHair)
            End If

            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberEyes) Then
                If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberHair) Then sb.Append(", ")
                sb.Append(DataItem.OtherMemberEyes)
            End If

            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberEthnicity) Then
                If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberEyes) OrElse Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberHair) Then sb.Append(", ")
                sb.Append(DataItem.OtherMemberEthnicity)
            End If

            sb.Append("</li>")
            sb.Append("</ul>")

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Return sb.ToString()
    End Function


    'Protected Sub rptAccepted_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptAccepted.ItemDataBound
    '    Try

    '        Dim divTooltipholder As Control = e.Item.FindControl("divTooltipholder")
    '        If (divTooltipholder IsNot Nothing) Then
    '            Dim itm As clsWinkUserListItem = e.Item.DataItem
    '            If (ProfileHelper.IsMale(itm.Genderid)) Then
    '                divTooltipholder.Visible = True
    '            End If
    '        End If

    '    Catch ex As Exception

    '    End Try
    'End Sub

    Protected Sub rptNew_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptNew.ItemDataBound
        Try

            Dim lnkWhatIs As LinkButton = e.Item.FindControl("lnkWhatIs")
            If (lnkWhatIs IsNot Nothing) Then
                lnkWhatIs.ToolTip = Me.CurrentPageData.GetCustomString("WhatIsText")
                Dim contentUrl As String = ResolveUrl("~/Members/InfoWin.aspx?info=LikesNewCommands")
                contentUrl = contentUrl.Replace("'", "\'")
                Dim headerText As String = Me.CurrentPageData.GetCustomString("Search_WhatIsPopup_HeaderText")
                If (Not String.IsNullOrEmpty(headerText)) Then headerText = headerText.Replace("'", "\'")
                lnkWhatIs.OnClientClick = lnkWhatIs.OnClientClick.Replace("[contentUrl]", contentUrl)
                lnkWhatIs.OnClientClick = lnkWhatIs.OnClientClick.Replace("[headerText]", headerText)
            End If

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub rptPending_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptPending.ItemDataBound
        Try

            Dim lnkWhatIs As LinkButton = e.Item.FindControl("lnkWhatIs")
            If (lnkWhatIs IsNot Nothing) Then
                lnkWhatIs.ToolTip = Me.CurrentPageData.GetCustomString("WhatIsText")
                Dim contentUrl As String = ResolveUrl("~/Members/InfoWin.aspx?info=LikesPendingCommands")
                contentUrl = contentUrl.Replace("'", "\'")
                Dim headerText As String = Me.CurrentPageData.GetCustomString("Search_WhatIsPopup_HeaderText")
                If (Not String.IsNullOrEmpty(headerText)) Then headerText = headerText.Replace("'", "\'")
                lnkWhatIs.OnClientClick = lnkWhatIs.OnClientClick.Replace("[contentUrl]", contentUrl)
                lnkWhatIs.OnClientClick = lnkWhatIs.OnClientClick.Replace("[headerText]", headerText)
            End If

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub rptRejected_ItemDataBound(source As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptRejected.ItemDataBound
        Try

            Dim lnkWhatIs As LinkButton = e.Item.FindControl("lnkWhatIs")
            If (lnkWhatIs IsNot Nothing) Then
                lnkWhatIs.ToolTip = Me.CurrentPageData.GetCustomString("WhatIsText")
                Dim contentUrl As String = ResolveUrl("~/Members/InfoWin.aspx?info=LikesRejectedCommands")
                contentUrl = contentUrl.Replace("'", "\'")
                Dim headerText As String = Me.CurrentPageData.GetCustomString("Search_WhatIsPopup_HeaderText")
                If (Not String.IsNullOrEmpty(headerText)) Then headerText = headerText.Replace("'", "\'")
                lnkWhatIs.OnClientClick = lnkWhatIs.OnClientClick.Replace("[contentUrl]", contentUrl)
                lnkWhatIs.OnClientClick = lnkWhatIs.OnClientClick.Replace("[headerText]", headerText)
            End If

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub rptAccepted_ItemDataBound(source As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptAccepted.ItemDataBound
        Try

            Dim lnkWhatIs As LinkButton = e.Item.FindControl("lnkWhatIs")
            If (lnkWhatIs IsNot Nothing) Then
                lnkWhatIs.ToolTip = Me.CurrentPageData.GetCustomString("WhatIsText")
                Dim contentUrl As String = ResolveUrl("~/Members/InfoWin.aspx?info=LikesAcceptedCommands")
                contentUrl = contentUrl.Replace("'", "\'")
                Dim headerText As String = Me.CurrentPageData.GetCustomString("Search_WhatIsPopup_HeaderText")
                If (Not String.IsNullOrEmpty(headerText)) Then headerText = headerText.Replace("'", "\'")
                lnkWhatIs.OnClientClick = lnkWhatIs.OnClientClick.Replace("[contentUrl]", contentUrl)
                lnkWhatIs.OnClientClick = lnkWhatIs.OnClientClick.Replace("[headerText]", headerText)
            End If


        Catch ex As Exception

        End Try
    End Sub
End Class
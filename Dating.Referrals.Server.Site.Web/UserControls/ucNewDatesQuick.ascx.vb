﻿Imports Dating.Server.Core.DLL
Imports DevExpress.Web.ASPxEditors

Public Class ucNewDatesQuick
    Inherits BaseUserControl

    Public Event ListBound(ByVal sender As Object, ByVal e As EventArgs)

    Dim _pageData As clsPageData
    Public ReadOnly Property CurrentPageData As clsPageData
        Get
            If (_pageData Is Nothing) Then _pageData = New clsPageData("control.DatesControl", Context)
            Return _pageData
        End Get
    End Property


    Public Property ItemsPerPage As Integer
        Get
            If (Me.ViewState("ItemsPerPage") IsNot Nothing) Then Return Me.ViewState("ItemsPerPage")
            Return 10
        End Get
        Set(value As Integer)
            Me.ViewState("ItemsPerPage") = value
        End Set
    End Property

    Public ReadOnly Property HasData As Boolean
        Get
            Return (dvDates.DataSource IsNot Nothing) AndAlso (DirectCast(dvDates.DataSource, DataTable).Rows.Count > 0)
        End Get
    End Property



    Private Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        dvDates.DataSource = clsSearchHelper.GetNewDatesQuickDataTable(Me.MasterProfileId,
                                                                       ProfileStatusEnum.Approved,
                                                                       OffersSortEnum.RecentOffers,
                                                                       Me.SessionVariables.MemberData.Zip,
                                                                       Me.SessionVariables.MemberData.latitude,
                                                                       Me.SessionVariables.MemberData.longitude,
                                                                       clsSearchHelper.DISTANCE_DEFAULT,
                                                                       Me.ItemsPerPage)
        dvDates.DataBind()
        RaiseEvent ListBound(Me, e)
    End Sub

    'Protected Overrides Sub OnPreRender(e As System.EventArgs)
    '    dvWinks.RowPerPage = Me.ItemsPerPage
    '    MyBase.OnPreRender(e)
    'End Sub



    'Protected Sub sdsDates_Selecting(sender As Object, e As System.Web.UI.WebControls.SqlDataSourceSelectingEventArgs) Handles sdsDates.Selecting
    '    For Each prm As SqlClient.SqlParameter In e.Command.Parameters

    '        If (prm.ParameterName = "@CurrentProfileId") Then
    '            Dim MasterProfileId = Me.MasterProfileId
    '            prm.Value = MasterProfileId
    '        ElseIf (prm.ParameterName = "@ReturnRecordsWithStatus") Then
    '            prm.Value = ProfileStatusEnum.Approved
    '        ElseIf (prm.ParameterName = "@NumberOfRecordsToReturn") Then
    '            prm.Value = Me.ItemsPerPage
    '        End If

    '    Next
    'End Sub



    Protected Sub dvDates_DataBound(sender As Object, e As EventArgs) Handles dvDates.DataBound
        Try

            For Each dvi As DevExpress.Web.ASPxDataView.DataViewItem In dvDates.Items
                Try

                    Dim dr As DataRowView = dvi.DataItem
                    'Dim distance As Integer = ProfileHelper.CalculateDistance(Me.GetCurrentProfile(), dr)


                    ''''''''''''''''''''''''''''''''''''
                    '' OfferAcceptedWithAmountText
                    ''''''''''''''''''''''''''''''''''''
                    Dim YouHaveNewDateFromText As String = ""
                    Dim OffersFromProfileID = IIf(dr("OffersFromProfileID") IsNot Nothing, dr("OffersFromProfileID"), 0)
                    YouHaveNewDateFromText = Me.CurrentPageData.GetCustomString("YouHaveNewDateFromText")

                    'If (OffersFromProfileID = Me.MasterProfileId) Then
                    '    '''''''''''''''''''
                    '    ' other user accepted an offer of current user
                    '    '''''''''''''''''''

                    '    OfferAcceptedWithAmountText = Me.CurrentPageData.GetCustomString("OtherMemberOfferAcceptedWithAmountText")
                    'Else
                    '    '''''''''''''''''''
                    '    ' current user accepted an offer
                    '    '''''''''''''''''''

                    '    OfferAcceptedWithAmountText = Me.CurrentPageData.GetCustomString("CurrentMemberOfferAcceptedWithAmountText")
                    'End If

                    YouHaveNewDateFromText = clsWinkUserListItem.ReplaceTokens(YouHaveNewDateFromText, dr("LoginName"), dr("OffersAmount"), ProfileHelper.Config_UNLOCK_CONVERSATION_CREDITS, dr("distance"))

                    Dim lblOfferAcceptedWithAmountText As Label = dvDates.FindItemControl("lblOfferAcceptedWithAmountText", dvi)
                    lblOfferAcceptedWithAmountText.Text = YouHaveNewDateFromText



                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "")
                End Try
            Next

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub
End Class
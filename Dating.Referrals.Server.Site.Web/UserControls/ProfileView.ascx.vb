﻿Imports Dating.Server.Datasets.DLL.DSMembers
Imports Dating.Server.Core.DLL
Imports Dating.Server.Datasets.DLL

Public Class ProfileView
    Inherits BaseUserControl

    Dim _pageData As clsPageData
    Protected ReadOnly Property CurrentPageData As clsPageData
        Get
            If (_pageData Is Nothing) Then _pageData = New clsPageData("~/Members/control.ProfileView", Context)
            Return _pageData
        End Get
    End Property


    Protected Property UserIdInView As Integer
        Get
            Return ViewState("UserIdInView")
        End Get
        Set(value As Integer)
            ViewState("UserIdInView") = value
        End Set
    End Property


    Protected Property UserLoginNameInView As String
        Get
            Return ViewState("UserLoginNameInView")
        End Get
        Set(value As String)
            ViewState("UserLoginNameInView") = value
        End Set
    End Property


    Protected Overrides Sub OnInit(e As System.EventArgs)
        MyBase.OnInit(e)
        ModGlobals.AddScriptResourceToHeader(Me.Page, "~/Scripts/bootstrap-dropdown.js")
    End Sub


    Private Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        Try
            If (Not Me.IsPostBack) Then

                LoadLAG()
                LoadView()
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub

    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender

        Try
            Dim cPageBasic As Dating.Server.Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub


    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
        LoadView()
    End Sub


    Protected Sub LoadLAG()
        Try
            'Dim cPageBasic As Dating.Server.Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic

            SetControlsValue(Me, CurrentPageData)
            'GeneralFunctions.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)

            lnkEditProf.Text = CurrentPageData.GetCustomString("lnkEditProf")

            lnkMessageF.Text = CurrentPageData.GetCustomString("lnkMessage")
            lnkFavoriteF.Text = CurrentPageData.GetCustomString("lnkFavorite")
            lnkUnFavoriteF.Text = CurrentPageData.GetCustomString("lnkUnFavorite")
            lnkMakeOfferF.Text = CurrentPageData.GetCustomString("lnkMakeOffer")
            lnkUnlockOfferF.Text = CurrentPageData.GetCustomString("OfferAcceptedUnlockText")
            lnkWinkF.Text = CurrentPageData.GetCustomString("lnkWink")
            lnkUnWinkF.Text = CurrentPageData.GetCustomString("lnkWink")

            lnkMessageM.Text = CurrentPageData.GetCustomString("lnkMessage")
            lnkFavoriteM.Text = CurrentPageData.GetCustomString("lnkFavorite")
            lnkUnFavoriteM.Text = CurrentPageData.GetCustomString("lnkUnFavorite")
            lnkMakeOfferM.Text = CurrentPageData.GetCustomString("lnkMakeOffer")
            lnkUnlockOfferM.Text = CurrentPageData.GetCustomString("OfferAcceptedUnlockText")
            lnkWinkM.Text = CurrentPageData.GetCustomString("lnkWink")
            lnkUnWinkM.Text = CurrentPageData.GetCustomString("lnkWink")
            msg_ActionsText.Text = CurrentPageData.GetCustomString("lnkMessage")

            cmdHideGifts.Text = CurrentPageData.GetCustomString("cmdHideGifts")
            lnkReport.Text = CurrentPageData.GetCustomString("lnkReport")
            lnkBlock.Text = CurrentPageData.GetCustomString("lnkBlock")
            'lnkUnWink.Text = CurrentPageData.GetCustomString("lnkUnWink")

            'lblAllowAccessPhotosLevel.Text = CurrentPageData.GetCustomString(lblAllowAccessPhotosLevel.ID)
            'lblAllowAccessPhotosLevelIcon.Text = CurrentPageData.GetCustomString(lblAllowAccessPhotosLevel.ID)

            setLevelPopUp.HeaderText = CurrentPageData.GetCustomString("popupWindows.PhotoLevelTitle")

            msg_LastUpdatedDT.Text = CurrentPageData.GetCustomString(msg_LastUpdatedDT.ID)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Private Sub LoadView()

        Try
            Me.UserIdInView = 0

            Try
                If (Request.QueryString("p") <> "") Then
                    Dim prof As EUS_Profile = DataHelpers.GetEUS_ProfileMasterByLoginName(Me.CMSDBDataContext, Request.QueryString("p"), ProfileStatusEnum.Approved)
                    If (prof IsNot Nothing) Then
                        Me.UserIdInView = prof.ProfileID
                        Me.UserLoginNameInView = prof.LoginName
                    End If
                End If
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try


            If (Me.UserIdInView > 1 AndAlso Me.UserIdInView <> Me.MasterProfileId) Then

                If (Me.IsFemale) Then
                    mvGender.SetActiveView(vwFemale)
                Else
                    mvGender.SetActiveView(vwMale)
                End If

                Me.LoadProfile(Me.UserIdInView)

                If (Me.IsMale) Then
                    Dim currentUserHasPhotos As Boolean = clsUserDoes.HasPhotos(Me.MasterProfileId, Me.MirrorProfileId)
                    If (Not currentUserHasPhotos) Then
                        UploadPhotoPopUp.ShowOnPageLoad = True
                        Dim _pageData2 As clsPageData = New clsPageData("~/Members/control.PhotosEdit", Context)
                        msg_HasNoPhotosText.Text = _pageData2.GetCustomString("msg_HasNoPhotosText")
                        lnkUploadPhoto.Text = CurrentPageData.GetCustomString("lnkUploadPhoto")
                    End If
                End If

                clsUserDoes.MarkAsViewed(Me.UserIdInView, Me.MasterProfileId)
                clsUserDoes.MarkDateOfferAsViewed(Me.UserIdInView, Me.MasterProfileId)

                Dim hasCommunication As Boolean = clsUserDoes.HasCommunication(Me.UserIdInView, Me.MasterProfileId)
                Dim hasAnyOffer As Boolean = clsUserDoes.HasAnyOffer(Me.UserIdInView, Me.MasterProfileId)
                Dim acceptedOffer As EUS_Offer = clsUserDoes.GetAcceptedOrUnlockedOffer(Me.UserIdInView, Me.MasterProfileId)
                Dim isMyWinkExists As Boolean = (clsUserDoes.GetMyPendingWink(Me.UserIdInView, Me.MasterProfileId) IsNot Nothing)
                Dim IsAnyMessageExchanged As Boolean = clsUserDoes.IsAnyMessageExchanged(Me.UserIdInView, Me.MasterProfileId)


                If (Me.IsFemale) Then

                    lnkFavoriteF.Visible = (Not clsUserDoes.IsFavorited(Me.UserIdInView, Me.MasterProfileId))
                    lnkFavoriteF.CommandArgument = Me.UserIdInView

                    lnkUnFavoriteF.Visible = Not lnkFavoriteF.Visible
                    '(clsUserDoes.IsFavorited(Me.UserIdInView, Me.MasterProfileId))
                    lnkUnFavoriteF.CommandArgument = Me.UserIdInView

                    lnkMakeOfferF.Visible = (Not HasOffer(Me.UserIdInView, Me.MasterProfileId) AndAlso Not clsUserDoes.HasCommunication(Me.UserIdInView, Me.MasterProfileId))
                    lnkMakeOfferF.CommandArgument = Me.UserIdInView

                    lnkReport.NavigateUrl = "~/Members/Report.aspx?p=" & HttpUtility.UrlEncode(Me.UserLoginNameInView)

                    lnkBlock.Visible = (Not clsUserDoes.IsBlocked(Me.UserIdInView, Me.MasterProfileId)) '
                    lnkBlock.CommandArgument = Me.UserIdInView
                    If (Not lnkBlock.Visible) Then
                        ltrSeparator.Visible = False
                    Else
                        ltrSeparator.Visible = True
                    End If


                    lnkMessageF.Visible = True
                    lnkMessageF.NavigateUrl = "~/Members/Conversation.aspx?p=" & Me.UserLoginNameInView
                    '' allow only women to send messages ,WHEN no offer  or unlocking exists
                    ''men will send messages when the communication is open
                    'If (ProfileHelper.IsMale(Me.GetCurrentProfile().GenderId)) Then
                    '    If (acceptedOffer IsNot Nothing OrElse hasCommunication = True) Then
                    '        lnkMessage.Visible = True
                    '    Else
                    '        lnkMessage.Visible = False
                    '    End If
                    'End If


                    lnkUnlockOfferF.Visible = False
                    divTooltipholderF.Visible = False
                    'If (ProfileHelper.IsMale(Me.GetCurrentProfile().GenderId)) Then
                    '    If (Not hasCommunication AndAlso acceptedOffer IsNot Nothing AndAlso acceptedOffer.StatusID = ProfileHelper.OfferStatusID_ACCEPTED) Then

                    '        Dim creditsRequired As Integer = clsUserDoes.GetRequiredCredits_UnlockConversation().CreditsAmount
                    '        lnkUnlockOfferF.Text = lnkUnlockOfferF.Text.Replace("###CREDITS###", creditsRequired.ToString())
                    '        lnkUnlockOfferF.CommandArgument = acceptedOffer.OfferID
                    '        lnkUnlockOfferF.CommandName = OfferControlCommandEnum.UNLOCKOFFER

                    '        lnkTooltipUnlockF.ToolTip = CurrentPageData.GetCustomString("UnlockNotice")
                    '        lnkTooltipUnlockF.ToolTip = lnkTooltipUnlockF.ToolTip.Replace("###LOGINNAME###", Me.UserLoginNameInView)

                    '        lnkUnlockOfferF.Visible = True
                    '        divTooltipholderF.Visible = True

                    '        lnkMessageF.Visible = False
                    '    End If
                    'End If




                    ' no offer, no communication -> allow wink
                    lnkWinkF.Visible = (hasAnyOffer = False AndAlso hasCommunication = False AndAlso IsAnyMessageExchanged = False)
                    lnkWinkF.CommandArgument = Me.UserIdInView

                    lnkUnWinkF.Visible = (Not lnkWinkF.Visible AndAlso isMyWinkExists)
                    lnkUnWinkF.CommandArgument = Me.UserIdInView

                Else

                    lnkFavoriteM.Visible = (Not clsUserDoes.IsFavorited(Me.UserIdInView, Me.MasterProfileId))
                    lnkFavoriteM.CommandArgument = Me.UserIdInView

                    lnkUnFavoriteM.Visible = Not lnkFavoriteM.Visible
                    '(clsUserDoes.IsFavorited(Me.UserIdInView, Me.MasterProfileId))
                    lnkUnFavoriteM.CommandArgument = Me.UserIdInView

                    lnkMakeOfferM.Visible = (Not HasOffer(Me.UserIdInView, Me.MasterProfileId) AndAlso Not clsUserDoes.HasCommunication(Me.UserIdInView, Me.MasterProfileId))
                    lnkMakeOfferM.CommandArgument = Me.UserIdInView

                    lnkReport.NavigateUrl = "~/Members/Report.aspx?p=" & HttpUtility.UrlEncode(Me.UserLoginNameInView)

                    lnkBlock.Visible = (Not clsUserDoes.IsBlocked(Me.UserIdInView, Me.MasterProfileId)) '
                    lnkBlock.CommandArgument = Me.UserIdInView
                    If (Not lnkBlock.Visible) Then
                        ltrSeparator.Visible = False
                    Else
                        ltrSeparator.Visible = True
                    End If


                    '' allow only women to send messages ,WHEN no offer  or unlocking exists
                    ''men will send messages when the communication is open
                    'If (ProfileHelper.IsMale(Me.GetCurrentProfile().GenderId)) Then
                    '    If (acceptedOffer IsNot Nothing OrElse hasCommunication = True) Then
                    '        lnkMessage.Visible = True
                    '    Else
                    '        lnkMessage.Visible = False
                    '    End If
                    'End If


                    lnkUnlockOfferM.Visible = False
                    divTooltipholderM.Visible = False
                    'If (ProfileHelper.IsMale(Me.GetCurrentProfile().GenderId)) Then
                    If (Not hasCommunication AndAlso acceptedOffer IsNot Nothing AndAlso acceptedOffer.StatusID = ProfileHelper.OfferStatusID_ACCEPTED) Then

                        Dim creditsRequired As Integer = ProfileHelper.Config_UNLOCK_CONVERSATION_CREDITS
                        lnkUnlockOfferM.Text = lnkUnlockOfferM.Text.Replace("###CREDITS###", creditsRequired.ToString())
                        lnkUnlockOfferM.CommandArgument = acceptedOffer.OfferID
                        lnkUnlockOfferM.CommandName = OfferControlCommandEnum.UNLOCKOFFER

                        lnkTooltipUnlockM.ToolTip = CurrentPageData.GetCustomString("UnlockNotice")
                        lnkTooltipUnlockM.ToolTip = lnkTooltipUnlockM.ToolTip.Replace("###LOGINNAME###", Me.UserLoginNameInView)

                        lnkUnlockOfferM.Visible = True
                        divTooltipholderM.Visible = True

                        lnkMessageM.Visible = False
                    End If
                    'End If




                    ' no offer, no communication -> allow wink
                    lnkWinkM.Visible = (hasAnyOffer = False AndAlso hasCommunication = False AndAlso IsAnyMessageExchanged = False)
                    lnkWinkM.CommandArgument = Me.UserIdInView

                    lnkUnWinkM.Visible = (Not lnkWinkM.Visible AndAlso isMyWinkExists)
                    lnkUnWinkM.CommandArgument = Me.UserIdInView


                    If (Not hasCommunication) Then
                        divActionsMenu.Visible = True

                        lnkSendMsgOnce.Text = BasePage.ReplaceCommonTookens(CurrentPageData.GetCustomString("SendMessageOnceText"), Me.UserLoginNameInView)
                        lnkSendMsgMany.Text = BasePage.ReplaceCommonTookens(CurrentPageData.GetCustomString("SendMessageManyText"), Me.UserLoginNameInView)
                        lnkSendMsgOnce.NavigateUrl = ResolveUrl("~/Members/Conversation.aspx?send=once&p=" & Server.UrlEncode(Me.UserLoginNameInView))
                        lnkSendMsgMany.NavigateUrl = ResolveUrl("~/Members/Conversation.aspx?send=unl&p=" & Server.UrlEncode(Me.UserLoginNameInView))
                    Else
                        lnkMessageM.Visible = True
                        lnkMessageM.NavigateUrl = "~/Members/Conversation.aspx?p=" & Me.UserLoginNameInView
                    End If
                End If

                ShowPhotosLevel()
                pPhotosLevel.Visible = True
                pReport.Visible = True

            Else
                Me.LoadProfile(Me.MasterProfileId)
                mvGender.ActiveViewIndex = 0
                'lnkEditProfF.Visible = True
                'lnkMessageF.Visible = False
                'lnkFavoriteF.Visible = False
                'lnkUnFavoriteF.Visible = False
                'lnkMakeOfferF.Visible = False
                'lnkUnWinkF.Visible = False
                'lnkWinkF.Visible = False
                'lnkUnlockOfferF.Visible = False
                pReport.Visible = False
                pPhotosLevel.Visible = False
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


    Private Sub ShowPhotosLevel()
        Try

            Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_PhotosDisplayLevel, Session("LagID"), "PhotosDisplayLevelId", cbPhotosLevel, True, False, "US")
            cbPhotosLevel.Items(0).Selected = True

            Dim phtLvl As EUS_ProfilePhotosLevel = GetEUS_ProfilePhotosLevel()
            If (phtLvl IsNot Nothing) Then
                ModGlobals.SelectComboItem(cbPhotosLevel, phtLvl.PhotoLevelID)
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub



    Sub LoadProfile(ByVal profileId As Integer)
        Try

            Dim allowDisplayLevel = 0

            'Dim masterRow As EUS_ProfilesRow = Nothing
            Dim _profilerows As New clsProfileRows(profileId)
            'Dim currentRow As EUS_ProfilesRow = _profilerows.GetMirrorRow()

            Dim usersHasCommunication As Boolean = False
            Dim currentRow As EUS_ProfilesRow = Nothing
            If (Me.MasterProfileId = profileId) Then
                currentRow = _profilerows.GetMirrorRow()
                usersHasCommunication = True

            Else
                currentRow = _profilerows.GetMasterRow()
                usersHasCommunication = clsUserDoes.HasCommunication(profileId, Me.MasterProfileId)

                Dim phtLvl As EUS_ProfilePhotosLevel = (From itm In Me.CMSDBDataContext.EUS_ProfilePhotosLevels
                                                       Where itm.FromProfileID = Me.UserIdInView AndAlso itm.ToProfileID = Me.MasterProfileId
                                                       Select itm).FirstOrDefault()


                If (phtLvl IsNot Nothing) Then
                    allowDisplayLevel = phtLvl.PhotoLevelID
                End If
            End If


            '''''''''''''''''''''''''''''''''''''
            ' load profile photos
            '''''''''''''''''''''''''''''''''''''
            Dim dtPhotos As EUS_CustomerPhotosDataTable = DataHelpers.GetEUS_CustomerPhotos_ByProfileOrMirrorID(profileId).EUS_CustomerPhotos
            Dim otherUserPhotos1 As New List(Of clsUserListItem)
            'Dim otherUserPhotos2 As New List(Of clsUserListItem)

            Dim defaultPhoto As New clsUserListItem()
            defaultPhoto.LoginName = currentRow.LoginName
            defaultPhoto.ProfileViewUrl = ResolveUrl("~/Members/Profile.aspx?p=" & currentRow.LoginName)

            For Each dr As EUS_CustomerPhotosRow In dtPhotos

                If (Me.MasterProfileId <> profileId AndAlso dr.DisplayLevel > allowDisplayLevel) Then
                    Continue For
                End If

                If (dr.HasAproved = True) Then

                    Dim uli As New clsUserListItem()
                    uli.LoginName = currentRow.LoginName
                    uli.ProfileViewUrl = ResolveUrl("~/Members/Profile.aspx?p=" & currentRow.LoginName)
                    uli.ImageUrl = ProfileHelper.GetProfileImageURL(dr.CustomerID, dr.FileName, currentRow.GenderId, False, Me.IsHTTPS)
                    uli.ImageThumbUrl = ProfileHelper.GetProfileImageURL(dr.CustomerID, dr.FileName, currentRow.GenderId, True, Me.IsHTTPS)
                    uli.ImageUploadDate = dr.DateTimeToUploading.ToLocalTime().ToString("d MMM, yyyy H:mm")
                    otherUserPhotos1.Add(uli)

                End If
            Next


            If (otherUserPhotos1.Count = 0) Then

                Dim uli As New clsUserListItem()
                uli.LoginName = currentRow.LoginName
                uli.ProfileViewUrl = ResolveUrl("~/Members/Profile.aspx?p=" & currentRow.LoginName)
                uli.ImageUrl = ProfileHelper.GetDefaultImageURL(currentRow.GenderId)
                uli.ImageThumbUrl = ProfileHelper.GetDefaultImageURL(currentRow.GenderId)
                'uli.ImageUploadDate = dr.DateTimeToUploading.ToLocalTime().ToString("d MMM, yyyy H:mm")
                otherUserPhotos1.Add(uli)

            End If


            ' set photo's list 
            lvPhotos.DataSource = otherUserPhotos1
            lvPhotos.DataBind()


            ' load profile text content
            '''''''''''''''''''''''''''''''''''''

            If (Me.MasterProfileId = profileId) Then
                ' the viewing profile is owned by member it self
                '''''''''''''''''''''''''''''''''''''''''''''

                If (Not currentRow.IsLastUpdateProfileDateTimeNull()) Then
                    lblLastUpdated.Text = currentRow.LastUpdateProfileDateTime.ToLocalTime().ToString("d MMM, yyyy H:mm")
                ElseIf (Not currentRow.IsDateTimeToRegisterNull()) Then
                    lblLastUpdated.Text = currentRow.DateTimeToRegister.ToLocalTime().ToString("d MMM, yyyy H:mm")
                End If

                If (Not currentRow.IsStatusNull()) Then
                    Dim profileStatus = "(<strong>###STATUS###</strong>)"
                    Dim statusText As String = globalStrings.GetCustomString("ProfileStatusEnum." & CType(currentRow.Status, ProfileStatusEnum).ToString(), Session("LAGID"))
                    lblStatus.Text = profileStatus.Replace("###STATUS###", statusText)

                    If (currentRow.Status = ProfileStatusEnum.Approved) Then
                        lblStatus.CssClass = "approved"
                    Else
                        lblStatus.CssClass = "warning"
                    End If
                End If

                'LoadWhoViewedAndFavoritedCurrentProfile()

                profileOwner.Visible = True
                'profileOwner2.Visible = True
            Else
                ' the viewing profile is owned by another member
                '''''''''''''''''''''''''''''''''''''''''''''

                profileOwner.Visible = False

                If ProfileHelper.IsMale(currentRow.GenderId) Then
                    lblPhotoLevel.Text = CurrentPageData.GetCustomString("MALE_lblPhotoLevelTitle")
                Else
                    lblPhotoLevel.Text = CurrentPageData.GetCustomString("FEMALE_lblPhotoLevelTitle")
                End If
                lblPhotoLevel.Text = lblPhotoLevel.Text.Replace("###LOGINNAME###", currentRow.LoginName)
                lblPhotoLevel.Text = lblPhotoLevel.Text.Replace(" ", "&nbsp;")

                lblAllowAccessPhotosLevel.Text = CurrentPageData.GetCustomString("lblAllowAccessPhotosLevel")
                lblAllowAccessPhotosLevel.Text = lblAllowAccessPhotosLevel.Text.Replace("###LOGINNAME###", currentRow.LoginName)

                btnSave.Text = CurrentPageData.GetCustomString("btnSave")

                If (currentRow.IsPrivacySettings_HideMyLastLoginDateTimeNull() OrElse currentRow.PrivacySettings_HideMyLastLoginDateTime = False) Then
                    pLastLoggedInDT.Visible = True
                Else
                    pLastLoggedInDT.Visible = False
                End If

            End If


            lblLogin.Text = currentRow.LoginName


            If ProfileHelper.IsMale(currentRow.GenderId) Then
                liAnnualIncome.Visible = True

                '    If (ProfileHelper.IsGenerous(currentRow.AccountTypeId)) Then
                '        lblAccountType.Text = globalStrings.GetCustomString("msg_GenerousMale")
                '    ElseIf (ProfileHelper.IsAttractive(currentRow.AccountTypeId)) Then
                '        lblAccountType.Text = globalStrings.GetCustomString("msg_AttractiveMale")
                '    End If

            ElseIf ProfileHelper.IsFemale(currentRow.GenderId) Then
                liAnnualIncome.Visible = False

                '    If (ProfileHelper.IsGenerous(currentRow.AccountTypeId)) Then
                '        lblAccountType.Text = globalStrings.GetCustomString("msg_GenerousFemale")
                '    ElseIf (ProfileHelper.IsAttractive(currentRow.AccountTypeId)) Then
                '        lblAccountType.Text = globalStrings.GetCustomString("msg_AttractiveFemale")
                '    End If

            End If

            'lblAccountType.Text = "(" & lblAccountType.Text & ")"


            Dim mins As Integer = 20
            Dim config As New clsConfigValues()
            If (Not Integer.TryParse(config.members_online_minutes, mins)) Then mins = 20
            Dim LastActivityUTCDate As DateTime = Date.UtcNow.AddMinutes(-mins)

            If (Not currentRow.IsLastActivityDateTimeNull() AndAlso Not currentRow.IsIsOnlineNull()) Then
                If (currentRow.LastActivityDateTime >= LastActivityUTCDate AndAlso currentRow.IsOnline) Then
                    imgOnline.Visible = True
                End If
            End If



            lblWillTravel.Visible = False
            If (Not currentRow.IsAreYouWillingToTravelNull()) Then
                If (currentRow.AreYouWillingToTravel) Then
                    lblWillTravel.Text = CurrentPageData.GetCustomString("lblWillTravel")
                    lblWillTravel.Visible = True
                End If
            End If


            lblAboutMeHeading.Visible = False
            If (Not currentRow.IsAboutMe_HeadingNull()) Then
                lblAboutMeHeading.Text = currentRow.AboutMe_Heading
                lblAboutMeHeading.Visible = True
            End If


            If (Not currentRow.IsAboutMe_DescribeYourselfNull()) Then
                lblAboutMeDescr.Text = currentRow.AboutMe_DescribeYourself
            End If
            msg_AboutMeDescrTitle.Visible = (lblAboutMeDescr.Text.Trim() <> "")


            If (Not currentRow.IsAboutMe_DescribeAnIdealFirstDateNull()) Then
                lblFirstDateExpectationDescr.Text = currentRow.AboutMe_DescribeAnIdealFirstDate
            End If
            msg_FirstDateExpectationDescrTitle.Visible = (lblFirstDateExpectationDescr.Text.Trim() <> "")


            SetLocationText(currentRow)
            If (Not currentRow.IsBirthdayNull()) Then
                lblAge.Text = Me.CurrentPageData.GetCustomString("lblAgeYearsOld")
                lblAge.Text = lblAge.Text.Replace("###AGE###", ProfileHelper.GetCurrentAge(currentRow.Birthday))

                Dim zodiac As String = globalStrings.GetCustomString("Zodiac" & ProfileHelper.ZodiacName(currentRow.Birthday), Session("LAGID"))
                lblZwdio.Text = String.Format("({0})", zodiac)
            End If

            If (Not currentRow.IsPersonalInfo_HeightIDNull()) Then
                lblHeight.Text = ProfileHelper.GetHeightString(currentRow.PersonalInfo_HeightID, Me.Session("LagID"))
            End If


            If (Not currentRow.IsPersonalInfo_BodyTypeIDNull()) Then
                lblBodyType.Text = ProfileHelper.GetBodyTypeString(currentRow.PersonalInfo_BodyTypeID, Me.Session("LagID"))
            End If


            If (Not currentRow.IsPersonalInfo_HairColorIDNull()) Then
                lblHairClr.Text = ProfileHelper.GetHairColorString(currentRow.PersonalInfo_HairColorID, Me.Session("LagID"))
            End If


            If (Not currentRow.IsPersonalInfo_EyeColorIDNull()) Then
                lblEyeClr.Text = ProfileHelper.GetEyeColorString(currentRow.PersonalInfo_EyeColorID, Me.Session("LagID"))
            End If


            If (Not currentRow.IsOtherDetails_EducationIDNull()) Then
                lblEducation.Text = ProfileHelper.GetEducationString(currentRow.OtherDetails_EducationID, Me.Session("LagID"))
            End If


            If (Not currentRow.IsPersonalInfo_ChildrenIDNull()) Then
                lblChildren.Text = ProfileHelper.GetChildrenNumberString(currentRow.PersonalInfo_ChildrenID, Me.Session("LagID"))
            End If


            If (Not currentRow.IsPersonalInfo_EthnicityIDNull()) Then
                lblEthnicity.Text = ProfileHelper.GetEthnicityString(currentRow.PersonalInfo_EthnicityID, Me.Session("LagID"))
            End If


            If (Not currentRow.IsPersonalInfo_ReligionIDNull()) Then
                lblReligion.Text = ProfileHelper.GetReligionString(currentRow.PersonalInfo_ReligionID, Me.Session("LagID"))
            End If


            If (Not currentRow.IsPersonalInfo_SmokingHabitIDNull()) Then
                lblSmoking.Text = ProfileHelper.GetSmokingString(currentRow.PersonalInfo_SmokingHabitID, Me.Session("LagID"))
            End If


            If (Not currentRow.IsPersonalInfo_DrinkingHabitIDNull()) Then
                lblDrinking.Text = ProfileHelper.GetDrinkingString(currentRow.PersonalInfo_DrinkingHabitID, Me.Session("LagID"))
            End If


            If (Not currentRow.IsLastLoginDateTimeNull()) Then
                lblLastLoginDT.Text = currentRow.LastLoginDateTime.ToLocalTime().ToString("d MMM, yyyy H:mm")
            End If

            If (Not currentRow.IsOtherDetails_OccupationNull()) Then
                lblOccupation.Text = currentRow.OtherDetails_Occupation
            End If


            If (Not currentRow.IsOtherDetails_AnnualIncomeIDNull()) Then
                lblIncomeLevel.Text = ProfileHelper.GetIncomeString(currentRow.OtherDetails_AnnualIncomeID, Me.Session("LagID"))
            End If


            If (Not currentRow.IsOtherDetails_NetWorthIDNull()) Then
                lblNetWorth.Text = ProfileHelper.GetNetWorthString(currentRow.OtherDetails_NetWorthID, Me.Session("LagID"))
            End If


            If (Not currentRow.IsLookingFor_RelationshipStatusIDNull()) Then
                lblRelationshipStatus.Text = ProfileHelper.GetRelationshipStatusString(currentRow.LookingFor_RelationshipStatusID, Me.Session("LagID"))
            End If


            lblLookingToMeet.Text = ""
            If (Not currentRow.IsLookingFor_ToMeetMaleIDNull() AndAlso currentRow.LookingFor_ToMeetMaleID) Then
                lblLookingToMeet.Text = lblLookingToMeet.Text & ProfileHelper.GetLookingToMeet_Male_String(Me.Session("LagID")) & ", "
            End If

            If (Not currentRow.IsLookingFor_ToMeetFemaleIDNull() AndAlso currentRow.LookingFor_ToMeetFemaleID) Then
                lblLookingToMeet.Text = lblLookingToMeet.Text & ProfileHelper.GetLookingToMeet_Female_String(Me.Session("LagID")) & ", "
            End If

            lblLookingToMeet.Text = lblLookingToMeet.Text.TrimEnd(" "c, ","c)



            Try

                Using _dt As New DataTable


                    _dt.Columns.Add("TypeOfDateText")

                    Dim i As Integer = 0
                    For i = 0 To (Lists.gDSLists.EUS_LISTS_TypeOfDating.Rows.Count - 1)

                        'While i < Lists.gDSLists.EUS_LISTS_TypeOfDating.Rows.Count
                        Try

                            Select Case Lists.gDSLists.EUS_LISTS_TypeOfDating.Rows(i)("TypeOfDatingId")
                                Case TypeOfDatingEnum.AdultDating_Casual
                                    If (Not currentRow.IsLookingFor_TypeOfDating_AdultDating_CasualNull() AndAlso currentRow.LookingFor_TypeOfDating_AdultDating_Casual = True) Then
                                        Dim dr As DataRow = _dt.NewRow()
                                        dr("TypeOfDateText") = Lists.gDSLists.EUS_LISTS_TypeOfDating.Rows(i)(GetLag())
                                        _dt.Rows.Add(dr)
                                    End If

                                Case TypeOfDatingEnum.Friendship
                                    If (Not currentRow.IsLookingFor_TypeOfDating_FriendshipNull() AndAlso currentRow.LookingFor_TypeOfDating_Friendship = True) Then
                                        Dim dr As DataRow = _dt.NewRow()
                                        dr("TypeOfDateText") = Lists.gDSLists.EUS_LISTS_TypeOfDating.Rows(i)(GetLag())
                                        _dt.Rows.Add(dr)
                                    End If

                                Case TypeOfDatingEnum.LongTermRelationship
                                    If (Not currentRow.IsLookingFor_TypeOfDating_LongTermRelationshipNull() AndAlso currentRow.LookingFor_TypeOfDating_LongTermRelationship = True) Then
                                        Dim dr As DataRow = _dt.NewRow()
                                        dr("TypeOfDateText") = Lists.gDSLists.EUS_LISTS_TypeOfDating.Rows(i)(GetLag())
                                        _dt.Rows.Add(dr)
                                    End If

                                Case TypeOfDatingEnum.MarriedDating
                                    If (Not currentRow.IsLookingFor_TypeOfDating_MarriedDatingNull() AndAlso currentRow.LookingFor_TypeOfDating_MarriedDating = True) Then
                                        Dim dr As DataRow = _dt.NewRow()
                                        dr("TypeOfDateText") = Lists.gDSLists.EUS_LISTS_TypeOfDating.Rows(i)(GetLag())
                                        _dt.Rows.Add(dr)
                                    End If

                                Case TypeOfDatingEnum.MutuallyBeneficialArrangements
                                    If (Not currentRow.IsLookingFor_TypeOfDating_MutuallyBeneficialArrangementsNull() AndAlso currentRow.LookingFor_TypeOfDating_MutuallyBeneficialArrangements = True) Then
                                        Dim dr As DataRow = _dt.NewRow()
                                        dr("TypeOfDateText") = Lists.gDSLists.EUS_LISTS_TypeOfDating.Rows(i)(GetLag())
                                        _dt.Rows.Add(dr)
                                    End If

                                Case TypeOfDatingEnum.ShortTermRelationship
                                    If (Not currentRow.IsLookingFor_TypeOfDating_ShortTermRelationshipNull() AndAlso currentRow.LookingFor_TypeOfDating_ShortTermRelationship = True) Then
                                        Dim dr As DataRow = _dt.NewRow()
                                        dr("TypeOfDateText") = Lists.gDSLists.EUS_LISTS_TypeOfDating.Rows(i)(GetLag())
                                        _dt.Rows.Add(dr)
                                    End If

                            End Select

                        Catch ex As Exception
                            WebErrorMessageBox(Me, ex, "")
                        End Try

                        '    i = i + 1
                        'End While
                    Next

                    lvTypeOfDate.DataSource = _dt
                    lvTypeOfDate.DataBind()
                End Using
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try

            'If (lvTypeOfDate.Items.Count = 0) Then
            '    msg_InterestedIn.Visible = False
            'Else
            '    msg_InterestedIn.Visible = True
            'End If



        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Private Sub SetLocationText(currentRow As EUS_ProfilesRow)
        Dim _locationStrCity = ""
        Dim _locationStrRegion = ""
        Dim _locationStrCountry = ""
        Dim _birthDay = ""


        If (Not currentRow.IsCityNull()) Then
            _locationStrCity = currentRow.City
        End If

        If (Not currentRow.Is_RegionNull()) Then
            _locationStrRegion = currentRow._Region
        End If

        If (Not currentRow.IsCountryNull()) Then
            _locationStrCountry = ProfileHelper.GetCountryName(currentRow.Country)
        End If



        If (Not currentRow.IsBirthdayNull()) Then
            Dim bornon As String = Me.CurrentPageData.GetCustomString("BornOn")
            bornon = bornon.Replace("###DATE###", currentRow.Birthday.ToString("d MMM, yyyy"))
            _birthDay = bornon
        End If
        lblFrom_LocationBirth.Text = ""

        If (_locationStrCity <> "") Then
            lblFrom_LocationBirth.Text = _locationStrCity
        End If
        If (_locationStrRegion <> "" AndAlso _locationStrCity <> "") Then
            lblFrom_LocationBirth.Text = lblFrom_LocationBirth.Text & ", "
        End If
        If (_locationStrRegion <> "") Then
            lblFrom_LocationBirth.Text = lblFrom_LocationBirth.Text & _locationStrRegion
        End If
        If (_locationStrRegion <> "" AndAlso _locationStrCountry <> "") Then
            lblFrom_LocationBirth.Text = lblFrom_LocationBirth.Text & ", "
        End If
        If (_locationStrCountry <> "") Then
            Dim fromStr = globalStrings.GetCustomString("FromWord")
            lblFrom_LocationBirth.Text = fromStr & ":&nbsp;" & lblFrom_LocationBirth.Text & _locationStrCountry
        End If


        If (lblFrom_LocationBirth.Text <> "" AndAlso _birthDay <> "") Then
            lblFrom_LocationBirth.Text = lblFrom_LocationBirth.Text & "&nbsp;&nbsp;&nbsp;"
        End If
        If (_birthDay <> "") Then
            lblFrom_LocationBirth.Text = lblFrom_LocationBirth.Text & _birthDay
        End If

        'If (Not String.IsNullOrEmpty(lblPositionTxt)) Then
        '    lblFrom_LocationBirth.Text = "From: " & lblPositionTxt.Trim(" "c, ","c)
        'End If

        'If (lblLocation.Text.Trim() = "") Then liLocationString.Visible = False
    End Sub


    Public Function HasOffer(userIdOfferReceiver As Integer, userIdWhoDid As Integer) As Boolean
        Dim val As Boolean
        Try

            If (clsUserDoes.GetLastOffer(userIdOfferReceiver, userIdWhoDid) IsNot Nothing) Then
                val = True
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
        Return val
    End Function


    Protected Sub lnkFavorite_Command(sender As Object, e As System.Web.UI.WebControls.CommandEventArgs) Handles lnkFavoriteF.Command, lnkFavoriteM.Command
        Try
            Dim _userId As Integer = CType(e.CommandArgument, Integer)
            If (_userId > 0) Then
                clsUserDoes.MarkAsFavorite(_userId, Me.MasterProfileId)
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Try
            LoadView()
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Protected Sub lnkMakeOffer_Command(sender As Object, e As System.Web.UI.WebControls.CommandEventArgs) Handles lnkMakeOfferF.Command, lnkMakeOfferM.Command
        Try
            Dim _userId As Integer = CType(e.CommandArgument, Integer)
            If (_userId > 0) Then

                Dim currentUserHasPhotos As Boolean = clsUserDoes.HasPhotos(Me.MasterProfileId, Me.MirrorProfileId)
                If (currentUserHasPhotos) Then

                    Dim CreateOfferUrl As String = ""

                    Dim rec As EUS_Offer = clsUserDoes.GetAnyWinkPending(_userId, Me.MasterProfileId)
                    Dim offerId As Integer = 0
                    If (rec IsNot Nothing) Then
                        offerId = rec.OfferID
                        CreateOfferUrl = ResolveUrl("~/Members/CreateOffer.aspx?offer=" & offerId)
                    Else

                        Dim prof As EUS_Profile = (From itm In Me.CMSDBDataContext.EUS_Profiles
                                                  Where itm.ProfileID = _userId AndAlso _
                                                        itm.IsMaster = True
                                                  Select itm).FirstOrDefault()


                        CreateOfferUrl = ResolveUrl("~/Members/CreateOffer.aspx?offerto=" & prof.LoginName)
                    End If


                    Response.Redirect(CreateOfferUrl, False)
                    HttpContext.Current.ApplicationInstance.CompleteRequest()
                    'Dim clsUserDoes As New clsUserDoes
                    'clsUserDoes.Make(_userId, Me.MasterProfileId)
                    'lnkMakeOffer.Visible = False
                Else
                    Response.Redirect(ResolveUrl("~/Members/Photos.aspx"), False)
                    HttpContext.Current.ApplicationInstance.CompleteRequest()
                End If
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Protected Sub lnkBlock_Command(sender As Object, e As System.Web.UI.WebControls.CommandEventArgs) Handles lnkBlock.Command
        Try
            Dim _userId As Integer = CType(e.CommandArgument, Integer)
            If (_userId > 0) Then
                clsUserDoes.MarkAsBlocked(_userId, Me.MasterProfileId)
                lnkBlock.Visible = False
                ltrSeparator.Visible = False
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Try
            LoadView()
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Protected Sub lnkUnFavorite_Command(sender As Object, e As System.Web.UI.WebControls.CommandEventArgs) Handles lnkUnFavoriteF.Command, lnkUnFavoriteM.Command
        Try
            Dim _userId As Integer = CType(e.CommandArgument, Integer)
            If (_userId > 0) Then
                clsUserDoes.MarkAsUnfavorite(_userId, Me.MasterProfileId)
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Try
            LoadView()
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub




    Protected Sub lnkWink_Command(sender As Object, e As System.Web.UI.WebControls.CommandEventArgs) Handles lnkWinkF.Command, lnkWinkM.Command
        Try

            If (Not String.IsNullOrEmpty(e.CommandArgument)) Then
                Dim currentUserHasPhotos As Boolean = clsUserDoes.HasPhotos(Me.MasterProfileId, Me.MirrorProfileId)
                If (currentUserHasPhotos) Then

                    Dim ToProfileID As Integer
                    Integer.TryParse(e.CommandArgument.ToString(), ToProfileID)

                    clsUserDoes.SendWink(ToProfileID, Me.MasterProfileId)
                Else
                    Response.Redirect(ResolveUrl("~/Members/Photos.aspx"), False)
                    HttpContext.Current.ApplicationInstance.CompleteRequest()
                End If
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Try
            LoadView()
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Protected Sub lnkUnWink_Command(sender As Object, e As System.Web.UI.WebControls.CommandEventArgs) Handles lnkUnWinkF.Command, lnkUnWinkM.Command
        Try
            If (Not String.IsNullOrEmpty(e.CommandArgument)) Then

                Dim ToProfileID As Integer
                Integer.TryParse(e.CommandArgument.ToString(), ToProfileID)

                clsUserDoes.CancelPendingWink(ToProfileID, Me.MasterProfileId)
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Try
            LoadView()
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Function WriteActionTopRowStyle() As String
        If (pPhotosLevel.Visible = True) Then Return "style='border-bottom:none;'"

        Return ""
    End Function

    Function WriteUserInsightClass() As String
        If (pPhotosLevel.Visible = True) Then Return "p_actions_insight" Else Return "p_actions_insight_onerow"

        Return ""
    End Function

    Protected Sub cbpnlPhotos_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cbpnlPhotos.Callback

        Try
            Dim lvl As Integer = cbPhotosLevel.SelectedItem.Value 'e.Parameter.Replace("lvl_", "")
            Try

                Dim phtLvl As EUS_ProfilePhotosLevel = GetEUS_ProfilePhotosLevel()

                If (phtLvl IsNot Nothing) Then
                    phtLvl.PhotoLevelID = lvl
                Else
                    phtLvl = New EUS_ProfilePhotosLevel()
                    phtLvl.FromProfileID = Me.MasterProfileId
                    phtLvl.PhotoLevelID = lvl
                    phtLvl.ToProfileID = Me.UserIdInView
                    phtLvl.DateTimeCreated = Date.UtcNow
                    Me.CMSDBDataContext.EUS_ProfilePhotosLevels.InsertOnSubmit(phtLvl)
                End If

                Me.CMSDBDataContext.SubmitChanges()

            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try

            'LoadPhotos()

            LoadLAG()
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


    Private Function GetEUS_ProfilePhotosLevel() As EUS_ProfilePhotosLevel
        Dim phtLvl As EUS_ProfilePhotosLevel = Nothing
        Try

            phtLvl = (From itm In Me.CMSDBDataContext.EUS_ProfilePhotosLevels
                                                   Where itm.FromProfileID = Me.MasterProfileId AndAlso itm.ToProfileID = Me.UserIdInView
                                                   Select itm).FirstOrDefault()

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
        Return phtLvl
    End Function



    Protected Sub lnkUnlockOffer_Command(sender As Object, e As System.Web.UI.WebControls.CommandEventArgs) Handles lnkUnlockOfferF.Command, lnkUnlockOfferM.Command
        Try
            If (e.CommandName = OfferControlCommandEnum.UNLOCKOFFER) Then

                Dim offerID As Integer
                Integer.TryParse(e.CommandArgument.ToString(), offerID)
                Dim success As Boolean
                If (AllowUnlimited) Then
                    success = clsUserDoes.PerformOfferUnlock(offerID, Me.MasterProfileId)
                Else
                    success = True
                End If

                If (success) Then
                    LoadView()
                End If
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

End Class
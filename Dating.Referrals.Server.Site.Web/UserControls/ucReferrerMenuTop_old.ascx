﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucReferrerMenuTop_old.ascx.vb"
    Inherits="Dating.Server.Site.Web.ucReferrerMenuTop" %>
<dx:ASPxMenu ID="mnu1" runat="server" AutoSeparators="RootOnly" 
    CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css" CssPostfix="PlasticBlue" 
    ItemAutoWidth="False" 
    SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css" Width="100%">
    <Items>
        <dx:MenuItem NavigateUrl="~/Members/Referrer.aspx" Text="Dashboard">
        </dx:MenuItem>
        <dx:MenuItem NavigateUrl="javascript:void(0);" Text="Tasks" DropDownMode="True">
            <Items>
                <dx:MenuItem NavigateUrl="~/Members/ReferrerFamilyTree.aspx" 
                    Text="Οικογενειακό δένδρο">
                </dx:MenuItem>
                <dx:MenuItem NavigateUrl="~/Members/ReferrerAnalytics.aspx" 
                    Text="Αναλυτική κατάσταση">
                </dx:MenuItem>
                <dx:MenuItem NavigateUrl="~/Members/ReferrerSupplies.aspx" 
                    Text="Προμήθειες μου">
                </dx:MenuItem>
                <dx:MenuItem NavigateUrl="~/Members/ReferrerStatistics.aspx" Text="Στατιστικά">
                </dx:MenuItem>
                <dx:MenuItem NavigateUrl="~/Members/ReferrerPayments.aspx" Text="Πληρωμές μου">
                </dx:MenuItem>
            </Items>
        </dx:MenuItem>
        <dx:MenuItem DropDownMode="True" NavigateUrl="javascript:void(0);" Text="Options">
            <Items>
                <dx:MenuItem NavigateUrl="~/Members/ReferrerSettings.aspx" Text="Ρυθμίσεις">
                </dx:MenuItem>
                <dx:MenuItem NavigateUrl="~/Members/ReferrerPercents.aspx" Text="Ποσοστά μου">
                </dx:MenuItem>
            </Items>
        </dx:MenuItem>
    </Items>
    <LoadingPanelImage Url="~/App_Themes/PlasticBlue/Web/Loading.gif">
    </LoadingPanelImage>
    <ItemSubMenuOffset FirstItemY="-1" LastItemY="-1" Y="-1" />
    <RootItemSubMenuOffset FirstItemX="1" LastItemX="1" X="1" />
    <ItemStyle DropDownButtonSpacing="13px" ToolbarDropDownButtonSpacing="5px" 
        ToolbarPopOutImageSpacing="5px" />
    <SubMenuStyle GutterWidth="0px" />
</dx:ASPxMenu>
<%--<dx:ASPxMenu ID="mnu2" runat="server">
    <Items>
    </Items>
</dx:ASPxMenu>--%>

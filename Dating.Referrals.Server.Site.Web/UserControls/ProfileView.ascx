﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ProfileView.ascx.vb" Inherits="Dating.Referrals.Server.Site.Web.ProfileView" %>
<%@ Register assembly="DevExpress.Web.v13.1, Version=13.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxCallbackPanel" tagprefix="dx" %>
<div class="p_filter p_stats" id="filter" style="padding-bottom:10px;">
										
		<div class="profile_info" style="vertical-align:text-bottom;">
            <h1 class="lfloat" style="background-image:url('<%= Page.ResolveUrl("~/Images/profilename.png") %>'); background-repeat:repeat-x;background-position:bottom left;padding-bottom:3px;"><dx:ASPxLabel ID="lblLogin" runat="server" EncodeHtml="False" Font-Size="16px" /></h1>
            <div class="lfloat" style="margin-left:10px;padding-top:3px;"><dx:ASPxLabel ID="lblStatus" runat="server" EncodeHtml="False" /></div>
 		    <div class="clear"></div>
           <dx:ASPxLabel ID="lblFrom_LocationBirth" runat="server" Text="From:" EncodeHtml="False" CssClass="lfloat" style="padding-top:5px ;" />
            <asp:Image ID="imgOnline" runat="server" ImageUrl="../images/online_now.png" Visible="false" style="position:absolute;right:10px;top:10px;" />
        </div>
		<div class="clear"></div>
	    <div class="p_search" id="profileOwner" runat="server" style="padding:5px 0px 10px 5px;">
			<dx:ASPxLabel ID="msg_LastUpdatedDT" runat="server" EncodeHtml="False" />&nbsp;<dx:ASPxLabel ID="lblLastUpdated" runat="server" EncodeHtml="False" />
	</div>
</div>
<div class="clear"></div>

<asp:ListView ID="lvPhotos" runat="server">
    <LayoutTemplate>
<div class="profile_gallery p_filter">
    <div runat="server" id="itemPlaceholder"></div>
	<div class="clear"></div>
</div>
    </LayoutTemplate>
    <ItemTemplate>
    <div class="d_big_photo">
        <a rel="prof_group" class="js fancybox" href="<%# Eval("ImageUrl") %>">
            <img src="<%# Eval("ImageThumbUrl") %>" /></a></div>
    </ItemTemplate>
</asp:ListView>
<%-- alt="<%# Eval("ImageUploadDate") %>" title="<%# Eval("ImageUploadDate") %>"--%>
<div class="p_wrap">
    <table cellpadding="0" cellspacing="0" width="100%" style="background:#f9f9f9;">
    <tr>
        <td>
            <div class="p_actions" <%= WriteActionTopRowStyle() %>>
                <div class="">
                <asp:MultiView ID="mvGender" runat="server" ActiveViewIndex="0">
                    <asp:View ID="vwMyProfile" runat="server">
                        <asp:HyperLink ID="lnkEditProf" CssClass="btn btn-small" runat="server" NavigateUrl="~/Members/Profile.aspx?do=edit" />
                    </asp:View>

                    <asp:View ID="vwFemale" runat="server">
                        <asp:LinkButton ID="lnkWinkF" CssClass="btn btn-small lfloat" runat="server" />
                        <asp:LinkButton ID="lnkUnWinkF" CssClass="btn btn-small disabled lfloat" Enabled="false" 
                            runat="server"  />
                        <asp:HyperLink ID="lnkMessageF" CssClass="btn btn-small lfloat" runat="server" NavigateUrl="#" >[lnkMessageF]</asp:HyperLink>
                        <asp:LinkButton ID="lnkMakeOfferF" CssClass="btn btn-small lfloat" runat="server"  />
                        <asp:LinkButton ID="lnkFavoriteF" CssClass="btn btn-small lfloat" runat="server" />
                        <asp:LinkButton ID="lnkUnlockOfferF" CssClass="btn btn-small btn-primary lfloat" 
                            runat="server" />
                        <asp:LinkButton ID="lnkUnFavoriteF" CssClass="btn btn-small lfloat" runat="server" />
                        <asp:Label ID="divTooltipholderF" CssClass="icon_tooltipholder" runat="server" 
                            Visible="False">
                            <asp:HyperLink ID="lnkTooltipUnlockF" CssClass="tooltiptipsy-s" runat="server" NavigateUrl="javascript:void(0);">
                                <img runat="server" id="icon_tipF" src="~/Images/icon_tip16.png" alt=""/></asp:HyperLink>
                        </asp:Label>
                    </asp:View>

                    <asp:View ID="vwMale" runat="server">
                        <asp:LinkButton ID="lnkWinkM" CssClass="btn btn-small lfloat" runat="server" />
                        <asp:LinkButton ID="lnkUnWinkM" CssClass="btn btn-small disabled lfloat" Enabled="false" runat="server"  />
                        <asp:HyperLink ID="lnkMessageM" CssClass="btn btn-small lfloat" runat="server" NavigateUrl="#" Visible="false" />
                        <div class="btn-group lfloat" ID="divActionsMenu" runat="server" visible="false">
                            <a class="btn btn-small dropdown-toggle" data-toggle="dropdown" href="#"><asp:Literal ID="msg_ActionsText" runat="server"/> <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li id="liActsSendMsgOnce" runat="server">
                                    <asp:HyperLink ID="lnkSendMsgOnce" runat="server" NavigateUrl="">SendMessageOnceText</asp:HyperLink></li>
                                <li id="liActsSendMsgMany" runat="server" visible="false">
                                    <asp:HyperLink ID="lnkSendMsgMany" runat="server" NavigateUrl="">SendMessageManyText</asp:HyperLink></li>
                            </ul>
                        </div>
                        <asp:LinkButton ID="lnkMakeOfferM" CssClass="btn btn-small lfloat" runat="server"  />
                        <asp:LinkButton ID="lnkFavoriteM" CssClass="btn btn-small lfloat" runat="server" />
                        <asp:LinkButton ID="lnkUnlockOfferM" CssClass="btn btn-small btn-primary lfloat" runat="server" />
                        <asp:LinkButton ID="lnkUnFavoriteM" CssClass="btn btn-small lfloat" runat="server" />
                        <asp:Label ID="divTooltipholderM" CssClass="icon_tooltipholder lfloat" runat="server" Visible="false">
                            <asp:HyperLink ID="lnkTooltipUnlockM" CssClass="tooltiptipsy-s" runat="server" NavigateUrl="javascript:void(0);">
                                <img runat="server" id="icon_tipM" src="~/Images/icon_tip16.png" alt=""/></asp:HyperLink>
                        </asp:Label>
                    </asp:View>
                </asp:MultiView>
                <div class="clear"></div>
                </div>
                <p>
                </p>
                <div id="pPhotosLevel" runat="server" class="p_actions lfloat" style="width:430px;padding:8px 5px 5px 8px;">
	
<script type="text/javascript">
    // <![CDATA[
    function OnPhotosDisplayLevelChanged(s, e) {
        LoadingPanel.Hide();
        cbpnlPhotos.PerformCallback(); //"lvl_" + s.GetValue());
        LoadingPanel.Show();
    }


    function CloseSharingPopUp() {
        try {
            var win = '<%= setLevelPopUp.ClientID %>';
            window[win].DoHideWindow(0);
        }
        catch (e) { }
    }
    // ]]> 
</script>

        <dx:ASPxCallbackPanel ID="cbpnlPhotos" runat="server" ShowLoadingPanel="False" 
                ShowLoadingPanelImage="False" ClientInstanceName="cbpnlPhotos">
                                <ClientSideEvents EndCallback="function(s, e) {
	LoadingPanel.Hide();
}" CallbackError="function(s, e) {
	LoadingPanel.Hide();
}" />
<ClientSideEvents EndCallback="function(s, e) {
	LoadingPanel.Hide();
    CloseSharingPopUp();
}" CallbackError="function(s, e) {
	LoadingPanel.Hide();
}"></ClientSideEvents>
            <PanelCollection>
                <dx:PanelContent runat="server" SupportsDisabledAttribute="True">

                    <div class="" style="padding:5px 2px;">
                        <%--<dx:ASPxButton ID="lblPhotoLevel" runat="server" Text="Επίπεδο&nbsp;φωτογραφίας"
                            EncodeHtml="False" Height="31px"
                            CssFilePath="~/App_Themes/SoftOrange/{0}/styles.css" 
                            CssPostfix="SoftOrange" Font-Bold="True" 
                            SpriteCssFilePath="~/App_Themes/SoftOrange/{0}/sprite.css" 
                            PostBackUrl="~/Register.aspx" UseSubmitBehavior="false">
<ClientSideEvents Click="function(s, e) { e.processOnServer=false; }"></ClientSideEvents>

                            <Paddings Padding="0px"></Paddings>
                            <Border BorderWidth="1px"></Border>
                            <ClientSideEvents Click="function(s, e) { e.processOnServer=false; }" />
                        </dx:ASPxButton>--%>
                       <asp:Hyperlink ID="lblPhotoLevel" runat="server" Text="Επίπεδο φωτογραφίας" CssClass="lblPhotoLevel" NavigateUrl="javascript: void(0);"/>
                        <%--<asp:Image
                             ID="imgPhotoLevelInfo" runat="server" ImageUrl="~/Images/icon_tip.png" />--%><%--<asp:Label ID="lblPhotoLevel" runat="server" Text="Επίπεδο φωτογραφίας"/>--%>
 <%--<dx:ASPxPopupControl SkinID="None" EncodeHtml="False" ID="popupWindows" runat="server"
        EnableViewState="False" EnableHotTrack="False" PopupHorizontalAlign="RightSides"
        PopupVerticalAlign="Above" PopupHorizontalOffset="1" PopupVerticalOffset="-4"
        EnableHierarchyRecreation="True" CloseAction="MouseOut" HeaderText="Επίπεδο φωτογραφίας" 
        PopupAction="MouseOver" RenderMode="Lightweight">
        <ClientSideEvents />
        <Windows>
            <dx:PopupWindow PopupElementID="lblPhotoLevel" CloseAction="MouseOut" PopupAction="MouseOver">
                <ContentCollection>
                    <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server" SupportsDisabledAttribute="True">
                         <asp:Label ID="lblAllowAccessPhotosLevel" runat="server" Text=""/>
                    </dx:PopupControlContentControl>
                </ContentCollection>
            </dx:PopupWindow>
            <dx:PopupWindow PopupElementID="imgPhotoLevelInfo" CloseAction="MouseOut" PopupAction="MouseOver">
                <ContentCollection>
                    <dx:PopupControlContentControl ID="PopupControlContentControl2" runat="server" SupportsDisabledAttribute="True">
                         <asp:Label ID="lblAllowAccessPhotosLevelIcon" runat="server" Text=""/>
                    </dx:PopupControlContentControl>
                </ContentCollection>
            </dx:PopupWindow>
        </Windows>
        <ContentCollection>
            <dx:PopupControlContentControl runat="server" SupportsDisabledAttribute="True">
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>--%>
                <dx:ASPxPopupControl ID="setLevelPopUp" runat="server" AllowResize="True" 
                    PopupHorizontalAlign="WindowCenter"
                    PopupVerticalAlign="WindowCenter" 
                    ShowSizeGrip="True" 
                    CloseAction="OuterMouseClick" AllowDragging="false" Modal="false">
                    <Windows>
                        <dx:PopupWindow Modal="false" Name="setLevelPopUpWin"
                             Height="150px" Width="400px" PopupElementID="lblPhotoLevel">
                            <ContentStyle HorizontalAlign="Left" VerticalAlign="Middle">
                                <backgroundimage imageurl="../images/bg.jpg" />
                                <Paddings Padding="10px" />
<Paddings Padding="10px"></Paddings>

<BackgroundImage ImageUrl="../images/bg.jpg"></BackgroundImage>
                            </ContentStyle>
                            <ContentCollection>
                                <dx:PopupControlContentControl ID="PopupControlContentControl3" runat="server" SupportsDisabledAttribute="True">
<asp:Label ID="lblAllowAccessPhotosLevel" runat="server" Text=""/><br /><br />

                    <div align="center" class=" mediumCombo" style="margin:5px auto;">
                        <dx:ASPxRadioButtonList ID="cbPhotosLevel" runat="server" Border-BorderStyle="None" Border-BorderWidth="0px">
<Border BorderWidth="0px" BorderStyle="None"></Border>
                        </dx:ASPxRadioButtonList> 
                        <%--<dx:ASPxComboBox ID="cbPhotosLevel" runat="server" 
                            PopupHorizontalAlign="RightSides">
<ClientSideEvents SelectedIndexChanged="function(s, e) {  OnPhotosDisplayLevelChanged(s,e); }" EndCallback="function(s, e) {
	LoadingPanel.Hide();
}" />
<ClientSideEvents EndCallback="function(s, e) {
	LoadingPanel.Hide();
}" SelectedIndexChanged="function(s, e) {  OnPhotosDisplayLevelChanged(s,e); }"></ClientSideEvents>
                        </dx:ASPxComboBox>--%>
                    </div>
                    <div align="center" style="margin:0 auto;">
<dx:ASPxButton ID="btnSave" runat="server" Text="Save"
    EncodeHtml="False"
    CssClass="btn btn-large btn-primary" Native="True">
<ClientSideEvents Click="function(s, e) {  
    OnPhotosDisplayLevelChanged(s,e); 
    e.processOnServer=false;
}"  />
<ClientSideEvents Click="function(s, e) {  
    OnPhotosDisplayLevelChanged(s,e); 
    e.processOnServer=false;
}"></ClientSideEvents>
</dx:ASPxButton>
                    </div>
                                </dx:PopupControlContentControl>
                            </ContentCollection>
                        </dx:PopupWindow>
                    </Windows>
                    <CloseButtonImage Url="~/Images/spacer10.png" Height="17px" Width="17px">
                    </CloseButtonImage>
                    <SizeGripImage Height="40px" Url="~/Images/resize.png" Width="40px">
                    </SizeGripImage>
                    <CloseButtonStyle>
                        <HoverStyle>
                            <BackgroundImage HorizontalPosition="center" ImageUrl="~/Images/close-button1.png"
                                Repeat="NoRepeat" VerticalPosition="center" />
<BackgroundImage ImageUrl="~/Images/close-button1.png" Repeat="NoRepeat" HorizontalPosition="center" VerticalPosition="center"></BackgroundImage>
                        </HoverStyle>
                        <BackgroundImage HorizontalPosition="center" ImageUrl="~/Images/close-button2.png"
                            Repeat="NoRepeat" VerticalPosition="center" />

<BackgroundImage ImageUrl="~/Images/close-button2.png" Repeat="NoRepeat" HorizontalPosition="center" VerticalPosition="center"></BackgroundImage>
                    </CloseButtonStyle>
                    <ContentStyle>
                        <Paddings Padding="0px" />
<Paddings Padding="0px"></Paddings>
                    </ContentStyle>
                    <HeaderStyle BackColor="#35619F" Font-Bold="True" ForeColor="White">
                        <Paddings PaddingBottom="10px" PaddingTop="10px" />
<Paddings PaddingTop="10px" PaddingBottom="10px"></Paddings>
                    </HeaderStyle>
                    <ModalBackgroundStyle CssClass="modalPopup">
                    </ModalBackgroundStyle>
                    <ContentCollection>
                        <dx:PopupControlContentControl ID="PopupControlContentControl4" runat="server" SupportsDisabledAttribute="True">
                        </dx:PopupControlContentControl>
                    </ContentCollection>
                </dx:ASPxPopupControl>
                    </div>
<%--                    <div class=" mediumCombo">
                        <dx:ASPxComboBox ID="cbPhotosLevel" runat="server" 
                            PopupHorizontalAlign="RightSides">
<ClientSideEvents SelectedIndexChanged="function(s, e) {  OnPhotosDisplayLevelChanged(s,e); }" EndCallback="function(s, e) {
	LoadingPanel.Hide();
}" />
<ClientSideEvents EndCallback="function(s, e) {
	LoadingPanel.Hide();
}" SelectedIndexChanged="function(s, e) {  OnPhotosDisplayLevelChanged(s,e); }"></ClientSideEvents>
                        </dx:ASPxComboBox>
                    </div>
--%>                </dx:PanelContent>
            </PanelCollection>
        </dx:ASPxCallbackPanel>
                </div>
	        </div>
            <div class="<%= WriteUserInsightClass() %>">
			    <h3 style="font-size:12px;"><asp:Literal ID="msg_UserInsightTitle" runat="server" /></h3>
			    <p id="pLastLoggedInDT" runat="server" ><asp:Literal ID="msg_LastLoggedInDT" runat="server" /> <asp:Label ID="lblLastLoginDT" runat="server" style="white-space:nowrap;" /></p>							
                <%--<p id="pLastLoggedInFrom" runat="server" visible="false"><asp:Literal ID="msg_LastLoggedInFrom" runat="server" /> <asp:Label ID="lblLastLoginFrom" runat="server" style="white-space:nowrap;" /> </p>--%>
                <p class="p_report" id="pReport" runat="server">
                    <asp:HyperLink ID="lnkReport" runat="server">Report Abuse</asp:HyperLink><asp:Literal
                        ID="ltrSeparator" runat="server"> / </asp:Literal><asp:LinkButton 
                        ID="lnkBlock" runat="server">Block</asp:LinkButton>
                </p>
		    </div>
		    <div class="clear"></div>
        </td>
    </tr>
        <%--<dx:ASPxButton ID="lblPhotoLevel" runat="server" Text="Επίπεδο&nbsp;φωτογραφίας"
                            EncodeHtml="False" Height="31px"
                            CssFilePath="~/App_Themes/SoftOrange/{0}/styles.css" 
                            CssPostfix="SoftOrange" Font-Bold="True" 
                            SpriteCssFilePath="~/App_Themes/SoftOrange/{0}/sprite.css" 
                            PostBackUrl="~/Register.aspx" UseSubmitBehavior="false">
<ClientSideEvents Click="function(s, e) { e.processOnServer=false; }"></ClientSideEvents>

                            <Paddings Padding="0px"></Paddings>
                            <Border BorderWidth="1px"></Border>
                            <ClientSideEvents Click="function(s, e) { e.processOnServer=false; }" />
                        </dx:ASPxButton>--%>
      </table>
  
        <div class="p_top" style="top: -59px;">
		<div class="clear"></div>

		<div class="profile_info">
			<h3 style="width: 310px; display:block;"><asp:Label ID="lblWillTravel" runat="server" /></h3>
			<h2><asp:Label ID="lblAboutMeHeading" runat="server"/></h2>
            <table style="width:100%;">
            <tr>
                <td valign="top"><ul class="defaultStyle">
				    <li><span><asp:Literal ID="msg_Age" runat="server" /></span>&nbsp;<asp:Literal ID="lblAge" runat="server" /> <asp:Literal ID="lblZwdio" runat="server" Text="" /></li>
				    <li><span><asp:Literal ID="msg_BodyType" runat="server" /></span>&nbsp;<asp:Literal ID="lblHeight" runat="server" /> - <asp:Literal ID="lblBodyType" runat="server" /></li>
				    <li><span><asp:Literal ID="msg_HairEyes" runat="server" /></span>&nbsp;<asp:Literal ID="lblHairClr" runat="server" /> / <asp:Literal ID="lblEyeClr" runat="server" /></li>
				    <li><span><asp:Literal ID="msg_Education" runat="server" /></span>&nbsp;<asp:Literal ID="lblEducation" runat="server"  /></li>
				    <li><span><asp:Literal ID="msg_Children" runat="server" /></span>&nbsp;<asp:Literal ID="lblChildren" runat="server"  /></li>
				    <li><span><asp:Literal ID="msg_Ethnicity" runat="server" /></span>&nbsp;<asp:Literal ID="lblEthnicity" runat="server"  /></li>
				    <li><span><asp:Literal ID="msg_Religion" runat="server" /></span>&nbsp;<asp:Literal ID="lblReligion" runat="server"  /></li>
				    <li><span><asp:Literal ID="msg_SmokingDrinking" runat="server" /></span>&nbsp;<asp:Literal ID="lblSmoking" runat="server"  /> / <asp:Literal ID="lblDrinking" runat="server"  /></li>
			    </ul></td>
                <td valign="top" style="width:10px;"><div style="width:10px;">&nbsp;</div></td>
                <td valign="top"><ul class="defaultStyle">
			        <li><span><asp:Literal ID="msg_Occupation" runat="server" /></span>&nbsp;<asp:Literal ID="lblOccupation" runat="server" /></li>
			        <li runat="server" id="liAnnualIncome"><span><asp:Literal ID="msg_IncomeLevel" runat="server" /></span>&nbsp;<asp:Literal ID="lblIncomeLevel" runat="server" /></li>								
			        <li runat="server" id="liNetWorth" visible="false"><span><asp:Literal ID="msg_NetWorth" runat="server" /></span>&nbsp;<asp:Literal ID="lblNetWorth" runat="server" /></li>
			        <li><span><asp:Literal ID="msg_RelationshipStatus" runat="server" /></span>&nbsp;<asp:Literal ID="lblRelationshipStatus" runat="server" /></li>
			        <li><span><asp:Literal ID="msg_LookingToMeet" runat="server" /></span>&nbsp;<asp:Literal ID="lblLookingToMeet" runat="server" /></li>
			        <li><span><asp:Literal ID="msg_InterestedIn" runat="server" /></span> 
                    <asp:ListView ID="lvTypeOfDate" runat="server">
                        <LayoutTemplate>
                        <ul style="padding-left:20px;" class="defaultStyle">
                            <li runat="server" id="itemPlaceholder"></li>
                        </ul>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <li><%# Eval("TypeOfDateText") %></li>
                        </ItemTemplate>
                    </asp:ListView>
			        </li>
		        </ul></td>
            </tr>
            </table>
			
		</div>
		<div class="clear"></div>
	</div>
    <%--<asp:Image
                             ID="imgPhotoLevelInfo" runat="server" ImageUrl="~/Images/icon_tip.png" />--%>
        <div class="p_summary">
		<h2><asp:Literal ID="msg_AboutMeDescrTitle" runat="server" /></h2>
		<p><asp:Literal ID="lblAboutMeDescr" runat="server" /></p>
		<h2><asp:Literal ID="msg_FirstDateExpectationDescrTitle" runat="server" /></h2>
		<p><asp:Literal ID="lblFirstDateExpectationDescr" runat="server" /></p>
		<h2 id="h2MyGiftsTitle" runat="server" visible="false"><asp:Literal ID="msg_MyGiftsTitle" runat="server" /><small><asp:HyperLink ID="cmdHideGifts" runat="server" NavigateUrl="~/Members/Settings.aspx"></asp:HyperLink></small></h2>
		<div class="gift_group_p">
        <asp:ListView ID="lvGifts" runat="server" Visible="false">
            <LayoutTemplate>
            <ul>
                <li runat="server" id="itemPlaceholder"></li>
            </ul>
            </LayoutTemplate>
            <ItemTemplate>
                <li><div><img title="<%# Eval("ImageTitle")%>" alt="<%# Eval("ImageAlt")%>" src="<%# Eval("ImageUrl")%>" /></div></li
            </ItemTemplate>
            </asp:ListView>
		</div>
							
	</div>
    <%--<asp:Label ID="lblPhotoLevel" runat="server" Text="Επίπεδο φωτογραφίας"/>--%>
	
	
	<div class="p_extras profile_info profile_own">
		
	</div>
	<div class="clear"></div>
</div>

<dx:ASPxPopupControl ID="UploadPhotoPopUp" runat="server" ClientInstanceName="UploadPhotoPopUp" 
    AllowResize="True" PopupHorizontalAlign="WindowCenter"
    PopupVerticalAlign="WindowCenter" 
    ShowSizeGrip="True" 
    CloseAction="CloseButton" 
    AllowDragging="True" Width="600px" HeaderText="Photo upload notification" 
    Modal="True">
    <CloseButtonImage Url="~/Images/spacer10.png" Height="17px" Width="17px">
    </CloseButtonImage>
    <SizeGripImage Height="40px" Url="~/Images/resize.png" Width="40px">
    </SizeGripImage>
    <CloseButtonStyle>
        <HoverStyle>
            <BackgroundImage HorizontalPosition="center" ImageUrl="~/Images/close-button1.png"
                Repeat="NoRepeat" VerticalPosition="center" />
        </HoverStyle>
        <BackgroundImage HorizontalPosition="center" ImageUrl="~/Images/close-button2.png"
            Repeat="NoRepeat" VerticalPosition="center" />
    </CloseButtonStyle>
    <ContentStyle HorizontalAlign="Center" VerticalAlign="Middle">
        <Paddings Padding="0px" />
    </ContentStyle>
    <HeaderStyle BackColor="#35619F" Font-Bold="True" ForeColor="White">
        <Paddings PaddingBottom="10px" PaddingTop="10px" />
    </HeaderStyle>
    <ModalBackgroundStyle BackColor="Black" Opacity="70"></ModalBackgroundStyle>
    <ContentCollection>
        <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server" SupportsDisabledAttribute="True" 
            HeaderText="Photo upload notification" Modal="True" 
            Name="winAutoNotificationsSettings">
	    <div class="items_none2" style="margin:10px;">
            <div style="padding:20px 0 0;margin-bottom:40px;text-align:left;">
                <asp:Literal ID="msg_HasNoPhotosText" runat="server"/>
            </div>
            <asp:HyperLink ID="lnkUploadPhoto" CssClass="btn btn-danger" ForeColor="#ffffff" runat="server" NavigateUrl="~/Members/Photos.aspx" onclick="closePopup('UploadPhotoPopUp');">Upload a Photo</asp:HyperLink>
	    </div>
        <div style="margin:0 10px 5px 0">
            <table style="width:100%;">
                <tr>
                    <td align="right"><a href="javascript:closePopup('UploadPhotoPopUp');">Close</a></td>
                </tr>
            </table>
        </div>
        </dx:PopupControlContentControl>
    </ContentCollection>
</dx:ASPxPopupControl>

<%--<Windows>
    <dx:PopupWindow
        HeaderText="Photo upload notification" Modal="True" 
        Name="winAutoNotificationsSettings"
        >
        <ContentStyle HorizontalAlign="Center" VerticalAlign="Middle">
            <Paddings Padding="0px" />
        </ContentStyle>
            
    </dx:PopupWindow>
</Windows>--%>
<%--<ContentStyle>
    <Paddings Padding="0px" />
</ContentStyle>--%>
<%--<ContentCollection>
    <dx:PopupControlContentControl ID="PopupControlContentControl2" runat="server" SupportsDisabledAttribute="True">
    </dx:PopupControlContentControl>
</ContentCollection>--%>

﻿Imports DevExpress.Web.ASPxUploadControl
Imports System.IO
Imports System.Drawing
Imports Dating.Server.Core.DLL
Imports DevExpress.Web.ASPxEditors
Imports DevExpress.Web.ASPxPopupControl
Imports Dating.Server.Datasets.DLL

Public Class PhotosEditIfr
    Inherits BaseUserControl


    Dim _pageData As clsPageData
    Protected ReadOnly Property CurrentPageData As clsPageData
        Get
            If (_pageData Is Nothing) Then _pageData = New clsPageData("~/Members/control.PhotosEdit", Context)
            Return _pageData
        End Get
    End Property

    Private Property PhotoUploadedNotified As Boolean
        Get
            If (ViewState("PhotoUploadedNotified") Is Nothing) Then Return False
            Return ViewState("PhotoUploadedNotified")
        End Get
        Set(value As Boolean)
            ViewState("PhotoUploadedNotified") = value
        End Set
    End Property

    Private Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If (Me.Visible) Then

            Try


                'Try
                '    Dim cPageBasic As Dating.Server.Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
                '    GeneralFunctions.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
                'Catch ex As Exception
                '    WebErrorMessageBox(Me, ex, "Page_Load")
                'End Try

                'If (Not Me.IsPostBack) Then
                LoadLAG()
                'End If

                If (Not Page.IsPostBack) Then
                    TogglePersonalCollapsible(False)
                End If

            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "Page_Load")
            End Try
        End If

    End Sub


    Private Sub TogglePersonalCollapsible(show As Boolean)
        If (show) Then
            lnkInfoExpColl.Text = "-"
            h2InfoTitle.Attributes("title") = globalStrings.GetCustomString("msg_ClickToCollapse", GetLag())
            divmp_info.Attributes.CssStyle.Remove("display")
        Else
            lnkInfoExpColl.Text = "+"
            h2InfoTitle.Attributes("title") = globalStrings.GetCustomString("msg_ClickToExpand", GetLag())
            divmp_info.Attributes.CssStyle("display") = "none"
        End If
    End Sub

    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        If (Me.Visible) Then
            Try

                LoadPhotos()

            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "Page_PreRender")
            End Try


            Try
                Dim cPageBasic As Dating.Server.Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
                AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "Page_PreRender")
            End Try
        End If

    End Sub


    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
    End Sub


    Protected Sub LoadLAG()
        Try

            btnDeletePhoto.Text = CurrentPageData.GetCustomString(btnDeletePhoto.ID)
            btnDeletePhoto.ClientSideEvents.SetEventHandler("Click", String.Format(<js><![CDATA[
function(s, e) {{
    if(!confirm('{0}')){{
        e.processOnServer=false;
        return false;
    }}
}}
]]></js>.Value, CurrentPageData.GetCustomString("btnDeleteClientClick").Replace("'", "\'")))
            lblProfilePhotoTitle.Text = CurrentPageData.GetCustomString(lblProfilePhotoTitle.ID)


            SetControlsValue(Me, CurrentPageData)

            msg_ProfileDefaultPicNote.Text = CurrentPageData.GetCustomString("msg_ProfileDefaultPicNote")
            msg_PubPhotos.Text = CurrentPageData.GetCustomString("msg_PubPhotos")
            msg_PrivatePhotos.Text = CurrentPageData.GetCustomString("msg_PrivatePhotos")
            msg_PrivatePhotosInfo.Text = CurrentPageData.GetCustomString("msg_PrivatePhotosInfo")

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub




    Sub SendNotificationEmailToSupport(mirrorRow As DSMembers.EUS_ProfilesRow, newPhoto As DSMembers.EUS_CustomerPhotosRow, isAutoApproveOn As Boolean)
        Try

            Dim toEmail As String = ConfigurationManager.AppSettings("gToEmail")
            Dim Content As String = globalStrings.GetCustomString("EmailSendToSupport_MemberNewPhoto", "US")

            If (isAutoApproveOn) Then
                Content = Content.Replace("###YESNO###", "YES")
            Else
                Content = Content.Replace("###YESNO###", "NO")
            End If

            Content = Content.Replace("###LOGINNAME###", mirrorRow.LoginName)
            Content = Content.Replace("###EMAIL###", mirrorRow.eMail)

            Dim approveUrl As String = ConfigurationManager.AppSettings("gApprovePhotoURL")
            Dim rejectUrl As String
            approveUrl = String.Format(approveUrl, newPhoto.CustomerPhotosID)
            rejectUrl = approveUrl & "&reject=1"

            'If (isAutoApproveOn) Then
            '    Content = Content.Replace("###APPROVEPHOTOURL###", "")
            '    Content = Content.Replace("###REJECTPHOTOURL###", "")
            'Else
            Content = Content.Replace("###APPROVEPHOTOURL###", approveUrl)
            Content = Content.Replace("###REJECTPHOTOURL###", rejectUrl)
            'End If

            Try
                'Dim siteUrl As String = ConfigurationManager.AppSettings("gSiteURL")
                Dim photoUrl As String = ""
                photoUrl = ProfileHelper.GetProfileImageURL(newPhoto.CustomerID, newPhoto.FileName, mirrorRow.GenderId, True, Me.IsHTTPS)
                'photoUrl = (siteUrl & photoUrl)

                Content = Content.Replace("###FILEPATH###", photoUrl)
                Content = Content.Replace("blank###PHOTOURL###", photoUrl) ' cms editor issue, adds word blank
                Content = Content.Replace("###PHOTOURL###", photoUrl)
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "default photo")
            End Try


            Try
                Content = Content.Replace("###BIRTHDATE###", mirrorRow.Birthday)
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###AGE###", ProfileHelper.GetCurrentAge(mirrorRow.Birthday))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###GENDER###", ProfileHelper.GetGenderString(mirrorRow.GenderId, "US"))
                Content = Content.Replace("###SEX###", ProfileHelper.GetGenderString(mirrorRow.GenderId, "US"))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###COUNTRY###", ProfileHelper.GetCountryName(mirrorRow.Country))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###STATEREGION###", mirrorRow._Region)
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###CITY###", mirrorRow.City)
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###ZIP###", mirrorRow.Zip)
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###DATETOREGISTER###", mirrorRow.DateTimeToRegister)
            Catch ex As Exception
            End Try


            Try
                Content = Content.Replace("###PROFILEAPPROVEDYESNO###", IIf(mirrorRow.Status = 4, "YES", "NO"))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###APPROVEDPHOTOSYESNO###", "NO")
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###AUTOAPPROVEDPHOTOYESNO###", "NO")
            Catch ex As Exception
            End Try


            Try
                Content = Content.Replace("###IP###", Dating.Server.Core.DLL.clsHTMLHelper.CreateIPLookupLinks(Session("IP")))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###GEOIP###", Session("GEO_COUNTRY_CODE"))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###AGENT###", Request.Params("HTTP_USER_AGENT"))
            Catch ex As Exception
            End Try
            Try
                'Dim _uri As Uri = Nothing

                'Try
                '    _uri = New Uri(Request.Params("HTTP_REFERER"))
                'Catch ex As Exception
                'End Try
                'If (_uri IsNot Nothing) Then
                '    Content = Content.Replace("###REFERRER###", _uri.Host & ":" & _uri.Port)
                'Else
                Content = Content.Replace("###REFERRER###", Dating.Server.Core.DLL.clsHTMLHelper.CreateURLLink(IIf(Session("Referrer") IsNot Nothing, Session("Referrer"), "")))
                'End If
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###CUSTOMREFERRER###", Session("CustomReferrer"))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###CUSTOMERID###", mirrorRow.ProfileID)
            Catch ex As Exception
            End Try
            Try
                Dim link As String = ConfigurationManager.AppSettings("gSiteURL") & _
                    "?logon_customer=" & mirrorRow.MirrorProfileID & "_" & mirrorRow.ProfileID
                Content = Content.Replace("###LOGONCUSTOMER###", link)
            Catch ex As Exception
            End Try


            Try
                Dim SearchEngineKeywords As String = IIf(Session("SearchEngineKeywords") Is Nothing, "", Session("SearchEngineKeywords"))
                Content = Content.Replace("###SEARCHENGINEKEYWORDS###", SearchEngineKeywords)
            Catch ex As Exception
            End Try


            Try
                Dim LandingPage As String = ""
                If (Not mirrorRow.IsLandingPageNull()) Then
                    LandingPage = mirrorRow.LandingPage
                    If (LandingPage Is Nothing) Then LandingPage = ""
                End If
                Content = Content.Replace("###LANDINGPAGE###", Dating.Server.Core.DLL.clsHTMLHelper.CreateURLLink(LandingPage))
            Catch ex As Exception
            End Try


            clsMyMail.SendMail(ConfigurationManager.AppSettings("gToEmail"), "New photos uploaded on Goomena.com", Content, True)

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


    'Sub SendNotificationEmailToSupport_old(mirrorRow As DSMembers.EUS_ProfilesRow, newPhoto As DSMembers.EUS_CustomerPhotosRow, isAutoApproveOn As Boolean)
    '    Try

    '        Dim toEmail As String = ConfigurationManager.AppSettings("gToEmail")
    '        Dim Content As String = globalStrings.GetCustomString("EmailSendToSupport_MemberNewPhoto", "US")

    '        If (isAutoApproveOn) Then
    '            Content = Content.Replace("###YESNO###", "YES")
    '        Else
    '            Content = Content.Replace("###YESNO###", "NO")
    '        End If

    '        Content = Content.Replace("###LOGINNAME###", mirrorRow.LoginName)
    '        Content = Content.Replace("###EMAIL###", mirrorRow.eMail)
    '        Content = Content.Replace("###GENDER###", ProfileHelper.GetGenderString(mirrorRow.GenderId, "US"))

    '        Try
    '            Dim siteUrl As String = ConfigurationManager.AppSettings("gSiteURL")
    '            Dim photoUrl As String = ""
    '            photoUrl = ProfileHelper.GetProfileImageURL(newPhoto.CustomerID, newPhoto.FileName, mirrorRow.GenderId, True)
    '            photoUrl = (siteUrl & photoUrl)

    '            Content = Content.Replace("###FILEPATH###", photoUrl)
    '            Content = Content.Replace("blank###PHOTOURL###", photoUrl) ' cms editor issue, adds word blank
    '            Content = Content.Replace("###PHOTOURL###", photoUrl)
    '        Catch ex As Exception
    '            WebErrorMessageBox(Me, ex, "default photo")
    '        End Try



    '        Dim approveUrl As String = ConfigurationManager.AppSettings("gApprovePhotoURL")
    '        Dim rejectUrl As String
    '        approveUrl = String.Format(approveUrl, newPhoto.CustomerPhotosID)
    '        rejectUrl = approveUrl & "&reject=1"

    '        ' allow reject photo after auto approve
    '        If (isAutoApproveOn) Then
    '            Content = Content.Replace("###APPROVEPHOTOURL###", "")
    '            Content = Content.Replace("###REJECTPHOTOURL###", rejectUrl)
    '        Else
    '            Content = Content.Replace("###APPROVEPHOTOURL###", approveUrl)
    '            Content = Content.Replace("###REJECTPHOTOURL###", rejectUrl)
    '        End If


    '        '"New photos uploaded by member " & Me.SessionVariables.DataRecordLoginMemberReturn.LoginName & ". Filename " & filePath
    '        clsMyMail.SendMail(toEmail, "New photos uploaded on Goomena.com", Content, True)

    '    Catch ex As Exception
    '        WebErrorMessageBox(Me, ex, "")
    '    End Try
    'End Sub


    Private Sub LoadPhotos()


        Try

            ' true when user makes logoff on photos page
            If (Me.GetCurrentProfile(True) Is Nothing) Then Return


            imgProfilePhoto.ImageUrl = ""
            btnDeletePhoto.CommandArgument = 0
            btnDeletePhoto.Visible = True



            Dim DeleteButtonText As String = CurrentPageData.GetCustomString("btnDelete")
            Dim DeleteButtonClientClickText As String = "if(!confirm('" & CurrentPageData.GetCustomString("btnDeleteClientClick").Replace("'", "\'") & "')) {return false;}"
            Dim DefaultButtonText As String = CurrentPageData.GetCustomString("btnDefault")
            Dim NotApprovedPhotoInfoText As String = CurrentPageData.GetCustomString("NotApprovedPhotoInfoText")
            Dim DeclinedPhotoInfoText As String = CurrentPageData.GetCustomString("DeclinedPhotoInfoText")
            Dim NotApprovedPrivatePhotoInfoText As String = CurrentPageData.GetCustomString("NotApprovedPrivatePhotoInfoText")
            Dim EditButtonText As String = CurrentPageData.GetCustomString("EditButtonText")


            'Dim profileId As Integer = Me.Session("ProfileID")
            Dim ds As DSMembers = DataHelpers.GetEUS_CustomerPhotos_ByProfileOrMirrorID(Me.MasterProfileId)

            Using dataTbl As New DataTable()


                dataTbl.Columns.Add("ImageId")
                dataTbl.Columns.Add("ImageUrl")
                dataTbl.Columns.Add("ImageThumbUrl")
                dataTbl.Columns.Add("IsDefault")
                dataTbl.Columns.Add("ImageCss")
                dataTbl.Columns.Add("DeleteButtonText")
                dataTbl.Columns.Add("DeleteButtonClientClickText")
                dataTbl.Columns.Add("DefaultButtonText")
                dataTbl.Columns.Add("DefaultVisible")
                dataTbl.Columns.Add("InfoVisible")
                dataTbl.Columns.Add("NotApprovedPhotoInfoText")
                dataTbl.Columns.Add("DisplayLevel")
                dataTbl.Columns.Add("EditButtonText")

                ' bind public photos list
                Dim foundRows As DSMembers.EUS_CustomerPhotosRow() = ds.EUS_CustomerPhotos.Select("DisplayLevel = 0")
                For Each row As DSMembers.EUS_CustomerPhotosRow In foundRows
                    Try

                        Dim dr As DataRow = dataTbl.NewRow()
                        dr("DisplayLevel") = row.DisplayLevel
                        dr("ImageId") = row.CustomerPhotosID

                        dr("ImageUrl") = ProfileHelper.GetProfileImageURL(Me.GetCurrentProfile().ProfileID, row.FileName, Me.GetCurrentProfile().GenderId, False, Me.IsHTTPS)
                        dr("ImageThumbUrl") = ProfileHelper.GetProfileImageURL(Me.GetCurrentProfile().ProfileID, row.FileName, Me.GetCurrentProfile().GenderId, True, Me.IsHTTPS)

                        If (Not row.IsIsDefaultNull()) Then
                            dr("IsDefault") = row.IsDefault

                            If (row.IsDefault) Then
                                imgProfilePhoto.ImageUrl = ProfileHelper.GetProfileImageURL(Me.GetCurrentProfile().ProfileID, row.FileName, Me.GetCurrentProfile().GenderId, True, Me.IsHTTPS)
                                btnDeletePhoto.CommandArgument = dr("ImageId")
                            End If
                        Else
                            dr("IsDefault") = False
                        End If



                        If (row.HasAproved) Then
                            dr("DefaultVisible") = (dr("IsDefault") = False)
                        Else
                            dr("DefaultVisible") = False
                        End If

                        If (row.HasDeclined) Then
                            dr("InfoVisible") = True
                            dr("NotApprovedPhotoInfoText") = DeclinedPhotoInfoText
                        ElseIf (Not row.HasAproved) Then
                            dr("InfoVisible") = True
                            dr("NotApprovedPhotoInfoText") = NotApprovedPhotoInfoText
                        Else
                            dr("InfoVisible") = False
                        End If


                        If (foundRows.Count > 1 AndAlso row.CustomerPhotosID = foundRows(foundRows.Count - 1).CustomerPhotosID) Then
                            dr("ImageCss") = "last"
                        End If

                        dr("DeleteButtonText") = DeleteButtonText
                        dr("DeleteButtonClientClickText") = DeleteButtonClientClickText
                        dr("DefaultButtonText") = DefaultButtonText
                        dr("EditButtonText") = EditButtonText

                        dataTbl.Rows.Add(dr)
                    Catch ex As Exception
                        WebErrorMessageBox(Me, ex, "")
                    End Try
                Next


                lvPubPhotos.DataSource = dataTbl
                lvPubPhotos.DataBind()

                If (dataTbl.Rows.Count = 0) Then
                    divNoPhoto.Visible = True
                Else
                    divNoPhoto.Visible = False
                End If


                dataTbl.Clear()


                ' bind private photos list
                foundRows = ds.EUS_CustomerPhotos.Select("DisplayLevel > 0")
                For Each row As DSMembers.EUS_CustomerPhotosRow In foundRows
                    Try
                        Dim dr As DataRow = dataTbl.NewRow()
                        dr("DisplayLevel") = row.DisplayLevel
                        dr("ImageId") = row.CustomerPhotosID

                        Dim path As String = String.Format(ProfileHelper.gMemberPhotosDirectoryView, Me.MasterProfileId)
                        dr("ImageUrl") = path & "/" & row.FileName
                        'dr("ImageUrl") = ResolveUrl(path & "/" & row.FileName)

                        Dim thumbPath As String = String.Format(ProfileHelper.gMemberPhotosThumbsDirectoryView, Me.MasterProfileId)
                        dr("ImageThumbUrl") = thumbPath & "/" & row.FileName
                        'dr("ImageThumbUrl") = ResolveUrl(thumbPath & "/" & row.FileName)

                        dr("IsDefault") = False

                        If (row.HasDeclined) Then
                            dr("InfoVisible") = True
                            dr("NotApprovedPhotoInfoText") = DeclinedPhotoInfoText
                        ElseIf (Not row.HasAproved) Then
                            dr("InfoVisible") = True
                            dr("NotApprovedPhotoInfoText") = NotApprovedPrivatePhotoInfoText
                        Else
                            dr("InfoVisible") = False
                        End If

                        If (foundRows.Count > 1 AndAlso row.CustomerPhotosID = foundRows(foundRows.Count - 1).CustomerPhotosID) Then
                            dr("ImageCss") = "last"
                        End If

                        dr("DeleteButtonText") = DeleteButtonText
                        dr("DeleteButtonClientClickText") = DeleteButtonClientClickText
                        dr("DefaultButtonText") = DefaultButtonText
                        dr("EditButtonText") = EditButtonText

                        dataTbl.Rows.Add(dr)
                    Catch ex As Exception
                        WebErrorMessageBox(Me, ex, "")
                    End Try
                Next


                lvPrivePhotos.DataSource = dataTbl
                lvPrivePhotos.DataBind()


                If (imgProfilePhoto.ImageUrl = "") Then
                    If ProfileHelper.IsMale(Me.SessionVariables.MemberData.GenderId) Then
                        imgProfilePhoto.ImageUrl = ProfileHelper.Male_DefaultImage
                    ElseIf ProfileHelper.IsFemale(Me.SessionVariables.MemberData.GenderId) Then
                        imgProfilePhoto.ImageUrl = ProfileHelper.Female_DefaultImage
                    End If
                    btnDeletePhoto.Visible = False
                End If

                'If (dataTbl.Rows.Count = 0) Then
                '    divNoPhoto.Visible = True
                'Else
                '    divNoPhoto.Visible = False
                'End If

                If (divNoPhoto.Visible) Then
                    If (dataTbl.Rows.Count = 0) Then
                        divNoPhoto.Visible = True
                    Else
                        divNoPhoto.Visible = False
                    End If
                End If
            End Using
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Failed to load photos")
        End Try

    End Sub



    Protected Sub lvPubPhotos_ItemCommand(sender As Object, e As System.Web.UI.WebControls.ListViewCommandEventArgs) Handles lvPubPhotos.ItemCommand
        Try
            Dim photoId As Integer
            Integer.TryParse(e.CommandArgument.ToString(), photoId)
            ExecCmd(e.CommandName.ToUpper(), photoId)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "lvPubPhotos_ItemCommand")
        End Try
    End Sub


    Protected Sub lvPrivePhotos_ItemCommand(sender As Object, e As System.Web.UI.WebControls.ListViewCommandEventArgs) Handles lvPrivePhotos.ItemCommand
        Try
            Dim photoId As Integer
            Integer.TryParse(e.CommandArgument.ToString(), photoId)
            ExecCmd(e.CommandName.ToUpper(), photoId)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "lvPrivePhotos_ItemCommand")
        End Try
    End Sub


    Protected Sub btnDeletePhoto_Command(sender As Object, e As System.Web.UI.WebControls.CommandEventArgs) Handles btnDeletePhoto.Command
        Try
            Dim photoId As Integer
            Integer.TryParse(e.CommandArgument.ToString(), photoId)
            ExecCmd(e.CommandName.ToUpper(), photoId)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "btnDeletePhoto_Command")
        End Try
    End Sub


    Private Sub ExecCmd(commandName As String, photoId As Integer)
        Try
            'Dim profileId As Integer = Me.Session("ProfileID")
            Select Case (commandName)

                Case "DELETE"

                    Dim ds As DSMembers = DataHelpers.GetEUS_CustomerPhotosByCustomerPhotosID(photoId)
                    Dim fileName As String = ds.EUS_CustomerPhotos.Rows(0).Field(Of String)("FileName")
                    Dim isDefault As Boolean = False
                    If (Not ds.EUS_CustomerPhotos.Rows(0).IsNull("IsDefault")) Then
                        isDefault = ds.EUS_CustomerPhotos.Rows(0).Field(Of Boolean)("IsDefault")
                    End If
                    'If (Not ds.EUS_CustomerPhotos.Rows(0).IsNull("IsDeleted")) Then
                    ds.EUS_CustomerPhotos.Rows(0).SetField("IsDeleted", True)
                    'End If

                    'ds.EUS_CustomerPhotos.Rows(0).Delete()
                    DataHelpers.UpdateEUS_CustomerPhotos(ds)


                    'Dim path As String = String.Format(ProfileHelper.gMemberPhotosDirectory, Me.MasterProfileId)
                    'path = MapPath(path & "/" & fileName)
                    'If (System.IO.File.Exists(path)) Then
                    '    Try
                    '        System.IO.File.Delete(path)
                    '    Catch ex As Exception
                    '        WebErrorMessageBox(Me, ex, "ExecCmd")
                    '    End Try
                    'End If


                    'Dim thumbPath As String = String.Format(ProfileHelper.gMemberPhotosThumbsDirectory, Me.MasterProfileId)
                    'thumbPath = MapPath(thumbPath & "/" & fileName)
                    'If (System.IO.File.Exists(thumbPath)) Then
                    '    Try
                    '        System.IO.File.Delete(thumbPath)
                    '    Catch ex As Exception
                    '        WebErrorMessageBox(Me, ex, "ExecCmd")
                    '    End Try
                    'End If

                    Try
                        clsMyMail.SendMail(ConfigurationManager.AppSettings("gToEmail"), "Photo deleted on Goomena.com", "Photo is deleted. Filename " & fileName & ". Delete by " & Me.SessionVariables.MemberData.LoginName)
                    Catch ex As Exception
                        WebErrorMessageBox(Me, ex, "ExecCmd")
                    End Try


                    If (isDefault) Then
                        ' set next approved public photo as default
                        SetNextDefaultPhoto()
                        RefreshProfilePreviewControl(Me.Page)
                    End If

                    ds.Dispose()

                Case "DEFAULT"
                    Dim hasDefaultChanged As Boolean = False
                    Dim ds As DSMembers = DataHelpers.GetEUS_CustomerPhotos_ByProfileOrMirrorID(Me.MasterProfileId)
                    For Each rawRecord As DSMembers.EUS_CustomerPhotosRow In ds.EUS_CustomerPhotos.Rows
                        If (rawRecord.CustomerPhotosID = photoId AndAlso rawRecord.HasAproved = True) Then
                            rawRecord.IsDefault = True
                            hasDefaultChanged = True
                        Else
                            rawRecord.IsDefault = False
                        End If
                    Next
                    DataHelpers.UpdateEUS_CustomerPhotos(ds)
                    ds.Dispose()

                    RefreshProfilePreviewControl(Me.Page)

                    If (hasDefaultChanged) Then
                        DataHelpers.UpdateEUS_Profiles_MirrorSetStatusUpdating(Me.MasterProfileId, ProfileStatusEnum.Updating, ProfileModifiedEnum.UpdatingPhotos)
                        'Dim profileRows As New clsProfileRows(Me.MasterProfileId)
                        'If (profileRows.GetMirrorRow().Status = ProfileStatusEnum.Approved) Then
                        '    profileRows.GetMirrorRow().Status = ProfileStatusEnum.Updating
                        '    profileRows.Update()
                        'End If
                    End If
            End Select

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "ExecCmd")
        End Try
    End Sub

    ''' <summary>
    ''' set next approved public photo as default
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub SetNextDefaultPhoto()
        Dim hasDefaultChanged As Boolean = False
        Dim _ds1 As DSMembers = DataHelpers.GetEUS_CustomerPhotos_ByProfileOrMirrorID(Me.MasterProfileId)
        Dim foundRows As DSMembers.EUS_CustomerPhotosRow() = _ds1.EUS_CustomerPhotos.Select("DisplayLevel = 0")
        For Each row As DSMembers.EUS_CustomerPhotosRow In foundRows
            If (row.HasAproved AndAlso Not row.HasDeclined) Then
                row.IsDefault = True
                DataHelpers.UpdateEUS_CustomerPhotos(_ds1)
                hasDefaultChanged = True
                Exit For
            End If
        Next

        If (hasDefaultChanged) Then
            DataHelpers.UpdateEUS_Profiles_MirrorSetStatusUpdating(Me.MasterProfileId, ProfileStatusEnum.Updating, ProfileModifiedEnum.UpdatingPhotos)
            'Dim profileRows As New clsProfileRows(Me.MasterProfileId)
            'If (profileRows.GetMirrorRow().Status = ProfileStatusEnum.Approved) Then
            '    profileRows.GetMirrorRow().Status = ProfileStatusEnum.Updating
            '    profileRows.Update()
            'End If
        End If

        _ds1.Dispose()
    End Sub


    '!!! leave this handler to avoid error on scriptmanager, it used in it's event validation
    Protected Sub lvPubPhotos_ItemDeleting(sender As Object, e As System.Web.UI.WebControls.ListViewDeleteEventArgs) Handles lvPubPhotos.ItemDeleting

    End Sub

    '!!! leave this handler to avoid error on scriptmanager, it used in it's event validation
    Protected Sub lvPrivePhotos_ItemDeleting(sender As Object, e As System.Web.UI.WebControls.ListViewDeleteEventArgs) Handles lvPrivePhotos.ItemDeleting

    End Sub


    Protected Sub lvPrivePhotos_DataBound(sender As Object, e As EventArgs) Handles lvPrivePhotos.DataBound

        Try

            If (lvPrivePhotos.Items.Count > 0) Then
                For Each itm As ListViewDataItem In lvPrivePhotos.Items


                    Try
                        Dim hdfDisplayLevel As HiddenField = itm.FindControl("hdfDisplayLevel")
                        Dim hdfImageID As HiddenField = itm.FindControl("hdfImageID")

                        'Dim _cbPhotosLevel1 As ASPxComboBox = itm.FindControl("cbPhotosLevel")
                        ''Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_PhotosDisplayLevel, Session("LagID"), "PhotosDisplayLevelId", _cbPhotosLevel1, True, "US")

                        Dim editPhotoWindow As ASPxPopupControl = itm.FindControl("editPhotoWindow")
                        Dim _cbPhotosLevel1 As ASPxComboBox = editPhotoWindow.Windows(0).FindControl("cbPhotosLevel")
                        _cbPhotosLevel1.ID = "cbPhotosLevel_" & hdfImageID.Value
                        editPhotoWindow.ClientInstanceName = editPhotoWindow.ID & "_" & hdfImageID.Value

                        If (Not SelectComboItem(_cbPhotosLevel1, hdfDisplayLevel.Value)) Then
                            For Each lei As ListEditItem In _cbPhotosLevel1.Items
                                If (lei.Value > 0) Then
                                    lei.Selected = True
                                    Exit For
                                End If
                            Next
                        End If

                        Dim lblInfoInsidePopup As Label = editPhotoWindow.Windows(0).FindControl("lblInfoInsidePopup")
                        lblInfoInsidePopup.Text = Me.CurrentPageData.GetCustomString("lblInfoInsidePopupChangingLevel")
                        editPhotoWindow.HeaderText = Me.CurrentPageData.GetCustomString("popupWindows.PhotoLevelTitle")
                    Catch ex As Exception
                        WebErrorMessageBox(Me, ex, "")
                    End Try


                Next
            End If

            LoadLAG()
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Protected Sub lvPubPhotos_DataBound(sender As Object, e As EventArgs) Handles lvPubPhotos.DataBound

        Try

            If (lvPubPhotos.Items.Count > 0) Then
                For Each itm As ListViewDataItem In lvPubPhotos.Items


                    Try
                        Dim hdfDisplayLevel As HiddenField = itm.FindControl("hdfDisplayLevel")
                        Dim hdfImageID As HiddenField = itm.FindControl("hdfImageID")

                        'Dim _cbPhotosLevel1 As ASPxComboBox = itm.FindControl("cbPhotosLevel")
                        ''Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_PhotosDisplayLevel, Session("LagID"), "PhotosDisplayLevelId", _cbPhotosLevel1, True, "US")
                        '

                        Dim editPhotoWindow As ASPxPopupControl = itm.FindControl("editPhotoWindow")
                        Dim _cbPhotosLevel1 As ASPxComboBox = editPhotoWindow.Windows(0).FindControl("cbPhotosLevel")
                        _cbPhotosLevel1.ID = "cbPhotosLevel_" & hdfImageID.Value
                        editPhotoWindow.ClientInstanceName = editPhotoWindow.ID & "_" & hdfImageID.Value

                        If (Not SelectComboItem(_cbPhotosLevel1, hdfDisplayLevel.Value)) Then
                            For Each lei As ListEditItem In _cbPhotosLevel1.Items
                                If (lei.Value = 0) Then
                                    lei.Selected = True
                                    Exit For
                                End If
                            Next
                        End If

                        Dim lblInfoInsidePopup As Label = editPhotoWindow.Windows(0).FindControl("lblInfoInsidePopup")
                        lblInfoInsidePopup.Text = Me.CurrentPageData.GetCustomString("lblInfoInsidePopupChangingLevel")

                    Catch ex As Exception
                        WebErrorMessageBox(Me, ex, "")
                    End Try


                Next
            End If

            LoadLAG()
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Protected Sub cbpnlPhotos_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cbpnlPhotos.Callback

        Try
            Dim ctlid As String = e.Parameter.Replace("ctlid_", "")
            ctlid = ctlid.Remove(ctlid.IndexOf("_lvl_"))
            ctlid = ctlid.Remove(0, ctlid.IndexOf("_") + 1)

            Dim lvl As String = e.Parameter.Substring(e.Parameter.IndexOf("_lvl_") + Len("_lvl_"))


            Dim allowAutoApproved As Boolean
            'query for photo auto approve setting
            Try
                Dim config As New clsConfigValues()
                allowAutoApproved = (config.auto_approve_photos = "1")
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "AutoApprovePhoto query failed.")
            End Try

            Dim isDefault As Boolean = False

            Try
                Dim ds As DSMembers = DataHelpers.GetEUS_CustomerPhotosByCustomerPhotosID(ctlid)
                Dim photoRow As DSMembers.EUS_CustomerPhotosRow = ds.EUS_CustomerPhotos.Rows(0)

                If (Not photoRow.IsNull("IsDefault")) Then
                    isDefault = photoRow.IsDefault
                End If

                If (allowAutoApproved) Then
                    photoRow.HasAproved = True
                    photoRow.IsDefault = False
                Else
                    If (photoRow.DisplayLevel > 0 AndAlso lvl = 0) Then
                        photoRow.HasAproved = False
                        photoRow.IsDefault = False
                    End If
                End If

                photoRow.DisplayLevel = lvl
                DataHelpers.UpdateEUS_CustomerPhotos(ds)

                ds.Dispose()
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try

            If (isDefault) Then
                SetNextDefaultPhoto()
            End If

            LoadLAG()
            LoadPhotos()
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


End Class
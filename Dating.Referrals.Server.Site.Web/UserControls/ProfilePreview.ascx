﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ProfilePreview.ascx.vb" Inherits="Dating.Referrals.Server.Site.Web.ProfilePreview" %>
<div class="" id="profile">
    <div class="left">
        <div class="photo">
            <div class="thumb">
                <a id="lnkPhoto" href="~/Members/Photos.aspx" runat="server" >
                    <img id="imgProfile" src="" runat="server" alt="" />
                </a>
            </div>
        </div>
        <div class="info">
            <h2>
                <asp:Literal ID="ltrName" runat="server"></asp:Literal></h2>
            <p class="sub_info">
                <asp:Literal ID="ltrInfo" runat="server"></asp:Literal></p>
            <div class="expired_padding">
            </div>
            <p>
                Edit: <dx:ASPxHyperLink ID="lnkEditProfile" runat="server" Text="Profile" NavigateUrl="~/Members/Profile.aspx?do=edit">
                </dx:ASPxHyperLink> | 
                <dx:ASPxHyperLink ID="lnkEditPhotos" runat="server" Text="Photos" NavigateUrl="~/Members/Photos.aspx">
                </dx:ASPxHyperLink></p>
            <p>
                <dx:ASPxHyperLink ID="lnkBilling" runat="server" Text="Standard Member" NavigateUrl="~/Members/SelectProduct.aspx">
                </dx:ASPxHyperLink></p>
        </div>
    </div>
    <div class="clear">
    </div>
    <%--<div class="upgrade_now_new">
        <a id="A4" runat="server" href="~/Members/SelectProduct.aspx">Get Noticed. Upgrade Now</a>
    </div>--%>
</div>

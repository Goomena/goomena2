﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="MemberLeftPanel.ascx.vb" Inherits="Dating.Referrals.Server.Site.Web.MemberLeftPanel" %>
<%@ Register assembly="DevExpress.Web.v13.1, Version=13.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" 
    namespace="DevExpress.Web.ASPxDataView" tagprefix="dx" %>
<%--<%@ Register src="ProfileIconCurrent.ascx" tagname="ProfileIconCurrent" tagprefix="uc4" %>--%>
<%@ Register src="LiveZilla.ascx" tagname="LiveZilla" tagprefix="uc1" %>
<asp:Panel ID="pnlProfile" runat="server" CssClass="profileSelf">
    <div class="sub_info">
        <div class="buttonWrap"></div>
        <dx:ASPxLabel ID="ltrLogin" runat="server" Text="" Font-Size="16px" 
            EnableDefaultAppearance="false" EnableTheming="false" EncodeHtml="False" 
            Font-Bold="True" />
    </div>
</asp:Panel>

<asp:Panel ID="pnlPhoto" runat="server" style="margin:0px 0;">
    <%--<uc4:ProfileIconCurrent ID="userIcon" runat="server" 
        CssClass="profile-picture" />--%>
    <asp:HyperLink ID="lnkPhoto" NavigateUrl="~/Members/Photos.aspx" runat="server"  CssClass="profile-picture">
        <asp:Image ID="imgPhoto" runat="server"  ImageUrl="~/Images/guy.jpg" CssClass="imgPhoto" />
    </asp:HyperLink>
</asp:Panel>

<asp:Panel ID="pnlProfile2" runat="server" CssClass="profileSelf">
    <div class="sub_info hidden" >
        <div class="buttonWrap"></div>
        <dx:ASPxLabel ID="ltrInfo" runat="server" Text="" EncodeHtml="False" />
    </div>
    <ul class="info">
        <li>
            <div class="buttonWrap"></div>
            <dx:ASPxHyperLink ID="lnkDashboard" runat="server" Text="Dashboard" 
                NavigateUrl="~/Members/Default.aspx" EnableDefaultAppearance="false" 
                EnableTheming="false" EncodeHtml="False"  CssClass="lnkDashboard" />
        </li>
        <li>
            <div class="buttonWrap"></div>
            <dx:ASPxHyperLink ID="lnkProfile" runat="server" Text="My Profile" NavigateUrl="~/Members/Profile.aspx" EnableDefaultAppearance="false" EnableTheming="false"  CssClass="lnkProfile" />
<%--
            <dx:ASPxHyperLink ID="lnkProfile" runat="server" Text="Profile" NavigateUrl="~/Members/Profile.aspx" CssClass="inline" EnableDefaultAppearance="false" EnableTheming="false" />
            /
            <dx:ASPxHyperLink ID="lnkEditProfile" runat="server" Text="Edit" NavigateUrl="~/Members/Profile.aspx?do=edit" CssClass="inlineLast" EnableDefaultAppearance="false" EnableTheming="false" />
--%>        </li>
        <li>
            <div class="buttonWrap"></div>
            <dx:ASPxHyperLink ID="lnkEditPhotos" runat="server" Text="Photos" 
                NavigateUrl="~/Members/Photos.aspx" EnableDefaultAppearance="false" 
                EnableTheming="false" EncodeHtml="False"  CssClass="lnkEditPhotos" />
        </li>
        <li>
            <div class="buttonWrap"></div>
            <dx:ASPxHyperLink ID="lnkLikes" runat="server" Text="Like" 
                NavigateUrl="~/Members/Likes.aspx?vw=likes" EnableDefaultAppearance="false" 
                EnableTheming="false" EncodeHtml="False"  CssClass="lnkLike" />
        </li>
        <li>
            <div class="buttonWrap"></div>
            <dx:ASPxHyperLink ID="lnkOffers" runat="server" Text="Offers" 
                NavigateUrl="~/Members/Offers3.aspx?vw=newoffers" EnableDefaultAppearance="false" 
                EnableTheming="false" EncodeHtml="False"  CssClass="lnkOffers" />
        </li>
        <li>
            <div class="buttonWrap"></div>
            <dx:ASPxHyperLink ID="lnkMessages" runat="server" Text="Messages" 
                NavigateUrl="~/Members/Messages.aspx" EnableDefaultAppearance="false" 
                EnableTheming="false" EncodeHtml="False"  CssClass="lnkMessages" />
        </li>
        <li>
            <div class="buttonWrap"></div>
            <dx:ASPxHyperLink ID="lnkDates" runat="server" Text="Dates" 
                NavigateUrl="~/Members/Dates.aspx?vw=accepted" EnableDefaultAppearance="false" 
                EnableTheming="false" EncodeHtml="False"  CssClass="lnkDates" />
        </li>
        <li runat="server" id="liBilling">
            <div class="buttonWrap"></div>
            <dx:ASPxHyperLink ID="lnkBilling" runat="server" Text="Standard Member" 
                NavigateUrl="~/Members/SelectProduct.aspx" EnableDefaultAppearance="false" 
                EnableTheming="false" EncodeHtml="False"  CssClass="lnkBilling" />
            <%--<div class="rfloat"></div>
            <div class="clear"></div>--%>
        </li>
        <li style="display:none;">
            <div class="buttonWrap"></div>
            <dx:ASPxHyperLink ID="lnkBilling2" runat="server" Text="Standard Member2" 
                NavigateUrl="~/Members/Billing.aspx" EnableDefaultAppearance="false" 
                EnableTheming="false" EncodeHtml="False"  CssClass="lnkBilling2" />
        </li>
        <li runat="server" id="liTestemonials" visible="False">
            <div class="buttonWrap"></div>
            <dx:ASPxHyperLink ID="lnkTestemonials" runat="server" Text="Testemonials" 
                NavigateUrl="~/Members/Testemonials.aspx" EnableDefaultAppearance="false" 
                EnableTheming="false" EncodeHtml="False" Visible="False"  CssClass="lnkTestemonials" />
        </li>
        <li>
            <div class="buttonWrap"></div>
            <dx:ASPxHyperLink ID="lnkNotifications" runat="server" Text="Dates" 
                NavigateUrl="~/Members/settings.aspx?vw=notifications" EnableDefaultAppearance="false" 
                EnableTheming="false" EncodeHtml="False"  CssClass="lnkNotifications" />
        </li>
    </ul>
    <div class="clear">
    </div>
</asp:Panel>

<asp:Panel ID="pnlQuickLinks" runat="server" CssClass="profileSelf">
    <h3 class="quickLinksHeader"><asp:HyperLink ID="lblHeaderQuickLinks" runat="server" CssClass="quickLinksHeader" NavigateUrl="~/Members/MyLists.aspx?vw=whoviewedme" /></h3>
    <asp:LinkButton ID="lnkViewDescription" runat="server" CssClass="more_info2" OnClientClick="OnMoreInfoClick('[contentUrl]', this, '[headerText]');return false;" />
    <ul class="info" style="margin:10px 0 10px;">
        <li>
            <%--<div class="buttonWrap"></div>--%>
            <dx:ASPxHyperLink ID="lnkWhoViewedMe" runat="server" Text="Who Viewed Me" 
                NavigateUrl="~/Members/MyLists.aspx?vw=whoviewedme" 
                EnableDefaultAppearance="false" EnableTheming="false" EncodeHtml="False" 
                CssClass="lnkWhoViewedMe" />
        </li>
        <li>
            <%--<div class="buttonWrap"></div>--%>
            <dx:ASPxHyperLink ID="lnkWhoFavoritedMe" runat="server" Text="Who Favorited Me" 
                NavigateUrl="~/Members/MyLists.aspx?vw=whofavoritedme" 
                EnableDefaultAppearance="false" EnableTheming="false" EncodeHtml="False" 
                CssClass="lnkWhoFavoritedMe" />
        </li>
        <li>
            <%--<div class="buttonWrap"></div>--%>
            <dx:ASPxHyperLink ID="lnkWhoSharedPhotos" runat="server" Text="Who Shared photos" 
                NavigateUrl="~/Members/MyLists.aspx?vw=whosharedphotos" 
                EnableDefaultAppearance="false" EnableTheming="false" EncodeHtml="False"  
                CssClass="lnkWhoSharedPhotos" />
        </li>
        <li>
            <%--<div class="buttonWrap"></div>--%>
            <dx:ASPxHyperLink ID="lnkMyFavoriteList" runat="server" Text="My Favorite List" 
                NavigateUrl="~/Members/MyLists.aspx?vw=myfavoritelist" 
                EnableDefaultAppearance="false" EnableTheming="false" EncodeHtml="False" />
        </li>
        <li>
            <%--<div class="buttonWrap"></div>--%>
            <dx:ASPxHyperLink ID="lnkMyBlockedList" runat="server" Text="My Blocked List" 
                NavigateUrl="~/Members/MyLists.aspx?vw=myblockedlist" 
                EnableDefaultAppearance="false" EnableTheming="false" EncodeHtml="False" />
        </li>
        <li>
            <%--<div class="buttonWrap"></div>--%>
            <dx:ASPxHyperLink ID="lnkMyViewedList" runat="server" Text="My Viewed List" 
                NavigateUrl="~/Members/MyLists.aspx?vw=myviewedlist" 
                EnableDefaultAppearance="false" EnableTheming="false" EncodeHtml="False" />
        </li>
    </ul>
    <div class="clear">
    </div>
</asp:Panel>


<div class="ld_block" id="divMembersNewestAll" runat="server">
	<h3><%--<asp:HyperLink ID="lnkMembersNewest" runat="server" NavigateUrl="~/Members/Search.aspx?vw=NEWEST"></asp:HyperLink>
        <br />--%>
        <span><asp:HyperLink ID="lnkMembersNewestAll" runat="server" NavigateUrl="~/Members/Search.aspx?vw=NEWEST"></asp:HyperLink></span></h3>
	<div class="d_winks_profiles">

<dx:ASPxDataView ID="dvNewest" runat="server" ColumnCount="1" 
    RowPerPage="10" DataSourceID="sdsNewest" Width="100%" 
            EnableDefaultAppearance="false" EnableTheming="False" ItemSpacing="0px" Layout="Flow">
    <ItemTemplate>
    <div class="m_item newest">
        <div class="clear"></div>
        <div class="pic lfloat">
            <dx:ASPxHyperLink ID="lnkFromImage" runat="server" EncodeHtml="false" 
                NavigateUrl='<%# System.Web.VirtualPathUtility.ToAbsolute("~/Members/Profile.aspx?p=") &  Eval("LoginName") %>'
                ImageUrl='<%# Dating.Referrals.Server.Site.Web.AppUtils.GetImage(Eval("CustomerID").Tostring(), Eval("FileName").Tostring(), Eval("GenderId").Tostring(), False,Me.IsHTTPS) %>' EnableTheming="False" EnableDefaultAppearance="False">
            </dx:ASPxHyperLink>
        </div>
        <dx:ASPxHyperLink ID="lnkFromLogin" runat="server" EncodeHtml="false" Text='<%# Eval("LoginName") %>' 
            NavigateUrl='<%# System.Web.VirtualPathUtility.ToAbsolute("~/Members/Profile.aspx?p=") &  Eval("LoginName") %>' EnableTheming="False" EnableDefaultAppearance="False" CssClass="loginName lfloat">
        </dx:ASPxHyperLink>
    </div>
    <div class="clear"></div>
    </ItemTemplate>
        <Paddings Padding="0px" />
    <ItemStyle>
    <Paddings Padding="0px" />
    </ItemStyle>
        </dx:ASPxDataView>
	</div>
</div>
<asp:SqlDataSource ID="sdsNewest" runat="server" ConnectionString="<%$ ConnectionStrings:AppDBconnectionString %>"
    SelectCommand="GetNewestMembers" SelectCommandType="StoredProcedure">
    <SelectParameters>
        <asp:Parameter DefaultValue="0" Name="CurrentProfileId" Type="Int32" />
        <asp:Parameter DefaultValue="4" Name="ReturnRecordsWithStatus" Type="Int32" />
        <asp:Parameter DefaultValue="5" Name="NumberOfRecordsToReturn" Type="Int32" />
    </SelectParameters>
</asp:SqlDataSource>

<div class="ld_block" id="divMembersNearAll" runat="server" visible="False">
	<h3><%--<asp:HyperLink ID="lnkMembersNear" runat="server" NavigateUrl="~/Members/Search.aspx?vw=NEAREST"></asp:HyperLink>
        <br />--%>
        <span><asp:HyperLink ID="lnkMembersNearAll" runat="server" NavigateUrl="~/Members/Search.aspx?vw=NEAREST"></asp:HyperLink></span></h3>
	<div class="d_winks_profiles">

<dx:ASPxDataView ID="dvNearest" runat="server" ColumnCount="1" 
    RowPerPage="10" Width="100%" 
            EnableDefaultAppearance="false" EnableTheming="False" ItemSpacing="0px" Layout="Flow">
    <ItemTemplate>
    <div class="m_item newest">
        <div class="clear"></div>
        <div class="pic lfloat">
            <dx:ASPxHyperLink ID="lnkFromImage" runat="server" EncodeHtml="false" 
                NavigateUrl='<%# System.Web.VirtualPathUtility.ToAbsolute("~/Members/Profile.aspx?p=") &  Eval("LoginName") %>'
                ImageUrl='<%# Dating.Referrals.Server.Site.Web.AppUtils.GetImage(Eval("CustomerID").Tostring(), Eval("FileName").Tostring(), Eval("GenderId").Tostring(), False,Me.IsHTTPS) %>' EnableTheming="False" EnableDefaultAppearance="False">
            </dx:ASPxHyperLink>
        </div>
        <dx:ASPxHyperLink ID="lnkFromLogin" runat="server" EncodeHtml="false" Text='<%# Eval("LoginName") %>' 
            NavigateUrl='<%# System.Web.VirtualPathUtility.ToAbsolute("~/Members/Profile.aspx?p=") &  Eval("LoginName") %>' EnableTheming="False" EnableDefaultAppearance="False" CssClass="loginName lfloat">
        </dx:ASPxHyperLink>
    </div>
    <div class="clear"></div>
    </ItemTemplate>
        <Paddings Padding="0px" />
    <ItemStyle>
    <Paddings Padding="0px" />
    </ItemStyle>
        </dx:ASPxDataView>
	</div>
</div>
<%--<asp:SqlDataSource ID="sdsNearest" runat="server" ConnectionString="<%$ ConnectionStrings:AppDBconnectionString %>"
    SelectCommand="GetNearestMembers" SelectCommandType="StoredProcedure">
    <SelectParameters>
        <asp:Parameter DefaultValue="0" Name="CurrentProfileId" Type="Int32" />
        <asp:Parameter DefaultValue="4" Name="ReturnRecordsWithStatus" Type="Int32" />
        <asp:Parameter DefaultValue="5" Name="NumberOfRecordsToReturn" Type="Int32" />
    </SelectParameters>
</asp:SqlDataSource>--%>

<asp:Panel ID="pnlLiveZilla" runat="server">
    <div class="rfloat">
        <uc1:LiveZilla ID="LiveZilla1" runat="server" />
    </div>
    <div class="clear"></div>
    <div style="height:10px;">&nbsp;</div>
</asp:Panel>


<asp:Panel ID="pnlBottom" runat="server" CssClass="profileSelf" style="position:relative;" Visible="false">
    <ul class="info" style="width:194px;position:absolute;bottom:-25px;left:0px;">
        <li ID="liHowWorksMale" runat="server" visible="false">
            <div class="buttonWrap"></div>
            <dx:ASPxHyperLink ID="lnkHowWorksMale" runat="server" Text="How it Works" 
                NavigateUrl="~/cmspage.aspx?pageid=185&title=HowItWorksman" EnableDefaultAppearance="false" 
                EnableTheming="false" EncodeHtml="False"  CssClass="lnkHowWorks" />
        </li>
        <li ID="liHowWorksFemale" runat="server" visible="false">
            <div class="buttonWrap"></div>
            <dx:ASPxHyperLink ID="lnkHowWorksFemale" runat="server" Text="How it Works" 
                NavigateUrl="~/cmspage.aspx?pageid=186&title=HowItWorksWoman" EnableDefaultAppearance="false" 
                EnableTheming="false" EncodeHtml="False"  CssClass="lnkHowWorks" />
        </li>
        <li ID="liReferrer" runat="server" visible="false">
            <div class="buttonWrap"></div>
            <dx:ASPxHyperLink ID="lnkReferrer" runat="server" Text="Συνεργάτης μας" 
                NavigateUrl="~/Members/Referrer.aspx" EnableDefaultAppearance="false" 
                EnableTheming="false" EncodeHtml="False"  CssClass="lnkReferrer" />
        </li>
        <li ID="liAffiliate" runat="server" visible="false">
            <div class="buttonWrap"></div>
            <dx:ASPxHyperLink ID="lnkAffiliate" runat="server" Text="Συνεργάτης μας" 
                NavigateUrl="~/Members/Affiliate.aspx" EnableDefaultAppearance="false" 
                EnableTheming="false" EncodeHtml="False"  CssClass="lnkAffiliate" />
        </li>
    </ul>
	        <%--<div id="content_bottom" runat="server" clientidmode="Static" class="container_12" style="position:relative;">
                <asp:Panel ID="pnlAffiliate" runat="server" Visible="false" style="padding-left:40px;width:192px;position:absolute;top:-17px;left:0px;" CssClass="profileSelf">
                    <asp:HyperLink ID="lnkAffiliate" runat="server" NavigateUrl="~/Members/Referrer.aspx">Συνεργάτης μας</asp:HyperLink>
                </asp:Panel>
	        </div>--%>
    <div class="clear">
    </div>
</asp:Panel>


﻿<%@ Control Language="vb" AutoEventWireup="true" CodeBehind="UCRegister.ascx.vb"
    Inherits="Dating.Referrals.Server.Site.Web.UCRegister" %>
<%@ Register Src="~/UserControls/DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<%@ Register Assembly="DevExpress.Web.v13.1, Version=13.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>
<style type="text/css">
    .BulletedList
    {
        margin-left: 0px;
        padding-left: 25px;
    }
    li.BulletedListItem
    {
    }
    .BulletedListItem a
    {
        color: Red;
        text-decoration: none;
        border-bottom-color: #f70;
        border-bottom-width: 1px;
        border-bottom-style: dashed;
    }
    .pe_form, .pe_login
    {
        width: 421px;
    }
    .lists
    {
        position: absolute;
        right: 471px;
        top: 264px;
        width: 400px;
    }
    .registernewcontainer
    {
        width: 421px;
        font-size: 13px;
    }
    .form-actions
    {
        float: right;
        margin-top: 9px;
        margin-bottom: 10px;
    }
    body
    {
        font-family: "Segoe UI" , "Arial";
        font-weight: lighter;
    }
    .pinakas
    {
        font-size: 13px;
    }
    .loginHereTitle
    {
        font-size: 24px;
        font-family: Arial;
        padding-left: 20px;
    }
    .lblform
    {
        float: left;
        margin-right: 5px;
        width: 120px;
        text-align: right;
    }
    .divform
    {
        margin-bottom: 3px;
        width: 440px;
    }
    .clear
    {
        clear: both;
    }
    .center
    {
        cursor: pointer;
    }
</style>
<script type="text/javascript">
    // <![CDATA[
    var btnRegisterClicked = false;
    function OnCountryChanged(cbCountry) {
        LoadingPanel.Hide();
        cbpnlZip.PerformCallback("country_" + cbCountry.GetValue().toString());
        LoadingPanel.Show();
    }
    function OnRegionChanged(cbRegion) {
        LoadingPanel.Hide();
        cbpnlZip.PerformCallback("region_" + cbRegion.GetValue().toString());
        LoadingPanel.Show();
    }
    function OnCityChanged(cbCity) {
        LoadingPanel.Hide();
        cbpnlZip.PerformCallback("city_" + cbCity.GetValue().toString() + "_region_" + cbRegion.GetValue().toString());
        LoadingPanel.Show();
    }
    function showZip() {
        var zip = jQuery('#<%= TrShowRegion.ClientID %>');
        var region = jQuery('#<%= TrRegion.ClientID %>');
        var city = jQuery('#<%= TrCity.ClientID %>');

        //zip.show()
        region.hide()
        city.hide()

        var status = jQuery('#<%= hdfLocationStatus.ClientID %>');
        status.val("zip")
    }
    function showRegions() {
//var zip = jQuery('#< %= liLocationsZip.ClientID %>');
var zip = jQuery('#<%= TrShowRegion.ClientID %>');
var region = jQuery('#<%= TrRegion.ClientID %>');
var city = jQuery('#<%= TrCity.ClientID %>');

        zip.hide()
        region.show()
        city.show()
        var status = jQuery('#<%= hdfLocationStatus.ClientID %>');
        status.val("region")
    }
    //runColapse("<%= globalStrings.GetCustomString("msg_ClickToExpand", GetLag() ) %>",  "<%= globalStrings.GetCustomString("msg_ClickToCollapse", GetLag()) %>")
    jQuery(function ($) {
	    $('.pe_box1').each(function () {
	        var pe_box = this;

	        $('.collapsibleHeader', pe_box).click(function () {
	            $('.pe_form', pe_box).toggle('fast', function () {
	                // Animation complete.
	                var isVisible = ($('.pe_form', pe_box).css('display') == 'none');
	                if (isVisible) {
	                    $('.togglebutton', pe_box).text("+");
	                    $('.collapsibleHeader', pe_box).attr("title", "<%= globalStrings.GetCustomString("msg_ClickToExpand", GetLag() )  %>");
	                }
	                else {
	                    $('.togglebutton', pe_box).text("-");
	                    $('.collapsibleHeader', pe_box).attr("title", "<%= globalStrings.GetCustomString("msg_ClickToCollapse", GetLag()) %>");
	                }
	            });

	        });
	    });
    	});
    // ]]> 
</script>
<div class="registernewcontainer">
    <asp:Panel ID="Panel1" runat="server" class="pe_box" DefaultButton="btnRegister">
        <h2 style="text-shadow: none; border: none; font-family: Lucida Grande,Lucida Sans Unicode,Verdana,Arial,sans-serif;">
            <asp:Label ID="msg_PrompToBegin" runat="server" CssClass="loginHereTitle" />
            <dx:ASPxHyperLink ID="lbLoginFB" runat="server" CssClass="js facebook-login fbimg"
                EnableDefaultAppearance="False" EnableTheming="False" EncodeHtml="false" ImageUrl="../images/signup_fb.png"
                NavigateUrl="~/Register.aspx?log=fb" Text="Login With Facebook" />
        </h2>
        <asp:BulletedList ID="serverValidationList" runat="server" Font-Size="14px" ForeColor="Red"
            DisplayMode="HyperLink" CssClass="BulletedList">
        </asp:BulletedList>
        <dx:ASPxValidationSummary ID="valSumm" runat="server" ValidationGroup="RegisterGroup"
            RenderMode="BulletedList" ShowErrorsInEditors="True" EncodeHtml="False">
            <Paddings PaddingBottom="10px" />
            <LinkStyle>
                <Font Size="13px"></Font>
            </LinkStyle>
            <ClientSideEvents VisibilityChanged="function(s, e) {
    jQuery(function($){
        //scrollToElem($(), 160);
    });
    btnRegisterClicked=false;
}" />
        </dx:ASPxValidationSummary>
        <asp:Panel ID="pnlContinueMessageCompleteForm" runat="server" CssClass="alert alert-success"
            Visible="False">
            <asp:Label ID="lblContinueMessageCompleteForm" runat="server" Text=""></asp:Label>
        </asp:Panel>
        <div class="pe_form pe_login">
            <div runat="server" id="TrGender" class="divform">
                <dx:ASPxRadioButtonList ID="rblGender" runat="server" RepeatLayout="Flow" ClientInstanceName="RegisterGenderRadioButtonList"
                    Border-BorderStyle="None">
                    <ValidationSettings ValidationGroup="RegisterGroup" ErrorDisplayMode="None"
                        ErrorTextPosition="Left" Display="Dynamic">
                        <RequiredField ErrorText="" IsRequired="True" />
                    </ValidationSettings>
                    <RadioButtonStyle BackgroundImage-HorizontalPosition="center" HorizontalAlign="Center">
                        <BackgroundImage HorizontalPosition="center"></BackgroundImage>
                    </RadioButtonStyle>
                </dx:ASPxRadioButtonList>
            </div>
            <div runat="server" id="TrAccountType" visible="False">
                <asp:Label ID="msg_WhoIs" runat="server" Text="" class="registerTableLabel" AssociatedControlID="cbAccountType" />
                <dx:ASPxComboBox ID="cbAccountType" runat="server" ClientInstanceName="RegisterAccountTypeComboBox"
                    EncodeHtml="False">
                    <ClientSideEvents Validation="function(s, e) {
if(s.GetSelectedIndex()==0) e.isValid=false;
}" />
                    <ValidationSettings ValidationGroup="RegisterGroup" ErrorDisplayMode="None"
                        ErrorTextPosition="Left" ErrorText="" Display="Dynamic">
                        <RequiredField ErrorText="" IsRequired="True" />
                    </ValidationSettings>
                </dx:ASPxComboBox>
            </div>
            <div runat="server" id="TrMyEmail" class="divform">
                <asp:Label ID="msg_MyEmail" runat="server" Text="" class="registerTableLabel" AssociatedControlID="txtEmail"
                    CssClass="lblform" />
                <dx:ASPxTextBox ID="txtEmail" runat="server" ClientInstanceName="RegisterEmailTextBox"
                    Width="309px">
                    <ValidationSettings ValidationGroup="RegisterGroup" ErrorDisplayMode="None"
                        ErrorTextPosition="Left" Display="Dynamic">
                        <RequiredField IsRequired="True" ErrorText="" />
                        <RegularExpression ValidationExpression="" ErrorText="" />
                    </ValidationSettings>
                </dx:ASPxTextBox>
            </div>
            <div runat="server" id="TrMyUsername" class="divform">
                <asp:Label ID="msg_MyUsername" runat="server" Text="" class="registerTableLabel"
                    AssociatedControlID="cbAccountType" CssClass="lblform" />
                <dx:ASPxTextBox ID="txtLogin" runat="server" ClientInstanceName="RegisterLoginTextBox"
                    Width="309px">
                    <ClientSideEvents Validation="function(s, e) {
	e.isValid = (s.GetText().length&gt;2) && (s.GetText().length&lt;=10)
}" />
                    <ValidationSettings ValidationGroup="RegisterGroup" ErrorDisplayMode="ImageWithTooltip"
                        ErrorTextPosition="Left" ErrorText="" Display="Dynamic">
                        <RequiredField ErrorText="" IsRequired="True" />
                        <RegularExpression ValidationExpression=".{3,10}" ErrorText="Το όνομα χρήστη πρέπει να είναι απο 3 μέχρι 10 γράμματα" />
                    </ValidationSettings>
                </dx:ASPxTextBox>
            </div>
            <div runat="server" id="TrMyPassword" class="divform">
                <asp:Label ID="msg_MyPassword" runat="server" Text="" class="registerTableLabel"
                    AssociatedControlID="txtPasswrd" CssClass="lblform" />
                <dx:ASPxTextBox ID="txtPasswrd" runat="server" Password="True" ClientInstanceName="txtPasswrd1"
                    Width="309px">
                    <ClientSideEvents Validation="function(s, e) {
    e.isValid = (s.GetText().length&gt;2)
}" />
                    <ValidationSettings ValidationGroup="RegisterGroup" ErrorDisplayMode="None"
                        ErrorText="" ErrorTextPosition="Left" Display="Dynamic">
                        <RequiredField ErrorText="" IsRequired="True" />
                        <RegularExpression ValidationExpression=".{3,20}" ErrorText="Ο κωδικός πρέπει να είναι απο 3 μέχρι 20 χαρακτήρες" />
                    </ValidationSettings>
                </dx:ASPxTextBox>
            </div>
            <div runat="server" id="TrMyPasswordConfirm" class="divform">
                <asp:Label ID="msg_MyPasswordConfirm" runat="server" Text="" class="registerTableLabel"
                    AssociatedControlID="txtPasswrd1Conf" CssClass="lblform" />
                <dx:ASPxTextBox ID="txtPasswrd1Conf" runat="server" Password="True" ClientInstanceName="txtPasswrd1Conf"
                    Width="309px">
                    <ClientSideEvents Validation="function(s, e) {e.isValid = (s.GetText() == txtPasswrd1.GetText());}" />
                    <ValidationSettings ValidationGroup="RegisterGroup" ErrorDisplayMode="None"
                        ErrorTextPosition="Left" ErrorText="" Display="Dynamic">
                    </ValidationSettings>
                </dx:ASPxTextBox>
            </div>
            <div runat="server" id="TrMyBirthdate" class="divform">
                <asp:Label ID="msg_MyBirthdate" runat="server" class="registerTableLabel" CssClass="lblform" />
                <uc1:DateControl Height="28px" FontSize="14px" ID="ctlDate" runat="server" DateTime='<%# Bind("Birthday") %>' />
            </div>





        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        
        <ContentTemplate>
        

                        <asp:HiddenField ID="hdfLocationStatus" runat="server" />
                        <div runat="server" id="TrCountry" class="divform">
                            <asp:Label ID="msg_CountryText" runat="server" Text="" class="registerTableLabel"
                                AssociatedControlID="cbCountry" CssClass="lblform" /><dx:ASPxComboBox ID="cbCountry"
                                    runat="server" Width="310px" Font-Size="14px" Height="18px" EncodeHtml="False"
                                    IncrementalFilteringMode="StartsWith" AutoPostBack="True">
                                    
                                    <ValidationSettings CausesValidation="True" Display="Dynamic" ValidationGroup="RegisterGroup">
                                    </ValidationSettings>
                                </dx:ASPxComboBox>
                        </div>
                        <div runat="server" id="TrShowRegion" class="divform" style="display: none;">
                            <span class="zip_list">
                                <asp:LinkButton ID="msg_SelectRegion" runat="server" CssClass="js" OnClientClick="showRegions();return false;" /></span></div>
                        <div runat="server" id="TrRegion" class="divform">
                            <asp:Label ID="msg_RegionText" runat="server" Text="" class="registerTableLabel"
                                AssociatedControlID="cbRegion" CssClass="lblform" />
                            <dx:ASPxComboBox ID="cbRegion" runat="server" Width="310px" Font-Size="14px" Height="18px"
                                EncodeHtml="False" EnableSynchronization="False" ClientInstanceName="cbRegion"
                                IncrementalFilteringMode="StartsWith" DataSourceID="sdsRegion" TextField="region1"
                                ValueField="region1" AutoPostBack="True">
                                
                                <ValidationSettings CausesValidation="True" Display="Dynamic" ValidationGroup="RegisterGroup">
                                </ValidationSettings>
                            </dx:ASPxComboBox>
                            <asp:SqlDataSource ID="sdsRegion" runat="server" ConnectionString="<%$ ConnectionStrings:AppDBconnectionString %>">
                            </asp:SqlDataSource>
                        </div>
                        <div runat="server" id="TrCity" class="divform">
                            <asp:Label ID="msg_CityText" runat="server" Text="" CssClass="lblform" AssociatedControlID="cbCity" /><dx:ASPxComboBox
                                ID="cbCity" runat="server" Width="310px" Font-Size="14px" Height="18px" EncodeHtml="False"
                                EnableSynchronization="False" ClientInstanceName="cbCity" IncrementalFilteringMode="StartsWith"
                                EnableIncrementalFiltering="True" DataSourceID="sdsCity" TextField="city" 
                                ValueField="city" AutoPostBack="True">
                                
                                <ValidationSettings CausesValidation="True" Display="Dynamic" ValidationGroup="RegisterGroup">
                                </ValidationSettings>
                            </dx:ASPxComboBox>
                            <asp:SqlDataSource ID="sdsCity" runat="server" ConnectionString="<%$ ConnectionStrings:AppDBconnectionString %>">
                            </asp:SqlDataSource>
                        </div>
                        <div runat="server" id="TrZip" class="divform">
                            <asp:Label ID="msg_ZipCodeText" runat="server" CssClass="lblform" Style="height: 30px" />
                            <dx:ASPxComboBox ID="txtZip" runat="server" DataSourceID="zip">
                                    <ValidationSettings Display="Dynamic">
                                    </ValidationSettings>
                                </dx:ASPxComboBox>
                                <asp:SqlDataSource ID="zip" runat="server" ConnectionString="<%$ ConnectionStrings:AppDBconnectionString %>">
                            </asp:SqlDataSource>
                            </div>
                                            
                        <div class="clear">
                        </div>

                            </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="cbCountry" EventName="SelectedIndexChanged" />
        <asp:AsyncPostBackTrigger ControlID="cbRegion" EventName="SelectedIndexChanged" />
        <asp:AsyncPostBackTrigger ControlID="cbCity" EventName="SelectedIndexChanged" />
 </Triggers>

        </asp:UpdatePanel>





            <div runat="server" id="TrAgreements" style="position: relative;">
                <div class="form_right form_tos">
                    <dx:ASPxCheckBox ID="cbAgreements" runat="server" Text="" ClientInstanceName="RegisterAgreementsCheckBox"
                        EncodeHtml="False" Checked="True">
                        <ClientSideEvents Validation="function(s, e) {
                                                    e.isValid = (s.GetChecked() == true);
                                                }" />
                        <ValidationSettings ValidationGroup="RegisterGroup" ErrorDisplayMode="ImageWithTooltip"
                            ErrorTextPosition="Left">
                            <RequiredField ErrorText="" IsRequired="True" />
                        </ValidationSettings>
                    </dx:ASPxCheckBox>
                    <dx:ASPxHyperLink ID="lnkToS" runat="server" Text="" NavigateUrl="" EncodeHtml="False"
                        Visible="False">
                    </dx:ASPxHyperLink>
                </div>
            </div>
            <div class="form-actions">
                <div>
                    <dx:ASPxButton ID="btnRegister" runat="server" CommandName="Insert" CssClass="center"
                        ValidationGroup="RegisterGroup" EncodeHtml="False" Font-Bold="True" Height="32px"
                        Font-Size="16px" Width="100px" HorizontalAlign="Center" VerticalAlign="Middle"
                        BackColor="#74A554" ForeColor="White" EnableDefaultAppearance="False" Cursor="pointer">
                        <Border BorderWidth="1px"></Border>
                    </dx:ASPxButton>
                </div>
            </div>
        </div>
    </asp:Panel>
</div>

﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PublicSearch.ascx.vb" 
    Inherits="Dating.Referrals.Server.Site.Web.PublicSearch" %>


<asp:Repeater ID="rptPubSrch" runat="server">
<ItemTemplate>

    <div class="result-item">
        <div class="pr-photo">
            <asp:HyperLink ID="lnk1" runat="server" NavigateUrl='<%# "~/PubProfile.aspx?p=" & HttpUtility.UrlEncode(Eval("OtherMemberLoginName")) %>'><img alt="" src="<%# Eval("OtherMemberImageUrl")%>" class="image-1"/></asp:HyperLink>
        </div>
        <div class="result-inner">
            <div class="pr-details">
                <div class="pr-login"><asp:HyperLink ID="lnk2" runat="server" NavigateUrl='<%# "~/PubProfile.aspx?p=" & HttpUtility.UrlEncode(Eval("OtherMemberLoginName")) %>'><%# Eval("OtherMemberLoginName")%></asp:HyperLink></div>
                <div class="pr-details-bottom">
                    <%--<div class="pr-age"><asp:Literal ID="lblAge" runat="server">29 ετών</asp:Literal></div>--%>
                    <div class="pr-other"><%# MyBase.WriteSearch_MemberInfo(Container.DataItem)%></div>
                </div>
            </div>
            <div class="pr-actions">
                <%--<div class="act-help"><asp:LinkButton ID="lnkWhatIs" runat="server" OnClientClick="OnMoreInfoClick('[contentUrl]',this,'[headerText]');return false;"/></div>--%>
                <div class="act-other"><asp:HyperLink ID="lnkOther" runat="server" CssClass="shadow" Text='<%# Eval("ActionsText") %>' NavigateUrl="~/Register.aspx"/></div>
                <div class="act-favore"><asp:HyperLink ID="lnkFav" runat="server" CssClass="shadow" Text='<%# Eval("FavoriteText") %>' NavigateUrl="~/Register.aspx"/></div>
                <div class="act-like"><asp:HyperLink ID="lnkLike" runat="server" CssClass="shadow" Text='<%# Eval("WinkText") %>' NavigateUrl="~/Register.aspx"/></div>
                <div class="act-distance"><img src="Images2/pub-search/car.png" alt=""/></div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
</ItemTemplate>
</asp:Repeater>
<%--
<div class="l_item deal_right">
    <div class="left">
    
        <div class="photo">
            <div class="thumb">
                <asp:HyperLink ID="hl2" runat="server" NavigateUrl='<%# Eval("ProfileViewUrl") %>'><img src="<%# Eval("ImageUrl") %>" /></asp:HyperLink></div>
        </div>
        <div class="info">
            <h2 class="wink"><%# Eval("YouReceivedWinkOfferText")%></h2>
            <p id="pWink" runat="server" visible='<%# Eval("IsWink") %>'><%# Eval("WantToKnowFirstDatePriceText")%></p>
            <p id="pPoke" runat="server" visible='<%# Eval("IsPoke") %>'><%# Eval("YouReceivedPokeDescriptionText")%></p>
            <p id="pAmount" runat="server" visible='<%# Eval("IsOffer") %>'><%# Eval("WillYouAcceptDateWithForAmountText")%></p>
 			<div class="bot_actions lfloat">
                <asp:Panel CssClass="rfloat icon_tooltipholder" runat="server" ID="divTooltipholder" Visible='<%# Eval("AllowTooltipPopupLikes")%>'>
                    <asp:LinkButton ID="lnkWhatIs" runat="server" OnClientClick="OnMoreInfoClick('[contentUrl]',this,'[headerText]');return false;">
                        <img runat="server" id="icon_tip" src="~/Images/icon_tip16.png" alt=""/></asp:LinkButton>
                </asp:Panel>
                <div class="lfloat">
                <asp:HyperLink ID="lnkNewMakeOffer" runat="server" visible='<%# Eval("AllowCreateOffer") %>' NavigateUrl='<%# Eval("CreateOfferUrl") %>'
                    class="btn btn-small"><span><%# Eval("MakeOfferText")%></span></asp:HyperLink>
                </div>
                <div class="lfloat">
                <asp:LinkButton ID="lnkNewAccept" runat="server" Visible='<%# Eval("AllowAccept") %>' CommandName="OFFERACCEPT" CommandArgument='<%# Eval("OfferID")%>' 
                    class="btn btn-small"><span><%# Eval("AcceptText")%></span></asp:LinkButton>
                </div>
                <div class="lfloat">
                <asp:LinkButton ID="lnkNewCounter" runat="server" Visible='<%# Eval("AllowCounter") %>' CommandName="OFFERCOUNTER" CommandArgument='<%# Eval("OfferID")%>' 
                    class="btn btn-small"><span><%# Eval("CounterText")%></span></asp:LinkButton>
                </div>
                <div class="lfloat">
                    <asp:LinkButton ID="lnkPoke" runat="server" Visible='<%# Eval("AllowPoke") %>' CommandName="POKE" CommandArgument='<%# "offr_" & Eval("OfferID") & "-prof_" & Eval("OtherMemberProfileID") %>' 
                    class="btn btn-small"><span><%# Eval("PokeText")%></span></asp:LinkButton>
                </div>
                <div class="btn-group">
                    <a class="btn btn-small dropdown-toggle" data-toggle="dropdown" href="#"><%# Eval("RejectText")%> <span class="caret"></span></a></td>
                    <ul class="dropdown-menu">
                        <li><asp:LinkButton ID="lnkRejectType" runat="server" CommandName="REJECTTYPE" CommandArgument='<%# Eval("OfferID")%>'><i class="icon-chevron-right"></i><%# Eval("NotInterestedText")%></asp:LinkButton></li>
                        <li><asp:LinkButton ID="lnkRejectFar" runat="server" CommandName="REJECTFAR" CommandArgument='<%# Eval("OfferID")%>'><i class="icon-chevron-right"></i><%# Eval("TooFarAwayText")%></asp:LinkButton></li>
                        <li><asp:LinkButton ID="lnkRejectBad" runat="server" CommandName="REJECTBAD" CommandArgument='<%# Eval("OfferID")%>'><i class="icon-chevron-right"></i><%# Eval("NotEnoughInfoText")%></asp:LinkButton></li>
                        <li><asp:LinkButton ID="lnkRejectExp" runat="server" CommandName="REJECTEXPECTATIONS" CommandArgument='<%# Eval("OfferID")%>'><i class="icon-chevron-right"></i><%# Eval("DifferentExpectationsText")%></asp:LinkButton></li>
                    </ul>
                </div>
                <div class="btn-group" ID="ActionsMenu" runat="server" Visible='<%# Eval("AllowActionsMenu") %>'>
                    <a class="btn btn-small dropdown-toggle" data-toggle="dropdown" href="#"><%# Eval("ActionsText")%> <span class="caret"></span></a></td>
                    <ul class="dropdown-menu">
                        <li ID="liActsUnlock" runat="server" visible='<%# Eval("AllowActionsUnlock")%>'><asp:LinkButton 
                                ID="lnkActsUnlock" runat="server" CommandName="UNLOCKOFFER" CommandArgument='<%# Eval("OfferId") %>'><%# Eval("ActionsUnlockText")%><i class="icon-chevron-right icon-white"></i></asp:LinkButton></li>
                        <li ID="liActsMakeOffer" runat="server" visible='<%# Eval("AllowActionsCreateOffer") %>' ><asp:HyperLink 
                                ID="lnkNewMakeOffer2" runat="server" NavigateUrl='<%# Eval("CreateOfferUrl") %>'><span><%# Eval("ActionsMakeOfferText")%></span></asp:HyperLink></li>
                        <li ID="liActsSendMsg" runat="server" visible='<%# Eval("AllowActionsSendMessage")%>'><asp:HyperLink 
                                ID="lnkSendMsg2" runat="server" NavigateUrl='<%# Eval("SendMessageUrl")%>'><%# Eval("SendMessageText")%><i class="icon-chevron-right"></i></asp:HyperLink></li>
                        <li ID="liActsDelOffr" runat="server" visible='<%# Eval("AllowActionsDeleteOffer")%>'><asp:LinkButton 
                                ID="lnkDelOffr" runat="server" CommandName="DELETEOFFER" CommandArgument='<%# Eval("OfferId") %>' OnClientClick='<%#  Eval("OfferDeleteConfirmMessage")  %>'><%# Eval("DeleteOfferText")%></asp:LinkButton></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="right plane ">
        <div class="photo">
            <div class="thumb">
                <asp:HyperLink ID="lnk1" runat="server" NavigateUrl='<%# Eval("OtherMemberProfileViewUrl")%>'><img alt="" src="<%# Eval("OtherMemberImageUrl")%>"/></asp:HyperLink>
            </div>
        </div>
        <div class="info">
            <h2><asp:HyperLink ID="lnk2" runat="server" NavigateUrl='<%# Eval("OtherMemberProfileViewUrl")%>'><%# Eval("OtherMemberLoginName")%></asp:HyperLink></h2>
            <p><%# Eval("OtherMemberHeading")%></p>
            <div class="stats">
                <%# MyBase.WriteOffer_MemberInfo(Container.DataItem)%>
                <div class="tooltip_offer" style="display: none;">
                    <div class="tooltip_text"><%# Eval("MilesAwayText")%></div>
                </div>
            </div>
            <div class="tt_enabled_distance distance_absolute <%# Eval("DistanceCss")%>">
            </div>
        </div>
    </div>
    <div class="clear"></div>
</div>
--%>
﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ContactControl.ascx.vb" Inherits="Dating.Referrals.Server.Site.Web.ContactControl" %>
<script language="javascript" type="text/javascript">
// <![CDATA[

    function cmdReset_onclick() {

    }

// ]]>
</script>
<style type="text/css">
#<%= cmdSend.ClientID %>{cursor:pointer;}
</style>
        <asp:Panel ID="pnlSendMessageWrap" runat="server" CssClass="contact_form" DefaultButton="cmdSend">
            <asp:Label ID="lblMessageSent" runat="server" ViewStateMode="Disabled"></asp:Label>

            <asp:Panel ID="pnlSendMessageForm" runat="server">
<ul class="defaultStyle">
	<!-- location -->
	<li>
        <asp:Panel ID="pnlYouremailErr" runat="server" CssClass="alert alert-danger" Visible="False">
            <asp:Label ID="lbYouremailErr" runat="server" Text=""></asp:Label>
        </asp:Panel>
		<asp:Label ID="lbYouremail" runat="server" Text="" CssClass="contactLabel floatLeft" AssociatedControlID="txtYouremail"/>
		<div class="form_right floatLeft">
            <asp:Label ID="lblYouremailRead" runat="server" Visible="False" class="contactLabel" style="width:auto;"/>
            <asp:TextBox ID="txtYouremail" runat="server" Width="310px" CssClass="lfloat"></asp:TextBox>
		</div>
        <div class="clearboth"></div>
	</li>

	<li id="liYourName" runat="server">
        <asp:Panel ID="pnlYourNameErr" runat="server" CssClass="alert alert-danger" Visible="False">
            <asp:Label ID="lbYourNameErr" runat="server" Text=""></asp:Label>
        </asp:Panel>
					
        <asp:Label ID="lbYourName" runat="server" Text="" class="contactLabel floatLeft" AssociatedControlID="txtYourName"/>
		<div class="form_right floatLeft">
            <asp:Label ID="lblYourNameRead" runat="server" Visible="False" class="contactLabel" style="width:auto;"/>
            <asp:TextBox ID="txtYourName" runat="server" Width="310px"/>
		</div>
        <div class="clearboth"></div>
	</li>
	<li>
        <asp:Label ID="lbSubject" runat="server" Text="" class="contactLabel floatLeft" AssociatedControlID="txtSubject"/>
		<div class="form_right floatLeft">
            <asp:TextBox ID="txtSubject" runat="server" Width="310px"/>
		</div>
        <div class="clearboth"></div>
	</li>
	<li>
        <asp:Label ID="lbMessage" runat="server" Text="" class="contactLabel floatLeft" AssociatedControlID="txtMessage"/>
		<div class="form_right floatLeft">
            <asp:TextBox ID="txtMessage" runat="server" Width="310px" TextMode="MultiLine" Height="100px"/>
		</div>
        <div class="clearboth"></div>
	</li>
</ul>
<div class="clearboth padding"></div>
<div class="form-actions">
<table class="center" cellpadding="0" cellspacing="0">
    <tr>
        <td><div style="width:100px">&nbsp;</div></td>
        <td><dx:ASPxButton ID="cmdSendDef" runat="server" Text="Send" 
                Font-Size="15px" Visible="false">
            </dx:ASPxButton>
            <dx:ASPxButton ID="cmdSend" runat="server" CausesValidation="True"
                    Text="Send"
                    ValidationGroup="RegisterGroup"  EncodeHtml="False" Font-Bold="True"
                    Height="41px" BackColor="#BF2130" EnableDefaultAppearance="False" 
                Font-Size="Smaller" ForeColor="White" Width="100px">
                    <Border BorderWidth="1px"></Border>
                    <ClientSideEvents />
                </dx:ASPxButton></td>
        <td><div style="width:50px">&nbsp;</div></td>
        <td><input type="reset" id="cmdReset" runat="server" class="linkbutton" style="font-size:10px;"
            value="Reset" onclick="return cmdReset_onclick()" /></td>
    </tr>
</table>
</div>
            </asp:Panel>

        </asp:Panel>

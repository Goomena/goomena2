﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UCSelectProductMembers.ascx.vb" Inherits="Dating.Referrals.Server.Site.Web.UCSelectProductMembers" %>
<%@ Register src="UCPrice.ascx" tagname="UCPrice" tagprefix="uc1" %>
<style>
    .ordbut { position: absolute; bottom: 20px; right: 20%; }
    .ordbut2 { position: absolute; bottom: 20px; right: 15%; }
    .pricecol { position: relative; width: 195px; height: 263px;font-family:"Segoe UI","Arial"; }
    .paymentsimgs { margin-right: auto; margin-left: auto; width: 315px; margin-top: 70px; margin-bottom: 70px; }
    .pricecolpadding { padding-right: 14px; }
    .pricetable { margin-left: 33px; }
    .pricecol-credits { text-align:center;font-size: 37px;line-height: 37px;padding-top: 13px;}
    .pricecol-credits-text { text-align:center;font-size: 20px;line-height: 17px;}
    .pricecol-price {position:absolute;top:108px;left:0px;right:0px; }
    .pricecol-price-text {text-align:center;font-size:42px;line-height:42px;color:#39B54A; }
    .pricecol-expire {position:absolute;top:149px;left:0px;right:0px; }
    .pricecol-expire-text {text-align:center;font-size:11px;}
    .pricecol-price.small {top:118px; }
    .pricecol-price.small .pricecol-price-text{font-size:22px;line-height:22px; }
    .pricecol-expire.small .pricecol-expire-text{}
    .timesdiv1{}
    .bonus-code{background-color:#F3F3F3;font-size:18px;text-align:center;border:1px solid #E6E6E6;padding:20px;}
    .bonus-code .button{background-color:#25AAE1;color:#fff;padding:5px 10px;}
    .bonus-code .textbox{width:180px;height:25px;font-size:16px;line-height:25px;}
</style>

<div>
    <asp:Label ID="lblPaymentsGuarantee" runat="server" Text=""></asp:Label>
</div>
<asp:Panel ID="pnlBonus" runat="server" CssClass="bonus-code">
    <div align="center">
        <asp:Label ID="lblBonusCode" runat="server" Text="" style="color:#25AAE1;"/>
        <asp:Label ID="lblBonusCodeSuccess" runat="server" ForeColor="Green" 
            ViewStateMode="Disabled"/>
        <div style="height:20px"></div>
        <asp:TextBox ID="txtBonusCode" runat="server" CssClass="textbox"></asp:TextBox>
        <asp:Button ID="btnBonusCredits" runat="server" Text="Υποβολή κωδικού" CssClass="button" />
        <div style="height:20px"></div>
        <asp:Label ID="lblBonusCodeError" runat="server" Text="" ForeColor="red" 
            ViewStateMode="Disabled"/>
    </div>
</asp:Panel>

<div class="paymentsimgs"><img src="../Images2/pricing-members/payment.png" alt="Payment Methods"/></div>
<div style="margin-bottom:20px">
    <asp:Label ID="lblSelectProduct" runat="server" Text=""></asp:Label>
</div>


<table cellspacing="0" cellpadding="0" style="">
    <tr>
        <td style="width:195px;height:263px;">
            <uc1:UCPrice ID="UCPrice1" runat="server" View="vwMember" />
        </td>
        <td style="width:20px;">
        &nbsp;
        </td>
        <td style="width:195px;height:263px;">
            <uc1:UCPrice ID="UCPrice2" runat="server" View="vwMember" />
        </td>
        <td style="width:20px;">
        &nbsp;
        </td>
        <td style="width:195px;height:263px;">
            <uc1:UCPrice ID="UCPrice3" runat="server" View="vwMember" />
        </td>
    </tr>
</table>

<%--<table class="pricetable">
<tr>
<td class="pricecol pricecolpadding" style="background-image: url('../Images2/pricing-members/100credits.png'); background-repeat: no-repeat;width:195px;height:263px;"align="center">
<div class="pricecol"><asp:ImageButton ID="img1000Credits" runat="server" CommandArgument="1000Credits,45,15" align="center" ImageUrl="~/Images2/pricing/order-button.png" CssClass="ordbut" /></div>
</td>
<td class="pricecol" style="background-image: url('../Images2/pricing-members/3000credits.png'); background-repeat: no-repeat;width:195px;height:263px;"align="center">
<div class="pricecol"><asp:ImageButton ID="img3000Credits" runat="server" CommandArgument="3000Credits,120,36" align="center" ImageUrl="~/Images2/pricing/order-button.png" CssClass="ordbut" /></div>
</td>
<td class="pricecol" style="background-image: url('../Images2/pricing-members/6000credits.png'); background-repeat: no-repeat;width:195px;height:263px;"align="center">
<div class="pricecol"><asp:ImageButton ID="img6000Credits" runat="server" CommandArgument="6000Credits,180,66" align="center" ImageUrl="~/Images2/pricing/order-button.png" CssClass="ordbut2" /></div>
</td>
</tr>
</table>--%>



<div>
    <dx:ASPxLabel runat="server" EncodeHtml="False" ClientIDMode="AutoID" ID="lbfootText">
    </dx:ASPxLabel>
</div>


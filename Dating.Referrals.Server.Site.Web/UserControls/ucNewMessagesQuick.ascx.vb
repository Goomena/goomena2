﻿Imports Dating.Server.Core.DLL
Imports DevExpress.Web.ASPxEditors

Public Class ucNewMessagesQuick
    Inherits BaseUserControl


    Dim _pageData As clsPageData
    Public ReadOnly Property CurrentPageData As clsPageData
        Get
            If (_pageData Is Nothing) Then _pageData = New clsPageData("~/Members/Messages.aspx", Context)
            Return _pageData
        End Get
    End Property


    Public Property ItemsPerPage As Integer
        Get
            If (Me.ViewState("ItemsPerPage") IsNot Nothing) Then Return Me.ViewState("ItemsPerPage")
            Return 10
        End Get
        Set(value As Integer)
            Me.ViewState("ItemsPerPage") = value
        End Set
    End Property



    Private Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        dvMessages.DataBind()
    End Sub

    'Protected Overrides Sub OnPreRender(e As System.EventArgs)
    '    dvWinks.RowPerPage = Me.ItemsPerPage
    '    MyBase.OnPreRender(e)
    'End Sub



    Protected Sub sdsMessages_Selecting(sender As Object, e As System.Web.UI.WebControls.SqlDataSourceSelectingEventArgs) Handles sdsMessages.Selecting
        For Each prm As SqlClient.SqlParameter In e.Command.Parameters

            If (prm.ParameterName = "@CurrentProfileId") Then
                Dim MasterProfileId = Me.MasterProfileId
                prm.Value = MasterProfileId
            ElseIf (prm.ParameterName = "@ReturnRecordsWithStatus") Then
                prm.Value = ProfileStatusEnum.Approved
            ElseIf (prm.ParameterName = "@NumberOfRecordsToReturn") Then
                prm.Value = Me.ItemsPerPage
            End If

        Next
    End Sub



    Protected Sub dvMessages_DataBound(sender As Object, e As EventArgs) Handles dvMessages.DataBound
        Try
            For Each dvi As DevExpress.Web.ASPxDataView.DataViewItem In dvMessages.Items
                Try

                    Dim dr As DataRowView = dvi.DataItem

                    Dim lblSubject As ASPxLabel = dvMessages.FindItemControl("lblSubject", dvi)
                    If (dr("ProfileID") = 1) Then
                        lblSubject.Text = dr("Subject")
                    ElseIf (dr("CommunicationStatus") = 0) Then
                        lblSubject.Text = "You received a message from " & dr("LoginName")
                    Else
                        lblSubject.Text = dr("Subject")
                    End If

                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "")
                End Try
            Next

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Protected Function GetCountMessagesString(ItemsCount As Object) As String

        Try
            If (Not AppUtils.IsDBNullOrNothingOrEmpty(ItemsCount)) Then
                Try
                    If (ItemsCount > 0) Then Return "(" & ItemsCount & ")"
                Catch ex As Exception

                End Try
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Return ""
    End Function


    Protected Function IsVisible(ItemsCount As Object) As Boolean

        If (Not AppUtils.IsDBNullOrNothingOrEmpty(ItemsCount)) Then
            Try
                If (ItemsCount > 1) Then Return True
            Catch ex As Exception

            End Try
        End If

        Return False
    End Function

End Class
﻿Imports Dating.Server.Core.DLL

Public Class ProfileCreditsHistory
    Inherits BaseUserControl


#Region "Props"


    Dim _pageData As clsPageData
    Protected Property CurrentPageData As clsPageData
        Get
            ' let it read custom strings from container page
            If (_pageData Is Nothing) Then _pageData = New clsPageData(Context)
            Return _pageData
        End Get
        Set(value As clsPageData)
            _pageData = value
        End Set
    End Property

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            If (Not Page.IsPostBack) Then
                LoadLAG()
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try
    End Sub


    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
    End Sub


    Protected Sub LoadLAG()
        Try

            gv.Columns("Date").Caption = CurrentPageData.GetCustomString("CreditsHistoryTableCaption_Date")
            gv.Columns("Action").Caption = CurrentPageData.GetCustomString("CreditsHistoryTableCaption_Action")
            gv.Columns("Description").Caption = CurrentPageData.GetCustomString("CreditsHistoryTableCaption_Description")

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Public Sub Bind()
        gv.DataBind()
    End Sub

    Protected Sub gvHistory_CustomColumnDisplayText(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewColumnDisplayTextEventArgs) Handles gv.CustomColumnDisplayText

        Try
            If e.Column.FieldName = "Action" Then

                Dim dr As System.Data.DataRow = gv.GetDataRow(e.VisibleRowIndex)
                If (dr("CustomerTransactionID") > 0) Then
                    If (dr("Expired") = True) Then
                        e.DisplayText = Me.CurrentPageData.GetCustomString("CreditsAction_Expired")
                    Else
                        e.DisplayText = Me.CurrentPageData.GetCustomString("CreditsAction_Credited")
                    End If

                ElseIf (dr("CreditsType") = "UNLOCK_CONVERSATION") Then
                    e.DisplayText = Me.CurrentPageData.GetCustomString("CreditsAction_Used")
                ElseIf (dr("CreditsType") = "UNLOCK_MESSAGE_READ") Then
                    e.DisplayText = Me.CurrentPageData.GetCustomString("CreditsAction_Used")
                ElseIf (dr("CreditsType") = "UNLOCK_MESSAGE_SEND") Then
                    e.DisplayText = Me.CurrentPageData.GetCustomString("CreditsAction_Used")
                End If


            ElseIf e.Column.FieldName = "Description" Then

                Dim dr As System.Data.DataRow = gv.GetDataRow(e.VisibleRowIndex)
                Dim credits As Double = 0
                If (Not dr.IsNull("Credits")) Then
                    credits = Convert.ToDouble(dr("Credits"))
                    credits = Math.Abs(credits)
                End If

                If (dr("CustomerTransactionID") > 0) Then
                    If (dr("Expired") = True) Then
                        e.DisplayText = Me.CurrentPageData.GetCustomString("CreditsDescription_Expired").Replace("###CREDITS###", credits.ToString()).Replace("###PURCHASED###", CType(dr("DatePurchased"), DateTime).ToString("d MMM, yyyy H:mm"))
                        '"Your ###CREDITS### credits expired.<br/> Credits purchased on ###PURCHASED###"
                    Else
                        e.DisplayText = Me.CurrentPageData.GetCustomString("CreditsDescription_Credited").Replace("###CREDITS###", credits.ToString())
                        '"Purchased ###CREDITS### Credits"
                    End If

                ElseIf (dr("CreditsType") = "UNLOCK_CONVERSATION") Then
                    Dim profileUrl As String = ProfileHelper.GetProfileNavigateUrl(dr("ConversationWithLoginName").ToString())
                    profileUrl = "<a href=""" & profileUrl & """>" & dr("ConversationWithLoginName") & "</a>"
                    e.DisplayText = Me.CurrentPageData.GetCustomString("CreditsDescription_Used").Replace("###CREDITS###", credits.ToString()).Replace("###PROFILEVIEWURL###", profileUrl)
                    '"Spent ###CREDITS### credits to unlock conversation with ###PROFILEVIEWURL###"

                ElseIf (dr("CreditsType") = "UNLOCK_MESSAGE_READ") Then
                    Dim profileUrl As String = ProfileHelper.GetProfileNavigateUrl(dr("ConversationWithLoginName").ToString())
                    profileUrl = "<a href=""" & profileUrl & """>" & dr("ConversationWithLoginName") & "</a>"
                    e.DisplayText = Me.CurrentPageData.GetCustomString("CreditsDescription_UsedUnlockMessage_READ").Replace("###CREDITS###", credits.ToString()).Replace("###PROFILEVIEWURL###", profileUrl)

                ElseIf (dr("CreditsType") = "UNLOCK_MESSAGE_SEND") Then
                    Dim profileUrl As String = ProfileHelper.GetProfileNavigateUrl(dr("ConversationWithLoginName").ToString())
                    profileUrl = "<a href=""" & profileUrl & """>" & dr("ConversationWithLoginName") & "</a>"
                    e.DisplayText = Me.CurrentPageData.GetCustomString("CreditsDescription_UsedUnlockMessage_SEND").Replace("###CREDITS###", credits.ToString()).Replace("###PROFILEVIEWURL###", profileUrl)

                End If

            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub

    Protected Sub sdsCreditsHistory_Selecting(sender As Object, e As System.Web.UI.WebControls.SqlDataSourceSelectingEventArgs) Handles sdsCreditsHistory.Selecting
        For Each prm As SqlClient.SqlParameter In e.Command.Parameters
            If (prm.ParameterName = "@CustomerId") Then
                prm.Value = Me.MasterProfileId
                Exit For
            End If
        Next
    End Sub


End Class
﻿Imports System.Data.SqlClient
Imports Dating.Server.Core.DLL.DSMembers
Imports Dating.Server.Core.DLL
Imports Dating.Referrals.Server.Site.Web.TWLib
Imports DevExpress.Web.ASPxEditors
Imports Dating.Server.Core.DLL.DSLists

Public Class UCRegister
    Inherits BaseUserControl



    Dim _pageData As clsPageData
    Protected ReadOnly Property CurrentPageData As clsPageData
        Get
            If (_pageData Is Nothing) Then _pageData = New clsPageData("Register.aspx", Context)
            Return _pageData
        End Get
    End Property


    Public Property FBUser As FBUserInfo
        Get
            If (Session("FBUser") IsNot Nothing) Then
                Return Session("FBUser")
            End If
            Return Nothing
        End Get
        Set(ByVal value As FBUserInfo)
            Session("FBUser") = value
        End Set
    End Property


    Public Property TWUser As TWUserInfo
        Get
            If (Session("TWUser") IsNot Nothing) Then
                Return Session("TWUser")
            End If
            Return Nothing
        End Get
        Set(ByVal value As TWUserInfo)
            Session("TWUser") = value
        End Set
    End Property



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try



            'Try
            '    Dim cPageBasic As Dating.Server.Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            '    GeneralFunctions.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
            'Catch ex As Exception
            '    WebErrorMessageBox(Me, ex, "Page_Load")
            'End Try


            If (Not Page.IsPostBack) Then

                'Dim asd As Integer = HttpContext.Current.Session("LagID")

                If (Request.QueryString("ref") = "fb" AndAlso String.IsNullOrEmpty(Request.QueryString("log")) AndAlso String.IsNullOrEmpty(Request.QueryString("conn"))) Then
                    Response.Redirect("Default.aspx", False)
                    HttpContext.Current.ApplicationInstance.CompleteRequest()
                End If

                If (HttpContext.Current.Session("LagID") Is Nothing) Then
                    clsCurrentContext.SetLAGID()
                End If

                Try
                    Facebook_TryLoginWith()
                    Twitter_TryLoginWith()
                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "Trying to login with facebook or twitter")
                End Try

                Try
                    If (clsConfigValues.Get__validate_email_address_normal()) Then
                        txtEmail.ValidationSettings.RegularExpression.ValidationExpression = gEmailAddressValidationRegex
                    Else
                        txtEmail.ValidationSettings.RegularExpression.ValidationExpression = gEmailAddressValidationRegex_Simple
                    End If
                    LoadLAG()
                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "Page_Load")
                End Try


                'If clsCurrentContext.VerifyLogin() = True Then
                '    If Not Session("FiredRedirectOnDefaultPage") Then
                '        Session("FiredRedirectOnDefaultPage") = True
                '        Response.Redirect("~/Members/default.aspx", False)
                '        HttpContext.Current.ApplicationInstance.CompleteRequest()
                '    End If
                'End If

                'TogglePersonalCollapsible(False)
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try
    End Sub


    'Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender

    '    Try
    '        Dim cPageBasic As Dating.Server.Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
    '        GeneralFunctions.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
    '    Catch ex As Exception
    '        WebErrorMessageBox(Me, ex, "Page_Load")
    '    End Try

    'End Sub



    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
    End Sub



    Protected Sub LoadLAG()
        Try
            Dim cPageBasic As Dating.Server.Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic

            Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_Gender, Me.GetLag(), "GenderId", rblGender, True, False, "US")
            For Each itm As ListEditItem In rblGender.Items
                If (itm.Value = ProfileHelper.gFemaleGender.GenderId) Then
                    itm.Text = itm.Text & ": " & CurrentPageData.GetCustomString("rblGender_FemaleItemText")
                ElseIf (itm.Value = ProfileHelper.gMaleGender.GenderId) Then
                    itm.Text = itm.Text & ": " & CurrentPageData.GetCustomString("rblGender_MaleItemText")
                End If
            Next

            'Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_AccountType, Me.GetLag(), "AccountTypeId", cbAccountType, True)
            'cbAccountType.Items(0).Selected = True

            SetControlsValue(Me, CurrentPageData)

            'msg_IAm.Text = CurrentPageData.GetCustomString("msg_IAm")
            msg_WhoIs.Text = CurrentPageData.GetCustomString("msg_WhoIs")
            msg_MyEmail.Text = CurrentPageData.GetCustomString("msg_MyEmail")
            msg_MyUsername.Text = CurrentPageData.GetCustomString("msg_MyUsername")
            msg_MyPassword.Text = CurrentPageData.GetCustomString("msg_MyPassword")
            msg_MyPasswordConfirm.Text = CurrentPageData.GetCustomString("msg_MyPasswordConfirm")
            msg_MyBirthdate.Text = CurrentPageData.GetCustomString("msg_MyBirthdate")
            msg_SelectRegion.Text = CurrentPageData.GetCustomString("msg_SelectRegion")

            'lnkLogin.Text = CurrentPageData.GetCustomString("lnkLogin")
            rblGender.ValidationSettings.RequiredField.ErrorText = CurrentPageData.GetCustomString("rblGender_Required_ErrorText")

            'cbAccountType.ValidationSettings.ErrorText = CurrentPageData.GetCustomString("cbAccountType_Required_ErrorText")
            'cbAccountType.ValidationSettings.RequiredField.ErrorText = CurrentPageData.GetCustomString("cbAccountType_Required_ErrorText")

            txtEmail.ValidationSettings.RequiredField.ErrorText = CurrentPageData.GetCustomString("txtEmail_Required_ErrorText")
            txtEmail.ValidationSettings.RegularExpression.ErrorText = CurrentPageData.GetCustomString("txtEmail_Validation_ErrorText")

            txtLogin.ValidationSettings.ErrorText = CurrentPageData.GetCustomString("txtLogin_Validation_ErrorText")
            txtLogin.ValidationSettings.RequiredField.ErrorText = CurrentPageData.GetCustomString("txtLogin_Required_ErrorText")
            txtLogin.ValidationSettings.RegularExpression.ErrorText = CurrentPageData.GetCustomString("txtLogin_Validation_RegularExpression_ErrorText")

            txtPasswrd.ValidationSettings.ErrorText = CurrentPageData.GetCustomString("txtPasswrd_Validation_ErrorText")
            txtPasswrd.ValidationSettings.RequiredField.ErrorText = CurrentPageData.GetCustomString("txtPasswrd_Required_ErrorText")
            txtPasswrd.ValidationSettings.RegularExpression.ErrorText = CurrentPageData.GetCustomString("txtPasswrd_Validation_RegularExpression_ErrorText")

            txtPasswrd1Conf.ValidationSettings.ErrorText = CurrentPageData.GetCustomString("txtPasswrd1Conf_Validation_ErrorText")

            cbAgreements.Text = CurrentPageData.GetCustomString("cbAgreements_Text")
            cbAgreements.ValidationSettings.RequiredField.ErrorText = CurrentPageData.GetCustomString("cbAgreements_Required_ErrorText")
            cbAgreements.ErrorText = CurrentPageData.GetCustomString("cbAgreements_Required_ErrorText")

            lnkToS.Text = CurrentPageData.GetCustomString("lnkToS_Text")
            lnkToS.NavigateUrl = CurrentPageData.GetCustomString("lnkToS.NavigateUrl")
            If (lnkToS.Text.Length = 0) Then lnkToS.Visible = False

            btnRegister.Text = CurrentPageData.GetCustomString("btnRegister_Text")
            lblContinueMessageCompleteForm.Text = CurrentPageData.GetCustomString(lblContinueMessageCompleteForm.ID)


            'lblHeaderRight.Text = CurrentPageData.GetCustomString("lblHeaderRight")
            'lblTextRight.Text = CurrentPageData.GetCustomString("lblTextRight")

            msg_CountryText.Text = CurrentPageData.GetCustomString("msg_CountryText")
            msg_RegionText.Text = CurrentPageData.GetCustomString("msg_RegionText")
            msg_CityText.Text = CurrentPageData.GetCustomString("msg_CityText")
            msg_ZipCodeText.Text = CurrentPageData.GetCustomString("msg_ZipCodeText")

            cbCountry.ValidationSettings.ErrorText = CurrentPageData.GetCustomString("lblCountryErr")
            cbRegion.ValidationSettings.ErrorText = CurrentPageData.GetCustomString("lblRegionErr")
            cbCity.ValidationSettings.ErrorText = CurrentPageData.GetCustomString("lblCityErr")

            'Web.ClsCombos.FillCombo(gLAG.ConnectionString, "SYS_CountriesGEO", "PrintableName", "Iso", cbCountry, True, False, "PrintableName", "ASC")
            Web.ClsCombos.FillComboUsingDatatable(Lists.gDSListsGEO__IsEnabled_PrintableNameASC.SYS_CountriesGEO, "PrintableName", "Iso", cbCountry, True, "PrintableName")
            Dim def As DevExpress.Web.ASPxEditors.ListEditItem = cbCountry.Items.FindByValue(Session("GEO_COUNTRY_CODE"))
            If (def IsNot Nothing) Then
                def.Selected = True
                'sdsRegion.SelectCommand = "SELECT DISTINCT region1 FROM dbo.SYS_GEO_" & cbCountry.SelectedItem.Value & " ORDER BY region1"
                'sdsRegion.SelectCommand = "SELECT DISTINCT region1 FROM dbo.SYS_GEO_GR where countrycode=N'" & cbCountry.SelectedItem.Value & "' ORDER BY region1"
                If (Session("LAGID") = "GR") Then
                    sdsRegion.SelectCommand = "SELECT DISTINCT region1 FROM dbo.SYS_GEO_GR WHERE  countrycode=N'" & cbCountry.SelectedItem.Value & "' and ((countrycode=N'GR' and language=N'EL') or countrycode<>N'GR') ORDER BY region1"
                Else
                    sdsRegion.SelectCommand = "SELECT DISTINCT region1 FROM dbo.SYS_GEO_GR WHERE countrycode=N'" & cbCountry.SelectedItem.Value & "' and ((countrycode=N'GR' and language=N'EN') or countrycode<>N'GR') ORDER BY region1"
                End If

                cbRegion.TextField = "region1"
                cbRegion.DataBind()
            Else
                def = New DevExpress.Web.ASPxEditors.ListEditItem(CurrentPageData.GetCustomString("cbCountry_default"), "-1")
                def.Selected = True
                cbCountry.Items.Insert(0, def)
            End If


            'Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_Height, GetLag(), "HeightId", cbHeight, True, "US")
            ''Dim HeightId As Integer = Lists.gDSLists.EUS_LISTS_Height.Single(Function(itm As DSLists.EUS_LISTS_HeightRow) itm.US = "170cm").HeightId
            ''If (Not SelectComboItem(cbHeight, HeightId)) Then
            'cbHeight.Items(0).Selected = True
            ''End If

            'Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_BodyType, GetLag(), "BodyTypeId", cbBodyType, True, "US")
            ''Dim bodtype As Integer = Lists.gDSLists.EUS_LISTS_BodyType.Single(Function(itm As DSLists.EUS_LISTS_BodyTypeRow) itm.US = "Average").BodyTypeId
            ''If (Not SelectComboItem(cbBodyType, bodtype)) Then
            'cbBodyType.Items(0).Selected = True
            ''End If

            'Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_EyeColor, GetLag(), "EyeColorId", cbEyeColor, True, "US")
            'cbEyeColor.Items(0).Selected = True

            'Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_HairColor, GetLag(), "HairColorId", cbHairClr, True, "US")
            ''Dim HairColorId As Integer = Lists.gDSLists.EUS_LISTS_HairColor.Single(Function(itm As DSLists.EUS_LISTS_HairColorRow) itm.US = "Black").HairColorId
            ''If (Not SelectComboItem(cbHairClr, HairColorId)) Then
            'cbHairClr.Items(0).Selected = True
            'End If

            'Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_RelationshipStatus, GetLag(), "RelationshipStatusId", cbRelationshipStatus, True, "US")
            ''Dim RelationshipStatusId As Integer = Lists.gDSLists.EUS_LISTS_RelationshipStatus.Single(Function(itm As DSLists.EUS_LISTS_RelationshipStatusRow) itm.US = "Single").RelationshipStatusId
            ''If (Not SelectComboItem(cbRelationshipStatus, RelationshipStatusId)) Then
            'cbRelationshipStatus.Items(0).Selected = True
            '' End If


            'Try

            '    Dim _dt As New DataTable
            '    _dt.Columns.Add("TypeOfDatingId")
            '    _dt.Columns.Add("TypeOfDateChecked")
            '    _dt.Columns.Add("TypeOfDateText")

            '    Dim i As Integer = 0
            '    For i = 0 To (Lists.gDSLists.EUS_LISTS_TypeOfDating.Rows.Count - 1)
            '        Try
            '            Dim dr As DataRow = _dt.NewRow()

            '            dr("TypeOfDateText") = Lists.gDSLists.EUS_LISTS_TypeOfDating.Rows(i)(GetLag())
            '            dr("TypeOfDateChecked") = False
            '            Select Case Lists.gDSLists.EUS_LISTS_TypeOfDating.Rows(i)("TypeOfDatingId")
            '                Case TypeOfDatingEnum.AdultDating_Casual
            '                    dr("TypeOfDatingId") = TypeOfDatingEnum.AdultDating_Casual.ToString()

            '                Case TypeOfDatingEnum.Friendship
            '                    dr("TypeOfDatingId") = TypeOfDatingEnum.Friendship.ToString()

            '                Case TypeOfDatingEnum.LongTermRelationship
            '                    dr("TypeOfDatingId") = TypeOfDatingEnum.LongTermRelationship.ToString()

            '                Case TypeOfDatingEnum.MarriedDating
            '                    dr("TypeOfDatingId") = TypeOfDatingEnum.MarriedDating.ToString()

            '                Case TypeOfDatingEnum.MutuallyBeneficialArrangements
            '                    dr("TypeOfDatingId") = TypeOfDatingEnum.MutuallyBeneficialArrangements.ToString()
            '                    'dr("TypeOfDateChecked") = True

            '                Case TypeOfDatingEnum.ShortTermRelationship
            '                    dr("TypeOfDatingId") = TypeOfDatingEnum.ShortTermRelationship.ToString()
            '                    'dr("TypeOfDateChecked") = True

            '            End Select

            '            _dt.Rows.Add(dr)
            '        Catch ex As Exception
            '            WebErrorMessageBox(Me, ex, "")
            '        End Try
            '    Next

            '    lvTypeOfDate.DataSource = _dt
            '    lvTypeOfDate.DataBind()
            'Catch ex As Exception
            '    WebErrorMessageBox(Me, ex, "")
            'End Try

            If (Me.FBUser IsNot Nothing) Then
                'RegisterWithFacebookInformation()

                'If (Page.IsValid AndAlso ValidateInput()) Then
                'CreateNewProfile()
                'End If
                RegisterWithFacebookInformation()
                pnlContinueMessageCompleteForm.Visible = True

            ElseIf (Me.TWUser IsNot Nothing) Then
                'RegisterWithFacebookInformation()

                'If (Page.IsValid AndAlso ValidateInput()) Then
                'CreateNewProfile()
                'End If
                pnlContinueMessageCompleteForm.Visible = True
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Private Sub Facebook_TryLoginWith()
        ' conn - connect from settings
        If (Request.QueryString("log") = "fb") Then

            Dim Appid As String = ConfigurationManager.AppSettings("FBApp_ID")
            Dim AppSecret As String = ConfigurationManager.AppSettings("FBApp_Secret")
            Dim ReplyURL As String = ConfigurationManager.AppSettings("FBRedirect")
            ReplyURL = HttpUtility.UrlEncode(ReplyURL)

            If (String.IsNullOrEmpty(Request.QueryString("code"))) Then
                Try

                    Dim strOut As String = FBLib.FBLoginURL(Appid, AppSecret, ReplyURL)
                    Response.Redirect(strOut, False)
                    HttpContext.Current.ApplicationInstance.CompleteRequest()

                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "Page_Load")
                End Try
            ElseIf (Not String.IsNullOrEmpty(Request.QueryString("code"))) Then
                Try

                    Me.FBUser = FBLib.FBLoadPage(Appid, AppSecret, ReplyURL, Request.QueryString("code"))
                    'Me.LoginWithFacebookInformation()

                    Dim result As clsDataRecordLoginMemberReturn = gLogin.PerfomReloginWithFacebookId(Me.FBUser.uid, True, True)
                    If (result.IsValid AndAlso Not result.HasErrors) Then
                        Response.Redirect(System.Web.VirtualPathUtility.ToAbsolute("~/Members/"), False)
                        HttpContext.Current.ApplicationInstance.CompleteRequest()
                    End If

                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "Page_Load")
                End Try
            End If

        ElseIf (Request.QueryString("conn") = "fb") Then

            Dim Appid As String = ConfigurationManager.AppSettings("FBApp_ID")
            Dim AppSecret As String = ConfigurationManager.AppSettings("FBApp_Secret")
            Dim ReplyURL As String = ConfigurationManager.AppSettings("FBRedirect")
            ReplyURL = ReplyURL.Replace("log=fb", "conn=fb")
            ReplyURL = HttpUtility.UrlEncode(ReplyURL)

            If (String.IsNullOrEmpty(Request.QueryString("code"))) Then
                Try

                    Dim strOut As String = FBLib.FBLoginURL(Appid, AppSecret, ReplyURL)
                    Response.Redirect(strOut, False)
                    HttpContext.Current.ApplicationInstance.CompleteRequest()

                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "Redirecting to Facebook")
                End Try
            ElseIf (Not String.IsNullOrEmpty(Request.QueryString("code"))) Then
                Try

                    Me.FBUser = FBLib.FBLoadPage(Appid, AppSecret, ReplyURL, Request.QueryString("code"))
                    DataHelpers.UpdateEUS_Profiles_FacebookData(Me.MasterProfileId, Me.FBUser.uid, Me.FBUser.name, Me.FBUser.username)

                    If (Me.MasterProfileId > 0) Then
                        Response.Redirect(System.Web.VirtualPathUtility.ToAbsolute("~/Members/Settings.aspx"), False)
                        HttpContext.Current.ApplicationInstance.CompleteRequest()
                    Else
                        Dim result As clsDataRecordLoginMemberReturn = gLogin.PerfomReloginWithFacebookId(Me.FBUser.uid, True, True)
                        If (result.IsValid AndAlso Not result.HasErrors) Then
                            Response.Redirect(System.Web.VirtualPathUtility.ToAbsolute("~/Members/Settings.aspx"), False)
                            HttpContext.Current.ApplicationInstance.CompleteRequest()
                        End If
                    End If

                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "Page_Load")
                End Try
            End If

        End If

    End Sub


    Private Sub Twitter_TryLoginWith()
        If (Request.QueryString("log") = "tw") Then

            Dim url As String = ""
            Dim xml As String = ""
            Dim oAuth As New oAuthTwitter()

            If Request("oauth_token") Is Nothing Then
                Try
                    'Redirect the user to Twitter for authorization.
                    'Using oauth_callback for local testing.
                    oAuth.CallBackUrl = System.Configuration.ConfigurationManager.AppSettings("TWRedirect")
                    Response.Redirect(oAuth.AuthorizationLinkGet(), False)
                    HttpContext.Current.ApplicationInstance.CompleteRequest()

                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "Page_Load")
                End Try
            Else
                Try
                    'Get the access token and secret.
                    oAuth.AccessTokenGet(Request("oauth_token"), Request("oauth_verifier"))
                    If oAuth.TokenSecret.Length > 0 Then
                        'We now have the credentials, so make a call to the Twitter API.
                        url = "http://twitter.com/account/verify_credentials.xml"
                        xml = oAuth.oAuthWebRequest(oAuthTwitter.Method.[GET], url, [String].Empty)
                        Me.TWUser = TWUserInfo.GetInfo(xml)


                        Dim result As clsDataRecordLoginMemberReturn = gLogin.PerfomReloginWithTwitterId(Me.TWUser.uid, True, True)
                        If (result.IsValid AndAlso Not result.HasErrors) Then
                            Response.Redirect(System.Web.VirtualPathUtility.ToAbsolute("~/Members/"), False)
                            HttpContext.Current.ApplicationInstance.CompleteRequest()
                        End If

                        'POST Test
                        'url = "http://twitter.com/statuses/update.xml";
                        'xml = oAuth.oAuthWebRequest(oAuthTwitter.Method.POST, url, "status=" + oAuth.UrlEncode("Hello @swhitley - Testing the .NET oAuth API"));
                        'apiResponse.InnerHtml = Server.HtmlEncode(xml);
                        'apiResponse.InnerHtml = Server.HtmlEncode(xml)
                    End If

                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "Page_Load")
                End Try
            End If
        End If
    End Sub



    Private Sub PerformLogin(ByRef ds As DSMembers, ByRef profile As EUS_ProfilesRow, ByVal rememberUserName As Boolean)
        Try
            'Create Form Authentication ticket
            Dim ticket As New FormsAuthenticationTicket(1, profile.LoginName, DateTime.Now, DateTime.Now.AddDays(5), rememberUserName, "MEMBER", FormsAuthentication.FormsCookiePath)

            'For security reasons we may hash the cookies
            Dim hashCookies As String = FormsAuthentication.Encrypt(ticket)
            Dim cookie As New HttpCookie(FormsAuthentication.FormsCookieName, hashCookies)

            cookie.Path = FormsAuthentication.FormsCookiePath()
            If rememberUserName Then
                cookie.Expires = ticket.Expiration
            End If

            'add the cookie to user browser
            Response.Cookies.Remove(cookie.Name)
            Response.Cookies.Add(cookie)


            Dim sesVars As clsSessionVariables = clsSessionVariables.GetCurrent()
            sesVars.MemberData.Fill(profile)
            Session("LagID") = sesVars.MemberData.LAGID
            Session("ProfileID") = sesVars.MemberData.ProfileID
            Session("MirrorProfileID") = sesVars.MemberData.MirrorProfileID


            'new profile auto approve
            Dim isAutoApproveOn As Boolean
            Try
                Dim dt As DataTable = DataHelpers.GetDataTable("   select ISNULL(ConfigValue,0) as ConfigValue from SYS_Config where ConfigName='auto_approve_new_profiles' ")
                If (dt.Rows.Count > 0 AndAlso dt.Rows(0)("ConfigValue") = "1") Then
                    isAutoApproveOn = True
                End If
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "AutoApprovePhoto failed.")
            End Try


            'new profile auto approve
            If (isAutoApproveOn) Then
                Try
                    DataHelpers.ApproveProfile(ds, True)
                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "AutoApproveProfile failed.")
                End Try
                Try
                    clsUserNotifications.SendEmailNotification_OnAdminApprove(profile, GetLag())
                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "")
                End Try
            End If

            SendNotificationEmailToSupport(profile, isAutoApproveOn)
            'CreateAutonotificationRecord(profile)


            Try
                clsUserNotifications.SendNotificationsToMembers_AboutNewMember(profile.ProfileID, False)
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try


            Try
                DataHelpers.LogProfileAccess(profile.ProfileID, txtLogin.Text, txtPasswrd.Text, True, "Register")
            Catch ex As Exception
                'WebErrorMessageBox(Me, ex, "")
            End Try

            SessionVariables.DateTimeToRegister = Date.UtcNow

            ' redirect new user to profile editing page
            'Dim returnUrl As String = Page.ResolveUrl("~/Members/Profile.aspx?do=edit")
            'Dim returnUrl As String = Page.ResolveUrl("~/Members/Photos.aspx")
            Dim returnUrl As String = Page.ResolveUrl("~/AfterRegister.aspx")
            Response.Redirect(returnUrl, False)
            HttpContext.Current.ApplicationInstance.CompleteRequest()

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub



    Protected Sub btnRegister_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnRegister.Click
        Try
            pnlContinueMessageCompleteForm.Visible = False
            If (Page.IsValid AndAlso ValidateInput()) Then
                CreateNewProfile()
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub

    Private Sub RegisterWithFacebookInformation()

        Try

            If (Not String.IsNullOrEmpty(Me.FBUser.birthday_date) AndAlso Me.FBUser.birthday_date.Trim() <> "") Then
                TrMyBirthdate.Visible = False
            End If

            If (Not String.IsNullOrEmpty(Me.FBUser.email) AndAlso Me.FBUser.email.Trim() <> "") Then
                TrMyEmail.Visible = False
            End If

            If (Not String.IsNullOrEmpty(Me.FBUser.sex) AndAlso (Me.FBUser.sex.Trim() = "male" OrElse Me.FBUser.sex.Trim() = "female")) Then
                TrGender.Visible = False
                'TrAccountType.Visible = False
            End If

            'TrMyPassword.Visible = False
            'TrMyPasswordConfirm.Visible = False



            txtEmail.Text = Me.FBUser.email
            txtEmail.Text = txtEmail.Text.Trim()

            'txtLogin.Text = Me.FBUser.username
            'txtLogin.Text = txtLogin.Text.Trim()
            'If (String.IsNullOrEmpty(txtLogin.Text)) Then
            '    Dim emailPart As String() = Me.FBUser.email.Split("@")
            '    txtLogin.Text = emailPart(0)
            'End If

            If (Me.FBUser.sex = "male") Then
                ' sex
                Dim item As DevExpress.Web.ASPxEditors.ListEditItem = rblGender.Items.FindByValue(ProfileHelper.gMaleGender.GenderId.ToString())
                If (item IsNot Nothing) Then item.Selected = True

                '' account
                'item = cbAccountType.Items.FindByValue(ProfileHelper.Generous_AccountTypeId.ToString())
                'If (item IsNot Nothing) Then item.Selected = True

            ElseIf (Me.FBUser.sex = "female") Then
                ' sex
                Dim item As DevExpress.Web.ASPxEditors.ListEditItem = rblGender.Items.FindByValue(ProfileHelper.gFemaleGender.GenderId.ToString())
                If (item IsNot Nothing) Then item.Selected = True

                '' account
                'item = cbAccountType.Items.FindByValue(ProfileHelper.Attractive_AccountTypeId.ToString())
                'If (item IsNot Nothing) Then item.Selected = True

            Else ' sex not returned from fb
                TrGender.Visible = True

            End If

            'txtPasswrd.Text = Guid.NewGuid().ToString("N")
            'txtPasswrd1Conf.Text = txtPasswrd.Text
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub



    Private Function ValidateInput() As Boolean

        Dim isValid As Boolean = True

        ' perform checks, in case user has bypass validation on client
        Try
            serverValidationList.Items.Clear()

            If (Not cbAgreements.Checked) Then
                Dim txt As String = CurrentPageData.GetCustomString("cbAgreements_Required_ErrorText")
                txt = StripTags(txt)
                Dim itm As ListItem = New ListItem(txt, "javascript:" & cbAgreements.ClientInstanceName & ".SetFocus();")
                itm.Attributes.Add("class", "BulletedListItem")
                serverValidationList.Items.Add(itm)
                cbAgreements.IsValid = False
                isValid = False
                Return isValid
            End If

            If (rblGender.SelectedItem Is Nothing OrElse
                rblGender.SelectedItem.Value <> ProfileHelper.gMaleGender.GenderId.ToString() AndAlso
                rblGender.SelectedItem.Value <> ProfileHelper.gFemaleGender.GenderId.ToString()) Then
                Dim txt As String = CurrentPageData.GetCustomString("rblGender_Required_ErrorText")

                txt = StripTags(txt)
                Dim itm As ListItem = New ListItem(txt, "javascript:" & rblGender.ClientInstanceName & ".SetFocus();")
                itm.Attributes.Add("class", "BulletedListItem")
                serverValidationList.Items.Add(itm)
                rblGender.IsValid = False
                isValid = False
                Return isValid
            End If

            'If (cbAccountType.SelectedItem.Value <> ProfileHelper.gGenerousMember.AccountTypeId.ToString() AndAlso cbAccountType.SelectedItem.Value <> ProfileHelper.gAttractiveMember.AccountTypeId.ToString()) Then
            '    Dim txt As String = CurrentPageData.GetCustomString("cbAccountType_Required_ErrorText")
            '    txt = StripTags(txt)
            '    Dim itm As ListItem = New ListItem(txt, "javascript:" & cbAccountType.ClientInstanceName & ".SetFocus();")
            '    itm.Attributes.Add("class", "BulletedListItem")
            '    serverValidationList.Items.Add(itm)
            '    cbAccountType.IsValid = False
            '    isValid = False
            'End If

            txtEmail.Text = txtEmail.Text.Trim()
            If (txtEmail.Text.Length = 0) Then
                Dim txt As String = CurrentPageData.GetCustomString("txtEmail_Required_ErrorText")
                txt = StripTags(txt)
                Dim itm As ListItem = New ListItem(txt, "javascript:" & txtEmail.ClientInstanceName & ".SetFocus();")
                itm.Attributes.Add("class", "BulletedListItem")
                serverValidationList.Items.Add(itm)
                txtEmail.IsValid = False
                isValid = False
                Return isValid
            End If

            txtLogin.Text = txtLogin.Text.Trim()
            If (txtLogin.Text.Trim().Length = 0) Then
                Dim txt As String = CurrentPageData.GetCustomString("txtLogin_Required_ErrorText")
                txt = StripTags(txt)
                Dim itm As ListItem = New ListItem(txt, "javascript:" & txtLogin.ClientInstanceName & ".SetFocus();")
                itm.Attributes.Add("class", "BulletedListItem")
                serverValidationList.Items.Add(itm)
                txtLogin.IsValid = False
                isValid = False
                Return isValid
            End If

            If (txtPasswrd.Text.Trim().Length = 0) Then 'AndAlso Me.FBUser Is Nothing AndAlso Me.TWUser Is Nothing
                Dim txt As String = CurrentPageData.GetCustomString("txtPasswrd_Required_ErrorText")
                txt = StripTags(txt)
                Dim itm As ListItem = New ListItem(txt, "javascript:" & txtPasswrd.ClientInstanceName & ".SetFocus();")
                itm.Attributes.Add("class", "BulletedListItem")
                serverValidationList.Items.Add(itm)
                txtPasswrd.IsValid = False
                isValid = False
                Return isValid
            End If

            Try

                If (ctlDate.DateTime Is Nothing) Then
                    Dim txt As String = CurrentPageData.GetCustomString("ctlBirthday_Required_ErrorText")
                    txt = StripTags(txt)
                    Dim itm As ListItem = New ListItem(txt, "javascript:void(0);")
                    itm.Attributes.Add("class", "BulletedListItem")
                    serverValidationList.Items.Add(itm)
                    isValid = False
                    Return isValid
                Else
                    Dim age = ProfileHelper.GetCurrentAge(ctlDate.DateTime)
                    If (age < 18) Then
                        Dim txt As String = CurrentPageData.GetCustomString("ctlBirthday_AgeNotAllowed_ErrorText")
                        txt = StripTags(txt)
                        Dim itm As ListItem = New ListItem(txt, "javascript:void(0);")
                        itm.Attributes.Add("class", "BulletedListItem")
                        serverValidationList.Items.Add(itm)
                        isValid = False
                        Return isValid
                    End If
                End If

            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")

                Dim txt As String = CurrentPageData.GetCustomString("ctlBirthday_InvalidDate_ErrorText")
                txt = StripTags(txt)
                Dim itm As ListItem = New ListItem(txt, "javascript:void(0);")
                itm.Attributes.Add("class", "BulletedListItem")
                serverValidationList.Items.Add(itm)
                isValid = False
                Return isValid
            End Try

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        If (cbCountry.SelectedItem IsNot Nothing AndAlso cbCountry.SelectedItem.Value = "-1") Then

            cbCountry.Focus()
            Dim txt As String = CurrentPageData.GetCustomString("lblCountryErr")
            txt = StripTags(txt)
            Dim itm As ListItem = New ListItem(txt, "javascript:" & cbCountry.ClientInstanceName & ".SetFocus();")
            itm.Attributes.Add("class", "BulletedListItem")
            serverValidationList.Items.Add(itm)
            isValid = False
            Return isValid

        ElseIf (cbCountry.SelectedItem IsNot Nothing AndAlso
                cbCountry.SelectedItem.Value <> "-1" AndAlso
                clsGeoHelper.CheckCountryTable(cbCountry.SelectedItem.Value)) Then

            If (isValid) Then
                txtZip.Text = GeneralFunctions.GetZip(txtZip.Text)
                If (txtZip.Text.Trim() <> "") Then 'AndAlso hdfLocationStatus.Value = "zip"
                    Try

                        Dim dt As DataTable = clsGeoHelper.GetGEOByZip(cbCountry.SelectedItem.Value, txtZip.Text, GetLag())
                        If (dt.Rows.Count > 0) Then
                            If (Not SelectComboItem(cbRegion, dt.Rows(0)("region1"))) Then
                                cbRegion.Items.Add(dt.Rows(0)("region1"))
                                cbRegion.Items(cbRegion.Items.Count - 1).Selected = True
                            End If
                            If (Not SelectComboItem(cbCity, dt.Rows(0)("city"))) Then
                                cbCity.Items.Add(dt.Rows(0)("city"))
                                cbCity.Items(cbCity.Items.Count - 1).Selected = True
                            End If
                        Else
                            txtZip.Focus()

                            Dim txt As String = CurrentPageData.GetCustomString("lblZipErr")
                            txt = StripTags(txt)
                            Dim itm As ListItem = New ListItem(txt, "javascript:" & txtZip.ClientInstanceName & ".SetFocus();")
                            itm.Attributes.Add("class", "BulletedListItem")
                            serverValidationList.Items.Add(itm)
                            isValid = False
                            Return isValid
                        End If

                    Catch ex As Exception
                        WebErrorMessageBox(Me, ex, "")

                        txtZip.Focus()
                        Dim txt As String = CurrentPageData.GetCustomString("lblZipErr")
                        txt = StripTags(txt)
                        Dim itm As ListItem = New ListItem(txt, "javascript:" & txtZip.ClientInstanceName & ".SetFocus();")
                        itm.Attributes.Add("class", "BulletedListItem")
                        serverValidationList.Items.Add(itm)
                        isValid = False
                        Return isValid
                    Finally
                    End Try

                Else
                    txtZip.Focus()
                    Dim txt As String = CurrentPageData.GetCustomString("lblZipErr")
                    txt = StripTags(txt)
                    Dim itm As ListItem = New ListItem(txt, "javascript:" & txtZip.ClientInstanceName & ".SetFocus();")
                    itm.Attributes.Add("class", "BulletedListItem")
                    serverValidationList.Items.Add(itm)
                    isValid = False
                    Return isValid
                End If
            End If


            If (isValid) Then
                If (cbRegion.SelectedItem Is Nothing OrElse cbRegion.SelectedIndex = -1) Then
                    cbRegion.Focus()
                    Dim txt As String = CurrentPageData.GetCustomString("lblRegionErr")
                    txt = StripTags(txt)
                    Dim itm As ListItem = New ListItem(txt, "javascript:" & cbRegion.ClientInstanceName & ".SetFocus();")
                    itm.Attributes.Add("class", "BulletedListItem")
                    serverValidationList.Items.Add(itm)
                    isValid = False
                    Return isValid
                End If
            End If


            If (isValid) Then
                If (cbCity.SelectedItem Is Nothing OrElse cbCity.SelectedIndex = -1) Then
                    cbCity.Focus()
                    Dim txt As String = CurrentPageData.GetCustomString("lblCityErr")
                    txt = StripTags(txt)
                    Dim itm As ListItem = New ListItem(txt, "javascript:" & cbCity.ClientInstanceName & ".SetFocus();")
                    itm.Attributes.Add("class", "BulletedListItem")
                    serverValidationList.Items.Add(itm)
                    isValid = False
                    Return isValid
                End If
            End If
        End If

        'If (isValid) Then
        '    ' personal information
        '    If (isValid AndAlso (cbHeight.SelectedItem Is Nothing OrElse cbHeight.SelectedIndex = -1 OrElse cbHeight.SelectedIndex = 0)) Then
        '        cbHeight.Focus()
        '        'Dim txt As String = CurrentPageData.GetCustomString("cbHeightErr")
        '        Dim txt As String = CurrentPageData.GetCustomString("msg_ProvidePersonalInfo")
        '        txt = StripTags(txt)
        '        Dim itm As ListItem = New ListItem(txt, "javascript:" & cbHeight.ClientInstanceName & ".SetFocus();")
        '        itm.Attributes.Add("class", "BulletedListItem")
        '        serverValidationList.Items.Add(itm)
        '        isValid = False
        '    End If

        '    If (isValid AndAlso (cbHeight.SelectedItem Is Nothing OrElse cbHeight.SelectedIndex = -1 OrElse cbHeight.SelectedIndex = 0)) Then
        '        cbHeight.Focus()
        '        'Dim txt As String = CurrentPageData.GetCustomString("cbHeightErr")
        '        Dim txt As String = CurrentPageData.GetCustomString("msg_ProvidePersonalInfo")
        '        txt = StripTags(txt)
        '        Dim itm As ListItem = New ListItem(txt, "javascript:" & cbHeight.ClientInstanceName & ".SetFocus();")
        '        itm.Attributes.Add("class", "BulletedListItem")
        '        serverValidationList.Items.Add(itm)
        '        isValid = False
        '    End If

        '    If (isValid AndAlso (cbBodyType.SelectedItem Is Nothing OrElse cbBodyType.SelectedIndex = -1 OrElse cbBodyType.SelectedIndex = 0)) Then
        '        cbBodyType.Focus()
        '        ' Dim txt As String = CurrentPageData.GetCustomString("cbBodyTypeErr")
        '        Dim txt As String = CurrentPageData.GetCustomString("msg_ProvidePersonalInfo")
        '        txt = StripTags(txt)
        '        Dim itm As ListItem = New ListItem(txt, "javascript:" & cbBodyType.ClientInstanceName & ".SetFocus();")
        '        itm.Attributes.Add("class", "BulletedListItem")
        '        serverValidationList.Items.Add(itm)
        '        isValid = False
        '    End If

        '    If (isValid AndAlso (cbEyeColor.SelectedItem Is Nothing OrElse cbEyeColor.SelectedIndex = -1 OrElse cbEyeColor.SelectedIndex = 0)) Then
        '        cbEyeColor.Focus()
        '        'Dim txt As String = CurrentPageData.GetCustomString("cbEyeColorErr")
        '        Dim txt As String = CurrentPageData.GetCustomString("msg_ProvidePersonalInfo")
        '        txt = StripTags(txt)
        '        Dim itm As ListItem = New ListItem(txt, "javascript:" & cbEyeColor.ClientInstanceName & ".SetFocus();")
        '        itm.Attributes.Add("class", "BulletedListItem")
        '        serverValidationList.Items.Add(itm)
        '        isValid = False
        '    End If


        '    If (isValid AndAlso (cbHairClr.SelectedItem Is Nothing OrElse cbHairClr.SelectedIndex = -1 OrElse cbHairClr.SelectedIndex = 0)) Then
        '        cbHairClr.Focus()
        '        'Dim txt As String = CurrentPageData.GetCustomString("cbHairClrErr")
        '        Dim txt As String = CurrentPageData.GetCustomString("msg_ProvidePersonalInfo")
        '        txt = StripTags(txt)
        '        Dim itm As ListItem = New ListItem(txt, "javascript:" & cbHairClr.ClientInstanceName & ".SetFocus();")
        '        itm.Attributes.Add("class", "BulletedListItem")
        '        serverValidationList.Items.Add(itm)
        '        isValid = False
        '    End If


        '    If (Not isValid) Then
        '        TogglePersonalCollapsible(True)
        '    End If
        'If (cbRelationshipStatus.SelectedItem Is Nothing OrElse cbRelationshipStatus.SelectedIndex = -1 OrElse cbRelationshipStatus.SelectedIndex = 0) Then
        '    cbRelationshipStatus.Focus()
        '    Dim txt As String = CurrentPageData.GetCustomString("cbRelationshipStatusErr")
        '    txt = StripTags(txt)
        '    Dim itm As ListItem = New ListItem(txt, "javascript:" & cbRelationshipStatus.ClientInstanceName & ".SetFocus();")
        '    itm.Attributes.Add("class", "BulletedListItem")
        '    serverValidationList.Items.Add(itm)
        '    isValid = False
        'End If


        'Dim isAnySelected As Boolean
        'Try
        '    For Each itm As ListViewDataItem In lvTypeOfDate.Items

        '        Try

        '            Dim hdf As HiddenField = DirectCast(itm.FindControl("hdf"), HiddenField)
        '            Dim chk As DevExpress.Web.ASPxEditors.ASPxCheckBox = DirectCast(itm.FindControl("chk"), DevExpress.Web.ASPxEditors.ASPxCheckBox)

        '            Dim tmpTypeOfDating As TypeOfDatingEnum = DirectCast(System.Enum.Parse(GetType(TypeOfDatingEnum), hdf.Value), TypeOfDatingEnum)
        '            Select Case tmpTypeOfDating
        '                Case TypeOfDatingEnum.AdultDating_Casual
        '                    If (chk.Checked) Then
        '                        isAnySelected = True
        '                        Exit For
        '                    End If

        '                Case TypeOfDatingEnum.Friendship
        '                    If (chk.Checked) Then
        '                        isAnySelected = True
        '                        Exit For
        '                    End If
        '                Case TypeOfDatingEnum.LongTermRelationship
        '                    If (chk.Checked) Then
        '                        isAnySelected = True
        '                        Exit For
        '                    End If

        '                Case TypeOfDatingEnum.MarriedDating
        '                    If (chk.Checked) Then
        '                        isAnySelected = True
        '                        Exit For
        '                    End If

        '                Case TypeOfDatingEnum.MutuallyBeneficialArrangements
        '                    If (chk.Checked) Then
        '                        isAnySelected = True
        '                        Exit For
        '                    End If

        '                Case TypeOfDatingEnum.ShortTermRelationship
        '                    If (chk.Checked) Then
        '                        isAnySelected = True
        '                        Exit For
        '                    End If

        '            End Select
        '        Catch ex As Exception
        '            WebErrorMessageBox(Me, ex, "")
        '        End Try
        '    Next

        'If (Not isAnySelected) Then
        '    isValid = False
        '    Dim txt As String = CurrentPageData.GetCustomString("lvTypeOfDateErr")
        '    txt = StripTags(txt)
        '    Dim chk As DevExpress.Web.ASPxEditors.ASPxCheckBox = DirectCast(lvTypeOfDate.Items(0).FindControl("chk"), DevExpress.Web.ASPxEditors.ASPxCheckBox)
        '    Dim itm As ListItem = New ListItem(txt, "javascript:" & chk.ClientInstanceName & ".SetFocus();")
        '    itm.Attributes.Add("class", "BulletedListItem")
        '    serverValidationList.Items.Add(itm)
        'End If
        'Catch ex As Exception
        '    WebErrorMessageBox(Me, ex, "")
        'End Try

        'End If


        ' check user name and email availability
        If (Page.IsValid AndAlso isValid) Then
            Try

                Dim profile As EUS_Profile

                ' check email
                profile = (From itm In Me.CMSDBDataContext.EUS_Profiles
                          Where itm.eMail.ToUpper() = txtEmail.Text.ToUpper() AndAlso _
                              itm.Status <> ProfileStatusEnum.DeletedByUser
                          Select itm).FirstOrDefault()

                'Me.CMSDBDataContext.EUS_Profiles.FirstOrDefault(Function(itm As EUS_Profile) itm.eMail.ToUpper() = txtEmail.Text.ToUpper())
                If (profile IsNot Nothing) Then
                    Dim txt As String = CurrentPageData.GetCustomString("txtEmail_EmailAlreadyInUse_ErrorText")
                    txt = StripTags(txt)
                    Dim itm As ListItem = New ListItem(txt, "javascript:" & txtEmail.ClientInstanceName & ".SetFocus();")
                    itm.Attributes.Add("class", "BulletedListItem")
                    serverValidationList.Items.Add(itm)
                    txtEmail.IsValid = False
                    isValid = False
                    Return isValid
                End If

                profile = Nothing

                ' check user name
                profile = Me.CMSDBDataContext.EUS_Profiles.FirstOrDefault(Function(itm As EUS_Profile) itm.LoginName.ToUpper() = txtLogin.Text.ToUpper())
                If (profile IsNot Nothing) Then
                    Dim txt As String = CurrentPageData.GetCustomString("txtEmail_UserNameAlreadyInUse_ErrorText")
                    txt = StripTags(txt)
                    Dim itm As ListItem = New ListItem(txt, "javascript:" & txtLogin.ClientInstanceName & ".SetFocus();")
                    itm.Attributes.Add("class", "BulletedListItem")
                    serverValidationList.Items.Add(itm)
                    txtLogin.IsValid = False
                    isValid = False
                    Return isValid
                End If

            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try
        End If

        Return isValid
    End Function



    'Private Sub TogglePersonalCollapsible(ByVal show As Boolean)
    '    If (show) Then
    '        lnkExpColl.Text = "-"
    '        h2PersonalTitle.Attributes("title") = globalStrings.GetCustomString("msg_ClickToCollapse", GetLag())
    '        pe_formPersonalInfo.Attributes.CssStyle.Remove("display")
    '    Else
    '        lnkExpColl.Text = "+"
    '        h2PersonalTitle.Attributes("title") = globalStrings.GetCustomString("msg_ClickToExpand", GetLag())
    '        pe_formPersonalInfo.Attributes.CssStyle("display") = "none"
    '    End If
    'End Sub


    Private Sub SetProfileWithFacebook(ByRef profile As EUS_ProfilesRow)

        profile.FacebookUserId = Me.FBUser.uid
        profile.FacebookName = Me.FBUser.name

        If (Me.FBUser.sex = "male") Then
            profile.GenderId = ProfileHelper.gMaleGender.GenderId
            'profile.AccountTypeId = ProfileHelper.Generous_AccountTypeId
        ElseIf (Me.FBUser.sex = "female") Then
            profile.GenderId = ProfileHelper.gFemaleGender.GenderId
            'profile.AccountTypeId = ProfileHelper.Attractive_AccountTypeId
        End If


        If (Not String.IsNullOrEmpty(Me.FBUser.email)) Then
            profile.eMail = Me.FBUser.email.Trim()
        End If

        'profile.LoginName = Me.FBUser.username.Trim()
        'If (String.IsNullOrEmpty(profile.LoginName)) Then
        '    Dim emailPart As String() = Me.FBUser.email.Split("@")
        '    profile.LoginName = emailPart(0)
        'End If

        If (Not String.IsNullOrEmpty(Me.FBUser.birthday_date)) Then
            Me.FBUser.birthday_date = Me.FBUser.birthday_date.Trim()
            If (Me.FBUser.birthday_date <> "") Then
                Try
                    DateTime.TryParse(Me.FBUser.birthday_date, profile.Birthday)
                Catch ex As Exception
                End Try
            End If
        End If

        If (Not String.IsNullOrEmpty(Me.FBUser.hometown_location_City)) Then
            profile.City = Me.FBUser.hometown_location_City.Trim()
        End If

        If (Not String.IsNullOrEmpty(Me.FBUser.hometown_location_State)) Then
            profile._Region = Me.FBUser.hometown_location_State.Trim()
        End If

        If (Not String.IsNullOrEmpty(Me.FBUser.hometown_location_State)) Then
            profile._Region = Me.FBUser.hometown_location_State.Trim()
        End If

        If (Not String.IsNullOrEmpty(Me.FBUser.hometown_location_Zip)) Then
            profile.Zip = Me.FBUser.hometown_location_Zip.Trim()
        End If

        If (Not String.IsNullOrEmpty(Me.FBUser.hometown_location_Country)) Then
            profile.Country = Me.FBUser.hometown_location_Country.Trim()
            profile.Country = ProfileHelper.GetCountryCode(profile.Country)
        End If

        If (Not String.IsNullOrEmpty(Me.FBUser.first_name)) Then
            profile.FirstName = Me.FBUser.first_name.Trim()
        End If

        If (Not String.IsNullOrEmpty(Me.FBUser.last_name)) Then
            profile.LastName = Me.FBUser.last_name.Trim()
        End If


        'If (Me.FBUser.meeting_sex = "male") Then
        '    profile.LookingFor_ToMeetMaleID = True
        'ElseIf (Me.FBUser.meeting_sex = "female") Then
        '    profile.LookingFor_ToMeetFemaleID = True
        'Else
        If (Not profile.IsGenderIdNull()) Then
            profile.LookingFor_ToMeetFemaleID = ProfileHelper.IsMale(profile.GenderId)
            profile.LookingFor_ToMeetMaleID = ProfileHelper.IsFemale(profile.GenderId)
        End If
        'End If

    End Sub


    Private Sub SetProfileWithTwitter(ByRef profile As EUS_ProfilesRow)

        profile.TwitterUserId = Me.TWUser.uid
        profile.TwitterName = Me.TWUser.name
        profile.eMail = Me.TWUser.email
        'profile.FirstName = Me.TWUser.name.Trim()

    End Sub


    Private Sub CreateNewProfile()

        Try

            'rblGender
            'cbAccountType
            'txtEmail
            'txtLogin
            'txtPasswrd
            'txtPasswrd1Conf
            'ctlDate
            'cbAgreements

            Dim adapter As New DSMembersTableAdapters.EUS_ProfilesTableAdapter()
            adapter.Connection = New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("AppDBconnectionString").ConnectionString)
            Dim ds As New DSMembers()
            Dim profile As EUS_ProfilesRow = ds.EUS_Profiles.NewEUS_ProfilesRow()


            ' set user provided data
            Try
                If (Me.FBUser IsNot Nothing) Then
                    SetProfileWithFacebook(profile)
                ElseIf (Me.TWUser IsNot Nothing) Then
                    SetProfileWithTwitter(profile)
                End If
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try

            If (profile.IsGenderIdNull()) Then
                profile.GenderId = rblGender.SelectedItem.Value
            End If
            If (profile.IseMailNull() OrElse String.IsNullOrEmpty(profile.eMail)) Then
                profile.eMail = txtEmail.Text
            End If
            If (profile.IsPasswordNull() OrElse String.IsNullOrEmpty(profile.Password)) Then
                profile.Password = txtPasswrd.Text
            End If

            profile.LookingFor_ToMeetFemaleID = ProfileHelper.IsMale(profile.GenderId)
            profile.LookingFor_ToMeetMaleID = ProfileHelper.IsFemale(profile.GenderId)

            If (profile.IsBirthdayNull()) Then
                profile.Birthday = ctlDate.DateTime
            End If

            If (profile.IsCountryNull() OrElse String.IsNullOrEmpty(profile.Country)) Then
                profile.Country = Session("GEO_COUNTRY_CODE")
            End If

            If (profile.City = "Attiki") Then profile.City = "Attica"


            '' Location
            profile.Country = cbCountry.SelectedItem.Value
            If (cbRegion.SelectedItem IsNot Nothing) Then
                profile._Region = cbRegion.SelectedItem.Value
            Else
                profile._Region = ""
            End If

            If (cbCity.SelectedItem IsNot Nothing) Then
                profile.City = cbCity.SelectedItem.Value
            Else
                profile.City = ""
            End If


            'txtZip.Text = Regex.Replace(txtZip.Text, "[^\d]", "")
            'If (txtZip.Text <> "") Then
            '    Dim zipInt As Integer = txtZip.Text
            '    profile.Zip = zipInt.ToString("### ##")
            'Else
            '    profile.Zip = clsGeoHelper.GetCityCenterPostcode(profile.Country, profile._Region, profile.City)
            'End If

            txtZip.Text = GeneralFunctions.GetZip(txtZip.Text)
            If (txtZip.Text <> "") Then
                profile.Zip = txtZip.Text
            Else
                profile.Zip = clsGeoHelper.GetCityCenterPostcode(profile.Country, profile._Region, profile.City)
            End If


            If (Not profile.IsZipNull()) Then
                ' set textbox zip value
                txtZip.Text = profile.Zip
            End If


            'If (profile.GenderId = ProfileHelper.gFemaleGender.GenderId) Then
            '    profile.AccountTypeId = ProfileHelper.Attractive_AccountTypeId
            'ElseIf (profile.GenderId = ProfileHelper.gMaleGender.GenderId) Then
            '    profile.AccountTypeId = ProfileHelper.Generous_AccountTypeId
            'End If

            profile.LoginName = txtLogin.Text
            profile.OtherDetails_EducationID = Lists.gDSLists.EUS_LISTS_Education.Single(Function(itm As DSLists.EUS_LISTS_EducationRow) itm.US = "Bachelors Degree").EducationId

            profile.OtherDetails_AnnualIncomeID = 26 '&euro;50.001 - &euro;60.000 
            ' Lists.gDSLists.EUS_LISTS_Income.Single(Function(itm As DSLists.EUS_LISTS_IncomeRow) itm.US = "Prefer Not To Say").IncomeId

            'profile.OtherDetails_NetWorthID = Lists.gDSLists.EUS_LISTS_NetWorth.Single(Function(itm As DSLists.EUS_LISTS_NetWorthRow) itm.US = "Prefer Not To Say").NetWorthId

            'If (Not profile.IsGenderIdNull()) Then
            '    If (ProfileHelper.IsFemale(profile.GenderId)) Then
            '        profile.PersonalInfo_HeightID = Lists.gDSLists.EUS_LISTS_Height.Single(Function(itm As DSLists.EUS_LISTS_HeightRow) itm.US = "170cm").HeightId
            '    Else
            '        profile.PersonalInfo_HeightID = Lists.gDSLists.EUS_LISTS_Height.Single(Function(itm As DSLists.EUS_LISTS_HeightRow) itm.US = "180cm").HeightId
            '    End If
            'End If

            'profile.PersonalInfo_BodyTypeID = Lists.gDSLists.EUS_LISTS_BodyType.Single(Function(itm As DSLists.EUS_LISTS_BodyTypeRow) itm.US = "Average").BodyTypeId
            'profile.PersonalInfo_EyeColorID = Lists.gDSLists.EUS_LISTS_EyeColor.Single(Function(itm As DSLists.EUS_LISTS_EyeColorRow) itm.US = "Black").EyeColorId
            'profile.PersonalInfo_HairColorID = Lists.gDSLists.EUS_LISTS_HairColor.Single(Function(itm As DSLists.EUS_LISTS_HairColorRow) itm.US = "Black").HairColorId

            'If (cbHeight.Items(0).Selected) Then
            '    profile.PersonalInfo_HeightID = -1
            'Else
            '    profile.PersonalInfo_HeightID = cbHeight.SelectedItem.Value
            'End If

            'If (cbBodyType.Items(0).Selected) Then
            '    profile.PersonalInfo_BodyTypeID = -1
            'Else
            '    profile.PersonalInfo_BodyTypeID = cbBodyType.SelectedItem.Value
            'End If

            'If (cbEyeColor.Items(0).Selected) Then
            '    profile.PersonalInfo_EyeColorID = -1
            'Else
            '    profile.PersonalInfo_EyeColorID = cbEyeColor.SelectedItem.Value
            'End If

            'If (cbHairClr.Items(0).Selected) Then
            '    profile.PersonalInfo_HairColorID = -1
            'Else
            '    profile.PersonalInfo_HairColorID = cbHairClr.SelectedItem.Value
            'End If

            'If (cbRelationshipStatus.Items(0).Selected) Then
            profile.LookingFor_RelationshipStatusID = -1
            'Else
            '    profile.LookingFor_RelationshipStatusID = cbRelationshipStatus.SelectedItem.Value
            'End If

            profile.LookingFor_TypeOfDating_AdultDating_Casual = True
            profile.LookingFor_TypeOfDating_Friendship = True
            profile.LookingFor_TypeOfDating_LongTermRelationship = True
            profile.LookingFor_TypeOfDating_MarriedDating = True
            profile.LookingFor_TypeOfDating_MutuallyBeneficialArrangements = True
            profile.LookingFor_TypeOfDating_ShortTermRelationship = True

            'Try
            '    For Each itm As ListViewDataItem In lvTypeOfDate.Items

            '        Try

            '            Dim hdf As HiddenField = DirectCast(itm.FindControl("hdf"), HiddenField)
            '            Dim chk As DevExpress.Web.ASPxEditors.ASPxCheckBox = DirectCast(itm.FindControl("chk"), DevExpress.Web.ASPxEditors.ASPxCheckBox)

            '            Dim tmpTypeOfDating As TypeOfDatingEnum = DirectCast(System.Enum.Parse(GetType(TypeOfDatingEnum), hdf.Value), TypeOfDatingEnum)
            '            Select Case tmpTypeOfDating
            '                Case TypeOfDatingEnum.AdultDating_Casual
            '                    profile.LookingFor_TypeOfDating_AdultDating_Casual = chk.Checked

            '                Case TypeOfDatingEnum.Friendship
            '                    profile.LookingFor_TypeOfDating_Friendship = chk.Checked

            '                Case TypeOfDatingEnum.LongTermRelationship
            '                    profile.LookingFor_TypeOfDating_LongTermRelationship = chk.Checked

            '                Case TypeOfDatingEnum.MarriedDating
            '                    profile.LookingFor_TypeOfDating_MarriedDating = chk.Checked

            '                Case TypeOfDatingEnum.MutuallyBeneficialArrangements
            '                    profile.LookingFor_TypeOfDating_MutuallyBeneficialArrangements = chk.Checked

            '                Case TypeOfDatingEnum.ShortTermRelationship
            '                    profile.LookingFor_TypeOfDating_ShortTermRelationship = chk.Checked

            '            End Select
            '        Catch ex As Exception
            '            WebErrorMessageBox(Me, ex, "")
            '        End Try
            '    Next

            'Catch ex As Exception
            '    WebErrorMessageBox(Me, ex, "")
            'End Try


            profile.PersonalInfo_ChildrenID = Lists.gDSLists.EUS_LISTS_ChildrenNumber.Single(Function(itm As DSLists.EUS_LISTS_ChildrenNumberRow) itm.US = "No Children").ChildrenNumberId
            profile.PersonalInfo_EthnicityID = Lists.gDSLists.EUS_LISTS_Ethnicity.Single(Function(itm As DSLists.EUS_LISTS_EthnicityRow) itm.US = "White / Caucasian").EthnicityId

            'Agnostic / Non-religious ---	Χωρίς Θρησκεία
            profile.PersonalInfo_ReligionID = 2
            'Lists.gDSLists.EUS_LISTS_Religion.Single(Function(itm As DSLists.EUS_LISTS_ReligionRow) itm.US = "Agnostic / Non-religious").ReligionId

            profile.PersonalInfo_SmokingHabitID = Lists.gDSLists.EUS_LISTS_Smoking.Single(Function(itm As DSLists.EUS_LISTS_SmokingRow) itm.US = "Non-Smoker").SmokingId
            profile.PersonalInfo_DrinkingHabitID = Lists.gDSLists.EUS_LISTS_Drinking.Single(Function(itm As DSLists.EUS_LISTS_DrinkingRow) itm.US = "Non-Drinker").DrinkingId
            profile.LookingFor_RelationshipStatusID = Lists.gDSLists.EUS_LISTS_RelationshipStatus.Single(Function(itm As DSLists.EUS_LISTS_RelationshipStatusRow) itm.US = "Single").RelationshipStatusId


            ' type of dating
            'profile.LookingFor_TypeOfDating_ShortTermRelationship = True
            ' profile.LookingFor_TypeOfDating_MutuallyBeneficialArrangements = True


            ' set notification settings
            profile.NotificationsSettings_WhenLikeReceived = True
            profile.NotificationsSettings_WhenNewMembersNearMe = False
            profile.NotificationsSettings_WhenNewMessageReceived = True
            profile.NotificationsSettings_WhenNewOfferReceived = True


            ' set implicit data
            profile.Role = "MEMBER"
            profile.Status = ProfileStatusEnum.NewProfile
            profile.IsMaster = False
            profile.MirrorProfileID = 0
            profile.LAGID = Me.Session("LagID")

            profile.DateTimeToRegister = DateTime.UtcNow
            profile.RegisterIP = Request.Params("REMOTE_ADDR")
            profile.RegisterGEOInfos = Session("GEO_COUNTRY_CODE")


            profile.LastLoginDateTime = DateTime.UtcNow
            profile.LastLoginIP = Request.Params("REMOTE_ADDR")
            profile.LastLoginGEOInfos = ""

            profile.LastUpdateProfileDateTime = DateTime.UtcNow
            profile.LastUpdateProfileIP = Request.Params("REMOTE_ADDR")
            profile.LastUpdateProfileGEOInfo = ""


            profile.CustomReferrer = Session("CustomReferrer")


            If (Not String.IsNullOrEmpty(Session("LandingPage"))) Then
                profile.LandingPage = Session("LandingPage")
                If (Not String.IsNullOrEmpty(profile.LandingPage)) Then
                    If (profile.LandingPage.Length > 1023) Then
                        profile.LandingPage = profile.LandingPage.Remove(1023)
                    End If
                End If
            End If


            profile.RegisterUserAgent = Request.Params("HTTP_USER_AGENT")
            If (Not String.IsNullOrEmpty(profile.RegisterUserAgent)) Then
                If (profile.RegisterUserAgent.Length > 1023) Then
                    profile.RegisterUserAgent = profile.RegisterUserAgent.Remove(1023)
                End If
            End If

            profile.Referrer = IIf(Session("HTTP_REFERER") IsNot Nothing, Session("HTTP_REFERER"), "")
            If (Not String.IsNullOrEmpty(profile.Referrer)) Then
                If (profile.Referrer.Length > 1023) Then
                    profile.Referrer = profile.Referrer.Remove(1023)
                End If
            End If

            profile.SearchKeywords = IIf(Session("SearchEngineKeywords") IsNot Nothing, Session("SearchEngineKeywords"), "")
            If (Not String.IsNullOrEmpty(profile.SearchKeywords)) Then
                If (profile.SearchKeywords.Length > 249) Then
                    profile.SearchKeywords = profile.SearchKeywords.Remove(249)
                End If
            End If
            profile.ShowOnFrontPage = False

            Dim countryCodeTmp As String = profile.Country
            Dim PostcodeTmp As String = profile.Zip
            Dim LAGIDTmp As String = profile.LAGID

            Try

                Dim dt As DataTable = clsGeoHelper.GetGEOByZip(countryCodeTmp, PostcodeTmp, LAGIDTmp)

                If (dt.Rows.Count > 0) Then

                    profile.latitude = dt.Rows(0)("latitude")
                    profile.longitude = dt.Rows(0)("longitude")

                Else

                    dt = clsGeoHelper.GetCountryMinPostcodeDataTable(profile.Country, profile._Region, profile.City, profile.LAGID)

                    If (dt.Rows.Count > 0) Then
                        If (Not IsDBNull(dt.Rows(0)("MinPostcode"))) Then PostcodeTmp = dt.Rows(0)("MinPostcode")
                        If (Not IsDBNull(dt.Rows(0)("countryCode"))) Then countryCodeTmp = dt.Rows(0)("countryCode")

                        dt = clsGeoHelper.GetGEOByZip(countryCodeTmp, PostcodeTmp, profile.LAGID)

                        profile.latitude = dt.Rows(0)("latitude")
                        profile.longitude = dt.Rows(0)("longitude")
                    End If

                End If

            Catch ex As Exception
                WebErrorSendEmail(ex, "Registering Profile: Failed to retrieve GEO info for specified zip, params (zip:" & profile.Zip & ")")
            End Try

            ds.EUS_Profiles.AddEUS_ProfilesRow(profile)

            adapter.Update(ds)


            PerformLogin(ds, profile, False)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


    Private Sub CreateAutonotificationRecord(ByVal profile As EUS_ProfilesRow)
        Try

            Dim settings As EUS_AutoNotificationSetting
            settings = New EUS_AutoNotificationSetting
            settings.CustomerID = profile.ProfileID


            If (ProfileHelper.IsMale(Me.SessionVariables.MemberData.GenderId)) Then
                settings.crGenderId = ProfileHelper.gFemaleGender.GenderId
            Else
                settings.crGenderId = ProfileHelper.gMaleGender.GenderId
            End If

            Me.CMSDBDataContext.EUS_AutoNotificationSettings.InsertOnSubmit(settings)


            settings.crHeightIdMin = 29
            settings.crHeightIdMax = 55

            Dim cbHeights As String = ""
            For Each itm As DSLists.EUS_LISTS_HeightRow In Lists.gDSLists.EUS_LISTS_Height.Rows
                If (itm.HeightId >= settings.crHeightIdMin AndAlso itm.HeightId <= settings.crHeightIdMax) Then
                    cbHeights = cbHeights & itm.HeightId & ","
                End If
            Next
            If (cbHeights <> "") Then
                cbHeights = cbHeights.TrimEnd(",")
                settings.crHeightId = cbHeights
            ElseIf (settings.crHeightIdMin > -1) Then
                settings.crHeightId = settings.crHeightIdMin
            ElseIf (settings.crHeightIdMax > -1) Then
                settings.crHeightId = settings.crHeightIdMax
            Else
                settings.crHeightId = Nothing
            End If




            Dim cbBodyTypes As String = ""
            For Each li As EUS_LISTS_BodyTypeRow In Lists.gDSLists.EUS_LISTS_BodyType
                If (li.BodyTypeId > 6) Then
                    cbBodyTypes = cbBodyTypes & li.BodyTypeId & ","
                End If
            Next
            If (cbBodyTypes <> "") Then
                cbBodyTypes = cbBodyTypes.TrimEnd(",")
                settings.crBodyTypeId = cbBodyTypes
            Else
                settings.crBodyTypeId = Nothing
            End If


            Dim cbEthnicitys As String = ""
            For Each li As EUS_LISTS_EthnicityRow In Lists.gDSLists.EUS_LISTS_Ethnicity
                If (li.EthnicityId > 1) Then
                    cbEthnicitys = cbEthnicitys & li.EthnicityId & ","
                End If
            Next
            If (cbEthnicitys <> "") Then
                cbEthnicitys = cbEthnicitys.TrimEnd(",")
                settings.crEthnicityId = cbEthnicitys
            Else
                settings.crEthnicityId = Nothing
            End If


            Dim cbEyeColors As String = ""
            For Each li As EUS_LISTS_EyeColorRow In Lists.gDSLists.EUS_LISTS_EyeColor
                If (li.EyeColorId > 6) Then
                    cbEyeColors = cbEyeColors & li.EyeColorId & ","
                End If
            Next
            If (cbEyeColors <> "") Then
                cbEyeColors = cbEyeColors.TrimEnd(",")
                settings.crEyeColorId = cbEyeColors
            Else
                settings.crEyeColorId = Nothing
            End If



            Dim cbHairColors As String = ""
            For Each li As EUS_LISTS_HairColorRow In Lists.gDSLists.EUS_LISTS_HairColor
                If (li.HairColorId > 10) Then
                    cbHairColors = cbHairColors & li.HairColorId & ","
                End If
            Next
            If (cbHairColors <> "") Then
                cbHairColors = cbHairColors.TrimEnd(",")
                settings.crHairColorId = cbHairColors
            Else
                settings.crHairColorId = Nothing
            End If


            settings.crAgeMin = 18
            settings.crAgeMax = 198


            Dim cbRelationshipStatuses As String = ""
            For Each li As EUS_LISTS_RelationshipStatusRow In Lists.gDSLists.EUS_LISTS_RelationshipStatus
                If (li.RelationshipStatusId > 1) Then
                    cbRelationshipStatuses = cbRelationshipStatuses & li.RelationshipStatusId & ","
                End If
            Next
            If (cbRelationshipStatuses <> "") Then
                cbRelationshipStatuses = cbRelationshipStatuses.TrimEnd(",")
                settings.crRelationshipStatusId = cbRelationshipStatuses
            Else
                settings.crRelationshipStatusId = Nothing
            End If



            Dim cbChildrens As String = ""
            For Each li As EUS_LISTS_ChildrenNumberRow In Lists.gDSLists.EUS_LISTS_ChildrenNumber
                If (li.ChildrenNumberId > 1) Then
                    cbChildrens = cbChildrens & li.ChildrenNumberId & ","
                End If
            Next
            If (cbChildrens <> "") Then
                cbChildrens = cbChildrens.TrimEnd(",")
                settings.crChildrenId = cbChildrens
            Else
                settings.crChildrenId = Nothing
            End If


            settings.crCountry = profile.Country
            settings.crRegion = Nothing
            settings.crCity = Nothing
            settings.crDistance = 250


            Dim cbReligions As String = ""
            For Each li As EUS_LISTS_ReligionRow In Lists.gDSLists.EUS_LISTS_Religion
                If (li.ReligionId > 1) Then
                    cbReligions = cbReligions & li.ReligionId & ","
                End If
            Next
            If (cbReligions <> "") Then
                cbReligions = cbReligions.TrimEnd(",")
                settings.crReligionId = cbReligions
            Else
                settings.crReligionId = Nothing
            End If


            Dim cbSmokings As String = ""
            For Each li As EUS_LISTS_SmokingRow In Lists.gDSLists.EUS_LISTS_Smoking
                If (li.SmokingId > 1) Then
                    cbSmokings = cbSmokings & li.SmokingId & ","
                End If
            Next
            If (cbSmokings <> "") Then
                cbSmokings = cbSmokings.TrimEnd(",")
                settings.crSmokingId = cbSmokings
            Else
                settings.crSmokingId = Nothing
            End If


            Dim cbDrinkings As String = ""
            For Each li As EUS_LISTS_DrinkingRow In Lists.gDSLists.EUS_LISTS_Drinking
                If (li.DrinkingId > 1) Then
                    cbDrinkings = cbDrinkings & li.DrinkingId & ","
                End If
            Next
            If (cbDrinkings <> "") Then
                cbDrinkings = cbDrinkings.TrimEnd(",")
                settings.crDrinkingId = cbDrinkings
            Else
                settings.crDrinkingId = Nothing
            End If


            Me.CMSDBDataContext.SubmitChanges()

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try


    End Sub


    Sub SendNotificationEmailToSupport(ByVal mirrorRow As EUS_ProfilesRow, ByVal isAutoApproveOn As Boolean)
        Try

            Dim toEmail As String = ConfigurationManager.AppSettings("gToEmail")
            Dim Content As String = globalStrings.GetCustomString("EmailSendToSupport_MemberNew", "US")

            If (isAutoApproveOn) Then
                Content = Content.Replace("###YESNO###", "YES")
            Else
                Content = Content.Replace("###YESNO###", "NO")
            End If

            Content = Content.Replace("###LOGINNAME###", mirrorRow.LoginName)
            Content = Content.Replace("###EMAIL###", mirrorRow.eMail)

            Dim approveUrl As String = ConfigurationManager.AppSettings("gApproveProfileURL")
            Dim rejectUrl As String
            approveUrl = String.Format(approveUrl, mirrorRow.LoginName)
            rejectUrl = approveUrl & "&reject=1"

            'If (isAutoApproveOn) Then
            '    Content = Content.Replace("###APPROVEPROFILEURL###", "")
            '    Content = Content.Replace("###REJECTPROFILEURL###", "")
            'Else
            Content = Content.Replace("###APPROVEPROFILEURL###", approveUrl)
            Content = Content.Replace("###REJECTPROFILEURL###", rejectUrl)
            'End If


            Try
                If (Not mirrorRow.IslongitudeNull()) Then
                    Content = Content.Replace("###LONGITUDE###", mirrorRow.longitude)
                Else
                    Content = Content.Replace("###LONGITUDE###", "[Longitude not set]")
                End If
            Catch ex As Exception
            End Try

            Try
                If (Not mirrorRow.IslatitudeNull()) Then
                    Content = Content.Replace("###LATITUDE###", mirrorRow.latitude)
                Else
                    Content = Content.Replace("###LATITUDE###", "[Latitude not set]")
                End If
            Catch ex As Exception
            End Try

            Try
                Content = Content.Replace("###BIRTHDATE###", mirrorRow.Birthday)
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###AGE###", ProfileHelper.GetCurrentAge(mirrorRow.Birthday))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###GENDER###", ProfileHelper.GetGenderString(mirrorRow.GenderId, "US"))
                Content = Content.Replace("###SEX###", ProfileHelper.GetGenderString(mirrorRow.GenderId, "US"))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###COUNTRY###", ProfileHelper.GetCountryName(mirrorRow.Country))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###STATEREGION###", mirrorRow._Region)
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###CITY###", mirrorRow.City)
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###ZIP###", mirrorRow.Zip)
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###DATETOREGISTER###", mirrorRow.DateTimeToRegister)
            Catch ex As Exception
            End Try


            Try
                Content = Content.Replace("###PROFILEAPPROVEDYESNO###", IIf(mirrorRow.Status = 4, "YES", "NO"))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###APPROVEDPHOTOSYESNO###", "NO")
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###AUTOAPPROVEDPHOTOYESNO###", "NO")
            Catch ex As Exception
            End Try


            Try
                Content = Content.Replace("###IP###", Session("IP"))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###GEOIP###", Session("GEO_COUNTRY_CODE"))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###AGENT###", Request.Params("HTTP_USER_AGENT"))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###REFERRER###", IIf(Session("Referrer") IsNot Nothing, Session("Referrer"), ""))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###CUSTOMREFERRER###", Session("CustomReferrer"))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###CUSTOMERID###", mirrorRow.ProfileID)
            Catch ex As Exception
            End Try

            Try
                Dim link As String = ConfigurationManager.AppSettings("gSiteURL") & _
                    "?logon_customer=" & mirrorRow.MirrorProfileID & "_" & mirrorRow.ProfileID
                Content = Content.Replace("###LOGONCUSTOMER###", link)
            Catch ex As Exception
            End Try

            Try
                Dim SearchEngineKeywords As String = IIf(Session("SearchEngineKeywords") Is Nothing, "", Session("SearchEngineKeywords"))
                Content = Content.Replace("###SEARCHENGINEKEYWORDS###", SearchEngineKeywords)
            Catch ex As Exception
            End Try



            Try
                Dim LandingPage As String = ""
                If (Not mirrorRow.IsLandingPageNull()) Then
                    LandingPage = mirrorRow.LandingPage
                    If (LandingPage Is Nothing) Then LandingPage = ""
                End If
                Content = Content.Replace("###LANDINGPAGE###", LandingPage)
            Catch ex As Exception
            End Try

            Try
                Dim DateTimeToRegister As String = ""
                If (Not mirrorRow.IsDateTimeToRegisterNull()) Then
                    DateTimeToRegister = mirrorRow.DateTimeToRegister.ToLocalTime().ToString("dd/MM/yyyy HH:mm:ss")
                End If
                Content = Content.Replace("###REGDATE###", DateTimeToRegister)
            Catch ex As Exception
            End Try

            clsMyMail.SendMail(ConfigurationManager.AppSettings("gToEmail"), "New user REGISTRATION on GOOMENA.", Content, True)

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


    Protected Sub sdsRegion_Selecting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceSelectingEventArgs) Handles sdsRegion.Selecting
        For Each prm As SqlClient.SqlParameter In e.Command.Parameters

            If (prm.ParameterName = "@language") Then
                If (Session("LAGID") = "GR") Then
                    prm.Value = "EL"
                Else
                    prm.Value = "EN"
                End If
            End If

        Next
    End Sub

    Protected Sub sdsCity_Selecting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceSelectingEventArgs) Handles sdsCity.Selecting
        For Each prm As SqlClient.SqlParameter In e.Command.Parameters

            If (prm.ParameterName = "@language") Then
                If (Session("LAGID") = "GR") Then
                    prm.Value = "EL"
                Else
                    prm.Value = "EN"
                End If
            End If

        Next
    End Sub

    'Protected Sub cbpnlZip_Callback(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cbpnlZip.Callback
    '    If (e.Parameter.StartsWith("country_")) Then
    '        FillRegionCombo(e.Parameter.Replace("country_", ""))

    '        If (cbCountry.SelectedItem Is Nothing OrElse cbCountry.SelectedItem.Value <> "GR") Then
    '            txtZip.Text = ""
    '        End If

    '    ElseIf (e.Parameter.StartsWith("region_")) Then
    '        FillCityCombo(e.Parameter.Replace("region_", ""))

    '    ElseIf (e.Parameter.StartsWith("city_")) Then
    '        Dim city As String = e.Parameter.Replace("city_", "")
    '        city = city.Remove(city.IndexOf("_region_"))

    '        Dim region As String = e.Parameter.Substring(e.Parameter.IndexOf("_region_") + Len("_region_"))

    '        FillZipTextBox(city, region)
    '    End If
    '    'SetLocationText()
    'End Sub

    Protected Sub FillRegionCombo(ByVal country As String)
        If String.IsNullOrEmpty(country) Then
            Return
        End If

        Try
            ShowLocationControls()
            cbRegion.DataBind()

            If (cbRegion.Items.Count = 0) Then
                cbRegion.Text = CurrentPageData.GetCustomString("msg_NotFound")
                cbCity.Text = CurrentPageData.GetCustomString("msg_NotFound")
            Else
                cbRegion.Text = CurrentPageData.GetCustomString(msg_RegionText.ID)
                cbCity.Text = CurrentPageData.GetCustomString("msg_NotFound")
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Protected Sub FillCityCombo(ByVal region1 As String)
        If String.IsNullOrEmpty(region1) Then
            Return
        End If

        Try
            ShowLocationControls()
            cbCity.Text = CurrentPageData.GetCustomString(msg_CityText.ID)
            cbCity.DataBind()

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Private Sub FillZipTextBox(ByVal city As String, Optional ByVal region As String = "")
        If String.IsNullOrEmpty(city) Then
            Return
        End If

        Try
            ShowLocationControls()

            ' if there is a value in zip, skip
            'If (txtZip.Text.Trim() <> "") Then
            '    Return
            'End If


            Dim country As String = cbCountry.SelectedItem.Value

            If (String.IsNullOrEmpty(region)) Then
                region = cbRegion.SelectedItem.Value
            End If
            ' city = cbCity.SelectedItem.Value
            If (country = "GR") Then
                txtZip.Text = clsGeoHelper.GetCityCenterPostcode(country, region, city)
            End If
            'ShowRegion(True)
            'ShowCity(True)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Private Sub ShowLocationControls()

        TrRegion.Attributes.CssStyle.Remove("display")
        TrCity.Attributes.CssStyle.Remove("display")
        TrZip.Attributes.CssStyle.Remove("display")
        If (hdfLocationStatus.Value = "region") Then
            TrShowRegion.Attributes.CssStyle.Add("display", "none")
            'Else
            '    TrShowRegion.Attributes.CssStyle.Remove("display")
        End If
    End Sub



    Protected Sub hdfLocationStatus_ValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles hdfLocationStatus.ValueChanged

    End Sub

    Protected Sub cbRegion_DataBound(ByVal sender As Object, ByVal e As EventArgs) Handles cbRegion.DataBound
        cbRegion.Items.Insert(0, New DevExpress.Web.ASPxEditors.ListEditItem(CurrentPageData.GetCustomString("RegionDefaultText"), ""))
    End Sub

    Private Sub cbCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbCountry.SelectedIndexChanged
        'cbRegion.DataSource = "sdsRegion"
        'sdsRegion.SelectCommand = "SELECT DISTINCT region1 FROM dbo.SYS_GEO_" & cbCountry.SelectedItem.Value & " ORDER BY region1"
        'sdsRegion.SelectCommand = "SELECT DISTINCT region1 FROM dbo.SYS_GEO_GR where countrycode=N'" & cbCountry.SelectedItem.Value & "' ORDER BY region1"
        If (Session("LAGID") = "GR") Then
            sdsRegion.SelectCommand = "SELECT DISTINCT region1 FROM dbo.SYS_GEO_GR where countrycode=N'" & cbCountry.SelectedItem.Value & "' and ((countrycode=N'GR' and language=N'EL') or countrycode<>N'GR') ORDER BY region1"
        Else
            sdsRegion.SelectCommand = "SELECT DISTINCT region1 FROM dbo.SYS_GEO_GR where countrycode=N'" & cbCountry.SelectedItem.Value & "' and ((countrycode=N'GR' and language=N'EN') or countrycode<>N'GR')  ORDER BY region1"
        End If
        cbRegion.TextField = "region1"
        cbRegion.DataBind()
    End Sub

    Private Sub cbRegion_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbRegion.SelectedIndexChanged
        Dim a As String = cbRegion.Value
        'cbCity.DataSource = "sdsCity"
        'sdsCity.SelectCommand = "SELECT DISTINCT city FROM dbo.SYS_GEO_" & cbCountry.SelectedItem.Value & " WHERE region1=N'" & a & "' ORDER BY city"
        'sdsCity.SelectCommand = "SELECT DISTINCT city FROM dbo.SYS_GEO_GR WHERE region1=N'" & a & "' and countrycode=N'" & cbCountry.SelectedItem.Value & "' ORDER BY city"
        If (Session("LAGID") = "GR") Then
            sdsCity.SelectCommand = "SELECT DISTINCT city FROM dbo.SYS_GEO_GR WHERE region1=N'" & a & "' and countrycode=N'" & cbCountry.SelectedItem.Value & "' and ((countrycode=N'GR' and language=N'EL') or countrycode<>N'GR') ORDER BY city"
        Else
            sdsCity.SelectCommand = "SELECT DISTINCT city FROM dbo.SYS_GEO_GR WHERE region1=N'" & a & "' and countrycode=N'" & cbCountry.SelectedItem.Value & "' and ((countrycode=N'GR' and language=N'EN') or countrycode<>N'GR') ORDER BY city"
        End If
        cbCity.TextField = "city"
        cbCity.DataBind()
    End Sub

    Private Sub cbCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbCity.SelectedIndexChanged
        Dim a As String = cbRegion.Value
        Dim b As String = cbCity.Value
        ' txtZip.DataSource = zip
        'zip.SelectCommand = "SELECT DISTINCT postcode FROM dbo.SYS_GEO_GR WHERE region1=N'" & a & "' AND city=N'" & b & "' and countrycode=N'" & cbCountry.SelectedItem.Value & "' ORDER BY postcode"
        'zip.SelectCommand = "SELECT DISTINCT postcode FROM dbo.SYS_GEO_" & cbCountry.SelectedItem.Value & " WHERE region1=N'" & a & "' AND city=N'" & b & "' ORDER BY postcode"
        If (Session("LAGID") = "GR") Then
            zip.SelectCommand = "SELECT DISTINCT postcode FROM dbo.SYS_GEO_GR WHERE region1=N'" & a & "' AND city=N'" & b & "' and countrycode=N'" & cbCountry.SelectedItem.Value & "' and ((countrycode=N'GR' and language=N'EL') or countrycode<>N'GR') ORDER BY postcode"
        Else
            zip.SelectCommand = "SELECT DISTINCT postcode FROM dbo.SYS_GEO_GR WHERE region1=N'" & a & "' AND city=N'" & b & "' and countrycode=N'" & cbCountry.SelectedItem.Value & "' and ((countrycode=N'GR' and language=N'EN') or countrycode<>N'GR') ORDER BY postcode"
        End If

        txtZip.ValueField = "postcode"
        txtZip.TextField = "postcode"
        txtZip.DataBind()
        txtZip.SelectedIndex = 0
    End Sub

End Class



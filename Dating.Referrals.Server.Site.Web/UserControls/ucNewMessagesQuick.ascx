﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucNewMessagesQuick.ascx.vb" Inherits="Dating.Referrals.Server.Site.Web.ucNewMessagesQuick" %>
<%@ Register assembly="DevExpress.Web.v13.1, Version=13.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxDataView" tagprefix="dx" %>

<dx:ASPxDataView ID="dvMessages" runat="server" ColumnCount="1" 
    DataSourceID="sdsMessages" EnableDefaultAppearance="False" EnableTheming="False" 
    ItemSpacing="0px" RowPerPage="10" Width="100%" 
    EmptyDataText="Can't find any new message.">
    <ItemTemplate>
<asp:Panel ID="pnlUnl" runat="server" CssClass="quick_list m_item newest" Visible='<%# Eval("CommunicationUnl") > 0 %>'>
    <a class="linkNotifWrapper" href="javascript:void(0);" location="<%# System.Web.VirtualPathUtility.ToAbsolute("~/Members/Conversation.aspx?vw=" &  Eval("LoginName"))  %>">
        <table cellpadding="0" cellspacing="0" style="width:100%;">
            <tr>
                <td><div class="pic">
                        <dx:ASPxHyperLink ID="lnkFromImg" runat="server" 
                            EnableDefaultAppearance="False" EnableTheming="False" EncodeHtml="false" 
                            ImageUrl='<%# Dating.Referrals.Server.Site.Web.AppUtils.GetImage(Eval("CustomerID").Tostring(), Eval("FileName").Tostring(), Eval("GenderId").Tostring(), False,Me.IsHTTPS) %>' 
                            NavigateUrl='<%# System.Web.VirtualPathUtility.ToAbsolute("~/Members/Conversation.aspx?vw=" &  Eval("LoginName")) %>'>
                        </dx:ASPxHyperLink>
                    </div></td>
                <td style="width:90%;"><div style="position:relative;">
                        <dx:ASPxHyperLink ID="lnkFromLogin" runat="server" 
                            EnableDefaultAppearance="False" EnableTheming="False" EncodeHtml="false" 
                            NavigateUrl='<%# System.Web.VirtualPathUtility.ToAbsolute("~/Members/Conversation.aspx?vw=" &  Eval("LoginName")) %>' 
                            Text='<%# Eval("LoginName") %>' style="margin-top:0px;padding-top:0px;" CssClass="loginName">
                        </dx:ASPxHyperLink>
                        <dx:ASPxLabel ID="lblSubject" runat="server" Text="" EncodeHtml="False"/><br />
                        <dx:ASPxLabel ID="lblSentDate" runat="server" Text='<%# Dating.Referrals.Server.Site.Web.AppUtils.GetDateTimeString(Eval("MAXDateTimeToCreate"), "sent ###DATETIME###") %>' EncodeHtml="False"/>
                        <div style="position:absolute;top:0px;right:0px;">
                            <dx:ASPxLabel ID="lblCount" runat="server" 
                                Text='<%# Mybase.GetCountMessagesString(Eval("ItemsCount")) %>' 
                                EncodeHtml="False" 
                                Visible='<%# Mybase.IsVisible(Eval("ItemsCount")) %>'/>
                        </div>
                    </div></td>
            </tr>
        </table>
    </a>
</asp:Panel>  
<asp:Panel ID="pnlLtd" runat="server" CssClass="quick_list m_item newest" Visible='<%# Eval("CommunicationUnl") = 0 %>'>
    <a class="linkNotifWrapper" href="javascript:void(0);" location="<%# System.Web.VirtualPathUtility.ToAbsolute("~/Members/Messages.aspx?vw=" &  Eval("LoginName"))  %>">
        <table cellpadding="0" cellspacing="0" style="width:100%;">
            <tr>
                <td><div class="pic">
                        <dx:ASPxHyperLink ID="lnkFromImgLtd" runat="server" 
                            EnableDefaultAppearance="False" EnableTheming="False" EncodeHtml="false" 
                            ImageUrl='<%# Dating.Referrals.Server.Site.Web.AppUtils.GetImage(Eval("CustomerID").Tostring(), Eval("FileName").Tostring(), Eval("GenderId").Tostring(), False,Me.IsHTTPS) %>' 
                            NavigateUrl='<%# System.Web.VirtualPathUtility.ToAbsolute("~/Members/Messages.aspx?vw=" &  Eval("LoginName")) %>'>
                        </dx:ASPxHyperLink>
                    </div></td>
                <td style="width:90%;"><div style="position:relative;">
                        <dx:ASPxHyperLink ID="lnkFromLoginLtd" runat="server" 
                            EnableDefaultAppearance="False" EnableTheming="False" EncodeHtml="false" 
                            NavigateUrl='<%# System.Web.VirtualPathUtility.ToAbsolute("~/Members/Messages.aspx?vw=" &  Eval("LoginName")) %>' 
                            Text='<%# Eval("LoginName") %>' style="margin-top:0px;padding-top:0px;" CssClass="loginName">
                        </dx:ASPxHyperLink>
                        <dx:ASPxLabel ID="lblSubjectLtd" runat="server" Text="" EncodeHtml="False"/><br />
                        <dx:ASPxLabel ID="lblSentDateLtd" runat="server" Text='<%# Dating.Referrals.Server.Site.Web.AppUtils.GetDateTimeString(Eval("MAXDateTimeToCreate"), "sent ###DATETIME###") %>' EncodeHtml="False"/>
                        <div style="position:absolute;top:0px;right:0px;">
                            <dx:ASPxLabel ID="lblCountLtd" runat="server" 
                                Text='<%# Mybase.GetCountMessagesString(Eval("ItemsCount")) %>' 
                                EncodeHtml="False" 
                                Visible='<%# Mybase.IsVisible(Eval("ItemsCount")) %>'/>
                        </div>
                    </div></td>
            </tr>
        </table>
    </a>
</asp:Panel>  
        <div class="clear">
        </div>
    </ItemTemplate>
    <Paddings Padding="0px" />
    <ItemStyle>
    <Paddings Padding="0px" />
    </ItemStyle>
</dx:ASPxDataView>

<asp:SqlDataSource ID="sdsMessages" runat="server" 
    ConnectionString="<%$ ConnectionStrings:AppDBconnectionString %>" 
    SelectCommand="GetNewMessagesQuick2" SelectCommandType="StoredProcedure">
    <SelectParameters>
        <asp:Parameter DefaultValue="0" Name="CurrentProfileId" Type="Int32" />
        <asp:Parameter DefaultValue="4" Name="ReturnRecordsWithStatus" Type="Int32" />
        <asp:Parameter DefaultValue="5" Name="NumberOfRecordsToReturn" Type="Int32" />
    </SelectParameters>
</asp:SqlDataSource>

<%--
<script type="text/javascript">
    jQuery('.m_item.newest').positionOn($('m_item.newest.linkNotifWrapper'))
</script>

--%>
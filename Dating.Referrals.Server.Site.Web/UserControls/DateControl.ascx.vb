﻿Imports System.ComponentModel
Imports DevExpress.Web.ASPxEditors

Public Class DateControl
    Inherits BaseUserControl

    Const __MIN_AGE As Integer = 12
    Const __MAX_AGE As Integer = 100


    <Bindable(True, BindingDirection.TwoWay)>
    Public Property DateTime As Nullable(Of DateTime)
        Get
            If (Me.Year > 0 AndAlso Me.Month > 0 AndAlso Me.Day > 0) Then
                Dim dt As New DateTime(Me.Year, Me.Month, Me.Day)
                Return dt
            End If

            Return Nothing
        End Get
        Set(value As Nullable(Of DateTime))

            If (value.HasValue) Then
                Me.Year = value.Value.Year
                Me.Month = value.Value.Month
                Me.Day = value.Value.Day
            End If

        End Set
    End Property


    Public Property Year As Integer
        Get
            Dim _int As Integer = 1980
            If (ddlyear.SelectedIndex > -1) Then
                Integer.TryParse(ddlyear.Items(ddlyear.SelectedIndex).Value, _int)
            End If

            Return _int
        End Get
        Set(value As Integer)
            Dim _itm As ListEditItem = ddlyear.Items.FindByValue(value.ToString())
            If (Not _itm Is Nothing) Then
                _itm.Selected = True
            End If
        End Set
    End Property


    Public Property Month As Integer
        Get
            Dim _int As Integer = 1
            If (ddlmonth.SelectedIndex > -1) Then
                Integer.TryParse(ddlmonth.Items(ddlmonth.SelectedIndex).Value, _int)
            End If

            Return _int
        End Get
        Set(value As Integer)
            Dim _itm As ListEditItem = ddlmonth.Items.FindByValue(value.ToString())
            If (Not _itm Is Nothing) Then
                _itm.Selected = True
            End If
        End Set
    End Property


    Public Property Day As Integer
        Get
            Dim _dayInt As Integer = 1
            If (ddlday.SelectedIndex > -1) Then
                Integer.TryParse(ddlday.Items(ddlday.SelectedIndex).Value, _dayInt)
            End If

            Return _dayInt
        End Get
        Set(value As Integer)
            Dim _day As ListEditItem = ddlday.Items.FindByValue(value.ToString())
            If (Not _day Is Nothing) Then
                _day.Selected = True
            End If
        End Set
    End Property




    Public Property Height As Unit
        Get
            Return ddlyear.Height
        End Get
        Set(value As Unit)
            ddlyear.Height = value
            ddlmonth.Height = value
            ddlday.Height = value
        End Set
    End Property


    Public Property FontSize As FontUnit
        Get
            Return ddlyear.Font.Size
        End Get
        Set(value As FontUnit)
            ddlyear.Font.Size = value
            ddlmonth.Font.Size = value
            ddlday.Font.Size = value
        End Set
    End Property


    Public Property MinAge As Integer
        Get
            If (ViewState("MinAge") Is Nothing) Then Return 0
            Return ViewState("MinAge")
        End Get
        Set(value As Integer)
            ViewState("MinAge") = value
        End Set
    End Property


    Public Property YearsFromNow As Integer
        Get
            If (ViewState("YearsFromNow") Is Nothing) Then Return 0
            Return ViewState("YearsFromNow")
        End Get
        Set(value As Integer)
            ViewState("YearsFromNow") = value
        End Set
    End Property


    Public Property YearsTillNow As Integer
        Get
            If (ViewState("YearsTillNow") Is Nothing) Then Return 0
            Return ViewState("YearsTillNow")
        End Get
        Set(value As Integer)
            ViewState("YearsTillNow") = value
        End Set
    End Property


    Private Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        Try
            If (Not Me.IsPostBack) Then
                If (ddlyear.Items.Count = 1) Then

                    Dim startDate As Integer = Date.Now.Year
                    Dim endDate As Integer = startDate
                    Dim y As Integer
                    If (MinAge > 0) Then
                        startDate = Date.Now.Year - MinAge
                        endDate = startDate - 100
                    Else
                        If (YearsFromNow > 0) Then startDate = Date.Now.Year + YearsFromNow
                        If (YearsTillNow > 0) Then endDate = Date.Now.Year - YearsTillNow
                    End If

                    For y = startDate To endDate Step -1
                        ddlyear.Items.Add(y, y)
                    Next
                End If
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If (Not Me.IsPostBack) Then
                LoadLAG()
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try
    End Sub


    Public Overrides Sub Master_LanguageChanged()
        LoadLAG()
    End Sub


    Protected Sub LoadLAG()
        Try

            Dim _Day As ListEditItem = ddlday.Items.FindByValue("")
            If (_Day IsNot Nothing) Then
                _Day.Text = globalStrings.GetCustomString("Day", Session("LagID"))
            End If

            Dim _Month As ListEditItem = ddlmonth.Items.FindByValue("")
            If (_Month IsNot Nothing) Then
                _Month.Text = globalStrings.GetCustomString("Month", Session("LagID"))
            End If

            Dim _Year As ListEditItem = ddlyear.Items.FindByValue("")
            If (_Year IsNot Nothing) Then
                _Year.Text = globalStrings.GetCustomString("Year", Session("LagID"))
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Public Overrides Sub Focus()
        ddlday.Focus()
    End Sub

End Class
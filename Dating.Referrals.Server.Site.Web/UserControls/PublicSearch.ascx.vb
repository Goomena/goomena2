﻿Imports DevExpress.Web.ASPxPager
Imports Dating.Server.Core.DLL

Public Class PublicSearch
    Inherits BaseUserControl




    Public Property InfoWinUrl As String
    Public Property InfoWinHeaderText As String

    Dim _pageData As clsPageData
    Public ReadOnly Property CurrentPageData As clsPageData
        Get
            If (_pageData Is Nothing) Then _pageData = New clsPageData("control.OfferControl", Context)
            Return _pageData
        End Get
    End Property


    Public Property ShowNoPhotoText As Boolean
    Public Property ShowEmptyListText As Boolean


    Private _list As List(Of clsWinkUserListItem)
    Public Property UsersList As List(Of clsWinkUserListItem)
        Get
            If (Me._list Is Nothing) Then Me._list = New List(Of clsWinkUserListItem)
            Return Me._list
        End Get
        Set(value As List(Of clsWinkUserListItem))
            Me._list = value
        End Set
    End Property



    <PersistenceMode(PersistenceMode.InnerDefaultProperty)>
    Public ReadOnly Property Repeater As Repeater
        Get
            Return rptPubSrch
        End Get
    End Property


    Protected Overrides Sub OnInit(e As System.EventArgs)
        MyBase.OnInit(e)
    End Sub



    Public Overrides Sub DataBind()

        If (Me.Repeater IsNot Nothing) Then

            Try
                For Each itm As clsWinkUserListItem In Me.UsersList
                    itm.FillTextResourceProperties(Me.CurrentPageData)
                Next
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try

            Me.Repeater.DataSource = Me.UsersList
            Me.Repeater.DataBind()
        End If

    End Sub


    Protected Function WriteOffer_MemberInfo(DataItem As Dating.Referrals.Server.Site.Web.clsWinkUserListItem) As Object

        Dim sb As New StringBuilder()
        Try
            sb.Append("<ul>")
            sb.Append("<li>")

            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberAge) Then
                sb.Append("<div class=""pr-age"">")
                sb.Append(DataItem.OtherMemberAge)
                sb.Append(" " & DataItem.YearsOldText & " ")
                sb.Append("</div>")
            End If

            sb.Append("</li>")


            sb.Append("<li>")
            sb.Append("<b>")

            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberCity) Then
                sb.Append(DataItem.OtherMemberCity)
            End If

            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberCity) AndAlso Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberRegion) Then
                sb.Append(",  ")
            End If

            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberRegion) Then
                sb.Append(DataItem.OtherMemberRegion)
            End If

            sb.Append("</b>")
            sb.Append("</li>")


            sb.Append("</ul>")

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Return sb.ToString()
    End Function


    Protected Function WriteSearch_MemberInfo(DataItem As Dating.Referrals.Server.Site.Web.clsWinkUserListItem) As Object

        'Dim str As String = "<ul>" & vbCrLf & _
        '    "<li><b>" & DataItem.OtherMemberAge & "</b> " & DataItem.YearsOldText & " <b>" & Eval("OtherMemberCity & ", " & Eval("OtherMemberRegion & "</b></li>" & vbCrLf & _
        '    "<li><b>" & DataItem.OtherMemberHeight & " - " & DataItem.OtherMemberPersonType & "</b></li>" & vbCrLf & _
        '    "<li>" & DataItem.OtherMemberHair & ", " & DataItem.OtherMemberEyes & ", " & DataItem.OtherMemberEthnicity & "</li>" & vbCrLf & _
        '    "</ul>" & vbCrLf

        Dim sb As New StringBuilder()
        Try

            sb.Append("<ul>")

            sb.Append("<li>")

            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberAge) Then
                sb.Append("<div class=""pr-age"">")
                sb.Append(DataItem.OtherMemberAge)
                sb.Append(" " & DataItem.YearsOldText & " ")
                sb.Append("</div>")
            End If

            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberCity) Then
                sb.Append(DataItem.OtherMemberCity)
            End If

            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberCity) AndAlso Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberRegion) Then
                sb.Append(",  ")
            End If

            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberRegion) Then
                sb.Append(DataItem.OtherMemberRegion)
            End If

            sb.Append("</li>")


            sb.Append("<li>")

            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberHeight) Then
                sb.Append(DataItem.OtherMemberHeight)
            End If

            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberHeight) AndAlso Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberPersonType) Then
                sb.Append(" - ")
            End If

            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberPersonType) Then
                sb.Append(DataItem.OtherMemberPersonType)
            End If

            sb.Append("</li>")


            sb.Append("<li>")

            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberHair) Then
                sb.Append(DataItem.OtherMemberHair)
            End If

            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberEyes) Then
                If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberHair) Then sb.Append(", ")
                sb.Append(DataItem.OtherMemberEyes)
            End If

            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberEthnicity) Then
                If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberEyes) OrElse Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberHair) Then sb.Append(", ")
                sb.Append(DataItem.OtherMemberEthnicity)
            End If

            sb.Append("</li>")
            sb.Append("</ul>")

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Return sb.ToString()
    End Function
    
    'Protected Sub rptSearch_ItemDataBound(source As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptPubSrch.ItemDataBound
    '    Try

    '        Dim lnkWhatIs As LinkButton = e.Item.FindControl("lnkWhatIs")
    '        If (lnkWhatIs IsNot Nothing) Then
    '            lnkWhatIs.ToolTip = Me.CurrentPageData.GetCustomString("WhatIsText")

    '            If (String.IsNullOrEmpty(InfoWinUrl)) Then InfoWinUrl = ResolveUrl("~/InfoWin.aspx?info=searchcommands")
    '            If (String.IsNullOrEmpty(InfoWinHeaderText)) Then InfoWinHeaderText = Me.CurrentPageData.GetCustomString("Search_WhatIsPopup_HeaderText")

    '            lnkWhatIs.OnClientClick = WhatIsIt.OnMoreInfoClickFunc(lnkWhatIs.OnClientClick,
    '                                         InfoWinUrl,
    '                                         InfoWinHeaderText)
    '        End If

    '    Catch ex As Exception

    '    End Try
    'End Sub


End Class
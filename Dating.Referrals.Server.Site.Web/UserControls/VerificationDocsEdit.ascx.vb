﻿Imports DevExpress.Web.ASPxUploadControl
Imports System.IO
Imports System.Drawing
Imports Dating.Server.Core.DLL
Imports DevExpress.Web.ASPxEditors
Imports DevExpress.Web.ASPxPopupControl
Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Datasets.DLL.DSMembers

Public Class VerificationDocsEdit
    Inherits BaseUserControl


    Dim _pageData As clsPageData
    Protected ReadOnly Property CurrentPageData As clsPageData
        Get
            If (_pageData Is Nothing) Then _pageData = New clsPageData("~/Members/control.VerificationDocsEdit", Context)
            Return _pageData
        End Get
    End Property

    Private Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If (Me.Visible) Then

            Try
                If (Not Page.IsPostBack) Then
                    Session("VerDoc_Uploaded") = Nothing
                End If


                LoadLAG()

                If (Not Page.IsPostBack) Then
                    TogglePersonalCollapsible(False)
                End If

                gvDocs.DataBind()
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "Page_Load")
            End Try
        End If

    End Sub


    Private Sub TogglePersonalCollapsible(show As Boolean)
        'If (show) Then
        '    lnkInfoExpColl.Text = "-"
        '    h2InfoTitle.Attributes("title") = globalStrings.GetCustomString("msg_ClickToCollapse", GetLag())
        '    divmp_info.Attributes.CssStyle.Remove("display")
        'Else
        '    lnkInfoExpColl.Text = "+"
        '    h2InfoTitle.Attributes("title") = globalStrings.GetCustomString("msg_ClickToExpand", GetLag())
        '    divmp_info.Attributes.CssStyle("display") = "none"
        'End If
    End Sub

    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        If (Me.Visible) Then
            Try

                'LoadPhotos()

            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "Page_PreRender")
            End Try


            Try
                Dim cPageBasic As Dating.Server.Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
                AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "Page_PreRender")
            End Try
        End If

    End Sub


    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
    End Sub


    Protected Sub LoadLAG()
        Try

            uplImage.BrowseButton.Text = CurrentPageData.GetCustomString("BrowseButton.Text")
            lblMaxFileSize.Text = CurrentPageData.GetCustomString(lblMaxFileSize.ID)
            btnUpload.Text = CurrentPageData.GetCustomString(btnUpload.ID)

            uplImage.ValidationSettings.GeneralErrorText = CurrentPageData.GetCustomString("Uploader.ValidationSettings.GeneralErrorText")
            uplImage.ValidationSettings.MaxFileSizeErrorText = CurrentPageData.GetCustomString("Uploader.ValidationSettings.MaxFileSizeErrorText")
            uplImage.ValidationSettings.NotAllowedFileExtensionErrorText = CurrentPageData.GetCustomString("Uploader.ValidationSettings.NotAllowedFileExtensionErrorText")

            SetControlsValue(Me, CurrentPageData)

            msg_PubPhotos.Text = CurrentPageData.GetCustomString("msg_PubPhotos")
            lblPhotoLevel.Text = CurrentPageData.GetCustomString("lblPhotoLevelTitle")

            If (Session("VerDoc_Uploaded") = "success") Then
                divUploadStatus.Visible = True
                imgResult.ImageUrl = "~/Images/IconImages/button_ok.png"
                lblUploadResult.Text = CurrentPageData.GetCustomString("Success.Document.upload")
                txtDescription.Text = ""
                cbDocType.SelectedIndex = 0

            ElseIf (Session("VerDoc_Uploaded") = "fail") Then
                divUploadStatus.Visible = True
                imgResult.ImageUrl = "~/Images/IconImages/cnrdelete-all.png"
                lblUploadResult.Text = CurrentPageData.GetCustomString("Error.Document.upload")
                txtDescription.Text = ""
                cbDocType.SelectedIndex = 0

            Else
                divUploadStatus.Visible = False


            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Protected Sub uplImage_FileUploadComplete(ByVal sender As Object, ByVal e As FileUploadCompleteEventArgs)
        e.CallbackData = SavePostedFile(e.UploadedFile)
    End Sub


    Private Function SavePostedFile(ByVal uploadedFile As UploadedFile) As String
        Session("VerDoc_Uploaded") = Nothing
        If (Not uploadedFile.IsValid) Then
            Return String.Empty
        End If


        Dim imgfilePath As String = ""
        Dim thumbfilePath As String = ""

        Dim _guid As Guid = Guid.NewGuid()

        Dim ext As String = uploadedFile.FileName
        ext = ext.Substring(ext.LastIndexOf(".") + 1)

        Dim filePath As String = _guid.ToString("N") & "." & ext


        Try

            Dim imgDir As String = String.Format(ProfileHelper.gMemberDocsDirectory, Me.MasterProfileId.ToString())
            imgDir = MapPath(imgDir)
            If Not System.IO.Directory.Exists(imgDir) Then
                System.IO.Directory.CreateDirectory(imgDir)
            End If
            imgfilePath = Path.Combine(imgDir, filePath)

            Dim buffer As Byte() = New Byte(uploadedFile.ContentLength) {}
            'uploadedFile.FileContent.Read(buffer, 0, uploadedFile.ContentLength)

            Dim original As FileInfo = New FileInfo(imgfilePath)
            Dim fileStream As FileStream = original.OpenWrite()
            Try
                uploadedFile.FileContent.CopyTo(fileStream)
                fileStream.Write(buffer, 0, buffer.Length)
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "Save verification doc failed (1).")
            Finally
                fileStream.Close()
                fileStream.Dispose()
            End Try


            Dim ver As EUS_CustomerVerificationDoc = New EUS_CustomerVerificationDoc()
            ver.CustomerID = Me.MasterProfileId
            ver.DateTimeToUploading = DateTime.UtcNow
            ver.Description = txtDescription.Text

            If (ver.Description.Length > 1024) Then
                ver.Description = ver.Description.Remove(1024)
            End If
            ver.DocumentType = cbDocType.SelectedItem.Text
            ver.FileName = filePath
            ver.OriginalName = uploadedFile.FileName

            Me.CMSDBDataContext.EUS_CustomerVerificationDocs.InsertOnSubmit(ver)
            Me.CMSDBDataContext.SubmitChanges()


            DataHelpers.UpdateEUS_Profiles_MirrorSetStatusUpdating(Me.MasterProfileId, ProfileStatusEnum.Updating, ProfileModifiedEnum.UpdatingDocs)


            Dim profileRows As New clsProfileRows(Me.MasterProfileId)
            Try
                SendNotificationEmailToSupport(profileRows.GetMirrorRow(), ver)
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try

            txtDescription.Text = ""
            Session("VerDoc_Uploaded") = "success"
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Save verification doc failed (2).")
            Session("VerDoc_Uploaded") = "fail"
        End Try

        Return imgfilePath
    End Function


    Sub SendNotificationEmailToSupport(mirrorRow As DSMembers.EUS_ProfilesRow, ver As EUS_CustomerVerificationDoc)
        Try

            Dim toEmail As String = ConfigurationManager.AppSettings("gToEmail")
            Dim Content As String = globalStrings.GetCustomString("EmailSendToSupport_MemberNewVerificationDoc", "US")


            Content = Content.Replace("###LOGINNAME###", mirrorRow.LoginName)
            Content = Content.Replace("###EMAIL###", mirrorRow.eMail)


            Try
                Content = Content.Replace("###FILENAME###", ver.OriginalName)
            Catch ex As Exception
            End Try

            Try
                Dim siteUrl As String = ConfigurationManager.AppSettings("gSiteURL")
                Dim photoUrl As String = ""
                photoUrl = String.Format(ProfileHelper.gMemberDocsDirectoryView, ver.CustomerID) & ver.FileName
                photoUrl = System.Web.VirtualPathUtility.ToAbsolute(photoUrl)
                photoUrl = (siteUrl & photoUrl)
                'photoUrl = ProfileHelper.GetProfileImageURL(newPhoto.CustomerID, newPhoto.FileName, mirrorRow.GenderId, True)

                Content = Content.Replace("###FILEPATH###", photoUrl)
                Content = Content.Replace("blank###PHOTOURL###", photoUrl) ' cms editor issue, adds word blank
                Content = Content.Replace("###PHOTOURL###", photoUrl)
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "Verification doc")
            End Try



            Try
                Content = Content.Replace("###BIRTHDATE###", mirrorRow.Birthday)
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###AGE###", ProfileHelper.GetCurrentAge(mirrorRow.Birthday))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###GENDER###", ProfileHelper.GetGenderString(mirrorRow.GenderId, "US"))
                Content = Content.Replace("###SEX###", ProfileHelper.GetGenderString(mirrorRow.GenderId, "US"))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###COUNTRY###", ProfileHelper.GetCountryName(mirrorRow.Country))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###STATEREGION###", mirrorRow._Region)
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###CITY###", mirrorRow.City)
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###ZIP###", mirrorRow.Zip)
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###DATETOREGISTER###", mirrorRow.DateTimeToRegister)
            Catch ex As Exception
            End Try


            Try
                Content = Content.Replace("###PROFILEAPPROVEDYESNO###", IIf(mirrorRow.Status = 4, "YES", "NO"))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###IP###", Dating.Server.Core.DLL.clsHTMLHelper.CreateIPLookupLinks(Session("IP")))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###GEOIP###", Session("GEO_COUNTRY_CODE"))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###AGENT###", Request.Params("HTTP_USER_AGENT"))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###REFERRER###", Dating.Server.Core.DLL.clsHTMLHelper.CreateURLLink(IIf(Session("Referrer") IsNot Nothing, Session("Referrer"), "")))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###CUSTOMREFERRER###", Session("CustomReferrer"))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###CUSTOMERID###", mirrorRow.ProfileID)
            Catch ex As Exception
            End Try
            Try
                Dim link As String = ConfigurationManager.AppSettings("gSiteURL") & _
                    "?logon_customer=" & mirrorRow.MirrorProfileID & "_" & mirrorRow.ProfileID
                Content = Content.Replace("###LOGONCUSTOMER###", link)
            Catch ex As Exception
            End Try


            Try
                Dim SearchEngineKeywords As String = IIf(Session("SearchEngineKeywords") Is Nothing, "", Session("SearchEngineKeywords"))
                Content = Content.Replace("###SEARCHENGINEKEYWORDS###", SearchEngineKeywords)
            Catch ex As Exception
            End Try


            Try
                Dim LandingPage As String = ""
                If (Not mirrorRow.IsLandingPageNull()) Then
                    LandingPage = mirrorRow.LandingPage
                    If (LandingPage Is Nothing) Then LandingPage = ""
                End If
                Content = Content.Replace("###LANDINGPAGE###", Dating.Server.Core.DLL.clsHTMLHelper.CreateURLLink(LandingPage))
            Catch ex As Exception
            End Try


            clsMyMail.SendMail(ConfigurationManager.AppSettings("gToEmail"), "New verification doc uploaded on Goomena.com", Content, True)

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


    'Private Sub LoadPhotos()


    '    Try

    '        ' true when user makes logoff on photos page
    '        If (Me.GetCurrentProfile(True) Is Nothing) Then Return




    '        Dim DeleteButtonText As String = CurrentPageData.GetCustomString("btnDelete")
    '        Dim DeleteButtonClientClickText As String = "if(!confirm('" & CurrentPageData.GetCustomString("btnDeleteClientClick").Replace("'", "\'") & "')) {return false;}"
    '        Dim DefaultButtonText As String = CurrentPageData.GetCustomString("btnDefault")
    '        Dim NotApprovedPhotoInfoText As String = CurrentPageData.GetCustomString("NotApprovedPhotoInfoText")
    '        Dim DeclinedPhotoInfoText As String = CurrentPageData.GetCustomString("DeclinedPhotoInfoText")
    '        Dim NotApprovedPrivatePhotoInfoText As String = CurrentPageData.GetCustomString("NotApprovedPrivatePhotoInfoText")
    '        Dim EditButtonText As String = CurrentPageData.GetCustomString("EditButtonText")


    '        'Dim profileId As Integer = Me.Session("ProfileID")
    '        Dim ds As DSMembers = DataHelpers.GetEUS_CustomerPhotos_ByProfileOrMirrorID(Me.MasterProfileId)

    '        Dim dataTbl As New DataTable()
    '        dataTbl.Columns.Add("ImageId")
    '        dataTbl.Columns.Add("ImageUrl")
    '        dataTbl.Columns.Add("ImageThumbUrl")
    '        dataTbl.Columns.Add("IsDefault")
    '        dataTbl.Columns.Add("ImageCss")
    '        dataTbl.Columns.Add("DeleteButtonText")
    '        dataTbl.Columns.Add("DeleteButtonClientClickText")
    '        dataTbl.Columns.Add("DefaultButtonText")
    '        dataTbl.Columns.Add("DefaultVisible")
    '        dataTbl.Columns.Add("InfoVisible")
    '        dataTbl.Columns.Add("NotApprovedPhotoInfoText")
    '        dataTbl.Columns.Add("DisplayLevel")
    '        dataTbl.Columns.Add("EditButtonText")

    '        ' bind public photos list
    '        Dim foundRows As DSMembers.EUS_CustomerPhotosRow() = ds.EUS_CustomerPhotos.Select("DisplayLevel = 0")
    '        For Each row As DSMembers.EUS_CustomerPhotosRow In foundRows
    '            Try

    '                Dim dr As DataRow = dataTbl.NewRow()
    '                dr("DisplayLevel") = row.DisplayLevel
    '                dr("ImageId") = row.CustomerPhotosID

    '                dr("ImageUrl") = ProfileHelper.GetProfileImageURL(Me.GetCurrentProfile().ProfileID, row.FileName, Me.GetCurrentProfile().GenderId, False)
    '                dr("ImageThumbUrl") = ProfileHelper.GetProfileImageURL(Me.GetCurrentProfile().ProfileID, row.FileName, Me.GetCurrentProfile().GenderId, True)

    '                If (Not row.IsIsDefaultNull()) Then
    '                    dr("IsDefault") = row.IsDefault

    '                    If (row.IsDefault) Then

    '                    End If
    '                Else
    '                    dr("IsDefault") = False
    '                End If



    '                If (row.HasAproved) Then
    '                    dr("DefaultVisible") = (dr("IsDefault") = False)
    '                Else
    '                    dr("DefaultVisible") = False
    '                End If

    '                If (row.HasDeclined) Then
    '                    dr("InfoVisible") = True
    '                    dr("NotApprovedPhotoInfoText") = DeclinedPhotoInfoText
    '                ElseIf (Not row.HasAproved) Then
    '                    dr("InfoVisible") = True
    '                    dr("NotApprovedPhotoInfoText") = NotApprovedPhotoInfoText
    '                Else
    '                    dr("InfoVisible") = False
    '                End If


    '                If (foundRows.Count > 1 AndAlso row.CustomerPhotosID = foundRows(foundRows.Count - 1).CustomerPhotosID) Then
    '                    dr("ImageCss") = "last"
    '                End If

    '                dr("DeleteButtonText") = DeleteButtonText
    '                dr("DeleteButtonClientClickText") = DeleteButtonClientClickText
    '                dr("DefaultButtonText") = DefaultButtonText
    '                dr("EditButtonText") = EditButtonText

    '                dataTbl.Rows.Add(dr)
    '            Catch ex As Exception
    '                WebErrorMessageBox(Me, ex, "")
    '            End Try
    '        Next


    '        lvPubPhotos.DataSource = dataTbl
    '        lvPubPhotos.DataBind()

    '        If (dataTbl.Rows.Count = 0) Then
    '            divNoPhoto.Visible = True
    '        Else
    '            divNoPhoto.Visible = False
    '        End If


    '        dataTbl.Clear()


    '        ' bind private photos list
    '        foundRows = ds.EUS_CustomerPhotos.Select("DisplayLevel > 0")
    '        For Each row As DSMembers.EUS_CustomerPhotosRow In foundRows
    '            Try
    '                Dim dr As DataRow = dataTbl.NewRow()
    '                dr("DisplayLevel") = row.DisplayLevel
    '                dr("ImageId") = row.CustomerPhotosID

    '                Dim path As String = String.Format(ProfileHelper.gMemberPhotosDirectoryView, Me.MasterProfileId)
    '                dr("ImageUrl") = path & "/" & row.FileName
    '                'dr("ImageUrl") = ResolveUrl(path & "/" & row.FileName)

    '                Dim thumbPath As String = String.Format(ProfileHelper.gMemberPhotosThumbsDirectoryView, Me.MasterProfileId)
    '                dr("ImageThumbUrl") = thumbPath & "/" & row.FileName
    '                'dr("ImageThumbUrl") = ResolveUrl(thumbPath & "/" & row.FileName)

    '                dr("IsDefault") = False

    '                If (row.HasDeclined) Then
    '                    dr("InfoVisible") = True
    '                    dr("NotApprovedPhotoInfoText") = DeclinedPhotoInfoText
    '                ElseIf (Not row.HasAproved) Then
    '                    dr("InfoVisible") = True
    '                    dr("NotApprovedPhotoInfoText") = NotApprovedPrivatePhotoInfoText
    '                Else
    '                    dr("InfoVisible") = False
    '                End If

    '                If (foundRows.Count > 1 AndAlso row.CustomerPhotosID = foundRows(foundRows.Count - 1).CustomerPhotosID) Then
    '                    dr("ImageCss") = "last"
    '                End If

    '                dr("DeleteButtonText") = DeleteButtonText
    '                dr("DeleteButtonClientClickText") = DeleteButtonClientClickText
    '                dr("DefaultButtonText") = DefaultButtonText
    '                dr("EditButtonText") = EditButtonText

    '                dataTbl.Rows.Add(dr)
    '            Catch ex As Exception
    '                WebErrorMessageBox(Me, ex, "")
    '            End Try
    '        Next



    '        If (divNoPhoto.Visible) Then
    '            If (dataTbl.Rows.Count = 0) Then
    '                divNoPhoto.Visible = True
    '            Else
    '                divNoPhoto.Visible = False
    '            End If
    '        End If

    '    Catch ex As Exception
    '        WebErrorMessageBox(Me, ex, "Failed to load photos")
    '    End Try

    'End Sub



    'Protected Sub lvPubPhotos_ItemCommand(sender As Object, e As System.Web.UI.WebControls.ListViewCommandEventArgs) Handles lvPubPhotos.ItemCommand
    '    Try
    '        Dim photoId As Integer
    '        Integer.TryParse(e.CommandArgument.ToString(), photoId)
    '        ExecCmd(e.CommandName.ToUpper(), photoId)
    '    Catch ex As Exception
    '        WebErrorMessageBox(Me, ex, "lvPubPhotos_ItemCommand")
    '    End Try
    'End Sub



    'Private Sub ExecCmd(commandName As String, photoId As Integer)
    '    Try
    '        Select Case (commandName)

    '            Case "DELETE"

    '                Dim ds As DSMembers = DataHelpers.GetEUS_CustomerPhotosByCustomerPhotosID(photoId)
    '                Dim fileName As String = ds.EUS_CustomerPhotos.Rows(0).Field(Of String)("FileName")
    '                Dim isDefault As Boolean = False
    '                If (Not ds.EUS_CustomerPhotos.Rows(0).IsNull("IsDefault")) Then
    '                    isDefault = ds.EUS_CustomerPhotos.Rows(0).Field(Of Boolean)("IsDefault")
    '                End If
    '                'If (Not ds.EUS_CustomerPhotos.Rows(0).IsNull("IsDeleted")) Then
    '                ds.EUS_CustomerPhotos.Rows(0).SetField("IsDeleted", True)
    '                'End If

    '                'ds.EUS_CustomerPhotos.Rows(0).Delete()
    '                DataHelpers.UpdateEUS_CustomerPhotos(ds)

    '                Try
    '                    clsMyMail.SendMail(ConfigurationManager.AppSettings("gToEmail"), "Photo deleted on Goomena.com", "Photo is deleted. Filename " & fileName & ". Delete by " & Me.SessionVariables.DataRecordLoginMemberReturn.LoginName)
    '                Catch ex As Exception
    '                    WebErrorMessageBox(Me, ex, "ExecCmd")
    '                End Try


    '                If (isDefault) Then
    '                    ' set next approved public photo as default
    '                    SetNextDefaultPhoto()
    '                    RefreshProfilePreviewControl(Me.Page)
    '                End If

    '                ds.Dispose()

    '            Case "DEFAULT"
    '                Dim hasDefaultChanged As Boolean = False
    '                Dim ds As DSMembers = DataHelpers.GetEUS_CustomerPhotos_ByProfileOrMirrorID(Me.MasterProfileId)
    '                For Each rawRecord As DSMembers.EUS_CustomerPhotosRow In ds.EUS_CustomerPhotos.Rows
    '                    If (rawRecord.CustomerPhotosID = photoId AndAlso rawRecord.HasAproved = True) Then
    '                        rawRecord.IsDefault = True
    '                        hasDefaultChanged = True
    '                    Else
    '                        rawRecord.IsDefault = False
    '                    End If
    '                Next
    '                DataHelpers.UpdateEUS_CustomerPhotos(ds)
    '                ds.Dispose()

    '                RefreshProfilePreviewControl(Me.Page)

    '                If (hasDefaultChanged) Then
    '                    DataHelpers.UpdateEUS_Profiles_MirrorSetStatusUpdating(Me.MasterProfileId, ProfileStatusEnum.Updating)
    '                End If
    '        End Select

    '    Catch ex As Exception
    '        WebErrorMessageBox(Me, ex, "ExecCmd")
    '    End Try
    'End Sub

    ' ''' <summary>
    ' ''' set next approved public photo as default
    ' ''' </summary>
    ' ''' <remarks></remarks>
    'Public Sub SetNextDefaultPhoto()
    '    Dim hasDefaultChanged As Boolean = False
    '    Dim _ds1 As DSMembers = DataHelpers.GetEUS_CustomerPhotos_ByProfileOrMirrorID(Me.MasterProfileId)
    '    Dim foundRows As DSMembers.EUS_CustomerPhotosRow() = _ds1.EUS_CustomerPhotos.Select("DisplayLevel = 0")
    '    For Each row As DSMembers.EUS_CustomerPhotosRow In foundRows
    '        If (row.HasAproved AndAlso Not row.HasDeclined) Then
    '            row.IsDefault = True
    '            DataHelpers.UpdateEUS_CustomerPhotos(_ds1)
    '            hasDefaultChanged = True
    '            Exit For
    '        End If
    '    Next

    '    If (hasDefaultChanged) Then
    '        DataHelpers.UpdateEUS_Profiles_MirrorSetStatusUpdating(Me.MasterProfileId, ProfileStatusEnum.Updating)
    '    End If

    '    _ds1.Dispose()
    'End Sub


    ''!!! leave this handler to avoid error on scriptmanager, it used in it's event validation
    'Protected Sub lvPubPhotos_ItemDeleting(sender As Object, e As System.Web.UI.WebControls.ListViewDeleteEventArgs) Handles lvPubPhotos.ItemDeleting

    'End Sub



    'Protected Sub lvPubPhotos_DataBound(sender As Object, e As EventArgs) Handles lvPubPhotos.DataBound

    '    Try

    '        If (lvPubPhotos.Items.Count > 0) Then
    '            For Each itm As ListViewDataItem In lvPubPhotos.Items


    '                Try
    '                    Dim hdfDisplayLevel As HiddenField = itm.FindControl("hdfDisplayLevel")
    '                    Dim hdfImageID As HiddenField = itm.FindControl("hdfImageID")

    '                    'Dim _cbPhotosLevel1 As ASPxComboBox = itm.FindControl("cbPhotosLevel")
    '                    ''Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_PhotosDisplayLevel, Session("LagID"), "PhotosDisplayLevelId", _cbPhotosLevel1, True, "US")
    '                    '

    '                    Dim editPhotoWindow As ASPxPopupControl = itm.FindControl("editPhotoWindow")
    '                    Dim _cbPhotosLevel1 As ASPxComboBox = editPhotoWindow.Windows(0).FindControl("cbPhotosLevel")
    '                    _cbPhotosLevel1.ID = "cbPhotosLevel_" & hdfImageID.Value
    '                    editPhotoWindow.ClientInstanceName = editPhotoWindow.ID & "_" & hdfImageID.Value

    '                    If (Not SelectComboItem(_cbPhotosLevel1, hdfDisplayLevel.Value)) Then
    '                        For Each lei As ListEditItem In _cbPhotosLevel1.Items
    '                            If (lei.Value = 0) Then
    '                                lei.Selected = True
    '                                Exit For
    '                            End If
    '                        Next
    '                    End If

    '                    Dim lblInfoInsidePopup As Label = editPhotoWindow.Windows(0).FindControl("lblInfoInsidePopup")
    '                    lblInfoInsidePopup.Text = Me.CurrentPageData.GetCustomString("lblInfoInsidePopupChangingLevel")

    '                Catch ex As Exception
    '                    WebErrorMessageBox(Me, ex, "")
    '                End Try


    '            Next
    '        End If

    '        LoadLAG()
    '    Catch ex As Exception
    '        WebErrorMessageBox(Me, ex, "")
    '    End Try
    'End Sub


    Protected Sub cbpnlPhotos_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cbpnlPhotos.Callback

        Try
            Dim ctlid As String = e.Parameter.Replace("ctlid_", "")
            ctlid = ctlid.Remove(ctlid.IndexOf("_lvl_"))
            ctlid = ctlid.Remove(0, ctlid.IndexOf("_") + 1)

            Dim lvl As String = e.Parameter.Substring(e.Parameter.IndexOf("_lvl_") + Len("_lvl_"))


            'Dim allowAutoApproved As Boolean
            ''query for photo auto approve setting
            'Try
            '    Dim config As New clsConfigValues()
            '    allowAutoApproved = (config.auto_approve_photos = "1")
            'Catch ex As Exception
            '    WebErrorMessageBox(Me, ex, "AutoApprovePhoto query failed.")
            'End Try

            'Dim isDefault As Boolean = False

            'Try
            '    Dim ds As DSMembers = DataHelpers.GetEUS_CustomerPhotosByCustomerPhotosID(ctlid)
            '    Dim photoRow As DSMembers.EUS_CustomerPhotosRow = ds.EUS_CustomerPhotos.Rows(0)

            '    If (Not photoRow.IsNull("IsDefault")) Then
            '        isDefault = photoRow.IsDefault
            '    End If

            '    If (allowAutoApproved) Then
            '        photoRow.HasAproved = True
            '        photoRow.IsDefault = False
            '    Else
            '        If (photoRow.DisplayLevel > 0 AndAlso lvl = 0) Then
            '            photoRow.HasAproved = False
            '            photoRow.IsDefault = False
            '        End If
            '    End If

            '    photoRow.DisplayLevel = lvl
            '    DataHelpers.UpdateEUS_CustomerPhotos(ds)

            '    ds.Dispose()
            'Catch ex As Exception
            '    WebErrorMessageBox(Me, ex, "")
            'End Try

            'If (isDefault) Then
            '    SetNextDefaultPhoto()
            'End If

            LoadLAG()
            'LoadPhotos()
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


    Public Function GetVerificationDocsForCustomer(CustomerID As Integer) As DSCustomer.EUS_CustomerVerificationDocsDataTable
        Dim ds As DSCustomer = DataHelpers.EUS_CustomerVerificationDocs_GetByCustomerID(CustomerID)
        Return ds.EUS_CustomerVerificationDocs
    End Function


    Protected Sub odsDocs_Selecting(sender As Object, e As System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs) Handles odsDocs.Selecting
        e.InputParameters("CustomerID") = Me.MasterProfileId
    End Sub
End Class
﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucReferrerMenuTop.ascx.vb"
    Inherits="Dating.Referrals.Server.Site.Web.ucReferrerMenuTop" %>

    
<dx:ASPxMenu ID="mnu1" runat="server" AutoSeparators="RootOnly" 
    ItemAutoWidth="False" Width="100%" 
            SeparatorWidth="0px" BackColor="White" EncodeHtml="False" 
    ItemSpacing="5px" GutterWidth="20px">
    <GutterBackgroundImage Repeat="NoRepeat" />
    <Items>
        <dx:MenuItem NavigateUrl="~/Members/Referrer.aspx" Text="Dashboard">
            <Image Url="~/Images2/members/home.png">
            </Image>
            <ItemStyle>
            <BackgroundImage Repeat="NoRepeat" />
            </ItemStyle>
        </dx:MenuItem>
        <dx:MenuItem NavigateUrl="javascript:void(0);" Text="Tasks" DropDownMode="True">
            <Items>
                <dx:MenuItem NavigateUrl="~/Members/ReferrerFamilyTree.aspx" 
                    Text="Οικογενειακό δένδρο">
                </dx:MenuItem>
                <dx:MenuItem NavigateUrl="~/Members/ReferrerAnalytics.aspx" 
                    Text="Αναλυτική κατάσταση">
                </dx:MenuItem>
                <dx:MenuItem NavigateUrl="~/Members/ReferrerSupplies.aspx" 
                    Text="Προμήθειες μου">
                </dx:MenuItem>
                <dx:MenuItem NavigateUrl="~/Members/ReferrerStatistics.aspx" Text="Στατιστικά">
                </dx:MenuItem>
                <dx:MenuItem NavigateUrl="~/Members/ReferrerPayments.aspx" Text="Πληρωμές μου">
                </dx:MenuItem>
            </Items>
            <Image Url="~/Images2/members/epiloges.png">
            </Image>
        </dx:MenuItem>
        <dx:MenuItem DropDownMode="True" NavigateUrl="javascript:void(0);" Text="Options">
            <Items>
                <dx:MenuItem NavigateUrl="~/Members/ReferrerSettings.aspx" Text="Ρυθμίσεις">
                </dx:MenuItem>
                <dx:MenuItem NavigateUrl="~/Members/ReferrerPercents.aspx" Text="Ποσοστά μου">
                </dx:MenuItem>
            </Items>
            <Image Url="~/Images2/members/ergasies.png">
            </Image>
        </dx:MenuItem>
        <dx:MenuItem Text="My Payments">
            <Image Url="~/Images2/members/pay-out.png">
            </Image>
        </dx:MenuItem>
        <dx:MenuItem Text="Help guide">
        </dx:MenuItem>
    </Items>
    <LoadingPanelImage Url="~/App_Themes/PlasticBlue/Web/Loading.gif">
    </LoadingPanelImage>
    <ItemSubMenuOffset FirstItemY="-1" LastItemY="-1" Y="-1" />
    <RootItemSubMenuOffset FirstItemX="1" LastItemX="1" X="1" />
    <ItemStyle DropDownButtonSpacing="20px" ToolbarDropDownButtonSpacing="5px" 
        ToolbarPopOutImageSpacing="5px" BackColor="#FF7A15" ForeColor="White" 
        Font-Size="16px" >
    <DropDownButtonStyle>
        <Paddings  /><%--PaddingLeft="10px" PaddingRight="10px"--%>
        <BackgroundImage HorizontalPosition="center" 
            ImageUrl="~/Images/referrer/arrow.png" Repeat="NoRepeat" 
            VerticalPosition="center" />
        <BorderLeft BorderColor="#FF7A15" />
    </DropDownButtonStyle>
    <SelectedStyle BackColor="#FEAA16">
    </SelectedStyle>
    <HoverStyle>
        <Border BorderColor="#8C8C8C" BorderStyle="Solid" BorderWidth="1px" />
    </HoverStyle>
    <Paddings PaddingLeft="4px" PaddingRight="4px" PaddingBottom="2px" PaddingTop="2px" />
    <Border BorderColor="#8C8C8C" BorderStyle="Solid" BorderWidth="1px" />
    </ItemStyle>
    <SubMenuItemStyle Height="30px" Width="150px" BackColor="#FF7A15" 
        ForeColor="White">
        <SelectedStyle BackColor="#FEAA16">
        </SelectedStyle>
        <Border BorderStyle="None" />
        <BorderBottom BorderStyle="None" BorderWidth="0px" />
    </SubMenuItemStyle>
    <SubMenuStyle GutterWidth="0px" />
    <BackgroundImage ImageUrl="../Images2/members/menu-bar-bg.jpg" />
    <Border BorderStyle="None" />
</dx:ASPxMenu>


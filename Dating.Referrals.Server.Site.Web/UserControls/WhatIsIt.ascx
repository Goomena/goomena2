﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="WhatIsIt.ascx.vb" Inherits="Dating.Referrals.Server.Site.Web.WhatIsIt" %>


<dx:ASPxPopupControl runat="server"
    Height="594px" Width="1000px"
    ID="popupWhatIs"
    ClientInstanceName="popupWhatIs" 
    ClientIDMode="AutoID" AllowDragging="True" PopupVerticalAlign="Below" 
        PopupHorizontalAlign="WindowCenter" PopupElementID="lnkViewDescription" 
        Modal="True" AllowResize="True" AutoUpdatePosition="True"><ContentStyle HorizontalAlign="Left" VerticalAlign="Middle">
        <BackgroundImage ImageUrl="../images/bg.jpg" />
        <Paddings Padding="10px" />
        <Paddings Padding="10px"></Paddings>
        <BackgroundImage ImageUrl="../images/bg.jpg"></BackgroundImage>
    </ContentStyle>
    <CloseButtonStyle>
        <HoverStyle>
            <BackgroundImage ImageUrl="~/Images/close-button1.png" Repeat="NoRepeat" HorizontalPosition="center"
                VerticalPosition="center"></BackgroundImage>
        </HoverStyle>
        <BackgroundImage ImageUrl="~/Images/close-button2.png" Repeat="NoRepeat" HorizontalPosition="center"
            VerticalPosition="center"></BackgroundImage>
    </CloseButtonStyle>
    <CloseButtonImage Url="~/Images/spacer10.png" Height="17px" Width="17px">
    </CloseButtonImage>
    <SizeGripImage Height="40px" Url="~/Images/resize.png" Width="40px">
    </SizeGripImage>
    <HeaderStyle BackColor="#35619F" Font-Bold="True" ForeColor="White">
        <Paddings PaddingBottom="10px" PaddingTop="10px" />
        <Paddings PaddingTop="10px" PaddingBottom="10px"></Paddings>
    </HeaderStyle>
    <%--<ModalBackgroundStyle CssClass="modalPopup">
    </ModalBackgroundStyle>--%>
    <ModalBackgroundStyle BackColor="Black" Opacity="70"></ModalBackgroundStyle>
    <ContentCollection>
        <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server" SupportsDisabledAttribute="True">
        </dx:PopupControlContentControl>
    </ContentCollection>
</dx:ASPxPopupControl>


<script type="text/javascript">
    function OnMoreInfoClick(contentUrl, obj, headerText) {
        if (headerText != null) popupWhatIs.SetHeaderText(headerText);
        if (obj != null) popupWhatIs.SetPopupElementID(obj.id);
        popupWhatIs.SetContentUrl(contentUrl);
        popupWhatIs.Show();
    }
</script>


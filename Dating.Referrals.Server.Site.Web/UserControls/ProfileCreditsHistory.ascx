﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ProfileCreditsHistory.ascx.vb" Inherits="Dating.Referrals.Server.Site.Web.ProfileCreditsHistory" %>
<dx:ASPxGridView ID="gv" runat="server" AutoGenerateColumns="False" 
    DataSourceID="sdsCreditsHistory" KeyFieldName="CustomerCreditsId" 
    Width="100%">
    <Columns>
        <dx:GridViewDataTextColumn FieldName="CustomerCreditsId" ReadOnly="True" 
            Visible="False" VisibleIndex="0">
            <EditFormSettings Visible="False" />
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="CustomerId" Visible="False" 
            VisibleIndex="1">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="Credits" Visible="False" VisibleIndex="2">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataDateColumn FieldName="Date" VisibleIndex="3">
            <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy HH:mm">
            </PropertiesDateEdit>
        </dx:GridViewDataDateColumn>
        <dx:GridViewDataTextColumn FieldName="CustomerTransactionID" Visible="False" 
            VisibleIndex="4">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="CreditsTypeId" Visible="False" 
            VisibleIndex="5">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="ConversationWithCustomerId" 
            Visible="False" VisibleIndex="6">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="ConversationWithLoginName" 
            Visible="False" VisibleIndex="7">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="CreditsType" Visible="False" 
            VisibleIndex="8">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="Action" ReadOnly="True" VisibleIndex="9">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="Description" ReadOnly="True" 
            VisibleIndex="10">
        </dx:GridViewDataTextColumn>
    </Columns>
    <SettingsPager PageSize="40">
    </SettingsPager>
    <Styles>
        <Header BackColor="#35619F" Font-Bold="True" ForeColor="White">
        </Header>
        <AlternatingRow BackColor="#EBF5FA" Enabled="True">
        </AlternatingRow>
        <Cell>
            <Paddings PaddingBottom="5px" PaddingLeft="5px" PaddingTop="5px" />
        </Cell>
        <HeaderPanel BackColor="#35619F">
        </HeaderPanel>
    </Styles>
</dx:ASPxGridView>

<asp:SqlDataSource ID="sdsCreditsHistory" runat="server" 
    ConnectionString="<%$ ConnectionStrings:AppDBconnectionString %>" 
    SelectCommand="CustomerCreditsHistory" SelectCommandType="StoredProcedure">
    <SelectParameters>
        <asp:Parameter DefaultValue="0" Name="CustomerId" />
    </SelectParameters>
</asp:SqlDataSource>


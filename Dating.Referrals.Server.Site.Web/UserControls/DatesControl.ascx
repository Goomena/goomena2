﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="DatesControl.ascx.vb" 
    Inherits="Dating.Referrals.Server.Site.Web.DatesControl" %>
<asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">

    <asp:View ID="View1" runat="server">
    </asp:View>
    
    
    <asp:View ID="vwNoPhoto" runat="server">
        <asp:FormView ID="fvNoPhoto" runat="server">
        <ItemTemplate>
<div class="items_hard">
    <%# Eval("HasNoPhotosText")%>
	<p><asp:HyperLink ID="lnk" runat="server" CssClass="btn lighter" NavigateUrl="~/Members/Photos.aspx"><%# Eval("AddPhotosText")%><i class="icon-chevron-right"></i></asp:HyperLink></p>
	<div class="clear"></div>
</div>
        </ItemTemplate>
        </asp:FormView>
    </asp:View>
    
    
    <asp:View ID="vwNoOffers" runat="server">
        <asp:FormView ID="fvNoOffers" runat="server">
        <ItemTemplate>
<div class="items_none">
    <%# Eval("NoOfferText")%>
	<p><asp:HyperLink ID="lnk4" runat="server" CssClass="btn lighter" NavigateUrl="~/Members/Search.aspx"><%# Eval("SearchOurMembersText")%><i class="icon-chevron-right"></i></asp:HyperLink></p>
	<div class="clear"></div>
</div>
        </ItemTemplate>
        </asp:FormView>
    </asp:View>


    <asp:View ID="vwDatesOffers" runat="server">
<asp:Repeater ID="rptDates" runat="server">
<ItemTemplate>
<div class="date_item" login="<%# Eval("OtherMemberLoginName")%>">
    <div class="left">
        <div class="photo">
            <div class="thumb">
                <asp:HyperLink ID="hl2" runat="server" NavigateUrl='<%# Eval("OtherMemberProfileViewUrl") %>'><img src="<%# Eval("OtherMemberImageUrl") %>" alt="<%# Eval("OtherMemberLoginName")%>" /></asp:HyperLink></div>
        </div>
        <div class="info">
            <h2><asp:HyperLink ID="hl" runat="server" NavigateUrl='<%# Eval("OtherMemberProfileViewUrl") %>' Text='<%# Eval("OtherMemberLoginName")%>' /></h2>
            <p><%# Eval("OtherMemberHeading")%></p>
            <div class="stats">
                <%# MyBase.WriteSearch_MemberInfo(Container.DataItem)%>
            </div>
        </div>
        <div class="info" style="position:absolute;top:5px;right:10px;z-index:1;text-align:right;height:60px;">
            <table>
                <%--<tr ID="lblStatusUnl" runat="server" Visible='<%# (Eval("CommunicationStatusUnlimitedText") IsNot Nothing) %>'>
                    <td><%# Eval("CommunicationStatusText") %></td>
                    <td><span class="jewelUnlimited"><span class="countValue"><%# Eval("CommunicationStatusUnlimitedText")%></span></span></td>
                    <td><asp:LinkButton ID="lnkWhatIsUNL" runat="server" OnClientClick="OnMoreInfoClick('[contentUrl]',this,'[headerText]');return false;">
                        <img runat="server" id="icon_tipUNL" src="~/Images/icon_tip16.png" alt=""/></asp:LinkButton></td>
                </tr>
                <tr ID="lblStatusLtd" runat="server" Visible='<%# (Eval("CommunicationStatusLimitedText") IsNot Nothing) %>'>
                    <td><%# Eval("CommunicationStatusText") %></td>
                    <td><span class="jewelLimited"><span class="countValue"><%# Eval("CommunicationStatusLimitedText")%></span></span></td>
                    <td><asp:LinkButton ID="lnkWhatIsLTD" runat="server" OnClientClick="OnMoreInfoClick('[contentUrl]',this,'[headerText]');return false;">
                        <img runat="server" id="icon_tipLTD" src="~/Images/icon_tip16.png" alt=""/></asp:LinkButton></td>
                </tr>--%>
                <tr>
                    <td><asp:Label ID="lblLastDating" runat="server" Text='<%# Eval("LastDatingText") %>'/></td>
                    <td><asp:Label ID="lblLastDatingDate" runat="server" Text='<%# Eval("LastDatingDateText") %>'/></td>
                    <td>&nbsp;</td>
                </tr>
            </table>
            <%--<div style="margin-top:50px;">
                <asp:Label ID="lblDateFirst" runat="server" Text='<%# Eval("FirstDateText") %>'></asp:Label>
            </div>--%>
        </div>
    </div>
    <div class="right">
        <div class="bot_right_actions">
            <div class="btn-group" ID="RejectsMenu" runat="server" Visible='<%# Eval("AllowRejectsMenu") %>'>
                <a class="btn btn-small dropdown-toggle" data-toggle="dropdown" href="#"><%# Eval("RejectText")%><span class="caret"></span></a>
                <ul class="dropdown-menu right-menu" style="text-align:left;">
                    <li><asp:LinkButton ID="lnkDelConv" runat="server" CommandName="REJECT_DELETECONV" CommandArgument='<%# Eval("OtherMemberProfileID")%>' Text='<%# Eval("RejectDeleteConversationText")%>' OnClientClick="return confirm('{0}')" /></li>
                    <li><asp:LinkButton ID="lnkBlock" runat="server" CommandName="REJECT_BLOCK" CommandArgument='<%# Eval("OtherMemberProfileID")%>' Text='<%# Eval("RejectBlockText")%>' /></li>
                </ul>
            </div>
            <div class="rfloat">
                <asp:LinkButton ID="lnkHistory" runat="server" CssClass="btn btn-small" 
                    Visible='<%# Eval("AllowHistory") %>' OnClientClick='<%# Eval("HistoryNavigateUrl")%>'><%# Eval("HistoryText")%></asp:LinkButton>
            </div>
            <div class="rfloat">
                <asp:HyperLink ID="lnkConversation" runat="server" CssClass="btn btn-small" 
                    Visible='<%# Eval("AllowConversation") %>' NavigateUrl='<%# Eval("ConversationNavigateUrl")%>'><%# Eval("ConversationText")%></asp:HyperLink>
            </div>
        </div>
        <div ID="divDistance" class='<%# "tt_enabled distance " & Eval("DistanceCss") %>' runat="server" style="display: none;"></div>
        <div class="tooltip tooltip_search" style="display: none;">
            <div class="tooltip_text"><%# Eval("MilesAwayText")%></div>
        </div>

    </div>
    <div class="clear"></div>
</div>
</ItemTemplate>
</asp:Repeater>
    </asp:View>


</asp:MultiView>














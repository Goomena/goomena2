﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ProfileEdit.ascx.vb" Inherits="Dating.Referrals.Server.Site.Web.ProfileEdit" %>
<%@ Register Assembly="DevExpress.Web.v13.1, Version=13.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>
<%@ Register Src="~/UserControls/DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<div id="pe">

	<!-- Form Box Wrapper -->
<script type="text/javascript">
    // <![CDATA[
    function OnCountryChanged(cbCountry) {
        LoadingPanel.Hide();
        cbpnlZip.PerformCallback("country_" + cbCountry.GetValue().toString());
        LoadingPanel.Show();
    }
    function OnRegionChanged(cbRegion) {
        LoadingPanel.Hide();
        cbpnlZip.PerformCallback("region_" + cbRegion.GetValue().toString());
        LoadingPanel.Show();
    }
    function OnCityChanged(cbCity) {
        LoadingPanel.Hide();
        cbpnlZip.PerformCallback("city_" + cbCity.GetValue().toString() + "_region_" + cbRegion.GetValue().toString());
        LoadingPanel.Show();
    }
    function showZip() {
        //var zip = jQuery('#< %= liLocationsZip.ClientID % >');
        var zip = jQuery('#<%= msg_SelectRegion.ClientID %>');
        var region = jQuery('#<%= liLocationsRegion.ClientID %>');
        var city = jQuery('#<%= liLocationsCity.ClientID %>');

        //zip.show()
        region.hide()
        city.hide()

        var status = jQuery('#<%= hdfLocationStatus.ClientID %>');
        status.val("zip")
    }
    function showRegions() {
        //var zip = jQuery('#<%= liLocationsZip.ClientID %>');
        var zip = jQuery('#<%= msg_SelectRegion.ClientID %>');
        var region = jQuery('#<%= liLocationsRegion.ClientID %>');
        var city = jQuery('#<%= liLocationsCity.ClientID %>');

        zip.hide()
        region.show()
        city.show()
        var status = jQuery('#<%= hdfLocationStatus.ClientID %>');
        status.val("region")
    }



	jQuery(function ($) {
	    $('.pe_box').each(function () {
	        var pe_box = this;

	        $('.collapsibleHeader', pe_box).click(function () {
	            $('.pe_form', pe_box).toggle('fast', function () {
	                // Animation complete.
	                var isVisible = ($('.pe_form', pe_box).css('display') == 'none');
	                if (isVisible) {
	                    $('.togglebutton', pe_box).text("+");
	                    $('.collapsibleHeader', pe_box).attr("title", "<%= CurrentPageData.GetCustomString("msg_ClickToExpand") %>");
	                }
	                else {
	                    $('.togglebutton', pe_box).text("-");
	                    $('.collapsibleHeader', pe_box).attr("title", "<%= CurrentPageData.GetCustomString("msg_ClickToCollapse") %>");
	                }
	            });

	        });
	    });
    	});
</script>	
	<div class="pe_wrapper">
		<div class="pe_box">
            <h2 class="collapsibleHeader" title="<%= CurrentPageData.GetCustomString("msg_ClickToCollapse") %>"><a href="javascript:void(0);" class="togglebutton">-</a> <asp:Literal ID="msg_LocationHeader" runat="server" Text=""></asp:Literal></h2>
			<div class="pe_form">
                <asp:Literal ID="msg_LocationDesc" runat="server" Text=""></asp:Literal>
                <asp:HiddenField ID="hdfLocationStatus" runat="server" />
                            <dx:ASPxCallbackPanel ID="cbpnlZip" runat="server"
                                ClientInstanceName="cbpnlZip" 
                                CssFilePath="~/App_Themes/DevEx/{0}/styles.css" 
                    CssPostfix="DevEx" ShowLoadingPanel="False" ShowLoadingPanelImage="False">
                                <ClientSideEvents EndCallback="function(s, e) {
	LoadingPanel.Hide();
}" CallbackError="function(s, e) {
	LoadingPanel.Hide();
}" />
                                <LoadingPanelImage Url="~/App_Themes/DevEx/Web/Loading.gif">
                                </LoadingPanelImage>
                                <LoadingPanelStyle ImageSpacing="5px">
                                </LoadingPanelStyle>
                                <PanelCollection>
                                    <dx:PanelContent ID="PanelContent1" runat="server" SupportsDisabledAttribute="True">

                <ul class="defaultStyle">
					<!-- location -->
					<li id="liLocationString" runat="server">
						<asp:Label ID="msg_LocationText" runat="server" Text="" CssClass="peLabel floatLeft" AssociatedControlID="lblLocation"/>
						<div class="form_right floatLeft">
						    <asp:Label ID="lblLocation" runat="server" Text="" CssClass="locations_string lfloat"/>
						</div>
					</li>
					<!-- Country  -->
					<li>
                        <asp:Panel ID="pnlCountryErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblCountryErr" runat="server" Text=""></asp:Label>
                        </asp:Panel>
					
                        <asp:Label ID="msg_CountryText" runat="server" Text="" class="peLabel floatLeft" AssociatedControlID="cbCountry"/>
						<div class="form_right floatLeft">
                            <dx:ASPxComboBox ID="cbCountry" runat="server" Width="310px" Font-Size="14px" 
                                Height="18px" EncodeHtml="False" 
                                IncrementalFilteringMode="StartsWith">
                                <ClientSideEvents SelectedIndexChanged="function(s, e) {  OnCountryChanged(s); }" EndCallback="function(s, e) {
	LoadingPanel.Hide();
}" />
        
                            </dx:ASPxComboBox>	
						</div>
					</li>
					<!-- region  -->
					<li class="locations_city" id="liLocationsRegion" runat="server">
                        <asp:Panel ID="pnlRegionErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblRegionErr" runat="server" Text=""></asp:Label>
                        </asp:Panel>
                        <asp:Label ID="msg_RegionText" runat="server" Text="" class="peLabel floatLeft" AssociatedControlID="cbRegion"/>
						<!--<span class="zip_list" style="position:relative;top:0;"><asp:LinkButton ID="msg_SelectZip" runat="server" OnClientClick="showZip();return false;"/></span>-->
						<div class="form_right floatLeft">
                            <dx:ASPxComboBox ID="cbRegion" runat="server" Width="310px" Font-Size="14px" 
                                Height="18px" EncodeHtml="False" EnableSynchronization="False"  ClientInstanceName="cbRegion"
                                IncrementalFilteringMode="StartsWith" DataSourceID="sdsRegion" 
                                TextField="region1" ValueField="region1">
                                <ClientSideEvents SelectedIndexChanged="function(s, e) {
OnRegionChanged(s);
}" EndCallback="function(s, e) {
	LoadingPanel.Hide();
}" />
                            </dx:ASPxComboBox>	
						    <asp:SqlDataSource ID="sdsRegion" runat="server" 
                                ConnectionString="<%$ ConnectionStrings:AppDBconnectionString %>" 
                                SelectCommand="SELECT 
DISTINCT region1 
FROM SYS_GEO_GR 
 	with (nolock)
WHERE (countrycode = @countrycode) 
AND   ((countrycode=N'GR' and language = @language) or countrycode<>N'GR')">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="cbCountry" DefaultValue="GR" 
                                        Name="countrycode" PropertyName="Value" />
                                    <asp:Parameter DefaultValue="EN" Name="language" />
                                </SelectParameters>
                            </asp:SqlDataSource>
						</div>
					</li>
					<!-- city  -->
					<li class="locations_city" id="liLocationsCity" runat="server">
                        <asp:Panel ID="pnlCityErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblCityErr" runat="server" Text=""></asp:Label>
                        </asp:Panel>
						<asp:Label ID="msg_CityText" runat="server" Text="" CssClass="peLabel floatLeft" AssociatedControlID="cbCity"/>
						<div class="form_right floatLeft">
                            <dx:ASPxComboBox ID="cbCity" runat="server" Width="310px" Font-Size="14px" 
                                Height="18px" EncodeHtml="False" EnableSynchronization="False"  ClientInstanceName="cbCity"
                                IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" 
                                DataSourceID="sdsCity" TextField="city" ValueField="city">
                                <clientsideevents SelectedIndexChanged="function(s, e) {
  OnCityChanged(s);
}" />
                            </dx:ASPxComboBox>	
                            <asp:SqlDataSource ID="sdsCity" runat="server" 
                                ConnectionString="<%$ ConnectionStrings:AppDBconnectionString %>" 
                                SelectCommand="SELECT DISTINCT city 
FROM SYS_GEO_GR 
		with (nolock)
WHERE (region1 = @region1) 
AND (countrycode = @countrycode) 
AND ((countrycode=N'GR' and language = @language) or countrycode<>N'GR')">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="cbCountry" DefaultValue="GR" 
                                        Name="countrycode" PropertyName="Value" />
                                    <asp:ControlParameter ControlID="cbRegion" Name="region1" 
                                        PropertyName="Value" />
                                    <asp:Parameter DefaultValue="EN" Name="language" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                            <dx:ASPxTextBox ID="txtCity" runat="server" autocomplete="off" Visible="False" />
                         </div>
					</li>
					<!-- zip  -->
					<li class="locations_zip" id="liLocationsZip" runat="server">

                        <asp:Panel ID="pnlZipErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblZipErr" runat="server" Text=""></asp:Label></asp:Panel>

                        <asp:Label ID="msg_ZipCodeText" runat="server" CssClass="peLabel floatLeft" style="height:30px" for="zip" AssociatedControlID="msg_SelectRegion"/>
						<span class="zip_list" style="position:relative;top:0;"><asp:LinkButton id="msg_SelectRegion" runat="server" CssClass="js" OnClientClick="showRegions();return false;" /></span>
						<div class="form_right floatLeft">
                            <dx:ASPxTextBox ID="txtZip" runat="server" autocomplete="off" />
                            <asp:Label id="msg_ZipExample" runat="server" />
						</div>
					</li>
					<li style="padding: 10px 0 5px 0;">														
						<label class="peLabel floatLeft">&nbsp;</label> <div class="form_right floatLeft">
                            <dx:ASPxCheckBox ID="chkTravelOnDate" runat="server" CssClass="text" 
                                Font-Size="14px" Height="18px" EncodeHtml="False">
                            </dx:ASPxCheckBox>
						</div>
					</li>

				</ul>
                                    </dx:PanelContent>
                                </PanelCollection>
                            </dx:ASPxCallbackPanel>
			<div class="clear"></div>
			</div>
		</div>
	</div>


	<div class="padding"></div>
	<div class="pe_wrapper">
		<div class="pe_box">
            <h2 class="collapsibleHeader" title="<%= CurrentPageData.GetCustomString("msg_ClickToCollapse") %>"><a href="javascript:void(0);" class="togglebutton">-</a> <asp:Literal ID="msg_SomePersonalInfo" runat="server" Text=""></asp:Literal></h2><asp:Literal ID="msg_SomePersonalInfoDescr" runat="server" Text=""></asp:Literal><div class="pe_form">
				<ul class="defaultStyle">
					<li id="liGenderContainer" runat="server">
                        <asp:Panel ID="pnlGenderErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblGenderErr" runat="server" Text=""></asp:Label></asp:Panel><label class="peLabel floatLeft"><asp:Literal ID="msg_IAm" runat="server" Text="msg_IAm"/></label>
						<div class="form_right floatLeft">
                            <dx:ASPxRadioButtonList ID="rblGender" runat="server" RepeatLayout="Flow" 
                                                RepeatDirection="Horizontal" Font-Size="14px" Height="18px" ClientInstanceName="RegisterGenderRadioButtonList">
                                            <Paddings PaddingLeft="0px" />
                                            <ValidationSettings ValidationGroup="RegisterGroup" ErrorDisplayMode="ImageWithTooltip" ErrorTextPosition="Left">
                                                <RequiredField ErrorText="" 
                                                    IsRequired="True" />
                                            </ValidationSettings>
                                            <Border BorderStyle="None" />
                                            <RadioButtonStyle BackgroundImage-HorizontalPosition="center" HorizontalAlign="Center"></RadioButtonStyle>
                                            </dx:ASPxRadioButtonList>
						</div>
					</li>
					<%--<li id="liWhoIsContainer" runat="server">
                        <asp:Panel ID="pnlAccountTypeErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblAccountTypeErr" runat="server" Text=""></asp:Label></asp:Panel>
						<asp:Label ID="msg_WhoIs" runat="server" Text="" CssClass="peLabel floatLeft" AssociatedControlID="cbAccountType"/>
						<div class="form_right floatLeft">
                            <dx:ASPxComboBox ID="cbAccountType" runat="server" ClientInstanceName="RegisterAccountTypeComboBox" EncodeHtml="False">
                                            <ClientSideEvents Validation="function(s, e) {
if(s.GetSelectedIndex()==0) e.isValid=false;
}" />
                                            <ValidationSettings ValidationGroup="RegisterGroup" ErrorDisplayMode="ImageWithTooltip" ErrorTextPosition="Left"
                                                ErrorText="">
                                                <RequiredField ErrorText="" 
                                                    IsRequired="True" />
                                            </ValidationSettings>
                                            </dx:ASPxComboBox>
						</div>
					</li>--%>
					<li id="liBirthdayContainer" runat="server">
                        <asp:Panel ID="pnlBirthdateErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblBirthdateErr" runat="server" Text=""></asp:Label></asp:Panel>
						<asp:Label ID="msg_MyBirthdate" runat="server" Text="" CssClass="peLabel floatLeft" AssociatedControlID="ctlDate"/>
						<div class="form_right floatLeft">
                            <uc1:DateControl ID="ctlDate" runat="server" />
						</div>
					</li>
					<li>
                        <asp:Panel ID="pnlFirstNameErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblFirstNameErr" runat="server" Text=""></asp:Label></asp:Panel>
						<asp:Label ID="msg_FirstName" runat="server" Text="" CssClass="peLabel floatLeft" AssociatedControlID="txtFirstName"/>
						<div class="form_right floatLeft">
                            <dx:ASPxTextBox ID="txtFirstName" runat="server" autocomplete="off" />
						</div>
					</li>
					<li>
                        <asp:Panel ID="pnlLastNameErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblLastNameErr" runat="server" Text=""></asp:Label></asp:Panel>
						<asp:Label ID="msg_LastName" runat="server" Text="" CssClass="peLabel floatLeft" AssociatedControlID="txtLastName"/>
						<div class="form_right floatLeft">
                            <dx:ASPxTextBox ID="txtLastName" runat="server" autocomplete="off" />
						</div>
					</li>
					<li style="display: none">
                        <asp:Panel ID="pnlCityAreaErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblCityAreaErr" runat="server" Text=""></asp:Label></asp:Panel>
						<asp:Label ID="msg_CityArea" runat="server" Text="" CssClass="peLabel floatLeft" AssociatedControlID="txtCityArea"/>
						<div class="form_right floatLeft">
                            <dx:ASPxTextBox ID="txtCityArea" runat="server" autocomplete="off" />
						</div>
					</li>
					<li>
                        <asp:Panel ID="pnlAddressErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblAddressErr" runat="server" Text=""></asp:Label></asp:Panel>
						<asp:Label ID="msg_Address" runat="server" Text="" CssClass="peLabel floatLeft" AssociatedControlID="txtAddress"/>
						<div class="form_right floatLeft">
                            <dx:ASPxTextBox ID="txtAddress" runat="server" autocomplete="off" />
						</div>
					</li>
					<li>
                        <asp:Panel ID="pnleMailErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lbleMailErr" runat="server" Text=""></asp:Label></asp:Panel>
						<asp:Label ID="msg_eMail" runat="server" Text="" CssClass="peLabel floatLeft" AssociatedControlID="txteMail"/>
						<div class="form_right floatLeft">
                            <dx:ASPxTextBox ID="txteMail" runat="server" autocomplete="off" />
						</div>
					</li>
					<li>
                        <asp:Panel ID="pnlTelephoneErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblTelephoneErr" runat="server" Text=""></asp:Label></asp:Panel>
						<asp:Label ID="msg_Telephone" runat="server" Text="" CssClass="peLabel floatLeft" AssociatedControlID="txtTelephone"/>
						<div class="form_right floatLeft">
                            <dx:ASPxTextBox ID="txtTelephone" runat="server" autocomplete="off" />
						</div>
					</li>
					<li>
                        <asp:Panel ID="pnlCellularErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblCellularErr" runat="server" Text=""></asp:Label></asp:Panel>
						<asp:Label ID="msg_Cellular" runat="server" Text="" CssClass="peLabel floatLeft" AssociatedControlID="txtCellular"/>
						<div class="form_right floatLeft">
                            <dx:ASPxTextBox ID="txtCellular" runat="server" autocomplete="off" />
						</div>
					</li>
				</ul>
			<div class="clear"></div>
			</div>
		</div>
	</div>

	<div class="padding"></div>
    <div class="pe_wrapper">
		<div class="pe_box">
			<h2 class="collapsibleHeader" title="<%= CurrentPageData.GetCustomString("msg_ClickToCollapse") %>"><a href="javascript:void(0);" class="togglebutton">-</a> <asp:Literal ID="msg_YrSlfTitle" runat="server"/></h2>
			<div class="pe_form">
                <asp:Label ID="msg_YrSlfnotice" runat="server" Text=""></asp:Label>
				<ul class="defaultStyle">
					<li>
                        <asp:Panel ID="pnlHeadingErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblHeadingErr" runat="server" Text=""></asp:Label></asp:Panel>
						<asp:Label ID="msg_YrSlfHeadingTitle" runat="server" Text="" CssClass="peLabel floatLeft" AssociatedControlID="txtYrSlfHeading"/>
						<div class="form_right floatLeft">
                            <dx:ASPxTextBox ID="txtYrSlfHeading" runat="server" CssClass="text" 
                            Text="Sexy woman looking for gentlemen"/>
						</div>
					</li>
					<li class="clearboth">
                        <asp:Panel ID="pnlDescErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblDescErr" runat="server" Text=""></asp:Label></asp:Panel>
						<asp:Label ID="msg_YrSlfDescTitle" runat="server" Text="" CssClass="peLabel floatLeft" AssociatedControlID="txtYrSlfDesc"/>
						<div class="form_right floatLeft">
                            <asp:TextBox ID="txtYrSlfDesc" runat="server" TextMode="MultiLine" Columns="50" Rows="10"></asp:TextBox></div></li>
                    <li class="clearboth">
                        <asp:Panel ID="pnlFirstDateExptErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblFirstDateExptErr" runat="server" Text=""></asp:Label></asp:Panel>
						<asp:Label ID="msg_YrSlfFirstDate" runat="server" Text="" CssClass="peLabel floatLeft" AssociatedControlID="txtYrSlfSeeking" style="line-height:16px;"/>
						<div class="form_right floatLeft">
                            <asp:TextBox ID="txtYrSlfSeeking" runat="server" TextMode="MultiLine" Columns="50" Rows="10"></asp:TextBox></div>
                            </li>
                            </ul>
                            <div class="clear"></div>
			</div>
		</div>
	</div>
	<div class="padding"></div><div class="pe_wrapper">
		<div class="pe_box">
			<h2 class="collapsibleHeader" title="<%= CurrentPageData.GetCustomString("msg_ClickToCollapse") %>"><a href="javascript:void(0);" class="togglebutton">-</a> <asp:Literal ID="msg_OtherTitle" runat="server"/></h2>
			<div class="pe_form">
				<ul class="defaultStyle">
					<li>
                        <asp:Panel ID="pnlEducationErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblEducationErr" runat="server" Text=""></asp:Label></asp:Panel>
						<asp:Label ID="msg_OtherEducat" runat="server" Text="" CssClass="peLabel floatLeft" AssociatedControlID="cbEducation"/>
						<div class="form_right floatLeft">
                            <dx:ASPxComboBox ID="cbEducation" runat="server" Width="310px" Font-Size="14px" 
                                Height="18px" EncodeHtml="False">
                            </dx:ASPxComboBox>	
                        </div>
					</li>
					<li runat="server" id="liAnnualIncome">
                        <asp:Panel ID="pnlIncomeErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblIncomeErr" runat="server" Text=""></asp:Label></asp:Panel>
						<asp:Label ID="msg_OtherAnnual" runat="server" Text="" CssClass="peLabel floatLeft" AssociatedControlID="cbIncome"/>
						<div class="form_right floatLeft">
                            <dx:ASPxComboBox ID="cbIncome" runat="server" Width="310px" Font-Size="14px" 
                                Height="18px" EncodeHtml="False">
                            </dx:ASPxComboBox>	
						</div>
					</li>
					<%--
                    <li runat="server" id="liNetWorth" visible="false">
                        <asp:Panel ID="pnlNetWorthErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblNetWorthErr" runat="server" Text=""></asp:Label></asp:Panel>
						<asp:Label ID="msg_OtherNetWorth" runat="server" Text="" CssClass="peLabel floatLeft" AssociatedControlID="cbNetWorth"/>
						<div class="form_right floatLeft">
                            <dx:ASPxComboBox ID="cbNetWorth" runat="server" Width="310px" Font-Size="14px" 
                                Height="18px" EncodeHtml="False">
                            </dx:ASPxComboBox>	
						</div>
					</li>
                    --%>
					<li>
                        <asp:Panel ID="pnlOccupationErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblOccupationErr" runat="server" Text=""></asp:Label></asp:Panel>
						<asp:Label ID="msg_OtherOccupation" runat="server" Text="" CssClass="peLabel floatLeft" AssociatedControlID="txtOccupation"/>
						<div class="form_right floatLeft">
                            <dx:ASPxTextBox ID="txtOccupation" runat="server" CssClass="text" 
                                                        Text="Employee at a multi"/>
						</div>
					</li>
				</ul>
			<div class="clear"></div>
			</div>
		</div>
	</div>
	<div class="padding"></div><div class="pe_wrapper">
		<div class="pe_box">
			<h2 class="collapsibleHeader" title="<%= CurrentPageData.GetCustomString("msg_ClickToCollapse") %>"><a href="javascript:void(0);" class="togglebutton">-</a> <asp:Literal ID="msg_PersonalTitle" runat="server"/></h2>
			<div class="pe_form">
				<ul class="defaultStyle">
					<li>
                        <asp:Panel ID="pnlHeightErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblHeightErr" runat="server" Text=""></asp:Label></asp:Panel>
						<asp:Label ID="msg_PersonalHeight" runat="server" Text="" CssClass="peLabel floatLeft" AssociatedControlID="cbHeight"/>
						<div class="form_right floatLeft">
                            <dx:ASPxComboBox ID="cbHeight" runat="server" EncodeHtml="False" Width="310px" Font-Size="14px" Height="18px">
                            </dx:ASPxComboBox>	
						</div>
					</li>
					<li>
                        <asp:Panel ID="pnlBodyTypeErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblBodyTypeErr" runat="server" Text=""></asp:Label></asp:Panel>
						<asp:Label ID="msg_PersonalBodyType" runat="server" Text="" CssClass="peLabel floatLeft" AssociatedControlID="cbBodyType"/>
						<div class="form_right floatLeft">
                            <dx:ASPxComboBox ID="cbBodyType" runat="server" Width="310px" Font-Size="14px" 
                                Height="18px" EncodeHtml="False">
                            </dx:ASPxComboBox>	
						</div>
					</li>
					<li>
                        <asp:Panel ID="pnlEyeColorErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblEyeColorErr" runat="server" Text=""></asp:Label></asp:Panel>
						<asp:Label ID="msg_PersonalEyeClr" runat="server" Text="" CssClass="peLabel floatLeft" AssociatedControlID="cbEyeColor"/>
						<div class="form_right floatLeft">
                            <dx:ASPxComboBox ID="cbEyeColor" runat="server" Width="310px" Font-Size="14px" 
                                Height="18px" EncodeHtml="False">
                            </dx:ASPxComboBox>	
						</div>
					</li>
					<li>
                        <asp:Panel ID="pnlHairClrErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblHairClrErr" runat="server" Text=""></asp:Label></asp:Panel>
						<asp:Label ID="msg_PersonalHairClr" runat="server" Text="" CssClass="peLabel floatLeft" AssociatedControlID="cbHairClr"/>
						<div class="form_right floatLeft">
                            <dx:ASPxComboBox ID="cbHairClr" runat="server" Width="310px" Font-Size="14px" 
                                Height="18px" EncodeHtml="False">
                            </dx:ASPxComboBox>	
						</div>
					</li>
					<li>
                        <asp:Panel ID="pnlChildrenErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblChildrenErr" runat="server" Text=""></asp:Label></asp:Panel>
						<asp:Label ID="msg_PersonalChildren" runat="server" Text="" CssClass="peLabel floatLeft" AssociatedControlID="cbChildren"/>
						<div class="form_right floatLeft">
                            <dx:ASPxComboBox ID="cbChildren" runat="server" Width="310px" Font-Size="14px" 
                                Height="18px" EncodeHtml="False">
                            </dx:ASPxComboBox>	
						</div>
					</li>
					<li>
                        <asp:Panel ID="pnlEthnicityErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblEthnicityErr" runat="server" Text=""></asp:Label></asp:Panel>
						<asp:Label ID="msg_PersonalEthnicity" runat="server" Text="" CssClass="peLabel floatLeft" AssociatedControlID="cbEthnicity"/>
						<div class="form_right floatLeft">
                            <dx:ASPxComboBox ID="cbEthnicity" runat="server" Width="310px" Font-Size="14px" 
                                Height="18px" EncodeHtml="False">
                            </dx:ASPxComboBox>	
						</div>
					</li>
					<li>
                        <asp:Panel ID="pnlReligionErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblReligionErr" runat="server" Text=""></asp:Label></asp:Panel>
						<asp:Label ID="msg_PersonalReligion" runat="server" Text="" CssClass="peLabel floatLeft" AssociatedControlID="cbReligion"/>
						<div class="form_right floatLeft">
                            <dx:ASPxComboBox ID="cbReligion" runat="server" Width="310px" Font-Size="14px" 
                                Height="18px" EncodeHtml="False">
                            </dx:ASPxComboBox>	
						</div>
					</li>
					<li>
                        <asp:Panel ID="pnlSmokingErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblSmokingErr" runat="server" Text=""></asp:Label></asp:Panel>
						<asp:Label ID="msg_PersonalSmokingHabit" runat="server" Text="" CssClass="peLabel floatLeft" AssociatedControlID="cbSmoking"/>
						<div class="form_right floatLeft">
                            <dx:ASPxComboBox ID="cbSmoking" runat="server" Width="310px" Font-Size="14px" 
                                Height="18px" EncodeHtml="False">
                            </dx:ASPxComboBox>	
						</div>
					</li>
					<li>
                        <asp:Panel ID="pnlDrinkingErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblDrinkingErr" runat="server" Text=""></asp:Label></asp:Panel>
						<asp:Label ID="msg_PersonalDrinkingHabit" runat="server" Text="" CssClass="peLabel floatLeft" AssociatedControlID="cbDrinking"/>
						<div class="form_right floatLeft">
                            <dx:ASPxComboBox ID="cbDrinking" runat="server" Width="310px" Font-Size="14px" 
                                Height="18px" EncodeHtml="False">
                            </dx:ASPxComboBox>	
						</div>
					</li>
				</ul>
			<div class="clear"></div>
			</div>
		</div>
	</div>


	<div class="padding"></div><div class="pe_wrapper">
		<div class="pe_box">
			<h2 class="collapsibleHeader" title="<%= CurrentPageData.GetCustomString("msg_ClickToCollapse") %>"><a href="javascript:void(0);" class="togglebutton">-</a> <asp:Literal ID="msg_WAYLFTitle" runat="server"/></h2>
			<div class="pe_form">
				<ul class="defaultStyle">
					<li>
                        <asp:Panel ID="pnlErrorSelectLookingForMaleFemale" runat="server" CssClass="alert alert-danger" Visible="False">
                            <dx:ASPxLabel ID="lblErrorSelectLookingForMaleFemale" runat="server" ForeColor="red" Font-Size="14px" >
                            </dx:ASPxLabel>
                        </asp:Panel>
					
						<asp:Label ID="msg_WAYLFMeet" runat="server" Text="" CssClass="peLabel floatLeft" AssociatedControlID="chkLookingMale" Visible="false"/>
						<div class="form_right floatLeft">
							<p class="form_cluster">
                                <dx:ASPxCheckBox ID="chkLookingMale" runat="server" Text="Male"  EncodeHtml="False" Visible="false">
                                </dx:ASPxCheckBox>
							</p>
							<p class="form_cluster">
                                <dx:ASPxCheckBox ID="chkLookingFemale" runat="server" Text="Female" EncodeHtml="False" Visible="false">
                                </dx:ASPxCheckBox>
							</p>
						</div>
					</li>
					<li>
                        <asp:Panel ID="pnlRelationshipErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblRelationshipErr" runat="server" Text=""></asp:Label></asp:Panel>
						<asp:Label ID="msg_WAYLFRelStatus" runat="server" Text="" CssClass="peLabel floatLeft" AssociatedControlID="cbRelationshipStatus"/>
						<div class="form_right floatLeft">
                            <dx:ASPxComboBox ID="cbRelationshipStatus" runat="server" Width="310px" 
                                Font-Size="14px" Height="18px" EncodeHtml="False">
                            </dx:ASPxComboBox>	
						</div>
					</li>
					<li>
                        <asp:Panel ID="pnlTypeOfDateErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblTypeOfDateErr" runat="server" Text=""></asp:Label></asp:Panel>
						<asp:Label ID="msg_WAYLFTypeDate" runat="server" Text="" CssClass="peLabel floatLeft" AssociatedControlID="lvTypeOfDate"/>
						<div class="form_right floatLeft">
                            <asp:ListView ID="lvTypeOfDate" runat="server">
                                <LayoutTemplate>
                                <p id="itemPlaceholder" runat="server"></p>
                                </LayoutTemplate>
                                <ItemTemplate>
                                <p class="form_cluster">
                                    <asp:HiddenField ID="hdf" runat="server" Visible="false" Value='<%# Eval("TypeOfDatingId") %>' />
                                    <dx:ASPxCheckBox ID="chk" runat="server" Checked='<%# Eval("TypeOfDateChecked") %>' Text='<%# Eval("TypeOfDateText") %>' EncodeHtml="False"></dx:ASPxCheckBox>
								</p>
                                </ItemTemplate>
                            </asp:ListView>
						</div>
					</li>
				</ul>
			<div class="clear"></div>
			</div>
		</div>
	</div>

    <div class="padding"></div>
    <%--<div class="pe_wrapper">
		<div class="pe_box">
			<h2 class="collapsibleHeader" title="<%= CurrentPageData.GetCustomString("msg_ClickToCollapse") %>"><a href="javascript:void(0);" class="togglebutton">-</a> <asp:Literal ID="msg_PrivacySettingsTitle" runat="server"/></h2>
			<div class="pe_form">
				<ul class="defaultStyle">
					<li><p style="font-size:0; line-height:0; margin:0; padding:0; text-indent:-99999px;">&nbsp;</p>
						<label class="peLabel floatLeft">&nbsp;</label>
						<div class="form_right floatLeft">
							 
								<p class="form_cluster">
                                    <dx:ASPxCheckBox ID="chkPS_HideMeFromSearchResults" runat="server">
                                    </dx:ASPxCheckBox>
								</p>
							 
								<p class="form_cluster">
                                    <dx:ASPxCheckBox ID="chkPS_HideMeFromMembersIHaveBlocked" runat="server">
                                    </dx:ASPxCheckBox>
								</p>
							 
								<p class="form_cluster">
                                    <dx:ASPxCheckBox ID="chkPS_NotShowInOtherUsersFavoritedList" runat="server">
                                    </dx:ASPxCheckBox>
								</p>
							 
								<p class="form_cluster">
                                    <dx:ASPxCheckBox ID="chkPS_NotShowInOtherUsersViewedList" runat="server">
                                    </dx:ASPxCheckBox>
								</p>
							 
								<p class="form_cluster">
                                    <dx:ASPxCheckBox ID="chkPS_NotShowMyGifts" runat="server">
                                    </dx:ASPxCheckBox>
								</p>
							 
								<p class="form_cluster">
                                    <dx:ASPxCheckBox ID="chkPS_HideMyLastLoginDateTime" runat="server">
                                    </dx:ASPxCheckBox>
								</p>
							</div>
					</li>
				</ul>
			<div class="clear"></div>
			</div>
		</div>
	</div>--%>

	<div class="clearboth padding"></div>
	<div class="form-actions">
        <dx:ASPxButton ID="btnProfileUpd" runat="server" 
            CssClass="btn btn-large btn-primary" Text="" 
            ValidationGroup="RegisterGroup" Native="True" EncodeHtml="False" 
            UseSubmitBehavior="False"  />
	</div>
    <script type="text/javascript">

        //try {
//jQuery(document).ready(function () {
//    setTimeout(function () {
//        jQuery('html').scrollTop(0);
//        jQuery('body').scrollTop(0);
//        jQuery(document).scrollTop(0);
//        jQuery(window).scrollTop(0);
//        //alert(jQuery(document).scrollTop() + "," + jQuery(window).scrollTop())
//    }, 100);
//});
        //}
        //catch (e) {alert(e) }
    </script>
	<!-- End Form Box Wrapper -->

</div>

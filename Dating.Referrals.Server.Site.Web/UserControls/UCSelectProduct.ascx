﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UCSelectProduct.ascx.vb"
    Inherits="Dating.Referrals.Server.Site.Web.UCSelectProduct" %>

<%@ Register src="UCPrice.ascx" tagname="UCPrice" tagprefix="uc1" %>

<style>
    .ordbut { position: absolute; bottom: 27px; right: 25%; }
    .ordbut2 { position: absolute; bottom: 27px; right: 29%; }
    .pricecol { position: relative;font-family:"Segoe UI","Arial"; }
    .pricecol-credits { text-align:center;font-size:42px;line-height:42px;padding-top:20px;}
    .pricecol-credits-text { text-align:center;font-size:22px;line-height:25px;}
    .pricecol-price {position:absolute;top:149px;left:0px;right:0px; }
    .pricecol-price-text {text-align:center;font-size:56px;line-height:42px;color:#39B54A; }
    .pricecol-expire {position:absolute;top:200px;left:0px;right:0px; }
    .pricecol-expire-text {text-align:center;font-size:14px;}
    .pricecol-price.small {top:159px; }
    .pricecol-price.small .pricecol-price-text{font-size:36px;line-height:36px; }
    .pricecol-expire.small .pricecol-expire-text{}
</style>
<table style="width:100%">
<tr>
    <td style="width:20px">&nbsp;</td>
    <td>
<table cellpadding="5" cellspacing="5" style="width:640px">
                <tr>
                    <td>

<div>
    <asp:Label ID="lblPaymentsGuarantee" runat="server" Text=""></asp:Label>
</div>
<div class="center" style="text-align:center;display:none;">
    <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/products/100.png" />
    <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/products/alert-pay.png" />
    <asp:Image ID="Image3" runat="server" ImageUrl="~/Images/products/paypal.png" />
    <asp:Image ID="Image4" runat="server" ImageUrl="~/Images/products/paypal-verified.png" />
    <asp:Image ID="Image5" runat="server" ImageUrl="~/Images/products/paysafe.png" />
</div>
                    </td>
                </tr>
                <tr>
                    <td style="height: 75px;" valign="top">
                        <dx:ASPxLabel ID="PanelSelectProduct" runat="server" Text="" EncodeHtml="False">
                        </dx:ASPxLabel>
                    </td>
                </tr>
                <tr>
                    <td align="center" style="background-image: url('../Images/paymentnew/bg2.png');
                        background-repeat: no-repeat; background-position: center;">
                        <div id="pricesSub" align="center" style="margin: 0 auto;">
                            <table cellspacing="0" cellpadding="0" style="">
                                <tr>
                                    <td height="338px" width="251px">
                                        <uc1:UCPrice ID="UCPrice1" runat="server" View="vwPublic" />
                                                <%-- <div class="pricecol" style="background-image: url('../Images2/pricing/1000credits.png'); background-repeat: no-repeat;width:251px;height:338px;" 
                                                    align="center">
                                                &nbsp;
                                                    <asp:ImageButton ID="img1000Credits" runat="server" align="center"
                                                        ImageUrl="~/Images2/pricing/order-button.png" CssClass="ordbut" 
                                                        PostBackUrl="~/Register.aspx" />
                                                </div>--%>
                                    </td>
                                    <td style="width:20px;">
                                    &nbsp;
                                    </td>
                                    <td height="338px" width="251px">
                                        <uc1:UCPrice ID="UCPrice2" runat="server" View="vwPublic" />
                                                <%--<div class="pricecol" style="background-image: url('../Images2/pricing/3000credits.png'); background-repeat: no-repeat;width:251px;height:338px;" 
                                                    align="center">
                                                &nbsp;
                                                    <asp:ImageButton ID="img3000Credits" runat="server" align="center"
                                                        ImageUrl="~/Images2/pricing/order-button.png" CssClass="ordbut" 
                                                        PostBackUrl="~/Register.aspx" />
                                                </div>--%>
                                    </td>
                                    <td style="width:20px;">
                                    &nbsp;
                                    </td>
                                    <td height="338px" width="251px">
                                        <uc1:UCPrice ID="UCPrice3" runat="server" View="vwPublic" />
                                                <%--<div class="pricecol" style="background-image: url('../Images2/pricing/6000credits.png'); background-repeat: no-repeat;width:287px;height:338px;" 
                                                    align="center">
                                                &nbsp;
                                                    <asp:ImageButton ID="img6000Credits" runat="server" align="center"
                                                        ImageUrl="~/Images2/pricing/order-button.png" CssClass="ordbut2" 
                                                        PostBackUrl="~/Register.aspx" />
                                                </div>--%>
                                    </td>
                                </tr>
                            </table>
                            <br />
                            <table cellspacing="0" cellpadding="0" style="display:none">
                                <tr>
                                    <td height="59px">
                                                <div style="background-image: url('../Images/prices2/prices2_04.png'); background-repeat: no-repeat;width:170px;height:247px;">
                                                &nbsp;
                                                </div>
                                                <div style="margin-top:-4px;margin-left:0;" class="lfloat">
                                                    <asp:ImageButton ID="img10000Credits" runat="server" 
                                                        CommandArgument="10000Credits,365,90" align="center"
                                                        ImageUrl="~/Images/prices2/prices2_10.png" />
                                                </div>
                                    </td>
                                    <td style="width:20px;">
                                    &nbsp;
                                    </td>
                                    <td height="59px">
                                                <div style="background-image: url('../Images/prices2/prices2_05.png'); background-repeat: no-repeat;width:173px;height:247px;">
                                                &nbsp;
                                                </div>
                                                <div style="margin-top:-4px;margin-left:0;" class="lfloat">
                                                    <asp:ImageButton ID="img30000Credits" runat="server" 
                                                        CommandArgument="30000Credits,548,240" align="center"
                                                        ImageUrl="~/Images/prices2/prices2_11.png" />
                                                </div>
                                    </td>
                                    <td style="width:20px;">
                                    &nbsp;
                                    </td>
                                    <td height="59px">
                                                <div style="background-image: url('../Images/prices2/prices2_06.png'); background-repeat: no-repeat;width:175px;height:247px;">
                                                &nbsp;
                                                </div>
                                                <div style="margin-top:-4px;margin-left:0;" class="lfloat">
                                                    <asp:ImageButton ID="img50000Credits" runat="server" 
                                                        CommandArgument="50000Credits,730,360" align="center"
                                                        ImageUrl="~/Images/prices2/prices2_12.png" />
                                                </div>
                                    </td>
                                </tr>
                            </table>
                            <div class="clear"></div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
<dx:ASPxLabel runat="server" EncodeHtml="False" ClientIDMode="AutoID" ID="lbfootText">
</dx:ASPxLabel>
                    </td>
                </tr>
            </table>
    </td>
    <td style="width:20px">&nbsp;</td>
</tr>
</table>

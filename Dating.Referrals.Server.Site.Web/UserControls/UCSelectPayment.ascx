﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UCSelectPayment.ascx.vb" Inherits="Dating.Referrals.Server.Site.Web.UCSelectPayment" %>
<%@ Register assembly="DevExpress.Web.v13.1, Version=13.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="SpiceLogicPayPalStd" namespace="SpiceLogic.PayPalCtrlForWPS.Controls" tagprefix="cc1" %>
<%--<%@ Register assembly="SpiceLogicPayPalStd" namespace="SpiceLogicPayPalStandard.Controls.BuyNowButton" tagprefix="cc1" %>--%>

<%@ Register assembly="DevExpress.Web.v13.1, Version=13.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxPanel" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v13.1, Version=13.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxPopupControl" tagprefix="dx" %>

<%@ Register assembly="DevExpress.Web.v13.1, Version=13.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxRoundPanel" tagprefix="dx" %>



    <table cellpadding="5" cellspacing="5" class="headerTable">
        <tr>
            <td>
                <div style="padding:20px;">
                    <div style="padding:10px;" class="blueBox"><dx:ASPxLabel ID="lblPaymentDescription" runat="server" Text="lblPaymentDescription" EncodeHtml="False"/></div>
                    <%--<div style="padding:10px;"><dx:ASPxLabel ID="lbPaymentExtraInfo" runat="server" Text="lbPaymentExtraInfo" EncodeHtml="False"/></div>--%>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <table cellpadding="5" cellspacing="5" class="style1">
                    <%--<tr>
                        <td  colspan="2" style="background-color: #138FEF">
                            <dx:ASPxLabel ID="lbProductInfo" runat="server"  EncodeHtml="false" /></td>
                    </tr>
                    <tr>
                        <td class="style3" colspan="2" valign="top" >
                            <dx:ASPxLabel ID="lbPaymentHeader" runat="server" ClientIDMode="AutoID" 
                                EncodeHtml="False">
                            </dx:ASPxLabel>
                        </td>
                    </tr>--%>

                    <tr runat="server" visible="false ">
                        <td class="style3" valign="top" >
                            Promo Code (if available):
                        </td> 
                        <td class="style1" valign="top" >
                            <dx:ASPxTextBox ID="txPromoCode"  runat="server" Width="170px">
                            </dx:ASPxTextBox>
                        </td>
                    </tr>

                    <tr runat="server" id="trNoPayment" visible="false">
                        <td class="style3" valign="top">
                            <%--<asp:ImageButton ID="img" runat="server" CssClass="style4" 
                                ImageUrl="/Images/logo_256x58.png" style="outline: none;background-color:#909090;" />--%>
<asp:Panel ID="pntTest" runat="server" Visible="False">
    <table>
    <tr>
        <td><dx:ASPxButton ID="btnPayTestUser" runat="server" Text="Payment for test #USER#" Font-Bold="True">
        </dx:ASPxButton></td>
    </tr>
    <tr>
        <td><dx:ASPxMemo ID="txtOutput" runat="server" Height="71px" Width="170px">
        </dx:ASPxMemo></td>
    </tr>
    <tr>
        <td><asp:HyperLink ID="lnkOK" runat="server" NavigateUrl="~/Members/paymentok.aspx">Go to OK page</asp:HyperLink>&nbsp;
        <asp:HyperLink ID="lnkFail" runat="server" 
            NavigateUrl="~/Members/paymentfail.aspx">Go to Fail page</asp:HyperLink></td>
    </tr>
    </table>
        
        
    </asp:Panel>
                        </td>
                        <td class="style1" valign="top" >
                            <dx:ASPxLabel ID="ASPxLabel1" runat="server" ClientIDMode="AutoID"
                                EncodeHtml="False" Text="Test payment!!!">
                            </dx:ASPxLabel></td>
                    </tr>
                    <tr runat="server" id="trBuyNowButton" visible="false">
                        <td class="style3" valign="top" >
                            <cc1:BuyNowButton ID="BuyNowButton1" runat="server" 
                                BusinessEmailOrMerchantID="sales@goomena.com" CurrencyCode="Euro" 
                                ImageUrl="/Images/paypal.png" PostClickBehavior="DEFAULT" 
                                ZIgnore="22/5/2012 4:11:27 μμ" Height="60px" Width="154px">
                                <PayPalReturn Custom_CancelledReturnURL="http://www.goomena.com/members/paymentfail.aspx" 
                                    Custom_CompletedReturnURL="http://www.goomena.com/members/paymentok.aspx" />
                            </cc1:BuyNowButton>
                        </td>
                        <td class="style1" valign="top" >
                            <dx:ASPxLabel ID="lblPaymentWithPaypal" runat="server" ClientIDMode="AutoID" 
                                EncodeHtml="False">
                            </dx:ASPxLabel></td>
                    </tr>
                    <tr runat="server" id="trPaymentWall" visible="false">
                        <td colspan="2">
	            <div style="background-color:#fff">
    <style type="text/css">
        .iframePayment {
            height: 500px;
            width: 640px;
            border:0;
            border-top:solid 2px #f0f0f0;
        }
    </style>
    <iframe src="" runat="server" id="iframePayment" frameborder="0" class="iframePayment"></iframe>
                </div>
                        </td>
                    </tr>
                    <tr id="TrPayPal" runat="server" visible="false">
                        <td class="style3">
                            <asp:ImageButton ID="btnPayPalDir" runat="server" CssClass="style4" 
                                ImageUrl="/Images/paypal.png" style="outline: none;" />
                           <asp:ImageButton ID="btnPaypalPW" runat="server" CssClass="style4" 
                                ImageUrl="/Images/paypal.png" style="outline: none;" Visible="False" />
                        </td>
                        <td class="style1">
                            <dx:ASPxLabel ID="lbPaypalHeadline" runat="server" ClientIDMode="AutoID" 
                                EncodeHtml="False" Text="Λογαριασμός PayPal">
                            </dx:ASPxLabel>&nbsp;<img id="PaypalTIP" 
                        alt="" src="/Images/icon_tip16.png" />
                            <br />

                        </td>
                    </tr>

                    <tr id="Tr3" runat="server" visible="True">
                        <td class="style3">
                            <img src="/Images/paysafe.png" style="outline: none; display:none;"  onclick="alert('paysafecard is still under test mode. Please choose alternative payment mode.')" onmouseover="this.style.cursor = 'pointer';" />
                            <asp:ImageButton ID="btnPaySafe" runat="server"
                                ImageUrl="~/Images/paysafe.png" style="outline: none;"  />
                        </td>
                        <td class="style1">
                            <dx:ASPxLabel ID="lbPaySafeHeadline" runat="server" ClientIDMode="AutoID" 
                                EncodeHtml="False">
                            </dx:ASPxLabel>&nbsp;<img id="PaysafeTIP" 
                        alt="" src="/Images/icon_tip16.png" />
                        </td>
                    </tr>

                    <tr id="Tr1CreditCard" runat="server" visible="true">
                        <td class="style3">
                            <asp:ImageButton ID="btnCreditCardsDir" runat="server" CssClass="style4" 
                                ImageUrl="/Images/creditCards.png" style="outline: none;" Visible="False" />
                            <asp:ImageButton ID="btnCreditCards" runat="server" CssClass="style4" 
                                ImageUrl="/Images/creditCards.png" style="outline: none;" 
                                />
                        </td>
                        <td class="style1">
                            <dx:ASPxLabel ID="lbCreditCardsHeadline" runat="server" ClientIDMode="AutoID"
                                EncodeHtml="False">
                            </dx:ASPxLabel>&nbsp;<img id="CreditCardsTIP" 
                        alt="" src="/Images/icon_tip16.png" /></td>
                    </tr>

                    <tr id="TrAlertPay" runat="server" visible="false">
                        <td class="style3">
                            <asp:ImageButton ID="btnAlertPay" runat="server" CssClass="style4" 
                                ImageUrl="/Images/payza.png" style="outline: none;" />
                        </td>
                        <td class="style1">
                            <dx:ASPxLabel ID="lbAlertPayHeadline" runat="server" ClientIDMode="AutoID"
                                EncodeHtml="False">
                            </dx:ASPxLabel>&nbsp;<img id="PayZaTIP" 
                        alt="" src="/Images/icon_tip16.png" /></td>
                    </tr>
                    <tr id="Tr4" runat="server" visible="false">
                        <td class="style3">
                            <asp:ImageButton ID="btnPayByPhone" runat="server" 
                                ImageUrl="~/Images/payByPhone.png" style="outline: none;" />
                        </td>
                        <td class="style1">
                            <dx:ASPxLabel ID="lbPayByPhoneHeadline" runat="server" ClientIDMode="AutoID" 
                                EncodeHtml="False">
                            </dx:ASPxLabel>&nbsp;<img id="PayByPhoneTIP" 
                        alt="" src="/Images/icon_tip16.png" /></td>
                    </tr>
                   
                    <tr id="Tr1" runat="server" visible="false">
                        <td class="style3" style="padding-top: 10px">
                            <asp:ImageButton ID="btnPaymentWall" runat="server" CssClass="style4" 
                                ImageUrl="/Images/paymentwall2.png" style="outline: none;"  />
                        </td>
                        <td class="style1">
                            <dx:ASPxLabel ID="lnkPaymentWall" runat="server" ClientIDMode="AutoID" 
                                EncodeHtml="False">
                            </dx:ASPxLabel>&nbsp;<img id="PaymentWallTIP" 
                        alt="" src="/Images/icon_tip16.png"  /></td>
                    </tr>
                    
                    <tr id="Tr2Other" runat="server" visible="false">
                        <td class="style3" style="padding-top: 10px">
                            <asp:ImageButton ID="btnOther" runat="server" CssClass="style4" 
                                ImageUrl="/Images/others.png" style="outline: none;"  />
                        </td>
                        <td class="style1">
                            <dx:ASPxLabel ID="lblOthersHeadline" runat="server" ClientIDMode="AutoID" 
                                EncodeHtml="False">
                            </dx:ASPxLabel>&nbsp;<img id="OthersTIP" 
                        alt="" src="/Images/icon_tip16.png"  /></td>
                    </tr>
                    <tr>
                        <td class="style3">&nbsp;
                           
                        </td>
                        <td class="style1">&nbsp;
                           </td>
                    </tr>
                    <tr>
                        <td class="style3" colspan="2">
                            <dx:ASPxLabel ID="lbPaymentExtraInfo" runat="server" ClientIDMode="AutoID" 
                                EncodeHtml="False">
                            </dx:ASPxLabel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <dx:ASPxRoundPanel ID="PanelSelectPayment" runat="server" ClientIDMode="AutoID" 
        CssFilePath="~/App_Themes/Office2010Silver/{0}/styles.css" 
        CssPostfix="Office2010Silver" EnableDefaultAppearance="False" 
        GroupBoxCaptionOffsetX="6px" GroupBoxCaptionOffsetY="-19px" 
        SpriteCssFilePath="~/App_Themes/Office2010Silver/{0}/sprite.css" 
    Width="900px" HeaderText="ΕΠΙΛΕΞΤΕ ΤΟΝ ΤΡΟΠΟ ΠΛΗΡΩΜΗΣ ΠΟΥ ΕΠΙΘΥΜΕΙΤΕ" 
    EncodeHtml="False" Visible="False">
        <ContentPaddings PaddingBottom="10px" PaddingLeft="9px" PaddingRight="11px" 
            PaddingTop="10px" />
        <HeaderStyle Font-Bold="True" Font-Size="Medium">
        <Paddings PaddingBottom="6px" PaddingLeft="9px" PaddingRight="11px" 
            PaddingTop="3px" />
        </HeaderStyle>
        <PanelCollection>
<dx:PanelContent ID="PanelContent1" runat="server" SupportsDisabledAttribute="True">
            </dx:PanelContent>
</PanelCollection>
    </dx:ASPxRoundPanel>
<%--
    CssFilePath="~/App_Themes/Office2010Blue/{0}/styles.css" 
    CssPostfix="Office2010Blue" 
    SpriteCssFilePath="~/App_Themes/Office2010Blue/{0}/sprite.css"
--%>
     <dx:ASPxPopupControl runat="server" ShowOnPageLoad="True" Width="600px" 
                    ID="ASPxPopupControl2" ClientIDMode="AutoID" 
    EnableHotTrack="False" >
    <ContentStyle HorizontalAlign="Left" VerticalAlign="Middle">
        <backgroundimage imageurl="../images/bg.jpg" />
        <Paddings Padding="10px" />
<Paddings Padding="10px"></Paddings>

<BackgroundImage ImageUrl="../images/bg.jpg"></BackgroundImage>
    </ContentStyle>
         <Windows>
             <dx:PopupWindow PopupAction="MouseOver" Name="Win_PaypalTIP" CloseAction="MouseOut" HeaderText="Buy credits using PayPal"
                 PopupElementID="PaypalTIP">
                 <CloseButtonStyle>
                     <HoverStyle>
                         <BackgroundImage HorizontalPosition="center" ImageUrl="~/Images/close-button1.png"
                             Repeat="NoRepeat" VerticalPosition="center" />
                         <BackgroundImage ImageUrl="~/Images/close-button1.png" Repeat="NoRepeat" HorizontalPosition="center"
                             VerticalPosition="center"></BackgroundImage>
                     </HoverStyle>
                     <BackgroundImage HorizontalPosition="center" ImageUrl="~/Images/close-button2.png"
                         Repeat="NoRepeat" VerticalPosition="center" />
                     <BackgroundImage ImageUrl="~/Images/close-button2.png" Repeat="NoRepeat" HorizontalPosition="center"
                         VerticalPosition="center"></BackgroundImage>
                 </CloseButtonStyle>
                 <HeaderStyle Font-Bold="True" Font-Size="Medium" />
                 <ContentCollection>
                     <dx:PopupControlContentControl ID="Popupcontrolcontentcontrol6" runat="server">
                         <dx:ASPxLabel ID="lbPaypalMore" runat="server" Text="" EncodeHtml="false">
                         </dx:ASPxLabel>
                     </dx:PopupControlContentControl>
                 </ContentCollection>
             </dx:PopupWindow>
             <dx:PopupWindow PopupAction="MouseOver" Name="Win_PaysafeTIP" CloseAction="MouseOut" HeaderText="Buy credits using paysafecard"
                 PopupElementID="PaysafeTIP">
                 <CloseButtonStyle>
                     <HoverStyle>
                         <BackgroundImage HorizontalPosition="center" ImageUrl="~/Images/close-button1.png"
                             Repeat="NoRepeat" VerticalPosition="center" />
                         <BackgroundImage ImageUrl="~/Images/close-button1.png" Repeat="NoRepeat" HorizontalPosition="center"
                             VerticalPosition="center"></BackgroundImage>
                     </HoverStyle>
                     <BackgroundImage HorizontalPosition="center" ImageUrl="~/Images/close-button2.png"
                         Repeat="NoRepeat" VerticalPosition="center" />
                     <BackgroundImage ImageUrl="~/Images/close-button2.png" Repeat="NoRepeat" HorizontalPosition="center"
                         VerticalPosition="center"></BackgroundImage>
                 </CloseButtonStyle>
                 <HeaderStyle Font-Bold="True" Font-Size="Medium" />
                 <ContentCollection>
                     <dx:PopupControlContentControl ID="Popupcontrolcontentcontrol2" runat="server">
                         <dx:ASPxLabel ID="lbPaysafeMore" runat="server" Text="" EncodeHtml="false">
                         </dx:ASPxLabel>
                     </dx:PopupControlContentControl>
                 </ContentCollection>
             </dx:PopupWindow>
             <dx:PopupWindow PopupAction="MouseOver" Name="Win_PayByPhoneTIP" CloseAction="MouseOut" HeaderText="Buy credits using Your Phone"
                 PopupElementID="PayByPhoneTIP">
                 <CloseButtonStyle>
                     <HoverStyle>
                         <BackgroundImage HorizontalPosition="center" ImageUrl="~/Images/close-button1.png"
                             Repeat="NoRepeat" VerticalPosition="center" />
                         <BackgroundImage ImageUrl="~/Images/close-button1.png" Repeat="NoRepeat" HorizontalPosition="center"
                             VerticalPosition="center"></BackgroundImage>
                     </HoverStyle>
                     <BackgroundImage HorizontalPosition="center" ImageUrl="~/Images/close-button2.png"
                         Repeat="NoRepeat" VerticalPosition="center" />
                     <BackgroundImage ImageUrl="~/Images/close-button2.png" Repeat="NoRepeat" HorizontalPosition="center"
                         VerticalPosition="center"></BackgroundImage>
                 </CloseButtonStyle>
                 <HeaderStyle Font-Bold="True" Font-Size="Medium" />
                 <ContentCollection>
                     <dx:PopupControlContentControl ID="Popupcontrolcontentcontrol3" runat="server">
                         <dx:ASPxLabel ID="lbPayByPhoneMore" runat="server" Text="" EncodeHtml="false">
                         </dx:ASPxLabel>
                     </dx:PopupControlContentControl>
                 </ContentCollection>
             </dx:PopupWindow>
             <dx:PopupWindow PopupAction="MouseOver" Name="Win_MoneyBookersTIP" CloseAction="MouseOut" HeaderText="Buy credits using MoneyBookers"
                 PopupElementID="MoneyBookersTIP">
                 <CloseButtonStyle>
                     <HoverStyle>
                         <BackgroundImage HorizontalPosition="center" ImageUrl="~/Images/close-button1.png"
                             Repeat="NoRepeat" VerticalPosition="center" />
                         <BackgroundImage ImageUrl="~/Images/close-button1.png" Repeat="NoRepeat" HorizontalPosition="center"
                             VerticalPosition="center"></BackgroundImage>
                     </HoverStyle>
                     <BackgroundImage HorizontalPosition="center" ImageUrl="~/Images/close-button2.png"
                         Repeat="NoRepeat" VerticalPosition="center" />
                     <BackgroundImage ImageUrl="~/Images/close-button2.png" Repeat="NoRepeat" HorizontalPosition="center"
                         VerticalPosition="center"></BackgroundImage>
                 </CloseButtonStyle>
                 <ContentCollection>
                     <dx:PopupControlContentControl ID="Popupcontrolcontentcontrol4" runat="server">
                         <dx:ASPxLabel ID="lbMoneyBookersMore" runat="server" Text="" EncodeHtml="false">
                         </dx:ASPxLabel>
                     </dx:PopupControlContentControl>
                 </ContentCollection>
             </dx:PopupWindow>
             <dx:PopupWindow PopupAction="MouseOver" Name="Win_PayZaTIP" CloseAction="MouseOut" HeaderText="Buy credits using PayZa"
                 PopupElementID="PayZaTIP">
                 <CloseButtonStyle>
                     <HoverStyle>
                         <BackgroundImage HorizontalPosition="center" ImageUrl="~/Images/close-button1.png"
                             Repeat="NoRepeat" VerticalPosition="center" />
                         <BackgroundImage ImageUrl="~/Images/close-button1.png" Repeat="NoRepeat" HorizontalPosition="center"
                             VerticalPosition="center"></BackgroundImage>
                     </HoverStyle>
                     <BackgroundImage HorizontalPosition="center" ImageUrl="~/Images/close-button2.png"
                         Repeat="NoRepeat" VerticalPosition="center" />
                     <BackgroundImage ImageUrl="~/Images/close-button2.png" Repeat="NoRepeat" HorizontalPosition="center"
                         VerticalPosition="center"></BackgroundImage>
                 </CloseButtonStyle>
                 <HeaderStyle Font-Bold="True" Font-Size="Medium" />
                 <ContentCollection>
                     <dx:PopupControlContentControl ID="Popupcontrolcontentcontrol5" runat="server">
                         <dx:ASPxLabel ID="lbAlertPayMore" runat="server" Text="" EncodeHtml="false">
                         </dx:ASPxLabel>
                     </dx:PopupControlContentControl>
                 </ContentCollection>
             </dx:PopupWindow>
             <dx:PopupWindow PopupAction="MouseOver" Name="Win_CreditCardsTIP" CloseAction="MouseOut" HeaderText="Buy credits using Credit Cards"
                 PopupElementID="CreditCardsTIP">
                 <CloseButtonStyle>
                     <HoverStyle>
                         <BackgroundImage HorizontalPosition="center" ImageUrl="~/Images/close-button1.png"
                             Repeat="NoRepeat" VerticalPosition="center" />
                         <BackgroundImage ImageUrl="~/Images/close-button1.png" Repeat="NoRepeat" HorizontalPosition="center"
                             VerticalPosition="center"></BackgroundImage>
                     </HoverStyle>
                     <BackgroundImage HorizontalPosition="center" ImageUrl="~/Images/close-button2.png"
                         Repeat="NoRepeat" VerticalPosition="center" />
                     <BackgroundImage ImageUrl="~/Images/close-button2.png" Repeat="NoRepeat" HorizontalPosition="center"
                         VerticalPosition="center"></BackgroundImage>
                 </CloseButtonStyle>
                 <HeaderStyle Font-Bold="True" Font-Size="Medium" />
                 <ContentCollection>
                     <dx:PopupControlContentControl ID="Popupcontrolcontentcontrol7" runat="server">
                         <dx:ASPxLabel ID="lbCreditCardsMore" runat="server" Text="" EncodeHtml="false">
                         </dx:ASPxLabel>
                     </dx:PopupControlContentControl>
                 </ContentCollection>
             </dx:PopupWindow>
             <dx:PopupWindow PopupAction="MouseOver" Name="Win_PaymentWallTIP" CloseAction="MouseOut" HeaderText="Buy credits using Payment Wall"
                 PopupElementID="PaymentWallTIP">
                 <CloseButtonStyle>
                     <HoverStyle>
                         <BackgroundImage HorizontalPosition="center" ImageUrl="~/Images/close-button1.png"
                             Repeat="NoRepeat" VerticalPosition="center" />
                         <BackgroundImage ImageUrl="~/Images/close-button1.png" Repeat="NoRepeat" HorizontalPosition="center"
                             VerticalPosition="center"></BackgroundImage>
                     </HoverStyle>
                     <BackgroundImage HorizontalPosition="center" ImageUrl="~/Images/close-button2.png"
                         Repeat="NoRepeat" VerticalPosition="center" />
                     <BackgroundImage ImageUrl="~/Images/close-button2.png" Repeat="NoRepeat" HorizontalPosition="center"
                         VerticalPosition="center"></BackgroundImage>
                 </CloseButtonStyle>
                 <HeaderStyle Font-Bold="True" Font-Size="Medium" />
                 <ContentCollection>
                     <dx:PopupControlContentControl ID="Popupcontrolcontentcontrol8" runat="server">
                         <dx:ASPxLabel ID="lbPaymentWallMore" runat="server" Text="" EncodeHtml="false">
                         </dx:ASPxLabel>
                     </dx:PopupControlContentControl>
                 </ContentCollection>
             </dx:PopupWindow>
             <dx:PopupWindow PopupAction="MouseOver" Name="Win_OthersTIP" CloseAction="MouseOut" HeaderText="Buy credits by other methods"
                 PopupElementID="OthersTIP">
                 <CloseButtonStyle>
                     <HoverStyle>
                         <BackgroundImage HorizontalPosition="center" ImageUrl="~/Images/close-button1.png"
                             Repeat="NoRepeat" VerticalPosition="center" />
                         <BackgroundImage ImageUrl="~/Images/close-button1.png" Repeat="NoRepeat" HorizontalPosition="center"
                             VerticalPosition="center"></BackgroundImage>
                     </HoverStyle>
                     <BackgroundImage HorizontalPosition="center" ImageUrl="~/Images/close-button2.png"
                         Repeat="NoRepeat" VerticalPosition="center" />
                     <BackgroundImage ImageUrl="~/Images/close-button2.png" Repeat="NoRepeat" HorizontalPosition="center"
                         VerticalPosition="center"></BackgroundImage>
                 </CloseButtonStyle>
                 <HeaderStyle Font-Bold="True" Font-Size="Medium" />
                 <ContentCollection>
                     <dx:PopupControlContentControl ID="pccc9" runat="server">
                         <dx:ASPxLabel ID="lbOthersTIP" runat="server" Text="" EncodeHtml="false">
                         </dx:ASPxLabel>
                     </dx:PopupControlContentControl>
                 </ContentCollection>
             </dx:PopupWindow>
         </Windows>

                <CloseButtonImage Url="~/Images/spacer10.png" Height="17px" Width="17px">
                </CloseButtonImage>
                <SizeGripImage Height="40px" Url="~/Images/resize.png" Width="40px">
                </SizeGripImage>
                <ContentStyle>
                </ContentStyle>
                <HeaderStyle BackColor="#35619F" Font-Bold="True" ForeColor="White">
                    <Paddings PaddingBottom="10px" PaddingTop="10px" />
<Paddings PaddingTop="10px" PaddingBottom="10px"></Paddings>
                </HeaderStyle>
                <ModalBackgroundStyle CssClass="modalPopup">
                </ModalBackgroundStyle>

                 <ContentCollection>
                     <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server" SupportsDisabledAttribute="True">
                     </dx:PopupControlContentControl>
                 </ContentCollection>
    
            </dx:ASPxPopupControl>
<%--                 <LoadingPanelImage Url="~/App_Themes/Office2010Blue/Web/Loading.gif">
                 </LoadingPanelImage>
                 <LoadingPanelStyle ImageSpacing="5px">
                 </LoadingPanelStyle>
--%>
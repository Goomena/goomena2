﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/ErrorPages/err.Master" CodeBehind="Unauthorized.aspx.vb" 
Inherits="Dating.Referrals.Server.Site.Web.Unauthorized" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <h2 style="text-shadow: none; border: none; font-family: Lucida Grande,Lucida Sans Unicode,Verdana,Arial,sans-serif;">
        <asp:Label ID="lblHeader" runat="server" Text="401 Unauthorized! " /><span class="loginHereLink"></span></h2>
    <div class="lfloat" style="margin:0 20px">
        <img alt="401 Error" src="../images/oops.png" />
    </div>
    <div class="lfloat">
        <dx:ASPxLabel ID="lbHTMLBody" runat="server" ClientIDMode="AutoID" EncodeHtml="False"
            Text="Sorry, you are not authorized to see this page." Font-Size="14px">
        </dx:ASPxLabel>
    </div>
    <div class="clear">
    </div>
</asp:Content>

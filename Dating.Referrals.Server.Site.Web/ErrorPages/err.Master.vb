﻿Imports Library.Public
Imports DevExpress.Web.ASPxMenu
Imports Dating.Server.Core.DLL
Imports DevExpress.Web.ASPxEditors
Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Datasets.DLL.DSMembers

Public Class _errMaster
    Inherits System.Web.UI.MasterPage


    Dim _pageData As clsPageData
    Protected ReadOnly Property CurrentPageData As clsPageData
        Get
            If (_pageData Is Nothing) Then _pageData = New clsPageData("master", Context)
            Return _pageData
        End Get
    End Property


    Dim _sessVars As clsSessionVariables
    Protected ReadOnly Property SessionVariables As clsSessionVariables
        Get
            If (_sessVars Is Nothing) Then _sessVars = clsSessionVariables.GetCurrent()
            Return _sessVars
        End Get
    End Property


    Private Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        Try

            'If Not Me.IsPostBack Then

            '    If Session("LagID") Is Nothing Then
            '        If (Session("GEO_COUNTRY_CODE") = "GR") Then
            '            Session("LagID") = "GR"
            '        Else
            '            Session("LagID") = gDefaultLAG
            '        End If
            '    End If

            '    cmbLag.Items(GetIndexFromLagID(Session("LagID"))).Selected = True
            '    SetLanguageLinks()
            '    LoadLAG()
            'End If


            'If (cmbLag.SelectedItem Is Nothing) Then
            '    cmbLag.Items(GetIndexFromLagID(Session("LagID"))).Selected = True
            'End If

            If Not Me.IsPostBack Then

                'If (GeneralFunctions.IsSupportedLAG(Request.QueryString("lag"))) Then
                If (Not String.IsNullOrEmpty(Request.QueryString("lag")) AndAlso (Request.QueryString("lag").ToUpper() = "US" OrElse Request.QueryString("lag").ToUpper() = "GR")) Then
                    Session("LagID") = Request.QueryString("lag").ToUpper()

                    ' update lagid info
                    Try
                        If (Me.Session("ProfileID") > 0) Then
                            DataHelpers.UpdateEUS_Profiles_LAGID(Me.Session("ProfileID"), Me.Session("LagID"))
                        End If
                    Catch ex As Exception
                        WebErrorMessageBox(Me, ex, "")
                    End Try
                End If

                If Session("LagID") Is Nothing Then
                    If (Session("GEO_COUNTRY_CODE") = "GR") Then
                        Session("LagID") = "GR"
                    Else
                        Session("LagID") = "US"
                    End If
                End If

                cmbLag.Items(GetIndexFromLagID(Session("LagID"))).Selected = True
                SetLanguageLinks()
            End If


            If (cmbLag.SelectedItem Is Nothing) Then
                cmbLag.Items(GetIndexFromLagID(Session("LagID"))).Selected = True
            End If

            Try
                ' otan to session exei diaforetiki glwsa apo tin epilegmeni glwssa
                If (GetIndexFromLagID(Session("LagID")) = 0 AndAlso Session("LagID") <> "US") Then
                    Session("LagID") = gDomainCOM_DefaultLAG
                End If
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try


            If Not Me.IsPostBack Then
                LoadLAG()
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try



        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Protected Sub cmbLag_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbLag.SelectedIndexChanged
        Try

            Dim lagCookie As HttpCookie = Request.Cookies("lagCookie")
            If lagCookie Is Nothing Then
                Dim szGuid As String = System.Guid.NewGuid().ToString()
                lagCookie = New HttpCookie("lagCookie", szGuid)
                lagCookie.Expires = DateTime.Now.AddYears(2)
                lagCookie.Item("LagID") = cmbLag.SelectedItem.Value
                Response.Cookies.Add(lagCookie)
                Session("lagCookie") = szGuid
            Else
                Session("lagCookie") = Server.HtmlEncode(lagCookie.Value)
                lagCookie.Item("LagID") = cmbLag.SelectedItem.Value
                If (lagCookie.Values.Count > 0) Then Session("lagCookie") = lagCookie.Values(0)
            End If
            Session("LagID") = lagCookie.Item("LagID")
            Response.Cookies.Add(lagCookie)
            'Response.AddHeader("Refresh", "1")

        Catch ex As Exception
            Session("LagID") = cmbLag.SelectedItem.Value
        End Try


        Try

            Me._pageData = Nothing
            LoadLAG()

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try


        Try
            ModGlobals.UpdateUserControls(Me.Page, True)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub

    Private Function GetIndexFromLagID(ByVal LagID As String) As Integer
        Select Case LagID
            Case "US"
                Return 0
            Case "GR"
                Return 4
            Case "DE"
                Return 3
            Case "FR"
                Return 2
            Case "HU"
                Return 5
            Case "IT"
                Return 6
            Case "RO"
                Return 7
            Case "AL"
                Return 1
            Case "TR"
                Return 8
            Case Else
                Return 0

        End Select
    End Function


    Protected Sub LoadLAG()
        'Try
        '    'PopulateMenuData(mnuHeaderLeft, "topLeft")
        '    'PopulateMenuData(mnuHeaderRight, "topRight")
        '    PopulateMenuData(mnuFooter, "bottom")
        '    'PopulateMenuData(mnuHeader, "bottom")
        '    _RootMaster.PopulateMenuData(mnuHeaderLeft, "topLeft")

        '    _RootMaster.PopulateMenuData(mnuFooter1, "mnuFooter1")
        '    _RootMaster.PopulateMenuData(mnuFooter2, "mnuFooter2")
        '    _RootMaster.PopulateMenuData(mnuFooter3, "mnuFooter3")
        '    _RootMaster.PopulateMenuData(mnuFooter4, "mnuFooter4")
        '    _RootMaster.PopulateMenuData(mnuFooter5, "mnuFooter5")


        '    lblFooterMenuTitle1.Text = CurrentPageData.GetCustomString("lblFooterMenuTitle1")
        '    lblFooterMenuTitle2.Text = CurrentPageData.GetCustomString("lblFooterMenuTitle2")
        '    lblFooterMenuTitle3.Text = CurrentPageData.GetCustomString("lblFooterMenuTitle3")
        '    lblFooterMenuTitle4.Text = CurrentPageData.GetCustomString("lblFooterMenuTitle4")
        '    lblFooterMenuTitle5.Text = CurrentPageData.GetCustomString("lblFooterMenuTitle5")

        '    tdFooterMenu1.Visible = (mnuFooter1.Items.Count > 0)
        '    tdFooterMenu2.Visible = (mnuFooter2.Items.Count > 0)
        '    tdFooterMenu3.Visible = (mnuFooter3.Items.Count > 0)
        '    tdFooterMenu4.Visible = (mnuFooter4.Items.Count > 0)
        '    tdFooterMenu5.Visible = (mnuFooter5.Items.Count > 0)

        'Catch ex As Exception
        '    WebErrorMessageBox(Me, ex, "")
        'End Try

        'Try
        '    'lblFooterMenuHeader.Text = CurrentPageData.GetCustomString("lblFooterMenuHeader")
        '    lblFooterSiteTitle.Text = CurrentPageData.GetCustomString("lblFooterSiteTitle")
        '    lblFooterSiteDescription.Text = CurrentPageData.GetCustomString("lblFooterSiteDescription")
        '    lblFooterCopyRight.Text = CurrentPageData.GetCustomString("lblFooterCopyRight")
        '    lblChartTitle.Text = CurrentPageData.GetCustomString("lblChartTitle")

        '    lblEscortsWarning.Text = CurrentPageData.GetCustomString("lblEscortsWarning")


        '    Dim lblEscortsWarningTooltip As Label = lblEscortsWarningPopup.FindControl("lblEscortsWarningTooltip")
        '    If (lblEscortsWarningTooltip IsNot Nothing) Then
        '        lblEscortsWarningTooltip.Text = CurrentPageData.GetCustomString("lblEscortsWarningTooltip")
        '        lblEscortsWarningPopup.Enabled = True
        '    Else
        '        lblEscortsWarningPopup.Enabled = False
        '    End If


        '    LoadLAG_LoginControl()

        'Catch ex As Exception
        '    WebErrorMessageBox(Me, ex, "")
        'End Try

    End Sub


    Protected Sub LoadLAG_LoginControl()
        'Try
        '    Dim cPageBasic As Dating.Server.Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
        '    Dim MainLogin As System.Web.UI.WebControls.Login = ucLogin1.LoginControl
        '    'MainLogin.UserNameLabelText = CurrentPageData.GetCustomString("txtUserNameLabelText")
        '    MainLogin.PasswordLabelText = CurrentPageData.GetCustomString("txtPasswordLabelText")
        '    MainLogin.RememberMeText = CurrentPageData.GetCustomString("txtRememberMeText")
        '    MainLogin.LoginButtonText = CurrentPageData.GetCustomString("txtLoginButtonText")
        '    MainLogin.UserNameRequiredErrorMessage = CurrentPageData.GetCustomString("txtUserNameRequiredErrorMessage")
        '    MainLogin.TitleText = CurrentPageData.GetCustomString("txtLoginTitleText")

        '    Dim lbForgotPassword As ASPxHyperLink = MainLogin.FindControl("lbForgotPassword")
        '    If (lbForgotPassword IsNot Nothing) Then
        '        lbForgotPassword.Text = CurrentPageData.GetCustomString("lbForgotPassword")
        '    End If

        '    Dim lbLoginFB As ASPxHyperLink = MainLogin.FindControl("lbLoginFB")
        '    If (lbLoginFB IsNot Nothing) Then
        '        lbLoginFB.Text = CurrentPageData.GetCustomString("lbLoginFB")
        '    End If

        '    Dim btnJoinToday As ASPxButton = MainLogin.FindControl("btnJoinToday")
        '    If (btnJoinToday IsNot Nothing) Then
        '        btnJoinToday.Text = CurrentPageData.GetCustomString("btnJoinToday")
        '        btnJoinToday.PostBackUrl = "~/Register.aspx"
        '        If (Not String.IsNullOrEmpty(Request.QueryString("ReturnUrl"))) Then
        '            btnJoinToday.PostBackUrl = btnJoinToday.PostBackUrl & "?ReturnUrl=" & Server.UrlEncode(Request.QueryString("ReturnUrl"))
        '        End If
        '    End If

        '    Dim UserName As ASPxTextBox = MainLogin.FindControl("UserName")
        '    If (UserName IsNot Nothing) Then
        '        'UserName.NullText = CurrentPageData.GetCustomString("UserName.NullText")
        '        UserName.ValidationSettings.RequiredField.ErrorText = CurrentPageData.GetCustomString("UserName.ValidationSettings.RequiredField.ErrorText")
        '    End If

        '    Dim Password As ASPxTextBox = MainLogin.FindControl("Password")
        '    If (Password IsNot Nothing) Then
        '        'Password.NullText = CurrentPageData.GetCustomString("Password.NullText")
        '        Password.ValidationSettings.RequiredField.ErrorText = CurrentPageData.GetCustomString("Password.ValidationSettings.RequiredField.ErrorText")
        '    End If

        '    Dim lblUsernameTop As Label = MainLogin.FindControl("lblUsernameTop")
        '    If (lblUsernameTop IsNot Nothing) Then
        '        lblUsernameTop.Text = CurrentPageData.GetCustomString("UserName.NullText")
        '    End If

        '    Dim lblPasswordTop As Label = MainLogin.FindControl("lblPasswordTop")
        '    If (lblPasswordTop IsNot Nothing) Then
        '        lblPasswordTop.Text = CurrentPageData.GetCustomString("Password.NullText")
        '    End If


        '    Dim btnLogin As ASPxButton = MainLogin.FindControl("btnLogin")
        '    If (btnLogin IsNot Nothing) Then
        '        btnLogin.Text = CurrentPageData.GetCustomString("btnLogin")
        '    End If

        '    Dim RememberMe As CheckBox = MainLogin.FindControl("RememberMe")
        '    If (RememberMe IsNot Nothing) Then
        '        RememberMe.Text = CurrentPageData.GetCustomString("RememberMe")
        '    End If


        '    Dim chkRememberMe As ASPxCheckBox = MainLogin.FindControl("chkRememberMe")
        '    If (chkRememberMe IsNot Nothing) Then
        '        chkRememberMe.Text = CurrentPageData.GetCustomString("RememberMe")
        '    End If


        '    '' controls found on login.aspx page
        '    Dim lblUserName As Literal = MainLogin.FindControl("lblUserName")
        '    If (lblUserName IsNot Nothing) Then
        '        lblUserName.Text = CurrentPageData.GetCustomString("lblUserName")
        '    End If


        '    Dim lblPassword As Literal = MainLogin.FindControl("lblPassword")
        '    If (lblPassword IsNot Nothing) Then
        '        lblPassword.Text = CurrentPageData.GetCustomString("lblPassword")
        '    End If

        '    Dim btnLoginNow As ASPxButton = MainLogin.FindControl("btnLoginNow")
        '    If (btnLoginNow IsNot Nothing) Then
        '        btnLoginNow.Text = CurrentPageData.GetCustomString("btnLoginNow")
        '        btnLoginNow.Text = btnLoginNow.Text.Replace("&nbsp;", " ")
        '    End If

        'Catch ex As Exception
        '    WebErrorMessageBox(Me, ex, "")
        'End Try
    End Sub


    '    Public Shared Sub PopulateMenuData(menu As ASPxMenu, position As String, Optional visibleOnAdmistratorUserRole As Boolean? = Nothing, Optional visibleOnAfficateUserRole As Boolean? = Nothing, Optional visibleOnMemberUserRole As Boolean? = Nothing, Optional visibleOnPublicArea As Boolean? = Nothing, Optional VisibleOnResellerUserRole As Boolean? = Nothing)
    '        If menu Is Nothing Then Return


    '        menu.Items.Clear()

    '        Dim q = From i In Caching.SiteMenuItems Where _
    '                i.Position.ToLower = position.ToLower AndAlso _
    '                (i.VisibleOnAdmistratorUserRole = visibleOnAdmistratorUserRole OrElse visibleOnAdmistratorUserRole Is Nothing) AndAlso _
    '                (i.VisibleOnAfficateUserRole = visibleOnAfficateUserRole OrElse visibleOnAfficateUserRole Is Nothing) AndAlso _
    '                (i.VisibleOnMemberUserRole = visibleOnMemberUserRole OrElse visibleOnMemberUserRole Is Nothing) AndAlso _
    '                (i.VisibleOnPublicArea = visibleOnPublicArea OrElse visibleOnPublicArea Is Nothing) AndAlso _
    '                (i.VisibleOnResellerUserRole = VisibleOnResellerUserRole OrElse VisibleOnResellerUserRole Is Nothing) _
    '                Where i.ParrentSiteMenuItemID = 0 _
    '                Order By i.SortNumber _
    '                Select i

    '        For Each row As dsMenu.SiteMenuItemsRow In q
    '            With row

    '                ' check the item is in list already
    '                If menu.Items.FindByName(.SiteMenuItemID) IsNot Nothing Then
    '                    Continue For
    '                End If

    '                Select Case HttpContext.Current.Session("LagID")
    '                    Case "US"
    '                        If .US.Length > 0 Then
    '                            menu.Items.Add(.US, .SiteMenuItemID, .ImageURL, .NavigateURL)
    '                            menu.Items.FindByName(.SiteMenuItemID).ToolTip = .ToolTipUS
    '                        Else
    '                            GoTo def
    '                        End If
    '                    Case "GR"
    '                        If .GR.Length > 0 Then
    '                            menu.Items.Add(.GR, .SiteMenuItemID, .ImageURL, .NavigateURL)
    '                            menu.Items.FindByName(.SiteMenuItemID).ToolTip = .ToolTipGR
    '                        Else
    '                            GoTo def
    '                        End If
    '                    Case "HU"
    '                        If .HU.Length > 0 Then
    '                            menu.Items.Add(.HU, .SiteMenuItemID, .ImageURL, .NavigateURL)
    '                            menu.Items.FindByName(.SiteMenuItemID).ToolTip = .ToolTipHU
    '                        Else
    '                            GoTo def
    '                        End If
    '                    Case "ES"
    '                        If .ES.Length > 0 Then
    '                            menu.Items.Add(.ES, .SiteMenuItemID, .ImageURL, .NavigateURL)
    '                            menu.Items.FindByName(.SiteMenuItemID).ToolTip = .ToolTipES
    '                        Else
    '                            GoTo def
    '                        End If
    '                    Case "DE"
    '                        If .DE.Length > 0 Then
    '                            menu.Items.Add(.DE, .SiteMenuItemID, .ImageURL, .NavigateURL)
    '                            menu.Items.FindByName(.SiteMenuItemID).ToolTip = .ToolTipDE
    '                        Else
    '                            GoTo def
    '                        End If
    '                    Case "RO"
    '                        If .RO.Length > 0 Then
    '                            menu.Items.Add(.RO, .SiteMenuItemID, .ImageURL, .NavigateURL)
    '                            menu.Items.FindByName(.SiteMenuItemID).ToolTip = .ToolTipRO
    '                        Else
    '                            GoTo def
    '                        End If
    '                    Case "TU"
    '                        If .TU.Length > 0 Then
    '                            menu.Items.Add(.TU, .SiteMenuItemID, .ImageURL, .NavigateURL)
    '                            menu.Items.FindByName(.SiteMenuItemID).ToolTip = .ToolTipTU
    '                        Else
    '                            GoTo def
    '                        End If
    '                    Case "IT"
    '                        If .IT.Length > 0 Then
    '                            menu.Items.Add(.IT, .SiteMenuItemID, .ImageURL, .NavigateURL)
    '                            menu.Items.FindByName(.SiteMenuItemID).ToolTip = .ToolTipIT
    '                        Else
    '                            GoTo def
    '                        End If
    '                    Case "IL"
    '                        If .IL.Length > 0 Then
    '                            menu.Items.Add(.IL, .SiteMenuItemID, .ImageURL, .NavigateURL)
    '                            menu.Items.FindByName(.SiteMenuItemID).ToolTip = .ToolTipIL
    '                        Else
    '                            GoTo def
    '                        End If
    '                    Case "FR"
    '                        If .FR.Length > 0 Then
    '                            menu.Items.Add(.FR, .SiteMenuItemID, .ImageURL, .NavigateURL)
    '                            menu.Items.FindByName(.SiteMenuItemID).ToolTip = .ToolTipFR
    '                        Else
    '                            GoTo def
    '                        End If
    '                    Case Else 'default to US
    '                        GoTo def
    '                End Select

    '                GoTo skip 'skip default load for US
    'def:
    '                If .US.Length > 0 Then
    '                    menu.Items.Add(.US, .SiteMenuItemID, .ImageURL, .NavigateURL)
    '                    menu.Items.FindByName(.SiteMenuItemID).ToolTip = .ToolTipUS
    '                End If
    'skip:
    '                'do nothing
    '            End With
    '        Next

    '        'now load all sub menus for loaded root menus
    '        Dim qSub = From i In Caching.SiteMenuItems Where _
    '                i.Position.ToLower = position.ToLower AndAlso _
    '                (i.VisibleOnAdmistratorUserRole = visibleOnAdmistratorUserRole OrElse visibleOnAdmistratorUserRole Is Nothing) AndAlso _
    '                (i.VisibleOnAfficateUserRole = visibleOnAfficateUserRole OrElse visibleOnAfficateUserRole Is Nothing) AndAlso _
    '                (i.VisibleOnMemberUserRole = visibleOnMemberUserRole OrElse visibleOnMemberUserRole Is Nothing) AndAlso _
    '                (i.VisibleOnPublicArea = visibleOnPublicArea OrElse visibleOnPublicArea Is Nothing) AndAlso _
    '                (i.VisibleOnResellerUserRole = VisibleOnResellerUserRole OrElse VisibleOnResellerUserRole Is Nothing) _
    '                Where i.ParrentSiteMenuItemID <> 0 _
    '                Order By i.SortNumber _
    '                Select i


    '        For Each row As dsMenu.SiteMenuItemsRow In qSub
    '            With row
    '                Dim menuItem As MenuItem = menu.Items.FindByName(.ParrentSiteMenuItemID)
    '                If menuItem IsNot Nothing Then

    '                    ' check the item is in list already
    '                    If menuItem.Items.FindByName(.SiteMenuItemID) IsNot Nothing Then
    '                        Continue For
    '                    End If

    '                    Select Case HttpContext.Current.Session("LagID")
    '                        Case "US"
    '                            If .US.Length > 0 Then
    '                                menuItem.Items.Add(.US, .SiteMenuItemID, .ImageURL, .NavigateURL)
    '                                menuItem.Items.FindByName(.SiteMenuItemID).ToolTip = .ToolTipUS
    '                            Else
    '                                GoTo defSub
    '                            End If
    '                        Case "GR"
    '                            If .GR.Length > 0 Then
    '                                menuItem.Items.Add(.GR, .SiteMenuItemID, .ImageURL, .NavigateURL)
    '                                menuItem.Items.FindByName(.SiteMenuItemID).ToolTip = .ToolTipGR
    '                            Else
    '                                GoTo defSub
    '                            End If
    '                        Case "HU"
    '                            If .HU.Length > 0 Then
    '                                menuItem.Items.Add(.HU, .SiteMenuItemID, .ImageURL, .NavigateURL)
    '                                menuItem.Items.FindByName(.SiteMenuItemID).ToolTip = .ToolTipHU
    '                            Else
    '                                GoTo defSub
    '                            End If
    '                        Case "ES"
    '                            If .ES.Length > 0 Then
    '                                menuItem.Items.Add(.ES, .SiteMenuItemID, .ImageURL, .NavigateURL)
    '                                menuItem.Items.FindByName(.SiteMenuItemID).ToolTip = .ToolTipES
    '                            Else
    '                                GoTo defSub
    '                            End If
    '                        Case "DE"
    '                            If .DE.Length > 0 Then
    '                                menuItem.Items.Add(.DE, .SiteMenuItemID, .ImageURL, .NavigateURL)
    '                                menuItem.Items.FindByName(.SiteMenuItemID).ToolTip = .ToolTipDE
    '                            Else
    '                                GoTo defSub
    '                            End If
    '                        Case "RO"
    '                            If .RO.Length > 0 Then
    '                                menuItem.Items.Add(.RO, .SiteMenuItemID, .ImageURL, .NavigateURL)
    '                                menuItem.Items.FindByName(.SiteMenuItemID).ToolTip = .ToolTipRO
    '                            Else
    '                                GoTo defSub
    '                            End If
    '                        Case "TU"
    '                            If .TU.Length > 0 Then
    '                                menuItem.Items.Add(.TU, .SiteMenuItemID, .ImageURL, .NavigateURL)
    '                                menuItem.Items.FindByName(.SiteMenuItemID).ToolTip = .ToolTipTU
    '                            Else
    '                                GoTo defSub
    '                            End If
    '                        Case "IT"
    '                            If .IT.Length > 0 Then
    '                                menuItem.Items.Add(.IT, .SiteMenuItemID, .ImageURL, .NavigateURL)
    '                                menuItem.Items.FindByName(.SiteMenuItemID).ToolTip = .ToolTipIT
    '                            Else
    '                                GoTo defSub
    '                            End If
    '                        Case "IL"
    '                            If .IL.Length > 0 Then
    '                                menuItem.Items.Add(.IL, .SiteMenuItemID, .ImageURL, .NavigateURL)
    '                                menuItem.Items.FindByName(.SiteMenuItemID).ToolTip = .ToolTipIL
    '                            Else
    '                                GoTo defSub
    '                            End If
    '                        Case "FR"
    '                            If .FR.Length > 0 Then
    '                                menuItem.Items.Add(.FR, .SiteMenuItemID, .ImageURL, .NavigateURL)
    '                                menuItem.Items.FindByName(.SiteMenuItemID).ToolTip = .ToolTipFR
    '                            Else
    '                                GoTo defSub
    '                            End If
    '                        Case Else 'default to US
    '                            GoTo defSub
    '                    End Select

    '                    GoTo skipSub 'skip default load for US
    'defSub:
    '                    If .US.Length > 0 Then
    '                        menuItem.Items.Add(.US, .SiteMenuItemID, .ImageURL, .NavigateURL)
    '                        menuItem.Items.FindByName(.SiteMenuItemID).ToolTip = .ToolTipUS
    '                    End If
    'skipSub:
    '                    'do nothing
    '                End If
    '            End With
    '        Next

    '    End Sub


    Public Sub PopulateMenuData(ByVal menu As ListView, ByVal position As String, Optional ByVal visibleOnAdmistratorUserRole As Boolean? = Nothing, Optional ByVal visibleOnAfficateUserRole As Boolean? = Nothing, Optional ByVal visibleOnMemberUserRole As Boolean? = Nothing, Optional ByVal visibleOnPublicArea As Boolean? = Nothing, Optional ByVal VisibleOnResellerUserRole As Boolean? = Nothing)
        If menu Is Nothing Then Return

        Dim _dt As DataTable = GetMenuData(menu.UniqueID, position, visibleOnAdmistratorUserRole, visibleOnAfficateUserRole, visibleOnMemberUserRole, visibleOnPublicArea, VisibleOnResellerUserRole)

        'Dim cnt As Integer = 0
        'If (menu.ID = mnuHeaderLeft.ID) Then ' Or menu.ID = mnuHeaderRight.ID

        '    Dim rawUrl = Request.RawUrl.ToLower()
        '    Dim relPathUrl = Page.AppRelativeVirtualPath.ToLower()

        '    For cnt = 0 To _dt.Rows.Count - 1
        '        Dim dr As DataRow = _dt.Rows(cnt)
        '        Dim itemUrl As String = dr("NavigateURL").ToString().ToLower()
        '        Dim style As String = ""

        '        'If (cnt = 0) Then
        '        '    style = "level1 item1 first"
        '        'ElseIf (cnt = _dt.Rows.Count - 1) Then
        '        '    style = "level1 item" & (cnt + 2) & " last"
        '        'ElseIf (cnt > 0) Then
        '        '    style = "level1 item" & (cnt + 2)
        '        'End If

        '        ' set selected item style
        '        If itemUrl = relPathUrl Or itemUrl.TrimStart("~"c) = rawUrl Then
        '            style = style & "down"
        '            dr("CssClass") = style
        '            Exit For
        '        End If

        '    Next
        'End If


        menu.Items.Clear()
        menu.DataSource = _dt
        menu.DataBind()
    End Sub



    Public Function GetMenuData(ByVal menuId As String, ByVal position As String, Optional ByVal visibleOnAdmistratorUserRole As Boolean? = Nothing, Optional ByVal visibleOnAfficateUserRole As Boolean? = Nothing, Optional ByVal visibleOnMemberUserRole As Boolean? = Nothing, Optional ByVal visibleOnPublicArea As Boolean? = Nothing, Optional ByVal VisibleOnResellerUserRole As Boolean? = Nothing) As DataTable

        If Session(menuId + Session("LagID") + position) Is Nothing Then
            Using da As New dsMenuTableAdapters.SiteMenuItemsTableAdapter
                Using con As New SqlClient.SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("AppDBconnectionString").ConnectionString)
                    da.Connection = con
                    Using dt As dsMenu.SiteMenuItemsDataTable = da.GetData(position, visibleOnAdmistratorUserRole, visibleOnAfficateUserRole, visibleOnMemberUserRole, visibleOnPublicArea, VisibleOnResellerUserRole)
                        Session(menuId + Session("LagID") + position) = dt
                    End Using
                End Using
            End Using
        End If


        Using _dt As New DataTable()


            For Each row As dsMenu.SiteMenuItemsRow In DirectCast(Session(menuId + Session("LagID") + position), dsMenu.SiteMenuItemsDataTable)

                If (_dt.Columns.Count = 0) Then
                    _dt.Columns.Add("Text", String.Empty.GetType())
                    _dt.Columns.Add("Name", row.SiteMenuItemID.GetType())
                    _dt.Columns.Add("ImageUrl", String.Empty.GetType())
                    _dt.Columns.Add("NavigateUrl", String.Empty.GetType())
                    _dt.Columns.Add("ToolTip", String.Empty.GetType())
                    _dt.Columns.Add("CssClass", String.Empty.GetType())
                End If


                Dim _text As String = ""
                Dim _toolTip As String = ""

                With row

                    ' by default use english
                    _text = .US
                    _toolTip = .ToolTipUS

                    Select Case Session("LagID")
                        Case "GR"
                            If .GR.Length > 0 Then
                                _text = .GR
                                _toolTip = .ToolTipGR
                            End If
                        Case "HU"
                            If .HU.Length > 0 Then
                                _text = .HU
                                _toolTip = .ToolTipHU
                            End If
                        Case "ES"
                            If .ES.Length > 0 Then
                                _text = .ES
                                _toolTip = .ToolTipES
                            End If
                        Case "DE"
                            If .DE.Length > 0 Then
                                _text = .DE
                                _toolTip = .ToolTipDE
                            End If
                        Case "RO"
                            If .RO.Length > 0 Then
                                _text = .RO
                                _toolTip = .ToolTipRO
                            End If
                        Case "TU"
                            If .TU.Length > 0 Then
                                _text = .TU
                                _toolTip = .ToolTipTU
                            End If
                        Case "IT"
                            If .IT.Length > 0 Then
                                _text = .IT
                                _toolTip = .ToolTipIT
                            End If
                        Case "IL"
                            If .IL.Length > 0 Then
                                _text = .IL
                                _toolTip = .ToolTipIL
                            End If
                        Case "FR"
                            If .FR.Length > 0 Then
                                _text = .FR
                                _toolTip = .ToolTipFR
                            End If
                    End Select

                End With


                Dim _dr As DataRow = _dt.NewRow()

                _dr("Name") = row.SiteMenuItemID
                _dr("ImageUrl") = row.ImageURL
                _dr("NavigateUrl") = row.NavigateURL
                _dr("Text") = _text
                _dr("ToolTip") = _toolTip
                _dt.Rows.Add(_dr)
            Next

            Return _dt

        End Using
    End Function



    Private Sub SetLanguageLinks()
        Try


            Dim url As String = Request.Url.AbsolutePath
            url = url & "?"

            For Each itm As String In Request.QueryString.AllKeys
                If (Not String.IsNullOrEmpty(itm) AndAlso itm.ToUpper() <> "LAG") Then
                    url = url & itm & "=" & Request.QueryString(itm) & "&"
                End If
            Next



            lnkEn.NavigateUrl = url & "lag=US"
            lnkGR.NavigateUrl = url & "lag=GR"
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


End Class
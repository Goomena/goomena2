﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/ErrorPages/err.Master" CodeBehind="NoAccess.aspx.vb" Inherits="Dating.Referrals.Server.Site.Web.NoAccess" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <h2 style="text-shadow: none; border: none; font-family: Lucida Grande,Lucida Sans Unicode,Verdana,Arial,sans-serif;">
        <asp:Label ID="lblHeader" runat="server" Text="403 It's not allowed! " /><span class="loginHereLink"></span></h2>
    <div class="lfloat" style="margin:0 20px">
        <img alt="403 Error" src="../images/oops.png" />
    </div>
    <div class="lfloat">
        <dx:ASPxLabel ID="lbHTMLBody" runat="server" ClientIDMode="AutoID" EncodeHtml="False"
            Text="Sorry, it's not allowed to answer." Font-Size="14px">
        </dx:ASPxLabel>
    </div>
    <div class="clear">
    </div>
</asp:Content>

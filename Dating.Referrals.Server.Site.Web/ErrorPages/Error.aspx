﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/ErrorPages/err.Master"
    CodeBehind="Error.aspx.vb" Inherits="Dating.Referrals.Server.Site.Web._Error" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <h2 style="text-shadow: none; border: none; font-family: Lucida Grande,Lucida Sans Unicode,Verdana,Arial,sans-serif;">
        <asp:Label ID="lblHeader" runat="server" Text="Oops! Error occured." /><span class="loginHereLink"></span></h2>
    <div class="lfloat" style="margin:0 20px">
        <img alt="404 Error Pages" src="../images/oops.png" />
    </div>
    <div class="lfloat">
        <dx:ASPxLabel ID="lbHTMLBody" runat="server" ClientIDMode="AutoID" EncodeHtml="False"
            Text="Sorry, but something gone really wrong!" Font-Size="14px">
        </dx:ASPxLabel>
    </div>
    <div class="clear">
    </div>
   <%-- <h2 style="text-shadow: none; border: none; font-family: Lucida Grande,Lucida Sans Unicode,Verdana,Arial,sans-serif;">
        <asp:Label ID="lblHeader" runat="server" Text="Error!" /><span class="loginHereLink"></span></h2>
    <dx:ASPxLabel ID="lbHTMLBody" runat="server" ClientIDMode="AutoID" EncodeHtml="False"
        Text="Something gone really wrong!">
    </dx:ASPxLabel>--%>
</asp:Content>

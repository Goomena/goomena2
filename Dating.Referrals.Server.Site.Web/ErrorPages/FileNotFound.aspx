﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/ErrorPages/err.Master"
    CodeBehind="FileNotFound.aspx.vb" Inherits="Dating.Referrals.Server.Site.Web.FileNotFound" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <h2 style="text-shadow: none; border: none; font-family: Lucida Grande,Lucida Sans Unicode,Verdana,Arial,sans-serif;">
        <asp:Label ID="lblHeader" runat="server" Text="404 Error." /><span class="loginHereLink"></span></h2>
    <div class="lfloat" style="margin:0 20px">
        <img alt="404 Error" src="../images/image001.png" />
    </div>
    <div class="lfloat">
        <dx:ASPxLabel ID="lbHTMLBody" runat="server" ClientIDMode="AutoID" EncodeHtml="False"
            Text="Sorry, we cannot find what you asked for." Font-Size="14px">
        </dx:ASPxLabel>
    </div>
    <div class="clear">
    </div>
</asp:Content>

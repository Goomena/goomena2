﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports Dating.Server.Core.DLL

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class UniPAYIPN
    Inherits System.Web.Services.WebService


    Dim Description As String
    Dim Money As Double
    Dim TransactionID As String
    Dim CreditsPurchased As Integer
    Dim PurchaseGigabytes As Integer
    Dim REMOTE_ADDR As String
    Dim HTTP_USER_AGENT As String
    Dim HTTP_REFERER As String
    Dim CustomerID As Integer
    Dim CustomReferrer As String



    <WebMethod()> _
    Public Function HelloWorld() As String
        Return "Hello World"
    End Function
    ' Exaples of call UniPAY
    ' http://www.soundsoft.com/PAY/pay.ashx?pid=12&pp=1&cid=1234&promo=ddddd&exd=dddddddd
    ' pid = to product ID apo tin DB tou UniPAY
    ' pp = Use ton PayProvider me ID 1 px Paypal alios tha xrisimophisi ton default
    ' cid = Customer ID
    ' promo= Promo Code
    ' exd = (optionasl) extra data
    ' Me krifo to soundsoft....
    ' Kanoume to paymix.com na kani redirect sto soundsoft 


    <WebMethod()> _
    Public Function IPNEventRaised(ByVal cDataRecordIPN As clsDataRecordIPN) As clsDataRecordIPNReturn
        Dim cDataRecordIPNReturn As New clsDataRecordIPNReturn
        Dim toAddress As String = ConfigurationManager.AppSettings("gToEmail")

        Try
            If (cDataRecordIPN IsNot Nothing) Then cDataRecordIPN.PromoCode = ""
            LogInput(cDataRecordIPN)

            If VerifyHASH(cDataRecordIPN) Then

                Dim paymethod As PaymentMethods = GetPayMethod(cDataRecordIPN)


                ' Ine OK Proxora stin analisi....
                Dim productCode As String = Nothing
                Try
                    Dim r As New Regex("ProductCode:\[(?<ProductCode>.*)\]", RegexOptions.IgnoreCase)
                    Dim m As Match = r.Match(cDataRecordIPN.SaleDescription)
                    If (m.Success) Then
                        productCode = m.Groups("ProductCode").Value
                    End If
                Catch ex As Exception
                End Try

                Dim bonusCode As String = Nothing
                Try
                    Dim r As New Regex("BonusCode:\[(?<BonusCode>.*)\]", RegexOptions.IgnoreCase)
                    Dim m As Match = r.Match(cDataRecordIPN.SaleDescription)
                    If (m.Success) Then
                        bonusCode = m.Groups("BonusCode").Value
                        If (Not String.IsNullOrEmpty(bonusCode) AndAlso Not DataHelpers.EUS_CreditsBonusCodes_IsCodeAvailable(cDataRecordIPN.CustomerID, bonusCode)) Then
                            bonusCode = Nothing

                            clsMyMail.SendMail(ConfigurationManager.AppSettings("gToEmail"), "no purch", cDataRecordIPN.SaleDescription)
                            cDataRecordIPNReturn.HasErrors = True
                            cDataRecordIPNReturn.ErrorCode = 100
                            cDataRecordIPNReturn.Message = "no purch"
                            Return cDataRecordIPNReturn
                        End If
                    End If
                Catch ex As Exception
                End Try

                If Not String.IsNullOrEmpty(productCode) Then

                    FillTransInfo(cDataRecordIPN.SaleDescription,
                                  cDataRecordIPN.PayProviderAmount,
                                  cDataRecordIPN.PayProviderTransactionID,
                                  cDataRecordIPN.SaleQuantity, 0,
                                  cDataRecordIPN.custopmerIP,
                                  HttpContext.Current.Request.ServerVariables("HTTP_USER_AGENT"),
                                  HttpContext.Current.Request.ServerVariables("HTTP_REFERER"),
                                  cDataRecordIPN.CustomerID,
                                  cDataRecordIPN.CustomReferrer)

                ElseIf Not String.IsNullOrEmpty(bonusCode) Then

                    FillTransInfo(cDataRecordIPN.SaleDescription,
                                      cDataRecordIPN.PayProviderAmount,
                                      cDataRecordIPN.PayProviderTransactionID,
                                      cDataRecordIPN.SaleQuantity, 0,
                                      cDataRecordIPN.custopmerIP,
                                      HttpContext.Current.Request.ServerVariables("HTTP_USER_AGENT"),
                                      HttpContext.Current.Request.ServerVariables("HTTP_REFERER"),
                                      cDataRecordIPN.CustomerID,
                                      cDataRecordIPN.CustomReferrer)

                    cDataRecordIPN.PromoCode = bonusCode


                ElseIf cDataRecordIPN.SaleDescription.ToUpper().Contains("DAYS") OrElse _
                    cDataRecordIPN.SaleDescription.ToUpper().Contains("CREDITS") OrElse _
                    cDataRecordIPN.SaleDescription.Contains("Lifetime") OrElse _
                    cDataRecordIPN.SaleDescription.Contains("Unlimited") OrElse _
                    cDataRecordIPN.SaleDescription.Contains("Reseller") Then

                    FillTransInfo(cDataRecordIPN.SaleDescription,
                                  cDataRecordIPN.PayProviderAmount,
                                  cDataRecordIPN.PayProviderTransactionID,
                                  cDataRecordIPN.SaleQuantity, 0,
                                  cDataRecordIPN.custopmerIP,
                                  HttpContext.Current.Request.ServerVariables("HTTP_USER_AGENT"),
                                  HttpContext.Current.Request.ServerVariables("HTTP_REFERER"),
                                  cDataRecordIPN.CustomerID,
                                  cDataRecordIPN.CustomReferrer)

                    'ElseIf cDataRecordIPN.SaleDescription.Contains("Giga") Then
                    '    FillTransInfo(cDataRecordIPN.SaleDescription,
                    '                  cDataRecordIPN.PayProviderAmount,
                    '                  cDataRecordIPN.PayProviderTransactionID, 0,
                    '                  cDataRecordIPN.SaleQuantity,
                    '                  cDataRecordIPN.custopmerIP,
                    '                  HttpContext.Current.Request.ServerVariables("HTTP_USER_AGENT"),
                    '                  HttpContext.Current.Request.ServerVariables("HTTP_REFERER"),
                    '                  cDataRecordIPN.CustomerID,
                    '                  cDataRecordIPN.CustomReferrer)

                Else
                    clsMyMail.SendMail(ConfigurationManager.AppSettings("gToEmail"), "no purch", "")
                    cDataRecordIPNReturn.HasErrors = True
                    cDataRecordIPNReturn.ErrorCode = 100
                    cDataRecordIPNReturn.Message = "no purch"
                    Return cDataRecordIPNReturn

                End If

                clsCustomer.AddTransaction(Description, Money,
                                           TransactionID, "",
                                           CreditsPurchased,
                                           REMOTE_ADDR,
                                           HTTP_USER_AGENT,
                                           HTTP_REFERER,
                                           CustomerID,
                                           CustomReferrer,
                                           cDataRecordIPN.BuyerInfo.PayerEmail,
                                           paymethod,
                                           cDataRecordIPN.PromoCode,
                                           cDataRecordIPN.TransactionTypeID,
                                           productCode)
                cDataRecordIPNReturn.HasErrors = False
                cDataRecordIPNReturn.ErrorCode = 0
            Else
                cDataRecordIPNReturn.HasErrors = True
                cDataRecordIPNReturn.ErrorCode = 101
                cDataRecordIPNReturn.Message = "Errors on HASH"
            End If
        Catch ex As Exception
            cDataRecordIPNReturn.HasErrors = True
            cDataRecordIPNReturn.ErrorCode = 100
            cDataRecordIPNReturn.Message = ex.Message
            Try
                clsMyMail.SendMail(ConfigurationManager.AppSettings("gToEmail"), "Payment service exceptions", ex.ToString(), False)
            Catch ex1 As Exception

            End Try
        End Try
        Return cDataRecordIPNReturn
    End Function

    Private Sub FillTransInfo(ByVal tDesc As String,
                              ByVal tMoney As Double,
                              ByVal tTransID As String,
                              ByVal tCredits As Integer,
                              ByVal tPGiga As Integer,
                              ByVal tRemAddr As String,
                              ByVal tUserAgt As String,
                              ByVal tHttpRef As String,
                              ByVal tcustID As Integer,
                              ByVal tCustRef As String)
        Description = tDesc
        Money = tMoney
        TransactionID = tTransID
        CreditsPurchased = tCredits
        PurchaseGigabytes = tPGiga
        REMOTE_ADDR = tRemAddr
        HTTP_USER_AGENT = tUserAgt
        HTTP_REFERER = tHttpRef
        CustomerID = tcustID
        CustomReferrer = tCustRef

    End Sub

    Private Function VerifyHASH(ByVal cDataRecordIPN As clsDataRecordIPN) As Boolean
        Try
            Dim sData As String
            sData = cDataRecordIPN.PayProviderAmount & "+" & cDataRecordIPN.PaymentDateTime & "+" & cDataRecordIPN.PayTransactionID & "+" & cDataRecordIPN.CustomerID & "+" & cDataRecordIPN.SalesSiteProductID & "Extr@Ded0men@"
            Dim h As New Library.Public.clsHash
            Dim Code As String = Library.Public.clsHash.ComputeHash(sData, "SHA512")
            If Code = cDataRecordIPN.VerifyHASH Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            Return False
        End Try
    End Function


    Function GetPayMethod(cDataRecordIPN As clsDataRecordIPN)
        Dim paymethod As PaymentMethods
        If cDataRecordIPN.SaleDescription.ToUpper().Contains("EPOCH") Then
            paymethod = PaymentMethods.CreditCard
        ElseIf cDataRecordIPN.SaleDescription.ToUpper().Contains("ALERTPAY") Then
            paymethod = PaymentMethods.AlertPay
        ElseIf cDataRecordIPN.SaleDescription.Contains("paysafecard") Then
            paymethod = PaymentMethods.PaySafe
        ElseIf cDataRecordIPN.SaleDescription.ToUpper().Contains("2CO") Then
            paymethod = PaymentMethods.CreditCard
        ElseIf cDataRecordIPN.SaleDescription.ToUpper().Contains("WEBMONEY") Then
            paymethod = PaymentMethods.WebMoney
        Else
            paymethod = PaymentMethods.PayPal
        End If
        Return paymethod
    End Function

    Sub LogInput(cDataRecordIPN As clsDataRecordIPN)
        Try

            Dim paymethod As PaymentMethods
            Try
                paymethod = GetPayMethod(cDataRecordIPN)
            Catch ex As Exception
            End Try

            Dim body As String = clsCustomer.GetMessageBody(
                cDataRecordIPN.SaleDescription,
                cDataRecordIPN.PayProviderAmount,
                cDataRecordIPN.PayProviderTransactionID,
                "",
                cDataRecordIPN.SaleQuantity,
                cDataRecordIPN.custopmerIP,
                HttpContext.Current.Request.ServerVariables("HTTP_USER_AGENT"),
                HttpContext.Current.Request.ServerVariables("HTTP_REFERER"),
                cDataRecordIPN.CustomerID,
                cDataRecordIPN.CustomReferrer,
                cDataRecordIPN.BuyerInfo.PayerEmail,
                paymethod,
                cDataRecordIPN.PromoCode,
                cDataRecordIPN.TransactionTypeID)


            clsMyMail.SendMail(ConfigurationManager.AppSettings("gToEmail"), "Payment service log", body, True)
        Catch ex As Exception

        End Try

    End Sub
End Class
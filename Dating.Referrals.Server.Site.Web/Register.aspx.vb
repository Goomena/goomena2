﻿Imports Dating.Server.Core.DLL
Imports System.IO
Imports ReferralsDLL = Dating.Referrals.Server.Core.DLL
Imports SiteDLL = Dating.Server.Core.DLL
Imports SiteDSDLL = Dating.Server.Datasets.DLL
Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Datasets.DLL.DSMembers

Public Class Register
    Inherits BasePage

    Dim _pageData As clsPageData
    Protected ReadOnly Property CurrentPageData As clsPageData
        Get
            If (_pageData Is Nothing) Then _pageData = New clsPageData("Referrer.Register.aspx", Context)
            Return _pageData
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
           
            If (Request("step") = "3" AndAlso Me.ReferrerCustomerID = 0) Then
                Response.Redirect(ResolveUrl("~/Logout.aspx"))
            End If
        Catch ex As System.Threading.ThreadAbortException
            Return
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Try

            txtLogin.Text = txtLogin.Text.Trim()
            txtPass.Text = txtPass.Text.Trim()

            If (Not Me.IsPostBack) Then
                LoadLAG()
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try


        Try
            If (Not Me.IsPostBack) Then

                If (Session("SuccesStep") = 2) Then
                    mvReg.SetActiveView(vwUpload)

                ElseIf (Session("SuccesStep") = 1) Then
                    mvReg.SetActiveView(vwAgreement)

                Else
                    If (Request.QueryString("step") = "1" OrElse String.IsNullOrEmpty(Request.QueryString("step"))) Then
                        mvReg.SetActiveView(vwIDNum)

                    ElseIf (Request.QueryString("step") = "2") Then
                        mvReg.SetActiveView(vwAgreement)

                    ElseIf (Request.QueryString("step") = "3") Then
                        mvReg.SetActiveView(vwUpload)

                    ElseIf (Request.QueryString("step") = "4") Then

                    End If

                End If

                'If (mvReg.GetActiveView() Is vwIDNum) Then

                'End If
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub



    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
    End Sub


    Protected Sub LoadLAG()
        Try
            Dim cPageBasic As clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic

            If (clsSessionVariables.GetCurrent().MemberData IsNot Nothing) Then
                lblAgreementTitle.Text = lblAgreementTitle.Text.Replace("[LOGINNAME]", clsSessionVariables.GetCurrent().MemberData.LoginName)
            End If

            lblAgreeErr.Text = CurrentPageData.GetCustomString("Error.AgreeTerms.Required")
            lblAgreementText.Text = CurrentPageData.GetCustomString("AgreeTerms.Text")

            lblRegisterPros.Text = CurrentPageData.GetCustomString("lblRegisterPros")


            Web.ClsCombos.FillComboUsingDatatable(SYS_CountriesGEOHelper.gDSListsGEO__IsEnabled_PrintableNameASC.SYS_CountriesGEO, "PrintableName", "Iso", cbCountry, True, "PrintableName")
            Dim defC As DevExpress.Web.ASPxEditors.ListEditItem = New DevExpress.Web.ASPxEditors.ListEditItem(CurrentPageData.GetCustomString("cbCountry_default"), "-1")
            defC.Selected = True
            cbCountry.Items.Insert(0, defC)

            Dim def As DevExpress.Web.ASPxEditors.ListEditItem = cbCountry.Items.FindByValue(Session("GEO_COUNTRY_CODE"))
            If (def IsNot Nothing) Then

                cbCountry.SelectedIndex = -1
                def.Selected = True
                cbRegion.DataSourceID = ""
                cbRegion.DataSource = clsGeoHelper.GetCountryRegions(cbCountry.SelectedItem.Value, Session("LAGID"))
                cbRegion.TextField = "region1"
                cbRegion.DataBind()

                Dim defR As DevExpress.Web.ASPxEditors.ListEditItem = New DevExpress.Web.ASPxEditors.ListEditItem(CurrentPageData.GetCustomString("cbRegion_default"), "-1")
                defR.Selected = True
                cbRegion.Items.Insert(0, defR)

            End If


            'lnkBack.Text = CurrentPageData.GetCustomString("lnkBack")

            'lblRefer.Text = CurrentPageData.GetCustomString("lblRefer")
            'btnCheckAndActivate.Text = CurrentPageData.GetCustomString("btnCheckAndActivate")
            'lblWelcomeLogin.Text = CurrentPageData.GetCustomString("lblWelcomeLogin")
            'lnkLearnAbout.Text = CurrentPageData.GetCustomString("lnkLearnAbout")
            'lnkLearnAbout2.Text = CurrentPageData.GetCustomString("lnkLearnAbout2")
            'lnkEstimateCenter.Text = CurrentPageData.GetCustomString("lnkEstimateCenter")
            'lnkEstimateCenter2.Text = CurrentPageData.GetCustomString("lnkEstimateCenter2")
            'lblYourCode.Text = CurrentPageData.GetCustomString("lblYourCode")
            'lblYourCodeDesc.Text = CurrentPageData.GetCustomString("lblYourCodeDesc")
            'lblBalance.Text = CurrentPageData.GetCustomString("lblBalance")
            'lblCredits.Text = CurrentPageData.GetCustomString("lblCredits")
            'lblAvailableBalanceText.Text = CurrentPageData.GetCustomString("lblAvailableBalanceText")
            'lblAvailableBalance.Text = CurrentPageData.GetCustomString("lblAvailableBalance")
            'lblPendingBalanceText.Text = CurrentPageData.GetCustomString("lblPendingBalanceText")
            'lblPendingBalance.Text = CurrentPageData.GetCustomString("lblPendingBalance")
            'lblBalanceMoneyText.Text = CurrentPageData.GetCustomString("lblBalanceMoneyText")
            'lblBalanceMoney.Text = CurrentPageData.GetCustomString("lblBalanceMoney")
            'lblFamilyMembersDesc.Text = CurrentPageData.GetCustomString("lblFamilyMembersDesc")

            'lblPayedMoneyText.Text = CurrentPageData.GetCustomString("lblPayedMoneyText")
            'lblPayedMoney.Text = CurrentPageData.GetCustomString("lblPayedMoney")

            'lblReferrerContact_AfterPhoto_Text.Text = CurrentPageData.GetCustomString("lblReferrerContact_AfterPhoto_Text")
            ''lblIncomeStatsDesc.Text = CurrentPageData.GetCustomString("lblIncomeStatsDesc")
            ''lnkMyTree.Text = CurrentPageData.GetCustomString("lnkMyTree")
            ''lblMyTreeDesc.Text = CurrentPageData.GetCustomString("lblMyTreeDesc")
            ''lnkMyMoneyPerLvl.Text = CurrentPageData.GetCustomString("lnkMyMoneyPerLvl")
            ''lblMyMoneyPerLvlDesc.Text = CurrentPageData.GetCustomString("lblMyMoneyPerLvlDesc")
            ''lnkMyPercents.Text = CurrentPageData.GetCustomString("lnkMyPercents")
            ''lblMyPercentsDesc.Text = CurrentPageData.GetCustomString("lblMyPercentsDesc")
            ''lnkFamilyExch.Text = CurrentPageData.GetCustomString("lnkFamilyExch")
            ''lblFamilyExchDesc.Text = CurrentPageData.GetCustomString("lblFamilyExchDesc")
            ''lnkMyPayments.Text = CurrentPageData.GetCustomString("lnkMyPayments")
            ''lblMyPaymentsDesc.Text = CurrentPageData.GetCustomString("lblMyPaymentsDesc")
            ''lnkSettings.Text = CurrentPageData.GetCustomString("lnkSettings")
            ''lblSettingsDesc.Text = CurrentPageData.GetCustomString("lblSettingsDesc")
            ''lnkHelperAnswers.Text = CurrentPageData.GetCustomString("lnkHelperAnswers")
            ''lblHelperAnswersDesc.Text = CurrentPageData.GetCustomString("lblHelperAnswersDesc")

            'lnkLearnAbout.NavigateUrl = CurrentPageData.GetCustomString("lnkLearnAbout.NavigateUrl")
            ''lnkHelperAnswers.NavigateUrl = CurrentPageData.GetCustomString("lnkHelperAnswers.NavigateUrl")


            ''lnkBalanceSheet.Text = CurrentPageData.GetCustomString("lnkBalanceSheet")
            ''lblBalanceSheetDesc.Text = CurrentPageData.GetCustomString("lblBalanceSheetDesc")


            'lblFromDate.Text = CurrentPageData.GetCustomString("lblFromDate")
            'lblToDate.Text = CurrentPageData.GetCustomString("lblToDate")
            'btnRefresh.Text = CurrentPageData.GetCustomString("btnRefresh")

            'lblReferrerContactText.Text = CurrentPageData.GetCustomString("lblReferrerContactText")

            'SetControlsValue(Me, CurrentPageData)

            'lblItemsName.Text = Me.CurrentPageData.GetCustomString("CartReferrerView")
            'lnkViewDescription.Text = Me.CurrentPageData.GetCustomString("CartReferrerWhatIsIt")
            'lnkViewDescription.OnClientClick = OnMoreInfoClickFunc(
            '    lnkViewDescription.OnClientClick,
            '    ResolveUrl("~/Members/InfoWin.aspx?info=referrer"),
            '    Me.CurrentPageData.GetCustomString("CartReferrerView"))
            'WhatIsIt1.PopupControl.HeaderText = Me.CurrentPageData.GetCustomString("CartReferrerView")


            'gvBalance.Columns("Currency").Caption = CurrentPageData.GetCustomString("BalanceTable-Currency")
            'gvBalance.Columns("Available").Caption = CurrentPageData.GetCustomString("BalanceTable-Available")
            'gvBalance.Columns("Pending").Caption = CurrentPageData.GetCustomString("BalanceTable-Pending")
            'gvBalance.Columns("Total").Caption = CurrentPageData.GetCustomString("BalanceTable-Total")

            'lblBalanceTableDescr.Text = CurrentPageData.GetCustomString("lblBalanceTableDescr")
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Private Function ValidateInput_Step1() As Boolean
        Try
            If (txtLogin.Text.Trim() = "") Then
                pnlLoginErr.CssClass = "alert alert-danger"
                pnlLoginErr.Visible = True
                lblLoginErr.Text = CurrentPageData.GetCustomString("Error.LoginName.Required")
                Return False
            End If

            txtEmail.Text = txtEmail.Text.Trim()
            If (txtEmail.Text.Trim() = "") Then
                pnlEmailErr.CssClass = "alert alert-danger"
                pnlEmailErr.Visible = True
                lblEmailErr.Text = CurrentPageData.GetCustomString("Error.Email.Required")
                Return False
            End If

            Dim regMatch As Boolean
            If (clsConfigValues.Get__validate_email_address_normal()) Then
                regMatch = Regex.IsMatch(txtEmail.Text, gEmailAddressValidationRegex)
            Else
                regMatch = Regex.IsMatch(txtEmail.Text, gEmailAddressValidationRegex_Simple)
            End If

            If (Not regMatch) Then
                pnlEmailErr.CssClass = "alert alert-danger"
                pnlEmailErr.Visible = True
                lblEmailErr.Text = CurrentPageData.GetCustomString("Error.Email.Invalid")
                Return False
            End If


            If (txtParentCode.Text.Trim() = "") Then
                pnlParentCodeErr.CssClass = "alert alert-danger"
                pnlParentCodeErr.Visible = True
                lblParentCodeErr.Text = CurrentPageData.GetCustomString("Error.InvitationCode.Required")
                Return False
            End If

            If (txtParentCode.Text.Trim() <> "") Then
                Dim r As New Regex(ModGlobals.Referrer_CODE_PATTERN_Check, RegexOptions.IgnoreCase)
                Dim m As Match = r.Match(txtParentCode.Text)
                If (Not m.Success) Then
                    pnlParentCodeErr.CssClass = "alert alert-danger"
                    pnlParentCodeErr.Visible = True
                    lblParentCodeErr.Text = CurrentPageData.GetCustomString("Error.InvitationCode.Invalid")
                    Return False
                Else

                    Dim parentCustomerId As Integer
                    Dim parentCustomerLogin As String
                    parentCustomerId = m.Groups("customer").Value
                    parentCustomerLogin = m.Groups("login").Value.ToLower()

                    Dim profParentLoginName As String = SiteDLL.DataHelpers.GetEUS_Profile_LoginName_ByProfileID(Me.CMSDBDataContext, parentCustomerId)
                    If (Not String.IsNullOrEmpty(profParentLoginName)) Then profParentLoginName = profParentLoginName.ToLower()

                    If (Not String.IsNullOrEmpty(profParentLoginName) AndAlso (profParentLoginName.StartsWith(parentCustomerLogin) OrElse parentCustomerLogin.StartsWith(profParentLoginName))) Then

                    Else

                        pnlParentCodeErr.CssClass = "alert alert-danger"
                        pnlParentCodeErr.Visible = True
                        lblParentCodeErr.Text = CurrentPageData.GetCustomString("Error.InvitationCode.Invalid")
                        Return False

                    End If
                End If
            End If

            If (txtFirstName.Text.Trim() = "") Then
                pnlFirstNameErr.CssClass = "alert alert-danger"
                pnlFirstNameErr.Visible = True
                lblFirstNameErr.Text = CurrentPageData.GetCustomString("Error.FirstName.Required")
                Return False
            End If


            If (txtLastName.Text.Trim() = "") Then
                pnlLastNameErr.CssClass = "alert alert-danger"
                pnlLastNameErr.Visible = True
                lblLastNameErr.Text = CurrentPageData.GetCustomString("Error.LastName.Required")
                Return False
            End If


            If (txtPass.Text.Trim() = "") Then
                pnlPassErr.CssClass = "alert alert-danger"
                pnlPassErr.Visible = True
                lblPassErr.Text = CurrentPageData.GetCustomString("Error.Password.Required")
                upPass.Update()

                Return False
            End If

            If (txtPassConfirm.Text.Trim() = "") Then
                pnlPassConfirmErr.CssClass = "alert alert-danger"
                pnlPassConfirmErr.Visible = True
                lblPassConfirmErr.Text = CurrentPageData.GetCustomString("Error.Password.Confirm.Required")

                upPassConfirm.Update()
                Return False
            End If

            If (txtPass.Text <> txtPassConfirm.Text) Then
                pnlPassErr.CssClass = "alert alert-danger"
                pnlPassErr.Visible = True
                lblPassErr.Text = CurrentPageData.GetCustomString("Error.Password.Confirm.Invalid")

                upPass.Update()
                Return False
            End If



            ' check user name and email availability

            Dim profile As ReferralsDLL.REF_ReferrerProfile

            ' check email
            profile = (From itm In Me.REF_CMSDBDataContext.REF_ReferrerProfiles
                      Where itm.eMail.ToUpper() = txtEmail.Text.ToUpper() AndAlso _
                          itm.Status <> ProfileStatusEnum.DeletedByUser
                      Select itm).FirstOrDefault()

            'Me.CMSDBDataContext.EUS_Profiles.FirstOrDefault(Function(itm As EUS_Profile) itm.eMail.ToUpper() = txtEmail.Text.ToUpper())
            If (profile IsNot Nothing) Then
                pnlEmailErr.CssClass = "alert alert-danger"
                pnlEmailErr.Visible = True
                lblEmailErr.Text = CurrentPageData.GetCustomString("txtEmail_EmailAlreadyInUse_ErrorText")
                Return False
            End If

            profile = Nothing

            ' check user name
            profile = Me.REF_CMSDBDataContext.REF_ReferrerProfiles.FirstOrDefault(Function(itm As ReferralsDLL.REF_ReferrerProfile) itm.LoginName.ToUpper() = txtLogin.Text.ToUpper())
            If (profile IsNot Nothing) Then
                pnlLoginErr.CssClass = "alert alert-danger"
                pnlLoginErr.Visible = True
                lblLoginErr.Text = CurrentPageData.GetCustomString("txtEmail_UserNameAlreadyInUse_ErrorText")
                Return False
            End If


            Return True

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Return False
    End Function


    Private Function ValidateInput_Step2() As Boolean
        Return chkAgree.Checked
    End Function


    Private Function ValidateInput_Step3() As Boolean
        Return True
    End Function


    Protected Sub btnSubmitStep1_Click(sender As Object, e As EventArgs) Handles btnSubmitStep1.Click
        Try
            If (ValidateInput_Step1()) Then
                CreateReferral()
            End If
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Step 1")
        End Try
    End Sub


    Private Sub CreateReferral()




        Try
            Dim success As Boolean
            Dim parentCustomerId As Integer
            Dim parentCustomerLogin As String = ""

            txtParentCode.Text = txtParentCode.Text.Trim()

            If (txtParentCode.Text.Trim() <> "") Then
                Dim r As New Regex(ModGlobals.Referrer_CODE_PATTERN_Check, RegexOptions.IgnoreCase)
                Dim m As Match = r.Match(txtParentCode.Text)
                parentCustomerId = m.Groups("customer").Value
                parentCustomerLogin = m.Groups("login").Value.ToLower()
            End If


            Using ds = ReferralsDLL.DataHelpers.GetREF_ReferrerProfiles_ByLoginNameOrEmail(txtEmail.Text.Trim())



                If (ds.REF_ReferrerProfiles.Rows.Count > 0) Then
                    'do nothing
                Else

                    Dim dr As ReferralsDLL.DSReferrers.REF_ReferrerProfilesRow = ds.REF_ReferrerProfiles.NewREF_ReferrerProfilesRow()
                    dr.DateTimeToRegister = DateTime.UtcNow
                    dr.FirstName = txtFirstName.Text.Trim()
                    dr.GenderId = 2
                    dr.Status = ProfileStatusEnum.NewProfile
                    dr.LAGID = Session("LagID")
                    dr.LastLoginDateTime = DateTime.UtcNow
                    dr.LastLoginIP = Request.ServerVariables("REMOTE_ADDR")
                    dr.LastName = txtLastName.Text.Trim()
                    'dr.LastUpdateProfileDateTime = DateTime.UtcNow
                    'dr.LastUpdateProfileIP = Request.ServerVariables("REMOTE_ADDR")
                    dr.LoginName = txtLogin.Text.Trim()
                    dr.Password = txtPass.Text.Trim()
                    dr.RegisterIP = Request.ServerVariables("REMOTE_ADDR")
                    dr.RegisterUserAgent = Request.ServerVariables("HTTP_USER_AGENT")
                    dr.Telephone = txtPhone.Text.Trim()
                    dr.FacebookUserName = txtFacebook.Text.Trim()
                    dr.eMail = txtEmail.Text.Trim()
                    dr.Role = "MEMBER"
                    dr.REF_FullParentCode = txtParentCode.Text
                    dr.Country = ""
                    dr._Region = ""
                    dr.City = ""

                    If (cbCountry.SelectedItem IsNot Nothing AndAlso cbCountry.SelectedItem.Value <> "-1") Then
                        dr.Country = cbCountry.SelectedItem.Value
                    End If

                    If (cbRegion.SelectedItem IsNot Nothing AndAlso cbRegion.SelectedItem.Value <> "-1") Then
                        dr._Region = cbRegion.SelectedItem.Value
                    End If

                    If (cbCity.SelectedItem IsNot Nothing AndAlso cbCity.SelectedItem.Value <> "-1") Then
                        dr.City = cbCity.SelectedItem.Value
                    End If

                    ' get parent referrer
                    Dim profParentLoginName As String = SiteDLL.DataHelpers.GetEUS_Profile_LoginName_ByProfileID(Me.CMSDBDataContext, parentCustomerId)
                    profParentLoginName = profParentLoginName.ToLower()
                    If (Not String.IsNullOrEmpty(profParentLoginName) AndAlso (profParentLoginName.StartsWith(parentCustomerLogin) OrElse parentCustomerLogin.StartsWith(profParentLoginName))) Then
                        dr.ReferrerParentId = parentCustomerId
                    End If

                    ' get matched member
                    Dim profMember As EUS_Profile = SiteDLL.DataHelpers.GetEUS_ProfileByLoginNameOrEmail(Me.CMSDBDataContext, dr.eMail)
                    If (profMember Is Nothing) Then
                        profMember = DataHelpers.GetEUS_ProfileByLoginNameOrEmail(Me.CMSDBDataContext, dr.LoginName)
                    End If

                    If (profMember IsNot Nothing) Then

                        ' SiteProfileID should have master profiled id value, where ismaster = true
                        dr.SiteProfileID = SiteDLL.DataHelpers.EUS_Profiles_GetMasterProfileID(profMember.ProfileID)
                        If (dr.SiteProfileID = profMember.ProfileID) Then
                            dr.SiteMirrorProfileID = profMember.MirrorProfileID
                        Else
                            dr.SiteMirrorProfileID = profMember.ProfileID
                        End If
                        dr.SiteLoginName = profMember.LoginName

                        If (profMember.Address IsNot Nothing) Then dr.Address = profMember.Address
                        If (profMember.Cellular IsNot Nothing) Then dr.Cellular = profMember.Cellular
                        If (profMember.CustomReferrer IsNot Nothing) Then dr.CustomReferrer = profMember.CustomReferrer
                        If (profMember.Zip IsNot Nothing) Then dr.Zip = profMember.Zip

                        If (dr.IsCountryNull() OrElse dr.Country = "") Then
                            If (profMember.Country IsNot Nothing) Then dr.Country = profMember.Country
                        End If
                        If (dr.Is_RegionNull() OrElse dr._Region = "") Then
                            If (profMember.Region IsNot Nothing) Then dr._Region = profMember.Region
                        End If
                        If (dr.IsCityNull() OrElse dr.City = "") Then
                            If (profMember.City IsNot Nothing) Then dr.City = profMember.City
                        End If

                        If (dr.IsFacebookUserNameNull() OrElse dr.FacebookUserName = "") Then
                            If (profMember.FacebookName IsNot Nothing) Then dr.FacebookName = profMember.FacebookName
                            If (profMember.FacebookUserId IsNot Nothing) Then dr.FacebookUserId = profMember.FacebookUserId
                            If (profMember.FacebookUserName IsNot Nothing) Then dr.FacebookUserName = profMember.FacebookUserName
                        End If

                        If (dr.IsTelephoneNull() OrElse dr.Telephone = "") Then
                            If (profMember.Telephone IsNot Nothing) Then dr.Telephone = profMember.Telephone
                        End If

                        SiteDLL.DataHelpers.UpdateEUS_Profiles_MirrorSetStatusUpdating(dr.SiteProfileID, ProfileStatusEnum.Updating, ProfileModifiedEnum.None)

                        success = True
                    End If

                    ds.REF_ReferrerProfiles.AddREF_ReferrerProfilesRow(dr)
                    ReferralsDLL.DataHelpers.UpdateREF_ReferrerProfiles(ds)


                    Try

                        SendNotificationEmailToSupport_ReferralCreated(dr, profMember, False)
                    Catch ex As Exception
                        WebErrorMessageBox(Me, ex, "Step 1->Create Referral->Send Notification email")
                    End Try

                End If


                Dim DataRecordLoginReturn As ReferralsDLL.clsDataRecordLoginMemberReturn = gLogin.PerfomSiteLogin(txtLogin.Text, txtPass.Text, False, False)
                If DataRecordLoginReturn.IsValid Then

                    Session("SuccesStep") = 1
                    Response.Redirect(ResolveUrl("~/Register.aspx?step=2"))

                Else
                    clsCurrentContext.ClearSession(Session)
                    FormsAuthentication.SignOut()

                    pnlCommonErr.CssClass = "alert alert-danger"
                    pnlCommonErr.Visible = True
                    lblCommonErr.Text = CurrentPageData.GetCustomString("Error.Check.Login.Pass")
                End If
            End Using
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Step 1->Create Referral")
        Finally

        End Try

    End Sub

    Protected Sub btnSubmitStep2_Click(sender As Object, e As EventArgs) Handles btnSubmitStep2.Click
        Try
            If (ValidateInput_Step2()) Then
                'DataHelpers.EUS_Profiles_UpdateREF_RegStep(Me.MasterProfileId, 2)
                Session("SuccesStep") = 2

                If (Me.MasterProfileId = 0) Then
                    'Response.Redirect(ResolveUrl("~/Members/"))
                    Dim rp As New RemotePost()
                    rp.Target = "_top"
                    rp.Method = "get"
                    rp.Url = ResolveUrl("~/Members/")
                    rp.Post()
                Else
                    Response.Redirect(ResolveUrl("~/Register.aspx?step=3"))
                End If
            End If
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Step 2")
        End Try
    End Sub


    Protected Sub btnSubmitStep3_Click(sender As Object, e As EventArgs) Handles btnSubmitStep3.Click
        Try

            Dim ds As ReferralsDLL.DSReferrers = ReferralsDLL.DataHelpers.REF_CustomerVerificationDocs_GetByCustomerID(Me.ReferrerCustomerID)
            If (ds.REF_CustomerVerificationDocs.Rows.Count > 0) Then
                Dim drs As ReferralsDLL.DSReferrers.REF_CustomerVerificationDocsRow() = ds.REF_CustomerVerificationDocs.Select("DocumentType='IDFrontPage'")

                If (drs.Length > 0) Then
                    'drs = ds.EUS_CustomerVerificationDocs.Select("DocumentType='IDBackPage'")

                    'If (drs.Length > 0) Then
                    'DataHelpers.EUS_Profiles_UpdateREF_RegStep(Me.MasterProfileId, 3)
                    Session("SuccesStep") = 3
                    'End If
                End If
            End If
            'Response.Redirect(ResolveUrl("~/Members/"))
            Dim rp As New RemotePost()
            rp.Target = "_top"
            rp.Method = "get"
            rp.Url = ResolveUrl("~/Members/")
            rp.Post()

        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Step 3")
        End Try
    End Sub


    Protected Sub mvReg_ActiveViewChanged(sender As Object, e As EventArgs) Handles mvReg.ActiveViewChanged
        Try
            If (mvReg.GetActiveView() Is vwIDNum) Then

            ElseIf (mvReg.GetActiveView() Is vwAgreement) Then

            ElseIf (mvReg.GetActiveView() Is vwUpload) Then

                Dim ds As ReferralsDLL.DSReferrers = ReferralsDLL.DataHelpers.REF_CustomerVerificationDocs_GetByCustomerID(Me.ReferrerCustomerID)
                'Return 
                If (ds.REF_CustomerVerificationDocs.Rows.Count > 0) Then

                    Dim drs As ReferralsDLL.DSReferrers.REF_CustomerVerificationDocsRow() = ds.REF_CustomerVerificationDocs.Select("DocumentType='IDFrontPage'")
                    If (drs.Length > 0) Then
                        uplFrontPage.Enabled = False
                        btnUploadFrontPage.Enabled = False
                    End If

                    drs = ds.REF_CustomerVerificationDocs.Select("DocumentType='IDBackPage'")
                    If (drs.Length > 0) Then
                        uplBackPage.Enabled = False
                        btnUploadBackPage.Enabled = False
                    End If

                    drs = ds.REF_CustomerVerificationDocs.Select("DocumentType='IDThirdPage'")
                    If (drs.Length > 0) Then
                        uplThirdPage.Enabled = False
                        btnUploadThirdPage.Enabled = False
                    End If
                End If
            End If

        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "View changed")
        End Try
    End Sub


    Protected Sub uplFrontPage_FileUploadComplete(sender As Object, e As DevExpress.Web.ASPxUploadControl.FileUploadCompleteEventArgs) Handles uplFrontPage.FileUploadComplete
        e.CallbackData = SavePostedFile(e.UploadedFile, "ID Front page", "IDFrontPage")
    End Sub

    Protected Sub uplBackPage_FileUploadComplete(sender As Object, e As DevExpress.Web.ASPxUploadControl.FileUploadCompleteEventArgs) Handles uplBackPage.FileUploadComplete
        e.CallbackData = SavePostedFile(e.UploadedFile, "ID Back page", "IDBackPage")
    End Sub

    Protected Sub uplThirdPage_FileUploadComplete(sender As Object, e As DevExpress.Web.ASPxUploadControl.FileUploadCompleteEventArgs) Handles uplThirdPage.FileUploadComplete
        e.CallbackData = SavePostedFile(e.UploadedFile, "ID Third page", "IDThirdPage")
    End Sub


    Private Function SavePostedFile(ByVal uploadedFile As DevExpress.Web.ASPxUploadControl.UploadedFile, description As String, docType As String) As String
        Session("VerDoc_Uploaded") = Nothing
        If (Not uploadedFile.IsValid) Then
            Return String.Empty
        End If


        Dim imgfilePath As String = ""
        Dim thumbfilePath As String = ""

        Dim _guid As Guid = Guid.NewGuid()

        Dim ext As String = uploadedFile.FileName
        ext = ext.Substring(ext.LastIndexOf(".") + 1)

        Dim filePath As String = _guid.ToString("N") & "." & ext


        Try

            Dim imgDir As String = String.Format(ReferralsDLL.ProfileHelper.gMemberDocsDirectory, Me.ReferrerCustomerID.ToString())
            imgDir = MapPath(imgDir)
            If Not System.IO.Directory.Exists(imgDir) Then
                System.IO.Directory.CreateDirectory(imgDir)
            End If
            imgfilePath = Path.Combine(imgDir, filePath)

            Dim buffer As Byte() = New Byte(uploadedFile.ContentLength) {}
            'uploadedFile.FileContent.Read(buffer, 0, uploadedFile.ContentLength)

            Dim original As FileInfo = New FileInfo(imgfilePath)
            Dim fileStream As FileStream = original.OpenWrite()
            Try
                uploadedFile.FileContent.CopyTo(fileStream)
                fileStream.Write(buffer, 0, buffer.Length)
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "Save verification doc failed (1).")
            Finally
                fileStream.Close()
                fileStream.Dispose()
            End Try


            Dim ver As ReferralsDLL.REF_CustomerVerificationDoc = New ReferralsDLL.REF_CustomerVerificationDoc()
            ver.CustomerID = Me.ReferrerCustomerID
            ver.DateTimeToUploading = DateTime.UtcNow
            ver.Description = description

            If (ver.Description.Length > 1024) Then
                ver.Description = ver.Description.Remove(1024)
            End If
            ver.DocumentType = docType
            ver.FileName = filePath
            ver.OriginalName = uploadedFile.FileName

            Me.REF_CMSDBDataContext.REF_CustomerVerificationDocs.InsertOnSubmit(ver)
            Me.REF_CMSDBDataContext.SubmitChanges()


            ReferralsDLL.DataHelpers.UpdateREF_ReferrerProfiles_SetStatusUpdating(Me.ReferrerCustomerID, ProfileStatusEnum.Updating)


            Dim profileRows As New clsProfileRows(Me.MasterProfileId)
            Try
                SendNotificationEmailToSupport_DocumentUploaded(profileRows.GetMirrorRow(), ver)
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try

            Session("VerDoc_Uploaded") = docType & "_success"
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Save verification doc failed (2).")
            Session("VerDoc_Uploaded") = docType & "_fail"
        End Try

        If (Session("VerDoc_Uploaded") Is Nothing) Then
            Return "false"
        Else
            Return Session("VerDoc_Uploaded").ToString().Replace(docType & "_", "")
        End If
    End Function


    Protected Sub cbCountry_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbCountry.SelectedIndexChanged
        Try

            cbRegion.DataSourceID = ""
            cbRegion.DataSource = clsGeoHelper.GetCountryRegions(cbCountry.SelectedItem.Value, Session("LAGID"))
            cbRegion.TextField = "region1"
            cbRegion.DataBind()

            Dim def2 As DevExpress.Web.ASPxEditors.ListEditItem = New DevExpress.Web.ASPxEditors.ListEditItem(CurrentPageData.GetCustomString("cbRegion_default"), "-1")
            def2.Selected = True
            cbRegion.Items.Insert(0, def2)

            upRegion.Update()
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorSendEmail(ex, "cbCountry changed")
        End Try
    End Sub

    Protected Sub cbRegion_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbRegion.SelectedIndexChanged
        Try
            Dim a As String = cbRegion.Value

            cbCity.DataSourceID = ""
            cbCity.DataSource = clsGeoHelper.GetCountryRegionCities(cbCountry.SelectedItem.Value, a, Session("LAGID"))
            cbCity.TextField = "city"
            cbCity.DataBind()


            Dim def2 As DevExpress.Web.ASPxEditors.ListEditItem = New DevExpress.Web.ASPxEditors.ListEditItem(CurrentPageData.GetCustomString("cbCity_default"), "-1")
            def2.Selected = True
            cbCity.Items.Insert(0, def2)

            upCity.Update()
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorSendEmail(ex, "cbRegion changed")
        End Try
    End Sub





    Sub SendNotificationEmailToSupport_ReferralCreated(refRow As ReferralsDLL.DSReferrers.REF_ReferrerProfilesRow, mirrorRow As SiteDSDLL.EUS_Profile, isAutoApproveOn As Boolean)
        Try

            Dim toEmail As String = ConfigurationManager.AppSettings("gToEmail")
            Dim Content As String = globalStrings.GetCustomString("Referrer.EmailSendToSupport_MemberNew", "US")

            If (mirrorRow IsNot Nothing) Then
                Content = Content.Replace("###LOGINNAME_MATCH###", IIf(mirrorRow.LoginName = refRow.LoginName, "YES", "NO"))
                Content = Content.Replace("###PASSWORD_MATCH###", IIf(mirrorRow.Password = refRow.Password, "YES", "NO"))
                Content = Content.Replace("###EMAIL_MATCH###", IIf(mirrorRow.eMail = refRow.eMail, "YES", "NO"))
            Else
                Content = Content.Replace("###LOGINNAME_MATCH###", "NOT FOUND")
                Content = Content.Replace("###PASSWORD_MATCH###", "NOT FOUND")
                Content = Content.Replace("###EMAIL_MATCH###", "NOT FOUND")
            End If

            Content = Content.Replace("###YESNO###", IIf(isAutoApproveOn, "YES", "NO"))

            Content = Content.Replace("###FIRSTNAME###", refRow.FirstName)
            Content = Content.Replace("###LASTNAME###", refRow.LastName)
            Content = Content.Replace("###LOGINNAME###", refRow.LoginName)
            Content = Content.Replace("###EMAIL###", refRow.eMail)

            Dim approveUrl As String = ConfigurationManager.AppSettings("gApproveProfileURL")
            Dim rejectUrl As String
            approveUrl = String.Format(approveUrl, refRow.LoginName)
            rejectUrl = approveUrl & "&reject=1"

            'If (isAutoApproveOn) Then
            '    Content = Content.Replace("###APPROVEPROFILEURL###", "")
            '    Content = Content.Replace("###REJECTPROFILEURL###", "")
            'Else
            Content = Content.Replace("###APPROVEPROFILEURL###", approveUrl)
            Content = Content.Replace("###REJECTPROFILEURL###", rejectUrl)
            'End If


            Try
                Content = Content.Replace("###GENDER###", ProfileHelper.GetGenderString(refRow.GenderId, "US"))
                Content = Content.Replace("###SEX###", ProfileHelper.GetGenderString(refRow.GenderId, "US"))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###COUNTRY###", ProfileHelper.GetCountryName(refRow.Country))
            Catch ex As Exception
                Content = Content.Replace("###COUNTRY###", "NOT FOUND")
            End Try
            Try
                Content = Content.Replace("###STATEREGION###", refRow._Region)
            Catch ex As Exception
                Content = Content.Replace("###STATEREGION###", "NOT FOUND")
            End Try
            Try
                Content = Content.Replace("###CITY###", refRow.City)
            Catch ex As Exception
                Content = Content.Replace("###CITY###", "NOT FOUND")
            End Try
            Try
                Content = Content.Replace("###ZIP###", refRow.Zip)
            Catch ex As Exception
                Content = Content.Replace("###ZIP###", "NOT FOUND")
            End Try
            Try
                Content = Content.Replace("###DATETOREGISTER###", refRow.DateTimeToRegister)
            Catch ex As Exception
            End Try


            Try
                Content = Content.Replace("###PROFILEAPPROVEDYESNO###", IIf(refRow.Status = 4, "YES", "NO"))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###APPROVEDPHOTOSYESNO###", "NO")
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###AUTOAPPROVEDPHOTOYESNO###", "NO")
            Catch ex As Exception
            End Try


            Try
                Content = Content.Replace("###IP###", Dating.Server.Core.DLL.clsHTMLHelper.CreateIPLookupLinks(Session("IP")))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###GEOIP###", Session("GEO_COUNTRY_CODE"))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###AGENT###", Request.Params("HTTP_USER_AGENT"))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###REFERRER###", Dating.Server.Core.DLL.clsHTMLHelper.CreateURLLink(IIf(Session("Referrer") IsNot Nothing, Session("Referrer"), "")))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###CUSTOMREFERRER###", Session("CustomReferrer"))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###CUSTOMERID###", refRow.CustomerID)
            Catch ex As Exception
            End Try

            Try
                Dim link As String = ConfigurationManager.AppSettings("gSiteURL") & _
                    "?logon_customer=" & mirrorRow.MirrorProfileID & "_" & mirrorRow.ProfileID
                Content = Content.Replace("###LOGONCUSTOMER###", link)
            Catch ex As Exception
            End Try

            Try
                Dim SearchEngineKeywords As String = IIf(Session("SearchEngineKeywords") Is Nothing, "", Session("SearchEngineKeywords"))
                Content = Content.Replace("###SEARCHENGINEKEYWORDS###", SearchEngineKeywords)
            Catch ex As Exception
            End Try


            Try
                Dim DateTimeToRegister As String = ""
                If (Not refRow.IsDateTimeToRegisterNull()) Then
                    DateTimeToRegister = refRow.DateTimeToRegister.ToLocalTime().ToString("dd/MM/yyyy HH:mm:ss")
                End If
                Content = Content.Replace("###REGDATE###", DateTimeToRegister)
            Catch ex As Exception
            End Try



            Try
                Content = Content.Replace("###SITE-LOGINNAME###", mirrorRow.LoginName)
            Catch ex As Exception
                Content = Content.Replace("###SITE-LOGINNAME###", "NOT FOUND")
            End Try
            Try
                Content = Content.Replace("###SITE-EMAIL###", mirrorRow.eMail)
            Catch ex As Exception
                Content = Content.Replace("###SITE-EMAIL###", "NOT FOUND")
            End Try
            Try
                Dim Birthday As String = ""
                If (mirrorRow.Birthday.HasValue) Then
                    Birthday = mirrorRow.Birthday.Value.ToLocalTime().ToString("dd/MM/yyyy HH:mm:ss")
                End If
                Content = Content.Replace("###SITE-BIRTHDATE###", Birthday)
            Catch ex As Exception
                Content = Content.Replace("###SITE-BIRTHDATE###", "NOT FOUND")
            End Try
            Try
                Content = Content.Replace("###SITE-AGE###", ProfileHelper.GetCurrentAge(mirrorRow.Birthday))
            Catch ex As Exception
                Content = Content.Replace("###SITE-AGE###", "NOT FOUND")
            End Try
            Try
                Content = Content.Replace("###SITE-GENDER###", ProfileHelper.GetGenderString(mirrorRow.GenderId, "US"))
                Content = Content.Replace("###SITE-SEX###", ProfileHelper.GetGenderString(mirrorRow.GenderId, "US"))
            Catch ex As Exception
                Content = Content.Replace("###SITE-GENDER###", "NOT FOUND")
                Content = Content.Replace("###SITE-SEX###", "NOT FOUND")
            End Try
            Try
                Content = Content.Replace("###SITE-COUNTRY###", ProfileHelper.GetCountryName(mirrorRow.Country) & " (" & mirrorRow.Country & ")")
            Catch ex As Exception
                Try
                    Content = Content.Replace("###SITE-COUNTRY###", mirrorRow.Country)
                Catch ex1 As Exception
                    Content = Content.Replace("###SITE-COUNTRY###", "NOT FOUND")
                End Try
            End Try

            Try
                Content = Content.Replace("###SITE-STATEREGION###", mirrorRow.Region)
            Catch ex As Exception
                Content = Content.Replace("###SITE-STATEREGION###", "NOT FOUND")
            End Try

            Try
                Content = Content.Replace("###SITE-CITY###", mirrorRow.City)
            Catch ex As Exception
                Content = Content.Replace("###SITE-CITY###", "NOT FOUND")
            End Try

            Try
                Content = Content.Replace("###SITE-ZIP###", mirrorRow.Zip)
            Catch ex As Exception
                Content = Content.Replace("###SITE-ZIP###", "NOT FOUND")
            End Try

            clsMyMail.SendMail(ConfigurationManager.AppSettings("gToEmail"), "New user REGISTRATION on " & ConfigurationManager.AppSettings("gSiteName") & ".", Content, True)

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub



    Sub SendNotificationEmailToSupport_DocumentUploaded(mirrorRow As DSMembers.EUS_ProfilesRow, ver As ReferralsDLL.REF_CustomerVerificationDoc)
        Try

            Dim toEmail As String = ConfigurationManager.AppSettings("gToEmail")
            Dim Content As String = globalStrings.GetCustomString("EmailSendToSupport_MemberNewVerificationDoc", "US")


            Content = Content.Replace("###LOGINNAME###", mirrorRow.LoginName)
            Content = Content.Replace("###EMAIL###", mirrorRow.eMail)


            Try
                Content = Content.Replace("###FILENAME###", ver.OriginalName)
            Catch ex As Exception
            End Try

            Try
                Dim config As New ReferralsDLL.clsConfigValues()
                Dim photoUrl As String = String.Format(config.gref_documents_url_path, ver.CustomerID, ver.FileName)
                'Dim siteUrl As String = ConfigurationManager.AppSettings("gSiteURL")
                'Dim photoUrl As String = ""
                'photoUrl = String.Format(ReferralsDLL.ProfileHelper.gMemberDocsDirectoryView, ver.CustomerID) & ver.FileName
                'photoUrl = System.Web.VirtualPathUtility.ToAbsolute(photoUrl)
                'photoUrl = (siteUrl & photoUrl)
                'photoUrl = ProfileHelper.GetProfileImageURL(newPhoto.CustomerID, newPhoto.FileName, mirrorRow.GenderId, True)

                Content = Content.Replace("###FILEPATH###", photoUrl)
                Content = Content.Replace("blank###PHOTOURL###", photoUrl) ' cms editor issue, adds word blank
                Content = Content.Replace("###PHOTOURL###", photoUrl)
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "Verification doc")
            End Try



            Try
                Content = Content.Replace("###BIRTHDATE###", mirrorRow.Birthday)
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###AGE###", ProfileHelper.GetCurrentAge(mirrorRow.Birthday))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###GENDER###", ProfileHelper.GetGenderString(mirrorRow.GenderId, "US"))
                Content = Content.Replace("###SEX###", ProfileHelper.GetGenderString(mirrorRow.GenderId, "US"))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###COUNTRY###", ProfileHelper.GetCountryName(mirrorRow.Country))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###STATEREGION###", mirrorRow._Region)
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###CITY###", mirrorRow.City)
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###ZIP###", mirrorRow.Zip)
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###DATETOREGISTER###", mirrorRow.DateTimeToRegister)
            Catch ex As Exception
            End Try


            Try
                Content = Content.Replace("###PROFILEAPPROVEDYESNO###", IIf(mirrorRow.Status = 4, "YES", "NO"))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###IP###", Dating.Server.Core.DLL.clsHTMLHelper.CreateIPLookupLinks(Session("IP")))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###GEOIP###", Session("GEO_COUNTRY_CODE"))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###AGENT###", Request.Params("HTTP_USER_AGENT"))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###REFERRER###", Dating.Server.Core.DLL.clsHTMLHelper.CreateURLLink(IIf(Session("Referrer") IsNot Nothing, Session("Referrer"), "")))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###CUSTOMREFERRER###", Session("CustomReferrer"))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###CUSTOMERID###", mirrorRow.ProfileID)
            Catch ex As Exception
            End Try
            Try
                Dim link As String = ConfigurationManager.AppSettings("gSiteURL") & _
                    "?logon_customer=" & mirrorRow.MirrorProfileID & "_" & mirrorRow.ProfileID
                Content = Content.Replace("###LOGONCUSTOMER###", link)
            Catch ex As Exception
            End Try


            Try
                Dim SearchEngineKeywords As String = IIf(Session("SearchEngineKeywords") Is Nothing, "", Session("SearchEngineKeywords"))
                Content = Content.Replace("###SEARCHENGINEKEYWORDS###", SearchEngineKeywords)
            Catch ex As Exception
            End Try


            Try
                Dim LandingPage As String = ""
                If (Not mirrorRow.IsLandingPageNull()) Then
                    LandingPage = mirrorRow.LandingPage
                    If (LandingPage Is Nothing) Then LandingPage = ""
                End If
                Content = Content.Replace("###LANDINGPAGE###", Dating.Server.Core.DLL.clsHTMLHelper.CreateURLLink(LandingPage))
            Catch ex As Exception
            End Try


            clsMyMail.SendMail(ConfigurationManager.AppSettings("gToEmail"), "New verification doc uploaded on " & ConfigurationManager.AppSettings("gSiteName"), Content, True)

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub



End Class
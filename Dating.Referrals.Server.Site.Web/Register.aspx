﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Root.Master" CodeBehind="Register.aspx.vb" Inherits="Dating.Referrals.Server.Site.Web.Register" %>
<%@ Register Src="UserControls/DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<%@ Register Assembly="DevExpress.Web.v13.1, Version=13.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxUploadControl" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<style type="text/css">
.peLabel-reg{font-size:14px;}
.register-form{font-size:14px;background-color:#DAD8D9;padding:20px;width:325px;}
.form-title{font-size:22px;margin:0;margin-bottom:10px;padding-left:20px;font-weight:normal;}
.form_right{border-left:5px solid #FFCC33;margin-bottom:10px;}
.form_right .dxeTextBox, .form_right .dxeMemo{border:none;}
.form_right .dxeTextBox .dxeEditArea{height:22px !important; width:300px;}
.check-box label{margin-left:5px;vertical-align:middle;}
.check-box input{vertical-align:middle;}
.upload-success{background:#00ee00;color:#000;display:none;padding:3px 5px;}
.agreement-text{width:730px;height:400px;overflow:auto;margin-left:auto;margin-left:auto;border-left:5px solid #FFCC33;font-size:14px;background-color:#fff;color:#000;text-align:left;padding:15px 10px;}

ul.ref-register-pros{width:270px;padding-left:0px;}
ul.ref-register-pros li{   
     list-style: none;
     background:url('../../Images2/star.png') no-repeat left top;
     padding-left:25px;
     text-align:left;
     margin-bottom:15px;
     font-size: 13px;
}

</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">

<div style="width:800px;margin:0 auto 50px;">

    <asp:MultiView ID="mvReg" runat="server" ActiveViewIndex="0">
        <asp:View ID="vwIDNum" runat="server">

    <%--<asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>--%>

<div class="lfloat">
    <h2 class="form-title">Register</h2>
    <div class="register-form">
        <asp:Panel ID="pnlCommonErr" runat="server" CssClass="alert alert-danger" Visible="False" EnableViewState="false">
                                    <asp:Label ID="lblCommonErr" runat="server" Text="" /></asp:Panel>
        <table class="ref-reg">
            <tr>
                <td></td>
                <td><asp:Panel ID="pnlLoginErr" runat="server" CssClass="alert alert-danger" Visible="False" EnableViewState="false">
                                    <asp:Label ID="lblLoginErr" runat="server" Text="" /></asp:Panel>
                
				    <asp:Label ID="msg_Login" runat="server" Text="Login Name" CssClass="peLabel-reg "/>*

                    <div class="form_right">
                        <dx:ASPxTextBox ID="txtLogin" runat="server" autocomplete="off" />
				    </div></td>
                <td>
                    <asp:Label ID="msg_LoginInfo" runat="server" Text="" CssClass="peLabel-reg"/>
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                        <asp:UpdatePanel ID="upPass" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                <asp:Panel ID="pnlPassErr" runat="server" CssClass="alert alert-danger" Visible="False" EnableViewState="false">
                                    <asp:Label ID="lblPassErr" runat="server" Text="" /></asp:Panel>
				    <asp:Label ID="msg_Pass" runat="server" Text="Password" CssClass="peLabel-reg "/>*
                    <div class="form_right">
                        <dx:ASPxTextBox ID="txtPass" runat="server" autocomplete="off" 
                            Password="True"  />
				    </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                <td>
                    <asp:Label ID="msg_PassInfo" runat="server" Text="" CssClass="peLabel-reg"/>
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                        <asp:UpdatePanel ID="upPassConfirm" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                <asp:Panel ID="pnlPassConfirmErr" runat="server" CssClass="alert alert-danger" Visible="False" EnableViewState="false">
                                    <asp:Label ID="lblPassConfirmErr" runat="server" Text="" /></asp:Panel>
				    <asp:Label ID="msg_PassConfirm" runat="server" Text="Password confirmation" CssClass="peLabel-reg "/>*
                    <div class="form_right">
                        <dx:ASPxTextBox ID="txtPassConfirm" runat="server" autocomplete="off" 
                            Password="True"  />
				    </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                <td>
                    <asp:Label ID="msg_PassConfirmInfo" runat="server" Text="" CssClass="peLabel-reg"/>
                </td>
            </tr>
            <tr>
                <td></td>
                <td><asp:Panel ID="pnlEmailErr" runat="server" CssClass="alert alert-danger" Visible="False" EnableViewState="false">
                                    <asp:Label ID="lblEmailErr" runat="server" Text="" /></asp:Panel>
				    <asp:Label ID="msg_Email" runat="server" Text="Email Address" CssClass="peLabel-reg "/>*
                    <div class="form_right">
                        <dx:ASPxTextBox ID="txtEmail" runat="server" autocomplete="off" />
				    </div></td>
                <td>
                    <asp:Label ID="msg_EmailInfo" runat="server" Text="" CssClass="peLabel-reg"/>
                </td>
            </tr>
            <tr>
                <td></td>
                <td><asp:Panel ID="pnlParentCodeErr" runat="server" CssClass="alert alert-danger" Visible="False" EnableViewState="false">
                                    <asp:Label ID="lblParentCodeErr" runat="server" Text="" /></asp:Panel>
				    <asp:Label ID="msg_ParentCode" runat="server" Text="Invitation Code" CssClass="peLabel-reg "/>*
                    <div class="form_right">
                        <dx:ASPxTextBox ID="txtParentCode" runat="server" autocomplete="off" />
				    </div></td>
                <td>
                    <asp:Label ID="msg_ParentCodeInfo" runat="server" Text="" CssClass="peLabel-reg"/>
                </td>
            </tr>
            <tr>
                <td></td>
                <td><asp:Panel ID="pnlFirstNameErr" runat="server" CssClass="alert alert-danger" Visible="False" EnableViewState="false">
                                    <asp:Label ID="lblFirstNameErr" runat="server" Text="" /></asp:Panel>
				    <asp:Label ID="msg_FirstName" runat="server" Text="First Name" CssClass="peLabel-reg "/>*
                    <div class="form_right">
                        <dx:ASPxTextBox ID="txtFirstName" runat="server" autocomplete="off" />
				    </div></td>
                <td>
                    <asp:Label ID="msg_FirstNameInfo" runat="server" Text="" CssClass="peLabel-reg"/>
                </td>
            </tr>
            <tr>
                <td></td>
                <td><asp:Panel ID="pnlLastNameErr" runat="server" CssClass="alert alert-danger" Visible="False" EnableViewState="false">
                                    <asp:Label ID="lblLastNameErr" runat="server" Text="" /></asp:Panel>
				    <asp:Label ID="msg_LastName" runat="server" Text="Last Name" CssClass="peLabel-reg "/>*
                    <div class="form_right">
                        <dx:ASPxTextBox ID="txtLastName" runat="server" autocomplete="off" />
				    </div></td>
                <td>
                    <asp:Label ID="msg_LastNameInfo" runat="server" Text="" CssClass="peLabel-reg"/>
                </td>
            </tr>
            <tr>
                <td></td>
                <td><asp:Label ID="msg_CountryText" runat="server" Text="Country" CssClass="peLabel-reg" /> (optional)
                    <div class="form_right">
                        <asp:UpdatePanel ID="upCountry" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                        <dx:ASPxComboBox ID="cbCountry" runat="server" Width="310px" Font-Size="14px" Height="24px"
                            EncodeHtml="False" IncrementalFilteringMode="StartsWith" 
                            AutoPostBack="True">
                        </dx:ASPxComboBox>				    
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div></td>
                <td>
                    <asp:Label ID="msg_CountryInfo" runat="server" Text="" CssClass="peLabel-reg"/>
                </td>
            </tr>
            <tr>
                <td></td>
                <td><asp:Label ID="msg_RegionText" runat="server" Text="Region" CssClass="peLabel-reg" /> (optional)
                    <div class="form_right">
                        <asp:UpdatePanel ID="upRegion" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                            <dx:ASPxComboBox ID="cbRegion" runat="server" Width="310px" Font-Size="14px" Height="24px"
                                EncodeHtml="False" EnableSynchronization="False" ClientInstanceName="cbRegion"
                                IncrementalFilteringMode="StartsWith" DataSourceID="sdsRegion" TextField="region1"
                                ValueField="region1" AutoPostBack="True">
                            </dx:ASPxComboBox>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <asp:SqlDataSource ID="sdsRegion" runat="server"></asp:SqlDataSource>				    
                    </div></td>
                <td>
                    <asp:Label ID="msg_RegionInfo" runat="server" Text="" CssClass="peLabel-reg"/>
                </td>
            </tr>
            <tr>
                <td></td>
                <td><asp:Label ID="msg_CityText" runat="server" Text="City" CssClass="peLabel-reg" /> (optional)
                    <div class="form_right">
                        <asp:UpdatePanel ID="upCity" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                            <dx:ASPxComboBox ID="cbCity" runat="server" Width="310px" Font-Size="14px" Height="24px"
                                EncodeHtml="False" EnableSynchronization="False" ClientInstanceName="cbCity"
                                IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" DataSourceID="sdsCity"
                                TextField="city" ValueField="city">
                            </dx:ASPxComboBox>				    
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <asp:SqlDataSource ID="sdsCity" runat="server"></asp:SqlDataSource>
                    </div></td>
                <td>
                    <asp:Label ID="msg_CityInfo" runat="server" Text="" CssClass="peLabel-reg"/>
                </td>
            </tr>
            <tr>
                <td></td>
                <td><asp:Panel ID="pnlPhoneErr" runat="server" CssClass="alert alert-danger" Visible="False" EnableViewState="false">
                                    <asp:Label ID="lblPhoneErr" runat="server" Text="" /></asp:Panel>
				    <asp:Label ID="msg_Phone" runat="server" Text="Phone" CssClass="peLabel-reg "/> (optional)
                    <div class="form_right ">
                        <dx:ASPxTextBox ID="txtPhone" runat="server" autocomplete="off" />
				    </div></td>
                <td>
                    <asp:Label ID="msg_PhoneInfo" runat="server" Text="" CssClass="peLabel-reg"/>
                </td>
            </tr>
            <tr>
                <td></td>
                <td><asp:Panel ID="pnlFacebookErr" runat="server" CssClass="alert alert-danger" Visible="False" EnableViewState="false">
                                    <asp:Label ID="lblFacebookErr" runat="server" Text="" /></asp:Panel>

				    <asp:Label ID="msg_Facebook" runat="server" Text="Facebook" CssClass="peLabel-reg "/> (optional)
                    <div class="form_right ">
                        <dx:ASPxTextBox ID="txtFacebook" runat="server" autocomplete="off" />
				    </div></td>
                <td>
                    <asp:Label ID="msg_FacebookInfo" runat="server" Text="" CssClass="peLabel-reg"/>
                </td>
            </tr>
        </table>


    </div>
    <div class="submit-button" align="center">
        <asp:Label ID="lblSubmitInfo" runat="server" Text="" CssClass="peLabel-reg"/><br />
        <div class="rfloat">
            <dx:ASPxButton ID="btnSubmitStep1" runat="server" Text="Next" 
                Font-Size="15px">
            </dx:ASPxButton>
        </div>
        <div class="clear"></div>
        <%--<asp:Button ID="btnSubmitStep1" runat="server" Text="Register" style="background-color:#3B3A38;padding:5px 20px;color:#fff;" />--%>
    </div>
</div>
<div class="lfloat" style="width:260;text-align:center;padding-top:150px;margin-left:50px;">
    <img src="/Images2/referrals.png" alt="referrals.png" title="" />

    <div>
        <asp:Label ID="lblRegisterPros" runat="server" Text="" CssClass=""/>
    </div>
</div>
<div class="clear"></div>
        <%--</ContentTemplate>
    </asp:UpdatePanel>--%>

        </asp:View>
        <asp:View ID="vwAgreement" runat="server">
            <script type="text/javascript">
                function btnSubmitStep2_Click(s, e) {
	                e.ProcessOnServer = ($('#<%= chkAgree.ClientID %>')[0].checked == true);
                    if(!e.ProcessOnServer){
                        $('#<%= pnlAgreeErr.ClientID %>').show()
                        setTimeout(function(){ $('#<%= pnlAgreeErr.ClientID %>').hide();}, 10000)
                    }
                }
            </script>


            <asp:Panel ID="pnlAgreeErr" runat="server" CssClass="alert alert-danger" EnableViewState="false" style="display:none;">
                                    <asp:Label ID="lblAgreeErr" runat="server" Text="" /></asp:Panel>

            <div style="margin-left:20px;">
            <asp:Label ID="lblAgreementTitle" runat="server"><h2 style="font-size:17px;font-weight:normal;">Admincash Agreement for [LOGINNAME]</h2></asp:Label>
            </div>
            <table>
                <tr>
                    <td style="padding:20px;background-color:#DAD8D9;" align="center">
                        <div class="agreement-text">
                            <asp:Literal ID="lblAgreementText" runat="server">Our agreement text</asp:Literal>
                        </div>
                        <div style="margin:20px 0 0;">
                            <asp:CheckBox ID="chkAgree" runat="server" Text="I Accept the conditions of Admincash" CssClass="check-box"  />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td align="center" style="padding-top:10px;"><dx:ASPxButton ID="btnSubmitStep2" runat="server" Text="Next" Font-Size="15px">
                        <ClientSideEvents Click="btnSubmitStep2_Click" />
                        </dx:ASPxButton></td>
                </tr>
            </table>
            <script type="text/javascript">
                $('#<%= chkAgree.ClientID %>').change(function () {
                    if ($('#<%= chkAgree.ClientID %>')[0].checked == true)
                        window['<%= btnSubmitStep2.ClientID %>'].SetEnabled(true);
                    else
                        window['<%= btnSubmitStep2.ClientID %>'].SetEnabled(false);
                });
                $(function () {
                    if ($('#<%= chkAgree.ClientID %>')[0].checked == true)
                        window['<%= btnSubmitStep2.ClientID %>'].SetEnabled(true);
                    else
                        window['<%= btnSubmitStep2.ClientID %>'].SetEnabled(false);
                });


            </script>
        </asp:View>
        <asp:View ID="vwUpload" runat="server">
            <script type="text/javascript">
                function oExists(o) {
                    return (o != null && typeof (o) !== 'undefined');
                }
                $(function () {
                    if (oExists(window["btnUploadFrontPage"])) btnUploadFrontPage.SetEnabled(false);
                    if (oExists(window["btnUploadBackPage"])) btnUploadBackPage.SetEnabled(false);
                    if (oExists(window["btnUploadThirdPage"])) btnUploadThirdPage.SetEnabled(false);
                });

                function OnUploadStart(s, e) {
                    switch (s.name) {
                        case 'ctl00_content_uplFrontPage':
                            if (oExists(window["btnUploadFrontPage"])) btnUploadFrontPage.SetEnabled(false);
                            break;
                        case 'ctl00_content_uplBackPage':
                            if (oExists(window["btnUploadBackPage"])) btnUploadBackPage.SetEnabled(false);
                            break;
                        case 'ctl00_content_uplThirdPage':
                            if (oExists(window["btnUploadThirdPage"])) btnUploadThirdPage.SetEnabled(false);
                            break;
                    }
                    //LoadingPanel.Show();
                }
                function OnFileUploadComplete(s, e) {
                    if (e.errorText && $.trim(e.errorText).length > 0) {
                        s.UpdateErrorMessageCell(0, e.errorText, false);
                    }
                    else {
                        s.SetEnabled(false);

                        var divSuc = s.name + '_success';
                        $('#' + divSuc).show();

                        //LoadingPanel.Hide();
                        //var o = document.getElementById("up1Refresh");
                        //if (o) o.click();
                        //LoadingPanel.Show();
                    }
                }
                function OnFilesUploadComplete(s, e) {
                    UpdateUploadButton(s, e);
                    //LoadingPanel.Hide();
                }
                function UpdateUploadButton(s, e) {
                    switch (s.name) {
                        case '<%= uplFrontPage.ClientID %>':
                            if (oExists(window["btnUploadFrontPage"])) btnUploadFrontPage.SetEnabled(s.GetText(0) != "");
                            break;
                        case '<%= uplBackPage.ClientID %>':
                            if (oExists(window["btnUploadBackPage"])) btnUploadBackPage.SetEnabled(s.GetText(0) != "");
                            break;
                        case '<%= uplThirdPage.ClientID %>':
                            if (oExists(window["btnUploadThirdPage"])) btnUploadThirdPage.SetEnabled(s.GetText(0) != "");
                            break;
                    }
                }
          
            </script>
        
            <div style="margin-left:20px;">
                <asp:Label ID="lblUploadHdr" runat="server"><h2 style="font-size:17px;font-weight:normal;">Upload your images of Documents - ID Photos</h2></asp:Label>
            </div>
        
            <div style="padding:10px 20px;background-color:#DAD8D9;font-size:15px;">
                <table>
                    <tr>
                        <td valign="top"><asp:Image ID="imgId" runat="server" ImageUrl="~/Images2/id_image.png" AlternateText="id image" /></td>
                        <td>&nbsp;</td>
                        <td>Here you need to upload Your ID photos. First take a photo of Your ID, or scan it, then upload the image below.
                            The photo has to be of very good quality and should be at least A5 (148 x 210 mm) size.</td>
                    </tr>
                </table>
            </div>

            <p style="font-size:15px;">
                Please make sure that your <span style="color:#FE7002;">NAME</span>, Your <span style="color:#FE7002;">BIRTHDATE</span> and Your 
                <span style="color:#FE7002;">ID NUMBER</span> is easily readable.<br />
                The max. filesize of each image is 4MB, allowed image formats are <span style="color:#FE7002;">JPEG, JPG, GIF, TIFF, PNG</span> 
                and <span style="color:#FE7002;">BMP</span>.<br /><br />
            </p>

            <div style="padding:10px;background-color:#fff;font-size:16px;border-left:5px solid #FFCC33;">
                ID Front page<br />
                <div id="<%= uplFrontPage.ClientID %>_success" class="upload-success"> - Uploaded succesfully</div>
                <table>
                    <tr>
                        <td><dx:ASPxUploadControl ID="uplFrontPage" runat="server" 
                                ClientInstanceName="uplFrontPage"
                                OnFileUploadComplete="uplFrontPage_FileUploadComplete" 
                                CssClass="uploadCtlText" FileUploadMode="OnPageLoad" EncodeHtml="False" 
                                Height="22px" Width="400px">
                                <ClientSideEvents 
                                    FileUploadComplete="OnFileUploadComplete"
                                    FilesUploadComplete="OnFilesUploadComplete"
                                    FileUploadStart="OnUploadStart"
                                    TextChanged="UpdateUploadButton"></ClientSideEvents>
                                <ValidationSettings MaxFileSize="4194304" AllowedFileExtensions=".jpg,.jpeg,.jpe,.gif,.bmp,.png">
                                </ValidationSettings>
                                <BrowseButton Text="Select Document">
                                </BrowseButton>
                                <TextBoxStyle BackColor="#E3E3E3" ForeColor="Black" >
                                <Paddings Padding="0px" />
                                </TextBoxStyle>
                                <BrowseButtonStyle Font-Size="15px">
                                </BrowseButtonStyle>
                            </dx:ASPxUploadControl></td>
                        <td>&nbsp;</td>
                        <td><dx:ASPxButton ID="btnUploadFrontPage" runat="server" 
                                AutoPostBack="False" Text="Upload" ClientInstanceName="btnUploadFrontPage" EncodeHtml="False" Font-Size="15px">
                                <ClientSideEvents Click="function(s, e) { uplFrontPage.Upload(); }" />
                            </dx:ASPxButton></td>
                    </tr>
                </table>
                <br />

                ID Back page<br />
                <div id="<%= uplBackPage.ClientID %>_success" class="upload-success"> - Uploaded succesfully</div>
                <table>
                    <tr>
                        <td><dx:ASPxUploadControl ID="uplBackPage" runat="server" 
                                ClientInstanceName="uplBackPage"
                                OnFileUploadComplete="uplBackPage_FileUploadComplete" 
                                CssClass="uploadCtlText" FileUploadMode="OnPageLoad" EncodeHtml="False" 
                                Height="22px" Width="400px">
                                <ClientSideEvents 
                                    FileUploadComplete="OnFileUploadComplete"
                                    FilesUploadComplete="OnFilesUploadComplete"
                                    FileUploadStart="OnUploadStart"
                                    TextChanged="UpdateUploadButton"></ClientSideEvents>
                                <ValidationSettings MaxFileSize="4194304" AllowedFileExtensions=".jpg,.jpeg,.jpe,.gif,.bmp,.png">
                                </ValidationSettings>
                                <BrowseButton Text="Select Document">
                                </BrowseButton>
                                <TextBoxStyle BackColor="#E3E3E3" ForeColor="Black" >
                                <Paddings Padding="0px" />
                                </TextBoxStyle>
                                <BrowseButtonStyle Font-Size="15px">
                                </BrowseButtonStyle>
                            </dx:ASPxUploadControl></td>
                        <td>&nbsp;</td>
                        <td><dx:ASPxButton ID="btnUploadBackPage" runat="server" 
                                AutoPostBack="False" Text="Upload" ClientInstanceName="btnUploadBackPage" EncodeHtml="False" Font-Size="15px">
                                <ClientSideEvents Click="function(s, e) { uplBackPage.Upload(); }" />
                            </dx:ASPxButton></td>
                    </tr>
                </table>
                <br /><br />
                <div style="font-size:14px;">
                Uploading a third page is necessary only if the required information can not be found on the first two pages of Your ID.
                </div><br />

                ID Third page<br />
                <div id="<%= uplThirdPage.ClientID %>_success" class="upload-success"> - Uploaded succesfully</div>
                <table>
                    <tr>
                        <td><dx:ASPxUploadControl ID="uplThirdPage" runat="server" 
                                ClientInstanceName="uplThirdPage"
                                OnFileUploadComplete="uplThirdPage_FileUploadComplete" 
                                CssClass="uploadCtlText" FileUploadMode="OnPageLoad" EncodeHtml="False" 
                                Height="22px" Width="400px">
                                <ClientSideEvents 
                                    FileUploadComplete="OnFileUploadComplete"
                                    FilesUploadComplete="OnFilesUploadComplete"
                                    FileUploadStart="OnUploadStart"
                                    TextChanged="UpdateUploadButton"></ClientSideEvents>
                                <ValidationSettings MaxFileSize="4194304" AllowedFileExtensions=".jpg,.jpeg,.jpe,.gif,.bmp,.png">
                                </ValidationSettings>
                                <BrowseButton Text="Select Document">
                                </BrowseButton>
                                <TextBoxStyle BackColor="#E3E3E3" ForeColor="Black" >
                                <Paddings Padding="0px" />
                                </TextBoxStyle>
                                <BrowseButtonStyle Font-Size="15px">
                                </BrowseButtonStyle>
                            </dx:ASPxUploadControl></td>
                        <td>&nbsp;</td>
                        <td><dx:ASPxButton 
                                ID="btnUploadThirdPage" runat="server" AutoPostBack="False" Text="Upload" 
                                ClientInstanceName="btnUploadThirdPage" EncodeHtml="False" Font-Size="15px">
                                <ClientSideEvents Click="function(s, e) { uplThirdPage.Upload(); }" />
                            </dx:ASPxButton></td>
                    </tr>
                </table>
                
            </div>


            <div class="submit-button" align="center">
                <dx:ASPxButton ID="btnSubmitStep3" runat="server" Text="Next" 
                    Font-Size="15px">
                </dx:ASPxButton>
            </div>
  
        </asp:View>
    </asp:MultiView>
    
</div>

</asp:Content>

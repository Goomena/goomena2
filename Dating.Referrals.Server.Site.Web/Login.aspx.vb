﻿Imports Library.Public
Imports DevExpress.Web.ASPxEditors

Public Class Login
    Inherits BasePage


    Dim _pageData As clsPageData
    Protected ReadOnly Property CurrentPageData As clsPageData
        Get
            If (_pageData Is Nothing) Then _pageData = New clsPageData(Context)
            Return _pageData
        End Get
    End Property



    Protected Overrides Sub OnPreInit(ByVal e As EventArgs)
        Try
            Dim check As Boolean = (Request.QueryString("check") = "1")
            If (check) Then
                Return
            End If

            If (Not Page.IsPostBack) Then

                If (Not String.IsNullOrEmpty(HttpContext.Current.Session("LoginName")) AndAlso
                    Not String.IsNullOrEmpty(Request("login")) AndAlso
                    HttpContext.Current.Session("LoginName") <> Request("login")) Then

                    'clear session
                    FormsAuthentication.SignOut()
                    Session.Abandon()

                    Dim url As String = Request.Url.ToString()
                    If (String.IsNullOrEmpty(Request.QueryString("escape"))) Then

                        url = url & IIf(url.IndexOf("?") = -1, "?", "&") & "escape=1"
                        Response.Redirect(url)

                    End If
                End If

                If (clsCurrentContext.VerifyLogin() = True) Then
                    Response.Redirect(Page.ResolveUrl("~/Members/Default.aspx"))

                ElseIf (HttpContext.Current.User.Identity.IsAuthenticated) Then
                    'clear session
                    FormsAuthentication.SignOut()
                    Session.Abandon()

                End If

                MyBase.OnPreInit(e)
            End If

        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "OnPreInit")
        End Try
    End Sub


    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try

            Me.Title = "Login"
            If clsCurrentContext.VerifyLogin() = True Then
                Response.Redirect("~/Members/default.aspx")
            End If


            'Try
            '    Dim cPageBasic As Dating.Server.Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            '    GeneralFunctions.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
            'Catch ex As Exception
            '    WebErrorMessageBox(Me, ex, "Page_Load")
            'End Try


            If (Not Me.IsPostBack) Then
                LoadLAG()
            End If

        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub



    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
    End Sub


    Protected Sub LoadLAG()
        Try
            Dim cPageBasic As Dating.Server.Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic

            'lbHTMLBody.Text = cPageBasic.BodyHTM
            'lblHeader.Text = CurrentPageData.GetCustomString("lblHeader")
            'lblWelcome.Text = CurrentPageData.GetCustomString("lblWelcome")
            'lnkRestore.Text = CurrentPageData.GetCustomString("lnkRestore")
            'lnkCreateAccount.Text = CurrentPageData.GetCustomString("lnkCreateAccount")
            ''lblNotMemberQuestion.Text = CurrentPageData.GetCustomString("lblNotMemberQuestion")
            ''lnkCreateAccountRight.Text = CurrentPageData.GetCustomString("lnkCreateAccountRight")
            ''lblHtmlBodyRight.Text = CurrentPageData.GetCustomString("lblHtmlBodyRight")

            'lblHeaderRight.Text = CurrentPageData.GetCustomString("lblHeaderRight")
            'lblTextRight.Text = CurrentPageData.GetCustomString("lblTextRight")

            'lnkCreateAccount.NavigateUrl = "~/Register.aspx"
            'If (Not String.IsNullOrEmpty(Request.QueryString("ReturnUrl"))) Then
            '    lnkCreateAccount.NavigateUrl = lnkCreateAccount.NavigateUrl & "?ReturnUrl=" & Server.UrlEncode(Request.QueryString("ReturnUrl"))
            'End If


            LoadLAG_LoginControl()

            AppUtils.setNaviLabel(Me, "lbPublicNavi2Page", cPageBasic.PageTitle)

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Protected Sub LoadLAG_LoginControl()
        Try
            Dim cPageBasic As Dating.Server.Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            Dim MainLogin As System.Web.UI.WebControls.Login = ucLogin1.LoginControl
            'MainLogin.UserNameLabelText = CurrentPageData.GetCustomString("txtUserNameLabelText")
            MainLogin.PasswordLabelText = CurrentPageData.GetCustomString("txtPasswordLabelText")
            MainLogin.RememberMeText = CurrentPageData.GetCustomString("txtRememberMeText")
            MainLogin.LoginButtonText = CurrentPageData.GetCustomString("txtLoginButtonText")
            MainLogin.UserNameRequiredErrorMessage = CurrentPageData.GetCustomString("txtUserNameRequiredErrorMessage")
            MainLogin.TitleText = CurrentPageData.GetCustomString("txtLoginTitleText")

            Dim lbForgotPassword As ASPxHyperLink = MainLogin.FindControl("lbForgotPassword")
            If (lbForgotPassword IsNot Nothing) Then
                lbForgotPassword.Text = CurrentPageData.GetCustomString("lbForgotPassword")
            End If

            Dim lbLoginFB As ASPxHyperLink = MainLogin.FindControl("lbLoginFB")
            If (lbLoginFB IsNot Nothing) Then
                lbLoginFB.Text = CurrentPageData.GetCustomString("lbLoginFB")
            End If

            Dim btnJoinToday As ASPxButton = MainLogin.FindControl("btnJoinToday")
            If (btnJoinToday IsNot Nothing) Then
                btnJoinToday.Text = CurrentPageData.GetCustomString("btnJoinToday")
            End If

            Dim UserName As ASPxTextBox = MainLogin.FindControl("UserName")
            If (UserName IsNot Nothing) Then
                'UserName.NullText = CurrentPageData.GetCustomString("UserName.NullText")
                UserName.ValidationSettings.RequiredField.ErrorText = CurrentPageData.GetCustomString("UserName.ValidationSettings.RequiredField.ErrorText")
            End If

            Dim Password As ASPxTextBox = MainLogin.FindControl("Password")
            If (Password IsNot Nothing) Then
                'Password.NullText = CurrentPageData.GetCustomString("Password.NullText")
                Password.ValidationSettings.RequiredField.ErrorText = CurrentPageData.GetCustomString("Password.ValidationSettings.RequiredField.ErrorText")
            End If


            Dim btnLogin As ASPxButton = MainLogin.FindControl("btnLogin")
            If (btnLogin IsNot Nothing) Then
                btnLogin.Text = CurrentPageData.GetCustomString("btnLogin")
            End If

            Dim RememberMe As CheckBox = MainLogin.FindControl("RememberMe")
            If (RememberMe IsNot Nothing) Then
                RememberMe.Text = CurrentPageData.GetCustomString("RememberMe")
            End If

            Dim chkRememberMe As ASPxCheckBox = MainLogin.FindControl("chkRememberMe")
            If (chkRememberMe IsNot Nothing) Then
                chkRememberMe.Text = CurrentPageData.GetCustomString("RememberMe")
            End If

            '' controls found on login.aspx page
            Dim lblUserName As Label = MainLogin.FindControl("lblUserName")
            If (lblUserName IsNot Nothing) Then
                lblUserName.Text = CurrentPageData.GetCustomString("lblUserName")
            End If


            Dim lblPassword As Label = MainLogin.FindControl("lblPassword")
            If (lblPassword IsNot Nothing) Then
                lblPassword.Text = CurrentPageData.GetCustomString("lblPassword")
            End If

            Dim btnLoginNow As ASPxButton = MainLogin.FindControl("btnLoginNow")
            If (btnLoginNow IsNot Nothing) Then
                btnLoginNow.Text = CurrentPageData.GetCustomString("btnLoginNow")
                btnLoginNow.Text = btnLoginNow.Text.Replace("&nbsp;", " ")
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

End Class
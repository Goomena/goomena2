﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Members/Members2Inner.master" 
CodeBehind="ReferrerFamilyTree.aspx.vb" Inherits="Dating.Referrals.Server.Site.Web.ReferrerFamilyTree" %>

<%@ Register assembly="DevExpress.Web.ASPxTreeList.v13.1, Version=13.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxTreeList" tagprefix="dx" %>
<%@ Register src="../UserControls/WhatIsIt.ascx" tagname="WhatIsIt" tagprefix="uc2" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">

           <div class="grid_3 top_sidebar" id="sidebar-outter">
                <div class="lfloat">
                    <h2 class="page-name"><asp:Literal ID="lblItemsName" runat="server" /></h2>
                    <h2 class="page-description"><asp:Label ID="lblPageTitle" runat="server" Text="Οι Προμήθειες μου ανά επίπεδο" /></h2>
                </div>
                <div class="page-back-btn">
                    <dx:ASPxHyperLink ID="lnkBack" runat="server" Text="Back" CssClass="btn" 
                        NavigateUrl="~/Members/Referrer.aspx">
                    </dx:ASPxHyperLink>
                </div>
                <div class="clear">
                </div>
            </div>

                    <div class="t_wrap">
                        <div class="send_msg t_to msg">
                            <asp:Panel ID="pnlErr" runat="server" CssClass="alert alert-danger" 
                                Visible="False" EnableViewState="False">
                                <asp:Label ID="lblErr" runat="server"></asp:Label>
                            </asp:Panel>
        
        <asp:Label ID="lblPageDesc" runat="server" Text="Εδώ μπορείτε να δείτε σε μορφή δένδρου όλα τα μέλη που αποτελούν την δική σας οικογένεια."></asp:Label>
        <br />
        <br />
<table cellpadding="5" cellspacing="1">
            <tr>
                <td>
                    <asp:Label ID="lblFromDate" runat="server" Text="Από Ημ/νία : "></asp:Label></td>
                <td>
                    <dx:ASPxDateEdit ID="dtFrom" runat="server" DisplayFormatString="dd/MM/yyyy" 
                        Width="120px">
                    </dx:ASPxDateEdit>
                </td>
                <td>
                    <asp:Label ID="lblToDate" runat="server" Text="Εως "></asp:Label></td>
                <td><dx:ASPxDateEdit ID="dtTo" runat="server" DisplayFormatString="dd/MM/yyyy" 
                        Width="120px">
                    </dx:ASPxDateEdit></td>
                <%--<td>
                    <asp:Label ID="lblLevel" runat="server" Text="Level: "></asp:Label></td>
                <td>
                    <dx:ASPxComboBox ID="cmbLevel" runat="server" SelectedIndex="0" Width="120px">
                        <Items>
                            <dx:ListEditItem Selected="True" Text="All" Value="-1" />
                            <dx:ListEditItem Text="Level 1" Value="1" />
                            <dx:ListEditItem Text="Level 2" Value="2" />
                            <dx:ListEditItem Text="Level 3" Value="3" />
                            <dx:ListEditItem Text="Level 4" Value="4" />
                            <dx:ListEditItem Text="Level 5" Value="5" />
                        </Items>
                    </dx:ASPxComboBox>
                </td>--%>
                <td>
                    <dx:ASPxButton ID="btnRefresh" runat="server" Text="Refresh">
                    </dx:ASPxButton>
                </td>
            </tr>
        </table>
        <br />
        <br />
                <dx:ASPxTreeList ID="trlReferrers" runat="server" AutoGenerateColumns="False" 
            KeyFieldName="ProfileID" DataSourceID="odsTree" ParentFieldName="ParentId" Width="100%">
            <Columns>
                <dx:TreeListTextColumn Caption="ProfileID" FieldName="ProfileID" 
                    VisibleIndex="0" Visible="False">
                </dx:TreeListTextColumn>
                <dx:TreeListTextColumn Caption="Users" FieldName="LoginName" 
                    VisibleIndex="1" Width="90%">
                </dx:TreeListTextColumn>
                <dx:TreeListTextColumn Caption="ParentId" FieldName="ParentId" VisibleIndex="1" 
                    Visible="False">
                </dx:TreeListTextColumn>
                <dx:TreeListTextColumn Caption="Referrals Count" FieldName="ChildsCount" 
                    VisibleIndex="1">
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dx:TreeListTextColumn>
                <dx:TreeListTextColumn Caption="CreditsSum" FieldName="CreditsSum" 
                    VisibleIndex="2" Visible="False">
                </dx:TreeListTextColumn>
                <dx:TreeListTextColumn Caption="CreditsAvailable" FieldName="CreditsAvailable" 
                    VisibleIndex="3" Visible="False">
                </dx:TreeListTextColumn>
                <dx:TreeListTextColumn Caption="CreditsPending" FieldName="CreditsPending" 
                    VisibleIndex="4" Visible="False">
                </dx:TreeListTextColumn>
                <dx:TreeListTextColumn Caption="Total" 
                    FieldName="CreditsSumLevelsTotal" VisibleIndex="5" Visible="False">
                    <PropertiesTextEdit DisplayFormatString="0.00"> <%-- DisplayFormatString="0.00 &euro;" Caption="Total &euro;" --%>
                    </PropertiesTextEdit>
                    <CellStyle HorizontalAlign="Right">
                    </CellStyle>
                </dx:TreeListTextColumn>
                <dx:TreeListTextColumn Caption="CreditsAvailableLevelsTotal" 
                    FieldName="CreditsAvailableLevelsTotal" VisibleIndex="6" Visible="False">
                </dx:TreeListTextColumn>
                <dx:TreeListTextColumn Caption="CreditsPendingLevelsTotal" 
                    FieldName="CreditsPendingLevelsTotal" VisibleIndex="7" Visible="False">
                </dx:TreeListTextColumn>
                <dx:TreeListTextColumn Caption="CommissionCredits" 
                    FieldName="CommissionCredits" VisibleIndex="8" Visible="False">
                </dx:TreeListTextColumn>
                <dx:TreeListTextColumn Caption="CommissionCreditsAvailable" 
                    FieldName="CommissionCreditsAvailable" VisibleIndex="9" Visible="False">
                </dx:TreeListTextColumn>
                <dx:TreeListTextColumn Caption="CommissionCreditsPending" 
                    FieldName="CommissionCreditsPending" VisibleIndex="10" Visible="False">
                </dx:TreeListTextColumn>
                <dx:TreeListTextColumn Caption="Total Commission" 
                    FieldName="CommissionCreditsLevelsTotal" VisibleIndex="11">
                    <PropertiesTextEdit DisplayFormatString="0.00"><%-- Caption="Total Commission &euro;" DisplayFormatString="0.00 &euro;"--%>
                    </PropertiesTextEdit>
                    <CellStyle HorizontalAlign="Right">
                    </CellStyle>
                </dx:TreeListTextColumn>
                <dx:TreeListTextColumn Caption="CommissionCreditsAvailableLevelsTotal" 
                    FieldName="CommissionCreditsAvailableLevelsTotal" VisibleIndex="12" 
                    Visible="False">
                </dx:TreeListTextColumn>
                <dx:TreeListTextColumn Caption="CommissionCreditsPendingLevelsTotal" 
                    FieldName="CommissionCreditsPendingLevelsTotal" VisibleIndex="13" 
                    Visible="False">
                </dx:TreeListTextColumn>
                <dx:TreeListTextColumn Caption="Level" FieldName="Level" VisibleIndex="16">
                    <CellStyle HorizontalAlign="Right">
                    </CellStyle>
                </dx:TreeListTextColumn>
            </Columns>
            <Styles>
                <Header HorizontalAlign="Center" VerticalAlign="Middle" BackColor="#FF7A15" Font-Bold="True" ForeColor="White">
                    
                </Header>
                <Footer BackColor="#FF8b26" HorizontalAlign="Right" VerticalAlign="Middle">
                </Footer>
                <PagerBottomPanel BackColor="#FF8b26">
                </PagerBottomPanel>
            </Styles>
                    <Border BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" />
        </dx:ASPxTreeList>
    <asp:ObjectDataSource ID="odsTree" runat="server" SelectMethod="GetData" 
        TypeName="Dating.Referrals.Server.Site.Web.ReferrerFamilyTree" 
                                OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
            <asp:ControlParameter ControlID="dtFrom" Name="dateFrom" PropertyName="Value" 
                Type="DateTime" />
            <asp:ControlParameter ControlID="dtTo" Name="dateTo" PropertyName="Value" 
                Type="DateTime" />
            <asp:Parameter Name="affCredits1" Type="Object" />
        </SelectParameters>
    </asp:ObjectDataSource>

        
                        </div>
                    </div>
        <%--</div>
	
	    </div>
        <div class="clear"></div>
    </div>--%>

	<uc2:WhatIsIt ID="WhatIsIt1" runat="server" />

</asp:Content>

﻿Imports Dating.Server.Core.DLL
Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Datasets.DLL.DSMembers

Public Class ReferrerSupplies
    Inherits ReferrerBasePage

#Region "Props"

    Dim _pageData As clsPageData
    Protected ReadOnly Property CurrentPageData As clsPageData
        Get
            If (_pageData Is Nothing) Then _pageData = New clsPageData(Context)
            Return _pageData
        End Get
    End Property


    Private affCredits As clsReferrerCredits


    Private ReadOnly Property LevelParam As Integer
        Get
            Dim level As Integer
            If (Not String.IsNullOrEmpty(Request.QueryString("lvl"))) Then
                Integer.TryParse(Request.QueryString("lvl"), level)
            End If
            Return level
        End Get
    End Property


    Private ReadOnly Property AffiliateIdParam As Integer
        Get
            Dim affiliateid As Integer
            If (Not String.IsNullOrEmpty(Request.QueryString("affid"))) Then
                Integer.TryParse(Request.QueryString("affid"), affiliateid)
            End If
            Return affiliateid
        End Get
    End Property

#End Region



    Protected Overrides Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        Try
            If clsCurrentContext.VerifyLogin() = True Then
                Me.MasterPageFile = "~/Members/Members2.Master"
            Else
                clsCurrentContext.ClearSession(Session)
                Response.Redirect(ResolveUrl("~/Login.aspx"), False)
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pnlErr.Visible = False

        Try
            If (Me.IsReferrer()) Then
                ' do nothing
            Else
                Response.Redirect("Referrer.aspx")
            End If

        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        'If (Not Page.IsPostBack) Then

        '    Try
        '        LoadLAG()

        '        'If (Request.UrlReferrer IsNot Nothing) Then
        '        '    lnkBack.NavigateUrl = Request.UrlReferrer.ToString()
        '        'Else
        '        '    lnkBack.NavigateUrl = "~/Members/"
        '        'End If


        '        GetAffiliateCreditsObject()
        '        If (LevelParam > 0) Then
        '            pnlPyramid.Visible = True
        '            pnlProfiles.Visible = True

        '            gvPyramid.Visible = True
        '            gvProfiles.Visible = True

        '            gvProfiles.DataBind()
        '            BindPyramid()
        '        Else
        '            pnlPyramid.Visible = True
        '            pnlProfiles.Visible = False

        '            gvPyramid.Visible = True
        '            gvProfiles.Visible = False

        '            BindPyramid()
        '        End If

        '    Catch ex As Exception
        '        WebErrorMessageBox(Me, ex, "Page_Load")
        '    End Try

        'End If

        Try
            If (Not Me.IsPostBack) Then
                LoadLAG()
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try


        Try
            SetAffiliateUI()
            'If (Me.GetCurrentProfile().AffiliateParentId > 0) Then
            '    'lblAffiliateTitle.Text = "Επισκόπηση συνεργασίας"
            'Else
            '    mvMain.SetActiveView(vwSetAffiliateParent)
            'End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
        SetAffiliateUI()
    End Sub


    Protected Sub LoadLAG()
        Try

            lblPageTitle.Text = CurrentPageData.GetCustomString("lblPageTitle")
            lnkBack.Text = CurrentPageData.GetCustomString("lnkBack")
            lblPageDesc.Text = CurrentPageData.GetCustomString("lblPageDesc")

            lblProfilesListInfo.Text = CurrentPageData.GetCustomString("lblProfilesListInfo")
            lblProfilesListInfo.Text = lblProfilesListInfo.Text.Replace("[LEVEL]", Me.LevelParam)

            popupAffiliateSupplies.HeaderText = CurrentPageData.GetCustomString("popupAffiliateSupplies.HeaderText")

            gvProfiles.SettingsText.EmptyDataRow = CurrentPageData.GetCustomString("gvProfiles.EmptyDataRow")

            lblFromDate.Text = CurrentPageData.GetCustomString("lblFromDate")
            lblToDate.Text = CurrentPageData.GetCustomString("lblToDate")
            btnRefresh.Text = CurrentPageData.GetCustomString("btnRefresh")


            lblItemsName.Text = Me.CurrentPageData.GetCustomString("CartReferrerSuppliesView")
            'lnkViewDescription.Text = Me.CurrentPageData.GetCustomString("CartReferrerSuppliesWhatIsIt")
            'lnkViewDescription.OnClientClick = OnMoreInfoClickFunc(
            '    lnkViewDescription.OnClientClick,
            '    ResolveUrl("~/Members/InfoWin.aspx?info=referrersupplies"),
            '    Me.CurrentPageData.GetCustomString("CartReferrerSuppliesView"))
            WhatIsIt1.PopupControl.HeaderText = Me.CurrentPageData.GetCustomString("CartReferrerSuppliesView")

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender

        Try
            Dim cPageBasic As Dating.Server.Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub


    Private Sub SetAffiliateUI()
        SetDaysRange()
        affCredits = GetReferrerCreditsObject(dtFrom.Date, dtTo.Date)

        If (LevelParam > 0) Then
            pnlPyramid.Visible = True
            pnlProfiles.Visible = True

            gvPyramid.Visible = True
            gvProfiles.Visible = True

            gvProfiles.DataBind()
            BindPyramid()
        Else
            pnlPyramid.Visible = True
            pnlProfiles.Visible = False

            gvPyramid.Visible = True
            gvProfiles.Visible = False

            BindPyramid()
        End If
    End Sub



    Private Sub SetDaysRange()
        If (Not Page.IsPostBack) Then
            If (SessionVariables.ReferrerCredits IsNot Nothing) Then
                Try
                    dtFrom.Value = SessionVariables.ReferrerCredits.DateFrom
                    dtTo.Value = SessionVariables.ReferrerCredits.DateTo
                Catch ex As Exception
                End Try
            End If

            If (dtFrom.Value Is Nothing OrElse dtTo.Value Is Nothing) Then
                dtFrom.Date = DateTime.UtcNow.AddDays(-clsReferrer.DefaultDaysRange)
                dtTo.Date = DateTime.UtcNow.AddDays(1)
            End If
        End If
    End Sub



    Public Function GetProfilesReferrers(targetLevel As Integer, profileId As Integer, DateFrom As DateTime?, DateTo As DateTime?, affCredits1 As clsReferrerCredits) As DataTable

        Using dtPyramid As New DataTable()


            dtPyramid.Columns.Add(New Data.DataColumn("LoginName"))
            dtPyramid.Columns.Add(New Data.DataColumn("Status", GetType(Integer)))
            dtPyramid.Columns.Add(New Data.DataColumn("SalesCRD", GetType(Double)))
            dtPyramid.Columns.Add(New Data.DataColumn("CommissionCRD", GetType(Double)))
            dtPyramid.Columns.Add(New Data.DataColumn("PersonsNavigateUrl"))


            Try

                Dim drs As DSCustom.ReferrersRepositoryDataRow() = affCredits1.GetReferrersForLevel(targetLevel)
                Dim SalesCRD As Double = 0
                Dim CommissionCRD As Double = 0
                Dim c As Integer
                For c = 0 To drs.Length - 1
                    Dim _dr As Data.DataRow = dtPyramid.NewRow()
                    _dr("LoginName") = drs(c).LoginName
                    _dr("Status") = drs(c).Status
                    _dr("SalesCRD") = drs(c).CreditsSum
                    _dr("CommissionCRD") = drs(c).CommissionCredits
                    _dr("PersonsNavigateUrl") = "~/Members/ReferrerSuppliesPersons.aspx?lvl=" & targetLevel & "&affid=" & drs(c).ProfileID
                    dtPyramid.Rows.Add(_dr)
                Next

            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "BindPyramid")
            End Try

            Return dtPyramid
        End Using
    End Function


    Protected Sub odsProfiles_Selecting(sender As Object, e As System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs) Handles odsProfiles.Selecting
        Dim level As Integer = LevelParam

        If (Me.MasterProfileId > 1) AndAlso (level = 0) Then level = 1

        e.InputParameters("targetLevel") = level
        e.InputParameters("profileId") = Me.MasterProfileId
        e.InputParameters("affCredits1") = GetReferrerCreditsObject(dtFrom.Value, dtTo.Value)

    End Sub



    Private Sub BindPyramid()
        Try
            'pnlPyramid.Visible = True
            Dim dtPyramid As DataTable = GetPyramidData()

            If (dtPyramid.Rows.Count > 0) Then
                gvPyramid.DataSource = dtPyramid
                gvPyramid.DataBind()
            Else
                pnlErr.CssClass = "alert alert-danger"
                pnlErr.Visible = True
                lblErr.Text = CurrentPageData.GetCustomString("Err_AffiliatesNotFound") ' "Συνεργάτες δεν βρέθηκαν"

                gvPyramid.DataSource = Nothing
                gvPyramid.DataBind()
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "BindPyramid")
        End Try
    End Sub


    Private Function GetPyramidData() As DataTable

        Using dtPyramid As New DataTable()



            Try

                dtPyramid.Columns.Add(New Data.DataColumn("Levels"))
                dtPyramid.Columns.Add(New Data.DataColumn("MembersCount"))
                dtPyramid.Columns.Add(New Data.DataColumn("SalesCRD", GetType(Double)))
                dtPyramid.Columns.Add(New Data.DataColumn("CommissionCRD", GetType(Double)))
                dtPyramid.Columns.Add(New Data.DataColumn("PersonsNavigateUrl"))


                Dim mySubAffiliates As New List(Of Integer)
                Dim level As Integer = 1

                Try

                    Dim drs As DSCustom.ReferrersRepositoryDataRow() = affCredits.GetReferrersForLevel(level)
                    Dim SalesCRD As Double = 0
                    Dim CommissionCRD As Double = 0
                    Dim c As Integer
                    For c = 0 To drs.Length - 1
                        Dim profileId As Integer = drs(c)("ProfileID")
                        mySubAffiliates.Add(profileId)

                        SalesCRD = SalesCRD + drs(c)("CreditsSum")
                        CommissionCRD = CommissionCRD + drs(c)("CommissionCredits")
                    Next

                    Dim _dr As Data.DataRow = dtPyramid.NewRow()
                    _dr("Levels") = level
                    _dr("MembersCount") = mySubAffiliates.Count
                    _dr("SalesCRD") = SalesCRD
                    _dr("CommissionCRD") = CommissionCRD
                    _dr("PersonsNavigateUrl") = "~/Members/ReferrerSupplies.aspx?lvl=" & level
                    dtPyramid.Rows.Add(_dr)
                Catch ex As Exception
                    Throw
                End Try


                ' next levels
                While (mySubAffiliates.Count > 0)
                    level = level + 1

                    Try
                        mySubAffiliates.Clear()

                        Dim drs As DSCustom.ReferrersRepositoryDataRow() = affCredits.GetReferrersForLevel(level)
                        Dim SalesCRD As Double = 0
                        Dim CommissionCRD As Double = 0
                        Dim c As Integer
                        For c = 0 To drs.Length - 1
                            Dim profileId As Integer = drs(c).ProfileID
                            mySubAffiliates.Add(profileId)

                            SalesCRD = SalesCRD + drs(c).CreditsSum
                            CommissionCRD = CommissionCRD + drs(c).CommissionCredits
                        Next

                        If (mySubAffiliates.Count > 0) Then
                            Dim _dr As Data.DataRow = dtPyramid.NewRow()
                            _dr("Levels") = level
                            _dr("MembersCount") = mySubAffiliates.Count
                            _dr("SalesCRD") = SalesCRD
                            _dr("CommissionCRD") = CommissionCRD
                            _dr("PersonsNavigateUrl") = "~/Members/ReferrerSupplies.aspx?lvl=" & level
                            dtPyramid.Rows.Add(_dr)

                        End If

                    Catch ex As Exception
                        Throw
                    End Try

                End While


            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "BindPyramid")
            End Try

            Return dtPyramid
        End Using
    End Function




    Protected Sub gvProfiles_DataBound(ByVal sender As Object, ByVal e As EventArgs)
        Dim link As DevExpress.Web.ASPxEditors.ASPxHyperLink = CType(sender, DevExpress.Web.ASPxEditors.ASPxHyperLink)
        Dim url As String = ResolveUrl(link.NavigateUrl)
        link.ClientSideEvents.Click = String.Format("function(s, e) {{ OnMoreInfoClickRef('{0}'); }}", url.Replace("'", "\'"))
        link.NavigateUrl = "javascript:void(0);"
    End Sub



    Protected Sub btnRefresh_Click(sender As Object, e As EventArgs) Handles btnRefresh.Click
        Try

            SetAffiliateUI()

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "btnGo")
        End Try
    End Sub

End Class
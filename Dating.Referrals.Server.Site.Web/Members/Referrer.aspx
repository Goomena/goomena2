﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Members/Members2Inner.master"
    CodeBehind="Referrer.aspx.vb" Inherits="Dating.Referrals.Server.Site.Web.Referrer2" %>

<%@ Register src="../UserControls/WhatIsIt.ascx" tagname="WhatIsIt" tagprefix="uc2" %>
<%@ Register src="../UserControls/ContactControl.ascx" tagname="ContactControl" tagprefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
                <asp:Panel ID="pnlTop" runat="server">
                   <div class="grid_3 top_sidebar" id="sidebar-outter">
                        <div class="lfloat">
                            <h2 class="page-name"><asp:Literal ID="lblItemsName" runat="server" /></h2>
                            <h2 class="page-description"><asp:Label ID="lblPageTitle" runat="server" Text="Οι Προμήθειες μου ανά επίπεδο" /></h2>
                        </div>
                        <div class="page-back-btn">
                            <dx:ASPxHyperLink ID="lnkBack" runat="server" Text="Back" CssClass="btn" Visible="False">
                                </dx:ASPxHyperLink>
                        </div>
                        <div class="clear">
                        </div>
                    </div>
                </asp:Panel>

            
                <div class="t_wrap">
                    <div class="send_msg t_to msg">
                        <asp:Panel ID="pnlLoginErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblLoginErr" runat="server" Text=""></asp:Label>
                        </asp:Panel>
                        <asp:MultiView ID="mvMain" runat="server" ActiveViewIndex="0">
                            <asp:View ID="vwSetReferrerParent" runat="server">
                                <div style="margin:50px 0 0 0;">

                                    <table>
                                        <tr>
                                            <td valign="top"><asp:Image ID="imgWarning" runat="server" ImageUrl="~/Images2/welcome.png" /></td>
                                            <td>&nbsp;</td>
                                            <td><div>
                                                <asp:Label ID="lblRefApproveNotif" runat="server" Text="">Thank you for registering for our Referral Program.<br /> Your Account will be activated soon!</asp:Label>
                                            </div></td>
                                        </tr>
                                    </table>
                                    <div style="margin-top:15px;">
<asp:Label ID="lblRefApproveNotifBody" runat="server" Text=""></asp:Label>
                                    </div>
                                </div>
                                <%--<table>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblRefer" runat="server" Text="Σύσταση"></asp:Label>
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            <dx:ASPxTextBox ID="txtParentLogin" runat="server" Width="170px" 
                                                AutoCompleteType="Disabled"></dx:ASPxTextBox>
                                        </td>
                                    </tr>
                                </table>
                                <div class="form-actions">
                                    <asp:Button ID="btnCheckAndActivate" runat="server" CssClass="btn btn-primary btn-large"
                                        Text="Έλεγχος και ενεργοποίηση" /></div>
                                <div class="clear">
                                </div>--%>

                            </asp:View>
                            <asp:View ID="vwPyramide" runat="server">
                                <div style="line-height:20px;font-size:14px;margin:20px 0;display:none;">
                                    <asp:HyperLink ID="lnkRegStep2" runat="server" NavigateUrl="~/Register.aspx?step=2" Visible="False">Complete step 2</asp:HyperLink><br />
                                    <asp:HyperLink ID="lnkRegStep3" runat="server" NavigateUrl="~/Register.aspx?step=3" Visible="False">Complete step 3</asp:HyperLink>
                                </div>


                                <div>
                                    <div class="login-welcome">
                                        <asp:Label ID="lblWelcomeLogin" runat="server" Text="Καλώς ήρθατε στην σελίδα συνεργασίας, [LOGINNAME]"></asp:Label>
                                    </div>

                                    <div style="width:600px;margin:20px auto;">
                                        <div class="lfloat" style="width:240px;">
                                            <asp:HyperLink ID="lnkLearnAbout" runat="server" CssClass="link-buttons button-left">Είστε νέος συνεργάτης μας μάθετε τη θα κερδίσετε μαζί μας</asp:HyperLink>
                                            <asp:HyperLink ID="lnkLearnAbout2" runat="server" CssClass="link">μάθετε τη θα κερδίσετε μαζί μας</asp:HyperLink>
                                        </div>
                                        <div class="rfloat" style="width:240px;">
                                            <asp:HyperLink ID="lnkEstimateCenter" runat="server" CssClass="link-buttons button-right" NavigateUrl="~/Members/ReferrerEstimate.aspx">Επιθυμείτε να έχετε μια εκτίμηση της συνεργασία μας; Επισκεφθείτε το Κέντρο Εκτιμήσεων.</asp:HyperLink>
                                            <asp:HyperLink ID="lnkEstimateCenter2" runat="server" CssClass="link">Εδώ μπορείς να υπολογίσεις και μόνη σου τα κέρδη σου!</asp:HyperLink>
                                        </div>
                                        <div class="clear">
                                        </div>
                                    </div>
                                    <hr />

                                    <div style="margin:20px 15px;">
                                        <div class="lfloat" style="width:60%;padding-top:7px;">
                                            <asp:Label ID="lblYourCodeDesc" runat="server" Text=""> Είναι ο κωδικός που μπορείτε να δώσετε στα μέλη που θέλετε να καλέσετε στην οικογένεια σας εναλλακτικά μπορείτε να δώσετε και το login name σας.&nbsp;  </asp:Label>
                                        </div>
                                        <div class="rfloat" style="width:38%;text-align: center;">
                                            <asp:Label ID="lblYourCode" runat="server" Text="Ο δικό σας κωδικός : "
                                                    Style="font-size: 11pt;" />
                                            <div style="text-align: center;font-size:15pt;font-weight:bold;">
                                                <asp:Label ID="lblYourCode2" runat="server" Text="[123456]" />
                                            </div>
                                        </div>
                                        <div class="clear">
                                        </div>
                                    </div>
                                    <hr />

                                    <div style="margin:20px 15px;">
                                        <table cellpadding="5" cellspacing="1" id="tblDates" runat="server" visible="false">
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblFromDate" runat="server" Text="Από Ημ/νία : "></asp:Label></td>
                                                <td>
                                                    <dx:ASPxDateEdit ID="dtFrom" runat="server" DisplayFormatString="dd/MM/yyyy" 
                                                        Width="120px"></dx:ASPxDateEdit>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblToDate" runat="server" Text="Εως "></asp:Label></td>
                                                <td><dx:ASPxDateEdit ID="dtTo" runat="server" DisplayFormatString="dd/MM/yyyy" 
                                                        Width="120px"></dx:ASPxDateEdit></td>
                                                <td>
                                                    <dx:ASPxButton ID="btnRefresh" runat="server" Text="Refresh"></dx:ASPxButton>
                                                </td>
                                            </tr>
                                        </table>

                                        <div style="width:370px;height:110px;margin:10px auto;background: transparent url(../images/referrer/counter.png) no-repeat top left; ">
                                            <div class="lfloat" style="width:50%;text-align:center;padding:38px 0 0 0;font-size:15pt;">
                                                <asp:Label ID="lblBalance" runat="server" Text="Balance for referrer: " />
                                            </div>
                                            <div class="rfloat" style="width:50%;text-align:left;padding:38px 0 0 0;font-size:20pt;font-weight:bold;">
                                                <div style="position:relative">
                                                    <div style="height: 36px;left: -1px;overflow: hidden;position: absolute;text-align: right;top: -4px;width: 160px;">
                                                        <asp:Label ID="lblTotalCredits" runat="server" Text="[CREDITS]"/>&nbsp;<asp:Literal ID="lblCredits" runat="server" Text=""/>
                                                    </div><%--Text="[CREDITS]Euro"--%>
                                                </div>
                                            </div>
                                            <div class="clear"></div>
                                                
                                        </div>
                                        <div class="balance-description">
                                            <asp:Label ID="lblAvailableBalanceText" runat="server" Text="Available balance : " CssClass="balance-text" /><asp:Label
                                                ID="lblAvailableBalance" runat="server" Text="[CREDITS]" /><%--Text="[CREDITS] EUR"--%>
                                            <br />
                                            <asp:Label ID="lblPendingBalanceText" runat="server" 
                                                Text="Pending balance : " CssClass="balance-text"  /><asp:Label
                                                ID="lblPendingBalance" runat="server" Text="[CREDITS]" /><%--Text="[CREDITS] EUR"--%>
                                            <br />
                                            <a id="lnkPayouts" runat="server" href="~/Members/ReferrerPayments.aspx">
                                                <asp:Label ID="lblPayedMoneyText" runat="server" Text="Last Payment on [DATE] : " CssClass="balance-text" /><asp:Label
                                                    ID="lblPayedMoney" runat="server" Text="[MONEY]" /></a><%--Text="[MONEY] EUR"--%>
                                            <br />
                                            <hr />
                                            <asp:Label ID="lblBalanceMoneyText" runat="server" Text="Total balance (available and pending) converted to " CssClass="balance-text" /><asp:Label
                                                ID="lblBalanceMoney" runat="server" Text="MONEY : [MONEY]" /> <%--Text="EUR : &euro;[MONEY]"--%>
                                            <br /> 
                                        </div>

                                    </div>
                                    <div style="margin:20px 0;">
                                        <asp:Label ID="lblBalanceTableDescr" runat="server" Text=""></asp:Label>
                                        <dx:ASPxGridView ID="gvBalance" runat="server" Width="100%" AutoGenerateColumns="False">
                                            <Columns>
                                                <dx:GridViewDataTextColumn Caption="Currency" FieldName="Currency" VisibleIndex="0" Visible="false">
                                                    <PropertiesTextEdit EncodeHtml="False">
                                                    </PropertiesTextEdit>
                                                    <CellStyle HorizontalAlign="Left">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Available" FieldName="Available" VisibleIndex="1">
                                                    <PropertiesTextEdit EncodeHtml="False">
                                                    </PropertiesTextEdit>
                                                    <CellStyle HorizontalAlign="Right">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Pending" FieldName="Pending" VisibleIndex="2">
                                                    <PropertiesTextEdit EncodeHtml="False">
                                                    </PropertiesTextEdit>
                                                    <CellStyle HorizontalAlign="Right">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Total" FieldName="Total" VisibleIndex="3">
                                                    <PropertiesTextEdit EncodeHtml="False">
                                                    </PropertiesTextEdit>
                                                    <CellStyle HorizontalAlign="Right">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                            <Paddings Padding="0px" />
                                            <Styles>
                                                <Header HorizontalAlign="Center" VerticalAlign="Middle" BackColor="#FF7A15" Font-Bold="True" ForeColor="White">
                                                    <Paddings Padding="0px" />
                                                </Header>
                                                <HeaderPanel>
                                                    <Paddings Padding="0px" />
                                                </HeaderPanel>
                                            </Styles>
                                        </dx:ASPxGridView>                                        
                                    </div>

                                    <div class="family">
                                        <div class="lfloat family-icon">
                                            <asp:Image ID="imgFamily" runat="server" ImageUrl="~/Images/referrer/family-ico.png" />
                                        </div>
                                        <div class="lfloat family-text">
                                            <asp:Label ID="lblFamilyMembersDesc" runat="server" Text=""></asp:Label>
                                            <div class="family-all-levels">
                                                <div class="lfloat family-level"><asp:Literal ID="lblLevel1" runat="server"></asp:Literal></div>
                                                <div class="lfloat family-level"><asp:Literal ID="lblLevel2" runat="server"></asp:Literal></div>
                                                <div class="lfloat family-level"><asp:Literal ID="lblLevel3" runat="server"></asp:Literal></div>
                                                <div class="lfloat family-level"><asp:Literal ID="lblLevel4" runat="server"></asp:Literal></div>
                                                <div class="lfloat family-level"><asp:Literal ID="lblLevel5" runat="server"></asp:Literal></div>
                                                <div class="clear">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clear">
                                        </div>
                                    </div>
                                    <hr />

                                    <div style="padding:20px 0 0 0;"><asp:Literal ID="lblReferrerContactText" runat="server"></asp:Literal></div>
                                    <table style="width:100%">
                                        <tr>
                                            <td style="width:70%"><uc3:ContactControl ID="ContactControl1" runat="server" /></td>
                                            <td style="width:30%"><table style="width:100%">
                                                    <tr>
                                                        <td valign="top" style="width:1%;text-align:center;"><asp:Image ID="Image1" runat="server" ImageUrl="~/images/referrer/contact.png" style="height:110px;border:1px solid #8C8C8E;"/></td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top" style="padding:20px 0 0 0;"><asp:Literal ID="lblReferrerContact_AfterPhoto_Text" runat="server"></asp:Literal></td>
                                                     </tr>
                                                </table></td>
                                        </tr>
                                    </table>
                                    
                                </div>
                                
                            </asp:View>
                        </asp:MultiView>
                    </div>
                </div>
            <%--</div>
        </div>
        <div class="clear">
        </div>
    </div>--%>

	<uc2:WhatIsIt ID="WhatIsIt1" runat="server" />


<asp:ObjectDataSource ID="odsAvailableCredits" runat="server" 
    OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
    TypeName="Dating.Server.Datasets.DLL.DSMembersTableAdapters.CustomerAvailableCreditsAdminTableAdapter">
    <SelectParameters>
        <asp:Parameter DefaultValue="1" Name="ReturnMinimum_AvailableCredits" 
            Type="Int32" />
        <asp:Parameter DefaultValue="True" Name="ShowOnline" Type="Boolean" />
        <asp:Parameter DefaultValue="" Name="LastActivityUTCDate" Type="DateTime" />
    </SelectParameters>
</asp:ObjectDataSource>



</asp:Content>

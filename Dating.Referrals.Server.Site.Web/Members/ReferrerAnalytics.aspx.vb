﻿Imports Dating.Server.Core.DLL
Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Datasets.DLL.DSMembers

Public Class ReferrerAnalytics
    Inherits ReferrerBasePage


    Dim _pageData As clsPageData
    Protected ReadOnly Property CurrentPageData As clsPageData
        Get
            If (_pageData Is Nothing) Then _pageData = New clsPageData(Context)
            Return _pageData
        End Get
    End Property


    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        Response.StatusCode = 400
        Response.Redirect(ResolveUrl("~/ErrorPages/FileNotFound.aspx"))
    End Sub


    Protected Overrides Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Return

        Try
            If clsCurrentContext.VerifyLogin() = True Then
                Me.MasterPageFile = "~/Members/Members2.Master"
            Else
                clsCurrentContext.ClearSession(Session)
                Response.Redirect(ResolveUrl("~/Login.aspx"), False)
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pnlErr.Visible = False

        Try
            If (Me.IsReferrer()) Then
                ' do nothing
            Else
                Response.Redirect("Referrer.aspx")
            End If

        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        'If (Not Page.IsPostBack) Then

        '    Try

        '        dtFrom.Date = DateTime.UtcNow.AddDays(-30)
        '        dtTo.Date = DateTime.UtcNow

        '        'If (Request.UrlReferrer IsNot Nothing) Then
        '        '    lnkBack.NavigateUrl = Request.UrlReferrer.ToString()
        '        'Else
        '        '    lnkBack.NavigateUrl = "~/Members/"
        '        'End If


        '        GetReferrerCreditsObject()
        '        LoadLAG()
        '    Catch ex As Exception
        '        WebErrorMessageBox(Me, ex, "Page_Load")
        '    End Try

        'End If

        Try
            If (Not Me.IsPostBack) Then
                LoadLAG()
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try


        Try
            SetReferrerUI()
            'If (Me.GetCurrentProfile().ReferrerParentId.HasValue AndAlso Me.GetCurrentProfile().ReferrerParentId > 0)Then
            '    'lblReferrerTitle.Text = "Επισκόπηση συνεργασίας"
            'Else
            '    mvMain.SetActiveView(vwSetReferrerParent)
            'End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
    End Sub


    Protected Sub LoadLAG()
        Try

            lblPageTitle.Text = CurrentPageData.GetCustomString("lblPageTitle")
            lnkBack.Text = CurrentPageData.GetCustomString("lnkBack")
            lblPageDesc.Text = CurrentPageData.GetCustomString("lblPageDesc")
            lblFromDate.Text = CurrentPageData.GetCustomString("lblFromDate")
            lblToDate.Text = CurrentPageData.GetCustomString("lblToDate")
            btnRefresh.Text = CurrentPageData.GetCustomString("btnRefresh")
            gvCreditsAnalisys.SettingsText.EmptyDataRow = CurrentPageData.GetCustomString("gvCreditsAnalisys.EmptyDataRow")


            lblItemsName.Text = Me.CurrentPageData.GetCustomString("CartReferrerAnalyticsView")
            'lnkViewDescription.Text = Me.CurrentPageData.GetCustomString("CartReferrerAnalyticsWhatIsIt")
            'lnkViewDescription.OnClientClick = OnMoreInfoClickFunc(
            '    lnkViewDescription.OnClientClick,
            '    ResolveUrl("~/Members/InfoWin.aspx?info=referreranalytics"),
            '    Me.CurrentPageData.GetCustomString("CartReferrerAnalyticsView"))
            WhatIsIt1.PopupControl.HeaderText = Me.CurrentPageData.GetCustomString("CartReferrerAnalyticsView")


            gvCreditsAnalisys.Columns("ReferrersLoginName").Caption = CurrentPageData.GetCustomString("AnalyticsTable-ReferrersLoginName")
            gvCreditsAnalisys.Columns("SalesCRD").Caption = CurrentPageData.GetCustomString("AnalyticsTable-SalesCRD")
            gvCreditsAnalisys.Columns("CommissionCRD").Caption = CurrentPageData.GetCustomString("AnalyticsTable-CommissionCRD")
            gvCreditsAnalisys.Columns("DateTimeCreated").Caption = CurrentPageData.GetCustomString("AnalyticsTable-DateTimeCreated")
            gvCreditsAnalisys.Columns("CommissionPercent").Caption = CurrentPageData.GetCustomString("AnalyticsTable-CommissionPercent")
            gvCreditsAnalisys.Columns("Description").Caption = CurrentPageData.GetCustomString("AnalyticsTable-Description")
            gvCreditsAnalisys.Columns("Level").Caption = CurrentPageData.GetCustomString("AnalyticsTable-Level")

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender

        Try
            Dim cPageBasic As Dating.Server.Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub


    Private Sub SetReferrerUI()
        SetDaysRange()
        GetReferrerCreditsObject(dtFrom.Value, dtTo.Value)
    End Sub



   

    Private Sub SetDaysRange()
        If (Not Page.IsPostBack) Then
            If (SessionVariables.ReferrerCredits IsNot Nothing) Then
                Try
                    dtFrom.Value = SessionVariables.ReferrerCredits.DateFrom
                    dtTo.Value = SessionVariables.ReferrerCredits.DateTo
                Catch ex As Exception
                End Try
            End If

            If (dtFrom.Value Is Nothing OrElse dtTo.Value Is Nothing) Then
                dtFrom.Date = DateTime.UtcNow.AddDays(-clsReferrer.DefaultDaysRange)
                dtTo.Date = DateTime.UtcNow.AddDays(1)
            End If
        End If
    End Sub


    'Private affCredits As clsReferrerCredits
    'Private Function GetReferrerCreditsObject(dateFrom As DateTime?, dateTo As DateTime?) As clsReferrerCredits
    '    If (affCredits Is Nothing) Then

    '        Try
    '            'If (SessionVariables.ReferrerCredits IsNot Nothing) Then
    '            '    affCredits = SessionVariables.ReferrerCredits


    '            '    If (affCredits.DateCreated.AddHours(1) < Date.UtcNow) Then
    '            '        ' if time expired than clear object
    '            '        affCredits = Nothing

    '            '    ElseIf (affCredits.DateFrom <> dateFrom OrElse affCredits.DateTo <> dateTo) Then
    '            '        ' if dates range is different than clear object
    '            '        affCredits = Nothing

    '            '    End If
    '            'End If


    '            If (affCredits Is Nothing) Then
    '                affCredits = New clsReferrerCredits(Me.MasterProfileId, Me.SessionVariables.DataRecordLoginMemberReturn.LoginName, dateFrom, dateTo)
    '                affCredits.Load()
    '                SessionVariables.ReferrerCredits = affCredits
    '            End If

    '        Catch ex As Exception
    '            Throw
    '        End Try
    '    End If

    '    Return affCredits
    'End Function


    Public Function GetCreditsAnalisys(DateFrom As DateTime?, DateTo As DateTime?, TargetLevel As Integer?, affCredits1 As clsReferrerCredits) As DSCustom.ReferrersAnalyticPyramidDataTable
        Using dtPyramid As New DSCustom.ReferrersAnalyticPyramidDataTable()




            Try

                If (TargetLevel Is Nothing) Then
                    TargetLevel = -1
                End If

                If (DateFrom Is Nothing OrElse DateTo Is Nothing) Then
                    DateFrom = Nothing
                    DateTo = Nothing
                End If

                Dim sql = <sql><![CDATA[
SELECT * FROM (
    SELECT [EUS_MessageID]
		  ,[REF_IsMaster]
		  ,[REF_Status]
		  ,[ReferrerParentId]
		  ,[CustomerLoginName]
		  ,[ReferrersLoginName]
		  ,Date = [DateTimeToCreate]
		  ,[CustomerId]
		  ,[ReferrerID]
		  ,[StatusID]
		  ,[IsSent]
		  ,[IsReceived]
		  ,[CopyMessageID]
		  ,[IsDeleted]
		  ,[REF_L1_Money]
		  ,[REF_L2_Money]
		  ,[REF_L3_Money]
		  ,[REF_L4_Money]
		  ,[REF_L5_Money]
		  ,[REF_L1_Commission]
		  ,[REF_L2_Commission]
		  ,[REF_L3_Commission]
		  ,[REF_L4_Commission]
		  ,[REF_L5_Commission]
		  ,[Cost]
		  ,cc.[CustomerCreditsId]
		  ,cc.[EUS_CreditsTypeID]
		  ,cc.DateBill
		  ,ct.CreditsType
		  ,Expired = cast('false' as bit)
		  ,DatePurchased = ''
		  ,Action = ''
		  ,Description = ''
		  ,CommissionCRD = 0  		
	    FROM [vw_REF_Commission] cc
            with(nolock)
		LEFT OUTER JOIN EUS_CreditsType AS ct ON ct.EUS_CreditsTypeID = cc.EUS_CreditsTypeID 
		WHERE cc.[ReferrerID] in (@SubReferrers)
        AND (
            (@DateFrom is null AND @DateTo is null) OR 
            (DateBill >= @DateFrom AND DateBill <= @DateTo)
        )
) AS t
ORDER BY [Date] DESC
]]></sql>.Value


                Dim level As Integer = 0
                Dim mySubReferrers As New List(Of Integer)

                While (level < clsReferrer.StopLevel)
                    level = level + 1

                    Try
                        mySubReferrers.Clear()

                        Dim drs As DSCustom.ReferrersRepositoryDataRow() = affCredits1.GetReferrersForLevel(level)
                        Dim c As Integer
                        For c = 0 To drs.Length - 1
                            Dim profileId As Integer = drs(c)("ProfileID")
                            mySubReferrers.Add(profileId)
                        Next

                        If (mySubReferrers.Count > 0 AndAlso (level = TargetLevel OrElse TargetLevel = -1)) Then
                            Dim sb As New StringBuilder()
                            For c = 0 To mySubReferrers.Count - 1
                                Dim _profileId As Integer = mySubReferrers(c)
                                sb.Append(_profileId & ",")
                            Next
                            If (sb.Length > 0) Then
                                sb.Remove(sb.Length - 1, 1)
                            End If

                            Dim execSql As String = sql.Replace("@SubReferrers", sb.ToString())
                            Using con As New SqlClient.SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("AppDBconnectionString").ConnectionString)

                                Dim cmd As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(con, execSql)
                                cmd.Parameters.Add(New SqlClient.SqlParameter("@DateFrom", DateFrom))
                                cmd.Parameters.Add(New SqlClient.SqlParameter("@DateTo", DateTo))

                                Dim dtCreditsInfo As DataTable = DataHelpers.GetDataTable(cmd)

                                For c = 0 To dtCreditsInfo.Rows.Count - 1
                                    Dim dr As DataRow = dtCreditsInfo.Rows(c)
                                    Dim _dr As DSCustom.ReferrersAnalyticPyramidRow = dtPyramid.NewReferrersAnalyticPyramidRow()
                                    _dr.CustomerCreditsId = dr("CustomerCreditsId")
                                    _dr.ReferrerId = dr("ReferrerId")
                                    _dr.ReferrersLoginName = dr("ReferrersLoginName")
                                    _dr.SalesCRD = dr("Cost")
                                    '_dr.CommissionCRD = GetCurrentReferrerCommissions().CalcCommissionForLevel(dr("EuroAmount"), level)
                                    _dr.DateTimeCreated = DirectCast(dr("Date"), DateTime)
                                    _dr.Description = dr("Description")
                                    _dr.Level = level

                                    Select Case level
                                        Case 1
                                            If (Not dr.IsNull("REF_L1_Money")) Then _dr.CommissionCRD = dr("REF_L1_Money")
                                            If (Not dr.IsNull("REF_L1_Commission")) Then _dr.CommissionPercent = dr("REF_L1_Commission")

                                        Case 2
                                            If (Not dr.IsNull("REF_L2_Money")) Then _dr.CommissionCRD = dr("REF_L2_Money")
                                            If (Not dr.IsNull("REF_L2_Commission")) Then _dr.CommissionPercent = dr("REF_L2_Commission")

                                        Case 3
                                            If (Not dr.IsNull("REF_L3_Money")) Then _dr.CommissionCRD = dr("REF_L3_Money")
                                            If (Not dr.IsNull("REF_L3_Commission")) Then _dr.CommissionPercent = dr("REF_L3_Commission")

                                        Case 4
                                            If (Not dr.IsNull("REF_L4_Money")) Then _dr.CommissionCRD = dr("REF_L4_Money")
                                            If (Not dr.IsNull("REF_L4_Commission")) Then _dr.CommissionPercent = dr("REF_L4_Commission")

                                        Case 5
                                            If (Not dr.IsNull("REF_L5_Money")) Then _dr.CommissionCRD = dr("REF_L5_Money")
                                            If (Not dr.IsNull("REF_L5_Commission")) Then _dr.CommissionPercent = dr("REF_L5_Commission")

                                    End Select

                                    _dr.CreditsType = dr("CreditsType")
                                    _dr.PersonsNavigateUrl = "~/Members/ReferrerCart.aspx?affid=" & dr("ReferrerId") & "&lvl=" & level
                                    _dr.CustomerLoginName = dr("CustomerLoginName")
                                    dtPyramid.Rows.Add(_dr)
                                Next
                            End Using
                        End If

                    Catch ex As Exception
                        Throw
                    End Try

                    If (mySubReferrers.Count = 0 OrElse level = TargetLevel) Then Exit While
                End While

            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "GetCreditsAnalisys")
            End Try


            Return dtPyramid
        End Using
    End Function


    Protected Sub gvCreditsAnalisys_CustomColumnDisplayText(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewColumnDisplayTextEventArgs) Handles gvCreditsAnalisys.CustomColumnDisplayText
        Try
            If e.Column.FieldName = "Description" Then

                Dim dr As System.Data.DataRow = gvCreditsAnalisys.GetDataRow(e.VisibleRowIndex)
                If (dr IsNot Nothing) Then
                    Dim credits As Double = 0
                    If (Not dr.IsNull("SalesCRD")) Then
                        credits = Convert.ToDouble(dr("SalesCRD"))
                        credits = Math.Abs(credits)
                    End If


                    If (dr("CreditsType") = "UNLOCK_CONVERSATION") Then
                        e.DisplayText = BasePage.ReplaceCommonTookens(Me.CurrentPageData.GetCustomString("CreditsDescription_Used2"), dr("CustomerLoginName").ToString())
                        e.DisplayText = e.DisplayText.Replace("###EURO###", credits.ToString("0.00"))

                    ElseIf (dr("CreditsType") = "UNLOCK_MESSAGE_READ") Then
                        e.DisplayText = BasePage.ReplaceCommonTookens(Me.CurrentPageData.GetCustomString("CreditsDescription_UsedUnlockMessage_READ2"), dr("CustomerLoginName").ToString())
                        e.DisplayText = e.DisplayText.Replace("###EURO###", credits.ToString("0.00"))

                    ElseIf (dr("CreditsType") = "UNLOCK_MESSAGE_SEND") Then
                        e.DisplayText = BasePage.ReplaceCommonTookens(Me.CurrentPageData.GetCustomString("CreditsDescription_UsedUnlockMessage_SEND2"), dr("CustomerLoginName").ToString())
                        e.DisplayText = e.DisplayText.Replace("###EURO###", credits.ToString("0.00"))

                    End If
                End If

            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Protected Sub btnRefresh_Click(sender As Object, e As EventArgs) Handles btnRefresh.Click
        Try

            gvCreditsAnalisys.DataBind()

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "btnGo")
        End Try
    End Sub






    Protected Sub odsCreditsAnalisys_Selecting(sender As Object, e As System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs) Handles odsCreditsAnalisys.Selecting
        e.InputParameters("affCredits1") = GetReferrerCreditsObject(dtFrom.Value, dtTo.Value)
    End Sub

End Class
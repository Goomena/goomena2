﻿Imports Dating.Server.Core.DLL
Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Datasets.DLL.DSMembers

Public Class ReferrerCart
    Inherits BasePage

#Region "Props"

    Dim _pageData As clsPageData
    Protected ReadOnly Property CurrentPageData As clsPageData
        Get
            If (_pageData Is Nothing) Then _pageData = New clsPageData(Context)
            Return _pageData
        End Get
    End Property




    Private ReadOnly Property LevelParam As Integer
        Get
            Dim level As Integer
            If (Not String.IsNullOrEmpty(Request.QueryString("lvl"))) Then
                Integer.TryParse(Request.QueryString("lvl"), level)
            End If
            Return level
        End Get
    End Property


    Private ReadOnly Property ReferrerIdParam As Integer
        Get
            Dim affiliateid As Integer
            If (Not String.IsNullOrEmpty(Request.QueryString("affid"))) Then
                Integer.TryParse(Request.QueryString("affid"), affiliateid)
            End If
            Return affiliateid
        End Get
    End Property


    'Private ReadOnly Property StartDaysBefore As Integer
    '    Get
    '        Dim days As Integer
    '        If (Not String.IsNullOrEmpty(hdfStartDaysBefore.Value)) Then
    '            Integer.TryParse(hdfStartDaysBefore.Value, days)
    '        End If
    '        Return days
    '    End Get
    'End Property

#End Region


    Protected Overrides Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        Try
            If clsCurrentContext.VerifyLogin() = True Then
                Me.MasterPageFile = "~/Members/Members2.Master"
            Else
                clsCurrentContext.ClearSession(Session)
                Response.Redirect(ResolveUrl("~/Login.aspx"), False)
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pnlErr.Visible = False

        Try
            If (Me.IsReferrer()) Then
                ' do nothing
            Else
                Response.Redirect("Referrer.aspx")
            End If

        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        'If (Not Page.IsPostBack) Then

        '    Try

        '        'If (Request.UrlReferrer IsNot Nothing) Then
        '        '    lnkBack.NavigateUrl = Request.UrlReferrer.ToString()
        '        'Else
        '        '    lnkBack.NavigateUrl = "~/Members/"
        '        'End If


        '        GetReferrerCreditsObject()
        '        LoadLAG()
        '        LoadData()


        '    Catch ex As Exception
        '        WebErrorMessageBox(Me, ex, "Page_Load")
        '    End Try

        'End If


        Try
            If (Not Me.IsPostBack) Then
                LoadLAG()
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try


        Try
            SetReferrerUI()
            'If (Me.GetCurrentProfile().ReferrerParentId.HasValue AndAlso Me.GetCurrentProfile().ReferrerParentId > 0)Then
            '    'lblReferrerTitle.Text = "Επισκόπηση συνεργασίας"
            'Else
            '    mvMain.SetActiveView(vwSetReferrerParent)
            'End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub



    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
        SetReferrerUI()
    End Sub


    Protected Sub LoadLAG()
        Try

            lblPageTitle.Text = CurrentPageData.GetCustomString("lblPageTitle")
            lnkBack.Text = CurrentPageData.GetCustomString("lnkBack")
            lblPageDesc.Text = CurrentPageData.GetCustomString("lblPageDesc")
            lblUserName.Text = CurrentPageData.GetCustomString("lblUserName")
            lblParenBy.Text = CurrentPageData.GetCustomString("lblParenBy")
            lblParenByDate.Text = CurrentPageData.GetCustomString("lblParenByDate")
            lblUserDates.Text = CurrentPageData.GetCustomString("lblUserDates")
            lblUserDatesCreate.Text = CurrentPageData.GetCustomString("lblUserDatesCreate")
            lblUserDatesLastLogin.Text = CurrentPageData.GetCustomString("lblUserDatesLastLogin")
            lblLastDaysActionsTitle.Text = CurrentPageData.GetCustomString("lblLastDaysActionsTitle")
            lblLastLogins.Text = CurrentPageData.GetCustomString("lblLastLogins")
            lblLastSendings.Text = CurrentPageData.GetCustomString("lblLastSendings")
            lblLastReceivings.Text = CurrentPageData.GetCustomString("lblLastReceivings")
            lblLastAnswers.Text = CurrentPageData.GetCustomString("lblLastAnswers")
            lblLastSalesCRD.Text = CurrentPageData.GetCustomString("lblLastSalesCRD")
            lblLastReceiveCommissions.Text = CurrentPageData.GetCustomString("lblLastReceiveCommissions")
            lblLastReceiveCommissionsAllVal.Text = CurrentPageData.GetCustomString("lblLastReceiveCommissionsAllVal")
            lnkViewCommissions.Text = CurrentPageData.GetCustomString("lnkViewCommissions")


            lblFromDate.Text = CurrentPageData.GetCustomString("lblFromDate")
            lblToDate.Text = CurrentPageData.GetCustomString("lblToDate")
            btnRefresh.Text = CurrentPageData.GetCustomString("btnRefresh")


            lblItemsName.Text = Me.CurrentPageData.GetCustomString("CartReferrerCartView")
            'lnkViewDescription.Text = Me.CurrentPageData.GetCustomString("CartReferrerCartWhatIsIt")
            'lnkViewDescription.OnClientClick = OnMoreInfoClickFunc(
            '    lnkViewDescription.OnClientClick,
            '    ResolveUrl("~/Members/InfoWin.aspx?info=referrercart"),
            '    Me.CurrentPageData.GetCustomString("CartReferrerCartView"))
            WhatIsIt1.PopupControl.HeaderText = Me.CurrentPageData.GetCustomString("CartReferrerCartView")

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender

        Try
            Dim cPageBasic As Dating.Server.Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub


    Private Sub SetReferrerUI()
        SetDaysRange()
        GetReferrerCreditsObject(dtFrom.Date, dtTo.Date)
        LoadData(dtFrom.Date, dtTo.Date)
    End Sub



    Private _referrerCommissions As clsReferrerCommissions
    Private Function GetCurrentReferrerCommissions() As clsReferrerCommissions
        If (_referrerCommissions Is Nothing) Then
            _referrerCommissions = New clsReferrerCommissions()
            _referrerCommissions.Load(Me.MasterProfileId)
        End If
        Return _referrerCommissions
    End Function



    Private Sub SetDaysRange()
        If (Not Page.IsPostBack) Then
            If (SessionVariables.ReferrerCredits IsNot Nothing) Then
                Try
                    dtFrom.Value = SessionVariables.ReferrerCredits.DateFrom
                    dtTo.Value = SessionVariables.ReferrerCredits.DateTo
                Catch ex As Exception
                End Try
            End If

            If (dtFrom.Value Is Nothing OrElse dtTo.Value Is Nothing) Then
                dtFrom.Date = DateTime.UtcNow.AddDays(-clsReferrer.DefaultDaysRange)
                dtTo.Date = DateTime.UtcNow.AddDays(1)
            End If
        End If
    End Sub


    Private affCredits As clsReferrerCredits
    Private Function GetReferrerCreditsObject(dateFrom As DateTime?, dateTo As DateTime?)
        If (affCredits Is Nothing) Then

            Try
                'If (SessionVariables.ReferrerCredits IsNot Nothing) Then
                '    affCredits = SessionVariables.ReferrerCredits


                '    If (affCredits.DateCreated.AddHours(1) < Date.UtcNow) Then
                '        ' if time expired than clear object
                '        affCredits = Nothing

                '    ElseIf (affCredits.DateFrom <> dateFrom OrElse affCredits.DateTo <> dateTo) Then
                '        ' if dates range is different than clear object
                '        affCredits = Nothing

                '    End If
                'End If


                If (affCredits Is Nothing) Then
                    Dim _dtFrom As DateTime?
                    Dim _dtTo As DateTime?
                    If (dateFrom.HasValue) Then _dtFrom = dateFrom.Value.Date
                    If (dateTo.HasValue) Then _dtTo = dateTo.Value.Date.AddDays(1).AddMilliseconds(-1)

                    affCredits = New clsReferrerCredits(Me.MasterProfileId, Me.SessionVariables.MemberData.LoginName, _dtFrom, _dtTo)
                    'affCredits = New clsReferrerCredits(Me.MasterProfileId, Me.SessionVariables.MemberData.LoginName, dateFrom, dateTo)
                    affCredits.Load()
                    SessionVariables.ReferrerCredits = affCredits
                End If

            Catch ex As Exception
                Throw
            End Try
        End If

        Return affCredits
    End Function



    Private Sub LoadData(DateFrom As DateTime?, DateTo As DateTime?)
        Try

            Dim daysDiff As Integer = clsReferrer.DefaultDaysRange
            If (DateTo.HasValue AndAlso DateFrom.HasValue) Then daysDiff = (DateTo.Value - DateFrom.Value).Days

            ' load variables
            '''''''''''''''''''''''
            Dim Logins As Integer = 0
            Dim SendActions As Integer = 0
            Dim Receive As Integer = 0
            Dim Answers As Integer = 0
            Dim SalesCRD As Integer = 0
            Dim CommissionCRD As Integer = 0

            Dim ds As DSMembersViews = DataHelpers.GetProfileStatistics(ReferrerIdParam, daysDiff)
            Dim dt As DSMembersViews.GetProfileStatisticsDataTable = ds.GetProfileStatistics
            If (dt.Rows.Count > 0) Then
                Dim dr As DSMembersViews.GetProfileStatisticsRow = dt.Rows(0)
                Logins = dr.LoginsCount
                SendActions = dr.SentMessagesCount + dr.SentOffersCount
                Receive = dr.ReceivedMessagesCount + dr.ReceivedOffersCount
                Answers = dr.AnsweredMessagesCount + dr.AnsweredOffersCount
            End If

            GetReferrerCreditsObject(DateFrom, DateTo)
            SalesCRD = affCredits.GetReferrerSales(ReferrerIdParam)
            CommissionCRD = affCredits.GetReferrerCommissions(ReferrerIdParam)

            ' load UI controls
            '''''''''''''''''''''''
            Dim affiliateProf As EUS_Profile = DataHelpers.GetEUS_ProfileByProfileID(Me.CMSDBDataContext, ReferrerIdParam)

            lblUserName.Text = lblUserName.Text.Replace("[LOGINNAME]", affiliateProf.LoginName)
            If (affiliateProf.ReferrerParentId IsNot Nothing) Then
                Dim affiliateParentProf As EUS_Profile = DataHelpers.GetEUS_ProfileByProfileID(Me.CMSDBDataContext, affiliateProf.ReferrerParentId)
                lblParenByUser.Text = affiliateParentProf.LoginName
            End If
            lblParenByDate.Text = lblParenByDate.Text.Replace("[DATE]", "")

            lblUserDatesCreate.Text = lblUserDatesCreate.Text.Replace("[CREATEDATE]", affiliateProf.DateTimeToRegister.Value.ToString("dd/MM/yyyy HH:mm"))

            If (affiliateProf.LastLoginDateTime.HasValue) Then
                lblUserDatesLastLogin.Text = lblUserDatesLastLogin.Text.Replace("[LASTLOGINDATE]", affiliateProf.LastLoginDateTime.Value.ToString("dd/MM/yyyy HH:mm"))
            Else
                lblUserDatesLastLogin.Text = lblUserDatesLastLogin.Text.Replace("[LASTLOGINDATE]", "")
            End If

            lblLastDaysActionsTitle.Text = lblLastDaysActionsTitle.Text.Replace("[LASTDAYS]", daysDiff)
            lblLastLoginsVal.Text = Logins
            lblLastSendingsVal.Text = SendActions
            lblLastReceivingsVal.Text = Receive
            lblLastAnswersVal.Text = Answers
            lblLastSalesCRDVal.Text = SalesCRD
            lblLastReceiveCommissionsAllVal.Text = CommissionCRD
            lnkViewCommissions.NavigateUrl = "~/Members/ReferrerSupplies.aspx?lvl=" & LevelParam

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "LoadData")
        End Try

    End Sub


    Protected Sub btnRefresh_Click(sender As Object, e As EventArgs) Handles btnRefresh.Click
        Try

            SetReferrerUI()

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "btnGo")
        End Try
    End Sub


End Class
﻿Imports Dating.Server.Core.DLL
Imports ReferralsDLL = Dating.Referrals.Server.Core.DLL
Imports SiteDLL = Dating.Server.Core.DLL
Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Datasets.DLL.DSMembers

Public Class ReferrerSettings
    Inherits ReferrerBasePage


    Protected Property PageHasErrors As Boolean


    Dim _pageData As clsPageData
    Protected ReadOnly Property CurrentPageData As clsPageData
        Get
            If (_pageData Is Nothing) Then _pageData = New clsPageData(Context)
            Return _pageData
        End Get
    End Property


    Protected Overrides Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        Try
            If (Me.IsReferrer()) Then
                ' do nothing
            Else
                Response.Redirect("Referrer.aspx")
            End If

        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Try
            If clsCurrentContext.VerifyLogin() = True Then
                Me.MasterPageFile = "~/Members/Members2.Master"
            Else
                clsCurrentContext.ClearSession(Session)
                Response.Redirect(ResolveUrl("~/Logout.aspx"), False)
            End If
        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


    Private Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        Try
            If (Me.IsReferrer()) Then
                ' do nothing
            Else
                Response.Redirect("Referrer.aspx")
            End If

        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Try
            pnlErr.Visible = False
            ''pnlPasswordErr.Visible = False



            If (Not Me.IsPostBack) Then
                If (Request.UrlReferrer IsNot Nothing) Then
                    lnkBack.NavigateUrl = Request.UrlReferrer.ToString()
                Else
                    lnkBack.NavigateUrl = "~/Members/"
                End If
            End If


            If (Not Me.IsPostBack) Then
                LoadLAG()
                Me.LoadProfile(Me.MasterProfileId)
                ToggleCollapsibles(True)
            End If

        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub


    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender

        Try
            Dim cPageBasic As Dating.Server.Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub


    Private Sub ToggleCollapsibles(show As Boolean)
        If (show) Then
            lnkExpCollBasicInfo.Text = "-"
            h2BasicHeader.Attributes("title") = globalStrings.GetCustomString("msg_ClickToCollapse", GetLag())
            pe_formBasicInfo.Attributes.CssStyle.Remove("display")

            lnkExpCollPayoutInfo.Text = "-"
            h2PayoutInfoHead.Attributes("title") = globalStrings.GetCustomString("msg_ClickToCollapse", GetLag())
            pe_formPayoutInfo.Attributes.CssStyle.Remove("display")
        Else
            lnkExpCollBasicInfo.Text = "+"
            h2BasicHeader.Attributes("title") = globalStrings.GetCustomString("msg_ClickToExpand", GetLag())
            pe_formBasicInfo.Attributes.CssStyle("display") = "none"

            lnkExpCollPayoutInfo.Text = "+"
            h2PayoutInfoHead.Attributes("title") = globalStrings.GetCustomString("msg_ClickToExpand", GetLag())
            pe_formPayoutInfo.Attributes.CssStyle("display") = "none"
        End If
    End Sub


    Public Overrides Sub Master_LanguageChanged()

        If (Me.Visible) Then
            Me._pageData = Nothing
            LoadLAG()
            Me.LoadProfile(Me.MasterProfileId)
        End If

    End Sub


    Protected Sub LoadLAG()
        Try
            Dim cPageBasic As Dating.Server.Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic


            Web.ClsCombos.FillComboUsingDatatable(SYS_CountriesGEOHelper.gDSListsGEO__IsEnabled_PrintableNameASC.SYS_CountriesGEO, "PrintableName", "Iso", cbCountry, True, "PrintableName")
            Dim def As DevExpress.Web.ASPxEditors.ListEditItem = Nothing
            If (def IsNot Nothing) Then
                def.Selected = True
            Else
                def = New DevExpress.Web.ASPxEditors.ListEditItem(CurrentPageData.GetCustomString("cbCountry_default"), "-1")
                def.Selected = True
                cbCountry.Items.Insert(0, def)
            End If


            'Web.ClsCombos.FillCombo(gLAG.ConnectionString, "SYS_CountriesGEO", "PrintableName", "Iso", cbBankCountryID, True, False, "PrintableName", "ASC")
            Web.ClsCombos.FillComboUsingDatatable(SYS_CountriesGEOHelper.gDSListsGEO__IsEnabled_PrintableNameASC.SYS_CountriesGEO, "PrintableName", "Iso", cbBankCountryID, True, "PrintableName")
            Dim def2 As DevExpress.Web.ASPxEditors.ListEditItem = Nothing
            If (def2 IsNot Nothing) Then
                def2.Selected = True
            Else
                def2 = New DevExpress.Web.ASPxEditors.ListEditItem(CurrentPageData.GetCustomString("cbCountry_default"), "-1")
                def2.Selected = True
                cbBankCountryID.Items.Insert(0, def2)
            End If

            Dim dt As DataTable = DataHelpers.GetAvailable_AFF_PayoutTypes()
            Web.ClsCombos.FillComboUsingDatatable(dt, "Title", "AFF_PayoutTypeID", cbPayoutTypeID, True)
            Dim def3 As DevExpress.Web.ASPxEditors.ListEditItem = Nothing
            If (def3 IsNot Nothing) Then
                def3.Selected = True
            Else
                def3 = New DevExpress.Web.ASPxEditors.ListEditItem(CurrentPageData.GetCustomString("cbPayoutTypeID_default"), "-1")
                def3.Selected = True
                cbPayoutTypeID.Items.Insert(0, def3)
            End If

            SetControlsValue(Me, CurrentPageData)

            lblPageTitle.Text = CurrentPageData.GetCustomString("lblPageTitle")
            lnkBack.Text = CurrentPageData.GetCustomString("lnkBack")
            lblPageDesc.Text = CurrentPageData.GetCustomString("lblPageDesc")
            msg_BasicHeader.Text = CurrentPageData.GetCustomString("msg_BasicHeader")
            msg_BasicDesc.Text = CurrentPageData.GetCustomString("msg_BasicDesc")
            msg_Login.Text = CurrentPageData.GetCustomString("msg_Login")
            msg_ParentAff.Text = CurrentPageData.GetCustomString("msg_ParentAff")

            msg_FirstName.Text = CurrentPageData.GetCustomString("msg_FirstName")
            msg_LastName.Text = CurrentPageData.GetCustomString("msg_LastName")
            msg_eMail.Text = CurrentPageData.GetCustomString("msg_eMail")
            msg_Mobile.Text = CurrentPageData.GetCustomString("msg_Mobile")
            msg_CountryText.Text = CurrentPageData.GetCustomString("msg_CountryText")
            msg_RegionText.Text = CurrentPageData.GetCustomString("msg_RegionText")
            msg_CityText.Text = CurrentPageData.GetCustomString("msg_CityText")

            btnProfileUpd.Text = CurrentPageData.GetCustomString("btnProfileUpd")
            msg_Payout_MinBalanceExplanation.Text = msg_Payout_MinBalanceExplanation.Text.Replace(" Euro", "")

            'txteMail.ValidationSettings.RegularExpression.ValidationExpression = gEmailAddressValidationRegex
            'txteMail.ValidationSettings.RegularExpression.ErrorText = ""
            If (clsConfigValues.Get__validate_email_address_normal()) Then
                txtPayout_email.ValidationSettings.RegularExpression.ValidationExpression = gEmailAddressValidationRegex
            Else
                txtPayout_email.ValidationSettings.RegularExpression.ValidationExpression = gEmailAddressValidationRegex_Simple
            End If
            txtPayout_email.ValidationSettings.RegularExpression.ErrorText = ""


            lblItemsName.Text = Me.CurrentPageData.GetCustomString("CartReferrerSettingsView")
            'lnkViewDescription.Text = Me.CurrentPageData.GetCustomString("CartReferrerSettingsWhatIsIt")
            'lnkViewDescription.OnClientClick = OnMoreInfoClickFunc(
            '    lnkViewDescription.OnClientClick,
            '    ResolveUrl("~/Members/InfoWin.aspx?info=referrersettings"),
            '    Me.CurrentPageData.GetCustomString("CartReferrerSettingsView"))
            WhatIsIt1.PopupControl.HeaderText = Me.CurrentPageData.GetCustomString("CartReferrerSettingsView")


            lblCode.Text = CurrentPageData.GetCustomString("lblYourCode")
            lblCodeNum.Text = CreateReferrerCode() 'Me.MasterProfileId

            lblBottomNotes.Text = CurrentPageData.GetCustomString("lblBottomNotes")
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub



    Protected Sub btnProfileUpd_Click(sender As Object, e As EventArgs) Handles btnProfileUpd.Click
        Try
            'Dim profileId As Integer = Me.Session("ReferrerID")
            ValidateInput()
            If (Page.IsValid AndAlso Not PageHasErrors) Then
                SaveProfile(Me.MasterProfileId)
                'SaveProfile_ManuallyApprovingFields(Me.MasterProfileId)
                MyBase.ResetEUS_ProfileObject()
                Me.LoadProfile(Me.MasterProfileId)
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Private Sub ValidateInput()

        PageHasErrors = False
        Try

            pnleMailErr.Visible = False
            Dim regMatch As Boolean
            If (clsConfigValues.Get__validate_email_address_normal()) Then
                regMatch = Regex.IsMatch(lbleMail.Text, gEmailAddressValidationRegex)
            Else
                regMatch = Regex.IsMatch(lbleMail.Text, gEmailAddressValidationRegex_Simple)
            End If

            If (lbleMail.Text.Trim() = "" OrElse Not regMatch) Then
                pnleMailErr.Visible = True
                If (lbleMailErr.Text.Trim() = "") Then
                    lbleMailErr.Text = CurrentPageData.GetCustomString("txteMail_Required_ErrorText")
                ElseIf (Not regMatch) Then
                    lbleMailErr.Text = CurrentPageData.GetCustomString("txteMail_Invalid_ErrorText")
                End If
                lbleMail.Focus()
                PageHasErrors = True
            End If


            pnlPayout_emailErr.Visible = False
            If (liPayout_email.Visible) Then

                If (clsConfigValues.Get__validate_email_address_normal()) Then
                    regMatch = Regex.IsMatch(txtPayout_email.Text, gEmailAddressValidationRegex)
                Else
                    regMatch = Regex.IsMatch(txtPayout_email.Text, gEmailAddressValidationRegex_Simple)
                End If

                If (txtPayout_email.Text.Trim() = "" OrElse Not regMatch) Then
                    pnlPayout_emailErr.Visible = True
                    If (txtPayout_email.Text.Trim() = "") Then
                        lblPayout_emailErr.Text = CurrentPageData.GetCustomString("txtPayout_email_Required_ErrorText")
                    ElseIf (Not regMatch) Then
                        lblPayout_emailErr.Text = CurrentPageData.GetCustomString("txtPayout_email_Invalid_ErrorText")
                    End If
                    txtPayout_email.Focus()
                    PageHasErrors = True
                End If
            End If


            pnlPayout_CompanyPostalCodeErr.Visible = False
            If (cbPayoutTypeID.SelectedIndex > 0) Then

                If (Not PageHasErrors AndAlso liPayout_CompanyPostalCode.Visible) Then

                    txtPayout_CompanyPostalCode.Text = AppUtils.GetZip(txtPayout_CompanyPostalCode.Text)
                    If (txtPayout_CompanyPostalCode.Text.Trim() = "") Then 'AndAlso hdfLocationStatus.Value = "zip"
                        pnlPayout_CompanyPostalCodeErr.Visible = True
                        lblPayout_CompanyPostalCodeErr.Text = CurrentPageData.GetCustomString("txtPayout_CompanyPostalCode_Required_ErrorText")
                        txtPayout_CompanyPostalCode.Focus()
                        PageHasErrors = True
                    End If


                    'Dim zipstr As String = Regex.Replace(txtPayout_CompanyPostalCode.Text, "[^\d]", "")
                    'Dim zipNum As Integer
                    'If (Integer.TryParse(zipstr, zipNum)) Then
                    '    zipstr = zipNum.ToString("### ##")
                    '    txtPayout_CompanyPostalCode.Text = zipstr
                    'End If

                    'If (zipstr = "") Then
                    '    pnlPayout_CompanyPostalCodeErr.Visible = True

                    '    If (clsConfigValues.Get__validate_email_address_normal()) Then
                    '        regMatch = Regex.IsMatch(txtPayout_CompanyPostalCode.Text, gEmailAddressValidationRegex)
                    '    Else
                    '        regMatch = Regex.IsMatch(txtPayout_CompanyPostalCode.Text, gEmailAddressValidationRegex_Simple)
                    '    End If

                    '    If (txtPayout_CompanyPostalCode.Text.Trim() = "") Then
                    '        lblPayout_CompanyPostalCodeErr.Text = CurrentPageData.GetCustomString("txtPayout_CompanyPostalCode_Required_ErrorText")
                    '    ElseIf (Not regMatch) Then
                    '        lblPayout_CompanyPostalCodeErr.Text = CurrentPageData.GetCustomString("txtPayout_CompanyPostalCode_Invalid_ErrorText")
                    '    End If
                    '    txtPayout_CompanyPostalCode.Focus()
                    '    PageHasErrors = True
                    'End If
                End If

            End If


            pnlCountryErr.Visible = False
            If (Not PageHasErrors) Then
                If (cbCountry.SelectedItem Is Nothing OrElse cbCountry.SelectedIndex = 0) Then
                    pnlCountryErr.Visible = True
                    lblCountryErr.Text = CurrentPageData.GetCustomString("lblCountryErr")
                    cbCountry.Focus()
                    PageHasErrors = True
                End If
            End If


        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub

    Sub SaveProfile(ByVal profileId As Integer)
        Try

            Dim ds As ReferralsDLL.DSReferrers = ReferralsDLL.DataHelpers.GetREF_ReferrerProfiles_ByCustomerID(Me.ReferrerCustomerID)
            Try
                Dim Row1 As ReferralsDLL.DSReferrers.REF_ReferrerProfilesRow = ds.REF_ReferrerProfiles.Rows(0)

                Row1.FirstName = txtFirstName.Text
                Row1.LastName = txtLastName.Text
                Row1.eMail = lbleMail.Text
                Row1.Cellular = txtMobile.Text

                If (cbCountry.SelectedItem IsNot Nothing) Then Row1.Country = cbCountry.SelectedItem.Value
                If (cbRegion.SelectedItem IsNot Nothing) Then Row1._Region = cbRegion.SelectedItem.Value
                If (cbCity.SelectedItem IsNot Nothing) Then Row1.City = cbCity.SelectedItem.Value
                ReferralsDLL.DataHelpers.UpdateREF_ReferrerProfiles(ds)

            Catch ex As Exception
                Throw New Exception(ex.Message, ex)
            Finally
                ds.Dispose()
            End Try


            Dim _profilerows As New clsProfileRows(profileId)
            Dim mirrorRow As EUS_ProfilesRow = _profilerows.GetMirrorRow()
            Dim dsM As DSMembers = _profilerows.DSMembers
            Try

                mirrorRow.REF_Payout_AFF_PayoutTypeID = -1
                Integer.TryParse(cbPayoutTypeID.SelectedItem.Value.ToString(), mirrorRow.REF_Payout_AFF_PayoutTypeID)

                mirrorRow.REF_Payout_email = txtPayout_email.Text
                mirrorRow.REF_Payout_Taxid = txtPayout_Taxid.Text
                mirrorRow.REF_Payout_BeneficiaryName = txtPayout_BeneficiaryName.Text
                mirrorRow.REF_Payout_CompanyStreetAddress = txtPayout_CompanyStreetAddress.Text
                mirrorRow.REF_Payout_CompanyCity = txtPayout_CompanyCity.Text
                mirrorRow.REF_Payout_CompanyPostalCode = txtPayout_CompanyPostalCode.Text
                mirrorRow.REF_Payout_BankName = txtPayout_BankName.Text
                mirrorRow.REF_Payout_BankAddress = txtPayout_BankAddress.Text
                mirrorRow.REF_Payout_BankCountryID = cbBankCountryID.SelectedItem.Value
                mirrorRow.REF_Payout_BankAccountNo = txtPayout_BankAccountNo.Text
                mirrorRow.REF_Payout_SWIFTCode = txtPayout_SWIFTCode.Text
                mirrorRow.REF_Payout_IBAN = txtPayout_IBAN.Text
                mirrorRow.REF_Payout_PreferredCurrency = cbPreferredCurrency.SelectedItem.Value
                mirrorRow.REF_Payout_AdditionalNotes = txtPayout_AdditionalNotes.Text
                mirrorRow.REF_Payout_MinBalance = spinMinBalance.Number

                mirrorRow.LastUpdateProfileDateTime = DateTime.UtcNow
                mirrorRow.LastUpdateProfileIP = Request.Params("REMOTE_ADDR")
                mirrorRow.LastUpdateProfileGEOInfo = Session("GEO_COUNTRY_CODE")
                mirrorRow.LAGID = Session("LagID")

                Dim masterRow As EUS_ProfilesRow = _profilerows.GetMasterRow()
                If (mirrorRow.ProfileID <> masterRow.ProfileID) Then

                    masterRow.REF_Payout_AFF_PayoutTypeID = mirrorRow.REF_Payout_AFF_PayoutTypeID
                    mirrorRow.REF_Payout_email = mirrorRow.REF_Payout_email
                    mirrorRow.REF_Payout_Taxid = mirrorRow.REF_Payout_Taxid
                    mirrorRow.REF_Payout_BeneficiaryName = mirrorRow.REF_Payout_BeneficiaryName
                    mirrorRow.REF_Payout_CompanyStreetAddress = mirrorRow.REF_Payout_CompanyStreetAddress
                    mirrorRow.REF_Payout_CompanyCity = mirrorRow.REF_Payout_CompanyCity
                    mirrorRow.REF_Payout_CompanyPostalCode = mirrorRow.REF_Payout_CompanyPostalCode
                    mirrorRow.REF_Payout_BankName = mirrorRow.REF_Payout_BankName
                    mirrorRow.REF_Payout_BankAddress = mirrorRow.REF_Payout_BankAddress
                    mirrorRow.REF_Payout_BankCountryID = mirrorRow.REF_Payout_BankCountryID
                    mirrorRow.REF_Payout_BankAccountNo = mirrorRow.REF_Payout_BankAccountNo
                    mirrorRow.REF_Payout_SWIFTCode = mirrorRow.REF_Payout_SWIFTCode
                    mirrorRow.REF_Payout_IBAN = mirrorRow.REF_Payout_IBAN
                    mirrorRow.REF_Payout_PreferredCurrency = mirrorRow.REF_Payout_PreferredCurrency
                    mirrorRow.REF_Payout_AdditionalNotes = mirrorRow.REF_Payout_AdditionalNotes
                    mirrorRow.REF_Payout_MinBalance = mirrorRow.REF_Payout_MinBalance

                    mirrorRow.LastUpdateProfileDateTime = DateTime.UtcNow
                    mirrorRow.LastUpdateProfileIP = Request.Params("REMOTE_ADDR")
                    mirrorRow.LastUpdateProfileGEOInfo = Session("GEO_COUNTRY_CODE")
                    mirrorRow.LAGID = Session("LagID")

                End If


                SiteDLL.DataHelpers.UpdateEUS_Profiles(dsM)


            Catch ex As Exception
                Throw New Exception(ex.Message, ex)
            Finally
                dsM.Dispose()
            End Try



            pnlErr.CssClass = "alert alert-success"
            pnlErr.Visible = True
            lblErr.Text = CurrentPageData.GetCustomString("Success_SettingsUpdated") ' "Οι ρυθμίσεις ενημερώθηκαν επιτυχώς."

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")

            pnlErr.CssClass = "alert alert-danger"
            pnlErr.Visible = True
            lblErr.Text = CurrentPageData.GetCustomString("Err_SettingsUpdateFailed") & ex.Message '"Προέκυψε πρόβλημα κατα την αποθήκευση. "
            lblErr.Text = lblErr.Text.Replace("[EXCEPTION_MESSAGE]", ex.Message)
        End Try
    End Sub


    ''Protected Sub btnSavePass_Click(sender As Object, e As EventArgs) Handles btnSavePass.Click
    ''    Try
    ''        'Dim profileId As Integer = Me.Session("ReferrerID")
    ''        ValidateInput_Password()
    ''        If (Page.IsValid AndAlso Not PageHasErrors) Then
    ''            SaveProfile_Password()
    ''        End If

    ''    Catch ex As Exception
    ''        WebErrorMessageBox(Me, ex, "")
    ''    End Try
    ''End Sub

    ''Private Sub ValidateInput_Password()

    ''    PageHasErrors = False
    ''    Try

    ''        If (txtPassword.Text.Trim() <> "" AndAlso txtPasswordConf.Text.Trim() <> "" AndAlso txtPassword.Text <> txtPasswordConf.Text) Then
    ''            pnlPasswordErr.CssClass = "alert alert-danger"
    ''            pnlPasswordErr.Visible = True
    ''            lblPasswordErr.Text = CurrentPageData.GetCustomString("Error.Password.Confirm.Invalid")

    ''            PageHasErrors = True
    ''        End If


    ''    Catch ex As Exception
    ''        WebErrorMessageBox(Me, ex, "")
    ''    End Try

    ''End Sub


    ''Sub SaveProfile_Password()
    ''    Try
    ''        If (txtPassword.Text.Trim() <> "" AndAlso txtPasswordConf.Text.Trim() <> "") Then
    ''            Dim dsRef As New Dating.Referrals.Server.Core.DLL.DSReferrers()
    ''            Try

    ''                dsRef = Dating.Referrals.Server.Core.DLL.DataHelpers.GetREF_ReferrerProfiles_ByCustomerID(Me.ReferrerCustomerID)
    ''                Dim dr As Dating.Referrals.Server.Core.DLL.DSReferrers.REF_ReferrerProfilesRow = dsRef.REF_ReferrerProfiles.Rows(0)
    ''                dr.Password = txtPassword.Text
    ''                Dating.Referrals.Server.Core.DLL.DataHelpers.UpdateREF_ReferrerProfiles(dsRef)

    ''            Catch ex As Exception
    ''                WebErrorMessageBox(Me, ex, "")
    ''            Finally
    ''                dsRef.Dispose()
    ''            End Try
    ''        End If

    ''        pnlPasswordErr.CssClass = "alert alert-success"
    ''        pnlPasswordErr.Visible = True
    ''        lblPasswordErr.Text = CurrentPageData.GetCustomString("Success.Password.Changed") ' "Οι ρυθμίσεις ενημερώθηκαν επιτυχώς."

    ''    Catch ex As Exception
    ''        WebErrorMessageBox(Me, ex, "")

    ''        pnlPasswordErr.CssClass = "alert alert-danger"
    ''        pnlPasswordErr.Visible = True
    ''        lblPasswordErr.Text = CurrentPageData.GetCustomString("Error.Password.NOT.Changed") & ex.Message '"Προέκυψε πρόβλημα κατα την αποθήκευση. "
    ''        lblPasswordErr.Text = lblErr.Text.Replace("[EXCEPTION_MESSAGE]", ex.Message)
    ''    End Try
    ''End Sub


    Sub LoadProfile(ByVal profileId As Integer)
        Try

            Dim ds As ReferralsDLL.DSReferrers = ReferralsDLL.DataHelpers.GetREF_ReferrerProfiles_ByCustomerID(Me.ReferrerCustomerID)
            Try
                Dim Row1 As ReferralsDLL.DSReferrers.REF_ReferrerProfilesRow = ds.REF_ReferrerProfiles.Rows(0)


                'Dim currentRow As EUS_Profile = Me.GetCurrentProfile()
                If (Not Row1.IsNull("LoginName")) Then lblLogin.Text = Row1.LoginName

                If (Not Row1.IsNull("ReferrerParentId")) Then
                    Dim prof As EUS_Profile = DataHelpers.GetEUS_ProfileByProfileID(Me.CMSDBDataContext, Row1.ReferrerParentId)
                    If (prof IsNot Nothing) Then
                        lblParentAff.Text = prof.LoginName
                        If (prof.LoginName = "Goomena.com") Then
                            lblParentAff.Text = "root"
                        End If
                    End If
                End If

                If (Not Row1.IsNull("FirstName")) Then txtFirstName.Text = Row1.FirstName
                If (Not Row1.IsNull("LastName")) Then txtLastName.Text = Row1.LastName
                If (Not Row1.IsNull("eMail")) Then lbleMail.Text = Row1.eMail
                If (Not Row1.IsNull("Cellular")) Then txtMobile.Text = Row1.Cellular

                If (Not Row1.IsNull("Country")) Then
                    SelectComboItem(cbCountry, Row1.Country)
                    cbCountry.Enabled = False

                    cbRegion.DataSourceID = ""
                    cbRegion.DataSource = clsGeoHelper.GetCountryRegions(cbCountry.SelectedItem.Value, Session("LAGID"))
                    cbRegion.TextField = "region1"
                    cbRegion.DataBind()
                End If

                If (Not Row1.IsNull("Region")) Then
                    SelectComboItem(cbRegion, Row1._Region)

                    cbCity.DataSourceID = ""
                    cbCity.DataSource = clsGeoHelper.GetCountryRegionCities(cbCountry.SelectedItem.Value, Row1._Region, Session("LAGID"))
                    cbCity.TextField = "city"
                    cbCity.DataBind()
                End If

                If (Not Row1.IsNull("City")) Then
                    SelectComboItem(cbCity, Row1.City)
                End If

            Catch ex As Exception
                Throw New Exception(ex.Message, ex)
            Finally
                ds.Dispose()
            End Try


            Dim _profilerows As New clsProfileRows(profileId)
            Dim currentRow As EUS_ProfilesRow = _profilerows.GetMirrorRow()
            Dim dsM As DSMembers = _profilerows.DSMembers
            Try

                If (Not currentRow.IsNull("REF_Payout_AFF_PayoutTypeID")) Then
                    SelectComboItem(cbPayoutTypeID, currentRow.REF_Payout_AFF_PayoutTypeID)
                    cbPayoutTypeID_SelectedIndexChanged(cbPayoutTypeID, New EventArgs())
                End If


                If (Not currentRow.IsNull("REF_Payout_email")) Then txtPayout_email.Text = currentRow.REF_Payout_email
                If (Not currentRow.IsNull("REF_Payout_Taxid")) Then txtPayout_Taxid.Text = currentRow.REF_Payout_Taxid
                If (Not currentRow.IsNull("REF_Payout_BeneficiaryName")) Then txtPayout_BeneficiaryName.Text = currentRow.REF_Payout_BeneficiaryName
                If (Not currentRow.IsNull("REF_Payout_CompanyStreetAddress")) Then txtPayout_CompanyStreetAddress.Text = currentRow.REF_Payout_CompanyStreetAddress
                If (Not currentRow.IsNull("REF_Payout_CompanyCity")) Then txtPayout_CompanyCity.Text = currentRow.REF_Payout_CompanyCity
                If (Not currentRow.IsNull("REF_Payout_CompanyPostalCode")) Then txtPayout_CompanyPostalCode.Text = currentRow.REF_Payout_CompanyPostalCode
                If (Not currentRow.IsNull("REF_Payout_BankName")) Then txtPayout_BankName.Text = currentRow.REF_Payout_BankName
                If (Not currentRow.IsNull("REF_Payout_BankAddress")) Then txtPayout_BankAddress.Text = currentRow.REF_Payout_BankAddress

                If (Not currentRow.IsNull("REF_Payout_BankCountryID")) Then SelectComboItem(cbBankCountryID, currentRow.REF_Payout_BankCountryID)

                If (Not currentRow.IsNull("REF_Payout_BankAccountNo")) Then txtPayout_BankAccountNo.Text = currentRow.REF_Payout_BankAccountNo
                If (Not currentRow.IsNull("REF_Payout_SWIFTCode")) Then txtPayout_SWIFTCode.Text = currentRow.REF_Payout_SWIFTCode
                If (Not currentRow.IsNull("REF_Payout_IBAN")) Then txtPayout_IBAN.Text = currentRow.REF_Payout_IBAN

                If (Not currentRow.IsNull("REF_Payout_PreferredCurrency")) Then SelectComboItem(cbPreferredCurrency, IIf(currentRow.REF_Payout_PreferredCurrency IsNot Nothing, currentRow.REF_Payout_PreferredCurrency, -1))

                If (Not currentRow.IsNull("REF_Payout_AdditionalNotes")) Then txtPayout_AdditionalNotes.Text = currentRow.REF_Payout_AdditionalNotes
                If (Not currentRow.IsNull("REF_Payout_MinBalance")) Then spinMinBalance.Number = currentRow.REF_Payout_MinBalance

            Catch ex As Exception
                Throw New Exception(ex.Message, ex)
            Finally
                dsM.Dispose()
            End Try


        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub




    Protected Sub cbPayoutTypeID_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbPayoutTypeID.SelectedIndexChanged
        Try
            Select Case cbPayoutTypeID.SelectedItem.Value
                Case "1" 'Bank Tranfer
                    liPayout_email.Visible = False
                    'liPayout_Taxid.Visible = True
                    liPayout_BeneficiaryName.Visible = True
                    liPayout_CompanyStreetAddress.Visible = False
                    liPayout_CompanyCity.Visible = False
                    liPayout_CompanyPostalCode.Visible = False
                    liPayout_BankName.Visible = True
                    liPayout_BankCountryID.Visible = True
                    liPayout_BankAddress.Visible = True
                    liPayout_BankAccountNo.Visible = True
                    liPayout_SWIFTCode.Visible = True
                    liPayout_IBAN.Visible = True
                    liPayout_PreferredCurrency.Visible = True
                    liPayout_AdditionalNotes.Visible = True

                Case "2" 'Payza
                    liPayout_email.Visible = True
                    'liPayout_Taxid.Visible = False
                    liPayout_BeneficiaryName.Visible = False
                    liPayout_CompanyStreetAddress.Visible = False
                    liPayout_CompanyCity.Visible = False
                    liPayout_CompanyPostalCode.Visible = False
                    liPayout_BankName.Visible = False
                    liPayout_BankCountryID.Visible = False
                    liPayout_BankAddress.Visible = False
                    liPayout_BankAccountNo.Visible = False
                    liPayout_SWIFTCode.Visible = False
                    liPayout_IBAN.Visible = False
                    liPayout_PreferredCurrency.Visible = True
                    liPayout_AdditionalNotes.Visible = True

                Case "3" 'Libetry Reverce
                    liPayout_email.Visible = True
                    'liPayout_Taxid.Visible = True
                    liPayout_BeneficiaryName.Visible = True
                    liPayout_CompanyStreetAddress.Visible = True
                    liPayout_CompanyCity.Visible = True
                    liPayout_CompanyPostalCode.Visible = True
                    liPayout_BankName.Visible = True
                    liPayout_BankCountryID.Visible = True
                    liPayout_BankAddress.Visible = True
                    liPayout_BankAccountNo.Visible = True
                    liPayout_SWIFTCode.Visible = True
                    liPayout_IBAN.Visible = True
                    liPayout_PreferredCurrency.Visible = True
                    liPayout_AdditionalNotes.Visible = True

                Case "4" 'Web Moneay
                    liPayout_email.Visible = True
                    'liPayout_Taxid.Visible = False
                    liPayout_BeneficiaryName.Visible = False
                    liPayout_CompanyStreetAddress.Visible = False
                    liPayout_CompanyCity.Visible = False
                    liPayout_CompanyPostalCode.Visible = False
                    liPayout_BankName.Visible = False
                    liPayout_BankCountryID.Visible = False
                    liPayout_BankAddress.Visible = False
                    liPayout_BankAccountNo.Visible = False
                    liPayout_SWIFTCode.Visible = False
                    liPayout_IBAN.Visible = False
                    liPayout_PreferredCurrency.Visible = True
                    liPayout_AdditionalNotes.Visible = True

                Case "5" 'Paypal
                    liPayout_email.Visible = True
                    'liPayout_Taxid.Visible = False
                    liPayout_BeneficiaryName.Visible = False
                    liPayout_CompanyStreetAddress.Visible = False
                    liPayout_CompanyCity.Visible = False
                    liPayout_CompanyPostalCode.Visible = False
                    liPayout_BankName.Visible = False
                    liPayout_BankCountryID.Visible = False
                    liPayout_BankAddress.Visible = False
                    liPayout_BankAccountNo.Visible = False
                    liPayout_SWIFTCode.Visible = False
                    liPayout_IBAN.Visible = False
                    liPayout_PreferredCurrency.Visible = True
                    liPayout_AdditionalNotes.Visible = True

                Case "6" 'Western Union
                    liPayout_email.Visible = True
                    'liPayout_Taxid.Visible = True
                    liPayout_BeneficiaryName.Visible = True
                    liPayout_CompanyStreetAddress.Visible = True
                    liPayout_CompanyCity.Visible = True
                    liPayout_CompanyPostalCode.Visible = True
                    liPayout_BankName.Visible = True
                    liPayout_BankCountryID.Visible = True
                    liPayout_BankAddress.Visible = True
                    liPayout_BankAccountNo.Visible = True
                    liPayout_SWIFTCode.Visible = True
                    liPayout_IBAN.Visible = True
                    liPayout_PreferredCurrency.Visible = True
                    liPayout_AdditionalNotes.Visible = True

                Case Else 'nothing selected
                    liPayout_email.Visible = False
                    'liPayout_Taxid.Visible = False
                    liPayout_BeneficiaryName.Visible = False
                    liPayout_CompanyStreetAddress.Visible = False
                    liPayout_CompanyCity.Visible = False
                    liPayout_CompanyPostalCode.Visible = False
                    liPayout_BankName.Visible = False
                    liPayout_BankCountryID.Visible = False
                    liPayout_BankAddress.Visible = False
                    liPayout_BankAccountNo.Visible = False
                    liPayout_SWIFTCode.Visible = False
                    liPayout_IBAN.Visible = False
                    liPayout_PreferredCurrency.Visible = False
                    liPayout_AdditionalNotes.Visible = False

            End Select

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Private Sub cbCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbCountry.SelectedIndexChanged
        cbRegion.DataSourceID = ""
        cbRegion.DataSource = clsGeoHelper.GetCountryRegions(cbCountry.SelectedItem.Value, Session("LAGID"))
        cbRegion.TextField = "region1"
        cbRegion.DataBind()
    End Sub

    Private Sub cbRegion_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbRegion.SelectedIndexChanged
        Dim a As String = cbRegion.Value
        cbCity.DataSourceID = ""
        cbCity.DataSource = clsGeoHelper.GetCountryRegionCities(cbCountry.SelectedItem.Value, a, Session("LAGID"))
        cbCity.TextField = "city"
        cbCity.DataBind()
    End Sub


End Class
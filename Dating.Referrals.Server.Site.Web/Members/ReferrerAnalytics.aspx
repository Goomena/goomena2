﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Members/Members2Inner.master" 
CodeBehind="ReferrerAnalytics.aspx.vb" Inherits="Dating.Referrals.Server.Site.Web.ReferrerAnalytics" %>

<%@ Register src="../UserControls/WhatIsIt.ascx" tagname="WhatIsIt" tagprefix="uc2" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">

           <div class="grid_3 top_sidebar" id="sidebar-outter">
                <div class="lfloat">
                    <h2 class="page-name"><asp:Literal ID="lblItemsName" runat="server" /></h2>
                    <h2 class="page-description"><asp:Label ID="lblPageTitle" runat="server" Text="Οι Προμήθειες μου ανά επίπεδο" /></h2>
                </div>
                <div class="page-back-btn">
                    <dx:ASPxHyperLink ID="lnkBack" runat="server" Text="Back" CssClass="btn" 
                        NavigateUrl="~/Members/Referrer.aspx">
                    </dx:ASPxHyperLink>
                </div>
                <div class="clear">
                </div>
            </div>

                    <div class="t_wrap">
                        <div class="send_msg t_to msg">
                            <asp:Panel ID="pnlErr" runat="server" CssClass="alert alert-danger" 
                                Visible="False" EnableViewState="False">
                                <asp:Label ID="lblErr" runat="server"></asp:Label>
                            </asp:Panel>


                <asp:Label ID="lblPageDesc" runat="server" Text="Εδώ μπορείτε να δείτε αναλυτικά από ημερομηνία έως ημερομηνία όλες της συναλλαγές που έγιναν από τα μέλη της οικογένεια σας ανά επίπεδο."></asp:Label>
                <br />
                <br />
        <table cellpadding="5" cellspacing="1">
            <tr>
                <td>
                    <asp:Label ID="lblFromDate" runat="server" Text="Από Ημ/νία : "></asp:Label></td>
                <td>
                    <dx:ASPxDateEdit ID="dtFrom" runat="server" DisplayFormatString="dd/MM/yyyy" 
                        Width="120px">
                    </dx:ASPxDateEdit>
                </td>
                <td>
                    <asp:Label ID="lblToDate" runat="server" Text="Εως "></asp:Label></td>
                <td><dx:ASPxDateEdit ID="dtTo" runat="server" DisplayFormatString="dd/MM/yyyy" 
                        Width="120px">
                    </dx:ASPxDateEdit></td>
                <td>
                    <asp:Label ID="lblLevel" runat="server" Text="Level: " Visible="False"></asp:Label></td>
                <td>
                    <dx:ASPxComboBox ID="cmbLevel" runat="server" SelectedIndex="0" Width="120px" 
                        Visible="False">
                        <Items>
                            <dx:ListEditItem Selected="True" Text="All" Value="-1" />
                            <dx:ListEditItem Text="Level 1" Value="1" />
                            <dx:ListEditItem Text="Level 2" Value="2" />
                            <dx:ListEditItem Text="Level 3" Value="3" />
                            <dx:ListEditItem Text="Level 4" Value="4" />
                            <dx:ListEditItem Text="Level 5" Value="5" />
                        </Items>
                    </dx:ASPxComboBox>
                </td>
                <td>
                    <dx:ASPxButton ID="btnRefresh" runat="server" Text="Refresh">
                    </dx:ASPxButton>
                </td>
            </tr>
        </table>
        
        <dx:ASPxGridView ID="gvCreditsAnalisys" runat="server" AutoGenerateColumns="False"
            DataSourceID="odsCreditsAnalisys" Width="100%" KeyFieldName="CustomerCreditsId">
            <TotalSummary>
                <%--<dx:ASPxSummaryItem DisplayFormat="Commission<br>Sum=0.00 &euro;" FieldName="CommissionCRD" ShowInColumn="colCommissionCRD"
                    SummaryType="Sum" />--%>
                <dx:ASPxSummaryItem DisplayFormat="Commission<br>Sum=0.00" FieldName="CommissionCRD" ShowInColumn="colCommissionCRD"
                    SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="Totals" ShowInColumn="colReferrerLoginName" SummaryType="Custom" />
            </TotalSummary>
            <Columns>
                <dx:GridViewDataTextColumn Caption="User" Name="colReferrersLoginName"
                    VisibleIndex="3" FieldName="ReferrersLoginName" Width="90px">
                    <DataItemTemplate>
                        <dx:ASPxHyperLink ID="lnk" runat="server" NavigateUrl='<%# Eval("PersonsNavigateUrl") %>'
                            Text='<%# Eval("ReferrersLoginName") %>' Width="100%">
                        </dx:ASPxHyperLink>
                    </DataItemTemplate>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Sales" FieldName="SalesCRD" Name="colSalesCRD"
                    VisibleIndex="2" Visible="False">
                    <PropertiesTextEdit DisplayFormatString="0.00"><%-- Caption="Sales &euro;" DisplayFormatString="0.00 &euro;"--%>
                    </PropertiesTextEdit>
                    <CellStyle HorizontalAlign="Right">
                    </CellStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Commission" 
                    FieldName="CommissionCRD" Name="colCommissionCRD"
                    VisibleIndex="5" Width="40px">
                    <PropertiesTextEdit DisplayFormatString="0.00"> <%--Caption="Commission &euro;" DisplayFormatString="0.00 &euro;"--%>
                    </PropertiesTextEdit>
                    <CellStyle HorizontalAlign="Right">
                    </CellStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataDateColumn CellStyle-Wrap="False" Caption="Date" FieldName="DateTimeCreated" Width="90px" VisibleIndex="0" ReadOnly="True">
                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy HH:mm">
                    </PropertiesDateEdit>
                    <CellStyle HorizontalAlign="Right">
                    </CellStyle>
                </dx:GridViewDataDateColumn>
                <dx:GridViewDataTextColumn Caption="Percent %" FieldName="CommissionPercent" 
                    VisibleIndex="10" Width="40px">
                    <PropertiesTextEdit DisplayFormatString="0.0%">
                    </PropertiesTextEdit>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Description" FieldName="Description" Name="colDescription"
                    VisibleIndex="1">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Lev" FieldName="Level" VisibleIndex="4" 
                    Width="30px">
                    <CellStyle HorizontalAlign="Right">
                    </CellStyle>
                </dx:GridViewDataTextColumn>
            </Columns>
            <SettingsText EmptyDataRow="Δεν βρέθηκαν συνεργάτες" />
            <SettingsPager AlwaysShowPager="True" PageSize="50">
            </SettingsPager>
            <Settings ShowFooter="True" ShowFilterBar="Auto" ShowFilterRow="True" ShowGroupPanel="false" />
            <Paddings Padding="0px" />
            <Styles>
                <Header HorizontalAlign="Center" VerticalAlign="Middle" BackColor="#FF7A15" Font-Bold="True" ForeColor="White">
                    <Paddings Padding="0px" PaddingLeft="5px" PaddingRight="5px" />
                </Header>
                <Footer BackColor="#FF8b26" HorizontalAlign="Right" VerticalAlign="Middle">
                </Footer>
                <HeaderPanel>
                    <Paddings Padding="0px" />
                </HeaderPanel>
                <PagerBottomPanel BackColor="#FF8b26">
                </PagerBottomPanel>
            </Styles>
        </dx:ASPxGridView>
        <asp:ObjectDataSource ID="odsCreditsAnalisys" runat="server" 
            SelectMethod="GetCreditsAnalisys" 
            TypeName="Dating.Referrals.Server.Site.Web.ReferrerAnalytics" 
                                OldValuesParameterFormatString="original_{0}">
            <SelectParameters>
                <asp:ControlParameter ControlID="dtFrom" Name="DateFrom" PropertyName="Value" 
                    Type="DateTime" />
                <asp:ControlParameter ControlID="dtTo" Name="DateTo" PropertyName="Value" 
                    Type="DateTime" />
                <asp:ControlParameter ControlID="cmbLevel" Name="TargetLevel" PropertyName="Value" 
                    Type="Int32" />
                <asp:Parameter Name="affCredits1" Type="Object" />
            </SelectParameters>
        </asp:ObjectDataSource>
                        </div>
                    </div>
        <%--</div>
        <div class="clear"></div>
    </div>--%>

	<uc2:WhatIsIt ID="WhatIsIt1" runat="server" />

</asp:Content>

﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Members/Members2Inner.Master" 
CodeBehind="ReferrerSuppliesPersons.aspx.vb" Inherits="Dating.Referrals.Server.Site.Web.ReferrerSuppliesPersons" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" language="javascript">
         function OnMoreInfoClickRef(contentUrl, obj) {
             if (obj != null) popupAffiliateSupplies.SetPopupElementID(obj.id);
             popupAffiliateSupplies.SetContentUrl(contentUrl);
             popupAffiliateSupplies.Show();
         }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
        <%--<div class="container_12" id="content" style="width: auto;background-image:none;">
            <div class="grid_9 body" style="width: 569px;">
                <div class="middle" id="content-outter" style="" >--%>

                            <asp:Panel ID="pnlErr" runat="server" CssClass="alert alert-danger" 
                                Visible="False" EnableViewState="False">
                                <asp:Label ID="lblErr" runat="server"></asp:Label>
                            </asp:Panel>

                            <asp:Label ID="lblPageDesc" runat="server" Text=""></asp:Label>
                   
                            <asp:Label ID="lblProfileInfo" runat="server" Text=""></asp:Label>
                            <br />
                            <br />
                    <dx:ASPxDateEdit ID="dtFrom" runat="server" DisplayFormatString="dd/MM/yyyy" Width="120px"
                        Visible="False">
                    </dx:ASPxDateEdit>
                    <dx:ASPxDateEdit ID="dtTo" runat="server" DisplayFormatString="dd/MM/yyyy" Width="120px"
                        Visible="False">
                    </dx:ASPxDateEdit>
                    <dx:ASPxGridView ID="gvProfileInfo" runat="server" AutoGenerateColumns="False" 
                                DataSourceID="odsProfileInfo" Width="100%" 
                                KeyFieldName="EUS_MessageID">
                                <TotalSummary>
                                    <dx:ASPxSummaryItem DisplayFormat="Sum=0.00" FieldName="Cost" 
                                         SummaryType="Sum" ShowInColumn="Cost" /><%--"Sum=0.00 &euro;"--%>
                                    <dx:ASPxSummaryItem DisplayFormat="Sum=0.00" FieldName="CommissionCRD" 
                                         SummaryType="Sum" ShowInColumn="Commission" /><%--DisplayFormat="Sum=0.00 &euro;" "Commission &euro;"--%>
                                </TotalSummary>
                                <Columns>
                                    <dx:GridViewDataTextColumn FieldName="CustomerCreditsId" VisibleIndex="3" ReadOnly="True" Visible="False">
                                        <EditFormSettings Visible="False" />
                                    </dx:GridViewDataTextColumn>
 				                    <dx:GridViewDataTextColumn VisibleIndex="4" FieldName="CustomerId" 
                                        Visible="False">
				                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="CustomerLoginName" VisibleIndex="5" 
                                        ReadOnly="True" Visible="False">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Credits" VisibleIndex="2" Visible="False">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Cost" VisibleIndex="2" Caption="Cost"> 
                    <PropertiesTextEdit DisplayFormatString="0.00"> <%--Caption="Euro &euro;" DisplayFormatString="0.00 &euro;"--%>
                    </PropertiesTextEdit>
                                    </dx:GridViewDataTextColumn>
 				                    <dx:GridViewDataDateColumn FieldName="Date" CellStyle-Wrap="False" 
                                        VisibleIndex="0">
                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy HH:mm">
                    </PropertiesDateEdit>
                                    </dx:GridViewDataDateColumn>
 				                    <dx:GridViewDataTextColumn FieldName="CustomerTransactionID" ReadOnly="True" 
                                        Visible="False" VisibleIndex="6">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="CreditsTypeId" Visible="False" 
                                        VisibleIndex="7">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="ConversationWithCustomerId" 
                                        Visible="False" VisibleIndex="8">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="CreditsType" Visible="False" 
                                        VisibleIndex="9">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataCheckColumn FieldName="Expired" ReadOnly="True" Visible="False" 
                                        VisibleIndex="10">
                                    </dx:GridViewDataCheckColumn>
                                    <dx:GridViewDataTextColumn FieldName="DatePurchased" ReadOnly="True" 
                                        Visible="False" VisibleIndex="11">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Action" ReadOnly="True" Visible="False" 
                                        VisibleIndex="12">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Description" ReadOnly="True" 
                                        VisibleIndex="1">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Commission" FieldName="CommissionCRD" 
                                        VisibleIndex="3">
                    <PropertiesTextEdit DisplayFormatString="0.00"><%--Caption="Commission &euro;" DisplayFormatString="0.00 &euro;"--%>
                    </PropertiesTextEdit>
                                    </dx:GridViewDataTextColumn>
                                </Columns>
                                <Settings ShowFooter="True" />
                                <SettingsText EmptyDataRow="Δεν βρέθηκαν πληροφορίες" />
                                <Paddings Padding="0px" />
                                <Styles>
                                    <Header HorizontalAlign="Center" VerticalAlign="Middle" BackColor="#51BAE7" Font-Bold="True" ForeColor="White">
                                        <Paddings Padding="0px" PaddingLeft="5px" PaddingRight="5px" />
                                    </Header>
                                    <Footer BackColor="#C6D9F1" HorizontalAlign="Right" VerticalAlign="Middle">
                                    </Footer>
                                    <HeaderPanel>
                                        <Paddings Padding="0px" />
                                    </HeaderPanel>
                                    <PagerBottomPanel BackColor="#C6D9F1">
                                    </PagerBottomPanel>
                                </Styles>
                            </dx:ASPxGridView>
        <asp:ObjectDataSource ID="odsProfileInfo" runat="server" 
            SelectMethod="GetReferrerCreditsHistory" 
            TypeName="Dating.Referrals.Server.Site.Web.ReferrerSuppliesPersons">
            <SelectParameters>
                <asp:ControlParameter ControlID="dtFrom" Name="DateFrom" PropertyName="Value" 
                    Type="DateTime" />
                <asp:ControlParameter ControlID="dtTo" Name="DateTo" PropertyName="Value" 
                    Type="DateTime" />
                <asp:Parameter Name="ReferrerID" Type="Int32" />
                <asp:Parameter Name="LevelParam" Type="Int32" />
            </SelectParameters>
        </asp:ObjectDataSource>

               <%-- </div>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
        </div>--%>

</asp:Content>

﻿Public Class Information
    Inherits BasePage

    Dim _pageData As clsPageData
    Protected ReadOnly Property CurrentPageData As clsPageData
        Get
            If (_pageData Is Nothing) Then _pageData = New clsPageData(Context)
            Return _pageData
        End Get
    End Property



    Protected Overrides Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        Try
            If clsCurrentContext.VerifyLogin() = True Then
                Me.MasterPageFile = "~/Members/Members2.Master"
            Else
                clsCurrentContext.ClearSession(Session)
                Response.Redirect(ResolveUrl("~/Login.aspx"), False)
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try


            If (Not Me.IsPostBack) Then

                If (Request.QueryString("vw") IsNot Nothing) Then

                    If (Request.QueryString("vw").ToUpper() = "NOPHOTO") Then
                        mvInformationMain.SetActiveView(vwHaveToProvidePhoto)


                    ElseIf (Request.QueryString("vw").ToUpper() = "CREDITS") Then
                        ''''''''' CREDITS REQUIRED -> BILLING '''''''''
                        mvInformationMain.SetActiveView(vwCreditsRequired)

                        If (Not String.IsNullOrEmpty(Request.QueryString("returnurl"))) Then
                            lnkCancelContinueToCredits.NavigateUrl = Server.UrlDecode(Request.QueryString("returnurl"))
                        Else
                            ' hacked
                            lnkCancelContinueToCredits.NavigateUrl = "#"
                        End If

                        If (Not String.IsNullOrEmpty(Request.QueryString("offerto")) AndAlso Not String.IsNullOrEmpty(Request.QueryString("offer"))) Then
                            lnkContinueToCredits.NavigateUrl =
                                                        ResolveUrl("~/Members/SelectProduct.aspx?") & _
                                                        Server.UrlEncode(Request.QueryString.ToString()) & _
                                                        ("&enc=" & 1)

                        Else
                            ' hacked
                            lnkContinueToCredits.NavigateUrl = "#"
                        End If




                    ElseIf (Request.QueryString("vw").ToUpper() = "CHARGING") Then
                        ''''''''' CHARGING -> MAKE CHARGE OF AVAILABLE CREDITS '''''''''
                        mvInformationMain.SetActiveView(vwCreditsCharging)

                        If (Not String.IsNullOrEmpty(Request.QueryString("returnurl"))) Then
                            lnkCancelCreditsCharging.NavigateUrl = Server.UrlDecode(Request.QueryString("returnurl"))
                        Else
                            ' hacked
                            lnkCancelCreditsCharging.NavigateUrl = "#"
                        End If

                        If (Not String.IsNullOrEmpty(Request.QueryString("offerto")) AndAlso Not String.IsNullOrEmpty(Request.QueryString("offer"))) Then
                            lnkAcceptCreditsCharging.NavigateUrl =
                                                        ResolveUrl("~/Members/CreateOffer.aspx?") & _
                                                        Request.QueryString.ToString()
                        Else
                            ' hacked
                            lnkAcceptCreditsCharging.NavigateUrl = "#"
                        End If



                    End If
                End If

                LoadLAG()
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try
    End Sub




    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
        UpdateUserControls(Me)
    End Sub


    Protected Sub LoadLAG()
        Try
            SetControlsValue(Me, CurrentPageData)

            'Dim cPageBasic As Dating.Server.Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            'GeneralFunctions.setSEOPageData(Me, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender

        Try
            Dim cPageBasic As Dating.Server.Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            GeneralFunctions.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub


End Class
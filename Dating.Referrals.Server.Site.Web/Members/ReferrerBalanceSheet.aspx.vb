﻿Imports Dating.Server.Core.DLL

Public Class ReferrerBalanceSheet
    Inherits ReferrerBasePage


    Dim _pageData As clsPageData
    Protected ReadOnly Property CurrentPageData As clsPageData
        Get
            If (_pageData Is Nothing) Then _pageData = New clsPageData(Context)
            Return _pageData
        End Get
    End Property



    Protected Overrides Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        Try
            If clsCurrentContext.VerifyLogin() = True Then
                Me.MasterPageFile = "~/Members/Members2.Master"
            Else
                clsCurrentContext.ClearSession(Session)
                Response.Redirect(ResolveUrl("~/Login.aspx"), False)
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pnlErr.Visible = False

        Try
            If (Me.IsReferrer()) Then
                ' do nothing
            Else
                Response.Redirect("Referrer.aspx")
            End If

        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try


        'If (Not Page.IsPostBack) Then

        '    Try

        '        dtFrom.Date = DateTime.UtcNow.AddDays(-30)
        '        dtTo.Date = DateTime.UtcNow

        '        'If (Request.UrlReferrer IsNot Nothing) Then
        '        '    lnkBack.NavigateUrl = Request.UrlReferrer.ToString()
        '        'Else
        '        '    lnkBack.NavigateUrl = "~/Members/"
        '        'End If


        '        GetReferrerCreditsObject()
        '        LoadLAG()
        '    Catch ex As Exception
        '        WebErrorMessageBox(Me, ex, "Page_Load")
        '    End Try

        'End If

        Try
            If (Not Me.IsPostBack) Then
                LoadLAG()
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try


        Try
            SetReferrerUI()
            'If (Me.GetCurrentProfile().ReferrerParentId.HasValue AndAlso Me.GetCurrentProfile().ReferrerParentId > 0)Then
            '    'lblReferrerTitle.Text = "Επισκόπηση συνεργασίας"
            'Else
            '    mvMain.SetActiveView(vwSetReferrerParent)
            'End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
    End Sub


    Protected Sub LoadLAG()
        Try

            lblPageTitle.Text = CurrentPageData.GetCustomString("lblPageTitle")
            lnkBack.Text = CurrentPageData.GetCustomString("lnkBack")
            lblPageDesc.Text = CurrentPageData.GetCustomString("lblPageDesc")
            lblFromDate.Text = CurrentPageData.GetCustomString("lblFromDate")
            lblToDate.Text = CurrentPageData.GetCustomString("lblToDate")
            btnRefresh.Text = CurrentPageData.GetCustomString("btnRefresh")
            gvBalance.SettingsText.EmptyDataRow = CurrentPageData.GetCustomString("gvBalance.EmptyDataRow")


            lblItemsName.Text = Me.CurrentPageData.GetCustomString("CartReferrerBalanceSheetView")
            'lnkViewDescription.Text = Me.CurrentPageData.GetCustomString("CartReferrerBalanceSheetWhatIsIt")
            'lnkViewDescription.OnClientClick = OnMoreInfoClickFunc(
            '    lnkViewDescription.OnClientClick,
            '    ResolveUrl("~/Members/InfoWin.aspx?info=referrerbalancesheet"),
            '    Me.CurrentPageData.GetCustomString("CartReferrerBalanceSheetView"))
            WhatIsIt1.PopupControl.HeaderText = Me.CurrentPageData.GetCustomString("CartReferrerBalanceSheetView")

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender

        Try
            Dim cPageBasic As Dating.Server.Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub


    Private Sub SetReferrerUI()
        If (Not Page.IsPostBack) Then
            cblView.SelectedIndex = 0
        End If
        SetDaysRange()
        GetReferrerCreditsObject(dtFrom.Value, dtTo.Value)
    End Sub



    Private Sub SetDaysRange()
        If (Not Page.IsPostBack) Then
            If (SessionVariables.ReferrerCredits IsNot Nothing) Then
                Try
                    dtFrom.Value = SessionVariables.ReferrerCredits.DateFrom
                    dtTo.Value = SessionVariables.ReferrerCredits.DateTo
                Catch ex As Exception
                End Try
            End If

            If (dtFrom.Value Is Nothing OrElse dtTo.Value Is Nothing) Then
                dtFrom.Date = DateTime.UtcNow.AddDays(-clsReferrer.DefaultDaysRange)
                dtTo.Date = DateTime.UtcNow.AddDays(1)
            End If
        End If
    End Sub





    Protected Sub odsBalance_Selecting(sender As Object, e As System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs) Handles odsBalance.Selecting
        e.InputParameters("ReferrerID") = Me.MasterProfileId
        If (cblView.SelectedItem.Value = "Per Month") Then
            e.InputParameters("ShowByPeriod") = "MONTH"
        Else
            e.InputParameters("ShowByPeriod") = "DAY"
        End If
    End Sub

    Protected Sub btnRefresh_Click(sender As Object, e As EventArgs) Handles btnRefresh.Click
        ' Try

        gvBalance.DataBind()

        'Catch ex As Exception
        '    WebErrorMessageBox(Me, ex, "btnGo")
        'End Try
    End Sub



    Protected Sub cblView_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cblView.SelectedIndexChanged

    End Sub

    Protected Sub cblView_ValueChanged(sender As Object, e As EventArgs) Handles cblView.ValueChanged

    End Sub
End Class
﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Members/Members2Inner.master" 
CodeBehind="ReferrerStatistics.aspx.vb" Inherits="Dating.Referrals.Server.Site.Web.ReferrerStatistics" %>

<%@ Register assembly="DevExpress.XtraCharts.v13.1.Web, Version=13.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraCharts.Web" tagprefix="dxchartsui" %>
<%@ Register assembly="DevExpress.XtraCharts.v13.1, Version=13.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraCharts" tagprefix="cc1" %>
<%@ Register src="../UserControls/WhatIsIt.ascx" tagname="WhatIsIt" tagprefix="uc2" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    

            
           <div class="grid_3 top_sidebar" id="sidebar-outter">
                <div class="lfloat">
                    <h2 class="page-name"><asp:Literal ID="lblItemsName" runat="server" /></h2>
                    <h2 class="page-description"><asp:Label ID="lblPageTitle" runat="server" Text="Οι Προμήθειες μου ανά επίπεδο" /></h2>
                </div>
                <div class="page-back-btn">
                    <dx:ASPxHyperLink ID="lnkBack" runat="server" Text="Back" CssClass="btn" 
                        NavigateUrl="~/Members/Referrer.aspx">
                    </dx:ASPxHyperLink>
                </div>
                <div class="clear">
                </div>
            </div>

                    <div class="t_wrap">
                        <div class="send_msg t_to msg">
                            <asp:Panel ID="pnlErr" runat="server" CssClass="alert alert-danger" 
                                Visible="False" EnableViewState="False">
                                <asp:Label ID="lblErr" runat="server"></asp:Label>
                            </asp:Panel>

                <asp:Label ID="lblPageDesc" runat="server" Text="Δείτε στατιστικές εσόδων ανά ημέρα, μήνα κτλ, με γραφήματα μπάρας, πίτας και άλλα πολλά."></asp:Label>
                <br />
                <br />
        <table cellpadding="5" cellspacing="1">
            <tr>
                <td>
                    <asp:Label ID="lblFromDate" runat="server" Text="Από Ημερομηνία : "></asp:Label></td>
                <td>
                    <dx:ASPxDateEdit ID="dtFrom" runat="server" DisplayFormatString="dd/MM/yyyy" 
                        Width="120px">
                    </dx:ASPxDateEdit>
                </td>
                <td>
                    <asp:Label ID="lblToDate" runat="server" Text="Εως "></asp:Label></td>
                <td><dx:ASPxDateEdit ID="dtTo" runat="server" DisplayFormatString="dd/MM/yyyy" 
                        Width="120px">
                    </dx:ASPxDateEdit></td>
                <td>
                    <asp:Label ID="lblLevel" runat="server" Text="Level: " Visible="False"></asp:Label></td>
                <td>
                    <dx:ASPxComboBox ID="cmbLevel" runat="server" SelectedIndex="0" Width="120px" 
                        Visible="False">
                        <Items>
                            <dx:ListEditItem Selected="True" Text="All" Value="-1" />
                            <dx:ListEditItem Text="Level 1" Value="1" />
                            <dx:ListEditItem Text="Level 2" Value="2" />
                            <dx:ListEditItem Text="Level 3" Value="3" />
                            <dx:ListEditItem Text="Level 4" Value="4" />
                            <dx:ListEditItem Text="Level 5" Value="5" />
                        </Items>
                    </dx:ASPxComboBox>
                </td>
                <td>
                    <dx:ASPxButton ID="btnRefresh" runat="server" Text="Refresh">
                    </dx:ASPxButton>
                </td>
            </tr>
        </table>
        <%--<div class="rfloat">
            <table cellpadding="5" cellspacing="1" >
                <tr>
                    <td><asp:Label ID="lblSeries" runat="server" Text="Series: "></asp:Label></td>
                    <td>
                        <dx:ASPxCheckBoxList ID="cblSeries" runat="server" SelectedIndex="0" 
                            RepeatDirection="Horizontal">
                            <Items>
                                <dx:ListEditItem Selected="True" Text="Money EUR" Value="Money EUR"/>
                                <dx:ListEditItem Text="Commission CRD" Value="Commission CRD" />
                            </Items>
                            <Border BorderStyle="None" />
                        </dx:ASPxCheckBoxList>
                    </td>
                </tr>
            </table></div>--%>
        <div class="clear"></div>
    <dxchartsui:webchartcontrol ID="wccStatistics" runat="server" 
            Height="300px" Width="650px" DataSourceID="odsStatistics">
        <diagramserializable>
            <cc1:XYDiagram>
                <axisx visibleinpanesserializable="-1">
                    <range sidemarginsenabled="True" />
<Range SideMarginsEnabled="True"></Range>
                    <datetimeoptions format="Custom" formatstring="dd/MM" />
                </axisx>
                <axisy visibleinpanesserializable="-1">
                    <range sidemarginsenabled="True" />
<Range SideMarginsEnabled="True"></Range>
                </axisy>
            </cc1:XYDiagram>
        </diagramserializable>
<FillStyle><OptionsSerializable>
<cc1:SolidFillOptions></cc1:SolidFillOptions>
</OptionsSerializable>
</FillStyle>

        <legend backcolor="224, 224, 224">
            <fillstyle fillmode="Solid">
                <optionsserializable>
                    <cc1:SolidFillOptions />
                </optionsserializable>
            </fillstyle>
        </legend>
        <seriesserializable><%--Name="Money EUR"--%>
            <cc1:Series ArgumentDataMember="DateTimeCreated" ArgumentScaleType="DateTime" 
                Name="Money" ValueDataMembersSerializable="MoneyLevelsSum" 
                SynchronizePointOptions="False">
                <viewserializable>
                    <cc1:SideBySideBarSeriesView>
                    </cc1:SideBySideBarSeriesView>
                </viewserializable>
                <labelserializable>
                    <cc1:SideBySideBarSeriesLabel LineVisible="True" 
                        ResolveOverlappingMode="Default">
                        <fillstyle>
                            <optionsserializable>
                                <cc1:SolidFillOptions />
                            </optionsserializable>
                        </fillstyle>
                        <pointoptionsserializable>
                            <cc1:PointOptions>
                            </cc1:PointOptions>
                        </pointoptionsserializable>
                    </cc1:SideBySideBarSeriesLabel>
                </labelserializable>
                <legendpointoptionsserializable>
                    <cc1:PointOptions>
                    <valuenumericoptions format="Number" /></cc1:PointOptions>
                </legendpointoptionsserializable>
            </cc1:Series>
            <cc1:Series ArgumentDataMember="DateTimeCreated" ArgumentScaleType="DateTime" 
                Name="Commission CRD" 
                ValueDataMembersSerializable="CommissionCRDLevelsSum" Visible="False">
                <viewserializable>
                    <cc1:SideBySideBarSeriesView>
                    </cc1:SideBySideBarSeriesView>
                </viewserializable>
                <labelserializable>
                    <cc1:SideBySideBarSeriesLabel LineVisible="True">
                        <fillstyle>
                            <optionsserializable>
                                <cc1:SolidFillOptions />
                            </optionsserializable>
                        </fillstyle>
                        <pointoptionsserializable>
                            <cc1:PointOptions>
                            </cc1:PointOptions>
                        </pointoptionsserializable>
                    </cc1:SideBySideBarSeriesLabel>
                </labelserializable>
                <legendpointoptionsserializable>
                    <cc1:PointOptions>
                    </cc1:PointOptions>
                </legendpointoptionsserializable>
            </cc1:Series>
        </seriesserializable>
        <seriesnametemplate begintext="Sales" />

<SeriesNameTemplate BeginText="Sales"></SeriesNameTemplate>

<SeriesTemplate argumentscaletype="DateTime" seriespointssorting="Ascending" 
            seriespointssortingkey="Value_1">
    <datafilters>
        <cc1:DataFilter DataType="System.DateTime" />
    </datafilters>
    <ViewSerializable>
<cc1:SideBySideBarSeriesView></cc1:SideBySideBarSeriesView>
</ViewSerializable>
<LabelSerializable>
<cc1:SideBySideBarSeriesLabel LineVisible="True">
<FillStyle><OptionsSerializable>
<cc1:SolidFillOptions></cc1:SolidFillOptions>
</OptionsSerializable>
</FillStyle>
<PointOptionsSerializable>
<cc1:PointOptions></cc1:PointOptions>
</PointOptionsSerializable>
</cc1:SideBySideBarSeriesLabel>
</LabelSerializable>
<LegendPointOptionsSerializable>
<cc1:PointOptions></cc1:PointOptions>
</LegendPointOptionsSerializable>
</SeriesTemplate>
    </dxchartsui:webchartcontrol>

        <asp:ObjectDataSource ID="odsStatistics" runat="server" 
            SelectMethod="GetCreditsStatisticsPerDate" 
            TypeName="Dating.Referrals.Server.Site.Web.ReferrerStatistics" 
            OldValuesParameterFormatString="original_{0}">
            <SelectParameters>
                <asp:ControlParameter ControlID="dtFrom" Name="DateFrom" PropertyName="Value" 
                    Type="DateTime" />
                <asp:ControlParameter ControlID="dtTo" Name="DateTo" PropertyName="Value" 
                    Type="DateTime" />
                <asp:ControlParameter ControlID="cmbLevel" Name="TargetLevel" 
                    PropertyName="Value" Type="Int32" />
                <asp:Parameter Name="affCredits1" Type="Object" />
            </SelectParameters>
        </asp:ObjectDataSource>

                        </div>
                    </div>
        <%--</div>
		
	
	    </div>
        <div class="clear"></div>
    </div>--%>

	<uc2:whatisit ID="WhatIsIt1" runat="server" />
    
</asp:Content>

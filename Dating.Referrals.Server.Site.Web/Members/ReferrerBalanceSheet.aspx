﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Members/Members2Inner.master" 
CodeBehind="ReferrerBalanceSheet.aspx.vb" Inherits="Dating.Referrals.Server.Site.Web.ReferrerBalanceSheet" %>

<%@ Register src="../UserControls/WhatIsIt.ascx" tagname="WhatIsIt" tagprefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">

           <div class="grid_3 top_sidebar" id="sidebar-outter">
                <div class="lfloat">
                    <h2 class="page-name"><asp:Literal ID="lblItemsName" runat="server" /></h2>
                    <h2 class="page-description"><asp:Label ID="lblPageTitle" runat="server" Text="Οι Προμήθειες μου ανά επίπεδο" /></h2>
                </div>
                <div class="page-back-btn">
                    <dx:ASPxHyperLink ID="lnkBack" runat="server" Text="Back" CssClass="btn" 
                        NavigateUrl="~/Members/Referrer.aspx">
                    </dx:ASPxHyperLink>
                </div>
                <div class="clear">
                </div>
            </div>
        <%--<div class="grid_3 top_sidebar" id="sidebar-outter">
            <div class="left_tab" id="left_tab" runat="server">
                <div class="top_info">
                </div>
                <div class="middle" id="search">
                    <div class="submit">
                        <h3><asp:Literal ID="lblItemsName" runat="server" /></h3>
                        <div class="clear"></div>
                        <asp:LinkButton ID="lnkViewDescription" runat="server" CssClass="more_info2" OnClientClick="OnMoreInfoClick('[contentUrl]', this, '[headerText]');return false;" />
                    </div>
                </div>
                </div>
        </div>

            <div id="filter" class="m_filter t_filter msg_filter">
                <div class="lfloat">
                    <h2>
                        <asp:Label ID="lblPageTitle" runat="server" Text="Αναλυτική κατάσταση των συναλλαγών από την οικογένεια μου" /></h2>
                </div>
                <div class="rfloat">
                    <dx:ASPxHyperLink ID="lnkBack" runat="server" Text="Back" CssClass="btn" 
                        NavigateUrl="~/Members/Referrer.aspx">
                    </dx:ASPxHyperLink>
                </div>
                <div class="clear">
                </div>
            </div>--%>

                    <div class="t_wrap">
                        <div class="send_msg t_to msg">
                            <asp:Panel ID="pnlErr" runat="server" CssClass="alert alert-danger" 
                                Visible="False" EnableViewState="False">
                                <asp:Label ID="lblErr" runat="server"></asp:Label>
                            </asp:Panel>


                <asp:Label ID="lblPageDesc" runat="server" Text="Εδώ μπορείτε να δείτε αναλυτικά από ημερομηνία έως ημερομηνία όλες της συναλλαγές που έγιναν από τα μέλη της οικογένεια σας ανά επίπεδο."></asp:Label>
                <br />
                <br />
                <table cellpadding="5" cellspacing="1">
            <tr>
                <td>
                    <asp:Label ID="lblFromDate" runat="server" Text="Από Ημ/νία : " CssClass="nobr"></asp:Label>
                </td>
                <td>
                    <dx:ASPxDateEdit ID="dtFrom" runat="server" DisplayFormatString="dd/MM/yyyy" 
                        Width="120px">
                    </dx:ASPxDateEdit>
                </td>
                <td>
                    <asp:Label ID="lblToDate" runat="server" Text="Εως " CssClass="nobr"></asp:Label>
                </td>
                <td>
                    <dx:ASPxDateEdit ID="dtTo" runat="server" DisplayFormatString="dd/MM/yyyy" 
                        Width="120px">
                    </dx:ASPxDateEdit>
                </td>
                <%--<td>
                    <asp:Label ID="lblLevel" runat="server" Text="Level: "></asp:Label>
                </td>
                <td>
                    <dx:ASPxComboBox ID="cmbLevel" runat="server" SelectedIndex="0" Width="120px">
                        <Items>
                            <dx:ListEditItem Selected="True" Text="All" Value="-1" />
                            <dx:ListEditItem Text="Level 1" Value="1" />
                            <dx:ListEditItem Text="Level 2" Value="2" />
                            <dx:ListEditItem Text="Level 3" Value="3" />
                            <dx:ListEditItem Text="Level 4" Value="4" />
                            <dx:ListEditItem Text="Level 5" Value="5" />
                        </Items>
                    </dx:ASPxComboBox>
                </td>--%>
                <td width="90%">
                    &nbsp;
                </td>
                <td>
                    <dx:ASPxButton ID="btnRefresh" runat="server" Text="Refresh">
                    </dx:ASPxButton>
                </td>
            </tr>
        </table>
        <div class="rfloat"><table cellpadding="5" cellspacing="1" >
                <tr>
                    <td><asp:Label ID="lblView" runat="server" Text="View: "></asp:Label></td>
                    <td>
                        <dx:ASPxRadioButtonList ID="cblView" runat="server" 
                            RepeatDirection="Horizontal">
                            <Items>
                                <dx:ListEditItem Text="Per Day" Value="Per Day" />
                                <dx:ListEditItem Text="Per Month" Value="Per Month" />
                            </Items>
                            <Border BorderStyle="None" />
                        </dx:ASPxRadioButtonList>
                    </td>
                </tr>
            </table></div>
        
        
    <dx:ASPxGridView ID="gvBalance" runat="server" AutoGenerateColumns="False" DataSourceID="odsBalance"
        Width="100%">
        <Columns>
            <dx:GridViewDataDateColumn FieldName="Date_Time" Caption="Date" VisibleIndex="0">
                <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy">
                </PropertiesDateEdit>
            </dx:GridViewDataDateColumn>
            <dx:GridViewDataTextColumn FieldName="Description" VisibleIndex="1">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Amount" FieldName="Amount" VisibleIndex="2">
                <PropertiesTextEdit DisplayFormatString="0.00"><%-- Caption="Amount &euro;" DisplayFormatString="0.00 &euro;"--%>
                </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Total Amount" FieldName="AmountSum" VisibleIndex="3">
                <PropertiesTextEdit DisplayFormatString="0.00"> <%--Caption="Total Amount &euro;" DisplayFormatString="0.00 &euro;"--%>
                </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
        </Columns>
        <SettingsPager PageSize="50">
        </SettingsPager>
        <Styles>
            <Header BackColor="#FF8b26" HorizontalAlign="Center" VerticalAlign="Middle">
                <Paddings Padding="0px" />
            </Header>
        </Styles>
    </dx:ASPxGridView>
    <asp:ObjectDataSource ID="odsBalance" runat="server" 
        SelectMethod="GetReferrerBalance" 
        TypeName="Dating.Referrals.Server.Site.Web.ReferrerBalanceSheet" 
        OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
            <asp:ControlParameter ControlID="dtFrom" Name="DateFrom" PropertyName="Value" 
                Type="DateTime" />
            <asp:ControlParameter ControlID="dtTo" Name="DateTo" PropertyName="Value" 
                Type="DateTime" />
            <asp:Parameter Name="ReferrerID" Type="Int32" />
            <asp:Parameter Name="ShowByPeriod" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>

                        </div>
                    </div>
        <%--</div>
		
	
	    </div>
        <div class="clear"></div>
    </div>--%>

	<uc2:WhatIsIt ID="WhatIsIt1" runat="server" />

</asp:Content>

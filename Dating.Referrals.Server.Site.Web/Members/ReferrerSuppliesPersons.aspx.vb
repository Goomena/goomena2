﻿Imports Dating.Server.Core.DLL
Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Datasets.DLL.DSMembers

Public Class ReferrerSuppliesPersons
    Inherits ReferrerBasePage

    Dim _pageData As clsPageData
    Protected ReadOnly Property CurrentPageData As clsPageData
        Get
            If (_pageData Is Nothing) Then _pageData = New clsPageData(Context)
            Return _pageData
        End Get
    End Property



    Private ReadOnly Property LevelParam As Integer
        Get
            Dim level As Integer
            If (Not String.IsNullOrEmpty(Request.QueryString("lvl"))) Then
                Integer.TryParse(Request.QueryString("lvl"), level)
            End If
            Return level
        End Get
    End Property


    Private ReadOnly Property ReferrerIdParam As Integer
        Get
            Dim affiliateid As Integer
            If (Not String.IsNullOrEmpty(Request.QueryString("affid"))) Then
                Integer.TryParse(Request.QueryString("affid"), affiliateid)
            End If
            Return affiliateid
        End Get
    End Property



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pnlErr.Visible = False

        Try
            If (Me.IsReferrer()) Then
                ' do nothing
            Else
                Response.Redirect("Referrer.aspx")
            End If

        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        'If (Not Page.IsPostBack) Then

        '    Try

        '        LoadLAG()
        '        GetReferrerCreditsObject()
        '        LoadReferrerInfo()



        '    Catch ex As Exception
        '        WebErrorMessageBox(Me, ex, "Page_Load")
        '    End Try

        'End If
        Try
            If (Not Me.IsPostBack) Then
                LoadLAG()
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try


        Try
            SetReferrerUI()
            'If (Me.GetCurrentProfile().ReferrerParentId.HasValue AndAlso Me.GetCurrentProfile().ReferrerParentId > 0)Then
            '    'lblReferrerTitle.Text = "Επισκόπηση συνεργασίας"
            'Else
            '    mvMain.SetActiveView(vwSetReferrerParent)
            'End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
        SetReferrerUI()
    End Sub


    Protected Sub LoadLAG()
        Try

            lblPageDesc.Text = CurrentPageData.GetCustomString("lblPageDesc")
            gvProfileInfo.SettingsText.EmptyDataRow = CurrentPageData.GetCustomString("gvProfileInfo.EmptyDataRow")

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender

        Try
            Dim cPageBasic As Dating.Server.Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub




    Private Sub SetReferrerUI()
        SetDaysRange()
        GetReferrerCreditsObject(dtFrom.Date, dtTo.Date)
        LoadReferrerInfo()
    End Sub


    Private Sub SetDaysRange()
        If (Not Page.IsPostBack) Then
            If (SessionVariables.ReferrerCredits IsNot Nothing) Then
                Try
                    dtFrom.Value = SessionVariables.ReferrerCredits.DateFrom
                    dtTo.Value = SessionVariables.ReferrerCredits.DateTo
                Catch ex As Exception
                End Try
            End If

            If (dtFrom.Value Is Nothing OrElse dtTo.Value Is Nothing) Then
                dtFrom.Date = DateTime.UtcNow.AddDays(-clsReferrer.DefaultDaysRange)
                dtTo.Date = DateTime.UtcNow.AddDays(1)
            End If
        End If
    End Sub


    Private Sub LoadReferrerInfo()
        Dim affiliate As DSMembers.EUS_ProfilesRow = DataHelpers.GetEUS_Profiles_ByProfileIDRow(ReferrerIdParam)
        Dim sb As New StringBuilder()

        sb.Append("<strong>")
        sb.Append(CurrentPageData.GetCustomString("lblUser") & ": " & affiliate.LoginName)
        sb.Append("</strong>")
        sb.Append(" - ")
        sb.Append(CurrentPageData.GetCustomString("lblLEVEL") & ": ")
        sb.Append("<strong>")
        sb.Append(LevelParam)
        sb.Append("</strong>")
        sb.Append("<br/>")
        sb.Append(CurrentPageData.GetCustomString("lblCountry") & ": ")
        sb.Append("<strong>")
        sb.Append(affiliate.Country)
        sb.Append("</strong>")
        sb.Append(" - ")
        sb.Append(CurrentPageData.GetCustomString("lblCity") & ": ")
        sb.Append("<strong>")
        sb.Append(affiliate.City)
        sb.Append("</strong>")
        lblProfileInfo.Text = sb.ToString()

        gvProfileInfo.DataBind()
    End Sub


    Protected Sub odsProfileInfo_Selecting(sender As Object, e As System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs) Handles odsProfileInfo.Selecting
        e.InputParameters("ReferrerID") = Me.ReferrerIdParam
        e.InputParameters("LevelParam") = Me.LevelParam
    End Sub



    'Public Function GetProfilesReferrers(targetLevel As Integer, profileId As Integer, DateFrom As DateTime?, DateTo As DateTime?, affCredits1 As clsReferrerCredits) As DataTable

    '    Dim dtPyramid As New DataTable()
    '    dtPyramid.Columns.Add(New Data.DataColumn("LoginName"))
    '    dtPyramid.Columns.Add(New Data.DataColumn("SalesCRD", GetType(Double)))
    '    dtPyramid.Columns.Add(New Data.DataColumn("CommissionCRD", GetType(Double)))
    '    dtPyramid.Columns.Add(New Data.DataColumn("PersonsNavigateUrl"))


    '    Try
    '        Dim drs As DSCustom.ReferrersRepositoryDataRow() = affCredits1.GetReferrersForLevel(targetLevel)
    '        Dim c As Integer
    '        For c = 0 To drs.Length - 1

    '            Dim _dr As Data.DataRow = dtPyramid.NewRow()
    '            _dr("LoginName") = drs(c).LoginName
    '            _dr("SalesCRD") = drs(c).CreditsSum
    '            _dr("CommissionCRD") = drs(c).CommissionCredits
    '            _dr("PersonsNavigateUrl") = "~/Members/ReferrerSuppliesPersons.aspx?lvl=" & targetLevel & "&affid=" & drs(c).ProfileID & "&master=inner"
    '            dtPyramid.Rows.Add(_dr)
    '        Next

    '    Catch ex As Exception
    '        WebErrorMessageBox(Me, ex, "BindPyramid")
    '    End Try

    '    Return dtPyramid
    'End Function



    Public Function GetReferrerCreditsHistory(ByVal DateFrom As Date?, ByVal DateTo As Date?, ByVal ReferrerID As Integer?, ByVal LevelParam As Integer?) As DSReferrers.GetReferrerCreditsHistory2DataTable
        Dim ds As DSReferrers = DataHelpers.GetReferrerCreditsHistory(DateFrom, DateTo, ReferrerID, LevelParam)
        Return ds.GetReferrerCreditsHistory2
    End Function




    Protected Sub gvProfileInfo_CustomColumnDisplayText(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewColumnDisplayTextEventArgs) Handles gvProfileInfo.CustomColumnDisplayText
        Try

            If (e.Column.FieldName = "Description") Then ' OrElse e.Column.FieldName = "CommissionCRD"


                Dim dr As System.Data.DataRow = gvProfileInfo.GetDataRow(e.VisibleRowIndex)

                Dim money As Double
                Select Case LevelParam
                    Case 1
                        If (Not dr.IsNull("REF_L1_Money")) Then money = dr("REF_L1_Money")
                    Case 2
                        If (Not dr.IsNull("REF_L2_Money")) Then money = dr("REF_L2_Money")
                    Case 3
                        If (Not dr.IsNull("REF_L3_Money")) Then money = dr("REF_L3_Money")
                    Case 4
                        If (Not dr.IsNull("REF_L4_Money")) Then money = dr("REF_L4_Money")
                    Case 5
                        If (Not dr.IsNull("REF_L5_Money")) Then money = dr("REF_L5_Money")
                End Select

                If e.Column.FieldName = "Description" Then

                    'Dim credits As Double = 0
                    'If (Not dr.IsNull("EuroAmount")) Then
                    '    credits = Convert.ToDouble(dr("EuroAmount"))
                    '    credits = Math.Abs(credits)
                    'End If

                    If (dr("CreditsType") = "UNLOCK_CONVERSATION") Then
                        Dim CreditsDescription_Used = CurrentPageData.GetCustomString("Info_UserUnlockedConversation")
                        Dim profileUrl As String = ProfileHelper.GetProfileNavigateUrl(dr("CustomerLoginName").ToString())
                        profileUrl = "<a href=""" & profileUrl & """ target=""_blank"">" & dr("CustomerLoginName") & "</a>"
                        e.DisplayText = CreditsDescription_Used.Replace("[LOGINNAME]", profileUrl)

                    ElseIf (dr("CreditsType") = "UNLOCK_MESSAGE_READ") Then
                        Dim profileUrl As String = ProfileHelper.GetProfileNavigateUrl(dr("CustomerLoginName").ToString())
                        profileUrl = "<a href=""" & profileUrl & """ target=""_blank"">" & dr("CustomerLoginName") & "</a>"
                        e.DisplayText = Me.CurrentPageData.GetCustomString("Info_UserUnlockedMessage_READ").Replace("[LOGINNAME]", profileUrl)

                    ElseIf (dr("CreditsType") = "UNLOCK_MESSAGE_SEND") Then
                        Dim profileUrl As String = ProfileHelper.GetProfileNavigateUrl(dr("CustomerLoginName").ToString())
                        profileUrl = "<a href=""" & profileUrl & """ target=""_blank"">" & dr("CustomerLoginName") & "</a>"
                        e.DisplayText = Me.CurrentPageData.GetCustomString("Info_UserUnlockedMessage_SEND").Replace("[LOGINNAME]", profileUrl)

                    End If
                End If

                'If e.Column.FieldName = "CommissionCRD" Then
                '    e.DisplayText = money

                '    'Dim credits As Double = 0
                '    'If (Not dr.IsNull("EuroAmount")) Then
                '    '    credits = Convert.ToDouble(dr("EuroAmount"))
                '    '    credits = Math.Abs(credits)
                '    'End If
                '    'e.DisplayText = GetCurrentReferrerCommissions().CalcCommissionForLevel(credits, Me.LevelParam)
                'End If

            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


End Class
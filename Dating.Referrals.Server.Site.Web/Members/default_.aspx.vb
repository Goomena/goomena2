﻿'Imports Sites.SharedSiteCode
Imports Library.Public
Imports Dating.Server.Core.DLL
Imports System.Data.SqlClient

Public Class _default1
    Inherits BasePage


    Const __TESTING__ As Boolean = False


    Dim _pageData As clsPageData
    Protected ReadOnly Property CurrentPageData As clsPageData
        Get
            If (_pageData Is Nothing) Then _pageData = New clsPageData(Context)
            Return _pageData
        End Get
    End Property


    Protected Overrides Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        Try
            If clsCurrentContext.VerifyLogin() = True Then
                Me.MasterPageFile = "~/Members/Members2.Master"
                'If (Request("master") = "inner") Then
                '    Me.MasterPageFile = "~/Members/Members2Inner.Master"
                'Else
                'End If
            Else
                clsCurrentContext.ClearSession(Session)
                Response.Redirect(ResolveUrl("~/Login.aspx"), False)
                HttpContext.Current.ApplicationInstance.CompleteRequest()
            End If
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try

            If (clsCurrentContext.VerifyLogin() = False) Then
                FormsAuthentication.SignOut()
                Response.Redirect(Page.ResolveUrl("~/Login.aspx"))
            End If


            'Try
            '    Dim cPageBasic As Dating.Server.Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            '    GeneralFunctions.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
            'Catch ex As Exception
            '    WebErrorMessageBox(Me, ex, "Page_Load")
            'End Try


            If (Not Me.IsPostBack) Then

                LoadLAG()
                Me._LoadData()

                Dim prms As New clsSearchHelperParameters()
                prms.CurrentProfileId = Me.MasterProfileId
                prms.ReturnRecordsWithStatus = ProfileStatusEnum.Approved
                prms.SearchSort = SearchSortEnum.NearestDistance
                prms.zipstr = Me.SessionVariables.MemberData.Zip
                prms.latitudeIn = Me.SessionVariables.MemberData.latitude
                prms.longitudeIn = Me.SessionVariables.MemberData.longitude
                prms.Distance = 1000
                prms.LookingFor_ToMeetMaleID = Me.SessionVariables.MemberData.LookingFor_ToMeetMaleID
                prms.LookingFor_ToMeetFemaleID = Me.SessionVariables.MemberData.LookingFor_ToMeetFemaleID
                prms.NumberOfRecordsToReturn = 12
                prms.AdditionalWhereClause = "and not exists(select * from EUS_ProfilesViewed where FromProfileID=@CurrentProfileId and ToProfileID=EUS_Profiles.ProfileID)"
                prms.performCount = False
                lvNearest.DataSource = clsSearchHelper.GetMembersToSearchDataTable(prms)

                'dvNearest.DataSource = clsSearchHelper.GetMembersToSearchDataTable(Me.MasterProfileId, ProfileStatusEnum.Approved, SearchSortEnum.NearestDistance, Me.GetCurrentProfile().Zip, 1000, 5)
                lvNearest.DataBind()

            End If

        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try
    End Sub


    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender

        Try
            Dim cPageBasic As Dating.Server.Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            GeneralFunctions.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub



    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
        _LoadData()
        UpdateUserControls(Me)
    End Sub


    Protected Sub LoadLAG()
        Try
            Dim cPageBasic As Dating.Server.Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            lnkUploadPhoto.Text = CurrentPageData.GetCustomString(lnkUploadPhoto.ID)
            lnkUploadPhoto.NavigateUrl = CurrentPageData.GetCustomString("lnkUploadPhoto.NavigateUrl")

            'lnkTestemonials.Text = CurrentPageData.GetCustomString("lnkTestemonials_ReadMore")
            'lblWelcome.Text = CurrentPageData.GetCustomString(lblWelcome.ID)
            'lnkMembersNewest.Text = CurrentPageData.GetCustomString(lnkMembersNewest.ID)
            'lnkMembersNear.Text = CurrentPageData.GetCustomString(lnkMembersNear.ID)
            'lnkMembersNewestAll.Text = CurrentPageData.GetCustomString(lnkMembersNewestAll.ID)
            'lnkMembersNearAll.Text = CurrentPageData.GetCustomString(lnkMembersNearAll.ID)

            lnkProfilesYouHaveViewed.Text = CurrentPageData.GetCustomString(lnkProfilesYouHaveViewed.ID)
            lnkAllProfilesYouHaveViewed.Text = CurrentPageData.GetCustomString(lnkAllProfilesYouHaveViewed.ID)

            lnkMembersNear.Text = CurrentPageData.GetCustomString(lnkMembersNear.ID)
            lnkMembersNearAll.Text = CurrentPageData.GetCustomString(lnkMembersNearAll.ID)

            lnkHowWorksMale.Text = CurrentPageData.GetCustomString("lnkHowWorks")
            lnkHowWorksFemale.Text = CurrentPageData.GetCustomString("lnkHowWorks")

            If (ProfileHelper.IsFemale(Me.GetCurrentProfile().GenderId)) Then
                lblDashboardTop1.Text = CurrentPageData.GetCustomString("FEMALE_lblDashboardTop1")
                lblDashboardTop2.Text = CurrentPageData.GetCustomString("FEMALE_lblDashboardTop2")

                lblImportantAnnouncement.Text = CurrentPageData.GetCustomString("lblImportantAnnouncement_Female")
                lblImportantAdvice.Text = CurrentPageData.GetCustomString("lblImportantAdvice_Female")
                'lblTestemonialsText.Text = CurrentPageData.GetCustomString("lblTestemonialsText_Female")

                liHowWorksMale.Visible = False
                liHowWorksFemale.Visible = True
            Else
                lblDashboardTop1.Text = CurrentPageData.GetCustomString("MALE_lblDashboardTop1")
                lblDashboardTop2.Text = CurrentPageData.GetCustomString("MALE_lblDashboardTop2")

                lblImportantAnnouncement.Text = CurrentPageData.GetCustomString("lblImportantAnnouncement_Male")
                lblImportantAdvice.Text = CurrentPageData.GetCustomString("lblImportantAdvice_Male")
                'lblTestemonialsText.Text = CurrentPageData.GetCustomString("lblTestemonialsText_Male")

                liHowWorksMale.Visible = True
                liHowWorksFemale.Visible = False
            End If


            lnkAddPhotos.Text = CurrentPageData.GetCustomString("lnkAddPhotos")
            lnkWhoViewedMe.Text = CurrentPageData.GetCustomString("lnkWhoViewedMe")
            lnkWhoFavoritedMe.Text = CurrentPageData.GetCustomString("lnkWhoFavoritedMe")
            lnkMyFavoriteList.Text = CurrentPageData.GetCustomString("lnkMyFavoriteList")
            lnkMyBlockedList.Text = CurrentPageData.GetCustomString("lnkMyBlockedList")
            lnkMyViewedList.Text = CurrentPageData.GetCustomString("lnkMyViewedList")
            lnkNotifications.Text = CurrentPageData.GetCustomString("lnkNotifications")

            lblUpdateProfileData.Text = CurrentPageData.GetCustomString("lblUpdateProfileData")
            lnkEditProfile.Text = CurrentPageData.GetCustomString("lnkEditProfile")

            SetControlsValue(Me, CurrentPageData)

            lblItemsName.Text = Me.CurrentPageData.GetCustomString("CartDefaultView")
            lnkViewDescription.Text = Me.CurrentPageData.GetCustomString("CartDefaultWhatIsIt")
            lnkViewDescription.OnClientClick = OnMoreInfoClickFunc(
                lnkViewDescription.OnClientClick,
                ResolveUrl("~/Members/InfoWin.aspx?info=dashboard"),
                Me.CurrentPageData.GetCustomString("CartDefaultView"))
            popupWhatIs.HeaderText = Me.CurrentPageData.GetCustomString("CartDefaultView")


        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub



    Private Sub _LoadData()
        Try

            lblWelcome.Text = CurrentPageData.GetCustomString("lblWelcome")
            lblWelcome.Text = lblWelcome.Text.Replace("###LOGINNAME###", Me.SessionVariables.MemberData.LoginName)

            btnSearch.Text = CurrentPageData.GetCustomString("btnSearch")
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

        Try

            'Dim _defaultPhoto = DataHelpers.GetProfilesDefaultPhoto(Me.MirrorProfileId)
            Dim AgeIssue = (Me.GetCurrentProfile().Birthday IsNot Nothing AndAlso ProfileHelper.GetCurrentAge(Me.GetCurrentProfile().Birthday) < 18)
            If AgeIssue Then
                divAgeIssue.Visible = True
                lblAgeIssueText.Text = CurrentPageData.GetCustomString("lblAgeIssueText")

                divUploadPhoto.Visible = False
                'divTestemonials.Visible = False
            Else
                divAgeIssue.Visible = False
                lblAgeIssueText.Text = ""

                Dim currentUserHasPhotos As Boolean = clsUserDoes.HasPhotos(Me.MasterProfileId, Me.MirrorProfileId)
                If (Not currentUserHasPhotos) Then
                    divUploadPhoto.Visible = True
                    'divTestemonials.Visible = False
                Else
                    divUploadPhoto.Visible = False
                    'divTestemonials.Visible = True
                End If

            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub






    Protected Sub lvWhomIViewed_DataBound(sender As Object, e As EventArgs) Handles lvWhomIViewed.DataBound
        Try
            pnlProfilesYouHaveViewed.Visible = lvWhomIViewed.Items.Count > 0

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Protected Sub lvNearest_DataBound(sender As Object, e As EventArgs) Handles lvNearest.DataBound
        Try
            divUpdateProfileData.Visible = (String.IsNullOrEmpty(Me.GetCurrentProfile().City) OrElse String.IsNullOrEmpty(Me.GetCurrentProfile().Region))
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub
End Class
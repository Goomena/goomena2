﻿Imports Dating.Server.Core.DLL

Public Class ReferrerEstimate2
    Inherits BasePage


#Region "Props"


    Dim _pageData As clsPageData
    Protected ReadOnly Property CurrentPageData As clsPageData
        Get
            If (_pageData Is Nothing) Then _pageData = New clsPageData("~/Members/ReferrerEstimate.aspx", Context)
            Return _pageData
        End Get
    End Property




    Private ReadOnly Property LevelParam As Integer
        Get
            Dim level As Integer
            If (Not String.IsNullOrEmpty(Request.QueryString("lvl"))) Then
                Integer.TryParse(Request.QueryString("lvl"), level)
            End If
            Return level
        End Get
    End Property


    Private ReadOnly Property ReferrerIdParam As Integer
        Get
            Dim affiliateid As Integer
            If (Not String.IsNullOrEmpty(Request.QueryString("affid"))) Then
                Integer.TryParse(Request.QueryString("affid"), affiliateid)
            End If
            Return affiliateid
        End Get
    End Property



    Private _referrerCommissions As clsReferrerCommissions
    Private Function GetCurrentReferrerCommissions() As clsReferrerCommissions
        If (_referrerCommissions Is Nothing) Then
            _referrerCommissions = New clsReferrerCommissions()
            _referrerCommissions.Load(Me.MasterProfileId)
        End If
        Return _referrerCommissions
    End Function


    Private m_Children1 As Double
    Private m_Children2 As Double
    Private m_Children3 As Double
    Private m_Children4 As Double
    Private m_Family As Double

    Private m_MessageDay As Double
    Private m_MessageMonth As Double
    Private m_MessageYear As Double
    Private m_VidDay As Double
    Private m_VidMonth As Double
    Private m_VidYear As Double
    Private m_OfferDay As Double
    Private m_OfferMonth As Double
    Private m_OfferYear As Double
    Private m_VidDayAndRate As Double
    Private m_MessageDayAndRate As Double


    Private ReadOnly Property CreditsForMessage As Double
        Get
            Dim days As Double = spinMessageCredits.Number
            'If (Not String.IsNullOrEmpty(hdfCreditsForMessage.Value)) Then
            '    Integer.TryParse(hdfCreditsForMessage.Value, days)
            'End If
            Return days
        End Get
    End Property


    Private ReadOnly Property CreditsForVidChat As Double
        Get
            Dim days As Double = spinVideoChatCredits.Number
            'If (Not String.IsNullOrEmpty(hdfCreditsForVidChat.Value)) Then
            '    Integer.TryParse(hdfCreditsForVidChat.Value, days)
            'End If
            'days = 0
            Return days
        End Get
    End Property

#End Region


    Protected Overrides Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        Try
            If clsCurrentContext.VerifyLogin() = True Then
                Me.MasterPageFile = "~/Members/Members2.Master"
            Else
                clsCurrentContext.ClearSession(Session)
                Response.Redirect(ResolveUrl("~/Login.aspx"), False)
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pnlErr.Visible = False

        Try
            If (Me.IsReferrer()) Then
                ' do nothing
            Else
                Response.Redirect("Referrer.aspx")
            End If

        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Try
            'GetReferrerCreditsObject()
            If (Not Page.IsPostBack) Then

                Dim config As New clsConfigValues()
                spinMessageCredits.Number = config.referrer_commission_conv_rate * ProfileHelper.Config_UNLOCK_MESSAGE_SEND_CREDITS
                'spinMessageCredits.Number = ProfileHelper.Config_UNLOCK_MESSAGE_SEND_CREDITS

                spinConvRate.Number = GetCurrentReferrerCommissions().REF_CommissionConvRate
                spinCommissionLev1.Number = GetCurrentReferrerCommissions().REF_CommissionLevel1
                spinCommissionLev2.Number = GetCurrentReferrerCommissions().REF_CommissionLevel2
                spinCommissionLev3.Number = GetCurrentReferrerCommissions().REF_CommissionLevel3
                spinCommissionLev4.Number = GetCurrentReferrerCommissions().REF_CommissionLevel4
                spinCommissionLev5.Number = GetCurrentReferrerCommissions().REF_CommissionLevel5

                LoadLAG()
                calcMoney()
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try
    End Sub




    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
        calcMoney()
    End Sub


    Protected Sub LoadLAG()
        Try

            lblPageTitle.Text = CurrentPageData.GetCustomString("lblPageTitle")
            lnkBack.Text = CurrentPageData.GetCustomString("lnkBack")
            lblPageDesc.Text = CurrentPageData.GetCustomString("lblPageDesc")
            lblCalcIncomeText.Text = CurrentPageData.GetCustomString("lblCalcIncomeText")
            lnkRecalc1.Text = CurrentPageData.GetCustomString("lnkRecalculate")
            lnkRecalc2.Text = CurrentPageData.GetCustomString("lnkRecalculate")

            lblSpinChildrenBfr.Text = CurrentPageData.GetCustomString("lblSpinChildrenBfr")
            lblSpinChildrenAft.Text = CurrentPageData.GetCustomString("lblSpinChildrenAft")
            lblSpinChildren2Bfr.Text = CurrentPageData.GetCustomString("lblSpinChildren2Bfr")
            lblSpinChildren2Aft.Text = CurrentPageData.GetCustomString("lblSpinChildren2Aft")
            lblReferrals2Text.Text = CurrentPageData.GetCustomString("lblReferrals2Text")
            lblReferrals3Text.Text = CurrentPageData.GetCustomString("lblReferrals3Text")
            lblReferrals4Text.Text = CurrentPageData.GetCustomString("lblReferrals4Text")
            lblReferrals5Text.Text = CurrentPageData.GetCustomString("lblReferrals5Text")
            lblFamilyText.Text = CurrentPageData.GetCustomString("lblFamilyText")


            lnkShowCalcs1.Text = CurrentPageData.GetCustomString("lnkShowCalcs")
            lnkShowCalcs2.Text = CurrentPageData.GetCustomString("lnkShowCalcs")
            lnkShowCalcs3.Text = CurrentPageData.GetCustomString("lnkShowCalcs")
            lnkShowCalcs4.Text = CurrentPageData.GetCustomString("lnkShowCalcs")
            lnkShowCalcs5.Text = CurrentPageData.GetCustomString("lnkShowCalcs")


            lblSpinMessagesBfr.Text = CurrentPageData.GetCustomString("lblSpinMessagesBfr")
            lblSpinMessagesAft.Text = CurrentPageData.GetCustomString("lblSpinMessagesAft")
            lblSpinMessages2Bfr.Text = CurrentPageData.GetCustomString("lblSpinMessages2Bfr")
            lblSpinMessages2Aft.Text = CurrentPageData.GetCustomString("lblSpinMessages2Aft")
            lblMessageDayText.Text = CurrentPageData.GetCustomString("lblAmountDayText")
            lblMessageMonthText.Text = CurrentPageData.GetCustomString("lblAmountMonthText")
            lblMessageYearText.Text = CurrentPageData.GetCustomString("lblAmountYearText")

            lblSpinVidChatBfr.Text = CurrentPageData.GetCustomString("lblSpinVidChatBfr")
            lblSpinVidChatAft.Text = CurrentPageData.GetCustomString("lblSpinVidChatAft")
            lblSpinVidChat2Bfr.Text = CurrentPageData.GetCustomString("lblSpinVidChat2Bfr")
            lblSpinVidChat2Aft.Text = CurrentPageData.GetCustomString("lblSpinVidChat2Aft")
            lblVidDayText.Text = CurrentPageData.GetCustomString("lblAmountDayText")
            lblVidMonthText.Text = CurrentPageData.GetCustomString("lblAmountMonthText")
            lblVidYearText.Text = CurrentPageData.GetCustomString("lblAmountYearText")

            lblSpinOffersBfr.Text = CurrentPageData.GetCustomString("lblSpinOffersBfr")
            lblSpinOffersAft.Text = CurrentPageData.GetCustomString("lblSpinOffersAft")
            lblSpinOffers2Bfr.Text = CurrentPageData.GetCustomString("lblSpinOffers2Bfr")
            lblSpinOffers2Aft.Text = CurrentPageData.GetCustomString("lblSpinOffers2Aft")
            lblOfferDayText.Text = CurrentPageData.GetCustomString("lblAmountDayText")
            lblOfferMonthText.Text = CurrentPageData.GetCustomString("lblAmountMonthText")
            lblOfferYearText.Text = CurrentPageData.GetCustomString("lblAmountYearText")

            lblTotalsText.Text = CurrentPageData.GetCustomString("lblTotalsText")
            lblTotalDayText.Text = CurrentPageData.GetCustomString("lblAmountDayText")
            lblTotalMonthText.Text = CurrentPageData.GetCustomString("lblAmountMonthText")
            lblTotalYearText.Text = CurrentPageData.GetCustomString("lblAmountYearText")

            lblFooterText.Text = CurrentPageData.GetCustomString("lblFooterText")

            lblItemsName.Text = Me.CurrentPageData.GetCustomString("CartReferrerEstimateView")
            'lnkViewDescription.Text = Me.CurrentPageData.GetCustomString("CartReferrerEstimateWhatIsIt")
            'lnkViewDescription.OnClientClick = OnMoreInfoClickFunc(
            '    lnkViewDescription.OnClientClick,
            '    ResolveUrl("~/Members/InfoWin.aspx?info=referrerestimate"),
            '    Me.CurrentPageData.GetCustomString("CartReferrerEstimateView"))
            WhatIsIt1.PopupControl.HeaderText = Me.CurrentPageData.GetCustomString("CartReferrerEstimateView")

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender

        Try
            Dim cPageBasic As Dating.Server.Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub


    Private Sub CalcChildren()
        m_Children1 = spinChildren.Number
        Dim _children2 As Integer = spinChildren2.Number

        m_Children2 = m_Children1 * _children2
        m_Children3 = m_Children1 * _children2 * _children2
        m_Children4 = m_Children1 * _children2 * _children2 * _children2
        m_Family = m_Children1 + m_Children2 + m_Children3 + m_Children4 + 1


        lblChildren.Text = m_Children1.ToString("0")
        lblChildren2.Text = m_Children2.ToString("0")
        lblChildren3.Text = m_Children3.ToString("0")
        lblChildren5.Text = m_Children4.ToString("0")
        lblFamily.Text = m_Family.ToString("0")

        lblCalcReferrals.Text =
            "{0} referrals[2] = {0}  <br>" & vbCrLf &
            "{2} referrals[3] = {0} * {1} <br>" & vbCrLf &
            "{3} referrals[4] = {2} * {1} <br>" & vbCrLf &
            "{4} referrals[5] = {3} * {1} <br>" & vbCrLf &
            "{5} ολόκληρη η οικογένεια σας = 1 + {0} + {2} + {3} + {4} <br>" & vbCrLf

        lblCalcReferrals.Text = String.Format(lblCalcReferrals.Text, m_Children1, _children2, m_Children2, m_Children3, m_Children4, m_Family)
    End Sub

    Private Sub CalcMessages()
        Dim comm1 As Double = Me.CreditsForMessage * spinCommissionLev1.Number
        Dim comm2 As Double = Me.CreditsForMessage * spinCommissionLev2.Number
        Dim comm3 As Double = Me.CreditsForMessage * spinCommissionLev3.Number
        Dim comm4 As Double = Me.CreditsForMessage * spinCommissionLev4.Number
        Dim comm5 As Double = Me.CreditsForMessage * spinCommissionLev5.Number
        'Dim rate As Double = spinConvRate.Number
        Dim _spinMessages As Double = spinMessages.Number
        Dim _spinMessages2 As Double = spinMessages2.Number

        m_MessageDay = (comm1 * _spinMessages)
        m_MessageDay = m_MessageDay + (comm2 * _spinMessages2 * m_Children1)
        m_MessageDay = m_MessageDay + (comm3 * _spinMessages2 * m_Children2)
        m_MessageDay = m_MessageDay + (comm4 * _spinMessages2 * m_Children3)
        m_MessageDay = m_MessageDay + (comm5 * _spinMessages2 * m_Children4)
        m_MessageDayAndRate = m_MessageDay '* rate

        m_MessageMonth = m_MessageDayAndRate * 30
        m_MessageYear = m_MessageDayAndRate * 365


        lblMessageDay.Text = m_MessageDayAndRate.ToString("0.00")
        lblMessageMonth.Text = m_MessageMonth.ToString("0")
        lblMessageYear.Text = m_MessageYear.ToString("0")
        'lblMessageDay.Text = "&euro;" & m_MessageDayAndRate.ToString("0.00")
        'lblMessageMonth.Text = "&euro;" & m_MessageMonth.ToString("0")
        'lblMessageYear.Text = "&euro;" & m_MessageYear.ToString("0")

        Dim str1 As String =
            "Credits for a Message = {0}  <br>" & vbCrLf &
            "Calculated commission per message lev[1] = {2} CRD <br>" & vbCrLf &
            "Calculated commission per message lev[2] = {3} CRD <br>" & vbCrLf &
            "Calculated commission per message lev[3] = {4} CRD <br>" & vbCrLf &
            "Calculated commission per message lev[4] = {5} CRD <br>" & vbCrLf &
            "Calculated commission per message lev[5] = {6} CRD <br>" & vbCrLf
        '"Money conversion rate = {1}  <br>" & vbCrLf &
        str1 = String.Format(str1, Me.CreditsForMessage, "", comm1, comm2, comm3, comm4, comm5) 'rate,

        Dim str2 As String =
            "Your messages input = {0}  <br>" & vbCrLf &
            "Referrals messages input = {1}  <br>" & vbCrLf
        str2 = String.Format(str2, _spinMessages, _spinMessages2)

        Dim str3 As String =
            "Your total commission credits {0} = {1} * {2} (Commission[1] * messages)  <br>" & vbCrLf &
            "Referrals[2] commission credits {3} = {4} * {5} * {6} (Commission[2] * messages * referrals[2]) <br>" & vbCrLf
        str3 = String.Format(str3, (comm1 * _spinMessages), comm1, _spinMessages, (comm2 * _spinMessages2 * m_Children1), comm2, _spinMessages2, m_Children1)

        Dim str4 As String =
            "Referrals[3] commission credits {0} = {1} * {2} * {3} (Commission[3] * messages * referrals[3])<br>" & vbCrLf &
            "Referrals[4] commission credits {4} = {5} * {6} * {7} (Commission[4] * messages * referrals[4])<br>" & vbCrLf
        str4 = String.Format(str4, (comm3 * _spinMessages2 * m_Children2), comm3, _spinMessages2, m_Children2, (comm4 * _spinMessages2 * m_Children3), comm4, _spinMessages2, m_Children3)

        Dim str5 As String =
            "Referrals[5] commission credits {0} = {1} * {2} * {3} (Commission[5] * messages * referrals[5])<br>" & vbCrLf
        str5 = String.Format(str5, (comm5 * _spinMessages2 * m_Children4), comm5, _spinMessages2, m_Children4)

        Dim str6 As String = ""
        '    "Commission credits for a day = {0} <br>" & vbCrLf &
        '    "Euros for a day {0} * {1} = {2} <br>" & vbCrLf &
        '    "Euros for a month {2} * {4} = {3} <br>" & vbCrLf &
        '    "Euros for a year {2} * {6} = {5} <br>" & vbCrLf
        'str6 = String.Format(str6, m_MessageDay, Rate, m_MessageDayAndRate, m_MessageMonth, 30, m_MessageYear, 365)

        lblCalcMessages.Text = str1 & str2 & str3 & str4 & str5 & str6

    End Sub


    Private Sub CalcVideoChat()
        Dim comm1 As Double = Me.CreditsForVidChat * spinCommissionLev1.Number
        Dim comm2 As Double = Me.CreditsForVidChat * spinCommissionLev2.Number
        Dim comm3 As Double = Me.CreditsForVidChat * spinCommissionLev3.Number
        Dim comm4 As Double = Me.CreditsForVidChat * spinCommissionLev4.Number
        Dim comm5 As Double = Me.CreditsForVidChat * spinCommissionLev5.Number
        Dim rate As Double = spinConvRate.Number
        Dim _spinVidChat As Integer = spinVidChat.Number
        Dim _spinVidChat2 As Integer = spinVidChat2.Number
        _spinVidChat = (_spinVidChat * _spinVidChat2)

        m_VidDay = (comm1 * _spinVidChat)
        m_VidDay = m_VidDay + (comm2 * _spinVidChat * m_Children1)
        m_VidDay = m_VidDay + (comm3 * _spinVidChat * m_Children2)
        m_VidDay = m_VidDay + (comm4 * _spinVidChat * m_Children3)
        m_VidDay = m_VidDay + (comm5 * _spinVidChat * m_Children4)
        m_VidDay = m_VidDay / 7
        m_VidDayAndRate = m_VidDay * rate

        m_VidMonth = m_VidDayAndRate * 30
        m_VidYear = m_VidDayAndRate * 365


        'lblVidDay.Text = "&euro;" & m_VidDayAndRate.ToString("0.00")
        'lblVidMonth.Text = "&euro;" & m_VidMonth.ToString("0")
        'lblVidYear.Text = "&euro;" & m_VidYear.ToString("0")
        lblVidDay.Text = m_VidDayAndRate.ToString("0.00")
        lblVidMonth.Text = m_VidMonth.ToString("0")
        lblVidYear.Text = m_VidYear.ToString("0")


        Dim str1 As String =
            "Credits for a VideoChat (1 minute) = {0}  <br>" & vbCrLf &
            "Money conversion rate = {1}  <br>" & vbCrLf &
            "Calculated commission per VideoChat[1] = {2} CRD <br>" & vbCrLf &
            "Calculated commission per VideoChat[2] = {3} CRD <br>" & vbCrLf &
            "Calculated commission per VideoChat[3] = {4} CRD <br>" & vbCrLf &
            "Calculated commission per VideoChat[4] = {5} CRD <br>" & vbCrLf &
            "Calculated commission per VideoChat[5] = {6} CRD <br>" & vbCrLf
        str1 = String.Format(str1, Me.CreditsForVidChat, rate, comm1, comm2, comm3, comm4, comm5)

        Dim str2 As String =
            "Your input of VideoChats per week = {0}  <br>" & vbCrLf &
            "Average minutes input per VideoChat = {1}  <br>" & vbCrLf &
            "Average minutes per week = {2}  <br>" & vbCrLf
        str2 = String.Format(str2, spinVidChat.Number, spinVidChat2.Number, _spinVidChat)

        Dim str3 As String =
            "Calculating commission credits for a week  <br>" & vbCrLf &
            "Your total commission credits {0} = {1} * {2} (Commission[1] * minutes)  <br>" & vbCrLf &
            "Referrals[2] commission credits {3} = {4} * {5} * {6} (Commission[2] * minutes * referrals[2]) <br>" & vbCrLf
        str3 = String.Format(str3, (comm1 * _spinVidChat), comm1, _spinVidChat, (comm2 * _spinVidChat * m_Children1), comm2, _spinVidChat, m_Children1)

        Dim str4 As String =
            "Referrals[3] commission credits {0} = {1} * {2} * {3} (Commission[3] * minutes * referrals[3])<br>" & vbCrLf &
            "Referrals[4] commission credits {4} = {5} * {6} * {7} (Commission[4] * minutes * referrals[4])<br>" & vbCrLf
        str4 = String.Format(str4, (comm3 * _spinVidChat * m_Children2), comm3, _spinVidChat, m_Children2, (comm4 * _spinVidChat * m_Children3), comm4, _spinVidChat, m_Children3)

        Dim str5 As String =
            "Referrals[5] commission credits {0} = {1} * {2} * {3} (Commission[5] * minutes * referrals[5])<br>" & vbCrLf
        str5 = String.Format(str5, (comm5 * _spinVidChat * m_Children4), comm5, _spinVidChat, m_Children4)

        Dim str6 As String =
            "Commission credits for a day = {0} <br>" & vbCrLf &
            "Euros for a day {0} * {1} = {2} <br>" & vbCrLf &
            "Euros for a month {2} * {4} = {3} <br>" & vbCrLf &
            "Euros for a year {2} * {6} = {5} <br>" & vbCrLf
        str6 = String.Format(str6, m_VidDay, rate, m_VidDayAndRate, m_VidMonth, 30, m_VidYear, 365)

        lblCalcVidChat.Text = str1 & str2 & str3 & str4 & str5 & str6
    End Sub


    Private Sub CalcOffers()
        Dim _spinOffers As Integer = spinOffers.Number
        Dim _spinOffers2 As Integer = spinOffers2.Number

        m_OfferDay = (_spinOffers * _spinOffers2) / 7
        m_OfferMonth = m_OfferDay * 30
        m_OfferYear = m_OfferDay * 365

        'lblOfferDay.Text = "&euro;" & m_OfferDay.ToString("0.00")
        'lblOfferMonth.Text = "&euro;" & m_OfferMonth.ToString("0")
        'lblOfferYear.Text = "&euro;" & m_OfferYear.ToString("0")

        lblOfferDay.Text = m_OfferDay.ToString("0.00")
        lblOfferMonth.Text = m_OfferMonth.ToString("0")
        lblOfferYear.Text = m_OfferYear.ToString("0")

        'Dim str1 As String =
        '    "Offers per week = {0}  <br>" & vbCrLf &
        '    "Money earned from Offer = {1} EUR  <br>" & vbCrLf
        'str1 = String.Format(str1, _spinOffers, _spinOffers2)
        Dim str1 As String =
    "Offers per week = {0}  <br>" & vbCrLf &
    "Money earned from Offer = {1} <br>" & vbCrLf
        str1 = String.Format(str1, _spinOffers, _spinOffers2)

        Dim str2 As String = ""

        Dim str3 As String =
            "Calculating offers for a week  <br>" & vbCrLf &
            "Your total money {0} = {1} * {2} (Offers * money per offer)  <br>" & vbCrLf
        str3 = String.Format(str3, (_spinOffers * _spinOffers2), _spinOffers, _spinOffers2)

        Dim str4 As String = ""

        Dim str5 As String = ""

        Dim str6 As String =
            "Euros for a day {0} / {1} = {2} <br>" & vbCrLf &
            "Euros for a month {2} * {4} = {3} <br>" & vbCrLf &
            "Euros for a year {2} * {6} = {5} <br>" & vbCrLf
        str6 = String.Format(str6, (_spinOffers * _spinOffers2), 7, m_OfferDay, m_OfferMonth, 30, m_OfferYear, 365)

        lblCalcOffers.Text = str1 & str2 & str3 & str4 & str5 & str6
    End Sub


    Private Sub CalcTotals()
        Dim _lblTotalDay As Double = m_MessageDayAndRate '+ m_VidDayAndRate + m_OfferDay
        Dim _lblTotalMonth As Double = m_MessageMonth '+ m_VidMonth + m_OfferMonth
        Dim _lblTotalYear As Double = m_MessageYear '+ m_VidYear + m_OfferYear

        'lblTotalDay.Text = "&euro;" & _lblTotalDay.ToString("0.00")
        'lblTotalMonth.Text = "&euro;" & _lblTotalMonth.ToString("0")
        'lblTotalYear.Text = "&euro;" & _lblTotalYear.ToString("0")
        lblTotalDay.Text = _lblTotalDay.ToString("0.00")
        lblTotalMonth.Text = _lblTotalMonth.ToString("0")
        lblTotalYear.Text = _lblTotalYear.ToString("0")


        Dim str1 As String = ""
        Dim str2 As String = ""
        Dim str3 As String = ""

        Dim str4 As String =
            "Euros for a day {1} + {2} + {3} = {0}<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(Messages + VideoChats + Offers) <br><br>" & vbCrLf
        str4 = String.Format(str4, _lblTotalDay, m_MessageDayAndRate, m_VidDayAndRate, m_OfferDay)

        Dim str5 As String =
            "Euros for a month {1} + {2} + {3} = {0} <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(Messages + VideoChats + Offers) <br><br>" & vbCrLf
        str5 = String.Format(str5, _lblTotalMonth, m_MessageMonth, m_VidMonth, m_OfferMonth)

        Dim str6 As String =
            "Euros for a year {1} + {2} + {3} = {0} <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(Messages + VideoChats + Offers) <br><br>" & vbCrLf
        str6 = String.Format(str6, _lblTotalYear, m_MessageYear, m_VidYear, m_OfferYear)

        lblCalcTotal.Text = str1 & str2 & str3 & str4 & str5 & str6
    End Sub

    Private Sub calcMoney()
        CalcChildren()
        CalcMessages()
        CalcVideoChat()
        CalcOffers()
        CalcTotals()
    End Sub



    'Protected Sub spinChildren_NumberChanged(sender As Object, e As EventArgs) Handles spinChildren.NumberChanged
    '    calcMoney()
    'End Sub

    'Protected Sub spinChildren2_NumberChanged(sender As Object, e As EventArgs) Handles spinChildren2.NumberChanged
    '    calcMoney()
    'End Sub

    'Protected Sub spinMessages_NumberChanged(sender As Object, e As EventArgs) Handles spinMessages.NumberChanged
    '    calcMoney()
    'End Sub

    'Protected Sub spinMessages2_NumberChanged(sender As Object, e As EventArgs) Handles spinMessages2.NumberChanged
    '    calcMoney()
    'End Sub

    'Protected Sub spinVidChat_NumberChanged(sender As Object, e As EventArgs) Handles spinVidChat.NumberChanged
    '    calcMoney()
    'End Sub

    'Protected Sub spinVidChat2_NumberChanged(sender As Object, e As EventArgs) Handles spinVidChat2.NumberChanged
    '    calcMoney()
    'End Sub

    'Protected Sub spinOffers_NumberChanged(sender As Object, e As EventArgs) Handles spinOffers.NumberChanged
    '    calcMoney()
    'End Sub

    'Protected Sub spinOffers2_NumberChanged(sender As Object, e As EventArgs) Handles spinOffers2.NumberChanged
    '    calcMoney()
    'End Sub

    Protected Sub lnkRecalc1_Click(sender As Object, e As EventArgs) Handles lnkRecalc1.Click
        calcMoney()
    End Sub

    Protected Sub lnkRecalc2_Click(sender As Object, e As EventArgs) Handles lnkRecalc2.Click
        calcMoney()
    End Sub

End Class
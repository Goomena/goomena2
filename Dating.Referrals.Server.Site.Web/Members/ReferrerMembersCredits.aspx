﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Members/Members2Inner.master"
    CodeBehind="ReferrerMembersCredits.aspx.vb" Inherits="Dating.Referrals.Server.Site.Web.ReferrerMembersCredits" %>

<%@ Register src="../UserControls/WhatIsIt.ascx" tagname="WhatIsIt" tagprefix="uc2" %>
<%@ Register src="../UserControls/ContactControl.ascx" tagname="ContactControl" tagprefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    
            
           <div class="grid_3 top_sidebar" id="sidebar-outter">
                <div class="lfloat">
                    <h2 class="page-name"><asp:Literal ID="lblItemsName" runat="server" /></h2>
                    <h2 class="page-description"><asp:Label ID="lblPageTitle" runat="server" Text="Οι Προμήθειες μου ανά επίπεδο" /></h2>
                </div>
                <div class="page-back-btn">
                    <dx:ASPxHyperLink ID="lnkBack" runat="server" Text="Back" CssClass="btn" Visible="False">
                        </dx:ASPxHyperLink>
                </div>
                <div class="clear">
                </div>
            </div>

                <div class="t_wrap">
                    <div class="send_msg t_to msg referrer">
                        <asp:Panel ID="pnlLoginErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblLoginErr" runat="server" Text=""></asp:Label>
                        </asp:Panel>
                        <asp:Label ID="lblPageDesc" runat="server" Text=""></asp:Label>
                        <br />
                        <br />
                        <div class="clear">
                        </div>


                        <asp:UpdatePanel ID="upAvCredits" runat="server">
                            <ContentTemplate>
    <div style="margin-top:20px;"><asp:Literal ID="lblAvCredits" runat="server" Text="Members having credits: [123456]"></asp:Literal></div>
    <div><asp:Literal ID="lblOnlineMembers" runat="server" Text="Members Online: [123456]"></asp:Literal></div>
                                <br />
                                <dx:ASPxGridView ID="gvAvCredits" runat="server" AutoGenerateColumns="False" DataSourceID="odsAvailableCredits"
                                    Width="100%" KeyFieldName="Id">
                                    <SettingsText EmptyDataRow="Δεν βρέθηκαν συνεργάτες" />
                                    <Columns>
                                        <dx:GridViewDataTextColumn FieldName="Id" ReadOnly="True" Visible="False" VisibleIndex="0">
                                            <EditFormSettings Visible="False" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="CustomerId" Visible="False" VisibleIndex="1">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="LoginName" VisibleIndex="2">
                                            <%--<DataItemTemplate>
                                                <dx:ASPxHyperLink ID="lnk" runat="server" NavigateUrl='<%# "~/Members/Profile.aspx?p=" & Eval("LoginName") %>'
                                                    Text='<%# Eval("LoginName") %>' Width="100%">
                                                </dx:ASPxHyperLink>
                                            </DataItemTemplate>--%>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn FieldName="minValidCreateDate" Visible="False" VisibleIndex="3">
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataDateColumn FieldName="maxValidExpirationDate" Visible="False" VisibleIndex="4">
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataTextColumn FieldName="creditsEarned" Visible="False" VisibleIndex="5">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="creditsUsed" Visible="False" VisibleIndex="6">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="creditsExpired" Visible="False" VisibleIndex="7">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="AvailableCredits" VisibleIndex="8">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="CreditsRecordsCount" Visible="False" VisibleIndex="9">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="creditsUsedBeforePreviousExpiration" Visible="False"
                                            VisibleIndex="10">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="creditsUsedAfterPreviousExpiration" Visible="False"
                                            VisibleIndex="11">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataImageColumn FieldName="IsOnline" Visible="true" 
                                            VisibleIndex="13" Width="30px">
                                            <DataItemTemplate>
                                                <asp:Image ID="imgOn" runat="server" ImageUrl="~/Images/icon_check.png" Visible='<%# Eval("IsOnline") %>' />
                                            </DataItemTemplate>
                                        </dx:GridViewDataImageColumn>
                                        <dx:GridViewDataDateColumn FieldName="LastActivityDateTime" Visible="true" 
                                            VisibleIndex="12" Caption="Last Activity" Width="10%">
                                            <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy HH:mm">
                                            </PropertiesDateEdit>
                                            <cellstyle wrap="False">
                                            </cellstyle>
                                        </dx:GridViewDataDateColumn>
                                    </Columns>
                                    <SettingsPager AlwaysShowPager="True" PageSize="50">
                                    </SettingsPager>
                                    <Settings ShowFooter="True" ShowFilterBar="Auto" ShowFilterRow="false" ShowGroupPanel="false" />
                                    <Paddings Padding="0px" />
                                    <Styles>
                                        <Header HorizontalAlign="Center" VerticalAlign="Middle" BackColor="#FF7A15" Font-Bold="True"
                                            ForeColor="White">
                                            <Paddings Padding="0px" PaddingLeft="5px" PaddingRight="5px" />
                                        </Header>
                                        <Footer BackColor="#FF8b26" HorizontalAlign="Right" VerticalAlign="Middle">
                                        </Footer>
                                        <HeaderPanel>
                                            <Paddings Padding="0px" />
                                        </HeaderPanel>
                                        <PagerBottomPanel BackColor="#FF8b26">
                                        </PagerBottomPanel>
                                    </Styles>
                                </dx:ASPxGridView>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <br />
                    </div>
                </div>
            </div>
        <%--</div>
        <div class="clear">
        </div>
    </div>--%>

	<uc2:WhatIsIt ID="WhatIsIt1" runat="server" />


<asp:ObjectDataSource ID="odsAvailableCredits" runat="server" 
    OldValuesParameterFormatString="original_{0}" SelectMethod="GetCustomerPaidCreditsAdmin" 
    TypeName="Dating.Referrals.Server.Site.Web.ReferrerMembersCredits">
    <SelectParameters>
        <asp:Parameter DefaultValue="0" Name="ReturnMinimum_AvailableCredits" 
            Type="Int32" />
        <asp:Parameter DefaultValue="True" Name="ShowOnline" Type="Boolean" />
        <asp:Parameter DefaultValue="" Name="LastActivityUTCDate" Type="DateTime" />
    </SelectParameters>
</asp:ObjectDataSource>

</asp:Content>

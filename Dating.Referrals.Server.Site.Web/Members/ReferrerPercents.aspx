﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Members/Members2Inner.master" CodeBehind="ReferrerPercents.aspx.vb" Inherits="Dating.Referrals.Server.Site.Web.ReferrerPercents" %>

<%@ Register src="../UserControls/WhatIsIt.ascx" tagname="WhatIsIt" tagprefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    
            
           <div class="grid_3 top_sidebar" id="sidebar-outter">
                <div class="lfloat">
                    <h2 class="page-name"><asp:Literal ID="lblItemsName" runat="server" /></h2>
                    <h2 class="page-description"><asp:Label ID="lblPageTitle" runat="server" Text="Οι Προμήθειες μου ανά επίπεδο" /></h2>
                </div>
                <div class="page-back-btn">
                    <dx:ASPxHyperLink ID="lnkBack" runat="server" Text="Back" CssClass="btn" >
                    </dx:ASPxHyperLink>
                </div>
                <div class="clear">
                </div>
            </div>


                    <div class="t_wrap">
                        <div class="send_msg t_to msg">
                            <asp:Panel ID="pnlLoginErr" runat="server" CssClass="alert alert-danger" Visible="False">
                                <asp:Label ID="lblLoginErr" runat="server" Text=""></asp:Label>
                            </asp:Panel>

                <dx:ASPxLabel ID="lblPageDesc" runat="server" Text="Εδώ μπορείτε να δείτε τα ποσοστά που παίρνετε ανά επίπεδο." EncodeHtml="false">
                </dx:ASPxLabel>
                         <br />
                         <br />
                        <div align="left" style="">
                            <dx:ASPxGridView ID="gvMyPercents" runat="server" AutoGenerateColumns="False" 
                                DataSourceID="odsMyPercents" Width="400px">
                                <TotalSummary>
                                    <dx:ASPxSummaryItem DisplayFormat="Total={0}%" ShowInColumn="Commission %" 
                                        FieldName="CommissionPercent" SummaryType="Sum" />
                                </TotalSummary>
                                <Columns>
                                    <dx:GridViewDataTextColumn Caption="Levels" FieldName="LEVELS" 
                                        ShowInCustomizationForm="True" VisibleIndex="0">
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <CellStyle HorizontalAlign="Right">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Commission %" FieldName="CommissionPercent" 
                                        ShowInCustomizationForm="True" VisibleIndex="1">
                                        <PropertiesTextEdit DisplayFormatString="{0}%">
                                        </PropertiesTextEdit>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <CellStyle HorizontalAlign="Right">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                </Columns>
                                <Settings ShowFooter="True" />
                                <Paddings Padding="0px" />
                                <Styles>
                                    <Header HorizontalAlign="Center" VerticalAlign="Middle" BackColor="#FF7A15" Font-Bold="True" ForeColor="White">
                                        <Paddings Padding="0px" />
                                    </Header>
                                    <Footer BackColor="#FF8b26" HorizontalAlign="Right" VerticalAlign="Middle">
                                    </Footer>
                                    <HeaderPanel>
                                        <Paddings Padding="0px" />
                                    </HeaderPanel>
                                    <PagerBottomPanel BackColor="#FF8b26">
                                    </PagerBottomPanel>
                                </Styles>
                            </dx:ASPxGridView>
                         </div>
                         <asp:ObjectDataSource ID="odsMyPercents" runat="server" 
                             SelectMethod="GetMyPercentsData" TypeName="Dating.Referrals.Server.Site.Web.ReferrerPercents"></asp:ObjectDataSource>
                        </div>
                    </div>
        <%--</div>
		
	
	    </div>
        <div class="clear"></div>
    </div>--%>

	<uc2:WhatIsIt ID="WhatIsIt1" runat="server" />
    
</asp:Content>

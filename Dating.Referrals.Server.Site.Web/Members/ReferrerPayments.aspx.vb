﻿Imports Dating.Server.Core.DLL

Public Class ReferrerPayments
    Inherits ReferrerBasePage


    Dim _pageData As clsPageData
    Protected ReadOnly Property CurrentPageData As clsPageData
        Get
            If (_pageData Is Nothing) Then _pageData = New clsPageData(Context)
            Return _pageData
        End Get
    End Property


    Protected Overrides Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        Try
            If clsCurrentContext.VerifyLogin() = True Then
                Me.MasterPageFile = "~/Members/Members2.Master"
            Else
                clsCurrentContext.ClearSession(Session)
                Response.Redirect(ResolveUrl("~/Login.aspx"), False)
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pnlErr.Visible = False

        Try
            If (Me.IsReferrer()) Then
                ' do nothing
            Else
                Response.Redirect("Referrer.aspx")
            End If

        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        'If (Not Page.IsPostBack) Then

        '    Try

        '        dtFrom.Date = DateTime.UtcNow.AddDays(-30)
        '        dtTo.Date = DateTime.UtcNow

        '        'If (Request.UrlReferrer IsNot Nothing) Then
        '        '    lnkBack.NavigateUrl = Request.UrlReferrer.ToString()
        '        'Else
        '        '    lnkBack.NavigateUrl = "~/Members/"
        '        'End If
        '        LoadLAG()

        '    Catch ex As Exception
        '        WebErrorMessageBox(Me, ex, "Page_Load")
        '    End Try

        'End If
        Try
            If (Not Me.IsPostBack) Then
                LoadLAG()
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try


        Try
            SetDaysRange()
            'If (Me.GetCurrentProfile().ReferrerParentId.HasValue AndAlso Me.GetCurrentProfile().ReferrerParentId > 0)Then
            '    'lblReferrerTitle.Text = "Επισκόπηση συνεργασίας"
            'Else
            '    mvMain.SetActiveView(vwSetReferrerParent)
            'End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
    End Sub


    Protected Sub LoadLAG()
        Try

            lblPageTitle.Text = CurrentPageData.GetCustomString("lblPageTitle")
            lnkBack.Text = CurrentPageData.GetCustomString("lnkBack")
            lblPageDesc.Text = CurrentPageData.GetCustomString("lblPageDesc")
            lblFromDate.Text = CurrentPageData.GetCustomString("lblFromDate")
            lblToDate.Text = CurrentPageData.GetCustomString("lblToDate")
            btnRefresh.Text = CurrentPageData.GetCustomString("btnRefresh")
            gvPayments.SettingsText.EmptyDataRow = CurrentPageData.GetCustomString("gvPayments.EmptyDataRow")


            lblItemsName.Text = Me.CurrentPageData.GetCustomString("CartReferrerPaymentsView")
            'lnkViewDescription.Text = Me.CurrentPageData.GetCustomString("CartReferrerPaymentsWhatIsIt")
            'lnkViewDescription.OnClientClick = OnMoreInfoClickFunc(
            '    lnkViewDescription.OnClientClick,
            '    ResolveUrl("~/Members/InfoWin.aspx?info=referrerpayments"),
            '    Me.CurrentPageData.GetCustomString("CartReferrerPaymentsView"))
            WhatIsIt1.PopupControl.HeaderText = Me.CurrentPageData.GetCustomString("CartReferrerPaymentsView")

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender

        Try
            Dim cPageBasic As Dating.Server.Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub


    Protected Sub btnGo_Click(sender As Object, e As EventArgs) Handles btnRefresh.Click
        gvPayments.DataBind()
    End Sub


    Private Sub SetDaysRange()
        If (Not Page.IsPostBack) Then
            If (SessionVariables.ReferrerCredits IsNot Nothing) Then
                Try
                    dtFrom.Value = SessionVariables.ReferrerCredits.DateFrom
                    dtTo.Value = SessionVariables.ReferrerCredits.DateTo
                Catch ex As Exception
                End Try
            End If

            If (dtFrom.Value Is Nothing OrElse dtTo.Value Is Nothing) Then
                dtFrom.Date = DateTime.UtcNow.AddDays(-clsReferrer.DefaultDaysRange)
                dtTo.Date = DateTime.UtcNow.AddDays(1)
            End If
        End If
    End Sub



    Protected Sub odsPayments_Selecting(sender As Object, e As System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs) Handles odsPayments.Selecting
        e.InputParameters("ReferrerID") = Me.MasterProfileId
    End Sub

    Protected Sub gvPayments_CustomColumnDisplayText(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewColumnDisplayTextEventArgs) Handles gvPayments.CustomColumnDisplayText
        Try
            If e.Column.FieldName = "DebitAmount" Then

                Dim dr As System.Data.DataRow = gvPayments.GetDataRow(e.VisibleRowIndex)
                If (dr IsNot Nothing) Then
                    Dim Currency As String = ""
                    Dim DebitAmount As Double
                    If (Not dr.IsNull("Currency")) Then Currency = dr("Currency")
                    If (Not dr.IsNull("DebitAmount")) Then DebitAmount = Convert.ToDouble(dr("DebitAmount"))

                    e.DisplayText = DebitAmount.ToString("0.00")
                    'If (Currency = "EUR") Then
                    '    e.DisplayText = "&euro;" & DebitAmount.ToString("0.00")
                    'ElseIf (Currency = "USD") Then
                    '    e.DisplayText = "$" & DebitAmount.ToString("0.00")
                    'End If
                End If

            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub






End Class
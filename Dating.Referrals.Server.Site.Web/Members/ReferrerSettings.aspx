﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Members/Members2Inner.master" CodeBehind="ReferrerSettings.aspx.vb" 
Inherits="Dating.Referrals.Server.Site.Web.ReferrerSettings" %>

<%@ Register Assembly="DevExpress.Web.v13.1, Version=13.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>
<%@ Register src="../UserControls/WhatIsIt.ascx" tagname="WhatIsIt" tagprefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
<script type="text/javascript">
    // <![CDATA[

       jQuery(function ($) {
	    $('.pe_box').each(function () {
	        var pe_box = this;

	        $('.collapsibleHeader', pe_box).click(function () {
	            $('.pe_form', pe_box).toggle('fast', function () {
	                // Animation complete.
	                var isVisible = ($('.pe_form', pe_box).css('display') == 'none');
	                if (isVisible) {
	                    $('.togglebutton', pe_box).text("+");
	                    $('.collapsibleHeader', pe_box).attr("title", "<%= globalStrings.GetCustomString("msg_ClickToExpand", GetLag() )  %>");
	                }
	                else {
	                    $('.togglebutton', pe_box).text("-");
	                    $('.collapsibleHeader', pe_box).attr("title", "<%= globalStrings.GetCustomString("msg_ClickToCollapse", GetLag()) %>");
	                }
	            });

	        });
	    });
    	});
    
    // ]]> 
</script>
           
           <div class="grid_3 top_sidebar" id="sidebar-outter">
                <div class="lfloat">
                    <h2 class="page-name"><asp:Literal ID="lblItemsName" runat="server" /></h2>
                    <h2 class="page-description"><asp:Label ID="lblPageTitle" runat="server" Text="Οι Προμήθειες μου ανά επίπεδο" /></h2>
                </div>
                <div class="page-back-btn">
                    <dx:ASPxHyperLink ID="lnkBack" runat="server" Text="Back" CssClass="btn" >
                    </dx:ASPxHyperLink>
                </div>
                <div class="clear">
                </div>
            </div>

                    <div id="pe">
                        <div class="send_msg t_to msg">
                            <asp:Panel ID="pnlErr" runat="server" CssClass="alert alert-danger" 
                                Visible="False">
                                <asp:Label ID="lblErr" runat="server" Text="" />
                            </asp:Panel>


    <asp:Label ID="lblPageDesc" runat="server">Εδώ πρέπει να ρυθμίσετε ορισμένες ιδιότητες του λογαριασμού σας όπως ο τρόπος που θέλετε να πληρώνεστε και άλλα.</asp:Label>
    <br />
    <br />
	<div class="pe_wrapper">
		<div class="pe_box">
            <h2 runat="server" id="h2BasicHeader" class="collapsibleHeader" title=""><asp:HyperLink ID="lnkExpCollBasicInfo" runat="server" NavigateUrl="javascript:void(0);" class="togglebutton">-</asp:HyperLink> <asp:Literal ID="msg_BasicHeader" runat="server">Basic Info Title</asp:Literal></h2>
            <asp:Literal ID="msg_BasicDesc" runat="server">Basic Info Description</asp:Literal>
			<div class="pe_form" runat="server" id="pe_formBasicInfo">

                            <dx:ASPxCallbackPanel ID="cbpnlZip" runat="server"
                                ClientInstanceName="cbpnlZip" 
                                CssFilePath="~/App_Themes/DevEx/{0}/styles.css" 
                    CssPostfix="DevEx" ShowLoadingPanel="False" ShowLoadingPanelImage="False">
                                <ClientSideEvents EndCallback="function(s, e) {
	LoadingPanel.Hide();
}" CallbackError="function(s, e) {
	LoadingPanel.Hide();
}" />
<ClientSideEvents EndCallback="function(s, e) {
	LoadingPanel.Hide();
}" CallbackError="function(s, e) {
	LoadingPanel.Hide();
}"></ClientSideEvents>

                                <LoadingPanelImage Url="~/App_Themes/DevEx/Web/Loading.gif">
                                </LoadingPanelImage>
                                <LoadingPanelStyle ImageSpacing="5px">
                                </LoadingPanelStyle>
                                <PanelCollection>
                                    <dx:PanelContent ID="PanelContent1" runat="server" SupportsDisabledAttribute="True">

                <ul class="defaultStyle">
					<!-- location -->
					<li>
						<asp:Label ID="lblCode" runat="server" Text="Your Code" CssClass="peLabel floatLeft" AssociatedControlID="lblLogin"/>
						<div class="form_right floatLeft">
                            <dx:ASPxLabel ID="lblCodeNum" runat="server" Font-Size="15px" />
						</div>
					</li>
					<li>
                        <asp:Panel ID="pnlLoginErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblLoginErr" runat="server" Text="" /></asp:Panel>
						<asp:Label ID="msg_Login" runat="server" Text="Login Name" CssClass="peLabel floatLeft" AssociatedControlID="lblLogin"/>
						<div class="form_right floatLeft">
                            <dx:ASPxLabel ID="lblLogin" runat="server" Font-Size="15px" />
						</div>
					</li>
					<li>
                        <asp:Panel ID="pnlParentAffErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblParentAffErr" runat="server" Text="" /></asp:Panel>
						<asp:Label ID="msg_ParentAff" runat="server" Text="Parent Name" CssClass="peLabel floatLeft" AssociatedControlID="lblParentAff"/>
						<div class="form_right floatLeft">
                            <dx:ASPxLabel ID="lblParentAff" runat="server" Font-Size="15px" />
						</div>
					</li>
					<li>
                        <asp:Panel ID="pnleMailErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lbleMailErr" runat="server" Text="" /></asp:Panel>
						<asp:Label ID="msg_eMail" runat="server" Text="Email" CssClass="peLabel floatLeft" AssociatedControlID="lbleMail"/>
						<div class="form_right floatLeft">
                            <dx:ASPxLabel ID="lbleMail" runat="server" Font-Size="15px" />
						</div>
					</li>

<%--
                    <li style="text-align:center;"><hr style="width:600px;" align="center" /></li>
					<li>
                        <asp:Panel ID="pnlPasswordErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblPasswordErr" runat="server" Text="" /></asp:Panel>
						<asp:Label ID="msg_Password" runat="server" Text="New Password" CssClass="peLabel floatLeft" AssociatedControlID="txtPassword"/>
						<div class="form_right floatLeft">
                            <dx:ASPxTextBox ID="txtPassword" runat="server" autocomplete="off" Password="true" />
						</div>
					</li>
					<li>
                        <asp:Panel ID="pnlPasswordConfErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblPasswordConfErr" runat="server" Text="" /></asp:Panel>
						<asp:Label ID="msg_PasswordConf" runat="server" Text="New Password Confirm" CssClass="peLabel floatLeft" AssociatedControlID="txtPasswordConf"/>
						<div class="form_right floatLeft">
                            <dx:ASPxTextBox ID="txtPasswordConf" runat="server" autocomplete="off" Password="true" />
						</div>
					</li>
                    <li>

	<div class="form-actions">
        <dx:ASPxButton ID="btnSavePass" runat="server" 
            CssClass="btn btn-normal" Text="Update Password" 
            Native="True" EncodeHtml="False" 
            UseSubmitBehavior="False"  />
	</div>

                    </li>
--%>
					<li style="text-align:center;"><hr style="width:600px;" align="center" /></li>

					<li>
                        <asp:Panel ID="pnlFirstNameErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblFirstNameErr" runat="server" Text="" /></asp:Panel>
						<asp:Label ID="msg_FirstName" runat="server" Text="FirstName" CssClass="peLabel floatLeft" AssociatedControlID="txtFirstName"/>
						<div class="form_right floatLeft">
                            <dx:ASPxTextBox ID="txtFirstName" runat="server" autocomplete="off" />
						</div>
					</li>
					<li>
                        <asp:Panel ID="pnlLastNameErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblLastNameErr" runat="server" Text="" /></asp:Panel>
						<asp:Label ID="msg_LastName" runat="server" Text="Last Name" CssClass="peLabel floatLeft" AssociatedControlID="txtLastName"/>
						<div class="form_right floatLeft">
                            <dx:ASPxTextBox ID="txtLastName" runat="server" autocomplete="off" />
						</div>
					</li>
					<li>
                        <asp:Panel ID="pnlMobileErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblMobileErr" runat="server" Text="" /></asp:Panel>
						<asp:Label ID="msg_Mobile" runat="server" Text="Mobile" CssClass="peLabel floatLeft" AssociatedControlID="txtMobile"/>
						<div class="form_right floatLeft">
                            <dx:ASPxTextBox ID="txtMobile" runat="server" autocomplete="off" />
						</div>
					</li>
					<!-- Country  -->
					<li>
                        <asp:Panel ID="pnlCountryErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblCountryErr" runat="server" Text="Country" />
                        </asp:Panel>
					
                        <asp:Label ID="msg_CountryText" runat="server" Text="" 
                            class="peLabel floatLeft" AssociatedControlID="cbCountry" />
						<div class="form_right floatLeft">
                            <dx:ASPxComboBox ID="cbCountry" runat="server" Width="310px" Font-Size="14px" 
                                Height="18px" EncodeHtml="False" 
                                IncrementalFilteringMode="StartsWith">
                                <ClientSideEvents EndCallback="function(s, e) {
	LoadingPanel.Hide();
}" />
        
<ClientSideEvents EndCallback="function(s, e) {
	LoadingPanel.Hide();
}"></ClientSideEvents>
        
                            </dx:ASPxComboBox>	
						</div>
					</li>
					<!-- region  -->
                    <li>
                        <asp:Label ID="msg_RegionText" runat="server" Text="" 
                            class="peLabel floatLeft" AssociatedControlID="cbRegion" />
						<div class="form_right floatLeft">
                            <dx:ASPxComboBox ID="cbRegion" runat="server" Width="310px" Font-Size="14px" Height="18px"
                                EncodeHtml="False" EnableSynchronization="False" ClientInstanceName="cbRegion"
                                IncrementalFilteringMode="StartsWith" DataSourceID="sdsRegion" TextField="region1"
                                ValueField="region1">
                                <ClientSideEvents SelectedIndexChanged="function(s, e) {
}" EndCallback="function(s, e) {
LoadingPanel.Hide();
}" />
                            </dx:ASPxComboBox>	
                            <asp:SqlDataSource ID="sdsRegion" runat="server" ConnectionString="<%$ ConnectionStrings:AppDBconnectionString %>">
                            </asp:SqlDataSource>
						</div>
					</li>
                    <li>
                        <asp:Label ID="msg_CityText" runat="server" Text="" 
                            class="peLabel floatLeft" AssociatedControlID="cbCity" />
						<div class="form_right floatLeft">
                            <dx:ASPxComboBox ID="cbCity" runat="server" Width="310px" Font-Size="14px" Height="18px"
                                EncodeHtml="False" EnableSynchronization="False" ClientInstanceName="cbCity"
                                IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" DataSourceID="sdsCity"
                                TextField="city" ValueField="city">
                                <ClientSideEvents SelectedIndexChanged="function(s, e) {
}" />
                            </dx:ASPxComboBox>	
                            <asp:SqlDataSource ID="sdsCity" runat="server" ConnectionString="<%$ ConnectionStrings:AppDBconnectionString %>">
                            </asp:SqlDataSource>
			            </div>
					</li>
				</ul>
                                    </dx:PanelContent>
                                </PanelCollection>
                            </dx:ASPxCallbackPanel>
			<div class="clear"></div>
			</div>
		</div>
	</div>


	<div class="padding"></div>
	<div class="pe_wrapper">
		<div class="pe_box">
            <h2 runat="server" id="h2PayoutInfoHead" class="collapsibleHeader" title=""><asp:HyperLink ID="lnkExpCollPayoutInfo" runat="server" NavigateUrl="javascript:void(0);" class="togglebutton">-</asp:HyperLink> <asp:Literal ID="msg_PayoutInfoHead" runat="server">Payout Info Title</asp:Literal></h2>
            <asp:Literal ID="msg_PayoutInfoDescr" runat="server">Payout Info Descr</asp:Literal>
            <div class="pe_form" runat="server" id="pe_formPayoutInfo">
                <asp:UpdatePanel ID="updPayout" runat="server">
                    <ContentTemplate>
                    
                <ul class="defaultStyle">
					<!-- location -->
					<li>
                        <asp:Panel ID="pnlPayout_AFF_PayoutTypeIDErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblPayout_AFF_PayoutTypeIDErr" runat="server" Text="" /></asp:Panel>
						<asp:Label ID="msg_Payout_AFF_PayoutTypeID" runat="server" 
                            Text="Payout Type" CssClass="peLabel floatLeft" 
                            AssociatedControlID="cbPayoutTypeID"/>
						<div class="form_right floatLeft">
<dx:ASPxComboBox ID="cbPayoutTypeID" runat="server" Width="310px" Font-Size="14px" 
                                Height="18px" EncodeHtml="False" 
                                IncrementalFilteringMode="StartsWith" AutoPostBack="True">
                                <ClientSideEvents EndCallback="function(s, e) {
	LoadingPanel.Hide();
}" />
        
                            </dx:ASPxComboBox>
                        </div>
					</li>
					<li ID="liPayout_MinBalance" runat="server">
                        <asp:Panel ID="pnlPayout_MinBalanceErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblPayout_MinBalanceErr" runat="server" Text="" /></asp:Panel>
						<asp:Label ID="msg_Payout_MinBalance" runat="server" 
                            Text="Minimum available balance" CssClass="peLabel floatLeft" 
                            AssociatedControlID="spinMinBalance"/>
						<div class="form_right floatLeft">
						    <dx:ASPxSpinEdit ID="spinMinBalance" runat="server" Height="21px" Number="100" 
                                Width="80px" MaxLength="5" MaxValue="10000" MinValue="50" />
						</div>
						<div class="form_right floatLeft" style="margin-top: 6px;margin-left:5px;">
						    <asp:Label ID="msg_Payout_MinBalanceExplanation" runat="server" 
                            Text="(ex. Minimum 100)"/><%--Text="(ex. Minimum 100 Euro)"--%>
						</div>
					</li>
					<li ID="liPayout_email" runat="server">
                        <asp:Panel ID="pnlPayout_emailErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblPayout_emailErr" runat="server" Text="" /></asp:Panel>
						<asp:Label ID="msg_Payout_email" runat="server" Text="Payout email" 
                            CssClass="peLabel floatLeft" AssociatedControlID="txtPayout_email"/>
						<div class="form_right floatLeft">
                            <dx:ASPxTextBox ID="txtPayout_email" runat="server" />
						</div>
					</li>
					<li ID="liPayout_Taxid" runat="server" visible="false">
                        <asp:Panel ID="pnlPayout_TaxidErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblPayout_TaxidErr" runat="server" Text="" /></asp:Panel>
						<asp:Label ID="msg_Payout_Taxid" runat="server" Text="Payout Taxid" 
                            CssClass="peLabel floatLeft" AssociatedControlID="txtPayout_Taxid" />
						<div class="form_right floatLeft">
                            <dx:ASPxTextBox ID="txtPayout_Taxid" runat="server" />
						</div>
					</li>
					<li ID="liPayout_BeneficiaryName" runat="server">
                        <asp:Panel ID="pnlPayout_BeneficiaryNameErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblPayout_BeneficiaryNameErr" runat="server" Text="" /></asp:Panel>
						<asp:Label ID="msg_Payout_BeneficiaryName" runat="server" 
                            Text="Payout Beneficiary Name" CssClass="peLabel floatLeft" 
                            AssociatedControlID="txtPayout_BeneficiaryName"/>
						<div class="form_right floatLeft">
                            <dx:ASPxTextBox ID="txtPayout_BeneficiaryName" runat="server" autocomplete="off" />
						</div>
					</li>
					<li ID="liPayout_CompanyStreetAddress" runat="server">
                        <asp:Panel ID="pnlPayout_CompanyStreetAddressErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblPayout_CompanyStreetAddressErr" runat="server" Text="" /></asp:Panel>
						<asp:Label ID="msg_Payout_CompanyStreetAddress" runat="server" 
                            Text="Payout Company Street Address" CssClass="peLabel floatLeft" 
                            AssociatedControlID="txtPayout_CompanyStreetAddress"/>
						<div class="form_right floatLeft">
                            <dx:ASPxTextBox ID="txtPayout_CompanyStreetAddress" runat="server" autocomplete="off" />
						</div>
					</li>
                    
					<li ID="liPayout_CompanyCity" runat="server">
                        <asp:Panel ID="pnlPayout_CompanyCityErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblPayout_CompanyCityErr" runat="server" Text="" /></asp:Panel>
						<asp:Label ID="msg_Payout_CompanyCity" runat="server" Text="Payout Company City" 
                            CssClass="peLabel floatLeft" AssociatedControlID="txtPayout_CompanyCity"/>
						<div class="form_right floatLeft">
                            <dx:ASPxTextBox ID="txtPayout_CompanyCity" runat="server" autocomplete="off" />
						</div>
					</li>
                    
					<li ID="liPayout_CompanyPostalCode" runat="server">
                        <asp:Panel ID="pnlPayout_CompanyPostalCodeErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblPayout_CompanyPostalCodeErr" runat="server" Text="" /></asp:Panel>
						<asp:Label ID="msg_Payout_CompanyPostalCode" runat="server" 
                            Text="Payout Company Postal Code" CssClass="peLabel floatLeft" 
                            AssociatedControlID="txtPayout_CompanyPostalCode"/>
						<div class="form_right floatLeft">
                            <dx:ASPxTextBox ID="txtPayout_CompanyPostalCode" runat="server" autocomplete="off" />
						</div>
					</li>
					<li ID="liPayout_BankName" runat="server">
                        <asp:Panel ID="pnlPayout_BankNameErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblPayout_BankNameErr" runat="server" Text="" /></asp:Panel>
						<asp:Label ID="msg_Payout_BankName" runat="server" Text="Payout Bank Name" 
                            CssClass="peLabel floatLeft" AssociatedControlID="txtPayout_BankName"/>
						<div class="form_right floatLeft">
                            <dx:ASPxTextBox ID="txtPayout_BankName" runat="server" autocomplete="off" />
						</div>
					</li>
					<!-- Country  -->
					<li ID="liPayout_BankCountryID" runat="server">
                        <asp:Panel ID="pnlPayout_BankCountryIDErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblPayout_BankCountryIDErr" runat="server" Text="" />
                        </asp:Panel>
					
                        <asp:Label ID="msg_Payout_BankCountryID" runat="server" 
                            Text="Payout Bank Country" class="peLabel floatLeft" 
                            AssociatedControlID="cbBankCountryID"/>
						<div class="form_right floatLeft">
                            <dx:ASPxComboBox ID="cbBankCountryID" runat="server" Width="310px" Font-Size="14px" 
                                Height="18px" EncodeHtml="False" 
                                IncrementalFilteringMode="StartsWith">
                                <ClientSideEvents EndCallback="function(s, e) {
	LoadingPanel.Hide();
}" />
        
                            </dx:ASPxComboBox>	
						</div>
					</li>
					<li ID="liPayout_BankAddress" runat="server">
                        <asp:Panel ID="pnlPayout_BankAddressErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblPayout_BankAddressErr" runat="server" Text="" /></asp:Panel>
						<asp:Label ID="msg_Payout_BankAddress" runat="server" Text="Payout Bank Address" 
                            CssClass="peLabel floatLeft" AssociatedControlID="txtPayout_BankAddress"/>
						<div class="form_right floatLeft">
                            <dx:ASPxTextBox ID="txtPayout_BankAddress" runat="server" autocomplete="off" />
						</div>
					</li>
					<li ID="liPayout_BankAccountNo" runat="server">
                        <asp:Panel ID="pnlPayout_BankAccountNoErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblPayout_BankAccountNoErr" runat="server" Text="" /></asp:Panel>
						<asp:Label ID="msg_Payout_BankAccountNo" runat="server" 
                            Text="Payout Bank Account No" CssClass="peLabel floatLeft" 
                            AssociatedControlID="txtPayout_BankAccountNo"/>
						<div class="form_right floatLeft">
                            <dx:ASPxTextBox ID="txtPayout_BankAccountNo" runat="server" autocomplete="off" />
						</div>
					</li>
					<li ID="liPayout_SWIFTCode" runat="server">
                        <asp:Panel ID="pnlPayout_SWIFTCodeErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblPayout_SWIFTCodeErr" runat="server" Text="" /></asp:Panel>
						<asp:Label ID="msg_Payout_SWIFTCode" runat="server" Text="Payout SWIFT Code" 
                            CssClass="peLabel floatLeft" AssociatedControlID="txtPayout_SWIFTCode"/>
						<div class="form_right floatLeft">
                            <dx:ASPxTextBox ID="txtPayout_SWIFTCode" runat="server" autocomplete="off" />
						</div>
					</li>
					<li ID="liPayout_IBAN" runat="server">
                        <asp:Panel ID="pnlPayout_IBANErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblPayout_IBANErr" runat="server" Text="" /></asp:Panel>
						<asp:Label ID="msg_Payout_IBAN" runat="server" Text="Payout IBAN" 
                            CssClass="peLabel floatLeft" AssociatedControlID="txtPayout_IBAN"/>
						<div class="form_right floatLeft">
                            <dx:ASPxTextBox ID="txtPayout_IBAN" runat="server" autocomplete="off" />
						</div>
					</li>
					<li ID="liPayout_PreferredCurrency" runat="server">
                        <asp:Panel ID="pnlPayout_PreferredCurrencyErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblPayout_PreferredCurrencyErr" runat="server" Text="" /></asp:Panel>
						<asp:Label ID="msg_Payout_PreferredCurrency" runat="server" 
                            Text="Payout Preferred Currency" CssClass="peLabel floatLeft" 
                            AssociatedControlID="cbPreferredCurrency"/>
						<div class="form_right floatLeft">
                            <dx:ASPxComboBox ID="cbPreferredCurrency" runat="server" Width="100px" Font-Size="14px" 
                                Height="18px" EncodeHtml="False" 
                                IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True">
                                <ClientSideEvents EndCallback="function(s, e) {
	LoadingPanel.Hide();
}" />
        
                                <Items>
                                    <dx:ListEditItem Text="EURO" Value="EUR" Selected="true" />
                                    <dx:ListEditItem Text="Dollars" Value="USD" />
                                </Items>
        
                            </dx:ASPxComboBox>
						</div>
					</li>
					<li ID="liPayout_AdditionalNotes" runat="server">
                        <asp:Panel ID="pnlPayout_AdditionalNotesErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblPayout_AdditionalNotesErr" runat="server" Text="" /></asp:Panel>
						<asp:Label ID="msg_Payout_AdditionalNotes" runat="server" 
                            Text="Payout Additional Notes" CssClass="peLabel floatLeft" 
                            AssociatedControlID="txtPayout_AdditionalNotes"/>
						<div class="form_right floatLeft">
                            <asp:TextBox ID="txtPayout_AdditionalNotes" autocomplete="off" runat="server" TextMode="MultiLine" Width="370px" Rows="10"></asp:TextBox>
						</div>
					</li>
					<!-- region  -->
				</ul>
                    </ContentTemplate>
                </asp:UpdatePanel>	
			<div class="clear"></div>

            <div>
                <asp:Label ID="lblBottomNotes" runat="server" Text=""> </asp:Label>
            </div>
			</div>
		</div>
	</div>


	<div class="clearboth padding"></div>
	<div class="form-actions">
        <dx:ASPxButton ID="btnProfileUpd" runat="server" 
            CssClass="btn btn-large btn-primary" Text="Update settings" 
            ValidationGroup="RegisterGroup" Native="True" EncodeHtml="False" 
            UseSubmitBehavior="False"  />
	</div>

             
                        </div>
                    </div>
       <%-- </div>
		
	
	    </div>
        <div class="clear"></div>
    </div>--%>

	<uc2:WhatIsIt ID="WhatIsIt1" runat="server" />
    
</asp:Content>

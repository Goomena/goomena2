﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Members/Members2Inner.master" CodeBehind="Information.aspx.vb" Inherits="Dating.Referrals.Server.Site.Web.Information" %>
<%@ Register src="../UserControls/MemberLeftPanel.ascx" tagname="MemberLeftPanel" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
        <div class="clear">
        </div>
        <div class="container_12" id="tabs-outter">
            <div class="grid_12 top_tabs">
                <div>
                </div>
            </div>
        </div>
        <div class="clear">
        </div>
        <div class="container_12" id="content_top">
        </div>
        <div class="container_12" id="content">
            <div class="grid_3" id="sidebar-outter">
                <uc1:memberleftpanel ID="leftPanel" runat="server" ShowBottomPanel="false" />
            </div>

            <div class="grid_9 body">
                <div class="middle" id="content-outter">
                    <div class="clear"></div>

                    <div class="l_wrap">
            <asp:MultiView ID="mvInformationMain" runat="server" ActiveViewIndex="0">

                <asp:View ID="vwNothing" runat="server"> 
                </asp:View>


                <asp:View ID="vwHaveToProvidePhoto" runat="server">
<div class="m_wrap">
	<div class="clearboth padding"></div>
	<div class="items_none items_hard">
        <asp:Literal ID="msg_HasNoPhotosText" runat="server"/>
		<div class="actions"><ul><li class=""><asp:HyperLink ID="lnk1" runat="server" NavigateUrl="~/Members/Photos.aspx"><asp:Label ID="msg_AddPhotosText" runat="server"/></asp:HyperLink></li></ul></div><div class="clear"></div>
	</div>
</div>
                </asp:View>


                <asp:View ID="vwCreditsRequired" runat="server">
<div class="items_hard">
    <asp:Literal ID="msg_CreditsRequiredText" runat="server"/>
    <p>
        <asp:HyperLink ID="lnkContinueToCredits" runat="server" CssClass="btn lighter" NavigateUrl="~/Members/SelectProduct.aspx">
            <asp:Literal ID="msg_ContinueToCreditsText" runat="server" /> <i class="icon-chevron-right"></i>
        </asp:HyperLink><asp:HyperLink ID="lnkCancelContinueToCredits" runat="server" CssClass="btn lighter">
            <asp:Literal ID="msg_CancelContinueToCreditsText" runat="server" /> <i class="icon-chevron-right"></i>
        </asp:HyperLink></p><div class="clear">
    </div>
</div>
                </asp:View>

                <asp:View ID="vwCreditsCharging" runat="server">
<div class="items_hard">
    <asp:Literal ID="msg_CreditsChargingText" runat="server"/>
    <p>
        <asp:HyperLink ID="lnkAcceptCreditsCharging" runat="server" CssClass="btn lighter"
            NavigateUrl="~/Members/SelectProduct.aspx"><asp:Literal ID="msg_AcceptCreditsChargingText" runat="server" /> <i class="icon-chevron-right"></i>
        </asp:HyperLink><asp:HyperLink ID="lnkCancelCreditsCharging" runat="server" CssClass="btn lighter">
            <asp:Literal ID="msg_CancelCreditsChargingText" runat="server" /> <i class="icon-chevron-right"></i>
        </asp:HyperLink></p><div class="clear"></div>
</div>
                </asp:View>

            </asp:MultiView>
                    </div>
                </div>
				<div class="bottom"></div>
	
			</div>
            <div class="clear">
            </div>
        </div>
</asp:Content>

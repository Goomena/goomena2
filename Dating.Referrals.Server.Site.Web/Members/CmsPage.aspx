﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="Members2.master" CodeBehind="CmsPage.aspx.vb" Inherits="Dating.Referrals.Server.Site.Web.Members_CmsPage" %>

<%@ Register assembly="DevExpress.Web.v13.1, Version=13.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxRoundPanel" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v13.1, Version=13.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxPanel" tagprefix="dx" %>

<%@ Register Src="../UserControls/MemberLeftPanel.ascx" TagName="MemberLeftPanel"
    TagPrefix="uc2" %>
<%@ Register Src="../UserControls/MessageNoPhoto.ascx" TagName="MessageNoPhoto" TagPrefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/v1/css/ref.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        p{margin:0; padding:0; }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <div class="container_12 referrer" id="content">
        <div class="grid_3 top_sidebar" id="sidebar-outter" style="padding-top:20px;">
            <uc2:MemberLeftPanel ID="leftPanel" runat="server" ShowBottomPanel="false" />
        </div>
        <div class="grid_9 body">
            <div id="content-outter" class="middle">
                <div id="filter" class="m_filter t_filter msg_filter">
                    <div class="lfloat">
                        <h2><asp:Label ID="lblHeader" runat="server" /></h2>
                    </div>
                    <div class="rfloat">
                        <dx:ASPxHyperLink ID="lnkBack" runat="server" Text="Back" CssClass="btn" Visible="False">
                        </dx:ASPxHyperLink>
                    </div>
                    <div class="clear">
                    </div>
                </div>
                <div class="t_wrap">
                    <div class="send_msg t_to msg">
                        <div class="clear">
                        </div>

                        <dx:ASPxLabel ID="lbHTMLBody" runat="server" ClientIDMode="AutoID" EncodeHtml="False">
                        </dx:ASPxLabel>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear">
        </div>
    </div>



</asp:Content>

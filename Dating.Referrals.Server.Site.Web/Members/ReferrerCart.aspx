﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Members/Members2Inner.master" 
CodeBehind="ReferrerCart.aspx.vb" Inherits="Dating.Referrals.Server.Site.Web.ReferrerCart" %>

<%@ Register src="../UserControls/WhatIsIt.ascx" tagname="WhatIsIt" tagprefix="uc2" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    

            

           <div class="grid_3 top_sidebar" id="sidebar-outter">
                <div class="lfloat">
                    <h2 class="page-name"><asp:Literal ID="lblItemsName" runat="server" /></h2>
                    <h2 class="page-description"><asp:Label ID="lblPageTitle" runat="server" Text="Οι Προμήθειες μου ανά επίπεδο" /></h2>
                </div>
                <div class="page-back-btn">
                    <dx:ASPxHyperLink ID="lnkBack" runat="server" Text="Back" CssClass="btn" 
                        NavigateUrl="~/Members/Referrer.aspx">
                    </dx:ASPxHyperLink>
                </div>
                <div class="clear">
                </div>
            </div>
                    <div class="t_wrap">
                        <div class="send_msg t_to msg">
                            <asp:Panel ID="pnlErr" runat="server" CssClass="alert alert-danger" 
                                Visible="False" EnableViewState="False">
                                <asp:Label ID="lblErr" runat="server"></asp:Label>
                            </asp:Panel>

        <asp:HiddenField ID="hdfStartDaysBefore" runat="server" Visible="False" 
            ViewStateMode="Disabled" Value="60" />
                <asp:Label ID="lblPageDesc" runat="server" Text=""></asp:Label>
                <br />
                <br />
<table cellpadding="5" cellspacing="1">
            <tr>
                <td>
                    <asp:Label ID="lblFromDate" runat="server" Text="Από Ημ/νία : "></asp:Label></td>
                <td>
                    <dx:ASPxDateEdit ID="dtFrom" runat="server" DisplayFormatString="dd/MM/yyyy" 
                        Width="120px">
                    </dx:ASPxDateEdit>
                </td>
                <td>
                    <asp:Label ID="lblToDate" runat="server" Text="Εως "></asp:Label></td>
                <td><dx:ASPxDateEdit ID="dtTo" runat="server" DisplayFormatString="dd/MM/yyyy" 
                        Width="120px">
                    </dx:ASPxDateEdit></td>
                <%--<td>
                    <asp:Label ID="lblLevel" runat="server" Text="Level: "></asp:Label></td>
                <td>
                    <dx:ASPxComboBox ID="cmbLevel" runat="server" SelectedIndex="0" Width="120px">
                        <Items>
                            <dx:ListEditItem Selected="True" Text="All" Value="-1" />
                            <dx:ListEditItem Text="Level 1" Value="1" />
                            <dx:ListEditItem Text="Level 2" Value="2" />
                            <dx:ListEditItem Text="Level 3" Value="3" />
                            <dx:ListEditItem Text="Level 4" Value="4" />
                            <dx:ListEditItem Text="Level 5" Value="5" />
                        </Items>
                    </dx:ASPxComboBox>
                </td>--%>
                <td>
                    <dx:ASPxButton ID="btnRefresh" runat="server" Text="Refresh">
                    </dx:ASPxButton>
                </td>
            </tr>
        </table>
        <br />
        <br />
        <table width="100%">
            <tr>
                <td colspan="3" style="background-color: #DDD9C3">
                    <asp:Label ID="lblUserName" runat="server" Text="User : [LOGINNAME]" Font-Bold="True"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right"><asp:Label ID="lblParenBy" runat="server" Text="Parent By: "></asp:Label></td>
                <td><asp:Label ID="lblParenByUser" runat="server" Text=""></asp:Label></td>
                <td><asp:Label ID="lblParenByDate" runat="server" Text="Date: [DATE]"></asp:Label></td>
            </tr>
            <tr>
                <td align="right"><asp:Label ID="lblUserDates" runat="server" Text="Dates: "></asp:Label></td>
                <td><asp:Label ID="lblUserDatesCreate" runat="server" Text="Create: [CREATEDATE]"></asp:Label></td>
                <td><asp:Label ID="lblUserDatesLastLogin" runat="server" Text="Last Login: [LASTLOGINDATE]"></asp:Label></td>
            </tr>
            <tr>
                <td colspan="3" style="background-color: #C6D9F1">
                    <asp:Label ID="lblLastDaysActionsTitle" runat="server" Text="Last [LASTDAYS] Days actions:"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right"><asp:Label ID="lblLastLogins" runat="server" Text="Logins: "></asp:Label></td>
                <td colspan="2"><asp:Label ID="lblLastLoginsVal" runat="server" Text=""></asp:Label></td>
            </tr>
            <tr>
                <td align="right"><asp:Label ID="lblLastSendings" runat="server" Text="Send Actions: "></asp:Label></td>
                <td colspan="2"><asp:Label ID="lblLastSendingsVal" runat="server" Text=""></asp:Label></td>
            </tr>
            <tr>
                <td align="right"><asp:Label ID="lblLastReceivings" runat="server" Text="Receive: "></asp:Label></td>
                <td colspan="2"><asp:Label ID="lblLastReceivingsVal" runat="server" Text=""></asp:Label></td>
            </tr>
            <tr>
                <td align="right"><asp:Label ID="lblLastAnswers" runat="server" Text="Answers: "></asp:Label></td>
                <td colspan="2"><asp:Label ID="lblLastAnswersVal" runat="server" Text=""></asp:Label></td>
            </tr>
            <tr>
                <td align="right"><asp:Label ID="lblLastSalesCRD" runat="server" Text="Sales in CRD: "></asp:Label></td>
                <td colspan="2"><asp:Label ID="lblLastSalesCRDVal" runat="server" Text=""></asp:Label></td>
            </tr>
            <tr>
                <td align="right"><asp:Label ID="lblLastReceiveCommissions" runat="server" Text="Receive Commission: "></asp:Label></td>
                <td><asp:Label ID="lblLastReceiveCommissionsAllVal" runat="server" Text="Create: [CREATEDATE]"></asp:Label></td>
                <td><asp:HyperLink ID="lnkViewCommissions" runat="server">View Commissions By Level</asp:HyperLink></td>
            </tr>
        </table>       


                        </div>
                    </div>
        <%--</div>
		
	
	    </div>
        <div class="clear"></div>
    </div>--%>

    <dx:ASPxPopupControl runat="server"
     Height="400px" Width="600px"
     ID="popupReferrerSupplies"
        ClientIDMode="AutoID" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
        ClientInstanceName="popupReferrerSupplies">
        <ContentStyle HorizontalAlign="Left" VerticalAlign="Middle">
            <BackgroundImage ImageUrl="../images/bg.jpg" />
            <Paddings Padding="10px" />
            <Paddings Padding="10px"></Paddings>
            <BackgroundImage ImageUrl="../images/bg.jpg"></BackgroundImage>
        </ContentStyle>
        <CloseButtonStyle>
            <HoverStyle>
                <BackgroundImage ImageUrl="~/Images/close-button1.png" Repeat="NoRepeat" HorizontalPosition="center"
                    VerticalPosition="center"></BackgroundImage>
            </HoverStyle>
            <BackgroundImage ImageUrl="~/Images/close-button2.png" Repeat="NoRepeat" HorizontalPosition="center"
                VerticalPosition="center"></BackgroundImage>
        </CloseButtonStyle>
        <CloseButtonImage Url="~/Images/spacer10.png" Height="17px" Width="17px">
        </CloseButtonImage>
        <SizeGripImage Height="40px" Url="~/Images/resize.png" Width="40px">
        </SizeGripImage>
        <ContentStyle>
        </ContentStyle>
        <HeaderStyle BackColor="#35619F" Font-Bold="True" ForeColor="White">
            <Paddings PaddingBottom="10px" PaddingTop="10px" />
            <Paddings PaddingTop="10px" PaddingBottom="10px"></Paddings>
        </HeaderStyle>
        <ModalBackgroundStyle CssClass="modalPopup">
        </ModalBackgroundStyle>
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server" SupportsDisabledAttribute="True">
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>

	<uc2:WhatIsIt ID="WhatIsIt1" runat="server" />

</asp:Content>

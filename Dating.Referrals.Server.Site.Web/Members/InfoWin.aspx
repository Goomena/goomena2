﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Members/Members2Inner.Master" 
CodeBehind="InfoWin.aspx.vb" Inherits="Dating.Referrals.Server.Site.Web.InfoWin" %>

<%@ Register src="../UserControls/MemberLeftPanel.ascx" tagname="MemberLeftPanel" tagprefix="uc2" %>
<%@ Register src="../UserControls/MessageNoPhoto.ascx" tagname="MessageNoPhoto" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
        <div class="container_12" id="content" style="width:500px;background-image:none;margin-top:10px;margin-bottom:20px;"><%--background-position:left top;style="width:auto;padding-left:10px;"--%>
            <div class="grid_9 body" style="width:auto;">
                <div class="middle" id="content-outter" style="padding-left:15px;" >
                    <asp:Panel ID="pnlErr" runat="server" CssClass="alert alert-danger" 
                        Visible="False" EnableViewState="False">
                        <asp:Label ID="lblErr" runat="server"></asp:Label>
                    </asp:Panel>
                    
        
                    <asp:Label ID="lblInfoTitle" runat="server"></asp:Label><br /><br />
                    <asp:Label ID="lblInfo" runat="server"></asp:Label>
                </div>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
        </div>

</asp:Content>

﻿Imports Dating.Server.Core.DLL

Public Class ReferrerPercents
    Inherits BasePage


    Private affCredits As clsReferrerCredits

    Dim _pageData As clsPageData
    Protected ReadOnly Property CurrentPageData As clsPageData
        Get
            If (_pageData Is Nothing) Then _pageData = New clsPageData(Context)
            Return _pageData
        End Get
    End Property


    Protected Overrides Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        Try
            If clsCurrentContext.VerifyLogin() = True Then
                Me.MasterPageFile = "~/Members/Members2.Master"
            Else
                clsCurrentContext.ClearSession(Session)
                Response.Redirect(ResolveUrl("~/Login.aspx"), False)
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pnlLoginErr.Visible = False

        Try
            If (Me.IsReferrer()) Then
                ' do nothing
            Else
                Response.Redirect("Referrer.aspx")
            End If

        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Try
            If (Not Me.IsPostBack) Then
                LoadLAG()

                If (Request.UrlReferrer IsNot Nothing) Then
                    lnkBack.NavigateUrl = Request.UrlReferrer.ToString()
                Else
                    lnkBack.NavigateUrl = "~/Members/"
                End If
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
    End Sub


    Protected Sub LoadLAG()
        Try

            lblPageTitle.Text = CurrentPageData.GetCustomString("lblPageTitle")
            lnkBack.Text = CurrentPageData.GetCustomString("lnkBack")
            lblPageDesc.Text = CurrentPageData.GetCustomString("lblPageDesc")


            lblItemsName.Text = Me.CurrentPageData.GetCustomString("CartReferrerPercentsView")
            'lnkViewDescription.Text = Me.CurrentPageData.GetCustomString("CartReferrerPercentsWhatIsIt")
            'lnkViewDescription.OnClientClick = OnMoreInfoClickFunc(
            '    lnkViewDescription.OnClientClick,
            '    ResolveUrl("~/Members/InfoWin.aspx?info=referrerpercents"),
            '    Me.CurrentPageData.GetCustomString("CartReferrerPercentsView"))
            WhatIsIt1.PopupControl.HeaderText = Me.CurrentPageData.GetCustomString("CartReferrerPercentsView")

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender

        Try
            Dim cPageBasic As Dating.Server.Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub


    Public Function GetMyPercentsData() As DataTable
        Dim config As New clsReferrerCommissions()
        config.Load(Me.MasterProfileId)

        Using dt As New DataTable()


            dt.Columns.Add("LEVELS")
            dt.Columns.Add("CommissionPercent")
            dt.Columns.Add("RowStyle")

            Dim dr As DataRow = dt.NewRow()
            dr("LEVELS") = "YOU"
            dr("CommissionPercent") = config.REF_CommissionLevel1 * 100
            dt.Rows.Add(dr)

            Dim dr2 As DataRow = dt.NewRow()
            dr2("LEVELS") = "2"
            dr2("CommissionPercent") = config.REF_CommissionLevel2 * 100
            dt.Rows.Add(dr2)

            Dim dr3 As DataRow = dt.NewRow()
            dr3("LEVELS") = "3"
            dr3("CommissionPercent") = config.REF_CommissionLevel3 * 100
            dt.Rows.Add(dr3)

            dr3 = dt.NewRow()
            dr3("LEVELS") = "4"
            dr3("CommissionPercent") = config.REF_CommissionLevel4 * 100
            dt.Rows.Add(dr3)

            dr3 = dt.NewRow()
            dr3("LEVELS") = "5"
            dr3("CommissionPercent") = config.REF_CommissionLevel5 * 100
            dt.Rows.Add(dr3)

            'Dim totals As Integer
            'totals = config.REF_CommissionLevel1
            'totals = totals + config.REF_CommissionLevel2
            'totals = totals + config.REF_CommissionLevel3
            'totals = totals + config.REF_CommissionLevel4
            'totals = totals + config.REF_CommissionLevel5

            'dr3 = dt.NewRow()
            'dr3("LEVELS") = "Totals"
            'dr3("CommissionPercent") = totals & "%"
            'dr3("RowStyle") = "background-color:#C6D9F1;"
            'dt.Rows.Add(dr3)

            Return dt
        End Using
    End Function


End Class
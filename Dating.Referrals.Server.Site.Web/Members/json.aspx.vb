﻿Imports Dating.Server.Core.DLL

Public Class JSON
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    End Sub


    <System.Web.Services.WebMethod()> _
    Public Shared Sub SetOffsetTime(time As String)
        Dim TimeOffset As Double
        If (time.Trim() <> "" AndAlso
            Double.TryParse(time, TimeOffset) AndAlso
            HttpContext.Current.Session("TimeOffset") <> TimeOffset) Then

            HttpContext.Current.Session("TimeOffset") = TimeOffset

        End If
    End Sub



End Class
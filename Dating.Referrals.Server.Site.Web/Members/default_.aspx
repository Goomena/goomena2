﻿<%@ Page Title="" Language="vb" AutoEventWireup="true" MasterPageFile="~/Members/Members2Inner.Master" 
    CodeBehind="default.aspx.vb" Inherits="Dating.Referrals.Server.Site.Web._default1" %>
<%@ Register src="../UserControls/UsersList.ascx" tagname="UsersList" tagprefix="uc2" %>
<%@ Register src="../UserControls/MemberLeftPanel.ascx" tagname="MemberLeftPanel" tagprefix="uc3" %>
<%@ Register assembly="DevExpress.Web.v13.1, Version=13.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxDataView" tagprefix="dx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <div class="clear"></div>
	<div class="container_12" id="content">
        <div class="grid_3 top_sidebar" id="sidebar-outter">
            <div class="left_tab" id="left_tab" runat="server">
                <div class="top_info">
                </div>
                <div class="middle" id="search">
                    <div class="submit">
                        <h3><asp:Literal ID="lblItemsName" runat="server" /></h3>
                        <div class="clear"></div>
                        <asp:LinkButton ID="lnkViewDescription" runat="server" CssClass="more_info2" OnClientClick="OnMoreInfoClick('[contentUrl]', this, '[headerText]');return false;" />
                    </div>
                </div>
                <div class="bottom">
                    &nbsp;</div>
            </div>
		    <uc3:MemberLeftPanel ID="leftPanel" runat="server" ShowBottomPanel="false" />
		</div>
		<div class="grid_9 body">
			<div class="middle" id="content-outter">
				<div class="d_wrap">
                    <div class="m_wrap" id="divUploadPhoto" runat="server">
                        <div class="clearboth padding">
                        </div>
                        <div class="items_none">
                            <asp:Literal ID="msg_UploadPhotoText" runat="server"></asp:Literal>
                            <p><asp:HyperLink ID="lnkUploadPhoto" runat="server" CssClass="btn btn-danger"></asp:HyperLink></p>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div class="m_wrap" id="divAgeIssue" runat="server">
                        <div class="clearboth padding">
                        </div>
                        <div class="items_none">
                            <asp:Literal ID="lblAgeIssueText" runat="server"></asp:Literal>
                            <div class="clear"></div>
                        </div>
                    </div>
                            
                    <div class="testi_hp hidden" id="divTestemonials" runat="server" >
                       <%-- <div class="clearboth padding"></div>   
                        <div class="testi_link <%= Session("LAGID") %>">
                            <dx:ASPxHyperLink ID="lnkTestemonials" runat="server" Text="Read More" NavigateUrl="~/Members/Testemonials.aspx" EncodeHtml="False">
                            </dx:ASPxHyperLink>
                        </div>
                        <div class="testi_blurb">
                            <p style="line-height: 15px !important">
                            <dx:ASPxLabel ID="lblTestemonialsText" runat="server" Text="ASPxLabel" 
                                EncodeHtml="False">
                            </dx:ASPxLabel></p>
                        </div>
                        <div class="testi_links">
                            <div class="clear"></div>
                            <asp:HyperLink ID="lnkTestemonials2" runat="server" class="testi_read" NavigateUrl="~/Members/Testemonials.aspx"></asp:HyperLink></div>
                        <div class="clear"></div>--%>
                    </div>
                            
                    <%--<div class="testi_hp">
    <div class="clearboth"></div><!--big_padding-->
                    </div>--%>
	<div class="d_block">
	<h3><asp:Literal ID="lblWelcome" runat="server" /></h3>
    <div style="padding:5px 5px 0;">
        <dx:ASPxLabel ID="lblDashboardTop1" runat="server" Text="lblDashboardTop1" 
            EncodeHtml="False">
        </dx:ASPxLabel>
    </div>
    <div class="d_announce" id="divLinks" runat="server" style="padding-top:0px;">
        <ul class="info lfloat">
            <li>
                <div class="buttonWrap"></div>
                <dx:ASPxHyperLink ID="lnkAddPhotos" runat="server" Text="Add photo" 
                    NavigateUrl="~/Members/Photos.aspx" EnableDefaultAppearance="false" 
                    EnableTheming="false" EncodeHtml="False"  CssClass="lnkEditPhotos" />
            </li>
            <li>
                <div class="buttonWrap"></div>
                <dx:ASPxHyperLink ID="lnkWhoViewedMe" runat="server" Text="Who Viewed Me" 
                    NavigateUrl="~/Members/MyLists.aspx?vw=whoviewedme" 
                    EnableDefaultAppearance="false" EnableTheming="false" EncodeHtml="False"  CssClass="lnkWhoViewedMe"  />
            </li>
            <li>
                <div class="buttonWrap"></div>
                <dx:ASPxHyperLink ID="lnkWhoFavoritedMe" runat="server" Text="Who Favorited Me" 
                    NavigateUrl="~/Members/MyLists.aspx?vw=whofavoritedme" 
                    EnableDefaultAppearance="false" EnableTheming="false" EncodeHtml="False"  CssClass="lnkWhoFavoritedMe"  />
            </li>
        </ul>

        <ul class="info lfloat" style="margin-left:20px;">
            <li>
                <div class="buttonWrap"></div>
                <dx:ASPxHyperLink ID="lnkMyFavoriteList" runat="server" Text="My Favorite List" 
                    NavigateUrl="~/Members/MyLists.aspx?vw=myfavoritelist" 
                    EnableDefaultAppearance="false" EnableTheming="false" EncodeHtml="False"  CssClass="lnkMyFavoriteList"  />
            </li>
            <li>
                <div class="buttonWrap"></div>
                <dx:ASPxHyperLink ID="lnkMyBlockedList" runat="server" Text="My Blocked List" 
                    NavigateUrl="~/Members/MyLists.aspx?vw=myblockedlist" 
                    EnableDefaultAppearance="false" EnableTheming="false" EncodeHtml="False"  CssClass="lnkMyBlockedList"  />
            </li>
            <li>
                <div class="buttonWrap"></div>
                <dx:ASPxHyperLink ID="lnkMyViewedList" runat="server" Text="My Viewed List" 
                    NavigateUrl="~/Members/MyLists.aspx?vw=myviewedlist" 
                    EnableDefaultAppearance="false" EnableTheming="false" EncodeHtml="False"  CssClass="lnkMyViewedList"  />
            </li>
        </ul>

        <ul class="info lfloat" style="margin-left:20px;">
            <li>
                <div class="buttonWrap"></div>
                <dx:ASPxHyperLink ID="btnSearch" runat="server" 
                    Text="Search our members" 
                    NavigateUrl="~/Members/Search.aspx" 
                    EnableDefaultAppearance="false" EnableTheming="false" 
                    EncodeHtml="False"  CssClass="lnkSearch"  />
            </li>
            <li>
                <div class="buttonWrap"></div>
                <dx:ASPxHyperLink ID="lnkNotifications" runat="server" Text="Autonotifications" 
                    NavigateUrl="~/Members/settings.aspx?vw=notifications" EnableDefaultAppearance="false" 
                    EnableTheming="false" EncodeHtml="False"  CssClass="lnkNotifications" />
            </li>
            <li ID="liHowWorksMale" runat="server" visible="false">
                <div class="buttonWrap"></div>
                <dx:ASPxHyperLink ID="lnkHowWorksMale" runat="server" Text="How it Works" 
                    NavigateUrl="~/cmspage.aspx?pageid=185&title=HowItWorksman" EnableDefaultAppearance="false" 
                    EnableTheming="false" EncodeHtml="False"  CssClass="lnkHowWorks" />
            </li>
            <li ID="liHowWorksFemale" runat="server" visible="false">
                <div class="buttonWrap"></div>
                <dx:ASPxHyperLink ID="lnkHowWorksFemale" runat="server" Text="How it Works" 
                    NavigateUrl="~/cmspage.aspx?pageid=186&title=HowItWorksWoman" EnableDefaultAppearance="false" 
                    EnableTheming="false" EncodeHtml="False"  CssClass="lnkHowWorks" />
            </li>
        </ul>

        <%--<ul class="info rfloat" style="margin-right:20px;">
            <li>
                <dx:ASPxButton ID="btnSearch" runat="server"
                    CssClass="center" EncodeHtml="False"
                    CssFilePath="~/App_Themes/SoftOrange/{0}/styles.css" 
                    CssPostfix="SoftOrange" Font-Bold="True" 
                    SpriteCssFilePath="~/App_Themes/SoftOrange/{0}/sprite.css" Text="Search our members" 
                    Font-Size="12px" >
                    <Paddings Padding="0px"></Paddings>
                    <Border BorderWidth="1px"></Border>
                    <ClientSideEvents Click="function(s, e) {
    window.location.href='Search.aspx';
    e.processOnServer=false;
    LoadingPanel.Show();
}" />
                </dx:ASPxButton>
            </li>
        </ul>--%>

        <div class="clear">
        </div>
        <div style="padding:0 5px 5px;">
            <dx:ASPxLabel ID="lblDashboardTop2" runat="server" Text="lblDashboardTop2" 
                EncodeHtml="False">
            </dx:ASPxLabel>
	    </div>
	</div>
	<div class="d_announce" id="divImportantAnnouncement" runat="server" visible="false">
		<div class="grid_4 alpha" style="width:auto;padding:0 0 20px 10px;">
            <dx:ASPxLabel ID="lblImportantAnnouncement" runat="server" Text="" 
                EncodeHtml="False">
            </dx:ASPxLabel>
			<div class="clearboth padding"></div>
		</div>
		<%--<div class="d_right">
		</div>--%>
	</div>


    <div class="d_block">
	    <h3><asp:HyperLink ID="lnkMembersNear" runat="server" NavigateUrl="~/Members/Search.aspx?vw=NEAREST"></asp:HyperLink>
            <span><asp:HyperLink ID="lnkMembersNearAll" runat="server" NavigateUrl="~/Members/Search.aspx?vw=NEAREST"></asp:HyperLink></span></h3>
        <div class="m_wrap" id="divUpdateProfileData" runat="server" >
            <div class="clearboth padding">
            </div>
            <div class="items_none">
                <asp:Literal ID="lblUpdateProfileData" runat="server"></asp:Literal>
                <p><asp:HyperLink ID="lnkEditProfile" runat="server" CssClass="btn btn-danger" NavigateUrl="~/Members/Profile.aspx?do=edit"></asp:HyperLink></p>
                <div class="clear"></div>
            </div>
        </div>
	    <div class="d_winks_profiles">
        <asp:ListView ID="lvNearest" runat="server">
            <ItemTemplate>
            <div class="d_big_photo">
                <dx:ASPxHyperLink ID="lnkPhoto" runat="server" 
                    EnableDefaultAppearance="False" EnableTheming="False" EncodeHtml="false" 
                    ImageUrl='<%# Dating.Referrals.Server.Site.Web.GeneralFunctions.GetImage(Eval("CustomerID").Tostring(), Eval("FileName").Tostring(), Eval("GenderId").Tostring(), False) %>' 
                    NavigateUrl='<%# System.Web.VirtualPathUtility.ToAbsolute("~/Members/Profile.aspx?p=") &  Eval("LoginName") %>'>
                </dx:ASPxHyperLink>
                <div class="text_content">
                    <dx:ASPxHyperLink ID="lnkLogin" runat="server" Text='<%# Eval("LoginName") %>' NavigateUrl='<%# System.Web.VirtualPathUtility.ToAbsolute("~/Members/Profile.aspx?p=") &  Eval("LoginName") %>'>
                    </dx:ASPxHyperLink>
                </div>
            </div>
            </ItemTemplate>
        </asp:ListView>

		    <div class="clear"></div>
	    </div>
    </div>

    <div class="d_block" id="pnlProfilesYouHaveViewed" runat="server">
	    <h3><asp:HyperLink ID="lnkProfilesYouHaveViewed" runat="server" NavigateUrl="~/Members/MyLists.aspx?vw=myviewedlist"></asp:HyperLink>
            <span><asp:HyperLink ID="lnkAllProfilesYouHaveViewed" runat="server" NavigateUrl="~/Members/MyLists.aspx?vw=myviewedlist"></asp:HyperLink></span></h3>
	    <div class="d_winks_profiles">

        <asp:ListView ID="lvWhomIViewed" runat="server" DataSourceID="sdsWhomIViewed">
            <ItemTemplate>
            <div class="d_big_photo">
                <dx:ASPxHyperLink ID="lnkPhoto" runat="server" 
                    EnableDefaultAppearance="False" EnableTheming="False" EncodeHtml="false" 
                    ImageUrl='<%# Dating.Referrals.Server.Site.Web.GeneralFunctions.GetImage(Eval("CustomerID").Tostring(), Eval("FileName").Tostring(), Eval("GenderId").Tostring(), False) %>' 
                    NavigateUrl='<%# System.Web.VirtualPathUtility.ToAbsolute("~/Members/Profile.aspx?p=") &  Eval("LoginName") %>'>
                </dx:ASPxHyperLink>
                <div class="text_content">
                    <dx:ASPxHyperLink ID="lnkLogin" runat="server" Text='<%# Eval("LoginName") %>' NavigateUrl='<%# System.Web.VirtualPathUtility.ToAbsolute("~/Members/Profile.aspx?p=") &  Eval("LoginName") %>'>
                    </dx:ASPxHyperLink>
                </div>
            </div>
            </ItemTemplate>
        </asp:ListView>
		<asp:SqlDataSource ID="sdsWhomIViewed" runat="server" 
            ConnectionString="<%$ ConnectionStrings:AppDBconnectionString %>" SelectCommand="
SELECT top(12) pr.ProfileID AS CustomerID, pr.LoginName, pr.GenderId, ISNULL(ph.FileName, '') AS FileName 
FROM EUS_Profiles AS pr 
		with (nolock)
INNER JOIN EUS_ProfilesViewed AS vw ON pr.ProfileID = vw.ToProfileID 
LEFT OUTER JOIN EUS_CustomerPhotos AS ph ON pr.ProfileID = ph.CustomerID  
AND pr.DefPhotoID = ph.CustomerPhotosID 
--(ph.IsDefault = 1) AND (ph.HasAproved = 1) AND ISNULL(ph.IsDeleted,0) = 0
WHERE (pr.ProfileID &lt;&gt; 1) 
AND (pr.Status = 4)
AND vw.FromProfileid=@CurrentProfileId
AND NOT EXISTS(
	select * 
	from EUS_ProfilesBlocked bl 
	where (bl.FromProfileID =pr.ProfileID and bl.ToProfileID = @CurrentProfileId) or
		(bl.FromProfileID =@CurrentProfileId and bl.ToProfileID = pr.ProfileID)
)
Order by ph.IsDefault desc, NEWID(), vw.DateTimeToCreate desc
">
            <SelectParameters>
                <asp:SessionParameter DefaultValue="0" Name="CurrentProfileId" 
                    SessionField="ProfileId" />
            </SelectParameters>
        </asp:SqlDataSource>


		    <div class="clear"></div>
	    </div>
    </div>

    <div class="d_announce" style="border-bottom:none;">
		<div class="d_bottom" style="padding-top:10px;">
            <dx:ASPxLabel ID="lblImportantAdvice" runat="server" Text="" 
                EncodeHtml="False">
            </dx:ASPxLabel>
		</div>
		<div class="clear"></div>
	</div>


</div>
		
		
<div class="clearboth big_padding"></div>

	

		
	
		
<%--
<div class="d_block">
	<h3><asp:HyperLink ID="lnkMembersNewest" runat="server" NavigateUrl="~/Members/Search.aspx?vw=NEWEST"></asp:HyperLink>
        <span><asp:HyperLink ID="lnkMembersNewestAll" runat="server" NavigateUrl="~/Members/Search.aspx?vw=NEWEST"></asp:HyperLink></span></h3>
	<div class="d_winks_profiles">
		<uc2:UsersList ID="newestMembers" runat="server" UsersListView="MembersDefaultNewest" />
		<div class="clear"></div>
	</div>
</div>

<div class="d_block">
	<h3><asp:HyperLink ID="lnkMembersNear" runat="server" NavigateUrl="~/Members/Search.aspx?vw=NEAREST"></asp:HyperLink>
        <span><asp:HyperLink ID="lnkMembersNearAll" runat="server" NavigateUrl="~/Members/Search.aspx?vw=NEAREST"></asp:HyperLink></span></h3>
	<div class="d_winks_profiles">
		<uc2:UsersList ID="moreNearMembers" runat="server" UsersListView="MembersDefaultMoreNear"  />
		<div class="clear"></div>
	</div>
</div>
--%>

</div>

			<div class="clear"></div>
			</div>
			<div class="bottom"></div>
	
		</div>
		<div class="clear"></div>
	</div>
<dx:ASPxPopupControl runat="server"
        Height="550px" Width="650px"
        ID="popupWhatIs"
        ClientInstanceName="popupWhatIs" 
        ClientIDMode="AutoID" AllowDragging="True" PopupVerticalAlign="Below" 
            PopupHorizontalAlign="WindowCenter" PopupElementID="lnkViewDescription" 
            Modal="True" AllowResize="True"><ContentStyle HorizontalAlign="Left" VerticalAlign="Middle">
            <BackgroundImage ImageUrl="../images/bg.jpg" />
            <Paddings Padding="10px" />
            <Paddings Padding="10px"></Paddings>
            <BackgroundImage ImageUrl="../images/bg.jpg"></BackgroundImage>
        </ContentStyle>
        <CloseButtonStyle>
            <HoverStyle>
                <BackgroundImage ImageUrl="~/Images/close-button1.png" Repeat="NoRepeat" HorizontalPosition="center"
                    VerticalPosition="center"></BackgroundImage>
            </HoverStyle>
            <BackgroundImage ImageUrl="~/Images/close-button2.png" Repeat="NoRepeat" HorizontalPosition="center"
                VerticalPosition="center"></BackgroundImage>
        </CloseButtonStyle>
        <CloseButtonImage Url="~/Images/spacer10.png" Height="17px" Width="17px">
        </CloseButtonImage>
        <SizeGripImage Height="40px" Url="~/Images/resize.png" Width="40px">
        </SizeGripImage>
        <HeaderStyle BackColor="#35619F" Font-Bold="True" ForeColor="White">
            <Paddings PaddingBottom="10px" PaddingTop="10px" />
            <Paddings PaddingTop="10px" PaddingBottom="10px"></Paddings>
        </HeaderStyle>
        <%--<ModalBackgroundStyle CssClass="modalPopup">
        </ModalBackgroundStyle>--%>
<ModalBackgroundStyle BackColor="Black" Opacity="70"></ModalBackgroundStyle>        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server" SupportsDisabledAttribute="True">
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
    

    <script type="text/javascript">
        function OnMoreInfoClick(contentUrl, obj, headerText) {
            if (headerText != null) popupWhatIs.SetHeaderText(headerText);
            if (obj != null) popupWhatIs.SetPopupElementID(obj.id);
            popupWhatIs.SetContentUrl(contentUrl);
            popupWhatIs.Show();
        }
    </script>

</asp:Content>



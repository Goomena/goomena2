﻿Imports Dating.Server.Core.DLL

Public Class ReferrerFamilyTree
    Inherits ReferrerBasePage


    Dim _pageData As clsPageData
    Protected ReadOnly Property CurrentPageData As clsPageData
        Get
            If (_pageData Is Nothing) Then _pageData = New clsPageData(Context)
            Return _pageData
        End Get
    End Property



    Protected Overrides Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        Try
            If clsCurrentContext.VerifyLogin() = True Then
                Me.MasterPageFile = "~/Members/Members2.Master"
            Else
                clsCurrentContext.ClearSession(Session)
                Response.Redirect(ResolveUrl("~/Login.aspx"), False)
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pnlErr.Visible = False

        Try
            If (Me.IsReferrer()) Then
                ' do nothing
            Else
                Response.Redirect("Referrer.aspx")
            End If

        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try


        'If (Not Page.IsPostBack) Then

        '    'Try


        '    '    GetReferrerCreditsObject()

        '    'Catch ex As Exception
        '    '    WebErrorMessageBox(Me, ex, "Page_Load")
        '    'End Try

        '    LoadLAG()
        'End If

        Try
            If (Not Me.IsPostBack) Then
                LoadLAG()
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try


        Try
            SetReferrerUI()
            'If (Me.GetCurrentProfile().ReferrerParentId.HasValue AndAlso Me.GetCurrentProfile().ReferrerParentId > 0)Then
            '    'lblReferrerTitle.Text = "Επισκόπηση συνεργασίας"
            'Else
            '    mvMain.SetActiveView(vwSetReferrerParent)
            'End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try


    End Sub



    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
        SetReferrerUI()
    End Sub


    Protected Sub LoadLAG()
        Try

            lblPageTitle.Text = CurrentPageData.GetCustomString("lblPageTitle")
            lnkBack.Text = CurrentPageData.GetCustomString("lnkBack")
            lblPageDesc.Text = CurrentPageData.GetCustomString("lblPageDesc")


            lblFromDate.Text = CurrentPageData.GetCustomString("lblFromDate")
            lblToDate.Text = CurrentPageData.GetCustomString("lblToDate")
            btnRefresh.Text = CurrentPageData.GetCustomString("btnRefresh")


            lblItemsName.Text = Me.CurrentPageData.GetCustomString("CartReferrerFamilyTreeView")
            'lnkViewDescription.Text = Me.CurrentPageData.GetCustomString("CartReferrerFamilyTreeWhatIsIt")
            'lnkViewDescription.OnClientClick = OnMoreInfoClickFunc(
            '    lnkViewDescription.OnClientClick,
            '    ResolveUrl("~/Members/InfoWin.aspx?info=referrerfamilytree"),
            '    Me.CurrentPageData.GetCustomString("CartReferrerFamilyTreeView"))
            WhatIsIt1.PopupControl.HeaderText = Me.CurrentPageData.GetCustomString("CartReferrerFamilyTreeView")

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender

        Try
            Dim cPageBasic As Dating.Server.Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub



    Private Sub SetReferrerUI()
        SetDaysRange()
        GetReferrerCreditsObject(dtFrom.Date, dtTo.Date)
    End Sub




    Private Sub SetDaysRange()
        If (Not Page.IsPostBack) Then
            If (SessionVariables.ReferrerCredits IsNot Nothing) Then
                Try
                    dtFrom.Value = SessionVariables.ReferrerCredits.DateFrom
                    dtTo.Value = SessionVariables.ReferrerCredits.DateTo
                Catch ex As Exception
                End Try
            End If

            If (dtFrom.Value Is Nothing OrElse dtTo.Value Is Nothing) Then
                dtFrom.Date = DateTime.UtcNow.AddDays(-clsReferrer.DefaultDaysRange)
                dtTo.Date = DateTime.UtcNow.AddDays(1)
            End If
        End If
    End Sub


    Public Function GetData(dateFrom As DateTime?, dateTo As DateTime?, affCredits1 As clsReferrerCredits) As DataTable
        Dim resuts As DataTable = Nothing
        Try
            resuts = affCredits1.Datatable
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "GetData")
        End Try
        Return resuts
    End Function


    Protected Sub trlReferrers_DataBound(sender As Object, e As EventArgs) Handles trlReferrers.DataBound
        If (trlReferrers.Nodes.Count > 0) Then
            trlReferrers.Nodes(0).Expanded = True
        End If
    End Sub


    Protected Sub btnRefresh_Click(sender As Object, e As EventArgs) Handles btnRefresh.Click
        Try

            SetReferrerUI()

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "btnRefresh")
        End Try
    End Sub


    Protected Sub odsTree_Selecting(sender As Object, e As System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs) Handles odsTree.Selecting
        'Dim _dtFrom As DateTime = dtFrom.Date.Date
        'Dim _dtTo As DateTime = dtTo.Date.Date
        e.InputParameters("affCredits1") = Me.GetReferrerCreditsObject(dtFrom.Date, dtTo.Date)
    End Sub


End Class



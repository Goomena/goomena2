﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class ReferrerPayments

    '''<summary>
    '''lblItemsName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblItemsName As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''lblPageTitle control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPageTitle As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lnkBack control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkBack As Global.DevExpress.Web.ASPxEditors.ASPxHyperLink

    '''<summary>
    '''pnlErr control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlErr As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''lblErr control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblErr As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblPageDesc control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPageDesc As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblFromDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblFromDate As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''dtFrom control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents dtFrom As Global.DevExpress.Web.ASPxEditors.ASPxDateEdit

    '''<summary>
    '''lblToDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblToDate As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''dtTo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents dtTo As Global.DevExpress.Web.ASPxEditors.ASPxDateEdit

    '''<summary>
    '''btnRefresh control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnRefresh As Global.DevExpress.Web.ASPxEditors.ASPxButton

    '''<summary>
    '''gvPayments control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents gvPayments As Global.DevExpress.Web.ASPxGridView.ASPxGridView

    '''<summary>
    '''odsPayments control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents odsPayments As Global.System.Web.UI.WebControls.ObjectDataSource

    '''<summary>
    '''WhatIsIt1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents WhatIsIt1 As Global.Dating.Referrals.Server.Site.Web.WhatIsIt
End Class

﻿Imports Dating.Server.Core.DLL

Public Class ReferrerStatistics
    Inherits ReferrerBasePage


    Dim _pageData As clsPageData
    Protected ReadOnly Property CurrentPageData As clsPageData
        Get
            If (_pageData Is Nothing) Then _pageData = New clsPageData(Context)
            Return _pageData
        End Get
    End Property


    Protected Overrides Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        Try
            If clsCurrentContext.VerifyLogin() = True Then
                Me.MasterPageFile = "~/Members/Members2.Master"
            Else
                clsCurrentContext.ClearSession(Session)
                Response.Redirect(ResolveUrl("~/Login.aspx"), False)
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pnlErr.Visible = False

        Try
            If (Me.IsReferrer()) Then
                ' do nothing
            Else
                Response.Redirect("Referrer.aspx")
            End If

        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        'If (Not Page.IsPostBack) Then

        '    Try

        '        dtFrom.Date = DateTime.UtcNow.AddDays(-30)
        '        dtTo.Date = DateTime.UtcNow

        '        'If (Request.UrlReferrer IsNot Nothing) Then
        '        '    lnkBack.NavigateUrl = Request.UrlReferrer.ToString()
        '        'Else
        '        '    lnkBack.NavigateUrl = "~/Members/"
        '        'End If


        '        GetReferrerCreditsObject()
        '        LoadLAG()
        '        SetSeriesVisibility()

        '    Catch ex As Exception
        '        WebErrorMessageBox(Me, ex, "Page_Load")
        '    End Try

        'End If

        Try
            If (Not Me.IsPostBack) Then
                LoadLAG()
            End If
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try


        Try
            SetReferrerUI()
            'If (Me.GetCurrentProfile().ReferrerParentId.HasValue AndAlso Me.GetCurrentProfile().ReferrerParentId > 0)Then
            '    'lblReferrerTitle.Text = "Επισκόπηση συνεργασίας"
            'Else
            '    mvMain.SetActiveView(vwSetReferrerParent)
            'End If
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
        SetReferrerUI()
    End Sub


    Protected Sub LoadLAG()
        Try

            lblPageTitle.Text = CurrentPageData.GetCustomString("lblPageTitle")
            lnkBack.Text = CurrentPageData.GetCustomString("lnkBack")
            lblPageDesc.Text = CurrentPageData.GetCustomString("lblPageDesc")
            lblFromDate.Text = CurrentPageData.GetCustomString("lblFromDate")
            lblToDate.Text = CurrentPageData.GetCustomString("lblToDate")
            btnRefresh.Text = CurrentPageData.GetCustomString("btnRefresh")
            'lblSeries.Text = CurrentPageData.GetCustomString("lblSeries")


            lblItemsName.Text = Me.CurrentPageData.GetCustomString("CartReferrerStatisticsView")
            'lnkViewDescription.Text = Me.CurrentPageData.GetCustomString("CartReferrerStatisticsWhatIsIt")
            'lnkViewDescription.OnClientClick = OnMoreInfoClickFunc(
            '    lnkViewDescription.OnClientClick,
            '    ResolveUrl("~/Members/InfoWin.aspx?info=referrerstatistics"),
            '    Me.CurrentPageData.GetCustomString("CartReferrerStatisticsView"))
            WhatIsIt1.PopupControl.HeaderText = Me.CurrentPageData.GetCustomString("CartReferrerStatisticsView")

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender

        Try
            Dim cPageBasic As Dating.Server.Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub


    Private Sub SetReferrerUI()
        SetDaysRange()
        GetReferrerCreditsObject(dtFrom.Date, dtTo.Date)
        'SetSeriesVisibility()
    End Sub




    Private Sub SetDaysRange()
        If (Not Page.IsPostBack) Then
            If (SessionVariables.ReferrerCredits IsNot Nothing) Then
                Try
                    dtFrom.Value = SessionVariables.ReferrerCredits.DateFrom
                    dtTo.Value = SessionVariables.ReferrerCredits.DateTo
                Catch ex As Exception
                End Try
            End If

            If (dtFrom.Value Is Nothing OrElse dtTo.Value Is Nothing) Then
                dtFrom.Date = DateTime.UtcNow.AddDays(-clsReferrer.DefaultDaysRange)
                dtTo.Date = DateTime.UtcNow.AddDays(1)
            End If
        End If
    End Sub

  
    'Private Sub SetSeriesVisibility()
    '    wccStatistics.Series("Money EUR").Visible = False
    '    wccStatistics.Series("Commission CRD").Visible = False

    '    If (cblSeries.SelectedValues.Contains("Money EUR")) Then wccStatistics.Series("Money EUR").Visible = True
    '    If (cblSeries.SelectedValues.Contains("Commission CRD")) Then wccStatistics.Series("Commission CRD").Visible = True
    'End Sub


    Private Function IsAnySerieSelected() As Boolean
        Return (wccStatistics.Series("Money").Visible OrElse wccStatistics.Series("Commission CRD").Visible)
        'Return (wccStatistics.Series("Money EUR").Visible OrElse wccStatistics.Series("Commission CRD").Visible)
    End Function






    Protected Sub btnRefresh_Click(sender As Object, e As EventArgs) Handles btnRefresh.Click
        Try

            If (Not IsAnySerieSelected()) Then
                ShowError(CurrentPageData.GetCustomString("Err_SelectSeries")) '"Please select one or more series")
            Else
                wccStatistics.DataBind()
                'SetSeriesVisibility()
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "btnGo")
        End Try
    End Sub





    Private Sub ShowError(msg As String)
        pnlErr.CssClass = "alert alert-danger"
        pnlErr.Visible = True
        lblErr.Text = msg
    End Sub

    Private Sub ShowSuccess(msg As String)
        pnlErr.CssClass = "alert alert-success"
        pnlErr.Visible = True
        lblErr.Text = msg
    End Sub



    Protected Sub odsStatistics_Selecting(sender As Object, e As System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs) Handles odsStatistics.Selecting
        e.InputParameters("affCredits1") = MyBase.GetReferrerCreditsObject(dtFrom.Date, dtTo.Date)
    End Sub
End Class
﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Members/Members2Inner.master"
    CodeBehind="ReferrerEstimate.aspx.vb" Inherits="Dating.Referrals.Server.Site.Web.ReferrerEstimate2" %>

<%@ Register src="../UserControls/WhatIsIt.ascx" tagname="WhatIsIt" tagprefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        p.MsoNormal, div.MsoNormal
        {
            font-size: 11.0pt;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    
           <div class="grid_3 top_sidebar" id="sidebar-outter">
                <div class="lfloat">
                    <h2 class="page-name"><asp:Literal ID="lblItemsName" runat="server" /></h2>
                    <h2 class="page-description"><asp:Label ID="lblPageTitle" runat="server" Text="Οι Προμήθειες μου ανά επίπεδο" /></h2>
                </div>
                <div class="page-back-btn">
                    <dx:ASPxHyperLink ID="lnkBack" runat="server" Text="Back" CssClass="btn" NavigateUrl="~/Members/Referrer.aspx">
                        </dx:ASPxHyperLink>
                </div>
                <div class="clear">
                </div>
            </div>
            
                <div class="t_wrap">
                    <div class="send_msg t_to" style="padding-right: 5px; padding-left: 5px;">
                        <asp:Panel ID="pnlErr" runat="server" CssClass="alert alert-danger" Visible="False"
                            EnableViewState="False">
                            <asp:Label ID="lblErr" runat="server" />
                        </asp:Panel>
                        <asp:HiddenField ID="hdfCreditsForMessage" runat="server" Value="10" Visible="false" />
                        <asp:HiddenField ID="hdfCreditsForVidChat" runat="server" Value="10" Visible="false" />
                        <asp:Label ID="lblPageDesc" runat="server" Text="" />
                        <br />
                        <br />
                        <div class="clear" style="height: 5px;">
                            &nbsp;</div>
                        <asp:UpdatePanel ID="updPnl" runat="server">
                            <ContentTemplate>
                                <div align="center" style="font-size: 10.0pt; mso-bidi-font-family: Calibri;">
                                    <div class="MsoNormal" align="left" style="margin-bottom: 20px;background-color:#26A9E1;height:30px;padding-top:5px;">
                                        <asp:Label ID="lblCalcIncomeText" runat="server" Text="Υπολογισμός εσόδων" Style="color: #fff; padding: 2px 10px; display: inline-block;" />
                                        <div class="rfloat">
                                            <asp:LinkButton ID="lnkRecalc1" CssClass="btn btn-primary" runat="server" 
                                                Visible="False">Επαναϋπολογισμός</asp:LinkButton>
                                        </div>
                                        <div class="clear">&nbsp;</div>
                                    </div>
                                    <hr style="height:1px;background-color:#9A9A9A;border:none;"/>
                                    <br />

                                    <asp:Panel ID="pnlSettings" runat="server" align="left" Visible="false">
                                        <table cellpadding="5">
                                            <tr>
                                                <td valign="middle">
                                                    <asp:Label ID="Label23" runat="server" Text="Credits for message" />
                                                </td>
                                                <td valign="middle">
                                                    <dx:ASPxSpinEdit ID="spinMessageCredits" runat="server" Height="22px" Number="10"
                                                        Width="50px" BackColor="#CCFFCC" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css"
                                                        CssPostfix="PlasticBlue" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css"
                                                        Font-Bold="True" MaxValue="999" NullText="0" />
                                                </td>
                                                <td valign="middle">
                                                    <asp:Label ID="Label24" runat="server" Text=" CRD " />
                                                </td>
                                            </tr>
                                        </table>
                                        <table cellpadding="5">
                                            <tr>
                                                <td valign="middle">
                                                    <asp:Label ID="Label25" runat="server" Text="Credits for VideoChat (minute)" />
                                                </td>
                                                <td valign="middle">
                                                    <dx:ASPxSpinEdit ID="spinVideoChatCredits" runat="server" Height="22px" Number="10"
                                                        Width="50px" BackColor="#CCFFCC" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css"
                                                        CssPostfix="PlasticBlue" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css"
                                                        Font-Bold="True" MaxValue="999" />
                                                </td>
                                                <td valign="middle">
                                                    <asp:Label ID="Label26" runat="server" Text=" CRD " />
                                                </td>
                                            </tr>
                                        </table>
                                        <table cellpadding="5">
                                            <tr>
                                                <td valign="middle">
                                                    <asp:Label ID="Label27" runat="server" Text="Conversion rate (CRD to EUR)" />
                                                </td>
                                                <td valign="middle">
                                                    <dx:ASPxSpinEdit ID="spinConvRate" runat="server" Height="22px" Number="0.45" Width="60px"
                                                        BackColor="#CCFFCC" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css" CssPostfix="PlasticBlue"
                                                        SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css" Font-Bold="True"
                                                        Increment="0.1" LargeIncrement="1" MaxValue="999" />
                                                </td>
                                                <td valign="middle">
                                                    <asp:Label ID="Label28" runat="server" Text="  " />
                                                </td>
                                            </tr>
                                        </table>
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <table cellpadding="5">
                                                        <tr>
                                                            <td valign="middle">
                                                                <asp:Label ID="Label29" runat="server" Text="Commissions (percent of credits)" />
                                                            </td>
                                                            <td valign="middle">
                                                                <asp:Label ID="Label30" runat="server" Text=" L[1] " />
                                                            </td>
                                                            <td valign="middle">
                                                                <dx:ASPxSpinEdit ID="spinCommissionLev1" runat="server" Height="22px" Number="0.2"
                                                                    Width="60px" BackColor="#CCFFCC" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css"
                                                                    CssPostfix="PlasticBlue" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css"
                                                                    Font-Bold="True" Increment="0.1" LargeIncrement="1" MaxValue="999" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td>
                                                    <table cellpadding="5">
                                                        <tr>
                                                            <td valign="middle">
                                                                <asp:Label ID="Label32" runat="server" Text=" L[2] " />
                                                            </td>
                                                            <td valign="middle">
                                                                <dx:ASPxSpinEdit ID="spinCommissionLev2" runat="server" Height="22px" Number="0.1"
                                                                    Width="60px" BackColor="#CCFFCC" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css"
                                                                    CssPostfix="PlasticBlue" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css"
                                                                    Font-Bold="True" Increment="0.1" LargeIncrement="1" MaxValue="999" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td>
                                                    <table cellpadding="5">
                                                        <tr>
                                                            <td valign="middle">
                                                                <asp:Label ID="Label34" runat="server" Text=" L[3] " />
                                                            </td>
                                                            <td valign="middle">
                                                                <dx:ASPxSpinEdit ID="spinCommissionLev3" runat="server" Height="22px" Number="0.05"
                                                                    Width="60px" BackColor="#CCFFCC" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css"
                                                                    CssPostfix="PlasticBlue" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css"
                                                                    Font-Bold="True" Increment="0.01" LargeIncrement="0.1" MaxValue="999" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td>
                                                    <table cellpadding="5">
                                                        <tr>
                                                            <td valign="middle">
                                                                <asp:Label ID="Label36" runat="server" Text=" L[4] " />
                                                            </td>
                                                            <td valign="middle">
                                                                <dx:ASPxSpinEdit ID="spinCommissionLev4" runat="server" Height="22px" Number="0.05"
                                                                    Width="60px" BackColor="#CCFFCC" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css"
                                                                    CssPostfix="PlasticBlue" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css"
                                                                    Font-Bold="True" Increment="0.01" LargeIncrement="0.1" MaxValue="999" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td>
                                                    <table cellpadding="5">
                                                        <tr>
                                                            <td valign="middle">
                                                                <asp:Label ID="Label38" runat="server" Text=" L[5] " />
                                                            </td>
                                                            <td valign="middle">
                                                                <dx:ASPxSpinEdit ID="spinCommissionLev5" runat="server" Height="22px" Number="0.05"
                                                                    Width="60px" BackColor="#CCFFCC" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css"
                                                                    CssPostfix="PlasticBlue" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css"
                                                                    Font-Bold="True" Increment="0.01" LargeIncrement="0.1" MaxValue="999" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <hr />
                                        <br />
                                    </asp:Panel>


                                    <div class="clear" style="height: 15px;">
                                        &nbsp;</div>
                                    <div class="MsoNormal" style="margin:0 0 30px;">
                                        <div class="lfloat" style="text-align:left;" >
                                            <table cellpadding="5">
                                                <tr>
                                                    <td valign="middle">
                                                        <asp:Label ID="lblSpinChildrenBfr" runat="server" Text="Εσείς έχετε την δυνατότητα να κάνετε" />
                                                    </td>
                                                    <td valign="middle">
                                                        <dx:ASPxSpinEdit ID="spinChildren" runat="server" Height="33px" Number="10" Width="84px"
                                                            Font-Bold="True" MaxValue="999" CssClass="spin-control">
                                                            <SpinButtons>
                                                                <IncrementImage Url="~/Images/referrer/p.png">
                                                                </IncrementImage>
                                                                <DecrementImage Url="~/Images/referrer/m.png">
                                                                </DecrementImage>
                                                            </SpinButtons>
                                                            <IncrementButtonStyle Width="26px">
                                                                <Border BorderStyle="None" />
                                                            </IncrementButtonStyle>
                                                            <DecrementButtonStyle VerticalAlign="Top" Width="26px">
                                                                <Border BorderStyle="None" />
                                                            </DecrementButtonStyle>
                                                            <BackgroundImage ImageUrl="~/Images/referrer/co.png" Repeat="NoRepeat" />
                                                            <Border BorderStyle="None" />
                                                        </dx:ASPxSpinEdit>
                                                    </td>
                                                    <td valign="middle">
                                                        <asp:Label ID="lblSpinChildrenAft" runat="server" Text=" referrals " />
                                                    </td>
                                                </tr>
                                            </table>
                                            <table cellpadding="5">
                                                <tr>
                                                    <td valign="middle">
                                                        <asp:Label ID="lblSpinChildren2Bfr" runat="server" Text=" και αυτοί να κάνουν κατά μέσο όρο" />
                                                    </td>
                                                    <td valign="middle">
                                                        <dx:ASPxSpinEdit ID="spinChildren2" runat="server" Height="39px" Number="4" Width="84px"
                                                            Font-Bold="True" MaxValue="999" CssClass="spin-control">
                                                            <SpinButtons>
                                                                <IncrementImage Url="~/Images/referrer/p.png">
                                                                </IncrementImage>
                                                                <DecrementImage Url="~/Images/referrer/m.png">
                                                                </DecrementImage>
                                                            </SpinButtons>
                                                            <IncrementButtonStyle Width="26px">
                                                                <Border BorderStyle="None" />
                                                            </IncrementButtonStyle>
                                                            <DecrementButtonStyle VerticalAlign="Top" Width="26px">
                                                                <Border BorderStyle="None" />
                                                            </DecrementButtonStyle>
                                                            <BackgroundImage ImageUrl="~/Images/referrer/co.png" Repeat="NoRepeat" />
                                                            <Border BorderStyle="None" />
                                                        </dx:ASPxSpinEdit>
                                                        <%--<dx:ASPxSpinEdit ID="spinChildren2" runat="server" Height="22px" Number="4" Width="50px"
                                                            BackColor="#CCFFCC" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css" CssPostfix="PlasticBlue"
                                                            SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css" 
                                                            Font-Bold="True" MaxValue="999" />--%>
                                                    </td>
                                                    <td valign="middle">
                                                        <asp:Label ID="lblSpinChildren2Aft" runat="server" Text=" referrals" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="rfloat" style="text-align:center;padding:5px 30px 0 0;">
                                            <div style="width:156px;height:90px;background:url(../Images/referrer/woman.png) repeat-x scroll">
                                            </div>
                                        </div>
                                        <div class="clear" style="height: 35px;">
                                            &nbsp;</div>
                                        <div class="lfloat" style="position:relative;width:276px;height:147px;background:url(../Images/referrer/levels.png) no-repeat scroll">
                                            <div style="position:absolute;top:9px;left:9px;color:#fff;"><asp:Literal ID="lblReferrals5Text" runat="server" Text="Level 5" /></div>
                                            <div style="position:absolute;top:49px;left:9px;color:#fff;"><asp:Literal ID="lblReferrals4Text" runat="server" Text="Level 4" /></div>
                                            <div style="position:absolute;top:86px;left:9px;color:#fff;"><asp:Literal ID="lblReferrals3Text" runat="server" Text="Level 3" /></div>
                                            <div style="position:absolute;top:123px;left:9px;color:#fff;"><asp:Literal ID="lblReferrals2Text" runat="server" Text="Level 2" /></div>
                                            <div style="position:absolute;top:3px;right:-39px;color:#000;font-weight:bold;"><asp:Literal ID="lblChildren5" runat="server" Text="5" /></div>
                                            <div style="position:absolute;top:39px;right: 10px;color:#000;font-weight:bold;"><asp:Literal ID="lblChildren3" runat="server" Text="4" /></div>
                                            <div style="position:absolute;top:79px;right:52px;color:#000;font-weight:bold;"><asp:Literal ID="lblChildren2" runat="server" Text="3" /></div>
                                            <div style="position:absolute;top:116px;right:80px;color:#000;font-weight:bold;"><asp:Literal ID="lblChildren" runat="server" Text="2" /></div>
                                            <div style="position:absolute;top:2px;left:84px;color:#fff;"><asp:Literal ID="lblReferralsWord1" runat="server" Text="Referrals" /></div>
                                            <div style="position:absolute;top:40px;left:84px;color:#fff;"><asp:Literal ID="lblReferralsWord2" runat="server" Text="Referrals" /></div>
                                            <div style="position:absolute;top:79px;left:84px;color:#fff;"><asp:Literal ID="lblReferralsWord3" runat="server" Text="Referrals" /></div>
                                            <div style="position:absolute;top:116px;left:84px;color:#fff;"><asp:Literal ID="lblReferralsWord4" runat="server" Text="Referrals" /></div>
                                        </div>
                                        <div class="rfloat" style="width:25%;">
                                            <asp:Image ID="imgPeople" runat="server" 
                                                ImageUrl="~/Images/referrer/humans.png" />
                                        </div>
                                        <div class="rfloat" style="position:relative;width:20%;height:147px;background:url(../Images/referrer/cycle.png) no-repeat scroll 50% 0;">
                                            <div class="family-count">
                                                <asp:Label ID="lblFamily" runat="server" Text="15,550" />
                                            </div>
                                            <div style="position:absolute;bottom:0px;left:0px;">
                                                <asp:Label ID="lblFamilyText" runat="server" Text=" ολόκληρη η οικογένεια σας" />
                                            </div>
                                        </div>
                                        <div class="clear" style="height: 15px;">
                                            &nbsp;</div>
                                        <%--
                                        <div align="center" class="MsoNormal">
                                            <b>
                                                <asp:Label ID="lblChildren" runat="server" Text="10" Style="color: #00A0EE; font-size: 16pt;" /><asp:Label
                                                    ID="lblReferrals2Text" runat="server" Text=" referrals[2] " />
                                                <asp:Label ID="lblChildren2" runat="server" Text="60" Style="color: #00A0EE; font-size: 16pt;" />
                                                <asp:Label ID="lblReferrals3Text" runat="server" Text=" referrals[3]" />
                                                <asp:Label ID="lblChildren3" runat="server" Text="360" Style="color: #00A0EE; font-size: 16pt;" />
                                                <asp:Label ID="lblReferrals4Text" runat="server" Text=" referrals[4]" />
                                                <asp:Label ID="lblChildren5" runat="server" Text="2,160" Style="color: #00A0EE; font-size: 16pt;" />
                                                </b></div>
                                        <br />
                                        <div align="center" class="MsoNormal">
                                            <b><span style="font-size: 20.0pt; color: #00A0EE">
                                                </span></b><b></b></div>
                                        <br />
                                        --%>
                                        <div align="center" class="MsoNormal">
                                            <b>
                                                <asp:HyperLink ID="lnkShowCalcs1" runat="server" NavigateUrl="#" 
                                                Visible="False">Προβολή υπολογισμών</asp:HyperLink></b></div>
                                    </div>

                                    <asp:Panel ID="pnlMessages" runat="server" CssClass="MsoNormal" style="margin:0 0 30px;">
                                        <hr class="slim" style=""/>
                                        <div class="lfloat" style="text-align:left;" >
                                            <table cellpadding="5">
                                                <tr>
                                                    <td valign="middle">
                                                        <asp:Label ID="lblSpinMessagesBfr" runat="server" Text="Εσείς διαχειρίζεστε" />
                                                    </td>
                                                    <td valign="middle">
                                                        <dx:ASPxSpinEdit ID="spinMessages" runat="server" Height="39px" Number="20" Width="84px"
                                                            Font-Bold="True" MaxValue="999" CssClass="spin-control">
                                                            <SpinButtons>
                                                                <IncrementImage Url="~/Images/referrer/p.png">
                                                                </IncrementImage>
                                                                <DecrementImage Url="~/Images/referrer/m.png">
                                                                </DecrementImage>
                                                            </SpinButtons>
                                                            <IncrementButtonStyle Width="26px">
                                                                <Border BorderStyle="None" />
                                                            </IncrementButtonStyle>
                                                            <DecrementButtonStyle VerticalAlign="Top" Width="26px">
                                                                <Border BorderStyle="None" />
                                                            </DecrementButtonStyle>
                                                            <BackgroundImage ImageUrl="~/Images/referrer/co.png" Repeat="NoRepeat" />
                                                            <Border BorderStyle="None" />
                                                        </dx:ASPxSpinEdit><%--<dx:ASPxSpinEdit ID="spinMessages" runat="server" Height="22px" Number="20" Width="50px"
                                                            BackColor="#CCFFCC" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css" CssPostfix="PlasticBlue"
                                                            SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css" 
                                                            Font-Bold="True" MaxValue="999" />--%>
                                                    </td>
                                                    <td valign="middle">
                                                        <asp:Label ID="lblSpinMessagesAft" runat="server" Text=" μηνύματα " />
                                                    </td>
                                                </tr>
                                            </table>
                                            <table cellpadding="5">
                                                <tr>
                                                    <td valign="middle">
                                                        <asp:Label ID="lblSpinMessages2Bfr" runat="server" Text=" και τα παιδιά σας ένα μέσο όρο" />
                                                    </td>
                                                    <td valign="middle">
                                                        <dx:ASPxSpinEdit ID="spinMessages2" runat="server" Height="39px" Number="10" Width="84px"
                                                            Font-Bold="True" MaxValue="999" CssClass="spin-control">
                                                            <SpinButtons>
                                                                <IncrementImage Url="~/Images/referrer/p.png">
                                                                </IncrementImage>
                                                                <DecrementImage Url="~/Images/referrer/m.png">
                                                                </DecrementImage>
                                                            </SpinButtons>
                                                            <IncrementButtonStyle Width="26px">
                                                                <Border BorderStyle="None" />
                                                            </IncrementButtonStyle>
                                                            <DecrementButtonStyle VerticalAlign="Top" Width="26px">
                                                                <Border BorderStyle="None" />
                                                            </DecrementButtonStyle>
                                                            <BackgroundImage ImageUrl="~/Images/referrer/co.png" Repeat="NoRepeat" />
                                                            <Border BorderStyle="None" />
                                                        </dx:ASPxSpinEdit><%--<dx:ASPxSpinEdit ID="spinMessages2" runat="server" Height="22px" Number="10" Width="50px"
                                                            BackColor="#CCFFCC" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css" CssPostfix="PlasticBlue"
                                                            SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css" 
                                                            Font-Bold="True" MaxValue="999" />--%>
                                                    </td>
                                                    <td valign="middle">
                                                        <asp:Label ID="lblSpinMessages2Aft" runat="server" Text=" μηνύματα την ημέρα" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="rfloat image-folders-wrap" style="">
                                            <div class="image-folders">
                                            </div>
                                        </div>
                                        <div class="clear" style="height: 15px;">
                                            &nbsp;</div>
                                        <br />
                                        <div align="center" class="MsoNormal">
                                            <div class="lfloat amount-box">
                                                <asp:Label ID="lblMessageDay" runat="server" Text="€0.17" CssClass="msg-amount-text" />&nbsp;<asp:Label ID="lblMessageDayText" runat="server" Text=" τη μέρα&nbsp;&nbsp; &nbsp;" />
                                            </div>

                                            <div class="lfloat amount-box">
                                                <asp:Label ID="lblMessageMonth" runat="server" Text="€4.95" CssClass="msg-amount-text" />&nbsp;<asp:Label ID="lblMessageMonthText" runat="server" Text=" το μήνα&nbsp;&nbsp; &nbsp;" />
                                            </div>

                                            <div class="lfloat amount-box">
                                                <asp:Label ID="lblMessageYear" runat="server" Text="€60.23" CssClass="msg-amount-text" />&nbsp;<asp:Label ID="lblMessageYearText" runat="server" Text=" το έτος&nbsp;&nbsp; &nbsp;" />
                                            </div>
                                        </div>
                                        <br />
                                        <div align="center" class="MsoNormal">
                                            <b>
                                                <asp:HyperLink ID="lnkShowCalcs2" runat="server" NavigateUrl="#" 
                                                Visible="False">Προβολή υπολογισμών</asp:HyperLink></b></div>
                                        <br />
                                        <br />
                                    </asp:Panel>

                                    <asp:Panel ID="pnlVid" runat="server" CssClass="MsoNormal" Visible="False">
                                        <div align="left">
                                            <table cellpadding="5">
                                                <tr>
                                                    <td valign="middle">
                                                        <asp:Label ID="lblSpinVidChatBfr" runat="server" Text="Εσείς και οι συνεργάτες πραγματοποιείτε" />
                                                    </td>
                                                    <td valign="middle">
                                                        <dx:ASPxSpinEdit ID="spinVidChat" runat="server" Height="22px" Number="2" Width="50px"
                                                            BackColor="#CCFFCC" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css" CssPostfix="PlasticBlue"
                                                            SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css" 
                                                            Font-Bold="True" MaxValue="999" />
                                                    </td>
                                                    <td valign="middle">
                                                        <asp:Label ID="lblSpinVidChatAft" runat="server" Text=" VideoChat την εβδομάδα " />
                                                    </td>
                                                </tr>
                                            </table>
                                            <table cellpadding="5">
                                                <tr>
                                                    <td valign="middle">
                                                        <asp:Label ID="lblSpinVidChat2Bfr" runat="server" Text=" με μέσο όρο" />
                                                    </td>
                                                    <td valign="middle">
                                                        <dx:ASPxSpinEdit ID="spinVidChat2" runat="server" Height="22px" Number="2" Width="50px"
                                                            BackColor="#CCFFCC" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css" CssPostfix="PlasticBlue"
                                                            SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css" 
                                                            Font-Bold="True" MaxValue="999" />
                                                    </td>
                                                    <td valign="middle">
                                                        <asp:Label ID="lblSpinVidChat2Aft" runat="server" Text=" λεπτά." />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="clear" style="height: 15px;">
                                            &nbsp;</div>
                                        <br />
                                        <div align="center" class="MsoNormal">
                                            <b>
                                                <asp:Label ID="lblVidDay" runat="server" Text="€0.17" Style="font-size: 14.0pt;
                                                    color: #00A0EE" /><asp:Label ID="lblVidDayText" runat="server" Text=" τη μέρα&nbsp;&nbsp; &nbsp;" />
                                                <asp:Label ID="lblVidMonth" runat="server" Text="€4.95" Style="font-size: 14.0pt;
                                                    color: #00A0EE" /><asp:Label ID="lblVidMonthText" runat="server" Text=" το μήνα&nbsp;&nbsp; &nbsp;" />
                                                <asp:Label ID="lblVidYear" runat="server" Text="€60.23" Style="font-size: 14.0pt;
                                                    color: #00A0EE" /><asp:Label ID="lblVidYearText" runat="server" Text=" το έτος&nbsp;&nbsp; &nbsp;" /></b></div>
                                        <br />
                                        <div align="center" class="MsoNormal">
                                            <b>
                                                <asp:HyperLink ID="lnkShowCalcs3" runat="server" NavigateUrl="#" 
                                                Visible="False">Προβολή υπολογισμών</asp:HyperLink></b></div>
                                        <br />
                                        <br />
                                    </asp:Panel>

                                    <asp:Panel ID="pnlOffers" runat="server" CssClass="MsoNormal" Visible="False">
                                        <div align="left">
                                            <table cellpadding="5">
                                                <tr>
                                                    <td valign="middle">
                                                        <asp:Label ID="lblSpinOffersBfr" runat="server" Text="Εσείς και οι συνεργάτες πραγματοποιείτε" />
                                                    </td>
                                                    <td valign="middle">
                                                        <dx:ASPxSpinEdit ID="spinOffers" runat="server" Height="22px" Number="2" Width="50px"
                                                            BackColor="#CCFFCC" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css" CssPostfix="PlasticBlue"
                                                            SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css" 
                                                            Font-Bold="True" MaxValue="999" />
                                                    </td>
                                                    <td valign="middle">
                                                        <asp:Label ID="lblSpinOffersAft" runat="server" Text=" Offers την εβδομάδα " />
                                                    </td>
                                                </tr>
                                            </table>
                                            <table cellpadding="5">
                                                <tr>
                                                    <td valign="middle">
                                                        <asp:Label ID="lblSpinOffers2Bfr" runat="server" Text=" με μέσο όρο" />
                                                    </td>
                                                    <td valign="middle">
                                                        <dx:ASPxSpinEdit ID="spinOffers2" runat="server" Height="22px" Number="20" Width="50px"
                                                            BackColor="#CCFFCC" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css" CssPostfix="PlasticBlue"
                                                            SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css" 
                                                            Font-Bold="True" MaxValue="999" />
                                                    </td>
                                                    <td valign="middle">
                                                        <asp:Label ID="lblSpinOffers2Aft" runat="server" Text=" Ευρώ" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="clear" style="height: 15px;">
                                            &nbsp;</div>
                                        <br />
                                        <div align="center" class="MsoNormal">
                                            <b>
                                                <asp:Label ID="lblOfferDay" runat="server" Text="€0.17" Style="font-size: 14.0pt;
                                                    color: #00A0EE" /><asp:Label ID="lblOfferDayText" runat="server" Text=" τη μέρα&nbsp;&nbsp; &nbsp;" />
                                                <asp:Label ID="lblOfferMonth" runat="server" Text="€4.95" Style="font-size: 14.0pt;
                                                    color: #00A0EE" /><asp:Label ID="lblOfferMonthText" runat="server" Text=" το μήνα&nbsp;&nbsp; &nbsp;" />
                                                <asp:Label ID="lblOfferYear" runat="server" Text="€60.23" Style="font-size: 14.0pt;
                                                    color: #00A0EE" /><asp:Label ID="lblOfferYearText" runat="server" Text=" το έτος&nbsp;&nbsp; &nbsp;" /></b></div>
                                        <br />
                                        <div align="center" class="MsoNormal">
                                            <b>
                                                <asp:HyperLink ID="lnkShowCalcs4" runat="server" NavigateUrl="#" 
                                                Visible="False">Προβολή υπολογισμών</asp:HyperLink></b></div>
                                        <br />
                                        <br />
                                    </asp:Panel>

                                    <div class="MsoNormal" style="margin:0 0 30px;">
                                        <hr style="height:1px;background-color:#9A9A9A;border:none;"/>
                                        <asp:Label ID="lblTotalsText" runat="server" Text="Συνολικά μπορείτε να έχετε έσοδα :" />
                                        <br />
                                        <br />
                                        <div style="font-size:20px;position:relative;text-align:center;width:327px;height:204px;background:url(../Images/referrer/box.png) no-repeat scroll 50% 0">
                                            <div style="position:absolute;width:143px;height:40px;top:36px;text-align:left;padding:10px 5px 5px 20px;">
                                                <asp:Label ID="lblTotalDayText" runat="server" Text=" τη μέρα&nbsp;&nbsp; &nbsp;" />
                                            </div>
                                            <div style="position:absolute;width:137px;height:32px;top:36px;right:19px;text-align:right;padding:5px;font-size:20px;font-weight:bold;text-align:center;">
                                                <asp:Label ID="lblTotalDay" runat="server" Text="€0.17" Style="font-size: 16.0pt;
                                                    color: #00A0EE" />
                                            </div>

                                            <div style="position:absolute;width:143px;height:40px;top:81px;text-align:left;padding:10px 5px 5px 20px;">
                                                <asp:Label ID="lblTotalMonthText" runat="server" Text=" τη μέρα&nbsp;&nbsp; &nbsp;" />
                                            </div>
                                            <div style="position:absolute;width:137px;height:32px;top:84px;right:19px;text-align:right;padding:5px;font-size:20px;font-weight:bold;text-align:center;">
                                                <asp:Label ID="lblTotalMonth" runat="server" Text="€0.17" Style="font-size: 16.0pt;
                                                    color: #00A0EE" />
                                            </div>

                                            <div style="position:absolute;width:143px;height:40px;top:123px;text-align:left;padding:10px 5px 5px 20px;">
                                                <asp:Label ID="lblTotalYearText" runat="server" Text=" τη μέρα&nbsp;&nbsp; &nbsp;" />
                                            </div>
                                            <div style="position:absolute;width:137px;height:32px;top:127px;right:19px;text-align:right;padding:5px;font-size:20px;font-weight:bold;text-align:center;">
                                                <asp:Label ID="lblTotalYear" runat="server" Text="€0.17" Style="font-size: 16.0pt;
                                                    color: #00A0EE" />
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                        <br />
                                        <div align="center" class="MsoNormal">
                                            <b>
                                                <asp:HyperLink ID="lnkShowCalcs5" runat="server" NavigateUrl="#" 
                                                Visible="False">Προβολή υπολογισμών</asp:HyperLink></b></div>
                                    </div>

                                    <asp:Label ID="lblFooterText" runat="server" />
                                    <br />
                                    <br />
                                    <asp:LinkButton ID="lnkRecalc2" CssClass="btn btn-blue-quad" runat="server">Επαναϋπολογισμός</asp:LinkButton>
                                    <br />
                                    <br />
                                    <dx:ASPxPopupControl ID="popupCalcs" runat="server" AllowDragging="True" HeaderText="Προβολή υπολογισμών"
                                        Height="350px" Width="700px" AllowResize="True" ScrollBars="Auto" PopupHorizontalAlign="WindowCenter"
                                        Modal="True" PopupVerticalAlign="WindowCenter" RenderMode="Lightweight">
                                        <ContentStyle HorizontalAlign="Left" VerticalAlign="Middle">
                                            <BackgroundImage ImageUrl="../images/bg.jpg" />
                                            <Paddings Padding="10px" />
                                            <Paddings Padding="10px"></Paddings>
                                            <BackgroundImage ImageUrl="../images/bg.jpg"></BackgroundImage>
                                        </ContentStyle>
                                        <CloseButtonStyle>
                                            <HoverStyle>
                                                <BackgroundImage ImageUrl="~/Images/close-button1.png" Repeat="NoRepeat" HorizontalPosition="center"
                                                    VerticalPosition="center"></BackgroundImage>
                                            </HoverStyle>
                                            <BackgroundImage ImageUrl="~/Images/close-button2.png" Repeat="NoRepeat" HorizontalPosition="center"
                                                VerticalPosition="center"></BackgroundImage>
                                        </CloseButtonStyle>
                                        <CloseButtonImage Url="~/Images/spacer10.png" Height="17px" Width="17px">
                                        </CloseButtonImage>
                                        <SizeGripImage Height="40px" Url="~/Images/resize.png" Width="40px">
                                        </SizeGripImage>
                                        <HeaderStyle BackColor="#35619F" Font-Bold="True" ForeColor="White">
                                            <Paddings PaddingBottom="10px" PaddingTop="10px" />
                                            <Paddings PaddingTop="10px" PaddingBottom="10px"></Paddings>
                                        </HeaderStyle>
                                        <ModalBackgroundStyle CssClass="modalPopup">
                                        </ModalBackgroundStyle>
                                        <Windows>
                                            <dx:PopupWindow PopupElementID="lnkShowCalcs1" HeaderText="Προβολή υπολογισμών οικογένειας">
                                                <ContentStyle Font-Size="12pt">
                                                </ContentStyle>
                                                <ContentCollection>
                                                    <dx:PopupControlContentControl ID="PopupControlContentControl2" runat="server" SupportsDisabledAttribute="True">
                                                        <asp:Label ID="lblCalcReferrals" runat="server" Text="" />
                                                    </dx:PopupControlContentControl>
                                                </ContentCollection>
                                            </dx:PopupWindow>
                                            <dx:PopupWindow PopupElementID="lnkShowCalcs2" HeaderText="Προβολή υπολογισμών μηνυμάτων">
                                                <ContentStyle Font-Size="12pt">
                                                </ContentStyle>
                                                <ContentCollection>
                                                    <dx:PopupControlContentControl ID="PopupControlContentControl3" runat="server" SupportsDisabledAttribute="True">
                                                        <asp:Label ID="lblCalcMessages" runat="server" Text="" />
                                                    </dx:PopupControlContentControl>
                                                </ContentCollection>
                                            </dx:PopupWindow>
                                            <dx:PopupWindow PopupElementID="lnkShowCalcs3" HeaderText="Προβολή υπολογισμών VideoChat">
                                                <ContentStyle Font-Size="12pt">
                                                </ContentStyle>
                                                <ContentCollection>
                                                    <dx:PopupControlContentControl ID="PopupControlContentControl4" runat="server" SupportsDisabledAttribute="True">
                                                        <asp:Label ID="lblCalcVidChat" runat="server" Text="" />
                                                    </dx:PopupControlContentControl>
                                                </ContentCollection>
                                            </dx:PopupWindow>
                                            <dx:PopupWindow PopupElementID="lnkShowCalcs4" HeaderText="Προβολή υπολογισμών προσφορών">
                                                <ContentStyle Font-Size="12pt">
                                                </ContentStyle>
                                                <ContentCollection>
                                                    <dx:PopupControlContentControl ID="PopupControlContentControl5" runat="server" SupportsDisabledAttribute="True">
                                                        <asp:Label ID="lblCalcOffers" runat="server" Text="" />
                                                    </dx:PopupControlContentControl>
                                                </ContentCollection>
                                            </dx:PopupWindow>
                                            <dx:PopupWindow PopupElementID="lnkShowCalcs5" HeaderText="Προβολή υπολογισμών συνόλου">
                                                <ContentStyle Font-Size="12pt">
                                                </ContentStyle>
                                                <ContentCollection>
                                                    <dx:PopupControlContentControl ID="PopupControlContentControl6" runat="server" SupportsDisabledAttribute="True">
                                                        <asp:Label ID="lblCalcTotal" runat="server" Text="" />
                                                    </dx:PopupControlContentControl>
                                                </ContentCollection>
                                            </dx:PopupWindow>
                                        </Windows>
                                        <ContentCollection>
                                            <dx:PopupControlContentControl ID="PopupControlContentControl7" runat="server" SupportsDisabledAttribute="True">
                                            </dx:PopupControlContentControl>
                                        </ContentCollection>
                                    </dx:ASPxPopupControl>
                                </div>
                                </span>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            <%--</div>
            
        </div>
        <div class="clear">
        </div>
    </div>--%>


	<uc2:WhatIsIt ID="WhatIsIt1" runat="server" />

</asp:Content>

﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Members/Members2Inner.master" 
CodeBehind="ReferrerPayments.aspx.vb" Inherits="Dating.Referrals.Server.Site.Web.ReferrerPayments" %>

<%@ Register src="../UserControls/WhatIsIt.ascx" tagname="WhatIsIt" tagprefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    
            
           <div class="grid_3 top_sidebar" id="sidebar-outter">
                <div class="lfloat">
                    <h2 class="page-name"><asp:Literal ID="lblItemsName" runat="server" /></h2>
                    <h2 class="page-description"><asp:Label ID="lblPageTitle" runat="server" Text="Οι Προμήθειες μου ανά επίπεδο" /></h2>
                </div>
                <div class="page-back-btn">
                    <dx:ASPxHyperLink ID="lnkBack" runat="server" Text="Back" CssClass="btn" 
                        NavigateUrl="~/Members/Referrer.aspx">
                    </dx:ASPxHyperLink>
                </div>
                <div class="clear">
                </div>
            </div>

                    <div class="t_wrap">
                        <div class="send_msg t_to msg">
                            <asp:Panel ID="pnlErr" runat="server" CssClass="alert alert-danger" 
                                Visible="False" EnableViewState="False">
                                <asp:Label ID="lblErr" runat="server"></asp:Label>
                            </asp:Panel>

                <asp:Label ID="lblPageDesc" runat="server" Text="Εδώ μπορείτε να δείτε τις πληρωμές προς εσάς από το site."></asp:Label>
        <br />
        <br />
        <table cellpadding="5" cellspacing="1">
            <tr>
                <td>
                    <asp:Label ID="lblFromDate" runat="server" Text="Από Ημερομηνία : "></asp:Label></td>
                <td>
                    <dx:ASPxDateEdit ID="dtFrom" runat="server" DisplayFormatString="dd/MM/yyyy" 
                        Width="120px">
                    </dx:ASPxDateEdit>
                </td>
                <td>
                    <asp:Label ID="lblToDate" runat="server" Text="Εως "></asp:Label></td>
                <td><dx:ASPxDateEdit ID="dtTo" runat="server" DisplayFormatString="dd/MM/yyyy" 
                        Width="120px">
                    </dx:ASPxDateEdit></td>
                <td>
                    <dx:ASPxButton ID="btnRefresh" runat="server" Text="Refresh">
                    </dx:ASPxButton>
                </td>
            </tr>
        </table>
        
        <dx:ASPxGridView ID="gvPayments" runat="server" AutoGenerateColumns="False"
            DataSourceID="odsPayments" Width="100%" 
            KeyFieldName="ReferrerTransactionID">
            <TotalSummary>
                <%--<dx:ASPxSummaryItem DisplayFormat="Sum=0.00 &euro;" FieldName="DebitAmount" ShowInColumn="DebitAmount"
                    SummaryType="Sum" />--%>
                <dx:ASPxSummaryItem DisplayFormat="Sum=0.00" FieldName="DebitAmount" ShowInColumn="DebitAmount"
                    SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="Sum={0}" FieldName="CommissionCRD" ShowInColumn="colCommissionCRD"
                    SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="Totals" ShowInColumn="colReferrerLoginName" SummaryType="Custom" />
            </TotalSummary>
            <SettingsText EmptyDataRow="Δεν βρέθηκαν πληρωμές" />
            <Columns>
                <dx:GridViewDataTextColumn FieldName="ReferrerTransactionID" ReadOnly="True" 
                    VisibleIndex="5" Visible="False">
                    <EditFormSettings Visible="False" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataDateColumn FieldName="Date_Time" VisibleIndex="0" 
                    Caption="Date" CellStyle-Wrap="False">
                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy HH:mm">
                    </PropertiesDateEdit>
                </dx:GridViewDataDateColumn>
                <dx:GridViewDataTextColumn FieldName="ReferrerID" VisibleIndex="7" 
                    Visible="False">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="CreditAmount" VisibleIndex="6" 
                    Visible="False">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="DebitAmount" VisibleIndex="4" 
                    Caption="Amount">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="TransactionType" VisibleIndex="8" 
                    Visible="False">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Description" VisibleIndex="2" Width="70%">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Referrer" VisibleIndex="9" 
                    Visible="False">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="CustomReferrer" VisibleIndex="10" 
                    Visible="False">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="IP" VisibleIndex="11" Visible="False">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="TransactionInfo" VisibleIndex="1">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Notes" VisibleIndex="12" Visible="False">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="ReceiptID" VisibleIndex="13" 
                    Visible="False">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="PayoutTypeID" VisibleIndex="14" 
                    Visible="False">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="PaymentProvider" VisibleIndex="3" 
                    Caption="Provider" CellStyle-Wrap="False">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="TransactionTypeStr" VisibleIndex="15" 
                    Visible="False">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Currency" Visible="False" 
                    VisibleIndex="16">
                </dx:GridViewDataTextColumn>
            </Columns>
            <Settings ShowFooter="True" ShowFilterBar="Auto" ShowFilterRow="false" ShowGroupPanel="false" />
            <Paddings Padding="0px" />
            <Styles>
                <Header HorizontalAlign="Center" VerticalAlign="Middle" BackColor="#FF7A15" Font-Bold="True"
                                                    ForeColor="White">
                </Header>
                <Footer BackColor="#FF8b26" HorizontalAlign="Right" VerticalAlign="Middle">
                </Footer>
                <HeaderPanel>
                    <Paddings Padding="0px" />
                </HeaderPanel>
            </Styles>
        </dx:ASPxGridView>
        <asp:ObjectDataSource ID="odsPayments" runat="server" 
            SelectMethod="GetReferrersTransactions" 
            TypeName="Dating.Referrals.Server.Site.Web.ReferrerPayments">
            <SelectParameters>
                <asp:ControlParameter ControlID="dtFrom" Name="DateFrom" PropertyName="Value" 
                    Type="DateTime" />
                <asp:ControlParameter ControlID="dtTo" Name="DateTo" PropertyName="Value" 
                    Type="DateTime" />
                <asp:Parameter Name="ReferrerID" Type="Int32" />
            </SelectParameters>
        </asp:ObjectDataSource>

                        </div>
                    </div>
       <%-- </div>
	
	    </div>
        <div class="clear"></div>
    </div>--%>

	<uc2:WhatIsIt ID="WhatIsIt1" runat="server" />

</asp:Content>

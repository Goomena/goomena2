﻿Imports System.Data.SqlClient
Imports Dating.Server.Core.DLL
Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Datasets.DLL.DSMembers
Imports DevExpress.Web.ASPxEditors



Public Class Members2
    Inherits BaseMasterPage


    Public Property HideBottom_MemberLeftPanel As Boolean


    Dim _pageData As clsPageData
    Protected ReadOnly Property CurrentPageData As clsPageData
        Get
            If (_pageData Is Nothing) Then _pageData = New clsPageData("master", Context)
            Return _pageData
        End Get
    End Property


    Dim _sessVars As clsSessionVariables
    Protected ReadOnly Property SessionVariables As clsSessionVariables
        Get
            If (_sessVars Is Nothing) Then _sessVars = clsSessionVariables.GetCurrent()
            Return _sessVars
        End Get
    End Property

    Private Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        Try
            If Not clsCurrentContext.VerifyLogin() Then
                clsCurrentContext.ClearSession(Session)
                Response.Redirect(ResolveUrl("~/Login.aspx"), False)
                HttpContext.Current.ApplicationInstance.CompleteRequest()
            End If


            If Not Me.IsPostBack Then
                If (Session("NoRedirectToMembersDefault") Is Nothing) Then Session("NoRedirectToMembersDefault") = True

                If (Not Page.IsPostBack) Then
                    Dim TimeOffset As Double
                    If (hdfTimeOffset.Value.Trim() <> "" AndAlso Double.TryParse(hdfTimeOffset.Value, TimeOffset)) Then
                        Session("TimeOffset") = TimeOffset
                    End If
                End If

                'If (GeneralFunctions.IsSupportedLAG(Request.QueryString("lag"))) Then
                If (Not String.IsNullOrEmpty(Request.QueryString("lag")) AndAlso (Request.QueryString("lag").ToUpper() = "US" OrElse Request.QueryString("lag").ToUpper() = "GR")) Then
                    Session("LagID") = Request.QueryString("lag").ToUpper()

                    '' update lagid info
                    'Try
                    '    If (Me.Session("ProfileID") > 0) Then
                    '        DataHelpers.UpdateEUS_Profiles_LAGID(Me.Session("ProfileID"), Me.Session("LagID"))
                    '    End If
                    'Catch ex As Exception
                    '    WebErrorMessageBox(Me, ex, "")
                    'End Try
                End If

                If Session("LagID") Is Nothing Then
                    If (Session("GEO_COUNTRY_CODE") = "GR") Then
                        Session("LagID") = "GR"
                    Else
                        Session("LagID") = gDomainCOM_DefaultLAG
                    End If
                End If

                cmbLag.Items(GetIndexFromLagID(Session("LagID"))).Selected = True



                SetLanguageLinks()
            End If


            If (cmbLag.SelectedItem Is Nothing) Then
                cmbLag.Items(GetIndexFromLagID(Session("LagID"))).Selected = True
            End If


            Try
                ' otan to session exei diaforetiki glwsa apo tin epilegmeni glwssa
                If (GetIndexFromLagID(Session("LagID")) = 0 AndAlso Session("LagID") <> "US") Then
                    Session("LagID") = gDomainCOM_DefaultLAG
                End If
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try

            Try
                If (clsSessionVariables.GetCurrent().MemberData.Status = ProfileStatusEnum.NewProfile) Then
                    ucReferrerMenu.Visible = False
                End If
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            '    BindMembersList() 'set popup controls enabled/disabled first
            '    LoadLAG()
            '    ShowUserMap()

            '    If (Not Page.IsPostBack) Then
            '        content_bottom.Visible = False

            '        If (Me.Request.RawUrl.ToUpper().StartsWith("/MEMBERS/")) Then
            '            content_bottom.Visible = True
            '            'SetReferrerButtonVisible()
            '        End If

            '    End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        Try
            lblLogin.Text = SessionVariables.MemberData.LoginName
            LoadLAG()

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Protected Sub cmbLag_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbLag.SelectedIndexChanged
        Try

            Dim lagCookie As HttpCookie = Request.Cookies("lagCookie")
            If lagCookie Is Nothing Then
                Dim szGuid As String = System.Guid.NewGuid().ToString()
                lagCookie = New HttpCookie("lagCookie", szGuid)
                lagCookie.Expires = DateTime.Now.AddYears(2)

                lagCookie.Item("LagID") = cmbLag.SelectedItem.Value
                Response.Cookies.Add(lagCookie)
                Session("lagCookie") = szGuid
            Else
                Session("lagCookie") = Server.HtmlEncode(lagCookie.Value)
                lagCookie.Item("LagID") = cmbLag.SelectedItem.Value
                If (lagCookie.Values.Count > 0) Then Session("lagCookie") = lagCookie.Values(0)
            End If
            Session("LagID") = lagCookie.Item("LagID")
            Response.Cookies.Add(lagCookie)
            'Response.AddHeader("Refresh", "1")

        Catch ex As Exception
            Session("LagID") = cmbLag.SelectedItem.Value
        End Try

        ' update lagid info
        Try

            DataHelpers.UpdateEUS_Profiles_LAGID(Me.Session("ProfileID"), Me.Session("LagID"))

            Dim url As String = Request.Url.AbsolutePath
            url = url & "?"
            If cmbLag.SelectedIndex = 0 Then
                url = url & "lag=US"
                Response.Redirect(url)
            ElseIf cmbLag.SelectedIndex = 1 Then
                url = url & "lag=GR"
                Response.Redirect(url)
            ElseIf cmbLag.SelectedIndex = 2 Then
                url = url & "lag=AL"
                Response.Redirect(url)
            End If

        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try



        Try

            Me._pageData = Nothing
            LoadLAG()

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try


        Try
            ModGlobals.UpdateUserControls(Me.Page, True)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub

    Private Function GetIndexFromLagID(ByVal LagID As String) As Integer
        Select Case LagID
            Case "US"
                Return 0
                'Case "GR"
                '    Return 4
            Case "GR"
                Return 1
                'Case "DE"
                '    Return 3
                'Case "FR"
                '    Return 2
                'Case "HU"
                '    Return 5
                'Case "IT"
                '    Return 6
                'Case "RO"
                '    Return 7
            Case "AL"
                Return 2
                'Case "TR"
                '    Return 8
            Case Else
                Return 0

        End Select
    End Function


    Protected Sub LoadLAG()


        Try

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub

   
    Private Sub SetLanguageLinks()
        Try


            Dim url As String = Request.Url.AbsolutePath
            url = url & "?"

            For Each itm As String In Request.QueryString.AllKeys
                If (Not String.IsNullOrEmpty(itm) AndAlso itm.ToUpper() <> "LAG") Then
                    url = url & itm & "=" & Request.QueryString(itm) & "&"
                End If
            Next



            lnkEn.NavigateUrl = url & "lag=US"
            lnkGR.NavigateUrl = url & "lag=GR"
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub

    Protected Sub LoginStatus1_LoggingOut(sender As Object, e As System.Web.UI.WebControls.LoginCancelEventArgs) Handles LoginStatus1.LoggingOut
        Try

            clsCurrentContext.ClearSession(Session)
            'Dim url As String = "http://www.admincash.net"
            'If (Not String.IsNullOrEmpty(ConfigurationManager.AppSettings("RedirectURLOnLogout"))) Then
            '    url = ConfigurationManager.AppSettings("RedirectURLOnLogout")
            'End If
            Response.Redirect(ResolveUrl("~/Logout.aspx"))

        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub
End Class
﻿Imports Dating.Server.Core.DLL
Imports System.Data.SqlClient
Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Datasets.DLL.DSMembers

Public Class ReferrerMembersCredits
    Inherits ReferrerBasePage


#Region "Props"

    Dim _pageData As clsPageData
    Protected ReadOnly Property CurrentPageData As clsPageData
        Get
            If (_pageData Is Nothing) Then _pageData = New clsPageData(Context)
            Return _pageData
        End Get
    End Property

#End Region



    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        Response.StatusCode = 400
        Response.Redirect(ResolveUrl("~/ErrorPages/FileNotFound.aspx"))
    End Sub


    Protected Overrides Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Return

        Try
            If clsCurrentContext.VerifyLogin() = True Then
                Me.MasterPageFile = "~/Members/Members2.Master"
            Else
                clsCurrentContext.ClearSession(Session)
                Response.Redirect(ResolveUrl("~/Login.aspx"), False)
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pnlLoginErr.Visible = False

        Try
            If (Me.IsReferrer()) Then
                ' do nothing
            Else
                Response.Redirect("Referrer.aspx")
            End If

        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Try
            If (Not Me.IsPostBack) Then
                If (Request.UrlReferrer IsNot Nothing) Then
                    lnkBack.NavigateUrl = Request.UrlReferrer.ToString()
                Else
                    lnkBack.NavigateUrl = "~/Members/"
                End If
                LoadLAG()
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub



    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender

        Try
            Dim cPageBasic As Dating.Server.Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub



    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
        SetReferrerUI()
    End Sub


    Protected Sub LoadLAG()
        Try
            Dim cPageBasic As Dating.Server.Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            lblPageTitle.Text = CurrentPageData.GetCustomString("lblPageTitle")
            lnkBack.Text = CurrentPageData.GetCustomString("lnkBack")
            lblPageDesc.Text = CurrentPageData.GetCustomString("lblPageDesc")
            lblOnlineMembers.Text = CurrentPageData.GetCustomString("lblOnlineMembers")
            lblAvCredits.Text = CurrentPageData.GetCustomString("lblAvCredits")

            'lblRefer.Text = CurrentPageData.GetCustomString("lblRefer")
            'btnCheckAndActivate.Text = CurrentPageData.GetCustomString("btnCheckAndActivate")
            'lblWelcomeLogin.Text = CurrentPageData.GetCustomString("lblWelcomeLogin")
            'lnkLearnAbout.Text = CurrentPageData.GetCustomString("lnkLearnAbout")
            'lnkLearnAbout2.Text = CurrentPageData.GetCustomString("lnkLearnAbout2")
            'lnkEstimateCenter.Text = CurrentPageData.GetCustomString("lnkEstimateCenter")
            'lnkEstimateCenter2.Text = CurrentPageData.GetCustomString("lnkEstimateCenter2")
            'lblYourCode.Text = CurrentPageData.GetCustomString("lblYourCode")
            'lblYourCodeDesc.Text = CurrentPageData.GetCustomString("lblYourCodeDesc")
            'lblBalance.Text = CurrentPageData.GetCustomString("lblBalance")
            'lblCredits.Text = CurrentPageData.GetCustomString("lblCredits")
            'lblAvailableBalanceText.Text = CurrentPageData.GetCustomString("lblAvailableBalanceText")
            'lblAvailableBalance.Text = CurrentPageData.GetCustomString("lblAvailableBalance")
            'lblPendingBalanceText.Text = CurrentPageData.GetCustomString("lblPendingBalanceText")
            'lblPendingBalance.Text = CurrentPageData.GetCustomString("lblPendingBalance")
            'lblBalanceMoneyText.Text = CurrentPageData.GetCustomString("lblBalanceMoneyText")
            'lblBalanceMoney.Text = CurrentPageData.GetCustomString("lblBalanceMoney")
            'lblFamilyMembersDesc.Text = CurrentPageData.GetCustomString("lblFamilyMembersDesc")
            'lnkIncomeStats.Text = CurrentPageData.GetCustomString("lnkIncomeStats")
            'lblIncomeStatsDesc.Text = CurrentPageData.GetCustomString("lblIncomeStatsDesc")
            'lnkMyTree.Text = CurrentPageData.GetCustomString("lnkMyTree")
            'lblMyTreeDesc.Text = CurrentPageData.GetCustomString("lblMyTreeDesc")
            'lnkMyMoneyPerLvl.Text = CurrentPageData.GetCustomString("lnkMyMoneyPerLvl")
            'lblMyMoneyPerLvlDesc.Text = CurrentPageData.GetCustomString("lblMyMoneyPerLvlDesc")
            'lnkMyPercents.Text = CurrentPageData.GetCustomString("lnkMyPercents")
            'lblMyPercentsDesc.Text = CurrentPageData.GetCustomString("lblMyPercentsDesc")
            'lnkFamilyExch.Text = CurrentPageData.GetCustomString("lnkFamilyExch")
            'lblFamilyExchDesc.Text = CurrentPageData.GetCustomString("lblFamilyExchDesc")
            'lnkMyPayments.Text = CurrentPageData.GetCustomString("lnkMyPayments")
            'lblMyPaymentsDesc.Text = CurrentPageData.GetCustomString("lblMyPaymentsDesc")
            'lnkSettings.Text = CurrentPageData.GetCustomString("lnkSettings")
            'lblSettingsDesc.Text = CurrentPageData.GetCustomString("lblSettingsDesc")
            'lnkHelperAnswers.Text = CurrentPageData.GetCustomString("lnkHelperAnswers")
            'lblHelperAnswersDesc.Text = CurrentPageData.GetCustomString("lblHelperAnswersDesc")

            'lnkLearnAbout.NavigateUrl = CurrentPageData.GetCustomString("lnkLearnAbout.NavigateUrl")
            ''lnkHelperAnswers.NavigateUrl = CurrentPageData.GetCustomString("lnkHelperAnswers.NavigateUrl")


            ''lnkBalanceSheet.Text = CurrentPageData.GetCustomString("lnkBalanceSheet")
            ''lblBalanceSheetDesc.Text = CurrentPageData.GetCustomString("lblBalanceSheetDesc")


            'lblFromDate.Text = CurrentPageData.GetCustomString("lblFromDate")
            'lblToDate.Text = CurrentPageData.GetCustomString("lblToDate")
            'btnRefresh.Text = CurrentPageData.GetCustomString("btnRefresh")

            'lblReferrerContactText.Text = CurrentPageData.GetCustomString("lblReferrerContactText")


            lblItemsName.Text = Me.CurrentPageData.GetCustomString("CartReferrerView")
            'lnkViewDescription.Text = Me.CurrentPageData.GetCustomString("CartReferrerWhatIsIt")
            'lnkViewDescription.OnClientClick = OnMoreInfoClickFunc(
            '    lnkViewDescription.OnClientClick,
            '    ResolveUrl("~/Members/InfoWin.aspx?info=referrer"),
            '    Me.CurrentPageData.GetCustomString("CartReferrerView"))
            WhatIsIt1.PopupControl.HeaderText = Me.CurrentPageData.GetCustomString("CartReferrerView")


        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Dim dtFrom As DateTime?
    Dim dtTo As DateTime?

    Private Sub SetReferrerUI()
        SetDaysRange()
        GetReferrerCreditsObject(dtFrom, dtTo)
        LoadLAG()
    End Sub


    Private Sub SetDaysRange()
        If (Not Page.IsPostBack) Then
            If (SessionVariables.ReferrerCredits IsNot Nothing) Then
                Try
                    dtFrom = SessionVariables.ReferrerCredits.DateFrom
                    dtTo = SessionVariables.ReferrerCredits.DateTo
                Catch ex As Exception
                End Try
            End If

            If (dtFrom Is Nothing OrElse dtTo Is Nothing) Then
                dtFrom = DateTime.UtcNow.AddDays(-clsReferrer.DefaultDaysRange)
                dtTo = DateTime.UtcNow.AddDays(1)
            End If
        End If
    End Sub


    'Private Sub LoadData()
    '    lblWelcomeLogin.Text = lblWelcomeLogin.Text.Replace("[LOGINNAME]", Me.SessionVariables.DataRecordLoginMemberReturn.LoginName)
    '    lblYourCode2.Text = Me.MasterProfileId
    '    lblTotalCredits.Text = affCredits.CommissionCreditsTotal.ToString("0.00")
    '    lblFamilyMembersDesc.Text = lblFamilyMembersDesc.Text.Replace("[123456]", affCredits.FamilyMembers)
    '    lblLevel1.Text = Me.CurrentPageData.GetCustomString("lblLevel1")
    '    lblLevel2.Text = Me.CurrentPageData.GetCustomString("lblLevel2")
    '    lblLevel3.Text = Me.CurrentPageData.GetCustomString("lblLevel3")
    '    lblLevel4.Text = Me.CurrentPageData.GetCustomString("lblLevel4")
    '    lblLevel5.Text = Me.CurrentPageData.GetCustomString("lblLevel5")

    '    lblLevel1.Visible = False
    '    lblLevel2.Visible = False
    '    lblLevel3.Visible = False
    '    lblLevel4.Visible = False
    '    lblLevel5.Visible = False

    '    Dim sb As New StringBuilder()
    '    Dim drs As DataRow() = affCredits.GetReferrersForLevel(1)
    '    If (drs.Length > 0) Then
    '        sb.Append(" L1:" & drs.Length & ",")
    '        lblLevel1.Text = lblLevel1.Text.Replace("[123456]", drs.Length)
    '        lblLevel1.Visible = True

    '        drs = affCredits.GetReferrersForLevel(2)
    '        If (drs.Length > 0) Then
    '            sb.Append(" L2:" & drs.Length & ",")
    '            lblLevel2.Text = lblLevel2.Text.Replace("[123456]", drs.Length)
    '            lblLevel2.Visible = True

    '            drs = affCredits.GetReferrersForLevel(3)
    '            If (drs.Length > 0) Then
    '                sb.Append(" L3:" & drs.Length & ",")
    '                lblLevel3.Text = lblLevel3.Text.Replace("[123456]", drs.Length)
    '                lblLevel3.Visible = True

    '                drs = affCredits.GetReferrersForLevel(4)
    '                If (drs.Length > 0) Then
    '                    sb.Append(" L4:" & drs.Length & ",")
    '                    lblLevel4.Text = lblLevel4.Text.Replace("[123456]", drs.Length)
    '                    lblLevel4.Visible = True

    '                    drs = affCredits.GetReferrersForLevel(5)
    '                    If (drs.Length > 0) Then
    '                        sb.Append(" L5:" & drs.Length & ",")
    '                        lblLevel5.Text = lblLevel5.Text.Replace("[123456]", drs.Length)
    '                        lblLevel5.Visible = True

    '                    End If
    '                End If
    '            End If
    '        End If
    '    End If

    '    If (sb.Length > 0) Then
    '        sb.Remove(sb.Length - 1, 1)
    '        sb.Append(" ")
    '        lblFamilyMembersDesc.Text = lblFamilyMembersDesc.Text.Replace("[MEMBERSPERLEVEL]", sb.ToString())
    '    End If

    '    lblAvailableBalance.Text = lblAvailableBalance.Text.Replace("[CREDITS]", affCredits.CommissionCreditsAvailable)
    '    lblPendingBalance.Text = lblPendingBalance.Text.Replace("[CREDITS]", affCredits.CommissionCreditsPending)
    '    lblBalanceMoney.Text = lblBalanceMoney.Text.Replace("[MONEY]", affCredits.MoneyTotal.ToString("0.00"))

    'End Sub


    'Private Sub BindBalanceGrid()

    '    Dim dt As New DataTable()
    '    dt.Columns.Add("Currency")
    '    dt.Columns.Add("Available")
    '    dt.Columns.Add("Pending")
    '    dt.Columns.Add("Total")

    '    'Dim dr As DataRow = dt.NewRow()
    '    'dr("Currency") = "Credits"
    '    'dr("Available") = affCredits.CommissionCreditsAvailable & " CRD"
    '    'dr("Pending") = affCredits.CommissionCreditsPending & " CRD"
    '    'dr("Total") = affCredits.CommissionCreditsTotal & " CRD"
    '    'dt.Rows.Add(dr)

    '    Dim dr2 As DataRow = dt.NewRow()
    '    dr2("Currency") = "EURO"
    '    dr2("Available") = affCredits.MoneyAvailable.ToString("0.00") & " &euro;"
    '    dr2("Pending") = affCredits.MoneyPending.ToString("0.00") & " &euro;"
    '    dr2("Total") = affCredits.MoneyTotal.ToString("0.00") & " &euro;"
    '    dt.Rows.Add(dr2)


    '    gvBalance.DataSource = dt
    '    gvBalance.DataBind()

    'End Sub


    'Protected Sub mvMain_ActiveViewChanged(sender As Object, e As EventArgs) Handles mvMain.ActiveViewChanged
    '    Dim actView As View = mvMain.GetActiveView()
    '    If (actView IsNot Nothing AndAlso actView.Equals(vwPyramide)) Then
    '        SetReferrerUI()
    '    End If
    'End Sub




    'Protected Sub btnRefresh_Click(sender As Object, e As EventArgs) Handles btnRefresh.Click
    '    Try

    '        LoadLAG()
    '        SetReferrerUI()

    '    Catch ex As Exception
    '        WebErrorMessageBox(Me, ex, "btnGo")
    '    End Try
    'End Sub

    'Protected Sub odsMembersOnline_Selecting(sender As Object, e As System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs) Handles odsMembersOnline.Selecting

    '    Dim mins As Integer = 20
    '    Dim config As New clsConfigValues()
    '    If (Not Integer.TryParse(config.members_online_minutes, mins)) Then mins = 20
    '    Dim utcDate As DateTime = Date.UtcNow.AddMinutes(-mins)
    '    e.InputParameters("LastActivityUTCDate") = utcDate

    'End Sub

    Protected Sub odsAvailableCredits_Selecting(sender As Object, e As System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs) Handles odsAvailableCredits.Selecting
        Dim mins As Integer = 20
        Dim config As New clsConfigValues()
        If (Not Integer.TryParse(config.members_online_minutes, mins)) Then mins = 20
        Dim utcDate As DateTime = Date.UtcNow.AddMinutes(-mins)
        e.InputParameters("LastActivityUTCDate") = utcDate
    End Sub

    'Protected Sub gvOnline_CustomColumnDisplayText(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewColumnDisplayTextEventArgs) Handles gvOnline.CustomColumnDisplayText
    '    Try
    '        If e.Column.FieldName = "LastActivityDateTime" Then
    '            Dim dr As System.Data.DataRow = gvOnline.GetDataRow(e.VisibleRowIndex)
    '            If (dr IsNot Nothing) Then
    '                Dim format As String = DirectCast(gvOnline.Columns("LastActivityDateTime"), DevExpress.Web.ASPxGridView.GridViewDataDateColumn).PropertiesDateEdit.DisplayFormatString
    '                Dim d As DateTime = dr("LastActivityDateTime")
    '                e.DisplayText = d.ToLocalTime().ToString(format)
    '            End If

    '        End If

    '    Catch ex As Exception
    '        WebErrorMessageBox(Me, ex, "")
    '    End Try
    'End Sub

    Protected Sub gvAvCredits_DataBound(sender As Object, e As EventArgs) Handles gvAvCredits.DataBound
        lblAvCredits.Text = lblAvCredits.Text.Replace("[123456]", gvAvCredits.VisibleRowCount)

    End Sub

    'Protected Sub gvOnline_CustomUnboundColumnData(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewColumnDataEventArgs) Handles gvOnline.CustomUnboundColumnData
    '    Try

    '        If e.IsGetData AndAlso e.Column.FieldName = "LastActivityDateTime" Then
    '            Dim dr As System.Data.DataRow = gvOnline.GetDataRow(e.ListSourceRowIndex)
    '            If (dr IsNot Nothing) Then
    '                ' Dim format As String = DirectCast(gvOnline.Columns("LastActivityDateTime"), DevExpress.Web.ASPxGridView.GridViewDataDateColumn).PropertiesDateEdit.DisplayFormatString
    '                Dim d As DateTime = dr("LastActivityDateTime")
    '                e.Value = d.ToLocalTime()
    '            End If

    '        End If

    '    Catch ex As Exception
    '        WebErrorMessageBox(Me, ex, "")
    '    End Try
    'End Sub

    'Protected Sub gvOnline_DataBound(sender As Object, e As EventArgs) Handles gvOnline.DataBound
    '    lblOnlineMembers.Text = lblOnlineMembers.Text.Replace("[123456]", gvOnline.VisibleRowCount)
    'End Sub

    Protected Sub odsAvailableCredits_Selected(sender As Object, e As System.Web.UI.WebControls.ObjectDataSourceStatusEventArgs) Handles odsAvailableCredits.Selected

        If (e.ReturnValue IsNot Nothing) Then
            Dim dt As Dating.Server.Datasets.DLL.DSReferrers.CustomerPaidCreditsAdmin_ForReferrerDataTable = e.ReturnValue
            Dim drs As DataRow() = dt.Select("IsOnline=1")
            lblOnlineMembers.Text = lblOnlineMembers.Text.Replace("[123456]", drs.Count)
        End If

    End Sub



    Public Function GetCustomerPaidCreditsAdmin(ReturnMinimum_AvailableCredits As Int32?, ShowOnline As Boolean?, LastActivityUTCDate As DateTime?) As DSReferrers.CustomerPaidCreditsAdmin_ForReferrerDataTable
        Using ta As New DSReferrersTableAdapters.CustomerPaidCreditsAdmin_ForReferrerTableAdapter

            Using con As New SqlConnection(ModGlobals.ConnectionString)
                ta.Connection = New SqlConnection(ModGlobals.ConnectionString)
                Using dt = ta.GetData(ReturnMinimum_AvailableCredits, ShowOnline, LastActivityUTCDate, False)
                    Return dt
                End Using

            End Using
        End Using


    End Function


End Class
﻿Imports Dating.Server.Core.DLL

Public Class InfoWin
    Inherits BasePage

#Region "Props"


    Dim _pageData As clsPageData
    Protected ReadOnly Property CurrentPageData As clsPageData
        Get
            If (_pageData Is Nothing) Then _pageData = New clsPageData(Context)
            Return _pageData
        End Get
    End Property



    Private ReadOnly Property ReferrerIdParam As Integer
        Get
            Dim affiliateid As Integer
            If (Not String.IsNullOrEmpty(Request.QueryString("affid"))) Then
                Integer.TryParse(Request.QueryString("affid"), affiliateid)
            End If
            Return affiliateid
        End Get
    End Property

#End Region


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pnlErr.Visible = False

        Try
            If (Not Me.IsPostBack) Then
                LoadLAG()
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try


        Try

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
    End Sub


    Protected Sub LoadLAG()
        Try
            'TODO CMS
            Select Case Request.QueryString("info")
                Case "likes"
                    If (Me.IsMale) Then
                        lblInfo.Text = CurrentPageData.GetCustomString("MALE_LikesInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("MALE_LikesInfoTitle")

                    Else
                        lblInfo.Text = CurrentPageData.GetCustomString("FEMALE_LikesInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("FEMALE_LikesInfoTitle")

                    End If

                Case "offers"
                    If (Me.IsMale) Then
                        lblInfo.Text = CurrentPageData.GetCustomString("MALE_OffersInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("MALE_OffersInfoTitle")

                    Else
                        lblInfo.Text = CurrentPageData.GetCustomString("FEMALE_OffersInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("FEMALE_OffersInfoTitle")

                    End If

                Case "OffersNewCommands"
                    If (Me.IsMale) Then
                        lblInfo.Text = CurrentPageData.GetCustomString("MALE_OffersNewCommandsInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("MALE_OffersNewCommandsInfoTitle")

                    Else
                        lblInfo.Text = CurrentPageData.GetCustomString("FEMALE_OffersNewCommandsInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("FEMALE_OffersNewCommandsInfoTitle")

                    End If

                Case "OffersPendingCommands"
                    If (Me.IsMale) Then
                        lblInfo.Text = CurrentPageData.GetCustomString("MALE_OffersPendingCommandsInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("MALE_OffersPendingCommandsInfoTitle")

                    Else
                        lblInfo.Text = CurrentPageData.GetCustomString("FEMALE_OffersPendingCommandsInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("FEMALE_OffersPendingCommandsInfoTitle")

                    End If

                Case "dates"
                    If (Me.IsMale) Then
                        lblInfo.Text = CurrentPageData.GetCustomString("MALE_DatesInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("MALE_DatesInfoTitle")

                    Else
                        lblInfo.Text = CurrentPageData.GetCustomString("FEMALE_DatesInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("FEMALE_DatesInfoTitle")

                    End If

                Case "likescommands"
                    If (Me.IsMale) Then
                        lblInfo.Text = CurrentPageData.GetCustomString("MALE_LikesCommandsInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("MALE_LikesCommandsInfoTitle")

                    Else
                        lblInfo.Text = CurrentPageData.GetCustomString("FEMALE_LikesCommandsInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("FEMALE_LikesCommandsInfoTitle")

                    End If

                Case "LikesNewCommands"
                    If (Me.IsMale) Then
                        lblInfo.Text = CurrentPageData.GetCustomString("MALE_LikesCommandsInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("MALE_LikesCommandsInfoTitle")

                    Else
                        lblInfo.Text = CurrentPageData.GetCustomString("FEMALE_LikesCommandsInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("FEMALE_LikesCommandsInfoTitle")

                    End If

                Case "LikesPendingCommands"
                    If (Me.IsMale) Then
                        lblInfo.Text = CurrentPageData.GetCustomString("MALE_LikesPendingCommandsInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("MALE_LikesPendingCommandsInfoTitle")

                    Else
                        lblInfo.Text = CurrentPageData.GetCustomString("FEMALE_LikesPendingCommandsInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("FEMALE_LikesPendingCommandsInfoTitle")

                    End If

                Case "LikesRejectedCommands"
                    If (Me.IsMale) Then
                        lblInfo.Text = CurrentPageData.GetCustomString("MALE_LikesRejectedCommandsInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("MALE_LikesRejectedCommandsInfoTitle")

                    Else
                        lblInfo.Text = CurrentPageData.GetCustomString("FEMALE_LikesRejectedCommandsInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("FEMALE_LikesRejectedCommandsInfoTitle")

                    End If

                Case "LikesAcceptedCommands"
                    If (Me.IsMale) Then
                        lblInfo.Text = CurrentPageData.GetCustomString("MALE_LikesAcceptedCommandsInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("MALE_LikesAcceptedCommandsInfoTitle")

                    Else
                        lblInfo.Text = CurrentPageData.GetCustomString("FEMALE_LikesAcceptedCommandsInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("FEMALE_LikesAcceptedCommandsInfoTitle")

                    End If

                Case "search"
                    If (Me.IsMale) Then
                        lblInfo.Text = CurrentPageData.GetCustomString("MALE_SearchInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("MALE_SearchInfoTitle")

                    Else
                        lblInfo.Text = CurrentPageData.GetCustomString("FEMALE_SearchInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("FEMALE_SearchInfoTitle")

                    End If

                Case "messages"
                    If (Me.IsMale) Then
                        lblInfo.Text = CurrentPageData.GetCustomString("MALE_MessagesInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("MALE_MessagesInfoTitle")

                    Else
                        lblInfo.Text = CurrentPageData.GetCustomString("FEMALE_MessagesInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("FEMALE_MessagesInfoTitle")

                    End If

                Case "conversation"
                    If (Me.IsMale) Then
                        lblInfo.Text = CurrentPageData.GetCustomString("MALE_ConversationInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("MALE_ConversationInfoTitle")

                    Else
                        lblInfo.Text = CurrentPageData.GetCustomString("FEMALE_ConversationInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("FEMALE_ConversationInfoTitle")

                    End If

                Case "searchcommands"
                    If (Me.IsMale) Then
                        lblInfo.Text = CurrentPageData.GetCustomString("MALE_SearchCommandsInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("MALE_SearchCommandsInfoTitle")

                    Else
                        lblInfo.Text = CurrentPageData.GetCustomString("FEMALE_SearchCommandsInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("FEMALE_SearchCommandsInfoTitle")

                    End If

                Case "mylists"
                    If (Me.IsMale) Then
                        lblInfo.Text = CurrentPageData.GetCustomString("MALE_MyListsInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("MALE_MyListsInfoTitle")

                    Else
                        lblInfo.Text = CurrentPageData.GetCustomString("FEMALE_MyListsInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("FEMALE_MyListsInfoTitle")

                    End If

                Case "messagescommands"
                    If (Me.IsMale) Then
                        lblInfo.Text = CurrentPageData.GetCustomString("MALE_MessagesCommandsInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("MALE_MessagesCommandsInfoTitle")

                    Else
                        lblInfo.Text = CurrentPageData.GetCustomString("FEMALE_MessagesCommandsInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("FEMALE_MessagesCommandsInfoTitle")

                    End If

                Case "dashboard"
                    If (Me.IsMale) Then
                        lblInfo.Text = CurrentPageData.GetCustomString("MALE_DashboardInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("MALE_DashboardInfoTitle")

                    Else
                        lblInfo.Text = CurrentPageData.GetCustomString("FEMALE_DashboardInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("FEMALE_DashboardInfoTitle")

                    End If

                Case "settings"
                    If (Me.IsMale) Then
                        lblInfo.Text = CurrentPageData.GetCustomString("MALE_SettingsInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("MALE_SettingsInfoTitle")

                    Else
                        lblInfo.Text = CurrentPageData.GetCustomString("FEMALE_SettingsInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("FEMALE_SettingsInfoTitle")

                    End If

                Case "createoffer"
                    If (Me.IsMale) Then
                        lblInfo.Text = CurrentPageData.GetCustomString("MALE_CreateOfferInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("MALE_CreateOfferInfoTitle")

                    Else
                        lblInfo.Text = CurrentPageData.GetCustomString("FEMALE_CreateOfferInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("FEMALE_CreateOfferInfoTitle")

                    End If


                Case "selectproduct"
                    If (Me.IsMale) Then
                        lblInfo.Text = CurrentPageData.GetCustomString("MALE_SelectProductInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("MALE_SelectProductInfoTitle")

                    Else
                        lblInfo.Text = CurrentPageData.GetCustomString("FEMALE_SelectProductInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("FEMALE_SelectProductInfoTitle")

                    End If


                Case "referrer"
                    If (Me.IsMale) Then
                        lblInfo.Text = CurrentPageData.GetCustomString("MALE_ReferrerInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("MALE_ReferrerInfoTitle")

                    Else
                        lblInfo.Text = CurrentPageData.GetCustomString("FEMALE_ReferrerInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("FEMALE_ReferrerInfoTitle")

                    End If


                Case "referreranalytics"
                    If (Me.IsMale) Then
                        lblInfo.Text = CurrentPageData.GetCustomString("MALE_ReferrerAnalyticsInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("MALE_ReferrerAnalyticsInfoTitle")

                    Else
                        lblInfo.Text = CurrentPageData.GetCustomString("FEMALE_ReferrerAnalyticsInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("FEMALE_ReferrerAnalyticsInfoTitle")

                    End If


                Case "referrerbalancesheet"
                    If (Me.IsMale) Then
                        lblInfo.Text = CurrentPageData.GetCustomString("MALE_ReferrerBalanceSheetInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("MALE_ReferrerBalanceSheetInfoTitle")

                    Else
                        lblInfo.Text = CurrentPageData.GetCustomString("FEMALE_ReferrerBalanceSheetInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("FEMALE_ReferrerBalanceSheetInfoTitle")

                    End If

                Case "referrercart"
                    If (Me.IsMale) Then
                        lblInfo.Text = CurrentPageData.GetCustomString("MALE_ReferrerCartInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("MALE_ReferrerCartInfoTitle")

                    Else
                        lblInfo.Text = CurrentPageData.GetCustomString("FEMALE_ReferrerCartInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("FEMALE_ReferrerCartInfoTitle")

                    End If


                Case "referrerestimate"
                    If (Me.IsMale) Then
                        lblInfo.Text = CurrentPageData.GetCustomString("MALE_ReferrerEstimateInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("MALE_ReferrerEstimateInfoTitle")

                    Else
                        lblInfo.Text = CurrentPageData.GetCustomString("FEMALE_ReferrerEstimateInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("FEMALE_ReferrerEstimateInfoTitle")

                    End If

                Case "referrerfamilytree"
                    If (Me.IsMale) Then
                        lblInfo.Text = CurrentPageData.GetCustomString("MALE_ReferrerFamilyTreeInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("MALE_ReferrerFamilyTreeInfoTitle")

                    Else
                        lblInfo.Text = CurrentPageData.GetCustomString("FEMALE_ReferrerFamilyTreeInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("FEMALE_ReferrerFamilyTreeInfoTitle")

                    End If

                Case "referrerpayments"
                    If (Me.IsMale) Then
                        lblInfo.Text = CurrentPageData.GetCustomString("MALE_ReferrerPaymentsInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("MALE_ReferrerPaymentsInfoTitle")

                    Else
                        lblInfo.Text = CurrentPageData.GetCustomString("FEMALE_ReferrerPaymentsInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("FEMALE_ReferrerPaymentsInfoTitle")

                    End If

                Case "referrerpercents"
                    If (Me.IsMale) Then
                        lblInfo.Text = CurrentPageData.GetCustomString("MALE_ReferrerPercentsInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("MALE_ReferrerPercentsInfoTitle")

                    Else
                        lblInfo.Text = CurrentPageData.GetCustomString("FEMALE_ReferrerPercentsInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("FEMALE_ReferrerPercentsInfoTitle")

                    End If

                Case "referrersettings"
                    If (Me.IsMale) Then
                        lblInfo.Text = CurrentPageData.GetCustomString("MALE_ReferrerSettingsInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("MALE_ReferrerSettingsInfoTitle")

                    Else
                        lblInfo.Text = CurrentPageData.GetCustomString("FEMALE_ReferrerSettingsInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("FEMALE_ReferrerSettingsInfoTitle")

                    End If

                Case "referrerstatistics"
                    If (Me.IsMale) Then
                        lblInfo.Text = CurrentPageData.GetCustomString("MALE_ReferrerStatisticsInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("MALE_ReferrerStatisticsInfoTitle")

                    Else
                        lblInfo.Text = CurrentPageData.GetCustomString("FEMALE_ReferrerStatisticsInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("FEMALE_ReferrerStatisticsInfoTitle")

                    End If

                Case "referrersupplies"
                    If (Me.IsMale) Then
                        lblInfo.Text = CurrentPageData.GetCustomString("MALE_ReferrerSuppliesInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("MALE_ReferrerSuppliesInfoTitle")

                    Else
                        lblInfo.Text = CurrentPageData.GetCustomString("FEMALE_ReferrerSuppliesInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("FEMALE_ReferrerSuppliesInfoTitle")

                    End If

                Case "report"
                    If (Me.IsMale) Then
                        lblInfo.Text = CurrentPageData.GetCustomString("MALE_ReportInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("MALE_ReportInfoTitle")

                    Else
                        lblInfo.Text = CurrentPageData.GetCustomString("FEMALE_ReportInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("FEMALE_ReportInfoTitle")

                    End If

                Case "selectpayment"
                    If (Me.IsMale) Then
                        lblInfo.Text = CurrentPageData.GetCustomString("MALE_SelectPaymentInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("MALE_SelectPaymentInfoTitle")

                    Else
                        lblInfo.Text = CurrentPageData.GetCustomString("FEMALE_SelectPaymentInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("FEMALE_SelectPaymentInfoTitle")

                    End If

                Case "voucher"
                    If (Me.IsMale) Then
                        lblInfo.Text = CurrentPageData.GetCustomString("MALE_VoucherInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("MALE_VoucherInfoTitle")

                    Else
                        lblInfo.Text = CurrentPageData.GetCustomString("FEMALE_VoucherInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("FEMALE_VoucherInfoTitle")

                    End If

                Case "profile"
                    If (Me.IsMale) Then
                        lblInfo.Text = CurrentPageData.GetCustomString("MALE_ProfileInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("MALE_ProfileInfoTitle")

                    Else
                        lblInfo.Text = CurrentPageData.GetCustomString("FEMALE_ProfileInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("FEMALE_ProfileInfoTitle")

                    End If

                Case "photos"
                    If (Me.IsMale) Then
                        lblInfo.Text = CurrentPageData.GetCustomString("MALE_PhotosInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("MALE_PhotosInfoTitle")

                    Else
                        lblInfo.Text = CurrentPageData.GetCustomString("FEMALE_PhotosInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("FEMALE_PhotosInfoTitle")

                    End If

                Case "datesunlimited"
                    If (Me.IsMale) Then
                        lblInfo.Text = CurrentPageData.GetCustomString("MALE_DatesUnlimitedInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("MALE_DatesUnlimitedInfoTitle")

                    Else
                        lblInfo.Text = CurrentPageData.GetCustomString("FEMALE_DatesUnlimitedInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("FEMALE_DatesUnlimitedInfoTitle")

                    End If

                Case "dateslimited"
                    If (Me.IsMale) Then
                        lblInfo.Text = CurrentPageData.GetCustomString("MALE_DatesLimitedInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("MALE_DatesLimitedInfoTitle")

                    Else
                        lblInfo.Text = CurrentPageData.GetCustomString("FEMALE_DatesLimitedInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("FEMALE_DatesLimitedInfoTitle")

                    End If

                Case "quicklinks"
                    If (Me.IsMale) Then
                        lblInfo.Text = CurrentPageData.GetCustomString("MALE_QuickLinksInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("MALE_QuickLinksInfoTitle")

                    Else
                        lblInfo.Text = CurrentPageData.GetCustomString("FEMALE_QuickLinksInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("FEMALE_QuickLinksInfoTitle")

                    End If

                Case "WhoFavoritedMeCommands"
                    If (Me.IsMale) Then
                        lblInfo.Text = CurrentPageData.GetCustomString("MALE_WhoFavoritedMeCommandsInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("MALE_WhoFavoritedMeCommandsInfoTitle")

                    Else
                        lblInfo.Text = CurrentPageData.GetCustomString("FEMALE_WhoFavoritedMeCommandsInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("FEMALE_WhoFavoritedMeCommandsInfoTitle")

                    End If

                Case "WhoSharedPhotosCommands"
                    If (Me.IsMale) Then
                        lblInfo.Text = CurrentPageData.GetCustomString("MALE_WhoSharedPhotosCommandsInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("MALE_WhoSharedPhotosCommandsInfoTitle")

                    Else
                        lblInfo.Text = CurrentPageData.GetCustomString("FEMALE_WhoSharedPhotosCommandsInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("FEMALE_WhoSharedPhotosCommandsInfoTitle")

                    End If

                Case "WhoViewedMeCommands"
                    If (Me.IsMale) Then
                        lblInfo.Text = CurrentPageData.GetCustomString("MALE_WhoViewedMeCommandsInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("MALE_WhoViewedMeCommandsInfoTitle")

                    Else
                        lblInfo.Text = CurrentPageData.GetCustomString("FEMALE_WhoViewedMeCommandsInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("FEMALE_WhoViewedMeCommandsInfoTitle")

                    End If

                Case "MyViewedListCommands"
                    If (Me.IsMale) Then
                        lblInfo.Text = CurrentPageData.GetCustomString("MALE_MyViewedListCommandsInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("MALE_MyViewedListCommandsInfoTitle")

                    Else
                        lblInfo.Text = CurrentPageData.GetCustomString("FEMALE_MyViewedListCommandsInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("FEMALE_MyViewedListCommandsInfoTitle")

                    End If

                Case "MyFavoriteListCommands"
                    If (Me.IsMale) Then
                        lblInfo.Text = CurrentPageData.GetCustomString("MALE_MyFavoriteListCommandsInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("MALE_MyFavoriteListCommandsInfoTitle")

                    Else
                        lblInfo.Text = CurrentPageData.GetCustomString("FEMALE_MyFavoriteListCommandsInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("FEMALE_MyFavoriteListCommandsInfoTitle")

                    End If

                Case "MyBlockedListCommands"
                    If (Me.IsMale) Then
                        lblInfo.Text = CurrentPageData.GetCustomString("MALE_MyBlockedListCommandsInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("MALE_MyBlockedListCommandsInfoTitle")

                    Else
                        lblInfo.Text = CurrentPageData.GetCustomString("FEMALE_MyBlockedListCommandsInfoDesc")
                        lblInfoTitle.Text = CurrentPageData.GetCustomString("FEMALE_MyBlockedListCommandsInfoTitle")

                    End If

                Case "affiliatedashboard"
                    lblInfo.Text = CurrentPageData.GetCustomString("affiliatedashboardInfoDesc")
                    lblInfoTitle.Text = CurrentPageData.GetCustomString("affiliatedashboardInfoTitle")

            End Select

            Dim ln As String = Request.QueryString("ln")
            If (String.IsNullOrEmpty(Request.QueryString("ln"))) Then ln = ""

            lblInfoTitle.Text = ReplaceCommonTookens(lblInfoTitle.Text, ln)
            lblInfo.Text = ReplaceCommonTookens(lblInfo.Text, ln)

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender

        Try
            Dim cPageBasic As Dating.Server.Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            GeneralFunctions.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub



End Class
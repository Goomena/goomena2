﻿Imports Dating.Server.Core.DLL
Imports ReferralsDLL = Dating.Referrals.Server.Core.DLL
Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Datasets.DLL.DSMembers

Public Class Referrer2
    Inherits ReferrerBasePage


#Region "Props"

    Dim _pageData As clsPageData
    Protected ReadOnly Property CurrentPageData As clsPageData
        Get
            If (_pageData Is Nothing) Then _pageData = New clsPageData("~/Members/Referrer.aspx", Context)
            Return _pageData
        End Get
    End Property

#End Region
    Private affCredits As clsReferrerCredits



    Protected Overrides Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

        Try
            If clsCurrentContext.VerifyLogin() = True Then
                Me.MasterPageFile = "~/Members/Members2.Master"
            Else
                clsCurrentContext.ClearSession(Session)
                Response.Redirect(ResolveUrl("~/Login.aspx"), False)
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pnlLoginErr.Visible = False

        ' 
        Try
            If (Not Me.IsReferrer()) Then
                Dim isapproved As Boolean = ReferralsDLL.DataHelpers.REF_ReferrerProfiles_CheckIfApproved(Me.ReferrerCustomerID)
                If (isapproved) Then
                    Dim dt As ReferralsDLL.DSReferrers.REF_ReferrerProfilesDataTable =
                        ReferralsDLL.DataHelpers.GetREF_ReferrerProfiles_ByCustomerID(Me.ReferrerCustomerID).REF_ReferrerProfiles
                    If (dt.Rows.Count > 0) Then
                        Dim dr As ReferralsDLL.DSReferrers.REF_ReferrerProfilesRow = dt.Rows(0)
                        gLogin.PerfomSiteLogin(dr.LoginName, dr.Password, False, True)
                    End If
                End If
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Try
            If (Not Me.IsPostBack) Then
                If (Request.UrlReferrer IsNot Nothing) Then
                    lnkBack.NavigateUrl = Request.UrlReferrer.ToString()
                Else
                    lnkBack.NavigateUrl = "~/Members/"
                End If
                LoadLAG()
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try


        Try
            If (Me.IsReferrer()) Then
                'lblReferrerTitle.Text = "Επισκόπηση συνεργασίας"
                mvMain.SetActiveView(vwPyramide)
            Else
                'lblReferrerTitle.Text = "Αίτηση συνεργασίας"
                mvMain.SetActiveView(vwSetReferrerParent)

            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub



    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender

        Try
            Dim cPageBasic As Dating.Server.Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub



    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
        SetReferrerUI()
    End Sub


    Protected Sub LoadLAG()
        Try
            Dim cPageBasic As Dating.Server.Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            lblPageTitle.Text = CurrentPageData.GetCustomString("lblPageTitle")
            lnkBack.Text = CurrentPageData.GetCustomString("lnkBack")


            'lblRefer.Text = CurrentPageData.GetCustomString("lblRefer")
            'btnCheckAndActivate.Text = CurrentPageData.GetCustomString("btnCheckAndActivate")
            lblWelcomeLogin.Text = CurrentPageData.GetCustomString("lblWelcomeLogin")
            lnkLearnAbout.Text = CurrentPageData.GetCustomString("lnkLearnAbout")
            lnkLearnAbout2.Text = CurrentPageData.GetCustomString("lnkLearnAbout2")
            lnkEstimateCenter.Text = CurrentPageData.GetCustomString("lnkEstimateCenter")
            lnkEstimateCenter2.Text = CurrentPageData.GetCustomString("lnkEstimateCenter2")
            lblYourCode.Text = CurrentPageData.GetCustomString("lblYourCode")
            lblYourCodeDesc.Text = CurrentPageData.GetCustomString("lblYourCodeDesc")
            lblBalance.Text = CurrentPageData.GetCustomString("lblBalance")
            'lblCredits.Text = CurrentPageData.GetCustomString("lblCredits")
            lblAvailableBalanceText.Text = CurrentPageData.GetCustomString("lblAvailableBalanceText")
            lblAvailableBalance.Text = CurrentPageData.GetCustomString("lblAvailableBalance")
            lblPendingBalanceText.Text = CurrentPageData.GetCustomString("lblPendingBalanceText")
            lblPendingBalance.Text = CurrentPageData.GetCustomString("lblPendingBalance")
            lblBalanceMoneyText.Text = CurrentPageData.GetCustomString("lblBalanceMoneyText")
            lblBalanceMoney.Text = CurrentPageData.GetCustomString("lblBalanceMoney")
            lblFamilyMembersDesc.Text = CurrentPageData.GetCustomString("lblFamilyMembersDesc")

            lblPayedMoneyText.Text = CurrentPageData.GetCustomString("lblPayedMoneyText")
            lblPayedMoney.Text = CurrentPageData.GetCustomString("lblPayedMoney")

            lblReferrerContact_AfterPhoto_Text.Text = CurrentPageData.GetCustomString("lblReferrerContact_AfterPhoto_Text")
            'lblIncomeStatsDesc.Text = CurrentPageData.GetCustomString("lblIncomeStatsDesc")
            'lnkMyTree.Text = CurrentPageData.GetCustomString("lnkMyTree")
            'lblMyTreeDesc.Text = CurrentPageData.GetCustomString("lblMyTreeDesc")
            'lnkMyMoneyPerLvl.Text = CurrentPageData.GetCustomString("lnkMyMoneyPerLvl")
            'lblMyMoneyPerLvlDesc.Text = CurrentPageData.GetCustomString("lblMyMoneyPerLvlDesc")
            'lnkMyPercents.Text = CurrentPageData.GetCustomString("lnkMyPercents")
            'lblMyPercentsDesc.Text = CurrentPageData.GetCustomString("lblMyPercentsDesc")
            'lnkFamilyExch.Text = CurrentPageData.GetCustomString("lnkFamilyExch")
            'lblFamilyExchDesc.Text = CurrentPageData.GetCustomString("lblFamilyExchDesc")
            'lnkMyPayments.Text = CurrentPageData.GetCustomString("lnkMyPayments")
            'lblMyPaymentsDesc.Text = CurrentPageData.GetCustomString("lblMyPaymentsDesc")
            'lnkSettings.Text = CurrentPageData.GetCustomString("lnkSettings")
            'lblSettingsDesc.Text = CurrentPageData.GetCustomString("lblSettingsDesc")
            'lnkHelperAnswers.Text = CurrentPageData.GetCustomString("lnkHelperAnswers")
            'lblHelperAnswersDesc.Text = CurrentPageData.GetCustomString("lblHelperAnswersDesc")

            lnkLearnAbout.NavigateUrl = CurrentPageData.GetCustomString("lnkLearnAbout.NavigateUrl")
            'lnkHelperAnswers.NavigateUrl = CurrentPageData.GetCustomString("lnkHelperAnswers.NavigateUrl")


            'lnkBalanceSheet.Text = CurrentPageData.GetCustomString("lnkBalanceSheet")
            'lblBalanceSheetDesc.Text = CurrentPageData.GetCustomString("lblBalanceSheetDesc")


            lblFromDate.Text = CurrentPageData.GetCustomString("lblFromDate")
            lblToDate.Text = CurrentPageData.GetCustomString("lblToDate")
            btnRefresh.Text = CurrentPageData.GetCustomString("btnRefresh")

            lblReferrerContactText.Text = CurrentPageData.GetCustomString("lblReferrerContactText")

            SetControlsValue(Me, CurrentPageData)

            lblItemsName.Text = Me.CurrentPageData.GetCustomString("CartReferrerView")
            'lnkViewDescription.Text = Me.CurrentPageData.GetCustomString("CartReferrerWhatIsIt")
            'lnkViewDescription.OnClientClick = OnMoreInfoClickFunc(
            '    lnkViewDescription.OnClientClick,
            '    ResolveUrl("~/Members/InfoWin.aspx?info=referrer"),
            '    Me.CurrentPageData.GetCustomString("CartReferrerView"))
            WhatIsIt1.PopupControl.HeaderText = Me.CurrentPageData.GetCustomString("CartReferrerView")


            gvBalance.Columns("Currency").Caption = CurrentPageData.GetCustomString("BalanceTable-Currency")
            gvBalance.Columns("Available").Caption = CurrentPageData.GetCustomString("BalanceTable-Available")
            gvBalance.Columns("Pending").Caption = CurrentPageData.GetCustomString("BalanceTable-Pending")
            gvBalance.Columns("Total").Caption = CurrentPageData.GetCustomString("BalanceTable-Total")

            lblBalanceTableDescr.Text = CurrentPageData.GetCustomString("lblBalanceTableDescr")


            'lblRefApproveNotif.Text = CurrentPageData.GetCustomString("lblRefApproveNotif_Complete")
            lblRefApproveNotifBody.Text = CurrentPageData.GetCustomString("lblRefApproveNotifBody_CompleteNew")
            lblRefApproveNotif.Text = CurrentPageData.GetCustomString("lblRefApproveNotif_CompleteNew")
            If (Me.IsReferrer()) Then
                lblRefApproveNotif.Text = ""

                Dim ds As DSCustomer = DataHelpers.EUS_CustomerVerificationDocs_GetByCustomerID(Me.MasterProfileId)
                If (ds.EUS_CustomerVerificationDocs.Rows.Count > 0) Then
                    Dim drs As DSCustomer.EUS_CustomerVerificationDocsRow() = ds.EUS_CustomerVerificationDocs.Select("DocumentType='IDFrontPage' or DocumentType='IDBackPage' or DocumentType='IDThirdPage'")

                    If (drs.Length > 0) Then
                        Session("SuccesStep") = 3
                        'lblRefApproveNotif.Text = CurrentPageData.GetCustomString("lblRefApproveNotif_Complete")
                        lblRefApproveNotifBody.Text = CurrentPageData.GetCustomString("lblRefApproveNotifBody_CompleteNew")
                        lblRefApproveNotif.Text = CurrentPageData.GetCustomString("lblRefApproveNotif_CompleteNew")
                    End If
                End If

                If (lblRefApproveNotif.Text = "") Then
                    lblRefApproveNotifBody.Text = CurrentPageData.GetCustomString("lblRefApproveNotifBody_PendingNew")
                    lblRefApproveNotif.Text = CurrentPageData.GetCustomString("lblRefApproveNotif_PendingNew")
                End If
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Private Sub SetReferrerUI()
        SetDaysRange()
        affCredits = GetReferrerCreditsObject(dtFrom.Value, dtTo.Value)
        LoadData()
        BindBalanceGrid()
    End Sub

    Protected Overrides Function GetReferrerCreditsObject(dateFrom As DateTime?, dateTo As DateTime?)
        If (affCredits Is Nothing) Then

            Try
                'If (SessionVariables.ReferrerCredits IsNot Nothing) Then
                '    affCredits = SessionVariables.ReferrerCredits


                '    If (affCredits.DateCreated.AddHours(1) < Date.UtcNow) Then
                '        ' if time expired than clear object
                '        affCredits = Nothing

                '    ElseIf (affCredits.DateFrom <> dateFrom OrElse affCredits.DateTo <> dateTo) Then
                '        ' if dates range is different than clear object
                '        affCredits = Nothing

                '    End If
                'End If


                If (affCredits Is Nothing) Then
                    Dim _dtFrom As DateTime?
                    Dim _dtTo As DateTime?
                    If (dateFrom.HasValue) Then _dtFrom = dateFrom.Value.Date
                    If (dateTo.HasValue) Then _dtTo = dateTo.Value.Date.AddDays(1).AddMilliseconds(-1)

                    affCredits = New clsReferrerCredits(Me.MasterProfileId, Me.SessionVariables.MemberData.LoginName, _dtFrom, _dtTo)
                    'affCredits = New clsReferrerCredits(Me.MasterProfileId, Me.SessionVariables.MemberData.LoginName, dateFrom, dateTo)
                    affCredits.Load()
                    'SessionVariables.ReferrerCredits = affCredits
                End If

            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try
        End If

        Return affCredits
    End Function


    Private Sub SetDaysRange()
        If (Not Page.IsPostBack) Then
            'If (SessionVariables.ReferrerCredits IsNot Nothing) Then
            '    Try
            '        dtFrom.Value = SessionVariables.ReferrerCredits.DateFrom
            '        dtTo.Value = SessionVariables.ReferrerCredits.DateTo
            '    Catch ex As Exception
            '    End Try
            'End If

            If (dtFrom.Value Is Nothing OrElse dtTo.Value Is Nothing) Then
                dtFrom.Date = New DateTime(2010, 1, 1)
                dtTo.Date = DateTime.UtcNow.AddDays(1)
            End If
        End If
    End Sub


    'Protected Sub btnSubmitReport_Click(sender As Object, e As EventArgs) Handles btnCheckAndActivate.Click
    '    Try
    '        Dim success As Boolean
    '        Dim customerId As Integer
    '        Dim customerLogin As String = ""

    '        If (txtParentLogin.Text.Trim() = "") Then
    '            pnlLoginErr.CssClass = "alert alert-danger"
    '            pnlLoginErr.Visible = True
    '            lblLoginErr.Text = CurrentPageData.GetCustomString("Err_ProvideLoginOrCode") ' "Παρακαλώ δώστε το login ονομα του χρήστη."
    '            Return
    '        End If

    '        If (txtParentLogin.Text.Trim() <> "") Then
    '            Dim r As New Regex(ModGlobals.Referrer_CODE_PATTERN_Check, RegexOptions.IgnoreCase)
    '            Dim m As Match = r.Match(txtParentLogin.Text)
    '            If (Not m.Success) Then
    '                pnlLoginErr.CssClass = "alert alert-danger"
    '                pnlLoginErr.Visible = True
    '                lblLoginErr.Text = CurrentPageData.GetCustomString("Err_ProvideLoginOrCode")
    '                Return
    '            End If
    '            customerId = m.Groups("customer").Value
    '            customerLogin = m.Groups("login").Value
    '        End If



    '        Dim prof As EUS_Profile = DataHelpers.GetEUS_ProfileByProfileID(Me.CMSDBDataContext, customerId)
    '        'If (prof Is Nothing) Then

    '        '    Dim profId As Integer
    '        '    If (Integer.TryParse(txtParentLogin.Text, profId)) Then
    '        '        prof = DataHelpers.GetEUS_ProfileByProfileID(Me.CMSDBDataContext, profId)
    '        '    End If

    '        '    If (prof Is Nothing) Then
    '        '        pnlLoginErr.CssClass = "alert alert-danger"
    '        '        pnlLoginErr.Visible = True
    '        '        lblLoginErr.Text = CurrentPageData.GetCustomString("Err_NonExistingCode") '"Μη αποδεκτός κωδικός. Παρακαλώ ελέγξτε...."
    '        '        Return
    '        '    End If
    '        'End If

    '        If (prof IsNot Nothing AndAlso (prof.LoginName.StartsWith(customerLogin) OrElse customerLogin.StartsWith(prof.LoginName))) Then

    '            'If (Me.GetCurrentProfile().ReferrerParentId > 0 AndAlso (IIf(prof.ReferrerParentId Is Nothing, 0, prof.ReferrerParentId) = 0)) Then

    '            If ((IIf(Me.GetCurrentProfile().ReferrerParentId Is Nothing, 0, Me.GetCurrentProfile().ReferrerParentId) = 0) AndAlso _
    '                (prof.ReferrerParentId > 0)) Then
    '                'Me.GetCurrentProfile().ReferrerParentId = prof.ProfileID
    '                'prof.ReferrerParentId = Me.MasterProfileId
    '                'Me.CMSDBDataContext.SubmitChanges()

    '                DataHelpers.UpdateEUS_Profiles_ReferrerParentId(Me.MasterProfileId, prof.ProfileID)

    '                pnlLoginErr.CssClass = "alert alert-success"
    '                pnlLoginErr.Visible = True
    '                lblLoginErr.Text = CurrentPageData.GetCustomString("Success_ReferralConnected") '"Συνδέθηκε επιτυχώς με το χρήστη <strong>" & txtParentLogin.Text & "</strong>."
    '                lblLoginErr.Text = lblLoginErr.Text.Replace("[REFERLOGIN]", txtParentLogin.Text)

    '                success = True
    '            Else
    '                pnlLoginErr.CssClass = "alert alert-danger"
    '                pnlLoginErr.Visible = True
    '                lblLoginErr.Text = CurrentPageData.GetCustomString("Err_NonReferrerCode") '"Λάθος κωδικός συνεργασίας. Παρακαλώ ελέγξτε...."
    '            End If
    '        End If


    '        If (success) Then
    '            mvMain.SetActiveView(vwPyramide)
    '        End If

    '    Catch ex As Exception
    '        WebErrorMessageBox(Me, ex, "Page_Load")
    '    End Try
    'End Sub


    Private Sub LoadData()

        Dim amount As Dictionary(Of String, Object) = affCredits.GetTotalPayment(Me.MasterProfileId)

        Dim LastPaymentAmount As Double = amount("LastPaymentAmount")
        Dim TotalDebitAmount As Double = amount("TotalDebitAmount")
        Dim totalMoneyToBePaid As Double = affCredits.CommissionCreditsTotal - TotalDebitAmount
        Dim AvailableAmount As Double = affCredits.CommissionCreditsAvailable - TotalDebitAmount
        Dim PendingAmount As Double = affCredits.CommissionCreditsPending
        If (AvailableAmount < 0) Then
            PendingAmount = PendingAmount + AvailableAmount
            AvailableAmount = 0
        End If

        lblYourCode2.Text = CreateReferrerCode() ' Me.MasterProfileId
        lblWelcomeLogin.Text = lblWelcomeLogin.Text.Replace("[LOGINNAME]", lblYourCode2.Text)
        'lblWelcomeLogin.Text = lblWelcomeLogin.Text.Replace("[LOGINNAME]", Me.SessionVariables.MemberData.LoginName)
        lblTotalCredits.Text = totalMoneyToBePaid.ToString("0.00")
        lblFamilyMembersDesc.Text = lblFamilyMembersDesc.Text.Replace("[123456]", affCredits.FamilyMembers)

        lblLevel1.Text = Me.CurrentPageData.GetCustomString("lblLevel1")
        lblLevel2.Text = Me.CurrentPageData.GetCustomString("lblLevel2")
        lblLevel3.Text = Me.CurrentPageData.GetCustomString("lblLevel3")
        lblLevel4.Text = Me.CurrentPageData.GetCustomString("lblLevel4")
        lblLevel5.Text = Me.CurrentPageData.GetCustomString("lblLevel5")

        lblLevel1.Visible = False
        lblLevel2.Visible = False
        lblLevel3.Visible = False
        lblLevel4.Visible = False
        lblLevel5.Visible = False

        Dim sb As New StringBuilder()
        Dim drs As DSCustom.ReferrersRepositoryDataRow() = affCredits.GetReferrersForLevel(1)
        If (drs.Length > 0) Then
            sb.Append(" L1:" & drs.Length & ",")
            lblLevel1.Text = lblLevel1.Text.Replace("[123456]", drs.Length)
            lblLevel1.Visible = True

            drs = affCredits.GetReferrersForLevel(2)
            If (drs.Length > 0) Then
                sb.Append(" L2:" & drs.Length & ",")
                lblLevel2.Text = lblLevel2.Text.Replace("[123456]", drs.Length)
                lblLevel2.Visible = True

                drs = affCredits.GetReferrersForLevel(3)
                If (drs.Length > 0) Then
                    sb.Append(" L3:" & drs.Length & ",")
                    lblLevel3.Text = lblLevel3.Text.Replace("[123456]", drs.Length)
                    lblLevel3.Visible = True

                    drs = affCredits.GetReferrersForLevel(4)
                    If (drs.Length > 0) Then
                        sb.Append(" L4:" & drs.Length & ",")
                        lblLevel4.Text = lblLevel4.Text.Replace("[123456]", drs.Length)
                        lblLevel4.Visible = True

                        drs = affCredits.GetReferrersForLevel(5)
                        If (drs.Length > 0) Then
                            sb.Append(" L5:" & drs.Length & ",")
                            lblLevel5.Text = lblLevel5.Text.Replace("[123456]", drs.Length)
                            lblLevel5.Visible = True

                        End If
                    End If
                End If
            End If
        End If

        If (sb.Length > 0) Then
            sb.Remove(sb.Length - 1, 1)
            sb.Append(" ")
            lblFamilyMembersDesc.Text = lblFamilyMembersDesc.Text.Replace("[MEMBERSPERLEVEL]", sb.ToString())
        End If


        lblAvailableBalance.Text = lblAvailableBalance.Text.Replace("[CREDITS]", AvailableAmount.ToString("0.00")).Replace("EUR", "")
        lblPendingBalance.Text = lblPendingBalance.Text.Replace("[CREDITS]", PendingAmount.ToString("0.00")).Replace("EUR", "")
        lblBalanceMoney.Text = lblBalanceMoney.Text.Replace("[MONEY]", totalMoneyToBePaid.ToString("0.00")).Replace("EUR", "")
        lblPayedMoney.Text = lblPayedMoney.Text.Replace("[MONEY]", LastPaymentAmount.ToString("0.00")).Replace("EUR", "")
        If (amount("LastPaymentDate") > Date.MinValue) Then
            lnkPayouts.Visible = True
            lblPayedMoneyText.Text = lblPayedMoneyText.Text.Replace("[DATE]", CType(amount("LastPaymentDate"), DateTime).ToString("dd/MM/yyyy HH:mm"))
        Else
            lnkPayouts.Visible = False
        End If

        Dim REF_RegStep As Integer = DataHelpers.EUS_Profiles_GetREF_RegStep(Me.MasterProfileId)
        If (REF_RegStep = 1) Then
            Session("SuccesStep") = 1
            lnkRegStep2.Visible = True
            lnkRegStep3.Visible = True
        ElseIf (REF_RegStep = 2) Then
            Session("SuccesStep") = 2
            lnkRegStep3.Visible = True
        End If

    End Sub


    Private Sub BindBalanceGrid()

        Using dt As New DataTable()


            dt.Columns.Add("Currency")
            dt.Columns.Add("Available")
            dt.Columns.Add("Pending")
            dt.Columns.Add("Total")

            Dim dr2 As DataRow = dt.NewRow()
            dr2("Currency") = "EURO"
            dr2("Available") = affCredits.MoneyAvailable.ToString("0.00") '& " &euro;"
            dr2("Pending") = affCredits.MoneyPending.ToString("0.00") '& " &euro;"
            dr2("Total") = affCredits.MoneyTotal.ToString("0.00") '& " &euro;"
            dt.Rows.Add(dr2)


            gvBalance.DataSource = dt
            gvBalance.DataBind()
        End Using
    End Sub


    Protected Sub mvMain_ActiveViewChanged(sender As Object, e As EventArgs) Handles mvMain.ActiveViewChanged
        Dim actView As View = mvMain.GetActiveView()
        If (actView IsNot Nothing AndAlso actView.Equals(vwPyramide)) Then
            SetReferrerUI()
        ElseIf (actView IsNot Nothing AndAlso actView.Equals(vwSetReferrerParent)) Then
            pnlTop.Visible = False

        End If
    End Sub




    Protected Sub btnRefresh_Click(sender As Object, e As EventArgs) Handles btnRefresh.Click
        Try

            LoadLAG()
            SetReferrerUI()

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "btnGo")
        End Try
    End Sub

    'Protected Sub odsMembersOnline_Selecting(sender As Object, e As System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs) Handles odsMembersOnline.Selecting

    '    Dim mins As Integer = 20
    '    Dim config As New clsConfigValues()
    '    If (Not Integer.TryParse(config.members_online_minutes, mins)) Then mins = 20
    '    Dim utcDate As DateTime = Date.UtcNow.AddMinutes(-mins)
    '    e.InputParameters("LastActivityUTCDate") = utcDate

    'End Sub

    Protected Sub odsAvailableCredits_Selecting(sender As Object, e As System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs) Handles odsAvailableCredits.Selecting
        Dim mins As Integer = 20
        Dim config As New clsConfigValues()
        If (Not Integer.TryParse(config.members_online_minutes, mins)) Then mins = 20
        Dim utcDate As DateTime = Date.UtcNow.AddMinutes(-mins)
        e.InputParameters("LastActivityUTCDate") = utcDate
    End Sub

    'Protected Sub gvOnline_CustomColumnDisplayText(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewColumnDisplayTextEventArgs) Handles gvOnline.CustomColumnDisplayText
    '    Try
    '        If e.Column.FieldName = "LastActivityDateTime" Then
    '            Dim dr As System.Data.DataRow = gvOnline.GetDataRow(e.VisibleRowIndex)
    '            If (dr IsNot Nothing) Then
    '                Dim format As String = DirectCast(gvOnline.Columns("LastActivityDateTime"), DevExpress.Web.ASPxGridView.GridViewDataDateColumn).PropertiesDateEdit.DisplayFormatString
    '                Dim d As DateTime = dr("LastActivityDateTime")
    '                e.DisplayText = d.ToLocalTime().ToString(format)
    '            End If

    '        End If

    '    Catch ex As Exception
    '        WebErrorMessageBox(Me, ex, "")
    '    End Try
    'End Sub

    'Protected Sub gvAvCredits_DataBound(sender As Object, e As EventArgs) Handles gvAvCredits.DataBound
    '    lblAvCredits.Text = lblAvCredits.Text.Replace("[123456]", gvAvCredits.VisibleRowCount)

    'End Sub

    'Protected Sub gvOnline_CustomUnboundColumnData(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewColumnDataEventArgs) Handles gvOnline.CustomUnboundColumnData
    '    Try

    '        If e.IsGetData AndAlso e.Column.FieldName = "LastActivityDateTime" Then
    '            Dim dr As System.Data.DataRow = gvOnline.GetDataRow(e.ListSourceRowIndex)
    '            If (dr IsNot Nothing) Then
    '                ' Dim format As String = DirectCast(gvOnline.Columns("LastActivityDateTime"), DevExpress.Web.ASPxGridView.GridViewDataDateColumn).PropertiesDateEdit.DisplayFormatString
    '                Dim d As DateTime = dr("LastActivityDateTime")
    '                e.Value = d.ToLocalTime()
    '            End If

    '        End If

    '    Catch ex As Exception
    '        WebErrorMessageBox(Me, ex, "")
    '    End Try
    'End Sub

    'Protected Sub gvOnline_DataBound(sender As Object, e As EventArgs) Handles gvOnline.DataBound
    '    lblOnlineMembers.Text = lblOnlineMembers.Text.Replace("[123456]", gvOnline.VisibleRowCount)
    'End Sub

    'Protected Sub odsAvailableCredits_Selected(sender As Object, e As System.Web.UI.WebControls.ObjectDataSourceStatusEventArgs) Handles odsAvailableCredits.Selected
    '    ' e.
    '    If (e.ReturnValue IsNot Nothing) Then
    '        Dim dt As Dating.Server.Core.DLL.DSMembers.CustomerAvailableCreditsAdminDataTable = e.ReturnValue
    '        Dim drs As DataRow() = dt.Select("IsOnline=1")
    '        lblOnlineMembers.Text = lblOnlineMembers.Text.Replace("[123456]", drs.Count)
    '    End If

    'End Sub
End Class
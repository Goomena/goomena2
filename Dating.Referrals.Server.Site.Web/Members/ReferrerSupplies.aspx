﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Members/Members2Inner.master" 
CodeBehind="ReferrerSupplies.aspx.vb" Inherits="Dating.Referrals.Server.Site.Web.ReferrerSupplies" %>

<%@ Register src="../UserControls/WhatIsIt.ascx" tagname="WhatIsIt" tagprefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" language="javascript">
    function OnMoreInfoClickRef(contentUrl, obj) {
        if (obj != null) popupAffiliateSupplies.SetPopupElementID(obj.id);
        popupAffiliateSupplies.SetContentUrl(contentUrl);
        popupAffiliateSupplies.Show();
    }

    jQuery(function ($) {
        $(window).ready(function () {
            //scrollToElem($('#<%=  lblProfilesListInfo.ClientID %>'), 160)
        });
    });
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">

            <div class="grid_3 top_sidebar" id="sidebar-outter">
                <div class="lfloat">
                    <h2 class="page-name"><asp:Literal ID="lblItemsName" runat="server" /></h2>
                    <h2 class="page-description"><asp:Label ID="lblPageTitle" runat="server" Text="Οι Προμήθειες μου ανά επίπεδο" /></h2>
                </div>
                <div class="page-back-btn">
                    <dx:ASPxHyperLink ID="lnkBack" runat="server" Text="Back" CssClass="btn" 
                        NavigateUrl="~/Members/Referrer.aspx">
                    </dx:ASPxHyperLink>
                </div>
                <div class="clear">
                </div>
            </div>

            <div class="t_wrap">
                <div class="send_msg t_to msg">
                    <asp:Panel ID="pnlErr" runat="server" CssClass="alert alert-danger" 
                        Visible="False" EnableViewState="False">
                        <asp:Label ID="lblErr" runat="server"></asp:Label>
                    </asp:Panel>

        <asp:Label ID="lblPageDesc" runat="server" Text=""></asp:Label>
        <br />
        <br />
<table cellpadding="5" cellspacing="1">
    <tr>
        <td>
            <asp:Label ID="lblFromDate" runat="server" Text="Από Ημ/νία : "></asp:Label></td>
        <td>
            <dx:ASPxDateEdit ID="dtFrom" runat="server" DisplayFormatString="dd/MM/yyyy" 
                Width="120px">
            </dx:ASPxDateEdit>
        </td>
        <td>
            <asp:Label ID="lblToDate" runat="server" Text="Εως "></asp:Label></td>
        <td><dx:ASPxDateEdit ID="dtTo" runat="server" DisplayFormatString="dd/MM/yyyy" 
                Width="120px">
            </dx:ASPxDateEdit></td>
        <%--<td>
            <asp:Label ID="lblLevel" runat="server" Text="Level: "></asp:Label></td>
        <td>
            <dx:ASPxComboBox ID="cmbLevel" runat="server" SelectedIndex="0" Width="120px">
                <Items>
                    <dx:ListEditItem Selected="True" Text="All" Value="-1" />
                    <dx:ListEditItem Text="Level 1" Value="1" />
                    <dx:ListEditItem Text="Level 2" Value="2" />
                    <dx:ListEditItem Text="Level 3" Value="3" />
                    <dx:ListEditItem Text="Level 4" Value="4" />
                    <dx:ListEditItem Text="Level 5" Value="5" />
                </Items>
            </dx:ASPxComboBox>
        </td>--%>
        <td>
            <dx:ASPxButton ID="btnRefresh" runat="server" Text="Refresh">
            </dx:ASPxButton>
        </td>
    </tr>
</table>
<br />
<br />
                    <asp:Panel ID="pnlPyramid" runat="server">
                        <dx:ASPxGridView ID="gvPyramid" runat="server" AutoGenerateColumns="False" 
                        Width="100%">
                        <TotalSummary>
                            <%--<dx:ASPxSummaryItem DisplayFormat="Sum=0.00 &euro;" FieldName="SalesCRD" 
                                ShowInColumn="colSalesCRD" SummaryType="Sum" />
                            <dx:ASPxSummaryItem DisplayFormat="Sum=0.00 &euro;" FieldName="CommissionCRD" 
                                ShowInColumn="colCommissionCRD" SummaryType="Sum" />--%>
                            <dx:ASPxSummaryItem DisplayFormat="Sum=0.00" FieldName="SalesCRD" 
                                ShowInColumn="colSalesCRD" SummaryType="Sum" />
                            <dx:ASPxSummaryItem DisplayFormat="Sum=0.00" FieldName="CommissionCRD" 
                                ShowInColumn="colCommissionCRD" SummaryType="Sum" />
                            <dx:ASPxSummaryItem DisplayFormat="Totals" ShowInColumn="colLevels" 
                                SummaryType="Custom" />
                        </TotalSummary>
                        <Columns>
                            <dx:GridViewDataTextColumn Caption="Levels" FieldName="Levels" Name="colLevels" 
                                VisibleIndex="0">
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                <CellStyle HorizontalAlign="Right">
                                </CellStyle>
                            </dx:GridViewDataTextColumn>
                            <%--<dx:GridViewDataHyperLinkColumn Caption="Members" FieldName="MembersCount" 
                                Name="colMembers" VisibleIndex="1">
                                <PropertiesHyperLinkEdit NavigateUrlFormatString="{0}dffdsdf" >
                                </PropertiesHyperLinkEdit>
                            </dx:GridViewDataHyperLinkColumn>--%>
				            <dx:GridViewDataTextColumn Caption="Members" UnboundType="String" VisibleIndex="1">
					            <DataItemTemplate>
						            <dx:ASPxHyperLink ID="lnk" runat="server" NavigateUrl='<%# Eval("PersonsNavigateUrl") %>'
							            Text='<%# Eval("MembersCount") %>' Width="100%" >
						            </dx:ASPxHyperLink>
					            </DataItemTemplate>
				                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                <CellStyle HorizontalAlign="Center">
                                </CellStyle>
				            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="Sales" FieldName="SalesCRD" 
                                Name="colSalesCRD" VisibleIndex="2" Visible="False">
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" /><%-- Caption="Sales &euro;" DisplayFormatString="0.00 &euro;"--%>
            <PropertiesTextEdit DisplayFormatString="0.00">
            </PropertiesTextEdit>
                                <CellStyle HorizontalAlign="Right">
                                </CellStyle>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="Commission" FieldName="CommissionCRD" 
                                Name="colCommissionCRD" VisibleIndex="3">
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" /><%-- Caption="Commission &euro;" DisplayFormatString="0.00 &euro;"--%>
            <PropertiesTextEdit DisplayFormatString="0.00">
            </PropertiesTextEdit>
                                <CellStyle HorizontalAlign="Right">
                                </CellStyle>
                            </dx:GridViewDataTextColumn>
                        </Columns>
                        <Settings ShowFooter="True" />
                        <Paddings Padding="0px" />
                        <Styles>
                            <Header HorizontalAlign="Center" VerticalAlign="Middle" BackColor="#FF7A15" Font-Bold="True" ForeColor="White">
                                <Paddings Padding="0px" PaddingLeft="5px" PaddingRight="5px" />
                            </Header>
                            <Footer BackColor="#FF8b26" HorizontalAlign="Right" VerticalAlign="Middle">
                            </Footer>
                            <HeaderPanel>
                                <Paddings Padding="0px" />
                            </HeaderPanel>
                            <PagerBottomPanel BackColor="#FF8b26">
                            </PagerBottomPanel>
                        </Styles>
                    </dx:ASPxGridView>
                    </asp:Panel>


                    <asp:Panel ID="pnlProfiles" runat="server">
                    <div style="margin:30px 0 10px">
                        <asp:Label ID="lblProfilesListInfo" runat="server" Text="Label"></asp:Label>
                    </div>
                    <dx:ASPxGridView ID="gvProfiles" runat="server" AutoGenerateColumns="False" 
                        DataSourceID="odsProfiles" Width="100%">
                        <TotalSummary>
                            <%--<dx:ASPxSummaryItem DisplayFormat="Sum=0.00 &euro;" FieldName="SalesCRD" 
                                ShowInColumn="colSalesCRD" SummaryType="Sum" />
                            <dx:ASPxSummaryItem DisplayFormat="Sum=0.00 &euro;" FieldName="CommissionCRD" 
                                ShowInColumn="colCommissionCRD" SummaryType="Sum" />--%>
                            <dx:ASPxSummaryItem DisplayFormat="Sum=0.00" FieldName="SalesCRD" 
                                ShowInColumn="colSalesCRD" SummaryType="Sum" />
                            <dx:ASPxSummaryItem DisplayFormat="Sum=0.00" FieldName="CommissionCRD" 
                                ShowInColumn="colCommissionCRD" SummaryType="Sum" />
                            <dx:ASPxSummaryItem DisplayFormat="Totals" ShowInColumn="colLoginName" 
                                SummaryType="Custom" />
                        </TotalSummary>
                        <Columns>
 				            <dx:GridViewDataTextColumn Caption="Login Name" Name="colLoginName" UnboundType="String" VisibleIndex="0">
					            <DataItemTemplate>
                                    <table>
                                        <tr>
                                              <%-- NavigateUrl='<%# Eval("PersonsNavigateUrl") %>'--%>
                                            <td><asp:Image ID="imgErrProfile" runat="server" ImageUrl="~/Images/exclamation.png" Visible='<%# Eval("Status") <> 4 %>' style="vertical-align:middle;"/></td> 
                                            <td>
                                                <asp:Label ID="lnk" runat="server" Text='<%# Eval("LoginName") %>' ></asp:Label>
                                                <%--<dx:ASPxHyperLink ID="lnk" runat="server" 
                                     
                                             NavigateUrl='<%# Eval("PersonsNavigateUrl") %>'
							            Text='<%# Eval("LoginName") %>' 
                                        Width="100%" 
                                        OnDataBound="gvProfiles_DataBound">
						            </dx:ASPxHyperLink>--%></td>
                                        </tr>
                                    </table>
					            </DataItemTemplate>
				            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="Sales" FieldName="SalesCRD" 
                                Name="colSalesCRD" VisibleIndex="1" Visible="false">
            <PropertiesTextEdit DisplayFormatString="0.00"><%-- Caption="Sales &euro;" DisplayFormatString="0.00 &euro;"--%>
            </PropertiesTextEdit>
                                <CellStyle HorizontalAlign="Right">
                                </CellStyle>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="Commission" FieldName="CommissionCRD" 
                                Name="colCommissionCRD" VisibleIndex="2">
            <PropertiesTextEdit DisplayFormatString="0.00"><%-- Caption="Commission &euro;" DisplayFormatString="0.00 &euro;"--%>
            </PropertiesTextEdit>
                                <CellStyle HorizontalAlign="Right">
                                </CellStyle>
                            </dx:GridViewDataTextColumn>
                        </Columns>
                        <SettingsText EmptyDataRow="Δεν βρέθηκαν συνεργάτες" />
                        <SettingsPager AlwaysShowPager="True">
                        </SettingsPager>
                        <Settings ShowFooter="True" />
                        <Paddings Padding="0px" />
                        <Styles>
                            <Header BackColor="#DDD9C3" HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings Padding="0px" />
                            </Header>
                            <Footer BackColor="#FF8b26" HorizontalAlign="Right" VerticalAlign="Middle">
                            </Footer>
                            <HeaderPanel>
                                <Paddings Padding="0px" />
                            </HeaderPanel>
                            <PagerBottomPanel BackColor="#FF8b26">
                            </PagerBottomPanel>
                        </Styles>
                    </dx:ASPxGridView>
                    <asp:ObjectDataSource ID="odsProfiles" runat="server" SelectMethod="GetProfilesReferrers" 
                        TypeName="Dating.Referrals.Server.Site.Web.ReferrerSupplies" 
                        OldValuesParameterFormatString="original_{0}">
                        <SelectParameters>
                            <asp:Parameter Name="targetLevel" Type="Int32" />
                            <asp:Parameter Name="profileId" Type="Int32" />
                            <asp:ControlParameter ControlID="dtFrom" Name="DateFrom" PropertyName="Value" 
                                Type="DateTime" />
                            <asp:ControlParameter ControlID="dtTo" Name="DateTo" PropertyName="Value" 
                                Type="DateTime" />
                            <asp:Parameter Name="affCredits1" Type="Object"  />
                        </SelectParameters>
                    </asp:ObjectDataSource>

                    </asp:Panel>

<%--
                    <asp:Panel ID="pnlDetails" runat="server">

                    <asp:Label ID="lblProfileInfo" runat="server" Text=""></asp:Label>
                    <br />
                    <br />
                    <dx:ASPxGridView ID="gvProfileInfo" runat="server" AutoGenerateColumns="False" 
                        DataSourceID="sdsProfileInfo" Width="100%" 
                        KeyFieldName="CustomerCreditsId">
                        <TotalSummary>
                            <dx:ASPxSummaryItem DisplayFormat="Sum={0}" FieldName="Credits" 
                                ShowInColumn="Credits" SummaryType="Sum" />
                        </TotalSummary>
                        <Columns>
                            <dx:GridViewDataTextColumn FieldName="CustomerCreditsId" VisibleIndex="3" ReadOnly="True" Visible="False">
                                <EditFormSettings Visible="False" />
                            </dx:GridViewDataTextColumn>
 				            <dx:GridViewDataTextColumn VisibleIndex="4" FieldName="CustomerId" 
                                Visible="False">
				            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="CustomerLoginName" VisibleIndex="5" 
                                ReadOnly="True" Visible="False">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Credits" VisibleIndex="2">
                            </dx:GridViewDataTextColumn>
 				            <dx:GridViewDataDateColumn FieldName="Date" 
                                VisibleIndex="0">
                            </dx:GridViewDataDateColumn>
 				            <dx:GridViewDataTextColumn FieldName="CustomerTransactionID" ReadOnly="True" 
                                Visible="False" VisibleIndex="6">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="CreditsTypeId" Visible="False" 
                                VisibleIndex="7">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="ConversationWithCustomerId" 
                                Visible="False" VisibleIndex="8">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="CreditsType" Visible="False" 
                                VisibleIndex="9">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataCheckColumn FieldName="Expired" ReadOnly="True" Visible="False" 
                                VisibleIndex="10">
                            </dx:GridViewDataCheckColumn>
                            <dx:GridViewDataTextColumn FieldName="DatePurchased" ReadOnly="True" 
                                Visible="False" VisibleIndex="11">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Action" ReadOnly="True" Visible="False" 
                                VisibleIndex="12">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Description" ReadOnly="True" 
                                VisibleIndex="1">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="Commission CRD" FieldName="CommissionCRD" 
                                VisibleIndex="3">
                            </dx:GridViewDataTextColumn>
                        </Columns>
                        <Settings ShowFooter="True" />
                        <SettingsText EmptyDataRow="Δεν βρέθηκαν πληροφορίες" />
                    </dx:ASPxGridView>
                    <asp:SqlDataSource ID="sdsProfileInfo" runat="server" ConnectionString="<%$ ConnectionStrings:AppDBconnectionString %>"
                        SelectCommand="AffiliateCreditsHistory" SelectCommandType="StoredProcedure"><SelectParameters>
                            <asp:Parameter DefaultValue="0" Name="CustomerId" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                                                </asp:Panel>
--%>
                </div>
            </div>


    <dx:ASPxPopupControl runat="server"
     Height="400px" Width="600px"
     ID="popupAffiliateSupplies"
        ClientIDMode="AutoID" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
        ClientInstanceName="popupAffiliateSupplies">
        <ContentStyle HorizontalAlign="Left" VerticalAlign="Middle">
            <BackgroundImage ImageUrl="../images/bg.jpg" />
            <Paddings Padding="10px" />
            <Paddings Padding="10px"></Paddings>
            <BackgroundImage ImageUrl="../images/bg.jpg"></BackgroundImage>
        </ContentStyle>
        <CloseButtonStyle>
            <HoverStyle>
                <BackgroundImage ImageUrl="~/Images/close-button1.png" Repeat="NoRepeat" HorizontalPosition="center"
                    VerticalPosition="center"></BackgroundImage>
            </HoverStyle>
            <BackgroundImage ImageUrl="~/Images/close-button2.png" Repeat="NoRepeat" HorizontalPosition="center"
                VerticalPosition="center"></BackgroundImage>
        </CloseButtonStyle>
        <CloseButtonImage Url="~/Images/spacer10.png" Height="17px" Width="17px">
        </CloseButtonImage>
        <SizeGripImage Height="40px" Url="~/Images/resize.png" Width="40px">
        </SizeGripImage>
        <HeaderStyle BackColor="#35619F" Font-Bold="True" ForeColor="White">
            <Paddings PaddingBottom="10px" PaddingTop="10px" />
            <Paddings PaddingTop="10px" PaddingBottom="10px"></Paddings>
        </HeaderStyle>
        <ModalBackgroundStyle CssClass="modalPopup">
        </ModalBackgroundStyle>
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server" SupportsDisabledAttribute="True">
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>


	<uc2:whatisit ID="WhatIsIt1" runat="server" />
    
</asp:Content>

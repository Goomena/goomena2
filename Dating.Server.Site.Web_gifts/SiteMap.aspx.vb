﻿Imports Dating.Server.Core.DLL
Imports System.Data.SqlClient
Imports System.Xml
Imports DevExpress.Web.ASPxSiteMapControl
Imports Dating.Server.Datasets.DLL

Public Class SiteMap
    Inherits BasePage


    'Dim _pageData As clsPageData
    'Protected ReadOnly Property CurrentPageData As clsPageData
    '    Get
    '        If (_pageData Is Nothing) Then
    '            _pageData = New clsPageData(Context)
    '            AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
    '        End If
    '        Return _pageData
    '    End Get
    'End Property



    Dim _MasterPageData As clsPageData
    Protected ReadOnly Property MasterPageData As clsPageData
        Get
            If (_MasterPageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _MasterPageData = New clsPageData("master", Context, coe)
            End If
            Return _MasterPageData
        End Get
    End Property

    Protected Overrides Sub OnPreInit(ByVal e As EventArgs)
        MyBase.OnPreInit(e)
    End Sub

    Private Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init

    End Sub


    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            LoadSiteMap()

            Dim cPageBasic As clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "PreRender")
        End Try


    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            LoadLAG()
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub


    Protected Sub LoadLAG()
        Try
            '  Dim cPageBasic As clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            'lblHeader.Text = CurrentPageData.GetCustomString("lblHeader")
            'lblCloseRdv.Text = CurrentPageData.GetCustomString("lblCloseRdv")
            'btnRegister.Text = CurrentPageData.GetCustomString("btnRegister")
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub



    Shared Function GetCMSpageLink(SitePageID As String, myTitle As String, Area As String, PageFileName As String) As String
        Dim url As String = ""
        Try
            Dim myTitleEncoded = System.Web.HttpUtility.UrlEncode(myTitle)


            Dim CMSPage As String = ""

            Select Case Area
                Case "Public"
                    CMSPage = "/cmspage.aspx?pageid="
                Case "Members"
                    CMSPage = "/members/cmspage.aspx?pageid="
                Case "Admin"
                    CMSPage = "/admin/cmspage.aspx?pageid="
            End Select
            url = CMSPage & SitePageID & "&title=" & myTitleEncoded
        Catch ex As Exception
            'ErrorMsgBox(ex, "")
        End Try


        Return url
    End Function


    Private Sub AddLinksForPosition(position As String, _ID As Integer, ByRef dt As DataTable)


        Dim visibleOnAdmistratorUserRole As Boolean? = Nothing,
                                    visibleOnAfficateUserRole As Boolean? = Nothing,
                                    visibleOnMemberUserRole As Boolean? = Nothing,
                                    visibleOnPublicArea As Boolean? = Nothing,
                                    VisibleOnResellerUserRole As Boolean? = Nothing


        Dim q As List(Of dsMenu.SiteMenuItemsRow) = (From i In Caching.Current.SiteMenuItems Where _
                i.Position.ToLower = position.ToLower AndAlso _
                (i.VisibleOnAdmistratorUserRole = visibleOnAdmistratorUserRole OrElse visibleOnAdmistratorUserRole Is Nothing) AndAlso _
                (i.VisibleOnAfficateUserRole = visibleOnAfficateUserRole OrElse visibleOnAfficateUserRole Is Nothing) AndAlso _
                (i.VisibleOnMemberUserRole = visibleOnMemberUserRole OrElse visibleOnMemberUserRole Is Nothing) AndAlso _
                (i.VisibleOnPublicArea = visibleOnPublicArea OrElse visibleOnPublicArea Is Nothing) AndAlso _
                (i.VisibleOnResellerUserRole = VisibleOnResellerUserRole OrElse VisibleOnResellerUserRole Is Nothing) _
                Where i.ParrentSiteMenuItemID = 0 AndAlso i.IsActive = True _
                Order By i.SortNumber _
                Select i).ToList()


        Dim cnt As Integer
        For cnt = 0 To q.Count - 1

            Dim _text As String = ""
            Dim _toolTip As String = ""
            Dim _subID As Integer = dt.Rows.Count + 1

            With q(cnt)

                ' by default use english
                _text = .US
                _toolTip = .ToolTipUS

                Select Case Session("LagID")
                    Case "GR"
                        If .GR.Length > 0 Then
                            _text = .GR
                            _toolTip = .ToolTipGR
                        End If
                    Case "HU"
                        If .HU.Length > 0 Then
                            _text = .HU
                            _toolTip = .ToolTipHU
                        End If
                    Case "ES"
                        If .ES.Length > 0 Then
                            _text = .ES
                            _toolTip = .ToolTipES
                        End If
                    Case "DE"
                        If .DE.Length > 0 Then
                            _text = .DE
                            _toolTip = .ToolTipDE
                        End If
                    Case "RO"
                        If .RO.Length > 0 Then
                            _text = .RO
                            _toolTip = .ToolTipRO
                        End If
                    Case "TU"
                        If .TU.Length > 0 Then
                            _text = .TU
                            _toolTip = .ToolTipTU
                        End If
                    Case "IT"
                        If .IT.Length > 0 Then
                            _text = .IT
                            _toolTip = .ToolTipIT
                        End If
                    Case "IL"
                        If .IL.Length > 0 Then
                            _text = .IL
                            _toolTip = .ToolTipIL
                        End If
                    Case "FR"
                        If .FR.Length > 0 Then
                            _text = .FR
                            _toolTip = .ToolTipFR
                        End If
                    Case "AL"
                        If Not .IsALNull() AndAlso .AL.Length > 0 Then
                            _text = .AL
                            _toolTip = .ToolTipAL
                        End If
                    Case "TR"
                        If .TR.Length > 0 Then
                            _text = .TR
                            _toolTip = .ToolTipTR
                        End If
                End Select



                Dim dr1 As DataRow = dt.NewRow()
                dr1("ID") = _subID
                dr1("ParentID") = _ID
                dr1("NavigateURL") = .NavigateURL
                dr1("ToolTip") = _toolTip


                Dim isVisible As Boolean = True
                Try

                    If (.US.Contains("[BLANK]")) Then
                        dr1("Target") = "_blank"
                        _text = _text.Replace("[BLANK]", "")
                    End If

                    If (.US.Contains("[BLOG]")) Then
                        _text = _text.Replace("[BLOG]", "")
                        If (HttpContext.Current.Session("GEO_COUNTRY_CODE") <> "GR") Then
                            isVisible = False
                        End If
                    End If

                Catch ex As Exception
                    WebErrorSendEmail(ex, "")
                End Try


                If (isVisible) Then

                    dr1("Text") = _text
                    dt.Rows.Add(dr1)

                End If


            End With
        Next
    End Sub


    Private Function GenerateSiteMapDatatable() As DataTable
        Using dt As New DataTable


            Try
                dt.Columns.Add(New DataColumn("ID", GetType(Integer)))
                dt.Columns.Add(New DataColumn("ParentID", GetType(Integer)))
                dt.Columns.Add(New DataColumn("Text", GetType(String)))
                dt.Columns.Add(New DataColumn("NavigateURL", GetType(String)))
                dt.Columns.Add(New DataColumn("ToolTip", GetType(String)))
                dt.Columns.Add(New DataColumn("Target", GetType(String)))

                Try
                    Dim dr As DataRow = dt.NewRow()
                    dr("ID") = dt.Rows.Count + 1
                    dr("ParentID") = 0
                    dr("Text") = "Root"
                    dr("NavigateURL") = ""
                    dr("ToolTip") = ""
                    dt.Rows.Add(dr)
                Catch ex As Exception
                End Try

                Dim titles As New List(Of String)
                titles.AddRange(New String() {MasterPageData.GetCustomString("lblTopMenu"),
                                              MasterPageData.GetCustomString("lblFooterMenuTitle1"),
                                              MasterPageData.GetCustomString("lblFooterMenuTitle2"),
                                              MasterPageData.GetCustomString("lblFooterMenuTitle3"),
                                              MasterPageData.GetCustomString("lblFooterMenuTitle4"),
                                              MasterPageData.GetCustomString("lblFooterMenuTitle5"),
                                              MasterPageData.GetCustomString("lblMoreInfo"),
                                              MasterPageData.GetCustomString("lblSiteMapUncategorized")})

                Dim positions As New List(Of String)
                positions.AddRange(New String() {"topLeft",
                                                 "mnuFooter1",
                                                 "mnuFooter2",
                                                 "mnuFooter3",
                                                 "mnuFooter4",
                                                 "mnuFooter5",
                                                 "MenuLinks1", "MenuLinks2", "MenuLinks3",
                                                 "SiteMapUncategorized"})

                Dim _ID As Integer = 0
                Dim parentID As Integer = 1
                Dim cntPos As Integer

                For cnt1 = 0 To titles.Count - 1
                    Dim itm As String = titles(cnt1)
                    _ID = dt.Rows.Count + 1

                    Dim dr As DataRow = dt.NewRow()
                    dr("ID") = _ID
                    dr("ParentID") = parentID
                    dr("Text") = "<div class=""sitemap-category"">" & itm & "</div>"
                    dr("NavigateURL") = ""
                    dr("ToolTip") = ""
                    dt.Rows.Add(dr)

                    Dim prevRowsCount As Integer = dt.Rows.Count
                    Dim nextRowsCount As Integer

                    Dim position As String = positions(cntPos)
                    AddLinksForPosition(position, _ID, dt)

                    If (position = "MenuLinks1") Then

                        cntPos += 1
                        position = positions(cntPos)
                        AddLinksForPosition(position, _ID, dt)


                        cntPos += 1
                        position = positions(cntPos)
                        AddLinksForPosition(position, _ID, dt)

                    End If

                    nextRowsCount = dt.Rows.Count
                    If (nextRowsCount = prevRowsCount) Then
                        dt.Rows.Remove(dr)
                    End If

                    cntPos += 1
                Next



                'Dim sql As String = <sql><![CDATA[

                'SELECT [SitePageID]
                '      ,[myTitle]
                '      ,[PageFileName]
                '      ,[PageCategoryID]
                '      ,[Area]
                '      ,[TitleUS]
                '      ,[metaDescriptionUS]
                '      ,[BodyUS]
                '      ,[SiteMapVisible]
                '  FROM [SYS_SitePages]
                'WHERE [Area]='Public' 
                'and [SiteMapVisible]=1
                'and isnull([MyTitle],'')<> ''
                'and isnull([PageFileName],'')<> ''
                'and not [MyTitle] like '***'
                'and not [PageFileName] like '***'

                ']]></sql>

                'Dim cmd As SqlCommand = DataHelpers.GetSqlCommand(sql)
                'Dim dt = DataHelpers.GetDataTable(cmd)
                'dt.Columns.Add("pageUrl")

                'For cnt = 0 To dt.Rows.Count - 1
                '    Dim dr As DataRow = dt.Rows(cnt)
                '    Dim PageFileName As String = dr("PageFileName").ToString()
                '    Dim newPage As String = ""
                '    Try
                '        newPage = ResolveUrl(PageFileName)
                '        dt.Rows(cnt)("pageUrl") = newPage
                '    Catch ex As Exception
                '    End Try

                '    If (Not String.IsNullOrEmpty(newPage)) Then
                '        newPage = Server.MapPath(newPage)
                '        If (Not System.IO.File.Exists(newPage)) Then
                '            newPage = ""
                '        End If
                '    End If

                '    If (String.IsNullOrEmpty(newPage)) Then
                '        dt.Rows(cnt)("pageUrl") = GetCMSpageLink(dr("SitePageID"), dr("myTitle"), dr("Area"), PageFileName)
                '    End If
                'Next

            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try

            Return dt
        End Using
    End Function



    Private Sub LoadSiteMap()
        Try
            Using dt As DataTable = GenerateSiteMapDatatable()
                Using dataSource As SiteMapDataSource = GenerateSiteMapHierarchy(dt)
                    dataSource.ShowStartingNode = False

                    ctlSiteMap.DataSource = dataSource
                    ctlSiteMap.DataBind()
                End Using
            End Using
           
            
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Protected Function GenerateSiteMapHierarchy(ByRef dataSource As SqlDataSource) As SiteMapDataSource
        Using ret As SiteMapDataSource = New SiteMapDataSource()
            Dim arg As DataSourceSelectArguments = New DataSourceSelectArguments("ParentID")
            Dim dataView As DataView = TryCast(dataSource.Select(arg), DataView)
            Using table As DataTable = dataView.Table
                Dim rowRootNode As DataRow = table.Select("ParentID = 0")(0)

                Dim provider As UnboundSiteMapProvider = New UnboundSiteMapProvider(rowRootNode("NavigateURL").ToString(), rowRootNode("Text").ToString())
                AddNodeToProviderRecursive(provider.RootNode, rowRootNode("ID").ToString(), table, provider)
                ret.Provider = provider
                Return ret
            End Using
          
        End Using

       
    End Function

    Protected Function GenerateSiteMapHierarchy(ByRef table As DataTable) As SiteMapDataSource
        Using ret As SiteMapDataSource = New SiteMapDataSource()
            Dim rowRootNode As DataRow = table.Select("ParentID = 0")(0)

            Dim provider As UnboundSiteMapProvider = New UnboundSiteMapProvider(rowRootNode("NavigateURL").ToString(), rowRootNode("Text").ToString())


            AddNodeToProviderRecursive(provider.RootNode, rowRootNode("ID").ToString(), table, provider)
            ret.Provider = provider
            Return ret
        End Using

        'Dim arg As DataSourceSelectArguments = New DataSourceSelectArguments("ParentID")
        'Dim dataView As DataView = TryCast(dataSource.Select(arg), DataView)
        'Dim table As DataTable = dataView.Table

    End Function

    Private Sub AddNodeToProviderRecursive(ByVal parentNode As SiteMapNode, ByVal parentID As String, ByVal table As DataTable, ByVal provider As UnboundSiteMapProvider)

        Dim childRows As DataRow() = table.Select("ParentID = " & parentID)
        For Each row As DataRow In childRows
            Try
                Dim childNode As SiteMapNode = CreateSiteMapNode(row, provider)
                provider.AddSiteMapNode(childNode, parentNode)
                AddNodeToProviderRecursive(childNode, row("ID").ToString(), table, provider)
            Catch ex As System.InvalidOperationException
            Catch ex As Exception
                WebErrorSendEmail(ex, "")
            End Try
        Next row
    End Sub

    Private Shared Function CreateSiteMapNode(ByVal dataRow As DataRow, ByVal provider As UnboundSiteMapProvider) As SiteMapNode
        Return (provider.CreateNode(dataRow("NavigateURL").ToString(), dataRow("Text").ToString(), "", Nothing, New NameValueCollection()))
    End Function

End Class
﻿Imports Dating.Server.Core.DLL

Public Class GeoCheck
    Inherits System.Web.UI.Page

    Public Property ifrUserMap_src As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Sub btnCheck_Click(sender As Object, e As EventArgs) Handles btnCheck.Click
        Dim country As clsCountryByIP = Nothing
        Try

            country = clsCountryByIP.GetCountryCodeFromIP(txtIP.Text, ConfigurationManager.ConnectionStrings("GeoDBconnectionString").ConnectionString)
            'Dim lst As New List(Of DataRow)
            'lst.Add(country.DataRecord)
            'gvResult.DataSource = lst
            gvResult.DataSource = country.DataRecord
            gvResult.DataBind()

        Catch ex As Exception
            lblError.Text = ex.ToString()
        End Try


        If (country IsNot Nothing) Then
            Try

                Dim zoom As Integer = 12
                Dim radius As Integer = 10
                Dim lat As Double = AppUtils.GetDoubleValue_FromStringWithPeriod(country.latitude)
                Dim lng As Double = AppUtils.GetDoubleValue_FromStringWithPeriod(country.longitude)
                Dim mapPath As String = System.Web.VirtualPathUtility.ToAbsolute("~/map.aspx?lat=" & lat & "&lng=" & lng & "&radius=" & radius & "&zoom=" & zoom)

                divMapContainer.Visible = True
                'ifrUserMap.Attributes.Add("src", mapPath)
                ifrUserMap_src = mapPath
            Catch ex As Exception
                lblError.Text = ex.ToString()
            End Try
        End If

    End Sub
End Class
﻿<%@ Page Title="" Language="vb" AutoEventWireup="true" MasterPageFile="~/Root.Master" CodeBehind="Default6.aspx.vb" 
Inherits="Dating.Server.Site.Web._Default4" %>


<%@ Register src="~/UserControls/UsersList.ascx" tagname="UsersList" tagprefix="uc1" %>
<%@ Register src="~/UserControls/LiveZillaPublic.ascx" tagname="LiveZillaPublic" tagprefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="//cdn.goomena.com/CSS/Hint.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="//cdn.goomena.com/css/slide/slide.css" />
    <link rel="stylesheet" href="//cdn.goomena.com/css/slide/custom.css" />
    <link rel="stylesheet" href="//cdn.goomena.com/balance/media/widgetkit/widgets/slideshow/css/slideshow.css" />
    <link rel="stylesheet" href="/v1/css/def-public.css?v=10" />
    <style type="text/css">
        .fb-connect-note { position: absolute; bottom: 17px; right: 10px; width: 401px; color: #fff; font-size: 11px; text-align: center; }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">

    <%--<div style="display:none;">
        <a href="http://zizina.com" target="_blank">zizina</a>
    </div>--%>

        <div class="regformcontainer" id="regformcontainer" runat="server">
            <div class="regmidcontainer" id="regmidcontainer" runat="server">
                <div class="registerform" id="registerform" runat="server">
                    <div class="mainregdiv">
                        <div class="notmember">
                            <asp:Literal ID="lblNotAMember" runat="server"></asp:Literal>
                        </div>
                            <div runat="server" id="mainregdivctr" class="mainregdivctr" data-hint="Please select Gender before you continue!">
                                <h1>
                                    <asp:Literal ID="lblChooseGender" runat="server"></asp:Literal>
                                </h1>
                                
                                    <dx:ASPxRadioButton ID="RdioMale" runat="server" GroupName="selectgender" 
                                        Font-Size="16px" Text="" encodehtml="false">
                                    </dx:ASPxRadioButton>
                                    <dx:ASPxRadioButton ID="RdioFemale" runat="server" GroupName="selectgender" 
                                        Font-Size="16px" Text="" encodehtml="false">
                                    </dx:ASPxRadioButton>
                                
                            </div>
                            
                        <p class="mainbememberp"><asp:Literal ID="lblBeMemberNow" runat="server"></asp:Literal></p>
                            <a id="HyperRegister" runat="server" href="Register.aspx" class="HyperRegister" onclick="ShowLoading();">
                                <asp:Literal ID="lblRegister" runat="server"></asp:Literal>
                            </a>
                        <div class="clear"></div>
                    </div>
                    <div class="mainalreadymembr">
                        <p>
                            <asp:Literal ID="lblAlreadyMember" runat="server"></asp:Literal></p>
                        <a id="HyperConnect" runat="server" href="Login.aspx" onclick="ShowLoading();">
                            <asp:Literal ID="lblConnect" runat="server"></asp:Literal></a>
                        <div class="clear"></div>
                    </div>
                    <div class="mainfblogindiv" style="position:relative;">
                        <img src="//cdn.goomena.com/goomena-kostas/first-general/facebook-logo.png" />
                        <%--<a href="#">
                            <asp:Literal ID="lblFBConnect" runat="server"></asp:Literal></a>--%>
                        <div class="clear"></div>
                        <div class="fb-connect-note">
                            <asp:HyperLink ID="lblFBConnect" runat="server" Text="Login With Facebook"
                             NavigateUrl="~/Register.aspx?log=fb" onclick="ShowLoading();" />
                             <div style="height:5px;">&nbsp;</div>
                            <asp:Literal ID="lbFBNote" runat="server"></asp:Literal>
                        </div>
                    </div>
                </div>
            </div>
        </div>
       <div class="gendermsg"></div>         
    <asp:Panel ID="pnlMosaic" CssClass="mosaic" runat="server" EnableViewState="false">
		<div runat="server" class="pic pic1" id="pic1"></div>
		<div runat="server" class="pic pic2" id="pic2"></div>
		<div runat="server" class="pic pic3" id="pic3"></div>
		<div runat="server" class="pic pic4" id="pic4"></div>
		<div runat="server" class="pic pic5" id="pic5"></div>
		<div runat="server" class="pic pic6" id="pic6"></div>
		<div runat="server" class="pic pic7" id="pic7"></div>
		<div runat="server" class="pic pic8" id="pic8"></div>
		<div runat="server" class="pic pic9" id="pic9"></div>
		<div runat="server" class="pic pic10" id="pic10"></div>
		<div runat="server" class="pic pic11" id="pic11"></div>
		<div runat="server" class="pic pic12" id="pic12"></div>
		<div runat="server" class="pic pic13" id="pic13"></div>
		<div runat="server" class="pic pic14" id="pic14"></div>
		<div runat="server" class="pic pic15" id="pic15" visible="false"></div>
		<div runat="server" class="pic pic16" id="pic16" visible="false"></div>
		<div runat="server" class="pic pic17" id="pic17" visible="false"></div>
		<div runat="server" class="pic pic18" id="pic18" visible="false"></div>
		<div runat="server" class="pic pic19" id="pic19" visible="false"></div>
		<div runat="server" class="pic pic20" id="pic20" visible="false"></div>
		<div runat="server" class="pic pic21" id="pic21" visible="false"></div>
		<div runat="server" class="pic pic22" id="pic22" visible="false"></div>
		<div runat="server" class="pic pic23" id="pic23"></div>
		<div runat="server" class="pic pic24" id="pic24"></div>
		<div runat="server" class="pic pic25" id="pic25"></div>
		<div runat="server" class="pic pic26" id="pic26"></div>
		<div runat="server" class="pic pic27" id="pic27"></div>
		<div runat="server" class="pic pic28" id="pic28"></div>
		<div runat="server" class="pic pic29" id="pic29"></div>
		<div runat="server" class="pic pic30" id="pic30"></div>
		<div runat="server" class="pic pic31" id="pic31"></div>
		<div runat="server" class="pic pic32" id="pic32"></div>
		<div runat="server" class="pic pic33" id="pic33"></div>
		<div runat="server" class="pic pic34" id="pic34"></div>
		<div runat="server" class="pic pic35" id="pic35"></div>
		<div runat="server" class="pic pic36" id="pic36"></div>
		<div runat="server" class="pic pic37" id="pic37" visible="false"></div>
		<div runat="server" class="pic pic38" id="pic38" visible="false"></div>
		<div runat="server" class="pic pic39" id="pic39" visible="false"></div>
		<div runat="server" class="pic pic40" id="pic40" visible="false"></div>
		<div runat="server" class="pic pic41" id="pic41" visible="false"></div>
		<div runat="server" class="pic pic42" id="pic42" visible="false"></div>
		<div runat="server" class="pic pic43" id="pic43" visible="false"></div>
		<div runat="server" class="pic pic44" id="pic44" visible="false"></div>
		<div runat="server" class="pic pic45" id="pic45"></div>
		<div runat="server" class="pic pic46" id="pic46"></div>
		<div runat="server" class="pic pic47" id="pic47"></div>
		<div runat="server" class="pic pic48" id="pic48"></div>
		<div runat="server" class="pic pic49" id="pic49"></div>
		<div runat="server" class="pic pic50" id="pic50"></div>
        <div runat="server" class="pic pic51" id="pic51"></div>
		<div runat="server" class="pic pic52" id="pic52"></div>
		<div runat="server" class="pic pic53" id="pic53"></div>
		<div runat="server" class="pic pic54" id="pic54"></div>
		<div runat="server" class="pic pic55" id="pic55"></div>
		<div runat="server" class="pic pic56" id="pic56"></div>
		<div runat="server" class="pic pic57" id="pic57"></div>
		<div runat="server" class="pic pic58" id="pic58"></div>
		<div runat="server" class="pic pic59" id="pic59"></div>
		<div runat="server" class="pic pic60" id="pic60"></div>
		<div runat="server" class="pic pic61" id="pic61" visible="false"></div>
		<div runat="server" class="pic pic62" id="pic62" visible="false"></div>
		<div runat="server" class="pic pic63" id="pic63" visible="false"></div>
		<div runat="server" class="pic pic64" id="pic64" visible="false"></div>
		<div runat="server" class="pic pic65" id="pic65" visible="false"></div>
		<div runat="server" class="pic pic66" id="pic66" visible="false"></div>
		<div runat="server" class="pic pic67" id="pic67" visible="false"></div>
		<div runat="server" class="pic pic68" id="pic68" visible="false"></div>
		<div runat="server" class="pic pic69" id="pic69"></div>
		<div runat="server" class="pic pic70" id="pic70"></div>
		<div runat="server" class="pic pic71" id="pic71"></div>
		<div runat="server" class="pic pic72" id="pic72"></div>
		<div runat="server" class="pic pic73" id="pic73"></div>
		<div runat="server" class="pic pic74" id="pic74"></div>
		<div runat="server" class="pic pic75" id="pic75"></div>
		<div runat="server" class="pic pic76" id="pic76"></div>
		<div runat="server" class="pic pic77" id="pic77"></div>
		<div runat="server" class="pic pic78" id="pic78"></div>
		<div runat="server" class="pic pic79" id="pic79"></div>
		<div runat="server" class="pic pic80" id="pic80"></div>
		<div runat="server" class="pic pic81" id="pic81"></div>
		<div runat="server" class="pic pic82" id="pic82" visible="false"></div>
		<div runat="server" class="pic pic83" id="pic83" visible="false"></div>
		<div runat="server" class="pic pic84" id="pic84" visible="false"></div>
		<div runat="server" class="pic pic85" id="pic85" visible="false"></div>
		<div runat="server" class="pic pic86" id="pic86" visible="false"></div>
		<div runat="server" class="pic pic87" id="pic87" visible="false"></div>
		<div runat="server" class="pic pic88" id="pic88" visible="false"></div>
        <%--<div class="gradient1">
    </div>--%>
     </asp:Panel> 

      <%--  <div class="maina">
        
    <asp:Panel ID="pnlStartingText" CssClass="manwomancontainer" runat="server" EnableViewState="false">
        <div class="top-text-wrap">
            <div class="woman"><asp:Literal ID="ltrMenMessage" runat="server"></asp:Literal></div>
            <div class="man"><asp:Literal ID="ltrLadiesMessage" runat="server"></asp:Literal></div>
            <div class="clear"></div>
        </div>
        <div class="collapsible-text-wrap">
            <div class="woman" style="margin-top:0px;">
                <asp:Literal ID="ltrMenMessageCollapse" runat="server"></asp:Literal>
            </div>
            <div class="man" style="margin-top:0px;">
                <asp:Literal ID="ltrLadiesMessageCollapse" runat="server"></asp:Literal>
            </div>
            <div class="clear"></div>
        </div>
        <div class="photos4-wrap">
            <div class="woman" style="margin-top:0px;">
                <div class="photosforwomen">
                    <asp:HyperLink ID="anchornum1" runat="server">
                        <div class="polaroid">
                            <p runat="server" id="imagename1" class="imagename1">
                            </p>
                            <img runat="server" id="imgtag1" class="imgtag1" />
                        </div>
                    </asp:HyperLink>
                    <asp:HyperLink ID="anchornum2" runat="server">
                        <div class="polaroid">
                            <p runat="server" id="imagename2" class="imagename2">
                            </p>
                            <img runat="server" id="imgtag2" class="imgtag2" />
                        </div>
                    </asp:HyperLink>
                    <asp:HyperLink ID="anchornum3" runat="server">
                        <div class="polaroid">
                            <p runat="server" id="imagename3" class="imagename3">
                            </p>
                            <img runat="server" id="imgtag3" class="imgtag3" />
                        </div>
                    </asp:HyperLink>
                    <asp:HyperLink ID="anchornum4" runat="server">
                        <div class="polaroid">
                            <p runat="server" id="imagename4" class="imagename4">
                            </p>
                            <img runat="server" id="imgtag4" class="imgtag4" />
                        </div>
                    </asp:HyperLink>
                </div>
                <div class="clear"></div>
            </div>
            <div class="man" style="margin-top:0px;">
                <div class="photosformen">
                    <asp:HyperLink ID="anchornum5" runat="server">
                        <div class="polaroid2">
                            <p runat="server" id="imagename5" class="imagename5">
                            </p>
                            <img runat="server" id="imgtag5" class="imgtag5" />
                        </div>
                    </asp:HyperLink>
                    <asp:HyperLink ID="anchornum6" runat="server">
                        <div class="polaroid2">
                            <p runat="server" id="imagename6" class="imagename6">
                            </p>
                            <img runat="server" id="imgtag6" class="imgtag6" />
                        </div>
                    </asp:HyperLink>
                    <asp:HyperLink ID="anchornum7" runat="server">
                        <div class="polaroid2">
                            <p runat="server" id="imagename7" class="imagename7">
                            </p>
                            <img runat="server" id="imgtag7" class="imgtag7" />
                        </div>
                    </asp:HyperLink>
                    <asp:HyperLink ID="anchornum8" runat="server">
                        <div class="polaroid2">
                            <p runat="server" id="imagename8" class="imagename8">
                            </p>
                            <img runat="server" id="imgtag8" class="imgtag8" />
                        </div>
                    </asp:HyperLink>
                </div>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </asp:Panel>
    </div>
    <div class="clear"></div>--%>
    <div class="maina">
        <asp:Panel ID="pnlStartingText" CssClass="manwomancontainer" runat="server" EnableViewState="false">
            <div class="top-text-wrap">
                <div class="woman"><asp:Literal ID="ltrMenMessage" runat="server"></asp:Literal></div>
                <div class="man"><asp:Literal ID="ltrLadiesMessage" runat="server"></asp:Literal></div>
                <div class="clear"></div>
            </div>
            <div class="collapsible-text-wrap">
                <div class="woman" style="margin-top:0px;">
                    <asp:Literal ID="ltrMenMessageCollapse" runat="server"></asp:Literal>
                </div>
                <div class="man" style="margin-top:0px;">
                    <asp:Literal ID="ltrLadiesMessageCollapse" runat="server"></asp:Literal>
                </div>
                <div class="clear"></div>
                <div class="ColouredBoxes" >
                    <div class="ColoredLadie">
                            <asp:Label ID="LblLadieMakeDate" runat="server" Text=""></asp:Label>

                         <div class="photos4-wrap">
                <div class="woman" style="margin-top:0px;">
                    <div class="photosforwomen">
                        <asp:HyperLink ID="anchornum1" runat="server" CssClass="photobox">
                            <div class="polaroid">
                                <p runat="server" id="imagename1" class="imagename" >
                                </p>
                                <img runat="server" id="imgtag1" class="imgtag" />
                            </div>
                        </asp:HyperLink>
                        <asp:HyperLink ID="anchornum2" runat="server"  CssClass="photobox">
                            <div class="polaroid">
                                <p runat="server" id="imagename2" class="imagename">
                                </p>
                                <img runat="server" id="imgtag2" class="imgtag" />
                            </div>
                        </asp:HyperLink>
                        <asp:HyperLink ID="anchornum3" runat="server"  CssClass="photobox">
                            <div class="polaroid">
                                <p runat="server" id="imagename3" class="imagename">
                                </p>
                                <img runat="server" id="imgtag3" class="imgtag" />
                            </div>
                        </asp:HyperLink>
                        <asp:HyperLink ID="anchornum4" runat="server"  CssClass="photobox">
                            <div class="polaroid">
                                <p runat="server" id="imagename4" class="imagename">
                                </p>
                                <img runat="server" id="imgtag4" class="imgtag" />
                            </div>
                        </asp:HyperLink>
                    </div>
                    <div class="clear"></div>
                </div>
                               <div class="clear"></div>
            </div>
                          <div class="RegisterButtoContainer">
                                        <asp:HyperLink ID="lblRegBottomwm" runat="server"
                        NavigateUrl="~/Register.aspx" onclick="ShowLoading();" CssClass="btnRegister">
                        Κάνε εγγραφή τώρα
                    </asp:HyperLink>
        </div>
                        </div>
    <div class="ColoredMan">
        <asp:Label ID="LblManMakeDate" runat="server" Text=""></asp:Label>
           <div class="photos4-wrap">
                 <div class="man" style="margin-top:0px;">
                    <div class="photosformen">
                        <asp:HyperLink ID="anchornum5" runat="server" CssClass="photobox">
                            <div class="polaroid2">
                                <p runat="server" id="imagename5" class="imagename">
                                </p>
                                <img runat="server" id="imgtag5" class="imgtag" />
                            </div>
                        </asp:HyperLink>
                        <asp:HyperLink ID="anchornum6" runat="server"  CssClass="photobox">
                            <div class="polaroid2">
                                <p runat="server" id="imagename6" class="imagename">
                                </p>
                                <img runat="server" id="imgtag6" class="imgtag" />
                            </div>
                        </asp:HyperLink>
                        <asp:HyperLink ID="anchornum7" runat="server"  CssClass="photobox">
                            <div class="polaroid2">
                                <p runat="server" id="imagename7" class="imagename">
                                </p>
                                <img runat="server" id="imgtag7" class="imgtag" />
                            </div>
                        </asp:HyperLink>
                        <asp:HyperLink ID="anchornum8" runat="server"  CssClass="photobox">
                            <div class="polaroid2">
                                <p runat="server" id="imagename8" class="imagename">
                                </p>
                                <img runat="server" id="imgtag8" class="imgtag" />
                            </div>
                        </asp:HyperLink>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
            </div>
        <div class="RegisterButtoContainer">
                               <asp:HyperLink ID="lblRegBottomm" runat="server"
                        NavigateUrl="~/Register.aspx" onclick="ShowLoading();" CssClass="btnRegister">
                        Κάνε εγγραφή τώρα
                    </asp:HyperLink>
        </div>
            </div>
     <div class="clear"></div>
  </div>

            </div>
           
            <div class="clear"></div>
        </asp:Panel>
    </div>
    <div class="clear"></div>
    <div class="PhotoInfoWrap hidden">
        <div id="PhotoInfos" class="PhotoInfos">
            <img class="InfoImg" />
        </div>
        <p class="ProfileID"></p>
        <p class="ProfileCity"></p>
        <div class="ProfileAgeWrap">
            <p class="ProfileAgeText"></p>
            <p class="ProfileAge"></p>
        </div>
    </div>


<script type="text/javascript">
/* preload images*/
    (function () {
        try {
            var a = new Image(); a.src = '//cdn.goomena.com/goomena-kostas/first-general/red-meion.png';
            var b = new Image(); b.src = '//cdn.goomena.com/goomena-kostas/first-general/black-meion.png';
        }
        catch (e) { }
    })();
    var profileBaseUrl = '<%= MyBase.LanguageHelper.GetPublicURL("/profile/", MyBase.GetLag()) %>';
    if (!stringIsEmpty(profileBaseUrl)) {
        if (profileBaseUrl.indexOf("?") > -1) profileBaseUrl = profileBaseUrl.substring(0, profileBaseUrl.indexOf("?"));
    }
    $("#lista1,#listaman1").toggle(function () {
        $("#lista2,#lista3,#lista4,#tiperimeneis,#listaman2,#listaman3,#listaman4,#tiperimeneisman").removeClass("hidden");
    },
    function () {
        $("#lista2,#lista3,#lista4,#tiperimeneis,#listaman2,#listaman3,#listaman4,#tiperimeneisman").addClass("hidden");
    });
    $("#lista1,#listaman1").toggle(function () {
        $("#lista1").css('background', 'url("//cdn.goomena.com/goomena-kostas/first-general/red-meion.png") no-repeat scroll -10px 0 transparent');
        $("#listaman1").css('background', 'url("//cdn.goomena.com/goomena-kostas/first-general/black-meion.png") no-repeat scroll -9px -1px transparent');
    }, function () {
        $("#lista1").css('background', 'url("//cdn.goomena.com/goomena-kostas/first-general/plus-red.png") no-repeat scroll -10px 0 transparent');
        $("#listaman1").css('background', 'url("//cdn.goomena.com/goomena-kostas/first-general/plus.png") no-repeat scroll -10px 0 transparent');
    });
    $("#listaman4").tooltip({ tooltipClass: "tooltipcustom" });
    var lag = document.cookie.split('LagID=');
    if (lag.length > 1) {
        if (lag[1].slice(0, 2) == "US") {
            $("#listaman4").tooltip({ content: "This offer represents the expenses a woman have to do in order to prepare herself for a date." });
        }
        else if (lag[1].slice(0, 2) == "GR") {
            $("#listaman4").tooltip({ content: "Η προσφορά αυτή , αντιστοιχεί στην ελάχιστη αμοιβή για την προετοιμασία της γυναίκας για να βγει ραντεβού." });
        }
        else if (lag[1].slice(0, 2) == "AL") {
            $("#listaman4").tooltip({ content: "Oferta ndikon për përgatitjen e vajzës për tu takuar." });
        }
    }

    $(".mosaic").children().mouseover(function (e) {
        $(".PhotoInfoWrap").removeClass("hidden");
        var loginName = $(this).attr("data-LoginName");
        var country = $(this).attr("data-Country");
        var age = $(this).attr("data-Age");
        var region = $(this).attr("data-Region");
        var city = $(this).attr("data-City");
        var bg = $(this).attr("data-URL");
        var gender = $(this).attr("data-Gender");
        $(".PhotoInfoWrap").click(function () {
            ShowLoading();
            var ln = loginName;
            if (typeof encodeURIComponent !== 'undefined') {
                ln = encodeURIComponent(loginName);
            }
            else if (typeof encodeURI !== 'undefined') {
                ln = encodeURI(loginName);
            }
            ln = ln.replace(".", "%2e").replace("+", "%20");
            window.location = profileBaseUrl + ln;
        }).css("cursor", "pointer");
        $(this).click(function () {
            ShowLoading();
            var ln = loginName;
            if (typeof encodeURIComponent !== 'undefined') {
                ln = encodeURIComponent(loginName);
            }
            else if (typeof encodeURI !== 'undefined') {
                ln = encodeURI(loginName);
            }
            ln = ln.replace(".", "%2e").replace("+", "%20");
            window.location = profileBaseUrl + ln;
        }).css("cursor", "pointer");

        $(".InfoImg").attr("src", bg);
        $(".PhotoInfoWrap").position({
            my: "left center",
            at: "top bottom",
            of: this
        });
        $(".PhotoInfoWrap").addClass("animate");
        $(".PhotoInfoWrap").css("z-index", "10");
        $(".ProfileID").html(loginName);

        if (!stringIsEmpty(region) && !stringIsEmpty(city) && !stringIsEmpty(country)) {
            $(".ProfileCity").html(region + ", " + city + ", " + country);
        }
        else if (!stringIsEmpty(city) && !stringIsEmpty(country)) {
            $(".ProfileCity").html(city + ", " + country);
        }
        else if (!stringIsEmpty(region) && !stringIsEmpty(country)) {
            $(".ProfileCity").html(region + ", " + country);
        }

        $(".ProfileAgeText").html("age" + '<br/>');
        $(".ProfileAge").html(age);
        if (gender == "1") {
            $(".ProfileID,.ProfileAgeWrap").css("background", "#13a8e4");
        }
        if (gender == "2") {
            $(".ProfileID,.ProfileAgeWrap").css("background", "#ED145B");
        }
    }).mouseleave(function () {
        $(".PhotoInfoWrap").removeClass("animate");
    })

//        $(".PhotoInfoWrap").mouseleave(function (e) {
//               $(".PhotoInfoWrap").addClass("animate");
//           });

    $(".PhotoInfoWrap").mouseover(function (e) { $(".PhotoInfoWrap").removeClass("hidden"); });
    $(".mosaic").mouseleave(function (e) { $(".PhotoInfoWrap").addClass("hidden"); });
    $(".maina").mouseenter(function () { $(".PhotoInfoWrap").addClass("hidden"); });
    $(".mosaic").mouseleave(function () { $(".PhotoInfoWrap").addClass("hidden"); });
    $(".registerform").mouseenter(function () { $(".PhotoInfoWrap").addClass("hidden"); });

//        $(".PhotoInfoWrap").mouseover(function (e) {
//        e.stopPropagation();
//        $(".PhotoInfoWrap").delay(1100).removeClass("animate");
//        }).mouseleave(function () {
//        $(".PhotoInfoWrap").addClass("hidden");
//    });

    function stringIsEmpty(s) {
        var b = (s == null || typeof s === 'undefined' || s == ''); return b;
    }

</script>                   
        <div class="mobilebanner20141010">
        <div class="Left">
        <div id="TriangleUp"></div>
        <div id="dvlblmobileText">
            <asp:Literal ID="lblmobileText2" runat="server" Text="Γνώρισε κόσμο από το κινητό σου"></asp:Literal>
            </div>
            <div id="mobileapplogo"></div>
            <div id="btnDownloadAppContainer">
                     <asp:HyperLink ID="btnDownloadApp" runat="server"
                        NavigateUrl="https://play.google.com/store/apps/details?id=com.goomena.app&hl=el" onclick="ShowLoading();" CssClass="btnDownloadApp" Target="_blank" >
                        Katebase th twra
                    </asp:HyperLink>
            </div>
             <a id="playstoreImg" href="https://play.google.com/store/apps/details?id=com.goomena.app&hl=el" target="_blank" >
        <img style="margin-left:auto;margin-right:auto;display:block;" alt="playstore-goomena" src="//cdn.goomena.com/Images/pub_default/20141010google-play.png"  runat="server" />
    </a>
            </div>
        <div class="Right">
            <div id="mobilePhoto"></div>
            <div id="TriangleDown"></div>
        </div>
        <div class="clear"></div>
    </div>
<%--<div class="mobilebanner" style=" width:1008px;margin-left:auto;margin-right:auto;display:block;margin-bottom:40px;"><a href="https://play.google.com/store/apps/details?id=com.goomena.app&hl=el" target="_blank"><img style="margin-left:auto;margin-right:auto;display:block;" src="../Images/MobileBanners/android-banner.png" id="iMobileBanner" runat="server" /></a></div>--%>
</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="cntBodyBottom" runat="server">
    <uc3:LiveZillaPublic ID="ctlLiveZillaPublic" runat="server" />
</asp:Content>

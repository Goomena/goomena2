﻿<%@ Page Title="" Language="vb" AutoEventWireup="true" MasterPageFile="~/Root.Master" CodeBehind="Prices.aspx.vb"  Inherits="Dating.Server.Site.Web.Prices" %>

<%@ Register src="~/UserControls/UCSelectProduct.ascx" tagname="UCSelectProduct" tagprefix="uc1" %>
<%@ Register src="~/UserControls/UCSelectProductTR.ascx" tagname="UCSelectProductTR" tagprefix="uc1" %>
<%@ Register src="~/UserControls/LiveZillaPublic.ascx" tagname="LiveZillaPublic" tagprefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<style>
.pricesnewcontainer
    {
        background: url("//cdn.goomena.com/goomena-kostas/first-general/bg-reapet.jpg") ;
        box-shadow: 0px 5px 5px 1px #c7c5c8 ;
        padding-bottom:65px; 
        border-top:6px solid #be202f;
    }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
<div class="pricesnewcontainer">
    <div class="mid_bg pageContainer">
        <div class="hp_cr" id="hp_top">
        </div>
        <div id="hp_content">
            <div class="container_12">
                <div class="cms_page">
                    <div class="cms_content">
                        <div class="ab_block">
                            <div class="pe_wrapper">
                                <div class="pe_box">
                                    <uc1:UCSelectProduct ID="ucProd" runat="server" visible="true" />
                                    <uc1:UCSelectProductTR ID="ucProdTR" runat="server" visible="false" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="clear">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="pageContainer" style="margin-top: 20px; display: none;">
        <div>
        </div>
    </div>
</div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cntBodyBottom" runat="server">
    <uc3:LiveZillaPublic ID="ctlLiveZillaPublic" runat="server" />
</asp:Content>

﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Root.Master" CodeBehind="SiteMap.aspx.vb" Inherits="Dating.Server.Site.Web.SiteMap" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
.sitemap{margin:50px auto 100px;width:983px;position:relative;}
.sitemap .content{position:relative;z-index:1;background:url(Images2/new/sitemap2.png) no-repeat scroll 70% 0; width:983px;font-size:15px;padding:20px 0 20px 20px;}
.sitemap .title{position:absolute;top:10px;right:10px;}
.sitemap .shadow{background-color:#f8f8f8;-ms-filter:"alpha(opacity=90)";filter:alpha(opacity=90);-moz-opacity:.9;-khtml-opacity:.9;opacity:.9;width:400px;}
.sitemap div.link a{text-decoration:none;line-height:25px;color:#000;}
.sitemap div.link a:hover{text-decoration:underline;}
.sitemap .ctlSiteMap.dxsmControl{
    /*
    background-color: transparent !important;
    font-family: "Segoe UI" , "Arial" !important;
    border: none !important;
    */
}
.sitemap div.sitemap-category{
    background:url(Images2/new/sitemap-plaisio.png) no-repeat scroll 0 0; 
    line-height:25px;
    height:41px;
    font-weight:bold;
    padding-left: 20px;
    padding-top: 5px;
    margin-left: -29px;
}
.sitemap div.sitemap-category p{
    margin:0px;
    padding:0px;
}
.sitemap .ctlSiteMap ul{
    list-style-type:disc !important;
    margin-left:0px !important;
}

.sitemap .ctlSiteMap ul li a{
    color: #0099cc;
    text-decoration:underline;
}
.sitemap .ctlSiteMap ul li p{
    margin:0px;
    padding:0px;
}
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="insidemosaiccontainer" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="content" runat="server">
    <div class="sitemap">
    <div class="content">
    
       <%-- <asp:ListView ID="lv1" runat="server">
            <ItemTemplate>
                <div class="link"><asp:HyperLink ID="lnk1" runat="server" Text='<%# Eval("myTitle") %>' NavigateUrl='<%# Eval("pageUrl") %>'></asp:HyperLink></div>
            </ItemTemplate>
            <LayoutTemplate>
                <div ID="itemPlaceholderContainer" runat="server">
                    <div runat="server" id="itemPlaceholder"></div>
                </div>
            </LayoutTemplate>
        </asp:ListView>--%>

        <div class="shadow">
	        <dx:ASPxSiteMapControl EnableViewState="False" Width="100%" ID="ctlSiteMap" 
                runat="server" EncodeHtml="false" EnableTheming="false" CssClass="ctlSiteMap" 
                EnableDefaultAppearance="False">
	        </dx:ASPxSiteMapControl>
        </div>

    </div>
    <div class="title">
        <asp:Label ID="lblTitle" runat="server" Text="Sitemap" style="font-size:30px;"></asp:Label>
    </div>
</div>

<dx:ASPxSiteMapDataSource ID="ASPxSiteMapDataSource1" runat="server" SiteMapProvider="ProductsSiteMapProvider" />

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cntBodyBottom" runat="server">
</asp:Content>

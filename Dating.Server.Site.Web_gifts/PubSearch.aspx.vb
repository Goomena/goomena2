﻿Imports Library.Public
Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Core.DLL
Imports System.Data.SqlClient

Public Class PubSearch
    Inherits BasePage


#Region "Props"

    Public Property SearchSorting As SearchSortEnum

        Get
            Dim sorting As SearchSortEnum = SearchSortEnum.RecentlyLoggedIn

            Dim index As Integer = cbSort.SelectedIndex
            If (index = 0) Then
                sorting = SearchSortEnum.NewestMember
            ElseIf (index = 1) Then
                sorting = SearchSortEnum.OldestMember
            ElseIf (index = 2) Then
                sorting = SearchSortEnum.RecentlyLoggedIn
            End If

            Return sorting
        End Get

        Set(value As SearchSortEnum)
            Dim index As Integer = 2

            If (value = SearchSortEnum.NewestMember) Then
                index = 0
            ElseIf (value = SearchSortEnum.OldestMember) Then
                index = 1
            ElseIf (value = SearchSortEnum.RecentlyLoggedIn) Then
                index = 2
            End If
            cbSort.SelectedIndex = index
        End Set

    End Property

    Public Property ItemsPerPage As Integer

        Get
            Dim perPageNumber As Integer = 10

            Dim index As Integer = cbPerPage.SelectedIndex
            If (index = 0) Then
                perPageNumber = 10
            ElseIf (index = 1) Then
                perPageNumber = 25
            ElseIf (index = 2) Then
                perPageNumber = 50
            End If

            Return perPageNumber
        End Get

        Set(value As Integer)
            Dim index As Integer = 0

            If (value = 25) Then
                index = 1
            ElseIf (value = 50) Then
                index = 2
            Else
                index = 0
            End If

            cbPerPage.SelectedIndex = index
        End Set

    End Property


    'Dim _pageData As clsPageData
    'Protected ReadOnly Property CurrentPageData As clsPageData
    '    Get
    '        If (_pageData Is Nothing) Then
    '            _pageData = New clsPageData(Context)
    '            AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
    '        End If
    '        Return _pageData
    '    End Get
    'End Property

    Dim _pageDataSearch As clsPageData
    Protected ReadOnly Property SearchPageData As clsPageData
        Get
            If (_pageDataSearch Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageDataSearch = New clsPageData("~/Members/Search.aspx", Context, coe)
            End If
            Return _pageDataSearch
        End Get
    End Property

#End Region


    Protected Overrides Sub Page_PreInit(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreInit
        SetMasterPage(CurrentPageData)
        MyBase.Page_PreInit(sender, e)

        'If clsCurrentContext.VerifyLogin() = True Then
        '    Me.MasterPageFile = "~/Members/Members2.Master"
        'End If

    End Sub



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Try
                Dim req As String = Request.QueryString("seo" & Me.Pager.ClientID)
                Dim currentPage As Integer
                If (Not String.IsNullOrEmpty(req)) Then
                    Try
                        req = req.Replace("page", "")
                        Integer.TryParse(req, currentPage)
                        Me.Pager.ItemCount = Me.ItemsPerPage * currentPage
                        If (Me.Pager.ItemCount > 0) Then Me.Pager.PageIndex = currentPage - 1
                    Catch ex As Exception
                    End Try
                End If
                If (currentPage > 2) Then
                    Response.Redirect("~/Register.aspx")
                End If
            Catch ex As System.Threading.ThreadAbortException
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try


            If (Not Me.IsPostBack OrElse IsLagChanged) Then
                LoadLAG()
                BindFilteredResults()
            End If

        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try
    End Sub


    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender

        Try
            Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_PreRender")
        End Try

    End Sub



    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
    End Sub


    Protected Sub LoadLAG()
        Try

            cbSort.Items.Clear()
            cbSort.Items.Add(SearchPageData.GetCustomString("cbSort_NewestMemberItem"), "NewestMember")
            cbSort.Items.Add(SearchPageData.GetCustomString("cbSort_OldestMemberItem"), "OldestMember")
            cbSort.Items.Add(SearchPageData.GetCustomString("cbSort_RecentlyLoggedInItem"), "RecentlyLoggedIn")
            'cbSort.Items.Add(SearchPageData.GetCustomString("cbSort_NearestDistanceItem"), "NearestDistance")
            cbSort.Items(0).Selected = True


            'Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = Me.CurrentPageData.cPageBasic
            'lblHeader.Text = Me.CurrentPageData.GetCustomString(lblHeader.ID)
            'lbHTMLBody.Text = cPageBasic.BodyHTM
            'lblinside.Text = Me.CurrentPageData.GetCustomString("lblinside")

            'Dim _popupTrigger1_ As String = Me.CurrentPageData.GetCustomString("_popupTrigger1_")
            'If (Not String.IsNullOrEmpty(_popupTrigger1_) AndAlso lbHTMLBody.Text.Contains("_popupTrigger1_")) Then
            '    '<IMG id="_popupTrigger1_" src="http://www.dating-deals.com/images/icon_tip.png">
            '    Dim win As New DevExpress.Web.ASPxPopupControl.PopupWindow(_popupTrigger1_)
            '    win.PopupElementID = "_popupTrigger1_"
            '    popupWindows.Windows.Add(win)
            'End If


            ''AppUtils.setSEOPageData(Me, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
            ''AppUtils.setNaviLabel(Me, "lbPublicNavi2Page", cPageBasic.PageTitle)
            'If (TypeOf Page.Master Is INavigation) Then
            '    CType(Page.Master, INavigation).AddNaviLink(Me.CurrentPageData.Title, "")
            'End If
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Private Sub BindFilteredResults()
        Try
            '   Dim sqlSet = False
            Dim sql As String = ""

            'Dim cmd As New SqlCommand()
            'cmd.Connection = New SqlConnection(ModGlobals.ConnectionString)

            Try
                Dim NumberOfRecordsToReturn As Integer = Me.GetRecordsToRetrieve()
                Dim prms As New clsSearchHelperParameters()
                prms.AdditionalWhereClause = sql
                prms.performCount = True
                prms.rowNumberMin = NumberOfRecordsToReturn - Me.ItemsPerPage
                prms.rowNumberMax = NumberOfRecordsToReturn
                prms.SearchSort = Me.SearchSorting
                FillPublicSearchParams(prms)

                Dim countRows As Integer
                Using ds As DataSet = clsSearchHelper.GetMembersToSearchDataTable_Public(prms)
                    If (prms.performCount) Then
                        countRows = ds.Tables(0).Rows(0)(0)
                        If (countRows > 0) Then
                            Using dt As DataTable = ds.Tables(1)
                                FillOfferControlList(dt, ctlSearch)
                            End Using

                        End If
                    Else
                        countRows = ds.Tables(0).Rows.Count
                        If (countRows > 0) Then
                            Using dt As DataTable = ds.Tables(0)
                                FillOfferControlList(dt, ctlSearch)
                            End Using

                        End If
                    End If
                End Using
                ctlSearch.DataBind()
                SetPager(countRows)

            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")

                SetPager(0)
                ctlSearch.UsersList = Nothing
                ctlSearch.DataBind()
            End Try
            'End If

        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Private Sub FillOfferControlList(dt As DataTable, offersControl As Dating.Server.Site.Web.PublicSearch)
        Try
            '' set pagination
            'Dim startingIndex As Integer = 0
            'Dim endIndex As Integer = Me.ItemsPerPage - 1
            'If (Me.Pager.PageIndex > 0) Then
            '    startingIndex = Me.ItemsPerPage * Me.Pager.PageIndex
            '    endIndex = (startingIndex + Me.ItemsPerPage) - 1
            'End If



            '     Dim drDefaultPhoto As DSMembers.EUS_CustomerPhotosRow = DataHelpers.GetProfilesDefaultPhoto(Me.MasterProfileId)
            '   Dim uliCurrentMember As New clsWinkUserListItem()
            'uliCurrentMember.ProfileID = Me.MasterProfileId ' 
            'uliCurrentMember.MirrorProfileID = Me.MirrorProfileId
            'uliCurrentMember.LoginName = Me.GetCurrentProfile().LoginName
            'uliCurrentMember.Genderid = Me.GetCurrentProfile().GenderId
            'uliCurrentMember.ProfileViewUrl = ResolveUrl("~/Members/Profile.aspx?p=" & Me.GetCurrentProfile().LoginName)

            'If (drDefaultPhoto IsNot Nothing) Then
            '    uliCurrentMember.ImageFileName = drDefaultPhoto.FileName
            '    uliCurrentMember.ImageUrl = ProfileHelper.GetProfileImageURL(drDefaultPhoto.CustomerID, drDefaultPhoto.FileName, Me.GetCurrentProfile().GenderId, True)
            '    uliCurrentMember.ImageThumbUrl = ProfileHelper.GetProfileImageURL(drDefaultPhoto.CustomerID, drDefaultPhoto.FileName, Me.GetCurrentProfile().GenderId, True)
            '    uliCurrentMember.ImageUploadDate = drDefaultPhoto.DateTimeToUploading.ToLocalTime().ToString("d MMM, yyyy H:mm")
            'Else
            '    uliCurrentMember.ImageUrl = ProfileHelper.GetDefaultImageURL(Me.GetCurrentProfile().GenderId)
            '    uliCurrentMember.ImageThumbUrl = uliCurrentMember.ImageUrl
            '    uliCurrentMember.ImageFileName = System.IO.Path.GetFileName(uliCurrentMember.ImageUrl)
            '    uliCurrentMember.ImageUploadDate = Nothing
            'End If



            Dim rowsCount As Integer
            For rowsCount = 0 To dt.Rows.Count - 1

                Try
                    Dim dr As DataRow = dt.Rows(rowsCount)
                    Dim uli As New clsWinkUserListItem()

                    uli.LAGID = Session("LAGID")
                    'uli.ProfileID = uliCurrentMember.ProfileID
                    'uli.MirrorProfileID = uliCurrentMember.MirrorProfileID
                    'uli.LoginName = uliCurrentMember.LoginName
                    'uli.Genderid = uliCurrentMember.Genderid
                    'uli.ProfileViewUrl = uliCurrentMember.ProfileViewUrl
                    'uli.ImageFileName = uliCurrentMember.ImageFileName
                    'uli.ImageUrl = uliCurrentMember.ImageUrl
                    'uli.ImageThumbUrl = uliCurrentMember.ImageThumbUrl
                    'uli.ImageUploadDate = uliCurrentMember.ImageUploadDate


                    If (dt.Columns.Contains("Birthday")) Then uli.OtherMemberBirthday = If(Not dr.IsNull("Birthday"), dr("Birthday"), Nothing)
                    uli.OtherMemberLoginName = dr("LoginName")
                    uli.OtherMemberProfileID = dr("ProfileID")
                    uli.OtherMemberMirrorProfileID = dr("MirrorProfileID")
                    uli.OtherMemberCity = IIf(Not dr.IsNull("City"), dr("City"), String.Empty)
                    uli.OtherMemberRegion = IIf(Not dr.IsNull("Region"), dr("Region"), String.Empty)
                    uli.OtherMemberCountry = IIf(Not dr.IsNull("Country"), dr("Country"), "")
                    uli.OtherMemberGenderid = dr("Genderid")
                    uli.OtherMemberHeading = IIf(Not dr.IsNull("AboutMe_Heading"), dr("AboutMe_Heading"), String.Empty)


                    If (Not dr.IsNull("PersonalInfo_HeightID")) Then
                        uli.OtherMemberHeight = ProfileHelper.GetHeightString(dr("PersonalInfo_HeightID"), Me.GetLag())
                    End If


                    If (Not dr.IsNull("PersonalInfo_BodyTypeID")) Then
                        uli.OtherMemberPersonType = ProfileHelper.GetBodyTypeString(dr("PersonalInfo_BodyTypeID"), Me.GetLag())
                    End If


                    If (Not dr.IsNull("PersonalInfo_HairColorID")) Then
                        uli.OtherMemberHair = ProfileHelper.GetHairColorString(dr("PersonalInfo_HairColorID"), Me.GetLag())
                    End If


                    If (Not dr.IsNull("PersonalInfo_EyeColorID")) Then
                        uli.OtherMemberEyes = ProfileHelper.GetEyeColorString(dr("PersonalInfo_EyeColorID"), Me.GetLag())
                    End If


                    If (Not dr.IsNull("PersonalInfo_EthnicityID") AndAlso dr("PersonalInfo_EthnicityID") > 1) Then
                        uli.OtherMemberEthnicity = ProfileHelper.GetEthnicityString(dr("PersonalInfo_EthnicityID"), Me.GetLag())
                    End If



                    If (Not dr.IsNull("Birthday")) Then
                        uli.OtherMemberAge = ProfileHelper.GetCurrentAge(dr("Birthday"))
                    Else
                        uli.OtherMemberAge = 20
                    End If


                    'If (Not dr.IsNull("FileName")) Then
                    '    uli.OtherMemberImageFileName = dr("FileName")
                    '    uli.OtherMemberImageUrl = ProfileHelper.GetProfileImageURL(dr("CustomerID"), uli.OtherMemberImageFileName, uli.OtherMemberGenderid, True, Me.IsHTTPS)
                    '    uli.OtherMemberImageThumbUrl = ProfileHelper.GetProfileImageURL(dr("CustomerID"), uli.OtherMemberImageFileName, uli.OtherMemberGenderid, True, Me.IsHTTPS)
                    'Else
                    '    uli.OtherMemberImageUrl = ProfileHelper.GetDefaultImageURL(uli.OtherMemberGenderid)
                    '    uli.OtherMemberImageThumbUrl = uli.OtherMemberImageUrl
                    '    uli.OtherMemberImageFileName = System.IO.Path.GetFileName(uli.OtherMemberImageUrl)
                    'End If
                    If (Not dr.IsNull("FileName")) Then uli.OtherMemberImageFileName = dr("FileName")
                    uli.OtherMemberImageUrl = ProfileHelper.GetProfileImageURL(uli.OtherMemberProfileID, uli.OtherMemberImageFileName, uli.OtherMemberGenderid, True, Me.IsHTTPS)
                    uli.OtherMemberImageThumbUrl = uli.OtherMemberImageUrl

                    uli.OtherMemberProfileViewUrl = ResolveUrl("~/Members/Profile.aspx?p=" & HttpUtility.UrlEncode(dr("LoginName")))

                    'uli.Distance = dr("distance")

                    'If (uli.Distance < gCarDistance) Then
                    '    uli.DistanceCss = "distance_car"
                    'Else
                    '    uli.DistanceCss = "distance_plane"
                    'End If



                    'Dim allowFavorite As Boolean = True
                    'If (dr("HasFavoritedMe") > 0 AndAlso dr("DidIFavorited") > 0) Then
                    '    ''''''''''''''''''''''''
                    '    ' both users has favorited, each other
                    '    ''''''''''''''''''''''''
                    '    allowFavorite = False

                    'ElseIf (dr("HasFavoritedMe") = 0 AndAlso dr("DidIFavorited") > 0) Then
                    '    ''''''''''''''''''''''''
                    '    ' current user has favorited, but the other not
                    '    ''''''''''''''''''''''''

                    '    allowFavorite = False
                    'ElseIf (dr("HasFavoritedMe") > 0 AndAlso dr("DidIFavorited") = 0) Then
                    '    ''''''''''''''''''''''''
                    '    ' current user has not favorited, but the other did
                    '    ''''''''''''''''''''''''

                    '    allowFavorite = True

                    'Else
                    '    ''''''''''''''''''''''''
                    '    ' none user has favorited, the other user
                    '    ''''''''''''''''''''''''

                    '    allowFavorite = True
                    'End If

                    'uli.AllowFavorite = allowFavorite
                    'uli.AllowUnfavorite = Not uli.AllowFavorite


                    'uli.AllowWink = True
                    'Try
                    '    Dim rec As Boolean = clsUserDoes.IsAnyWinkOrIsAnyOffer(uli.OtherMemberProfileID, Me.MasterProfileId)
                    '    uli.AllowWink = (Not rec)
                    'Catch ex As Exception
                    'End Try

                    '' if like enabled and users have exchanged at least one message, then disable like
                    'If (uli.AllowWink) Then
                    '    Try
                    '        Dim rec As Boolean = clsUserDoes.IsAnyMessageExchanged(uli.OtherMemberProfileID, Me.MasterProfileId)
                    '        uli.AllowWink = (Not rec)
                    '    Catch ex As Exception
                    '    End Try
                    'End If

                    'If (Not uli.AllowWink) Then uli.AllowUnWink = True

                    'uli.SendMessageUrl = ResolveUrl("~/Members/Conversation.aspx?p=" & HttpUtility.UrlEncode(uli.OtherMemberLoginName) & "&offerid=" & uli.OfferID)
                    'uli.SendMessageUrlOnce = ResolveUrl("~/Members/Conversation.aspx?send=once&p=" & HttpUtility.UrlEncode(uli.OtherMemberLoginName) & "&offerid=" & uli.OfferID)
                    'uli.SendMessageUrlMany = ResolveUrl("~/Members/Conversation.aspx?send=unl&p=" & HttpUtility.UrlEncode(uli.OtherMemberLoginName) & "&offerid=" & uli.OfferID)


                    'If (Me.IsMale AndAlso dr("CommunicationUnl") = 0) Then
                    '    Dim rec As EUS_Offer = clsUserDoes.GetLastOffer(uli.OtherMemberProfileID, Me.MasterProfileId)

                    '    If (rec Is Nothing) Then
                    '        uli.CreateOfferUrl = ResolveUrl("~/Members/CreateOffer.aspx?offerto=" & HttpUtility.UrlEncode(uli.OtherMemberLoginName))
                    '    End If

                    '    uli.AllowActionsMenu = True
                    '    uli.AllowActionsCreateOffer = True
                    '    uli.AllowActionsSendMessageOnce = True
                    '    uli.AllowActionsSendMessageMany = True
                    'Else
                    '    uli.AllowSendMessage = True
                    'End If

                    uli.AllowTooltipPopupSearch = True

                    offersControl.UsersList.Add(uli)
                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "")
                End Try

            Next


        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub



    Sub SetPager(itemsCount As Integer)
        Me.Pager.ItemCount = itemsCount
        Me.Pager.ItemsPerPage = Me.ItemsPerPage
        Me.Pager.Visible = (itemsCount > Me.ItemsPerPage)

        If (Me.Pager.PageIndex = -1) AndAlso (itemsCount > 0) Then
            Me.Pager.PageIndex = 0
        End If
    End Sub


    Function GetRecordsToRetrieve() As Integer
        Me.Pager.ItemsPerPage = Me.ItemsPerPage

        If (Me.Pager.PageIndex = -1) AndAlso (Me.Pager.ItemCount > 0) Then
            Me.Pager.PageIndex = 0
        End If

        If (Me.Pager.PageIndex > 0) Then
            Return (Me.Pager.ItemsPerPage * (Me.Pager.PageIndex + 1))
        End If
        Return Me.Pager.ItemsPerPage
    End Function



    Protected Sub Pager_PageIndexChanged(sender As Object, e As EventArgs) Handles Pager.PageIndexChanged
        Try
            If (Pager.PageIndex > 2) Then
                Response.Redirect("~/Register.aspx")
            Else
                BindFilteredResults()
            End If
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Protected Sub ddlSort_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbSort.SelectedIndexChanged
        Try
            Me.Pager.PageIndex = -1
            BindFilteredResults()
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Protected Sub ddlPerPage_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbPerPage.SelectedIndexChanged
        Try
            Me.Pager.PageIndex = -1
            BindFilteredResults()
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


End Class
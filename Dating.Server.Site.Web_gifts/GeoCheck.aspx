﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="GeoCheck.aspx.vb" Inherits="Dating.Server.Site.Web.GeoCheck" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="//cdn.goomena.com/Scripts/jquery-1.7.min.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:TextBox ID="txtIP" runat="server"></asp:TextBox><asp:Button ID="btnCheck" runat="server" Text="Check" />
    </div>
    <div>
        <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
    </div>
    <div>
        <asp:GridView ID="gvResult" runat="server"></asp:GridView>
    </div>

        <div class="module mod-line deepest" style="min-height: 254px;" id="divMapContainer" runat="server" visible="false">
            <h3 class="module-title">
                <asp:Label ID="msg_MemberPosition" runat="server" Text="" CssClass="member-position" />
            </h3>

<script type="text/javascript">
    function runExtLinks_PV() {
        var src = '<%= ifrUserMap_src %>';
        var mapIframe = '<iframe id="ifrUserMap" style="width:693px; height:350px;" src="' + src + '"></iframe>'
        $('#<%= divMapContainer.ClientID %>').append(mapIframe);
        $('#<%= msg_MemberPosition.ClientID %>').show();
    }

    $(function () {
        if (typeof runExtLinks_PV_Helper === 'function') {
            runExtLinks_PV_Helper()
        }
        else {
            setTimeout(runExtLinks_PV, 1500);
        }
    });
</script>

        </div>
    </form>
</body>
</html>

﻿<%@ Page Title="" Language="vb" AutoEventWireup="true" MasterPageFile="~/Root.Master" CodeBehind="Default.aspx.vb" 
Inherits="Dating.Server.Site.Web._Default20141010" %>


<%@ Register src="~/UserControls/UsersList.ascx" tagname="UsersList" tagprefix="uc1" %>
<%@ Register src="~/UserControls/LiveZillaPublic.ascx" tagname="LiveZillaPublic" tagprefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<link href="//cdn.goomena.com/CSS/Hint.css" rel="stylesheet" type="text/css" />
<%--
<link rel="stylesheet" href="//cdn.goomena.com/css/slide/slide.css" />
<link rel="stylesheet" href="//cdn.goomena.com/css/slide/custom.css" />
--%>
<link rel="stylesheet" href="//cdn.goomena.com/balance/media/widgetkit/widgets/slideshow/css/slideshow.css" />
<link rel="stylesheet" href="/v1/css/def-public-nogrid.css?theme8.3" />
    <script type="text/javascript">
        var ctlRdioMale = '<%= RdioMale.ClientID %>';
        var ctlRdioFemale= '<%= RdioFemale.ClientID%>';
        var ctlmainregdivctr = '<%= mainregdivctr.ClientID%>';
        var defaultReg_data_hint = "Please select Gender before you continue!";
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">

<div id="all_sections" class="themes theme8<%= If(DateTime.UtcNow < New DateTime(2015, 2, 22, 3, 0, 0), " carnaval", "")%>"><%--themes--%>
    <div class="section-1-ext">
        <div class="section-1">
            <div class="regformcontainer" id="regformcontainer" runat="server">
                <div class="regmidcontainer" id="regmidcontainer" runat="server">
                    <div class="reg-box">
                        <div class="registerform" id="registerform" runat="server">
                            <div class="mainregdiv">
                                <div class="notmember">
                                    <asp:Literal ID="lblNotAMember" runat="server"></asp:Literal>
                                </div>
                                <div runat="server" id="mainregdivctr" class="mainregdivctr" data-hint="Please select Gender before you continue!">
                                    <h1>
                                        <asp:Literal ID="lblChooseGender" runat="server"></asp:Literal>
                                    </h1>
                                
                                    <dx:ASPxRadioButton ID="RdioMale" runat="server" GroupName="selectgender" 
                                        Font-Size="16px" Text="" EncodeHtml="false">
                                        <ClientSideEvents CheckedChanged="gender_CheckedChanged" />
                                    </dx:ASPxRadioButton>
                                    <dx:ASPxRadioButton ID="RdioFemale" runat="server" GroupName="selectgender" 
                                        Font-Size="16px" Text="" EncodeHtml="false">
                                        <ClientSideEvents CheckedChanged="gender_CheckedChanged" />
                                    </dx:ASPxRadioButton>
                                
                                </div>
                                <div class="mainbemember-reg">
                                    <p class="mainbememberp"><asp:Literal ID="lblBeMemberNow" runat="server"></asp:Literal></p>
                                    <a id="HyperRegister" runat="server" href="Register.aspx" class="HyperRegister" onclick="return defaultRegisterClick();">
                                        <asp:Literal ID="lblRegister" runat="server"></asp:Literal>
                                    </a>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="mainalreadymembr">
                                <p>
                                    <asp:Literal ID="lblAlreadyMember" runat="server"></asp:Literal></p>
                                <a id="HyperConnect" runat="server" href="Login.aspx" onclick="ShowLoading();">
                                    <asp:Literal ID="lblConnect" runat="server"></asp:Literal></a>
                                <div class="clear"></div>
                            </div>
                            <div class="mainfblogindiv" style="position:relative;">
                               <%-- <img src="//cdn.goomena.com/goomena-kostas/first-general/facebook-logo.png" />--%>
                                <%--<a href="#">
                                    <asp:Literal ID="lblFBConnect" runat="server"></asp:Literal></a>--%>
                                <div class="clear"></div>
                                <div class="fb-connect-note">
                                    <asp:HyperLink ID="lblFBConnect" runat="server" Text="Login With Facebook"
                                     NavigateUrl="~/Register.aspx?log=fb" onclick="ShowLoading();" />
                                     <div class="wall-note-fb"><asp:Literal ID="lbFBNote" runat="server"></asp:Literal></div>
                                </div>
                            </div>
                             <div class="maingplogindiv" style="position:relative;">
                                <img src="//cdn.goomena.com/images2/pub2/g-icon_white.png" />
                                <%--<a href="#">
                                    <asp:Literal ID="lblFBConnect" runat="server"></asp:Literal></a>--%>
                                <div class="clear"></div>
                                <div class="gp-connect-note">
                                    <asp:HyperLink ID="lblGPConnect" runat="server" Text="Login With Google+"
                                     NavigateUrl="~/Register.aspx?log=google" onclick="ShowLoading();" />
                               </div>
                            </div>
                        </div>
                        <h1 class="reg-wellcome"><asp:Literal ID="lblWellcomeMsg" runat="server"></asp:Literal></h1>
                    </div>
                </div>
            </div>
           <div class="gendermsg"></div>         
           <div id="top-pic" class="top-pic <%= LagID%>" ></div>
        </div>
        <script type="text/javascript">
            $('.fb-connect-note', '#all_sections.themes')
                .mouseenter(function () {
                    var t = $('.fb-connect-note', '#all_sections.themes');
                    t.addClass("hovered");
                })
                .mouseleave(function () {
                    var t = $('.fb-connect-note', '#all_sections.themes');
                    t.removeClass("hovered");
                });
            $('.gp-connect-note', '#all_sections.themes')
               .mouseenter(function () {
                   var t = $('.gp-connect-note', '#all_sections.themes');
                   t.addClass("hovered");
               })
               .mouseleave(function () {
                   var t = $('.gp-connect-note', '#all_sections.themes');
                   t.removeClass("hovered");
               });
        </script>

    </div> 
    <div class="section-1">
        <div class="members-count">
            <asp:Literal ID="lblMembersCount" runat="server"></asp:Literal>
        </div>
    </div> 
    <div class="section-3-ext">
        <div class="section-3">
            <div class="members-photos">
                <asp:Panel ID="pnlMosaic" CssClass="mosaic" runat="server" EnableViewState="false">
		            <div runat="server" class="pic" id="pic1"></div>
		            <div runat="server" class="pic" id="pic2"></div>
		            <div runat="server" class="pic" id="pic3"></div>
		            <div runat="server" class="pic" id="pic4"></div>
		            <div runat="server" class="pic" id="pic5"></div>
		            <div runat="server" class="pic" id="pic6"></div>
		            <div runat="server" class="pic" id="pic7"></div>
		            <div runat="server" class="pic" id="pic8"></div>
		            <div runat="server" class="pic" id="pic9"></div>
		            <div runat="server" class="pic" id="pic10"></div>
		            <div runat="server" class="pic" id="pic11"></div>
		            <div runat="server" class="pic" id="pic12"></div>
		            <div runat="server" class="pic" id="pic13"></div>
		            <div runat="server" class="pic" id="pic14"></div>
		            <div runat="server" class="pic" id="pic15"></div>
		            <div runat="server" class="pic" id="pic16"></div>
		            <div runat="server" class="pic" id="pic17"></div>
		            <div runat="server" class="pic" id="pic18"></div>
		            <div runat="server" class="pic" id="pic19"></div>
                    <div class="clear"></div>
                 </asp:Panel> 
            </div>
        </div>
    </div> 
</div> 

    <div class="maina">
        <asp:Panel ID="pnlStartingText" CssClass="manwomancontainer" runat="server" EnableViewState="false">
            <div class="top-text-wrap">
                <div class="woman"><asp:Literal ID="ltrMenMessage" runat="server"></asp:Literal></div>
                <div class="man"><asp:Literal ID="ltrLadiesMessage" runat="server"></asp:Literal></div>
                <div class="clear"></div>
            </div>
            <div class="collapsible-text-wrap">
                <div class="woman" style="margin-top:0px;">
                    <asp:Literal ID="ltrMenMessageCollapse" runat="server"></asp:Literal>
                </div>
                <div class="man" style="margin-top:0px;">
                    <asp:Literal ID="ltrLadiesMessageCollapse" runat="server"></asp:Literal>
                </div>
                <div class="clear"></div>
               
            </div>
           
            <div class="clear"></div>
        </asp:Panel>
         <div class="trianglecontainer"><div id="triangleUp"></div></div>
        <div class="manwomancontainer2">
         <div class="ColouredBoxes" >
                    <div class="ColoredLadie">
                            <asp:Label ID="LblLadieMakeDate" runat="server" Text=""></asp:Label>

                         <div class="photos4-wrap">
                <div class="woman" style="margin-top:0px;">
                    <div class="photosforwomen">
                        <asp:HyperLink ID="anchornum1" runat="server" CssClass="photobox">
                            <div class="polaroid">
                                <p runat="server" id="imagename1" class="imagename" >
                                </p>
                                <img runat="server" id="imgtag1" class="imgtag" />
                            </div>
                        </asp:HyperLink>
                        <asp:HyperLink ID="anchornum2" runat="server"  CssClass="photobox">
                            <div class="polaroid">
                                <p runat="server" id="imagename2" class="imagename">
                                </p>
                                <img runat="server" id="imgtag2" class="imgtag" />
                            </div>
                        </asp:HyperLink>
                        <asp:HyperLink ID="anchornum3" runat="server"  CssClass="photobox">
                            <div class="polaroid">
                                <p runat="server" id="imagename3" class="imagename">
                                </p>
                                <img runat="server" id="imgtag3" class="imgtag" />
                            </div>
                        </asp:HyperLink>
                        <asp:HyperLink ID="anchornum4" runat="server"  CssClass="photobox">
                            <div class="polaroid">
                                <p runat="server" id="imagename4" class="imagename">
                                </p>
                                <img runat="server" id="imgtag4" class="imgtag" />
                            </div>
                        </asp:HyperLink>
                    </div>
                    <div class="clear"></div>
                </div>
                               <div class="clear"></div>
            </div>
                          <div class="RegisterButtoContainer">
                              <p class="noto"></p>
                                        <asp:HyperLink ID="lblRegBottomwm" runat="server"
                        NavigateUrl="~/Register.aspx" onclick="ShowLoading();" CssClass="btnRegister">
                        Κάνε εγγραφή τώρα
                    </asp:HyperLink>
        </div>
                        </div>
                   
                        <div class="ColoredMan">
        <asp:Label ID="LblManMakeDate" runat="server" Text=""></asp:Label>
           <div class="photos4-wrap">
                 <div class="man" style="margin-top:0px;">
                    <div class="photosformen">
                        <asp:HyperLink ID="anchornum5" runat="server" CssClass="photobox">
                            <div class="polaroid2">
                                <p runat="server" id="imagename5" class="imagename">
                                </p>
                                <img runat="server" id="imgtag5" class="imgtag" />
                            </div>
                        </asp:HyperLink>
                        <asp:HyperLink ID="anchornum6" runat="server"  CssClass="photobox">
                            <div class="polaroid2">
                                <p runat="server" id="imagename6" class="imagename">
                                </p>
                                <img runat="server" id="imgtag6" class="imgtag" />
                            </div>
                        </asp:HyperLink>
                        <asp:HyperLink ID="anchornum7" runat="server"  CssClass="photobox">
                            <div class="polaroid2">
                                <p runat="server" id="imagename7" class="imagename">
                                </p>
                                <img runat="server" id="imgtag7" class="imgtag" />
                            </div>
                        </asp:HyperLink>
                        <asp:HyperLink ID="anchornum8" runat="server"  CssClass="photobox">
                            <div class="polaroid2">
                                <p runat="server" id="imagename8" class="imagename">
                                </p>
                                <img runat="server" id="imgtag8" class="imgtag" />
                            </div>
                        </asp:HyperLink>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
            </div>
        <div class="RegisterButtoContainer">
                               <asp:HyperLink ID="lblRegBottomm" runat="server"
                        NavigateUrl="~/Register.aspx" onclick="ShowLoading();" CssClass="btnRegister">
                        Κάνε εγγραφή τώρα
                    </asp:HyperLink>
        </div>
            </div>
     <div class="clear"></div>
                   
  </div></div>
         <div class="trianglecontainer"><div id="triangleDown"></div></div>
    </div>
    <div class="clear"></div>

<div class="PhotoInfoWrap hidden">
    <div id="PhotoInfos" class="PhotoInfos">
        <img class="InfoImg" alt="" src="" />
    </div>
    <p class="ProfileID"></p>
    <p class="ProfileCity"></p>
    <div class="ProfileAgeWrap">
        <p class="ProfileAgeText"></p>
        <p class="ProfileAge"></p>
    </div>
</div>


<script type="text/javascript">
/* preload images*/
    (function () {
        try {
            var a = new Image(); a.src = '//cdn.goomena.com/goomena-kostas/first-general/red-meion.png';
            var b = new Image(); b.src = '//cdn.goomena.com/goomena-kostas/first-general/black-meion.png';
        }
        catch (e) { }
    })();
    var profileBaseUrl = '<%= MyBase.LanguageHelper.GetPublicURL("/profile/", MyBase.GetLag()) %>';
    if (!stringIsEmpty(profileBaseUrl)) {
        if (profileBaseUrl.indexOf("?") > -1) profileBaseUrl = profileBaseUrl.substring(0, profileBaseUrl.indexOf("?"));
    }

    function checkBorders(e) {
        var itmsExt = ['.section-1-ext', '.section-3-ext'];
        var itms = ['.section-1', '.section-3'];
        var widthCheck = [1424 + 20, 2004 + 20];

        for (var i = 0; i < itmsExt.length; i++) {
            var itmExt = $(itmsExt[i]);
            var itm = $(itms[i], itmsExt[i]);
            var w = itmExt.width();
            if (w >= widthCheck[i] && !itm.is('.ext-borders')) {
                itm.addClass('ext-borders');
            }
            else if (w < widthCheck[i] && itm.is('.ext-borders')) {
                itm.removeClass('ext-borders');
            }
        }
    }

    $(checkBorders);
    $(window).on('resize', checkBorders);

</script>  
<script type="text/javascript" src="/v1/Scripts/mosaic.js"></script>  
<script type="text/javascript">

    $("#lista1,#listaman1").toggle(function () {
        $("#lista2,#lista3,#lista4,#tiperimeneis,#listaman2,#listaman3,#listaman4,#tiperimeneisman").removeClass("hidden");
    },
    function () {
        $("#lista2,#lista3,#lista4,#tiperimeneis,#listaman2,#listaman3,#listaman4,#tiperimeneisman").addClass("hidden");
    });
    $("#lista1,#listaman1").toggle(function () {
        $("#lista1").css('background', 'url("//cdn.goomena.com/goomena-kostas/first-general/red-meion.png") no-repeat scroll -10px 0 transparent');
        $("#listaman1").css('background', 'url("//cdn.goomena.com/goomena-kostas/first-general/black-meion.png") no-repeat scroll -9px -1px transparent');
    }, function () {
        $("#lista1").css('background', 'url("//cdn.goomena.com/goomena-kostas/first-general/plus-red.png") no-repeat scroll -10px 0 transparent');
        $("#listaman1").css('background', 'url("//cdn.goomena.com/goomena-kostas/first-general/plus.png") no-repeat scroll -10px 0 transparent');
    });
    $("#listaman4").tooltip({ tooltipClass: "tooltipcustom" });
    var lag = document.cookie.split('LagID=');
    if (lag.length > 1) {
        if (lag[1].slice(0, 2) == "US") {
            $("#listaman4").tooltip({ content: "This offer represents the expenses a woman have to do in order to prepare herself for a date." });
        }
        else if (lag[1].slice(0, 2) == "GR") {
            $("#listaman4").tooltip({ content: "Η προσφορά αυτή , αντιστοιχεί στην ελάχιστη αμοιβή για την προετοιμασία της γυναίκας για να βγει ραντεβού." });
        }
        else if (lag[1].slice(0, 2) == "AL") {
            $("#listaman4").tooltip({ content: "Oferta ndikon për përgatitjen e vajzës për tu takuar." });
        }
    }

    function stringIsEmpty(s) {
        var b = (s == null || typeof s === 'undefined' || s == ''); return b;
    }

</script>                   
    <div class="mobilebanner20141017">
    <div class="mobilebanner20141010">
        <div class="Left">
        <div id="TriangleUp"></div>
        <div id="dvlblmobileText">
            <asp:Literal ID="lblmobileText2" runat="server" Text="Γνώρισε κόσμο από το κινητό σου"></asp:Literal>
            </div>
            <div id="mobileapplogo"></div>
            <div id="btnDownloadAppContainer">
                     <asp:HyperLink ID="btnDownloadApp" runat="server"
                        NavigateUrl="https://play.google.com/store/apps/details?id=com.goomena.app&hl=el" onclick="ShowLoading();" CssClass="btnDownloadApp" Target="_blank" >
                        Katebase th twra
                    </asp:HyperLink>
            </div>
             <a id="playstoreImg" href="https://play.google.com/store/apps/details?id=com.goomena.app&hl=el" target="_blank" >
        <img style="margin-left:auto;margin-right:auto;display:block;" alt="playstore-goomena" src="//cdn.goomena.com/Images/pub_default/20141010google-play.png"  runat="server" />
    </a>
            </div>
        <div class="Right">
            <div id="mobilePhoto"></div>
            <div id="TriangleDown"></div>
        </div>
        <div class="clear"></div>
    </div></div>
</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="cntBodyBottom" runat="server">
    <uc3:LiveZillaPublic ID="ctlLiveZillaPublic" runat="server" />
</asp:Content>

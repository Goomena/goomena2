﻿<%@ Page Language="vb" AutoEventWireup="true" MasterPageFile="~/Root.Master" CodeBehind="Register.aspx.vb"
    Inherits="Dating.Server.Site.Web.Register3" MaintainScrollPositionOnPostback="false" %>
<%@ Register Src="UserControls/DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<%@ Register src="~/UserControls/LiveZillaPublic.ascx" tagname="LiveZillaPublic" tagprefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<link rel="stylesheet" href="/v1/css/register3.css?v14" />
<script type="text/javascript" src="//cdn.goomena.com/v1/Scripts/register3.js?v10"></script>
<script type="text/javascript">

 
    //function BindEvents() {
    //    $("body").on(")
    //}


// <![CDATA[
    try{
        Login_checkChars=<%= Dating.Server.Site.Web.AppUtils.JS_ConverToArray(MyBase.UserNameInvalidStrings)%>;
    }
    catch(e){}

    try{
        LoginWithGoogle = '<%= ResolveUrl("~/LoginWithGoogle.aspx")%>';
        passwordValidation_DiffThanLogin = '<%= MyBase.CurrentPageData.GetCustomStringJS("Error.Password.DifferentThanUsername")%>';
        Login_Validation = '<%= Dating.Server.Site.Web.AppUtils.JS_PrepareString(txtLogin.ValidationSettings.ErrorText)%>';
        Login_Required = '<%= MyBase.CurrentPageData.GetCustomStringJS("Login.Required.Error.Text")%>';
        Height_Required = '<%= MyBase.CurrentPageData.GetCustomStringJS("Height.Validation.ErrorText")%>';
        BodyType_Required = '<%= MyBase.CurrentPageData.GetCustomStringJS("BodyType.Validation.ErrorText")%>';
        EyeColor_Required = '<%= MyBase.CurrentPageData.GetCustomStringJS("EyeColor.Validation.ErrorText")%>';
        HairClr_Required = '<%= MyBase.CurrentPageData.GetCustomStringJS("HairClr.Validation.ErrorText")%>';
  
        Region_Required = '<%= MyBase.CurrentPageData.GetCustomStringJS("lblRegionErr")%>';
        City_Required = '<%= MyBase.CurrentPageData.GetCustomStringJS("lblCityErr")%>';
        Zip_Required = '<%= MyBase.CurrentPageData.GetCustomStringJS("lblZipErr")%>';
        Birthday_Required = '<%= MyBase.CurrentPageData.GetCustomStringJS("ctlBirthday_Required_ErrorText")%>';
        Birthday_Invalid = '<%= MyBase.CurrentPageData.GetCustomStringJS("ctlBirthday_InvalidDate_ErrorText")%>';
        PassCheck1 = '<%= MyBase.CurrentPageData.GetCustomStringJS("Password.Validation.ErrorText2")%>';
        CheckingEmail = '<%= MyBase.CurrentPageData.GetCustomStringJS("Checking.Email.On.Server.Wait")%>';
        CheckingUserName = '<%= MyBase.CurrentPageData.GetCustomStringJS("Checking.UserName.On.Server.Wait")%>';
        PasswordConfirmationRequired = '<%= MyBase.CurrentPageData.GetCustomStringJS("Password.Confirmation.Required")%>';
        CheckingOnSrv = '<%= MyBase.CurrentPageData.GetCustomStringJS("Checking.On.Server.Wait")%>';
    }
    catch(e){};

    $(function () {
        RegValidation.fillValidatingElements();
        $('#<%= btnRegister.ClientID %>')
        .mousedown(function () { 
            RegisterButtonClicked = true; 
        });
    });

    function  GetCheckBoxValueVal(s, e){
        var value = s.GetChecked();
        if (value==true){
            btnRegister.SetEnabled(true);
            $('#<%= btnRegister.ClientID %>').removeClass("register-btnDis");
            $('#<%= btnRegister.ClientID %>').addClass("register-btn");//register-btnDis
        }else{
            btnRegister.SetEnabled(false);
            $('#<%= btnRegister.ClientID %>').removeClass("register-btn");
            $('#<%= btnRegister.ClientID %>').addClass("register-btnDis");//register-btnDis
        }
       
      

    }

    function btnRegister_Init(s,e){
        setTimeout(function(){
            $('.form-actions').show();
            btnRegister.SetEnabled(false);
            //$('#<%= btnRegister.ClientID %>')[0].click();
        },500);
    }
// ]]> 
</script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">

<asp:UpdatePanel ID="updRegister" runat="server">
    <ContentTemplate>

<div id="register-sections">
    <div class="mainregistercon">
        <div class="regmidcont">
            <div class="registernewcontainer">

                <asp:Panel ID="Panel1" runat="server" DefaultButton="btnRegister">
                    <h2 style="text-shadow: none; padding-top: 0px; padding-left: 30px; border: none;
                        font-family: Lucida Grande,Lucida Sans Unicode,Verdana,Arial,sans-serif;">
                        <asp:Label ID="msg_PrompToBegin" runat="server" CssClass="loginHereTitle" /></h2>

                    <div style="margin:-49px 20px 14px 148px;">
                        <div class="lfloat">
                            <dx:ASPxHyperLink ID="lbLoginGp" runat="server" CssClass="js gplus-login" EncodeHtml="false"
                                Text="Login with Google+" EnableDefaultAppearance="False" EnableTheming="False"
                                NavigateUrl="~/Register.aspx?log=google" ImageUrl="//cdn.goomena.com/images/join-with-google.png" ClientSideEvents-Click="function(){ShowLoading();}" />
                        </div>
                        <div class="rfloat">
                            <dx:ASPxHyperLink ID="lbLoginFB" runat="server" CssClass="js facebook-login" EncodeHtml="false"
                                Text="Login With Facebook" EnableDefaultAppearance="False" EnableTheming="False"
                                NavigateUrl="~/Register.aspx?log=fb" ImageUrl="//cdn.goomena.com/images/join-with-facebook.png" ClientSideEvents-Click="function(){ShowLoading();}" />
                        </div>
                        <div class="clear">
                        </div>
                    </div>
                    <asp:BulletedList ID="serverValidationList" runat="server" Font-Size="14px" ForeColor="Red"
                        DisplayMode="HyperLink" CssClass="BulletedList">
                    </asp:BulletedList>
                    <dx:ASPxValidationSummary ID="valSumm" runat="server" ValidationGroup="RegisterGroup"
                        RenderMode="BulletedList" ShowErrorsInEditors="True" EncodeHtml="False" Visible="false">
                        <Paddings PaddingBottom="10px" />
                        <LinkStyle>
                            <Font Size="14px"></Font>
                        </LinkStyle>
                        <ClientSideEvents VisibilityChanged="valSumm_VisibilityChanged_v2"/>
                    </dx:ASPxValidationSummary>
                    <asp:Panel ID="pnlContinueMessageCompleteForm" runat="server" CssClass="alert alert-success"
                        Visible="False">
                        <br /> 
                        <div style="padding-top:10px;padding-bottom:10px;">
                        <asp:Label ID="lblContinueMessageCompleteForm" runat="server" Text="" Font-Bold="true"></asp:Label></div>
                    </asp:Panel>

                    <div class="pe_form pe_login">
                        <div class="reglogintable">

                            <div runat="server" id="TrGender" clientidmode="Static">
                                <div class="lfloat check-wrap">
                                    <dx:ASPxRadioButtonList ID="rblGender" runat="server" RepeatLayout="Flow" ClientInstanceName="RegisterGenderRadioButtonList" EncodeHtml="false">
                                        <Paddings PaddingLeft="0px" />
                                        <ValidationSettings ValidationGroup="RegisterGroup" ErrorDisplayMode="ImageWithTooltip"
                                            ErrorTextPosition="Right" Display="None">
                                            <RequiredField ErrorText="" IsRequired="True" />
                                        </ValidationSettings>
                                        <Border BorderStyle="None" />
                                        <RadioButtonStyle BackgroundImage-HorizontalPosition="center" HorizontalAlign="Center">
                                            <BackgroundImage HorizontalPosition="center"></BackgroundImage>
                                        </RadioButtonStyle>
                                        <ClientSideEvents Validation="field_Validation_v2" />
                                    </dx:ASPxRadioButtonList>
                                    <img src="//cdn.goomena.com/Images/spacer10.png" alt="check" width="21" height="21" class="check hidden"/>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="fields-separator"></div>

                            <div runat="server" id="TrMyEmail" class="reg-field" clientidmode="Static">
                                <asp:Label ID="msg_MyEmail" runat="server" Text="" class="registerTableLabel" AssociatedControlID="txtEmail" CssClass="lblform" />
                                <div class="lfloat login-input-wrap RegisterEmailTextBox">
                                    <dx:ASPxTextBox ID="txtEmail" runat="server" ClientInstanceName="RegisterEmailTextBox"
                                         CssClass="txtEmail" Text="">
                                        <ValidationSettings ValidationGroup="RegisterGroup" ErrorDisplayMode="ImageWithTooltip"
                                            ErrorTextPosition="Right" Display="None">
                                            <RequiredField IsRequired="True" ErrorText="" />
                                            <RegularExpression ValidationExpression="" ErrorText="" />
                                        </ValidationSettings>
                                        <ClientSideEvents Validation="field_Validation_v2" />
                                    </dx:ASPxTextBox>
                                    <img src="//cdn.goomena.com/Images/spacer10.png" alt="check" width="21" height="21" class="check hidden"/>
                                </div>
                                <div class="clear"></div>
                                <asp:Label ID="mailValidation" runat="server" Text="" CssClass="mailValidation"></asp:Label>
                            </div>
                            <div class="fields-separator"></div>

                            <div runat="server" id="TrMyUsername" class="reg-field" clientidmode="Static">
                                <asp:Label ID="msg_MyUsername" runat="server" Text="" class="registerTableLabel"
                                    AssociatedControlID="txtLogin" CssClass="lblform" />
                                <div class="lfloat login-input-wrap RegisterLoginTextBox">
                                    <dx:ASPxTextBox ID="txtLogin" runat="server" ClientInstanceName="RegisterLoginTextBox"
                                         CssClass="txtLogin">
                                        <ValidationSettings ValidationGroup="RegisterGroup" ErrorDisplayMode="ImageWithTooltip"
                                            ErrorTextPosition="Right" ErrorText="" Display="None">
                                            <RequiredField ErrorText="" IsRequired="True" />
                                            <RegularExpression ErrorText="Το όνομα χρήστη πρέπει να αποτελείται απο 3 μέχρι 12 χαρακτήρες και να περιλαμβάνει τουλάχιστον ένα γράμμα και έναν αριθμό." />
                                        </ValidationSettings>
                                        <ClientSideEvents Validation="field_Validation_v2" />
                                    </dx:ASPxTextBox>
                                    <img src="//cdn.goomena.com/Images/spacer10.png" alt="check" width="21" height="21" class="check hidden"/>
                                </div>
                                <div class="clear"></div>
                                <asp:Label ID="usernameValidation" runat="server" Text="" CssClass="usernameValidation"></asp:Label>
                            </div>
                            <div class="fields-separator"></div>

                            <div runat="server" id="TrMyPassword" class="reg-field" clientidmode="Static">
                                <asp:Label ID="msg_MyPassword" runat="server" Text="" class="registerTableLabel"
                                    AssociatedControlID="txtPasswrd" CssClass="lblform" />
                                <div class="lfloat login-input-wrap RegisterPassword-wrap">
                                    <dx:ASPxTextBox ID="txtPasswrd" runat="server" Password="True" ClientInstanceName="RegisterPassword"
                                         CssClass="txtPasswrd">
                                        <ValidationSettings ValidationGroup="RegisterGroup" ErrorDisplayMode="ImageWithTooltip"
                                            ErrorText="" ErrorTextPosition="Right" Display="None">
                                            <RequiredField ErrorText="" IsRequired="True" />
                                            <RegularExpression ErrorText="Ο κωδικός πρέπει να αποτελείται απο 3 μέχρι 20 χαρακτήρες και να περιλαμβάνει τουλάχιστον ένα γράμμα και έναν αριθμό." />
                                        </ValidationSettings>
                                        <ClientSideEvents Validation="field_Validation_v2" />
                                    </dx:ASPxTextBox>
                                    <img src="//cdn.goomena.com/Images/spacer10.png" alt="check" width="21" height="21" class="check hidden"/>
                                </div>
                                <div class="clear"></div>
                                <asp:Label ID="passwordValidation" runat="server" Text="" CssClass="passwordValidation"></asp:Label>
                            </div>
                            <div class="fields-separator"></div>

                            <div runat="server" id="TrMyPasswordConfirm" class="reg-field" clientidmode="Static">
                                <asp:Label ID="msg_MyPasswordConfirm" runat="server" Text="" class="registerTableLabel"
                                    AssociatedControlID="txtPasswrd1Conf" CssClass="lblform" />
                                <div class="lfloat login-input-wrap RegisterPasswordConfirm-wrap">
                                    <dx:ASPxTextBox ID="txtPasswrd1Conf" runat="server" Password="True" ClientInstanceName="RegisterPasswordConfirm">
                                        <ValidationSettings ValidationGroup="RegisterGroup" ErrorDisplayMode="ImageWithTooltip"
                                            ErrorTextPosition="Right" ErrorText="" Display="None">
                                            <RequiredField ErrorText="" IsRequired="True" />
                                        </ValidationSettings>
                                        <ClientSideEvents Validation="field_Validation_v2" />
                                    </dx:ASPxTextBox>
                                    <img src="//cdn.goomena.com/Images/spacer10.png" alt="check" width="21" height="21" class="check hidden"/>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="fields-separator"></div>

                            <div runat="server" id="TrMyBirthdate" clientidmode="Static">
                                <asp:Label ID="msg_MyBirthdate" runat="server" Text="" class="registerTableLabel" CssClass="lblform" />
                                <div class="lfloat birth-wrap" style="padding-bottom:21px;">
                                    <uc1:DateControl Height="28px" FontSize="14px" ID="ctlDate" runat="server" DateTime='<%# Bind("Birthday") %>' YearsFromNow="-18" YearsTillNow="100"  />
                                    <img src="//cdn.goomena.com/Images/spacer10.png" alt="check" width="21" height="21" class="check hidden"/>
                                    <div style="position:absolute;left:0px;bottom:0px;z-index:10;width:100%;white-space:nowrap;"><a href="javascript:void(1);" runat="server" id="lnkMorePopup" title="Click for more information" class="birthday-info">Why do I need to provide my birthday?</a></div>

<dx:ASPxPopupControl ID="pcMoreBirthday" runat="server"  ClientInstanceName="pcMoreBirthday"
    CloseAction="OuterMouseClick" PopupAction="LeftMouseClick" 
    PopupElementID="lnkMorePopup" PopupVerticalAlign="Above" 
    ShowHeader="False" ShowFooter="true" Width="300px">                        
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl10" runat="server">
<asp:Label ID="lblMoreBirthday" runat="server" Text=""></asp:Label>
            </dx:PopupControlContentControl>
        </ContentCollection>
        <ContentStyle>
            <Paddings Padding="10px" />
        </ContentStyle>
        <HeaderStyle>
        <Paddings Padding="10px" />
        </HeaderStyle>
        <FooterStyle>
        <Paddings Padding="10px" />
        </FooterStyle>
    <FooterTemplate>
        <div class="popup-close" style="text-align:right"><a href="javascript:void(0)" onclick="pcMoreBirthday.HideWindow()">okay&nbsp;<img src="//cdn.goomena.com/Images/spacer10.png" alt="check" width="17" height="16"/></a></div>
    </FooterTemplate>
</dx:ASPxPopupControl>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="fields-separator" style="margin-bottom:10px;"></div>


                        </div>
                        <dx:ASPxCallbackPanel Style="background: white;" ID="cbpnlZip" runat="server" ClientInstanceName="cbpnlZip"
                            CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" ShowLoadingPanel="False"
                            ShowLoadingPanelImage="False">
                            <ClientSideEvents 
                                EndCallback="cbpnlZip_EndCallback_v2" 
                                CallbackError="cbpnlZip_CallbackError" 
                                BeginCallback="cbpnlZip_BeginCallback" />

                            <LoadingPanelImage Url="~/App_Themes/DevEx/Web/Loading.gif">
                            </LoadingPanelImage>
                            <LoadingPanelStyle ImageSpacing="5px">
                            </LoadingPanelStyle>
                            <PanelCollection>
                                <dx:PanelContent ID="PanelContent1" runat="server" SupportsDisabledAttribute="True">
                                    <asp:HiddenField ID="hdfLocationStatus" runat="server" />
                                    <div>

                                        <div runat="server" id="TrCountry" class="reg-field" clientidmode="Static">
                                            <asp:Label ID="msg_CountryText" runat="server" Text="" class="registerTableLabel"
                                                AssociatedControlID="cbCountry" CssClass="lblform" />
                                            <div class="lfloat login-input-wrap">
                                                <dx:ASPxComboBox ID="cbCountry" runat="server"  Font-Size="14px" Height="18px"
                                                    EncodeHtml="False" IncrementalFilteringMode="StartsWith" ClientInstanceName="RegisterCountry">
                                                    <ValidationSettings CausesValidation="True" Display="Dynamic" ValidationGroup="RegisterGroup">
                                                    </ValidationSettings>
                                                    <ClientSideEvents 
                                                        SelectedIndexChanged="cbCountry_SelectedIndexChanged"
                                                        EndCallback="cbCountry_EndCallback" />
                                                </dx:ASPxComboBox>
                                                <img src="//cdn.goomena.com/Images/spacer10.png" alt="check" width="21" height="21" class="check hidden"/>
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="fields-separator"></div>

                                        <div runat="server" id="TrZip" class="reg-field" clientidmode="Static">
                                            <asp:Label ID="msg_ZipCodeText" runat="server" CssClass="lblform" Style="height: 30px"
                                                for="zip" />
                                            <div class="lfloat login-input-wrap">
                                                <div class="lfloat">
                                                    <dx:ASPxTextBox ID="txtZip" runat="server" autocomplete="off"  Font-Size="14px" Height="18px" ClientInstanceName="RegisterZip">
                                                        <ClientSideEvents LostFocus="txtZip_LostFocus" />
                                                        <ValidationSettings ValidationGroup="RegisterGroup" ErrorDisplayMode="ImageWithTooltip"
                                                            ErrorTextPosition="Right" Display="None">
                                                            <RequiredField ErrorText="" IsRequired="True" />
                                                        </ValidationSettings>
                                                    </dx:ASPxTextBox>
                                                </div>
                                                <div class="lfloat popup-close" style="text-align:right"><a href="javascript:void(0)" onclick="CheckZip()">&nbsp;find&nbsp;</a></div>
                                                <div class="clear"></div>
                                                <img src="//cdn.goomena.com/Images/spacer10.png" alt="check" width="21" height="21" class="check hidden"/>
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                        <div style="margin-left:141px;">
                                            <a href="javascript:void(1);" onclick="showRegionCity()" runat="server" id="btnForgotZip" class="show-region-city" clientidmode="Static">I don't remember postal code...</a>
                                        </div>
                                        <div class="fields-separator"></div>
                                        <div runat="server" id="TrRegion" class="reg-field" clientidmode="Static">
                                            <asp:Label ID="msg_RegionText" runat="server" Text="" class="registerTableLabel"
                                                AssociatedControlID="cbRegion" CssClass="lblform" />
                                            <div class="lfloat login-input-wrap">
                                                <dx:ASPxComboBox ID="cbRegion" runat="server" ClientInstanceName="cbRegion" 
                                                     Font-Size="14px" Height="18px"
                                                    EncodeHtml="False" EnableSynchronization="False"
                                                    IncrementalFilteringMode="StartsWith" DataSourceID="sdsRegion" TextField="region1"
                                                    ValueField="region1">
                                                    <ClientSideEvents 
                                                        SelectedIndexChanged="cbRegion_SelectedIndexChanged" 
                                                        EndCallback="cbRegion_EndCallback" />
                                                    <ValidationSettings ValidationGroup="RegisterGroup" ErrorDisplayMode="ImageWithTooltip"
                                                        ErrorTextPosition="Right" Display="None">
                                                        <RequiredField ErrorText="" IsRequired="True" />
                                                    </ValidationSettings>
                                                </dx:ASPxComboBox>
                                                <img src="//cdn.goomena.com/Images/spacer10.png" alt="check" width="21" height="21" class="check hidden"/>
                                            </div>
                                            <div class="clear"></div>
                                            <asp:SqlDataSource ID="sdsRegion" runat="server" ConnectionString="<%$ ConnectionStrings:AppDBconnectionString %>">
                                            </asp:SqlDataSource>
                                        </div>
                                        <div class="fields-separator"></div>

                                        <div runat="server" id="TrCity" class="reg-field" clientidmode="Static">
                                            <asp:Label ID="msg_CityText" runat="server" Text="" CssClass="lblform"
                                                AssociatedControlID="cbCity" />
                                            <div class="lfloat login-input-wrap">
                                                <dx:ASPxComboBox ID="cbCity" runat="server" DataSourceID="sdsCity" 
                                                    ClientInstanceName="cbCity" TextField="city" ValueField="city" 
                                                     Font-Size="14px" Height="18px"
                                                    EncodeHtml="False" EnableSynchronization="False"
                                                    IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True">
                                                    <ClientSideEvents 
                                                        SelectedIndexChanged="cbCity_SelectedIndexChanged" />
                                                    <ValidationSettings ValidationGroup="RegisterGroup" ErrorDisplayMode="ImageWithTooltip"
                                                        ErrorTextPosition="Right" Display="None">
                                                        <RequiredField ErrorText="" IsRequired="True" />
                                                    </ValidationSettings>
                                                </dx:ASPxComboBox>
                                                <img src="//cdn.goomena.com/Images/spacer10.png" alt="check" width="21" height="21" class="check hidden"/>
                                            </div>
                                            <div class="clear"></div>
                                            <asp:SqlDataSource ID="sdsCity" runat="server" ConnectionString="<%$ ConnectionStrings:AppDBconnectionString %>">
                                            </asp:SqlDataSource>
                                        </div>

                                    </div>
                                </dx:PanelContent>
                            </PanelCollection>
                        </dx:ASPxCallbackPanel>
                        <div class="fields-separator"></div>

                        <div id="TrHeight" class="reg-field">
                            <asp:Label ID="msg_PersonalHeight" runat="server" Text="" CssClass="lblform"
                                AssociatedControlID="cbHeight" />
                            <div class="lfloat login-input-wrap" >
                                <dx:ASPxComboBox ID="cbHeight" runat="server" EncodeHtml="False"  Font-Size="14px"
                                    Height="18px" ClientInstanceName="cbHeight">
                                    <ClientSideEvents Validation="field_Validation_v2" />
                                    <ValidationSettings ValidationGroup="RegisterGroup" ErrorDisplayMode="ImageWithTooltip"
                                        ErrorTextPosition="Right" Display="None">
                                        <RequiredField ErrorText="" IsRequired="True" />
                                    </ValidationSettings>
                                </dx:ASPxComboBox>
                                <img src="//cdn.goomena.com/Images/spacer10.png" alt="check" width="21" height="21" class="check hidden"/>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="fields-separator"></div>

                        <div id="TrBodyType" class="reg-field">
                            <asp:Label ID="msg_PersonalBodyType" runat="server" Text="" CssClass="lblform"
                                AssociatedControlID="cbBodyType" />
                            <div class="lfloat login-input-wrap" >
                                <dx:ASPxComboBox ID="cbBodyType" runat="server"  Font-Size="14px" Height="18px"
                                    EncodeHtml="False" ClientInstanceName="cbBodyType">
                                    <ClientSideEvents Validation="field_Validation_v2" />
                                    <ValidationSettings ValidationGroup="RegisterGroup" ErrorDisplayMode="ImageWithTooltip"
                                        ErrorTextPosition="Right" Display="None">
                                        <RequiredField ErrorText="" IsRequired="True" />
                                    </ValidationSettings>
                                </dx:ASPxComboBox>
                                <img src="//cdn.goomena.com/Images/spacer10.png" alt="check" width="21" height="21" class="check hidden"/>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="fields-separator"></div>

                        <div id="TrEyeColor" class="reg-field">
                            <asp:Label ID="msg_PersonalEyeClr" runat="server" Text="" CssClass="lblform"
                                AssociatedControlID="cbEyeColor" />
                            <div class="lfloat login-input-wrap">
                                <dx:ASPxComboBox ID="cbEyeColor" runat="server"  Font-Size="14px" Height="18px"
                                    EncodeHtml="False" ClientInstanceName="cbEyeColor">
                                    <ClientSideEvents Validation="field_Validation_v2" ValueChanged="function(s, e) {
	sfd
}" />
                                    <ValidationSettings ValidationGroup="RegisterGroup" ErrorDisplayMode="ImageWithTooltip"
                                        ErrorTextPosition="Right" Display="None">
                                        <RequiredField ErrorText="" IsRequired="True" />
                                    </ValidationSettings>
                                </dx:ASPxComboBox>
                                <img src="//cdn.goomena.com/Images/spacer10.png" alt="check" width="21" height="21" class="check hidden"/>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="fields-separator"></div>

                        <div id="TrHairClr" class="reg-field">
                            <asp:Label ID="msg_PersonalHairClr" runat="server" Text="" CssClass="lblform"
                                AssociatedControlID="cbHairClr" />
                            <div class="lfloat login-input-wrap" >
                                <dx:ASPxComboBox ID="cbHairClr" runat="server"  Font-Size="14px" Height="18px"
                                    EncodeHtml="False" ClientInstanceName="cbHairClr">
                                    <ClientSideEvents Validation="field_Validation_v2" />
                                    <ValidationSettings ValidationGroup="RegisterGroup" ErrorDisplayMode="ImageWithTooltip"
                                        ErrorTextPosition="Right" Display="None">
                                        <RequiredField ErrorText="" IsRequired="True" />
                                    </ValidationSettings>
                                </dx:ASPxComboBox>
                                <img src="//cdn.goomena.com/Images/spacer10.png" alt="check" width="21" height="21" class="check hidden"/>
                            </div>
                            <div class="clear"></div>
                        </div>

                    </div>
                    <div>
                        <div runat="server" id="TrAgreements" style="margin-top:15px;">
                            <div class="form_right form_tos">
                                <asp:Label ID="msg_AgreementsText" runat="server" Visible="False"  />
                                <dx:ASPxCheckBox ID="cbAgreements" runat="server" ClientInstanceName="RegisterAgreementsCheckBox"
                                    EncodeHtml="False" Font-Bold="True" CheckState="Unchecked">
                                    <ClientSideEvents Validation="field_Validation_v2" ValueChanged="function(s, e) {
	GetCheckBoxValueVal(s, e);
}" />
                                    <ValidationSettings ValidationGroup="RegisterGroup" ErrorDisplayMode="ImageWithTooltip"
                                        ErrorTextPosition="Left">
                                        <RequiredField ErrorText="" IsRequired="True" />
                                    </ValidationSettings>
                                </dx:ASPxCheckBox>
                                <dx:ASPxHyperLink ID="lnkToS" runat="server" Text="" NavigateUrl="" EncodeHtml="False"
                                    Visible="False">
                                </dx:ASPxHyperLink>
                            </div>
                        </div>
                    </div>

                    <div class="form-actions">
                        <dx:ASPxButton ID="btnRegister" runat="server" 

                            CommandName="Insert" CssClass="register-btnDis"
                            ValidationGroup="RegisterGroup" 
                            EncodeHtml="False" 
                            EnableDefaultAppearance="False"
                            Cursor="pointer"
                            EnableClientSideAPI="True"
                            Native="true" ClientInstanceName="btnRegister">
                            <Border BorderWidth="1px"></Border>
                            <ClientSideEvents Click="btnRegister_click" Init="btnRegister_Init" />
                               <DisabledStyle Cursor="default" />
                        </dx:ASPxButton>                            
                            <%--
                        <div style="width: 100px;">
                            <dx:ASPxButton ID="btnRegister" runat="server" CommandName="Insert" CssClass="register-btn"
                                ValidationGroup="RegisterGroup" 
                                EncodeHtml="False" 
                                Font-Bold="True" 
                                Height="32px"
                                Font-Size="16px" 
                                Width="100px" 
                                HorizontalAlign="Center" VerticalAlign="Middle"
                                BackColor="#74A554" 
                                EnableDefaultAppearance="False" ForeColor="White" 
                                Cursor="pointer"
                                EnableClientSideAPI="True"
                                Native="true">
                                <Border BorderWidth="1px"></Border>
                            </dx:ASPxButton>
                        </div>--%>
                    </div>
                    <script type="text/javascript">
                        $('.form-actions').hide();
                        //setTimeout(function(){
                        //    $('.form-actions').show();
                        //},700);
                    </script>
                    <div class="clear"></div>
                </asp:Panel>
                <div class="clear">
                </div>
            </div>
            <div class="rightsidediv">
                <div class="lists">
                    <h2>
                        <asp:Label ID="lblHeaderRight" runat="server" CssClass="rightColumnTitle" Text="" /></h2>
                    <asp:Label ID="lblTextRight" runat="server" CssClass="rightColumnContent" Text="" />
                </div>
            </div>
            <div class="clear">
            </div>
        </div>
    </div>
    <div class="clear">
    </div>

    <div class="uiContextualLayerPositioner _572t uiLayer">
        <div class="uiContextualLayer uiContextualLayerRight" style="right: 0px;max-width:330px">
            <div class="_5v-0 _53in">
                <div class="_5633 _5634 _53ij">What’s your name?</div>
                <i class="_53io" style="top: 0%; margin-top: 12px;"></i>
                <a class="accessible_elem layer_close_elem" href="#">Close popup and return</a>
            </div>
        </div>
    </div>
</div>
                    


        <asp:HyperLink Visible="false"  ID="lnkLogGoogle" runat="server" onclick="OpenGoogleLoginPopup()" CssClass="google-connect">Login with google</asp:HyperLink>

        </ContentTemplate>
</asp:UpdatePanel> 
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cntBodyBottom" runat="server">
    <uc3:LiveZillaPublic ID="ctlLiveZillaPublic" runat="server" />
    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function (sender, args) {
            RegisterForm_LoadAsyncValidation.start()
        });
        RegisterForm_LoadAsyncValidation.start();
    </script>
</asp:Content>

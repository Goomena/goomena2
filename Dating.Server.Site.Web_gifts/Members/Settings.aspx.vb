﻿Imports Dating.Server.Core.DLL
Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Datasets.DLL.DSMembers
Imports Dating.Server.Site.Web.TWLib
Imports System.Web.Script.Serialization

Public Class Settings
    Inherits BasePage

#Region "Props"

    Public Property FBUser As FBUserInfo
        Get
            If (Session("FBUser") IsNot Nothing) Then
                Return Session("FBUser")
            End If
            Return Nothing
        End Get
        Set(value As FBUserInfo)
            Session("FBUser") = value
        End Set
    End Property


    Public Property TWUser As TWUserInfo
        Get
            If (Session("TWUser") IsNot Nothing) Then
                Return Session("TWUser")
            End If
            Return Nothing
        End Get
        Set(value As TWUserInfo)
            Session("TWUser") = value
        End Set
    End Property



    'Dim _pageData As clsPageData
    'Protected ReadOnly Property CurrentPageData As clsPageData
    '    Get
    '        If (_pageData Is Nothing) Then _pageData = New clsPageData(Context)
    '        Return _pageData
    '    End Get
    'End Property

#End Region


    Protected Overrides Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        SetMasterPage(CurrentPageData)

        Try
            If clsCurrentContext.VerifyLogin() = True Then
                Me.MasterPageFile = "~/Members/Members2.Master"
                'If (Request("master") = "inner") Then
                '    Me.MasterPageFile = "~/Members/Members2Inner.Master"
                'Else
                'End If
            Else
                clsCurrentContext.ClearSession(Session)
                If (Not Me.IsCallback) Then
                    Response.Redirect(ResolveUrl("~/Login.aspx"), False)
                End If
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            pnlNSUserMessage.Visible = False
            pnlPSUserMessage.Visible = False
            'pnlNS_NotifyNewMembersErr.Visible = False

            If (Not Page.IsPostBack) Then

                If (Request("conn") = "fb" AndAlso Request("remove") = "1") Then
                    ' disconnect user profile from facebook
                    DataHelpers.UpdateEUS_Profiles_FacebookData(Me.MasterProfileId, Nothing, Nothing, Nothing)
                    Me.GetCurrentProfile().FacebookUserId = Nothing
                    Me.GetCurrentProfile().FacebookName = Nothing
                    Me.GetCurrentProfile().FacebookUserName = Nothing
                End If

                LoadLAG()
                LoadViews()
                LoadFacebookStatus()
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try
    End Sub

    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender

        Try
            Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

        Try
            If (mvSettings.GetActiveView().Equals(vwAccountSettings)) Then
                lblItemsName.Text = msg_RightColumnSettingsTitle1.Text
                msg_RightColumnSettingsTitle1.Visible = False

            ElseIf (mvSettings.GetActiveView().Equals(vwPrivacy)) Then
                lblItemsName.Text = msg_RightColumnSettingsTitle2.Text
                msg_RightColumnSettingsTitle2.Visible = False

            ElseIf (mvSettings.GetActiveView().Equals(vwNotifications)) Then
                lblItemsName.Text = msg_RightColumnSettingsotificationsTitle.Text
                msg_RightColumnSettingsotificationsTitle.Visible = False

            ElseIf (mvSettings.GetActiveView().Equals(vwCredits)) Then
                lblItemsName.Text = msg_RightColumnSettingsTitle4.Text
                msg_RightColumnSettingsTitle4.Visible = False

            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub



    Private Sub LoadViews()

        Try
            ''vw=CREDITS
            'txtEmail.ValidationSettings.RegularExpression.ValidationExpression = gEmailAddressValidationRegex
            If (clsConfigValues.Get__validate_email_address_normal()) Then
                txtEmail.ValidationSettings.RegularExpression.ValidationExpression = AppUtils.gEmailAddressValidationRegex
            Else
                txtEmail.ValidationSettings.RegularExpression.ValidationExpression = AppUtils.gEmailAddressValidationRegex_Simple
            End If

            'Dim sqlProc As String = "EXEC [CustomerAvailableCredits] @CustomerId = " & MasterProfileId
            'Dim dt As DataTable = DataHelpers.GetDataTable(sqlProc)

            Dim __cac As clsCustomerAvailableCredits = BasePage.GetCustomerAvailableCredits(Me.MasterProfileId)
            If (__cac.CreditsRecordsCount > 0 OrElse __cac.HasSubscription) Then
                liCredits.Visible = True

                If (Request.QueryString("vw") IsNot Nothing) Then
                    If (Request.QueryString("vw").ToUpper() = "CREDITS") Then
                        mvSettings.SetActiveView(vwCredits)
                    End If
                End If

            Else
                liCredits.Visible = False
            End If

            If (Request.QueryString("vw") IsNot Nothing AndAlso Request.QueryString("vw").ToUpper() = "ACCOUNT") Then
                mvSettings.SetActiveView(vwAccountSettings)
            End If

            If (Request.QueryString("vw") IsNot Nothing AndAlso Request.QueryString("vw").ToUpper() = "PRIVACY") Then
                mvSettings.SetActiveView(vwPrivacy)
            End If

            If (Request.QueryString("vw") IsNot Nothing AndAlso Request.QueryString("vw").ToUpper() = "NOTIFICATIONS") Then
                mvSettings.SetActiveView(vwNotifications)
            End If

            If (mvSettings.ActiveViewIndex = -1) Then
                mvSettings.ActiveViewIndex = 0
            End If

            If (Not String.IsNullOrEmpty(ShowCriteriasPopUp.ClientInstanceName)) Then
                ShowCriteriasPopUp.Windows(0).ContentUrl = "~/Members/AutoNotificationsSettings.aspx?popup=" & ShowCriteriasPopUp.ClientInstanceName
            Else
                ShowCriteriasPopUp.Windows(0).ContentUrl = "~/Members/AutoNotificationsSettings.aspx?popup=" & ShowCriteriasPopUp.ClientID
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
        LoadFacebookStatus()
    End Sub



    Protected Sub LoadLAG()
        Try

            SetControlsValue(Me, CurrentPageData)


            txtOldPassword.ValidationSettings.ErrorText = CurrentPageData.GetCustomString("txtOldPassword_Validation_ErrorText")
            txtOldPassword.ValidationSettings.RequiredField.ErrorText = CurrentPageData.GetCustomString("txtOldPassword_Required_ErrorText")

            txtPasswrd.ValidationSettings.ErrorText = CurrentPageData.GetCustomString("txtNewPassword_Validation_ErrorText_ND")
            txtPasswrd.ValidationSettings.RequiredField.ErrorText = CurrentPageData.GetCustomString("txtPasswrd_Required_ErrorText_ND")

            txtPasswrd1Conf.ValidationSettings.ErrorText = CurrentPageData.GetCustomString("txtPasswrd1Conf_Validation_ErrorText")


            txtEmail.ValidationSettings.RequiredField.ErrorText = CurrentPageData.GetCustomString("txtEmail_Required_ErrorText")
            txtEmail.ValidationSettings.RegularExpression.ErrorText = CurrentPageData.GetCustomString("txtEmail_Validation_ErrorText")

            txtEmailPassword.ValidationSettings.RequiredField.ErrorText = CurrentPageData.GetCustomString("txtEmailPassword_Required_ErrorText")

            btnChangePassword.Text = CurrentPageData.GetCustomString("btnChangePassword_Text")
            btnChangeEmail.Text = CurrentPageData.GetCustomString("btnChangeEmail_Text")
            lnkDeleteAccount.Text = CurrentPageData.GetCustomString("lnkDeleteAccount_Text")
            'lnkDeleteAccount.OnClientClick = String.Format("confirm('{0}')", CurrentPageData.GetCustomString("WarningUserDeletingAccount").Replace("'", "\'"))
            'lnkDeleteAccount.OnClientClick = "if(" & lnkDeleteAccount.OnClientClick & "){try{ShowLoading();}catch(e){};return true;}else{return false;}"


            chkPS_HideMeFromSearchResults.Text = CurrentPageData.GetCustomString(chkPS_HideMeFromSearchResults.ID)
            chkPS_HideMeFromMembersIHaveBlocked.Text = CurrentPageData.GetCustomString(chkPS_HideMeFromMembersIHaveBlocked.ID)
            chkPS_NotShowInOtherUsersFavoritedList.Text = CurrentPageData.GetCustomString(chkPS_NotShowInOtherUsersFavoritedList.ID)
            chkPS_NotShowInOtherUsersViewedList.Text = CurrentPageData.GetCustomString(chkPS_NotShowInOtherUsersViewedList.ID)
            chkPS_HideMyLastLoginDateTime.Text = CurrentPageData.GetCustomString(chkPS_HideMyLastLoginDateTime.ID)
            chkPS_DontShowOnPhotosGrid.Text = CurrentPageData.GetCustomString("chkPS_DontShowOnPhotosGrid")
            btnPrivacySettingsUpd.Text = CurrentPageData.GetCustomString(btnPrivacySettingsUpd.ID)
            chkPS_ShowMeOffline.Text = CurrentPageData.GetCustomString("chkPS_ShowMeOffline")

            'chkNS_EmailRecentMatches.Text = CurrentPageData.GetCustomString(chkNS_EmailRecentMatches.ID)
            chkNS_NotifyNewUpdatedOffers.Text = CurrentPageData.GetCustomString(chkNS_NotifyNewUpdatedOffers.ID)
            'chkNS_NotifyOnAutoReject.Text = CurrentPageData.GetCustomString(chkNS_NotifyOnAutoReject.ID)
            chkNS_NotifyOnNewMessage.Text = CurrentPageData.GetCustomString("chkNS_NotifyOnNewMessage")
            'chkNS_NotifyOnFavorite.Text = CurrentPageData.GetCustomString(chkNS_NotifyOnFavorite.ID)
            'chkNS_NotifyOnViewed.Text = CurrentPageData.GetCustomString(chkNS_NotifyOnViewed.ID)
            chkNS_NotifyNewLikes.Text = CurrentPageData.GetCustomString("chkNS_NotifyNewLikes")
            chkNS_NotifyNewMembers.Text = CurrentPageData.GetCustomString("chkNS_NotifyNewMembers")
            chkNS_MyProfileViewed.Text = CurrentPageData.GetCustomString("chkNS_MyProfileViewed")
            chkNS_ProfilesINotVisited.Text = CurrentPageData.GetCustomString("chkNS_ProfilesINotVisited")

            lnkShowCriterias.Text = CurrentPageData.GetCustomString("lnkShowCriterias")
            ShowCriteriasPopUp.Windows(0).HeaderText = CurrentPageData.GetCustomString("popupShowCriteriasHeader")
            btnNotificationsUpdate.Text = CurrentPageData.GetCustomString("btnNotificationsUpdate")

            lblItemsName.Text = Me.CurrentPageData.GetCustomString("CartSettingsView")
            lnkViewDescription.Text = Me.CurrentPageData.GetCustomString("CartSettingsWhatIsIt")
            lnkViewDescription.OnClientClick = OnMoreInfoClickFunc(
                lnkViewDescription.OnClientClick,
                ResolveUrl("~/Members/InfoWin.aspx?info=settings"),
                Me.CurrentPageData.GetCustomString("CartSettingsView"))
            WhatIsIt1.PopupControl.HeaderText = Me.CurrentPageData.GetCustomString("CartSettingsView")
            lnkVerificationDocs.Text = Me.CurrentPageData.GetCustomString("lnkVerificationDocs")


            chkPSSuppressWarning_WhenReadingMessageFromDifferentCountry.Text = CurrentPageData.GetCustomString("chkPSSuppressWarning_WhenReadingMessageFromDifferentCountry")
            chkPSHideMeFromUsersInDifferentCountry.Text = CurrentPageData.GetCustomString("chkPSHideMeFromUsersInDifferentCountry")
            chkPSDisableReceivingMessagesFromDifferentCountry.Text = CurrentPageData.GetCustomString("chkPSDisableReceivingMessagesFromDifferentCountry")
            chkPSDisableReceivingLikesFromDifferentCountry.Text = CurrentPageData.GetCustomString("chkPSDisableReceivingLikesFromDifferentCountry")
            chkPSDisableReceivingOffersFromDifferentCountry.Text = CurrentPageData.GetCustomString("chkPSDisableReceivingOffersFromDifferentCountry")

            msg_PSSuppressWarning_WhenReadingMessageFromDifferentCountryDateSet.Text = CurrentPageData.GetCustomString("LastDateChanged")
            msg_PSHideMeFromUsersInDifferentCountryDateSet.Text = CurrentPageData.GetCustomString("LastDateChanged")
            msg_PSDisableReceivingMessagesFromDifferentCountryDateSet.Text = CurrentPageData.GetCustomString("LastDateChanged")
            msg_PSDisableReceivingLikesFromDifferentCountryDateSet.Text = CurrentPageData.GetCustomString("LastDateChanged")
            msg_PSDisableReceivingOffersFromDifferentCountryDateSet.Text = CurrentPageData.GetCustomString("LastDateChanged")

            lnkClearHistory.Text = CurrentPageData.GetCustomString("lnkClearHistory")
            lnkShowStatistics.Text = CurrentPageData.GetCustomString("lnkShowStatistics")

            If (Me.IsFemale) Then
                liPSSuppressWarning_WhenReadingMessageFromDifferentCountry.Visible = False
            Else
                liPSSuppressWarning_WhenReadingMessageFromDifferentCountry.Visible = True
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Protected Sub btnChangePassword_Click(sender As Object, e As EventArgs) Handles btnChangePassword.Click

        Dim isValid As Boolean = True

        ' perform checks, in case user has bypass validation on client
        Try
            ChangePasswordServerValidationList.Items.Clear()

            If (txtOldPassword.Text.Trim().Length = 0) Then
                Dim txt As String = CurrentPageData.GetCustomString("txtOldPasswrd_Required_ErrorText")
                txt = StripTags(txt)

                Dim itm As ListItem = New ListItem(txt, "javascript:" & txtOldPassword.ClientInstanceName & ".SetFocus();")
                itm.Attributes.Add("class", "BulletedListItem")

                ChangePasswordServerValidationList.Items.Add(itm)
                txtOldPassword.IsValid = False
                isValid = False
            Else

                If (txtPasswrd.Text.Trim().Length = 0) Then
                    Dim txt As String = CurrentPageData.GetCustomString("txtPasswrd_Required_ErrorText_ND")
                    txt = StripTags(txt)

                    Dim itm As ListItem = New ListItem(txt, "javascript:" & txtPasswrd.ClientInstanceName & ".SetFocus();")
                    itm.Attributes.Add("class", "BulletedListItem")

                    ChangePasswordServerValidationList.Items.Add(itm)
                    txtPasswrd.IsValid = False
                    isValid = False
                Else


                    If (txtPasswrd.Text.Trim() <> txtPasswrd1Conf.Text.Trim()) Then
                        Dim txt As String = CurrentPageData.GetCustomString("txtPasswrd1Conf_Validation_ErrorText")
                        txt = StripTags(txt)

                        Dim itm As ListItem = New ListItem(txt, "javascript:" & txtPasswrd.ClientInstanceName & ".SetFocus();")
                        itm.Attributes.Add("class", "BulletedListItem")

                        ChangePasswordServerValidationList.Items.Add(itm)
                        txtPasswrd.IsValid = False
                        isValid = False
                    End If


                    Dim loginName As String = Me.SessionVariables.MemberData.LoginName

                    If txtPasswrd.Text.Trim().Equals(loginName) Then
                        Dim txt As String = CurrentPageData.GetCustomString("Error.Password.DifferentThanUsername")
                        txt = StripTags(txt)
                        Dim itm As ListItem = New ListItem(txt, "javascript:" & txtPasswrd.ClientInstanceName & ".SetFocus();")
                        itm.Attributes.Add("class", "BulletedListItem")
                        ChangePasswordServerValidationList.Items.Add(itm)
                        txtPasswrd.IsValid = False
                        isValid = False
                    End If

                    'password validation check
                    Dim passMatch As Boolean = Regex.IsMatch(txtPasswrd.Text, "^(?=.*\d)(?=.*[a-zA-Z]).{3,20}$", RegexOptions.Singleline)

                    If Not passMatch Then
                        Dim txt As String = CurrentPageData.GetCustomString("txtNewPassword_Validation_ErrorText_ND")
                        txt = StripTags(txt)
                        Dim itm As ListItem = New ListItem(txt, "javascript:" & txtPasswrd.ClientInstanceName & ".SetFocus();")
                        itm.Attributes.Add("class", "BulletedListItem")
                        ChangePasswordServerValidationList.Items.Add(itm)
                        txtPasswrd.IsValid = False
                        isValid = False
                    End If

                End If

            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try




        If (Page.IsValid AndAlso isValid) Then
            Try
                ChangePasswordServerValidationList.Items.Clear()

                Dim _profileRows As New clsProfileRows(Me.MasterProfileId)
                If (_profileRows.GetMasterRow().Password = txtOldPassword.Text) Then
                    _profileRows.GetMasterRow().Password = txtPasswrd.Text
                    _profileRows.GetMirrorRow().Password = txtPasswrd.Text
                    _profileRows.Update()

                    pnlPasswordChangedSuccessfully.Visible = True
                    lblPasswordChangedSuccessfully.Text = Me.CurrentPageData.GetCustomString(lblPasswordChangedSuccessfully.ID)
                Else
                    Dim txt As String = CurrentPageData.GetCustomString("msg_ErrorPasswordChangeProvidePassword")
                    txt = StripTags(txt)

                    Dim itm As ListItem = New ListItem(txt, "javascript:" & txtOldPassword.ClientInstanceName & ".SetFocus();")
                    itm.Attributes.Add("class", "BulletedListItem")

                    ChangePasswordServerValidationList.Items.Add(itm)
                End If

            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try
        End If
    End Sub

    Protected Sub btnChangeEmail_Click(sender As Object, e As EventArgs) Handles btnChangeEmail.Click

        Dim isValid As Boolean = True

        ' perform checks, in case user has bypass validation on client
        Try
            If (txtEmail.Text.Trim().Length = 0) Then
                Dim txt As String = CurrentPageData.GetCustomString("txtEmail_Required_ErrorText")
                txt = StripTags(txt)

                Dim itm As ListItem = New ListItem(txt, "javascript:" & txtEmail.ClientInstanceName & ".SetFocus();")
                itm.Attributes.Add("class", "BulletedListItem")
                ChangeEmailServerValidationList.Items.Add(itm)
                txtEmail.IsValid = False
                isValid = False

            End If

            If (txtEmailPassword.Text.Trim().Length = 0) Then
                Dim txt As String = CurrentPageData.GetCustomString("txtEmailPassword_Required_ErrorText")
                txt = StripTags(txt)

                Dim itm As ListItem = New ListItem(txt, "javascript:" & txtEmailPassword.ClientInstanceName & ".SetFocus();")
                itm.Attributes.Add("class", "BulletedListItem")
                ChangeEmailServerValidationList.Items.Add(itm)
                txtEmailPassword.IsValid = False
                isValid = False

            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try



        ' check email availability
        If (Page.IsValid AndAlso isValid) Then
            Try

                Dim profile As EUS_Profile

                ' check email
                Using cmdb As CMSDBDataContext = Me.GetCMSDBDataContext


                    profile = cmdb.EUS_Profiles.FirstOrDefault(Function(itm As EUS_Profile) itm.eMail.ToUpper() = txtEmail.Text.ToUpper() AndAlso Not (New Integer() {Me.MirrorProfileId, Me.MasterProfileId}).Contains(itm.ProfileID))
                    If (profile IsNot Nothing) Then
                        Dim txt As String = CurrentPageData.GetCustomString("msg_ErrorEmailInUse")
                        txt = StripTags(txt)

                        Dim itm As ListItem = New ListItem(txt, "javascript:" & txtEmail.ClientInstanceName & ".SetFocus();")
                        itm.Attributes.Add("class", "BulletedListItem")
                        ChangeEmailServerValidationList.Items.Add(itm)
                        txtEmail.IsValid = False
                        isValid = False

                    End If
                End Using
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try
        End If



        If (Page.IsValid AndAlso isValid) Then
            Try

                Dim _profileRows As New clsProfileRows(Me.MasterProfileId)
                If (_profileRows.GetMasterRow().Password = txtEmailPassword.Text) Then
                    _profileRows.GetMasterRow().eMail = txtEmail.Text
                    _profileRows.GetMirrorRow().eMail = txtEmail.Text
                    _profileRows.Update()
                    txtEmail.Text = ""


                    pnlEmailChangedSuccessfully.Visible = True
                    lblEmailChangedSuccessfully.Text = Me.CurrentPageData.GetCustomString(lblEmailChangedSuccessfully.ID)
                Else
                    Dim txt As String = CurrentPageData.GetCustomString("msg_ErrorEmailChangeProvidePassword")
                    txt = StripTags(txt)
                    Dim itm As ListItem = New ListItem(txt, "javascript:" & txtEmailPassword.ClientInstanceName & ".SetFocus();")
                    itm.Attributes.Add("class", "BulletedListItem")
                    ChangeEmailServerValidationList.Items.Add(itm)

                End If

            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try
        End If

    End Sub


    Protected Sub lnkDeleteAccount_Click(sender As Object, e As EventArgs) Handles lnkDeleteAccount.Click

        Dim params As New DeleteAccountParams()
        params.IP = Session("IP")
        params.GEO_COUNTRY_CODE = Session("GEO_COUNTRY_CODE")
        params.HTTP_USER_AGENT = Request.Params("HTTP_USER_AGENT")
        params.Referrer = IIf(Session("Referrer") IsNot Nothing, Session("Referrer"), "")
        params.SupportTeamEmail = ConfigurationManager.AppSettings("gToEmail")

        Dim ActionResult1 As ActionResult = ProfileHelper.DeleteAccount(Me.MasterProfileId, Me.MirrorProfileId, params)


        '        ' Dim _profilerows As New clsProfileRows(Me.MasterProfileId)
        '        '_profilerows.GetMirrorRow().LoginName = "deleted_" & _profilerows.GetMirrorRow().LoginName
        '        '_profilerows.GetMirrorRow().eMail = "deleted_" & _profilerows.GetMirrorRow().eMail
        '        '_profilerows.GetMirrorRow().Status = ProfileStatusEnum.Deleted

        '        'If (_profilerows.MasterProfileID > 0) Then
        '        '    _profilerows.GetMasterRow().LoginName = "deleted_" & _profilerows.GetMasterRow().LoginName
        '        '    _profilerows.GetMasterRow().eMail = "deleted_" & _profilerows.GetMasterRow().eMail
        '        '    _profilerows.GetMasterRow().Status = ProfileStatusEnum.Deleted
        '        '    '_profilerows.GetMasterRow().isonline = false
        '        'End If
        '        '_profilerows.Update()

        '        DataHelpers.EUS_Profiles_MarkDeleted(Me.MasterProfileId)

        '        Try
        '            Dim body As String = <body><![CDATA[
        'User deleted profile. <br><br>
        'LoginName: <b>[LOGIN]</b>  <br>
        'Register date: <b>[REG_DATE]</b><br><br>
        ']]></body>
        '            body = body.Replace("[LOGIN]", Me.SessionVariables.MemberData.LoginName)
        '            body = body.Replace("[REG_DATE]", Me.SessionVariables.MemberData.RegisterDate.ToString("dd/MM/yyyy HH:mm"))

        '            If (Me.IsMale) Then
        '                Try

        '                    Dim body2 As String = <body><![CDATA[
        'Total money sum: [MONEY]<br>
        'Total credits sum: [CREDITS]<br><br>
        ']]></body>

        '                    Dim result As DataTable = DataHelpers.GetCustomerTotalMoneyAndCredits(Me.MasterProfileId)

        '                    body2 = body2.Replace("[MONEY]", result.Rows(0)("DebitAmountReceived"))
        '                    body2 = body2.Replace("[CREDITS]", result.Rows(0)("Credits"))

        '                    body = body & body2

        '                Catch ex As Exception
        '                    WebErrorSendEmail(ex, "")
        '                End Try

        '            End If

        '            body = body & <body><![CDATA[
        '--- <strong>System Details</strong> ------------------------------------<br>
        'IP: ###IP### GEO IP = ###GEOIP###<br>
        'Agent: ###AGENT###<br>
        'Referrer: ###REFERRER###<br>
        '<strong>Custom Referrer</strong>: ###CUSTOMREFERRER###<br>
        'CustomerID: ###CUSTOMERID###<br>
        '<strong>Keywords</strong>:###SEARCHENGINEKEYWORDS###<br>
        '<strong>Landing Page</strong>:###LANDINGPAGE###<br>
        '<br><br>
        ']]></body>.Value

        '            body = body.Replace("###IP###", Session("IP"))
        '            body = body.Replace("###GEOIP###", Session("GEO_COUNTRY_CODE"))

        '            Try
        '                body = body.Replace("###AGENT###", Request.Params("HTTP_USER_AGENT"))
        '            Catch ex As Exception
        '            End Try

        '            Try
        '                body = body.Replace("###REFERRER###", clsHTMLHelper.CreateURLLink(IIf(Session("Referrer") IsNot Nothing, Session("Referrer"), "")))
        '            Catch ex As Exception
        '                body = body.Replace("###REFERRER###", "")
        '            End Try

        '            Try
        '                body = body.Replace("###CUSTOMREFERRER###", Me.GetCurrentProfile().CustomReferrer)
        '            Catch ex As Exception
        '                body = body.Replace("###CUSTOMREFERRER###", "")
        '            End Try

        '            Try
        '                body = body.Replace("###CUSTOMERID###", Me.MasterProfileId)
        '            Catch ex As Exception
        '                body = body.Replace("###CUSTOMERID###", "")
        '            End Try

        '            Try
        '                body = body.Replace("###SEARCHENGINEKEYWORDS###", Me.GetCurrentProfile().SearchKeywords)
        '            Catch ex As Exception
        '                body = body.Replace("###SEARCHENGINEKEYWORDS###", "")
        '            End Try

        '            Try
        '                body = body.Replace("###LANDINGPAGE###", clsHTMLHelper.CreateURLLink(Me.GetCurrentProfile().LandingPage))
        '            Catch ex As Exception
        '                body = body.Replace("###LANDINGPAGE###", "")
        '            End Try



        '            '' last 3 received and last 3 sent messages
        '            ''''''''''''
        '            Try

        '                Dim result As List(Of EUS_Messages_GetLastExchangedResult) = CMSDBDataContext.EUS_Messages_GetLastExchanged(Me.MasterProfileId).ToList()


        '                'Dim ds As DataTable = clsMessagesHelper.GetMessages_LastExchanged(Me.MasterProfileId)
        '                'Dim sb As New StringBuilder()
        '                Dim htmlTable As String = <tbl><![CDATA[
        '    <div><u><b>Message [IsSentReceived] [COUNT]</b></u></div>
        '    <table>
        '        <tr>
        '            <th align="right" valign="top">From:<th>
        '            <td>[FromLoginName]<td>
        '        </tr>
        '        <tr>
        '            <th align="right" valign="top">To:<th>
        '            <td>[ToLoginName]<td>
        '        </tr>
        '        <tr>
        '            <th align="right" valign="top">Date:<th>
        '            <td>[DateTimeToCreate]<td>
        '        </tr>
        '        <tr>
        '            <th align="right" valign="top">Subject:<th>
        '            <td>[Subject]<td>
        '        </tr>
        '        <tr>
        '            <th align="right" valign="top">Body:<th>
        '            <td>[Body]<td>
        '        </tr>
        '        <tr>
        '            <th align="right" valign="top">MessageID:<th>
        '            <td>[EUS_MessageID]<td>
        '        </tr>
        '    </table>
        '    <br/>
        ']]></tbl>

        '                Dim wrapper As String = <tbl><![CDATA[
        '<table border="1">
        '    <tr><td valign="top"><div style="width:350px">[Received1]</div></td><td valign="top"><div style="width:350px">[Sent1]</div></td></tr>
        '    <tr><td valign="top"><div style="width:350px">[Received2]</div></td><td valign="top"><div style="width:350px">[Sent2]</div></td></tr>
        '    <tr><td valign="top"><div style="width:350px">[Received3]</div></td><td valign="top"><div style="width:350px">[Sent3]</div></td></tr>
        '</table>
        ']]></tbl>
        '                Dim cell As Integer = 0
        '                Dim count1 As Integer = 0
        '                Dim previousValue As String = ""
        '                For cnt = 0 To result.Count - 1
        '                    Dim row1 As String = htmlTable
        '                    Dim dr As EUS_Messages_GetLastExchangedResult = result(cnt)

        '                    row1 = row1.Replace("[EUS_MessageID]", dr.EUS_MessageID)

        '                    If (Not String.IsNullOrEmpty(dr.IsSentReceived)) Then
        '                        row1 = row1.Replace("[IsSentReceived]", dr.IsSentReceived)
        '                    Else
        '                        row1 = row1.Replace("[IsSentReceived]", "")
        '                    End If

        '                    If ((previousValue = "") OrElse (dr.IsSentReceived <> previousValue)) Then
        '                        count1 = 1
        '                        previousValue = dr.IsSentReceived
        '                    ElseIf (dr.IsSentReceived = previousValue) Then
        '                        count1 = count1 + 1
        '                    End If
        '                    row1 = row1.Replace("[COUNT]", count1)


        '                    If (Not String.IsNullOrEmpty(dr.FromLoginName)) Then
        '                        row1 = row1.Replace("[FromLoginName]", dr.FromLoginName)
        '                    Else
        '                        row1 = row1.Replace("[FromLoginName]", "")
        '                    End If

        '                    If (Not String.IsNullOrEmpty(dr.ToLoginName)) Then
        '                        row1 = row1.Replace("[ToLoginName]", dr.ToLoginName)
        '                    Else
        '                        row1 = row1.Replace("[ToLoginName]", "")
        '                    End If

        '                    If (dr.DateTimeToCreate.HasValue) Then
        '                        row1 = row1.Replace("[DateTimeToCreate]", dr.DateTimeToCreate.Value.ToString("dd/MM/yyyy HH:mm"))
        '                    Else
        '                        row1 = row1.Replace("[DateTimeToCreate]", "")
        '                    End If

        '                    If (Not String.IsNullOrEmpty(dr.Subject)) Then
        '                        row1 = row1.Replace("[Subject]", dr.Subject)
        '                    Else
        '                        row1 = row1.Replace("[Subject]", "")
        '                    End If

        '                    If (Not String.IsNullOrEmpty(dr.Body)) Then
        '                        row1 = row1.Replace("[Body]", dr.Body)
        '                    Else
        '                        row1 = row1.Replace("[Body]", "")
        '                    End If

        '                    If (Not String.IsNullOrEmpty(dr.IsSentReceived)) Then
        '                        wrapper = wrapper.Replace("[" & dr.IsSentReceived & count1 & "]", row1)
        '                    End If
        '                Next

        '                If (result.Count > 0) Then
        '                    body = body & "<br/><br/>" & wrapper
        '                End If


        '            Catch ex As Exception
        '                WebErrorSendEmail(ex, "")
        '            End Try


        '            clsMyMail.SendMail(ConfigurationManager.AppSettings("gToEmail"), "User deleted profile on Goomena.com", body, True)
        '        Catch ex As Exception
        '            WebErrorMessageBox(Me, ex, "Delete account.")
        '        End Try
        If (ActionResult1.HasErrors) Then
            WebErrorSendEmail(New System.Exception(ActionResult1.ExtraMessage), "Delete account.")
        End If

        Response.Redirect("~/Logout.aspx", False)
        HttpContext.Current.ApplicationInstance.CompleteRequest()
    End Sub

    Protected Sub btnPrivacySettingsUpd_Click(sender As Object, e As EventArgs) Handles btnPrivacySettingsUpd.Click
        'If (chkPS_ShowMeOffline.Checked) Then
        '    SessionVariables.ShowMeOffline = True
        'End If
        SaveProfile_PrivacySettings(Me.MasterProfileId)
        LoadProfile_PrivacySettings(Me.MasterProfileId)
    End Sub




    Sub SaveProfile_PrivacySettings(ByVal profileId As Integer)
        Try
            Dim _profilerows As New clsProfileRows(profileId)

            _profilerows.GetMasterRow().PrivacySettings_HideMeFromSearchResults = chkPS_HideMeFromSearchResults.Checked
            _profilerows.GetMasterRow().PrivacySettings_HideMeFromMembersIHaveBlocked = chkPS_HideMeFromMembersIHaveBlocked.Checked
            _profilerows.GetMasterRow().PrivacySettings_NotShowInOtherUsersFavoritedList = chkPS_NotShowInOtherUsersFavoritedList.Checked
            _profilerows.GetMasterRow().PrivacySettings_NotShowInOtherUsersViewedList = chkPS_NotShowInOtherUsersViewedList.Checked
            _profilerows.GetMasterRow().PrivacySettings_HideMyLastLoginDateTime = chkPS_HideMyLastLoginDateTime.Checked
            _profilerows.GetMasterRow().PrivacySettings_DontShowOnPhotosGrid = chkPS_DontShowOnPhotosGrid.Checked

            _profilerows.GetMasterRow().LastUpdateProfileDateTime = DateTime.UtcNow
            _profilerows.GetMasterRow().LastUpdateProfileIP = Request.Params("REMOTE_ADDR")
            _profilerows.GetMasterRow().LastUpdateProfileGEOInfo = Session("GEO_COUNTRY_CODE")


            _profilerows.GetMirrorRow().PrivacySettings_HideMeFromSearchResults = chkPS_HideMeFromSearchResults.Checked
            _profilerows.GetMirrorRow().PrivacySettings_HideMeFromMembersIHaveBlocked = chkPS_HideMeFromMembersIHaveBlocked.Checked
            _profilerows.GetMirrorRow().PrivacySettings_NotShowInOtherUsersFavoritedList = chkPS_NotShowInOtherUsersFavoritedList.Checked
            _profilerows.GetMirrorRow().PrivacySettings_NotShowInOtherUsersViewedList = chkPS_NotShowInOtherUsersViewedList.Checked
            _profilerows.GetMirrorRow().PrivacySettings_HideMyLastLoginDateTime = chkPS_HideMyLastLoginDateTime.Checked
            _profilerows.GetMirrorRow().PrivacySettings_DontShowOnPhotosGrid = chkPS_DontShowOnPhotosGrid.Checked

            _profilerows.GetMirrorRow().LastUpdateProfileDateTime = DateTime.UtcNow
            _profilerows.GetMirrorRow().LastUpdateProfileIP = Request.Params("REMOTE_ADDR")
            _profilerows.GetMirrorRow().LastUpdateProfileGEOInfo = Session("GEO_COUNTRY_CODE")


            _profilerows.Update()

            Using cmdb As CMSDBDataContext = Me.GetCMSDBDataContext


                Dim _EUS_Pps As EUS_ProfilesPrivacySetting = (From itm In cmdb.EUS_ProfilesPrivacySettings
                                                                                 Where itm.ProfileID = Me.MasterProfileId OrElse itm.MirrorProfileID = Me.MasterProfileId
                                                                                 Select itm).FirstOrDefault()


                If (_EUS_Pps Is Nothing) Then
                    '_EUS_Pps = New EUS_ProfilesPrivacySetting()
                    '_EUS_Pps.ProfileID = Me.MasterProfileId
                    '_EUS_Pps.MirrorProfileID = Me.MirrorProfileId
                    'CMSDBDataContext.EUS_ProfilesPrivacySettings.InsertOnSubmit(_EUS_Pps)
                    _EUS_Pps = clsProfilesPrivacySettings.CreateNew_EUS_ProfilesPrivacySetting(Me.MasterProfileId, Me.MirrorProfileId)
                End If

                _EUS_Pps.ProfileID = Me.MasterProfileId
                _EUS_Pps.MirrorProfileID = Me.MirrorProfileId

                If (clsNullable.NullTo(_EUS_Pps.DisableReceivingLikesFromDifferentCountry) <> chkPSDisableReceivingLikesFromDifferentCountry.Checked) Then
                    _EUS_Pps.DisableReceivingLikesFromDifferentCountryDateSet = DateTime.UtcNow
                End If
                _EUS_Pps.DisableReceivingLikesFromDifferentCountry = chkPSDisableReceivingLikesFromDifferentCountry.Checked


                If (clsNullable.NullTo(_EUS_Pps.DisableReceivingMessagesFromDifferentCountry) <> chkPSDisableReceivingMessagesFromDifferentCountry.Checked) Then
                    _EUS_Pps.DisableReceivingMessagesFromDifferentCountryDateSet = DateTime.UtcNow
                End If
                _EUS_Pps.DisableReceivingMessagesFromDifferentCountry = chkPSDisableReceivingMessagesFromDifferentCountry.Checked


                If (clsNullable.NullTo(_EUS_Pps.DisableReceivingOffersFromDifferentCountry) <> chkPSDisableReceivingOffersFromDifferentCountry.Checked) Then
                    _EUS_Pps.DisableReceivingOffersFromDifferentCountryDateSet = DateTime.UtcNow
                End If
                _EUS_Pps.DisableReceivingOffersFromDifferentCountry = chkPSDisableReceivingOffersFromDifferentCountry.Checked


                If (clsNullable.NullTo(_EUS_Pps.HideMeFromUsersInDifferentCountry) <> chkPSHideMeFromUsersInDifferentCountry.Checked) Then
                    _EUS_Pps.HideMeFromUsersInDifferentCountryDateSet = DateTime.UtcNow
                End If
                _EUS_Pps.HideMeFromUsersInDifferentCountry = chkPSHideMeFromUsersInDifferentCountry.Checked


                If (clsNullable.NullTo(_EUS_Pps.SupressWarning_WhenReadingMessageFromDifferentCountry) <> chkPSSuppressWarning_WhenReadingMessageFromDifferentCountry.Checked) Then
                    _EUS_Pps.SupressWarning_WhenReadingMessageFromDifferentCountryDateSet = DateTime.UtcNow
                End If
                _EUS_Pps.SupressWarning_WhenReadingMessageFromDifferentCountry = chkPSSuppressWarning_WhenReadingMessageFromDifferentCountry.Checked

                If (clsNullable.NullTo(_EUS_Pps.PrivacySettings_ShowMeOffline) <> chkPS_ShowMeOffline.Checked) Then
                    _EUS_Pps.PrivacySettings_ShowMeOfflineDateSet = DateTime.UtcNow
                End If
                _EUS_Pps.PrivacySettings_ShowMeOffline = chkPS_ShowMeOffline.Checked

                cmdb.SubmitChanges()
            End Using

            '    lblNSUserMessage.Text = "Privacy settings succesfully saved."
            'Catch ex As Exception
            '    WebErrorMessageBox(Me, ex, "")
            '    lblNSUserMessage.Text = "Error occured while saving privacy settings."
            'End Try

            lblPSUserMessage.Text = Me.CurrentPageData.GetCustomString("UserMessagePrivacySettingsUpdated")
            pnlPSUserMessage.Visible = True
            pnlPSUserMessage.CssClass = "alert alert-success blue"
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
            lblPSUserMessage.Text = Me.CurrentPageData.GetCustomString("ErrorPrivacySettingsUpdateFailed")
            pnlPSUserMessage.Visible = True
            pnlPSUserMessage.CssClass = "alert alert-danger"
        End Try
    End Sub


    Sub LoadProfile_PrivacySettings(ByVal profileId As Integer)
        Try

            'If (SessionVariables.ShowMeOffline) Then
            '    chkPS_ShowMeOffline.Checked = True
            'End If


            Dim _profilerows As New clsProfileRows(profileId)
            Dim mirrorRow As EUS_ProfilesRow = _profilerows.GetMirrorRow()

            If (Not mirrorRow.IsPrivacySettings_HideMeFromSearchResultsNull()) Then chkPS_HideMeFromSearchResults.Checked = mirrorRow.PrivacySettings_HideMeFromSearchResults
            If (Not mirrorRow.IsPrivacySettings_HideMeFromMembersIHaveBlockedNull()) Then chkPS_HideMeFromMembersIHaveBlocked.Checked = mirrorRow.PrivacySettings_HideMeFromMembersIHaveBlocked
            If (Not mirrorRow.IsPrivacySettings_NotShowInOtherUsersFavoritedListNull()) Then chkPS_NotShowInOtherUsersFavoritedList.Checked = mirrorRow.PrivacySettings_NotShowInOtherUsersFavoritedList
            If (Not mirrorRow.IsPrivacySettings_NotShowInOtherUsersViewedListNull()) Then chkPS_NotShowInOtherUsersViewedList.Checked = mirrorRow.PrivacySettings_NotShowInOtherUsersViewedList
            If (Not mirrorRow.IsPrivacySettings_HideMyLastLoginDateTimeNull()) Then chkPS_HideMyLastLoginDateTime.Checked = mirrorRow.PrivacySettings_HideMyLastLoginDateTime
            If (Not mirrorRow.IsPrivacySettings_DontShowOnPhotosGridNull()) Then chkPS_DontShowOnPhotosGrid.Checked = mirrorRow.PrivacySettings_DontShowOnPhotosGrid
            Dim _EUS_pps As EUS_ProfilesPrivacySetting = Nothing
            Using cmdb As CMSDBDataContext = Me.GetCMSDBDataContext
                _EUS_pps = (From itm In cmdb.EUS_ProfilesPrivacySettings
                             Where itm.ProfileID = Me.MasterProfileId OrElse itm.MirrorProfileID = Me.MasterProfileId
                                                               Select itm).FirstOrDefault()
            End Using
          


            If (_EUS_pps IsNot Nothing) Then
                chkPSDisableReceivingLikesFromDifferentCountry.Checked = clsNullable.NullTo(_EUS_pps.DisableReceivingLikesFromDifferentCountry)
                chkPSDisableReceivingMessagesFromDifferentCountry.Checked = clsNullable.NullTo(_EUS_pps.DisableReceivingMessagesFromDifferentCountry)
                chkPSDisableReceivingOffersFromDifferentCountry.Checked = clsNullable.NullTo(_EUS_pps.DisableReceivingOffersFromDifferentCountry)
                chkPSHideMeFromUsersInDifferentCountry.Checked = clsNullable.NullTo(_EUS_pps.HideMeFromUsersInDifferentCountry)
                chkPSSuppressWarning_WhenReadingMessageFromDifferentCountry.Checked = clsNullable.NullTo(_EUS_pps.SupressWarning_WhenReadingMessageFromDifferentCountry)
                chkPS_ShowMeOffline.Checked = clsNullable.NullTo(_EUS_pps.PrivacySettings_ShowMeOffline)

                If (_EUS_pps.SupressWarning_WhenReadingMessageFromDifferentCountryDateSet.HasValue) Then
                    msg_PSSuppressWarning_WhenReadingMessageFromDifferentCountryDateSet.Visible = True
                    msg_PSSuppressWarning_WhenReadingMessageFromDifferentCountryDateSet.Text =
                        msg_PSSuppressWarning_WhenReadingMessageFromDifferentCountryDateSet.Text.Replace("[DATE_CHANGED]", _EUS_pps.SupressWarning_WhenReadingMessageFromDifferentCountryDateSet.Value.ToString("dd/MM/yyyy HH:mm"))
                Else
                    msg_PSSuppressWarning_WhenReadingMessageFromDifferentCountryDateSet.Visible = False
                End If

                If (_EUS_pps.HideMeFromUsersInDifferentCountryDateSet.HasValue) Then
                    msg_PSHideMeFromUsersInDifferentCountryDateSet.Visible = True
                    msg_PSHideMeFromUsersInDifferentCountryDateSet.Text =
                        msg_PSHideMeFromUsersInDifferentCountryDateSet.Text.Replace("[DATE_CHANGED]", _EUS_pps.HideMeFromUsersInDifferentCountryDateSet.Value.ToString("dd/MM/yyyy HH:mm"))
                Else
                    msg_PSHideMeFromUsersInDifferentCountryDateSet.Visible = False
                End If

                If (_EUS_pps.DisableReceivingMessagesFromDifferentCountryDateSet.HasValue) Then
                    msg_PSDisableReceivingMessagesFromDifferentCountryDateSet.Visible = True
                    msg_PSDisableReceivingMessagesFromDifferentCountryDateSet.Text =
                        msg_PSDisableReceivingMessagesFromDifferentCountryDateSet.Text.Replace("[DATE_CHANGED]", _EUS_pps.DisableReceivingMessagesFromDifferentCountryDateSet.Value.ToString("dd/MM/yyyy HH:mm"))
                Else
                    msg_PSDisableReceivingMessagesFromDifferentCountryDateSet.Visible = False
                End If

                If (_EUS_pps.DisableReceivingLikesFromDifferentCountryDateSet.HasValue) Then
                    msg_PSDisableReceivingLikesFromDifferentCountryDateSet.Visible = True
                    msg_PSDisableReceivingLikesFromDifferentCountryDateSet.Text =
                        msg_PSDisableReceivingLikesFromDifferentCountryDateSet.Text.Replace("[DATE_CHANGED]", _EUS_pps.DisableReceivingLikesFromDifferentCountryDateSet.Value.ToString("dd/MM/yyyy HH:mm"))
                Else
                    msg_PSDisableReceivingLikesFromDifferentCountryDateSet.Visible = False
                End If

                If (_EUS_pps.DisableReceivingOffersFromDifferentCountryDateSet.HasValue) Then
                    msg_PSDisableReceivingOffersFromDifferentCountryDateSet.Visible = True
                    msg_PSDisableReceivingOffersFromDifferentCountryDateSet.Text =
                        msg_PSDisableReceivingOffersFromDifferentCountryDateSet.Text.Replace("[DATE_CHANGED]", _EUS_pps.DisableReceivingOffersFromDifferentCountryDateSet.Value.ToString("dd/MM/yyyy HH:mm"))
                Else
                    msg_PSDisableReceivingOffersFromDifferentCountryDateSet.Visible = False
                End If
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub



    Protected Sub btnNotificationsUpdate_Click(sender As Object, e As EventArgs) Handles btnNotificationsUpdate.Click
        SaveProfile_NotificationSettings(Me.MasterProfileId)
        LoadProfile_NotificationSettings(Me.MasterProfileId)

    End Sub


    Function ValidateInput_EUS_AutoNotificationSettings(ByVal profileId As Integer)
        Dim success As Boolean

        Try
            Using cmdb As CMSDBDataContext = Me.GetCMSDBDataContext


                Dim settings As EUS_AutoNotificationSetting = (From itm In cmdb.EUS_AutoNotificationSettings
                                                  Where itm.CustomerID = Me.MasterProfileId
                                                  Select itm).FirstOrDefault()
                If (settings Is Nothing) Then
                    success = False
                Else
                    success = True
                End If
            End Using
        Catch ex As Exception

        End Try


        Return success
    End Function


    Sub SaveProfile_NotificationSettings(ByVal profileId As Integer)


        Try
            Dim messageSet As Boolean = False
            Dim _profilerows As New clsProfileRows(profileId)

            _profilerows.GetMasterRow().NotificationsSettings_WhenLikeReceived = chkNS_NotifyNewLikes.Checked
            _profilerows.GetMasterRow().NotificationsSettings_WhenNewMessageReceived = chkNS_NotifyOnNewMessage.Checked
            _profilerows.GetMasterRow().NotificationsSettings_WhenNewOfferReceived = chkNS_NotifyNewUpdatedOffers.Checked

            _profilerows.GetMasterRow().LastUpdateProfileDateTime = DateTime.UtcNow
            _profilerows.GetMasterRow().LastUpdateProfileIP = Request.Params("REMOTE_ADDR")
            _profilerows.GetMasterRow().LastUpdateProfileGEOInfo = Session("GEO_COUNTRY_CODE")


            _profilerows.GetMirrorRow().NotificationsSettings_WhenLikeReceived = chkNS_NotifyNewLikes.Checked
            _profilerows.GetMirrorRow().NotificationsSettings_WhenNewMessageReceived = chkNS_NotifyOnNewMessage.Checked
            _profilerows.GetMirrorRow().NotificationsSettings_WhenNewOfferReceived = chkNS_NotifyNewUpdatedOffers.Checked

            _profilerows.GetMirrorRow().LastUpdateProfileDateTime = DateTime.UtcNow
            _profilerows.GetMirrorRow().LastUpdateProfileIP = Request.Params("REMOTE_ADDR")
            _profilerows.GetMirrorRow().LastUpdateProfileGEOInfo = Session("GEO_COUNTRY_CODE")

            If (chkNS_NotifyNewMembers.Checked) Then
                If (Not ValidateInput_EUS_AutoNotificationSettings(Me.MasterProfileId)) Then
                    'lblNS_NotifyNewMembersErr.Text = Me.CurrentPageData.GetCustomString("ErrorNotificationSettingsNotDefined")
                    'pnlNS_NotifyNewMembersErr.Visible = True
                    'pnlNS_NotifyNewMembersErr.CssClass = "alert"
                    chkNS_NotifyNewMembers.Checked = False

                    lblNSUserMessage.Text = Me.CurrentPageData.GetCustomString("ErrorNotificationSettingsNotDefined")
                    pnlNSUserMessage.Visible = True
                    pnlNSUserMessage.CssClass = "alert"

                    messageSet = True
                End If
            End If

            _profilerows.GetMasterRow().NotificationsSettings_WhenNewMembersNearMe = chkNS_NotifyNewMembers.Checked
            _profilerows.GetMirrorRow().NotificationsSettings_WhenNewMembersNearMe = chkNS_NotifyNewMembers.Checked


            _profilerows.Update()

            Using cmdb As CMSDBDataContext = Me.GetCMSDBDataContext


                Dim _EUS_Pps As EUS_ProfilesPrivacySetting = (From itm In cmdb.EUS_ProfilesPrivacySettings
                                                            Where itm.ProfileID = Me.MasterProfileId OrElse itm.MirrorProfileID = Me.MasterProfileId
                                                            Select itm).FirstOrDefault()


                If (_EUS_Pps Is Nothing) Then
                    _EUS_Pps = New EUS_ProfilesPrivacySetting()
                    _EUS_Pps.ProfileID = Me.MasterProfileId
                    _EUS_Pps.MirrorProfileID = Me.MirrorProfileId
                    _EUS_Pps.HideMeFromUsersInDifferentCountry = False
                    _EUS_Pps.PrivacySettings_ShowMeOffline = False
                    cmdb.EUS_ProfilesPrivacySettings.InsertOnSubmit(_EUS_Pps)
                Else
                    _EUS_Pps.ProfileID = Me.MasterProfileId
                    _EUS_Pps.MirrorProfileID = Me.MirrorProfileId
                End If

                _EUS_Pps.NotificationsSettings_WhenMyProfileVisited = chkNS_MyProfileViewed.Checked
                _EUS_Pps.NotificationsSettings_SendMeAboutUnvisitedProfiles = chkNS_ProfilesINotVisited.Checked
                cmdb.SubmitChanges()
            End Using


            If (Not messageSet) Then
                lblNSUserMessage.Text = Me.CurrentPageData.GetCustomString("UserMessageNotificationSettingsUpdated")
                pnlNSUserMessage.Visible = True
                pnlNSUserMessage.CssClass = "alert alert-success blue"
            End If


        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
            lblNSUserMessage.Text = Me.CurrentPageData.GetCustomString("ErrorNotificationSettingsUpdateFailed")
            pnlNSUserMessage.Visible = True
            pnlNSUserMessage.CssClass = "alert alert-danger"
        End Try
    End Sub


    Sub LoadProfile_NotificationSettings(ByVal profileId As Integer)
        Try

            Dim _profilerows As New clsProfileRows(profileId)
            Dim mirrorRow As EUS_ProfilesRow = _profilerows.GetMirrorRow()

            If (Not mirrorRow.IsNotificationsSettings_WhenLikeReceivedNull()) Then chkNS_NotifyNewLikes.Checked = mirrorRow.NotificationsSettings_WhenLikeReceived
            If (Not mirrorRow.IsNotificationsSettings_WhenNewMembersNearMeNull()) Then
                chkNS_NotifyNewMembers.Checked = mirrorRow.NotificationsSettings_WhenNewMembersNearMe
            End If
            If (Not mirrorRow.IsNotificationsSettings_WhenNewMessageReceivedNull()) Then chkNS_NotifyOnNewMessage.Checked = mirrorRow.NotificationsSettings_WhenNewMessageReceived
            If (Not mirrorRow.IsNotificationsSettings_WhenNewOfferReceivedNull()) Then chkNS_NotifyNewUpdatedOffers.Checked = mirrorRow.NotificationsSettings_WhenNewOfferReceived
            Dim settings As EUS_AutoNotificationSetting = Nothing
            Using cmdb As CMSDBDataContext = Me.GetCMSDBDataContext
                settings = (From itm In cmdb.EUS_AutoNotificationSettings
                              Where itm.CustomerID = Me.MasterProfileId
                                       Select itm).FirstOrDefault()
            End Using
     

            ' when setting checkbox is checked show criterias 
            If (settings Is Nothing) Then
                chkNS_NotifyNewMembers.ClientSideEvents.CheckedChanged = <js><![CDATA[
function(s, e) {
    if(s.GetChecked()==true){
        SetPCVisible(true, '#SHOWCRITERIASPOPUP#', #INDEX#)

		//window['#SHOWCRITERIASPOPUP#'].ShowWindow(0);
		//window['#SHOWCRITERIASPOPUP#'].Show();
    }
}
]]></js>
                'function(s, e) {
                '	if(s.GetChecked()==true){
                '		//window['#SHOWCRITERIASPOPUP#'].ShowWindow(0);
                '		window['#SHOWCRITERIASPOPUP#'].Show();
                '	}winAutoNotificationsSettings
                '}
                chkNS_NotifyNewMembers.ClientSideEvents.CheckedChanged = chkNS_NotifyNewMembers.ClientSideEvents.CheckedChanged.Replace("#SHOWCRITERIASPOPUP#", ShowCriteriasPopUp.ClientID)
                chkNS_NotifyNewMembers.ClientSideEvents.CheckedChanged = chkNS_NotifyNewMembers.ClientSideEvents.CheckedChanged.Replace("#INDEX#", 0)
            End If

            lnkShowMembersThatFillsCriteria.Visible = False
            If (chkNS_NotifyNewMembers.Checked) Then

                If (settings IsNot Nothing) Then
                    Dim textPattern As String = <html><![CDATA[
#TEXT# <span id="notificationsCountWrapper" class="jewelCount"><span class="countValue">#COUNT#</span></span>
]]></html>.Value
                    lnkShowMembersThatFillsCriteria.Text = CurrentPageData.GetCustomString("lnkShowMembersThatFillsCriteria")
                    Dim countRows As Integer = GetMembersThatShouldBeNotifiedForNewMember_Count()
                    If (countRows > 0) Then
                        lnkShowMembersThatFillsCriteria.Text = textPattern.Replace("#TEXT#", lnkShowMembersThatFillsCriteria.Text).Replace("#COUNT#", countRows)
                        lnkShowMembersThatFillsCriteria.NavigateUrl = "~/Members/Search.aspx?result=autonotif"
                        lnkShowMembersThatFillsCriteria.Visible = True
                    End If
                End If
            End If
            Dim _EUS_pps As EUS_ProfilesPrivacySetting = Nothing
            Using cmdb As CMSDBDataContext = Me.GetCMSDBDataContext
                _EUS_pps = (From itm In cmdb.EUS_ProfilesPrivacySettings
                                                               Where itm.ProfileID = Me.MasterProfileId OrElse itm.MirrorProfileID = Me.MasterProfileId
                                                               Select itm).FirstOrDefault()
            End Using

          

            If (_EUS_pps IsNot Nothing) Then
                chkNS_MyProfileViewed.Checked = clsNullable.NullTo(_EUS_pps.NotificationsSettings_WhenMyProfileVisited)
                chkNS_ProfilesINotVisited.Checked = clsNullable.NullTo(_EUS_pps.NotificationsSettings_SendMeAboutUnvisitedProfiles)
            Else
                ' default values
                chkNS_MyProfileViewed.Checked = True
                chkNS_ProfilesINotVisited.Checked = True
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Private Function GetMembersThatShouldBeNotifiedForNewMember_Count() As Integer
        Dim countRows As Integer
        Dim ans As EUS_AutoNotificationSetting = Nothing
        Using cmdb As CMSDBDataContext = Me.GetCMSDBDataContext
            ans = (From itm In cmdb.EUS_AutoNotificationSettings
                    Where itm.CustomerID = Me.MasterProfileId
                                        Select itm).FirstOrDefault()
        End Using


        If (ans IsNot Nothing) Then
            Dim sbSql As New StringBuilder

            sbSql.AppendLine(" and  EUS_Profiles.GenderId=" & ans.crGenderId & " ")
            If (Not String.IsNullOrEmpty(ans.crCountry)) Then
                sbSql.AppendLine(" and  EUS_Profiles.Country='" & ans.crCountry & "' ")
            End If
            If (Not String.IsNullOrEmpty(ans.crRegion)) Then
                sbSql.AppendLine(" and  EUS_Profiles.Region='" & ans.crRegion & "' ")
            End If
            If (Not String.IsNullOrEmpty(ans.crCity)) Then
                sbSql.AppendLine(" and  EUS_Profiles.City='" & ans.crCity & "' ")
            End If
            If (Not String.IsNullOrEmpty(ans.crBodyTypeId)) Then
                sbSql.AppendLine(" and  EUS_Profiles.PersonalInfo_BodyTypeID in (" & ans.crBodyTypeId & ") ")
            End If
            If (Not String.IsNullOrEmpty(ans.crEyeColorId)) Then
                sbSql.AppendLine(" and  EUS_Profiles.PersonalInfo_EyeColorID in (" & ans.crEyeColorId & ") ")
            End If
            If (Not String.IsNullOrEmpty(ans.crHairColorId)) Then
                sbSql.AppendLine(" and  EUS_Profiles.PersonalInfo_HairColorID in (" & ans.crHairColorId & ") ")
            End If
            If (Not String.IsNullOrEmpty(ans.crChildrenId)) Then
                sbSql.AppendLine(" and  EUS_Profiles.PersonalInfo_ChildrenID in (" & ans.crChildrenId & ") ")
            End If
            If (Not String.IsNullOrEmpty(ans.crEthnicityId)) Then
                sbSql.AppendLine(" and  EUS_Profiles.PersonalInfo_EthnicityID in (" & ans.crEthnicityId & ") ")
            End If
            If (Not String.IsNullOrEmpty(ans.crReligionId)) Then
                sbSql.AppendLine(" and  EUS_Profiles.PersonalInfo_ReligionID in (" & ans.crReligionId & ") ")
            End If
            If (Not String.IsNullOrEmpty(ans.crSmokingId)) Then
                sbSql.AppendLine(" and  EUS_Profiles.PersonalInfo_SmokingHabitID in (" & ans.crSmokingId & ") ")
            End If
            If (Not String.IsNullOrEmpty(ans.crDrinkingId)) Then
                sbSql.AppendLine(" and  EUS_Profiles.PersonalInfo_DrinkingHabitID in (" & ans.crDrinkingId & ") ")
            End If
            If (Not String.IsNullOrEmpty(ans.crRelationshipStatusId)) Then
                sbSql.AppendLine(" and  EUS_Profiles.LookingFor_RelationshipStatusID in (" & ans.crRelationshipStatusId & ") ")
            End If
            If (Not String.IsNullOrEmpty(ans.crHeightId)) Then
                sbSql.AppendLine(" and  EUS_Profiles.PersonalInfo_HeightID in (" & ans.crHeightId & ") ")
            End If


            If (ans.crAgeMin > 0 AndAlso ans.crAgeMax > 0) Then

                Dim fromDate = DateAdd(DateInterval.Year, ans.crAgeMax.Value * -1, Date.Now).Date
                fromDate = DateAdd(DateInterval.Year, -1, fromDate).Date
                Dim toDate = DateAdd(DateInterval.Year, ans.crAgeMin.Value * -1, Date.Now).Date

                sbSql.AppendLine(" and  EUS_Profiles.Birthday between '" & fromDate.ToString("yyyy-MM-dd hh:mm:ss.mmm") & "' and '" & toDate.ToString("yyyy-MM-dd hh:mm:ss.mmm") & "'  ")

            ElseIf (ans.crAgeMin > 0) Then

                Dim toDate = DateAdd(DateInterval.Year, ans.crAgeMin.Value * -1, Date.Now).Date
                sbSql.AppendLine(" and EUS_Profiles.Birthday < '" & toDate.ToString("yyyy-MM-dd hh:mm:ss.mmm") & "' ")

            ElseIf (ans.crAgeMax > 0) Then

                Dim fromDate = DateAdd(DateInterval.Year, ans.crAgeMax.Value * -1, Date.Now).Date
                fromDate = DateAdd(DateInterval.Year, -1, fromDate).Date
                sbSql.AppendLine(" and EUS_Profiles.Birthday between '" & fromDate.ToString("yyyy-MM-dd hh:mm:ss.mmm") & "' and getdate() ")

            End If


            If (sbSql.Length > 1) Then

                Dim prms As New clsSearchHelperParameters()
                prms.CurrentProfileId = Me.MasterProfileId
                prms.ReturnRecordsWithStatus = ProfileStatusEnum.Approved
                prms.SearchSort = SearchSortEnum.None
                prms.zipstr = Me.SessionVariables.MemberData.Zip
                prms.latitudeIn = Me.SessionVariables.MemberData.latitude
                prms.longitudeIn = Me.SessionVariables.MemberData.longitude
                prms.LookingFor_ToMeetMaleID = Me.SessionVariables.MemberData.LookingFor_ToMeetMaleID
                prms.LookingFor_ToMeetFemaleID = Me.SessionVariables.MemberData.LookingFor_ToMeetFemaleID
                prms.NumberOfRecordsToReturn = 0
                prms.AdditionalWhereClause = sbSql.ToString()
                prms.rowNumberMin = 0
                prms.rowNumberMax = 1000
                prms.performCount = True
                prms.returnCountOnly = True

                Dim ds As DataSet = clsSearchHelper.GetMembersToSearchDataTable(prms)
                If (ds.Tables.Count > 0 AndAlso
                    ds.Tables(0).Rows.Count > 0) Then

                    countRows = ds.Tables(0).Rows(0)(0)

                End If
            End If
        End If
        Return countRows
    End Function

    Protected Sub mvSettings_ActiveViewChanged(sender As Object, e As EventArgs) Handles mvSettings.ActiveViewChanged


        If (mvSettings.GetActiveView().Equals(vwAccountSettings)) Then

            liAccSett.Attributes.Add("class", "down")

            liPrivacy.Attributes.Remove("class")
            liNotifications.Attributes.Remove("class")
            liCredits.Attributes.Remove("class")


        ElseIf (mvSettings.GetActiveView().Equals(vwPrivacy)) Then

            liPrivacy.Attributes.Add("class", "down")

            liAccSett.Attributes.Remove("class")
            liNotifications.Attributes.Remove("class")
            liCredits.Attributes.Remove("class")

            LoadProfile_PrivacySettings(Me.MasterProfileId)

        ElseIf (mvSettings.GetActiveView().Equals(vwNotifications)) Then

            liNotifications.Attributes.Add("class", "down")

            liAccSett.Attributes.Remove("class")
            liPrivacy.Attributes.Remove("class")
            liCredits.Attributes.Remove("class")

            LoadProfile_NotificationSettings(Me.MasterProfileId)

        ElseIf (mvSettings.GetActiveView().Equals(vwCredits)) Then

            liCredits.Attributes.Add("class", "down")

            liAccSett.Attributes.Remove("class")
            liPrivacy.Attributes.Remove("class")
            liNotifications.Attributes.Remove("class")

            gvHistory.Bind()
        End If

    End Sub


    'Protected Sub lnkAccSett_Click(sender As Object, e As EventArgs) Handles lnkAccSett.Click
    '    mvSettings.SetActiveView(vwAccountSettings)
    'End Sub

    'Protected Sub lnkPrivacy_Click(sender As Object, e As EventArgs) Handles lnkPrivacy.Click
    '    mvSettings.SetActiveView(vwPrivacy)
    'End Sub

    'Protected Sub lnkNotifications_Click(sender As Object, e As EventArgs) Handles lnkNotifications.Click
    '    mvSettings.SetActiveView(vwNotifications)
    'End Sub

    'Protected Sub lnkCredits_Click(sender As Object, e As EventArgs) Handles lnkCredits.Click
    '    mvSettings.SetActiveView(vwCredits)
    'End Sub

    Sub LoadFacebookStatus()
        Try
            If (Not String.IsNullOrEmpty(Me.GetCurrentProfile().FacebookName) AndAlso
                clsNullable.NullTo(Me.GetCurrentProfile().FacebookUserId) > 0) Then

                lblFacebookConnected.Text = Me.CurrentPageData.VerifyCustomString("msg_FacebookConnected")
                divFBState.Attributes("class") = divFBState.Attributes("class").Replace("off", "")

                lbLoginFB.Visible = False
                lblSeparatorFBstatus.Visible = False

                lbLoginFBDisconn.Text = Me.CurrentPageData.VerifyCustomString("msg_FacebookDisconnect")
                lbLoginFBDisconn.Visible = True

            Else
                lblFacebookConnected.Text = Me.CurrentPageData.VerifyCustomString("msg_FacebookNotConnected")
                lblFacebookConnected.CssClass = lblFacebookConnected.CssClass.Replace("connected", "")

                lbLoginFB.Text = Me.CurrentPageData.VerifyCustomString("msg_FacebookConnectNow")
                lbLoginFB.Visible = True
                lblSeparatorFBstatus.Visible = True
                lbLoginFBDisconn.Visible = False

            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub

End Class






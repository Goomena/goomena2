﻿Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Core.DLL
Imports System.Drawing
Imports DevExpress.Web.ASPxEditors
Imports Dating.Server.Core.DLL.clsUserDoes
Imports Dating.Server.Datasets.DLL.DSMembers

Public Class MessagesList
    Inherits BasePage


#Region "Props"

    Public Property ScrollToElem As String

    Protected Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            'If (_pageData Is Nothing) Then _pageData = New clsPageData("~/Members/Messages.aspx", Context)
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData("~/Members/Messages.aspx", Context, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property


    Protected Property MessagesView As MessagesViewEnum
        Get
            Return ViewState("MessagesView")
        End Get
        Set(value As MessagesViewEnum)
            ViewState("MessagesView") = value
        End Set
    End Property


    Protected Property OfferId As Integer
        Get
            Return ViewState("OfferId")
        End Get
        Set(value As Integer)
            ViewState("OfferId") = value
        End Set
    End Property


    Protected Property UserIdInView As Integer
        Get
            Return ViewState("UserIdInView")
        End Get
        Set(value As Integer)
            ViewState("UserIdInView") = value
        End Set
    End Property


    Protected Property SubjectInView As String
        Get
            Return ViewState("SubjectInView")
        End Get
        Set(value As String)
            ViewState("SubjectInView") = value
        End Set
    End Property


    Protected Property MessageIdInView As Long
        Get
            Return ViewState("MessageIdInView")
        End Get
        Set(value As Long)
            ViewState("MessageIdInView") = value
        End Set
    End Property

    ' used with delete message action
    Protected Property MessageIdListInView As List(Of Long)
        Get
            Return ViewState("MessageIdListInView")
        End Get
        Set(value As List(Of Long))
            ViewState("MessageIdListInView") = value
        End Set
    End Property


    Protected Property BackUrl As String
        Get
            Return ViewState("BackUrl")
        End Get
        Set(value As String)
            ViewState("BackUrl") = value
        End Set
    End Property





    Private ReadOnly Property CurrentOffersControl As MessagesControlLeft
        Get
            Return msgsL
        End Get
    End Property


    Protected Property StopExecution As Boolean

#End Region


    Protected Overrides Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            If (Me.SessionVariables.MemberData.Status = ProfileStatusEnum.LIMITED) Then
                Response.Write("{""redirect"":""" & ResolveUrl("~/Members/Default.aspx") & """}")
                HttpContext.Current.ApplicationInstance.CompleteRequest()
                StopExecution = True
            End If
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (StopExecution) Then Return

        Try
            If (Not Page.IsPostBack) Then
                'itemsPerPage)
                Dim _itemsPerPage As Integer
                Dim req As String = Request.QueryString("itemsPerPage")
                If (Not String.IsNullOrEmpty(req)) Then
                    Integer.TryParse(req, _itemsPerPage)
                End If


            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Try


            Dim viewLoaded As Boolean = False

            Dim unlockMessage As Boolean = MyBase.UnlimitedMsgID > 0 AndAlso Not String.IsNullOrEmpty(Request.QueryString("vw"))
            Dim viewMessage As Boolean = Not MyBase.UnlimitedMsgID > 0 AndAlso Not String.IsNullOrEmpty(Request.QueryString("vw"))
            Dim sendMessageTo As Boolean = Not String.IsNullOrEmpty(Request.QueryString("p"))
            '    Dim tabChanged As Boolean = Not String.IsNullOrEmpty(Request.QueryString("t"))


            If (Not Page.IsPostBack) Then
                Me.BackUrl = Request.Params("HTTP_REFERER")

                If (Not String.IsNullOrEmpty(Request.Params("offerid"))) Then Integer.TryParse(Request.Params("offerid"), Me.OfferId)

                If (viewMessage) Then
                    '' load conversation for requested and current profiles
                    LoadLAG()
                    'LoadViews()

                    Dim otherLoginName = Request.QueryString("vw")
                    Using cmdb As CMSDBDataContext = Me.GetCMSDBDataContext


                        Dim memberReceivingMessage As EUS_Profile = DataHelpers.GetEUS_ProfileMasterByLoginName(cmdb, otherLoginName, ProfileStatusEnum.Approved, ProfileStatusEnum.LIMITED)
                        If (memberReceivingMessage IsNot Nothing) Then


                            If (clsUserDoes.HasCommunication(memberReceivingMessage.ProfileID, Me.MasterProfileId)) Then
                                Dim message As EUS_Message = clsUserDoes.GetLastMessageForProfiles(Me.MasterProfileId, memberReceivingMessage.ProfileID)
                                Me.UserIdInView = memberReceivingMessage.ProfileID
                                Me.MessageIdInView = message.EUS_MessageID

                                viewLoaded = True
                            End If
                        End If
                    End Using
                End If



                If (Not viewLoaded AndAlso sendMessageTo) Then

                    ' '' send message to profile
                    'LoadLAG()
                    ''LoadViews()
                    'LoadNewMessageView()
                    Response.Redirect(ResolveUrl("~/Members/Conversation.aspx?p=" & HttpUtility.UrlEncode(Server.UrlDecode(Request.QueryString("p")))), False)
                    viewLoaded = True

                End If

                If (Not viewLoaded AndAlso unlockMessage) Then
                    'PerformMessageUnlock()
                    Response.Redirect(ResolveUrl("~/Members/Conversation.aspx?unmsg=" & HttpUtility.UrlEncode(Request.QueryString("unmsg"))), False)
                    viewLoaded = True
                End If

                If (Not viewLoaded) Then
                    LoadLAG()
                    BindMessagesList()
                End If

            End If

            'If (Not viewLoaded AndAlso unlockMessage) Then
            '    PerformMessageUnlock()
            'End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub






    Private Sub BindMessagesList()
        Try
            msgsL.ShowEmptyListText = True




            If (Me.GetCurrentProfile(True) IsNot Nothing) Then

                Dim prms As New clsMessagesHelperParameters()
                Try

                    Dim countRows As Integer
                    '       Dim NumberOfRecordsToReturn As Integer = 0

                    prms.CurrentProfileId = Me.MasterProfileId
                    prms.MessagesView = MessagesViewEnum.NEWMESSAGES
                    prms.MessagesSort = MessagesSortEnum.Recent
                    prms.cmdFilter = "" 'cmdFilter
                    prms.showActive = True
                    prms.performCount = True


                    Dim ds As DataSet = clsMessagesHelper.GetMessagesLeftDataTable(prms)
                    If (prms.performCount) Then
                        countRows = ds.Tables(0).Rows(0)(0)
                        If (countRows > 0) Then
                            Dim dt As DataTable = ds.Tables(1)
                            FillListControl(dt, msgsL)
                            msgsL.ShowEmptyListText = False
                        End If
                    Else
                        countRows = ds.Tables(0).Rows.Count
                        If (countRows > 0) Then
                            Dim dt As DataTable = ds.Tables(0)
                            FillListControl(dt, msgsL)
                            msgsL.ShowEmptyListText = False
                        End If
                    End If

                    msgsL.DataBind()

                Catch ex As Exception
                    Dim extraMessage As String = ""
                    Try
                        extraMessage = AppUtils.ConvertObjectToXML(prms, prms.GetType())
                        If (Not String.IsNullOrEmpty(extraMessage)) Then
                            extraMessage = extraMessage.Replace("<?xml version=""1.0"" encoding=""utf-16""?>", "")
                            extraMessage = "SERIALIAZED object follows " & vbCrLf & (New String("-"c, 30)) & extraMessage & vbCrLf & (New String("-"c, 30))
                        End If
                    Catch
                    End Try

                    WebErrorMessageBox(Me, ex, extraMessage)

                    msgsL.UsersList = Nothing
                    msgsL.DataBind()
                End Try
            End If

            'If (Me.Pager.ItemCount = 0 AndAlso Me.GetPhotosList().Count = 0) Then

            '    Try

            '        msgsL.ShowNoPhotoText = True
            '        msgsL.ShowEmptyListText = False
            '        msgsL.DataBind()

            '    Catch ex As Exception
            '        WebErrorMessageBox(Me, ex, "")
            '    Finally
            '        SetPager(0)
            '    End Try

            'End If


        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub



    Private Sub FillListControl(dt As DataTable, offersControl As Dating.Server.Site.Web.MessagesControlLeft)

        Dim drDefaultPhoto As EUS_CustomerPhotosRow = DataHelpers.GetProfilesDefaultPhoto(Me.MasterProfileId)
        Dim uliCurrentMember As New clsMessageUserListItem()
        uliCurrentMember.ProfileID = Me.MasterProfileId ' 
        uliCurrentMember.MirrorProfileID = Me.MirrorProfileId
        uliCurrentMember.LoginName = Me.GetCurrentProfile().LoginName
        uliCurrentMember.Genderid = Me.GetCurrentProfile().GenderId
        uliCurrentMember.ProfileViewUrl = ResolveUrl("~/Members/Profile.aspx?p=" & HttpUtility.UrlEncode(Me.GetCurrentProfile().LoginName)) & "&t=6"

        If (drDefaultPhoto IsNot Nothing) Then
            uliCurrentMember.ImageFileName = drDefaultPhoto.FileName
            uliCurrentMember.ImageUploadDate = drDefaultPhoto.DateTimeToUploading.ToLocalTime().ToString("d MMM, yyyy H:mm")
        End If

        uliCurrentMember.ImageUrl = ProfileHelper.GetProfileImageURL(Me.MasterProfileId, uliCurrentMember.ImageFileName, Me.SessionVariables.MemberData.GenderId, True, Me.IsHTTPS, PhotoSize.D150)
        uliCurrentMember.ImageThumbUrl = uliCurrentMember.ImageUrl


        Dim mins As Integer = clsConfigValues.Get__members_online_minutes()
        Dim LastActivityUTCDate As DateTime = Date.UtcNow.AddMinutes(-mins)
        Dim hours As Integer = clsConfigValues.Get__members_online_recently_hours()
        Dim LastActivityRecentlyUTCDate As DateTime = Date.UtcNow.AddHours(-hours)


        Dim rowsCount As Integer
        For rowsCount = 0 To dt.Rows.Count - 1

            Try
                Dim dr As DataRow = dt.Rows(rowsCount)

                Dim uli As New clsMessageUserListItem()
                uli.ProfileID = uliCurrentMember.ProfileID
                uli.MirrorProfileID = uliCurrentMember.MirrorProfileID
                uli.LoginName = uliCurrentMember.LoginName
                uli.Genderid = uliCurrentMember.Genderid
                uli.ProfileViewUrl = uliCurrentMember.ProfileViewUrl
                uli.ImageFileName = uliCurrentMember.ImageFileName
                uli.ImageUrl = uliCurrentMember.ImageUrl
                uli.ImageThumbUrl = uliCurrentMember.ImageThumbUrl
                uli.ImageUploadDate = uliCurrentMember.ImageUploadDate

                uli.OtherMemberLoginName = dr("LoginName")
                uli.OtherMemberProfileID = dr("ProfileID")
                uli.OtherMemberMirrorProfileID = dr("MirrorProfileID")
                uli.OtherMemberCity = dr("City")
                uli.OtherMemberRegion = IIf(Not dr.IsNull("Region"), dr("Region"), "")
                uli.OtherMemberCountry = IIf(Not dr.IsNull("Country"), dr("Country"), "")
                uli.OtherMemberGenderid = dr("Genderid")

                If (Not dr.IsNull("Birthday")) Then
                    uli.OtherMemberAge = ProfileHelper.GetCurrentAge(dr("Birthday"))
                Else
                    uli.OtherMemberAge = 20
                End If

                If (Not dr.IsNull("FileName")) Then uli.OtherMemberImageFileName = dr("FileName")
                If (dt.Columns.Contains("HasPhoto")) Then
                    If (Not dr.IsNull("HasPhoto") AndAlso (dr("HasPhoto").ToString() = "1" OrElse dr("HasPhoto").ToString() = "True")) Then
                        ' user has photo
                        If (uli.OtherMemberImageFileName IsNot Nothing) Then
                            'has public photos
                            uli.OtherMemberImageUrl = ProfileHelper.GetProfileImageURL(uli.OtherMemberProfileID, uli.OtherMemberImageFileName, uli.OtherMemberGenderid, True, Me.IsHTTPS, PhotoSize.D150)

                        Else
                            'has private photos
                            uli.OtherMemberImageUrl = ProfileHelper.GetPrivateImageURL(uli.OtherMemberGenderid)

                        End If
                    Else
                        ' has no photo
                        uli.OtherMemberImageUrl = ProfileHelper.GetDefaultImageURL(uli.OtherMemberGenderid)

                    End If
                    If IsHTTPS AndAlso uli.OtherMemberImageUrl.StartsWith("http:") Then uli.OtherMemberImageUrl = "https" & uli.OtherMemberImageUrl.Remove(0, "http".Length)
                Else
                    uli.OtherMemberImageUrl = ProfileHelper.GetProfileImageURL(uli.OtherMemberProfileID, uli.OtherMemberImageFileName, uli.OtherMemberGenderid, True, Me.IsHTTPS, PhotoSize.D150)
                End If

                uli.OtherMemberImageThumbUrl = uli.OtherMemberImageUrl

                'uli.OtherMemberProfileViewUrl = ResolveUrl("~/Members/Profile.aspx?p=" & HttpUtility.UrlEncode(uli.OtherMemberLoginName))
                uli.OtherMemberProfileViewUrl = ResolveUrl("~/Members/Conversation.aspx?vw=" & HttpUtility.UrlEncode(uli.OtherMemberLoginName))

                'If (dt.Columns.Contains("IsOnlineNow")) Then
                '    uli.OtherMemberIsOnline = IIf(Not dr.IsNull("IsOnlineNow"), dr("IsOnlineNow"), False)
                'Else
                '    uli.OtherMemberIsOnline = IIf(Not dr.IsNull("IsOnline"), dr("IsOnline"), False)
                '    If (uli.OtherMemberIsOnline) Then
                '        Dim __LastActivityDateTime As DateTime? = IIf(Not dr.IsNull("LastActivityDateTime"), dr("LastActivityDateTime"), Nothing)
                '        If (__LastActivityDateTime.HasValue) Then
                '            uli.OtherMemberIsOnline = __LastActivityDateTime >= LastActivityUTCDate
                '        Else
                '            uli.OtherMemberIsOnline = False
                '        End If
                '    End If
                'End If
                uli.OtherMemberIsOnline = IIf(Not dr.IsNull("IsOnline"), dr("IsOnline"), False)
                Dim __LastActivityDateTime As DateTime? = IIf(Not dr.IsNull("LastActivityDateTime"), dr("LastActivityDateTime"), Nothing)

                If (dt.Columns.Contains("IsOnlineNow")) Then
                    uli.OtherMemberIsOnline = IIf(Not dr.IsNull("IsOnlineNow"), dr("IsOnlineNow"), False)
                Else
                    If (uli.OtherMemberIsOnline) Then
                        If (__LastActivityDateTime.HasValue) Then
                            uli.OtherMemberIsOnline = __LastActivityDateTime >= LastActivityUTCDate
                        Else
                            uli.OtherMemberIsOnline = False
                        End If
                    End If
                End If


                If (dt.Columns.Contains("IsOnlineRecently")) Then
                    uli.OtherMemberIsOnlineRecently = IIf(Not dr.IsNull("IsOnlineRecently"), dr("IsOnlineRecently"), False)
                Else
                    If (uli.OtherMemberIsOnline) Then
                        If (__LastActivityDateTime.HasValue) Then
                            uli.OtherMemberIsOnlineRecently = __LastActivityDateTime >= LastActivityRecentlyUTCDate
                        Else
                            uli.OtherMemberIsOnlineRecently = False
                        End If
                    End If
                End If

                uli.EUS_MessageID = dr("EUS_MessageID")


                Dim sentText As String = Me.CurrentPageData.GetCustomString("SentDateText")
                uli.SentDate = sentText.Replace("###DATE###", AppUtils.GetDateTimeString(dr("MAXDateTimeToCreate"), "", "dd/MM/yyyy H:mm"))
                uli.MAXDateTimeToCreateUtc = dr("MAXDateTimeToCreate")

                If (uli.OtherMemberProfileID = Me.MasterProfileId) Then
                    uli.Read = "read"
                    uli.ReadText = Me.CurrentPageData.GetCustomString("ReadText")
                    uli.ReadButtonClass = "read button"
                Else
                    If (dr("messageData_StatusID").ToString() = "1") Then
                        uli.Read = "read"
                        uli.ReadText = Me.CurrentPageData.GetCustomString("ReadText")
                        uli.ReadButtonClass = "read button"
                    Else
                        uli.Read = "unread"
                        uli.ReadText = Me.CurrentPageData.GetCustomString("UnreadText")
                        uli.ReadButtonClass = "unread button"
                    End If
                End If

                If (Not dr.IsNull("OfferAmount") AndAlso dr("OfferAmount") > 0) Then
                    uli.OfferAmount = dr("OfferAmount")
                    uli.OfferAmountText = " &euro;" & dr("OfferAmount") & " " & Me.CurrentPageData.GetCustomString("OfferAmountOfferWord")
                End If

                uli.btnUnlockVisible = False
                uli.btnOpenVisible = True

                uli.UnlockMessageUrlOnce = ResolveUrl("~/Members/Conversation.aspx?read=once&vw=" & HttpUtility.UrlEncode(dr("LoginName")) & "&unmsg=" & dr("eus_messageid"))
                uli.UnlockMessageOnceText = Me.CurrentPageData.GetCustomString("UnlockMessageOnceText")
                uli.UnlockMessageManyText = Me.CurrentPageData.GetCustomString("UnlockMessageManyText")

                If (Not dr.IsNull("ItemsCountRead") AndAlso Not dr.IsNull("ItemsCount")) Then
                    uli.ItemsCountRead = dr("ItemsCountRead")
                    uli.ItemsCount = dr("ItemsCount")
                    If (uli.ItemsCountRead < uli.ItemsCount) Then
                        Dim unreadCount As Integer = uli.ItemsCount - uli.ItemsCountRead
                        Dim credits As Integer = unreadCount * ProfileHelper.Config_UNLOCK_MESSAGE_READ_CREDITS

                        If (uli.UnlockMessageOnceText.IndexOf("###UNLOCK_MESSAGE_READ_CRD###") > -1) Then
                            uli.UnlockMessageOnceText = uli.UnlockMessageOnceText.Replace("###UNLOCK_MESSAGE_READ_CRD###", credits)
                        End If

                        If (uli.UnlockMessageManyText.IndexOf("###UNLOCK_CONVERSATIONCRD###") > -1) Then
                            uli.UnlockMessageManyText = uli.UnlockMessageManyText.Replace("###UNLOCK_CONVERSATIONCRD###", credits)
                        End If
                    End If

                End If

                uli.UnlockMessageOnceText = ReplaceCommonTookens(uli.UnlockMessageOnceText, uli.OtherMemberLoginName)
                uli.UnlockMessageManyText = ReplaceCommonTookens(uli.UnlockMessageManyText, uli.OtherMemberLoginName)

                uli.OnMoreInfoClickFunc = OnMoreInfoClickFunc(
                    "OnMoreInfoClick('[contentUrl]',this,'[headerText]');return false;",
                    ResolveUrl("~/Members/InfoWin.aspx?info=messagescommands&ln=" & uli.OtherMemberLoginName),
                    CurrentPageData.GetCustomString("MessagesCommands_HeaderText"))

                uli.AllowTooltipPopupMessages = False
                uli.ShowWarningProfileDeleted = (dr("Status") <> 4)
                uli.WarningProfileDeletedText = CurrentPageData.GetCustomString("WarningProfileDeletedText")
                If (dr("Status") = ProfileStatusEnum.LIMITED) Then
                    uli.WarningProfileDeletedText = CurrentPageData.GetCustomString("WarningProfileLimitedText")
                End If

                uli.MessagesCountText = CurrentPageData.GetCustomString("MessagesCountText")

                If (MessagesViewEnum.NEWMESSAGES = MessagesView) Then
                    BindMessagesList_NEW(dr, uli)

                ElseIf (MessagesViewEnum.INBOX = MessagesView) Then
                    BindMessagesList_INBOX(dr, uli)

                ElseIf (MessagesViewEnum.SENT = MessagesView) Then
                    BindMessagesList_SENT(dr, uli)

                ElseIf (MessagesViewEnum.TRASH = MessagesView) Then
                    BindMessagesList_TRASH(dr, uli)

                End If

                'sent Mar 27, 2012
                uli.MessagesView = Me.MessagesView
                msgsL.UsersList.Add(uli)

            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try

        Next

    End Sub





    Private Sub BindMessagesList_NEW(ByRef dr As DataRow, ByRef uli As clsMessageUserListItem)
        Try
            If (Me.IsMale) Then

                If (uli.OtherMemberProfileID = 1) Then

                    ' communication with administrator is open
                    uli.ViewMessageText = Me.CurrentPageData.GetCustomString("CommandViewMessageText")

                ElseIf (uli.CommunicationStatus = 0) Then
                    ' check for credits amount required to unlock
                    Dim gotoBilling = False
                    Try
                        gotoBilling = True
                        Dim HasRequiredCredits_To_UnlockConversation As Boolean = HasRequiredCredits(UnlockType.UNLOCK_CONVERSATION)
                        If (HasRequiredCredits_To_UnlockConversation) Then
                            gotoBilling = False
                        End If
                    Catch ex As Exception

                    End Try

                    'credits label text
                    Dim UnlockCreditsAmountText As String = Me.CurrentPageData.GetCustomString("UnlockCreditsAmountText")
                    If (Not String.IsNullOrEmpty(UnlockCreditsAmountText)) Then
                        uli.UnlockCreditsAmountText = UnlockCreditsAmountText.Replace("###CREDITS###", ProfileHelper.Config_UNLOCK_CONVERSATION_CREDITS.ToString())
                    End If


                    ' unblock button text
                    uli.ViewMessageText = Me.CurrentPageData.GetCustomString("CommandUnlockMessageText")


                    ' hide message's subject
                    uli.Subject = "You received a message from " & dr("LoginName")

                    ' unblock button navigate
                    If (gotoBilling) Then
                        Dim billingurl As String = ResolveUrl("~/Members/SelectProduct.aspx?returnurl=" & HttpUtility.UrlEncode(Request.Url.ToString()))
                        uli.UnlockUrl = billingurl
                    Else 'If (AllowUnlimited) Then
                        Dim unlockurl As String = ResolveUrl("~/Members/Messages.aspx?unmsg=" & dr("EUS_MessageID"))
                        uli.UnlockUrl = unlockurl
                    End If

                    uli.btnUnlockVisible = True
                    uli.AllowTooltipPopupMessages = True

                ElseIf (uli.CommunicationStatus > 0) Then
                    uli.ViewMessageText = Me.CurrentPageData.GetCustomString("CommandViewMessageText")
                End If

            ElseIf (Me.IsFemale) Then

                uli.btnUnlockVisible = False
                uli.ViewMessageText = Me.CurrentPageData.GetCustomString("CommandViewMessageText")

            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "LoadGrid")
        End Try
    End Sub


    Private Sub BindMessagesList_INBOX(ByRef dr As DataRow, ByRef uli As clsMessageUserListItem)
        Try

            If (Me.IsMale) Then

                uli.btnUnlockVisible = False
                uli.ViewMessageText = Me.CurrentPageData.GetCustomString("CommandViewMessageText")

            ElseIf (Me.IsFemale) Then

                uli.btnUnlockVisible = False
                uli.ViewMessageText = Me.CurrentPageData.GetCustomString("CommandViewMessageText")

            End If


        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "LoadGrid")
        End Try
    End Sub


    Private Sub BindMessagesList_SENT(ByRef dr As DataRow, ByRef uli As clsMessageUserListItem)
        Try

            If (Me.IsMale) Then
                uli.btnUnlockVisible = False
                uli.ViewMessageText = Me.CurrentPageData.GetCustomString("CommandViewMessageText")
            Else
                uli.btnUnlockVisible = False
                uli.ViewMessageText = Me.CurrentPageData.GetCustomString("CommandViewMessageText")
            End If


        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "LoadGrid")
        End Try
    End Sub


    Private Sub BindMessagesList_TRASH(ByRef dr As DataRow, ByRef uli As clsMessageUserListItem)
        Try

            If (Me.IsMale) Then
                uli.btnUnlockVisible = False
                uli.ViewMessageText = Me.CurrentPageData.GetCustomString("CommandViewMessageText")

                Dim _messagesView As MessagesViewEnum = clsMessagesHelper.GetMessagesViewEnum(dr("IsReceived"), dr("statusID"))
                If (_messagesView = MessagesViewEnum.NEWMESSAGES) Then
                    uli.AllowTooltipPopupMessages = False
                End If
            Else
                uli.btnUnlockVisible = False
                uli.ViewMessageText = Me.CurrentPageData.GetCustomString("CommandViewMessageText")
            End If


        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "BindMessagesList_TRASH")
        End Try
    End Sub


    Private Shared Function SetLocationText(dr As DataRow) As String
        Dim _result = ""
        Dim _locationStrCity = dr("City").ToString()
        Dim _locationStrRegion = dr("Region").ToString()
        Dim _locationStrCountry = dr("Country").ToString()

        If (_locationStrCity <> "") Then
            _result = _locationStrCity
        End If
        If (_locationStrRegion <> "" AndAlso _locationStrCity <> "") Then
            _result = _result & ", "
        End If
        If (_locationStrRegion <> "") Then
            _result = _result & _locationStrRegion
        End If
        If (_locationStrRegion <> "" AndAlso _locationStrCountry <> "") Then
            _result = _result & ", "
        End If
        If (_locationStrCountry <> "") Then
            _locationStrCountry = ProfileHelper.GetCountryName(_locationStrCountry)
            _result = _result & _locationStrCountry
        End If

        Return _result
    End Function






    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
        BindMessagesList()
    End Sub


    Protected Sub LoadLAG()
        Try
            'If (String.IsNullOrEmpty(CurrentPageData.GetCustomString("lnkNew"))) Then
            '    clsPageData.ClearCache()
            'End If


        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub



End Class





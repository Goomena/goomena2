﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Members/Members2Inner.Master" CodeBehind="PopupNewTerms.aspx.vb" Inherits="Dating.Server.Site.Web.PopupNewTerms" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<style type="text/css">
       
</style>
<link href="//cdn.goomena.com/v1/Scripts/scrollbars/jquery.mCustomScrollbar.css" rel="stylesheet" />
<link href="/v1/CSS/new-terms.css?v2" rel="stylesheet" />
<script type="text/javascript" src="//cdn.goomena.com/v1/Scripts/scrollbars/jquery.mCustomScrollbar.concat.min.js"></script>
    <script type="text/javascript">

        //$(document).ready(function () {
        //    $('.NewTermsBottom').enscroll({
        //        showOnHover: false,
        //        verticalTrackClass: 'track3',
        //        verticalHandleClass: 'handle3'
        //    });
        //});



     <%--   $(document).ready(function () {
            $("#<%=lblNewTermsBottom.ClientID %>").mCustomScrollbar({ theme: "dark" });
                   });--%>

        function ShowNewtrms() {
        
            $('#NewTermsBottom').show();
            $('.NewTermsPopupConathiner').css('width','727px');
            window.parent.ShowTerms();

          <%--  if ('<%=Request("scrollTo")%>' == 'reward') {
                setTimeout(function () {
                    $("#<%=lblNewTermsBottom.ClientID %>").mCustomScrollbar("scrollTo", ".reward_program");
                }, 500);
            }--%>

        }



        function closePopup() {
            acceptedTerms = true;
                window.parent.HidePopupAndShowInfo();
        
        }
        function GetCheckBoxValueVal(s, e) {
            var value = s.GetChecked();
            if (value == true) {
                acceptedTerms = true;
                $('#<%= btnNewTermsAccept.ClientID%>').attr('href','javascript:closePopup();');
               $('#<%= btnNewTermsAccept.ClientID%>').removeClass("btnNewTermsAcceptDis");
               $('#<%= btnNewTermsAccept.ClientID%>').addClass("btnNewTermsAccept");//register-btnDis
            } else {
                acceptedTerms = false;
                $('#<%= btnNewTermsAccept.ClientID%>').removeAttr('href');
               $('#<%= btnNewTermsAccept.ClientID%>').removeClass("btnNewTermsAccept");
               $('#<%= btnNewTermsAccept.ClientID%>').addClass("btnNewTermsAcceptDis");//register-btnDis
           }
        }


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
   
    <div class="NewTermsPopupConathiner" >
        <div class="MainContent">
            <div class="lfloat">
                <div class="newTermsMessage" style="WIDTH: 450PX;">
                     <asp:Label ID="lblnewTermsMessageHeader"  CssClass="lblnewTermsMessageHeader" runat="server" ></asp:Label>
                  <asp:Label ID="lblnewTermsMessage"  CssClass="lblnewTermsMessage" runat="server" ></asp:Label>
                     </div>
                <div class="newTermsReadMore">
                  <%-- <asp:HyperLink ID="lnkReadMore" runat="server" NavigateUrl="javascript:ShowNewtrms();">--%>

                        <div class="NewTermsCheckAccept">
                            <div class="lfloat">
                                  <dx:ASPxCheckBox ID="cbAgreements"  runat="server" CssClass="cbAgreements" ClientInstanceName="RegisterAgreementsCheckBox"
                                                EncodeHtml="false"  CheckState="Unchecked">
                                                   <ClientSideEvents Init="function(s, e) {GetCheckBoxValueVal(s, e);}" ValueChanged="function(s, e) {GetCheckBoxValueVal(s, e);}" />
                                   
                                     </dx:ASPxCheckBox>

                            </div>
                          <div class="lfloat" style="margin-top:2px;">  
                              <asp:Label ID="lblcbAgreements"  cssclass="lblcbAgreements" runat="server" Text="diabasa">

                              </asp:Label>
                           </div>
                            <div class="clear">

                            </div>
                         </div>
                       
                 <%--   </asp:HyperLink>--%>
                </div>
             </div>
            <div class="lfloat">
                <div class="NewTermsAccept">
                    <asp:HyperLink ID="btnNewTermsAccept" CssClass="btnNewTermsAcceptDis"  NavigateUrl="javascript:closePopup();" runat="server">HyperLink</asp:HyperLink>
                   
               </div>
                
             </div>
            <div class="clear">
</div>
           <div class="NewTermsBotomImage"></div>
        </div>

        <div id="NewTermsBottom">
                <div class="NewTermsBottomLine"></div>
                <asp:Label ID="lblNewTermsBottom"   CssClass ="lblNewTermsBottom" runat="server"  Text=""></asp:Label>

            
              
        </div>
    </div>

    <script type="text/javascript">

        //$(document).ready(function () {
        //    $('.NewTermsBottom').enscroll({
        //        showOnHover: false,
        //        verticalTrackClass: 'track3',
        //        verticalHandleClass: 'handle3'
        //    });
        //});


      
        $(document).ready(function () {
            $("#<%=lblNewTermsBottom.ClientID %>").mCustomScrollbar({ theme: "dark" });
            $('#<%= btnNewTermsAccept.ClientID%>').attr('disable', 'disable');
        });
         </script>

</asp:Content>



﻿Public Class pwSetPayment
    Inherits BasePage

    Dim isSubscriptionUser As Boolean

    'Dim _pageData As clsPageData
    'Protected ReadOnly Property CurrentPageData As clsPageData
    '    Get
    '        If (_pageData Is Nothing) Then _pageData = New clsPageData(Context)
    '        Return _pageData
    '    End Get
    'End Property


    Protected Overrides Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        SetMasterPage(CurrentPageData)

        Try
            If clsCurrentContext.VerifyLogin() = True Then
                isSubscriptionUser = (Me.MasterProfileId.ToString() = ConfigurationManager.AppSettings("TEST_PROFILE_ID"))
                Me.MasterPageFile = "~/Members/Members2.Master"

                Try
                    If (ConfigurationManager.AppSettings("AllowSubscription") = "1" OrElse isSubscriptionUser) Then
                        If (Not ProfileCountry.MonthlySubscriptionEnabled AndAlso Not isSubscriptionUser) Then
                            Dim product = Request("product")

                            If (Not String.IsNullOrEmpty(product) AndAlso
                                product.EndsWith("days", StringComparison.OrdinalIgnoreCase)) Then

                                If (Not isSubscriptionUser) Then
                                    Dim url As String = ResolveUrl("~/Members/SelectProduct.aspx")
                                    Response.Redirect(url, True)
                                End If

                            End If
                        End If
                    End If
                Catch ex As Exception
                    WebErrorSendEmail(ex, "")
                End Try
            Else
                clsCurrentContext.ClearSession(Session)
                If (Not Me.IsCallback) Then
                    Response.Redirect(ResolveUrl("~/Login.aspx"), False)
                End If
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


    Private Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Try
            DirectCast(Me.Master, Members2).HideBottom_MemberLeftPanel = True

            Dim amount = Request("amount")
            Dim product = Request("product")
            Dim type As String = Request.QueryString("type")

            If (Not String.IsNullOrEmpty(product) AndAlso
                Not product.StartsWith("dd", StringComparison.OrdinalIgnoreCase) AndAlso
                product.EndsWith("days", StringComparison.OrdinalIgnoreCase)) Then
                product = "dd" & product
            End If

            ''Dim url = "payment.aspx?method=paymentWall&price=" & amount & "&product=" & product
            'Dim url = "payment.aspx?method=epoch&price=" & amount & "&product=" & product
            Dim url As String = "https://www.goomena.com/Members/payment.aspx?method=epoch&price=" & amount & "&product=" & product
            If (Not String.IsNullOrEmpty(ConfigurationManager.AppSettings("PaymentSendPage"))) Then
                url = ConfigurationManager.AppSettings("PaymentSendPage") & "?method=epoch&price=" & amount & "&product=" & product
            End If


            If (Not String.IsNullOrEmpty(type)) Then
                If (type.ToLower() = "pp") Then
                    ' epoch with paypal - europe

                    'url = "payment.aspx?method=epoch&type=" & type & "&price=" & amount & "&product=" & product
                    url = "https://www.goomena.com/Members/payment.aspx?method=epoch&type=" & type & "&price=" & amount & "&product=" & product
                    If (Not String.IsNullOrEmpty(ConfigurationManager.AppSettings("PaymentSendPage"))) Then
                        url = ConfigurationManager.AppSettings("PaymentSendPage") & "?method=epoch&type=" & type & "&price=" & amount & "&product=" & product
                    End If

                    Response.Redirect(url)

                Else
                    If (type.ToLower() = "paypal") Then
                        'url = "payment.aspx?method=paymentWall&type=paypal&price=" & amount & "&product=" & product
                        url = "https://www.goomena.com/Members/payment.aspx?method=paymentWall&type=paypal&price=" & amount & "&product=" & product
                        If (Not String.IsNullOrEmpty(ConfigurationManager.AppSettings("PaymentSendPage"))) Then
                            url = ConfigurationManager.AppSettings("PaymentSendPage") & "?method=paymentWall&type=paypal&price=" & amount & "&product=" & product
                        End If

                    ElseIf (type.ToLower() = "clickandbuy") Then
                        'url = "payment.aspx?method=paymentWall&type=clickandbuy&price=" & amount & "&product=" & product
                        url = "https://www.goomena.com/Members/payment.aspx?method=paymentWall&type=clickandbuy&price=" & amount & "&product=" & product
                        If (Not String.IsNullOrEmpty(ConfigurationManager.AppSettings("PaymentSendPage"))) Then
                            url = ConfigurationManager.AppSettings("PaymentSendPage") & "?method=paymentWall&type=clickandbuy&price=" & amount & "&product=" & product
                        End If
                    ElseIf (type.ToLower() = "pv" OrElse type.ToLower() = "cc") Then
                        ' url = "payment.aspx?method=epoch&type=" & type & "&price=" & amount & "&product=" & product
                        url = "https://www.goomena.com/Members/payment.aspx?method=epoch&type=" & type & "&price=" & amount & "&product=" & product
                        If (Not String.IsNullOrEmpty(ConfigurationManager.AppSettings("PaymentSendPage"))) Then
                            url = ConfigurationManager.AppSettings("PaymentSendPage") & "?method=epoch&type=" & type & "&price=" & amount & "&product=" & product
                        End If
                    ElseIf (type.ToLower() = "paymentwall") Then
                        ' url = "payment.aspx?method=epoch&type=" & type & "&price=" & amount & "&product=" & product
                        url = "https://www.goomena.com/Members/payment.aspx?method=paymentWall&price=" & amount & "&product=" & product
                        If (Not String.IsNullOrEmpty(ConfigurationManager.AppSettings("PaymentSendPage"))) Then
                            url = ConfigurationManager.AppSettings("PaymentSendPage") & "?method=paymentWall&price=" & amount & "&product=" & product
                        End If
                        iframePayment.Attributes("class") = "iframePayment_PW"
                        iframePayment_Wrap.Attributes("class") = "iframePayment_PW"
                    End If
                End If

            End If

            iframePayment.Attributes.Add("src", url)



            'Try
            '    Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            '    AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
            'Catch ex As Exception
            '    WebErrorMessageBox(Me, ex, "Page_Load")
            'End Try


            If (Not Me.IsPostBack) Then
                LoadLAG()
                'If (Request.UrlReferrer IsNot Nothing) Then
                '    lnkBack.NavigateUrl = Request.UrlReferrer.ToString()
                'Else
                '    lnkBack.NavigateUrl = "~/Members/"
                'End If
            End If

        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub

    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender

        Try
            Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub



    Public Overrides Sub Master_LanguageChanged()

        If (Me.Visible) Then
            Me._pageData = Nothing
            LoadLAG()
        End If

    End Sub


    Protected Sub LoadLAG()
        Try
            '   Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic


            'lblPaymentMethodTitle.Text = CurrentPageData.GetCustomString(lblPaymentMethodTitle.ID)
            'lnkBack.Text = CurrentPageData.GetCustomString(lnkBack.ID)
            lblPaymentDescription.Text = CurrentPageData.GetCustomString("lblPaymentDescription")
            lbPaymentExtraInfo.Text = CurrentPageData.GetCustomString("lbPaymentExtraInfo")

            Dim type As String = Request.QueryString("type")
            If (Not String.IsNullOrEmpty(type)) Then
                If (type.ToLower() = "paypal") Then
                    lblPaymentDescription.Text = CurrentPageData.GetCustomString("lblPaymentDescription_PayPal")
                    lbPaymentExtraInfo.Text = CurrentPageData.GetCustomString("lbPaymentExtraInfo_PayPal")

                ElseIf (type.ToLower() = "clickandbuy") Then
                    lblPaymentDescription.Text = CurrentPageData.GetCustomString("lblPaymentDescription_clickandbuy")
                    lbPaymentExtraInfo.Text = CurrentPageData.GetCustomString("lbPaymentExtraInfo_clickandbuy")

                ElseIf (type.ToLower() = "adyen") Then
                    lblPaymentDescription.Text = CurrentPageData.GetCustomString("lblPaymentDescription_adyen")
                    lbPaymentExtraInfo.Text = CurrentPageData.GetCustomString("lbPaymentExtraInfo_adyen")

                End If
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub
End Class
﻿Imports Dating.Server.Core.DLL
Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Datasets.DLL.DSMembers

Public Class PaymentCoupon
    Inherits BasePage

    'Dim _pageData As clsPageData
    'Protected ReadOnly Property CurrentPageData As clsPageData
    '    Get
    '        If (_pageData Is Nothing) Then _pageData = New clsPageData(Context)
    '        Return _pageData
    '    End Get
    'End Property


    Dim _token As String
    Private ReadOnly Property Token As String
        Get
            If (_token Is Nothing) Then
                Dim WSTokenString As String = "ASDFSD@$@#%#$FFSDFDGDFGDF12232334654FDGFG!!!"
                Dim Code As String = Library.Public.clsHash.ComputeHash(WSTokenString, "SHA512")
                _token = Code
            End If
            Return _token
        End Get
    End Property

    'Dim eus_PriceRec As EUS_Price
    ' Dim amount As String = ""
    ' Dim type As String = ""
    ' Dim quantity As String = ""
    ' Dim choise As String = ""


    Protected Overrides Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        SetMasterPage(CurrentPageData)

        Try
            If clsCurrentContext.VerifyLogin() = True Then
                Me.MasterPageFile = "~/Members/Members2.Master"
            Else
                clsCurrentContext.ClearSession(Session)
                If (Not Me.IsCallback) Then
                    Response.Redirect(ResolveUrl("~/Login.aspx"), False)
                End If
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            pnlErr.Visible = False
            If (Not Page.IsPostBack) Then
                LoadLAG()
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
    End Sub


    Protected Sub LoadLAG()
        Try

            lblPageTitle.Text = CurrentPageData.GetCustomString("lblPageTitle")
            lnkBack.Text = CurrentPageData.GetCustomString("lnkBack")
            lblTopDescription.Text = CurrentPageData.GetCustomString("lblTopDescription")
            lblInsertVoucher.Text = CurrentPageData.GetCustomString("lblInsertVoucher")
            btnProceed.Text = CurrentPageData.GetCustomString("btnProceed")


            lblItemsName.Text = Me.CurrentPageData.GetCustomString("CartVoucherView")
            lnkViewDescription.Text = Me.CurrentPageData.GetCustomString("CartVoucherWhatIsIt")
            lnkViewDescription.OnClientClick = OnMoreInfoClickFunc(
                lnkViewDescription.OnClientClick,
                ResolveUrl("~/Members/InfoWin.aspx?info=voucher"),
                Me.CurrentPageData.GetCustomString("CartVoucherView"))
            WhatIsIt1.PopupControl.HeaderText = Me.CurrentPageData.GetCustomString("CartVoucherView")

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender

        Try
            Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub

    Private Sub ShowError(msg As String)
        pnlErr.CssClass = "alert alert-danger"
        pnlErr.Visible = True
        lblErr.Text = msg
    End Sub

    Private Sub ShowSuccess(msg As String)
        'pnlActivateDone.CssClass = "alert alert-success"
        pnlActivateDone.Visible = True
        lblResult.Text = msg
    End Sub

    Protected Sub btnRedeemVoucher_Click(sender As Object, e As EventArgs) Handles btnProceed.Click

        Try
            If (String.IsNullOrEmpty(txtVoucher.Text.Trim())) Then
                ShowError(CurrentPageData.GetCustomString("Err_VoucherInputEmpty")) '"Error voucher input is empty") 
                Return
            End If

            If (Not CheckVoucherOnSite(txtVoucher.Text)) Then
                ShowError(CurrentPageData.GetCustomString("Err_VoucherActivated"))
                Return
            End If


            Dim result As net.paymix.vouchercontrol.clsVoucherControlReturn = Nothing
            result = RedeemVoucher()

            If (result.hasError) Then
                If (result.ErrorMessage = "Voucher Already Activated!") Then
                    ShowError(CurrentPageData.GetCustomString("Err_VoucherActivated")) '"Voucher Already Activated! <br> You may not activate it again."
                ElseIf (result.ErrorMessage = "Voucher Not Found!") Then
                    ShowError(CurrentPageData.GetCustomString("Err_VoucherNotFound")) '("Voucher not found.")
                End If
            ElseIf (result.Voucher IsNot Nothing AndAlso result.Voucher.Status = "Activated now") Then

                Dim eus_PriceRec As EUS_Price = clsPricing.GetEUS_PricesByAmount(result.Voucher.Amount)

                Dim Description As String = "Activated with voucher code " & result.Voucher.VoucherCode
                Dim Money As Double = result.Voucher.Amount
                Dim TransactionInfo As String = "Voucher code " & result.Voucher.VoucherCode
                Dim TransactionTypeID As String = result.Voucher.UniPayTransactionID
                Dim CreditsPurchased As Integer = eus_PriceRec.Credits
                Dim REMOTE_ADDR As String = Session("IP")
                Dim HTTP_USER_AGENT As String = Request.ServerVariables("HTTP_USER_AGENT")
                Dim HTTP_REFERER As String = Session("HTTP_REFERER")
                Dim CustomerID As Integer = Me.MasterProfileId
                Dim CustomReferrer As String = Me.SessionVariables.MemberData.CustomReferrer

                Dim freeUnlocks As Integer = 0
                If (Not String.IsNullOrEmpty(result.Voucher.unlocks)) Then
                    Integer.TryParse(result.Voucher.unlocks, freeUnlocks)
                End If


                Dim CustomerTransactionID As Integer =
                    clsCustomer.AddTransaction(Description, Money, TransactionInfo, "",
                                               CreditsPurchased, REMOTE_ADDR, HTTP_USER_AGENT, HTTP_REFERER, CustomerID,
                                               CustomReferrer, Me.GetCurrentProfile().eMail,
                                               99, "",
                                               TransactionTypeID,
                                               Nothing,
                                               freeUnlocks,
                                               New clsDataRecordIPN())

                pnlActivate.Visible = False
                ShowSuccess(CurrentPageData.GetCustomString("Success_VoucherActivated")) '("Your voucher was activated succesfully!")
                Using cmdb As CMSDBDataContext = Me.GetCMSDBDataContext
                    DataHelpers.InsertToEUS_VoucherActivated(cmdb, txtVoucher.Text, Me.MasterProfileId, CustomerTransactionID)
                End Using

            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub


    Function RedeemVoucher() As net.paymix.vouchercontrol.clsVoucherControlReturn

        Dim result As net.paymix.vouchercontrol.clsVoucherControlReturn = Nothing
        Try

            Dim voucher As New net.paymix.vouchercontrol.clsVoucher()
            voucher.VoucherCode = txtVoucher.Text

            Dim voucherData As New net.paymix.vouchercontrol.clsVoucherData()
            voucherData.SiteID = ConfigurationManager.AppSettings("siteID")
            voucherData.token = Me.Token
            voucherData.Voucher = voucher

            Dim ws As New net.paymix.vouchercontrol.VoucherControl()
            ws.Url = ConfigurationManager.AppSettings("WSRedeemVoucherURL")
            ws.UseDefaultCredentials = True
            result = ws.RedeemVoucher(voucherData)

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "RedeemVoucher")
        End Try
        Return result
    End Function


    Sub CreateVoucher()

        '   Dim result As New net.paymix.vouchercontrol.clsVoucherControlReturn()

        Dim voucher As New net.paymix.vouchercontrol.clsVoucher()
        voucher.ActivationDate = Date.UtcNow
        voucher.Amount = 15
        voucher.CreationDate = Date.UtcNow
        voucher.Product = "100Credits"
        voucher.ProductID = 39
        voucher.SiteActivatedFrom = ConfigurationManager.AppSettings("siteID")
        voucher.Status = "ok"
        voucher.UniPayTransactionID = "-1"
        voucher.VoucherCode = txtVoucher.Text

        Dim voucherData As New net.paymix.vouchercontrol.clsVoucherData()
        voucherData.SiteID = ConfigurationManager.AppSettings("siteID")
        voucherData.token = Me.Token
        voucherData.Voucher = voucher

        Dim ws As New net.paymix.vouchercontrol.VoucherControl()
        ws.Url = ConfigurationManager.AppSettings("WSRedeemVoucherURL")
        ws.UseDefaultCredentials = True
        ws.CreateVoucher(voucherData)

    End Sub


    'Sub calculateTransactionData()

    '    choise = Request.QueryString("choice").ToLower
    '    If choise.Contains("credits") Then

    '        type = "credits"
    '        quantity = choise.Replace("credits", "")

    '        'Dim quantities() As String = quantity.Split(New String() {"t"}, StringSplitOptions.RemoveEmptyEntries)
    '        'If (quantities.Length = 2) Then
    '        '    quantity = quantities(1)
    '        'Else
    '        '    quantity = "0"
    '        'End If
    '    End If

    '    eus_PriceRec = clsCustomer.GetPriceFromCredits(quantity)
    '    amount = eus_PriceRec.Amount

    'End Sub


    Protected Sub btnCreateTest_Click(sender As Object, e As EventArgs) Handles btnCreateTest.Click
        CreateVoucher()
    End Sub

    Private Function CheckVoucherOnSite(voucher As String) As Boolean
        Dim available As Boolean
        Try
            Using cmdb As CMSDBDataContext = Me.GetCMSDBDataContext
                Dim o As EUS_VoucherActivated = DataHelpers.GetEUS_VoucherActivatedByVoucher(cmdb, voucher)
                available = (o Is Nothing)
            End Using
       
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "CheckVoucherOnSite")
        End Try
        Return available
    End Function

End Class
﻿Imports System.Drawing
Imports DevExpress.Web.ASPxEditors
Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Core.DLL
Imports Dating.Server.Core.DLL.clsUserDoes


''' <summary>
''' 
''' Send messages, view conversations.
''' 
''' Performs checks 
''' when No member selected to send to
''' when male has no credits to read or send message
''' when female sends to female
''' 
''' Referal checks
''' stop message when, it's being sent to man without photo
''' when referral sending a third message to the same man, which has not replied to previous messages
''' 
''' Other functionalities
''' predefined random messages, 
''' predefined message for  private images
''' 
''' </summary>
''' <remarks></remarks>
Public Class Conversation
    Inherits MessagesBasePage

#Region "Props"

    

    Public Property ScrollToElem As String
    Public Property WarningPopup_HeaderText As String
    Public Property memberReceivingMessage As EUS_Profile
    Public Property RequestedMessage As EUS_Message

    Protected Property FocusAndWriteMessages As Boolean
    Protected Property MaySendFreeMessage As Boolean
    Protected Property IsFreeMessageSent As Boolean

    Protected Property OfferId As Integer
        Get
            Return ViewState("OfferId")
        End Get
        Set(value As Integer)
            ViewState("OfferId") = value
        End Set
    End Property


    Protected Property IsMaleSendingMessageFirstTime As Boolean
        Get
            Return ViewState("IsMaleSendingMessageFirstTime")
        End Get
        Set(value As Boolean)
            ViewState("IsMaleSendingMessageFirstTime") = value
        End Set
    End Property


    Protected Property UserIdInView As Integer
        Get
            Return ViewState("UserIdInView")
        End Get
        Set(value As Integer)
            ViewState("UserIdInView") = value
        End Set
    End Property


    Protected Property UserIdInView_IsDeleted As Boolean
        Get
            Return ViewState("UserIdInView_IsDeleted")
        End Get
        Set(value As Boolean)
            ViewState("UserIdInView_IsDeleted") = value
        End Set
    End Property


    Protected Property CurrentLoginName As String
        Get
            Return ViewState("CurrentLoginName")
        End Get
        Set(value As String)
            ViewState("CurrentLoginName") = value
            ViewState("CurrentLoginName_enc") = Nothing
        End Set
    End Property

    Protected ReadOnly Property CurrentLoginName_enc As String
        Get
            If (ViewState("CurrentLoginName_enc") Is Nothing) Then ViewState("CurrentLoginName_enc") = HttpUtility.UrlEncode(CurrentLoginName)
            Return ViewState("CurrentLoginName_enc")
        End Get
    End Property


    Protected Property SubjectInView As String
        Get
            Return ViewState("SubjectInView")
        End Get
        Set(value As String)
            ViewState("SubjectInView") = value
        End Set
    End Property


    Protected Property MessageIdInView As Long
        Get
            Return ViewState("MessageIdInView")
        End Get
        Set(value As Long)
            ViewState("MessageIdInView") = value
        End Set
    End Property

    ' used with delete message action
    Protected Property MessageIdListInView As List(Of Long)
        Get
            Return ViewState("MessageIdListInView")
        End Get
        Set(value As List(Of Long))
            ViewState("MessageIdListInView") = value
        End Set
    End Property


    Protected Property BackUrl As String
        Get
            Return ViewState("BackUrl")
        End Get
        Set(value As String)
            ViewState("BackUrl") = value
        End Set
    End Property


    Protected Property SendAnotherMessage As Boolean
        Get
            Return ViewState("SendAnotherMessage")
        End Get
        Set(value As Boolean)
            ViewState("SendAnotherMessage") = value
        End Set
    End Property


    ''' <summary>
    ''' Recipient id.
    ''' Used when current user opens it's sent message and wants to send another one.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected Property SendAnotherMessageToRecipient As Integer
        Get
            Return ViewState("SendAnotherMessageToRecipient")
        End Get
        Set(value As Integer)
            ViewState("SendAnotherMessageToRecipient") = value
        End Set
    End Property


    Protected Property SendOnce As Boolean
        Get
            Return ViewState("SendOnce")
        End Get
        Set(value As Boolean)
            ViewState("SendOnce") = value
        End Set
    End Property


    Protected Property SendUnlimited As Boolean
        Get
            Return ViewState("SendUnlimited")
        End Get
        Set(value As Boolean)
            ViewState("SendUnlimited") = value
        End Set
    End Property


    Private __RequestedMessageID As Integer?
    Protected ReadOnly Property RequestedMessageID As Integer
        Get
            If (__RequestedMessageID Is Nothing) Then
                __RequestedMessageID = 0
                Dim messageID As Integer
                If (Not String.IsNullOrEmpty(Request.QueryString("msg")) AndAlso Integer.TryParse(Request.QueryString("msg"), messageID)) Then
                    __RequestedMessageID = messageID
                End If
            End If

            Return __RequestedMessageID
        End Get
    End Property

#End Region


    Protected Overrides Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        SetMasterPage(CurrentPageData)

        Try
            'If (Me.SessionVariables.MemberData.Status = ProfileStatusEnum.LIMITED) Then
            '    Response.Redirect(ResolveUrl("~/Members/Default.aspx"))
            'End If

            MyBase.Page_PreInit()
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        ' add a handler for handling coomands of Dates control
        AddHandler Me.newDates.Repeater.ItemCommand, AddressOf Me.offersRepeater_ItemCommand

        btnSendMessagesUnl.Visible = False

        lblActionError.Text = ""
        pnlWriteMsgErr.Visible = False
        lnkSendMessageAltAction.Visible = True

        Try

            SendOnce = (Request.QueryString("send") = "once")
            SendUnlimited = (Request.QueryString("send") = "unl")



            Dim viewLoaded As Boolean = False

            Dim unlockMessage As Boolean = MyBase.UnlimitedMsgID > 0 AndAlso Not String.IsNullOrEmpty(Request.QueryString("vw"))
            Dim viewMessage As Boolean = Not MyBase.UnlimitedMsgID > 0 AndAlso (Not String.IsNullOrEmpty(Request.QueryString("vw")) OrElse Me.RequestedMessageID > 0)
            Dim sendMessageTo As Boolean = Not String.IsNullOrEmpty(Request.QueryString("p"))
            '   Dim tabChanged As Boolean = Not String.IsNullOrEmpty(Request.QueryString("t"))
            Dim sendMessageToRedirected As Boolean = Not String.IsNullOrEmpty(Request.QueryString("vw")) AndAlso (Request.QueryString("focus") = "send" OrElse Request.QueryString("open") = "send")
            Dim hasAnyMessageExchanged As Boolean?

            If (sendMessageTo) Then
                Dim url = Request.RawUrl
                If (url.ToLower().IndexOf("?p=") > 0) Then
                    url = url.ToLower().Replace("?p=", "?vw=")
                Else
                    url = url.ToLower().Replace("&p=", "&vw=")
                End If
                url = url.ToLower().
                    Replace("?1", "").
                    Replace("?focus=send", "?1").
                    Replace("&focus=send", "").
                    Replace("?open=send", "?1").
                    Replace("&open=send", "")

                If (Me.IsMale) Then
                    url = url & "&focus=send"
                Else
                    url = url & "&open=send"
                End If
                Response.Redirect(url)
            End If

            If (Not Page.IsPostBack) Then

                CurrentLoginName = Request.QueryString("p")
                If (String.IsNullOrEmpty(CurrentLoginName)) Then CurrentLoginName = Request.QueryString("vw")
                If (Not String.IsNullOrEmpty(CurrentLoginName)) Then
                    CurrentLoginName = UrlUtils.RevertValidProfileURI(CurrentLoginName)
                    Using cmdb As CMSDBDataContext = Me.GetCMSDBDataContext

                        memberReceivingMessage = DataHelpers.GetEUS_ProfileMasterByLoginName(cmdb, CurrentLoginName, ProfileStatusEnum.Approved, ProfileStatusEnum.LIMITED)

                    End Using
  End If

                If (memberReceivingMessage IsNot Nothing) Then
                    ' hasAnyMessageExchanged : set to true or false
                    hasAnyMessageExchanged = clsUserDoes.IsAnyMessageExchanged(memberReceivingMessage.ProfileID, Me.MasterProfileId)
                ElseIf (Me.RequestedMessageID > 0) Then
                    ' hasAnyMessageExchanged : set to true
                    RequestedMessage = clsUserDoes.GetMessage(Me.RequestedMessageID)
                    If (RequestedMessage IsNot Nothing) Then hasAnyMessageExchanged = True
                End If



                Me.BackUrl = Request.Params("HTTP_REFERER")
                If (Not String.IsNullOrEmpty(Request.Params("offerid"))) Then Integer.TryParse(Request.Params("offerid"), Me.OfferId)
                LoadLAG()


                ' perform deletion
                If (Request.QueryString("d") = "1") Then
                    viewMessage = False
                    If (Me.RequestedMessageID > 0) Then
                        ExecCmd("DELETEMSG", RequestedMessageID)
                    End If
                End If

                ''comment 2014-03-28
                'If (viewMessage OrElse sendMessageToRedirected) Then
                '    If (Not String.IsNullOrEmpty(CurrentLoginName)) Then
                '        memberReceivingMessage = DataHelpers.GetEUS_ProfileMasterByLoginName(Me.CMSDBDataContext, CurrentLoginName, ProfileStatusEnum.Approved, ProfileStatusEnum.LIMITED)
                '    End If
                'End If

                If (sendMessageToRedirected) Then 'AndAlso Me.IsMale
                    If (memberReceivingMessage IsNot Nothing AndAlso (hasAnyMessageExchanged.HasValue AndAlso hasAnyMessageExchanged = False)) Then
                        ' current user, has sent any message before?
                        Dim IsAnyMessage As Boolean = clsUserDoes.IsAnyMessageExchangedFromTo(memberReceivingMessage.ProfileID, Me.MasterProfileId, Me.MasterProfileId)
                        IsMaleSendingMessageFirstTime = (IsAnyMessage = False)
                    End If
                End If

                If (Len(Request.QueryString("text")) > 0) Then
                    If (Request.QueryString("text").ToLower() = "privatephoto") Then
                        txtWriteMsgText.Text = CurrentPageData.VerifyCustomString("Private.Photo.Message.Body").
                            Replace("[LEVEL]", Request.QueryString("lvl"))
                        txtWriteMsgText.Text = AppUtils.StripTags(txtWriteMsgText.Text)
                    End If
                    If (Request.QueryString("text").ToLower() = "traveltogether") Then
                        If (ProfileHelper.IsFemale(memberReceivingMessage.GenderId)) Then
                            txtWriteMsgText.Text = CurrentPageData.VerifyCustomString("Travel.Together.Message.Body.Receive.FEMALE")
                        Else
                            txtWriteMsgText.Text = CurrentPageData.VerifyCustomString("Travel.Together.Message.Body.Receive.MALE")
                        End If
                        txtWriteMsgText.Text = txtWriteMsgText.Text.Replace("[LOGIN-NAME]", CurrentLoginName).Replace("&nbsp;", " ").Replace("<br>", vbLf).Replace("<br/>", vbLf).Replace("<br />", vbLf)
                        txtWriteMsgText.Text = AppUtils.StripTags(txtWriteMsgText.Text)
                    End If
                    If (Request.QueryString("text").ToLower() = "worksuggest") Then
                        If (ProfileHelper.IsFemale(memberReceivingMessage.GenderId)) Then
                            txtWriteMsgText.Text = CurrentPageData.VerifyCustomString("Work.Suggestion.Message.Body.Receive.FEMALE")
                        Else
                            txtWriteMsgText.Text = CurrentPageData.VerifyCustomString("Work.Suggestion.Message.Body.Receive.MALE")
                        End If
                        txtWriteMsgText.Text = txtWriteMsgText.Text.Replace("[LOGIN-NAME]", CurrentLoginName).Replace("&nbsp;", " ").Replace("<br>", vbLf).Replace("<br/>", vbLf).Replace("<br />", vbLf)
                        txtWriteMsgText.Text = AppUtils.StripTags(txtWriteMsgText.Text)
                    End If
                End If


                If (viewMessage) Then
                    '' load conversation for requested and current profiles

                    If (memberReceivingMessage IsNot Nothing) Then
                        CurrentLoginName = memberReceivingMessage.LoginName

                        '    Dim HasCommunication As Boolean = GetHasCommunicationDict(memberReceivingMessage.ProfileID)
                        Dim message As EUS_Message = clsUserDoes.GetLastMessageForProfiles(Me.MasterProfileId, memberReceivingMessage.ProfileID)
                        If (message IsNot Nothing) Then
                            Me.UserIdInView = memberReceivingMessage.ProfileID
                            Me.MessageIdInView = message.EUS_MessageID
                        Else
                            ' there are no messages among users
                            Me.UserIdInView = memberReceivingMessage.ProfileID
                            Me.MessageIdInView = 0
                        End If

                        ExecCmd("VIEWMSG", Me.MessageIdInView)
                        viewLoaded = True

                    End If



                    If (Me.RequestedMessageID > 0) Then

                        If (RequestedMessage Is Nothing) Then
                            RequestedMessage = clsUserDoes.GetMessage(Me.RequestedMessageID)
                        End If

                        If (RequestedMessage Is Nothing OrElse RequestedMessage.ProfileIDOwner <> Me.MasterProfileId) Then
                            ' message not found or message is owned by another user

                            If (Not String.IsNullOrEmpty(Request.QueryString("vwh"))) Then
                                '''''''''''''''''
                                ' if parameter vwh presents and not empty, then show the conversation based on loginname
                                '''''''''''''''''
                                Dim url As String = Request.Url.ToString()
                                If (url.IndexOf("?") > -1) Then
                                    url = url.Remove(url.IndexOf("?") + 1)
                                    For Each itm As String In Request.QueryString.AllKeys
                                        If (Not String.IsNullOrEmpty(itm) AndAlso
                                            itm.ToLower() <> "msg" AndAlso
                                            Not String.IsNullOrEmpty(Request.QueryString(itm))) Then
                                            If (itm.ToLower() = "vwh") Then
                                                url = url & "vw" & "=" & HttpUtility.UrlEncode(Request.QueryString(itm)) & "&"
                                            Else
                                                url = url & itm & "=" & HttpUtility.UrlEncode(Request.QueryString(itm)) & "&"
                                            End If
                                        End If
                                    Next
                                    url = url.TrimEnd({"&"c, "?"c})
                                End If
                                Response.Redirect(url, True)
                            Else
                                divMessageNotFound.Visible = True

                                lnkBackMNF.Visible = True
                                If (Not String.IsNullOrEmpty(Me.BackUrl)) Then
                                    lnkBackMNF.Text = CurrentPageData.GetCustomString("lnkAlternativeAction_Back")
                                    lnkBackMNF.NavigateUrl = Me.BackUrl
                                Else
                                    lnkBackMNF.Text = CurrentPageData.GetCustomString("lnkAlternativeAction_Search")
                                    lnkBackMNF.NavigateUrl = "~/Members/Search.aspx"
                                End If

                                ltrMessageNotFound.Text = CurrentPageData.GetCustomString("ErrorMessageNotFound")
                                mvMessages.SetActiveView(vwEmpty)
                                viewLoaded = True
                            End If

                        ElseIf (RequestedMessage.ProfileIDOwner = Me.MasterProfileId) Then
                            If (RequestedMessage.FromProfileID = Me.MasterProfileId) Then
                                Me.UserIdInView = RequestedMessage.ToProfileID
                            Else
                                Me.UserIdInView = RequestedMessage.FromProfileID
                            End If
                            Me.MessageIdInView = RequestedMessage.EUS_MessageID
                            ExecCmd("VIEWMSG", Me.MessageIdInView)
                            viewLoaded = True

                        End If
                    End If

                End If



                If (Not viewLoaded AndAlso sendMessageTo) Then

                    '' send message to profile
                    LoadLAG()
                    LoadNewMessageView()
                    viewLoaded = True

                End If

                If (Not viewLoaded AndAlso unlockMessage) Then
                    LoadLAG()
                    PerformMessageUnlock()
                    viewLoaded = True
                End If

                If (Not viewLoaded) Then

                End If
            End If

            'If (Page.IsPostBack) Then
            If (Not viewLoaded AndAlso unlockMessage) Then
                PerformMessageUnlock()
            End If
            'End If


        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub




    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender

        Try
            Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "PreRender 0")
        End Try

        'Try
        '    If (SiteRulesTIP.Visible = True) Then
        '        Dim SiteRulesTIP_click As String = WhatIsIt.OnMoreInfoClickFunc(ResolveUrl("~/Members/InfoWin.aspx?info=Conversation_Site_Rules&popup=popupWhatIs"), "More info", 601, 350, Nothing)
        '        SiteRulesTIP.Attributes.Add("onclick", SiteRulesTIP_click)
        '    End If
        'Catch ex As Exception
        '    WebErrorMessageBox(Me, ex, "Page_Load-->SiteRulesTIP")
        'End Try

        If (mvMessages.GetActiveView().Equals(vwConversation)) Then
            Try
                If (lnkFromRight.NavigateUrl = "#") Then
                    lnkFromRight.NavigateUrl = "javascript:void(0);"

                    Dim fileName As String = ""
                    Dim dr As DSMembers.EUS_CustomerPhotosRow = DataHelpers.GetProfilesDefaultPhoto(Me.MasterProfileId)
                    If (dr IsNot Nothing) Then fileName = dr.FileName

                    imgFromRight.Src = ProfileHelper.GetProfileImageURL(Me.MasterProfileId, fileName, Me.SessionVariables.MemberData.GenderId, True, Me.IsHTTPS, PhotoSize.D150)
                End If


                If (convCtl.IsThereMoreMessages) Then
                    pnlMoreMsgs.Attributes.Add("data", "more-messages")
                End If

                If (Not {ProfileStatusEnum.Approved, ProfileStatusEnum.Updating, ProfileStatusEnum.NewProfile}.Contains(DataHelpers.GetEUS_Profiles_Status(Me.UserIdInView))) Then
                    send_buttons.Visible = False
                    write_message_wrap.Visible = False
                End If
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "PreRender 1")
            End Try

            Try
                If (MaySendFreeMessage) Then
                    Me.SetUI_SendingFreeMessage()
                ElseIf (IsFreeMessageSent) Then
                    Me.SetUI_NoFreeMessageAllowed()
                End If


                If (IsMaleSendingMessageFirstTime AndAlso Me.IsFemale) Then
                    lblMaleFirstMsgBottomPanelText2.Visible = False
                    Dim cls As String = "emtpy-div"
                    If (divMaleFirstMsgBottomPanelText2.Attributes("class") IsNot Nothing) Then
                        divMaleFirstMsgBottomPanelText2.Attributes("class") = divMaleFirstMsgBottomPanelText2.Attributes("class") & " " & cls
                    Else
                        divMaleFirstMsgBottomPanelText2.Attributes("class") = cls
                    End If
                End If
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "PreRender 2")
            End Try
        End If

    End Sub


    Private Sub Conversation_Unload(sender As Object, e As EventArgs) Handles Me.Unload
        '  If (__freeMessagesHelper IsNot Nothing) Then __freeMessagesHelper.Dispose()
    End Sub


    Private Sub BindMessagesConversation(messageId As Integer)
        Dim num As Integer = 0
        convCtl.RowNumberMin = 0
        If (Not String.IsNullOrEmpty(Request("rowTo")) AndAlso Integer.TryParse(Request("rowTo"), num)) Then
            convCtl.RowNumberMax = num
        Else
            convCtl.RowNumberMax = 5
        End If
        If (Not String.IsNullOrEmpty(Request("olderMsgIdAvailable")) AndAlso Integer.TryParse(Request("olderMsgIdAvailable"), num)) Then
            convCtl.OlderMsgIdAvailable = num
        End If

        convCtl.OfferId = Me.OfferId
        convCtl.UserIdInView = Me.UserIdInView
        convCtl.SubjectInView = Me.SubjectInView
        convCtl.MessageIdInView = Me.MessageIdInView
        convCtl.MessageIdListInView = Me.MessageIdListInView
        convCtl.SendAnotherMessageToRecipient = Me.SendAnotherMessageToRecipient
        convCtl.FromRight_NavigateUrl = lnkFromRight.NavigateUrl
        convCtl.FromRight_ImageSrc = imgFromRight.Src
        convCtl.CurrentLoginName = Me.CurrentLoginName

        convCtl.LoadUsersConversation(messageId)
        Me.CurrentLoginName = convCtl.CurrentLoginName

        lnkFromRight.NavigateUrl = convCtl.FromRight_NavigateUrl
        imgFromRight.Src = convCtl.FromRight_ImageSrc
        Me.MessageIdListInView = convCtl.MessageIdListInView

        If (Not String.IsNullOrEmpty(convCtl.SuppressWarning_Country)) Then
            lblWarningMessage.Text = Me.CurrentPageData.GetCustomString("SuppressWarning_WhenReadingMessageFromDifferentCountry.Warning.Message")
            lblWarningMessage.Text = lblWarningMessage.Text.Replace("[COUNTRY]", ProfileHelper.GetCountryName(convCtl.SuppressWarning_Country))
            chkSuppressWarning.Text = Me.CurrentPageData.GetCustomString("SuppressWarning_WhenReadingMessageFromDifferentCountry.Warning.Message.Checkbox")
            btnContinue.Text = Me.CurrentPageData.GetCustomString("SuppressWarning_WhenReadingMessageFromDifferentCountry.Warning.Message.Continue")
            btnCancel.Text = Me.CurrentPageData.GetCustomString("SuppressWarning_WhenReadingMessageFromDifferentCountry.Warning.Message.Cancel")
        End If

        Me.ScrollToElem = convCtl.ScrollToElem
    End Sub

    Private Sub LoadUsersConversation_MyProfileLimited(messageId As Integer)

        divDateAndInfo.Visible = True
        lblDateAndInfo.Text = CurrentPageData.GetCustomString("lblYourProfileIsLIMITEDInfo")

        write_message_wrap.Visible = False
        first_message_info.Visible = False
        send_buttons.Visible = False

        ' when message id specified, Me.UserIdInView set on  dvConversation DataBound
        Dim otherUser As Integer = Me.UserIdInView
        If (Me.UserIdInView > 1) OrElse (Me.SendAnotherMessageToRecipient > 0) Then
            otherUser = Me.UserIdInView
            If (otherUser = 0) Then otherUser = Me.SendAnotherMessageToRecipient
        End If

        updOffers.Visible = False
        If (Me.UserIdInView > 1) OrElse (Me.SendAnotherMessageToRecipient > 0) Then
            BindNewDates()
            If (newDates.UsersList.Count > 0) Then
                clsUserDoes.MarkDateOfferAsViewed(otherUser, Me.MasterProfileId)
            End If
            updOffers.Visible = (newDates.UsersList.Count > 0)
        End If

        divProfileDeletedInfo.Visible = False
        divSendAnotherMessageInfo.Visible = False
        divSendWithCredits.Visible = False

    End Sub

    Private Sub LoadUsersConversation(messageId As Integer)
        mvMessages.SetActiveView(vwConversation)
        Me.MessageIdInView = messageId

        If (memberReceivingMessage Is Nothing AndAlso Me.UserIdInView > 1) Then
            Using cmdb As CMSDBDataContext = Me.GetCMSDBDataContext
                memberReceivingMessage = DataHelpers.GetEUS_ProfileByProfileID(cmdb, Me.UserIdInView)
            End Using

        End If
        UserIdInView_IsDeleted = (memberReceivingMessage IsNot Nothing AndAlso memberReceivingMessage.Status = ProfileStatusEnum.Deleted)

        If (Me.SessionVariables.MemberData.Status = ProfileStatusEnum.LIMITED AndAlso Me.UserIdInView > 1) Then

            LoadUsersConversation_MyProfileLimited(messageId)
            Return


        ElseIf (messageId > 0) AndAlso UserIdInView_IsDeleted Then

            divProfileDeletedInfo.Visible = False
            divSendAnotherMessageInfo.Visible = False
            divSendWithCredits.Visible = False

        ElseIf (messageId > 0) Then
            BindMessagesConversation(messageId)
            CheckIsAllowedToSendMessage()

            divProfileDeletedInfo.Visible = False
            divSendAnotherMessageInfo.Visible = False
            divSendWithCredits.Visible = False

        Else

            If (Me.UserIdInView > 0) Then
                convCtl.Visible = False

                divSendWithCredits.Visible = False
                divProfileDeletedInfo.Visible = False

                divSendAnotherMessage.Visible = True

                If (String.IsNullOrEmpty(CurrentLoginName)) Then
                    Using cmdb As CMSDBDataContext = Me.GetCMSDBDataContext
                        CurrentLoginName = DataHelpers.GetEUS_Profile_LoginName_ByProfileID(cmdb, Me.UserIdInView)
                    End Using

                End If
                Dim url As String = ResolveUrl("~/Members/Conversation.aspx?p=" & CurrentLoginName_enc)

                btnSendAnotherMessage.NavigateUrl = url
                btnSendAnotherMessage.Text = CurrentPageData.GetCustomString("btnSendMessage")

                'Dim HasCommunication As Boolean = clsUserDoes.HasCommunication(Me.UserIdInView, Me.MasterProfileId)

                'If (HasCommunication) Then
                '    'divConversationReply.Visible = True
                '    'SetReplyInfo(otherLoginName)

                'End If


            End If

        End If


        ' when message id specified, Me.UserIdInView set on  dvConversation DataBound
        Dim otherUser As Integer = Me.UserIdInView
        If (Me.UserIdInView > 1) OrElse (Me.SendAnotherMessageToRecipient > 0) Then
            otherUser = Me.UserIdInView
            If (otherUser = 0) Then otherUser = Me.SendAnotherMessageToRecipient
        End If

        If (otherUser > 0) Then
            divProfileDeletedInfo.Visible = False

            Dim commandsSet As Boolean = False

            ' show to male two buttons with credits
            If (Me.IsMale) Then
                Dim HasCommunication As Boolean = GetHasCommunicationDict(otherUser, True)

                'If (Not clsUserDoes.HasCommunication(otherUser, Me.MasterProfileId)) Then
                If (Not HasCommunication) Then
                    divSendWithCredits.Visible = True

                    If (String.IsNullOrEmpty(CurrentLoginName)) Then
                        Using cmdb As CMSDBDataContext = Me.GetCMSDBDataContext
                            CurrentLoginName = DataHelpers.GetEUS_Profile_LoginName_ByProfileID(cmdb, otherUser)
                        End Using

                    End If

                    btnSendMessageOnce.NavigateUrl = ResolveUrl("~/Members/Conversation.aspx?send=once&p=" & CurrentLoginName_enc)
                    btnSendMessagesUnl.NavigateUrl = ResolveUrl("~/Members/Conversation.aspx?send=unl&p=" & CurrentLoginName_enc)

                    commandsSet = True
                End If
            End If

            ' if male may send unlimited messages or it's female, 
            ' then show button "Send another message"
            If (Not commandsSet) Then

                divSendAnotherMessage.Visible = True

                If (String.IsNullOrEmpty(CurrentLoginName)) Then
                    Using cmdb As CMSDBDataContext = Me.GetCMSDBDataContext
                        CurrentLoginName = DataHelpers.GetEUS_Profile_LoginName_ByProfileID(cmdb, otherUser)
                    End Using

                End If
                Dim url As String = ResolveUrl("~/Members/Conversation.aspx?p=" & CurrentLoginName_enc)
                btnSendAnotherMessage.NavigateUrl = url

            End If

        ElseIf (Me.UserIdInView = Me.MasterProfileId) Then
            'divConversationReply.Visible = False
            'divFormActions.Visible = False
            divProfileDeletedInfo.Visible = False

        ElseIf (Me.UserIdInView = 1) Then
            'divConversationReply.Visible = False
            'divFormActions.Visible = False

        ElseIf (Not DataHelpers.EUS_Profile_IsProfileActive(Me.UserIdInView)) Then
            'ElseIf (Not clsCustomer.IsProfileActive(Me.UserIdInView)) Then

            'divConversationReply.Visible = False
            'divFormActions.Visible = False
            ' todo: show message to user that profile is not active
            divProfileDeletedInfo.Visible = True

        ElseIf (Me.UserIdInView > 1) Then

            If (Me.IsMale) Then
                Dim HasCommunication As Boolean = GetHasCommunicationDict(Me.UserIdInView)
                If (Not HasCommunication) Then
                    divProfileDeletedInfo.Visible = False
                    divSendWithCredits.Visible = True

                    If (String.IsNullOrEmpty(CurrentLoginName)) Then
                        Using cmdb As CMSDBDataContext = Me.GetCMSDBDataContext
                            CurrentLoginName = DataHelpers.GetEUS_Profile_LoginName_ByProfileID(cmdb, Me.UserIdInView)
                        End Using

                    End If
                    btnSendMessageOnce.NavigateUrl = ResolveUrl("~/Members/Conversation.aspx?send=once&p=" & CurrentLoginName_enc)
                    btnSendMessagesUnl.NavigateUrl = ResolveUrl("~/Members/Conversation.aspx?send=unl&p=" & CurrentLoginName_enc)
                End If
            End If

        End If

        If (IsMaleSendingMessageFirstTime AndAlso Me.UserIdInView > 1) Then

            LoadNewMessageView2()
            CheckIsAllowedToSendMessage()

        Else
            updOffers.Visible = False
            If (Me.UserIdInView > 1) OrElse (Me.SendAnotherMessageToRecipient > 0) Then
                BindNewDates()
                If (newDates.UsersList.Count > 0) Then
                    clsUserDoes.MarkDateOfferAsViewed(otherUser, Me.MasterProfileId)
                End If
                updOffers.Visible = (newDates.UsersList.Count > 0)
            End If

            convCtl.Visible = (convCtl.Count > 0)
        End If

        If (divSendAnotherMessage.Visible AndAlso divSendWithCredits.Visible) Then
            If (Me.IsFemale) Then
                divSendWithCredits.Visible = False
            ElseIf (Me.IsMale) Then
                divSendAnotherMessage.Visible = False
            End If
        End If


        If (Me.SessionVariables.MemberData.Status = ProfileStatusEnum.LIMITED AndAlso Me.UserIdInView = 1) Then
            write_message_wrap.Visible = False
            first_message_info.Visible = False
            send_buttons.Visible = False
            pnlMoreMsgs.Visible = False
        End If

        If UserIdInView_IsDeleted Then
            divDateAndInfo.Visible = True
            lblDateAndInfo.Text = CurrentPageData.GetCustomString("lblProfileDeletedInfo")
        End If

    End Sub

    Private Sub LoadNewMessageView()

        Try
            mvMessages.SetActiveView(vwCreateMessage)

            Dim MaySendMessage_CheckSetting As Boolean = True

            CurrentLoginName = Request.QueryString("p")
            If (String.IsNullOrEmpty(CurrentLoginName)) Then CurrentLoginName = Request.QueryString("vw")

            If (String.IsNullOrEmpty(CurrentLoginName)) Then
                Return
            End If

            If (CurrentLoginName.ToUpper() = Me.GetCurrentProfile().LoginName.ToUpper()) Then
                ShowError_vwEmpty(ConversationErrorEnum.ErrorSendMessageToMySelf)
                Return
            End If

            If (memberReceivingMessage Is Nothing) Then
                CurrentLoginName = UrlUtils.RevertValidProfileURI(CurrentLoginName)
                Using cmdb As CMSDBDataContext = Me.GetCMSDBDataContext
                    memberReceivingMessage = DataHelpers.GetEUS_ProfileMasterByLoginName(cmdb, CurrentLoginName, ProfileStatusEnum.Approved, ProfileStatusEnum.LIMITED)
                End Using
            End If

            If (memberReceivingMessage Is Nothing) Then
                ShowError_vwEmpty(ConversationErrorEnum.ErrorMemberDoesNotExistCannotSendMessage)
                Return
            End If

            Me.UserIdInView = memberReceivingMessage.ProfileID
            CurrentLoginName = memberReceivingMessage.LoginName


            'Dim MaySendMessage As Boolean = False
            Dim femaleSend As Boolean = Me.IsFemale
            'Dim femaleSend As Boolean = MaySendMessage_CheckSetting
            'If (femaleSend) Then
            '    femaleSend = Me.IsFemale
            'End If
            Dim femaleReceiving As Boolean = ProfileHelper.IsFemale(memberReceivingMessage.GenderId)
            If (femaleSend AndAlso femaleReceiving) Then
                ShowError_vwEmpty(ConversationErrorEnum.FemaleMemberCannotSendToFemale)
                Return
            End If

            Dim AlreadyUnlockedUnlimited As Boolean = GetHasCommunicationDict(Me.UserIdInView)
            'Dim AlreadyUnlockedUnlimited As Boolean = ModGlobals.AllowUnlimited AndAlso GetHasCommunication(Me.UserIdInView)
            'Dim AlreadyUnlockedUnlimited As Boolean = clsUserDoes.HasCommunication(Me.MasterProfileId, Me.UserIdInView)


            'Dim maleSend As Boolean = MaySendMessage_CheckSetting
            'If (maleSend) Then
            '    maleSend = ((SendOnce OrElse SendUnlimited) AndAlso Me.IsMale) OrElse (AlreadyUnlockedUnlimited AndAlso Me.IsMale)
            'End If
            Dim maleSend As Boolean = ((SendOnce OrElse SendUnlimited) AndAlso Me.IsMale) OrElse (AlreadyUnlockedUnlimited AndAlso Me.IsMale)
            If (maleSend AndAlso Not AlreadyUnlockedUnlimited) Then

                'If (Not AlreadyUnlockedUnlimited) Then
                If (Not SendOnce AndAlso Not SendUnlimited) Then
                    'lblSendMessageErr.Text = Me.CurrentPageData.GetCustomString("ErrMayNotSendMessage")
                    'pnlSendMessageErr.Visible = True
                    maleSend = False
                End If


                If (SendOnce) Then
                    Dim HasRequiredCredits_UNLOCK_MESSAGE_SEND As Boolean = HasRequiredCredits(UnlockType.UNLOCK_MESSAGE_SEND)
                    'Dim hasCreditsToSendMessage As Boolean = clsUserDoes.HasRequiredCredits(Me.MasterProfileId, UnlockType.UNLOCK_MESSAGE_SEND)
                    If Not maleSend AndAlso HasRequiredCredits_UNLOCK_MESSAGE_SEND Then maleSend = True

                    ' user has no credits
                    If Not HasRequiredCredits_UNLOCK_MESSAGE_SEND Then
                        maleSend = False
                    End If

                    If (Not maleSend) Then
                        Dim mySubscriptionMessages As Integer = GetSubsriptionAvailableMessages(Me.MasterProfileId, Me.UserIdInView, Me.SessionVariables.MemberData.Country)
                        If (mySubscriptionMessages > 0) Then
                            maleSend = True
                        End If
                    End If

                    If (Not maleSend) Then
                        Dim myFreeMessages As Integer = Me.GetMembersFreeMessagesAvailableToSEND(Me.MasterProfileId, Me.UserIdInView, Me.SessionVariables.MemberData.Country)
                        If (myFreeMessages > 0) Then maleSend = True
                    End If


                ElseIf (SendUnlimited) Then
                    Dim hasCreditsToSendMessageUnl As Boolean = HasRequiredCredits(UnlockType.UNLOCK_CONVERSATION)
                    'Dim hasCreditsToSendMessageUnl As Boolean = clsUserDoes.HasRequiredCredits(Me.MasterProfileId, UnlockType.UNLOCK_CONVERSATION)
                    If Not maleSend AndAlso hasCreditsToSendMessageUnl Then maleSend = True

                    ' user has no credits
                    If Not hasCreditsToSendMessageUnl Then maleSend = False

                End If


                'pnlSendMessageErr.Visible = True
                'lnkSendMessageAltAction.Visible = False

                'If (SendOnce) Then
                '    lblSendMessageErr.Text = CurrentPageData.GetCustomString("WarnSendMessageOnce")
                '    SiteRulesTIP.Visible = True
                'ElseIf (SendUnlimited) Then
                '    lblSendMessageErr.Text = CurrentPageData.GetCustomString("WarnSendMessageUnlimited")
                'End If
                ''End If

            End If

            MaySendMessage_CheckSetting = Not clsProfilesPrivacySettings.GET_DisableReceivingMESSAGESFromDifferentCountryFromTo(memberReceivingMessage.ProfileID, Me.MasterProfileId)
            If (MaySendMessage_CheckSetting = False) Then
                ShowErrorPopup(ConversationErrorEnum.Error_Sending_Receiver_Disabled_Messages_From_Diff_Country)

                'divActionError.Visible = True
                'lnkAlternativeAction.Text = CurrentPageData.GetCustomString("lnkAlternativeAction_Search")
                'lnkAlternativeAction.NavigateUrl = "~/Members/Search.aspx"
                'lblActionError.Text = CurrentPageData.GetCustomString("DisableReceivingMessagesFromDifferentCountry.Text")
                'pnlSendMessageErr.Visible = True
                'lnkSendMessageAltAction.Visible = True
                'lnkSendMessageAltAction.Text = CurrentPageData.GetCustomString("DisableReceivingMessagesFromDifferentCountry.Alternative.Action")
                'lnkSendMessageAltAction.NavigateUrl = CurrentPageData.GetCustomString("DisableReceivingMessagesFromDifferentCountry.Alternative.Action.URL")
                'lblSendMessageErr.Text = CurrentPageData.GetCustomString("DisableReceivingMessagesFromDifferentCountry.Text")

                ''mvMessages.SetActiveView(vwEmpty)

                ''Return
            End If

            If (Not MaySendMessage_CheckSetting) Then
                femaleSend = False
                maleSend = False
            End If

            If (False AndAlso maleSend = False AndAlso Me.IsMale AndAlso MaySendMessage_CheckSetting) Then
                ' when man is going to send message  and settings allow to send,
                ' if he has no credits then
                ' check his country settings, if he is allowed to send any free message

                MaySendFreeMessage = clsUserDoes.MaySendFreeMessage(memberReceivingMessage.ProfileID, Me.MasterProfileId, Me.SessionVariables.MemberData.Country)
                If (MaySendFreeMessage) Then maleSend = True
            End If

            newMsgCtl.OfferId = Me.OfferId
            newMsgCtl.UserIdInView = Me.UserIdInView
            newMsgCtl.SubjectInView = Me.SubjectInView
            newMsgCtl.MessageIdInView = Me.MessageIdInView
            newMsgCtl.MessageIdListInView = Me.MessageIdListInView
            newMsgCtl.BackUrl = Me.BackUrl
            newMsgCtl.SendAnotherMessage = Me.SendAnotherMessage
            newMsgCtl.SendAnotherMessageToRecipient = Me.SendAnotherMessageToRecipient
            newMsgCtl.LoadNewMessageView(CurrentLoginName, memberReceivingMessage, SendOnce, SendUnlimited, MaySendMessage_CheckSetting, femaleSend, maleSend)

            'If (femaleSend OrElse maleSend) Then
            '    txtMessageText.Enabled = True
            '    btnSendMessage.Enabled = True
            '    txtMessageSubject.Enabled = True

            'Else
            '    txtMessageText.Enabled = False
            '    btnSendMessage.Enabled = False
            '    txtMessageSubject.Enabled = False


            '    If (MaySendMessage_CheckSetting) Then

            '        pnlSendMessageErr.Visible = True
            '        lnkSendMessageAltAction.Visible = True


            '        If (Me.IsMale) Then
            '            lnkSendMessageAltAction.Text = CurrentPageData.GetCustomString("lnkErrSendMessageCreditsNotAvailable")
            '            lblSendMessageErr.Text = CurrentPageData.GetCustomString("ErrSendMessageOnceCreditsNotAvailable")

            '            If (SendOnce) Then
            '                lnkSendMessageAltAction.Text = CurrentPageData.GetCustomString("lnkErrSendMessageCreditsNotAvailable")
            '                lblSendMessageErr.Text = CurrentPageData.GetCustomString("ErrSendMessageOnceCreditsNotAvailable")

            '            ElseIf (SendUnlimited) Then
            '                lnkSendMessageAltAction.Text = CurrentPageData.GetCustomString("lnkErrSendMessageCreditsNotAvailable")
            '                lblSendMessageErr.Text = CurrentPageData.GetCustomString("ErrSendMessageUnlimitedCreditsNotAvailable")

            '            End If
            '        End If

            '    End If 'MaySendMessage_CheckSetting

            'End If


            'lnkSendMessageAltAction.Text = ReplaceCommonTookens(lnkSendMessageAltAction.Text, memberReceivingMessage.LoginName)
            'lblSendMessageErr.Text = ReplaceCommonTookens(lblSendMessageErr.Text, memberReceivingMessage.LoginName)



            'Try

            '    If (ProfileHelper.IsFemale(memberReceivingMessage.GenderId)) Then
            '        lblSendMessageToTitle.Text = CurrentPageData.GetCustomString("lblSendMessageToTitle_SendToFEMALE")
            '        lblYourMsgTo.Text = CurrentPageData.GetCustomString("lblYourMsgTo_SendToFEMALE")
            '    Else
            '        lblSendMessageToTitle.Text = CurrentPageData.GetCustomString("lblSendMessageToTitle_SendToMALE")
            '        lblYourMsgTo.Text = CurrentPageData.GetCustomString("lblYourMsgTo_SendToMALE")
            '    End If
            '    lblSendMessageToTitle.Text = ReplaceCommonTookens(lblSendMessageToTitle.Text, memberReceivingMessage.LoginName)
            'Catch ex As Exception
            '    WebErrorMessageBox(Me, ex, "LoadNewMessageView #1")
            'End Try


            'Try
            '    Dim defaultPhoto As DSMembers.EUS_CustomerPhotosRow =
            '        DataHelpers.GetProfilesDefaultPhoto(Me.MasterProfileId)

            '    Dim memberReceivingMessagePhoto As DSMembers.EUS_CustomerPhotosRow =
            '        DataHelpers.GetProfilesDefaultPhoto(memberReceivingMessage.ProfileID)

            '    Dim result As clsUserDoes.clsLoadProfilesViewsResult =
            '        clsUserDoes.GetProfilesThumbViews(Me.GetCurrentProfile(), defaultPhoto, memberReceivingMessage, memberReceivingMessagePhoto, Me.IsHTTPS)

            '    lnkProfImg.NavigateUrl = result.CurrentMemberProfileViewUrl
            '    imgProf.ImageUrl = result.CurrentMemberImageUrl
            '    lnkProfLogin.NavigateUrl = result.CurrentMemberProfileViewUrl
            '    lnkProfLogin.Text = result.CurrentMemberLoginName

            '    lnkOtherProfImg.NavigateUrl = result.OtherMemberProfileViewUrl
            '    imgOtherProf.ImageUrl = result.OtherMemberImageUrl
            '    lnkOtherProfLogin.NavigateUrl = result.OtherMemberProfileViewUrl
            '    lnkOtherProfLogin.Text = result.OtherMemberLoginName

            '    If (Len(Request.QueryString("text")) > 0 AndAlso Request.QueryString("text").ToLower() = "privatephoto") Then
            '        txtMessageSubject.Text = CurrentPageData.GetCustomString("Private.Photo.Message.Subject")
            '        txtMessageText.Text = CurrentPageData.GetCustomString("Private.Photo.Message.Body")
            '        txtMessageText.Text = txtMessageText.Text.Replace("[LEVEL]", Request.QueryString("lvl"))
            '    End If
            'Catch ex As Exception
            '    WebErrorMessageBox(Me, ex, "GetProfilesThumbViews")
            'End Try




            'Dim unlockedConv As EUS_UnlockedConversation = clsUserDoes.GetUnlockedConversation(memberReceivingMessage.ProfileID, Me.MasterProfileId)
            'Dim acceptedOffer As EUS_Offer = clsUserDoes.GetAcceptedOrUnlockedOffer(memberReceivingMessage.ProfileID, Me.MasterProfileId)

            'If (acceptedOffer IsNot Nothing OrElse unlockedConv IsNot Nothing) Then

            '    If (acceptedOffer IsNot Nothing AndAlso acceptedOffer.Amount > 0) Then
            '        divDatingAmount.Visible = True
            '        lblDatingAmount.Text = "&euro;" & acceptedOffer.Amount
            '    ElseIf (unlockedConv IsNot Nothing AndAlso unlockedConv.CurrentOfferAmount > 0) Then
            '        divDatingAmount.Visible = True
            '        lblDatingAmount.Text = "&euro;" & unlockedConv.CurrentOfferAmount
            '    End If
            'End If




        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "LoadNewMessageView")
        End Try

    End Sub



    Private Sub LoadNewMessageView2()

        Try
            mvMessages.SetActiveView(vwConversation)

            lnkClearText.Visible = True
            lnkCancel.Visible = False

            first_message_info.Visible = True
            first_message_info_bottom.Visible = True
            updCnv.Visible = False
            updOffers.Visible = False
            write_message_wrap.Visible = True
            send_buttons.Visible = False
            write_message_wrap.Attributes.CssStyle.Add("display", "block")

            lnkFemaleFirstMsg.NavigateUrl = ProfileHelper.GetProfileNavigateUrl(CurrentLoginName)
            lnkFemaleFirstMsgLogin.NavigateUrl = ProfileHelper.GetProfileNavigateUrl(CurrentLoginName)
            lnkFemaleFirstMsgLogin.Text = CurrentLoginName
            lblMaleFirstMsgTopPanelText.Text = lblMaleFirstMsgTopPanelText.Text.Replace("[LOGIN-NAME]", CurrentLoginName)

            Dim fileName As String = ""
            Dim dr As DSMembers.EUS_CustomerPhotosRow = DataHelpers.GetProfilesDefaultPhoto(Me.UserIdInView)
            If (dr IsNot Nothing) Then fileName = dr.FileName

            Dim genderId As Integer = 2
            If (memberReceivingMessage IsNot Nothing) Then
                genderId = memberReceivingMessage.GenderId
            Else
                Using cmdb As CMSDBDataContext = Me.GetCMSDBDataContext
                    genderId = DataHelpers.GetEUS_Profile_GenderId_ByProfileID(cmdb, Me.UserIdInView)
                End Using

            End If
            imgFemaleFirstMsg.ImageUrl = ProfileHelper.GetProfileImageURL(Me.UserIdInView, fileName, genderId, True, Me.IsHTTPS, PhotoSize.D150)

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "LoadNewMessageView")
        End Try

    End Sub



    Private Sub CheckIsAllowedToSendMessage()

        Try

            Dim MaySendMessage_CheckSetting As Boolean = True

            If (String.IsNullOrEmpty(CurrentLoginName)) Then
                Return
            End If

            If (CurrentLoginName.ToUpper() = Me.GetCurrentProfile().LoginName.ToUpper()) Then
                ShowError_vwEmpty(ConversationErrorEnum.ErrorSendMessageToMySelf)

                'divActionError.Visible = True
                'lnkAlternativeAction.Text = CurrentPageData.GetCustomString("lnkAlternativeAction_Back")
                'If (Not String.IsNullOrEmpty(Me.BackUrl)) Then
                '    lnkAlternativeAction.NavigateUrl = Me.BackUrl
                'Else
                '    lnkAlternativeAction.NavigateUrl = "~/Members/"
                'End If
                'lblActionError.Text = CurrentPageData.GetCustomString("ErrorSendMessageToMySelf")
                'mvMessages.SetActiveView(vwEmpty)

                Return
            End If

            If (memberReceivingMessage Is Nothing) Then
                'If (String.IsNullOrEmpty(CurrentLoginName)) Then
                '    CurrentLoginName = DataHelpers.GetEUS_Profile_LoginName_ByProfileID(Me.CMSDBDataContext, Me.UserIdInView)
                'End If
                'memberReceivingMessage = DataHelpers.GetEUS_ProfileMasterByLoginName(Me.CMSDBDataContext, CurrentLoginName, ProfileStatusEnum.Approved, ProfileStatusEnum.LIMITED)
                Using cmdb As CMSDBDataContext = Me.GetCMSDBDataContext
                    memberReceivingMessage = DataHelpers.GetEUS_ProfileByProfileID(cmdb, Me.UserIdInView)
                End Using
                If (memberReceivingMessage IsNot Nothing) Then
                    If (memberReceivingMessage.Status = ProfileStatusEnum.Approved OrElse
                        memberReceivingMessage.Status = ProfileStatusEnum.LIMITED OrElse
                        memberReceivingMessage.Status = ProfileStatusEnum.Deleted) Then

                        CurrentLoginName = memberReceivingMessage.LoginName
                    Else
                        memberReceivingMessage = Nothing
                    End If
                End If
            End If

            If (memberReceivingMessage Is Nothing) Then
                ShowError_vwEmpty(ConversationErrorEnum.ErrorMemberDoesNotExistCannotSendMessage)

                'divActionError.Visible = True
                'lnkAlternativeAction.Text = CurrentPageData.GetCustomString("lnkAlternativeAction_Search")
                'lnkAlternativeAction.NavigateUrl = "~/Members/Search.aspx"
                'lblActionError.Text = CurrentPageData.GetCustomString("ErrorMemberDoesNotExistCannotSendMessage")
                'mvMessages.SetActiveView(vwEmpty)

                Return
            End If

            Me.UserIdInView = memberReceivingMessage.ProfileID
            CurrentLoginName = memberReceivingMessage.LoginName


            'Dim MaySendMessage As Boolean = False
            Dim femaleSend As Boolean = Me.IsFemale
            'Dim femaleSend As Boolean = MaySendMessage_CheckSetting
            'If (femaleSend) Then
            '    femaleSend = Me.IsFemale
            'End If
            If (femaleSend) Then
                Dim femaleReceiving As Boolean = ProfileHelper.IsFemale(memberReceivingMessage.GenderId)
                If (femaleSend AndAlso femaleReceiving) Then
                    ShowError_vwEmpty(ConversationErrorEnum.FemaleMemberCannotSendToFemale)

                    'divActionError.Visible = True
                    'lnkAlternativeAction.Text = CurrentPageData.GetCustomString("lnkAlternativeAction_Search")
                    'lnkAlternativeAction.NavigateUrl = "~/Members/Search.aspx"
                    'lblActionError.Text = CurrentPageData.GetCustomString("Error.FemaleMemberCannotSendToFemale")
                    'mvMessages.SetActiveView(vwEmpty)

                    Return
                End If
            End If

            Dim AlreadyUnlockedUnlimited As Boolean = GetHasCommunicationDict(Me.UserIdInView)

            'Dim AlreadyUnlockedUnlimited As Boolean = ModGlobals.AllowUnlimited AndAlso GetHasCommunication(Me.UserIdInView)
            'Dim AlreadyUnlockedUnlimited As Boolean = clsUserDoes.HasCommunication(Me.MasterProfileId, Me.UserIdInView)
            'Dim maleSend As Boolean = MaySendMessage_CheckSetting
            'If (maleSend) Then
            '    maleSend = ((SendOnce OrElse SendUnlimited) AndAlso Me.IsMale) OrElse (AlreadyUnlockedUnlimited AndAlso Me.IsMale)
            'End If

            SendOnce = True
            Dim maleSend As Boolean = ((SendOnce OrElse SendUnlimited) AndAlso Me.IsMale) OrElse (AlreadyUnlockedUnlimited AndAlso Me.IsMale)
            If (maleSend AndAlso Not AlreadyUnlockedUnlimited) Then

                'If (Not AlreadyUnlockedUnlimited) Then
                If (Not SendOnce AndAlso Not SendUnlimited) Then
                    'lblSendMessageErr.Text = Me.CurrentPageData.GetCustomString("ErrMayNotSendMessage")
                    'pnlSendMessageErr.Visible = True
                    maleSend = False
                End If


                If (SendOnce) Then
                    Dim hasCreditsToSendMessage As Boolean = HasRequiredCredits(UnlockType.UNLOCK_MESSAGE_SEND)
                    'If Not maleSend AndAlso hasCreditsToSendMessage Then maleSend = True
                    '' user has no credits
                    'If Not hasCreditsToSendMessage Then maleSend = False

                    maleSend = hasCreditsToSendMessage

                    If (Not maleSend) Then
                        Dim mySubscriptionMessages As Integer = GetSubsriptionAvailableMessages(Me.MasterProfileId, Me.UserIdInView, Me.SessionVariables.MemberData.Country)
                        If (mySubscriptionMessages > 0) Then
                            maleSend = True
                        End If
                    End If

                    If (Not maleSend) Then
                        Dim myFreeMessages As Integer = Me.GetMembersFreeMessagesAvailableToSEND(Me.MasterProfileId, Me.UserIdInView, Me.SessionVariables.MemberData.Country)
                        If (myFreeMessages > 0) Then maleSend = True
                    End If

                ElseIf (SendUnlimited) Then
                    Dim hasCreditsToSendMessageUnl As Boolean = HasRequiredCredits(UnlockType.UNLOCK_CONVERSATION)
                    If Not maleSend AndAlso hasCreditsToSendMessageUnl Then maleSend = True

                    ' user has no credits
                    If Not hasCreditsToSendMessageUnl Then maleSend = False

                End If


                'pnlSendMessageErr.Visible = True
                'lnkSendMessageAltAction.Visible = False
                'If (SendOnce) Then
                '    lblSendMessageErr.Text = CurrentPageData.GetCustomString("WarnSendMessageOnce")
                '    SiteRulesTIP.Visible = True
                'ElseIf (SendUnlimited) Then
                '    lblSendMessageErr.Text = CurrentPageData.GetCustomString("WarnSendMessageUnlimited")
                'End If
                ''End If

            End If

            MaySendMessage_CheckSetting = Not clsProfilesPrivacySettings.GET_DisableReceivingMESSAGESFromDifferentCountryFromTo(memberReceivingMessage.ProfileID, Me.MasterProfileId)
            If (MaySendMessage_CheckSetting = False) Then

                ShowErrorPopup(ConversationErrorEnum.Error_Sending_Receiver_Disabled_Messages_From_Diff_Country)

                'Dim popupMessage As String = WhatIsIt.OnMoreInfoClickFunc(ResolveUrl("~/Members/InfoWin.aspx?info=Error_Sending_Receiver_Disabled_Messages_From_Diff_Country&popup=popupWhatIs"), "Error! Can't send", 650, 350)
                'popupMessage = "function fnError_Sending_Receiver_DoNot_Have_Photo(){" & popupMessage & "};"
                'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Error_Sending_Receiver_DoNot_Have_Photo", popupMessage, True)


                'divActionError.Visible = True
                'lnkAlternativeAction.Text = CurrentPageData.GetCustomString("lnkAlternativeAction_Search")
                'lnkAlternativeAction.NavigateUrl = "~/Members/Search.aspx"
                'lblActionError.Text = CurrentPageData.GetCustomString("DisableReceivingMessagesFromDifferentCountry.Text")
                'pnlSendMessageErr.Visible = True
                'lnkSendMessageAltAction.Visible = True
                'lnkSendMessageAltAction.Text = CurrentPageData.GetCustomString("DisableReceivingMessagesFromDifferentCountry.Alternative.Action")
                'lnkSendMessageAltAction.NavigateUrl = CurrentPageData.GetCustomString("DisableReceivingMessagesFromDifferentCountry.Alternative.Action.URL")
                'lblSendMessageErr.Text = CurrentPageData.GetCustomString("DisableReceivingMessagesFromDifferentCountry.Text")

                ''mvMessages.SetActiveView(vwEmpty)

                ''Return
            End If

            If (Not MaySendMessage_CheckSetting) Then
                femaleSend = False
                maleSend = False
            End If


            'If (False AndAlso maleSend = False AndAlso Me.IsMale AndAlso MaySendMessage_CheckSetting) Then
            '    ' when man is going to send message  and settings allow to send,
            '    ' if he has no credits then
            '    ' check his country settings, if he is allowed to send any free message

            '    MaySendFreeMessage = clsUserDoes.MaySendFreeMessage(memberReceivingMessage.ProfileID, Me.MasterProfileId, Me.SessionVariables.MemberData.Country)
            '    If (MaySendFreeMessage) Then maleSend = True

            'End If


            If (femaleSend OrElse maleSend) Then
                txtWriteMsgText.Enabled = True
                btnSendMessage.Enabled = True
                If (btnSendMessage.CssClass.IndexOf("opacity70") > -1) Then btnSendMessage.CssClass = btnSendMessage.CssClass.Replace("opacity70", "")
                lnkCancel.Enabled = True
                If (lnkCancel.CssClass.IndexOf("opacity70") > -1) Then lnkCancel.CssClass = lnkCancel.CssClass.Replace("opacity70", "")
                btnRandomText.Enabled = True
                If (btnRandomText.CssClass.IndexOf("opacity70") > -1) Then btnRandomText.CssClass = btnRandomText.CssClass.Replace("opacity70", "")

            Else
                txtWriteMsgText.Enabled = False
                btnSendMessage.Enabled = False
                If (btnSendMessage.CssClass.IndexOf("opacity70") = -1) Then btnSendMessage.CssClass = btnSendMessage.CssClass & " opacity70"
                lnkCancel.Enabled = False
                If (lnkCancel.CssClass.IndexOf("opacity70") = -1) Then lnkCancel.CssClass = lnkCancel.CssClass & " opacity70"
                btnRandomText.Enabled = False
                If (btnRandomText.CssClass.IndexOf("opacity70") = -1) Then btnRandomText.CssClass = btnRandomText.CssClass & " opacity70"

                If (MaySendMessage_CheckSetting) Then

                    pnlSendMessageErr.Visible = True
                    lnkSendMessageAltAction.Visible = True


                    If (Me.IsMale) Then

                        If (SendOnce) Then
                            ShowErrorPanel(ConversationErrorEnum.ErrSendMessageOnceCreditsNotAvailable)
                            'lnkSendMessageAltAction.Text = CurrentPageData.GetCustomString("lnkErrSendMessageCreditsNotAvailable")
                            'lblSendMessageErr.Text = CurrentPageData.GetCustomString("ErrSendMessageOnceCreditsNotAvailable")
                            'lnkSendMessageAltAction.Visible = False

                        ElseIf (SendUnlimited) Then
                            ShowErrorPanel(ConversationErrorEnum.ErrSendMessageUnlimitedCreditsNotAvailable)
                            'lnkSendMessageAltAction.Text = CurrentPageData.GetCustomString("lnkErrSendMessageCreditsNotAvailable")
                            'lblSendMessageErr.Text = CurrentPageData.GetCustomString("ErrSendMessageUnlimitedCreditsNotAvailable")
                            'lnkSendMessageAltAction.Visible = True

                        Else
                            ShowErrorPanel(ConversationErrorEnum.ErrSendMessageOnceCreditsNotAvailable)
                            'lnkSendMessageAltAction.Text = CurrentPageData.GetCustomString("lnkErrSendMessageCreditsNotAvailable")
                            'lblSendMessageErr.Text = CurrentPageData.GetCustomString("ErrSendMessageOnceCreditsNotAvailable")
                            'lnkSendMessageAltAction.Visible = False

                        End If
                    End If

                End If 'MaySendMessage_CheckSetting

            End If



        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "LoadNewMessageView")
        End Try

    End Sub


    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        'MessagesView = MessagesViewEnum.INBOX
        LoadLAG()
    End Sub


    Protected Sub LoadLAG()
        Try

            'lnkNew.Text = "<span></span>" & CurrentPageData.GetCustomString("lnkNew")
            'lnkSent.Text = "<span></span>" & CurrentPageData.GetCustomString("lnkSent")
            'lnkInbox.Text = "<span></span>" & CurrentPageData.GetCustomString("lnkInbox")
            'lnkTrash.Text = "<span></span>" & CurrentPageData.GetCustomString("lnkTrash")
            ''lnkConversation.Text = "<span></span>" & CurrentPageData.GetCustomString("lnkConversation")

            'lblInfoMessage.Text = CurrentPageData.GetCustomString("lblInfoMessage")
            'lblSelectFilter.Text = CurrentPageData.GetCustomString("lblSelectFilter")

            'cbFilter.Items.Clear()
            'cbFilter.Items.Add("---", "0")
            'cbFilter.Items.Add(CurrentPageData.GetCustomString("cbFilter_Unread"), "Unread")
            'cbFilter.Items.Add(CurrentPageData.GetCustomString("cbFilter_Read"), "Read")

            'btnDeleteSeleted.Text = CurrentPageData.GetCustomString("btnDeleteSeleted")


            'lblReplyTitle.Text = CurrentPageData.GetCustomString("lblReplyTitle")
            'btnReplyMessage.Text = CurrentPageData.GetCustomString("btnReplyMessage")

            lnkCancel.Text = CurrentPageData.GetCustomString("lnkCancel")
            lnkClearText.Text = CurrentPageData.GetCustomString("lnkClearText")
            'lnkClearTextConfirm.Text = CurrentPageData.GetCustomString("lnkClearTextConfirm")
            txtWriteMsgText.NullText = CurrentPageData.GetCustomString("lblWatermark")
            lblWriteMsgHdr.Text = CurrentPageData.GetCustomString("lblReplyTitle_ND")
            btnSendMessage.Text = CurrentPageData.GetCustomString("btnReplyMessage")

            '' new message section
            ''lblSendMessageToTitle.Text = CurrentPageData.GetCustomString("lblSendMessageToTitle")
            'lnkBack.Text = CurrentPageData.GetCustomString("lnkBack")
            'lblYourMsgSubject.Text = CurrentPageData.GetCustomString("lblYourMsgSubject")
            'lblYourMsgBody.Text = CurrentPageData.GetCustomString("lblYourMsgBody")
            'lblYourFirstDateStartedTitle.Text = CurrentPageData.GetCustomString("lblYourFirstDateStartedTitle")
            'lblYourFirstDateStartedText.Text = CurrentPageData.GetCustomString("lblYourFirstDateStartedText")
            'lblYourMsgFrom.Text = CurrentPageData.GetCustomString("lblYourMsgFrom")
            'lblYourMsgTo.Text = CurrentPageData.GetCustomString("lblYourMsgTo")
            'btnSendMessage.Text = CurrentPageData.GetCustomString("btnSendMessage")

            ' no messages
            msg_SearchOurMembersText.Text = CurrentPageData.GetCustomString("msg_SearchOurMembersText")
            lblNoMessagesText.Text = CurrentPageData.GetCustomString("lblNoConversationText") 'lblNoMessagesText
            lblProfileDeletedInfo.Text = CurrentPageData.GetCustomString("lblProfileDeletedInfo")
            'lblSendAnotherMessageInfo.Text = CurrentPageData.GetCustomString("lblSendAnotherMessageInfo")
            btnSendAnotherMessage.Text = CurrentPageData.GetCustomString("btnSendAnotherMessage")


            lnkMessages.Text = CurrentPageData.GetCustomString("CartMessagesView")
            lnkMessagesAll.Text = CurrentPageData.GetCustomString("lnkMessagesAll_ND")

            'imgItems.ImageUrl = "~/images/members_bar/7.png"
            lblItemsName.Text = Me.CurrentPageData.GetCustomString("CartConversationView")
            lnkViewDescription.Text = Me.CurrentPageData.GetCustomString("CartConversationWhatIsIt")
            WhatIsIt1.PopupControl.HeaderText = Me.CurrentPageData.GetCustomString("CartConversationView")
            lnkViewDescription.OnClientClick = OnMoreInfoClickFunc(
                lnkViewDescription.OnClientClick,
                ResolveUrl("~/Members/InfoWin.aspx?info=conversation"),
                Me.CurrentPageData.GetCustomString("CartConversationView"))

            WarningPopup_HeaderText = Me.CurrentPageData.GetCustomString("SuppressWarning_WhenReadingMessageFromDifferentCountry.Warning.HeaderText")
            WarningPopup_HeaderText = AppUtils.JS_PrepareString(WarningPopup_HeaderText)

            btnSave.Text = btnSave.Text.Replace("[CONTENT]", Me.CurrentPageData.GetCustomString("SuppressWarning_WhenReadingMessageFromDifferentCountry.Warning.Save.Option"))
            lnkBackToMainMenu.Text = Me.CurrentPageData.GetCustomString("Back.To.Menu.Left")
            lnkMoreMsgs.Text = Me.CurrentPageData.GetCustomString("btn.Load.More.Messages")
            'lnkLessMsgs.Text = Me.CurrentPageData.GetCustomString("btn.Show.Less.Messages")
            'lnkShowYesterday.Text = Me.CurrentPageData.GetCustomString("btn.Load.More.Messages.Yesterday")
            lnkShow7Days.Text = Me.CurrentPageData.GetCustomString("btn.Load.More.Messages.7Days")
            lnkShow30Days.Text = Me.CurrentPageData.GetCustomString("btn.Load.More.Messages.30Days")
            lnkShow3Months.Text = Me.CurrentPageData.GetCustomString("btn.Load.More.Messages.3Months")
            lnkShowAll.Text = Me.CurrentPageData.GetCustomString("btn.Load.More.Messages.All")
            lblHistoryButtonsTitle.Text = Me.CurrentPageData.GetCustomString("lblHistoryButtonsTitle")

            lblMaleFirstMsgTopPanelText.Text = Me.CurrentPageData.GetCustomString("lblMaleFirstMsgTopPanelText")
            lblMaleFirstMsgBottomPanelText.Text = Me.CurrentPageData.GetCustomString("lblMaleFirstMsgBottomPanelText")
            btnRandomText.Text = Me.CurrentPageData.GetCustomString("btnRandomText")

            btnSendMessagesUnl.Text = ReplaceCommonTookens(CurrentPageData.GetCustomString("btnSendMessagesUnl"))
            lblMaleFirstMsgBottomPanelText3.Text = Me.CurrentPageData.GetCustomString("lblMaleFirstMsgBottomPanelText3")

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub





    Protected Sub offersRepeater_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs)
        Try
            ExecCmd(e.CommandName, e.CommandArgument)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Private Sub ExecCmd(CommandName As String, CommandArgument As String)
        Try

            If (CommandName = "DELETEMSG") Then


                Dim MsgID As Long
                Long.TryParse(CommandArgument, MsgID)
                If (clsNullable.NullTo(Me.GetCurrentProfile().ReferrerParentId) > 0) Then
                    clsUserDoes.DeleteMessage(Me.MasterProfileId, MsgID, True)
                Else
                    clsUserDoes.DeleteMessage(Me.MasterProfileId, MsgID, False)
                End If

                Dim conversationLoaded As Boolean = False
                If (Me.MessageIdListInView IsNot Nothing) Then Me.MessageIdListInView.Remove(MsgID)
                If (Me.MessageIdListInView IsNot Nothing AndAlso Me.MessageIdListInView.Count > 0) Then
                    MsgID = Me.MessageIdListInView(0)
                    LoadUsersConversation(MsgID)
                    conversationLoaded = True
                End If

                If (Not conversationLoaded) Then
                    CurrentLoginName = Request.QueryString("p")
                    If (String.IsNullOrEmpty(CurrentLoginName)) Then CurrentLoginName = Request.QueryString("vw")
                    If (Me.MessageIdListInView Is Nothing AndAlso Not String.IsNullOrEmpty(CurrentLoginName)) Then

                        If (memberReceivingMessage IsNot Nothing) Then
                            CurrentLoginName = UrlUtils.RevertValidProfileURI(CurrentLoginName)
                            Using cmdb As CMSDBDataContext = Me.GetCMSDBDataContext
                                memberReceivingMessage = DataHelpers.GetEUS_ProfileMasterByLoginName(cmdb, CurrentLoginName, ProfileStatusEnum.Approved, ProfileStatusEnum.LIMITED)
                            End Using

                        End If

                        If (memberReceivingMessage IsNot Nothing) Then
                            Me.CurrentLoginName = memberReceivingMessage.LoginName
                            Dim m As EUS_Message = clsUserDoes.GetLastMessageForProfiles(Me.MasterProfileId, memberReceivingMessage.ProfileID)

                            MsgID = 0
                            If (m IsNot Nothing) Then MsgID = m.EUS_MessageID

                            Me.UserIdInView = memberReceivingMessage.ProfileID
                            LoadUsersConversation(MsgID)
                            convCtl.Visible = (convCtl.Count > 0)
                            conversationLoaded = True
                        End If

                    End If
                End If

                If (Not conversationLoaded) Then
                    LoadUsersConversation(0)
                    convCtl.Visible = (convCtl.Count > 0)
                End If
            End If


            If (CommandName = "VIEWMSG") Then

                Dim MsgID As Integer
                Integer.TryParse(CommandArgument, MsgID)
                LoadUsersConversation(MsgID)
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try


        If (CommandName = "REJECT_DELETECONV" OrElse CommandName = "REJECT_BLOCK") Then


            ' commands of Dates control, copied from dates.aspx
            Dim success = False
            Dim showNoPhotoNote = False

            Try
                Dim currentUserHasPhotos As Boolean = clsUserDoes.HasPhotos(Me.MasterProfileId, Me.MirrorProfileId)
                If (currentUserHasPhotos) Then

                    ''''''''''''''''''''''
                    'photo aware actions
                    ''''''''''''''''''''''
                    If (CommandName = "REJECT_DELETECONV") Then

                        ''''''''''''''''''''''
                        'photo aware action
                        ''''''''''''''''''''''

                        Dim OtherMemberProfileID As Integer
                        Integer.TryParse(CommandArgument.ToString(), OtherMemberProfileID)

                        If (OtherMemberProfileID > 0) Then
                            clsMessagesHelper.DeleteMessagesFromMemberInView(Me.MasterProfileId, Me.MasterProfileId, OtherMemberProfileID, MessagesViewEnum.SENT, (IsReferrer = True))
                            clsMessagesHelper.DeleteMessagesFromMemberInView(Me.MasterProfileId, OtherMemberProfileID, Me.MasterProfileId, MessagesViewEnum.INBOX, (IsReferrer = True))
                            clsMessagesHelper.DeleteMessagesFromMemberInView(Me.MasterProfileId, OtherMemberProfileID, Me.MasterProfileId, MessagesViewEnum.NEWMESSAGES, (IsReferrer = True))
                            clsMessagesHelper.DeleteMessagesFromMemberInView(Me.MasterProfileId, OtherMemberProfileID, Me.MasterProfileId, MessagesViewEnum.TRASH, (IsReferrer = True))

                            clsUserDoes.MarkDateOfferAsViewed(OtherMemberProfileID, Me.MasterProfileId)
                        End If

                        success = True

                    ElseIf (CommandName = "REJECT_BLOCK") Then

                        Dim OtherMemberProfileID As Integer
                        Integer.TryParse(CommandArgument.ToString(), OtherMemberProfileID)

                        If (OtherMemberProfileID > 0) Then
                            clsUserDoes.MarkAsBlocked(OtherMemberProfileID, Me.MasterProfileId)
                            clsUserDoes.MarkDateOfferAsViewed(OtherMemberProfileID, Me.MasterProfileId)
                        End If

                        success = True

                    End If

                Else
                    showNoPhotoNote = True
                End If

            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try



            Try

                If (showNoPhotoNote) Then
                    Response.Redirect("~/Members/Photos.aspx", False)
                    HttpContext.Current.ApplicationInstance.CompleteRequest()

                ElseIf (success) Then
                    LoadUsersConversation(Me.MessageIdInView)
                End If
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try

        End If


    End Sub



    Private Function SendMessageToUser(Subject As String, Body As String) As Long
        Dim NewMessageID As Long
        Try
            Dim MaySendMessage As Boolean = True
            Dim ReferrerMaySendMessage As ReferrerStopSendingMessageEnum = ReferrerStopSendingMessageEnum.None
            Dim AlreadyUnlockedUnlimited As Boolean = GetHasCommunicationDict(Me.UserIdInView)

            If (Me.IsMale) Then

                MaySendMessage = AlreadyUnlockedUnlimited
                If (Not AlreadyUnlockedUnlimited) Then
                    If (Not SendOnce AndAlso Not SendUnlimited) Then
                        'lblSendMessageErr.Text = Me.CurrentPageData.GetCustomString("ErrMayNotSendMessage")
                        'pnlSendMessageErr.Visible = True
                        MaySendMessage = False
                    End If

                    If (SendOnce OrElse SendUnlimited) Then
                        MaySendMessage = True
                    End If

                End If
            ElseIf (Me.IsFemale) Then
                If (Me.IsReferrer) Then


                    ReferrerMaySendMessage = clsUserDoes.IsReferrerAllowedSendMessage(Me.UserIdInView, Me.MasterProfileId)
                    MaySendMessage = (ReferrerMaySendMessage = ReferrerStopSendingMessageEnum.None)

                Else
                    MaySendMessage = True
                End If
            End If


            If (ReferrerMaySendMessage <> ReferrerStopSendingMessageEnum.None) Then
                'lblSubjectErr.Text = Me.CurrentPageData.GetCustomString("Error.Referrer.MayNot.Send.Message")
                'pnlSubjectErr.Visible = True

                If (ReferrerMaySendMessage = ReferrerStopSendingMessageEnum.RecipientHasNoPhoto) Then

                    ShowErrorPopup(ConversationErrorEnum.ReferrerStopSendingMessageEnum_RecipientHasNoPhoto)
                    'Dim popupMessage As String = WhatIsIt.OnMoreInfoClickFunc(ResolveUrl("~/Members/InfoWin.aspx?info=Error_Sending_Receiver_DoNot_Have_Photo&popup=popupWhatIs"), "Error! Can't send", 650, 350)
                    'popupMessage = "function fnError_Sending_Receiver_DoNot_Have_Photo(){" & popupMessage & "};"
                    'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Error_Sending_Receiver_DoNot_Have_Photo", popupMessage, True)

                End If
                If (ReferrerMaySendMessage = ReferrerStopSendingMessageEnum.MaxAllowedMessagesSent) Then

                    ShowErrorPopup(ConversationErrorEnum.ReferrerStopSendingMessageEnum_MaxAllowedMessagesSent)
                    'Dim popupMessage As String = WhatIsIt.OnMoreInfoClickFunc(ResolveUrl("~/Members/InfoWin.aspx?info=Error_Sending_MaxAllowedMessagesSent&popup=popupWhatIs"), "Error! Can't send", 650, 350)
                    'popupMessage = "function fnError_Sending_Receiver_DoNot_Have_Photo(){" & popupMessage & "};"
                    'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Error_Sending_Receiver_DoNot_Have_Photo", popupMessage, True)

                End If

                MaySendMessage = False
            End If

            If (Not (Not String.IsNullOrEmpty(Body) OrElse Not String.IsNullOrEmpty(Subject))) Then
                'lblSubjectErr.Text = Me.CurrentPageData.GetCustomString("ErrorSubjectRequired")
                'pnlSubjectErr.Visible = True

                MaySendMessage = False
            End If


            '   Dim messagePaid As Boolean = Me.IsFemale OrElse ((SendOnce OrElse SendUnlimited) AndAlso Me.IsMale) OrElse (AlreadyUnlockedUnlimited AndAlso Me.IsMale)
            '    Dim failMessage As String = ""

            If (MaySendMessage) Then

                'Dim MaySendMessage As Boolean = False
                Dim femaleSend As Boolean = Me.IsFemale
                'Dim femaleSend As Boolean = MaySendMessage_CheckSetting
                'If (femaleSend) Then
                '    femaleSend = Me.IsFemale
                'End If

                If (memberReceivingMessage Is Nothing) Then
                    Using cmdb As CMSDBDataContext = Me.GetCMSDBDataContext
                        If (String.IsNullOrEmpty(CurrentLoginName)) Then CurrentLoginName = DataHelpers.GetEUS_Profile_LoginName_ByProfileID(cmdb, Me.UserIdInView)
                        memberReceivingMessage = DataHelpers.GetEUS_ProfileMasterByLoginName(cmdb, CurrentLoginName, ProfileStatusEnum.Approved, ProfileStatusEnum.LIMITED)
                    End Using
                   
                End If

                If (femaleSend) Then
                    Dim femaleReceiving As Boolean = ProfileHelper.IsFemale(memberReceivingMessage.GenderId)
                    If (femaleReceiving) Then
                        'divActionError.Visible = True
                        'lnkAlternativeAction.Text = CurrentPageData.GetCustomString("lnkAlternativeAction_Search")
                        'lnkAlternativeAction.NavigateUrl = "~/Members/Search.aspx"
                        'lblActionError.Text = CurrentPageData.GetCustomString("Error.FemaleMemberCannotSendToFemale")


                        ShowErrorPanel(ConversationErrorEnum.FemaleMemberCannotSendToFemale_SendingMessage)
                        femaleSend = False

                        'mvMessages.SetActiveView(vwEmpty)
                        'Return
                    End If
                End If


                'Dim AlreadyUnlockedUnlimited As Boolean = GetHasCommunicationDict(Me.UserIdInView)
                ''Dim AlreadyUnlockedUnlimited As Boolean = ModGlobals.AllowUnlimited AndAlso GetHasCommunication(Me.UserIdInView)
                ''Dim AlreadyUnlockedUnlimited As Boolean = clsUserDoes.HasCommunication(Me.MasterProfileId, Me.UserIdInView)


                'Dim maleSend As Boolean = MaySendMessage_CheckSetting
                'If (maleSend) Then
                '    maleSend = ((SendOnce OrElse SendUnlimited) AndAlso Me.IsMale) OrElse (AlreadyUnlockedUnlimited AndAlso Me.IsMale)
                'End If
                Dim maleSend As Boolean = (SendOnce OrElse SendUnlimited OrElse AlreadyUnlockedUnlimited) AndAlso Me.IsMale
                If (maleSend AndAlso Not AlreadyUnlockedUnlimited) Then

                    'If (Not AlreadyUnlockedUnlimited) Then
                    If (Not SendOnce AndAlso Not SendUnlimited) Then
                        'lblSendMessageErr.Text = Me.CurrentPageData.GetCustomString("ErrMayNotSendMessage")
                        'pnlSendMessageErr.Visible = True
                        maleSend = False
                    End If


                    If (SendOnce) Then
                        Dim hasCreditsToSendMessage As Boolean = HasRequiredCredits(UnlockType.UNLOCK_MESSAGE_SEND)
                        'If Not maleSend AndAlso hasCreditsToSendMessage Then maleSend = True

                        '' user has no credits
                        'If Not hasCreditsToSendMessage Then maleSend = False
                        maleSend = hasCreditsToSendMessage

                        If (Not maleSend) Then
                            Dim mySubscriptionMessages As Integer = GetSubsriptionAvailableMessages(Me.MasterProfileId, Me.UserIdInView, Me.SessionVariables.MemberData.Country)
                            If (mySubscriptionMessages > 0) Then
                                maleSend = True
                            End If
                        End If

                        If (Not maleSend) Then
                            Dim myFreeMessages As Integer = Me.GetMembersFreeMessagesAvailableToSEND(Me.MasterProfileId, Me.UserIdInView, Me.SessionVariables.MemberData.Country)
                            If (myFreeMessages > 0) Then maleSend = True
                        End If

                    ElseIf (SendUnlimited) Then
                        Dim hasCreditsToSendMessageUnl As Boolean = HasRequiredCredits(UnlockType.UNLOCK_CONVERSATION)
                        If Not maleSend AndAlso hasCreditsToSendMessageUnl Then maleSend = True

                        ' user has no credits
                        If Not hasCreditsToSendMessageUnl Then maleSend = False

                    End If

                    'pnlSendMessageErr.Visible = True
                    'lnkSendMessageAltAction.Visible = False

                    'If (SendOnce) Then
                    '    lblSendMessageErr.Text = CurrentPageData.GetCustomString("WarnSendMessageOnce")
                    '    SiteRulesTIP.Visible = True
                    'ElseIf (SendUnlimited) Then
                    '    lblSendMessageErr.Text = CurrentPageData.GetCustomString("WarnSendMessageUnlimited")
                    'End If
                    ''End If

                End If

                'If (False AndAlso maleSend = False AndAlso Me.IsMale) Then
                '    ' when man is going to send message  and settings allow to send,
                '    ' if he has no credits then
                '    ' check his country settings, if he is allowed to send any free message

                '    MaySendFreeMessage = clsUserDoes.MaySendFreeMessage(memberReceivingMessage.ProfileID, Me.MasterProfileId, Me.SessionVariables.MemberData.Country)
                '    If (MaySendFreeMessage) Then maleSend = True
                'End If

                Dim MaySendMessage_CheckSetting As Boolean = False
                If (maleSend OrElse femaleSend) Then

                    MaySendMessage_CheckSetting = Not clsProfilesPrivacySettings.GET_DisableReceivingMESSAGESFromDifferentCountryFromTo(memberReceivingMessage.ProfileID, Me.MasterProfileId)
                    If (MaySendMessage_CheckSetting = False) Then
                        'divActionError.Visible = True
                        'lnkAlternativeAction.Text = CurrentPageData.GetCustomString("lnkAlternativeAction_Search")
                        'lnkAlternativeAction.NavigateUrl = "~/Members/Search.aspx"
                        'lblActionError.Text = CurrentPageData.GetCustomString("DisableReceivingMessagesFromDifferentCountry.Text")

                        ShowErrorPopup(ConversationErrorEnum.Error_Sending_Receiver_Disabled_Messages_From_Diff_Country)
                        'Dim popupMessage As String = WhatIsIt.OnMoreInfoClickFunc(ResolveUrl("~/Members/InfoWin.aspx?info=Error_Sending_Receiver_Disabled_Messages_From_Diff_Country&popup=popupWhatIs"), "Error! Can't send", 650, 350)
                        'popupMessage = "function fnError_Sending_Receiver_DoNot_Have_Photo(){" & popupMessage & "};"
                        'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Error_Sending_Receiver_DoNot_Have_Photo", popupMessage, True)


                        femaleSend = False
                        maleSend = False

                        'pnlSendMessageErr.Visible = True
                        'lnkSendMessageAltAction.Visible = True
                        'lnkSendMessageAltAction.Text = CurrentPageData.GetCustomString("DisableReceivingMessagesFromDifferentCountry.Alternative.Action")
                        'lnkSendMessageAltAction.NavigateUrl = CurrentPageData.GetCustomString("DisableReceivingMessagesFromDifferentCountry.Alternative.Action.URL")
                        'lblSendMessageErr.Text = CurrentPageData.GetCustomString("DisableReceivingMessagesFromDifferentCountry.Text")
                        ''mvMessages.SetActiveView(vwEmpty)
                        ''Return
                    End If

                End If

                If (Not MaySendMessage_CheckSetting) Then
                    femaleSend = False
                    maleSend = False
                End If


                If (femaleSend OrElse maleSend) Then
                    txtWriteMsgText.Enabled = True
                    btnSendMessage.Enabled = True

                Else
                    txtWriteMsgText.Enabled = False
                    btnSendMessage.Enabled = False
                    MaySendMessage = False


                    If (MaySendMessage_CheckSetting) Then

                        pnlSendMessageErr.Visible = True
                        lnkSendMessageAltAction.Visible = True
                        MaySendMessage = False


                        If (Me.IsMale) Then
                            If (SendOnce) Then
                                ShowErrorPanel(ConversationErrorEnum.ErrSendMessageOnceCreditsNotAvailable)
                                'lnkSendMessageAltAction.Text = CurrentPageData.GetCustomString("lnkErrSendMessageCreditsNotAvailable")
                                'lblSendMessageErr.Text = CurrentPageData.GetCustomString("ErrSendMessageOnceCreditsNotAvailable")
                                'lnkSendMessageAltAction.Visible = False

                            ElseIf (SendUnlimited) Then
                                ShowErrorPanel(ConversationErrorEnum.ErrSendMessageUnlimitedCreditsNotAvailable)
                                'lnkSendMessageAltAction.Text = CurrentPageData.GetCustomString("lnkErrSendMessageCreditsNotAvailable")
                                'lblSendMessageErr.Text = CurrentPageData.GetCustomString("ErrSendMessageUnlimitedCreditsNotAvailable")
                                'lnkSendMessageAltAction.Visible = True
                            Else
                                ShowErrorPanel(ConversationErrorEnum.ErrSendMessageOnceCreditsNotAvailable)
                                'lnkSendMessageAltAction.Text = CurrentPageData.GetCustomString("lnkErrSendMessageCreditsNotAvailable")
                                'lblSendMessageErr.Text = CurrentPageData.GetCustomString("ErrSendMessageOnceCreditsNotAvailable")
                                'lnkSendMessageAltAction.Visible = False

                            End If
                        End If


                    End If 'MaySendMessage_CheckSetting

                End If
            End If

            If (MaySendMessage) Then
                Dim result As PerformMessageSendResultEnum = PerformMessageSendResultEnum.none
                NewMessageID = PerformMessageSend(Subject, Body, Me.UserIdInView, AlreadyUnlockedUnlimited, result)


                If (result = PerformMessageSendResultEnum.success) Then
                    If (MaySendFreeMessage) Then IsFreeMessageSent = True

                    ' o andras, epeidi plirwnei, dimiourgeitai neo date
                    clsUserDoes.MakeNewDate(Me.MasterProfileId, Me.UserIdInView, Me.OfferId, Me.IsMale)

                    If (String.IsNullOrEmpty(CurrentLoginName)) Then
                        Using cmdb As CMSDBDataContext = Me.GetCMSDBDataContext


                            CurrentLoginName = DataHelpers.GetEUS_Profile_LoginName_ByProfileID(cmdb, Me.UserIdInView)
                        End Using
 End If
                    If (Not String.IsNullOrEmpty(Request.QueryString("p"))) Then
                        Dim url As String = ResolveUrl("~/Members/Messages.aspx?vw=" & CurrentLoginName & "&t=" & MessagesViewEnum.SENT.ToString())
                        Response.Redirect(url, False)
                    End If
                End If
            End If


        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Return NewMessageID
    End Function


    Private Sub Messages_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        Try

            btnSendMessageOnce.Text = ReplaceCommonTookens(CurrentPageData.GetCustomString("btnSendMessageOnce_ND"))
            lblMaleFirstMsgBottomPanelText2.Text = BasePage.ReplaceCommonTookens(Me.CurrentPageData.VerifyCustomString("lblMaleFirstMsgBottomPanelText2"))

            If (Me.IsMale) Then
                Dim HasRequiredCredits_UNLOCK_MESSAGE_SEND As Boolean = HasRequiredCredits(UnlockType.UNLOCK_MESSAGE_SEND)
                Dim maleSend As Boolean = HasRequiredCredits_UNLOCK_MESSAGE_SEND

                If (Not maleSend) Then
                    Dim mySubscriptionMessages As Integer = GetSubsriptionAvailableMessages(Me.MasterProfileId, Me.UserIdInView, Me.SessionVariables.MemberData.Country)
                    If (mySubscriptionMessages > 0) Then
                        btnSendMessageOnce.Text = ReplaceCommonTookens(CurrentPageData.GetCustomString("btnSendMessageWithSubscription"))
                        lblMaleFirstMsgBottomPanelText2.Text = BasePage.ReplaceCommonTookens(Me.CurrentPageData.VerifyCustomString("lblMaleFirstMsgBottomPanelText2.Send.By.Subscription"))
                        maleSend = True
                    End If
                End If

                If (Not maleSend) Then
                    Dim myFreeMessages As Integer = Me.GetMembersFreeMessagesAvailableToSEND(Me.MasterProfileId, Me.UserIdInView, Me.SessionVariables.MemberData.Country)
                    If (myFreeMessages > 0) Then
                        btnSendMessageOnce.Text = ReplaceCommonTookens(CurrentPageData.GetCustomString("btnSendMessageForFree"))
                        lblMaleFirstMsgBottomPanelText2.Text = BasePage.ReplaceCommonTookens(Me.CurrentPageData.VerifyCustomString("lblMaleFirstMsgBottomPanelText2.Send.Free"))
                        maleSend = True
                    End If
                End If

            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Protected Sub btnSendMessage_Click(sender As Object, e As EventArgs) Handles btnSendMessage.Click
        Try
            If (Not String.IsNullOrEmpty(txtWriteMsgText.Text)) Then
                Dim replySubject As String = Me.SubjectInView
                If (Not String.IsNullOrEmpty(replySubject) AndAlso Not replySubject.StartsWith(globalStrings.GetCustomString("Re") & ":")) Then
                    replySubject = globalStrings.GetCustomString("Re") & ": " & replySubject
                End If
                If (replySubject Is Nothing) Then replySubject = ""

                Me.SendOnce = True

                '''''''''''''
                'reset session property to update available credits, when master.page loads
                Session("CustomerAvailableCredits") = Nothing
                '''''''''''''

                Dim NewMessageID As Integer = SendMessageToUser(replySubject, txtWriteMsgText.Text)
                If (NewMessageID > 0) Then
                    txtWriteMsgText.Text = ""
                    If (IsMaleSendingMessageFirstTime) Then
                        IsMaleSendingMessageFirstTime = False
                        Using cmdb As CMSDBDataContext = Me.GetCMSDBDataContext
                            If (String.IsNullOrEmpty(CurrentLoginName)) Then CurrentLoginName = DataHelpers.GetEUS_Profile_LoginName_ByProfileID(cmdb, Me.UserIdInView)
                        End Using
                        Response.Redirect(ResolveUrl("~/Members/Conversation.aspx?vw=" & CurrentLoginName_enc))
                    End If
                End If

                CheckIsAllowedToSendMessage()

                'If (MaySendFreeMessage) Then
                '    MaySendFreeMessage = clsUserDoes.MaySendFreeMessage(memberReceivingMessage.ProfileID, Me.MasterProfileId, Me.SessionVariables.MemberData.Country)
                'End If
                ''LoadUsersConversation(Me.MessageIdInView)
            Else
                ShowErrorPanel(ConversationErrorEnum.ErrorEmptyMessage)
            End If


        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Protected Shared Function GetCountMessagesString(ItemsCount As Object) As String

        If (Not AppUtils.IsDBNullOrNothingOrEmpty(ItemsCount)) Then
            Try
                If (ItemsCount > 0) Then Return "(" & ItemsCount & ")"
            Catch ex As Exception

            End Try
        End If

        Return ""
    End Function


    Protected Shared Function IsVisible(ItemsCount As Object) As Boolean

        If (Not AppUtils.IsDBNullOrNothingOrEmpty(ItemsCount)) Then
            Try
                If (ItemsCount > 1) Then Return True
            Catch ex As Exception

            End Try
        End If

        Return False
    End Function


    Protected Sub mvMessages_ActiveViewChanged(sender As Object, e As EventArgs) Handles mvMessages.ActiveViewChanged

    End Sub



    Private Sub BindNewDates()
        Try

            newDates.ShowEmptyListText = True

            If (Me.GetCurrentProfile(True) IsNot Nothing) Then

                ' bind new offers
                Try
                    Dim otherUser As Integer = Me.UserIdInView
                    If (Me.UserIdInView > 1) OrElse (Me.SendAnotherMessageToRecipient > 0) Then
                        otherUser = Me.UserIdInView
                        If (otherUser = 0) Then otherUser = Me.SendAnotherMessageToRecipient
                    End If

                    Dim countRows As Integer
                    Dim performCount As Boolean = False
                    '    Dim NumberOfRecordsToReturn As Integer = 1

                    Dim prms As New clsDatesHelperParameters()
                    prms.CurrentProfileId = Me.MasterProfileId
                    prms.sorting = DatesSortEnum.None
                    prms.ReturnRecordsWithStatus = ProfileStatusEnum.Approved
                    prms.zipstr = Me.SessionVariables.MemberData.Zip
                    prms.latitudeIn = Me.SessionVariables.MemberData.latitude
                    prms.longitudeIn = Me.SessionVariables.MemberData.longitude
                    prms.Distance = clsSearchHelper.DISTANCE_DEFAULT
                    prms.performCount = performCount
                    prms.rowNumberMin = 0
                    prms.rowNumberMax = 1
                    prms.OtherProfileId = otherUser

                    Dim ds As DataSet = clsSearchHelper.GetNewDatesDataTable(prms)

                    If (prms.performCount) Then
                        countRows = ds.Tables(0).Rows(0)(0)
                        Dim dt As DataTable = ds.Tables(1)
                        If (countRows > 0) Then
                            FillOfferControlList(dt, newDates)
                            newDates.ShowEmptyListText = False
                        Else
                            FillOfferControlList(dt, newDates, True)
                            newDates.ShowEmptyListText = False
                        End If
                    Else
                        countRows = ds.Tables(0).Rows.Count
                        Dim dt As DataTable = ds.Tables(0)
                        If (countRows > 0) Then
                            FillOfferControlList(dt, newDates)
                            newDates.ShowEmptyListText = False
                        Else
                            FillOfferControlList(dt, newDates, True)
                            newDates.ShowEmptyListText = False
                        End If
                    End If

                    newDates.DataBind()

                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "")

                    newDates.UsersList = Nothing
                    newDates.DataBind()
                End Try
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


    Private Sub FillOfferControlList(dt As DataTable, offersControl As Dating.Server.Site.Web.DatesControl, Optional getDummyDate As Boolean = False)


        Dim drDefaultPhoto As DSMembers.EUS_CustomerPhotosRow = DataHelpers.GetProfilesDefaultPhoto(Me.MasterProfileId)
        Dim uliCurrentMember As New clsWinkUserListItem()
        uliCurrentMember.ProfileID = Me.MasterProfileId
        uliCurrentMember.MirrorProfileID = Me.MirrorProfileId
        uliCurrentMember.LoginName = Me.SessionVariables.MemberData.LoginName
        uliCurrentMember.Genderid = Me.SessionVariables.MemberData.GenderId
        uliCurrentMember.ProfileViewUrl = ResolveUrl("~/Members/Profile.aspx?p=" & HttpUtility.UrlEncode(Me.SessionVariables.MemberData.LoginName)) & "&t=3"


        uliCurrentMember.ImageFileName = ""
        If (drDefaultPhoto IsNot Nothing) Then
            uliCurrentMember.ImageFileName = drDefaultPhoto.FileName
            uliCurrentMember.ImageUploadDate = drDefaultPhoto.DateTimeToUploading.ToLocalTime().ToString("d MMM, yyyy H:mm")
        End If


        uliCurrentMember.ImageUrl = ProfileHelper.GetProfileImageURL(Me.MasterProfileId, uliCurrentMember.ImageFileName, Me.SessionVariables.MemberData.GenderId, True, Me.IsHTTPS, PhotoSize.D150)
        uliCurrentMember.ImageThumbUrl = uliCurrentMember.ImageUrl


        Dim mins As Integer = clsConfigValues.Get__members_online_minutes()
        Dim LastActivityUTCDate As DateTime = Date.UtcNow.AddMinutes(-mins)
        Dim hours As Integer = clsConfigValues.Get__members_online_recently_hours()
        Dim LastActivityRecentlyUTCDate As DateTime = Date.UtcNow.AddHours(-hours)

        If (getDummyDate) Then

            Try
                Dim otherUser As Integer = Me.UserIdInView
                If (Me.UserIdInView > 1) OrElse (Me.SendAnotherMessageToRecipient > 0) Then
                    otherUser = Me.UserIdInView
                    If (otherUser = 0) Then otherUser = Me.SendAnotherMessageToRecipient
                End If

                If (memberReceivingMessage Is Nothing) Then
                    Using cmdb As CMSDBDataContext = Me.GetCMSDBDataContext
                        memberReceivingMessage = DataHelpers.GetEUS_ProfileByProfileID(cmdb, otherUser)
                    End Using
                End If


                CurrentLoginName = memberReceivingMessage.LoginName

                Dim uli As New clsWinkUserListItem()
                uli.LAGID = Session("LAGID")
                uli.ProfileID = uliCurrentMember.ProfileID
                uli.MirrorProfileID = uliCurrentMember.MirrorProfileID
                uli.LoginName = uliCurrentMember.LoginName
                uli.Genderid = uliCurrentMember.Genderid
                uli.ProfileViewUrl = uliCurrentMember.ProfileViewUrl
                uli.ImageFileName = uliCurrentMember.ImageFileName
                uli.ImageUrl = uliCurrentMember.ImageUrl
                uli.ImageThumbUrl = uliCurrentMember.ImageThumbUrl
                uli.ImageUploadDate = uliCurrentMember.ImageUploadDate

                uli.OtherMemberBirthday = memberReceivingMessage.Birthday
                uli.OtherMemberLoginName = memberReceivingMessage.LoginName
                uli.OtherMemberProfileID = memberReceivingMessage.ProfileID
                uli.OtherMemberMirrorProfileID = memberReceivingMessage.MirrorProfileID
                uli.OtherMemberCity = memberReceivingMessage.City
                uli.OtherMemberRegion = memberReceivingMessage.Region
                uli.OtherMemberCountry = memberReceivingMessage.Country
                uli.OtherMemberGenderid = memberReceivingMessage.GenderId
                uli.OtherMemberHeading = memberReceivingMessage.AboutMe_Heading


                uli.OtherMemberHeight = ProfileHelper.GetHeightString(clsNullable.NullTo(memberReceivingMessage.PersonalInfo_HeightID), Me.GetLag())
                uli.OtherMemberPersonType = ProfileHelper.GetBodyTypeString(clsNullable.NullTo(memberReceivingMessage.PersonalInfo_BodyTypeID), Me.GetLag())
                uli.OtherMemberHair = ProfileHelper.GetHairColorString(clsNullable.NullTo(memberReceivingMessage.PersonalInfo_HairColorID), Me.GetLag())
                uli.OtherMemberEyes = ProfileHelper.GetEyeColorString(clsNullable.NullTo(memberReceivingMessage.PersonalInfo_EyeColorID), Me.GetLag())
                uli.OtherMemberEthnicity = ProfileHelper.GetEthnicityString(clsNullable.NullTo(memberReceivingMessage.PersonalInfo_EthnicityID), Me.GetLag())

                If (memberReceivingMessage.Birthday IsNot Nothing) Then
                    uli.OtherMemberAge = ProfileHelper.GetCurrentAge(memberReceivingMessage.Birthday)
                Else
                    uli.OtherMemberAge = 20
                End If

                uli.OtherMemberIsOnline = clsNullable.NullTo(memberReceivingMessage.IsOnline)
                If (uli.OtherMemberIsOnline) Then
                    Dim __LastActivityDateTime As DateTime? = memberReceivingMessage.LastActivityDateTime
                    If (__LastActivityDateTime.HasValue) Then
                        uli.OtherMemberIsOnline = __LastActivityDateTime >= LastActivityUTCDate
                    Else
                        uli.OtherMemberIsOnline = False
                    End If
                End If

                uli.OtherMemberImageFileName = ""
                Dim dr As DSMembers.EUS_CustomerPhotosRow = DataHelpers.GetProfilesDefaultPhoto(uli.OtherMemberProfileID)
                If (dr IsNot Nothing) Then uli.OtherMemberImageFileName = dr.FileName

                uli.OtherMemberImageUrl = ProfileHelper.GetProfileImageURL(uli.OtherMemberProfileID, uli.OtherMemberImageFileName, uli.OtherMemberGenderid, True, Me.IsHTTPS, PhotoSize.D150)
                uli.OtherMemberImageThumbUrl = uli.OtherMemberImageUrl



                uli.OtherMemberProfileViewUrl = ResolveUrl("~/Members/Profile.aspx?p=" & CurrentLoginName_enc) & "&t=4"

                'uli.Distance = dr("distance")

                'If (uli.Distance < gCarDistance) Then
                '    uli.DistanceCss = "distance_car"
                'Else
                '    uli.DistanceCss = "distance_plane"
                'End If

                If ({ProfileStatusEnum.Approved, ProfileStatusEnum.NewProfile, ProfileStatusEnum.Updating}.Contains(memberReceivingMessage.Status)) Then
                    uli.AllowSendMessage = True
                Else
                    uli.AllowSendMessage = False
                End If
                uli.SendMessageUrl = ResolveUrl("~/Members/Conversation.aspx?p=" & CurrentLoginName_enc)

                uli.AllowConversation = False
                uli.ConversationNavigateUrl = ResolveUrl("~/Members/Conversation.aspx?vw=" & CurrentLoginName_enc)

                uli.AllowHistory = True
                If (UserIdInView_IsDeleted) Then
                    uli.AllowHistory = False
                End If

                Dim hdrTitle As String = "<div id=""history-popup-title""><span class=""title-text"">" & Me.CurrentPageData.GetCustomString("DatesHistory_HeaderText") & "</span><span class=""title-image""></span></div>"
                uli.HistoryNavigateUrl = WhatIsIt.OnMoreInfoClickFunc(
                    ResolveUrl("~/Members/DatesHistory.aspx?vw=" & CurrentLoginName_enc),
                    hdrTitle,
                    734)


                offersControl.UsersList.Add(uli)
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try
        End If


        Dim rowsCount As Integer
        For rowsCount = 0 To dt.Rows.Count - 1
            'For Each dr As DataRow In dt.Rows
            If (rowsCount > 1) Then Exit For

            Try
                Dim dr As DataRow = dt.Rows(rowsCount)
                'Dim distance As Integer = ProfileHelper.CalculateDistance(Me.GetCurrentProfile(), dr)
                CurrentLoginName = dr("LoginName")

                Dim uli As New clsWinkUserListItem()
                uli.LAGID = Session("LAGID")
                uli.ProfileID = uliCurrentMember.ProfileID
                uli.MirrorProfileID = uliCurrentMember.MirrorProfileID
                uli.LoginName = uliCurrentMember.LoginName
                uli.Genderid = uliCurrentMember.Genderid
                uli.ProfileViewUrl = uliCurrentMember.ProfileViewUrl
                uli.ImageFileName = uliCurrentMember.ImageFileName
                uli.ImageUrl = uliCurrentMember.ImageUrl
                uli.ImageThumbUrl = uliCurrentMember.ImageThumbUrl
                uli.ImageUploadDate = uliCurrentMember.ImageUploadDate

                If (dt.Columns.Contains("Birthday")) Then uli.OtherMemberBirthday = If(Not dr.IsNull("Birthday"), dr("Birthday"), Nothing)
                uli.OtherMemberLoginName = dr("LoginName")
                uli.OtherMemberProfileID = dr("ProfileID")
                uli.OtherMemberMirrorProfileID = dr("MirrorProfileID")
                uli.OtherMemberCity = dr("City")
                uli.OtherMemberRegion = IIf(Not dr.IsNull("Region"), dr("Region"), "")
                uli.OtherMemberCountry = IIf(Not dr.IsNull("Country"), dr("Country"), "")
                uli.OtherMemberGenderid = dr("Genderid")
                uli.OtherMemberHeading = IIf(Not dr.IsNull("AboutMe_Heading"), dr("AboutMe_Heading"), String.Empty)


                If (Not dr.IsNull("PersonalInfo_HeightID")) Then
                    uli.OtherMemberHeight = ProfileHelper.GetHeightString(dr("PersonalInfo_HeightID"), Me.GetLag())
                End If


                If (Not dr.IsNull("PersonalInfo_BodyTypeID")) Then
                    uli.OtherMemberPersonType = ProfileHelper.GetBodyTypeString(dr("PersonalInfo_BodyTypeID"), Me.GetLag())
                End If


                If (Not dr.IsNull("PersonalInfo_HairColorID")) Then
                    uli.OtherMemberHair = ProfileHelper.GetHairColorString(dr("PersonalInfo_HairColorID"), Me.GetLag())
                End If


                If (Not dr.IsNull("PersonalInfo_EyeColorID")) Then
                    uli.OtherMemberEyes = ProfileHelper.GetEyeColorString(dr("PersonalInfo_EyeColorID"), Me.GetLag())
                End If


                If (Not dr.IsNull("PersonalInfo_EthnicityID")) Then
                    uli.OtherMemberEthnicity = ProfileHelper.GetEthnicityString(dr("PersonalInfo_EthnicityID"), Me.GetLag())
                End If

                If (Not dr.IsNull("Birthday")) Then
                    uli.OtherMemberAge = ProfileHelper.GetCurrentAge(dr("Birthday"))
                Else
                    uli.OtherMemberAge = 20
                End If


                uli.OtherMemberIsOnline = IIf(Not dr.IsNull("IsOnline"), dr("IsOnline"), False)
                Dim __LastActivityDateTime As DateTime? = IIf(Not dr.IsNull("LastActivityDateTime"), dr("LastActivityDateTime"), Nothing)

                If (dt.Columns.Contains("IsOnlineNow")) Then
                    uli.OtherMemberIsOnline = IIf(Not dr.IsNull("IsOnlineNow"), dr("IsOnlineNow"), False)
                Else
                    If (uli.OtherMemberIsOnline) Then
                        If (__LastActivityDateTime.HasValue) Then
                            uli.OtherMemberIsOnline = __LastActivityDateTime >= LastActivityUTCDate
                        Else
                            uli.OtherMemberIsOnline = False
                        End If
                    End If
                End If


                If (dt.Columns.Contains("IsOnlineRecently")) Then
                    uli.OtherMemberIsOnlineRecently = IIf(Not dr.IsNull("IsOnlineRecently"), dr("IsOnlineRecently"), False)
                Else
                    If (uli.OtherMemberIsOnline) Then
                        If (__LastActivityDateTime.HasValue) Then
                            uli.OtherMemberIsOnlineRecently = __LastActivityDateTime >= LastActivityRecentlyUTCDate
                        Else
                            uli.OtherMemberIsOnlineRecently = False
                        End If
                    End If
                End If

                uli.OtherMemberImageFileName = dr("FileName").ToString()
                'If (Not dr.IsNull("FileName")) Then
                'Else
                '    uli.OtherMemberImageFileName = ""
                'End If

                'uli.OtherMemberImageUrl = ProfileHelper.GetProfileImageURL(uli.OtherMemberProfileID, uli.OtherMemberImageFileName, uli.OtherMemberGenderid, True, Me.IsHTTPS, PhotoSize.D150)
                If (dt.Columns.Contains("HasPhoto")) Then
                    If (Not dr.IsNull("HasPhoto") AndAlso (dr("HasPhoto").ToString() = "1" OrElse dr("HasPhoto").ToString() = "True")) Then
                        ' user has photo
                        If (uli.OtherMemberImageFileName IsNot Nothing) Then
                            'has public photos
                            uli.OtherMemberImageUrl = ProfileHelper.GetProfileImageURL(uli.OtherMemberProfileID, uli.OtherMemberImageFileName, uli.OtherMemberGenderid, True, Me.IsHTTPS, PhotoSize.D150)

                        Else
                            'has private photos
                            uli.OtherMemberImageUrl = ProfileHelper.GetPrivateImageURL(uli.OtherMemberGenderid)

                        End If
                    Else
                        ' has no photo
                        uli.OtherMemberImageUrl = ProfileHelper.GetDefaultImageURL(uli.OtherMemberGenderid)

                    End If
                    If IsHTTPS AndAlso uli.OtherMemberImageUrl.StartsWith("http:") Then uli.OtherMemberImageUrl = "https" & uli.OtherMemberImageUrl.Remove(0, "http".Length)
                Else
                    uli.OtherMemberImageUrl = ProfileHelper.GetProfileImageURL(uli.OtherMemberProfileID, uli.OtherMemberImageFileName, uli.OtherMemberGenderid, True, Me.IsHTTPS, PhotoSize.D150)
                End If
                uli.OtherMemberImageThumbUrl = uli.OtherMemberImageUrl



                uli.OtherMemberProfileViewUrl = ResolveUrl("~/Members/Profile.aspx?p=" & CurrentLoginName_enc) & "&t=5"

                uli.Distance = dr("distance")

                If (uli.Distance < gCarDistance) Then
                    uli.DistanceCss = "distance_car"
                Else
                    uli.DistanceCss = "distance_plane"
                End If

                If ({ProfileStatusEnum.Approved, ProfileStatusEnum.NewProfile, ProfileStatusEnum.Updating}.Contains(dr("Status"))) Then
                    uli.AllowSendMessage = True
                Else
                    uli.AllowSendMessage = False
                End If
                uli.SendMessageUrl = ResolveUrl("~/Members/Conversation.aspx?p=" & CurrentLoginName_enc)

                uli.AllowConversation = False
                uli.ConversationNavigateUrl = ResolveUrl("~/Members/Conversation.aspx?vw=" & CurrentLoginName_enc)

                uli.AllowHistory = True
                If (UserIdInView_IsDeleted) Then
                    uli.AllowHistory = False
                End If

                Dim hdrTitle As String = "<div id=""history-popup-title""><span class=""title-text"">" & Me.CurrentPageData.GetCustomString("DatesHistory_HeaderText") & "</span><span class=""title-image""></span></div>"
                uli.HistoryNavigateUrl = WhatIsIt.OnMoreInfoClickFunc(
                    ResolveUrl("~/Members/DatesHistory.aspx?vw=" & CurrentLoginName_enc),
                    hdrTitle,
                    734)

                uli.AllowRejectsMenu = True

                '' CHECK OFFER CASES
                uli.OfferID = dr("OfferID")
                Dim offerRecVals As New Offers3.OfferRecordValues()
                offerRecVals.OfferID = dr("OfferID")
                offerRecVals.OfferTypeID = IIf(Not dr.IsNull("OffersOfferTypeID"), dr("OffersOfferTypeID"), 0)
                offerRecVals.OffersStatusID = IIf(Not dr.IsNull("OffersStatusID"), dr("OffersStatusID"), 0)
                offerRecVals.OffersFromProfileID = IIf(Not dr.IsNull("OffersFromProfileID"), dr("OffersFromProfileID"), 0)
                offerRecVals.OffersToProfileID = IIf(Not dr.IsNull("OffersToProfileID"), dr("OffersToProfileID"), 0)

                uli.OfferAmount = IIf(Not dr.IsNull("OffersAmount"), dr("OffersAmount"), 0)

                If (dr("CommunicationUnl") > 0) Then
                    uli.CommunicationStatusUnlimitedText = offersControl.CurrentPageData.GetCustomString("CommunicationStatusUnlimitedText")
                Else
                    uli.CommunicationStatusLimitedText = offersControl.CurrentPageData.GetCustomString("CommunicationStatusLimitedText")
                End If

                'If (Not dr.IsNull("FirstMessageDate")) Then
                '    Dim _dateTime As DateTime = dr("FirstMessageDate")
                '    uli.FirstDateText = offersControl.CurrentPageData.GetCustomString("FirstDateText")
                '    uli.FirstDateText = uli.FirstDateText.Replace("###DATE###", _dateTime.ToString("dd/MM/yyyy"))
                'End If

                If (Not dr.IsNull("LastMessageDate")) Then
                    Dim _dateTime As DateTime = dr("LastMessageDate")
                    Dim dateDescrpt As String = AppUtils.GetMessageDateDescription(_dateTime, Me.globalStrings)

                    uli.LastDatingText = offersControl.CurrentPageData.GetCustomString("LastDatingText")
                    uli.LastDatingDateText = dateDescrpt ' _dateTime.ToString("dd/MM/yyyy")
                End If


                offersControl.UsersList.Add(uli)
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try

        Next
    End Sub



    Protected Function IsMessageHidden(DataItem As DataRowView) As String
        Dim class1 As String = ""
        Dim HasCommunication As Boolean = GetHasCommunicationDict(DataItem("ProfileID"))
        If (Not HasCommunication AndAlso DataItem("StatusID") = 0 AndAlso Me.IsMale) Then
            class1 = "closed"
        End If

        Return class1
    End Function

    Private Property HasCommunicationDict As Dictionary(Of Integer, Boolean)
    Protected Function GetHasCommunicationDict(otherProfileID As Integer, Optional force As Boolean = False) As Boolean
        Dim HasCommunication As Boolean

        If (HasCommunicationDict Is Nothing) Then HasCommunicationDict = New Dictionary(Of Integer, Boolean)
        If (Not HasCommunicationDict.ContainsKey(otherProfileID) OrElse force) Then

            HasCommunication = GetHasCommunication(otherProfileID)
            If (force) Then
                HasCommunicationDict(otherProfileID) = HasCommunication
            Else
                HasCommunicationDict.Add(otherProfileID, HasCommunication)
            End If

        ElseIf (HasCommunicationDict.ContainsKey(otherProfileID)) Then

            HasCommunication = HasCommunicationDict(otherProfileID)

        End If

        Return HasCommunication
    End Function


    Public Sub SetUI_SendingFreeMessage()
        lblMaleFirstMsgBottomPanelText2.Visible = False
        Dim cls As String = "emtpy-div"
        If (divMaleFirstMsgBottomPanelText2.Attributes("class") IsNot Nothing) Then
            divMaleFirstMsgBottomPanelText2.Attributes("class") = divMaleFirstMsgBottomPanelText2.Attributes("class") & " " & cls
        Else
            divMaleFirstMsgBottomPanelText2.Attributes("class") = cls
        End If

        FocusAndWriteMessages = True

        divSendWithCredits.Visible = False
        divSendAnotherMessage.Visible = True
    End Sub



    Public Sub SetUI_NoFreeMessageAllowed()
        divSendWithCredits.Visible = True
        divSendAnotherMessage.Visible = False
    End Sub


    Private __clsFreeMessagesHelper As CustomerFreeMessagesAvailableResult
    Private Function GetMembersFreeMessagesAvailableToSEND(manProfileId As Integer?, womanProfileId As Integer?, manCountry As String) As Integer
        Dim freeMessagesCount As Integer = 0
        If (__clsFreeMessagesHelper Is Nothing) Then

            If (ConfigurationManager.AppSettings("AllowFreeMessagesToSend") = "1" AndAlso
                freeMessagesHelper.CreditsPackagesBonusEnabled) Then
                Using cmdb As CMSDBDataContext = Me.GetCMSDBDataContext
                    __clsFreeMessagesHelper = cmdb.CustomerFreeMessagesAvailable(manProfileId, womanProfileId, manCountry).FirstOrDefault()
                End Using

                '__clsFreeMessagesHelper = clsFreeMessagesHelper.GetData(Me.MasterProfileId, toProfileID, Me.SessionVariables.MemberData.Country)
            End If
        End If

        If (__clsFreeMessagesHelper IsNot Nothing) Then
            If (Not __clsFreeMessagesHelper.IsProfilesSentCountLimit OrElse __clsFreeMessagesHelper.HasProfileConversation) Then
                freeMessagesCount = __clsFreeMessagesHelper.Credits_FreeMessagesSendToProfile - __clsFreeMessagesHelper.MessagesSentToProfile
            End If
        End If

        Return freeMessagesCount
    End Function


    Private __SubscriptionMessagesHelper As CustomerSubscriptionAvailableMessages2Result
    'Private __SubscriptionMessagesHelper As clsSubscriptionMessagesHelper
    Private Function GetSubsriptionAvailableMessages(manProfileId As Integer?,
                                                     womanProfileId As Integer?,
                                                     manCountry As String,
                                                     Optional dateFrom As Date? = Nothing,
                                                     Optional dateTo As Date? = Nothing) As Integer
        Dim subsriptionMessagesCount As Integer = 0

        If (__SubscriptionMessagesHelper Is Nothing) Then
            If (ConfigurationManager.AppSettings("AllowSubscription") = "1" AndAlso
                freeMessagesHelper.HasSubscription) Then

                Dim tmp As Integer = 0
                'If (ProfileCountry.MaxMessagesToProfilePerDay > 0) Then tmp = tmp + ProfileCountry.MaxMessagesToProfilePerDay
                'If (ProfileCountry.MaxMessagesToDifferentProfilesPerDay > 0) Then tmp = tmp + ProfileCountry.MaxMessagesToDifferentProfilesPerDay
                If (ProfileSettings.MaxMessagesToProfilePerDay > 0) Then tmp = tmp + ProfileSettings.MaxMessagesToProfilePerDay
                If (ProfileSettings.MaxMessagesToDifferentProfilesPerDay > 0) Then tmp = tmp + ProfileSettings.MaxMessagesToDifferentProfilesPerDay

                If (tmp > 0) Then
                    Using cmdb As CMSDBDataContext = Me.GetCMSDBDataContext
                        __SubscriptionMessagesHelper = cmdb.CustomerSubscriptionAvailableMessages2(manProfileId, womanProfileId, manCountry, dateFrom, dateTo).FirstOrDefault()
                    End Using

                    '__SubscriptionMessagesHelper = clsSubscriptionMessagesHelper.GetData(Me.MasterProfileId, fromProfileID, Me.SessionVariables.MemberData.Country)
                End If
            End If
        End If

        If (__SubscriptionMessagesHelper IsNot Nothing) Then
            If (Not __SubscriptionMessagesHelper.IsProfilesCountLimit OrElse __SubscriptionMessagesHelper.HasProfileConversationToday) Then
                subsriptionMessagesCount = __SubscriptionMessagesHelper.TotalMessagesAvailableForProfile
            End If
        End If
        Return subsriptionMessagesCount
    End Function


    Private Function PerformMessageSend(Subject As String, Body As String, ToProfileID As Integer, AlreadyUnlockedUnlimited As Boolean, ByRef result As PerformMessageSendResultEnum) As Long
        Dim NewMessageID As Long = 0
        result = PerformMessageSendResultEnum.success

        Dim myFreeMessages As Integer = 0
        Dim mySubscriptionMessages As Integer = 0
        Dim HasRequiredCredits_UNLOCK_MESSAGE_SEND As Boolean
        Dim messageReceived As EUS_Message = Nothing
        Dim isSubscriptionUsed As Boolean = False
        'Dim messageReceived As EUS_Message = clsUserDoes.SendMessage(Subject, Body, Me.MasterProfileId, ToProfileID,
        '                                                             (Me.IsFemale), 0,
        '                                                             MyBase.BlockMessagesToBeReceivedByOther,
        '                                                             False,
        '                                                             If(Me.IsFemale, 2, 1))

        'If (messageReceived.EUS_MessageID > 0) Then NewMessageID = messageReceived.EUS_MessageID

        Try
            'messageReceived.CopyMessageID = clsNullable.NullTo(messageReceived.CopyMessageID)
            If (Me.IsMale) Then

                HasRequiredCredits_UNLOCK_MESSAGE_SEND = HasRequiredCredits(UnlockType.UNLOCK_MESSAGE_SEND)
                If (Not HasRequiredCredits_UNLOCK_MESSAGE_SEND) Then

                    mySubscriptionMessages = GetSubsriptionAvailableMessages(Me.MasterProfileId, ToProfileID, Me.SessionVariables.MemberData.Country)
                    If (mySubscriptionMessages < 1) Then
                        myFreeMessages = Me.GetMembersFreeMessagesAvailableToSEND(Me.MasterProfileId, ToProfileID, Me.SessionVariables.MemberData.Country)
                    End If

                End If

                isSubscriptionUsed = (Not HasRequiredCredits_UNLOCK_MESSAGE_SEND AndAlso mySubscriptionMessages > 0)


                If (HasRequiredCredits_UNLOCK_MESSAGE_SEND OrElse
                    isSubscriptionUsed OrElse
                    myFreeMessages > 0) Then

                    messageReceived = clsUserDoes.SendMessage(Subject, Body, Me.MasterProfileId, ToProfileID,
                                                                     (Me.IsFemale), 0,
                                                                     MyBase.BlockMessagesToBeReceivedByOther,
                                                                     False,
                                                                     If(Me.IsFemale, 2, 1))

                    If (messageReceived.EUS_MessageID > 0) Then NewMessageID = messageReceived.EUS_MessageID
                    messageReceived.CopyMessageID = clsNullable.NullTo(messageReceived.CopyMessageID)


                End If


                If (isSubscriptionUsed) Then
                    freeMessagesHelper.MessageSentByMan(messageReceived.EUS_MessageID, messageReceived.CopyMessageID,
                                                            ToProfileID, Me.MasterProfileId,
                                                            clsCurrentContext.GetCurrentIP(), clsCurrentContext.GetCookieID(), FreeMessagesArchiveEnum.SubscriptionMessage)

                ElseIf (myFreeMessages > 0) Then
                    freeMessagesHelper.MessageSentByMan(messageReceived.EUS_MessageID, messageReceived.CopyMessageID,
                                                            ToProfileID, Me.MasterProfileId,
                                                            clsCurrentContext.GetCurrentIP(), clsCurrentContext.GetCookieID(), FreeMessagesArchiveEnum.FreeMessage)
                End If



            Else

                messageReceived = clsUserDoes.SendMessage(Subject, Body, Me.MasterProfileId, ToProfileID,
                                                             (Me.IsFemale), 0,
                                                             MyBase.BlockMessagesToBeReceivedByOther,
                                                             False,
                                                             If(Me.IsFemale, 2, 1))

                If (messageReceived.EUS_MessageID > 0) Then NewMessageID = messageReceived.EUS_MessageID
                messageReceived.CopyMessageID = clsNullable.NullTo(messageReceived.CopyMessageID)


                If (messageReceived.CopyMessageID > 0) Then
                    freeMessagesHelper.MessageSentToMan(messageReceived.CopyMessageID, messageReceived.EUS_MessageID,
                                                        Me.MasterProfileId, ToProfileID,
                                                        clsCurrentContext.GetCurrentIP(), clsCurrentContext.GetCookieID())
                Else
                    freeMessagesHelper.MessageSentToMan(messageReceived.EUS_MessageID, messageReceived.CopyMessageID,
                                                        Me.MasterProfileId, ToProfileID,
                                                        clsCurrentContext.GetCurrentIP(), clsCurrentContext.GetCookieID())
                End If
            End If
        Catch ex As Exception
            WebErrorSendEmail(ex, "update FreeMessageArchive")
        End Try


        ' send email message to administrator
        If (ToProfileID = 1) Then
            Try
                Dim newSubject As String = "Message sent to administrator of Goomena.com"
                Dim newBody As String =
                    "<br>" &
                    "Message sent to administrator of Goomena.com" & "<br>" & _
                    "<hr>" & _
                    "Login Name : " & Me.SessionVariables.MemberData.LoginName & "<br>" & _
                    "E-mail : " & Me.SessionVariables.MemberData.Email & "<br>" & _
                    "<hr>" & _
                    "Subject : " & Subject & "<br>" & _
                    "Body : " & Body

                clsMyMail.SendMail(ConfigurationManager.AppSettings("gToEmail"), newSubject, newBody, True)
            Catch
            End Try
        End If



        If (Me.IsMale) Then
            If (Not AlreadyUnlockedUnlimited) Then
                If (SendOnce) Then

                    If (HasRequiredCredits_UNLOCK_MESSAGE_SEND OrElse isSubscriptionUsed OrElse (myFreeMessages > 0)) Then

                        Dim _REF_CRD2EURO_Rate As Double = 0

                        If (HasRequiredCredits_UNLOCK_MESSAGE_SEND) Then
                            _REF_CRD2EURO_Rate = DataHelpers.GetEUS_Profiles_REF_CRD2EURO_Rate(messageReceived.FromProfileID)
                            'ElseIf (isSubscriptionUsed) Then
                            '    _REF_CRD2EURO_Rate = SYS_CountriesGEOHelper.SYS_CountriesGEO_GetSubscriptionREF_CRD2EURO_Rate(ProfileCountry)
                        End If

                        Dim customerCreditsId As Long

                        If (HasRequiredCredits_UNLOCK_MESSAGE_SEND) Then

                            customerCreditsId = clsUserDoes.UnlockMessageOnce(ToProfileID,
                                                                            Me.MasterProfileId,
                                                                            ProfileHelper.Config_UNLOCK_MESSAGE_SEND_CREDITS,
                                                                            UnlockType.UNLOCK_MESSAGE_SEND,
                                                                            messageReceived.EUS_MessageID,
                                                                            _REF_CRD2EURO_Rate,
                                                                            False)

                            Try
                                ' write commissition for recipient referrer
                                Dim costPerMessage As Double = ProfileHelper.Config_UNLOCK_MESSAGE_SEND_CREDITS * _REF_CRD2EURO_Rate
                                clsReferrer.SetCommissionForMessage(messageReceived.EUS_MessageID,
                                                                    messageReceived.ToProfileID,
                                                                    costPerMessage,
                                                                    ProfileHelper.Config_UNLOCK_MESSAGE_SEND_CREDITS,
                                                                    UnlockType.UNLOCK_MESSAGE_SEND,
                                                                    customerCreditsId)
                            Catch ex As Exception
                                WebErrorSendEmail(ex, "")
                            End Try


                        ElseIf isSubscriptionUsed Then
                            Dim SubscriptionREF_CRD2EURO_Rate As Double = SYS_CountriesGEOHelper.SYS_CountriesGEO_GetSubscriptionREF_CRD2EURO_Rate(ProfileCountry)

                            Dim isSet As Boolean = False
                            Dim setCommission As Boolean = False

                            Dim costPerMessage As Double = 0
                            Dim subscriptionMessageRate As Double = 0
                            If (freeMessagesHelper.SubscriptionMessageReferralAmount IsNot Nothing) Then
                                costPerMessage = freeMessagesHelper.SubscriptionMessageReferralAmount
                                subscriptionMessageRate = SubscriptionREF_CRD2EURO_Rate
                            Else
                                costPerMessage = ProfileHelper.Config_UNLOCK_MESSAGE_SEND_CREDITS * _REF_CRD2EURO_Rate
                                subscriptionMessageRate = _REF_CRD2EURO_Rate
                            End If

                            Try
                                customerCreditsId = clsUserDoes.UnlockMessageOnce(ToProfileID,
                                                                                Me.MasterProfileId,
                                                                                ProfileHelper.Config_UNLOCK_MESSAGE_SEND_CREDITS,
                                                                                UnlockType.UNLOCK_MESSAGE_SEND,
                                                                                messageReceived.EUS_MessageID,
                                                                                subscriptionMessageRate,
                                                                                True)
                            Catch ex As Exception
                                WebErrorSendEmail(ex, "mySubscriptionMessages-->clsUserDoes.UnlockMessageOnce")
                            End Try


                            Try
                                ' write commissition for receiving referrer from subscription message
                                If (freeMessagesHelper.SubscriptionMessagesCountReferralPayed = -1) Then
                                    setCommission = True
                                ElseIf (freeMessagesHelper.SubscriptionMessagesCountReferralPayed > 0) Then
                                    If (__SubscriptionMessagesHelper IsNot Nothing AndAlso
                                        __SubscriptionMessagesHelper.CommisionSetMessagesCountToday < freeMessagesHelper.SubscriptionMessagesCountReferralPayed) Then
                                        setCommission = True
                                    End If
                                End If

                                If (setCommission) Then
                                    Try
                                        isSet = clsReferrer.SetCommissionForMessage_Subscription(Me.MasterProfileId, costPerMessage, messageReceived.EUS_MessageID, messageReceived.ToProfileID, customerCreditsId, UnlockType.UNLOCK_MESSAGE_SEND)
                                    Catch ex As Exception
                                        WebErrorSendEmail(ex, "")
                                    End Try

                                    'isSet = clsReferrer.SetCommissionForMessage(messageReceived.EUS_MessageID,
                                    '                                            messageReceived.ToProfileID,
                                    '                                            costPerMessage,
                                    '                                            ProfileHelper.Config_UNLOCK_MESSAGE_SEND_CREDITS,
                                    '                                            UnlockType.UNLOCK_MESSAGE_SEND,
                                    '                                            customerCreditsId)
                                End If
                            Catch ex As Exception
                                WebErrorSendEmail(ex, "mySubscriptionMessages-->clsReferrer.SetCommissionForMessage")
                            End Try


                            Try
                                If (isSet) Then
                                    freeMessagesHelper.MessageSetCommission(messageReceived.EUS_MessageID, messageReceived.CopyMessageID, Me.MasterProfileId, ToProfileID)
                                End If
                            Catch ex As Exception
                                WebErrorSendEmail(ex, "mySubscriptionMessages-->freeMessagesHelper.MessageSetCommission")
                            End Try

                            __SubscriptionMessagesHelper = Nothing

                        ElseIf (myFreeMessages > 0) Then
                            Dim isSet As Boolean = False
                            Dim setCommission As Boolean = False

                            Dim costPerMessage As Double = 0
                            Dim freeMessageRate As Double = 0
                            If (freeMessagesHelper.CreditsFreeMessageReferralAmount IsNot Nothing) Then
                                costPerMessage = freeMessagesHelper.CreditsFreeMessageReferralAmount
                                freeMessageRate = freeMessagesHelper.CreditsFreeMessageReferralAmount / ProfileHelper.Config_UNLOCK_MESSAGE_SEND_CREDITS
                            Else
                                costPerMessage = ProfileHelper.Config_UNLOCK_MESSAGE_SEND_CREDITS * _REF_CRD2EURO_Rate
                                freeMessageRate = _REF_CRD2EURO_Rate
                            End If

                            Try
                                customerCreditsId = clsUserDoes.UnlockMessageOnce(ToProfileID,
                                                                      Me.MasterProfileId,
                                                                      ProfileHelper.Config_UNLOCK_MESSAGE_FREE_CREDITS,
                                                                      UnlockType.UNLOCK_MESSAGE_SEND_FREE,
                                                                      messageReceived.EUS_MessageID,
                                                                      freeMessageRate,
                                                                      False)
                            Catch ex As Exception
                                WebErrorSendEmail(ex, "myFreeMessages-->clsUserDoes.UnlockMessageOnce")
                            End Try


                            Try
                                ' write commissition for receiving referrer from free message
                                If (freeMessagesHelper.CreditsFreeMessagesCountReferralPayed = -1) Then
                                    setCommission = True
                                ElseIf (freeMessagesHelper.CreditsFreeMessagesCountReferralPayed > 0) Then
                                    If (__clsFreeMessagesHelper IsNot Nothing AndAlso
                                        __clsFreeMessagesHelper.CommisionSetMessagesCount < freeMessagesHelper.CreditsFreeMessagesCountReferralPayed) Then
                                        setCommission = True
                                    End If
                                End If

                                If (setCommission) Then

                                    isSet = clsReferrer.SetCommissionForMessage(messageReceived.EUS_MessageID,
                                                                                messageReceived.ToProfileID,
                                                                                costPerMessage,
                                                                                ProfileHelper.Config_UNLOCK_MESSAGE_SEND_CREDITS,
                                                                                UnlockType.UNLOCK_MESSAGE_SEND,
                                                                                customerCreditsId)
                                End If
                            Catch ex As Exception
                                WebErrorSendEmail(ex, "myFreeMessages-->clsReferrer.SetCommissionForMessage")
                            End Try


                            Try
                                If (isSet) Then
                                    freeMessagesHelper.MessageSetCommission(messageReceived.EUS_MessageID, messageReceived.CopyMessageID, Me.MasterProfileId, ToProfileID)
                                End If
                            Catch ex As Exception
                                WebErrorSendEmail(ex, "myFreeMessages-->freeMessagesHelper.MessageSetCommission")
                            End Try

                            __clsFreeMessagesHelper = Nothing

                        End If



                        '''''''''''''
                        'reset session property to update available credits, when master.page loads
                        Session("CustomerAvailableCredits") = Nothing
                        '''''''''''''



                    ElseIf (False AndAlso MaySendFreeMessage) Then

                        Try
                            Dim ctr As SYS_CountriesGEO = SYS_CountriesGEOHelper.SYS_CountriesGEO_GetForCountry(Me.SessionVariables.MemberData.Country)
                            ' write commissition for recipient referrer
                            Dim costPerMessage As Double = clsNullable.NullTo(ctr.CreditsFreeMessageReferralAmount)
                            Dim _REF_CRD2EURO_Rate As Double? = costPerMessage / ProfileHelper.Config_UNLOCK_MESSAGE_SEND_CREDITS

                            Dim customerCreditsId As Long = clsUserDoes.UnlockMessageOnce(ToProfileID,
                                                                                          Me.MasterProfileId,
                                                                                          ProfileHelper.Config_UNLOCK_MESSAGE_FREE_CREDITS,
                                                                                          UnlockType.UNLOCK_MESSAGE_SEND_FREE,
                                                                                          messageReceived.EUS_MessageID,
                                                                                          _REF_CRD2EURO_Rate,
                                                                                          False)

                            clsReferrer.SetCommissionForMessage(messageReceived.EUS_MessageID,
                                                                messageReceived.ToProfileID,
                                                                costPerMessage,
                                                                ProfileHelper.Config_UNLOCK_MESSAGE_FREE_CREDITS,
                                                                UnlockType.UNLOCK_MESSAGE_SEND_FREE,
                                                                customerCreditsId)


                        Catch ex As Exception
                            WebErrorSendEmail(ex, "")
                        End Try


                    Else
                        result = PerformMessageSendResultEnum.fail_CreditsUNLOCK_MESSAGE_SEND
                    End If

                ElseIf (SendUnlimited) Then

                    Dim HasRequiredCredits_UNLOCK_CONVERSATION As Boolean = HasRequiredCredits(UnlockType.UNLOCK_CONVERSATION)
                    'Dim HasRequiredCredits_To_UnlockConversation As Boolean = clsUserDoes.HasRequiredCredits(Me.MasterProfileId, UnlockType.UNLOCK_CONVERSATION)
                    If (HasRequiredCredits_UNLOCK_CONVERSATION) Then
                        Dim customerCreditsId As Long = clsUserDoes.UnlockMessageUnlimited(ToProfileID, Me.MasterProfileId, Me.OfferId, False)

                        '''''''''''''
                        'reset session property to update available credits, when master.page loads
                        Session("CustomerAvailableCredits") = Nothing
                        '''''''''''''

                        Try
                            ' write commissition for recipient referrer
                            Dim _REF_CRD2EURO_Rate As Double = DataHelpers.GetEUS_Profiles_REF_CRD2EURO_Rate(messageReceived.FromProfileID)
                            Dim costPerMessage As Double = ProfileHelper.Config_UNLOCK_CONVERSATION_CREDITS * _REF_CRD2EURO_Rate
                            clsReferrer.SetCommissionForMessage(messageReceived.EUS_MessageID,
                                                                messageReceived.ToProfileID,
                                                                costPerMessage,
                                                                ProfileHelper.Config_UNLOCK_CONVERSATION_CREDITS,
                                                                UnlockType.UNLOCK_CONVERSATION,
                                                                customerCreditsId)
                        Catch ex As Exception
                            WebErrorSendEmail(ex, "")
                        End Try

                    Else
                        result = PerformMessageSendResultEnum.fail_CreditsUNLOCK_CONVERSATION
                    End If

                End If

            End If
        End If
        Return NewMessageID
    End Function


    Private Sub PerformMessageUnlock()
        Try
            '' unlock message
            Dim gotoBilling = False
            '       Dim ReadOnce As Boolean = (Request.QueryString("read") = "once")
            '   Dim ReadUnlimited As Boolean = (Request.QueryString("read") = "unl") 'AndAlso ModGlobals.AllowUnlimited


            Dim unmsgId As Long = MyBase.UnlimitedMsgID
            Dim message As EUS_Message = clsUserDoes.GetMessage(unmsgId)
            If (message Is Nothing) Then Throw New MessageNotFoundException()

            If (message.FromProfileID <> Me.MasterProfileId) Then
                Me.UserIdInView = message.FromProfileID
            Else
                Me.UserIdInView = message.ToProfileID
            End If

            '' just in case if some wants to unlock message directly, calling conversation.aspx
            Dim result As String = JSON.MessageUnlock2(Me.UserIdInView, Request.QueryString("read"), Request.QueryString("unmsg"), Request.QueryString("onemsg"), Request.QueryString("free"), Request.QueryString("subscription"))
            If (result.StartsWith("{""redirect""")) Then
                gotoBilling = True
            End If

            ''Dim HasCommunication As Boolean = GetHasCommunicationDict(Me.UserIdInView)
            ''If (Not HasCommunication) Then

            ''    ' here read once specific message, and pay credits only for that
            ''    If (Request.QueryString("onemsg") = "1" AndAlso message.StatusID = 0) Then

            ''        Try
            ''            gotoBilling = True
            ''            Dim HasRequiredCredits_UNLOCK_MESSAGE_READ As Boolean = HasRequiredCredits(UnlockType.UNLOCK_MESSAGE_READ)
            ''            'Dim HasRequiredCredits_To_UnlockMessage As Boolean = clsUserDoes.HasRequiredCredits(Me.MasterProfileId, UnlockType.UNLOCK_MESSAGE_READ)
            ''            If (HasRequiredCredits_UNLOCK_MESSAGE_READ) Then


            ''                Dim _REF_CRD2EURO_Rate As Double = DataHelpers.GetEUS_Profiles_REF_CRD2EURO_Rate(Me.MasterProfileId)

            ''                ' get current unread mesages from UserIdInView to MasterProfileId
            ''                Dim customerCreditsId As Long = clsUserDoes.UnlockMessageOnce(Me.UserIdInView,
            ''                                                                              Me.MasterProfileId,
            ''                                                                              ProfileHelper.Config_UNLOCK_MESSAGE_READ_CREDITS,
            ''                                                                              UnlockType.UNLOCK_MESSAGE_READ,
            ''                                                                              message.EUS_MessageID,
            ''                                                                              _REF_CRD2EURO_Rate)
            ''                clsUserDoes.MarkMessageRead(message.EUS_MessageID)
            ''                clsUserDoes.MakeNewDate(Me.MasterProfileId, Me.UserIdInView, Me.OfferId, Me.IsMale)
            ''                ' MakeNewDate(Me.MasterProfileId, Me.UserIdInView)

            ''                '''''''''''''
            ''                'reset session property to update available credits, when master.page loads
            ''                Session("CustomerAvailableCredits") = Nothing
            ''                '''''''''''''

            ''                Try

            ''                    ' write commissition for sender referrer
            ''                    Dim cost As Double = ProfileHelper.Config_UNLOCK_MESSAGE_READ_CREDITS * _REF_CRD2EURO_Rate
            ''                    clsReferrer.SetCommissionForMessage(message.CopyMessageID,
            ''                                                        message.FromProfileID,
            ''                                                        cost,
            ''                                                        ProfileHelper.Config_UNLOCK_MESSAGE_READ_CREDITS,
            ''                                                        UnlockType.UNLOCK_MESSAGE_READ,
            ''                                                        customerCreditsId)

            ''                Catch ex As Exception
            ''                    WebErrorSendEmail(ex, "")
            ''                End Try

            ''                gotoBilling = False
            ''            End If

            ''        Catch ex As Exception
            ''            WebErrorMessageBox(Me, ex, "")
            ''        End Try

            ''    ElseIf (message.StatusID = 0) Then

            ''        ' check for credits amount required to unlock
            ''        If (ReadOnce) Then
            ''            Try
            ''                gotoBilling = True
            ''                Dim HasRequiredCredits_To_UnlockMessage As Boolean = HasRequiredCredits(UnlockType.UNLOCK_MESSAGE_READ)
            ''                'Dim HasRequiredCredits_To_UnlockMessage As Boolean = clsUserDoes.HasRequiredCredits(Me.MasterProfileId, UnlockType.UNLOCK_MESSAGE_READ)
            ''                If (HasRequiredCredits_To_UnlockMessage) Then
            ''                    ' get all unread mesages from UserIdInView to MasterProfileId
            ''                    Dim messages As List(Of EUS_Message) = clsUserDoes.GetAllUnreadMessages(Me.UserIdInView, Me.MasterProfileId)
            ''                    If (messages.Count > 0) Then

            ''                        Dim _REF_CRD2EURO_Rate As Double = DataHelpers.GetEUS_Profiles_REF_CRD2EURO_Rate(Me.MasterProfileId)
            ''                        Dim costPerMessage As Double = ProfileHelper.Config_UNLOCK_MESSAGE_READ_CREDITS * _REF_CRD2EURO_Rate

            ''                        Dim customerCreditsId As Long = clsUserDoes.UnlockMessageOnce(Me.UserIdInView,
            ''                                                                                      Me.MasterProfileId,
            ''                                                                                      ProfileHelper.Config_UNLOCK_MESSAGE_READ_CREDITS * messages.Count,
            ''                                                                                      UnlockType.UNLOCK_MESSAGE_READ,
            ''                                                                                      0,
            ''                                                                                      _REF_CRD2EURO_Rate)
            ''                        '''''''''''''
            ''                        'reset session property to update available credits, when master.page loads
            ''                        Session("CustomerAvailableCredits") = Nothing
            ''                        '''''''''''''

            ''                        clsUserDoes.MakeNewDate(Me.MasterProfileId, Me.UserIdInView, Me.OfferId, Me.IsMale)
            ''                        'MakeNewDate(Me.MasterProfileId, Me.UserIdInView)

            ''                        Dim cnt As Integer
            ''                        For cnt = 0 To messages.Count - 1
            ''                            clsUserDoes.MarkMessageRead(messages(cnt).EUS_MessageID)

            ''                            Try

            ''                                ' write commissition for recipient referrer
            ''                                clsReferrer.SetCommissionForMessage(messages(cnt).EUS_MessageID,
            ''                                                                    Me.UserIdInView,
            ''                                                                    costPerMessage,
            ''                                                                    ProfileHelper.Config_UNLOCK_MESSAGE_READ_CREDITS,
            ''                                                                    UnlockType.UNLOCK_MESSAGE_READ,
            ''                                                                    customerCreditsId)
            ''                            Catch ex As Exception
            ''                                WebErrorSendEmail(ex, "")
            ''                            End Try
            ''                        Next
            ''                    End If
            ''                    gotoBilling = False
            ''                End If
            ''            Catch ex As Exception
            ''                WebErrorMessageBox(Me, ex, "")
            ''            End Try
            ''        End If

            ''        If (ReadUnlimited) Then
            ''            Try
            ''                gotoBilling = True
            ''                Dim HasRequiredCredits_To_UnlockConversation As Boolean = HasRequiredCredits(UnlockType.UNLOCK_CONVERSATION)
            ''                'Dim HasRequiredCredits_To_UnlockConversation As Boolean = clsUserDoes.HasRequiredCredits(Me.MasterProfileId, UnlockType.UNLOCK_CONVERSATION)
            ''                If (HasRequiredCredits_To_UnlockConversation) Then
            ''                    Dim customerCreditsId As Long = clsUserDoes.UnlockMessageConversation(unmsgId,
            ''                                                          ProfileHelper.Config_UNLOCK_CONVERSATION_CREDITS,
            ''                                                          Me.MasterProfileId,
            ''                                                          clsUserDoes.GetRequiredCredits(UnlockType.UNLOCK_CONVERSATION).EUS_CreditsTypeID)

            ''                    'MakeNewDate(Me.MasterProfileId, Me.UserIdInView)
            ''                    clsUserDoes.MakeNewDate(Me.MasterProfileId, Me.UserIdInView, Me.OfferId, Me.IsMale)
            ''                    clsUserDoes.MarkMessageRead(unmsgId)

            ''                    Try
            ''                        ' write commissition for recipient referrer
            ''                        Dim _REF_CRD2EURO_Rate As Double = DataHelpers.GetEUS_Profiles_REF_CRD2EURO_Rate(Me.MasterProfileId)
            ''                        Dim cost As Double = ProfileHelper.Config_UNLOCK_CONVERSATION_CREDITS * _REF_CRD2EURO_Rate
            ''                        clsReferrer.SetCommissionForMessage(unmsgId,
            ''                                                            Me.UserIdInView,
            ''                                                            cost,
            ''                                                            ProfileHelper.Config_UNLOCK_CONVERSATION_CREDITS,
            ''                                                            UnlockType.UNLOCK_CONVERSATION,
            ''                                                            customerCreditsId)
            ''                    Catch ex As Exception
            ''                        WebErrorSendEmail(ex, "")
            ''                    End Try

            ''                    gotoBilling = False
            ''                End If
            ''            Catch ex As Exception
            ''                WebErrorMessageBox(Me, ex, "")
            ''            End Try
            ''        End If


            ''    End If


            ''End If


            If (gotoBilling) Then
                SessionVariables.UserUnlockInfo.OfferId = 0
                SessionVariables.UserUnlockInfo.ReturnUrl = Request.Url.ToString()

                If (Page.IsCallback) Then

                Else
                    Dim billingurl As String = ResolveUrl("~/Members/SelectProduct.aspx?returnurl=" & HttpUtility.UrlEncode(Request.Url.ToString()))

                    Response.Redirect(billingurl, False)
                    HttpContext.Current.ApplicationInstance.CompleteRequest()
                End If
            Else
                'LoadMessagesList()
                LoadUsersConversation(unmsgId)

                ' refresh credits on page left part
                RefreshProfilePreviewControl(Me.Page)
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Enum ConversationErrorEnum
        none = 0
        ErrorSendMessageToMySelf = 1
        ErrorMemberDoesNotExistCannotSendMessage = 2
        FemaleMemberCannotSendToFemale = 3
        Error_Sending_Receiver_Disabled_Messages_From_Diff_Country = 4
        ErrSendMessageOnceCreditsNotAvailable = 5
        ErrSendMessageUnlimitedCreditsNotAvailable = 6
        ReferrerStopSendingMessageEnum_RecipientHasNoPhoto = 7
        ReferrerStopSendingMessageEnum_MaxAllowedMessagesSent = 8
        FemaleMemberCannotSendToFemale_SendingMessage = 9
        ErrorEmptyMessage = 10
    End Enum

    Private Sub ShowError_vwEmpty(reason As ConversationErrorEnum)

        If (reason = ConversationErrorEnum.ErrorSendMessageToMySelf) Then

            divActionError.Visible = True
            lnkAlternativeAction.Text = CurrentPageData.GetCustomString("lnkAlternativeAction_Back")
            If (Not String.IsNullOrEmpty(Me.BackUrl)) Then
                lnkAlternativeAction.NavigateUrl = Me.BackUrl
            Else
                lnkAlternativeAction.NavigateUrl = "~/Members/"
            End If
            lblActionError.Text = CurrentPageData.GetCustomString("ErrorSendMessageToMySelf")
            mvMessages.SetActiveView(vwEmpty)

        ElseIf (reason = ConversationErrorEnum.ErrorMemberDoesNotExistCannotSendMessage) Then

            divActionError.Visible = True
            lnkAlternativeAction.Text = CurrentPageData.GetCustomString("lnkAlternativeAction_Search")
            lnkAlternativeAction.NavigateUrl = "~/Members/Search.aspx"
            lblActionError.Text = CurrentPageData.GetCustomString("ErrorMemberDoesNotExistCannotSendMessage")
            mvMessages.SetActiveView(vwEmpty)

        ElseIf (reason = ConversationErrorEnum.FemaleMemberCannotSendToFemale) Then
            divActionError.Visible = True
            lnkAlternativeAction.Text = CurrentPageData.GetCustomString("lnkAlternativeAction_Search")
            lnkAlternativeAction.NavigateUrl = "~/Members/Search.aspx"
            lblActionError.Text = CurrentPageData.GetCustomString("Error.FemaleMemberCannotSendToFemale")
            mvMessages.SetActiveView(vwEmpty)

        End If

    End Sub

    Private Sub ShowErrorPopup(reason As ConversationErrorEnum)

        If (reason = ConversationErrorEnum.Error_Sending_Receiver_Disabled_Messages_From_Diff_Country) Then

            Dim popupMessage As String = WhatIsIt.OnMoreInfoClickFunc(ResolveUrl("~/Members/InfoWin.aspx?info=Error_Sending_Receiver_Disabled_Messages_From_Diff_Country&popup=popupWhatIs"), "Error! Can't send", 650, 350)
            popupMessage = "function fnError_Sending_Receiver_DoNot_Have_Photo(){" & popupMessage & "};"
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Error_Sending_Receiver_DoNot_Have_Photo", popupMessage, True)

        ElseIf (reason = ConversationErrorEnum.ReferrerStopSendingMessageEnum_RecipientHasNoPhoto) Then

            Dim popupMessage As String = WhatIsIt.OnMoreInfoClickFunc(ResolveUrl("~/Members/InfoWin.aspx?info=Error_Sending_Receiver_DoNot_Have_Photo&popup=popupWhatIs"), "Error! Can't send", 650, 350)
            popupMessage = "function fnError_Sending_Receiver_DoNot_Have_Photo(){" & popupMessage & "};"
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Error_Sending_Receiver_DoNot_Have_Photo", popupMessage, True)

        ElseIf (reason = ConversationErrorEnum.ReferrerStopSendingMessageEnum_MaxAllowedMessagesSent) Then

            Dim popupMessage As String = WhatIsIt.OnMoreInfoClickFunc(ResolveUrl("~/Members/InfoWin.aspx?info=Error_Sending_MaxAllowedMessagesSent&popup=popupWhatIs"), "Error! Can't send", 650, 350)
            popupMessage = "function fnError_Sending_Receiver_DoNot_Have_Photo(){" & popupMessage & "};"
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Error_Sending_Receiver_DoNot_Have_Photo", popupMessage, True)

        End If

    End Sub

    Private Sub ShowErrorPanel(reason As ConversationErrorEnum)

        If (reason = ConversationErrorEnum.ErrSendMessageOnceCreditsNotAvailable) Then

            lnkSendMessageAltAction.Text = CurrentPageData.GetCustomString("lnkErrSendMessageCreditsNotAvailable")
            lblSendMessageErr.Text = CurrentPageData.GetCustomString("ErrSendMessageOnceCreditsNotAvailable")
            lnkSendMessageAltAction.Visible = False

        ElseIf (reason = ConversationErrorEnum.ErrSendMessageUnlimitedCreditsNotAvailable) Then

            lnkSendMessageAltAction.Text = CurrentPageData.GetCustomString("lnkErrSendMessageCreditsNotAvailable")
            lblSendMessageErr.Text = CurrentPageData.GetCustomString("ErrSendMessageUnlimitedCreditsNotAvailable")
            lnkSendMessageAltAction.Visible = True

        End If

        If (reason = ConversationErrorEnum.ErrSendMessageOnceCreditsNotAvailable OrElse
            reason = ConversationErrorEnum.ErrSendMessageUnlimitedCreditsNotAvailable) Then
            If (pnlSendMessageErr.Visible) Then
                lnkSendMessageAltAction.Text = ReplaceCommonTookens(lnkSendMessageAltAction.Text, memberReceivingMessage.LoginName)
                lblSendMessageErr.Text = ReplaceCommonTookens(lblSendMessageErr.Text, memberReceivingMessage.LoginName)
                lblSendMessageErr.Text = lblSendMessageErr.Text.Replace("[GO_TO_PRODUCTS]", ResolveUrl("~/Members/SelectProduct.aspx"))
            End If
        End If


        If (reason = ConversationErrorEnum.FemaleMemberCannotSendToFemale_SendingMessage) Then

            ScrollToElem = pnlWriteMsgErr.ClientID
            pnlWriteMsgErr.Visible = True
            lblWriteMsgErr.Text = Me.CurrentPageData.GetCustomString("Error.FemaleMemberCannotSendToFemale")

        ElseIf (reason = ConversationErrorEnum.ErrorEmptyMessage) Then

            ScrollToElem = pnlWriteMsgErr.ClientID
            pnlWriteMsgErr.Visible = True
            lblWriteMsgErr.Text = Me.CurrentPageData.GetCustomString("ErrorEmptyMessage")

        End If

    End Sub


End Class



#Region "Comments"

'Private Sub MaleSendFirstMsg()
'    first_message_info.Visible = True
'    first_message_info_bottom.Visible = True
'    updCnv.Visible = False
'    updOffers.Visible = False
'    write_message_wrap.Visible = True
'    send_buttons.Visible = False
'    write_message_wrap.Attributes.CssStyle.Add("display", "block")

'    lnkFemaleFirstMsg.NavigateUrl = ProfileHelper.GetProfileNavigateUrl(CurrentLoginName)
'    lnkFemaleFirstMsgLogin.NavigateUrl = ProfileHelper.GetProfileNavigateUrl(CurrentLoginName)
'    lnkFemaleFirstMsgLogin.Text = CurrentLoginName
'    lblMaleFirstMsgTopPanelText.Text = lblMaleFirstMsgTopPanelText.Text.Replace("[LOGIN-NAME]", CurrentLoginName)

'    Dim fileName As String = ""
'    Dim dr As DSMembers.EUS_CustomerPhotosRow = DataHelpers.GetProfilesDefaultPhoto(Me.UserIdInView)
'    If (dr IsNot Nothing) Then fileName = dr.FileName

'    Dim genderId As Integer = 2
'    If (memberReceivingMessage IsNot Nothing) Then
'        genderId = memberReceivingMessage.GenderId
'    Else
'        genderId = DataHelpers.GetEUS_Profile_GenderId_ByProfileID(CMSDBDataContext, Me.UserIdInView)
'    End If
'    imgFemaleFirstMsg.ImageUrl = ProfileHelper.GetProfileImageURL(Me.UserIdInView, fileName, genderId, True, Me.IsHTTPS, PhotoSize.D150)


'End Sub


'Protected Sub sdsConversations_Selecting(sender As Object, e As System.Web.UI.WebControls.SqlDataSourceSelectingEventArgs) Handles sdsConversations.Selecting
'    For Each prm As SqlClient.SqlParameter In e.Command.Parameters
'        If (prm.ParameterName = "@messageID") Then
'            prm.Value = Me.MessageIdInView
'        ElseIf (prm.ParameterName = "@ownerProfileID") Then
'            prm.Value = Me.MasterProfileId
'        ElseIf (prm.ParameterName = "@rowNumberMin") Then
'            '    prm.Value = Me.MasterProfileId
'        ElseIf (prm.ParameterName = "@rowNumberMax") Then
'            ' prm.Value = Me.MasterProfileId
'        End If
'    Next
'End Sub

'Protected Sub dvConversation_DataBound(sender As Object, e As EventArgs) Handles dvConversation.DataBound
'    SetConversationFields()
'End Sub


'Private dvConversationData As List(Of DataRowView)
'Protected Sub dvConversation_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.ListViewItemEventArgs) Handles dvConversation.ItemDataBound
'    If (dvConversationData Is Nothing) Then dvConversationData = New List(Of DataRowView)
'    dvConversationData.Add(e.Item.DataItem)
'End Sub



'Protected Sub dvConversation_PageIndexChanging(source As Object, e As DevExpress.Web.ASPxDataView.DataViewPageEventArgs) Handles dvConversation.PageIndexChanging
'    'SetConversationFields()
'End Sub


'Protected Sub dvConversation_PageIndexChanged(sender As Object, e As EventArgs) Handles dvConversation.PageIndexChanged
'    Try
'        LoadUsersConversation(Me.MessageIdInView)
'    Catch ex As Exception
'        WebErrorMessageBox(Me, ex, "")
'    End Try
'End Sub

'Protected Sub dvConversation_ItemCommand(source As Object, e As System.Web.UI.WebControls.ListViewCommandEventArgs) Handles dvConversation.ItemCommand
'    Try
'        ExecCmd(e.CommandName, e.CommandArgument)
'    Catch ex As Exception
'        WebErrorMessageBox(Me, ex, "")
'    End Try
'End Sub



'Protected Sub btnSendMessage_Click(sender As Object, e As EventArgs) Handles btnSendMessage.Click
'    Try
'        SendMessageToUser(txtMessageSubject.Text, txtMessageText.Text, Me.UserIdInView)
'        txtMessageSubject.Text = ""
'        txtMessageText.Text = ""

'    Catch ex As Exception
'        WebErrorMessageBox(Me, ex, "")
'    End Try
'End Sub



#End Region


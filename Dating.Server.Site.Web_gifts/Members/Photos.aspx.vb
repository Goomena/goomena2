﻿Imports DevExpress.Web.ASPxUploadControl
Imports System.IO
Imports System.Drawing
Imports Dating.Server.Core.DLL

Public Class Photos
    Inherits BasePage

    Const __TESTING__ As Boolean = False


    'Dim _pageData As clsPageData
    'Protected ReadOnly Property CurrentPageData As clsPageData
    '    Get
    '        If (_pageData Is Nothing) Then _pageData = New clsPageData(Context)
    '        Return _pageData
    '    End Get
    'End Property




    Protected Overrides Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        SetMasterPage(CurrentPageData)

        Try
            If clsCurrentContext.VerifyLogin() = True Then
                Me.MasterPageFile = "~/Members/Members2.Master"
            Else
                clsCurrentContext.ClearSession(Session)
                If (Not Me.IsCallback) Then
                    Response.Redirect(ResolveUrl("~/Login.aspx"), False)
                End If
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            If (clsCurrentContext.VerifyLogin() = False) Then
                FormsAuthentication.SignOut()
                Response.Redirect(Page.ResolveUrl("~/Login.aspx"), False)
                HttpContext.Current.ApplicationInstance.CompleteRequest()
            End If


            If (__TESTING__) Then
                If (Not Page.IsPostBack AndAlso Session("ProfileID") = 0) Then
                    Dim DataRecordLoginReturn As Core.DLL.clsDataRecordLoginMemberReturn = gLogin.PerfomSiteLogin("slavva", "123456!", True)

                    Dim sesVars As clsSessionVariables = DirectCast(Session("SessionVariables"), clsSessionVariables)
                    sesVars.MemberData = DataRecordLoginReturn
                    Session("LagID") = DataRecordLoginReturn.LAGID
                    Session("ProfileID") = DataRecordLoginReturn.ProfileID
                    Session("MirrorProfileID") = DataRecordLoginReturn.MirrorProfileID
                End If
            End If


            If (Not Me.IsPostBack) Then
                LoadLAG()
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try
    End Sub




    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
    End Sub


    Protected Sub LoadLAG()
        Try

            'SetControlsValue(Me, CurrentPageData)


            lblItemsName.Text = Me.CurrentPageData.GetCustomString("CartPhotosView")
            lnkViewDescription.Text = Me.CurrentPageData.GetCustomString("CartPhotosWhatIsIt")
            lnkViewDescription.OnClientClick = OnMoreInfoClickFunc(
                lnkViewDescription.OnClientClick,
                ResolveUrl("~/Members/InfoWin.aspx?info=photos"),
                Me.CurrentPageData.GetCustomString("CartPhotosView"))
            WhatIsIt1.PopupControl.HeaderText = Me.CurrentPageData.GetCustomString("CartPhotosView")

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Private Sub Photos_PreRenderComplete(sender As Object, e As System.EventArgs) Handles Me.PreRenderComplete
        Try
            If (PhotosEdit1.Visible) Then
                If (PhotosEdit1.NoPhotoAvailable) Then
                    divNoPhoto.Visible = PhotosEdit1.NoPhotoAvailable
                    msg_HasNoPhotosText.Text = PhotosEdit1.CurrentPageData.GetCustomString("msg_HasNoPhotosText_ND")
                End If
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

End Class
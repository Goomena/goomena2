﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Members/Members2Inner.Master" CodeBehind="Messages.aspx.vb" 
    Inherits="Dating.Server.Site.Web.Messages" %>

<%@ Register src="~/UserControls/MemberLeftPanel.ascx" tagname="MemberLeftPanel" tagprefix="uc1" %>
<%@ Register src="~/UserControls/WhatIsIt.ascx" tagname="WhatIsIt" tagprefix="uc2" %>
<%@ Register src="~/UserControls/MessagesControl.ascx" tagname="MessagesControl" tagprefix="uc2" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%--<script src="../Scripts/bootstrap-dropdown.js" type="text/javascript"></script>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
<div id="messages-list">

<div id="messages-list-top">
    <div id="top-links-wrap">
    <table class="center-table" cellpadding="0" cellspacing="0">
        <tr>
            <td>
<ul id="top-links">
    <li runat="server" id="liNew"><asp:HyperLink ID="lnkNew" runat="server" 
                                        NavigateUrl="?t=NEW" onclick="ShowLoading();" 
                                        CssClass="btn list-btn lnk83"><span></span>New</asp:HyperLink></li>
    <li runat="server" id="liInbox"><asp:HyperLink ID="lnkInbox" runat="server" 
                                        NavigateUrl="?t=INBOX" onclick="ShowLoading();" 
                                        CssClass="btn list-btn lnk112"><span></span>Inbox</asp:HyperLink></li>
    <li runat="server" id="liSent"><asp:HyperLink ID="lnkSent" runat="server" 
                                        NavigateUrl="?t=SENT" onclick="ShowLoading();" 
                                        CssClass="btn list-btn lnk112"><span></span>Rejected</asp:HyperLink></li>
    <li runat="server" id="liTrash"><asp:HyperLink ID="lnkTrash" runat="server" 
                                        NavigateUrl="?t=TRASH" onclick="ShowLoading();" 
                                        CssClass="btn list-btn lnk112"><span></span>Accepted</asp:HyperLink></li>
</ul>

<div class="list-arrow"></div>
<div class="clear"></div>
            </td>
        </tr>
    </table>
    </div>

    <div class="list-title">
        <asp:Literal ID="lblItemsName" runat="server" />
        <asp:LinkButton ID="lnkViewDescription" runat="server" CssClass="more_info2" OnClientClick="OnMoreInfoClick('[contentUrl]', this, '[headerText]');return false;" />
    </div>

    <div id="title-filters">
        <div id="divFilter" runat="server" clientidmode="Static" class="messages-filters">
            <div class="lfloat cr-word">
                <dx:ASPxLabel ID="lblSortByText" runat="server" Text="" EncodeHtml="False">
                </dx:ASPxLabel>
            </div>
            <div class="lfloat">
                <dx:ASPxComboBox ID="cbSort" runat="server" class="update_uri" 
                    AutoPostBack="True" EncodeHtml="False">
                    <ClientSideEvents SelectedIndexChanged="ShowLoadingOnMenu" />
                </dx:ASPxComboBox>
            </div>
            <div class="rfloat">
                <dx:ASPxComboBox ID="cbPerPage" runat="server" class="update_uri input-small" style="width: 60px;" 
                    AutoPostBack="True" EncodeHtml="False">
                    <ClientSideEvents SelectedIndexChanged="ShowLoadingOnMenu" />
                    <Items>
                        <dx:ListEditItem Text="10" Value="10" Selected="true" />
                        <dx:ListEditItem Text="25" Value="25"  />
                        <dx:ListEditItem Text="50" Value="50" />
                    </Items>
                </dx:ASPxComboBox>
            </div>
            <div class="rfloat cr-word">
                <dx:ASPxLabel ID="lblResultsPerPageText" runat="server" Text="" EncodeHtml="False">
                </dx:ASPxLabel>
            </div>
            <div class="clear"></div>
        </div>
    </div>

</div>
 
<script type="text/javascript">
    (function () {
        var itms = $("a", "#top-links").css("width", "auto");
        for (var c = 0; c < itms.length; c++) {
            var w = $(itms[c]).width();
            $(itms[c]).css("width", "").removeClass("lnk83").removeClass("lnk112").removeClass("lnk127").removeClass("lnk161")
            if (w < 73)
                $(itms[c]).addClass("lnk83");
            else if (w < 100)
                $(itms[c]).addClass("lnk112");
            else if (w < 115)
                $(itms[c]).addClass("lnk127");
            else
                $(itms[c]).addClass("lnk161");
        }

        try {
            var el = $("a", "#top-links .down");
            var arr = $(".list-arrow", "#top-links-wrap");
            var p1 = el.position().left;
            p1 = (p1 + (el.width() / 2)) - (arr.width() / 2);
            arr.css("left", p1 + "px");
        }
        catch (e) { }
    })();
</script>
       
    <div class="m_wrap">
 
    <div id="divActionError" runat="server" visible="false">
        <div class="clear"></div>
        <div class="items_hard">
            <asp:Literal ID="lblActionError" runat="server"></asp:Literal><p><asp:HyperLink ID="lnkAlternativeAction" runat="server" CssClass="btn lighter"></asp:HyperLink></p><div class="clear">
            </div>
        </div>
    </div>
    
    <div class="rfloat">
        <asp:HyperLink ID="lnkInactiveProfiles" runat="server" NavigateUrl="~/Members/Messages.aspx?t=NEW&inactive=1" onClick="ShowLoading();">Inactive Profiles</asp:HyperLink>
    </div>
    <div class="clear"></div>


    <asp:MultiView ID="mvMessages" runat="server" ActiveViewIndex="0">

        <asp:View ID="vwAllMessage" runat="server">

            <uc2:MessagesControl ID="msgsL" runat="server" UsersListView="AllMessages"/>
            <div class="pagination">
                <table align="center">
                    <tr>
                        <td><dx:ASPxPager ID="Pager" runat="server"
                                ItemCount="3" 
                                ItemsPerPage="1" 
                                RenderMode="Lightweight" CssFilePath="~/App_Themes/Aqua/{0}/styles.css" 
                                CssPostfix="Aqua" 
                                CurrentPageNumberFormat="{0}" 
                                PageNumberFormat="<span onclick='ShowLoading();'>{0}</span>"
                                EncodeHtml="false"
                                SpriteCssFilePath="~/App_Themes/Aqua/{0}/sprite.css"
                                SeoFriendly="Enabled">
                                <lastpagebutton visible="True"> 
                                </lastpagebutton>
                                <firstpagebutton visible="True">
                                </firstpagebutton>
                                <Summary Position="Left" Text="Page {0} of {1} ({2} members)" Visible="false" />
                        </dx:ASPxPager></td>
                    </tr>
                </table>
            </div>

        </asp:View>
        <asp:View ID="vwNoMessages" runat="server">

            <div class="m_wrap">
                <div class="clearboth padding">
                </div>
<div class="items_none">
    <div id="messages_icon" class="middle_icon" runat="server" clientidmode="Static" enableviewstate="false"></div>
            <div class="items_none_wrap">
    <div class="items_none_text">
        <dx:ASPxLabel ID="lblNoMessagesText" runat="server" Text="" EncodeHtml="False">
        </dx:ASPxLabel>
        <div class="search_members_button">
            <asp:HyperLink ID="lnkSearchMembers" runat="server" CssClass="btn lighter" NavigateUrl="~/Members/Search.aspx">
                <asp:Literal ID="msg_SearchOurMembersText" runat="server"></asp:Literal>
                <i class="icon-chevron-right"></i>
            </asp:HyperLink>
        </div>
        <div class="clear">
        </div>
    </div>
    </div>
</div>
            </div>
            <br />
        </asp:View>


        <asp:View ID="vwEmpty" runat="server">
        </asp:View>

    </asp:MultiView>


    </div>
</div>

	<uc2:whatisit ID="WhatIsIt1" runat="server"  />
    

    <script type="text/javascript">
        jQuery(function ($) {
            $(window).ready(function () {
                scrollToElem($('#<%=  Mybase.ScrollToElem %>'), 160)
                scrollToLogin();
            });
        });

        //        function OnMoreInfoClick(contentUrl, obj, headerText) {
        //            if (headerText != null) popupWhatIs.SetHeaderText(headerText);
        //            if (obj != null) popupWhatIs.SetPopupElementID(obj.id);
        //            popupWhatIs.SetContentUrl(contentUrl);
        //            popupWhatIs.Show();
        //        }
    </script>

</asp:Content>



<%--        <asp:View ID="vwConversation" runat="server">
<div id="divProfileDeletedInfo" runat="server" visible="false" >
<div class="clearboth padding">
</div>
<div class="items_none">
<asp:Literal ID="lblProfileDeletedInfo" runat="server"></asp:Literal><div class="clear"></div>
</div>
</div>
<asp:SqlDataSource ID="sdsConversations" runat="server" 
ConnectionString="<%$ ConnectionStrings:AppDBconnectionString %>" 
SelectCommand="GetConversationWithMessage" SelectCommandType="StoredProcedure">
<SelectParameters>
<asp:Parameter DefaultValue="0" Name="messageID" Type="Int32" />
<asp:Parameter DefaultValue="0" Name="ownerProfileID" Type="Int32" />
</SelectParameters>
</asp:SqlDataSource>
<dx:ASPxDataView ID="dvConversation" runat="server" ColumnCount="1" 
RowPerPage="10" DataSourceID="sdsConversations" Width="100%">
<ItemTemplate>
<table style="width:100%;">
<tr class="m_item" style="border-bottom:none;">
<td valign="top" align="left">
<asp:Label ID="lblScrollto" runat="server" Text="" Font-Italic="true" />
<div class="clear"></div>
<div style="padding:0 0 20px;">
</div>
<div style="">
<dx:ASPxLabel ID="lblSubjectTitle" runat="server" Text="Subject" EncodeHtml="false" Font-Italic="true"/>
</div>
<div style="padding:5px 0 20px;">
<dx:ASPxLabel ID="lblSubject" runat="server" Text='<%# Eval("subject") %>' EncodeHtml="false"/>
</div>
<dx:ASPxLabel ID="lblTextTitle" runat="server" Text="Body" EncodeHtml="false" Font-Italic="true"/>
<div style="padding:5px 0;">
<dx:ASPxLabel ID="lblText" runat="server" Text='<%# Eval("Body") %>' EncodeHtml="false"/>
</div>
</td>
<td valign="top" style="width:10%;">
<dx:ASPxHyperLink ID="lnlFromLogin" runat="server" EncodeHtml="false" 
Text='<%# Eval("LoginName") %>' CssClass="loginNameRight"
NavigateUrl='<%# System.Web.VirtualPathUtility.ToAbsolute("~/Members/Profile.aspx?p=") &  HttpUtility.UrlEncode(Eval("LoginName").Tostring()) %>'>
</dx:ASPxHyperLink>
<dx:ASPxLabel ID="lblAmountDate" runat="server"  EncodeHtml="false"  Text="">
</dx:ASPxLabel>
</td>
<td valign="top" style="width:10%;position:relative;">
<div class="pic">
<dx:ASPxHyperLink ID="lnkFromImage" runat="server" EncodeHtml="false" Text="" NavigateUrl='<%# System.Web.VirtualPathUtility.ToAbsolute("~/Members/Profile.aspx?p=") &  HttpUtility.UrlEncode(Eval("LoginName").Tostring()) %>'>
</dx:ASPxHyperLink>
</div> 
<div class="delete rfloat" style="margin:10px 0;">
<asp:LinkButton ID="btnDelete" runat="server" CommandArgument='<%# Eval("EUS_MessageID") %>' CommandName="DELETEMSG" CssClass="button">
                        </asp:LinkButton></div><div class="clear"></div>
</td>
</tr>
</table>
</ItemTemplate>
<Paddings Padding="0px" />
</dx:ASPxDataView>


            <div class="t_reply" id="divConversationReply" runat="server">
                <div class="left msg">
                    <dx:ASPxMemo ID="txtConversationReply" runat="server" Columns="50" Rows="7">
                    </dx:ASPxMemo>
                    <asp:Panel ID="pnlConversationReplyErr" runat="server" CssClass="alert alert-error" Visible="False">
                        <asp:Label ID="lblConversationReplyErr" runat="server" Text=""></asp:Label></asp:Panel><div class="clearboth padding">
                    </div>
                </div>
                <div class="right">
                    <h2>
                        <dx:ASPxLabel ID="lblConversationReplyTitle" runat="server" Text="" EncodeHtml="False">
                        </dx:ASPxLabel>
                        </h2>
                    <dx:ASPxLabel ID="lblConversationReplyInformation" runat="server" Text="" 
                        EncodeHtml="False">
                    </dx:ASPxLabel>
                </div>
                <div class="clear">
                </div>
            </div>
            <div class="form-actions" id="divFormActions" runat="server" >
                <asp:Button ID="btnConversationReply" runat="server" 
                    CssClass="btn btn-primary btn-large" Text="Send Message" />
                &nbsp;</div><div class="clear">
            </div>
        </asp:View>


        <asp:View ID="vwViewMessage" runat="server">
            <dx:ASPxDataView ID="msgView" runat="server" ColumnCount="1" Width="100%" EnableDefaultAppearance="false" EnableTheming="False"  >
                <ItemTemplate>
                <div ID="filter" class="m_filter t_filter">
                    <div class="clear">
                    </div>
                    <dx:ASPxLabel ID="lblInfoMessage" runat="server" EncodeHtml="False" Text="" 
                        Visible="false" />
                    <table width="100%">
                        <tr>
                            <td><table>
                                    <tr>
                                        <td>
                                            <dx:ASPxButton ID="btnBack" runat="server" CommandName="CMDBACK" 
                                                EncodeHtml="False" Text="Back" /></td>
                                        <td>
                                            <dx:ASPxButton ID="btnDelete" runat="server" 
                                                CommandArgument='<%# Eval("EUS_MessageID") %>' CommandName="DELETEMSG" 
                                                EncodeHtml="False" Text="Delete" /></td>
                                    </tr>
                                </table></td>
                            <td align="right">
                                <dx:ASPxLabel ID="lblSubjectTop" runat="server" Text='<%# Eval("Subject") %>' />
                                <p>
                                    <dx:ASPxLabel ID="lblConversation" runat="server" EncodeHtml="False" 
                                        Text='<%# Eval("ConversationWithText") %>' /></p>
                            </td>
                        </tr>
                    </table>
                    <div class="clear">
                    </div>
                </div>
                <div class="t_wrap">
                    <table width="100%">
                        <tr>
                            <td><div class="left">
                                    <p class="time">
                                        <dx:ASPxLabel ID="lblDate" runat="server" EncodeHtml="False" 
                                            Text='<%# Eval("SentDateTime") %>' /></p>
                                    <div class="message">
                                        <div class="arrow"></div>
                                        <p>
                                            <dx:ASPxLabel ID="lblMessage" runat="server" EncodeHtml="False" 
                                                Text='<%# Eval("Body") %>' /></p>
                                    </div>
                                </div>
                            </td>
                            <td align="right"><table>
                                    <tr>
                                        <td><div class="info">
                                                <h2>
                                                    <dx:ASPxHyperLink ID="lnkLogin" runat="server" CssClass="loginName" 
                                                        NavigateUrl='<%# Eval("OtherMemberProfileViewUrl") %>' 
                                                        Text='<%# Eval("LoginName") %>' /></h2>
                                                <p>
                                                    <dx:ASPxLabel ID="lblDateOffer" runat="server" EncodeHtml="false" 
                                                        Text='<%# Eval("OfferAmountText") %>' /></p>
                                            </div></td>
                                        <td><div class="photo">
                                                <div class="thumb">
                                                    <a href='<%# Eval("OtherMemberProfileViewUrl") %>'>
                                                    <dx:ASPxImage ID="imgPhoto" runat="server" 
                                                        ImageUrl='<%# Eval("OtherMemberImageUrl") %>' /></a></div>
                                            </div>
                                        </td>
                                    </tr>
                                </table></td>
                        </tr>
                    </table>
                </div>
                <div class="clear">
                </div></ItemTemplate></dx:ASPxDataView>

            <div class="t_reply">
                <div class="left msg">
                    <dx:ASPxMemo ID="txtReplyMessageBody" runat="server" Columns="50" Rows="7">
                    </dx:ASPxMemo>
                    <asp:Panel ID="pnlMessageErr" runat="server" CssClass="alert alert-error" Visible="False">
                        <asp:Label ID="lblMessageErr" runat="server" Text=""></asp:Label></asp:Panel><div class="clearboth padding">
                    </div>
                </div>
                <div class="right">
                    <h2>
                        <dx:ASPxLabel ID="lblReplyTitle" runat="server" Text="" EncodeHtml="False">
                        </dx:ASPxLabel>
                        </h2>
                    <dx:ASPxLabel ID="lblReplyInformation" runat="server" Text="" 
                        EncodeHtml="False">
                    </dx:ASPxLabel>
                </div>
                <div class="clear">
                </div>
            </div>
            <div class="form-actions">
                <asp:Button ID="btnReplyMessage" runat="server" 
                    CssClass="btn btn-primary btn-large" Text="Send Message" />
                &nbsp;</div><div class="clear">
            </div>


        </asp:View>
                      
        <asp:View ID="vwCreateMessage" runat="server">
            <div ID="content-outter0" class="middle">
                <div ID="Div1" class="m_filter t_filter msg_filter">
                    <table style="width:100%;">
                    <tr>
                        <td align="left" valign="top"><div class="left">
                            <h2>
                                <dx:ASPxLabel ID="lblSendMessageToTitle" runat="server" EncodeHtml="False" 
                                    Text="Send a Message to " EnableDefaultAppearance="False"/>
                                <dx:ASPxLabel ID="lblSendToLogin" runat="server" Text="" 
                                    EncodeHtml="False" EnableDefaultAppearance="False" EnableTheming="True"/>
                            </h2>
                        </div></td>
                        <td align="right" valign="top"><div class="right">
                        <dx:ASPxHyperLink ID="lnkBack" runat="server" CssClass="back_btn" Text="Back" EncodeHtml="False" Visible="False" />
                    </div></td>
                    </tr>
                    </table>
                    <div class="clear">
                    </div>
                </div>
                <div class="t_wrap" style="background-color:#ffffff;">
                    <div class="items_none" id="pnlSendMessageErr" runat="server" visible="false">
                        <asp:Literal ID="lblSendMessageErr" runat="server"></asp:Literal><p><asp:HyperLink ID="lnkSendMessageAltAction" runat="server" CssClass="btn lighter" NavigateUrl="~/Members/SelectProduct.aspx"/></p>
                        <div class="clear">
                        </div>
                    </div>
                    <div class="send_msg t_to msg">

                    

                        <div class="left">
                            <div class="message">
                                <div class="arrow">
                                </div>
                                <div class="msg_form">
                                    <p>
                                        <dx:ASPxLabel ID="lblYourMsgSubject" runat="server" Text="Subject:" EncodeHtml="False">
                                        </dx:ASPxLabel>
                                    </p>
                                    <asp:Panel ID="pnlSubjectErr" runat="server" CssClass="alert alert-danger" Visible="False" ViewStateMode="Disabled">
                                        <asp:Label ID="lblSubjectErr" runat="server" Text=""></asp:Label></asp:Panel><dx:ASPxTextBox ID="txtMessageSubject" runat="server" Width="170px">
                                    </dx:ASPxTextBox>
                                    <div class="clearboth big_padding">
                                    </div>
                                    <p>
                                        <dx:ASPxLabel ID="lblYourMsgBody" runat="server" Text="Your Message:" EncodeHtml="False" />
                                    </p>
                                    <dx:ASPxMemo ID="txtMessageText" runat="server" Rows="7" Width="170px"/>
                                    <div id="Div2" class="alert alert-error hidden"></div>
                                </div>
                            </div>
                            <div class="msg_info">
                                <h2><dx:ASPxLabel ID="lblYourFirstDateStartedTitle" runat="server" Text="Getting Your First Date Started" EncodeHtml="False"/></h2>
                                    <dx:ASPxLabel ID="lblYourFirstDateStartedText" runat="server" Text="" EncodeHtml="False" CssClass="bottom_text" />
                            </div>
                        </div>


                        <div class="right">
                            <div class="photo">
                                <asp:HyperLink ID="lnkProfImg" runat="server"><asp:Image ID="imgProf" runat="server" /></asp:HyperLink>
                            </div>
                            <div class="info">
                                <p>
                                    <b><dx:ASPxLabel ID="lblYourMsgFrom" runat="server" Text="From:" EncodeHtml="False">
                                        </dx:ASPxLabel></b></p>
                                <dx:ASPxHyperLink ID="lnkProfLogin" runat="server" 
                                    Text=""  EncodeHtml="False" CssClass="loginNameRight"/>
                            </div>
                            <div class="clearboth padding">
                            </div>
                            <div class="photo">                                                
                                <asp:HyperLink ID="lnkOtherProfImg" runat="server"><asp:Image ID="imgOtherProf" runat="server" /></asp:HyperLink>
                            </div>
                            <div class="info">
                                <p>
                                    <b><dx:ASPxLabel ID="lblYourMsgTo" runat="server" Text="To:" EncodeHtml="False">
                                        </dx:ASPxLabel></b></p>
                                    <dx:ASPxHyperLink ID="lnkOtherProfLogin" runat="server" Text=""  EncodeHtml="False" CssClass="loginNameRight"/>
                            </div>
                            <div class="clearboth padding">
                            </div>
                            <div class="fdate" id="divDatingAmount" runat="server">
                                <asp:Label ID="lblDatingAmount" runat="server" Text=""/>
                            </div>
                        </div>
                        
                                        
                        <div class="clear">
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    &nbsp;<asp:Button ID="btnSendMessage" runat="server" 
                        CssClass="btn btn-primary btn-large" Text="Send Message" />
                </div>
                <div class="clear">
                </div>
            </div>

        </asp:View>
--%>


﻿'Imports Sites.SharedSiteCode
'Imports SpiceLogicPayPalStandard
Imports Library.Public
Imports Dating.Server.Core.DLL


Public Class SelectPayment
    Inherits BasePage



    'Dim _pageData As clsPageData
    'Protected ReadOnly Property CurrentPageData As clsPageData
    '    Get
    '        If (_pageData Is Nothing) Then _pageData = New clsPageData(Context)
    '        Return _pageData
    '    End Get
    'End Property


    Protected Overrides Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        SetMasterPage(CurrentPageData)

        Try
            If clsCurrentContext.VerifyLogin() = True Then
                Me.MasterPageFile = "~/Members/Members2.Master"
                'If (Request("master") = "inner") Then
                '    Me.MasterPageFile = "~/Members/Members2Inner.Master"
                'Else
                'End If
                If Me.IsFemale = True Then
                    Response.Redirect(ResolveUrl("~/Members/PaymentWoman.aspx"), True)
                End If
            Else
                clsCurrentContext.ClearSession(Session)
                If (Not Me.IsCallback) Then
                    Response.Redirect(ResolveUrl("~/Login.aspx"), False)
                End If
            End If
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If (Not Me.IsPostBack) Then
                If (Me.Master.GetType() Is GetType(Members2)) Then
                    DirectCast(Me.Master, Members2).HideBottom_MemberLeftPanel = True
                End If

                If (Me.SessionVariables.MemberData.Country = "TR") Then
                    Response.Redirect("SelectPaymentTR.aspx?choice=" & Request.QueryString("choice"))
                    'UCSelectPayment1.Visible = False
                    'UCSelectPayment_TR1.Visible = True
                Else
                    ' ok
                End If

                LoadLAG()
                'If (Request.UrlReferrer IsNot Nothing) Then
                '    lnkBack.NavigateUrl = Request.UrlReferrer.ToString()
                'Else
                '    lnkBack.NavigateUrl = "~/Members/"
                'End If
            End If

        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try
    End Sub


    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender

        Try
            Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub




    Public Overrides Sub Master_LanguageChanged()

        If (Me.Visible) Then
            Me._pageData = Nothing
            LoadLAG()
        End If

    End Sub


    Protected Sub LoadLAG()
        Try

            '    Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic


            'lblPaymentMethodTitle.Text = CurrentPageData.GetCustomString(lblPaymentMethodTitle.ID)
            lnkBack.Text = CurrentPageData.GetCustomString(lnkBack.ID)


            lblItemsName.Text = Me.CurrentPageData.GetCustomString("CartSelectPaymentView")
            lnkViewDescription.Text = Me.CurrentPageData.GetCustomString("CartSelectPaymentWhatIsIt")
            lnkViewDescription.OnClientClick = OnMoreInfoClickFunc(
                lnkViewDescription.OnClientClick,
                ResolveUrl("~/Members/InfoWin.aspx?info=selectpayment"),
                Me.CurrentPageData.GetCustomString("CartSelectPaymentView"))
            WhatIsIt1.PopupControl.HeaderText = Me.CurrentPageData.GetCustomString("CartSelectPaymentView")

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub
End Class
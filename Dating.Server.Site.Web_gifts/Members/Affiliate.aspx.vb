﻿Imports Dating.Server.Core.DLL
Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Datasets.DLL.DSMembers

Public Class Affiliate
    Inherits BasePage

    'Dim _pageData As clsPageData
    'Protected ReadOnly Property CurrentPageData As clsPageData
    '    Get
    '        If (_pageData Is Nothing) Then
    '            _pageData = New clsPageData(Context)
    '            AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
    '        End If
    '        Return _pageData
    '    End Get
    'End Property


    Protected Overrides Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        SetMasterPage(CurrentPageData)

        Try
            If clsCurrentContext.VerifyLogin() = True Then
                Me.MasterPageFile = "~/Members/Members2.Master"
            End If
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If (Not Me.IsPostBack) Then
                CmdRun_Click(CmdRun, Nothing)
                LoadLAG()
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub



    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender

        Try
            Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub




    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
        UpdateUserControls(Me)
    End Sub


    Protected Sub LoadLAG()
        Try
            ' Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic

            CmdRun.Text = CurrentPageData.GetCustomString("CmdRun")

            SetControlsValue(Me, CurrentPageData)

            lblItemsName.Text = Me.CurrentPageData.GetCustomString("CartAffiliateView")
            lnkViewDescription.Text = Me.CurrentPageData.GetCustomString("CartAffiliateWhatIsIt")
            lnkViewDescription.OnClientClick = OnMoreInfoClickFunc(
                lnkViewDescription.OnClientClick,
                ResolveUrl("~/Members/InfoWin.aspx?info=affiliatedashboard"),
                Me.CurrentPageData.GetCustomString("CartAffiliateView"))
            WhatIsIt1.PopupControl.HeaderText = Me.CurrentPageData.GetCustomString("CartAffiliateView")


        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Protected Sub CmdRun_Click(sender As Object, e As EventArgs) Handles CmdRun.Click
        Try

            If (clsCurrentContext.IsAffiliate()) Then

                If (Not IsPostBack) Then
                    deFromDate.Value = Date.Now.Date.AddDays(-5)
                    deToDate.Value = Date.Now.Date.AddDays(1)

                    affiliate_area.Visible = True
                End If


                Dim prof As vw_EusProfile_Light = Me.GetCurrentProfile()
                '     Dim transactions As Double
                'Dim CreditAmount As Double
                Dim DebitAmount As Double
                Dim Visitors As Double, Registers As Double

                If (Not String.IsNullOrEmpty(prof.AFF_Code)) Then
                    lblAffInfo.Text = "Login name: " & prof.LoginName & _
                        "<br> Affiliate code: " & prof.AFF_Code & _
                        "<br> Commission: " & If(Not prof.AffiliateCommissionPercent.HasValue, 0, prof.AffiliateCommissionPercent.Value) & _
                        "%<br> Registered on: " & prof.DateTimeToRegister

                    lblAffLink.Text = lblAffLink.Text.Replace("[CODE]", prof.AFF_Code)

                    Dim sql As String = <sql><![CDATA[
EXEC [AFFILIATE_GetStatistics] @CustomReferrer=@CustomReferrer,@DateFrom=@DateFrom,@DateTo=@DateTo
]]></sql>
                    Dim dt As DataTable
                    Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                        Using cmd As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                            cmd.Parameters.AddWithValue("@CustomReferrer", prof.AFF_Code)
                            cmd.Parameters.AddWithValue("@DateFrom", deFromDate.Value)
                            cmd.Parameters.AddWithValue("@DateTo", deToDate.Value)
                            dt = DataHelpers.GetDataTable(cmd)
                        End Using
                    End Using


                    If (dt.Rows.Count > 0) Then
                        '     transactions = dt.Rows(0)("transactions")
                        'CreditAmount = dt.Rows(0)("CreditAmount")
                        DebitAmount = dt.Rows(0)("DebitAmount")
                        Visitors = dt.Rows(0)("Visitors")
                        Registers = dt.Rows(0)("Registers")
                    End If
                End If

                lbUniqueByIP.Text = Visitors
                lbRegistration.Text = Registers
                lbPayments.Text = DebitAmount
                lbEarning.Text = (DebitAmount * If(Not prof.AffiliateCommissionPercent.HasValue, 0, prof.AffiliateCommissionPercent.Value)) / 100

            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "CmdRun->Click")
        End Try

    End Sub

End Class
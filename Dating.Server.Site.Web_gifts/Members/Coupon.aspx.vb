﻿Imports Dating.Server.Core.DLL
Imports Dating.Server.Datasets.DLL

Public Class Coupon
    Inherits BasePage

    Private affCredits As clsReferrerCredits

    Protected Overrides Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        SetMasterPage(CurrentPageData)

        Try
            If clsCurrentContext.VerifyLogin() = True Then
                ''''  Me.MasterPageFile = "~/Members/Members2.Master"
            Else
                clsCurrentContext.ClearSession(Session)
                If (Not Me.IsCallback) Then
                    Response.Redirect(ResolveUrl("~/Login.aspx"), False)
                End If
                HttpContext.Current.ApplicationInstance.CompleteRequest()
            End If
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Protected Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData(Context, Me.Page, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Me.IsMale Then
                Response.Redirect(Page.ResolveUrl("~/Members/Default.aspx"))
            End If
            If (Not Me.IsPostBack) Then
                Dim rpp As Boolean = ProfileHelper.GetRewardProgramParticipate(Me.MasterProfileId)
                If Not (rpp) Then
                    Response.Redirect("GiftsInfo.aspx")
                    Return
                End If

            End If
            LoadLAG()
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender

        Try
            Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try
        'Try
        '    If (Not Page.IsPostBack) Then
        '        If (ucCredits.HasCredits) Then
        '            CouponCreate.Visible = True
        '            CouponPreview.Visible = True
        '        End If
        '    End If
        'Catch ex As Exception
        '    WebErrorMessageBox(Me, ex, "Page_Load")
        'End Try
    End Sub

    Protected Sub LoadLAG()
        Try
            'If (String.IsNullOrEmpty(CurrentPageData.GetCustomString("lnkNew"))) Then
            '    clsPageData.ClearCache()
            'End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub
   


End Class

﻿<%@ Page Title="LoveHate" Language="vb" AutoEventWireup="false" MasterPageFile="~/Members/Members2Inner.Master" MaintainScrollPositionOnPostback="false"
     CodeBehind="LoveHate.aspx.vb" Inherits="Dating.Server.Site.Web.LoveHate" enableEventValidation="true" %>

<%@ Register Assembly="DevExpress.Web.v13.1, Version=13.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>


<%@ Register src="~/UserControls/LoveHateControl.ascx" tagname="LoveHateControl" tagprefix="uc2" %>
<%@ Register src="~/UserControls/MemberLeftPanel.ascx" tagname="MemberLeftPanel" tagprefix="uc3" %>
<%@ Register src="~/UserControls/LoveHateWinnersControl.ascx" tagname="LoveHateWinnersControl" tagprefix="uc4" %>
<%@ Register src="~/UserControls/WhatIsIt.ascx" tagname="WhatIsIt" tagprefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   

	<link href="/v1/css/LoveHate.css?v108" rel="stylesheet" />

<script type="text/javascript">
    $(document).ready(function () {
        BindEvents();

        //var prm = Sys.WebForms.PageRequestManager.


        //prm.add_beginRequest(beginRequest);

    });

    function CLEAR() {
        if(window["<%= txtUserNameQ.ClientID%>"] != null){
            window["<%= txtUserNameQ.ClientID%>"].SetText('');
            $("#<%= imgSearchByUserName.ClientID %>" ).click()
        }
    }

   
    function scrollToResultsTop() {
        scrollWin('#<%= divFilter.ClientID%>', -200, 500);
    }

    
    function BindEvents() {
             $("body").on("mouseenter", ".btn.lnkHate.NoVoted", function () {
            var parentTag = '#' + $(this).parent().parent().attr('id');
            var ppp = $(".PhotoOverlay", parentTag);
            ppp.hide();

            ppp.addClass("Hate");//('background', "url('//cdn.goomena.com/images2/Lovehate_Negative.png') no-repeat");
            ppp.fadeIn(200);
        }).on("mouseleave", ".btn.lnkHate.NoVoted", function () {
            //  var parentTag = '#' + $(this).parent().parent().attr('ID');
            $(".PhotoOverlay").removeClass("Hate");
            $(".PhotoOverlay").fadeOut(100);
        });
        $("body").on("mouseenter", ".btn.lnkLove.NoVoted", function () {
            var parentTag = '#' + $(this).parent().parent().attr('id');
            var ppp = $(".PhotoOverlay", parentTag);
            ppp.hide();

            ppp.addClass("Love");//('background', "url('//cdn.goomena.com/images2/Lovehate_Negative.png') no-repeat");
            ppp.fadeIn(200);
        }).on("mouseleave", ".btn.lnkLove.NoVoted", function () {
            //  var parentTag = '#' + $(this).parent().parent().attr('ID');
            $(".PhotoOverlay").removeClass("Love");
            $(".PhotoOverlay").fadeOut(100);
        });
        $("body").on("mouseenter", ".tt_enabled_LoveHatePhoto", function () {
            $(".tooltip").hide();
            var parentTag = '#' + $(this).parent().parent().attr('id');
            $(".tooltip", parentTag).fadeIn(200);
        }).on("mouseleave", ".tt_enabled_LoveHatePhoto", function () {
            $(".tooltip").fadeOut(50)
        });
        $("body").on("mouseenter", ".tt_enabled_btnLoveHate", function () {
            $(".tooltip").hide();
            var parentTag = '#' + $(this).parent().parent().attr('id');
            $(".tooltip", parentTag).fadeIn(200);
        }).on("mouseleave", ".tt_enabled", function () {
            $(".tooltip").fadeOut(100);
        });
        $(".btn.lnkLove.NoVoted").click(function () {
            if ($(this).attr('disable') == null) {
                $(this).attr('disable', 'disable');
                var ppp4 = $(this).siblings(".btn.lnkHate.NoVoted")
                ppp4.attr('disable', 'disable');
                var parentTag = '#' + $(this).parent().parent().attr('id');
                $(this).removeClass("NoVoted");
                $(this).addClass("Voted");
                $(this).addClass("Enabled");
                $(this).css({ 'cursor': 'default' });
                ppp4.removeClass("NoVoted");
                ppp4.addClass("Voted");
                ppp4.css({ 'cursor': 'default' });
               
                var ppp = $(".PlusSymbol", parentTag);
                var ppp2 = $(".PhotoOverlay", parentTag);
                var pp3 = $(".LoveCounter", parentTag);
              
              
                //ppp4.removeClass("NoVoted");
                //ppp4.removeClass("Voted");
                ppp.removeClass(".PlusSymbol");
                pp3.removeClass(".LoveCounter");
                ppp2.removeClass("PhotoOverlay");
                ppp2.addClass("prmOverlayLoveEnabled");
                ppp.fadeIn(100);
                ppp.animate({ 'top': '157px', 'left': '5px' }, 300);
                ppp.fadeOut(50);
                pp3.text(parseInt(pp3.text()) + 1);
             
            }
        });
              $(".btn.lnkHate.NoVoted").click(function () {
            if ($(this).attr('disable') == null) {
                $(this).attr('disable', 'disable');
                var ppp4 = $(this).siblings(".btn.lnkLove.NoVoted")
                ppp4.attr('disable', 'disable');
                $(this).removeClass("NoVoted");
                $(this).addClass("Voted");
                $(this).addClass("Enabled");
                $(this).css({ 'cursor': 'default' });
                ppp4.removeClass("NoVoted");
                ppp4.addClass("Voted");
                ppp4.css({ 'cursor': 'default' });
                var parentTag = '#' + $(this).parent().parent().attr('id');
                var ppp = $(".PlusSymbol", parentTag);
                var ppp2 = $(".PhotoOverlay", parentTag);
                var pp3 = $(".HateCounter", parentTag);
               
                ppp.removeClass(".PlusSymbol");
                pp3.removeClass(".HateCounter");
                ppp2.removeClass("PhotoOverlay");
                ppp2.addClass("prmOverlayHateEnabled");
                ppp.css({ 'top': '57px', 'z-index': '1000' });
                ppp.fadeIn(100);
                ppp.animate({ 'top': '160px', 'left': '68px' }, 300);
                ppp.fadeOut(50);
                pp3.text(parseInt(pp3.text()) + 1);
                                     }
         });
         $("body").on("mouseenter", ".btn.lnkLove.DisVote", function () {
             var parentTag = '#' + $(this).parent().parent().attr('id');
             var ppp = $(".LoveHateNoVote", parentTag);
             ppp.fadeIn(100);

         }).on("mouseleave", ".btn.lnkLove.DisVote", function () {
             $(".LoveHateNoVote").fadeOut(100);
         });
         $("body").on("mouseenter", ".btn.lnkHate.DisVote", function () {
             var parentTag = '#' + $(this).parent().parent().attr('id');
             var ppp = $(".LoveHateNoVote", parentTag);
             ppp.fadeIn(100);

         }).on("mouseleave", ".btn.lnkHate.DisVote", function () {
             $(".LoveHateNoVote").fadeOut(100);
         });
      }

   
    //);
      
    
</script>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <asp:Label ID="LoveHateCmsContentID" runat="server" Text="">
   <%-- <div id="LoveHateCmsContentId" class="LoveHateCmsContent <%# Eval("cmspageClass")%>" >--%>
        <div id="Header"><asp:Label Id="lblContentHeader" runat ="server" ></asp:Label></div>
       <div id="MainTxt"><asp:Label Id="lblContentTXT" runat ="server" ></asp:Label></div>

    </asp:Label>
      


     <asp:UpdatePanel runat="server" UpdateMode="Always">
       
         <ContentTemplate>

              <script type="text/javascript">
                  Sys.Application.add_load(BindEvents);
     </script>
        
<div id="LoveHate-members">
          <div class="PreviousVotingWinners">
              
        <uc4:LoveHateWinnersControl ID="LoveHateWinners" runat="server" UsersListView="SearchingOffers" />
    </div>
            <div class="LoveHateViewHeader">
                <asp:Label ID="lblLoveHateViewHeader" runat="server" CssClass="lblLoveHateViewHeader" Text=''>

                </asp:Label>
                

            </div>
   

    <table style="margin-left:auto;margin-right:auto;">
    <tbody>
        <tr>
            <td style="">
                 <asp:Literal ID="lblUserHasVotes1" runat="server"  Text=''>

                </asp:Literal>
            </td>
            
            </tr>
        </tbody>
    </table>
         
    <div id="quick-search-form2" class="outter-box" >
        <asp:Panel ID="searchform2" runat="server">
      <div id="search-top-row">
        <asp:Panel ID="pnlQUserName" runat="server" DefaultButton="imgSearchByUserName" CssClass="input-user">
            <div class="search_textbox_container">
                <dx:ASPxTextBox ID="txtUserNameQ" runat="server" Width="191" Size="191"  CssClass="txtusnmQ">
                </dx:ASPxTextBox>
            </div>
            <div class="search_button_container">
                <asp:ImageButton ID="imgSearchByUserName" runat="server" ImageUrl="//cdn.goomena.com/Images/spacer10.png"
                    CssClass="btn imgSearchByUserName"  />
             </div>
            <a href="javascript:CLEAR();" class="link-clear">
            <asp:Label ID="msg_ClearForm" runat="server" Text="" CssClass="clear-label"></asp:Label>
        </a>
        </asp:Panel>
          
        
    
</div>
        <div class="ss-filters">
      
         <div id="divFilter" runat="server" clientidmode="Static">
             <div class="rfloat">
            <dx:ASPxComboBox ID="cbPerPage" runat="server" class="update_uri input-small" style="width: 60px;" 
                AutoPostBack="True">
                <ClientSideEvents SelectedIndexChanged="ShowLoadingOnMenu" />
                <Items>
                    <dx:ListEditItem Text="10" Value="10" Selected="true"  />
                    <dx:ListEditItem Text="25" Value="25" />
                    <dx:ListEditItem Text="50" Value="50" />
                </Items>
<ClientSideEvents BeginCallback="function(s, e) {
ShowLoading({ delay: 10, text: ''});
}" CallbackError="function(s, e) {
HideLoading();
}" EndCallback="function(s, e) {
HideLoading();
} 
" />
            </dx:ASPxComboBox>
        </div>
        <div class="rfloat descr-wrap">
            <asp:Label ID="msg_ResultsPerPage" runat="server" Text="Results Per Page" AssociatedControlID="cbPerPage" CssClass="description"></asp:Label></div><div class="clear"></div>
    </div>
    <div class="clear"></div>
    </div>
            </asp:Panel>
        </div>
       
<div class="s_wrap">
                     <uc2:LoveHateControl ID="searchL" runat="server" UsersListView="SearchingOffers" />
     </div>
<div class="pagination">
<table align="center">
    <tr>
        <td><dx:ASPxPager ID="Pager" runat="server" ItemCount="3" 
                ItemsPerPage="1" 
                EncodeHtml="false"
                RenderMode="Lightweight" CssFilePath="~/App_Themes/Aqua/{0}/styles.css" 
                CssPostfix="Aqua" 
                CurrentPageNumberFormat="{0}" 
                PageNumberFormat="<span onclick='ShowLoading();'>{0}</span>"
                SpriteCssFilePath="~/App_Themes/Aqua/{0}/sprite.css">
                <lastpagebutton visible="True">
                </lastpagebutton>
                <firstpagebutton visible="True">
                </firstpagebutton>
                <Summary Position="Left" Text="Page {0} of {1} ({2} members)" Visible="false" />
        </dx:ASPxPager></td>
    </tr>
</table>
</div>
     
        <table style="margin-left:auto;margin-right:auto;">
    <tbody>
        <tr>
            <td style="">
                 <asp:Literal ID="lblNoresults" runat="server"  Text=''>

                </asp:Literal>
            </td>
            
            </tr>
        </tbody>
    </table>
</div>
         </ContentTemplate></asp:UpdatePanel>
    <%--<uc2:whatisit ID="WhatIsIt1" runat="server" />--%>
        <dx:ASPxPopupControl runat="server"
        Height="340px" Width="500px"
        ID="popupSearchName"
        ClientInstanceName="popupSearchName" 
        ClientIDMode="AutoID" 
        AllowDragging="True" 
        PopupVerticalAlign="WindowCenter" 
        PopupHorizontalAlign="WindowCenter" 
        Modal="True" 
        CloseAction="CloseButton"
        AllowResize="True" 
        AutoUpdatePosition="True"
        ShowShadow="False">
    <ContentStyle HorizontalAlign="Left" VerticalAlign="Middle">
        <Paddings Padding="0px" PaddingTop="25px" />
        <Paddings Padding="0px" PaddingTop="25px" ></Paddings>
    </ContentStyle>
    <CloseButtonStyle BackColor="White">
        <HoverStyle>
            <BackgroundImage ImageUrl="//cdn.goomena.com/Images2/popup-wrn/close.png" Repeat="NoRepeat" HorizontalPosition="center"
                VerticalPosition="center"></BackgroundImage>
        </HoverStyle>
        <BackgroundImage ImageUrl="//cdn.goomena.com/Images2/popup-wrn/close.png" Repeat="NoRepeat" HorizontalPosition="center"
            VerticalPosition="center"></BackgroundImage>
        <BorderLeft BorderColor="White" BorderStyle="Solid" BorderWidth="10px" />
    </CloseButtonStyle>
    <CloseButtonImage Url="//cdn.goomena.com/Images/spacer10.png" Height="43px" Width="43px">
    </CloseButtonImage>
    <SizeGripImage Height="40px" Url="//cdn.goomena.com/Images/resize.png" Width="40px">
    </SizeGripImage>
    <HeaderStyle>
        <Paddings Padding="0px" PaddingLeft="0px" />
        <Paddings PaddingTop="0px" PaddingBottom="0px"></Paddings>
        <BorderTop BorderColor="White" BorderStyle="Solid" BorderWidth="5px" />
        <BorderRight BorderColor="White" BorderStyle="Solid" BorderWidth="5px" />
        <BorderBottom BorderColor="White" BorderStyle="Solid" BorderWidth="5px" />
    </HeaderStyle>
    <ModalBackgroundStyle BackColor="Black" Opacity="70" CssClass="WhatIs-ModalBG"></ModalBackgroundStyle>
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server" SupportsDisabledAttribute="True">
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>


</asp:Content>


﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="jsonConversationOlder.aspx.vb" Inherits="Dating.Server.Site.Web.ConversationOlder" %>
<%@ Register src="~/UserControls/MessagesConversation.ascx" tagname="MessagesConversation" tagprefix="uc3" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Panel ID="pnlMoreMsgs" runat="server" CssClass="pnlMoreMsgs-remove">
        </asp:Panel>
        <uc3:MessagesConversation ID="convCtl" runat="server" EnableViewState="false" />
    </form>
</body>
</html>

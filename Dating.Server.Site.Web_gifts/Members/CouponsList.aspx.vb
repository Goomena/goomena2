﻿Imports Dating.Server.Core.DLL
Imports Dating.Server.Datasets.DLL

Public Class CouponsList
    Inherits BasePage

    Protected Overrides Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        SetMasterPage(CurrentPageData)

        Try
            If clsCurrentContext.VerifyLogin() = True Then
                Me.MasterPageFile = "~/Members/Members2.Master"
            Else
                clsCurrentContext.ClearSession(Session)
                If (Not Me.IsCallback) Then
                    Response.Redirect(ResolveUrl("~/Login.aspx"), False)
                End If
                HttpContext.Current.ApplicationInstance.CompleteRequest()
            End If

        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub
    Protected Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData(Context, Me.Page, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Me.IsMale Then
                Response.Redirect(Page.ResolveUrl("~/Members/Default.aspx"))
            End If
            If (Not Me.IsPostBack) Then


            End If
            LoadLAG()
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender

        Try
            Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
            BindControlData()
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try
    End Sub
    Public Function GetGiftcard(ByVal Title As String, ByVal Amount As String) As String
        Dim tm As String = ""
        Try
            Dim i As Decimal = 0
            Decimal.TryParse(Amount, i)
            tm = Title & " " & i.ToString(0) & "€"
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
        Return tm
    End Function
    Public Function GetDatetime(ByVal datetime As Date) As String
        Dim tm As String = ""
        Try
            tm = AppUtils.GetDateTimeString(datetime, "", "dd/MM/yyyy HH:mm")
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
        Return tm
    End Function
    Private dictStatus As New Dictionary(Of Integer, String)
    Public Function GetStatus(ByVal statusId As Integer) As String
        Dim tm As String = ""
        Try
            tm = dictStatus(statusId)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
        Return tm
    End Function
    Private Sub BindControlData()
        Try


            Dim sql As String = <sql><![CDATA[
  SELECT 
      ord.[DateTimeCreated]
      ,ord.[StatusId]
      ,ord.[Notes]
      ,ord.[Points]
      ,ord.[Amount]
	  ,gif.[Title]
  FROM [dbo].[EUS_RewardsGiftCardsOrders]  as ord with (nolock) left join [dbo].[EUS_RewardsGiftCardsAvaliable] as av with (nolock)
  on av.RewardsGiftCardsAvaliableId=ord.[RewardsGiftCardsAvaliableId] 
   left join [dbo].[EUS_RewardsGiftsCards] as gif with (nolock) on av.RewardGiftsCardsId=gif.RewardGiftsCardsId 
  where [ProfileId]=@PrId or ProfileId =@MPrId
  order by ord.[DateTimeCreated] desc
]]></sql>
            Dim dtResults As DataTable = Nothing
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection
                Using cmd As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@PrId", Me.MasterProfileId)
                    cmd.Parameters.AddWithValue("@MPrId", Me.MirrorProfileId)
                    dtResults = DataHelpers.GetDataTable(cmd)
                End Using
            End Using
            rptOrders.DataSource = dtResults
            rptOrders.DataBind()
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub
    Protected Sub LoadLAG()
        Try
            lblTitle.Text = CurrentPageData.VerifyCustomString("lblTitle")
            lblHGiftCard.Text = CurrentPageData.VerifyCustomString("HGiftCard")
            lblHPoints.Text = CurrentPageData.VerifyCustomString("HPoints")
            lblHDate.Text = CurrentPageData.VerifyCustomString("HDate")
            lblHStatus.Text = CurrentPageData.VerifyCustomString("HStatus")
            dictStatus.Add(1, CurrentPageData.VerifyCustomString("Stat_InProcess"))
            lnkBackTOCoupon.NavigateUrl = ResolveUrl("~/Members/Coupon.aspx")
            lblBackTOCoupon.Text = CurrentPageData.VerifyCustomString("lblBackTOCoupon")
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub
End Class
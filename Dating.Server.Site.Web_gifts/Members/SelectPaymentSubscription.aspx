﻿<%@ Page Title="" Language="vb" AutoEventWireup="true" MasterPageFile="~/Members/Members2Inner.master" CodeBehind="SelectPaymentSubscription.aspx.vb" Inherits="Dating.Server.Site.Web.SelectPaymentSubscription" %>
<%--<%@ Register assembly="SpiceLogicPayPalStd" namespace="SpiceLogicPayPalStandard.Controls.BuyNowButton" tagprefix="cc1" %>
--%>
<%@ Register src="~/UserControls/UCSelectPayment2.ascx" tagname="UCSelectPayment" tagprefix="uc1" %>
<%@ Register src="~/UserControls/MemberLeftPanel.ascx" tagname="MemberLeftPanel" tagprefix="uc1" %>
<%@ Register src="~/UserControls/WhatIsIt.ascx" tagname="WhatIsIt" tagprefix="uc2" %>
<%@ Register src="~/UserControls/LiveZillaPublic.ascx" tagname="LiveZillaPublic" tagprefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<script type="text/javascript" src="//cdn.goomena.com/Scripts/fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>
	<script type="text/javascript" src="//cdn.goomena.com/Scripts/fancybox/source/jquery.fancybox.pack.js?v=2.1.4"></script>
	<script type="text/javascript" src="//cdn.goomena.com/Scripts/fancybox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
	<script type="text/javascript" src="//cdn.goomena.com/Scripts/fancybox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
	<script type="text/javascript" src="//cdn.goomena.com/Scripts/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.5"></script>

	<link rel="stylesheet" type="text/css" href="//cdn.goomena.com/Scripts/fancybox/source/jquery.fancybox.css?v=2.1.4" media="screen" />
	<link rel="stylesheet" type="text/css" href="//cdn.goomena.com/Scripts/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" />
	<link rel="stylesheet" type="text/css" href="//cdn.goomena.com/Scripts/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
<script type="text/javascript">
    // <![CDATA[
    (function () {
        $('.left-side-wrap').css("display", "none");
        $('#column-shadow').css("display", "none");
        $('.right-side').removeClass("lfloat");
        $('.right-side').css("margin-left", "auto");
        $('.right-side').css("margin-right", "auto");
    })();
    // ]]>
</script>
    <div class="clear"></div>


    <div class="container_12" id="tabs-outter">
        <div class="grid_12 top_tabs">
            <div>
            </div>
        </div></div><div class="clear"></div>
	<div class="container_12" id="content_top"></div>
	<div class="container_12" id="content">
        <div class="grid_3 top_sidebar" id="sidebar-outter">
            <div class="left_tab" id="left_tab" runat="server">
                <div class="top_info">
                </div>
                <div class="middle-top">
                        <h3><asp:Literal ID="lblItemsName" runat="server" /></h3>
                        <%--<div class="clear"></div>--%>
                        <asp:LinkButton ID="lnkViewDescription" runat="server" CssClass="more_info2" OnClientClick="OnMoreInfoClick('[contentUrl]', this, '[headerText]');return false;" />
                </div>
                <%--<div class="bottom">
                    &nbsp;</div>--%></div>
                <%--<uc1:memberleftpanel ID="leftPanel" runat="server" ShowBottomPanel="false" />--%>
            </div>
            <div class="grid_9 body">
                <div class="middle" id="content-outter">

            

            <div class="t_wrap">
	            <div style="background-color:#fff">
                    
                    <div id="filter" class="m_filter t_filter msg_filter">
	            
                        <div class="rfloat payment-back-link-container">
                        <dx:ASPxHyperLink ID="lnkBack" runat="server" Text="Back" CssClass="btn" NavigateUrl="~/Members/SelectProduct.aspx">
                        </dx:ASPxHyperLink>
	                </div>
	            <div class="clear"></div>
            </div>

        <uc1:UCSelectPayment ID="UCSelectPayment1" runat="server" />
                </div>
            </div>
			    </div>
				<div class="bottom"></div>
            </div>
            <div class="clear">
            </div>
        </div>



	<uc2:whatisit ID="WhatIsIt1" runat="server" />

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cntBodyBottom" runat="server">
    <uc3:LiveZillaPublic ID="ctlLiveZillaPublic" runat="server" />
</asp:Content>

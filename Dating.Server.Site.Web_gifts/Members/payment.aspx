﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="payment.aspx.vb" Inherits="Dating.Server.Site.Web.payment" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="../Scripts/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.percentageloader-0.1.js" type="text/javascript"></script>
    <script src="/v1/Scripts/script1.js?v=17" type="text/javascript"></script>
    <link rel="stylesheet" href="../Scripts/jquery.percentageloader-0.1.css"/>

    <style type="text/css">
        body { margin: 0px; padding: 0px; }
        #topLoader { width: 256px; height: 256px; margin-bottom: 32px; }
        #container { width: 256px; padding: 10px; margin-left: auto; margin-right: auto; margin-top: 100px; }
        #animateButton { width: 256px; }
    </style>

</head>
<body>
    <form id="form1" runat="server">
    <div>
<div id="container">
                    <div id="topLoader">      
                    </div>
    
                 <script>
                     $(function () {
                         var $topLoader = $("#topLoader").percentageLoader({ width: 256, height: 256, controllable: true, progress: 0.5, onProgressUpdate: function (val) {
                             $topLoader.setValue(Math.round(val * 100.0));
                         }
                         });

                         var topLoaderRunning = false;
                         $(document).ready(function () {

                             topLoaderRunning = true;
                             $topLoader.setProgress(0);
                             $topLoader.setValue(0);


                             var kb = 0;
                             var totalKb = 100;

                             var animateFunc = function () {
                                 kb += 1;
                                 $topLoader.setProgress(kb / totalKb);
                                 $topLoader.setValue(kb);

                                 if (kb < totalKb) {
                                     if (kb > 50) {
                                         if (kb > 70) {
                                             if (kb > 90) {

                                                 setTimeout(animateFunc, 1500);
                                             } else {

                                                 setTimeout(animateFunc, 400);
                                             }

                                         } else {

                                             setTimeout(animateFunc, 130);
                                         }

                                     } else {

                                         setTimeout(animateFunc, 50);
                                     }
                                 } else {
                                     topLoaderRunning = false;
                                 }
                             }

                             setTimeout(animateFunc, 25);

                         });
                     });      
      </script>
                </div>
                    </div>
    </form>
<% if not Me.IsPostBack then %>
    <script type="text/javascript">
        setTimeout(function () {
            $('form')[0].submit();
        }, 10);
    </script>
<% end if %>
</body>
</html>

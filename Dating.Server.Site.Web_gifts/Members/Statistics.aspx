﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Members/Members2.master" CodeBehind="Statistics.aspx.vb" Inherits="Dating.Server.Site.Web.Statistics" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .statistics-page { font-size:14px;margin:40px 0 0;}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="leftPanelUpperContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="content" runat="server">
<div class="statistics-page">

    <asp:UpdatePanel ID="up1" runat="server">
        <ContentTemplate>
<div style="width:708px;">
    <table cellpadding="0" cellspacing="0" class="center-table">
        <tr>
            <td><asp:LinkButton ID="btnLast7Days" runat="server">Last 7 days</asp:LinkButton></td>
            <td>&nbsp;&nbsp;&nbsp;</td>
            <td><asp:LinkButton ID="btnThisMonth" runat="server">This Month</asp:LinkButton></td>
            <td>&nbsp;&nbsp;&nbsp;</td>
            <td><asp:LinkButton ID="btn3Months" runat="server">Last 3 Months</asp:LinkButton></td>
            <td>&nbsp;&nbsp;&nbsp;</td>
            <td><asp:LinkButton ID="btn6Months" runat="server">Last 6 Months</asp:LinkButton></td>
        </tr>
    </table>
    <div style="height:15px;"></div>
    <table cellpadding="0" cellspacing="0" class="center-table">
        <tr>
            <td colspan="7"><h3 style="margin-bottom:5px;"><asp:Literal ID="msg_Custom" runat="server" Text=""/></h3></td>
        </tr>
        <tr>
            <td><asp:Literal ID="msg_From" runat="server" Text=""/>:&nbsp;</td>
            <td><dx:ASPxDateEdit ID="dteFrom" runat="server" DisplayFormatString="dd/MM/yyyy"  EditFormatString="dd/MM/yyyy" Width="120px"></dx:ASPxDateEdit></td>
            <td>&nbsp;&nbsp;&nbsp;</td>
            <td><asp:Literal ID="msg_To" runat="server" Text=""/>:&nbsp;</td>
            <td><dx:ASPxDateEdit ID="dteTo" runat="server" DisplayFormatString="dd/MM/yyyy"  EditFormatString="dd/MM/yyyy" Width="120px"></dx:ASPxDateEdit></td>
            <td>&nbsp;&nbsp;&nbsp;</td>
            <td><asp:Button ID="btnCustom" runat="server" Text="Submit" /></td>
        </tr>
    </table>
</div>

<div style="margin-top:20px;">
    <div style="border:1px solid #C1C1C3;width:708px;">
        <div style="border:10px solid #FFFFFF;background-color:#F5F5F5;padding:30px;">

            <asp:Panel ID="pnlStart" runat="server" EnableViewState="false">
                <div style="text-align:center;"><asp:Label ID="msg_Start" runat="server" Text=""></asp:Label></div>
            </asp:Panel>

            <asp:Panel ID="pnlResults" runat="server" EnableViewState="false" Visible="false">
                <div style="margin-bottom:20px;"><asp:Label ID="msg_ResultsDates" runat="server" Text=""></asp:Label></div>
            </asp:Panel>

            <asp:Panel ID="pnlLikes" runat="server" EnableViewState="false" Visible="false">
                <h2 style="margin-top:0px"><asp:Label ID="msg_LikesTitle" runat="server" Text=""></asp:Label></h2>
                <div><asp:Label ID="msg_LikesSent" runat="server" Text=""></asp:Label></div>
                <div><asp:Label ID="msg_LikesReceived" runat="server" Text=""></asp:Label></div>
            </asp:Panel>

            <asp:Panel ID="pnlMessages" runat="server" EnableViewState="false" Visible="false">
                <h2><asp:Label ID="msg_MessagesTitle" runat="server" Text=""></asp:Label></h2>
                <div><asp:Label ID="msg_MessagesSent" runat="server" Text=""></asp:Label></div>
                <div><asp:Label ID="msg_MessagesReceived" runat="server" Text=""></asp:Label></div>
            </asp:Panel>
        </div>
    </div>
</div>

        </ContentTemplate>
    </asp:UpdatePanel>
</div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cntBodyBottom" runat="server">
</asp:Content>

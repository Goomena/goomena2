﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Members/Members2.Master" CodeBehind="CouponsList.aspx.vb" Inherits="Dating.Server.Site.Web.CouponsList" %>




<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../v1/CSS/GiftsStyle.css?v4" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="content" runat="server">
    <div id="OrderList">


    <div class="list-title">
        <asp:Label ID="lblTitle" runat="server" Text="Label"></asp:Label>
    </div>
    <div class="rptContainerOrders">
        <div class="HeaderContainer">
            <div class="lfloat">
                <div class="Outer">
                    <div class="middle">
                        <div class="GiftCard">
                            <asp:Label ID="lblHGiftCard" runat="server" Text="GiftCard"></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="lfloat">
                <div class="Outer">
                    <div class="middle">
                        <div class="Points">
                            <asp:Label ID="lblHPoints" runat="server" Text="Points"></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="lfloat">
                <div class="Outer">
                    <div class="middle">
                        <div class="Date">
                            <asp:Label ID="lblHDate" runat="server" Text="Date"></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="lfloat">
                <div class="Outer">
                    <div class="middle">
                        <div class="Status">
                            <asp:Label ID="lblHStatus" runat="server" Text="Status"></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
        <asp:Repeater ID="rptOrders" runat="server">
            <ItemTemplate>
                <div class="OrderContainer">
                    <div class="lfloat">
                        <div class="Outer">
                            <div class="middle">
                                <div class="GiftCard">
                                    <asp:Label ID="lblGiftCard" runat="server" Text='<%# Me.GetGiftcard(Eval("Title"), Eval("Amount"))%>'></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="lfloat">
                        <div class="Outer">
                            <div class="middle">
                                <div class="Points">
                                    <asp:Label ID="lblPoints" runat="server" Text='<%# CInt(Eval("Points")).ToString(0)%>'></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="lfloat">
                        <div class="Outer">
                            <div class="middle">
                                <div class="Date">
                                    <asp:Label ID="lblDate" runat="server" Text='<%# GetDatetime(Eval("DateTimeCreated"))%>'></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="lfloat">
                        <div class="Outer">
                            <div class="middle">
                                <div class="Status">
                                    <asp:Label ID="Status" runat="server" Text='<%#GetStatus(Eval("StatusId"))%>'></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>

            </ItemTemplate>
        </asp:Repeater>
    </div>
    <div class="lnkBackTOCouponContainer">
        <asp:HyperLink ID="lnkBackTOCoupon" runat="server">
            <asp:Label ID="lblBackTOCoupon" runat="server" Text=""></asp:Label></asp:HyperLink>
    </div>
        </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cntBodyBottom" runat="server">
</asp:Content>

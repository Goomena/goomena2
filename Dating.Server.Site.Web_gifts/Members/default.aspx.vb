﻿'Imports Sites.SharedSiteCode
Imports Library.Public
Imports Dating.Server.Core.DLL
Imports System.Data.SqlClient

Public Class _default1
    Inherits BasePage


    Const __TESTING__ As Boolean = False


    'Dim _pageData As clsPageData
    'Protected ReadOnly Property CurrentPageData As clsPageData
    '    Get
    '        If (_pageData Is Nothing) Then
    '            _pageData = New clsPageData(Context)
    '            AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
    '        End If
    '        Return _pageData
    '    End Get
    'End Property


    Protected Overrides Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        SetMasterPage(CurrentPageData)

        Try
            If clsCurrentContext.VerifyLogin() = True Then
                Me.MasterPageFile = "~/Members/Members2.Master"
                'If (Request("master") = "inner") Then
                '    Me.MasterPageFile = "~/Members/Members2Inner.Master"
                'Else
                'End If
            Else
                clsCurrentContext.ClearSession(Session)
                If (Not Me.IsCallback) Then
                    Response.Redirect(ResolveUrl("~/Login.aspx"), False)
                End If
                HttpContext.Current.ApplicationInstance.CompleteRequest()
            End If
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try

            If (clsCurrentContext.VerifyLogin() = False) Then
                FormsAuthentication.SignOut()
                Response.Redirect(Page.ResolveUrl("~/Login.aspx"))
            End If


            'Try
            '    Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            '    AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
            'Catch ex As Exception
            '    WebErrorMessageBox(Me, ex, "Page_Load")
            'End Try

            'If (Session("RedirectedLoveHate") Is Nothing) Then
            '    Session("RedirectedLoveHate") = True
            '    Response.Redirect("LoveHate.aspx")
            'End If

            If (Not Me.IsPostBack) Then
                LoadLAG()
                Me._LoadData()
                '    ucPopupNewTerms.ShowPopUp()
            End If

        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try
    End Sub


    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        '   hdrWelcome.Attributes("class") = If(Me.IsFemale, "welcome-msg female", "welcome-msg")
        Try
            Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub



    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
        _LoadData()
        UpdateUserControls(Me)

    End Sub


    Protected Sub LoadLAG()
        Try
            If (CurrentPageData.LagID <> GetLag()) Then
                Me._pageData = Nothing
            End If

            '   Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            lnkUploadPhoto.Text = CurrentPageData.GetCustomString(lnkUploadPhoto.ID)
            lnkUploadPhoto.NavigateUrl = CurrentPageData.GetCustomString("lnkUploadPhoto.NavigateUrl")
            'lblWelcomeMsg.Text = CurrentPageData.GetCustomString("Welcome.Message.Male")



            h3NearestOptionsPopup.Text = CurrentPageData.GetCustomString("lblViewingPreferences_ND")
            h3ProfilesYouHaveViewedOptionsPopup.Text = CurrentPageData.GetCustomString("lblViewingPreferences_ND")
            nearestPopupSaveButton.Text = CurrentPageData.GetCustomString("lblSaveSettings_ND")
            profilesYouHaveViewedPopupSaveButton.Text = CurrentPageData.GetCustomString("lblSaveSettings_ND")
            nearestOptionsNearestMembersLabel.Text = CurrentPageData.GetCustomString("lblMembersNear_ND")
            nearestOptionsNewMembersLabel.Text = CurrentPageData.GetCustomString("lblNewMembers_ND")
            profilesYouHaveViewedLabel.Text = CurrentPageData.GetCustomString("lnkMyViewedList")
            lblProfilesHaveBirthday.Text = CurrentPageData.GetCustomString("lblProfilesHaveBirthday")
            whoViewedMeLabel.Text = CurrentPageData.GetCustomString("lnkWhoViewedMe")
            myFavoriteListLabel.Text = CurrentPageData.GetCustomString("lnkMyFavoriteList")

            Dim nearestMembersSelectedOption As Integer = Core.DLL.clsProfilesPrivacySettings.GET_NearestMembersOptions(Me.MasterProfileId)
            setNearestMembersContent(nearestMembersSelectedOption)

            Dim profilesViewedSelectedOption As Integer = Core.DLL.clsProfilesPrivacySettings.GET_ProfilesViewedOptions(Me.MasterProfileId)
            setProfilesViewedContent(profilesViewedSelectedOption)



            'lnkHowWorksMale.Text = CurrentPageData.GetCustomString("lnkHowWorks")
            'lnkHowWorksFemale.Text = CurrentPageData.GetCustomString("lnkHowWorks")

            If (Me.IsFemale()) Then
                'lblDashboardTop1.Text = CurrentPageData.GetCustomString("FEMALE_lblDashboardTop1")
                'lblDashboardTop2.Text = CurrentPageData.GetCustomString("FEMALE_lblDashboardTop2")

                'lblImportantAnnouncement.Text = CurrentPageData.GetCustomString("lblImportantAnnouncement_Female")
                lblImportantAdvice.Text = CurrentPageData.GetCustomString("lblImportantAdvice_Female_ND")
                ''lblTestemonialsText.Text = CurrentPageData.GetCustomString("lblTestemonialsText_Female")

                'liHowWorksMale.Visible = False
                'liHowWorksFemale.Visible = True
            Else
                'lblDashboardTop1.Text = CurrentPageData.GetCustomString("MALE_lblDashboardTop1")
                'lblDashboardTop2.Text = CurrentPageData.GetCustomString("MALE_lblDashboardTop2")

                'lblImportantAnnouncement.Text = CurrentPageData.GetCustomString("lblImportantAnnouncement_Male")
                lblImportantAdvice.Text = CurrentPageData.GetCustomString("lblImportantAdvice_Male_ND")
                ''lblTestemonialsText.Text = CurrentPageData.GetCustomString("lblTestemonialsText_Male")

                'liHowWorksMale.Visible = True
                'liHowWorksFemale.Visible = False
            End If


            lnkSearch.Text = CurrentPageData.GetCustomString("lnkSearch_ND")
            lnkAddPhotos.Text = CurrentPageData.GetCustomString("lnkAddPhotos")
            lnkWhoViewedMe.Text = CurrentPageData.GetCustomString("lnkWhoViewedMe")
            lnkWhoFavoritedMe.Text = CurrentPageData.GetCustomString("lnkWhoFavoritedMe")
            lnkMyFavoriteList.Text = CurrentPageData.GetCustomString("lnkMyFavoriteList")
            lnkMyBlockedList.Text = CurrentPageData.GetCustomString("lnkMyBlockedList")
            lnkMyViewedList.Text = CurrentPageData.GetCustomString("lnkMyViewedList")
            lnkNotifications.Text = CurrentPageData.GetCustomString("lnkNotifications")
            lnkHowWorks.Text = CurrentPageData.GetCustomString("lnkHowWorks")

            lblUpdateProfileData.Text = CurrentPageData.GetCustomString("lblUpdateProfileData")
            lnkEditProfile.Text = CurrentPageData.GetCustomString("lnkEditProfile")

            SetControlsValue(Me, CurrentPageData)

            lblItemsName.Text = Me.CurrentPageData.GetCustomString("CartDefaultView")
            lnkViewDescription.Text = Me.CurrentPageData.GetCustomString("CartDefaultWhatIsIt")
            lnkViewDescription.OnClientClick = OnMoreInfoClickFunc(
                lnkViewDescription.OnClientClick,
                ResolveUrl("~/Members/InfoWin.aspx?info=dashboard"),
                Me.CurrentPageData.GetCustomString("CartDefaultView"))
            WhatIsIt1.PopupControl.HeaderText = Me.CurrentPageData.GetCustomString("CartDefaultView")


            If (Me.IsHTTPS) Then
                Select Case Me.GetLag()
                    Case "GR"
                        iMobileBanner.Src = Me.Request.Url.Scheme & "://cdn.goomena.com/Images/MobileBanners/goomena-androind-gr.png"
                    Case "TR"
                        iMobileBanner.Src = Me.Request.Url.Scheme & "://cdn.goomena.com/Images/MobileBanners/goomena-androind-tu.png"
                    Case Else
                        iMobileBanner.Src = Me.Request.Url.Scheme & "://cdn.goomena.com/Images/MobileBanners/goomena-androind-en.png"
                End Select

            Else
                Select Case Me.GetLag()
                    Case "GR"
                        iMobileBanner.Src = Me.Request.Url.Scheme & "://cdn.goomena.com/Images/MobileBanners/goomena-androind-gr.png"
                    Case "TR"
                        iMobileBanner.Src = Me.Request.Url.Scheme & "://cdn.goomena.com/Images/MobileBanners/goomena-androind-tu.png"
                    Case Else
                        iMobileBanner.Src = Me.Request.Url.Scheme & "://cdn.goomena.com/Images/MobileBanners/goomena-androind-en.png"
                End Select

            End If
           

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub



    Private Sub _LoadData()
        Try
            If (Me.SessionVariables.MemberData.Status = ProfileStatusEnum.LIMITED) Then
                lblWelcome.Text = CurrentPageData.VerifyCustomString("lblWelcome_Limited_ND")
                lblWelcome.Text = lblWelcome.Text.Replace("###LOGINNAME###", Me.SessionVariables.MemberData.LoginName)
                If (hdrWelcome.Attributes("class") IsNot Nothing) Then
                    hdrWelcome.Attributes("class") = hdrWelcome.Attributes("class") & " limited"

                Else
                    hdrWelcome.Attributes.Add("class", "limited")
                End If
            Else
                If (Me.IsFemale) Then
                    lblWelcome.Text = CurrentPageData.GetCustomString("lblWelcome_FEMALE_ND")
                Else
                    lblWelcome.Text = CurrentPageData.GetCustomString("lblWelcome_MALE_ND")
                End If
                lblWelcome.Text = lblWelcome.Text.Replace("###LOGINNAME###", "<span class='lblWelcomeLoginName'>" & Me.SessionVariables.MemberData.LoginName & "</span>")
            End If

            'btnSearch.Text = CurrentPageData.GetCustomString("btnSearch")
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

        Try

            ''Dim _defaultPhoto = DataHelpers.GetProfilesDefaultPhoto(Me.MirrorProfileId)
            'Dim AgeIssue = (Me.GetCurrentProfile().Birthday IsNot Nothing AndAlso ProfileHelper.GetCurrentAge(Me.GetCurrentProfile().Birthday) < 18)
            'If AgeIssue Then
            '    divAgeIssue.Visible = True
            '    lblAgeIssueText.Text = CurrentPageData.GetCustomString("lblAgeIssueText")

            '    divUploadPhoto.Visible = False
            '    'divTestemonials.Visible = False
            'Else
            '    divAgeIssue.Visible = False
            '    lblAgeIssueText.Text = ""

            Dim currentUserHasPhotos As Boolean = clsUserDoes.HasPhotos(Me.MasterProfileId, Me.MirrorProfileId)
            If (Not currentUserHasPhotos) Then
                divUploadPhoto.Visible = True
                'divTestemonials.Visible = False
            Else
                divUploadPhoto.Visible = False
                'divTestemonials.Visible = True
            End If

            'End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub






    Protected Sub lvWhomIViewed_DataBound(sender As Object, e As EventArgs) Handles lvWhomIViewed.DataBound
        Try
            pnlProfilesYouHaveViewed.Visible = lvWhomIViewed.Items.Count > 0

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Protected Sub lvNearest_DataBound(sender As Object, e As EventArgs) Handles lvNearest.DataBound
        Try
            divUpdateProfileData.Visible = (String.IsNullOrEmpty(Me.GetCurrentProfile().City) OrElse String.IsNullOrEmpty(Me.GetCurrentProfile().Region))
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Protected Shared Function GetOnClickJS(LoginName As String) As String
        Dim fn As String = "jsLoad(""" & System.Web.VirtualPathUtility.ToAbsolute("~/Members/Profile.aspx?p=") & HttpUtility.UrlEncode(LoginName) & """)"
        Return fn
    End Function


    Protected Sub setNearestMembersContent(selectedOption As Integer)

        Try
            If (Me.GetCurrentProfile(True) IsNot Nothing) Then

            

                If selectedOption = NearestMembersOptions.NearestMembers Then
                    Dim prms As New clsSearchHelperParameters()
                    prms.CurrentProfileId = Me.MasterProfileId
                    prms.ReturnRecordsWithStatus = ProfileStatusEnum.Approved
                    prms.zipstr = Me.SessionVariables.MemberData.Zip
                    prms.latitudeIn = Me.SessionVariables.MemberData.latitude
                    prms.longitudeIn = Me.SessionVariables.MemberData.longitude
                    prms.Distance = 5
                    prms.NumberOfRecordsToReturn = 8

                    lvNearest.DataSource = clsSearchHelper.GetMembersToSearchDataTable_MembersNear(prms)
                    lvNearest.DataBind()

                    nearestOptionsNearestMembersRadioButton.Checked = True
                    lnkMembersNear.Text = nearestOptionsNearestMembersLabel.Text
                    lnkMembersNearAll.Text = CurrentPageData.GetCustomString("lnkMembersNearAll_ND")
                    lnkMembersNearAll.NavigateUrl = "~/Members/Search.aspx?vw=NEAREST"

                Else
                    Dim params As New clsSearchHelperParameters()
                    params.CurrentProfileId = Me.MasterProfileId
                    params.ReturnRecordsWithStatus = ProfileStatusEnum.Approved

                    If selectedOption = NearestMembersOptions.NearestMembers Then
                        'Nearest members
                        params.SearchSort = SearchSortEnum.NearestDistance

                    Else
                        'Newest membes
                        params.SearchSort = SearchSortEnum.NewestMember

                    End If

                    params.zipstr = Me.SessionVariables.MemberData.Zip
                    params.latitudeIn = Me.SessionVariables.MemberData.latitude
                    params.longitudeIn = Me.SessionVariables.MemberData.longitude
                    params.Distance = 1000
                    params.LookingFor_ToMeetMaleID = Me.SessionVariables.MemberData.LookingFor_ToMeetMaleID
                    params.LookingFor_ToMeetFemaleID = Me.SessionVariables.MemberData.LookingFor_ToMeetFemaleID
                    params.NumberOfRecordsToReturn = 8
                    params.rowNumberMin = 0
                    params.rowNumberMax = params.NumberOfRecordsToReturn
                    params.AdditionalWhereClause = "and not exists(select * from EUS_ProfilesViewed where FromProfileID=@CurrentProfileId and ToProfileID=EUS_Profiles.ProfileID)"
                    params.performCount = False
                    lvNearest.DataSource = clsSearchHelper.GetMembersToSearchDataTable(params)
                    lvNearest.DataBind()

                    nearestOptionsNewMembersRadioButton.Checked = True
                    lnkMembersNear.Text = nearestOptionsNewMembersLabel.Text
                    lnkMembersNearAll.Text = CurrentPageData.GetCustomString("lnkMembersNewAll_ND")
                    lnkMembersNearAll.NavigateUrl = "~/Members/Search.aspx?vw=NEWEST"
                End If

            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Protected Sub setProfilesViewedContent(selectedOption As Integer)

        Try


            If (Me.GetCurrentProfile(True) IsNot Nothing) Then

                'Profiles you have viewed
                If selectedOption = ProfilesYouHaveViewedOptions.ProfilesYouHaveViewed Then
                    lvWhomIViewed.DataSourceID = "sdsWhomIViewed"
                    lvWhomIViewed.DataBind()

                    profilesYouHaveViewedRadioButton.Checked = True
                    lnkProfilesYouHaveViewed.Text = profilesYouHaveViewedLabel.Text
                    lnkAllProfilesYouHaveViewed.Text = CurrentPageData.GetCustomString("lnkViewedListAll_ND")
                    lnkAllProfilesYouHaveViewed.NavigateUrl = "~/Members/MyLists.aspx?vw=myviewedlist"

                Else


                    Dim countRows As Integer
                    Dim NumberOfRecordsToReturn As Integer = 5

                    Dim params As New clsMyListsHelperParameters()
                    params.CurrentProfileId = Me.MasterProfileId

                    params.sorting = MyListsSortEnum.Recent
                    params.ReturnRecordsWithStatus = ProfileStatusEnum.Approved
                    params.zipstr = Me.SessionVariables.MemberData.Zip
                    params.latitudeIn = Me.SessionVariables.MemberData.latitude
                    params.longitudeIn = Me.SessionVariables.MemberData.longitude
                    params.Distance = clsSearchHelper.DISTANCE_DEFAULT
                    params.NumberOfRecordsToReturn = NumberOfRecordsToReturn
                    params.performCount = False
                    params.rowNumberMin = 0
                    params.rowNumberMax = NumberOfRecordsToReturn

                    Dim ds As DataSet = Nothing

                    'Who viewed my profile
                    If selectedOption = ProfilesYouHaveViewedOptions.WhoViewedMyProfile Then
                        ds = clsSearchHelper.GetWhoViewedMeMembersDataTable(params)

                    ElseIf selectedOption = ProfilesYouHaveViewedOptions.MyFavoriteList Then 'My Favorite list
                        ds = clsSearchHelper.GetMyFavoriteMembersDataTable(params)

                    ElseIf selectedOption = ProfilesYouHaveViewedOptions.ProfilesHaveBirthdayToday Then
                        'list of members who have birthday today


                        Dim params2 As New clsSearchHelperParameters()
                        params2.CurrentProfileId = Me.MasterProfileId
                        params2.ReturnRecordsWithStatus = ProfileStatusEnum.Approved

                        ''Nearest members
                        'If selectedOption = NearestMembersOptions.NearestMembers Then
                        '    params2.SearchSort = SearchSortEnum.NearestDistance
                        'Else 'Newest membes
                        '    params2.SearchSort = SearchSortEnum.NewestMember
                        'End If
                        params2.SearchSort = SearchSortEnum.Birthday
                        params2.zipstr = Me.SessionVariables.MemberData.Zip
                        params2.latitudeIn = Me.SessionVariables.MemberData.latitude
                        params2.longitudeIn = Me.SessionVariables.MemberData.longitude
                        params2.Distance = 1000
                        params2.LookingFor_ToMeetMaleID = Me.SessionVariables.MemberData.LookingFor_ToMeetMaleID
                        params2.LookingFor_ToMeetFemaleID = Me.SessionVariables.MemberData.LookingFor_ToMeetFemaleID
                        params2.NumberOfRecordsToReturn = 5
                        params2.rowNumberMin = 0
                        params2.rowNumberMax = params2.NumberOfRecordsToReturn
                        params2.performCount = False

                        '   Dim fromDate = Date.UtcNow.Date.AddSeconds(-1)
                        '         Dim toDate = Date.UtcNow.Date.AddDays(1).AddSeconds(-1)
                        'params2.AdditionalWhereClause = " and  (DATEPART (dd,Birthday)=" & Date.UtcNow.Day & " and  DATEPART (mm,Birthday)=" & Date.UtcNow.Month & ")"
                        params2.AdditionalWhereClause = " and  (CelebratingBirth>=DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE())))"
                        ds = clsSearchHelper.GetMembersToSearchDataTable(params2)

                    End If

                    countRows = ds.Tables(0).Rows.Count
                    If (countRows > 0) Then
                        Dim dt As DataTable = ds.Tables(0)
                        lvWhomIViewed.DataSourceID = ""
                        lvWhomIViewed.DataSource = dt
                        lvWhomIViewed.DataBind()

                        If selectedOption = ProfilesYouHaveViewedOptions.WhoViewedMyProfile Then 'We set the new links only if there are results
                            whoViewedMeRadioButton.Checked = True
                            lnkProfilesYouHaveViewed.Text = whoViewedMeLabel.Text
                            lnkAllProfilesYouHaveViewed.Text = CurrentPageData.GetCustomString("lnkWhoViewedMeAll_ND")
                            lnkAllProfilesYouHaveViewed.NavigateUrl = "~/Members/MyLists.aspx?vw=whoviewedme"
                        ElseIf selectedOption = ProfilesYouHaveViewedOptions.MyFavoriteList Then
                            myFavoriteListRadioButton.Checked = True
                            lnkProfilesYouHaveViewed.Text = myFavoriteListLabel.Text
                            lnkAllProfilesYouHaveViewed.Text = CurrentPageData.GetCustomString("lnkMyFavoriteListAll_ND")
                            lnkAllProfilesYouHaveViewed.NavigateUrl = "~/Members/MyLists.aspx?vw=myfavoritelist"
                        Else
                            rblProfilesHaveBirthday.Checked = True
                            lnkProfilesYouHaveViewed.Text = lblProfilesHaveBirthday.Text
                            lnkAllProfilesYouHaveViewed.Text = CurrentPageData.GetCustomString("lblProfilesHaveBirthday_All")
                            lnkAllProfilesYouHaveViewed.NavigateUrl = "~/Members/search.aspx?vw=birthdaytoday"
                        End If

                    End If

                End If

            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Protected Sub nearestPopupSaveButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) _
        Handles nearestPopupSaveButton.Click

        Try

            Dim selectedOption As Integer
            ScriptManager.RegisterClientScriptBlock(Me.Page, GetType(String), "configurePopup1", "configurePopup()", True)

            If (Me.GetCurrentProfile(True) IsNot Nothing) Then
                'If the first Radio Button is Checked (Nearest members)
                If nearestOptionsNearestMembersRadioButton.Checked Then
                    selectedOption = NearestMembersOptions.NearestMembers
                Else 'If the second Radio Button is Checked (New members)
                    selectedOption = NearestMembersOptions.NewMembers
                End If

                'set the content of the NearestMembers DIV, based on the selected setting
                setNearestMembersContent(selectedOption)

                'save setting to database
                Core.DLL.clsProfilesPrivacySettings.Update_NearestMembersOptions(Me.MasterProfileId, selectedOption)
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Protected Sub profilesYouHaveViewedPopupSaveButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) _
        Handles profilesYouHaveViewedPopupSaveButton.Click

        Try

            Dim selectedOption As Integer
            ScriptManager.RegisterClientScriptBlock(Me.Page, GetType(String), "configurePopup2", "configurePopup()", True)

            If (Me.GetCurrentProfile(True) IsNot Nothing) Then
                'If the first Radio Button is Checked (Profiles you have viewed)
                If profilesYouHaveViewedRadioButton.Checked Then
                    selectedOption = ProfilesYouHaveViewedOptions.ProfilesYouHaveViewed

                ElseIf whoViewedMeRadioButton.Checked Then 'If the second Radio Button is Checked (Who viewed my profile)
                    selectedOption = ProfilesYouHaveViewedOptions.WhoViewedMyProfile

                ElseIf myFavoriteListRadioButton.Checked Then 'If the third Radio Button is Checked (My Favorite list)
                    selectedOption = ProfilesYouHaveViewedOptions.MyFavoriteList

                ElseIf rblProfilesHaveBirthday.Checked Then
                    selectedOption = ProfilesYouHaveViewedOptions.ProfilesHaveBirthdayToday

                End If

                'set the content of the ProfilesYouHaveViewed DIV, based on the selected setting
                setProfilesViewedContent(selectedOption)

                'save setting to database
                Core.DLL.clsProfilesPrivacySettings.Update_ProfilesViewedOptions(Me.MasterProfileId, selectedOption)
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


 
End Class
﻿Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Core.DLL

Public Class BaseMasterPageMembers
    Inherits BaseMasterPage
    Implements ILanguageDependableContent



    Dim _RequestedLAGID As String
    Protected ReadOnly Property RequestedLAGID As String
        Get
            If (_RequestedLAGID Is Nothing) Then
                _RequestedLAGID = ""
                If (Not String.IsNullOrEmpty(Request.QueryString("lag"))) Then
                    _RequestedLAGID = Request.QueryString("lag")
                    If (clsLanguageHelper.IsSupportedLAG(_RequestedLAGID, Session("GEO_COUNTRY_CODE"))) Then
                        _RequestedLAGID = _RequestedLAGID.ToUpper()
                    Else
                        _RequestedLAGID = ""
                    End If
                End If
            End If
            Return _RequestedLAGID
        End Get
    End Property

    Private __isReferrer As Boolean?
    Protected ReadOnly Property IsReferrer As Boolean
        Get
            If (__isReferrer Is Nothing) Then
                __isReferrer = (Me.GetCurrentProfile().ReferrerParentId.HasValue AndAlso Me.GetCurrentProfile().ReferrerParentId > 0)
            End If
            Return __isReferrer
        End Get
    End Property



    Dim _mineMasterProfile As EUS_Profile
    Protected Function GetCurrentMasterProfile() As EUS_Profile

        If (_mineMasterProfile Is Nothing) AndAlso Me.MasterProfileId > 0 Then
            Using cmdb As CMSDBDataContext = Me.GetCMSDBDataContext
                _mineMasterProfile = cmdb.EUS_Profiles.Where(Function(itm) itm.ProfileID = Me.MasterProfileId AndAlso itm.IsMaster = True).SingleOrDefault()
            End Using

        End If

        Return _mineMasterProfile
    End Function


    Dim _mineMirrorProfile As EUS_Profile
    Protected Function GetCurrentMirrorProfile() As EUS_Profile

        If (_mineMirrorProfile Is Nothing) AndAlso Me.MirrorProfileId > 0 Then
            Using cmdb As CMSDBDataContext = Me.GetCMSDBDataContext
                _mineMirrorProfile = cmdb.EUS_Profiles.Where(Function(itm) itm.ProfileID = Me.MirrorProfileId AndAlso itm.IsMaster = False).SingleOrDefault()
            End Using

        End If

        Return _mineMirrorProfile
    End Function


    Dim _currentProfile As EUS_Profile
    Protected Function GetCurrentProfile(Optional ByVal CheckOnly As Boolean = False) As EUS_Profile
        Try

            If (_currentProfile Is Nothing) Then
                _currentProfile = Me.GetCurrentMasterProfile()
                If (_currentProfile Is Nothing) Then
                    _currentProfile = Me.GetCurrentMirrorProfile()
                End If

                ' not approved member
                If (_currentProfile Is Nothing) Then
                    Using cmdb As CMSDBDataContext = Me.GetCMSDBDataContext
                        _currentProfile = cmdb.EUS_Profiles.Where(Function(itm) itm.ProfileID = Me.MasterProfileId AndAlso itm.IsMaster = False).SingleOrDefault()
                    End Using

                End If
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        If (_currentProfile Is Nothing AndAlso Not CheckOnly) Then Throw New ProfileNotFoundException()
        Return _currentProfile
    End Function


    Public Overridable Sub Master_LanguageChanged() Implements ILanguageDependableContent.Master_LanguageChanged

    End Sub


End Class

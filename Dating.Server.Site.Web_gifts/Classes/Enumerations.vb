﻿

Public Enum WhatToSearchEnum
    None = 0
    ByFilters = 1
    ByUserName = 2
    BirthdayToday = 4
End Enum


Public Enum PageScopeEnum
    None = 0
    PublicPage = 1
    MembersPage = 2
    OteherFolderContent = 256
End Enum



Public Enum LogginFailReasonEnum
    None = 0
    UserNameOrPasswordInvalid = 1
    UserDeleted = 2
    OtherErrorOcurredRetry = 4
    UserNameEmpty = 8
    PasswordEmpty = 16
End Enum


Public Enum MemberConnectDescriptionEnum
    None = 0
    MemberConnect_Recently = 1
    MemberConnect_Today = 2
    MemberConnect_Yesterday = 4
    MemberConnect_2DaysAgo = 8
    MemberConnect_ThisWeek = 16
    MemberConnect_PreviousWeek = 20
    MemberConnect_PreviousFortnight = 21
    MemberConnect_MoreThenMonth = 22
    MemberConnect_MoreThen3Moths = 23
    MemberConnect_MoreThen6Moths = 24
End Enum

Public Enum MessageDateDescriptionEnum
    None = 0
    MessageDate_Now = 1
    MessageDate_AnHourAgo = 2
    MessageDate_HoursAgo = 4
    MessageDate_Yesterday = 8
    MessageDate_2DaysAgo = 16
    MessageDate_ShowDate = 20
End Enum



Public Enum ActionForItemsEnum
    None = 0
    TakeItemsWithGreaterSortNumber = 1
    SkipItemsWithGreaterSortNumberAndTakeRemainingItems = 2
End Enum


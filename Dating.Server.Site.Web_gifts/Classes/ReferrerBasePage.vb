Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Collections.Generic
Imports System.Configuration
Imports System.IO
Imports System.Xml
Imports System.Xml.Schema
Imports System.Globalization
Imports System.Text
Imports System.Text.RegularExpressions
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Reflection
Imports DevExpress.Web.ASPxHeadline
Imports DevExpress.Web.ASPxSiteMapControl
Imports DevExpress.Web.ASPxClasses
Imports DevExpress.Web.ASPxClasses.Internal
Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Core.DLL


Partial Public Class ReferrerBasePage
    Inherits BasePage





    Protected Overridable Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        Response.StatusCode = 400
        Response.Redirect(ResolveUrl("~/ErrorPages/FileNotFound.aspx"))
    End Sub


    Private _referrerCommissions As clsReferrerCommissions
    Protected Function GetCurrentReferrerCommissions() As clsReferrerCommissions
        If (_referrerCommissions Is Nothing) Then
            _referrerCommissions = New clsReferrerCommissions()
            _referrerCommissions.Load(Me.MasterProfileId)
        End If
        Return _referrerCommissions
    End Function


    Private affCredits As clsReferrerCredits
    Protected Overridable Function GetReferrerCreditsObject(dateFrom As DateTime?, dateTo As DateTime?)
        If (affCredits Is Nothing) Then

            Try
                'If (SessionVariables.ReferrerCredits IsNot Nothing) Then
                '    affCredits = SessionVariables.ReferrerCredits


                '    If (affCredits.DateCreated.AddHours(1) < Date.UtcNow) Then
                '        ' if time expired than clear object
                '        affCredits = Nothing

                '    ElseIf (affCredits.DateFrom <> dateFrom OrElse affCredits.DateTo <> dateTo) Then
                '        ' if dates range is different than clear object
                '        affCredits = Nothing

                '    End If
                'End If


                If (affCredits Is Nothing) Then
                    affCredits = New clsReferrerCredits(Me.MasterProfileId, Me.SessionVariables.MemberData.LoginName, dateFrom, dateTo)
                    affCredits.Load()
                    SessionVariables.ReferrerCredits = affCredits
                End If

            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try
        End If

        Return affCredits
    End Function





    Public Function GetReferrerBalance(DateFrom As DateTime?, DateTo As DateTime?, ByVal ReferrerID As Integer?, ShowByPeriod As String) As DataView
        If (DateFrom Is Nothing OrElse DateTo Is Nothing) Then
            DateFrom = Nothing
            DateTo = Nothing
        Else
            DateFrom = DateFrom.Value.Date.AddSeconds(-1)
        End If

        Dim moneyTotal As Double
        Using dtPyramid As New DSCustom.ReferrerBalanceDataTable()



            ' summary credits till the from date
            affCredits = New clsReferrerCredits(Me.MasterProfileId, Me.SessionVariables.MemberData.LoginName, Nothing, DateFrom)
            affCredits.Load()

            Try
                'moneyTotal = affCredits.MoneyTotal

                Dim _dr As DSCustom.ReferrerBalanceRow = dtPyramid.NewReferrerBalanceRow()
                _dr.Amount = affCredits.MoneyTotal
                '_dr.AmountSum = affCredits.MoneyTotal
                _dr.Currency = "&euro;"
                _dr.Description = "Balance To " & DateFrom.Value.Date.ToString("dd/MM/yyyy")
                _dr.Date_Time = DateFrom.Value.Date
                dtPyramid.Rows.Add(_dr)

            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try

            Dim cnt As Integer

            If (ShowByPeriod = "MONTH") Then

                Dim dtPerMonthStatistics As DSCustom.ReferrersStatisticsDataTable = GetCreditsStatisticsPerMonth(DateFrom, DateTo, Nothing, affCredits)
                For cnt = 0 To dtPerMonthStatistics.Rows.Count - 1
                    Try
                        Dim row As DSCustom.ReferrersStatisticsRow = dtPerMonthStatistics.Rows(cnt)
                        Dim _dr As DSCustom.ReferrerBalanceRow = dtPyramid.NewReferrerBalanceRow()

                        Dim insertIndex As Integer = 0

                        _dr.Amount = row.MoneyLevelsSum
                        '_dr.AmountSum = moneyTotal
                        _dr.Currency = "&euro;"
                        _dr.Description = "Monthly Commissions All Levels"
                        _dr.Date_Time = row.DateTimeCreated
                        dtPyramid.Rows.InsertAt(_dr, insertIndex)

                    Catch ex As Exception
                        WebErrorMessageBox(Me, ex, "")
                    End Try
                Next

            Else

                Dim dtPerDayStatistics As DSCustom.ReferrersStatisticsDataTable = GetCreditsStatisticsPerDate(DateFrom, DateTo, Nothing, affCredits)
                For cnt = 0 To dtPerDayStatistics.Rows.Count - 1
                    Try
                        Dim row As DSCustom.ReferrersStatisticsRow = dtPerDayStatistics.Rows(cnt)
                        Dim _dr As DSCustom.ReferrerBalanceRow = dtPyramid.NewReferrerBalanceRow()

                        Dim insertIndex As Integer = 0

                        _dr.Amount = row.MoneyLevelsSum
                        '_dr.AmountSum = moneyTotal
                        _dr.Currency = "&euro;"
                        _dr.Description = "Daily Commissions All Levels"
                        _dr.Date_Time = row.DateTimeCreated
                        dtPyramid.Rows.InsertAt(_dr, insertIndex)

                    Catch ex As Exception
                        WebErrorMessageBox(Me, ex, "")
                    End Try
                Next

            End If


            Dim dtTransactions As DSReferrers.GetReferrersTransactionsDataTable = GetReferrersTransactions(DateFrom, DateTo, ReferrerID)

            For cnt = 0 To dtTransactions.Rows.Count - 1
                Try
                    Dim row As DSReferrers.GetReferrersTransactionsRow = dtTransactions.Rows(cnt)
                    Dim _dr As DSCustom.ReferrerBalanceRow = dtPyramid.NewReferrerBalanceRow()


                    Dim insertIndex As Integer = 0
                    Dim description As String = "Payment to [PAYMENT_PROVIDER] [TRANSACTION_INFO]"
                    description = description.Replace("[PAYMENT_PROVIDER]", row.PaymentProvider)
                    description = description.Replace("[TRANSACTION_INFO]", row.TransactionInfo)

                    _dr.Amount = (-row.DebitAmount)
                    '_dr.AmountSum = moneyTotal
                    _dr.Currency = "&euro;"
                    _dr.Description = description
                    _dr.Date_Time = row.Date_Time
                    dtPyramid.Rows.InsertAt(_dr, insertIndex)

                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "")
                End Try
            Next

            ' sort views
            Dim View As DataView = dtPyramid.DefaultView
            View.Sort = "Date_Time DESC"

            Using ReturnTable As New DSCustom.ReferrerBalanceDataTable


                For i As Int32 = 0 To View.Count - 1
                    ReturnTable.Rows.Add(View(i).Row.ItemArray)
                Next


                For cnt = ReturnTable.Rows.Count - 1 To cnt = 0 Step -1
                    Try
                        If (cnt > -1) Then
                            Dim row As DSCustom.ReferrerBalanceRow = ReturnTable.Rows(cnt)

                            moneyTotal = moneyTotal + row.Amount
                            row.AmountSum = moneyTotal
                        End If
                    Catch ex As Exception
                        WebErrorMessageBox(Me, ex, "")
                    End Try
                Next
                View = ReturnTable.DefaultView
                Return View
            End Using
        End Using

    End Function


    Shared Function FindRow(ByVal dt As DataTable, ByVal id As Integer) As Integer
        For i As Integer = 0 To dt.Rows.Count
            If dt.Rows(i)("Id") = id Then Return i
        Next
        Return -1
    End Function


    Public Function GetCreditsStatisticsPerDate(DateFrom As DateTime?, DateTo As DateTime?, TargetLevel As Integer?, affCredits1 As clsReferrerCredits) As DSCustom.ReferrersStatisticsDataTable
        If (TargetLevel Is Nothing) Then
            TargetLevel = -1
        End If

        If (DateFrom Is Nothing OrElse DateTo Is Nothing) Then
            DateFrom = Nothing
            DateTo = Nothing
        End If


        Using dtPyramid As New DSCustom.ReferrersStatisticsDataTable

    


        Dim sql = <sql><![CDATA[
SELECT * FROM (
    SELECT [EUS_MessageID]
		  ,[REF_IsMaster]
		  ,[REF_Status]
		  ,[ReferrerParentId]
		  ,[CustomerLoginName]
		  ,[ReferrersLoginName]
		  ,Date = [DateTimeToCreate]
		  ,[CustomerId]
		  ,[ReferrerID]
		  ,[StatusID]
		  ,[IsSent]
		  ,[IsReceived]
		  ,[CopyMessageID]
		  ,[IsDeleted]
		  ,[REF_L1_Money]
		  ,[REF_L2_Money]
		  ,[REF_L3_Money]
		  ,[REF_L4_Money]
		  ,[REF_L5_Money]
		  ,[REF_L1_Commission]
		  ,[REF_L2_Commission]
		  ,[REF_L3_Commission]
		  ,[REF_L4_Commission]
		  ,[REF_L5_Commission]
		  ,[Cost]
		  ,cc.[CustomerCreditsId]
		  ,cc.[EUS_CreditsTypeID]
		  ,cc.DateBill
		  ,ct.CreditsType
		  ,Expired = cast('false' as bit)
		  ,DatePurchased = ''
		  ,Action = ''
		  ,Description = ''
		  ,CommissionCRD = 0  		
	    FROM [vw_REF_Commission] cc
            with(nolock)
		LEFT OUTER JOIN EUS_CreditsType AS ct ON ct.EUS_CreditsTypeID = cc.EUS_CreditsTypeID 
		WHERE cc.[ReferrerID] in (@SubReferrers)
        AND (
            (@DateFrom is null AND @DateTo is null) OR 
            (DateBill >= @DateFrom AND DateBill <= @DateTo)
        )
) AS t
ORDER BY [Date] DESC
]]></sql>.Value

        Dim level As Integer = 0
        Dim mySubReferrers As New List(Of Integer)
        GetReferrerCreditsObject(DateFrom, DateTo)
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                While (level < clsReferrer.StopLevel)
                    level = level + 1

                    Try
                        mySubReferrers.Clear()

                        Dim drs As DSCustom.ReferrersRepositoryDataRow() = affCredits1.GetReferrersForLevel(level)
                        Dim c As Integer
                        For c = 0 To drs.Length - 1
                            Dim profileId As Integer = drs(c)("ProfileID")
                            mySubReferrers.Add(profileId)
                        Next

                        If (mySubReferrers.Count > 0 AndAlso (level = TargetLevel OrElse TargetLevel = -1)) Then
                            Dim sb As New StringBuilder()
                            For c = 0 To mySubReferrers.Count - 1
                                Dim _profileId As Integer = mySubReferrers(c)
                                sb.Append(_profileId & ",")
                            Next
                            If (sb.Length > 0) Then
                                sb.Remove(sb.Length - 1, 1)
                            End If

                            Dim execSql As String = sql.Replace("@SubReferrers", sb.ToString())

                            Using cmd As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(con, execSql)



                                cmd.Parameters.Add(New SqlClient.SqlParameter("@DateFrom", DateFrom))
                                cmd.Parameters.Add(New SqlClient.SqlParameter("@DateTo", DateTo))

                                Using dtCreditsInfo As DataTable = DataHelpers.GetDataTable(cmd)
                                    For c = 0 To dtCreditsInfo.Rows.Count - 1
                                        Dim dr As DataRow = dtCreditsInfo.Rows(c)

                                        Dim money As Double
                                        Select Case level
                                            Case 1
                                                If (Not dr.IsNull("REF_L1_Money")) Then money = dr("REF_L1_Money")
                                            Case 2
                                                If (Not dr.IsNull("REF_L2_Money")) Then money = dr("REF_L2_Money")
                                            Case 3
                                                If (Not dr.IsNull("REF_L3_Money")) Then money = dr("REF_L3_Money")
                                            Case 4
                                                If (Not dr.IsNull("REF_L4_Money")) Then money = dr("REF_L4_Money")
                                            Case 5
                                                If (Not dr.IsNull("REF_L5_Money")) Then money = dr("REF_L5_Money")
                                        End Select


                                        Dim _dr As DSCustom.ReferrersStatisticsRow = Nothing
                                        Dim _drs As DataRow() = dtPyramid.Select("DateTimeCreated='" & DirectCast(dr("Date"), DateTime).ToString("yyyy-MM-dd") & "'")
                                        If (_drs.Length > 0) Then
                                            _dr = _drs(0)
                                            _dr.CommissionCRDLevelsSum = _dr.CommissionCRDLevelsSum + money
                                            '_dr.CommissionCRDLevelsSum = _dr.CommissionCRDLevelsSum + GetCurrentReferrerCommissions().CalcCommissionForLevel(dr("EuroAmount"), level)
                                            _dr.MoneyLevelsSum = _dr.CommissionCRDLevelsSum
                                        Else
                                            _dr = dtPyramid.NewReferrersStatisticsRow()
                                            _dr.CommissionCRDLevelsSum = money
                                            '_dr.CommissionCRDLevelsSum = GetCurrentReferrerCommissions().CalcCommissionForLevel(dr("EuroAmount"), level)
                                            _dr.MoneyLevelsSum = _dr.CommissionCRDLevelsSum
                                            _dr.DateTimeCreated = DirectCast(dr("Date"), DateTime).Date
                                            dtPyramid.Rows.Add(_dr)
                                        End If

                                    Next

                                End Using
                            End Using


                        End If

                    Catch ex As Exception
                        WebErrorMessageBox(Me, ex, "")
                    End Try

                    If (mySubReferrers.Count = 0 OrElse level = TargetLevel) Then Exit While
                End While
            End Using
            Return dtPyramid
        End Using

    End Function


    Public Function GetCreditsStatisticsPerMonth(DateFrom As DateTime?, DateTo As DateTime?, TargetLevel As Integer?, affCredits1 As clsReferrerCredits) As DSCustom.ReferrersStatisticsDataTable
        If (TargetLevel Is Nothing) Then
            TargetLevel = -1
        End If

        If (DateFrom Is Nothing OrElse DateTo Is Nothing) Then
            DateFrom = Nothing
            DateTo = Nothing
        End If


        Using dtPyramid As New DSCustom.ReferrersStatisticsDataTable




            Dim sql = <sql><![CDATA[
SELECT * FROM (
    SELECT [EUS_MessageID]
		  ,[REF_IsMaster]
		  ,[REF_Status]
		  ,[ReferrerParentId]
		  ,[CustomerLoginName]
		  ,[ReferrersLoginName]
		  ,Date = [DateTimeToCreate]
		  ,[CustomerId]
		  ,[ReferrerID]
		  ,[StatusID]
		  ,[IsSent]
		  ,[IsReceived]
		  ,[CopyMessageID]
		  ,[IsDeleted]
		  ,[REF_L1_Money]
		  ,[REF_L2_Money]
		  ,[REF_L3_Money]
		  ,[REF_L4_Money]
		  ,[REF_L5_Money]
		  ,[REF_L1_Commission]
		  ,[REF_L2_Commission]
		  ,[REF_L3_Commission]
		  ,[REF_L4_Commission]
		  ,[REF_L5_Commission]
		  ,[Cost]
		  ,cc.[CustomerCreditsId]
		  ,cc.[EUS_CreditsTypeID]
		  ,cc.DateBill
		  ,ct.CreditsType
		  ,Expired = cast('false' as bit)
		  ,DatePurchased = ''
		  ,Action = ''
		  ,Description = ''
		  ,CommissionCRD = 0  		
	    FROM [vw_REF_Commission] cc
            with(nolock)
		LEFT OUTER JOIN EUS_CreditsType AS ct ON ct.EUS_CreditsTypeID = cc.EUS_CreditsTypeID 
		WHERE cc.[ReferrerID] in (@SubReferrers)
        AND (
            (@DateFrom is null AND @DateTo is null) OR 
            ([DateBill] >= @DateFrom AND [DateBill] <= @DateTo)
        )
) AS t
ORDER BY [Date] DESC
]]></sql>.Value

            Dim level As Integer = 0
            Dim mySubReferrers As New List(Of Integer)
            'GetReferrerCreditsObject(DateFrom, DateTo)
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection

                While (level < clsReferrer.StopLevel)
                    level = level + 1

                    Try
                        mySubReferrers.Clear()

                        Dim drs As DSCustom.ReferrersRepositoryDataRow() = affCredits1.GetReferrersForLevel(level)
                        Dim c As Integer
                        For c = 0 To drs.Length - 1
                            Dim profileId As Integer = drs(c)("ProfileID")
                            mySubReferrers.Add(profileId)
                        Next

                        If (mySubReferrers.Count > 0 AndAlso (level = TargetLevel OrElse TargetLevel = -1)) Then
                            Dim sb As New StringBuilder()
                            For c = 0 To mySubReferrers.Count - 1
                                Dim _profileId As Integer = mySubReferrers(c)
                                sb.Append(_profileId & ",")
                            Next
                            If (sb.Length > 0) Then
                                sb.Remove(sb.Length - 1, 1)
                            End If

                            Dim execSql As String = sql.Replace("@SubReferrers", sb.ToString())

                            Using cmd As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(con, execSql)
                                cmd.Parameters.Add(New SqlClient.SqlParameter("@DateFrom", DateFrom))
                                cmd.Parameters.Add(New SqlClient.SqlParameter("@DateTo", DateTo))

                                Using dtCreditsInfo As DataTable = DataHelpers.GetDataTable(cmd)
                                    For c = 0 To dtCreditsInfo.Rows.Count - 1
                                        Dim dr As DataRow = dtCreditsInfo.Rows(c)
                                        Dim currentDate As DateTime = DirectCast(dr("Date"), DateTime)
                                        Dim dateMonthStart As DateTime = New DateTime(currentDate.Year, currentDate.Month, 1)
                                        Dim dateMonthEnd As DateTime = New DateTime(currentDate.Year, currentDate.Month, 1).AddMonths(1).AddDays(-1)

                                        Dim money As Double
                                        Select Case level
                                            Case 1
                                                If (Not dr.IsNull("REF_L1_Money")) Then money = dr("REF_L1_Money")
                                            Case 2
                                                If (Not dr.IsNull("REF_L2_Money")) Then money = dr("REF_L2_Money")
                                            Case 3
                                                If (Not dr.IsNull("REF_L3_Money")) Then money = dr("REF_L3_Money")
                                            Case 4
                                                If (Not dr.IsNull("REF_L4_Money")) Then money = dr("REF_L4_Money")
                                            Case 5
                                                If (Not dr.IsNull("REF_L5_Money")) Then money = dr("REF_L5_Money")
                                        End Select


                                        Dim _dr As DSCustom.ReferrersStatisticsRow = Nothing
                                        Dim _drs As DataRow() = dtPyramid.Select("DateTimeCreated>='" & dateMonthStart.ToString("yyyy-MM-dd") & "' AND DateTimeCreated<='" & dateMonthEnd.ToString("yyyy-MM-dd") & "'")

                                        If (_drs.Length > 0) Then
                                            _dr = _drs(0)
                                            '_dr.CommissionCRDLevelsSum = _dr.CommissionCRDLevelsSum + GetCurrentReferrerCommissions().CalcCommissionForLevel(dr("EuroAmount"), level)
                                            _dr.CommissionCRDLevelsSum = _dr.CommissionCRDLevelsSum + money
                                            _dr.MoneyLevelsSum = _dr.CommissionCRDLevelsSum
                                        Else
                                            _dr = dtPyramid.NewReferrersStatisticsRow()
                                            '_dr.CommissionCRDLevelsSum = GetCurrentReferrerCommissions().CalcCommissionForLevel(dr("EuroAmount"), level)
                                            _dr.CommissionCRDLevelsSum = money
                                            _dr.MoneyLevelsSum = _dr.CommissionCRDLevelsSum
                                            _dr.DateTimeCreated = dateMonthEnd
                                            dtPyramid.Rows.Add(_dr)
                                        End If

                                    Next
                                End Using
                            End Using
                        End If

                    Catch ex As Exception
                        WebErrorMessageBox(Me, ex, "")
                    End Try

                    If (mySubReferrers.Count = 0 OrElse level = TargetLevel) Then Exit While
                End While

            End Using
            Return dtPyramid
        End Using
    End Function




    Public Function GetReferrersTransactions(ByVal DateFrom As Date?, ByVal DateTo As Date?, ByVal ReferrerID As Integer?) As DSReferrers.GetReferrersTransactionsDataTable
        Try

            Dim ds As DSReferrers = DataHelpers.GetReferrersTransactions(DateFrom, DateTo, ReferrerID)
            Return ds.GetReferrersTransactions

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
        Return Nothing
    End Function



    Public Function CreateReferrerCode() As String
        Dim login3 As String = Me.SessionVariables.MemberData.LoginName
        If (login3.Length > 3) Then
            login3 = login3.Remove(3)
        ElseIf (login3.Length = 2) Then
            login3 = login3 & "c"
        End If

        Dim code As String = ModGlobals.Referrer_CODE_PATTERN.Replace("[123]", Me.MasterProfileId).Replace("[LOGIN]", login3)
        Return code
    End Function


End Class


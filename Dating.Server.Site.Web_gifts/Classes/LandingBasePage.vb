﻿

Public Class LandingBasePage
    Inherits BasePage


    Public Sub CheckMasterPageAndRedirect(pageData As clsPageData)

        Dim isCmsPage As Boolean = (Me.GetType().BaseType Is GetType(CmsPage))
        Dim isLanding As Boolean = (Me.GetType().BaseType Is GetType(Landing))
        Dim isLanding2014 As Boolean = (Me.GetType().BaseType Is GetType(Landing2014))
        Dim newUrl As String = Request.Url.ToString()

        Dim MasterPage_DisplayName As String = pageData.cPageBasic.MasterPage_DisplayName
        If (Not String.IsNullOrEmpty(MasterPage_DisplayName)) Then
            MasterPage_DisplayName = MasterPage_DisplayName.ToLower()


            If (Not isCmsPage AndAlso MasterPage_DisplayName = "public landing") Then
                Dim url As String = Request.Url.ToString()

                If (url.IndexOf("/landing/y2014/", StringComparison.OrdinalIgnoreCase) > -1) Then
                    url = Regex.Replace(url, "/landing/y2014/", "/cmspage/", RegexOptions.IgnoreCase)
                    newUrl = url

                ElseIf (url.IndexOf("/landing/", StringComparison.OrdinalIgnoreCase) > -1) Then
                    url = Regex.Replace(url, "/landing/", "/cmspage/", RegexOptions.IgnoreCase)
                    newUrl = url

                End If


            End If


            If (isLanding) Then
                If (MasterPage_DisplayName = "landing 14, main photo-middle photos") Then

                    Dim url As String = Request.Url.ToString()
                    If (url.IndexOf("/landing/", StringComparison.OrdinalIgnoreCase) > -1 AndAlso
                        url.IndexOf("/landing/y2014/", StringComparison.OrdinalIgnoreCase) = -1) Then

                        url = Regex.Replace(url, "/landing/", "/landing/y2014/", RegexOptions.IgnoreCase)
                        newUrl = url

                    End If

                End If
            End If

            If (isLanding2014) Then
                If (MasterPage_DisplayName = "public landing with main photo" OrElse
                      MasterPage_DisplayName = "public landing 2014 with main photo") Then

                    Dim url As String = Request.Url.ToString()
                    url = Regex.Replace(url, "/landing/y2014/", "/landing/", RegexOptions.IgnoreCase)
                    newUrl = url


                End If
            End If

            If (isCmsPage) Then
                If (MasterPage_DisplayName = "public landing with main photo" OrElse
                      MasterPage_DisplayName = "public landing 2014 with main photo") Then

                    Dim url As String = Request.Url.ToString()
                    If (url.IndexOf("/cmspage/", StringComparison.OrdinalIgnoreCase) > -1) Then

                        url = Regex.Replace(url, "/cmspage/", "/landing/", RegexOptions.IgnoreCase)
                        newUrl = url

                    End If
                End If


                If (MasterPage_DisplayName = "landing 14, main photo-middle photos") Then

                    Dim url As String = Request.Url.ToString()
                    If (url.IndexOf("/cmspage/y2014/", StringComparison.OrdinalIgnoreCase) > -1) Then

                        url = Regex.Replace(url, "/cmspage/", "/landing/", RegexOptions.IgnoreCase)
                        newUrl = url

                    ElseIf (url.IndexOf("/cmspage/", StringComparison.OrdinalIgnoreCase) > -1) Then

                        url = Regex.Replace(url, "/cmspage/", "/landing/y2014/", RegexOptions.IgnoreCase)
                        newUrl = url

                    End If

                End If
            End If

        End If
        If (Request.Url.ToString() <> newUrl) Then
            Response.RedirectPermanent(newUrl)
        End If
    End Sub



End Class

﻿Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Core.DLL

Public Class clsLogin

    Public Shared Function PerfomLogin(DataRecordLogin As clsDataRecordLoginMember) As clsDataRecordLoginMemberReturn
        Dim DataRecordLoginReturn As New clsDataRecordLoginMemberReturn
        DataRecordLoginReturn.IsValid = False
        DataRecordLoginReturn.HasErrors = False

        Try
            Using ds = DataHelpers.GetEUS_Profiles_ByLoginPass(DataRecordLogin.LoginName, DataRecordLogin.Password)


                Dim EUS_ProfilesDataTable As DSMembers.EUS_ProfilesDataTable = ds.EUS_Profiles
                If EUS_ProfilesDataTable.Rows.Count > 0 Then

                    Dim updateMember As Boolean = False
                    Dim rowIndex As Integer = 0
                    For rowIndex = 0 To (EUS_ProfilesDataTable.Rows.Count - 1)
                        'While rowIndex < EUS_ProfilesDataTable.Rows.Count


                        Dim EUS_MembersRow As DSMembers.EUS_ProfilesRow = EUS_ProfilesDataTable.Rows(rowIndex)

                        If ((EUS_ProfilesDataTable.Rows.Count = 2 AndAlso EUS_MembersRow.IsMaster = True) OrElse (EUS_ProfilesDataTable.Rows.Count = 1)) Then
                            If (EUS_MembersRow.Status <> ProfileStatusEnum.Deleted) Then
                                DataRecordLoginReturn.Fill(EUS_MembersRow)

                                EUS_MembersRow.LastLoginDateTime = DateTime.UtcNow
                                EUS_MembersRow.LastLoginIP = HttpContext.Current.Request.Params("REMOTE_ADDR")
                                ' EUS_MembersRow.LastLoginGEOInfos, 
                            Else
                                DataRecordLoginReturn.HasErrors = True
                                DataRecordLoginReturn.Message = ProfileStatusEnum.Deleted.ToString()

                            End If

                            ' ElseIf (EUS_ProfilesDataTable.Rows.Count = 2 AndAlso EUS_MembersRow.IsMaster = False) Then
                        End If


                        ' allow members that have deleted their profile to relogin and activate their profile
                        If (EUS_MembersRow.Status = ProfileStatusEnum.DeletedByUser) Then
                            If (EUS_MembersRow.IsMaster = True) Then
                                EUS_MembersRow.Status = ProfileStatusEnum.Approved
                                updateMember = True
                            Else
                                EUS_MembersRow.Status = ProfileStatusEnum.Updating
                                updateMember = True
                            End If
                        End If

                        'rowIndex += 1
                        'End While
                    Next

                    If (updateMember) Then
                        DataHelpers.UpdateEUS_Profiles(ds)
                    End If

                Else
                    DataRecordLoginReturn.Message = "Error Login Or Password"
                End If

                ' log last login data
                If (DataRecordLoginReturn.ProfileID > 0 AndAlso DataRecordLogin.RecordLastLogin <> -11) Then
                    Try

                        DataHelpers.UpdateEUS_Profiles_LoginData(DataRecordLoginReturn.ProfileID,
                                                                     HttpContext.Current.Request.Params("REMOTE_ADDR"),
                                                                     HttpContext.Current.Session("GEO_COUNTRY_CODE"),
                                                                     True,
                                                                     DataRecordLogin.IsMobile)

                    Catch ex As Exception
                        WebErrorSendEmail(ex, "Updating member login data")
                    End Try

                    'Try
                    '    clsProfilesPrivacySettings.Update_PrivacySettings_ShowMeOffline(DataRecordLoginReturn.ProfileID, False)
                    'Catch ex As Exception
                    '    WebErrorSendEmail(ex, "Updating member ShowMeOffline flag")
                    'End Try

                End If
            End Using
        Catch ex As Exception
            DataRecordLoginReturn.HasErrors = True
            DataRecordLoginReturn.Message = ex.Message
       
        End Try
        Return DataRecordLoginReturn
    End Function



    Public Function PerfomSiteLogin(loginName As String, pass As String, ByVal rememberUserName As Boolean) As clsDataRecordLoginMemberReturn
        Return PerfomSiteLogin(loginName, pass, rememberUserName, 0)
    End Function



    Public Function PerfomSiteLogin(loginName As String, pass As String, ByVal rememberUserName As Boolean, recordLogin As Integer) As clsDataRecordLoginMemberReturn
        Dim DataRecordLoginReturn As New clsDataRecordLoginMemberReturn
        DataRecordLoginReturn.IsValid = False
        DataRecordLoginReturn.HasErrors = False

        Try
            '  Dim core As New clsCore
            Dim Session As System.Web.SessionState.HttpSessionState = HttpContext.Current.Session
            '     Dim Request As System.Web.HttpRequest = HttpContext.Current.Request
            Dim IsMobileAccess As Boolean? = Session("IsMobileAccess")

            Dim DataRecordLogin As New clsDataRecordLoginMember()
            DataRecordLogin.LoginName = loginName
            DataRecordLogin.Password = pass
            DataRecordLogin.RecordLastLogin = recordLogin
            DataRecordLogin.IsMobile = clsNullable.NullTo(IsMobileAccess, False)

            DataRecordLoginReturn = clsLogin.PerfomLogin(DataRecordLogin)

            If (Session("SessionVariables") Is Nothing) Then
                Session("SessionVariables") = New clsSessionVariables()
            End If

            Dim sesVars As clsSessionVariables = clsSessionVariables.GetCurrent()
            sesVars.MemberData = DataRecordLoginReturn
            Session("LagID") = DataRecordLoginReturn.LAGID
            Session("ProfileID") = DataRecordLoginReturn.ProfileID
            Session("MirrorProfileID") = DataRecordLoginReturn.MirrorProfileID
            Session("LoginName") = DataRecordLoginReturn.LoginName

            Session("StartDate") = DateTime.UtcNow
            If (Session("LagID") Is Nothing) Then clsLanguageHelper.SetLAGID()

            Try
                clsCurrentContext.GetCountryByIP()

                'Dim country As clsCountryByIP = clsCountryByIP.GetCountryCodeFromIP(Session("IP"), ConfigurationManager.ConnectionStrings("GeoDBconnectionString").ConnectionString)

                'If (Len(country.CountryCode) = 2) Then
                '    Session("GEO_COUNTRY_CODE") = country.CountryCode
                '    Session("GEO_COUNTRY_CODE_BY_SITE") = "sql server" 'ConfigurationManager.AppSettings("WSGeoServiceURL")
                '    Session("GEO_COUNTRY_LATITUDE") = country.latitude
                '    Session("GEO_COUNTRY_LONGITUDE") = country.longitude
                '    Session("GEO_COUNTRY_POSTALCODE") = country.postalCode
                '    Session("GEO_COUNTRY_CITY") = country.city
                '    Session("GEO_COUNTRY_INFO") = country
                'End If

            Catch
            End Try

            If DataRecordLoginReturn.IsValid Then

                'Create Form Authentication ticket
                Dim ticket As New FormsAuthenticationTicket(1, DataRecordLoginReturn.LoginName, DateTime.Now, DateTime.Now.AddDays(5), rememberUserName, DataRecordLoginReturn.Role, FormsAuthentication.FormsCookiePath)


                'For security reasons we may hash the cookies
                Dim hashCookies As String = FormsAuthentication.Encrypt(ticket)
                Dim cookie As New HttpCookie(FormsAuthentication.FormsCookieName, hashCookies)

                cookie.Path = FormsAuthentication.FormsCookiePath()
                If rememberUserName Then
                    cookie.Expires = ticket.Expiration
                End If

                cookie.Domain = Nothing

                'Dim CookiesDomain1 As String = ConfigurationManager.AppSettings("CookiesDomain1")
                'Dim CookiesDomain2 As String = ConfigurationManager.AppSettings("CookiesDomain2")

                'If (HttpContext.Current.Request.Url.Host.EndsWith(CookiesDomain1.TrimStart("."c))) Then
                '    cookie.Domain = CookiesDomain1
                'ElseIf (HttpContext.Current.Request.Url.Host.EndsWith(CookiesDomain2.TrimStart("."c))) Then
                '    cookie.Domain = CookiesDomain2
                'End If


                'add the cookie to user browser
                HttpContext.Current.Response.Cookies.Remove(cookie.Name)
                HttpContext.Current.Response.Cookies.Add(cookie)

                HttpContext.Current.Session("DataRecordLoginReturn") = DataRecordLoginReturn

            End If

        Catch ex As Exception
            DataRecordLoginReturn.HasErrors = True
            DataRecordLoginReturn.Message = ex.Message
        End Try


        Return DataRecordLoginReturn
    End Function


    Public Function PerfomAdminLogin(logon_customerParam As String) As clsDataRecordLoginMemberReturn

        Try
            Using dc = New CMSDBDataContext(ModGlobals.ConnectionString)
                Dim customerId As String() = logon_customerParam.Split("_"c)
                Dim name As String = (From itm In dc.EUS_Profiles
                                             Where itm.MirrorProfileID = customerId(0) AndAlso itm.ProfileID = customerId(1)
                                             Select itm.LoginName).FirstOrDefault()

                Return PerfomRelogin(name, False, -11)
            End Using
     
        Catch ex As Exception
        End Try

        Return (Nothing)
    End Function


    Public Function PerfomRelogin(loginName As String, ByVal rememberUserName As Boolean) As clsDataRecordLoginMemberReturn
        Return PerfomRelogin(loginName, rememberUserName, 0)
    End Function


    Public Function PerfomRelogin(loginName As String, ByVal rememberUserName As Boolean, recordLogin As Integer) As clsDataRecordLoginMemberReturn
        Dim DataRecordLoginReturn As New clsDataRecordLoginMemberReturn
        DataRecordLoginReturn.IsValid = False
        DataRecordLoginReturn.HasErrors = False

        Try
            Dim pass As String = Nothing
            Using dc As New CMSDBDataContext(ModGlobals.ConnectionString)



                Try
                    ' retrieve user password
                    pass = (From itm In dc.EUS_Profiles
                            Where (itm.LoginName.ToUpper() = loginName.ToUpper() OrElse _
                                   itm.eMail.ToUpper() = loginName.ToUpper()) _
                            AndAlso _
                                  (itm.Status = ProfileStatusEnum.Approved OrElse _
                                     itm.Status = ProfileStatusEnum.NewProfile OrElse _
                                     itm.Status = ProfileStatusEnum.Rejected OrElse _
                                     itm.Status = ProfileStatusEnum.LIMITED) _
                            Select itm.Password).FirstOrDefault()
                Catch ex As Exception

             
                End Try

            End Using
            DataRecordLoginReturn = Me.PerfomSiteLogin(loginName, pass, rememberUserName, recordLogin)

            Try
                Dim Request As HttpRequest = HttpContext.Current.Request
                Dim Session As HttpSessionState = HttpContext.Current.Session

                Dim LogProfileAccessID As Long = 0
                If (recordLogin = -11) Then
                    LogProfileAccessID = DataHelpers.LogProfileAccess(DataRecordLoginReturn.ProfileID, loginName, pass,
                                                 DataRecordLoginReturn.IsValid,
                                                 "Admin Login",
                                                 Nothing,
                                                 Session("lagCookie"),
                                                 Request.ServerVariables("REMOTE_ADDR"),
                                                 Session("IsMobileAccess"))
                Else
                    LogProfileAccessID = DataHelpers.LogProfileAccess(DataRecordLoginReturn.ProfileID, loginName, pass,
                                                 DataRecordLoginReturn.IsValid,
                                                 "Auto Login",
                                                 Nothing,
                                                 Session("lagCookie"),
                                                 Request.ServerVariables("REMOTE_ADDR"),
                                                 Session("IsMobileAccess"))
                End If

                If (Session("IsMobileAccess") Is Nothing) Then
                    clsCurrentContext.AddAccessID(LogProfileAccessID)
                End If
            Catch ex As Exception
            End Try


        Catch ex As Exception
            DataRecordLoginReturn.HasErrors = True
            DataRecordLoginReturn.Message = ex.Message
        End Try

        Return DataRecordLoginReturn
    End Function

    Function PerfomReloginWithTwitterId(twUserId As String, rememberUserName As Boolean) As clsDataRecordLoginMemberReturn
        Dim DataRecordLoginReturn As New clsDataRecordLoginMemberReturn
        DataRecordLoginReturn.IsValid = False
        DataRecordLoginReturn.HasErrors = False

        Using dc As New CMSDBDataContext(ModGlobals.ConnectionString)


            Try
                Dim pass = Nothing

                ' retrieve user password
                pass = (From itm In dc.EUS_Profiles
                        Where (itm.TwitterUserId.ToString() = twUserId) _
                        AndAlso _
                              (itm.Status = ProfileStatusEnum.Approved OrElse _
                                 itm.Status = ProfileStatusEnum.NewProfile OrElse _
                                 itm.Status = ProfileStatusEnum.Rejected) _
                        Select itm.LoginName, itm.Password).FirstOrDefault()


                If (pass IsNot Nothing) Then
                    DataRecordLoginReturn = Me.PerfomSiteLogin(pass.LoginName, pass.Password, rememberUserName)
                End If

            Catch ex As Exception
                DataRecordLoginReturn.HasErrors = True
                DataRecordLoginReturn.Message = ex.Message
         
            End Try
        End Using
        Return DataRecordLoginReturn
    End Function

    Function PerfomReloginWithFacebookId(fb_uid As String, fb_name As String, fb_username As String, fb_email As String, rememberUserName As Boolean) As clsDataRecordLoginMemberReturn
        Dim DataRecordLoginReturn As New clsDataRecordLoginMemberReturn
        DataRecordLoginReturn.IsValid = False
        DataRecordLoginReturn.HasErrors = False

        Try
            Dim prms As LoginWithFacebookSuccessParams = ProfileHelper.PerfomReloginWithFacebookId(fb_uid, fb_name, fb_username, fb_email)
            DataRecordLoginReturn = Me.PerfomSiteLogin(prms.LoginName, prms.Password, rememberUserName)

            If (Not DataRecordLoginReturn.HasErrors AndAlso
                DataRecordLoginReturn.IsValid AndAlso
                prms.OnLoginSuccessSaveFBData) Then

                DataHelpers.UpdateEUS_Profiles_FacebookData(DataRecordLoginReturn.ProfileID, fb_uid, fb_name, fb_username)

            End If

        Catch ex As Exception
            DataRecordLoginReturn.HasErrors = True
            DataRecordLoginReturn.Message = ex.Message
        End Try

        'Dim ph As New ProfileHelper()
        'AddHandler ph.OnLoginWithFacebookSuccess, AddressOf ProfileHelper_OnLoginWithFacebookSuccess
        'DataRecordLoginReturn = ph.PerfomReloginWithFacebookId(fb_uid, fb_name, fb_username, fb_email, rememberUserName)


        'Dim dc As New CMSDBDataContext(ModGlobals.ConnectionString)
        'Try
        '    Dim pass = Nothing

        '    ' retrieve user password
        '    pass = (From itm In dc.EUS_Profiles
        '            Where ((itm.FacebookUserId.ToString() = fb_uid) OrElse
        '                   (itm.FacebookName.ToString() = fb_name) OrElse
        '                   (itm.eMail.ToString() = fb_email) OrElse
        '                   (itm.FacebookUserName.ToString() = fb_username)) _
        '            AndAlso _
        '                  (itm.Status = ProfileStatusEnum.Approved OrElse _
        '                     itm.Status = ProfileStatusEnum.NewProfile OrElse _
        '                     itm.Status = ProfileStatusEnum.Rejected) _
        '            AndAlso itm.ProfileID > 1 _
        '            Select itm.LoginName, itm.Password, itm.eMail, itm.FacebookUserId, itm.FacebookName, itm.FacebookUserName).FirstOrDefault()


        '    If (pass IsNot Nothing) Then
        '        DataRecordLoginReturn = Me.PerfomSiteLogin(pass.LoginName, pass.Password, rememberUserName)
        '        If (Not DataRecordLoginReturn.HasErrors AndAlso
        '            DataRecordLoginReturn.IsValid AndAlso
        '            pass.eMail = fb_email AndAlso (pass.FacebookUserId <> fb_uid OrElse
        '                                           pass.FacebookName <> fb_name OrElse
        '                                           pass.FacebookUserName <> fb_username)) Then

        '            DataHelpers.UpdateEUS_Profiles_FacebookData(DataRecordLoginReturn.ProfileID, fb_uid, fb_name, fb_username)

        '        End If
        '    End If


        'Catch ex As Exception
        '    DataRecordLoginReturn.HasErrors = True
        '    DataRecordLoginReturn.Message = ex.Message
        'Finally
        '    dc.Dispose()
        'End Try

        Return DataRecordLoginReturn
    End Function

    'Sub ProfileHelper_OnLoginWithFacebookSuccess(ByRef params As LoginWithFacebookSuccessParams)
    '    params.DataRecordLoginReturn = Me.PerfomSiteLogin(params.LoginName, params.Password, params.RememberUserName)
    'End Sub


    Function PerfomReloginWithGoogleId(go_uid As String, go_name As String, go_email As String, rememberUserName As Boolean) As clsDataRecordLoginMemberReturn
        Dim DataRecordLoginReturn As New clsDataRecordLoginMemberReturn
        DataRecordLoginReturn.IsValid = False
        DataRecordLoginReturn.HasErrors = False

        Using dc As New CMSDBDataContext(ModGlobals.ConnectionString)


            Try
                Dim pass = Nothing

                ' retrieve user password

                'OrElse
                '(itm.eMail.ToString() = go_email)

                pass = (From itm In dc.EUS_Profiles
                        Where ((itm.GoogleUserId.ToString() = go_uid) OrElse
                               (itm.GoogleName.ToString() = go_name)) _
                        AndAlso _
                              (itm.Status = ProfileStatusEnum.Approved OrElse _
                                 itm.Status = ProfileStatusEnum.NewProfile OrElse _
                                 itm.Status = ProfileStatusEnum.Rejected) _
                        AndAlso itm.ProfileID > 1 _
                        Select itm.LoginName, itm.Password, itm.eMail, itm.GoogleUserId, itm.GoogleName, itm.GoogleEmail).FirstOrDefault()


                If (pass IsNot Nothing) Then
                    DataRecordLoginReturn = Me.PerfomSiteLogin(pass.LoginName, pass.Password, rememberUserName)
                    If (Not DataRecordLoginReturn.HasErrors AndAlso
                        DataRecordLoginReturn.IsValid AndAlso
                        pass.eMail = go_email AndAlso (pass.GoogleUserId <> go_uid OrElse
                                                       pass.GoogleName <> go_name)) Then

                        DataHelpers.UpdateEUS_Profiles_GoogleUserData(DataRecordLoginReturn.ProfileID, go_uid, go_name, go_email)

                    End If
                End If


            Catch ex As Exception
                DataRecordLoginReturn.HasErrors = True
                DataRecordLoginReturn.Message = ex.Message
           
            End Try
        End Using
        Return DataRecordLoginReturn
    End Function


    Public Shared Function PerfomRelogin_ByAUTHCookies() As clsDataRecordLoginMemberReturn
        Dim result As New clsDataRecordLoginMemberReturn()
        If (HttpContext.Current Is Nothing OrElse HttpContext.Current.User Is Nothing) Then Return result

        Dim Request As HttpRequest = HttpContext.Current.Request
        Dim Session As HttpSessionState = HttpContext.Current.Session

        If HttpContext.Current.User.Identity.IsAuthenticated AndAlso Not Request("logout") = "1" Then
            Dim LoginName As String = HttpContext.Current.User.Identity.Name
            Dim id As System.Web.Security.FormsIdentity = HttpContext.Current.User.Identity
            Dim ticket As System.Web.Security.FormsAuthenticationTicket = id.Ticket
            Session("UserRole") = ticket.UserData

            Try
                If (Not String.IsNullOrEmpty(Request("logon_customer")) AndAlso (Session("IsAdminLogin") Is Nothing OrElse Session("IsAdminLogin") = False)) Then
                    Try
                        result = gLogin.PerfomAdminLogin(Request("logon_customer"))
                        If (result IsNot Nothing AndAlso result.IsValid) Then
                            Session("IsAdminLogin") = True
                        End If
                    Catch ex As Exception
                    End Try
                Else
                    result = gLogin.PerfomRelogin(LoginName, True)
                End If
            Catch ex As Exception

            End Try
        End If

        Return result
    End Function


End Class

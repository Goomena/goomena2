﻿Imports System.Diagnostics
Imports Microsoft.VisualBasic
Imports System
Imports System.Drawing
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.IO
Imports System.Text.RegularExpressions
Imports System.Data.SqlClient
Imports System.Data.OleDb

Public Class SQLConnectionStringData
    Public HasErrors As Boolean = False
    Public Message As String = ""

    Public DB_SQLType As String = "" 'SQL,CE,JET
    Public DB_ServerName As String = "(local)"
    Public DB_Login As String = "sa"
    Public DB_Password As String = ""
    Public DB_NTSecurity As Boolean = False
    Public DB_DataBase As String = ""
    Public DB_Connect_Timeout As Integer = 120
    Public DB_CeFileName As String = ""
    Public DB_ReadyConnectionStringOptional As String = ""

    Public Function ReadFromConfigINI() As Boolean
        Try
            Dim FullFileName As String = Environment.CurrentDirectory & "\Config.ini"
            Dim myIni As New ClsIniConfig(FullFileName)
            If Not My.Computer.FileSystem.FileExists(FullFileName) Then
                HasErrors = True
                Message = "Config File Not Found:" & FullFileName
            End If

            DB_ServerName = myIni.Read("Database", "DB_ServerName", "(local)")
            DB_Login = myIni.Read("Database", "DB_Login", "sa")
            DB_Password = myIni.Read("Database", "DB_Password", "123")
            DB_NTSecurity = myIni.Read("Database", "DB_NTSecurity", "False")
            DB_DataBase = myIni.Read("Database", "DB_DataBase", "LotteryDB")
            DB_Connect_Timeout = myIni.Read("Database", "DB_Connect_Timeout", "120")
            DB_CeFileName = myIni.Read("Database", "DB_CeFileName", "")
            DB_ReadyConnectionStringOptional = myIni.Read("Database", "DB_ReadyConnectionStringOptional", "")

        Catch ex As Exception
            HasErrors = True
            Message = "General Error:" & ex.Message
        End Try
    End Function
End Class


Public Class SQLFunctions

    Public Shared Function GetADOConnectString(ByVal cSQLConnectionStringData As SQLConnectionStringData) As String
        On Error GoTo er
        'If Not gIsReadGlobals Then ReadGlobals()

        If cSQLConnectionStringData.DB_NTSecurity = 0 Then
            GetADOConnectString = "Provider=SQLOLEDB.1;Password=" _
            & cSQLConnectionStringData.DB_Password _
            & "; Persist Security Info=False;User ID=" _
            & cSQLConnectionStringData.DB_Login & ";Initial Catalog=" _
            & cSQLConnectionStringData.DB_DataBase & ";Data Source=" & cSQLConnectionStringData.DB_ServerName _
            & ";Connect Timeout=" & cSQLConnectionStringData.DB_Connect_Timeout
        Else
            GetADOConnectString = "Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False;" _
            & "Initial Catalog=" & cSQLConnectionStringData.DB_DataBase & ";Data Source=" & cSQLConnectionStringData.DB_ServerName _
            & ";Connect Timeout=" & cSQLConnectionStringData.DB_Connect_Timeout
        End If
        Exit Function
er:
        gER.ErrorMsgBox("")
    End Function

    Public Shared Function GetADOConnectString(DB_ServerName As String, DB_DataBase As String, DB_Login As String, DB_Password As String, DB_NTSecurity As Boolean, DB_Connect_Timeout As Integer) As String
        On Error GoTo er
        'If Not gIsReadGlobals Then ReadGlobals()

        If DB_NTSecurity = 0 Then
            GetADOConnectString = "Provider=SQLOLEDB.1;Password=" _
            & DB_Password _
            & "; Persist Security Info=False;User ID=" _
            & DB_Login & ";Initial Catalog=" _
            & DB_DataBase & ";Data Source=" & DB_ServerName _
            & ";Connect Timeout=" & DB_Connect_Timeout
        Else
            GetADOConnectString = "Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False;" _
            & "Initial Catalog=" & DB_DataBase & ";Data Source=" & DB_ServerName _
            & ";Connect Timeout=" & DB_Connect_Timeout
        End If
        Exit Function
er:
        gER.ErrorMsgBox("")
    End Function


    Public Shared Function GetSQLCEConnectString(ByVal cSQLConnectionStringData As SQLConnectionStringData) As String
        On Error GoTo er
        ' If Not gIsReadGlobals Then ReadGlobals()

        If cSQLConnectionStringData.DB_NTSecurity = 0 Then
            GetSQLCEConnectString = "Provider=SQLOLEDB.1;Password=" _
            & cSQLConnectionStringData.DB_Password _
            & "; Persist Security Info=False;User ID=" _
            & cSQLConnectionStringData.DB_Login & ";Initial Catalog=" _
            & cSQLConnectionStringData.DB_DataBase & ";Data Source=" & cSQLConnectionStringData.DB_ServerName _
            & ";Connect Timeout=" & cSQLConnectionStringData.DB_Connect_Timeout
        Else
            GetSQLCEConnectString = "Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False;" _
            & "Initial Catalog=" & cSQLConnectionStringData.DB_DataBase & ";Data Source=" & cSQLConnectionStringData.DB_ServerName _
            & ";Connect Timeout=" & cSQLConnectionStringData.DB_Connect_Timeout
        End If
        Exit Function
er:
        gER.ErrorMsgBox("")
    End Function

    Public Shared Function GetNETSQLConnectString(ByVal cSQLConnectionStringData As SQLConnectionStringData) As String
        On Error GoTo er
        If cSQLConnectionStringData.DB_NTSecurity = 0 Then
            GetNETSQLConnectString = "Data Source=" & cSQLConnectionStringData.DB_ServerName _
                & ";Initial Catalog=" & cSQLConnectionStringData.DB_DataBase _
                & ";Integrated Security=False" _
                & ";Persist Security Info=True" _
                & ";User ID=" & cSQLConnectionStringData.DB_Login _
                & ";Password=" & cSQLConnectionStringData.DB_Password _
                & ";Connect Timeout=" & cSQLConnectionStringData.DB_Connect_Timeout
        Else
            GetNETSQLConnectString = "Data Source=" & cSQLConnectionStringData.DB_ServerName _
            & ";Initial Catalog=" & cSQLConnectionStringData.DB_DataBase _
            & ";Integrated Security=True;Connect Timeout=" & cSQLConnectionStringData.DB_Connect_Timeout
        End If
        Exit Function
er:     gER.ErrorMsgBox("")
    End Function

    Public Shared Function GetNETMYSQLConnectString(ByVal cSQLConnectionStringData As SQLConnectionStringData) As String
        On Error GoTo er
        GetNETMYSQLConnectString = "server=" & cSQLConnectionStringData.DB_ServerName & ";" _
            & "uid=" & cSQLConnectionStringData.DB_Login & ";" _
            & "pwd=" & cSQLConnectionStringData.DB_Password & ";" _
            & "database=" & cSQLConnectionStringData.DB_DataBase & ";"

        Exit Function
er:     gER.ErrorMsgBox("")
    End Function


    Public Shared Function ExecuteSQLCommand(ByVal szCommand As String, ByVal ConnectionString As String) As String
        Try
            Dim connection As SqlConnection = New SqlConnection(ConnectionString)
            connection.Open()
            Dim adapter As SqlDataAdapter = New SqlDataAdapter()
            Dim command As SqlCommand
            command = New SqlCommand(szCommand, connection)
            command.ExecuteNonQuery()
            connection.Close()
            Return "OK"
        Catch ex As Exception
            gER.ErrorMsgBox(ex, "")
            Return ex.message
        End Try
    End Function

    Public Shared Function ExecuteSQLCommand(ByVal szCommand As String, ByVal ConnectionString As String, ByVal CommandTimeout As Integer) As String
        Try
            Dim connection As SqlConnection = New SqlConnection(ConnectionString)
            connection.Open()
            Dim adapter As SqlDataAdapter = New SqlDataAdapter()
            Dim command As SqlCommand
            command = New SqlCommand(szCommand, connection)
            command.CommandTimeout = CommandTimeout
            command.ExecuteNonQuery()
            connection.Close()
            Return "OK"
        Catch ex As Exception
            gER.ErrorMsgBox(ex, "")
            Return ex.Message
        End Try
    End Function

    Public Shared Function GetTableField(ByVal TableName As String, _
    ByVal TitleFieldName As String, _
    ByVal IDFieldName As String, _
    ByVal ValueID As Integer, ByVal ConnectionString As String) As String

        On Error GoTo er

        Dim conn As New SqlConnection(ConnectionString)
        Dim cmd As New SqlCommand("Select * From " & TableName & " WHERE " & IDFieldName & "=" & ValueID, conn)
        ' Open the connection and execute the command
        conn.Open()
        Dim dr As SqlDataReader = cmd.ExecuteReader()
        Dim i As Integer = 0
        While dr.Read()
            i = i + 1
            If Not IsDBNull(dr.Item(TitleFieldName)) Then
                GetTableField = dr.Item(TitleFieldName).ToString
            End If
        End While
GoClose:
        If Not dr.IsClosed Then dr.Close()
        If conn.State = ConnectionState.Open Then conn.Close()
        Exit Function
er:     gER.ErrorMsgBox("")
        GoTo GoClose
    End Function

    Public Shared Function CheckRelatedRecords(ByVal TableName As String, ByVal ParentFieldName As String, ByVal ParentFiledValueID As Integer, ByVal ConnectionString As String) As Boolean
        On Error GoTo er
        Dim conn As New SqlConnection(ConnectionString)
        Dim cmd As New SqlCommand("Select * From " & TableName & " WHERE " & ParentFieldName & "=" & ParentFiledValueID, conn)
        ' Open the connection and execute the command
        conn.Open()
        Dim dr As SqlDataReader = cmd.ExecuteReader()
        CheckRelatedRecords = dr.HasRows
        dr.Close()
        conn.Close()
        Return CheckRelatedRecords
        Exit Function
er:     gER.ErrorMsgBox("")
    End Function

    Public Shared Sub DeleteRecord(ByVal TableName As String, ByVal szWhere As String, ByVal ConnectionString As String)
        On Error GoTo er
        Dim connection As SqlConnection = New SqlConnection(ConnectionString)
        connection.Open()
        Dim adapter As SqlDataAdapter = New SqlDataAdapter()
        ' Create the DeleteCommand.
        Dim command As SqlCommand
        command = New SqlCommand( _
            "DELETE FROM " & TableName & szWhere, connection)
        command.ExecuteNonQuery()
        connection.Close()
        Exit Sub
er:     gER.ErrorMsgBox("")
    End Sub

    Public Shared Sub DeleteRelationRecords(ByVal TableName As String, ByVal ParentFieldName As String, ByVal ParentFiledValueID As Integer, ByVal ConnectionString As String)
        On Error GoTo er
        Dim connection As SqlConnection = New SqlConnection(ConnectionString)
        connection.Open()
        Dim adapter As SqlDataAdapter = New SqlDataAdapter()
        ' Create the DeleteCommand.
        Dim command As SqlCommand
        command = New SqlCommand( _
            "DELETE FROM " & TableName & " WHERE " & ParentFieldName & " = " & ParentFiledValueID, connection)
        command.ExecuteNonQuery()
        connection.Close()
        Exit Sub
er:     gER.ErrorMsgBox("")
    End Sub

    '' PROSOXI DELETE ALL RECORDS FROM TABLE
    Public Shared Sub DeleteAllRecordsFromTable(ByVal TableName As String, ByVal szWhereString As String, ByVal ConnectionString As String)
        On Error GoTo er
        Dim connection As SqlConnection = New SqlConnection(ConnectionString)
        connection.Open()
        Dim adapter As SqlDataAdapter = New SqlDataAdapter()
        ' Create the DeleteCommand.
        Dim command As SqlCommand
        command = New SqlCommand( _
            "DELETE FROM " & TableName & " " & szWhereString, connection)
        command.ExecuteNonQuery()
        connection.Close()
        Exit Sub
er:     gER.ErrorMsgBox("")
    End Sub

    Event OnClearTableDeleteItem(ByVal ItemNum As Integer, ByVal SumOfRecords As Integer, ByVal StatusMessage As String)

    Public Function ClearSQLTable(ByVal szTableName As String, ByVal cMyConnectionString As ClsMyConnectionString) As String
        Try
            Dim rs As New Library.Public.ClsMyADODB(cMyConnectionString)
            If rs.Open("SELECT * FROM " & szTableName, Library.Public.ClsMyADODB.OpenMode.ReadWriteMode) Then
                Dim i As Long
                Dim SumRecords As Long = rs.RecordCount
                While Not rs.EOF
                    rs.Delete()
                    rs.Update()
                    rs.MoveNext()
                    i = i + 1
                    Dim StatusMessage As String = "Deleted: " & i & " of " & SumRecords & " Records."
                    RaiseEvent OnClearTableDeleteItem(i, SumRecords, StatusMessage)
                    Library.Public.Common.DoEventsDLL()

                End While
            End If
            Return "OK"
        Catch ex As Exception
            gER.ErrorMsgBox(ex, "")
            Return ex.Message
        End Try
    End Function

#Region "SQL DATE Time Functions"
    Public Shared Function SQLDate(ByVal D As DevExpress.XtraEditors.DateEdit) As String
        On Error GoTo er
        Dim sz As String
        Dim dd As DateTime
        dd = D.EditValue
        sz = "" & dd.Year & "-" & dd.Month & "-" & dd.Day & ""
        SQLDate = " CONVERT(DATETIME, '" & sz & "  00:00:00', 102)"
        Exit Function
er:     gER.ErrorMsgBox("")
    End Function

    Public Shared Function SQLDateFilter(ByVal D As DevExpress.XtraEditors.DateEdit) As String
        On Error GoTo er
        Dim sz As String
        Dim dd As DateTime
        dd = D.EditValue
        sz = "" & dd.Year & "/" & dd.Month & "/" & dd.Day & ""
        SQLDateFilter = sz '" CONVERT(DATETIME, '" & sZ & "  00:00:00', 102)"
        Exit Function
er:     gER.ErrorMsgBox("")
    End Function

    Public Shared Function mySQLDate(ByVal D As DateTime) As String
        On Error GoTo er
        Dim sz As String
        sz = Microsoft.VisualBasic.Year(D) & "/" & Microsoft.VisualBasic.Month(D) & "/" & Microsoft.VisualBasic.DateAndTime.Day(D) ' d.Year & "/" & d.Month & "/" & d.Day
        mySQLDate = " CONVERT(DATETIME, '" & sz & "  00:00:00', 102)" '"'" & sZ & "'"
        Exit Function
er:     gER.ErrorMsgBox("")
    End Function

    Public Shared Function mySQLDateTime(ByVal D As DateTime) As String
        On Error GoTo er
        Dim sz As String
        sz = Microsoft.VisualBasic.Year(D) _
            & "/" _
            & Microsoft.VisualBasic.Month(D) _
            & "/" _
            & Microsoft.VisualBasic.DateAndTime.Day(D) _
            & " " _
            & Microsoft.VisualBasic.Format(Microsoft.VisualBasic.DateAndTime.Hour(D), "#0") _
            & ":" _
            & Microsoft.VisualBasic.Format(Microsoft.VisualBasic.DateAndTime.Minute(D), "#0") _
            & ":" _
            & Microsoft.VisualBasic.Format(Microsoft.VisualBasic.DateAndTime.Second(D), "#0")
        mySQLDateTime = " CONVERT(DATETIME, '" & sz & "', 102)" '"'" & sZ & "'"
        Exit Function
er:     gER.ErrorMsgBox("")
    End Function

    Public Shared Function mySQLDateTimeCustom(ByVal D As DateTime, ByVal szTime As String) As String
        On Error GoTo er
        Dim sz As String
        sz = Microsoft.VisualBasic.Year(D) & "/" & Microsoft.VisualBasic.Month(D) & "/" & Microsoft.VisualBasic.DateAndTime.Day(D) ' d.Year & "/" & d.Month & "/" & d.Day
        mySQLDateTimeCustom = " CONVERT(DATETIME, '" & sz & "  " & szTime & "', 102)" '"'" & sZ & "'"
        Exit Function
er:     gER.ErrorMsgBox("")
    End Function

    Public Shared Function mySQLDateOnly(ByVal D As DateTime) As String
        On Error GoTo er
        Dim sz As String
        sz = Microsoft.VisualBasic.Year(D) & "/" & Microsoft.VisualBasic.Month(D) & "/" & Microsoft.VisualBasic.DateAndTime.Day(D) ' d.Year & "/" & d.Month & "/" & d.Day
        mySQLDateOnly = sz & "  00:00:00" '"'" & sZ & "'"
        Exit Function
er:     gER.ErrorMsgBox("")
    End Function



#End Region

End Class

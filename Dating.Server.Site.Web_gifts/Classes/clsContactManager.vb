﻿Imports Dating.Server.Core.DLL
Imports System.Data.SqlClient


Public Class clsContactManager

    Public Shared Function Insert_UserContacts(Assoc_ProfileID As Integer, contacts As List(Of clsContact)) As Integer
        Dim rowsAffected As Integer = 0
        Dim finalResult As Integer = 0
        Try

            Dim insertSQL As String = <sql><![CDATA[
    INSERT INTO [EUS_Contacts] ([AssociatedGoomenaProfileID], [ContactFirstName], [ContactLastName], [ContactEmail])
    SELECT @Assoc_ProfileID, @ContactFirstName, @ContactLastName, @ContactEmail
    WHERE NOT EXISTS(
                        SELECT * FROM [EUS_Contacts] 
                        WHERE ([AssociatedGoomenaProfileID]=@Assoc_ProfileID
                              AND [ContactEmail]=@ContactEmail)
                    )

    UPDATE [EUS_Contacts]
    SET
		[isGoomenaUser] = 1,
        [GoomenaProfileID] =  (
							   SELECT [ProfileId]
							   FROM [EUS_Profiles]
							   WHERE [EUS_Profiles].[eMail] = @ContactEmail 
								     AND [EUS_Profiles].IsMaster=1
							   )
        WHERE ([EUS_Contacts].[ContactEmail]=@ContactEmail)
        AND EXISTS(
					  SELECT [ProfileID]
					  FROM [EUS_Profiles]
					  WHERE ([EUS_Profiles].[eMail]=@ContactEmail)
							AND ([EUS_Profiles].IsMaster=1)
				   )

    ]]></sql>

            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                For Each contact As clsContact In contacts

                    Dim ContactFirstName As String = contact.ContactFirstName
                    Dim ContactLastName As String = contact.ContactLastName
                    Dim ContactEmail As String = contact.ContactEmail

                    Using cmd As SqlCommand = DataHelpers.GetSqlCommand(con, insertSQL)



                        cmd.Parameters.AddWithValue("@Assoc_ProfileID", Assoc_ProfileID)
                        cmd.Parameters.AddWithValue("@ContactEmail", ContactEmail)

                        If ContactFirstName <> Nothing Then
                            cmd.Parameters.AddWithValue("@ContactFirstName", ContactFirstName)
                        Else
                            cmd.Parameters.AddWithValue("@ContactFirstName", DBNull.Value)
                        End If

                        If ContactLastName <> Nothing Then
                            cmd.Parameters.AddWithValue("@ContactLastName", ContactLastName)
                        Else
                            cmd.Parameters.AddWithValue("@ContactLastName", DBNull.Value)
                        End If

                        rowsAffected = DataHelpers.ExecuteNonQuery(cmd)
                    End Using
                    If rowsAffected >= 1 Then 'Αν η τρέχουσα επαφή εισήχθη (αρα δεν υπήρχε ήδη)...
                        finalResult += rowsAffected
                    End If

                Next
            End Using
        Catch ex As Exception
            Throw
        Finally

        End Try 'Αν το τελικό αποτέλεσμα είναι >=1, σημαίνει ότι εισήχθη ή ενημερώθηκε έστω και μια εγγραφή...
        Return finalResult
    End Function

    Public Shared Function Count_All_UserContacts(ProfileID As Integer)
        Dim numberOfContacts As Integer
        Try

            Dim sql As String = <sql><![CDATA[
    SELECT COUNT(*) as count FROM [EUS_Contacts]
    WHERE [AssociatedGoomenaProfileID] = @ProfileID
]]></sql>
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd As SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@ProfileID", ProfileID)
                    numberOfContacts = DataHelpers.ExecuteScalar(cmd)
                End Using
            End Using
        Catch ex As Exception
            Throw
        Finally

        End Try
        Return numberOfContacts
    End Function

    Public Shared Function Get_All_UserContacts(ProfileID As Integer) As List(Of clsContact)
        Dim contacts As New List(Of clsContact)()
        Try

            Dim sql As String = <sql><![CDATA[
    SELECT * FROM [EUS_Contacts]
    WHERE [AssociatedGoomenaProfileID] = @ProfileID
]]></sql>
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd As SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@ProfileID", ProfileID)
                    Using result As DataTable = DataHelpers.GetDataTable(cmd)


                        If (result.Rows.Count > 0) Then
                            For Each row As DataRow In result.Rows
                                Dim userContact As New clsContact
                                userContact.ContactEmail = row("ContactEmail").ToString()
                                userContact.ContactFirstName = row("ContactFirstName").ToString()
                                userContact.ContactLastName = row("ContactLastName").ToString()
                                userContact.AssociatedGoomenaProfileID = ProfileID
                                contacts.Add(userContact)
                            Next row
                        End If
                    End Using
                End Using
            End Using
        Catch ex As Exception
            Throw
        Finally

        End Try
        Return contacts
    End Function

    Public Shared Function Count_Registered_UserContacts(ProfileID As Integer)
        Dim numberOfContacts As Integer
        Try

            Dim sql As String = <sql><![CDATA[
    SELECT COUNT(*) as count FROM [EUS_Contacts]
    WHERE [AssociatedGoomenaProfileID] = @ProfileID
    AND [isGoomenaUser] = 1
]]></sql>
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd As SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@ProfileID", ProfileID)
                    numberOfContacts = DataHelpers.ExecuteScalar(cmd)
                End Using
            End Using
        Catch ex As Exception
            Throw
        Finally

        End Try
        Return numberOfContacts
    End Function

    Public Shared Function Get_Registered_UserContacts(ProfileID As Integer) As List(Of clsContact)
        Dim contacts As New List(Of clsContact)()
        Try

            Dim sql As String = <sql><![CDATA[
    SELECT * FROM [EUS_Contacts]
    WHERE [AssociatedGoomenaProfileID] = @ProfileID
    AND [isGoomenaUser] = 1
]]></sql>
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection

                Using cmd As SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@ProfileID", ProfileID)
                    Using result As DataTable = DataHelpers.GetDataTable(cmd)


                        If (result.Rows.Count > 0) Then
                            For Each row As DataRow In result.Rows
                                Dim userContact As New clsContact
                                userContact.AssociatedGoomenaProfileID = ProfileID
                                userContact.ContactEmail = row("ContactEmail").ToString()
                                userContact.ContactFirstName = row("ContactFirstName").ToString()
                                userContact.ContactLastName = row("ContactLastName").ToString()
                                userContact.GoomenaProfileID = row("GoomenaProfileID")
                                contacts.Add(userContact)
                            Next row
                        End If
                    End Using
                End Using

            End Using
        Catch ex As Exception
            Throw
        Finally

        End Try
        Return contacts
    End Function

    Public Shared Function Count_Non_Registered_UserContacts(ProfileID As Integer)
        Dim numberOfContacts As Integer
        Try

            Dim sql As String = <sql><![CDATA[
    SELECT COUNT(*) as count FROM [EUS_Contacts]
    WHERE [AssociatedGoomenaProfileID] = @ProfileID
    AND [isGoomenaUser] = 0
]]></sql>
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                Using cmd As SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@ProfileID", ProfileID)
                    numberOfContacts = DataHelpers.ExecuteScalar(cmd)
                End Using
            End Using
        Catch ex As Exception
            Throw
        Finally

        End Try
        Return numberOfContacts
    End Function

    Public Shared Function Get_Non_Registered_UserContacts(ProfileID As Integer) As List(Of clsContact)
        Dim contacts As New List(Of clsContact)()
        Try

            Dim sql As String = <sql><![CDATA[
    SELECT * FROM [EUS_Contacts]
    WHERE [AssociatedGoomenaProfileID] = @ProfileID
    AND [isGoomenaUser] = 0
]]></sql>
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection

                Using cmd As SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@ProfileID", ProfileID)
                    Using result As DataTable = DataHelpers.GetDataTable(cmd)
                        If (result.Rows.Count > 0) Then
                            For Each row As DataRow In result.Rows
                                Dim userContact As New clsContact
                                userContact.AssociatedGoomenaProfileID = ProfileID
                                userContact.ContactEmail = row("ContactEmail").ToString()
                                userContact.ContactFirstName = row("ContactFirstName").ToString()
                                userContact.ContactLastName = row("ContactLastName").ToString()
                                contacts.Add(userContact)
                            Next row
                        End If
                    End Using
                End Using
            End Using


        Catch ex As Exception
            Throw
        Finally

        End Try
        Return contacts
    End Function

    Public Shared Function getContactAssociatedGoomenaProfiles(ProfileID As Integer) As List(Of Integer)
        Dim associatedGoomenaProfiles As New List(Of Integer)()
        Try

            Dim sql As String = <sql><![CDATA[
    SELECT [AssociatedGoomenaProfileID] FROM [EUS_Contacts]
    WHERE [GoomenaProfileID] = @ProfileID
]]></sql>
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection
                Using cmd As SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@ProfileID", ProfileID)
                    Using result As DataTable = DataHelpers.GetDataTable(cmd)
                        If (result.Rows.Count > 0) Then
                            For Each row As DataRow In result.Rows
                                associatedGoomenaProfiles.Add(row("AssociatedGoomenaProfileID"))
                            Next row
                        End If
                    End Using
                End Using
            End Using

        Catch ex As Exception
            Throw
        Finally

        End Try
        Return associatedGoomenaProfiles
    End Function
End Class

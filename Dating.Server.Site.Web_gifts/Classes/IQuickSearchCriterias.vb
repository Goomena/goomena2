﻿
Public Interface IQuickSearchCriterias

    Event SearchByUserNameClicked(sender As Object, e As System.EventArgs)
    Event SearchClicked(sender As Object, e As EventArgs)
    Event AdvancedClicked(sender As Object, e As EventArgs)

    Property txtUserNameQText As String
    Property ageMinSelectedValue As Integer
    Property ageMaxSelectedValue As Integer
    Property ddlDistanceSelectedValue As Integer

    Property chkOnlineChecked As Boolean
    Property chkPhotosChecked As Boolean
    Property chkPhotosPrivateChecked As Boolean
    Property chkWillingToTravelChecked As Boolean

    Property SelectedBodyTypesList As SearchListInfo
    Property SelectedSpokenLanguagesList As SearchListInfo
    Property SelectedDatingTypesList As SearchListInfo

    Sub Distance_SelectComboItem(value As Integer)
    Sub VerifyControlLoaded()
    Sub ShowBirthdayNote()

End Interface



Public Class SearchListInfo
    Public Property List As List(Of Integer)
    Public Property IsAllSelected As Boolean
End Class


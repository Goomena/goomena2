﻿

Public Class clsLanguageHelper
    ''''''''''''''''''''''''''''''''''''''''''
    '
    ' language notes
    '
    ' parameters for lang:
    ' 1.url directory /gr/, /es/, /tr/
    ' 2.cookie lag value
    ' 3.lag param of query string
    ' 4.session lagid value
    '
    ' Url directory used for public pages' SEO.
    ' lag parameter is used 
    '       on localhost and 
    '       members folder and 
    '       if SupportedLAGS_URLS setting is not set.
    '
    ' (check all this parameters and return correct language to display on client)
    ''''''''''''''''''''''''''''''''''''''''''

    Public Shared Property gSiteName As String = If(ConfigurationManager.AppSettings("gSiteName") Is Nothing, "", ConfigurationManager.AppSettings("gSiteName")).ToUpper()

    Private __context As HttpContext
    Private ReadOnly Property Session As HttpSessionState
        Get
            Return __context.Session
        End Get
    End Property
    Private ReadOnly Property Request As HttpRequest
        Get
            Return __context.Request
        End Get
    End Property
    Private ReadOnly Property Response As HttpResponse
        Get
            Return __context.Response
        End Get
    End Property
    Private ReadOnly Property Server As HttpServerUtility
        Get
            Return __context.Server
        End Get
    End Property

    Public Sub New(ByRef context As HttpContext)
        __context = context
    End Sub


    Public Function GetNewLAGUrl(LagIndex As Integer) As String
        Dim requestedLag As String = GetLagIDForIndex(LagIndex)
        Return GetNewLAGUrl(requestedLag)
    End Function

    Public Function GetNewLAGUrl(requestedLag As String) As String

        Dim url As String = Nothing
        Dim currentUrl As String = Request.Url.ToString()
        Dim host As String = Request.Url.Host.ToUpper()
        '    Dim LAGParam As String = Request.QueryString("LAG")

        If (host = gSiteName) Then
            '' Here we are when the host is www.goomena.com, not localhost
            '' Here we want to make url like http://www.goomena.com/gr/,
            '' if US is the selected lang, than url should be without /gr 
            '' if LAG param is present in QueryString, than will be cleared

            Dim requestedLagURL As String = GetURLAvailableForLAG(requestedLag)
            If (Not String.IsNullOrEmpty(requestedLagURL)) Then

                Dim currentLag As String = GetLAGFromURL(currentUrl)
                Dim currentLagURL As String = GetURLAvailableForLAG(currentLag)

                requestedLag = If(String.IsNullOrEmpty(requestedLag), "", requestedLag)
                currentLag = If(String.IsNullOrEmpty(currentLag), "", currentLag)

                Dim isSameLag As Boolean = requestedLag.Equals(currentLag)


                url = Request.Url.ToString()
                If (Not isSameLag) Then
                    url = url.Replace(currentLagURL, requestedLagURL)
                End If

                If (url.IndexOf("?") > -1) Then
                    url = url.Remove(url.IndexOf("?") + 1)
                    For Each itm As String In Request.QueryString.AllKeys
                        If (Not String.IsNullOrEmpty(itm) AndAlso itm.ToUpper() <> "LAG" AndAlso Not String.IsNullOrEmpty(Request.QueryString(itm))) Then
                            url = url & itm & "=" & HttpUtility.UrlEncode(Request.QueryString(itm)) & "&"
                        End If
                    Next
                    url = url.TrimEnd({"&"c, "?"c})
                End If
            End If

        End If

        If (String.IsNullOrEmpty(url)) Then
            url = Request.Url.AbsolutePath
            url = url & "?"

            For Each itm As String In Request.QueryString.AllKeys
                If (Not String.IsNullOrEmpty(itm) AndAlso itm.ToUpper() <> "LAG" AndAlso Not String.IsNullOrEmpty(Request.QueryString(itm))) Then
                    url = url & itm & "=" & HttpUtility.UrlEncode(Request.QueryString(itm)) & "&"
                End If
            Next

            url = url & "lag=" & requestedLag

        End If

        Return url
    End Function

    Public Function GetNewLAGUrl(RequestUrl As System.Uri, requestedLag As String, targetUrl As String) As String
        If (String.IsNullOrEmpty(targetUrl)) Then Return targetUrl

        Dim url As String = targetUrl
        Dim host As String = RequestUrl.Host.ToUpper()

        If (host = gSiteName) Then
            '' Here we are when the host is www.goomena.com, not localhost
            '' Here we want to make url like http://www.goomena.com/gr/,
            '' if US is the selected lang, than url should be without /gr 
            '' if LAG param is present in QueryString, than will be cleared

            Dim requestedLagURL As String = GetURLAvailableForLAG(requestedLag)
            If (Not String.IsNullOrEmpty(requestedLagURL)) Then

                Dim currentLag As String = GetLAGFromURL(targetUrl)
                Dim currentLagURL As String = GetURLAvailableForLAG(currentLag)

                requestedLag = If(String.IsNullOrEmpty(requestedLag), "", requestedLag)
                currentLag = If(String.IsNullOrEmpty(currentLag), "", currentLag)

                Dim isSameLag As Boolean = requestedLag.Equals(currentLag)
                If (Not isSameLag) Then
                    url = url.Replace(currentLagURL, requestedLagURL)
                End If
            End If

        End If

        If (url.IndexOf("?") > -1) Then
            Dim QueryString As String = url.Remove(0, url.IndexOf("?") + 1)
            Dim QueryStringArr As String() = QueryString.Split({"&"c}, StringSplitOptions.RemoveEmptyEntries)

            url = url.Remove(url.IndexOf("?") + 1)

            For Each itm As String In QueryStringArr
                If (Not itm.ToUpper().StartsWith("LAG")) Then url = url & itm & "&"
            Next
            url = url.TrimEnd({"&"c, "?"c})
        End If

        If (host <> gSiteName) Then
            url = If(url IsNot Nothing, url, "") & If(url.IndexOf("?") > -1, "&", "?") & "lag=" & requestedLag
        End If

        Return url
    End Function


    Public Function GetNewLAGUrl2(RequestUrl As System.Uri, requestedLag As String, targetUrl As String) As String
        If (String.IsNullOrEmpty(targetUrl)) Then Return targetUrl

        Dim url As String = targetUrl
        Dim host As String = RequestUrl.Host.ToUpper()

        If (host = gSiteName) Then
            '' Here we are when the host is www.goomena.com, not localhost
            '' Here we want to make url like http://www.goomena.com/gr/,
            '' if US is the selected lang, than url should be without /gr 
            '' if LAG param is present in QueryString, than will be cleared

            Dim requestedLagURL As String = GetURLAvailableForLAG(requestedLag)
            If (Not String.IsNullOrEmpty(requestedLagURL)) Then

                Dim currentLag As String = GetLAGFromURL(targetUrl)
                Dim currentLagURL As String = GetURLAvailableForLAG(currentLag)

                currentLag = If(String.IsNullOrEmpty(currentLag), "", currentLag)
                requestedLag = If(String.IsNullOrEmpty(requestedLag), "", requestedLag)

                Dim isSameLag As Boolean = requestedLag.Equals(currentLag)
                If (Not isSameLag) Then
                    url = url.Replace(currentLagURL, requestedLagURL)
                End If
            End If

        End If

        If (url.IndexOf("?") > -1) Then url = url.TrimEnd({"?"c, "&"c})
        If (url.IndexOf("?") > -1) Then
            Dim QueryString As String = url.Remove(0, url.IndexOf("?") + 1)
            Dim QueryStringArr As String() = QueryString.Split({"&"c}, StringSplitOptions.RemoveEmptyEntries)

            url = url.Remove(url.IndexOf("?") + 1)

            For Each itm As String In QueryStringArr
                If (Not itm.ToUpper().StartsWith("LAG")) Then url = url & itm & "&"
            Next
            url = url.TrimEnd({"&"c, "?"c})
        End If

        If (host <> gSiteName) Then
            url = If(url IsNot Nothing, url, "") & If(url.IndexOf("?") > -1, "&", "?") & "lag=" & requestedLag
        End If

        Return url
    End Function



    'Public Function SetIdenticalLAGToTargetURL(currentLagURL As String, targetUri As Uri) As String
    '    Dim host As String = Request.Url.Host.ToUpper()
    '    Dim targetUrl As String = targetUri.ToString()

    '    If (host = gSiteName) Then
    '        Dim requestedLagURL As String = GetLAGFromURL(targetUrl)

    '        requestedLag = If(String.IsNullOrEmpty(requestedLag), "", requestedLag)
    '        currentLag = If(String.IsNullOrEmpty(currentLag), "", currentLag)

    '        Dim isSameLag As Boolean = requestedLag.Equals(currentLag)
    '        If (Not isSameLag) Then
    '            targetUrl = targetUrl.Replace(currentLagURL, targetUri)
    '        End If
    '    End If

    '    End If

    '    Return targetUrl
    'End Function


    Public Function GetNewLAGUrl_WithLagParam(LagIndex As Integer) As String
        Dim requestedLag As String = GetLagIDForIndex(LagIndex)
        Return GetNewLAGUrl_WithLagParam(requestedLag)
    End Function

    Public Function GetNewLAGUrl_WithLagParam(requestedLag As String) As String
        Dim url As String = Nothing
        url = Request.Url.AbsolutePath
        url = url & "?"

        For Each itm As String In Request.QueryString.AllKeys
            If (Not String.IsNullOrEmpty(itm) AndAlso itm.ToUpper() <> "LAG" AndAlso Not String.IsNullOrEmpty(Request.QueryString(itm))) Then
                url = url & itm & "=" & HttpUtility.UrlEncode(Request.QueryString(itm)) & "&"
            End If
        Next

        url = url & "lag=" & requestedLag

        Return url
    End Function


    Public Shared Function GetNewLAGUrl_WithLagParam(url As String, requestedLag As String) As String
        If (Not String.IsNullOrEmpty(url) AndAlso
            Not String.IsNullOrEmpty(requestedLag) ) Then

            If (url.IndexOf("?") > -1) Then

                Dim QueryString As String = url.Remove(0, url.IndexOf("?") + 1)
                Dim QueryStringArr As String() = QueryString.Split({"&"c}, StringSplitOptions.RemoveEmptyEntries)

                url = url.Remove(url.IndexOf("?") + 1)

                For Each itm As String In QueryStringArr
                    If (Not itm.ToUpper().StartsWith("LAG")) Then url = url & itm & "&"
                Next
                url = url.TrimEnd({"&"c, "?"c})
            End If

            url = url & If(url.IndexOf("?") = -1, "?", "&") & "lag=" & requestedLag
        End If

        Return url
    End Function


    Public Shared Function RemoveLagParam(url As String) As String
        If (Not String.IsNullOrEmpty(url)) Then

            If (url.IndexOf("?") > -1) Then

                Dim QueryString As String = url.Remove(0, url.IndexOf("?") + 1)
                Dim QueryStringArr As String() = QueryString.Split({"&"c}, StringSplitOptions.RemoveEmptyEntries)

                url = url.Remove(url.IndexOf("?") + 1)

                For Each itm As String In QueryStringArr
                    If (Not itm.ToUpper().StartsWith("LAG")) Then url = url & itm & "&"
                Next
                url = url.TrimEnd({"&"c, "?"c})
            End If

        End If

        Return url
    End Function


    Public Function RemoveLagParam_FromRequestURL() As String
        Dim url As String = Request.Url.AbsolutePath
        url = url & "?"

        For Each itm As String In Request.QueryString.AllKeys
            If (Not String.IsNullOrEmpty(itm) AndAlso itm.ToUpper() <> "LAG" AndAlso Not String.IsNullOrEmpty(Request.QueryString(itm))) Then
                url = url & itm & "=" & HttpUtility.UrlEncode(Request.QueryString(itm)) & "&"
            End If
        Next
        Return url
    End Function


    Public Function GetRedirectURL_WhenLAGParam(lagParam As String) As String
        Dim checkIfShouldRedirect As Boolean = False
        Dim url As String = Nothing

        If (String.IsNullOrEmpty(lagParam)) Then Return url

        Dim requestedLagURL As String = ""
        Dim host As String = Request.Url.Host.ToUpper()
        If (host = clsLanguageHelper.gSiteName) Then
            If (clsLanguageHelper.IsSupportedLAG(lagParam, Session("GEO_COUNTRY_CODE"))) Then
                requestedLagURL = clsLanguageHelper.GetURLForLAG(lagParam)
                If (Not String.IsNullOrEmpty(requestedLagURL)) Then checkIfShouldRedirect = True
            End If
        End If


        If (checkIfShouldRedirect) Then
            '' Here we are when the host is www.goomena.com, not localhost
            '' Here we want to make url like http://www.goomena.com/gr/,
            '' if US is the selected lang, than url should be without /gr 
            '' if LAG param is present in QueryString, than will be cleared

            Dim currentUrl As String = Request.Url.ToString()
            Dim currentLag As String = GetLAGFromURL(currentUrl)
            Dim currentLagURL As String = GetURLForLAG(currentLag)
            url = currentUrl.ToLower()

            lagParam = If(String.IsNullOrEmpty(lagParam), "", lagParam).ToLower()
            currentLag = If(String.IsNullOrEmpty(currentLag), "", currentLag).ToLower()


            Dim isSameLag As Boolean = lagParam.Equals(currentLag)
            If (Not isSameLag) Then
                url = url.Replace(currentLagURL, requestedLagURL)
            End If

            If (url.IndexOf("?") > -1) Then
                url = url.Remove(url.IndexOf("?") + 1)
                For Each itm As String In Request.QueryString.AllKeys
                    If (Not String.IsNullOrEmpty(itm) AndAlso itm.ToUpper() <> "LAG" AndAlso Not String.IsNullOrEmpty(Request.QueryString(itm))) Then
                        url = url & itm & "=" & HttpUtility.UrlEncode(Request.QueryString(itm)) & "&"
                    End If
                Next
                url = url.TrimEnd({"&"c, "?"c})
            End If

        End If

        Return url
    End Function


    Shared RedirectFromTo_Dict As New Dictionary(Of String, String)
    Public Shared Sub CheckDomainAndRedirect(host As String)
        Try

            If (RedirectFromTo_Dict.Count = 0) Then
                ' fill shared dictionary

                '''''''''''''''''''''''''''''''''''''''''''''''''''
                ''  format for RedirectFromTo appSetting example
                ''  [goomena.com www.goomena.com],[goomena.gr www.goomena.com/gr],[www.goomena.gr www.goomena.com/gr]
                '''''''''''''''''''''''''''''''''''''''''''''''''''
                Dim config As String = ConfigurationManager.AppSettings("RedirectFromTo")
                Dim redirections As String() = config.Split(","c)
                Dim cnt As Integer
                For cnt = 0 To redirections.Length - 1
                    Dim domains As String() = (redirections(cnt).Replace("[", "").Replace("]", "")).Split(" "c)
                    If (domains.Length = 2) Then
                        RedirectFromTo_Dict.Add(domains(0), domains(1))
                    End If
                Next
            End If


            If (RedirectFromTo_Dict.Count > 0) Then
                ' process URL
                host = host.ToUpper()
                Dim cnt As Integer
                For cnt = 0 To RedirectFromTo_Dict.Count - 1
                    Dim key As String = RedirectFromTo_Dict.Keys(cnt)
                    If (host.ToUpper() = key.ToUpper()) Then
                        Dim newUrl As String = HttpContext.Current.Request.Url.ToString()
                        newUrl = newUrl.Replace(HttpContext.Current.Request.Url.Host, RedirectFromTo_Dict(key))

                        'goomena.gr
                        If HttpContext.Current.Request.Url.Host.EndsWith(".gr", StringComparison.OrdinalIgnoreCase) Then
                            Dim ndxSchema As Integer = newUrl.IndexOf("://")
                            Dim ndxLastSlash As Integer = -1
                            Dim appendLang As String = "/?lag=" & gDomainGR_DefaultLAG
                            If (ndxSchema > -1) Then
                                ndxLastSlash = newUrl.IndexOf("/", ndxSchema)
                                If (ndxLastSlash > -1) Then
                                    appendLang = If(newUrl.IndexOf("?") = -1, "?", "&") & "lag=" & gDomainGR_DefaultLAG
                                End If
                            End If

                            newUrl = newUrl & appendLang
                        End If


                        HttpContext.Current.Response.RedirectPermanent(newUrl)

                    End If
                Next

            End If

        Catch
        End Try
    End Sub



    Shared SupportedLAGS_URLS_Dict As New Dictionary(Of String, String)
    Private Shared Sub FillSupportedLAGS_URLS_Dict()
        If (SupportedLAGS_URLS_Dict.Count = 0) Then
            ' fill shared dictionary

            '''''''''''''''''''''''''''''''''''''''''''''''''''
            ''  format for SupportedLAGS_URLS appSetting example
            ''  [US www.goomena.com],[GR www.goomena.com/gr],[TR www.goomena.com/tr],[ES www.goomena.com/es]
            '''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim config As String = ConfigurationManager.AppSettings("SupportedLAGS_URLS")
            Dim redirections As String() = config.Split(","c)
            Dim cnt As Integer
            For cnt = 0 To redirections.Length - 1
                Dim domains As String() = (redirections(cnt).Replace("[", "").Replace("]", "")).Split(" "c)
                If (domains.Length = 2) Then
                    Try
                        SupportedLAGS_URLS_Dict.Add(domains(0).ToUpper(), domains(1))
                    Catch
                    End Try
                End If
            Next
        End If
    End Sub

    Public Shared Function GetURLForLAG(lag As String) As String
        If (String.IsNullOrEmpty(lag)) Then Return ""
        lag = lag.ToUpper()
        Try
            FillSupportedLAGS_URLS_Dict()

            If (SupportedLAGS_URLS_Dict.Count > 0 AndAlso SupportedLAGS_URLS_Dict.ContainsKey(lag)) Then
                Return SupportedLAGS_URLS_Dict(lag)
            End If
        Catch
        End Try

        Return ""
    End Function


    Public Function GetURLAvailableForLAG(lag As String) As String
        Dim url As String = ""
        If (String.IsNullOrEmpty(lag)) Then Return url
        Dim host As String = Request.Url.Host.ToUpper()
        Try
            If (host = clsLanguageHelper.gSiteName) Then
                FillSupportedLAGS_URLS_Dict()

                lag = lag.ToUpper()
                If (SupportedLAGS_URLS_Dict.Count > 0 AndAlso SupportedLAGS_URLS_Dict.ContainsKey(lag)) Then
                    url = SupportedLAGS_URLS_Dict(lag)
                End If
            End If
        Catch
        End Try

        Return url
    End Function

    'Public Shared Function GetLAGFromURL(url As String, LAGParam As String) As String
    '    Dim lag As String = ""
    '    Try

    '        If (Not String.IsNullOrEmpty(LAGParam)) Then
    '            ' 1. get current language from QueryString param

    '            lag = LAGParam
    '        Else
    '            ' 2. get current language from URL 

    '            FillSupportedLAGS_URLS_Dict()
    '            url = url.ToUpper()

    '            For cnt = 0 To SupportedLAGS_URLS_Dict.Keys.Count - 1

    '                Dim key As String = SupportedLAGS_URLS_Dict.Keys(cnt)
    '                'If (Not key.Contains("/")) Then Continue For

    '                Dim itm As String = SupportedLAGS_URLS_Dict(key)
    '                If (url.EndsWith(itm.ToUpper())) Then
    '                    lag = key
    '                    ' iterate all collection
    '                    'Exit For
    '                Else
    '                    url = url & "/"
    '                    If (Not itm.EndsWith("/")) Then itm = itm & "/"
    '                    If (url.Contains(itm.ToUpper())) Then
    '                        lag = key
    '                        ' iterate all collection
    '                        'Exit For
    '                    End If
    '                End If
    '            Next
    '        End If

    '    Catch
    '    End Try

    '    Return lag
    'End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="LAGParam"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetLAGFromURLParam(LAGParam As String) As String
        Dim lag As String = ""
        Try
            Dim Session As HttpSessionState = HttpContext.Current.Session

            If (Not String.IsNullOrEmpty(LAGParam) AndAlso clsLanguageHelper.IsSupportedLAG(LAGParam, Session("GEO_COUNTRY_CODE"))) Then
                ' get current language from QueryString param
                lag = LAGParam
            End If

        Catch
        End Try

        Return lag
    End Function


    Public Shared Function GetLAGFromURL(url As String) As String
        Dim lag As String = ""
        Try
            ' get current language from URL 

            FillSupportedLAGS_URLS_Dict()
            url = url.ToUpper()

            For cnt = 0 To SupportedLAGS_URLS_Dict.Keys.Count - 1

                Dim key As String = SupportedLAGS_URLS_Dict.Keys(cnt)
                'If (Not key.Contains("/")) Then Continue For

                Dim itm As String = SupportedLAGS_URLS_Dict(key)
                If (url.EndsWith(itm.ToUpper())) Then
                    lag = key
                    ' iterate all collection
                    'Exit For
                Else
                    url = url & "/"
                    If (Not itm.EndsWith("/")) Then itm = itm & "/"
                    If (url.Contains(itm.ToUpper())) Then
                        lag = key
                        ' iterate all collection
                        'Exit For
                    End If
                End If
            Next

        Catch
        End Try

        Return lag
    End Function


    Public Shared Sub SetLagCookie(lag As String)
        If (String.IsNullOrEmpty(lag)) Then Return
        Try
            Dim Request As System.Web.HttpRequest = HttpContext.Current.Request
            Dim Response As System.Web.HttpResponse = HttpContext.Current.Response
            Dim Session As HttpSessionState = HttpContext.Current.Session
            Dim Server As HttpServerUtility = HttpContext.Current.Server

            Dim lagCookie As HttpCookie = Request.Cookies("lagCookie")
            If lagCookie Is Nothing Then
                Dim szGuid As String = System.Guid.NewGuid().ToString()
                lagCookie = New HttpCookie("lagCookie", szGuid)
                lagCookie.Expires = DateTime.Now.AddYears(2)
                lagCookie.Item("LagID") = lag
                Response.Cookies.Add(lagCookie)
                Session("lagCookie") = szGuid
                Session("LagID") = lagCookie.Item("LagID")
                Response.Cookies.Add(lagCookie)
            Else
                If (lagCookie.Item("LagID") <> lag) Then
                    Session("lagCookie") = Server.HtmlEncode(lagCookie.Value)
                    lagCookie.Item("LagID") = lag
                    If (lagCookie.Values.Count > 0) Then Session("lagCookie") = lagCookie.Values(0)
                    Session("LagID") = lagCookie.Item("LagID")
                    Response.Cookies.Add(lagCookie)
                End If
            End If
            'Session("LagID") = lagCookie.Item("LagID")
            'Response.Cookies.Add(lagCookie)

        Catch ex As Exception
            WebErrorSendEmail(ex, "Writting lag param on cookie.")
        End Try
    End Sub


    Public Shared Function GetLagCookie() As String
        Dim LagID As String = Nothing
        Try
            Dim Request As System.Web.HttpRequest = HttpContext.Current.Request
            '   Dim Response As System.Web.HttpResponse = HttpContext.Current.Response
            Dim Session As HttpSessionState = HttpContext.Current.Session
            '  Dim Server As HttpServerUtility = HttpContext.Current.Server

            Dim lagCookie As HttpCookie = Request.Cookies("lagCookie")
            If lagCookie IsNot Nothing Then
                LagID = lagCookie.Item("LagID")
            Else
                LagID = Session("LagID")
            End If
        Catch ex As Exception
            WebErrorSendEmail(ex, "Reading lag param from cookie.")
        End Try
        Return LagID
    End Function


    Public Sub RedirectToNewLAGForMembers(LagIndex As Integer)
        Try

            Dim url As String = GetNewLAGUrl_WithLagParam(LagIndex)
            Response.Redirect(url)

        Catch ex As System.Threading.ThreadAbortException
        End Try
    End Sub


    Public Sub RedirectToNewLAG(LagIndex As Integer)
        Try

            Dim url As String = GetNewLAGUrl(LagIndex)
            Response.Redirect(url)

        Catch ex As System.Threading.ThreadAbortException
        End Try
    End Sub


    Public Function SessionStarted_RedirectToCountryLAG(Optional useThreadAbort As Boolean = True) As Boolean

        ''''''''''''''''''''''''''''''''''''''''''
        ' parameters for lang:
        ' 1.url directory /gr/, /es/, /tr/
        ' 2.cookie lag value
        ' 3.lag param of query string
        ' 4.session lagid value
        '
        ' Url directory used for public pages' SEO.
        ' lag parameter is used 
        '       on localhost and 
        '       members folder and 
        '       if SupportedLAGS_URLS setting is not set.
        '
        ' (check all this parameters and return correct language to display on client)
        ''''''''''''''''''''''''''''''''''''''''''

        Dim isredirected As Boolean = False
        If (Request.QueryString("lang") = "1") Then Return isredirected

        Dim RequestUrl As Uri = Request.Url
        Dim currentUrl As String = RequestUrl.ToString()
        Dim lagFromURL As String = clsLanguageHelper.GetLAGFromURL(currentUrl)

        ' perform redirect to user language, when LAGID is us only
        ' it means that the default url was used www.goomena.com and not www.goomena.com/es
        If (Session("LAGID") <> "US" AndAlso Not String.IsNullOrEmpty(lagFromURL) AndAlso lagFromURL = "US") Then

            Dim newUrl As String = GetNewLAGUrl(Session("LAGID"))
            If (Not String.IsNullOrEmpty(newUrl)) Then

                Dim urlNew As Uri = Nothing
                If (Not newUrl.StartsWith("/")) Then
                    Try
                        urlNew = New Uri(newUrl)
                    Catch ex As System.UriFormatException
                    End Try
                End If
                If (urlNew Is Nothing) Then urlNew = UrlUtils.GetFullURI(UrlUtils.GetCurrentHostURL(RequestUrl), newUrl)

                If (RequestUrl.ToString().ToLower() <> urlNew.ToString().ToLower() AndAlso
                    RequestUrl.LocalPath.ToLower() <> urlNew.LocalPath.ToLower()) Then
                    'urlCurrent.PathAndQuery.ToLower() <> urlNew.PathAndQuery.ToLower()) Then

                    SetLagCookie(Session("LagID"))
                    If (useThreadAbort) Then
                        Response.Redirect(newUrl)
                    Else
                        Response.Redirect(newUrl, False)
                        __context.ApplicationInstance.CompleteRequest()
                    End If
                    isredirected = True
                End If
            End If

        End If
        Return isredirected
    End Function


    Public Sub RedirectToNewLAGURL()
        If (Request.QueryString("lang") = "1") Then Return

        If (Not String.IsNullOrEmpty(Request.QueryString("LAG"))) Then

            Dim currentUrl As String = Request.Url.ToString()
            Dim lagParam As String = Request.QueryString("LAG")
            Dim newUrl As String = GetRedirectURL_WhenLAGParam(lagParam)


            If (Not String.IsNullOrEmpty(newUrl)) Then
                Dim urlCurrent As New Uri(currentUrl)
                Dim urlNew As New Uri(newUrl)
                If (currentUrl.ToLower() <> newUrl.ToLower() AndAlso
                    urlCurrent.PathAndQuery.ToLower() <> urlNew.PathAndQuery.ToLower()) Then
                    'Try
                    Response.Redirect(newUrl)
                    'Catch ex As System.Exception
                    '    WebInfoSendEmail("OnLoad-isPagePublic (with lag param), " & " newUrl:" & newUrl)
                    'End Try
                End If
            End If

        Else
            Dim currentUrl As String = Request.Url.ToString()
            Dim lagFromURL As String = GetLAGFromURL(currentUrl)

            If (Not String.IsNullOrEmpty(lagFromURL) AndAlso lagFromURL <> Session("LAGID").ToString()) Then

                Dim newUrl As String = GetNewLAGUrl(lagFromURL)
                If (Not String.IsNullOrEmpty(newUrl)) Then
                    Dim urlCurrent As New Uri(currentUrl)
                    Dim urlNew As New Uri(newUrl)
                    If (currentUrl.ToLower() <> newUrl.ToLower() AndAlso
                        urlCurrent.PathAndQuery.ToLower() <> urlNew.PathAndQuery.ToLower()) Then
                        'Try
                        Response.Redirect(newUrl)
                        'Catch ex As System.Exception
                        '    WebInfoSendEmail("OnLoad-isPagePublic (no lag param), currentUrl:" & currentUrl & "---newUrl:" & newUrl)
                        'End Try
                    End If
                End If
            End If

        End If
    End Sub




    Public Sub CheckIfMemberPageShouldRedirectToMainURL()
        Try
            Dim currentUrl As String = Request.Url.ToString()
        Dim lagFromURL As String = clsLanguageHelper.GetLAGFromURL(currentUrl)
        If (Not String.IsNullOrEmpty(lagFromURL) AndAlso lagFromURL.ToUpper() <> "US") Then
            Dim url As String = GetNewLAGUrl("US")
            If (Not Regex.IsMatch(url, "[?&]lag=1", RegexOptions.IgnoreCase)) Then
                url = If(url IsNot Nothing, url, "") & If(url.IndexOf("?") > -1, "&", "?") & "lag=" & lagFromURL
            End If
            url = clsCurrentContext.FormatRedirectUrl(url)
            Response.Redirect(url)
        End If

        If (Not String.IsNullOrEmpty(Request.QueryString(FormsAuthentication.FormsCookieName))) Then
            currentUrl = currentUrl.Remove(currentUrl.IndexOf("?") + 1)
            For Each itm As String In Request.QueryString.AllKeys
                If (itm.ToUpper() <> FormsAuthentication.FormsCookieName.ToUpper() AndAlso Not String.IsNullOrEmpty(Request.QueryString(itm))) Then
                    currentUrl = currentUrl & itm & "=" & HttpUtility.UrlEncode(Request.QueryString(itm)) & "&"
                End If
            Next
            currentUrl = currentUrl.TrimEnd({"&"c, "?"c})

                Response.RedirectPermanent(currentUrl)

            End If
        Catch ex1 As System.Threading.ThreadAbortException
        End Try
    End Sub


    Public Sub CheckIfPublicPageShouldRedirect()

        If (Not String.IsNullOrEmpty(Request.QueryString("LAG"))) Then
            Dim newUrl As String = GetRedirectURL_WhenLAGParam(Request.QueryString("LAG"))
            If (Not String.IsNullOrEmpty(newUrl)) Then
                Try
                    Response.Redirect(newUrl)
                Catch ex As System.Threading.ThreadAbortException

                End Try

            End If
        End If

    End Sub

    Public Shared Function GetIndexFromLagID(ByVal LagID As String) As Integer
        Select Case LagID
            Case "GR" : Return 2
                'Case "AL" : Return 4
            Case "TR" : Return 3
            Case "ES" : Return 1
        End Select

        Return 0
    End Function


    Public Shared Function GetLagIDForIndex(ByVal index As Integer) As String
        Select Case index
            Case 2 : Return "GR"
                'Case 4 : Return "AL"
            Case 3 : Return "TR"
            Case 1 : Return "ES"

        End Select

        Return "US"
    End Function



    Public Shared Sub SetLAGID()
        Dim Session As HttpSessionState = HttpContext.Current.Session
        If (clsLanguageHelper.IsSupportedLAG(Session("GEO_COUNTRY_CODE"), Session("GEO_COUNTRY_CODE"))) Then
            Session("LagID") = Session("GEO_COUNTRY_CODE")
        Else
            Session("LagID") = "US"
        End If
    End Sub


    Public Function GetPublicURL(href As String, requestedLag As String) As String
        Dim currentHost As String = UrlUtils.GetCurrentHostURL(Request.Url)
        Dim _uri As System.Uri = UrlUtils.GetFullURI(currentHost, href)
        Dim newHref As String = Me.GetNewLAGUrl(Request.Url, requestedLag, _uri.ToString())
        Return newHref
    End Function



    Private Shared _alllags() As String
    Public Shared Function IsSupportedLAG(lag As String, country As String) As Boolean
        Dim success As Boolean
        If (String.IsNullOrEmpty(lag)) Then Return success

        If (_alllags Is Nothing) Then
            '''''''''''''''''''''''''''''''''''''''''''''''''''
            ''  format for SupportedLAGS appSetting example
            ''  US,GR,TR,ES
            '''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim SupportedLAGS As String = ConfigurationManager.AppSettings("SupportedLAGS")
            If (String.IsNullOrEmpty(SupportedLAGS)) Then SupportedLAGS = "GR,US,DE,FR,HU,IN,RO"
            _alllags = SupportedLAGS.Split(",")
        End If

        If (Not String.IsNullOrEmpty(lag)) Then
            Dim reqLag As String = lag.ToUpper()
            If (_alllags.Contains(reqLag)) Then
                success = True
            End If
            If (success) Then
                Dim cc As clsLanguageHelper.CountryConfig = clsLanguageHelper.GetCountryConfig(country)
                If (cc IsNot Nothing AndAlso cc.HasConfig()) Then
                    success = cc.ShowLangs.Contains(reqLag)
                End If
            End If
        End If

        Return success
    End Function


    Shared CountryConfig_Dict As New Dictionary(Of String, CountryConfig)
    Private Shared Sub FillCountryConfig_Dict(country As String)
        If (CountryConfig_Dict.Count = 0) Then
            ' fill shared dictionary

            '''''''''''''''''''''''''''''''''''''''''''''''''''
            ''  format for CountriesConfig appSetting example
            ''  [ES showlang:US,ES]
            '''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim config As String = ConfigurationManager.AppSettings("CountriesConfig")
            If (Not String.IsNullOrEmpty(config)) Then

                Try
                    Dim redirections As String() = config.Split(","c)
                    Dim cnt As Integer
                    For cnt = 0 To redirections.Length - 1
                        Dim domains As String() = (redirections(cnt).Replace("[", "").Replace("]", "")).Split(" "c)
                        If (domains.Length > 1) Then
                            Dim lag As String = domains(0).ToUpper()
                            If (Not CountryConfig_Dict.ContainsKey(lag)) Then
                                Try
                                    Dim cc As New CountryConfig()
                                    cc.SetConfig(lag, domains(1).ToUpper())
                                    CountryConfig_Dict.Add(lag, cc)
                                Catch
                                End Try
                            End If
                        End If
                    Next
                Catch
                End Try

            End If

        End If
    End Sub

    Public Shared Function HasCountryConfig(country As String) As Boolean
        If country IsNot Nothing Then


            country = country.ToUpper()
            FillCountryConfig_Dict(country)
            If (CountryConfig_Dict.ContainsKey(country)) Then
                Dim cc As CountryConfig = CountryConfig_Dict(country)
                Return cc.HasConfig()
            End If
        End If
        Return False
    End Function


    Public Shared Function GetCountryConfig(country As String) As CountryConfig
        If (country IsNot Nothing) Then
            country = country.ToUpper()
            FillCountryConfig_Dict(country)
            If (CountryConfig_Dict.ContainsKey(country)) Then
                Dim cc As CountryConfig = CountryConfig_Dict(country)
                Return cc
            End If
        End If
        Return Nothing
    End Function

    Public Class CountryConfig
        Public Property CountryCode As String
        Public Property ShowLangs As String()
        '   Public Property LanguagesOrder As String()

        Public Sub New()
            ShowLangs = New String() {}
        End Sub

        Public Function SetConfig(country As String, configInfo As String) As Boolean
            If (String.IsNullOrEmpty(configInfo)) Then Return False

            CountryCode = country
            If (configInfo.IndexOf("showlang:", StringComparison.OrdinalIgnoreCase) > -1) Then
                ShowLangs = configInfo.
                    Replace("showlang:", "").
                    Replace("SHOWLANG:", "").
                    ToUpper().
                    Split({"-"c}, StringSplitOptions.RemoveEmptyEntries)
            End If
            Return True
        End Function

        Public Function HasConfig() As Boolean
            If (ShowLangs.Length > 0) Then
                Return True
            Else
                Return False
            End If
        End Function
    End Class



    Public Function FixHTMLBodyAnchors(html As String, currentLag As String, RouteData As System.Web.Routing.RouteData) As String

        If (String.IsNullOrEmpty(html)) Then
            Return html
        End If

        Dim currentLagUrl As String = Me.GetURLAvailableForLAG(currentLag)
        Dim RequestUrl As System.Uri = Request.Url
        Dim pageScope As PageScopeEnum = UrlUtils.GetPageScope(RequestUrl, RouteData)
        Dim currentHost As String = UrlUtils.GetCurrentHostURL(RequestUrl)

        Dim processHtml As Boolean =
            currentLag <> "US" AndAlso
            Not String.IsNullOrEmpty(currentLagUrl) AndAlso
            Not currentLagUrl.ToLower().Equals(clsLanguageHelper.GetURLForLAG("US").ToLower()) AndAlso
            (pageScope = PageScopeEnum.MembersPage OrElse pageScope = PageScopeEnum.PublicPage)


        If (processHtml) Then

            If (pageScope = PageScopeEnum.MembersPage) Then

                Dim matchStartIndex As Integer = 0
                Dim ancMatch As Match = clsWebFormHelper.AnchorRegex.Match(html, matchStartIndex)

                While ancMatch.Success
                    Try

                        Dim hrefGroup As Group = ancMatch.Groups(1)
                        Dim href As String = hrefGroup.Value
                        matchStartIndex = hrefGroup.Index + hrefGroup.Length

                        'If (href.StartsWith("/")) Then

                        '    Dim _uri As System.Uri = UrlUtils.GetFullURI(currentHost, href)
                        '    Dim newHref As String = Me.GetNewLAGUrl2(RequestUrl, currentLag, _uri.ToString())
                        '    html = html.Remove(hrefGroup.Index, hrefGroup.Length)
                        '    html = html.Insert(hrefGroup.Index, newHref)
                        '    matchStartIndex = hrefGroup.Index + newHref.Length

                        'Else
                        If (Not UrlUtils.IsPathInCurrentFolder(RequestUrl, href)) Then

                            Dim _uri As System.Uri = UrlUtils.GetFullURI(currentHost, href)
                            Dim scope As PageScopeEnum = UrlUtils.GetPageScope(_uri)
                            If (scope = PageScopeEnum.PublicPage) Then
                                Dim newHref As String = Me.GetNewLAGUrl(RequestUrl, currentLag, _uri.ToString())
                                html = html.Remove(hrefGroup.Index, hrefGroup.Length)
                                html = html.Insert(hrefGroup.Index, newHref)
                                matchStartIndex = hrefGroup.Index + newHref.Length
                            End If

                        End If

                    Catch
                    End Try
                    ancMatch = clsWebFormHelper.AnchorRegex.Match(html, matchStartIndex)
                End While

            ElseIf (pageScope = PageScopeEnum.PublicPage) Then

                Dim matchStartIndex As Integer = 0
                Dim ancMatch As Match = clsWebFormHelper.AnchorRegex.Match(html, matchStartIndex)

                While ancMatch.Success

                    Try

                        Dim hrefGroup As Group = ancMatch.Groups(1)
                        Dim href As String = hrefGroup.Value
                        Dim newHref As String = ""
                        matchStartIndex = hrefGroup.Index + hrefGroup.Length

                        If href.IndexOf("http") = 0 Then
                            Dim urlLag As String = clsLanguageHelper.GetLAGFromURL(href)
                            If (Not String.IsNullOrEmpty(urlLag) AndAlso currentLag <> urlLag) Then
                                newHref = Me.GetNewLAGUrl(RequestUrl, currentLag, href)
                            End If
                        End If


                        'If (Not UrlUtils.IsPathInCurrentFolder(RequestUrl, href)) Then
                        If (href.StartsWith("/")) Then
                            Dim _uri As System.Uri = UrlUtils.GetFullURI(currentHost, href)
                            newHref = Me.GetNewLAGUrl2(RequestUrl, currentLag, _uri.ToString())
                        End If
                        'End If


                        If (newHref.Length > 0) Then
                            html = html.Remove(hrefGroup.Index, hrefGroup.Length)
                            html = html.Insert(hrefGroup.Index, newHref)
                            matchStartIndex = hrefGroup.Index + newHref.Length
                        End If

                    Catch
                    End Try

                    ancMatch = clsWebFormHelper.AnchorRegex.Match(html, matchStartIndex)
                End While

            End If
        End If

        Return html
    End Function



End Class



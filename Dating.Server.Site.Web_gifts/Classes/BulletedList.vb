Namespace SCC.WebUserControls
    ''' <summary>
    ''' I wanted a way to display a bulleted list of items, bound from a strongly-typed list of strings.
    ''' The .net "BulletedList" control seemed promising, but it insisted on HTML-endcoding the content of the string, so HTML could not
    ''' be displayed onto the page within the bullet. This control was created to be clean, and be easy to build from a List(Of String).
    ''' However I realise that it may be useful to sometimes have HTML encoded, so I also provide the "EncodeHtml" property to do this.
    ''' </summary>
    ''' <remarks>Ben Sloan, 11 July 2008</remarks>
    Public Class BulletedList
        Inherits Control

        ''' <summary>
        ''' Gets or sets the list of strings to be rendered by the control.
        ''' </summary>
        ''' <value>A strongly typed list of strings</value>
        ''' <returns>A strongly typed list of strings</returns>
        Public Property Items() As List(Of String)
            Get
                If IsNothing(ViewState("Items")) Then
                    ' Instantiate the list of items to a new list; which will allow us to do ".Items.Add" from elsewhere in code without 
                    ' having to worry about null values checks.
                    ViewState("Items") = New list(Of String)
                End If
                Return DirectCast(ViewState("Items"), List(Of String))
            End Get
            Set(ByVal value As List(Of String))
                ViewState("Items") = value
            End Set
        End Property

        ''' <summary>
        ''' Determines whether HTML should be encoded before it is rendered to the output stream.
        ''' For example, should a "&gt;" symbol be rendered as the &gt;, or as &amp;gt;?
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property EncodeHtml() As Boolean
            Get
                If IsNothing(ViewState("EncodeHtml")) Then
                    Return False
                Else
                    Return DirectCast(ViewState("EncodeHtml"), Boolean)
                End If
            End Get
            Set(ByVal value As Boolean)
                ViewState("EncodeHtml") = value
            End Set
        End Property

        ''' <summary>
        ''' Determines how the control should render itself.
        ''' </summary>
        Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
            ' Only output something if there are items in the collection:
            If Items.Count > 0 Then
                ' Output start tag <ul>
                writer.RenderBeginTag(HtmlTextWriterTag.Ul)

                ' Output all the items
                For Each s As String In Items
                    writer.RenderBeginTag(HtmlTextWriterTag.Li)
                    If EncodeHtml Then
                        writer.Write(HttpContext.Current.Server.HtmlEncode(s))
                    Else
                        writer.Write(s)
                    End If
                    writer.RenderEndTag()

                    writer.WriteLine()
                Next

                ' Output end tag </ul>
                writer.RenderEndTag()
            End If

            MyBase.Render(writer)
        End Sub
    End Class
End Namespace



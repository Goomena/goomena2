﻿<%@ Page Language="vb" AutoEventWireup="true" MasterPageFile="~/Root2.Master" CodeBehind="Register2.aspx.vb"
    Inherits="Dating.Server.Site.Web.Register2" MaintainScrollPositionOnPostback="false" %>

<%@ Register Src="UserControls/DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<%@ Register Assembly="DevExpress.Web.v13.1, Version=13.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>
<%@ Register src="~/UserControls/LiveZillaPublic.ascx" tagname="LiveZillaPublic" tagprefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="/v1/Scripts/userAvailabilityCheck2.js"></script>
    <style type="text/css">
        .rightsidediv { float: right; }
        .clear { clear: both; }
         ul.BulletedList{margin-left:0px !important;padding-left:6px;}
        .BulletedList li{margin-bottom:6px;}
        .BulletedListItem a { color: Red; text-decoration: none; border-bottom-color: #f70; border-bottom-width: 1px; border-bottom-style: dashed;}
        .lists { background: url(goomena-kostas/first-general/bg-reapet.jpg); }
        .registernewcontainer { background: white; width: 485px; position: relative; float: left; padding-left: 10px; padding-top: 0; padding-bottom: 55px; font-size: 13px; }
        .registernewcontainer li { list-style: none; }
        .registernewcontainer ul { padding-left: 0px; }
        .mainregistercon { background: url("//cdn.goomena.com/goomena-kostas/first-general/bg-reapet.jpg") repeat; border-top: 6px solid #be202f; padding-bottom: 100px; padding-top: 61px; }
        .regmidcont { margin-left: auto; margin-right: auto; width: 1080px; position: relative; }
        .lblform { float: left; margin-right: 5px; width: 135px; text-align: right; }
        .divform { margin-bottom: 3px; width: 480px; }
        .center { cursor: pointer; }
        .loadingIndicator { background: url('//cdn.goomena.com/Images2/ajax-loader.gif') no-repeat right center; box-sizing: border-box; padding-right: 20px; }
        .mailValidation, .usernameValidation, .passwordValidation { text-align: left;width:100% !important; margin-top:10px;padding-left:0px;margin-bottom:20px;display:none;}
        ul.dxvsL{margin-left:0px !important;padding-left:0px;}
        ul.dxvsL li.dxvsE{margin-bottom:4px;}
        ul.dxvsL li.dxvsE a{}

        .pe_form .divform .dxeTextBox input.dxeEditArea {height: 17px !important; line-height:17px !important; }
        .pe_form .divform .dxeButtonEdit input.dxeEditArea {height: 17px !important; line-height:17px !important; }
        .pe_form .form_right .dxeButtonEdit input.dxeEditArea {height: 17px !important; line-height:17px !important; }
        .google-connect { display:none;}
        
        /* genders list, btnRegister button outter table */
        #ctl00_content_rblGender_ET, #ctl00_content_btnRegister {margin-left: auto; margin-right: auto; }
    </style>
    <script type="text/javascript" src="/v1/Scripts/register.js?v=8"></script>
    <script type="text/javascript">

        msg_ClickToExpand = "<%= globalStrings.GetCustomString("msg_ClickToExpand", GetLag() )  %>";
        msg_ClickToCollapse = "<%= globalStrings.GetCustomString("msg_ClickToCollapse", GetLag()) %>";
        LoginWithGoogle = '<%= ResolveUrl("~/LoginWithGoogle.aspx")%>';
        passwordValidation_DiffThanLogin = '<%= Dating.Server.Site.Web.AppUtils.JS_PrepareString(Mybase.CurrentPageData.GetCustomString("Error.Password.DifferentThanUsername"))  %>';

        // <![CDATA[
    function showZip() {
        var zip = jQuery('#<%-- = TrShowRegion.ClientID --%>');
        var region = jQuery('#<%= TrRegion.ClientID %>');
        var city = jQuery('#<%= TrCity.ClientID %>');

        //zip.show()
        region.hide()
        city.hide()

        var status = jQuery('#<%= hdfLocationStatus.ClientID %>');
        status.val("zip")
    }
    function showRegions() {
        //var zip = jQuery('#< %= liLocationsZip.ClientID %>');
        var zip = jQuery('#<%-- = TrShowRegion.ClientID --%>');
        var region = jQuery('#<%= TrRegion.ClientID %>');
        var city = jQuery('#<%= TrCity.ClientID %>');
        zip.hide()
        region.show()
        city.show()
        var status = jQuery('#<%= hdfLocationStatus.ClientID %>');
        status.val("region")
    }

    function valSumm_VisibilityChanged1(s, e) {
        valSumm_VisibilityChanged(s, e)
        showValidationPanel(e.visible);
    }

    function showValidationPanel(isVisible) {
        if (isVisible) {
            $('#<%= pnlValidation.ClientID%>').removeClass('hidden');
        }
        else {
            $('#<%= pnlValidation.ClientID%>').addClass('hidden');
        }
    }
</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
<asp:UpdatePanel ID="updRegister" runat="server">
    <ContentTemplate>

<div id="register-sections">
<div id="all_sections">
    <div class="section-1-ext">
        <div class="section-1">
            <div class="regmidcontainer" id="regmidcontainer" runat="server">
                <div class="reg-box">
                    <div class="registerform" id="registerform" runat="server">
                        <div class="mainregdiv">
<div class="notmember">
    <h1 class="login-title font-light"><asp:Label ID="lblHeader" runat="server"/></h1>
    <asp:Label ID="lblHeaderText" runat="server" CssClass="select-item" Visible="false"></asp:Label>
</div>

<asp:Panel ID="pnlReg" runat="server" DefaultButton="btnRegister" class="login_box">
    <asp:Panel ID="pnlContinueMessageCompleteForm" runat="server" CssClass="alert alert-success"
        Visible="False">
        <br /> 
        <div style="padding-top:10px;padding-bottom:10px;">
        <asp:Label ID="lblContinueMessageCompleteForm" runat="server" Text="" Font-Bold="true"></asp:Label></div>
    </asp:Panel>


    <div runat="server" id="TrGender" class="check-wrap RegisterGenderRadioButtonList" clientidmode="Static">
        <dx:ASPxRadioButtonList ID="rblGender" runat="server" RepeatLayout="UnorderedList" ClientInstanceName="RegisterGenderRadioButtonList">
            <Paddings PaddingLeft="0px" />
            <ValidationSettings ValidationGroup="RegisterGroup" 
                ErrorDisplayMode="ImageWithTooltip"
                ErrorTextPosition="Right" 
                Display="Dynamic">
                <RequiredField ErrorText="" IsRequired="True" />
            </ValidationSettings>
            <Border BorderStyle="None" />
            <RadioButtonStyle BackgroundImage-HorizontalPosition="center" HorizontalAlign="Center">
                <BackgroundImage HorizontalPosition="center"></BackgroundImage>
            </RadioButtonStyle>
            <CheckedImage Url="//cdn.goomena.com/images2/search/check.png" Height="19px"
                Width="18px">
            </CheckedImage>
            <UncheckedImage Url="//cdn.goomena.com/images2/search/uncheck.png" Height="19px"
                Width="18px">
            </UncheckedImage>
            <ClientSideEvents Validation="field_Validation" />
        </dx:ASPxRadioButtonList>
        <img src="//cdn.goomena.com/images2/pub2/check.png" alt="check ok" width="18" height="18" class="check RegisterGenderRadioButtonList-ok hidden"/>
        <img src="//cdn.goomena.com/images2/pub2/error.png" alt="check error" width="18" height="18" class="check RegisterGenderRadioButtonList-error hidden"/>
    </div>


    <div runat="server" id="TrMyEmail" class="login-input-wrap RegisterEmailTextBox" clientidmode="Static">
        <dx:ASPxTextBox ID="txtEmail" runat="server" 
            ClientInstanceName="RegisterEmailTextBox"
            Width="309px" CssClass="txtEmail">
            <ValidationSettings ValidationGroup="RegisterGroup" ErrorDisplayMode="ImageWithTooltip"
                ErrorTextPosition="Right" Display="Dynamic">
                <RequiredField IsRequired="True" ErrorText="" />
                <RegularExpression ValidationExpression="" ErrorText="" />
            </ValidationSettings>
            <BackgroundImage HorizontalPosition="0px" ImageUrl="//cdn.goomena.com/images2/pub2/mail-icon.png" Repeat="NoRepeat" VerticalPosition="50%" />
            <ClientSideEvents Validation="field_Validation" />
        </dx:ASPxTextBox>
        <asp:Label ID="mailValidation" runat="server" Text="" CssClass="mailValidation"></asp:Label>
        <img src="//cdn.goomena.com/images2/pub2/check.png" alt="check ok" width="18" height="18" class="check RegisterEmailTextBox-ok hidden"/>
        <img src="//cdn.goomena.com/images2/pub2/error.png" alt="check error" width="18" height="18" class="check RegisterEmailTextBox-error hidden"/>
    </div>
    <div class="fields-separator"></div>
    
    <div runat="server" id="TrMyUsername" class="login-input-wrap RegisterLoginTextBox" clientidmode="Static">
        <dx:ASPxTextBox ID="txtLogin" runat="server" ClientInstanceName="RegisterLoginTextBox"
            Width="309px" CssClass="txtLogin">
            <ValidationSettings ValidationGroup="RegisterGroup" ErrorDisplayMode="ImageWithTooltip"
                ErrorTextPosition="Right" ErrorText="" Display="Dynamic">
                <RequiredField ErrorText="" IsRequired="True" />
                <RegularExpression 
                    ValidationExpression="^(?=.*\d)(?=(.*[a-zA-Z]){2}).[^&:]{3,12}$" 
                    ErrorText="Το όνομα χρήστη πρέπει να αποτελείται απο 3 μέχρι 12 χαρακτήρες και να περιλαμβάνει τουλάχιστον ένα γράμμα και έναν αριθμό." />
            </ValidationSettings>
            <BackgroundImage HorizontalPosition="0px" ImageUrl="//cdn.goomena.com/images2/pub2/username-icon.png" Repeat="NoRepeat" VerticalPosition="50%" />
            <ClientSideEvents Validation="field_Validation" />
        </dx:ASPxTextBox>
        <asp:Label ID="usernameValidation" runat="server" Text="" CssClass="usernameValidation"></asp:Label>
        <img src="//cdn.goomena.com/images2/pub2/check.png" alt="check ok" width="18" height="18" class="check RegisterLoginTextBox-ok hidden"/>
        <img src="//cdn.goomena.com/images2/pub2/error.png" alt="check error" width="18" height="18" class="check RegisterLoginTextBox-error hidden"/>
    </div>
    <div class="fields-separator"></div>

    <div runat="server" id="TrMyPassword" class="login-input-wrap RegisterPassword-wrap" clientidmode="Static">
        <dx:ASPxTextBox ID="txtPasswrd" runat="server" Password="True" 
            ClientInstanceName="RegisterPassword"
            Width="309px" CssClass="txtPasswrd">
            <ValidationSettings ValidationGroup="RegisterGroup" ErrorDisplayMode="ImageWithTooltip"
                ErrorText="" ErrorTextPosition="Right" Display="Dynamic">
                <RequiredField ErrorText="" IsRequired="True" />
                <RegularExpression 
                    ValidationExpression="^(?=.*\d)(?=.*[a-zA-Z]).{3,20}$" 
                    ErrorText="Ο κωδικός πρέπει να αποτελείται απο 3 μέχρι 20 χαρακτήρες και να περιλαμβάνει τουλάχιστον ένα γράμμα και έναν αριθμό." />
            </ValidationSettings>
            <BackgroundImage HorizontalPosition="0px" ImageUrl="//cdn.goomena.com/images2/pub2/password-icon.png" Repeat="NoRepeat" VerticalPosition="50%" />
            <ClientSideEvents LostFocus="showWaterMark5" GotFocus="hideWaterMark5" Validation="field_Validation"/>
        </dx:ASPxTextBox>
        <asp:Label ID="passwordValidation" runat="server" Text="" CssClass="passwordValidation"></asp:Label>
        <div class="text-watermark RegisterPassword"><asp:Label ID="msg_MyPassword" runat="server" Text="Password"/></div>
        <img src="//cdn.goomena.com/images2/pub2/check.png" alt="check ok" width="18" height="18" class="check RegisterPassword-ok hidden"/>
        <img src="//cdn.goomena.com/images2/pub2/error.png" alt="check error" width="18" height="18" class="check RegisterPassword-error hidden"/>
    </div>
    <div class="fields-separator"></div>

    <div runat="server" id="TrMyPasswordConfirm" class="login-input-wrap RegisterPasswordConfirm-wrap" clientidmode="Static">
        <dx:ASPxTextBox ID="txtPasswrd1Conf" runat="server" Password="True" 
            ClientInstanceName="RegisterPasswordConfirm"
            Width="309px">
            <ValidationSettings ValidationGroup="RegisterGroup" ErrorDisplayMode="ImageWithTooltip"
                ErrorTextPosition="Right" ErrorText="" Display="Dynamic">
            </ValidationSettings>
            <BackgroundImage HorizontalPosition="0px" ImageUrl="//cdn.goomena.com/images2/pub2/verify-icon.png" Repeat="NoRepeat" VerticalPosition="50%" />
            <ClientSideEvents LostFocus="showWaterMark5" GotFocus="hideWaterMark5" Validation="field_Validation" />
        </dx:ASPxTextBox>
        <div class="text-watermark"><asp:Label ID="msg_MyPasswordConfirm" runat="server" Text="Password"/></div>
        <img src="//cdn.goomena.com/images2/pub2/check.png" alt="check ok" width="18" height="18" class="check RegisterPasswordConfirm-ok hidden"/>
        <img src="//cdn.goomena.com/images2/pub2/error.png" alt="check error" width="18" height="18" class="check RegisterPasswordConfirm-error hidden"/>
    </div>
    <div class="fields-separator"></div>

    <div runat="server" id="TrMyBirthdate" class="birth-wrap" clientidmode="Static">
        <div class="birth-title-wrap">
            <asp:Label ID="msg_MyBirthdate" runat="server" Text="" CssClass="birth-title font-semibold" />
        </div>
        <uc1:DateControl Height="28px" FontSize="14px" ID="ctlDate" runat="server" DateTime='<%# Bind("Birthday") %>' YearsFromNow="-18" YearsTillNow="100"  />
    </div>
    <div class="fields-separator"></div>

    <dx:ASPxCallbackPanel Style="background: white;" ID="cbpnlZip" runat="server" ClientInstanceName="cbpnlZip"
        CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" ShowLoadingPanel="False"
        ShowLoadingPanelImage="False">
        <ClientSideEvents 
            EndCallback="cbpnlZip_EndCallback" 
            CallbackError="cbpnlZip_CallbackError" 
            BeginCallback="cbpnlZip_BeginCallback" />

        <LoadingPanelImage Url="~/App_Themes/DevEx/Web/Loading.gif">
        </LoadingPanelImage>
        <LoadingPanelStyle ImageSpacing="5px">
        </LoadingPanelStyle>
        <PanelCollection>
            <dx:PanelContent ID="PanelContent1" runat="server" SupportsDisabledAttribute="True">
                <asp:HiddenField ID="hdfLocationStatus" runat="server" />
                <div>

                    <div runat="server" id="TrCountry" class="login-input-wrap" clientidmode="Static">
                        <dx:ASPxComboBox ID="cbCountry" runat="server"
                            EncodeHtml="False" IncrementalFilteringMode="StartsWith">
                            <ClientSideEvents 
                                SelectedIndexChanged="cbCountry_SelectedIndexChanged"
                                EndCallback="cbCountry_EndCallback" />
                            <ValidationSettings CausesValidation="True" Display="Dynamic" ValidationGroup="RegisterGroup">
                            </ValidationSettings>
                        </dx:ASPxComboBox>
                    </div>
                    <div class="fields-separator"></div>

                    <%--<div runat="server" id="TrShowRegion" style="display:none;">
                        <span class="zip_list">
                            <asp:LinkButton ID="msg_SelectRegion" runat="server" CssClass="js" OnClientClick="showRegions();return false;" /></span>
                    </div>--%>
                    <div runat="server" id="TrRegion" class="login-input-wrap" clientidmode="Static">
                        <dx:ASPxComboBox ID="cbRegion" runat="server"
                            EncodeHtml="False" EnableSynchronization="False" ClientInstanceName="cbRegion"
                            IncrementalFilteringMode="StartsWith" DataSourceID="sdsRegion" TextField="region1"
                            ValueField="region1">
                            <ClientSideEvents 
                                SelectedIndexChanged="cbRegion_SelectedIndexChanged" 
                                EndCallback="cbRegion_EndCallback" />
                            <ValidationSettings CausesValidation="True" Display="Dynamic" ValidationGroup="RegisterGroup">
                            </ValidationSettings>
                        </dx:ASPxComboBox>
                        <asp:SqlDataSource ID="sdsRegion" runat="server" ConnectionString="<%$ ConnectionStrings:AppDBconnectionString %>">
                        </asp:SqlDataSource>
                    </div>
                    <div class="fields-separator"></div>


                    <div runat="server" id="TrCity" class="login-input-wrap" clientidmode="Static">
                        <dx:ASPxComboBox ID="cbCity" runat="server"
                            EncodeHtml="False" EnableSynchronization="False" ClientInstanceName="cbCity"
                            IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" DataSourceID="sdsCity"
                            TextField="city" ValueField="city">
                            <ClientSideEvents 
                                SelectedIndexChanged="cbCity_SelectedIndexChanged" />
                            <ValidationSettings CausesValidation="True" Display="Dynamic" ValidationGroup="RegisterGroup">
                            </ValidationSettings>
                        </dx:ASPxComboBox>
                        <asp:SqlDataSource ID="sdsCity" runat="server" ConnectionString="<%$ ConnectionStrings:AppDBconnectionString %>">
                        </asp:SqlDataSource>
                    </div>
                    <div class="fields-separator"></div>

                    <div runat="server" id="TrZip" class="login-input-wrap" clientidmode="Static">
                        <dx:ASPxComboBox ID="txtZip" runat="server" DataSourceID="zip">
                            <ValidationSettings Display="Dynamic">
                            </ValidationSettings>
                        </dx:ASPxComboBox>
                        <asp:SqlDataSource ID="zip" runat="server" ConnectionString="<%$ ConnectionStrings:AppDBconnectionString %>"></asp:SqlDataSource>
                    </div>

                </div>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>

    <div runat="server" id="TrAgreements" class="check-wrap RegisterAgreementsCheckBox" clientidmode="Static">
        <dx:ASPxCheckBox ID="cbAgreements" runat="server" Text="" 
            ClientInstanceName="RegisterAgreementsCheckBox"
            EncodeHtml="False" Checked="false">
            <ClientSideEvents Validation="field_Validation" />
            <ValidationSettings 
                ValidationGroup="RegisterGroup" 
                ErrorDisplayMode="ImageWithTooltip"
                ErrorTextPosition="Right">
                <RequiredField ErrorText="" IsRequired="True" />
            </ValidationSettings>
            <CheckedImage Url="//cdn.goomena.com/images2/search/check.png" Height="19px"
                Width="18px">
            </CheckedImage>
            <UncheckedImage Url="//cdn.goomena.com/images2/search/uncheck.png" Height="19px"
                Width="18px">
            </UncheckedImage>
        </dx:ASPxCheckBox>
        <img src="//cdn.goomena.com/images2/pub2/check.png" alt="check ok" width="18" height="18" class="check RegisterAgreementsCheckBox-ok hidden"/>
        <img src="//cdn.goomena.com/images2/pub2/error.png" alt="check error" width="18" height="18" class="check RegisterAgreementsCheckBox-error hidden"/>
    </div>
    <div class="clear"></div>

    <div class="form-actions">
        <dx:ASPxButton ID="btnRegister" runat="server" CommandName="Insert"
            ValidationGroup="RegisterGroup" 
            EncodeHtml="False" 
            Height="42px"
            Font-Size="24px" 
            Width="174px" 
            EnableDefaultAppearance="False" 
            Cursor="pointer"
            EnableClientSideAPI="True" 
            Native="true">
            <Border BorderWidth="0px"></Border>
            <BackgroundImage HorizontalPosition="50%" ImageUrl="//cdn.goomena.com/images2/pub2/register-button.png" Repeat="NoRepeat" VerticalPosition="0" />
            <HoverStyle>
                <BackgroundImage HorizontalPosition="50%" ImageUrl="//cdn.goomena.com/images2/pub2/register-button.png" Repeat="NoRepeat" VerticalPosition="-107px" />
            </HoverStyle>
        </dx:ASPxButton>
    </div>
                    

</asp:Panel>

                            <div class="clear"></div>
                        </div>
                    </div>
                    <asp:Panel ID="pnlSocials" runat="server" class="login-buttons">
                        <div class="login-with-wrap font-semibold">
                            <asp:Label ID="lblJoinWith" runat="server" CssClass="login-with">Join with</asp:Label>
                        </div>
                        <asp:HyperLink ID="lblFB" runat="server" Text="Login With Facebook"
                            NavigateUrl="~/Register.aspx?log=fb" onclick="ShowLoading();" CssClass="main-fb-logindiv">
                            <img src="//cdn.goomena.com/images/spacer10.png"  alt="facebook" width="24" height="47"/>
                        </asp:HyperLink>
                        <asp:HyperLink ID="lblGP" runat="server" Text="Login With Google"
                            NavigateUrl="~/Register.aspx?log=google" onclick="ShowLoading();" CssClass="main-gp-logindiv">
                            <img src="//cdn.goomena.com/images/spacer10.png" alt="g+" width="25" height="39"/>
                        </asp:HyperLink>
                    </asp:Panel>

                    <asp:Panel runat="server" ID="pnlValidation" CssClass="validations hidden">
                        <div class="triangle-left"></div>
                        <div class="validations-controls">
                            <asp:BulletedList ID="serverValidationList" runat="server" Font-Size="14px" ForeColor="Red"
                                DisplayMode="HyperLink" CssClass="BulletedList">
                            </asp:BulletedList>

                            <dx:ASPxValidationSummary ID="valSumm" runat="server" 
                                ValidationGroup="RegisterGroup"
                                RenderMode="BulletedList" 
                                ShowErrorsInEditors="True" 
                                EncodeHtml="False">
                                <Paddings PaddingBottom="10px" />
                                <LinkStyle>
                                    <Font Size="14px"></Font>
                                </LinkStyle>
                                <ClientSideEvents VisibilityChanged="valSumm_VisibilityChanged1"/>
                            </dx:ASPxValidationSummary>
                        </div>
                    </asp:Panel>

                    <h1 class="reg-wellcome"><asp:Literal ID="lblWellcomeMsg" runat="server"></asp:Literal></h1>
                </div>
            </div>
            <div class="gendermsg"></div> 
            <div id="top_pic" runat="server" clientidmode="Static"></div>
        </div>
    </div> 
    <div class="section-1">
        <div class="members-count">
            <asp:Literal ID="lblMembersCount" runat="server"></asp:Literal>
        </div>
    </div> 
    <div class="section-1-ext">
        <div class="section-1">
            <div class="members-photos">
                <asp:Panel ID="pnlMosaic" runat="server" CssClass="mosaic" EnableViewState="false">
		            <div runat="server" class="pic" id="pic1"></div>
		            <div runat="server" class="pic" id="pic2"></div>
		            <div runat="server" class="pic" id="pic3"></div>
		            <div runat="server" class="pic" id="pic4"></div>
		            <div runat="server" class="pic" id="pic5"></div>
		            <div runat="server" class="pic" id="pic6"></div>
		            <div runat="server" class="pic" id="pic7"></div>
		            <div runat="server" class="pic" id="pic8"></div>
		            <div runat="server" class="pic" id="pic9"></div>
		            <div runat="server" class="pic" id="pic10"></div>
		            <div runat="server" class="pic" id="pic11"></div>
		            <div runat="server" class="pic" id="pic12"></div>
		            <div runat="server" class="pic" id="pic13"></div>
		            <div runat="server" class="pic" id="pic14"></div>
		            <div runat="server" class="pic" id="pic15"></div>
		            <div runat="server" class="pic" id="pic16"></div>
		            <div runat="server" class="pic" id="pic17"></div>
		            <div runat="server" class="pic" id="pic18"></div>
		            <div runat="server" class="pic" id="pic19"></div>
                    <div class="clear"></div>
                 </asp:Panel> 
            </div>
        </div>
    </div> 
</div> 
</div>

        <asp:HyperLink ID="lnkLogGoogle" runat="server" onclick="OpenGoogleLoginPopup()" CssClass="google-connect">Login with google</asp:HyperLink>

        </ContentTemplate>
</asp:UpdatePanel> 
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cntBodyBottom" runat="server">
<div class="PhotoInfoWrap hidden">
    <div id="PhotoInfos" class="PhotoInfos">
        <img class="InfoImg" alt="" src="" />
    </div>
    <p class="ProfileID"></p>
    <p class="ProfileCity"></p>
    <div class="ProfileAgeWrap">
        <p class="ProfileAgeText"></p>
        <p class="ProfileAge"></p>
    </div>
</div>
                       
<script type="text/javascript">
    var profileBaseUrl = '<%= MyBase.LanguageHelper.GetPublicURL("/profile/", MyBase.GetLag()) %>';

    if (!stringIsEmpty(profileBaseUrl)) {
        if (profileBaseUrl.indexOf("?") > -1) profileBaseUrl = profileBaseUrl.substring(0, profileBaseUrl.indexOf("?"));
    }

    function checkBorders(e) {
        var widthCheck = 2004 + 20;
        var itmsExt = ['.section-1-ext'];
        var itms = ['.section-1'];
        for (var i = 0; i < itmsExt.length; i++) {
            var itmExt = $(itmsExt[i]);
            var itm = $(itms[i], itmsExt[i]);
            var w = itmExt.width();
            if (w >= widthCheck && !itm.is('.ext-borders')) {
                itm.addClass('ext-borders');
            }
            else if (w < widthCheck && itm.is('.ext-borders')) {
                itm.removeClass('ext-borders');
            }
        }
    }


    function setWaterMarkHandlers() {
        try {
            $(".text-watermark", "#TrMyPassword").click(function () { RegisterPassword.SetFocus(); });
            $(".text-watermark", "#TrMyPasswordConfirm").click(function () { RegisterPasswordConfirm.SetFocus(); });
        }
        catch (e) { }
        try {
            showWaterMark5(window["RegisterPassword"], null);
            showWaterMark5(window["RegisterPasswordConfirm"], null);
        }
        catch (e) { }
    }
</script>  
<script type="text/javascript" src="/v1/Scripts/mosaic.js"></script> 
<script type="text/javascript">
    $(checkBorders);
    $(window).on('resize', checkBorders);

    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function (sender, args) {
        RegisterForm_LoadAsyncValidation()
        setWaterMarkHandlers();

        if ($('#<%= serverValidationList.ClientID%>').length > 0) {
            showValidationPanel(true);
        }
    });
    RegisterForm_LoadAsyncValidation();
    setWaterMarkHandlers();
</script>
<uc3:LiveZillaPublic ID="ctlLiveZillaPublic" runat="server" />
          
</asp:Content>

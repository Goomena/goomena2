﻿Imports Library.Public
Imports System.Data.SqlClient
Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Core.DLL
Imports Dating.Server.Datasets.DLL.DSLists

Public Class _Default20140624
    Inherits BasePage


    Protected Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            'If (_pageData Is Nothing) Then _pageData = New clsPageData("Default.aspx", Context)
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData("Default.aspx", Context, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property



    Protected Overrides Sub OnPreInit(ByVal e As EventArgs)
        MyBase.OnPreInit(e)
    End Sub




    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Dim errorInfo As String = "http://www.goomena.com:80/cdn.goomena.com/balance/media/widgetkit/widgets/slideshow/css/slideshow.css" 'test string
        'dim UseThreadAbort As Boolean=false
        'Dim  result As Boolean =CheckURLAndRedirectPermanent(errorInfo As String, UseThreadAbort As Boolean)
        Try
            If (Not Page.IsPostBack) Then

                If (Session("NoRedirectToMembersDefault") = True OrElse
                    Request.QueryString("vw") = "pub") Then

                    Session("NoRedirectToMembersDefault") = True

                ElseIf (clsCurrentContext.VerifyLogin() = True) Then
                    clsCurrentContext.RedirectToMembersDefault(False)

                ElseIf (HttpContext.Current.User.Identity.IsAuthenticated) Then
                    'clear session
                    clsCurrentContext.LogOff()
                    'ScriptManager.RegisterStartupScript(Me, Me.GetType(), "clearcookies", "deleteCookies();", True)
                End If


                'ShowMenMessageCollapsible(False)
                'ShowLadiesMessageCollapsible(False)
            End If
        Catch ex As Exception

        End Try


        Try

            Try
                If (Not String.IsNullOrEmpty(Request("logon_customer"))) Then

                    clsCurrentContext.ClearSession(Session)

                    Dim result As clsDataRecordLoginMemberReturn = gLogin.PerfomAdminLogin(Request("logon_customer"))
                    If (result IsNot Nothing AndAlso result.IsValid) Then
                        Session("IsAdminLogin") = True
                        Response.Redirect(ResolveUrl("~/Members/"), False)
                        HttpContext.Current.ApplicationInstance.CompleteRequest()
                        Return
                    Else
                        WebErrorMessageBox(Me, "User not found or not valid.")
                    End If

                End If
            Catch ex As Exception
            End Try

            If (Not Me.IsPostBack) Then
                LoadLAG()
            End If

            'BindMembersList()
            'LoadUsersList()

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try
    End Sub

    Private Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete

    End Sub


    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            ' bind on prerender, because Session("GEO_COUNTRY_POSTALCODE") in many cases fills from Root.master on master-load event
            'BindMembersList()


            '    If clsCurrentContext.VerifyLogin() = True Then
            '        If Not Session("FiredRedirectOnDefaultPage") Then
            '            Session("FiredRedirectOnDefaultPage") = True
            '            Response.Redirect("~/Members/default.aspx", False)
            '            HttpContext.Current.ApplicationInstance.CompleteRequest()
            '        End If
            '        'Else
            '        '    FormsAuthentication.SignOut()
            '    End If


        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "page_prerender")
        End Try

        Try
            Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

        Try
            LoadPhotos()

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "page_prerender")
        End Try

        Try
            ' create canonical link only when pageid param is present

            Dim canonicalHref As String = "/default.aspx"
            MyBase.CreateCanonicalLink(canonicalHref)

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "PreRender->canonical link")
        End Try
    End Sub


    Private Sub LoadPhotos()

        Dim photoid As String
        Dim photoname As String
        Dim address As String = Me.Request.Url.Scheme & "://photos.goomena.com/"
        Dim address2 As String
        Dim address3 As String
        Dim i As Integer
        Dim picture As New HtmlGenericControl
        Dim imageforwomen As New HtmlImage
        Dim profilename As String
        Dim checkList As New List(Of Integer)
        Dim pnlBottomPhotos1 As Control = Master.FindControl("Content")
        '     Dim topControlsCount As Integer = pnlMosaic.Controls.Count

        If (clsCurrentContext.VerifyLogin()) Then
            registerform.Visible = False
            'CustomCssClass = CustomCssClass & 
            'lgnfrm = pnlMosaic1.FindControl("regformcontainer").FindControl("regmidcontainer").FindControl("registerform")
            'lgnfrm.Style("display") = "none"
        End If


        Dim latitudeIn As Double? = Nothing
        Dim longitudeIn As Double? = Nothing

        Try

            'If (Session("GEO_COUNTRY_INFO") IsNot Nothing) Then
            '    Dim country As clsCountryByIP = Session("GEO_COUNTRY_INFO")
            Dim country As clsCountryByIP = clsCurrentContext.GetCountryByIP()
            If (country IsNot Nothing) Then

                Try
                    Dim pos As New clsGlobalPositionWCF(country.latitude, country.longitude)
                    latitudeIn = pos.latitude
                    longitudeIn = pos.longitude

                    'If (Not String.IsNullOrEmpty(country.latitude)) Then
                    '    Dim __latitudeIn As Double = 0
                    '    'country.latitude = country.latitude.Replace(".", ",")
                    '    If (Double.TryParse(country.latitude, __latitudeIn)) Then latitudeIn = __latitudeIn
                    '    If (__latitudeIn > 180 OrElse __latitudeIn < -180) Then
                    '        country.latitude = country.latitude.Replace(".", ",")
                    '        If (Double.TryParse(country.latitude, __latitudeIn)) Then latitudeIn = __latitudeIn
                    '    End If
                    'End If
                    'If (Not String.IsNullOrEmpty(country.longitude)) Then
                    '    Dim __longitudeIn As Double = 0
                    '    'country.longitude = country.longitude.Replace(".", ",")
                    '    If (Double.TryParse(country.longitude, __longitudeIn)) Then longitudeIn = __longitudeIn
                    '    If (__longitudeIn > 180 OrElse __longitudeIn < -180) Then
                    '        country.longitude = country.longitude.Replace(".", ",")
                    '        If (Double.TryParse(country.longitude, __longitudeIn)) Then longitudeIn = __longitudeIn
                    '    End If
                    'End If
                Catch ex As Exception
                    WebErrorSendEmail(ex, "Get lat-lon from ip.")
                End Try

            End If
            'End If


            If (Not latitudeIn.HasValue AndAlso Not longitudeIn.HasValue) Then
                Try

                    If (MasterProfileId > 0 AndAlso clsCurrentContext.VerifyLogin()) Then
                        If (SessionVariables.MemberData.latitude.HasValue) Then latitudeIn = SessionVariables.MemberData.latitude
                        If (SessionVariables.MemberData.longitude.HasValue) Then longitudeIn = SessionVariables.MemberData.longitude
                    End If
                Catch ex As Exception
                    WebErrorSendEmail(ex, "Get lat-lon from profile.")
                End Try
            End If

            If (Not latitudeIn.HasValue AndAlso Not longitudeIn.HasValue) Then
                Try

                    Dim pos As clsGlobalPositionWCF = clsGeoHelper.GetCountryMinPostcodeDataTable_Position(BasePage.GetGeoCountry(), Nothing, Nothing)
                    latitudeIn = pos.latitude
                    longitudeIn = pos.longitude

                    'Dim dtGEO As DataTable = clsGeoHelper.GetCountryMinPostcodeDataTable(MyBase.GetGeoCountry(), Nothing, Nothing)
                    'If (dtGEO.Rows.Count > 0 AndAlso Not IsDBNull(dtGEO.Rows(0)("latitude")) AndAlso Not IsDBNull(dtGEO.Rows(0)("longitude"))) Then
                    '    latitudeIn = clsNullable.DBNullToDecimal(dtGEO.Rows(0)("latitude"))
                    '    longitudeIn = clsNullable.DBNullToDecimal(dtGEO.Rows(0)("longitude"))
                    'End If

                Catch ex As Exception
                    WebErrorSendEmail(ex, "Get lat-lon from geo data.")
                End Try
            End If

        Catch ex As Exception
            WebErrorSendEmail(ex, "Get lat-lon info.")
        End Try
        Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection

            Dim dt As DataTable
            Dim sql As String = "EXEC [CustomerPhotos_Grid2]  @latitudeIn=@latitudeIn, @longitudeIn=@longitudeIn,@Country=@Country"
            Using cmd As SqlCommand = DataHelpers.GetSqlCommand(con, sql)



                cmd.Parameters.AddWithValue("@Country", BasePage.GetGeoCountry())
                Dim prm1 As SqlClient.SqlParameter = DataHelpers.GetSqlParam_LatitubeOrLongitude("@latitudeIn", latitudeIn)
                cmd.Parameters.Add(prm1)

                Dim prm2 As SqlClient.SqlParameter = DataHelpers.GetSqlParam_LatitubeOrLongitude("@longitudeIn", longitudeIn)
                cmd.Parameters.Add(prm2)

                'If (Not latitudeIn.HasValue AndAlso Not longitudeIn.HasValue) Then
                '    cmd.Parameters.AddWithValue("@latitudeIn", DBNull.Value)
                '    cmd.Parameters.AddWithValue("@longitudeIn", DBNull.Value)
                'Else
                '    cmd.Parameters.AddWithValue("@latitudeIn", latitudeIn.Value)
                '    cmd.Parameters.AddWithValue("@longitudeIn", longitudeIn.Value)
                'End If

                dt = DataHelpers.GetDataTable(cmd)
            End Using
            dt.Columns.Add("InUse", GetType(Boolean))

            Try

                Dim women As New List(Of Integer)
                Dim men As New List(Of Integer)

                For cnt = 0 To dt.Rows.Count - 1
                    Dim dr As DataRow = dt.Rows(cnt)

                    Dim CustomerID As Integer = dr("CustomerID")
                    If (checkList.Contains(CustomerID)) Then
                        Continue For
                    End If
                    Dim gender As Integer = dr("GenderId")
                    If (gender = 2) Then
                        If (women.Count > 59) Then
                            Continue For
                        Else
                            women.Add(cnt)
                        End If
                    ElseIf (gender = 1) Then
                        If (men.Count > 27) Then
                            Continue For
                        Else
                            men.Add(cnt)
                        End If
                    End If

                    checkList.Add(CustomerID)


                    'stis 88 photos exit
                    If (checkList.Count > 87) Then Exit For

                Next


                Dim photos_panel As Control = pnlMosaic
                Dim picSuffix As String = ""
                Dim currentPhotosCount As Integer = 0
                i = 0
                For cnt = 0 To checkList.Count - 1
                    If (currentPhotosCount > 87) Then Exit For
                    Dim drs As DataRow() = dt.Select("CustomerId=" & checkList(cnt) & "and (InUse is null)")
                    If (drs.Length = 0) Then Continue For

                    i += 1
                    picture = photos_panel.FindControl("pic" & i & picSuffix)
                    While (i < 87 AndAlso (picture Is Nothing OrElse Not picture.Visible) AndAlso (currentPhotosCount < 38))
                        i += 1
                        picture = photos_panel.FindControl("pic" & i & picSuffix)
                    End While
                    If (picture Is Nothing) Then Exit For


                    Dim dr As DataRow = drs(0)
                    dr("InUse") = True
                    currentPhotosCount = currentPhotosCount + 1

                    photoid = dr("CustomerID").ToString
                    photoname = dr("FileName")
                    profilename = dr("LoginName")
                    '   Dim country As String = dr("Country")
                    Dim region As String = dr("Region")
                    Dim birthday As Date = dr("Birthday")
                    Dim city As String = dr("City")
                    Dim age As Integer = ProfileHelper.GetCurrentAge(birthday)
                    Dim gender As Integer = dr("GenderId")
                    address2 = address + photoid
                    address3 = address2 + "/thumbs/" + photoname
                    If Me.IsHTTPS AndAlso address3.StartsWith("http:") Then address3 = "https" & address3.Remove(0, "http".Length)

                    picture.Attributes.Add("style", "background-image:url(" + address3 + ")")
                    picture.Attributes.Add("data-URL", address3)
                    picture.Attributes.Add("data-ID", photoid)
                    picture.Attributes.Add("data-LoginName", profilename)
                    picture.Attributes.Add("data-Country", dr("CountryName")) 'ProfileHelper.GetCountryName(country))
                    picture.Attributes.Add("data-Region", region)
                    picture.Attributes.Add("data-Age", age)
                    picture.Attributes.Add("data-City", city)
                    picture.Attributes.Add("data-Gender", gender)

                    If (cnt = checkList.Count - 1 AndAlso currentPhotosCount < 87) Then
                        cnt = 0
                    End If
                Next

            Catch ex As Exception
                Throw New Exception(ex.Message, ex)
            Finally

            End Try
            checkList.Clear()




            Dim memberctr As HyperLink
            Dim loginname As HtmlGenericControl

            Dim sql1 As String = "EXEC [CustomerPhotos_FrontPage2]  @latitudeIn=@latitudeIn, @longitudeIn=@longitudeIn,@Country=@Country"
            Using cmd = DataHelpers.GetSqlCommand(con, sql1)


                cmd.Parameters.AddWithValue("@Country", BasePage.GetGeoCountry())
                cmd.Parameters.AddWithValue("@latitudeIn", DBNull.Value)
                cmd.Parameters.AddWithValue("@longitudeIn", DBNull.Value)
                dt = DataHelpers.GetDataTable(cmd)
            End Using
            dt.Columns.Add("InUse", GetType(Boolean))

            Try

                Dim women As New List(Of Integer)
                Dim men As New List(Of Integer)

                For cnt = 0 To dt.Rows.Count - 1
                    Dim dr As DataRow = dt.Rows(cnt)

                    Dim CustomerID As Integer = dr("CustomerID")
                    If (checkList.Contains(CustomerID)) Then
                        Continue For
                    End If
                    Dim gender As Integer = dr("GenderId")
                    If (gender = 2) Then
                        If (women.Count > 3) Then
                            Continue For
                        Else
                            women.Add(CustomerID)
                        End If
                    ElseIf (gender = 1) Then
                        If (men.Count > 3) Then
                            Continue For
                        Else
                            men.Add(CustomerID)
                        End If
                    End If

                    checkList.Add(CustomerID)


                    'stis 7 photos exit
                    If (checkList.Count > 7) Then Exit For

                Next


                i = 0
                For cnt = 0 To men.Count - 1

                    Dim drs As DataRow() = dt.Select("CustomerId=" & men(cnt) & "and (InUse is null)")
                    If (drs.Length = 0) Then Continue For

                    Dim dr As DataRow = drs(0)
                    dr("InUse") = True

                    i += 1


                    imageforwomen = pnlBottomPhotos1.FindControl("imgtag" & i)
                    loginname = pnlBottomPhotos1.FindControl("imagename" & i)
                    photoid = dr("CustomerID").ToString
                    photoname = dr("FileName")
                    profilename = dr("LoginName")
                    address2 = address + photoid
                    address3 = address2 + "/thumbs/" + photoname
                    If Me.IsHTTPS AndAlso address3.StartsWith("http:") Then address3 = "https" & address3.Remove(0, "http".Length)

                    imageforwomen.Attributes.Add("src", address3)
                    loginname.InnerHtml = profilename.ToString()
                    memberctr = pnlBottomPhotos1.FindControl("anchornum" & i)
                    If (HttpContext.Current.User.Identity.IsAuthenticated) Then
                        memberctr.NavigateUrl = "Members/Profile.aspx?p=" & HttpUtility.UrlEncode(profilename)
                        memberctr.Attributes.Add("onclick", "ShowLoading();")
                    Else
                        Dim url As String = UrlUtils.GetValidRoutingProfileURI(HttpUtility.UrlEncode(profilename)) 'HttpUtility.UrlEncode(profilename).Replace("+", " ").Replace("<", "&lt;").Replace(">", "&gt;")
                        url = MyBase.LanguageHelper.GetPublicURL("/profile/" & url, MyBase.GetLag())
                        memberctr.NavigateUrl = url
                        memberctr.Attributes.Add("onclick", "ShowLoading();")
                    End If
                Next


                ' i = 0
                For cnt = 0 To women.Count - 1

                    Dim drs As DataRow() = dt.Select("CustomerId=" & women(cnt) & "and (InUse is null)")
                    If (drs.Length = 0) Then Continue For

                    Dim dr As DataRow = drs(0)
                    dr("InUse") = True

                    i += 1

                    imageforwomen = pnlBottomPhotos1.FindControl("imgtag" & i)
                    loginname = pnlBottomPhotos1.FindControl("imagename" & i)
                    photoid = dr(0).ToString
                    photoname = dr(1)
                    profilename = dr(2)
                    address2 = address + photoid
                    address3 = address2 + "/thumbs/" + photoname
                    If Me.IsHTTPS AndAlso address3.StartsWith("http:") Then address3 = "https" & address3.Remove(0, "http".Length)

                    imageforwomen.Attributes.Add("src", address3)
                    loginname.InnerHtml = profilename.ToString()
                    memberctr = pnlBottomPhotos1.FindControl("anchornum" & i)
                    If (HttpContext.Current.User.Identity.IsAuthenticated) Then
                        memberctr.NavigateUrl = "Members/Profile.aspx?p=" & UrlUtils.GetValidRoutingProfileURI(HttpUtility.UrlEncode(profilename)) 'HttpUtility.UrlEncode(profilename)
                        memberctr.Attributes.Add("onclick", "ShowLoading();")
                    Else
                        'memberctr.NavigateUrl = "profile/" & HttpUtility.UrlEncode(profilename)
                        'memberctr.NavigateUrl = MyBase.LanguageHelper.GetPublicURL("/profile/" & HttpUtility.UrlEncode(profilename), MyBase.GetLag())
                        Dim url As String = UrlUtils.GetValidRoutingProfileURI(HttpUtility.UrlEncode(profilename)) 'HttpUtility.UrlEncode(profilename).Replace("+", " ")
                        url = MyBase.LanguageHelper.GetPublicURL("/profile/" & url, MyBase.GetLag())
                        memberctr.NavigateUrl = url
                        memberctr.Attributes.Add("onclick", "ShowLoading();")
                    End If

                Next


            Catch ex As Exception
                Throw New Exception(ex.Message, ex)
            End Try
        End Using
    End Sub



    Private Sub LoadPhotos_Grid()

        'Dim myConn As SqlConnection
        'Dim AppDBconnectionString As SqlConnection
        'Dim myCmd As SqlCommand
        'Dim myCmd2 As SqlCommand
        Dim photoid As String
        Dim photoname As String
        Dim address As String = Me.Request.Url.Scheme & "://photos.goomena.com/"
        Dim address2 As String
        Dim address3 As String
        Dim i As Integer
        'Dim pic As String
        Dim picture As New HtmlGenericControl
        Dim imageforwomen As New HtmlImage
        Dim loginname As HtmlGenericControl
        Dim profilename As String
        Dim memberctr As HyperLink
        Dim lgnfrm As HtmlContainerControl
        Dim checkList As New List(Of Integer)
        Dim pnlBottomPhotos1 As Control = Master.FindControl("Content")
        Dim pnlMosaic1 As Control = Master.FindControl("Content")


        If (HttpContext.Current.User.Identity.IsAuthenticated) Then
            lgnfrm = pnlMosaic1.FindControl("regformcontainer").FindControl("regmidcontainer").FindControl("registerform")
            lgnfrm.Style("display") = "none"
        End If


        Dim latitudeIn As Double? = Nothing
        Dim longitudeIn As Double? = Nothing

        Try

            'If (Session("GEO_COUNTRY_INFO") IsNot Nothing) Then
            '    Dim country As clsCountryByIP = Session("GEO_COUNTRY_INFO")
            Dim country As clsCountryByIP = clsCurrentContext.GetCountryByIP()
            If (country IsNot Nothing) Then

                Try
                    Dim pos As New clsGlobalPositionWCF(country.latitude, country.longitude)
                    latitudeIn = pos.latitude
                    longitudeIn = pos.longitude

                    'If (Not String.IsNullOrEmpty(country.latitude)) Then
                    '    Dim __latitudeIn As Double = 0
                    '    'country.latitude = country.latitude.Replace(".", ",")
                    '    If (Double.TryParse(country.latitude, __latitudeIn)) Then latitudeIn = __latitudeIn
                    '    If (__latitudeIn > 180 OrElse __latitudeIn < -180) Then
                    '        country.latitude = country.latitude.Replace(".", ",")
                    '        If (Double.TryParse(country.latitude, __latitudeIn)) Then latitudeIn = __latitudeIn
                    '    End If
                    'End If
                    'If (Not String.IsNullOrEmpty(country.longitude)) Then
                    '    Dim __longitudeIn As Double = 0
                    '    'country.longitude = country.longitude.Replace(".", ",")
                    '    If (Double.TryParse(country.longitude, __longitudeIn)) Then longitudeIn = __longitudeIn
                    '    If (__longitudeIn > 180 OrElse __longitudeIn < -180) Then
                    '        country.longitude = country.longitude.Replace(".", ",")
                    '        If (Double.TryParse(country.longitude, __longitudeIn)) Then longitudeIn = __longitudeIn
                    '    End If
                    'End If
                Catch ex As Exception
                    WebErrorSendEmail(ex, "Get lat-lon from ip.")
                End Try

            End If
            'End If


            If (Not latitudeIn.HasValue AndAlso Not longitudeIn.HasValue) Then
                Try

                    If (MasterProfileId > 0 AndAlso clsCurrentContext.VerifyLogin()) Then
                        If (SessionVariables.MemberData.latitude.HasValue) Then latitudeIn = SessionVariables.MemberData.latitude
                        If (SessionVariables.MemberData.longitude.HasValue) Then longitudeIn = SessionVariables.MemberData.longitude
                    End If
                Catch ex As Exception
                    WebErrorSendEmail(ex, "Get lat-lon from profile.")
                End Try
            End If

            If (Not latitudeIn.HasValue AndAlso Not longitudeIn.HasValue) Then
                Try

                    Dim pos As clsGlobalPositionWCF = clsGeoHelper.GetCountryMinPostcodeDataTable_Position(BasePage.GetGeoCountry(), Nothing, Nothing)
                    latitudeIn = pos.latitude
                    longitudeIn = pos.longitude

                    'Dim dtGEO As DataTable = clsGeoHelper.GetCountryMinPostcodeDataTable(MyBase.GetGeoCountry(), Nothing, Nothing)

                    'If (dtGEO.Rows.Count > 0 AndAlso Not IsDBNull(dtGEO.Rows(0)("latitude")) AndAlso Not IsDBNull(dtGEO.Rows(0)("longitude"))) Then
                    '    latitudeIn = clsNullable.DBNullToDecimal(dtGEO.Rows(0)("latitude"))
                    '    longitudeIn = clsNullable.DBNullToDecimal(dtGEO.Rows(0)("longitude"))
                    'End If

                Catch ex As Exception
                    WebErrorSendEmail(ex, "Get lat-lon from geo data.")
                End Try
            End If

        Catch ex As Exception
            WebErrorSendEmail(ex, "Get lat-lon info.")
        End Try
        Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection

            Dim dt As DataTable
            Dim sql As String = "EXEC [CustomerPhotos_Grid2]  @latitudeIn=@latitudeIn, @longitudeIn=@longitudeIn,@Country=@Country"
            Using cmd As SqlCommand = DataHelpers.GetSqlCommand(con, sql)


                cmd.Parameters.AddWithValue("@Country", BasePage.GetGeoCountry())
                Dim prm1 As SqlClient.SqlParameter = DataHelpers.GetSqlParam_LatitubeOrLongitude("@latitudeIn", latitudeIn)
                cmd.Parameters.Add(prm1)

                Dim prm2 As SqlClient.SqlParameter = DataHelpers.GetSqlParam_LatitubeOrLongitude("@longitudeIn", longitudeIn)
                cmd.Parameters.Add(prm2)

                'If (Not latitudeIn.HasValue AndAlso Not longitudeIn.HasValue) Then
                '    cmd.Parameters.AddWithValue("@latitudeIn", DBNull.Value)
                '    cmd.Parameters.AddWithValue("@longitudeIn", DBNull.Value)
                'Else
                '    cmd.Parameters.AddWithValue("@latitudeIn", latitudeIn.Value)
                '    cmd.Parameters.AddWithValue("@longitudeIn", longitudeIn.Value)
                'End If

                dt = DataHelpers.GetDataTable(cmd)
            End Using
            dt.Columns.Add("InUse", GetType(Boolean))

            Try

                Dim women As New List(Of Integer)
                Dim men As New List(Of Integer)

                For cnt = 0 To dt.Rows.Count - 1
                    Dim dr As DataRow = dt.Rows(cnt)

                    Dim CustomerID As Integer = dr("CustomerID")
                    If (checkList.Contains(CustomerID)) Then
                        Continue For
                    End If
                    Dim gender As Integer = dr("GenderId")
                    If (gender = 2) Then
                        If (women.Count > 59) Then
                            Continue For
                        Else
                            women.Add(cnt)
                        End If
                    ElseIf (gender = 1) Then
                        If (men.Count > 27) Then
                            Continue For
                        Else
                            men.Add(cnt)
                        End If
                    End If

                    checkList.Add(CustomerID)


                    'stis 88 photos exit
                    If (checkList.Count > 87) Then Exit For

                Next

                Dim currentPhotosCount As Integer = 0
                i = 0
                For cnt = 0 To checkList.Count - 1
                    If (currentPhotosCount > 87) Then Exit For
                    Dim drs As DataRow() = dt.Select("CustomerId=" & checkList(cnt) & "and (InUse is null)")
                    If (drs.Length = 0) Then Continue For

                    i += 1
                    picture = pnlMosaic1.FindControl("pic" & i)
                    While (i < 87 AndAlso (picture Is Nothing OrElse Not picture.Visible))
                        i += 1
                        picture = pnlMosaic1.FindControl("pic" & i)
                    End While
                    If (picture Is Nothing) Then Exit For


                    Dim dr As DataRow = drs(0)
                    dr("InUse") = True
                    currentPhotosCount = currentPhotosCount + 1

                    photoid = dr("CustomerID").ToString
                    photoname = dr("FileName")
                    profilename = dr("LoginName")
                    '       Dim country As String = dr("Country")
                    Dim region As String = dr("Region")
                    Dim birthday As Date = dr("Birthday")
                    Dim city As String = dr("City")
                    Dim age As Integer = ProfileHelper.GetCurrentAge(birthday)
                    Dim gender As Integer = dr("GenderId")
                    address2 = address + photoid
                    address3 = address2 + "/thumbs/" + photoname
                    If Me.IsHTTPS AndAlso address3.StartsWith("http:") Then address3 = "https" & address3.Remove(0, "http".Length)

                    picture.Attributes.Add("style", "background-image:url(" + address3 + ")")
                    picture.Attributes.Add("data-URL", address3)
                    picture.Attributes.Add("data-ID", photoid)
                    picture.Attributes.Add("data-LoginName", profilename)
                    picture.Attributes.Add("data-Country", dr("CountryName")) 'ProfileHelper.GetCountryName(country))
                    picture.Attributes.Add("data-Region", region)
                    picture.Attributes.Add("data-Age", age)
                    picture.Attributes.Add("data-City", city)
                    picture.Attributes.Add("data-Gender", gender)

                    If (cnt = checkList.Count - 1 AndAlso currentPhotosCount < 87) Then
                        cnt = 0
                    End If
                Next

            Catch ex As Exception
                Throw New Exception(ex.Message, ex)
            Finally
            End Try
            checkList.Clear()


            Dim sql1 As String = "EXEC [CustomerPhotos_FrontPage2]  @latitudeIn=@latitudeIn, @longitudeIn=@longitudeIn,@Country=@Country"
            Using cmd = DataHelpers.GetSqlCommand(con, sql1)
                cmd.Parameters.AddWithValue("@Country", BasePage.GetGeoCountry())
                cmd.Parameters.AddWithValue("@latitudeIn", DBNull.Value)
                cmd.Parameters.AddWithValue("@longitudeIn", DBNull.Value)
                dt = DataHelpers.GetDataTable(cmd)
            End Using
            dt.Columns.Add("InUse", GetType(Boolean))

            Try

                Dim women As New List(Of Integer)
                Dim men As New List(Of Integer)

                For cnt = 0 To dt.Rows.Count - 1
                    Dim dr As DataRow = dt.Rows(cnt)

                    Dim CustomerID As Integer = dr("CustomerID")
                    If (checkList.Contains(CustomerID)) Then
                        Continue For
                    End If
                    Dim gender As Integer = dr("GenderId")
                    If (gender = 2) Then
                        If (women.Count > 3) Then
                            Continue For
                        Else
                            women.Add(CustomerID)
                        End If
                    ElseIf (gender = 1) Then
                        If (men.Count > 3) Then
                            Continue For
                        Else
                            men.Add(CustomerID)
                        End If
                    End If

                    checkList.Add(CustomerID)


                    'stis 7 photos exit
                    If (checkList.Count > 7) Then Exit For

                Next


                i = 0
                For cnt = 0 To men.Count - 1

                    Dim drs As DataRow() = dt.Select("CustomerId=" & men(cnt) & "and (InUse is null)")
                    If (drs.Length = 0) Then Continue For

                    Dim dr As DataRow = drs(0)
                    dr("InUse") = True

                    i += 1


                    imageforwomen = pnlBottomPhotos1.FindControl("imgtag" & i)
                    loginname = pnlBottomPhotos1.FindControl("imagename" & i)
                    photoid = dr("CustomerID").ToString
                    photoname = dr("FileName")
                    profilename = dr("LoginName")
                    address2 = address + photoid
                    address3 = address2 + "/thumbs/" + photoname
                    If Me.IsHTTPS AndAlso address3.StartsWith("http:") Then address3 = "https" & address3.Remove(0, "http".Length)

                    imageforwomen.Attributes.Add("src", address3)
                    loginname.InnerHtml = profilename.ToString()
                    memberctr = pnlBottomPhotos1.FindControl("anchornum" & i)
                    If (HttpContext.Current.User.Identity.IsAuthenticated) Then
                        memberctr.NavigateUrl = "Members/Profile.aspx?p=" & HttpUtility.UrlEncode(profilename)
                        memberctr.Attributes.Add("onclick", "ShowLoading();")
                    Else
                        Dim url As String = UrlUtils.GetValidRoutingProfileURI(HttpUtility.UrlEncode(profilename)) 'HttpUtility.UrlEncode(profilename).Replace("+", " ").Replace("<", "&lt;").Replace(">", "&gt;")
                        url = MyBase.LanguageHelper.GetPublicURL("/profile/" & url, MyBase.GetLag())
                        memberctr.NavigateUrl = url
                        memberctr.Attributes.Add("onclick", "ShowLoading();")
                    End If
                Next


                ' i = 0
                For cnt = 0 To women.Count - 1

                    Dim drs As DataRow() = dt.Select("CustomerId=" & women(cnt) & "and (InUse is null)")
                    If (drs.Length = 0) Then Continue For

                    Dim dr As DataRow = drs(0)
                    dr("InUse") = True

                    i += 1

                    imageforwomen = pnlBottomPhotos1.FindControl("imgtag" & i)
                    loginname = pnlBottomPhotos1.FindControl("imagename" & i)
                    photoid = dr(0).ToString
                    photoname = dr(1)
                    profilename = dr(2)
                    address2 = address + photoid
                    address3 = address2 + "/thumbs/" + photoname
                    If Me.IsHTTPS AndAlso address3.StartsWith("http:") Then address3 = "https" & address3.Remove(0, "http".Length)

                    imageforwomen.Attributes.Add("src", address3)
                    loginname.InnerHtml = profilename.ToString()
                    memberctr = pnlBottomPhotos1.FindControl("anchornum" & i)
                    If (HttpContext.Current.User.Identity.IsAuthenticated) Then
                        memberctr.NavigateUrl = "Members/Profile.aspx?p=" & UrlUtils.GetValidRoutingProfileURI(HttpUtility.UrlEncode(profilename)) 'HttpUtility.UrlEncode(profilename)
                        memberctr.Attributes.Add("onclick", "ShowLoading();")
                    Else
                        'memberctr.NavigateUrl = "profile/" & HttpUtility.UrlEncode(profilename)
                        'memberctr.NavigateUrl = MyBase.LanguageHelper.GetPublicURL("/profile/" & HttpUtility.UrlEncode(profilename), MyBase.GetLag())
                        Dim url As String = UrlUtils.GetValidRoutingProfileURI(HttpUtility.UrlEncode(profilename)) 'HttpUtility.UrlEncode(profilename).Replace("+", " ")
                        url = MyBase.LanguageHelper.GetPublicURL("/profile/" & url, MyBase.GetLag())
                        memberctr.NavigateUrl = url
                        memberctr.Attributes.Add("onclick", "ShowLoading();")
                    End If

                Next


            Catch ex As Exception
                Throw New Exception(ex.Message, ex)
            Finally

            End Try
        End Using
    End Sub

    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
    End Sub



    Protected Sub LoadLAG()
        Try
            '    Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            ltrMenMessage.Text = CurrentPageData.GetCustomString("ltrMenMessage3")
            ltrMenMessageCollapse.Text = CurrentPageData.GetCustomString("ltrMenMessageCollapse3")
            ltrLadiesMessage.Text = CurrentPageData.GetCustomString("ltrLadiesMessage3")
            ltrLadiesMessageCollapse.Text = CurrentPageData.GetCustomString("ltrLadiesMessageCollapse3")
            lblBeMemberNow.Text = CurrentPageData.GetCustomString("lblBeMemberNow")
            lblNotAMember.Text = CurrentPageData.GetCustomString("lblNotAMember")
            lblChooseGender.Text = CurrentPageData.GetCustomString("lblChooseGender")
            lblRegister.Text = CurrentPageData.GetCustomString("lblRegister")
            RdioMale.Text = CurrentPageData.GetCustomString("RdioMale")
            RdioFemale.Text = CurrentPageData.GetCustomString("RdioFemale")
            lblAlreadyMember.Text = CurrentPageData.GetCustomString("lblAlreadyMember")
            lblConnect.Text = CurrentPageData.GetCustomString("lblConnect")
            lblFBConnect.Text = CurrentPageData.GetCustomString("lblFBConnect")
            lbFBNote.Text = CurrentPageData.GetCustomString("lbFBNote")

            lblWellcomeMsg.Text = CurrentPageData.GetCustomString("lblWellcomeMsg_Def_No_GRID")
            lblMembersCount.Text = CurrentPageData.GetCustomString("lblMembersCount")

            'lblinside.Text = Me.CurrentPageData.GetCustomString("lblinside")
            'lnkBrowseGenerous.Text = CurrentPageData.GetCustomString("lnkBrowseGenerous")
            'lnkBrowseAttractive.Text = CurrentPageData.GetCustomString("lnkBrowseAttractive")


            'ltrMenMessage_collapsible.Text = CurrentPageData.GetCustomString("ltrMenMessage_collapsible")
            'ltrMenMessage_collapsible_title.Text = CurrentPageData.GetCustomString("ltrMenMessage_collapsible_title")
            'ltrLadiesMessage_collapsible.Text = CurrentPageData.GetCustomString("ltrLadiesMessage_collapsible")
            'ltrLadiesMessage_collapsible_title.Text = CurrentPageData.GetCustomString("ltrLadiesMessage_collapsible_title")

            'CType(Page.Master, _RootMaster).PopulateMenuData(lvwLocation, "PriceInCountry")
            'CType(Page.Master.Master, _RootMaster).PopulateMenuData(lvwLocation, "PriceInCountry")

            If (Me.IsHTTPS) Then
                Select Case Me.GetLag()
                    Case "GR"
                        iMobileBanner.Src = "https://cdn.goomena.com/Images/MobileBanners/goomena-androind-gr.png"
                    Case "TR"
                        iMobileBanner.Src = "https://cdn.goomena.com/Images/MobileBanners/goomena-androind-tu.png"
                    Case Else
                        iMobileBanner.Src = "https://cdn.goomena.com/Images/MobileBanners/goomena-androind-en.png"
                End Select

            Else
                Select Case Me.GetLag()
                    Case "GR"
                        iMobileBanner.Src = "http://cdn.goomena.com/Images/MobileBanners/goomena-androind-gr.png"
                    Case "TR"
                        iMobileBanner.Src = "http://cdn.goomena.com/Images/MobileBanners/goomena-androind-tu.png"
                    Case Else
                        iMobileBanner.Src = "http://cdn.goomena.com/Images/MobileBanners/goomena-androind-en.png"
                End Select

            End If


        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub



    'Private Sub LoadUsersList()
    '    Try
    '        Dim dataTbl As DataTable = DataHelpers.GetApprovedUsersDatatable(5)
    '        Dim tblMales As DataTable = dataTbl.Clone()
    '        Dim tblFemales As DataTable = dataTbl.Clone()
    '        Dim foundRows As DataRow()


    '        foundRows = dataTbl.Select(" Genderid = " &  ProfileHelper.gMaleGender.GenderId)
    '        For Each row As DataRow In foundRows
    '            tblMales.LoadDataRow(row.ItemArray, True)

    '            Dim dr As DataRow = tblMales.Rows(tblMales.Rows.Count - 1)
    '            If (dr("ImageFileName").ToString() = "") Then
    '                dr("ImageFileName") = ResolveUrl(ProfileHelper. ProfileHelper.Male_DefaultImage)
    '            Else
    '                dr("ImageFileName") = ResolveUrl("~/Members/Photo.aspx?u=") & dr("LoginName").ToString()
    '            End If
    '        Next

    '        foundRows = dataTbl.Select(" Genderid = " &  ProfileHelper.gFemaleGender.GenderId)
    '        For Each row As DataRow In foundRows
    '            tblFemales.LoadDataRow(row.ItemArray, True)

    '            Dim dr As DataRow = tblFemales.Rows(tblFemales.Rows.Count - 1)
    '            If (dr("ImageFileName").ToString() = "") Then
    '                dr("ImageFileName") = ResolveUrl(ProfileHelper.Female_DefaultImage)
    '            Else
    '                dr("ImageFileName") = ResolveUrl("~/Members/Photo.aspx?u=") & dr("LoginName").ToString()
    '            End If
    '        Next


    '        lvwGenerous.DataSource = tblMales
    '        lvwGenerous.DataBind()


    '        lvwAttractive.DataSource = tblFemales
    '        lvwAttractive.DataBind()


    '    Catch ex As Exception
    '        WebErrorMessageBox(Me, ex, "Failed to load LoadUsersList")
    '    End Try

    'End Sub



    'Private Sub BindMembersList()

    '    ' membersGenerous
    '    Try
    '        Dim zipStr As String = Session("GEO_COUNTRY_POSTALCODE")
    '        If (String.IsNullOrEmpty(zipStr)) Then zipStr = clsGeoHelper.GetCountryMinPostcode("GR")

    '        Dim dt As DataTable = clsSearchHelper.GetMembersToSearchUsingGenderDataTable(ProfileStatusEnum.Approved, ProfileHelper.gMaleGender.GenderId, zipStr, True, 5, 60)
    '        If (dt.Rows.Count < 5) Then dt = clsSearchHelper.GetMembersToSearchUsingGenderDataTable(ProfileStatusEnum.Approved, ProfileHelper.gMaleGender.GenderId, zipStr, True, 5, 1000)
    '        If (dt.Rows.Count < 5) Then dt = clsSearchHelper.GetMembersToSearchUsingGenderDataTable(ProfileStatusEnum.Approved, ProfileHelper.gMaleGender.GenderId, zipStr, True, 5)

    '        For Each dr As DataRow In dt.Rows

    '            Dim uli As New clsUserListItem()
    '            uli.ProfileID = dr("ProfileID")
    '            uli.MirrorProfileID = dr("MirrorProfileID")
    '            uli.LoginName = dr("LoginName")
    '            uli.Genderid = dr("GenderId")
    '            uli.ProfileViewUrl = ResolveUrl("~/Register.aspx") '& _

    '            If (Not dr.IsNull("FileName")) Then
    '                uli.ImageUrl = ProfileHelper.GetProfileImageURL(dr("CustomerId"), dr("FileName"), uli.Genderid, True)
    '                uli.ImageFileName = dr("FileName")
    '            Else
    '                uli.ImageUrl = ProfileHelper.GetDefaultImageURL(uli.Genderid)
    '                uli.ImageFileName = System.IO.Path.GetFileName(uli.ImageUrl)
    '            End If

    '            membersGenerous.UsersList.Add(uli)
    '        Next

    '        membersGenerous.DataBind()
    '    Catch ex As Exception
    '        WebErrorMessageBox(Me, ex, "")

    '        membersGenerous.UsersList = Nothing
    '        membersGenerous.DataBind()
    '    End Try



    '    'membersAttractive
    '    Try

    '        'Dim sqlProc As String = " EXEC	[dbo].[GetMembersToSearchUsingGender] " & _
    '        '    " 	@ReturnRecordsWithStatus = " & CType(ProfileStatusEnum.Approved, Integer) & "," & _
    '        '    "	@NumberOfRecordsToReturn = 5 ," & _
    '        '    "	@Gender = " & ProfileHelper.gFemaleGender.GenderId & ""

    '        'Dim dt As DataTable = DataHelpers.GetDataTable(sqlProc)
    '        Dim zipStr As String = clsGeoHelper.GetCountryMinPostcode("GR")
    '        Dim dt As DataTable = clsSearchHelper.GetMembersToSearchUsingGenderDataTable(ProfileStatusEnum.Approved, ProfileHelper.gFemaleGender.GenderId, zipStr, True, 5, 60)
    '        If (dt.Rows.Count < 5) Then dt = clsSearchHelper.GetMembersToSearchUsingGenderDataTable(ProfileStatusEnum.Approved, ProfileHelper.gFemaleGender.GenderId, zipStr, True, 5, 1000)
    '        If (dt.Rows.Count < 5) Then dt = clsSearchHelper.GetMembersToSearchUsingGenderDataTable(ProfileStatusEnum.Approved, ProfileHelper.gFemaleGender.GenderId, zipStr, True, 5)

    '        For Each dr As DataRow In dt.Rows

    '            Dim uli As New clsUserListItem()
    '            uli.ProfileID = dr("ProfileID")
    '            uli.MirrorProfileID = dr("MirrorProfileID")
    '            uli.LoginName = dr("LoginName")
    '            uli.Genderid = dr("GenderId")
    '            uli.ProfileViewUrl = ResolveUrl("~/Register.aspx") ' ResolveUrl("~/Members/Profile.aspx?p=" & dr("LoginName"))

    '            If (Not dr.IsNull("FileName")) Then
    '                uli.ImageUrl = ProfileHelper.GetProfileImageURL(dr("CustomerId"), dr("FileName"), uli.Genderid, True)
    '                uli.ImageFileName = dr("FileName")
    '            Else
    '                uli.ImageUrl = ProfileHelper.GetDefaultImageURL(uli.Genderid)
    '                uli.ImageFileName = System.IO.Path.GetFileName(uli.ImageUrl)
    '            End If

    '            membersAttractive.UsersList.Add(uli)
    '        Next

    '        membersAttractive.DataBind()
    '    Catch ex As Exception
    '        WebErrorMessageBox(Me, ex, "")

    '        membersAttractive.UsersList = Nothing
    '        membersAttractive.DataBind()
    '    End Try


    'End Sub



    'Private Sub ShowMenMessageCollapsible(show As Boolean)
    '    If (show) Then
    '        lnkExpColl1.Text = "-"
    '        h2Title1.Attributes("title") = globalStrings.GetCustomString("msg_ClickToCollapse", GetLag())
    '        pe_form1.Attributes.CssStyle.Remove("display")
    '    Else
    '        lnkExpColl1.Text = "+"
    '        h2Title1.Attributes("title") = globalStrings.GetCustomString("msg_ClickToExpand", GetLag())
    '        pe_form1.Attributes.CssStyle("display") = "none"
    '    End If
    'End Sub


    'Private Sub ShowLadiesMessageCollapsible(show As Boolean)
    '    If (show) Then
    '        lnkExpColl2.Text = "-"
    '        h2Title2.Attributes("title") = globalStrings.GetCustomString("msg_ClickToCollapse", GetLag())
    '        pe_form2.Attributes.CssStyle.Remove("display")
    '    Else
    '        lnkExpColl2.Text = "+"
    '        h2Title2.Attributes("title") = globalStrings.GetCustomString("msg_ClickToExpand", GetLag())
    '        pe_form2.Attributes.CssStyle("display") = "none"
    '    End If
    'End Sub



    Private Sub HyperRegister_ServerClick(sender As Object, e As System.EventArgs) Handles HyperRegister.ServerClick
        If RdioMale.Checked Then
            Response.Redirect("Register.aspx?gender=male")
        ElseIf RdioFemale.Checked Then
            Response.Redirect("Register.aspx?gender=female")
        Else
            mainregdivctr.Attributes.Add("data-hint", "Please select Gender before you continue!")
            mainregdivctr.Attributes.Add("class", "hint--error hint--always mainregdivctr")
        End If
    End Sub
End Class
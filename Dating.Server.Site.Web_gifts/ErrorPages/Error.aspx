﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/ErrorPages/err.Master"
    CodeBehind="Error.aspx.vb" Inherits="Dating.Server.Site.Web._Error" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .dxb
        {
            font-size: 18px;
            color: #fff;
            width: 180px;
            height: 42px;
            background: url('//cdn.goomena.com/Images2/mbr2/buttons-uclogin.png') no-repeat #fff;
            background-position: 0% 0px;
            line-height: 40px;
            font-weight: normal;
            cursor: pointer;
        }

        .row-content > table
        {
            margin-left: 320px;
        }
            .dxb:hover
            {
                background-position: 0% -64px;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <script type="text/javascript">
        // <![CDATA[
        (function () {
            $('.second-row').css("height", "auto");
            $('.second-row').css("padding-bottom", "5px");
            $('.pub-top-welcome').css("float", "right");
            $('.pub-top-welcome').css("padding-top", "15px");
            $('.pub-top-welcome > a').css("margin-right", "185px");
            $('.pub-top-welcome > a').css("font-size", "18px");
            $('.pub-top-welcome span.dxeBase').css("display", "none");
            $('.pub-top-welcome + div').css("top", "3px");
            $('.pub-top-welcome + div').css("right", "-5px");
            $('.pub-top-welcome + div').css("bottom", "");
        })();
        // ]]>
    </script>

    <div class="lfloat" style="margin:0 20px">
        <img alt="404 Error Pages" src="//cdn.goomena.com/Images2/mbr2/oops.png" />
    </div>
    <%--<div class="lfloat">
        <dx:ASPxLabel ID="lbHTMLBody" runat="server" ClientIDMode="AutoID" EncodeHtml="False"
            Text="Sorry, but something gone really wrong!" Font-Size="14px">
        </dx:ASPxLabel>
    </div>--%>
    <div class="clear">
    </div>
   <%-- <h2 style="text-shadow: none; border: none; font-family: Lucida Grande,Lucida Sans Unicode,Verdana,Arial,sans-serif;">
        <asp:Label ID="lblHeader" runat="server" Text="Error!" /><span class="loginHereLink"></span></h2>
    <dx:ASPxLabel ID="lbHTMLBody" runat="server" ClientIDMode="AutoID" EncodeHtml="False"
        Text="Something gone really wrong!">
    </dx:ASPxLabel>--%>
</asp:Content>

﻿Imports System.Web
Imports System.Web.Services
Imports System.Data.SqlClient
Imports Dating.Server.Core.DLL

Public Class initPayment
    Implements System.Web.IHttpHandler
    Dim cmd As New SqlCommand
    Dim sqlStr As String = ""
    Public transID As String = ""
    Public cmdGetIdentity As SqlCommand
    Dim m_DS As New DataSet()
    Dim m_DatRow As DataRow
    Dim m_HasIdentityColumn As Boolean
    Dim m_IdentityID As Integer
    Dim okURL As String = "/IPN/ok.aspx"


    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        okURL = UrlUtils.GetFullURI(UrlUtils.GetCurrentHostURL(context.Request.Url), okURL).ToString()

        Dim haserror As Boolean = True
        Dim i As Integer = 0
        While haserror And i < 5
            Try

                haserror = False

                ''If Helper.GetHistoryPageName() = "" Or Helper.GetHistoryPageName = "loader" Then
                ''    Helper.SetHistoryPageName("initpayment")
                ''Else
                ''    context.Response.Redirect("notok.aspx")
                ''End If
                Dim currency As String = ""
                Try
                    currency = context.Request("currency")
                Catch ex As Exception

                End Try
                Dim productCodeSTR As String = context.Request("ProductCode")
                Dim productCode As String = ""
                Dim siteID As Integer = context.Request("SalesSiteID")
                If Len(siteID) = 0 Then
                    siteID = 1
                End If

                '' Getting product id
                'sqlStr = "SELECT SalesSiteProductID FROM SalesSitesProducts WHERE SalesSiteID = " & siteID & " AND productCode = '" & productCodeSTR & "' AND SaleTypeID = 1"
                'productCode = DataHelpers.ExecuteScalar(sqlStr, ModGlobals.ConnectionString)
                ''cmd = New SqlCommand(sqlStr, New SqlConnection(ModGlobals.ConnectionString))
                ''Try
                ''    cmd.Connection.Close()
                ''Catch ex As Exception

                ''End Try
                ''cmd.Connection.Open()
                ''productCode = cmd.ExecuteScalar()
                ''cmd.Connection.Close()

                Dim epochProduct As clsEpochProduct = clsEpochProduct.GetProductBySiteCode(productCodeSTR)
                productCode = epochProduct.SalesSiteProductID


                If productCode <> "" Then

                    Dim paymentProviderID As Integer = 0
                    Dim url As String = ""

                    If Len(context.Request("PaymentMethod")) AndAlso context.Request("PaymentMethod") <> "" Then

                        'sqlStr = "SELECT TOP(1) PayProviderID FROM PayProviders WHERE Title LIKE '" & context.Request("PaymentMethod") & "%' AND SiteID = " & siteID
                        'paymentProviderID = DataHelpers.ExecuteScalar(sqlStr, ModGlobals.ConnectionString)

                        ''cmd = New SqlCommand(sqlStr, New SqlConnection(ModGlobals.ConnectionString))
                        ''Try
                        ''    cmd.Connection.Close()
                        ''Catch ex As Exception

                        ''End Try
                        ''cmd.Connection.Open()
                        ''paymentProviderID = cmd.ExecuteScalar()
                        ''cmd.Connection.Close()


                        'If paymentProviderID = 0 Then
                        '    sqlStr = "SELECT TOP(1) PayProviderID FROM PayProviders WHERE ShortName LIKE '" & context.Request("PaymentMethod") & "%' AND SiteID = " & siteID
                        '    paymentProviderID = DataHelpers.ExecuteScalar(sqlStr, ModGlobals.ConnectionString)

                        '    'cmd = New SqlCommand(sqlStr, New SqlConnection(ModGlobals.ConnectionString))
                        '    'Try
                        '    '    cmd.Connection.Close()
                        '    'Catch ex As Exception

                        '    'End Try
                        '    'cmd.Connection.Open()
                        '    'paymentProviderID = cmd.ExecuteScalar()
                        '    'cmd.Connection.Close()
                        'End If

                    Else

                        sqlStr = "Select DefaultPayProviderID FROM SalesSitesProducts WHERE SalesSiteProductID = " & productCode
                        productCode = DataHelpers.ExecuteScalar(sqlStr, ModGlobals.ConnectionString)
                        'cmd = New SqlCommand(sqlStr, New SqlConnection(ModGlobals.ConnectionString))
                        'Try
                        '    cmd.Connection.Close()
                        'Catch
                        'End Try
                        'cmd.Connection.Open()
                        'paymentProviderID = cmd.ExecuteScalar()
                        'cmd.Connection.Close()

                    End If

                    GetPaymentURL(paymentProviderID, url)
                    Dim salesSiteExtraData As String = ""
                    salesSiteExtraData = salesSiteExtraData & "promoCode=" & context.Request("promoCode")
                    salesSiteExtraData = salesSiteExtraData & ";customReferrer=" & context.Request("CustomReferrer")
                    salesSiteExtraData = salesSiteExtraData & ";downloadURL=" & context.Request("downloadURL")

                    ''sqlStr = "INSERT INTO UNI_PayTransactions (PayProviderID, SalesSiteID, SalesSiteProductID, CustomerID, PaymentDateTime, SaleTypeID, SalesSiteExtraData) VALUES " _
                    ''      & "(" & paymentProviderID & ", " & context.Request("SalesSiteID") & ", " & productCode & ", " & context.Request("CustomerID") & ", GETDATE(), 1, '" & salesSiteExtraData & "')"

                    ''cmd = New SqlCommand(sqlStr, sqlcon)
                    ''Try
                    '' cmd.Connection.Close()
                    ''Catch ex As Exception

                    ''End Try

                    ''        cmd.Connection.Open()
                    ''       cmd.ExecuteNonQuery()

                    ''sqlStr = "SELECT SCOPE_IDENTITY()"
                    ''cmd = New SqlCommand(sqlStr, sqlcon)

                    ''Dim transID As Integer = cmd.ExecuteScalar
                    ''cmd.Connection.Close()
                    ''Dim transID As Integer = 0

                    Dim rnd As New Random
                    Dim IsRebill As Boolean = False
                    Try
                        If (Not String.IsNullOrEmpty(context.Request("isrebill"))) Then
                            IsRebill = context.Request("isrebill")
                        End If
                    Catch ex As Exception

                    End Try

                    transID = (DateTime.Now - New DateTime(1970, 1, 1)).TotalMilliseconds.ToString  ' Date.Now.ToShortDateString & "_" & Date.Now.TimeOfDay.ToString & "_" & rnd.Next(0, 10000)
                    If paymentProviderID = 6 Then
                        transID = Date.Now.ToShortDateString & "_" & Date.Now.TimeOfDay.ToString & "_" & rnd.Next(0, 10000)
                    End If
                    If paymentProviderID = 8 Then
                        transID = Date.Now.ToShortDateString & "_" & Date.Now.TimeOfDay.ToString & "_" & rnd.Next(0, 10000)
                    End If
                    If paymentProviderID = 11 Or paymentProviderID = 12 Then
                        transID = context.Request("CustomerID") & rnd.Next(0, 1000) & siteID
                        transID = transID.Replace("/", "").Replace(":", "").Replace(".", "")
                    End If
                    If paymentProviderID = 42 Then
                        transID = context.Request("CustomerID") & rnd.Next(0, 100000) & siteID
                        transID = transID.Replace("/", "").Replace(":", "").Replace(".", "")
                    End If
                    If paymentProviderID = 46 Then
                        transID = context.Request("CustomerID") & rnd.Next(0, 100000) & siteID
                        transID = transID.Replace("/", "").Replace(":", "").Replace(".", "")
                    End If

                    transID = transID.Replace(",", ".")
                    sqlStr = "INSERT INTO UNI_PayTransactions (PayProviderID, SalesSiteID, SalesSiteProductID, CustomerID, LoginName, PaymentDateTime, SaleTypeID, CustomerIP, SalesSiteExtraData, UniqueID, isRebill, CustomReferrer) VALUES " _
                          & "(" & paymentProviderID & ", " & context.Request("SalesSiteID") & ", " & productCode & ", " & context.Request("CustomerID") & ", '" & context.Request("LoginName") & "', GETDATE(), 1, '" & context.Request.ServerVariables("REMOTE_ADDR") & "', '" & salesSiteExtraData & "', '" & transID & "', '" & IsRebill & "', '" & context.Request("CustomReferrer") & "')"

                    DataHelpers.ExecuteNonQuery(sqlStr, ModGlobals.ConnectionString)
                    'cmd = New SqlCommand(sqlStr, New SqlConnection(ModGlobals.ConnectionString))
                    'Try
                    '    cmd.Connection.Close()
                    'Catch ex As Exception
                    'End Try
                    'cmd.Connection.Open()
                    'cmd.ExecuteNonQuery()
                    'cmd.Connection.Close()




                    ''Dim crs As New ClsRecordSetSQL(ModGlobals.ConnectionString)
                    ''If crs.Open("SELECT * FROM UNI_PayTransactions WHERE PayTransactionID=0") Then
                    ''    crs.AddNew()

                    ''    crs.SetValue("PayProviderID", paymentProviderID)
                    ''    crs.SetValue("SalesSiteID", siteID)
                    ''    crs.SetValue("SalesSiteProductID", productCode)
                    ''    crs.SetValue("CustomerID", context.Request("CustomerID"))
                    ''    crs.SetValue("LoginName", context.Request("LoginName"))
                    ''    crs.SetValue("PaymentDateTime", Now)
                    ''    crs.SetValue("SaleTypeID", 1)
                    ''    crs.SetValue("CustomerIP", context.Request.ServerVariables("REMOTE_ADDR"))
                    ''    crs.SetValue("SalesSiteExtraData", salesSiteExtraData)
                    ''    crs.SetValue("UniqueID", transID)
                    ''    crs.SetValue("IsRebill", IsRebill)
                    ''    crs.Save()

                    ''End If

                    ''crs.Close()
                    ''crs = Nothing
                    ''crs = New ClsRecordSetSQL(ModGlobals.ConnectionString)
                    ''If crs.Open("SELECT * FROM UNI_PayTransactions WHERE UniqueID='" & transID & "'") Then
                    ''    transID = crs.GetValue("PayTransactionID") & context.Request("CustomerID")

                    ''End If

                    ''crs.Close()


                    Dim oPost As New RemotePost()

                    '' Exaples of call UniPAY
                    '' http://www.soundsoft.com/PAY/pay.ashx?pid=12&pp=1&cid=1234&promo=ddddd&exd=dddddddd
                    '' pid = to product ID apo tin DB tou UniPAY
                    '' pp = Use ton PayProvider me ID 1 px Paypal alios tha xrisimophisi ton default
                    '' cid = Customer ID
                    '' promo= Promo Code
                    '' exd = (optionasl) extra data
                    '' Me krifo to soundsoft....
                    '' Kanoume to paymix.com na kani redirect sto soundsoft 

                    oPost.Url = url
                    'sqlStr = "select * from SalesSites inner join SalesSitesProducts on SalesSites.SalesSiteID = SalesSitesProducts.SalesSiteID WHERE SalesSitesProducts.SalesSiteProductID = " & productCode
                    'Dim rs As SqlDataReader
                    'cmd = New SqlCommand(sqlStr, New SqlConnection(ModGlobals.ConnectionString))
                    'Try
                    '    cmd.Connection.Close()
                    'Catch ex As Exception

                    'End Try
                    'cmd.Connection.Open()
                    'rs = cmd.ExecuteReader()
                    'AddHandler oPost.errorAddingField, AddressOf sendmail

                    'While rs.Read
                    '    If paymentProviderID = 6 Then
                    '        oPost.Add("itemName", rs("CO2ProductID"))
                    '        oPost.Add("type", context.Request("type"))
                    '    ElseIf paymentProviderID = 8 Or paymentProviderID = 22 Or paymentProviderID = 32 Or paymentProviderID = 35 Then
                    '        oPost.Add("itemName", rs("paymentWallApplicationID"))
                    '        oPost.Add("email", context.Request("LoginName"))
                    '        oPost.Add("type", context.Request("type"))
                    '        Dim widget As String = "p2_1"
                    '        Dim key As String = "20bd0226eb38e48d0150fb70dbed9aff"
                    '        Try
                    '            widget = context.Request("widget")
                    '        Catch ex As Exception

                    '        End Try
                    '        If widget Is Nothing Then
                    '            widget = "p2_1"
                    '        End If
                    '        If siteID = 5 Or siteID = 9 Then
                    '            key = "20bd0226eb38e48d0150fb70dbed9aff"
                    '        Else
                    '            key = "dc9e09b0c11ed6427d1b0dd0f2d5a731"
                    '        End If
                    '        oPost.Add("widget", widget)
                    '        oPost.Add("key", key)
                    '    ElseIf paymentProviderID = 9 Then
                    '        oPost.Add("itemName", productCodeSTR)
                    '    ElseIf paymentProviderID = 26 Or paymentProviderID = 39 Or paymentProviderID = 40 Then

                    oPost.Add("itemName", epochProduct.ProviderProductCode) 'rs("epochProductCode"))
                    oPost.Add("type", context.Request("type"))
                    oPost.Add("name", context.Request("fn"))
                    oPost.Add("email", context.Request("eml"))
                    oPost.Add("zip", context.Request("zip"))
                    oPost.Add("city", context.Request("cit"))
                    oPost.Add("state", context.Request("reg"))
                    oPost.Add("camcharge", context.Request("camcharge"))
                    oPost.Add("productDesc", epochProduct.Description) ' rs("productDescription"))
                    oPost.Add("okURL", okURL) 'rs("URL"))

                    'ElseIf paymentProviderID = 13 Then
                    '    oPost.Add("itemName", rs("EJID"))
                    'ElseIf paymentProviderID = 14 Or paymentProviderID = 18 Or paymentProviderID = 37 Or paymentProviderID = 42 Or paymentProviderID = 46 Then
                    '    oPost.Add("itemName", rs("RegNowProductID"))
                    '    oPost.Add("providerid", paymentProviderID)
                    'ElseIf paymentProviderID = 29 Or paymentProviderID = 45 Then
                    '    oPost.Add("itemName", rs("eSellerateProductID"))

                    'ElseIf paymentProviderID = 16 Or paymentProviderID = 28 Or paymentProviderID = 38 Or paymentProviderID = 41 Then
                    '    Dim kagiStoreID As String = ""
                    '    Try
                    '        If siteID = 8 Then
                    '            kagiStoreID = "6FJLV"
                    '        ElseIf siteID = 10 Then
                    '            kagiStoreID = "6FJQS"
                    '        Else
                    '            If paymentProviderID = 41 Then
                    '                kagiStoreID = "6FJQS"
                    '            Else
                    '                kagiStoreID = "6FJKP"
                    '            End If

                    '        End If
                    '    Catch ex As Exception

                    '    End Try
                    '    oPost.Add("kagistoreid", kagiStoreID)
                    '    oPost.Add("itemName", rs("KagiProductID"))
                    'ElseIf paymentProviderID = 27 Then
                    '    oPost.Add("itemName", rs("swRegProductID"))

                    'Else
                    '    oPost.Add("itemName", rs("ProductDescription"))

                    'End If
                    'If context.Request("CustomerID") = 35 Then
                    '    oPost.Add("amount", 1)
                    'Else
                    '    If context.Request("amount") = 0 Then
                    '        If paymentProviderID = 4 Then
                    '            oPost.Add("amount", rs("Amount2"))
                    '        Else

                    '            oPost.Add("amount", rs("Amount"))
                    '        End If
                    '    Else
                    '        If rs("amount") = 0 And rs("ProductDescription").ToString.Contains("Reseller") Then

                    '            oPost.Add("amount", context.Request("amount"))
                    '        Else
                    '            oPost.Add("amount", rs("Amount"))
                    '        End If
                    '    End If

                    'End If

                    oPost.Add("amount", epochProduct.MoneyAmount)
                    oPost.Add("TransID", transID)
                    If currency = "" Then
                        oPost.Add("currency", epochProduct.Currency) ' rs("Currency"))
                    Else
                        oPost.Add("currency", currency)
                    End If

                    oPost.Add("customerID", context.Request("CustomerID"))
                    oPost.Add("productCode", context.Request("ProductCode"))
                    oPost.Add("ourl", context.Request("downloadURL"))
                    oPost.Add("countryCode", context.Request("countryCode"))
                    oPost.Add("siteID", siteID)

                    '            End While

                    'rs.Close()
                    'cmd.Connection.Close()
                    ' ''Helper.SetHistoryPageName(url)
                    oPost.Post()
                Else
                    context.Response.Write(productCodeSTR)
                End If


            Catch ex As Threading.ThreadAbortException
            Catch ex As Exception

                WebErrorSendEmail(ex, "initPayment")

                i += 1
                haserror = True

            End Try
        End While

    End Sub

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

    Private Sub GetPaymentURL(ByVal paymentProviderID As Integer, ByRef url As String)
        url = "/IPN/epochSetPayment.aspx"

        'sqlStr = "Select SetPaymentURL FROM PayProviders WHERE PayProviderID = " & paymentProviderID
        'cmd = New SqlCommand(sqlStr, New SqlConnection(ModGlobals.ConnectionString))

        'cmd.Connection.Open()
        'url = cmd.ExecuteScalar()
        'cmd.Connection.Close()
    End Sub

    Private Sub RowUpdatedSetIdentity(ByVal sender As Object, ByVal e As SqlRowUpdatedEventArgs)

        If e.Status = UpdateStatus.Continue AndAlso _
           e.StatementType = StatementType.Insert Then
            transID = CInt(cmdGetIdentity.ExecuteScalar)
        End If
        Exit Sub

    End Sub

    Private Sub sendmail(ByVal fieldName As String, ByVal fieldValue As String, ByVal errorMessage As String)
        Try
            'Dim mail As New clsMyMail
            'mail.SendMailUseGmail("info@zevera.com", "savvas@000.gr", "IMPORTANT!: ERROR on Adding Field", "No ORDER ID " & vbCrLf & "FieldName: " & fieldName & vbCrLf & "ErrorMessage: " & errorMessage)

            clsMyMail.TrySendMail(clsCurrentContext.ExceptionsEmail, "IMPORTANT!: ERROR on Adding Field",
                               "No ORDER ID " & vbCrLf & _
                               "FieldName: " & fieldName & vbCrLf & _
                               "FieldValue: " & If(fieldValue IsNot Nothing, fieldValue, "[NOTHING]") & vbCrLf & _
                               "ErrorMessage: " & errorMessage)
        Catch
        End Try
    End Sub

End Class



﻿
function findComponent(result, type, short) {
    var address = result.address_components;
    if (short == 'short') { var short = 'short_name'; }
    else { var short = 'long_name'; }
    if (address) {
        for (var i = 0; i < address.length; i++) {
            if (address[i]['types'].indexOf(type) != -1) { return address[i][short]; }
        }
    }
    return null;
}
var max_repeating = function (str, max) {
    var chars = str.toLowerCase().split(''),
        repeating = 0,
        prev_char = null;
    for (var i = 0; i < chars.length; i++) {
        repeating = (chars[i] == prev_char) ? repeating + 1 : 0;
        if (repeating == max) return false;
        prev_char = chars[i];
    }
    return true;
};

$(document).ready(function () {
    if ($('#search_visit_location').length > 0) {
        var visit_input = $('#search_visit_location')[0];
        var autocomplete_visit = new google.maps.places.Autocomplete(visit_input);
        autocomplete_visit.setTypes(['(regions)']);
        google.maps.event.addListener(autocomplete_visit, 'place_changed', function () {
            var place = autocomplete_visit.getPlace();
            var text_place = $('#search_visit_location').val();
            var loc_country = findComponent(place, 'country');
            var loc_country_short = findComponent(place, 'country', 'short');
            var loc_region = findComponent(place, 'administrative_area_level_1');
            var loc_subregion = findComponent(place, 'administrative_area_level_2');
            var loc_city = findComponent(place, 'locality');
            var loc_subcity = findComponent(place, 'sublocality');

            var isCurrentCity = false;
            var isCurrentCountry = false;
            var isCurrentRegion = false;
            if ($('#liLocationString').length > 0) {
                var hdf = $('input[type="hidden"]', '#liLocationString');
                var __city, __country, __region;
                for (var i = 0; i < hdf.length; i++) {
                    var __id = hdf[i].id;
                    if (!stringIsEmpty(__id) && __id.indexOf("hdfCity") > -1) {
                        // get city name
                        __city = $(hdf[i]).val();
                    }
                    if (!stringIsEmpty(__id) && __id.indexOf("hdfCountry") > -1) {
                        // get country name
                        __country = $(hdf[i]).val();
                    }
                    if (!stringIsEmpty(__id) && __id.indexOf("hdfRegion") > -1) {
                        // get region name
                        __region = $(hdf[i]).val();
                    }
                }
                if (!stringIsEmpty(__city) && !stringIsEmpty(__country) && !stringIsEmpty(__region)) {
                    /*
                    0Object { long_name="Glifada", short_name="Glifada", types=[2]}
                    1Object { long_name="Glifada", short_name="Glifada", types=[1]}
                    2Object { long_name="Glyfada", short_name="Glyfada", types=[1]}
                    3Object { long_name="Notios Tomeas Athinon", short_name="Notios Tomeas Athinon", types=[2]}
                    4Object { long_name="Attica", short_name="Attica", types=[2]}
                    5Object { long_name="Attica", short_name="Attica", types=[2]}
                    6Object { long_name="Greece", short_name="GR", types=[2]}
                    */
                    var ln = place.address_components.length;
                    var place0 = (ln > 0 ? place.address_components[0].long_name : '');
                    var place1 = (ln > 1 ? place.address_components[1].long_name : '');
                    var place2 = (ln > 2 ? place.address_components[2].long_name : '');
                    var place3 = (ln > 3 ? place.address_components[3].long_name : '');
                    var place4 = (ln > 4 ? place.address_components[4].long_name : '');
                    var place5 = (ln > 5 ? place.address_components[5].long_name : '');
                    var place6 = (ln > 6 ? place.address_components[6].long_name : '');

                    // check city
                    var myRe = new RegExp(__city, "gi");
                    var res = place0.match(myRe);
                    if (res != null && res.length > 0) { isCurrentCity = true; }
                    if (!isCurrentCity) {
                        res = place1.match(myRe);
                        if (res != null && res.length > 0) {
                            isCurrentCity = true;
                        }
                        else {
                            res = place2.match(myRe);
                            if (res != null && res.length > 0) { isCurrentCity = true; }
                        }
                    }

                    // check region
                    myRe = new RegExp(__region, "gi");
                    res = place3.match(myRe);
                    if (res != null && res.length > 0) { isCurrentRegion = true; }
                    if (!isCurrentRegion) {
                        res = place4.match(myRe);
                        if (res != null && res.length > 0) {
                            isCurrentRegion = true;
                        }
                        else {
                            res = place5.match(myRe);
                            if (res != null && res.length > 0) { isCurrentRegion = true; }
                        }
                    }

                    // check country
                    myRe = new RegExp(__country, "gi");
                    res = loc_country.match(myRe);
                    if (res != null && res.length > 0) { isCurrentCountry = true; }
                }
            }

            if ((isCurrentCity == false || isCurrentRegion == false || isCurrentCountry == false) && $('#location-' + place.id).length == 0) {
                var param = $.toJSON({
                    "location_id": place.id,
                    "locality_type": place.address_components[0].types[0],
                    "desc": place.formatted_address,
                    "ref": place.reference,
                    "loc_country": (stringIsEmpty(loc_country) ? "" : loc_country),
                    "loc_country_short": (stringIsEmpty(loc_country_short) ? "" : loc_country_short),
                    "loc_region": (stringIsEmpty(loc_region) ? "" : loc_region),
                    "loc_subregion": (stringIsEmpty(loc_subregion) ? "" : loc_subregion),
                    "loc_city": (stringIsEmpty(loc_city) ? "" : loc_city),
                    "loc_subcity": (stringIsEmpty(loc_subcity) ? "" : loc_subcity)
                });
                $.ajax({
                    type: "POST",
                    url: "/Members/json.aspx/WantVisitSave",
                    data: param,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: true
                });

                var html =
				'<p>' +
				'<span>' + text_place + '</span>' +
				'<input type="hidden" value="' + place.id + '" class="locationId" id="location-' + place.id + '"/>' +
				'<a href="#" class="remove_wants" title="Remove" data-location="' + place.id + '"><img  src="https://cdn.goomena.com/Images/spacer10.png" border="0" alt="Remove" /></a>' +
				'</p><div class="clear"></div>';
                $('#wants_to_visit').before(html);

                if (window["chkTravelOnDate"] != null) {
                    if (chkTravelOnDate.GetChecked() == false) chkTravelOnDate.SetChecked(true);
                }
            }

            setTimeout(function () {
                $('#search_visit_location').val('').focus();
            }, 100);
        });
    }

    // Delete wants to visit record
    $(document).on("click", ".remove_wants", function () {
        var remove_block = $(this);
        var loc_id = remove_block.attr('data-location');
        var param = $.toJSON({ "location_id": (stringIsEmpty(loc_id) ? "" : loc_id) });
        $.ajax({
            type: "POST",
            url: "/Members/json.aspx/WantVisitDelete",
            data: param,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true
        });
        $(this).parents('p').remove();
        if ($('p', '#liTravelingPlaces .form_right').length == 0) {
            if (window["chkTravelOnDate"] != null)  chkTravelOnDate.SetChecked(false); 
        }
        return false;
    });

    $(document).on("keydown", "#wants_to_visit", function (e) {
        e = e || window.event;
        var charCode = e.which || e.keyCode;
        if (charCode === 13) return false;
    });

});
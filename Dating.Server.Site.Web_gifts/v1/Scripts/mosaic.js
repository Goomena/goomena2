

$(".mosaic")
    .children()
    .mouseover(function (e) {

        $(".PhotoInfoWrap").removeClass("hidden");
        var loginName = $(this).attr("data-LoginName");
        var country = $(this).attr("data-Country");
        var age = $(this).attr("data-Age");
        var region = $(this).attr("data-Region");
        var city = $(this).attr("data-City");
        var bg = $(this).attr("data-URL");
        var gender = $(this).attr("data-Gender");

        $(".PhotoInfoWrap").click(function () {
            ShowLoading();
            var ln = loginName;
            if (typeof encodeURIComponent !== 'undefined') {
                ln = encodeURIComponent(loginName);
            }
            else if (typeof encodeURI !== 'undefined') {
                ln = encodeURI(loginName);
            }
            ln = ln.replace(".", "%2e").replace("+", "%20");
            window.location = profileBaseUrl + ln;
        }).css("cursor", "pointer");

        $(this).click(function () {
            ShowLoading();
            var ln = loginName;
            if (typeof encodeURIComponent !== 'undefined') {
                ln = encodeURIComponent(loginName);
            }
            else if (typeof encodeURI !== 'undefined') {
                ln = encodeURI(loginName);
            }
            ln = ln.replace(".", "%2e").replace("+", "%20");
            window.location = profileBaseUrl + ln;
        }).css("cursor", "pointer");

        $(".InfoImg").attr("src", bg);
        $(".PhotoInfoWrap").position({
            my: "center center",
            at: "center center",
            of: this
        });
        $(".PhotoInfoWrap").addClass("animate");
        $(".PhotoInfoWrap").css("z-index", "10");
        $(".ProfileID").html(loginName);

        if (!stringIsEmpty(region) && !stringIsEmpty(city) && !stringIsEmpty(country)) {
            $(".ProfileCity").html(region + ", " + city + ", " + country);
        }
        else if (!stringIsEmpty(city) && !stringIsEmpty(country)) {
            $(".ProfileCity").html(city + ", " + country);
        }
        else if (!stringIsEmpty(region) && !stringIsEmpty(country)) {
            $(".ProfileCity").html(region + ", " + country);
        }

        $(".ProfileAgeText").html("age" + '<br/>');
        $(".ProfileAge").html(age);
        if (gender == "1") {
            $(".ProfileID,.ProfileAgeWrap").css("background", "#13a8e4");
        }
        if (gender == "2") {
            $(".ProfileID,.ProfileAgeWrap").css("background", "#ED145B");
        }
    })
    .mouseleave(function () {
        $(".PhotoInfoWrap").removeClass("animate");
    });

$(".PhotoInfoWrap").mouseover(function (e) { $(".PhotoInfoWrap").removeClass("hidden"); });
$(".PhotoInfoWrap").mouseleave(function (e) {
    $(".PhotoInfoWrap").hide(0, 'linear', function (e) {
        $(".PhotoInfoWrap").addClass("hidden").css("display", "");
    });
});
$(".mosaic").mouseleave(function (e) { $(".PhotoInfoWrap").addClass("hidden"); });


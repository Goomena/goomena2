
var loading_htm_path = '', setLevelPopUp_ClientID = '', lblPhotoLevel_ClientID = '';

function profileViewLoad(imgVipId, VIPTooltip) {
    $(function () {
        var imgVip = $("#" + imgVipId);
        imgVip.tooltip({
            tooltipClass: "tooltip-VIP",
            content: VIPTooltip,
            show: { delay: 700 }
        });
        imgVip.tooltip("option", "hide", { duration: 1000 });
        imgVip.tooltip("option", "position", { my: "left top", at: "left bottom", collision: "flipfit" });

        var last = $('.line-vert3', ".female-buttons .btn-wrap").last();
        if (last.length == 1) {
            $('.line-vert3', ".female-buttons .btn-wrap").show();
            last.hide();
        };

        last = $('.line-vert3', ".male-buttons .btn-wrap").last();
        if (last.length == 1) {
            $('.line-vert3', ".male-buttons .btn-wrap").show();
            last.hide();
        };
    });
}

function CloseSharingPopUp() {
    try {
        var win = setLevelPopUp_ClientID;
        window[win].DoHideWindow(0);
    }
    catch (e) { }
}
function cbpnlPhotosLevelText_EndCallback(s, e) {
    HideLoading();
    setLevelPopUp.SetWindowPopupElementID(setLevelPopUp.GetWindow(0), lblPhotoLevel_ClientID);
}

function OnPhotosDisplayLevelChanged(s, e) {
    LoadingPanel.Hide();
    cbpnlPhotos.PerformCallback();
    ShowLoading();
}


function cbpnlPhotos_EndCallback(s, e) {
    HideLoading();
    CloseSharingPopUp();
    setPopupWindowUI();
    cbpnlPhotosLevelText.PerformCallback();
    ShowLoading();
}


function loadUserMap(ifrUserMap_src, memberPositionId) {
    $(document).ready(function () {
        /* Every time the window is scrolled ... */
        $(window).scroll(function () {
            if ($('#MapArea').css('display') == 'none') {
                var bottom_of_object = $('#MapAreaScroll').offset().top + $('#MapAreaScroll').outerHeight() + 100;
                var bottom_of_window = $(window).scrollTop() + $(window).height();
                if (bottom_of_window > bottom_of_object) {
                    $('#MapArea').show();
                    runExtLinks_PV();
                }
            }
        });
    });


    function runExtLinks_PV() {
        var mapIframe = '<iframe id="ifrUserMap" style="width:693px; height:250px;" src="' + ifrUserMap_src + '"></iframe>'
        $('#MapArea').append(mapIframe);
        //$('#'+memberPositionId).show();
    }
}

function openPopupSharePhoto2(obj, headerText) {
    var wdt = 670, hgt = 619;
    if (typeof wdt === 'undefined' || wdt == null) wdt = 1000
    if (typeof hgt === 'undefined' || hgt == null) hgt = 594
    $.fancybox({
        //'titlePosition': 'inside',
        'width': wdt + 'px',
        'height': hgt + 'px',
        'autoScale': false,
        'transitionIn': 'none',
        'transitionOut': 'none',
        'padding': 0,
        'type': 'iframe',
        'href': obj.href,
        'title': headerText,
        'wrapCSS': 'private-photo',
        helpers: {
            title: {
                type: 'inside'
            }
        }
    });
}
function setFancyboxTitle(headerText) {
    $('div.fancybox-title.fancybox-title-inside-wrap').html(headerText);
}

function setPopupWindowUI() {
    function m__setPopupClass() {
        var popup = setLevelPopUp
        var divId = popup.name + '_PW0';
        $('#' + divId)
        .addClass('fancybox-popup')
        .prepend($('<div class="header-line"></div>'));
        var closeId = popup.name + '_HCB0Img';
        $('#' + closeId).addClass('fancybox-popup-close').css('right', '-46px');
        var hdrId = popup.name + '_PWH0T';
        $('#' + hdrId).addClass('fancybox-popup-header');
        //var frmId = popup.name + '_CSD0';
        //$('#' + frmId).addClass('iframe-container');
    }

    function m__setPopupClass_UploadPhotoPopUp() {
        var popup = UploadPhotoPopUp
        var divId = popup.name + '_PW-1';
        $('#' + divId)
        .addClass('fancybox-popup')
        .prepend($('<div class="header-line"></div>'));
        var closeId = popup.name + '_HCB-1Img';
        $('#' + closeId).addClass('fancybox-popup-close');
        var hdrId = popup.name + '_PWH-1T';
        $('#' + hdrId).addClass('fancybox-popup-header');
        //var frmId = popup.name + '_CSD-1';
        //$('#' + frmId).addClass('iframe-container');
    }

    setTimeout(m__setPopupClass, 300);
    setTimeout(m__setPopupClass_UploadPhotoPopUp, 300);
}


function checkProfileButtons() {
    var len = $('div.btn-wrap', '#profile_buttons').length;
    if (len > 4) {
        $('#profile_buttons').addClass('full-width');
    }
}



function openPopupSharePhoto(contentUrl, obj, headerText, wdt, hgt, params) {
    var popup = window["popupWhatIs"]
    popup.SetContentUrl(loading_htm_path);

    if (headerText != null) popup.SetHeaderText(headerText);
    if (obj != null) popup.SetPopupElementID(obj.id);
    popup.SetContentUrl(contentUrl);
    popup.Show();

    if (typeof wdt === 'undefined' || wdt == null) wdt = 1000
    if (typeof hgt === 'undefined' || hgt == null) hgt = 594

    popup.SetSize(wdt, hgt);
    popup.UpdatePosition();

    popup.Closing.AddHandler(__Closing_Remove);
    function __Closing_Remove(s, e) {
        try {
            s.SetContentUrl('about:blank');
            //__removePopupFooter();
            s.Closing.RemoveHandler(__Closing_Remove);
        }
        catch (e) { }
    }
}

var msg_ClickToExpand = '', msg_ClickToCollapse = '', cancelHtml = '', editHtml = '', hdfDefaultPath = '';
var isChangeDefault = false;

jQuery.fn.photos_center = function () {
    this.css("position", "absolute");
    this.css("top", Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) + $(window).scrollTop()) + "px");
    this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) + $(window).scrollLeft()) + "px");
    return this;
}

function confirmDelete(msg) {
    if (confirm(msg)) {
        ShowLoading();
        return true;
    }
    return false;
}
function OnPubPhotoDisplayLevelChanged(s, e) {
    try {
        var imgid = s.uniqueID.split("_")[1];
        window["editPhotoWindow_" + imgid].DoHideWindow(0);
    }
    catch (e) { }
    HideLoading();
    cbpnlPhotos.PerformCallback("ctlid_" + s.uniqueID + "_lvl_" + s.GetValue());
    ShowLoading();
}


function OnPrivatePhotoDisplayLevelChanged(s, e) {
    try {
        var imgid = s.uniqueID.split("_")[1];
        window["editPhotoWindow_" + imgid].DoHideWindow(0);
    }
    catch (e) { }
    HideLoading();
    cbpnlPhotos.PerformCallback("ctlid_" + s.uniqueID + "_lvl_" + s.GetValue());
    ShowLoading();
}

function runPhotosCollapsible() {
    jQuery(function ($) {
        $('.mp_info_container').each(function () {
            var pe_box = this;

            $('.collapsibleHeader', pe_box).click(function () {
                $('.mp_info', pe_box).toggle('fast', function () {
                    // Animation complete.
                    var isVisible = ($('.mp_info', pe_box).css('display') == 'none');
                    if (isVisible) {
                        $('.togglebutton', pe_box).text("+").removeClass('open');
                        $('.collapsibleHeader', pe_box).attr("title", msg_ClickToExpand);
                    }
                    else {
                        $('.togglebutton', pe_box).text("-").addClass('open');
                        $('.collapsibleHeader', pe_box).attr("title", msg_ClickToCollapse);
                    }
                });

            });
        });
    });
}



function ChangeDefaultImg(o) {
    isChangeDefault = false;
    if ($(o).html() != cancelHtml) {
        var lnks = $('.btnDefault', '.public-photos .d_big_photo');
        if (lnks.length > 0) {
            isChangeDefault = true;
            $(o).html(cancelHtml);
            lnks.show();
            $('.mp_links', '.public-photos .d_big_photo').addClass('change-default').show();
            var items = $('.mp_links', '.public-photos .d_big_photo')
            for (var c = 0; c < items.length; c++) {
                $(items[c]).css({
                    top: centerElemVert(items[c])
                });
            }
            coverPageWithVeil(true);
        }
    }
    else if ($(o).html() == cancelHtml) {
        $(o).html(editHtml);
        var lnks = $('.btnDefault', '.public-photos .d_big_photo');
        if (lnks.length > 0) {
            coverPageWithVeil(false);
            lnks.hide();
            $('.mp_links', '.public-photos .d_big_photo').hide().removeClass('change-default');
        }
    }
}


function coverPageWithVeil(cover) {
    if (cover) {
        window['popupWhatIs'].DoShowWindowModalElement(-1)
        $('.dxpcModalBackground').css({
            'z-index': 12000
        });
        $('body').css("overflow", "visible");
        $('#public-photos-wrap').addClass('modal');//.photos_center();

        $('.close-btn', '#mp_photo .public-photos').click(function () {
            ChangeDefaultImg($('.btnEditDefault', '#mp_photo .left-box .one-box'))
            $('.close-btn', '#mp_photo .public-photos').unbind("click");
        });
    }
    else {
        $('#public-photos-wrap').removeClass('modal').attr('style', '');
        window['popupWhatIs'].DoHideWindowCore(-1);
        //$('.dxpcModalBackground').css({ 'z-index': 9999 });
    }
}


function CallBackPanelModifyControls() {

    $('.d_big_photo', '.public-photos').mouseenter(function () {
        if (!isChangeDefault) {
            var t = this;
            $('.mp_links', t).show().css({
                top: centerElemVert($('.mp_links', this))
            });
        }
    }).mouseleave(function () {
        if (!isChangeDefault) { $('.mp_links', this).hide(); }
    });
    $('.d_big_photo', '.private-photos').mouseenter(function () {
        var t = this;
        $('.mp_links', t).show().css({
            top: centerElemVert($('.mp_links', this))
        });
    }).mouseleave(function () {
        $('.mp_links', this).hide();
    });

}
function centerElemHorz(o) {
    var l = ($(o).parent().width() - $(o).outerWidth()) / 2;
    return l;
}
function centerElemVert(o) {
    var l = ($(o).parent().height() - $(o).outerHeight()) / 2;
    return l;
}



function cbpnlPhotos_EndCallback(s, e) {
    CallBackPanelModifyControls();
    HideLoading();
    try {
        var path = $('#' + hdfDefaultPath).val();
        if (!stringIsEmpty(path)) {
            $('#' + imgPhotoID).attr('src', path);
            $('#' + hdfDefaultPath).val('')
        }
    } catch (e) { }
}

function cbpnlPhotos_CallbackError(s, e) {
    HideLoading();
}



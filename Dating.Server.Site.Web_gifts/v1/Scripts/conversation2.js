﻿
var messagesLoader = new function () {
    var t = this;
    var linksConversation = {}
    var ConversationSysData = {}
    var scrollToId = null;
    var inProcess = false;
    var inProcess_dateUntil = null;

    t.loadMessages = function (dateUntil) {

        // lock the messages's load

        if (inProcess) { return };
        inProcess = true;

        __loadMessages(dateUntil);

        setTimeout(function () {
            // auto unlock in 30 seconds
            inProcess = false;
        }, 5000);
    }


    var __loadMessages = function (dateUntil) {

        // lock the messages's load
        if (scrollToId == null) {
            if ($('.cnv_item', '#loaded_messages').length > 0) {
                scrollToId = $('.cnv_item', '#loaded_messages')[0].id;
            }
            else if ($('.cnv_item', '#initial_messages').length > 0) {
                scrollToId = $('.cnv_item', '#initial_messages')[0].id;
            }
        }

        if (dateUntil == 'more') {
            //inProcess_dateUntil = 91;
            performRequest_LoadMoreMessages({
                updateInitial: false,
                callback: callbackFnMore
            });

        }
        else if (dateUntil == 'all') {
            inProcess_dateUntil = 91;
            performRequest_LoadMoreMessages({
                updateInitial: false,
                dateUntil: null,
                callback: callbackFn,
                skipScroll: true
            });
        }
        else {
            inProcess_dateUntil = dateUntil;
            performRequest_LoadMoreMessages({
                updateInitial: false,
                dateUntil: dateUntil,
                callback: callbackFn,
                skipScroll: true
            });

        }

        function callbackFn() {
            if (ConversationSysData.isMore() && !ConversationSysData.isFullResult()) {
                setTimeout(function () {
                    __loadMessages(dateUntil);
                    inProcess = true;
                }, 500);
            }
            else {
                scrollWin($('#' + scrollToId, '#all_messages'), -200, 500);
                scrollToId = null;
                inProcess = false;
            }
        };

        function callbackFnMore() {
            scrollToId = null;
            inProcess = false;
            $('#top-links-period').removeClass('hidden');
            $('#top-links-more').addClass('hidden');
        };

    }

    t.ReadListData = function () {
        var isSuccess = false;
        var v = '';
        if ($('#hdfListData').length > 0) {
            v = $('#hdfListData')[$('#hdfListData').length - 1].value
            $('#hdfListData').val("");
            $('#hdfListData').remove();
        }
        if (!stringIsEmpty(v)) {
            ConversationSysData = $.evalJSON(v);
            ConversationSysData['requestedItems'] = parseInt(ConversationSysData['requestedItems']);
            ConversationSysData['rowNumberMax'] = parseInt(ConversationSysData['rowNumberMax']);
            ConversationSysData['resultCount'] = parseInt(ConversationSysData['resultCount']);
            ConversationSysData['items'] = parseInt(ConversationSysData['items']);
            ConversationSysData['DaysDiff'] = parseInt(ConversationSysData['DaysDiff']);
            ConversationSysData['olderMessageDays'] = parseInt(ConversationSysData['olderMessageDays']);

            ConversationSysData.isMore = function () {
                var more = (ConversationSysData['more'] == 'true');
                return more;
            }
            ConversationSysData.isFullResult = function () {
                var more =
                    (ConversationSysData['resultCount'] < ConversationSysData['requestedItems']) ||
                    (ConversationSysData['rowNumberMax'] == ConversationSysData['items']);
                return more;
            }
            isSuccess = true;
        }
        return isSuccess;
    }

    t.ShowPeriodButtons = function () {
        var isSuccess = t.ReadListData();
        if (!isSuccess) { return; }

        var DaysDiff = ConversationSysData['DaysDiff'];
        var MinDate = ConversationSysData['MinDate'];
        var items = ConversationSysData['items'];
        var more = ConversationSysData.isMore();

        if (more != true) {
            DaysDiff = 91;
        }

        var o7days = $('.link-7days a', '#top-links'),
            o30days = $('.link-30days a', '#top-links'),
            o3months = $('.link-3months a', '#top-links'),
            oAll = $('.link-all a', '#top-links');
        var o7daysHidden = false, o30daysHidden = false, o3monthsHidden = false, oAllHidden = false;

        if (ConversationSysData['olderMessageDays'] < 7) {
            o7days.hide();
            o7daysHidden = true;
        }
        else if (DaysDiff > 7 || inProcess_dateUntil >= 7) {
            if (o7days.attr('disabled') == null) {
                o7days.attr('disabled', 'disabled');
                if (linksConversation["link-7days"] == null)
                    linksConversation["link-7days"] = o7days.attr('onclick');
                o7days.attr('onclick', "return false;");
            }
        }
        else {
            if (o7days.attr('disabled') != null) {
                o7days.removeAttr("disabled");
                o7days.attr('onclick', linksConversation["link-7days"]);
            }
        }

        if (ConversationSysData['olderMessageDays'] < 30) {
            o30days.hide();
            o30daysHidden = true;
        }
        else if (DaysDiff > 30 || inProcess_dateUntil >= 30) {
            if (o30days.attr('disabled') == null) {
                o30days.attr('disabled', 'disabled');
                if (linksConversation["link-30days"] == null)
                    linksConversation["link-30days"] = o30days.attr('onclick');
                o30days.attr('onclick', "return false;");
            }
        }
        else {
            if (o30days.attr('disabled') != null) {
                o30days.removeAttr("disabled");
                o30days.attr('onclick', linksConversation["link-30days"]);
            }
        }

        if (ConversationSysData['olderMessageDays'] < 90) {
            o3months.hide();
            o3monthsHidden = true;
        }
        else if (DaysDiff > 90 || inProcess_dateUntil >= 90) {
            if (o3months.attr('disabled') == null) {
                o3months.attr('disabled', 'disabled');
                if (linksConversation["link-3months"] == null)
                    linksConversation["link-3months"] = o3months.attr('onclick');
                o3months.attr('onclick', "return false;");
            }
        }
        else {
            if (o3months.attr('disabled') != null) {
                o3months.removeAttr("disabled");
                o3months.attr('onclick', linksConversation["link-3months"]);
            }
        }


       if (!more) {
            if (oAll.attr('disabled') == null) {
                oAll.attr('disabled', 'disabled');
                if (linksConversation["link-all"] == null)
                    linksConversation["link-all"] = oAll.attr('onclick');
                oAll.attr('onclick', "return false;");
            }
        }
        else {
            if (oAll.attr('disabled') != null) {
                oAll.removeAttr("disabled");
                oAll.attr('onclick', linksConversation["link-all"]);
            }
        }

       if (!more && o7daysHidden && o30daysHidden && o3monthsHidden) {
           oAll.hide();
           oAllHidden = true;
           $('.pnlMoreMsgs', '#conv-messages-list').hide();
       }

       if (!oAllHidden && o7daysHidden && o30daysHidden && o3monthsHidden) {
           oAll.parent().css("float", "none").css("text-align","center");
       }
    }
}

var loadMessages = messagesLoader.loadMessages;



// <![CDATA[

var RegisterForm_LoadAsyncValidation = new function () {
    var t = this;

    var prevEmailText = "";
    var prevEmailRequestData = {};

    t.checkEmail = function () {
        $('.mailValidation').html('');
        var request = true;

        if (!RegisterEmailTextBox.GetIsValid()) {
            request = false;
        }
        else {
            var email = RegisterEmailTextBox.GetText();
            if (email === "") {
                request = false;
            }
            else if (prevEmailText != "" && prevEmailText == email) {
                if (prevEmailRequestData == [] || (!prevEmailRequestData.Exists && stringIsEmpty(prevEmailRequestData.Message))) {
                    request = false;
                }
                else if (prevEmailRequestData != null && (prevEmailRequestData.Exists || !stringIsEmpty(prevEmailRequestData.Message))) {
                    setTimeout(function () {
                        t.successMail(prevEmailRequestData);
                    }, 100);
                    request = false;
                }
            }
        }


        if (request == true) {
            $('.txtEmail input').addClass('loadingIndicator');
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "Register.aspx/checkEmailAvailability",
                data: '{ "email": "' + email + '" }',
                dataType: "json",
                success: function (msg) {
                    var data = eval("(" + msg.d + ")");
                    prevEmailText = email;
                    prevEmailRequestData = data;

                    t.successMail(data);
                }
            });
        }

        return request;
    }

    t.successMail = function (data) {
        var exists = data.Exists;
        var message = data.Message;
        $('.txtEmail input').removeClass('loadingIndicator');

        var s = window["RegisterEmailTextBox"];
        var isValid = (exists != true && stringIsEmpty(message));
        if (isValid) {
            prevEmailRequestData = [];
        }
        RegValidation.handleElement(s, isValid, message)
    }


    var prevUsernameText = "";
    var prevUsernameRequestData = {};

    t.checkUsername = function () {
        $('.usernameValidation').html('')

        var request = true;
        var username = RegisterLoginTextBox.GetText();
        if (username === "") {
            request = false;
        }
        else if (prevUsernameText != "" && prevUsernameText == username) {
            if (prevUsernameRequestData == [] || (!prevUsernameRequestData.Exists && stringIsEmpty(prevUsernameRequestData.Message))) {
                request = false;
            }
            else if (prevUsernameRequestData != null && (prevUsernameRequestData.Exists || !stringIsEmpty(prevUsernameRequestData.Message))) {
                t.successUsername(prevUsernameRequestData);
                request = false;
            }
        }

        if (request == true) {
            $('.txtLogin input').addClass('loadingIndicator');

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "Register.aspx/checkUsernameAvailability",
                data: '{ "username": "' + username + '" }',
                dataType: "json",
                success: function (msg) {
                    var data = eval("(" + msg.d + ")");
                    prevUsernameText = username;
                    prevUsernameRequestData = data;

                    t.successUsername(data);
                }
            });
        }

        return request;
    }

    t.successUsername = function (data) {
        var exists = data.Exists;
        var message = data.Message;

        $('.txtLogin input').removeClass('loadingIndicator');

        var s = window["RegisterLoginTextBox"];
        var isValid = (exists != true && stringIsEmpty(message));
        if (isValid) {
            prevUsernameRequestData = [];
        }

        RegValidation.handleElement(s, isValid, message)
    }


    //t.validatePassword = function () {
    //    $('.passwordValidation').html('');
    //    $('.passwordValidation').css("color", "green").removeClass('dxEditors_edtError').css("display", "none");

    //    var username = $(".txtLogin input").val();
    //    var password = $(".txtPasswrd input").val();

    //    var isValid = (password != username && username !== "" && password !== "");

    //    var s = window["RegisterPassword"];
    //    RegValidation.handleElement(s, isValid, passwordValidation_DiffThanLogin)
    //    s.SetText('');

    //    return isValid
    //}

    t.start = function () {
        jQuery(function ($) {
            $(document).ready(function () {
                $('.txtEmail').focusout(t.checkEmail);
                //$('.txtLogin').focusout(t.checkUsername);
                //$('.txtPasswrd').focusout(t.validatePassword);
            });
        });
    }

}
// ]]> 
﻿Imports DevExpress.Web.ASPxEditors

Public Class DistanceDropDown
    Inherits System.Web.UI.UserControl


    Public ReadOnly Property SelectedItem As ListeditItem
        Get
            Return ddl.SelectedItem
        End Get
    End Property

    Public Property CssClass As String
        Get
            Return ddl.CssClass
        End Get
        Set(value As String)
            ddl.CssClass = value
        End Set
    End Property


    Public Property SelectedValue As String
        Get

            If (ddl.SelectedIndex > -1) Then
                Return ddl.Items(ddl.SelectedIndex).Value
            End If
            Return Nothing

        End Get
        Set(ByVal value As String)

            If (Not value Is Nothing) Then
                Dim itm As ListEditItem = Me.ddl.Items.FindByValue(value)
                If (Not itm Is Nothing) Then
                    itm.Selected = True
                End If
            End If

        End Set
    End Property


    Public Property AutoPostBack As Boolean
        Get
            Return ddl.AutoPostBack
        End Get
        Set(value As Boolean)
            ddl.AutoPostBack = value
        End Set
    End Property

    <PersistenceMode(PersistenceMode.InnerDefaultProperty)> _
    Public ReadOnly Property ListControl As ASPxComboBox
        Get
            Return ddl
        End Get
    End Property


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

End Class
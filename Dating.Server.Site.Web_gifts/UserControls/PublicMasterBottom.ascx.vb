﻿Imports Library.Public
Imports DevExpress.Web.ASPxMenu
Imports Dating.Server.Core.DLL
Imports DevExpress.Web.ASPxEditors


Public Class PublicMasterBottom
    Inherits BaseUserControl

    Private __LoadedLag As String

    Protected Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            'If (_pageData Is Nothing) Then _pageData = New clsPageData("master", Context)
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData("master", Context, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property


    Private Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        'Try
        '    If (Me.IsPostBack) Then
        '        If (hdfTimeOffset.Value.Trim() <> "") Then
        '            JSON.SetOffsetTime(hdfTimeOffset.Value)
        '        End If
        '        If (hdfIsMobile.Value.Trim() <> "" AndAlso Session("IsMobileAccess") Is Nothing) Then
        '            JSON.SetIsMobile(hdfIsMobile.Value)
        '        End If
        '    End If

        'Catch ex As Exception
        '    WebErrorMessageBox(Me, ex, "")
        'End Try
    End Sub




    Public Sub LoadLAG()
        Try
            __LoadedLag = Me.GetLag()


            'clsMenuHelper.PopulateMenuData(mnuFooter1, "mnuFooter1", 10, ActionForItemsEnum.TakeItemsWithGreaterSortNumber)
            'clsMenuHelper.PopulateMenuData(mnuFooter1More, "mnuFooter1", 10, ActionForItemsEnum.SkipItemsWithGreaterSortNumberAndTakeRemainingItems)

            'clsMenuHelper.PopulateMenuData(mnuFooter2, "mnuFooter2", 10, ActionForItemsEnum.TakeItemsWithGreaterSortNumber)
            'clsMenuHelper.PopulateMenuData(mnuFooter2More, "mnuFooter2", 10, ActionForItemsEnum.SkipItemsWithGreaterSortNumberAndTakeRemainingItems)

            'clsMenuHelper.PopulateMenuData(mnuFooter3, "mnuFooter3", 10, ActionForItemsEnum.TakeItemsWithGreaterSortNumber)
            'clsMenuHelper.PopulateMenuData(mnuFooter3More, "mnuFooter3", 10, ActionForItemsEnum.SkipItemsWithGreaterSortNumberAndTakeRemainingItems)

            'clsMenuHelper.PopulateMenuData(mnuFooter4, "mnuFooter4", 10, ActionForItemsEnum.TakeItemsWithGreaterSortNumber)
            'clsMenuHelper.PopulateMenuData(mnuFooter4More, "mnuFooter4", 10, ActionForItemsEnum.SkipItemsWithGreaterSortNumberAndTakeRemainingItems)

            'clsMenuHelper.PopulateMenuData(mnuFooter5, "mnuFooter5", 10, ActionForItemsEnum.TakeItemsWithGreaterSortNumber)
            'clsMenuHelper.PopulateMenuData(mnuFooter5More, "mnuFooter5", 10, ActionForItemsEnum.SkipItemsWithGreaterSortNumberAndTakeRemainingItems)

            clsMenuHelper.PopulateMenuData(mnuFooter1, "mnuFooter1")
            clsMenuHelper.PopulateMenuData(mnuFooter2, "mnuFooter2")
            clsMenuHelper.PopulateMenuData(mnuFooter3, "mnuFooter3")
            clsMenuHelper.PopulateMenuData(mnuFooter4, "mnuFooter4")
            clsMenuHelper.PopulateMenuData(mnuFooter5, "mnuFooter5")

            clsMenuHelper.PopulateMenuData(MenuLinks1, "MenuLinks1", 10, ActionForItemsEnum.TakeItemsWithGreaterSortNumber)
            clsMenuHelper.PopulateMenuData(MenuLinks1More, "MenuLinks1", 10, ActionForItemsEnum.SkipItemsWithGreaterSortNumberAndTakeRemainingItems)
            clsMenuHelper.PopulateMenuData(MenuLinks2, "MenuLinks2", 10, ActionForItemsEnum.TakeItemsWithGreaterSortNumber)
            clsMenuHelper.PopulateMenuData(MenuLinks2More, "MenuLinks2", 10, ActionForItemsEnum.SkipItemsWithGreaterSortNumberAndTakeRemainingItems)
            clsMenuHelper.PopulateMenuData(MenuLinks3, "MenuLinks3", 10, ActionForItemsEnum.TakeItemsWithGreaterSortNumber)
            clsMenuHelper.PopulateMenuData(MenuLinks3More, "MenuLinks3", 10, ActionForItemsEnum.SkipItemsWithGreaterSortNumberAndTakeRemainingItems)
            clsMenuHelper.PopulateMenuData(MenuLinks4, "MenuLinks4")


            clsMenuHelper.CheckPublicMenuLinks(mnuFooter1, GetLag())
            clsMenuHelper.CheckPublicMenuLinks(mnuFooter2, GetLag())
            clsMenuHelper.CheckPublicMenuLinks(mnuFooter3, GetLag())
            clsMenuHelper.CheckPublicMenuLinks(mnuFooter4, GetLag())
            clsMenuHelper.CheckPublicMenuLinks(mnuFooter5, GetLag())
            clsMenuHelper.CheckPublicMenuLinks(MenuLinks1, GetLag())
            clsMenuHelper.CheckPublicMenuLinks(MenuLinks2, GetLag())
            clsMenuHelper.CheckPublicMenuLinks(MenuLinks3, GetLag())
            clsMenuHelper.CheckPublicMenuLinks(MenuLinks4, GetLag())


            lblFooterMenuTitle1.Text = CurrentPageData.GetCustomString("lblFooterMenuTitle1")
            lblFooterMenuTitle2.Text = CurrentPageData.GetCustomString("lblFooterMenuTitle2")
            lblFooterMenuTitle3.Text = CurrentPageData.GetCustomString("lblFooterMenuTitle3")
            lblFooterMenuTitle4.Text = CurrentPageData.GetCustomString("lblFooterMenuTitle4")
            lblFooterMenuTitle5.Text = CurrentPageData.GetCustomString("lblFooterMenuTitle5")

            lnkTogglMenuLinks1More.Text = CurrentPageData.GetCustomString("MenuLinksMore.Show")
            lnkTogglMenuLinks2More.Text = lnkTogglMenuLinks1More.Text
            lnkTogglMenuLinks3More.Text = lnkTogglMenuLinks1More.Text

            'MenuLinksMore.Less
            'MenuLinksMore.Show
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Try
            'lblFooterMenuHeader.Text = CurrentPageData.GetCustomString("lblFooterMenuHeader")
            lblFooterSiteTitle.Text = CurrentPageData.GetCustomString("lblFooterSiteTitle")
            lblFooterSiteDescription.Text = CurrentPageData.GetCustomString("lblFooterSiteDescription")
            lblFooterCopyRight.Text = CurrentPageData.GetCustomString("lblFooterCopyRight")
            lblMoreInfo.Text = CurrentPageData.GetCustomString("lblMoreInfo")
            'lblChartTitle.Text = CurrentPageData.GetCustomString("lblChartTitle")
            lnkSiteMap.Text = CurrentPageData.GetCustomString("lnkSiteMap")
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Try
            lblEscortsWarning.Text = CurrentPageData.GetCustomString("lblEscortsWarning")


            Dim lblEscortsWarningTooltip As Label = lblEscortsWarningPopup.FindControl("lblEscortsWarningTooltip")
            If (lblEscortsWarningTooltip IsNot Nothing) Then
                lblEscortsWarningTooltip.Text = CurrentPageData.GetCustomString("lblEscortsWarningTooltip")
                lblEscortsWarningPopup.Enabled = True
            Else
                lblEscortsWarningPopup.Enabled = False
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try


    End Sub


    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        Try
            If (String.IsNullOrEmpty(__LoadedLag) OrElse __LoadedLag <> Me.GetLag()) Then
                LoadLAG()
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub
End Class
﻿<%@ Control Language="vb" AutoEventWireup="false" 
    CodeBehind="MessagesControl.ascx.vb" 
    Inherits="Dating.Server.Site.Web.MessagesControl" %>
<script type="text/javascript">
    var animating_elemnts_list = new Array();
</script>
<asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">

    <asp:View ID="View1" runat="server">
    </asp:View>
    
    
    <asp:View ID="vwNoPhoto" runat="server">
        <asp:FormView ID="fvNoPhoto" runat="server" EnableViewState="false">
        <ItemTemplate>
<div id="ph-edit" class="no-photo">
    <div class="items_none items_hard">
            <div class="items_none_wrap">
        <div class="items_none_text">
            <%# Eval("HasNoPhotosText")%>
        </div>
        <div class="search_members_button_right"><asp:HyperLink ID="lnk" runat="server" CssClass="btn lighter" NavigateUrl="~/Members/Photos.aspx" onclick="ShowLoading();"><%# Eval("AddPhotosText")%><i class="icon-chevron-right"></i></asp:HyperLink></div>
        <div class="clear"></div>
    </div>
    </div>
</div>
        </ItemTemplate>
        </asp:FormView>
    </asp:View>
    
    
    <asp:View ID="vwNoOffers" runat="server">
        <asp:FormView ID="fvNoOffers" runat="server" Width="69px" EnableViewState="false">
        <ItemTemplate>
<div class="items_none">
    <%# Eval("NoOfferText")%>
	<p><asp:HyperLink ID="lnk4" runat="server" CssClass="btn lighter" NavigateUrl="~/Members/Search.aspx" onclick="ShowLoading();"><%# Eval("SearchOurMembersText")%><i class="icon-chevron-right"></i></asp:HyperLink></p>
	<div class="clear"></div>
</div>
        </ItemTemplate>
        </asp:FormView>
    </asp:View>


    <asp:View ID="vwMessages" runat="server">
    <asp:Repeater ID="rptNew" runat="server">
    <ItemTemplate>
        <asp:HiddenField ID="hdfProfileID" runat="server" Value='<%# Eval("OtherMemberProfileID") %>' Visible="False" />
        <asp:HiddenField ID="hdfMessageRead" runat="server" Value='<%# Eval("Read") %>' Visible="False" />
<div class="msl_item <%# Eval("Read") %>">
    <div class="left" ID="dLeft" runat="server" EnableViewState="false">
        <div class="photo">
            <div class="pr-photo-116-noshad">
                <asp:HyperLink ID="hl2" runat="server" NavigateUrl='<%# Eval("OtherMemberProfileViewUrl") %>' onclick="ShowLoading();"><img src="<%# Eval("OtherMemberImageUrl") %>" class="round-img-116" /></asp:HyperLink>
            </div>
        </div>
        <div class="info">
            <h2><asp:HyperLink ID="lnk2" runat="server" NavigateUrl='<%# Eval("OtherMemberProfileViewUrl")%>' onclick="ShowLoading();"><%# Eval("OtherMemberLoginName").ToString().Replace("deleted_","")%></asp:HyperLink></h2>
            <div class="stats">
                <%# MyBase.WriteOffer_MemberInfo(Container.DataItem)%>
                <div class="tooltip_offer" style="display: none;">
                    <div class="tooltip_text"><%# Eval("MilesAwayText")%></div>
                </div>
            </div>
            <div class="tt_enabled_distance distance_absolute <%# Eval("DistanceCss")%>"></div>
        </div>
    </div>
    <div class="middle">
        <div class="info<%# If(Eval("ShowWarningProfileDeleted").ToString()="True", " profile-inactive", "") %><%# If(Eval("IsLimited").ToString()="True", " limited", "") %>">
            <div id="tdWarningProfileDeleted" runat="server" EnableViewState="false" visible='<%# Eval("ShowWarningProfileDeleted") %>' class="inactive-msg">
                <asp:Label ID="lblWarningProfileDeleted" runat="server" Text='<%# Eval("WarningProfileDeletedText") %>' CssClass="WarningProfileDeleted" />
            </div>
            <div id="tdProfileActive" runat="server" EnableViewState="false" visible='<%# Not (Eval("ShowWarningProfileDeleted")) %>'>
                <h2 class="m_subject">
                    <asp:Literal ID="lblSubject" runat="server" Text='<%# Eval("Subject") %>' />
                </h2>
                <asp:Label ID="lblReadStat" runat="server" Text='<%# Eval("ReadText") %>' CssClass="closed" />
                <asp:Literal ID="lblSeparatorReadDate" runat="server"> - </asp:Literal><asp:Label
                    ID="lblSentDate" runat="server" Text='<%# Eval("SentDate") %>' CssClass="m_sentdate" />
                <br />
                <asp:Label ID="lblOfferAmount" runat="server" Text='<%# Eval("OfferAmountText") %>' CssClass="m_offer" />
                <asp:Label ID="lblCommStat" runat="server" Text='<%# Eval("CommunicationStatus") %>' CssClass="m_comstat" Visible="False" />
                <div class="msg-count">
                <asp:Label ID="lblCount" runat="server" Text='<%# Eval("MessagesCountText") & " " & Mybase.GetCountMessagesString(Eval("ItemsCount")) %>' 
                    Visible='<%# Mybase.IsVisible(Eval("ItemsCount")) %>'/>
                </div>
                
            </div>
            <asp:Panel CssClass="icon_tooltipholder" runat="server" ID="divTooltipholder" Visible='<%# Eval("AllowTooltipPopupMessages")%>'>
                <asp:LinkButton ID="lnkWhatIs" runat="server" OnClientClick='<%# Eval("OnMoreInfoClickFunc")%>'>
                    <img runat="server" id="icon_tip" src="~/Images/spacer102.png" alt="" class="questmark" /></asp:LinkButton>
            </asp:Panel>
        </div>
    </div>

    <div class="right">
        <asp:Panel runat="server" ID="pnlIsOnline" Visible='<%# Eval("OtherMemberIsOnline") %>' CssClass="rfloat" EnableViewState="false">
            <div id="pnlIsOnline<%# Eval("OtherMemberProfileID") %>" 
                class="online-indicator"><asp:Image ID="imgOn" runat="server" ImageUrl="../Images/spacer10.png" AlternateText="online" ToolTip="online" /><asp:Label
                                ID="lblOnline" runat="server" Text="online"></asp:Label>
            </div>
            <script type="text/javascript">
                if ('<%# Eval("OtherMemberIsOnline").ToString() %>' == 'True') animating_elemnts_list.push('pnlIsOnline<%# Eval("OtherMemberProfileID") %>');
            </script>
        </asp:Panel>
        <div class="clear"></div>
        <asp:HyperLink ID="btnOpen" runat="server" Text='<%# Eval("ViewMessageText") %>' OnClientClick="ShowLoading()"
            EnableViewState="false"
            NavigateUrl='<%# "~/Members/Conversation.aspx?msg=" & Eval("EUS_MessageID") & "&vwh=" & HttpUtility.UrlEncode(Eval("OtherMemberLoginName").ToString())%>'
            Visible='<%# Eval("btnOpenVisible") %>' 
            CssClass="btn btnOpen rfloat">
        </asp:HyperLink>
        <div class="clear"></div>

        <div class="delete rfloat">
            <asp:LinkButton ID="btnDeleteAllInConversation" runat="server" CommandArgument='<%# Eval("EUS_MessageID") %>'
                CommandName="DELETEALLMSG" CssClass="btn" OnClientClick="ShowLoading();" Text="Delete">
            </asp:LinkButton>
        </div>
        <div class="clear"></div>
    </div>


    
    <div class="clear"></div>
</div>
                
    </ItemTemplate>
    </asp:Repeater>




    </asp:View>


</asp:MultiView>

<script type="text/javascript">
    function animateImg(elid) {
        var a = $('img', '#' + elid);
        if (a.is(".slide1"))
            a.removeClass("slide1");
        else
            a.addClass("slide1");
        if (a.length > 0)
            setTimeout("animateImg('" + elid + "')", 500);
    }
    function startAnimating() {
        for (c = 0; c < animating_elemnts_list.length; c++) {
            animateImg(animating_elemnts_list[c]);
        }
    }
    if (animating_elemnts_list.length > 0) { setTimeout(startAnimating, 500); }

    $("div.bot_actions", ".l_new .left .info").click(function () {
        var t = this;
        $(t).addClass("selected");
    }).mouseenter(function () {
        var t = this;
        $(t).addClass("selected");
    }).mouseleave(function () {
        var t = this;
        $(t).removeClass("selected");
        /*if ($(".btn-group.open", t).length > 0) {$(t).addClass("selected");} else {$(t).removeClass("selected");}*/
    });
</script>


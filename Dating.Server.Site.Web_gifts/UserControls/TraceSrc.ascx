﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="TraceSrc.ascx.vb" Inherits="Dating.Server.Site.Web.TraceSrc" %>
<asp:Panel ID="trcScripts" runat="server" style="height:0px;width:0px;">

<script type="text/javascript">
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-32245832-1']);
    _gaq.push(['_trackPageview']);

    (function () {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

    var _kmq = _kmq || [];
    var _kmk = _kmk || '248a3d6838dff1a02a5dcc4a7c90123792416969';
    function _kms(u) {
        setTimeout(function () {
            var d = document, f = d.getElementsByTagName('script')[0],
      s = d.createElement('script');
            s.type = 'text/javascript'; s.async = true; s.src = u;
            f.parentNode.insertBefore(s, f);
        }, 1);
    }
    _kms('//i.kissmetrics.com/i.js');
    _kms('//doug1izaerwt3.cloudfront.net/' + _kmk + '.1.js');
</script>

<!-- Google Code for &Gamma;&nu;&omega;&rho;&iota;&mu;&#943;&epsilon;&sigmaf; Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1000407937;
var google_conversion_language = "en";
var google_conversion_format = "2";
var google_conversion_color = "ffffff";
var google_conversion_label = "bQnmCJe5ygQQgYeE3QM";
var google_conversion_value = 0;
/* ]]> */
</script>

<div style="display:none;">
<%
    If (Request.Url.Scheme = "https") Then
%><script type="text/javascript" src="https://www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:none;">
<img height="1" width="1" style="border-style:none;display:none;" alt="" src="https://www.googleadservices.com/pagead/conversion/1000407937/?value=0&amp;label=bQnmCJe5ygQQgYeE3QM&amp;guid=ON&amp;script=0"/>
</div>
</noscript><%
               
           Else
               
%><script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:none;">
<img height="1" width="1" style="border-style:none;display:none;" alt="" src="http://www.googleadservices.com/pagead/conversion/1000407937/?value=0&amp;label=bQnmCJe5ygQQgYeE3QM&amp;guid=ON&amp;script=0"/>
</div>
</noscript><%
        End If
%>
</div>

<%
    If (Request.Url.AbsolutePath.ToUpper().IndexOf("/MEMBERS/") = -1) Then
        'hide alexa script in members area
%>
<!-- Start Alexa Certify Javascript -->
<script type="text/javascript">
_atrk_opts = { atrk_acct:"Panli1aUCm008W", domain:"goomena.com",dynamic: true};
(function() { var as = document.createElement('script'); as.type = 'text/javascript'; as.async = true; as.src = "//d31qbv1cthcecs.cloudfront.net/atrk.js"; var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(as, s); })();
</script>
<noscript><img src="//d5nxst8fruw4z.cloudfront.net/atrk.gif?account=Panli1aUCm008W" style="display:none" height="1" width="1" alt="" /></noscript>
<!-- End Alexa Certify Javascript -->
<%
    End If
%>
</asp:Panel>
﻿Imports Dating.Server.Core.DLL
Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Datasets.DLL.DSMembers

Public Class ProfileIconOther
    Inherits BaseUserControl

    'Public Enum NavigateToEnum
    '    None = 0
    '    ProfilePhotos = 1
    '    Profile = 2
    'End Enum



    'Protected Property NavigateTo As NavigateToEnum
    '    Get
    '        If (ViewState("NavigateTo") IsNot Nothing) Then
    '            Return ViewState("NavigateTo")
    '        End If
    '        Return NavigateToEnum.None
    '    End Get
    '    Set(value As NavigateToEnum)
    '        ViewState("NavigateTo") = value
    '    End Set
    'End Property


    Public Property ShowName As Boolean
        Get
            If (ViewState("ShowName") IsNot Nothing) Then
                Return ViewState("ShowName")
            End If
            Return False
        End Get
        Set(value As Boolean)
            ViewState("ShowName") = value
        End Set
    End Property



    Public Property ProfileId As Integer
        Get

            If (ViewState("ProfileId") IsNot Nothing) Then
                Return ViewState("ProfileId")
            End If
            Return 0

        End Get
        Set(value As Integer)

            If (ViewState("ProfileId") <> value) Then
                ViewState("ProfileId") = value
                LoadProfileIcon(value)
            End If

        End Set
    End Property



    Protected Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            'If (_pageData Is Nothing) Then _pageData = New clsPageData("~/Members/control.ProfileView", Context)
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData("~/Members/control.ProfileView", Context, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property




    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            If (Not Me.IsPostBack) Then
                LoadLAG()

                'Dim profileId As Integer = Me.Session("ProfileID")
                'Me.LoadProfile()
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

        If (Not Page.IsPostBack) Then
        End If

    End Sub


    Protected Overrides Sub OnPreRender(e As System.EventArgs)
        MyBase.OnPreRender(e)

        lblName.Visible = Me.ShowName
    End Sub


    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
    End Sub


    Protected Sub LoadLAG()
        Try
            'Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic


        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub




    Public Sub LoadProfileIcon(profileId As Integer)

        Try
            ' view other user's icon

            Dim _profilerows As New clsProfileRows(profileId)
            Dim masterRow As EUS_ProfilesRow = _profilerows.GetMasterRow()


            lblName.Text = masterRow.LoginName

            '   Dim fileName As String = Nothing
            Dim photo As DSMembers.EUS_CustomerPhotosRow = DataHelpers.GetProfilesDefaultPhoto(profileId)
            If (photo IsNot Nothing) Then
                imgPhoto.ImageUrl = ProfileHelper.GetProfileImageURL(photo.CustomerID, photo.FileName, masterRow.GenderId, True, Me.IsHTTPS)
            Else
                imgPhoto.ImageUrl = ProfileHelper.GetDefaultImageURL(masterRow.GenderId)
            End If

            lnkPhoto.NavigateUrl = ResolveUrl("~/Members/Profile.aspx?p=") & HttpUtility.UrlEncode(masterRow.LoginName)


            '~/Members/Profile.aspx?do=edit
            'If (Me.NavigateTo = NavigateToEnum.Profile) Then
            'ElseIf (Me.NavigateTo = NavigateToEnum.ProfilePhotos) Then
            'End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub



End Class
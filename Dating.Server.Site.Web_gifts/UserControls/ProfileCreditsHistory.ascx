﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ProfileCreditsHistory.ascx.vb" Inherits="Dating.Server.Site.Web.ProfileCreditsHistory" %>
<div class="credits-history">

<dx:ASPxGridView ID="gv" runat="server" AutoGenerateColumns="False" 
    DataSourceID="sdsCreditsHistory" KeyFieldName="CustomerCreditsId" 
    Width="100%">
    <Columns>
        <dx:GridViewDataDateColumn FieldName="Date" VisibleIndex="3" Width="130">
            <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy HH:mm">
            </PropertiesDateEdit>
            <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
            </CellStyle>
            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
        </dx:GridViewDataDateColumn>
        <dx:GridViewDataTextColumn FieldName="Action" ReadOnly="True" VisibleIndex="9" Width="130">
            <DataItemTemplate>
                <asp:Image ID="i1" runat="server" ImageUrl="https://cdn.goomena.com/Images/spacer10.png" 
                    AlternateText='<%# Eval("Action") %>' 
                    ToolTip='<%# Eval("Action") %>' />
                <asp:Label  ID="lblAct" runat="server"></asp:Label>
            </DataItemTemplate>
            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="Description" ReadOnly="True" 
            VisibleIndex="10">
            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
        </dx:GridViewDataTextColumn>
    </Columns>
    <SettingsPager PageSize="40">
    </SettingsPager>
    <Styles>
        <Row BackColor="#f8f8f8"></Row>
        <AlternatingRow BackColor="#ffffff" Enabled="True">
        </AlternatingRow>
        <Cell>
            <Paddings PaddingBottom="5px" PaddingLeft="5px" PaddingTop="5px" />
        </Cell>
    </Styles>
</dx:ASPxGridView>

<asp:SqlDataSource ID="sdsCreditsHistory" runat="server" 
    ConnectionString="<%$ ConnectionStrings:AppDBconnectionString %>" 
    SelectCommand="CustomerCreditsHistory" SelectCommandType="StoredProcedure">
    <SelectParameters>
        <asp:Parameter DefaultValue="0" Name="CustomerId" />
    </SelectParameters>
</asp:SqlDataSource>

</div>
<%--<dx:GridViewDataTextColumn FieldName="CustomerCreditsId" ReadOnly="True" 
    Visible="False" VisibleIndex="0">
    <EditFormSettings Visible="False" />
</dx:GridViewDataTextColumn>
<dx:GridViewDataTextColumn FieldName="CustomerId" Visible="False" 
    VisibleIndex="1">
</dx:GridViewDataTextColumn>
<dx:GridViewDataTextColumn FieldName="Credits" Visible="False" VisibleIndex="2">
</dx:GridViewDataTextColumn>
<dx:GridViewDataTextColumn FieldName="CustomerTransactionID" Visible="False" 
    VisibleIndex="4">
</dx:GridViewDataTextColumn>
<dx:GridViewDataTextColumn FieldName="CreditsTypeId" Visible="False" 
    VisibleIndex="5">
</dx:GridViewDataTextColumn>
<dx:GridViewDataTextColumn FieldName="ConversationWithCustomerId" 
    Visible="False" VisibleIndex="6">
</dx:GridViewDataTextColumn>
<dx:GridViewDataTextColumn FieldName="ConversationWithLoginName" 
    Visible="False" VisibleIndex="7">
</dx:GridViewDataTextColumn>
<dx:GridViewDataTextColumn FieldName="CreditsType" Visible="False" 
    VisibleIndex="8">
</dx:GridViewDataTextColumn>--%>

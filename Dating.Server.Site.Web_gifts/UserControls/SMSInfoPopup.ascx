﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="SMSInfoPopup.ascx.vb" Inherits="Dating.Server.Site.Web.SMSInfoPopup" %>
<div class="sms-payment-info-popup">
<dx:ASPxPopupControl runat="server"
    Height="566px" Width="500px"
    ID="popupSMSInfo"
    ClientInstanceName="popupSMSInfo" 
    ClientIDMode="AutoID" 
    AllowDragging="True" 
    PopupVerticalAlign="WindowCenter" 
    PopupHorizontalAlign="WindowCenter" 
    Modal="True" 
    AllowResize="True" 
    AutoUpdatePosition="True"
    ShowShadow="False"
    ShowHeader="false" >
    <ContentStyle HorizontalAlign="Left" VerticalAlign="Middle">
        <Paddings Padding="0px" PaddingTop="25px" />
        <Paddings Padding="0px" PaddingTop="25px" ></Paddings>
    </ContentStyle>
    <CloseButtonStyle BackColor="White">
        <HoverStyle>
            <BackgroundImage ImageUrl="//cdn.goomena.com/Images2/popup-wrn/close.png" Repeat="NoRepeat" HorizontalPosition="center"
                VerticalPosition="center"></BackgroundImage>
        </HoverStyle>
        <BackgroundImage ImageUrl="//cdn.goomena.com/Images2/popup-wrn/close.png" Repeat="NoRepeat" HorizontalPosition="center"
            VerticalPosition="center"></BackgroundImage>
        <BorderLeft BorderColor="White" BorderStyle="Solid" BorderWidth="10px" />
    </CloseButtonStyle>
    <CloseButtonImage Url="//cdn.goomena.com/Images/spacer10.png" Height="43px" Width="43px">
    </CloseButtonImage>
    <SizeGripImage Height="40px" Url="//cdn.goomena.com/Images/resize.png" Width="40px">
    </SizeGripImage>
    <HeaderStyle>
        <Paddings Padding="0px" PaddingLeft="0px" />
        <Paddings PaddingTop="0px" PaddingBottom="0px"></Paddings>
        <BorderTop BorderColor="White" BorderStyle="Solid" BorderWidth="5px" />
        <BorderRight BorderColor="White" BorderStyle="Solid" BorderWidth="5px" />
        <BorderBottom BorderColor="White" BorderStyle="Solid" BorderWidth="5px" />
    </HeaderStyle>
    <ModalBackgroundStyle BackColor="Black" Opacity="70" CssClass="WhatIs-ModalBG"></ModalBackgroundStyle>
    <ContentCollection>
        <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server" SupportsDisabledAttribute="True">
            <asp:Label ID="lblContent" runat="server" Text=""></asp:Label>
        </dx:PopupControlContentControl>
    </ContentCollection>
</dx:ASPxPopupControl>
</div>

<script type="text/javascript">
    __popupSMSInfo__.headerText = '<%= Dating.Server.Site.Web.AppUtils.JS_PrepareString(Me.CurrentPageData.GetCustomString("SMS.Info.Popup.HeaderText"))%>';
    __popupSMSInfo__.productsPage = '<%= ResolveUrl("~/Members/SelectProduct.aspx")%>';  
    __popupSMSInfo__.onLoaded();
</script>

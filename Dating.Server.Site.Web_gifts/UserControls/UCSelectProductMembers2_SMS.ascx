﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UCSelectProductMembers2_SMS.ascx.vb" 
    Inherits="Dating.Server.Site.Web.UCSelectProductMembers2_SMS" %>

<asp:Panel ID="pnlSms" runat="server" CssClass="mobile-sms">
    <style type="text/css">
        .mobile-sms .alert { margin-top:7px;}
    </style>
    <asp:Label ID="lblSMSBanner" runat="server" CssClass="sms-banner"></asp:Label>

    <asp:Panel ID="pnlSMSCodeSuccessfull" runat="server" 
                CssClass="alert alert-success blue" Visible="False" ViewStateMode="Disabled"><asp:Label ID="lblSMSCodeSuccessfull" runat="server" Text=""/></asp:Panel>

    <div class="lfloat form-text"><asp:Literal ID="lblCodeText" runat="server"></asp:Literal></div>
    <div class="rfloat form-input">
        <table cellspacing="0" cellpadding="0">
            <tr>
                <td><asp:TextBox ID="txtSMSCode" runat="server"></asp:TextBox></td>
                <td>&nbsp;</td>
                <td><asp:LinkButton ID="btnSMSCode" runat="server">&nbsp;OK&nbsp;</asp:LinkButton></td>
            </tr>
        </table>
    </div>
    <div class="clear"></div>
    <asp:Literal ID="lblSMSFooter" runat="server"></asp:Literal>
</asp:Panel>

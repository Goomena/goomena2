﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="DatesControl.ascx.vb" 
    Inherits="Dating.Server.Site.Web.DatesControl" %>
<script type="text/javascript">
    var animating_elemnts_list = new Array();
</script>
<asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">

    <asp:View ID="View1" runat="server">
    </asp:View>
    
    
    <asp:View ID="vwNoPhoto" runat="server">
        <asp:FormView ID="fvNoPhoto" runat="server">
        <ItemTemplate>
<div id="ph-edit" class="no-photo">
    <div class="items_none items_hard">
            <div class="items_none_wrap">
        <div class="items_none_text">
            <%# Eval("HasNoPhotosText")%>
        </div>
        <div class="search_members_button_right"><asp:HyperLink ID="lnk" runat="server" CssClass="btn lighter" NavigateUrl="~/Members/Photos.aspx" onclick="ShowLoading();"><%# Eval("AddPhotosText")%><i class="icon-chevron-right"></i></asp:HyperLink></div>
        <div class="clear"></div>
    </div>
    </div>
</div>
<%--<div class="items_hard">
    <%# Eval("HasNoPhotosText")%>
	<p><asp:HyperLink ID="lnk" runat="server" CssClass="btn lighter" NavigateUrl="~/Members/Photos.aspx" onclick="ShowLoading();"><%# Eval("AddPhotosText")%><i class="icon-chevron-right"></i></asp:HyperLink></p>
	<div class="clear"></div>
</div>--%>
        </ItemTemplate>
        </asp:FormView>
    </asp:View>
    
    
    <asp:View ID="vwNoOffers" runat="server">
        <asp:FormView ID="fvNoOffers" runat="server">
        <ItemTemplate>
<div class="items_none">
    <div id="dates_icon" class="middle_icon"></div>
    <div class="items_none_text">
        <%# Eval("NoOfferText")%>
        <div class="search_members_button">
	        <asp:HyperLink ID="lnk4" runat="server" CssClass="btn lighter" NavigateUrl="~/Members/Search.aspx" onclick="ShowLoading();"><%# Eval("SearchOurMembersText")%><i class="icon-chevron-right"></i></asp:HyperLink>
        </div>
	    <div class="clear"></div>
    </div>
</div>
        </ItemTemplate>
        </asp:FormView>
    </asp:View>


    <asp:View ID="vwDatesOffers" runat="server">
<asp:Repeater ID="rptDates" runat="server">
<ItemTemplate>
<div class="date_item" login="<%# Eval("OtherMemberLoginName")%>">
    <div class="left">
        <div class="photo">
            <div class="pr-photo-116-noshad">
                <asp:HyperLink ID="hl2" runat="server" NavigateUrl='<%# Eval("OtherMemberProfileViewUrl") %>' onclick="ShowLoading();"><img src="<%# Eval("OtherMemberImageUrl") %>" class="round-img-116" /></asp:HyperLink>
            </div>
        </div>
        <div class="info">
            <h2><asp:HyperLink ID="hl" runat="server" NavigateUrl='<%# Eval("OtherMemberProfileViewUrl") %>' Text='<%# Eval("OtherMemberLoginName")%>'  onclick="ShowLoading();"/></h2>
            <%--<p><%# Eval("OtherMemberHeading")%></p>--%>
            <div class="stats">
                <%# MyBase.WriteSearch_MemberInfo(Container.DataItem)%>
            </div>
            <div class="date-info">
                <div class="lfloat"><asp:Literal ID="lblLastDating" runat="server" Text='<%# Eval("LastDatingText") %>'/></div>
                <div class="lfloat"><asp:Literal ID="lblLastDatingDate" runat="server" Text='<%# Eval("LastDatingDateText") %>'/></div>
	            <div class="clear"></div>
            </div>
        </div>
    </div>
    <div class="right">
        <asp:Panel runat="server" ID="pnlIsOnline" Visible='<%# Eval("OtherMemberIsOnline") %>' class="rfloat">
            <div id="pnlIsOnline<%# Eval("OtherMemberProfileID") %>" 
                class="online-indicator"><asp:Image ID="imgOn" runat="server" ImageUrl="//cdn.goomena.com/Images/spacer10.png" AlternateText="online" ToolTip="online" /><asp:Label
                                ID="lblOnline" runat="server" Text="online"></asp:Label>
            </div>
            <script type="text/javascript">
                if ('<%# Eval("OtherMemberIsOnline").ToString() %>' == 'True') animating_elemnts_list.push('pnlIsOnline<%# Eval("OtherMemberProfileID") %>');
            </script>
        </asp:Panel>
        <div class="bot_actions">
            <div class="btn-group" ID="RejectsMenu" runat="server" Visible='<%# Eval("AllowRejectsMenu") %>'>
                <a class="btn btn-small dropdown-toggle lnk107" data-toggle="dropdown" href="#"><%# Eval("RejectText")%><span class="caret"></span></a>
                <ul class="dropdown-menu right-menu" style="text-align:left;">
                    <li><asp:LinkButton ID="lnkDelConv" runat="server" 
                            CommandName="REJECT_DELETECONV" 
                            CommandArgument='<%# Eval("OtherMemberProfileID")%>' 
                            Text='<%# Eval("RejectDeleteConversationText")%>' 
                            OnClientClick="if(confirm({0})){return true;}else{return false;}"
                            CssClass="btn" /></li>
                    <li><asp:LinkButton ID="lnkBlock" runat="server" 
                            CommandName="REJECT_BLOCK" 
                            CommandArgument='<%# Eval("OtherMemberProfileID")%>' 
                            Text='<%# Eval("RejectBlockText")%>' 
                            OnClientClick="ShowLoading();"
                            CssClass="btn" /></li>
                </ul>
            </div>
            <div class="rfloat">
                <asp:LinkButton ID="lnkHistory" runat="server" 
                    CssClass="btn btn-small lnk130" 
                    Visible='<%# Eval("AllowHistory") %>' 
                    OnClientClick='<%# Eval("HistoryNavigateUrl")%>'><%# Eval("HistoryText")%></asp:LinkButton>
            </div>
            <div class="rfloat">
                <asp:HyperLink ID="lnk1" runat="server" Visible='<%# Eval("AllowSendMessage")%>' 
                    CssClass="btn lnkSendMessage lighter"
                    NavigateUrl='<%# Eval("SendMessageUrl")%>'><%# Eval("SendMessageText")%><i class="icon-chevron-right"></i></asp:HyperLink>
            </div>
            <div class="rfloat">
                <asp:HyperLink ID="lnkConversation" runat="server" 
                    CssClass="btn btn-small lnk130" 
                    Visible='<%# Eval("AllowConversation") %>' 
                    NavigateUrl='<%# Eval("ConversationNavigateUrl")%>' 
                    onclick="ShowLoading();"><%# Eval("ConversationText")%></asp:HyperLink>
            </div>
        </div>
    </div>
    <div class="clear"></div>
</div>
</ItemTemplate>
</asp:Repeater>
    </asp:View>


</asp:MultiView>


<script type="text/javascript">
    function animateImg(elid) {
        var a = $('img', '#' + elid);
        if (a.is(".slide1"))
            a.removeClass("slide1");
        else
            a.addClass("slide1");
        if (a.length > 0)
            setTimeout("animateImg('" + elid + "')", 500);
    }
    function startAnimating() {
        for (c = 0; c < animating_elemnts_list.length; c++) {
            animateImg(animating_elemnts_list[c]);
        }
    }
    if (animating_elemnts_list.length > 0) { setTimeout(startAnimating, 500); }

    $('.lnkSendMessage', '#conv-item .date_item').click(function (e) {
        var a = $('a.read-message:visible', '#send_buttons');
        if (a.length > 0) {
            e.stopPropagation();
            e.preventDefault();
            scrollWin($('a.read-message', '#send_buttons'), -200, 500)
            setTimeout(function () {
                a[0].click();
            }, 800);
        }
        else {
            // edit window isalready opened
            a = $('#write_message_wrap:visible');
            if (a.length > 0) {
                e.stopPropagation();
                e.preventDefault();
                scrollWin($(a), -200, 500)
            }
        }
    })
</script>













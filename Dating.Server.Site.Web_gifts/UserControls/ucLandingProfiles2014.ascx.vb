﻿Imports System.Data.SqlClient
Imports Dating.Server.Core.DLL

Public Class ucLandingProfiles2014
    Inherits BaseUserControl
    'Inherits System.Web.UI.Page

    Protected Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData(Context, Me.Page, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property

    Protected _MasterPageData As clsPageData
    Protected ReadOnly Property MasterPageData As clsPageData
        Get
            'If (_pageData Is Nothing) Then _pageData = New clsPageData("master", Context)
            If (_MasterPageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _MasterPageData = New clsPageData("master", Context, coe)
                AddHandler _MasterPageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If

            Return _MasterPageData
        End Get
    End Property


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            btnRegister.Text = MasterPageData.VerifyCustomString("Landing.btnRegister")
            lblCloseRdv.Text = MasterPageData.VerifyCustomString("Landing.lblCloseRdv").Replace("<br>", " ").Replace("<br/>", " ").Replace("<br />", " ")

            lblRegisterEmail.Text = MasterPageData.VerifyCustomString("Landing.lblRegisterEmail")
            lblRegisterFB.Text = MasterPageData.VerifyCustomString("Landing.lblRegisterFB")
            lblRegisterGplus.Text = MasterPageData.VerifyCustomString("Landing.lblRegisterGplus")
            lbFBNote.Text = MasterPageData.VerifyCustomString("Landing.lbFBNote")


            If (MyBase.IsPublicRootMaster2) Then
                btnRegister.Text = Regex.Replace(btnRegister.Text, "/Register.aspx", "/Register2.aspx", RegexOptions.IgnoreCase)
                lblRegisterEmail.Text = Regex.Replace(lblRegisterEmail.Text, "/Register.aspx", "/Register2.aspx", RegexOptions.IgnoreCase)
                lblRegisterFB.Text = Regex.Replace(lblRegisterFB.Text, "/Register.aspx", "/Register2.aspx", RegexOptions.IgnoreCase)
                lblRegisterGplus.Text = Regex.Replace(lblRegisterGplus.Text, "/Register.aspx", "/Register2.aspx", RegexOptions.IgnoreCase)
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

        ' Dim myCmd As SqlCommand = Nothing
        'Dim myConn As SqlConnection
        'Dim myReader As SqlDataReader
        Dim address As String = Me.Request.Url.Scheme & "://photos.goomena.com/"
        Dim htmlup As String = ""
        Dim htmldown As String = ""
        Dim counter As Integer
        Dim photoid As String
        Dim photoname As String
        Dim profilename As String
        Dim address2 As String
        Dim src As String

        Try
            'myConn = New SqlConnection(ConfigurationManager.ConnectionStrings("AppDBconnectionString").ConnectionString)
            Dim dt As DataTable
            Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection
                Using myCmd = DataHelpers.GetSqlCommand(con)
                    myCmd.CommandText = <sql><![CDATA[exec [dbo].[CustomerPhotos_Landing] @Country=@Country1,@GenderID=@GenderID1]]></sql>.Value

                    Dim country1 As String = Session("GEO_COUNTRY_CODE")
                    If (Not String.IsNullOrEmpty(Me.SessionVariables.MemberData.Country)) Then
                        country1 = Me.SessionVariables.MemberData.Country
                    End If
                    myCmd.Parameters.AddWithValue("@Country1", country1)
                    myCmd.Parameters.AddWithValue("@GenderID1", 2)
                    dt = DataHelpers.GetDataTable(myCmd)
                End Using
            End Using
         
           



            Dim checkList As New List(Of Integer)
            counter = 0
            For cnt = 0 To dt.Rows.Count - 1

                Dim dr As DataRow = dt.Rows(cnt)
                'Do While myReader.Read()

                Dim CustomerID As Integer = dr("CustomerID")
                If (checkList.Contains(CustomerID)) Then
                    Continue For
                End If
                checkList.Add(CustomerID)

                photoid = dr("CustomerID").ToString
                photoname = dr("FileName")
                profilename = dr("LoginName")
                address2 = address + photoid
                src = address2 + "/thumbs/" + photoname
                Dim url As String = UrlUtils.GetValidRoutingProfileURI(HttpUtility.UrlEncode(profilename)) 'HttpUtility.UrlEncode(profilename).Replace("+", " ")
                url = MyBase.LanguageHelper.GetPublicURL("/profile/" & url, MyBase.GetLag())

                Dim OtherMemberCity As String = dr("City").ToString()
                '  Dim OtherMemberRegion As String = dr("Region").ToString()
                Dim OtherMemberCountry As String = dr("Country").ToString()
                Dim sb As New System.Text.StringBuilder()
                sb.Append(IIf(Not String.IsNullOrEmpty(OtherMemberCity), OtherMemberCity, ""))
                sb.Append(",  ")
                'sb.Append(IIf(Not String.IsNullOrEmpty(OtherMemberRegion), OtherMemberRegion, ""))
                'sb.Append(",  ")
                sb.Append(IIf(Not String.IsNullOrEmpty(OtherMemberCountry), ProfileHelper.GetCountryName(OtherMemberCountry), ""))
                sb.Replace(",  " & ",  ", ",  ")

                Dim html As String = "<div class=""landingFacesContainer " & If(counter = 5, "face-last", "") & """ title=""" & profilename & vbCrLf & sb.ToString() & """><a class=""landingimgborder"" href=""" & url & """ onclick=""ShowLoading();""><img class=""FaceImg"" src=""" & src & """/><p class=""login-name"">" & profilename & "</p><p class=""profile-info"">" & sb.ToString() & "</p></a></div>"

                If counter < 6 Then
                    htmlup += html
                Else
                    htmldown += html
                End If
                counter = counter + 1

                'stis 8 photos exit
                If (counter > 8) Then Exit For

            Next

            lblFacesUp.Text = htmlup
            lblFacesDown.Text = htmldown

        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        Finally
        End Try


    End Sub



End Class
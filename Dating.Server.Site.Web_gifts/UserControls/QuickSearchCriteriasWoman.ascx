﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="QuickSearchCriteriasWoman.ascx.vb"
    Inherits="Dating.Server.Site.Web.QuickSearchCriteriasWoman" %>
<%@ Register Src="~/UserControls/AgeDropDown.ascx" TagName="AgeDropDown" TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/DistanceDropDown.ascx" TagName="DistanceDropDown"
    TagPrefix="uc4" %>
<script type="text/javascript">
// <![CDATA[

function fire_imgSearchByUserName(s,e){
    try{
        if((e.htmlEvent.charCode && e.htmlEvent.charCode==13) || (e.htmlEvent.keyCode && e.htmlEvent.keyCode==13)){
            //if(WebForm_FireDefaultButton(event, "<%= imgSearchByUserName.UniqueID %>")){
                <%= MyBase.SearchByUserNameOnClientClicked %>
            //}
        }
    }
    catch(e){}
}

(function () {
try {
    new Image().src = '//cdn.goomena.com/images2/search/check.png';
    new Image().src = '//cdn.goomena.com/images2/search/uncheck.png';
    new Image().src = '//cdn.goomena.com/images2/search/arrow-select.png';
}
catch (e) { }
})();


var textSeparator = ";";
function OnListBoxSelectionChanged(listBox, args) {
    if (args.index == 0)
        args.isSelected ? listBox.SelectAll() : listBox.UnselectAll();
    UpdateSelectAllItemState(listBox);
    UpdateText(listBox);

    if(listBox == window["clstJobs"]){
        if(listBox.GetSelectedItems().length>0)
            window['<%= chkJob.ClientID %>'].SetChecked(true);
    }
}
function UpdateSelectAllItemState(clst) {
    IsAllSelected(clst) ? clst.SelectIndices([0]) : clst.UnselectIndices([0]);
}
function IsAllSelected(clst) {
    var selectedDataItemCount = clst.GetItemCount() - (clst.GetItem(0).selected ? 0 : 1);
    return clst.GetSelectedItems().length == selectedDataItemCount;
}
function UpdateText(clst) {
    var selectedItems = clst.GetSelectedItems();
    if (window['clstSpokenLang'] == clst) {
        ddeSpokenLangWrap.SetText(GetSelectedItemsText(selectedItems));
    }
    else if (window['clstTypeOfDating'] == clst) {
        ddeTypeOfDatingWrap.SetText(GetSelectedItemsText(selectedItems));
    }
    else if (window['clstJobs'] == clst) {
        ddeJobsWrap.SetText(GetSelectedItemsText(selectedItems));
    }
}

function SynchronizeListBoxValues(dropDown, args) {
    var clst = null;
    if (window['ddeSpokenLangWrap'] == dropDown) {
        clst = clstSpokenLang;
    }
    else if (window['ddeTypeOfDatingWrap'] == dropDown) {
        clst = clstTypeOfDating;
    }
    else if (window['ddeJobsWrap'] == dropDown) {
        clst = clstJobs;
    }
    clst.UnselectAll();
    var texts = dropDown.GetText().split(textSeparator);
    var values = GetValuesByTexts(clst, texts);
    clst.SelectValues(values);
    UpdateSelectAllItemState(clst);
    UpdateText(clst); // for remove non-existing texts
}

function GetSelectedItemsText(items) {
    var texts = [];
    for (var i = 0; i < items.length; i++)
        if (items[i].index != 0)
            texts.push(items[i].text);
    return texts.join(textSeparator);
}
function GetValuesByTexts(clst, texts) {
    var actualValues = [];
    var item;
    for (var i = 0; i < texts.length; i++) {
        item = clst.FindItemByText(texts[i]);
        if (item != null)
            actualValues.push(item.value);
    }
    return actualValues;
}

var __menuCollapsed = null;
function initQuickOptions() {
    $('.link-more', '#search-footer-row').attr("title", '<%= MyBase.globalStrings.GetCustomString("msg_ClickToExpand", GetLag())  %>');
    $('.img-more', '#search-footer-row').html('+');
    $('.more-options', '#search-footer-row').html('<%= MyBase.CurrentPageData.GetCustomString("QS.More.Filters") %>');
    __menuCollapsed = true;
}
        function toggleQuickOptions() {
        var form2 = $('.inner-box', '#quick-search-form2');
        form2 = $(form2);
        if(__menuCollapsed==null){
            //$('.bottom_submit', form2).hide();
            setTimeout(function(){
                __menuCollapsed = true;
                form2.hide();
                $('.link-clear', '#search-footer-row').hide();
                $('.img-more', '#search-footer-row').html('+');
                $('.more-options', '#search-footer-row').html('<%= MyBase.CurrentPageData.GetCustomString("QS.More.Filters") %>');
            },300);
        }
        if (__menuCollapsed) {
            __menuCollapsed = false;
            setTimeout(function(){
                form2.show();
                window["trcIncome"].SetVisible(true);
                window["trcDistance"].SetVisible(true);
                window["trcAge"].SetVisible(true);
                $('.link-clear', '#search-footer-row').show();
                $('.img-more', '#search-footer-row').html('-');
                $('.more-options', '#search-footer-row').html('<%= MyBase.CurrentPageData.GetCustomString("QS.Less.Filters") %>');
            },300);
        }
        else {
            __menuCollapsed = true;
                form2.hide();
                $('.link-clear', '#search-footer-row').hide();
                $('.img-more', '#search-footer-row').html('+');
                $('.more-options', '#search-footer-row').html('<%= MyBase.CurrentPageData.GetCustomString("QS.More.Filters") %>');
              
        }
    }
    function clearForm(){
        //try{
        if(window['<%= chkOnline.ClientID%>']!=null)   window['<%= chkOnline.ClientID%>'].SetChecked(false);
        if(window['<%= chkPhotos.ClientID%>']!=null)  window['<%= chkPhotos.ClientID%>'].SetChecked(false);
        if(window['<%= chkPhotosPrivate.ClientID%>']!=null)   window['<%= chkPhotosPrivate.ClientID%>'].SetChecked(false);
        if(window['<%= chkVIP.ClientID%>']!=null)  window['<%= chkVIP.ClientID%>'].SetChecked(false);
        if(window['<%= chkJob.ClientID%>']!=null)  window['<%= chkJob.ClientID%>'].SetChecked(false);
        if(window['<%= chkTravel.ClientID%>']!=null)  window['<%= chkTravel.ClientID%>'].SetChecked(false);
        if(window['<%= clstBodyType.ClientID%>']!=null)  window['<%= clstBodyType.ClientID%>'].UnselectAll();
        if(window['<%= clstEducat.ClientID%>']!=null)window['<%= clstEducat.ClientID%>'].UnselectAll();
        if(window['<%= clstSmoking.ClientID%>']!=null)window['<%= clstSmoking.ClientID%>'].UnselectAll();
        if(window['<%= clstDrinking.ClientID%>']!=null)window['<%= clstDrinking.ClientID%>'].UnselectAll();
               
        if(window['clstJobs']!=null) {
            window['clstJobs'].UnselectAll();
            UpdateSelectAllItemState(window['clstJobs']);
            UpdateText(window['clstJobs']);
        }

        if(window['clstSpokenLang']!=null) {
            window['clstSpokenLang'].UnselectAll();
            UpdateSelectAllItemState(window['clstSpokenLang']);
            UpdateText(window['clstSpokenLang']);
        }

        if(window['clstTypeOfDating']!=null) {
            window['clstTypeOfDating'].UnselectAll();
            UpdateSelectAllItemState(window['clstTypeOfDating']);
            UpdateText(window['clstTypeOfDating']);
        }

        if(window['trcAge']!=null) {
            window["trcAge"].SetPositionStart(-1);
            window["trcAge"].SetPositionEnd(window["trcAge"].maxValue);
            SetTextForPositions(window["trcAge"]);
        }
        if(window['trcDistance']!=null) {
            window["trcDistance"].SetPosition(window["trcDistance"].maxValue);
            SetTextForPositions(window["trcDistance"]);
        }

        if(window['trcIncome']!=null) {
            window["trcIncome"].SetPositionStart(-1);
            window["trcIncome"].SetPositionEnd(window["trcIncome"].maxValue);
            SetTextForPositions(window["trcIncome"]);
        }
        //}
        //catch(e){}
    }


function TrackBar_Init(s, e) {
    if (s == window["trcIncome"]) {

    }
    if (s == window["trcDistance"]) {
        //s.SetPosition(31);
    }
    if (s == window["trcAge"]) {

    }
    if (trackBarInfoList && trackBarInfoList != null &&
        trackBarInfoList.List != null && trackBarInfoList.List.length > 0) {
        SetTextForPositions(s, e);
    }
}

function TrackBar_PositionChanging(s, e) {
   // SaveDefaultText(s, e);
}

function TrackBar_PositionChanged(s, e) {
    SetTextForPositions(s, e);
}

function SaveDefaultText2() {
    try {
        trackBarInfoList.AddInfo("trcIncome", "DefaultTextMin", '<%= msg_IncomeMin.Text%>', 0);
        trackBarInfoList.AddInfo("trcIncome", "DefaultTextMax", '<%= msg_IncomeMax.Text%>', <%= trcIncome.Items.Count - 1%>);

        trackBarInfoList.AddInfo("trcDistance", "DefaultText", '<%= msg_DistanceVal.Text%>', <%= trcDistance.Items.Count - 1%>);

        trackBarInfoList.AddInfo("trcAge", "DefaultTextMin", '18', 0);
        trackBarInfoList.AddInfo("trcAge", "DefaultTextMax", '120', <%= trcAge.Items.Count - 1%>);
    }
    catch (e) { }
}

function SetTextForPositions(s, e) {
    var txt = "", ti;
    try {
        //s.GetItemText(s.GetPosition())
        //s.GetPositionStart()
        if (s == window["trcIncome"]) {
            ti = trackBarInfoList.GetInfoByPos("trcIncome", "DefaultTextMin", s.GetPositionStart());
            if (ti != null) { txt = ti.DefaultTextMin; } else {
                txt = s.GetItemText(s.GetPositionStart());
                if (stringIsEmpty(txt)) txt = s.GetPositionStart();
            }
            $('#income-min', '#criteria_income').html(txt);

            ti = trackBarInfoList.GetInfoByPos("trcIncome", "DefaultTextMax", s.GetPositionEnd());
            if (ti != null) { txt = ti.DefaultTextMax; } else {
                txt = s.GetItemText(s.GetPositionEnd());
                if (stringIsEmpty(txt)) txt = s.GetPositionEnd();
            }
            $('#income-max', '#criteria_income').html(txt);
        }
        else if (s == window["trcDistance"]) {
            ti = trackBarInfoList.GetInfoByPos("trcDistance", "DefaultText", s.GetPosition());
            if (ti != null) { txt = ti.DefaultText; } else {
                txt = s.GetItemText(s.GetPosition());
                if (stringIsEmpty(txt)) txt = s.GetPosition();
            }
            $('.trackbar-pos-val', '#criteria_distance').html(txt);
        }
        else if (s == window["trcAge"]) {
            ti = trackBarInfoList.GetInfoByPos("trcAge", "DefaultTextMin", s.GetPositionStart());
            if (ti != null) { txt = ti.DefaultTextMin; } else {
                txt = s.GetItemText(s.GetPositionStart());
                if (stringIsEmpty(txt)) txt = s.GetPositionStart();
            }
            $('#age-min', '#criteria_age').html(txt);

            ti = trackBarInfoList.GetInfoByPos("trcAge", "DefaultTextMax", s.GetPositionEnd());
            if (ti != null) { txt = ti.DefaultTextMax; }
            else {
                txt = s.GetItemText(s.GetPositionEnd());
                if (stringIsEmpty(txt)) txt = s.GetPositionEnd();
            }
            $('#age-max', '#criteria_age').html(txt);
        }
    }
    catch (e) { }
}

$(function(){
    SaveDefaultText2();
    initQuickOptions();
    //toggleQuickOptions();
});
/*
function SaveDefaultText(s, e) {
    var txt;
    try {
        if (s == window["trcIncome"]) {
            txt = $('#income-min', '#criteria_income').html();
            trackBarInfoList.AddInfo("trcIncome", "DefaultTextMin", txt, e.currentPositionStart);
            txt = $('#income-max', '#criteria_income').html();
            trackBarInfoList.AddInfo("trcIncome", "DefaultTextMax", txt, e.currentPositionEnd);
        }
        else if (s == window["trcDistance"]) {
            txt = $('.trackbar-pos-val', '#criteria_distance').html();
            trackBarInfoList.AddInfo("trcDistance", "DefaultText", txt, e.currentPosition);
        }
        else if (s == window["trcAge"]) {
            txt = $('#age-min', '#criteria_age').html();
            trackBarInfoList.AddInfo("trcAge", "DefaultTextMin", txt, e.currentPositionStart);
            txt = $('#age-max', '#criteria_age').html();
            trackBarInfoList.AddInfo("trcAge", "DefaultTextMax", txt, e.currentPositionEnd);
        }
    }
    catch (e) { } 
}
*/

// ]]>
</script>
<style type="text/css">
    #quick-search-form2 .criteria {position: relative;padding: 5px 25px 5px 5px;} 
    #criteria_online{width:170px;}
    #criteria_photos{width:194px;}
    #criteria_priv_photos{width:242px;}
    #criteria_travel{width:235px;}
    #quick-search-form2 #criteria_travel .criteria {position: relative;padding:5px;}
    #criteria_job{width:auto;}
    #criteria_vip{width:170px;}
    #criteria_age{width:212px;}
    #quick-search-form2  #criteria_age .criteria{padding:5px;}
    #criteria_distance{width:212px;}
    #quick-search-form2  #criteria_distance .criteria{padding:5px;}
    #criteria_income{width:212px;}
    #quick-search-form2  #criteria_income .criteria{padding:5px;}
    #criteria_breast{width:150px;}
    #criteria_hair{width:150px;}
    #criteria_body{width:175px;}
    #criteria_education{width:150px;}
    #criteria_education .criteria{padding:5px;}
    #criteria_smoking{width:160px;}
    #criteria_smoking .criteria{padding:5px;}
    #criteria_drinking{width:160px;}
    #criteria_drinking .criteria{padding:5px;}
    #criteria_spoken_lang{width:185px;}
    #criteria_dating_type{width:185px;}
    #criteria_spoken_lang .criteria{padding: 5px 0px 5px 0px;}
    #criteria_spoken_lang .criteria-title{padding-bottom: 5px;display: block;}
    #criteria_dating_type{width:185px;margin-top:15px;}
    #criteria_dating_type .criteria{padding: 5px 0px 5px 0px;}
    #criteria_dating_type .criteria-title{padding-bottom: 5px;display: block;}
    .trackbar-pos-val{font-size:13px;font-weight:bold;}
    #quick-search-form2 .bottom_submit {position: absolute;bottom: 10px;right: 15px;}
</style>
<style type="text/css">
    #<%= ddeTypeOfDatingWrap.ClientID%>_DDD_DDTC_clstTypeOfDating_D {width:auto !important;overflow: auto !important;height: 230px !important;}
    #<%= ddeJobsWrap.ClientID%>_DDD_DDTC_clstJobs_D {width:auto !important;overflow: auto !important;}
</style>
<div id="quick-search-form2" class="outter-box">
    <div id="search-top-row">
        <asp:Panel ID="pnlQUserName" runat="server" DefaultButton="imgSearchByUserName" CssClass="input-user">
            <div class="search_textbox_container">
                <dx:ASPxTextBox ID="txtUserNameQ" runat="server" Width="191" Size="191">
                </dx:ASPxTextBox>
            </div>
            <div class="search_button_container">
                <asp:ImageButton ID="imgSearchByUserName" runat="server" ImageUrl="//cdn.goomena.com/Images/spacer10.png"
                    CssClass="btn imgSearchByUserName" /></div>
        </asp:Panel>
        <div class="adv-search-link">
            <asp:LinkButton ID="lnkAdvanced" runat="server" CssClass="btn">Advanced</asp:LinkButton>
        </div>
    </div>
    <div class="inner-box" style="display:none;" >
        <div class="filters_prompt_container">
            <asp:Label ID="msg_UserUserNameOrFiltersQ_ND" runat="server" Text="Or use filters below"></asp:Label>
        </div>
        <div class="clear">
        </div>

        <div id="criteria_online" class="lfloat checkbox-wrap">
            <div class="criteria checkbox">
                <dx:ASPxCheckBox ID="chkOnline" runat="server" Text="Is Online" EncodeHtml="False"
                    CssPostfix="ci">
                    <CheckedImage Url="//cdn.goomena.com/images2/search/check.png" Height="19px"
                        Width="18px">
                    </CheckedImage>
                    <UncheckedImage Url="//cdn.goomena.com/images2/search/uncheck.png" Height="19px"
                        Width="18px">
                    </UncheckedImage>
                </dx:ASPxCheckBox>
            </div>
        </div>
        <div id="criteria_photos" class="lfloat checkbox-wrap">
            <div class="criteria checkbox">
                <dx:ASPxCheckBox ID="chkPhotos" runat="server" Text="Has Photos" EncodeHtml="False"
                    CssPostfix="ci">
                    <CheckedImage Url="//cdn.goomena.com/images2/search/check.png" Height="19px"
                        Width="18px">
                    </CheckedImage>
                    <UncheckedImage Url="//cdn.goomena.com/images2/search/uncheck.png" Height="19px"
                        Width="18px">
                    </UncheckedImage>
                </dx:ASPxCheckBox>
            </div>
        </div>
        <div id="criteria_priv_photos" class="lfloat checkbox-wrap">
            <div class="criteria checkbox">
                <dx:ASPxCheckBox ID="chkPhotosPrivate" runat="server" Text="Has Private Photos" EncodeHtml="False"
                    CssPostfix="ci">
                    <CheckedImage Url="//cdn.goomena.com/images2/search/check.png" Height="19px"
                        Width="18px">
                    </CheckedImage>
                    <UncheckedImage Url="//cdn.goomena.com/images2/search/uncheck.png" Height="19px"
                        Width="18px">
                    </UncheckedImage>
                </dx:ASPxCheckBox>
            </div>
        </div>


        <div id="criteria_vip" class="lfloat checkbox-wrap">
            <div class="criteria checkbox">
                <dx:ASPxCheckBox ID="chkVIP" runat="server" Text="Is VIP" EncodeHtml="False" CssPostfix="ci">
                    <CheckedImage Url="//cdn.goomena.com/images2/search/check.png" Height="19px"
                        Width="18px">
                    </CheckedImage>
                    <UncheckedImage Url="//cdn.goomena.com/images2/search/uncheck.png" Height="19px"
                        Width="18px">
                    </UncheckedImage>
                </dx:ASPxCheckBox>
            </div>
        </div>

        <div id="criteria_travel" class="lfloat checkbox-wrap">
            <div class="criteria checkbox">
                <dx:ASPxCheckBox ID="chkTravel" runat="server" Text="Willing to travel" EncodeHtml="False"
                    CssPostfix="ci">
                    <CheckedImage Url="//cdn.goomena.com/images2/search/check.png" Height="19px"
                        Width="18px">
                    </CheckedImage>
                    <UncheckedImage Url="//cdn.goomena.com/images2/search/uncheck.png" Height="19px"
                        Width="18px">
                    </UncheckedImage>
                </dx:ASPxCheckBox>
            </div>
        </div>
        <div id="criteria_job" class="lfloat checkbox-wrap">
            <div class="criteria checkbox">
                <table cellspacing="0" cellpadding="0">
                    <tr>
                        <td><dx:ASPxCheckBox ID="chkJob" runat="server" Text="Willing to work" EncodeHtml="False"
                                CssPostfix="ci">
                                <CheckedImage Url="//cdn.goomena.com/images2/search/check.png" Height="19px"
                                    Width="18px">
                                </CheckedImage>
                                <UncheckedImage Url="//cdn.goomena.com/images2/search/uncheck.png" Height="19px"
                                    Width="18px">
                                </UncheckedImage>
                            </dx:ASPxCheckBox></td>
                        <td><div style="width:15px;">&nbsp;&nbsp;&nbsp;</div></td>
                        <td><dx:ASPxDropDownEdit ClientInstanceName="ddeJobsWrap" ID="ddeJobsWrap"
                                Width="210px" Height="21" runat="server" AnimationType="None" NullText="Any Language" ClientSideEvents-ButtonClick="function(s, e) {s.ShowDropDown();}" ClientSideEvents-Init="function(s, e) {s.GetInputElement().disabled = true;}">
                                <DropDownWindowTemplate>
                                    <dx:ASPxListBox Width="100%" ID="clstJobs" ClientInstanceName="clstJobs"
                                        SelectionMode="CheckColumn" runat="server" Height="300px">
                                        <CheckBoxCheckedImage Url="//cdn.goomena.com/images2/search/check.png" Height="19px"
                                            Width="18px">
                                        </CheckBoxCheckedImage>
                                        <CheckBoxUncheckedImage Url="//cdn.goomena.com/images2/search/uncheck.png" Height="19px"
                                            Width="18px">
                                        </CheckBoxUncheckedImage>
                                        <ClientSideEvents SelectedIndexChanged="OnListBoxSelectionChanged"  />
                                    </dx:ASPxListBox>
                                </DropDownWindowTemplate>
                                <ClientSideEvents TextChanged="SynchronizeListBoxValues" DropDown="SynchronizeListBoxValues"   />
                            </dx:ASPxDropDownEdit></td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="clear"></div>

        <div id="criteria_age" class="lfloat crt-wrap">
            <div class="criteria trackbar tb-age">
                <div class="trackbar-title"><asp:Literal ID="msg_AgeText" runat="server" Text="Ηλικία"/></div>
                <dx:ASPxTrackBar runat="server" ID="trcAge" MinValue="18" MaxValue="120" Step="1"
                    LargeTickInterval="16" SmallTickFrequency="6" Width="200" AllowRangeSelection="true"
                    PositionStart="18" PositionEnd="120" ClientInstanceName="trcAge" 
                    EnableTheming="False" ScaleLabelHighlightMode="HandlePosition" 
                    ShowChangeButtons="False">
                    <ClientSideEvents Init="TrackBar_Init" PositionChanging="TrackBar_PositionChanging" PositionChanged="TrackBar_PositionChanged" />
                </dx:ASPxTrackBar>
                <div>
                    <div id="age-min" class="trackbar-pos-val lfloat"><asp:Literal ID="msg_AgeMin" runat="server" Text="18"/></div>
                    <div id="age-max" class="trackbar-pos-val rfloat"><asp:Literal ID="msg_AgeMax" runat="server" Text="120"/></div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>

        <div id="criteria_distance" class="lfloat crt-wrap">
            <div class="criteria trackbar tb-distance">
                <div class="trackbar-title"><asp:Literal ID="msg_DistanceFromMe_ND" runat="server" Text="Απόσταση απο εμένα"/></div>
                <dx:ASPxTrackBar runat="server" ID="trcDistance" Step="1" 
                    MinValue="20"
                    MaxValue="10000"
                    PositionStart="0" PositionEnd="30"
                    LargeTickInterval="4" LargeTickEndValue="1" Width="200px" 
                    ClientInstanceName="trcDistance" EnableTheming="False" ScaleLabelHighlightMode="HandlePosition" 
                    ShowChangeButtons="False">
                    <Items>
                        <dx:TrackBarItem Text="20km" ToolTip="20km" Value="20" />
                        <dx:TrackBarItem Text="30km" ToolTip="30km" Value="30" />
                        <dx:TrackBarItem Text="40km" ToolTip="40km" Value="40" />
                        <dx:TrackBarItem Text="50km" ToolTip="50km" Value="50" />
                        <dx:TrackBarItem Text="60km" ToolTip="60km" Value="60" />
                        <dx:TrackBarItem Text="70km" ToolTip="70km" Value="70" />
                        <dx:TrackBarItem Text="80km" ToolTip="80km" Value="80" />
                        <dx:TrackBarItem Text="90km" ToolTip="90km" Value="90" />
                        <dx:TrackBarItem Text="100km" ToolTip="100km" Value="100" />
                        <dx:TrackBarItem Text="120km" ToolTip="120km" Value="120" />
                        <dx:TrackBarItem Text="150km" ToolTip="150km" Value="150" />
                        <dx:TrackBarItem Text="200km" ToolTip="200km" Value="200" />
                        <dx:TrackBarItem Text="300km" ToolTip="300km" Value="300" />
                        <dx:TrackBarItem Text="400km" ToolTip="400km" Value="400" />
                        <dx:TrackBarItem Text="500km" ToolTip="500km" Value="500" />
                        <dx:TrackBarItem Text="600km" ToolTip="600km" Value="600" />
                        <dx:TrackBarItem Text="700km" ToolTip="700km" Value="700" />
                        <dx:TrackBarItem Text="800km" ToolTip="800km" Value="800" />
                        <dx:TrackBarItem Text="900km" ToolTip="900km" Value="900" />
                        <dx:TrackBarItem Text="1000km" ToolTip="1000km" Value="1000" />
                        <dx:TrackBarItem Text="1200km" ToolTip="1200km" Value="1200" />
                        <dx:TrackBarItem Text="1400km" ToolTip="1400km" Value="1400" />
                        <dx:TrackBarItem Text="1600km" ToolTip="1600km" Value="1600" />
                        <dx:TrackBarItem Text="2000km" ToolTip="2000km" Value="2000" />
                        <dx:TrackBarItem Text="3000km" ToolTip="3000km" Value="3000" />
                        <dx:TrackBarItem Text="4000km" ToolTip="4000km" Value="4000" />
                        <dx:TrackBarItem Text="5000km" ToolTip="5000km" Value="5000" />
                        <dx:TrackBarItem Text="6000km" ToolTip="6000km" Value="6000" />
                        <dx:TrackBarItem Text="7000km" ToolTip="7000km" Value="7000" />
                        <dx:TrackBarItem Text="8000km" ToolTip="8000km" Value="8000" />
                        <dx:TrackBarItem Text="9000km" ToolTip="9000km" Value="9000" />
                        <dx:TrackBarItem Text="10000km" ToolTip="10000km" Value="10000" />
                    </Items>
                    <ClientSideEvents Init="TrackBar_Init" PositionChanging="TrackBar_PositionChanging" PositionChanged="TrackBar_PositionChanged" />
                </dx:ASPxTrackBar>
                <div>
                    <div class="trackbar-pos-val"><asp:Literal ID="msg_DistanceVal" runat="server" Text="Any Distance"/></div>
                </div>
            </div>
        </div>

        <div id="criteria_income" class="lfloat crt-wrap">
            <div class="criteria trackbar tb-height">
                <div class="trackbar-title"><asp:Literal ID="msg_OtherAnnual" runat="server" Text="Ετήσιο Εισόδημα"/></div>
                <dx:ASPxTrackBar runat="server" ID="trcIncome" MinValue="0" MaxValue="14" Step="1"
                    LargeTickInterval="3" SmallTickFrequency="3" 
                    Width="200" AllowRangeSelection="true"
                    PositionStart="0" PositionEnd="14" 
                    ClientInstanceName="trcIncome" 
                    EnableTheming="False" ScaleLabelHighlightMode="HandlePosition" 
                    ShowChangeButtons="False">
                    <Items>
                        <dx:TrackBarItem Text="&lt;&euro;20.000" ToolTip="&lt;&euro;20.000" Value="22" />
                        <dx:TrackBarItem Text="&euro;30.000" ToolTip="&euro;30.000" Value="23" />
                        <dx:TrackBarItem Text="&euro;40.000" ToolTip="&euro;40.000" Value="24" />
                        <dx:TrackBarItem Text="&euro;50.000" ToolTip="&euro;50.000" Value="25" />
                        <dx:TrackBarItem Text="&euro;60.000" ToolTip="&euro;60.000" Value="26" />
                        <dx:TrackBarItem Text="&euro;70.000" ToolTip="&euro;70.000" Value="27" />
                        <dx:TrackBarItem Text="&euro;80.000" ToolTip="&euro;80.000" Value="28" />
                        <dx:TrackBarItem Text="&euro;100.000" ToolTip="&euro;100.000" Value="29" />
                        <dx:TrackBarItem Text="&euro;150.000" ToolTip="&euro;150.000" Value="30" />
                        <dx:TrackBarItem Text="&euro;200.000" ToolTip="&euro;200.000" Value="31" />
                        <dx:TrackBarItem Text="&euro;300.000" ToolTip="&euro;300.000" Value="32" />
                        <dx:TrackBarItem Text="&euro;400.000" ToolTip="&euro;400.000" Value="33" />
                        <dx:TrackBarItem Text="&euro;500.000" ToolTip="&euro;500.000" Value="34" />
                        <dx:TrackBarItem Text="&euro;1.000.000" ToolTip="&euro;1.000.000" Value="35" />
                        <dx:TrackBarItem Text="&gt;&euro;1.000.000" ToolTip="&gt;&euro;1.000.000" Value="36" />
                    </Items>
                    <ClientSideEvents Init="TrackBar_Init" PositionChanging="TrackBar_PositionChanging" PositionChanged="TrackBar_PositionChanged" />
                </dx:ASPxTrackBar>
                <div>
                    <div id="income-min" class="trackbar-pos-val lfloat"><asp:Literal ID="msg_IncomeMin" runat="server" Text="&lt;&euro;20.000"/></div>
                    <div id="income-max" class="trackbar-pos-val rfloat"><asp:Literal ID="msg_IncomeMax" runat="server" Text="&gt;&euro;1.000.000"/></div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>

        <div id="criteria_body" class="lfloat">
            <div class="criteria">
                <asp:Label ID="msg_PersonalBodyType" runat="server" Text="" CssClass="criteria-title"></asp:Label>
                <dx:ASPxCheckBoxList ID="clstBodyType" runat="server" ValueType="System.String" CssPostfix="ci">
                    <CheckedImage Url="//cdn.goomena.com/images2/search/check.png" Height="19px"
                        Width="18px">
                    </CheckedImage>
                    <UncheckedImage Url="//cdn.goomena.com/images2/search/uncheck.png" Height="19px"
                        Width="18px">
                    </UncheckedImage>
                </dx:ASPxCheckBoxList>
            </div>
        </div>

        <div id="criteria_education" class="lfloat">
            <div class="criteria">
                <asp:Label ID="msg_OtherEducat" runat="server" Text="Education" CssClass="criteria-title"></asp:Label>
                <dx:ASPxCheckBoxList ID="clstEducat" runat="server" ValueType="System.String" CssPostfix="ci">
                    <CheckedImage Url="//cdn.goomena.com/images2/search/check.png" Height="19px"
                        Width="18px">
                    </CheckedImage>
                    <UncheckedImage Url="//cdn.goomena.com/images2/search/uncheck.png" Height="19px"
                        Width="18px">
                    </UncheckedImage>
                </dx:ASPxCheckBoxList>
            </div>
        </div>


        <div class="lfloat">
            <div id="criteria_smoking">
                <div class="criteria">
                    <asp:Label ID="msg_PersonalSmokingHabit" runat="server" Text="Smoking Habits" CssClass="criteria-title"></asp:Label>
                    <dx:ASPxCheckBoxList ID="clstSmoking" runat="server" ValueType="System.String" CssPostfix="ci">
                        <CheckedImage Url="//cdn.goomena.com/images2/search/check.png" Height="19px"
                            Width="18px">
                        </CheckedImage>
                        <UncheckedImage Url="//cdn.goomena.com/images2/search/uncheck.png" Height="19px"
                            Width="18px">
                        </UncheckedImage>
                    </dx:ASPxCheckBoxList>
                </div>
            </div>
            <div id="criteria_drinking">
                <div class="criteria">
                    <asp:Label ID="msg_PersonalDrinkingHabit" runat="server" Text="Drinking Habits" CssClass="criteria-title"></asp:Label>
                    <dx:ASPxCheckBoxList ID="clstDrinking" runat="server" ValueType="System.String" CssPostfix="ci">
                        <CheckedImage Url="//cdn.goomena.com/images2/search/check.png" Height="19px"
                            Width="18px">
                        </CheckedImage>
                        <UncheckedImage Url="//cdn.goomena.com/images2/search/uncheck.png" Height="19px"
                            Width="18px">
                        </UncheckedImage>
                    </dx:ASPxCheckBoxList>
                </div>
            </div>
        </div>


        <div id="criteria_spoken_lang" class="rfloat">
            <div class="criteria rfloat">
                <asp:Label ID="msg_SpokenLang" runat="server" Text="" CssClass="criteria-title"></asp:Label>
                <dx:ASPxDropDownEdit ClientInstanceName="ddeSpokenLangWrap" ID="ddeSpokenLangWrap"
                    Width="170px" runat="server" AnimationType="None" NullText="Any Language" ClientSideEvents-ButtonClick="function(s, e) {s.ShowDropDown();}" ClientSideEvents-Init="function(s, e) {s.GetInputElement().disabled = true;}">
                    <DropDownWindowTemplate>
                        <dx:ASPxListBox Width="100%" ID="clstSpokenLang" ClientInstanceName="clstSpokenLang"
                            SelectionMode="CheckColumn" runat="server" Height="300px">
                            <CheckBoxCheckedImage Url="//cdn.goomena.com/images2/search/check.png" Height="19px"
                                Width="18px">
                            </CheckBoxCheckedImage>
                            <CheckBoxUncheckedImage Url="//cdn.goomena.com/images2/search/uncheck.png" Height="19px"
                                Width="18px">
                            </CheckBoxUncheckedImage>
                            <ClientSideEvents SelectedIndexChanged="OnListBoxSelectionChanged" />
                        </dx:ASPxListBox>
                    </DropDownWindowTemplate>
                    <ClientSideEvents TextChanged="SynchronizeListBoxValues" DropDown="SynchronizeListBoxValues" />
                </dx:ASPxDropDownEdit>
            </div>
            <div class="clear">
            </div>
        </div>


        <div id="criteria_dating_type" class="rfloat">
            <div class="criteria rfloat">
                <asp:Label ID="msg_TypeOfDating" runat="server" Text="" CssClass="criteria-title"></asp:Label>
                <dx:ASPxDropDownEdit ClientInstanceName="ddeTypeOfDatingWrap" ID="ddeTypeOfDatingWrap"
                    Width="170px" runat="server" AnimationType="None" NullText="" ClientSideEvents-ButtonClick="function(s, e) {s.ShowDropDown();}" ClientSideEvents-Init="function(s, e) {s.GetInputElement().disabled = true;}">
                    <DropDownWindowTemplate>
                        <dx:ASPxListBox Width="100%" ID="clstTypeOfDating" ClientInstanceName="clstTypeOfDating"
                            SelectionMode="CheckColumn" runat="server">
                            <CheckBoxCheckedImage Url="//cdn.goomena.com/images2/search/check.png" Height="19px"
                                Width="18px">
                            </CheckBoxCheckedImage>
                            <CheckBoxUncheckedImage Url="//cdn.goomena.com/images2/search/uncheck.png" Height="19px"
                                Width="18px">
                            </CheckBoxUncheckedImage>
                            <ClientSideEvents SelectedIndexChanged="OnListBoxSelectionChanged" />
                        </dx:ASPxListBox>
                    </DropDownWindowTemplate>
                    <ClientSideEvents TextChanged="SynchronizeListBoxValues" DropDown="SynchronizeListBoxValues"  />
                </dx:ASPxDropDownEdit>
            </div>
            <div class="clear">
            </div>
        </div>

        <div class="clear">
        </div>
        <div class="submit bottom_submit">
            <asp:Button ID="btnSearch_ND" runat="server" Text="Update Search »" CssClass="btn btnSearch" />
        </div>
        <div class="clear">
        </div>
        <asp:Label ID="msg_BirthdayNote" runat="server" Text="" CssClass="birthday-text"></asp:Label>
    </div>
    <div id="search-footer-row">
        <a href="javascript:toggleQuickOptions()" class="link-more">
            <span class="img-more">+</span>
            <asp:Label ID="msg_MoreOptions" runat="server" Text="More Options" CssClass="more-options"></asp:Label>
        </a>

        <a href="javascript:clearForm()" class="link-clear">
            <asp:Label ID="msg_ClearForm" runat="server" Text="Clear Form" CssClass="clear-label"></asp:Label>
        </a>
    </div>
</div>
<script type="text/javascript">
    $(function(){
        //SetTextForPositions(window["trcHeight"]);
        //SetTextForPositions(window["trcDistance"]);
        //SetTextForPositions(window["trcAge"]);
        window["trcIncome"].SetVisible(false);
        window["trcDistance"].SetVisible(false);
        window["trcAge"].SetVisible(false);
    });
    </script>

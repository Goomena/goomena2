﻿Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Core.DLL

Public Class MemberLeftPanel
    Inherits BaseUserControl

    Protected Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            'If (_pageData Is Nothing) Then _pageData = New clsPageData("~/Members/control.ProfileView", Context)
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData("~/Members/control.ProfileView", Context, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property


    Public Property CssClass As String
        Get
            Return pnlProfile.CssClass
        End Get
        Set(value As String)
            pnlProfile.CssClass = value
            pnlProfile2.CssClass = value
        End Set
    End Property

    Public Property ShowMembersNewest As Boolean
        Get
            Return divMembersNewestAll.Visible
        End Get
        Set(value As Boolean)
            divMembersNewestAll.Visible = value
        End Set
    End Property


    Public Property ShowMembersNear As Boolean
        Get
            Return divMembersNearAll.Visible
        End Get
        Set(value As Boolean)
            divMembersNearAll.Visible = value
        End Set
    End Property


    Public Property ShowQuickLinks As Boolean
        Get
            Return pnlQuickLinks.Visible
        End Get
        Set(value As Boolean)
            pnlQuickLinks.Visible = value
        End Set
    End Property


    'Public Property ShowLiveZilla As Boolean
    '    Get
    '        Return pnlLiveZilla.Visible
    '    End Get
    '    Set(value As Boolean)
    '        pnlLiveZilla.Visible = value
    '    End Set
    'End Property


    Public Property ShowBottomPanel As Boolean
        Get
            Return pnlBottom.Visible
        End Get
        Set(value As Boolean)
            pnlBottom.Visible = value
        End Set
    End Property


    Public Property ShowDefaultView As Boolean
        Get
            Return pnlProfile.Visible
        End Get
        Set(value As Boolean)
            pnlProfile.Visible = value
            pnlPhoto.Visible = value
            pnlProfile2.Visible = value
        End Set
    End Property



    'Public Property CssClass As String
    '    Get
    '        If (ViewState("CssClass") IsNot Nothing) Then
    '            Return ViewState("CssClass")
    '        End If
    '        Return ""
    '    End Get
    '    Set(value As String)

    '        If (ViewState("CssClass") <> value) Then

    '            Dim oldStyle As String = ViewState("CssClass")

    '            If (Not String.IsNullOrEmpty(oldStyle)) Then
    '                pnlProfile.CssClass.Replace(oldStyle, "")
    '            End If

    '            If (Not String.IsNullOrEmpty(value)) Then
    '                pnlProfile.CssClass = pnlProfile.CssClass & " " & value
    '            End If

    '            ViewState("CssClass") = value
    '        End If

    '    End Set
    'End Property



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            If (Not Me.IsPostBack) Then
                LoadLAG()
                'LoadProfile(Me.MasterProfileId)


                'Dim prms As New clsSearchHelperParameters()
                'prms.CurrentProfileId = Me.MasterProfileId
                'prms.ReturnRecordsWithStatus = ProfileStatusEnum.Approved
                'prms.SearchSort = SearchSortEnum.NearestDistance
                'prms.zipstr = Me.GetCurrentProfile().Zip
                'prms.Distance = 1000
                'prms.LookingFor_ToMeetMaleID = Me.GetCurrentProfile().LookingFor_ToMeetMaleID
                'prms.LookingFor_ToMeetFemaleID = Me.GetCurrentProfile().LookingFor_ToMeetFemaleID
                'prms.NumberOfRecordsToReturn = 5
                'prms.AdditionalWhereClause = ""
                'dvNearest.DataSource = clsSearchHelper.GetMembersToSearchDataTable(prms)

                ''dvNearest.DataSource = clsSearchHelper.GetMembersToSearchDataTable(Me.MasterProfileId, ProfileStatusEnum.Approved, SearchSortEnum.NearestDistance, Me.GetCurrentProfile().Zip, 1000, 5)
                'dvNearest.DataBind()
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

        If (Not Page.IsPostBack) Then
        End If

    End Sub

    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        Try

            If (Not Me.IsPostBack) Then LoadProfile(Me.MasterProfileId)

            Dim btnUrl As String
            Dim currentUrl As String = Request.Url.PathAndQuery.ToUpper()

            btnUrl = lnkDashboard.NavigateUrl.TrimStart("~"c).ToUpper()
            If (btnUrl.StartsWith(currentUrl)) Then
                lnkDashboard.CssClass = lnkDashboard.CssClass & " selectedLink"
            Else
                lnkDashboard.CssClass = lnkDashboard.CssClass.Replace(" selectedLink", "")
            End If

            btnUrl = lnkProfile.NavigateUrl.TrimStart("~"c).ToUpper()
            If (btnUrl.StartsWith(currentUrl)) Then
                lnkProfile.CssClass = lnkProfile.CssClass & " selectedLink"
            Else
                lnkProfile.CssClass = lnkProfile.CssClass.Replace(" selectedLink", "")
            End If

            btnUrl = lnkEditPhotos.NavigateUrl.TrimStart("~"c).ToUpper()
            If (btnUrl.StartsWith(currentUrl)) Then
                lnkEditPhotos.CssClass = lnkEditPhotos.CssClass & " selectedLink"
            Else
                lnkEditPhotos.CssClass = lnkEditPhotos.CssClass.Replace(" selectedLink", "")
            End If

            btnUrl = lnkProfile.NavigateUrl.TrimStart("~"c).ToUpper()
            If (btnUrl.StartsWith(currentUrl)) Then
                lnkProfile.CssClass = lnkProfile.CssClass & " selectedLink"
            Else
                lnkProfile.CssClass = lnkProfile.CssClass.Replace(" selectedLink", "")
            End If

            btnUrl = lnkLikes.NavigateUrl.TrimStart("~"c).ToUpper()
            If (btnUrl.StartsWith(currentUrl)) Then
                lnkLikes.CssClass = lnkLikes.CssClass & " selectedLink"
            Else
                lnkLikes.CssClass = lnkLikes.CssClass.Replace(" selectedLink", "")
            End If

            btnUrl = lnkOffers.NavigateUrl.TrimStart("~"c).ToUpper()
            If (btnUrl.StartsWith(currentUrl)) Then
                lnkOffers.CssClass = lnkOffers.CssClass & " selectedLink"
            Else
                lnkOffers.CssClass = lnkOffers.CssClass.Replace(" selectedLink", "")
            End If


            btnUrl = lnkMessages.NavigateUrl.TrimStart("~"c).ToUpper()
            If (btnUrl.StartsWith(currentUrl)) Then
                lnkMessages.CssClass = lnkMessages.CssClass & " selectedLink"
            Else
                lnkMessages.CssClass = lnkMessages.CssClass.Replace(" selectedLink", "")
            End If


            btnUrl = lnkDates.NavigateUrl.TrimStart("~"c).ToUpper()
            If (btnUrl.StartsWith(currentUrl)) Then
                lnkDates.CssClass = lnkDates.CssClass & " selectedLink"
            Else
                lnkDates.CssClass = lnkDates.CssClass.Replace(" selectedLink", "")
            End If

            btnUrl = lnkBilling.NavigateUrl.TrimStart("~"c).ToUpper()
            If (btnUrl.StartsWith(currentUrl)) Then
                lnkBilling.CssClass = lnkBilling.CssClass & " selectedLink"
            Else
                lnkBilling.CssClass = lnkBilling.CssClass.Replace(" selectedLink", "")
            End If

            btnUrl = lnkWhoViewedMe.NavigateUrl.TrimStart("~"c).ToUpper()
            If (btnUrl.StartsWith(currentUrl)) Then
                lnkWhoViewedMe.CssClass = lnkWhoViewedMe.CssClass & " selectedLink"
            Else
                lnkWhoViewedMe.CssClass = lnkWhoViewedMe.CssClass.Replace(" selectedLink", "")
            End If

            btnUrl = lnkWhoFavoritedMe.NavigateUrl.TrimStart("~"c).ToUpper()
            If (btnUrl.StartsWith(currentUrl)) Then
                lnkWhoFavoritedMe.CssClass = lnkWhoFavoritedMe.CssClass & " selectedLink"
            Else
                lnkWhoFavoritedMe.CssClass = lnkWhoFavoritedMe.CssClass.Replace(" selectedLink", "")
            End If

            btnUrl = lnkWhoSharedPhotos.NavigateUrl.TrimStart("~"c).ToUpper()
            If (btnUrl.StartsWith(currentUrl)) Then
                lnkWhoSharedPhotos.CssClass = lnkWhoSharedPhotos.CssClass & " selectedLink"
            Else
                lnkWhoSharedPhotos.CssClass = lnkWhoSharedPhotos.CssClass.Replace(" selectedLink", "")
            End If

            btnUrl = lnkSharedPhotosWithWhom.NavigateUrl.TrimStart("~"c).ToUpper()
            If (btnUrl.StartsWith(currentUrl)) Then
                lnkSharedPhotosWithWhom.CssClass = lnkSharedPhotosWithWhom.CssClass & " selectedLink"
            Else
                lnkSharedPhotosWithWhom.CssClass = lnkSharedPhotosWithWhom.CssClass.Replace(" selectedLink", "")
            End If

            btnUrl = lnkMyFavoriteList.NavigateUrl.TrimStart("~"c).ToUpper()
            If (btnUrl.StartsWith(currentUrl)) Then
                lnkMyFavoriteList.CssClass = lnkMyFavoriteList.CssClass & " selectedLink"
            Else
                lnkMyFavoriteList.CssClass = lnkMyFavoriteList.CssClass.Replace(" selectedLink", "")
            End If

            btnUrl = lnkMyBlockedList.NavigateUrl.TrimStart("~"c).ToUpper()
            If (btnUrl.StartsWith(currentUrl)) Then
                lnkMyBlockedList.CssClass = lnkMyBlockedList.CssClass & " selectedLink"
            Else
                lnkMyBlockedList.CssClass = lnkMyBlockedList.CssClass.Replace(" selectedLink", "")
            End If

            btnUrl = lnkMyViewedList.NavigateUrl.TrimStart("~"c).ToUpper()
            If (btnUrl.StartsWith(currentUrl)) Then
                lnkMyViewedList.CssClass = lnkMyViewedList.CssClass & " selectedLink"
            Else
                lnkMyViewedList.CssClass = lnkMyViewedList.CssClass.Replace(" selectedLink", "")
            End If

            btnUrl = lnkNotifications.NavigateUrl.TrimStart("~"c).ToUpper()
            If (btnUrl.StartsWith(currentUrl)) Then
                lnkNotifications.CssClass = lnkNotifications.CssClass & " selectedLink"
            Else
                lnkNotifications.CssClass = lnkNotifications.CssClass.Replace(" selectedLink", "")
            End If

            'btnUrl = lnkOnline.NavigateUrl.TrimStart("~"c).ToUpper()
            'If (btnUrl.StartsWith(currentUrl)) Then
            '    lnkOnline.CssClass = lnkOnline.CssClass & " selectedLink"
            'Else
            '    lnkOnline.CssClass = lnkOnline.CssClass.Replace(" selectedLink", "")
            'End If

            'btnUrl = lnkAutomations.NavigateUrl.TrimStart("~"c).ToUpper()
            'If (btnUrl.StartsWith(currentUrl)) Then
            '    lnkAutomations.CssClass = lnkAutomations.CssClass & " selectedLink"
            'Else
            '    lnkAutomations.CssClass = lnkAutomations.CssClass.Replace(" selectedLink", "")
            'End If


            If (liLikes.Attributes("class") Is Nothing) Then liLikes.Attributes.Add("class", "")
            If (liOffers.Attributes("class") Is Nothing) Then liOffers.Attributes.Add("class", "")
            If (liMessages.Attributes("class") Is Nothing) Then liMessages.Attributes.Add("class", "")
            If (liDates.Attributes("class") Is Nothing) Then liDates.Attributes.Add("class", "")

            liLikes.Attributes("class") = liLikes.Attributes("class").Replace(" limited", "")
            liOffers.Attributes("class") = liOffers.Attributes("class").Replace(" limited", "")
            liMessages.Attributes("class") = liMessages.Attributes("class").Replace(" limited", "")
            liDates.Attributes("class") = liDates.Attributes("class").Replace(" limited", "")

            If (Me.SessionVariables.MemberData IsNot Nothing AndAlso Me.SessionVariables.MemberData.Status = ProfileStatusEnum.LIMITED) Then
                liLikes.Attributes("class") = liLikes.Attributes("class") & " limited"
                liOffers.Attributes("class") = liOffers.Attributes("class") & " limited"
                liMessages.Attributes("class") = liMessages.Attributes("class") & " limited"
                liDates.Attributes("class") = liDates.Attributes("class") & " limited"

                Dim likes As String = WhatIsIt.OnMoreInfoClickFunc(ResolveUrl("~/Members/InfoWin.aspx?info=Limited_Access"), lnkLikes.Text, 650, 400)
                liLikes.Attributes.Add("onclick", likes)

                Dim Offers As String = WhatIsIt.OnMoreInfoClickFunc(ResolveUrl("~/Members/InfoWin.aspx?info=Limited_Access"), lnkOffers.Text, 650, 400)
                liOffers.Attributes.Add("onclick", Offers)

                Dim Messages As String = WhatIsIt.OnMoreInfoClickFunc(ResolveUrl("~/Members/InfoWin.aspx?info=Limited_Access"), lnkMessages.Text, 650, 400)
                liMessages.Attributes.Add("onclick", Messages)

                Dim Dates As String = WhatIsIt.OnMoreInfoClickFunc(ResolveUrl("~/Members/InfoWin.aspx?info=Limited_Access"), lnkDates.Text, 650, 400)
                liDates.Attributes.Add("onclick", Dates)

                lnkLikes.NavigateUrl = "javascript:void(0)"
                lnkLikes.Attributes.Remove("onclick")

                lnkOffers.NavigateUrl = "javascript:void(1)"
                lnkOffers.Attributes.Remove("onclick")

                lnkMessages.NavigateUrl = "javascript:void(2)"
                lnkMessages.Attributes.Remove("onclick")

                lnkDates.NavigateUrl = "javascript:void(3)"
                lnkDates.Attributes.Remove("onclick")
            End If

        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try
    End Sub


    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
        LoadProfile(Me.MasterProfileId)
    End Sub


    Protected Sub LoadLAG()
        Try
            '  Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            lnkProfile.Text = globalStrings.GetCustomString("MyProfile")
            lnkEditPhotos.Text = globalStrings.GetCustomString("Photos")
            'lnkTestemonials.Text = globalStrings.GetCustomString("Testemonials")
            ' lnkProfile.Text = globalStrings.GetCustomString("MyProfile")

            lnkBilling.Text = CurrentPageData.GetCustomString("StandardMember")
            lnkMembersNewestAll.Text = CurrentPageData.GetCustomString("lnkMembersNewestAll")
            lnkMembersNewest_InCountry.Text = CurrentPageData.GetCustomString("lnkMembersNewest_InCountry")
            lnkMembersNearAll.Text = CurrentPageData.GetCustomString("lnkMembersNearAll")
            'lnkMembersNewest.Text = CurrentPageData.GetCustomString(lnkMembersNewest.ID)
            'lnkMembersNear.Text = CurrentPageData.GetCustomString(lnkMembersNear.ID)

            lnkWhoViewedMe.Text = CurrentPageData.GetCustomString("lnkWhoViewedMe")
            lnkWhoFavoritedMe.Text = CurrentPageData.GetCustomString("lnkWhoFavoritedMe")
            lnkWhoSharedPhotos.Text = CurrentPageData.GetCustomString("lnkWhoSharedPhotos")
            lnkSharedPhotosWithWhom.Text = CurrentPageData.GetCustomString("lnkSharedPhotosWithWhom")
            lnkMyFavoriteList.Text = CurrentPageData.GetCustomString("lnkMyFavoriteList")
            lnkMyBlockedList.Text = CurrentPageData.GetCustomString("lnkMyBlockedList")
            lnkMyViewedList.Text = CurrentPageData.GetCustomString("lnkMyViewedList")

            lnkDashboard.Text = CurrentPageData.GetCustomString("lnkDashboard")
            lnkLikes.Text = CurrentPageData.GetCustomString("lnkLikes")
            lnkOffers.Text = CurrentPageData.GetCustomString("lnkOffers")
            lnkMessages.Text = CurrentPageData.GetCustomString("lnkMessages")
            lnkDates.Text = CurrentPageData.GetCustomString("lnkDates")
            lnkNotifications.Text = CurrentPageData.GetCustomString("lnkNotifications")
            'lnkAutomations.Text = CurrentPageData.GetCustomString("lnkAutomations")
            lnkGiftCards.Text = CurrentPageData.GetCustomString("lnkGiftCards")
            lblHeaderQuickLinks.Text = CurrentPageData.GetCustomString(lblHeaderQuickLinks.ID)



            lnkHowWorksMale.Text = CurrentPageData.GetCustomString("lnkHowWorks")
            lnkHowWorksFemale.Text = CurrentPageData.GetCustomString("lnkHowWorks")
            'lnkReferrer.Text = CurrentPageData.GetCustomString("lnkAffiliate")
            lnkAffiliate.Text = CurrentPageData.GetCustomString("lnkAffiliate2")
           


            If (Me.IsFemale) Then
                liHowWorksMale.Visible = False
                liHowWorksFemale.Visible = True
                ' liReferrer.Visible = True
                liAffiliate.Visible = False
            ElseIf (Me.IsMale AndAlso clsCurrentContext.IsAffiliate()) Then
                liHowWorksMale.Visible = False
                liHowWorksFemale.Visible = False
                liAffiliate.Visible = True
                'liReferrer.Visible = False
                pnlBottom.Visible = True
            Else
                liHowWorksMale.Visible = True
                liHowWorksFemale.Visible = False

                liAffiliate.Visible = False
                ' liReferrer.Visible = False
            End If



            lnkViewDescription.Text = Me.CurrentPageData.GetCustomString("QuickLinksWhatIsIt")
            lnkViewDescription.ToolTip = lnkViewDescription.Text
            lnkViewDescription.OnClientClick = WhatIsIt.OnMoreInfoClickFunc(
                lnkViewDescription.OnClientClick,
                ResolveUrl("~/Members/InfoWin.aspx?info=quicklinks"),
                Me.CurrentPageData.GetCustomString("lblHeaderQuickLinks"))


        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Sub LoadProfile(ByVal profileId As Integer)
        Dim textPatternParentCSS As String = "with-number"
        Dim textPattern As String = <html><![CDATA[
#TEXT# <span id="notificationsCountWrapper" class="jewelCount"><span class="countValue">#COUNT#</span></span>
]]></html>.Value


        Try

            'Dim _profilerows As New clsProfileRows(profileId)
            'Dim masterRow As EUS_ProfilesRow = _profilerows.GetMasterRow()

            ltrLogin.Text = Me.SessionVariables.MemberData.LoginName

            'If Me.IsMale Then

            '    'If (ProfileHelper.IsGenerous(Me.GetCurrentProfile().AccountTypeId)) Then
            '    ltrInfo.Text = globalStrings.GetCustomString("msg_GenerousMale")
            '    'ElseIf (ProfileHelper.IsAttractive(Me.GetCurrentProfile().AccountTypeId)) Then
            '    '    ltrInfo.Text = globalStrings.GetCustomString("msg_AttractiveMale")
            '    'End If

            'ElseIf Me.IsFemale Then

            '    'If (ProfileHelper.IsGenerous(Me.GetCurrentProfile().AccountTypeId)) Then
            '    '    ltrInfo.Text = globalStrings.GetCustomString("msg_GenerousFemale")
            '    'ElseIf (ProfileHelper.IsAttractive(Me.GetCurrentProfile().AccountTypeId)) Then
            '    ltrInfo.Text = globalStrings.GetCustomString("msg_AttractiveFemale")
            '    'End If

            'End If



            'Dim totalMemberCredits As Integer?
            'Dim memberCreditsQuery = From itm In Me.CMSDBDataContext.EUS_CustomerCredits
            '                       Where itm.CustomerId = Me.MasterProfileId
            '                       Select itm.Credits

            'If (memberCreditsQuery IsNot Nothing) Then
            '    totalMemberCredits = memberCreditsQuery.Sum()
            'End If


            If (Me.pnlProfile2.Visible) Then

                If (Me.IsMale) Then
                    liBilling.Visible = True
                    '  liGiftCards.Visible = False
                    Try

                        'Dim sql As String = "EXEC [CustomerAvailableCredits] @CustomerId = " & Me.MasterProfileId & ""
                        'Dim dt As DataTable = DataHelpers.GetDataTable(sql)

                        'If (dt.Rows(0)("CreditsRecordsCount") > 0) Then

                        Dim __cac As clsCustomerAvailableCredits = BasePage.GetCustomerAvailableCredits(Me.MasterProfileId)
                        If (__cac.CreditsRecordsCount > 0) Then
                            lnkBilling.NavigateUrl = "~/Members/SelectProduct.aspx"
                            lnkBilling.Text = CurrentPageData.GetCustomString("Credits")
                            lnkBilling.Text = textPattern.Replace("#TEXT#", lnkBilling.Text).Replace("#COUNT#", "(" & __cac.AvailableCredits & ")")
                        ElseIf (__cac.HasSubscription) Then
                            lnkBilling.NavigateUrl = "~/Members/SelectProduct.aspx"
                            lnkBilling.Text = CurrentPageData.GetCustomString("Credits")
                            lnkBilling.Text = textPattern.Replace("#TEXT#", lnkBilling.Text).Replace("#COUNT#", "")
                        Else
                            lnkBilling.NavigateUrl = "~/Members/SelectProduct.aspx"
                            lnkBilling.Text = CurrentPageData.GetCustomString("StandardMember")
                        End If

                    Catch ex As System.Threading.ThreadAbortException
                    Catch ex As Exception
                        WebErrorMessageBox(Me, ex, "")
                    End Try
                    liGiftCards.Visible = False
                Else
                    liBilling.Visible = False

                    Try
                        Dim affCredits As clsReferrerCredits
                        Dim credits As Double = 0


                        affCredits = New clsReferrerCredits(Me.MasterProfileId, Me.SessionVariables.MemberData.LoginName, New DateTime(2010, 1, 1), DateTime.UtcNow.AddDays(1))
                        affCredits.Load()
                        Dim amount As Dictionary(Of String, Object) = clsReferrerCredits.GetTotalPayment(Me.MasterProfileId)
                        Dim LastPaymentAmount As Double = amount("LastPaymentAmount")
                        Dim LastPaymentDate As DateTime = amount("LastPaymentDate")
                        Dim TotalDebitAmount As Double = amount("TotalDebitAmount")
                        Dim totalMoneyToBePaid As Double = affCredits.CommissionCreditsTotal - TotalDebitAmount
                        Dim AvailableAmount As Double = affCredits.CommissionCreditsAvailable - TotalDebitAmount
                        Dim PendingAmount As Double = affCredits.CommissionCreditsPending
                        If (AvailableAmount < 0) Then
                            PendingAmount = PendingAmount + AvailableAmount
                            AvailableAmount = 0
                        End If
                        credits = Math.Round(Math.Floor(AvailableAmount), 0)
                        lnkGiftCards.Text = CurrentPageData.GetCustomString("lnkGiftCards")
                        lnkGiftCards.Text = textPattern.Replace("#TEXT#", lnkGiftCards.Text).Replace("#COUNT#", "(" & credits & ")")
                    Catch ex As Exception

                    End Try
                    liGiftCards.Visible = True
                End If

            End If

        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        If (pnlPhoto.Visible) Then

            Try
                ' view icon of current user, that has logged in 

                'Dim fileName As String = Nothing
                Dim photo As DSMembers.EUS_CustomerPhotosRow = DataHelpers.GetProfilesDefaultPhoto(profileId)
                'If (photo IsNot Nothing) Then
                '    imgPhoto.ImageUrl = ProfileHelper.GetProfileImageURL(photo.CustomerID, photo.FileName, Me.SessionVariables.MemberData.GenderId, True, Me.IsHTTPS)
                'Else
                '    imgPhoto.ImageUrl = ProfileHelper.GetDefaultImageURL(Me.SessionVariables.MemberData.GenderId)
                'End If
                If (photo IsNot Nothing) Then
                    imgPhoto.ImageUrl = ProfileHelper.GetProfileImageURL(profileId, photo.FileName, Me.SessionVariables.MemberData.GenderId, True, Me.IsHTTPS)
                Else
                    imgPhoto.ImageUrl = ProfileHelper.GetProfileImageURL(profileId, Nothing, Me.SessionVariables.MemberData.GenderId, True, Me.IsHTTPS)
                    If (imgPhoto.ImageUrl.Contains(ProfileHelper.Male_DefaultImagePrivate.Replace("~/", ""))) Then
                        imgPhoto.ImageUrl = imgPhoto.ImageUrl.Replace(ProfileHelper.Male_DefaultImagePrivate.Replace("~/", ""), ProfileHelper.Male_DefaultImagePrivate180.Replace("~/", ""))
                    ElseIf (imgPhoto.ImageUrl.Contains(ProfileHelper.Female_DefaultImagePrivate.Replace("~/", ""))) Then
                        imgPhoto.ImageUrl = imgPhoto.ImageUrl.Replace(ProfileHelper.Female_DefaultImagePrivate.Replace("~/", ""), ProfileHelper.Female_DefaultImagePrivate180.Replace("~/", ""))
                    End If
                End If

                'If (profileId = 269) Then
                '    If (Request.Url.Scheme = "https") Then
                '        imgPhoto.ImageUrl = imgPhoto.ImageUrl.Replace("http://", "https://")
                '    End If
                'End If
                lnkPhoto.NavigateUrl = ResolveUrl("~/Members/Photos.aspx")

            Catch ex As System.Threading.ThreadAbortException
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try

        End If

        Try
            ' set numbers to Who viewed me, who favorited me
            Dim memberActionsCounters As clsGetMemberActionsCounters = BasePage.GetMemberActionsCounters(Me.MasterProfileId)

            If (memberActionsCounters.CountWhoViewedMe > 0) Then
                lnkWhoViewedMe.Text = CurrentPageData.GetCustomString("lnkWhoViewedMe")
                lnkWhoViewedMe.Text = textPattern.Replace("#TEXT#", lnkWhoViewedMe.Text).Replace("#COUNT#", memberActionsCounters.CountWhoViewedMe)
                liWhoViewedMe.Attributes.Add("class", " " & textPatternParentCSS)
            Else
                lnkWhoViewedMe.Text = CurrentPageData.GetCustomString("lnkWhoViewedMe")
            End If

            If (memberActionsCounters.CountWhoFavoritedMe > 0) Then
                lnkWhoFavoritedMe.Text = CurrentPageData.GetCustomString("lnkWhoFavoritedMe")
                lnkWhoFavoritedMe.Text = textPattern.Replace("#TEXT#", lnkWhoFavoritedMe.Text).Replace("#COUNT#", memberActionsCounters.CountWhoFavoritedMe)
                liWhoFavoritedMe.Attributes.Add("class", " " & textPatternParentCSS)
            Else
                lnkWhoFavoritedMe.Text = CurrentPageData.GetCustomString("lnkWhoFavoritedMe")
            End If

            If (memberActionsCounters.CountWhoSharedPhotos > 0) Then
                lnkWhoSharedPhotos.Text = CurrentPageData.GetCustomString("lnkWhoSharedPhotos")
                lnkWhoSharedPhotos.Text = textPattern.Replace("#TEXT#", lnkWhoSharedPhotos.Text).Replace("#COUNT#", memberActionsCounters.CountWhoSharedPhotos)
                liWhoSharedPhotos.Attributes.Add("class", " " & textPatternParentCSS)
            Else
                lnkWhoSharedPhotos.Text = CurrentPageData.GetCustomString("lnkWhoSharedPhotos")
            End If

        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Try
            If (ConfigurationManager.AppSettings("AllowSubscription") = "1") Then
                If (ProfileCountry.MonthlySubscriptionEnabled) Then
                    lnkBilling.NavigateUrl = "~/Members/SelectProductSubscription.aspx"
                End If
            End If
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorSendEmail(ex, "")
        End Try
    End Sub
    'Protected Function GetReferrerCreditsObject(dateFrom As DateTime?, dateTo As DateTime?)
    '    If (affCredits Is Nothing) Then

    '        Try
    '            If (affCredits Is Nothing) Then
    '                Dim _dtFrom As DateTime?
    '                Dim _dtTo As DateTime?
    '                If (dateFrom.HasValue) Then _dtFrom = dateFrom.Value.Date
    '                If (dateTo.HasValue) Then _dtTo = dateTo.Value.Date.AddDays(1).AddMilliseconds(-1)

    '                affCredits = New clsReferrerCredits(Me.MasterProfileId, Me.SessionVariables.MemberData.LoginName, _dtFrom, _dtTo)
    '                affCredits.Load()
    '            End If

    '        Catch ex As Exception
    '            WebErrorMessageBox(Me, ex, "")
    '        End Try
    '    End If

    '    Return affCredits
    'End Function

    Public Sub RefreshThumb()
        LoadProfile(Me.MasterProfileId)
    End Sub


    Protected Sub sdsNewest_Selecting(sender As Object, e As System.Web.UI.WebControls.SqlDataSourceSelectingEventArgs) Handles sdsNewest.Selecting
        For Each prm As SqlClient.SqlParameter In e.Command.Parameters

            If (prm.ParameterName = "@CurrentProfileId") Then
                prm.Value = Me.MasterProfileId
            ElseIf (prm.ParameterName = "@ReturnRecordsWithStatus") Then
                prm.Value = ProfileStatusEnum.Approved
            ElseIf (prm.ParameterName = "@NumberOfRecordsToReturn") Then
                prm.Value = 5
            ElseIf (prm.ParameterName = "@LastActivityUTCDate") Then
                Dim mins As Integer = clsConfigValues.Get__members_online_minutes()
                Dim LastActivityUTCDate As DateTime = Date.UtcNow.AddMinutes(-mins)
                prm.Value = LastActivityUTCDate
            End If

        Next
    End Sub



    'Protected Sub sdsNearest_Selecting(sender As Object, e As System.Web.UI.WebControls.SqlDataSourceSelectingEventArgs) Handles sdsNearest.Selecting
    '    For Each prm As SqlClient.SqlParameter In e.Command.Parameters

    '        If (prm.ParameterName = "@CurrentProfileId") Then
    '            prm.Value = Me.MasterProfileId
    '        ElseIf (prm.ParameterName = "@ReturnRecordsWithStatus") Then
    '            prm.Value = ProfileStatusEnum.Approved
    '        ElseIf (prm.ParameterName = "@NumberOfRecordsToReturn") Then
    '            prm.Value = 5
    '        End If

    '    Next
    'End Sub

    'Protected Sub sdsNewest_Selected(sender As Object, e As System.Web.UI.WebControls.SqlDataSourceStatusEventArgs) Handles sdsNewest.Selected

    'End Sub


    Protected Sub dvNewest_DataBound(sender As Object, e As EventArgs) Handles dvNewest.DataBound

        Try
            Dim isSameCountry As Boolean = True
            Dim prevCountry As String = ""

            For cnt = 0 To dvNewest.Items.Count - 1

                Dim dvi As DevExpress.Web.ASPxDataView.DataViewItem = dvNewest.Items(cnt)
                Dim dr As DataRowView = dvi.DataItem


                If (prevCountry = "") Then
                    isSameCountry = True
                Else
                    isSameCountry = isSameCountry AndAlso (prevCountry = dr("Country"))
                End If
                prevCountry = dr("Country")
            Next

            lnkMembersNewestAll.Visible = False
            lnkMembersNewest_InCountry.Visible = True

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub

    'Protected Sub dvNewest_DataBinding(sender As Object, e As EventArgs) Handles dvNewest.DataBinding

    'End Sub


    Protected Shared Function GetOnClickJS(LoginName As String) As String
        Dim fn As String = "jsLoad(""" & System.Web.VirtualPathUtility.ToAbsolute("~/Members/Profile.aspx?p=") & HttpUtility.UrlEncode(LoginName) & """)"
        Return fn
    End Function

    Public Function evaluateAge(birthday As Object) As String
        Dim age As String
        If birthday IsNot Nothing Then
            Try
                age = Me.CurrentPageData.VerifyCustomString("lblAgeYearsOld").Replace("###AGE###", ProfileHelper.GetCurrentAge(birthday))
                Return "<div class=""member_age"">" + age + "</div>"
            Catch ex As Exception
                Return ""
                WebErrorMessageBox(Me, ex, "")
            End Try
        Else
            Return ""
        End If
        

    End Function
End Class

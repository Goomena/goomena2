﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucRewardGiftsCards.ascx.vb" Inherits="Dating.Server.Site.Web.ucRewardGiftsCards" %>
<%@ Import Namespace="Dating.Server.Core.DLL" %>

          <div class="GiftCards-title">
              <div class="TitleContainer">
                  <asp:Literal ID="lblTitle" runat="server"  Text ="select"></asp:Literal> 
                  <asp:LinkButton ID="lnkViewDescription" runat="server" CssClass="more_info2" OnClientClick="OnMoreInfoClick('[contentUrl]', this, '[headerText]');return false;" />
              </div>
             
           <asp:Literal ID="lblTopText" runat="server"  Text ="Description"></asp:Literal>
          </div>
<div class="ReapeaterHeaderConatiner">
    <div class="ReapeaterHeader"></div>
</div>
          <div class="rptContainer">
             

<asp:Repeater ID="rptGiftCards" runat="server">
<ItemTemplate>
       <div class="GiftCardBox">
           <div class="GiftCardInnerBox">
                        <div class="Header">
                            <asp:Label ID="lblTitle" runat="server" CssClass="lblTitle" Text='<%# Eval("Title")%>'></asp:Label>
                        </div>
                           <div class="Image">
                               <img Class ="imgGiftCard"  src ='<%# clsGiftCardHelper.ImageUrl(Eval("ImageUrl"))%>'/>
                           </div>
                            <div class="Description">
                                <asp:literal ID="ltrDescription"  runat="server" Text='<%# GetText(Eval("Description"))%>'></asp:literal>
                            </div>
               <div class="btnSelect">
                   <asp:LinkButton ID="btnSelect" CssClass="btnSelectGiftCard" 
                       CommandArgument='<%# Eval("RewardGiftsCardsId")%>'
                       CommandName="Select"
                        runat="server"><span class="btnSelectText"><%# Me._SelectLabel%></span>
                   </asp:LinkButton>
               </div>
     
  
        </div>
            <div class="SepLine"></div>
                </div>
 


</ItemTemplate>
</asp:Repeater>
           <div class="clear"></div>
           
          </div>
   <div class="Shadow"></div>
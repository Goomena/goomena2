﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucRewardGiftsAvaliable.ascx.vb" Inherits="Dating.Server.Site.Web.ucRewardGiftsAvaliable" %>
<%@ Import Namespace="Dating.Server.Core.DLL" %>



<script type="text/javascript">
    function showpopup(Id, CardTitle, Points, Amount) {
        var popup = window["ASPxPopupClientControl"];
       var message ='<%= Me._PopupMessage%>';
        message=message.replace("###Card###", CardTitle);
        message=message.replace("###Amount###", Amount+'€');
        message=message.replace("###Points###", Points);
        document.getElementById("<%= lblMessage.ClientID%>").innerHTML = message;
        document.getElementById("<%= ValueHiddenField.ClientID%>").value = Id;
        popup.Show();
    }
    function OnBtnClientClick() {
        var popup = window["ASPxPopupClientControl"];
        popup.Hide();
    }
</script>


<div class="GiftCards-title">
    <div class="TitleContainer">
        <asp:Literal ID="lblTitle" runat="server" Text="select"></asp:Literal>
        <asp:LinkButton ID="lnkViewDescription" runat="server" CssClass="more_info2" OnClientClick="OnMoreInfoClick('[contentUrl]', this, '[headerText]');return false;" />
    </div>
    <asp:Literal ID="lblTopText" runat="server" Text="Description"></asp:Literal>
</div>
<div class="ReapeaterHeaderConatiner">
      <asp:Label ID="lblError" CssClass="ErrorBox" runat="server" Text="" Visible="false" ></asp:Label>
    <div class="AvaliableHeader"></div>
</div>
<div class="rptOrderGiftCards">
  
    <asp:Repeater ID="rptOrderGiftCards" runat="server">
        <ItemTemplate>
            <div class="GiftCardBox2">
                <div class="GiftCardInnerBox2 <%# Me.CardTitle.Replace(" ", "")%>">
                    <div class="Amount">
                        <asp:Label ID="lblAmount" CssClass="lblAmmount" runat="server" Text='<%# clsNullable.DBNullToInteger(Eval("Amount")) & "€ GIFT CARD"%>'></asp:Label>
                        <img class="ValuedGiftCard" src='<%# Me.GetImageValue(Eval("Points"))%>' />
                    </div>
                    <div class="Description">
                        <asp:Literal ID="ltrDescription" runat="server" Text='<%# GetText(Eval("Description"))%>'></asp:Literal>
                    </div>
                    <div class="btnSelect">
                        <%# GetButton(Eval("Points"), Eval("RewardsGiftCardsAvaliableId"), Eval("Amount"))%>

                        <%--<a id="btnSelect" onclick='showpopup(<%# Eval("RewardsGiftCardsAvaliableId")%>)' class=' ,"btnSelectGiftCard","btnSelectGiftCard Disabled") %>' <%# If(Eval("Points") <= Me.AvaliableCredits, "", "disabled")%>><span class="btnSelectText"><%# Me._SelectLabel%></span></a>--%>

                        <asp:Literal ID="ltrPopup" runat="server" Text='<%# GetPopupMessage(Eval("Points"))%>'></asp:Literal>
                    </div>
                </div>
            </div>
        </ItemTemplate>
    </asp:Repeater>
    <div class="clear"></div>

    <asp:Panel ID="pnlPopUp" CssClass="AvaliablePopUpConfirm" runat="server" Visible="true">
        <dx:ASPxPopupControl ClientInstanceName="ASPxPopupClientControl" Width="423px" Height="213px"
            MaxWidth="423px" MaxHeight="213px" MinHeight="213px" MinWidth="423px" ID="pcMain"
            ShowFooter="false"  PopupElementID="imgButton" ShowShadow="false" Border-BorderStyle="None" HeaderText=""
            PopupVerticalAlign="WindowCenter"
            PopupHorizontalAlign="WindowCenter"
            Modal="True"
            runat="server" EnableViewState="false" EnableHierarchyRecreation="True" ShowHeader="False" ShowLoadingPanel="False" CloseAction="None">
            <ContentCollection>
                <dx:PopupControlContentControl runat="server">
                    <asp:Label ID="lblMessage" CssClass="PopUpMessage" runat="server" Text=""></asp:Label>
                    <asp:LinkButton ID="btnConfirm" CssClass="btnConfirm"   runat="server" OnClientClick="OnBtnClientClick()">
                        <asp:Label ID="lblbtnConfirm" runat="server" Text="" ></asp:Label>
                    </asp:LinkButton>
                    <a onclick="OnBtnClientClick()" class="btnCancel">
                        <asp:Label ID="lblCancel" runat="server" Text=""></asp:Label></a>
                    <asp:HiddenField ID="ValueHiddenField"
                       Value=""
                        runat="server" />
                </dx:PopupControlContentControl>
            </ContentCollection>
            <ClientSideEvents CloseUp="function(s, e) { SetImageState(false); }" PopUp="function(s, e) { SetImageState(true); }" />
        </dx:ASPxPopupControl>



    </asp:Panel>
</div>






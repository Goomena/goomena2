﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucCouponsList.ascx.vb" Inherits="Dating.Server.Site.Web.ucCouponsList" %>
<asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="2">

    <asp:View ID="View1" runat="server">
    </asp:View>
    
    
    <asp:View ID="vwNoCoupons" runat="server">
        <asp:FormView ID="fvNoCoupons" runat="server">
        <ItemTemplate>
<div class="items_none">
    <div id="dates_icon" class="middle_icon"></div>
    <div class="items_none_text">
        <%# Eval("NoOfferText")%>
        <div class="search_members_button">
	        <asp:HyperLink ID="lnk4" runat="server" CssClass="btn lighter" NavigateUrl="~/Members/Search.aspx" onclick="ShowLoading();"><%# Eval("CouponsNotFoundText")%><i class="icon-chevron-right"></i></asp:HyperLink>
        </div>
	    <div class="clear"></div>
    </div>
</div>
        </ItemTemplate>
        </asp:FormView>
    </asp:View>


    <asp:View ID="vwCouponsOffers" runat="server">
<asp:Repeater ID="rptCoupons" runat="server">
<ItemTemplate>


<div class="PreviewCouponContainer">
    <div class="PreviewCouponArea">
        <div class="lfloat Preview">
            <div class="CouponFirstLine">
                <asp:Label ID="lblCouponFirstLine" CssClass="lblCouponFirstLine" runat="server" Text='<%# Eval("CouponFirstLine")%>'>
                </asp:Label>
            </div>
            <div class="PreviewCouponcredits">
                <div class="PreviewCouponcreditsCont">
                    <asp:Label ID="lblPreviewCouponCredits" CssClass="lblPreviewCouponCredits" runat="server" Text='<%# Eval("CreditAmmountText")%>'>
                    </asp:Label>
                    <asp:Label ID="lblCouponCreditsΤext" CssClass="lblCouponCreditsΤext" runat="server" Text='<%# Eval("CouponCreditsΤext")%>'>
                    </asp:Label>
                </div>
            </div>

            <div class="PreviewCouponName">
                <asp:Label ID="lblPreviewCouponName" CssClass="lblPreviewCouponName" runat="server" Text='<%# Eval("CouponName")%>'>
                </asp:Label>
            </div>

            <div class="CouponBarcode">
            </div>
            <div class="CouponBotomNotes">
                <asp:Label ID="lblCouponBotomNotes" CssClass="lblCouponBotomNotes" runat="server" Text='<%# Eval("CouponBotomNotes")%>'>
                </asp:Label>
            </div>

        </div>
        <div class="lfloat PreviewOnlyPrint">
            <div class="PreviewCouponMoney">
                <asp:Label ID="lblCouponMoney" CssClass="PreviewlblCouponMoney" runat="server" Text='<%# Eval("CreditAmmountText")%>'></asp:Label>
            </div>
            <div class="CouponCode">
                <asp:Label ID="lblCouponCode" CssClass="PreviewlblCouponCode" runat="server" Text='<%# Eval("CouponCode")%>'></asp:Label>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</div>

</ItemTemplate>
</asp:Repeater>
 </asp:View>


</asp:MultiView>
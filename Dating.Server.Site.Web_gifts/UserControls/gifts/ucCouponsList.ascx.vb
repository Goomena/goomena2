﻿Imports Dating.Server.Core.DLL
Imports DevExpress.Web.ASPxEditors

Public Class ucCouponsList
    Inherits BaseUserControl

    Private _BindControlDataComplete As Boolean

    Protected Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            'If (_pageData Is Nothing) Then _pageData = New clsPageData("control.OfferControl", Context)
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData("control.CouponPreview", Context, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property


    Public Property ItemsPerPage As Integer
        Get
            If (Me.ViewState("ItemsPerPage") IsNot Nothing) Then Return Me.ViewState("ItemsPerPage")
            Return 10
        End Get
        Set(value As Integer)
            Me.ViewState("ItemsPerPage") = value
        End Set
    End Property




    Private Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
    End Sub


    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        BindControlData()
    End Sub

    Public Function BindControlData() As Integer
        Dim recordsFound As Integer = 0
        If (Not _BindControlDataComplete) Then
            Try

                Dim sql As String = <sql><![CDATA[
SELECT [CouponId]
      ,[ProfileId]
      ,[MirrorProfileId]
      ,[CreditAmmount]
      ,[DebitAmmount]
      ,[DatetimeCreated]
      ,[CouponName]
      ,[IP]
      ,[SessionID]
      ,[LoginName]
      ,[PassWord]
      ,[Email]
      ,[CouponCode]
      ,[CouponHash]
      ,[Country]
      ,[TransactionId]
  FROM [dbo].[Aff_Coupons]
where ProfileId=@MasterProfileID
order by CouponId desc
]]></sql>
                Dim dtResults As DataTable
                Using con As SqlClient.SqlConnection = DataHelpers.GetSqlConnection


                    Using cmd As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(con, sql)


                        cmd.Parameters.AddWithValue("@MasterProfileID", Me.MasterProfileId)

                        dtResults = DataHelpers.GetDataTable(cmd)
                    End Using
                End Using
                dtResults.Columns.Add("CouponFirstLine", GetType(String))
                dtResults.Columns.Add("CouponCreditsΤext", GetType(String))
                dtResults.Columns.Add("CouponBotomNotes", GetType(String))
                dtResults.Columns.Add("CreditAmmountText", GetType(String))

                Dim lblCouponFirstLine As String = CurrentPageData.GetCustomString("lblCouponFirstLine")
                Dim lblCouponCreditsΤext As String = CurrentPageData.GetCustomString("lblCouponCreditsΤext")
                Dim lblCouponBotomNotes As String = CurrentPageData.GetCustomString("lblCouponBotomNotes")

                For cnt = 0 To dtResults.Rows.Count - 1
                    dtResults.Rows(cnt)("CouponFirstLine") = lblCouponFirstLine
                    dtResults.Rows(cnt)("CouponCreditsΤext") = lblCouponCreditsΤext
                    dtResults.Rows(cnt)("CouponBotomNotes") = lblCouponBotomNotes
                    dtResults.Rows(cnt)("CreditAmmountText") = clsNullable.DBNullToDouble(dtResults.Rows(cnt)("CreditAmmount")).ToString("0.#")
                Next

                rptCoupons.DataSource = dtResults
                rptCoupons.DataBind()

                If (dtResults IsNot Nothing) Then
                    recordsFound = dtResults.Rows.Count
                End If
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try

            _BindControlDataComplete = True
        Else
            Dim dtResults As DataTable = rptCoupons.DataSource
            If (dtResults IsNot Nothing) Then
                recordsFound = dtResults.Rows.Count
            End If
        End If

        Return recordsFound
    End Function


    'Protected Sub dvWinks_DataBound(sender As Object, e As EventArgs) Handles dvWinks.DataBound
    '    '<%# Eval("OtherMemberLoginName")%> <%# Eval("WantToKnowFirstDatePriceText")%>
    '    Try
    '        For Each dvi As DevExpress.Web.ASPxDataView.DataViewItem In dvWinks.Items
    '            Try

    '                Dim dr As DataRowView = dvi.DataItem

    '                '' CHECK OFFER CASES
    '                Dim OfferID = dr("OfferID")
    '                Dim OfferTypeID = dr("OffersOfferTypeID")
    '                Dim OffersStatusID = dr("OffersStatusID")
    '                Dim OffersFromProfileID = dr("OffersFromProfileID")
    '                Dim OffersToProfileID = dr("OffersToProfileID")
    '                Dim OfferAmount = dr("OffersAmount")

    '                Dim lblYouReceivedWinkOfferText As Literal = dvWinks.FindItemControl("lblYouReceivedWinkOfferText", dvi)
    '                Dim lblWantToKnowFirstDatePriceText As Literal = dvWinks.FindItemControl("lblWantToKnowFirstDatePriceText", dvi)

    '                If (OfferTypeID = ProfileHelper.OfferTypeID_POKE) Then
    '                    lblYouReceivedWinkOfferText.Text = Me.CurrentPageData.GetCustomString("YouReceivedPokeText")
    '                    lblYouReceivedWinkOfferText.Text = lblYouReceivedWinkOfferText.Text.Replace("###LOGINNAME###", dr("LoginName"))

    '                    lblWantToKnowFirstDatePriceText.Text = Me.CurrentPageData.GetCustomString("YouReceivedPokeDescriptionText")
    '                    lblWantToKnowFirstDatePriceText.Text = lblWantToKnowFirstDatePriceText.Text.Replace("###LOGINNAME###", dr("LoginName"))
    '                Else
    '                    lblYouReceivedWinkOfferText.Text = Me.CurrentPageData.GetCustomString("YouReceivedWinkText")
    '                    lblYouReceivedWinkOfferText.Text = lblYouReceivedWinkOfferText.Text.Replace("###LOGINNAME###", dr("LoginName"))

    '                    lblWantToKnowFirstDatePriceText.Text = Me.CurrentPageData.GetCustomString("WantToKnowFirstDatePriceText")
    '                    lblWantToKnowFirstDatePriceText.Text = lblWantToKnowFirstDatePriceText.Text.Replace("###LOGINNAME###", dr("LoginName"))
    '                End If

    '            Catch ex As Exception
    '                WebErrorMessageBox(Me, ex, "")
    '            End Try
    '        Next

    '    Catch ex As Exception
    '        WebErrorMessageBox(Me, ex, "")
    '    End Try
    'End Sub
End Class
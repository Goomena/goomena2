﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucCredits.ascx.vb" Inherits="Dating.Server.Site.Web.ucCredits" %>
<%@ Register Src="~/UserControls/gifts/ucRewardGiftsCards.ascx" TagPrefix="uc1" TagName="ucRewardGiftsCards" %>
<%@ Register Src="~/UserControls/gifts/ucRewardGiftsAvaliable.ascx" TagPrefix="uc1" TagName="ucRewardGiftsAvaliable" %>
<%--<link href="/v1/css/gifts.css?v103" type="text/css" rel="Stylesheet" />--%>

<script type="text/javascript">
    var lnkGetCreditsID = '<%= lnkGetCredits.ClientID%>';
    var lnkGetCreditsText = '';


</script>

<div id="SelectGiftCardContainer">
    <asp:MultiView ID="GiftsView" runat="server" ActiveViewIndex="0">
        <asp:View ID="vwEmpty" runat="server">
        </asp:View>
        <asp:View ID="vwAvaliableGiftCards" runat="server">
            <div class="crtlSelectGiftCard">
                <uc1:ucRewardGiftsAvaliable runat="server" ID="ucRewardGiftsAvaliable" Visible="false" />
                <script type="text/javascript">
                    var Inbtn = false;
                    var inPop = false;
                    var InSpan = false;//btnSelectText
                    $('body').on("mouseover", "#SelectGiftCardContainer .btnSelectGiftCard.Disabled", function () {
                        //  $(".SmallPopupNotEnoughCreditsContainer:not(:hover)").hide();
                        Inbtn = true;
                        $(this).siblings(".SmallPopupNotEnoughCreditsContainer").fadeIn(200);
                    });
                    $('body').on("mouseleave", "#SelectGiftCardContainer .btnSelectGiftCard.Disabled", function () {
                        Inbtn = false;
                        setTimeout(function () {
                            if (Inbtn == false && inPop == false && InSpan == false) {
                                $(".SmallPopupNotEnoughCreditsContainer").fadeOut(100)
                            }
                        }, 20);
                    });
                    $('body').on("mouseover", "#SelectGiftCardContainer .SmallPopupNotEnoughCreditsContainer", function () {
                        inPop = true;
                    });
                    $('body').on("mouseleave", "#SelectGiftCardContainer .SmallPopupNotEnoughCreditsContainer", function () {
                        inPop = false;
                        setTimeout(function () {
                            if (Inbtn == false && inPop == false && InSpan == false) {
                                $(".SmallPopupNotEnoughCreditsContainer").fadeOut(100)
                            }
                        }, 20);
                    });
                    $('body').on("mouseover", "#SelectGiftCardContainer .btnSelectText", function () {
                        InSpan = true;
                    });
                    $('body').on("mouseleave", "#SelectGiftCardContainer .btnSelectText", function () {
                        InSpan = false;
                        setTimeout(function () {
                            if (Inbtn == false && inPop == false && InSpan == false) {
                                $(".SmallPopupNotEnoughCreditsContainer").fadeOut(100)
                            }
                        }, 20);
                    });
                </script>
            </div>
        </asp:View>
        <asp:View ID="vwSelectedGiftCards" runat="server">
            <div class="crtlSelectGiftCard">
                <uc1:ucRewardGiftsCards runat="server" ID="ucRewardGiftsCards" Visible="false" />
                <script type="text/javascript">
                    $('body').on("click", "#SelectGiftCardContainer .ReadMoreLink", function () {
                        $(".Bigtext").hide();
                        $(this).siblings(".Bigtext").fadeIn(200)
                    });
                    $('body').on("click", "#SelectGiftCardContainer .ClosebtnBigText", function () {
                        $(".Bigtext").fadeOut(100)
                    });

                </script>
            </div>
        </asp:View>
    </asp:MultiView>

</div>

<asp:Panel ID="pnlTopHeader" CssClass="gifts-credits-Container" runat="server" Visible="true">
    <div class="CouponTitle">
        <asp:Literal ID="lblTitle" runat="server" Text="Points Page Title"></asp:Literal>
        <asp:LinkButton ID="lnkViewDescription" runat="server" CssClass="more_info2" OnClientClick="OnMoreInfoClick('[contentUrl]', this, '[headerText]');return false;" />
    </div>
    <div class="TopTextContainer">
        <asp:Literal ID="lblTopText" runat="server" Text="Points Page Title"></asp:Literal>
    </div>
</asp:Panel>

<div class="gifts-credits-page">
    <div class="gifts-credits-content">
        <asp:Panel ID="pnlCreditsHeader" runat="server" CssClass="header-box">
            <asp:Literal ID="lblHeader" runat="server">
            </asp:Literal>
        </asp:Panel>

        <asp:Panel ID="pnlCreditsAnalysis" CssClass="pnlCreditsAnalysis" runat="server">
            <%--  <div class="your-Friends">
        
          <asp:Literal ID="lblYourFriends" runat="server">
        </asp:Literal>
    </div>--%>
            <div class="your-credits">

                <asp:Literal ID="lblYourCredits" runat="server">
                </asp:Literal>
            </div>
            <div class="credits-analysis OuterBox">


                <div class="credits-analysis InnerBox">
                    <div class="top-part">

                        <div class="details v2">
                            <div class="lfloat">
                                <div class="InnerPoints">
                                    <div class="FreeCountContainer">
                                        <div class="InnerFreeCountContainer">
                                            <asp:Label ID="lblFree" CssClass="lblFreeTop" runat="server" Text="Points" />
                                            <asp:Label ID="lblFreeCount" CssClass="lblFreeCountTop" runat="server" Text="121" />
                                            <asp:Label ID="lblPoints" CssClass="lblPoints" runat="server" Text="POINTS" />
                                        </div>
                                    </div>
                                    <div class="bottom-part">

                                        <div class="action-link <%= MyBase.CreditsDisabledCssClass %>">
                                            <asp:LinkButton ID="lnkGetCredits" runat="server">ΕΞΑΡΓΥΡΩΣΗ</asp:LinkButton>
                                            <asp:Label ID="lnkGetPointsNotEnouph" CssClass="lnkGetPointsNotEnouph" runat="server">ΕΞΑΡΓΥΡΩΣΗ</asp:Label>
                                            <%--  <asp:Label ID="lnkNotEnoughCredits" CssClass="lnkNotEnoughCredits" runat="server">Not enough credits</asp:Label>--%>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="lfloat v2">
                                <div class="InnerAnalysis">
                                    <div class="lblAnalysis-Title">
                                        <asp:Literal ID="lblAnalysisTitle" runat="server" Text="Αναλυτικά"></asp:Literal>
                                    </div>
                                    <div class="Analysis">

                                        <div class="free">
                                            <asp:Label ID="lblFree2" CssClass="lblLabel" runat="server" Text="ΕΛΕΥΘΕΡΑ" />
                                            <div class="ResultsBox">


                                                <asp:Label ID="lblFreeCount2" CssClass="lblValue" runat="server" Text="121" />
                                                <asp:Image ID="imgHelpFree" runat="server" ImageUrl="//cdn.goomena.com/images2/gifts/q.png" ToolTip="" CssClass="quest-small" />
                                            </div>
                                        </div>
                                        <div class="Reserved">
                                            <asp:Label ID="lblReserved" CssClass="lblLabel" runat="server" Text="ΔΕΣΜΕΥΜΕΝΑ" />
                                            <div class="ResultsBox">
                                                <asp:Label ID="lblReservedCount" CssClass="lblValue" runat="server" Text="121" />
                                                <asp:Image ID="imgHelpReserved" runat="server" ImageUrl="//cdn.goomena.com/images2/gifts/q.png" ToolTip="" CssClass="quest-small" />
                                            </div>
                                        </div>
                                        <div class="Total">
                                            <asp:Label ID="lblTotal" CssClass="lblLabel" runat="server" Text="Συνολικά Credits" />
                                            <div class="ResultsBox">
                                                <asp:Label ID="lblTotalCount" CssClass="lblValue" runat="server" Text="123" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="BookShadow"></div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="credits-analysis <%= MyBase.FriendsCountCssClass %> v2">
                <asp:HyperLink ID="lnkPhoto" NavigateUrl="~/Members/Photos.aspx" runat="server" CssClass="profile-picture2" onClick="ShowLoading();">
                    <asp:Image ID="imgPhoto" runat="server" ImageUrl="~/Images/guy.jpg" CssClass="round-img-78" />
                </asp:HyperLink>
                <div class="lfloat left-box">
                    <div class="profile ">
                        <div class="lfloat PhotoContainer">

                            <div class="Name">
                                <asp:Label ID="ltrLogin" runat="server" CssClass="login-name" Text="sdfasdfas" />
                            </div>
                        </div>
                        <div class="lfloat Details">

                            <div class="RefCodeContainer">
                                <asp:Label ID="lblRefCodeLabel" runat="server" CssClass="Ref-CodeTitle" Text="Your Code" />
                                <asp:Label ID="lblRefCode" runat="server" CssClass="Ref-Code" Text="RefCode" />
                                <%-- <asp:Image ID="imgHelpRefCode" runat="server" Visible="false"  ImageUrl="//cdn.goomena.com/images2/gifts/help-icon-small.png" ToolTip="" CssClass="quest-small" />--%>
                                <asp:Label ID="lblRefCodeHelp" runat="server" CssClass="Ref-CodeHelp" Text="Αυτόν τον κωδικό δίνεις στις φίλες σου." />
                            </div>

                        </div>
                        <div class="clear"></div>

                    </div>

                    <%-- <asp:Literal ID="lblFriendsDescr" runat="server" Text="" >
            </asp:Literal>--%>
                    <asp:Literal ID="liFriendsDescr" runat="server"></asp:Literal>
                </div>
                <div class="lfloat center-box">
                </div>
                <div class="lfloat right-box">

                    <asp:MultiView ID="mvFriends" runat="server" ActiveViewIndex="0">

                        <asp:View ID="vwNoFriends" runat="server">
                            <div class="friends-list-empty">
                                <asp:Literal ID="linoFriendsDescr" runat="server"></asp:Literal>
                                <%--    <div class="action-link">
                        <asp:HyperLink ID="lnkCall1" Visible="false"  runat="server">ΠΡΟΣΚΑΛΕΣΕ</asp:HyperLink>
                    </div>--%>
                            </div>
                        </asp:View>

                        <asp:View ID="vwFriends" runat="server">
                            <div class="friends-list ">

                                <asp:Panel ID="pnlFriend1" runat="server" class="one-box">
                                    <div class="lfloat">
                                        <div class="pr-photo-43-noshad">
                                            <asp:Label ID="lnkFriend1" runat="server" CssClass="profile-picture">
                                                <asp:Image ID="lnkFriend1Img" runat="server" ImageUrl="~/Images/guy.jpg" CssClass="round-img-43" />
                                            </asp:Label>
                                        </div>
                                        <table cellpadding="0" cellspacing="0" class="login-wrap">
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lnkFriend1Login" runat="server" CssClass="login-name" Text="" /></td>
                                            </tr>
                                        </table>

                                    </div>
                                    <div class="rfloat">
                                        <asp:Label ID="lnkFriend1Count" runat="server" CssClass="" Text="45" />
                                    </div>
                                    <div class="clear"></div>

                                </asp:Panel>

                                <asp:Panel ID="pnlFriend2" runat="server" class="two-box">
                                    <div class="lfloat">
                                        <div class="pr-photo-43-noshad">
                                            <asp:Label ID="lnkFriend2" runat="server" CssClass="profile-picture">
                                                <asp:Image ID="lnkFriend2Img" runat="server" ImageUrl="~/Images/guy.jpg" CssClass="round-img-43" />
                                            </asp:Label>
                                        </div>
                                        <table cellpadding="0" cellspacing="0" class="login-wrap">
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lnkFriend2Login" runat="server" CssClass="login-name" Text="" /></td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="rfloat">
                                        <asp:Label ID="lnkFriend2Count" runat="server" CssClass="" Text="45" />
                                    </div>
                                    <div class="clear"></div>
                                </asp:Panel>

                                <asp:Panel ID="pnlFriend3" runat="server" class="three-box">
                                    <div class="lfloat">
                                        <div class="pr-photo-43-noshad">
                                            <asp:Label ID="lnkFriend3" runat="server" CssClass="profile-picture">
                                                <asp:Image ID="lnkFriend3Img" runat="server" ImageUrl="~/Images/guy.jpg" CssClass="round-img-43" />
                                            </asp:Label>
                                        </div>
                                        <table cellpadding="0" cellspacing="0" class="login-wrap">
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lnkFriend3Login" runat="server" CssClass="login-name" Text="" /></td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="rfloat">
                                        <asp:Label ID="lnkFriend3Count" runat="server" CssClass="" Text="45" />
                                    </div>
                                    <div class="clear"></div>
                                </asp:Panel>



                                <%--                <div class="last-box">
                    <div class="action-link">
                        <asp:HyperLink ID="lnkCall" runat="server">ΠΡΟΣΚΑΛΕΣΕ</asp:HyperLink>
                        <img src="http://localhost:61234/cdn.goomena.com/images2/gifts/help-icon-small.png" alt="" class="quest-small hidden" />
                    </div>
                    <div class="rfloat"></div>
                    <div class="clear"></div>
                </div>--%>
                            </div>
                            <asp:Literal ID="lioneFriendsDescr" runat="server"></asp:Literal>
                        </asp:View>

                    </asp:MultiView>
                    <asp:Panel ID="pnlSeeMore" runat="server" Visible="false">
                        <script type="text/javascript">
                            function showpopup() {
                                var popup = window["popMoreFriends"];

                                popup.Show();
                                $(function () {
                                    var b = $('.rptContainerMoreFriends').hasScrollBar();
                                    if (b==true){
                                        $('.rptContainerMoreFriends').addClass('scroll');
                                    }
                                   

                                });
                            }
                            function closep() {
                                var popup = window["popMoreFriends"];

                                popup.Hide();
                            }
                            (function ($) {
                                $.fn.hasScrollBar = function () {
                                    return this.get(0).scrollHeight > this.height();
                                }
                            })(jQuery);
                         
                        </script>
                        <div class="more-box">
                            <div class="moreFriends">
                                <asp:HyperLink ID="lnkMore" runat="server" CssClass="darker" NavigateUrl="javascript:showpopup();"></asp:HyperLink>
                            </div>
                            <div class="rfloat"></div>
                            <div class="clear"></div>
                        </div>
                        <dx:ASPxPopupControl CssClass="popMoreFriends" ClientInstanceName="popMoreFriends" Width="470px" Height="570px"
                    MaxWidth="470px" MaxHeight="570px" ShowCloseButton="true" MinHeight="300px" MinWidth="300px" ID="popMoreFriends"
                    ShowFooter="false" PopupElementID="imgButton"  ShowShadow="false" Border-BorderStyle="None" HeaderText=""
                    PopupVerticalAlign="WindowCenter"
                    PopupHorizontalAlign="WindowCenter"
                    Modal="True"
                    runat="server" EnableViewState="false" EnableHierarchyRecreation="True" ShowHeader="false" ShowLoadingPanel="False" CloseAction="CloseButton">
                                        <ContentCollection>
                        <dx:PopupControlContentControl runat="server">
                              <asp:Panel ID="pnlMoreFriends" CssClass="pnlMoreFriends" runat="server">
                            <a class="popUpClosebtn" onclick="closep();"><img id="tnClose" src="//cdn.goomena.com/Images2/popup-wrn/close.png" alt="[Close]" style="height:43px;width:43px;"></a>
                     
                                <div class="MoreFriendsHeader">
                                    <div class="lfloat">
                                        <asp:Label ID="lblMoreFriendsHeader" runat="server" Text="Friends"></asp:Label>
                                    </div>
                                    <div class="rfloat">
                                        <asp:Label ID="lblTotalMoreFriends" runat="server" CssClass="" Text="" />
                                    </div>
                                    <div class="clear"></div>
                                </div>

                                <div class="rptContainerMoreFriends">
                                    <asp:Repeater ID="rptMoreFriends" runat="server">
                                        <ItemTemplate>
                                            <div class="FriendContainer">
                                                <div class="lfloat">
                                                    <div class="lfloat">
                                                        <div class="Outer">
                                                            <div class="middle">
                                                                <div class="pr-photo-43-noshad">
                                                                    <asp:Label ID="imgFriendContainer" runat="server" CssClass="profile-picture">
                                                                        <asp:Image ID="imgFriend" runat="server" ImageUrl='<%# GetProfilePhotoUrl(Eval("ProfileID"), Dating.Server.Core.DLL.PhotoSize.D150)%>' CssClass="round-img-43" />
                                                                    </asp:Label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="lfloat">
                                                        <div class="Outer">
                                                            <div class="middle">
                                                                <asp:Label ID="lblLogin" runat="server" CssClass="login-name" Text='<%# Eval("LoginName")%>' />
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="clear"></div>
                                                </div>
                                                <div class="rfloat">
                                                    <div class="Outer">
                                                        <div class="middle">
                                                            <asp:Label ID="lnkFriend2Count" runat="server" CssClass="" Text='<%# Math.Floor(Dating.Server.Core.DLL.clsNullable.DBNullToDouble(Eval("CommissionCredits"))).ToString("0")%>' />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                            
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>
                               </asp:Panel>
                          
                        </dx:PopupControlContentControl>
                    </ContentCollection>
                    <ClientSideEvents />

                </dx:ASPxPopupControl>
                    </asp:Panel>
                </div>
                <div class="clear"></div>
                     
                
                        











            </div>

            <asp:Panel ID="pnlMyCoupons" runat="server">

                <div class="my-coupons">
                    <div class="lfloat lcorner"></div>
                    <div class="lfloat action-link">
                        <asp:HyperLink ID="lnkMyCoupons" runat="server" CssClass="darker" NavigateUrl="~/Members/CouponsList.aspx">ΤΑ ΚΟΥΠΟΝΙΑ ΜΟΥ</asp:HyperLink>
                    </div>
                    <div class="rfloat rcorner"></div>
                    <div class="clear"></div>
                </div>
            </asp:Panel>
        </asp:Panel>

        <div class="logos">
            <img class="quest hidden" alt="" src="//cdn.goomena.com/images2/gifts/help-icon.png" />
            <asp:Literal ID="lblLogos" runat="server">
            </asp:Literal>
        </div>

    </div>
</div>
<dx:ASPxPopupControl ID="popupHelp" runat="server"
    CloseAction="MouseOut"
    PopupAction="MouseOver"
    PopupVerticalAlign="NotSet"
    ShowHeader="False"
    Width="300px"
    CssClass="tooltip_Popup"
    ForeColor="White"
    BackColor="Black">
    <Windows>
        <dx:PopupWindow PopupElementID="imgHelpFree">
            <ContentCollection>
                <dx:PopupControlContentControl runat="server" SupportsDisabledAttribute="True">
                    <asp:Label ID="lblFreeCreditsPopup" runat="server" Text="" ForeColor="White"></asp:Label>
                </dx:PopupControlContentControl>
            </ContentCollection>
        </dx:PopupWindow>
        <dx:PopupWindow PopupElementID="imgHelpReserved">
            <ContentCollection>
                <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server" SupportsDisabledAttribute="True">
                    <asp:Label ID="lblReservedCreditsPopup" runat="server" Text="" ForeColor="White"></asp:Label>
                </dx:PopupControlContentControl>
            </ContentCollection>
        </dx:PopupWindow>
        <dx:PopupWindow PopupElementID="imgHelpRefCode">
            <ContentCollection>
                <dx:PopupControlContentControl ID="PopupControlContentControl3" runat="server" SupportsDisabledAttribute="True">
                    <asp:Label ID="lblRefCodePopup" runat="server" Text="" ForeColor="White"></asp:Label>
                </dx:PopupControlContentControl>
            </ContentCollection>
        </dx:PopupWindow>

    </Windows>
    <ContentStyle>
        <Paddings Padding="10px" />
    </ContentStyle>
    <HeaderStyle>
        <Paddings Padding="10px" />
    </HeaderStyle>
    <FooterStyle>
        <Paddings Padding="10px" />
    </FooterStyle>
    <ContentCollection>
        <dx:PopupControlContentControl runat="server" SupportsDisabledAttribute="True">
        </dx:PopupControlContentControl>
    </ContentCollection>
</dx:ASPxPopupControl>

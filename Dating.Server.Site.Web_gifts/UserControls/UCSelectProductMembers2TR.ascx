﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UCSelectProductMembers2TR.ascx.vb" Inherits="Dating.Server.Site.Web.UCSelectProductMembers2TR" %>
<%@ Register src="UCPrice2.ascx" tagname="UCPrice" tagprefix="uc1" %>

<style type="text/css">
    .vat-info { font-size:12px;font-weight:bold;margin:10px 0;
                border-bottom: 1px solid #bababc;
                padding-bottom: 3px;}
#payment_2 { font-family: 'Segoe UI' , 'Arial'; }
#payment_2 .top-wrap { }
#payment_2 .top-wrap h3 { font-size: 19px; font-weight: normal; margin-bottom: 10px; }
#payment_2 .top-wrap h4 { font-size: 15px; font-weight: normal; margin-top: 0px; }
#payment_2 .top-wrap div#title_one {display: block;background: url('//cdn.goomena.com/Images2/payment/1.png') no-repeat scroll 0 0;width: 157px;height: 48px;font-size: 13px;font-weight: bold;overflow: hidden;padding-left: 17px;padding-top: 10px;padding-right: 10px;}
#payment_2 .top-wrap div#title_two {display: block;background: url('//cdn.goomena.com/Images2/payment/2.png') no-repeat scroll 0 0;width: 157px;height: 53px;font-size: 13px;font-weight: bold;overflow: hidden;padding-left: 17px;padding-top: 10px;padding-right: 10px;}
#payment_2 .top-wrap div#title_three {display:block; background: url('//cdn.goomena.com/Images2/payment/3.png') no-repeat scroll 0 0;width: 184px;height: 53px;font-size: 13px;font-weight: bold;overflow: hidden;padding-left: 17px;padding-top: 10px;padding-right: 10px;}
#payment_2 .top-wrap .img-text-3 { position: relative; height: 150px; }
#payment_2 .top-wrap .text-31 { width: 186px; text-align: left; position: absolute; left: 0px;}
#payment_2 .top-wrap .text-32 { width: 186px; text-align: left; position: absolute; left: 219px; }
#payment_2 .top-wrap .text-33 { width: 211px; text-align: left; position: absolute; left: 433px; }
#payment_2 .top-wrap .img-text-3 p { font-size: 13px; }
#payment_2 .top-wrap .img-text-3 p { font-size: 13px; }
    
#payment_2 .wrap-2 { position: relative; width: 663px; }
#payment_2 .wrap-2 .blue-credits { font-size: 18px; color: #33b5e5; font-weight: normal; }
#payment_2 .wrap-2 .geotrust { position: absolute; right: 0px; top: 0px; }
    
#payment_2 #products_2 { position: relative; /*background-image: url('//cdn.goomena.com/Images2/payment/credits-background.png'); background-repeat: no-repeat; width: 663px;*/ height: 350px; }
#payment_2 #products_2 .product-1 {background: url('//cdn.goomena.com/Images2/payment/1000-box.png') no-repeat scroll 0 0; position: absolute; width: 217px; height: 340px; left: 0px; top:9px;}
#payment_2 #products_2 .product-2 {background: url('//cdn.goomena.com/Images2/payment/3000-box.png') no-repeat scroll 0 0; position: absolute; width: 217px; height: 340px; left: 221px; top:9px;}
#payment_2 #products_2 .product-3 {background: url('//cdn.goomena.com/Images2/payment/6000-box.png') no-repeat scroll 0 0; position: absolute; width: 217px; height: 340px; left: 442px; top:0px;}
#payment_2 #products_2 .product-1:hover{background: url('//cdn.goomena.com/Images2/payment/1000-box-hover.png') no-repeat scroll 0 0; }
#payment_2 #products_2 .product-2:hover{background: url('//cdn.goomena.com/Images2/payment/3000-box-hover.png') no-repeat scroll 0 0;}
#payment_2 #products_2 .product-3:hover{background: url('//cdn.goomena.com/Images2/payment/6000-box-hover.png') no-repeat scroll 0 0;}
    
#payment_2 #products_2 .pricecol { position: absolute; top: 20px; left: 15px; background-repeat: no-repeat; width: 85px; height: 85px; }
#payment_2 #products_2 .product-1 .pricecol { }
#payment_2 #products_2 .product-2 .pricecol {}
#payment_2 #products_2 .product-3 .pricecol {}
    
#payment_2 #products_2 .pricecol-credits { position: absolute; top: 20px; left: 0; right: 0; font-size: 42px; color: #fff;text-align: center; }
#payment_2 #products_2 .pricecol-credits-text { position: absolute; top: 42px; left: 0;right:0;  font-size: 27px; color: #fff; text-align: center; }
#payment_2 #products_2 .product-3  .pricecol-credits { top: 29px;  }
#payment_2 #products_2 .product-3  .pricecol-credits-text { top: 45px;  }

#payment_2 #products_2 .pricecol-price { position: absolute; top: 145px; left: 0;right:0; font-size: 54px; color: #000; text-align: center;}
#payment_2 #products_2 .product-3 .pricecol-price { top: 154px;}
    
    
#payment_2 #products_2 .pricecol-expire-wrap { position: absolute; top: 51px; left: 0;right:0; font-size: 14px; color: #000; background: url('//cdn.goomena.com/Images2/payment/check-cycle.png') no-repeat scroll 0 50%; padding-left: 20px; vertical-align: middle; display:none;}
#payment_2 #products_2 .product-3 .pricecol-expire-wrap { top: 58px;}
#payment_2 #products_2 .pricecol-expire { position: absolute; top: 152px; left: 30px; font-size: 14px; color: #000; background: url('//cdn.goomena.com/Images2/payment/check-cycle.png') no-repeat scroll 0 50%; padding-left: 20px; vertical-align: middle; }
#payment_2 #products_2 .pricecol-expire-line2 { position: absolute; top: 176px; left: 30px; font-size: 14px; color: #000; background: url('//cdn.goomena.com/Images2/payment/check-cycle.png') no-repeat scroll 0 50%; padding-left: 20px; vertical-align: middle; }
#payment_2 #products_2 .pricecol-expire-line3 { position: absolute; top: 200px; left: 30px; font-size: 14px; color: #000; background: url('//cdn.goomena.com/Images2/payment/check-cycle.png') no-repeat scroll 0 50%; padding-left: 20px; vertical-align: middle; }
#payment_2 #products_2 .pricecol-order-btn { color: #fff; text-transform: uppercase; font-size: 15px; text-align: center; line-height: 37px; 
                                            border-width: 0px; width: 129px; height: 35px; position: absolute; bottom: 39px; left: 44px; 
                                            display: inline-block; 
                                            background: url('//cdn.goomena.com/Images2/payment/order-button.png') no-repeat scroll 100% 100%; }
#payment_2 #products_2 .pricecol-order-btn:hover {background-position:0 -7%;}
#payment_2 #products_2 .product-3 .pricecol-order-btn {bottom: 30px;}
#payment_2 .wrap-3 { margin-top: 40px; width: 663px; margin-left: auto; margin-right: auto; }
#payment_2 .wrap-3 .rightdiv { text-align: center; }
#payment_2 .wrap-3 .rightdiv p { line-height: 20px; font-size: 14px; }
#payment_2 .wrap-3 .oikonomikadiv {margin-top: 40px; margin-left: auto; margin-right: auto; width: 663px; height: 115px; background: url('//cdn.goomena.com/Images2/payment/symferei.png') no-repeat scroll 0 0; }
#payment_2 .wrap-3 .oikonomikadiv p {padding-top: 10px;margin-left: 25px;}
#payment_2 .wrap-3 p.note-2 { font-size: 13px; margin-top: 50px; }
#payment_2 .wrap-3 .gifts { margin-top: 50px; display: none; }
#payment_2 .wrap-3 p.note { padding-top: 25px; border-top: 1px solid #bababc; margin-top: 60px; font-size: 10px; font-family: "Segoe UI" , "Arial"; }
#payment_2 .paymentsimgs {margin:15px auto 0;width:663px;text-align:center;}
    
#payment_2 #products_2 .pricecol-price.small {position: absolute; top: 76px; left: 0;right:20px; font-size: 32px; color: #000;text-align:right;}
#payment_2 #products_2 .product-tab {display:block;cursor:pointer;}
#payment_2 #products_2 .product-1.tr .pricecol-price {top: 152px;background: url('//cdn.goomena.com/Images2/payment/42-1.png') no-repeat scroll 50% 50%;height:56px;}
#payment_2 #products_2 .product-2.tr .pricecol-price {top: 152px;background: url('//cdn.goomena.com/Images2/payment/100-1.png') no-repeat scroll 50% 50%;height:57px;}
#payment_2 #products_2 .product-3.tr .pricecol-price {top: 159px;background: url('//cdn.goomena.com/Images2/payment/180-1.png') no-repeat scroll 50% 50%;height:57px;}
#payment_2 #products_2 .tr .pricecol-price .pricecol-price-text{display:none;}
    
#payment_2.tr .top-wrap .img-text-3{display:none;height: 140px;background: url('//cdn.goomena.com/Images2/payment/banner-tourk-product-page.png') no-repeat scroll 50% 50%;}
    
/*
#payment_2.tr .top-wrap .img-text-3{height: 20px;}
#payment_2.tr #products_2 .pricecol-price.small{position: absolute; top: 113px; left: 0;right:0; font-size: 32px; color: #000;text-align:center;}
#payment_2.tr #products_2 .product-3 .pricecol-price.small{position: absolute; top: 120px; left: 0;right:0; font-size: 32px; color: #000;text-align:center;}
*/
</style>
<script type="text/javascript">
    $(function () {
        $('.product-1.product-tab', '#payment_2 #products_2').click(function () {
            $('.pricecol-order-btn', '#payment_2 #products_2 .product-1')[0].click();
        })
        $('.product-2.product-tab', '#payment_2 #products_2').click(function () {
            $('.pricecol-order-btn', '#payment_2 #products_2 .product-2')[0].click();
        })
        $('.product-3.product-tab', '#payment_2 #products_2').click(function () {
            $('.pricecol-order-btn', '#payment_2 #products_2 .product-3')[0].click();
        })
        setTimeout(function () {
            new Image().src = '<%= ResolveUrl("~/Images2/payment/1000-box-hover.png") %>';
            new Image().src = '<%= ResolveUrl("~/Images2/payment/3000-box-hover.png") %>';
            new Image().src = '<%= ResolveUrl("~/Images2/payment/6000-box-hover.png") %>';
        }, 100);
    })
</script>
<div id="payment_2" runat="server" clientidmode="Static">
    <div>
        <asp:Label ID="lblPaymentsGuarantee" runat="server" Text=""></asp:Label>
    </div>
    <asp:Panel ID="pnlBonus" runat="server" CssClass="bonus-code">
        <div align="center">
            <asp:Label ID="lblBonusCode" runat="server" Text="" style="color:#25AAE1;"/>
            <asp:Label ID="lblBonusCodeSuccess" runat="server" ForeColor="Green" 
                ViewStateMode="Disabled"/>
            <div style="height:20px"></div>
            <asp:TextBox ID="txtBonusCode" runat="server" CssClass="textbox"></asp:TextBox>
            <asp:Button ID="btnBonusCredits" runat="server" Text="Υποβολή κωδικού" CssClass="button" />
            <div style="height:20px"></div>
            <asp:Label ID="lblBonusCodeError" runat="server" Text="" ForeColor="red" 
                ViewStateMode="Disabled"/>
        </div>
    </asp:Panel>


    <div id="products_2">
        <uc1:UCPrice ID="UCPrice1" runat="server" View="vwMember" CssClass="product-1"  />
        <uc1:UCPrice ID="UCPrice2" runat="server" View="vwMember" CssClass="product-2" Discount="30"  />
        <uc1:UCPrice ID="UCPrice3" runat="server" View="vwMember" CssClass="product-3" IsRecommended="true" Discount="45"   />
    </div>

    <div class="paymentsimgs">
        <img ID="imgPaymentSigns" runat="server" src="//cdn.goomena.com/Images2/payment/payment-logos.png" alt="Logos" />
    </div>
    
    <asp:Literal runat="server" ID="lbfootText">
    </asp:Literal>

</div>

﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="DateControl.ascx.vb" Inherits="Dating.Server.Site.Web.DateControl" %>
<table cellpadding="0" cellspacing="0" class="date-table">
<tr>
<td><div class="day-wrap date-part"><dx:ASPxComboBox ID="ddlday" runat="server" EncodeHtml="False" Width="100px" ClientInstanceName="Date_DayDDL">
<Items>
    <dx:ListEditItem Text="Day" Value="" />
    <dx:ListEditItem Text="1" Value="1" />
    <dx:ListEditItem Text="2" Value="2" />
    <dx:ListEditItem Text="3" Value="3" />
    <dx:ListEditItem Text="4" Value="4" />
    <dx:ListEditItem Text="5" Value="5" />
    <dx:ListEditItem Text="6" Value="6" />
    <dx:ListEditItem Text="7" Value="7" />
    <dx:ListEditItem Text="8" Value="8" />
    <dx:ListEditItem Text="9" Value="9" />
    <dx:ListEditItem Text="10" Value="10" />
    <dx:ListEditItem Text="11" Value="11" />
    <dx:ListEditItem Text="12" Value="12" />
    <dx:ListEditItem Text="13" Value="13" />
    <dx:ListEditItem Text="14" Value="14" />
    <dx:ListEditItem Text="15" Value="15" />
    <dx:ListEditItem Text="16" Value="16" />
    <dx:ListEditItem Text="17" Value="17" />
    <dx:ListEditItem Text="18" Value="18" />
    <dx:ListEditItem Text="19" Value="19" />
    <dx:ListEditItem Text="20" Value="20" />
    <dx:ListEditItem Text="21" Value="21" />
    <dx:ListEditItem Text="22" Value="22" />
    <dx:ListEditItem Text="23" Value="23" />
    <dx:ListEditItem Text="24" Value="24" />
    <dx:ListEditItem Text="25" Value="25" />
    <dx:ListEditItem Text="26" Value="26" />
    <dx:ListEditItem Text="27" Value="27" />
    <dx:ListEditItem Text="28" Value="28" />
    <dx:ListEditItem Text="29" Value="29" />
    <dx:ListEditItem Text="30" Value="30" />
    <dx:ListEditItem Text="31" Value="31" />
</Items>
</dx:ASPxComboBox></div></td>
<td>&nbsp;</td>
<td><div class="month-wrap date-part"><dx:ASPxComboBox ID="ddlmonth" runat="server" EncodeHtml="False" Width="100px" ClientInstanceName="Date_MonthDDL">
<Items>
    <dx:ListEditItem Text="Month" Value="" />
    <dx:ListEditItem Text="1" Value="1" />
    <dx:ListEditItem Text="2" Value="2" />
    <dx:ListEditItem Text="3" Value="3" />
    <dx:ListEditItem Text="4" Value="4" />
    <dx:ListEditItem Text="5" Value="5" />
    <dx:ListEditItem Text="6" Value="6" />
    <dx:ListEditItem Text="7" Value="7" />
    <dx:ListEditItem Text="8" Value="8" />
    <dx:ListEditItem Text="9" Value="9" />
    <dx:ListEditItem Text="10" Value="10" />
    <dx:ListEditItem Text="11" Value="11" />
    <dx:ListEditItem Text="12" Value="12" />
</Items>
</dx:ASPxComboBox></div></td>
<td>&nbsp;</td>
<td><div class="year-wrap date-part"><dx:ASPxComboBox ID="ddlyear" runat="server" EncodeHtml="False" Width="100px" ClientInstanceName="Date_YearDDL">
<Items>
    <dx:ListEditItem Text="Year" Value="" />
</Items>
</dx:ASPxComboBox></div></td>
    </tr>
</table>

<script type="text/javascript">
    function dcGetSelectedDate() {
        var dt = null;
        try {
            var year = window["<%= ddlyear.ClientInstanceName%>"].GetValue();
            if (year == null || year == '') {
                return null;
            }
            var month = window["<%= ddlmonth.ClientInstanceName%>"].GetValue();
            if (month == null || month == '') {
                return null;
            }
            var day = window["<%= ddlday.ClientInstanceName%>"].GetValue();
            if (day == null || day == '') {
                return null;
            }
            try { year = parseInt(year); } catch (e) { }
            try { month = parseInt(month); } catch (e) { }
            try { day = parseInt(day); } catch (e) { }
            month = month - 1;
            dt = new Date(year, month, day);
        } catch (e) { }

        return dt;
    }
</script>

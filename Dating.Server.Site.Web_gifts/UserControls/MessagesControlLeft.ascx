﻿<%@ Control Language="vb" AutoEventWireup="false" 
    CodeBehind="MessagesControlLeft.ascx.vb" 
    Inherits="Dating.Server.Site.Web.MessagesControlLeft" %>

<asp:Repeater ID="rptNew" runat="server">
<SeparatorTemplate>
    <div class="qmsl_item-separator"></div>
</SeparatorTemplate>
<ItemTemplate>
<div class="qmsl_item" login="name-<%# System.Web.HttpUtility.UrlEncode(Eval("OtherMemberLoginName").ToString()) %>" onclick="jsLoad('<%# Eval("OtherMemberProfileViewUrl") %>');">
    <div class="photo">
        <asp:HyperLink ID="hl2" runat="server" NavigateUrl='<%# Eval("OtherMemberProfileViewUrl") %>' onclick="ShowLoading();"><img src="<%# Eval("OtherMemberImageUrl") %>" class="round-img-48" /></asp:HyperLink>
    </div>
    <div class="info">
        <h2 class="lfloat"><asp:HyperLink ID="lnk2" runat="server" NavigateUrl='<%# Eval("OtherMemberProfileViewUrl")%>' onclick="ShowLoading();" CssClass="login-name"><%# Eval("OtherMemberLoginName")%></asp:HyperLink></h2>
        <asp:Label ID="lblCount" runat="server" 
            Text='<%# MyBase.GetCountMessagesString(Eval("ItemsCount")) %>' 
            Visible='<%# MyBase.IsVisible(Eval("ItemsCount")) %>' 
            CssClass="msg-count lfloat"/>
        <div class="clear"></div>
        <%# MyBase.Write_MemberInfo(Container.DataItem)%>
    </div>
    <div class="clear"></div>
    <asp:HiddenField ID="hdfProfileID" runat="server" Value='<%# Eval("OtherMemberProfileID") %>' Visible="False" />
    <asp:HiddenField ID="hdfMessageRead" runat="server" Value='<%# Eval("Read") %>' Visible="False" />
</div>
</ItemTemplate>
</asp:Repeater>





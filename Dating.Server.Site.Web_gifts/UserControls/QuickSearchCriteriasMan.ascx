﻿<%@ Control Language="vb" AutoEventWireup="false" 
    CodeBehind="QuickSearchCriteriasMan.ascx.vb" 
    Inherits="Dating.Server.Site.Web.QuickSearchCriteriasMan" %>
<%@ Register src="~/UserControls/AgeDropDown.ascx" tagname="AgeDropDown" tagprefix="uc1" %>
<%@ Register src="~/UserControls/DistanceDropDown.ascx" tagname="DistanceDropDown" tagprefix="uc4" %>

<div id="quick-search-form" class="outter-box">
    <div id="search-top-row">
        <asp:Panel ID="pnlQUserName" runat="server" DefaultButton="imgSearchByUserName" CssClass="input-user">
            <%--<div class="input-title"><asp:Label ID="msg_UserNameSearchQ" runat="server" Text="" AssociatedControlID="txtUserNameQ" /></div>--%>
            <div class="search_textbox_container"><dx:ASPxTextBox ID="txtUserNameQ" runat="server" Width="191" Size="191"></dx:ASPxTextBox></div>
            <div class="search_button_container"><asp:ImageButton ID="imgSearchByUserName" runat="server" ImageUrl="//cdn.goomena.com/Images/spacer10.png" CssClass="btn imgSearchByUserName" /></div>
        </asp:Panel>

        <div class="adv-search-link">
            <asp:LinkButton ID="lnkAdvanced" runat="server" CssClass="btn">Advanced</asp:LinkButton>
        </div>
    </div>

    <div class="inner-box">
        <div class="filters_prompt_container">
            <asp:Label ID="msg_UserUserNameOrFiltersQ_ND" runat="server" Text="Or use filters below"></asp:Label>
        </div>
        <div class="clear"></div>
        <asp:Panel ID="pnlQUserName1" runat="server" CssClass="input-user">
            <div class="age_selector_container">
                <div class="lfloat">
                    <uc1:AgeDropDown ID="ageMin" runat="server" Style="width: 40px" DefaultValue="18" />
                </div>
                <div class="lfloat" style="padding-top: 2px; padding-left: 5px; padding-right: 5px; font-size: 12px">
                    <asp:Label ID="msg_ToSmall" runat="server" Text="to" AssociatedControlID="ageMin" CssClass="description"></asp:Label></div>
                <div class="lfloat">
                    <uc1:AgeDropDown ID="ageMax" runat="server" Style="width: 40px" DefaultValue="120" />
                </div>
                <div class="lfloat" style="padding-top: 2px; padding-left: 5px; padding-right: 5px; font-size: 12px">
                    <asp:Label ID="lblYears" runat="server" Text="Years old"></asp:Label>
                </div>
            </div>

            <div class="distance_pulldown_container">
                <div class="age_label_container">
                    <asp:Label ID="msg_DistanceFromMe_ND" runat="server" Text="to" AssociatedControlID="ddlDistance" CssClass="description"></asp:Label>
                </div>
                <uc4:DistanceDropDown ID="ddlDistance" runat="server" AutoPostBack="false" />
            </div>
        
            <div class="clear"></div>
        </asp:Panel>
        <asp:Panel ID="pnlQFilters" runat="server" DefaultButton="btnSearch_ND" CssClass="other-criterias">

            <%--<div class="lfloat" style="padding: 0 5px;">
                <asp:Label ID="msg_AgeText" runat="server" Text="to" AssociatedControlID="ageMax" CssClass="description"></asp:Label></div>--%>
            <div class="clear"></div>

            <table class="options_table">
                <tr>
                    <td>
            <ul>
                <li style="padding-right:20px;">
                    <dx:ASPxCheckBox ID="chkPhotos" runat="server" Text="Has Photos" EncodeHtml="False">
                    </dx:ASPxCheckBox>
                </li>
                <li style="padding-right:20px;">
                    <dx:ASPxCheckBox ID="chkOnline" runat="server" Text="Is Online" EncodeHtml="False">
                    </dx:ASPxCheckBox>
                </li>
                <li>
                    <dx:ASPxCheckBox ID="chkVIP" runat="server" Text="Is VIP" EncodeHtml="False">
                    </dx:ASPxCheckBox>
                </li>
            </ul>
            <div class="clear"></div>
            <asp:Label ID="msg_BirthdayNote" runat="server" Text="" CssClass="birthday-text" visible="false"></asp:Label>
                    </td>
                </tr>
            </table>
            
            <div class="submit bottom_submit">
                <asp:Button ID="btnSearch_ND" runat="server" Text="Update Search »" CssClass="btn btnSearch" />
            </div>
        </asp:Panel>                                 
    </div>                                                        
</div>

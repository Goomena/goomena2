﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Root.Master" CodeBehind="Landing7.aspx.vb" Inherits="Dating.Server.Site.Web.Landing7" %>
<%@ Register src="UserControls/ucLandingProfiles.ascx" tagname="ucLandingProfiles" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="insidemosaiccontainer" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="content" runat="server">
    <div class="landingcontainer">
        <asp:Label ID="lblHeader" runat="server" Text="Label"></asp:Label>
        <div class="landingdiv3">
            <div class="landingdiv3left">
                <asp:Label ID="btnRegister" runat="server" Text="Label"></asp:Label>
                <asp:Label ID="lblCloseRdv" runat="server" Text="Label"></asp:Label>
            </div>
            <div class="landingdiv3right">
                <uc1:ucLandingProfiles ID="ucLP1" runat="server" />
                <%--<div class="landingdiv3rightup">
                    <asp:Label ID="lblFacesUp" runat="server" Text=""></asp:Label>
                    <div class="clear"></div>
                </div>
                <div class="landingdiv3rightdown">
                    <asp:Label ID="lblFacesDown" runat="server" Text=""></asp:Label>
                    <div class="clear"></div>
                </div>--%>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cntBodyBottom" runat="server">
</asp:Content>

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMessageInfo
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMessageInfo))
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.PictureEdit1 = New DevExpress.XtraEditors.PictureEdit()
        Me.pnl = New DevExpress.XtraEditors.PanelControl()
        Me.pnlButtons = New DevExpress.XtraEditors.PanelControl()
        Me.pnlExtraInfo = New DevExpress.XtraEditors.PanelControl()
        Me.XtraMessage = New DevExpress.XtraEditors.MemoEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.dt = New System.Windows.Forms.Timer(Me.components)
        CType(Me.PictureEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pnl, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnl.SuspendLayout()
        CType(Me.pnlButtons, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pnlExtraInfo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlExtraInfo.SuspendLayout()
        CType(Me.XtraMessage.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LabelControl1.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter
        Me.LabelControl1.Location = New System.Drawing.Point(70, 0)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Padding = New System.Windows.Forms.Padding(10)
        Me.LabelControl1.Size = New System.Drawing.Size(392, 48)
        Me.LabelControl1.TabIndex = 1
        Me.LabelControl1.Text = "LabelControl1"
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "Attention")
        Me.ImageList1.Images.SetKeyName(1, "Denied")
        Me.ImageList1.Images.SetKeyName(2, "Info")
        Me.ImageList1.Images.SetKeyName(3, "Error")
        Me.ImageList1.Images.SetKeyName(4, "Question")
        '
        'PictureEdit1
        '
        Me.PictureEdit1.Dock = System.Windows.Forms.DockStyle.Left
        Me.PictureEdit1.Location = New System.Drawing.Point(0, 0)
        Me.PictureEdit1.Name = "PictureEdit1"
        Me.PictureEdit1.Properties.AllowFocused = False
        Me.PictureEdit1.Properties.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.[False]
        Me.PictureEdit1.Properties.AllowScrollViaMouseDrag = False
        Me.PictureEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit1.Properties.Appearance.Options.UseBackColor = True
        Me.PictureEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PictureEdit1.Properties.ReadOnly = True
        Me.PictureEdit1.Properties.ShowMenu = False
        Me.PictureEdit1.Properties.ShowZoomSubMenu = DevExpress.Utils.DefaultBoolean.[False]
        Me.PictureEdit1.Size = New System.Drawing.Size(70, 48)
        Me.PictureEdit1.TabIndex = 3
        '
        'pnl
        '
        Me.pnl.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.pnl.Controls.Add(Me.LabelControl1)
        Me.pnl.Controls.Add(Me.PictureEdit1)
        Me.pnl.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnl.Location = New System.Drawing.Point(0, 0)
        Me.pnl.Name = "pnl"
        Me.pnl.Size = New System.Drawing.Size(462, 48)
        Me.pnl.TabIndex = 4
        '
        'pnlButtons
        '
        Me.pnlButtons.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pnlButtons.Location = New System.Drawing.Point(0, 68)
        Me.pnlButtons.Name = "pnlButtons"
        Me.pnlButtons.Size = New System.Drawing.Size(462, 35)
        Me.pnlButtons.TabIndex = 5
        '
        'pnlExtraInfo
        '
        Me.pnlExtraInfo.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.pnlExtraInfo.Appearance.Options.UseFont = True
        Me.pnlExtraInfo.Controls.Add(Me.XtraMessage)
        Me.pnlExtraInfo.Controls.Add(Me.LabelControl2)
        Me.pnlExtraInfo.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pnlExtraInfo.Location = New System.Drawing.Point(0, 48)
        Me.pnlExtraInfo.Name = "pnlExtraInfo"
        Me.pnlExtraInfo.Size = New System.Drawing.Size(462, 20)
        Me.pnlExtraInfo.TabIndex = 6
        Me.pnlExtraInfo.Visible = False
        '
        'XtraMessage
        '
        Me.XtraMessage.Dock = System.Windows.Forms.DockStyle.Fill
        Me.XtraMessage.Location = New System.Drawing.Point(2, 19)
        Me.XtraMessage.Name = "XtraMessage"
        Me.XtraMessage.Properties.ReadOnly = True
        Me.XtraMessage.Size = New System.Drawing.Size(458, 0)
        Me.XtraMessage.TabIndex = 2
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.LabelControl2.Appearance.Image = CType(resources.GetObject("LabelControl2.Appearance.Image"), System.Drawing.Image)
        Me.LabelControl2.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.LabelControl2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.LabelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.LabelControl2.Dock = System.Windows.Forms.DockStyle.Top
        Me.LabelControl2.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter
        Me.LabelControl2.Location = New System.Drawing.Point(2, 2)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Padding = New System.Windows.Forms.Padding(10, 0, 0, 0)
        Me.LabelControl2.Size = New System.Drawing.Size(458, 17)
        Me.LabelControl2.TabIndex = 1
        Me.LabelControl2.Text = "Show more.."
        '
        'dt
        '
        Me.dt.Interval = 1000
        '
        'frmMessageInfo
        '
        Me.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(462, 103)
        Me.ControlBox = False
        Me.Controls.Add(Me.pnl)
        Me.Controls.Add(Me.pnlExtraInfo)
        Me.Controls.Add(Me.pnlButtons)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "frmMessageInfo"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmMessageInfo"
        CType(Me.PictureEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pnl, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnl.ResumeLayout(False)
        CType(Me.pnlButtons, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pnlExtraInfo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlExtraInfo.ResumeLayout(False)
        CType(Me.XtraMessage.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents PictureEdit1 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents pnl As DevExpress.XtraEditors.PanelControl
    Friend WithEvents pnlButtons As DevExpress.XtraEditors.PanelControl
    Friend WithEvents pnlExtraInfo As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents XtraMessage As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents dt As System.Windows.Forms.Timer
End Class

﻿Public Class frmClearPhotosCache

    Private InfoBottomText As String
    Private Sub frmClearPhotosCache_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        dteFrom.EditValue = Date.Now.Date.AddYears(-1)
        dteTo.EditValue = Date.Now.Date.AddDays(1)

        InfoBottomText = lblInfoBottom.Text
        lblInfoBottom.Visible = False
    End Sub

    Private Sub btnClear_Click(sender As System.Object, e As System.EventArgs) Handles btnClear.Click
        Try
            lblInfoBottom.Visible = False

            Dim result As Dating.Server.Core.DLL.RemotePost.ADMIN_CACHE_FOLDER_Stats =
                Dating.Server.Core.DLL.RemotePost.RemoveCachedFoldersForPeriod(dteFrom.DateTime.Date, dteTo.DateTime.Date)

            lblInfoBottom.Visible = True
            lblInfoBottom.Text = InfoBottomText
            lblInfoBottom.Text = lblInfoBottom.Text.Replace("[FOLDERS]", result.Folders)
            lblInfoBottom.Text = lblInfoBottom.Text.Replace("[FILES]", result.Files)

        Catch ex As Exception
        End Try
    End Sub

  
    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub
End Class
﻿Imports Dating.Client.Admin.WSReferences.DLL

Public Class ControlsGlobals

    Public Shared gER As New Library.Public.clsErrors(True, False)
    Public Shared gWorkingSiteName As String
    Public Shared gLoginRec As AdminWS.clsDataRecordLoginReturn
    Public Shared gLoginReferrersRec As AdminReferrersWS.clsDataRecordLoginReturn


    Public Shared Sub ErrorMsgBox(ex As Exception, message As String)
        If (ex.Message.Contains("Server did not recognize the value of HTTP Header SOAPAction: ")) Then
            gER.ErrorMsgBox(ex, vbCrLf & vbCrLf & "Web services definition is outdated." & vbCrLf & "An updated version of web services are required." & vbCrLf & vbCrLf & message & vbCrLf & vbCrLf & ex.ToString())
        Else
            gER.ErrorMsgBox(ex, message & vbCrLf & vbCrLf & ex.ToString())
        End If
    End Sub

    Public Shared Sub ErrorMsgBox(message As String)
        gER.ErrorMsgBox(message)
    End Sub


End Class

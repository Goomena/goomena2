﻿Imports System.Windows.Forms

Public Module Message

    Public Function ShowMessage(ByVal Title As String, ByVal Message As String, ByVal Type As MessageIcon, ByVal Buttons As MessageButtons, Optional ByRef _frm As Form = Nothing) As DialogResult
        Application.DoEvents()
        Dim re As DialogResult = DialogResult.Cancel
        Using o As New frmMessageInfo
            re = o.ShowMessage(Title, Message, Type, Buttons, _frm)
        End Using
        Return re
    End Function
    Public Function ShowMessage(ByVal Title As String, ByVal Message As String, ByVal Type As MessageIcon, ByVal Buttons As MessageButtonsCustom, ByVal CustomButtonName As String, Optional ByRef _frm As Form = Nothing) As DialogResult
        Application.DoEvents()
        Dim re As DialogResult = DialogResult.Cancel
        Using o As New frmMessageInfo
            re = o.ShowMessage(Title, Message, Type, Buttons, CustomButtonName, _frm)
        End Using
        Return re
    End Function
    Public Function ShowMessage(ByVal ex As Exception, ByVal formName As String, ByVal methodName As String, ByVal _Title As String, ByVal _Message As String, ByVal _Type As MessageIcon, ByVal _Buttons As MessageButtons, Optional ByRef _frm As Form = Nothing) As DialogResult
        Application.DoEvents()
        Dim ms As String = "Sender: " & formName & vbNewLine & "Method: " & methodName & vbNewLine & " Message: " & ex.Message
        'ErrorFile.add(ex, formName, methodName)
        Dim re As DialogResult = DialogResult.Cancel
        Using o As New frmMessageInfo
            re = o.ShowMessage(_Title, _Message, ms, _Type, _Buttons, _frm)
        End Using
        Return re
    End Function
    Public Function ShowMessage(ByVal msg1 As String, ByVal formName As String, ByVal methodName As String, ByVal _Title As String, ByVal _Message As String, ByVal _Type As MessageIcon, ByVal _Buttons As MessageButtons, Optional ByRef _frm As Form = Nothing) As DialogResult
        Application.DoEvents()
        Dim ms As String = "Sender: " & formName & vbNewLine & "Method: " & methodName & vbNewLine & " Message: " & msg1
        'ErrorFile.add(msg1, formName, methodName)
        Dim re As DialogResult = DialogResult.Cancel
        Using o As New frmMessageInfo
            re = o.ShowMessage(_Title, _Message, ms, _Type, _Buttons, _frm)
        End Using
        Return re
    End Function
    Public Function ShowMessage(ByVal ex As Exception, ByVal formName As String, ByVal methodName As String, ByVal Title As String, ByVal Message As String, ByVal Type As MessageIcon, ByVal Buttons As MessageButtonsCustom, ByVal CustomButtonName As String, Optional ByRef _frm As Form = Nothing) As DialogResult
        Application.DoEvents()
        Dim ms As String = "Sender: " & formName & vbNewLine & "Method: " & methodName & vbNewLine & " Message: " & ex.Message
        ' ErrorFile.add(ex, formName, methodName)
        Dim re As DialogResult = DialogResult.Cancel
        Using o As New frmMessageInfo
            re = o.ShowMessage(Title, Message, ms, Type, Buttons, CustomButtonName, _frm)
        End Using
        Return re
    End Function
    Public Function ShowMessage(ByVal msg1 As String, ByVal formName As String, ByVal methodName As String, ByVal Title As String, ByVal Message As String, ByVal Type As MessageIcon, ByVal Buttons As MessageButtonsCustom, ByVal CustomButtonName As String, Optional ByRef _frm As Form = Nothing) As DialogResult
        Application.DoEvents()
        Dim ms As String = "Sender: " & formName & vbNewLine & "Method: " & methodName & vbNewLine & " Message: " & msg1
        'ErrorFile.add(msg1, formName, methodName)
        Dim re As DialogResult = DialogResult.Cancel
        Using o As New frmMessageInfo
            re = o.ShowMessage(Title, Message, ms, Type, Buttons, CustomButtonName, _frm)
        End Using
        Return re
    End Function
    Public Enum MessageIcon
        Attention
        Info
        Denied
        [Error]
        Question
    End Enum
    Public Enum MessageButtons
        OkOnly
        YesNo
        YesNoCancel
    End Enum
    Public Enum MessageButtonsCustom
        OkCustom
        YesNoCustom
    End Enum
End Module

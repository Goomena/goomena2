﻿Public Class UCLoading


    Private Sub UC_Resize(sender As Object, e As EventArgs) Handles Me.Resize
        If (Me.Visible) Then
            ShowLoader()
        End If
    End Sub


    Public Sub ShowLoader()
        ShowLoader(Me.ParentForm)
    End Sub

    Public Sub ShowLoader(centerOnControl As Windows.Forms.Control)
        Me.Visible = True
        'prgPanel1.Visible = True
        If (centerOnControl IsNot Nothing) Then
            Dim absolutePoint As System.Drawing.Point = centerOnControl.Location

            Dim ctl As Windows.Forms.Control = centerOnControl.Parent()
            While (ctl IsNot Nothing AndAlso Not (TypeOf ctl Is Windows.Forms.Form))
                absolutePoint.X = absolutePoint.X + ctl.Location.X
                absolutePoint.Y = absolutePoint.Y + ctl.Location.Y
                ctl = ctl.Parent()
            End While


            Me.Left = (centerOnControl.Width / 2 - Me.Width / 2) + absolutePoint.X
            Me.Top = (centerOnControl.Height / 2 - Me.Height / 2) + absolutePoint.Y
            Me.BringToFront()
            prgPanel1.BringToFront()
        End If
    End Sub

    Public Sub HideLoader()
        'prgPanel1.Visible = False
        Me.Visible = False
    End Sub

End Class

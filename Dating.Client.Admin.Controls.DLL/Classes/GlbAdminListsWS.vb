﻿Imports AdminListsWS = Dating.Client.Admin.WSReferences.DLL.AdminListsWS
Imports System.Net
Imports System.IO

Public Class GlbAdminListsWS

    Public Shared Proxies As New AdminListsWS.AdminLists()
    Public Shared Headers As New AdminListsWS.clsWSHeaders()
    Public Shared LoginData As New AdminListsWS.clsDataRecordLogin
    Public Shared LoginDataResult As AdminListsWS.clsDataRecordLoginReturn


    Public Shared Sub PerformLogin(LoginName As String, Password As String, HardwareID As String)
        LoginData.LoginName = LoginName
        LoginData.Password = Password
        LoginData.HardwareID = HardwareID

        LoginDataResult = Proxies.Login(Headers, LoginData)

        If LoginDataResult.HasErrors Or Not LoginDataResult.IsValid Then
            MsgBox(LoginDataResult.Message)
        End If
    End Sub


    Public Shared Sub Logout(Optional AllowReLogin As Boolean = True)

        Try
            Proxies.CancelAsync(Nothing)
            Proxies.Dispose()
        Catch ex As Exception
            'ErrorMsgBox(ex, "frmMain->gAdminListsWS")
        End Try


        If (AllowReLogin) Then
            Proxies = New AdminListsWS.AdminLists()
            LoginData = New AdminListsWS.clsDataRecordLogin
            Headers = New AdminListsWS.clsWSHeaders()
        End If

    End Sub


    Public Shared Function CheckResult(webResult As AdminListsWS.clsDataRecordErrorsReturn) As AdminListsWS.clsDataRecordErrorsReturn
        If webResult.HasErrors Then
            If webResult.ErrorCode = 999 Then
                Throw New UserCredentialsInvalidException()
            Else
                MsgBox("WS Message:" & vbCrLf & webResult.Message)
            End If
        End If

        Return webResult
    End Function


End Class


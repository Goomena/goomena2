﻿Imports Dating.Client.Admin.WSReferences.DLL.AdminWS
Imports AdminWS = Dating.Client.Admin.WSReferences.DLL.AdminWS
Imports System.IO
Imports System.Linq
Imports System.Runtime.CompilerServices


Public Class clsProfileImagesList
    Dim _list As List(Of ProfileImage)

    Public Sub New()
        _list = New List(Of ProfileImage)
    End Sub

    <MethodImplAttribute(MethodImplOptions.Synchronized)> _
    Public Sub Add(item As ProfileImage)
        Dim foundItem = Nothing
        Try
            foundItem = _list.Where(Function(itm As ProfileImage) itm.PhotoID = item.PhotoID).ToList().SingleOrDefault()
            If (foundItem Is Nothing) Then
                _list.Add(item)
            End If
        Catch ex As System.InvalidOperationException
            foundItem = _list.Where(Function(itm As ProfileImage) itm.PhotoID = item.PhotoID).ToList().SingleOrDefault()
            If (foundItem Is Nothing) Then
                _list.Add(item)
            End If
        End Try
    End Sub


    <MethodImplAttribute(MethodImplOptions.Synchronized)> _
    Public Function GetItemByPhotoId(PhotoId As Integer) As ProfileImage
        'Dim foundItem = _list.Where(Function(itm As ProfileImage) itm.PhotoID = PhotoId).ToList().SingleOrDefault()
        'Return foundItem
        Dim foundItem = Nothing
        Try
            foundItem = _list.Where(Function(itm As ProfileImage) itm.PhotoID = PhotoId).ToList().SingleOrDefault()
        Catch ex As System.InvalidOperationException
            System.Threading.Thread.Sleep(1000)
            foundItem = _list.Where(Function(itm As ProfileImage) itm.PhotoID = PhotoId).ToList().SingleOrDefault()
        End Try
        Return foundItem
    End Function


    <MethodImplAttribute(MethodImplOptions.Synchronized)> _
    Public Function GetItemByPhotoId(PhotoId As Integer, isThumb As Boolean) As ProfileImage
        'Dim foundItem = _list.Where(Function(itm As ProfileImage) itm.PhotoID = PhotoId AndAlso itm.IsThumb = isThumb).ToList().SingleOrDefault()
        'Return foundItem

        Dim foundItem = Nothing
        Try
            foundItem = _list.Where(Function(itm As ProfileImage) itm.PhotoID = PhotoId AndAlso itm.IsThumb = isThumb).ToList().SingleOrDefault()
        Catch ex As System.InvalidOperationException
            System.Threading.Thread.Sleep(1000)
            foundItem = _list.Where(Function(itm As ProfileImage) itm.PhotoID = PhotoId AndAlso itm.IsThumb = isThumb).ToList().SingleOrDefault()
        Catch ex As System.Data.RowNotInTableException
            _list.Clear()
        End Try
        Return foundItem
    End Function


    <MethodImplAttribute(MethodImplOptions.Synchronized)> _
    Public Function RemoveItem(ByRef itm As ProfileImage) As Boolean
        Try
            Return _list.Remove(itm)
        Catch ex As System.InvalidOperationException
            System.Threading.Thread.Sleep(1000)
            Return _list.Remove(itm)
        End Try
    End Function


    <MethodImplAttribute(MethodImplOptions.Synchronized)> _
    Public Function GetDefaultItemByCustomerID(CustomerID As Integer) As ProfileImage
        'Dim foundItem = _list.Where(Function(itm As ProfileImage) itm.ProfileID = CustomerID).ToList().FirstOrDefault()
        'Return foundItem
        Dim foundItem = Nothing
        Try
            foundItem = _list.Where(Function(itm As ProfileImage) itm.ProfileID = CustomerID).ToList().FirstOrDefault()
        Catch ex As System.InvalidOperationException
            System.Threading.Thread.Sleep(1000)
            foundItem = _list.Where(Function(itm As ProfileImage) itm.ProfileID = CustomerID).ToList().FirstOrDefault()
        End Try
        Return foundItem
    End Function


    <MethodImplAttribute(MethodImplOptions.Synchronized)> _
    Public Function GetItemByFileName(fileName As String) As ProfileImage
        'Dim foundItem = _list.Where(Function(itm As ProfileImage) itm.FileName = fileName).ToList().SingleOrDefault()
        'Return foundItem
        Dim foundItem = Nothing
        Try
            foundItem = _list.Where(Function(itm As ProfileImage) itm.FileName = fileName).ToList().SingleOrDefault()
        Catch ex As System.InvalidOperationException
            System.Threading.Thread.Sleep(1000)
            foundItem = _list.Where(Function(itm As ProfileImage) itm.FileName = fileName).ToList().SingleOrDefault()
        End Try
        Return foundItem
    End Function


    <MethodImplAttribute(MethodImplOptions.Synchronized)> _
    Public Sub Clear()
        'For Each itm As ProfileImage In _list
        '    itm.Dispose()
        'Next
        '_list.Clear()
        Try
            For Each itm As ProfileImage In _list
                itm.Dispose()
            Next
            _list.Clear()
        Catch ex As System.InvalidOperationException
            System.Threading.Thread.Sleep(1000)

            For Each itm As ProfileImage In _list
                itm.Dispose()
            Next
            _list.Clear()
        End Try
    End Sub


    Public ReadOnly Property Count As Integer
        Get
            ' Return _list.Count
            Try
                Return _list.Count
            Catch ex As System.InvalidOperationException
                System.Threading.Thread.Sleep(1000)
                Return _list.Count
            End Try
        End Get
    End Property

End Class


Public Class ProfileImage

    Private _row As DSMembersViews.CustomerPhotosViewRow


    Public ReadOnly Property ProfileID As Integer
        Get
            Return _row.CustomerID
        End Get
    End Property


    Public ReadOnly Property PhotoID As Long
        Get
            Return _row.CustomerPhotosID
        End Get
    End Property


    Public ReadOnly Property FileName As String
        Get
            Return _row.FileName
        End Get
    End Property


    Private __Width As Integer
    Public ReadOnly Property Width As Integer
        Get
            Return __Width
        End Get
    End Property

    Private __Height As Integer
    Public ReadOnly Property Height As Integer
        Get
            Return __Height
        End Get
    End Property

    Private __Image As System.Drawing.Image
    Public Property Image As System.Drawing.Image
        Get
            Return Me.GetImage()
        End Get
        Set(value As System.Drawing.Image)
            __Image = value
        End Set
    End Property

    Private __EditImage As System.Drawing.Image
    Public ReadOnly Property EditImage As System.Drawing.Image
        Get
            Return Me.GetEditImage()
        End Get
    End Property


    ' Public Property Image As System.Drawing.Image
    Public Property IsThumb As Boolean
    Public Property ImagePath As String
    Public Property FileURL As String
    Public Property FormattedFileSize As String
    Public Property Length As Integer
    Public Property FileType As String
    Public Property BackupImagePath As String
    Public Property EditImagePath As String

    Public Event LoadingPhotoStarted(ByVal sender As Object, ByVal e As EventArgs)
    Public Event LoadingPhotoComplete(ByVal sender As Object, ByVal e As EventArgs)


    Public Sub New()
    End Sub

    Public Sub New(row As DSMembersViews.CustomerPhotosViewRow, Optional loadThumb As Boolean = False)
        LoadPhoto(row, loadThumb)
    End Sub

    Public Sub LoadPhoto(row As DSMembersViews.CustomerPhotosViewRow, Optional loadThumb As Boolean = False)
        _row = row
        IsThumb = loadThumb
        _LoadPhoto()
    End Sub


    Private Sub _LoadPhoto()

        Dim fstr As FileStream = Nothing
        Dim _Image As System.Drawing.Image = Nothing
        Try
            If (Not IsThumb) Then
                Me.FileURL = String.Format(gMemberPhotoURL, Me.ProfileID, Me.FileName)
            Else
                Me.FileURL = String.Format(gMemberPhotoURLThumb, Me.ProfileID, Me.FileName)
            End If

            Dim localFilePath As String = Dating.Server.Core.DLL.RemotePost.GetLocalPhotoPathByURL(Me.FileURL)
            Me.ImagePath = localFilePath
            If (String.IsNullOrEmpty(localFilePath) OrElse Not System.IO.File.Exists(localFilePath)) Then
                RaiseEvent LoadingPhotoStarted(Me, New EventArgs())
                Me.ImagePath = GetWebFile(Me.FileURL)
                RaiseEvent LoadingPhotoComplete(Me, New EventArgs())
            End If

            If (Me.ImagePath <> "" And Me.ImagePath IsNot Nothing) Then
                Dim fi As New FileInfo(Me.ImagePath)
                fstr = fi.OpenRead()

                'Me.Image = System.Drawing.Image.FromStream(fstr)
                _Image = System.Drawing.Image.FromStream(fstr)
                __Height = _Image.Height
                __Width = _Image.Width

                Me.Length = fi.Length
                Me.FormattedFileSize = FormatFileSize(fi.Length)
                Me.FileType = fi.Extension.TrimStart("."c).ToUpper()
            End If
        Catch ex As Exception
            'ErrorMsgBox(ex, "")
        Finally
            If (fstr IsNot Nothing) Then fstr.Dispose()
            If (_Image IsNot Nothing) Then _Image.Dispose()
        End Try


    End Sub


    Private Function GetImage() As System.Drawing.Image
        If (__Image IsNot Nothing) Then
            Return __Image
        End If

        Dim _Image As System.Drawing.Image = Nothing
        Dim fstr As FileStream = Nothing

        Try
            If (Me.ImagePath <> "" And Me.ImagePath IsNot Nothing) Then
                If (System.IO.File.Exists(Me.ImagePath)) Then
                    Dim fi As New FileInfo(Me.ImagePath)
                    fstr = fi.OpenRead()
                    _Image = System.Drawing.Image.FromStream(fstr)
                Else
                    _LoadPhoto()
                End If
            End If
        Catch ex As Exception
            ' ErrorMsgBox(ex, "ProfileImage->GetImage")
        Finally
            If (fstr IsNot Nothing) Then fstr.Dispose()
        End Try

        Return _Image
    End Function


    Private Function GetEditImage() As System.Drawing.Image
        If (__EditImage IsNot Nothing) Then
            Return __EditImage
        End If

        Dim _editImage As System.Drawing.Image = Nothing
        Dim fstr As FileStream = Nothing

        Try
            'If (System.IO.File.Exists(Me.EditImagePath)) Then
            '    Dim fi As New FileInfo(Me.EditImagePath)
            '    fstr = fi.OpenRead()
            '    _editImage = System.Drawing.Image.FromStream(fstr)
            'Else
            '    _LoadPhoto()
            'End If
            If (Me.EditImagePath <> "" And Me.EditImagePath IsNot Nothing) Then
                Dim fi As New FileInfo(Me.EditImagePath)
                fstr = fi.OpenRead()
                _editImage = System.Drawing.Image.FromStream(fstr)
            End If
        Catch ex As Exception
            ErrorMsgBox(ex, "ProfileImage->GetEditImage")
        Finally
            If (fstr IsNot Nothing) Then fstr.Dispose()
        End Try

        Return _editImage
    End Function


    Public Function HasBackupFolder() As Boolean
        Dim exists As Boolean = False
        If (Not String.IsNullOrEmpty(Me.ImagePath)) Then
            Dim newFileName As String = Me.ImagePath
            Dim DirectoryName As String = System.IO.Path.GetDirectoryName(newFileName).Replace("\thumb", "")
            Dim BackDirectoryName As String = DirectoryName.TrimEnd("\"c) & "-BAK"


            If (Directory.Exists(BackDirectoryName)) Then
                exists = True
            End If
        End If
        Return exists
    End Function

    Public Sub CreateEditFolder()
        If (Not String.IsNullOrEmpty(Me.ImagePath)) Then
            Dim newFileName As String = Me.ImagePath
            Dim DirectoryName As String = System.IO.Path.GetDirectoryName(newFileName).Replace("\thumb", "")

            Dim count As String = 1
            Dim BackDirectoryName As String = DirectoryName.TrimEnd("\"c) & "-EDIT-" & count

            While (Directory.Exists(BackDirectoryName))
                Try
                    Directory.Delete(BackDirectoryName, True)
                Catch ex As Exception
                End Try
                count = count + 1
                BackDirectoryName = DirectoryName.TrimEnd("\"c) & "-EDIT-" & count
            End While
            'If (Directory.Exists(BackDirectoryName)) Then
            '    Try
            '        Directory.Delete(BackDirectoryName, True)
            '    Catch ex As Exception
            '    End Try
            'End If

            DirectoryCopy(DirectoryName, BackDirectoryName, True)
            Me.EditImagePath = System.IO.Path.Combine(BackDirectoryName, System.IO.Path.GetFileName(newFileName))
        End If
    End Sub


    Public Sub BackupImage()
        If (Not String.IsNullOrEmpty(Me.ImagePath)) Then
            Dim newFileName As String = Me.ImagePath
            'Dim newpath As String = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(newFileName), System.IO.Path.GetFileNameWithoutExtension(newFileName) & ".bak")
            Dim DirectoryName As String = System.IO.Path.GetDirectoryName(newFileName).Replace("\thumb", "")
            Dim BackDirectoryName As String = DirectoryName.TrimEnd("\"c) & "-BAK"


            If (Directory.Exists(BackDirectoryName)) Then
                Try
                    Directory.Delete(BackDirectoryName, True)
                Catch ex As Exception
                End Try
                If (Directory.Exists(BackDirectoryName)) Then
                    Try
                        Directory.Delete(BackDirectoryName, True)
                    Catch ex As Exception
                    End Try
                End If
            End If

            DirectoryCopy(DirectoryName, BackDirectoryName, True)
            Me.BackupImagePath = System.IO.Path.Combine(BackDirectoryName, System.IO.Path.GetFileName(newFileName))
        End If
    End Sub


    Public Sub RestoreImage()
        If (Not String.IsNullOrEmpty(Me.BackupImagePath)) Then
            Dim BackDirectoryName As String = System.IO.Path.GetDirectoryName(Me.BackupImagePath).Replace("\thumb", "")
            Dim DirectoryName As String = System.IO.Path.GetDirectoryName(Me.ImagePath).Replace("\thumb", "")

            If (Not String.IsNullOrEmpty(Me.ImagePath) AndAlso Not String.IsNullOrEmpty(Me.BackupImagePath)) Then
                If (Directory.Exists(DirectoryName)) Then
                    Try
                        Directory.Delete(DirectoryName, True)
                    Catch ex As Exception
                    End Try
                    If (Directory.Exists(DirectoryName)) Then
                        Try
                            Directory.Delete(DirectoryName, True)
                        Catch ex As Exception
                        End Try
                    End If
                End If


                Try

                    Directory.Move(BackDirectoryName, DirectoryName)
                Catch ex As Exception
                End Try
            End If
        End If
    End Sub


    Public Sub DeleteMainFolder()
        If (Not String.IsNullOrEmpty(Me.ImagePath)) Then
            Dim newFileName As String = Me.ImagePath
            'Dim newpath As String = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(newFileName), System.IO.Path.GetFileNameWithoutExtension(newFileName) & ".bak")
            Dim DirectoryName As String = System.IO.Path.GetDirectoryName(newFileName).Replace("\thumb", "")
            Dim DirectoryNameMove As String = DirectoryName & "" & Date.Now.ToFileTime()
            If (Directory.Exists(DirectoryName)) Then
                Try
                    Dim dir As New DirectoryInfo(DirectoryName)
                    dir.MoveTo(DirectoryNameMove)
                Catch ex As Exception
                End Try
            End If

            If (Directory.Exists(DirectoryNameMove)) Then
                Try
                    Directory.Delete(DirectoryNameMove, True)
                Catch ex As Exception
                End Try
                'If (Directory.Exists(DirectoryNameMove)) Then
                '    Try
                '        Directory.Delete(DirectoryNameMove, True)
                '    Catch ex As Exception
                '    End Try
                'End If
            End If
        End If
    End Sub


    Public Sub DeleteBAKFolder()
        If (Not String.IsNullOrEmpty(Me.ImagePath)) Then
            Dim newFileName As String = Me.ImagePath
            'Dim newpath As String = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(newFileName), System.IO.Path.GetFileNameWithoutExtension(newFileName) & ".bak")
            Dim DirectoryName As String = System.IO.Path.GetDirectoryName(newFileName).Replace("\thumb", "")
            Dim BackDirectoryName As String = DirectoryName.TrimEnd("\"c) & "-BAK"


            If (Directory.Exists(BackDirectoryName)) Then
                Try
                    Directory.Delete(BackDirectoryName, True)
                Catch ex As Exception
                End Try
                'If (Directory.Exists(BackDirectoryName)) Then
                '    Try
                '        Directory.Delete(BackDirectoryName, True)
                '    Catch ex As Exception
                '    End Try
                'End If
            End If
        End If
    End Sub


    Public Sub DeleteEDITFolder()
        If (Not String.IsNullOrEmpty(Me.ImagePath)) Then
            Dim newFileName As String = Me.ImagePath
            'Dim newpath As String = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(newFileName), System.IO.Path.GetFileNameWithoutExtension(newFileName) & ".bak")
            Dim DirectoryName As String = System.IO.Path.GetDirectoryName(newFileName).Replace("\thumb", "")

            Dim count As String = 1
            Dim EDITDirectoryName As String = DirectoryName.TrimEnd("\"c) & "-EDIT-" & count
            While (Directory.Exists(EDITDirectoryName))
                Try
                    Directory.Delete(EDITDirectoryName, True)
                Catch ex As Exception
                End Try
                count = count + 1
                EDITDirectoryName = DirectoryName.TrimEnd("\"c) & "-EDIT-" & count
            End While
            'Dim EDITDirectoryNameMove As String = EDITDirectoryName & "" & Date.Now.ToFileTime()
            'If (Directory.Exists(DirectoryName)) Then
            '    Try
            '        Dim dir As New DirectoryInfo(EDITDirectoryName)
            '        dir.MoveTo(EDITDirectoryNameMove)
            '    Catch ex As Exception
            '    End Try
            'End If

            'If (Directory.Exists(EDITDirectoryNameMove)) Then
            '    Try
            '        Directory.Delete(EDITDirectoryNameMove, True)
            '    Catch ex As Exception
            '    End Try
            '    'If (Directory.Exists(EDITDirectoryNameMove)) Then
            '    '    Try
            '    '        Directory.Delete(EDITDirectoryNameMove, True)
            '    '    Catch ex As Exception
            '    '    End Try
            '    'End If
            'End If
        End If
    End Sub


    Public Sub Dispose()

        ' delete local file from disk
        If (Not String.IsNullOrEmpty(Me.ImagePath)) Then
            Try
                If (__Image IsNot Nothing) Then
                    __Image.Dispose()
                    __Image = Nothing
                End If

                'Dim fi As New FileInfo(Me.ImagePath)
                'If (fi.Exists()) Then
                '    fi.Delete()
                'End If
            Catch ex As Exception
                ErrorMsgBox(ex, "")
            End Try
        End If

    End Sub


    Private Shared Sub DirectoryCopy( _
        ByVal sourceDirName As String, _
        ByVal destDirName As String, _
        ByVal copySubDirs As Boolean)

        ' Get the subdirectories for the specified directory. 
        Dim dir As DirectoryInfo = New DirectoryInfo(sourceDirName)
        Dim dirs As DirectoryInfo() = dir.GetDirectories()

        If Not dir.Exists Then
            Throw New DirectoryNotFoundException( _
                "Source directory does not exist or could not be found: " _
                + sourceDirName)
        End If

        ' If the destination directory doesn't exist, create it. 
        If Not Directory.Exists(destDirName) Then
            Directory.CreateDirectory(destDirName)
        End If
        ' Get the files in the directory and copy them to the new location. 
        Dim files As FileInfo() = dir.GetFiles()
        For Each file In files
            Dim temppath As String = Path.Combine(destDirName, file.Name)
            file.CopyTo(temppath, False)
        Next file

        ' If copying subdirectories, copy them and their contents to new location. 
        If copySubDirs Then
            For Each subdir In dirs
                Dim temppath As String = Path.Combine(destDirName, subdir.Name)
                DirectoryCopy(subdir.FullName, temppath, copySubDirs)
            Next subdir
        End If
    End Sub

End Class


﻿Imports System.Drawing
Imports System.Windows.Forms

Public Class PictureEditHelper


    Public Shared Sub AutosizeImage(ByVal image As Image,
                                  ByVal picBox As PictureBox,
                                  Optional ByVal pSizeMode As PictureBoxSizeMode = PictureBoxSizeMode.CenterImage)

        Dim imgShow As Bitmap = Nothing

        Try
            picBox.Image = Nothing
            picBox.SizeMode = pSizeMode
            If image IsNot Nothing Then
                Dim imgOrg As Bitmap = image
                Dim g As Graphics
                Dim divideBy, divideByH, divideByW As Double
                'imgOrg = DirectCast(Bitmap.FromFile(ImagePath), Bitmap)

                divideByW = imgOrg.Width / picBox.Width
                divideByH = imgOrg.Height / picBox.Height
                If divideByW > 1 Or divideByH > 1 Then
                    If divideByW > divideByH Then
                        divideBy = divideByW
                    Else
                        divideBy = divideByH
                    End If

                    imgShow = New Bitmap(CInt(CDbl(imgOrg.Width) / divideBy), CInt(CDbl(imgOrg.Height) / divideBy))
                    imgShow.SetResolution(imgOrg.HorizontalResolution, imgOrg.VerticalResolution)
                    g = Graphics.FromImage(imgShow)
                    g.InterpolationMode = Drawing2D.InterpolationMode.HighQualityBicubic
                    g.DrawImage(imgOrg, New Rectangle(0, 0, CInt(CDbl(imgOrg.Width) / divideBy), CInt(CDbl(imgOrg.Height) / divideBy)), 0, 0, imgOrg.Width, imgOrg.Height, GraphicsUnit.Pixel)
                    g.Dispose()
                Else
                    imgShow = New Bitmap(imgOrg.Width, imgOrg.Height)
                    imgShow.SetResolution(imgOrg.HorizontalResolution, imgOrg.VerticalResolution)
                    g = Graphics.FromImage(imgShow)
                    g.InterpolationMode = Drawing2D.InterpolationMode.HighQualityBicubic
                    g.DrawImage(imgOrg, New Rectangle(0, 0, imgOrg.Width, imgOrg.Height), 0, 0, imgOrg.Width, imgOrg.Height, GraphicsUnit.Pixel)
                    g.Dispose()
                End If
                imgOrg.Dispose()
                picBox.Image = imgShow
            Else
                picBox.Image = Nothing
            End If

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try


    End Sub


    'Public Shared Sub AutosizeImage(ByVal image As Image,
    '                              ByVal picBox As DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit,
    '                              Optional ByVal pSizeMode As PictureBoxSizeMode = PictureBoxSizeMode.CenterImage)

    '    Dim imgShow As Bitmap = Nothing

    '    Try
    '        picBox.Image = Nothing
    '        picBox.SizeMode = pSizeMode
    '        If image IsNot Nothing Then
    '            Dim imgOrg As Bitmap = image
    '            Dim g As Graphics
    '            Dim divideBy, divideByH, divideByW As Double
    '            'imgOrg = DirectCast(Bitmap.FromFile(ImagePath), Bitmap)

    '            divideByW = imgOrg.Width / picBox.Width
    '            divideByH = imgOrg.Height / picBox.Height
    '            If divideByW > 1 Or divideByH > 1 Then
    '                If divideByW > divideByH Then
    '                    divideBy = divideByW
    '                Else
    '                    divideBy = divideByH
    '                End If

    '                imgShow = New Bitmap(CInt(CDbl(imgOrg.Width) / divideBy), CInt(CDbl(imgOrg.Height) / divideBy))
    '                imgShow.SetResolution(imgOrg.HorizontalResolution, imgOrg.VerticalResolution)
    '                g = Graphics.FromImage(imgShow)
    '                g.InterpolationMode = Drawing2D.InterpolationMode.HighQualityBicubic
    '                g.DrawImage(imgOrg, New Rectangle(0, 0, CInt(CDbl(imgOrg.Width) / divideBy), CInt(CDbl(imgOrg.Height) / divideBy)), 0, 0, imgOrg.Width, imgOrg.Height, GraphicsUnit.Pixel)
    '                g.Dispose()
    '            Else
    '                imgShow = New Bitmap(imgOrg.Width, imgOrg.Height)
    '                imgShow.SetResolution(imgOrg.HorizontalResolution, imgOrg.VerticalResolution)
    '                g = Graphics.FromImage(imgShow)
    '                g.InterpolationMode = Drawing2D.InterpolationMode.HighQualityBicubic
    '                g.DrawImage(imgOrg, New Rectangle(0, 0, imgOrg.Width, imgOrg.Height), 0, 0, imgOrg.Width, imgOrg.Height, GraphicsUnit.Pixel)
    '                g.Dispose()
    '            End If
    '            imgOrg.Dispose()
    '            picBox.Image = imgShow
    '        Else
    '            picBox.Image = Nothing
    '        End If

    '    Catch ex As Exception
    '        MsgBox(ex.ToString)
    '    End Try


    'End Sub

End Class

﻿Imports AdminReferrersWS = Dating.Client.Admin.WSReferences.DLL.AdminReferrersWS
Imports System.Net
Imports System.IO

Public Class GlbAdminReferrersWS

    Public Shared Proxies As New AdminReferrersWS.AdminReferrers()
    Public Shared Headers As New AdminReferrersWS.clsWSHeaders()
    Public Shared LoginData As New AdminReferrersWS.clsDataRecordLogin
    Public Shared LoginDataResult As AdminReferrersWS.clsDataRecordLoginReturn


    Public Shared Sub PerformLogin(LoginName As String, Password As String, HardwareID As String)
        LoginData.LoginName = LoginName
        LoginData.Password = Password
        LoginData.HardwareID = HardwareID

        'GlbAdminWS.gHeaders.LoginName = LoginName
        'GlbAdminWS.gHeaders.Password = Password
        'GlbAdminWS.gHeaders.HardwareID = gHardwareID

        LoginDataResult = Proxies.Login(GlbAdminReferrersWS.Headers, LoginData)

        If LoginDataResult.HasErrors Or Not LoginDataResult.IsValid Then
            MsgBox(LoginDataResult.Message)
        End If
    End Sub


    Public Shared Sub Logout(Optional AllowReLogin As Boolean = True)

        Try
            Proxies.CancelAsync(Nothing)
            Proxies.Dispose()
        Catch ex As Exception
            'ErrorMsgBox(ex, "frmMain->gAdminReferrersWS")
        End Try


        If (AllowReLogin) Then
            Proxies = New AdminReferrersWS.AdminReferrers()
            LoginData = New AdminReferrersWS.clsDataRecordLogin
            Headers = New AdminReferrersWS.clsWSHeaders()
        End If

    End Sub


    Public Shared Function CheckResult(webResult As AdminReferrersWS.clsDataRecordErrorsReturn) As AdminReferrersWS.clsDataRecordErrorsReturn
        If webResult.HasErrors Then
            If webResult.ErrorCode = 999 Then
                Throw New UserCredentialsInvalidException()
            Else
                MsgBox("WS Message:" & vbCrLf & webResult.Message)
            End If
        End If

        Return webResult
    End Function

End Class


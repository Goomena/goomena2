﻿<Serializable()>
Public Class UserCredentialsInvalidException
    Inherits Exception

    Public Sub New()
        MyBase.New("User credentials incorrect. Please perform login.")
    End Sub
    Public Sub New(ByVal message As String)
        MyBase.New(message)
    End Sub
    Public Sub New(ByVal message As String, ByVal e As Exception)
        MyBase.New(message, e)
    End Sub

    ' A constructor is needed for serialization when an 
    ' exception propagates from a remoting server to the client.  
    Protected Sub New(info As System.Runtime.Serialization.SerializationInfo, context As System.Runtime.Serialization.StreamingContext)

    End Sub
End Class

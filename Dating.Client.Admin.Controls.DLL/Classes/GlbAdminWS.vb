﻿Imports AdminWS = Dating.Client.Admin.WSReferences.DLL.AdminWS
Imports System.Net
Imports System.IO

Public Class GlbAdminWS

    Public Shared Proxies As New AdminWS.Admin()
    Public Shared Headers As New AdminWS.clsWSHeaders()
    Public Shared LoginData As New AdminWS.clsDataRecordLogin
    Public Shared LoginDataResult As AdminWS.clsDataRecordLoginReturn



    Public Shared Sub PerformLogin(LoginName As String, Password As String, HardwareID As String)
        LoginData.LoginName = LoginName
        LoginData.Password = Password
        LoginData.HardwareID = HardwareID


    End Sub



    Public Shared Sub Logout(Optional AllowReLogin As Boolean = True)

        Try
            GlbAdminWS.Proxies.CancelAsync(Nothing)
            GlbAdminWS.Proxies.Dispose()
        Catch ex As Exception
            'ErrorMsgBox(ex, "frmMain->gAdminListsWS")
        End Try


        If (AllowReLogin) Then
            Proxies = New AdminWS.Admin()
            LoginData = New AdminWS.clsDataRecordLogin()
            Headers = New AdminWS.clsWSHeaders()
        End If

    End Sub



    Public Shared Function CheckResult(webResult As AdminWS.clsDataRecordErrorsReturn) As AdminWS.clsDataRecordErrorsReturn
        If webResult.HasErrors Then
            If webResult.ErrorCode = 999 Then
                Throw New UserCredentialsInvalidException()
            Else
                MsgBox("WS Message:" & vbCrLf & webResult.Message)
            End If
        End If

        Return webResult
    End Function


End Class


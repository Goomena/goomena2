﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UCLoading
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.prgPanel1 = New DevExpress.XtraWaitForm.ProgressPanel()
        Me.SuspendLayout()
        '
        'prgPanel1
        '
        Me.prgPanel1.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.prgPanel1.Appearance.Options.UseBackColor = True
        Me.prgPanel1.AppearanceCaption.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.prgPanel1.AppearanceCaption.Options.UseFont = True
        Me.prgPanel1.AppearanceDescription.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.prgPanel1.AppearanceDescription.Options.UseFont = True
        Me.prgPanel1.Location = New System.Drawing.Point(2, 3)
        Me.prgPanel1.Name = "prgPanel1"
        Me.prgPanel1.Size = New System.Drawing.Size(246, 66)
        Me.prgPanel1.TabIndex = 248
        Me.prgPanel1.Text = "ProgressPanel1"
        '
        'UCLoading
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.prgPanel1)
        Me.Name = "UCLoading"
        Me.Size = New System.Drawing.Size(251, 72)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents prgPanel1 As DevExpress.XtraWaitForm.ProgressPanel

End Class

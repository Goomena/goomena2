﻿Public Class frmMacros

    Public Event MacroSelected(sender As Object, macroParam As MacroSelectEventArgs)
    Private Sub frmMacros_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub btnLoginName_Click(sender As Object, e As EventArgs) Handles btnLoginName.Click
        Dim macro As String = "[LOGIN-NAME]"
        SendMacro(macro)
    End Sub


    Private Sub SendMacro(macro As String)
        Dim macroParam As New MacroSelectEventArgs(macro)
        RaiseEvent MacroSelected(Me, macroParam)
        Me.Close()
    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub
End Class

Public Class MacroSelectEventArgs
    Public Property Text As String
    Public Sub New(txt As String)
        Text = txt
    End Sub
End Class
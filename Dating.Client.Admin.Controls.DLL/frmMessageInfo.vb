﻿Imports System.Windows.Forms



Public Class frmMessageInfo
    Private _Title As String = "'"
    Public Function ShowMessage(ByVal Title As String, ByVal Message As String, ByVal Type As MessageIcon, ByVal Buttons As MessageButtons, Optional ByRef frm As Form = Nothing) As DialogResult
        Me.Text = Title
        _Title = Title
        LabelControl1.Text = Message
        Select Case Type
            Case MessageIcon.Question
                PictureEdit1.Image = ImageList1.Images.Item("Question")
                Exit Select
            Case MessageIcon.Attention
                PictureEdit1.Image = ImageList1.Images.Item("Attention")
                Exit Select
            Case MessageIcon.Denied
                PictureEdit1.Image = ImageList1.Images.Item("Denied")
                Exit Select
            Case MessageIcon.Error
                PictureEdit1.Image = ImageList1.Images.Item("Error")
                Exit Select
            Case Else
                PictureEdit1.Image = ImageList1.Images.Item("Info")
        End Select
        Select Case Buttons
            Case MessageButtons.OkOnly
                Dim cmd As New DevExpress.XtraEditors.SimpleButton With {.Width = 75, .Height = 23, .Top = 6, .Text = "OK", .Left = Math.Round((pnlButtons.Size.Width / 2) - (75 / 2), 0), .Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))}
                Me.pnlButtons.Controls.Add(cmd)
                AddHandler cmd.Click, AddressOf cmdOk_Click
                cmd.Focus()
                Exit Select
            Case MessageButtons.YesNo
                Dim cmd1 As New DevExpress.XtraEditors.SimpleButton With {.Width = 75, .Height = 23, .Top = 6, .Text = "Yes", .Left = Math.Round((pnlButtons.Size.Width / 2) - ((75 + 75 + 10) / 2), 0), .Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))}
                Me.pnlButtons.Controls.Add(cmd1)
                AddHandler cmd1.Click, AddressOf cmdYes_Click
                Dim cmd2 As New DevExpress.XtraEditors.SimpleButton With {.Width = 75, .Height = 23, .Top = 6, .Text = "No", .Left = cmd1.Left + 85, .Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))}
                Me.pnlButtons.Controls.Add(cmd2)
                AddHandler cmd2.Click, AddressOf cmdNo_Click
                cmd1.Focus()
                Exit Select
            Case MessageButtons.YesNoCancel
                Dim cmd1 As New DevExpress.XtraEditors.SimpleButton With {.Width = 75, .Height = 23, .Top = 6, .Text = "Yes", .Left = Math.Round((pnlButtons.Size.Width / 2) - ((75 + 75 + 75 + 10 + 10) / 2), 0), .Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))}
                Me.pnlButtons.Controls.Add(cmd1)
                AddHandler cmd1.Click, AddressOf cmdYes_Click
                Dim cmd2 As New DevExpress.XtraEditors.SimpleButton With {.Width = 75, .Height = 23, .Top = 6, .Text = "No", .Left = cmd1.Left + 85, .Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))}
                Me.pnlButtons.Controls.Add(cmd2)
                AddHandler cmd2.Click, AddressOf cmdNo_Click
                Dim cmd3 As New DevExpress.XtraEditors.SimpleButton With {.Width = 75, .Height = 23, .Top = 6, .Text = "Discard", .Left = cmd2.Left + 85, .Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))}
                Me.pnlButtons.Controls.Add(cmd3)
                AddHandler cmd3.Click, AddressOf cmdCancel_Click
                cmd1.Focus()
                Exit Select
        End Select
        If frm Is Nothing Then
            'Return Me.ShowDialog(frmStatus)
            Return Windows.Forms.DialogResult.None
        Else
            Return Me.ShowDialog(frm)
        End If
    End Function

    Private Sub cmdOk_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.DialogResult = DialogResult.OK
        Me.Close()
    End Sub
    Private Sub cmdYes_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.DialogResult = DialogResult.Yes
        Me.Close()
    End Sub
    Private Sub cmdNo_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.DialogResult = DialogResult.No
        Me.Close()
    End Sub
    Private Sub cmdCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.DialogResult = DialogResult.Cancel
        Me.Close()
    End Sub
    Private Sub cmdCustom_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub
    Public Function ShowMessage(ByVal Title As String, ByVal Message As String, ByVal Type As MessageIcon, ByVal Buttons As MessageButtonsCustom, ByVal CustomButton As String, Optional ByRef frm As Form = Nothing) As DialogResult
        Me.Text = Title
        _Title = Title
        LabelControl1.Text = Message
        Select Case Type
            Case MessageIcon.Question
                PictureEdit1.Image = ImageList1.Images.Item("Question")
                Exit Select
            Case MessageIcon.Attention
                PictureEdit1.Image = ImageList1.Images.Item("Attention")
                Exit Select
            Case MessageIcon.Denied
                PictureEdit1.Image = ImageList1.Images.Item("Denied")
                Exit Select
            Case MessageIcon.Error
                PictureEdit1.Image = ImageList1.Images.Item("Error")
                Exit Select
            Case Else
                PictureEdit1.Image = ImageList1.Images.Item("Info")
        End Select
        Select Case Buttons
            Case MessageButtonsCustom.OkCustom
                Dim cmd1 As New DevExpress.XtraEditors.SimpleButton With {.Width = 75, .Height = 23, .Top = 6, .Text = "OK", .Left = Math.Round((pnlButtons.Size.Width / 2) - (75 / 2), 0), .Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))}
                Me.pnlButtons.Controls.Add(cmd1)
                AddHandler cmd1.Click, AddressOf cmdYes_Click
                Dim cmd2 As New DevExpress.XtraEditors.SimpleButton With {.Width = 75, .Height = 23, .Top = 6, .Text = CustomButton, .Left = cmd1.Left + 85, .Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))}
                Me.pnlButtons.Controls.Add(cmd2)
                AddHandler cmd2.Click, AddressOf cmdCustom_Click
                cmd1.Focus()
                Exit Select
            Case MessageButtonsCustom.YesNoCustom
                Dim cmd1 As New DevExpress.XtraEditors.SimpleButton With {.Width = 75, .Height = 23, .Top = 6, .Text = "Yes", .Left = Math.Round((pnlButtons.Size.Width / 3) - (75 / 2), 0), .Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))}
                Me.pnlButtons.Controls.Add(cmd1)
                AddHandler cmd1.Click, AddressOf cmdYes_Click
                Dim cmd2 As New DevExpress.XtraEditors.SimpleButton With {.Width = 75, .Height = 23, .Top = 6, .Text = "No", .Left = cmd1.Left + 85, .Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))}
                Me.pnlButtons.Controls.Add(cmd2)
                AddHandler cmd2.Click, AddressOf cmdNo_Click
                Dim cmd3 As New DevExpress.XtraEditors.SimpleButton With {.Width = 75, .Height = 23, .Top = 6, .Text = CustomButton, .Left = cmd2.Left + 85, .Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))}
                Me.pnlButtons.Controls.Add(cmd3)
                AddHandler cmd3.Click, AddressOf cmdCustom_Click
                cmd1.Focus()
                Exit Select
        End Select
        If frm Is Nothing Then
            ' Return Me.ShowDialog(frmStatus)
            Return Windows.Forms.DialogResult.None
        Else
            Return Me.ShowDialog(frm)
        End If
    End Function
    Public Function ShowMessage(ByVal _Title As String, ByVal _Message As String, ByVal _extrainfo As String, ByVal _Type As MessageIcon, ByVal _Buttons As MessageButtons, Optional ByRef _frm As Form = Nothing) As DialogResult
        pnlExtraInfo.Visible = True
        XtraMessage.EditValue = _extrainfo
        Return ShowMessage(_Title, _Message, _Type, _Buttons, _frm)
    End Function
    Public Function ShowMessage(ByVal _Title As String, ByVal _Message As String, ByVal _extrainfo As String, ByVal _Type As MessageIcon, ByVal _Buttons As MessageButtonsCustom, ByVal _CustomButton As String, Optional ByRef _frm As Form = Nothing) As DialogResult
        pnlExtraInfo.Visible = True
        XtraMessage.EditValue = _extrainfo
        Return ShowMessage(_Title, _Message, _Type, _Buttons, _CustomButton, _frm)
    End Function
    Private Expanded As Boolean = False
    Dim s As New Object
    Private Sub LabelControl2_Click(sender As Object, e As EventArgs) Handles LabelControl2.Click
        SyncLock s
            If Me.Expanded = True Then
                Me.Height -= 110
                Me.pnlExtraInfo.Height = 20
                Expanded = False
            Else
                Me.Height += 110
                Me.pnlExtraInfo.Height = 130
                Expanded = True
            End If
        End SyncLock
    End Sub
    Public Sub EnableTimer()
        dt.Enabled = True
        Me.TopMost = True
        dt.Start()
    End Sub

    Private Sub frmMessageInfo_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        dt.Stop()
    End Sub
    Public Sub SetTopmost()
        Me.TopMost = True
    End Sub
    Dim ticks As Integer = 30
    Private Sub dt_Tick(sender As Object, e As EventArgs) Handles dt.Tick
        ticks -= 1
        Me.Text = _Title & " - " & ticks & "s"
        If ticks = 0 Then
            dt.Stop()
            Me.DialogResult = Windows.Forms.DialogResult.Cancel
            Me.Close()
        End If
    End Sub

    Public Function ShowMessage2(ByVal Title As String, ByVal Message As String, ByVal Type As MessageIcon, ByVal Buttons As MessageButtons, Optional ByRef frm As Form = Nothing) As DialogResult
        Me.Text = Title
        _Title = Title
        LabelControl1.Text = Message
        Select Case Type
            Case MessageIcon.Question
                PictureEdit1.Image = ImageList1.Images.Item("Question")
                Exit Select
            Case MessageIcon.Attention
                PictureEdit1.Image = ImageList1.Images.Item("Attention")
                Exit Select
            Case MessageIcon.Denied
                PictureEdit1.Image = ImageList1.Images.Item("Denied")
                Exit Select
            Case MessageIcon.Error
                PictureEdit1.Image = ImageList1.Images.Item("Error")
                Exit Select
            Case Else
                PictureEdit1.Image = ImageList1.Images.Item("Info")
        End Select
        Select Case Buttons
            Case MessageButtons.OkOnly
                Dim cmd As New DevExpress.XtraEditors.SimpleButton With {.Width = 75, .Height = 23, .Top = 6, .Text = "OK", .Left = Math.Round((pnlButtons.Size.Width / 2) - (75 / 2), 0), .Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))}
                Me.pnlButtons.Controls.Add(cmd)
                AddHandler cmd.Click, AddressOf cmdOk_Click
                cmd.Focus()
                Exit Select
            Case MessageButtons.YesNo
                Dim cmd1 As New DevExpress.XtraEditors.SimpleButton With {.Width = 75, .Height = 23, .Top = 6, .Text = "Yes", .Left = Math.Round((pnlButtons.Size.Width / 2) - ((75 + 75 + 10) / 2), 0), .Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))}
                Me.pnlButtons.Controls.Add(cmd1)
                AddHandler cmd1.Click, AddressOf cmdYes_Click
                Dim cmd2 As New DevExpress.XtraEditors.SimpleButton With {.Width = 75, .Height = 23, .Top = 6, .Text = "No", .Left = cmd1.Left + 85, .Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))}
                Me.pnlButtons.Controls.Add(cmd2)
                AddHandler cmd2.Click, AddressOf cmdNo_Click
                cmd2.Focus()
                Exit Select
            Case MessageButtons.YesNoCancel
                Dim cmd1 As New DevExpress.XtraEditors.SimpleButton With {.Width = 75, .Height = 23, .Top = 6, .Text = "Yes", .Left = Math.Round((pnlButtons.Size.Width / 2) - ((75 + 75 + 75 + 10 + 10) / 2), 0), .Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))}
                Me.pnlButtons.Controls.Add(cmd1)
                AddHandler cmd1.Click, AddressOf cmdYes_Click
                Dim cmd2 As New DevExpress.XtraEditors.SimpleButton With {.Width = 75, .Height = 23, .Top = 6, .Text = "No", .Left = cmd1.Left + 85, .Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))}
                Me.pnlButtons.Controls.Add(cmd2)
                AddHandler cmd2.Click, AddressOf cmdNo_Click
                Dim cmd3 As New DevExpress.XtraEditors.SimpleButton With {.Width = 75, .Height = 23, .Top = 6, .Text = "Discard", .Left = cmd2.Left + 85, .Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))}
                Me.pnlButtons.Controls.Add(cmd3)
                AddHandler cmd3.Click, AddressOf cmdCancel_Click
                cmd1.Focus()
                Exit Select
        End Select
        If frm Is Nothing Then
            ' Return Me.ShowDialog(frmStatus)
            Return Windows.Forms.DialogResult.None
        Else
            Return Me.ShowDialog(frm)
        End If
    End Function
End Class
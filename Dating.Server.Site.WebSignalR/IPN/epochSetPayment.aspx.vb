﻿Imports System.Security.Cryptography
Imports System.Reflection
Imports Dating.Server.Core.DLL

Public Class epochSetPayment
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim camCharge As String = ""
            Try
                camCharge = Request("camcharge")
            Catch ex As Exception

            End Try
            If camCharge = "" Then
                Dim pgUrl As String = "https://wnu.com/secure/fpost.cgi"
                Dim productID As String = Request("itemName")
                Dim amt As Decimal = Request("amount")
                Dim payTransID As String = Request("TransID")
                Dim currency As String = Request("currency")
                Dim type As String = Request("type")
                Dim okURL As String = Request("okURL")
                Dim oRemotePost As New RemotePost()
                oRemotePost.Url = pgUrl

                oRemotePost.Add("api", "join")
                oRemotePost.Add("pi_code", productID)
                oRemotePost.Add("reseller", "a")
                oRemotePost.Add("no_userpass", "Y")

                oRemotePost.Add("selected_type", type)
                oRemotePost.Add("name", Context.Request("name"))
                oRemotePost.Add("email", Context.Request("email"))
                oRemotePost.Add("zip", Context.Request("zip"))
                oRemotePost.Add("city", Context.Request("city"))
                oRemotePost.Add("state", Context.Request("state"))
                oRemotePost.Add("version", "3")

                oRemotePost.Add("x_merchant_order_id", payTransID)
                oRemotePost.Add("pi_returnurl", okURL & "/IPN/Ok.aspx?transID=" & payTransID)

                oRemotePost.Post()
                ' Catch ex As Threading.ThreadAbortException
            Else
                Dim pgUrl As String = "https://wnu.com/secure/services/?"
                Dim productID As String = Request("productDesc")
                productID = productID.Replace(" ", "").Replace("Goomena-", "")
                Dim amt As Decimal = Request("amount")
                Dim payTransID As String = Request("TransID")
                Dim currency As String = Request("currency")
                If Len(currency) = 0 Then currency = "EUR"
                Dim type As String = Request("type")
                Dim hmacHashKey As String = "6beac8f7343e625f682442118cda7ebb"
                Dim oRemotePost As New RemotePost()
                Dim parameters As New Dictionary(Of String, String)
                oRemotePost.Url = pgUrl


                Dim epochIPNHandlerURL As String = "http://www.goomena.com/IPN/epochIPNHandler.aspx"
                Dim returnurl As String = "http://www.goomena.com/IPN/Ok.aspx?transID=" & payTransID

                If (Me.Request.Url.Scheme.ToLower() = "https") Then
                    epochIPNHandlerURL = UrlUtils.GetHTTPSUrl(New Uri(epochIPNHandlerURL))
                    returnurl = UrlUtils.GetHTTPSUrl(New Uri(returnurl))
                End If


                pgUrl = pgUrl & "api=camcharge&" _
                              & "action=authandclose&" _
                              & "auth_amount=" & amt & "&" _
                              & "currency=" & currency & "&" _
                              & "description=" & productID & "&" _
                              & "pburl=" & System.Web.HttpUtility.UrlEncode(epochIPNHandlerURL) & "&" _
                              & "member_id=" & type & "&" _
                              & "x_merchant_order_id=" & payTransID & "&" _
                              & "returnurl=" & System.Web.HttpUtility.UrlEncode(returnurl) & "&"


                parameters.Add("api", "camcharge")
                parameters.Add("action", "authandclose")
                parameters.Add("auth_amount", amt)
                parameters.Add("currency", currency)
                parameters.Add("description", productID)
                parameters.Add("pburl", epochIPNHandlerURL)
                parameters.Add("member_id", type)
                parameters.Add("x_merchant_order_id", payTransID)
                parameters.Add("returnurl", returnurl)

                Dim singSTR As String = ""
                ' Get an array of keys and sort them
                Dim parameterKeys As New List(Of String)(parameters.Keys)
                parameterKeys.Sort()

                ' Loop through and concatenate the key and trimmed value
                Dim sb As New StringBuilder
                Dim key As String
                For Each key In parameterKeys
                    sb.Append(key)
                    sb.Append(parameters.Item(key).Trim)
                Next
                Dim hmacInput As String = sb.ToString
                Dim encoding As Encoding = encoding.UTF8
                Dim hmacMD5 As New HMACMD5(encoding.GetBytes(hmacHashKey))
                Dim hmachash As Byte() = hmacMD5.ComputeHash(encoding.GetBytes(hmacInput))

                Dim digest As New StringBuilder
                For i As Integer = 0 To hmachash.Length - 1
                    digest.Append(hmachash(i).ToString("x2"))
                Next i
                pgUrl = pgUrl & "epoch_digest=" & digest.ToString
                Response.Redirect(pgUrl)
            End If

        Catch ex As Exception

        End Try
    End Sub

    Function getHMACHash(ByVal SignStr As String, ByVal key As String) As Byte()
        Dim e As Encoding = Encoding.UTF8

        ' Initialize the keyed hash object.
        Dim hmac As New HMACMD5(e.GetBytes(key))
        Dim b As Byte() = hmac.ComputeHash(e.GetBytes(SignStr))
        Return b

    End Function
    Private Function Bytes_To_String2(ByVal bytes_Input As Byte()) As String
        Dim strTemp As New StringBuilder(bytes_Input.Length * 2)
        For Each b As Byte In bytes_Input
            strTemp.Append(Conversion.Hex(b))
        Next
        Return strTemp.ToString()
    End Function
End Class
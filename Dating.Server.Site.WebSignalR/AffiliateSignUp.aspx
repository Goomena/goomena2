﻿<%@ Page Title="" Language="vb" AutoEventWireup="true" MasterPageFile="~/Root.Master" CodeBehind="AffiliateSignUp.aspx.vb" Inherits="Dating.Server.Site.Web.AffiliateSignUp" %>
<%@ Register src="UserControls/LiveZilla.ascx" tagname="LiveZilla" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<%--    
    <!--[if lt IE 9]>
		<script src="Scripts/ie9.js">IE7_PNG_SUFFIX = ".png";</script>
	<![endif]-->
    <!--[if lt IE 8]>
	    <link rel="stylesheet" type="text/css" media="screen, projection" charset="utf-8" href="CSS/ie_fix.css">
	<![endif]-->
    <!--[if lte IE 7]>
	    <link rel="stylesheet" type="text/css" media="screen, projection" charset="utf-8" href="CSS/ie7_fix.css">
	<![endif]-->
--%>
    <style type="text/css">
        td.header { width: 1%; white-space: nowrap; padding: 0 20px 0 0px; vertical-align: text-top; display: block; text-align: left; }
        .mainregistercon { background: url("https://cdn.goomena.com/goomena-kostas/first-general/bg-reapet.jpg") repeat; border-top: 6px solid #be202f; padding-bottom: 100px; padding-top: 61px; }
        .regmidcont { margin-left: auto; margin-right: auto; position: relative; width: 500px; }
        .regmidcont li { list-style: none; }
        ul li { list-style: none; clear: both; display: block; height: 30px; margin-bottom: 10px; }
        ul li:after { clear: both; }
        ul li div.form_right { float: right; }
        div.form-actions { margin-top: 20px; position: relative; }
        div.form-actions input.linkbutton { position: absolute; top: -15px; right: -1px; }
        div.form-actions table { margin: 0 auto 0; }
        div.form-actions .cmdSend { cursor: pointer; }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <div class="mainregistercon">
        <div class="regmidcont">
            <h2 style="text-shadow: none; border: none; font-family: Segoe UI , Arial; font-size:24px; font-weight:lighter;margin-left: 38px;">
            <asp:Label ID="lbHeader" runat="server" CssClass="loginHereTitle" /><span class="loginHereLink"></span></h2>


            <asp:Panel ID="pnlSendMessageWrap" runat="server" CssClass="contact_form" DefaultButton="cmdSend">
                <asp:Label ID="lblMessageSent" runat="server" ViewStateMode="Disabled"></asp:Label>

                <asp:Panel ID="pnlSendMessageForm" runat="server">
                    <ul class="defaultStyle">
					    <!-- location -->
					    <li>
                            <asp:Panel ID="pnlYouremailErr" runat="server" CssClass="alert alert-danger" Visible="False">
                                <asp:Label ID="lbYouremailErr" runat="server" Text=""></asp:Label>
                            </asp:Panel>
						    <asp:Label ID="lbYouremail" runat="server" Text="" CssClass="contactLabel lfloat" AssociatedControlID="txtYouremail"/>
						    <div class="form_right lfloat">
                                <asp:Label ID="lblYouremailRead" runat="server" Visible="False" class="contactLabel" style="width:auto;"/>
                                <asp:TextBox ID="txtYouremail" runat="server" Width="310px" CssClass="lfloat"></asp:TextBox>
						    </div>
					    </li>
					    <!-- Country  -->
					    <li>
                            <asp:Panel ID="pnlYourWebSiteErr" runat="server" CssClass="alert alert-danger" Visible="False">
                                <asp:Label ID="lbYourWebSiteErr" runat="server" Text=""></asp:Label>
                            </asp:Panel>
					
                            <asp:Label ID="lbYourWebSite" runat="server" Text="" class="contactLabel lfloat" AssociatedControlID="txtYourWebSite"/>
						    <div class="form_right lfloat">
                                <asp:TextBox ID="txtYourWebSite" runat="server" Width="310px"/>
						    </div>
					    </li>
					    <li>
                            <asp:Panel ID="pnlMonthlyTrafficErr" runat="server" CssClass="alert alert-danger" Visible="False">
                                <asp:Label ID="lblMonthlyTrafficErr" runat="server" Text=""></asp:Label>
                            </asp:Panel>
					
                            <asp:Label ID="lblMonthlyTraffic" runat="server" Text="" class="contactLabel lfloat" AssociatedControlID="txtMonthlyTraffic"/>
						    <div class="form_right lfloat">
                                <asp:TextBox ID="txtMonthlyTraffic" runat="server" Width="310px"/>
						    </div>
					    </li>

					    <li>
                            <asp:Label ID="lbMessage" runat="server" Text="" class="contactLabel lfloat" AssociatedControlID="txtMessage"/>
						    <div class="form_right lfloat">
                                <asp:TextBox ID="txtMessage" runat="server" Width="310px" TextMode="MultiLine" Height="100px"/>
						    </div>
					    </li>
                    </ul>
	                <div class="clear padding"></div>
                    <div class="form-actions">
                    <table class="center" cellpadding="0" cellspacing="0">
                        <tr>
                            <td><div style="width:100px">&nbsp;</div></td>
                            <td><dx:ASPxButton ID="cmdSend" runat="server" CausesValidation="True"
                                    Text="Send"
                                    ValidationGroup="RegisterGroup"  EncodeHtml="False" Font-Bold="True"
                                    Height="41px" BackColor="#BF2130" EnableDefaultAppearance="False" 
                                    Font-Size="Smaller" ForeColor="White" Width="100px"
                                    CssClass="cmdSend">
                                    <Border BorderWidth="1px"></Border>
                                    <ClientSideEvents />
                                </dx:ASPxButton><%--<dx:ASPxButton ID="cmdSend" runat="server" CausesValidation="True"
                                        Text="Send"
                                        ValidationGroup="RegisterGroup"  EncodeHtml="False"
                                        CssFilePath="~/App_Themes/SoftOrange/{0}/styles.css" 
                                        CssPostfix="SoftOrange" Font-Bold="True" 
                                        SpriteCssFilePath="~/App_Themes/SoftOrange/{0}/sprite.css"
                                        Height="31px">
                                        <Paddings Padding="0px"></Paddings>
                                        <Border BorderWidth="1px"></Border>
                                        <ClientSideEvents />
                                    </dx:ASPxButton>--%></td>
                            <td><div style="width:50px">&nbsp;</div></td>
                            <td><input type="reset" id="cmdReset" runat="server" class="linkbutton" style="font-size:10px;"
                                value="Reset" /></td>
                        </tr>
                    </table>
                    </div>

                                </asp:Panel>
                            </asp:Panel>


            <div class="clear"></div>
</div>
</div>
        
</asp:Content>

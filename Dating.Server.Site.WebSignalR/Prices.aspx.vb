﻿'Imports Sites.SharedSiteCode
Imports Library.Public
Imports Dating.Server.Core.DLL

Public Class Prices
    Inherits BasePage


    'Dim _pageData As clsPageData
    'Protected ReadOnly Property CurrentPageData As clsPageData
    '    Get
    '        If (_pageData Is Nothing) Then
    '            _pageData = New clsPageData(Context)
    '            AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
    '        End If
    '        Return _pageData
    '    End Get
    'End Property


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'If Session("CustomerID") Is Nothing Then
        '    Response.Redirect("register.aspx")
        'End If

        If Len(Request.QueryString("choice")) Then
            Response.Redirect("selectPayment2.aspx?choice=" & Request.QueryString("choice"))
        End If

        If (Not Me.IsPostBack) Then
            If (Me.SessionVariables.MemberData.Country = "TR" OrElse (GetGeoCountry() = "TR" AndAlso String.IsNullOrEmpty(Me.SessionVariables.MemberData.Country))) Then
                ucProdTR.Visible = True
                ucProd.Visible = False
            Else
                ucProdTR.Visible = False
                ucProd.Visible = True
            End If
        End If

    End Sub

    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        ''If Not (IsPostBack) Then
        '
        'Dim LagID As String = Session("LagID")
        'Dim PageName As String = AppUtils.getPageFileNameFromURL(Request.Url.ToString())
        'cPageBasic = gLAG.GetPageBasics(LagID, PageName)

        ''End If
        'AppUtils.setNaviLabel(Me, "lbPublicNavi2Page", "Sign Up")
        'UCSelectProduct1.LoadLAG(Session("LagID"), Session("NoCache"))


        Try
            Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub

End Class
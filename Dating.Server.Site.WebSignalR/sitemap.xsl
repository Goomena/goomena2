<?xml version="1.0" encoding="UTF-8" ?> 
<xsl:stylesheet version="2.0" xmlns:html="http://www.w3.org/TR/REC-html40" xmlns:sitemap="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes" /> 
<xsl:template match="/">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Goomena.com</title> 
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">
body { font-family: Arial, sans-serif; font-size:13px; }
#intro { background-color:#CFEBF7; border:1px #2580B2 solid; padding:5px 13px 5px 13px; margin:10px; margin-top:40px; }
#intro p { line-height: 16.8667px; }
td { font-size:11px; }
th { text-align:left; padding-right:30px; font-size:11px; }
tr.high { background-color:whitesmoke; }
#footer { padding:2px; margin:10px; font-size:8pt; color:gray; }
#footer a { color:gray; } a { color:black; }
#content { padding: 30px; }
img, img a { border:none; }
</style>
</head>
<body>
  <h1>Goomena Spanish sitemap</h1> 
<div id="content">
<table cellpadding="5">
<tr style="border-bottom:1px black solid;">
<th>URL</th>
<th>LastChange</th>
<th>Change Frequency</th>
<th>Priority</th>

</tr>
<xsl:variable name="lower" select="'abcdefghijklmnopqrstuvwxyz'" /> 
<xsl:variable name="upper" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'" /> 
<xsl:for-each select="sitemap:urlset/sitemap:url">
<tr>
<xsl:if test="position() mod 2 != 1">
<xsl:attribute name="class">high</xsl:attribute> 
</xsl:if>
<td>
<xsl:variable name="itemURL">
<xsl:value-of select="sitemap:loc" /> 
</xsl:variable>
<a href="{$itemURL}">
<xsl:value-of select="sitemap:loc" /> 
</a>
</td>
<td>
<xsl:value-of select="concat(substring(sitemap:lastmod,0,11),concat(' ', substring(sitemap:lastmod,12,5)))" /> 
</td>
<td>
<xsl:value-of select="concat(translate(substring(sitemap:changefreq, 1, 1),concat($lower, $upper),concat($upper, $lower)),substring(sitemap:changefreq, 2))" /> 
</td>
<td>
<xsl:value-of select="concat(sitemap:priority*100,'%')" /> 
</td>
</tr>
</xsl:for-each>
</table>
</div>
<div id="footer">
<a href="http://www.brainpulse.com">Search Engine Optimisation</a> by BrainPulse
</div>
</body>
</html>
</xsl:template>
</xsl:stylesheet>

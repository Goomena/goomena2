﻿

Public Class UrlUtils


    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="_urlAbsolutePath">
    ''' it is an Absolute path of url.
    ''' Example /es/members/default.aspx
    ''' </param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetPageScope(_urlAbsolutePath As String) As PageScopeEnum
        Dim scope As PageScopeEnum = PageScopeEnum.None
        _urlAbsolutePath = _urlAbsolutePath.Trim("/")
        Dim urlParts As String() = _urlAbsolutePath.Split("/")
        If (urlParts.Length = 1) Then
            ' example url is /default.aspx
            scope = PageScopeEnum.PublicPage
        ElseIf (urlParts.Length >= 2) Then

            Dim Session As HttpSessionState = HttpContext.Current.Session
            Dim First_IsSupportedLAG As Boolean = clsLanguageHelper.IsSupportedLAG(urlParts(0), Session("GEO_COUNTRY_CODE"))

            If (urlParts.Length = 2 AndAlso First_IsSupportedLAG) Then
                ' example url is /es/default.aspx
                scope = PageScopeEnum.PublicPage

            ElseIf (urlParts.Length >= 2) Then

                If ((urlParts(0).IndexOf("members", System.StringComparison.OrdinalIgnoreCase) > -1) OrElse
                    (urlParts(1).IndexOf("members", System.StringComparison.OrdinalIgnoreCase) > -1)) Then

                    ' example url is /es/members/default.aspx
                    ' example url is /members/default.aspx
                    scope = PageScopeEnum.MembersPage

                ElseIf ((urlParts(0).IndexOf("cmspage", System.StringComparison.OrdinalIgnoreCase) > -1) OrElse
                        (urlParts(1).IndexOf("cmspage", System.StringComparison.OrdinalIgnoreCase) > -1) OrElse
                        (urlParts(0).IndexOf("landing", System.StringComparison.OrdinalIgnoreCase) > -1) OrElse
                        (urlParts(1).IndexOf("landing", System.StringComparison.OrdinalIgnoreCase) > -1)) Then

                    ' example url is /es/cmspage/196/sdasda
                    ' example url is /cmspage/196/sdasda
                    scope = PageScopeEnum.PublicPage

                End If

            Else
                scope = PageScopeEnum.OteherFolderContent
            End If

        End If
        Return scope
    End Function



    Public Shared Function GetPageScope(RequestUrl As Uri, routeData As Routing.RouteData) As PageScopeEnum
        Dim scope As PageScopeEnum = PageScopeEnum.None
        Dim _url As String = RequestUrl.AbsolutePath
        If (routeData.Values.Count > 0) Then
            _url = VirtualPathUtility.ToAbsolute(DirectCast(routeData.RouteHandler, System.Web.Routing.PageRouteHandler).VirtualPath)
        Else
            _url = RequestUrl.AbsolutePath
        End If
        scope = GetPageScope(_url)
        Return scope
    End Function


    Public Shared Function GetPageScope(Url As System.Uri) As PageScopeEnum
        Dim _url As String = Url.AbsolutePath
        Return GetPageScope(_url)
    End Function


    Public Shared Function IsPathInCurrentFolder(currentUrl As System.Uri, path As String) As Boolean
        If (String.IsNullOrEmpty(path)) Then Return True
        If (path = "#") Then Return True
        If (path.IndexOf("/") = -1) Then Return True
        If (path.IndexOf("javascript:") = 0) Then Return True

        If (path.IndexOf("?") > -1) Then path = path.Remove(path.IndexOf("?"))
        If (path.IndexOf("/") = 0) Then
            '
            'Dim pathUri As New Uri(path)
            Dim slashIndex As Integer = currentUrl.AbsolutePath.LastIndexOf("/")
            If (slashIndex > 0) Then
                Dim lcurrentUrl As String = currentUrl.AbsolutePath.Remove(slashIndex)

                Dim slashIndexPath As Integer = path.LastIndexOf("/")
                If (slashIndexPath > 0) Then
                    Dim lpath As String = path.Remove(slashIndexPath)
                    If (lcurrentUrl.Equals(lpath, System.StringComparison.OrdinalIgnoreCase)) Then Return True
                End If
            End If
        Else
            path = path.Trim("/")
            Dim urlParts As String() = path.Split("/")
            If (urlParts.Length = 0 OrElse urlParts.Length = 1) Then
                ' example url is /default.aspx
                Return True
            End If
        End If
        Return False
    End Function

    Public Shared Function GetFullURI(CurrentHostURL As String, href As String) As System.Uri
        Dim _url As String = CurrentHostURL
        If (_url.IndexOf("?") > -1) Then _url = _url.Remove(_url.IndexOf("?"))
        _url = _url.TrimEnd("/") &
            If(href.StartsWith("/"), "", "/") & href

        Dim _uri As New System.Uri(_url)
        Return _uri
    End Function


    Public Shared Function GetCurrentHostURL(RequestUrl As System.Uri) As String
        Dim currentHost As String = RequestUrl.AbsoluteUri
        If (RequestUrl.PathAndQuery.Length > 0) Then currentHost = currentHost.Remove(currentHost.IndexOf(RequestUrl.PathAndQuery))
        Return currentHost
    End Function

    Public Shared Function GetFixedRoutingURLParam(text As String) As String
        If (String.IsNullOrEmpty(text)) Then Return text
        Return text.Replace("/", "-").Replace("?", "-").Replace("&", "-").Replace(" ", "-").Replace(".", "-").Replace(",", "-").Replace(":", "-")
    End Function


    Public Shared Function GetHTTPSUrl(RequestUrl As System.Uri) As String
        Dim newUrl As String = RequestUrl.ToString()
        If (RequestUrl.Scheme.ToUpper() <> "HTTPS") Then
            newUrl = newUrl.Remove(0, "http://".Length)
            newUrl = "https://" & newUrl
        End If
        Return newUrl
    End Function


    Public Shared Function GetHTTPUrl(RequestUrl As System.Uri) As String
        Dim newUrl As String = RequestUrl.ToString()
        If (RequestUrl.Scheme.ToUpper() <> "HTTP") Then
            newUrl = newUrl.Remove(0, "https://".Length)
            newUrl = "http://" & newUrl
        End If
        Return newUrl
    End Function




    Public Shared Function GetFixedURLForRedirection(errorInfo As String, isFromFileNotFoundPage As Boolean) As String
        Dim redirectPermanentUrl As String = ""

        If (isFromFileNotFoundPage) Then
            ' if running from application_error method, throws 
            If (redirectPermanentUrl = "") Then
                Try
                    Dim usString As String = "%22/"
                    'Dim errorInfo As String = "http://localhost:61234/Default.aspx%22/" 'test string
                    If (errorInfo.IndexOf(usString, StringComparison.OrdinalIgnoreCase) > -1) Then
                        errorInfo = errorInfo.Replace(usString, "")
                        redirectPermanentUrl = errorInfo
                    End If
                Catch
                    Throw
                End Try
            End If

            If (redirectPermanentUrl = "") Then
                Try
                    Dim usString As String = """/"
                    'Dim errorInfo As String = "http://www.goomena.com:80/default.aspx""/" 'test string
                    If (errorInfo.IndexOf(usString, StringComparison.OrdinalIgnoreCase) > -1) Then
                        errorInfo = errorInfo.Replace(usString, "")
                        redirectPermanentUrl = errorInfo
                    End If
                Catch
                    Throw
                End Try
            End If
        End If

        If (redirectPermanentUrl = "") Then
            Try
                Dim usString As String = "/us/"
                'Dim errorInfo As String = "http://www.goomena.com:80/us/landing/cities/330" 'test string
                If (errorInfo.IndexOf(usString, StringComparison.OrdinalIgnoreCase) > 0) Then
                    Try
                        Dim url As New Uri(errorInfo)
                        If (url.AbsolutePath.StartsWith(usString, StringComparison.OrdinalIgnoreCase)) Then
                            ' fix URL  and redirect
                            redirectPermanentUrl = errorInfo.Replace(usString, "/")
                        End If
                    Catch ex As System.UriFormatException
                    End Try
                End If
            Catch
                Throw
            End Try
        End If

        If (redirectPermanentUrl = "") Then
            Try
                Dim cdnString As String = "/cdn.goomena.com/"
                'Dim errorInfo As String = "http://www.goomena.com:80/cdn.goomena.com/balance/media/widgetkit/widgets/slideshow/css/slideshow.css" 'test string
                Dim ndx As Integer = errorInfo.IndexOf(cdnString, StringComparison.OrdinalIgnoreCase)
                If (ndx > 0) Then
                    Try
                        Dim url As New Uri(errorInfo)
                        If (url.AbsolutePath.StartsWith(cdnString, StringComparison.OrdinalIgnoreCase)) Then
                            ' fix URL  and redirect
                            redirectPermanentUrl = url.Scheme & "://" & errorInfo.Remove(0, ndx).TrimStart({"/"c})
                        End If
                    Catch ex As System.UriFormatException
                    End Try
                End If
            Catch
                Throw
            End Try
        End If


        If (redirectPermanentUrl = "") Then
            Dim prfString As String = "/profile/"
            If (errorInfo.IndexOf(prfString, StringComparison.OrdinalIgnoreCase) > 0) Then
                ' fix URL and redirect
                Dim profile As String = errorInfo.Substring(errorInfo.IndexOf(prfString, StringComparison.OrdinalIgnoreCase) + prfString.Length)
                If (HasInvalidCharsForRouting(profile)) Then

                    Dim newUrl As String = errorInfo
                    'If (profile.Contains("+")) Then
                    '    newUrl = errorInfo.Replace(profile, profile.Replace("+", " "))
                    'End If
                    newUrl = newUrl.Replace(profile, UrlUtils.GetValidRoutingURI(profile))
                    redirectPermanentUrl = newUrl

                End If
            End If
        End If

        If (redirectPermanentUrl = "") Then
            Dim regString As String = "/register/"
            If (errorInfo.IndexOf(regString, StringComparison.OrdinalIgnoreCase) > 0) Then
                ' fix URL and redirect
                Dim newUrl As String = "/Register.aspx"
                If (HttpContext.Current.Session("LAGID") IsNot Nothing) Then
                    newUrl = "/Register.aspx?lag=" & HttpContext.Current.Session("LAGID")
                End If
                HttpContext.Current.Response.RedirectPermanent(newUrl)
            End If
        End If

        If (redirectPermanentUrl = "") Then
            Dim cmsString As String = "/cmspage/"
            If (errorInfo.IndexOf(cmsString, StringComparison.OrdinalIgnoreCase) > 0 AndAlso
                errorInfo.IndexOf("?retry", StringComparison.OrdinalIgnoreCase) = -1 AndAlso
                errorInfo.IndexOf("&retry", StringComparison.OrdinalIgnoreCase) = -1) Then

                'http://www.goomena.com/cmspage/395/Γνωριμίες%20με%20πανέμορφες%20γυναίκες%20στο%20Goomena.com

                ' fix URL and redirect
                Dim pageidStr As String = errorInfo.Substring(errorInfo.IndexOf(cmsString, StringComparison.OrdinalIgnoreCase) + cmsString.Length)
                pageidStr = pageidStr.Trim({"/"c})
                pageidStr = pageidStr.Remove(pageidStr.IndexOf("/"))

                Dim pageid As Integer
                If (Not String.IsNullOrEmpty(pageidStr) AndAlso Integer.TryParse(pageidStr, pageid) AndAlso pageid > 0) Then

                    Dim cmsPageName As String = errorInfo.Remove(0, (errorInfo.IndexOf(pageidStr, StringComparison.OrdinalIgnoreCase) + pageidStr.Length))
                    Dim newUrl As String = errorInfo.Remove(errorInfo.IndexOf(pageidStr, StringComparison.OrdinalIgnoreCase) + pageidStr.Length)

                    cmsPageName = cmsPageName.Trim({"/"c})
                    newUrl = newUrl.Trim({"/"c})

                    newUrl = clsMenuHelper.AdoptLanguageURL(newUrl, cmsPageName)
                    redirectPermanentUrl = newUrl
                End If

            End If
        End If

        Return redirectPermanentUrl
    End Function


    Public Shared Function CheckURLAndRedirectPermanent(errorInfo As String, UseThreadAbort As Boolean, isFromFileNotFoundPage As Boolean) As Boolean
        Dim isRedirecting As Boolean = False
        Dim redirectPermanentUrl As String = GetFixedURLForRedirection(errorInfo, isFromFileNotFoundPage)

        If (Not String.IsNullOrEmpty(redirectPermanentUrl)) Then
            If (UseThreadAbort) Then
                isRedirecting = True
                HttpContext.Current.Response.RedirectPermanent(redirectPermanentUrl)
            Else
                isRedirecting = True
                HttpContext.Current.Response.RedirectPermanent(redirectPermanentUrl, False)
                HttpContext.Current.ApplicationInstance.CompleteRequest()
            End If
        End If

        Return isRedirecting
    End Function


    Public Shared Function GetValidRoutingProfileURI(profile As String) As String
        'If (Not String.IsNullOrEmpty(profile)) Then
        '    profile = profile.Replace("+", "%20").Replace("<", "&lt;").Replace(">", "&gt;").Replace("%3C", "&gt;").Replace("%3E", "&gt;")
        'End If
        Return GetValidRoutingURI(profile)
    End Function

    Public Shared Function RevertValidProfileURI(profile As String) As String
        If (Not String.IsNullOrEmpty(profile)) Then
            profile = profile.
                Replace("_lt;", "<").
                Replace("_gt;", ">").
                Replace("_60;", "<").
                Replace("_62;", ">").
                Replace("_38;", "&").
                Replace("_63;", "?").
                Replace("_46;", ".").
                Replace("_34;", """").
                Replace("_35;", "#").
                Replace("_39;", "'").
                Replace("_42;", "*")
        End If
        Return profile
    End Function

    Public Shared Function GetValidRoutingURI(text As String) As String
        If (Not String.IsNullOrEmpty(text)) Then
            text = text.
                Replace("+", "%20").
                Replace("%3C", "_60;").
                Replace("%3E", "_62;").
                Replace("<", "_60;").
                Replace(">", "_62;").
                Replace("&", "_38;").
                Replace("?", "_63;").
                Replace("%3f", "_63;").
                Replace(".", "_46;").
                Replace("""", "_34;").
                Replace("#", "_35;").
                Replace("'", "_39;").
                Replace("*", "_42;")
        End If
        Return text
    End Function

    Public Shared Function HasInvalidCharsForRouting(text As String) As Boolean
        Dim b As Boolean = False
        If (Not String.IsNullOrEmpty(text)) Then
            b = b OrElse text.Contains("+")
            b = b OrElse text.Contains(">")
            b = b OrElse text.Contains("<")
            b = b OrElse text.Contains("%3C")
            b = b OrElse text.Contains("%3E")
            b = b OrElse text.Contains("&")
            b = b OrElse text.Contains("?")
            b = b OrElse text.Contains(".")
            b = b OrElse text.Contains("""")
            b = b OrElse text.Contains("#")
            b = b OrElse text.Contains("'")
            b = b OrElse text.Contains("*")
        End If
        Return b
    End Function




    Public Shared Function GetURLWithScheme(url As String) As String
        Dim Request As System.Web.HttpRequest = HttpContext.Current.Request

        If (url.StartsWith("http://", StringComparison.OrdinalIgnoreCase) AndAlso
            Request.Url.Scheme.IndexOf("http", StringComparison.OrdinalIgnoreCase) > -1) Then
            ' do nothing

        ElseIf (url.StartsWith("https://", StringComparison.OrdinalIgnoreCase) AndAlso
            Request.Url.Scheme.IndexOf("https", StringComparison.OrdinalIgnoreCase) > -1) Then
            ' do nothing

        Else
            ' do nothing
            If (url.IndexOf("https://", StringComparison.OrdinalIgnoreCase) = 0) Then
                url = url.Remove(0, "https:".Length)
            ElseIf (url.IndexOf("http://", StringComparison.OrdinalIgnoreCase) = 0) Then
                url = url.Remove(0, "http:".Length)
            End If
            url = Request.Url.Scheme & ":" & url
        End If
        Return url
    End Function


End Class

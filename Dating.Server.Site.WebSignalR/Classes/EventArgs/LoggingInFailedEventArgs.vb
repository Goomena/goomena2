﻿Public Class LoggingInFailedEventArgs
    Inherits System.EventArgs

    Public Property LogginFailReason As LogginFailReasonEnum
    Public Property ErrorMessage As String
    Public Property Handled As Boolean

    Public Sub New()
        MyBase.New()
    End Sub

    Public Sub New(ByVal _reason As LogginFailReasonEnum, ByVal _errorMessage As String)
        MyBase.New()
        LogginFailReason = _reason
        ErrorMessage = _errorMessage
    End Sub

End Class

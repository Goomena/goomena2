﻿Imports Dating.Server.Core.DLL

Public Class clsQuickSearchItem
    Public Sub New()
        ItemIndex = DateTime.UtcNow.Ticks
    End Sub

    Public Property ItemIndex As Long
    Public Property ProfileID As Integer
    Public Property MirrorProfileID As Integer
    Public Property LoginName As String


    Public Property OtherMemberLoginName As String
    Public Property OtherMemberProfileID As Integer
    Public Property OtherMemberMirrorProfileID As Integer


    Public Property OtherMemberCity As String
    Public Property OtherMemberRegion As String
    Public Property OtherMemberCountry As String
    Public Property OtherMemberImageFileName As String
    Public Property OtherMemberImageUrl As String
    Public Property OtherMemberImageThumbUrl As String
    Public Property OtherMemberProfileViewUrl As String
    Public Property OtherMemberIsOnline As Boolean



    Public Property YearsOldText As String


    Public Property PhotoCssClass As String
    Public Property PhotosCountString As String


    Public Property OtherMemberRelationshipTooltip As String
    Public Property OtherMemberRelationshipClass As String
    Public Property OtherMemberBirthday As DateTime?
    Public Property OtherMemberHairClass As String
    Public Property OtherMemberHairTooltip As String
    Public Property OtherMemberBreastSizeClass As String
    Public Property OtherMemberBreastSizeTooltip As String
    Public Property ZodiacString As String

    Private __ZodiacName As String
    Public ReadOnly Property ZodiacName As String
        Get
            If (Me.OtherMemberBirthday.HasValue AndAlso Me.OtherMemberBirthday.Value > DateTime.MinValue) Then
                If (__ZodiacName Is Nothing) Then __ZodiacName = "Zodiac" & ProfileHelper.ZodiacName(Me.OtherMemberBirthday)
                If (__ZodiacName Is Nothing) Then __ZodiacName = String.Empty
            End If
            Return __ZodiacName
        End Get
    End Property






    Public Sub FillTextResourceProperties(pageData As clsPageData)



    End Sub




End Class



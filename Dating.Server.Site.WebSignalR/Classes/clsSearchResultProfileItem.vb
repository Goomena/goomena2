﻿Imports Dating.Server.Core.DLL

Public Class clsSearchResultProfileItem
    Public Sub New()
        ItemIndex = DateTime.UtcNow.Ticks
    End Sub
    Public Property IsOdd As Boolean
    Public Property ItemIndex As Long
    Public Property AddItemCssClass As String

    Public Property ProfileID As Integer
    Public Property MirrorProfileID As Integer
    Public Property LoginName As String
    Public Property Genderid As Integer
    Public Property ProfileViewUrl As String
    Public Property ImageFileName As String
    Public Property ImageUrl As String
    Public Property ImageThumbUrl As String
    Public Property ImageUploadDate As String

    Public Property OtherMemberLoginName As String
    Public Property OtherMemberProfileID As Integer
    Public Property OtherMemberMirrorProfileID As Integer
    Public Property OtherMemberGenderid As Integer
    Public Property OtherMemberAge As Integer
    Public Property OtherMemberCity As String
    Public Property OtherMemberRegion As String
    Public Property OtherMemberCountry As String
    Public Property OtherMemberHeading As String
    Public Property OtherMemberHeight As String
    Public Property OtherMemberPersonType As String
    Public Property OtherMemberHair As String
    Public Property OtherMemberHairClass As String
    Public Property OtherMemberHairTooltip As String
    Public Property OtherMemberEyes As String
    Public Property OtherMemberEthnicity As String
    Public Property OtherMemberImageFileName As String
    Public Property OtherMemberImageUrl As String
    Public Property OtherMemberImageThumbUrl As String
    Public Property OtherMemberProfileViewUrl As String
    Public Property OtherMemberIsOnline As Boolean
    Public Property OtherMemberIsOnlineRecently As Boolean
    Public Property OtherMemberIsOnlineText As String
    Public Property OtherMemberIsVip As Boolean
    Public Property OtherMemberDrinkingText As String
    Public Property OtherMemberDrinkingClass As String
    Public Property OtherMemberSmokingText As String
    Public Property OtherMemberSmokingClass As String
    Public Property OtherMemberRelationshipTooltip As String
    Public Property OtherMemberRelationshipClass As String
    Public Property OtherMemberWantVisitText As String
    Public Property OtherMemberWantJobTooltip As String
    Public Property OtherMemberBreastSizeClass As String
    Public Property OtherMemberBreastSizeTooltip As String

    Public Property OtherMemberBirthday As DateTime?
    Public Property OtherMemberIsMale As Boolean
    Public Property OtherMemberIsFemale As Boolean
    Public Property OtherMemberGenderClass As String
    Public Property OtherMemberShowInfoIcons As Boolean

    Public Property Distance As String
    Public Property DistanceCss As String
    Public Property CreateOfferUrl As String
    Public Property RejectOfferUrl As String
    Public Property RejectCssClass As String
    Public Property RejectedByProfileID As Integer

    Public Property OfferID As Integer
    Public Property OfferAmount As Integer

    Public Property IsWink As Boolean
    'Public Property IsCounter As Boolean
    Public Property IsOffer As Boolean
    Public Property IsPoke As Boolean

    Public Property AllowWink As Boolean
    Public Property AllowUnWink As Boolean

    Public Property AllowFavorite As Boolean
    Public Property AllowUnfavorite As Boolean

    Public Property AllowAccept As Boolean
    Public Property AllowCounter As Boolean
    Public Property AllowPoke As Boolean
    Public Property AllowCreateOffer As Boolean
    Public Property AllowCancelWink As Boolean
    Public Property AllowCancelPendingOffer As Boolean
    Public Property AllowDeletePendingOffer As Boolean

    Public Property AllowTryWink As Boolean
    Public Property AllowDeleteWink As Boolean
    Public Property AllowDeleteOffer As Boolean



    Public Property AllowUnblock As Boolean
    'Public Property UnblockText As String



    Public Property NoOfferText As String

    ' Public Property HasNoPhotosText As String
    ' Public Property SearchOurMembersText As String
    Public Property ForDateWithText As String
    ' Public Property YouReceivedWinkOfferText As String
    ' Public Property WantToKnowFirstDatePriceText As String
    ' Public Property MakeOfferText As String
    '  Public Property AcceptText As String
    ' Public Property CounterText As String
    ' Public Property RejectText As String
    '  Public Property NotInterestedText As String
    'Public Property TooFarAwayText As String
    ' Public Property NotEnoughInfoText As String
    ' Public Property DifferentExpectationsText As String
    ' Public Property YearsOldText As String
    Public Property MilesAwayText As String
    '  Public Property DeleteOfferText As String
    ' Public Property SendMessageText As String
    ' Public Property YearsOldFromText As String
    'Public Property TryWinkText As String
    ' Public Property MakeNewOfferText As String
    ' Public Property YourWinkSentText As String
    ' Public Property AwaitingResponseText As String

    ' Public Property WinkText As String
    ' Public Property UnWinkText As String

    ' Public Property FavoriteText As String
    ' Public Property UnfavoriteText As String

    ' Public Property AddPhotosText As String
    Public Property SendMessageUrl As String
    Public Property WillYouAcceptDateWithForAmountText As String
    'Public Property ProposeNewOfferText As String


    Public Property OfferAcceptedWithAmountText As String
    Public Property OfferAcceptedHowToContinueText As String
    Public Property OfferAcceptedUnlockText As String
    Public Property AllowSendMessage As Boolean
    Public Property AllowDeleteAcceptedOffer As Boolean
    Public Property AllowOfferAcceptedUnlock As Boolean

    Public Property Credits As String
    Public Property OfferDeleteConfirmMessage As String

    '  Public Property CancelledRejectedTitleText As String
    Public Property CancelledRejectedDescriptionText As String
    'Public Property CancelWinkText As String
    '  Public Property CancelOfferText As String
    ' Public Property DeleteWinkText As String
    'Public Property PokeText As String

    Public Property UnlockNotice As String
    Public Property AllowTooltipUnlockNotice As Boolean

    Public Property YouReceivedPokeDescriptionText As String


    Public Property AllowActionsMenu As Boolean
    Public Property AllowRejectsMenu As Boolean
    Public Property AllowActionsUnlock As Boolean
    Public Property AllowActionsCreateOffer As Boolean

    ' Public Property ActionsText As String
    Public Property ActionsUnlockText As String
    Public Property ActionsMakeOfferText As String

    Public Property AllowActionsSendMessage As Boolean
    Public Property AllowActionsDeleteOffer As Boolean

    Public Property AllowCancelPoke As Boolean
    '  Public Property CancelPokeText As String


    Public Property IsMessageSent As Boolean?
    ' Public Property MessageSentNotice As String


    Public Property AllowWhatIs As Boolean
    '  Public Property WhatIsText As String
    Public Property AllowTooltipPopupLikes As Boolean?
    Public Property AllowTooltipPopupSearch As Boolean

    Public Property AllowActionsPoke As Boolean

    Public Property AllowActionsSendMessageOnce As Boolean
    Public Property SendMessageUrlOnce As String
    Public Property SendMessageOnceText As String


    Public Property AllowActionsSendMessageMany As Boolean
    Public Property SendMessageUrlMany As String
    Public Property SendMessageManyText As String


    Public Property AllowConversation As Boolean
    '  Public Property ConversationText As String
    Public Property ConversationNavigateUrl As String

    Public Property AllowHistory As Boolean
    ' Public Property HistoryText As String
    Public Property HistoryNavigateUrl As String

    Public Property RejectDeleteConversationText As String
    Public Property RejectBlockText As String

    '  Public Property CommunicationStatusText As String
    Public Property CommunicationStatusLimitedText As String
    Public Property CommunicationStatusUnlimitedText As String

    'Public Property FirstDateText As String
    Public Property LastDatingText As String
    Public Property LastDatingDateText As String

    Public Property CreatedDate As DateTime?

    Public Property ItsCurrentProfileAction As Boolean
    Public Property HasCommunication As Boolean?

    Public Property LAGID As String

    Public Property PhotoCssClass As String
    Public Property PhotosCountString As String

    Public Property RowNumber As Integer

    Private _PhotosProperties() As PhotoImagesProperties = {New PhotoImagesProperties, New PhotoImagesProperties, New PhotoImagesProperties, New PhotoImagesProperties, New PhotoImagesProperties}
    Public ReadOnly Property PhotosProperties As PhotoImagesProperties()
        Get
            Return _PhotosProperties
        End Get

    End Property
    ''' <summary>
    ''' Replaces following tookens ###LOGINNAME###,###AMOUNT###,###CREDITS###,###DISTANCE###
    ''' </summary>
    ''' <param name="text"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ReplaceCommonTookens(text As String) As String
        If (Not String.IsNullOrEmpty(text)) Then

            If (text.IndexOf("###LOGINNAME###") > -1) Then text = text.Replace("###LOGINNAME###", "<span class=""login-name"">" & Me.OtherMemberLoginName & "</span>")
            If (text.IndexOf("###AMOUNT###") > -1) Then text = text.Replace("###AMOUNT###", "&euro;" & Me.OfferAmount.ToString())
            If (text.IndexOf("###CREDITS###") > -1) Then text = text.Replace("###CREDITS###", Me.Credits)
            If (text.IndexOf("###UNLOCK_MESSAGE_SEND_CRD###") > -1) Then text = text.Replace("###UNLOCK_MESSAGE_SEND_CRD###", ProfileHelper.Config_UNLOCK_MESSAGE_SEND_CREDITS)
            If (text.IndexOf("###UNLOCK_MESSAGE_READ_CRD###") > -1) Then text = text.Replace("###UNLOCK_MESSAGE_READ_CRD###", ProfileHelper.Config_UNLOCK_MESSAGE_READ_CREDITS)
            If (text.IndexOf("###UNLOCK_CONVERSATIONCRD###") > -1) Then text = text.Replace("###UNLOCK_CONVERSATIONCRD###", ProfileHelper.Config_UNLOCK_CONVERSATION_CREDITS)


            If (text.IndexOf("###DISTANCE###") > -1) Then

                Try
                    If (Len(Me.Distance) > 0) Then
                        Dim ndx As Integer = Me.Distance.IndexOfAny(New Char() {","c, "."c})
                        If (ndx > -1) Then

                            Dim dst As String = Me.Distance.Remove(ndx)
                            Me.Distance = dst

                            Dim c As Integer
                            If (Integer.TryParse(Me.Distance, c)) Then
                                Me.Distance = c + 1
                            End If
                        End If
                    End If

                Catch ex As Exception
                End Try

                text = text.Replace("###DISTANCE###", Me.Distance)
            End If

        End If
        Return text
    End Function


    Public Shared Function ReplaceTokens(text As String, OtherMemberLoginName As String, OfferAmount As Integer, Credits As String, Distance As String) As String
        If (Not String.IsNullOrEmpty(text)) Then
            text = text.Replace("###LOGINNAME###", OtherMemberLoginName)
            text = text.Replace("###AMOUNT###", "&euro;" & OfferAmount.ToString())
            text = text.Replace("###CREDITS###", Credits)
            text = text.Replace("###DISTANCE###", Distance)
        End If
        Return text
    End Function
    Private Sub ReplaceKeysWithVakues()
        If (String.IsNullOrEmpty(Me.ActionsMakeOfferText)) Then Me.ActionsMakeOfferText = Me.ReplaceCommonTookens(Me.ActionsMakeOfferText)
        If (String.IsNullOrEmpty(Me.RejectDeleteConversationText)) Then Me.RejectDeleteConversationText = Me.ReplaceCommonTookens(Me.RejectDeleteConversationText)
        If (String.IsNullOrEmpty(Me.RejectBlockText)) Then Me.RejectBlockText = Me.ReplaceCommonTookens(Me.RejectBlockText)
        If (String.IsNullOrEmpty(Me.CancelledRejectedDescriptionText)) Then Me.CancelledRejectedDescriptionText = Me.ReplaceCommonTookens(Me.CancelledRejectedDescriptionText)
        If (String.IsNullOrEmpty(Me.MilesAwayText)) Then Me.MilesAwayText = Me.ReplaceCommonTookens(Me.MilesAwayText)
        If (String.IsNullOrEmpty(Me.OfferDeleteConfirmMessage)) Then Me.OfferDeleteConfirmMessage = Me.OfferDeleteConfirmMessage.Replace("'", "\'")
        If (String.IsNullOrEmpty(Me.OfferDeleteConfirmMessage)) Then Me.OfferDeleteConfirmMessage = "return confirm('" & Me.OfferDeleteConfirmMessage & "');"
        If (String.IsNullOrEmpty(Me.YouReceivedPokeDescriptionText)) Then Me.YouReceivedPokeDescriptionText = Me.ReplaceCommonTookens(Me.YouReceivedPokeDescriptionText)
        If (String.IsNullOrEmpty(Me.WillYouAcceptDateWithForAmountText)) Then Me.WillYouAcceptDateWithForAmountText = Me.ReplaceCommonTookens(Me.WillYouAcceptDateWithForAmountText)
        If (String.IsNullOrEmpty(Me.OfferAcceptedHowToContinueText)) Then Me.OfferAcceptedHowToContinueText = Me.ReplaceCommonTookens(Me.OfferAcceptedHowToContinueText)
        If (String.IsNullOrEmpty(Me.OfferAcceptedUnlockText)) Then Me.OfferAcceptedUnlockText = Me.ReplaceCommonTookens(Me.OfferAcceptedUnlockText)
        If (String.IsNullOrEmpty(Me.ActionsUnlockText)) Then Me.ActionsUnlockText = Me.ReplaceCommonTookens(Me.ActionsUnlockText)
        If (String.IsNullOrEmpty(Me.UnlockNotice)) Then Me.UnlockNotice = Me.ReplaceCommonTookens(Me.UnlockNotice)
        If (String.IsNullOrEmpty(Me.SendMessageOnceText)) Then Me.SendMessageOnceText = Me.ReplaceCommonTookens(Me.SendMessageOnceText)
        If (String.IsNullOrEmpty(Me.SendMessageManyText)) Then Me.SendMessageManyText = Me.ReplaceCommonTookens(Me.SendMessageManyText)
    End Sub


    Public Sub FillTextResourceProperties(pageData As KeyStrings)
        If (String.IsNullOrEmpty(Me.SendMessageOnceText)) Then Me.SendMessageOnceText = pageData.SendMessageOnceText
        If (String.IsNullOrEmpty(Me.SendMessageManyText)) Then Me.SendMessageManyText = pageData.SendMessageManyText
        If (String.IsNullOrEmpty(Me.UnlockNotice)) Then Me.UnlockNotice = pageData.UnlockNotice
        If (String.IsNullOrEmpty(Me.ActionsUnlockText)) Then Me.ActionsUnlockText = pageData.ActionsUnlockText
        If (String.IsNullOrEmpty(Me.OfferAcceptedUnlockText)) Then Me.OfferAcceptedUnlockText = pageData.OfferAcceptedUnlockText
        If (String.IsNullOrEmpty(Me.OfferAcceptedHowToContinueText)) Then Me.OfferAcceptedHowToContinueText = pageData.OfferAcceptedHowToContinueText
        If (String.IsNullOrEmpty(Me.WillYouAcceptDateWithForAmountText)) Then Me.WillYouAcceptDateWithForAmountText = pageData.WillYouAcceptDateWithForAmountText
        If (String.IsNullOrEmpty(Me.YouReceivedPokeDescriptionText)) Then Me.YouReceivedPokeDescriptionText = pageData.YouReceivedPokeDescriptionText
        If (String.IsNullOrEmpty(Me.OfferDeleteConfirmMessage)) Then Me.OfferDeleteConfirmMessage = pageData.OfferDeleteConfirmMessage
        If (String.IsNullOrEmpty(Me.MilesAwayText)) Then Me.MilesAwayText = pageData.MilesAwayText
        If (String.IsNullOrEmpty(Me.CancelledRejectedDescriptionText)) Then Me.CancelledRejectedDescriptionText = pageData.CancelledRejectedDescriptionText
        If (String.IsNullOrEmpty(Me.RejectBlockText)) Then Me.RejectBlockText = pageData.RejectBlockText
        If (String.IsNullOrEmpty(Me.RejectDeleteConversationText)) Then Me.RejectDeleteConversationText = pageData.RejectDeleteConversationText
        If (String.IsNullOrEmpty(Me.RejectDeleteConversationText)) Then Me.RejectDeleteConversationText = pageData.RejectDeleteConversationText
        ReplaceKeysWithVakues()
        If (OtherMemberIsOnline) Then OtherMemberIsOnlineText = pageData.IsOnlinetext
        If (OtherMemberIsOnlineRecently) Then OtherMemberIsOnlineText = pageData.IsOnlineRecentlyText
        If (Not Me.AllowTooltipPopupLikes.HasValue) Then Me.AllowTooltipPopupLikes = True

        If (Me.RejectedByProfileID = Me.OtherMemberProfileID) Then
            Me.RejectCssClass = "act-right"
        Else
            Me.RejectCssClass = "act-left"
        End If



        If (Not String.IsNullOrEmpty(Me.OtherMemberHeading)) Then
            If (Len(Me.OtherMemberHeading) > 60) Then
                Me.OtherMemberHeading = Me.OtherMemberHeading.Remove(57) & "..."
            End If
        End If

        If (OtherMemberRelationshipTooltip IsNot Nothing AndAlso OtherMemberRelationshipTooltip.Length > 0) Then OtherMemberShowInfoIcons = True
        If (OtherMemberHairClass IsNot Nothing AndAlso OtherMemberHairClass.Length > 0) Then OtherMemberShowInfoIcons = True
        If (OtherMemberWantVisitText IsNot Nothing AndAlso OtherMemberWantVisitText.Length > 0) Then OtherMemberShowInfoIcons = True
        If (OtherMemberWantJobTooltip IsNot Nothing AndAlso OtherMemberWantJobTooltip.Length > 0) Then OtherMemberShowInfoIcons = True
        If (OtherMemberDrinkingText IsNot Nothing AndAlso OtherMemberDrinkingText.Length > 0) Then OtherMemberShowInfoIcons = True
        If (OtherMemberSmokingText IsNot Nothing AndAlso OtherMemberSmokingText.Length > 0) Then OtherMemberShowInfoIcons = True
        If (OtherMemberBreastSizeTooltip IsNot Nothing AndAlso OtherMemberBreastSizeTooltip.Length > 0) Then OtherMemberShowInfoIcons = True
        If (OtherMemberIsVip) Then OtherMemberShowInfoIcons = True


    End Sub

    Public Property ZodiacString As String

    Private __ZodiacName As String
    Public ReadOnly Property ZodiacName As String
        Get
            If (Me.OtherMemberBirthday.HasValue AndAlso Me.OtherMemberBirthday.Value > DateTime.MinValue) Then
                If (__ZodiacName Is Nothing) Then __ZodiacName = "Zodiac" & ProfileHelper.ZodiacName(Me.OtherMemberBirthday)
                If (__ZodiacName Is Nothing) Then __ZodiacName = String.Empty
            End If
            Return __ZodiacName
        End Get
    End Property

End Class
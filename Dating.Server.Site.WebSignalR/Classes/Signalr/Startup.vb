﻿Imports Microsoft.Owin
Imports Owin
Imports Microsoft.AspNet.SignalR
Imports Microsoft.AspNet.SignalR.SqlServer
<Assembly: OwinStartup(GetType(Startup))> 
Public Class Startup
    Public Sub Configuration(app As IAppBuilder)
        ' Any connection or hub wire up and configuration should go here
        GlobalHost.DependencyResolver.UseSqlServer(System.Configuration.ConfigurationManager.ConnectionStrings("AppDBconnectionString").ConnectionString)
        app.MapSignalR()
    End Sub
End Class


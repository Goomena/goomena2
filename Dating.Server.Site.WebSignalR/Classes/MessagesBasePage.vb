Imports Dating.Server.Core.DLL



Partial Public Class MessagesBasePage
    Inherits BasePage

    Protected Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            'If (_pageData Is Nothing) Then _pageData = New clsPageData("~/Members/Messages.aspx", Context)
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData("~/Members/Messages.aspx", Context, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property

   
    Protected __freeMessagesHelper As clsFreeAndSubscriptionMessagesHelper
    Protected ReadOnly Property freeMessagesHelper As clsFreeAndSubscriptionMessagesHelper
        Get
            If (__freeMessagesHelper Is Nothing) Then
                __freeMessagesHelper = New clsFreeAndSubscriptionMessagesHelper(Me.MasterProfileId,
                                                               Me.SessionVariables.MemberData.Country)

            End If
            Return __freeMessagesHelper
        End Get
    End Property



    Private Sub MessagesBasePage_Unload(sender As Object, e As EventArgs) Handles Me.Unload
        If (__freeMessagesHelper IsNot Nothing) Then __freeMessagesHelper.Dispose()
    End Sub


End Class



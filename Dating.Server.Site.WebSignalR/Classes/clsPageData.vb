﻿Imports Dating.Server.Core.DLL
Imports Dating.Server.Core.DLL.clsSiteLAG

''' <summary>
''' v 0.4
''' </summary>
''' <remarks></remarks>
Public Class clsPageData

    Public Enum CacheOptionsEnum
        None = 0
        DisabledCacheForPageBasics = 1
        DisabledCacheForContent = 2
    End Enum


#Region "caching related fuynctionality"

    Public Class CachedDatatable
        Public DtStrings As New DataTable()
        Public TableSize As Integer
        Public LastAccess As DateTime = Date.UtcNow
    End Class
    Public Shared gLAG As New Core.DLL.clsSiteLAG()
    Private Shared Property gDataList_Pages As New Dictionary(Of String, clsPageBasicReturn)
    Private Shared Property gDataList_PageCustoms As New Dictionary(Of String, CachedDatatable)
    Private Shared Property LastCheckCustoms As DateTime = DateTime.UtcNow.AddMinutes(2)
    Private Shared Property LastCheckPages As DateTime = DateTime.UtcNow.AddMinutes(2)
    Public Shared Function GetCachedDataTable_PageCustoms(pageName As String) As DataTable
        Dim dtStrings As DataTable = Nothing

        Try
            If (Not String.IsNullOrEmpty(pageName) AndAlso gDataList_PageCustoms.ContainsKey(pageName)) Then
                dtStrings = gDataList_PageCustoms(pageName).DtStrings
                gDataList_PageCustoms(pageName).LastAccess = DateTime.UtcNow
            End If
            If LastCheckCustoms < DateTime.UtcNow Then
                LastCheckCustoms = DateTime.UtcNow.AddMinutes(2)
                For cnt = 0 To gDataList_PageCustoms.Keys.Count - 1
                    Try
                        Dim key As String = gDataList_PageCustoms.Keys(cnt)
                        Dim mins As Integer = (DateTime.UtcNow - gDataList_PageCustoms(key).LastAccess).TotalMinutes
                        If (gDataList_PageCustoms(key).TableSize > 2048 AndAlso mins > 1) Then

                            'if the size of BodyHTM column is greater than 2kb
                            'remove that item

                            gDataList_PageCustoms.Remove(key)
                            Exit For

                        ElseIf (mins > 3) Then

                            'remove one item at a time
                            gDataList_PageCustoms.Remove(key)
                            Exit For

                        End If

                    Catch e As System.Collections.Generic.KeyNotFoundException
                    Catch ex As Exception
                    End Try
                Next
                LastCheckCustoms = DateTime.UtcNow.AddMinutes(2)
            End If
           
        Catch ex As Exception

        End Try

        Return dtStrings
    End Function
    Public Shared Sub SetCachedDataTable_PageCustoms(pageName As String, dtStrings As DataTable)
        Try
            If (Not String.IsNullOrEmpty(pageName) AndAlso Not gDataList_PageCustoms.ContainsKey(pageName)) Then
                Dim cch As New CachedDatatable()
                cch.DtStrings = dtStrings
                gDataList_PageCustoms.Add(pageName, cch)
            End If
        Catch ex As Exception
        End Try
    End Sub
    Public Shared Function GetCachedDataTable_Pages(pageNameAndLang As String) As clsPageBasicReturn
        Dim dtStrings As clsPageBasicReturn = Nothing
        Try
            If (Not String.IsNullOrEmpty(pageNameAndLang) AndAlso gDataList_Pages.ContainsKey(pageNameAndLang)) Then
                dtStrings = gDataList_Pages(pageNameAndLang)
                gDataList_Pages(pageNameAndLang).LastAccess = DateTime.UtcNow
            End If
            If LastCheckPages < DateTime.UtcNow Then
                LastCheckPages = DateTime.UtcNow.AddMinutes(2)
                If gDataList_PageCustoms.Keys.Count > 0 Then
                    For cnt = 0 To gDataList_Pages.Keys.Count - 1
                        Try
                            Dim key As String = gDataList_Pages.Keys(cnt)
                            Dim mins As Integer = (DateTime.UtcNow - gDataList_Pages(key).LastAccess).TotalMinutes
                            If (gDataList_Pages(key).BodyHTMSize > 2048 AndAlso mins > 1) Then
                                'if the size of BodyHTM column is greater than 2kb
                                'remove that item
                                gDataList_Pages.Remove(key)
                                Exit For
                            ElseIf (mins > 3) Then
                                'remove one item at a time
                                gDataList_Pages.Remove(key)
                                Exit For
                            End If
                        Catch e As System.Collections.Generic.KeyNotFoundException
                        Catch ex As Exception
                        End Try
                    Next
                End If
                LastCheckPages = DateTime.UtcNow.AddMinutes(2)
            End If

        Catch ex As Exception
        End Try
        Return dtStrings
    End Function


    Public Shared Sub SetCachedDataTable_Pages(pageName As String, dtStrings As clsPageBasicReturn)
        Try
            If (Not String.IsNullOrEmpty(pageName) AndAlso Not gDataList_Pages.ContainsKey(pageName)) Then
                gDataList_Pages.Add(pageName, dtStrings)
            End If
        Catch ex As Exception
        End Try
    End Sub
    Public Shared Sub ClearCache()
        Try
            gDataList_PageCustoms.Clear()
        Catch
        End Try
        Try
            gDataList_Pages.Clear()
        Catch
        End Try
    End Sub
#End Region

    Public Property IsQueryStringParamDetected As Boolean
    Public Property CacheDisabledForPageBasics As Boolean
    Public Property CacheDisabledForContent As Boolean

    Dim _SitePageID As Long = -1
    Public ReadOnly Property SitePageID As Long
        Get
            Return _SitePageID
        End Get
    End Property


    Dim _PageName As String
    Public ReadOnly Property PageName As String
        Get
            If (_PageName Is Nothing) Then _PageName = String.Empty
            Return _PageName
        End Get
    End Property


    Dim _Title As String
    Public ReadOnly Property Title As String
        Get
            If (_Title Is Nothing) Then _Title = String.Empty
            Return _Title
        End Get
    End Property


    Dim _LagID As String
    Public ReadOnly Property LagID As String
        Get
            If (_LagID Is Nothing) Then _LagID = String.Empty
            Return _LagID
        End Get
    End Property


    Dim _cPageBasic As clsSiteLAG.clsPageBasicReturn
    Public ReadOnly Property cPageBasic As clsSiteLAG.clsPageBasicReturn
        Get
            Return _cPageBasic
        End Get
    End Property


    ' Dim _keysDictionary As Dictionary(Of String, String)
    ' Dim _keysDictionaryHTTPS As Dictionary(Of String, String)
    Dim _dtStrings As DataTable

    Private __context As HttpContext
    Private ReadOnly Property Session As HttpSessionState
        Get
            Return __context.Session
        End Get
    End Property
    Private ReadOnly Property Request As HttpRequest
        Get
            Return __context.Request
        End Get
    End Property
    Private ReadOnly Property Response As HttpResponse
        Get
            Return __context.Response
        End Get
    End Property
    Private ReadOnly Property Server As HttpServerUtility
        Get
            Return __context.Server
        End Get
    End Property


    Private __Language As clsLanguageHelper
    Protected ReadOnly Property LanguageHelper As clsLanguageHelper
        Get
            If (__Language Is Nothing) Then __Language = New clsLanguageHelper(__context)
            Return __Language
        End Get
    End Property


    'Private Shared _globalStrings As clsPageData
    'Public Shared ReadOnly Property globalStrings As clsPageData
    '    Get
    '        If (_globalStrings Is Nothing) Then
    '            _globalStrings = New clsPageData("GlobalStrings", HttpContext.Current)
    '        End If
    '        Return _globalStrings
    '    End Get
    'End Property


    Public Event CustomStringRetrievalComplete(ByRef sender As clsPageData, ByRef Args As CustomStringRetrievalCompleteEventArgs)

    Public Sub New(ByRef context As HttpContext)
        Me.New(context, CacheOptionsEnum.None)
    End Sub

    Public Sub New(ByRef context As HttpContext, cacheOptions As clsPageData.CacheOptionsEnum)
        __context = context
        If ((cacheOptions And clsPageData.CacheOptionsEnum.DisabledCacheForPageBasics) > 0) Then
            Me.CacheDisabledForPageBasics = True
        End If
        If ((cacheOptions And clsPageData.CacheOptionsEnum.DisabledCacheForContent) > 0) Then
            Me.CacheDisabledForContent = True
        End If

        If context.Request("PageId") IsNot Nothing Then
            _SitePageID = If(IsNumeric(Trim(context.Request("PageId"))), Trim(context.Request("PageId")), -1)
        End If

        If context.Request("PageName") IsNot Nothing Then
            _PageName = Trim(context.Request("PageName"))
        End If

        '  Dim sessVars As clsSessionVariables = 
        clsSessionVariables.GetCurrent()
        If HttpContext.Current.Session("LagID") IsNot Nothing Then
            _LagID = HttpContext.Current.Session("LagID")
        Else
            _LagID = "US"
        End If



        If String.IsNullOrEmpty(_PageName) Then
            _PageName = context.Request.AppRelativeCurrentExecutionFilePath
            _PageName = _PageName.TrimStart("~"c).TrimStart("/"c)
        End If

        _cPageBasic = GetPageBasics(_LagID)
        If (_cPageBasic Is Nothing) Then Throw New PageBasicNotIntializedException()

        _SitePageID = _cPageBasic.SitePageID
        _Title = _cPageBasic.PageTitle
        If String.IsNullOrEmpty(_Title) Then
            If context.Request("Title") IsNot Nothing Then
                _Title = Trim(context.Request("Title"))
            End If
        End If

    End Sub


    Public Sub New(ByVal pageName As String, ByRef context As HttpContext)
        Me.New(pageName, context, CacheOptionsEnum.None)
    End Sub


    Public Sub New(ByVal pageName As String, ByRef context As HttpContext, cacheOptions As clsPageData.CacheOptionsEnum)
        __context = context
        _PageName = pageName

        If ((cacheOptions And clsPageData.CacheOptionsEnum.DisabledCacheForPageBasics) > 0) Then
            Me.CacheDisabledForPageBasics = True
        End If
        If ((cacheOptions And clsPageData.CacheOptionsEnum.DisabledCacheForContent) > 0) Then
            Me.CacheDisabledForContent = True
        End If

        If Not context.Request("Title") Is Nothing Then
            _Title = Trim(context.Request("Title"))
        End If

        If Not context.Session("LagID") Is Nothing Then
            _LagID = context.Session("LagID")
        Else
            _LagID = "US"
        End If

        _cPageBasic = GetPageBasics(_LagID)
        If (_cPageBasic Is Nothing) Then Throw New PageBasicNotIntializedException()

        _SitePageID = _cPageBasic.SitePageID
        If _Title Is Nothing OrElse _Title.Length = 0 Then
            _Title = _cPageBasic.PageTitle
        End If

    End Sub

    Public Sub New(ByRef context As HttpContext, ByRef page As System.Web.UI.Page)
        Me.New(context, page, CacheOptionsEnum.None)
    End Sub

    Public Sub New(ByRef context As HttpContext, ByRef page As System.Web.UI.Page, cacheOptions As clsPageData.CacheOptionsEnum)
        __context = context

        If ((cacheOptions And clsPageData.CacheOptionsEnum.DisabledCacheForPageBasics) > 0) Then
            Me.CacheDisabledForPageBasics = True
        End If
        If ((cacheOptions And clsPageData.CacheOptionsEnum.DisabledCacheForContent) > 0) Then
            Me.CacheDisabledForContent = True
        End If

        _SitePageID = -1
        If Not String.IsNullOrEmpty(context.Request("PageId")) Then
            Integer.TryParse(context.Request("PageId").Trim(), _SitePageID)
            'If(IsNumeric(Trim(context.Request("PageId"))), Trim(context.Request("PageId")), -1)
        ElseIf Not String.IsNullOrEmpty(page.RouteData.Values("pageid")) Then
            Integer.TryParse(page.RouteData.Values("pageid").Trim(), _SitePageID)
        End If
        If (_SitePageID > 0) Then
            IsQueryStringParamDetected = True
        End If

        If context.Request("PageName") IsNot Nothing Then
            _PageName = Trim(context.Request("PageName"))
        End If

        ' Dim sessVars As clsSessionVariables =
        clsSessionVariables.GetCurrent()
        If HttpContext.Current.Session("LagID") IsNot Nothing Then
            _LagID = HttpContext.Current.Session("LagID")
        Else
            _LagID = "US"
        End If



        If String.IsNullOrEmpty(_PageName) Then
            _PageName = context.Request.AppRelativeCurrentExecutionFilePath
            _PageName = _PageName.TrimStart("~"c).TrimStart("/"c)
        End If

        _cPageBasic = GetPageBasics(_LagID)
        If (_cPageBasic Is Nothing) Then Throw New PageBasicNotIntializedException()

        _SitePageID = _cPageBasic.SitePageID
        _Title = _cPageBasic.PageTitle
        If String.IsNullOrEmpty(_Title) Then
            If context.Request("Title") IsNot Nothing Then
                _Title = Trim(context.Request("Title"))
            End If
        End If

    End Sub


    Public Function GetPageBasics(ByVal LagIDparam As String) As clsPageBasicReturn
        Dim dtStrings As New clsPageBasicReturn
        If (Not String.IsNullOrEmpty(_PageName)) Then
            _PageName = _PageName.ToLower()
        End If


        ' get sys_messages records for a page

        'If ({"cmspage.aspx", "landing.aspx"}.Contains(_PageName) AndAlso _SitePageID > 0) Then

        '    Dim _newPageName = _PageName & "_" & _SitePageID
        '    dtStrings = clsPageData.GetCachedDataTable_Pages(LagIDparam, _newPageName)
        '    If (dtStrings Is Nothing) Then
        '        dtStrings = gLAG.GetPageBasics(LagIDparam, _SitePageID)
        '        clsPageData.SetCachedDataTable_Pages(_newPageName, dtStrings)
        '    End If


        'ElseIf (_SitePageID > 0) Then
        Dim _newPageName = _PageName
        If (_SitePageID > 0) Then _newPageName = _PageName & "_" & _SitePageID
        _newPageName = _newPageName & LagIDparam

        If (Me.CacheDisabledForPageBasics = True OrElse Me.CacheDisabledForContent = True) Then

            If (_SitePageID > 0) Then
                dtStrings = gLAG.GetPageBasics(LagIDparam, _SitePageID)
            Else
                dtStrings = gLAG.GetPageBasics(LagIDparam, _PageName)
            End If

        Else

            dtStrings = clsPageData.GetCachedDataTable_Pages(_newPageName)
            If (dtStrings Is Nothing) Then
                If (_SitePageID > 0) Then
                    dtStrings = gLAG.GetPageBasics(LagIDparam, _SitePageID)
                Else
                    dtStrings = gLAG.GetPageBasics(LagIDparam, _PageName)
                End If
                clsPageData.SetCachedDataTable_Pages(_newPageName, dtStrings)
            End If

        End If

        'Else

        '    dtStrings = clsPageData.GetCachedDataTable_Pages(LagIDparam, _PageName)
        '    If (dtStrings Is Nothing) Then
        '        
        '        clsPageData.SetCachedDataTable_Pages(_PageName, dtStrings)
        '    End If

        'End If

        Return dtStrings
    End Function

    Public Function VerifyCustomString(ByVal key As String) As String
        Dim result As String = Me.VerifyCustomString(key, LagID)
        Return result
    End Function

    Public Function VerifyCustomString(ByVal key As String, LagIDparam As String) As String
        Dim result As String = Me.GetCustomString(key, LagIDparam)
        If (result Is Nothing) Then
            gDataList_PageCustoms.Clear()
            ' _keysDictionary = Nothing
            _dtStrings = Nothing
            result = Me.GetCustomString(key, LagIDparam)
        End If
        Return result
    End Function

    ''' <summary>
    ''' Returns NOTHING if specified key is not found.
    ''' </summary>
    ''' <param name="key">A key to be found in database</param>
    ''' <returns>value paired to spicified key, otherwise NOTHING</returns>
    ''' <remarks></remarks>
    Public Function GetCustomStringJS(ByVal key As String) As String
        Dim result As String = GetCustomString(key, LagID)
        If (result IsNot Nothing) Then AppUtils.JS_PrepareString(result)
        Return result
    End Function

    ''' <summary>
    ''' Returns NOTHING if specified key is not found.
    ''' </summary>
    ''' <param name="key">A key to be found in database</param>
    ''' <returns>value paired to spicified key, otherwise NOTHING</returns>
    ''' <remarks></remarks>
    Public Function GetCustomString(ByVal key As String) As String
        Dim result As String = GetCustomString(key, LagID)
        Return result
    End Function


    ''' <summary>
    ''' Returns NOTHING if specified key is not found.
    ''' </summary>
    ''' <param name="key">A key to be found in database</param>
    ''' <returns>value paired to spicified key, otherwise NOTHING</returns>
    ''' <remarks></remarks>
    Function GetCustomString(ByVal key As String, LagIDparam As String) As String

        Dim result As String = ""

        Try

            If (_dtStrings Is Nothing) Then

                '   _keysDictionary = New Dictionary(Of String, String)

                _dtStrings = New DataTable()

                If (Not String.IsNullOrEmpty(_PageName)) Then _PageName = _PageName.ToLower()

                Try
                    ' get sys_messages records for a page

                    Dim _newPageName = _PageName
                    If (cPageBasic.SitePageID > 0) Then _newPageName = _PageName & "_" & cPageBasic.SitePageID

                    If (Not Me.CacheDisabledForContent) Then
                        _dtStrings = clsPageData.GetCachedDataTable_PageCustoms(_newPageName & LagIDparam)
                    Else
                        Try
                            If (gDataList_PageCustoms.ContainsKey(_newPageName & LagIDparam)) Then
                                gDataList_PageCustoms.Remove(_newPageName & LagIDparam)
                            End If
                            'If (gDataList_PageCustoms.ContainsKey(_newPageName)) Then
                            '    gDataList_PageCustoms.Remove(_newPageName)
                            'End If
                        Catch
                        End Try
                    End If

                    If (_dtStrings Is Nothing) Then
                        ' get dummy dt
                        _dtStrings = gLAG.GetCustomStrings(_SitePageID, LagIDparam)
                        clsPageData.SetCachedDataTable_PageCustoms(_newPageName & LagIDparam, _dtStrings)
                    End If

                Catch
                End Try

            End If

            ' load cache using sys_messages records 
            '     If (Not _keysDictionary.ContainsKey(key)) Then
            'Dim dr As DataRow = (From itm In _dtStrings.Rows
            '                      Where itm("MessageKey") = key
            '                      Select itm).FirstOrDefault()
            '   Dim sqlKey As String = key.Replace("'", "''")
            Dim dr As DataRow = Nothing
            Try

                If _dtStrings IsNot Nothing AndAlso _dtStrings.Columns.Contains("MessageKey") Then
                    Dim r As DataRow() = _dtStrings.Select("MessageKey='" & key & "'")
                    If r.Length > 0 Then
                        dr = r(0)
                    End If
                End If
               
                'For cnt = 0 To _dtStrings.Rows.Count - 1
                '    If (_dtStrings.Rows(cnt)("MessageKey") = key) Then
                '        dr = _dtStrings.Rows(cnt)
                '        Exit For
                '    End If
                'Next
            Catch ex As Exception

            End Try
            'Dim drs As DataRow() = {}
            'Try
            '    drs = _dtStrings.Select("MessageKey='" & sqlKey & "'")
            'Catch ex As System.InvalidOperationException
            '    Try
            '        drs = _dtStrings.Select("MessageKey='" & sqlKey & "'")
            '    Catch ex1 As System.InvalidOperationException
            '    End Try
            'End Try

            'If (drs.Length > 0) Then dr = drs(0)
            '   Dim removeit As Boolean = True
            If (dr Is Nothing) Then

                Using dt As DataTable = gLAG.GetCustomStringDataTable(cPageBasic.SitePageID, key, LagID)


                    If (dt.Rows.Count = 0) Then
                        Dim __dr1 As DataRow = dt.NewRow()
                        __dr1("MessageID") = -(_dtStrings.Rows.Count)
                        __dr1("SitePageID") = cPageBasic.SitePageID
                        __dr1("MessageKey") = key
                        dt.Rows.Add(__dr1)
                    End If

                    If (dt.Rows.Count > 0) Then
                        dr = dt.Rows(0)
                        'removeit = False
                        ' if the size of US column is greater than 2,5kb
                        'If (dr(LagID).ToString().Length < 2560) Then
                        '    Try
                        '        _dtStrings.Merge(dt)
                        '    Catch ex As System.InvalidOperationException
                        '        Try
                        '            _dtStrings.Merge(dt)
                        '        Catch ex1 As System.InvalidOperationException
                        '        End Try
                        '    End Try
                        'End If
                    End If
                End Using
            End If

            If (dr IsNot Nothing) Then
                Dim messageKey As String = dr("MessageKey").ToString()

                Dim valueLag As String = ""
                If (_dtStrings.Columns.Contains(LagIDparam)) Then valueLag = dr(LagIDparam).ToString()
                If (valueLag.Length > 0) Then
                    result = valueLag
                Else
                    Dim valueUS As String = dr(LagIDparam).ToString()
                    result = valueUS
                End If
                ' If removeit Then _dtStrings.Rows.Remove(dr)
            End If

            'End If


            'If (_keysDictionary.ContainsKey(key)) Then
            '    result = _keysDictionary(key)
            'End If


            If (Not String.IsNullOrEmpty(result)) Then

                Dim args As New CustomStringRetrievalCompleteEventArgs(result, LagIDparam)
                RaiseEvent CustomStringRetrievalComplete(Me, args)

                result = args.CustomString

            End If
        Catch ex As Exception

        End Try

        Return result
    End Function


    'Public Function FixHTMLBodyAnchors(ByRef HTMLBody As String, LagIDparam As String)

    '    If (LagIDparam <> "US" AndAlso Not String.IsNullOrEmpty(HTMLBody)) Then


    '        Dim matchStartIndex As Integer = 0
    '        Dim ancMatch As Match = clsWebFormHelper.AnchorRegex.Match(HTMLBody, matchStartIndex)

    '        While ancMatch.Success

    '            Try

    '                Dim hrefGroup As Group = ancMatch.Groups(1)
    '                Dim href As String = hrefGroup.Value
    '                matchStartIndex = hrefGroup.Index + hrefGroup.Length


    '                If (href.StartsWith("/")) Then

    '                    Dim RequestUrl As System.Uri = HttpContext.Current.Request.Url
    '                    Dim currentHost As String = UrlUtils.GetCurrentHostURL(RequestUrl)
    '                    Dim _uri As System.Uri = UrlUtils.GetFullURI(currentHost, href)
    '                    Dim newHref As String = LanguageHelper.GetNewLAGUrl2(RequestUrl, LagIDparam, _uri.ToString())
    '                    HTMLBody = HTMLBody.Remove(hrefGroup.Index, hrefGroup.Length)
    '                    HTMLBody = HTMLBody.Insert(hrefGroup.Index, newHref)
    '                    matchStartIndex = hrefGroup.Index + newHref.Length

    '                ElseIf href.IndexOf("http") = 0 Then
    '                    Dim urlLag As String = clsLanguageHelper.GetLAGFromURL(href)
    '                    If (Not String.IsNullOrEmpty(urlLag) AndAlso LagIDparam <> urlLag) Then
    '                        Dim _uri As New Uri(href)
    '                        Dim newHref As String = _uri.PathAndQuery
    '                        HTMLBody = HTMLBody.Remove(hrefGroup.Index, hrefGroup.Length)
    '                        HTMLBody = HTMLBody.Insert(hrefGroup.Index, newHref)
    '                        matchStartIndex = hrefGroup.Index + newHref.Length
    '                    End If
    '                End If

    '            Catch
    '            End Try

    '            ancMatch = clsWebFormHelper.AnchorRegex.Match(HTMLBody, matchStartIndex)
    '        End While

    '    End If

    '    Return HTMLBody
    'End Function

    Public Overrides Function ToString() As String
        Dim info As String = String.Empty

        'info = info & "_affCode: " & _affCode & vbCrLf
        info = info & "_SitePageID: " & _SitePageID & vbCrLf
        info = info & "_PageName: " & _PageName & vbCrLf
        info = info & "_Title: " & _Title & vbCrLf
        info = info & "_LagID: " & _LagID & vbCrLf

        Return info
    End Function

End Class



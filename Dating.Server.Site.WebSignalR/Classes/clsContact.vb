﻿Public Class clsContact

    Private _associatedGoomenaProfileID As Integer
    Public Property AssociatedGoomenaProfileID As Integer
        Get
            Return _associatedGoomenaProfileID
        End Get
        Set(ByVal value As Integer)
            _associatedGoomenaProfileID = value
        End Set
    End Property

    Private _contactFirstName As String
    Public Property ContactFirstName As String
        Get
            Return _contactFirstName
        End Get
        Set(ByVal value As String)
            _contactFirstName = value
        End Set
    End Property

    Private _contactLastName As String
    Public Property ContactLastName As String
        Get
            Return _contactLastName
        End Get
        Set(ByVal value As String)
            _contactLastName = value
        End Set
    End Property

    Private _contactEmail As String
    Public Property ContactEmail As String
        Get
            Return _contactEmail
        End Get
        Set(ByVal value As String)
            _contactEmail = value
        End Set
    End Property

    Private _goomenaProfileID As Integer
    Public Property GoomenaProfileID As Integer
        Get
            Return _goomenaProfileID
        End Get
        Set(ByVal value As Integer)
            _goomenaProfileID = value
        End Set
    End Property

End Class

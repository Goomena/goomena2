﻿Public Class map
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub


    Protected Function GetLat() As String
        If (Request("lat") IsNot Nothing) Then
            Return Request("lat").Replace(",", ".")
        End If
        Return "0"
    End Function

    Protected Function GetLng() As String
        If (Request("lng") IsNot Nothing) Then
            Return Request("lng").Replace(",", ".")
        End If
        Return "0"
    End Function

    Protected Function GetRadius() As String
        If (Request("radius") IsNot Nothing) Then
            Return Request("radius")
        End If
        Return "100000" ' 100 km
    End Function

    Protected Function GetZoom() As String
        If (Request("zoom") IsNot Nothing) Then
            Return Request("zoom")
        End If
        Return "8" ' 100 km
    End Function
End Class
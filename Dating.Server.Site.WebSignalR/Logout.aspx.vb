﻿Imports Dating.Server.Core.DLL


Public Class Logout
    Inherits System.Web.UI.Page

    Private Sub Logout_Init(sender As Object, e As System.EventArgs) Handles Me.Init
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Response.Cookies.Clear()
        'Dim SessionVariables As clsSessionVariables = clsSessionVariables.GetCurrent()

        'Try
        '    Dim profileid = Session("ProfileID")
        '    If (profileid > 0) Then
        '        DataHelpers.UpdateEUS_Profiles_LogoutData(profileid)
        '    End If
        'Catch ex As Exception

        'End Try

        '' copy
        'Dim _SessionVariablesNew As New clsSessionVariables()
        '_SessionVariablesNew.LagID = Session("LagID")
        '_SessionVariablesNew.myCulture = SessionVariables.myCulture

        ' clear
        'Request.Cookies.Clear()
        'Session("Logoff") = True
        'Session.Clear()
        'FormsAuthentication.SignOut()
        'Response.Cookies.Remove(FormsAuthentication.FormsCookieName)
        'Session.Abandon()
        'FormsAuthentication.SignOut()
        'FormsAuthentication.RedirectToLoginPage()

        'init
        'Session("SessionVariables") = _SessionVariablesNew


        'Response.Redirect(Page.ResolveUrl("~/Default.aspx"))

        '' the below makes an issue with autologin
        'Response.Redirect(Page.ResolveUrl("~/Default.aspx"), false)
        'HttpContext.Current.ApplicationInstance.CompleteRequest()

        'FormsAuthentication.SetAuthCookie(HttpContext.Current.User.Identity.Name, False)
        'FormsAuthentication.SignOut()
        'For Each cookie As String In Request.Cookies.AllKeys
        '    Request.Cookies.Remove(cookie)
        'Next
        'For Each cookie As String In Response.Cookies.AllKeys
        '    Response.Cookies.Remove(cookie)
        'Next
        'FormsAuthentication.SignOut()
        'Response.Cookies.Remove(FormsAuthentication.FormsCookieName)
        'HttpContext.Current.User = Nothing

        '' ''FormsAuthentication.SignOut()
        '' ''Session.Abandon()

        ' '' '' clear authentication cookie
        '' ''Dim cookie1 As HttpCookie = New HttpCookie(FormsAuthentication.FormsCookieName, "")
        '' ''cookie1.Expires = DateTime.Now.AddYears(-1)
        '' ''Response.Cookies.Add(cookie1)

        ' '' '' clear session cookie (not necessary for your current problem but i would recommend you do it anyway)
        '' ''Dim cookie2 As HttpCookie = New HttpCookie("ASP.NET_SessionId", "")
        '' ''cookie2.Expires = DateTime.Now.AddYears(-1)
        '' ''Response.Cookies.Add(cookie2)

        clsCurrentContext.LogOff()
        Response.Redirect(Page.ResolveUrl("~/Default.aspx") & "?logout=1")

    End Sub

    Private Sub Logout_PreRenderComplete(sender As Object, e As System.EventArgs) Handles Me.PreRenderComplete
        'FormsAuthentication.SignOut()
    End Sub
End Class
﻿Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Core.DLL
Imports System.Data.SqlClient
Imports System.Globalization
Imports Dating.Server.Datasets.DLL.DSMembers

Public Class Matching
    Inherits BasePage

    Protected Overrides Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            If clsCurrentContext.VerifyLogin() = True Then
                Me.MasterPageFile = "~/Members/Members2.Master"
            Else
                clsCurrentContext.ClearSession(Session)
                If (Not Me.IsCallback) Then
                    Response.Redirect(ResolveUrl("~/Login.aspx"), False)
                End If
                HttpContext.Current.ApplicationInstance.CompleteRequest()
            End If
            If Not Me.IsMale Then
                Response.Redirect(ResolveUrl("~/Members/default.aspx"), False)
                HttpContext.Current.ApplicationInstance.CompleteRequest()
            End If
            If Not clsMatchingHelper.HasFilledMatchingCriteria(Me.MasterProfileId) Then
                Response.Redirect(ResolveUrl("~/Members/MatchingEdit.aspx"), False)
                HttpContext.Current.ApplicationInstance.CompleteRequest()
            End If
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub
    Protected Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData(Context, Me.Page, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If (Not Me.IsPostBack) Then
                LoadLAG()
            End If
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub



    Protected Sub LoadLAG()
        Try
          
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

   
   
End Class



﻿Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Core.DLL
Imports System.Data.SqlClient
Imports System.Globalization
Imports Dating.Server.Datasets.DLL.DSMembers

Public Class Search1
    Inherits BasePage


#Region "Props"

    Protected Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            If (_pageData Is Nothing) Then
                '_pageData = New clsPageData("~/Members/Search.aspx", Context)
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData(Context, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property


    Private Property LastAdvancedSearchquery As String
        Get
            Return ViewState("LastAdvancedSearchquery")
        End Get
        Set(value As String)
            ViewState("LastAdvancedSearchquery") = value
        End Set
    End Property


    Private Property LastWhatToSearch As WhatToSearchEnum
        Get
            Return ViewState("LastWhatToSearch")
        End Get
        Set(value As WhatToSearchEnum)
            ViewState("LastWhatToSearch") = value
        End Set
    End Property


    'Private Property LoadAdvancedSearchLoaded As Boolean
    '    Get
    '        Return ViewState("LoadAdvancedSearchLoaded")
    '    End Get
    '    Set(value As Boolean)
    '        ViewState("LoadAdvancedSearchLoaded") = value
    '    End Set
    'End Property

    Private Property OldPageIndex As Integer
        Get
            Return ViewState("OldPageIndex")
        End Get
        Set(value As Integer)
            ViewState("OldPageIndex") = value
        End Set
    End Property

    Public Property SearchSorting As SearchSortEnum

        Get
            Dim sorting As SearchSortEnum = SearchSortEnum.RecentlyLoggedIn

            Dim index As Integer = cbSort.SelectedIndex
            If (index = 4) Then
                sorting = SearchSortEnum.NewestNearMember

            ElseIf (index = 0) Then
                sorting = SearchSortEnum.NewestMember

            ElseIf (index = 1) Then
                sorting = SearchSortEnum.OldestMember

            ElseIf (index = 3) Then
                sorting = SearchSortEnum.NearestDistance

            ElseIf (index = 2) Then
                sorting = SearchSortEnum.RecentlyLoggedIn

            ElseIf (index = 5) Then
                sorting = SearchSortEnum.Birthday

            End If

            Return sorting
        End Get

        Set(value As SearchSortEnum)
            Dim index As Integer = 2

            If (value = SearchSortEnum.NewestNearMember) Then
                index = 4

            ElseIf (value = SearchSortEnum.NewestMember) Then
                index = 0

            ElseIf (value = SearchSortEnum.OldestMember) Then
                index = 1

            ElseIf (value = SearchSortEnum.NearestDistance) Then
                index = 3

            ElseIf (value = SearchSortEnum.RecentlyLoggedIn) Then
                index = 2

            ElseIf (value = SearchSortEnum.Birthday) Then
                index = 5

            End If
            cbSort.SelectedIndex = index
        End Set

    End Property

    Public Property ItemsPerPage As Integer

        Get
            Dim perPageNumber As Integer = 10

            Dim index As Integer = cbPerPage.SelectedIndex
            If (index = 0) Then
                perPageNumber = 10
            ElseIf (index = 1) Then
                perPageNumber = 25
            ElseIf (index = 2) Then
                perPageNumber = 50
            End If

            Return perPageNumber
        End Get

        Set(value As Integer)
            Dim index As Integer = 0

            If (value = 25) Then
                index = 1
            ElseIf (value = 50) Then
                index = 2
            Else
                index = 0
            End If

            cbPerPage.SelectedIndex = index
        End Set

    End Property

    ReadOnly Property QSCreterias As IQuickSearchCriterias
        Get
            If (Me.IsMale) Then
                Return ctlQSCrM
            Else
                Return ctlQSCrW
            End If
        End Get
    End Property



    Protected ReadOnly Property DontAskForSave As Boolean
        Get
            If (Session("AdvancedSearch_Dont_Save_Dont_Ask") Is Nothing) Then
                Return False
            ElseIf (Session("AdvancedSearch_Dont_Save_Dont_Ask") = True) Then
                Return True
            End If
            Return False
        End Get
    End Property


#End Region



    Protected Overrides Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        SetMasterPage(CurrentPageData)

        Try
            If clsCurrentContext.VerifyLogin() = True Then
                Me.MasterPageFile = "~/Members/Members2.Master"
            Else
                clsCurrentContext.ClearSession(Session)
                If (Not Me.IsCallback) Then
                    Response.Redirect(ResolveUrl("~/Login.aspx"), False)
                End If
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            AddHandler Me.searchL.Repeater.ItemCommand, AddressOf Me.offersRepeater_ItemCommand


            If (Me.IsMale) Then
                ctlQSCrM.Visible = True
                ctlQSCrW.Visible = False
                ctlQSCrW.EnableViewState = False
            Else
                ctlQSCrW.Visible = True
                ctlQSCrM.Visible = False
                ctlQSCrM.EnableViewState = False
            End If


            If (Not Page.IsPostBack) Then
                Try
                    Me.ItemsPerPage = clsProfilesPrivacySettings.GET_ItemsPerPage(Me.MasterProfileId)
                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "GET_ItemsPerPage")
                End Try

                Dim currentPage As Integer
                Dim req As String = Request.QueryString("seo" & Me.Pager.ClientID)
                If (Not String.IsNullOrEmpty(req)) Then
                    Try
                        req = req.Replace("page", "")
                        Integer.TryParse(req, currentPage)
                        Me.Pager.ItemCount = Me.ItemsPerPage * currentPage
                        Me.Pager.PageIndex = currentPage - 1
                    Catch ex As Exception
                    End Try
                End If

            End If

            If (Not Me.IsPostBack) Then
                'TODO: remove comment
                'mvSearchMain.SetActiveView(vwSearchBrowse)
                LoadLAG()
                LoadView()
                BindFilteredResults()
                'BindSearchResults()
            End If

            If (ProfileHelper.IsMale(Me.GetCurrentProfile().GenderId)) Then
                divIncome.Visible = False
                'divNetWorth.Visible = False
            Else
                divIncome.Visible = True
                'divNetWorth.Visible = True
            End If

            If (Request("__EVENTTARGET") = btnSearch_ND.UniqueID) Then
                btnSearch_Click(btnSearch_ND, e)
            End If
            If (Request("__EVENTTARGET") = imgSearchByUserName.UniqueID) Then
                imgSearchByUserName_Click(imgSearchByUserName, e)
            End If

            If (Me.DontAskForSave OrElse hdfNewName.Value = "#DO_NOT_SAVE#") Then
                btnSaveAndFind.Text = CurrentPageData.GetCustomString("btnSaveAndFind.JustFind")
                btnSaveAndFind.Text = AppUtils.StripHTML(btnSaveAndFind.Text)
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub

    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender

        Try
            Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try


        Try
            If (Me.Pager.PageIndex > 0) Then
                Me.Header.Title = Me.Header.Title & " -- Search results page " & (Me.Pager.PageIndex + 1)
            End If

            ' check if page is changed, if so scroll to top.
            ' not scrolling to top cause of updatePanel
            If (Page.IsPostBack) Then
                If (Me.OldPageIndex <> Me.Pager.PageIndex) Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "scrollToResultsTop", "scrollToResultsTop();", True)
                    Me.OldPageIndex = Me.Pager.PageIndex
                End If
            End If

        Catch ex As Exception
            'WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub



    Public Overrides Sub Master_LanguageChanged()
        Try

            If (Me.Visible) Then
                Me._pageData = Nothing
                LoadLAG()
                BindFilteredResults()
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


    Protected Sub LoadLAG()
        Try
            '  Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            SetControlsValue(Me, CurrentPageData)
            chkTravelOnDate.Text = CurrentPageData.GetCustomString(chkTravelOnDate.ID)

            cbSort.Items.Clear()
            cbSort.Items.Add(CurrentPageData.GetCustomString("cbSort_NewestMemberItem"), "NewestMember")
            cbSort.Items.Add(CurrentPageData.GetCustomString("cbSort_OldestMemberItem"), "OldestMember")
            cbSort.Items.Add(CurrentPageData.GetCustomString("cbSort_RecentlyLoggedInItem"), "RecentlyLoggedIn")
            cbSort.Items.Add(CurrentPageData.GetCustomString("cbSort_NearestDistanceItem"), "NearestDistance")
            cbSort.Items.Add(CurrentPageData.GetCustomString("cbSort_NewestNearMemberItem"), "NewestNearMember")
            cbSort.Items(cbSort.Items.Count - 1).Selected = True
            cbSort.Items.Add(CurrentPageData.GetCustomString("cbSort_BirthdayItem"), "Birthday")

            'msg_LocationText.Text = CurrentPageData.GetCustomString(msg_LocationText.ID)
            msg_SelectCountry.Text = CurrentPageData.GetCustomString(msg_SelectCountry.ID)
            msg_SelectRegionText.Text = CurrentPageData.GetCustomString(msg_SelectRegionText.ID)
            msg_SelectCountry.Text = CurrentPageData.GetCustomString(msg_SelectCountry.ID)
            msg_SelectCity.Text = CurrentPageData.GetCustomString(msg_SelectCity.ID)
            'msg_CityText.Text = CurrentPageData.GetCustomString(msg_CityText.ID)

            msg_ZipCodeText.Text = CurrentPageData.GetCustomString(msg_ZipCodeText.ID)
            'msg_SelectRegion.Text = CurrentPageData.GetCustomString(msg_SelectRegion.ID)
            msg_ZipExample.Text = CurrentPageData.GetCustomString(msg_ZipExample.ID)
            'msg_LocationText2.Text = CurrentPageData.GetCustomString(msg_LocationText2.ID)
            'lblLocation.Text = CurrentPageData.GetCustomString(lblLocation.ID)

            'lblYears.Text = CurrentPageData.GetCustomString("lblYears_ND")
            msg_DistanceFromMeText.Text = CurrentPageData.GetCustomString("msg_DistanceFromMeText")
            msg_AgeToWord.Text = CurrentPageData.GetCustomString("msg_AgeToWord")
            btnDeleteSS.Text = CurrentPageData.GetCustomString("btnDeleteSavedSearch")

            'msg_UserNameSearchQ.Text = CurrentPageData.GetCustomString("msg_UserNameSearch")
            'txtUserNameQ.NullText = CurrentPageData.GetCustomString("msg_UserNameSearch")

            If (mvSearchMain.ActiveViewIndex > -1 AndAlso mvSearchMain.GetActiveView().Equals(vwSearchAdvanced)) Then
                cbCountry.Items.Clear()
                LoadAdvancedSearch()
            End If


            'lnkViewDescription.Text = Me.CurrentPageData.GetCustomString("CartSearchWhatIsIt")
            lnkViewDescription2.Text = Me.CurrentPageData.GetCustomString("CartSearchWhatIsIt")
            lnkViewDescription2.OnClientClick = OnMoreInfoClickFunc(
                lnkViewDescription2.OnClientClick,
                ResolveUrl("~/Members/InfoWin.aspx?info=search"),
                Me.CurrentPageData.GetCustomString("CartSearchView"))

            WhatIsIt1.PopupControl.HeaderText = Me.CurrentPageData.GetCustomString("CartSearchView")
            msg_divNoResultsTryChangeCriteria_ND.Text = msg_divNoResultsTryChangeCriteria_ND.Text.
                Replace("[SEARCH_NEAR_MEMBERS_URL]", ResolveUrl("~/Members/Search.aspx?vw=NEAREST")).
                Replace("[SEARCH_NEW_MEMBERS_URL]", ResolveUrl("~/Members/Search.aspx?vw=NEWEST"))

            btnSaveAndFind.Text = CurrentPageData.GetCustomString("btnSaveAndFind")
            btnSaveAndFind.Text = AppUtils.StripHTML(btnSaveAndFind.Text)

            If (Me.DontAskForSave) Then
                hdfNewName.Value = "#DO_NOT_SAVE#"
                hdfSelectedName.Value = "-1"
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub




    Protected Sub offersRepeater_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs)
        Dim success = False
        Try
            Dim currentUserHasPhotos As Boolean = clsUserDoes.HasPhotos(Me.MasterProfileId, Me.MirrorProfileId)
            If (currentUserHasPhotos) Then
                If (e.CommandName = OfferControlCommandEnum.WINK.ToString()) Then

                    Try
                        Dim ToProfileID As Integer
                        Integer.TryParse(e.CommandArgument.ToString(), ToProfileID)

                        'clsUserDoes.SendWink(ToProfileID, Me.MasterProfileId)
                        'success = True

                        Dim MaySendWink As Boolean = True
                        Dim MaySendWink_CheckSetting As Boolean = True


                        MaySendWink_CheckSetting = Not clsProfilesPrivacySettings.GET_DisableReceivingLIKESFromDifferentCountryFromTo(ToProfileID, Me.MasterProfileId)
                        If (MaySendWink_CheckSetting = False) Then
                            MaySendWink = False
                        End If


                        If (MaySendWink AndAlso Me.IsFemale) Then
                            If (Me.IsReferrer) Then
                                MaySendWink = clsUserDoes.IsReferrerAllowedSendWink(ToProfileID, Me.MasterProfileId)
                            End If
                        End If

                        If (MaySendWink) Then
                            clsUserDoes.SendWink(ToProfileID, Me.MasterProfileId)
                        Else
                            If (MaySendWink_CheckSetting = False) Then
                                Dim popupMessage As String = WhatIsIt.OnMoreInfoClickFunc(ResolveUrl("~/Members/InfoWin.aspx?info=Error_Sending_Receiver_Disabled_Likes_From_Diff_Country&popup=popupWhatIs"), "Error! Can't send", 650, 350)
                                popupMessage = "function fnError_Sending_Receiver_DoNot_Have_Photo(){" & popupMessage & "};"
                                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Error_Sending_Receiver_DoNot_Have_Photo", popupMessage, True)
                            Else
                                Dim popupMessage As String = WhatIsIt.OnMoreInfoClickFunc(ResolveUrl("~/Members/InfoWin.aspx?info=Error_Sending_Receiver_DoNot_Have_Photo&popup=popupWhatIs"), "Error! Can't send", 650, 350)
                                popupMessage = "function fnError_Sending_Receiver_DoNot_Have_Photo(){" & popupMessage & "};"
                                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Error_Sending_Receiver_DoNot_Have_Photo", popupMessage, True)
                            End If
                        End If

                        success = MaySendWink

                    Catch ex As Exception
                        WebErrorMessageBox(Me, ex, "")
                    End Try


                ElseIf (e.CommandName = OfferControlCommandEnum.UNWINK.ToString()) Then

                    Try
                        Dim ToProfileID As Integer
                        Integer.TryParse(e.CommandArgument.ToString(), ToProfileID)

                        clsUserDoes.CancelPendingWink(ToProfileID, Me.MasterProfileId)
                        success = True
                    Catch ex As Exception
                        WebErrorMessageBox(Me, ex, "")
                    End Try
                    'success = True

                ElseIf (e.CommandName = OfferControlCommandEnum.FAVORITE.ToString()) Then

                    Try
                        Dim _userId As Integer = CType(e.CommandArgument, Integer)
                        If (_userId > 0) Then
                            clsUserDoes.MarkAsFavorite(_userId, Me.MasterProfileId)
                        End If
                        success = True
                    Catch ex As Exception
                        WebErrorMessageBox(Me, ex, "")
                    End Try

                ElseIf (e.CommandName = OfferControlCommandEnum.UNFAVORITE.ToString()) Then

                    Try
                        Dim _userId As Integer = CType(e.CommandArgument, Integer)
                        If (_userId > 0) Then
                            clsUserDoes.MarkAsUnfavorite(_userId, Me.MasterProfileId)
                        End If
                        success = True
                    Catch ex As Exception
                        WebErrorMessageBox(Me, ex, "")
                    End Try


                ElseIf (e.CommandName = OfferControlCommandEnum.UNBLOCK.ToString()) Then

                    Try
                        Dim _userId As Integer = CType(e.CommandArgument, Integer)
                        If (_userId > 0) Then
                            clsUserDoes.MarkAsUnblocked(_userId, Me.MasterProfileId)
                        End If
                        success = True
                    Catch ex As Exception
                        WebErrorMessageBox(Me, ex, "")
                    End Try

                End If
            Else
                Response.Redirect("~/Members/Photos.aspx", False)
                HttpContext.Current.ApplicationInstance.CompleteRequest()
            End If



        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try



        Try
            If (success) Then
                BindFilteredResults()
                'BindSearchResults()
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try


    End Sub


    Protected Sub imgSearchByUserName_Click(sender As Object, e As System.EventArgs) 'Handles imgSearchByUserName.Click
        Try
            Me.LastAdvancedSearchquery = Nothing

            ' remove flag WhatToSearchEnum.ByFilters
            If ((Me.LastWhatToSearch And WhatToSearchEnum.ByFilters) > 0) Then Me.LastWhatToSearch = Me.LastWhatToSearch Xor WhatToSearchEnum.ByFilters
            ' add flag WhatToSearchEnum.ByUserName
            Me.LastWhatToSearch = Me.LastWhatToSearch Or WhatToSearchEnum.ByUserName

            Me.Pager.PageIndex = -1
            BindFilteredResults()

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) 'Handles btnSearch_ND.Click
        Try
            Me.LastAdvancedSearchquery = Nothing

            ' remove flag WhatToSearchEnum.ByUserName
            If ((Me.LastWhatToSearch And WhatToSearchEnum.ByUserName) > 0) Then Me.LastWhatToSearch = Me.LastWhatToSearch Xor WhatToSearchEnum.ByUserName
            ' add flag WhatToSearchEnum.ByFilters
            Me.LastWhatToSearch = Me.LastWhatToSearch Or WhatToSearchEnum.ByFilters

            Me.Pager.PageIndex = -1
            BindFilteredResults()

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Protected Sub btnSaveAndFind_Click(sender As Object, e As EventArgs) Handles btnSaveAndFind.Click
        Try

            Me.Pager.PageIndex = -1
            Me.LastAdvancedSearchquery = Nothing
            BindAdvancedSearchResults()
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "SaveAndFindComplete", "SaveAndFindComplete()", True)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub



    Protected Sub Pager_PageIndexChanged(sender As Object, e As EventArgs) Handles Pager.PageIndexChanged
        Try

            BindFilteredResults()
            'BindSearchResults()
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Protected Sub ddlSort_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbSort.SelectedIndexChanged
        Try
            Me.Pager.PageIndex = -1
            BindFilteredResults()
            'BindSearchResults()

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Protected Sub ddlPerPage_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbPerPage.SelectedIndexChanged
        Try
            clsProfilesPrivacySettings.Update_ItemsPerPage(Me.MasterProfileId, Me.ItemsPerPage)
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorSendEmail(ex, "")
        End Try
        Try
            Me.Pager.PageIndex = -1
            BindFilteredResults()
            'BindSearchResults()

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    'Protected Sub hlBrowse_Click(sender As Object, e As EventArgs) Handles lnkBrowse.Click
    '    Try

    '        mvSearchMain.SetActiveView(vwSearchBrowse)
    '        BindFilteredResults()
    '        'BindSearchResults()
    '    Catch ex As Exception
    '        WebErrorMessageBox(Me, ex, "")
    '    End Try
    'End Sub


    Protected Sub hlAdvanced_Click(sender As Object, e As EventArgs) 'Handles lnkAdvanced.Click
        mvSearchMain.SetActiveView(vwSearchAdvanced)
    End Sub



    Sub SetPager(itemsCount As Integer)
        Me.Pager.ItemCount = itemsCount
        Me.Pager.ItemsPerPage = Me.ItemsPerPage
        Me.Pager.Visible = (itemsCount > Me.ItemsPerPage)

        If (Me.Pager.PageIndex = -1) AndAlso (itemsCount > 0) Then
            Me.Pager.PageIndex = 0
        End If
    End Sub

    Function GetRecordsToRetrieve() As Integer
        Me.Pager.ItemsPerPage = Me.ItemsPerPage

        If (Me.Pager.PageIndex = -1) AndAlso (Me.Pager.ItemCount > 0) Then
            Me.Pager.PageIndex = 0
        End If

        If (Me.Pager.PageIndex > 0) Then
            Return (Me.Pager.ItemsPerPage * (Me.Pager.PageIndex + 1))
        End If
        Return Me.Pager.ItemsPerPage
    End Function


    'Private Function GetDistanceNumber() As Integer
    '    Dim distanceNumber As Integer = 250

    '    If (Not String.IsNullOrEmpty(ddlDistance.SelectedValue)) Then
    '        Integer.TryParse(ddlDistance.SelectedValue, distanceNumber)
    '    End If

    '    Return distanceNumber
    'End Function


    Protected Sub mvSearchMain_ActiveViewChanged(sender As Object, e As EventArgs) Handles mvSearchMain.ActiveViewChanged
        Try

            If (mvSearchMain.GetActiveView().Equals(vwSearchBrowse)) Then

                'liBrowse.Attributes.Add("class", "down")
                'liAdvanced.Attributes.Remove("class")
                'left_tab.Visible = True

            ElseIf (mvSearchMain.GetActiveView().Equals(vwSearchAdvanced)) Then

                'liAdvanced.Attributes.Add("class", "down")
                'liBrowse.Attributes.Remove("class")
                '' If (Not Page.IsPostBack) Then
                LoadAdvancedSearch()
                'left_tab.Visible = False

            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


    Private Sub FillUliWithPhotos(ByRef _uli As clsWinkUserListItem)
        Dim allowDisplayLevel = 0
        '   Dim usersHasCommunication As Boolean = False
        ' usersHasCommunication = clsUserDoes.HasCommunication(_uli.OtherMemberProfileID, Me.MasterProfileId)
        Dim PrId As Integer = _uli.OtherMemberProfileID
        Using cmdb As CMSDBDataContext = Me.GetCMSDBDataContext
            Dim phtLvl As EUS_ProfilePhotosLevel = (From itm In cmdb.EUS_ProfilePhotosLevels
                                              Where itm.FromProfileID = PrId AndAlso itm.ToProfileID = Me.MasterProfileId
                                              Select itm).FirstOrDefault()


            If (phtLvl IsNot Nothing) Then
                allowDisplayLevel = phtLvl.PhotoLevelID
            End If
        End Using

        Using ds As DataSet = clsSearchHelper.GetTop5Photos(_uli.OtherMemberProfileID, _uli.OtherMemberMirrorProfileID)
            If ds IsNot Nothing AndAlso ds.Tables.Count = 1 AndAlso ds.Tables(0).Rows.Count > 0 Then
                Dim popupTitle1 = CurrentPageData.VerifyCustomString("Private.Photo.Level.of.Levels")
                For i As Integer = 0 To 4
                    If ds.Tables(0).Rows.Count >= (i + 1) Then
                        _uli.PhotosProperties(i).HasPhoto = True
                        _uli.PhotosProperties(i).PhotoFilename = ds.Tables(0).Rows(i)("FileName")

                        _uli.PhotosProperties(i).Photolvl = ds.Tables(0).Rows(i)("DisplayLevel")
                        If (_uli.PhotosProperties(i).Photolvl > allowDisplayLevel) Then

                            ' view private photo of other

                            If (Me.IsFemale) Then
                                'uli.ImageThumbUrl = ResolveUrl("~/Images2/men_level.png")
                                _uli.PhotosProperties(i).ImageThumbUrl = ResolveUrl("~/Images2/private/list-menlevel" & _uli.PhotosProperties(i).Photolvl & ".png")
                            Else
                                'uli.ImageThumbUrl = ResolveUrl("~/Images2/woman_level.png")
                                _uli.PhotosProperties(i).ImageThumbUrl = ResolveUrl("~/Images2/private/list-womenlevel" & _uli.PhotosProperties(i).Photolvl & ".png")
                            End If

                            _uli.PhotosProperties(i).ImageUrl = "javascript:void(0);"


                            'Dim url As String = ResolveUrl("~/Members/InfoWin.aspx?info=Blocked_Private_Photo_LevelDesc2&lvl=[PHOTO_LEVEL]&g=[GENDER]&p=[LOGIN]")
                            Dim url As String = Me.ResolveUrl("~/Members/SharePrivatePhoto.aspx?info=Blocked_Private_Photo_LevelDesc2&lvl=[PHOTO_LEVEL]&g=[GENDER]&p=[LOGIN]&id=[LOGINID]") '&popup=popupWhatIs
                            url = url.Replace("[PHOTO_LEVEL]", _uli.PhotosProperties(i).Photolvl)
                            url = url.Replace("[LOGIN]", HttpUtility.UrlEncode(_uli.OtherMemberLoginName))
                            url = url.Replace("[LOGINID]", _uli.OtherMemberProfileID)
                            If (Me.IsFemale) Then
                                url = url.Replace("[GENDER]", "1")
                            Else
                                url = url.Replace("[GENDER]", "2")
                            End If
                            Dim popupTitle = popupTitle1
                            If popupTitle IsNot Nothing Then popupTitle = popupTitle.Replace("[LEVEL]", _uli.PhotosProperties(i).Photolvl)


                            _uli.PhotosProperties(i).ImageUrl = url
                            _uli.PhotosProperties(i).ImageUrlOnClick = "openPopupSharePhoto2(this,'" & AppUtils.JS_PrepareString(popupTitle) & "');return false;"

                            'uli.PhotoLevelText = CurrentPageData.GetCustomString("Blocked.Private.Photo.View.Text")
                            'uli.PhotoLevelText = uli.PhotoLevelText.Replace("###PHOTO_LEVEL###", dr.DisplayLevel)
                            'uli.PhotoLevelText = "<div class=""photo-level-text""><div class=""photo-level-text-top"">" & _
                            '    CurrentPageData.GetCustomString("Blocked.Private.Photo.View.Text").Replace("###PHOTO_LEVEL###", "") & _
                            '    "</div><div class=""photo-level-text-number"">" & dr.DisplayLevel & "</div></div>"
                            'If (Me.IsMale) Then
                            _uli.PhotosProperties(i).PhotoLevelText = "<div class=""photo-level-text""><div>&nbsp;</div></div>"
                            'End If
                            '   isLocked = True
                        Else
                            _uli.PhotosProperties(i).PhotoLevelText = "<div class=""photo-level-text""><div>&nbsp;</div></div>"
                            _uli.PhotosProperties(i).ImageUrl = ProfileHelper.GetProfileImageURL(_uli.OtherMemberProfileID, _uli.PhotosProperties(i).PhotoFilename, _uli.OtherMemberGenderid, False, Me.IsHTTPS)
                            _uli.PhotosProperties(i).ImageThumbUrl = ProfileHelper.GetProfileImageURL(_uli.OtherMemberProfileID, _uli.PhotosProperties(i).PhotoFilename, _uli.OtherMemberGenderid, True, Me.IsHTTPS)
                        End If
                        _uli.PhotosProperties(i).DisplayLevelText = ProfileHelper.GetPhotosDisplayLevelString(_uli.PhotosProperties(i).Photolvl, Me.GetLag())
                    Else
                        _uli.PhotosProperties(i).HasPhoto = False
                    End If

                Next
            Else
                For i As Integer = 0 To 4
                    _uli.PhotosProperties(i).HasPhoto = False
                Next
            End If
        End Using
    End Sub
    Private Sub FillOfferControlList(dt As DataTable, offersControl As Dating.Server.Site.Web.SearchControl)
        Try

            Dim drDefaultPhoto As DSMembers.EUS_CustomerPhotosRow = DataHelpers.GetProfilesDefaultPhoto(Me.MasterProfileId)
            Dim uliCurrentMember As New clsWinkUserListItem()
            uliCurrentMember.ProfileID = Me.MasterProfileId ' 
            uliCurrentMember.MirrorProfileID = Me.MirrorProfileId
            uliCurrentMember.LoginName = Me.GetCurrentProfile().LoginName
            uliCurrentMember.Genderid = Me.GetCurrentProfile().GenderId
            uliCurrentMember.ProfileViewUrl = ResolveUrl("~/Members/Profile.aspx?p=" & HttpUtility.UrlEncode(Me.GetCurrentProfile().LoginName))

            If (drDefaultPhoto IsNot Nothing) Then
                uliCurrentMember.ImageFileName = drDefaultPhoto.FileName
                uliCurrentMember.ImageUploadDate = drDefaultPhoto.DateTimeToUploading.ToLocalTime().ToString("d MMM, yyyy H:mm")
            End If

            uliCurrentMember.ImageUrl = ProfileHelper.GetProfileImageURL(Me.MasterProfileId, uliCurrentMember.ImageFileName, Me.SessionVariables.MemberData.GenderId, True, Me.IsHTTPS, PhotoSize.D150)
            uliCurrentMember.ImageThumbUrl = uliCurrentMember.ImageUrl


            Dim mins As Integer = clsConfigValues.Get__members_online_minutes()
            Dim LastActivityUTCDate As DateTime = Date.UtcNow.AddMinutes(-mins)
            Dim hours As Integer = clsConfigValues.Get__members_online_recently_hours()
            Dim LastActivityRecentlyUTCDate As DateTime = Date.UtcNow.AddHours(-hours)


            Dim rowsCount As Integer
            For rowsCount = 0 To dt.Rows.Count - 1
                'If (rowsCount > endIndex) Then Exit For

                Try
                    Dim dr As DataRow = dt.Rows(rowsCount)
                    Dim uli As New clsWinkUserListItem()

                    uli.LAGID = Session("LAGID")
                    uli.ProfileID = uliCurrentMember.ProfileID
                    uli.MirrorProfileID = uliCurrentMember.MirrorProfileID
                    uli.LoginName = uliCurrentMember.LoginName
                    uli.Genderid = uliCurrentMember.Genderid
                    uli.ProfileViewUrl = uliCurrentMember.ProfileViewUrl
                    uli.ImageFileName = uliCurrentMember.ImageFileName
                    uli.ImageUrl = uliCurrentMember.ImageUrl
                    uli.ImageThumbUrl = uliCurrentMember.ImageThumbUrl
                    uli.ImageUploadDate = uliCurrentMember.ImageUploadDate



                    If (dt.Columns.Contains("Birthday")) Then uli.OtherMemberBirthday = clsNullable.DBNullToDateTimeNull(dr("Birthday")) 'If(Not dr.IsNull("Birthday"), dr("Birthday"), Nothing)
                    uli.OtherMemberLoginName = dr("LoginName")
                    uli.OtherMemberProfileID = dr("ProfileID")
                    uli.OtherMemberMirrorProfileID = dr("MirrorProfileID")
                    uli.OtherMemberCity = dr("City").ToString() 'IIf(Not dr.IsNull("City"), dr("City"), String.Empty)
                    uli.OtherMemberRegion = dr("Region").ToString() 'IIf(Not dr.IsNull("Region"), dr("Region"), String.Empty)
                    uli.OtherMemberCountry = dr("Country").ToString() 'IIf(Not dr.IsNull("Country"), dr("Country"), "")
                    'uli.OtherMemberHeading = IIf(Not dr.IsNull("AboutMe_Heading"), dr("AboutMe_Heading"), String.Empty)
                    uli.OtherMemberGenderid = dr("Genderid")
                    uli.OtherMemberIsFemale = ProfileHelper.IsFemale(uli.OtherMemberGenderid)
                    uli.OtherMemberIsMale = ProfileHelper.IsMale(uli.OtherMemberGenderid)
                    uli.OtherMemberGenderClass = If(uli.OtherMemberIsFemale, "other-female", "other-male")
                    FillUliWithPhotos(uli)

                    uli.OtherMemberIsOnline = clsNullable.DBNullToBoolean(dr("IsOnline")) ' IIf(Not dr.IsNull("IsOnline"), dr("IsOnline"), False)
                    Dim __LastActivityDateTime As DateTime? = clsNullable.DBNullToDateTimeNull(dr("LastActivityDateTime")) 'IIf(Not dr.IsNull("LastActivityDateTime"), dr("LastActivityDateTime"), Nothing)

                    If (dt.Columns.Contains("IsOnlineNow")) Then
                        uli.OtherMemberIsOnline = clsNullable.DBNullToBoolean(dr("IsOnlineNow")) 'IIf(Not dr.IsNull("IsOnlineNow"), dr("IsOnlineNow"), False)
                    Else
                        If (uli.OtherMemberIsOnline) Then
                            If (__LastActivityDateTime.HasValue) Then
                                uli.OtherMemberIsOnline = __LastActivityDateTime >= LastActivityUTCDate
                            Else
                                uli.OtherMemberIsOnline = False
                            End If
                        End If
                    End If

                    If (Not uli.OtherMemberIsOnline) Then
                        If (dt.Columns.Contains("IsOnlineRecently")) Then
                            'uli.OtherMemberIsOnlineRecently = IIf(Not dr.IsNull("IsOnlineRecently"), dr("IsOnlineRecently"), False)
                            uli.OtherMemberIsOnlineRecently = clsNullable.DBNullToBoolean(dr("IsOnlineRecently"))
                        Else
                            If (uli.OtherMemberIsOnline) Then
                                If (__LastActivityDateTime.HasValue) Then
                                    uli.OtherMemberIsOnlineRecently = __LastActivityDateTime >= LastActivityRecentlyUTCDate
                                Else
                                    uli.OtherMemberIsOnlineRecently = False
                                End If
                            End If
                        End If
                    End If

                    'is man and has credits
                    If (uli.OtherMemberGenderid = 1) Then
                        'uli.OtherMemberIsVip = (IIf(Not dr.IsNull("AvailableCredits"), dr("AvailableCredits"), 0) >= ProfileHelper.Config_UNLOCK_MESSAGE_READ_CREDITS)
                        'uli.OtherMemberIsVip = uli.OtherMemberIsVip OrElse IIf(Not dr.IsNull("HasSubscription"), dr("HasSubscription"), False)
                        uli.OtherMemberIsVip = (clsNullable.DBNullToInteger(dr("AvailableCredits")) >= ProfileHelper.Config_UNLOCK_MESSAGE_READ_CREDITS)
                        uli.OtherMemberIsVip = uli.OtherMemberIsVip OrElse clsNullable.DBNullToBoolean(dr("HasSubscription"))
                    End If

                    uli.PhotoCssClass = "tt_enabled"
                    uli.PhotosCountString = globalStrings.GetCustomString("Member.Has.Photos.Count", uli.LAGID)
                    Try
                        If (clsNullable.DBNullToInteger(dr("PhotosApproved")) > 0) Then
                            'If (Not dr.IsNull("PhotosApproved") AndAlso dr("PhotosApproved") > 0) Then
                            uli.PhotosCountString = uli.PhotosCountString.Replace("[PHOTOS_COUNT]", dr("PhotosApproved"))
                        Else
                            uli.PhotosCountString = uli.PhotosCountString.Replace("[PHOTOS_COUNT]", 0)
                        End If
                    Catch ex As Exception
                        WebErrorSendEmail(ex, "PhotosCountString")
                    End Try


                    If (Not dr.IsNull("PersonalInfo_HeightID")) Then
                        uli.OtherMemberHeight = ProfileHelper.GetHeightString(dr("PersonalInfo_HeightID"), uli.LAGID)
                    End If


                    If (Not dr.IsNull("PersonalInfo_BodyTypeID")) Then
                        uli.OtherMemberPersonType = ProfileHelper.GetBodyTypeString(dr("PersonalInfo_BodyTypeID"), uli.LAGID)
                    End If


                    If (Not dr.IsNull("PersonalInfo_HairColorID")) Then
                        uli.OtherMemberHair = ProfileHelper.GetHairColorString(dr("PersonalInfo_HairColorID"), uli.LAGID)
                    End If


                    If (Not dr.IsNull("PersonalInfo_EyeColorID")) Then
                        uli.OtherMemberEyes = ProfileHelper.GetEyeColorString(dr("PersonalInfo_EyeColorID"), uli.LAGID)
                    End If


                    If (Not dr.IsNull("PersonalInfo_EthnicityID") AndAlso dr("PersonalInfo_EthnicityID") > 1) Then
                        uli.OtherMemberEthnicity = ProfileHelper.GetEthnicityString(dr("PersonalInfo_EthnicityID"), uli.LAGID)
                    End If



                    If (Not dr.IsNull("Birthday")) Then
                        uli.OtherMemberAge = ProfileHelper.GetCurrentAge(dr("Birthday"))
                    Else
                        uli.OtherMemberAge = 20
                    End If


                    If (Not dr.IsNull("FileName")) Then
                        uli.OtherMemberImageFileName = dr("FileName")
                        'ElseIf (dr("HasPhoto") = 1) Then
                        '    uli.OtherMemberImageFileName = "Private"
                        '    uli.OtherMemberImageUrl = ProfileHelper.GetPrivateImageURL(uli.OtherMemberGenderid)
                        '    uli.OtherMemberImageThumbUrl = ProfileHelper.GetPrivateImageURL(uli.OtherMemberGenderid)
                        'Else
                        '    uli.OtherMemberImageUrl = ProfileHelper.GetDefaultImageURL(uli.OtherMemberGenderid)
                        '    uli.OtherMemberImageThumbUrl = uli.OtherMemberImageUrl
                        '    uli.OtherMemberImageFileName = System.IO.Path.GetFileName(uli.OtherMemberImageUrl)
                    End If
                    'uli.OtherMemberImageUrl = ProfileHelper.GetProfileImageURL(dr("ProfileID"), uli.OtherMemberImageFileName, uli.OtherMemberGenderid, True, Me.IsHTTPS, PhotoSize.D150)
                    If (dt.Columns.Contains("HasPhoto")) Then
                        If (Not dr.IsNull("HasPhoto") AndAlso (dr("HasPhoto").ToString() = "1" OrElse dr("HasPhoto").ToString() = "True")) Then
                            ' user has photo
                            If (uli.OtherMemberImageFileName IsNot Nothing) Then
                                'has public photos
                                uli.OtherMemberImageUrl = ProfileHelper.GetProfileImageURL(uli.OtherMemberProfileID, uli.OtherMemberImageFileName, uli.OtherMemberGenderid, True, Me.IsHTTPS, PhotoSize.D150)

                            Else
                                'has private photos
                                uli.OtherMemberImageUrl = ProfileHelper.GetPrivateImageURL(uli.OtherMemberGenderid)

                            End If
                        Else
                            ' has no photo
                            uli.OtherMemberImageUrl = ProfileHelper.GetDefaultImageURL(uli.OtherMemberGenderid)

                        End If
                        If IsHTTPS AndAlso uli.OtherMemberImageUrl.StartsWith("http:") Then uli.OtherMemberImageUrl = "https" & uli.OtherMemberImageUrl.Remove(0, "http".Length)
                    Else
                        uli.OtherMemberImageUrl = ProfileHelper.GetProfileImageURL(uli.OtherMemberProfileID, uli.OtherMemberImageFileName, uli.OtherMemberGenderid, True, Me.IsHTTPS, PhotoSize.D150)
                    End If
                    uli.OtherMemberImageThumbUrl = uli.OtherMemberImageUrl


                    uli.OtherMemberProfileViewUrl = ResolveUrl("~/Members/Profile.aspx?p=" & HttpUtility.UrlEncode(dr("LoginName")))

                    uli.Distance = dr("distance")

                    If (uli.Distance < gCarDistance) Then
                        uli.DistanceCss = "distance_car"
                    Else
                        uli.DistanceCss = "distance_plane"
                    End If



                    Dim allowFavorite As Boolean = True
                    If (dr("HasFavoritedMe") > 0 AndAlso dr("DidIFavorited") > 0) Then
                        ''''''''''''''''''''''''
                        ' both users has favorited, each other
                        ''''''''''''''''''''''''
                        allowFavorite = False

                    ElseIf (dr("HasFavoritedMe") = 0 AndAlso dr("DidIFavorited") > 0) Then
                        ''''''''''''''''''''''''
                        ' current user has favorited, but the other not
                        ''''''''''''''''''''''''

                        allowFavorite = False
                    ElseIf (dr("HasFavoritedMe") > 0 AndAlso dr("DidIFavorited") = 0) Then
                        ''''''''''''''''''''''''
                        ' current user has not favorited, but the other did
                        ''''''''''''''''''''''''

                        allowFavorite = True

                    Else
                        ''''''''''''''''''''''''
                        ' none user has favorited, the other user
                        ''''''''''''''''''''''''

                        allowFavorite = True
                    End If

                    uli.AllowFavorite = allowFavorite
                    uli.AllowUnfavorite = Not uli.AllowFavorite


                    uli.AllowWink = True
                    'Try
                    '    Dim rec As Boolean = clsUserDoes.IsAnyWinkOrIsAnyOffer(uli.OtherMemberProfileID, Me.MasterProfileId)
                    '    uli.AllowWink = (Not rec)
                    'Catch ex As Exception
                    'End Try
                    If (Not dr.IsNull("IsAnyWinkOrIsAnyOffer")) OrElse
                        (Not dr.IsNull("ProfilesCommunication_IsAnySentLike")) OrElse
                        (Not dr.IsNull("ProfilesCommunication_IsAnySentOffer")) Then

                        uli.AllowWink = False

                    End If


                    ' if like enabled and users have exchanged at least one message, then disable like
                    If (uli.AllowWink) Then
                        uli.AllowWink = (dr.IsNull("ProfilesCommunication_IsAnySentMessage"))


                        'Try
                        '    Dim rec As Boolean = clsUserDoes.IsAnyMessageExchanged(uli.OtherMemberProfileID, Me.MasterProfileId)
                        '    uli.AllowWink = (Not rec)
                        'Catch ex As Exception
                        'End Try
                    End If

                    If (Not uli.AllowWink) Then uli.AllowUnWink = True

                    'If (Not uli.AllowWink) Then
                    '    Try
                    '        Dim rec As EUS_Offer = clsUserDoes.GetMyPendingWink(uli.OtherMemberProfileID, Me.MasterProfileId)
                    '        uli.AllowUnWink = (rec IsNot Nothing)
                    '    Catch ex As Exception

                    '    End Try
                    'End If

                    uli.SendMessageUrl = ResolveUrl("~/Members/Conversation.aspx?p=" & HttpUtility.UrlEncode(uli.OtherMemberLoginName) & "&offerid=" & uli.OfferID)
                    uli.SendMessageUrlOnce = ResolveUrl("~/Members/Conversation.aspx?send=once&p=" & HttpUtility.UrlEncode(uli.OtherMemberLoginName) & "&offerid=" & uli.OfferID)
                    uli.SendMessageUrlMany = ResolveUrl("~/Members/Conversation.aspx?send=unl&p=" & HttpUtility.UrlEncode(uli.OtherMemberLoginName) & "&offerid=" & uli.OfferID)


                    If (Me.IsMale AndAlso dr("CommunicationUnl") = 0) Then
                        'Dim rec As EUS_Offer = clsUserDoes.GetLastOffer(uli.OtherMemberProfileID, Me.MasterProfileId)

                        'If (rec Is Nothing) Then
                        '    uli.CreateOfferUrl = ResolveUrl("~/Members/CreateOffer.aspx?offerto=" & HttpUtility.UrlEncode(uli.OtherMemberLoginName))
                        'End If

                        If (dr.IsNull("IsAnyLastOffer")) Then
                            uli.CreateOfferUrl = ResolveUrl("~/Members/CreateOffer.aspx?offerto=" & HttpUtility.UrlEncode(uli.OtherMemberLoginName))
                            uli.AllowActionsCreateOffer = True
                        End If
                        uli.AllowActionsMenu = True
                        uli.AllowActionsSendMessageOnce = True
                        uli.AllowActionsSendMessageMany = True
                    Else
                        uli.AllowSendMessage = True
                    End If

                    uli.AllowTooltipPopupSearch = True

                    If (uli.OtherMemberIsMale AndAlso dt.Columns.Contains("PersonalInfo_DrinkingHabitID")) Then
                        Try
                            If (dr("PersonalInfo_DrinkingHabitID").ToString().Length > 0) Then
                                Dim PersonalInfo_DrinkingHabitID As Integer = DirectCast(dr("PersonalInfo_DrinkingHabitID"), Integer)
                                If (PersonalInfo_DrinkingHabitID > 1) Then
                                    uli.OtherMemberDrinkingText = ProfileHelper.GetDrinkingString(PersonalInfo_DrinkingHabitID, uli.LAGID)
                                    If (PersonalInfo_DrinkingHabitID > 2) Then uli.OtherMemberDrinkingClass = "drinks"
                                End If
                            End If
                        Catch ex As Exception
                            WebErrorSendEmail(ex, "")
                        End Try
                    End If

                    If (uli.OtherMemberIsMale AndAlso dt.Columns.Contains("PersonalInfo_SmokingHabitID")) Then
                        Try
                            If (dr("PersonalInfo_SmokingHabitID").ToString().Length > 0) Then
                                Dim PersonalInfo_SmokingHabitID As Integer = DirectCast(dr("PersonalInfo_SmokingHabitID"), Integer)
                                If (PersonalInfo_SmokingHabitID > 1) Then
                                    uli.OtherMemberSmokingText = ProfileHelper.GetSmokingString(PersonalInfo_SmokingHabitID, uli.LAGID)
                                    If (PersonalInfo_SmokingHabitID > 2) Then uli.OtherMemberSmokingClass = "smokes"
                                End If
                            End If
                        Catch ex As Exception
                            WebErrorSendEmail(ex, "")
                        End Try
                    End If

                    If (uli.OtherMemberIsFemale AndAlso Not dr.IsNull("PersonalInfo_HairColorID")) Then
                        Try

                            Dim PersonalInfo_HairColorID As Integer = DirectCast(dr("PersonalInfo_HairColorID"), Integer)
                            If (PersonalInfo_HairColorID > 10) Then
                                Dim str As String = ProfileHelper.GetHairColorString(PersonalInfo_HairColorID, "US")
                                str = " clr-" & str.ToLower().Replace(" ", "-")
                                uli.OtherMemberHairClass = str
                                uli.OtherMemberHairTooltip = offersControl.CurrentPageData.GetCustomString("HairColor.Tooltip") & "&nbsp;" & ProfileHelper.GetHairColorString(PersonalInfo_HairColorID, uli.LAGID)
                            End If
                        Catch ex As Exception
                            WebErrorSendEmail(ex, "")
                        End Try
                    End If

                    uli.ZodiacString = globalStrings.GetCustomString("Zodiac" & ProfileHelper.ZodiacName(uli.OtherMemberBirthday), uli.LAGID)
                    Try
                        If (dt.Columns.Contains("LookingFor_RelationshipStatusID") AndAlso
                            dr("LookingFor_RelationshipStatusID").ToString().Length > 0) Then
                            Dim LookingFor_RelationshipStatusID As Integer = DirectCast(dr("LookingFor_RelationshipStatusID"), Integer)

                            Dim Tooltip1 As String = ""
                            If (uli.OtherMemberIsFemale) Then
                                If LookingFor_RelationshipStatusID = 6 Then
                                    Tooltip1 = offersControl.CurrentPageData.GetCustomString("RelationshipStatus.Married.Female.Tooltip")
                                Else
                                    Tooltip1 = offersControl.CurrentPageData.GetCustomString("RelationshipStatus.Single.Female.Tooltip")
                                End If
                            End If
                            If (String.IsNullOrEmpty(Tooltip1)) Then Tooltip1 = ProfileHelper.GetRelationshipStatusString(LookingFor_RelationshipStatusID, Me.Session("LagID"))

                            uli.OtherMemberRelationshipTooltip = Tooltip1
                            If uli.OtherMemberIsFemale Then
                                If LookingFor_RelationshipStatusID = 6 Then
                                    uli.OtherMemberRelationshipClass = "woman-icon married"
                                Else
                                    uli.OtherMemberRelationshipClass = "woman-icon female_single"
                                End If
                            Else
                                If LookingFor_RelationshipStatusID = 6 Then
                                    uli.OtherMemberRelationshipClass = "man-icon married"
                                Else
                                    uli.OtherMemberRelationshipClass = "man-icon male_single"
                                End If
                            End If

                        End If
                    Catch ex As Exception
                        WebErrorSendEmail(ex, "")
                    End Try

                    Try
                        If (dr("AreYouWillingToTravel").ToString() = Boolean.TrueString) Then
                            If uli.OtherMemberIsMale Then
                                uli.OtherMemberWantVisitText = offersControl.CurrentPageData.VerifyCustomString("msg_WantToVisitTitle.Male.Tooltip")
                            ElseIf uli.OtherMemberIsFemale Then
                                uli.OtherMemberWantVisitText = offersControl.CurrentPageData.VerifyCustomString("msg_WantToVisitTitle.Female.Tooltip")
                            End If
                        End If
                    Catch ex As Exception
                        WebErrorSendEmail(ex, "")
                    End Try

                    If uli.OtherMemberIsFemale Then

                        Try
                            If (dt.Columns.Contains("BreastSizeID") AndAlso dr("BreastSizeID").ToString().Length > 0) Then

                                Dim BreastSizeID As Integer = DirectCast(dr("BreastSizeID"), Integer)
                                Dim str As String = ProfileHelper.GetBreastSizeString(BreastSizeID, "US")
                                If (Not String.IsNullOrEmpty(str)) Then
                                    str = " size-" & str.ToLower().Replace("+", "plus")
                                    uli.OtherMemberBreastSizeClass = str
                                    uli.OtherMemberBreastSizeTooltip = offersControl.CurrentPageData.GetCustomString("BreastSize.Tooltip") & "&nbsp;" & ProfileHelper.GetBreastSizeString(BreastSizeID, uli.LAGID)
                                End If
                            End If

                        Catch ex As Exception
                            WebErrorSendEmail(ex, "")
                        End Try


                        Try
                            If (dt.Columns.Contains("WantJob") AndAlso dr("WantJob").ToString().Length > 0) Then
                                uli.OtherMemberWantJobTooltip = offersControl.CurrentPageData.VerifyCustomString("msg_WantJob_FEMALE.Tooltip")
                                uli.OtherMemberWantJobTooltip = uli.OtherMemberWantJobTooltip.Replace("[LOGIN-NAME]", uli.OtherMemberLoginName)
                            End If
                        Catch ex As Exception
                            WebErrorSendEmail(ex, "")
                        End Try
                    End If


                    If (Me.SessionVariables.MemberData.Status = ProfileStatusEnum.LIMITED) Then
                        uli.AllowUnblock = False
                        uli.AllowUnfavorite = False
                        uli.AllowFavorite = False
                        uli.AllowActionsMenu = False
                        uli.AllowSendMessage = False
                        uli.AllowWink = False
                        uli.AllowUnWink = False
                    End If

                    offersControl.UsersList.Add(uli)
                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "")
                End Try

            Next


        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub



    'Private Sub ShowBreastSize(currentRow As EUS_ProfilesRow)
    '    'imgBreastInfo.Visible = False
    '    'If ProfileHelper.IsFemale(currentRow.GenderId) Then

    '    '    Dim pbs As EUS_ProfileBreastSize =
    '    '                       (From itm In CMSDBDataContext.EUS_ProfileBreastSizes
    '    '                         Where itm.ProfileID = currentRow.ProfileID
    '    '                         Select itm).FirstOrDefault()

    '    '    If (pbs IsNot Nothing AndAlso pbs.BreastSizeID > 1) Then
    '    '        Dim str As String = ProfileHelper.GetBreastSizeString(pbs.BreastSizeID, "US")
    '    '        If (Not String.IsNullOrEmpty(str)) Then
    '    '            str = " size-" & str.ToLower().Replace("+", "plus")
    '    '            If (imgBreastInfo.Attributes("class") Is Nothing) Then
    '    '                imgBreastInfo.Attributes.Add("class", str)
    '    '            Else
    '    '                imgBreastInfo.Attributes("class") = imgBreastInfo.Attributes("class") & " " & str
    '    '            End If

    '    '            imgBreastInfo.Visible = True
    '    '            lblBreastInfo.Text = ProfileHelper.GetBreastSizeString(pbs.BreastSizeID, Session("LAGID"))

    '    '        End If
    '    '    End If

    '    'End If

    ' End Sub



    Private Sub BindFilteredResults()
        Try

            '   Dim sqlSet = False
            Dim sql As String = ""




            ' when BirthdayToday option set, allow other options also to filter view
            If (SearchSorting = SearchSortEnum.Birthday) Then
                'sql = sql & vbCrLf & " and  (DATEPART (dd,Birthday)=" & Date.UtcNow.Day & " and  DATEPART (mm,Birthday)=" & Date.UtcNow.Month & ")"
                sql = sql & vbCrLf & "  and  (CelebratingBirth>=DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE())))"
            End If


            If ((LastWhatToSearch And WhatToSearchEnum.ByUserName) > 0) Then

                Dim userName As String = QSCreterias.txtUserNameQText.Trim()
                clsSQLInjectionClear.ClearString(userName, 100)
                If (Len(userName) > 0) Then
                    sql = sql & vbCrLf & " and  EUS_Profiles.LoginName like N'" & userName & "%' "
                End If

            Else

                If (QSCreterias.ageMinSelectedValue > -1 AndAlso QSCreterias.ageMaxSelectedValue > -1) Then

                    Dim fromDate = DateAdd(DateInterval.Year, QSCreterias.ageMaxSelectedValue * -1, Date.Now).Date
                    fromDate = DateAdd(DateInterval.Year, -1, fromDate).Date

                    Dim toDate = DateAdd(DateInterval.Year, QSCreterias.ageMinSelectedValue * -1, Date.Now).Date

                    sql = sql & vbCrLf & " and  (EUS_Profiles.Birthday between '" & fromDate.ToString("yyyy-MM-dd hh:mm:ss") & "' and '" & toDate.ToString("yyyy-MM-dd hh:mm:ss") & "')"
                    '  sqlSet = True

                ElseIf (QSCreterias.ageMinSelectedValue > -1) Then
                    Dim toDate = DateAdd(DateInterval.Year, QSCreterias.ageMinSelectedValue * -1, Date.Now).Date

                    sql = sql & vbCrLf & " and (EUS_Profiles.Birthday < '" & toDate.ToString("yyyy-MM-dd hh:mm:ss") & "') "
                    '  sqlSet = True
                    '
                ElseIf (QSCreterias.ageMaxSelectedValue > -1) Then

                    Dim fromDate = DateAdd(DateInterval.Year, QSCreterias.ageMaxSelectedValue * -1, Date.Now).Date
                    fromDate = DateAdd(DateInterval.Year, -1, fromDate).Date

                    sql = sql & vbCrLf & " and (EUS_Profiles.Birthday between '" & fromDate.ToString("yyyy-MM-dd hh:mm:ss") & "' and getdate()) "
                    '  sqlSet = True
                End If

                If (QSCreterias.chkPhotosChecked) Then
                    sql = sql & " and exists (select customerId from EUS_CustomerPhotos phot1 where phot1.customerId = EUS_Profiles.ProfileId and phot1.HasAproved = 1 and (phot1.HasDeclined is null or phot1.HasDeclined=0) and (phot1.IsDeleted=0))"
                    '  sqlSet = True
                End If

                If (QSCreterias.chkPhotosPrivateChecked) Then
                    sql = sql & " and exists (select customerId from EUS_CustomerPhotos phot1 where phot1.customerId = EUS_Profiles.ProfileId and phot1.HasAproved = 1 and (phot1.HasDeclined is null or phot1.HasDeclined=0) and (phot1.IsDeleted=0) and phot1.DisplayLevel>0)"
                    ' sqlSet = True
                End If

                If (QSCreterias.chkWillingToTravelChecked) Then
                    sql = sql & " and (AreYouWillingToTravel=1)" 'or exists (select [ProfileLocationID] from [dbo].[EUS_ProfileLocation] pl where pl.[ProfileID] = EUS_Profiles.ProfileId)
                    ' sqlSet = True
                End If

                Dim sli As SearchListInfo = Nothing
                If (Me.IsMale) Then

                    ' creterias that only man see for searching women

                    If (ctlQSCrM.chkWillingToWorkChecked) Then

                        sli = ctlQSCrM.SelectedJobsList
                        If (Not sli.IsAllSelected AndAlso
                                sli.List.Count > 0) Then

                            Dim txt As String = GetListSubquery(sli.List)
                            sql = sql & " and exists (select [ProfileJobsID] from [dbo].[EUS_ProfileJobs] pj where pj.[ProfileID] = EUS_Profiles.ProfileId and [JobID] in (" & txt & "))"
                        Else
                            sql = sql & " and exists (select [ProfileJobsID] from [dbo].[EUS_ProfileJobs] pj where pj.[ProfileID] = EUS_Profiles.ProfileId)"
                        End If
                        '  sqlSet = True
                        '
                    End If


                    If (ctlQSCrM.HeightMin.HasValue AndAlso ctlQSCrM.HeightMax.HasValue) Then

                        sql = sql & vbCrLf & " and  EUS_Profiles.PersonalInfo_HeightID>=" & ctlQSCrM.HeightMin.Value & " and EUS_Profiles.PersonalInfo_HeightID<=" & ctlQSCrM.HeightMax.Value & "  "
                        '    sqlSet = True

                    ElseIf (ctlQSCrM.HeightMin.HasValue) Then
                        sql = sql & vbCrLf & " and EUS_Profiles.PersonalInfo_HeightID>=" & ctlQSCrM.HeightMin.Value & " "
                        ' sqlSet = True

                    ElseIf (ctlQSCrM.HeightMax.HasValue) Then
                        sql = sql & vbCrLf & " and EUS_Profiles.PersonalInfo_HeightID<=" & ctlQSCrM.HeightMax.Value & " "
                        '  sqlSet = True

                    End If


                    sli = ctlQSCrM.SelectedBreastSizesList
                    If (Not sli.IsAllSelected AndAlso
                            sli.List.Count > 0) Then

                        Dim txt As String = GetListSubquery(sli.List)
                        sql = sql & " and exists (select [ProfileBreastSizeID] from [dbo].[EUS_ProfileBreastSize] pb where pb.[ProfileID] = EUS_Profiles.ProfileId and pb.[BreastSizeID] in (" & txt & "))"
                        '   sqlSet = True
                    End If

                    sli = ctlQSCrM.SelectedHairColorsList
                    If (Not sli.IsAllSelected AndAlso
                        sli.List.Count > 0) Then

                        Dim txt As String = GetListSubquery(sli.List)
                        sql = sql & " and EUS_Profiles.PersonalInfo_HairColorID in (" & txt & ")"
                        '  sqlSet = True
                    End If


                ElseIf (Me.IsFemale) Then

                    ' creterias that only woman see for searching men

                    If (ctlQSCrW.chkWillingToWorkChecked) Then

                        sli = ctlQSCrW.SelectedJobsList
                        If (Not sli.IsAllSelected AndAlso
                                sli.List.Count > 0) Then

                            Dim txt As String = GetListSubquery(sli.List)
                            sql = sql & " and exists (select [ProfileJobsID] from [dbo].[EUS_ProfileJobs] pj where pj.[ProfileID] = EUS_Profiles.ProfileId and [JobID] in (" & txt & "))"
                        Else
                            sql = sql & " and exists (select [ProfileJobsID] from [dbo].[EUS_ProfileJobs] pj where pj.[ProfileID] = EUS_Profiles.ProfileId)"
                        End If
                        '  sqlSet = True

                    End If


                    If (ctlQSCrW.chkVIPChecked) Then
                        sql = sql & " and ((EUS_Profiles.AvailableCredits >=" & ProfileHelper.Config_UNLOCK_MESSAGE_READ_CREDITS & ") or exists (select CustomerCreditsId from EUS_CustomerCredits cc where cc.CustomerId = EUS_Profiles.ProfileId and cc.IsSubscription=1 and cc.DateTimeExpiration>getutcdate()))"
                        '    sqlSet = True
                    End If

                    If (ctlQSCrW.IncomeMin.HasValue AndAlso ctlQSCrW.IncomeMax.HasValue) Then

                        sql = sql & vbCrLf & " and EUS_Profiles.OtherDetails_AnnualIncomeID>=" & ctlQSCrW.IncomeMin.Value & " and EUS_Profiles.OtherDetails_AnnualIncomeID<=" & ctlQSCrW.IncomeMax.Value & "  "
                        '   sqlSet = True

                    ElseIf (ctlQSCrW.IncomeMin.HasValue) Then
                        sql = sql & vbCrLf & " and EUS_Profiles.OtherDetails_AnnualIncomeID>=" & ctlQSCrW.IncomeMin.Value & " "
                        '  sqlSet = True

                    ElseIf (ctlQSCrW.IncomeMax.HasValue) Then
                        sql = sql & vbCrLf & " and EUS_Profiles.OtherDetails_AnnualIncomeID<=" & ctlQSCrW.IncomeMax.Value & " "
                        '  sqlSet = True

                    End If


                    sli = ctlQSCrW.SelectedEducationTitlesList
                    If (Not sli.IsAllSelected AndAlso
                        sli.List.Count > 0) Then

                        Dim txt As String = GetListSubquery(sli.List)
                        sql = sql & " and EUS_Profiles.OtherDetails_EducationID in (" & txt & ")"
                        '    sqlSet = True
                    End If

                    sli = ctlQSCrW.SelectedSmokingHabitsList
                    If (Not sli.IsAllSelected AndAlso
                            sli.List.Count > 0) Then

                        Dim txt As String = GetListSubquery(sli.List)
                        sql = sql & " and EUS_Profiles.PersonalInfo_SmokingHabitID in (" & txt & ")"
                        '  sqlSet = True
                    End If

                    sli = ctlQSCrW.SelectedDrinkingHabitsList
                    If (Not sli.IsAllSelected AndAlso
                            sli.List.Count > 0) Then

                        Dim txt As String = GetListSubquery(sli.List)
                        sql = sql & " and EUS_Profiles.PersonalInfo_DrinkingHabitID in (" & txt & ")"
                        ' sqlSet = True
                    End If


                End If


                sli = QSCreterias.SelectedBodyTypesList
                If (Not sli.IsAllSelected AndAlso
                    sli.List.Count > 0) Then

                    Dim txt As String = GetListSubquery(sli.List)
                    sql = sql & " and EUS_Profiles.PersonalInfo_BodyTypeID in (" & txt & ")"
                    '  sqlSet = True

                End If


                sli = QSCreterias.SelectedSpokenLanguagesList
                If (Not sli.IsAllSelected AndAlso
                        sli.List.Count > 0) Then

                    Dim txt As String = GetListSubquery(sli.List)
                    sql = sql & " and exists (select [ProfileSpokenLangID] from [dbo].[EUS_ProfileSpokenLang] psl where psl.[ProfileID] = EUS_Profiles.ProfileId and psl.[SpokenLangId] in (" & txt & "))"
                    ' sqlSet = True
                End If


                sli = QSCreterias.SelectedDatingTypesList
                If (Not sli.IsAllSelected AndAlso
                        sli.List.Count > 0) Then

                    Dim lst As List(Of Integer) = sli.List

                    Dim txt As String = ""
                    For Each li As Integer In lst
                        Select Case li
                            Case TypeOfDatingEnum.AdultDating_Casual
                                txt = txt & vbCrLf & " or  EUS_Profiles.LookingFor_TypeOfDating_AdultDating_Casual=1"

                            Case TypeOfDatingEnum.Friendship
                                txt = txt & vbCrLf & " or  EUS_Profiles.LookingFor_TypeOfDating_Friendship=1"

                            Case TypeOfDatingEnum.LongTermRelationship
                                txt = txt & vbCrLf & " or  EUS_Profiles.LookingFor_TypeOfDating_LongTermRelationship=1"

                            Case TypeOfDatingEnum.MarriedDating
                                txt = txt & vbCrLf & " or  EUS_Profiles.LookingFor_TypeOfDating_MarriedDating=1"

                            Case TypeOfDatingEnum.MutuallyBeneficialArrangements
                                txt = txt & vbCrLf & " or  EUS_Profiles.LookingFor_TypeOfDating_MutuallyBeneficialArrangements=1"

                            Case TypeOfDatingEnum.ShortTermRelationship
                                txt = txt & vbCrLf & " or  EUS_Profiles.LookingFor_TypeOfDating_ShortTermRelationship=1"

                        End Select
                    Next

                    If (txt <> "") Then
                        txt = txt.Trim()
                        txt = txt.Remove(0, 2)
                        sql = sql & vbCrLf & " and  (" & txt & ")"
                        '    sqlSet = True
                    End If

                End If


                If (QSCreterias.chkOnlineChecked) Then
                    Dim LastActivityUTCDate As DateTime
                    Dim hours As Integer = clsConfigValues.Get__members_online_recently_hours()
                    If (hours > 0) Then
                        LastActivityUTCDate = Date.UtcNow.AddHours(-hours)
                    Else
                        Dim mins As Integer = clsConfigValues.Get__members_online_minutes()
                        LastActivityUTCDate = Date.UtcNow.AddMinutes(-mins)
                    End If

                    sql = sql & " and (EUS_Profiles.IsOnline=1 and EUS_Profiles.LastActivityDateTime>='" & LastActivityUTCDate.ToString("yyyy-MM-dd HH:mm") & "')" & vbCrLf & _
                    " and (not exists (SELECT  [EUS_ProfilesPrivacySettingsID] FROM [EUS_ProfilesPrivacySettings]  where ([ProfileID]=EUS_Profiles.ProfileId Or MirrorProfileId=EUS_Profiles.ProfileId) and [PrivacySettings_ShowMeOffline]=1))"
                    '  sqlSet = True
                End If


                'If (ddlDistance.SelectedValue <> "0") Then
                '    Dim profile As EUS_Profile = Me.GetCurrentProfile()
                '    Dim _list As String = clsGeoHelper.GetNearestPostalCodesCommaSeparatedString(profile.Country, profile.Zip, ddlDistance.SelectedValue, Me.GetLag())
                '    If (Not String.IsNullOrEmpty(_list)) Then
                '        sql = sql & " and EUS_Profiles.Zip IN (" & _list & ")"
                '    End If
                '    sqlSet = True
                'End If


                'If (sqlSet) Then

                '    cmd.CommandText = sql
                '    Dim dt As DataTable = DataHelpers.GetDataTable(cmd)

                '    If (dt.Rows.Count > 0) Then

                '        For Each dr As DataRow In dt.Rows
                '            sb.Append(dr("ProfileId") & ",")
                '        Next
                '        sb = sb.Remove(sb.Length - 1, 1)
                '    End If

                'End If
            End If


            mvSearchMain.SetActiveView(vwSearchBrowse)
            divNoResultsChangeCriteria.Visible = True
            divNoresultsToManyParams.Visible = False


            'Dim checkUserNameAndFilters As Boolean = True
            'If (Len(sqlUserName) > 0 AndAlso Len(sql) > 0) Then
            '    checkUserNameAndFilters = False
            '    divUserNameAndFiltersDisabled.Visible = True
            '    divNoResultsChangeCriteria.Visible = False
            '    divNoresultsToManyParams.Visible = False

            '    msg_divUserNameAndFiltersDisabled.Text = CurrentPageData.GetCustomString("msg_divUserNameAndFiltersDisabled")
            'ElseIf (Len(sqlUserName) > 0) Then
            '    sql = sqlUserName
            'End If


            'If (checkUserNameAndFilters) Then
            Try

                If (Not String.IsNullOrEmpty(Me.LastAdvancedSearchquery)) Then sql = Me.LastAdvancedSearchquery

                Dim NumberOfRecordsToReturn As Integer = Me.GetRecordsToRetrieve()

                Dim prms As New clsSearchHelperParameters()
                prms.CurrentProfileId = Me.MasterProfileId
                prms.ReturnRecordsWithStatus = ProfileStatusEnum.Approved
                prms.SearchSort = Me.SearchSorting
                prms.zipstr = Me.SessionVariables.MemberData.Zip
                prms.latitudeIn = Me.SessionVariables.MemberData.latitude
                prms.longitudeIn = Me.SessionVariables.MemberData.longitude

                If (QSCreterias.ddlDistanceSelectedValue > 0) Then
                    prms.Distance = QSCreterias.ddlDistanceSelectedValue
                End If

                prms.LookingFor_ToMeetMaleID = Me.SessionVariables.MemberData.LookingFor_ToMeetMaleID
                prms.LookingFor_ToMeetFemaleID = Me.SessionVariables.MemberData.LookingFor_ToMeetFemaleID
                prms.NumberOfRecordsToReturn = 0
                prms.AdditionalWhereClause = sql
                prms.performCount = True
                prms.rowNumberMin = NumberOfRecordsToReturn - Me.ItemsPerPage
                prms.rowNumberMax = NumberOfRecordsToReturn

                Dim countRows As Integer
                searchL.FillKeyStrings()
                Using ds As DataSet = clsSearchHelper.GetMembersToSearchDataTable(prms)
                    If (prms.performCount) Then
                        countRows = ds.Tables(0).Rows(0)(0)
                        If (countRows > 0) Then
                            Using dt As DataTable = ds.Tables(1)
                                FillOfferControlList(dt, searchL)
                                divNoResultsChangeCriteria.Visible = False
                                divNoresultsToManyParams.Visible = False
                            End Using

                        End If
                    Else
                        countRows = ds.Tables(0).Rows.Count
                        If (countRows > 0) Then
                            Using dt As DataTable = ds.Tables(0)
                                FillOfferControlList(dt, searchL)
                                divNoResultsChangeCriteria.Visible = False
                                divNoresultsToManyParams.Visible = False
                            End Using
                        End If
                    End If

                    searchL.DataBind()
                    SetPager(countRows)
                End Using


            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")

                SetPager(0)
                searchL.UsersList = Nothing
                searchL.DataBind()
            End Try
            'End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Private Sub BindAdvancedSearchResults()
        Try
            Dim sql As String = ""



            'If (txtUserName.Text.Trim() <> "") Then
            '    Dim username As String = txtUserName.Text
            '    ClsSQLInjectionClear.ClearString(username, 100)
            '    sql = sql & vbCrLf & " and  EUS_Profiles.LoginName like N'" & username & "%' "
            '    'cmd.Parameters.Add(New SqlParameter("LoginName", txtUserName.Text))

            'Else

            If (cbHeightMin.SelectedIndex > 0 AndAlso cbHeightMax.SelectedIndex > 0) Then

                Dim minHeight = cbHeightMin.SelectedItem.Value
                Dim maxHeight = cbHeightMax.SelectedItem.Value

                sql = sql & vbCrLf & " and  EUS_Profiles.PersonalInfo_HeightID between  '" & minHeight & "' and '" & maxHeight & "' "

            ElseIf (cbHeightMin.SelectedIndex > 0) Then

                Dim minHeight = cbHeightMin.SelectedItem.Value
                sql = sql & vbCrLf & " and  EUS_Profiles.PersonalInfo_HeightID =  '" & minHeight & "' "

            ElseIf (cbHeightMax.SelectedIndex > 0) Then

                Dim maxHeight = cbHeightMax.SelectedItem.Value
                sql = sql & vbCrLf & " and  EUS_Profiles.PersonalInfo_HeightID =  '" & maxHeight & "' "

            End If


            sql = GetListSubquery(sql, cbBodyType, "EUS_Profiles.PersonalInfo_BodyTypeID")
            sql = GetListSubquery(sql, cbEthnicity, "EUS_Profiles.PersonalInfo_EthnicityID")
            sql = GetListSubquery(sql, cbEyeColor, "EUS_Profiles.PersonalInfo_EyeColorID")
            sql = GetListSubquery(sql, cbHairColor, "EUS_Profiles.PersonalInfo_HairColorID")


            If (ageMin2.SelectedValue <> "-1" AndAlso ageMax2.SelectedValue <> "-1") Then

                Dim fromDate = DateAdd(DateInterval.Year, Double.Parse(ageMax2.SelectedValue) * -1, Date.Now).Date
                fromDate = DateAdd(DateInterval.Year, -1, fromDate).Date

                Dim toDate = DateAdd(DateInterval.Year, Double.Parse(ageMin2.SelectedValue) * -1, Date.Now).Date


                sql = sql & vbCrLf & " and  EUS_Profiles.Birthday between '" & fromDate.ToString("yyyy-MM-dd hh:mm:ss.mmm") & "' and '" & toDate.ToString("yyyy-MM-dd hh:mm:ss.mmm") & "'  "
                'cmd.Parameters.Add(New SqlParameter("FromDate", fromDate))
                'cmd.Parameters.Add(New SqlParameter("ToDate", toDate))

            ElseIf (ageMin2.SelectedValue <> "-1") Then

                Dim toDate = DateAdd(DateInterval.Year, Double.Parse(ageMin2.SelectedValue) * -1, Date.Now).Date
                sql = sql & vbCrLf & " and EUS_Profiles.Birthday < '" & toDate.ToString("yyyy-MM-dd hh:mm:ss.mmm") & "' "

                'cmd.Parameters.Add(New SqlParameter("ToDate", toDate))

            ElseIf (ageMax2.SelectedValue <> "-1") Then

                Dim fromDate = DateAdd(DateInterval.Year, Double.Parse(ageMax2.SelectedValue) * -1, Date.Now).Date
                fromDate = DateAdd(DateInterval.Year, -1, fromDate).Date
                sql = sql & vbCrLf & " and EUS_Profiles.Birthday between '" & fromDate.ToString("yyyy-MM-dd hh:mm:ss.mmm") & "' and getdate() "

                'cmd.Parameters.Add(New SqlParameter("FromDate", fromDate))

            End If


            Dim cbDatingTypesAll As Boolean = True
            Dim cbDatingTypes As String = ""
            For Each li As ListItem In cbDatingType.Items
                If (li.Selected) Then
                    Dim TypeOfDatingId = li.Value
                    'Dim TypeOfDatingId = Lists.gDSLists.EUS_LISTS_TypeOfDating.Single(Function(itm As DSLists.EUS_LISTS_TypeOfDatingRow) itm.US = value OrElse itm.GR = value OrElse itm.HU = value).TypeOfDatingId

                    Select Case TypeOfDatingId
                        Case TypeOfDatingEnum.AdultDating_Casual
                            cbDatingTypes = cbDatingTypes & vbCrLf & " or  EUS_Profiles.LookingFor_TypeOfDating_AdultDating_Casual= 1"

                        Case TypeOfDatingEnum.Friendship
                            cbDatingTypes = cbDatingTypes & vbCrLf & " or  EUS_Profiles.LookingFor_TypeOfDating_Friendship= 1"

                        Case TypeOfDatingEnum.LongTermRelationship
                            cbDatingTypes = cbDatingTypes & vbCrLf & " or  EUS_Profiles.LookingFor_TypeOfDating_LongTermRelationship= 1"

                        Case TypeOfDatingEnum.MarriedDating
                            cbDatingTypes = cbDatingTypes & vbCrLf & " or  EUS_Profiles.LookingFor_TypeOfDating_MarriedDating= 1"

                        Case TypeOfDatingEnum.MutuallyBeneficialArrangements
                            cbDatingTypes = cbDatingTypes & vbCrLf & " or  EUS_Profiles.LookingFor_TypeOfDating_MutuallyBeneficialArrangements= 1"

                        Case TypeOfDatingEnum.ShortTermRelationship
                            cbDatingTypes = cbDatingTypes & vbCrLf & " or  EUS_Profiles.LookingFor_TypeOfDating_ShortTermRelationship= 1"

                    End Select
                End If

                If (Not li.Selected) Then cbDatingTypesAll = False
            Next
            If (cbDatingTypesAll) Then
                cbDatingTypes = cbDatingTypes & vbCrLf & " or  EUS_Profiles.LookingFor_TypeOfDating_AdultDating_Casual is null "
                cbDatingTypes = cbDatingTypes & vbCrLf & " or  EUS_Profiles.LookingFor_TypeOfDating_Friendship is null "
                cbDatingTypes = cbDatingTypes & vbCrLf & " or  EUS_Profiles.LookingFor_TypeOfDating_LongTermRelationship is null "
                cbDatingTypes = cbDatingTypes & vbCrLf & " or  EUS_Profiles.LookingFor_TypeOfDating_MarriedDating is null "
                cbDatingTypes = cbDatingTypes & vbCrLf & " or  EUS_Profiles.LookingFor_TypeOfDating_MutuallyBeneficialArrangements is null "
                cbDatingTypes = cbDatingTypes & vbCrLf & " or  EUS_Profiles.LookingFor_TypeOfDating_ShortTermRelationship is null "
            End If
            If (cbDatingTypes <> "") Then
                cbDatingTypes = cbDatingTypes.Trim()
                cbDatingTypes = cbDatingTypes.Remove(0, 2)
                sql = sql & vbCrLf & " and  (" & cbDatingTypes & ")"
            End If


            sql = GetListSubquery(sql, cbChildren, "EUS_Profiles.PersonalInfo_ChildrenID")
            sql = GetListSubquery(sql, cbRelationshipStatus, "EUS_Profiles.LookingFor_RelationshipStatusID")
            sql = GetListSubquery(sql, cbEducation, "EUS_Profiles.OtherDetails_EducationID")
            sql = GetListSubquery(sql, cbIncome, "EUS_Profiles.OtherDetails_AnnualIncomeID")
            sql = GetListSubquery(sql, cbReligion, "EUS_Profiles.PersonalInfo_ReligionID")
            sql = GetListSubquery(sql, cbSmoking, "EUS_Profiles.PersonalInfo_SmokingHabitID")
            sql = GetListSubquery(sql, cbDrinking, "EUS_Profiles.PersonalInfo_DrinkingHabitID")

            Dim location As String = ""

            Dim zipstr As String = AppUtils.GetZip(txtZip.Text)
            If (Len(zipstr) > 0) Then
                location = <sql><![CDATA[
and  Zip = N'@ZIP' ]]></sql>.Value

                location = location.Replace("@ZIP", zipstr)

            ElseIf (cbCountry.SelectedItem.Value <> "-1") Then

                If (Not String.IsNullOrEmpty(GetRegionName()) AndAlso _
                    Not String.IsNullOrEmpty(GetCityName())) Then
                    location = <sql><![CDATA[
and 
(
    City in (
        select distinct lag1.city 
        from dbo.SYS_GEO_[tblCountryCode] as lag1
        left join SYS_GEO_[tblCountryCode] as lag2 on lag1.postcode= lag2.postcode
        where lag2.city = N'@CITY'
    ) or 
    City =  N'@CITY'
)
and (
    Region in (
        select distinct lag1.region1 
        from dbo.SYS_GEO_[tblCountryCode] as lag1
        left join SYS_GEO_[tblCountryCode] as lag2 on lag1.postcode= lag2.postcode
        where lag2.region1 = N'@REGION1'
    ) or 
    Region =   N'@REGION1'
)
and  Country = N'@COUNTRY'
]]></sql>.Value


                    location = location.Replace("@CITY", GetCityName())
                    location = location.Replace("@REGION1", GetRegionName())
                    location = location.Replace("@COUNTRY", cbCountry.SelectedItem.Value)

                ElseIf (Not String.IsNullOrEmpty(GetRegionName())) Then
                    location = <sql><![CDATA[
and (
    Region in (
        select distinct lag1.region1 
        from dbo.SYS_GEO_[tblCountryCode] as lag1
        left join SYS_GEO_[tblCountryCode] as lag2 on lag1.postcode= lag2.postcode
        where lag2.region1 = N'@REGION1'
    ) or 
    Region =   N'@REGION1'
)
and  Country = N'@COUNTRY'
]]></sql>.Value
                    location = location.Replace("@REGION1", GetRegionName())
                    location = location.Replace("@COUNTRY", cbCountry.SelectedItem.Value)

                Else
                    location = <sql><![CDATA[
and  Country = N'@COUNTRY'
]]></sql>.Value

                    location = location.Replace("@COUNTRY", cbCountry.SelectedItem.Value)
                End If

                location = location.Replace("[tblCountryCode]", clsGeoHelper.GetValidCountryCode(cbCountry.SelectedItem.Value))
            End If

            If (location <> "") Then
                sql = sql & vbCrLf & location
                location = ""
            End If



            If (chkTravelOnDate.Checked) Then
                sql = sql & vbCrLf & " and  EUS_Profiles.AreYouWillingToTravel= 1"
            End If


            'If (ddldistanceAdv.SelectedValue <> "0") Then
            '    Dim profile As EUS_Profile = Me.GetCurrentProfile()
            '    Dim _list As String = clsGeoHelper.GetNearestPostalCodesCommaSeparatedString(profile.Country, profile.Zip, ddldistanceAdv.SelectedValue, Me.GetLag())
            '    If (Not String.IsNullOrEmpty(_list)) Then
            '        sql = sql & " and EUS_Profiles.Zip IN (" & _list & ")"
            '    End If
            'End If

            'End If

            Using cmd As SqlCommand = New SqlCommand()
                Using con As New SqlConnection(ModGlobals.ConnectionString)
                    cmd.Connection = con
                    cmd.CommandText = sql
                    SaveSearch(GetExecutionStatement(cmd))
                End Using
            End Using






            mvSearchMain.SetActiveView(vwSearchBrowse)
            divNoResultsChangeCriteria.Visible = False
            divNoresultsToManyParams.Visible = True

            Dim prms As New clsSearchHelperParameters()
            Try
                'If (sb.Length > 0) Then
                'Dim sqlProc As String = " EXEC	[dbo].[GetMembersToSearch] " & _
                '  " 	@CurrentProfileId = " & Me.MasterProfileId & "," & _
                '  " 	@ReturnRecordsWithStatus = " & CType(ProfileStatusEnum.Approved, Integer) & "," & _
                '  " 	@ProfilesIdList = '" & sb.ToString() & "'"


                'Dim dt As DataTable = DataHelpers.GetDataTable(sqlProc)
                Me.LastAdvancedSearchquery = sql
                Dim NumberOfRecordsToReturn As Integer = Me.GetRecordsToRetrieve()

                prms.CurrentProfileId = Me.MasterProfileId
                prms.ReturnRecordsWithStatus = ProfileStatusEnum.Approved
                prms.SearchSort = Me.SearchSorting
                prms.zipstr = Me.SessionVariables.MemberData.Zip
                prms.latitudeIn = Me.SessionVariables.MemberData.latitude
                prms.longitudeIn = Me.SessionVariables.MemberData.longitude

                If (ddldistanceAdv.SelectedValue > 0) Then
                    prms.Distance = ddldistanceAdv.SelectedValue
                End If

                prms.LookingFor_ToMeetMaleID = Me.SessionVariables.MemberData.LookingFor_ToMeetMaleID
                prms.LookingFor_ToMeetFemaleID = Me.SessionVariables.MemberData.LookingFor_ToMeetFemaleID
                prms.NumberOfRecordsToReturn = 0 'Me.GetRecordsToRetrieve()
                prms.AdditionalWhereClause = sql
                prms.performCount = True
                prms.rowNumberMin = NumberOfRecordsToReturn - Me.ItemsPerPage
                prms.rowNumberMax = NumberOfRecordsToReturn

                'Dim ds As DataSet = clsSearchHelper.GetMembersToSearchDataTable(prms)
                'Dim countRows As Integer = ds.Tables(0).Rows.Count
                'If (countRows > 0) Then
                '    Dim dt As DataTable = ds.Tables(0)
                '    Me.Pager.PageIndex = -1
                '    FillOfferControlList(dt, searchL)
                '    divNoResultsChangeCriteria.Visible = False
                '    divNoresultsToManyParams.Visible = False
                'End If

                Dim countRows As Integer
                Using ds As DataSet = clsSearchHelper.GetMembersToSearchDataTable(prms)


                    Me.Pager.PageIndex = -1
                    searchL.FillKeyStrings()
                    If (prms.performCount) Then
                        countRows = ds.Tables(0).Rows(0)(0)
                        If (countRows > 0) Then
                            Using dt As DataTable = ds.Tables(1)
                                FillOfferControlList(dt, searchL)
                                divNoResultsChangeCriteria.Visible = False
                                divNoresultsToManyParams.Visible = False
                            End Using

                        End If
                    Else
                        countRows = ds.Tables(0).Rows.Count
                        If (countRows > 0) Then
                            Using dt As DataTable = ds.Tables(0)
                                FillOfferControlList(dt, searchL)
                                divNoResultsChangeCriteria.Visible = False
                                divNoresultsToManyParams.Visible = False
                            End Using
                        End If
                    End If

                    searchL.DataBind()
                    SetPager(countRows)
                End Using
                'Else
                'searchL.DataBind()
                'SetPager(0)
                'End If

            Catch ex As Exception
                Dim extraMessage As String = ""
                Try
                    extraMessage = AppUtils.ConvertObjectToXML(prms, prms.GetType())
                    If (Not String.IsNullOrEmpty(extraMessage)) Then
                        extraMessage = extraMessage.Replace("<?xml version=""1.0"" encoding=""utf-16""?>", "")
                        extraMessage = "SERIALIAZED object follows " & vbCrLf & (New String("-"c, 30)) & extraMessage & vbCrLf & (New String("-"c, 30))
                    End If
                Catch
                End Try

                WebErrorMessageBox(Me, ex, extraMessage)

                SetPager(0)
                searchL.UsersList = Nothing
                searchL.DataBind()
            End Try

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Private Sub LoadSavedSearch(sql As String, distance As Integer?)
        Try
            'Dim sb As New StringBuilder
            'Dim dt As DataTable = DataHelpers.GetDataTable(sql)
            'If (dt.Rows.Count > 0) Then

            '    For Each dr As DataRow In dt.Rows
            '        sb.Append(dr("ProfileId") & ",")
            '    Next
            '    sb = sb.Remove(sb.Length - 1, 1)
            'End If




            mvSearchMain.SetActiveView(vwSearchBrowse)
            divNoResultsChangeCriteria.Visible = False
            divNoresultsToManyParams.Visible = True

            Try

                'If (sb.Length > 0) Then
                'Dim sqlProc As String = " EXEC	[dbo].[GetMembersToSearch] " & _
                '  " 	@CurrentProfileId = " & Me.MasterProfileId & "," & _
                '  " 	@ReturnRecordsWithStatus = " & CType(ProfileStatusEnum.Approved, Integer) & "," & _
                '  " 	@ProfilesIdList = '" & sb.ToString() & "'"


                'Dim dt2 As DataTable = DataHelpers.GetDataTable(sqlProc)

                QSCreterias.Distance_SelectComboItem(distance.GetValueOrDefault())
                'ClsCombos.SelectComboItem(ddlDistance.ListControl, distance.GetValueOrDefault())

                Me.LastAdvancedSearchquery = sql
                Dim NumberOfRecordsToReturn As Integer = Me.GetRecordsToRetrieve()

                Dim prms As New clsSearchHelperParameters()
                prms.CurrentProfileId = Me.MasterProfileId
                prms.ReturnRecordsWithStatus = ProfileStatusEnum.Approved
                prms.SearchSort = Me.SearchSorting
                prms.zipstr = Me.SessionVariables.MemberData.Zip
                prms.latitudeIn = Me.SessionVariables.MemberData.latitude
                prms.longitudeIn = Me.SessionVariables.MemberData.longitude

                If (QSCreterias.ddlDistanceSelectedValue > 0) Then
                    prms.Distance = QSCreterias.ddlDistanceSelectedValue
                End If

                prms.LookingFor_ToMeetMaleID = Me.SessionVariables.MemberData.LookingFor_ToMeetMaleID
                prms.LookingFor_ToMeetFemaleID = Me.SessionVariables.MemberData.LookingFor_ToMeetFemaleID
                prms.NumberOfRecordsToReturn = 0 'Me.GetRecordsToRetrieve()
                prms.AdditionalWhereClause = sql
                prms.performCount = True
                prms.rowNumberMin = NumberOfRecordsToReturn - Me.ItemsPerPage
                prms.rowNumberMax = NumberOfRecordsToReturn

                Dim countRows As Integer
                Dim ds As DataSet = clsSearchHelper.GetMembersToSearchDataTable(prms)
                searchL.FillKeyStrings()
                If (prms.performCount) Then
                    countRows = ds.Tables(0).Rows(0)(0)
                    If (countRows > 0) Then
                        Dim dt2 As DataTable = ds.Tables(1)
                        FillOfferControlList(dt2, searchL)
                        divNoResultsChangeCriteria.Visible = False
                        divNoresultsToManyParams.Visible = False
                    End If
                Else
                    countRows = ds.Tables(0).Rows.Count
                    If (countRows > 0) Then
                        Dim dt2 As DataTable = ds.Tables(0)
                        FillOfferControlList(dt2, searchL)
                        divNoResultsChangeCriteria.Visible = False
                        divNoresultsToManyParams.Visible = False
                    End If
                End If

                searchL.DataBind()
                SetPager(countRows)
                'Else
                'searchL.DataBind()
                'SetPager(0)
                'End If

            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")

                SetPager(0)
                searchL.UsersList = Nothing
                searchL.DataBind()
            End Try

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub



    Private Sub LoadAdvancedSearch()
        If (cbCountry.Items.Count = 0) Then

            'Web.ClsCombos.FillCombo(gLAG.ConnectionString, "SYS_CountriesGEO", "PrintableName", "Iso", cbCountry, True, False, "PrintableName", "ASC")
            Web.ClsCombos.FillComboUsingDatatable(SYS_CountriesGEOHelper.gDSListsGEO__IsEnabled_PrintableNameASC.SYS_CountriesGEO, "PrintableName", "Iso", cbCountry, True, "PrintableName")
            Dim def As DevExpress.Web.ASPxEditors.ListEditItem = Nothing 'cbCountry.Items.FindByValue(Session("GEO_COUNTRY_CODE"))
            If (def IsNot Nothing) Then
                def.Selected = True
            Else
                def = New DevExpress.Web.ASPxEditors.ListEditItem(CurrentPageData.GetCustomString("cbCountry_default"), "-1")
                def.Selected = True
                cbCountry.Items.Insert(0, def)
            End If


            Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_Height, Me.GetLag(), "HeightId", cbHeightMin, True, "US")
            If (cbHeightMin.Items.Count > 0) Then cbHeightMin.Items(0).Selected = True
            'cbHeightMin.Items(1).Selected = True

            Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_Height, Me.GetLag(), "HeightId", cbHeightMax, True, "US")
            If (cbHeightMax.Items.Count > 0) Then cbHeightMax.Items(0).Selected = True
            'cbHeightMax.Items(cbHeightMax.Items.Count - 1).Selected = True

            Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_BodyType, Me.GetLag(), "BodyTypeId", cbBodyType, True)
            If (cbBodyType.Items.Count > 0) Then
                cbBodyType.Items.RemoveAt(0)
                SelectAllCheckBoxes(cbBodyType, True)
            End If

            Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_Ethnicity, Me.GetLag(), "EthnicityId", cbEthnicity, True)
            If (cbEthnicity.Items.Count > 0) Then
                cbEthnicity.Items.RemoveAt(0)
                SelectAllCheckBoxes(cbEthnicity, True)
            End If

            Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_EyeColor, Me.GetLag(), "EyeColorId", cbEyeColor, True)
            If (cbEyeColor.Items.Count > 0) Then
                cbEyeColor.Items.RemoveAt(0)
                SelectAllCheckBoxes(cbEyeColor, True)
            End If

            Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_HairColor, Me.GetLag(), "HairColorId", cbHairColor, True)
            If (cbHairColor.Items.Count > 0) Then
                cbHairColor.Items.RemoveAt(0)
                SelectAllCheckBoxes(cbHairColor, True)

                'If (Me.IsMale) Then
                '    ' remove shaven value
                '    Dim li As ListItem = cbHairColor.Items.FindByValue("21")
                '    cbHairColor.Items.Remove(li)
                'End If
            End If

            Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_RelationshipStatus, Me.GetLag(), "RelationshipStatusId", cbRelationshipStatus, True)
            If (cbRelationshipStatus.Items.Count > 0) Then
                cbRelationshipStatus.Items.RemoveAt(0)
                SelectAllCheckBoxes(cbRelationshipStatus, True)
            End If

            Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_ChildrenNumber, Me.GetLag(), "ChildrenNumberId", cbChildren, True)
            If (cbChildren.Items.Count > 0) Then
                cbChildren.Items.RemoveAt(0)
                SelectAllCheckBoxes(cbChildren, True)
            End If

            Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_TypeOfDating, Me.GetLag(), "TypeOfDatingId", cbDatingType, True)
            SelectAllCheckBoxes(cbDatingType, True)

            Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_Education, Me.GetLag(), "EducationId", cbEducation, True)
            If (cbEducation.Items.Count > 0) Then
                cbEducation.Items.RemoveAt(0)
                SelectAllCheckBoxes(cbEducation, True)
            End If

            Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_Income, Me.GetLag(), "IncomeId", cbIncome, True)
            If (cbIncome.Items.Count > 0) Then
                cbIncome.Items.RemoveAt(0)
                'cbIncome.Items.RemoveAt(0)
                SelectAllCheckBoxes(cbIncome, True)
            End If

            'Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_NetWorth, Me.GetLag(), "NetWorthId", cbNetWorth, True)
            'cbNetWorth.Items.RemoveAt(0)
            'cbNetWorth.Items.RemoveAt(0)
            'SelectAllCheckBoxes(cbNetWorth, True)

            Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_Religion, Me.GetLag(), "ReligionId", cbReligion, True)
            If (cbReligion.Items.Count > 0) Then
                cbReligion.Items.RemoveAt(0)
                SelectAllCheckBoxes(cbReligion, True)
            End If

            Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_Smoking, Me.GetLag(), "SmokingId", cbSmoking, True)
            If (cbSmoking.Items.Count > 0) Then
                cbSmoking.Items.RemoveAt(0)
                SelectAllCheckBoxes(cbSmoking, True)
            End If


            Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_Drinking, Me.GetLag(), "DrinkingId", cbDrinking, True)
            If (cbDrinking.Items.Count > 0) Then
                cbDrinking.Items.RemoveAt(0)
                SelectAllCheckBoxes(cbDrinking, True)
            End If

            'Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_Gender, Me.GetLag(), "GenderId", cbLooking, True)
            'SelectAllCheckBoxes(cbLooking, True)


            ''cbLooking.Items.add(New ListItem(
            'If (Not Me.GetCurrentProfile().LookingFor_ToMeetFemaleID) Then
            '    cbLooking.Items.Remove(cbLooking.Items.FindByValue(ProfileHelper.gFemaleGender.GenderId))
            'End If
            'If (Not Me.GetCurrentProfile().LookingFor_ToMeetMaleID) Then
            '    cbLooking.Items.Remove(cbLooking.Items.FindByValue(ProfileHelper.gMaleGender.GenderId))
            'End If
        End If


        If (cbCountry.SelectedItem IsNot Nothing) Then
            FillRegionCombo(cbCountry.SelectedItem.Value)
            cbCountry_SelectedIndexChanged(cbCountry, Nothing)
            ClsCombos.SelectComboItem(cbRegion, cbRegion.Text)

            If (cbRegion.SelectedItem IsNot Nothing) Then
                FillCityCombo(cbRegion.SelectedItem.Value)
                cbRegion_SelectedIndexChanged(cbRegion, Nothing)
                ClsCombos.SelectComboItem(cbCity, cbCity.Text)
            End If
        End If


        'If (hdfLocationStatus.Value = "zip") Then
        '    liLocationsRegion.Attributes.CssStyle.Add("display", "none")
        '    liLocationsCity.Attributes.CssStyle.Add("display", "none")
        '    msg_SelectRegion.Attributes.CssStyle.Remove("display")
        'End If

        If (cbRegion.Items.Count = 0) Then cbRegion.Text = CurrentPageData.GetCustomString("msg_NotFound")
        If (cbCity.Items.Count = 0) Then cbCity.Text = CurrentPageData.GetCustomString("msg_NotFound")

    End Sub

    Private Sub SaveSearch(sql As String)
        Try
            If (hdfNewName.Value = "#DO_NOT_SAVE#") Then
                'hdfNewName.Value = ""
                hdfSelectedName.Value = "-1"
                Return
            End If

            Dim SavedSearchId As Integer
            If (hdfSelectedName.Value.Trim() <> "") Then Integer.TryParse(hdfSelectedName.Value.Trim(), SavedSearchId)
            Dim isSaved As Boolean = False

            If (SavedSearchId > 0) Then
                Using cmdb As CMSDBDataContext = Me.GetCMSDBDataContext


                    Dim savedSearch1 = (From itm In cmdb.EUS_SavedSearches
                        Where itm.SavedSearchId = SavedSearchId AndAlso itm.ProfileId = Me.MasterProfileId
                        Select itm).SingleOrDefault()

                    If (savedSearch1 IsNot Nothing) Then
                        savedSearch1.CreatedDate = Date.UtcNow
                        savedSearch1.Distance = ddldistanceAdv.SelectedValue
                        savedSearch1.SearchText = sql

                        cmdb.SubmitChanges()
                        isSaved = True
                    End If
                End Using
            End If


            If (Not isSaved) Then
                Using cmdb As CMSDBDataContext = Me.GetCMSDBDataContext


                    Dim newName As String = ""
                    If (hdfNewName.Value.Trim() <> "") Then
                        newName = hdfNewName.Value

                        ' check if search exists
                        Try
                            Dim savedSearch1 = (From itm In cmdb.EUS_SavedSearches
                                                Where itm.Name = newName AndAlso itm.ProfileId = Me.MasterProfileId
                                                Select itm).SingleOrDefault()

                            If (savedSearch1 IsNot Nothing) Then
                                newName = newName & " (" & Date.Now.ToString("d/M/yy, HH:mm", CultureInfo.CreateSpecificCulture("en-US")) & ")"
                            End If
                        Catch
                            newName = ""
                        End Try
                    End If

                    If (newName = "") Then
                        newName = Date.Now.ToString("dddd dd MMMM, HH:mm", CultureInfo.CreateSpecificCulture("en-US"))
                    End If




                    Dim savedSearch As New EUS_SavedSearch()
                    savedSearch.CreatedDate = Date.UtcNow
                    savedSearch.Distance = ddldistanceAdv.SelectedValue
                    savedSearch.Name = newName
                    savedSearch.ProfileId = Me.MasterProfileId
                    savedSearch.SearchText = sql

                    cmdb.EUS_SavedSearches.InsertOnSubmit(savedSearch)
                    cmdb.SubmitChanges()

                    cbSavedSrch.DataBind()
                    ClsCombos.SelectComboItem(cbSavedSrch, savedSearch.SavedSearchId)
                    hdfSelectedName.Value = savedSearch.SavedSearchId
                End Using
            End If


        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub

    'Protected Sub lvSavedSearches_ItemCommand(sender As Object, e As System.Web.UI.WebControls.ListViewCommandEventArgs) Handles lvSavedSearches.ItemCommand
    '    Try

    '        If (e.CommandName = "DELETESEARCH") Then
    '            Dim savedSearch = (From itm In Me.CMSDBDataContext.EUS_SavedSearches
    '                    Where itm.SavedSearchId = Integer.Parse(e.CommandArgument)
    '                    Select itm).SingleOrDefault()

    '            Me.CMSDBDataContext.EUS_SavedSearches.DeleteOnSubmit(savedSearch)
    '            Me.CMSDBDataContext.SubmitChanges()
    '            cbSavedSrch.DataBind()
    '        End If

    '        If (e.CommandName = "LOADSEARCH") Then
    '            Dim savedSearch = (From itm In Me.CMSDBDataContext.EUS_SavedSearches
    '                    Where itm.SavedSearchId = Integer.Parse(e.CommandArgument)
    '                    Select itm).SingleOrDefault()

    '            LoadSavedSearch(savedSearch.SearchText, savedSearch.Distance)
    '        End If

    '    Catch ex As Exception
    '        WebErrorMessageBox(Me, ex, "")
    '    End Try

    'End Sub

    Private Sub LoadView()

        divNoResultsChangeCriteria.Visible = False
        divNoresultsToManyParams.Visible = False

        hdfLocationStatus.Value = "zip"

        QSCreterias.VerifyControlLoaded()

        If (Request.QueryString("vw") IsNot Nothing) Then
            If (Request.QueryString("vw").ToUpper() = "NEAREST") Then
                SearchSorting = SearchSortEnum.NearestDistance
            End If
            If (Request.QueryString("vw").ToUpper() = "NEWEST") Then
                SearchSorting = SearchSortEnum.NewestMember
            End If
            If (Request.QueryString("vw").ToUpper() = "NEWESTINCOUNTRY") Then
                SearchSorting = SearchSortEnum.NewestNearMember
            End If
            If (Request.QueryString("vw").ToUpper() = "ONLINE") Then
                QSCreterias.chkOnlineChecked = True
                SearchSorting = SearchSortEnum.RecentlyLoggedIn
            End If
            If (Request.QueryString("vw").ToUpper() = "BIRTHDAYTODAY") Then
                SearchSorting = SearchSortEnum.Birthday
                QSCreterias.ShowBirthdayNote()
            End If
        End If

        If (Request.QueryString("result") IsNot Nothing) Then
            If (Request.QueryString("result").ToUpper() = "AUTONOTIF") Then

                Me.GetMembersThatShouldBeNotifiedForNewMember()

            End If
        End If

    End Sub


    Private Sub GetMembersThatShouldBeNotifiedForNewMember()
        Dim ans As EUS_AutoNotificationSetting = Nothing
        Using cmdb As CMSDBDataContext = Me.GetCMSDBDataContext
            ans = (From itm In cmdb.EUS_AutoNotificationSettings
                                                    Where itm.CustomerID = Me.MasterProfileId
                                                    Select itm).FirstOrDefault()
        End Using
        If (ans IsNot Nothing) Then
            Dim sbSql As New StringBuilder

            sbSql.AppendLine(" and  EUS_Profiles.GenderId=" & ans.crGenderId & " ")
            If (Not String.IsNullOrEmpty(ans.crCountry)) Then
                sbSql.AppendLine(" and  EUS_Profiles.Country='" & ans.crCountry & "' ")
            End If
            If (Not String.IsNullOrEmpty(ans.crRegion)) Then
                sbSql.AppendLine(" and  EUS_Profiles.Region='" & ans.crRegion & "' ")
            End If
            If (Not String.IsNullOrEmpty(ans.crCity)) Then
                sbSql.AppendLine(" and  EUS_Profiles.City='" & ans.crCity & "' ")
            End If
            If (Not String.IsNullOrEmpty(ans.crBodyTypeId)) Then
                sbSql.AppendLine(" and  EUS_Profiles.PersonalInfo_BodyTypeID in (" & ans.crBodyTypeId & ") ")
            End If
            If (Not String.IsNullOrEmpty(ans.crEyeColorId)) Then
                sbSql.AppendLine(" and  EUS_Profiles.PersonalInfo_EyeColorID in (" & ans.crEyeColorId & ") ")
            End If
            If (Not String.IsNullOrEmpty(ans.crHairColorId)) Then
                sbSql.AppendLine(" and  EUS_Profiles.PersonalInfo_HairColorID in (" & ans.crHairColorId & ") ")
            End If
            If (Not String.IsNullOrEmpty(ans.crChildrenId)) Then
                sbSql.AppendLine(" and  EUS_Profiles.PersonalInfo_ChildrenID in (" & ans.crChildrenId & ") ")
            End If
            If (Not String.IsNullOrEmpty(ans.crEthnicityId)) Then
                sbSql.AppendLine(" and  EUS_Profiles.PersonalInfo_EthnicityID in (" & ans.crEthnicityId & ") ")
            End If
            If (Not String.IsNullOrEmpty(ans.crReligionId)) Then
                sbSql.AppendLine(" and  EUS_Profiles.PersonalInfo_ReligionID in (" & ans.crReligionId & ") ")
            End If
            If (Not String.IsNullOrEmpty(ans.crSmokingId)) Then
                sbSql.AppendLine(" and  EUS_Profiles.PersonalInfo_SmokingHabitID in (" & ans.crSmokingId & ") ")
            End If
            If (Not String.IsNullOrEmpty(ans.crDrinkingId)) Then
                sbSql.AppendLine(" and  EUS_Profiles.PersonalInfo_DrinkingHabitID in (" & ans.crDrinkingId & ") ")
            End If
            If (Not String.IsNullOrEmpty(ans.crRelationshipStatusId)) Then
                sbSql.AppendLine(" and  EUS_Profiles.LookingFor_RelationshipStatusID in (" & ans.crRelationshipStatusId & ") ")
            End If
            If (Not String.IsNullOrEmpty(ans.crHeightId)) Then
                sbSql.AppendLine(" and  EUS_Profiles.PersonalInfo_HeightID in (" & ans.crHeightId & ") ")
            End If


            If (ans.crAgeMin > 0 AndAlso ans.crAgeMax > 0) Then

                Dim fromDate = DateAdd(DateInterval.Year, ans.crAgeMax.Value * -1, Date.Now).Date
                fromDate = DateAdd(DateInterval.Year, -1, fromDate).Date
                Dim toDate = DateAdd(DateInterval.Year, ans.crAgeMin.Value * -1, Date.Now).Date

                sbSql.AppendLine(" and  EUS_Profiles.Birthday between '" & fromDate.ToString("yyyy-MM-dd hh:mm:ss.mmm") & "' and '" & toDate.ToString("yyyy-MM-dd hh:mm:ss.mmm") & "'  ")

            ElseIf (ans.crAgeMin > 0) Then

                Dim toDate = DateAdd(DateInterval.Year, ans.crAgeMin.Value * -1, Date.Now).Date
                sbSql.AppendLine(" and EUS_Profiles.Birthday < '" & toDate.ToString("yyyy-MM-dd hh:mm:ss.mmm") & "' ")

            ElseIf (ans.crAgeMax > 0) Then

                Dim fromDate = DateAdd(DateInterval.Year, ans.crAgeMax.Value * -1, Date.Now).Date
                fromDate = DateAdd(DateInterval.Year, -1, fromDate).Date
                sbSql.AppendLine(" and EUS_Profiles.Birthday between '" & fromDate.ToString("yyyy-MM-dd hh:mm:ss.mmm") & "' and getdate() ")

            End If


            If (sbSql.Length > 1) Then
                Me.LastAdvancedSearchquery = sbSql.ToString()
            End If
        End If

    End Sub


    Private Sub cbCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbCountry.SelectedIndexChanged
        If (cbCountry.SelectedItem IsNot Nothing AndAlso cbCountry.SelectedItem.Value <> "-1") Then
            cbRegion.DataSourceID = ""
            cbRegion.DataSource = clsGeoHelper.GetCountryRegions(cbCountry.SelectedItem.Value, Session("LAGID"))
            cbRegion.TextField = "region1"
            cbRegion.DataBind()

        End If
    End Sub

    Private Sub cbRegion_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbRegion.SelectedIndexChanged
        If (cbRegion.SelectedItem IsNot Nothing AndAlso cbRegion.SelectedItem.Value <> "-1") Then
            cbCity.DataSourceID = ""
            cbCity.DataSource = clsGeoHelper.GetCountryRegionCities(cbCountry.SelectedItem.Value, cbRegion.SelectedItem.Value, Session("LAGID"))
            cbCity.TextField = "city"
            cbCity.DataBind()
        End If
    End Sub



    Protected Sub cbRegion_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cbRegion.Callback
        FillRegionCombo(e.Parameter)
        cbCountry_SelectedIndexChanged(cbCountry, Nothing)

    End Sub

    Protected Sub cbCity_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cbCity.Callback
        FillCityCombo(e.Parameter)
        ClsCombos.SelectComboItem(cbRegion, e.Parameter)
        cbRegion_SelectedIndexChanged(cbRegion, Nothing)

    End Sub

    Protected Sub cbpnlZip_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cbpnlZip.Callback
        If (e.Parameter.StartsWith("country_")) Then
            FillRegionCombo(e.Parameter.Replace("country_", ""))
            'cbCountry_SelectedIndexChanged(cbCountry, Nothing)

        ElseIf (e.Parameter.StartsWith("region_")) Then
            FillCityCombo(e.Parameter.Replace("region_", ""))
            'ClsCombos.SelectComboItem(cbRegion, e.Parameter.Replace("region_", ""))
            'cbRegion_SelectedIndexChanged(cbRegion, Nothing)

            'ElseIf (e.Parameter.StartsWith("city_")) Then
            '    Dim city As String = e.Parameter.Replace("city_", "")
            '    city = city.Remove(city.IndexOf("_region_"))

            '    Dim region As String = e.Parameter.Substring(e.Parameter.IndexOf("_region_") + Len("_region_"))

            '    FillZipTextBox(city, region)

        End If

    End Sub

    Protected Sub FillRegionCombo(ByVal country As String)
        If String.IsNullOrEmpty(country) Then
            Return
        End If

        Try
            'ShowLocationControls()
            'cbRegion.DataBind()

            If (cbRegion.Items.Count = 0) Then
                cbRegion.Text = CurrentPageData.GetCustomString("msg_NotFound")
                cbCity.Text = CurrentPageData.GetCustomString("msg_NotFound")
            Else
                cbRegion.Text = CurrentPageData.GetCustomString(msg_SelectRegionText.ID)
                cbCity.Text = CurrentPageData.GetCustomString("msg_NotFound")
            End If


        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        Finally

        End Try
    End Sub


    Protected Sub FillCityCombo(ByVal region1 As String)
        If String.IsNullOrEmpty(region1) Then
            Return
        End If

        Try
            'ShowLocationControls()
            cbCity.Text = CurrentPageData.GetCustomString(msg_SelectCity.ID)


        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        Finally

        End Try
    End Sub


    Private Sub FillZipTextBox(city As String, Optional region As String = "")
        If String.IsNullOrEmpty(city) Then
            Return
        End If

        Try
            'ShowLocationControls()

            ' if there is a value in zip, skip
            'If (txtZip.Text.Trim() <> "") Then
            '    Return
            'End If


            Dim country As String = cbCountry.SelectedItem.Value

            If (String.IsNullOrEmpty(region)) Then
                region = cbRegion.SelectedItem.Value
            End If
            ' city = cbCity.SelectedItem.Value
            If (country = "GR") Then
                txtZip.Text = clsGeoHelper.GetCityCenterPostcode(country, region, city)
            End If
            'ShowRegion(True)
            'ShowCity(True)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        Finally

        End Try
    End Sub


    Protected Sub sdsRegion_Selecting(sender As Object, e As System.Web.UI.WebControls.SqlDataSourceSelectingEventArgs) Handles sdsRegion.Selecting
        For Each prm As SqlClient.SqlParameter In e.Command.Parameters

            If (prm.ParameterName = "@language") Then
                If (GetLag() = "GR") Then
                    prm.Value = "EL"
                Else
                    prm.Value = "EN"
                End If
            End If

        Next

    End Sub

    Protected Sub sdsCity_Selecting(sender As Object, e As System.Web.UI.WebControls.SqlDataSourceSelectingEventArgs) Handles sdsCity.Selecting
        For Each prm As SqlClient.SqlParameter In e.Command.Parameters

            If (prm.ParameterName = "@language") Then
                If (GetLag() = "GR") Then
                    prm.Value = "EL"
                Else
                    prm.Value = "EN"
                End If
            End If

        Next

    End Sub

    Public Shared Function GetListSubquery(sql As String, cb As CheckBoxList, dbField As String) As String

        Dim cbItemsAll As Boolean = True
        Dim cbItems As String = ""
        For Each li As ListItem In cb.Items
            If (li.Selected) Then
                cbItems = cbItems & li.Value & ","
            End If
            If (Not li.Selected) Then cbItemsAll = False
        Next
        If (cbItemsAll) Then
            If (cbItems <> "") Then
                cbItems = cbItems.TrimEnd(",")
                sql = sql & vbCrLf & " and  (" & dbField & " in  (" & cbItems & ") or " & dbField & "=-1  or " & dbField & " is null)"
            End If
        Else
            If (cbItems <> "") Then
                cbItems = cbItems.TrimEnd(",")
                sql = sql & vbCrLf & " and  " & dbField & " in  (" & cbItems & ")"
            End If
        End If

        Return sql
    End Function

    Public Shared Function GetListSubquery(Of T)(cb As List(Of T)) As String
        Dim sql As String = ""
        For cnt = 0 To cb.Count - 1
            If (cnt = cb.Count - 1) Then
                sql = sql & cb(cnt).ToString()
            Else
                sql = sql & cb(cnt).ToString() & ","
            End If
        Next
        Return sql
    End Function



    Private Function GetCityName() As String
        Dim city As String = ""
        If (cbCity.SelectedItem IsNot Nothing) Then
            city = cbCity.SelectedItem.Value
        Else
            city = cbCity.Text.Trim()
        End If

        If (city = CurrentPageData.GetCustomString(msg_SelectCity.ID)) Then
            city = ""
        End If

        If (city = CurrentPageData.GetCustomString("msg_NotFound")) Then
            city = ""
        End If

        Return city
    End Function


    Private Function GetRegionName() As String
        Dim region As String = ""
        If (cbRegion.SelectedItem IsNot Nothing) Then
            region = cbRegion.SelectedItem.Value
        Else
            region = cbRegion.Text.Trim()
        End If

        If (region = CurrentPageData.GetCustomString(msg_SelectRegionText.ID)) Then
            region = ""
        End If

        If (region = CurrentPageData.GetCustomString("msg_NotFound")) Then
            region = ""
        End If

        Return region
    End Function


    Private Sub ibtSavedSearchLoad_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles ibtSavedSearchLoad.Click
        Try
            If (cbSavedSrch.SelectedItem IsNot Nothing) Then
                Dim id As Integer = 0
                Integer.TryParse(cbSavedSrch.SelectedItem.Value, id)
                Using cmdb As CMSDBDataContext = Me.GetCMSDBDataContext


                    Dim savedSearch = (From itm In cmdb.EUS_SavedSearches
                                        Where itm.SavedSearchId = id
                                        Select itm).SingleOrDefault()

                    LoadSavedSearch(savedSearch.SearchText, savedSearch.Distance)
                End Using
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub

    Private Sub btnDeleteSS_Click(sender As Object, e As System.EventArgs) Handles btnDeleteSS.Click
        Try
            If (cbSavedSrch.SelectedItem IsNot Nothing) Then
                Dim id As Integer = 0
                Integer.TryParse(cbSavedSrch.SelectedItem.Value, id)
                Using cmdb As CMSDBDataContext = Me.GetCMSDBDataContext


                    Dim savedSearch = (From itm In cmdb.EUS_SavedSearches
                            Where itm.SavedSearchId = id
                            Select itm).SingleOrDefault()

                    cmdb.EUS_SavedSearches.DeleteOnSubmit(savedSearch)
                    cmdb.SubmitChanges()
                End Using
                cbSavedSrch.Items.Clear()
                cbSavedSrch.SelectedItem = Nothing
                cbSavedSrch.Value = Nothing
                cbSavedSrch.DataBind()
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub



End Class



﻿Imports Dating.Server.Core.DLL
Imports Dating.Server.Datasets.DLL.DSMembers
Imports Dating.Server.Datasets.DLL

Public Class AfterRegister1
    Inherits BasePage

    Protected Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            ' If (_pageData Is Nothing) Then _pageData = New clsPageData("~/Members/control.ProfileEdit", Context)
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData("AfterRegister.aspx", Context, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property


    Protected Overrides Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        SetMasterPage(CurrentPageData)

        Try
            If clsCurrentContext.VerifyLogin() = True Then
                'Me.MasterPageFile = "~/Members/Members2.Master"
            Else
                clsCurrentContext.ClearSession(Session)
                Response.Redirect(ResolveUrl("~/Login.aspx"), False)
                HttpContext.Current.ApplicationInstance.CompleteRequest()
            End If
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


    Private Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        '''''''''''''''''''''''''''''
        ' !!!   visibility is set by profile.aspx, if set to false do not load control
        '''''''''''''''''''''''''''''
        If (Not Me.Visible) Then Return


        Try


            'Try
            '    Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            '    AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
            'Catch ex As Exception
            '    WebErrorMessageBox(Me, ex, "Page_Load")
            'End Try

            If (Not Me.IsPostBack) Then
                LoadLAG()
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub




    Protected Sub LoadLAG()
        Try
            '    Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic

            SetControlsValue(Me, CurrentPageData)
            btnContinue.Text = AppUtils.StripHTML(CurrentPageData.GetCustomString("btnContinue"))

            Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_Height, Session("LagID"), "HeightId", cbHeight, True, "US")
            cbHeight.Items(0).Selected = True

            Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_BodyType, Session("LagID"), "BodyTypeId", cbBodyType, True, "US")
            cbBodyType.Items(0).Selected = True

            Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_EyeColor, Session("LagID"), "EyeColorId", cbEyeColor, True, "US")
            cbEyeColor.Items(0).Selected = True

            Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_HairColor, Session("LagID"), "HairColorId", cbHairClr, True, "US")
            cbHairClr.Items(0).Selected = True

            Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_RelationshipStatus, Session("LagID"), "RelationshipStatusId", cbRelationshipStatus, True, "US")
            cbRelationshipStatus.Items.RemoveAt(0)
            cbRelationshipStatus.Items(0).Selected = True

            Dim couple As String = "//cdn.goomena.com/Images2/pub2/couple.png?v=1"
            Dim single1 As String = ""
            If (Me.IsFemale) Then
                single1 = "//cdn.goomena.com/Images2/pub2/singe_woman.png"
            Else
                single1 = "//cdn.goomena.com/Images2/pub2/single-men.png"
            End If

            For Each itm As DevExpress.Web.ASPxEditors.ListEditItem In cbRelationshipStatus.Items
                Select Case itm.Value
                    Case "1"
                        itm.ImageUrl = single1
                    Case "2" '{Μόνος/η}
                        itm.ImageUrl = single1
                    Case "3" '{Διαζευγμένος/η}
                        itm.ImageUrl = single1
                    Case "4" '{Χήρος/α}
                        itm.ImageUrl = single1
                    Case "5" '{Χωρισμένος/η}
                        itm.ImageUrl = single1
                    Case "6" '{Παντρεμένος/η αλλά Ψάχνω}
                        itm.ImageUrl = couple
                    Case "7"
                        itm.ImageUrl = single1
                    Case "8" '{Δεν απαντώ}
                        itm.ImageUrl = single1
                    Case "9" '{Σε σχέση}
                        itm.ImageUrl = couple

                End Select
            Next

            cbHeight.ValidationSettings.ErrorText = CurrentPageData.GetCustomString("Height.Validation.ErrorText")
            cbHeight.ValidationSettings.RequiredField.ErrorText = CurrentPageData.GetCustomString("Height.Validation.ErrorText")

            cbBodyType.ValidationSettings.ErrorText = CurrentPageData.GetCustomString("BodyType.Validation.ErrorText")
            cbBodyType.ValidationSettings.RequiredField.ErrorText = CurrentPageData.GetCustomString("BodyType.Validation.ErrorText")

            cbEyeColor.ValidationSettings.ErrorText = CurrentPageData.GetCustomString("EyeColor.Validation.ErrorText")
            cbEyeColor.ValidationSettings.RequiredField.ErrorText = CurrentPageData.GetCustomString("EyeColor.Validation.ErrorText")

            cbHairClr.ValidationSettings.ErrorText = CurrentPageData.GetCustomString("HairClr.Validation.ErrorText")
            cbHairClr.ValidationSettings.RequiredField.ErrorText = CurrentPageData.GetCustomString("HairClr.Validation.ErrorText")

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub





    Private Sub btnContinue_Click(sender As Object, e As EventArgs) Handles btnContinue.Click
        Try
            If (ValidateInput()) Then

                SaveProfile()
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "closeAdditionalInfo", "closeAdditionalInfo();", True)

                Session("SET_ADDITIONAL_INFO") = False
                clsProfilesPrivacySettings.Update_RegisterAdditionalInfo(Me.MasterProfileId, False)

            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Saving after register")
        End Try
    End Sub


    Private Function ValidateInput() As Boolean

        Dim isValid As Boolean = True

        ' perform checks, in case user has bypass validation on client
        Try
            serverValidationList.Items.Clear()

            ' personal information
            If ((cbHeight.SelectedItem Is Nothing OrElse cbHeight.SelectedIndex = -1 OrElse cbHeight.SelectedIndex = 0)) Then
                cbHeight.Focus()
                Dim txt As String = CurrentPageData.GetCustomString("Height.Validation.ErrorText")
                txt = StripTags(txt)
                Dim itm As ListItem = New ListItem(txt, "javascript:" & cbHeight.ClientInstanceName & ".SetFocus();")
                itm.Attributes.Add("class", "BulletedListItem")
                serverValidationList.Items.Add(itm)
                isValid = False
            End If

            If ((cbBodyType.SelectedItem Is Nothing OrElse cbBodyType.SelectedIndex = -1 OrElse cbBodyType.SelectedIndex = 0)) Then
                cbBodyType.Focus()
                Dim txt As String = CurrentPageData.GetCustomString("BodyType.Validation.ErrorText")
                txt = StripTags(txt)
                Dim itm As ListItem = New ListItem(txt, "javascript:" & cbBodyType.ClientInstanceName & ".SetFocus();")
                itm.Attributes.Add("class", "BulletedListItem")
                serverValidationList.Items.Add(itm)
                isValid = False
            End If

            If ((cbEyeColor.SelectedItem Is Nothing OrElse cbEyeColor.SelectedIndex = -1 OrElse cbEyeColor.SelectedIndex = 0)) Then
                cbEyeColor.Focus()
                Dim txt As String = CurrentPageData.GetCustomString("EyeColor.Validation.ErrorText")
                txt = StripTags(txt)
                Dim itm As ListItem = New ListItem(txt, "javascript:" & cbEyeColor.ClientInstanceName & ".SetFocus();")
                itm.Attributes.Add("class", "BulletedListItem")
                serverValidationList.Items.Add(itm)
                isValid = False
            End If


            If ((cbHairClr.SelectedItem Is Nothing OrElse cbHairClr.SelectedIndex = -1 OrElse cbHairClr.SelectedIndex = 0)) Then
                cbHairClr.Focus()
                Dim txt As String = CurrentPageData.GetCustomString("HairClr.Validation.ErrorText")
                txt = StripTags(txt)
                Dim itm As ListItem = New ListItem(txt, "javascript:" & cbHairClr.ClientInstanceName & ".SetFocus();")
                itm.Attributes.Add("class", "BulletedListItem")
                serverValidationList.Items.Add(itm)
                isValid = False
            End If





        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
        Return isValid
    End Function


    Private Sub SaveProfile()
        Try
            Dim _profilerows As New clsProfileRows(Me.MasterProfileId)
            Dim mirrorRow As EUS_ProfilesRow = _profilerows.GetMirrorRow() 'ds.EUS_Profiles(_profilerows.MirrorRowIndex)
            Dim ds As DSMembers = _profilerows.DSMembers
            Dim dsOld As DSMembers = ds.Copy()
            Dim mirrorRowOld As EUS_ProfilesRow = Nothing
            Dim masterRowOld As EUS_ProfilesRow = Nothing

            Dim _drs As EUS_ProfilesRow() = dsOld.EUS_Profiles.Select("IsMaster=0")
            If (_drs.Length > 0) Then
                mirrorRowOld = _drs(0)
            End If
            _drs = dsOld.EUS_Profiles.Select("IsMaster=1")
            If (_drs.Length > 0) Then
                masterRowOld = _drs(0)
            End If


            If (cbHeight.Items(0).Selected) Then
                mirrorRow.PersonalInfo_HeightID = -1
            Else
                mirrorRow.PersonalInfo_HeightID = cbHeight.SelectedItem.Value
            End If

            If (cbBodyType.Items(0).Selected) Then
                mirrorRow.PersonalInfo_BodyTypeID = -1
            Else
                mirrorRow.PersonalInfo_BodyTypeID = cbBodyType.SelectedItem.Value
            End If

            If (cbEyeColor.Items(0).Selected) Then
                mirrorRow.PersonalInfo_EyeColorID = -1
            Else
                mirrorRow.PersonalInfo_EyeColorID = cbEyeColor.SelectedItem.Value
            End If

            If (cbHairClr.Items(0).Selected) Then
                mirrorRow.PersonalInfo_HairColorID = -1
            Else
                mirrorRow.PersonalInfo_HairColorID = cbHairClr.SelectedItem.Value
            End If

            If (cbRelationshipStatus.Items(0).Selected) Then
                mirrorRow.LookingFor_RelationshipStatusID = -1
            Else
                mirrorRow.LookingFor_RelationshipStatusID = cbRelationshipStatus.SelectedItem.Value
            End If

            mirrorRow.LastUpdateProfileDateTime = DateTime.UtcNow
            mirrorRow.LastUpdateProfileIP = Request.Params("REMOTE_ADDR")
            mirrorRow.LastUpdateProfileGEOInfo = Session("GEO_COUNTRY_CODE")

            Dim masterRow As EUS_ProfilesRow = _profilerows.GetMasterRow()
            If (masterRow IsNot Nothing AndAlso mirrorRow.ProfileID <> masterRow.ProfileID) Then
                masterRow.PersonalInfo_HeightID = mirrorRow.PersonalInfo_HeightID
                masterRow.PersonalInfo_BodyTypeID = mirrorRow.PersonalInfo_BodyTypeID
                masterRow.PersonalInfo_EyeColorID = mirrorRow.PersonalInfo_EyeColorID
                masterRow.PersonalInfo_HairColorID = mirrorRow.PersonalInfo_HairColorID
                masterRow.LookingFor_RelationshipStatusID = mirrorRow.LookingFor_RelationshipStatusID
                masterRow.LastUpdateProfileDateTime = mirrorRow.LastUpdateProfileDateTime
                masterRow.LastUpdateProfileIP = mirrorRow.LastUpdateProfileIP
                masterRow.LastUpdateProfileGEOInfo = mirrorRow.LastUpdateProfileGEOInfo

                Me.SessionVariables.MemberData.Fill(masterRow)
                DataHelpers.UpdateEUS_Profiles_ModifiedFields(masterRow, masterRowOld)
            Else
                Me.SessionVariables.MemberData.Fill(mirrorRow)
            End If

            DataHelpers.UpdateEUS_Profiles_ModifiedFields(mirrorRow, mirrorRowOld)

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Saving after register")
        End Try
    End Sub
End Class
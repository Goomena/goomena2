﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Members/Members2Inner.master" CodeBehind="pwSetPayment.aspx.vb" Inherits="Dating.Server.Site.Web.pwSetPayment" %>
<%@ Register src="~/UserControls/MemberLeftPanel.ascx" tagname="MemberLeftPanel" tagprefix="uc2" %>
<%@ Register src="~/UserControls/LiveZillaPublic.ascx" tagname="LiveZillaPublic" tagprefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

<style type="text/css">
    .iframePayment { height: 1010px; width: 990px; border: 0; /*border-top:solid 2px #f0f0f0;*/ }
    .iframePayment_PW { height: 820px; width: 640px; border: 0; overflow: hidden; margin-left: auto; margin-right: auto; }
    .iframePayment_BitPay { height:548px; width: 768px; border: 0; display:block ;overflow: hidden; margin-left: auto; margin-right: auto;
background:url("//cdn.goomena.com/Images2/mbr2/bitcoinPayment.png?v2") no-repeat scroll 50% 50%;
    }
      .iframePayment_BitPayWR {   display: block;
  margin-left: auto;
  margin-right: auto;
  top: 81px;
  position: relative;
  width: 518px;
  left: -12px;
  height: 305px;
  margin-top: 76px;
    }
    .iframePayment_BitPay #container{margin-top:-9px;}
    .right-side {width: 910px;padding-left: 0;margin-left: -12px;}
    .left-side-wrap { display:none;}
    #column-shadow { display:none;}
    .right-side {float:none;/*margin-left:auto;margin-right:auto;*/ }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <script type="text/javascript">
        /*
        // <![CDATA[
        (function () {
            //$('.left-side-wrap').css("display", "none");
            //$('#column-shadow').css("display", "none");
            //$('.right-side').removeClass("lfloat");
            //$('.right-side').css("margin-left", "auto");
            //$('.right-side').css("margin-right", "auto");
        })();
        // ]]>
        */
    </script>
    <div style="padding-top:100px;">
        <div style="padding:20px;display:none;">
            <div style="padding:10px;" class="blueBox"><dx:ASPxLabel ID="lblPaymentDescription" runat="server" Text="lblPaymentDescription" EncodeHtml="False"/></div>
            <div style="padding:10px;"><dx:ASPxLabel ID="lbPaymentExtraInfo" runat="server" Text="lbPaymentExtraInfo" EncodeHtml="False"/></div>
        </div>
       
        <div runat="server" id="iframePayment_Wrap">
          
            <iframe src="" runat="server" id="iframePayment" frameborder="0" class="iframePayment"></iframe>
        </div>
    </div>


<%--    <div class="clear"></div>
     
    <div class="container_12" id="tabs-outter">
        <div class="grid_12 top_tabs">
            <div>
            </div>
        </div></div><div class="clear"></div>
	<div class="container_12" id="content_top"></div>
	<div class="container_12" id="content">
            <div class="grid_3" id="sidebar-outter">
               
            </div>
            <div class="grid_9 body">
                <div class="middle" id="content-outter">

            <div id="filter" class="m_filter t_filter msg_filter">
	            <div class="lfloat">
		            <h2><asp:Label ID="lblPaymentMethodTitle" runat="server" Text="Report"/></h2>
	            </div>
	            <div class="rfloat">
                    <dx:ASPxHyperLink ID="lnkBack" runat="server" Text="Back" CssClass="btn">
                    </dx:ASPxHyperLink>
	            </div>

	            <div class="clear"></div>
            </div>

            <div class="t_wrap">
	            <div style="background-color:#fff">
        <uc1:UCSelectPayment ID="UCSelectPayment1" runat="server" />
                </div>
            </div>
			    </div>
				<div class="bottom"></div>
            </div>
            <div class="clear">
            </div>
        </div>


    <div class="container_12" id="content_bottom">
    </div>
--%>
      
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cntBodyBottom" runat="server">
    <uc3:LiveZillaPublic ID="ctlLiveZillaPublic" runat="server" />
</asp:Content>

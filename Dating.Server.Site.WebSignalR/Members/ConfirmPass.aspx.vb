﻿Public Class ConfirmPass
    Inherits BasePage



#Region "Props"


    'Dim _pageData As clsPageData
    'Protected ReadOnly Property CurrentPageData As clsPageData
    '    Get
    '        If (_pageData Is Nothing) Then
    '            _pageData = New clsPageData(Context)
    '            AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
    '        End If
    '        Return _pageData
    '    End Get
    'End Property

#End Region


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If (Not Me.IsPostBack) Then
                LoadLAG()
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try
    End Sub


    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender

        Try
            Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub



    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
    End Sub


    Protected Sub LoadLAG()
        Try

            '     Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = Me.CurrentPageData.cPageBasic
            btnCheckPassword.Text = Me.CurrentPageData.GetCustomString("btnCheckPassword")
            lblYourPassword.Text = Me.CurrentPageData.GetCustomString("lblYourPassword")
            lblTitle.Text = Me.CurrentPageData.GetCustomString("lblTitle")

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Protected Sub btnCheckPassword_Click(sender As Object, e As EventArgs) Handles btnCheckPassword.Click
        Try
            Dim b As Boolean = Dating.Server.Core.DLL.DataHelpers.EUS_Profiles_CheckPassword(Me.MasterProfileId, txtPassword.Text)
            If (b) Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "notifyParentSuccess", "notifyParentSuccess()", True)
            Else
                pnlPasswordVerifyFailed.Visible = True
                lblPasswordVerifyFailed.Text = Me.CurrentPageData.GetCustomString("lblPasswordVerifyFailed")
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub
End Class
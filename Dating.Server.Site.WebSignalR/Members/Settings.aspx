﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Members/Members2Inner.Master" 
    CodeBehind="Settings.aspx.vb" Inherits="Dating.Server.Site.Web.Settings" %>
<%@ Register src="../UserControls/MemberLeftPanel.ascx" tagname="MemberLeftPanel" tagprefix="uc3" %>
<%@ Register assembly="DevExpress.Web.v13.1, Version=13.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register src="../UserControls/WhatIsIt.ascx" tagname="WhatIsIt" tagprefix="uc2" %>
<%@ Register src="../UserControls/ProfileCreditsHistory.ascx" tagname="ProfileCreditsHistory" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        table.TextBoxItem { margin-top: 10px; }
        .TextBoxItem td { padding-bottom: 3px; }
        .BulletedList { margin-left: 0px; padding-left: 25px; }
        li.BulletedListItem { }
        .BulletedListItem a { color: Red; text-decoration: none; border-bottom-color: #f70; border-bottom-width: 1px; border-bottom-style: dashed; }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
<script type="text/javascript">
    function confirmDelete(bypassPassw) {
        var warning = '<%= CurrentPageData.GetCustomString("WarningUserDeletingAccount").Replace("'", "\'") %>';
        if (!bypassPassw) {
            if (confirm(warning)) {
                ShowConfirmPass( '<%= Dating.Server.Site.Web.AppUtils.JS_PrepareAlertString(CurrentPageData.GetCustomString("DeleteConfirmPopupTitle").Replace("'", "\'"))%>')
            }
        }
        if (bypassPassw) {
            if (confirm(warning)) {
                try { ShowLoading(); } catch (e) { };
                return true;
            }
            else {
                if ($('#<%= lnkDeleteAccount.ClientID %>').length > 0) {
                    $('#<%= lnkDeleteAccount.ClientID %>').attr("onclick", "return confirmDelete(false);")
                }
                return false;
            }
        }
        return false;
    }



        var isOkPasswordConfirm = false;

        function ShowConfirmPass(title) {
            isOkPasswordConfirm = false;
            ShowDevExPopUpByURL('popupConfirmPass', '<%= ResolveUrl("~/loading.htm") %>', null, title);
            ShowDevExPopUpByURL('popupConfirmPass', 'ConfirmPass.aspx?popup=popupConfirmPass', null, title);
            window["popupConfirmPass"].Closing.AddHandler(__Closing_Remove);
            //window["popupConfirmPass"].CloseUp.AddHandler(__CloseUp_Remove);

            function __Closing_Remove(s, e) {
                try {
                    s.SetContentUrl('about:blank');
                    s.Closing.RemoveHandler(__Closing_Remove);
                    if (isOkPasswordConfirm == true)
                        if ($('#<%= lnkDeleteAccount.ClientID %>').length > 0) {
                            $('#<%= lnkDeleteAccount.ClientID %>').attr("onclick", "return confirmDelete(true);")
                            $('#<%= lnkDeleteAccount.ClientID %>')[0].click();
                        }
                }
                catch (e) { }
            }
            function __CloseUp_Remove(s, e) {
                try {
                    s.CloseUp.RemoveHandler(__CloseUp_Remove);
                    if (isOkPasswordConfirm == true)
                        if ($('#<%= lnkDeleteAccount.ClientID %>').length > 0) {
                            $('#<%= lnkDeleteAccount.ClientID %>').attr("onclick", "return confirmDelete(true);")
                            $('#<%= lnkDeleteAccount.ClientID %>')[0].click();
                        }
                }
                catch (e) { }
            }

        }
    </script>

<div id="settings-content">

<div id="settings-list-top">

    <div id="top-links-wrap">
    <table class="center-table" cellpadding="0" cellspacing="0">
        <tr>
            <td>
<ul id="top-links">
    <li runat="server" id="liAccSett"><asp:HyperLink ID="lnkAccSett" runat="server" 
                                        NavigateUrl="~/Members/Settings.aspx?vw=account" onclick="ShowLoading();" 
                                        CssClass="btn list-btn lnk83"><span></span><asp:Literal ID="msg_TabTitleAccountSettings" runat="server" /></asp:HyperLink></li>
    <li runat="server" id="liPrivacy"><asp:HyperLink ID="lnkPrivacy" runat="server" 
                                        NavigateUrl="~/Members/Settings.aspx?vw=privacy" onclick="ShowLoading();" 
                                        CssClass="btn list-btn lnk112"><span></span><asp:Literal ID="msg_TabTitlePrivacy" runat="server" /></asp:HyperLink></li>
    <li runat="server" id="liNotifications"><asp:HyperLink ID="lnkNotifications" runat="server" 
                                        NavigateUrl="~/Members/Settings.aspx?vw=notifications" onclick="ShowLoading();" 
                                        CssClass="btn list-btn lnk112"><span></span><asp:Literal ID="msg_TabTitleNotifications" runat="server" /></asp:HyperLink></li>
    <li runat="server" id="liCredits"><asp:HyperLink ID="lnkCredits" runat="server" 
                                        NavigateUrl="~/Members/Settings.aspx?vw=credits" onclick="ShowLoading();" 
                                        CssClass="btn list-btn lnk112"><span></span><asp:Literal ID="msg_TabTitleCredits" runat="server" /></asp:HyperLink></li>
</ul>
<div class="list-arrow"></div>
<div class="clear"></div>
            </td>
        </tr>
    </table>
    </div>

    <div class="list-title">
        <asp:Literal ID="lblItemsName" runat="server" />
        <asp:LinkButton ID="lnkViewDescription" runat="server" CssClass="more_info2" OnClientClick="OnMoreInfoClick('[contentUrl]', this, '[headerText]');return false;" />
    </div>


    <div class="clear"></div>
</div>

<script type="text/javascript">
(function () {
    var itms = $("a", "#top-links").css("width", "auto");
    for (var c = 0; c < itms.length; c++) {
        var w = $(itms[c]).width();
        $(itms[c]).css("width", "").removeClass("lnk83").removeClass("lnk112").removeClass("lnk127").removeClass("lnk161").removeClass("lnk173").removeClass("lnk254")
        if (w < 73)
            $(itms[c]).addClass("lnk83");
        else if (w < 100)
            $(itms[c]).addClass("lnk112");
        else if (w < 115)
            $(itms[c]).addClass("lnk127");
        else if (w < 148)
            $(itms[c]).addClass("lnk161");
        else if (w < 160)
            $(itms[c]).addClass("lnk173");
        else
            $(itms[c]).addClass("lnk254");
    }

    try {
        var el = $("a", "#top-links .down");
        var arr = $(".list-arrow", "#top-links-wrap");
        var p1 = el.position().left;
        p1 = (p1 + (el.width() / 2)) - (arr.width() / 2);
        arr.css("left", p1 + "px");
    }
    catch (e) { }
})();
</script>


	
	
    <asp:MultiView ID="mvSettings" runat="server">


        <asp:View ID="vwAccountSettings" runat="server">
<div id="settings-account">
	<h3 class="top-title"><asp:Literal ID="msg_RightColumnSettingsTitle1" runat="server"/></h3>
		
    <div class="pe_wrapper">
        <div class="pe_box fb-settings">
            <h3 class="sub-title"><asp:Literal ID="msg_FacebookConnectTitle" runat="server" /></h3>
            <div class="pe_form pe_login">
                <div class="lfloat fb_current_status"><asp:Label ID="msg_FBCurrentState" runat="server"  /></div>
                <div class="lfloat state_indicator off" ID="divFBState" runat="server"><asp:Label ID="lblFacebookConnected" runat="server" /></div>
                <div class="lfloat">&nbsp;<asp:Literal ID="lblSeparatorFBstatus" runat="server"> - </asp:Literal>&nbsp;</div>
                <div class="rfloat cmd">
                    <asp:HyperLink ID="lbLoginFB" runat="server" CssClass="js facebook-login btn lnk130-fff"
                            Text="Connect with Facebook" 
                            NavigateUrl="~/Register.aspx?conn=fb" />
                    <asp:HyperLink ID="lbLoginFBDisconn" runat="server" CssClass="js facebook-login btn lnk130-fff"
                            Text="Connect with Facebook" 
                            NavigateUrl="~/Members/Settings.aspx?conn=fb&remove=1"
                            Visible="False" />
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>

<!--		
	<div class="as_holder">
		<div class="as_body ab_block ab_settings as_small">
			<div class="pe_wrapper">
			<div class="pe_box">
			<h3 style="text-shadow:none; border:none; font-family:Lucida Grande,Lucida Sans Unicode,Verdana,Arial,sans-serif; "><asp:Literal ID="msg_TwitterConnectTitle" runat="server"/></h3>

			<div class="pe_form pe_login">
				<ul>
					<! -- New Twitter Connect  -- >
					<li><label><asp:Literal ID="msg_TwitterCurrentStatus" runat="server"/></label>
						<div class="form_right form_clust">
							<p><span class="not"><asp:Literal ID="msg_TwitterNotConnected" runat="server"/></span> - <a href="http://www.twitter.com" class="js"><asp:Literal ID="msg_TwitterConnectNow" runat="server"/></a></p>
						</div>
					</li>
					<li class="clear"></li>
				</ul>
				<div class="clear"></div>
			</div>
			</div>
			</div>
		</div>
	</div>
-->

	<div class="pe_wrapper">
		<div class="pe_box">
		<h3 class="sub-title"><asp:Literal ID="msg_ChangeYourPasswordTitle" runat="server"/></h3>

            <asp:Panel runat="server" DefaultButton="btnChangePassword" class="" ID="pnlChangePassword">

            <asp:Panel ID="pnlPasswordChangedSuccessfully" runat="server" 
                CssClass="alert alert-success blue" Visible="False" ViewStateMode="Disabled"><asp:Label ID="lblPasswordChangedSuccessfully" runat="server" Text=""/></asp:Panel>
                        
                <div class="pe_form">
                    <div class="pe_list">
                        <asp:BulletedList ID="ChangePasswordServerValidationList" runat="server" Font-Size="14px" ForeColor="Red"
                            DisplayMode="HyperLink" BulletStyle="Numbered" CssClass="BulletedList">
                        </asp:BulletedList>
                        <dx:ASPxValidationSummary ID="ChangePasswordSummary" runat="server" 
                            ValidationGroup="ChangePasswordGroup" RenderMode="BulletedList" 
                            ShowErrorsInEditors="true">
                            <Paddings PaddingBottom="10px" />
                            <LinkStyle><Font Size="14px"></Font></LinkStyle></dx:ASPxValidationSummary>
                        <table class="TextBoxItem">
                            <tr>
                                <td class="lbl-right"><asp:Literal ID="msg_MyOldPassword" runat="server"/></td>
                                <td><dx:ASPxTextBox ID="txtOldPassword" runat="server" Password="True" Width="230px" Font-Size="14px" 
                                        Height="18px" ClientInstanceName="txtPasswrdOld">
                                    <validationsettings errordisplaymode="ImageWithTooltip" errortext="" errortextposition="Right" 
                                                                    validationgroup="ChangePasswordGroup"><requiredfield errortext="" isrequired="True" /></validationsettings></dx:ASPxTextBox></td></tr><tr>
                                <td class="lbl-right"><asp:Literal ID="msg_MyPassword" runat="server"/></td>
                                <td><dx:ASPxTextBox ID="txtPasswrd" runat="server" Password="True" Width="230px" Font-Size="14px" 
                                        Height="18px" ClientInstanceName="RegisterPassword">
                                    <validationsettings errordisplaymode="ImageWithTooltip" errortext="" errortextposition="Right" 
                                                                    validationgroup="ChangePasswordGroup"><requiredfield errortext="" isrequired="True" />
                                        <RegularExpression ValidationExpression="^(?=.*\d)(?=.*[a-zA-Z]).{3,20}$" ErrorText="" />
                                    </validationsettings></dx:ASPxTextBox></td></tr><tr>
                                <td class="lbl-right"><asp:Literal ID="msg_MyPasswordConfirm" runat="server"/></td>
                                <td><dx:ASPxTextBox ID="txtPasswrd1Conf" runat="server" Width="230px"
                                        Password="True" ClientInstanceName="RegisterPasswordConfirm" Font-Size="14px" Height="18px">
                                    <ClientSideEvents Validation="function(s, e) {e.isValid = (s.GetText() == RegisterPassword.GetText());}" />
                                    <ValidationSettings ValidationGroup="ChangePasswordGroup" ErrorDisplayMode="ImageWithTooltip" ErrorTextPosition="Right"
                                            ErrorText=""></ValidationSettings></dx:ASPxTextBox></td></tr></table><div class="clear"></div>
                    </div>
                    <div class="form-actions">
                        <dx:ASPxButton ID="btnChangePassword" runat="server" 
                            CausesValidation="True"
                            CssClass="btn lnk172-blue" Text="Change Password" 
                            ValidationGroup="ChangePasswordGroup" 
                            Native="True"  EncodeHtml="False" />
                    </div>
                </div>
			</asp:Panel>
		</div>
	</div>
		
	<div class="as_holder" runat="server" visible="false" id="divEmailChange" enableviewstate="false"> 
		<div class="as_body ab_block ab_settings">
			<div class="pe_wrapper">
			    <div class="pe_box">
			    <h3 class="sub-title"><asp:Literal ID="msg_ChangeEmailTitle" runat="server"/></h3>


                    <asp:Panel ID="pnlEmailChangedSuccessfully" runat="server" 
                        CssClass="alert alert-success blue" Visible="False" ViewStateMode="Disabled"><asp:Label ID="lblEmailChangedSuccessfully" runat="server" Text=""/></asp:Panel>
                        
			        <asp:Panel runat="server" DefaultButton="btnChangeEmail" class="pe_form pe_login pe_list" ID="pnlChangeEmail">
                        <asp:BulletedList ID="ChangeEmailServerValidationList" runat="server" 
                            Font-Size="14px" ForeColor="Red"
                            DisplayMode="HyperLink" BulletStyle="Numbered" 
                            CssClass="BulletedList" EnableViewState="False"></asp:BulletedList>
                        <dx:ASPxValidationSummary ID="ChangeEmailSummary" runat="server" 
                            ValidationGroup="ChangeEmailGroup" RenderMode="BulletedList" 
                            ShowErrorsInEditors="true">
                            <Paddings PaddingBottom="10px" />
                            <LinkStyle><Font Size="14px"></Font></LinkStyle></dx:ASPxValidationSummary><table class="TextBoxItem">
                            <tr>
                                <td><label class="lbl-right"><asp:Literal ID="msg_MyEmail" runat="server"/></label></td>
                                <td><dx:ASPxTextBox ID="txtEmail" runat="server" Width="230px" Font-Size="14px" Height="18px" ClientInstanceName="ChangeEmailTextBox">
                                    <validationsettings errordisplaymode="ImageWithTooltip" errortextposition="Left" 
                                                                    validationgroup="ChangeEmailGroup"><requiredfield errortext="" 
                                                                    isrequired="True" /><regularexpression errortext="" validationexpression="" /></validationsettings></dx:ASPxTextBox></td></tr><tr>
                                <td><label class="lbl-right"><asp:Literal ID="msg_MyEmailPassword" runat="server"/></label></td>
                                <td><dx:ASPxTextBox ID="txtEmailPassword" runat="server" Password="True" Width="230px" Font-Size="14px" 
                                        Height="18px" ClientInstanceName="ChangeEmailPasswordTextBox">
                                    <validationsettings errordisplaymode="ImageWithTooltip" errortextposition="Left" 
                                                                    validationgroup="ChangeEmailGroup"><requiredfield errortext="" isrequired="True" /></validationsettings></dx:ASPxTextBox></td></tr></table><div class="clear"></div>
				        <div class="form-actions">
                            <dx:ASPxButton ID="btnChangeEmail" runat="server" CausesValidation="True"
                                CssClass="btn btn-primary" Text="Change Email" 
                                ValidationGroup="ChangeEmailGroup" Native="True"  EncodeHtml="False" />
				        </div>
			        </asp:Panel>
			    </div>
			</div>
		</div>
	</div>

    <div class="pe_wrapper">
        <div class="pe_box">
            <h3 class="sub-title"><asp:Literal ID="msg_UploadVerDocsTitle" runat="server" /></h3>
            <div class="pe_form pe_login" style="padding-top:0px;">
                <div class="form-actions">
                    <asp:HyperLink ID="lnkVerificationDocs" runat="server" CssClass="btn lnk391-blue persist_css"
                        NavigateUrl="~/Members/VerificationDocs.aspx">Upload verification documents</asp:HyperLink>
                </div>
            </div>
        </div>
    </div>

    <div class="pe_wrapper">
        <div class="pe_box">
            <h3 class="sub-title"><asp:Literal ID="msg_ClearHistoryTitle" runat="server" /></h3>
            <div class="pe_form pe_login" style="padding-top:0px;">
                <div class="form-actions">
                    <asp:HyperLink ID="lnkClearHistory" runat="server" CssClass="btn lnk391-blue persist_css"
                        NavigateUrl="~/Members/SettingsHistory.aspx">Clear History</asp:HyperLink>
                </div>
            </div>
        </div>
    </div>

    <div class="pe_wrapper">
        <div class="pe_box">
            <h3 class="sub-title"><asp:Literal ID="msg_MyStatisticsTitle" runat="server" /></h3>
            <div class="pe_form pe_login" style="padding-top:0px;">
                <div class="form-actions">
                    <asp:HyperLink ID="lnkShowStatistics" runat="server" CssClass="btn lnk391-blue persist_css"
                        NavigateUrl="~/Members/Statistics.aspx">Show statistics</asp:HyperLink>
                </div>
            </div>
        </div>
    </div>

    <div class="pe_wrapper">
        <div class="pe_box acc-status">
            <h3 class="sub-title"><asp:Literal ID="msg_DeleteAccountTitle" runat="server" /></h3>
            <div class="pe_form pe_login">
                <div class="lfloat current_status"><asp:Label ID="msg_AccountCurrentStatus_ND" runat="server"  /></div>
                <div class="lfloat state_indicator" ID="divAccState" runat="server"><asp:Label ID="msg_AccountStatusActive" runat="server" /></div>
                <div class="lfloat">&nbsp;<asp:Literal ID="lblSeparatorAccStatus" runat="server"> - </asp:Literal>&nbsp;</div>
                <div class="rfloat cmd">
                    <asp:LinkButton ID="lnkDeleteAccount" runat="server" CssClass="btn lnk130-fff" OnClientClick="return confirmDelete(false);" />
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>

    <div class="clear"></div>
</div>
        </asp:View>


        <asp:View ID="vwPrivacy" runat="server">
<h3 class="top-title"><asp:Literal ID="msg_RightColumnSettingsTitle2" runat="server" /></h3>
<asp:Panel ID="pnlPrivacySettings" runat="server" DefaultButton="btnPrivacySettingsUpd">
    <div class="pe_wrapper pr-settings">
        <div class="pe_box">
            <h3 class="sub-title"><asp:Literal ID="msg_PSHideYourselfTitle" runat="server" /></h3>
            <div class="pe_form pe_login">
                <asp:Panel ID="pnlPSUserMessage" runat="server" CssClass="alert alert-success blue" Visible="False">
                    <asp:Label ID="lblPSUserMessage" runat="server" Text="" />
                </asp:Panel>
                <ul>
                    <li>
                        <label class="settingsLbl">
                            <asp:Literal ID="msg_PSSearchResults" runat="server" /></label>
                        <!-- Recent Matches  -->
                        <div class="settingsChk">
                            <dx:ASPxCheckBox ID="chkPS_HideMeFromSearchResults" runat="server" EncodeHtml="False"
                                Layout="Flow" />
                            <div class="clear">
                            </div>
                        </div>
                    </li>
                    <li id="liHideMeFromMembersIHaveBlocked" runat="server" visible="false">
                        <label class="settingsLbl">
                            <asp:Literal ID="msg_PSWhoIBlocked" runat="server" /></label>
                        <!-- Who I Blocked   -->
                        <div class="settingsChk">
                            <dx:ASPxCheckBox ID="chkPS_HideMeFromMembersIHaveBlocked" runat="server" EncodeHtml="False"
                                Layout="Flow" />
                            <div class="clear">
                            </div>
                        </div>
                    </li>
                    <li>
                        <label class="settingsLbl">
                            <asp:Literal ID="msg_PSWhoIFavorite" runat="server" /></label>
                        <!-- Who I Favorite   -->
                        <div class="settingsChk">
                            <dx:ASPxCheckBox ID="chkPS_NotShowInOtherUsersFavoritedList" runat="server" EncodeHtml="False"
                                Layout="Flow" />
                            <div class="clear">
                            </div>
                        </div>
                    </li>
                    <li>
                        <label class="settingsLbl">
                            <asp:Literal ID="msg_PSWhoIView" runat="server" /></label>
                        <!-- Who I View  -->
                        <div class="settingsChk">
                            <dx:ASPxCheckBox ID="chkPS_NotShowInOtherUsersViewedList" runat="server" EncodeHtml="False"
                                Layout="Flow" />
                            <div class="clear">
                            </div>
                        </div>
                    </li>
                    <li>
                        <label class="settingsLbl">
                            <asp:Literal ID="msg_PSLoginInformation" runat="server" /></label>
                        <!-- Login Information   -->
                        <div class="settingsChk">
                            <dx:ASPxCheckBox ID="chkPS_HideMyLastLoginDateTime" runat="server" EncodeHtml="False"
                                Layout="Flow" />
                            <div class="clear">
                            </div>
                        </div>
                        <div class="settingsChk">
                            <dx:ASPxCheckBox ID="chkPS_ShowMeOffline" runat="server" EncodeHtml="False"
                                Layout="Flow" />
                            <div class="clear">
                            </div>
                        </div>
                    </li>
                    <li>
                        <label class="settingsLbl">
                            <asp:Literal ID="msg_PSDontShowOnPhotosGrid" runat="server" /></label>
                        <!-- Login Information   -->
                        <div class="settingsChk">
                            <dx:ASPxCheckBox ID="chkPS_DontShowOnPhotosGrid" runat="server" EncodeHtml="False"
                                Layout="Flow" />
                            <div class="clear">
                            </div>
                        </div>
                        <div class="settingsChk" ID="liPSSuppressWarning_WhenReadingMessageFromDifferentCountry" runat="server" visible="false">
                            <dx:ASPxCheckBox ID="chkPSSuppressWarning_WhenReadingMessageFromDifferentCountry" runat="server" EncodeHtml="False"
                                Layout="Flow" /><br />
                            <div style="margin-left:50px">
                            <asp:Literal ID="msg_PSSuppressWarning_WhenReadingMessageFromDifferentCountryDateSet" runat="server" Visible="false" />
                            </div>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="settingsChk">
                            <dx:ASPxCheckBox ID="chkPSHideMeFromUsersInDifferentCountry" runat="server" EncodeHtml="False"
                                Layout="Flow" /><br />
                            <div style="margin-left:50px">
                            <asp:Literal ID="msg_PSHideMeFromUsersInDifferentCountryDateSet" runat="server" Visible="false" />
                            </div>
                                <div class="clear">
                            </div>
                        </div>
                        <div class="settingsChk">
                            <dx:ASPxCheckBox ID="chkPSDisableReceivingMessagesFromDifferentCountry" runat="server" EncodeHtml="False"
                                Layout="Flow" /><br />
                            <div style="margin-left:50px">
                            <asp:Literal ID="msg_PSDisableReceivingMessagesFromDifferentCountryDateSet" runat="server" Visible="false" />
                            </div>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="settingsChk">
                            <dx:ASPxCheckBox ID="chkPSDisableReceivingLikesFromDifferentCountry" runat="server" EncodeHtml="False"
                                Layout="Flow" /><br />
                            <div style="margin-left:50px">
                            <asp:Literal ID="msg_PSDisableReceivingLikesFromDifferentCountryDateSet" runat="server" Visible="false" />
                            </div>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="settingsChk">
                            <dx:ASPxCheckBox ID="chkPSDisableReceivingOffersFromDifferentCountry" runat="server" EncodeHtml="False"
                                Layout="Flow" />
                            <div style="margin-left:50px">
                            <asp:Literal ID="msg_PSDisableReceivingOffersFromDifferentCountryDateSet" runat="server" Visible="false" />
                            </div>
                            <div class="clear">
                            </div>
                        </div>
                    </li>
                    <%--<li>
                        <label class="settingsLbl">
                            <asp:Literal ID="msg_PSSuppressWarning_WhenReadingMessageFromDifferentCountry" runat="server" /></label>
                        <!-- Login Information   -->
                    </li>
                    <li>
                        <label class="settingsLbl">
                            <asp:Literal ID="msg_PSHideMeFromUsersInDifferentCountry" runat="server" /></label>
                        <!-- Login Information   -->
                    </li>
                    <li>
                        <label class="settingsLbl">
                            <asp:Literal ID="msg_PSDisableReceivingMessagesFromDifferentCountry" runat="server" /></label>
                        <!-- Login Information   -->
                    </li>
                    <li>
                        <label class="settingsLbl">
                            <asp:Literal ID="msg_PSDisableReceivingLikesFromDifferentCountry" runat="server" /></label>
                        <!-- Login Information   -->
                    </li>
                    <li>
                        <label class="settingsLbl">
                            <asp:Literal ID="msg_PSDisableReceivingOffersFromDifferentCountry" runat="server" /></label>
                        <!-- Login Information   -->
                    </li>--%>
                    <li class="clear"></li>
                </ul>
                <div class="clear"></div>
            </div>
        </div>
        <div class="form-actions">
            <dx:ASPxButton ID="btnPrivacySettingsUpd" runat="server" CssClass="btn lnk172-blue"
                Text="" Native="True" EncodeHtml="False" />
        </div>
    </div>
    <div class="clear"></div>
</asp:Panel>
        </asp:View>
 
 
        <asp:View ID="vwNotifications" runat="server">
<h3 class="top-title"><asp:Literal ID="msg_RightColumnSettingsotificationsTitle" runat="server"/></h3>
<asp:Panel ID="pnlNotification" runat="server" DefaultButton="btnNotificationsUpdate">
    <div class="pe_wrapper nofications">
        <div class="pe_box">
            <h3 class="sub-title"><asp:Literal ID="msg_NSTitle" runat="server" /></h3>
            <div class="pe_form pe_login pe_list">
                <asp:Panel ID="pnlNSUserMessage" runat="server" CssClass="alert alert-success blue" Visible="False">
                    <asp:Label ID="lblNSUserMessage" runat="server" Text="" />
                </asp:Panel>

<dx:ASPxPopupControl runat="server"
    ID="ShowCriteriasPopUp"
    ClientInstanceName="ShowCriteriasPopUp" 
    ClientIDMode="AutoID" 
    Modal="True" 
    AutoUpdatePosition="True"
    ShowShadow="False"
    AllowResize="True" 
    PopupHorizontalAlign="WindowCenter"
    PopupVerticalAlign="WindowCenter" 
    ShowSizeGrip="True" 
    CloseAction="CloseButton"
    AllowDragging="True">
    <Windows>
        <dx:PopupWindow ContentUrl="~/Members/AutoNotificationsSettings.aspx" ContentUrlIFrameTitle="Auto Notifications Settings"
            HeaderText="Auto Notifications Settings" Modal="True" Name="winAutoNotificationsSettings"
            PopupElementID="lnkShowCriterias" Height="600px" Width="600px">
            <ContentStyle HorizontalAlign="Center" VerticalAlign="Middle">
                <Paddings Padding="0px" />
            </ContentStyle>
            <ContentCollection>
                <dx:PopupControlContentControl runat="server" SupportsDisabledAttribute="True">
                </dx:PopupControlContentControl>
            </ContentCollection>
        </dx:PopupWindow>
    </Windows>
    <CloseButtonStyle BackColor="White">
        <HoverStyle>
            <BackgroundImage ImageUrl="//cdn.goomena.com/Images2/popup-wrn/close.png" Repeat="NoRepeat" HorizontalPosition="center"
                VerticalPosition="center"></BackgroundImage>
        </HoverStyle>
        <BackgroundImage ImageUrl="//cdn.goomena.com/Images2/popup-wrn/close.png" Repeat="NoRepeat" HorizontalPosition="center"
            VerticalPosition="center"></BackgroundImage>
        <BorderLeft BorderColor="White" BorderStyle="Solid" BorderWidth="10px" />
    </CloseButtonStyle>
    <CloseButtonImage Url="//cdn.goomena.com/Images/spacer10.png" Height="43px" Width="43px">
    </CloseButtonImage>
    <SizeGripImage Height="40px" Url="//cdn.goomena.com/Images/resize.png" Width="40px">
    </SizeGripImage>
    <ContentStyle HorizontalAlign="Left" VerticalAlign="Middle">
        <Paddings Padding="0px" PaddingTop="25px" />
        <Paddings Padding="0px" PaddingTop="25px" ></Paddings>
    </ContentStyle>
    <HeaderStyle>
        <Paddings Padding="0px" PaddingLeft="0px" />
        <Paddings PaddingTop="0px" PaddingBottom="0px"></Paddings>
        <BorderTop BorderColor="White" BorderStyle="Solid" BorderWidth="5px" />
        <BorderRight BorderColor="White" BorderStyle="Solid" BorderWidth="5px" />
        <BorderBottom BorderColor="White" BorderStyle="Solid" BorderWidth="5px" />
    </HeaderStyle>
    <ContentCollection>
        <dx:PopupControlContentControl runat="server" SupportsDisabledAttribute="True">
        </dx:PopupControlContentControl>
    </ContentCollection>
    <ModalBackgroundStyle BackColor="Black" Opacity="70" CssClass="WhatIs-ModalBG"></ModalBackgroundStyle>
</dx:ASPxPopupControl>
                <ul class="defaultStyle">
                    <%--
			<!-- Recent Matches  -->
			<li><label class="settingsLbl"><asp:Literal ID="msg_NSRecentMatches" runat="server"/></label> 
                <div class="settingsChk">
					<dx:ASPxCheckBox ID="chkNS_EmailRecentMatches" runat="server" EncodeHtml="False" Layout="Flow"/> 
                    <div class="clear"></div>
				</div>
			</li>
                    --%>
                    <!-- new members near   -->
                    <li style="display: block;">
                        <label class="settingsLbl"><asp:Literal ID="msg_NSNewMembersNear" runat="server" /></label>
                        <div class="settingsChk">
                            <dx:ASPxCheckBox ID="chkNS_NotifyNewMembers" runat="server" EncodeHtml="False" Layout="Flow"
                                CheckState="Unchecked"><ClientSideEvents CheckedChanged="" />
                            </dx:ASPxCheckBox>
                            <br />
                            <div class="links">
                                <div class="lfloat">
                                    <asp:LinkButton ID="lnkShowCriterias" runat="server" OnClientClick="return false;">Show Criterias</asp:LinkButton>
                                </div>
                                <div class="lfloat">&nbsp;|&nbsp;<asp:HyperLink ID="lnkShowMembersThatFillsCriteria" runat="server" Visible="False"
                                        CssClass="lnkShowMembersThatFillsCriteria">Show Members That Fills Criteria</asp:HyperLink>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </li>
                    <!-- new likes   -->
                    <li>
                        <label class="settingsLbl">
                            <asp:Literal ID="msg_NSLikes" runat="server" /></label>
                        <div class="settingsChk">
                            <dx:ASPxCheckBox ID="chkNS_NotifyNewLikes" runat="server" EncodeHtml="False" Layout="Flow" />
                            <div class="clear">
                            </div>
                        </div>
                    </li>
                    <!-- offer updates   -->
                    <li>
                        <label class="settingsLbl">
                            <asp:Literal ID="msg_NSOfferUpdates" runat="server" /></label>
                        <div class="settingsChk">
                            <dx:ASPxCheckBox ID="chkNS_NotifyNewUpdatedOffers" runat="server" EncodeHtml="False"
                                Layout="Flow" />
                            <div class="clear">
                            </div>
                        </div>
                    </li>
                    <%--
            <!-- auto reject offer updates   -->
			<li><label class="settingsLbl"><asp:Literal ID="msg_NSAutoRejected" runat="server"/></label> 
                <div class="settingsChk">
					<dx:ASPxCheckBox ID="chkNS_NotifyOnAutoReject" runat="server" EncodeHtml="False" Layout="Flow" /><div class="clear"></div>
				</div>
			</li>
                    --%>
                    <!-- New Messages   -->
                    <li>
                        <label class="settingsLbl">
                            <asp:Literal ID="msg_NSNewMessages" runat="server" /></label>
                        <div class="settingsChk">
                            <dx:ASPxCheckBox ID="chkNS_NotifyOnNewMessage" runat="server" EncodeHtml="False"
                                Layout="Flow" />
                            <div class="clear"></div>
                        </div>
                    </li>
                    <li>
                        <label class="settingsLbl"><asp:Literal ID="msg_NSProfileViewed" runat="server" /></label>
                        <div class="settingsChk">
                            <dx:ASPxCheckBox ID="chkNS_MyProfileViewed" runat="server" EncodeHtml="False"
                                Layout="Flow" />
                            <div class="clear"></div>
                        </div>
                    </li>
                    <li>
                        <label class="settingsLbl"><asp:Literal ID="msg_NSProfilesINotVisited" runat="server" /></label>
                        <div class="settingsChk">
                            <dx:ASPxCheckBox ID="chkNS_ProfilesINotVisited" runat="server" EncodeHtml="False"
                                Layout="Flow" />
                            <div class="clear"></div>
                        </div>
                    </li>
                    <%--
			<!-- Profile Favorites  -->
			<li><label class="settingsLbl"><asp:Literal ID="msg_NSProfileFavorites" runat="server"/></label> 
            <div class="settingsChk">
					<dx:ASPxCheckBox ID="chkNS_NotifyOnFavorite" runat="server" EncodeHtml="False" Layout="Flow"/>  
                    <div class="clear"></div>
				</div>
			</li>
				<!-- Profile Views   -->
			<li><label class="settingsLbl"><asp:Literal ID="msg_NSProfileViews" runat="server"/></label> 
            <div class="settingsChk">
					<dx:ASPxCheckBox ID="chkNS_NotifyOnViewed" runat="server" EncodeHtml="False" Layout="Flow"/>  
                    <div class="clear"></div>
				</div>
			</li>
                    --%>
                    <li class="clear"></li>
                </ul>
                <div class="clear">
                </div>
            </div>
        </div>
        <div class="form-actions">
            <dx:ASPxButton ID="btnNotificationsUpdate" runat="server" CssClass="btn upd-notifications"
                Text="" Native="True" EncodeHtml="False" />
        </div>
    </div>
    <div class="clear"></div>
</asp:Panel>
        </asp:View>


        <asp:View ID="vwCredits" runat="server">
<h3 class="top-title"><asp:Literal ID="msg_RightColumnSettingsTitle4" runat="server" /></h3>
<div class="as_body ab_block ab_settings ab_static">
    <uc1:ProfileCreditsHistory ID="gvHistory" runat="server" />
    <div class="form-actions">
        <asp:HyperLink ID="lnkBuyCredits" runat="server" CssClass="btn lnk172-blue persist_css"
                Text="Buy Credits" 
                NavigateUrl="~/Members/SelectProduct.aspx" />
    </div>
</div>
<div class="clear"></div>
        </asp:View>


    </asp:MultiView>

	<uc2:whatisit ID="WhatIsIt1" runat="server" />

    <dx:ASPxPopupControl runat="server"
        Height="340px" Width="500px"
        ID="popupConfirmPass"
        ClientInstanceName="popupConfirmPass" 
        ClientIDMode="AutoID" 
        AllowDragging="True" 
        PopupVerticalAlign="WindowCenter" 
        PopupHorizontalAlign="WindowCenter" 
        Modal="True" 
        CloseAction="CloseButton"
        AllowResize="True" 
        AutoUpdatePosition="True"
        ShowShadow="False">
    <ContentStyle HorizontalAlign="Left" VerticalAlign="Middle">
        <Paddings Padding="0px" PaddingTop="25px" />
        <Paddings Padding="0px" PaddingTop="25px" ></Paddings>
    </ContentStyle>
    <CloseButtonStyle BackColor="White">
        <HoverStyle>
            <BackgroundImage ImageUrl="//cdn.goomena.com/Images2/popup-wrn/close.png" Repeat="NoRepeat" HorizontalPosition="center"
                VerticalPosition="center"></BackgroundImage>
        </HoverStyle>
        <BackgroundImage ImageUrl="//cdn.goomena.com/Images2/popup-wrn/close.png" Repeat="NoRepeat" HorizontalPosition="center"
            VerticalPosition="center"></BackgroundImage>
        <BorderLeft BorderColor="White" BorderStyle="Solid" BorderWidth="10px" />
    </CloseButtonStyle>
    <CloseButtonImage Url="//cdn.goomena.com/Images/spacer10.png" Height="43px" Width="43px">
    </CloseButtonImage>
    <SizeGripImage Height="40px" Url="//cdn.goomena.com/Images/resize.png" Width="40px">
    </SizeGripImage>
    <HeaderStyle>
        <Paddings Padding="0px" PaddingLeft="0px" />
        <Paddings PaddingTop="0px" PaddingBottom="0px"></Paddings>
        <BorderTop BorderColor="White" BorderStyle="Solid" BorderWidth="5px" />
        <BorderRight BorderColor="White" BorderStyle="Solid" BorderWidth="5px" />
        <BorderBottom BorderColor="White" BorderStyle="Solid" BorderWidth="5px" />
    </HeaderStyle>
    <ModalBackgroundStyle BackColor="Black" Opacity="70" CssClass="WhatIs-ModalBG"></ModalBackgroundStyle>
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server" SupportsDisabledAttribute="True">
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
</div>

    
<script type="text/javascript">
    (function () {
        var itms = $("input,a", "#settings-content");
        for (var c = 0; c < itms.length; c++) {
            if ($(itms[c]).is(".persist_css")) continue;

            if ($(itms[c]).is(".lnk130-fff") || $(itms[c]).is(".lnk200-fff")) {
                $(itms[c]).css("width", "auto");
                var w = $(itms[c]).width();
                $(itms[c]).css("width", "").removeClass("lnk130-fff").removeClass("lnk200-fff")
                if (w < 120)
                    $(itms[c]).addClass("lnk130-fff");
                else
                    $(itms[c]).addClass("lnk200-fff");
            }
            else if ($(itms[c]).is(".lnk172-blue") || $(itms[c]).is(".lnk391-blue")) {
                $(itms[c]).css("width", "auto");
                var w = $(itms[c]).width();
                $(itms[c]).css("width", "").removeClass("lnk172-blue").removeClass("lnk391-blue")
                if (w < 160)
                    $(itms[c]).addClass("lnk172-blue");
                else
                    $(itms[c]).addClass("lnk391-blue");
            }
        }
    })();


    $(function () {
        function m__setPopupClass() {
            if (window["popupConfirmPass"] != null) {
                var popup = popupConfirmPass
                var divId = popup.name + '_PW-1';
                $('#' + divId)
            .addClass('fancybox-popup')
            .prepend($('<div class="header-line"></div>'));
                var closeId = popup.name + '_HCB-1Img';
                $('#' + closeId).addClass('fancybox-popup-close');
                var hdrId = popup.name + '_PWH-1T';
                $('#' + hdrId).addClass('fancybox-popup-header');
                //var frmId = popup.name + '_CSD-1';
                //$('#' + frmId).addClass('iframe-container');
            }
        }
        function m__setPopupClassCreterias() {
            if (window["ShowCriteriasPopUp"] != null) {
                var popup = ShowCriteriasPopUp
                var divId = popup.name + '_PW0';
                $('#' + divId)
                    .addClass('fancybox-popup')
                    .prepend($('<div class="header-line"></div>'));
                var closeId = popup.name + '_HCB0Img';
                $('#' + closeId).addClass('fancybox-popup-close');
                var hdrId = popup.name + '_PWH0T';
                $('#' + hdrId).addClass('fancybox-popup-header');
                //var frmId = popup.name + '_CSD0';
                //$('#' + frmId).addClass('iframe-container');
            }
        }


        setTimeout(m__setPopupClass, 300);
        setTimeout(m__setPopupClassCreterias, 400);
    });
    
</script>
</asp:Content>

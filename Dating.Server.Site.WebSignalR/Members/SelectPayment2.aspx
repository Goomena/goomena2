﻿<%@ Page Title="" Language="vb" AutoEventWireup="true" MasterPageFile="~/Members/Members2Inner.master" CodeBehind="SelectPayment2.aspx.vb" Inherits="Dating.Server.Site.Web.SelectPayment2" %>
<%@ Register assembly="DevExpress.Web.v13.1, Version=13.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%--<%@ Register assembly="SpiceLogicPayPalStd" namespace="SpiceLogicPayPalStandard.Controls.BuyNowButton" tagprefix="cc1" %>
--%>
<%@ Register assembly="DevExpress.Web.v13.1, Version=13.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxPanel" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v13.1, Version=13.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxPopupControl" tagprefix="dx" %>

<%@ Register assembly="DevExpress.Web.v13.1, Version=13.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxRoundPanel" tagprefix="dx" %>
<%--<%@ Register src="~/UserControls/UCSelectPayment.ascx" tagname="UCSelectPayment" tagprefix="uc1" %>--%>
<%@ Register src="../UserControls/MemberLeftPanel.ascx" tagname="MemberLeftPanel" tagprefix="uc1" %>
<%@ Register src="~/UserControls/LiveZillaPublic.ascx" tagname="LiveZillaPublic" tagprefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        .iframePayment {
            height: 820px;
            width: 900px;
            border:0;
            /*border-top:solid 2px #f0f0f0;*/
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">

    <asp:Panel ID="pntTest" runat="server" Visible="False">
    <table>
    <tr>
        <td><dx:ASPxButton ID="btnPayTestUser" runat="server" Text="Payment for test #USER#">
        </dx:ASPxButton></td>
    </tr>
    <tr>
        <td><dx:ASPxMemo ID="txtOutput" runat="server" Height="71px" Width="170px">
        </dx:ASPxMemo></td>
    </tr>
    <tr>
        <td><asp:HyperLink ID="lnkOK" runat="server" NavigateUrl="~/Members/paymentok.aspx">Go to OK page</asp:HyperLink>&nbsp;
        <asp:HyperLink ID="lnkFail" runat="server" 
            NavigateUrl="~/Members/paymentfail.aspx">Go to Fail page</asp:HyperLink></td>
    </tr>
    </table>
        
        
    </asp:Panel>

    <div style="background-color:#ffffff;width: 900px;margin-left:40px;margin-top:20px;">
        <div style="padding:20px;">
            <div style="padding:10px;" class="blueBox"><dx:ASPxLabel ID="lblPaymentDescription" runat="server" Text="lblPaymentDescription" EncodeHtml="False"/></div>
            <div style="padding:10px;"><dx:ASPxLabel ID="lbPaymentExtraInfo" runat="server" Text="lbPaymentExtraInfo" EncodeHtml="False"/></div>
        </div>
        <iframe src="" runat="server" id="iframePayment" frameborder="0" class="iframePayment"></iframe>
    </div>


<%--    <div class="clear"></div>
    <div class="container_12" id="tabs-outter">
        <div class="grid_12 top_tabs">
            <div>
            </div>
        </div></div><div class="clear"></div>
	<div class="container_12" id="content_top"></div>
	<div class="container_12" id="content">
            <div class="grid_3" id="sidebar-outter">

            </div>
            <div class="grid_9 body">
                <div class="middle" id="content-outter">

            <div id="filter" class="m_filter t_filter msg_filter">
	            <div class="lfloat">
		            <h2><asp:Label ID="lblPaymentMethodTitle" runat="server" Text="Report"/></h2>
	            </div>
	            <div class="rfloat">
                    <dx:ASPxHyperLink ID="lnkBack" runat="server" Text="Back" CssClass="btn">
                    </dx:ASPxHyperLink>
	            </div>

	            <div class="clear"></div>
            </div>

            <div class="t_wrap">
	            <div style="background-color:#fff">
        <uc1:UCSelectPayment ID="UCSelectPayment1" runat="server" />
                </div>
            </div>
			    </div>
				<div class="bottom"></div>
            </div>
            <div class="clear">
            </div>
        </div>


    <div class="container_12" id="content_bottom">
    </div>
--%>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cntBodyBottom" runat="server">
    <uc3:LiveZillaPublic ID="ctlLiveZillaPublic" runat="server" />
</asp:Content>

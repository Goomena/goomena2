﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" 
    MasterPageFile="~/Members/Members2Inner.Master" CodeBehind="Conversation.aspx.vb" 
    Inherits="Dating.Server.Site.Web.Conversation" MaintainScrollPositionOnPostback="False" %>
<%@ Register src="~/UserControls/MemberLeftPanel.ascx" tagname="MemberLeftPanel" tagprefix="uc1" %>
<%@ Register src="~/UserControls/DatesControl.ascx" tagname="DatesControl" tagprefix="uc3" %>
<%@ Register src="~/UserControls/WhatIsIt.ascx" tagname="WhatIsIt" tagprefix="uc2" %>
<%@ Register src="~/UserControls/NewMessageControl.ascx" tagname="NewMessageControl" tagprefix="uc3" %>
<%@ Register src="~/UserControls/MessagesConversation.ascx" tagname="MessagesConversation" tagprefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<%--<script src="//cdn.goomena.com/Scripts/jquery-ui-1.10.3.min.js" type="text/javascript"></script>--%>
<script src="//cdn.goomena.com/Scripts/jquery.mousewheel.js" type="text/javascript"></script>
<script src="/v1/Scripts/conversation.js?v=20" type="text/javascript"></script>
<script src="/v1/Scripts/conversation2.js?v=20" type="text/javascript"></script>
<style type="text/css">
/*#ctl00_content_popupShowWarning_PWC-1{position:relative;height: 303px;}*/
#ctl00_content_popupShowWarning_CSD-1{position:relative;height: 303px;}
</style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
<div id="conv-item" class="<%= if(IsMaleSendingMessageFirstTime, " first-message", "") %>">
    <h3 id="h3Dashboard" class="page-title">
        <asp:Literal ID="lblItemsName" runat="server" />
        <asp:LinkButton ID="lnkViewDescription" runat="server" CssClass="more_info2" OnClientClick="OnMoreInfoClick('[contentUrl]', this, '[headerText]');return false;" />
    </h3>
    <div class="m_wrap">
        <div id="divActionError" runat="server" visible="false">
            <div class="items_hard">
                <asp:Literal ID="lblActionError" runat="server" />
                <p><asp:HyperLink ID="lnkAlternativeAction" runat="server" CssClass="btn lighter" onclick="ShowLoading();"></asp:HyperLink></p>
                <div class="clear"></div>
            </div>
        </div>
        <div class="items_none no-results" id="divMessageNotFound" runat="server" visible="false">
            <asp:Literal ID="ltrMessageNotFound" runat="server" />
            <p><asp:HyperLink ID="lnkBackMNF" runat="server" CssClass="btn lighter" Visible="false"
                    onclick="ShowLoading();" /></p>
            <div class="clear"></div>
        </div>
    </div>

    <asp:MultiView ID="mvMessages" runat="server" ActiveViewIndex="1">

        <asp:View ID="vwCreateMessage" runat="server">
            <uc3:NewMessageControl ID="newMsgCtl" runat="server" />
        </asp:View>


        <asp:View ID="vwNoMessages" runat="server">

            <div class="m_wrap">
                <div class="items_none">
                    <asp:Label ID="lblNoMessagesText" runat="server" Text="">
                    </asp:Label>

                    <div class="search_members_button">
                        <asp:HyperLink ID="lnkSearchMembers" runat="server" CssClass="btn lighter" NavigateUrl="~/Members/Search.aspx"
                            onclick="ShowLoading();">
                            <asp:Literal ID="msg_SearchOurMembersText" runat="server"></asp:Literal>
                        </asp:HyperLink>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>


        </asp:View>

        <asp:View ID="vwEmpty" runat="server">
        </asp:View>

        <asp:View ID="vwConversation" runat="server">
            <asp:UpdatePanel ID="updConv" runat="server" RenderMode="Inline">
                <ContentTemplate>


            <div class="m_wrap" id="divProfileDeletedInfo" runat="server" visible="false">
                <div class="clearboth padding"></div>
                <div class="items_none">
                    <asp:Literal ID="lblProfileDeletedInfo" runat="server"></asp:Literal>
                    <div class="clear"></div>
                </div>
            </div>

            <div class="m_wrap" id="divSendAnotherMessageInfo" runat="server" visible="false">
                <div class="clearboth padding"></div>
                <div class="items_none">
                    <asp:Literal ID="lblSendAnotherMessageInfo" runat="server" />
                    <div class="clear"></div>
                </div>
            </div>

            <asp:UpdatePanel ID="updOffers" runat="server" RenderMode="Inline">
                <ContentTemplate>
                    <div class="l_wrap" style="margin-bottom: 20px;">
                        <uc3:DatesControl ID="newDates" runat="server" UsersListView="DatesOffers" EnableViewState="true" />
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>

            <div class="m_wrap" id="divDateAndInfo" runat="server" visible="false">
                <div class="items_none no-results">
                    <asp:Literal ID="lblDateAndInfo" runat="server"/>
	                <div class="clear"></div>
                </div>
            </div>

            <div id="conv-messages-list">
            <asp:UpdatePanel ID="updCnv" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Panel ID="pnlMoreMsgs" runat="server" CssClass="pnlMoreMsgs">
                        <div id="top-links-wrap">
                        <table class="center-table" cellpadding="0" cellspacing="0">
                            <tr>
                                <td valign="top"><div id="history-btns-title"><asp:Label ID="lblHistoryButtonsTitle" runat="server">Show older <br />messages options:</asp:Label></div></td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <div id="top-links">
                                        <ul id="top-links-period" class="hidden">
                                            <%--<li class="link-yesterday"><asp:HyperLink ID="lnkShowYesterday" runat="server" NavigateUrl="javascript:void(0);" CssClass="show-messages-link" onclick="loadMessages(1);return false;"/></li>
                                            <li class="empty hidden"></li>--%>
                                            <li class="link-7days"><asp:HyperLink ID="lnkShow7Days" runat="server" NavigateUrl="javascript:void(0);" CssClass="show-messages-link" onclick="loadMessages(7);return false;"/></li>
                                            <li class="empty hidden"></li>
                                            <li class="link-30days"><asp:HyperLink ID="lnkShow30Days" runat="server" NavigateUrl="javascript:void(0);" CssClass="show-messages-link" onclick="loadMessages(30);return false;"/></li>
                                            <li class="empty hidden"></li>
                                            <li class="link-3months"><asp:HyperLink ID="lnkShow3Months" runat="server" NavigateUrl="javascript:void(0);" CssClass="show-messages-link" onclick="loadMessages(90);return false;"/></li>
                                            <li class="empty hidden"></li>
                                            <li class="link-all"><asp:HyperLink ID="lnkShowAll" runat="server" NavigateUrl="javascript:void(0);" CssClass="show-messages-link" onclick="loadMessages('all');return false;"/></li>
                                        </ul>
                                        <div class="clear"></div>
                                        <ul id="top-links-more">
                                            <li><asp:HyperLink ID="lnkMoreMsgs" runat="server" NavigateUrl="javascript:void(0);" CssClass="more-messages show-messages-link" onclick="loadMessages('more');return false;" /></li>
                                        </ul>
                                        <div class="clear"></div>
                                    </div>

                                </td></tr>
                        </table>
                                <%--<table class="center-table" cellpadding="0" cellspacing="0">
                            <tr><td valign="top"><ul id="top-links">
                                <li><asp:HyperLink ID="lnkLessMsgs" runat="server" NavigateUrl="javascript:void(0);" CssClass="btn less-messages list-btn lnk83 hidden" onclick="loadLessMessages();return false;"/></li>
                                <li>&nbsp;</li>
                                <li><asp:HyperLink ID="lnkMoreMsgs" runat="server" NavigateUrl="javascript:void(0);" CssClass="btn more-messages list-btn lnk112 hidden" onclick="loadMoreMessages();return false;" /></li>
                            </ul></td></tr>
                        </table>--%>
                        </div>


                    </asp:Panel>
                    <div id="all_messages">
                        <div id="loaded_messages"></div>
                        <div id="initial_messages">
                            <uc3:MessagesConversation ID="convCtl" runat="server" EnableViewState="false" />
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            </div>     
            
            <div id="first_message_info" runat="server" visible="false" clientidmode="Static">
                <div class="top-panel">
                    <div class="left"></div>
                    <div class="middle">
                        <p><asp:Literal ID="lblMaleFirstMsgTopPanelText" runat="server" /></p>
                    </div>
                    <div class="right">
                        <div class="pr-photo-94-noshad">
                            <asp:HyperLink ID="lnkFemaleFirstMsg" runat="server" NavigateUrl="" onclick="ShowLoading();"><asp:Image ID="imgFemaleFirstMsg" runat="server" ImageUrl="" class="round-img-94" /></asp:HyperLink>
                        </div>
                        <asp:HyperLink ID="lnkFemaleFirstMsgLogin" runat="server" NavigateUrl="" onclick="ShowLoading();" CssClass="login-name"></asp:HyperLink>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="bottom-panel">
                    <p><asp:Literal ID="lblMaleFirstMsgBottomPanelText" runat="server" /></p>
                    <asp:HyperLink ID="btnRandomText" runat="server" CssClass="btn btn-random-text" OnClientClick="return false;" NavigateUrl="javascript:void(0);">Random Text</asp:HyperLink>
                </div>
                <div class="bottom-panel2" ID="divMaleFirstMsgBottomPanelText2" runat="server">       
                    <asp:Literal ID="lblMaleFirstMsgBottomPanelText2" runat="server" />
                </div>
            </div>

            <div class="cnv_item" id="write_message_wrap" runat="server" visible="true" clientidmode="Static">
                <div class="items_none" id="pnlSendMessageErr" runat="server" visible="false">
                    <div class="items_none_wrap">
                        <div class="arrow"></div>
                        <asp:Literal ID="lblSendMessageErr" runat="server"></asp:Literal>&nbsp;&nbsp;&nbsp;<img id="SiteRulesTIP" runat="server" visible="false" alt="" src="//cdn.goomena.com/Images2/payment/erwtimatiko.png" style="vertical-align:text-top;text-align:right;" />
                        <p><asp:HyperLink ID="lnkSendMessageAltAction" runat="server" CssClass="btn lighter" NavigateUrl="~/Members/SelectProduct.aspx" onclick="ShowLoading();"/></p>
                        <div class="clear"></div>
                    </div>
                </div>

                <div class="left">&nbsp;</div>
                <div class="middle text-black">
                    <div class="write-msg-box closed">
                        <div id="write_message">
                            <asp:UpdatePanel ID="updWriteMsg" runat="server" RenderMode="Inline">
                                <ContentTemplate>
                            <div class="msg_reply center">
                                <asp:Panel ID="pnlWriteMsgErr" runat="server" CssClass="alert alert-error" Visible="False" EnableViewState="false">
                                    <asp:Label ID="lblWriteMsgErr" runat="server" Text=""/>
                                </asp:Panel>
                                <div class="write-message-hdr">
                                    <asp:Label ID="lblWriteMsgHdr" runat="server" Text=""/>
                                </div>
                                <div class="msg-box" style="padding-top:10px;position:relative;">
                                    <dx:ASPxMemo ID="txtWriteMsgText" runat="server" Columns="50" Rows="7" ClientInstanceName="txtWriteMsgText">
                                    </dx:ASPxMemo>
                                </div>
                                <div id="msg-emoticons">
                                    <div id="emoticons-link"><asp:HyperLink ID="lnkShowEmoticons" runat="server" NavigateUrl="javascript:void(0);" onclick="toggleEmoticons();return false;" ToolTip="Insert Emoticons"><img src="//cdn.goomena.com/Images2/arrow10x4.png" alt="" /></asp:HyperLink></div>
                                    <div id="emoticons-box"></div>
                                </div>
                            </div>

                            <div class="center" style="width:212px;">
                                <asp:Button ID="btnSendMessage" runat="server" CssClass="btn read-message lnk212-green"
                                    Text="Send Message" UseSubmitBehavior="false" OnClientClick="sendingNewMessage(this);" />
                                    <p><asp:HyperLink ID="lnkCancel" runat="server" NavigateUrl="javascript:void(0);" onclick="hideWriteMessage();return false;">Cancel</asp:HyperLink>
                                        <asp:HyperLink ID="lnkClearText" runat="server" NavigateUrl="javascript:void(0);" onclick="clearMessage();return false;" Visible="false">Clear Message</asp:HyperLink></p>
                            </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
                <div class="right text-black">
                    <div class="photo">
                        <div class="pr-photo-88-noshad">
                            <asp:HyperLink ID="lnkFromRight" runat="server" NavigateUrl="#" onclick="ShowLoading();"><img id="imgFromRight" runat="server" class="round-img-88" alt=""/></asp:HyperLink>
                        </div>
                        <div class="m-arrow"></div>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>

                <div id="first_message_info_bottom" runat="server" visible="false" clientidmode="Static">
                    <div class="arrow"></div>
                    <div class="inner-box">
                        <asp:Literal ID="lblMaleFirstMsgBottomPanelText3" runat="server" />
                    </div>
                </div>
            </div>

            <div id="send_buttons" runat="server" visible="true" clientidmode="Static">
                <div id="divSendAnotherMessage" runat="server" visible="false" class="center">
                    <asp:HyperLink ID="btnSendAnotherMessage" runat="server" CssClass="btn read-message lnk212-green"
                        Text="Send another message" NavigateUrl="#" onclick="writeMessage();return false;" />
                </div>
                <div id="divSendWithCredits" runat="server" visible="false" class="center">
                    <asp:HyperLink ID="btnSendMessageOnce" runat="server" CssClass="btn read-message lnk212-green"
                        Text="Send message for ###UNLOCK_MESSAGE_SEND_CRD### credits" NavigateUrl="#"
                        onclick="writeMessage();return false;" />
                    <asp:HyperLink ID="btnSendMessagesUnl" runat="server" CssClass="btn read-message lnk212-green"
                        Text="Send unlimited messages for ###UNLOCK_CONVERSATIONCRD### credits" NavigateUrl="#"
                        onclick="writeMessage();return false;" 
                        Visible="false" EnableViewState="false" />
                </div>
            </div>
                            </ContentTemplate>
                </asp:UpdatePanel>

        </asp:View>
    </asp:MultiView>



    <dx:ASPxPopupControl runat="server"
        ID="popupShowWarning"
        Height="378px"
        Width="551px"
        ClientInstanceName="popupShowWarning" 
        ClientIDMode="AutoID" 
        AllowDragging="True" 
        PopupVerticalAlign="WindowCenter" 
        PopupHorizontalAlign="WindowCenter" 
        Modal="True" 
        AllowResize="True" 
        AutoUpdatePosition="True" 
        HeaderText="Warning!" 
        CloseAction="CloseButton"
        ShowShadow="False">
        <ContentStyle HorizontalAlign="Left" VerticalAlign="Middle" Font-Names="Segoe UI, Arial">
            <Paddings Padding="0px" PaddingTop="25px" />
            <Paddings Padding="0px" PaddingTop="25px" ></Paddings>
        </ContentStyle>
        <ClientSideEvents CloseButtonClick="function(s, e) {
	    skipReadingMessage();
    }" />
        <CloseButtonStyle BackColor="White">
            <HoverStyle>
                <BackgroundImage ImageUrl="//cdn.goomena.com/Images2/popup-wrn/close.png" Repeat="NoRepeat" HorizontalPosition="center"
                    VerticalPosition="center"></BackgroundImage>
            </HoverStyle>
            <BackgroundImage ImageUrl="//cdn.goomena.com/Images2/popup-wrn/close.png" Repeat="NoRepeat" HorizontalPosition="center"
                VerticalPosition="center"></BackgroundImage>
            <BorderLeft BorderColor="White" BorderStyle="Solid" BorderWidth="10px" />
        </CloseButtonStyle>
        <CloseButtonImage Url="//cdn.goomena.com/Images/spacer10.png" Height="43px" Width="43px">
        </CloseButtonImage>
        <SizeGripImage Height="40px" Url="//cdn.goomena.com/Images/resize.png" Width="40px">
        </SizeGripImage>
        <HeaderStyle>
            <Paddings Padding="0px" PaddingLeft="0px" />
            <Paddings PaddingTop="0px" PaddingBottom="0px"></Paddings>
            <BorderTop BorderColor="White" BorderStyle="Solid" BorderWidth="5px" />
            <BorderRight BorderColor="White" BorderStyle="Solid" BorderWidth="5px" />
            <BorderBottom BorderColor="White" BorderStyle="Solid" BorderWidth="5px" />
        </HeaderStyle>
        <ModalBackgroundStyle BackColor="Black" Opacity="70"></ModalBackgroundStyle>
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server" SupportsDisabledAttribute="True">
                <div class="top-content">
                    <div style="width:485px;margin:10px auto;font-size:15px;font-family:Segoe UI;">
                        <asp:Label ID="lblWarningMessage" runat="server" Text="">
                        </asp:Label>
                    </div>
                    <table style="margin:0 auto;"> 
                        <tr>
                            <td>
                                <asp:Button ID="btnContinue" runat="server" Text="Continue" OnClientClick="continueReadingMessage();return false;" CssClass="popup-warning-continue" />
                            </td>
                            <td>&nbsp;</td>
                            <td>
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClientClick="skipReadingMessage();return false;" CssClass="popup-warning-cancel" />
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="footer-content-wrap">
                    <div class="footer-content">
                        <div class="lfloat">
                            <asp:CheckBox ID="chkSuppressWarning" runat="server" Checked="false"  Text="" CssClass="popup-warning-check" />
                        </div>
                        <div class="rfloat">
                            <asp:HyperLink ID="btnSave" runat="server" OnClick="return false;" CssClass="popup-save"><span class='popup-save-img'></span>[CONTENT]</asp:HyperLink>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
    <uc2:whatisit ID="WhatIsIt1" runat="server" />
</div>


<script type="text/javascript">
    function scrollAndWrite() {
        scrollWin($('a.read-message', '#send_buttons'), -200, 500);
        setTimeout(function () { writeMessage(); }, 800);
    }

    function __performStartup(args) {
        var openSend = true;
        var focus_writemessages = false;
        try {
            if (args != null) {
                if (args["writemessages"] != null && args["writemessages"] == false)
                    openSend = false;
                if (args["focus_writemessages"] == true)
                    focus_writemessages = true;
            }
        }
        catch (e) { }
        performStartup();

        if (gup("focus") == "send") {
            scrollWin($('a.read-message', '#send_buttons'), -200, 500);
            if (focus_writemessages) 
                setTimeout(function () { writeMessage(); }, 800);
        }
        else if (openSend && gup("open") == "send") {
            scrollAndWrite();
            //scrollWin($('a.read-message', '#send_buttons'), -200, 500);
            //setTimeout(function () { writeMessage(); }, 800);
        }
        else if (!stringIsEmpty('<%=  Mybase.ScrollToElem %>')) {
            if ($('.cnv_item', '#all_messages').length > 2)
                scrollWin('#<%=  Mybase.ScrollToElem %>', -360, 800);
        }

        LoadEmoticonsList();
    }

    function GetRandomPredefinedMessage() {
        if (stringIsEmpty($('#<%=btnRandomText.ClientID %>').attr("disabled"))) {
            $('#<%=btnRandomText.ClientID %>').click(function (e) {
                performRequest_GetRandomPredefinedMessage(e, $('#<%=btnRandomText.ClientID %>'), function (text) {
                    if (window["txtWriteMsgText"] != null) {
                        txtWriteMsgText.SetText(text)
                    }
                });
            })
        }
    }

   
    function __setPopupClass() {
        var popup = popupShowWarning;
        var divId = popup.name + '_PW-1';
        $('#' + divId)
        .addClass('fancybox-popup-wrn')
        .prepend($('<div class="header-line"></div>'));
        var closeId = popup.name + '_HCB-1Img';
        $('#' + closeId).addClass('fancybox-popup-close');
        var hdrId = popup.name + '_PWH-1T';
        $('#' + hdrId).addClass('fancybox-popup-header');
        popup.SetHeaderText('<%= WarningPopup_HeaderText %>');
    }

    CurrentLoginName1 = '<%= System.Web.HttpUtility.UrlEncode(CurrentLoginName) %>';
    __back_to_menu_left = '<%= MyBase.CurrentPageData.GetCustomString("Back.To.Menu.Left") %>';
    __back_to_menu_right = '<%= MyBase.CurrentPageData.GetCustomString("Back.To.Conversion.List.Menu") %>';
    jQuery(function ($) {
        conversations_TopLinks();

        if ('<%= MyBase.FocusAndWriteMessages.tostring().tolower() %>' == 'true')
            __performStartup({ focus_writemessages: true });
        else
            __performStartup();

        $('#<%=btnSave.ClientID %>').click(function () {
            if ($('#<%=chkSuppressWarning.ClientID %>:checked').length > 0) 
                fnSuppressWarning_WhenReadingMessageFromDifferentCountry(true);
            else 
                fnSuppressWarning_WhenReadingMessageFromDifferentCountry(false);

            skipReadingMessage();
        })
        GetRandomPredefinedMessage();

    });
    $(function () {
        setTimeout(__setPopupClass, 300);
    });

    $(function () { performRequest_MessagesList(); });

    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function (sender, args) {
        conversations_TopLinks();
        GetRandomPredefinedMessage();
        __performStartup({writemessages:false});
    });


</script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="leftPanelUpperContent" runat="server">
    <div id="quick-list-messages" style="display:none;">
        <div class="top-links">
            <div class="lfloat">
                <asp:HyperLink ID="lnkMessages" runat="server" Text="Messages" 
                    NavigateUrl="~/Members/Messages.aspx" 
                    onclick="ShowLoading();"
                    CssClass="lnkMessages icon" />
            </div>
            <div class="rfloat">
                <asp:HyperLink ID="lnkMessagesAll" runat="server" Text="View All" 
                    NavigateUrl="~/Members/Messages.aspx" 
                    onclick="ShowLoading();"
                    CssClass="lnkMessagesAll" />
            </div>
            <div class="clear"></div>
        </div>
        <div id="scroll-bar-wrap">
            <div id="scroll-bar">
                <div id="slider"></div>
            </div>
        </div>
        <div id="list-container-wrap">
            <div id="list-container">
            </div>
        </div>
        <hr id="hr-separator"/>
        <asp:HyperLink ID="lnkBackToMainMenu" runat="server" Text="Back to menu" 
                    NavigateUrl="~/Members/Messages.aspx" 
                    onclick="ToggleMainMenu();return false;"
                    CssClass="back-to-menu" />
    </div>

</asp:Content>


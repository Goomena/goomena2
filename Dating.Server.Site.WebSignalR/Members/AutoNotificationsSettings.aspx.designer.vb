﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class AutoNotificationsSettings

    '''<summary>
    '''updCnt control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents updCnt As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''divLocationsCountry control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents divLocationsCountry As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''msg_SelectCountry control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents msg_SelectCountry As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''cbCountry control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cbCountry As Global.DevExpress.Web.ASPxEditors.ASPxComboBox

    '''<summary>
    '''liLocationsRegion control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents liLocationsRegion As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''msg_SelectZip control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents msg_SelectZip As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''msg_SelectRegionText control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents msg_SelectRegionText As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''cbRegion control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cbRegion As Global.DevExpress.Web.ASPxEditors.ASPxComboBox

    '''<summary>
    '''sdsRegion control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents sdsRegion As Global.System.Web.UI.WebControls.SqlDataSource

    '''<summary>
    '''liLocationsCity control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents liLocationsCity As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''msg_SelectCity control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents msg_SelectCity As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''cbCity control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cbCity As Global.DevExpress.Web.ASPxEditors.ASPxComboBox

    '''<summary>
    '''sdsCity control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents sdsCity As Global.System.Web.UI.WebControls.SqlDataSource

    '''<summary>
    '''msg_SelectDistance control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents msg_SelectDistance As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''ddldistanceAdv control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddldistanceAdv As Global.Dating.Server.Site.Web.DistanceDropDown

    '''<summary>
    '''msg_AgeWord control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents msg_AgeWord As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''msg_FromWord2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents msg_FromWord2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''ageMin2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ageMin2 As Global.Dating.Server.Site.Web.AgeDropDown

    '''<summary>
    '''msg_ToWord2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents msg_ToWord2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''ageMax2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ageMax2 As Global.Dating.Server.Site.Web.AgeDropDown

    '''<summary>
    '''msg_PersonalHeight control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents msg_PersonalHeight As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''msg_FromWord control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents msg_FromWord As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''cbHeightMin control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cbHeightMin As Global.DevExpress.Web.ASPxEditors.ASPxComboBox

    '''<summary>
    '''msg_ToWord control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents msg_ToWord As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''cbHeightMax control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cbHeightMax As Global.DevExpress.Web.ASPxEditors.ASPxComboBox

    '''<summary>
    '''msg_PersonalEyeClr control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents msg_PersonalEyeClr As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''cbEyeColor control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cbEyeColor As Global.System.Web.UI.WebControls.CheckBoxList

    '''<summary>
    '''lnkEyeClr control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkEyeClr As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''lnkEyeAll control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkEyeAll As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''msg_PersonalHairClr control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents msg_PersonalHairClr As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''cbHairColor control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cbHairColor As Global.System.Web.UI.WebControls.CheckBoxList

    '''<summary>
    '''lnkHairClr control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkHairClr As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''lnkHairAll control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkHairAll As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''msg_PersonalBodyType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents msg_PersonalBodyType As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''cbBodyType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cbBodyType As Global.System.Web.UI.WebControls.CheckBoxList

    '''<summary>
    '''lnkBodyClr control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkBodyClr As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''lnkBodyAll control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkBodyAll As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''msg_RelationshipStatus control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents msg_RelationshipStatus As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''cbRelationshipStatus control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cbRelationshipStatus As Global.System.Web.UI.WebControls.CheckBoxList

    '''<summary>
    '''lnkRelStatClr control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkRelStatClr As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''lnkRelStatAll control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkRelStatAll As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''msg_PersonalReligion control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents msg_PersonalReligion As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''cbReligion control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cbReligion As Global.System.Web.UI.WebControls.CheckBoxList

    '''<summary>
    '''lnkReligClr control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkReligClr As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''lnkReligAll control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkReligAll As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''msg_PersonalEthnicity control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents msg_PersonalEthnicity As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''cbEthnicity control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cbEthnicity As Global.System.Web.UI.WebControls.CheckBoxList

    '''<summary>
    '''lnkEthnClr control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkEthnClr As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''lnkEthnAll control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkEthnAll As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''msg_PersonalChildren control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents msg_PersonalChildren As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''cbChildren control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cbChildren As Global.System.Web.UI.WebControls.CheckBoxList

    '''<summary>
    '''lnkChildClr control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkChildClr As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''lnkChildAll control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkChildAll As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''msg_PersonalSmokingHabit control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents msg_PersonalSmokingHabit As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''cbSmoking control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cbSmoking As Global.System.Web.UI.WebControls.CheckBoxList

    '''<summary>
    '''lnkSmokeClr control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkSmokeClr As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''lnkSmokeAll control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkSmokeAll As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''msg_PersonalDrinkingHabit control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents msg_PersonalDrinkingHabit As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''cbDrinking control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cbDrinking As Global.System.Web.UI.WebControls.CheckBoxList

    '''<summary>
    '''lnkDrinkClr control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkDrinkClr As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''lnkDrinkAll control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkDrinkAll As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''btnSave control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnSave As Global.DevExpress.Web.ASPxEditors.ASPxButton
End Class

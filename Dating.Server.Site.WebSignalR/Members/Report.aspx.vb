﻿Imports Dating.Server.Core.DLL
Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Datasets.DLL.DSMembers


Public Class Report
    Inherits BasePage

    'Dim _pageData As clsPageData
    'Protected ReadOnly Property CurrentPageData As clsPageData
    '    Get
    '        If (_pageData Is Nothing) Then _pageData = New clsPageData(Context)
    '        Return _pageData
    '    End Get
    'End Property



    Protected Overrides Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        SetMasterPage(CurrentPageData)

        Try
            If clsCurrentContext.VerifyLogin() = True Then
                Me.MasterPageFile = "~/Members/Members2.Master"
            Else
                clsCurrentContext.ClearSession(Session)
                If (Not Me.IsCallback) Then
                    Response.Redirect(ResolveUrl("~/Login.aspx"), False)
                End If
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub

    Private Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        Try

            If (Not Me.IsPostBack) Then
                LoadLAG()
                If (Request.UrlReferrer IsNot Nothing) Then
                    lnkBack.NavigateUrl = Request.UrlReferrer.ToString()
                Else
                    lnkBack.NavigateUrl = "~/Members/Profile.aspx?p=" & HttpUtility.UrlEncode(Request.QueryString("p"))
                End If
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub

    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender

        Try
            Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub



    Public Overrides Sub Master_LanguageChanged()

        If (Me.Visible) Then
            Me._pageData = Nothing
            LoadLAG()
        End If

    End Sub


    Protected Sub LoadLAG()
        Try
            '   Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            btnSubmitReport.Text = CurrentPageData.GetCustomString("btnSubmitReport")

            Dim _userId As Integer = 0
            If (Request.QueryString("p") <> "") Then
                Dim prof As EUS_Profile = Nothing

                Try
                    Using cmdb As CMSDBDataContext = Me.GetCMSDBDataContext
                        prof = DataHelpers.GetEUS_ProfileMasterByLoginName(cmdb, Request.QueryString("p"), ProfileStatusEnum.Approved)
                    End Using

                    _userId = prof.ProfileID
                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "")
                End Try


                If (_userId > 0) Then

                    Dim dt As DataTable = DataHelpers.GetEUS_LISTS_ReportingReason().EUS_LISTS_ReportingReason()
                    Web.ClsCombos.FillComboUsingDatatable(dt, Me.Session("LagID"), "ReportingReasonId", cbReportReason, True, "US")
                    cbReportReason.Items(0).Selected = True
                    txtBody.Visible = False

                    lblReportingTitle.Text = CurrentPageData.GetCustomString(lblReportingTitle.ID)
                    lblReportingTitle.Text = lblReportingTitle.Text.Replace("###LOGINNAME###", "<span class=""login-name"">" & prof.LoginName & "</span>")

                    lblWhyReporting.Text = CurrentPageData.GetCustomString(lblWhyReporting.ID)
                    lblWhyReporting.Text = lblWhyReporting.Text.Replace("###LOGINNAME###", "<span class=""login-name"">" & prof.LoginName & "</span>")

                    lnkBack.Text = CurrentPageData.GetCustomString(lnkBack.ID)

                    lblAdditionalInfo.Text = CurrentPageData.GetCustomString("lblAdditionalInfo_ND")

                    Dim fileName As String = ""
                    Dim dr As DSMembers.EUS_CustomerPhotosRow = DataHelpers.GetProfilesDefaultPhoto(_userId)
                    If (dr IsNot Nothing) Then fileName = dr.FileName

                    lnkOtherProf.NavigateUrl = ResolveUrl("~/Members/Profile.aspx?p=" & HttpUtility.UrlEncode(prof.LoginName))
                    imgOtherProf.ImageUrl = ProfileHelper.GetProfileImageURL(_userId, fileName, prof.GenderId, True, Me.IsHTTPS, PhotoSize.D150)
                End If
            End If





            'lblItemsName.Text = Me.CurrentPageData.GetCustomString("CartReportView")
            lnkViewDescription.Text = Me.CurrentPageData.GetCustomString("CartReportWhatIsIt")
            lnkViewDescription.OnClientClick = OnMoreInfoClickFunc(
                lnkViewDescription.OnClientClick,
                ResolveUrl("~/Members/InfoWin.aspx?info=report"),
                Me.CurrentPageData.GetCustomString("CartReportView"))
            WhatIsIt1.PopupControl.HeaderText = Me.CurrentPageData.GetCustomString("CartReportView")


        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Protected Sub btnSubmitReport_Click(sender As Object, e As EventArgs) Handles btnSubmitReport.Click
        Try
            Using cmdb As CMSDBDataContext = Me.GetCMSDBDataContext


                Dim prof As EUS_Profile = DataHelpers.GetEUS_ProfileMasterByLoginName(cmdb, Request.QueryString("p"), ProfileStatusEnum.Approved)

                Dim messageText As String = ""
                '<message><![CDATA[
                'Contact message from Goomena.com

                'Member who sent a report:
                'Login name: ###LOGINNAME###
                'Profile Id: ###PROFILEID###

                'Member for whom report was sent
                'Login name: ###LOGINNAMEOTHER###
                'Profile Id: ###PROFILEIDOTHER###

                'Reporting reason: ###REASON###
                'Message: ###MESSAGE###
                ']]></message>.Value

                Dim subject As String = globalStrings.GetCustomString("EmailSendToSupport_MemberReportingSubject", "US")
                messageText = globalStrings.GetCustomString("EmailSendToSupport_MemberReporting", "US")
                'If (Not txtBody.Visible) Then txtBody.Text = ""

                messageText = messageText.Replace("###LOGINNAME###", Me.GetCurrentProfile().LoginName)
                messageText = messageText.Replace("###PROFILEID###", Me.GetCurrentProfile().ProfileID)
                messageText = messageText.Replace("###LOGINNAMEOTHER###", prof.LoginName)
                messageText = messageText.Replace("###PROFILEIDOTHER###", prof.ProfileID)
                messageText = messageText.Replace("###REASON###", cbReportReason.SelectedItem.Text)
                messageText = messageText.Replace("###MESSAGE###", txtBody.Text)

                'Dim sendToEmailAddress As String = ConfigurationManager.AppSettings("gToEmail")
                clsMyMail.SendMail(ConfigurationManager.AppSettings("gToEmail"), subject, messageText, True)

                'pnlSendMessageForm.Visible = False
                'pnlSendMessageWrap.Visible = True

                divReportForm.Visible = False
                btnSubmitReport.Visible = False
                lblMessageSent.Text = CurrentPageData.GetCustomString("MessageSentSuccessfully")
                lblMessageSent.Visible = True

                Try
                    clsUserDoes.LogReporting(Me.GetCurrentProfile().ProfileID, prof.ProfileID, cbReportReason.SelectedItem.Value, txtBody.Text)
                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "")
                End Try
            End Using
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
            'lblMessageSent.Text = ShowUserErrorMsg(CurrentPageData.GetCustomString("MessageSendFailed"))
        End Try

    End Sub


    Protected Sub cbpnlReport_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cbpnlReport.Callback
        Try
            Dim dt As Dating.Server.Datasets.DLL.DSLists.EUS_LISTS_ReportingReasonDataTable = DataHelpers.GetEUS_LISTS_ReportingReason().EUS_LISTS_ReportingReason
            Dim dr As Dating.Server.Datasets.DLL.DSLists.EUS_LISTS_ReportingReasonRow = (From itm In dt
                                                            Where itm.ReportingReasonId = e.Parameter
                                                            Select itm).FirstOrDefault()

            txtBody.Visible = (dr.US = "Other")
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


End Class
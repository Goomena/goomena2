﻿Imports System.Drawing
Imports DevExpress.Web.ASPxEditors
Imports Dating.Server.Core.DLL
Imports Dating.Server.Core.DLL.clsUserDoes
Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Datasets.DLL.DSMembers

Public Class Messages
    Inherits BasePage


#Region "Props"

    Public Property ScrollToElem As String

    'Dim _pageData As clsPageData
    'Protected ReadOnly Property CurrentPageData As clsPageData
    '    Get
    '        If (_pageData Is Nothing) Then _pageData = New clsPageData(Context)
    '        Return _pageData
    '    End Get
    'End Property

    Protected Property MessagesView As MessagesViewEnum
        Get
            Return ViewState("MessagesView")
        End Get
        Set(value As MessagesViewEnum)
            ViewState("MessagesView") = value
        End Set
    End Property


    Private __showActive As Boolean? = True
    Protected ReadOnly Property showActive As Boolean?
        Get
            If (Request("inactive") = "1") Then __showActive = False
            Return __showActive
        End Get
    End Property


    Protected Property OfferId As Integer
        Get
            Return ViewState("OfferId")
        End Get
        Set(value As Integer)
            ViewState("OfferId") = value
        End Set
    End Property


    Protected Property UserIdInView As Integer
        Get
            Return ViewState("UserIdInView")
        End Get
        Set(value As Integer)
            ViewState("UserIdInView") = value
        End Set
    End Property


    Protected Property SubjectInView As String
        Get
            Return ViewState("SubjectInView")
        End Get
        Set(value As String)
            ViewState("SubjectInView") = value
        End Set
    End Property


    Protected Property MessageIdInView As Long
        Get
            Return ViewState("MessageIdInView")
        End Get
        Set(value As Long)
            ViewState("MessageIdInView") = value
        End Set
    End Property

    ' used with delete message action
    Protected Property MessageIdListInView As List(Of Long)
        Get
            Return ViewState("MessageIdListInView")
        End Get
        Set(value As List(Of Long))
            ViewState("MessageIdListInView") = value
        End Set
    End Property


    Protected Property BackUrl As String
        Get
            Return ViewState("BackUrl")
        End Get
        Set(value As String)
            ViewState("BackUrl") = value
        End Set
    End Property




    Public Property ItemsPerPage As Integer

        Get
            Dim perPageNumber As Integer = 0

            Dim index As Integer = cbPerPage.SelectedIndex
            If (index = 0) Then
                perPageNumber = 10
            ElseIf (index = 1) Then
                perPageNumber = 25
            Else
                perPageNumber = 50
            End If

            Return perPageNumber
        End Get

        Set(value As Integer)
            Dim index As Integer = -1

            If (value = 10) Then
                index = 0
            ElseIf (value = 50) Then
                index = 2
            ElseIf (value = 25) Then
                index = 1
            End If

            If (index > -1) Then cbPerPage.SelectedIndex = index
        End Set

    End Property

    Private ReadOnly Property CurrentOffersControl As MessagesControl
        Get
            Return msgsL
        End Get
    End Property


    Public Property MessagesSorting As MessagesSortEnum

        Get
            Dim sorting As MessagesSortEnum = MessagesSortEnum.Recent

            Dim index As Integer = cbSort.SelectedIndex

            If (index = 0) Then

            ElseIf (index = 1) Then
                sorting = MessagesSortEnum.Oldest
            End If

            Return sorting
        End Get

        Set(value As MessagesSortEnum)
            Dim index As Integer = 0

            If (value = MessagesSortEnum.Oldest) Then
                index = 1
            Else
                index = 0
            End If
            cbSort.SelectedIndex = index
        End Set

    End Property

    Protected Property CurrentLoginName As String
        Get
            Return ViewState("CurrentLoginName")
        End Get
        Set(value As String)
            ViewState("CurrentLoginName") = value
            ViewState("CurrentLoginName_enc") = Nothing
        End Set
    End Property

    Protected ReadOnly Property CurrentLoginName_enc As String
        Get
            If (ViewState("CurrentLoginName_enc") Is Nothing) Then ViewState("CurrentLoginName_enc") = HttpUtility.UrlEncode(CurrentLoginName)
            Return ViewState("CurrentLoginName_enc")
        End Get
    End Property


#End Region


    Protected Overrides Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        SetMasterPage(CurrentPageData)
        Try
            If (Me.SessionVariables.MemberData.Status = ProfileStatusEnum.LIMITED) Then
                Response.Redirect(ResolveUrl("~/Members/Default.aspx"))
            End If

            MyBase.Page_PreInit()
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        AddHandler Me.CurrentOffersControl.Repeater.ItemCommand, AddressOf Me.lvMsgs_ItemCommand

        Try
            If (Not Page.IsPostBack) Then
                Try
                    Me.ItemsPerPage = clsProfilesPrivacySettings.GET_ItemsPerPage(Me.MasterProfileId)
                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "GET_ItemsPerPage")
                End Try

                'itemsPerPage)
                Dim _itemsPerPage As Integer
                Dim req As String = Request.QueryString("itemsPerPage")
                If (Not String.IsNullOrEmpty(req)) Then
                    Integer.TryParse(req, _itemsPerPage)
                End If

                Me.ItemsPerPage = _itemsPerPage

                Dim currentPage As Integer
                req = Request.QueryString("seo" & Me.Pager.ClientID)
                If (Not String.IsNullOrEmpty(req)) Then
                    Try
                        req = req.Replace("page", "")
                        Integer.TryParse(req, currentPage)
                        Me.Pager.ItemCount = Me.ItemsPerPage * currentPage
                        Me.Pager.PageIndex = currentPage - 1
                    Catch ex As Exception
                    End Try
                End If

            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        lblActionError.Text = ""
        ''pnlMessageErr.Visible = False
        ''pnlConversationReplyErr.Visible = False
        ''pnlSendMessageErr.Visible = False
        ''divDatingAmount.Visible = False

        'If (Not Page.IsPostBack) Then
        '    lnkInactiveProfiles.Visible = False
        'End If


        Try


            'Try
            '    Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            '    AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
            'Catch ex As Exception
            '    WebErrorMessageBox(Me, ex, "Page_Load")
            'End Try

            Dim viewLoaded As Boolean = False

            Dim unlockMessage As Boolean = MyBase.UnlimitedMsgID > 0 AndAlso Not String.IsNullOrEmpty(Request.QueryString("vw"))
            Dim viewMessage As Boolean = Not MyBase.UnlimitedMsgID > 0 AndAlso Not String.IsNullOrEmpty(Request.QueryString("vw"))
            Dim sendMessageTo As Boolean = Not String.IsNullOrEmpty(Request.QueryString("p"))
            '    Dim tabChanged As Boolean = Not String.IsNullOrEmpty(Request.QueryString("t"))


            If (Not Page.IsPostBack) Then
                Me.BackUrl = Request.Params("HTTP_REFERER")

                If (Not String.IsNullOrEmpty(Request.Params("offerid"))) Then Integer.TryParse(Request.Params("offerid"), Me.OfferId)

                If (viewMessage) Then
                    '' load conversation for requested and current profiles
                    LoadLAG()
                    'LoadViews()

                    Dim otherLoginName = Request.QueryString("vw")
                    Using cmdb As CMSDBDataContext = Me.GetCMSDBDataContext


                        Dim memberReceivingMessage As EUS_Profile = DataHelpers.GetEUS_ProfileMasterByLoginName(cmdb, otherLoginName, ProfileStatusEnum.Approved, ProfileStatusEnum.LIMITED)
                        If (memberReceivingMessage IsNot Nothing) Then


                            If (clsUserDoes.HasCommunication(memberReceivingMessage.ProfileID, Me.MasterProfileId)) Then
                                Dim message As EUS_Message = clsUserDoes.GetLastMessageForProfiles(Me.MasterProfileId, memberReceivingMessage.ProfileID)
                                Me.UserIdInView = memberReceivingMessage.ProfileID
                                Me.MessageIdInView = message.EUS_MessageID
                                Me.CurrentLoginName = memberReceivingMessage.LoginName
                                ExecCmd("VIEWMSG", Me.MessageIdInView)
                                viewLoaded = True
                            End If
                        End If
                    End Using
                End If
                If (Not viewLoaded AndAlso sendMessageTo) Then

                    ' '' send message to profile
                    'LoadLAG()
                    ''LoadViews()
                    'LoadNewMessageView()
                    Response.Redirect(ResolveUrl("~/Members/Conversation.aspx?p=" & HttpUtility.UrlEncode(Server.UrlDecode(Request.QueryString("p")))), False)
                    viewLoaded = True

                End If

                If (Not viewLoaded AndAlso unlockMessage) Then
                    'PerformMessageUnlock()
                    Response.Redirect(ResolveUrl("~/Members/Conversation.aspx?unmsg=" & HttpUtility.UrlEncode(Request.QueryString("unmsg"))), False)
                    viewLoaded = True
                End If

                If (Not viewLoaded) Then
                    LoadLAG()
                    LoadViews()
                    BindMessagesList()
                End If

            End If

            'If (Not viewLoaded AndAlso unlockMessage) Then
            '    PerformMessageUnlock()
            'End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub



    Sub LoadView()

        'If (MessagesViewEnum.SENDMESSAGE = MessagesView) Then
        '    liSent.Attributes.Remove("class")
        '    liInbox.Attributes.Remove("class")
        '    liNew.Attributes.Remove("class")

        '    mvMessages.SetActiveView(vwConversation)

        'Else
        If (MessagesViewEnum.SEND_NEW_MESSAGE = MessagesView) Then
            liSent.Visible = False
            liInbox.Visible = False
            liNew.Visible = False
            liTrash.Visible = False

        Else
            liSent.Visible = True
            liInbox.Visible = True
            liNew.Visible = True
            liTrash.Visible = True

            If (MessagesViewEnum.SENT = MessagesView) Then
                liSent.Attributes.Add("class", "down")
                liInbox.Attributes.Remove("class")
                liNew.Attributes.Remove("class")
                liTrash.Attributes.Remove("class")
                mvMessages.SetActiveView(vwAllMessage)

            ElseIf (MessagesViewEnum.INBOX = MessagesView) Then
                liInbox.Attributes.Add("class", "down")
                liSent.Attributes.Remove("class")
                liNew.Attributes.Remove("class")
                liTrash.Attributes.Remove("class")
                mvMessages.SetActiveView(vwAllMessage)

            ElseIf (MessagesViewEnum.TRASH = MessagesView) Then
                liTrash.Attributes.Add("class", "down")
                liSent.Attributes.Remove("class")
                liNew.Attributes.Remove("class")
                liInbox.Attributes.Remove("class")
                mvMessages.SetActiveView(vwAllMessage)

            Else
                ' new messages
                liNew.Attributes.Add("class", "down")
                liSent.Attributes.Remove("class")
                liInbox.Attributes.Remove("class")
                liTrash.Attributes.Remove("class")
                'lnkInactiveProfiles.Visible = True
                mvMessages.SetActiveView(vwAllMessage)

            End If
        End If

    End Sub


    Private Sub LoadViews()

        Try

            If (Not Page.IsPostBack) Then

                If (Request.QueryString("t") IsNot Nothing) Then
                    If (Request.QueryString("t").ToUpper() = "NEW") Then
                        MessagesView = MessagesViewEnum.NEWMESSAGES

                    ElseIf (Request.QueryString("t").ToUpper() = "INBOX") Then
                        MessagesView = MessagesViewEnum.INBOX

                    ElseIf (Request.QueryString("t").ToUpper() = "SENT") Then
                        MessagesView = MessagesViewEnum.SENT

                    ElseIf (Request.QueryString("t").ToUpper() = "TRASH") Then
                        MessagesView = MessagesViewEnum.TRASH

                    Else
                        MessagesView = MessagesViewEnum.NEWMESSAGES

                    End If
                Else
                    MessagesView = MessagesViewEnum.NEWMESSAGES

                End If
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender

        Try
            Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)


            If (Me.Pager.PageIndex > 0) Then
                Me.Header.Title = Me.Header.Title & " -- Messages page " & (Me.Pager.PageIndex + 1)
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub


    Protected Sub ddlSort_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbSort.SelectedIndexChanged
        Try
            Me.Pager.PageIndex = -1
            BindMessagesList()
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Protected Sub ddlPerPage_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbPerPage.SelectedIndexChanged
        'Try
        '    Me.Pager.PageIndex = -1
        '    BindMessagesList()
        'Catch ex As Exception
        '    WebErrorMessageBox(Me, ex, "")
        'End Try
        Try
            clsProfilesPrivacySettings.Update_ItemsPerPage(Me.MasterProfileId, Me.ItemsPerPage)
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorSendEmail(ex, "")
        End Try
        Try
            Me.Pager.PageIndex = -1
            BindMessagesList()
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(ex, "")
        End Try
    End Sub


    Private Sub BindMessagesList()
        Try

            LoadView()

            divFilter.Visible = False
            msgsL.ShowEmptyListText = True

            If (showActive) Then
                lnkInactiveProfiles.Text = "Show Inactive Profiles"
                lnkInactiveProfiles.NavigateUrl = ResolveUrl("~/Members/Messages.aspx?t=[VIEW]&inactive=1")
            ElseIf (Not showActive) Then
                lnkInactiveProfiles.Text = "Show Active Profiles"
                lnkInactiveProfiles.NavigateUrl = ResolveUrl("~/Members/Messages.aspx?t=[VIEW]&inactive=0")
            End If
            lnkInactiveProfiles.NavigateUrl = lnkInactiveProfiles.NavigateUrl.Replace("[VIEW]", MessagesView.ToString())

            '     Dim sql As String = ""


            If (Me.GetCurrentProfile(True) IsNot Nothing) Then

                Dim prms As New clsMessagesHelperParameters()
                Try

                    Dim countRows As Integer
                    Dim NumberOfRecordsToReturn As Integer = Me.GetRecordsToRetrieve()

                    prms.CurrentProfileId = Me.MasterProfileId
                    prms.MessagesView = MessagesView
                    prms.MessagesSort = Me.MessagesSorting
                    prms.cmdFilter = "" 'cmdFilter
                    prms.showActive = showActive
                    'parms.NumberOfRecordsToReturn = NumberOfRecordsToReturn
                    prms.performCount = True
                    prms.rowNumberMin = NumberOfRecordsToReturn - Me.ItemsPerPage
                    prms.rowNumberMax = NumberOfRecordsToReturn


                    Dim ds As DataSet = clsMessagesHelper.GetMessagesDataTable(prms)
                    If (prms.performCount) Then
                        countRows = ds.Tables(0).Rows(0)(0)
                        If (countRows > 0) Then
                            Dim dt As DataTable = ds.Tables(1)
                            FillListControl(dt, msgsL)
                            msgsL.ShowEmptyListText = False
                            divFilter.Visible = True
                        End If
                    Else
                        countRows = ds.Tables(0).Rows.Count
                        If (countRows > 0) Then
                            Dim dt As DataTable = ds.Tables(0)
                            FillListControl(dt, msgsL)
                            msgsL.ShowEmptyListText = False
                            divFilter.Visible = True
                        End If
                    End If

                    msgsL.DataBind()
                    SetPager(countRows)

                Catch ex As Exception
                    Dim extraMessage As String = ""
                    Try
                        extraMessage = AppUtils.ConvertObjectToXML(prms, prms.GetType())
                        If (Not String.IsNullOrEmpty(extraMessage)) Then
                            extraMessage = extraMessage.Replace("<?xml version=""1.0"" encoding=""utf-16""?>", "")
                            extraMessage = "SERIALIAZED object follows " & vbCrLf & (New String("-"c, 30)) & extraMessage & vbCrLf & (New String("-"c, 30))
                        End If
                    Catch
                    End Try

                    WebErrorMessageBox(Me, ex, extraMessage)

                    SetPager(0)
                    msgsL.UsersList = Nothing
                    msgsL.DataBind()
                End Try
            End If

            If (Me.Pager.ItemCount = 0 AndAlso Me.GetPhotosCount() = 0) Then

                Try

                    msgsL.ShowNoPhotoText = True
                    msgsL.ShowEmptyListText = False
                    msgsL.DataBind()

                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "")
                Finally
                    SetPager(0)
                End Try

            End If


        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub



    Private Sub FillListControl(dt As DataTable, offersControl As Dating.Server.Site.Web.MessagesControl)

        Dim drDefaultPhoto As EUS_CustomerPhotosRow = DataHelpers.GetProfilesDefaultPhoto(Me.MasterProfileId)
        Dim uliCurrentMember As New clsMessageUserListItem()
        uliCurrentMember.ProfileID = Me.MasterProfileId ' 
        uliCurrentMember.MirrorProfileID = Me.MirrorProfileId
        uliCurrentMember.LoginName = Me.GetCurrentProfile().LoginName
        uliCurrentMember.Genderid = Me.GetCurrentProfile().GenderId
        uliCurrentMember.ProfileViewUrl = ResolveUrl("~/Members/Profile.aspx?p=" & HttpUtility.UrlEncode(Me.GetCurrentProfile().LoginName))

        If (drDefaultPhoto IsNot Nothing) Then
            uliCurrentMember.ImageFileName = drDefaultPhoto.FileName
            uliCurrentMember.ImageUploadDate = drDefaultPhoto.DateTimeToUploading.ToLocalTime().ToString("d MMM, yyyy H:mm")
        End If

        uliCurrentMember.ImageUrl = ProfileHelper.GetProfileImageURL(Me.MasterProfileId, uliCurrentMember.ImageFileName, Me.SessionVariables.MemberData.GenderId, True, Me.IsHTTPS, PhotoSize.D150)
        uliCurrentMember.ImageThumbUrl = uliCurrentMember.ImageUrl


        Dim mins As Integer = clsConfigValues.Get__members_online_minutes()
        Dim LastActivityUTCDate As DateTime = Date.UtcNow.AddMinutes(-mins)
        Dim hours As Integer = clsConfigValues.Get__members_online_recently_hours()
        Dim LastActivityRecentlyUTCDate As DateTime = Date.UtcNow.AddHours(-hours)


        Dim rowsCount As Integer
        For rowsCount = 0 To dt.Rows.Count - 1

            Try
                Dim dr As DataRow = dt.Rows(rowsCount)

                Dim uli As New clsMessageUserListItem()
                uli.ProfileID = uliCurrentMember.ProfileID
                uli.MirrorProfileID = uliCurrentMember.MirrorProfileID
                uli.LoginName = uliCurrentMember.LoginName
                uli.Genderid = uliCurrentMember.Genderid
                uli.ProfileViewUrl = uliCurrentMember.ProfileViewUrl
                uli.ImageFileName = uliCurrentMember.ImageFileName
                uli.ImageUrl = uliCurrentMember.ImageUrl
                uli.ImageThumbUrl = uliCurrentMember.ImageThumbUrl
                uli.ImageUploadDate = uliCurrentMember.ImageUploadDate

                uli.OtherMemberLoginName = dr("LoginName")
                uli.OtherMemberProfileID = dr("ProfileID")
                uli.OtherMemberMirrorProfileID = dr("MirrorProfileID")
                uli.OtherMemberCity = dr("City")
                uli.OtherMemberRegion = IIf(Not dr.IsNull("Region"), dr("Region"), "")
                uli.OtherMemberCountry = IIf(Not dr.IsNull("Country"), dr("Country"), "")
                uli.OtherMemberGenderid = dr("Genderid")

                If (Not dr.IsNull("Birthday")) Then
                    uli.OtherMemberAge = ProfileHelper.GetCurrentAge(dr("Birthday"))
                Else
                    uli.OtherMemberAge = 20
                End If

                If (Not dr.IsNull("FileName")) Then uli.OtherMemberImageFileName = dr("FileName")
                If (dt.Columns.Contains("HasPhoto")) Then
                    If (Not dr.IsNull("HasPhoto") AndAlso (dr("HasPhoto").ToString() = "1" OrElse dr("HasPhoto").ToString() = "True")) Then
                        ' user has photo
                        If (uli.OtherMemberImageFileName IsNot Nothing) Then
                            'has public photos
                            uli.OtherMemberImageUrl = ProfileHelper.GetProfileImageURL(uli.OtherMemberProfileID, uli.OtherMemberImageFileName, uli.OtherMemberGenderid, True, Me.IsHTTPS, PhotoSize.Thumb)

                        Else
                            'has private photos
                            uli.OtherMemberImageUrl = ProfileHelper.GetPrivateImageURL(uli.OtherMemberGenderid)

                        End If
                    Else
                        ' has no photo
                        uli.OtherMemberImageUrl = ProfileHelper.GetDefaultImageURL(uli.OtherMemberGenderid)

                    End If
                    If IsHTTPS AndAlso uli.OtherMemberImageUrl.StartsWith("http:") Then uli.OtherMemberImageUrl = "https" & uli.OtherMemberImageUrl.Remove(0, "http".Length)
                Else
                    uli.OtherMemberImageUrl = ProfileHelper.GetProfileImageURL(uli.OtherMemberProfileID, uli.OtherMemberImageFileName, uli.OtherMemberGenderid, True, Me.IsHTTPS, PhotoSize.Thumb)
                End If
                uli.OtherMemberImageThumbUrl = uli.OtherMemberImageUrl

                uli.OtherMemberProfileViewUrl = ResolveUrl("~/Members/Profile.aspx?p=" & HttpUtility.UrlEncode(dr("LoginName")))

                'If (dt.Columns.Contains("IsOnlineNow")) Then
                '    uli.OtherMemberIsOnline = IIf(Not dr.IsNull("IsOnlineNow"), dr("IsOnlineNow"), False)
                'Else
                '    uli.OtherMemberIsOnline = IIf(Not dr.IsNull("IsOnline"), dr("IsOnline"), False)
                '    If (uli.OtherMemberIsOnline) Then
                '        Dim __LastActivityDateTime As DateTime? = IIf(Not dr.IsNull("LastActivityDateTime"), dr("LastActivityDateTime"), Nothing)
                '        If (__LastActivityDateTime.HasValue) Then
                '            uli.OtherMemberIsOnline = __LastActivityDateTime >= LastActivityUTCDate
                '        Else
                '            uli.OtherMemberIsOnline = False
                '        End If
                '    End If
                'End If
                uli.OtherMemberIsOnline = IIf(Not dr.IsNull("IsOnline"), dr("IsOnline"), False)
                Dim __LastActivityDateTime As DateTime? = IIf(Not dr.IsNull("LastActivityDateTime"), dr("LastActivityDateTime"), Nothing)

                If (dt.Columns.Contains("IsOnlineNow")) Then
                    uli.OtherMemberIsOnline = IIf(Not dr.IsNull("IsOnlineNow"), dr("IsOnlineNow"), False)
                Else
                    If (uli.OtherMemberIsOnline) Then
                        If (__LastActivityDateTime.HasValue) Then
                            uli.OtherMemberIsOnline = __LastActivityDateTime >= LastActivityUTCDate
                        Else
                            uli.OtherMemberIsOnline = False
                        End If
                    End If
                End If


                If (dt.Columns.Contains("IsOnlineRecently")) Then
                    uli.OtherMemberIsOnlineRecently = IIf(Not dr.IsNull("IsOnlineRecently"), dr("IsOnlineRecently"), False)
                Else
                    If (uli.OtherMemberIsOnline) Then
                        If (__LastActivityDateTime.HasValue) Then
                            uli.OtherMemberIsOnlineRecently = __LastActivityDateTime >= LastActivityRecentlyUTCDate
                        Else
                            uli.OtherMemberIsOnlineRecently = False
                        End If
                    End If
                End If

                uli.EUS_MessageID = dr("EUS_MessageID")

                'uli.Distance = dr("distance")
                'If (uli.Distance < gCarDistance) Then
                '    uli.DistanceCss = "distance_car"
                'Else
                '    uli.DistanceCss = "distance_plane"
                'End If
                'uli.FullAddress = SetLocationText(dr)

                Dim sentText As String = Me.CurrentPageData.GetCustomString("SentDateText")
                uli.SentDate = sentText.Replace("###DATE###", AppUtils.GetDateTimeString(dr("MAXDateTimeToCreate"), "", "dd/MM/yyyy H:mm"))
                uli.MAXDateTimeToCreateUtc = dr("MAXDateTimeToCreate")

                If (uli.OtherMemberProfileID = Me.MasterProfileId) Then
                    uli.Read = "read"
                    uli.ReadText = Me.CurrentPageData.GetCustomString("ReadText")
                    uli.ReadButtonClass = "read button"
                Else
                    If (dr("messageData_StatusID").ToString() = "1") Then
                        uli.Read = "read"
                        uli.ReadText = Me.CurrentPageData.GetCustomString("ReadText")
                        uli.ReadButtonClass = "read button"
                    Else
                        uli.Read = "unread"
                        uli.ReadText = Me.CurrentPageData.GetCustomString("UnreadText")
                        uli.ReadButtonClass = "unread button"
                    End If
                End If

                If (Not dr.IsNull("OfferAmount") AndAlso dr("OfferAmount") > 0) Then
                    uli.OfferAmount = dr("OfferAmount")
                    uli.OfferAmountText = " &euro;" & dr("OfferAmount") & " " & Me.CurrentPageData.GetCustomString("OfferAmountOfferWord")
                End If

                uli.btnUnlockVisible = False
                If (showActive) Then uli.btnOpenVisible = True

                uli.UnlockMessageUrlOnce = ResolveUrl("~/Members/Conversation.aspx?read=once&vw=" & HttpUtility.UrlEncode(dr("LoginName")) & "&unmsg=" & dr("eus_messageid"))
                uli.UnlockMessageOnceText = Me.CurrentPageData.GetCustomString("UnlockMessageOnceText")
                uli.UnlockMessageManyText = Me.CurrentPageData.GetCustomString("UnlockMessageManyText")

                If (Not dr.IsNull("ItemsCountRead") AndAlso Not dr.IsNull("ItemsCount")) Then
                    uli.ItemsCountRead = dr("ItemsCountRead")
                    uli.ItemsCount = dr("ItemsCount")
                    If (uli.ItemsCountRead < uli.ItemsCount) Then
                        Dim unreadCount As Integer = uli.ItemsCount - uli.ItemsCountRead
                        Dim credits As Integer = unreadCount * ProfileHelper.Config_UNLOCK_MESSAGE_READ_CREDITS

                        If (uli.UnlockMessageOnceText.IndexOf("###UNLOCK_MESSAGE_READ_CRD###") > -1) Then
                            uli.UnlockMessageOnceText = uli.UnlockMessageOnceText.Replace("###UNLOCK_MESSAGE_READ_CRD###", credits)
                        End If

                        If (uli.UnlockMessageManyText.IndexOf("###UNLOCK_CONVERSATIONCRD###") > -1) Then
                            uli.UnlockMessageManyText = uli.UnlockMessageManyText.Replace("###UNLOCK_CONVERSATIONCRD###", credits)
                        End If
                    End If

                End If

                uli.UnlockMessageOnceText = ReplaceCommonTookens(uli.UnlockMessageOnceText, uli.OtherMemberLoginName)
                uli.UnlockMessageManyText = ReplaceCommonTookens(uli.UnlockMessageManyText, uli.OtherMemberLoginName)

                uli.OnMoreInfoClickFunc = OnMoreInfoClickFunc(
                    "OnMoreInfoClick('[contentUrl]',this,'[headerText]');return false;",
                    ResolveUrl("~/Members/InfoWin.aspx?info=messagescommands&ln=" & uli.OtherMemberLoginName),
                    CurrentPageData.GetCustomString("MessagesCommands_HeaderText"))

                uli.AllowTooltipPopupMessages = False
                uli.ShowWarningProfileDeleted = (dr("Status") <> 4)
                uli.WarningProfileDeletedText = CurrentPageData.GetCustomString("WarningProfileDeletedText")
                If (dr("Status") = ProfileStatusEnum.LIMITED) Then
                    uli.WarningProfileDeletedText = CurrentPageData.GetCustomString("WarningProfileLimitedText")
                    uli.IsLimited = True
                End If

                uli.MessagesCountText = CurrentPageData.GetCustomString("MessagesCountText")

                If (MessagesViewEnum.NEWMESSAGES = MessagesView) Then
                    BindMessagesList_NEW(dr, uli)

                ElseIf (MessagesViewEnum.INBOX = MessagesView) Then
                    BindMessagesList_INBOX(dr, uli)

                ElseIf (MessagesViewEnum.SENT = MessagesView) Then
                    BindMessagesList_SENT(dr, uli)

                ElseIf (MessagesViewEnum.TRASH = MessagesView) Then
                    BindMessagesList_TRASH(dr, uli)

                End If

                'sent Mar 27, 2012
                uli.MessagesView = Me.MessagesView
                msgsL.UsersList.Add(uli)

            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try

        Next

    End Sub




    'Private Sub BindMessagesList(Optional cmdFilter As String = Nothing)
    '    Try
    '        LoadView()

    '        Dim showActive As Boolean? = True
    '        If (Not String.IsNullOrEmpty(Request("inactive"))) Then
    '            If (Request("inactive") = "1") Then showActive = False
    '        End If

    '        If (showActive) Then
    '            lnkInactiveProfiles.Text = "Show Inactive Profiles"
    '            lnkInactiveProfiles.NavigateUrl = ResolveUrl("~/Members/Messages.aspx?t=[VIEW]&inactive=1")
    '        ElseIf (Not showActive) Then
    '            lnkInactiveProfiles.Text = "Show Active Profiles"
    '            lnkInactiveProfiles.NavigateUrl = ResolveUrl("~/Members/Messages.aspx?t=[VIEW]&inactive=0")
    '        End If
    '        lnkInactiveProfiles.NavigateUrl = lnkInactiveProfiles.NavigateUrl.Replace("[VIEW]", MessagesView.ToString())

    '        Dim sql As String = ""
    '        Dim countRows As Integer
    '        Dim NumberOfRecordsToReturn As Integer = Me.GetRecordsToRetrieve()

    '        Dim parms As New clsMessagesHelperParameters()
    '        parms.CurrentProfileId = Me.MasterProfileId
    '        parms.MessagesView = MessagesView
    '        parms.cmdFilter = cmdFilter
    '        parms.showActive = showActive
    '        'parms.NumberOfRecordsToReturn = NumberOfRecordsToReturn
    '        parms.performCount = True
    '        parms.rowNumberMin = NumberOfRecordsToReturn - Me.ItemsPerPage
    '        parms.rowNumberMax = NumberOfRecordsToReturn


    '        Dim ds As DataSet = clsMessagesHelper.GetMessagesDataTable(parms)
    '        Dim dt As New DataTable()

    '        If (parms.performCount) Then
    '            countRows = ds.Tables(0).Rows(0)(0)
    '            If (countRows > 0) Then
    '                dt = ds.Tables(1)
    '            End If
    '        Else
    '            countRows = ds.Tables(0).Rows.Count
    '            If (countRows > 0) Then
    '                dt = ds.Tables(0)
    '            End If
    '        End If

    '        dt.Columns.Add("OtherMemberImageUrl", GetType(String))
    '        dt.Columns.Add("OtherMemberProfileViewUrl", GetType(String))
    '        dt.Columns.Add("Read", GetType(String))
    '        dt.Columns.Add("ReadText", GetType(String))
    '        dt.Columns.Add("SentDate", GetType(String))
    '        dt.Columns.Add("FullAddress", GetType(String))
    '        dt.Columns.Add("ReadButtonClass", GetType(String))
    '        dt.Columns.Add("DeleteText", GetType(String))
    '        dt.Columns.Add("ViewMessageText", GetType(String))
    '        dt.Columns.Add("OfferAmountText", GetType(String))
    '        dt.Columns.Add("UnlockUrl", GetType(String))
    '        dt.Columns.Add("btnOpenVisible", GetType(Boolean))
    '        dt.Columns.Add("btnUnlockVisible", GetType(Boolean))
    '        dt.Columns.Add("UnlockCreditsAmountText", GetType(String))
    '        dt.Columns.Add("UnlockMessageUrlOnce", GetType(String))
    '        dt.Columns.Add("UnlockMessageUrlMany", GetType(String))
    '        dt.Columns.Add("UnlockMessageOnceText", GetType(String))
    '        dt.Columns.Add("UnlockMessageManyText", GetType(String))
    '        dt.Columns.Add("AllowTooltipPopupMessages", GetType(Boolean))
    '        dt.Columns.Add("OnMoreInfoClickFunc", GetType(String))
    '        dt.Columns.Add("WarningProfileDeletedText", GetType(String))
    '        dt.Columns.Add("ShowWarningProfileDeleted", GetType(Boolean))
    '        dt.Columns.Add("MessagesCountText", GetType(String))
    '        '

    '        For Each dr In dt.Rows

    '            If (Not dr.IsNull("FileName")) Then
    '                dr("OtherMemberImageUrl") = ProfileHelper.GetProfileImageURL(dr("ProfileID"), dr("FileName"), dr("GenderId"), True, Me.IsHTTPS)
    '            Else
    '                dr("OtherMemberImageUrl") = ProfileHelper.GetProfileImageURL(dr("ProfileID"), Nothing, dr("GenderId"), True, Me.IsHTTPS)
    '            End If


    '            dr("FullAddress") = SetLocationText(dr)
    '            dr("OtherMemberProfileViewUrl") = ResolveUrl("~/Members/Profile.aspx?p=" & HttpUtility.UrlEncode(dr("LoginName").ToString()))

    '            Dim sentText As String = Me.CurrentPageData.GetCustomString("SentDateText")
    '            dr("SentDate") = sentText.Replace("###DATE###", AppUtils.GetDateTimeString(dr("MAXDateTimeToCreate")))
    '            'dr("SentDate") = sentText.Replace("###DATE###", CType(dr("MAXDateTimeToCreate"), DateTime).ToLocalTime().ToString("d MMM, yyyy H:mm"))


    '            If (dr("ProfileID") = Me.MasterProfileId) Then
    '                dr("Read") = "read"
    '                dr("ReadText") = Me.CurrentPageData.GetCustomString("ReadText") ''"Read"
    '                dr("ReadButtonClass") = "read button"
    '            Else
    '                'Dim dtMsg As DataTable = DataHelpers.GetDataTable("select top(1) * from EUS_Messages where ISNULL(EUS_Messages.IsHidden,0)=0 and eus_messageid=" & dr("eus_messageid"))
    '                'Dim drMsg As DataRow = dtMsg.Rows(0)

    '                If (dr("messageData_StatusID").ToString() = "1") Then
    '                    dr("Read") = "read"
    '                    dr("ReadText") = Me.CurrentPageData.GetCustomString("ReadText") ' "Read"
    '                    dr("ReadButtonClass") = "read button"
    '                Else
    '                    dr("Read") = "unread"
    '                    dr("ReadText") = Me.CurrentPageData.GetCustomString("UnreadText") '"Read"
    '                    dr("ReadButtonClass") = "unread button"
    '                End If
    '            End If

    '            If (Not dr.isnull("OfferAmount") AndAlso dr("OfferAmount") > 0) Then
    '                dr("OfferAmountText") = " &euro;" & dr("OfferAmount") & " " & Me.CurrentPageData.GetCustomString("OfferAmountOfferWord")
    '            End If

    '            dr("btnUnlockVisible") = False
    '            dr("btnOpenVisible") = True

    '            'If (ModGlobals.AllowUnlimited) Then
    '            dr("UnlockMessageUrlOnce") = ResolveUrl("~/Members/Conversation.aspx?read=once&vw=" & HttpUtility.UrlEncode(dr("LoginName")) & "&unmsg=" & dr("eus_messageid"))
    '            '    dr("UnlockMessageUrlMany") = ResolveUrl("~/Members/Conversation.aspx?read=unl&vw=" & HttpUtility.UrlEncode(dr("LoginName")) & "&unmsg=" & dr("eus_messageid"))
    '            'Else
    '            '    dr("UnlockMessageUrlOnce") = ResolveUrl("~/Members/Conversation.aspx?read=once&vw=" & HttpUtility.UrlEncode(dr("LoginName")))
    '            'End If

    '            dr("UnlockMessageOnceText") = Me.CurrentPageData.GetCustomString("UnlockMessageOnceText")
    '            dr("UnlockMessageManyText") = Me.CurrentPageData.GetCustomString("UnlockMessageManyText")

    '            If (Not dr.IsNull("ItemsCountRead") AndAlso Not dr.IsNull("ItemsCount")) Then

    '                If (dr("ItemsCountRead") < dr("ItemsCount")) Then
    '                    Dim unreadCount As Integer = dr("ItemsCount") - dr("ItemsCountRead")
    '                    Dim credits As Integer = unreadCount * ProfileHelper.Config_UNLOCK_MESSAGE_READ_CREDITS

    '                    If (dr("UnlockMessageOnceText").IndexOf("###UNLOCK_MESSAGE_READ_CRD###") > -1) Then
    '                        dr("UnlockMessageOnceText") = dr("UnlockMessageOnceText").Replace("###UNLOCK_MESSAGE_READ_CRD###", credits)
    '                    End If

    '                    If (dr("UnlockMessageManyText").IndexOf("###UNLOCK_CONVERSATIONCRD###") > -1) Then
    '                        dr("UnlockMessageManyText") = dr("UnlockMessageManyText").Replace("###UNLOCK_CONVERSATIONCRD###", credits)
    '                    End If
    '                End If

    '            End If

    '            dr("UnlockMessageOnceText") = ReplaceCommonTookens(dr("UnlockMessageOnceText"), dr("LoginName"))
    '            dr("UnlockMessageManyText") = ReplaceCommonTookens(dr("UnlockMessageManyText"), dr("LoginName"))

    '            dr("OnMoreInfoClickFunc") = OnMoreInfoClickFunc(
    '                "OnMoreInfoClick('[contentUrl]',this,'[headerText]');return false;",
    '                ResolveUrl("~/Members/InfoWin.aspx?info=messagescommands&ln=" & dr("LoginName")),
    '                CurrentPageData.GetCustomString("MessagesCommands_HeaderText"))

    '            dr("AllowTooltipPopupMessages") = False
    '            dr("ShowWarningProfileDeleted") = (dr("Status") <> 4)
    '            dr("WarningProfileDeletedText") = CurrentPageData.GetCustomString("WarningProfileDeletedText")
    '            If (dr("Status") = ProfileStatusEnum.LIMITED) Then
    '                dr("WarningProfileDeletedText") = CurrentPageData.GetCustomString("WarningProfileLimitedText")
    '            End If

    '            dr("MessagesCountText") = CurrentPageData.GetCustomString("MessagesCountText")

    '            If (MessagesViewEnum.NEWMESSAGES = MessagesView) Then
    '                BindMessagesList_NEW(dr)

    '            ElseIf (MessagesViewEnum.INBOX = MessagesView) Then
    '                BindMessagesList_INBOX(dr)

    '            ElseIf (MessagesViewEnum.SENT = MessagesView) Then
    '                BindMessagesList_SENT(dr)

    '            ElseIf (MessagesViewEnum.TRASH = MessagesView) Then
    '                BindMessagesList_TRASH(dr)

    '            End If

    '            'sent Mar 27, 2012
    '        Next


    '        lvMsgs.DataSource = dt
    '        lvMsgs.DataBind()
    '        SetPager(countRows)

    '    Catch ex As Exception
    '        WebErrorMessageBox(Me, ex, "LoadGrid")

    '        SetPager(0)
    '        lvMsgs.DataSource = Nothing
    '        lvMsgs.DataBind()
    '    End Try
    'End Sub



    Private Sub BindMessagesList_NEW(ByRef dr As DataRow, ByRef uli As clsMessageUserListItem)
        Try
            If (Me.IsMale) Then

                If (uli.OtherMemberProfileID = 1) Then

                    ' communication with administrator is open
                    uli.ViewMessageText = Me.CurrentPageData.GetCustomString("CommandViewMessageText")

                ElseIf (uli.CommunicationStatus = 0) Then
                    ' check for credits amount required to unlock
                    Dim gotoBilling = False
                    Try
                        gotoBilling = True
                        Dim HasRequiredCredits_To_UnlockConversation As Boolean = HasRequiredCredits(UnlockType.UNLOCK_CONVERSATION)
                        If (HasRequiredCredits_To_UnlockConversation) Then
                            gotoBilling = False
                        End If
                    Catch ex As Exception

                    End Try

                    'credits label text
                    Dim UnlockCreditsAmountText As String = Me.CurrentPageData.GetCustomString("UnlockCreditsAmountText")
                    If (Not String.IsNullOrEmpty(UnlockCreditsAmountText)) Then
                        uli.UnlockCreditsAmountText = UnlockCreditsAmountText.Replace("###CREDITS###", ProfileHelper.Config_UNLOCK_CONVERSATION_CREDITS.ToString())
                    End If


                    ' unblock button text
                    uli.ViewMessageText = Me.CurrentPageData.GetCustomString("CommandUnlockMessageText")


                    ' hide message's subject
                    uli.Subject = "You received a message from " & dr("LoginName")

                    ' unblock button navigate
                    If (gotoBilling) Then
                        Dim billingurl As String = ResolveUrl("~/Members/SelectProduct.aspx?returnurl=" & HttpUtility.UrlEncode(Request.Url.ToString()))
                        uli.UnlockUrl = billingurl
                    Else 'If (AllowUnlimited) Then
                        Dim unlockurl As String = ResolveUrl("~/Members/Messages.aspx?unmsg=" & dr("EUS_MessageID"))
                        uli.UnlockUrl = unlockurl
                    End If

                    uli.btnUnlockVisible = True
                    uli.AllowTooltipPopupMessages = True

                ElseIf (uli.CommunicationStatus > 0) Then
                    uli.ViewMessageText = Me.CurrentPageData.GetCustomString("CommandViewMessageText")
                End If

            ElseIf (Me.IsFemale) Then

                uli.btnUnlockVisible = False
                uli.ViewMessageText = Me.CurrentPageData.GetCustomString("CommandViewMessageText")

            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "LoadGrid")
        End Try
    End Sub


    Private Sub BindMessagesList_INBOX(ByRef dr As DataRow, ByRef uli As clsMessageUserListItem)
        Try

            If (Me.IsMale) Then

                uli.btnUnlockVisible = False
                uli.ViewMessageText = Me.CurrentPageData.GetCustomString("CommandViewMessageText")

            ElseIf (Me.IsFemale) Then

                uli.btnUnlockVisible = False
                uli.ViewMessageText = Me.CurrentPageData.GetCustomString("CommandViewMessageText")

            End If


        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "LoadGrid")
        End Try
    End Sub


    Private Sub BindMessagesList_SENT(ByRef dr As DataRow, ByRef uli As clsMessageUserListItem)
        Try

            If (Me.IsMale) Then
                uli.btnUnlockVisible = False
                uli.ViewMessageText = Me.CurrentPageData.GetCustomString("CommandViewMessageText")
            Else
                uli.btnUnlockVisible = False
                uli.ViewMessageText = Me.CurrentPageData.GetCustomString("CommandViewMessageText")
            End If


        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "LoadGrid")
        End Try
    End Sub


    Private Sub BindMessagesList_TRASH(ByRef dr As DataRow, ByRef uli As clsMessageUserListItem)
        Try

            If (Me.IsMale) Then
                uli.btnUnlockVisible = False
                uli.ViewMessageText = Me.CurrentPageData.GetCustomString("CommandViewMessageText")

                Dim _messagesView As MessagesViewEnum = clsMessagesHelper.GetMessagesViewEnum(dr("IsReceived"), dr("statusID"))
                If (_messagesView = MessagesViewEnum.NEWMESSAGES) Then
                    uli.AllowTooltipPopupMessages = False
                End If
            Else
                uli.btnUnlockVisible = False
                uli.ViewMessageText = Me.CurrentPageData.GetCustomString("CommandViewMessageText")
            End If


        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "BindMessagesList_TRASH")
        End Try
    End Sub

    Sub SetPager(itemsCount As Integer)
        Me.Pager.ItemCount = itemsCount
        Me.Pager.ItemsPerPage = Me.ItemsPerPage
        Me.Pager.Visible = (itemsCount > Me.ItemsPerPage)

        If (Me.Pager.PageIndex = -1) AndAlso (itemsCount > 0) Then
            Me.Pager.PageIndex = 0
        End If
    End Sub

    Function GetRecordsToRetrieve() As Integer
        Me.Pager.ItemsPerPage = Me.ItemsPerPage

        If (Me.Pager.PageIndex = -1) AndAlso (Me.Pager.ItemCount > 0) Then
            Me.Pager.PageIndex = 0
        End If

        If (Me.Pager.PageIndex > 0) Then
            Return (Me.Pager.ItemsPerPage * (Me.Pager.PageIndex + 1))
        End If
        Return Me.Pager.ItemsPerPage
    End Function

    Private Shared Function SetLocationText(dr As DataRow) As String
        Dim _result = ""
        Dim _locationStrCity = dr("City").ToString()
        Dim _locationStrRegion = dr("Region").ToString()
        Dim _locationStrCountry = dr("Country").ToString()

        If (_locationStrCity <> "") Then
            _result = _locationStrCity
        End If
        If (_locationStrRegion <> "" AndAlso _locationStrCity <> "") Then
            _result = _result & ", "
        End If
        If (_locationStrRegion <> "") Then
            _result = _result & _locationStrRegion
        End If
        If (_locationStrRegion <> "" AndAlso _locationStrCountry <> "") Then
            _result = _result & ", "
        End If
        If (_locationStrCountry <> "") Then
            _locationStrCountry = ProfileHelper.GetCountryName(_locationStrCountry)
            _result = _result & _locationStrCountry
        End If

        Return _result
    End Function




    '    Private Sub LoadReadMessageView(messageId As Integer)
    '        Try
    '            mvMessages.SetActiveView(vwViewMessage)


    '            Dim sql As String = ""

    '            If (MessagesViewEnum.INBOX = Me.MessagesView) Then
    '                sql = <sql><![CDATA[
    'select 
    '	msg.* ,prof.*, phot.CustomerID, phot.FileName, 
    '	(select Amount
    '    from eus_offers  ofr
    '    where ofr.StatusID=50 and (
    '	(ofr.ToProfileID = prof.ProfileId and ofr.FromProfileID =@currentProfileId) or
    '	(ofr.ToProfileID =@currentProfileId and ofr.FromProfileID = prof.ProfileId)
    '	)) as OfferAmount,	 
    '    (select top(1) UnlockedConversationId
    '        where	(con.ToProfileID = prof.ProfileId and con.FromProfileID =@currentProfileId) or
    '		        (con.ToProfileID =@currentProfileId and con.FromProfileID = prof.ProfileId)
    '    ) as CommunicationStatus	 

    'from EUS_Messages msg
    'inner join EUS_Profiles prof on prof.ProfileId = msg.FromProfileID
    'left join EUS_CustomerPhotos phot on phot.CustomerId=prof.ProfileId and phot.IsDefault= 1 and phot.HasAproved = 1 and ISNULL(phot.IsDeleted,0) = 0
    'where msg.ToProfileID=@currentProfileId
    'and msg.EUS_MessageID = @messageId
    'and ISNULL(msg.IsHidden,0)=0
    ']]></sql>.Value

    '            Else

    '                sql = <sql><![CDATA[
    'select 
    '	msg.* ,prof.*, phot.CustomerID, phot.FileName,  
    '	(select Amount
    '    from eus_offers  ofr
    '    where ofr.StatusID=50 and (
    '	(ofr.ToProfileID = prof.ProfileId and ofr.FromProfileID =@currentProfileId) or
    '	(ofr.ToProfileID =@currentProfileId and ofr.FromProfileID = prof.ProfileId)
    '	)) as OfferAmount,	 
    '    (select top(1) UnlockedConversationId
    '        where	(con.ToProfileID = prof.ProfileId and con.FromProfileID =@currentProfileId) or
    '		        (con.ToProfileID =@currentProfileId and con.FromProfileID = prof.ProfileId)
    '    ) as CommunicationStatus	 

    'from EUS_Messages msg
    'inner join EUS_Profiles prof on prof.ProfileId = msg.ToProfileID
    'left join EUS_CustomerPhotos phot on phot.CustomerId=prof.ProfileId and phot.IsDefault= 1 and phot.HasAproved = 1 and ISNULL(phot.IsDeleted,0) = 0
    'where msg.FromProfileID=@currentProfileId
    'and msg.EUS_MessageID = @messageId
    'and ISNULL(msg.IsHidden,0)=0
    ']]></sql>.Value

    '            End If

    '            sql = sql.Replace("@currentProfileId", Me.MasterProfileId)
    '            sql = sql.Replace("@messageId", messageId)


    '            Dim recipientUserId As Integer
    '            Dim dt As DataTable = DataHelpers.GetDataTable(sql)
    '            dt.Columns.Add("SentDateTime", GetType(String))
    '            dt.Columns.Add("OtherMemberProfileViewUrl", GetType(String))
    '            dt.Columns.Add("OtherMemberImageUrl", GetType(String))
    '            dt.Columns.Add("ConversationWithText", GetType(String))
    '            dt.Columns.Add("OfferAmountText", GetType(String))

    '            ' the result should be one row
    '            For Each dr In dt.Rows

    '                'If (Not dr.IsNull("FileName")) Then
    '                '    dr("OtherMemberImageUrl") = ProfileHelper.GetProfileImageURL(dr("CustomerID"), dr("FileName"), dr("GenderId"), True, Me.IsHTTPS)
    '                'Else
    '                '    dr("OtherMemberImageUrl") = ProfileHelper.GetDefaultImageURL(dr("GenderId"))
    '                'End If
    '                If (Not dr.IsNull("FileName")) Then
    '                    dr("OtherMemberImageUrl") = ProfileHelper.GetProfileImageURL(dr("ProfileID"), dr("FileName"), dr("GenderId"), True, Me.IsHTTPS)
    '                Else
    '                    dr("OtherMemberImageUrl") = ProfileHelper.GetProfileImageURL(dr("ProfileID"), Nothing, dr("GenderId"), True, Me.IsHTTPS)
    '                End If

    '                dr("OtherMemberProfileViewUrl") = ResolveUrl("~/Members/Profile.aspx?p=" & HttpUtility.UrlEncode(dr("LoginName").ToString()))
    '                dr("SentDateTime") = AppUtils.GetDateTimeString(dr("DateTimeToCreate"))
    '                ' CType(dr("DateTimeToCreate"), DateTime).ToString("d MMM, H:mm")

    '                If (Not dr.isnull("OfferAmount")) Then
    '                    Dim offerAmount As Integer = dr("OfferAmount")

    '                    Dim conversationWithText As String = Me.CurrentPageData.GetCustomString("ConversationWithText")
    '                    conversationWithText = conversationWithText.Replace("###MEMBERPROFILELINK###", "<a href=""" & dr("OtherMemberProfileViewUrl") & """>" & dr("LoginName") & "</a>")
    '                    If (offerAmount > 0) Then
    '                        conversationWithText = conversationWithText.Replace("###CURRENCYAMOUNT###", "&euro;" & offerAmount)
    '                    Else
    '                        conversationWithText = conversationWithText.Replace("###CURRENCYAMOUNT###", "")
    '                    End If
    '                    dr("ConversationWithText") = conversationWithText

    '                    If (offerAmount > 0) Then
    '                        dr("OfferAmountText") = "&euro;" & offerAmount & " " & Me.CurrentPageData.GetCustomString("OfferAmountDateWord")
    '                    End If
    '                End If

    '                Me.UserIdInView = dr("ProfileId")
    '                Me.SubjectInView = dr("Subject")
    '                Me.MessageIdInView = dr("EUS_MessageID")
    '                recipientUserId = dr("ToProfileID")
    '            Next

    '            If (Me.MessageIdInView > 0 AndAlso recipientUserId = Me.MasterProfileId) Then
    '                clsUserDoes.MarkMessageRead(Me.MessageIdInView)
    '            End If

    '            msgView.DataSource = dt
    '            msgView.DataBind()

    '        Catch ex As Exception
    '            WebErrorMessageBox(Me, ex, "LoadViewMessage")
    '        End Try
    '    End Sub



    'Private Sub LoadUsersConversation(messageId As Integer)

    '    MessagesView = MessagesViewEnum.CONVERSATION
    '    LoadView()
    '    'mvMessages.SetActiveView(vwConversation)
    '    Me.MessageIdInView = messageId
    '    dvConversation.DataBind()


    '    'If (Me.MessageIdInView > 0 AndAlso Me.MessagesView = MessagesViewEnum.INBOX) Then
    '    '    clsUserDoes.MarkMessageRead(Me.MessageIdInView)
    '    'End If

    '    divConversationReply.Visible = True
    '    divFormActions.Visible = True
    '    divProfileDeletedInfo.Visible = False

    '    If (Me.UserIdInView = 0) Then
    '        divConversationReply.Visible = False
    '        divFormActions.Visible = False
    '        divProfileDeletedInfo.Visible = False

    '    ElseIf (Me.UserIdInView = Me.MasterProfileId) Then
    '        divConversationReply.Visible = False
    '        divFormActions.Visible = False
    '        divProfileDeletedInfo.Visible = False

    '    ElseIf (Me.UserIdInView = 1) Then
    '        divConversationReply.Visible = False
    '        divFormActions.Visible = False

    '    ElseIf (Not clsCustomer.IsProfileActive(Me.UserIdInView)) Then
    '        divConversationReply.Visible = False
    '        divFormActions.Visible = False
    '        ' todo: show message to user that profile is not active
    '        divProfileDeletedInfo.Visible = True

    '    End If
    'End Sub


    ''Private Sub LoadNewMessageView()

    ''    Try
    ''        mvMessages.SetActiveView(vwCreateMessage)
    ''        MessagesView = MessagesViewEnum.SEND_NEW_MESSAGE
    ''        LoadView()

    ''        Dim otherLoginName = Request.QueryString("p")
    ''        If (Not String.IsNullOrEmpty(otherLoginName)) Then

    ''            If (otherLoginName.ToUpper() <> Me.GetCurrentProfile().LoginName.ToUpper()) Then


    ''                Dim memberReceivingMessage As EUS_Profile = DataHelpers.GetEUS_ProfileMasterByLoginName(Me.CMSDBDataContext, otherLoginName, ProfileStatusEnum.Approved, ProfileStatusEnum.LIMITED)
    ''                If (memberReceivingMessage IsNot Nothing) Then

    ''                    Me.UserIdInView = memberReceivingMessage.ProfileID
    ''                    Dim MaySendMessage As Boolean = False
    ''                    Dim AlreadyUnlockedUnlimited As Boolean = clsUserDoes.HasCommunication(Me.MasterProfileId, Me.UserIdInView)
    ''                    'Dim AlreadyUnlockedUnlimited As Boolean = ModGlobals.AllowUnlimited AndAlso clsUserDoes.HasCommunication(Me.MasterProfileId, Me.UserIdInView)

    ''                    Dim SendOnce As Boolean = (Request.QueryString("send") = "once")
    ''                    Dim SendUnlimited As Boolean = (Request.QueryString("send") = "unl") 'AndAlso ModGlobals.AllowUnlimited


    ''                    If (Me.IsMale) Then

    ''                        MaySendMessage = AlreadyUnlockedUnlimited
    ''                        If (Not AlreadyUnlockedUnlimited) Then
    ''                            If (Not SendOnce AndAlso Not SendUnlimited) Then
    ''                                lblSendMessageErr.Text = Me.CurrentPageData.GetCustomString("ErrMayNotSendMessage")
    ''                                pnlSendMessageErr.Visible = True
    ''                                MaySendMessage = False
    ''                            End If


    ''                            If (SendOnce OrElse SendUnlimited) Then
    ''                                MaySendMessage = True
    ''                            End If

    ''                            If Not MaySendMessage AndAlso (HasRequiredCredits(UnlockType.UNLOCK_MESSAGE_SEND)) Then MaySendMessage = True
    ''                            'If Not MaySendMessage AndAlso (clsUserDoes.HasRequiredCredits(Me.MasterProfileId, UnlockType.UNLOCK_MESSAGE_SEND)) Then MaySendMessage = True
    ''                        End If

    ''                        If (Not AlreadyUnlockedUnlimited) Then
    ''                            pnlSendMessageErr.Visible = True
    ''                            lnkSendMessageAltAction.Visible = False

    ''                            If (SendOnce) Then
    ''                                lblSendMessageErr.Text = CurrentPageData.GetCustomString("WarnSendMessageOnce")
    ''                            ElseIf (SendUnlimited) Then
    ''                                lblSendMessageErr.Text = CurrentPageData.GetCustomString("WarnSendMessageUnlimited")
    ''                            End If
    ''                        End If

    ''                    ElseIf (Me.IsFemale) Then
    ''                        MaySendMessage = True

    ''                    End If



    ''                    If (MaySendMessage) Then
    ''                        txtMessageText.Enabled = True
    ''                        btnSendMessage.Enabled = True
    ''                        txtMessageSubject.Enabled = True

    ''                    Else
    ''                        txtMessageText.Enabled = False
    ''                        btnSendMessage.Enabled = False
    ''                        txtMessageSubject.Enabled = False

    ''                        pnlSendMessageErr.Visible = True
    ''                        lnkSendMessageAltAction.Visible = True


    ''                        If (Me.IsMale) Then
    ''                            If (SendOnce) Then
    ''                                lnkSendMessageAltAction.Text = CurrentPageData.GetCustomString("lnkErrSendMessageCreditsNotAvailable")
    ''                                lblSendMessageErr.Text = CurrentPageData.GetCustomString("ErrSendMessageOnceCreditsNotAvailable")

    ''                            ElseIf (SendUnlimited) Then
    ''                                lnkSendMessageAltAction.Text = CurrentPageData.GetCustomString("lnkErrSendMessageCreditsNotAvailable")
    ''                                lblSendMessageErr.Text = CurrentPageData.GetCustomString("ErrSendMessageUnlimitedCreditsNotAvailable")

    ''                            End If
    ''                        End If

    ''                    End If

    ''                    lnkSendMessageAltAction.Text = ReplaceCommonTookens(lnkSendMessageAltAction.Text, memberReceivingMessage.LoginName)
    ''                    lblSendMessageErr.Text = ReplaceCommonTookens(lblSendMessageErr.Text, memberReceivingMessage.LoginName)

    ''                    Try
    ''                        Dim defaultPhoto As DSMembers.EUS_CustomerPhotosRow =
    ''                            DataHelpers.GetProfilesDefaultPhoto(Me.MasterProfileId)

    ''                        Dim memberReceivingMessagePhoto As DSMembers.EUS_CustomerPhotosRow =
    ''                            DataHelpers.GetProfilesDefaultPhoto(memberReceivingMessage.ProfileID)

    ''                        Dim result As clsUserDoes.clsLoadProfilesViewsResult =
    ''                            clsUserDoes.GetProfilesThumbViews(Me.GetCurrentProfile(), defaultPhoto, memberReceivingMessage, memberReceivingMessagePhoto, Me.IsHTTPS)

    ''                        lnkProfImg.NavigateUrl = result.CurrentMemberProfileViewUrl
    ''                        imgProf.ImageUrl = result.CurrentMemberImageUrl
    ''                        lnkProfLogin.NavigateUrl = result.CurrentMemberProfileViewUrl
    ''                        lnkProfLogin.Text = result.CurrentMemberLoginName

    ''                        lnkOtherProfImg.NavigateUrl = result.OtherMemberProfileViewUrl
    ''                        imgOtherProf.ImageUrl = result.OtherMemberImageUrl
    ''                        lnkOtherProfLogin.NavigateUrl = result.OtherMemberProfileViewUrl
    ''                        lnkOtherProfLogin.Text = result.OtherMemberLoginName
    ''                        lblSendToLogin.Text = result.OtherMemberLoginName

    ''                    Catch ex As Exception
    ''                        WebErrorMessageBox(Me, ex, "GetProfilesThumbViews")
    ''                    End Try




    ''                    Dim unlockedConv As EUS_UnlockedConversation = clsUserDoes.GetUnlockedConversation(memberReceivingMessage.ProfileID, Me.MasterProfileId)
    ''                    Dim acceptedOffer As EUS_Offer = clsUserDoes.GetAcceptedOrUnlockedOffer(memberReceivingMessage.ProfileID, Me.MasterProfileId)

    ''                    If (acceptedOffer IsNot Nothing OrElse unlockedConv IsNot Nothing) Then

    ''                        If (acceptedOffer IsNot Nothing AndAlso acceptedOffer.Amount > 0) Then
    ''                            divDatingAmount.Visible = True
    ''                            lblDatingAmount.Text = "&euro;" & acceptedOffer.Amount
    ''                        ElseIf (unlockedConv IsNot Nothing AndAlso unlockedConv.CurrentOfferAmount > 0) Then
    ''                            divDatingAmount.Visible = True
    ''                            lblDatingAmount.Text = "&euro;" & unlockedConv.CurrentOfferAmount
    ''                        End If
    ''                    End If

    ''                Else
    ''                    divActionError.Visible = True
    ''                    lnkAlternativeAction.Text = CurrentPageData.GetCustomString("lnkAlternativeAction_Search")
    ''                    lnkAlternativeAction.NavigateUrl = "~/Members/Search.aspx"
    ''                    lblActionError.Text = CurrentPageData.GetCustomString("ErrorMemberDoesNotExistCannotSendMessage")
    ''                    mvMessages.SetActiveView(vwEmpty)
    ''                End If


    ''            Else
    ''                divActionError.Visible = True
    ''                lnkAlternativeAction.Text = CurrentPageData.GetCustomString("lnkAlternativeAction_Back")
    ''                If (Not String.IsNullOrEmpty(Me.BackUrl)) Then
    ''                    lnkAlternativeAction.NavigateUrl = Me.BackUrl
    ''                Else
    ''                    lnkAlternativeAction.NavigateUrl = "~/Members/"
    ''                End If
    ''                lblActionError.Text = CurrentPageData.GetCustomString("ErrorSendMessageToMySelf")
    ''                mvMessages.SetActiveView(vwEmpty)
    ''            End If

    ''        End If
    ''    Catch ex As Exception
    ''        WebErrorMessageBox(Me, ex, "LoadNewMessageView")
    ''    End Try

    ''End Sub


    'Protected Sub lnkSent_Click(sender As Object, e As EventArgs) Handles lnkSent.Click

    '    Try

    '        MessagesView = MessagesViewEnum.SENT
    '        BindMessagesList()
    '    Catch ex As Exception
    '        WebErrorMessageBox(Me, ex, "")
    '    End Try
    'End Sub

    'Protected Sub lnkInbox_Click(sender As Object, e As EventArgs) Handles lnkInbox.Click

    '    Try
    '        MessagesView = MessagesViewEnum.INBOX
    '        BindMessagesList()
    '    Catch ex As Exception
    '        WebErrorMessageBox(Me, ex, "")
    '    End Try
    'End Sub


    'Protected Sub lnkNew_Click(sender As Object, e As EventArgs) Handles lnkNew.Click

    '    Try
    '        MessagesView = MessagesViewEnum.NEWMESSAGES
    '        BindMessagesList()
    '    Catch ex As Exception
    '        WebErrorMessageBox(Me, ex, "")
    '    End Try
    'End Sub

    'Protected Sub lnkConversation_Click(sender As Object, e As EventArgs) Handles lnkConversation.Click

    '    Try
    '        ExecCmd("VIEWMSG", Me.MessageIdInView)

    '    Catch ex As Exception
    '        WebErrorMessageBox(Me, ex, "")
    '    End Try
    'End Sub


    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
        BindMessagesList()
    End Sub


    Protected Sub LoadLAG()
        Try
            If (String.IsNullOrEmpty(CurrentPageData.GetCustomString("lnkNew"))) Then
                clsPageData.ClearCache()
            End If

            lnkNew.Text = "<span></span>" & CurrentPageData.GetCustomString("lnkNew")
            lnkSent.Text = "<span></span>" & CurrentPageData.GetCustomString("lnkSent")
            lnkInbox.Text = "<span></span>" & CurrentPageData.GetCustomString("lnkInbox")
            lnkTrash.Text = "<span></span>" & CurrentPageData.GetCustomString("lnkTrash")
            'lnkConversation.Text = "<span></span>" & CurrentPageData.GetCustomString("lnkConversation")


            cbSort.Items.Clear()
            cbSort.Items.Add(CurrentPageData.GetCustomString("cbSort_Recent"), "Recent")
            cbSort.Items(cbSort.Items.Count - 1).Selected = True
            cbSort.Items.Add(CurrentPageData.GetCustomString("cbSort_Oldest"), "Oldest")

            lblSortByText.Text = CurrentPageData.GetCustomString("lblSortByText")

            ''lblSelectFilter.Text = CurrentPageData.GetCustomString(lblSelectFilter.ID)

            ''cbFilter.Items.Clear()
            ''cbFilter.Items.Add("---", "0")
            ''cbFilter.Items.Add(CurrentPageData.GetCustomString("cbFilter_Unread"), "Unread")
            ''cbFilter.Items.Add(CurrentPageData.GetCustomString("cbFilter_Read"), "Read")

            ''btnDeleteSeleted.Text = CurrentPageData.GetCustomString(btnDeleteSeleted.ID)


            ''lblReplyTitle.Text = CurrentPageData.GetCustomString(lblReplyTitle.ID)
            ''btnReplyMessage.Text = CurrentPageData.GetCustomString(btnReplyMessage.ID)

            ''lblConversationReplyTitle.Text = CurrentPageData.GetCustomString(lblReplyTitle.ID)
            ''btnConversationReply.Text = CurrentPageData.GetCustomString(btnReplyMessage.ID)

            ' '' new message section
            ''lblSendMessageToTitle.Text = CurrentPageData.GetCustomString("lblSendMessageToTitle")
            ''lnkBack.Text = CurrentPageData.GetCustomString("lnkBack")
            ''lblYourMsgSubject.Text = CurrentPageData.GetCustomString("lblYourMsgSubject")
            ''lblYourMsgBody.Text = CurrentPageData.GetCustomString("lblYourMsgBody")
            ''lblYourFirstDateStartedTitle.Text = CurrentPageData.GetCustomString("lblYourFirstDateStartedTitle")
            ''lblYourFirstDateStartedText.Text = CurrentPageData.GetCustomString("lblYourFirstDateStartedText")
            ''lblYourMsgFrom.Text = CurrentPageData.GetCustomString("lblYourMsgFrom")
            ''lblYourMsgTo.Text = CurrentPageData.GetCustomString("lblYourMsgTo")
            ''btnSendMessage.Text = CurrentPageData.GetCustomString("btnSendMessage")

            ' no messages
            msg_SearchOurMembersText.Text = CurrentPageData.GetCustomString("msg_SearchOurMembersText")
            lblNoMessagesText.Text = CurrentPageData.GetCustomString("lblNoMessagesText_ND")

            ''lblProfileDeletedInfo.Text = CurrentPageData.GetCustomString("lblProfileDeletedInfo")
            lnkInactiveProfiles.Text = CurrentPageData.GetCustomString("lnkInactiveProfiles")

            'imgItems.ImageUrl = "~/images/members_bar/7.png"
            lblItemsName.Text = Me.CurrentPageData.GetCustomString("CartMessagesView")
            lnkViewDescription.Text = Me.CurrentPageData.GetCustomString("CartMessagesWhatIsIt")
            lnkViewDescription.OnClientClick = WhatIsIt.OnMoreInfoClickFunc(
                lnkViewDescription.OnClientClick,
                ResolveUrl("~/Members/InfoWin.aspx?info=messages"),
                Me.CurrentPageData.GetCustomString("CartMessagesView"))
            WhatIsIt1.PopupControl.HeaderText = Me.CurrentPageData.GetCustomString("CartMessagesView")

            lblSortByText.Text = CurrentPageData.GetCustomString("lblSortByText")
            lblResultsPerPageText.Text = CurrentPageData.GetCustomString("lblResultsPerPageText")


        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    'Protected Sub msgView_DataBound(sender As Object, e As EventArgs) Handles msgView.DataBound

    '    Try
    '        If (msgView.Items.Count = 1) Then
    '            Dim item As DevExpress.Web.ASPxDataView.DataViewItem = msgView.Items(0)
    '            CType(msgView.FindItemControl("lblInfoMessage", item), ASPxLabel).Text = CurrentPageData.GetCustomString("lblInfoMessage")
    '            CType(msgView.FindItemControl("btnBack", item), ASPxButton).Text = CurrentPageData.GetCustomString("btnBack")
    '            CType(msgView.FindItemControl("btnDelete", item), ASPxButton).Text = CurrentPageData.GetCustomString("btnDelete")
    '            'CType(msgView.FindItemControl("btnDelete", item), LinkButton).ToolTip = CurrentPageData.GetCustomString("btnDelete")
    '        End If

    '    Catch ex As Exception
    '        WebErrorMessageBox(Me, ex, "")
    '    End Try


    'End Sub


    ''Protected Sub lvMsgs_DataBound(sender As Object, e As EventArgs) Handles lvMsgs.DataBound

    ''    Try
    ''        If (lvMsgs.Items.Count > 0) Then

    ''            If (Me.MessagesView = MessagesViewEnum.INBOX) Then
    ''                CType(lvMsgs.FindControl("lblHeaderFromTo"), ASPxLabel).Text = CurrentPageData.GetCustomString("lblHeaderFrom")
    ''            ElseIf (Me.MessagesView = MessagesViewEnum.SENT) Then
    ''                If (Me.IsMale) Then
    ''                    CType(lvMsgs.FindControl("lblHeaderFromTo"), ASPxLabel).Text = CurrentPageData.GetCustomString("MALE_lblHeaderTo")
    ''                Else
    ''                    CType(lvMsgs.FindControl("lblHeaderFromTo"), ASPxLabel).Text = CurrentPageData.GetCustomString("FEMALE_lblHeaderTo")
    ''                End If
    ''            ElseIf (Me.MessagesView = MessagesViewEnum.TRASH) Then
    ''                CType(lvMsgs.FindControl("lblHeaderFromTo"), ASPxLabel).Text = ""
    ''            Else
    ''                CType(lvMsgs.FindControl("lblHeaderFromTo"), ASPxLabel).Text = CurrentPageData.GetCustomString("lblHeaderFrom")
    ''            End If
    ''            CType(lvMsgs.FindControl("lblHeaderMessage"), ASPxLabel).Text = CurrentPageData.GetCustomString("lblHeaderMessage")
    ''            CType(lvMsgs.FindControl("lblHeaderStatus"), ASPxLabel).Text = CurrentPageData.GetCustomString("lblHeaderStatus")


    ''            For Each itm As ListViewDataItem In lvMsgs.Items

    ''                Dim hdfProfileID As HiddenField = CType(itm.FindControl("hdfProfileID"), HiddenField)

    ''                Dim divTooltipholder As Panel = CType(itm.FindControl("divTooltipholder"), Panel)
    ''                Dim lnkTooltip As HyperLink = CType(itm.FindControl("lnkTooltip"), HyperLink)
    ''                Dim lnkLogin As ASPxHyperLink = CType(itm.FindControl("lnkLogin"), ASPxHyperLink)

    ''                Dim lblCommStat As ASPxLabel = CType(itm.FindControl("lblCommStat"), ASPxLabel)
    ''                Dim comStat As Long = 0
    ''                Dim comIsOpen = False
    ''                If (Not String.IsNullOrEmpty(lblCommStat.Text)) Then
    ''                    If (Long.TryParse(lblCommStat.Text, comStat)) Then
    ''                        If (comStat > 0) Then comIsOpen = True
    ''                    End If
    ''                End If

    ''                If (Me.MessagesView = MessagesViewEnum.NEWMESSAGES) Then
    ''                    If (Me.IsMale) Then

    ''                        If (hdfProfileID.Value = "1") Then
    ''                            lblCommStat.Text = ""
    ''                            'lblCommStat.ForeColor = Color.Green
    ''                        ElseIf (comIsOpen) Then
    ''                            lblCommStat.Text = CurrentPageData.GetCustomString("CommunicationOpen")
    ''                            lblCommStat.CssClass = lblCommStat.CssClass.Replace("closed", "")
    ''                            lblCommStat.CssClass = lblCommStat.CssClass & " open"
    ''                            'lblCommStat.ForeColor = Color.Green
    ''                        Else
    ''                            lblCommStat.Text = CurrentPageData.GetCustomString("CommunicationLocked")
    ''                            lblCommStat.CssClass = lblCommStat.CssClass.Replace("open", "")
    ''                            lblCommStat.CssClass = lblCommStat.CssClass & " closed"
    ''                            'lblCommStat.ForeColor = Color.Magenta

    ''                            'Try
    ''                            '    If (Me.IsMale) Then
    ''                            '        divTooltipholder.Visible = True
    ''                            '        Dim tt As String = CurrentPageData.GetCustomString("UnlockNotice")
    ''                            '        tt = tt.Replace("###LOGINNAME###", lnkLogin.Text)
    ''                            '        lnkTooltip.ToolTip = tt
    ''                            '    End If
    ''                            'Catch ex As Exception

    ''                            'End Try
    ''                        End If
    ''                    ElseIf (Me.IsFemale) Then

    ''                        lblCommStat.Text = ""
    ''                        If (comIsOpen) Then
    ''                            lblCommStat.Text = CurrentPageData.GetCustomString("CommunicationOpen")
    ''                            lblCommStat.CssClass = lblCommStat.CssClass.Replace("closed", "")
    ''                            lblCommStat.CssClass = lblCommStat.CssClass & " open"
    ''                            'lblCommStat.ForeColor = Color.Green
    ''                        End If

    ''                    End If

    ''                ElseIf (Me.MessagesView = MessagesViewEnum.INBOX) Then
    ''                    If (Me.IsMale) Then

    ''                        'messages are read, don't display status of communication
    ''                        lblCommStat.Text = ""

    ''                        'If (hdfProfileID.Value = "1") Then
    ''                        '    lblCommStat.Text = ""
    ''                        '    'lblCommStat.ForeColor = Color.Green
    ''                        'ElseIf (comIsOpen) Then
    ''                        '    lblCommStat.Text = CurrentPageData.GetCustomString("CommunicationOpen")
    ''                        '    lblCommStat.CssClass = lblCommStat.CssClass.Replace("closed", "")
    ''                        '    lblCommStat.CssClass = lblCommStat.CssClass & " open"
    ''                        '    'lblCommStat.ForeColor = Color.Green
    ''                        'Else
    ''                        '    lblCommStat.Text = CurrentPageData.GetCustomString("CommunicationLocked")
    ''                        '    lblCommStat.CssClass = lblCommStat.CssClass.Replace("open", "")
    ''                        '    lblCommStat.CssClass = lblCommStat.CssClass & " closed"
    ''                        '    'lblCommStat.ForeColor = Color.Magenta

    ''                        '    Try
    ''                        '        If (Me.IsMale) Then
    ''                        '            divTooltipholder.Visible = True
    ''                        '            Dim tt As String = CurrentPageData.GetCustomString("UnlockNotice")
    ''                        '            tt = tt.Replace("###LOGINNAME###", lnkLogin.Text)
    ''                        '            lnkTooltip.ToolTip = tt
    ''                        '        End If
    ''                        '    Catch ex As Exception

    ''                        '    End Try
    ''                        'End If
    ''                    ElseIf (Me.IsFemale) Then

    ''                        lblCommStat.Text = ""
    ''                        If (comIsOpen) Then
    ''                            lblCommStat.Text = CurrentPageData.GetCustomString("CommunicationOpen")
    ''                            lblCommStat.CssClass = lblCommStat.CssClass.Replace("closed", "")
    ''                            lblCommStat.CssClass = lblCommStat.CssClass & " open"
    ''                            'lblCommStat.ForeColor = Color.Green
    ''                        End If

    ''                    End If

    ''                ElseIf (Me.MessagesView = MessagesViewEnum.TRASH) Then
    ''                    If (Me.IsMale) Then

    ''                        'messages are read, don't display status of communication
    ''                        lblCommStat.Text = ""

    ''                        'If (hdfProfileID.Value = "1") Then
    ''                        '    lblCommStat.Text = ""
    ''                        '    'lblCommStat.ForeColor = Color.Green
    ''                        'ElseIf (comIsOpen) Then
    ''                        '    lblCommStat.Text = CurrentPageData.GetCustomString("CommunicationOpen")
    ''                        '    lblCommStat.CssClass = lblCommStat.CssClass.Replace("closed", "")
    ''                        '    lblCommStat.CssClass = lblCommStat.CssClass & " open"
    ''                        '    'lblCommStat.ForeColor = Color.Green
    ''                        'Else
    ''                        '    lblCommStat.Text = CurrentPageData.GetCustomString("CommunicationLocked")
    ''                        '    lblCommStat.CssClass = lblCommStat.CssClass.Replace("open", "")
    ''                        '    lblCommStat.CssClass = lblCommStat.CssClass & " closed"
    ''                        '    'lblCommStat.ForeColor = Color.Magenta

    ''                        '    Try
    ''                        '        If (Me.IsMale) Then
    ''                        '            divTooltipholder.Visible = True
    ''                        '            Dim tt As String = CurrentPageData.GetCustomString("UnlockNotice")
    ''                        '            tt = tt.Replace("###LOGINNAME###", lnkLogin.Text)
    ''                        '            lnkTooltip.ToolTip = tt
    ''                        '        End If
    ''                        '    Catch ex As Exception

    ''                        '    End Try
    ''                        'End If
    ''                    ElseIf (Me.IsFemale) Then

    ''                        lblCommStat.Text = ""
    ''                        If (comIsOpen) Then
    ''                            lblCommStat.Text = CurrentPageData.GetCustomString("CommunicationOpen")
    ''                            lblCommStat.CssClass = lblCommStat.CssClass.Replace("closed", "")
    ''                            lblCommStat.CssClass = lblCommStat.CssClass & " open"
    ''                            'lblCommStat.ForeColor = Color.Green
    ''                        End If

    ''                    End If

    ''                Else

    ''                    lblCommStat.Text = ""

    ''                    If (hdfProfileID.Value = "1") Then
    ''                        lblCommStat.Text = ""
    ''                        'lblCommStat.ForeColor = Color.Green
    ''                    ElseIf (comIsOpen) Then
    ''                        lblCommStat.Text = CurrentPageData.GetCustomString("CommunicationOpen")
    ''                        lblCommStat.CssClass = lblCommStat.CssClass.Replace("closed", "")
    ''                        lblCommStat.CssClass = lblCommStat.CssClass & " open"
    ''                        'lblCommStat.ForeColor = Color.Green
    ''                    End If
    ''                End If

    ''                Dim lblOfferAmount As ASPxLabel = CType(itm.FindControl("lblOfferAmount"), ASPxLabel)
    ''                If (Not String.IsNullOrEmpty(lblCommStat.Text) AndAlso Not String.IsNullOrEmpty(lblOfferAmount.Text)) Then
    ''                    lblOfferAmount.Text = lblOfferAmount.Text & " / "
    ''                End If

    ''                Dim lblReadStat As ASPxLabel = CType(itm.FindControl("lblReadStat"), ASPxLabel)
    ''                Dim lblSentDate As ASPxLabel = CType(itm.FindControl("lblSentDate"), ASPxLabel)
    ''                If (Me.MessagesView = MessagesViewEnum.INBOX) Then
    ''                    Dim hdfMessageRead As HiddenField = CType(itm.FindControl("hdfMessageRead"), HiddenField)
    ''                    If (hdfMessageRead.Value = "read") Then
    ''                        lblReadStat.CssClass = "open"
    ''                    Else
    ''                        lblReadStat.CssClass = "closed"
    ''                    End If
    ''                Else
    ''                    lblReadStat.Visible = False
    ''                    Dim lblSeparatorReadDate = itm.FindControl("lblSeparatorReadDate")
    ''                    lblSeparatorReadDate.Visible = False
    ''                End If

    ''                Dim btnDelete As LinkButton = itm.FindControl("btnDeleteAllInConversation")
    ''                btnDelete.OnClientClick = String.Format(<js><![CDATA[return confirm('{0}');]]></js>.Value, CurrentPageData.GetCustomString("MessagesAllDeleteConfirmationText").Replace("'", "\'"))
    ''                btnDelete.OnClientClick = ReplaceCommonTookens(btnDelete.OnClientClick, lnkLogin.Text)

    ''                Select Case Me.MessagesView
    ''                    Case MessagesViewEnum.INBOX
    ''                        btnDelete.OnClientClick = Regex.Replace(btnDelete.OnClientClick, "###VIEW###", CurrentPageData.GetCustomString("lnkInbox"))

    ''                    Case MessagesViewEnum.NEWMESSAGES
    ''                        btnDelete.OnClientClick = Regex.Replace(btnDelete.OnClientClick, "###VIEW###", CurrentPageData.GetCustomString("lnkNew"))

    ''                    Case MessagesViewEnum.SENT
    ''                        btnDelete.OnClientClick = Regex.Replace(btnDelete.OnClientClick, "###VIEW###", CurrentPageData.GetCustomString("lnkSent"))

    ''                    Case MessagesViewEnum.TRASH
    ''                        btnDelete.OnClientClick = String.Format(<js><![CDATA[return confirm('{0}');]]></js>.Value, CurrentPageData.GetCustomString("MessagesAllDeletePermanentlyConfirmationText").Replace("'", "\'"))
    ''                        btnDelete.OnClientClick = ReplaceCommonTookens(btnDelete.OnClientClick, lnkLogin.Text)
    ''                        btnDelete.OnClientClick = Regex.Replace(btnDelete.OnClientClick, "###VIEW###", CurrentPageData.GetCustomString("lnkTrash"))

    ''                End Select

    ''                '                    btnDelete.ClientSideEvents.SetEventHandler("Click", String.Format(<js><![CDATA[
    ''                'function(s, e) {{
    ''                '    if(!confirm('{0}')){{
    ''                '        e.processOnServer=false;
    ''                '        return false;
    ''                '    }}
    ''                '}}
    ''                ']]></js>.Value, CurrentPageData.GetCustomString("MessageDeleteConfirmationText").Replace("'", "\'")))
    ''                'btnDelete.Text = Me.CurrentPageData.GetCustomString("ListItemDeleteButtonText")
    ''                btnDelete.ToolTip = Me.CurrentPageData.GetCustomString("ListItemDeleteButtonText")


    ''            Next


    ''        End If

    ''    Catch ex As Exception
    ''        WebErrorMessageBox(Me, ex, "")
    ''    End Try
    ''End Sub

    ''Protected Sub btnDeleteSeleted_Click(sender As Object, e As EventArgs) Handles btnDeleteSeleted.Click
    ''    Try

    ''        For Each itm As ListViewDataItem In lvMsgs.Items
    ''            Try

    ''                If (CType(itm.FindControl("chkSelectMsg"), ASPxCheckBox).Checked) Then
    ''                    Dim msgId = CType(itm.FindControl("btnDelete"), ASPxButton).CommandArgument
    ''                    ExecCmd("DELETEMSG", msgId)
    ''                End If
    ''            Catch ex As Exception
    ''                WebErrorMessageBox(Me, ex, "")
    ''            End Try
    ''        Next
    ''        BindMessagesList()
    ''    Catch ex As Exception
    ''        WebErrorMessageBox(Me, ex, "")
    ''    End Try
    ''End Sub

    Protected Sub lvMsgs_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs)
        Try
            ExecCmd(e.CommandName, e.CommandArgument)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub

    'Protected Sub lvMsgs_ItemCommand(sender As Object, e As System.Web.UI.WebControls.ListViewCommandEventArgs) Handles lvMsgs.ItemCommand
    '    Try
    '        ExecCmd(e.CommandName, e.CommandArgument)
    '    Catch ex As Exception
    '        WebErrorMessageBox(Me, ex, "")
    '    End Try

    'End Sub

    'Protected Sub msgView_ItemCommand(source As Object, e As DevExpress.Web.ASPxDataView.DataViewItemCommandEventArgs) Handles msgView.ItemCommand
    '    Try
    '        ExecCmd(e.CommandName, e.CommandArgument)
    '    Catch ex As Exception
    '        WebErrorMessageBox(Me, ex, "")
    '    End Try
    'End Sub


    ''Protected Sub dvConversation_ItemCommand(source As Object, e As DevExpress.Web.ASPxDataView.DataViewItemCommandEventArgs) Handles dvConversation.ItemCommand
    ''    Try
    ''        ExecCmd(e.CommandName, e.CommandArgument)
    ''    Catch ex As Exception
    ''        WebErrorMessageBox(Me, ex, "")
    ''    End Try
    ''End Sub


    Private Sub ExecCmd(CommandName As String, CommandArgument As String)
        Try

            If (CommandName = "DELETEMSG") Then


                'If (MessagesView = MessagesViewEnum.CONVERSATION) Then
                '    Dim MsgID As Integer
                '    Integer.TryParse(CommandArgument, MsgID)
                '    If (clsNullable.NullTo(Me.GetCurrentProfile().ReferrerParentId) > 0) Then
                '        clsUserDoes.DeleteMessage(Me.MasterProfileId, MsgID, True)
                '    Else
                '        clsUserDoes.DeleteMessage(Me.MasterProfileId, MsgID, False)
                '    End If
                '    Me.MessageIdListInView.Remove(MsgID)
                '    If (Me.MessageIdListInView.Count > 0) Then
                '        MsgID = Me.MessageIdListInView(0)
                '        LoadUsersConversation(MsgID)
                '    Else
                '        MessagesView = MessagesViewEnum.INBOX
                '        Me.MessageIdInView = 0
                '        BindMessagesList()
                '    End If
                'Else
                Dim MsgID As Integer
                Integer.TryParse(CommandArgument, MsgID)
                If (clsNullable.NullTo(Me.GetCurrentProfile().ReferrerParentId) > 0) Then
                    clsUserDoes.DeleteMessage(Me.MasterProfileId, MsgID, True)
                Else
                    clsUserDoes.DeleteMessage(Me.MasterProfileId, MsgID, False)
                End If
                BindMessagesList()
                'End If
            End If


            If (CommandName = "DELETEALLMSG") Then

                Dim MsgID As Integer
                Integer.TryParse(CommandArgument, MsgID)
                Dim isReferrer As Boolean = (clsNullable.NullTo(Me.GetCurrentProfile().ReferrerParentId) > 0)
                Dim message As EUS_Message = clsUserDoes.GetMessage(MsgID)
                If (message IsNot Nothing) Then clsMessagesHelper.DeleteMessagesFromMemberInView(Me.MasterProfileId, message.FromProfileID, message.ToProfileID, Me.MessagesView, (isReferrer = True))
                BindMessagesList()
            End If

            If (CommandName = "VIEWMSG") Then

                Dim MsgID As Integer
                Integer.TryParse(CommandArgument, MsgID)
                'LoadUsersConversation(MsgID)

                Dim url As String = System.Web.VirtualPathUtility.ToAbsolute("~/Members/Conversation.aspx?msg=" & MsgID & "&vwh=" & Me.CurrentLoginName_enc)
                Response.Redirect(url, False)
                HttpContext.Current.ApplicationInstance.CompleteRequest()
            End If

            If (CommandName = "CMDBACK") Then
                BindMessagesList()
            End If

            LoadView()
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    ''Protected Sub cbFilter_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbFilter.SelectedIndexChanged
    ''    Try
    ''        BindMessagesList(cbFilter.SelectedItem.Text.ToUpper())

    ''    Catch ex As Exception
    ''        WebErrorMessageBox(Me, ex, "")
    ''    End Try
    ''End Sub

    ''Protected Sub btnReplyMessage_Click(sender As Object, e As EventArgs) Handles btnReplyMessage.Click
    ''    Try
    ''        If (Not String.IsNullOrEmpty(txtReplyMessageBody.Text)) Then
    ''            Dim replySubject As String = Me.SubjectInView
    ''            If (Not String.IsNullOrEmpty(replySubject) AndAlso Not replySubject.StartsWith(globalStrings.GetCustomString("Reply") & ":")) Then
    ''                replySubject = globalStrings.GetCustomString("Reply") & ":" & replySubject
    ''            End If
    ''            clsUserDoes.SendMessage(replySubject, txtReplyMessageBody.Text, Me.MasterProfileId, Me.UserIdInView, Me.MessageIdInView, 0, MyBase.BlockMessagesToBeReceivedByOther)
    ''            txtReplyMessageBody.Text = ""

    ''            Me.MessagesView = MessagesViewEnum.SENT
    ''            BindMessagesList()
    ''        Else
    ''            lblMessageErr.Text = Me.CurrentPageData.GetCustomString("ErrorEmptyMessage")
    ''            pnlMessageErr.Visible = True
    ''        End If
    ''    Catch ex As Exception
    ''        WebErrorMessageBox(Me, ex, "")
    ''    End Try
    ''End Sub

    'Protected Sub btnSendMessage_Click(sender As Object, e As EventArgs) Handles btnSendMessage.Click
    '    Try

    '    Catch ex As Exception
    '        WebErrorMessageBox(Me, ex, "")
    '    End Try
    'End Sub


    Private Sub Messages_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        'If (lblActionError.Text.Length > 0) Then
        '    mvMessages.SetActiveView(vwEmpty)
        'End If

        'If (Me.MessageIdInView = 0) Then
        '    liConversation.Visible = False
        'Else
        '    liConversation.Visible = True
        'End If

        If (mvMessages.GetActiveView().Equals(vwAllMessage) AndAlso msgsL.UsersList.Count = 0) Then
            mvMessages.SetActiveView(vwNoMessages)
        Else

            Dim url As String = Request.Url.AbsolutePath
            url = url & "?"
            Dim seo As String = ("seo" & Me.Pager.ClientID).ToUpper()
            For Each itm As String In Request.QueryString.AllKeys
                If (Not String.IsNullOrEmpty(itm) AndAlso itm.ToUpper() <> seo AndAlso itm.ToUpper() <> "ITEMSPERPAGE") Then
                    url = url & itm & "=" & HttpUtility.UrlEncode(Request.QueryString(itm)) & "&"
                End If
            Next

            Me.Pager.SeoNavigateUrlFormatString = url & "seo" & Me.Pager.ClientID & "={0}&itemsPerPage=" & Me.ItemsPerPage
        End If


    End Sub


    ''Protected Sub sdsConversations_Selecting(sender As Object, e As System.Web.UI.WebControls.SqlDataSourceSelectingEventArgs) Handles sdsConversations.Selecting
    ''    For Each prm As SqlClient.SqlParameter In e.Command.Parameters
    ''        If (prm.ParameterName = "@messageID") Then
    ''            prm.Value = Me.MessageIdInView
    ''        ElseIf (prm.ParameterName = "@ownerProfileID") Then
    ''            prm.Value = Me.MasterProfileId
    ''        End If
    ''    Next
    ''End Sub

    ''Protected Sub dvConversation_DataBound(sender As Object, e As EventArgs) Handles dvConversation.DataBound
    ''    Try
    ''        Me.UserIdInView = 0
    ''        Me.SubjectInView = ""
    ''        'Me.MessageIdInView = 0
    ''        Dim loginName As String = ""

    ''        Me.MessageIdListInView = New List(Of Integer)
    ''        Dim cnt As Integer
    ''        For cnt = 0 To dvConversation.Items.Count - 1
    ''            'For Each dvi As DevExpress.Web.ASPxDataView.DataViewItem In dvConversation.Items
    ''            Try

    ''                Dim dvi As DevExpress.Web.ASPxDataView.DataViewItem = dvConversation.Items(cnt)
    ''                Dim dr As DataRowView = dvi.DataItem

    ''                If (Me.MessageIdInView > 0) Then

    ''                    If (Me.MessageIdInView = dr("EUS_MessageID")) Then
    ''                        Me.SubjectInView = dr("Subject")
    ''                        Me.MessageIdInView = dr("EUS_MessageID")
    ''                    End If

    ''                    If (dr("ProfileId") <> Me.MasterProfileId) Then
    ''                        Me.UserIdInView = dr("ProfileId")
    ''                        loginName = dr("LoginName")
    ''                    End If
    ''                    'Else

    ''                    '    Me.UserIdInView = dr("ProfileId")
    ''                    '    loginName = dr("LoginName")
    ''                    '    Me.SubjectInView = dr("Subject")
    ''                    '    Me.MessageIdInView = dr("EUS_MessageID")
    ''                End If

    ''                Me.MessageIdListInView.Add(dr("EUS_MessageID"))

    ''                Dim lblAmountDate As ASPxLabel = dvConversation.FindItemControl("lblAmountDate", dvi)

    ''                If (lblAmountDate IsNot Nothing) Then

    ''                    If (dr("Amount") IsNot System.DBNull.Value AndAlso dr("Amount") > 0) Then
    ''                        lblAmountDate.Text = "&euro;" & dr("Amount") & "&nbsp;" & Me.CurrentPageData.GetCustomString("OfferAmountDateWord")
    ''                    Else
    ''                        lblAmountDate.Visible = False
    ''                    End If

    ''                    Dim lnkFromImage As ASPxHyperLink = dvConversation.FindItemControl("lnkFromImage", dvi)
    ''                    lnkFromImage.ImageUrl = ProfileHelper.GetProfileImageURL(dr("PhotoCustomerID"), dr("PhotoFileName"), dr("GenderId"), True, Me.IsHTTPS)

    ''                    Dim lblSubjectTitle As ASPxLabel = dvConversation.FindItemControl("lblSubjectTitle", dvi)
    ''                    lblSubjectTitle.Text = CurrentPageData.GetCustomString(lblYourMsgSubject.ID)

    ''                    Dim lblTextTitle As ASPxLabel = dvConversation.FindItemControl("lblTextTitle", dvi)
    ''                    lblTextTitle.Text = CurrentPageData.GetCustomString("ConversationMessageTextTitle")

    ''                    Dim btnDelete As LinkButton = dvConversation.FindItemControl("btnDelete", dvi)
    ''                    btnDelete.Text = CurrentPageData.GetCustomString("btnDelete")
    ''                    btnDelete.OnClientClick = String.Format(<js><![CDATA[return confirm('{0}');]]></js>.Value, CurrentPageData.GetCustomString("MessageDeleteConfirmationText").Replace("'", "\'"))
    ''                    btnDelete.ToolTip = Me.CurrentPageData.GetCustomString("ListItemDeleteButtonText")



    ''                    Dim lblScrollto As Label = dvConversation.FindItemControl("lblScrollto", dvi)
    ''                    If (cnt = dvConversation.Items.Count - 1) Then
    ''                        lblScrollto.Visible = True
    ''                    Else
    ''                        lblScrollto.Visible = False
    ''                    End If
    ''                    ScrollToElem = lblScrollto.ClientID
    ''                End If


    ''                If (dr("StatusID") = 0 AndAlso dr("EUS_MessageID") > 0) Then
    ''                    clsUserDoes.MarkMessageRead(dr("EUS_MessageID"))
    ''                End If

    ''            Catch ex As NullReferenceException
    ''                'WebErrorMessageBox(Me, ex, "dvConversation_DataBound")
    ''            Catch ex As Exception
    ''                WebErrorMessageBox(Me, ex, "dvConversation_DataBound")
    ''            End Try
    ''        Next


    ''        If (ProfileHelper.IsMale(Me.SessionVariables.MemberData.GenderId)) Then

    ''            lblReplyInformation.Text = CurrentPageData.GetCustomString("MALE_lblReplyInformation")
    ''            lblReplyInformation.Text = lblReplyInformation.Text.Replace("###LOGINNAME###", loginName)

    ''            lblConversationReplyInformation.Text = CurrentPageData.GetCustomString("MALE_lblReplyInformation")
    ''            lblConversationReplyInformation.Text = lblConversationReplyInformation.Text.Replace("###LOGINNAME###", loginName)

    ''        Else

    ''            lblReplyInformation.Text = CurrentPageData.GetCustomString("FEMALE_lblReplyInformation")
    ''            lblReplyInformation.Text = lblReplyInformation.Text.Replace("###LOGINNAME###", loginName)

    ''            lblConversationReplyInformation.Text = CurrentPageData.GetCustomString("FEMALE_lblReplyInformation")
    ''            lblConversationReplyInformation.Text = lblConversationReplyInformation.Text.Replace("###LOGINNAME###", loginName)

    ''        End If


    ''    Catch ex As Exception
    ''        WebErrorMessageBox(Me, ex, "")
    ''    End Try

    ''End Sub




    ''Protected Sub btnConversationReply_Click(sender As Object, e As EventArgs) Handles btnConversationReply.Click
    ''    Try
    ''        If (Not String.IsNullOrEmpty(txtConversationReply.Text)) Then
    ''            Dim replySubject As String = Me.SubjectInView
    ''            If (Not String.IsNullOrEmpty(replySubject) AndAlso Not replySubject.StartsWith(globalStrings.GetCustomString("Reply") & ":")) Then
    ''                replySubject = globalStrings.GetCustomString("Reply") & ": " & replySubject
    ''            End If
    ''            clsUserDoes.SendMessage(replySubject, txtConversationReply.Text, Me.MasterProfileId, Me.UserIdInView, Me.MessageIdInView, 0, MyBase.BlockMessagesToBeReceivedByOther)
    ''            txtConversationReply.Text = ""

    ''            Me.MessagesView = MessagesViewEnum.SENT
    ''            BindMessagesList()
    ''        Else
    ''            pnlConversationReplyErr.Visible = True
    ''            lblConversationReplyErr.Text = Me.CurrentPageData.GetCustomString("ErrorEmptyMessage")
    ''        End If
    ''    Catch ex As Exception
    ''        WebErrorMessageBox(Me, ex, "")
    ''    End Try
    ''End Sub



    Protected Sub mvMessages_ActiveViewChanged(sender As Object, e As EventArgs) Handles mvMessages.ActiveViewChanged

    End Sub


End Class





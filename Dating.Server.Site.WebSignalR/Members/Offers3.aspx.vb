﻿Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Core.DLL
Imports Dating.Server.Datasets.DLL.DSMembers

Public Class Offers3
    Inherits BasePage


    Public Property OffersSorting As OffersSortEnum

        Get
            Dim sorting As OffersSortEnum = OffersSortEnum.RecentOffers

            Dim index As Integer = cbSort.SelectedIndex

            If (index = 0) Then
                sorting = OffersSortEnum.OfferAmountHighest
            ElseIf (index = 1) Then
                sorting = OffersSortEnum.OfferAmountLowest
            ElseIf (index = 3) Then
                sorting = OffersSortEnum.OldestOffers
            End If

            Return sorting
        End Get

        Set(value As OffersSortEnum)
            Dim index As Integer = 2

            If (value = OffersSortEnum.OfferAmountHighest) Then
                index = 0
            ElseIf (value = OffersSortEnum.OfferAmountLowest) Then
                index = 1
            ElseIf (value = OffersSortEnum.OldestOffers) Then
                index = 3
            Else
                index = 2
            End If
            cbSort.SelectedIndex = index
        End Set

    End Property


    Public Property ItemsPerPage As Integer

        Get
            Dim perPageNumber As Integer = 10

            Dim index As Integer = cbPerPage.SelectedIndex
            If (index = 0) Then
                perPageNumber = 10
            ElseIf (index = 1) Then
                perPageNumber = 25
            ElseIf (index = 2) Then
                perPageNumber = 50
            End If

            Return perPageNumber
        End Get

        Set(value As Integer)
            Dim index As Integer = 0

            If (value = 25) Then
                index = 1
            ElseIf (value = 50) Then
                index = 2
            Else
                index = 0
            End If

            cbPerPage.SelectedIndex = index
        End Set

    End Property



    Protected Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            'If (_pageData Is Nothing) Then _pageData = New clsPageData("~/Members/Offers.aspx", Context)
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData("~/Members/Offers.aspx", Context, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property


    Private ReadOnly Property CurrentOffersControl As OfferControl3
        Get

            If (mvOfferMain.GetActiveView() Is Nothing OrElse mvOfferMain.GetActiveView().Equals(vwNewOffers)) Then
                Return newOffers

            ElseIf (mvOfferMain.GetActiveView().Equals(vwPendingOffers)) Then
                Return pendingOffers

            ElseIf (mvOfferMain.GetActiveView().Equals(vwRejectedOffers)) Then
                Return rejectedOffers

            ElseIf (mvOfferMain.GetActiveView().Equals(vwAcceptedOffers)) Then
                Return acceptedOffers

            End If

            Return Nothing
        End Get
    End Property



    Protected Overrides Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        SetMasterPage(CurrentPageData)
        Try
            If (Me.SessionVariables.MemberData.Status = ProfileStatusEnum.LIMITED) Then
                Response.Redirect(ResolveUrl("~/Members/Default.aspx"))
            End If

            MyBase.Page_PreInit()
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub




    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        AddHandler Me.CurrentOffersControl.Repeater.ItemCommand, AddressOf Me.offersRepeater_ItemCommand
        Try

            If (Not Page.IsPostBack) Then
                Try
                    Me.ItemsPerPage = clsProfilesPrivacySettings.GET_ItemsPerPage(Me.MasterProfileId)
                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "GET_ItemsPerPage")
                End Try

                Dim req As String = Request.QueryString("itemsPerPage")
                Dim _itemsPerPage As Integer
                If (Not String.IsNullOrEmpty(req)) Then
                    Integer.TryParse(req, _itemsPerPage)
                    Me.ItemsPerPage = _itemsPerPage
                End If


                req = Request.QueryString("sorting")
                Dim _cbSort As Integer
                If (Not String.IsNullOrEmpty(req)) Then
                    Integer.TryParse(req, _cbSort)
                    If (_cbSort >= 0 AndAlso _cbSort < cbSort.Items.Count) Then
                        cbSort.SelectedIndex = _cbSort
                    End If
                End If


                req = Request.QueryString("seo" & Me.Pager.ClientID)
                Dim currentPage As Integer
                If (Not String.IsNullOrEmpty(req)) Then
                    Try
                        req = req.Replace("page", "")
                        Integer.TryParse(req, currentPage)
                        Me.Pager.ItemsPerPage = Me.ItemsPerPage
                        Me.Pager.ItemCount = Me.ItemsPerPage * currentPage
                        If (Me.Pager.ItemCount > 0) Then Me.Pager.PageIndex = currentPage - 1
                    Catch ex As Exception
                    End Try
                End If

            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try


        Try

            If (Not Page.IsPostBack) Then
                LoadLAG()
                LoadViews()

                ' if user has paid to unlock an offer, execute unlock command
                If (SessionVariables.UserUnlockInfo.IsUnlocked AndAlso SessionVariables.UserUnlockInfo.IsValid()) Then
                    ExecCmd(OfferControlCommandEnum.UNLOCKOFFER, SessionVariables.UserUnlockInfo.OfferId.ToString())
                End If
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub

    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender

        Try
            Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try


        Try

            Dim url As String = Request.Url.AbsolutePath
            url = url & "?"

            Dim seo As String = ("seo" & Me.Pager.ClientID).ToUpper()
            For Each itm As String In Request.QueryString.AllKeys
                If (Not String.IsNullOrEmpty(itm)) Then

                    Dim s As String = itm.ToUpper()
                    If (s <> seo AndAlso s <> "ITEMSPERPAGE" AndAlso s <> "SORTING") Then
                        url = url & itm & "=" & HttpUtility.UrlEncode(Request.QueryString(itm)) & "&"
                    End If

                End If
            Next

            Me.Pager.SeoFriendly = True
            Me.Pager.SeoNavigateUrlFormatString = url & "seo" & Me.Pager.ClientID & "={0}&itemsPerPage=" & Me.ItemsPerPage & "&sorting=" & cbSort.SelectedIndex
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub



    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
        LoadViews()
    End Sub



    Protected Sub LoadLAG()
        Try

            lnkNew.Text = CurrentPageData.GetCustomString("lnkNew_ND")
            lnkPending.Text = CurrentPageData.GetCustomString("lnkPending")
            lnkRejected.Text = CurrentPageData.GetCustomString("lnkRejected")
            lnkAccepted.Text = CurrentPageData.GetCustomString("lnkAccepted")
            'lnkNew.Text = "<span></span>" & CurrentPageData.GetCustomString("lnkNew")
            'lnkPending.Text = "<span></span>" & CurrentPageData.GetCustomString("lnkPending")
            'lnkRejected.Text = "<span></span>" & CurrentPageData.GetCustomString("lnkRejected")
            'lnkAccepted.Text = "<span></span>" & CurrentPageData.GetCustomString("lnkAccepted")

            cbSort.Items.Clear()
            cbSort.Items.Add(CurrentPageData.GetCustomString("cbSort_OfferAmountHighest"), "Highest")
            cbSort.Items.Add(CurrentPageData.GetCustomString("cbSort_OfferAmountLowest"), "Lowest")
            cbSort.Items.Add(CurrentPageData.GetCustomString("cbSort_RecentOffers"), "Recent")
            cbSort.Items(cbSort.Items.Count - 1).Selected = True
            cbSort.Items.Add(CurrentPageData.GetCustomString("cbSort_OldestOffers"), "Oldest")

            Try
                If (Not Page.IsPostBack) Then
                    Me.OffersSorting = OffersSortEnum.RecentOffers

                    Dim req As String = Request.QueryString("sorting")
                    Dim _cbSort As Integer
                    If (Not String.IsNullOrEmpty(req)) Then
                        Integer.TryParse(req, _cbSort)
                        If (_cbSort >= 0 AndAlso _cbSort < cbSort.Items.Count) Then
                            cbSort.SelectedIndex = _cbSort
                        End If
                    End If

                End If
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try

            lblSortByText.Text = CurrentPageData.GetCustomString(lblSortByText.ID)
            lblResultsPerPageText.Text = CurrentPageData.GetCustomString(lblResultsPerPageText.ID)

            'TODO CMS
            'imgItems.ImageUrl = "~/images/members_bar/4.png"
            lblItemsName.Text = Me.CurrentPageData.GetCustomString("CartOffersView")
            lnkViewDescription.Text = Me.CurrentPageData.GetCustomString("CartOffersWhatIsIt")
            WhatIsIt1.PopupControl.HeaderText = Me.CurrentPageData.GetCustomString("CartOffersView")
            lnkViewDescription.OnClientClick = OnMoreInfoClickFunc(
                lnkViewDescription.OnClientClick,
                ResolveUrl("~/Members/InfoWin.aspx?info=offers"),
                Me.CurrentPageData.GetCustomString("CartOffersView"))

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Private Sub LoadViews()

        Try
            Me.OffersSorting = OffersSortEnum.RecentOffers
            Me.ItemsPerPage = 10

            If (Not Page.IsPostBack) Then
                If (Request.QueryString("vw") IsNot Nothing) Then
                    If (Request.QueryString("vw").ToUpper() = "PENDING") Then
                        mvOfferMain.SetActiveView(vwPendingOffers)
                    ElseIf (Request.QueryString("vw").ToUpper() = "REJECTED") Then
                        mvOfferMain.SetActiveView(vwRejectedOffers)
                    ElseIf (Request.QueryString("vw").ToUpper() = "ACCEPTED") Then
                        mvOfferMain.SetActiveView(vwAcceptedOffers)
                    ElseIf (Request.QueryString("vw").ToUpper() = "NEWOFFERS") Then
                        mvOfferMain.SetActiveView(vwNewOffers)
                    Else
                        mvOfferMain.SetActiveView(vwNewOffers)
                    End If
                Else
                    mvOfferMain.SetActiveView(vwNewOffers)
                End If
            Else
                BindSearchResults()
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


    Protected Sub mvOfferMain_ActiveViewChanged(sender As Object, e As EventArgs) Handles mvOfferMain.ActiveViewChanged
        BindSearchResults()
    End Sub



    Protected Sub Pager_PageIndexChanged(sender As Object, e As EventArgs) Handles Pager.PageIndexChanged
        BindSearchResults()
    End Sub


    Protected Sub ddlSort_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbSort.SelectedIndexChanged
        Me.Pager.PageIndex = 0
        BindSearchResults()
    End Sub

    Protected Sub ddlPerPage_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbPerPage.SelectedIndexChanged
        'Me.Pager.PageIndex = 0
        'BindSearchResults()
        Try
            clsProfilesPrivacySettings.Update_ItemsPerPage(Me.MasterProfileId, Me.ItemsPerPage)
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorSendEmail(ex, "")
        End Try
        Try
            Me.Pager.PageIndex = 0
            BindSearchResults()
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(ex, "")
        End Try
    End Sub

    Protected Sub offersRepeater_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs)
        ExecCmd(e.CommandName, e.CommandArgument)
    End Sub




    Private Sub ExecCmd(CommandName As String, CommandArgument As String)

        Dim success = False
        '  Dim redirected = False
        Dim showNoPhotoNote = False

        Try
            Dim currentUserHasPhotos As Boolean = clsUserDoes.HasPhotos(Me.MasterProfileId, Me.MirrorProfileId)
            If (currentUserHasPhotos) Then

                ''''''''''''''''''''''
                'photo aware actions
                ''''''''''''''''''''''
                If (CommandName = OfferControlCommandEnum.WINK.ToString()) Then
                    If (Not String.IsNullOrEmpty(CommandArgument)) Then

                        Dim ToProfileID As Integer
                        Integer.TryParse(CommandArgument.ToString(), ToProfileID)

                        ''clsUserDoes.SendWink(ToProfileID, Me.MasterProfileId)
                        ''success = True

                        'Dim MaySendWink As Boolean = True

                        'If (Me.IsFemale) Then
                        '    If (Me.IsReferrer) Then
                        '        MaySendWink = clsUserDoes.IsReferrerAllowedSendWink(ToProfileID, Me.MasterProfileId)
                        '    End If
                        'End If

                        'If (MaySendWink) Then
                        '    clsUserDoes.SendWink(ToProfileID, Me.MasterProfileId)
                        'Else
                        '    Dim popupMessage As String = WhatIsIt.OnMoreInfoClickFunc(ResolveUrl("~/Members/InfoWin.aspx?info=Error_Sending_Receiver_DoNot_Have_Photo"), "Error! Can't send", 650, 350)
                        '    popupMessage = "function fnError_Sending_Receiver_DoNot_Have_Photo(){" & popupMessage & "};"
                        '    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Error_Sending_Receiver_DoNot_Have_Photo", popupMessage, True)
                        'End If
                        Dim MaySendWink As Boolean = True
                        Dim MaySendWink_CheckSetting As Boolean = True


                        MaySendWink_CheckSetting = Not clsProfilesPrivacySettings.GET_DisableReceivingLIKESFromDifferentCountryFromTo(ToProfileID, Me.MasterProfileId)
                        If (MaySendWink_CheckSetting = False) Then
                            MaySendWink = False
                        End If


                        If (MaySendWink AndAlso Me.IsFemale) Then
                            If (Me.IsReferrer) Then
                                MaySendWink = clsUserDoes.IsReferrerAllowedSendWink(ToProfileID, Me.MasterProfileId)
                            End If
                        End If

                        If (MaySendWink) Then
                            clsUserDoes.SendWink(ToProfileID, Me.MasterProfileId)
                        Else
                            If (MaySendWink_CheckSetting = False) Then
                                Dim popupMessage As String = WhatIsIt.OnMoreInfoClickFunc(ResolveUrl("~/Members/InfoWin.aspx?info=Error_Sending_Receiver_Disabled_Likes_From_Diff_Country&popup=popupWhatIs"), "Error! Can't send", 650, 350)
                                popupMessage = "function fnError_Sending_Receiver_DoNot_Have_Photo(){" & popupMessage & "};"
                                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Error_Sending_Receiver_DoNot_Have_Photo", popupMessage, True)
                            Else
                                Dim popupMessage As String = WhatIsIt.OnMoreInfoClickFunc(ResolveUrl("~/Members/InfoWin.aspx?info=Error_Sending_Receiver_DoNot_Have_Photo&popup=popupWhatIs"), "Error! Can't send", 650, 350)
                                popupMessage = "function fnError_Sending_Receiver_DoNot_Have_Photo(){" & popupMessage & "};"
                                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Error_Sending_Receiver_DoNot_Have_Photo", popupMessage, True)
                            End If
                        End If
                        success = MaySendWink

                    End If
                ElseIf (CommandName = OfferControlCommandEnum.POKE.ToString()) Then
                    If (Not String.IsNullOrEmpty(CommandArgument)) Then

                        Dim cmdParams As String() = CommandArgument.Split("-")


                        Dim offerid As Integer
                        Dim OtherMemberProfileID As Integer

                        If (cmdParams.Length > 0) Then
                            Dim offeridStr As String = cmdParams(0).Split("_")(1)
                            Integer.TryParse(offeridStr, offerid)
                        End If
                        If (cmdParams.Length > 1) Then
                            Dim profStr As String = cmdParams(1).Split("_")(1)
                            Integer.TryParse(profStr, OtherMemberProfileID)
                        End If

                        Dim parms As clsUserDoes.NewOfferParameters = Nothing

                        Dim offrRec As EUS_Offer = clsUserDoes.GetMyPendingPoke(OtherMemberProfileID, Me.MasterProfileId)
                        If (offrRec Is Nothing) Then
                            parms = New clsUserDoes.NewOfferParameters()
                            parms.offerAmount = -1
                            parms.parentOfferId = offerid
                            parms.userIdReceiver = -1
                            parms.userIdWhoDid = Me.MasterProfileId

                            clsUserDoes.SendPoke(parms)
                        End If

                        success = True
                    End If
                ElseIf (CommandName = OfferControlCommandEnum.TRYWINK.ToString()) Then

                    Dim offerID As Integer
                    Integer.TryParse(CommandArgument.ToString(), offerID)
                    'clsUserDoes.ResendWinkOrOffer(offerID)
                    'success = True

                ElseIf (CommandName = OfferControlCommandEnum.UNWINK.ToString()) Then

                    Dim ToProfileID As Integer
                    Integer.TryParse(CommandArgument.ToString(), ToProfileID)

                    clsUserDoes.CancelPendingWink(ToProfileID, Me.MasterProfileId)
                    'success = True

                ElseIf (CommandName = OfferControlCommandEnum.TRYCREATEOFFER.ToString()) Then

                    Dim offerID As Integer
                    Integer.TryParse(CommandArgument.ToString(), offerID)
                    'clsUserDoes.DeleteOffer(offerID)
                    'success = True


                ElseIf (CommandName = OfferControlCommandEnum.FAVORITE.ToString()) Then

                    Dim _userId As Integer = CType(CommandArgument, Integer)
                    If (_userId > 0) Then
                        clsUserDoes.MarkAsFavorite(_userId, Me.MasterProfileId)

                        success = True
                    End If

                ElseIf (CommandName = OfferControlCommandEnum.OFFERACCEPT.ToString()) Then

                    Dim offerID As Integer
                    Integer.TryParse(CommandArgument.ToString(), offerID)
                    clsUserDoes.AcceptOffer(offerID, OfferStatusEnum.ACCEPTED)

                    'mvOfferMain.SetActiveView(vwAcceptedOffers)
                    Dim acceptedUrl As String = ResolveUrl("~/Members/Offers3.aspx?vw=accepted")
                    Response.Redirect(acceptedUrl, True)
                    HttpContext.Current.ApplicationInstance.CompleteRequest()

                    'success = True
                    '   redirected = True
                ElseIf (CommandName = OfferControlCommandEnum.OFFERCOUNTER.ToString()) Then

                    Dim offerID As Integer
                    Integer.TryParse(CommandArgument.ToString(), offerID)

                    Dim CreateOfferUrl As String = ResolveUrl("~/Members/CreateOffer.aspx?counter=" & offerID)
                    Response.Redirect(CreateOfferUrl, True)
                    HttpContext.Current.ApplicationInstance.CompleteRequest()
                    'clsUserDoes.CounterOffer(offerID)
                    'success = True

                ElseIf (CommandName = OfferControlCommandEnum.UNLOCKOFFER.ToString()) Then


                    Dim offerID As Integer
                    Integer.TryParse(CommandArgument.ToString(), offerID)

                    If (AllowUnlimited) Then
                        success = clsUserDoes.PerformOfferUnlock(offerID, Me.MasterProfileId)
                    Else
                        success = True
                    End If


                    ' success = false 
                    '   means that user has no credits available, 
                    '   and should be redirected to products page
                    '   PerformOfferUnlock, performs redirection without ending response.


                    If (success AndAlso offerID > 0) Then
                        ' if the offer was a poke
                        Using cmdb As CMSDBDataContext = Me.GetCMSDBDataContext


                            Dim pokeOffer As EUS_Offer = (From itm In cmdb.EUS_Offers
                                                             Where itm.OfferID = offerID AndAlso itm.OfferTypeID = ProfileHelper.OfferTypeID_POKE AndAlso itm.StatusID = ProfileHelper.OfferStatusID_PENDING
                                                            Select itm).SingleOrDefault()
                            If (pokeOffer IsNot Nothing) Then
                                pokeOffer.StatusID = ProfileHelper.OfferStatusID_ACCEPTED
                                cmdb.SubmitChanges()

                                Dim AcceptedOffersUrl As String = ResolveUrl("~/Members/Offers3.aspx?vw=ACCEPTED")
                                Response.Redirect(AcceptedOffersUrl, True)
                                HttpContext.Current.ApplicationInstance.CompleteRequest()

                            End If
                        End Using
                    End If


                ElseIf (CommandName = OfferControlCommandEnum.REJECTBAD.ToString() OrElse _
                              CommandName = OfferControlCommandEnum.REJECTEXPECTATIONS.ToString() OrElse _
                              CommandName = OfferControlCommandEnum.REJECTFAR.ToString() OrElse _
                              CommandName = OfferControlCommandEnum.REJECTTYPE.ToString()) Then

                    ''''''''''''''''''''''
                    'not photo aware action
                    ''''''''''''''''''''''

                    Dim offerID As Integer
                    Integer.TryParse(CommandArgument.ToString(), offerID)

                    clsUserDoes.RejectOffer(offerID, CommandName.ToString())

                    success = True
                End If

            Else
                showNoPhotoNote = True
            End If


            If (CommandName = OfferControlCommandEnum.DELETEOFFER.ToString() OrElse _
                CommandName = OfferControlCommandEnum.CANCELOFFER.ToString() OrElse _
                CommandName = OfferControlCommandEnum.CANCELPOKE.ToString()) Then
                ''''''''''''''''''''''
                'not photo aware action
                ''''''''''''''''''''''

                Dim offerID As Integer
                Integer.TryParse(CommandArgument.ToString(), offerID)

                clsUserDoes.DeleteOfferAndParents(offerID)

                success = True
                showNoPhotoNote = False

            ElseIf (CommandName = OfferControlCommandEnum.CANCELWINK.ToString()) Then
                ''''''''''''''''''''''
                'not photo aware action
                ''''''''''''''''''''''

                Dim offerID As Integer
                Integer.TryParse(CommandArgument.ToString(), offerID)
                clsUserDoes.CancelWinkOrOffer(offerID)
                success = True
                showNoPhotoNote = False

            End If

        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try



        Try
            'offersControl.FillKeyStrings()
            If (showNoPhotoNote) Then
                Response.Redirect("~/Members/Photos.aspx", True)
                HttpContext.Current.ApplicationInstance.CompleteRequest()

            ElseIf (success) Then
                SessionVariables.UserUnlockInfo.Reset()
                BindSearchResults()
            End If
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub



    Private Sub BindSearchResults()

        If (mvOfferMain.GetActiveView().Equals(vwNewOffers)) Then

            liNew.Attributes.Add("class", "down")

            liAccepted.Attributes.Remove("class")
            liPending.Attributes.Remove("class")
            liRejected.Attributes.Remove("class")

            newOffers.InfoWinHeaderText = CurrentPageData.GetCustomString("lnkNew")
            newOffers.InfoWinUrl = ResolveUrl("~/Members/InfoWin.aspx?info=OffersNewCommands")

            BindNewOffers()

        ElseIf (mvOfferMain.GetActiveView().Equals(vwPendingOffers)) Then

            liPending.Attributes.Add("class", "down")

            liNew.Attributes.Remove("class")
            liAccepted.Attributes.Remove("class")
            liRejected.Attributes.Remove("class")

            pendingOffers.InfoWinHeaderText = CurrentPageData.GetCustomString("lnkPending")
            pendingOffers.InfoWinUrl = ResolveUrl("~/Members/InfoWin.aspx?info=OffersPendingCommands")

            BindPendingOffers()

        ElseIf (mvOfferMain.GetActiveView().Equals(vwRejectedOffers)) Then

            liRejected.Attributes.Add("class", "down")

            liNew.Attributes.Remove("class")
            liAccepted.Attributes.Remove("class")
            liPending.Attributes.Remove("class")

            BindRejectedOffers()

        ElseIf (mvOfferMain.GetActiveView().Equals(vwAcceptedOffers)) Then

            liAccepted.Attributes.Add("class", "down")

            liNew.Attributes.Remove("class")
            liPending.Attributes.Remove("class")
            liRejected.Attributes.Remove("class")

            BindAcceptedOffers()

        End If

    End Sub



    Private Sub BindNewOffers()
        Try

            divFilter.Visible = False
            newOffers.ShowEmptyListText = True

            If (Me.GetCurrentProfile(True) IsNot Nothing) Then

                ' bind new offers
                Try
                    Dim countRows As Integer
                    Dim performCount As Boolean = True
                    Dim NumberOfRecordsToReturn As Integer = Me.GetRecordsToRetrieve()

                    Dim parms As New clsWinksHelperParameters()
                    parms.CurrentProfileId = Me.MasterProfileId
                    parms.sorting = Me.OffersSorting
                    parms.ReturnRecordsWithStatus = ProfileStatusEnum.Approved
                    parms.zipstr = Me.SessionVariables.MemberData.Zip
                    parms.latitudeIn = Me.SessionVariables.MemberData.latitude
                    parms.longitudeIn = Me.SessionVariables.MemberData.longitude
                    parms.Distance = clsSearchHelper.DISTANCE_DEFAULT
                    'parms.NumberOfRecordsToReturn = NumberOfRecordsToReturn
                    parms.performCount = True
                    parms.rowNumberMin = NumberOfRecordsToReturn - Me.ItemsPerPage
                    parms.rowNumberMax = NumberOfRecordsToReturn


                    Dim ds As DataSet = clsSearchHelper.GetNewOffersDataTable(parms)
                    newOffers.FillKeyStrings()
                    If (performCount) Then
                        countRows = ds.Tables(0).Rows(0)(0)
                        If (countRows > 0) Then
                            Dim dt As DataTable = ds.Tables(1)
                            FillOfferControlList(dt, newOffers)
                            newOffers.ShowEmptyListText = False
                            divFilter.Visible = True
                        End If
                    Else
                        countRows = ds.Tables(0).Rows.Count
                        If (countRows > 0) Then
                            Dim dt As DataTable = ds.Tables(0)
                            FillOfferControlList(dt, newOffers)
                            newOffers.ShowEmptyListText = False
                            divFilter.Visible = True
                        End If
                    End If


                    newOffers.DataBind()
                    SetPager(countRows)

                    'Dim dt As DataTable = clsSearchHelper.GetNewOffersDataTable(Me.MasterProfileId,
                    '                                                            ProfileStatusEnum.Approved,
                    '                                                            Me.OffersSorting,
                    '                                                            Me.SessionVariables.MemberData.Zip,
                    '                                                            Me.SessionVariables.MemberData.latitude,
                    '                                                            Me.SessionVariables.MemberData.longitude)

                    'If (dt.Rows.Count > 0) Then
                    '    FillOfferControlList(dt, newOffers)
                    '    newOffers.ShowEmptyListText = False
                    '    divFilter.Visible = True
                    'End If

                    'newOffers.DataBind()
                    'SetPager(dt.Rows.Count)

                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "")

                    SetPager(0)
                    newOffers.UsersList = Nothing
                    newOffers.DataBind()
                End Try
            End If

            If (Me.Pager.ItemCount = 0 AndAlso Me.GetPhotosCount() = 0) Then

                Try

                    newOffers.ShowNoPhotoText = True
                    newOffers.ShowEmptyListText = False
                    newOffers.DataBind()

                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "")
                Finally
                    SetPager(0)
                End Try

            End If


        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


    Private Sub BindPendingOffers()
        Try

            divFilter.Visible = False
            pendingOffers.ShowEmptyListText = True

            If (Me.GetCurrentProfile(True) IsNot Nothing) Then

                ' bind new offers
                Try
                    Dim countRows As Integer
                    Dim performCount As Boolean = True
                    Dim NumberOfRecordsToReturn As Integer = Me.GetRecordsToRetrieve()

                    Dim parms As New clsWinksHelperParameters()
                    parms.CurrentProfileId = Me.MasterProfileId
                    parms.sorting = Me.OffersSorting
                    parms.ReturnRecordsWithStatus = ProfileStatusEnum.Approved
                    parms.zipstr = Me.SessionVariables.MemberData.Zip
                    parms.latitudeIn = Me.SessionVariables.MemberData.latitude
                    parms.longitudeIn = Me.SessionVariables.MemberData.longitude
                    parms.Distance = clsSearchHelper.DISTANCE_DEFAULT
                    'parms.NumberOfRecordsToReturn = NumberOfRecordsToReturn
                    parms.performCount = True
                    parms.rowNumberMin = NumberOfRecordsToReturn - Me.ItemsPerPage
                    parms.rowNumberMax = NumberOfRecordsToReturn


                    Dim ds As DataSet = clsSearchHelper.GetPendingOffersDataTable(parms)
                    pendingOffers.FillKeyStrings()
                    If (performCount) Then
                        countRows = ds.Tables(0).Rows(0)(0)

                        If (countRows > 0) Then
                            Dim dt As DataTable = ds.Tables(1)
                            FillOfferControlList(dt, pendingOffers)
                            pendingOffers.ShowEmptyListText = False
                            divFilter.Visible = True
                        End If
                    Else
                        countRows = ds.Tables(0).Rows.Count
                        If (countRows > 0) Then
                            Dim dt As DataTable = ds.Tables(0)
                            FillOfferControlList(dt, pendingOffers)
                            pendingOffers.ShowEmptyListText = False
                            divFilter.Visible = True
                        End If
                    End If


                    pendingOffers.DataBind()
                    SetPager(countRows)

                    'Dim dt As DataTable = clsSearchHelper.GetPendingOffersDataTable(Me.MasterProfileId,
                    '                                                                ProfileStatusEnum.Approved,
                    '                                                                Me.OffersSorting,
                    '                                                                Me.SessionVariables.MemberData.Zip,
                    '                                                                Me.SessionVariables.MemberData.latitude,
                    '                                                                Me.SessionVariables.MemberData.longitude)

                    'If (dt.Rows.Count > 0) Then
                    '    FillOfferControlList(dt, pendingOffers)
                    '    pendingOffers.ShowEmptyListText = False
                    '    divFilter.Visible = True
                    'End If

                    'pendingOffers.DataBind()
                    'SetPager(dt.Rows.Count)

                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "")

                    SetPager(0)
                    pendingOffers.UsersList = Nothing
                    pendingOffers.DataBind()
                End Try
            End If

            If (Me.Pager.ItemCount = 0 AndAlso Me.GetPhotosCount() = 0) Then

                Try

                    pendingOffers.ShowNoPhotoText = True
                    pendingOffers.ShowEmptyListText = False
                    pendingOffers.DataBind()

                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "")
                Finally
                    SetPager(0)
                End Try

            End If


        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


    Private Sub BindRejectedOffers()
        Try

            divFilter.Visible = False
            rejectedOffers.ShowEmptyListText = True

            If (Me.GetCurrentProfile(True) IsNot Nothing) Then

                ' bind new offers
                Try
                    Dim countRows As Integer
                    Dim performCount As Boolean = True
                    Dim NumberOfRecordsToReturn As Integer = Me.GetRecordsToRetrieve()

                    Dim parms As New clsWinksHelperParameters()
                    parms.CurrentProfileId = Me.MasterProfileId
                    parms.sorting = Me.OffersSorting
                    parms.ReturnRecordsWithStatus = ProfileStatusEnum.Approved
                    parms.zipstr = Me.SessionVariables.MemberData.Zip
                    parms.latitudeIn = Me.SessionVariables.MemberData.latitude
                    parms.longitudeIn = Me.SessionVariables.MemberData.longitude
                    parms.Distance = clsSearchHelper.DISTANCE_DEFAULT
                    'parms.NumberOfRecordsToReturn = NumberOfRecordsToReturn
                    parms.performCount = True
                    parms.rowNumberMin = NumberOfRecordsToReturn - Me.ItemsPerPage
                    parms.rowNumberMax = NumberOfRecordsToReturn


                    Dim ds As DataSet = clsSearchHelper.GetRejectedOffersDataTable(parms)
                    rejectedOffers.FillKeyStrings()
                    If (performCount) Then
                        countRows = ds.Tables(0).Rows(0)(0)
                        If (countRows > 0) Then
                            Dim dt As DataTable = ds.Tables(1)
                            FillOfferControlList(dt, rejectedOffers)
                            rejectedOffers.ShowEmptyListText = False
                            divFilter.Visible = True
                        End If
                    Else
                        countRows = ds.Tables(0).Rows.Count
                        If (countRows > 0) Then
                            Dim dt As DataTable = ds.Tables(0)
                            FillOfferControlList(dt, rejectedOffers)
                            rejectedOffers.ShowEmptyListText = False
                            divFilter.Visible = True
                        End If
                    End If


                    rejectedOffers.DataBind()
                    SetPager(countRows)

                    'Dim dt As DataTable = clsSearchHelper.GetRejectedOffersDataTable(Me.MasterProfileId,
                    '                                                                 ProfileStatusEnum.Approved,
                    '                                                                 Me.OffersSorting,
                    '                                                                 Me.SessionVariables.MemberData.Zip,
                    '                                                                Me.SessionVariables.MemberData.latitude,
                    '                                                                Me.SessionVariables.MemberData.longitude)

                    'If (dt.Rows.Count > 0) Then
                    '    FillOfferControlList(dt, rejectedOffers)
                    '    rejectedOffers.ShowEmptyListText = False
                    '    divFilter.Visible = True
                    'End If

                    'rejectedOffers.DataBind()
                    'SetPager(dt.Rows.Count)

                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "")

                    SetPager(0)
                    rejectedOffers.UsersList = Nothing
                    rejectedOffers.DataBind()
                End Try
            End If

            If (Me.Pager.ItemCount = 0 AndAlso Me.GetPhotosCount() = 0) Then

                Try

                    rejectedOffers.ShowNoPhotoText = True
                    rejectedOffers.ShowEmptyListText = False
                    rejectedOffers.DataBind()

                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "")
                Finally
                    SetPager(0)
                End Try

            End If


        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


    Private Sub BindAcceptedOffers()
        Try

            divFilter.Visible = False
            acceptedOffers.ShowEmptyListText = True

            If (Me.GetCurrentProfile(True) IsNot Nothing) Then


                ' bind new offers
                Try
                    Dim countRows As Integer
                    Dim performCount As Boolean = True
                    Dim NumberOfRecordsToReturn As Integer = Me.GetRecordsToRetrieve()

                    Dim parms As New clsWinksHelperParameters()
                    parms.CurrentProfileId = Me.MasterProfileId
                    parms.sorting = Me.OffersSorting
                    parms.ReturnRecordsWithStatus = ProfileStatusEnum.Approved
                    parms.zipstr = Me.SessionVariables.MemberData.Zip
                    parms.latitudeIn = Me.SessionVariables.MemberData.latitude
                    parms.longitudeIn = Me.SessionVariables.MemberData.longitude
                    parms.Distance = clsSearchHelper.DISTANCE_DEFAULT
                    'parms.NumberOfRecordsToReturn = NumberOfRecordsToReturn
                    parms.performCount = True
                    parms.rowNumberMin = NumberOfRecordsToReturn - Me.ItemsPerPage
                    parms.rowNumberMax = NumberOfRecordsToReturn


                    Dim ds As DataSet = clsSearchHelper.GetAcceptedOffersDataTable(parms)
                    acceptedOffers.FillKeyStrings()
                    If (performCount) Then
                        countRows = ds.Tables(0).Rows(0)(0)
                        If (countRows > 0) Then
                            Dim dt As DataTable = ds.Tables(1)
                            FillOfferControlList(dt, acceptedOffers)
                            acceptedOffers.ShowEmptyListText = False
                            divFilter.Visible = True
                        End If
                    Else
                        countRows = ds.Tables(0).Rows.Count
                        If (countRows > 0) Then
                            Dim dt As DataTable = ds.Tables(0)
                            FillOfferControlList(dt, acceptedOffers)
                            acceptedOffers.ShowEmptyListText = False
                            divFilter.Visible = True
                        End If
                    End If


                    acceptedOffers.DataBind()
                    SetPager(countRows)

                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "")

                    SetPager(0)
                    acceptedOffers.UsersList = Nothing
                    acceptedOffers.DataBind()
                End Try
            End If

            If (Me.Pager.ItemCount = 0 AndAlso Me.GetPhotosCount() = 0) Then

                Try

                    acceptedOffers.ShowNoPhotoText = True
                    acceptedOffers.ShowEmptyListText = False
                    acceptedOffers.DataBind()

                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "")
                Finally
                    SetPager(0)
                End Try

            End If


        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub

    Private Sub FillOfferControlList(dt As DataTable, offersControl As Dating.Server.Site.Web.OfferControl3)

        ' set pagination
        'Dim startingIndex As Integer = 0
        'Dim endIndex As Integer = Me.ItemsPerPage - 1
        'If (Me.Pager.PageIndex > 0) Then
        '    startingIndex = Me.ItemsPerPage * Me.Pager.PageIndex
        '    endIndex = (startingIndex + Me.ItemsPerPage) - 1
        'End If


        Dim drDefaultPhoto As EUS_CustomerPhotosRow = DataHelpers.GetProfilesDefaultPhoto(Me.MasterProfileId)
        Dim uliCurrentMember As New clsWinkUserListItem()
        uliCurrentMember.ProfileID = Me.MasterProfileId ' 
        uliCurrentMember.MirrorProfileID = Me.MirrorProfileId
        uliCurrentMember.LoginName = Me.GetCurrentProfile().LoginName
        uliCurrentMember.Genderid = Me.GetCurrentProfile().GenderId
        uliCurrentMember.ProfileViewUrl = ResolveUrl("~/Members/Profile.aspx?p=" & HttpUtility.UrlEncode(Me.GetCurrentProfile().LoginName))

        If (drDefaultPhoto IsNot Nothing) Then
            uliCurrentMember.ImageFileName = drDefaultPhoto.FileName
            uliCurrentMember.ImageUploadDate = drDefaultPhoto.DateTimeToUploading.ToLocalTime().ToString("d MMM, yyyy H:mm")
        Else
            uliCurrentMember.ImageUploadDate = Nothing
            uliCurrentMember.ImageFileName = ""
        End If

        uliCurrentMember.ImageUrl = ProfileHelper.GetProfileImageURL(Me.MasterProfileId, uliCurrentMember.ImageFileName, Me.SessionVariables.MemberData.GenderId, True, Me.IsHTTPS, PhotoSize.D150)
        uliCurrentMember.ImageThumbUrl = uliCurrentMember.ImageUrl




        Dim rowsCount As Integer = 0
        For Each dr As DataRow In dt.Rows

            Try

                'If (rowsCount >= startingIndex AndAlso rowsCount <= endIndex) Then

                Dim uli As New clsWinkUserListItem()
                uli.LAGID = Session("LAGID")
                uli.ProfileID = uliCurrentMember.ProfileID
                uli.MirrorProfileID = uliCurrentMember.MirrorProfileID
                uli.LoginName = uliCurrentMember.LoginName
                uli.Genderid = uliCurrentMember.Genderid
                uli.ProfileViewUrl = uliCurrentMember.ProfileViewUrl
                uli.ImageFileName = uliCurrentMember.ImageFileName
                uli.ImageUrl = uliCurrentMember.ImageUrl
                uli.ImageThumbUrl = uliCurrentMember.ImageThumbUrl
                uli.ImageUploadDate = uliCurrentMember.ImageUploadDate

                If (dt.Columns.Contains("Birthday")) Then uli.OtherMemberBirthday = If(Not dr.IsNull("Birthday"), dr("Birthday"), Nothing)
                uli.OtherMemberLoginName = dr("LoginName")
                uli.OtherMemberProfileID = dr("ProfileID")
                uli.OtherMemberMirrorProfileID = dr("MirrorProfileID")
                uli.OtherMemberCity = dr("City")
                uli.OtherMemberRegion = IIf(Not dr.IsNull("Region"), dr("Region"), "")
                uli.OtherMemberCountry = IIf(Not dr.IsNull("Country"), dr("Country"), "")
                uli.OtherMemberGenderid = dr("Genderid")


                If (Not dr.IsNull("Birthday")) Then
                    uli.OtherMemberAge = ProfileHelper.GetCurrentAge(dr("Birthday"))
                Else
                    uli.OtherMemberAge = "020"
                End If


                If (Not dr.IsNull("FileName")) Then
                    uli.OtherMemberImageFileName = dr("FileName")
                Else
                    uli.OtherMemberImageFileName = ""
                End If

                'uli.OtherMemberImageUrl = ProfileHelper.GetProfileImageURL(uli.OtherMemberProfileID, uli.OtherMemberImageFileName, uli.OtherMemberGenderid, True, Me.IsHTTPS, PhotoSize.D150)
                If (dt.Columns.Contains("HasPhoto")) Then
                    If (Not dr.IsNull("HasPhoto") AndAlso (dr("HasPhoto") = True OrElse dr("HasPhoto") = 1)) Then
                        ' user has photo
                        If (uli.OtherMemberImageFileName IsNot Nothing) Then
                            'has public photos
                            uli.OtherMemberImageUrl = ProfileHelper.GetProfileImageURL(uli.OtherMemberProfileID, uli.OtherMemberImageFileName, uli.OtherMemberGenderid, True, Me.IsHTTPS, PhotoSize.D150)

                        Else
                            'has private photos
                            uli.OtherMemberImageUrl = ProfileHelper.GetPrivateImageURL(uli.OtherMemberGenderid)

                        End If
                    Else
                        ' has no photo
                        uli.OtherMemberImageUrl = ProfileHelper.GetDefaultImageURL(uli.OtherMemberGenderid)

                    End If
                    If IsHTTPS AndAlso uli.OtherMemberImageUrl.StartsWith("http:") Then uli.OtherMemberImageUrl = "https" & uli.OtherMemberImageUrl.Remove(0, "http".Length)
                Else
                    uli.OtherMemberImageUrl = ProfileHelper.GetProfileImageURL(uli.OtherMemberProfileID, uli.OtherMemberImageFileName, uli.OtherMemberGenderid, True, Me.IsHTTPS, PhotoSize.D150)
                End If
                uli.OtherMemberImageThumbUrl = uli.OtherMemberImageUrl

                uli.OtherMemberProfileViewUrl = ResolveUrl("~/Members/Profile.aspx?p=" & HttpUtility.UrlEncode(dr("LoginName")))

                uli.Distance = dr("distance")

                If (uli.Distance < gCarDistance) Then
                    uli.DistanceCss = "distance_car"
                Else
                    uli.DistanceCss = "distance_plane"
                End If

                If (dt.Columns.Contains("CommunicationUnl")) Then
                    If (Not dr.IsNull("CommunicationUnl") AndAlso (dr("CommunicationUnl") > 0) OrElse dr("CommunicationUnl") = True) Then
                        uli.HasCommunication = True
                    Else
                        uli.HasCommunication = False
                    End If
                End If

                If (dt.Columns.Contains("IsMessageSent")) Then
                    If (Not dr.IsNull("IsMessageSent") AndAlso (dr("IsMessageSent") > 0) OrElse dr("IsMessageSent") = True) Then
                        uli.IsMessageSent = True
                    Else
                        uli.IsMessageSent = False
                    End If
                End If


                '' CHECK OFFER CASES
                uli.OfferID = dr("OfferID")
                Dim offerRecVals As New OfferRecordValues()
                offerRecVals.OfferID = dr("OfferID")
                offerRecVals.OfferTypeID = IIf(Not dr.IsNull("OffersOfferTypeID"), dr("OffersOfferTypeID"), 0)
                offerRecVals.OffersStatusID = IIf(Not dr.IsNull("OffersStatusID"), dr("OffersStatusID"), 0)
                offerRecVals.OffersFromProfileID = IIf(Not dr.IsNull("OffersFromProfileID"), dr("OffersFromProfileID"), 0)
                offerRecVals.OffersToProfileID = IIf(Not dr.IsNull("OffersToProfileID"), dr("OffersToProfileID"), 0)

                uli.OfferAmount = IIf(Not dr.IsNull("OffersAmount"), dr("OffersAmount"), 0)

                uli.SendMessageUrl = ResolveUrl("~/Members/Conversation.aspx?p=" & HttpUtility.UrlEncode(uli.OtherMemberLoginName) & "&offerid=" & uli.OfferID)
                uli.SendMessageUrlOnce = ResolveUrl("~/Members/Conversation.aspx?send=once&p=" & HttpUtility.UrlEncode(uli.OtherMemberLoginName) & "&offerid=" & uli.OfferID)
                uli.SendMessageUrlMany = ResolveUrl("~/Members/Conversation.aspx?send=unl&p=" & HttpUtility.UrlEncode(uli.OtherMemberLoginName) & "&offerid=" & uli.OfferID)

                If (offerRecVals.OfferTypeID = ProfileHelper.OfferTypeID_WINK AndAlso offerRecVals.OffersStatusID = ProfileHelper.OfferStatusID_PENDING) Then

                    If (offerRecVals.OffersFromProfileID = Me.MasterProfileId) Then
                        '''''''''''''''''''
                        ' wink pending
                        '''''''''''''''''''

                        uli.IsWink = True
                        uli.AllowCancelWink = True
                        uli.YourWinkSentTextNew = offersControl.MykeyStrings.YourWinkSentText

                    ElseIf (offerRecVals.OffersToProfileID = Me.MasterProfileId) Then
                        '''''''''''''''''''
                        ' wink new received
                        '''''''''''''''''''

                        uli.IsWink = True

                        uli.YouReceivedWinkOfferTextNew = offersControl.MykeyStrings.YouReceivedWinkOfferText.Replace("###LOGINNAME###", uli.OtherMemberLoginName)

                        If (ProfileHelper.IsMale(uli.Genderid)) Then
                            uli.AllowActionsMenu = True
                            LoadPokeView(offersControl, uli)
                        Else
                            uli.AllowPoke = True
                            LoadWinkView(offersControl, uli)
                        End If

                    End If

                ElseIf (offerRecVals.OfferTypeID = ProfileHelper.OfferTypeID_POKE AndAlso offerRecVals.OffersStatusID = ProfileHelper.OfferStatusID_PENDING) Then

                    If (offerRecVals.OffersFromProfileID = Me.MasterProfileId) Then
                        '''''''''''''''''''
                        ' new offer pending
                        '''''''''''''''''''

                        uli.AllowCancelPoke = True
                        uli.YourWinkSentTextNew = offersControl.MykeyStrings.YourPokeSentText

                    ElseIf (offerRecVals.OffersToProfileID = Me.MasterProfileId) Then
                        '''''''''''''''''''
                        ' new poke received
                        '''''''''''''''''''

                        uli.IsPoke = True
                        uli.AllowActionsMenu = True
                        ' uli.YouReceivedWinkOfferText = offersControl.CurrentPageData.GetCustomString("YouReceivedPokeText")
                        uli.YouReceivedWinkOfferTextNew = offersControl.MykeyStrings.YouReceivedPokeText.Replace("###LOGINNAME###", uli.OtherMemberLoginName)

                        LoadPokeView(offersControl, uli)
                    End If

                ElseIf (offerRecVals.OfferTypeID = ProfileHelper.OfferTypeID_OFFERNEW AndAlso offerRecVals.OffersStatusID = ProfileHelper.OfferStatusID_PENDING) Then

                    If (offerRecVals.OffersFromProfileID = Me.MasterProfileId) Then
                        '''''''''''''''''''
                        ' new offer pending
                        '''''''''''''''''''

                        uli.AllowCancelPendingOffer = True
                        uli.YourWinkSentTextNew = offersControl.MykeyStrings.YourOfferSentText

                        If (Me.IsMale) Then
                            uli.AllowActionsMenu = True
                            uli.AllowActionsSendMessageOnce = True
                            uli.AllowActionsSendMessageMany = True
                        End If
                    ElseIf (offerRecVals.OffersToProfileID = Me.MasterProfileId) Then
                        '''''''''''''''''''
                        ' new offer received
                        '''''''''''''''''''

                        uli.AllowAccept = True
                        uli.AllowCounter = True
                        uli.YouReceivedWinkOfferTextNew = offersControl.MykeyStrings.YouReceivedOfferText

                    End If

                ElseIf (offerRecVals.OfferTypeID = ProfileHelper.OfferTypeID_OFFERCOUNTER AndAlso (offerRecVals.OffersStatusID = ProfileHelper.OfferStatusID_REPLACINGBYCOUNTER OrElse offerRecVals.OffersStatusID = ProfileHelper.OfferStatusID_COUNTER)) Then

                    If (offerRecVals.OffersFromProfileID = Me.MasterProfileId) Then
                        '''''''''''''''''''
                        ' counter offer pending
                        '''''''''''''''''''

                        uli.AllowCancelPendingOffer = True
                        uli.YourWinkSentTextNew = offersControl.MykeyStrings.YourOfferSentText


                    ElseIf (offerRecVals.OffersToProfileID = Me.MasterProfileId) Then
                        '''''''''''''''''''
                        ' counter offer received
                        '''''''''''''''''''
                        uli.AllowAccept = True
                        uli.AllowCounter = True
                        uli.YouReceivedWinkOfferTextNew = offersControl.MykeyStrings.YouReceivedOfferText
                    End If

                ElseIf (offerRecVals.OffersStatusID = ProfileHelper.OfferStatusID_CANCELED) Then

                    If (offerRecVals.OfferTypeID = ProfileHelper.OfferTypeID_WINK) Then

                        If (offerRecVals.OffersFromProfileID = Me.MasterProfileId) Then
                            '''''''''''''''''''
                            ' a wink canceled by current user
                            '''''''''''''''''''
                            ''uli.AllowTryWink = True
                            uli.AllowDeleteWink = True
                            uli.CancelledRejectedTitleTextNew = offersControl.MykeyStrings.WinkCancelledText
                            uli.CancelledRejectedDescriptionText = uli.ReplaceCommonTookens(offersControl.MykeyStrings.YouCancelledWinkToText)
                  
                        End If
                    End If

                    If (offerRecVals.OfferTypeID = ProfileHelper.OfferTypeID_OFFERNEW OrElse offerRecVals.OfferTypeID = ProfileHelper.OfferTypeID_OFFERCOUNTER) Then
                        If (offerRecVals.OffersFromProfileID = Me.MasterProfileId) Then
                            '''''''''''''''''''
                            ' an offer canceled by current user
                            '''''''''''''''''''
                            ''uli.AllowCreateOffer = True
                            uli.AllowDeleteOffer = True
                            uli.CancelledRejectedTitleTextNew = offersControl.MykeyStrings.OfferCancelledText
                            uli.CancelledRejectedDescriptionText = uli.ReplaceCommonTookens(offersControl.MykeyStrings.YouCancelledOfferToText)
                        End If
                    End If

                ElseIf (offerRecVals.OffersStatusID = ProfileHelper.OfferStatusID_REJECTED OrElse _
                        offerRecVals.OffersStatusID = ProfileHelper.OfferStatusID_REJECTBAD OrElse _
                        offerRecVals.OffersStatusID = ProfileHelper.OfferStatusID_REJECTEXPECTATIONS OrElse _
                        offerRecVals.OffersStatusID = ProfileHelper.OfferStatusID_REJECTFAR OrElse _
                       offerRecVals.OffersStatusID = ProfileHelper.OfferStatusID_REJECTTYPE) Then

                    LoadRejectView(offersControl, uli, offerRecVals)


                ElseIf (offerRecVals.OfferTypeID = ProfileHelper.OfferTypeID_OFFERCOUNTER AndAlso offerRecVals.OffersStatusID = ProfileHelper.OfferStatusID_PENDING) Then

                    If (offerRecVals.OffersFromProfileID = Me.MasterProfileId) Then
                        '''''''''''''''''''
                        ' new offer counter pending
                        '''''''''''''''''''


                    ElseIf (offerRecVals.OffersToProfileID = Me.MasterProfileId) Then
                        '''''''''''''''''''
                        ' new offer counter received
                        '''''''''''''''''''

                    End If

                ElseIf (offerRecVals.OffersStatusID = ProfileHelper.OfferStatusID_ACCEPTED) OrElse (offerRecVals.OffersStatusID = ProfileHelper.OfferStatusID_OFFER_ACCEEPTED_WITH_MESSAGE) Then
                    'OrElse OffersStatusID = OfferStatusID_UNLOCKED
                    '''''''''''''''''''
                    ' offer accepted
                    '''''''''''''''''''

                    If (offerRecVals.OfferTypeID = ProfileHelper.OfferTypeID_POKE) Then
                        '''''''''''''''''''
                        ' it's a poke was accepted
                        '''''''''''''''''''


                        If (offerRecVals.OffersFromProfileID = Me.MasterProfileId) Then
                            '''''''''''''''''''
                            ' other user accepted an offer of current user
                            '''''''''''''''''''
                            uli.OfferAcceptedWithAmountText = uli.ReplaceCommonTookens(offersControl.MykeyStrings.OtherMemberPokeAcceptedText)
                            uli.ItsCurrentProfileAction = True
                        Else
                            '''''''''''''''''''
                            ' current user accepted an offer
                            ''''''''''''''''''
                            uli.OfferAcceptedWithAmountText = uli.ReplaceCommonTookens(offersControl.MykeyStrings.CurrentMemberPokeAcceptedText)

                        End If

                        uli.AllowSendMessage = True
                        uli.AllowDeleteAcceptedOffer = True

                    Else


                        If (offerRecVals.OffersFromProfileID = Me.MasterProfileId) Then
                            '''''''''''''''''''
                            ' other user accepted an offer of current user
                            '''''''''''''''''''

                            ' uli.OfferAcceptedWithAmountText = offersControl.CurrentPageData.GetCustomString("OtherMemberOfferAcceptedWithAmountText")
                            uli.OfferAcceptedWithAmountText = uli.ReplaceCommonTookens(offersControl.MykeyStrings.OtherMemberOfferAcceptedWithAmountText)
                            uli.ItsCurrentProfileAction = True
                        Else
                            '''''''''''''''''''
                            ' current user accepted an offer
                            '''''''''''''''''''

                            '   uli.OfferAcceptedWithAmountText = offersControl.CurrentPageData.GetCustomString("CurrentMemberOfferAcceptedWithAmountText")
                            uli.OfferAcceptedWithAmountText = uli.ReplaceCommonTookens(offersControl.MykeyStrings.CurrentMemberOfferAcceptedWithAmountText)

                        End If

                        If (uli.HasCommunication Is Nothing) Then uli.HasCommunication = clsUserDoes.HasCommunication(uli.OtherMemberProfileID, Me.MasterProfileId)
                        If (Not uli.HasCommunication) Then
                            ' If (OffersStatusID =  ProfileHelper.OfferStatusID_ACCEPTED) Then

                            '''''''''''''''''''
                            ' communication is locked
                            '''''''''''''''''''

                            If (Me.IsMale) Then
                                'uli.AllowOfferAcceptedUnlock = True
                                'uli.AllowTooltipUnlockNotice = True
                                uli.Credits = ProfileHelper.Config_UNLOCK_CONVERSATION_CREDITS

                                'If (ProfileHelper.IsMale(uli.Genderid)) Then
                                'End If
                                uli.AllowActionsMenu = True
                                uli.AllowActionsSendMessageOnce = True
                                uli.AllowActionsSendMessageMany = True
                            Else
                                uli.AllowSendMessage = True
                                If (uli.IsMessageSent Is Nothing) Then uli.IsMessageSent = clsUserDoes.IsMessageSent(uli.OtherMemberProfileID, Me.MasterProfileId)
                            End If

                            uli.AllowDeleteAcceptedOffer = True
                        Else

                            'If (OffersStatusID = OfferStatusID_UNLOCKED) Then
                            '''''''''''''''''''
                            ' communication is unlocked
                            '''''''''''''''''''

                            uli.AllowSendMessage = True
                            uli.AllowDeleteAcceptedOffer = True
                        End If

                    End If
                End If


                uli.IsOffer = (offerRecVals.OfferTypeID = ProfileHelper.OfferTypeID_OFFERNEW OrElse offerRecVals.OfferTypeID = ProfileHelper.OfferTypeID_OFFERCOUNTER)

                If (uli.IsOffer) Then
                    uli.AllowAccept = True
                End If


                ' counter
                uli.AllowCounter = Not (offerRecVals.OfferTypeID = ProfileHelper.OfferTypeID_WINK OrElse offerRecVals.OfferTypeID = ProfileHelper.OfferTypeID_POKE)

                If (offersControl.Equals(newOffers) OrElse offersControl.Equals(pendingOffers)) Then
                    uli.AllowTooltipPopupLikes = True
                Else
                    uli.AllowTooltipPopupLikes = False
                End If

                If (Me.SessionVariables.MemberData.Status = ProfileStatusEnum.LIMITED) Then
                    uli.AllowUnblock = False
                    uli.AllowUnfavorite = False
                    uli.AllowFavorite = False
                    uli.AllowActionsMenu = False
                    uli.AllowSendMessage = False
                    uli.AllowWink = False
                    uli.AllowUnWink = False
                End If

                offersControl.UsersList.Add(uli)
                'End If
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try

            rowsCount += 1
        Next
    End Sub


    Sub SetPager(itemsCount As Integer)
        Me.Pager.ItemCount = itemsCount
        Me.Pager.ItemsPerPage = Me.ItemsPerPage
        Me.Pager.Visible = (itemsCount > Me.ItemsPerPage)

        If (Me.Pager.PageIndex = -1) AndAlso (itemsCount > 0) Then
            Me.Pager.PageIndex = 0
        End If
    End Sub

    Function GetRecordsToRetrieve() As Integer
        Me.Pager.ItemsPerPage = Me.ItemsPerPage

        If (Me.Pager.PageIndex = -1) AndAlso (Me.Pager.ItemCount > 0) Then
            Me.Pager.PageIndex = 0
        End If

        If (Me.Pager.PageIndex > 0) Then
            Return (Me.Pager.ItemsPerPage * (Me.Pager.PageIndex + 1))
        End If
        Return Me.Pager.ItemsPerPage
    End Function

    Private Sub LoadPokeView(offersControl As Dating.Server.Site.Web.OfferControl3, uli As clsWinkUserListItem)

        If (uli.HasCommunication Is Nothing) Then uli.HasCommunication = clsUserDoes.HasCommunication(uli.OtherMemberProfileID, Me.MasterProfileId)

        If (Not uli.HasCommunication) Then
            uli.AllowActionsUnlock = True
            uli.AllowActionsCreateOffer = True

            '''''''''''''''''''
            ' communication is locked
            '''''''''''''''''''

            Dim rec As EUS_Offer = clsUserDoes.GetLastOffer(uli.OtherMemberProfileID, Me.MasterProfileId)

            If (rec Is Nothing) Then
                uli.CreateOfferUrl =
                        ResolveUrl("~/Members/CreateOffer.aspx?offer=" & uli.OfferID)

            End If

            'If (ProfileHelper.IsGenerous(Me.GetCurrentProfile().AccountTypeId)) Then
            If (Me.IsMale) Then
                uli.AllowOfferAcceptedUnlock = True
                uli.Credits = ProfileHelper.Config_UNLOCK_CONVERSATION_CREDITS
            End If
        Else

            uli.AllowActionsSendMessage = True
            uli.AllowActionsDeleteOffer = True

        End If
    End Sub

    Private Sub LoadWinkView(offersControl As OfferControl3, uli As clsWinkUserListItem)

        Try

            If (ProfileHelper.IsFemale(uli.OtherMemberGenderid)) Then
                ' like apo gynaika
                ' uli.WantToKnowFirstDatePriceText = offersControl.MykeyStrings.FEMALE_WantToKnowFirstDatePriceText
                uli.WantToKnowFirstDatePriceTextNew = offersControl.MykeyStrings.FEMALE_WantToKnowFirstDatePriceText.Replace("###LOGINNAME###", uli.OtherMemberLoginName)
            Else
                ' like apo andra
                '  uli.WantToKnowFirstDatePriceText = offersControl.MykeyStrings.MALE_WantToKnowFirstDatePriceText
                uli.WantToKnowFirstDatePriceTextNew = offersControl.MykeyStrings.MALE_WantToKnowFirstDatePriceText.Replace("###LOGINNAME###", uli.OtherMemberLoginName)
            End If

        Catch ex As Exception

        End Try

        'If (drDefaultPhoto Is Nothing) Then
        '    uli.AllowCreateOffer = True

        '    uli.CreateOfferUrl =
        '        ResolveUrl("~/Members/Information.aspx") & _
        '        "?vw=NOPHOTO"
        'Else

        Dim rec As EUS_Offer = clsUserDoes.GetLastOffer(uli.OtherMemberProfileID, Me.MasterProfileId)

        If (rec Is Nothing) Then
            uli.AllowCreateOffer = True
            uli.CreateOfferUrl =
                    ResolveUrl("~/Members/CreateOffer.aspx") & _
                    ("?offer=" & uli.OfferID) ' & _
            '("&offerto=" & HttpUtility.UrlEncode(uli.OtherMemberLoginName)) & _
            '("&returnurl=" & HttpUtility.UrlEncode(Request.Url.AbsoluteUri))

        End If
        'End If   
    End Sub


    Private Sub LoadRejectView(ByRef offersControl As OfferControl3, ByRef uli As clsWinkUserListItem, ByRef offerRecVals As OfferRecordValues)

        If (offerRecVals.OfferTypeID = ProfileHelper.OfferTypeID_WINK) Then
            '''''''''''''''''''
            ' wink rejected
            '''''''''''''''''''


            If (offerRecVals.OffersFromProfileID = Me.MasterProfileId) Then
                '''''''''''''''''''
                ' wink rejected by other-receiver user, the user who sent is viewing
                '''''''''''''''''''
                uli.AllowDeleteOffer = True

                uli.CancelledRejectedTitleTextNew = offersControl.MykeyStrings.WinkREJECTEDText
                uli.CancelledRejectedDescriptionText = uli.ReplaceCommonTookens(offersControl.MykeyStrings.YourWinkREJECTEDReasonText)

                uli.RejectedByProfileID = offerRecVals.OffersToProfileID

            ElseIf (offerRecVals.OffersToProfileID = Me.MasterProfileId) Then
                '''''''''''''''''''
                ' wink rejected by current-receiver user, the user who rejected is viewing
                '''''''''''''''''''
                uli.AllowDeleteOffer = True
                uli.CancelledRejectedTitleTextNew = offersControl.MykeyStrings.WinkREJECTEDText
                uli.CancelledRejectedDescriptionText = uli.ReplaceCommonTookens(offersControl.MykeyStrings.YourWinkREJECTEDReasonText)

                uli.CancelledRejectedDescriptionText = uli.ReplaceCommonTookens(uli.CancelledRejectedDescriptionText)

                uli.RejectedByProfileID = Me.MasterProfileId
            End If

        ElseIf (offerRecVals.OfferTypeID = ProfileHelper.OfferTypeID_POKE) Then
            '''''''''''''''''''
            ' poke rejected
            '''''''''''''''''''


            If (offerRecVals.OffersFromProfileID = Me.MasterProfileId) Then
                '''''''''''''''''''
                ' wink rejected by other-receiver user, the user who sent is viewing
                '''''''''''''''''''
                uli.AllowDeleteOffer = True

                uli.CancelledRejectedTitleTextNew = offersControl.MykeyStrings.PokeREJECTEDText
                uli.CancelledRejectedDescriptionText = uli.ReplaceCommonTookens(offersControl.MykeyStrings.YourPokeREJECTEDReasonText)

                uli.RejectedByProfileID = offerRecVals.OffersToProfileID

            ElseIf (offerRecVals.OffersToProfileID = Me.MasterProfileId) Then
                '''''''''''''''''''
                ' wink rejected by current-receiver user, the user who rejected is viewing
                '''''''''''''''''''
                uli.AllowDeleteOffer = True

                uli.CancelledRejectedTitleTextNew = offersControl.MykeyStrings.PokeREJECTEDText
                uli.CancelledRejectedDescriptionText = uli.ReplaceCommonTookens(offersControl.MykeyStrings.YourPokeREJECTEDReasonText)

                uli.CancelledRejectedDescriptionText = uli.ReplaceCommonTookens(uli.CancelledRejectedDescriptionText)

                uli.RejectedByProfileID = Me.MasterProfileId
            End If

        ElseIf (offerRecVals.OfferTypeID = ProfileHelper.OfferTypeID_OFFERNEW OrElse offerRecVals.OfferTypeID = ProfileHelper.OfferTypeID_OFFERCOUNTER) Then
            '''''''''''''''''''
            ' an offer rejected
            '''''''''''''''''''


            If (offerRecVals.OffersFromProfileID = Me.MasterProfileId) Then
                '''''''''''''''''''
                ' an offer rejected by othen-receiver user
                '''''''''''''''''''

                ''uli.AllowTryWink = True
                ''uli.AllowCreateOffer = True
                uli.AllowDeleteOffer = True

                uli.CancelledRejectedTitleTextNew = offersControl.MykeyStrings.OfferREJECTEDText
                uli.CancelledRejectedDescriptionText = offersControl.MykeyStrings.YourOfferREJECTEDReasonText
                If (Not String.IsNullOrEmpty(uli.CancelledRejectedDescriptionText)) Then
                    uli.CancelledRejectedDescriptionText = uli.CancelledRejectedDescriptionText.Replace("###LOGINNAME###", uli.OtherMemberLoginName)
                    uli.CancelledRejectedDescriptionText = uli.CancelledRejectedDescriptionText.Replace("###AMOUNT###", "&euro;" & uli.OfferAmount.ToString())
                End If
                uli.RejectedByProfileID = offerRecVals.OffersToProfileID

            ElseIf (offerRecVals.OffersToProfileID = Me.MasterProfileId) Then
                '''''''''''''''''''
                ' an offer rejected by current-receiver user
                '''''''''''''''''''

                'uli.AllowTryWink = True
                'uli.AllowCreateOffer = True
                uli.AllowDeleteOffer = True


                uli.CancelledRejectedTitleTextNew = offersControl.MykeyStrings.OfferREJECTEDText
                uli.CancelledRejectedDescriptionText = offersControl.MykeyStrings.YourOfferREJECTEDReasonText
                uli.CancelledRejectedDescriptionText = uli.ReplaceCommonTookens(uli.CancelledRejectedDescriptionText)

                uli.RejectedByProfileID = Me.MasterProfileId
            End If
        End If


        If (Not String.IsNullOrEmpty(uli.CancelledRejectedDescriptionText)) Then
            Select Case offerRecVals.OffersStatusID
                Case ProfileHelper.OfferStatusID_REJECTBAD
                    uli.CancelledRejectedDescriptionText = uli.CancelledRejectedDescriptionText.Replace("###REJECTIONREASON###", offersControl.MykeyStrings.NotEnoughInfoText)
                Case ProfileHelper.OfferStatusID_REJECTEXPECTATIONS
                    uli.CancelledRejectedDescriptionText = uli.CancelledRejectedDescriptionText.Replace("###REJECTIONREASON###", offersControl.MykeyStrings.DifferentExpectationsText)
                Case ProfileHelper.OfferStatusID_REJECTFAR
                    uli.CancelledRejectedDescriptionText = uli.CancelledRejectedDescriptionText.Replace("###REJECTIONREASON###", offersControl.MykeyStrings.TooFarAwayText)
                Case ProfileHelper.OfferStatusID_REJECTTYPE
                    uli.CancelledRejectedDescriptionText = uli.CancelledRejectedDescriptionText.Replace("###REJECTIONREASON###", offersControl.MykeyStrings.NotInterestedText)
                Case Else
                    uli.CancelledRejectedDescriptionText = uli.CancelledRejectedDescriptionText.Replace("###REJECTIONREASON###", "")
            End Select
        End If
    End Sub


    Class OfferRecordValues
        Property OfferID As Integer
        Property OfferTypeID As Integer
        Property OffersStatusID As Integer
        Property OffersFromProfileID As Integer
        Property OffersToProfileID As Integer
    End Class


End Class

﻿<%@ Page Title="" Language="vb" AutoEventWireup="true" MasterPageFile="~/Members/Members2Inner.Master" 
    MaintainScrollPositionOnPostback="true"
    CodeBehind="Likes.aspx.vb" Inherits="Dating.Server.Site.Web.Likes" %>

<%@ Register src="~/UserControls/LikesControl.ascx" tagname="LikesControl" tagprefix="uc2" %>
<%@ Register src="~/UserControls/MemberLeftPanel.ascx" tagname="MemberLeftPanel" tagprefix="uc1" %>
<%@ Register src="../UserControls/WhatIsIt.ascx" tagname="WhatIsIt" tagprefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script type="text/javascript">
    function scrollToResultsTop() {
    scrollWin('#<%= divFilter.ClientID%>', -200, 500);
}
</script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">

<div id="likes-list">

<div id="likes-list-top">

    <div id="top-links-wrap">
    <table class="center-table" cellpadding="0" cellspacing="0">
        <tr>
            <td>
<ul id="top-links">
    <li runat="server" id="liLikes"><asp:HyperLink ID="lnkLikes" runat="server" 
                                        NavigateUrl="?vw=LIKES" onclick="ShowLoading();" 
                                        CssClass="btn list-btn lnk83"><span></span>Likes</asp:HyperLink></li>
    <li runat="server" id="liPending"><asp:HyperLink ID="lnkPending" runat="server" 
                                        NavigateUrl="?vw=PENDING" onclick="ShowLoading();" 
                                        CssClass="btn list-btn lnk112"><span></span>Pending</asp:HyperLink></li>
    <li runat="server" id="liRejected"><asp:HyperLink ID="lnkRejected" runat="server" 
                                        NavigateUrl="?vw=REJECTED" onclick="ShowLoading();" 
                                        CssClass="btn list-btn lnk112"><span></span>Rejected</asp:HyperLink></li>
    <li runat="server" id="liAccepted"><asp:HyperLink ID="lnkAccepted" runat="server" 
                                        NavigateUrl="?vw=ACCEPTED" onclick="ShowLoading();" 
                                        CssClass="btn list-btn lnk112"><span></span>Accepted</asp:HyperLink></li>
</ul>
<div class="list-arrow"></div>
<div class="clear"></div>
            </td>
        </tr>
    </table>
    </div>

    <div class="list-title">
        <asp:Literal ID="lblItemsName" runat="server" />
        <asp:LinkButton ID="lnkViewDescription" runat="server" CssClass="more_info2" OnClientClick="OnMoreInfoClick('[contentUrl]', this, '[headerText]');return false;" />
    </div>


    <div id="divFilter" runat="server" clientidmode="Static" class="likes-filters">
        <div class="lfloat cr-word">
            <dx:ASPxLabel ID="lblSortByText" runat="server" Text="" EncodeHtml="False">
            </dx:ASPxLabel>
        </div>
        <div class="lfloat">
            <dx:ASPxComboBox ID="cbSort" runat="server" class="update_uri" 
                AutoPostBack="True" EncodeHtml="False">
                <ClientSideEvents SelectedIndexChanged="ShowLoadingOnMenu" />
            </dx:ASPxComboBox>
        </div>
        <div class="rfloat">
            <dx:ASPxComboBox ID="cbPerPage" runat="server" class="update_uri input-small" style="width: 60px;" 
                AutoPostBack="True" EncodeHtml="False">
                <ClientSideEvents SelectedIndexChanged="ShowLoadingOnMenu" />
                <Items>
                    <dx:ListEditItem Text="10" Value="10" Selected="true"  />
                    <dx:ListEditItem Text="25" Value="25" />
                    <dx:ListEditItem Text="50" Value="50" />
                </Items>
            </dx:ASPxComboBox>
        </div>
        <div class="rfloat cr-word">
            <dx:ASPxLabel ID="lblResultsPerPageText" runat="server" Text="" EncodeHtml="False">
            </dx:ASPxLabel>
        </div>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>

</div>

<script type="text/javascript">
(function () {
    var itms = $("a", "#top-links").css("width", "auto");
    for (var c = 0; c < itms.length; c++) {
        var w = $(itms[c]).width();
        $(itms[c]).css("width", "").removeClass("lnk83").removeClass("lnk112").removeClass("lnk127").removeClass("lnk161")
        if (w < 73)
            $(itms[c]).addClass("lnk83");
        else if (w < 100)
            $(itms[c]).addClass("lnk112");
        else if (w < 115)
            $(itms[c]).addClass("lnk127");
        else
            $(itms[c]).addClass("lnk161");
    }

    try {
        var el = $("a", "#top-links .down");
        var arr = $(".list-arrow", "#top-links-wrap");
        var p1 = el.position().left;
        p1 = (p1 + (el.width() / 2)) - (arr.width() / 2);
        arr.css("left", p1 + "px");
    }
    catch (e) { }
})();
</script>

<asp:UpdatePanel ID="updOffers" runat="server" RenderMode="Inline">
<ContentTemplate>
    <div class="l_wrap">
<asp:MultiView ID="mvOfferMain" runat="server" ActiveViewIndex="-1">

<asp:View ID="vwLikes" runat="server"> 
    <uc2:LikesControl ID="likesOffers" runat="server" UsersListView="LikesOffers"/>
</asp:View>

<asp:View ID="vwPendingOffers" runat="server">
    <uc2:LikesControl ID="pendingOffers" runat="server" UsersListView="PendingOffers"/>
</asp:View>

<asp:View ID="vwRejectedOffers" runat="server">
    <uc2:LikesControl ID="rejectedOffers" runat="server" UsersListView="RejectedOffers"/>
</asp:View>

<asp:View ID="vwAcceptedOffers" runat="server">
    <uc2:LikesControl ID="acceptedOffers" runat="server" UsersListView="AcceptedOffers"/>
</asp:View>
</asp:MultiView>
    </div>
    <div class="pagination">
<table align="center">
    <tr>
        <td><dx:ASPxPager ID="Pager" runat="server" ItemCount="3" ItemsPerPage="1" 
                RenderMode="Lightweight" CssFilePath="~/App_Themes/Aqua/{0}/styles.css" 
                CssPostfix="Aqua" CurrentPageNumberFormat="{0}" 
                SpriteCssFilePath="~/App_Themes/Aqua/{0}/sprite.css"
                PageNumberFormat="<span onclick='ShowLoading();'>{0}</span>"
                EncodeHtml="false"
                SeoFriendly="Enabled">
                <lastpagebutton visible="True">
                </lastpagebutton>
                <firstpagebutton visible="True">
                </firstpagebutton>
                <Summary Position="Left" Text="Page {0} of {1} ({2} members)" Visible="false" />
            </dx:ASPxPager></td>
    </tr>
</table>
    </div>

</ContentTemplate>
</asp:UpdatePanel>

</div>


<script type="text/javascript">
    jQuery(function ($) {
        scrollToLogin();
    });

    (function () {
        if (typeof _runScripts_LikesControl !== 'undefined') {
            Sys.WebForms.PageRequestManager.getInstance().remove_endRequest(_runScripts_LikesControl);
            _runScripts_LikesControl();
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(_runScripts_LikesControl);
        }
    })();
</script>

<uc2:whatisit ID="WhatIsIt1" runat="server" />
</asp:Content>


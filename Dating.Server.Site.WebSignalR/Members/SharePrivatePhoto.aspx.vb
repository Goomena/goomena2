﻿Imports Dating.Server.Core.DLL
Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Datasets.DLL.DSMembers

Public Class SharePrivatePhoto
    Inherits BasePage

#Region "Props"


    'Dim _pageData As clsPageData
    'Protected ReadOnly Property CurrentPageData As clsPageData
    '    Get
    '        If (_pageData Is Nothing) Then _pageData = New clsPageData(Context)
    '        Return _pageData
    '    End Get
    'End Property


    Protected Property UserIdInView As Integer
        Get
            Return ViewState("UserIdInView")
        End Get
        Set(value As Integer)
            ViewState("UserIdInView") = value
        End Set
    End Property

    Protected Property RequestedLevel As Integer
        Get
            Return ViewState("RequestedLevel")
        End Get
        Set(value As Integer)
            ViewState("RequestedLevel") = value
        End Set
    End Property


    Protected Property AvailableLevel As Integer
        Get
            Return ViewState("AvailableLevel")
        End Get
        Set(value As Integer)
            ViewState("AvailableLevel") = value
        End Set
    End Property

    Protected Property PopupTitle As String
        Get
            Return ViewState("PopupTitle")
        End Get
        Set(value As String)
            ViewState("PopupTitle") = value
        End Set
    End Property

#End Region


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            If (Not Me.IsPostBack) Then
                LoadLAG()
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try


        Try

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub



    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
    End Sub


    Protected Sub LoadLAG()
        Try
            Dim LEVEL As String = Request("lvl")
            Dim GENDER As String = Request("g")
            Dim LOGIN As String = Request("p")
            Dim sLOGINID As String = Request("id")

            If (Not String.IsNullOrEmpty(LEVEL)) Then Integer.TryParse(LEVEL, RequestedLevel)
            If (Not String.IsNullOrEmpty(sLOGINID)) Then Integer.TryParse(sLOGINID, UserIdInView)



            Dim fileName As String = ""
            If (GENDER = "1") Then
                fileName = "menlevel" & LEVEL & ".png"
            Else
                fileName = "womenlevel" & LEVEL & ".png"
                'pnlSharePhotoWomen.Visible = True
            End If

            Dim OpacityLevelSet As Integer = 0
            Dim phtLvl As EUS_ProfilePhotosLevel = GetEUS_ProfilePhotosLevel_Available()
            If (phtLvl IsNot Nothing AndAlso phtLvl.PhotoLevelID > 0) Then
                OpacityLevelSet = phtLvl.PhotoLevelID
                AvailableLevel = phtLvl.PhotoLevelID
                'For cnt = 1 To phtLvl.PhotoLevelID
                '    Dim tdIndLevel As HtmlTableCell = AppUtils.FindControlIterative(tblSharePhotoLevels, "tdIndLevel" & cnt)
                '    tdIndLevel.Attributes.Add("class", "levels-ind-available")
                'Next
            End If

            'For cnt = OpacityLevelSet + 1 To 10
            '    Dim tdWmnLevel As HtmlGenericControl = AppUtils.FindControlIterative(tblSharePhotoLevels, "tdWmnLevel" & cnt)
            '    tdWmnLevel.Attributes("class") = tdWmnLevel.Attributes("class") & " off"
            'Next

            lblInfo.Text = CurrentPageData.GetCustomString("Blocked_Private_Photo_LevelDesc4")
            lblInfoTitle.Text = CurrentPageData.GetCustomString("Blocked_Private_Photo_LevelTitle2")

            If (OpacityLevelSet > 0) Then
                'lblInfoFooter.Text = CurrentPageData.GetCustomString("Blocked_Private_Photo_LevelDesc4_Private")

                lblInfo.Text = lblInfo.Text.
                                           Replace("[FOOTER-CONTENT]", CurrentPageData.GetCustomString("Blocked_Private_Photo_LevelDesc4_Private")).
                                           Replace("[FILENAME]", fileName).
                                           Replace("[LOGIN_NAME]", LOGIN).
                                           Replace("[LEVEL]", LEVEL).
                                           Replace("[CURRENT_LEVEL]", OpacityLevelSet)

                'lblInfoFooter.Text = lblInfoFooter.Text.
                '                           Replace("[FILENAME]", fileName).
                '                           Replace("[LOGIN_NAME]", LOGIN).
                '                           Replace("[LEVEL]", LEVEL).
                '                           Replace("[CURRENT_LEVEL]", OpacityLevelSet)


                PopupTitle = CurrentPageData.GetCustomString("Private.Photo.Level.of.Levels")
                PopupTitle = PopupTitle.Replace("[LEVEL]", LEVEL)
                PopupTitle = PopupTitle.Replace("[CURRENT_LEVEL]", OpacityLevelSet)

            Else

                'lblInfoFooter.Text = CurrentPageData.GetCustomString("Blocked_Private_Photo_LevelDesc4_Public")

                Dim public1 As String = ProfileHelper.GetPhotosDisplayLevelString(OpacityLevelSet, Me.GetLag())
                lblInfo.Text = lblInfo.Text.
                                           Replace("[FOOTER-CONTENT]", CurrentPageData.GetCustomString("Blocked_Private_Photo_LevelDesc4_Public")).
                                           Replace("[FILENAME]", fileName).
                                           Replace("[LOGIN_NAME]", LOGIN).
                                           Replace("[LEVEL]", LEVEL).
                                           Replace("[CURRENT_LEVEL]", public1)

                'lblInfoFooter.Text = lblInfoFooter.Text.
                '                           Replace("[FILENAME]", fileName).
                '                           Replace("[LOGIN_NAME]", LOGIN).
                '                           Replace("[LEVEL]", LEVEL).
                '                           Replace("[CURRENT_LEVEL]", public1)


                PopupTitle = CurrentPageData.GetCustomString("Private.Photo.Level.of.Levels")
                PopupTitle = PopupTitle.Replace("[LEVEL]", LEVEL)
                PopupTitle = PopupTitle.Replace("[CURRENT_LEVEL]", public1)

            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender

        Try
            Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub



    Private Function GetEUS_ProfilePhotosLevel_Available() As EUS_ProfilePhotosLevel
        Dim phtLvl As EUS_ProfilePhotosLevel = Nothing
        Try
            Using cmdb As CMSDBDataContext = Me.GetCMSDBDataContext
                phtLvl = (From itm In cmdb.EUS_ProfilePhotosLevels
                                                Where itm.FromProfileID = Me.UserIdInView AndAlso itm.ToProfileID = Me.MasterProfileId
                                                Select itm).FirstOrDefault()
            End Using
         

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
        Return phtLvl
    End Function


End Class
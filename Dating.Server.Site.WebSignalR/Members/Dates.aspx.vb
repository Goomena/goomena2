﻿Imports Dating.Server.Core.DLL
Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Datasets.DLL.DSMembers

Public Class Dates
    Inherits BasePage


#Region "Props"

    'Dim _pageData As clsPageData
    'Protected ReadOnly Property CurrentPageData As clsPageData
    '    Get
    '        If (_pageData Is Nothing) Then
    '            _pageData = New clsPageData(Context)
    '            AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
    '        End If
    '        Return _pageData
    '    End Get
    'End Property


    Public Property DatesSort As DatesSortEnum

        Get
            Dim sorting As DatesSortEnum = DatesSortEnum.NewDates

            Dim index As Integer = cbSort.SelectedIndex

            If (index = 0) Then
                sorting = DatesSortEnum.NewDates
            ElseIf (index = 1) Then
                sorting = DatesSortEnum.ByDate
                'ElseIf (index = 2) Then
                '    sorting = DatesSortEnum.ByStatus
            ElseIf (index = 2) Then
                sorting = DatesSortEnum.ByLastDating
            End If

            Return sorting
        End Get

        Set(value As DatesSortEnum)
            Dim index As Integer = 0

            If (value = DatesSortEnum.NewDates) Then
                index = 0
            ElseIf (value = DatesSortEnum.ByDate) Then
                index = 1
                'ElseIf (value = DatesSortEnum.ByStatus) Then
                '    index = 2
            ElseIf (value = DatesSortEnum.ByLastDating) Then
                index = 2
            End If

            cbSort.SelectedIndex = index
        End Set

    End Property


    Public Property ItemsPerPage As Integer

        Get
            Dim perPageNumber As Integer = 10

            Dim index As Integer = cbPerPage.SelectedIndex
            If (index = 0) Then
                perPageNumber = 10
            ElseIf (index = 1) Then
                perPageNumber = 25
            ElseIf (index = 2) Then
                perPageNumber = 50
            End If

            Return perPageNumber
        End Get

        Set(value As Integer)
            Dim index As Integer = 0

            If (value = 25) Then
                index = 1
            ElseIf (value = 50) Then
                index = 2
            Else
                index = 0
            End If

            cbPerPage.SelectedIndex = index
        End Set

    End Property


    Private ReadOnly Property CurrentOffersControl As DatesControl
        Get

            If (mvOfferMain.GetActiveView() Is Nothing OrElse mvOfferMain.GetActiveView().Equals(vwDatesOffers)) Then
                Return newDates

            End If

            Return Nothing
        End Get
    End Property

    Private Property LastWhatToSearch As WhatToSearchEnum
        Get
            Return ViewState("LastWhatToSearch")
        End Get
        Set(value As WhatToSearchEnum)
            ViewState("LastWhatToSearch") = value
        End Set
    End Property

#End Region




    Protected Overrides Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        SetMasterPage(CurrentPageData)
        Try
            If (Me.SessionVariables.MemberData.Status = ProfileStatusEnum.LIMITED) Then
                Response.Redirect(ResolveUrl("~/Members/Default.aspx"))
            End If

            MyBase.Page_PreInit()
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        ' add a handler for handling coomands of Dates control
        AddHandler Me.CurrentOffersControl.Repeater.ItemCommand, AddressOf Me.offersRepeater_ItemCommand
        Try

            If (Not Page.IsPostBack) Then
                Try
                    Me.ItemsPerPage = clsProfilesPrivacySettings.GET_ItemsPerPage(Me.MasterProfileId)
                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "GET_ItemsPerPage")
                End Try

                Dim req As String = Request.QueryString("itemsPerPage")
                Dim _itemsPerPage As Integer
                If (Not String.IsNullOrEmpty(req)) Then
                    Integer.TryParse(req, _itemsPerPage)
                    Me.ItemsPerPage = _itemsPerPage
                End If


                req = Request.QueryString("sorting")
                Dim _cbSort As Integer
                If (Not String.IsNullOrEmpty(req)) Then
                    Integer.TryParse(req, _cbSort)
                    If (_cbSort >= 0 AndAlso _cbSort < cbSort.Items.Count) Then
                        cbSort.SelectedIndex = _cbSort
                    End If
                End If


                req = Request.QueryString("seo" & Me.Pager.ClientID)
                Dim currentPage As Integer
                If (Not String.IsNullOrEmpty(req)) Then
                    Try
                        req = req.Replace("page", "")
                        Integer.TryParse(req, currentPage)
                        Me.Pager.ItemCount = Me.ItemsPerPage * currentPage
                        If (Me.Pager.ItemCount > 0) Then Me.Pager.PageIndex = currentPage - 1
                    Catch ex As Exception
                    End Try
                End If


                req = Request.QueryString("search")
                If (Not String.IsNullOrEmpty(req)) Then
                    txtUserNameQ.Text = req
                    Me.LastWhatToSearch = WhatToSearchEnum.ByUserName
                End If

            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Try

            If (Not Page.IsPostBack) Then
                LoadLAG()
                LoadViews()

                ' if user has paid to unlock an offer, execute unlock command
                If (SessionVariables.UserUnlockInfo.IsUnlocked AndAlso SessionVariables.UserUnlockInfo.IsValid()) Then
                    ExecCmd(OfferControlCommandEnum.UNLOCKOFFER, SessionVariables.UserUnlockInfo.OfferId.ToString())
                End If
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub

    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender

        Try
            Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)

            If (Me.Pager.PageIndex > 0) Then
                Me.Header.Title = Me.Header.Title & " -- Dates page " & (Me.Pager.PageIndex + 1)
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try


        Try
         
            Dim url As String = Request.Url.AbsolutePath
            url = url & "?"

            Dim seo As String = ("seo" & Me.Pager.ClientID).ToUpper()
            For Each itm As String In Request.QueryString.AllKeys
                If (Not String.IsNullOrEmpty(itm)) Then

                    Dim s As String = itm.ToUpper()
                    If (s <> seo AndAlso s <> "ITEMSPERPAGE" AndAlso s <> "SORTING" AndAlso s <> "SEARCH") Then
                        url = url & itm & "=" & HttpUtility.UrlEncode(Request.QueryString(itm)) & "&"
                    End If

                End If
            Next

            Me.Pager.SeoFriendly = True
            Me.Pager.SeoNavigateUrlFormatString = url & "seo" & Me.Pager.ClientID & "={0}&itemsPerPage=" & Me.ItemsPerPage & "&sorting=" & cbSort.SelectedIndex
            If (txtUserNameQ.Text.Trim().Length > 0) Then
                Me.Pager.SeoNavigateUrlFormatString = Me.Pager.SeoNavigateUrlFormatString & "&search=" & HttpUtility.UrlEncode(txtUserNameQ.Text)
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub



    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
        LoadViews()
    End Sub



    Protected Sub LoadLAG()
        Try

            'lnkAccepted.Text = "<span></span>" & CurrentPageData.GetCustomString("lnkAccepted")
            ' lnkPoke.Text = "<span></span>" & CurrentPageData.GetCustomString("lnkPoke")

            cbSort.Items.Clear()
            cbSort.Items.Add(CurrentPageData.GetCustomString("cbSort_NewDates"), "cbSort_NewDates")
            cbSort.Items(cbSort.Items.Count - 1).Selected = True
            cbSort.Items.Add(CurrentPageData.GetCustomString("cbSort_ByDate"), "cbSort_ByDate")
            'cbSort.Items.Add(CurrentPageData.GetCustomString("cbSort_ByStatus"), "cbSort_ByStatus")
            cbSort.Items.Add(CurrentPageData.GetCustomString("cbSort_ByLastDating"), "cbSort_ByLastDating")

    

            Try
                If (Not Page.IsPostBack) Then
                    Me.DatesSort = DatesSortEnum.NewDates

                    Dim req As String = Request.QueryString("sorting")
                    Dim _cbSort As Integer
                    If (Not String.IsNullOrEmpty(req)) Then
                        Integer.TryParse(req, _cbSort)
                        If (_cbSort >= 0 AndAlso _cbSort < cbSort.Items.Count) Then
                            cbSort.SelectedIndex = _cbSort
                        End If
                    End If

                End If
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try

            lblSortByText.Text = CurrentPageData.GetCustomString(lblSortByText.ID)
            lblResultsPerPageText.Text = CurrentPageData.GetCustomString(lblResultsPerPageText.ID)
            txtUserNameQ.NullText = CurrentPageData.GetCustomString("msg_UserNameSearch")
            msg_divNoResultsTryChangeCriteria_ND.Text = CurrentPageData.GetCustomString("msg_divNoResultsTryChangeCriteria_ND")

            lblItemsName.Text = Me.CurrentPageData.GetCustomString("CartDatesView")
            lnkViewDescription.Text = Me.CurrentPageData.GetCustomString("CartDatesWhatIsIt")
            lnkViewDescription.OnClientClick = OnMoreInfoClickFunc(
                lnkViewDescription.OnClientClick,
                ResolveUrl("~/Members/InfoWin.aspx?info=dates"),
                Me.CurrentPageData.GetCustomString("CartDatesView"))
            WhatIsIt1.PopupControl.HeaderText = Me.CurrentPageData.GetCustomString("CartDatesView")

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Private Sub LoadViews()

        Try
            If (Not Page.IsPostBack) Then
                If (Request.QueryString("vw") IsNot Nothing) Then
                    If (Request.QueryString("vw").ToUpper() = "ACCEPTED") Then
                        mvOfferMain.SetActiveView(vwDatesOffers)
                    Else
                        mvOfferMain.SetActiveView(vwDatesOffers)
                    End If
                Else
                    mvOfferMain.SetActiveView(vwDatesOffers)
                End If
            Else
                BindNewDates()
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


    Protected Sub mvOfferMain_ActiveViewChanged(sender As Object, e As EventArgs) Handles mvOfferMain.ActiveViewChanged
        BindNewDates()
    End Sub



    Protected Sub Pager_PageIndexChanged(sender As Object, e As EventArgs) Handles Pager.PageIndexChanged
        BindNewDates()
    End Sub


    Protected Sub ddlSort_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbSort.SelectedIndexChanged
        Me.Pager.PageIndex = 0
        BindNewDates()
    End Sub

    Protected Sub ddlPerPage_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbPerPage.SelectedIndexChanged
        Try
            clsProfilesPrivacySettings.Update_ItemsPerPage(Me.MasterProfileId, Me.ItemsPerPage)
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorSendEmail(ex, "")
        End Try
        Try
            Me.Pager.PageIndex = 0
            BindNewDates()
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(ex, "")
        End Try
    End Sub

    Protected Sub offersRepeater_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs)
        ExecCmd(e.CommandName, e.CommandArgument)
    End Sub




    Private Sub ExecCmd(CommandName As String, CommandArgument As String)

        Dim success = False
        Dim showNoPhotoNote = False

        Try
            Dim currentUserHasPhotos As Boolean = clsUserDoes.HasPhotos(Me.MasterProfileId, Me.MirrorProfileId)
            If (currentUserHasPhotos) Then

                ''''''''''''''''''''''
                'photo aware actions
                ''''''''''''''''''''''
                If (CommandName = "REJECT_DELETECONV") Then

                    ''''''''''''''''''''''
                    'photo aware action
                    ''''''''''''''''''''''

                    Dim OtherMemberProfileID As Integer
                    Integer.TryParse(CommandArgument.ToString(), OtherMemberProfileID)

                    If (OtherMemberProfileID > 0) Then
                        Dim isReferrer As Boolean = (Me.GetCurrentProfile().ReferrerParentId.HasValue AndAlso Me.GetCurrentProfile().ReferrerParentId > 0) ' DataHelpers.EUS_Profile_GetIsReferrer(Me.CMSDBDataContext, OtherMemberProfileID)
                        clsMessagesHelper.DeleteMessagesFromMemberInView(Me.MasterProfileId, Me.MasterProfileId, OtherMemberProfileID, MessagesViewEnum.SENT, (isReferrer = True))
                        clsMessagesHelper.DeleteMessagesFromMemberInView(Me.MasterProfileId, OtherMemberProfileID, Me.MasterProfileId, MessagesViewEnum.INBOX, (isReferrer = True))
                        clsMessagesHelper.DeleteMessagesFromMemberInView(Me.MasterProfileId, OtherMemberProfileID, Me.MasterProfileId, MessagesViewEnum.NEWMESSAGES, (isReferrer = True))
                        clsMessagesHelper.DeleteMessagesFromMemberInView(Me.MasterProfileId, OtherMemberProfileID, Me.MasterProfileId, MessagesViewEnum.TRASH, (isReferrer = True))

                        clsUserDoes.MarkDateOfferAsViewed(OtherMemberProfileID, Me.MasterProfileId)
                    End If

                    success = True

                ElseIf (CommandName = "REJECT_BLOCK") Then

                    Dim OtherMemberProfileID As Integer
                    Integer.TryParse(CommandArgument.ToString(), OtherMemberProfileID)

                    If (OtherMemberProfileID > 0) Then
                        clsUserDoes.MarkAsBlocked(OtherMemberProfileID, Me.MasterProfileId)
                        clsUserDoes.MarkDateOfferAsViewed(OtherMemberProfileID, Me.MasterProfileId)
                    End If

                    success = True

                End If

            Else
                showNoPhotoNote = True
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try



        Try

            If (showNoPhotoNote) Then
                Response.Redirect("~/Members/Photos.aspx", False)
                HttpContext.Current.ApplicationInstance.CompleteRequest()

            ElseIf (success) Then
                SessionVariables.UserUnlockInfo.Reset()
                BindNewDates()
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub



    'Private Sub BindSearchResults()

    '    ' If (mvOfferMain.GetActiveView().Equals(vwDatesOffers)) Then

    '    'liAccepted.Attributes.Add("class", "down")


    '    BindNewDates()
    '    'End If

    'End Sub


    Private Sub BindNewDates()
        Try

            divFilter.Visible = False
            newDates.ShowEmptyListText = True

            If (Me.GetCurrentProfile(True) IsNot Nothing) Then

                If (Me.LastWhatToSearch = WhatToSearchEnum.ByUserName) Then
                    If (txtUserNameQ.Text.Trim().Length > 0) Then
                        divNoResultsChangeCriteria.Visible = True
                        newDates.ShowEmptyListText = False
                    End If
                End If

                'Dim dc As CMSDBDataContext = Me.CMSDBDataContext

                ' bind new offers
                Try

                    'If (dt.Rows.Count > 0) Then
                    '    FillOfferControlList(dt, newDates)
                    '    newDates.ShowEmptyListText = False
                    '    divFilter.Visible = True
                    'End If


                    Dim countRows As Integer
                    Dim performCount As Boolean = True
                    Dim NumberOfRecordsToReturn As Integer = Me.GetRecordsToRetrieve()

                    Dim parms As New clsDatesHelperParameters()
                    parms.CurrentProfileId = Me.MasterProfileId
                    parms.sorting = Me.DatesSort
                    parms.ReturnRecordsWithStatus = ProfileStatusEnum.Approved
                    parms.zipstr = Me.SessionVariables.MemberData.Zip
                    parms.latitudeIn = Me.SessionVariables.MemberData.latitude
                    parms.longitudeIn = Me.SessionVariables.MemberData.longitude
                    parms.Distance = clsSearchHelper.DISTANCE_DEFAULT
                    'parms.NumberOfRecordsToReturn = NumberOfRecordsToReturn
                    parms.performCount = True
                    parms.rowNumberMin = NumberOfRecordsToReturn - Me.ItemsPerPage
                    parms.rowNumberMax = NumberOfRecordsToReturn

                    If (Me.LastWhatToSearch = WhatToSearchEnum.ByUserName) Then
                        Dim sql As String = ""
                        Dim userName As String = txtUserNameQ.Text.Trim()
                        ClsSQLInjectionClear.ClearString(userName, 100)
                        If (Len(userName) > 0) Then
                            sql = sql & vbCrLf & " AND  EUS_Profiles.LoginName like N'" & userName & "%' "
                        End If

                        parms.AdditionalWhereClause = sql
                    End If

                    Dim ds As DataSet = clsSearchHelper.GetNewDatesDataTable(parms)

                    If (performCount) Then
                        countRows = ds.Tables(0).Rows(0)(0)
                        If (countRows > 0) Then
                            Dim dt As DataTable = ds.Tables(1)
                            FillOfferControlList(dt, newDates)
                            newDates.ShowEmptyListText = False
                            divFilter.Visible = True
                            divNoResultsChangeCriteria.Visible = False
                        End If
                    Else
                        countRows = ds.Tables(0).Rows.Count
                        If (countRows > 0) Then
                            Dim dt As DataTable = ds.Tables(0)
                            FillOfferControlList(dt, newDates)
                            newDates.ShowEmptyListText = False
                            divFilter.Visible = True
                            divNoResultsChangeCriteria.Visible = False
                        End If
                    End If

                    newDates.DataBind()
                    SetPager(countRows)

                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "")

                    SetPager(0)
                    newDates.UsersList = Nothing
                    newDates.DataBind()
                End Try
            End If

            If (Me.Pager.ItemCount = 0 AndAlso Me.GetPhotosCount() = 0) Then

                Try

                    newDates.ShowNoPhotoText = True
                    newDates.ShowEmptyListText = False
                    newDates.DataBind()

                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "")
                Finally
                    SetPager(0)
                End Try

            End If


        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


    Private Sub FillOfferControlList(dt As DataTable, offersControl As Dating.Server.Site.Web.DatesControl)

        ' set pagination
        'Dim startingIndex As Integer = 0
        'Dim endIndex As Integer = Me.ItemsPerPage - 1
        'If (Me.Pager.PageIndex > 0) Then
        '    startingIndex = Me.ItemsPerPage * Me.Pager.PageIndex
        '    endIndex = (startingIndex + Me.ItemsPerPage) - 1
        'End If

        offersControl.FillKeyStrings()

        Dim drDefaultPhoto As EUS_CustomerPhotosRow = DataHelpers.GetProfilesDefaultPhoto(Me.MasterProfileId)
        Dim uliCurrentMember As New clsWinkUserListItem()
        uliCurrentMember.ProfileID = Me.MasterProfileId ' 
        uliCurrentMember.MirrorProfileID = Me.MirrorProfileId
        uliCurrentMember.LoginName = Me.GetCurrentProfile().LoginName
        uliCurrentMember.Genderid = Me.GetCurrentProfile().GenderId
        uliCurrentMember.ProfileViewUrl = ResolveUrl("~/Members/Profile.aspx?p=" & HttpUtility.UrlEncode(Me.GetCurrentProfile().LoginName))


        'If (drDefaultPhoto IsNot Nothing) Then
        '    uliCurrentMember.ImageFileName = drDefaultPhoto.FileName
        '    uliCurrentMember.ImageUrl = ProfileHelper.GetProfileImageURL(drDefaultPhoto.CustomerID, drDefaultPhoto.FileName, Me.GetCurrentProfile().GenderId, True, Me.IsHTTPS)
        '    uliCurrentMember.ImageThumbUrl = ProfileHelper.GetProfileImageURL(drDefaultPhoto.CustomerID, drDefaultPhoto.FileName, Me.GetCurrentProfile().GenderId, True, Me.IsHTTPS)
        '    uliCurrentMember.ImageUploadDate = drDefaultPhoto.DateTimeToUploading.ToLocalTime().ToString("d MMM, yyyy H:mm")
        'Else
        '    uliCurrentMember.ImageUrl = ProfileHelper.GetDefaultImageURL(Me.GetCurrentProfile().GenderId)
        '    uliCurrentMember.ImageThumbUrl = uliCurrentMember.ImageUrl
        '    uliCurrentMember.ImageFileName = System.IO.Path.GetFileName(uliCurrentMember.ImageUrl)
        '    uliCurrentMember.ImageUploadDate = Nothing
        'End If



        If (drDefaultPhoto IsNot Nothing) Then
            uliCurrentMember.ImageFileName = drDefaultPhoto.FileName
            uliCurrentMember.ImageUploadDate = drDefaultPhoto.DateTimeToUploading.ToLocalTime().ToString("d MMM, yyyy H:mm")
        End If

        uliCurrentMember.ImageUrl = ProfileHelper.GetProfileImageURL(Me.MasterProfileId, uliCurrentMember.ImageFileName, Me.SessionVariables.MemberData.GenderId, True, Me.IsHTTPS, PhotoSize.D150)
        uliCurrentMember.ImageThumbUrl = uliCurrentMember.ImageUrl

        Dim rowsCount As Integer
        For rowsCount = 0 To dt.Rows.Count - 1

            Try
                Dim dr As DataRow = dt.Rows(rowsCount)
                'Dim distance As Integer = ProfileHelper.CalculateDistance(Me.GetCurrentProfile(), dr)

                Dim uli As New clsWinkUserListItem()
                uli.LAGID = Session("LAGID")
                uli.ProfileID = uliCurrentMember.ProfileID
                uli.MirrorProfileID = uliCurrentMember.MirrorProfileID
                uli.LoginName = uliCurrentMember.LoginName
                uli.Genderid = uliCurrentMember.Genderid
                uli.ProfileViewUrl = uliCurrentMember.ProfileViewUrl
                uli.ImageFileName = uliCurrentMember.ImageFileName
                uli.ImageUrl = uliCurrentMember.ImageUrl
                uli.ImageThumbUrl = uliCurrentMember.ImageThumbUrl
                uli.ImageUploadDate = uliCurrentMember.ImageUploadDate

                If (dt.Columns.Contains("Birthday")) Then uli.OtherMemberBirthday = If(Not dr.IsNull("Birthday"), dr("Birthday"), Nothing)
                uli.OtherMemberLoginName = dr("LoginName")
                uli.OtherMemberProfileID = dr("ProfileID")
                uli.OtherMemberMirrorProfileID = dr("MirrorProfileID")
                uli.OtherMemberCity = dr("City")
                uli.OtherMemberRegion = dr("Region").ToString()
                uli.OtherMemberCountry = dr("Country").ToString()
                uli.OtherMemberGenderid = dr("Genderid")
                uli.OtherMemberHeading = dr("AboutMe_Heading").ToString()

                Dim CurrentLoginName_enc As String = HttpUtility.UrlEncode(uli.OtherMemberLoginName)

                If (Not dr.IsNull("PersonalInfo_HeightID")) Then
                    uli.OtherMemberHeight = ProfileHelper.GetHeightString(dr("PersonalInfo_HeightID"), Me.GetLag())
                End If


                If (Not dr.IsNull("PersonalInfo_BodyTypeID")) Then
                    uli.OtherMemberPersonType = ProfileHelper.GetBodyTypeString(dr("PersonalInfo_BodyTypeID"), Me.GetLag())
                End If


                If (Not dr.IsNull("PersonalInfo_HairColorID")) Then
                    uli.OtherMemberHair = ProfileHelper.GetHairColorString(dr("PersonalInfo_HairColorID"), Me.GetLag())
                End If


                If (Not dr.IsNull("PersonalInfo_EyeColorID")) Then
                    uli.OtherMemberEyes = ProfileHelper.GetEyeColorString(dr("PersonalInfo_EyeColorID"), Me.GetLag())
                End If


                If (Not dr.IsNull("PersonalInfo_EthnicityID")) Then
                    uli.OtherMemberEthnicity = ProfileHelper.GetEthnicityString(dr("PersonalInfo_EthnicityID"), Me.GetLag())
                End If

                If (Not dr.IsNull("Birthday")) Then
                    uli.OtherMemberAge = ProfileHelper.GetCurrentAge(dr("Birthday"))
                Else
                    uli.OtherMemberAge = 25
                End If


                uli.OtherMemberImageFileName = dr("FileName").ToString()
                If (dt.Columns.Contains("HasPhoto")) Then
                    If (dr("HasPhoto").ToString() = "1" OrElse dr("HasPhoto").ToString() = "True") Then
                        ' user has photo
                        If (Not String.IsNullOrEmpty(uli.OtherMemberImageFileName)) Then
                            'has public photos
                            uli.OtherMemberImageUrl = ProfileHelper.GetProfileImageURL(uli.OtherMemberProfileID, uli.OtherMemberImageFileName, uli.OtherMemberGenderid, True, Me.IsHTTPS, PhotoSize.D150)

                        Else
                            'has private photos
                            uli.OtherMemberImageUrl = ProfileHelper.GetPrivateImageURL(uli.OtherMemberGenderid)

                        End If
                    Else
                        ' has no photo
                        uli.OtherMemberImageUrl = ProfileHelper.GetDefaultImageURL(uli.OtherMemberGenderid)
                    End If
                    If IsHTTPS AndAlso uli.OtherMemberImageUrl.StartsWith("http:") Then uli.OtherMemberImageUrl = "https" & uli.OtherMemberImageUrl.Remove(0, "http".Length)
                Else
                    uli.OtherMemberImageUrl = ProfileHelper.GetProfileImageURL(uli.OtherMemberProfileID, uli.OtherMemberImageFileName, uli.OtherMemberGenderid, True, Me.IsHTTPS, PhotoSize.D150)
                End If
                uli.OtherMemberImageThumbUrl = uli.OtherMemberImageUrl
                uli.OtherMemberProfileViewUrl = ResolveUrl("~/Members/Profile.aspx?p=" & CurrentLoginName_enc)
                uli.Distance = dr("distance")

                If (uli.Distance < gCarDistance) Then
                    uli.DistanceCss = "distance_car"
                Else
                    uli.DistanceCss = "distance_plane"
                End If

                uli.SendMessageUrl = ResolveUrl("~/Members/Conversation.aspx?p=" & CurrentLoginName_enc)

                uli.AllowConversation = True
                uli.ConversationNavigateUrl = ResolveUrl("~/Members/Conversation.aspx?vw=" & CurrentLoginName_enc)

                uli.AllowHistory = True
                'uli.HistoryNavigateUrl = "OnMoreInfoClick('[contentUrl]', this, '[headerText]');return false;"
                'Dim contentUrl As String = ResolveUrl("~/Members/DatesHistory.aspx?vw=" & HttpUtility.UrlEncode(uli.OtherMemberLoginName))
                'contentUrl = contentUrl.Replace("'", "\'")
                'Dim headerText As String = Me.CurrentPageData.GetCustomString("DatesHistory_HeaderText")
                'If (Not String.IsNullOrEmpty(headerText)) Then headerText = headerText.Replace("'", "\'")
                'uli.HistoryNavigateUrl = uli.HistoryNavigateUrl.Replace("[contentUrl]", contentUrl)
                'uli.HistoryNavigateUrl = uli.HistoryNavigateUrl.Replace("[headerText]", headerText)
                '            uli.HistoryNavigateUrl = OnMoreInfoClickFunc(
                '"OnMoreInfoClick('[contentUrl]', this, '[headerText]');return false;",
                'ResolveUrl("~/Members/DatesHistory.aspx?vw=" & CurrentLoginName_enc),
                'Me.CurrentPageData.GetCustomString("DatesHistory_HeaderText"))

                Dim hdrTitle As String = "<div id=""history-popup-title""><span class=""title-text"">" & Me.CurrentPageData.GetCustomString("DatesHistory_HeaderText") & "</span><span class=""title-image""></span></div>"
                uli.HistoryNavigateUrl = WhatIsIt.OnMoreInfoClickFunc(
                    ResolveUrl("~/Members/DatesHistory.aspx?vw=" & CurrentLoginName_enc),
                    hdrTitle,
                    734)
                uli.AllowRejectsMenu = True

                '' CHECK OFFER CASES
                uli.OfferID = clsNullable.DBNullToInteger(dr("OfferID"))
                uli.OfferAmount = clsNullable.DBNullToInteger(dr("OffersAmount"))

                Dim offerRecVals As New Offers3.OfferRecordValues()
                offerRecVals.OfferID = uli.OfferID
                offerRecVals.OfferTypeID = clsNullable.DBNullToInteger(dr("OffersOfferTypeID"))
                offerRecVals.OffersStatusID = clsNullable.DBNullToInteger(dr("OffersStatusID"))
                offerRecVals.OffersFromProfileID = clsNullable.DBNullToInteger(dr("OffersFromProfileID"))
                offerRecVals.OffersToProfileID = clsNullable.DBNullToInteger(dr("OffersToProfileID"))

                If (dr("CommunicationUnl") > 0) Then
                    uli.CommunicationStatusUnlimitedText = offersControl.CurrentPageData.GetCustomString("CommunicationStatusUnlimitedText")
                Else
                    uli.CommunicationStatusLimitedText = offersControl.CurrentPageData.GetCustomString("CommunicationStatusLimitedText")
                End If

                'If (Not dr.IsNull("FirstMessageDate")) Then
                '    Dim _dateTime As DateTime = dr("FirstMessageDate")
                '    uli.FirstDateText = offersControl.CurrentPageData.GetCustomString("FirstDateText")
                '    uli.FirstDateText = uli.FirstDateText.Replace("###DATE###", _dateTime.ToString("dd/MM/yyyy"))
                'End If

                If (Not dr.IsNull("LastMessageDate")) Then
                    'Dim _dateTime As DateTime = dr("LastMessageDate")
                    'uli.LastDatingText = offersControl.CurrentPageData.GetCustomString("LastDatingText")
                    'uli.LastDatingDateText = _dateTime.ToString("dd/MM/yyyy")
                    Dim _dateTime As DateTime = dr("LastMessageDate")
                    Dim dateDescrpt As String = AppUtils.GetMessageDateDescription(_dateTime, Me.globalStrings)

                    uli.LastDatingText = offersControl.CurrentPageData.VerifyCustomString("LastDatingText")
                    uli.LastDatingDateText = dateDescrpt ' _dateTime.ToString("dd/MM/yyyy")

                End If


                offersControl.UsersList.Add(uli)
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try

        Next
    End Sub


    'Sub SetPager(itemCount As Integer)
    '    Me.Pager.ItemCount = itemCount
    '    Me.Pager.ItemsPerPage = Me.ItemsPerPage
    '    Me.Pager.Visible = (itemCount > Me.ItemsPerPage)
    'End Sub

    Sub SetPager(itemsCount As Integer)
        Me.Pager.ItemCount = itemsCount
        Me.Pager.ItemsPerPage = Me.ItemsPerPage
        Me.Pager.Visible = (itemsCount > Me.ItemsPerPage)

        If (Me.Pager.PageIndex = -1) AndAlso (itemsCount > 0) Then
            Me.Pager.PageIndex = 0
        End If
    End Sub

    Function GetRecordsToRetrieve() As Integer
        Me.Pager.ItemsPerPage = Me.ItemsPerPage

        If (Me.Pager.PageIndex = -1) AndAlso (Me.Pager.ItemCount > 0) Then
            Me.Pager.PageIndex = 0
        End If

        If (Me.Pager.PageIndex > 0) Then
            Return (Me.Pager.ItemsPerPage * (Me.Pager.PageIndex + 1))
        End If
        Return Me.Pager.ItemsPerPage
    End Function


    Private Sub LoadPokeView(offersControl As Dating.Server.Site.Web.DatesControl, uli As clsWinkUserListItem)


        Dim usersHasCommunication As Boolean = clsUserDoes.HasCommunication(uli.OtherMemberProfileID, Me.MasterProfileId)
        If (Not usersHasCommunication) Then

            uli.AllowActionsUnlock = True
            uli.AllowActionsCreateOffer = True

            '''''''''''''''''''
            ' communication is locked
            '''''''''''''''''''

            Dim rec As EUS_Offer = clsUserDoes.GetLastOffer(uli.OtherMemberProfileID, Me.MasterProfileId)

            If (rec Is Nothing) Then
                uli.CreateOfferUrl =
                        ResolveUrl("~/Members/CreateOffer.aspx?offer=" & uli.OfferID)

            End If

            'If (ProfileHelper.IsGenerous(Me.GetCurrentProfile().AccountTypeId)) Then
            If (Me.IsMale) Then
                uli.AllowOfferAcceptedUnlock = True
                uli.Credits = ProfileHelper.Config_UNLOCK_CONVERSATION_CREDITS
            End If
        Else

            uli.AllowActionsSendMessage = True
            uli.AllowActionsDeleteOffer = True

        End If
    End Sub

    Private Shared Sub LoadWinkView(offersControl As DatesControl, uli As clsWinkUserListItem, MasterProfileId As Integer)

        Try

            If (ProfileHelper.IsFemale(uli.OtherMemberGenderid)) Then
                ' like apo gynaika

                uli.WantToKnowFirstDatePriceTextNew = offersControl.MykeyStrings.FEMALE_WantToKnowFirstDatePriceText.Replace("###LOGINNAME###", uli.OtherMemberLoginName)
            Else
                ' like apo andra

                uli.WantToKnowFirstDatePriceTextNew = offersControl.MykeyStrings.MALE_WantToKnowFirstDatePriceText.Replace("###LOGINNAME###", uli.OtherMemberLoginName)
            End If

        Catch ex As Exception

        End Try


        Dim rec As EUS_Offer = clsUserDoes.GetLastOffer(uli.OtherMemberProfileID, MasterProfileId)

        If (rec Is Nothing) Then
            uli.AllowCreateOffer = True
            uli.CreateOfferUrl = System.Web.VirtualPathUtility.ToAbsolute("~/Members/CreateOffer.aspx?offer=" & uli.OfferID)
            ' & _
            '("&offerto=" & HttpUtility.UrlEncode(uli.OtherMemberLoginName)) & _
            '("&returnurl=" & HttpUtility.UrlEncode(Request.Url.AbsoluteUri))

        End If
        'End If   
    End Sub


    Protected Sub imgSearchByUserName_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles imgSearchByUserName.Click
        Try
            Me.LastWhatToSearch = WhatToSearchEnum.ByUserName
            Me.Pager.PageIndex = -1
            BindNewDates()

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

End Class

﻿Imports Dating.Server.Core.DLL

Public Class PopupGifts
    Inherits BasePage

    Protected Overrides Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        '  SetMasterPage(CurrentPageData)
        Try
            ' If (Me.SessionVariables.MemberData.Status = ProfileStatusEnum.LIMITED) Then
            '  Response.Redirect(ResolveUrl("~/Members/Default.aspx"))
            '     End If

            '  MyBase.Page_PreInit()
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Protected Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData(Context, Me.Page, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            LoadLAG()
            Session("HasSeenGiftPopUp") = True
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

       
    End Sub


    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender

        Try
            Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub

    Protected Sub LoadLAG()
        Try
            If (String.IsNullOrEmpty(CurrentPageData.GetCustomString("lnkNew"))) Then
                clsPageData.ClearCache()
            End If
            lblbottomBrands.Text = CurrentPageData.GetCustomString("lblBottomBrands")
            lblLeftContent.Text = CurrentPageData.GetCustomString("lblLeftContent")
            lblRightContent.Text = CurrentPageData.GetCustomString("lblRightContent")
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub
End Class
﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Members/Members2Inner.master"
    CodeBehind="paymentok.aspx.vb" Inherits="Dating.Server.Site.Web.paymentok" %>

<%@ Register Src="../UserControls/MemberLeftPanel.ascx" TagName="MemberLeftPanel"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        try {
            if (window.top != window.self) {
                window.top.location = window.self.location;
            }
        }
        catch (e) { }
    </script>
<style type="text/css">
    #payment_ok {font-family:'Segoe UI', 'Arial';}
    #payment_ok .main-wrap{background: url('//cdn.goomena.com/Images2/payment-finish/bg.png') no-repeat scroll 0 0;width:685px;height:570px;position:relative;}
    #payment_ok .main-wrap .success-title{text-align:center;position:absolute;top:30px;left:0px;right:0px;}
    #payment_ok .main-wrap .success-title .line1{font-size:25px;font-weight:bold;color:#61c006;}
    #payment_ok .main-wrap .success-title .line2{font-size:19px;font-style:italic;color:#61c006;}
    #payment_ok .main-wrap .main-content{text-align:center;position:absolute;top:250px;color:#333;}
    #payment_ok .start-conv { color: #fff;font-size: 16px;font-weight:bold; text-align: center; line-height: 50px; 
                                                 border-width: 0px; width: 412px; height: 54px;display: inline-block; 
                                                 background: url('//cdn.goomena.com/Images2/payment-finish/button.png') no-repeat scroll 0% 0%;
                                                 margin: 0 auto; }
    #payment_ok .start-conv:hover {background-position:100% 100%;}
    #payment_ok ul.success-points {font-size:13px;text-align:left;}
    #payment_ok ul.success-points li {margin-left:40px;margin-bottom:15px;color:#333;margin-right: 30px;}
    #payment_ok div.grey-line {height:2px;margin:10px 20px;background-color:#7C8274;}
    #payment_ok div.logo-holder {text-align:center;position:absolute;bottom:30px;left:0px;right:0px;}
    #payment_ok div.link-holder {text-align:center;position:relative;margin-top:20px;}

</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <div class="clear">
    </div>
    <div class="container_12" id="tabs-outter">
        <div class="grid_12 top_tabs">
            <div>
            </div>
        </div>
    </div>
    <div class="clear">
    </div>
    <div class="container_12" id="content_top">
    </div>
    <div class="container_12" id="content">
        <div class="grid_3" id="sidebar-outter">
            <%--<uc1:MemberLeftPanel ID="leftPanel" runat="server" ShowBottomPanel="false" />--%>
        </div>
        <div class="grid_9 body" id="payment_ok">
            <div class="middle" id="content-outter">

                <asp:Literal ID="lDescription2" runat="server">
                <div class="main-wrap">

                    <div class="success-title">
                        <img src="//cdn.goomena.com/Images2/payment-finish/ok.png" alt="Your Transaction was successful. Your account has been credited the amount purchased. Now start talking." title="Your Transaction was successful. Your account has been credited the amount purchased. Now start talking." /><br />
                        <span class="line1">Transaction was Successful</span><br />
                        <span class="line2">Thank you for your purchase.</span>
                    </div>

                    <div class="main-content">
                        <div style="line-height:25px;">
                            <span style="font-size:16px;font-weight:bold;">Your Goomena account will be updated automatically!</span><br />
                            <span style="font-size:16px;">Your transaction was successful.</span><br />
                        </div>
                        <div class="grey-line"></div>
<ul class="success-points">
    <li>Allow enough time for our payment processors be updated about purchase.</li>
    <li>Should the problem persist after 3 hours,notify us via email <a href="mailto:info@goomena.com">info@goomena.com</a>
        highlighting the package purchased, your Transaction ID and your goomena registered email.</li>
    <li>If you have any problems with the transaction, please do not hesitate to contact us.</li>
    <li>Please include your Goomena login name & PayPal transaction ID in your inquiry. </li>
</ul>
                    </div>

                    <div class="logo-holder">
                        <img src="//cdn.goomena.com/Images2/payment-finish/logo-small.png" alt="logo-small.png" />
                    </div>

                </div>


                <div class="link-holder">
<a href="http://www.goomena.com/Members/Default.aspx" target="_self" class="start-conv">Click here to go to your dashboard.</a>
                </div>
                </asp:Literal>

                <%--<div style="padding:40px;">

                    <dx:ASPxLabel ID="lGroupName" runat="server" EncodeHtml="False">
                    </dx:ASPxLabel>

                    <table id="ctl00_tblContent" cellpadding="0" cellspacing="0" border="0" width="100%">
                        <tbody>
                            <tr>
                                <td valign="top">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tbody>
                                            <tr>
                                                <td class="DetailsPageLeftEdge">
                                                    <div class="Spacer DetailsPageEdge">
                                                    </div>
                                                </td>
                                                <td valign="top" class="DetailsPageContent">
                                                    <div>
                                                        <dx:ASPxLabel ID="lDescription" runat="server" Text="" EncodeHtml="False">
                                                        </dx:ASPxLabel>
                                                    </div>
                                                </td>
                                                <td class="DetailsPageRightEdge">
                                                    <div class="Spacer DetailsPageEdge">
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table id="ctl00_tblFooter" border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tbody>
                            <tr>
                                <td class="DetailsPageFooter">
                                    <div class="Spacer">
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>--%>
            </div>
            <div class="bottom">
            </div>
        </div>
        <div class="clear">
        </div>
    </div>
</asp:Content>

﻿'Imports Sites.SharedSiteCode
Imports Library.Public
Imports Dating.Server.Core.DLL
Imports System.Data.SqlClient
Imports Dating.Server.Datasets.DLL

Public Class _default1
    Inherits BasePage


    Const __TESTING__ As Boolean = False

    Protected Overrides Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        SetMasterPage(CurrentPageData)

        Try
            If clsCurrentContext.VerifyLogin() = True Then
                Me.MasterPageFile = "~/Members/Members2.Master"
            Else
                clsCurrentContext.ClearSession(Session)
                If (Not Me.IsCallback) Then
                    Response.Redirect(ResolveUrl("~/Login.aspx"), False)
                End If
                HttpContext.Current.ApplicationInstance.CompleteRequest()
            End If
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try

            If (clsCurrentContext.VerifyLogin() = False) Then
                FormsAuthentication.SignOut()
                Response.Redirect(Page.ResolveUrl("~/Login.aspx"))
            End If
            If (Not Me.IsPostBack) Then
                LoadLAG()
                Me._LoadData()
            End If

        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try
    End Sub

    Private Property OldPageIndex As Integer
        Get
            Return ViewState("OldPageIndex")
        End Get
        Set(value As Integer)
            ViewState("OldPageIndex") = value
        End Set
    End Property
    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        Try
            Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
            If (Page.IsPostBack) Then
                'If (Me.OldPageIndex <> Me.Pager.PageIndex) Then
                Dim jv As String = <javascript><![CDATA[
	
$('html,body').animate({
   scrollTop: $("#SearchResults").offset().top-115
});

]]></javascript>.Value
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "scrollToResultsTop", jv, True)
                '    Me.OldPageIndex = Me.Pager.PageIndex
                'End If
            Else
                OldPageIndex = 1
                BindFilteredResults()
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub



    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
        _LoadData()
        UpdateUserControls(Me)

    End Sub


    Protected Sub LoadLAG()
        Try
            If (CurrentPageData.LagID <> GetLag()) Then
                Me._pageData = Nothing
            End If

            lnkUploadPhoto.Text = CurrentPageData.GetCustomString(lnkUploadPhoto.ID)
            lnkUploadPhoto.NavigateUrl = CurrentPageData.GetCustomString("lnkUploadPhoto.NavigateUrl")

            Dim nearestMembersSelectedOption As Integer = Core.DLL.clsProfilesPrivacySettings.GET_NearestMembersOptions(Me.MasterProfileId)
            setNearestMembersContent(nearestMembersSelectedOption)

            Dim profilesViewedSelectedOption As Integer = Core.DLL.clsProfilesPrivacySettings.GET_ProfilesViewedOptions(Me.MasterProfileId)

            If (Me.IsFemale()) Then
                lblImportantAdvice.Text = CurrentPageData.GetCustomString("lblImportantAdvice_Female_ND")
            Else
                lblImportantAdvice.Text = CurrentPageData.GetCustomString("lblImportantAdvice_Male_ND")
            End If


            lblWhoHasSeenmyProfile.Text = CurrentPageData.GetCustomString("lnkWhoViewedMe2")
            lblMyFavoriteList.Text = CurrentPageData.GetCustomString("lnkMyFavoriteList2")
            lblProfilesIhaveSeen.Text = CurrentPageData.GetCustomString("lnkMyViewedList2")
            lblAutoNotification.Text = CurrentPageData.GetCustomString("lnkNotifications2")
            SetControlsValue(Me, CurrentPageData)
            If (Me.IsHTTPS) Then
                Select Case Me.GetLag()
                    Case "GR"
                        iMobileBanner.Src = Me.Request.Url.Scheme & "://cdn.goomena.com/Images/MobileBanners/goomena-androind-gr.png"
                    Case "TR"
                        iMobileBanner.Src = Me.Request.Url.Scheme & "://cdn.goomena.com/Images/MobileBanners/goomena-androind-tu.png"
                    Case Else
                        iMobileBanner.Src = Me.Request.Url.Scheme & "://cdn.goomena.com/Images/MobileBanners/goomena-androind-en.png"
                End Select
            Else
                Select Case Me.GetLag()
                    Case "GR"
                        iMobileBanner.Src = Me.Request.Url.Scheme & "://cdn.goomena.com/Images/MobileBanners/goomena-androind-gr.png"
                    Case "TR"
                        iMobileBanner.Src = Me.Request.Url.Scheme & "://cdn.goomena.com/Images/MobileBanners/goomena-androind-tu.png"
                    Case Else
                        iMobileBanner.Src = Me.Request.Url.Scheme & "://cdn.goomena.com/Images/MobileBanners/goomena-androind-en.png"
                End Select
            End If


        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub



    Private Sub _LoadData()
        Try
            If (Me.SessionVariables.MemberData.Status = ProfileStatusEnum.LIMITED) Then
                lblWelcome.Text = CurrentPageData.VerifyCustomString("lblWelcome_Limited_ND2")
                lblWelcome.Text = lblWelcome.Text.Replace("###LOGINNAME###", Me.SessionVariables.MemberData.LoginName)
                If (hdrWelcome.Attributes("class") IsNot Nothing) Then
                    hdrWelcome.Attributes("class") = hdrWelcome.Attributes("class") & " limited"
                Else
                    hdrWelcome.Attributes.Add("class", "limited")
                End If
            Else
                If (Me.IsFemale) Then
                    lblWelcome.Text = CurrentPageData.GetCustomString("lblWelcome_FEMALE_ND3")
                Else
                    lblWelcome.Text = CurrentPageData.GetCustomString("lblWelcome_MALE_ND2")
                End If
                lblWelcome.Text = lblWelcome.Text.Replace("###LOGINNAME###", Me.SessionVariables.MemberData.LoginName)
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

        Try
            Dim currentUserHasPhotos As Boolean = clsUserDoes.HasPhotos(Me.MasterProfileId, Me.MirrorProfileId)
            If (Not currentUserHasPhotos) Then
                divUploadPhoto.Visible = True
            Else
                divUploadPhoto.Visible = False
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub
    Protected Shared Function GetOnClickJS(LoginName As String) As String
        Dim fn As String = "jsLoad(""" & System.Web.VirtualPathUtility.ToAbsolute("~/Members/Profile.aspx?p=") & HttpUtility.UrlEncode(LoginName) & """)"
        Return fn
    End Function
    Protected Sub setNearestMembersContent(selectedOption As Integer)

        Try
            If (Me.GetCurrentProfile(True) IsNot Nothing) Then
                If selectedOption = NearestMembersOptions.NearestMembers Then
                    Dim prms As New clsSearchHelperParameters()
                    prms.CurrentProfileId = Me.MasterProfileId
                    prms.ReturnRecordsWithStatus = ProfileStatusEnum.Approved
                    prms.zipstr = Me.SessionVariables.MemberData.Zip
                    prms.latitudeIn = Me.SessionVariables.MemberData.latitude
                    prms.longitudeIn = Me.SessionVariables.MemberData.longitude
                    prms.Distance = 5
                    prms.NumberOfRecordsToReturn = 8
                Else
                    Dim params As New clsSearchHelperParameters()
                    params.CurrentProfileId = Me.MasterProfileId
                    params.ReturnRecordsWithStatus = ProfileStatusEnum.Approved

                    If selectedOption = NearestMembersOptions.NearestMembers Then
                        params.SearchSort = SearchSortEnum.NearestDistance

                    Else
                        params.SearchSort = SearchSortEnum.NewestMember
                    End If
                    params.zipstr = Me.SessionVariables.MemberData.Zip
                    params.latitudeIn = Me.SessionVariables.MemberData.latitude
                    params.longitudeIn = Me.SessionVariables.MemberData.longitude
                    params.Distance = 1000
                    params.LookingFor_ToMeetMaleID = Me.SessionVariables.MemberData.LookingFor_ToMeetMaleID
                    params.LookingFor_ToMeetFemaleID = Me.SessionVariables.MemberData.LookingFor_ToMeetFemaleID
                    params.NumberOfRecordsToReturn = 8
                    params.rowNumberMin = 0
                    params.rowNumberMax = params.NumberOfRecordsToReturn
                    params.AdditionalWhereClause = "and not exists(select * from EUS_ProfilesViewed where FromProfileID=@CurrentProfileId and ToProfileID=EUS_Profiles.ProfileID)"
                    params.performCount = False
                End If

            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    ReadOnly Property QSCreterias As INewQuickSearchCriterias
        Get
            Return UcQuickSearch
        End Get
    End Property
    Function GetRecordsToRetrieve() As Integer
        If OldPageIndex <= 0 Then
            Return 1
        Else
            Return OldPageIndex
        End If
    End Function
    Private Sub BindFilteredResults()
        Try
            Dim sql As String = ""
            If (QSCreterias.ageMinSelectedValue > -1 AndAlso QSCreterias.ageMaxSelectedValue > -1) Then
                Dim fromDate = DateAdd(DateInterval.Year, QSCreterias.ageMaxSelectedValue * -1, Date.Now).Date
                fromDate = DateAdd(DateInterval.Year, -1, fromDate).Date
                Dim toDate = DateAdd(DateInterval.Year, QSCreterias.ageMinSelectedValue * -1, Date.Now).Date
                sql = sql & vbCrLf & " and  (EUS_Profiles.Birthday between '" & fromDate.ToString("yyyy-MM-dd hh:mm:ss") & "' and '" & toDate.ToString("yyyy-MM-dd hh:mm:ss") & "')"
            ElseIf (QSCreterias.ageMinSelectedValue > -1) Then
                Dim toDate = DateAdd(DateInterval.Year, QSCreterias.ageMinSelectedValue * -1, Date.Now).Date
                sql = sql & vbCrLf & " and (EUS_Profiles.Birthday < '" & toDate.ToString("yyyy-MM-dd hh:mm:ss") & "') "
            ElseIf (QSCreterias.ageMaxSelectedValue > -1) Then
                Dim fromDate = DateAdd(DateInterval.Year, QSCreterias.ageMaxSelectedValue * -1, Date.Now).Date
                fromDate = DateAdd(DateInterval.Year, -1, fromDate).Date
                sql = sql & vbCrLf & " and (EUS_Profiles.Birthday between '" & fromDate.ToString("yyyy-MM-dd hh:mm:ss") & "' and getdate()) "
            End If
            If (QSCreterias.chkPhotosChecked) Then
                sql = sql & " and exists (select customerId from EUS_CustomerPhotos phot1 where phot1.customerId = EUS_Profiles.ProfileId and phot1.HasAproved = 1 and (phot1.HasDeclined is null or phot1.HasDeclined=0) and (phot1.IsDeleted=0))"
            End If
            Dim sli As SearchListInfo = Nothing
            sli = UcQuickSearch.SelectedHairColorList
            If (Not sli.IsAllSelected AndAlso
                sli.List.Count > 0) Then
                Dim txt As String = GetListSubquery(sli.List)
                sql = sql & " and EUS_Profiles.PersonalInfo_HairColorID in (" & txt & ")"
            End If
            sli = QSCreterias.SelectedBodyTypesList
            If (Not sli.IsAllSelected AndAlso
                sli.List.Count > 0) Then
                Dim txt As String = GetListSubquery(sli.List)
                sql = sql & " and EUS_Profiles.PersonalInfo_BodyTypeID in (" & txt & ")"
            End If
            If (QSCreterias.chkOnlineChecked) Then
                Dim LastActivityUTCDate As DateTime
                Dim hours As Integer = clsConfigValues.Get__members_online_recently_hours()
                If (hours > 0) Then
                    LastActivityUTCDate = Date.UtcNow.AddHours(-hours)
                Else
                    Dim mins As Integer = clsConfigValues.Get__members_online_minutes()
                    LastActivityUTCDate = Date.UtcNow.AddMinutes(-mins)
                End If

                sql = sql & " and (EUS_Profiles.IsOnline=1 and EUS_Profiles.LastActivityDateTime>='" & LastActivityUTCDate.ToString("yyyy-MM-dd HH:mm") & "')" & vbCrLf & _
                " and (not exists (SELECT  [EUS_ProfilesPrivacySettingsID] FROM [EUS_ProfilesPrivacySettings]  where ([ProfileID]=EUS_Profiles.ProfileId Or MirrorProfileId=EUS_Profiles.ProfileId) and [PrivacySettings_ShowMeOffline]=1))"
            End If
            Dim NumberOfRecordsToReturn As Integer = 8 * Me.GetRecordsToRetrieve
            Dim prms As New clsSearchHelperParameters()
            prms.CurrentProfileId = Me.MasterProfileId
            prms.ReturnRecordsWithStatus = ProfileStatusEnum.Approved
            prms.SearchSort = SearchSortEnum.NewestNearMember
            prms.zipstr = Me.SessionVariables.MemberData.Zip
            prms.latitudeIn = Me.SessionVariables.MemberData.latitude
            prms.longitudeIn = Me.SessionVariables.MemberData.longitude
            If (QSCreterias.ddlDistanceSelectedValue > 0) Then
                prms.Distance = QSCreterias.ddlDistanceSelectedValue
            End If
            prms.LookingFor_ToMeetMaleID = Me.SessionVariables.MemberData.LookingFor_ToMeetMaleID
            prms.LookingFor_ToMeetFemaleID = Me.SessionVariables.MemberData.LookingFor_ToMeetFemaleID
            prms.NumberOfRecordsToReturn = 0
            prms.AdditionalWhereClause = sql
            prms.performCount = True
            prms.rowNumberMin = NumberOfRecordsToReturn - 8
            prms.rowNumberMax = NumberOfRecordsToReturn
            Dim countRows As Integer
            Using ds As DataSet = clsSearchHelper.GetMembersToSearchDataTableQuick(prms)
                If (prms.performCount) Then
                    countRows = ds.Tables(0).Rows(0)(0)
                    If (countRows > 0) Then
                        Using dt As DataTable = ds.Tables(1)
                            FillOfferControlList(dt, UcQuickSearch)
                        End Using
                    End If
                Else
                    countRows = ds.Tables(0).Rows.Count
                    If (countRows > 0) Then
                        Using dt As DataTable = ds.Tables(0)
                            FillOfferControlList(dt, UcQuickSearch)
                        End Using
                    End If
                End If
                If Me.GetRecordsToRetrieve = 1 Then
                    UcQuickSearch.showPreviousButton = False
                Else
                    UcQuickSearch.showPreviousButton = True
                End If
                If prms.rowNumberMin + 8 >= countRows And countRows <= prms.rowNumberMax + 8 Then
                    UcQuickSearch.showNextButton = False
                Else
                    UcQuickSearch.showNextButton = True
                End If
                UcQuickSearch.DataBind()

            End Using


        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")

            ' SetPager(0)
            UcQuickSearch.UsersList = Nothing
            UcQuickSearch.DataBind()
        End Try

    End Sub

    Private Sub FillOfferControlList(dt As DataTable, offersControl As Dating.Server.Site.Web.UcQuickSearch)
        Try
            Dim drDefaultPhoto As DSMembers.EUS_CustomerPhotosRow = DataHelpers.GetProfilesDefaultPhoto(Me.MasterProfileId)
            Dim uliCurrentMember As New clsNewQuickSearchUserListItem()
            uliCurrentMember.ProfileID = Me.MasterProfileId ' 
            uliCurrentMember.MirrorProfileID = Me.MirrorProfileId
            ' uliCurrentMember.LoginName = Me.GetCurrentProfile().LoginName
            uliCurrentMember.Genderid = Me.GetCurrentProfile().GenderId
            '  uliCurrentMember.ProfileViewUrl = ResolveUrl("~/Members/Profile.aspx?p=" & HttpUtility.UrlEncode(Me.GetCurrentProfile().LoginName))
            If (drDefaultPhoto IsNot Nothing) Then
                '     uliCurrentMember.ImageFileName = drDefaultPhoto.FileName
                '      uliCurrentMember.ImageUploadDate = drDefaultPhoto.DateTimeToUploading.ToLocalTime().ToString("d MMM, yyyy H:mm")
            End If
            '  uliCurrentMember.ImageUrl = ProfileHelper.GetProfileImageURL(Me.MasterProfileId, uliCurrentMember.ImageFileName, Me.SessionVariables.MemberData.GenderId, True, Me.IsHTTPS, PhotoSize.D150)
            '   uliCurrentMember.ImageThumbUrl = uliCurrentMember.ImageUrl
            Dim mins As Integer = clsConfigValues.Get__members_online_minutes()
            Dim LastActivityUTCDate As DateTime = Date.UtcNow.AddMinutes(-mins)
            Dim hours As Integer = clsConfigValues.Get__members_online_recently_hours()
            Dim LastActivityRecentlyUTCDate As DateTime = Date.UtcNow.AddHours(-hours)
            Dim CurrentROw As Integer = 0
            For rowsCount As Integer = 0 To dt.Rows.Count - 1
                CurrentROw += 1
                Try
                    Dim dr As DataRow = dt.Rows(rowsCount)
                    Dim uli As New clsNewQuickSearchUserListItem()
                    uli.RowNumber = CurrentROw
                    uli.LAGID = Session("LAGID")
                    uli.ProfileID = uliCurrentMember.ProfileID
                    uli.MirrorProfileID = uliCurrentMember.MirrorProfileID
                    '   uli.LoginName = uliCurrentMember.LoginName
                    uli.Genderid = uliCurrentMember.Genderid
                    '   uli.ProfileViewUrl = uliCurrentMember.ProfileViewUrl
                    '   uli.ImageFileName = uliCurrentMember.ImageFileName
                    '    uli.ImageUrl = uliCurrentMember.ImageUrl
                    '   uli.ImageThumbUrl = uliCurrentMember.ImageThumbUrl
                    '     uli.ImageUploadDate = uliCurrentMember.ImageUploadDate
                    If (dt.Columns.Contains("Birthday")) Then uli.OtherMemberBirthday = clsNullable.DBNullToDateTimeNull(dr("Birthday"))
                    uli.OtherMemberLoginName = dr("LoginName")
                    uli.OtherMemberProfileID = dr("ProfileID")
                    uli.OtherMemberMirrorProfileID = dr("MirrorProfileID")
                    uli.OtherMemberCity = dr("City").ToString()
                    uli.OtherMemberRegion = dr("Region").ToString()
                    uli.OtherMemberCountry = dr("Country").ToString()
                    uli.OtherMemberGenderid = dr("Genderid")
                    uli.OtherMemberIsFemale = ProfileHelper.IsFemale(uli.OtherMemberGenderid)
                    uli.OtherMemberIsMale = ProfileHelper.IsMale(uli.OtherMemberGenderid)
                    uli.OtherMemberGenderClass = If(uli.OtherMemberIsFemale, "other-female", "other-male")
                    uli.OtherMemberIsOnline = clsNullable.DBNullToBoolean(dr("IsOnline"), False)
                    If (Not dr.IsNull("Birthday")) Then
                        uli.OtherMemberAge = ProfileHelper.GetCurrentAge(dr("Birthday"))
                    Else
                        uli.OtherMemberAge = 20
                    End If
                    Dim __LastActivityDateTime As DateTime? = clsNullable.DBNullToDateTimeNull(dr("LastActivityDateTime"))
                    If (dt.Columns.Contains("IsOnlineNow")) Then
                        uli.OtherMemberIsOnline = clsNullable.DBNullToBoolean(dr("IsOnlineNow"))
                    Else
                        If (uli.OtherMemberIsOnline) Then
                            If (__LastActivityDateTime.HasValue) Then
                                uli.OtherMemberIsOnline = __LastActivityDateTime >= LastActivityUTCDate
                            Else
                                uli.OtherMemberIsOnline = False
                            End If
                        End If
                    End If

                    If (Not uli.OtherMemberIsOnline) Then
                        If (dt.Columns.Contains("IsOnlineRecently")) Then
                            uli.OtherMemberIsOnlineRecently = clsNullable.DBNullToBoolean(dr("IsOnlineRecently"))
                        Else
                            If (uli.OtherMemberIsOnline) Then
                                If (__LastActivityDateTime.HasValue) Then
                                    uli.OtherMemberIsOnlineRecently = __LastActivityDateTime >= LastActivityRecentlyUTCDate
                                Else
                                    uli.OtherMemberIsOnlineRecently = False
                                End If
                            End If
                        End If
                    End If
                    uli.PhotosCountString = globalStrings.GetCustomString("Member.Has.Photos.Count", uli.LAGID)
                    Try
                        uli.PhotosCountString = "(" & clsNullable.DBNullToInteger(dr("PhotosApproved")) & ")"
                    Catch ex As Exception
                        WebErrorSendEmail(ex, "PhotosCountString")
                    End Try

                    If (Not dr.IsNull("PersonalInfo_HairColorID")) Then
                        uli.OtherMemberHair = ProfileHelper.GetHairColorString(dr("PersonalInfo_HairColorID"), uli.LAGID)
                    End If

                    If (Not dr.IsNull("FileName")) Then
                        uli.OtherMemberImageFileName = dr("FileName")

                    End If
                    If (dt.Columns.Contains("HasPhoto")) Then
                        If (Not dr.IsNull("HasPhoto") AndAlso (dr("HasPhoto").ToString() = "1" OrElse dr("HasPhoto").ToString() = "True")) Then
                            ' user has photo
                            If (uli.OtherMemberImageFileName IsNot Nothing) Then
                                'has public photos
                                uli.OtherMemberImageUrl = ProfileHelper.GetProfileImageURL(uli.OtherMemberProfileID, uli.OtherMemberImageFileName, uli.OtherMemberGenderid, True, Me.IsHTTPS, PhotoSize.D350)
                            Else
                                'has private photos
                                uli.OtherMemberImageUrl = ProfileHelper.GetPrivateImageURL(uli.OtherMemberGenderid)
                            End If
                        Else
                            ' has no photo
                            uli.OtherMemberImageUrl = ProfileHelper.GetDefaultImageURL(uli.OtherMemberGenderid)
                        End If
                        If IsHTTPS AndAlso uli.OtherMemberImageUrl.StartsWith("http:") Then uli.OtherMemberImageUrl = "https" & uli.OtherMemberImageUrl.Remove(0, "http".Length)
                    Else
                        uli.OtherMemberImageUrl = ProfileHelper.GetProfileImageURL(uli.OtherMemberProfileID, uli.OtherMemberImageFileName, uli.OtherMemberGenderid, True, Me.IsHTTPS, PhotoSize.D350)
                    End If
                    uli.OtherMemberProfileViewUrl = ResolveUrl("~/Members/Profile.aspx?p=" & HttpUtility.UrlEncode(dr("LoginName")))

                    If (uli.OtherMemberIsMale AndAlso dt.Columns.Contains("PersonalInfo_DrinkingHabitID")) Then
                        Try
                            If (dr("PersonalInfo_DrinkingHabitID").ToString().Length > 0) Then
                                Dim PersonalInfo_DrinkingHabitID As Integer = DirectCast(dr("PersonalInfo_DrinkingHabitID"), Integer)
                                If (PersonalInfo_DrinkingHabitID > 1) Then
                                    uli.OtherMemberDrinkingText = ProfileHelper.GetDrinkingString(PersonalInfo_DrinkingHabitID, uli.LAGID)
                                    If (PersonalInfo_DrinkingHabitID > 2) Then uli.OtherMemberDrinkingClass = "drinks"
                                End If
                            End If
                        Catch ex As Exception
                            WebErrorSendEmail(ex, "")
                        End Try
                    End If

                    If (uli.OtherMemberIsMale AndAlso dt.Columns.Contains("PersonalInfo_SmokingHabitID")) Then
                        Try
                            If (dr("PersonalInfo_SmokingHabitID").ToString().Length > 0) Then
                                Dim PersonalInfo_SmokingHabitID As Integer = DirectCast(dr("PersonalInfo_SmokingHabitID"), Integer)
                                If (PersonalInfo_SmokingHabitID > 1) Then
                                    uli.OtherMemberSmokingText = ProfileHelper.GetSmokingString(PersonalInfo_SmokingHabitID, uli.LAGID)
                                    If (PersonalInfo_SmokingHabitID > 2) Then uli.OtherMemberSmokingClass = "smokes"
                                End If
                            End If
                        Catch ex As Exception
                            WebErrorSendEmail(ex, "")
                        End Try
                    End If

                    If (uli.OtherMemberIsFemale AndAlso Not dr.IsNull("PersonalInfo_HairColorID")) Then
                        Try

                            Dim PersonalInfo_HairColorID As Integer = DirectCast(dr("PersonalInfo_HairColorID"), Integer)
                            If (PersonalInfo_HairColorID > 10) Then
                                Dim str As String = ProfileHelper.GetHairColorString(PersonalInfo_HairColorID, "US")
                                str = " clr-" & str.ToLower().Replace(" ", "-")
                                uli.OtherMemberHairClass = str
                                uli.OtherMemberHairTooltip = Me.CurrentPageData.GetCustomString("HairColor.Tooltip") & "&nbsp;" & ProfileHelper.GetHairColorString(PersonalInfo_HairColorID, uli.LAGID)
                            End If
                        Catch ex As Exception
                            WebErrorSendEmail(ex, "")
                        End Try
                    End If

                    uli.ZodiacString = globalStrings.GetCustomString("Zodiac" & ProfileHelper.ZodiacName(uli.OtherMemberBirthday), uli.LAGID)
                    Try
                        If (dt.Columns.Contains("LookingFor_RelationshipStatusID") AndAlso
                            dr("LookingFor_RelationshipStatusID").ToString().Length > 0) Then
                            Dim LookingFor_RelationshipStatusID As Integer = DirectCast(dr("LookingFor_RelationshipStatusID"), Integer)

                            Dim Tooltip1 As String = ""
                            If (uli.OtherMemberIsFemale) Then
                                If LookingFor_RelationshipStatusID = 6 Then
                                    Tooltip1 = Me.CurrentPageData.GetCustomString("RelationshipStatus.Married.Female.Tooltip")
                                Else
                                    Tooltip1 = Me.CurrentPageData.GetCustomString("RelationshipStatus.Single.Female.Tooltip")
                                End If
                            End If
                            If (String.IsNullOrEmpty(Tooltip1)) Then Tooltip1 = ProfileHelper.GetRelationshipStatusString(LookingFor_RelationshipStatusID, Me.Session("LagID"))

                            uli.OtherMemberRelationshipTooltip = Tooltip1
                            If uli.OtherMemberIsFemale Then
                                If LookingFor_RelationshipStatusID = 6 Then
                                    uli.OtherMemberRelationshipClass = "woman-icon married"
                                Else
                                    uli.OtherMemberRelationshipClass = "woman-icon female_single"
                                End If
                            Else
                                If LookingFor_RelationshipStatusID = 6 Then
                                    uli.OtherMemberRelationshipClass = "man-icon married"
                                Else
                                    uli.OtherMemberRelationshipClass = "man-icon male_single"
                                End If
                            End If

                        End If
                    Catch ex As Exception
                        WebErrorSendEmail(ex, "")
                    End Try
                    If uli.OtherMemberIsFemale Then
                        Try
                            If (dt.Columns.Contains("BreastSizeID") AndAlso dr("BreastSizeID").ToString().Length > 0) Then

                                Dim BreastSizeID As Integer = DirectCast(dr("BreastSizeID"), Integer)
                                Dim str As String = ProfileHelper.GetBreastSizeString(BreastSizeID, "US")
                                If (Not String.IsNullOrEmpty(str)) Then
                                    str = " size-" & str.ToLower().Replace("+", "plus")
                                    uli.OtherMemberBreastSizeClass = str
                                    uli.OtherMemberBreastSizeTooltip = Me.CurrentPageData.GetCustomString("BreastSize.Tooltip") & "&nbsp;" & ProfileHelper.GetBreastSizeString(BreastSizeID, uli.LAGID)
                                End If
                            End If
                        Catch ex As Exception
                            WebErrorSendEmail(ex, "")
                        End Try
                    End If
                    offersControl.UsersList.Add(uli)
                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "")
                End Try

            Next
        Catch ex As Exception

        End Try
    End Sub
    'Function GetRecordsToRetrieve() As Integer
    '    Me.Pager.ItemsPerPage = Me.ItemsPerPage

    '    If (Me.Pager.PageIndex = -1) AndAlso (Me.Pager.ItemCount > 0) Then
    '        Me.Pager.PageIndex = 0
    '    End If

    '    If (Me.Pager.PageIndex > 0) Then
    '        Return (Me.Pager.ItemsPerPage * (Me.Pager.PageIndex + 1))
    '    End If
    '    Return Me.Pager.ItemsPerPage
    'End Function
    Public Shared Function GetListSubquery(Of T)(cb As List(Of T)) As String
        Dim sql As String = ""
        For cnt = 0 To cb.Count - 1
            If (cnt = cb.Count - 1) Then
                sql = sql & cb(cnt).ToString()
            Else
                sql = sql & cb(cnt).ToString() & ","
            End If
        Next
        Return sql
    End Function

    Private Function divNoResultsChangeCriteria() As Object
        Throw New NotImplementedException
    End Function

    Private Sub UcQuickSearch_NextPageClicked(sender As Object, e As EventArgs) Handles UcQuickSearch.NextPageClicked
        OldPageIndex = Me.GetRecordsToRetrieve() + 1
        BindFilteredResults()
    End Sub

    Private Sub UcQuickSearch_PreviousPageClicked(sender As Object, e As EventArgs) Handles UcQuickSearch.PreviousPageClicked
        OldPageIndex = Me.GetRecordsToRetrieve() - 1
        BindFilteredResults()
    End Sub

    Private Sub UcQuickSearch_RefreshClicked(sender As Object, e As EventArgs) Handles UcQuickSearch.RefreshClicked
        OldPageIndex = -1
        BindFilteredResults()
    End Sub
End Class
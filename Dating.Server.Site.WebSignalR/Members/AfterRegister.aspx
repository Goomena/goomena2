﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AfterRegister.aspx.vb" 
    MasterPageFile="~/Members/Members2Inner.Master" Inherits="Dating.Server.Site.Web.AfterRegister1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/v1/css/def-public3.css?v=5" />
    <style type="text/css">
        body { background-image:none;background-color:transparent;margin:0;padding:0;}
        .BulletedList { margin-left: 0px; padding-left: 6px; }
        .BulletedListItem a { color: Red; text-decoration: none; border-bottom-color: #f70; border-bottom-width: 1px; border-bottom-style: dashed; }
        ul.dxvsL{margin-left:0px !important;padding-left:0px;}
        ul.dxvsL li.dxvsE{margin-bottom:0px;padding-left:23px;background:url('//cdn.goomena.com/images2/pub2/error.png') no-repeat scroll 0 50%;}
        .dxvsValidationSummary td.dxvsRC { padding:0 0 0 20px;}
    </style>
    <script src="//cdn.goomena.com/Scripts/head.min.js" type="text/javascript"></script>
    <script type="text/javascript">

        function valSumm_VisibilityChanged(s, e) {
            jQuery(function ($) {
                //scrollToElem($(), 160);
            });
            btnRegisterClicked = false;
            HideLoading();
        }

        function field_Validation(s, e) {
            s.ValidateWithPatterns();
            e.isValid = s.GetIsValid();

            if (s == window["cbHeight"]) {
                if (s.GetSelectedIndex() == 0) e.isValid = false;
            }
            else if (s == window["cbHairClr"]) {
                if (s.GetSelectedIndex() == 0) e.isValid = false;
            }
            else if (s == window["cbEyeColor"]) {
                if (s.GetSelectedIndex() == 0) e.isValid = false;
            }
            else if (s == window["cbBodyType"]) {
                if (s.GetSelectedIndex() == 0) e.isValid = false;
            }
            else if (s == window["cbRelationshipStatus"]) {
                e.isValid = true;
            }
            if (e.isValid) s.SetIsValid(true, false);
        }

        function validateFields(err) {
            var result = true;
            try {
                var passFail = false, userFail = false;
                if (jsPassword) passFail = stringIsEmpty($.trim(jsPassword.GetValue()));
                if (jsUserName) userFail = stringIsEmpty($.trim(jsUserName.GetValue()));
                if (err) { userFail = passFail = true; }

                if (userFail) {
                    $('.jsUserName-error', '.login-input-wrap').show();
                    $('.jsUserName-ok', '.login-input-wrap').hide();
                }
                else {
                    $('.jsUserName-error', '.login-input-wrap').hide();
                    $('.jsUserName-ok', '.login-input-wrap').show();
                }
                if (passFail) {
                    $('.jsPassword-error', '.login-input-wrap').show();
                    $('.jsPassword-ok', '.login-input-wrap').hide();
                }
                else {
                    $('.jsPassword-error', '.login-input-wrap').hide();
                    $('.jsPassword-ok', '.login-input-wrap').show();
                }
                result = !userFail && !passFail;
            }
            catch (e) { }
            return result;
        }

        function validateFieldsReload() {
            setTimeout(function () { validateFields(1) }, 300);
        }


        function closeAdditionalInfo() {
            if (window.parent) {
                window.parent.closeAdditionalInfo();
            }
            else if (window.top) {
                window.top.closeAdditionalInfo();
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">


<div id="after-register-sections">
            <div class="regmidcontainer" id="regmidcontainer" runat="server">
                <div class="reg-box">
                    <div class="last-info-title-box">
                        <div class="last-info-title">
                            <asp:Literal ID="msg_LastInfoTitle" runat="server">κάτι τελευταίο</asp:Literal>
                        </div>
                    </div>
                    <div class="registerform" id="registerform" runat="server">

                        <h1 class="last-info-text font-semibold">
                            <asp:Literal ID="msg_LastInfoText" runat="server">Λίγα στοιχεία για σενα ακόμα</asp:Literal>
                        </h1>

                        <asp:BulletedList ID="serverValidationList" runat="server" Font-Size="14px" ForeColor="Red"
                            DisplayMode="HyperLink" CssClass="BulletedList">
                        </asp:BulletedList>

                        <dx:ASPxValidationSummary ID="valSumm" runat="server" 
                            ValidationGroup="RegisterGroup"
                            RenderMode="BulletedList" 
                            ShowErrorsInEditors="True" 
                            EncodeHtml="False">
                            <Paddings PaddingBottom="10px" />
                            <LinkStyle>
                                <Font Size="14px"></Font>
                            </LinkStyle>
                            <ClientSideEvents VisibilityChanged="valSumm_VisibilityChanged"/>
                        </dx:ASPxValidationSummary>


                        <div id="pe_formPersonalInfo">
                            <ul>
                                <li>
                                    <div class="lfloat" style="margin-bottom: 4px;">
                                        <asp:Label ID="msg_PersonalHeight" runat="server" Text="" CssClass="lblform"
                                            AssociatedControlID="cbHeight" />
                                    </div>
                                    <div class="login-input-wrap lfloat" style="margin-bottom: 4px;">
                                        <dx:ASPxComboBox ID="cbHeight" runat="server" EncodeHtml="False" ClientInstanceName="cbHeight">
                                            <ClientSideEvents Validation="field_Validation" />
                                            <ValidationSettings ValidationGroup="RegisterGroup" 
                                                ErrorDisplayMode="None"
                                                ErrorTextPosition="Right" 
                                                Display="Dynamic">
                                                <RequiredField ErrorText="" IsRequired="True" />
                                            </ValidationSettings>
                                        </dx:ASPxComboBox>
                                    </div>
                                    <div class="clear"></div>
                                </li>
                                <li>
                                    <div class="lfloat" style="margin-bottom: 4px;">
                                        <asp:Label ID="msg_PersonalBodyType" runat="server" Text="" CssClass="lblform"
                                            AssociatedControlID="cbBodyType" />
                                    </div>
                                    <div class="login-input-wrap lfloat" style="margin-bottom: 4px;">
                                        <dx:ASPxComboBox ID="cbBodyType" runat="server" EncodeHtml="False" ClientInstanceName="cbBodyType">
                                            <ClientSideEvents Validation="field_Validation" />
                                            <ValidationSettings ValidationGroup="RegisterGroup" 
                                                ErrorDisplayMode="None"
                                                ErrorTextPosition="Right" 
                                                Display="Dynamic">
                                                <RequiredField ErrorText="" IsRequired="True" />
                                            </ValidationSettings>
                                        </dx:ASPxComboBox>
                                    </div>
                                    <div class="clear"></div>
                                </li>
                                <li>
                                    <div class="lfloat" style="margin-bottom: 4px;">
                                        <asp:Label ID="msg_PersonalEyeClr" runat="server" Text="" CssClass="lblform"
                                            AssociatedControlID="cbEyeColor" />
                                    </div>
                                    <div class="login-input-wrap lfloat" style="margin-bottom: 4px;">
                                        <dx:ASPxComboBox ID="cbEyeColor" runat="server" EncodeHtml="False" ClientInstanceName="cbEyeColor">
                                            <ClientSideEvents Validation="field_Validation" />
                                            <ValidationSettings ValidationGroup="RegisterGroup" 
                                                ErrorDisplayMode="None"
                                                ErrorTextPosition="Right" 
                                                Display="Dynamic">
                                                <RequiredField ErrorText="" IsRequired="True" />
                                            </ValidationSettings>
                                        </dx:ASPxComboBox>
                                    </div>
                                    <div class="clear"></div>
                                </li>
                                <li>
                                    <div class="lfloat" style="margin-bottom: 4px;">
                                        <asp:Label ID="msg_PersonalHairClr" runat="server" Text="" CssClass="lblform"
                                            AssociatedControlID="cbHairClr" />
                                    </div>
                                    <div class="login-input-wrap lfloat" style="margin-bottom: 4px;">
                                        <dx:ASPxComboBox ID="cbHairClr" runat="server" EncodeHtml="False" ClientInstanceName="cbHairClr">
                                            <ClientSideEvents Validation="field_Validation" />
                                            <ValidationSettings ValidationGroup="RegisterGroup" 
                                                ErrorDisplayMode="None"
                                                ErrorTextPosition="Right" 
                                                Display="Dynamic">
                                                <RequiredField ErrorText="" IsRequired="True" />
                                            </ValidationSettings>
                                        </dx:ASPxComboBox>
                                    </div>
                                    <div class="clear"></div>
                                </li>
			                    <li class="grey-box">
                                    <div class="legend lfloat" style="margin-bottom: 4px;">
				                        <asp:Label ID="msg_WAYLFRelStatus" runat="server" Text="" CssClass="lblform" 
                                            AssociatedControlID="cbRelationshipStatus"/>
                                    </div>
				                    <div class="login-input-wrap lfloat">
                                        <dx:ASPxComboBox ID="cbRelationshipStatus" runat="server" EncodeHtml="False" ShowImageInEditBox="True" ClientInstanceName="cbRelationshipStatus">
                                            <ClientSideEvents Validation="field_Validation" />
                                            <ValidationSettings ValidationGroup="RegisterGroup" 
                                                ErrorDisplayMode="None"
                                                ErrorTextPosition="Right" 
                                                Display="Dynamic">
                                                <RequiredField ErrorText="" IsRequired="True" />
                                            </ValidationSettings>
                                        </dx:ASPxComboBox>	
				                    </div>
                                        <div class="clear"></div>
			                    </li>
                            </ul>
                            <div class="clear"></div>
                        </div>

                        <div class="form-actions">
                            <dx:ASPxButton ID="btnContinue" runat="server" CommandName="Insert"
                                ValidationGroup="RegisterGroup" 
                                EncodeHtml="False" 
                                EnableDefaultAppearance="False" 
                                Cursor="pointer"
                                EnableClientSideAPI="True" 
                                Native="true">
                                <Border BorderWidth="0px"></Border>
                                <BackgroundImage HorizontalPosition="50%" ImageUrl="//cdn.goomena.com/Images2/pub2/enter_button.png" Repeat="NoRepeat" VerticalPosition="0" />
                                <HoverStyle>
                                    <BackgroundImage HorizontalPosition="50%" ImageUrl="//cdn.goomena.com/Images2/pub2/enter_button.png" Repeat="NoRepeat" VerticalPosition="-124px" />
                                </HoverStyle>
                            </dx:ASPxButton>
                        </div>
                    </div>
                    <h1 class="reg-wellcome"><asp:Literal ID="msg_WellcomeMsg" runat="server"></asp:Literal></h1>
                </div>
            </div>
</div>

</asp:Content>


﻿Imports DevExpress.Web.ASPxUploadControl
Imports System.IO
Imports System.Drawing
Imports Dating.Server.Core.DLL
Imports DevExpress.Web.ASPxEditors
Imports DevExpress.Web.ASPxPopupControl

Public Class VerificationDocsEdit
    Inherits BaseUserControl


    Protected Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            'If (_pageData Is Nothing) Then _pageData = New clsPageData("~/Members/control.VerificationDocsEdit", Context)
            If (_pageData Is Nothing) Then
                _pageData = New clsPageData("~/Members/control.VerificationDocsEdit", Context)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property

    Private Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If (Me.Visible) Then

            Try
                'If (Not Page.IsPostBack) Then
                '    Session("VerDoc_Uploaded") = Nothing
                'End If


                LoadLAG()

                Page.Form.Enctype = "multipart/form-data"
                'If (Not Page.IsPostBack) Then
                '    TogglePersonalCollapsible(False)
                'End If

                gvDocs.DataBind()
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "Page_Load")
            End Try
        End If

    End Sub



    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        If (Me.Visible) Then
            Try
                Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
                AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "Page_PreRender")
            End Try
        End If

    End Sub


    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
    End Sub


    Protected Sub LoadLAG()
        Try

            divUploadStatus.Visible = False
            imgResult.ImageUrl = "~/Images/IconImages/button_ok.png"
            lblUploadResult.Text = CurrentPageData.GetCustomString("Success.Document.upload")

            BrowseButtonText.Text = CurrentPageData.GetCustomString("BrowseButton.Text")
            lblMaxFileSize.Text = CurrentPageData.GetCustomString(lblMaxFileSize.ID)
            btnUpload.Text = CurrentPageData.GetCustomString(btnUpload.ID)
            DocumentIDLabel.Text = CurrentPageData.GetCustomString("lblDocumentID")
            DocumentPassportLabel.Text = CurrentPageData.GetCustomString("lblDocumentPassport")
            DocumentPhoneBillLabel.Text = CurrentPageData.GetCustomString("lblDocumentPhoneBill")
            DocumentOtherLabel.Text = CurrentPageData.GetCustomString("lblDocumentOther")

            SetControlsValue(Me, CurrentPageData)

            msg_PubPhotos.Text = CurrentPageData.GetCustomString("msg_PubPhotos")
            lblPhotoLevel.Text = CurrentPageData.GetCustomString("lblPhotoLevelTitle")

            gvDocs.Columns("DateTimeToUploading").Caption = CurrentPageData.GetCustomString("lblUploadDate")
            gvDocs.Columns("OriginalName").Caption = CurrentPageData.GetCustomString("lblOriginalName")
            gvDocs.Columns("DocumentType").Caption = CurrentPageData.GetCustomString("lblDocumentType")
            gvDocs.Columns("Description").Caption = CurrentPageData.GetCustomString("lblDescription")

            If (Session("VerDoc_Uploaded") = "success") Then
                divUploadStatus.Visible = True
                imgResult.ImageUrl = "~/Images/IconImages/button_ok.png"
                lblUploadResult.Text = CurrentPageData.GetCustomString("Success.Document.upload")
                txtDescription.Text = ""
                Session("VerDoc_Uploaded") = Nothing
                DocumentIDRadioButton.Checked = True

            ElseIf (Session("VerDoc_Uploaded") = "fail") Then
                divUploadStatus.Visible = True
                imgResult.ImageUrl = "~/Images/IconImages/cnrdelete-all.png"
                lblUploadResult.Text = CurrentPageData.GetCustomString("Error.Document.upload")
                txtDescription.Text = ""
                DocumentIDRadioButton.Checked = True
                Session("VerDoc_Uploaded") = Nothing
                'cbDocType.SelectedIndex = 0
            Else
                divUploadStatus.Visible = False


            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpload.Click
        'Page.Form.Enctype = "multipart/form-data"
        If (fileUploader.HasFile) Then
            Try
                Dim uploadedFile As HttpPostedFile = fileUploader.PostedFile
                Dim fileName As String = uploadedFile.FileName
                If fileName.Equals("") Then
                    'lblUploadResult.Text = "Παρακαλώ, επιλέξτε αρχείο για ανέβασμα"
                    Return
                End If
                Dim extension As String = uploadedFile.FileName.ToLower().Substring(uploadedFile.FileName.Length - 4)
                If (String.Compare(extension, ".jpg", True) = 0) Or
                (String.Compare(extension, ".jpeg", True) = 0) Or
                (String.Compare(extension, ".gif", True) = 0) Or
                (String.Compare(extension, ".png", True) = 0) Or
                (String.Compare(extension, ".bmp", True) = 0) Or
                (String.Compare(extension, ".txt", True) = 0) Or
                (String.Compare(extension, ".pdf", True) = 0) Or
                (String.Compare(extension, ".doc", True) = 0) Or
                (String.Compare(extension, "docx", True) = 0) Then
                    If (uploadedFile.ContentLength > 0) And (uploadedFile.ContentLength <= 4194304) Then

                        'Στο σημείο αυτό, το αρχείο μας έχει περάσει με επιτυχία το Validation...


                        'ploadStatus.Text = ""
                        SavePostedFile(uploadedFile)


                        ' make redirect to same page to AVOID DOUBLE PHOTOS UPLOAD issue
                        ' issue hapens when user makes refresh of page and selects resubmition of previous request
                        Try
                            Dim url As String = clsCurrentContext.GetRefreshURL()
                            Response.Redirect(url)
                        Catch ex As System.Threading.ThreadAbortException
                        End Try
                        ' ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Error_Sending_Receiver_DoNot_Have_Photo", "$(function(){forceRefresh();})", True)
                    Else
                        imgResult.ImageUrl = "~/Images/IconImages/cnrdelete-all.png"
                        lblUploadResult.Text = CurrentPageData.GetCustomString("Uploader.ValidationSettings.MaxFileSizeErrorText")
                        divUploadStatus.Visible = True
                    End If
                Else
                    imgResult.ImageUrl = "~/Images/IconImages/cnrdelete-all.png"
                    lblUploadResult.Text = CurrentPageData.GetCustomString("Uploader.ValidationSettings.NotAllowedFileExtensionErrorText")
                    divUploadStatus.Visible = True
                End If
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try
        End If
    End Sub


    Private Function SavePostedFile(ByVal uploadedFile As HttpPostedFile) As String
        Session("VerDoc_Uploaded") = Nothing
        divUploadStatus.Visible = False

        Dim imgfilePath As String = ""
        Dim thumbfilePath As String = ""

        Dim _guid As Guid = Guid.NewGuid()

        Dim ext As String = uploadedFile.FileName
        ext = ext.Substring(ext.LastIndexOf(".") + 1)

        Dim filePath As String = _guid.ToString("N") & "." & ext


        Try

            Dim imgDir As String = String.Format(ProfileHelper.gMemberDocsDirectory, Me.MasterProfileId.ToString())
            imgDir = MapPath(imgDir)
            If Not System.IO.Directory.Exists(imgDir) Then
                System.IO.Directory.CreateDirectory(imgDir)
            End If
            imgfilePath = Path.Combine(imgDir, filePath)

            Dim buffer As Byte() = New Byte(uploadedFile.ContentLength) {}
            'uploadedFile.FileContent.Read(buffer, 0, uploadedFile.ContentLength)

            Dim original As FileInfo = New FileInfo(imgfilePath)
            Dim fileStream As FileStream = original.OpenWrite()
            Try
                uploadedFile.InputStream.CopyTo(fileStream)
                fileStream.Write(buffer, 0, buffer.Length)
            Catch ex As Exception
                imgResult.ImageUrl = "~/Images/IconImages/cnrdelete-all.png"
                lblUploadResult.Text = CurrentPageData.GetCustomString("Uploader.ValidationSettings.GeneralErrorText")
                divUploadStatus.Visible = True
                WebErrorMessageBox(Me, ex, "Save verification doc failed (1).")
            Finally
                fileStream.Close()
                fileStream.Dispose()
            End Try


            Dim ver As EUS_CustomerVerificationDoc = New EUS_CustomerVerificationDoc()
            ver.CustomerID = Me.MasterProfileId
            ver.DateTimeToUploading = DateTime.UtcNow
            ver.Description = txtDescription.Text

            If (ver.Description.Length > 1024) Then
                ver.Description = ver.Description.Remove(1024)
            End If

            If DocumentIDRadioButton.Checked Then
                ver.DocumentType = "ID"
            ElseIf DocumentPassportRadioButton.Checked Then
                ver.DocumentType = "Passport"
            ElseIf DocumentPhoneBillRadioButton.Checked Then
                ver.DocumentType = "Phone Bill"
            Else
                ver.DocumentType = "Other"
            End If

            ver.FileName = filePath
            ver.OriginalName = uploadedFile.FileName

            Me.CMSDBDataContext.EUS_CustomerVerificationDocs.InsertOnSubmit(ver)
            Me.CMSDBDataContext.SubmitChanges()


            DataHelpers.UpdateEUS_Profiles_MirrorSetStatusUpdating(Me.MasterProfileId, ProfileStatusEnum.Updating, ProfileModifiedEnum.UpdatingDocs)


            Dim profileRows As New clsProfileRows(Me.MasterProfileId)
            Try
                SendNotificationEmailToSupport(profileRows.GetMirrorRow(), ver)
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try

            txtDescription.Text = ""
            Session("VerDoc_Uploaded") = "success"
        Catch ex As Exception
            imgResult.ImageUrl = "~/Images/IconImages/cnrdelete-all.png"
            lblUploadResult.Text = CurrentPageData.GetCustomString("Uploader.ValidationSettings.GeneralErrorText")
            divUploadStatus.Visible = True
            WebErrorMessageBox(Me, ex, "Save verification doc failed (2).")
            Session("VerDoc_Uploaded") = "fail"
        End Try

        Return imgfilePath
    End Function


    Sub SendNotificationEmailToSupport(mirrorRow As DSMembers.EUS_ProfilesRow, ver As EUS_CustomerVerificationDoc)
        Try

            Dim toEmail As String = ConfigurationManager.AppSettings("gToEmail")
            Dim Content As String = globalStrings.GetCustomString("EmailSendToSupport_MemberNewVerificationDoc", "US")


            Content = Content.Replace("###LOGINNAME###", mirrorRow.LoginName)
            Content = Content.Replace("###EMAIL###", mirrorRow.eMail)


            Try
                Content = Content.Replace("###FILENAME###", ver.OriginalName)
            Catch ex As Exception
            End Try

            Try
                Dim siteUrl As String = ConfigurationManager.AppSettings("gSiteURL")
                Dim photoUrl As String = ""
                photoUrl = String.Format(ProfileHelper.gMemberDocsDirectoryView, ver.CustomerID) & ver.FileName
                photoUrl = System.Web.VirtualPathUtility.ToAbsolute(photoUrl)
                photoUrl = (siteUrl & photoUrl)
                'photoUrl = ProfileHelper.GetProfileImageURL(newPhoto.CustomerID, newPhoto.FileName, mirrorRow.GenderId, True)

                Content = Content.Replace("###FILEPATH###", photoUrl)
                Content = Content.Replace("blank###PHOTOURL###", photoUrl) ' cms editor issue, adds word blank
                Content = Content.Replace("###PHOTOURL###", photoUrl)
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "Verification doc")
            End Try



            Try
                Content = Content.Replace("###BIRTHDATE###", mirrorRow.Birthday)
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###AGE###", ProfileHelper.GetCurrentAge(mirrorRow.Birthday))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###GENDER###", ProfileHelper.GetGenderString(mirrorRow.GenderId, "US"))
                Content = Content.Replace("###SEX###", ProfileHelper.GetGenderString(mirrorRow.GenderId, "US"))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###COUNTRY###", ProfileHelper.GetCountryName(mirrorRow.Country))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###STATEREGION###", mirrorRow._Region)
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###CITY###", mirrorRow.City)
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###ZIP###", mirrorRow.Zip)
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###DATETOREGISTER###", mirrorRow.DateTimeToRegister)
            Catch ex As Exception
            End Try


            Try
                Content = Content.Replace("###PROFILEAPPROVEDYESNO###", IIf(mirrorRow.Status = 4, "YES", "NO"))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###IP###", clsHTMLHelper.CreateIPLookupLinks(Session("IP")))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###GEOIP###", Session("GEO_COUNTRY_CODE"))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###AGENT###", Request.Params("HTTP_USER_AGENT"))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###REFERRER###", clsHTMLHelper.CreateURLLink(IIf(Session("Referrer") IsNot Nothing, Session("Referrer"), "")))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###CUSTOMREFERRER###", Session("CustomReferrer"))
            Catch ex As Exception
            End Try
            Try
                Content = Content.Replace("###CUSTOMERID###", mirrorRow.ProfileID)
            Catch ex As Exception
            End Try
            Try
                Dim link As String = ConfigurationManager.AppSettings("gSiteURL") & _
                    "?logon_customer=" & mirrorRow.MirrorProfileID & "_" & mirrorRow.ProfileID
                Content = Content.Replace("###LOGONCUSTOMER###", link)
            Catch ex As Exception
            End Try


            Try
                Dim SearchEngineKeywords As String = IIf(Session("SearchEngineKeywords") Is Nothing, "", Session("SearchEngineKeywords"))
                Content = Content.Replace("###SEARCHENGINEKEYWORDS###", SearchEngineKeywords)
            Catch ex As Exception
            End Try


            Try
                Dim LandingPage As String = ""
                If (Not mirrorRow.IsLandingPageNull()) Then
                    LandingPage = mirrorRow.LandingPage
                    If (LandingPage Is Nothing) Then LandingPage = ""
                End If
                Content = Content.Replace("###LANDINGPAGE###", clsHTMLHelper.CreateURLLink(LandingPage))
            Catch ex As Exception
            End Try


            clsMyMail.SendMail(ConfigurationManager.AppSettings("gToEmail"), "New verification doc uploaded on Goomena.com", Content, True)

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub



    Public Function GetVerificationDocsForCustomer(CustomerID As Integer) As DSCustomer.EUS_CustomerVerificationDocsDataTable
        Dim ds As DSCustomer = DataHelpers.EUS_CustomerVerificationDocs_GetByCustomerID(CustomerID)
        Return ds.EUS_CustomerVerificationDocs
    End Function

    Protected Sub gvDocs_CustomColumnDisplayText(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewColumnDisplayTextEventArgs) Handles gvDocs.CustomColumnDisplayText
        If e.Column.FieldName = "DocumentType" Then
            If e.Value.ToString.Equals("ID") Then
                e.DisplayText = CurrentPageData.GetCustomString("lblDocumentID")
            ElseIf e.Value.ToString.Equals("Passport") Then
                e.DisplayText = CurrentPageData.GetCustomString("lblDocumentPhoneBill")
            ElseIf e.Value.ToString.Equals("Phone Bill") Then
                e.DisplayText = CurrentPageData.GetCustomString("lblDocumentPhoneBill")
            Else
                e.DisplayText = CurrentPageData.GetCustomString("lblDocumentOther")
            End If
        End If
    End Sub


    Protected Sub odsDocs_Selecting(sender As Object, e As System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs) Handles odsDocs.Selecting
        e.InputParameters("CustomerID") = Me.MasterProfileId
    End Sub
End Class
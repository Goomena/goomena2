﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Members/Members2Inner.Master" CodeBehind="MyLists.aspx.vb" Inherits="Dating.Server.Site.Web.MyLists" %>
<%@ Register src="../UserControls/SearchControl.ascx" tagname="SearchControl" tagprefix="uc2" %>
<%@ Register src="../UserControls/MemberLeftPanel.ascx" tagname="MemberLeftPanel" tagprefix="uc1" %>
<%@ Register src="~/UserControls/WhatIsIt.ascx" tagname="WhatIsIt" tagprefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
<div id="lists-view">

<div id="lists-view-top">

    <h3 id="h3Dashboard" class="page-title">
        <asp:Literal ID="lblItemsName" runat="server" />
        <asp:LinkButton ID="lnkViewDescription" runat="server" CssClass="more_info2" OnClientClick="OnMoreInfoClick('[contentUrl]', this, '[headerText]');return false;" />
    </h3>

    <div id="top-links-wrap">
    <table class="center-table" cellpadding="0" cellspacing="0">
        <tr>
            <td>
<ul id="top-links">
    <li runat="server" id="liWhoViewedMe"><asp:HyperLink ID="lnkWhoViewedMe" runat="server" 
                                        NavigateUrl="~/Members/MyLists.aspx?vw=whoviewedme" onclick="ShowLoading();" 
                                        CssClass="btn list-btn lnkWhoViewedMe">Who Viewed Me</asp:HyperLink></li>

    <li runat="server" id="liWhoFavoritedMe"><asp:HyperLink ID="lnkWhoFavoritedMe" runat="server" 
                                        NavigateUrl="~/Members/MyLists.aspx?vw=whofavoritedme" onclick="ShowLoading();" 
                                        CssClass="btn list-btn lnkWhoFavoritedMe">Who Favorited Me</asp:HyperLink></li>

    <li runat="server" id="liMyFavoriteList"><asp:HyperLink ID="lnkMyFavoriteList" runat="server" 
                                        NavigateUrl="~/Members/MyLists.aspx?vw=myfavoritelist" onclick="ShowLoading();" 
                                        CssClass="btn list-btn lnkMyFavoriteList">My Favorite List</asp:HyperLink></li>

    <li runat="server" id="liWhoSharedPhotos"><asp:HyperLink ID="lnkWhoSharedPhotos" runat="server" 
                                        NavigateUrl="~/Members/MyLists.aspx?vw=whosharedphotos" onclick="ShowLoading();" 
                                        CssClass="btn list-btn lnkWhoSharedPhotos">Who Shared Photos</asp:HyperLink></li>

    <li runat="server" id="liMyBlockedList"><asp:HyperLink ID="lnkMyBlockedList" runat="server" 
                                        NavigateUrl="~/Members/MyLists.aspx?vw=myblockedlist" onclick="ShowLoading();" 
                                        CssClass="btn list-btn lnkMyBlockedList">My Blocked List</asp:HyperLink></li>

    <li runat="server" id="liMyViewedList"><asp:HyperLink ID="lnkMyViewedList" runat="server" 
                                        NavigateUrl="~/Members/MyLists.aspx?vw=myviewedlist" onclick="ShowLoading();" 
                                        CssClass="btn list-btn lnkMyViewedList">My Viewed List</asp:HyperLink></li>
</ul>
<div class="clear"></div>
            </td>
        </tr>
    </table>
    </div>

    <div id="divFilter" runat="server" clientidmode="Static" class="lists-filters">
        <div class="lfloat cr-word">
            <dx:ASPxLabel ID="lblSortByText" runat="server" Text="" EncodeHtml="False">
            </dx:ASPxLabel>
        </div>
        <div class="lfloat">
            <dx:ASPxComboBox ID="cbSort" runat="server" class="update_uri" 
                AutoPostBack="True" EncodeHtml="False">
                <ClientSideEvents SelectedIndexChanged="ShowLoadingOnMenu" />
                <Items>
                    <dx:ListEditItem Text="Recent" Value="Recent" Selected="true"  />
                    <dx:ListEditItem Text="Oldest" Value="Oldest" />
                </Items>
            </dx:ASPxComboBox>
        </div>
        <div class="rfloat">
            <dx:ASPxComboBox ID="cbPerPage" runat="server" class="update_uri input-small" style="width: 60px;" 
                AutoPostBack="True" EncodeHtml="False">
                <ClientSideEvents SelectedIndexChanged="ShowLoadingOnMenu" />
                <Items>
                    <dx:ListEditItem Text="10" Value="10" Selected="true"  />
                    <dx:ListEditItem Text="25" Value="25" />
                    <dx:ListEditItem Text="50" Value="50" />
                </Items>
            </dx:ASPxComboBox>
        </div>
        <div class="rfloat cr-word">
            <dx:ASPxLabel ID="lblResultsPerPageText" runat="server" Text="" EncodeHtml="False">
            </dx:ASPxLabel>
        </div>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>

</div>


    <div class="l_wrap">
<asp:UpdatePanel ID="updOffers" runat="server">
        <ContentTemplate>
    <asp:MultiView ID="mvMyListsMain" runat="server">

    <asp:View ID="vwWhoViewedMe" runat="server"> 
        <uc2:SearchControl ID="whoViewedMeList" runat="server" 
            UsersListView="WhoViewedMeList"/>
    </asp:View>

    <asp:View ID="vwWhoFavoritedMe" runat="server">
    <uc2:SearchControl ID="whoFavoritedMeList" runat="server" 
        UsersListView="WhoFavoritedMeList"/>
    </asp:View>

    <asp:View ID="vwWhoSharedPhoto" runat="server">
    <uc2:SearchControl ID="whoSharedPhotoList" runat="server" 
        UsersListView="WhoSharedPhotoList"/>
    </asp:View>

    <asp:View ID="vwMyFavoriteList" runat="server">
    <uc2:SearchControl ID="myFavoriteList" runat="server" 
        UsersListView="MyFavoriteList"/>
    </asp:View>

    <asp:View ID="vwMyBlockedList" runat="server">
    <uc2:SearchControl ID="myBlockedList" runat="server" 
        UsersListView="MyBlockedList"/>
    </asp:View>

    <asp:View ID="vwMyViewedList" runat="server">
    <uc2:SearchControl ID="myViewedList" runat="server" 
        UsersListView="MyViewedList"/>
    </asp:View>
    </asp:MultiView>
        </ContentTemplate>
    </asp:UpdatePanel>
    </div>
    <div class="pagination">
        <table align="center">
        <tr>
            <td><dx:ASPxPager ID="Pager" runat="server" ItemCount="3" ItemsPerPage="1" 
                    RenderMode="Lightweight" CssFilePath="~/App_Themes/Aqua/{0}/styles.css" 
                    CssPostfix="Aqua" CurrentPageNumberFormat="{0}" 
                    SpriteCssFilePath="~/App_Themes/Aqua/{0}/sprite.css"
                    PageNumberFormat="<span onclick='ShowLoading();'>{0}</span>"
                    EncodeHtml="false"
                    SeoFriendly="Enabled">
                    <lastpagebutton visible="True">
                    </lastpagebutton>
                    <firstpagebutton visible="True">
                    </firstpagebutton>
                    <Summary Position="Left" Text="Page {0} of {1} ({2} members)" Visible="false" />
                </dx:ASPxPager></td>
        </tr>
        </table>
    </div>

</div>




<script type="text/javascript">
    $(window).ready(function () {
        scrollToLogin();
    });
</script>

<uc2:whatisit ID="WhatIsIt1" runat="server" />
</asp:Content>



﻿<%@ Page Title="" Language="vb" AutoEventWireup="true" MasterPageFile="~/Members/Members2Inner.Master"
    CodeBehind="ImportContacts.aspx.vb" Inherits="Dating.Server.Site.Web.ImportContacts" %>

<%@ Register Src="../UserControls/UsersList.ascx" TagName="UsersList" TagPrefix="uc2" %>
<%@ Register Src="../UserControls/MemberLeftPanel.ascx" TagName="MemberLeftPanel" TagPrefix="uc3" %>
<%@ Register Assembly="DevExpress.Web.v13.1, Version=13.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxDataView" TagPrefix="dx" %>
<%@ Register Src="../UserControls/WhatIsIt.ascx" TagName="WhatIsIt" TagPrefix="uc2" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="//js.live.net/v5.0/wl.js"></script>
    <script type="text/javascript">
        function PreOpenGmailPopup() {
            window.open('about:blank', 'popup_window', 'width=600,height=600,left=100,top=100,resizable=no');
        }
        function OpenGmailPopup() {
            var clientID = '<%=System.Web.HttpUtility.UrlEncode(ConfigurationManager.AppSettings("gmailclientid"))%>';
            var redirectURI = '<%=System.Web.HttpUtility.UrlEncode(Dating.Server.Site.Web.UrlUtils.GetURLWithScheme(ConfigurationManager.AppSettings("gmailreturnurl")))%>';
            var url ='https://accounts.google.com/o/oauth2/auth?client_id=' + clientID + '&redirect_uri=' + redirectURI + '&scope=https://www.google.com/m8/feeds/&response_type=code';
            window.open(url, 'popup_window', 'width=600,height=600,left=100,top=100,resizable=no');
        }

        function OpenYahooPopup() {
            window.open('about:blank', 'yahoo_popup_window', 'width=600,height=600,left=100,top=100,resizable=no');
        }
    </script>

    <script type ="text/javascript">
        var HOTMAIL_APP_CLIENT_ID = '<%=ConfigurationManager.AppSettings("hotmailclientid")%>';
        var HOTMAIL_REDIRECT_URL = '<%= Dating.Server.Site.Web.UrlUtils.GetURLWithScheme(ConfigurationManager.AppSettings("hotmailreturnurl"))%>';
        function OpenHotmailPopup() {

            WL.init({
                client_id: HOTMAIL_APP_CLIENT_ID,
                redirect_uri: HOTMAIL_REDIRECT_URL,
                scope: "wl.signin",
                response_type: "token"
            });

            WL.login({
                scope: ['wl.basic', 'wl.emails', 'wl.contacts_emails']
            }).then(
                function (response) {
                    WL.api({
                        path: "me/contacts",
                        method: "GET"
                    }).then(
                        function (response) {
                            var resultData = response.data;
                            var contacts = new Array;
                            for (i = 0; i < resultData.length; i++) {
                                var firstname = resultData[i].first_name;
                                var lastname = resultData[i].last_name;
                                var preferred_email = resultData[i].emails.preferred;
                                var currentContact = new contact(-1, firstname, lastname, preferred_email, -1);
                                contacts.push(currentContact);
                            }

                            sendHotmailContacs(contacts);
                        },
                        function (responseFailed) {
                            document.getElementById("infoArea").innerHTML =
                                "Error calling API: " + responseFailed.error.message;
                        }
                    );
                },
                function (responseFailed) {
                    document.getElementById("infoArea").innerHTML =
                        "Error signing in: " + responseFailed.error_description;
                }
            );
        }

        function sendHotmailContacs(contacts) {

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Members/ImportContacts.aspx/RetrieveHotmailContacts",
                data: JSON.stringify({ HotmailContacts: contacts }),
                dataType: "json",
                success: function () { alert("success"); } //Success: to be changed...
            });
        }

        function contact(associatedGoomenaProfileID, firstname, lastname, email, goomenaProfileID) {
            this.AssociatedGoomenaProfileID = associatedGoomenaProfileID;
            this.ContactFirstName = firstname;
            this.ContactLastName = lastname;
            this.ContactEmail = email;
            this.GoomenaProfileID = goomenaProfileID;
        }

        //function test() {

        //    var contacts = new Array;
        //    var contact1 = new contact(-1, "testname1", "testLasteName1", "test_mail1@mail.com", -1);
        //    contacts.push(contact1);
        //    var contact2 = new contact(-1, "testname2", "testLasteName2", "test_mail2@mail.com", -1);
        //    contacts.push(contact2);

        //    $.ajax({
        //        type: "POST",
        //        contentType: "application/json; charset=utf-8",
        //        url: "/Members/ImportContacts.aspx/RetrieveHotmailContacts",
        //        data: JSON.stringify({ HotmailContacts: contacts }),
        //        dataType: "json",
        //        success: function () { alert("success"); } //Success: to be changed...
        //    });
        //}
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">

    <div class="main_container">
        <h1 class="main_header">
            <asp:Label ID="lblHeader" runat="server" Text="Import Contacts"></asp:Label>
        </h1>

        <div class="main_content">
            <table>
                <tr>
                    <td><asp:Button ID="btnImportGmailContacts" runat="server" Text="Import Gmail Contacts" OnClientClick="OpenGmailPopup();return false;" />
                        <br />
            <br /></td>
                    <td><input type="button" value="Pre-Open gmail popup" onclick="PreOpenGmailPopup();" /></td>
                </tr>
                <tr>
                    <td><asp:Button ID="btnImportYahooContacts" runat="server" Text="Import Yahoo Contacts" OnClientClick="OpenYahooPopup();" />
                        <br />
            <br /></td>
                    <td></td>
                </tr>
                <tr>
                    <td><asp:Button ID="btnImportHotmailContacts" runat="server" Text="Import Hotmail Contacts" onClientClick="javascript:OpenHotmailPopup();return false" UseSubmitBehavior="False" />
            <br />
            <br /></td>
                    <td></td>
                </tr>
                <tr>
                    <td><asp:Button ID="btnCountContacts" runat="server" Text="Count Contacts" UseSubmitBehavior="False" />
            <br />
            <br /></td>
                    <td><b><asp:Label ID="lblCountContacts" runat="server" Text=""></asp:Label></b></td>
                </tr>
            </table>
            
            <br />
            <br />
            
            <asp:GridView ID="gvContacts" runat="server">
            </asp:GridView>
            <br />
            <br />
            <div>
<asp:Label ID="lblLog" runat="server" Text=""></asp:Label>
                </div>
        </div>
    </div>

    <%--<div class="men_search_link_container">
        <asp:HyperLink ID="btnSearchMen" runat="server" CssClass="men_search_link" NavigateUrl="~/Members/Search.aspx" onclick="ShowLoading();"></asp:HyperLink>
    </div>--%>

    <uc2:WhatIsIt ID="WhatIsIt1" runat="server" />
</asp:Content>



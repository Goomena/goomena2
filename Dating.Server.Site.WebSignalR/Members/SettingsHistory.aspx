﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Members/Members2Inner.master" CodeBehind="SettingsHistory.aspx.vb" Inherits="Dating.Server.Site.Web.SettingsHistory" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">

        .middle#content-outer
        {
            margin-top: -30px;
        }

        .container_12#content
        {
            padding-top: 0px;
        }
        table.TextBoxItem { margin-top: 10px; }
        .TextBoxItem td { padding-bottom: 3px; }
        .BulletedList { margin-left: 0px; padding-left: 25px; }
        li.BulletedListItem { }
        .BulletedListItem a { color: Red; text-decoration: none; border-bottom-color: #f70; border-bottom-width: 1px; border-bottom-style: dashed; }
    </style>

    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    
    <div class="clear"></div>
    <div class="container_12" id="tabs-outter">
        <div class="grid_12 top_tabs">
            <div>
                <%--<ul class="defaultStyle">
                    <li runat="server" id="liAccSett">
                        <asp:HyperLink ID="lnkAccSett" runat="server" NavigateUrl="~/Members/Settings.aspx?vw=account"><span></span><asp:Literal ID="msg_TabTitleAccountSettings" runat="server" /></asp:HyperLink></li><li runat="server" id="liPrivacy">
                        <asp:HyperLink ID="lnkPrivacy" runat="server" NavigateUrl="~/Members/Settings.aspx?vw=privacy"><span></span><asp:Literal ID="msg_TabTitlePrivacy" runat="server" /></asp:HyperLink></li><li runat="server" id="liNotifications">
                        <asp:HyperLink ID="lnkNotifications" runat="server" NavigateUrl="~/Members/Settings.aspx?vw=notifications"><span></span><asp:Literal ID="msg_TabTitleNotifications" runat="server" /></asp:HyperLink></li><li runat="server" id="liCredits">
                        <asp:HyperLink ID="lnkCredits" runat="server" NavigateUrl="~/Members/Settings.aspx?vw=credits"><span></span><asp:Literal ID="msg_TabTitleCredits" runat="server" /></asp:HyperLink></li></ul>--%></div></div></div><div class="clear"></div>
	<div class="container_12" id="content_top">


	</div>
	<div class="container_12" id="content">
        <div class="grid_3 top_sidebar" id="sidebar-outter">
            <div class="left_tab" id="left_tab" runat="server">
                <div class="top_info">
                </div>
                <div class="middle" id="search">
                    <div class="submit">
                        <h3><asp:Literal ID="lblItemsName" runat="server" /></h3>
                        <div class="clear"></div>
                        <asp:LinkButton ID="lnkViewDescription" runat="server" CssClass="more_info2" OnClientClick="OnMoreInfoClick('[contentUrl]', this, '[headerText]');return false;" />
                    </div>
                </div>
                <div class="bottom">&nbsp;</div>
            </div>
		</div>

        <div class="grid_9 body">
            <div id="content-outter" class="middle" style="padding-top:0px;">

                <asp:UpdatePanel ID="updPanel1" runat="server" RenderMode="Inline">
                    <ContentTemplate>
                            
                                <h3 class="page-title" id="clear_history_title">
                                    <asp:Literal ID="msg_PSHistoryClear" runat="server" Text="Καθαρισμός Ιστορικού" />
                                </h3>
                                <asp:Panel ID="pnlPrivacySettings" runat="server"
                                    class="as_holder">
                                    <div class="as_body ab_block ab_settings ab_static">
                                        <div class="pe_wrapper">
                                            <div class="pe_box">
                                                <div class="settings_history_main_container">
                                                    <div class="rfloat history_back_link_container">
                                                        <dx:ASPxHyperLink ID="lnkHistoryBack" runat="server" Text="Back" CssClass="btn" NavigateUrl="~/Members/Settings.aspx">
                                                        </dx:ASPxHyperLink>
	                                                </div>
                                                    <asp:Panel ID="pnlPSUserMessage" runat="server" CssClass="alert alert-success blue" Visible="False">
                                                        <asp:Label ID="lblPSUserMessage" runat="server" Text="" />
                                                    </asp:Panel>
                                                    <ul class="defaultStyle">
                                                        <li class="settings_history_item_container">
                                                            <div class="settings_history_item_content" id="incoming_messages">
                                                                <div class="delete_button_container">
                                                                <asp:Button ID="btn_PSHistoryDeleteIncomingMessages" runat="server" Text="Delete" Enabled="false" UseSubmitBehavior="false" />
                                                                <%--<dx:ASPxCheckBox ID="chkPS_HideMeFromSearchResults" runat="server" EncodeHtml="False"
                                                                    Layout="Flow" />--%>
                                                                <div class="clear">
                                                                </div>
                                                            </div>
                                                            <label class="settingsLbl">
                                                                <asp:Literal ID="msg_PSHistoryDeleteIncomingMessages" runat="server" Text="Διαγραφή του ιστορικού των εισερχομένων μηνυμάτων μου" />
                                                                <br />
                                                                <asp:Label ID="msg_PSHistoryDeleteIncomingMessages_Count" runat="server" 
                                                                    Text="Incoming messages found [COUNT] until [DATE_UNTIL]" /></label>
                                                                </div>
                                                        </li>
                                                        <li class="settings_history_item_container">
                                                            <div class="settings_history_item_content" id="outgoing_messages">
                                                                <div class="delete_button_container">
                                                                <asp:Button ID="btn_PSHistoryDeleteOutgoingMessages" runat="server" Text="Delete" Enabled="false" UseSubmitBehavior="false"  />
                                                                <%--<dx:ASPxCheckBox ID="chkPS_HideMeFromMembersIHaveBlocked" runat="server" EncodeHtml="False"
                                                                    Layout="Flow" />--%>
                                                                <div class="clear">
                                                                </div>
                                                            </div>
                                                            <label class="settingsLbl">
                                                                <asp:Literal ID="msg_PSHistoryDeleteOutgoingMessages" runat="server" Text="Διαγραφή του ιστορικού των εξερχόμενων μηνυμάτων μου" />
                                                                <br />
                                                                <asp:Label ID="msg_PSHistoryDeleteOutgoingMessages_Count" runat="server" 
                                                                Text="Outgoing messages found [COUNT] until [DATE_UNTIL]" /></label>
                                                                </div>
                                                        </li>
                                                        <li class="settings_history_item_container">
                                                            <div class="settings_history_item_content" id="likes">
                                                                <div class="delete_button_container">
                                                                <asp:Button ID="btn_PSHistoryDeleteLIKES" runat="server" Text="Delete" Enabled="false" UseSubmitBehavior="false" />
                                                                <%--<dx:ASPxCheckBox ID="chkPS_NotShowInOtherUsersFavoritedList" runat="server" EncodeHtml="False"
                                                                    Layout="Flow" />--%>
                                                                <div class="clear">
                                                                </div>
                                                            </div>
                                                            <label class="settingsLbl">
                                                                <asp:Literal ID="msg_PSHistoryDeleteLIKES" runat="server" Text="Διαγραφή του ιστορικού των LIKE" />
                                                                <br />
                                                                <asp:Label ID="msg_PSHistoryDeleteLIKES_Count" runat="server"
                                                                Text="Likes made from me and to me are [COUNT] until [DATE_UNTIL]" /></label>
                                                                </div>
                                                        </li>
                                                        <li class="settings_history_item_container">
                                                            <div class="settings_history_item_content" id="offers">
                                                                <div class="delete_button_container">
                                                                <asp:Button ID="btn_PSHistoryDeleteOffers" runat="server" Text="Delete" Enabled="false" UseSubmitBehavior="false" />
                                                                <%--<dx:ASPxCheckBox ID="chkPS_NotShowInOtherUsersViewedList" runat="server" EncodeHtml="False"
                                                                    Layout="Flow" />--%>
                                                                <div class="clear">
                                                                </div>
                                                                </div>
                                                            <label class="settingsLbl">
                                                                <asp:Literal ID="msg_PSHistoryDeleteOffers" runat="server" 
                                                                Text="Διαγραφή του ιστορικού των Offers" />
                                                                <br />
                                                                <asp:Label ID="msg_PSHistoryDeleteOffers_Count" runat="server"
                                                                Text="Offers made from me and to me are [COUNT] until [DATE_UNTIL]" /></label>
                                                            </div>
                                                        </li>
                                                        <li class="settings_history_item_container">
                                                            <div class="settings_history_item_content" id="dates">
                                                                <div class="delete_button_container">
                                                                <asp:Button ID="btn_PSHistoryDeleteDates" runat="server" Text="Delete" Enabled="false" UseSubmitBehavior="false" />
                                                                <%--<dx:ASPxCheckBox ID="chkPS_HideMyLastLoginDateTime" runat="server" EncodeHtml="False"
                                                                    Layout="Flow" />--%>
                                                                <div class="clear">
                                                                </div>
                                                            </div>
                                                            <label class="settingsLbl">
                                                                <asp:Literal ID="msg_PSHistoryDeleteDates" runat="server" 
                                                                Text="Διαγραφή του ιστορικού των Dates" />
                                                                <br />
                                                                <asp:Label ID="msg_PSHistoryDeleteDates_Count" runat="server"
                                                                Text="Dates made from me and to me are [COUNT] until [DATE_UNTIL]" /></label>
                                                                </div>
                                                        </li>
                                                        <li class="settings_history_item_container">
                                                            <div class="settings_history_item_content" id="my_photos">
                                                                <div class="delete_button_container">
                                                                <asp:Button ID="btn_PSHistoryDeleteMySharedPhotos" runat="server" Text="Delete" Enabled="false" UseSubmitBehavior="false" />
                                                                <%--<dx:ASPxCheckBox ID="chkPS_DontShowOnPhotosGrid" runat="server" EncodeHtml="False"
                                                                    Layout="Flow" />--%>
                                                                <div class="clear">
                                                                </div>
                                                            </div>
                                                            <label class="settingsLbl">
                                                                <asp:Literal ID="msg_PSHistoryDeleteMySharedPhotos" runat="server" 
                                                                Text="Διαγραφή του ιστορικού των μοιρασμένων φωτογραφιών μου" />
                                                                <br />
                                                                <asp:Label ID="msg_PSHistoryDeleteMySharedPhotos_Count" runat="server" 
                                                                    Text="My shared photos are [COUNT] until [DATE_UNTIL]" /></label>
                                                                </div>
                                                        </li>
                                                        <li class="settings_history_item_container">
                                                            <div class="settings_history_item_content" id="other_photos">
                                                                <div class="delete_button_container">
                                                                <asp:Button ID="btn_PSHistoryDeleteSharedPhotosByOther" runat="server" Text="Delete" Enabled="false" UseSubmitBehavior="false" />
                                                                <%--<dx:ASPxCheckBox ID="chkPSSuppressWarning_WhenReadingMessageFromDifferentCountry" runat="server" EncodeHtml="False"
                                                                    Layout="Flow" />--%>
                                                                <div class="clear">
                                                                </div>
                                                            </div>
                                                            <label class="settingsLbl">
                                                                <asp:Literal ID="msg_PSHistoryDeleteSharedPhotosByOther" 
                                                                runat="server" 
                                                                Text="Διαγραφή του ιστορικού των φωτογραφιών που μου μοιράστηκαν" />
                                                                <br />
                                                                <asp:Label ID="msg_PSHistoryDeleteSharedPhotosByOther_Count" runat="server"
                                                                    Text="Shared photos shared by others to me are [COUNT] until [DATE_UNTIL]" /></label>
                                                                </div>
                                                        </li>
                                                        <li class="settings_history_item_container">
                                                            <div class="settings_history_item_content" id="profiles_me">
                                                                <div class="delete_button_container">
                                                                <asp:Button ID="btn_PSHistoryDeleteProfilesViewedByME" runat="server" Text="Delete" Enabled="false" UseSubmitBehavior="false" />
                                                                <%--<dx:ASPxCheckBox ID="chkPSHideMeFromUsersInDifferentCountry" runat="server" EncodeHtml="False"
                                                                    Layout="Flow" />--%>
                                                                <div class="clear">
                                                                </div>
                                                            </div>
                                                            <label class="settingsLbl">
                                                                <asp:Literal ID="msg_PSHistoryDeleteProfilesViewedByME" runat="server" 
                                                                Text="Διαγραφή του ιστορικού των Profile που έχω δει" />
                                                                <br />
                                                                <asp:Label ID="msg_PSHistoryDeleteProfilesViewedByME_Count" runat="server" 
                                                                    Text="Profiles I have viewed are [COUNT] until [DATE_UNTIL]" /></label>
                                                                </div>
                                                        </li>
                                                        <li class="settings_history_item_container">
                                                            <div class="settings_history_item_content" id="profiles_others">
                                                                <div class="delete_button_container">
                                                                <asp:Button ID="btn_PSHistoryDeleteProfilesViewedByOther" runat="server" Text="Delete" Enabled="false" UseSubmitBehavior="false" />
                                                                <%--<dx:ASPxCheckBox ID="chkPSHideMeFromUsersInDifferentCountry" runat="server" EncodeHtml="False"
                                                                    Layout="Flow" />--%>
                                                                <div class="clear">
                                                                </div>
                                                            </div>
                                                            <label class="settingsLbl">
                                                                <asp:Literal ID="msg_PSHistoryDeleteProfilesViewedByOther" runat="server" 
                                                                Text="Διαγραφή του ιστορικού των Profile που με έχουν δει" />
                                                                <br />
                                                                <asp:Label ID="msg_PSHistoryDeleteProfilesViewedByOther_Count" runat="server"
                                                                Text="Profiles who viewed my profile are [COUNT] until [DATE_UNTIL]" /></label>
                                                                </div>
                                                        </li>
                                                    </ul>
                                                    <div class="clear">
                                                    </div>
                                                    <%--<div class="form-actions">
                                                        <dx:ASPxButton ID="btnPrivacySettingsUpd" runat="server" CssClass="btn btn-primary"
                                                            Text="" Native="True" EncodeHtml="False" />
                                                    </div>--%>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clear">
                                    </div>
                                </asp:Panel>
                            <%--</div>--%>
                        <asp:LinkButton ID="lnkRefresh" runat="server" style="display:none;"></asp:LinkButton>
                    </ContentTemplate>
                </asp:UpdatePanel>
         
            </div>
            <div class="bottom">
            </div>
        </div>
	<div class="clear"></div>
</div>

</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="cntBodyBottom" runat="server">


 <dx:ASPxPopupControl runat="server"
        Height="385px"
        ID="popupConfirmAction"
        ClientInstanceName="popupConfirmAction" 
        ClientIDMode="AutoID" 
        AllowDragging="True" 
        PopupVerticalAlign="WindowCenter" 
        PopupHorizontalAlign="WindowCenter" 
        Modal="True" 
        AllowResize="True" 
        CloseAction="CloseButton"
        ShowShadow="False">
    <ContentStyle HorizontalAlign="Left" VerticalAlign="Middle">
        <Paddings Padding="0px" PaddingTop="15px" />
        <Paddings Padding="0px" PaddingTop="15px" ></Paddings>
    </ContentStyle>
    <CloseButtonStyle BackColor="White">
        <HoverStyle>
            <BackgroundImage ImageUrl="~/Images2/popup-wrn/close.png" Repeat="NoRepeat" HorizontalPosition="center"
                VerticalPosition="center"></BackgroundImage>
        </HoverStyle>
        <BackgroundImage ImageUrl="~/Images2/popup-wrn/close.png" Repeat="NoRepeat" HorizontalPosition="center"
            VerticalPosition="center"></BackgroundImage>
        <BorderLeft BorderColor="White" BorderStyle="Solid" BorderWidth="10px" />
    </CloseButtonStyle>
    <CloseButtonImage Url="~/Images/spacer10.png" Height="43px" Width="43px">
    </CloseButtonImage>
        <SizeGripImage Height="40px" Url="~/Images/resize.png" Width="40px">
        </SizeGripImage>
    <HeaderStyle>
        <Paddings Padding="0px" PaddingLeft="0px" />
        <Paddings PaddingTop="0px" PaddingBottom="0px"></Paddings>
        <BorderTop BorderColor="White" BorderStyle="Solid" BorderWidth="5px" />
        <BorderRight BorderColor="White" BorderStyle="Solid" BorderWidth="5px" />
        <BorderBottom BorderColor="White" BorderStyle="Solid" BorderWidth="5px" />
    </HeaderStyle>
<ModalBackgroundStyle BackColor="Black" Opacity="70"></ModalBackgroundStyle>
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server" SupportsDisabledAttribute="True">
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>

    <script type="text/javascript">
        function EndRequestHandler1(sender, args) {
            try {
                LoadingPanel.Hide();
            }
            catch (e) { }
            try {
                setActions();
                m__setPopupClass();
            }
            catch (e) { }
        }

        function setActions() {
            var wdt = 551, hgt = 385;
            __RefreshPage = false;
            if ($('#<%= btn_PSHistoryDeleteIncomingMessages.ClientID %>').attr('onclick') != "return false;") {
                $('#<%= btn_PSHistoryDeleteIncomingMessages.ClientID %>').removeAttr('disabled').click(function () {
                    var url = '<%= ResolveUrl("~/Members/SettingsHistoryClear.aspx?items=") & HttpUtility.UrlEncode("incoming-messages") %>&popup=popupConfirmAction';
                    openPopupSettingsHistoryClearWarning(url, '', wdt, hgt)
                })
            }
            if ($('#<%= btn_PSHistoryDeleteOutgoingMessages.ClientID %>').attr('onclick') != "return false;") {
                $('#<%= btn_PSHistoryDeleteOutgoingMessages.ClientID %>').removeAttr('disabled').click(function () {
                    var url = '<%= ResolveUrl("~/Members/SettingsHistoryClear.aspx?items=") & HttpUtility.UrlEncode("outgoing-messages") %>&popup=popupConfirmAction';
                    openPopupSettingsHistoryClearWarning(url, '', wdt, hgt)
                })
            }
            if ($('#<%= btn_PSHistoryDeleteLIKES.ClientID %>').attr('onclick') != "return false;") {
                $('#<%= btn_PSHistoryDeleteLIKES.ClientID %>').removeAttr('disabled').click(function () {
                    var url = '<%= ResolveUrl("~/Members/SettingsHistoryClear.aspx?items=") & HttpUtility.UrlEncode("likes")  %>&popup=popupConfirmAction';
                    openPopupSettingsHistoryClearWarning(url, '', wdt, hgt)
                })
            }
            if ($('#<%= btn_PSHistoryDeleteOffers.ClientID %>').attr('onclick') != "return false;") {
                $('#<%= btn_PSHistoryDeleteOffers.ClientID %>').removeAttr('disabled').click(function () {
                    var url = '<%= ResolveUrl("~/Members/SettingsHistoryClear.aspx?items=") & HttpUtility.UrlEncode("offers")  %>&popup=popupConfirmAction';
                    openPopupSettingsHistoryClearWarning(url, '', wdt, hgt)
                })
            }
            if ($('#<%= btn_PSHistoryDeleteDates.ClientID %>').attr('onclick') != "return false;") {
                $('#<%= btn_PSHistoryDeleteDates.ClientID %>').removeAttr('disabled').click(function () {
                    var url = '<%= ResolveUrl("~/Members/SettingsHistoryClear.aspx?items=") & HttpUtility.UrlEncode("dates")  %>&popup=popupConfirmAction';
                    openPopupSettingsHistoryClearWarning(url, '', wdt, hgt)
                })
            }
            if ($('#<%= btn_PSHistoryDeleteMySharedPhotos.ClientID %>').attr('onclick') != "return false;") {
                $('#<%= btn_PSHistoryDeleteMySharedPhotos.ClientID %>').removeAttr('disabled').click(function () {
                    var url = '<%= ResolveUrl("~/Members/SettingsHistoryClear.aspx?items=") & HttpUtility.UrlEncode("my-shared-photos")  %>&popup=popupConfirmAction';
                    openPopupSettingsHistoryClearWarning(url, '', wdt, hgt)
                })
            }
            if ($('#<%= btn_PSHistoryDeleteSharedPhotosByOther.ClientID %>').attr('onclick') != "return false;") {
                $('#<%= btn_PSHistoryDeleteSharedPhotosByOther.ClientID %>').removeAttr('disabled').click(function () {
                    var url = '<%= ResolveUrl("~/Members/SettingsHistoryClear.aspx?items=") & HttpUtility.UrlEncode("other-shared-photos") %>&popup=popupConfirmAction';
                    openPopupSettingsHistoryClearWarning(url, '', wdt, hgt)
                    
                })
            }
            if ($('#<%= btn_PSHistoryDeleteProfilesViewedByME.ClientID %>').attr('onclick') != "return false;") {
                $('#<%= btn_PSHistoryDeleteProfilesViewedByME.ClientID %>').removeAttr('disabled').click(function () {
                    var url = '<%= ResolveUrl("~/Members/SettingsHistoryClear.aspx?items=") & HttpUtility.UrlEncode("my-viewed-profiles") %>&popup=popupConfirmAction';
                    openPopupSettingsHistoryClearWarning(url, '', wdt, hgt)
                })
            }
            if ($('#<%= btn_PSHistoryDeleteProfilesViewedByOther.ClientID %>').attr('onclick') != "return false;") {
                $('#<%= btn_PSHistoryDeleteProfilesViewedByOther.ClientID %>').removeAttr('disabled').click(function () {
                    var url = '<%= ResolveUrl("~/Members/SettingsHistoryClear.aspx?items=") & HttpUtility.UrlEncode("other-viewed-profiles") %>&popup=popupConfirmAction';
                    openPopupSettingsHistoryClearWarning(url, '', wdt, hgt)
                })
            }
        }

        function openPopupSettingsHistoryClearWarning(contentUrl, headerText, wdt, hgt) {
            var popup = window["popupConfirmAction"]
            popup.SetContentUrl('<%= ResolveUrl("~/loading.htm") %>');
            popup.SetContentUrl(contentUrl);
            if (headerText != null) popup.SetHeaderText(headerText);
            popup.Show();

            if (typeof wdt !== 'undefined' && wdt != null) {
                if (typeof hgt !== 'undefined' && hgt != null) {
                    popup.SetSize(wdt, hgt);
                }
            }

            popup.UpdatePosition();
            popup.Closing.AddHandler(__Closing_Remove);
            popup.CloseUp.AddHandler(__CloseUp_Remove);


            //var popup = popupConfirmAction
            //var divId1 = popup.name + '_CIFD-1';

            function __Closing_Remove(s, e) {
                try {
                    if (__RefreshPage == true) {
                        eval($('#<%= lnkRefresh.ClientID %>').attr("href").replace("javascript:",""));
                    }
                    s.SetContentUrl('about:blank');
                    s.Closing.RemoveHandler(__Closing_Remove);
                }
                catch (e) { }
            }
            function __CloseUp_Remove(s, e) {
                try {
                    s.CloseUp.RemoveHandler(__CloseUp_Remove);
                }
                catch (e) { }
            }


        }

        var __RefreshPage = false;
        function SetRefreshPage() {
            __RefreshPage = true;
        }


        $(function () { setActions(); });
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler1);


        function m__setPopupClass() {
            var popup = popupConfirmAction
            var divId = popup.name + '_PW-1';
            $('#' + divId).addClass('fancybox-history-popup').prepend($('<div class="header-line"></div>'));
            var closeId = popup.name + '_HCB-1Img';
            $('#' + closeId).addClass('fancybox-popup-close');
            var hdrId = popup.name + '_PWH-1T';
            $('#' + hdrId).addClass('fancybox-popup-header').css('text-align', 'left');
            //var frmId = popup.name + '_CSD-1';
            //$('#' + frmId).addClass('iframe-container');
            //ctl00_cntBodyBottom_popupConfirmAction_CIFD-1
        }

        $(function () {
            setTimeout(m__setPopupClass, 300);
        });
    
    </script>

</asp:Content>

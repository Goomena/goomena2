// <![CDATA[
var passwordValidation_DiffThanLogin = "Your Password must not be the same as your Username.";

function RegisterForm_LoadAsyncValidation() {
    jQuery(function ($) {
        $(document).ready(function () {
            $('.txtEmail').focusout(checkEmail);
            $('.txtLogin').focusout(checkUsername);
            $('.txtPasswrd').focusout(validatePassword);
        });

        function checkEmail() {

            $('.mailValidation').html('');

            if (RegisterEmailTextBox.GetIsValid()) {

                var email = $(".txtEmail input").val();
                if (email === "") {
                    return
                }

                $('.txtEmail input').addClass('loadingIndicator');

                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "Register2.aspx/checkEmailAvailability",
                    data: '{ "email": "' + email + '" }',
                    dataType: "json",
                    success: successMail
                });

            }
        }

        successMail = function (msg) {
            var data = eval("(" + msg.d + ")");
            var exists = data.Exists;
            var message = data.Message;
            $('.txtEmail input').removeClass('loadingIndicator');

            isEmailAvailable = !exists;
            isValid_Email(isEmailAvailable);

            if (exists == true) {
                //$('.mailValidation').css("color", "red").addClass('dxEditors_edtError').css("display", "block");
                $('.mailValidation').css("color", "red").html(message).show();
                //$('.txtEmail input').focus();
            }
            else {
                //$('.mailValidation').css("color", "green").removeClass('dxEditors_edtError').css("display", "none");
                $('.mailValidation').css("color", "green").html(message).hide();
            }
            //alert("Exists: " + exists + " Message: " + message);
        }


        function checkUsername() {

            $('.usernameValidation').html('')

            if (RegisterLoginTextBox.GetIsValid()) {

                var username = $(".txtLogin input").val();
                if (username === "") {
                    return
                }

                $('.txtLogin input').addClass('loadingIndicator');

                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "Register2.aspx/checkUsernameAvailability",
                    data: '{ "username": "' + username + '" }',
                    dataType: "json",
                    success: successUsername
                });

            }
        }

        successUsername = function (msg) {
            var data = eval("(" + msg.d + ")");
            var exists = data.Exists;
            var message = data.Message;
            $('.txtLogin input').removeClass('loadingIndicator');

            isUserNameAvailable = !exists;
            isValid_Login(isUserNameAvailable);

            if (exists == true) {
                $('.usernameValidation').css("color", "red").html(message).show();
                //$('.usernameValidation').css("color", "red").addClass('dxEditors_edtError').css("display", "block");
                //$('.txtLogin input').focus();
            }
            else {
                $('.usernameValidation').css("color", "green").html(message).hide();
                //$('.usernameValidation').css("color", "green").removeClass('dxEditors_edtError').css("display", "none");
            }
            //alert("Exists: " + exists + " Message: " + message);
        }
    });


    function validatePassword() {
        $('.passwordValidation').html('');
        $('.passwordValidation').css("color", "green").removeClass('dxEditors_edtError').css("display", "none");

        var username = $(".txtLogin input").val();
        var password = $(".txtPasswrd input").val();

        if (password == username && username !== "" && password !== "") {
            $('.passwordValidation').html(passwordValidation_DiffThanLogin);
            $('.passwordValidation').css("color", "red").addClass('dxEditors_edtError').css("display", "block");
            $(".txtPasswrd input").val('');
        }
    }
}
// ]]> 
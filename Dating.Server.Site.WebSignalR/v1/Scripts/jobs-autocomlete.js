﻿
$(function () {
    function split(val) { return val.split(/,\s*/); }
    function extractLast(term) { return split(term).pop(); }

    // don't navigate away from the field on tab when selecting an item
    $("#job_select")
      .bind("keydown", function (event) {
          if (event.keyCode === $.ui.keyCode.TAB &&
      $(this).data("ui-autocomplete").menu.active) {
              event.preventDefault();
          }
      })
      .autocomplete({
          source: function (request, response) {
              var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");

              function success(data) {
                  response($.grep(data, function (value) {
                      value = value.label || value.value || value;
                      return matcher.test(value);
                  }));
              }
              $.getJSON("/members/json.aspx?option=GetJobsList&term=" + extractLast(request.term), success);
          },
          search: function () {
              // custom minLength
              var term = extractLast(this.value);
              /*if (term.length < 2) {
              return false;
              }*/
          },
          focus: function () {
              // prevent value inserted on focus
              return false;
          },
          select: function (event, ui) {
              var terms = split(this.value);
              // remove the current input
              terms.pop();
              //// add the selected item
              //terms.push(ui.item.value);
              // add placeholder to get the comma-and-space at the end
              terms.push("");
              this.value = "";
              //this.value = terms.join(", ");

              var job = ui.item;
              //if ((isCurrentCity == false || isCurrentRegion == false || isCurrentCountry == false) && $('#job-' + place.id).length == 0) {
              var param = $.toJSON({
                  "job_id": job.id
              });
              $.ajax({
                  type: "POST",
                  url: "/Members/json.aspx/JobsSave",
                  data: param,
                  contentType: "application/json; charset=utf-8",
                  dataType: "json",
                  async: true
              });

              var html =
				'<p>' +
				'<span>' + job.value + '</span>' +
				'<input type="hidden" value="' + job.id + '" class="locationId" id="job-' + job.id + '"/>' +
				'<a href="#" class="remove_job" title="Remove" data-location="' + job.id + '"><img  src="https://cdn.goomena.com/Images/spacer10.png" border="0" alt="Remove" /></a>' +
				'</p><div class="clear"></div>';
              $('#selected_jobs').before(html);
              //}

              return false;
          }
      });

    // Delete wants to visit record
    $(document).on("click", ".remove_job", function () {
        var remove_block = $(this);
        var job_id = remove_block.attr('data-location');
        var param = $.toJSON({ "job_id": (stringIsEmpty(job_id) ? "" : job_id) });
        $.ajax({
            type: "POST",
            url: "/Members/json.aspx/JobsDelete",
            data: param,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true
        });
        $(this).parents('p').remove();
        return false;
    });

    $(document).on("keydown", "#selected_jobs", function (e) {
        e = e || window.event;
        var charCode = e.which || e.keyCode;
        if (charCode === 13) return false;
    });

});

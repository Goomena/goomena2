﻿

var LastNum = -1;
var IsmyRight = false;
var retries_list_loading = 5;
var _NoMoreRows = false;

function fillMatching() {
    try {
            var param = {}
            param = $.toJSON({ LastRowNum: LastNum, IsRight: IsmyRight });
            ShowLoading()
            $.ajax({
                type: "POST",
                url: "json.aspx/GetProfilesMatching",
                data: param,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                error: function () {
                    if (retries_list_loading > 0) {
                        setTimeout(function () { fillMatching() }, 3000);
                        retries_list_loading--;
                    }
                    else {
                        AddNewContent('');
                        retries_list_loading = 0;
                    }
                },
                success: function (msg) {
                    //jQuery.parseJSON();
                    var Result = eval("(" + msg.d + ")");
                  
                    if(Result){
                        if (Result.lastRowNum != LastNum) {
                            LastNum = Result.lastRowNum;
                            _NoMoreRows = Result.NoMoreRows;
                            IsmyRight = Result.IsLastLeft;
                            AddNewContent(Result.Html);
                           } else {
                            setTimeout(function () { fillMatching() }, 3000);
                        }
                    }
                    
                  
                                   },
                beforeSend: function () { },
                complete: function () { }
            });
    }
    catch (e) { }
}
function AddNewContent(htmlContent) {
    $('.ResultsArea', '#MatchingResults').append(htmlContent);
    imagesAllLoaded();
      HideLoading();
    if (_NoMoreRows == true) {
        HideButton();
    }
}
function HideButton() {
    var btnsMore = $('.ButtonMore', '#MatchingResults');
    btnsMore.hide();
}
function ShowButton() {
    var btnsMore = $('.ButtonMore', '#MatchingResults');
    btnsMore.show();
}
function HideLoading() {
    
    var btnsMore = $('.Loading', '#MatchingResults');
    btnsMore.hide();
    ShowButton();
}
function ShowLoading() {
    var btnsMore = $('.Loading', '#MatchingResults');
    btnsMore.show();
    HideButton();
}


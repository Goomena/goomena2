var consoleLog = true,
msg_ClickToExpand = '', msg_ClickToCollapse = '', btnRegisterClicked = false, LoginWithGoogle = '', Login_Validation = '',
Login_Required = '', Login_checkChars = [], Height_Required = '', BodyType_Required = '', EyeColor_Required = '',
HairClr_Required = '', Region_Required = '', City_Required = '', Zip_Required = '', Birthday_Required = '',
passwordValidation_DiffThanLogin = "Your Password must not be the same as your Username.", RegisterButtonClicked = false,
isRegionCityVisible = false, RegisterZipLastTextOnError = '', RegisterZipLastError = '', CheckingEmail = '',
CheckingUserName = '', PasswordConfirmationRequired = '', CheckingOnSrv = '', ZipChangedCallback = false;




function OnCountryChanged(cbCountry) {
    Validation_fieldCheckOnServer('#TrCountry', CheckingOnSrv);
    HideLoading();
    cbpnlZip.PerformCallback("country_" + cbCountry.GetValue().toString());
    ShowLoading();
}
function OnRegionChanged(cbRegion) {
    Validation_fieldCheckOnServer('#TrRegion', CheckingOnSrv);
    HideLoading();
    cbpnlZip.PerformCallback("region_" + cbRegion.GetValue().toString());
    ShowLoading();
}
function OnCityChanged(cbCity) {
    Validation_fieldCheckOnServer('#TrCity', CheckingOnSrv);
    HideLoading();
    cbpnlZip.PerformCallback("city_" + cbCity.GetValue().toString() + "_region_" + cbRegion.GetValue().toString());
    ShowLoading();
}
function ZipChanged(txtZip) {
    try {
        if (consoleLog) console.log('ZipChangedCallback: ' + ZipChangedCallback);
    }
    catch (e) { }

    if (ZipChangedCallback == true) { return; }
    ZipChangedCallback = true;
    Validation_fieldCheckOnServer('#TrRegion', CheckingOnSrv);
    Validation_fieldCheckOnServer('#TrZip', CheckingOnSrv);
    Validation_fieldCheckOnServer('#TrCity', CheckingOnSrv);
    HideLoading();
    cbpnlZip.PerformCallback("zip_" + txtZip.GetText());
    ShowLoading();
}

function valSumm_VisibilityChanged(s, e) {
    jQuery(function ($) {
        //scrollToElem($(), 160);
    });
    btnRegisterClicked = false;
    HideLoading();
}

function field_LostFocus(s, e) {
    try {
        if (consoleLog) console.log('LostFocus:' + s.name)
    } catch (e) { }
    s.Validate();
}

function field_GotFocus(s, e) {
    try {
        if (consoleLog) console.log('GotFocus:' + s.name)
    } catch (e) { }

    if (RegValidation.isContainsError(s) == true) {
        try {
            if (consoleLog) console.log('isContainsError: true')
        } catch (e) { }
        RegValidation.showErrorOnFocusedField(s);
    }
}

function cbCountry_SelectedIndexChanged(s, e) { OnCountryChanged(s); }
function cbCountry_EndCallback(s, e) { HideLoading(); }
function cbpnlZip_EndCallback(s, e) { HideLoading(); }
function cbpnlZip_EndCallback_v2(s, e) {
    try {
        if (consoleLog) console.log('ZipChangedCallback: ' + ZipChangedCallback);
    }
    catch (e) { }
    ZipChangedCallback = false;
    HideLoading();

    function setRegionCityValidation(s, e) {
        try {
            showRegionCity();

            field_Validation_v2_cbRegion(window["cbRegion"], e);
            if (window["cbRegion"].GetIsValid()) {
                field_Validation_v2_cbCity(window["cbCity"], e);
                if (window["cbCity"].GetIsValid()) {
                    field_Validation_v2_RegisterZip(window["RegisterZip"], e);
                    field_GotFocus(window["RegisterZip"], e)
                }
            }

        }
        catch (e) { }
    }

    setTimeout(function () {
        setRegionCityValidation(s, e);
    }, 100);
}
function cbpnlZip_CallbackError(s, e) {
    try {
        if (consoleLog) console.log('ZipChangedCallback: ' + ZipChangedCallback);
    }
    catch (e) { }
    ZipChangedCallback = false;
    HideLoading();

    if (!stringIsEmpty(e.message)) {
        // zip not found message
        var s1 = window["RegisterZip"];
        RegisterZipLastTextOnError = s1.GetText();
        RegisterZipLastError = e.message;
        field_Validation_v2_RegisterZip(s1, null)
        if (isRegionCityVisible)
            showRegionCity();

        e.handled = true;
    }
}
function cbpnlZip_BeginCallback(s, e) { ShowLoading(); }
function cbRegion_SelectedIndexChanged(s, e) { OnRegionChanged(s); }
function cbRegion_EndCallback(s, e) { HideLoading(); }
function cbCity_SelectedIndexChanged(s, e) { OnCityChanged(s); }
function txtZip_LostFocus(s, e) { ZipChanged(s); }
function CheckZip() { ZipChanged(window["RegisterZip"]); }

var isUserNameAvailable = true;
var isEmailAvailable = true;
function isValid_Login(isValid) {
    if (!isValid || !isUserNameAvailable) {
        $('.RegisterLoginTextBox-error', '#TrMyUsername').show();
        $('.RegisterLoginTextBox-ok', '#TrMyUsername').hide();
        window["RegisterLoginTextBox"].SetIsValid(false, false);
    }
    else {
        $('.RegisterLoginTextBox-error', '#TrMyUsername').hide();
        $('.RegisterLoginTextBox-ok', '#TrMyUsername').show();

        window["RegisterLoginTextBox"].SetIsValid(true, false);
    }
}

function showWaterMark5(s, e) {
    if (s == window["RegisterPassword"]) {
        if (stringIsEmpty($.trim(s.GetValue()))) {
            $('.text-watermark:hidden', '.RegisterPassword-wrap').show();
        }
    }
    else if (s == window["RegisterPasswordConfirm"]) {
        if (stringIsEmpty($.trim(s.GetValue()))) {
            $('.text-watermark:hidden', '.RegisterPasswordConfirm-wrap').show();
        }
    }
}
function hideWaterMark5(s, e) {
    if (s == window["RegisterPassword"]) {
        $('.text-watermark', '.RegisterPassword-wrap').hide();
    }
    else if (s == window["RegisterPasswordConfirm"]) {
        $('.text-watermark', '.RegisterPasswordConfirm-wrap').hide();
    }
}

function checkWaterMark5(s, e) {
    if (s == window["RegisterPassword"]) {
        if (stringIsEmpty($.trim(s.GetValue()))) {
            $('.text-watermark:hidden', '.RegisterPassword-wrap').show();
        }
        else {
            $('.text-watermark', '.RegisterPassword-wrap').hide();
        }
    }
    else if (s == window["RegisterPasswordConfirm"]) {
        if (stringIsEmpty($.trim(s.GetValue()))) {
            $('.text-watermark:hidden', '.RegisterPasswordConfirm-wrap').show();
        }
        else {
            $('.text-watermark', '.RegisterPasswordConfirm-wrap').hide();
        }
    }
}


function showRegionCity() {
    var link = jQuery('#btnForgotZip'),
        region = jQuery('#TrRegion'),
        city = jQuery('#TrCity');

    link.hide()
    region.show()
    city.show()
    isRegionCityVisible = true;
    //var status = jQuery('#<%= hdfLocationStatus.ClientID %>');
    //status.val("region")
}

function showRegions() {
    //var zip = jQuery('#< %= liLocationsZip.ClientID %>');
    //var zip = jQuery('#<%=  TrShowRegion.ClientID %>');
    var region = jQuery('#<%= TrRegion.ClientID %>');
    var city = jQuery('#<%= TrCity.ClientID %>');

    zip.hide()
    region.show()
    city.show()
    var status = jQuery('#<%= hdfLocationStatus.ClientID %>');
    status.val("region")
}

jQuery(function ($) {
    $('.pe_box1').each(function () {
        var pe_box = this;

        $('.collapsibleHeader', pe_box).click(function () {
            $('.pe_form', pe_box).toggle('fast', function () {
                // Animation complete.
                var isVisible = ($('.pe_form', pe_box).css('display') == 'none');
                if (isVisible) {
                    $('.togglebutton', pe_box).text("+");
                    $('.collapsibleHeader', pe_box).attr("title", msg_ClickToExpand);
                }
                else {
                    $('.togglebutton', pe_box).text("-");
                    $('.collapsibleHeader', pe_box).attr("title", msg_ClickToCollapse);
                }
            });

        });
    });
});



function OpenGoogleLoginPopup() {
    jsLoad(LoginWithGoogle);
}


function valSumm_VisibilityChanged_v2(s, e) {
    valSumm_VisibilityChanged(s, e)
}


var RegValidation = new function () {
    var t = this;


    var elementsWithErrors = null;// = new Array();
    var elementsLastErrorText = null;// = new Array();
    var validatingElements = new Array();
    var prevSetErroOnElement = null;
    var prevSetErroOnElementErrorText = null;
    var lastElementError = { element: null, error: null, value: null };

    t.fillValidatingElements = function () {
        var s = window["RegisterGenderRadioButtonList"];
        if (s != null) {
            validatingElements.push(s);
            s.LostFocus.AddHandler(field_LostFocus);
            s.GotFocus.AddHandler(field_GotFocus);
        }

        s = window["RegisterEmailTextBox"];
        if (s != null) {
            validatingElements.push(s);
            s.LostFocus.AddHandler(field_LostFocus);
            s.GotFocus.AddHandler(field_GotFocus);
        }

        s = window["RegisterLoginTextBox"];
        if (s != null) {
            validatingElements.push(s);
            s.LostFocus.AddHandler(field_LostFocus);
            s.GotFocus.AddHandler(field_GotFocus);
        }

        s = window["RegisterPassword"];
        if (s != null) {
            validatingElements.push(s);
            s.LostFocus.AddHandler(field_LostFocus);
            s.GotFocus.AddHandler(field_GotFocus);
        }

        s = window["RegisterPasswordConfirm"];
        if (s != null) {
            validatingElements.push(s);
            s.LostFocus.AddHandler(field_LostFocus);
            s.GotFocus.AddHandler(field_GotFocus);
        }

        s = window["Date_YearDDL"];
        if (s != null) {
            validatingElements.push(s);
            s.SelectedIndexChanged.AddHandler(field_Validation_v2_BirthDay);
            window["Date_MonthDDL"].SelectedIndexChanged.AddHandler(field_Validation_v2_BirthDay);
            window["Date_DayDDL"].SelectedIndexChanged.AddHandler(field_Validation_v2_BirthDay);

            s.LostFocus.AddHandler(field_Validation_v2_BirthDay);
            window["Date_MonthDDL"].LostFocus.AddHandler(field_Validation_v2_BirthDay);
            window["Date_DayDDL"].LostFocus.AddHandler(field_Validation_v2_BirthDay);

            s.GotFocus.AddHandler(field_GotFocus);
            window["Date_MonthDDL"].GotFocus.AddHandler(function (s1, e) { field_GotFocus(window["Date_YearDDL"], e) });
            window["Date_DayDDL"].GotFocus.AddHandler(function (s1, e) { field_GotFocus(window["Date_YearDDL"], e) });
        }

        s = window["RegisterZip"];
        if (s != null) {
            validatingElements.push(s);
            //s.LostFocus.AddHandler(field_LostFocus);
            //s.GotFocus.AddHandler(field_GotFocus);
        }

        s = window["cbRegion"];
        if (s != null) {
            validatingElements.push(s);
            s.LostFocus.AddHandler(field_LostFocus);
            s.GotFocus.AddHandler(field_GotFocus);
        }

        s = window["cbCity"];
        if (s != null) {
            validatingElements.push(s);
            s.LostFocus.AddHandler(field_LostFocus);
            s.GotFocus.AddHandler(field_GotFocus);
        }

        s = window["cbHeight"];
        if (s != null) {
            validatingElements.push(s);
            s.LostFocus.AddHandler(field_LostFocus);
            s.GotFocus.AddHandler(field_GotFocus);
        }

        s = window["cbBodyType"];
        if (s != null) {
            validatingElements.push(s);
            s.LostFocus.AddHandler(field_LostFocus);
            s.GotFocus.AddHandler(field_GotFocus);
        }

        s = window["cbEyeColor"];
        if (s != null) {
            validatingElements.push(s);
            s.LostFocus.AddHandler(field_LostFocus);
            s.GotFocus.AddHandler(field_GotFocus);
        }

        s = window["cbHairClr"];
        if (s != null) {
            validatingElements.push(s);
            s.LostFocus.AddHandler(field_LostFocus);
            s.GotFocus.AddHandler(field_GotFocus);
        }
    }

    t.isContainsError = function (s) {
        if (elementsWithErrors == null) { return false; }
        for (var c = 0 ; c < elementsWithErrors.length; c++) {
            if (elementsWithErrors[c] != null && elementsWithErrors[c].name == s.name)
                return true;
        }
    }

    var addElementWithErr = function (s, errorText) {
        if (elementsWithErrors == null) {
            elementsWithErrors = new Array();
            elementsLastErrorText = new Array();
            for (var c = 0 ; c < validatingElements.length; c++) {
                elementsWithErrors.push(null);
                elementsLastErrorText.push(null);
            }
        }

        for (var c = 0 ; c < validatingElements.length; c++) {
            if (validatingElements[c].name == s.name) {
                elementsWithErrors[c] = s;
                elementsLastErrorText[c] = { element: s, error: errorText, value: null };
                break;
            }
        }

    }


    function removeElementWithErr(s) {
        if (elementsWithErrors == null) return;
        for (var c = 0 ; c < validatingElements.length; c++) {
            if (validatingElements[c].name == s.name) {
                elementsWithErrors[c] = null;
                elementsLastErrorText[c] = null;
                break;
            }
        }
    }


    function findNextLeastIndex(s) {
        var showForS = false;
        if (elementsWithErrors == null) return showForS;

        for (var c = 0 ; c < elementsWithErrors.length; c++) {
            if (elementsWithErrors[c] != null) {
                if (elementsWithErrors[c].name == s.name) showForS = true;
                break;
            }
        }
        return showForS;
    }

    function findLastErrorForS(s) {
        var errForS = "";
        if (elementsLastErrorText == null) return errForS;

        for (var c = 0 ; c < elementsLastErrorText.length; c++) {
            if (elementsLastErrorText[c] != null) {
                var itm = elementsLastErrorText[c];
                if (itm.element.name == s.name) {
                    errForS = itm.error;
                    break;
                }
            }
        }
        return errForS;
    }


    function showNextError(errorText) {
        var errorOnLeastIndex = -1;
        if (elementsWithErrors != null) {
            var c = 0;
            for (; c < elementsWithErrors.length; c++) {
                if (elementsWithErrors[c] != null) {
                    errorOnLeastIndex = c;
                    break;
                }
            }
        }
        if (errorOnLeastIndex > -1) {
            var s = validatingElements[errorOnLeastIndex];

            if (s.name == window["Date_YearDDL"].name && stringIsEmpty(errorText)) {
                errorText = Birthday_Required;
            }

            if (stringIsEmpty(errorText)) errorText = findLastErrorForS(s);
            if (stringIsEmpty(errorText)) errorText = s.GetErrorText();
            SetErroOnElement($('#' + s.name), errorText);
        }
        else {
            var ef = $('.uiContextualLayerPositioner');
            ef.hide();
            elementsWithErrors = null;
            elementsLastErrorText = null;
        }
    }



    function showErrorOnField(s, errorText) {
        addElementWithErr(s, errorText);
        var showForS = findNextLeastIndex(s);
        if (showForS) {
            if (stringIsEmpty(errorText)) errorText = findLastErrorForS(s);
            if (stringIsEmpty(errorText)) errorText = s.GetErrorText();
            SetErroOnElement($('#' + s.name), errorText);
        }
    }

    t.showErrorOnFocusedField = function (s, errorText) {
        if (stringIsEmpty(errorText)) errorText = findLastErrorForS(s);
        if (stringIsEmpty(errorText)) errorText = s.GetErrorText();
        SetErroOnElement($('#' + s.name), errorText);
    }


    function SetErroOnElement(el, errorText) {
        var ef, _5633, container, efInner, s_name = $(el);
        if (prevSetErroOnElement == s_name[0].id && (prevSetErroOnElementErrorText == errorText || errorText == '')) {
            return;
        }


        function _onPosition() {
            if ($('.uiContextualLayerPositioner:visible').length > 0) {
                var width1, offset, offsetHeight, offsetLeft, p, w2, w3;

                offset = s_name.offset();
                offsetHeight = offset.top;
                offsetLeft = offset.left;
                p = s_name.parent();

                w2 = p.width();
                w3 = efInner.width();
                if (efInner.outerWidth() > 0) w3 = efInner.outerWidth();

                width1 = offsetLeft + w2 + w3 + 30;

                if (s_name[0].id == window["RegisterGenderRadioButtonList"].name) {
                    ef.css({ top: offsetHeight, width: width1 + 30 });
                }
                else if (s_name[0].id == window["Date_YearDDL"].name) {
                    offsetHeight = offsetHeight - 10;
                    ef.css({ top: offsetHeight, width: width1 + 37 });
                }
                else if (s_name[0].id == window["RegisterZip"].name) {
                    offsetHeight = offsetHeight - 10;
                    ef.css({ top: offsetHeight, width: width1 + 87 });
                }
                else {
                    offsetHeight = offsetHeight - 10;
                    ef.css({ top: offsetHeight, width: width1 });
                }
            }
        }

        $(window).off('resize', _onPosition);

        prevSetErroOnElement = s_name[0].id;
        prevSetErroOnElementErrorText = errorText;

        ef = $('.uiContextualLayerPositioner');
        container = $('.registernewcontainer');
        efInner = $('.uiContextualLayer');

        if ($('.uiContextualLayerPositioner:visible').length == 0) {
            ef.show();
        }
        _5633 = $('._5633', ef);
        _5633.html(errorText);
        _onPosition();
        scrollWin($('#' + s_name[0].id), -200, 500);

        $(window).on('resize', _onPosition);
    }

    //var windowResizeContextualLayerHandler = null;
    //if (windowResizeContextualLayerHandler == null) {
    //    windowResizeContextualLayerHandler = 1
    //}

    t.handleElement = function (s, isValid, errorText) {
        if (isValid) {
            removeElementWithErr(s);
            s.SetIsValid(true, false);
            showNextError(errorText);
        }
        else {
            showErrorOnField(s, errorText);
        }
    }


}

function field_Validation_v2_BirthDay(s, e) {
    if (s == null) { s = window["Date_YearDDL"]; }
    if (s != null) {
        var isValid1 = true, errorText1 = "";
        try {
            if (dcGetSelectedDate() == null) {
                isValid1 = false;
                errorText1 = Birthday_Required;
            }
        }
        catch (e) {
            isValid1 = false;
            errorText1 = Birthday_Invalid;
        }

        Validation_fieldSetUI('#TrMyBirthdate', isValid1);
        Validation_SetIsValid(s, isValid1)
        RegValidation.handleElement(s, isValid1, errorText1);
    }

}


function field_Validation_v2_RegisterZip(s, e) {
    if (s == null) { s = window["RegisterZip"]; }
    if (s != null) {
        var isValid1 = true, errorText1 = "";
        s.SetErrorText(Zip_Required);

        if (typeof s.GetText !== "undefined") {
            var text = s.GetText();
            text = $.trim(text);
            s.SetText(text);

            if (text == '') {
                isValid1 = false;
                errorText1 = Zip_Required;
            }
            else if (RegisterZipLastTextOnError == text) {
                isValid1 = false;
                errorText1 = RegisterZipLastError;
            }
        }
        else if (typeof s.GetSelectedIndex !== "undefined" && s.GetSelectedIndex() == -1) {
            isValid1 = false;
            errorText1 = Zip_Required;
        }

        Validation_fieldSetUI('#TrZip', isValid1);
        Validation_SetIsValid(s, isValid1)
        RegValidation.handleElement(s, isValid1, errorText1)
    }
}

function field_Validation_v2_cbRegion(s, e) {
    if (s == null) { s = window["cbRegion"]; }
    if (s != null) {
        var isValid1 = true, errorText1 = "";
        s.SetErrorText(Region_Required);

        if (s.GetSelectedIndex() == -1) {
            isValid1 = false;
            errorText1 = Region_Required;
        }
        Validation_fieldSetUI('#TrRegion', isValid1);
        Validation_SetIsValid(s, isValid1)
        RegValidation.handleElement(s, isValid1, errorText1);
    }
}
function field_Validation_v2_cbCity(s, e) {
    if (s == null) { s = window["cbCity"]; }
    if (s != null) {
        var isValid1 = true, errorText1 = "";
        s.SetErrorText(City_Required);

        if (s.GetSelectedIndex() == -1 && s.GetItemCount() > 0) {
            isValid1 = false;
            errorText1 = City_Required;
        }
        Validation_fieldSetUI('#TrCity', isValid1);
        Validation_SetIsValid(s, isValid1)
        RegValidation.handleElement(s, isValid1, errorText1)
    }
}


function hasDigits(text) {
    var num = 0;
    try {
        var c = 0;
        for (; c < text.length; c++) {
            var n1 = parseInt(text.charAt(c));
            if (!isNaN(n1)) {
                num++;
                n1 = parseInt(text.charAt(++c));
                if (!isNaN(n1)) num++;
            }
        }
    }
    catch (e) { }
    return num
}

function Validation_fieldSetUI(selector, isValid) {
    var el = $('img.check', selector);
    if (isValid) {
        el.show().attr('src', '//cdn.goomena.com/images2/pub2/check21.png');
        el.attr('title', '');
        $(selector).removeClass('error-field');
        $(selector).addClass('checked');

        if (selector == '#TrGender') {
            $('#' + window["RegisterGenderRadioButtonList"].name + '_RB0').removeClass('error-field');
            $('#' + window["RegisterGenderRadioButtonList"].name + '_RB1').removeClass('error-field');
            if (window["RegisterGenderRadioButtonList"].GetSelectedIndex() == 0) {
                $(selector).removeClass('me-female').addClass('me-male');
            }
            else if (window["RegisterGenderRadioButtonList"].GetSelectedIndex() == 1) {
                $(selector).removeClass('me-male').addClass('me-female');
            }
        }
    }
    else {
        el.show().attr('src', '//cdn.goomena.com/images2/pub2/error21.png');
        el.attr('title', '');
        $(selector).removeClass('checked');
        $(selector).addClass('error-field');

        if (selector == '#TrGender') {
            $('#' + window["RegisterGenderRadioButtonList"].name + '_RB0').addClass('error-field');
            $('#' + window["RegisterGenderRadioButtonList"].name + '_RB1').addClass('error-field');
        }
    }
}

function Validation_fieldCheckOnServer(selector, title) {
    var el = $('img.check', selector);
    el.show().attr('src', '//cdn.goomena.com/Images2/ajax-loader.gif');
    if (!stringIsEmpty(title)) { el.attr('title', title); }
    $(selector).removeClass('error-field');
    $(selector).removeClass('checked');
    $(selector).addClass('checking');
}

function Validation_SetIsValid(s, isValid) {
    if (!isValid)
        s.SetIsValid(false, false);
    else
        s.SetIsValid(true, false);
}


function field_Validation_v2(s, e) {
    var errorText = null;
    s.ValidateWithPatterns();
    e.isValid = s.GetIsValid();

    try {
        if (consoleLog) console.log('field_Validation_v2: ' + s.name);
    }
    catch (e) { }
    if (s == window["RegisterGenderRadioButtonList"]) {
        Validation_fieldSetUI('#TrGender', e.isValid);
        Validation_SetIsValid(s, e.isValid)
    }
    else if (s == window["RegisterEmailTextBox"]) {

        var text = s.GetText();
        text = $.trim(text);
        s.SetText(text);

        var isrequest = false;
        if (e.isValid && !stringIsEmpty(text)) {
            isrequest = RegisterForm_LoadAsyncValidation.checkEmail();
        }

        if (isrequest) {
            Validation_fieldCheckOnServer('#TrMyEmail', CheckingEmail);
        }
        else {
            Validation_fieldSetUI('#TrMyEmail', e.isValid);
            Validation_SetIsValid(s, e.isValid)
        }

    }
    else if (s == window["RegisterLoginTextBox"]) {

        var text = s.GetText();
        text = $.trim(text);
        s.SetText(text);

        var isrequest = false;
        if (stringIsEmpty(text)) {
            errorText = Login_Required;
            e.isValid = false;
        }

        if (e.isValid && !stringIsEmpty(text)) {
            isrequest = RegisterForm_LoadAsyncValidation.checkUsername();
        }

        if (isrequest) {
            Validation_fieldCheckOnServer('#TrMyUsername', CheckingUserName);
        }
        else {
            Validation_fieldSetUI('#TrMyUsername', e.isValid);
            Validation_SetIsValid(s, e.isValid)
        }

    }
    else if (s == window["RegisterPassword"]) {

        var text = s.GetText();
        text = $.trim(text);
        s.SetText(text);

        var checked = false;
        if (!stringIsEmpty(text)) {
            var patt = new RegExp("[A-Za-z]", 'gi');
            if (text.length > 2 && patt.test(text)) {
                checked = true;
            }
            else if (text.length > 5 && hasDigits(text) > 5) {
                checked = true;
            }
            if (text.indexOf('--') > -1 || text.indexOf('*/') > -1 || text.indexOf('/*') > -1) {
                checked = false;
            }
        }

        e.isValid = checked;

        if (e.isValid) {
            var username = window["RegisterLoginTextBox"].GetText();
            var password = text;

            var isInvalid = (password == username && username !== "" && password !== "");
            if (isInvalid) {
                errorText = passwordValidation_DiffThanLogin;
                e.isValid = false;
            }
        }

        Validation_fieldSetUI('#TrMyPassword', e.isValid);
        Validation_SetIsValid(s, e.isValid)

    }
    else if (s == window["RegisterPasswordConfirm"]) {

        var text = s.GetText();
        text = $.trim(text);
        s.SetText(text);

        var regPass = RegisterPassword.GetText();
        if (text == "")
            errorText = PasswordConfirmationRequired;

        e.isValid = (text != "" && regPass == text);
        Validation_fieldSetUI('#TrMyPasswordConfirm', e.isValid);
        Validation_SetIsValid(s, e.isValid)

    }
    else if (s == window["RegisterZip"]) {
        field_Validation_v2_RegisterZip(s, e);
    }
    else if (s == window["cbRegion"]) {
        field_Validation_v2_cbRegion(s, e);
    }
    else if (s == window["cbCity"]) {
        field_Validation_v2_cbCity(s, e);
    }
    else if (s == window["cbHeight"]) {
        if (s.GetSelectedIndex() == 0) {
            e.isValid = false;
            errorText = Height_Required;
        }

        Validation_fieldSetUI('#TrHeight', e.isValid);
        Validation_SetIsValid(s, e.isValid)

    }
    else if (s == window["cbBodyType"]) {
        if (s.GetSelectedIndex() == 0) {
            e.isValid = false;
            errorText = BodyType_Required;
        }

        Validation_fieldSetUI('#TrBodyType', e.isValid);
        Validation_SetIsValid(s, e.isValid)

    }
    else if (s == window["cbEyeColor"]) {
        if (s.GetSelectedIndex() == 0) {
            e.isValid = false;
            errorText = EyeColor_Required;
        }

        Validation_fieldSetUI('#TrEyeColor', e.isValid);
        Validation_SetIsValid(s, e.isValid)

    }
    else if (s == window["cbHairClr"]) {
        if (s.GetSelectedIndex() == 0) {
            e.isValid = false;
            errorText = HairClr_Required;
        }

        Validation_fieldSetUI('#TrHairClr', e.isValid);
        Validation_SetIsValid(s, e.isValid)

    }


    RegValidation.handleElement(s, e.isValid, errorText)

    if (RegisterButtonClicked == true) {
        if (s == window["RegisterPassword"]) {

            // allow this, the next validation control would be  RegisterPasswordConfirm
            if (window["RegisterPasswordConfirm"] != null) {
                var s1 = window["RegisterPasswordConfirm"];

                var regPass = RegisterPassword.GetText();
                var regPassCof = s1.GetText();
                var isValid1 = true, errorText1 = "";
                isValid1 = (regPassCof != "" && regPass == regPassCof);

                Validation_fieldSetUI('#TrMyPasswordConfirm', isValid1);
                Validation_SetIsValid(s1, isValid1)
                RegValidation.handleElement(s1, isValid1, errorText1);
            }

            // validate birth date
            field_Validation_v2_BirthDay();
            field_Validation_v2_RegisterZip();
            field_Validation_v2_cbRegion();
            field_Validation_v2_cbCity();

        }
    }
}


function btnRegister_click() {
    //elementsWithErrors = new Array();
    RegisterButtonClicked = false;
}




// <![CDATA[

var RegisterForm_LoadAsyncValidation = new function () {
    var t = this;

    var prevEmailText = "";
    var prevEmailRequestData = {};

    t.checkEmail = function () {
        $('.mailValidation').html('');
        var request = true;

        if (!RegisterEmailTextBox.GetIsValid()) {
            request = false;
        }
        else {
            var email = RegisterEmailTextBox.GetText();
            if (email === "") {
                request = false;
            }
            else if (prevEmailText != "" && prevEmailText == email) {
                if (prevEmailRequestData == [] || (!prevEmailRequestData.Exists && stringIsEmpty(prevEmailRequestData.Message))) {
                    request = false;
                }
                else if (prevEmailRequestData != null && (prevEmailRequestData.Exists || !stringIsEmpty(prevEmailRequestData.Message))) {
                    setTimeout(function () {
                        t.successMail(prevEmailRequestData);
                    }, 100);
                    request = false;
                }
            }
        }


        if (request == true) {
            //$('.txtEmail input').addClass('loadingIndicator');
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "Register.aspx/checkEmailAvailability",
                data: '{ "email": "' + email + '" }',
                dataType: "json",
                success: function (msg) {
                    var data = eval("(" + msg.d + ")");
                    prevEmailText = email;
                    prevEmailRequestData = data;

                    t.successMail(data);
                }
            });
        }

        return request;
    }

    t.successMail = function (data) {
        var exists = data.Exists;
        var message = data.Message;

        var s = window["RegisterEmailTextBox"];
        var isValid = (exists != true && stringIsEmpty(message));
        if (isValid) {
            prevEmailRequestData = [];
        }

        Validation_fieldSetUI('#TrMyEmail', isValid);
        Validation_SetIsValid(s, isValid)
        RegValidation.handleElement(s, isValid, message)
    }


    var prevUsernameText = "";
    var prevUsernameRequestData = {};

    t.checkUsername = function () {
        $('.usernameValidation').html('')

        var request = true;
        var username = window["RegisterLoginTextBox"].GetText();
        if (username === "") {
            request = false;
        }
        else if (prevUsernameText != "" && prevUsernameText == username) {
            if (prevUsernameRequestData == [] || (!prevUsernameRequestData.Exists && stringIsEmpty(prevUsernameRequestData.Message))) {
                request = false;
            }
            else if (prevUsernameRequestData != null && (prevUsernameRequestData.Exists || !stringIsEmpty(prevUsernameRequestData.Message))) {
                t.successUsername(prevUsernameRequestData);
                request = false;
            }
        }

        if (request == true) {
            //$('.txtLogin input').addClass('loadingIndicator');

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "Register.aspx/checkUsernameAvailability",
                data: '{ "username": "' + username + '" }',
                dataType: "json",
                success: function (msg) {
                    var data = eval("(" + msg.d + ")");
                    prevUsernameText = username;
                    prevUsernameRequestData = data;

                    t.successUsername(data);
                }
            });
        }

        return request;
    }

    t.successUsername = function (data) {
        var exists = data.Exists;
        var message = data.Message;

        var s = window["RegisterLoginTextBox"];
        var isValid = (exists != true && stringIsEmpty(message));
        if (isValid) {
            prevUsernameRequestData = [];
        }

        Validation_fieldSetUI('#TrMyUsername', isValid);
        Validation_SetIsValid(s, isValid)
        RegValidation.handleElement(s, isValid, message)
    }


    t.start = function () {
        jQuery(function ($) {
            $(document).ready(function () {
                //$('.txtEmail').focusout(t.checkEmail);
                //$('.txtLogin').focusout(t.checkUsername);
                //$('.txtPasswrd').focusout(t.validatePassword);
            });
        });
    }

}
// ]]> 
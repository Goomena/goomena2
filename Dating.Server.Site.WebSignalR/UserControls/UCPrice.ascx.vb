﻿Imports Dating.Server.Core.DLL
Imports Dating.Server.Datasets.DLL

Public Class Price
    Inherits System.Web.UI.UserControl


    Protected _pageData As clsPageData
    Protected ReadOnly Property CurrentPageData As clsPageData
        Get
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData("control.UCSelectProduct", Context, coe)
            End If
            Return _pageData
        End Get
    End Property

    Public Enum ViewEnum
        None = 0
        vwMember = 1
        vwMemberRecom = 2
        vwPublic = 3
        vwPublicRecom = 4
    End Enum


    Public Event PriceSelected(ByVal sender As Object, ByVal e As PriceSelectedEventArgs)


    Public Property View As ViewEnum
        Get
            If (ViewState("View") IsNot Nothing) Then Return ViewState("View")
            Return ViewEnum.vwPublic
        End Get
        Set(value As ViewEnum)
            ViewState("View") = value
        End Set
    End Property

    Public Property Credits As Integer
        Get
            If (ViewState("Credits") IsNot Nothing) Then Return ViewState("Credits")
            Return 0
        End Get
        Set(value As Integer)
            ViewState("Credits") = value
        End Set
    End Property

    Public Property Price As Integer
        Get
            If (ViewState("Price") IsNot Nothing) Then Return ViewState("Price")
            Return 0
        End Get
        Set(value As Integer)
            ViewState("Price") = value
        End Set
    End Property

    Public Property Duration As Integer
        Get
            If (ViewState("Duration") IsNot Nothing) Then Return ViewState("Duration")
            Return 0
        End Get
        Set(value As Integer)
            ViewState("Duration") = value
        End Set
    End Property


    Public Property IsRecommended As Boolean
        Get
            If (ViewState("IsRecommended") IsNot Nothing) Then Return ViewState("IsRecommended")
            Return False
        End Get
        Set(value As Boolean)
            ViewState("IsRecommended") = value
        End Set
    End Property

    Public Property MoneySymb As String
        Get
            If (ViewState("MoneySymb") IsNot Nothing) Then Return ViewState("MoneySymb")
            Return False
        End Get
        Set(value As String)
            ViewState("MoneySymb") = value
        End Set
    End Property

    Public Property SmallClass As String
        Get
            If (ViewState("SmallClass") IsNot Nothing) Then Return ViewState("SmallClass")
            Return ""
        End Get
        Set(value As String)
            ViewState("SmallClass") = value
        End Set
    End Property

    Public Property CreditsText As String
        Get
            If (ViewState("CreditsText") IsNot Nothing) Then Return ViewState("CreditsText")
            Return False
        End Get
        Set(value As String)
            ViewState("CreditsText") = value
        End Set
    End Property


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    End Sub



    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        If (Me.View = ViewEnum.vwMember) Then
            mv.SetActiveView(vwMember)

            If (Me.IsRecommended) Then
                mv.SetActiveView(vwMemberRecom)
            End If

        ElseIf (Me.View = ViewEnum.vwMemberRecom) Then
            mv.SetActiveView(vwMemberRecom)

        ElseIf (Me.View = ViewEnum.vwPublic) Then
            mv.SetActiveView(vwPublic)

            If (Me.IsRecommended) Then
                mv.SetActiveView(vwPublicRecom)
            End If

        ElseIf (Me.View = ViewEnum.vwPublicRecom) Then
            mv.SetActiveView(vwPublicRecom)

        End If

        If (Not Me.IsPostBack) Then
            lblExpireAfterPub.Text = CurrentPageData.GetCustomString("Price.Expire.After")
            lblExpireAfterPub.Text = lblExpireAfterPub.Text.Replace("[DURATION]", Me.Duration)
            lblExpireAfterPubRecom.Text = lblExpireAfterPub.Text
            lblExpireAfterMemb.Text = lblExpireAfterPub.Text
            lblExpireAfterMembRecom.Text = lblExpireAfterPub.Text
            Me.CreditsText = CurrentPageData.GetCustomString("Credits.Text")
        End If

    End Sub

    Public Sub SetPricing(pricing As EUS_Price)
        Me.Credits = pricing.Credits
        Me.Price = pricing.Amount
        Me.Duration = pricing.duration
        Me.IsRecommended = pricing.IsRecommended

        If (pricing.Currency = "LEK") Then
            Me.MoneySymb = "Lek"
            Me.SmallClass = " small"
        ElseIf (pricing.Currency = "TRY") Then
            Me.MoneySymb = "TL"
            Me.SmallClass = " small"
        Else
            Me.MoneySymb = "&euro;"
        End If
    End Sub

    Protected Sub imgCreditsP_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles imgCreditsP.Click, imgCreditsPR.Click, imgCreditsM.Click, imgCreditsMR.Click
        Dim Args As New PriceSelectedEventArgs(Me.Credits, Me.Price, Me.Duration)
        RaiseEvent PriceSelected(Me, Args)
    End Sub



End Class
﻿Imports Dating.Server.Core.DLL
Imports DevExpress.Web.ASPxEditors
Imports Dating.Server.Datasets.DLL

''' <summary>
''' This control is loaded when a MAN view the search
''' </summary>
''' <remarks></remarks>
Public Class UcMatchingResults
    Inherits BaseUserControl


    Protected Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData("control.UcMatchingResults", Context, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If (Not Me.IsPostBack) Then
                LoadLAG()
            End If
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Protected Sub LoadLAG()
        Try
            ButtonEditLink.HRef = ResolveUrl("~/Members/MatchingEdit.aspx")
            lblButtonEditLink.InnerText = CurrentPageData.GetCustomString("lblEdit")
            lblResultsHeader.Text = CurrentPageData.GetCustomString("lblResultsHeader")
            ButtonMoreLink.InnerText = CurrentPageData.GetCustomString("ButtonMoreLink")
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


End Class
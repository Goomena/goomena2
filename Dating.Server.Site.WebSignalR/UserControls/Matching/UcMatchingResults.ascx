﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UcMatchingResults.ascx.vb"
    Inherits="Dating.Server.Site.Web.UcMatchingResults" %>
<script src="http://cdn.goomena.com/v1/Scripts/matching.js?v2"></script>

<script type="text/javascript">
    var img = new Image();
    var imagesOK = 0;
    img.onload = function () {
        imagesOK++;
        imagesAllLoaded();
   };
    img.src = "http://cdn.goomena.com/Images2/Matching/heart_empty.png";

    var imagesAllLoaded = function () {
        if (imagesOK >= 1) {
            // all images are fully loaded an ready to use
         
            var x = document.getElementsByClassName("DrawCanvas");
            do{
            var i;
            for (i = 0; i < x.length; i++) {
                      start(x[i]);
            }
            x = document.getElementsByClassName("DrawCanvas");
            }
            while(x.length!=0)

        }
    };

    function start(canvas) {
        var ctx = canvas.getContext("2d");
        var pers = canvas.getAttribute("value");
        canvas.className = "Heart";
        // save the context state
        ctx.save();

        // draw the overlay
        ctx.drawImage(img, 0, 0,53,43);
    
        // change composite mode to source-in
        // any new drawing will only overwrite existing pixels
        ctx.globalCompositeOperation = "source-in";

        // draw a purple rectangle the size of the canvas
        // Only the overlay will become purple
        ctx.fillStyle ='#EE0558';


        var h = canvas.height - (canvas.height * pers / 100);
        ctx.fillRect(0, h, canvas.width, canvas.height - h);
        //var h = 43 - (43 * pers / 100);
        //ctx.fillRect(0, h, 53, 43 - h);
    
        // change the composite mode to destination-atop
        // any new drawing will not overwrite any existing pixels
        ctx.globalCompositeOperation = "destination-atop";

        // draw the full logo
        // This will NOT overwrite any existing purple overlay pixels
        ctx.drawImage(img, 0, 0, 53, 43);
    
        ctx.restore();
        ctx.font = 'bold 14px Segoe UI';
        ctx.textAlign = "center";
        ctx.fillStyle = '#FFFFFF';
        ctx.fillText(pers + "%", 27, 25);
        ctx.restore();
    }
 


</script>

<div id="MatchingResults">
        <div class="ButtonEdit">
               <a id="ButtonEditLink" class="ButtonEditLink" runat="server" href="" >
                   <div id="lblButtonEditLink" class="lblButtonEditLink" runat="server"></div>
               </a>
                   </div>
    <div class="ResultContainerTop">
        <asp:Literal ID="lblResultsHeader" runat="server" Text=""></asp:Literal>
    </div>
    <div class="ResultsArea">
     
    </div>
   <script type="text/javascript">
   



       $(function () {
           fillMatching();
       });

</script>
    <div class="bottomContainer">
        <div class="ButtonMore">
               <a id="ButtonMoreLink" class="ButtonMoreLink" runat="server" href="javascript:void(0);"  onclick="fillMatching();return false;">
                   
               </a>
            
        </div>
        <div class="Loading" style="display:none;">
           <img src="//cdn.goomena.com/images2/GOO-Loading.gif" alt="" align="middle">
        </div>
    </div>

</div>

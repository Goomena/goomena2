﻿Imports DevExpress.Web.ASPxPager
Imports Dating.Server.Core.DLL

Public Class ucCouponPreview
    Inherits BaseUserControl
    Private _Credits As Decimal = 0
    Public Property Credits As Decimal
        Get
            Return _Credits
        End Get
        Set(value As Decimal)
            _Credits = value
            lblCouponMoney.Text = value & "$"
            lblPreviewCouponCredits.Text = Credits
        End Set
    End Property
    '  Private _CouponName As String
    Public Property CouponName As String
        Get
            Return lblPreviewCouponName.Text
        End Get
        Set(value As String)
            lblPreviewCouponName.Text = value
        End Set
    End Property

    Public Property CouponCode As String
        Get
            Return lblCouponCode.Text
        End Get
        Set(value As String)
            lblCouponCode.Text = value
        End Set
    End Property
    Protected Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            'If (_pageData Is Nothing) Then _pageData = New clsPageData("control.OfferControl", Context)
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData("control.CouponPreview", Context, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub
    Protected Sub LoadLAG()
        Try
            Dim mail As String = Me.GetCurrentProfile().eMail
            If Mail Is Nothing Or Mail Is DBNull.Value Then Mail = ""
            lblPreviewCouponTitle.Text = CurrentPageData.VerifyCustomString("lblPreviewCouponTitle")
            lblPreviewCouponNotes1.Text = CurrentPageData.VerifyCustomString("lblPreviewCouponNotes1").Replace("[###Email###]", mail)
            lblPreviewCouponNotes2.Text = CurrentPageData.VerifyCustomString("lblPreviewCouponNotes2")
            lblCouponFirstLine.Text = CurrentPageData.VerifyCustomString("lblCouponFirstLine")
            lblCouponCreditsΤext.Text = CurrentPageData.VerifyCustomString("lblCouponCreditsΤext")
            lblCouponBotomNotes.Text = CurrentPageData.VerifyCustomString("lblCouponBotomNotes")
            ' lblDownloadText.Text = CurrentPageData.GetCustomString("lblDownloadText")
            lblResendText.Text = CurrentPageData.VerifyCustomString("lblResendText")
            '    lblPreviewCouponName.Text = mail
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Private Sub Page_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        Try
            If (Me.Visible) Then
                LoadLAG()
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "HideLoading1", "HideLoading()", True)
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub
End Class
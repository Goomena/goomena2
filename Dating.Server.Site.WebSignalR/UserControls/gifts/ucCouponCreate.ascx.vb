﻿Imports DevExpress.Web.ASPxPager
Imports Dating.Server.Core.DLL
Imports Dating.Server.Datasets.DLL

Public Class ucCouponCreate
    Inherits BaseUserControl
    Public Event CreateCouponComplete(ByVal credits As Integer, ByVal name As String)
    Public ReadOnly Property lblClientId As String
        Get
            Return lblCouponMoney.ClientID
        End Get
    End Property
    Public ReadOnly Property MySpinEdit As Global.DevExpress.Web.ASPxEditors.ASPxSpinEdit
        Get
            Return spinCredits2
        End Get
    End Property
    Protected Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            'If (_pageData Is Nothing) Then _pageData = New clsPageData("control.OfferControl", Context)
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData("control.CouponCreate", Context, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            LoadLAG()
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub
    Private Sub InitSpin()
        Dim profile As DSMembers.EUS_ProfilesRow = DataHelpers.GetEUS_Profiles_ByProfileIDRow(Me.MasterProfileId)

        If (Not profile.IsREF_Payout_MinBalanceNull()) Then
            spinCredits2.Number = profile.REF_Payout_MinBalance
            spinCredits2.MinValue = profile.REF_Payout_MinBalance
        End If
    End Sub
    Protected Sub LoadLAG()
        Try
            'If (String.IsNullOrEmpty(CurrentPageData.VerifyCustomString("lnkNew"))) Then
            '    clsPageData.ClearCache()

            'End If
            If Not IsPostBack Then
                lblCouponCredits.Text = CurrentPageData.VerifyCustomString("lblCouponCredits")
                lblCouponName.Text = CurrentPageData.VerifyCustomString("lblCouponName")
                lblcreateCouponNotes.Text = CurrentPageData.VerifyCustomString("lblcreateCouponNotes")
                lblcreateCouponTitle.Text = CurrentPageData.VerifyCustomString("lblcreateCouponTitle")
                btnCouponCreateLabel.Text = CurrentPageData.VerifyCustomString("btnCouponCreateLabel")
                txtCouponName.Text = Me.SessionVariables.MemberData.Email
                InitSpin()
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Private Sub btnCouponCreate_Click(sender As Object, e As EventArgs) Handles btnCouponCreate.Click
        RaiseEvent CreateCouponComplete(spinCredits2.Number, txtCouponName.Text)
    End Sub
End Class
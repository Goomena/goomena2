﻿Imports Dating.Server.Core.DLL

Public Class ProfileCreditsHistory
    Inherits BaseUserControl


#Region "Props"


    Protected Overloads Property CurrentPageData As clsPageData
        Get
            ' let it read custom strings from container page
            'If (_pageData Is Nothing) Then _pageData = New clsPageData(Context)
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData(Context, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
        Set(value As clsPageData)
            _pageData = value
        End Set
    End Property

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            If (Not Page.IsPostBack) Then
                LoadLAG()
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try
    End Sub


    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
    End Sub


    Protected Sub LoadLAG()
        Try

            gv.Columns("Date").Caption = CurrentPageData.GetCustomString("CreditsHistoryTableCaption_Date")
            gv.Columns("Action").Caption = CurrentPageData.GetCustomString("CreditsHistoryTableCaption_Action")
            gv.Columns("Description").Caption = CurrentPageData.GetCustomString("CreditsHistoryTableCaption_Description")

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Public Sub Bind()
        gv.DataBind()
    End Sub

    Protected Sub gvHistory_CustomColumnDisplayText(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewColumnDisplayTextEventArgs) Handles gv.CustomColumnDisplayText

        Try
            If e.Column.FieldName = "Action" Then

                'Dim dr As System.Data.DataRow = gv.GetDataRow(e.VisibleRowIndex)
                'If (dr IsNot Nothing) Then

                '    If (dr("CustomerTransactionID") > 0) Then
                '        If (dr("Expired") = True) Then
                '            e.DisplayText = Me.CurrentPageData.GetCustomString("CreditsAction_Expired")
                '        Else
                '            e.DisplayText = Me.CurrentPageData.GetCustomString("CreditsAction_Credited")
                '        End If

                '    ElseIf (dr("CreditsType") = "UNLOCK_CONVERSATION") Then
                '        e.DisplayText = Me.CurrentPageData.GetCustomString("CreditsAction_Used")
                '    ElseIf (dr("CreditsType") = "UNLOCK_MESSAGE_READ") Then
                '        e.DisplayText = Me.CurrentPageData.GetCustomString("CreditsAction_Used")
                '    ElseIf (dr("CreditsType") = "UNLOCK_MESSAGE_SEND") Then
                '        e.DisplayText = Me.CurrentPageData.GetCustomString("CreditsAction_Used")
                '    End If

                'End If

            ElseIf e.Column.FieldName = "Date" Then
                Dim dr As System.Data.DataRow = gv.GetDataRow(e.VisibleRowIndex)
                If (Not dr.IsNull("Date")) Then
                    e.DisplayText = AppUtils.GetDateTimeString(dr("Date"), "", "dd/MM/yyyy H:mm")
                End If

            ElseIf e.Column.FieldName = "Description" Then

                Dim dr As System.Data.DataRow = gv.GetDataRow(e.VisibleRowIndex)
                Dim credits As Double = 0
                If (dr IsNot Nothing) Then
                    Dim loginName As String = dr("ConversationWithLoginName").ToString()
                    Dim profileUrl As String = ""
                    Dim DatePurchased As String = ""

                    If (Not String.IsNullOrEmpty(loginName)) Then
                        profileUrl = ProfileHelper.GetProfileNavigateUrl(loginName)
                        profileUrl = "<a href=""" & profileUrl & """ class=""login-name"">" & loginName.Replace("deleted_", "") & "</a>"
                    End If

                    If (Not dr.IsNull("DatePurchased")) Then DatePurchased = AppUtils.GetDateTimeString(CType(dr("DatePurchased"), DateTime), "", "d MMMM, yyyy H:mm")

                    If (Not dr.IsNull("Credits")) Then
                        credits = Convert.ToDouble(dr("Credits"))
                        credits = Math.Abs(credits)
                    End If

                    If (dr("CustomerTransactionID") > 0) Then

                        If (dr("IsSubscription") = True) Then
                            Dim Duration As Integer = If(Not dr.IsNull("Duration"), dr("Duration"), 0)

                            If (dr("Expired") = True) Then
                                e.DisplayText = Me.CurrentPageData.GetCustomString("CreditsDescription_Subscription_Expired").Replace("###PURCHASED###", DatePurchased).Replace("###DURATION###", Duration.ToString())
                            Else
                                e.DisplayText = Me.CurrentPageData.GetCustomString("CreditsDescription_Subscription_Credited").Replace("###DURATION###", Duration.ToString())
                            End If

                        Else
                            If (dr("Expired") = True) Then
                                e.DisplayText = Me.CurrentPageData.GetCustomString("CreditsDescription_Expired").Replace("###CREDITS###", credits.ToString()).Replace("###PURCHASED###", DatePurchased)
                                '"Your ###CREDITS### credits expired.<br/> Credits purchased on ###PURCHASED###"
                            Else
                                e.DisplayText = Me.CurrentPageData.GetCustomString("CreditsDescription_Credited").Replace("###CREDITS###", credits.ToString())
                                '"Purchased ###CREDITS### Credits"
                            End If
                        End If

                    ElseIf (dr("CreditsType") = "UNLOCK_CONVERSATION") Then
                        e.DisplayText = Me.CurrentPageData.GetCustomString("CreditsDescription_Used").Replace("###CREDITS###", credits.ToString()).Replace("###PROFILEVIEWURL###", profileUrl)

                    ElseIf (dr("CreditsType") = "UNLOCK_MESSAGE_READ") Then

                        If (dr("IsSubscription") = True) Then
                            e.DisplayText = Me.CurrentPageData.GetCustomString("CreditsDescription_UsedSubscription_READ").Replace("###PROFILEVIEWURL###", profileUrl)
                        Else
                            e.DisplayText = Me.CurrentPageData.GetCustomString("CreditsDescription_UsedUnlockMessage_READ").Replace("###CREDITS###", credits.ToString()).Replace("###PROFILEVIEWURL###", profileUrl)
                        End If

                    ElseIf (dr("CreditsType") = "UNLOCK_MESSAGE_SEND") Then

                        If (dr("IsSubscription") = True) Then
                            e.DisplayText = Me.CurrentPageData.GetCustomString("CreditsDescription_UsedSubscription_SEND").Replace("###PROFILEVIEWURL###", profileUrl)
                        Else
                            e.DisplayText = Me.CurrentPageData.GetCustomString("CreditsDescription_UsedUnlockMessage_SEND").Replace("###CREDITS###", credits.ToString()).Replace("###PROFILEVIEWURL###", profileUrl)
                        End If

                    ElseIf (dr("CreditsType") = "UNLOCK_MESSAGE_READ_FREE") Then
                        e.DisplayText = Me.CurrentPageData.GetCustomString("CreditsDescription_MESSAGE_READ_FREE").Replace("###PROFILEVIEWURL###", profileUrl)

                    ElseIf (dr("CreditsType") = "UNLOCK_MESSAGE_SEND_FREE") Then
                        e.DisplayText = Me.CurrentPageData.GetCustomString("CreditsDescription_MESSAGE_SEND_FREE").Replace("###PROFILEVIEWURL###", profileUrl)

                    End If
                End If

            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub

    Protected Sub sdsCreditsHistory_Selecting(sender As Object, e As System.Web.UI.WebControls.SqlDataSourceSelectingEventArgs) Handles sdsCreditsHistory.Selecting
        For Each prm As SqlClient.SqlParameter In e.Command.Parameters
            If (prm.ParameterName = "@CustomerId") Then
                prm.Value = Me.MasterProfileId
                Exit For
            End If
        Next
    End Sub


    Protected Sub gv_HtmlRowCreated(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs) Handles gv.HtmlRowCreated
        Try
            Dim i1 As Image = DirectCast(e.Row.Controls(1).Controls(0).Controls(0), DevExpress.Web.ASPxGridView.GridViewDataItemTemplateContainer).FindControl("i1")
            Dim lblAct As Label = DirectCast(e.Row.Controls(1).Controls(0).Controls(0), DevExpress.Web.ASPxGridView.GridViewDataItemTemplateContainer).FindControl("lblAct")
            'Dim i1 As Image = e.Row.Controls(1).FindControl("i1")

            Dim dr As System.Data.DataRow = gv.GetDataRow(e.VisibleIndex)
            If (dr IsNot Nothing) Then

                If (i1 IsNot Nothing) Then
                    If (dr("CustomerTransactionID") > 0) Then
                        If (dr("Expired") = True) Then
                            i1.CssClass = "credits-icon-expired"
                        Else
                            i1.CssClass = "credits-icon-credited"
                        End If

                    ElseIf (dr("CreditsType") = "UNLOCK_CONVERSATION") Then
                        i1.CssClass = "credits-icon-conversation"
                    ElseIf (dr("CreditsType") = "UNLOCK_MESSAGE_READ") Then
                        i1.CssClass = "credits-icon-message-read"
                    ElseIf (dr("CreditsType") = "UNLOCK_MESSAGE_SEND") Then
                        i1.CssClass = "credits-icon-message-send"
                    ElseIf (dr("CreditsType") = "UNLOCK_MESSAGE_READ_FREE") Then
                        i1.CssClass = "credits-icon-message-read"
                    ElseIf (dr("CreditsType") = "UNLOCK_MESSAGE_SEND_FREE") Then
                        i1.CssClass = "credits-icon-message-send"
                    End If
                End If


                If (lblAct IsNot Nothing) Then
                    If (dr("CustomerTransactionID") > 0) Then
                        If (dr("Expired") = True) Then
                            lblAct.Text = Me.CurrentPageData.GetCustomString("CreditsAction_Expired")
                        Else
                            lblAct.Text = Me.CurrentPageData.GetCustomString("CreditsAction_Credited")
                        End If

                    ElseIf (dr("CreditsType") = "UNLOCK_CONVERSATION") Then
                        lblAct.Text = Me.CurrentPageData.GetCustomString("CreditsAction_Used")
                    ElseIf (dr("CreditsType") = "UNLOCK_MESSAGE_READ") Then
                        lblAct.Text = Me.CurrentPageData.GetCustomString("CreditsAction_Used")
                    ElseIf (dr("CreditsType") = "UNLOCK_MESSAGE_SEND") Then
                        lblAct.Text = Me.CurrentPageData.GetCustomString("CreditsAction_Used")
                    ElseIf (dr("CreditsType") = "UNLOCK_MESSAGE_READ_FREE") Then
                        lblAct.Text = Me.CurrentPageData.GetCustomString("CreditsAction_Used")
                    ElseIf (dr("CreditsType") = "UNLOCK_MESSAGE_SEND_FREE") Then
                        lblAct.Text = Me.CurrentPageData.GetCustomString("CreditsAction_Used")
                    End If

                End If

            End If
        Catch ex As Exception
        End Try

    End Sub


End Class
﻿'Imports Sites.SharedSiteCode
Imports Library.Public
Imports Dating.Server.Core.DLL
Imports Dating.Server.Datasets.DLL


Public Class UCSelectProductMembers2_SMS
    Inherits BaseUserControl

    Protected Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData("control.UCSelectProductMembers", Context, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property


    Dim _control As String = ""
    Public Property control As String
        Get
            Return _control
        End Get
        Set(ByVal value As String)
            _control = value
        End Set
    End Property

    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try

            If (Not Me.IsPostBack AndAlso Me.Visible) Then
                LoadLAG()
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub



    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
        'LoadView()
    End Sub


    Public Sub LoadLAG() 'ByVal LAGID As String, ByVal NoCache As Boolean
        Try
            Dim priceO As EUS_Price = clsPricing.GetPriceForProductCode_AnyIndex("SMS150Credits")

            lblSMSBanner.Text = Me.CurrentPageData.GetCustomString("lblSMSBanner")
            lblCodeText.Text = Me.CurrentPageData.GetCustomString("lblCodeText")
            lblSMSFooter.Text = Me.CurrentPageData.GetCustomString("lblSMSFooter")

            lblSMSFooter.Text = lblSMSFooter.Text.Replace("[CREDITS-AMOUNT]", priceO.Credits)

            ''lbConfusedMore.Text = Me.CurrentPageData.GetCustomString("lbconfusedMore")
            ''lblTopTitle.Text = Me.CurrentPageData.GetCustomString("lblTopTitle")

            'If (Me.SessionVariables.MemberData.Country = "TR") Then
            '    lblPaymentsGuarantee.Text = Me.CurrentPageData.GetCustomString("lblPaymentsGuarantee3_TR")
            'Else
            '    lblPaymentsGuarantee.Text = Me.CurrentPageData.GetCustomString("lblPaymentsGuarantee3")
            'End If

            'btnBonusCredits.Text = Me.CurrentPageData.GetCustomString("btnBonusCredits")
            'lblBonusCode.Text = Me.CurrentPageData.GetCustomString("lblBonusCode")
            ''lblSelectProduct.Text = Me.CurrentPageData.GetCustomString("lblSelectProduct")
            'lbfootText.Text = Me.CurrentPageData.GetCustomString("lbfootText3")

            'If (Not clsPricing.VATCountries.Contains(Session("GEO_COUNTRY_CODE"))) Then
            '    imgPaymentSigns.Src = imgPaymentSigns.Src.Replace("/payment-logos.png", "/payment-logos_nopp.png")
            'End If

        Catch ex As Exception
            WebErrorMessageBox(ex, "")
        End Try

    End Sub

    Protected Sub btnSMSCode_Click(sender As Object, e As EventArgs) Handles btnSMSCode.Click
        Dim success As Integer = 0
        Try
            If (txtSMSCode.Text.Trim() <> "") Then
                Dim smsCode As String = txtSMSCode.Text
                If (smsCode.Length < 10) Then
                    smsCode = smsCode.PadRight(10, Convert.ToChar(" "))
                End If
                Using cmdb As CMSDBDataContext = Me.GetCMSDBDataContext


                    Dim code As EUS_OneTimeCode = (From itm In cmdb.EUS_OneTimeCodes
                                                   Where itm.Code = smsCode
                                                   Select itm).FirstOrDefault()
                    If (code IsNot Nothing AndAlso Not code.IsRenewed) Then

                        Dim transactionInfo As String = smsCode
                        Dim Description As String = "SMS Payment-renewal code:" & smsCode
                        Dim Profile As vw_EusProfile_Light = Me.GetCurrentProfile()

                        success = clsCustomer.RenewCreditsWithSMS(Profile, transactionInfo, Description, Request.ServerVariables("REMOTE_ADDR"), Request.ServerVariables("HTTP_REFERER"), Description)

                        code.IsRenewed = True
                        code.ProfileID = Me.MasterProfileId
                        cmdb.SubmitChanges()

                        success = 1
                    ElseIf (code IsNot Nothing AndAlso code.IsRenewed) Then

                        success = -5
                    Else
                        success = -4
                    End If
                End Using
            Else

                success = -2
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
            success = -3
        End Try


        If (success = 1) Then
            txtSMSCode.Text = ""
            lblSMSCodeSuccessfull.Text = Me.CurrentPageData.GetCustomString("Credits.Renew.Successfull")
            pnlSMSCodeSuccessfull.Visible = True
            pnlSMSCodeSuccessfull.CssClass = "alert alert-success blue"

            'Dim ctl2 As Control = Me.LoadControl("~/UserControls/SMSInfoPopup.ascx")
            'Me.Page.Controls.Add(ctl2)

        ElseIf (success = -2) Then

            lblSMSCodeSuccessfull.Text = Me.CurrentPageData.GetCustomString("Credits.Renew.Code.Empty")
            pnlSMSCodeSuccessfull.Visible = True
            pnlSMSCodeSuccessfull.CssClass = "alert alert-danger"

        ElseIf (success = -3) Then

            'some exception occured
            lblSMSCodeSuccessfull.Text = Me.CurrentPageData.GetCustomString("Credits.Renew.Failed.Retry")
            pnlSMSCodeSuccessfull.Visible = True
            pnlSMSCodeSuccessfull.CssClass = "alert alert-danger"

        ElseIf (success = -4) Then
            ' there was a code, but we couldn't find it
            lblSMSCodeSuccessfull.Text = Me.CurrentPageData.GetCustomString("Credits.Renew.Failed")
            pnlSMSCodeSuccessfull.Visible = True
            pnlSMSCodeSuccessfull.CssClass = "alert alert-danger"

        ElseIf (success = -5) Then
            ' there was a code, but we couldn't find it
            lblSMSCodeSuccessfull.Text = Me.CurrentPageData.GetCustomString("Credits.Renew.Failed.AlreadyUsed")
            pnlSMSCodeSuccessfull.Visible = True
            pnlSMSCodeSuccessfull.CssClass = "alert alert-danger"

        End If
    End Sub
End Class
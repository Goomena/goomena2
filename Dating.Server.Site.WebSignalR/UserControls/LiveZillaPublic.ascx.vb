﻿Imports Dating.Server.Core.DLL

Public Class LiveZillaPublic
    Inherits System.Web.UI.UserControl

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'Return
        If (Me.Visible) Then
            LiveZilla_btn.Visible = True

            If (Request.Cookies("LiveZillaClosed") IsNot Nothing) Then
                Session("LiveZillaClosed") = Request.Cookies("LiveZillaClosed").Value

                Dim aCookie As HttpCookie = Request.Cookies("LiveZillaClosed")
                aCookie.Values.Remove("LiveZillaClosed")
                aCookie.Expires = DateTime.Now.AddDays(-1)
                Response.Cookies.Add(aCookie)
            End If

            If (Session("LiveZillaClosed") = "1") Then
                LiveZilla_btn.Visible = False
                'Session("LiveZillaOnline") = Nothing
            Else

                Dim url As String = "http://chat.goomena.com/onoffline.php".ToLower()
                Try
                    Dim rp As String = clsWebPageProcessor.GetPageContent(url, 5000)
                    If (rp.ToLower().Contains("offline")) Then
                        Me.Visible = False
                    Else
                        Me.Visible = True
                        'Session("LiveZillaOnline") = "1"
                    End If
                Catch ex As Exception
                    'WebErrorMessageBox(Me, ex, "Page_PreRender, url " & url)
                    Session("LiveZillaClosed") = "1"
                    'Session("LiveZillaOnline") = Nothing
                    Me.Visible = False
                End Try

            End If

        End If

        'Try
        '    Dim ctl As Control = Me.FindControl("livezilla_tracking")
        '    If (ctl IsNot Nothing) Then
        '        ctl.Visible = If(Session("LiveZillaClosed") = "1", False, True)
        '    End If
        'Catch ex As Exception
        'End Try

    End Sub

End Class
﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UCSelectProductMembers.ascx.vb" Inherits="Dating.Server.Site.Web.UCSelectProductMembers" %>
<%@ Register src="UCPrice.ascx" tagname="UCPrice" tagprefix="uc1" %>
<style>
    .ordbut { position: absolute; bottom: 20px; right: 20%; }
    .ordbut2 { position: absolute; bottom: 20px; right: 15%; }
    .pricecol { position: relative; width: 195px; height: 263px;font-family:"Segoe UI","Arial"; }
    .paymentsimgs { margin-right: auto; margin-left: auto; width: 315px; margin-top: 70px; margin-bottom: 70px; }
    .pricecolpadding { padding-right: 14px; }
    .pricetable { margin-left: 33px; }
    .pricecol-credits { text-align:center;font-size: 37px;line-height: 37px;padding-top: 13px;}
    .pricecol-credits-text { text-align:center;font-size: 20px;line-height: 17px;}
    .pricecol-price {position:absolute;top:108px;left:0px;right:0px; }
    .pricecol-price-text {text-align:center;font-size:42px;line-height:42px;color:#39B54A; }
    .pricecol-expire {position:absolute;top:149px;left:0px;right:0px;display:none;}
    .pricecol-expire-text {text-align:center;font-size:11px;}
    .pricecol-price.small {top:118px; }
    .pricecol-price.small .pricecol-price-text{font-size:22px;line-height:22px; }
    .pricecol-expire.small .pricecol-expire-text{}
    .timesdiv1{}
    .bonus-code{background-color:#F3F3F3;font-size:18px;text-align:center;border:1px solid #E6E6E6;padding:20px;}
    .bonus-code .button{background-color:#25AAE1;color:#fff;padding:5px 10px;}
    .bonus-code .textbox{width:180px;height:25px;font-size:16px;line-height:25px;}

    .clear { clear: both; }
    .timesdiv3 { font-family: "Segoe UI" , "Arial"; width: 700px; margin-left: auto; margin-right: auto; position: relative; margin-top: 70px; }
    .oikonomikadiv { display: inline-block; }
    .oikonomika { width: 274px; height: 152px; background: #bf2331; color: white; font-size: 18px; font-weight: lighter; margin-top: 0px; padding-top: 26px; padding-left: 27px; }
    .oikonomika p { }
    .rightdiv { float: right; width: 395px; }
    .voucherh1, .guranteeh1 { font-size: 15px; font-weight: lighter; }
    .voucherp1, .guranteep1 { font-size: 12px; }
    .guranteep1 a { text-decoration: none; color: black; }
    .returnh1 { font-size: 18px; font-weight: lighter; }
    .returnp1 { font-size: 13px; font-weight: lighter; }
    .note { padding-top: 25px; border-top: 1px solid #bababc; margin-top: 60px; font-size: 10px; font-family: "Segoe UI" , "Arial"; }
    .note a { text-decoration: none; font-weight: bold; color: #25AAE2; }
    .tooltipcustom { width: 300px; background: white; color: white; font-size: 14px; background: black; opacity: 0.8; padding: 20px; }
    .tip { cursor: pointer; font-weight: bold; color: #26a9e1; }

    .timesdiv1 { width: 1004px; margin-left: auto; margin-right: auto; position: relative; background: url(//cdn.goomena.com/goomena-kostas/first-general/bg-reapet.jpg) repeat; }
    .timesdiv1 h1 { margin-bottom: 33px; font-weight: lighter; font-size: 24px; margin-top: 50px; }
    .timesdiv1 h2 { color: #25aae2; font-size: 17px; font-weight: lighter; margin-bottom: 0px; }
    .timesdiv1 p { font-size: 14px; font-weight: lighter; }
    .paymentsimgs { margin-right: auto; margin-left: auto; width: 315px; margin-top: 70px; margin-bottom: 70px; }
    .timesdiv1 ul { padding-left: 0px; }
    .timesdiv1 ul li { display: inline-block; list-style-type: none; }
    .li1 { background: #E0E0E1 url(//cdn.goomena.com/goomena-kostas/pricing/box1.png) repeat-y; font-size: 14px; height: 70px; padding-left: 24px; padding-top: 20px; width: 186px; margin-right: 2px; }
    .li2 { background: #E0E0E1 url(//cdn.goomena.com/goomena-kostas/pricing/box2.png) repeat-y; font-size: 14px; height: 70px; margin-right: 6px; padding-left: 24px; padding-top: 20px; width: 180px; }
    .li3 { background: #E0E0E1 url(//cdn.goomena.com/goomena-kostas/pricing/box3.png) repeat-y; font-size: 14px; height: 70px; padding-left: 24px; padding-right: 0; padding-top: 20px; width: 236px; }
</style>
<div>
    <asp:Label ID="lblPaymentsGuarantee" runat="server" Text=""></asp:Label>
</div>
<asp:Panel ID="pnlBonus" runat="server" CssClass="bonus-code">
    <div align="center">
        <asp:Label ID="lblBonusCode" runat="server" Text="" style="color:#25AAE1;"/>
        <asp:Label ID="lblBonusCodeSuccess" runat="server" ForeColor="Green" 
            ViewStateMode="Disabled"/>
        <div style="height:20px"></div>
        <asp:TextBox ID="txtBonusCode" runat="server" CssClass="textbox"></asp:TextBox>
        <asp:Button ID="btnBonusCredits" runat="server" Text="Υποβολή κωδικού" CssClass="button" />
        <div style="height:20px"></div>
        <asp:Label ID="lblBonusCodeError" runat="server" Text="" ForeColor="red" 
            ViewStateMode="Disabled"/>
    </div>
</asp:Panel>

<div class="paymentsimgs"><img ID="imgPaymentSigns" runat="server" src="//cdn.goomena.com/goomena-kostas/pricing-members/payment2.png?v4" alt="Payment Methods"/></div>
<div style="margin-bottom:20px">
    <asp:Label ID="lblSelectProduct" runat="server" Text=""></asp:Label>
</div>


<table cellspacing="0" cellpadding="0" style="">
    <tr>
        <td style="width:195px;height:263px;">
            <uc1:UCPrice ID="UCPrice1" runat="server" View="vwMember" />
        </td>
        <td style="width:20px;">
        &nbsp;
        </td>
        <td style="width:195px;height:263px;">
            <uc1:UCPrice ID="UCPrice2" runat="server" View="vwMember" />
        </td>
        <td style="width:20px;">
        &nbsp;
        </td>
        <td style="width:195px;height:263px;">
            <uc1:UCPrice ID="UCPrice3" runat="server" View="vwMember" />
        </td>
    </tr>
</table>

<%--<table class="pricetable">
<tr>
<td class="pricecol pricecolpadding" style="background-image: url('//cdn.goomena.com/goomena-kostas/pricing-members/100credits.png'); background-repeat: no-repeat;width:195px;height:263px;"align="center">
<div class="pricecol"><asp:ImageButton ID="img1000Credits" runat="server" CommandArgument="1000Credits,45,15" align="center" ImageUrl="~/goomena-kostas/pricing/order-button.png" CssClass="ordbut" /></div>
</td>
<td class="pricecol" style="background-image: url('//cdn.goomena.com/goomena-kostas/pricing-members/3000credits.png'); background-repeat: no-repeat;width:195px;height:263px;"align="center">
<div class="pricecol"><asp:ImageButton ID="img3000Credits" runat="server" CommandArgument="3000Credits,120,36" align="center" ImageUrl="~/goomena-kostas/pricing/order-button.png" CssClass="ordbut" /></div>
</td>
<td class="pricecol" style="background-image: url('//cdn.goomena.com/goomena-kostas/pricing-members/6000credits.png'); background-repeat: no-repeat;width:195px;height:263px;"align="center">
<div class="pricecol"><asp:ImageButton ID="img6000Credits" runat="server" CommandArgument="6000Credits,180,66" align="center" ImageUrl="~/goomena-kostas/pricing/order-button.png" CssClass="ordbut2" /></div>
</td>
</tr>
</table>--%>



<div>
    <dx:ASPxLabel runat="server" EncodeHtml="False" ClientIDMode="AutoID" ID="lbfootText">
    </dx:ASPxLabel>
</div>


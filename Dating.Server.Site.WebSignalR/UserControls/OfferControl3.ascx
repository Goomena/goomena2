﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="OfferControl3.ascx.vb" 
    Inherits="Dating.Server.Site.Web.OfferControl3" %>
<script type="text/javascript">
    var animating_elemnts_list = new Array();
    var img_zodiacs = new Array();
</script>
<div id="<%= Mybase.UsersListView.toString() %>">
<asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">

    <asp:View ID="View1" runat="server">
    </asp:View>
    
    
    <asp:View ID="vwNoPhoto" runat="server" EnableViewState="false">
        <asp:FormView ID="fvNoPhoto" runat="server">
        <ItemTemplate>
<div id="ph-edit" class="no-photo">
    <div class="items_none items_hard">
            <div class="items_none_wrap">
        <div class="items_none_text">
            <%= Me.MykeyStrings.HasNoPhotosText%>
        </div>
        <div class="search_members_button_right"><asp:HyperLink ID="HyperLink1" runat="server" CssClass="btn lighter" NavigateUrl="~/Members/Photos.aspx" onclick="ShowLoading();"><%= Me.MykeyStrings.AddPhotosText%><i class="icon-chevron-right"></i></asp:HyperLink></div>
        <div class="clear"></div>
    </div>
    </div>
</div>
        </ItemTemplate>
        </asp:FormView>
    </asp:View>
    
    
    <asp:View ID="vwNoOffers" runat="server">
        <asp:FormView ID="fvNoOffers" runat="server" EnableViewState="false">
        <ItemTemplate>
            <div class="items_none">
                <div id="offers_icon" class="middle_icon"></div>
                <div class="items_none_text">
                    <%# Eval("NoOfferText")%>
	                <div class="clear"></div>
                    <div class="search_members_button">
                    <asp:HyperLink ID="lnk4" runat="server" NavigateUrl="~/Members/Search.aspx"><%= Me.MykeyStrings.SearchOurMembersText%><i class="icon-chevron-right"></i></asp:HyperLink>
                    </div>
                </div>
            </div>
        </ItemTemplate>
        </asp:FormView>
    </asp:View>


    <asp:View ID="vwNewOffers" runat="server">
<asp:Repeater ID="rptNew" runat="server">
<ItemTemplate>
<div class="l_item l_new deal_right">
    <div class="left">
        <div class="info">
            <h2 class="wink"><%# Eval("YouReceivedWinkOfferTextNew")%></h2>
            <p id="pWink" runat="server" visible='<%# Eval("IsWink") %>'><%# Eval("WantToKnowFirstDatePriceTextNew")%></p>
            <p id="pPoke" runat="server" visible='<%# Eval("IsPoke") %>'><%# Eval("YouReceivedPokeDescriptionText")%></p>
            <p id="pAmount" runat="server" visible='<%# Eval("IsOffer") %>'><%# Eval("WillYouAcceptDateWithForAmountText")%></p>
 			<div class="bot_actions lfloat">
                <asp:Panel CssClass="rfloat icon_tooltipholder" runat="server" ID="divTooltipholder" Visible='<%# Eval("AllowTooltipPopupLikes")%>'>
                    <asp:LinkButton ID="lnkWhatIs" runat="server" OnClientClick="OnMoreInfoClick('[contentUrl]',this,'[headerText]');return false;">
                        <img runat="server" id="icon_tip" src="//cdn.goomena.com/Images/spacer102.png" alt="" class="questmark"/></asp:LinkButton>
                </asp:Panel>
                <div class="lfloat">
                <asp:HyperLink ID="lnkNewMakeOffer" runat="server" visible='<%# Eval("AllowCreateOffer") %>' NavigateUrl='<%# Eval("CreateOfferUrl") %>'
                    class="btn btn-small"><span><%= Me.MykeyStrings.MakeOfferText%></span></asp:HyperLink>
                </div>
                <div class="lfloat">
                <asp:LinkButton ID="lnkNewAccept" runat="server" Visible='<%# Eval("AllowAccept") %>' CommandName="OFFERACCEPT" CommandArgument='<%# Eval("OfferID")%>' 
                    class="btn btn-small"><span><%= Me.MykeyStrings.AcceptText%></span></asp:LinkButton>
                </div>
                <div class="lfloat">
                <asp:LinkButton ID="lnkNewCounter" runat="server" Visible='<%# Eval("AllowCounter") %>' CommandName="OFFERCOUNTER" CommandArgument='<%# Eval("OfferID")%>' 
                    class="btn btn-small"><span><%= Me.MykeyStrings.CounterText%></span></asp:LinkButton>
                </div>
                <div class="lfloat">
                    <asp:LinkButton ID="lnkPoke" runat="server" Visible='<%# Eval("AllowPoke") %>' CommandName="POKE" CommandArgument='<%# "offr_" & Eval("OfferID") & "-prof_" & Eval("OtherMemberProfileID") %>' 
                    class="btn btn-small"><span><%= Me.MykeyStrings.PokeText%></span></asp:LinkButton>
                </div>
                <div class="btn-group">
                    <a class="btn btn-small dropdown-toggle" data-toggle="dropdown" href="#"><%= Me.MykeyStrings.RejectText%> <span class="caret"></span></a></td>
                    <ul class="dropdown-menu">
                        <li><asp:LinkButton ID="lnkRejectType" runat="server" CommandName="REJECTTYPE" CommandArgument='<%# Eval("OfferID")%>' CssClass="btn"><i class="icon-chevron-right"></i><%= Me.MykeyStrings.NotInterestedText%></asp:LinkButton></li>
                        <li><asp:LinkButton ID="lnkRejectFar" runat="server" CommandName="REJECTFAR" CommandArgument='<%# Eval("OfferID")%>' CssClass="btn"><i class="icon-chevron-right"></i><%= Me.MykeyStrings.TooFarAwayText%></asp:LinkButton></li>
                        <li><asp:LinkButton ID="lnkRejectBad" runat="server" CommandName="REJECTBAD" CommandArgument='<%# Eval("OfferID")%>' CssClass="btn"><i class="icon-chevron-right"></i><%= Me.MykeyStrings.NotEnoughInfoText%></asp:LinkButton></li>
                        <li><asp:LinkButton ID="lnkRejectExp" runat="server" CommandName="REJECTEXPECTATIONS" CommandArgument='<%# Eval("OfferID")%>' CssClass="btn"><i class="icon-chevron-right"></i><%= Me.MykeyStrings.DifferentExpectationsText%></asp:LinkButton></li>
                    </ul>
                </div>
                <div class="btn-group" ID="ActionsMenu" runat="server" Visible='<%# Eval("AllowActionsMenu") %>'>
                    <a class="btn btn-small dropdown-toggle" data-toggle="dropdown" href="#"><%= Me.MykeyStrings.ActionsText%> <span class="caret"></span></a></td>
                    <ul class="dropdown-menu">
                        <li ID="liActsUnlock" runat="server" visible='<%# Eval("AllowActionsUnlock")%>'><asp:LinkButton 
                                ID="lnkActsUnlock" runat="server" CommandName="UNLOCKOFFER" CommandArgument='<%# Eval("OfferId") %>'><%# Eval("ActionsUnlockText")%><i class="icon-chevron-right icon-white"></i></asp:LinkButton></li>
                        <li ID="liActsMakeOffer" runat="server" visible='<%# Eval("AllowActionsCreateOffer") %>' ><asp:HyperLink 
                                ID="lnkNewMakeOffer2" runat="server" NavigateUrl='<%# Eval("CreateOfferUrl") %>'><span><%# Eval("ActionsMakeOfferText")%></span></asp:HyperLink></li>
                        <li ID="liActsSendMsg" runat="server" visible='<%# Eval("AllowActionsSendMessage")%>'><asp:HyperLink 
                                ID="lnkSendMsg2" runat="server" NavigateUrl='<%# Eval("SendMessageUrl")%>'><%= Me.MykeyStrings.SendMessageText%><i class="icon-chevron-right"></i></asp:HyperLink></li>
                        <li ID="liActsDelOffr" runat="server" visible='<%# Eval("AllowActionsDeleteOffer")%>'><asp:LinkButton 
                                ID="lnkDelOffr" runat="server" CommandName="DELETEOFFER" CommandArgument='<%# Eval("OfferId") %>' OnClientClick='<%#  Eval("OfferDeleteConfirmMessage")  %>'><%= Me.MykeyStrings.DeleteOfferText%></asp:LinkButton></li>
                    </ul>
                </div>
                <%--<asp:HyperLink ID="lnkNewAccept" runat="server" Visible='<%# Eval("AllowAccept") %>' CommandName="OFFERACCEPT" CommandArgument='<%# Eval("OfferID")%>' class="btn btn-small"><span><%# Eval("AcceptText")%></span></asp:HyperLink>--%>
            </div>
        </div>
    </div>
    <div class="right plane" ID="dRight" runat="server" EnableViewState="false">
        <div class="photo">
            <div class="pr-photo-116-noshad">
                <asp:HyperLink ID="lnk1" runat="server" NavigateUrl='<%# Eval("OtherMemberProfileViewUrl")%>'><img alt="" src="<%# Eval("OtherMemberImageUrl")%>" class="round-img-116" /></asp:HyperLink>
            </div>
        </div>
        <div class="info">
            <h2><asp:HyperLink ID="lnk2" runat="server" NavigateUrl='<%# Eval("OtherMemberProfileViewUrl")%>'><%# Eval("OtherMemberLoginName")%></asp:HyperLink></h2>
            <div class="datecreated"><%# Eval("CreatedDate")%></div>
            <%--<p><%# Eval("OtherMemberHeading")%></p>--%>
            <div class="stats">
                <%# MyBase.WriteOffer_MemberInfo(Container.DataItem)%>
                <div class="tooltip_offer" style="display: none;">
                    <div class="tooltip_text"><%# Eval("MilesAwayText")%></div>
                </div>
            </div>
            <div class="tt_enabled_distance distance_absolute <%# Eval("DistanceCss")%>">
            </div>
        </div>
    </div>
    <div class="clear"></div>
</div>
</ItemTemplate>
</asp:Repeater>
    </asp:View>


    <asp:View ID="vwPendingOffers" runat="server">
<asp:Repeater ID="rptPending" runat="server">
<ItemTemplate>

<div class="l_item deal_left l_pending"  login="<%# Eval("OtherMemberLoginName")%>">
    <div class="left">
        <div class="info">
            <h2><%= Me.MykeyStrings.AwaitingResponseText%></h2>
            <p><%# Eval("YourWinkSentTextNew")%></p>
 			<div class="bot_actions lfloat">
                <asp:Panel CssClass="rfloat icon_tooltipholder" runat="server" ID="divTooltipholder" Visible='<%# Eval("AllowTooltipPopupLikes")%>'>
                    <asp:LinkButton ID="lnkWhatIs" runat="server" OnClientClick="OnMoreInfoClick('[contentUrl]',this,'[headerText]');return false;">
                        <img runat="server" id="icon_tip" src="//cdn.goomena.com/Images/spacer102.png" alt="" class="questmark"/></asp:LinkButton>
                </asp:Panel>
                <div class="lfloat">
                    <asp:LinkButton ID="lnkCancelWink" runat="server" CssClass="btn btn-small" Visible='<%# Eval("AllowCancelWink") %>' CommandName="CANCELWINK" CommandArgument='<%# Eval("OfferID")%>'><span><%= Me.MykeyStrings.CancelWinkText%></span></asp:LinkButton>
                </div>
                <div class="lfloat">
                    <asp:LinkButton ID="lnkCancelOffer" runat="server" CssClass="btn btn-small" Visible='<%# Eval("AllowCancelPendingOffer")%>' CommandName="CANCELOFFER" CommandArgument='<%# Eval("OfferID")%>'><span><%= Me.MykeyStrings.CancelOfferText%></span></asp:LinkButton>
                </div>
                <div class="lfloat">
                    <asp:LinkButton ID="lnkCancelPoke" runat="server" CssClass="btn btn-small" Visible='<%# Eval("AllowCancelPoke")%>' CommandName="CANCELPOKE" CommandArgument='<%# Eval("OfferID")%>'><span><%= Me.MykeyStrings.CancelPokeText%></span></asp:LinkButton>
                </div>
                <div class="btn-group" ID="ActionsMenu" runat="server" Visible='<%# Eval("AllowActionsMenu") %>'>
                    <a class="btn btn-small dropdown-toggle" data-toggle="dropdown" href="#"><%= Me.MykeyStrings.ActionsText%> <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li ID="liActsSendMsgOnce" runat="server" visible='<%# Eval("AllowActionsSendMessageOnce")%>'><asp:HyperLink 
                                ID="lnkSendMsgOnce" runat="server" NavigateUrl='<%# Eval("SendMessageUrlOnce")%>' CssClass="btn"><%# Eval("SendMessageOnceText")%></asp:HyperLink></li>

                        <li ID="liActsSendMsgMany" runat="server" visible="false"><%-- visible='<%# Eval("AllowActionsSendMessageMany")%>'--%><asp:HyperLink 
                                ID="lnkSendMsgMany" runat="server" NavigateUrl='<%# Eval("SendMessageUrlMany")%>' CssClass="btn"><%# Eval("SendMessageManyText")%></asp:HyperLink></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="right car " ID="dRight" runat="server" EnableViewState="false">
        <div class="photo">
            <div class="pr-photo-116-noshad">
                <asp:HyperLink ID="lnk1" runat="server" NavigateUrl='<%# Eval("OtherMemberProfileViewUrl") %>'><img src="<%# Eval("OtherMemberImageUrl") %>" class="round-img-116"  /></asp:HyperLink></div>
        </div>
        <div class="info">
            <h2><asp:HyperLink ID="lnk2" runat="server" NavigateUrl='<%# Eval("OtherMemberProfileViewUrl") %>' Text='<%# Eval("OtherMemberLoginName")%>' /></h2>
            <%--<p><%# Eval("OtherMemberHeading")%></p>--%>
            <div class="stats">
                <%# MyBase.WriteOffer_MemberInfo(Container.DataItem)%>
                <div class="tt_enabled_distance distance_absolute <%# Eval("DistanceCss")%>">
                </div>
                <div class="tooltip_offer" style="display: none;">
                    <div class="tooltip_text"><%# Eval("MilesAwayText")%></div>
                </div>
            </div>
        </div>
    </div>
    <div class="clear">
    </div>
</div>
</ItemTemplate>
</asp:Repeater>
    </asp:View>


    <asp:View ID="vwRejectedOffers" runat="server">
<asp:Repeater ID="rptRejected" runat="server">
<ItemTemplate>

<div class="l_item <%# Eval("RejectCssClass")  %> l_rejected"  login="<%# Eval("OtherMemberLoginName")%>">
	<div class="left">
		<div class="info">
			<h2 class="rejected"><%# Eval("CancelledRejectedTitleTextNew")%></h2>
			<p class="rejected"><%# Eval("CancelledRejectedDescriptionText")%></p>
 			<div class="bot_actions lfloat">
                <asp:Panel CssClass="rfloat icon_tooltipholder" runat="server" ID="divTooltipholder" Visible='<%# Eval("AllowTooltipPopupLikes")%>'>
                    <asp:LinkButton ID="lnkWhatIs" runat="server" OnClientClick="OnMoreInfoClick('[contentUrl]',this,'[headerText]');return false;">
                        <img runat="server" id="icon_tip" src="//cdn.goomena.com/Images/spacer102.png" alt="" class="questmark"/></asp:LinkButton>
                </asp:Panel>
                <div class="lfloat">
                    <asp:LinkButton ID="lnkTryWink" runat="server" Visible='<%# Eval("AllowTryWink")%>' CommandName="TRYWINK" CommandArgument='<%# Eval("OfferId") %>' CssClass="btn btn-small"><%= Me.MykeyStrings.TryWinkText%></asp:LinkButton>
                </div>
                <div class="lfloat">
                    <asp:LinkButton ID="lnkDeleteWink" runat="server" Visible='<%# Eval("AllowDeleteWink")%>' CommandName="DELETEOFFER" CommandArgument='<%# Eval("OfferId") %>' CssClass="btn btn-small"><%= Me.MykeyStrings.DeleteWinkText%></asp:LinkButton>
                </div>
                <div class="lfloat">
                    <asp:LinkButton ID="lnkCreateOffer" runat="server" Visible='<%# Eval("AllowCreateOffer")%>' CommandName="TRYCREATEOFFER" CommandArgument='<%# Eval("OfferId") %>' CssClass="btn btn-small"><%= Me.MykeyStrings.MakeNewOfferText%></asp:LinkButton>
                </div>
                <div class="lfloat">
                    <asp:LinkButton ID="lnkDeleteOffer" runat="server" Visible='<%# Eval("AllowDeleteOffer")%>' CommandName="DELETEOFFER" CommandArgument='<%# Eval("OfferId") %>' CssClass="btn btn-small"><%= Me.MykeyStrings.DeleteOfferText%></asp:LinkButton>
                </div>
			</div>
		</div>
	</div>
	<div class="right car " ID="dRight" runat="server" EnableViewState="false">
        <div class="photo">
            <div class="pr-photo-116-noshad">
                <asp:HyperLink ID="lnk2" runat="server" NavigateUrl='<%# Eval("OtherMemberProfileViewUrl") %>'><img src="<%# Eval("OtherMemberImageUrl") %>" class="round-img-116"  /></asp:HyperLink></div>
        </div>
		<div class="info">
            <h2><asp:HyperLink ID="lnk3" runat="server" NavigateUrl='<%# Eval("OtherMemberProfileViewUrl") %>' Text='<%# Eval("OtherMemberLoginName")%>' /></h2>
			<%--<p><%# Eval("OtherMemberHeading")%></p>--%>
			<div class="stats">
                <%# MyBase.WriteOffer_MemberInfo(Container.DataItem)%>
				<div class="tooltip_offer" style="display: none;">
					<div class="tooltip_text"><%# Eval("MilesAwayText")%></div>
				</div>
			</div>
			<div class="tt_enabled_distance distance_absolute <%# Eval("DistanceCss")%>"></div>
		</div>
	</div>
	<div class="clear"></div>
</div>
</ItemTemplate>
</asp:Repeater>
    </asp:View>


    <asp:View ID="vwAcceptedOffers" runat="server">
<asp:Repeater ID="rptAccepted" runat="server">
<ItemTemplate>

<div class="l_item deal_accepted new_date l_accepted" login="<%# Eval("OtherMemberLoginName")%>">
	<div class="left">
		<div class="info">
			<h2><%# Eval("OfferAcceptedWithAmountText") %></h2>
			<p class="padding"><%# Eval("OfferAcceptedHowToContinueText") %></p>
 			<div class="bot_actions lfloat">
                <asp:Panel CssClass="rfloat icon_tooltipholder" runat="server" ID="divTooltipholder" Visible='<%# Eval("AllowTooltipPopupLikes")%>'>
                    <asp:LinkButton ID="lnkWhatIs" runat="server" OnClientClick="OnMoreInfoClick('[contentUrl]',this,'[headerText]');return false;">
                        <img runat="server" id="icon_tip" src="//cdn.goomena.com/Images/spacer102.png" alt="" class="questmark"/></asp:LinkButton>
                </asp:Panel>
                <div class="lfloat">
                    <asp:HyperLink ID="lnk1" runat="server" Visible='<%# Eval("AllowSendMessage")%>' CssClass="btn btn-small lighter" NavigateUrl='<%# Eval("SendMessageUrl")%>'><%= Me.MykeyStrings.SendMessageText%><i class="icon-chevron-right"></i></asp:HyperLink>
                </div>
                <div class="lfloat">
                    <asp:LinkButton ID="lnk2" runat="server" Visible='<%# Eval("AllowDeleteAcceptedOffer")%>' CommandName="DELETEOFFER" CommandArgument='<%# Eval("OfferId") %>' CssClass="btn btn-small" OnClientClick='<%#  Eval("OfferDeleteConfirmMessage")  %>'><%= Me.MykeyStrings.DeleteOfferText%></asp:LinkButton>
                </div>
                <div class="lfloat">
                    <asp:LinkButton ID="lnk3" runat="server" Visible='<%# Eval("AllowOfferAcceptedUnlock")%>' CssClass="btn btn-primary" CommandName="UNLOCKOFFER" CommandArgument='<%# Eval("OfferId") %>'><%# Eval("OfferAcceptedUnlockText")%><i class="icon-chevron-right icon-white"></i></asp:LinkButton>
                </div>
                <div class="btn-group" ID="ActionsMenu" runat="server" Visible='<%# Eval("AllowActionsMenu") %>'>
                    <a class="btn btn-small dropdown-toggle" data-toggle="dropdown" href="#"><%= Me.MykeyStrings.ActionsText%> <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li ID="liActsSendMsgOnce" runat="server" visible='<%# Eval("AllowActionsSendMessageOnce")%>'><asp:HyperLink 
                                ID="lnkSendMsgOnce" runat="server" NavigateUrl='<%# Eval("SendMessageUrlOnce")%>' CssClass="btn"><%# Eval("SendMessageOnceText")%></asp:HyperLink></li>

                        <li ID="liActsSendMsgMany" runat="server" visible="false"><%-- visible='<%# Eval("AllowActionsSendMessageMany")%>'--%><asp:HyperLink 
                                ID="lnkSendMsgMany" runat="server" NavigateUrl='<%# Eval("SendMessageUrlMany")%>' CssClass="btn"><%# Eval("SendMessageManyText")%></asp:HyperLink></li>
                    </ul>
                </div>
			</div>
            <asp:Panel runat="server" ID="pnlMsgSent" Visible='<%# Eval("IsMessageSent")%>' CssClass="rfloat pnlMsgSent">
                <asp:Image ID="imgCheck" runat="server" Imageurl="//cdn.goomena.com/Images2/mbr2/message-send.png" ImageAlign="AbsMiddle" /><asp:Label ID="lblMsgSent" runat="server" ><%= me.MykeyStrings.MessageSentNotice %></asp:Label>
            </asp:Panel>
		</div>
	</div>

	<div class="right <%# iif(Eval("ItsCurrentProfileAction").ToString()="True","accepted_left","accepted_right") %>">
        <div class="photo">
            <div class="pr-photo-116-noshad">
                <asp:HyperLink ID="hl3" runat="server" NavigateUrl='<%# Eval("OtherMemberProfileViewUrl") %>'><img src="<%# Eval("OtherMemberImageUrl") %>" class="round-img-116"  /></asp:HyperLink></div>
        </div>
		<div class="info">
            <h2><asp:HyperLink ID="lnk21" runat="server" NavigateUrl='<%# Eval("OtherMemberProfileViewUrl") %>' Text='<%# Eval("OtherMemberLoginName")%>'  onclick="ShowLoading();"/></h2>
			<%--<p><%# Eval("OtherMemberHeading")%></p>--%>
			<div class="stats">
                <%# MyBase.WriteOffer_MemberInfo(Container.DataItem)%>
				<div class="tooltip_offer" style="display: none;">
					<div class="tooltip_text"><%# Eval("MilesAwayText")%></div>
				</div>
			</div>
			<div class="tt_enabled_distance distance_absolute <%# Eval("DistanceCss")%>"></div>
		</div>
	</div>
	<div class="clear"></div>
</div>
</ItemTemplate>
</asp:Repeater>
    </asp:View>
    </asp:MultiView>


<script type="text/javascript">
    function LoadSearch() {
       <%-- /*
        $(".vip-png", '#search-results').tooltip({
            tooltipClass: "tooltip-VIP",
            content: "<%= Dating.Server.Site.Web.AppUtils.JS_PrepareString(Me.VIPTooltip) %>",
            show: { delay: 1500 }
        });
        $("img.round-img-116", '#search-results').mouseenter(function () {
            var t = this;
            var p = $(t).parent();
            while (!p.is('.left')) { p = $(p).parent(); }
            if (p.length > 0) {
                p.find('a', '.info h2').addClass('hovered')
            }
        }).mouseleave(function () {
            var t = this;
            var p = $(t).parent();
            while (!p.is('.left')) { p = $(p).parent(); }
            if (p.length > 0) {
                p.find('a', '.info h2').removeClass('hovered')
            }
        });
        $("a.login-name", '#search-results').mouseenter(function () {
            var t = this;
            var p = $(t).parent();
            while (!p.is('.left')) { p = $(p).parent(); }
            if (p.length > 0) {
                p.find('.pr-photo-116-noshad', '.photo').addClass('hovered')
            }
        }).mouseleave(function () {
            var t = this;
            var p = $(t).parent();
            while (!p.is('.left')) { p = $(p).parent(); }
            if (p.length > 0) {
                p.find('.pr-photo-116-noshad', '.photo').removeClass('hovered')
            }
        });
    */--%>

        var scope = "#offers-list .l_new", scope2;
        scope = (($(".l_pending", "#offers-list").length > 0) ? "#offers-list .l_pending" : scope);
        scope = (($(".l_rejected", "#offers-list").length > 0) ? "#offers-list .l_rejected" : scope);
        scope = (($(".l_accepted", "#offers-list").length > 0) ? "#offers-list .l_accepted" : scope);
        scope2 = (($(".s_item", "#MyBlockedList").length > 0) ? "#MyBlockedList .s_item" : scope);

        var itms = $("a.btn", scope + " .left").css("width", "auto");
        if (itms.length == 0) { itms = $("a.btn", scope2 + " .bot_right_actions").css("width", "auto"); }
        for (var c = 0; c < itms.length; c++) {
            var w = $(itms[c]).width();
            $(itms[c]).css("width", "").removeClass("lnk130").removeClass("lnk180");
            if (w < 120)
                $(itms[c]).addClass("lnk130");
            else
                $(itms[c]).addClass("lnk180");
        }

        itms = $("a.dropdown-toggle", scope + " .left .btn-group").css("width", "auto");
        for (c = 0; c < itms.length; c++) {
            var w = $(itms[c]).width();
            $(itms[c]).css("width", "").removeClass("lnk130").removeClass("lnk180").removeClass("lnk107").removeClass("lnk116");
            if (w < 83)
                $(itms[c]).addClass("lnk107");
            else
                $(itms[c]).addClass("lnk116");
        }
    }

    function startAnimating() {
        if (animating_elemnts_list.length > 0) { setTimeout(startAnimateOnlineImg, 500); }
    }
    /*
    function processAnimating() {
        try {
            animating_elemnts_list = new Array();
            var online = $('.online-indicator', '#search-results');
            if (online.length > 0) {
                for (var c = 0; c < online.length; c++) {
                    animating_elemnts_list.push(online[c].id)
                }
                setTimeout(startAnimateOnlineImg, 500);
            }
        }
        catch (e) { }
    }
    */
    (function () {
        LoadSearch()
        startAnimating();
        //Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(processAnimating);
    })();
</script>

</div>












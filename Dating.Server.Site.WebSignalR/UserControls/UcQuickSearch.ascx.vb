﻿Imports Dating.Server.Core.DLL
Imports DevExpress.Web.ASPxEditors
Imports Dating.Server.Datasets.DLL

''' <summary>
''' This control is loaded when a MAN view the search
''' </summary>
''' <remarks></remarks>
Public Class UcQuickSearch
    Inherits BaseUserControl
    Implements INewQuickSearchCriterias
    Protected Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData("control.UcQuickSearch", Context, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If (Not Me.IsPostBack) Then

                chkBeOnline.Checked = True
                Me.trcDistance.Position = 31
                '    chkHasPhoto.Checked = True

                LoadLAG()
            Else
                '  LoadLagInButtons()
            End If
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Protected Sub LoadLAG()
        Try
            ddeHairColorWrap.NullText = CurrentPageData.GetCustomString("QS.PersonalHair.Any")
            msg_DistanceVal.Text = CurrentPageData.GetCustomString("QS.AnyDistance")
            msg_DistanceFromMe_ND.Text = CurrentPageData.GetCustomString("msg_DistanceFromMe_ND")
            msg_AgeText.Text = CurrentPageData.GetCustomString("msg_AgeText")
            msg_BodyType.Text = CurrentPageData.GetCustomString("msg_PersonalBodyType")
            chkBeOnline.Text = CurrentPageData.GetCustomString("chkOnline")
            chkHasPhoto.Text = CurrentPageData.GetCustomString("chkPhotos")
            msg_HairColor.Text = CurrentPageData.GetCustomString("msg_PersonalHairClr")
            lblHeader.Text = CurrentPageData.GetCustomString("lblHeader")
          
            LoadLagInButtons()
            LoadQuickSearch()
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub
    Private Sub LoadLagInButtons()
        Try
            lblNextPageResults.Text = CurrentPageData.GetCustomString("lnkNextPageResults")
            lblPreviousPageResults.Text = CurrentPageData.GetCustomString("lnkPreviousPageResults")
            btnSearch.Text = CurrentPageData.GetCustomString("btnSearch")
            msg_divNoResultsTryChangeCriteria_ND.Text = CurrentPageData.GetCustomString("msg_divNoResultsTryChangeCriteria_ND")
            msg_divNoResultsTryChangeCriteria_ND.Text = msg_divNoResultsTryChangeCriteria_ND.Text.
         Replace("[SEARCH_NEAR_MEMBERS_URL]", ResolveUrl("~/Members/Search.aspx?vw=NEAREST")).
         Replace("[SEARCH_NEW_MEMBERS_URL]", ResolveUrl("~/Members/Search.aspx?vw=NEWEST"))
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub
    Private Sub LoadQuickSearch()
        Dim clstSpokenLang As ASPxListBox = ddeHairColorWrap.FindControl("clstHairColor")
        Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_HairColor, Me.GetLag(), "HairColorId", clstSpokenLang, True)
        If (clstSpokenLang.Items.Count > 0) Then
            Dim itm As DevExpress.Web.ASPxEditors.ListEditItem = clstSpokenLang.Items(0)
            itm.Value = -1
            itm.Text = CurrentPageData.GetCustomString("QS.PersonalHair.Any")
        End If
        Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_BodyType, Me.GetLag(), "BodyTypeId", cbBodyType, True)
        If (cbBodyType.Items.Count > 0) Then
            cbBodyType.Items.RemoveAt(0)
            For Each itm As DevExpress.Web.ASPxEditors.ListEditItem In cbBodyType.Items
                itm.Selected = True
            Next
        End If
    End Sub

    Private _list As List(Of clsNewQuickSearchUserListItem)
    Public Property UsersList As List(Of clsNewQuickSearchUserListItem)
        Get
            If (Me._list Is Nothing) Then Me._list = New List(Of clsNewQuickSearchUserListItem)
            Return Me._list
        End Get
        Set(value As List(Of clsNewQuickSearchUserListItem))
            Me._list = value
        End Set
    End Property
    Public Property showPreviousButton As Boolean
        Get
            Return lnkPreviousPageResults.Visible
        End Get
        Set(value As Boolean)

            lnkPreviousPageResults.Visible = value
        End Set
    End Property
    Public Property showNextButton As Boolean
        Get
            Return lnkNextPageResults.Visible
        End Get
        Set(value As Boolean)

            lnkNextPageResults.Visible = value
        End Set
    End Property
    Public Overrides Sub DataBind()
        If (Me.rptQuickSearch IsNot Nothing) Then

            Try
                For Each itm As clsNewQuickSearchUserListItem In Me.UsersList
                    itm.FillTextResourceProperties(Me.CurrentPageData)
                    If (itm.ZodiacString Is Nothing AndAlso itm.OtherMemberBirthday.HasValue AndAlso itm.OtherMemberBirthday.Value > DateTime.MinValue) Then
                        itm.ZodiacString = globalStrings.GetCustomString("Zodiac" & ProfileHelper.ZodiacName(itm.OtherMemberBirthday), itm.LAGID)
                    End If

                    'If (Not String.IsNullOrEmpty(itm.OtherMemberHeading)) Then
                    '    itm.AddItemCssClass = "bigger"
                    'End If
                Next
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try

            Me.rptQuickSearch.DataSource = Me.UsersList
            Me.rptQuickSearch.DataBind()
            If UsersList.Count = 0 Then
                showNextButton = False
                showPreviousButton = False
                divNoResultsChangeCriteria.Visible = True
            Else
                divNoResultsChangeCriteria.Visible = False
            End If
        End If
    End Sub
    Function getProfileClass(ByVal rowNumber As Integer) As String
        Try
            If rowNumber = 1 Or rowNumber = 4 Or rowNumber = 7 Then
                Return "OtherProfileContainer middleCol pr" & rowNumber
            ElseIf rowNumber = 2 Or rowNumber = 3 Or rowNumber = 6 Then
                Return "OtherProfileContainer rightCol pr" & rowNumber
            ElseIf rowNumber = 5 Or rowNumber = 8 Then
                Return "OtherProfileContainer leftCol pr" & rowNumber

            Else
                Return "OtherProfileContainer leftCol pr" & rowNumber
            End If
              
        Catch ex As Exception
            Return "OtherProfileContainer leftCol pr"
        End Try
    End Function
    Function getPhotoStyle(ByVal url As String) As String
        Try
            'Dim s As String = "<div class=""imageAsbg"" style=""background: url('" & url & "') no-repeat 0px 0px; background-size: cover;""></div>"
            Return "<div class=""imageAsbg"" style=""background: url('" & url & "') no-repeat 0px 0px; background-size: cover;""></div>"
        Catch ex As Exception
            Return "<div class=""imageAsbg"" style=""background: url('') no-repeat 0px 0px; background-size: cover;""></div>"
        End Try
    End Function
    Function evaluateAgeAndCity(birthday As Object, city As Object) As String
        Dim age As String = ""
        If birthday IsNot Nothing Then
            Try
                '  age = Me.CurrentPageData.VerifyCustomString("lblAgeYearsOld").Replace("###AGE###", ProfileHelper.GetCurrentAge(birthday))
                age = birthday

            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try
        End If
        If city IsNot Nothing Then
            Try
                '  age = Me.CurrentPageData.VerifyCustomString("lblAgeYearsOld").Replace("###AGE###", ProfileHelper.GetCurrentAge(birthday))
                If age <> "" Then
                    age &= ", "
                End If
                age &= CStr(city)

            Catch ex As Exception

                WebErrorMessageBox(Me, ex, "")
            End Try
        End If
        Return age

    End Function
    Function evaluateRegionAndCountry(region As Object, country As Object) As String
        Dim regionstr As String = ""
        If region IsNot Nothing Then
            Try
                '  age = Me.CurrentPageData.VerifyCustomString("lblAgeYearsOld").Replace("###AGE###", ProfileHelper.GetCurrentAge(birthday))
                regionstr = region

            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try
        End If
        If country IsNot Nothing Then
            Try
                '  age = Me.CurrentPageData.VerifyCustomString("lblAgeYearsOld").Replace("###AGE###", ProfileHelper.GetCurrentAge(birthday))
                If regionstr <> "" Then
                    regionstr &= ", "
                End If
                regionstr &= CStr(country)

            Catch ex As Exception

                WebErrorMessageBox(Me, ex, "")
            End Try
        End If
        Return regionstr

    End Function
    Private Sub FillContentWhoHasBirthday()

        'Dim params2 As New clsSearchHelperParameters()
        'params2.CurrentProfileId = Me.MasterProfileId
        'params2.ReturnRecordsWithStatus = ProfileStatusEnum.Approved
        'params2.SearchSort = SearchSortEnum.Birthday
        'params2.zipstr = Me.SessionVariables.MemberData.Zip
        'params2.latitudeIn = Me.SessionVariables.MemberData.latitude
        'params2.longitudeIn = Me.SessionVariables.MemberData.longitude
        'params2.Distance = 1000
        'params2.LookingFor_ToMeetMaleID = Me.SessionVariables.MemberData.LookingFor_ToMeetMaleID
        'params2.LookingFor_ToMeetFemaleID = Me.SessionVariables.MemberData.LookingFor_ToMeetFemaleID
        'params2.NumberOfRecordsToReturn = 4
        'params2.rowNumberMin = 0
        'params2.rowNumberMax = params2.NumberOfRecordsToReturn
        'params2.performCount = False
        'params2.AdditionalWhereClause = " and  (CelebratingBirth>=DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE())))"
        'lvWhoHasBirthday.DataSource = clsSearchHelper.GetMembersToSearchDataTable(params2)
        'lvWhoHasBirthday.DataBind()
    End Sub

    Public Property ageMaxSelectedValue As Integer Implements INewQuickSearchCriterias.ageMaxSelectedValue
        Get
            Return trcAge.PositionEnd
        End Get
        Set(value As Integer)
            trcAge.PositionStart = value
        End Set
    End Property
    Public Property ageMinSelectedValue As Integer Implements INewQuickSearchCriterias.ageMinSelectedValue
        Get
            Return trcAge.PositionStart
        End Get
        Set(value As Integer)
            trcAge.PositionStart = value
        End Set
    End Property
    Public Property chkOnlineChecked As Boolean Implements INewQuickSearchCriterias.chkOnlineChecked
        Get
            Return chkBeOnline.Checked
        End Get
        Set(value As Boolean)
            chkBeOnline.Checked = value
        End Set
    End Property
    Public Property chkPhotosChecked As Boolean Implements INewQuickSearchCriterias.chkPhotosChecked
        Get
            Return chkHasPhoto.Checked
        End Get
        Set(value As Boolean)
            chkHasPhoto.Checked = value
        End Set
    End Property
    Public Property ddlDistanceSelectedValue As Integer Implements INewQuickSearchCriterias.ddlDistanceSelectedValue
        Get
            If (trcDistance.Position = trcDistance.Items.Count - 1) Then
                Return clsSearchHelper.DISTANCE_DEFAULT
            End If
            Dim itm As DevExpress.Web.ASPxEditors.TrackBarItem = trcDistance.Items(trcDistance.Position)
            Return itm.Value
        End Get
        Set(value As Integer)
            ' ddlDistance.SelectedValue = value
        End Set
    End Property
    Public Event NextPageClicked(sender As Object, e As EventArgs) Implements INewQuickSearchCriterias.NextPageClicked

    Public Event PreviousPageClicked(sender As Object, e As EventArgs) Implements INewQuickSearchCriterias.PreviousPageClicked

    Public Event RefreshClicked(sender As Object, e As EventArgs) Implements INewQuickSearchCriterias.RefreshClicked

    Public Property SelectedBodyTypesList As SearchListInfo Implements INewQuickSearchCriterias.SelectedBodyTypesList
        Get
            Dim allSelected As Boolean = True
            Dim lst As New List(Of Integer)()
            For Each itm As DevExpress.Web.ASPxEditors.ListEditItem In cbBodyType.Items
                If (itm.Selected AndAlso itm.Value > -1) Then lst.Add(itm.Value)
                If (Not itm.Selected AndAlso itm.Value > -1) Then allSelected = False
            Next

            Dim li As New SearchListInfo()
            li.List = lst
            li.IsAllSelected = allSelected
            Return li
        End Get
        Set(value As SearchListInfo)
        End Set
    End Property
    Public Property SelectedHairColorList As SearchListInfo Implements INewQuickSearchCriterias.SelectedHairColorList
        Get
            Dim allSelected As Boolean = True
            Dim lst As New List(Of Integer)()
            Dim clstHairColor As ASPxListBox = ddeHairColorWrap.FindControl("clstHairColor")
            For Each itm As DevExpress.Web.ASPxEditors.ListEditItem In clstHairColor.Items
                If (itm.Selected AndAlso itm.Value > -1) Then lst.Add(itm.Value)
                If (Not itm.Selected AndAlso itm.Value > -1) Then allSelected = False
            Next

            Dim li As New SearchListInfo()
            li.List = lst
            li.IsAllSelected = allSelected
            Return li
        End Get
        Set(value As SearchListInfo)
        End Set
    End Property
    Public Sub VerifyControlLoaded() Implements INewQuickSearchCriterias.VerifyControlLoaded
        
    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        RaiseEvent RefreshClicked(sender, e)
    End Sub

    Private Sub lnkNextPageResults_Click(sender As Object, e As EventArgs) Handles lnkNextPageResults.Click
        RaiseEvent NextPageClicked(sender, e)
    End Sub

    Private Sub lnkPreviousPageResults_Click(sender As Object, e As EventArgs) Handles lnkPreviousPageResults.Click
        RaiseEvent PreviousPageClicked(sender, e)
    End Sub

    Private Sub Page_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        Try
            If Not Me.IsPostBack Then
          
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try
     
    End Sub
End Class
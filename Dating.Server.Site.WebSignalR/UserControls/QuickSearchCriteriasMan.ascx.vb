﻿Imports Dating.Server.Core.DLL

Public Class QuickSearchCriteriasMan
    Inherits BaseUserControl

    Public Event SearchByUserNameClicked(sender As Object, e As System.Web.UI.ImageClickEventArgs)
    Public Event SearchClicked(sender As Object, e As EventArgs)
    Public Event AdvancedClicked(sender As Object, e As EventArgs)

    Protected Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData("~/Members/Search.aspx", Context, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property


    Public Property txtUserNameQText As String
        Get
            Return txtUserNameQ.Text
        End Get
        Set(value As String)
            txtUserNameQ.Text = value
        End Set
    End Property



    Public Property ageMinSelectedValue As String
        Get
            Return ageMin.SelectedValue
        End Get
        Set(value As String)
            ageMin.SelectedValue = value
        End Set
    End Property


    Public Property ageMaxSelectedValue As String
        Get
            Return ageMax.SelectedValue
        End Get
        Set(value As String)
            ageMax.SelectedValue = value
        End Set
    End Property


    Public Property ddlDistanceSelectedValue As String
        Get
            Return ddlDistance.SelectedValue
        End Get
        Set(value As String)
            ddlDistance.SelectedValue = value
        End Set
    End Property


    Public Property chkOnlineChecked As Boolean
        Get
            Return chkOnline.Checked
        End Get
        Set(value As Boolean)
            chkOnline.Checked = value
        End Set
    End Property


    Public Property chkPhotosChecked As Boolean
        Get
            Return chkPhotos.Checked
        End Get
        Set(value As Boolean)
            chkPhotos.Checked = value
        End Set
    End Property


    Public Property chkVIPChecked As Boolean
        Get
            Return chkVIP.Checked
        End Get
        Set(value As Boolean)
            chkVIP.Checked = value
        End Set
    End Property


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If (Not Me.IsPostBack) Then
                LoadLAG()
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub



    Protected Sub LoadLAG()
        Try
            '  Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            SetControlsValue(Me, CurrentPageData)
            ''lblItemsName.Text = Me.CurrentPageData.GetCustomString("msg_RefineYourSearchTitle_ND")

            lnkAdvanced.Text = CurrentPageData.GetCustomString("lnkAdvanced_ND")
            chkPhotos.Text = CurrentPageData.GetCustomString(chkPhotos.ID)
            chkOnline.Text = CurrentPageData.GetCustomString(chkOnline.ID)
            If (Me.IsMale) Then
                chkVIP.Visible = False
            Else
                chkVIP.Text = CurrentPageData.GetCustomString(chkVIP.ID)
            End If
            btnSearch_ND.Text = CurrentPageData.GetCustomString("btnSearch_ND")
            chkPhotos.Text = CurrentPageData.GetCustomString(chkPhotos.ID)
            chkPhotos.Text = CurrentPageData.GetCustomString(chkPhotos.ID)


            lblYears.Text = CurrentPageData.GetCustomString("lblYears_ND")
            'msg_UserNameSearchQ.Text = CurrentPageData.GetCustomString("msg_UserNameSearch")
            txtUserNameQ.NullText = CurrentPageData.GetCustomString("msg_UserNameSearch")


        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Protected Sub imgSearchByUserName_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles imgSearchByUserName.Click
        RaiseEvent SearchByUserNameClicked(sender, e)
    End Sub

    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch_ND.Click
        RaiseEvent SearchClicked(sender, e)
    End Sub

    Protected Sub hlAdvanced_Click(sender As Object, e As EventArgs) Handles lnkAdvanced.Click
        RaiseEvent AdvancedClicked(sender, e)
    End Sub

    Public Sub ShowBirthdayNote()
        msg_BirthdayNote.Visible = True
    End Sub


    Public Sub Distance_SelectComboItem(value As Object)
        ClsCombos.SelectComboItem(ddlDistance.ListControl, value)
    End Sub


    'Public Property ShowNoPhotoText As Boolean
    'Public Property ShowEmptyListText As Boolean

    'Public Property UsersListView As WinkUsersListViewEnum
    '    Get
    '        If (Me.ViewState("WinkUsersListViewEnum") IsNot Nothing) Then Return Me.ViewState("WinkUsersListViewEnum")
    '        Return WinkUsersListViewEnum.None
    '    End Get
    '    Set(value As WinkUsersListViewEnum)
    '        Me.ViewState("WinkUsersListViewEnum") = value
    '    End Set
    'End Property


    'Private _list As List(Of clsWinkUserListItem)
    'Public Property UsersList As List(Of clsWinkUserListItem)
    '    Get
    '        If (Me._list Is Nothing) Then Me._list = New List(Of clsWinkUserListItem)
    '        Return Me._list
    '    End Get
    '    Set(value As List(Of clsWinkUserListItem))
    '        Me._list = value
    '    End Set
    'End Property



    '<PersistenceMode(PersistenceMode.InnerDefaultProperty)>
    'Public ReadOnly Property Repeater As Repeater
    '    Get
    '        If (UsersListView = WinkUsersListViewEnum.AcceptedOffers) Then
    '            Return Me.rptAccepted

    '        ElseIf (UsersListView = WinkUsersListViewEnum.NewOffers) Then
    '            Return Me.rptNew

    '        ElseIf (UsersListView = WinkUsersListViewEnum.LikesOffers) Then
    '            Return Me.rptNew

    '        ElseIf (UsersListView = WinkUsersListViewEnum.PokesOffers) Then
    '            Return Me.rptNew

    '        ElseIf (UsersListView = WinkUsersListViewEnum.PendingOffers) Then
    '            Return Me.rptPending

    '        ElseIf (UsersListView = WinkUsersListViewEnum.RejectedOffers) Then
    '            Return Me.rptRejected

    '        Else
    '            Return Nothing
    '        End If
    '    End Get
    'End Property


    'Protected Overrides Sub OnInit(e As System.EventArgs)
    '    MyBase.OnInit(e)
    '    'ModGlobals.AddScriptResourceToHeader(Me.Page, "~/Scripts/jquery-1.7.min.js")
    '    'ModGlobals.AddScriptResourceToHeader(Me.Page, "~/Scripts/bootstrap-dropdown.js")
    'End Sub



    'Public Overrides Sub DataBind()

    '    If (Me.ShowNoPhotoText) Then

    '        Dim oNoPhoto As New clsWinkUserListItem()
    '        oNoPhoto.HasNoPhotosText = Me.CurrentPageData.GetCustomString("HasNoPhotosText")
    '        oNoPhoto.AddPhotosText = Me.CurrentPageData.GetCustomString("AddPhotosText")

    '        Dim os As New List(Of clsWinkUserListItem)
    '        os.Add(oNoPhoto)

    '        fvNoPhoto.DataSource = os
    '        fvNoPhoto.DataBind()

    '        Me.MultiView1.SetActiveView(vwNoPhoto)


    '    ElseIf (Me.ShowEmptyListText) Then

    '        Dim oNoOffer As New clsWinkUserListItem()
    '        If (UsersListView = WinkUsersListViewEnum.AcceptedOffers) Then
    '            oNoOffer.NoOfferText = Me.CurrentPageData.GetCustomString("AcceptedOffersListEmptyText_ND")

    '        ElseIf (UsersListView = WinkUsersListViewEnum.NewOffers) Then
    '            oNoOffer.NoOfferText = Me.CurrentPageData.GetCustomString("NewOffersListEmptyText_ND")

    '        ElseIf (UsersListView = WinkUsersListViewEnum.LikesOffers) Then
    '            oNoOffer.NoOfferText = Me.CurrentPageData.GetCustomString("WinksListEmptyText_ND")

    '        ElseIf (UsersListView = WinkUsersListViewEnum.PokesOffers) Then
    '            oNoOffer.NoOfferText = Me.CurrentPageData.GetCustomString("PokesListEmptyText")

    '        ElseIf (UsersListView = WinkUsersListViewEnum.PendingOffers) Then
    '            oNoOffer.NoOfferText = Me.CurrentPageData.GetCustomString("PendingOffersListEmptyText_ND")

    '        ElseIf (UsersListView = WinkUsersListViewEnum.RejectedOffers) Then
    '            oNoOffer.NoOfferText = Me.CurrentPageData.GetCustomString("RejectedOffersListEmptyText_ND")


    '        ElseIf (UsersListView = WinkUsersListViewEnum.WhoViewedMeList) Then
    '            oNoOffer.NoOfferText = Me.CurrentPageData.GetCustomString("WhoViewedMeListEmptyText")

    '        ElseIf (UsersListView = WinkUsersListViewEnum.WhoFavoritedMeList) Then
    '            oNoOffer.NoOfferText = Me.CurrentPageData.GetCustomString("WhoFavoritedMeListEmptyText")

    '        ElseIf (UsersListView = WinkUsersListViewEnum.MyFavoriteList) Then
    '            oNoOffer.NoOfferText = Me.CurrentPageData.GetCustomString("MyFavoriteListEmptyText")

    '        ElseIf (UsersListView = WinkUsersListViewEnum.MyBlockedList) Then
    '            oNoOffer.NoOfferText = Me.CurrentPageData.GetCustomString("MyBlockedListEmptyText")

    '        ElseIf (UsersListView = WinkUsersListViewEnum.MyViewedList) Then
    '            oNoOffer.NoOfferText = Me.CurrentPageData.GetCustomString("MyViewedListEmptyText")

    '        End If

    '        oNoOffer.SearchOurMembersText = Me.CurrentPageData.GetCustomString("SearchOurMembersText")

    '        Dim os As New List(Of clsWinkUserListItem)
    '        os.Add(oNoOffer)

    '        fvNoOffers.DataSource = os
    '        fvNoOffers.DataBind()

    '        Me.MultiView1.SetActiveView(vwNoOffers)

    '    Else

    '        If (UsersListView = WinkUsersListViewEnum.AcceptedOffers) Then
    '            Me.MultiView1.SetActiveView(vwAcceptedOffers)

    '        ElseIf (UsersListView = WinkUsersListViewEnum.NewOffers) Then
    '            Me.MultiView1.SetActiveView(vwNewOffers)

    '        ElseIf (UsersListView = WinkUsersListViewEnum.LikesOffers) Then
    '            Me.MultiView1.SetActiveView(vwNewOffers)

    '        ElseIf (UsersListView = WinkUsersListViewEnum.PokesOffers) Then
    '            Me.MultiView1.SetActiveView(vwNewOffers)

    '        ElseIf (UsersListView = WinkUsersListViewEnum.PendingOffers) Then
    '            Me.MultiView1.SetActiveView(vwPendingOffers)

    '        ElseIf (UsersListView = WinkUsersListViewEnum.RejectedOffers) Then
    '            Me.MultiView1.SetActiveView(vwRejectedOffers)

    '        Else
    '            Me.MultiView1.SetActiveView(View1)

    '        End If
    '    End If




    '    If (Me.Repeater IsNot Nothing) Then

    '        Try
    '            For Each itm As clsWinkUserListItem In Me.UsersList
    '                itm.FillTextResourceProperties(Me.CurrentPageData)
    '                If (itm.OtherMemberBirthday.HasValue AndAlso itm.OtherMemberBirthday.Value > DateTime.MinValue) Then
    '                    itm.ZodiacString = globalStrings.GetCustomString("Zodiac" & ProfileHelper.ZodiacName(itm.OtherMemberBirthday), itm.LAGID)
    '                End If
    '            Next
    '        Catch ex As Exception
    '            WebErrorMessageBox(Me, ex, "")
    '        End Try

    '        Me.Repeater.DataSource = Me.UsersList
    '        Me.Repeater.DataBind()
    '    End If

    'End Sub



    'Protected Function WriteOffer_MemberInfo(DataItem As Dating.Server.Site.Web.clsWinkUserListItem) As String
    '    Dim sb As New StringBuilder()
    '    Try
    '        sb.Append("<ul class=""info2"">")

    '        If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberAge) Then
    '            sb.Append("<li class=""age1"">")
    '            sb.Append("<b>")
    '            sb.Append(DataItem.OtherMemberAge)
    '            sb.Append(" " & DataItem.YearsOldText & " ")
    '            sb.Append("</b>")
    '            sb.Append("</li>")
    '        End If


    '        If (Not String.IsNullOrEmpty(DataItem.OtherMemberCity)) Then
    '            sb.Append("<li>")
    '            sb.Append(IIf(Not String.IsNullOrEmpty(DataItem.OtherMemberCity), DataItem.OtherMemberCity, ""))
    '            If (Not String.IsNullOrEmpty(DataItem.OtherMemberRegion)) Then sb.Append(",  ")
    '            sb.Append("</li>")
    '        End If

    '        sb.Append("<li>")
    '        sb.Append(IIf(Not String.IsNullOrEmpty(DataItem.OtherMemberRegion), DataItem.OtherMemberRegion, ""))
    '        sb.Append(",  ")
    '        sb.Append(IIf(Not String.IsNullOrEmpty(DataItem.OtherMemberCountry), ProfileHelper.GetCountryName(DataItem.OtherMemberCountry), ""))
    '        sb.Replace(",  " & ",  ", ",  ")
    '        sb.Append("</li>")


    '        sb.Append("</ul>")

    '    Catch ex As Exception
    '        WebErrorMessageBox(Me, ex, "")
    '    End Try

    '    Return sb.ToString()
    'End Function


    'Protected Function WriteSearch_MemberInfo(DataItem As Dating.Server.Site.Web.clsWinkUserListItem) As String

    '    'Dim str As String = "<ul>" & vbCrLf & _
    '    '    "<li><b>" & DataItem.OtherMemberAge & "</b> " & DataItem.YearsOldText & " <b>" & Eval("OtherMemberCity & ", " & Eval("OtherMemberRegion & "</b></li>" & vbCrLf & _
    '    '    "<li><b>" & DataItem.OtherMemberHeight & " - " & DataItem.OtherMemberPersonType & "</b></li>" & vbCrLf & _
    '    '    "<li>" & DataItem.OtherMemberHair & ", " & DataItem.OtherMemberEyes & ", " & DataItem.OtherMemberEthnicity & "</li>" & vbCrLf & _
    '    '    "</ul>" & vbCrLf

    '    Dim sb As New StringBuilder()
    '    Try

    '        sb.Append("<ul>")

    '        sb.Append("<li>")

    '        If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberAge) Then
    '            sb.Append("<b>" & DataItem.OtherMemberAge & "</b>")
    '            sb.Append(" " & DataItem.YearsOldText & " ")
    '        End If

    '        sb.Append("<b>")

    '        sb.Append(IIf(Not String.IsNullOrEmpty(DataItem.OtherMemberCity), DataItem.OtherMemberCity, ""))
    '        sb.Append(",  ")
    '        sb.Append(IIf(Not String.IsNullOrEmpty(DataItem.OtherMemberRegion), DataItem.OtherMemberRegion, ""))
    '        sb.Append(",  ")
    '        sb.Append(IIf(Not String.IsNullOrEmpty(DataItem.OtherMemberCountry), ProfileHelper.GetCountryName(DataItem.OtherMemberCountry), ""))
    '        sb.Replace(",  " & ",  ", ",  ")

    '        'If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberCity) Then
    '        'End If

    '        'If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberCity) AndAlso Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberRegion) Then
    '        'End If

    '        'If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberRegion) Then
    '        'End If

    '        'If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberRegion) AndAlso Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberCountry) Then
    '        'End If

    '        'If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberCountry) Then
    '        'End If

    '        sb.Append("</b>")
    '        sb.Append("</li>")


    '        sb.Append("<li>")
    '        sb.Append("<b>")

    '        If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberHeight) Then
    '            sb.Append(DataItem.OtherMemberHeight)
    '        End If

    '        If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberHeight) AndAlso Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberPersonType) Then
    '            sb.Append(" - ")
    '        End If

    '        If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberPersonType) Then
    '            sb.Append(DataItem.OtherMemberPersonType)
    '        End If

    '        sb.Append("</b>")
    '        sb.Append("</li>")


    '        sb.Append("<li>")

    '        If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberHair) Then
    '            sb.Append(DataItem.OtherMemberHair)
    '        End If

    '        If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberEyes) Then
    '            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberHair) Then sb.Append(", ")
    '            sb.Append(DataItem.OtherMemberEyes)
    '        End If

    '        If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberEthnicity) Then
    '            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberEyes) OrElse Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberHair) Then sb.Append(", ")
    '            sb.Append(DataItem.OtherMemberEthnicity)
    '        End If

    '        sb.Append("</li>")
    '        sb.Append("</ul>")

    '    Catch ex As Exception
    '        WebErrorMessageBox(Me, ex, "")
    '    End Try

    '    Return sb.ToString()
    'End Function


    ''Protected Sub rptAccepted_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptAccepted.ItemDataBound
    ''    Try

    ''        Dim divTooltipholder As Control = e.Item.FindControl("divTooltipholder")
    ''        If (divTooltipholder IsNot Nothing) Then
    ''            Dim itm As clsWinkUserListItem = e.Item.DataItem
    ''            If (ProfileHelper.IsMale(itm.Genderid)) Then
    ''                divTooltipholder.Visible = True
    ''            End If
    ''        End If

    ''    Catch ex As Exception

    ''    End Try
    ''End Sub

    'Protected Sub rptNew_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptNew.ItemDataBound
    '    Try

    '        Dim lnkWhatIs As LinkButton = e.Item.FindControl("lnkWhatIs")
    '        If (lnkWhatIs IsNot Nothing) Then
    '            lnkWhatIs.ToolTip = Me.CurrentPageData.GetCustomString("WhatIsText")
    '            Dim contentUrl As String = ResolveUrl("~/Members/InfoWin.aspx?info=LikesNewCommands")
    '            contentUrl = contentUrl.Replace("'", "\'")
    '            Dim headerText As String = Me.CurrentPageData.GetCustomString("Search_WhatIsPopup_HeaderText")
    '            If (Not String.IsNullOrEmpty(headerText)) Then headerText = headerText.Replace("'", "\'")
    '            lnkWhatIs.OnClientClick = lnkWhatIs.OnClientClick.Replace("[contentUrl]", contentUrl)
    '            lnkWhatIs.OnClientClick = lnkWhatIs.OnClientClick.Replace("[headerText]", headerText)
    '        End If

    '    Catch ex As Exception

    '    End Try
    'End Sub

    'Protected Sub rptPending_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptPending.ItemDataBound
    '    Try

    '        Dim lnkWhatIs As LinkButton = e.Item.FindControl("lnkWhatIs")
    '        If (lnkWhatIs IsNot Nothing) Then
    '            lnkWhatIs.ToolTip = Me.CurrentPageData.GetCustomString("WhatIsText")
    '            Dim contentUrl As String = ResolveUrl("~/Members/InfoWin.aspx?info=LikesPendingCommands")
    '            contentUrl = contentUrl.Replace("'", "\'")
    '            Dim headerText As String = Me.CurrentPageData.GetCustomString("Search_WhatIsPopup_HeaderText")
    '            If (Not String.IsNullOrEmpty(headerText)) Then headerText = headerText.Replace("'", "\'")
    '            lnkWhatIs.OnClientClick = lnkWhatIs.OnClientClick.Replace("[contentUrl]", contentUrl)
    '            lnkWhatIs.OnClientClick = lnkWhatIs.OnClientClick.Replace("[headerText]", headerText)
    '        End If

    '    Catch ex As Exception

    '    End Try
    'End Sub

    'Protected Sub rptRejected_ItemDataBound(source As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptRejected.ItemDataBound
    '    Try

    '        Dim lnkWhatIs As LinkButton = e.Item.FindControl("lnkWhatIs")
    '        If (lnkWhatIs IsNot Nothing) Then
    '            lnkWhatIs.ToolTip = Me.CurrentPageData.GetCustomString("WhatIsText")
    '            Dim contentUrl As String = ResolveUrl("~/Members/InfoWin.aspx?info=LikesRejectedCommands")
    '            contentUrl = contentUrl.Replace("'", "\'")
    '            Dim headerText As String = Me.CurrentPageData.GetCustomString("Search_WhatIsPopup_HeaderText")
    '            If (Not String.IsNullOrEmpty(headerText)) Then headerText = headerText.Replace("'", "\'")
    '            lnkWhatIs.OnClientClick = lnkWhatIs.OnClientClick.Replace("[contentUrl]", contentUrl)
    '            lnkWhatIs.OnClientClick = lnkWhatIs.OnClientClick.Replace("[headerText]", headerText)
    '        End If

    '    Catch ex As Exception

    '    End Try
    'End Sub

    'Protected Sub rptAccepted_ItemDataBound(source As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptAccepted.ItemDataBound
    '    Try

    '        Dim lnkWhatIs As LinkButton = e.Item.FindControl("lnkWhatIs")
    '        If (lnkWhatIs IsNot Nothing) Then
    '            lnkWhatIs.ToolTip = Me.CurrentPageData.GetCustomString("WhatIsText")
    '            Dim contentUrl As String = ResolveUrl("~/Members/InfoWin.aspx?info=LikesAcceptedCommands")
    '            contentUrl = contentUrl.Replace("'", "\'")
    '            Dim headerText As String = Me.CurrentPageData.GetCustomString("Search_WhatIsPopup_HeaderText")
    '            If (Not String.IsNullOrEmpty(headerText)) Then headerText = headerText.Replace("'", "\'")
    '            lnkWhatIs.OnClientClick = lnkWhatIs.OnClientClick.Replace("[contentUrl]", contentUrl)
    '            lnkWhatIs.OnClientClick = lnkWhatIs.OnClientClick.Replace("[headerText]", headerText)
    '        End If


    '    Catch ex As Exception

    '    End Try
    'End Sub


End Class
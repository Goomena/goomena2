﻿Public Class CountryDropDown
    Inherits System.Web.UI.UserControl


    Public Property Style As String
        Get
            If (Not ViewState("Style") Is Nothing) Then
                Return ViewState("Style").ToString()
            End If
            Return ""
        End Get
        Set(ByVal value As String)
            ViewState("Style") = value
        End Set
    End Property


    Public Property CssClass As String
        Get
            If (Not ViewState("CssClass") Is Nothing) Then
                Return ViewState("CssClass").ToString()
            End If
            Return ""
        End Get
        Set(ByVal value As String)
            ViewState("CssClass") = value
        End Set
    End Property


    Public Property DefaultValue As String
        Get
            If (Not ViewState("DefaultValue") Is Nothing) Then
                Return ViewState("DefaultValue").ToString()
            End If
            Return Nothing
        End Get
        Set(ByVal value As String)
            ViewState("DefaultValue") = value
        End Set
    End Property


    Public Property SelectedValue As String
        Get

            If (ddl.SelectedIndex > -1) Then
                Return ddl.Items(ddl.SelectedIndex).Value
            End If
            Return Nothing

        End Get
        Set(ByVal value As String)

            If (Not value Is Nothing) Then
                Dim itm As ListItem = Me.ddl.Items.FindByValue(value)
                If (Not itm Is Nothing) Then
                    itm.Selected = True
                End If
            End If

        End Set
    End Property


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub


    Protected Overrides Sub OnPreRender(ByVal e As System.EventArgs)
        MyBase.OnPreRender(e)

        If (Not String.IsNullOrEmpty(Me.Style)) Then
            Me.ddl.Attributes.Add("style", Me.Style)
        End If

        If (Not String.IsNullOrEmpty(Me.CssClass)) Then
            Me.ddl.Attributes.Add("class", Me.CssClass)
        End If

        If Not Me.DefaultValue Is Nothing AndAlso Me.ddl.Items(0).Value <> "-1" Then
            Me.ddl.Items.Insert(0, New ListItem(Me.DefaultValue, "-1"))
        End If

    End Sub

End Class
﻿Imports DevExpress.Web.ASPxPager
Imports Dating.Server.Core.DLL

Public Class OfferControl3
    Inherits BaseUserControl


    Private _VIPTooltip As String
    Protected Property VIPTooltip As String
        Get
            Return _VIPTooltip
        End Get
        Set(value As String)
            _VIPTooltip = value
        End Set
    End Property


    Public Enum WinkUsersListViewEnum
        None = 0

        LikesOffers = 12
        NewOffers = 1
        PendingOffers = 2
        RejectedOffers = 3
        AcceptedOffers = 4
        PokesOffers = 14

        SearchingOffers = 5

        WhoViewedMeList = 6
        WhoFavoritedMeList = 7
        WhoSharedPhotoList = 11
        SharedPhotosByMeList = 20
        MyFavoriteList = 8
        MyBlockedList = 9
        MyViewedList = 10

    End Enum

    Public Enum MessagesListViewEnum
        None = 0
        AllMessages = 24

        NewMessages = 12
        Inbox = 1
        SentMessages = 2
        Trash = 3
    End Enum

    Public Property InfoWinUrl As String
    Public Property InfoWinHeaderText As String

    Public Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            'If (_pageData Is Nothing) Then _pageData = New clsPageData("control.OfferControl", Context)
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData("control.OfferControl", Context, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property


    Public Property ShowNoPhotoText As Boolean
    Public Property ShowEmptyListText As Boolean

    Public Property UsersListView As WinkUsersListViewEnum
        Get
            If (Me.ViewState("WinkUsersListViewEnum") IsNot Nothing) Then Return Me.ViewState("WinkUsersListViewEnum")
            Return WinkUsersListViewEnum.None
        End Get
        Set(value As WinkUsersListViewEnum)
            Me.ViewState("WinkUsersListViewEnum") = value
        End Set
    End Property


    Private _list As List(Of clsWinkUserListItem)
    Public Property UsersList As List(Of clsWinkUserListItem)
        Get
            If (Me._list Is Nothing) Then Me._list = New List(Of clsWinkUserListItem)
            Return Me._list
        End Get
        Set(value As List(Of clsWinkUserListItem))
            Me._list = value
        End Set
    End Property



    <PersistenceMode(PersistenceMode.InnerDefaultProperty)>
    Public ReadOnly Property Repeater As Repeater
        Get
            If (UsersListView = WinkUsersListViewEnum.AcceptedOffers) Then
                Return Me.rptAccepted

            ElseIf (UsersListView = WinkUsersListViewEnum.NewOffers) Then
                Return Me.rptNew

            ElseIf (UsersListView = WinkUsersListViewEnum.LikesOffers) Then
                Return Me.rptNew

            ElseIf (UsersListView = WinkUsersListViewEnum.PokesOffers) Then
                Return Me.rptNew

            ElseIf (UsersListView = WinkUsersListViewEnum.PendingOffers) Then
                Return Me.rptPending

            ElseIf (UsersListView = WinkUsersListViewEnum.RejectedOffers) Then
                Return Me.rptRejected

                'ElseIf (UsersListView = WinkUsersListViewEnum.SearchingOffers _
                '        OrElse UsersListView = WinkUsersListViewEnum.WhoViewedMeList _
                '        OrElse UsersListView = WinkUsersListViewEnum.WhoFavoritedMeList _
                '        OrElse UsersListView = WinkUsersListViewEnum.WhoSharedPhotoList _
                '        OrElse UsersListView = WinkUsersListViewEnum.SharedPhotosByMeList _
                '        OrElse UsersListView = WinkUsersListViewEnum.MyFavoriteList _
                '        OrElse UsersListView = WinkUsersListViewEnum.MyBlockedList _
                '        OrElse UsersListView = WinkUsersListViewEnum.MyViewedList) Then
                '    Return Me.rptSearch

            Else
                Return Nothing
            End If
        End Get
    End Property


    Protected Overrides Sub OnInit(e As System.EventArgs)
        MyBase.OnInit(e)
        'ModGlobals.AddScriptResourceToHeader(Me.Page, "~/Scripts/jquery-1.7.min.js")
        'ModGlobals.AddScriptResourceToHeader(Me.Page, "~/Scripts/bootstrap-dropdown.js")
    End Sub



    Public Overrides Sub DataBind()

        If (Me.ShowNoPhotoText) Then

            Dim oNoPhoto As New clsWinkUserListItem()
            '  oNoPhoto.HasNoPhotosText = Me.CurrentPageData.GetCustomString("HasNoPhotosText")
            '    oNoPhoto.AddPhotosText = Me.CurrentPageData.GetCustomString("AddPhotosText")

            Dim os As New List(Of clsWinkUserListItem)
            os.Add(oNoPhoto)

            fvNoPhoto.DataSource = os
            fvNoPhoto.DataBind()

            Me.MultiView1.SetActiveView(vwNoPhoto)


        ElseIf (Me.ShowEmptyListText) Then

            Dim oNoOffer As New clsWinkUserListItem()
            If (UsersListView = WinkUsersListViewEnum.AcceptedOffers) Then
                oNoOffer.NoOfferText = Me.CurrentPageData.GetCustomString("AcceptedOffersListEmptyText_ND")

            ElseIf (UsersListView = WinkUsersListViewEnum.NewOffers) Then
                oNoOffer.NoOfferText = Me.CurrentPageData.GetCustomString("NewOffersListEmptyText_ND")

            ElseIf (UsersListView = WinkUsersListViewEnum.LikesOffers) Then
                oNoOffer.NoOfferText = Me.CurrentPageData.GetCustomString("WinksListEmptyText")

            ElseIf (UsersListView = WinkUsersListViewEnum.PokesOffers) Then
                oNoOffer.NoOfferText = Me.CurrentPageData.GetCustomString("PokesListEmptyText")

            ElseIf (UsersListView = WinkUsersListViewEnum.PendingOffers) Then
                oNoOffer.NoOfferText = Me.CurrentPageData.GetCustomString("PendingOffersListEmptyText_ND")

            ElseIf (UsersListView = WinkUsersListViewEnum.RejectedOffers) Then
                oNoOffer.NoOfferText = Me.CurrentPageData.GetCustomString("RejectedOffersListEmptyText_ND")


            ElseIf (UsersListView = WinkUsersListViewEnum.WhoViewedMeList) Then
                oNoOffer.NoOfferText = Me.CurrentPageData.GetCustomString("WhoViewedMeListEmptyText_ND")

            ElseIf (UsersListView = WinkUsersListViewEnum.WhoFavoritedMeList) Then
                oNoOffer.NoOfferText = Me.CurrentPageData.GetCustomString("WhoFavoritedMeListEmptyText_ND")

            ElseIf (UsersListView = WinkUsersListViewEnum.WhoSharedPhotoList) Then
                oNoOffer.NoOfferText = Me.CurrentPageData.GetCustomString("WhoSharedPhotoListEmptyText_ND")

            ElseIf (UsersListView = WinkUsersListViewEnum.SharedPhotosByMeList) Then
                oNoOffer.NoOfferText = Me.CurrentPageData.GetCustomString("SharedPhotosByMeListEmptyText_ND")

            ElseIf (UsersListView = WinkUsersListViewEnum.MyFavoriteList) Then
                oNoOffer.NoOfferText = Me.CurrentPageData.GetCustomString("MyFavoriteListEmptyText_ND")

            ElseIf (UsersListView = WinkUsersListViewEnum.MyBlockedList) Then
                oNoOffer.NoOfferText = Me.CurrentPageData.GetCustomString("MyBlockedListEmptyText_ND")

            ElseIf (UsersListView = WinkUsersListViewEnum.MyViewedList) Then
                oNoOffer.NoOfferText = Me.CurrentPageData.GetCustomString("MyViewedListEmptyText_ND")

            End If

            '  oNoOffer.SearchOurMembersText = Me.CurrentPageData.GetCustomString("SearchOurMembersText")

            Dim os As New List(Of clsWinkUserListItem)
            os.Add(oNoOffer)

            fvNoOffers.DataSource = os
            fvNoOffers.DataBind()

            Me.MultiView1.SetActiveView(vwNoOffers)

        Else

            If (UsersListView = WinkUsersListViewEnum.AcceptedOffers) Then
                Me.MultiView1.SetActiveView(vwAcceptedOffers)

            ElseIf (UsersListView = WinkUsersListViewEnum.NewOffers) Then
                Me.MultiView1.SetActiveView(vwNewOffers)

            ElseIf (UsersListView = WinkUsersListViewEnum.LikesOffers) Then
                Me.MultiView1.SetActiveView(vwNewOffers)

            ElseIf (UsersListView = WinkUsersListViewEnum.PokesOffers) Then
                Me.MultiView1.SetActiveView(vwNewOffers)

            ElseIf (UsersListView = WinkUsersListViewEnum.PendingOffers) Then
                Me.MultiView1.SetActiveView(vwPendingOffers)

            ElseIf (UsersListView = WinkUsersListViewEnum.RejectedOffers) Then
                Me.MultiView1.SetActiveView(vwRejectedOffers)
            Else
                Me.MultiView1.SetActiveView(View1)

            End If
        End If




        If (Me.Repeater IsNot Nothing) Then

            Try
                For Each itm As clsWinkUserListItem In Me.UsersList
                    itm.FillTextResourceProperties(MykeyStrings)
                    If (itm.ZodiacString Is Nothing AndAlso itm.OtherMemberBirthday.HasValue AndAlso itm.OtherMemberBirthday.Value > DateTime.MinValue) Then
                        Dim tmpKeystr As String = "Zodiac" & ProfileHelper.ZodiacName(itm.OtherMemberBirthday)
                        If MykeyStrings.ZodiacNames.ContainsKey(tmpKeystr & itm.LAGID) Then
                            itm.ZodiacString = MykeyStrings.ZodiacNames(tmpKeystr & itm.LAGID)
                        Else
                            itm.ZodiacString = globalStrings.GetCustomString(tmpKeystr, itm.LAGID)
                            MykeyStrings.ZodiacNames.Add(tmpKeystr & itm.LAGID, itm.ZodiacString)
                        End If
                    End If
                Next
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try

            Me.Repeater.DataSource = Me.UsersList
            Me.Repeater.DataBind()
        End If


        If (Me.IsFemale) Then
            VIPTooltip = Me.CurrentPageData.GetCustomString("lbTooltip.MenVIP.ForWomen")
        Else
            VIPTooltip = Me.CurrentPageData.GetCustomString("lbTooltip.MenVIP.ForMen")
        End If

    End Sub

    Public Property MykeyStrings As New KeyStrings
    Public Function FillKeyStrings(Optional ByVal Strings As KeyStrings = Nothing, Optional ByVal pageData As clsPageData = Nothing) As Boolean

        If pageData Is Nothing Then
            pageData = CurrentPageData
        End If
        If Strings Is Nothing Then
            Strings = MykeyStrings
        End If
        If (String.IsNullOrEmpty(Strings.ActionsMakeOfferText)) Then Strings.ActionsMakeOfferText = pageData.GetCustomString("ActionsMakeOfferText")
        If (String.IsNullOrEmpty(Strings.SendMessageOnceText)) Then Strings.SendMessageOnceText = pageData.GetCustomString("SendMessageOnceText")
        If (String.IsNullOrEmpty(Strings.SendMessageManyText)) Then Strings.SendMessageManyText = pageData.GetCustomString("SendMessageManyText")
        If (String.IsNullOrEmpty(Strings.UnlockNotice)) Then Strings.UnlockNotice = pageData.GetCustomString("UnlockNotice")
        If (String.IsNullOrEmpty(Strings.ActionsUnlockText)) Then Strings.ActionsUnlockText = pageData.GetCustomString("ActionsUnlockText")
        If (String.IsNullOrEmpty(Strings.OfferAcceptedUnlockText)) Then Strings.OfferAcceptedUnlockText = pageData.GetCustomString("OfferAcceptedUnlockText")
        If (String.IsNullOrEmpty(Strings.OfferAcceptedHowToContinueText)) Then Strings.OfferAcceptedHowToContinueText = pageData.GetCustomString("OfferAcceptedHowToContinueText")
        If (String.IsNullOrEmpty(Strings.WillYouAcceptDateWithForAmountText)) Then Strings.WillYouAcceptDateWithForAmountText = pageData.GetCustomString("WillYouAcceptDateWithForAmountText")
        If (String.IsNullOrEmpty(Strings.YouReceivedPokeDescriptionText)) Then Strings.YouReceivedPokeDescriptionText = pageData.GetCustomString("YouReceivedPokeDescriptionText")
        If (String.IsNullOrEmpty(Strings.OfferDeleteConfirmMessage)) Then Strings.OfferDeleteConfirmMessage = pageData.GetCustomString("OfferDeleteConfirmMessage")
        If (String.IsNullOrEmpty(Strings.MilesAwayText)) Then Strings.MilesAwayText = pageData.GetCustomString("MilesAwayText")
        If (String.IsNullOrEmpty(Strings.CancelledRejectedDescriptionText)) Then Strings.CancelledRejectedDescriptionText = pageData.GetCustomString("YouCancelledWinkToText")
        If (String.IsNullOrEmpty(Strings.RejectBlockText)) Then Strings.RejectBlockText = pageData.GetCustomString("RejectBlockText")
        If (String.IsNullOrEmpty(Strings.RejectDeleteConversationText)) Then Strings.RejectDeleteConversationText = pageData.GetCustomString("RejectDeleteConversationText")
        If (String.IsNullOrEmpty(Strings.RejectDeleteConversationText)) Then Strings.RejectDeleteConversationText = pageData.GetCustomString("RejectDeleteConversationText")
        If (String.IsNullOrEmpty(Strings.IsOnlinetext)) Then Strings.IsOnlinetext = pageData.GetCustomString("Online.Now")
        If (String.IsNullOrEmpty(Strings.IsOnlineRecentlyText)) Then Strings.IsOnlineRecentlyText = pageData.GetCustomString("Online.Recently")

        If (String.IsNullOrEmpty(Strings.HasNoPhotosText)) Then Strings.HasNoPhotosText = pageData.GetCustomString("HasNoPhotosText")
        If (String.IsNullOrEmpty(Strings.SearchOurMembersText)) Then Strings.SearchOurMembersText = pageData.GetCustomString("SearchOurMembersText")
        If (String.IsNullOrEmpty(Strings.YouReceivedWinkOfferText)) Then Strings.YouReceivedWinkOfferText = pageData.GetCustomString("YouReceivedWinkText")
        If (String.IsNullOrEmpty(Strings.YearsOldText)) Then Strings.YearsOldText = pageData.GetCustomString("YearsOldText")
        If (String.IsNullOrEmpty(Strings.WantToKnowFirstDatePriceText)) Then Strings.WantToKnowFirstDatePriceText = pageData.GetCustomString("WantToKnowFirstDatePriceText")
        If (String.IsNullOrEmpty(Strings.MakeOfferText)) Then Strings.MakeOfferText = pageData.GetCustomString("MakeOfferText")
        If (String.IsNullOrEmpty(Strings.AcceptText)) Then Strings.AcceptText = pageData.GetCustomString("AcceptText")
        If (String.IsNullOrEmpty(Strings.CounterText)) Then Strings.CounterText = pageData.GetCustomString("CounterText")
        If (String.IsNullOrEmpty(Strings.NotInterestedText)) Then Strings.NotInterestedText = pageData.GetCustomString("NotInterestedText")
        If (String.IsNullOrEmpty(Strings.TooFarAwayText)) Then Strings.TooFarAwayText = pageData.GetCustomString("TooFarAwayText")
        If (String.IsNullOrEmpty(Strings.NotEnoughInfoText)) Then Strings.NotEnoughInfoText = pageData.GetCustomString("NotEnoughInfoText")
        If (String.IsNullOrEmpty(Strings.DifferentExpectationsText)) Then Strings.DifferentExpectationsText = pageData.GetCustomString("DifferentExpectationsText")
        If (String.IsNullOrEmpty(Strings.DeleteOfferText)) Then Strings.DeleteOfferText = pageData.GetCustomString("DeleteOfferText_ND")
        If (String.IsNullOrEmpty(Strings.DeleteWinkText)) Then Strings.DeleteWinkText = pageData.GetCustomString("DeleteWinkText")
        If (String.IsNullOrEmpty(Strings.SendMessageText)) Then Strings.SendMessageText = pageData.GetCustomString("SendMessageText_ND")
        '  If (String.IsNullOrEmpty(Strings.WinkText)) Then Strings.WinkText = pageData.GetCustomString("WinkText")
        '  If (String.IsNullOrEmpty(Strings.UnWinkText)) Then Strings.UnWinkText = pageData.GetCustomString("UnWinkText")
        '  If (String.IsNullOrEmpty(Strings.FavoriteText)) Then Strings.FavoriteText = pageData.GetCustomString("FavoriteText")
        '  If (String.IsNullOrEmpty(Strings.UnfavoriteText)) Then Strings.UnfavoriteText = pageData.GetCustomString("UnfavoriteText")
        '   If (String.IsNullOrEmpty(Strings.YearsOldFromText)) Then Strings.YearsOldFromText = pageData.GetCustomString("YearsOldFromText")
        If (String.IsNullOrEmpty(Strings.TryWinkText)) Then Strings.TryWinkText = pageData.GetCustomString("TryWinkText")
        If (String.IsNullOrEmpty(Strings.MakeNewOfferText)) Then Strings.MakeNewOfferText = pageData.GetCustomString("MakeNewOfferText")
        If (String.IsNullOrEmpty(Strings.YourWinkSentText)) Then Strings.YourWinkSentText = pageData.GetCustomString("YourWinkSentText")
        If (String.IsNullOrEmpty(Strings.AwaitingResponseText)) Then Strings.AwaitingResponseText = pageData.GetCustomString("AwaitingResponseText")
        If (String.IsNullOrEmpty(Strings.AddPhotosText)) Then Strings.AddPhotosText = pageData.GetCustomString("AddPhotosText")
        ' If (String.IsNullOrEmpty(Strings.UnblockText)) Then Strings.UnblockText = pageData.GetCustomString("UnblockText")
        If (String.IsNullOrEmpty(Strings.PokeText)) Then Strings.PokeText = pageData.GetCustomString("PokeText")
        If (String.IsNullOrEmpty(Strings.ActionsText)) Then Strings.ActionsText = pageData.GetCustomString("ActionsText")
        If (String.IsNullOrEmpty(Strings.CancelPokeText)) Then Strings.CancelPokeText = pageData.GetCustomString("CancelPokeText")
        If (String.IsNullOrEmpty(Strings.CancelWinkText)) Then Strings.CancelWinkText = pageData.GetCustomString("CancelWinkText_ND")
        If (String.IsNullOrEmpty(Strings.CancelOfferText)) Then Strings.CancelOfferText = pageData.GetCustomString("CancelOfferText_ND")
        If (String.IsNullOrEmpty(Strings.RejectText)) Then Strings.RejectText = pageData.GetCustomString("RejectText")
        If (String.IsNullOrEmpty(Strings.CancelledRejectedTitleText)) Then Strings.CancelledRejectedTitleText = pageData.GetCustomString("WinkCancelledText")
        If (String.IsNullOrEmpty(Strings.MessageSentNotice)) Then Strings.MessageSentNotice = pageData.GetCustomString("MessageSentNotice")
        '  If (String.IsNullOrEmpty(Strings.ConversationText)) Then Strings.ConversationText = pageData.GetCustomString("ConversationText")
        '  If (String.IsNullOrEmpty(Strings.HistoryText)) Then Strings.HistoryText = pageData.GetCustomString("HistoryText")
        '  If (String.IsNullOrEmpty(Strings.CommunicationStatusText)) Then Strings.CommunicationStatusText = pageData.GetCustomString("CommunicationStatusText")
        If (String.IsNullOrEmpty(Strings.WhatIsText)) Then Strings.WhatIsText = pageData.GetCustomString("WhatIsText")
        If (String.IsNullOrEmpty(Strings.OffersCommands_WhatIsPopup_HeaderText)) Then Strings.OffersCommands_WhatIsPopup_HeaderText = pageData.GetCustomString("OffersCommands_WhatIsPopup_HeaderText")
        If (String.IsNullOrEmpty(Strings.YourPokeSentText)) Then Strings.YourPokeSentText = pageData.GetCustomString("YourPokeSentText")
        If (String.IsNullOrEmpty(Strings.YouReceivedPokeText)) Then Strings.YouReceivedPokeText = pageData.GetCustomString("YouReceivedPokeText")
        If (String.IsNullOrEmpty(Strings.YourOfferSentText)) Then Strings.YourOfferSentText = pageData.GetCustomString("YourOfferSentText")
        If (String.IsNullOrEmpty(Strings.YouReceivedOfferText)) Then Strings.YouReceivedOfferText = pageData.GetCustomString("YouReceivedOfferText")
        If (String.IsNullOrEmpty(Strings.OtherMemberPokeAcceptedText)) Then Strings.OtherMemberPokeAcceptedText = pageData.GetCustomString("OtherMemberPokeAcceptedText")
        If (String.IsNullOrEmpty(Strings.CurrentMemberPokeAcceptedText)) Then Strings.CurrentMemberPokeAcceptedText = pageData.GetCustomString("CurrentMemberPokeAcceptedText")
        If (String.IsNullOrEmpty(Strings.OtherMemberOfferAcceptedWithAmountText)) Then Strings.OtherMemberOfferAcceptedWithAmountText = pageData.GetCustomString("OtherMemberOfferAcceptedWithAmountText")
        If (String.IsNullOrEmpty(Strings.CurrentMemberOfferAcceptedWithAmountText)) Then Strings.CurrentMemberOfferAcceptedWithAmountText = pageData.GetCustomString("CurrentMemberOfferAcceptedWithAmountText")
        If (String.IsNullOrEmpty(Strings.YouCancelledWinkToText)) Then Strings.YouCancelledWinkToText = pageData.GetCustomString("YouCancelledWinkToText")
        If (String.IsNullOrEmpty(Strings.WinkCancelledText)) Then Strings.WinkCancelledText = pageData.GetCustomString("WinkCancelledText")
        If (String.IsNullOrEmpty(Strings.OfferCancelledText)) Then Strings.OfferCancelledText = pageData.GetCustomString("OfferCancelledText")
        If (String.IsNullOrEmpty(Strings.YouCancelledOfferToText)) Then Strings.YouCancelledOfferToText = pageData.GetCustomString("YouCancelledOfferToText")
        If (String.IsNullOrEmpty(Strings.WinkREJECTEDText)) Then Strings.WinkREJECTEDText = pageData.GetCustomString("WinkREJECTEDText")
        If (String.IsNullOrEmpty(Strings.YourWinkREJECTEDReasonText)) Then Strings.YourWinkREJECTEDReasonText = pageData.GetCustomString("YourWinkREJECTEDReasonText")
        If (String.IsNullOrEmpty(Strings.PokeREJECTEDText)) Then Strings.PokeREJECTEDText = pageData.GetCustomString("PokeREJECTEDText")
        If (String.IsNullOrEmpty(Strings.YourPokeREJECTEDReasonText)) Then Strings.YourPokeREJECTEDReasonText = pageData.GetCustomString("YourPokeREJECTEDReasonText")
        If (String.IsNullOrEmpty(Strings.OfferREJECTEDText)) Then Strings.OfferREJECTEDText = pageData.GetCustomString("OfferREJECTEDText")
        If (String.IsNullOrEmpty(Strings.YourOfferREJECTEDReasonText)) Then Strings.YourOfferREJECTEDReasonText = pageData.GetCustomString("YourOfferREJECTEDReasonText")
        If (String.IsNullOrEmpty(Strings.FEMALE_WantToKnowFirstDatePriceText)) Then Strings.FEMALE_WantToKnowFirstDatePriceText = pageData.GetCustomString("FEMALE_WantToKnowFirstDatePriceText")
        If (String.IsNullOrEmpty(Strings.MALE_WantToKnowFirstDatePriceText)) Then Strings.MALE_WantToKnowFirstDatePriceText = pageData.GetCustomString("MALE_WantToKnowFirstDatePriceText")



    End Function
    Protected Function WriteOffer_MemberInfo(DataItem As Dating.Server.Site.Web.clsWinkUserListItem) As String
        Dim sb As New StringBuilder()
        Try
            sb.Append("<ul class=""info2"">")

            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberAge) Then
                sb.Append("<li class=""age1"">")
                sb.Append("<b>")
                sb.Append(DataItem.OtherMemberAge)
                sb.Append(" " & MykeyStrings.YearsOldText & " ")
                sb.Append("</b>")
                sb.Append("</li>")
            End If


            If (Not String.IsNullOrEmpty(DataItem.OtherMemberCity)) Then
                sb.Append("<li>")
                sb.Append(IIf(Not String.IsNullOrEmpty(DataItem.OtherMemberCity), DataItem.OtherMemberCity, ""))
                If (Not String.IsNullOrEmpty(DataItem.OtherMemberRegion)) Then sb.Append(",  ")
                sb.Append("</li>")
            End If

            sb.Append("<li>")
            sb.Append(IIf(Not String.IsNullOrEmpty(DataItem.OtherMemberRegion), DataItem.OtherMemberRegion, ""))
            sb.Append(",  ")
            sb.Append(IIf(Not String.IsNullOrEmpty(DataItem.OtherMemberCountry), ProfileHelper.GetCountryName(DataItem.OtherMemberCountry), ""))
            sb.Replace(",  " & ",  ", ",  ")
            sb.Append("</li>")


            sb.Append("</ul>")

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Return sb.ToString()
    End Function

    Protected Function WriteSearch_MemberInfo(DataItem As Dating.Server.Site.Web.clsWinkUserListItem) As Object
        Dim sb As New StringBuilder()
        Try

            sb.Append("<ul>")
            sb.Append("<li>")

            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberAge) Then
                sb.Append("<b>" & DataItem.OtherMemberAge & "</b>")
                sb.Append(" " & MykeyStrings.YearsOldText & " ")

                If (Not String.IsNullOrEmpty(DataItem.ZodiacString)) Then
                    sb.Append("(" & DataItem.OtherMemberBirthday.Value.ToString("d MMM, yyyy") & ")")
                End If
            End If

            sb.Append("<div class=""s_item_address"">")

            sb.Append(IIf(Not String.IsNullOrEmpty(DataItem.OtherMemberCity), DataItem.OtherMemberCity, ""))
            sb.Append(",  ")
            sb.Append(IIf(Not String.IsNullOrEmpty(DataItem.OtherMemberRegion), DataItem.OtherMemberRegion, ""))
            sb.Append(",  ")
            sb.Append(IIf(Not String.IsNullOrEmpty(DataItem.OtherMemberCountry), ProfileHelper.GetCountryName(DataItem.OtherMemberCountry), ""))
            sb.Replace(",  " & ",  ", ",  ")

            sb.Append("</div>")
            sb.Append("</li>")


            sb.Append("<li>")
            sb.Append("<b>")

            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberHeight) Then
                sb.Append(DataItem.OtherMemberHeight)
            End If

            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberHeight) AndAlso Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberPersonType) Then
                sb.Append(" - ")
            End If

            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberPersonType) Then
                sb.Append(DataItem.OtherMemberPersonType)
            End If

            sb.Append("</b>")
            sb.Append("</li>")


            sb.Append("<li>")

            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberHair) Then
                sb.Append(DataItem.OtherMemberHair)
            End If

            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberEyes) Then
                If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberHair) Then sb.Append(", ")
                sb.Append(DataItem.OtherMemberEyes)
            End If

            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberEthnicity) Then
                If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberEyes) OrElse Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberHair) Then sb.Append(", ")
                sb.Append(DataItem.OtherMemberEthnicity)
            End If

            sb.Append("</li>")
            sb.Append("</ul>")

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Return sb.ToString()
    End Function

    Protected Shared Function ShowIsOnline(DataItem As Dating.Server.Site.Web.clsWinkUserListItem) As Boolean
        If (DataItem.OtherMemberIsOnline OrElse DataItem.OtherMemberIsOnlineRecently) Then Return True
        Return False
    End Function



    Protected Sub rptPending_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptPending.ItemDataBound
        Try

            Dim lnkWhatIs As LinkButton = e.Item.FindControl("lnkWhatIs")
            If (lnkWhatIs IsNot Nothing) Then
                lnkWhatIs.ToolTip = Me.MykeyStrings.WhatIsText

                If (String.IsNullOrEmpty(InfoWinUrl)) Then InfoWinUrl = ResolveUrl("~/Members/InfoWin.aspx?info=offerscommands")
                If (String.IsNullOrEmpty(InfoWinHeaderText)) Then InfoWinHeaderText = Me.MykeyStrings.OffersCommands_WhatIsPopup_HeaderText

                lnkWhatIs.OnClientClick = WhatIsIt.OnMoreInfoClickFunc(lnkWhatIs.OnClientClick,
                                             InfoWinUrl,
                                             InfoWinHeaderText)
            End If

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub rptAccepted_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptAccepted.ItemDataBound
        Try

            Dim lnkWhatIs As LinkButton = e.Item.FindControl("lnkWhatIs")
            If (lnkWhatIs IsNot Nothing) Then
                lnkWhatIs.ToolTip = Me.MykeyStrings.WhatIsText

                If (String.IsNullOrEmpty(InfoWinUrl)) Then InfoWinUrl = ResolveUrl("~/Members/InfoWin.aspx?info=offerscommands")
                If (String.IsNullOrEmpty(InfoWinHeaderText)) Then InfoWinHeaderText = Me.MykeyStrings.OffersCommands_WhatIsPopup_HeaderText

                lnkWhatIs.OnClientClick = WhatIsIt.OnMoreInfoClickFunc(lnkWhatIs.OnClientClick,
                                             InfoWinUrl,
                                             InfoWinHeaderText)
            End If

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub rptRejected_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptRejected.ItemDataBound
        Try

            Dim lnkWhatIs As LinkButton = e.Item.FindControl("lnkWhatIs")
            If (lnkWhatIs IsNot Nothing) Then
                lnkWhatIs.ToolTip = Me.MykeyStrings.WhatIsText

                If (String.IsNullOrEmpty(InfoWinUrl)) Then InfoWinUrl = ResolveUrl("~/Members/InfoWin.aspx?info=offerscommands")
                If (String.IsNullOrEmpty(InfoWinHeaderText)) Then InfoWinHeaderText = Me.MykeyStrings.OffersCommands_WhatIsPopup_HeaderText

                lnkWhatIs.OnClientClick = WhatIsIt.OnMoreInfoClickFunc(lnkWhatIs.OnClientClick,
                                             InfoWinUrl,
                                             InfoWinHeaderText)
            End If

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub rptNew_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptNew.ItemDataBound
        Try

            Dim lnkWhatIs As LinkButton = e.Item.FindControl("lnkWhatIs")
            If (lnkWhatIs IsNot Nothing) Then
                lnkWhatIs.ToolTip = Me.MykeyStrings.WhatIsText

                If (String.IsNullOrEmpty(InfoWinUrl)) Then InfoWinUrl = ResolveUrl("~/Members/InfoWin.aspx?info=offerscommands")
                If (String.IsNullOrEmpty(InfoWinHeaderText)) Then InfoWinHeaderText = Me.MykeyStrings.OffersCommands_WhatIsPopup_HeaderText

                lnkWhatIs.OnClientClick = WhatIsIt.OnMoreInfoClickFunc(lnkWhatIs.OnClientClick,
                                             InfoWinUrl,
                                             InfoWinHeaderText)
            End If

        Catch ex As Exception

        End Try
    End Sub
End Class
﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UCSelectProduct.ascx.vb"
    Inherits="Dating.Server.Site.Web.UCSelectProduct" %>

<%@ Register Src="UCPrice2.ascx" TagName="UCPrice" TagPrefix="uc1" %>
<style type="text/css">
    .vat-info { font-size: 12px; font-weight: bold; margin: 17px 0 0; border-bottom: 1px solid #bababc; padding-bottom: 3px; width: 1004px; }
</style>
<link rel="stylesheet" href="/v1/css/payments2.css?v=20" />
<style type="text/css">
    #payments_pub #products_2 { background:none;}
    #payments_pub #products_2 .center-table img {margin:0 9px;}
    #payments_pub #products_2 .center-table td:first-child img {margin:0 9px 0 0;}
    #payments_pub #products_2 .center-table td:last-child img {margin:0 0 0 9px;}
</style>
<div id="payments_pub" runat="server" clientidmode="Static">
    <div>
        <asp:Label ID="lblPaymentsGuarantee" runat="server" Text=""></asp:Label>
    </div>
    <div class="center" style="text-align: center; display: none;">
        <asp:Image ID="Image1" runat="server" ImageUrl="//cdn.goomena.com/Images/products/100.png" />
        <asp:Image ID="Image2" runat="server" ImageUrl="//cdn.goomena.com/Images/products/alert-pay.png" />
        <asp:Image ID="Image3" runat="server" ImageUrl="//cdn.goomena.com/Images/products/paypal.png" />
        <asp:Image ID="Image4" runat="server" ImageUrl="//cdn.goomena.com/Images/products/paypal-verified.png" />
        <asp:Image ID="Image5" runat="server" ImageUrl="//cdn.goomena.com/Images/products/paysafe.png" />
    </div>
    <div>
        <dx:ASPxLabel ID="PanelSelectProduct" runat="server" Text="" EncodeHtml="False">
        </dx:ASPxLabel>
    </div>
    <div id="products_2">
        <uc1:UCPrice ID="UCPrice1" runat="server" View="vwMember" CssClass="product-1" />
        <uc1:UCPrice ID="UCPrice2" runat="server" View="vwMember" CssClass="product-2" Discount="30" />
        <uc1:UCPrice ID="UCPrice4" runat="server" View="vwMember" CssClass="product-4" Visible="false" />
        <uc1:UCPrice ID="UCPrice3" runat="server" View="vwMember" CssClass="product-3" IsRecommended="true" Discount="45" />
        <uc1:UCPrice ID="UCPrice5" runat="server" View="vwMember" CssClass="product-5" />
        <div style="position:absolute;bottom:-3px;left:0px;right:0px;height:46px;">
        <table class="center-table" cellpadding="0" cellspacing="0">
            <tr>
                <td class="img-cell"><asp:Image ID="imgPaypalLogo" runat="server" ImageUrl="//cdn.goomena.com/Images2/payment/paypal-logo.png" /></td>
                <td class="img-cell"><asp:Image ID="imgVisaLogo" runat="server" ImageUrl="//cdn.goomena.com/Images2/payment/visa-logo.png" /></td>
                <td class="img-cell"><asp:Image ID="imgMastercardLogo" runat="server" ImageUrl="//cdn.goomena.com/Images2/payment/mastercard-logo.png" /></td>
                <td class="img-cell"><asp:Image ID="imgPaysafeLogo" runat="server" ImageUrl="//cdn.goomena.com/Images2/payment/paysafe-card.png" /></td>
                <td class="img-cell"><asp:Image ID="imgSMSLogo" runat="server"  Visible="false" /></td>
                <td class="img-cell"><asp:Image ID="imgAmericanLogo" runat="server" ImageUrl="//cdn.goomena.com/Images2/payment/american-express.png" /></td>
                <td class="img-cell"><asp:Image ID="imgBitPay" runat="server" ImageUrl="//cdn.goomena.com/Images2/payment/bitcoin-card.png" /></td>
            </tr>
        </table>
        </div>
    </div>
    <div>
        <asp:Label ID="lblVatInfo" runat="server" Text="">
        <div class="vat-info">* All prices are inclusive VAT for our EU customers.</div>
        </asp:Label>
    </div>
    <div>
        <dx:ASPxLabel runat="server" EncodeHtml="False" ClientIDMode="AutoID" ID="lbfootText">
        </dx:ASPxLabel>
    </div>
</div>

<script type="text/javascript">
    $(function () {
        $('.product-1.product-tab', '#products_2').click(function () { $('.pricecol-order-btn', '#products_2 .product-1')[0].click(); })
        $('.product-2.product-tab', '#products_2').click(function () { $('.pricecol-order-btn', '#products_2 .product-2')[0].click(); })
        $('.product-3.product-tab', '#products_2').click(function () { $('.pricecol-order-btn', '#products_2 .product-3')[0].click(); })
        $('.product-4.product-tab', '#products_2').click(function () { $('.pricecol-order-btn', '#products_2 .product-4')[0].click(); })
        $('.product-5.product-tab', '#products_2').click(function () { $('.pricecol-order-btn', '#products_2 .product-5')[0].click(); })
    })
</script>



﻿Imports DevExpress.Web.ASPxPager
Imports Dating.Server.Core.DLL
Imports Dating.Server.Site.Web.OfferControl3
Imports DevExpress.Web.ASPxEditors
Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Datasets.DLL.DSMembers

Public Class NewMessageControl
    Inherits BaseUserControl


#Region "Props"

    Public Property ScrollToElem As String
    Public Property WarningPopup_HeaderText As String

    Protected Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            'If (_pageData Is Nothing) Then _pageData = New clsPageData("~/Members/Messages.aspx", Context)
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData("~/Members/Messages.aspx", Context, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property


    Public Property OfferId As Integer
        Get
            Return ViewState("OfferId")
        End Get
        Set(value As Integer)
            ViewState("OfferId") = value
        End Set
    End Property


    Public Property UserIdInView As Integer
        Get
            Return ViewState("UserIdInView")
        End Get
        Set(value As Integer)
            ViewState("UserIdInView") = value
        End Set
    End Property


    Public Property SubjectInView As String
        Get
            Return ViewState("SubjectInView")
        End Get
        Set(value As String)
            ViewState("SubjectInView") = value
        End Set
    End Property


    Public Property MessageIdInView As Long
        Get
            Return ViewState("MessageIdInView")
        End Get
        Set(value As Long)
            ViewState("MessageIdInView") = value
        End Set
    End Property

    ' used with delete message action
    Public Property MessageIdListInView As List(Of Long)
        Get
            Return ViewState("MessageIdListInView")
        End Get
        Set(value As List(Of Long))
            ViewState("MessageIdListInView") = value
        End Set
    End Property


    Public Property BackUrl As String
        Get
            Return ViewState("BackUrl")
        End Get
        Set(value As String)
            ViewState("BackUrl") = value
        End Set
    End Property


    Public Property SendAnotherMessage As Boolean
        Get
            Return ViewState("SendAnotherMessage")
        End Get
        Set(value As Boolean)
            ViewState("SendAnotherMessage") = value
        End Set
    End Property

    ''' <summary>
    ''' Recipient id.
    ''' Used when current user opens it's sent message and wants to send another one.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property SendAnotherMessageToRecipient As Integer
        Get
            Return ViewState("SendAnotherMessageToRecipient")
        End Get
        Set(value As Integer)
            ViewState("SendAnotherMessageToRecipient") = value
        End Set
    End Property

#End Region



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        pnlSendMessageErr.Visible = False
        divDatingAmount.Visible = False

        Try


            '    Dim viewLoaded As Boolean = False

            '     Dim unlockMessage As Boolean = MyBase.UnlimitedMsgID > 0 AndAlso Not String.IsNullOrEmpty(Request.QueryString("vw"))
            '    Dim viewMessage As Boolean = Not MyBase.UnlimitedMsgID > 0 AndAlso (Not String.IsNullOrEmpty(Request.QueryString("vw")) OrElse Not String.IsNullOrEmpty(Request.QueryString("msg")))
            '     Dim sendMessageTo As Boolean = Not String.IsNullOrEmpty(Request.QueryString("p"))
            '   Dim tabChanged As Boolean = Not String.IsNullOrEmpty(Request.QueryString("t"))

            If (Not Page.IsPostBack) Then
                Me.BackUrl = Request.Params("HTTP_REFERER")
                If (Not String.IsNullOrEmpty(Request.Params("offerid"))) Then Integer.TryParse(Request.Params("offerid"), Me.OfferId)


                'If (viewMessage) Then
                '    '' load conversation for requested and current profiles
                '    'LoadViews()

                '    'If (Not String.IsNullOrEmpty(Request.QueryString("vw"))) Then
                '    '    Dim otherLoginName = Request.QueryString("vw")

                '    '    Dim memberReceivingMessage As EUS_Profile = DataHelpers.GetEUS_ProfileMasterByLoginName(Me.CMSDBDataContext, otherLoginName, ProfileStatusEnum.Approved, ProfileStatusEnum.LIMITED)
                '    '    If (memberReceivingMessage IsNot Nothing) Then

                '    '        Dim HasCommunication As Boolean = GetHasCommunication(memberReceivingMessage.ProfileID)
                '    '        Dim message As EUS_Message = clsUserDoes.GetLastMessageForProfiles(Me.MasterProfileId, memberReceivingMessage.ProfileID)
                '    '        If (message IsNot Nothing) Then
                '    '            Me.UserIdInView = memberReceivingMessage.ProfileID
                '    '            Me.MessageIdInView = message.EUS_MessageID
                '    '        Else
                '    '            ' there are no messages among users
                '    '            Me.UserIdInView = memberReceivingMessage.ProfileID
                '    '            Me.MessageIdInView = 0
                '    '        End If

                '    '        ExecCmd("VIEWMSG", Me.MessageIdInView)
                '    '        viewLoaded = True

                '    '    End If
                '    'End If



                '    'Dim messageID As Integer
                '    'If (Not String.IsNullOrEmpty(Request.QueryString("msg")) AndAlso Integer.TryParse(Request.QueryString("msg"), messageID)) Then
                '    '    If (messageID > 0) Then
                '    '        Dim message As EUS_Message = clsUserDoes.GetMessage(messageID)

                '    '        If (message Is Nothing OrElse message.ProfileIDOwner <> Me.MasterProfileId) Then
                '    '            ' message not found or message is owned by another user

                '    '            viewLoaded = False

                '    '        ElseIf (message.ProfileIDOwner = Me.MasterProfileId) Then
                '    '            If (message.FromProfileID = Me.MasterProfileId) Then
                '    '                Me.UserIdInView = message.ToProfileID
                '    '            Else
                '    '                Me.UserIdInView = message.FromProfileID
                '    '            End If
                '    '            Me.MessageIdInView = message.EUS_MessageID
                '    '            ExecCmd("VIEWMSG", Me.MessageIdInView)
                '    '            viewLoaded = True

                '    '        End If
                '    '    End If
                '    'End If

                'End If



                ''If (Not viewLoaded AndAlso sendMessageTo) Then

                ''    '' send message to profile
                ''    LoadLAG()
                ''    'LoadViews()
                ''    LoadNewMessageView()
                ''    viewLoaded = True

                ''End If

                ''If (Not viewLoaded AndAlso unlockMessage) Then
                ''    LoadLAG()
                ''    PerformMessageUnlock()
                ''    viewLoaded = True
                ''End If

                ''If (Not viewLoaded) Then

                ''End If
            End If

            ''If (Page.IsPostBack) Then
            'If (Not viewLoaded AndAlso unlockMessage) Then
            '    PerformMessageUnlock()
            'End If
            ''End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub




    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender


        Try
            If (SiteRulesTIP.Visible = True) Then
                Dim SiteRulesTIP_click As String = WhatIsIt.OnMoreInfoClickFunc(ResolveUrl("~/Members/InfoWin.aspx?info=Conversation_Site_Rules&popup=popupWhatIs"), "More info", 601, 350, Nothing)
                SiteRulesTIP.Attributes.Add("onclick", SiteRulesTIP_click)
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load-->SiteRulesTIP")
        End Try
    End Sub

    Public Sub LoadNewMessageView(otherLoginName As String, ByRef memberReceivingMessage As EUS_Profile, SendOnce As Boolean, SendUnlimited As Boolean, MaySendMessage_CheckSetting As Boolean, femaleSend As Boolean, maleSend As Boolean)

        Try
            LoadLAG()

            'mvMessages.SetActiveView(vwCreateMessage)

            ' Dim MaySendMessage_CheckSetting As Boolean = True
            'Dim otherLoginName = Request.QueryString("p")

            'If (String.IsNullOrEmpty(otherLoginName)) Then otherLoginName = Request.QueryString("vw")

            'If (String.IsNullOrEmpty(otherLoginName)) Then
            '    Return
            'End If

            'If (otherLoginName.ToUpper() = Me.GetCurrentProfile().LoginName.ToUpper()) Then
            '    divActionError.Visible = True
            '    lnkAlternativeAction.Text = CurrentPageData.GetCustomString("lnkAlternativeAction_Back")
            '    If (Not String.IsNullOrEmpty(Me.BackUrl)) Then
            '        lnkAlternativeAction.NavigateUrl = Me.BackUrl
            '    Else
            '        lnkAlternativeAction.NavigateUrl = "~/Members/"
            '    End If
            '    lblActionError.Text = CurrentPageData.GetCustomString("ErrorSendMessageToMySelf")
            '    mvMessages.SetActiveView(vwEmpty)

            '    Return
            'End If

            'Dim memberReceivingMessage As EUS_Profile = DataHelpers.GetEUS_ProfileMasterByLoginName(Me.CMSDBDataContext, otherLoginName, ProfileStatusEnum.Approved, ProfileStatusEnum.LIMITED)
            'If (memberReceivingMessage Is Nothing) Then
            '    'divActionError.Visible = True
            '    'lnkAlternativeAction.Text = CurrentPageData.GetCustomString("lnkAlternativeAction_Search")
            '    'lnkAlternativeAction.NavigateUrl = "~/Members/Search.aspx"
            '    'lblActionError.Text = CurrentPageData.GetCustomString("ErrorMemberDoesNotExistCannotSendMessage")
            '    'mvMessages.SetActiveView(vwEmpty)

            '    Return
            'End If

            'Me.UserIdInView = memberReceivingMessage.ProfileID


            ''Dim MaySendMessage As Boolean = False
            'Dim femaleSend As Boolean = Me.IsFemale
            ''Dim femaleSend As Boolean = MaySendMessage_CheckSetting
            ''If (femaleSend) Then
            ''    femaleSend = Me.IsFemale
            ''End If
            'Dim femaleReceiving As Boolean = ProfileHelper.IsFemale(memberReceivingMessage.GenderId)

            'If (femaleSend AndAlso femaleReceiving) Then
            '    divActionError.Visible = True
            '    lnkAlternativeAction.Text = CurrentPageData.GetCustomString("lnkAlternativeAction_Search")
            '    lnkAlternativeAction.NavigateUrl = "~/Members/Search.aspx"
            '    lblActionError.Text = CurrentPageData.GetCustomString("Error.FemaleMemberCannotSendToFemale")
            '    mvMessages.SetActiveView(vwEmpty)

            '    Return
            'End If

            '  Dim AlreadyUnlockedUnlimited As Boolean = GetHasCommunication(Me.UserIdInView)
            ''Dim AlreadyUnlockedUnlimited As Boolean = ModGlobals.AllowUnlimited AndAlso GetHasCommunication(Me.UserIdInView)
            ''Dim AlreadyUnlockedUnlimited As Boolean = clsUserDoes.HasCommunication(Me.MasterProfileId, Me.UserIdInView)

            'Dim SendOnce As Boolean = (Request.QueryString("send") = "once")
            'Dim SendUnlimited As Boolean = (Request.QueryString("send") = "unl") 'AndAlso ModGlobals.AllowUnlimited

            ''Dim maleSend As Boolean = MaySendMessage_CheckSetting
            ''If (maleSend) Then
            ''    maleSend = ((SendOnce OrElse SendUnlimited) AndAlso Me.IsMale) OrElse (AlreadyUnlockedUnlimited AndAlso Me.IsMale)
            ''End If
            'Dim maleSend As Boolean = ((SendOnce OrElse SendUnlimited) AndAlso Me.IsMale) OrElse (AlreadyUnlockedUnlimited AndAlso Me.IsMale)
            'If (maleSend AndAlso Not AlreadyUnlockedUnlimited) Then

            '    'If (Not AlreadyUnlockedUnlimited) Then
            '    If (Not SendOnce AndAlso Not SendUnlimited) Then
            '        lblSendMessageErr.Text = Me.CurrentPageData.GetCustomString("ErrMayNotSendMessage")
            '        pnlSendMessageErr.Visible = True
            '        maleSend = False
            '    End If


            '    If (SendOnce) Then
            '        Dim hasCreditsToSendMessage As Boolean = HasRequiredCredits(UnlockType.UNLOCK_MESSAGE_SEND)
            '        'Dim hasCreditsToSendMessage As Boolean = clsUserDoes.HasRequiredCredits(Me.MasterProfileId, UnlockType.UNLOCK_MESSAGE_SEND)
            '        If Not maleSend AndAlso hasCreditsToSendMessage Then maleSend = True

            '        ' user has no credits
            '        If Not hasCreditsToSendMessage Then maleSend = False

            '    ElseIf (SendUnlimited) Then
            '        Dim hasCreditsToSendMessageUnl As Boolean = HasRequiredCredits(UnlockType.UNLOCK_CONVERSATION)
            '        'Dim hasCreditsToSendMessageUnl As Boolean = clsUserDoes.HasRequiredCredits(Me.MasterProfileId, UnlockType.UNLOCK_CONVERSATION)
            '        If Not maleSend AndAlso hasCreditsToSendMessageUnl Then maleSend = True

            '        ' user has no credits
            '        If Not hasCreditsToSendMessageUnl Then maleSend = False

            '    End If


            '    pnlSendMessageErr.Visible = True
            '    lnkSendMessageAltAction.Visible = False

            '    If (SendOnce) Then
            '        lblSendMessageErr.Text = CurrentPageData.GetCustomString("WarnSendMessageOnce")
            '        SiteRulesTIP.Visible = True
            '    ElseIf (SendUnlimited) Then
            '        lblSendMessageErr.Text = CurrentPageData.GetCustomString("WarnSendMessageUnlimited")
            '    End If
            '    'End If

            'End If

            ''MaySendMessage_CheckSetting = Not clsProfilesPrivacySettings.GET_DisableReceivingMESSAGESFromDifferentCountryFromTo(memberReceivingMessage.ProfileID, Me.MasterProfileId)
            'If (MaySendMessage_CheckSetting = False) Then
            '    'divActionError.Visible = True
            '    'lnkAlternativeAction.Text = CurrentPageData.GetCustomString("lnkAlternativeAction_Search")
            '    'lnkAlternativeAction.NavigateUrl = "~/Members/Search.aspx"
            '    'lblActionError.Text = CurrentPageData.GetCustomString("DisableReceivingMessagesFromDifferentCountry.Text")
            '    Dim popupMessage As String = WhatIsIt.OnMoreInfoClickFunc(ResolveUrl("~/Members/InfoWin.aspx?info=Error_Sending_Receiver_Disabled_Messages_From_Diff_Country&popup=popupWhatIs"), "Error! Can't send", 650, 350)
            '    popupMessage = "function fnError_Sending_Receiver_DoNot_Have_Photo(){" & popupMessage & "};"
            '    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Error_Sending_Receiver_DoNot_Have_Photo", popupMessage, True)


            '    pnlSendMessageErr.Visible = True
            '    lnkSendMessageAltAction.Visible = True
            '    lnkSendMessageAltAction.Text = CurrentPageData.GetCustomString("DisableReceivingMessagesFromDifferentCountry.Alternative.Action")
            '    lnkSendMessageAltAction.NavigateUrl = CurrentPageData.GetCustomString("DisableReceivingMessagesFromDifferentCountry.Alternative.Action.URL")
            '    lblSendMessageErr.Text = CurrentPageData.GetCustomString("DisableReceivingMessagesFromDifferentCountry.Text")

            '    'mvMessages.SetActiveView(vwEmpty)

            '    'Return
            'End If

            If (Not MaySendMessage_CheckSetting) Then
                FemaleSend = False
                MaleSend = False
            End If

            If (FemaleSend OrElse MaleSend) Then
                txtMessageText.Enabled = True
                btnSendMessage.Enabled = True
                txtMessageSubject.Enabled = True

            Else
                txtMessageText.Enabled = False
                btnSendMessage.Enabled = False
                txtMessageSubject.Enabled = False


                If (MaySendMessage_CheckSetting) Then

                    pnlSendMessageErr.Visible = True
                    lnkSendMessageAltAction.Visible = True


                    If (Me.IsMale) Then
                        lnkSendMessageAltAction.Text = CurrentPageData.GetCustomString("lnkErrSendMessageCreditsNotAvailable")
                        lblSendMessageErr.Text = CurrentPageData.GetCustomString("ErrSendMessageOnceCreditsNotAvailable")

                        If (SendOnce) Then
                            lnkSendMessageAltAction.Text = CurrentPageData.GetCustomString("lnkErrSendMessageCreditsNotAvailable")
                            lblSendMessageErr.Text = CurrentPageData.GetCustomString("ErrSendMessageOnceCreditsNotAvailable")

                        ElseIf (SendUnlimited) Then
                            lnkSendMessageAltAction.Text = CurrentPageData.GetCustomString("lnkErrSendMessageCreditsNotAvailable")
                            lblSendMessageErr.Text = CurrentPageData.GetCustomString("ErrSendMessageUnlimitedCreditsNotAvailable")

                        End If
                    End If

                End If 'MaySendMessage_CheckSetting

            End If


            lnkSendMessageAltAction.Text = BasePage.ReplaceCommonTookens(lnkSendMessageAltAction.Text, memberReceivingMessage.LoginName)
            lblSendMessageErr.Text = BasePage.ReplaceCommonTookens(lblSendMessageErr.Text, memberReceivingMessage.LoginName)



            Try

                If (ProfileHelper.IsFemale(memberReceivingMessage.GenderId)) Then
                    lblSendMessageToTitle.Text = CurrentPageData.GetCustomString("lblSendMessageToTitle_SendToFEMALE")
                    lblYourMsgTo.Text = CurrentPageData.GetCustomString("lblYourMsgTo_SendToFEMALE")
                Else
                    lblSendMessageToTitle.Text = CurrentPageData.GetCustomString("lblSendMessageToTitle_SendToMALE")
                    lblYourMsgTo.Text = CurrentPageData.GetCustomString("lblYourMsgTo_SendToMALE")
                End If
                lblSendMessageToTitle.Text = BasePage.ReplaceCommonTookens(lblSendMessageToTitle.Text, memberReceivingMessage.LoginName)
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "LoadNewMessageView #1")
            End Try


            Try
                Dim defaultPhoto As DSMembers.EUS_CustomerPhotosRow =
                    DataHelpers.GetProfilesDefaultPhoto(Me.MasterProfileId)

                Dim memberReceivingMessagePhoto As DSMembers.EUS_CustomerPhotosRow =
                    DataHelpers.GetProfilesDefaultPhoto(memberReceivingMessage.ProfileID)

                Dim result As clsUserDoes.clsLoadProfilesViewsResult =
                    clsUserDoes.GetProfilesThumbViews(Me.GetCurrentProfile(), defaultPhoto, memberReceivingMessage, memberReceivingMessagePhoto, Me.IsHTTPS, PhotoSize.D150)

                lnkProfImg.NavigateUrl = result.CurrentMemberProfileViewUrl
                imgProf.ImageUrl = result.CurrentMemberImageUrl
                lnkProfLogin.NavigateUrl = result.CurrentMemberProfileViewUrl
                lnkProfLogin.Text = result.CurrentMemberLoginName

                lnkOtherProfImg.NavigateUrl = result.OtherMemberProfileViewUrl
                imgOtherProf.ImageUrl = result.OtherMemberImageUrl
                lnkOtherProfLogin.NavigateUrl = result.OtherMemberProfileViewUrl
                lnkOtherProfLogin.Text = result.OtherMemberLoginName

                If (Len(Request.QueryString("text")) > 0 AndAlso Request.QueryString("text").ToLower() = "privatephoto") Then
                    txtMessageSubject.Text = CurrentPageData.GetCustomString("Private.Photo.Message.Subject")
                    txtMessageText.Text = CurrentPageData.GetCustomString("Private.Photo.Message.Body")
                    txtMessageText.Text = txtMessageText.Text.Replace("[LEVEL]", Request.QueryString("lvl"))
                End If
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "GetProfilesThumbViews")
            End Try




            Dim unlockedConv As EUS_UnlockedConversation = clsUserDoes.GetUnlockedConversation(memberReceivingMessage.ProfileID, Me.MasterProfileId)
            Dim acceptedOffer As EUS_Offer = clsUserDoes.GetAcceptedOrUnlockedOffer(memberReceivingMessage.ProfileID, Me.MasterProfileId)

            If (acceptedOffer IsNot Nothing OrElse unlockedConv IsNot Nothing) Then

                If (acceptedOffer IsNot Nothing AndAlso acceptedOffer.Amount > 0) Then
                    divDatingAmount.Visible = True
                    lblDatingAmount.Text = "&euro;" & acceptedOffer.Amount
                ElseIf (unlockedConv IsNot Nothing AndAlso unlockedConv.CurrentOfferAmount > 0) Then
                    divDatingAmount.Visible = True
                    lblDatingAmount.Text = "&euro;" & unlockedConv.CurrentOfferAmount
                End If
            End If




        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "LoadNewMessageView")
        End Try

    End Sub



    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        'MessagesView = MessagesViewEnum.INBOX
        LoadLAG()
    End Sub


    Protected Sub LoadLAG()
        Try

            ''lnkNew.Text = "<span></span>" & CurrentPageData.GetCustomString("lnkNew")
            ''lnkSent.Text = "<span></span>" & CurrentPageData.GetCustomString("lnkSent")
            ''lnkInbox.Text = "<span></span>" & CurrentPageData.GetCustomString("lnkInbox")
            ''lnkTrash.Text = "<span></span>" & CurrentPageData.GetCustomString("lnkTrash")
            ' ''lnkConversation.Text = "<span></span>" & CurrentPageData.GetCustomString("lnkConversation")

            ''lblInfoMessage.Text = CurrentPageData.GetCustomString("lblInfoMessage")
            ''lblSelectFilter.Text = CurrentPageData.GetCustomString("lblSelectFilter")

            ''cbFilter.Items.Clear()
            ''cbFilter.Items.Add("---", "0")
            ''cbFilter.Items.Add(CurrentPageData.GetCustomString("cbFilter_Unread"), "Unread")
            ''cbFilter.Items.Add(CurrentPageData.GetCustomString("cbFilter_Read"), "Read")

            ''btnDeleteSeleted.Text = CurrentPageData.GetCustomString("btnDeleteSeleted")


            ''lblReplyTitle.Text = CurrentPageData.GetCustomString("lblReplyTitle")
            ''btnReplyMessage.Text = CurrentPageData.GetCustomString("btnReplyMessage")

            'lblConversationReplyTitle.Text = CurrentPageData.GetCustomString("lblReplyTitle")
            'btnConversationReply.Text = CurrentPageData.GetCustomString("btnReplyMessage")

            '' new message section
            ''lblSendMessageToTitle.Text = CurrentPageData.GetCustomString("lblSendMessageToTitle")
            lnkBack.Text = CurrentPageData.GetCustomString("lnkBack")
            lblYourMsgSubject.Text = CurrentPageData.GetCustomString("lblYourMsgSubject")
            lblYourMsgBody.Text = CurrentPageData.GetCustomString("lblYourMsgBody")
            lblYourFirstDateStartedTitle.Text = CurrentPageData.GetCustomString("lblYourFirstDateStartedTitle")
            lblYourFirstDateStartedText.Text = CurrentPageData.GetCustomString("lblYourFirstDateStartedText")
            lblYourMsgFrom.Text = CurrentPageData.GetCustomString("lblYourMsgFrom")
            lblYourMsgTo.Text = CurrentPageData.GetCustomString("lblYourMsgTo")
            btnSendMessage.Text = CurrentPageData.GetCustomString("btnSendMessage")

            '' no messages
            'msg_SearchOurMembersText.Text = CurrentPageData.GetCustomString("msg_SearchOurMembersText")
            'lblNoMessagesText.Text = CurrentPageData.GetCustomString("lblNoConversationText") 'lblNoMessagesText

            'lblProfileDeletedInfo.Text = CurrentPageData.GetCustomString("lblProfileDeletedInfo")
            ''lblSendAnotherMessageInfo.Text = CurrentPageData.GetCustomString("lblSendAnotherMessageInfo")
            'btnSendAnotherMessage.Text = CurrentPageData.GetCustomString("btnSendAnotherMessage")

            'btnSendMessageOnce.Text = ReplaceCommonTookens(CurrentPageData.GetCustomString("btnSendMessageOnce"))
            'btnSendMessagesUnl.Text = ReplaceCommonTookens(CurrentPageData.GetCustomString("btnSendMessagesUnl"))


            ''imgItems.ImageUrl = "~/images/members_bar/7.png"
            'lblItemsName.Text = Me.CurrentPageData.GetCustomString("CartConversationView")
            'lnkViewDescription.Text = Me.CurrentPageData.GetCustomString("CartConversationWhatIsIt")
            'WhatIsIt1.PopupControl.HeaderText = Me.CurrentPageData.GetCustomString("CartConversationView")
            'lnkViewDescription.OnClientClick = OnMoreInfoClickFunc(
            '    lnkViewDescription.OnClientClick,
            '    ResolveUrl("~/Members/InfoWin.aspx?info=conversation"),
            '    Me.CurrentPageData.GetCustomString("CartConversationView"))

            WarningPopup_HeaderText = Me.CurrentPageData.GetCustomString("SuppressWarning_WhenReadingMessageFromDifferentCountry.Warning.HeaderText")
            WarningPopup_HeaderText = AppUtils.JS_PrepareString(WarningPopup_HeaderText)

            'btnSave.Text = btnSave.Text.Replace("[CONTENT]", Me.CurrentPageData.GetCustomString("SuppressWarning_WhenReadingMessageFromDifferentCountry.Warning.Save.Option"))
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub



    Protected Sub btnSendMessage_Click(sender As Object, e As EventArgs) Handles btnSendMessage.Click
        Try
            SendMessageToUser(txtMessageSubject.Text, txtMessageText.Text, Me.UserIdInView)
            txtMessageSubject.Text = ""
            txtMessageText.Text = ""

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Private Sub SendMessageToUser(Subject As String, Body As String, recipientUserId As Integer)
        Try
            Dim MaySendMessage As Boolean = True
            '   Dim MaySendFreeMessage As Boolean = False
            Dim ReferrerMaySendMessage As ReferrerStopSendingMessageEnum = ReferrerStopSendingMessageEnum.None
            Dim AlreadyUnlockedUnlimited As Boolean = GetHasCommunication(recipientUserId)

            Dim SendOnce As Boolean = (Request.QueryString("send") = "once")
            Dim SendUnlimited As Boolean = (Request.QueryString("send") = "unl")


            If (Me.IsMale) Then

                MaySendMessage = AlreadyUnlockedUnlimited
                If (Not AlreadyUnlockedUnlimited) Then
                    If (Not SendOnce AndAlso Not SendUnlimited) Then
                        lblSendMessageErr.Text = Me.CurrentPageData.GetCustomString("ErrMayNotSendMessage")
                        pnlSendMessageErr.Visible = True
                        MaySendMessage = False
                    End If

                    If (SendOnce OrElse SendUnlimited) Then
                        MaySendMessage = True
                    End If

                End If

            ElseIf (Me.IsFemale) Then
                If (Me.IsReferrer) Then


                    ReferrerMaySendMessage = clsUserDoes.IsReferrerAllowedSendMessage(recipientUserId, Me.MasterProfileId)
                    MaySendMessage = (ReferrerMaySendMessage = ReferrerStopSendingMessageEnum.None)

                Else
                    MaySendMessage = True
                End If
            End If

            If (ReferrerMaySendMessage <> ReferrerStopSendingMessageEnum.None) Then
                'lblSubjectErr.Text = Me.CurrentPageData.GetCustomString("Error.Referrer.MayNot.Send.Message")
                'pnlSubjectErr.Visible = True

                If (ReferrerMaySendMessage = ReferrerStopSendingMessageEnum.RecipientHasNoPhoto) Then

                    Dim popupMessage As String = WhatIsIt.OnMoreInfoClickFunc(ResolveUrl("~/Members/InfoWin.aspx?info=Error_Sending_Receiver_DoNot_Have_Photo&popup=popupWhatIs"), "Error! Can't send", 650, 350)
                    popupMessage = "function fnError_Sending_Receiver_DoNot_Have_Photo(){" & popupMessage & "};"
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Error_Sending_Receiver_DoNot_Have_Photo", popupMessage, True)

                End If
                If (ReferrerMaySendMessage = ReferrerStopSendingMessageEnum.MaxAllowedMessagesSent) Then

                    Dim popupMessage As String = WhatIsIt.OnMoreInfoClickFunc(ResolveUrl("~/Members/InfoWin.aspx?info=Error_Sending_MaxAllowedMessagesSent&popup=popupWhatIs"), "Error! Can't send", 650, 350)
                    popupMessage = "function fnError_Sending_Receiver_DoNot_Have_Photo(){" & popupMessage & "};"
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Error_Sending_Receiver_DoNot_Have_Photo", popupMessage, True)

                End If

                MaySendMessage = False
            End If

            If (Not (Not String.IsNullOrEmpty(Body) OrElse Not String.IsNullOrEmpty(Subject))) Then
                lblSubjectErr.Text = Me.CurrentPageData.GetCustomString("ErrorSubjectRequired")
                pnlSubjectErr.Visible = True

                MaySendMessage = False
            End If

            'If (maleSend = False) Then
            '    ' when man is going to send message  and settings allow to send,
            '    ' if he has no credits then
            '    ' check his country settings, if he is allowed to send any free message

            '    MaySendFreeMessage = clsUserDoes.MaySendFreeMessage(memberReceivingMessage.ProfileID, Me.MasterProfileId, Me.SessionVariables.MemberData.Country)
            '    If (MaySendFreeMessage) Then
            '        maleSend = MaySendFreeMessage
            '        Me.SetUI_SendingFreeMessage()
            '    End If
            'End If

            Dim messagePaid As Boolean = Me.IsFemale OrElse (AlreadyUnlockedUnlimited AndAlso Me.IsMale)
            Dim failMessage As String = ""

            If (MaySendMessage) Then
                Dim IsAuto As Boolean = False
                Dim messageReceived As EUS_Message = clsUserDoes.SendMessage(Subject, Body, Me.MasterProfileId, recipientUserId, (Me.IsFemale), 0, MyBase.BlockMessagesToBeReceivedByOther, IsAuto, If(Me.IsFemale, 2, 1))
                ' send email message to administrator
                If (recipientUserId = 1) Then
                    Try
                        Dim newSubject As String = "Message sent to administrator of Goomena.com"
                        Dim newBody As String =
                            "<br>" &
                            "Message sent to administrator of Goomena.com" & "<br>" & _
                            "<hr>" & _
                            "Login Name : " & Me.GetCurrentProfile().LoginName & "<br>" & _
                            "E-mail : " & Me.GetCurrentProfile().eMail & "<br>" & _
                            "<hr>" & _
                            "Subject : " & Subject & "<br>" & _
                            "Body : " & Body

                        clsMyMail.SendMail(ConfigurationManager.AppSettings("gToEmail"), newSubject, newBody, True)
                    Catch ex As Exception
                    End Try
                End If

                If (Me.IsMale) Then
                    If (Not AlreadyUnlockedUnlimited) Then
                        If (SendOnce) Then

                            Dim HasRequiredCredits_To_UnlockMessage As Boolean = HasRequiredCredits(UnlockType.UNLOCK_MESSAGE_SEND)
                            If (HasRequiredCredits_To_UnlockMessage) Then
                                Dim _REF_CRD2EURO_Rate As Double = DataHelpers.GetEUS_Profiles_REF_CRD2EURO_Rate(messageReceived.FromProfileID)
                                Dim customerCreditsId As Long = clsUserDoes.UnlockMessageOnce(recipientUserId,
                                                                                              Me.MasterProfileId,
                                                                                              ProfileHelper.Config_UNLOCK_MESSAGE_SEND_CREDITS,
                                                                                              UnlockType.UNLOCK_MESSAGE_SEND,
                                                                                              messageReceived.EUS_MessageID,
                                                                                              _REF_CRD2EURO_Rate, False)

                                '''''''''''''
                                'reset session property to update available credits, when master.page loads
                                Session("CustomerAvailableCredits") = Nothing
                                '''''''''''''

                                Try
                                    ' write commissition for recipient referrer
                                    Dim costPerMessage As Double = ProfileHelper.Config_UNLOCK_MESSAGE_SEND_CREDITS * _REF_CRD2EURO_Rate
                                    clsReferrer.SetCommissionForMessage(messageReceived.EUS_MessageID,
                                                                        messageReceived.ToProfileID,
                                                                        costPerMessage,
                                                                        ProfileHelper.Config_UNLOCK_MESSAGE_SEND_CREDITS,
                                                                        UnlockType.UNLOCK_MESSAGE_SEND,
                                                                        customerCreditsId)
                                Catch ex As Exception
                                    WebErrorSendEmail(ex, "")
                                End Try

                                messagePaid = True
                            ElseIf (False AndAlso False) Then

                                Try
                                    Dim ctr As SYS_CountriesGEO = SYS_CountriesGEOHelper.SYS_CountriesGEO_GetForCountry(Me.SessionVariables.MemberData.Country)
                                    ' write commissition for recipient referrer
                                    Dim costPerMessage As Double = clsNullable.NullTo(ctr.CreditsFreeMessageReferralAmount)
                                    Dim _REF_CRD2EURO_Rate As Double? = costPerMessage / ProfileHelper.Config_UNLOCK_MESSAGE_SEND_CREDITS

                                    Dim customerCreditsId As Long = clsUserDoes.UnlockMessageOnce(recipientUserId,
                                                                                                  Me.MasterProfileId,
                                                                                                  ProfileHelper.Config_UNLOCK_MESSAGE_FREE_CREDITS,
                                                                                                  UnlockType.UNLOCK_MESSAGE_SEND_FREE,
                                                                                                  messageReceived.EUS_MessageID,
                                                                                                  _REF_CRD2EURO_Rate, False)

                                    clsReferrer.SetCommissionForMessage(messageReceived.EUS_MessageID,
                                                                        messageReceived.ToProfileID,
                                                                        costPerMessage,
                                                                        ProfileHelper.Config_UNLOCK_MESSAGE_FREE_CREDITS,
                                                                        UnlockType.UNLOCK_MESSAGE_SEND_FREE,
                                                                        customerCreditsId)
                                Catch ex As Exception
                                    WebErrorSendEmail(ex, "")
                                End Try

                                messagePaid = True
                            Else
                                failMessage = "Not enough credits for UNLOCK_MESSAGE_SEND"
                            End If

                        ElseIf (SendUnlimited) Then

                            Dim HasRequiredCredits_To_UnlockConversation As Boolean = HasRequiredCredits(UnlockType.UNLOCK_CONVERSATION)
                            'Dim HasRequiredCredits_To_UnlockConversation As Boolean = clsUserDoes.HasRequiredCredits(Me.MasterProfileId, UnlockType.UNLOCK_CONVERSATION)
                            If (HasRequiredCredits_To_UnlockConversation) Then
                                Dim customerCreditsId As Long = clsUserDoes.UnlockMessageUnlimited(recipientUserId, Me.MasterProfileId, Me.OfferId, False)

                                '''''''''''''
                                'reset session property to update available credits, when master.page loads
                                Session("CustomerAvailableCredits") = Nothing
                                '''''''''''''

                                Try
                                    ' write commissition for recipient referrer
                                    Dim _REF_CRD2EURO_Rate As Double = DataHelpers.GetEUS_Profiles_REF_CRD2EURO_Rate(messageReceived.FromProfileID)
                                    Dim costPerMessage As Double = ProfileHelper.Config_UNLOCK_CONVERSATION_CREDITS * _REF_CRD2EURO_Rate
                                    clsReferrer.SetCommissionForMessage(messageReceived.EUS_MessageID,
                                                                        messageReceived.ToProfileID,
                                                                        costPerMessage,
                                                                        ProfileHelper.Config_UNLOCK_CONVERSATION_CREDITS,
                                                                        UnlockType.UNLOCK_CONVERSATION,
                                                                        customerCreditsId)
                                Catch ex As Exception
                                    WebErrorSendEmail(ex, "")
                                End Try

                                messagePaid = True
                            Else
                                failMessage = "Not enough credits for UNLOCK_CONVERSATION"
                            End If

                        End If

                    End If
                End If

                If (messagePaid) Then
                    ' o andras, epeidi plirwnei, dimiourgeitai neo date
                    MakeNewDate(Me.MasterProfileId, recipientUserId)
                    Using cmdb As CMSDBDataContext = Me.GetCMSDBDataContext
                        Dim login As String = DataHelpers.GetEUS_Profile_LoginName_ByProfileID(cmdb, recipientUserId)
                        Dim url As String = ResolveUrl("~/Members/Messages.aspx?vw=" & login & "&t=" & MessagesViewEnum.SENT.ToString())
                        Response.Redirect(url, False)
                    End Using
                    ' 
                  

                Else
                    If (failMessage = "Not enough credits for UNLOCK_MESSAGE_SEND") Then

                    ElseIf (failMessage = "Not enough credits for UNLOCK_CONVERSATION") Then

                    End If
                End If
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Private Sub MakeNewDate(FromProfileId As Integer, ToProfileId As Integer)
        Dim anyOfferAccepted As Boolean = False

        Dim parms As New clsUserDoes.NewOfferParameters()
        If (Me.OfferId > 0) Then

            Dim _offer As EUS_Offer = clsUserDoes.GetOfferByOfferId(Me.MasterProfileId, Me.OfferId)
            If (_offer.OfferTypeID = ProfileHelper.OfferTypeID_POKE) Then
                clsUserDoes.AcceptPoke(Me.OfferId, OfferStatusEnum.POKE_ACCEEPTED_WITH_MESSAGE)
                anyOfferAccepted = True
            ElseIf (_offer.OfferTypeID = ProfileHelper.OfferTypeID_WINK) Then
                clsUserDoes.AcceptLike(Me.OfferId, OfferStatusEnum.LIKE_ACCEEPTED_WITH_MESSAGE)
                anyOfferAccepted = True
            ElseIf (_offer.OfferTypeID = ProfileHelper.OfferTypeID_OFFERNEW) OrElse (_offer.OfferTypeID = ProfileHelper.OfferTypeID_OFFERCOUNTER) Then
                clsUserDoes.AcceptOffer(Me.OfferId, OfferStatusEnum.OFFER_ACCEEPTED_WITH_MESSAGE)
                anyOfferAccepted = True
            End If


            'If (SendUnlimited) Then
            parms.messageText1 = ""
            parms.messageText2 = ""
            parms.offerAmount = _offer.Amount
            parms.parentOfferId = _offer.OfferID
            parms.userIdReceiver = _offer.ToProfileID
            parms.userIdWhoDid = _offer.FromProfileID
            'End If
        Else
            parms.messageText1 = ""
            parms.messageText2 = ""
            parms.offerAmount = 0
            'parms.parentOfferId = _offer.OfferID
            parms.userIdReceiver = ToProfileId
            parms.userIdWhoDid = FromProfileId
        End If

        If (Me.IsMale) Then
            If (Not clsUserDoes.HasDateOfferAlready(parms.userIdWhoDid, parms.userIdReceiver)) Then clsUserDoes.MakeNewDate(parms)
        End If


        If (Not anyOfferAccepted) Then

            Dim rec As EUS_Offer = clsUserDoes.GetAnyWinkPending(Me.MasterProfileId, Me.UserIdInView)
            If (rec IsNot Nothing) Then
                'like check succesfull
                clsUserDoes.AcceptLike(rec.OfferID, OfferStatusEnum.LIKE_ACCEEPTED_WITH_MESSAGE)
            Else
                rec = clsUserDoes.GetAnyPokePending(Me.MasterProfileId, Me.UserIdInView)
                If (rec IsNot Nothing) Then
                    'poke check succesfull
                    clsUserDoes.AcceptPoke(rec.OfferID, OfferStatusEnum.POKE_ACCEEPTED_WITH_MESSAGE)
                Else
                    rec = clsUserDoes.GetLastOffer(Me.MasterProfileId, Me.UserIdInView)
                    If (rec IsNot Nothing) Then
                        'offer check succesfull
                        clsUserDoes.AcceptOffer(rec.OfferID, OfferStatusEnum.OFFER_ACCEEPTED_WITH_MESSAGE)
                    End If
                End If
            End If

        End If

    End Sub


    Protected Shared Function GetCountMessagesString(ItemsCount As Object) As String

        If (Not AppUtils.IsDBNullOrNothingOrEmpty(ItemsCount)) Then
            Try
                If (ItemsCount > 0) Then Return "(" & ItemsCount & ")"
            Catch ex As Exception

            End Try
        End If

        Return ""
    End Function


    Protected Shared Function IsVisible(ItemsCount As Object) As Boolean

        If (Not AppUtils.IsDBNullOrNothingOrEmpty(ItemsCount)) Then
            Try
                If (ItemsCount > 1) Then Return True
            Catch ex As Exception

            End Try
        End If

        Return False
    End Function




End Class






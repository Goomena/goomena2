﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UCPrice.ascx.vb" Inherits="Dating.Server.Site.Web.Price" %>

<asp:MultiView ID="mv" runat="server" ActiveViewIndex="0">

    <asp:View ID="vwPublic" runat="server">
<div class="pricecol" style="background-image: url('//cdn.goomena.com/goomena-kostas/pricing/for-public.png'); background-repeat: no-repeat;width:251px;height:338px;" 
    align="center">
    <div class="pricecol-credits"><%= MyBase.Credits%><div class="pricecol-credits-text"><%= MyBase.CreditsText%></div></div>
    <div class="pricecol-price<%= MyBase.SmallClass%>"><div class="pricecol-price-text"><%= MyBase.MoneySymb%><span style="font-size:14px;">&nbsp;</span><%= MyBase.Price%></div></div>
    <div class="pricecol-expire<%= MyBase.SmallClass%>"><div class="pricecol-expire-text"><asp:Literal ID="lblExpireAfterPub" runat="server">Expire after [DURATION] Days</asp:Literal></div></div>
&nbsp;
    <asp:ImageButton ID="imgCreditsP" runat="server" align="center"
        ImageUrl="//cdn.goomena.com/goomena-kostas/pricing/order-button.png" CssClass="ordbut" OnClientClick="ShowLoading();" />
</div>
    </asp:View>


    <asp:View ID="vwPublicRecom" runat="server">
<div class="pricecol" style="background-image: url('//cdn.goomena.com/goomena-kostas/pricing/for-public-recommend.png'); background-repeat: no-repeat;width:287px;height:338px;" 
    align="center">
    <div class="pricecol-credits"><%= MyBase.Credits%><div class="pricecol-credits-text"><%= MyBase.CreditsText%></div></div>
    <div class="pricecol-price<%= MyBase.SmallClass%>"><div class="pricecol-price-text"><%= MyBase.MoneySymb%><span style="font-size:14px;">&nbsp;</span><%= MyBase.Price%></div></div>
    <div class="pricecol-expire<%= MyBase.SmallClass%>"><div class="pricecol-expire-text"><asp:Literal ID="lblExpireAfterPubRecom" runat="server">Expire after [DURATION] Days</asp:Literal></div></div>
&nbsp;
    <asp:ImageButton ID="imgCreditsPR" runat="server" align="center"
        ImageUrl="//cdn.goomena.com/goomena-kostas/pricing/order-button.png" CssClass="ordbut" OnClientClick="ShowLoading();" />
</div>
    </asp:View>


    <asp:View ID="vwMember" runat="server">
<div class="pricecol" style="background-image: url('//cdn.goomena.com/goomena-kostas/pricing/for-member.png'); background-repeat: no-repeat;width:195px;height:263px;" 
    align="center">
    <div class="pricecol-credits"><%= MyBase.Credits%><div class="pricecol-credits-text"><%= MyBase.CreditsText%></div></div>
    <div class="pricecol-price<%= MyBase.SmallClass%>"><div class="pricecol-price-text"><%= MyBase.MoneySymb%><span style="font-size:14px;">&nbsp;</span><%= MyBase.Price%></div></div>
    <div class="pricecol-expire<%= MyBase.SmallClass%>"><div class="pricecol-expire-text"><asp:Literal ID="lblExpireAfterMemb" runat="server">Expire after [DURATION] Days</asp:Literal></div></div>
&nbsp;
    <asp:ImageButton ID="imgCreditsM" runat="server" align="center"
        ImageUrl="//cdn.goomena.com/goomena-kostas/pricing/order-button.png" CssClass="ordbut" OnClientClick="ShowLoading();" />
</div>
    </asp:View>


    <asp:View ID="vwMemberRecom" runat="server">
<div class="pricecol" style="background-image: url('//cdn.goomena.com/goomena-kostas/pricing/for-member-recommend.png'); background-repeat: no-repeat;width:224px;height:263px;" 
    align="center">
    <div class="pricecol-credits"><%= MyBase.Credits%><div class="pricecol-credits-text"><%= MyBase.CreditsText%></div></div>
    <div class="pricecol-price<%= MyBase.SmallClass%>"><div class="pricecol-price-text"><%= MyBase.MoneySymb%><span style="font-size:14px;">&nbsp;</span><%= MyBase.Price%></div></div>
    <div class="pricecol-expire<%= MyBase.SmallClass%>"><div class="pricecol-expire-text"><asp:Literal ID="lblExpireAfterMembRecom" runat="server">Expire after [DURATION] Days</asp:Literal></div></div>
&nbsp;
    <asp:ImageButton ID="imgCreditsMR" runat="server" align="center"
        ImageUrl="//cdn.goomena.com/goomena-kostas/pricing/order-button.png" CssClass="ordbut" OnClientClick="ShowLoading();"  />
</div>
    </asp:View>

</asp:MultiView>

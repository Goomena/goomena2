﻿'Imports Sites.SharedSiteCode
Imports Library.Public
Imports Dating.Server.Core.DLL
Imports Dating.Server.Datasets.DLL


Public Class UCSelectProductSubscriptionMembers
    Inherits BaseUserControl

    Protected Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            'If (_pageData Is Nothing) Then _pageData = New clsPageData("control.UCSelectProductMembers", Context)
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData("control.UCSelectProductMembers", Context, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property


    Dim _control As String = ""
    Public Property control As String
        Get
            Return _control
        End Get
        Set(ByVal value As String)
            _control = value
        End Set
    End Property

    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try

            If (Not Me.IsPostBack AndAlso Me.Visible) Then
                LoadLAG()
                'LoadView()

                'If (Me.SessionVariables.MemberData.Country = "TR") Then
                '    UCPrice1.CssClass = UCPrice1.CssClass & " tr"
                '    UCPrice2.CssClass = UCPrice2.CssClass & " tr"
                '    UCPrice3.CssClass = UCPrice3.CssClass & " tr"
                '    payment_2.Attributes("class") = payment_2.Attributes("class") & " tr"
                'Else
                '    ' ok
                'End If

                Dim currency As String = Me.GetCurrency()
                UCPrice1Sub.SetPricing(clsPricing.GetPriceForDisplayIndex(10, currency))
                UCPrice2Sub.SetPricing(clsPricing.GetPriceForDisplayIndex(11, currency))
                UCPrice3Sub.SetPricing(clsPricing.GetPriceForDisplayIndex(13, currency))
                UCPrice4Sub.SetPricing(clsPricing.GetPriceForDisplayIndex(12, currency))

                UCPrice1Crd.SetPricing(clsPricing.GetPriceForDisplayIndex(1, currency))
                UCPrice2Crd.SetPricing(clsPricing.GetPriceForDisplayIndex(2, currency))
                UCPrice3Crd.SetPricing(clsPricing.GetPriceForDisplayIndex(3, currency))
                UCPrice5Crd.SetPricing(clsPricing.GetPriceForDisplayIndex(5, currency))

                pnlBonus.Visible = (clsConfigValues.Get__credits_bonus_codes() = 1) OrElse (Request.QueryString("bonus") = "1")

                If (payment_2.Attributes("class") IsNot Nothing) Then payment_2.Attributes("class") = payment_2.Attributes("class").Replace(" es", "").Replace(" us", "")
                If (GetLag() = "ES") Then
                    payment_2.Attributes("class") = payment_2.Attributes("class") & " es"
                ElseIf (GetLag() <> "GR") Then
                    payment_2.Attributes("class") = payment_2.Attributes("class") & " us"
                End If
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub



    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
        'LoadView()
    End Sub


    Public Sub LoadLAG() 'ByVal LAGID As String, ByVal NoCache As Boolean
        Try

            'PanelSelectProduct.Text = Me.CurrentPageData.GetCustomString("PanelSelectProduct")
            'lbFlashText.Text = Me.CurrentPageData.GetCustomString("lbFlashText")
            'lbConfusedText.Text = Me.CurrentPageData.GetCustomString("lbconfusedText")
            'lbConfusedMore.Text = Me.CurrentPageData.GetCustomString("lbconfusedMore")
            'lblTopTitle.Text = Me.CurrentPageData.GetCustomString("lblTopTitle")

            lblPaymentsGuarantee.Text = Me.CurrentPageData.GetCustomString("lblPaymentsGuarantee4")

            btnBonusCredits.Text = Me.CurrentPageData.GetCustomString("btnBonusCredits")
            lblBonusCode.Text = Me.CurrentPageData.GetCustomString("lblBonusCode")
            'lblSelectProduct.Text = Me.CurrentPageData.GetCustomString("lblSelectProduct")
            lbfootText.Text = Me.CurrentPageData.GetCustomString("lbfootText3")

            If (Not clsPricing.VATCountries.Contains(Session("GEO_COUNTRY_CODE"))) Then
                imgPaymentSigns.Src = imgPaymentSigns.Src.Replace("/payment-logos.png", "/payment-logos_nopp.png")
            End If

            lblSubscrHeader.Text = Me.CurrentPageData.GetCustomString("Subscription.Header")
            lblSubscrInfo.Text = Me.CurrentPageData.GetCustomString("Subscription.Info")
            lblCreditsHeader.Text = Me.CurrentPageData.GetCustomString("Credits.Header")

            'If (ProfileCountry.MonthlySubscriptionEnabled = True) Then
            lblSubscrInfo.Text = lblSubscrInfo.Text.Replace("[MAX_PROFILES]", ProfileCountry.SubscriptionMaxWomanProfiles).Replace("[MAX_MESSAGES]", ProfileCountry.MaxMessagesToProfilePerDay)
            'End If

        Catch ex As Exception
            WebErrorMessageBox(ex, "")
        End Try

    End Sub


    'Private Sub img30Days_Command(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs) Handles img1000Credits.Command, img3000Credits.Command, img6000Credits.Command ', img10000Credits.Command, img30000Credits.Command, img50000Credits.Command ', img999Days.Command ', img50GB.Command, img150GB.Command, img300GB.Command, img600GB.Command, img5000GB.Command
    'Private Sub img30Days_Command(ByVal sender As Object, ByVal e As PriceSelectedEventArgs) Handles UCPrice1.PriceSelected, UCPrice2.PriceSelected, UCPrice3.PriceSelected, UCPrice4.PriceSelected, UCPrice5.PriceSelected
    'Handles lnkSubcribeMonth.Click, lnkSubcribe3Months.Click, lnkSubcribe6Months.Click
    Private Sub SubscriptionProduct_Command(ByVal sender As Object, ByVal e As PriceSelectedEventArgs) Handles UCPrice1Sub.PriceSelected, UCPrice2Sub.PriceSelected, UCPrice3Sub.PriceSelected, UCPrice4Sub.PriceSelected
        Dim amount As Integer = 0
        Dim days As Integer = 0
        Dim credits As Integer = 0
        Dim product As String = credits

        days = e.Duration
        amount = e.Price
        product = e.ProductCode
        credits = days * amount * 100
        product = product.ToLower().Replace("dd", "")

        Session("credits") = credits
        Session("purDesc") = days
        Session("amount") = amount

        Dim url As String = ResolveUrl("~/Members/SelectPaymentSubscription.aspx?choice=" & product.ToLower())
        'If (Me.SessionVariables.MemberData.Country = "TR") Then
        '    url = ResolveUrl("~/Members/selectPaymentTR.aspx?choice=" & product)
        'End If


        If _control = "new" Then
            url = HttpUtility.UrlEncode(url)
            Dim registerUrl As String = ResolveUrl("~/register.aspx?ReturnUrl=" & url)
            Response.Redirect(registerUrl)

        ElseIf Me.SessionVariables.MemberData Is Nothing OrElse Me.Session("ProfileID") = 0 Then
            url = HttpUtility.UrlEncode(url)
            Dim loginUrl As String = ResolveUrl("~/login.aspx?ReturnUrl=" & url)
            Response.Redirect(loginUrl)
            'Response.Redirect("http://" & gSiteName & "/login.aspx")

        Else
            'Response.Redirect("selectPayment2.aspx?choice=" & product)
            Response.Redirect(url)

        End If

    End Sub

    Private Sub CreditsProduct_Command(ByVal sender As Object, ByVal e As PriceSelectedEventArgs) Handles UCPrice1Crd.PriceSelected, UCPrice2Crd.PriceSelected, UCPrice3Crd.PriceSelected, UCPrice5Crd.PriceSelected
        Dim amount As String = ""
        Dim days As String = ""
        Dim credits As String = ""

        'credits = e.CommandArgument.ToString.Split(",")(0)
        'days = e.CommandArgument.ToString.Split(",")(1)
        'amount = e.CommandArgument.ToString.Split(",")(2)
        credits = e.Credits
        days = e.Duration
        amount = e.Price

        Session("credits") = credits.Replace("Credits", "")
        Session("purDesc") = days
        Session("amount") = amount

        Dim product As String = credits
        Dim url As String = ResolveUrl("~/Members/selectPayment.aspx?choice=" & product)
        If (Me.SessionVariables.MemberData.Country = "TR") Then
            url = ResolveUrl("~/Members/selectPaymentTR.aspx?choice=" & product)
        End If


        If _control = "new" Then
            url = HttpUtility.UrlEncode(url)
            Dim registerUrl As String = ResolveUrl("~/register.aspx?ReturnUrl=" & url)
            Response.Redirect(registerUrl)

        ElseIf Me.SessionVariables.MemberData Is Nothing OrElse Me.Session("ProfileID") = 0 Then
            url = HttpUtility.UrlEncode(url)
            Dim loginUrl As String = ResolveUrl("~/login.aspx?ReturnUrl=" & url)
            Response.Redirect(loginUrl)
            'Response.Redirect("http://" & gSiteName & "/login.aspx")

        Else
            'Response.Redirect("selectPayment2.aspx?choice=" & product)
            Response.Redirect(url)

        End If

    End Sub


    Protected Sub btnBonusCredits_Click(sender As Object, e As EventArgs) Handles btnBonusCredits.Click
        Try
            If (txtBonusCode.Text.Trim() = "") Then
                lblBonusCodeError.Text = Me.CurrentPageData.GetCustomString("lblBonusCodeError")
                Return
            End If

            If (Not DataHelpers.EUS_CreditsBonusCodes_IsCodeAvailable(Me.MasterProfileId, txtBonusCode.Text.Trim())) Then
                lblBonusCodeError.Text = Me.CurrentPageData.GetCustomString("lblBonusCodeError.NotValid")
                Return
            End If


            Dim ipnData As New clsDataRecordIPN()
            ipnData.BuyerInfo = New clsDataRecordBuyerInfo With {
                .PayerID = "PayerID", _
                .FirstName = Me.GetCurrentProfile.FirstName, _
                .LastName = Me.GetCurrentProfile.LastName, _
                .PayerEmail = Me.GetCurrentProfile.eMail _
            }
            ipnData.CustomerID = Me.MasterProfileId
            ipnData.CustomReferrer = Me.GetCurrentProfile().CustomReferrer
            ipnData.custopmerIP = Session("IP")
            ipnData.PaymentDateTime = Date.UtcNow
            ipnData.PayProviderAmount = 0
            ipnData.PayProviderID = -1
            ipnData.PayProviderTransactionID = -1
            ipnData.PayTransactionID = -1
            ipnData.PromoCode = Nothing
            ipnData.SaleDescription = "Add credits using bonus code. BonusCode:[" & txtBonusCode.Text & "]"
            ipnData.SaleQuantity = 300
            ipnData.SalesSiteID = ConfigurationManager.AppSettings("siteID")
            ipnData.TransactionTypeID = -1


            Dim sData As String
            sData = ipnData.PayProviderAmount & "+" & ipnData.PaymentDateTime & "+" & ipnData.PayTransactionID & "+" & ipnData.CustomerID & "+" & ipnData.SalesSiteProductID & "Extr@Ded0men@"
            '   Dim h As New Library.Public.clsHash
            Dim Code As String = Library.Public.clsHash.ComputeHash(sData, "SHA512")
            ipnData.VerifyHASH = Code

            Using srvc As New UniPAYIPN


                Dim cDataRecordIPNReturn As clsDataRecordIPNReturn = srvc.IPNEventRaised(ipnData)
                If (cDataRecordIPNReturn.ErrorCode = 0) Then

                    DataHelpers.EUS_CreditsBonusCodes_DisableCode(Me.MasterProfileId, txtBonusCode.Text.Trim())
                    Session("CustomerAvailableCredits") = Nothing

                    lblBonusCode.Text = ""
                    lblBonusCodeSuccess.Text = Me.CurrentPageData.GetCustomString("lblBonusCodeSuccess")
                    txtBonusCode.Visible = False
                    btnBonusCredits.Visible = False

                    'Response.Redirect(ResolveUrl("~/Members/"))
                    ' lblBonusCodeError.Text = Me.CurrentPageData.GetCustomString("lblBonusCodeError.NotValid")
                End If
            End Using
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try
    End Sub

End Class
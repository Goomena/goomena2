﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="MessageNoPhoto.ascx.vb" Inherits="Dating.Server.Site.Web.MessageNoPhoto" %>
<%--<div class="m_wrap">
    <div class="clearboth padding">
    </div>
    <div class="items_none items_hard">
        <asp:Literal ID="msg_HasNoPhotosText" runat="server" />
        <div class="actions">
            <ul>
                <li class="">
                    <asp:HyperLink ID="lnk1" runat="server" NavigateUrl="~/Members/Photos.aspx">
                        <asp:Label
                            ID="msg_AddPhotosText" runat="server" />
                    </asp:HyperLink></li>
            </ul>
        </div>
        <div
            class="clear">
        </div>
    </div>
</div>--%>
<div id="ph-edit" class="no-photo">
    <div class="items_none items_hard">
        <div class="items_none_wrap">
            <div class="items_none_text">
                <asp:Literal ID="msg_HasNoPhotosText" runat="server"></asp:Literal>
                <div class="search_members_button_right"><asp:HyperLink ID="lnkUploadPhoto" runat="server" CssClass="btn btn-danger" NavigateUrl="~/Members/Photos.aspx">
                    <asp:Literal
                            ID="msg_AddPhotosText" runat="server" /></asp:HyperLink></div>
            </div>
        </div>
    </div>
</div>

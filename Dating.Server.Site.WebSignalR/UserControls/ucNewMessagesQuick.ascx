﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucNewMessagesQuick.ascx.vb" Inherits="Dating.Server.Site.Web.ucNewMessagesQuick" %>
<%@ Register assembly="DevExpress.Web.v13.1, Version=13.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxDataView" tagprefix="dx" %>
<%--    DataSourceID="sdsMessages" --%>

<dx:ASPxDataView ID="dvMessages" runat="server" ColumnCount="1" 
    EnableDefaultAppearance="False" EnableTheming="False" 
    ItemSpacing="0px" RowPerPage="10" Width="100%" 
    EmptyDataText="Can't find any new message.">
    <ItemTemplate>
<asp:Panel ID="pnlUnl" runat="server" CssClass="quick_list m_item newest ql-msg">
    <div class="linkNotifWrapper" onclick="jsLoad('<%# System.Web.VirtualPathUtility.ToAbsolute("~/Members/Conversation.aspx?vw=") & HttpUtility.UrlEncode(Eval("LoginName"))%>');">
        <div class="pic-round-img-75 lfloat">
            <asp:HyperLink ID="lnkFromImg" runat="server" 
                ImageUrl='<%# Dating.Server.Site.Web.AppUtils.GetImage(Eval("ProfileID").ToString(), Eval("FileName").ToString(), Eval("GenderId").ToString(), False, Me.IsHTTPS, Dating.Server.Core.DLL.PhotoSize.D150)%>' 
                NavigateUrl='<%# System.Web.VirtualPathUtility.ToAbsolute("~/Members/Conversation.aspx?vw=") & HttpUtility.UrlEncode(Eval("LoginName"))%>'
                onclick="ShowLoading()" >
            </asp:HyperLink>
        </div>
        <div class="lfloat login-link">
            <asp:HyperLink ID="lnkFromLogin" runat="server" 
                NavigateUrl='<%# System.Web.VirtualPathUtility.ToAbsolute("~/Members/Conversation.aspx?vw=") & HttpUtility.UrlEncode(Eval("LoginName"))%>' 
                Text='<%# Eval("LoginName") %>' CssClass="login-name"
                onclick="ShowLoading()" >
            </asp:HyperLink>
            <asp:Label ID="lblSubject" runat="server" Text="" Style="display:block;"/><br />
            <asp:Label ID="lblSentDate" runat="server" Text='<%# MyBase.GetMessageDateDescription(Eval("MAXDateTimeToCreate"))%>' Style="font-style:italic !improtant;"/>
            <div class="msg-counter">
                <asp:Label ID="lblCount" runat="server" 
                    Text='<%# Mybase.GetCountMessagesString(Eval("ItemsCount")) %>' 
                    Visible='<%# Mybase.IsVisible(Eval("ItemsCount")) %>'/>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</asp:Panel>  
<div class="clear"></div>
    </ItemTemplate>
    <Paddings Padding="0px" />
    <ItemStyle>
    <Paddings Padding="0px" />
    </ItemStyle>
</dx:ASPxDataView>

<%--
<asp:SqlDataSource ID="sdsMessages" runat="server" 
    ConnectionString="<%$ ConnectionStrings:AppDBconnectionString %>" 
    SelectCommand="GetNewMessagesQuick2" SelectCommandType="StoredProcedure">
    <SelectParameters>
        <asp:Parameter DefaultValue="0" Name="CurrentProfileId" Type="Int32" />
        <asp:Parameter DefaultValue="4" Name="ReturnRecordsWithStatus" Type="Int32" />
        <asp:Parameter DefaultValue="5" Name="NumberOfRecordsToReturn" Type="Int32" />
    </SelectParameters>
</asp:SqlDataSource>
    
<asp:Panel ID="pnlUnl" runat="server" CssClass="quick_list m_item newest ql-msg" Visible='<%# Eval("CommunicationUnl") > 0 %>'>
    <div class="linkNotifWrapper" onclick="jsLoad('<%# System.Web.VirtualPathUtility.ToAbsolute("~/Members/Conversation.aspx?vw=") & HttpUtility.UrlEncode(Eval("LoginName"))%>');">
        <div class="pic-round-img-75 lfloat">
            <asp:HyperLink ID="lnkFromImg" runat="server" 
                ImageUrl='<%# Dating.Server.Site.Web.AppUtils.GetImage(Eval("ProfileID").Tostring(), Eval("FileName").Tostring(), Eval("GenderId").Tostring(), False, Me.IsHTTPS,Dating.Server.Core.DLL.PhotoSize.D150) %>' 
                NavigateUrl='<%# System.Web.VirtualPathUtility.ToAbsolute("~/Members/Conversation.aspx?vw=") & HttpUtility.UrlEncode(Eval("LoginName"))%>'
                onclick="ShowLoading()" >
            </asp:HyperLink>
        </div>
        <div class="lfloat login-link">
            <asp:HyperLink ID="lnkFromLogin" runat="server" 
                NavigateUrl='<%# System.Web.VirtualPathUtility.ToAbsolute("~/Members/Conversation.aspx?vw=") & HttpUtility.UrlEncode(Eval("LoginName"))%>' 
                Text='<%# Eval("LoginName") %>' CssClass="login-name"
                onclick="ShowLoading()" >
            </asp:HyperLink>
            <asp:Label ID="lblSubject" runat="server" Text=""/><br />
            <asp:Label ID="lblSentDate" runat="server" Text='<%# MyBase.GetMessageDateDescription(Eval("MAXDateTimeToCreate"))%>' Style="font-style:italic !improtant;"/>
            <div class="msg-counter">
                <asp:Label ID="lblCount" runat="server" 
                    Text='<%# Mybase.GetCountMessagesString(Eval("ItemsCount")) %>' 
                    Visible='<%# Mybase.IsVisible(Eval("ItemsCount")) %>'/>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</asp:Panel>  
<asp:Panel ID="pnlLtd" runat="server" CssClass="quick_list m_item newest ql-msg" Visible='<%# Eval("CommunicationUnl") = 0 %>'>
    <div class="linkNotifWrapper" onclick="jsLoad('<%# System.Web.VirtualPathUtility.ToAbsolute("~/Members/Conversation.aspx?vw=") & HttpUtility.UrlEncode(Eval("LoginName"))%>');">
        <div class="pic-round-img-75 lfloat">
            <asp:HyperLink ID="lnkFromImgLtd" runat="server" 
                ImageUrl='<%# Dating.Server.Site.Web.AppUtils.GetImage(Eval("ProfileID").ToString(), Eval("FileName").ToString(), Eval("GenderId").ToString(), False, Me.IsHTTPS)%>' 
                NavigateUrl='<%# System.Web.VirtualPathUtility.ToAbsolute("~/Members/Conversation.aspx?vw=") & HttpUtility.UrlEncode(Eval("LoginName"))%>'
                onclick="ShowLoading()" >
            </asp:HyperLink>
        </div>
        <div class="lfloat login-link">
            <asp:HyperLink ID="lnkFromLoginLtd" runat="server" 
                NavigateUrl='<%# System.Web.VirtualPathUtility.ToAbsolute("~/Members/Conversation.aspx?vw=") & HttpUtility.UrlEncode(Eval("LoginName")) %>' 
                Text='<%# Eval("LoginName") %>' CssClass="login-name"
                onclick="ShowLoading()">
            </asp:HyperLink>
            <asp:Label ID="lblSubjectLtd" runat="server" Text=""/><br />
            <asp:Label ID="lblSentDateLtd" runat="server" Text='<%# MyBase.GetMessageDateDescription(Eval("MAXDateTimeToCreate"))%>' Style="font-style:italic !improtant;"/>
            <div class="msg-counter">
                <asp:Label ID="lblCountLtd" runat="server" 
                    Text='<%# Mybase.GetCountMessagesString(Eval("ItemsCount")) %>' 
                    Visible='<%# Mybase.IsVisible(Eval("ItemsCount")) %>'/>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</asp:Panel>  
    
    
    --%>

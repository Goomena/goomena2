﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UsersList.ascx.vb" Inherits="Dating.Server.Site.Web.UsersList" %>
<asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">

    <asp:View ID="View1" runat="server">
    </asp:View>


    <asp:View ID="vwMembersDefaulMainList" runat="server">
<asp:ListView ID="lv" runat="server">
<LayoutTemplate>
    <ul>
        <li id="itemPlaceholder" runat="server"></li>
    </ul>
</LayoutTemplate>
<ItemTemplate>
    <li>
        <div class="d_prof_wrap">
            <a href="<%# Eval("ProfileViewUrl") %>">
                <img alt="<%# Eval("LoginName") %>" src="<%# Eval("ImageUrl") %>"/>
            </a>
            <div class="d_check" id="chkBox" runat="server" visible='<%# Eval("ShowCheckBox") %>'>
                <input type="checkbox" value="<%# Eval("ProfileId") %>" checked="checked" /><label><%# Eval("CheckBoxText")%></label>
            </div>
        </div>
    </li>
</ItemTemplate>
</asp:ListView>
    </asp:View>


    <asp:View ID="vwMembersDefaultFavoritedOrViewed" runat="server">
<asp:ListView ID="lv2" runat="server">
<LayoutTemplate>
    <ul>
        <li id="itemPlaceholder" runat="server"></li>
    </ul>
</LayoutTemplate>
<ItemTemplate>
    <li>
        <div class="photo">
            <div class="thumb"><a href="<%# Eval("ProfileViewUrl") %>"><img alt="<%# Eval("LoginName") %>" src="<%# Eval("ImageThumbUrl") %>"/></a></div>
        </div>
    </li>
</ItemTemplate>
</asp:ListView>
    </asp:View>


    <asp:View ID="vwGenerous" runat="server">
<asp:ListView ID="lvG" runat="server">
    <LayoutTemplate>
        <div class="users">
            <div id="itemPlaceholder" runat="server"></div>
        </div>
    </LayoutTemplate>
    <ItemTemplate>
        <div>
            <a id="lnk" href="<%# Eval("ProfileViewUrl") %>">
                <img src='<%# Eval("ImageUrl") %>' alt="" />
                <span class="block"><%# Eval("LoginName") %></span></a>
        </div>
    </ItemTemplate>
</asp:ListView>
    </asp:View>


    <asp:View ID="vwAttractives" runat="server">
<asp:ListView ID="lvA" runat="server">
    <LayoutTemplate>
        <div class="attract">
            <div id="itemPlaceholder" runat="server"></div>
        </div>
    </LayoutTemplate>
    <ItemTemplate>
        <div>
            <a id="lnk" href="<%# Eval("ProfileViewUrl") %>">
                <img src='<%# Eval("ImageUrl") %>' alt="" />
                <span class="block"><%# Eval("LoginName") %></span></a>
        </div>
    </ItemTemplate>
</asp:ListView>
    </asp:View>

    <asp:View ID="vwGenerous2" runat="server">
<asp:ListView ID="lvG2" runat="server">
    <LayoutTemplate>
        <div class="users2">
            <div id="itemPlaceholder" runat="server"></div>
            <div class="clear"></div>
        </div>
    </LayoutTemplate>
    <ItemTemplate>
        <div>
            <a id="lnk" href="<%# Eval("ProfileViewUrl") %>" title="<%# Eval("LoginName") %>">
                <img src='<%# Eval("ImageUrl") %>' alt="" />
                <span class="block"><%# Eval("LoginName") %></span></a>
        </div>
    </ItemTemplate>
</asp:ListView>
    </asp:View>


    <asp:View ID="vwAttractives2" runat="server">
<asp:ListView ID="lvA2" runat="server">
    <LayoutTemplate>
        <div class="attract2">
            <div id="itemPlaceholder" runat="server"></div>
            <div class="clear"></div>
        </div>
    </LayoutTemplate>
    <ItemTemplate>
        <div>
            <a id="lnk" href="<%# Eval("ProfileViewUrl") %>" title="<%# Eval("LoginName") %>">
                <img src='<%# Eval("ImageUrl") %>' alt="" />
                <span class="block"><%# Eval("LoginName") %></span></a>
        </div>
    </ItemTemplate>
</asp:ListView>
    </asp:View>

</asp:MultiView>




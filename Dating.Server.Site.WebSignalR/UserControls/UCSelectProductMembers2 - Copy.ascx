﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UCSelectProductMembers2.ascx.vb" Inherits="Dating.Server.Site.Web.UCSelectProductMembers2" %>
<%@ Register src="UCPrice2.ascx" tagname="UCPrice" tagprefix="uc1" %>

<link rel="stylesheet" href="/v1/css/payments2.css?v=12" />
<style type="text/css">
    .vat-info { font-size:12px;font-weight:bold;
                margin:40px 0px 0px;
                border-bottom: 1px solid #bababc;
                padding-bottom: 3px;padding-left: 11px;}
</style>
<script type="text/javascript">
    $(function () {
        $('.product-1.product-tab', '#products_2').click(function () { $('.pricecol-order-btn', '#products_2 .product-1')[0].click(); });
        $('.product-2.product-tab', '#products_2').click(function () { $('.pricecol-order-btn', '#products_2 .product-2')[0].click(); });
        $('.product-3.product-tab', '#products_2').click(function () { $('.pricecol-order-btn', '#products_2 .product-3')[0].click(); });
        $('.product-4.product-tab', '#products_2').click(function () { $('.pricecol-order-btn', '#products_2 .product-4')[0].click(); });
        $('.product-5.product-tab', '#products_2').click(function () { $('.pricecol-order-btn', '#products_2 .product-5')[0].click(); });
        $('img', '.sms-link-banner')
            .mouseenter(function () {
                var t=$('img', '.sms-link-banner');
                var src = t.attr('src');
                src = src.replace('.png', '-hover.png')
                t.attr('src', src);
            })
            .mouseleave(function () {
                var t = $('img', '.sms-link-banner');
                var src = t.attr('src');
                src = src.replace('-hover.png', '.png')
                t.attr('src', src);
            });
    })
    setTimeout(function () {
        new Image().src = '//cdn.goomena.com/Images2/payment/sms-en.png?v5';
        new Image().src = '//cdn.goomena.com/Images2/payment/sms-en-hover.png?v5';
        new Image().src = '//cdn.goomena.com/Images2/payment/sms-gr.png?v5';
        new Image().src = '//cdn.goomena.com/Images2/payment/sms-gr-hover.png?v5';
    }, 500);
</script>

<div id="payment_2" runat="server" clientidmode="Static">
    <div class="<%= If(MyBase.ShowSMSInput, "sms-form", "")%><%= If(MyBase.ShowSMSLink, "sms-link", "")%>">

    <asp:PlaceHolder ID="placeSMS" runat="server"></asp:PlaceHolder>
    <div>
        <asp:Label ID="lblPaymentsGuarantee" runat="server" Text=""></asp:Label>
        <div class="OfferArrow"></div>
    </div>
    <asp:Panel ID="pnlBonus" runat="server" CssClass="bonus-code">
        <div align="center">
            <asp:Label ID="lblBonusCode" runat="server" Text="" style="color:#25AAE1;"/>
            <asp:Label ID="lblBonusCodeSuccess" runat="server" ForeColor="Green" 
                ViewStateMode="Disabled"/>
            <div style="height:20px"></div>
            <asp:TextBox ID="txtBonusCode" runat="server" CssClass="textbox"></asp:TextBox>
            <asp:Button ID="btnBonusCredits" runat="server" Text="Υποβολή κωδικού" CssClass="button" />
            <div style="height:20px"></div>
            <asp:Label ID="lblBonusCodeError" runat="server" Text="" ForeColor="red" 
                ViewStateMode="Disabled"/>
        </div>
    </asp:Panel>

    <div id="products_2_wrapper">
        <div id="products_2">
            <uc1:UCPrice ID="UCPrice1" runat="server" View="vwMember" CssClass="product-1"  />
            <uc1:UCPrice ID="UCPrice2" runat="server" View="vwMember" CssClass="product-2" Discount="30"  />
            <uc1:UCPrice ID="UCPrice4" runat="server" View="vwMember" CssClass="product-4" visible="false" />
            <uc1:UCPrice ID="UCPrice3" runat="server" View="vwMember" CssClass="product-3" IsRecommended="true" Discount="45"   />
            <uc1:UCPrice ID="UCPrice5" runat="server" View="vwMember" CssClass="product-5" />
        </div>

        <div class="paymentsimgs">
            <img ID="imgPaymentSigns" runat="server" src="//cdn.goomena.com/goomena-kostas/pricing-members/payment2.png?v2" alt="Logos" />
        </div>
    </div>
    <asp:Label runat="server" ID="lbVatInfo">
    </asp:Label>

    <div class="after-vat-info"></div>   
    <asp:HyperLink ID="lnkSMSForm" runat="server" CssClass="sms-link-banner" ImageUrl="//cdn.goomena.com/Images2/payment/sms-en.png?v5" onclick="return Show_popupSMSInfo();">
    </asp:HyperLink>

    <asp:Literal runat="server" ID="lbfootText">
    </asp:Literal>

    </div>
</div>

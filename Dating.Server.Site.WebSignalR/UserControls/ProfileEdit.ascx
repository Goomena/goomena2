﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ProfileEdit.ascx.vb" Inherits="Dating.Server.Site.Web.ProfileEdit" %>
<%@ Register Assembly="DevExpress.Web.v13.1, Version=13.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>
<%@ Register Src="~/UserControls/DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<link rel="stylesheet" href="//cdn.goomena.com/CSS/jquery-ui-1.10.4.css" />
<link rel="stylesheet" href="/v1/CSS/profile-edit.css" />
<style type="text/css">
</style>
<script type="text/javascript" src="//cdn.goomena.com/Scripts/URI.js?v=1"></script>
<script type="text/javascript" src="/v1/Scripts/profile-edit.js?v1"></script>
<div id="pe">

	<!-- Form Box Wrapper -->
<script type="text/javascript">
    // <![CDATA[
    function showZip() {
        var zip = jQuery('#<%= msg_SelectRegion.ClientID %>');
        var region = jQuery('#<%= liLocationsRegion.ClientID %>');
        var city = jQuery('#<%= liLocationsCity.ClientID %>');

        region.hide()
        city.hide()

        var status = jQuery('#<%= hdfLocationStatus.ClientID %>');
        status.val("zip")
    }
    function showRegions() {
        var zip = jQuery('#<%= msg_SelectRegion.ClientID %>');
        var region = jQuery('#<%= liLocationsRegion.ClientID %>');
        var city = jQuery('#<%= liLocationsCity.ClientID %>');

        zip.hide()
        region.show()
        city.show()
        var status = jQuery('#<%= hdfLocationStatus.ClientID %>');
        status.val("region")
    }

    msg_HideAllLangs = '<%= CurrentPageData.GetCustomString("msg_HideAllLangs") %>';
    msg_ShowAllLangs = '<%= CurrentPageData.GetCustomString("msg_ShowAllLangs")%>';
    msg_ClickToExpand = '<%= CurrentPageData.GetCustomString("msg_ClickToExpand") %>';
    msg_ClickToCollapse = '<%= CurrentPageData.GetCustomString("msg_ClickToCollapse")%>';

    profileEditLoad();

</script>	
<div id="profile_edit">

    <h3 id="h3Dashboard" class="page-title">
        <asp:Literal ID="lblItemsName" runat="server" />
        <asp:LinkButton ID="lnkViewDescription" runat="server" CssClass="more_info2" OnClientClick="OnMoreInfoClick('[contentUrl]', this, '[headerText]');return false;" />
    </h3>
    <div class="clear"></div>

	<div id="section_your_location" class="pe_wrapper">
		<div class="pe_box">
            <h2 class="collapsibleHeader" title="<%= CurrentPageData.GetCustomString("msg_ClickToCollapse") %>"><a href="javascript:void(0);" class="togglebutton">-</a> <asp:Literal ID="msg_LocationHeader" runat="server" Text=""></asp:Literal></h2>
			<div class="pe_form">
                <asp:Literal ID="msg_LocationDesc" runat="server" Text=""></asp:Literal>
                <asp:HiddenField ID="hdfLocationStatus" runat="server" />
                            <dx:ASPxCallbackPanel ID="cbpnlZip" runat="server"
                                ClientInstanceName="cbpnlZip" 
                                CssFilePath="~/App_Themes/DevEx/{0}/styles.css" 
                    CssPostfix="DevEx" ShowLoadingPanel="False" ShowLoadingPanelImage="False">
                                <ClientSideEvents EndCallback="cbpnlZip_PerformCallback" CallbackError="cbpnlZip_PerformCallback" />
                                <LoadingPanelImage Url="~/App_Themes/DevEx/Web/Loading.gif">
                                </LoadingPanelImage>
                                <LoadingPanelStyle ImageSpacing="5px">
                                </LoadingPanelStyle>
                                <PanelCollection>
                                    <dx:PanelContent ID="PanelContent1" runat="server" SupportsDisabledAttribute="True">

                <ul class="list-form">
					<!-- location -->
					<li id="liLocationString" runat="server" clientidmode="Static">
						<asp:Label ID="msg_LocationText" runat="server" Text="" CssClass="peLabel lfloat" AssociatedControlID="lblLocation"/>
						<div class="form_right lfloat">
						    <asp:Label ID="lblLocation" runat="server" Text="" CssClass="locations_string lfloat"/>
                            <asp:HiddenField ID="hdfCity" runat="server" />
                            <asp:HiddenField ID="hdfRegion" runat="server" />
                            <asp:HiddenField ID="hdfCountry" runat="server" />
						</div>
                            <div class="clear"></div>
					</li>
					<!-- Country  -->
					<li>
                        <asp:Panel ID="pnlCountryErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblCountryErr" runat="server" Text=""></asp:Label>
                        </asp:Panel>
					
                        <asp:Label ID="msg_CountryText" runat="server" Text="" class="peLabel lfloat" AssociatedControlID="cbCountry"/>
						<div class="form_right lfloat">
                            <dx:ASPxComboBox ID="cbCountry" runat="server" EncodeHtml="False" 
                                IncrementalFilteringMode="StartsWith">
                                <ClientSideEvents SelectedIndexChanged="function(s, e) {  OnCountryChanged(s); }" EndCallback="function(s, e) {	HideLoading();}" />
        
                            </dx:ASPxComboBox>	
						</div>
                            <div class="clear"></div>
					</li>
					<!-- region  -->
					<li class="locations_city" id="liLocationsRegion" runat="server">
                        <asp:Panel ID="pnlRegionErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblRegionErr" runat="server" Text=""></asp:Label>
                        </asp:Panel>
                        <asp:Label ID="msg_RegionText" runat="server" Text="" class="peLabel lfloat" AssociatedControlID="cbRegion"/>
						<!--<span class="zip_list" style="position:relative;top:0;"><asp:LinkButton ID="msg_SelectZip" runat="server" OnClientClick="showZip();return false;"/></span>-->
						<div class="form_right lfloat">
                            <dx:ASPxComboBox ID="cbRegion" runat="server" Width="310px" Font-Size="14px" 
                                Height="18px" EncodeHtml="False" EnableSynchronization="False"  ClientInstanceName="cbRegion"
                                IncrementalFilteringMode="StartsWith"
                                TextField="region1" ValueField="region1">
                                <ClientSideEvents SelectedIndexChanged="function(s, e) {OnRegionChanged(s);}" EndCallback="function(s, e) {	HideLoading();}" />
                            </dx:ASPxComboBox>	
						    <%--<asp:SqlDataSource ID="sdsRegion" runat="server" 
                                ConnectionString="<%$ ConnectionStrings:AppDBconnectionString %>" 
                                SelectCommand="SELECT 
DISTINCT region1 
FROM SYS_GEO_GR 
 	with (nolock)
WHERE (countrycode = @countrycode) 
AND   ((countrycode=N'GR' and language = @language) or countrycode<>N'GR')">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="cbCountry" DefaultValue="GR" 
                                        Name="countrycode" PropertyName="Value" />
                                    <asp:Parameter DefaultValue="EN" Name="language" />
                                </SelectParameters>
                            </asp:SqlDataSource>--%>
						</div>
                            <div class="clear"></div>
					</li>
					<!-- city  -->
					<li class="locations_city" id="liLocationsCity" runat="server">
                        <asp:Panel ID="pnlCityErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblCityErr" runat="server" Text=""></asp:Label>
                        </asp:Panel>
						<asp:Label ID="msg_CityText" runat="server" Text="" CssClass="peLabel lfloat" AssociatedControlID="cbCity"/>
						<div class="form_right lfloat">
                            <dx:ASPxComboBox ID="cbCity" runat="server" Width="310px" Font-Size="14px" 
                                Height="18px" EncodeHtml="False" EnableSynchronization="False"  ClientInstanceName="cbCity"
                                IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" 
                                TextField="city" ValueField="city">
                                <clientsideevents SelectedIndexChanged="function(s, e) {
  OnCityChanged(s);
}" />
                            </dx:ASPxComboBox>	
                            <%--<asp:SqlDataSource ID="sdsCity" runat="server" 
                                ConnectionString="<%$ ConnectionStrings:AppDBconnectionString %>" 
                                SelectCommand="SELECT DISTINCT city 
FROM SYS_GEO_GR 
		with (nolock)
WHERE (region1 = @region1) 
AND (countrycode = @countrycode) 
AND ((countrycode=N'GR' and language = @language) or countrycode<>N'GR')">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="cbCountry" DefaultValue="GR" 
                                        Name="countrycode" PropertyName="Value" />
                                    <asp:ControlParameter ControlID="cbRegion" Name="region1" 
                                        PropertyName="Value" />
                                    <asp:Parameter DefaultValue="EN" Name="language" />
                                </SelectParameters>
                            </asp:SqlDataSource>--%>
                            <dx:ASPxTextBox ID="txtCity" runat="server" autocomplete="off" Visible="False" />
                         </div>
                            <div class="clear"></div>
					</li>
					<!-- zip  -->
					<li class="locations_zip" id="liLocationsZip" runat="server">

                        <asp:Panel ID="pnlZipErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblZipErr" runat="server" Text=""></asp:Label></asp:Panel>

                        <asp:Label ID="msg_ZipCodeText" runat="server" CssClass="peLabel lfloat" style="height:30px" for="zip" AssociatedControlID="msg_SelectRegion"/>
						<div class="form_right lfloat">
						    <span class="zip_list rfloat"><asp:LinkButton id="msg_SelectRegion" runat="server" CssClass="js" OnClientClick="showRegions();return false;" /></span>
                            <dx:ASPxTextBox ID="txtZip" runat="server" autocomplete="off" />
                            <asp:Label id="msg_ZipExample" runat="server" />
                            <div class="clear"></div>
						</div>
                        <div class="clear"></div>
					</li>
                    <li id="liSpokenLangs">
<asp:Label ID="msg_MySpokenLangs" runat="server" CssClass="peLabel lfloat" style="height:30px" AssociatedControlID="lvSpokenLangs"/>
<div class="form_right lfloat">
    <asp:ListView ID="lvSpokenLangs" runat="server" GroupItemCount="4">
        <ItemTemplate>
            <td><label><input type="checkbox" name="spoken_langs" value="<%# Eval("SpokenLangId")%>" <%# if(Eval("IsSelected")="1","checked","")%>><%# Eval("LangName")%></label></td>
        </ItemTemplate>
        <GroupSeparatorTemplate>
        </GroupSeparatorTemplate>
        <GroupTemplate>
            <tr class="hidden">
                <td ID="itemPlaceholder" runat="server"></td>
            </tr>
        </GroupTemplate>
        <ItemSeparatorTemplate></ItemSeparatorTemplate>
        <LayoutTemplate>
        <div id="spoken-langs-wrap">
            <table class="spoken-langs-list">
                <tr ID="groupPlaceholder" runat="server"></tr>
            </table>
        </div>
        </LayoutTemplate>
    </asp:ListView>
    <div>
        <span>
        <asp:HyperLink ID="lnkShowAllLangs" CssClass="show-all" runat="server" Text="Show all languages" EnableViewState="false" NavigateUrl="javascript:void(0);">
        </asp:HyperLink>
        </span>
    </div>
</div>
                        <div class="clear"></div>
                    </li>
					<li id="section_traveling" class="traveling">	
                    	<table cellpadding="0" cellspacing="0"><tr>
                            <td><div class="peLabel" style="height:30px;width:205px">&nbsp;</div></td>
                            <td><div class="form_right" style="width:410px">
                                <dx:ASPxCheckBox ID="chkTravelOnDate" runat="server" ClientInstanceName="chkTravelOnDate"
                                                Font-Size="14px" Height="18px" EncodeHtml="False">
                                                <ClientSideEvents ValueChanged="toggleTravelingPlaces" />
                                            </dx:ASPxCheckBox></div></td>
                                </tr>
                            </table>
                    </li>
                    <li id="liTravelingPlaces" class="<%= if(not chkTravelOnDate.Checked, "hidden","")%>">
<asp:Label ID="msg_WantVisitSelectPlaces" runat="server" CssClass="peLabel lfloat" style="height:30px" AssociatedControlID="lvWantVisit"/>
                        <div class="form_right lfloat">
                            <asp:ListView ID="lvWantVisit" runat="server">
                                <ItemTemplate>
                            <p><span><%# Eval("Descr")%></span><input type="hidden" value="<%# Eval("LocationID")%>"
                                class="locationId" id="location-<%# Eval("LocationID")%>" /><a
                                    href="#" class="remove_wants" title="Remove" data-location="<%# Eval("LocationID")%>"><img
                                        src="//cdn.goomena.com/Images/spacer10.png" border="0" alt="Remove" /></a></p>
                                </ItemTemplate>
                            </asp:ListView> 
                            <table id="wants_to_visit" class="dxeTextBoxSys dxeTextBox " style="border-collapse: collapse;" border="0" cellspacing="0" cellpadding="0">
		                        <tr>
			                        <td class="dxic" style="width: 100%;"><input name="location_visit" type="text" class="dxeEditArea dxeEditAreaSys" id="search_visit_location" 
                                                        placeholder="City, Region, Country" value="" /></td>
		                        </tr></table>
                        </div>
                        <div class="clear"></div>
                    </li>
                    <li id="liJobs">
<asp:Label ID="msg_WantJob" runat="server" CssClass="peLabel lfloat" style="height:30px" AssociatedControlID="lvJobs"/>
                        <div class="form_right lfloat">
                            <asp:ListView ID="lvJobs" runat="server">
                                <ItemTemplate>
                            <p><span><%# Eval("Descr")%></span><input type="hidden" value="<%# Eval("JobID")%>"
                                class="jobId" id="job-<%# Eval("JobID")%>" /><a
                                    href="#" class="remove_job" title="Remove" data-location="<%# Eval("JobID")%>"><img
                                        src="//cdn.goomena.com/Images/spacer10.png" border="0" alt="Remove" /></a></p>
                                </ItemTemplate>
                            </asp:ListView> 
                            <table id="selected_jobs" name="selected_jobs" class="dxeTextBoxSys dxeTextBox " style="border-collapse: collapse;" border="0" cellspacing="0" cellpadding="0">
		                        <tr>
			                        <td class="dxic" style="width: 100%;"><input name="job_select" type="text" class="dxeEditArea dxeEditAreaSys" id="job_select" 
                                                        placeholder="job title" value="" /></td>
		                        </tr></table>
                        </div>
                        <div class="clear"></div>
                    </li>
				</ul>
                                    </dx:PanelContent>
                                </PanelCollection>
                            </dx:ASPxCallbackPanel>
			<div class="clear"></div>
			</div>
		</div>
	</div>



	<div id="section_personal" class="pe_wrapper">
		<div class="pe_box">
            <h2 class="collapsibleHeader" title="<%= CurrentPageData.GetCustomString("msg_ClickToCollapse") %>"><a href="javascript:void(0);" class="togglebutton">-</a> <asp:Literal ID="msg_SomePersonalInfo" runat="server" Text=""></asp:Literal></h2><asp:Literal ID="msg_SomePersonalInfoDescr" runat="server" Text=""></asp:Literal><div class="pe_form">
				<ul class="list-form">
					<li id="liGenderContainer" runat="server">
                        <asp:Panel ID="pnlGenderErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblGenderErr" runat="server" Text=""></asp:Label></asp:Panel><label class="peLabel lfloat"><asp:Literal ID="msg_IAm" runat="server" Text="msg_IAm"/></label>
						<div class="form_right lfloat">
                            <dx:ASPxRadioButtonList ID="rblGender" runat="server" RepeatLayout="Flow" 
                                                RepeatDirection="Horizontal" Font-Size="14px" Height="18px" ClientInstanceName="RegisterGenderRadioButtonList">
                                            <Paddings PaddingLeft="0px" />
                                            <ValidationSettings ValidationGroup="RegisterGroup" ErrorDisplayMode="ImageWithTooltip" ErrorTextPosition="Left">
                                                <RequiredField ErrorText="" 
                                                    IsRequired="True" />
                                            </ValidationSettings>
                                            <Border BorderStyle="None" />
                                            <RadioButtonStyle BackgroundImage-HorizontalPosition="center" HorizontalAlign="Center"></RadioButtonStyle>
                                            </dx:ASPxRadioButtonList>
						</div>
                            <div class="clear"></div>
					</li>
					<%--<li id="liWhoIsContainer" runat="server">
                        <asp:Panel ID="pnlAccountTypeErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblAccountTypeErr" runat="server" Text=""></asp:Label></asp:Panel>
						<asp:Label ID="msg_WhoIs" runat="server" Text="" CssClass="peLabel lfloat" AssociatedControlID="cbAccountType"/>
						<div class="form_right lfloat">
                            <dx:ASPxComboBox ID="cbAccountType" runat="server" ClientInstanceName="RegisterAccountTypeComboBox" EncodeHtml="False">
                                            <ClientSideEvents Validation="function(s, e) {
if(s.GetSelectedIndex()==0) e.isValid=false;
}" />
                                            <ValidationSettings ValidationGroup="RegisterGroup" ErrorDisplayMode="ImageWithTooltip" ErrorTextPosition="Left"
                                                ErrorText="">
                                                <RequiredField ErrorText="" 
                                                    IsRequired="True" />
                                            </ValidationSettings>
                                            </dx:ASPxComboBox>
						</div>
					</li>--%>
					<li id="liBirthdayContainer" runat="server">
                        <asp:Panel ID="pnlBirthdateErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblBirthdateErr" runat="server" Text=""></asp:Label></asp:Panel>
						<asp:Label ID="msg_MyBirthdate" runat="server" Text="" CssClass="peLabel lfloat" AssociatedControlID="ctlDate"/>
						<div class="form_right lfloat">
                            <uc1:DateControl ID="ctlDate" runat="server" />
						</div>
                            <div class="clear"></div>
					</li>
					<li>
                        <asp:Panel ID="pnlFirstNameErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblFirstNameErr" runat="server" Text=""></asp:Label></asp:Panel>
						<asp:Label ID="msg_FirstName" runat="server" Text="" CssClass="peLabel lfloat" AssociatedControlID="txtFirstName"/>
						<div class="form_right lfloat">
                            <dx:ASPxTextBox ID="txtFirstName" runat="server" autocomplete="off" />
						</div>
                            <div class="clear"></div>
					</li>
					<li>
                        <asp:Panel ID="pnlLastNameErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblLastNameErr" runat="server" Text=""></asp:Label></asp:Panel>
						<asp:Label ID="msg_LastName" runat="server" Text="" CssClass="peLabel lfloat" AssociatedControlID="txtLastName"/>
						<div class="form_right lfloat">
                            <dx:ASPxTextBox ID="txtLastName" runat="server" autocomplete="off" />
						</div>
                            <div class="clear"></div>
					</li>
					<li style="display: none">
                        <asp:Panel ID="pnlCityAreaErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblCityAreaErr" runat="server" Text=""></asp:Label></asp:Panel>
						<asp:Label ID="msg_CityArea" runat="server" Text="" CssClass="peLabel lfloat" AssociatedControlID="txtCityArea"/>
						<div class="form_right lfloat">
                            <dx:ASPxTextBox ID="txtCityArea" runat="server" autocomplete="off" />
						</div>
                            <div class="clear"></div>
					</li>
					<li>
                        <asp:Panel ID="pnlAddressErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblAddressErr" runat="server" Text=""></asp:Label></asp:Panel>
						<asp:Label ID="msg_Address" runat="server" Text="" CssClass="peLabel lfloat" AssociatedControlID="txtAddress"/>
						<div class="form_right lfloat" style="position:relative;">
                            <dx:ASPxTextBox ID="txtAddress" runat="server" autocomplete="off">
                                <ClientSideEvents Init="checkAddressText" LostFocus="checkAddressText" />
                            </dx:ASPxTextBox>
                            <div class="go-link"><a href="#" id="lnkAddressUrl" class="hidden" target="_blank">&nbsp;Go&nbsp;</a></div>
						</div>
                            <div class="clear"></div>
					</li>
					<li>
                        <asp:Panel ID="pnleMailErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lbleMailErr" runat="server" Text=""></asp:Label></asp:Panel>
						<asp:Label ID="msg_eMail" runat="server" Text="" CssClass="peLabel lfloat" AssociatedControlID="txteMail"/>
						<div class="form_right lfloat">
                            <dx:ASPxTextBox ID="txteMail" runat="server" autocomplete="off" />
						</div>
                            <div class="clear"></div>
					</li>
					<li>
                        <asp:Panel ID="pnlTelephoneErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblTelephoneErr" runat="server" Text=""></asp:Label></asp:Panel>
						<asp:Label ID="msg_Telephone" runat="server" Text="" CssClass="peLabel lfloat" AssociatedControlID="txtTelephone"/>
						<div class="form_right lfloat">
                            <dx:ASPxTextBox ID="txtTelephone" runat="server" autocomplete="off" />
						</div>
                            <div class="clear"></div>
					</li>
					<li>
                        <asp:Panel ID="pnlCellularErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblCellularErr" runat="server" Text=""></asp:Label></asp:Panel>
						<asp:Label ID="msg_Cellular" runat="server" Text="" CssClass="peLabel lfloat" AssociatedControlID="txtCellular"/>
						<div class="form_right lfloat">
                            <dx:ASPxTextBox ID="txtCellular" runat="server" autocomplete="off" />
						</div>
                            <div class="clear"></div>
					</li>
				</ul>
			<div class="clear"></div>
			</div>
		</div>
	</div>


    <div id="section_about" class="pe_wrapper">
		<div class="pe_box">
			<h2 class="collapsibleHeader" title="<%= CurrentPageData.GetCustomString("msg_ClickToCollapse") %>"><a href="javascript:void(0);" class="togglebutton">-</a> <asp:Literal ID="msg_YrSlfTitle" runat="server"/></h2>
			<div class="pe_form">
                <asp:Label ID="msg_YrSlfnotice" runat="server" Text=""></asp:Label>
				<ul class="list-form">
					<li>
                        <asp:Panel ID="pnlHeadingErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblHeadingErr" runat="server" Text=""></asp:Label></asp:Panel>
						<asp:Label ID="msg_YrSlfHeadingTitle" runat="server" Text="" CssClass="peLabel lfloat" AssociatedControlID="txtYrSlfHeading"/>
						<div class="form_right lfloat">
                            <dx:ASPxTextBox ID="txtYrSlfHeading" runat="server" CssClass="text" Text=""/>
						</div>
                            <div class="clear"></div>
					</li>
					<li>
                        <asp:Panel ID="pnlDescErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblDescErr" runat="server" Text=""></asp:Label></asp:Panel>
						<asp:Label ID="msg_YrSlfDescTitle" runat="server" Text="" CssClass="peLabel lfloat" AssociatedControlID="txtYrSlfDesc"/>
						<div class="form_right lfloat">
                            <asp:TextBox ID="txtYrSlfDesc" runat="server" TextMode="MultiLine" Columns="50" Rows="10"></asp:TextBox></div>
                            <div class="clear"></div>
                            </li>
                    <li>
                        <asp:Panel ID="pnlFirstDateExptErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblFirstDateExptErr" runat="server" Text=""></asp:Label></asp:Panel>
						<asp:Label ID="msg_YrSlfFirstDate" runat="server" Text="" CssClass="peLabel lfloat" AssociatedControlID="txtYrSlfSeeking" style="line-height:16px;"/>
						<div class="form_right lfloat">
                            <asp:TextBox ID="txtYrSlfSeeking" runat="server" TextMode="MultiLine" Columns="50" Rows="10"></asp:TextBox></div>
                            <div class="clear"></div>
                            </li>
                            </ul>
                            <div class="clear"></div>
			</div>
		</div>
	</div>


    <div id="section_other_details" class="pe_wrapper">
		<div class="pe_box">
			<h2 class="collapsibleHeader" title="<%= CurrentPageData.GetCustomString("msg_ClickToCollapse") %>"><a href="javascript:void(0);" class="togglebutton">-</a> <asp:Literal ID="msg_OtherTitle" runat="server"/></h2>
			<div class="pe_form">
				<ul class="list-form">
					<li>
                        <asp:Panel ID="pnlEducationErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblEducationErr" runat="server" Text=""></asp:Label></asp:Panel>
						<asp:Label ID="msg_OtherEducat" runat="server" Text="" CssClass="peLabel lfloat" AssociatedControlID="cbEducation"/>
						<div class="form_right lfloat">
                            <dx:ASPxComboBox ID="cbEducation" runat="server" Width="310px" Font-Size="14px" 
                                Height="18px" EncodeHtml="False">
                            </dx:ASPxComboBox>	
                        </div>
                            <div class="clear"></div>
					</li>
					<li runat="server" id="liAnnualIncome">
                        <asp:Panel ID="pnlIncomeErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblIncomeErr" runat="server" Text=""></asp:Label></asp:Panel>
						<asp:Label ID="msg_OtherAnnual" runat="server" Text="" CssClass="peLabel lfloat" AssociatedControlID="cbIncome"/>
						<div class="form_right lfloat">
                            <dx:ASPxComboBox ID="cbIncome" runat="server" Width="310px" Font-Size="14px" 
                                Height="18px" EncodeHtml="False">
                            </dx:ASPxComboBox>	
						</div>
                            <div class="clear"></div>
					</li>
					<%--
                    <li runat="server" id="liNetWorth" visible="false">
                        <asp:Panel ID="pnlNetWorthErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblNetWorthErr" runat="server" Text=""></asp:Label></asp:Panel>
						<asp:Label ID="msg_OtherNetWorth" runat="server" Text="" CssClass="peLabel lfloat" AssociatedControlID="cbNetWorth"/>
						<div class="form_right lfloat">
                            <dx:ASPxComboBox ID="cbNetWorth" runat="server" Width="310px" Font-Size="14px" 
                                Height="18px" EncodeHtml="False">
                            </dx:ASPxComboBox>	
						</div>
					</li>
                    --%>
					<li>
                        <asp:Panel ID="pnlOccupationErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblOccupationErr" runat="server" Text=""></asp:Label></asp:Panel>
						<asp:Label ID="msg_OtherOccupation" runat="server" Text="" CssClass="peLabel lfloat" AssociatedControlID="txtOccupation"/>
						<div class="form_right lfloat">
                            <dx:ASPxTextBox ID="txtOccupation" runat="server" CssClass="text" Text=""/>
						</div>
                            <div class="clear"></div>
					</li>
				</ul>
			<div class="clear"></div>
			</div>
		</div>
	</div>


    <div id="section_personal_info" class="pe_wrapper">
		<div class="pe_box">
			<h2 class="collapsibleHeader" title="<%= CurrentPageData.GetCustomString("msg_ClickToCollapse") %>"><a href="javascript:void(0);" class="togglebutton">-</a> <asp:Literal ID="msg_PersonalTitle" runat="server"/></h2>
			<div class="pe_form">
				<ul class="list-form">
					<li>
                        <asp:Panel ID="pnlHeightErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblHeightErr" runat="server" Text=""></asp:Label></asp:Panel>
						<asp:Label ID="msg_PersonalHeight" runat="server" Text="" CssClass="peLabel lfloat" AssociatedControlID="cbHeight"/>
						<div class="form_right lfloat">
                            <dx:ASPxComboBox ID="cbHeight" runat="server" EncodeHtml="False" Width="310px" Font-Size="14px" Height="18px">
                            </dx:ASPxComboBox>	
						</div>
                            <div class="clear"></div>
					</li>
					<li>
                        <asp:Panel ID="pnlBodyTypeErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblBodyTypeErr" runat="server" Text=""></asp:Label></asp:Panel>
						<asp:Label ID="msg_PersonalBodyType" runat="server" Text="" CssClass="peLabel lfloat" AssociatedControlID="cbBodyType"/>
						<div class="form_right lfloat">
                            <dx:ASPxComboBox ID="cbBodyType" runat="server" Width="310px" Font-Size="14px" 
                                Height="18px" EncodeHtml="False">
                            </dx:ASPxComboBox>	
						</div>
                            <div class="clear"></div>
					</li>
					<li>
                        <asp:Panel ID="pnlEyeColorErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblEyeColorErr" runat="server" Text=""></asp:Label></asp:Panel>
						<asp:Label ID="msg_PersonalEyeClr" runat="server" Text="" CssClass="peLabel lfloat" AssociatedControlID="cbEyeColor"/>
						<div class="form_right lfloat">
                            <dx:ASPxComboBox ID="cbEyeColor" runat="server" Width="310px" Font-Size="14px" 
                                Height="18px" EncodeHtml="False">
                            </dx:ASPxComboBox>	
						</div>
                            <div class="clear"></div>
					</li>
					<li>
                        <asp:Panel ID="pnlHairClrErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblHairClrErr" runat="server" Text=""></asp:Label></asp:Panel>
						<asp:Label ID="msg_PersonalHairClr" runat="server" Text="" CssClass="peLabel lfloat" AssociatedControlID="cbHairClr"/>
						<div class="form_right lfloat">
                            <dx:ASPxComboBox ID="cbHairClr" runat="server" Width="310px" Font-Size="14px" 
                                Height="18px" EncodeHtml="False">
                            </dx:ASPxComboBox>	
						</div>
                            <div class="clear"></div>
					</li>
					<li>
                        <asp:Panel ID="pnlChildrenErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblChildrenErr" runat="server" Text=""></asp:Label></asp:Panel>
						<asp:Label ID="msg_PersonalChildren" runat="server" Text="" CssClass="peLabel lfloat" AssociatedControlID="cbChildren"/>
						<div class="form_right lfloat">
                            <dx:ASPxComboBox ID="cbChildren" runat="server" Width="310px" Font-Size="14px" 
                                Height="18px" EncodeHtml="False">
                            </dx:ASPxComboBox>	
						</div>
                            <div class="clear"></div>
					</li>
					<li>
                        <asp:Panel ID="pnlEthnicityErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblEthnicityErr" runat="server" Text=""></asp:Label></asp:Panel>
						<asp:Label ID="msg_PersonalEthnicity" runat="server" Text="" CssClass="peLabel lfloat" AssociatedControlID="cbEthnicity"/>
						<div class="form_right lfloat">
                            <dx:ASPxComboBox ID="cbEthnicity" runat="server" Width="310px" Font-Size="14px" 
                                Height="18px" EncodeHtml="False">
                            </dx:ASPxComboBox>	
						</div>
                            <div class="clear"></div>
					</li>
					<li>
                        <asp:Panel ID="pnlReligionErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblReligionErr" runat="server" Text=""></asp:Label></asp:Panel>
						<asp:Label ID="msg_PersonalReligion" runat="server" Text="" CssClass="peLabel lfloat" AssociatedControlID="cbReligion"/>
						<div class="form_right lfloat">
                            <dx:ASPxComboBox ID="cbReligion" runat="server" Width="310px" Font-Size="14px" 
                                Height="18px" EncodeHtml="False">
                            </dx:ASPxComboBox>	
						</div>
                            <div class="clear"></div>
					</li>
					<li>
                        <asp:Panel ID="pnlSmokingErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblSmokingErr" runat="server" Text=""></asp:Label></asp:Panel>
						<asp:Label ID="msg_PersonalSmokingHabit" runat="server" Text="" CssClass="peLabel lfloat" AssociatedControlID="cbSmoking"/>
						<div class="form_right lfloat">
                            <dx:ASPxComboBox ID="cbSmoking" runat="server" Width="310px" Font-Size="14px" 
                                Height="18px" EncodeHtml="False">
                            </dx:ASPxComboBox>	
						</div>
                            <div class="clear"></div>
					</li>
					<li>
                        <asp:Panel ID="pnlDrinkingErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblDrinkingErr" runat="server" Text=""></asp:Label></asp:Panel>
						<asp:Label ID="msg_PersonalDrinkingHabit" runat="server" Text="" CssClass="peLabel lfloat" AssociatedControlID="cbDrinking"/>
						<div class="form_right lfloat">
                            <dx:ASPxComboBox ID="cbDrinking" runat="server" Width="310px" Font-Size="14px" 
                                Height="18px" EncodeHtml="False">
                            </dx:ASPxComboBox>	
						</div>
                            <div class="clear"></div>
					</li>
					<li id="liBreastSize" runat="server" Visible="False">
                        <asp:Panel ID="pnlBreastErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblBreastErr" runat="server" Text=""></asp:Label></asp:Panel>
						<asp:Label ID="msg_BreastSize" runat="server" Text="" CssClass="peLabel lfloat" AssociatedControlID="cbBreastSize"/>
						<div class="form_right lfloat">
                            <dx:ASPxComboBox ID="cbBreastSize" runat="server" Width="310px" Font-Size="14px" 
                                Height="18px" EncodeHtml="False">
                            </dx:ASPxComboBox>	
						</div>
                            <div class="clear"></div>
					</li>
				</ul>
			<div class="clear"></div>
			</div>
		</div>
	</div>



    <div id="section_looking_for" class="pe_wrapper">
		<div class="pe_box">
			<h2 class="collapsibleHeader" title="<%= CurrentPageData.GetCustomString("msg_ClickToCollapse") %>"><a href="javascript:void(0);" class="togglebutton">-</a> <asp:Literal ID="msg_WAYLFTitle" runat="server"/></h2>
			<div class="pe_form">
				<ul class="list-form">
					<li>
                        <asp:Panel ID="pnlErrorSelectLookingForMaleFemale" runat="server" CssClass="alert alert-danger" Visible="False">
                            <dx:ASPxLabel ID="lblErrorSelectLookingForMaleFemale" runat="server" ForeColor="red" Font-Size="14px" >
                            </dx:ASPxLabel>
                        </asp:Panel>
					
						<asp:Label ID="msg_WAYLFMeet" runat="server" Text="" CssClass="peLabel lfloat" AssociatedControlID="chkLookingMale" Visible="false"/>
						<div class="form_right lfloat">
							<p class="form_cluster">
                                <dx:ASPxCheckBox ID="chkLookingMale" runat="server" Text="Male"  EncodeHtml="False" Visible="false">
                                </dx:ASPxCheckBox>
							</p>
							<p class="form_cluster">
                                <dx:ASPxCheckBox ID="chkLookingFemale" runat="server" Text="Female" EncodeHtml="False" Visible="false">
                                </dx:ASPxCheckBox>
							</p>
						</div>
                            <div class="clear"></div>
					</li>
					<li>
                        <asp:Panel ID="pnlRelationshipErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblRelationshipErr" runat="server" Text=""></asp:Label></asp:Panel>
						<asp:Label ID="msg_WAYLFRelStatus" runat="server" Text="" CssClass="peLabel lfloat" AssociatedControlID="cbRelationshipStatus"/>
						<div class="form_right lfloat">
                            <dx:ASPxComboBox ID="cbRelationshipStatus" runat="server" Width="310px" 
                                Font-Size="14px" Height="18px" EncodeHtml="False">
                            </dx:ASPxComboBox>	
						</div>
                            <div class="clear"></div>
					</li>
					<li>
                        <asp:Panel ID="pnlTypeOfDateErr" runat="server" CssClass="alert alert-danger" Visible="False">
                            <asp:Label ID="lblTypeOfDateErr" runat="server" Text=""></asp:Label></asp:Panel>
						<asp:Label ID="msg_WAYLFTypeDate" runat="server" Text="" CssClass="peLabel lfloat" AssociatedControlID="lvTypeOfDate"/>
						<div class="form_right lfloat">
                            <asp:ListView ID="lvTypeOfDate" runat="server">
                                <LayoutTemplate>
                                <p id="itemPlaceholder" runat="server"></p>
                                </LayoutTemplate>
                                <ItemTemplate>
                                <p class="form_cluster">
                                    <asp:HiddenField ID="hdf" runat="server" Visible="false" Value='<%# Eval("TypeOfDatingId") %>' />
                                    <dx:ASPxCheckBox ID="chk" runat="server" Checked='<%# Eval("TypeOfDateChecked") %>' Text='<%# Eval("TypeOfDateText") %>' EncodeHtml="False"></dx:ASPxCheckBox>
								</p>
                                </ItemTemplate>
                            </asp:ListView>
						</div>
                            <div class="clear"></div>
					</li>
				</ul>
			<div class="clear"></div>
			</div>
		</div>
	</div>


	<div class="clear"></div>
	<div class="form-actions">
        <dx:ASPxButton ID="btnProfileUpd" runat="server" 
            CssClass="btn btn-large btn-primary" Text="" 
            ValidationGroup="RegisterGroup" Native="True" EncodeHtml="False" 
            UseSubmitBehavior="False"  />
	</div>
	<!-- End Form Box Wrapper -->
    
    <div class="left-side-inn">
        <asp:HyperLink ID="lnkBack" NavigateUrl="~/Members/Profile.aspx" runat="server" onClick="ShowLoading();" Text="lnkBack">
        </asp:HyperLink>
    </div>

</div>
</div>


<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?language=en&sensor=false&v=3&libraries=places"></script>
<script type="text/javascript" src="/v1/Scripts/country-autocomplete.js"></script>
<script type="text/javascript" src="/v1/Scripts/jobs-autocomlete.js"></script>


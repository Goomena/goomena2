﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PublicSearch.ascx.vb" 
    Inherits="Dating.Server.Site.Web.PublicSearch" %>


<asp:Repeater ID="rptPubSrch" runat="server">
<ItemTemplate>

    <div class="result-item">
        <div class="pr-photo"><%--HttpUtility.UrlEncode(Eval("OtherMemberLoginName")).Replace("+", " ") --%>
            <asp:HyperLink ID="lnk1" runat="server" NavigateUrl='<%# "~/profile/" & Dating.Server.Site.Web.UrlUtils.GetValidRoutingProfileURI(HttpUtility.UrlEncode(Eval("OtherMemberLoginName")))%>' onclick="ShowLoading();"><img alt="" src="<%# Eval("OtherMemberImageUrl")%>" class="image-1"/></asp:HyperLink>
        </div>
        <div class="result-inner">
            <div class="pr-details"><%--HttpUtility.UrlEncode(Eval("OtherMemberLoginName")).Replace("+", " ")--%>
                <div class="pr-login"><asp:HyperLink ID="lnk2" runat="server" NavigateUrl='<%# "~/profile/" & Dating.Server.Site.Web.UrlUtils.GetValidRoutingProfileURI(HttpUtility.UrlEncode(Eval("OtherMemberLoginName")))%>' onclick="ShowLoading();"><%# Eval("OtherMemberLoginName")%></asp:HyperLink></div>
                <div class="pr-details-bottom">
                    <%--<div class="pr-age"><asp:Literal ID="lblAge" runat="server">29 ετών</asp:Literal></div>--%>
                    <div class="pr-other"><%# MyBase.WriteSearch_MemberInfo(Container.DataItem)%></div>
                </div>
            </div>
            <div class="pr-actions">
                <%--<div class="act-help"><asp:LinkButton ID="lnkWhatIs" runat="server" OnClientClick="OnMoreInfoClick('[contentUrl]',this,'[headerText]');return false;"/></div>--%>
                <div class="act-other"><asp:HyperLink ID="lnkOther" runat="server" CssClass="shadow"  NavigateUrl="~/Register.aspx"  onclick="ShowLoading();"><%= Me.MykeyStrings.ActionsText%></div></asp:HyperLink>
                <div class="act-favore"><asp:HyperLink ID="lnkFav" runat="server" CssClass="shadow"  NavigateUrl="~/Register.aspx"  onclick="ShowLoading();"><%= Me.MykeyStrings.FavoriteText%></div></asp:HyperLink>
                <div class="act-like"><asp:HyperLink ID="lnkLike" runat="server" CssClass="shadow"  NavigateUrl="~/Register.aspx"  onclick="ShowLoading();"><%= Me.MykeyStrings.WinkText%></div></asp:HyperLink>
                <div class="act-distance"><img src="//cdn.goomena.com/Images2/pub-search/car.png" alt=""/></div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
</ItemTemplate>
</asp:Repeater>

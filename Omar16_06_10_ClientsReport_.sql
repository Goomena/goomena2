USE [CMSGoomena] 

go 

BEGIN 
    DECLARE @StartDate AS DATETIME='20130101', 
            @EndDate   AS DATETIME='20160701' 
    --declare @TEMP table (Months as nvarchar(6)) 
    DECLARE @TEMP TABLE 
      ( 
         months INT 
      ) 
    DECLARE @tempStart AS DATETIME=Dateadd(month, -1, @StartDate) 
    DECLARE @c AS INT=(SELECT Datediff(month, @StartDate, @EndDate)) 
    DECLARE @i INT=0; 

    WHILE @i < @c 
      BEGIN 
          --set @tempStart =DATEADD(month,1,@tempStart) 
          INSERT INTO @TEMP 
                      (months) 
          VALUES     (@i + 1) 

          SET @i=@i + 1 
      END 

    DECLARE @SQLQuery AS NVARCHAR(max) 
    DECLARE @PivotColumns AS NVARCHAR(max) 
    DECLARE @SelectPivotColumns AS NVARCHAR(max) 

    SELECT @PivotColumns = COALESCE(@PivotColumns + ',', '') 
                           + Quotename(months) 
    FROM   @TEMP 

    SELECT @SelectPivotColumns = COALESCE(@SelectPivotColumns + ',', '') 
                                 + 'Isnull(' + Quotename(months) + ',0) as ' 
                                 + Quotename(months) 
    FROM   @TEMP 

    --Create the dynamic query with all the values for RMonth, ' +   @PivotColumns + '--,prs.CountPr   
    --pivot column at runtime 
    SET @SQLQuery = 'declare @StartDate as datetime=''' +  convert(VARCHAR(8), @StartDate,112 )+''',
						   @EndDate as datetime=''' + convert(VARCHAR(8), @EndDate,112 )+''' 
SELECT    pv.rmonth as ClientGroup, prs.RegisteredCustomers ,prs2.PayedCustomers,' + @SelectPivotColumns+ '
          
FROM      ( 
                 SELECT * 
                 FROM   ( 
                               SELECT t.rmonth, 
                                      t.money, 
                                      t.monthcount 
                               FROM   ( 
                                               SELECT   monthcount=Row_number() OVER( partition BY rmonth ORDER BY [Cmonth] ASC),
                                                        [Cmonth], 
                                                        Sum([DebitProductPrice]) AS [Money] ,
                                                        rmonth 
                                               FROM    ( 
                                                                  SELECT    [Cmonth]=substring(convert(VARCHAR(8), a.date_time,112 ),1,6) , 
                                                                            a.[DebitProductPrice] , 
                                                                            rmonth=substring(convert(VARCHAR(8), datetimetoregister,112 ),1,6)
                                                                  FROM      [EUS_CustomerTransaction] AS a
                                                                  LEFT JOIN eus_profiles              AS p
                                                                  ON        a.customerid =p.profileid
                                                                  WHERE     customerid IN 
                                                                            ( 
                                                                                   SELECT profileid
                                                                                   FROM   eus_profiles
                                                                                   WHERE  ismaster=1
                                                                                   AND    genderid=1
                                                                                   AND    datetimetoregister>=@StartDate
                                                                                   AND    datetimetoregister<@EndDate)
                                                                  AND       a.[DebitAmount]>0
                                                                  AND       a.date_time>=@StartDate
                                                                  AND       a.date_time<@EndDate) AS t1
                                               GROUP BY rmonth, 
                                                        [Cmonth]) AS t ) AS newtb PIVOT( sum([Money]) FOR monthcount IN (' + @PivotColumns + ')) AS p) AS pv
LEFT JOIN 
          ( 
                          SELECT DISTINCT [RegisteredCustomers]=count(o.profileid ), 
                                          o.rmonth 
                          FROM            ( 
                                                 SELECT profileid, 
                                                        rmonth=substring(convert(VARCHAR(8), datetimetoregister,112 ),1,6) 
                                                 FROM   eus_profiles AS p 
                                                 WHERE  ismaster=1 
                                                 AND    genderid=1 
                                                 AND    datetimetoregister>=@StartDate 
                                                 AND    datetimetoregister<@EndDate) AS o 
                          GROUP BY        rmonth ) AS prs 
ON        pv.rmonth=prs.rmonth
LEFT JOIN 
          ( 
                          SELECT DISTINCT [PayedCustomers]=count(o.profileid ), 
                                          o.rmonth 
                          FROM            ( 
                                                 SELECT profileid, 
                                                        rmonth=substring(convert(VARCHAR(8), datetimetoregister,112 ),1,6) 
                                                 FROM   eus_profiles AS p 
                                                 WHERE  ismaster=1 
                                                 AND    genderid=1 
                                                 AND    datetimetoregister>=@StartDate 
                                                 AND    datetimetoregister<@EndDate
												 and exists( select [CustomerTransactionID] from EUS_CustomerTransaction where customerid=p.ProfileID and DebitProductPrice>0)) AS o 
                          GROUP BY        rmonth ) AS prs2 
ON        pv.rmonth=prs2.rmonth
order by ClientGroup '
    

    EXEC Sp_executesql 
      @SQLQuery 
END 
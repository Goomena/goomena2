﻿Imports Dating.Server.Core.DLL

Imports Dating.Server.Datasets.DLL

Public Class mobrejectitem
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not IsPostBack) Then
            Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_RejectingReasons, "US", "RejectingReasonsId", cbReason1, True)
            cbReason1.Items(0).Selected = True
        End If
    End Sub

    Protected Sub lnkOk_Click(sender As Object, e As EventArgs) Handles lnkOk.Click
        Try
            _RejectProfile(cbReason1.SelectedItem.Text, txReasonDescr1.Text)
        Catch ex As Exception

        End Try
        Response.Redirect("default.aspx")
    End Sub

    Protected Sub lnkCancel_Click(sender As Object, e As EventArgs) Handles lnkCancel.Click
        Response.Redirect("mobitem.aspx?p=" & Request.QueryString("p"))
    End Sub


    Private Sub _RejectProfile(reasonStr As String, reasonTextStr As String)
        Try

            Dim members As DSMembers = DataHelpers.GetEUS_Profiles_ByLoginName(Request.QueryString("p"))

            'Dim masterRow As EUS_ProfilesRow = members.EUS_Profiles.Rows(0)
            'masterRow.Status = ProfileStatusEnum.Rejected

            'DataHelpers.UpdateEUS_Profiles(members)

            If (members.EUS_Profiles.Rows.Count = 1) Then
                Dim masterRow As DSMembers.EUS_ProfilesRow = members.EUS_Profiles.Rows(0)
                AdminActions.RejectNewProfile(members)

                Dim text =
                    "Your subscription to Goomena.com rejected." & vbCrLf & vbCrLf & _
                    reasonStr & vbCrLf & vbCrLf & _
                    reasonTextStr

                'clsMyMail.SendMail(masterRow.eMail, "Your subscription to Goomena.com rejected.", text)
                clsMyMail.SendMailFromQueue(masterRow.eMail, "Your subscription to Goomena.com rejected.", text, masterRow.ProfileID, "Mobile Reject New Profile", False, Nothing, Nothing)
            Else
                Dim masterRow As DSMembers.EUS_ProfilesRow = members.EUS_Profiles.Rows(0)
                AdminActions.RejectExistingProfile(members)

                Dim text =
                    "Your changes to Goomena.com rejected." & vbCrLf & vbCrLf & _
                    reasonStr & vbCrLf & vbCrLf & _
                    reasonTextStr

                'clsMyMail.SendMail(masterRow.eMail, "Your changes to Goomena.com rejected.", text)
                clsMyMail.SendMailFromQueue(masterRow.eMail, "Your changes to Goomena.com rejected.", text, masterRow.ProfileID, "Mobile Reject Changed Profile", False, Nothing, Nothing)
            End If

        Catch ex As Exception
            Throw
        End Try
    End Sub


    Protected Sub cbReason_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbReason1.SelectedIndexChanged
        Try
            txReasonDescr1.Text = (From itm In Lists.gDSLists.EUS_LISTS_RejectingReasons
                                 Where itm.RejectingReasonsId = cbReason1.SelectedItem.Value
                                 Select itm.TextUS).FirstOrDefault()
        Catch ex As Exception

        End Try
    End Sub
End Class
Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Collections.Generic
Imports System.Configuration
Imports System.IO
Imports System.Xml
Imports System.Xml.Schema
Imports System.Globalization
Imports System.Text
Imports System.Text.RegularExpressions
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Reflection
Imports DevExpress.Web.ASPxHeadline
Imports DevExpress.Web.ASPxSiteMapControl
Imports DevExpress.Web.ASPxClasses
Imports DevExpress.Web.ASPxClasses.Internal
Imports Dating.Server.Core.DLL
Imports Dating.Server.Datasets.DLL


Partial Public Class BasePage
    Inherits System.Web.UI.Page
    Implements ILanguageDependableContent


    Public ReadOnly Property MasterProfileId As Integer
        Get
            Return Me.Session("ProfileID")
        End Get
    End Property


    Protected ReadOnly Property MirrorProfileId As Integer
        Get
            Return Me.Session("MirrorProfileID")
        End Get
    End Property


    Dim _sessVars As clsSessionVariables
    Protected ReadOnly Property SessionVariables As clsSessionVariables
        Get
            If (_sessVars Is Nothing) Then _sessVars = clsSessionVariables.GetCurrent()
            Return _sessVars
        End Get
    End Property


    Private _globalStrings As clsPageData
    Public ReadOnly Property globalStrings As clsPageData
        Get
            If (_globalStrings Is Nothing) Then
                _globalStrings = New clsPageData("GlobalStrings", HttpContext.Current)
            End If
            Return _globalStrings
        End Get
    End Property



    Dim _CMSDBDataContext As CMSDBDataContext
    Protected ReadOnly Property CMSDBDataContext As CMSDBDataContext
        Get
            If (_CMSDBDataContext Is Nothing) Then
                _CMSDBDataContext = New CMSDBDataContext(ConfigurationManager.ConnectionStrings("AppDBconnectionString").ConnectionString)
            End If
            Return _CMSDBDataContext
        End Get
    End Property



    Dim _mineMasterProfile As EUS_Profile
    Protected Function GetCurrentMasterProfile() As EUS_Profile
        Try

            If (_mineMasterProfile Is Nothing) Then
                _mineMasterProfile = Me.CMSDBDataContext.EUS_Profiles.Where(Function(itm) (itm.ProfileID = Me.MirrorProfileId OrElse itm.ProfileID = Me.MasterProfileId) AndAlso itm.IsMaster = True).SingleOrDefault()
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Return _mineMasterProfile
    End Function


    Dim _mineMirrorProfile As EUS_Profile
    Protected Function GetCurrentMirrorProfile() As EUS_Profile
        Try

            If (_mineMirrorProfile Is Nothing) Then
                _mineMirrorProfile = Me.CMSDBDataContext.EUS_Profiles.Where(Function(itm) (itm.ProfileID = Me.MirrorProfileId OrElse itm.ProfileID = Me.MasterProfileId) AndAlso itm.IsMaster = False).SingleOrDefault()
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Return _mineMirrorProfile
    End Function




    Dim _currentProfile As EUS_Profile
    Protected Function GetCurrentProfile() As EUS_Profile
        Try

            If (_currentProfile Is Nothing) Then
                _currentProfile = Me.GetCurrentMasterProfile()
                If (_currentProfile Is Nothing) Then
                    _currentProfile = Me.GetCurrentMirrorProfile()
                End If
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Return _currentProfile
    End Function


    Dim _minePhotosList As List(Of EUS_CustomerPhoto)
    Protected Function GetPhotosList() As List(Of EUS_CustomerPhoto)
        Try

            If (_minePhotosList Is Nothing) And Me.MasterProfileId > 0 Then
                _minePhotosList = Me.CMSDBDataContext.EUS_CustomerPhotos.Where(Function(itm) itm.CustomerID = Me.MasterProfileId OrElse itm.CustomerID = Me.MirrorProfileId).ToList()
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Return _minePhotosList
    End Function



    ' Page PreInit 

    Public Overridable Sub Master_LanguageChanged() Implements ILanguageDependableContent.Master_LanguageChanged

    End Sub


    Protected Overridable Sub Page_PreInit(ByVal sender As Object, ByVal e As EventArgs)

        If clsCurrentContext.VerifyLogin() = True Then
            Me.MasterPageFile = "~/Members/Members2.Master"
        End If


        If Not String.IsNullOrEmpty(Request.QueryString("theme")) Then
            SessionVariables.myTheme = Request.QueryString("theme")
        End If

        Dim themeName = gDefaultThemeName

        If Not String.IsNullOrEmpty(SessionVariables.myTheme) Then
            themeName = SessionVariables.myTheme
        End If



        Dim clientScriptBlock As String = "var DXCurrentThemeCookieName = """ & themeName & """;"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "DXCurrentThemeCookieName", clientScriptBlock, True)

        'Me.Theme = themeName

        Try
            If Not String.IsNullOrEmpty(SessionVariables.myCulture) Then
                SessionVariables.myCulture = "US"
                Dim aCookie As New HttpCookie("otmData")
                aCookie.Values("languagePref") = SessionVariables.myCulture
                aCookie.Expires = System.DateTime.Now.AddDays(21)
                Response.Cookies.Add(aCookie)
            End If
            'gLAG.SetLag(SessionVariables.myCulture)
        Catch ex As Exception

        End Try
    End Sub



    ' Page Load 
    Protected Overrides Sub OnLoad(ByVal e As EventArgs)
        MyBase.OnLoad(e)

        'If UsingIE7CompatibilityMode Then
        '    Dim ie7CompatModeMeta As LiteralControl = RenderUtils.CreateLiteralControl()
        '    'ie7CompatModeMeta.ID = RenderUtils.IE8CompatibilityMetaID
        '    'ie7CompatModeMeta.Text = RenderUtils.IE8CompatibilityMetaText
        '    Header.Controls.AddAt(0, ie7CompatModeMeta)
        'End If

        '' Scripts
        'ASPxWebControl.RegisterUtilsScript(Me)
        ''RegisterScript("Utilities", "~/Scripts/Utilities.js")
        '' CSS
        ''RegisterCSSLink("~/CSS/styles.css")
        ''RegisterCSSLink("~/CSS/ToolBars.css")
        'If (Not String.IsNullOrEmpty(Me.cssLink_Renamed)) Then
        '    RegisterCSSLink(Me.cssLink_Renamed)
        'End If

        If Not IsPostBack Or Not IsCallback Then
            Dim szURL As String
            Try
                szURL = Request.Url.ToString()
                If Len(szURL) Then
                    Dim szPage As String = System.IO.Path.GetFileName(Request.Url.ToString())
                    Dim iStartParams As Integer = InStr(szPage, "?")
                    If iStartParams > 1 Then
                        szPage = Mid(szPage, 1, iStartParams - 1)
                    End If
                    SessionVariables.Page = szPage
                    SessionVariables.IdentityLoginName = HttpContext.Current.User.Identity.Name
                    Dim gStat As New clsWebStatistics
                    gStat.AddNew(Me)
                End If
            Catch ex As Exception
                Exit Try
            End Try
        End If

    End Sub

    ' Functions 
    Protected Overridable Function IsCurrentPage(ByVal oUrl As Object) As Boolean
        If oUrl Is Nothing Then
            Return False
        End If

        Dim result As Boolean = False
        Dim url As String = oUrl.ToString()
        If url.ToLower() = Page.Request.AppRelativeCurrentExecutionFilePath.ToLower() Then
            result = True
        End If
        Return result
    End Function

    Private Sub RegisterScript(ByVal key As String, ByVal url As String)
        Page.ClientScript.RegisterClientScriptInclude(key, Page.ResolveUrl(url))
    End Sub

    Private Sub RegisterCSSLink(ByVal url As String)
        Dim link As New HtmlLink()
        Page.Header.Controls.Add(link)
        link.EnableViewState = False
        link.Attributes.Add("type", "text/css")
        link.Attributes.Add("rel", "stylesheet")
        link.Href = url
    End Sub


    Protected Overridable Sub Page_Unload(sender As Object, e As System.EventArgs) Handles Me.Unload
        If (_CMSDBDataContext IsNot Nothing) Then
            _CMSDBDataContext.Dispose()
        End If

        If (_minePhotosList IsNot Nothing) Then
            _minePhotosList.Clear()
        End If
    End Sub





End Class



'Private Const InvalidHighlightIndex As Integer = Int32.MinValue
'Private cssLink_Renamed As String = ""


'Protected ReadOnly Property Browser() As BrowserInfo
'    Get
'        Return RenderUtils.Browser
'    End Get
'End Property

'Protected WriteOnly Property CSSLink() As String
'    Set(ByVal value As String)
'        cssLink_Renamed = value
'    End Set
'End Property

'Protected Overridable ReadOnly Property RequiresIE7CompatibilityMode() As Boolean
'    Get
'        Return False
'    End Get
'End Property

'Private ReadOnly Property UsingIE7CompatibilityMode() As Boolean
'    Get
'        'Return Browser.IsIE AndAlso RequiresIE7CompatibilityMode AndAlso (Browser.Version >= 8 OrElse (Browser.Version = 7)) 'andAlso Browser.IsIE7CompatibilityMode))
'        Return False
'    End Get
'End Property

'Protected Overrides Sub OnPreInit(ByVal e As EventArgs)
'    If UsingIE7CompatibilityMode Then
'        RenderUtils.Browser.RequestIE7CompatibilityMode()
'    End If
'    MyBase.OnPreInit(e)
'End Sub

' Events 
'Protected Overridable Sub TraceEvent(ByVal memo As HtmlGenericControl, ByVal sender As Object, ByVal args As EventArgs, ByVal eventName As String)
'    Dim s As String = ""
'    Dim properties() As PropertyInfo = args.GetType().GetProperties(BindingFlags.Instance Or BindingFlags.Public)
'    For Each propertyInfo As PropertyInfo In properties
'        If propertyInfo.PropertyType.IsValueType Then
'            s &= propertyInfo.Name & " = " & (propertyInfo.GetValue(args, Nothing)) & "<br />"
'        Else
'            s &= propertyInfo.Name & " = " & ("[" & propertyInfo.PropertyType.Name & "]") & "<br />"
'        End If
'    Next propertyInfo

'    memo.InnerHtml &= "<table cellspacing=""0"" cellpadding=""0"" border=""0""><tr><td valign=""top"" style=""width: 100px;"">Sender:</td><td valign=""top"">" & (TryCast(sender, Control)).ID & "</td></tr><tr><td valign=""top"">EventType:</td><td valign=""top""><b>" & eventName & "</b></td></tr><tr><td valign=""top"">Arguments:</td><td valign=""top"">" & s & "</td></tr></table><br />"
'End Sub
'Protected Overridable Sub ClearEvents(ByVal memo As HtmlGenericControl)
'    memo.InnerHtml = ""
'End Sub
'Protected Overridable Sub InitEvents(ByVal memo As HtmlGenericControl)
'    Page.ClientScript.RegisterStartupScript(GetType(BasePage), "ScrollEvents", "document.getElementById('" & memo.ClientID & "').scrollTop = 100000;", True)
'End Sub

'Private Function GetStatusKey(ByVal text As String, ByVal url As String) As String
'    Return text & "-" & url
'End Function

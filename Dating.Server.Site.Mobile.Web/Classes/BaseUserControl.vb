﻿Imports Dating.Server.Core.DLL

Public Class BaseUserControl
    Inherits System.Web.UI.UserControl
    Implements ILanguageDependableContent


    Protected ReadOnly Property MasterProfileId As Integer
        Get
            Return Me.Session("ProfileID")
        End Get
    End Property


    Protected ReadOnly Property MirrorProfileId As Integer
        Get
            Return Me.Session("MirrorProfileID")
        End Get
    End Property


    Dim _sessVars As clsSessionVariables
    Protected ReadOnly Property SessionVariables As clsSessionVariables
        Get
            If (_sessVars Is Nothing) Then _sessVars = clsSessionVariables.GetCurrent()
            Return _sessVars
        End Get
    End Property


    Private _globalStrings As clsPageData
    Public ReadOnly Property globalStrings As clsPageData
        Get
            If (_globalStrings Is Nothing) Then
                _globalStrings = New clsPageData("GlobalStrings", HttpContext.Current)
            End If
            Return _globalStrings
        End Get
    End Property



    Dim _CMSDBDataContext As CMSDBDataContext
    Protected ReadOnly Property CMSDBDataContext As CMSDBDataContext
        Get
            If (_CMSDBDataContext Is Nothing) Then
                _CMSDBDataContext = New CMSDBDataContext(ConfigurationManager.ConnectionStrings("AppDBconnectionString").ConnectionString)
            End If
            Return _CMSDBDataContext
        End Get
    End Property


    Dim _mineMasterProfile As EUS_Profile
    Protected Function GetCurrentMasterProfile() As EUS_Profile

        If (_mineMasterProfile Is Nothing) And Me.MasterProfileId > 0 Then
            _mineMasterProfile = Me.CMSDBDataContext.EUS_Profiles.Where(Function(itm) itm.ProfileID = Me.MasterProfileId AndAlso itm.IsMaster = True).SingleOrDefault()
        End If

        Return _mineMasterProfile
    End Function


    Dim _mineMirrorProfile As EUS_Profile
    Protected Function GetCurrentMirrorProfile() As EUS_Profile

        If (_mineMirrorProfile Is Nothing) And Me.MirrorProfileId > 0 Then
            _mineMirrorProfile = Me.CMSDBDataContext.EUS_Profiles.Where(Function(itm) itm.ProfileID = Me.MirrorProfileId AndAlso itm.IsMaster = False).SingleOrDefault()
        End If

        Return _mineMirrorProfile
    End Function


    Dim _currentProfile As EUS_Profile
    Protected Function GetCurrentProfile() As EUS_Profile
        Try

            If (_currentProfile Is Nothing) Then
                _currentProfile = Me.GetCurrentMasterProfile()
                If (_currentProfile Is Nothing) Then
                    _currentProfile = Me.GetCurrentMirrorProfile()
                End If

                ' not approved member
                If (_currentProfile Is Nothing) Then
                    _currentProfile = Me.CMSDBDataContext.EUS_Profiles.Where(Function(itm) itm.ProfileID = Me.MasterProfileId AndAlso itm.IsMaster = False).SingleOrDefault()
                End If
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Return _currentProfile
    End Function



    Public Overridable Sub Master_LanguageChanged() Implements ILanguageDependableContent.Master_LanguageChanged

    End Sub


    Protected Overridable Sub Page_Unload(sender As Object, e As System.EventArgs) Handles Me.Unload
        If (Not _CMSDBDataContext Is Nothing) Then
            _CMSDBDataContext.Dispose()
        End If
    End Sub


End Class

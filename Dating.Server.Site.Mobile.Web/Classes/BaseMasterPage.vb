﻿Public Class BaseMasterPage
    Inherits System.Web.UI.MasterPage
    Implements ILanguageDependableContent



    Private _globalStrings As clsPageData
    Public ReadOnly Property globalStrings As clsPageData
        Get
            If (_globalStrings Is Nothing) Then
                _globalStrings = New clsPageData("GlobalStrings", HttpContext.Current)
            End If
            Return _globalStrings
        End Get
    End Property


    Public Overridable Sub Master_LanguageChanged() Implements ILanguageDependableContent.Master_LanguageChanged

    End Sub


End Class

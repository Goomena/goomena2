﻿Imports Dating.Server.Core.DLL

Public Class clsLogin

    Public Function PerfomLogin(DataRecordLogin As clsDataRecordLoginMember) As clsDataRecordLoginMemberReturn
        Dim DataRecordLoginReturn As New clsDataRecordLoginMemberReturn
        DataRecordLoginReturn.IsValid = False
        DataRecordLoginReturn.HasErrors = False

        Try
            Dim ds As DSMembers = DataHelpers.GetEUS_Profiles_ByLoginPass(DataRecordLogin.LoginName, DataRecordLogin.Password)
            Dim EUS_ProfilesDataTable As DSMembers.EUS_ProfilesDataTable = ds.EUS_Profiles
            If EUS_ProfilesDataTable.Rows.Count > 0 Then

                Dim rowIndex As Integer = 0
                For rowIndex = 0 To (EUS_ProfilesDataTable.Rows.Count - 1)
                    'While rowIndex < EUS_ProfilesDataTable.Rows.Count


                    Dim EUS_MembersRow As DSMembers.EUS_ProfilesRow = EUS_ProfilesDataTable.Rows(rowIndex)

                    If ((EUS_ProfilesDataTable.Rows.Count = 2 AndAlso EUS_MembersRow.IsMaster = True) OrElse (EUS_ProfilesDataTable.Rows.Count = 1)) Then
                        If (EUS_MembersRow.Status <> ProfileStatusEnum.Deleted) Then
                            DataRecordLoginReturn.Fill(EUS_MembersRow)

                            EUS_MembersRow.LastLoginDateTime = DateTime.UtcNow
                            EUS_MembersRow.LastLoginIP = HttpContext.Current.Request.Params("REMOTE_ADDR")
                            ' EUS_MembersRow.LastLoginGEOInfos, 

                        Else
                            DataRecordLoginReturn.HasErrors = True
                            DataRecordLoginReturn.Message = ProfileStatusEnum.Deleted.ToString()

                        End If
                    End If

                    'rowIndex += 1
                    'End While
                Next

            Else
                DataRecordLoginReturn.Message = "Error Login Or Password"
            End If

            ' log last login data
            If (DataRecordLoginReturn.ProfileID > 0) Then
                DataHelpers.UpdateEUS_Profiles_LoginData(DataRecordLoginReturn.ProfileID)
            End If

        Catch ex As Exception
            DataRecordLoginReturn.HasErrors = True
            DataRecordLoginReturn.Message = ex.Message
        End Try
        Return DataRecordLoginReturn
    End Function



    Public Function PerfomSiteLogin(loginName As String, pass As String, ByVal rememberUserName As Boolean) As clsDataRecordLoginMemberReturn
        Dim DataRecordLoginReturn As New clsDataRecordLoginMemberReturn
        DataRecordLoginReturn.IsValid = False
        DataRecordLoginReturn.HasErrors = False

        Try
            Dim core As New clsCore

            Dim DataRecordLogin As New clsDataRecordLoginMember()
            DataRecordLogin.LoginName = loginName
            DataRecordLogin.Password = pass

            DataRecordLoginReturn = Me.PerfomLogin(DataRecordLogin)

            If (HttpContext.Current.Session("SessionVariables") Is Nothing) Then
                HttpContext.Current.Session("SessionVariables") = New clsSessionVariables()
            End If

            Dim sesVars As clsSessionVariables = DirectCast(HttpContext.Current.Session("SessionVariables"), clsSessionVariables)
            sesVars.DataRecordLoginMemberReturn = DataRecordLoginReturn
            HttpContext.Current.Session("LagID") = DataRecordLoginReturn.LAGID
            HttpContext.Current.Session("ProfileID") = DataRecordLoginReturn.ProfileID
            HttpContext.Current.Session("MirrorProfileID") = DataRecordLoginReturn.MirrorProfileID

            If (HttpContext.Current.Session("LagID") Is Nothing) Then
                clsCurrentContext.SetLAGID()
            End If

            If DataRecordLoginReturn.IsValid Then

                'Create Form Authentication ticket
                Dim ticket As New FormsAuthenticationTicket(1, DataRecordLoginReturn.LoginName, DateTime.Now, DateTime.Now.AddDays(5), rememberUserName, DataRecordLoginReturn.Role, FormsAuthentication.FormsCookiePath)


                'For security reasons we may hash the cookies
                Dim hashCookies As String = FormsAuthentication.Encrypt(ticket)
                Dim cookie As New HttpCookie(FormsAuthentication.FormsCookieName, hashCookies)

                cookie.Path = FormsAuthentication.FormsCookiePath()
                If rememberUserName Then
                    cookie.Expires = ticket.Expiration
                End If

                'add the cookie to user browser
                HttpContext.Current.Response.Cookies.Remove(cookie.Name)
                HttpContext.Current.Response.Cookies.Add(cookie)

                HttpContext.Current.Session("DataRecordLoginReturn") = DataRecordLoginReturn

            End If

        Catch ex As Exception
            DataRecordLoginReturn.HasErrors = True
            DataRecordLoginReturn.Message = ex.Message
        End Try


        Return DataRecordLoginReturn
    End Function


    Public Function PerfomRelogin(loginName As String, ByVal rememberUserName As Boolean) As clsDataRecordLoginMemberReturn
        Dim DataRecordLoginReturn As New clsDataRecordLoginMemberReturn
        DataRecordLoginReturn.IsValid = False
        DataRecordLoginReturn.HasErrors = False

        Try
            Dim dc As New CMSDBDataContext(System.Configuration.ConfigurationManager.ConnectionStrings("AppDBconnectionString").ConnectionString)
            Dim pass As String = Nothing

            Try
                ' retrieve user password
                pass = (From itm In dc.EUS_Profiles
                        Where (itm.LoginName.ToUpper() = loginName.ToUpper() OrElse _
                               itm.eMail.ToUpper() = loginName.ToUpper()) _
                        AndAlso _
                              (itm.Status = ProfileStatusEnum.Approved OrElse _
                                 itm.Status = ProfileStatusEnum.NewProfile OrElse _
                                 itm.Status = ProfileStatusEnum.Rejected) _
                        Select itm.Password).FirstOrDefault()
            Catch ex As Exception

            Finally
                dc.Dispose()
            End Try

            DataRecordLoginReturn = Me.PerfomSiteLogin(loginName, pass, rememberUserName)

        Catch ex As Exception
            DataRecordLoginReturn.HasErrors = True
            DataRecordLoginReturn.Message = ex.Message
        End Try

        Return DataRecordLoginReturn
    End Function

    Function PerfomReloginWithTwitterId(twUserId As String, rememberUserName As Boolean) As clsDataRecordLoginMemberReturn
        Dim DataRecordLoginReturn As New clsDataRecordLoginMemberReturn
        DataRecordLoginReturn.IsValid = False
        DataRecordLoginReturn.HasErrors = False

        Dim dc As New CMSDBDataContext(System.Configuration.ConfigurationManager.ConnectionStrings("AppDBconnectionString").ConnectionString)
        Try
            Dim pass = Nothing

            ' retrieve user password
            pass = (From itm In dc.EUS_Profiles
                    Where (itm.TwitterUserId.ToString() = twUserId) _
                    AndAlso _
                          (itm.Status = ProfileStatusEnum.Approved OrElse _
                             itm.Status = ProfileStatusEnum.NewProfile OrElse _
                             itm.Status = ProfileStatusEnum.Rejected) _
                    Select itm.LoginName, itm.Password).FirstOrDefault()


            If (pass IsNot Nothing) Then
                DataRecordLoginReturn = Me.PerfomSiteLogin(pass.LoginName, pass.Password, rememberUserName)
            End If

        Catch ex As Exception
            DataRecordLoginReturn.HasErrors = True
            DataRecordLoginReturn.Message = ex.Message
        Finally
            dc.Dispose()
        End Try

        Return DataRecordLoginReturn
    End Function

    Function PerfomReloginWithFacebookId(fbUserId As String, rememberUserName As Boolean) As clsDataRecordLoginMemberReturn
        Dim DataRecordLoginReturn As New clsDataRecordLoginMemberReturn
        DataRecordLoginReturn.IsValid = False
        DataRecordLoginReturn.HasErrors = False

        Dim dc As New CMSDBDataContext(System.Configuration.ConfigurationManager.ConnectionStrings("AppDBconnectionString").ConnectionString)
        Try
            Dim pass = Nothing

            ' retrieve user password
            pass = (From itm In dc.EUS_Profiles
                    Where (itm.FacebookUserId.ToString() = fbUserId) _
                    AndAlso _
                          (itm.Status = ProfileStatusEnum.Approved OrElse _
                             itm.Status = ProfileStatusEnum.NewProfile OrElse _
                             itm.Status = ProfileStatusEnum.Rejected) _
                    Select itm.LoginName, itm.Password).FirstOrDefault()


            If (pass IsNot Nothing) Then
                DataRecordLoginReturn = Me.PerfomSiteLogin(pass.LoginName, pass.Password, rememberUserName)
            End If


        Catch ex As Exception
            DataRecordLoginReturn.HasErrors = True
            DataRecordLoginReturn.Message = ex.Message
        Finally
            dc.Dispose()
        End Try

        Return DataRecordLoginReturn
    End Function


End Class

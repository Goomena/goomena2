﻿Imports System.Diagnostics
Imports Microsoft.VisualBasic
Imports System
Imports System.Drawing
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.IO
Imports DevExpress.XtraEditors
Imports DevExpress.Utils.Drawing
Imports DevExpress.Utils
Imports System.Text.RegularExpressions
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports System.Web.UI
Imports System.Text
Imports System.Web.UI.HtmlControls
Imports System.Web
Imports Library.Public
Imports Dating.Server.Core.DLL

Public Class AppUtils
    Public Shared Function mySQLDate(ByVal D As DateTime) As String
        On Error GoTo er
        Dim sz As String
        sz = Microsoft.VisualBasic.Year(D) & "/" & Microsoft.VisualBasic.Month(D) & "/" & Microsoft.VisualBasic.DateAndTime.Day(D) ' d.Year & "/" & d.Month & "/" & d.Day
        mySQLDate = " CONVERT(DATETIME, '" & sz & "  00:00:00', 102)" '"'" & sZ & "'"
        Exit Function
er:     ErrorMsgBox("")
    End Function

    Public Shared Function mySQLDateTime(ByVal D As DateTime) As String
        On Error GoTo er
        Dim sz As String
        sz = Microsoft.VisualBasic.Year(D) _
            & "/" _
            & Microsoft.VisualBasic.Month(D) _
            & "/" _
            & Microsoft.VisualBasic.DateAndTime.Day(D) _
            & " " _
            & Microsoft.VisualBasic.Format(Microsoft.VisualBasic.DateAndTime.Hour(D), "#0") _
            & ":" _
            & Microsoft.VisualBasic.Format(Microsoft.VisualBasic.DateAndTime.Minute(D), "#0") _
            & ":" _
            & Microsoft.VisualBasic.Format(Microsoft.VisualBasic.DateAndTime.Second(D), "#0")
        mySQLDateTime = " CONVERT(DATETIME, '" & sz & "', 102)" '"'" & sZ & "'"
        Exit Function
er:     ErrorMsgBox("")
    End Function

    Public Shared Function mySQLDateTimeCustom(ByVal D As DateTime, ByVal szTime As String) As String
        On Error GoTo er
        Dim sz As String
        sz = Microsoft.VisualBasic.Year(D) & "/" & Microsoft.VisualBasic.Month(D) & "/" & Microsoft.VisualBasic.DateAndTime.Day(D) ' d.Year & "/" & d.Month & "/" & d.Day
        mySQLDateTimeCustom = " CONVERT(DATETIME, '" & sz & "  " & szTime & "', 102)" '"'" & sZ & "'"
        Exit Function
er:     ErrorMsgBox("")
    End Function

    Public Shared Function mySQLDateOnly(ByVal D As DateTime) As String
        On Error GoTo er
        Dim sz As String
        sz = Microsoft.VisualBasic.Year(D) & "/" & Microsoft.VisualBasic.Month(D) & "/" & Microsoft.VisualBasic.DateAndTime.Day(D) ' d.Year & "/" & d.Month & "/" & d.Day
        mySQLDateOnly = sz & "  00:00:00" '"'" & sZ & "'"
        Exit Function
er:     ErrorMsgBox("")
    End Function


    Public Shared Sub setSEOPageData(ByVal page As BasePage, ByVal title As String, Optional ByVal meta_description_content As String = "", Optional ByVal meta_keywords_content As String = "", Optional ByVal meta_copyright_content As String = "")
        'SEO '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        page.Header.Title = title

        'Render: <meta name="keywords" content="Some words listed here" />
        Dim meta_description As New HtmlMeta
        meta_description.Name = "description"
        meta_description.Content = meta_description_content
        page.Header.Controls.Add(meta_description)

        'Render: <meta name="keywords" content="Some words listed here" />
        Dim meta_keywords As New HtmlMeta
        meta_keywords.Name = "keywords"
        meta_keywords.Content = meta_keywords_content
        page.Header.Controls.Add(meta_keywords)

        'Render: <meta name="copyright" content="Some words listed here" />
        Dim meta_copyright As New HtmlMeta
        meta_copyright.Name = "copyright"
        meta_copyright.Content = meta_copyright_content
        page.Header.Controls.Add(meta_copyright)


        ' Render: <meta name="robots" content="noindex" />
        '<META NAME="ROBOTS" CONTENT="NOINDEX, FOLLOW">
        '<META NAME="ROBOTS" CONTENT="INDEX, NOFOLLOW">
        '<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">

        Dim meta_robots = New HtmlMeta
        meta_robots.Name = "robots"
        meta_robots.Content = "INDEX, FOLLOW"
        page.Header.Controls.Add(meta_robots)

        ' Render: <meta name="date" content="2006-03-25" scheme="YYYY-MM-DD" />
        Dim meta_date = New HtmlMeta
        meta_date.Name = "date"
        meta_date.Content = DateTime.Now.ToString("yyyy-MM-dd")
        meta_date.Scheme = "YYYY-MM-DD" & vbCrLf
        page.Header.Controls.Add(meta_date)

        Dim verify_robots = New HtmlMeta
        verify_robots.Name = "verify-v1"
        verify_robots.Content = "verify Google"
        page.Header.Controls.Add(verify_robots)



        'END SEO '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    End Sub

    Public Shared Sub initPageData(ByVal page As BasePage, ByVal labelName As String, ByVal pageURL As String, ByVal lagID As String)
        ' Try

        'Dim PageData As Dictionary(Of String, String) = GetPageMainMessages(getPageFileNameFromURL(pageURL), siteID, lagID)
        'setSEOPageData(page, PageData("Title"), PageData("MetaDescription"))
        'If labelName <> "" Then
        '    Dim label As DevExpress.Web.ASPxEditors.ASPxLabel = page.Controls.Item(labelName)
        '    label.Text = PageData("Body")
        'End If
        'Catch ex As Exception

        'End Try
    End Sub
    Public Shared Sub setNaviLabel(ByVal page As BasePage, ByVal labelName As String, ByVal labelText As String)
        Dim label As DevExpress.Web.ASPxEditors.ASPxLabel = page.Master.FindControl(labelName)
        If (Not label Is Nothing) Then
            label.Text = labelText
        End If
    End Sub
    Public Shared Function ErrorMsgBox(ByVal sExtaMessage As String)
        'On Error GoTo er ' **** na min enegorpithi katastrefete to error
        Dim s As String = Err.Description & vbCrLf & "Error Code: " & Err.Number & vbCrLf & "Source: " & Err.Source & vbCrLf & "Function:" & sExtaMessage
        HttpContext.Current.Response.Write(s)
        ' Err.Clear()
        Return Nothing
er:
        MsgBox("Error on ErrorMsgBox->" & sExtaMessage)
    End Function

    Public Shared Function ErrorMsgBox(ByVal ex As Exception, ByVal sExtaMessage As String)
        Try
            Dim s As String = ex.Message & "<br>" & "Source: " & ex.Source & " <br>[Function:" & sExtaMessage & "<br>] Stack: " & ex.StackTrace & "<br>"
            'MsgBox(s, "Error", MessageBoxButtons.OK)
            HttpContext.Current.Response.Write(s)
        Catch inex As Exception
            'MsgBox(inex.Message & vbCrLf & "Error on ErrorMsgBox->" & sExtaMessage)
        End Try
        Return Nothing
    End Function


    Public Shared Function CheckNULL(ByVal nValue As Object, ByVal DefaultValue As String) As String
        Try
            If IsDBNull(nValue) Then nValue = DefaultValue
            If Len(DefaultValue) > 0 And Len(nValue) = 0 Then Return DefaultValue
            Return nValue
        Catch ex As Exception
            ErrorMsgBox(ex, "")
            Return DefaultValue
        End Try
    End Function

    Public Shared Function FormatFileSize(ByVal Size As Long) As String
        Try
            Dim KB As Integer = 1024
            Dim MB As Integer = KB * KB
            ' Return size of file in kilobytes.
            If Size < KB Then
                Return (Size.ToString("D") & " bytes")
            Else
                Select Case Size / KB
                    Case Is < 1000
                        Return (Size / KB).ToString("N") & "KB"
                    Case Is < 1000000
                        Return (Size / MB).ToString("N") & "MB"
                    Case Is < 10000000
                        Return (Size / MB / KB).ToString("N") & "GB"
                End Select
            End If
        Catch ex As Exception
        End Try

        Return Size.ToString
    End Function


    Public Shared Function FormatFileSizeMB(ByVal Size As Long) As String
        Try
            Dim KB As Integer = 1024
            Dim MB As Integer = KB * KB
            ' Return size of file in kilobytes.
            Return (Size / KB).ToString("N") & "GB"
        Catch ex As Exception
        End Try

        Return Size.ToString
    End Function


    Public Shared Function GetParamValueFromUrl(ByVal szURL As String, ByVal szParamName As String, ByVal DefaultValue As String) As String
        Try

            Dim StartPos As Integer = InStr(szURL, szParamName & "=")
            If StartPos = 0 Then Return ""
            Dim NewString As String = Mid(szURL, StartPos + 2)
            Dim EndPos As Integer = InStr(NewString, "&")
            If EndPos = 0 Then EndPos = Len(NewString) - StartPos
            Dim Value As String = Mid(szURL, StartPos + 2, EndPos - 1)
            Return Value

        Catch ex As Exception
            'ErrorMsgBox(ex, "")
            Return DefaultValue
        End Try
    End Function



    Public Shared Function GetImage(CustomerID As String, FileName As String, GenderId As String, isThumb As Boolean, isHTTPS As Boolean) As String
        Dim photoPath As String = ""
        Try

            Dim _CustomerID As Integer = 0
            Dim _GenderId As Integer = 0

            If (Not String.IsNullOrEmpty(CustomerID)) Then
                Integer.TryParse(CustomerID, _CustomerID)
            End If

            If (Not String.IsNullOrEmpty(GenderId)) Then
                Integer.TryParse(GenderId, _GenderId)
            End If

            photoPath = ProfileHelper.GetProfileImageURL(_CustomerID, FileName, _GenderId, True, isHTTPS)
        Catch ex As Exception

        End Try
        Return photoPath
    End Function

    Public Shared Function GetDateTimeString(dateTimeValue As Object, tookenString As String) As String
        Dim txt = ""
        Try
            txt = CType(dateTimeValue, DateTime).ToLocalTime().ToString("d MMM, yyyy H:mm")
            txt = tookenString.Replace("###DATETIME###", txt)
        Catch ex As Exception

        End Try
        Return txt
    End Function


    Public Shared Function IsDBNullOrNothingOrEmpty(obj As Object) As Boolean
        If (obj Is System.DBNull.Value) Then Return True
        If (obj Is Nothing) Then Return True
        If (obj.GetType() Is GetType(String) AndAlso obj.ToString() = String.Empty) Then Return True

        Return False
    End Function


End Class

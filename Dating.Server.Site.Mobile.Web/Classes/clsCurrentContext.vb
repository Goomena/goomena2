﻿Public Class clsCurrentContext



    Public Shared Function VerifyLogin() As Boolean

        If (HttpContext.Current.Request.Params("AUTH_USER") <> "" AndAlso HttpContext.Current.User.Identity.IsAuthenticated AndAlso HttpContext.Current.Session("ProfileID") > 0) Then
            Return True
        End If

        Return False
    End Function

    Public Shared Sub SetLAGID()
        If (HttpContext.Current.Session("LagID") Is Nothing) Then
            If (HttpContext.Current.Session("GEO_COUNTRY_CODE") = "US" OrElse HttpContext.Current.Session("GEO_COUNTRY_CODE") = "GR" OrElse HttpContext.Current.Session("GEO_COUNTRY_CODE") = "HU") Then
                HttpContext.Current.Session("LagID") = HttpContext.Current.Session("GEO_COUNTRY_CODE")
            Else
                HttpContext.Current.Session("LagID") = "US"
            End If
        End If
    End Sub

End Class

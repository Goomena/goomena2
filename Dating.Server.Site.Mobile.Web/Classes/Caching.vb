﻿Imports Dating.Server.Core.DLL

Public Class Caching

#Region "Read-only properties to return data in application"
    Private Shared _siteMenuItems As List(Of dsMenu.SiteMenuItemsRow)
    Public Shared ReadOnly Property SiteMenuItems As List(Of dsMenu.SiteMenuItemsRow)
        Get
            If _siteMenuItems Is Nothing Then GetSiteMenuItems()
            Return _siteMenuItems
        End Get
    End Property
#End Region

#Region "Public methods to manipulate cache"
    Public Shared Sub Reload()
        GetSiteMenuItems()
    End Sub
#End Region

#Region "Private methods to fill cache"
    Private Shared Sub GetSiteMenuItems()
        _siteMenuItems = New List(Of dsMenu.SiteMenuItemsRow)

        Dim da As New dsMenuTableAdapters.SiteMenuItemsTableAdapter
        da.Connection = New SqlClient.SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("AppDBconnectionString").ConnectionString)
        Dim dt As dsMenu.SiteMenuItemsDataTable = da.GetAllActive()

        For Each row As dsMenu.SiteMenuItemsRow In dt.Rows
            _siteMenuItems.Add(row)
        Next

        da.Dispose()
    End Sub
#End Region

End Class




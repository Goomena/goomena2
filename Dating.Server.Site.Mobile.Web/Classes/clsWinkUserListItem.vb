﻿

'Interface UserListItem
'End Interface

Public Class clsWinkUserListItem

    Public Property ProfileID As Integer
    Public Property MirrorProfileID As Integer
    Public Property LoginName As String
    Public Property Genderid As Integer
    Public Property ProfileViewUrl As String
    Public Property ImageFileName As String
    Public Property ImageUrl As String
    Public Property ImageThumbUrl As String
    Public Property ImageUploadDate As String

    Public Property OtherMemberLoginName As String
    Public Property OtherMemberProfileID As Integer
    Public Property OtherMemberMirrorProfileID As Integer
    Public Property OtherMemberGenderid As Integer
    Public Property OtherMemberAge As Integer
    Public Property OtherMemberCity As String
    Public Property OtherMemberRegion As String
    Public Property OtherMemberHeading As String
    Public Property OtherMemberHeight As String
    Public Property OtherMemberPersonType As String
    Public Property OtherMemberHair As String
    Public Property OtherMemberEyes As String
    Public Property OtherMemberEthnicity As String
    Public Property OtherMemberImageFileName As String
    Public Property OtherMemberImageUrl As String
    Public Property OtherMemberImageThumbUrl As String
    Public Property OtherMemberProfileViewUrl As String

    Public Property Distance As String
    Public Property DistanceCss As String
    Public Property CreateOfferUrl As String
    Public Property RejectOfferUrl As String

    Public Property OfferID As Integer
    Public Property OfferAmount As Integer

    Public Property IsWink As Boolean
    'Public Property IsCounter As Boolean
    Public Property IsOffer As Boolean

    Public Property AllowWink As Boolean
    Public Property AllowUnWink As Boolean

    Public Property AllowFavorite As Boolean
    Public Property AllowUnfavorite As Boolean

    Public Property AllowAccept As Boolean
    Public Property AllowCounter As Boolean
    Public Property AllowCreateOffer As Boolean
    Public Property AllowCancelWink As Boolean
    Public Property AllowCancelPendingOffer As Boolean
    Public Property AllowDeletePendingOffer As Boolean

    Public Property AllowTryWink As Boolean
    Public Property AllowDeleteWink As Boolean
    Public Property AllowDeleteOffer As Boolean



    Public Property AllowUnblock As Boolean
    Public Property UnblockText As String



    Public Property NoOfferText As String

    Public Property HasNoPhotosText As String
    Public Property SearchOurMembersText As String
    Public Property ForDateWithText As String
    Public Property YouReceivedWinkOfferText As String
    Public Property WantToKnowFirstDatePriceText As String
    Public Property MakeOfferText As String
    Public Property AcceptText As String
    Public Property CounterText As String
    Public Property RejectText As String
    Public Property NotInterestedText As String
    Public Property TooFarAwayText As String
    Public Property NotEnoughInfoText As String
    Public Property DifferentExpectationsText As String
    Public Property YearsOldText As String
    Public Property MilesAwayText As String
    Public Property DeleteOfferText As String
    Public Property SendMessageText As String
    Public Property YearsOldFromText As String
    Public Property TryWinkText As String
    Public Property MakeNewOfferText As String
    Public Property YourWinkSentText As String
    Public Property AwaitingResponseText As String

    Public Property WinkText As String
    Public Property UnWinkText As String

    Public Property FavoriteText As String
    Public Property UnfavoriteText As String

    Public Property AddPhotosText As String
    Public Property SendMessageUrl As String
    Public Property WillYouAcceptDateWithForAmountText As String
    'Public Property ProposeNewOfferText As String


    Public Property OfferAcceptedWithAmountText As String
    Public Property OfferAcceptedHowToContinueText As String
    Public Property OfferAcceptedUnlockText As String
    Public Property AllowSendMessage As Boolean
    Public Property AllowDeleteAcceptedOffer As Boolean
    Public Property AllowOfferAcceptedUnlock As Boolean

    Public Property Credits As String
    Public Property OfferDeleteConfirmMessage As String

    Public Property CancelledRejectedTitleText As String
    Public Property CancelledRejectedDescriptionText As String
    Public Property CancelWinkText As String
    Public Property CancelOfferText As String
    Public Property DeleteWinkText As String


    ''' <summary>
    ''' Replaces following tookens ###LOGINNAME###,###AMOUNT###,###CREDITS###,###DISTANCE###
    ''' </summary>
    ''' <param name="text"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ReplaceTokens(text As String) As String
        If (Not String.IsNullOrEmpty(text)) Then
            text = text.Replace("###LOGINNAME###", Me.OtherMemberLoginName)
            text = text.Replace("###AMOUNT###", "&euro;" & Me.OfferAmount.ToString())
            text = text.Replace("###CREDITS###", Me.Credits)
            text = text.Replace("###DISTANCE###", Me.Distance)
        End If
        Return text
    End Function


    Public Shared Function ReplaceTokens(text As String, OtherMemberLoginName As String, OfferAmount As Integer, Credits As String, Distance As String) As String
        If (Not String.IsNullOrEmpty(text)) Then
            text = text.Replace("###LOGINNAME###", OtherMemberLoginName)
            text = text.Replace("###AMOUNT###", "&euro;" & OfferAmount.ToString())
            text = text.Replace("###CREDITS###", Credits)
            text = text.Replace("###DISTANCE###", Distance)
        End If
        Return text
    End Function


End Class


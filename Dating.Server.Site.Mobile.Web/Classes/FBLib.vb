﻿Imports FilesProxy.DownloadSites.Web
Imports System.Xml
Imports System.IO

Public Class FBLib


    'Public Shared Function LoginWithFacebook() As FBUserInfo


    '    Dim Appid As String = ConfigurationManager.AppSettings("FBApp_ID")
    '    Dim AppSecret As String = ConfigurationManager.AppSettings("FBApp_Secret")
    '    Dim ReplyURL As String = ConfigurationManager.AppSettings("FBRedirect")
    '    Dim origQS As NameValueCollection

    '    Dim strOut As String = FBLib.FBLoginURL(Appid, AppSecret, ReplyURL)
    '    origQS = HttpUtility.ParseQueryString(strOut)
    '    Dim str As String = origQS("code")

    '    Dim fb As FBUserInfo = FBLib.FBLoadPage(Appid, AppSecret, ReplyURL, str)
    '    Return fb

    'End Function



    ''' <summary>
    ''' Get Facebook login Url
    ''' </summary>
    ''' <returns>Facebook login URL(including AppID,AppSECREt,RedirectLink)</returns>
    Public Shared Function FBLoginURL(ByVal Appid As String, ByVal AppSecret As String, ByVal ReplyURL As String) As String
        Try
            ' "https://www.facebook.com/dialog/oauth?client_id=" + Appid + "&redirect_uri=" + ReplyURL + "&client_secret=" + AppSecret + "&scope=email,read_stream,publish_stream,user_location,user_hometown"
            Dim strOut As String = "https://www.facebook.com/dialog/oauth?client_id=" + Appid + "&redirect_uri=" + ReplyURL + "&client_secret=" + AppSecret + "&scope=email,read_stream,publish_stream,user_location,user_hometown"
            Return strOut
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Starting Function for getting users Information
    ''' </summary>
    ''' <returns>Facebook User Info as FBUserInfo</returns>
    Public Shared Function FBLoadPage(ByVal Appid As String, ByVal AppSecret As String, ByVal ReplyURL As String, ByVal str As String) As FBUserInfo '' As Dictionary(Of String, String)
        ''Dim dic As New Dictionary(Of String, String)
        Dim fb As New FBUserInfo
        If Not str = "" Then
            Dim strOut As String = "https://graph.facebook.com/oauth/access_token?client_id=" + Appid + "&redirect_uri=" + ReplyURL + "&client_secret=" + AppSecret + "&code=" + str
            Dim bro As SimpleBrowser
            Dim url As New WebPageUrl(strOut)
            bro = New SimpleBrowser()
            bro.Navigate(url)
            Dim returned As String = bro.LastPageText()
            If Not returned Is Nothing Then
                'Get loginuser https://api.facebook.com/method/users.getinfo?access_token=
                Dim countrycount As String = FBGetLogInUser(returned)

                If Not countrycount Is Nothing Then
                    'Get UserInfo
                    '    Dim tkn As String = "https://api.facebook.com/method/users.getinfo?uids=" + countrycount + "&fields=email,current_location,hometown_location&" + returned
                    Dim chk As Boolean = FBGetUserInfo(returned, countrycount, fb)
                End If
            End If
        End If
        Return fb
    End Function

    ''' <summary>
    ''' Get logged in facebook user info
    ''' </summary>
    ''' <returns>True if ok</returns>
    Private Shared Function FBGetUserInfo(ByVal access_token As String, ByVal user_Id As String, ByRef fb As FBUserInfo) As Boolean
        Dim tkn As String = "https://api.facebook.com/method/users.getinfo?uids=" + user_Id + "&fields=email,current_location,hometown_location,sex,birthday,birthday_date,username,meeting_sex,name,first_name,last_name&" + access_token
        Dim urlATKN As New WebPageUrl(tkn)
        Dim bro As New SimpleBrowser
        bro.Navigate(urlATKN)
        Dim returnedInfo As String = bro.LastPageText()

        ''  Dim fb As New FBUserInfo

        Dim settings As New XmlReaderSettings()
        Using reader As XmlReader = XmlReader.Create(New System.IO.StringReader(returnedInfo), settings)
            '' Dim strKey As String = reader.Name
            '' Dim strValue
            While (reader.Read())
                Select Case reader.NodeType
                    Case XmlNodeType.Element
                        Select Case reader.Name
                            Case "uid"
                                reader.Read()
                                fb.uid = reader.Value

                            Case "email"
                                reader.Read()
                                fb.email = reader.Value

                            Case "sex"
                                reader.Read()
                                fb.sex = reader.Value

                            Case "meeting_sex"
                                If (reader.MoveToAttribute("xsi:nil") AndAlso reader.Read() = True) Then
                                    Continue While
                                End If
                                'If (reader.ReadToFollowing("city")) Then
                                '    reader.Read()
                                '    fb.current_location_City = reader.Value
                                'End If
                                'reader.Read()
                                'fb.meeting_sex = reader.Value

                            Case "first_name"
                                reader.Read()
                                fb.first_name = reader.Value

                            Case "middle_name"
                                reader.Read()
                                fb.middle_name = reader.Value

                            Case "last_name"
                                reader.Read()
                                fb.last_name = reader.Value

                            Case "name"
                                reader.Read()
                                fb.name = reader.Value

                            Case "birthday"
                                reader.Read()
                                fb.birthday = reader.Value

                            Case "birthday_date"
                                reader.Read()
                                fb.birthday_date = reader.Value

                            Case "username"
                                reader.Read()
                                fb.username = reader.Value

                            Case "current_location"
                                If (reader.MoveToAttribute("xsi:nil") AndAlso reader.Read() = True) Then
                                    Continue While
                                End If

                                If (reader.ReadToFollowing("city")) Then
                                    reader.Read()
                                    fb.current_location_City = reader.Value
                                End If

                                If (reader.ReadToFollowing("state")) Then
                                    reader.Read()
                                    fb.current_location_State = reader.Value
                                End If

                                If (reader.ReadToFollowing("country")) Then
                                    reader.Read()
                                    fb.current_location_Country = reader.Value
                                End If

                                If (reader.ReadToFollowing("zip")) Then
                                    reader.Read()
                                    fb.current_location_Zip = reader.Value
                                End If

                                If (reader.ReadToFollowing("id")) Then
                                    reader.Read()
                                    fb.current_location_Id = reader.Value
                                End If

                                If (reader.ReadToFollowing("name")) Then
                                    reader.Read()
                                    fb.current_location_Name = reader.Value
                                End If
                            Case "hometown_location"
                                If (reader.MoveToAttribute("xsi:nil") AndAlso reader.Read() = True) Then
                                    Continue While
                                End If

                                If (reader.ReadToFollowing("city")) Then
                                    reader.Read()
                                    fb.hometown_location_City = reader.Value
                                End If

                                If (reader.ReadToFollowing("state")) Then
                                    reader.Read()
                                    fb.hometown_location_State = reader.Value
                                End If

                                If (reader.ReadToFollowing("country")) Then
                                    reader.Read()
                                    fb.hometown_location_Country = reader.Value
                                End If

                                If (reader.ReadToFollowing("zip")) Then
                                    reader.Read()
                                    fb.hometown_location_Zip = reader.Value
                                End If

                                If (reader.ReadToFollowing("id")) Then
                                    reader.Read()
                                    fb.hometown_location_Id = reader.Value
                                End If

                                If (reader.ReadToFollowing("name")) Then
                                    reader.Read()
                                    fb.hometown_location_Name = reader.Value
                                End If

                        End Select
                End Select

            End While

        End Using

        Return True
    End Function

    ''' <summary>
    ''' Get logged in facebook user id
    ''' </summary>
    ''' <returns>Facebook User ID as string</returns>
    Private Shared Function FBGetLogInUser(ByVal access_token As String) As String
        'Get loginuser https://api.facebook.com/method/users.getinfo?access_token=

        Dim userID1 As String = "https://api.facebook.com/method/users.getLoggedInUser?" + access_token
        Dim url1 As New WebPageUrl(userID1)
        Dim bro As New SimpleBrowser
        bro.Navigate(url1)
        Dim userID As String = bro.LastPageText()

        Dim countrycount As String = ""
        Dim settings As New XmlReaderSettings()
        settings.IgnoreComments = True

        Using reader As XmlReader = XmlReader.Create(New System.IO.StringReader(userID), settings)
            While (reader.Read())
                Select Case reader.NodeType
                    Case XmlNodeType.Text
                        countrycount = reader.Value
                End Select
            End While
        End Using

        Return countrycount
    End Function
End Class

''' <summary>
''' Class Containig Facebook User Info
''' </summary>
<Serializable()>
Public Class FBUserInfo
    Private uidtmp As String
    Property uid() As String
        Get
            Return uidtmp
        End Get
        Set(ByVal Value As String)
            uidtmp = Value
        End Set
    End Property

    Private emailtmp As String
    Property email() As String
        Get
            Return emailtmp
        End Get
        Set(ByVal Value As String)
            emailtmp = Value
        End Set
    End Property

    'Current Location
    Private cyrrent_locationcitytmp As String
    Property current_location_City() As String
        Get
            Return cyrrent_locationcitytmp
        End Get
        Set(ByVal Value As String)
            cyrrent_locationcitytmp = Value
        End Set
    End Property

    Private cyrrent_locationstatetmp As String
    Property current_location_State() As String
        Get
            Return cyrrent_locationstatetmp
        End Get
        Set(ByVal Value As String)
            cyrrent_locationstatetmp = Value
        End Set
    End Property

    Private cyrrent_locationcountrytmp As String
    Property current_location_Country() As String
        Get
            Return cyrrent_locationcountrytmp
        End Get
        Set(ByVal Value As String)
            cyrrent_locationcountrytmp = Value
        End Set
    End Property

    Private cyrrent_locationziptmp As String
    Property current_location_Zip() As String
        Get
            Return cyrrent_locationziptmp
        End Get
        Set(ByVal Value As String)
            cyrrent_locationziptmp = Value
        End Set
    End Property

    Private cyrrent_locationidtmp As String
    Property current_location_Id() As String
        Get
            Return cyrrent_locationidtmp
        End Get
        Set(ByVal Value As String)
            cyrrent_locationidtmp = Value
        End Set
    End Property

    Private cyrrent_locationnametmp As String
    Property current_location_Name() As String
        Get
            Return cyrrent_locationnametmp
        End Get
        Set(ByVal Value As String)
            cyrrent_locationnametmp = Value
        End Set
    End Property

    'Hometown Location
    Private hometown_locationcitytmp As String
    Property hometown_location_City() As String
        Get
            Return hometown_locationcitytmp
        End Get
        Set(ByVal Value As String)
            hometown_locationcitytmp = Value
        End Set
    End Property

    Private hometown_locationstatetmp As String
    Property hometown_location_State() As String
        Get
            Return hometown_locationstatetmp
        End Get
        Set(ByVal Value As String)
            hometown_locationstatetmp = Value
        End Set
    End Property

    Private hometown_locationcountrytmp As String
    Property hometown_location_Country() As String
        Get
            Return hometown_locationcountrytmp
        End Get
        Set(ByVal Value As String)
            hometown_locationcountrytmp = Value
        End Set
    End Property

    Private hometown_locationziptmp As String
    Property hometown_location_Zip() As String
        Get
            Return hometown_locationziptmp
        End Get
        Set(ByVal Value As String)
            hometown_locationziptmp = Value
        End Set
    End Property

    Private hometown_locationidtmp As String
    Property hometown_location_Id() As String
        Get
            Return hometown_locationidtmp
        End Get
        Set(ByVal Value As String)
            hometown_locationidtmp = Value
        End Set
    End Property

    Private hometown_locationnametmp As String
    Property hometown_location_Name() As String
        Get
            Return hometown_locationnametmp
        End Get
        Set(ByVal Value As String)
            hometown_locationnametmp = Value
        End Set
    End Property

    Property sex As String

    Property birthday_date As String

    Property username As String

    Property birthday As String

    Property meeting_sex As String

    Property first_name As String

    Property middle_name As String

    Property last_name As String

    Property name As String

End Class

﻿'Imports Dating.Server.Site.Web.Security
Imports Dating.Server.Core.DLL
Imports Dating.Server.Datasets.DLL

Public Class clsWebStatistics


    Public Function AddNew(ByVal SessionID As String, _
        ByVal Referrer As String, _
        ByVal CustomReferrer As String, _
        ByVal IP As String, _
        ByVal Agent As String, _
        ByVal Page As String, _
        ByVal LoginName As String, _
        ByVal StatCookie As String, _
        ByVal CustomAction As String, _
        ByVal GEO_COUNTRY_CODE As String) As Boolean

        Try
            ' min kratas statical for local
            '    If IP = "127.0.0.1" Then Return True

            Dim ta As New DSWebStatisticsTableAdapters.SYS_WebStatisticsTableAdapter
            ta.Connection = New Data.SqlClient.SqlConnection(ModGlobals.SqlConnectionString)
            Dim ds As New DSWebStatistics.SYS_WebStatisticsDataTable
            Dim row As DSWebStatistics.SYS_WebStatisticsRow
            row = ds.NewSYS_WebStatisticsRow
            row.Item("DateTime") = Now
            row.Item("SessionID") = CheckString(SessionID, 100, "")
            row.Item("Referrer") = CheckString(Referrer, 200, "")
            row.Item("CustomReferrer") = CheckString(CustomReferrer, 100, "")
            row.Item("IP") = CheckString(IP, 30, "")
            row.Item("Agent") = CheckString(Agent, 100, "")
            row.Item("Page") = CheckString(Page, 100, "")
            row.Item("LoginName") = CheckString(LoginName, 30, "")
            row.Item("CustomAction") = CheckString(CustomAction, 100, "")
            row.Item("StatCookie") = CheckString(StatCookie, 100, "")
            row.Item("GEO_COUNTRY_CODE") = CheckString(GEO_COUNTRY_CODE, 2, "NN")
            Dim SearchEngineKeywords As String = GetKeywords(Referrer)
            row.Item("SearchEngineKeywords") = CheckString(SearchEngineKeywords, 100, "")

            ta.Update(row)

            Return True
        Catch ex As Exception
            WebErrorMessageBox("clsWebStatistics->AddNew", ex, "")
            Return False
        End Try
    End Function

    Private Function CheckString(ByVal szString As String, ByVal iMax As Integer, ByVal DefaultString As String) As String
        Try
            If Len(szString) > 0 Then
                Return Mid(szString, 1, iMax)
            Else
                Return DefaultString
            End If
        Catch ex As Exception
            WebErrorMessageBox("clsWebStatistics->CheckString", ex, "")
        End Try

        Return Nothing
    End Function


    'Public Function AddNew(ByVal mBasePage As BasePage, ByVal SessionID, ByVal Referrer, ByVal CustomReferrer, ByVal IP, ByVal Agency, ByVal Page, ByVal LoginName, ByVal CustomAction, ByVal SpyCokkie)
    Public Function AddNew(ByVal mPage As System.Web.HttpApplication)
        Try

            mPage.Application.Lock()

            Dim sessVars As clsSessionVariables = DirectCast(mPage.Session("SessionVariables"), clsSessionVariables)

            AddNew( _
             mPage.Session.SessionID, _
             mPage.Session("Referrer"), _
             mPage.Session("CustomReferrer"), _
             mPage.Session("IP"), _
             mPage.Session("Agent"), _
             sessVars.Page, _
             sessVars.IdentityLoginName, _
             mPage.Session("StatCookie"), _
             mPage.Session("StaticalCustomAction"), _
             mPage.Session("GEO_COUNTRY_CODE"))
            mPage.Application.UnLock()
        Catch ex As Exception
            WebErrorMessageBox("clsWebStatistics->AddNew", ex, "")
            mPage.Application.UnLock()
        End Try

        Return Nothing
    End Function

    Public Function AddNew(ByVal mPage As BasePage)
        Try
            mPage.Application.Lock()

            Dim sessVars As clsSessionVariables = DirectCast(mPage.Session("SessionVariables"), clsSessionVariables)

            AddNew( _
             mPage.Session.SessionID, _
             mPage.Session("Referrer"), _
             mPage.Session("CustomReferrer"), _
             mPage.Session("IP"), _
             mPage.Session("Agent"), _
             sessVars.Page, _
             HttpContext.Current.User.Identity.Name, _
             mPage.Session("StatCookie"), _
             mPage.Session("StaticalCustomAction"), _
             mPage.Session("GEO_COUNTRY_CODE"))

            mPage.Application.UnLock()
        Catch ex As Exception
            WebErrorMessageBox("clsWebStatistics->AddNew2", ex, "")
            mPage.Application.UnLock()
        End Try

        Return Nothing
    End Function

    Public Function AddNew(ByVal mPage As BasePageMobileAdmin)
        Try
            mPage.Application.Lock()

            Dim sessVars As clsSessionVariables = DirectCast(mPage.Session("SessionVariables"), clsSessionVariables)

            AddNew( _
             mPage.Session.SessionID, _
             mPage.Session("Referrer"), _
             mPage.Session("CustomReferrer"), _
             mPage.Session("IP"), _
             mPage.Session("Agent"), _
             sessVars.Page, _
             HttpContext.Current.User.Identity.Name, _
             mPage.Session("StatCookie"), _
             mPage.Session("StaticalCustomAction"), _
             mPage.Session("GEO_COUNTRY_CODE"))

            mPage.Application.UnLock()
        Catch ex As Exception
            WebErrorMessageBox("clsWebStatistics->AddNew2", ex, "")
            mPage.Application.UnLock()
        End Try

        Return Nothing
    End Function

    Private Function GetKeywords(ByVal urlReferrer As String) As String
        If String.IsNullOrEmpty(urlReferrer) Then Return ""

        Try
            Dim searchQuery = String.Empty
            Dim url = New Uri(urlReferrer)
            Dim query = HttpUtility.ParseQueryString(urlReferrer)
            Select Case url.Host
                Case "google", "daum", "msn", "bing", "ask", "altavista", _
                 "alltheweb", "live", "najdi", "aol", "seznam", "search", _
                 "szukacz", "pchome", "kvasir", "sesam", "ozu", "mynet", _
                 "ekolay"
                    searchQuery = query("q")
                    Exit Select
                Case "naver", "netscape", "mama", "mamma", "terra", "cnn"
                    searchQuery = query("query")
                    Exit Select
                Case "virgilio", "alice"
                    searchQuery = query("qs")
                    Exit Select
                Case "yahoo"
                    searchQuery = query("p")
                    Exit Select
                Case "onet"
                    searchQuery = query("qt")
                    Exit Select
                Case "eniro"
                    searchQuery = query("search_word")
                    Exit Select
                Case "about"
                    searchQuery = query("terms")
                    Exit Select
                Case "voila"
                    searchQuery = query("rdata")
                    Exit Select
                Case "baidu"
                    searchQuery = query("wd")
                    Exit Select
                Case "yandex"
                    searchQuery = query("text")
                    Exit Select
                Case "szukaj"
                    searchQuery = query("wp")
                    Exit Select
                Case "yam"
                    searchQuery = query("k")
                    Exit Select
                Case "rambler"
                    searchQuery = query("words")
                    Exit Select
                Case Else
                    searchQuery = query("q")
                    Exit Select
            End Select

            If Not String.IsNullOrEmpty(searchQuery) Then
                searchQuery = searchQuery.Replace("+", " ")
            End If

            Return searchQuery
        Catch ex As Exception
            WebErrorMessageBox("clsWebStatistics->GetKeywords", ex, "")
            Return ""
        End Try
    End Function

End Class

﻿Imports Dating.Server.Core.DLL
Imports Dating.Server.Datasets.DLL


Public Class mobitem
    Inherits BasePageMobileAdmin


    Dim members As DSMembers

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Sub lnkApprove_Click(sender As Object, e As EventArgs) Handles lnkApprove.Click
        Try

            AdminActions.ApproveProfile(Request.QueryString("p"))
            Response.Redirect("default.aspx", False)

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub lnkReject_Click(sender As Object, e As EventArgs) Handles lnkReject.Click
        Response.Redirect("mobrejectitem.aspx?p=" & Request.QueryString("p"))
    End Sub
End Class
﻿<%@ Page Language="vb" AutoEventWireup="false" 
CodeBehind="mobitem.aspx.vb" 
MasterPageFile="~/Site1.Master" 
Inherits="Dating.Server.Site.Mobile.Web.mobitem" %>
<%@ Register assembly="DevExpress.Web.v13.1, Version=13.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxDataView" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <dx:ASPxDataView ID="ASPxDataView1" runat="server" ColumnCount="1" 
            DataSourceID="sdsProfile" RowPerPage="1">
            <ItemTemplate>
                <%--<b>ProfileID</b>:
                <asp:Label ID="ProfileIDLabel" runat="server" Text='<%# Eval("ProfileID") %>' />
                <br/>
                <b>IsMaster</b>:
                <asp:Label ID="IsMasterLabel" runat="server" Text='<%# Eval("IsMaster") %>' />
                <br/>
                <b>MirrorProfileID</b>:
                <asp:Label ID="MirrorProfileIDLabel" runat="server" 
                    Text='<%# Eval("MirrorProfileID") %>' />
                <br/>
                <b>Status</b>:
                <asp:Label ID="StatusLabel" runat="server" Text='<%# Eval("Status") %>' />
                <br/>--%>
                <b>LoginName</b>:
                <asp:Label ID="LoginNameLabel" runat="server" Text='<%# Eval("LoginName") %>' />
                <br/>
                <%--<b>Password</b>:
                <asp:Label ID="PasswordLabel" runat="server" Text='<%# Eval("Password") %>' />
                <br/>--%>
                <b>FirstName</b>:
                <asp:Label ID="FirstNameLabel" runat="server" Text='<%# Eval("FirstName") %>' />
                <br/>
                <b>LastName</b>:
                <asp:Label ID="LastNameLabel" runat="server" Text='<%# Eval("LastName") %>' />
                <br/>
                <%--<b>Gender</b>:
                <asp:Label ID="GenderLabel" runat="server" Text='<%# Eval("Gender") %>' />
                <br/>
                <b>CountryName</b>:
                <asp:Label ID="CountryNameLabel" runat="server" 
                    Text='<%# Eval("CountryName") %>' />
                <br/>
                <b>Region</b>:
                <asp:Label ID="RegionLabel" runat="server" Text='<%# Eval("Region") %>' />
                <br/>
                <b>City</b>:
                <asp:Label ID="CityLabel" runat="server" Text='<%# Eval("City") %>' />
                <br/>--%>
                <b>Zip</b>:
                <asp:Label ID="ZipLabel" runat="server" Text='<%# Eval("Zip") %>' />
                <br/>
                <%--<b>CityArea</b>:
                <asp:Label ID="CityAreaLabel" runat="server" Text='<%# Eval("CityArea") %>' />
                <br/>--%>
                <b>Address</b>:
                <asp:Label ID="AddressLabel" runat="server" Text='<%# Eval("Address") %>' />
                <br/>
                <b>Telephone</b>:
                <asp:Label ID="TelephoneLabel" runat="server" Text='<%# Eval("Telephone") %>' />
                <br/>
                <b>eMail</b>:
                <asp:Label ID="eMailLabel" runat="server" Text='<%# Eval("eMail") %>' />
                <br/>
                <b>Cellular</b>:
                <asp:Label ID="CellularLabel" runat="server" Text='<%# Eval("Cellular") %>' />
                <br/>
                <%--<b>AreYouWillingToTravel</b>:
                <asp:Label ID="AreYouWillingToTravelLabel" runat="server" 
                    Text='<%# Eval("AreYouWillingToTravel") %>' />
                <br/>--%>
                <b>AboutMe&nbsp;Heading</b>:
                <asp:Label ID="AboutMe_HeadingLabel" runat="server" 
                    Text='<%# Eval("AboutMe_Heading") %>' />
                <br/>
                <b>AboutMe&nbsp;DescribeYourself</b>:
                <asp:Label ID="AboutMe_DescribeYourselfLabel" runat="server" 
                    Text='<%# Eval("AboutMe_DescribeYourself") %>' />
                <br/>
                <b>AboutMe&nbsp;DescribeAnIdealFirstDate</b>:
                <asp:Label ID="AboutMe_DescribeAnIdealFirstDateLabel" runat="server" 
                    Text='<%# Eval("AboutMe_DescribeAnIdealFirstDate") %>' />
                <br/>
                <%--<b>Education</b>:
                <asp:Label ID="EducationLabel" runat="server" 
                    Text='<%# Eval("Education") %>' />
                <br/>
                <b>AnnualIncome</b>:
                <asp:Label ID="AnnualIncomeLabel" runat="server" 
                    Text='<%# Eval("AnnualIncome") %>' />
                <br/>
                <b>NetWorth</b>:
                <asp:Label ID="NetWorthLabel" runat="server" 
                    Text='<%# Eval("NetWorth") %>' />
                <br/>--%>
                <b>OtherDetails&nbsp;Occupation</b>:
                <asp:Label ID="OtherDetails_OccupationLabel" runat="server" 
                    Text='<%# Eval("OtherDetails_Occupation") %>' />
                <br/>
                <%--<b>Height</b>:
                <asp:Label ID="HeightLabel" runat="server" 
                    Text='<%# Eval("Height") %>' />
                <br/>
                <b>BodyType</b>:
                <asp:Label ID="BodyTypeLabel" runat="server" 
                    Text='<%# Eval("BodyType") %>' />
                <br/>
                <b>EyeColor</b>:
                <asp:Label ID="EyeColorLabel" runat="server" 
                    Text='<%# Eval("EyeColor") %>' />
                <br/>
                <b>HairColor</b>:
                <asp:Label ID="HairColorLabel" runat="server" 
                    Text='<%# Eval("HairColor") %>' />
                <br/>
                <b>Children</b>:
                <asp:Label ID="ChildrenLabel" runat="server" 
                    Text='<%# Eval("Children") %>' />
                <br/>
                <b>Ethnicity</b>:
                <asp:Label ID="EthnicityLabel" runat="server" 
                    Text='<%# Eval("Ethnicity") %>' />
                <br/>
                <b>Religion</b>:
                <asp:Label ID="ReligionLabel" runat="server" 
                    Text='<%# Eval("Religion") %>' />
                <br/>
                <b>Smoking</b>:
                <asp:Label ID="SmokingLabel" runat="server" 
                    Text='<%# Eval("Smoking") %>' />
                <br/>
                <b>Drinking</b>:
                <asp:Label ID="DrinkingLabel" runat="server" 
                    Text='<%# Eval("Drinking") %>' />
                <br/>
                <b>LookingFor_ToMeetMaleID</b>:
                <asp:Label ID="LookingFor_ToMeetMaleIDLabel" runat="server" 
                    Text='<%# Eval("LookingFor_ToMeetMaleID") %>' />
                <br/>
                <b>LookingFor_ToMeetFemaleID</b>:
                <asp:Label ID="LookingFor_ToMeetFemaleIDLabel" runat="server" 
                    Text='<%# Eval("LookingFor_ToMeetFemaleID") %>' />
                <br/>
                <b>RelationshipStatus</b>:
                <asp:Label ID="RelationshipStatusLabel" runat="server" 
                    Text='<%# Eval("RelationshipStatus") %>' />
                <br/>
                <b>ShortTermRelationship</b>:
                <asp:Label ID="ShortTermRelationshipLabel" 
                    runat="server" 
                    Text='<%# Eval("ShortTermRelationship") %>' />
                <br/>
                <b>LookingFor_TypeOfDating_Friendship</b>:
                <asp:Label ID="LookingFor_TypeOfDating_FriendshipLabel" runat="server" 
                    Text='<%# Eval("LookingFor_TypeOfDating_Friendship") %>' />
                <br/>
                <b>LookingFor_TypeOfDating_LongTermRelationship</b>:
                <asp:Label ID="LookingFor_TypeOfDating_LongTermRelationshipLabel" 
                    runat="server" 
                    Text='<%# Eval("LookingFor_TypeOfDating_LongTermRelationship") %>' />
                <br/>
                <b>LookingFor_TypeOfDating_MutuallyBeneficialArrangements</b>:
                <asp:Label ID="LookingFor_TypeOfDating_MutuallyBeneficialArrangementsLabel" 
                    runat="server" 
                    Text='<%# Eval("LookingFor_TypeOfDating_MutuallyBeneficialArrangements") %>' />
                <br/>
                <b>LookingFor_TypeOfDating_MarriedDating</b>:
                <asp:Label ID="LookingFor_TypeOfDating_MarriedDatingLabel" runat="server" 
                    Text='<%# Eval("LookingFor_TypeOfDating_MarriedDating") %>' />
                <br/>
                <b>LookingFor_TypeOfDating_AdultDating_Casual</b>:
                <asp:Label ID="LookingFor_TypeOfDating_AdultDating_CasualLabel" runat="server" 
                    Text='<%# Eval("LookingFor_TypeOfDating_AdultDating_Casual") %>' />
                <br/>
                <b>PrivacySettings_HideMeFromSearchResults</b>:
                <asp:Label ID="PrivacySettings_HideMeFromSearchResultsLabel" runat="server" 
                    Text='<%# Eval("PrivacySettings_HideMeFromSearchResults") %>' />
                <br/>
                <b>PrivacySettings_HideMeFromMembersIHaveBlocked</b>:
                <asp:Label ID="PrivacySettings_HideMeFromMembersIHaveBlockedLabel" 
                    runat="server" 
                    Text='<%# Eval("PrivacySettings_HideMeFromMembersIHaveBlocked") %>' />
                <br/>
                <b>PrivacySettings_NotShowInOtherUsersFavoritedList</b>:
                <asp:Label ID="PrivacySettings_NotShowInOtherUsersFavoritedListLabel" 
                    runat="server" 
                    Text='<%# Eval("PrivacySettings_NotShowInOtherUsersFavoritedList") %>' />
                <br/>
                <b>PrivacySettings_NotShowInOtherUsersViewedList</b>:
                <asp:Label ID="PrivacySettings_NotShowInOtherUsersViewedListLabel" 
                    runat="server" 
                    Text='<%# Eval("PrivacySettings_NotShowInOtherUsersViewedList") %>' />
                <br/>
                <b>PrivacySettings_NotShowMyGifts</b>:
                <asp:Label ID="PrivacySettings_NotShowMyGiftsLabel" runat="server" 
                    Text='<%# Eval("PrivacySettings_NotShowMyGifts") %>' />
                <br/>
                <b>PrivacySettings_HideMyLastLoginDateTime</b>:
                <asp:Label ID="PrivacySettings_HideMyLastLoginDateTimeLabel" runat="server" 
                    Text='<%# Eval("PrivacySettings_HideMyLastLoginDateTime") %>' />
                <br/>
                --%>
                <b>DateTimeToRegister</b>:
                <asp:Label ID="DateTimeToRegisterLabel" runat="server" 
                    Text='<%# Eval("DateTimeToRegister") %>' />
                <br/>
<%--
                <b>RegisterIP</b>:
                <asp:Label ID="RegisterIPLabel" runat="server" 
                    Text='<%# Eval("RegisterIP") %>' />
                <br/>
                <b>RegisterGEOInfos</b>:
                <asp:Label ID="RegisterGEOInfosLabel" runat="server" 
                    Text='<%# Eval("RegisterGEOInfos") %>' />
                <br/>
                <b>RegisterUserAgent</b>:
                <asp:Label ID="RegisterUserAgentLabel" runat="server" 
                    Text='<%# Eval("RegisterUserAgent") %>' />
                <br/>
                <b>LastLoginDateTime</b>:
                <asp:Label ID="LastLoginDateTimeLabel" runat="server" 
                    Text='<%# Eval("LastLoginDateTime") %>' />
                <br/>
                <b>LastLoginIP</b>:
                <asp:Label ID="LastLoginIPLabel" runat="server" 
                    Text='<%# Eval("LastLoginIP") %>' />
                <br/>
                <b>LastLoginGEOInfos</b>:
                <asp:Label ID="LastLoginGEOInfosLabel" runat="server" 
                    Text='<%# Eval("LastLoginGEOInfos") %>' />
                <br/>
                <b>LastUpdateProfileDateTime</b>:
                <asp:Label ID="LastUpdateProfileDateTimeLabel" runat="server" 
                    Text='<%# Eval("LastUpdateProfileDateTime") %>' />
                <br/>
                <b>LastUpdateProfileIP</b>:
                <asp:Label ID="LastUpdateProfileIPLabel" runat="server" 
                    Text='<%# Eval("LastUpdateProfileIP") %>' />
                <br/>
                <b>LastUpdateProfileGEOInfo</b>:
                <asp:Label ID="LastUpdateProfileGEOInfoLabel" runat="server" 
                    Text='<%# Eval("LastUpdateProfileGEOInfo") %>' />
                <br/>
                <b>LAGID</b>:
                <asp:Label ID="LAGIDLabel" runat="server" Text='<%# Eval("LAGID") %>' />
                <br/>
                <b>ThemeName</b>:
                <asp:Label ID="ThemeNameLabel" runat="server" Text='<%# Eval("ThemeName") %>' />
                <br/>
                <b>Referrer</b>:
                <asp:Label ID="ReferrerLabel" runat="server" Text='<%# Eval("Referrer") %>' />
                <br/>
                <b>CustomReferrer</b>:
                <asp:Label ID="CustomReferrerLabel" runat="server" 
                    Text='<%# Eval("CustomReferrer") %>' />
                <br/>
                <b>Birthday</b>:
                <asp:Label ID="BirthdayLabel" runat="server" Text='<%# Eval("Birthday") %>' />
                <br/>
                <b>Role</b>:
                <asp:Label ID="RoleLabel" runat="server" Text='<%# Eval("Role") %>' />
                <br/>
                <b>AccountTypeId</b>:
                <asp:Label ID="AccountTypeIdLabel" runat="server" 
                    Text='<%# Eval("AccountTypeId") %>' />
                <br/>
                <b>IsOnline</b>:
                <asp:Label ID="IsOnlineLabel" runat="server" Text='<%# Eval("IsOnline") %>' />
                <br/>
                <b>PointsVerification</b>:
                <asp:Label ID="PointsVerificationLabel" runat="server" 
                    Text='<%# Eval("PointsVerification") %>' />
                <br/>
                <b>PointsBeauty</b>:
                <asp:Label ID="PointsBeautyLabel" runat="server" 
                    Text='<%# Eval("PointsBeauty") %>' />
                <br/>
                <b>PointsCredits</b>:
                <asp:Label ID="PointsCreditsLabel" runat="server" 
                    Text='<%# Eval("PointsCredits") %>' />
                <br/>
                <b>PointsUnlocks</b>:
                <asp:Label ID="PointsUnlocksLabel" runat="server" 
                    Text='<%# Eval("PointsUnlocks") %>' />
                <br/>
                <b>FacebookUserId</b>:
                <asp:Label ID="FacebookUserIdLabel" runat="server" 
                    Text='<%# Eval("FacebookUserId") %>' />
                <br/>
                <b>FacebookName</b>:
                <asp:Label ID="FacebookNameLabel" runat="server" 
                    Text='<%# Eval("FacebookName") %>' />
                <br/>
                <b>TwitterUserId</b>:
                <asp:Label ID="TwitterUserIdLabel" runat="server" 
                    Text='<%# Eval("TwitterUserId") %>' />
                <br/>
                <b>TwitterName</b>:
                <asp:Label ID="TwitterNameLabel" runat="server" 
                    Text='<%# Eval("TwitterName") %>' />
                <br/>
--%>
            </ItemTemplate>
        </dx:ASPxDataView>
        <asp:LinkButton ID="lnkApprove" runat="server" OnClientClick="return confirm('You are about to APPROVE this profile. \r\n Continue?');">Approve</asp:LinkButton>
    &nbsp;&nbsp;
        <asp:LinkButton ID="lnkReject" runat="server">Reject</asp:LinkButton>
    &nbsp;&nbsp; &nbsp;&nbsp;
        <asp:HyperLink ID="lnkList" runat="server" NavigateUrl="default.aspx">Go to list</asp:HyperLink>

    <asp:SqlDataSource ID="sdsProfile" runat="server" 
        ConnectionString="<%$ ConnectionStrings:AppDBconnectionString %>" SelectCommand="
SELECT 
    ProfileID, IsMaster, MirrorProfileID, Status, 
    LoginName, Password, FirstName, LastName,
    (select top(1) US from dbo.EUS_LISTS_Gender where GenderId=GenderId) 
		as Gender,
    (select top(1) PrintableName from dbo.SYS_CountriesGEO where Iso=Country) 
		as CountryName,
    Region, City, Zip, CityArea, 
    Address, Telephone, eMail, 
    Cellular, AreYouWillingToTravel, 
    AboutMe_Heading, AboutMe_DescribeYourself, AboutMe_DescribeAnIdealFirstDate, 
    (select top(1) US from EUS_LISTS_Education where EducationId=OtherDetails_EducationID) 
		as Education,
    (select top(1) US from EUS_LISTS_Income where IncomeId=OtherDetails_AnnualIncomeID) 
		as AnnualIncome,
    (select top(1) US from dbo.EUS_LISTS_NetWorth where NetWorthId=OtherDetails_NetWorthID) 
		as NetWorth,
    OtherDetails_Occupation,
    (select top(1) US from dbo.EUS_LISTS_Height where HeightId=PersonalInfo_HeightID) as Height,
    (select top(1) US from dbo.EUS_LISTS_BodyType where BodyTypeId=PersonalInfo_BodyTypeID) as BodyType,
    (select top(1) US from dbo.EUS_LISTS_EyeColor where EyeColorId=PersonalInfo_EyeColorID) as EyeColor,
    (select top(1) US from dbo.EUS_LISTS_HairColor where HairColorId=PersonalInfo_HairColorID) as HairColor,
    (select top(1) US from dbo.EUS_LISTS_ChildrenNumber where ChildrenNumberId=PersonalInfo_ChildrenID) as Children,
    (select top(1) US from dbo.EUS_LISTS_Ethnicity where EthnicityId=PersonalInfo_EthnicityID) as Ethnicity,
    (select top(1) US from dbo.EUS_LISTS_Religion where ReligionId=PersonalInfo_ReligionID) as Religion,
    (select top(1) US from dbo.EUS_LISTS_Smoking where SmokingId=PersonalInfo_SmokingHabitID) as Smoking,
    (select top(1) US from dbo.EUS_LISTS_Drinking where DrinkingId=PersonalInfo_DrinkingHabitID) as Drinking,
    LookingFor_ToMeetMaleID, 
    LookingFor_ToMeetFemaleID, 
    (select top(1) US from dbo.EUS_LISTS_RelationshipStatus where RelationshipStatusId=LookingFor_RelationshipStatusID) 
        as RelationshipStatus,
    CASE
		WHEN  LookingFor_TypeOfDating_ShortTermRelationship=1 THEN 'Short Term Relationship / Dating'
		ELSE ''
	END as ShortTermRelationship,
    LookingFor_TypeOfDating_Friendship, 
    LookingFor_TypeOfDating_LongTermRelationship,
    LookingFor_TypeOfDating_MutuallyBeneficialArrangements, 
    LookingFor_TypeOfDating_MarriedDating,
    LookingFor_TypeOfDating_AdultDating_Casual, 
    PrivacySettings_HideMeFromSearchResults, 
    PrivacySettings_HideMeFromMembersIHaveBlocked, 
    PrivacySettings_NotShowInOtherUsersFavoritedList, 
    PrivacySettings_NotShowInOtherUsersViewedList, 
    PrivacySettings_NotShowMyGifts, 
    PrivacySettings_HideMyLastLoginDateTime, 
    DateTimeToRegister, RegisterIP, RegisterGEOInfos, 
    RegisterUserAgent, LastLoginDateTime, LastLoginIP, LastLoginGEOInfos, LastUpdateProfileDateTime, 
    LastUpdateProfileIP, LastUpdateProfileGEOInfo, LAGID, ThemeName, Referrer, CustomReferrer, 
    Birthday, 
    Role, AccountTypeId, 
    IsOnline, 
    PointsVerification, 
    PointsBeauty, PointsCredits, PointsUnlocks, 
    FacebookUserId, FacebookName, 
    TwitterUserId, TwitterName
FROM EUS_Profiles 
WHERE (LoginName = @LoginName) AND (Status = 1 or Status=2)
">
        <SelectParameters>
            <asp:QueryStringParameter Name="LoginName" QueryStringField="p" />
        </SelectParameters>
    </asp:SqlDataSource>

</asp:Content>


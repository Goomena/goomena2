﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Site1.Master" CodeBehind="default.aspx.vb" Inherits="Dating.Server.Site.Mobile.Web.moblist" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
        <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="sdsNewProfiles"
            KeyFieldName="ProfileID">
            <Columns>
                <dx:GridViewDataTextColumn FieldName="ProfileID" ReadOnly="True" 
                    VisibleIndex="10" Visible="False">
                    <PropertiesTextEdit EncodeHtml="False">
                    </PropertiesTextEdit>
                    <EditFormSettings Visible="False" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="LoginName" VisibleIndex="2" 
                    Visible="False">
                    <PropertiesTextEdit EncodeHtml="False">
                    </PropertiesTextEdit>
                    <EditFormSettings Visible="False" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="FirstName" VisibleIndex="3" 
                    Visible="False">
                    <PropertiesTextEdit EncodeHtml="False">
                    </PropertiesTextEdit>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="LastName" VisibleIndex="4" 
                    Visible="False">
                    <PropertiesTextEdit EncodeHtml="False">
                    </PropertiesTextEdit>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Gender" VisibleIndex="5" Visible="False">
                    <PropertiesTextEdit EncodeHtml="False">
                    </PropertiesTextEdit>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Country" VisibleIndex="6" Visible="False">
                    <PropertiesTextEdit EncodeHtml="False">
                    </PropertiesTextEdit>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="eMail" VisibleIndex="7" Visible="False">
                    <PropertiesTextEdit EncodeHtml="False">
                    </PropertiesTextEdit>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Status" VisibleIndex="8" Visible="False">
                    <PropertiesTextEdit EncodeHtml="False">
                    </PropertiesTextEdit>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataDateColumn FieldName="DateTimeToRegister" VisibleIndex="9">
                    <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy H:mm">
                    </PropertiesDateEdit>
                </dx:GridViewDataDateColumn>
                <dx:GridViewDataHyperLinkColumn Caption="view" FieldName="LoginName" VisibleIndex="0">
                    <PropertiesHyperLinkEdit EncodeHtml="False" NavigateUrlFormatString="mobitem.aspx?p={0}">
                    </PropertiesHyperLinkEdit>
                </dx:GridViewDataHyperLinkColumn>
                <dx:GridViewDataTextColumn Caption="CurrentStatus" FieldName="CurrentStatus" 
                    VisibleIndex="11">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataColumn Caption="" FieldName="NewPhotos" VisibleIndex="1">
                    <DataItemTemplate>
                        <asp:HyperLink ID="HyperLink1" runat="server" 
                            NavigateUrl='<%# "mobphotos.aspx?p=" & Eval("LoginName") %>' Text='<%# "photos " & Eval("NewPhotos") %>' 
                           Visible='<%#Eval("NewPhotos") > 0 %>'></asp:HyperLink>
                    </DataItemTemplate>
                </dx:GridViewDataColumn>
            </Columns>
            <SettingsPager PageSize="20">
            </SettingsPager>
        </dx:ASPxGridView>
        <asp:SqlDataSource ID="sdsNewProfiles" runat="server" ConnectionString="<%$ ConnectionStrings:AppDBconnectionString %>"
            SelectCommand="
SELECT     ProfileID, LoginName, FirstName, LastName, 
  Gender = case
	when GenderId= 1 then 'Male'
	when GenderId= 2 then 'Female'
	end,
  Country, eMail, 
  CurrentStatus = case
	when Status= 1 then 'New'
	when Status= 2 then 'Updating'
	end,
  DateTimeToRegister, 
  NewPhotos =(select count(*) from EUS_CustomerPhotos where HasAproved=0 and HasDeclined=0 and (customerId=EUS_Profiles.ProfileID or customerId=EUS_Profiles.MirrorProfileID))
FROM         EUS_Profiles
WHERE     (Status = 1 or Status=2)
ORDER BY CurrentStatus, DateTimeToRegister
            "></asp:SqlDataSource>

</asp:Content>

﻿<%@ Page Language="vb" AutoEventWireup="false" 
MasterPageFile="~/Site1.Master" 
CodeBehind="mobphotos.aspx.vb" Inherits="Dating.Server.Site.Mobile.Web.mobphotos" %>

<%@ Register assembly="DevExpress.Web.v13.1, Version=13.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxDataView" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
<div align="center" style="text-align:center;margin-left:auto;margin-right:auto;width:400px;margin-bottom:10px;">
    <div style="float:right;">
         <asp:HyperLink ID="lnkList" runat="server" NavigateUrl="default.aspx">Go to list</asp:HyperLink>
    </div>
    <div style="clear:both;"></div>
</div>

        <dx:ASPxDataView ID="ASPxDataView1" runat="server" ColumnCount="1" 
            DataSourceID="sdsPhotos" RowPerPage="10">
            <ItemTemplate>
            <table>
                <tr>
                    <td valign="top"><img width="150" src="<%# ConfigurationManager.AppSettings("gSiteURL") & Dating.Server.Site.Mobile.Web.AppUtils.GetImage(Eval("CustomerID").Tostring(), Eval("FileName").Tostring(), Eval("GenderId").Tostring(), true, Me.IsHTTPS).Replace("/Mobile/","/") %>" /></td>
                    <td valign="top" style="padding:0 10px;"> 
                        <table cellpadding="3">
                            <tr>
                                <td><b>LoginName</b>:</td>
                                <td><asp:Label ID="LoginNameLabel" runat="server" Text='<%# Eval("LoginName") %>' /></td>
                            </tr>
                            <tr>
                                <td colspan="2"><b>DateTimeToUploading</b>:<br />
                                    <asp:Label ID="DateTimeToUploadingLabel" runat="server" 
                            Text='<%# Eval("DateTimeToUploading") %>' /></td>
                            </tr>
                            <tr>
                                <td><b>DisplayLevel</b>:</td>
                                <td><asp:DropDownList ID="ddlDisplayLevel" runat="server" 
                                    DataSourceID="sdsPhotosDispLvl" DataTextField="US" 
                                    DataValueField="PhotosDisplayLevelId" Enabled="false" SelectedValue='<%# Eval("DisplayLevel") %>'>
                                                </asp:DropDownList>
                                                <%--<asp:Label ID="DisplayLevelLabel" runat="server" 
                                                    Text='<%# Eval("DisplayLevel") %>' />--%></td>
                            </tr>
                            <%--<tr>
                                <td><b>HasAproved</b>:</td>
                                <td><dx:ASPxCheckBox ID="chkHasAproved" Enabled="false" runat="server" Checked='<%# Eval("HasAproved") %>'>
                                        </dx:ASPxCheckBox></td>
                            </tr>
                            <tr>
                                <td><b>HasDeclined</b>:</td>
                                <td><dx:ASPxCheckBox ID="chkHasDeclined" Enabled="false" runat="server" Checked='<%# Eval("HasDeclined") %>'>
                                    </dx:ASPxCheckBox></td>
                            </tr>
                            <tr>
                                <td><b>IsDefault</b>:</td>
                                <td><dx:ASPxCheckBox ID="chkIsDefault" Enabled="false" runat="server" Checked='<%# Eval("IsDefault") %>'>
                            </dx:ASPxCheckBox></td>
                            </tr>
                            <tr>
                                <td colspan="2"><b>FileName</b>:<br /><asp:Label ID="FileNameLabel" runat="server" Text='<%# Eval("FileName") %>' /></td>
                            </tr>--%>
                        </table>               
                        <div style="padding-top:20px;">
                            <div style="float:left;">
<asp:LinkButton ID="btnApprove" runat="server" CommandArgument='<%# Eval("CustomerPhotosID") %>' 
        CommandName="APPROVE" CssClass="button" Font-Size="18px">Approve</asp:LinkButton>
                            </div>
                            <div style="float:right;">
<asp:LinkButton ID="btnReject" runat="server" CommandArgument='<%# Eval("CustomerPhotosID") %>' 
        CommandName="REJECT" CssClass="button" Font-Size="18px">Reject</asp:LinkButton>
                            </div>
                            <div style="clear:both;"></div>
                        </div>
                    </td>
                </tr>
            </table>
            </ItemTemplate>
            <Paddings Padding="0px" />
        </dx:ASPxDataView>
<%--                                <asp:DropDownList ID="DropDownList1" runat="server" 
            DataSourceID="sdsPhotosDispLvl" DataTextField="US" 
            DataValueField="PhotosDisplayLevelId">
                        </asp:DropDownList>
--%>
        <asp:SqlDataSource ID="sdsPhotosDispLvl" runat="server" 
            ConnectionString="<%$ ConnectionStrings:AppDBconnectionString %>" 
            SelectCommand="SELECT [PhotosDisplayLevelId], [US] FROM [EUS_LISTS_PhotosDisplayLevel]">
        </asp:SqlDataSource>

        <asp:SqlDataSource ID="sdsPhotos" runat="server" 
            ConnectionString="<%$ ConnectionStrings:AppDBconnectionString %>" 
            SelectCommand="
SELECT 
    EUS_CustomerPhotos.CustomerPhotosID, 
    EUS_CustomerPhotos.CustomerID, 
    EUS_CustomerPhotos.DateTimeToUploading,
    EUS_CustomerPhotos.FileName, 
    EUS_CustomerPhotos.DisplayLevel, 
    EUS_CustomerPhotos.HasAproved, 
    EUS_CustomerPhotos.HasDeclined, 
    EUS_CustomerPhotos.IsDefault, 
    EUS_Profiles.ProfileID, 
    EUS_Profiles.LoginName, 
    EUS_Profiles.GenderId 
FROM EUS_CustomerPhotos 
INNER JOIN EUS_Profiles ON EUS_Profiles.ProfileID = EUS_CustomerPhotos.CustomerID 
WHERE (EUS_Profiles.LoginName = @LoginName) 
AND (EUS_CustomerPhotos.HasAproved = 0) 
AND (EUS_CustomerPhotos.HasDeclined = 0)
">
            <SelectParameters>
                <asp:QueryStringParameter Name="LoginName" QueryStringField="p" />
            </SelectParameters>
        </asp:SqlDataSource>
    
</asp:Content>

﻿Imports Dating.Server.Core.DLL



Public Class approve
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Page.IsPostBack) Then
            Try

                If (Not String.IsNullOrEmpty(Request.QueryString("p")) AndAlso Request.QueryString("reject") <> "1") Then
                    AdminActions.ApproveProfile(Request.QueryString("p"))
                    Response.Write("Profile approved.")
                ElseIf (Not String.IsNullOrEmpty(Request.QueryString("p"))) Then
                    Response.Redirect("mobrejectitem.aspx?p=" & Request.QueryString("p"))
                End If

                If (Not String.IsNullOrEmpty(Request.QueryString("photo")) AndAlso Request.QueryString("reject") <> "1") Then
                    AdminActions.ApprovePhoto(Request.QueryString("photo"), False, True)
                    Response.Write("Photo approved succesfully.")
                ElseIf (Not String.IsNullOrEmpty(Request.QueryString("photo"))) Then
                    AdminActions.RejectPhoto(Request.QueryString("photo"))
                    Response.Write("Photo rejected succesfully.")
                End If

            Catch ex As Exception
                Response.Write(ex.Message)
            End Try
        End If
    End Sub

End Class
﻿
Imports Dating.Server.Core.DLL

Public Class mobphotos
    Inherits BasePageMobileAdmin


    Protected Property IsDefaultPhotoSet As Boolean
        Get
            If (ViewState("IsDefaultPhotoSet") Is Nothing) Then
                Return False
            End If

            Return ViewState("IsDefaultPhotoSet")
        End Get
        Set(value As Boolean)
            ViewState("IsDefaultPhotoSet") = value
        End Set
    End Property




    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Sub ASPxDataView1_ItemCommand(source As Object, e As DevExpress.Web.ASPxDataView.DataViewItemCommandEventArgs) Handles ASPxDataView1.ItemCommand

        Try

            If (e.CommandName = "APPROVE") Then
                cmdApprovePhoto(e.CommandArgument, False, True)
            ElseIf (e.CommandName = "REJECT") Then
                cmdRejectPhoto(e.CommandArgument)
            End If

            ASPxDataView1.DataBind()

            If (ASPxDataView1.Items.Count = 0) Then
                Response.Redirect("default.aspx")
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub


    Private Sub cmdApprovePhoto(photoId As Integer,
                                IsUser As Boolean,
                                IsAdmin As Boolean)
        Try

            IsDefaultPhotoSet = AdminActions.ApprovePhoto(photoId, IsUser, IsAdmin)

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub cmdRejectPhoto(photoId As Integer)
        'Try

        '    Dim photo As Core.DLL.EUS_CustomerPhoto = (From itm In Me.CMSDBDataContext.EUS_CustomerPhotos
        '                                      Where itm.CustomerPhotosID = photoId
        '                                      Select itm).FirstOrDefault()

        '    photo.HasAproved = False
        '    photo.HasDeclined = True

        '    Me.CMSDBDataContext.SubmitChanges()

        'Catch ex As Exception
        '    Response.Write(ex.Message)
        'End Try
        Try

            IsDefaultPhotoSet = AdminActions.RejectPhoto(photoId)

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
End Class
﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Site1.Master" 
CodeBehind="mobrejectitem.aspx.vb" 
Inherits="Dating.Server.Site.Mobile.Web.mobrejectitem" 
ValidateRequest="false" %>

<%@ Register assembly="DevExpress.Web.v13.1, Version=13.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxDataView" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">

        <dx:ASPxComboBox ID="cbReason1" runat="server" AutoPostBack="True">
        </dx:ASPxComboBox>
        <br />
        <dx:ASPxMemo ID="txReasonDescr1" runat="server" Height="165px" Width="172px">
        </dx:ASPxMemo>
        <br />
        <br />
        <asp:LinkButton ID="lnkOk" runat="server" 
            onclientclick="return confirm('You are about to REJECT this profile. \r\n Continue?');"> Ok </asp:LinkButton>
&nbsp;
        <asp:LinkButton ID="lnkCancel" runat="server">Cancel</asp:LinkButton>

</asp:Content>

﻿Imports Dating.Server.Core.DLL
Imports System.Data.SqlClient
Imports System.Xml
Imports System.Xml.Linq


Public Module DataHelpers

    Private _ConnectionString As String
    Friend Property ConnectionString As String
        Get
            Return _ConnectionString
        End Get
        Set(value As String)
            _ConnectionString = value
        End Set
    End Property

    Public Sub SetConnectionString(connString)
        _ConnectionString = connString
    End Sub

#Region "EUS_Profiles"

    Public Function GetEUS_Profiles() As DSMembers
        Dim ta As New DSMembersTableAdapters.EUS_ProfilesTableAdapter
        Dim ds As New DSMembers()
        ta.Connection = New SqlConnection(DataHelpers.ConnectionString)
        ta.Fill(ds.EUS_Profiles)

        Return ds
    End Function


    Public Function GetEUS_Profiles_ByProfileID(profileId As Integer) As DSMembers
        Dim ta As New DSMembersTableAdapters.EUS_ProfilesTableAdapter
        Dim ds As New DSMembers()
        ta.Connection = New SqlConnection(DataHelpers.ConnectionString)
        ta.FillByProfileID(ds.EUS_Profiles, profileId)

        Return ds
    End Function


    Public Function GetEUS_Profiles_ByProfileIDRow(profileId As Integer) As DSMembers.EUS_ProfilesRow
        Dim ta As New DSMembersTableAdapters.EUS_ProfilesTableAdapter
        Dim ds As New DSMembers()
        ta.Connection = New SqlConnection(DataHelpers.ConnectionString)
        ta.FillByProfileID(ds.EUS_Profiles, profileId)

        Dim row As DSMembers.EUS_ProfilesRow = ds.EUS_Profiles.Rows(0)
        Return row
    End Function


    Public Function GetEUS_Profiles_ForReferrerBalance(profileId As Integer?, dateFrom As DateTime?, dateTo As DateTime?, Optional PreviousRowNumber As Integer? = Nothing, Optional RecordsToSelect As Integer? = Nothing) As DSReferrers
        Dim ta As New DSReferrersTableAdapters.GetReferrersBalance_ADMINTableAdapter
        Dim ds As New DSReferrers()
        ta.Connection = New SqlConnection(DataHelpers.ConnectionString)
        ta.Fill(ds.GetReferrersBalance_ADMIN, profileId, dateFrom, dateTo, PreviousRowNumber, RecordsToSelect)

        Return ds
    End Function


    Public Function GetEUS_Profiles_ByProfileOrMirrorID(profileId As Integer) As DSMembers
        Dim ta As New DSMembersTableAdapters.EUS_ProfilesTableAdapter
        Dim ds As New DSMembers()
        ta.Connection = New SqlConnection(DataHelpers.ConnectionString)
        ta.FillByProfileOrMirrorID(ds.EUS_Profiles, profileId)

        Return ds
    End Function

    Public Function GetEUS_Profiles_AdminUpdater_ByProfileOrMirrorID(profileId As Integer) As DSMembers
        Dim ta As New DSMembersTableAdapters.EUS_Profiles_AdminUpdaterTableAdapter
        Dim ds As New DSMembers()
        ta.Connection = New SqlConnection(DataHelpers.ConnectionString)
        ta.FillBy_ProfileIDOrMirrorProfileID(ds.EUS_Profiles_AdminUpdater, profileId)

        Return ds
    End Function


    Public Function GetEUS_Profiles_ByLoginName(loginName As String) As DSMembers
        Dim ta As New DSMembersTableAdapters.EUS_ProfilesTableAdapter
        Dim ds As New DSMembers()
        ta.Connection = New SqlConnection(DataHelpers.ConnectionString)
        ta.FillBy_LoginName(ds.EUS_Profiles, loginName)

        Return ds
    End Function


    Public Function GetEUS_Profiles_ByLoginOrEmail(LoginOrEmail As String) As DSMembers
        Dim ta As New DSMembersTableAdapters.EUS_ProfilesTableAdapter
        Dim ds As New DSMembers()
        ta.Connection = New SqlConnection(DataHelpers.ConnectionString)
        ta.FillBy_LoginOrEmail(ds.EUS_Profiles, LoginOrEmail)

        Return ds
    End Function


    Public Function GetEUS_Profiles_ByStatus(profileStatus As Integer) As DSMembers
        Dim ta As New DSMembersTableAdapters.EUS_ProfilesTableAdapter
        Dim ds As New DSMembers()
        ta.Connection = New SqlConnection(DataHelpers.ConnectionString)
        ta.FillByStatus(ds.EUS_Profiles, profileStatus)

        Return ds
    End Function


    Public Function GetEUS_Profiles_ByLoginPass(ByVal LoginName As String, ByVal Password As String) As DSMembers
        Dim ta As New DSMembersTableAdapters.EUS_ProfilesTableAdapter
        Dim ds As New DSMembers()
        ta.Connection = New SqlConnection(DataHelpers.ConnectionString)
        ta.FillByLoginPass(ds.EUS_Profiles, LoginName, Password)

        Return ds
    End Function


    Public Function GetProfileStatistics(CustomerId As Integer, ByVal StartDaysBefore As Integer) As DSMembersViews
        Dim ds As New DSMembersViews()

        Dim ta As New DSMembersViewsTableAdapters.GetProfileStatisticsTableAdapter
        ta.Connection = New SqlConnection(DataHelpers.ConnectionString)
        ta.Fill(ds.GetProfileStatistics, CustomerId, StartDaysBefore)

        Return ds
    End Function



    ' OLD - using credits
    'Public Function GetReferrersCreditsSum_Admin(dateFrom As DateTime?, dateTo As DateTime?) As DSReferrers
    '    Dim ds As New DSReferrers()

    '    Dim ta As New DSReferrersTableAdapters.GetReferrersCreditsSum_AdminTableAdapter
    '    ta.Connection = New SqlConnection(DataHelpers.ConnectionString)
    '    ta.Fill(ds.GetReferrersCreditsSum_Admin, dateFrom, dateTo)

    '    Return ds
    'End Function


    '' NEW - using messages
    'Public Function GetReferrersCreditsSum_Admin2(dateFrom As DateTime?, dateTo As DateTime?) As DSReferrers
    '    Dim ds As New DSReferrers()

    '    Dim ta As New DSReferrersTableAdapters.GetReferrersCreditsSum_Admin2TableAdapter
    '    ta.Connection = New SqlConnection(DataHelpers.ConnectionString)
    '    ta.Fill(ds.GetReferrersCreditsSum_Admin2, dateFrom, dateTo)

    '    Return ds
    'End Function



    'Public Function GetReferrersCreditsSum_Admin3(ReferrerID As Integer, SelectReferrerTree As Boolean, dateFrom As DateTime?, dateTo As DateTime?) As DSReferrers
    '    Dim ds As New DSReferrers()

    '    Dim ta As New DSReferrersTableAdapters.GetReferrersCreditsSum_Admin3TableAdapter
    '    ta.Connection = New SqlConnection(DataHelpers.ConnectionString)
    '    ta.Fill(ds.GetReferrersCreditsSum_Admin3, ReferrerID, SelectReferrerTree, dateFrom, dateTo)

    '    Return ds
    'End Function


    Public Function GetReferrersBonus(dateFrom As DateTime?, dateTo As DateTime?, profileID As Integer?) As DSReferrers
        Dim ds As New DSReferrers()
        Dim ta As New DSReferrersTableAdapters.REF_BonusTableAdapter
        ta.Connection = New SqlConnection(DataHelpers.ConnectionString)
        ta.FillBy_Params(ds.REF_Bonus, dateFrom, dateTo, profileID)

        '        Dim sql As String = <sql><![CDATA[
        ']]></sql>

        '        Dim command As SqlClient.SqlCommand = GetSqlCommand(sql)
        '        If (dateFrom IsNot Nothing) Then
        '            command.Parameters.AddWithValue("@dateFrom", dateFrom)
        '        Else
        '            command.Parameters.AddWithValue("@dateFrom", System.DBNull.Value)
        '        End If
        '        If (dateTo IsNot Nothing) Then
        '            command.Parameters.AddWithValue("@dateTo", dateTo)
        '        Else
        '            command.Parameters.AddWithValue("@dateTo", System.DBNull.Value)
        '        End If
        '        If (profileID IsNot Nothing) Then
        '            command.Parameters.AddWithValue("@profileID", profileID)
        '        Else
        '            command.Parameters.AddWithValue("@profileID", System.DBNull.Value)
        '        End If


        '        Dim sdr As SqlDataReader = DataHelpers.GetDataReader(command)
        '        Try
        '            ds.REF_Bonus.Load(sdr)
        '        Catch ex As Exception
        '            Throw
        '        Finally
        '            command.Dispose()
        '            sdr.Close()
        '        End Try

        Return ds
    End Function


    Public Function GetReferrerCommissions_Admin() As DSReferrers
        Dim ds As New DSReferrers()

        Dim ta As New DSReferrersTableAdapters.GetReferrerCommissions_AdminTableAdapter
        ta.Connection = New SqlConnection(DataHelpers.ConnectionString)
        ta.Fill(ds.GetReferrerCommissions_Admin)

        Return ds
    End Function


    Public Function GetReferrersTransactions(ByVal DateFrom As Date?, ByVal DateTo As Date?, ByVal AffiliateID As Integer?) As DSReferrers
        Dim ds As New DSReferrers()

        Dim ta As New DSReferrersTableAdapters.GetReferrersTransactionsTableAdapter
        ta.Connection = New SqlConnection(DataHelpers.ConnectionString)
        ta.Fill(ds.GetReferrersTransactions, AffiliateID, DateFrom, DateTo, True, False)

        Return ds
    End Function


    Public Function GetReferrerCreditsHistory(ByVal DateFrom As Date?, ByVal DateTo As Date?, ByVal AffiliateID As Integer?, ByVal LevelParam As Integer?) As DSReferrers
        Dim ds As New DSReferrers()

        Dim ta As New DSReferrersTableAdapters.GetReferrerCreditsHistory2TableAdapter
        ta.Connection = New SqlConnection(DataHelpers.ConnectionString)
        ta.Fill(ds.GetReferrerCreditsHistory2, AffiliateID, DateFrom, DateTo, LevelParam)

        Return ds
    End Function



    Public Sub UpdateEUS_Profiles(ByRef ds As DSMembers)
        Dim ta As New DSMembersTableAdapters.EUS_ProfilesTableAdapter
        ta.Connection = New SqlConnection(DataHelpers.ConnectionString)
        ta.Update(ds.EUS_Profiles)
    End Sub


    Public Sub UpdateEUS_Profiles_AdminUpdater(ByRef ds As DSMembers)
        Dim ta As New DSMembersTableAdapters.EUS_Profiles_AdminUpdaterTableAdapter
        ta.Connection = New SqlConnection(DataHelpers.ConnectionString)
        ta.Update(ds.EUS_Profiles_AdminUpdater)
    End Sub

    Public Function UpdateEUS_Profiles_ModifiedFields(drNew As DSMembers.EUS_ProfilesRow, drOld As DSMembers.EUS_ProfilesRow) As Integer
        Dim rowsAffected As Integer = 0
        If (drNew.RowState = DataRowState.Unchanged) Then Return rowsAffected

        Dim query As New System.Text.StringBuilder()
        Dim cmd As SqlCommand = DataHelpers.GetSqlCommand()

        ' copy data that is not read only
        Dim cnt As Integer = 0
        For cnt = 0 To drNew.Table.Columns.Count - 1
            Dim dc As DataColumn = drNew.Table.Columns(cnt)
            If (dc.ReadOnly) Then
                'If (dc.ReadOnly OrElse dc.ColumnName = "IsMaster" OrElse dc.ColumnName = "MirrorProfileID") Then
                Continue For
            End If

            Dim modified As Boolean = drNew.IsNull(cnt) AndAlso Not drOld.IsNull(cnt)
            modified = modified OrElse Not drNew.IsNull(cnt) AndAlso drOld.IsNull(cnt)
            modified = modified OrElse Not drNew.IsNull(cnt) AndAlso Not drOld.IsNull(cnt) AndAlso (drNew(cnt) <> drOld(cnt))

            If (modified) Then
                query.AppendLine("[" & dc.ColumnName & "]=@" & dc.ColumnName & ",")
                If (drNew.IsNull(dc.ColumnName)) Then
                    cmd.Parameters.AddWithValue("@" & dc.ColumnName, System.DBNull.Value)
                Else
                    cmd.Parameters.AddWithValue("@" & dc.ColumnName, drNew(cnt))
                End If
            End If

        Next


        If (cmd.Parameters.Count > 0) Then

            If (query.Length > 1) Then
                query.Remove(query.Length - 3, 3)
                query.AppendLine("")
            End If

            query.Insert(0, "UPDATE EUS_Profiles SET")
            query.AppendLine("WHERE ProfileID=@ProfileID")
            cmd.Parameters.AddWithValue("@ProfileID", drNew("ProfileID"))

            cmd.CommandText = query.ToString()
            rowsAffected = DataHelpers.ExecuteNonQuery(cmd)
        End If

        Return rowsAffected
    End Function


    Public Function GetEUS_ProfileMasterByLoginName(ctx As CMSDBDataContext, loginNameOrEmail As String,
                                                    Optional status As ProfileStatusEnum = ProfileStatusEnum.None,
                                                    Optional status2 As ProfileStatusEnum = ProfileStatusEnum.None) As EUS_Profile
        Dim prof As EUS_Profile = Nothing
        'Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try

            For cnt = 0 To 2
                ' perform retry, in case when transaction was deadlocked

                Try
                    If (status > ProfileStatusEnum.None OrElse status2 > ProfileStatusEnum.None) Then
                        prof = (From itm In ctx.EUS_Profiles
                                Where (itm.LoginName.ToUpper() = loginNameOrEmail.ToUpper() OrElse itm.eMail.ToUpper() = loginNameOrEmail.ToUpper()) _
                                AndAlso itm.IsMaster = True _
                                AndAlso (itm.Status = status OrElse itm.Status = status2)
                                Select itm).SingleOrDefault()

                    ElseIf (status > ProfileStatusEnum.None) Then
                        prof = (From itm In ctx.EUS_Profiles
                                Where (itm.LoginName.ToUpper() = loginNameOrEmail.ToUpper() OrElse itm.eMail.ToUpper() = loginNameOrEmail.ToUpper()) _
                                AndAlso itm.IsMaster = True _
                                AndAlso itm.Status = status
                                Select itm).SingleOrDefault()

                    Else
                        prof = (From itm In ctx.EUS_Profiles
                                Where (itm.LoginName.ToUpper() = loginNameOrEmail.ToUpper() OrElse itm.eMail.ToUpper() = loginNameOrEmail.ToUpper()) _
                                AndAlso itm.IsMaster = True _
                                Select itm).SingleOrDefault()

                    End If

                    ' if qeury executed, exit for
                    Exit For

                Catch ex As System.Data.SqlClient.SqlException
                    If (ex.Message.Contains("Rerun the transaction.")) Then
                        ' if there was deadlock, retry
                    Else
                        Exit For
                    End If
                End Try

            Next

        Catch ex As System.InvalidOperationException
            If (ex.Message = "Sequence contains more than one element") Then
                Throw New System.Exception("There should be only one profile with specified login name, but found more. Please check goomena profiles!", ex)
            Else
                Throw New System.InvalidOperationException(ex.Message, ex)
            End If
        Catch ex As Exception
            Throw New System.Exception(ex.Message, ex)
        Finally
            '_CMSDBDataContext.Dispose()
        End Try

        Return prof

    End Function



    Public Function GetEUS_ProfileByLoginNameOrEmail(ctx As CMSDBDataContext, loginNameOrEmail As String) As EUS_Profile
        Dim prof As EUS_Profile = Nothing
        Try

            prof = (From itm In ctx.EUS_Profiles
                        Where (itm.LoginName.ToUpper() = loginNameOrEmail.ToUpper() OrElse itm.eMail.ToUpper() = loginNameOrEmail.ToUpper()) _
                        Select itm).FirstOrDefault()


        Catch ex As Exception
            Throw
        Finally
        End Try

        Return prof

    End Function

    Public Function GetEUS_ProfileByLoginNameOrEmail(loginNameOrEmail As String) As EUS_Profile
        Dim ctx As New CMSDBDataContext(DataHelpers.ConnectionString)
        Dim prof As EUS_Profile = Nothing
        Try

            prof = GetEUS_ProfileByLoginNameOrEmail(ctx, loginNameOrEmail)


        Catch ex As Exception
            Throw
        Finally
            ctx.Dispose()
        End Try
        Return prof
    End Function

    Public Function GetEUS_ProfileByProfileID(profileID As Integer) As EUS_Profile
        Dim ctx As New CMSDBDataContext(DataHelpers.ConnectionString)
        Dim prof As EUS_Profile = Nothing
        Try

            prof = GetEUS_ProfileByProfileID(ctx, profileID)


        Catch ex As Exception
            Throw
        Finally
            ctx.Dispose()
        End Try
        Return prof
    End Function

    Public Function EUS_Profile_CheckEmail(Email As String) As Boolean
        Dim ctx As New CMSDBDataContext(DataHelpers.ConnectionString)
        Dim Count As Integer
        Try

            Count = (From itm In ctx.EUS_Profiles
                                Where (itm.eMail.ToUpper() = Email.ToUpper() AndAlso itm.Status <> ProfileStatusEnum.DeletedByUser)
                                Select itm).Count()


        Catch ex As Exception
            Throw
        Finally
            ctx.Dispose()
        End Try

        Return Count > 0
    End Function

    Public Function EUS_Profile_CheckLoginName(LoginName As String) As Boolean
        Dim ctx As New CMSDBDataContext(DataHelpers.ConnectionString)
        Dim Count As Integer
        Try

            Count = (From itm In ctx.EUS_Profiles
                    Where (itm.LoginName.ToUpper() = LoginName.ToUpper())
                    Select itm).Count()


        Catch ex As Exception
            Throw
        Finally
            ctx.Dispose()
        End Try

        Return Count > 0
    End Function



    Public Function GetEUS_ProfileByProfileID(ctx As CMSDBDataContext, profileID As Integer) As EUS_Profile
        Dim prof As EUS_Profile = Nothing
        Try

            prof = (From itm In ctx.EUS_Profiles
                        Where (itm.ProfileID = profileID) _
                        Select itm).SingleOrDefault()


        Catch ex As Exception
            Throw
        Finally

        End Try

        Return prof
    End Function


    Public Function GetEUS_Profile_GenderId_ByProfileID(ctx As CMSDBDataContext, profileID As String) As Integer
        Dim LogName As Integer
        Try

            LogName = (From itm In ctx.EUS_Profiles
                        Where (itm.ProfileID = profileID) _
                        Select itm.GenderId).SingleOrDefault()

        Catch ex As Exception
            Throw
        End Try

        Return LogName
    End Function


    Public Function GetEUS_Profile_LoginName_ByProfileID(ctx As CMSDBDataContext, profileID As String) As String
        Dim LogName As String
        Try

            LogName = (From itm In ctx.EUS_Profiles
                        Where (itm.ProfileID = profileID) _
                        Select itm.LoginName).SingleOrDefault()

        Catch ex As Exception
            Throw
        End Try

        Return LogName
    End Function



    Public Function GetEUS_Profile_LoginName_ByProfileID(profileID As String) As String
        Dim ctx As New CMSDBDataContext(DataHelpers.ConnectionString)
        Dim LogName As String
        Try

            LogName = DataHelpers.GetEUS_Profile_LoginName_ByProfileID(ctx, profileID)

        Catch ex As Exception
            Throw
        Finally
            ctx.Dispose()
        End Try

        Return LogName
    End Function


    Public Function EUS_Profile_GetIsReferrer(ctx As CMSDBDataContext, profileID As String) As Boolean
        Dim prof As Integer = 0
        Try

            prof = (From itm In ctx.EUS_Profiles
                        Where (itm.ProfileID = profileID AndAlso itm.ReferrerParentId > 0) _
                        Select itm).Count()


        Catch ex As Exception
            Throw
        Finally

        End Try

        Return prof > 0
    End Function



    Public Function SYS_ProfilesAccess_GetByCookie(Cookie As String) As DSWebStatistics.SYS_ProfilesAccess_GetByCookieDataTable
        Dim ta As New DSWebStatisticsTableAdapters.SYS_ProfilesAccess_GetByCookieTableAdapter
        Dim ds As New DSWebStatistics()
        ta.Connection = New SqlConnection(DataHelpers.ConnectionString)
        ta.Fill(ds.SYS_ProfilesAccess_GetByCookie, Cookie)

        Return ds.SYS_ProfilesAccess_GetByCookie

        '        Dim dt As New DataTable()

        '        Try
        '            Dim sql As String = <sql><![CDATA[
        'select 
        '    distinct 
        '        -- datetimeaccess,
        '        [SYS_ProfilesAccess].ProfileID, 
        '        KeybLoginName, 
        '        Cookie, 
        '        IP,
        '        -- Title,
        '        Eus_Profiles.MirrorProfileID,
        '        Eus_Profiles.LoginName,
        '        Eus_Profiles.Status,
        '        Eus_Profiles.DateTimeToRegister,
        '        Eus_Profiles.LastLoginDateTime
        'from dbo.[SYS_ProfilesAccess] 
        'inner join Eus_Profiles on Eus_Profiles.ProfileID=SYS_ProfilesAccess.ProfileID
        'where Cookie=@LastCookie
        ']]></sql>

        '            Dim command As SqlClient.SqlCommand = GetSqlCommand(sql)
        '            command.Parameters.AddWithValue("@LastCookie", Cookie)
        '            DataHelpers.FillDataTable(command, dt)

        '        Catch ex As Exception
        '            Throw
        '        Finally
        '        End Try

        '        Return dt
    End Function

    'Public Function SYS_ProfilesAccess_GetByCookieDS(Cookie As String) As DSCustom
    '    Dim ta As New DSWebStatisticsTableAdapters.SYS_ProfilesAccess_GetByCookieTableAdapter
    '    Dim ds As New DSWebStatistics()
    '    ta.Connection = New SqlConnection(DataHelpers.ConnectionString)
    '    ta.Fill(ds.SYS_ProfilesAccess_GetByCookie, Cookie)

    '    Return ds
    'End Function


    Public Sub UpdateEUS_Profiles_LoginData(profileid As Integer, REMOTE_ADDR As String, GEO_COUNTRY_CODE As String, IsOnline As Boolean)

        Try
            Dim sql As String = <sql><![CDATA[
update EUS_Profiles set
    LastLoginDateTime=@LastLoginDateTime, 
    LastLoginIP=ISNULL(@LastLoginIP,LastLoginIP), 
    LastLoginGEOInfos=ISNULL(@LastLoginGEOInfos,LastLoginGEOInfos), 
    IsOnline=@IsOnline
where 
    ProfileID=@ProfileID or 
    MirrorProfileID=@ProfileID
]]></sql>

            Dim command As SqlClient.SqlCommand = GetSqlCommand(sql)
            command.Parameters.AddWithValue("@ProfileID", profileid)
            command.Parameters.AddWithValue("@LastLoginDateTime", DateTime.UtcNow)
            command.Parameters.AddWithValue("@LastLoginIP", REMOTE_ADDR)
            command.Parameters.AddWithValue("@IsOnline", IsOnline)

            If (String.IsNullOrEmpty(GEO_COUNTRY_CODE)) Then
                command.Parameters.AddWithValue("@LastLoginGEOInfos", DBNull.Value)
            Else
                command.Parameters.AddWithValue("@LastLoginGEOInfos", GEO_COUNTRY_CODE)
            End If

            DataHelpers.ExecuteNonQuery(command)

        Catch ex As Exception
            Throw
        Finally
        End Try
        'Dim dc As CMSDBDataContext = Nothing
        'Try
        '    dc = New CMSDBDataContext(DataHelpers.ConnectionString)
        '    Dim prof = From itm In dc.EUS_Profiles
        '            Where itm.ProfileID = profileid OrElse itm.MirrorProfileID = profileid
        '            Select itm

        '    Dim enumerator As IEnumerator = prof.GetEnumerator()
        '    While (enumerator.MoveNext())
        '        enumerator.Current.LastLoginDateTime = DateTime.UtcNow
        '        enumerator.Current.LastLoginIP = Web.HttpContext.Current.Request.Params("REMOTE_ADDR")
        '        enumerator.Current.LastLoginGEOInfos = Web.HttpContext.Current.Session("GEO_COUNTRY_CODE")
        '        enumerator.Current.IsOnline = True
        '    End While
        '    dc.SubmitChanges()


        '    'Dim sb As New System.Text.StringBuilder()
        '    'sb.AppendLine("update EUS_Profiles set IsOnline=0 where ProfileID=" & profileid & " or MirrorProfileID=" & profileid & "")
        '    'If (sb.Length > 0) Then
        '    '    DataHelpers.ExecuteNonQuery(sb.ToString())
        '    'End Ifa


        'Catch ex As Exception
        '    Throw
        'Finally
        '    If (dc IsNot Nothing) Then dc.Dispose()
        'End Try
    End Sub


    Public Sub UpdateEUS_Profiles_FacebookData(profileid As Integer, fbUid As String, fbName As String, fbUserName As String)
        Try
            Dim sql As String = "update EUS_Profiles set FacebookUserId=@FacebookUserId, FacebookName=@FacebookName, FacebookUserName=@FacebookUserName where ProfileID=" & profileid & " or MirrorProfileID=" & profileid & ""
            Dim command As SqlClient.SqlCommand = GetSqlCommand(sql)
            If (fbUid IsNot Nothing) Then
                command.Parameters.AddWithValue("@FacebookUserId", fbUid)
            Else
                command.Parameters.AddWithValue("@FacebookUserId", System.DBNull.Value)
            End If
            If (fbName IsNot Nothing) Then
                command.Parameters.AddWithValue("@FacebookName", fbName)
            Else
                command.Parameters.AddWithValue("@FacebookName", System.DBNull.Value)
            End If
            If (fbUserName IsNot Nothing) Then
                command.Parameters.AddWithValue("@FacebookUserName", fbUserName)
            Else
                command.Parameters.AddWithValue("@FacebookUserName", System.DBNull.Value)
            End If

            DataHelpers.ExecuteNonQuery(command)

        Catch ex As Exception
            Throw
        Finally

        End Try
    End Sub

    Public Sub UpdateEUS_Profiles_FacebookPhoto(profileid As Integer, FacebookPhoto As Boolean)
        Try
            Dim sb As String = "update EUS_Profiles set FacebookPhoto=@FacebookPhoto where ProfileID=" & profileid & " or MirrorProfileID=" & profileid & ""
            Dim command As SqlClient.SqlCommand = GetSqlCommand(sb.ToString())
            command.Parameters.AddWithValue("@FacebookPhoto", FacebookPhoto)

            DataHelpers.ExecuteNonQuery(command)

        Catch ex As Exception
            Throw
        Finally

        End Try
    End Sub



    Public Sub UpdateEUS_Profiles_LogoutData(profileid As Integer)
        Try
            Dim sql As String = "update EUS_Profiles set IsOnline=0 where ProfileID=" & profileid & " or MirrorProfileID=" & profileid & ""
            DataHelpers.ExecuteNonQuery(sql)
        Catch ex As Exception
            Throw
        Finally

        End Try
    End Sub


    Public Sub UpdateEUS_Profiles_AreYouWillingToTravel(profileid As Integer, willing As Boolean)
        Try
            Dim sql As String = ""
            If (willing) Then
                sql ="update EUS_Profiles set AreYouWillingToTravel=1 where ProfileID=" & profileid & ";" & vbCrLf & _
                    "update EUS_Profiles set AreYouWillingToTravel=1 where MirrorProfileID=" & profileid & ""
            Else
                sql = "update EUS_Profiles set AreYouWillingToTravel=0 where ProfileID=" & profileid & ";" & vbCrLf & _
                    "update EUS_Profiles set AreYouWillingToTravel=0 where MirrorProfileID=" & profileid & ""
            End If
            DataHelpers.ExecuteNonQuery(sql)
        Catch ex As Exception
            Throw
        Finally

        End Try
    End Sub


    Public Sub Update_EUS_Profiles_UpdateAvailableCredits(profileid As Integer, AvailableCredits As Integer, Optional ReadAvailableCreditsFromDB As Boolean = False)
        Try
            Dim sql As String = "EXEC EUS_Profiles_UpdateAvailableCredits @customerid=@customerid, @AvailableCredits=@AvailableCredits, @ReadAvailableCredits=@ReadAvailableCredits"
            Dim cmd As SqlCommand = DataHelpers.GetSqlCommand(sql)
            cmd.Parameters.AddWithValue("@customerid", profileid)
            cmd.Parameters.AddWithValue("@AvailableCredits", AvailableCredits)
            cmd.Parameters.AddWithValue("@ReadAvailableCredits", ReadAvailableCreditsFromDB)
            DataHelpers.ExecuteNonQuery(cmd)
        Catch ex As Exception
            Throw
        Finally

        End Try
    End Sub


    Public Function EUS_Profiles_CheckPassword(profileid As Integer, password As String) As Boolean
        Dim result As Long = 0
        Try
            Dim sql As String = "SELECT COUNT(*) as Rows FROM EUS_Profiles WHERE Password=@Password AND (ProfileID=@ProfileID OR MirrorProfileID=@ProfileID)"
            Dim cmd As SqlCommand = DataHelpers.GetSqlCommand(sql)
            cmd.Parameters.AddWithValue("@ProfileID", profileid)
            cmd.Parameters.AddWithValue("@Password", password)
            result = DataHelpers.ExecuteScalar(cmd)
        Catch ex As Exception
            Throw
        Finally

        End Try
        Return (result > 0)
    End Function


    Public Sub UpdateEUS_Profiles_AffiliateParentId(profileid As Integer, AffiliateParentId As Integer)
        Try
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("update EUS_Profiles set AffiliateParentId=" & AffiliateParentId & " where ProfileID=" & profileid & " or MirrorProfileID=" & profileid & "")
            If (sb.Length > 0) Then
                DataHelpers.ExecuteNonQuery(sb.ToString())
            End If

        Catch ex As Exception
            Throw
        Finally

        End Try
    End Sub


    Public Function GetEUS_Profiles_ReferrerParentId(profileid As Integer) As Integer
        Dim _ReferrerParentId As Integer?
        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try

            _ReferrerParentId = (From itm In _CMSDBDataContext.EUS_Profiles
                                    Where itm.ProfileID = profileid
                                    Select itm.ReferrerParentId).SingleOrDefault()


        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try
        If (Not _ReferrerParentId.HasValue) Then _ReferrerParentId = 0

        Return _ReferrerParentId
    End Function


    Public Sub UpdateEUS_Profiles_ReferrerParentId(profileid As Integer, ReferrerParentId As Integer)
        Try
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("update EUS_Profiles set ReferrerParentId=" & ReferrerParentId & " where ProfileID=" & profileid & " or MirrorProfileID=" & profileid & "")
            If (sb.Length > 0) Then
                DataHelpers.ExecuteNonQuery(sb.ToString())
            End If

        Catch ex As Exception
            Throw
        Finally

        End Try
    End Sub


    Public Sub UpdateEUS_Profiles_LAGID(profileid As Integer, lagId As String)
        Try
            ClsSQLInjectionClear.ClearString(lagId, 3)

            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("update EUS_Profiles set LAGID='" & lagId & "' where ProfileID=" & profileid & " or MirrorProfileID=" & profileid & "")
            If (sb.Length > 0) Then
                DataHelpers.ExecuteNonQuery(sb.ToString())
            End If

        Catch ex As Exception
            Throw
        Finally

        End Try
    End Sub


    Public Sub UpdateEUS_Profiles_REF_CRD2EURO_Rate(profileid As Integer, REF_CRD2EURO_Rate As Double)
        Try

            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("update EUS_Profiles set REF_CRD2EURO_Rate=@REF_CRD2EURO_Rate where ProfileID=" & profileid & " or MirrorProfileID=" & profileid & "")
            If (sb.Length > 0) Then
                Dim cmd As SqlCommand = DataHelpers.GetSqlCommand(sb.ToString())
                cmd.Parameters.Add(New SqlParameter("REF_CRD2EURO_Rate", REF_CRD2EURO_Rate))
                DataHelpers.ExecuteNonQuery(cmd)
            End If

        Catch ex As Exception
            Throw
        Finally

        End Try
    End Sub

    Public Sub UpdateEUS_Profiles_ShowOnFirstPage(profileid As Integer, show As Boolean)
        Try

            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("update EUS_Profiles set ShowOnFrontPage=@ShowOnFrontPage where ProfileID=" & profileid & " or MirrorProfileID=" & profileid & "")
            If (sb.Length > 0) Then
                Dim cmd As SqlCommand = DataHelpers.GetSqlCommand(sb.ToString())
                cmd.Parameters.Add(New SqlParameter("ShowOnFrontPage", show))
                DataHelpers.ExecuteNonQuery(cmd)
            End If

        Catch ex As Exception
            Throw
        Finally

        End Try
    End Sub


    Public Sub UpdateEUS_Profiles_Activity(profileid As Integer, LastActivityIP As String, isOnline As Boolean?, Optional TimeUntil As DateTime? = Nothing)
        Try
            ' Dim sb As New System.Text.StringBuilder()
            'update EUS_Profiles set LastActivityDateTime='" & Date.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff") & "' where ProfileID=" & profileid & " or MirrorProfileID=" & profileid & "")
            'Dim sql As String = "exec [EUS_Profiles_LastActivityDateTime] @CustomerId=@CustomerId"
            'Dim sql As String = "update EUS_Profiles set LastActivityDateTime=@LastActivityDateTime, IsOnline=isnull(@IsOnline,IsOnline) where ProfileID=@CustomerId or MirrorProfileID=@CustomerId"

            Dim sql As String = <sql><![CDATA[
update EUS_Profiles set 
    LastActivityDateTime=@LastActivityDateTime, 
    IsOnline=isnull(@IsOnline,IsOnline) , 
    LastActivityIP=isnull(@LastActivityIP,LastActivityIP) 
where ProfileID=@CustomerId or MirrorProfileID=@CustomerId
]]></sql>
            Dim cmd As SqlCommand = DataHelpers.GetSqlCommand(sql)
            cmd.Parameters.Add(New SqlParameter("@CustomerId", profileid))

            If (Not TimeUntil.HasValue) Then
                cmd.Parameters.Add(New SqlParameter("@LastActivityDateTime", Date.UtcNow))
            Else
                cmd.Parameters.Add(New SqlParameter("@LastActivityDateTime", TimeUntil))
            End If
            If (isOnline.HasValue) Then
                cmd.Parameters.Add(New SqlParameter("@IsOnline", isOnline))
            Else
                cmd.Parameters.Add(New SqlParameter("@IsOnline", System.DBNull.Value))
            End If
            If (Not String.IsNullOrEmpty(LastActivityIP)) Then
                cmd.Parameters.Add(New SqlParameter("@LastActivityIP", LastActivityIP))
            Else
                cmd.Parameters.Add(New SqlParameter("@LastActivityIP", System.DBNull.Value))
            End If
            DataHelpers.ExecuteNonQuery(cmd)

        Catch ex As Exception
            Throw
        Finally

        End Try
    End Sub


    Public Function GetEUS_Profiles_REF_CRD2EURO_Rate(profileid As Integer) As Double
        Dim _REF_CRD2EURO_Rate As Double?
        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try

            _REF_CRD2EURO_Rate = (From itm In _CMSDBDataContext.EUS_Profiles
                                    Where itm.ProfileID = profileid
                                    Select itm.REF_CRD2EURO_Rate).SingleOrDefault()


        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

        If (Not _REF_CRD2EURO_Rate.HasValue) Then
            Dim config As New clsConfigValues()
            _REF_CRD2EURO_Rate = config.referrer_commission_conv_rate
        End If

        Return _REF_CRD2EURO_Rate.Value
    End Function


    Public Function GetEUS_Profiles_Status(profileid As Integer) As Integer
        Dim Status As Integer

        Try
            Dim dt As New DataTable()
            Dim sb As String = "select top(1) Status from EUS_Profiles where ProfileID=" & profileid & " or MirrorProfileID=" & profileid & ""
            Dim o = DataHelpers.ExecuteScalar(sb.ToString())
            If (o IsNot Nothing AndAlso Not o Is System.DBNull.Value) Then Status = o
        Catch ex As Exception
            Throw
        Finally

        End Try

        Return Status
    End Function



    Public Sub UpdateEUS_Profiles_OnUpdateAutoNotificationSent(profileid As Integer, value As Boolean)
        Try
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("update EUS_Profiles set OnUpdateAutoNotificationSent='" & value & "' where ProfileID=" & profileid & " or MirrorProfileID=" & profileid & "")
            If (sb.Length > 0) Then
                DataHelpers.ExecuteNonQuery(sb.ToString())
            End If
        Catch ex As Exception
            Throw
        Finally
        End Try
    End Sub


    Public Function GetEUS_Profiles_OnUpdateAutoNotificationSent(profileid As Integer) As Boolean
        Dim value As Boolean
        Try
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("select top(1) ISNULL(OnUpdateAutoNotificationSent,0) as OnUpdateAutoNotificationSent from EUS_Profiles where ProfileID=" & profileid & " or MirrorProfileID=" & profileid & "")
            If (sb.Length > 0) Then
                value = DataHelpers.ExecuteScalar(sb.ToString())
            End If
        Catch ex As Exception
            Throw
        Finally
        End Try
        Return value
    End Function



    Public Sub UpdateEUS_Profiles_OnUpdateAutoNotificationSentForPhoto(profileid As Integer, value As Boolean)
        Try
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("update EUS_Profiles set OnUpdateAutoNotificationSentForPhoto='" & value & "' where ProfileID=" & profileid & " or MirrorProfileID=" & profileid & "")
            If (sb.Length > 0) Then
                DataHelpers.ExecuteNonQuery(sb.ToString())
            End If
        Catch ex As Exception
            Throw
        Finally
        End Try
    End Sub

    Public Function UpdateEUS_Profiles_MirrorSetStatusUpdating(profileid As Integer, Status As ProfileStatusEnum, ProfileModified As ProfileModifiedEnum) As Integer
        Dim ROWCOUNT As Integer
        Try
            Dim sql As String = <sql><![CDATA[

-- set status
update EUS_Profiles set 
    Status=@Status,
    LastUpdateProfileDateTime=@LastUpdateProfileDateTime
where 
    (ProfileID=@ProfileID or MirrorProfileID=@ProfileID)
    and IsMaster=0 
    and Status=@Approved



-- set UpdatingDocs or UpdatingPhotos

if(@@ROWCOUNT > 0 and @Status=2)
begin

    update EUS_Profiles set 
        UpdatingDocs=isnull(UpdatingDocs, @UpdatingDocs)
        ,UpdatingPhotos=isnull(UpdatingPhotos, @UpdatingPhotos)
    where 
        (ProfileID=@ProfileID or MirrorProfileID=@ProfileID)

end


-- set remove  IsAutoApproved flag

if(@Status=2 or @Status=16 or @Status=18)
begin

    update EUS_Profiles 
        set IsAutoApproved = null 
    where 
        (ProfileID=@ProfileID or MirrorProfileID=@ProfileID)

end


]]></sql>
            Dim cmd As SqlClient.SqlCommand = GetSqlCommand(sql)
            cmd.Parameters.AddWithValue("@Status", Status)
            cmd.Parameters.AddWithValue("@ProfileID", profileid)
            cmd.Parameters.AddWithValue("@Approved", CInt(ProfileStatusEnum.Approved))

            If (ProfileModified = ProfileModifiedEnum.UpdatingDocs) Then
                cmd.Parameters.AddWithValue("@UpdatingDocs", True)
            Else
                cmd.Parameters.AddWithValue("@UpdatingDocs", System.DBNull.Value)
            End If

            If (ProfileModified = ProfileModifiedEnum.UpdatingPhotos) Then
                cmd.Parameters.AddWithValue("@UpdatingPhotos", True)
            Else
                cmd.Parameters.AddWithValue("@UpdatingPhotos", System.DBNull.Value)
            End If
            cmd.Parameters.AddWithValue("@LastUpdateProfileDateTime", DateTime.UtcNow)

            ROWCOUNT = DataHelpers.ExecuteNonQuery(cmd)
        Catch ex As Exception
            Throw
        Finally
        End Try
        Return ROWCOUNT
    End Function

    Public Function GetEUS_Profiles_OnUpdateAutoNotificationSentForPhoto(profileid As Integer) As Boolean
        Dim value As Boolean
        Try
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("select top(1) ISNULL(OnUpdateAutoNotificationSentForPhoto,0) as OnUpdateAutoNotificationSentForPhoto from EUS_Profiles where ProfileID=" & profileid & " or MirrorProfileID=" & profileid & "")
            If (sb.Length > 0) Then
                value = DataHelpers.ExecuteScalar(sb.ToString())
            End If
        Catch ex As Exception
            Throw
        Finally
        End Try
        Return value
    End Function

#End Region

#Region "EUS_CustomerPhotos"

    Public Function GetEUS_CustomerPhotos() As DSMembers
        Dim ta As New DSMembersTableAdapters.EUS_CustomerPhotosTableAdapter
        Dim ds As New DSMembers()
        ta.Connection = New SqlConnection(DataHelpers.ConnectionString)
        ta.Fill(ds.EUS_CustomerPhotos)

        Return ds
    End Function


    Public Function GetEUS_CustomerPhotos_ByProfileOrMirrorID(customerId As Integer) As DSMembers
        Dim ta As New DSMembersTableAdapters.EUS_CustomerPhotosTableAdapter
        Dim ds As New DSMembers()
        ta.Connection = New SqlConnection(DataHelpers.ConnectionString)
        ta.FillByProfileIDOrMirrorID(ds.EUS_CustomerPhotos, customerId)
        Return ds
    End Function


    Public Function GetEUS_CustomerPhotosByCustomerPhotosID(photoId As Integer) As DSMembers
        Dim ta As New DSMembersTableAdapters.EUS_CustomerPhotosTableAdapter
        Dim ds As New DSMembers()
        ta.Connection = New SqlConnection(DataHelpers.ConnectionString)
        ta.FillByCustomerPhotosID(ds.EUS_CustomerPhotos, photoId)
        Return ds
    End Function


    Public Function GetEUS_CustomerVerificationDocsByID(CustomerVerificationDocsID As Integer) As DSCustomer
        Dim ta As New DSCustomerTableAdapters.EUS_CustomerVerificationDocsTableAdapter
        Dim ds As New DSCustomer()
        ta.Connection = New SqlConnection(DataHelpers.ConnectionString)
        ta.FillBy_CustomerVerificationDocsID(ds.EUS_CustomerVerificationDocs, CustomerVerificationDocsID)
        Return ds
    End Function


    Public Sub UpdateEUS_CustomerPhotos(ByRef ds As DSMembers)
        Dim ta As New DSMembersTableAdapters.EUS_CustomerPhotosTableAdapter
        ta.Connection = New SqlConnection(DataHelpers.ConnectionString)
        ta.Update(ds.EUS_CustomerPhotos)
    End Sub

    Public Sub UpdateEUS_CustomerVerificationDocs(ByRef ds As DSCustomer)
        Dim ta As New DSCustomerTableAdapters.EUS_CustomerVerificationDocsTableAdapter
        ta.Connection = New SqlConnection(DataHelpers.ConnectionString)
        ta.Update(ds.EUS_CustomerVerificationDocs)
    End Sub


    Public Function GetProfilesDefaultPhoto(profileId As Integer) As DSMembers.EUS_CustomerPhotosRow
        Dim ta As New DSMembersTableAdapters.EUS_CustomerPhotosTableAdapter
        Dim ds As New DSMembers()
        ta.Connection = New SqlConnection(DataHelpers.ConnectionString)
        ta.FillBy_CustomerID_IsDefault(ds.EUS_CustomerPhotos, profileId, True)

        If (ds.EUS_CustomerPhotos.Rows.Count > 0) Then
            Return ds.EUS_CustomerPhotos.Rows(0)
        End If

        Return Nothing
    End Function


    'Public Function GetProfilesDefaultPhotoLINQ(profileId As Integer) As EUS_CustomerPhoto
    '    Dim photo As EUS_CustomerPhoto = Nothing

    '    Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
    '    Try
    '        photo =
    '                (From phot In _CMSDBDataContext.EUS_CustomerPhotos
    '                Join pr In _CMSDBDataContext.EUS_Profiles On phot.CustomerID Equals pr.ProfileID
    '                Where (pr.ProfileID = profileId OrElse pr.MirrorProfileID = profileId) AndAlso _
    '                phot.IsDefault = True AndAlso phot.HasAproved = True AndAlso (phot.IsDeleted Is Nothing Or phot.IsDeleted = 0)
    '                Select phot).FirstOrDefault()


    '    Catch ex As Exception
    '        Throw
    '    Finally
    '        _CMSDBDataContext.Dispose()
    '    End Try

    '    Return photo
    'End Function

#End Region



    Public Function GetEUS_LISTS__OLD() As DSLists
        Dim ds As New DSLists()
        Dim ta As New DSListsTableAdapters.TableAdapterManager
        Dim connection As New SqlConnection(DataHelpers.ConnectionString)

        ta.EUS_LISTS_BodyTypeTableAdapter = New DSListsTableAdapters.EUS_LISTS_BodyTypeTableAdapter()
        ta.EUS_LISTS_BodyTypeTableAdapter.Connection = connection
        ta.EUS_LISTS_BodyTypeTableAdapter.Fill(ds.EUS_LISTS_BodyType)

        ta.EUS_LISTS_ChildrenNumberTableAdapter = New DSListsTableAdapters.EUS_LISTS_ChildrenNumberTableAdapter()
        ta.EUS_LISTS_ChildrenNumberTableAdapter.Connection = connection
        ta.EUS_LISTS_ChildrenNumberTableAdapter.Fill(ds.EUS_LISTS_ChildrenNumber)

        ta.EUS_LISTS_DrinkingTableAdapter = New DSListsTableAdapters.EUS_LISTS_DrinkingTableAdapter()
        ta.EUS_LISTS_DrinkingTableAdapter.Connection = connection
        ta.EUS_LISTS_DrinkingTableAdapter.Fill(ds.EUS_LISTS_Drinking)

        ta.EUS_LISTS_EducationTableAdapter = New DSListsTableAdapters.EUS_LISTS_EducationTableAdapter()
        ta.EUS_LISTS_EducationTableAdapter.Connection = connection
        ta.EUS_LISTS_EducationTableAdapter.Fill(ds.EUS_LISTS_Education)

        ta.EUS_LISTS_EthnicityTableAdapter = New DSListsTableAdapters.EUS_LISTS_EthnicityTableAdapter()
        ta.EUS_LISTS_EthnicityTableAdapter.Connection = connection
        ta.EUS_LISTS_EthnicityTableAdapter.Fill(ds.EUS_LISTS_Ethnicity)

        ta.EUS_LISTS_EyeColorTableAdapter = New DSListsTableAdapters.EUS_LISTS_EyeColorTableAdapter()
        ta.EUS_LISTS_EyeColorTableAdapter.Connection = connection
        ta.EUS_LISTS_EyeColorTableAdapter.Fill(ds.EUS_LISTS_EyeColor)

        ta.EUS_LISTS_HairColorTableAdapter = New DSListsTableAdapters.EUS_LISTS_HairColorTableAdapter()
        ta.EUS_LISTS_HairColorTableAdapter.Connection = connection
        ta.EUS_LISTS_HairColorTableAdapter.Fill(ds.EUS_LISTS_HairColor)

        ta.EUS_LISTS_HeightTableAdapter = New DSListsTableAdapters.EUS_LISTS_HeightTableAdapter()
        ta.EUS_LISTS_HeightTableAdapter.Connection = connection
        ta.EUS_LISTS_HeightTableAdapter.Fill(ds.EUS_LISTS_Height)

        ta.EUS_LISTS_IncomeTableAdapter = New DSListsTableAdapters.EUS_LISTS_IncomeTableAdapter()
        ta.EUS_LISTS_IncomeTableAdapter.Connection = connection
        ta.EUS_LISTS_IncomeTableAdapter.Fill(ds.EUS_LISTS_Income)

        ta.EUS_LISTS_NetWorthTableAdapter = New DSListsTableAdapters.EUS_LISTS_NetWorthTableAdapter()
        ta.EUS_LISTS_NetWorthTableAdapter.Connection = connection
        ta.EUS_LISTS_NetWorthTableAdapter.Fill(ds.EUS_LISTS_NetWorth)

        ta.EUS_LISTS_RelationshipStatusTableAdapter = New DSListsTableAdapters.EUS_LISTS_RelationshipStatusTableAdapter()
        ta.EUS_LISTS_RelationshipStatusTableAdapter.Connection = connection
        ta.EUS_LISTS_RelationshipStatusTableAdapter.Fill(ds.EUS_LISTS_RelationshipStatus)

        ta.EUS_LISTS_ReligionTableAdapter = New DSListsTableAdapters.EUS_LISTS_ReligionTableAdapter()
        ta.EUS_LISTS_ReligionTableAdapter.Connection = connection
        ta.EUS_LISTS_ReligionTableAdapter.Fill(ds.EUS_LISTS_Religion)

        ta.EUS_LISTS_SmokingTableAdapter = New DSListsTableAdapters.EUS_LISTS_SmokingTableAdapter()
        ta.EUS_LISTS_SmokingTableAdapter.Connection = connection
        ta.EUS_LISTS_SmokingTableAdapter.Fill(ds.EUS_LISTS_Smoking)

        ta.EUS_LISTS_RejectingReasonsTableAdapter = New DSListsTableAdapters.EUS_LISTS_RejectingReasonsTableAdapter()
        ta.EUS_LISTS_RejectingReasonsTableAdapter.Connection = connection
        ta.EUS_LISTS_RejectingReasonsTableAdapter.Fill(ds.EUS_LISTS_RejectingReasons)

        ta.SYS_CountriesGEOTableAdapter = New DSListsTableAdapters.SYS_CountriesGEOTableAdapter()
        ta.SYS_CountriesGEOTableAdapter.Connection = connection
        ta.SYS_CountriesGEOTableAdapter.Fill(ds.SYS_CountriesGEO)

        ta.EUS_LISTS_AccountTypeTableAdapter = New DSListsTableAdapters.EUS_LISTS_AccountTypeTableAdapter()
        ta.EUS_LISTS_AccountTypeTableAdapter.Connection = connection
        ta.EUS_LISTS_AccountTypeTableAdapter.Fill(ds.EUS_LISTS_AccountType)

        ta.EUS_LISTS_GenderTableAdapter = New DSListsTableAdapters.EUS_LISTS_GenderTableAdapter()
        ta.EUS_LISTS_GenderTableAdapter.Connection = connection
        ta.EUS_LISTS_GenderTableAdapter.Fill(ds.EUS_LISTS_Gender)

        ta.EUS_LISTS_PhotosDisplayLevelTableAdapter = New DSListsTableAdapters.EUS_LISTS_PhotosDisplayLevelTableAdapter()
        ta.EUS_LISTS_PhotosDisplayLevelTableAdapter.Connection = connection
        ta.EUS_LISTS_PhotosDisplayLevelTableAdapter.Fill(ds.EUS_LISTS_PhotosDisplayLevel)

        ta.EUS_LISTS_TypeOfDatingTableAdapter = New DSListsTableAdapters.EUS_LISTS_TypeOfDatingTableAdapter()
        ta.EUS_LISTS_TypeOfDatingTableAdapter.Connection = connection
        ta.EUS_LISTS_TypeOfDatingTableAdapter.Fill(ds.EUS_LISTS_TypeOfDating)

        ta.EUS_OffersStatusTableAdapter = New DSListsTableAdapters.EUS_OffersStatusTableAdapter()
        ta.EUS_OffersStatusTableAdapter.Connection = connection
        ta.EUS_OffersStatusTableAdapter.Fill(ds.EUS_OffersStatus)

        ta.EUS_OffersTypesTableAdapter = New DSListsTableAdapters.EUS_OffersTypesTableAdapter()
        ta.EUS_OffersTypesTableAdapter.Connection = connection
        ta.EUS_OffersTypesTableAdapter.Fill(ds.EUS_OffersTypes)

        Return ds
    End Function



    Public Function GetEUS_LISTS() As DSLists
        Dim sql As String = <sql><![CDATA[
select * from EUS_LISTS_BodyType; --0
select * from EUS_LISTS_ChildrenNumber; --1
select * from EUS_LISTS_Drinking; --2
select * from EUS_LISTS_Education; --3
select * from EUS_LISTS_Ethnicity; --4
select * from EUS_LISTS_EyeColor; --5 
select * from EUS_LISTS_HairColor; --6
select * from EUS_LISTS_Height; --7
select * from EUS_LISTS_Income; --8
select * from EUS_LISTS_NetWorth; --9
select * from EUS_LISTS_RelationshipStatus; --10
select * from EUS_LISTS_Religion; --11
select * from EUS_LISTS_Smoking; --12
select * from EUS_LISTS_RejectingReasons; --13
select * from SYS_CountriesGEO; --14
select * from EUS_LISTS_AccountType; --15
select * from EUS_LISTS_Gender; --16
select * from EUS_LISTS_PhotosDisplayLevel; --17
select * from EUS_LISTS_TypeOfDating; --18
select * from EUS_OffersStatus; --19
select * from EUS_OffersTypes; --20
select * from EUS_LISTS_Job; --21
select * from EUS_LISTS_BreastSize; --22
select * from EUS_LISTS_SpokenLanguages; --23
]]></sql>.Value


        Dim ds As New DSLists()
        Dim ta As New DSListsTableAdapters.TableAdapterManager
        Dim connection As New SqlConnection(DataHelpers.ConnectionString)

        Dim dsFill As DataSet = DataHelpers.GetDataSet(sql)
        ds.EUS_LISTS_BodyType.Merge(dsFill.Tables(0))
        ds.EUS_LISTS_ChildrenNumber.Merge(dsFill.Tables(1))
        ds.EUS_LISTS_Drinking.Merge(dsFill.Tables(2))
        ds.EUS_LISTS_Education.Merge(dsFill.Tables(3))
        ds.EUS_LISTS_Ethnicity.Merge(dsFill.Tables(4))
        ds.EUS_LISTS_EyeColor.Merge(dsFill.Tables(5))
        ds.EUS_LISTS_HairColor.Merge(dsFill.Tables(6))
        ds.EUS_LISTS_Height.Merge(dsFill.Tables(7))
        ds.EUS_LISTS_Income.Merge(dsFill.Tables(8))
        ds.EUS_LISTS_NetWorth.Merge(dsFill.Tables(9))
        ds.EUS_LISTS_RelationshipStatus.Merge(dsFill.Tables(10))
        ds.EUS_LISTS_Religion.Merge(dsFill.Tables(11))
        ds.EUS_LISTS_Smoking.Merge(dsFill.Tables(12))
        ds.EUS_LISTS_RejectingReasons.Merge(dsFill.Tables(13))
        ds.SYS_CountriesGEO.Merge(dsFill.Tables(14))
        ds.EUS_LISTS_AccountType.Merge(dsFill.Tables(15))
        ds.EUS_LISTS_Gender.Merge(dsFill.Tables(16))
        ds.EUS_LISTS_PhotosDisplayLevel.Merge(dsFill.Tables(17))
        ds.EUS_LISTS_TypeOfDating.Merge(dsFill.Tables(18))
        ds.EUS_OffersStatus.Merge(dsFill.Tables(19))
        ds.EUS_OffersTypes.Merge(dsFill.Tables(20))
        ds.EUS_LISTS_Job.Merge(dsFill.Tables(21))
        ds.EUS_LISTS_BreastSize.Merge(dsFill.Tables(22))
        ds.EUS_LISTS_SpokenLanguages.Merge(dsFill.Tables(23))

        'If (dsFill.Tables.Count > 0) Then
        '    dsFill.Tables(0).TableName = "EUS_LISTS_BodyTypeTable"
        '    dsFill.Tables(1).TableName = "EUS_LISTS_ChildrenNumber"
        '    dsFill.Tables(2).TableName = "EUS_LISTS_DrinkingTable"
        '    dsFill.Tables(3).TableName = "EUS_LISTS_Education"
        '    dsFill.Tables(4).TableName = "EUS_LISTS_Ethnicity"
        '    dsFill.Tables(5).TableName = "EUS_LISTS_EyeColor"
        '    dsFill.Tables(6).TableName = "EUS_LISTS_HairColor"
        '    dsFill.Tables(7).TableName = "EUS_LISTS_Height"
        '    dsFill.Tables(8).TableName = "EUS_LISTS_Income"
        '    dsFill.Tables(9).TableName = "EUS_LISTS_NetWorth"
        '    dsFill.Tables(10).TableName = "EUS_LISTS_RelationshipStatus"
        '    dsFill.Tables(11).TableName = "EUS_LISTS_Religion"
        '    dsFill.Tables(12).TableName = "EUS_LISTS_Smoking"
        '    dsFill.Tables(13).TableName = "EUS_LISTS_RejectingReasons"
        '    dsFill.Tables(14).TableName = "SYS_CountriesGEO"
        '    dsFill.Tables(15).TableName = "EUS_LISTS_AccountType"
        '    dsFill.Tables(16).TableName = "EUS_LISTS_Gender"
        '    dsFill.Tables(17).TableName = "EUS_LISTS_PhotosDisplayLevel"
        '    dsFill.Tables(18).TableName = "EUS_LISTS_TypeOfDating"
        '    dsFill.Tables(19).TableName = "EUS_OffersStatus"
        '    dsFill.Tables(20).TableName = "EUS_OffersTypes"
        'End If



        ''ta.EUS_LISTS_BodyTypeTableAdapter = New DSListsTableAdapters.EUS_LISTS_BodyTypeTableAdapter()
        ''ta.EUS_LISTS_BodyTypeTableAdapter.Connection = connection
        ''ta.EUS_LISTS_BodyTypeTableAdapter.Fill(ds.EUS_LISTS_BodyType)

        'ds.EUS_LISTS_ChildrenNumber.Merge(dsFill.Tables(1))
        ''ta.EUS_LISTS_ChildrenNumberTableAdapter = New DSListsTableAdapters.EUS_LISTS_ChildrenNumberTableAdapter()
        ''ta.EUS_LISTS_ChildrenNumberTableAdapter.Connection = connection
        ''ta.EUS_LISTS_ChildrenNumberTableAdapter.Fill(ds.EUS_LISTS_ChildrenNumber)

        'ds.EUS_LISTS_Drinking.Merge(dsFill.Tables(2))
        ''ta.EUS_LISTS_DrinkingTableAdapter = New DSListsTableAdapters.EUS_LISTS_DrinkingTableAdapter()
        ''ta.EUS_LISTS_DrinkingTableAdapter.Connection = connection
        ''ta.EUS_LISTS_DrinkingTableAdapter.Fill(ds.EUS_LISTS_Drinking)

        'ds.EUS_LISTS_Education.Merge(dsFill.Tables(3))
        ''ta.EUS_LISTS_EducationTableAdapter = New DSListsTableAdapters.EUS_LISTS_EducationTableAdapter()
        ''ta.EUS_LISTS_EducationTableAdapter.Connection = connection
        ''ta.EUS_LISTS_EducationTableAdapter.Fill(ds.EUS_LISTS_Education)

        'ds.EUS_LISTS_Ethnicity.Merge(dsFill.Tables(4))
        ''ta.EUS_LISTS_EthnicityTableAdapter = New DSListsTableAdapters.EUS_LISTS_EthnicityTableAdapter()
        ''ta.EUS_LISTS_EthnicityTableAdapter.Connection = connection
        ''ta.EUS_LISTS_EthnicityTableAdapter.Fill(ds.EUS_LISTS_Ethnicity)

        'ds.EUS_LISTS_EyeColor.Merge(dsFill.Tables(5))
        ''ta.EUS_LISTS_EyeColorTableAdapter = New DSListsTableAdapters.EUS_LISTS_EyeColorTableAdapter()
        ''ta.EUS_LISTS_EyeColorTableAdapter.Connection = connection
        ''ta.EUS_LISTS_EyeColorTableAdapter.Fill(ds.EUS_LISTS_EyeColor)

        'ds.EUS_LISTS_HairColor.Merge(dsFill.Tables(6))
        ''ta.EUS_LISTS_HairColorTableAdapter = New DSListsTableAdapters.EUS_LISTS_HairColorTableAdapter()
        ''ta.EUS_LISTS_HairColorTableAdapter.Connection = connection
        ''ta.EUS_LISTS_HairColorTableAdapter.Fill(ds.EUS_LISTS_HairColor)

        'ds.EUS_LISTS_Height.Merge(dsFill.Tables(7))
        ''ta.EUS_LISTS_HeightTableAdapter = New DSListsTableAdapters.EUS_LISTS_HeightTableAdapter()
        ''ta.EUS_LISTS_HeightTableAdapter.Connection = connection
        ''ta.EUS_LISTS_HeightTableAdapter.Fill(ds.EUS_LISTS_Height)

        'ds.EUS_LISTS_Income.Merge(dsFill.Tables(8))
        ''ta.EUS_LISTS_IncomeTableAdapter = New DSListsTableAdapters.EUS_LISTS_IncomeTableAdapter()
        ''ta.EUS_LISTS_IncomeTableAdapter.Connection = connection
        ''ta.EUS_LISTS_IncomeTableAdapter.Fill(ds.EUS_LISTS_Income)

        'ds.EUS_LISTS_NetWorth.Merge(dsFill.Tables(9))
        ''ta.EUS_LISTS_NetWorthTableAdapter = New DSListsTableAdapters.EUS_LISTS_NetWorthTableAdapter()
        ''ta.EUS_LISTS_NetWorthTableAdapter.Connection = connection
        ''ta.EUS_LISTS_NetWorthTableAdapter.Fill(ds.EUS_LISTS_NetWorth)

        'ds.EUS_LISTS_RelationshipStatus.Merge(dsFill.Tables(10))
        ''ta.EUS_LISTS_RelationshipStatusTableAdapter = New DSListsTableAdapters.EUS_LISTS_RelationshipStatusTableAdapter()
        ''ta.EUS_LISTS_RelationshipStatusTableAdapter.Connection = connection
        ''ta.EUS_LISTS_RelationshipStatusTableAdapter.Fill(ds.EUS_LISTS_RelationshipStatus)

        'ds.EUS_LISTS_Religion.Merge(dsFill.Tables(11))
        ''ta.EUS_LISTS_ReligionTableAdapter = New DSListsTableAdapters.EUS_LISTS_ReligionTableAdapter()
        ''ta.EUS_LISTS_ReligionTableAdapter.Connection = connection
        ''ta.EUS_LISTS_ReligionTableAdapter.Fill(ds.EUS_LISTS_Religion)

        'ds.EUS_LISTS_Smoking.Merge(dsFill.Tables(12))
        ''ta.EUS_LISTS_SmokingTableAdapter = New DSListsTableAdapters.EUS_LISTS_SmokingTableAdapter()
        ''ta.EUS_LISTS_SmokingTableAdapter.Connection = connection
        ''ta.EUS_LISTS_SmokingTableAdapter.Fill(ds.EUS_LISTS_Smoking)

        'ds.EUS_LISTS_RejectingReasons.Merge(dsFill.Tables(13))
        ''ta.EUS_LISTS_RejectingReasonsTableAdapter = New DSListsTableAdapters.EUS_LISTS_RejectingReasonsTableAdapter()
        ''ta.EUS_LISTS_RejectingReasonsTableAdapter.Connection = connection
        ''ta.EUS_LISTS_RejectingReasonsTableAdapter.Fill(ds.EUS_LISTS_RejectingReasons)

        'ds.SYS_CountriesGEO.Merge(dsFill.Tables(14))
        ''ta.SYS_CountriesGEOTableAdapter = New DSListsTableAdapters.SYS_CountriesGEOTableAdapter()
        ''ta.SYS_CountriesGEOTableAdapter.Connection = connection
        ''ta.SYS_CountriesGEOTableAdapter.Fill(ds.SYS_CountriesGEO)

        'ds.EUS_LISTS_AccountType.Merge(dsFill.Tables(15))
        ''ta.EUS_LISTS_AccountTypeTableAdapter = New DSListsTableAdapters.EUS_LISTS_AccountTypeTableAdapter()
        ''ta.EUS_LISTS_AccountTypeTableAdapter.Connection = connection
        ''ta.EUS_LISTS_AccountTypeTableAdapter.Fill(ds.EUS_LISTS_AccountType)

        'ds.EUS_LISTS_Gender.Merge(dsFill.Tables(16))
        ''ta.EUS_LISTS_GenderTableAdapter = New DSListsTableAdapters.EUS_LISTS_GenderTableAdapter()
        ''ta.EUS_LISTS_GenderTableAdapter.Connection = connection
        ''ta.EUS_LISTS_GenderTableAdapter.Fill(ds.EUS_LISTS_Gender)

        'ds.EUS_LISTS_PhotosDisplayLevel.Merge(dsFill.Tables(17))
        ''ta.EUS_LISTS_PhotosDisplayLevelTableAdapter = New DSListsTableAdapters.EUS_LISTS_PhotosDisplayLevelTableAdapter()
        ''ta.EUS_LISTS_PhotosDisplayLevelTableAdapter.Connection = connection
        ''ta.EUS_LISTS_PhotosDisplayLevelTableAdapter.Fill(ds.EUS_LISTS_PhotosDisplayLevel)

        'ds.EUS_LISTS_TypeOfDating.Merge(dsFill.Tables(18))
        ''ta.EUS_LISTS_TypeOfDatingTableAdapter = New DSListsTableAdapters.EUS_LISTS_TypeOfDatingTableAdapter()
        ''ta.EUS_LISTS_TypeOfDatingTableAdapter.Connection = connection
        ''ta.EUS_LISTS_TypeOfDatingTableAdapter.Fill(ds.EUS_LISTS_TypeOfDating)

        'ds.EUS_OffersStatus.Merge(dsFill.Tables(19))
        ''ta.EUS_OffersStatusTableAdapter = New DSListsTableAdapters.EUS_OffersStatusTableAdapter()
        ''ta.EUS_OffersStatusTableAdapter.Connection = connection
        ''ta.EUS_OffersStatusTableAdapter.Fill(ds.EUS_OffersStatus)

        'ds.EUS_OffersTypes.Merge(dsFill.Tables(20))
        ''ta.EUS_OffersTypesTableAdapter = New DSListsTableAdapters.EUS_OffersTypesTableAdapter()
        ''ta.EUS_OffersTypesTableAdapter.Connection = connection
        ''ta.EUS_OffersTypesTableAdapter.Fill(ds.EUS_OffersTypes)

        Return ds
    End Function



    Public Function GetEUS_LISTS_PhotosDisplayLevel() As DSLists
        Dim ds As New DSLists()
        Dim ta As New DSListsTableAdapters.TableAdapterManager
        Dim connection As New SqlConnection(DataHelpers.ConnectionString)

        ta.EUS_LISTS_PhotosDisplayLevelTableAdapter = New DSListsTableAdapters.EUS_LISTS_PhotosDisplayLevelTableAdapter()
        ta.EUS_LISTS_PhotosDisplayLevelTableAdapter.Connection = connection
        ta.EUS_LISTS_PhotosDisplayLevelTableAdapter.Fill(ds.EUS_LISTS_PhotosDisplayLevel)

        Return ds
    End Function


    Public Function GetEUS_LISTS_ReportingReason() As DSLists
        Dim ds As New DSLists()
        Dim ta As New DSListsTableAdapters.EUS_LISTS_ReportingReasonTableAdapter

        ta.Connection = New SqlConnection(DataHelpers.ConnectionString)
        ta.Fill(ds.EUS_LISTS_ReportingReason)
        Return ds
    End Function


    'Public  Function GetEUS_LISTS_Gender() As DSLists
    '    Dim ta As New DSListsTableAdapters.EUS_LISTS_GenderTableAdapter
    '    Dim ds As New DSLists()
    '    ta.Connection = New SqlConnection(DataHelpers.ConnectionString)
    '    ta.Fill(ds.EUS_LISTS_Gender)

    '    Return ds
    'End Function


    'Public  Function GetEUS_LISTS_AccountType() As DSLists
    '    Dim ta As New DSListsTableAdapters.EUS_LISTS_AccountTypeTableAdapter
    '    Dim ds As New DSLists()
    '    ta.Connection = New SqlConnection(DataHelpers.ConnectionString)
    '    ta.Fill(ds.EUS_LISTS_AccountType)

    '    Return ds
    'End Function




    Public Function GetApprovedUsersDatatable(topRecords As Integer) As DataTable
        Dim dataTbl As DataTable = Nothing

        Dim sql As String = <sql><![CDATA[
SELECT TOP({0}) 
    [ProfileID], 
    [LoginName], 
    [Genderid],
    (Select top(1) [FileName] from EUS_CustomerPhotos where [CustomerID]=[ProfileID] and IsDefault=1 and HasAproved=1 and ISNULL(phot.IsDeleted,0) = 0) as ImageFileName
FROM [dbo].[EUS_Profiles]
WHERE [Status]= {1} and [IsMaster]=1 and 
Genderid = {2}
UNION
SELECT TOP({0}) 
    [ProfileID], 
    [LoginName], 
    [Genderid],
    (Select top(1) [FileName] from EUS_CustomerPhotos where [CustomerID]=[ProfileID] and IsDefault=1 and HasAproved=1 and ISNULL(phot.IsDeleted,0) = 0) as ImageFileName
FROM [dbo].[EUS_Profiles]
WHERE [Status]={1} and [IsMaster]=1 and  
Genderid = {3}
]]></sql>.Value


        sql = String.Format(sql, topRecords.ToString(), CType(ProfileStatusEnum.Approved, Integer).ToString(), ProfileHelper.gMaleGender.GenderId.ToString(), ProfileHelper.gFemaleGender.GenderId.ToString())

        Dim command As SqlCommand = GetSqlCommand(sql)
        Dim adapter As New SqlDataAdapter(command)
        dataTbl = New DataTable()
        Try
            adapter.Fill(dataTbl)
        Catch ex As Exception
            Throw
        Finally
            command.Connection.Close()
            command.Dispose()
        End Try

        Return dataTbl
    End Function


    Public Function GetSqlCommand(Optional ByVal sqlStatement As String = "") As SqlClient.SqlCommand
        If (sqlStatement Is Nothing) Then sqlStatement = ""

        Dim connection As SqlClient.SqlConnection = New SqlClient.SqlConnection(DataHelpers.ConnectionString)
        Dim command As SqlClient.SqlCommand = New SqlClient.SqlCommand(sqlStatement, connection)

        Return command
    End Function



    Public Function GetDataTable(sqlStatement As String) As DataTable


        Dim connection As SqlClient.SqlConnection = New SqlClient.SqlConnection(DataHelpers.ConnectionString)
        Dim command As SqlClient.SqlCommand = New SqlClient.SqlCommand(sqlStatement, connection)

        Dim dt As New DataTable()
        Dim adapter As New SqlDataAdapter(command)

        Try
            'adapter.Fill(dt)
            Dim rerunTran As Boolean = False
            Try
                adapter.Fill(dt)
            Catch ex As System.Data.SqlClient.SqlException
                If (ex.Message.Contains("Rerun the transaction.")) Then
                    rerunTran = True
                Else
                    Throw
                End If
            End Try

            If (rerunTran) Then
                rerunTran = False
                Try
                    adapter.Fill(dt)
                Catch ex As System.Data.SqlClient.SqlException
                    If (ex.Message.Contains("Rerun the transaction.")) Then
                        rerunTran = True
                    Else
                        Throw
                    End If
                End Try
            End If

        Catch ex As Exception
            Throw
        Finally
            command.Connection.Close()
            command.Dispose()
            adapter.Dispose()
        End Try

        Return dt
    End Function

    Public Sub EUS_Profiles_LoadDataTable(ByRef ds As DSMembers, sql As String)

        Dim cmd As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql)
        Dim rsRead As SqlClient.SqlDataReader = Nothing
        Try
            rsRead = DataHelpers.GetDataReader(cmd)
            ds.EUS_Profiles.Load(rsRead, LoadOption.OverwriteChanges)
        Catch ex As Exception
            Throw
        Finally
            If (rsRead IsNot Nothing) Then rsRead.Close()
            If (cmd IsNot Nothing) Then
                cmd.Connection.Close()
                cmd.Dispose()
            End If
        End Try

    End Sub


    'Public Function GetDataTable(sqlStatement As String, ByRef sqlcon As SqlConnection) As DataTable


    '    'Dim connection As SqlClient.SqlConnection = New SqlClient.SqlConnection(DataHelpers.ConnectionString)
    '    Dim command As SqlClient.SqlCommand = New SqlClient.SqlCommand(sqlStatement, sqlcon)

    '    Dim dt As New DataTable()
    '    Dim adapter As New SqlDataAdapter(command)

    '    Try
    '        adapter.Fill(dt)
    '    Catch ex As Exception
    '        Throw
    '    Finally
    '        'command.Connection.Close()
    '        'command.Dispose()
    '        adapter.Dispose()
    '    End Try

    '    Return dt
    'End Function



    'Public Function GetDataReader(sqlStatement As String) As SqlDataReader


    '    Dim connection As SqlClient.SqlConnection = New SqlClient.SqlConnection(DataHelpers.ConnectionString)
    '    Dim command As SqlClient.SqlCommand = New SqlClient.SqlCommand(sqlStatement, connection)

    '    Dim sdr As SqlDataReader
    '    Try
    '        command.Connection.Open()
    '        sdr = command.ExecuteReader()
    '    Catch ex As Exception
    '        Throw
    '    End Try

    '    Return sdr
    'End Function



    Public Function GetDataReader(command As SqlClient.SqlCommand) As SqlDataReader

        Dim sdr As SqlDataReader
        Try
            command.Connection.Open()
            sdr = command.ExecuteReader()
        Catch ex As Exception
            Throw
        End Try

        Return sdr
    End Function

    Public Function GetDataSet(command As SqlClient.SqlCommand) As DataSet

        Dim ds As New DataSet()
        Dim adapter As New SqlDataAdapter(command)

        Try
            Dim rerunTran As Boolean = False
            Try
                adapter.Fill(ds)
            Catch ex As System.Data.SqlClient.SqlException
                If (ex.Message.Contains("Rerun the transaction.")) Then
                    rerunTran = True
                Else
                    Throw
                End If
            End Try

            If (rerunTran) Then
                rerunTran = False
                Try
                    adapter.Fill(ds)
                Catch ex As System.Data.SqlClient.SqlException
                    If (ex.Message.Contains("Rerun the transaction.")) Then
                        rerunTran = True
                    Else
                        Throw
                    End If
                End Try
            End If

        Catch ex As Exception
            Throw
        Finally
            command.Connection.Close()
            command.Dispose()
            adapter.Dispose()
        End Try

        Return ds
    End Function


    Public Function GetDataSet(sqlStatement As String) As DataSet

        Dim connection As SqlClient.SqlConnection = New SqlClient.SqlConnection(DataHelpers.ConnectionString)
        Dim command As SqlClient.SqlCommand = New SqlClient.SqlCommand(sqlStatement, connection)

        Dim ds As New DataSet()
        Dim adapter As New SqlDataAdapter(command)

        Try
            'adapter.Fill(ds)
            Dim rerunTran As Boolean = False
            Try
                adapter.Fill(ds)
            Catch ex As System.Data.SqlClient.SqlException
                If (ex.Message.Contains("Rerun the transaction.")) Then
                    rerunTran = True
                Else
                    Throw
                End If
            End Try

            If (rerunTran) Then
                rerunTran = False
                Try
                    adapter.Fill(ds)
                Catch ex As System.Data.SqlClient.SqlException
                    If (ex.Message.Contains("Rerun the transaction.")) Then
                        rerunTran = True
                    Else
                        Throw
                    End If
                End Try
            End If

        Catch ex As Exception
            Throw
        Finally
            command.Connection.Close()
            command.Dispose()
            adapter.Dispose()
        End Try

        Return ds
    End Function

    Public Function GetDataTable(command As SqlClient.SqlCommand) As DataTable

        Dim dt As New DataTable()
        Dim adapter As New SqlDataAdapter(command)

        Try
            'adapter.Fill(dt)
            Dim rerunTran As Boolean = False
            Try
                adapter.Fill(dt)
            Catch ex As System.Data.SqlClient.SqlException
                If (ex.Message.Contains("Rerun the transaction.")) Then
                    rerunTran = True
                Else
                    Throw
                End If
            End Try

            If (rerunTran) Then
                rerunTran = False
                Try
                    adapter.Fill(dt)
                Catch ex As System.Data.SqlClient.SqlException
                    If (ex.Message.Contains("Rerun the transaction.")) Then
                        rerunTran = True
                    Else
                        Throw
                    End If
                End Try
            End If

        Catch ex As Exception
            Throw
        Finally
            command.Connection.Close()
            command.Dispose()
            adapter.Dispose()
        End Try

        Return dt
    End Function



    Public Sub FillDataTable(sqlStatement As String, ByRef table As DataTable)

        Dim connection As SqlClient.SqlConnection = New SqlClient.SqlConnection(DataHelpers.ConnectionString)
        Dim command As SqlClient.SqlCommand = New SqlClient.SqlCommand(sqlStatement, connection)

        Dim adapter As New SqlDataAdapter(command)
        Try
            'adapter.Fill(table)
            Dim rerunTran As Boolean = False
            Try
                adapter.Fill(table)
            Catch ex As System.Data.SqlClient.SqlException
                If (ex.Message.Contains("Rerun the transaction.")) Then
                    rerunTran = True
                Else
                    Throw
                End If
            End Try

            If (rerunTran) Then
                rerunTran = False
                Try
                    adapter.Fill(table)
                Catch ex As System.Data.SqlClient.SqlException
                    If (ex.Message.Contains("Rerun the transaction.")) Then
                        rerunTran = True
                    Else
                        Throw
                    End If
                End Try
            End If

        Catch ex As Exception
            Throw
        Finally
            command.Connection.Close()
            command.Dispose()
        End Try

    End Sub



    Public Sub FillDataTable(command As SqlClient.SqlCommand, ByRef table As DataTable)

        Dim adapter As New SqlDataAdapter(command)
        Try
            'adapter.Fill(table)
            Dim rerunTran As Boolean = False
            Try
                adapter.Fill(table)
            Catch ex As System.Data.SqlClient.SqlException
                If (ex.Message.Contains("Rerun the transaction.")) Then
                    rerunTran = True
                Else
                    Throw
                End If
            End Try

            If (rerunTran) Then
                rerunTran = False
                Try
                    adapter.Fill(table)
                Catch ex As System.Data.SqlClient.SqlException
                    If (ex.Message.Contains("Rerun the transaction.")) Then
                        rerunTran = True
                    Else
                        Throw
                    End If
                End Try
            End If

        Catch ex As Exception
            Throw
        Finally
            command.Connection.Close()
            command.Dispose()
        End Try

    End Sub



    Public Function ExecuteNonQuery(command As SqlClient.SqlCommand) As Integer
        Dim result As Integer = -1
        Dim connection As SqlClient.SqlConnection = New SqlClient.SqlConnection(DataHelpers.ConnectionString)
        command.Connection = connection

        Try
            command.Connection.Open()
            result = command.ExecuteNonQuery()
        Catch ex As Exception
            Throw
        Finally
            command.Connection.Close()
            command.Dispose()
        End Try

        Return result
    End Function



    Public Function ExecuteNonQuery(sqlStatement As String) As Integer

        Dim connection As SqlClient.SqlConnection = New SqlClient.SqlConnection(DataHelpers.ConnectionString)
        Dim command As SqlClient.SqlCommand = New SqlClient.SqlCommand(sqlStatement, connection)
        Dim result As Integer = -1

        Try
            command.Connection.Open()
            result = command.ExecuteNonQuery()
        Catch ex As Exception
            Throw
        Finally
            command.Connection.Close()
            command.Dispose()
        End Try

        Return result
    End Function



    Public Function ExecuteScalar(command As SqlClient.SqlCommand) As Object
        Dim result As Long = -1
        Dim connection As SqlClient.SqlConnection = New SqlClient.SqlConnection(DataHelpers.ConnectionString)
        command.Connection = connection

        Try
            command.Connection.Open()
            result = command.ExecuteScalar()
        Catch ex As Exception
            Throw
        Finally
            command.Connection.Close()
            command.Dispose()
        End Try

        Return result
    End Function

    Public Function ExecuteScalar(sqlStatement As String) As Object

        Dim connection As SqlClient.SqlConnection = New SqlClient.SqlConnection(DataHelpers.ConnectionString)
        Dim command As SqlClient.SqlCommand = New SqlClient.SqlCommand(sqlStatement, connection)
        Dim result As Integer = -1

        Try
            command.Connection.Open()
            result = command.ExecuteScalar()
        Catch ex As Exception
            Throw
        Finally
            command.Connection.Close()
            command.Dispose()
        End Try

        Return result
    End Function



    Public Function ToDataTable(ctx As System.Data.Linq.DataContext, query As Object) As DataTable
        If query Is Nothing Then
            Throw New ArgumentNullException("query")
        End If

        Dim cmd As IDbCommand = ctx.GetCommand(TryCast(query, IQueryable))
        Dim adapter As New SqlClient.SqlDataAdapter()
        adapter.SelectCommand = DirectCast(cmd, SqlClient.SqlCommand)
        Dim dt As New DataTable("Generated")

        Try
            cmd.Connection.Open()
            adapter.FillSchema(dt, SchemaType.Source)
            adapter.Fill(dt)
        Finally
            cmd.Connection.Close()
        End Try
        Return dt
    End Function



    Public Function GetExecutionStatement(cmd As SqlCommand)

        Dim quotedParameterTypes As DbType() = New DbType() {DbType.AnsiString, DbType.Date, DbType.DateTime, DbType.Guid, DbType.String, DbType.AnsiStringFixedLength, _
 DbType.StringFixedLength}
        Dim query As String = cmd.CommandText

        Dim arrParams = New SqlParameter(cmd.Parameters.Count - 1) {}
        cmd.Parameters.CopyTo(arrParams, 0)

        For Each prm As SqlParameter In arrParams.OrderByDescending(Function(p) p.ParameterName.Length)
            Dim value As String = prm.Value.ToString()

            If (prm.DbType = DbType.DateTime) Then
                value = "'" & DirectCast(prm.Value, DateTime).ToString("yyyy-MM-dd hh:mm:ss.mmm") & "'"
                'value = "'" & value & "'"
            ElseIf quotedParameterTypes.Contains(prm.DbType) Then
                value = "'" & value & "'"
            End If


            query = query.Replace("@" & prm.ParameterName, value)
        Next

        Return query
    End Function



    Public Function EUS_CustomerPhotos_SetDefault(CustomerID As Integer, CustomerPhotosID As Long?,
                                                  IsUser As Boolean,
                                                  IsAdmin As Boolean) As Long
        Dim success As Long
        Try
            Dim sql As String = <sql><![CDATA[
declare @result bigint=0
exec @result=[EUS_CustomerPhotos_SetDefault] @CustomerId=@CustomerId, @CustomerPhotosID=@CustomerPhotosID,@IsUser=@IsUser,@IsAdmin=@IsAdmin;
select @result as Result;
]]></sql>
            Dim cmd As SqlCommand = DataHelpers.GetSqlCommand(sql)
            cmd.Parameters.AddWithValue("@CustomerId", CustomerID)
            If (CustomerPhotosID.HasValue) Then
                cmd.Parameters.AddWithValue("@CustomerPhotosID", CustomerPhotosID)
            Else
                cmd.Parameters.AddWithValue("@CustomerPhotosID", System.DBNull.Value)
            End If
            cmd.Parameters.AddWithValue("@IsUser", IsUser)
            cmd.Parameters.AddWithValue("@IsAdmin", IsAdmin)
            success = DataHelpers.ExecuteScalar(cmd)

            'Dim dt As DataTable = DataHelpers.GetDataTable(cmd)
            'If (dt) Then
        Catch ex As Exception
            Throw
        End Try

        Return success
    End Function


    '    Public Function SetFirstDefaultEUS_CustomerPhotos(CustomerID As Integer,
    '                                                 IsUser As Boolean,
    '                                                 IsAdmin As Boolean) As Long
    '        Dim success As Long
    '        Try

    '            Dim sql As String = <sql><![CDATA[
    'declare @result bigint=0
    'exec @result=[EUS_CustomerPhotos_SetDefault] @CustomerId=@CustomerId,@IsUser=@IsUser,@IsAdmin=@IsAdmin;
    'select @result as Result;
    ']]></sql>
    '            Dim cmd As SqlCommand = DataHelpers.GetSqlCommand(sql)
    '            cmd.Parameters.AddWithValue("@CustomerId", CustomerID)
    '            cmd.Parameters.AddWithValue("@IsUser", IsUser)
    '            cmd.Parameters.AddWithValue("@IsAdmin", IsAdmin)
    '            success = DataHelpers.ExecuteScalar(cmd)

    '        Catch ex As Exception
    '            Throw
    '        End Try

    '        Return success

    '        'Try

    '        '    Dim ta As New DSMembersTableAdapters.EUS_CustomerPhotosTableAdapter
    '        '    ta.Connection = New SqlConnection(DataHelpers.ConnectionString)
    '        '    'ByRef ds As DSMembers,

    '        '    Dim ds As New DSMembers
    '        '    ta.FillByProfileIDOrMirrorID(ds.EUS_CustomerPhotos, CustomerID)

    '        '    Dim firstPhotoIndex As Integer = -1
    '        '    Dim defaultPhotoFound As Boolean = False

    '        '    Dim counter As Integer = 0
    '        '    For counter = 0 To (ds.EUS_CustomerPhotos.Count - 1)
    '        '        Dim dr As DSMembers.EUS_CustomerPhotosRow = ds.EUS_CustomerPhotos(counter)


    '        '        ''''''''''''''''''''''
    '        '        ' get the first photo that is not default and not declined
    '        '        ''''''''''''''''''''''
    '        '        If (firstPhotoIndex = -1 AndAlso
    '        '            dr.DisplayLevel = 0 AndAlso
    '        '            dr.HasAproved = True AndAlso
    '        '            (dr.IsHasDeclinedNull() OrElse dr.HasDeclined = False) AndAlso
    '        '            (dr.IsIsDefaultNull() OrElse dr.IsDefault = False) AndAlso
    '        '            (dr.IsIsDeletedNull() OrElse dr.IsDeleted = False)) Then

    '        '            firstPhotoIndex = counter
    '        '        End If

    '        '        ''''''''''''''''''''''
    '        '        ' search for available default photos
    '        '        ''''''''''''''''''''''
    '        '        If (dr.DisplayLevel = 0 AndAlso
    '        '            dr.HasAproved = True AndAlso
    '        '            (dr.IsHasDeclinedNull() OrElse dr.HasDeclined = False) AndAlso
    '        '            dr.IsDefault = True AndAlso
    '        '            (dr.IsIsDeletedNull() OrElse dr.IsDeleted = False)) Then

    '        '            defaultPhotoFound = True
    '        '        End If
    '        '    Next


    '        '    If (Not defaultPhotoFound AndAlso firstPhotoIndex >= 0 AndAlso firstPhotoIndex < ds.EUS_CustomerPhotos.Count) Then
    '        '        ds.EUS_CustomerPhotos.Rows(firstPhotoIndex)("IsDefault") = True
    '        '        ta.Update(ds)
    '        '        success = True
    '        '    End If
    '        'Catch ex As Exception
    '        '    Throw
    '        'End Try

    '        Return success
    '    End Function


    Public Function EUS_CustomerPhotos_ApproveNewPhotos(CustomerID As Integer) As Long
        Dim success As Long
        Try

            Dim sql As String = "exec [EUS_CustomerPhotos_ApproveNewPhotos] @CustomerId=@CustomerId"
            Dim cmd As SqlCommand = DataHelpers.GetSqlCommand(sql)
            cmd.Parameters.AddWithValue("@CustomerId", CustomerID)
            success = DataHelpers.ExecuteNonQuery(cmd)

        Catch ex As Exception
            Throw
        End Try

        Return success
    End Function




    Public Sub CopyEUS_ProfilesRowsData(ByRef EUS_Profiles As DSMembers.EUS_ProfilesDataTable, ByRef TargetDataRow As DataRow, ByRef SourceDataRow As DataRow)
        ''''''''''''''''''''''''''''''''''''''''''''
        ' copy data from mirror record to master, overwrite old user data with new
        ''''''''''''''''''''''''''''''''''''''''''''

        ' copy data that is not read only
        Dim cnt As Integer = 0
        For cnt = 0 To EUS_Profiles.Columns.Count - 1
            Dim dc As DataColumn = EUS_Profiles.Columns(cnt)
            If (dc.ReadOnly OrElse dc.ColumnName = "IsMaster" OrElse dc.ColumnName = "MirrorProfileID") Then
                Continue For
            End If

            TargetDataRow(cnt) = SourceDataRow(cnt)
        Next

    End Sub


    Public Function ApproveProfile(ByRef members As DSMembers, Optional IsAutoApproved As Boolean = False) As Boolean
        Dim success As Boolean
        Try

            If (members.EUS_Profiles.Rows.Count = 1) Then
                success = ApproveNewProfile(members, IsAutoApproved)
            Else
                success = ApproveExistingProfile(members, IsAutoApproved)
            End If
        Catch ex As Exception
            Throw
        End Try
        Return success
    End Function

    Public Function ApproveProfile(loginName As String, Optional IsAutoApproved As Boolean = False) As Boolean
        Dim success As Boolean
        Try
            Dim members As DSMembers = DataHelpers.GetEUS_Profiles_ByLoginName(loginName)

            If (members.EUS_Profiles.Rows.Count = 1) Then
                success = ApproveNewProfile(members, IsAutoApproved)

                'Dim customerid As Integer = members.EUS_Profiles.Rows(0)("ProfileID")
                'Dim dt As DataTable = clsSearchHelper.GetMembersThatShouldBeNotifiedForNewMember(customerid)

                'If (dt.Rows.Count > 0) Then
                '    Dim cnt As Integer
                '    For cnt = 0 To dt.Rows.Count - 1
                '        Dim toProfileId As Integer = dt.Rows(cnt)("CustomerID")
                '        clsUserDoes.SendEmailNotification(NotificationType.AutoNotification, customerid, toProfileId)
                '    Next
                'End If


            Else
                success = ApproveExistingProfile(members, IsAutoApproved)
            End If
        Catch ex As Exception
            Throw
        End Try
        Return success
    End Function

    Public Function ApproveNewProfile(members As DSMembers, Optional IsAutoApproved As Boolean = False) As Boolean
        Dim success As Boolean
        Try

            Dim masterRow As DSMembers.EUS_ProfilesRow = members.EUS_Profiles.Rows(0)

            Dim sql As String = "exec [EUS_Profiles_ApproveNewProfile] @CustomerId=@CustomerId, @IsAutoApproved=@IsAutoApproved"
            Dim cmd As SqlCommand = DataHelpers.GetSqlCommand(sql)
            cmd.Parameters.AddWithValue("@CustomerId", masterRow.ProfileID)
            cmd.Parameters.AddWithValue("@IsAutoApproved", IsAutoApproved)
            DataHelpers.ExecuteNonQuery(cmd)
            success = True

            'Dim masterRow As DSMembers.EUS_ProfilesRow = members.EUS_Profiles.Rows(0)
            'masterRow.MirrorProfileID = 0
            'masterRow.IsMaster = True
            'masterRow.Status = ProfileStatusEnum.Approved
            'masterRow.IsAutoApproved = IsAutoApproved


            'Dim mirrorRow As DSMembers.EUS_ProfilesRow = members.EUS_Profiles.NewEUS_ProfilesRow()
            'mirrorRow.ItemArray = masterRow.ItemArray
            'mirrorRow.IsMaster = False
            'mirrorRow.ProfileID = -1
            'mirrorRow.MirrorProfileID = masterRow.ProfileID
            'members.EUS_Profiles.AddEUS_ProfilesRow(mirrorRow)

            'DataHelpers.UpdateEUS_Profiles(members)


            'Dim newProfileId As Integer = 0
            'For Each dr As DataRow In members.EUS_Profiles.Rows
            '    If (dr("MirrorProfileID").ToString() = masterRow.ProfileID.ToString()) Then
            '        newProfileId = dr("ProfileID")
            '        Exit For
            '    End If
            'Next

            'For Each dr As DataRow In members.EUS_Profiles.Rows
            '    If (dr("ProfileID").ToString() = masterRow.ProfileID.ToString()) Then
            '        dr("MirrorProfileID") = newProfileId
            '        Exit For
            '    End If
            'Next

            'DataHelpers.UpdateEUS_Profiles(members)
            'success = True
            ''Try
            ''    clsMyMail.SendMail(masterRow.eMail, "Your subscription to Goomena.com approved.", "Your subscription to Goomena.com approved.")
            ''Catch ex As Exception
            ''End Try

        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        End Try
        Return success
    End Function

    Public Function ApproveExistingProfile(members As DSMembers, Optional IsAutoApproved As Boolean = False) As Boolean
        Dim success As Boolean
        Try

            Dim masterRow As DSMembers.EUS_ProfilesRow = members.EUS_Profiles.Rows(0)
            Dim mirrorRow As DSMembers.EUS_ProfilesRow = members.EUS_Profiles.Rows(1)

            Dim isvalid = True
            If (masterRow.ProfileID = mirrorRow.ProfileID) Then
                isvalid = False
                Throw New System.Exception("Invalid profile data: " & vbCrLf & vbTab & "the mirror and master is same table record." & vbCrLf & vbTab & "Cannot approve.")
            End If

            If (isvalid) Then

                ''''''''''''''''''''''''''''''''''''''''''''
                ' copy data from mirror record to master, overwrite old user data with new
                ''''''''''''''''''''''''''''''''''''''''''''

                CopyEUS_ProfilesRowsData(members.EUS_Profiles, masterRow, mirrorRow)
                '' copy data that is not read only
                'Dim cnt As Integer = 0
                'For cnt = 0 To members.EUS_Profiles.Columns.Count - 1
                '    If (Not members.EUS_Profiles.Columns(cnt).ReadOnly) Then
                '        masterRow(cnt) = mirrorRow(cnt)
                '    End If
                'Next


                mirrorRow.Status = ProfileStatusEnum.Approved
                masterRow.Status = ProfileStatusEnum.Approved

                'mirrorRow.IsMaster = False
                'masterRow.IsMaster = True

                'mirrorRow.MirrorProfileID = masterRow.ProfileID
                'masterRow.MirrorProfileID = mirrorRow.ProfileID

                mirrorRow.IsAutoApproved = IsAutoApproved
                masterRow.IsAutoApproved = IsAutoApproved

                '' update db
                'PergormReLogin()
                'CheckWebServiceCallResult(gAdminWS.UpdateEUS_Profiles(gLoginRec.Token, Me.DSMembers))

                '' send email to user
                '_SendEmailMessage(mirrorRow.eMail, "Your changes to " & My.MySettings.Default.SiteNameForCustomers & " profile are accepted.", "Your changes to " & My.MySettings.Default.SiteNameForCustomers & " profile are accepted.")

                DataHelpers.UpdateEUS_Profiles(members)

                'clsMyMail.SendMail(masterRow.eMail, "Your changes to Goomena.com profile are accepted.", "Your changes to Goomena.com profile are accepted.")

                success = True
            End If

        Catch ex As Exception
            Throw
        End Try
        Return success
    End Function




    Public Function RejectProfile(ByRef members As DSMembers) As Boolean
        Dim success As Boolean
        Try
            If (members.EUS_Profiles.Rows.Count = 1) Then
                success = RejectNewProfile(members)
            Else
                success = RejectExistingProfile(members)
            End If
        Catch ex As Exception
            Throw
        End Try
        Return success
    End Function


    Public Function RejectProfile(loginName As String) As Boolean
        Dim success As Boolean
        Try
            Dim members As DSMembers = DataHelpers.GetEUS_Profiles_ByLoginName(loginName)

            If (members.EUS_Profiles.Rows.Count = 1) Then
                success = RejectNewProfile(members)
            Else
                success = RejectExistingProfile(members)
            End If
        Catch ex As Exception
            Throw
        End Try
        Return success
    End Function


    Public Function RejectNewProfile(members As DSMembers)
        Dim success As Boolean
        Try

            Dim masterRow As DSMembers.EUS_ProfilesRow = members.EUS_Profiles.Rows(0)
            masterRow.Status = ProfileStatusEnum.Rejected

            DataHelpers.UpdateEUS_Profiles(members)

            success = True
        Catch ex As Exception
            Throw
        End Try
        Return success
    End Function


    Public Function RejectExistingProfile(members As DSMembers)
        Dim success As Boolean
        Try

            Dim masterRow As DSMembers.EUS_ProfilesRow = members.EUS_Profiles.Rows(0)
            Dim mirrorRow As DSMembers.EUS_ProfilesRow = members.EUS_Profiles.Rows(1)

            If (masterRow.IsMaster = False) Then
                masterRow = members.EUS_Profiles.Rows(1)
                mirrorRow = members.EUS_Profiles.Rows(0)
            End If

            Dim isvalid = True
            If (masterRow.ProfileID = mirrorRow.ProfileID) Then
                isvalid = False
                Throw New System.Exception("Invalid profile data: " & vbCrLf & vbTab & "the mirror and master is same table record." & vbCrLf & vbTab & "Cannot approve.")
            End If

            If (isvalid) Then
                ''''''''''''''''''''''''''''''''''''''''''''
                ' copy data from master record to mirror, overwrite user changes
                ''''''''''''''''''''''''''''''''''''''''''''

                '' copy data that is not read only
                'Dim cnt As Integer = 0
                'For cnt = 0 To members.EUS_Profiles.Columns.Count - 1
                '    If (Not members.EUS_Profiles.Columns(cnt).ReadOnly) Then
                '        mirrorRow(cnt) = masterRow(cnt)
                '    End If
                'Next

                'mirrorRow.IsMaster = False
                'masterRow.IsMaster = True

                'mirrorRow.MirrorProfileID = masterRow.ProfileID
                'masterRow.MirrorProfileID = mirrorRow.ProfileID

                CopyEUS_ProfilesRowsData(members.EUS_Profiles, mirrorRow, masterRow)

                ' update db
                DataHelpers.UpdateEUS_Profiles(members)

                success = True
            End If

        Catch ex As Exception
            Throw
        End Try
        Return success
    End Function



    Public Function ApprovePhoto(photoId As Integer,
                                IsUser As Boolean,
                                IsAdmin As Boolean, Optional IsAutoApproved As Boolean = False) As Boolean
        Dim IsDefaultPhotoSet As Long
        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try

            Dim photo As Core.DLL.EUS_CustomerPhoto = (From itm In _CMSDBDataContext.EUS_CustomerPhotos
                                              Where itm.CustomerPhotosID = photoId
                                              Select itm).FirstOrDefault()

            'If (IsAutoApproved) Then
            photo.HasAproved = True
            photo.HasDeclined = False
            'End If
            photo.IsAutoApproved = IsAutoApproved

            _CMSDBDataContext.SubmitChanges()


            IsDefaultPhotoSet = DataHelpers.EUS_CustomerPhotos_SetDefault(photo.CustomerID, Nothing, IsUser, IsAdmin)
            If (IsDefaultPhotoSet > 0) Then
                clsCustomer.AddFreeRegistrationCredits_WithCheck(photo.CustomerID, "", "", "")
            End If

        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try
        Return IsDefaultPhotoSet > 0
    End Function


    Public Function RejectPhoto(photoId As Integer) As Boolean
        Dim IsDefaultPhotoSet As Long
        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try


            Dim photo As Core.DLL.EUS_CustomerPhoto = (From itm In _CMSDBDataContext.EUS_CustomerPhotos
                                              Where itm.CustomerPhotosID = photoId
                                              Select itm).FirstOrDefault()

            photo.HasAproved = False
            photo.HasDeclined = True
            photo.IsDefault = False
            photo.IsAutoApproved = False

            _CMSDBDataContext.SubmitChanges()

            IsDefaultPhotoSet = DataHelpers.EUS_CustomerPhotos_SetDefault(photo.CustomerID, Nothing, False, False)
            If (IsDefaultPhotoSet > 0) Then
                clsCustomer.AddFreeRegistrationCredits_WithCheck(photo.CustomerID, "", "", "")
            End If

        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try
        Return IsDefaultPhotoSet > 0
    End Function



    Public Sub LogProfileAccess(ProfileID As Integer, KeybLoginName As String, keybPassword As String,
                                Success As Boolean,
                                Title As String,
                                Device As String,
                                Cookie As String,
                                IP As String)

        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try
            Dim access As New Core.DLL.SYS_ProfilesAccess

            access.DateTimeAccess = Date.UtcNow
            access.KeybLoginName = KeybLoginName
            access.keybPassword = keybPassword
            access.ProfileID = ProfileID
            access.Success = Success
            access.Title = Title
            If (Not String.IsNullOrEmpty(Device)) Then
                access.Device = Mid(Device, 1, 100)
            End If
            access.Cookie = Mid(Cookie, 1, 50)
            access.IP = Mid(IP, 1, 30)

            _CMSDBDataContext.SYS_ProfilesAccesses.InsertOnSubmit(access)
            _CMSDBDataContext.SubmitChanges()

        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try
    End Sub



    Public Function GetAvailable_AFF_PayoutTypes() As DataTable
        Dim dt As DataTable = Nothing
        dt = DataHelpers.GetDataTable("SELECT * FROM AFF_PayoutTypes WHERE IsActive=1 ORDER BY Title")
        Return dt
    End Function


    Public Function EUS_CreditsBonusCodes_IsCodeAvailable(ProfileId As Integer, Code As String) As Boolean
        Dim dt As Long = 0
        'Dim sql As String = "select isnull((SELECT case when CreditsCodeId > 0 then 0 else 1 end  FROM EUS_CreditsBonusCodes WHERE Code=@Code and ProfileID=@ProfileID), 1) as IsCodeAvailable"
        'Dim sql As String = "select isnull((SELECT case when SubmittedDate is null then 1 else 0 end FROM EUS_CreditsBonusCodes WHERE Code=@Code and ProfileID=@ProfileID), 0) as IsCodeAvailable"

        Dim sql As String = <sql><![CDATA[
if('Q5JKLRS4T'=@Code and not exists(SELECT  CreditsCodeId 
									FROM EUS_CreditsBonusCodes 
									WHERE Code=@Code and ProfileID=@ProfileID))	
	select 1 as IsCodeAvailable
else
	select 0 as IsCodeAvailable
]]></sql>

        Dim command As SqlClient.SqlCommand = GetSqlCommand(sql)
        command.Parameters.AddWithValue("@Code", Code)
        command.Parameters.AddWithValue("@ProfileID", ProfileId)
        dt = DataHelpers.ExecuteScalar(command)

        Return (dt = 1)
    End Function

    Public Function EUS_CreditsBonusCodes_DisableCode(ProfileId As Integer, Code As String) As Boolean
        Dim dt As Long = 0
        Dim sql As String = <sql><![CDATA[
if(exists(SELECT * FROM EUS_CreditsBonusCodes WHERE Code=@Code and ProfileID=@ProfileID))
begin
    update EUS_CreditsBonusCodes	set SubmittedDate = GETUTCDATE(),ProfileID=@ProfileID	WHERE Code=@Code
end
else
begin
    INSERT INTO [EUS_CreditsBonusCodes]
           ([Code]
           ,[SubmittedDate]
           ,[ProfileID])
     VALUES
           (@Code
           ,GETUTCDATE()
           ,@ProfileID)
end
]]></sql>
        '""

        Dim command As SqlClient.SqlCommand = GetSqlCommand(sql)
        command.Parameters.AddWithValue("@Code", Code)
        command.Parameters.AddWithValue("@ProfileID", ProfileId)
        dt = DataHelpers.ExecuteNonQuery(command)

        Return (dt > 0)
    End Function

    Public Function GetEUS_VoucherActivatedByVoucher(ctx As CMSDBDataContext, voucher As String) As EUS_VoucherActivated
        Dim prof As EUS_VoucherActivated = Nothing
        'Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try

            prof = (From itm In ctx.EUS_VoucherActivateds
                        Where (itm.Voucher.ToUpper() = voucher.ToUpper()) _
                        Select itm).FirstOrDefault()


        Catch ex As Exception
            Throw
        Finally
            '_CMSDBDataContext.Dispose()
        End Try

        Return prof
    End Function


    Public Sub InsertToEUS_VoucherActivated(ctx As CMSDBDataContext, voucher As String, profileid As Integer, CustomerTransactionID As Integer)
        Dim prof As EUS_VoucherActivated = Nothing
        'Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try

            prof = New EUS_VoucherActivated()
            prof.DateActivated = Date.UtcNow
            prof.ProfileID = profileid
            prof.Voucher = voucher
            prof.CustomerTransactionID = CustomerTransactionID

            ctx.EUS_VoucherActivateds.InsertOnSubmit(prof)
            ctx.SubmitChanges()

        Catch ex As Exception
            Throw
        Finally
            '_CMSDBDataContext.Dispose()
        End Try
    End Sub


    Public Function EUS_CustomerVerificationDocs_GetByCustomerID(CustomerID As Integer) As DSCustomer
        Dim ta As New DSCustomerTableAdapters.EUS_CustomerVerificationDocsTableAdapter
        Dim ds As New DSCustomer()
        ta.Connection = New SqlConnection(DataHelpers.ConnectionString)
        ta.FillBy_CustomerID(ds.EUS_CustomerVerificationDocs, CustomerID)

        Return ds
    End Function



    Public Function GetAffiliateBy_SiteName(SiteName As String) As String
        Dim result As String = ""
        Try
            Dim sql As String = <sql><![CDATA[
select dbo.EUS_Profiles.aff_code
from AFF_Sites 
inner join dbo.EUS_Profiles on AFF_Sites.CustomerID=EUS_Profiles.ProfileID 
where [Site] like @SiteName or [Site] like 'www.'+@SiteName
]]></sql>
            Dim cmd As SqlCommand = DataHelpers.GetSqlCommand(sql)
            cmd.Parameters.AddWithValue("@SiteName", SiteName)
            Dim dt As DataTable = DataHelpers.GetDataTable(cmd)
            If (dt.Rows.Count > 0) Then
                result = dt.Rows(0)("aff_code").ToString()
            End If

        Catch ex As Exception
            Throw
        Finally

        End Try
        Return result
    End Function


    Public Function EUS_CustomerPhotos_GetVisiblePhotos(CustomerID As Integer) As Integer
        Dim result As Integer = 0
        Try
            Dim sql As String = <sql><![CDATA[
select COUNT(*) as VisiblePhotos
from dbo.EUS_CustomerPhotos
where CustomerID=@CustomerID
and isnull(IsDeleted,0)=0
and isnull(HasDeclined,0)=0 
and isnull(HasAproved,0)=1
]]></sql>
            Dim cmd As SqlCommand = DataHelpers.GetSqlCommand(sql)
            cmd.Parameters.AddWithValue("@CustomerID", CustomerID)
            result = DataHelpers.ExecuteScalar(cmd)

        Catch ex As Exception
            Throw
        Finally

        End Try
        Return result
    End Function


    Public Function EUS_CustomerPhotos_HasPrivatePhotos(CustomerID As Integer) As Integer
        Dim result As Integer = 0
        Try
            Dim sql As String = <sql><![CDATA[
select COUNT(*) as VisiblePhotos
from dbo.EUS_CustomerPhotos
where CustomerID=@CustomerID
and isnull(IsDeleted,0)=0
and isnull(HasDeclined,0)=0 
and isnull(HasAproved,0)=1
and isnull(DisplayLevel,0)>0
]]></sql>
            Dim cmd As SqlCommand = DataHelpers.GetSqlCommand(sql)
            cmd.Parameters.AddWithValue("@CustomerID", CustomerID)
            result = DataHelpers.ExecuteScalar(cmd)

        Catch ex As Exception
            Throw
        Finally

        End Try
        Return result
    End Function


    Public Function EUS_CustomerPhotos_GetVisiblePhotosFromTo(userWhoOwnsPhotos As Integer, userViewingPhotos As Integer) As Integer
        Dim result As Integer = 0
        Try
            Dim sql As String = <sql><![CDATA[

declare @VisiblePhotos int=0

select @VisiblePhotos=COUNT(*) 
from dbo.EUS_CustomerPhotos
where CustomerID=@userWhoOwnsPhotos
and isnull(IsDeleted,0)=0
and isnull(HasDeclined,0)=0 
and isnull(HasAproved,0)=1
and isnull(DisplayLevel,0)=0

if(@VisiblePhotos = 0)
begin
	select @VisiblePhotos=COUNT(*) 
	from dbo.EUS_CustomerPhotos
	where CustomerID=@userWhoOwnsPhotos
	and isnull(IsDeleted,0)=0
	and isnull(HasDeclined,0)=0 
	and isnull(HasAproved,0)=1
	and isnull(DisplayLevel,0)>0
	and exists(select * from EUS_ProfilePhotosLevel lvl where lvl.FromProfileID=@userWhoOwnsPhotos and lvl.ToProfileID=@userViewingPhotos)
end

select @VisiblePhotos as VisiblePhotos

]]></sql>
            Dim cmd As SqlCommand = DataHelpers.GetSqlCommand(sql)
            cmd.Parameters.AddWithValue("@userWhoOwnsPhotos", userWhoOwnsPhotos)
            cmd.Parameters.AddWithValue("@userViewingPhotos", userViewingPhotos)
            result = DataHelpers.ExecuteScalar(cmd)

        Catch ex As Exception
            Throw
        Finally

        End Try
        Return result
    End Function

    ''' <summary>
    '''  find how many messages the one member sent to other and vice versa
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function EUS_Messages_CountMessagesExchangedFromTo_ByReferrer(ReferrerID As Integer, CustomerID As Integer) As DataTable
        Dim dt As New DataTable()
        Try
            Dim sql As String = <sql><![CDATA[
select
		MessagesSent=isnull((Select COUNT(*) 
								from dbo.EUS_Messages
								where ProfileIDOwner=@female and FromProfileID=@female and ToProfileID=@male),0)
		,MessagesSentAndOpened=isnull((Select COUNT(*) 
								from dbo.EUS_Messages
								where ProfileIDOwner=@female and FromProfileID=@female and ToProfileID=@male and isnull(cost,0)>0),0)
		,MessagesSentAndOpened_Male=isnull((Select COUNT(*) 
								from dbo.EUS_Messages
								where ProfileIDOwner=@male and FromProfileID=@female and ToProfileID=@male and StatusID=1),0)
		,MessagesReceivedAndOpened=isnull((Select COUNT(*) 
									from dbo.EUS_Messages
									where ProfileIDOwner=@female and FromProfileID=@male and ToProfileID=@female and isnull(cost,0)>0),0)
		,MessagesReceivedAndOpened_Male=isnull((Select COUNT(*) 
									from dbo.EUS_Messages
									where ProfileIDOwner=@male and FromProfileID=@male and ToProfileID=@female and StatusID=1),0)
]]></sql>
            Dim cmd As SqlCommand = DataHelpers.GetSqlCommand(sql)
            cmd.Parameters.AddWithValue("@female", ReferrerID)
            cmd.Parameters.AddWithValue("@male", CustomerID)
            dt = DataHelpers.GetDataTable(cmd)

        Catch ex As Exception
            Throw
        Finally

        End Try
        Return dt
    End Function


    '    Public Function EUS_Profiles_GetCountry(CustomerID As Integer) As String
    '        Dim result As string 0
    '        Try
    '            Dim sql As String = <sql><![CDATA[
    'select top (1) Country
    'from EUS_Profiles
    'where profileid=@CustomerID
    ']]></sql>
    '            Dim cmd As SqlCommand = DataHelpers.GetSqlCommand(sql)
    '            cmd.Parameters.AddWithValue("@CustomerID", CustomerID)
    '            result = DataHelpers.GetDataTable(cmd)

    '        Catch ex As Exception
    '            Throw
    '        Finally

    '        End Try
    '        Return result
    '    End Function



    Public Function EUS_Profiles_GetREF_RegStep(CustomerID As Integer) As Integer
        Dim result As Integer = 0
        Try
            Dim sql As String = <sql><![CDATA[
select isnull((
    select top (1) REF_RegStep
    from EUS_Profiles
    where profileid=@CustomerID
),0) as REF_RegStep
]]></sql>
            Dim cmd As SqlCommand = DataHelpers.GetSqlCommand(sql)
            cmd.Parameters.AddWithValue("@CustomerID", CustomerID)
            result = DataHelpers.ExecuteScalar(cmd)

        Catch ex As Exception
            Throw
        Finally

        End Try
        Return result
    End Function

    Public Function EUS_Profiles_UpdateREF_RegStep(CustomerID As Integer, REF_RegStep As Integer) As Integer
        Dim result As Integer = 0
        Try
            Dim sql As String = <sql><![CDATA[
update dbo.EUS_Profiles set REF_RegStep=@REF_RegStep where referrerParentId>0 and profileid=@CustomerID and REF_RegStep<>@REF_RegStep
]]></sql>
            Dim cmd As SqlCommand = DataHelpers.GetSqlCommand(sql)
            cmd.Parameters.AddWithValue("@CustomerID", CustomerID)
            cmd.Parameters.AddWithValue("@REF_RegStep", REF_RegStep)
            result = DataHelpers.ExecuteNonQuery(cmd)

        Catch ex As Exception
            Throw
        Finally

        End Try
        Return result
    End Function

    Public Function EUS_Profiles_Update_StartingFreeCredits(CustomerID As Integer) As Integer
        Dim result As Integer = 0
        Try
            Dim sql As String = <sql><![CDATA[
update dbo.EUS_Profiles set StartingFreeCredits=@StartingFreeCredits where (ProfileID=@CustomerId or MirrorProfileID=@CustomerId) and StartingFreeCredits=0
]]></sql>
            Dim cmd As SqlCommand = DataHelpers.GetSqlCommand(sql)
            cmd.Parameters.AddWithValue("@CustomerID", CustomerID)
            cmd.Parameters.AddWithValue("@StartingFreeCredits", True)
            result = DataHelpers.ExecuteNonQuery(cmd)

        Catch ex As Exception
            Throw
        Finally

        End Try
        Return result
    End Function


    Public Function EUS_Profiles_GetMasterProfileID(CustomerID As Integer) As Integer
        Dim result As Integer = 0
        Try
            Dim sql As String = <sql><![CDATA[
declare @FoundProfileID int =0
select @FoundProfileID = 
		ISNULL((select profileID from dbo.EUS_Profiles 
				where (profileID=@profileID or mirrorprofileID=@profileID)
				and ismaster=1),0)
				
if(@FoundProfileID = 0)
	select @FoundProfileID=profileID from dbo.EUS_Profiles 
	where (profileID=@profileID or mirrorprofileID=@profileID)

select @FoundProfileID as ProfileID
]]></sql>
            Dim cmd As SqlCommand = DataHelpers.GetSqlCommand(sql)
            cmd.Parameters.AddWithValue("@profileID", CustomerID)
            result = DataHelpers.ExecuteScalar(cmd)

        Catch ex As Exception
            Throw
        Finally

        End Try
        Return result
    End Function



    Public Function EUS_Profiles_GetMasterStatus(CustomerID As Integer) As Long
        Dim result As Long = 0
        Try
            Dim sql As String = <sql><![CDATA[
declare @FoundProfileID int =0
select @FoundProfileID = 
		ISNULL((select profileID from dbo.EUS_Profiles 
				where (profileID=@profileID or mirrorprofileID=@profileID)
				and ismaster=1),0)
				
if(@FoundProfileID = 0)
	select @FoundProfileID=profileID from dbo.EUS_Profiles 
	where (profileID=@profileID or mirrorprofileID=@profileID)

select Status from dbo.EUS_Profiles where ProfileID=@FoundProfileID
]]></sql>
            Dim cmd As SqlCommand = DataHelpers.GetSqlCommand(sql)
            cmd.Parameters.AddWithValue("@profileID", CustomerID)
            result = DataHelpers.ExecuteScalar(cmd)
        Catch ex As Exception
            Throw
        Finally

        End Try
        Return result
    End Function


    Public Function SYS_SMTPServers_GetByName(name As String) As DataSet
        Dim result As New DataSet()
        Try
            Dim sql As String = <sql><![CDATA[
SELECT [SMTPServerCredentialsID]
      ,[Name]
      ,[SMTPServerName]
      ,[SMTPLoginName]
      ,[SMTPPassword]
      ,[SMTPOutPort]
      ,[SMTPFromEmail]
  FROM [SYS_SMTPServers]
WHERE [Name]=@Name
]]></sql>
            Dim cmd As SqlCommand = DataHelpers.GetSqlCommand(sql)
            cmd.Parameters.AddWithValue("@Name", name)
            result = DataHelpers.GetDataSet(cmd)

        Catch ex As Exception
            Throw
        Finally

        End Try
        Return result
    End Function

    Public Function SYS_SMTPServers() As DataSet
        Dim result As New DataSet()
        Try
            Dim sql As String = <sql><![CDATA[
SELECT [SMTPServerCredentialsID]
      ,[Name]
      ,[SMTPServerName]
      ,[SMTPLoginName]
      ,[SMTPPassword]
      ,[SMTPOutPort]
      ,[SMTPFromEmail]
  FROM [SYS_SMTPServers]
]]></sql>
            Dim cmd As SqlCommand = DataHelpers.GetSqlCommand(sql)
            result = DataHelpers.GetDataSet(cmd)

        Catch ex As Exception
            Throw
        Finally

        End Try
        Return result
    End Function

    Public Function SYS_SMTPServers_GetByID(SMTPServerCredentialsID As Integer) As SYS_SMTPServer
        Dim result As SYS_SMTPServer = Nothing
        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try

            result = (From itm In _CMSDBDataContext.SYS_SMTPServers
                    Where itm.SMTPServerCredentialsID = SMTPServerCredentialsID
                    Select itm).SingleOrDefault()


        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

        Return result
    End Function




    Public Function SYS_SMTPServers_GetByNameLINQ(name As String) As SYS_SMTPServer
        Dim result As SYS_SMTPServer = Nothing
        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try

            result = (From itm In _CMSDBDataContext.SYS_SMTPServers
                    Where itm.Name = name
                    Select itm).SingleOrDefault()


        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

        Return result
    End Function



    Public Function EUS_UserMessages_GetByPosition(ProfileID As Integer, position As String) As DSCustomer
        Dim ta As New DSCustomerTableAdapters.EUS_UserMessagesTableAdapter
        Dim ds As New DSCustomer()
        ta.Connection = New SqlConnection(DataHelpers.ConnectionString)
        ta.FillBy_CustomerID_Position(ds.EUS_UserMessages, position, ProfileID)

        Return ds
    End Function



    Public Sub UpdateEUS_UserMessages(ByRef ds As DSCustomer)
        Dim ta As New DSCustomerTableAdapters.EUS_UserMessagesTableAdapter
        ta.Connection = New SqlConnection(DataHelpers.ConnectionString)
        ta.Update(ds.EUS_UserMessages)
    End Sub


    Public Function EUS_Profiles_SetLimitedOnDate(CustomerID As Integer, LimitedOnDate As DateTime) As Long
        Dim result As Long = 0
        Try
            Dim sql As String = <sql><![CDATA[
update   dbo.EUS_Profiles 
set LimitedOnDate=@LimitedOnDate
where (profileID=@profileID or mirrorprofileID=@profileID)
]]></sql>
            Dim cmd As SqlCommand = DataHelpers.GetSqlCommand(sql)
            cmd.Parameters.AddWithValue("@profileID", CustomerID)
            cmd.Parameters.AddWithValue("@LimitedOnDate", LimitedOnDate)
            result = DataHelpers.ExecuteNonQuery(cmd)
        Catch ex As Exception
            Throw
        Finally

        End Try
        Return result
    End Function


    Public Function EUS_Profiles_MarkDeleted(CustomerID As Integer) As Long
        Dim result As Long = 0
        Try
            Dim sql As String = <sql><![CDATA[
update   dbo.EUS_Profiles 
set 
    LoginName='deleted_' + LoginName
    ,eMail='deleted_' + eMail
    ,Status=@Status
where (profileID=@profileID or mirrorprofileID=@profileID)
]]></sql>
            Dim cmd As SqlCommand = DataHelpers.GetSqlCommand(sql)
            cmd.Parameters.AddWithValue("@profileID", CustomerID)
            cmd.Parameters.AddWithValue("@Status", ProfileStatusEnum.Deleted)
            result = DataHelpers.ExecuteNonQuery(cmd)
        Catch ex As Exception
            Throw
        Finally

        End Try
        Return result
    End Function


    Public Function EUS_Profiles_GetLimitedOnDate(CustomerID As Integer) As DateTime
        Dim LimitedOnDate As DateTime = DateTime.MinValue

        Try
            Dim sql As String = <sql><![CDATA[
select top(1) LimitedOnDate
from dbo.EUS_Profiles
where (profileID=@profileID or mirrorprofileID=@profileID)
]]></sql>
            Dim cmd As SqlCommand = DataHelpers.GetSqlCommand(sql)
            cmd.Parameters.AddWithValue("@profileID", CustomerID)
            cmd.Parameters.AddWithValue("@LimitedOnDate", LimitedOnDate)
            Dim dt As DataTable = DataHelpers.GetDataTable(cmd)
            If (dt.Rows.Count > 0) Then
                If (Not dt.Rows(0)("LimitedOnDate").ToString() <> "") Then LimitedOnDate = dt.Rows(0)("LimitedOnDate")
            End If

        Catch ex As Exception
            Throw
        Finally

        End Try
        Return LimitedOnDate
    End Function

    Public Function EUS_CustomerTransaction_IsTransactionExists(CustomerID As Integer, TransactionInfo As String) As Long
        Dim ROWCOUNT As Long

        Dim sql As String = <sql><![CDATA[
select ISNULL((select top(1) [CustomerTransactionID]
                from dbo.[EUS_CustomerTransaction]
                where CustomerId=@CustomerId 
                and [TransactionInfo] like @TransactionInfo   --'%'+@TransactionInfo+'%'
                and [DebitAmount]>0),
      -1)
]]></sql>

        Dim cmd As SqlCommand = DataHelpers.GetSqlCommand(sql)
        cmd.Parameters.AddWithValue("@CustomerID", CustomerID)
        cmd.Parameters.AddWithValue("@TransactionInfo", TransactionInfo)
        ROWCOUNT = DataHelpers.ExecuteScalar(cmd)

        Return ROWCOUNT
    End Function




    Public Function SYS_UsersLogins_GetAll() As DSSYSData
        Dim ds As New DSSYSData()
        Dim ta As New DSSYSDataTableAdapters.SYS_UsersLoginsTableAdapter
        ta.Connection = New SqlConnection(DataHelpers.ConnectionString)
        ta.Fill(ds.SYS_UsersLogins)
        Return ds
    End Function


    Public Function SYS_UsersLogins_GetByLoginIdentifier(computerName As String) As DSSYSData
        Dim ds As New DSSYSData()
        Dim ta As New DSSYSDataTableAdapters.SYS_UsersLoginsTableAdapter
        ta.Connection = New SqlConnection(DataHelpers.ConnectionString)
        ta.FillBy_ComputerName(ds.SYS_UsersLogins, computerName)
        Return ds
    End Function



    Public Function GetCustomerTotalMoneyAndCredits(CustomerID As Integer) As DataTable
        Dim result As DataTable

        Dim sql As String = <sql><![CDATA[

select 
	isnull(Credits,0) as Credits, 
	isnull(DebitAmountReceived,0) as DebitAmountReceived
from (
	select SUM(Credits) as Credits
	from EUS_CustomerCredits
	where CustomerId=@CustomerID
	and Credits > 0
) as t1,
(
	select SUM(DebitAmountReceived) as DebitAmountReceived
	from EUS_CustomerTransaction
	where CustomerId=@CustomerID
	and TransactionType <> '-1'
	and TransactionType <> '1111'
	and TransactionInfo <> '1111'
	and TransactionInfo <> '3333'
	and TransactionInfo <> '-1'
	and TransactionInfo <> ''
) as t2


]]></sql>

        Dim cmd As SqlCommand = DataHelpers.GetSqlCommand(sql)
        cmd.Parameters.AddWithValue("@CustomerID", CustomerID)
        result = DataHelpers.GetDataTable(cmd)

        Return result
    End Function



    Public Function GetProofiles_ForRegistrationEmail(Country As String, GenderID As Integer) As DataTable
        Dim result As DataTable

        Dim sql As String = <sql><![CDATA[

select * from (
	SELECT TOP 32
		EUS_CustomerPhotos.CustomerID, 
		EUS_CustomerPhotos.FileName, 
		EUS_Profiles.LoginName,
		EUS_Profiles.DateTimeToRegister,
		CountryQualifier = case 
			when @Country=Country then 1
			else 0
                End
	FROM EUS_CustomerPhotos 
	with (nolock)
	JOIN EUS_Profiles ON EUS_CustomerPhotos.CustomerID = EUS_Profiles.ProfileID 
	WHERE(
		EUS_CustomerPhotos.ShowOnGrid=1
		And DisplayLevel=0
		And HasAproved=1 
		And ISNULL(HasDeclined,0)=0 
		AND ISNULL(IsDeleted,0)=0  
		And GenderId=@GenderID  
		AND (EUS_Profiles.Status = 2 OR EUS_Profiles.Status = 4) 
		and ISNULL(EUS_Profiles.PrivacySettings_DontShowOnPhotosGrid,0)=0
		AND ISNULL(EUS_Profiles.PrivacySettings_HideMeFromSearchResults,0)=0
	) 
	ORDER BY CountryQualifier desc, DateTimeToRegister desc 
) as t1 
ORDER BY newid() 

]]></sql>.Value


        Dim cmd As SqlCommand = DataHelpers.GetSqlCommand(sql)
        cmd.Parameters.AddWithValue("@Country", Country)
        cmd.Parameters.AddWithValue("@GenderID", GenderID)
        result = DataHelpers.GetDataTable(cmd)

        Return result
    End Function



    Public Sub EUS_Profile_SetOnlineManually(ProfileID As Integer, IP As String, Optional TimeUntil As DateTime? = Nothing)
        DataHelpers.UpdateEUS_Profiles_Activity(ProfileID, IP, True, TimeUntil)
        Dim loginName As String = DataHelpers.GetEUS_Profile_LoginName_ByProfileID(ProfileID)
        DataHelpers.LogProfileAccess(ProfileID, loginName, "[SET-ONLINE]", True, "[SET-ONLINE]", Nothing, Nothing, IP)
    End Sub


    Public Sub EUS_Profile_SetOfflineManually(ProfileID As Integer, IP As String)
        DataHelpers.UpdateEUS_Profiles_LogoutData(ProfileID)
        Dim loginName As String = DataHelpers.GetEUS_Profile_LoginName_ByProfileID(ProfileID)
        DataHelpers.LogProfileAccess(ProfileID, loginName, "[SET-OFFLINE]", True, "[SET-OFFLINE]", Nothing, Nothing, IP)
    End Sub


    Public Sub UpdateEUS_Messages_SetCopyMessageRead(profileid As Integer, MessageID As Long, CopyMessageID As Long, Optional isRead As Boolean = True)
        Try
            Dim sql As String = Nothing
            If (MessageID > 0) Then
                sql = "update dbo.EUS_Messages set CopyMessageRead=@CopyMessageRead where ProfileIDOwner=@ProfileIDOwner and EUS_MessageID=@MessageID"
            ElseIf (CopyMessageID > 0) Then
                sql = "update dbo.EUS_Messages set CopyMessageRead=@CopyMessageRead where ProfileIDOwner=@ProfileIDOwner and CopyMessageID=@CopyMessageID"
            End If
            If (sql IsNot Nothing) Then
                Dim cmd As SqlCommand = DataHelpers.GetSqlCommand(sql)
                cmd.Parameters.AddWithValue("@CopyMessageRead", isRead)
                cmd.Parameters.AddWithValue("@ProfileIDOwner", profileid)
                cmd.Parameters.AddWithValue("@MessageID", MessageID)
                cmd.Parameters.AddWithValue("@CopyMessageID", CopyMessageID)
                DataHelpers.ExecuteNonQuery(cmd)
            End If
        Catch ex As Exception
            Throw
        Finally
        End Try
    End Sub

    Public Function EUS_CustomerCredits_GetLastCreditsObtained(CustomerID As Integer, Optional NewBuy_CustomerTransactionID As Integer = -1) As DataTable
        Dim result As DataTable
        Dim sql As String = ""

        If (NewBuy_CustomerTransactionID > 0) Then
            sql = <sql><![CDATA[
SELECT TOP 1 [CustomerCreditsId]
      ,[CustomerId]
      ,[Credits]
      ,[DateTimeCreated]
      ,DateTimeExpiration
FROM [EUS_CustomerCredits]
where customerid=@CustomerID
and [Credits]>0
and not  [CustomerTransactionID] is null
and CustomerTransactionID<@NewBuy_CustomerTransactionID
and exists(
	select [CustomerTransactionID] 
	from [EUS_CustomerTransaction] 
	where [EUS_CustomerTransaction].[CustomerTransactionID]=[EUS_CustomerCredits].[CustomerTransactionID]
	and [DebitAmountReceived]>0
)
--and DateTimeExpiration>GETUTCDATE()
order by [CustomerCreditsId] desc
]]></sql>.Value

        Else
            sql = <sql><![CDATA[
SELECT TOP 1 [CustomerCreditsId]
      ,[CustomerId]
      ,[Credits]
      ,[DateTimeCreated]
      ,DateTimeExpiration
FROM [EUS_CustomerCredits]
where customerid=@CustomerID
and [Credits]>0
and not  [CustomerTransactionID] is null
and exists(
	select [CustomerTransactionID] 
	from [EUS_CustomerTransaction] 
	where [EUS_CustomerTransaction].[CustomerTransactionID]=[EUS_CustomerCredits].[CustomerTransactionID]
	and [DebitAmountReceived]>0
)
--and DateTimeExpiration>GETUTCDATE()
order by [CustomerCreditsId] desc
]]></sql>.Value

        End If


        Dim cmd As SqlCommand = DataHelpers.GetSqlCommand(sql)
        cmd.Parameters.AddWithValue("@CustomerID", CustomerID)
        cmd.Parameters.AddWithValue("@NewBuy_CustomerTransactionID", NewBuy_CustomerTransactionID)
        result = DataHelpers.GetDataTable(cmd)

        Return result
    End Function




    Public Function EUS_Profile_IsProfileActive(profileId As Integer) As Boolean
        Dim isactive As Boolean = False
        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try

            ' check
            Dim rec = From itm In _CMSDBDataContext.EUS_Profiles
            Where itm.ProfileID = profileId AndAlso _
                  (((itm.Status = ProfileStatusEnum.NewProfile OrElse itm.Status = ProfileStatusEnum.Updating) AndAlso itm.IsMaster = False) OrElse _
                   (itm.Status = ProfileStatusEnum.Approved AndAlso itm.IsMaster = True))
            Select itm

            isactive = rec.Count() > 0
        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

        Return isactive
    End Function

    Public Function GetRandomPredefinedMessage(TargetGenderID As Integer, LagID As String) As String
        Dim MessageText As String = ""
        Try
            Dim sqlProc As String = "EXEC [GetRandomPredefinedMessage] @TargetGenderID=" & TargetGenderID & ",@CurrentLag='" & LagID & "'"
            Dim dt As DataTable = DataHelpers.GetDataTable(sqlProc)
            If (dt.Rows.Count > 0) Then
                If (Not dt.Rows(0).IsNull("MessageText")) Then MessageText = dt.Rows(0)("MessageText")
            End If
        Catch ex As Exception
            Throw
        Finally

        End Try
        Return MessageText
    End Function


    Public Function GetAllPredefinedMessages(TargetGenderID As Integer, LagID As String) As DataTable
        Dim MessageText As New DataTable()
        Try
            Dim sqlProc As String = "EXEC [GetAllPredefinedMessages] @TargetGenderID=" & TargetGenderID & ",@CurrentLag='" & LagID & "'"
            MessageText = DataHelpers.GetDataTable(sqlProc)
           
        Catch ex As Exception
            Throw
        Finally

        End Try
        Return MessageText
    End Function



    Public Function GetEUS_Profiles_LAGID(profileid As Integer) As String
        Dim _lagId As String
        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try

            ' check
            _lagId = (From itm In _CMSDBDataContext.EUS_Profiles
                        Where itm.ProfileID = profileid
                        Select itm.LAGID).FirstOrDefault()

        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

        Return _lagId
    End Function

    Public Sub EUS_Profiles_UpdateCelebratingBirth()
        Dim MessageText As String = ""
        Try
            Dim sqlProc As String = <sql><![CDATA[
if( exists(select top(1)[CelebratingBirth]  
			FROM [EUS_Profiles]
			where datepart(year,[CelebratingBirth])<datepart(year,getutcdate())))
begin

	EXEC [EUS_Profiles_UpdateCelebratingBirth]

end
]]></sql>
            DataHelpers.ExecuteNonQuery(sqlProc)
        Catch ex As Exception
            Throw
        Finally
        End Try
    End Sub


    Public Function GetEUS_LISTS_SpokenLanguages_Datatable() As DataTable
        Dim dt As New DataTable()
        Try
            Dim sqlProc As String = <sql><![CDATA[
select SpokenLangId,LangName from dbo.EUS_LISTS_SpokenLanguages order by Sorting ASC
]]></sql>
            dt = DataHelpers.GetDataTable(sqlProc)
        Catch ex As Exception
            Throw
        Finally
        End Try
        Return dt
    End Function


    Public Function GetEUS_LISTS_SpokenLanguages_Datatable(ProfileID As Integer) As DataTable
        Dim dt As New DataTable()
        Try
            Dim sql As String = <sql><![CDATA[
select 
	l.SpokenLangId, 
	l.LangName, 
	psl.Profileid,
	IsSelected=case
		when psl.Profileid>0 then 1
		else 0
	end
from dbo.EUS_LISTS_SpokenLanguages l
left join EUS_ProfileSpokenLang psl on  psl.SpokenLangId=l.SpokenLangId and psl.Profileid=@ProfileID
where isnull(l.IsEnabled,1)=1
order by Sorting ASC
]]></sql>
            Dim cmd As SqlCommand = DataHelpers.GetSqlCommand(sql)
            cmd.Parameters.AddWithValue("@ProfileID", ProfileID)
            dt = DataHelpers.GetDataTable(cmd)
        Catch ex As Exception
            Throw
        Finally
        End Try
        Return dt
    End Function

End Module




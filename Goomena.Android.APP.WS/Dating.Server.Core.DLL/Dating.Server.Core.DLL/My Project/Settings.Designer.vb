﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'     Runtime Version:4.0.30319.18052
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Namespace My
    
    <Global.System.Runtime.CompilerServices.CompilerGeneratedAttribute(),  _
     Global.System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "10.0.0.0"),  _
     Global.System.ComponentModel.EditorBrowsableAttribute(Global.System.ComponentModel.EditorBrowsableState.Advanced)>  _
    Partial Friend NotInheritable Class MySettings
        Inherits Global.System.Configuration.ApplicationSettingsBase
        
        Private Shared defaultInstance As MySettings = CType(Global.System.Configuration.ApplicationSettingsBase.Synchronized(New MySettings()),MySettings)
        
#Region "My.Settings Auto-Save Functionality"
#If _MyType = "WindowsForms" Then
    Private Shared addedHandler As Boolean

    Private Shared addedHandlerLockObject As New Object

    <Global.System.Diagnostics.DebuggerNonUserCodeAttribute(), Global.System.ComponentModel.EditorBrowsableAttribute(Global.System.ComponentModel.EditorBrowsableState.Advanced)> _
    Private Shared Sub AutoSaveSettings(ByVal sender As Global.System.Object, ByVal e As Global.System.EventArgs)
        If My.Application.SaveMySettingsOnExit Then
            My.Settings.Save()
        End If
    End Sub
#End If
#End Region
        
        Public Shared ReadOnly Property [Default]() As MySettings
            Get
                
#If _MyType = "WindowsForms" Then
               If Not addedHandler Then
                    SyncLock addedHandlerLockObject
                        If Not addedHandler Then
                            AddHandler My.Application.Shutdown, AddressOf AutoSaveSettings
                            addedHandler = True
                        End If
                    End SyncLock
                End If
#End If
                Return defaultInstance
            End Get
        End Property
        
        <Global.System.Configuration.ApplicationScopedSettingAttribute(), _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(), _
         Global.System.Configuration.SpecialSettingAttribute(Global.System.Configuration.SpecialSetting.ConnectionString), _
         Global.System.Configuration.DefaultSettingValueAttribute("Data Source=(LOCAL);Initial Catalog=Goomena__Local;Persist Security Info=True;Use" & _
            "r ID=developer1")> _
        Public ReadOnly Property AppDBconnectionString() As String
            Get
                Return CType(Me("AppDBconnectionString"), String)
            End Get
        End Property

        <Global.System.Configuration.ApplicationScopedSettingAttribute(), _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(), _
         Global.System.Configuration.SpecialSettingAttribute(Global.System.Configuration.SpecialSetting.ConnectionString), _
         Global.System.Configuration.DefaultSettingValueAttribute("Data Source=5.135.134.124;Initial Catalog=CMSPutDrive;Persist Security Info=True;" & _
            "User ID=developer1;Password=P0ssibl3Not")> _
        Public ReadOnly Property CMSPutDriveConnectionString() As String
            Get
                Return CType(Me("CMSPutDriveConnectionString"), String)
            End Get
        End Property

        <Global.System.Configuration.ApplicationScopedSettingAttribute(), _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(), _
         Global.System.Configuration.SpecialSettingAttribute(Global.System.Configuration.SpecialSetting.ConnectionString), _
         Global.System.Configuration.DefaultSettingValueAttribute("Data Source=(LOCAL);Initial Catalog=CMSGoomena_Local;Persist Security Info=True;U" & _
            "ser ID=developer1;Password=123456789")> _
        Public ReadOnly Property CMSGoomena_LocalConnectionString() As String
            Get
                Return CType(Me("CMSGoomena_LocalConnectionString"), String)
            End Get
        End Property

        <Global.System.Configuration.ApplicationScopedSettingAttribute(), _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(), _
         Global.System.Configuration.SpecialSettingAttribute(Global.System.Configuration.SpecialSetting.ConnectionString), _
         Global.System.Configuration.DefaultSettingValueAttribute("Data Source=(local);Initial Catalog=Goomena__Local;Persist Security Info=True;Use" & _
            "r ID=developer1")> _
        Public ReadOnly Property Goomena__LocalConnectionString() As String
            Get
                Return CType(Me("Goomena__LocalConnectionString"), String)
            End Get
        End Property

        <Global.System.Configuration.ApplicationScopedSettingAttribute(), _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(), _
         Global.System.Configuration.SpecialSettingAttribute(Global.System.Configuration.SpecialSetting.ConnectionString), _
         Global.System.Configuration.DefaultSettingValueAttribute("Data Source=(local);Initial Catalog=Goomena__Local;Persist Security Info=True;Use" & _
            "r ID=developer1;Password=123456789")> _
        Public ReadOnly Property Goomena__LocalConnectionString1() As String
            Get
                Return CType(Me("Goomena__LocalConnectionString1"), String)
            End Get
        End Property
        
        <Global.System.Configuration.ApplicationScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.SpecialSettingAttribute(Global.System.Configuration.SpecialSetting.ConnectionString),  _
         Global.System.Configuration.DefaultSettingValueAttribute("Data Source=188.165.25.51;Initial Catalog=CMSGoomena;Persist Security Info=True;U"& _ 
            "ser ID=KostasTh")>  _
        Public ReadOnly Property CMSGoomenaConnectionString() As String
            Get
                Return CType(Me("CMSGoomenaConnectionString"),String)
            End Get
        End Property
    End Class
End Namespace

Namespace My
    
    <Global.Microsoft.VisualBasic.HideModuleNameAttribute(),  _
     Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
     Global.System.Runtime.CompilerServices.CompilerGeneratedAttribute()>  _
    Friend Module MySettingsProperty
        
        <Global.System.ComponentModel.Design.HelpKeywordAttribute("My.Settings")>  _
        Friend ReadOnly Property Settings() As Global.Dating.Server.Core.DLL.My.MySettings
            Get
                Return Global.Dating.Server.Core.DLL.My.MySettings.Default
            End Get
        End Property
    End Module
End Namespace

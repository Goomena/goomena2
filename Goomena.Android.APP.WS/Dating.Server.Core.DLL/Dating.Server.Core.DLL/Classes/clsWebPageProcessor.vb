﻿Imports FilesProxy.DownloadSites.Web
Imports System.Net
Imports System.IO
Imports System.Text.RegularExpressions



Public Class clsWebPageProcessor

   
    Public Shared Function GetPageContent(webPageUrl As String, Optional timeOut As Integer = 0) As String
        Dim br As SimpleBrowser = New SimpleBrowser()

        Dim page As WebPageUrl = New WebPageUrl(webPageUrl)
        Dim webresponse As HttpWebResponse = Nothing
        If (timeOut > 0) Then br.Timeout = timeOut
        webresponse = br.OpenResponse(page)
        Dim sr As StreamReader = New StreamReader(webresponse.GetResponseStream())
        Dim str As String
        str = sr.ReadToEnd()

        Return str
    End Function


    Public Shared Function GetPageContentByPost(webPageUrl As String, Parameters As System.Collections.Generic.Dictionary(Of String, String)) As String
        Dim br As SimpleBrowser = New SimpleBrowser()

        'Dim Inputs As System.Collections.Specialized.NameValueCollection = ReadParametersFromURL(webPageUrl)
        'Dim newUrl As String = webPageUrl
        'If (newUrl.IndexOf("?") > -1) Then newUrl = newUrl.Remove(newUrl.IndexOf("?"))

        Dim page As WebPageUrl = New WebPageUrl(webPageUrl)
        page.UsePost = True
        For Each itm In Parameters.Keys
            page.Parameters.Add(itm, Parameters(itm))
        Next

        Dim webresponse As HttpWebResponse = Nothing
        webresponse = br.OpenResponse(page)
        Dim sr As StreamReader = New StreamReader(webresponse.GetResponseStream())
        Dim str As String
        str = sr.ReadToEnd()

        Return str
    End Function

    Public Shared Function ReadParametersFromURL(url As String) As System.Collections.Specialized.NameValueCollection
        Dim Inputs As System.Collections.Specialized.NameValueCollection = New System.Collections.Specialized.NameValueCollection
        Dim matches As System.Text.RegularExpressions.MatchCollection = System.Text.RegularExpressions.Regex.Matches(url, "[\?&](?<name>[^&=]+)=(?<value>[^&=]+)")

        For Each m As System.Text.RegularExpressions.Match In matches
            Dim name As String = m.Groups(1).Value
            Dim value As String = m.Groups(2).Value
            value = System.Web.HttpUtility.UrlDecode(value)
            Inputs.Add(name, value)
        Next
        Return Inputs
    End Function


    Public Shared Function GetWebResponse(webPageUrl As String, Optional timeOut As Integer = 0) As HttpWebResponse
        Dim br As SimpleBrowser = New SimpleBrowser()

        Dim page As WebPageUrl = New WebPageUrl(webPageUrl)
        If (timeOut > 0) Then br.Timeout = timeOut
        Dim webresponse As HttpWebResponse = Nothing
        webresponse = br.OpenResponse(page)

        Return webresponse
    End Function



    Public Shared Function GetWebResponseHead(webPageUrl As String) As HttpWebResponse
        Dim request As HttpWebRequest = CType(WebRequest.Create(webPageUrl), HttpWebRequest)
        request.Method = WebRequestMethods.Http.Head
        Dim response As HttpWebResponse = request.GetResponse()
        Return response
    End Function




End Class

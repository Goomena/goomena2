﻿

Public Class ForbiddenWordsChecker

    Private Shared __inst_count As Integer

    Private Shared Property gDataList As Dictionary(Of String, Integer)

    Private Shared Function Get_NotAllowedWordsInMessage() As Dictionary(Of String, Integer)
        If (gDataList Is Nothing) Then
            Dim DTSYS_ As dsSecurity.SYS_NotAllowedWordsInMessageDataTable
            Dim ds As New dsSecurity()
            Dim ta As New dsSecurityTableAdapters.SYS_NotAllowedWordsInMessageTableAdapter
            ta.Connection = New SqlClient.SqlConnection(DataHelpers.ConnectionString)
            DTSYS_ = ta.GetData()

            gDataList = New Dictionary(Of String, Integer)
            For cnt = 0 To DTSYS_.Rows.Count - 1
                Dim dr As dsSecurity.SYS_NotAllowedWordsInMessageRow = DTSYS_.Rows(cnt)
                Try
                    gDataList.Add(dr.Word, dr.WarningIndex)
                Catch ex As Exception
                End Try
            Next

        End If
        Return gDataList
    End Function




    Public Function CheckText(text As String) As Integer
        __inst_count += 1
        Dim warnLvl As Integer = 0
        Try

            Dim _dict As Dictionary(Of String, Integer) = Get_NotAllowedWordsInMessage()
            For cnt = 0 To _dict.Keys.Count - 1
                Dim key As String = _dict.Keys(cnt)
                If (text.IndexOf(key, System.StringComparison.OrdinalIgnoreCase) > -1) Then
                    warnLvl += _dict(key)
                End If
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        Finally
            __inst_count -= 1
        End Try
        Return warnLvl
    End Function


    Public Shared Sub ClearCache()
        Try
            If (gDataList IsNot Nothing) Then
                gDataList.Clear()
                gDataList = Nothing
            End If
        Catch ex As Exception

        End Try
    End Sub

End Class


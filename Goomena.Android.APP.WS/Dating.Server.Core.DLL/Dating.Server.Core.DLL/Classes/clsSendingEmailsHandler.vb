﻿Public Class clsSendingEmailsHandler
    Implements IDisposable

    'Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


    Public Sub AddEmail(emailTo As String, emailSubject As String,
                        emailBody As String, customerid As Integer?,
                        IsHtmlBody As Boolean,
                        EmailAction As String,
                        SMTPServerCredentialsID As Integer?,
                        BCCAddresses As String)
        Dim cntx As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try
            Dim email As New Core.DLL.SYS_EmailsQueue


            email.DateCreated = Date.UtcNow
            email.Body = emailBody
            email.EmailTo = emailTo
            email.Subject = emailSubject
            email.CustomerId = customerid
            email.EmailAction = EmailAction
            email.IsHtmlBody = IsHtmlBody
            email.SMTPServerCredentialsID = SMTPServerCredentialsID
            email.BCCAddresses = BCCAddresses

            cntx.SYS_EmailsQueues.InsertOnSubmit(email)
            cntx.SubmitChanges()

        Catch ex As Exception
            Throw
        Finally
            cntx.Dispose()
        End Try
    End Sub


    Public Function CheckEmailIfExists(emailTo As String, emailSubject As String, customerid As Integer?, Optional EmailAction As String = Nothing) As Boolean
        Dim cntx As New CMSDBDataContext(DataHelpers.ConnectionString)
        Dim result As Boolean
        Try
            Dim email As Core.DLL.SYS_EmailsQueue = (From itm In cntx.SYS_EmailsQueues
                    Where itm.EmailTo = emailTo AndAlso itm.Subject = emailSubject AndAlso itm.CustomerId = customerid
                   Select itm).FirstOrDefault()
            result = (email IsNot Nothing)

        Catch ex As Exception
            Throw
        Finally
            cntx.Dispose()
        End Try
        Return result
    End Function


    Public Function GetNextEmail() As Core.DLL.SYS_EmailsQueue
        Dim cntx As New CMSDBDataContext(DataHelpers.ConnectionString)
        Dim file As Core.DLL.SYS_EmailsQueue
        Try
            file = (From itm In cntx.SYS_EmailsQueues
                    Where itm.DateSent Is Nothing
                   Select itm).FirstOrDefault()

            cntx.SubmitChanges()

        Catch ex As Exception
            Throw
        Finally
            cntx.Dispose()
        End Try

        Return file
    End Function


    Public Function GetNextEmailToSend() As Core.DLL.SYS_EmailsQueue
        Dim cntx As New CMSDBDataContext(DataHelpers.ConnectionString)
        Dim file As Core.DLL.SYS_EmailsQueue
        Try
            file = (From itm In cntx.SYS_EmailsQueues
                    Where itm.DateSent Is Nothing AndAlso (itm.Exception Is Nothing OrElse itm.Exception = "")
                   Select itm).FirstOrDefault()

            '_CMSDBDataContext.SubmitChanges()

        Catch ex As Exception
            Throw
        Finally
            cntx.Dispose()
        End Try

        Return file
    End Function


    Public Sub SetEmailSent(email1 As Core.DLL.SYS_EmailsQueue)
        Dim cntx As New CMSDBDataContext(DataHelpers.ConnectionString)

        Try
            Dim email As Core.DLL.SYS_EmailsQueue = (From itm In cntx.SYS_EmailsQueues
                   Where itm.MessageQueueId = email1.MessageQueueId
                   Select itm).FirstOrDefault()


            email.DateSent = email1.DateSent

            If (Len(email1.Exception) > 1023) Then email1.Exception = email1.Exception.Remove(1023)
            email.Exception = email1.Exception
            If (email.Subject <> email1.Subject) Then email.Subject = email1.Subject
            cntx.SubmitChanges()



            If (email.Body <> email1.Body) Then
                email.Body = email1.Body
                cntx.SubmitChanges()
            End If

            'Catch ex As Exception
            'End Try

        Catch ex As Exception
            Throw
        Finally
            cntx.Dispose()
        End Try

    End Sub


    Public Sub DeleteSentEmails()
        Dim cntx As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try
            Dim email = From itm In cntx.SYS_EmailsQueues
                   Where itm.DateSent IsNot Nothing
                   Select itm


            cntx.SYS_EmailsQueues.DeleteAllOnSubmit(email)
            cntx.SubmitChanges()
        Catch ex As Exception
            Throw
        Finally
            cntx.Dispose()
        End Try

    End Sub


    Public Shared Sub SetRetryingPhotosEmail_To_Support()
        Try
            Dim sql As String = <q><![CDATA[
update SYS_EmailsQueue
set exception=null
where Subject='New photos uploaded on Goomena.com'
and exception='asyncProcessingStarted'
and datecreated >= @datecreated
]]></q>

            Dim command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql)
            command.Parameters.AddWithValue("@datecreated", Date.UtcNow.AddDays(-1))
            DataHelpers.ExecuteNonQuery(command)

        Catch ex As Exception
            Throw
        Finally
        End Try

    End Sub

    Public Sub Dispose() Implements IDisposable.Dispose
        '    If (_CMSDBDataContext IsNot Nothing) Then _CMSDBDataContext.Dispose()
    End Sub

End Class

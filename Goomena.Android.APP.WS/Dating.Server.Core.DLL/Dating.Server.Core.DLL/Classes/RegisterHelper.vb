﻿Imports Dating.Server.Core.DLL
Imports Dating.Server.Core.DLL.DSMembers
Imports System.Data.SqlClient

Public Class RegisterHelper
    Public Property ErrorsList As New List(Of String)

    Public Property cbAgreements As Boolean
    Public Property GenderId As Integer?
    Public Property Email As String
    Public Property Login As String
    Public Property Password As String
    Public Property Birthday As DateTime?
    Public Property Country As String
    Public Property Region As String
    Public Property City As String
    Public Property Zip As String
    Public Property Lag As String

    Public Property Height As String
    Public Property BodyType As String
    Public Property EyeColor As String
    Public Property HairClr As String

    Public Property GEO_COUNTRY_CODE As String
    Public Property REMOTE_ADDR As String
    Public Property HTTP_USER_AGENT As String
    Public Property HTTP_REFERER As String
    Public Property CustomReferrer As String
    Public Property LandingPage As String
    Public Property SearchEngineKeywords As String





    Public Sub CreateNewProfile(ByRef profile As EUS_ProfilesRow)

        ErrorsList.Clear()

    
        If (profile.IsGenderIdNull()) Then
            profile.GenderId = GenderId
        End If
        If (profile.IseMailNull() OrElse String.IsNullOrEmpty(profile.eMail)) Then
            profile.eMail = Email
        End If
        If (profile.IsPasswordNull() OrElse String.IsNullOrEmpty(profile.Password)) Then
            profile.Password = Password
        End If

        profile.LookingFor_ToMeetFemaleID = ProfileHelper.IsMale(profile.GenderId)
        profile.LookingFor_ToMeetMaleID = ProfileHelper.IsFemale(profile.GenderId)

        If (profile.IsBirthdayNull()) Then
            profile.Birthday = Birthday
        End If

        If (profile.IsCountryNull() OrElse String.IsNullOrEmpty(profile.Country)) Then
            profile.Country = GEO_COUNTRY_CODE
        End If

        'If (profile.City = "Attiki") Then profile.City = "Attica"


        '' Location
        profile.Country = Country
        If (Region IsNot Nothing) Then
            profile._Region = Region
        Else
            profile._Region = ""
        End If

        If (City IsNot Nothing) Then
            profile.City = City
        Else
            profile.City = ""
        End If


        Zip = AppUtils.GetZip(Zip)
        If (Zip <> "") Then
            profile.Zip = Zip
        Else
            profile.Zip = clsGeoHelper.GetCityCenterPostcode(profile.Country, profile._Region, profile.City)
        End If


        If (Not profile.IsZipNull()) Then
            ' set textbox zip value
            Zip = profile.Zip
        End If


        'If (profile.GenderId = ProfileHelper.gFemaleGender.GenderId) Then
        '    profile.AccountTypeId = ProfileHelper.Attractive_AccountTypeId
        'ElseIf (profile.GenderId = ProfileHelper.gMaleGender.GenderId) Then
        '    profile.AccountTypeId = ProfileHelper.Generous_AccountTypeId
        'End If

        profile.LoginName = Login
        profile.OtherDetails_EducationID = Lists.gDSLists.EUS_LISTS_Education.Single(Function(itm As DSLists.EUS_LISTS_EducationRow) itm.US = "Bachelors Degree").EducationId

        profile.OtherDetails_AnnualIncomeID = 26 '&euro;50.001 - &euro;60.000 
        ' Lists.gDSLists.EUS_LISTS_Income.Single(Function(itm As DSLists.EUS_LISTS_IncomeRow) itm.US = "Prefer Not To Say").IncomeId

        'profile.OtherDetails_NetWorthID = Lists.gDSLists.EUS_LISTS_NetWorth.Single(Function(itm As DSLists.EUS_LISTS_NetWorthRow) itm.US = "Prefer Not To Say").NetWorthId

        'If (Not profile.IsGenderIdNull()) Then
        '    If (ProfileHelper.IsFemale(profile.GenderId)) Then
        '        profile.PersonalInfo_HeightID = Lists.gDSLists.EUS_LISTS_Height.Single(Function(itm As DSLists.EUS_LISTS_HeightRow) itm.US = "170cm").HeightId
        '    Else
        '        profile.PersonalInfo_HeightID = Lists.gDSLists.EUS_LISTS_Height.Single(Function(itm As DSLists.EUS_LISTS_HeightRow) itm.US = "180cm").HeightId
        '    End If
        'End If

        'profile.PersonalInfo_BodyTypeID = Lists.gDSLists.EUS_LISTS_BodyType.Single(Function(itm As DSLists.EUS_LISTS_BodyTypeRow) itm.US = "Average").BodyTypeId
        'profile.PersonalInfo_EyeColorID = Lists.gDSLists.EUS_LISTS_EyeColor.Single(Function(itm As DSLists.EUS_LISTS_EyeColorRow) itm.US = "Black").EyeColorId
        'profile.PersonalInfo_HairColorID = Lists.gDSLists.EUS_LISTS_HairColor.Single(Function(itm As DSLists.EUS_LISTS_HairColorRow) itm.US = "Black").HairColorId

        If (Height < 1) Then
            profile.PersonalInfo_HeightID = -1
        Else
            profile.PersonalInfo_HeightID = Height
        End If

        If (BodyType < 1) Then
            profile.PersonalInfo_BodyTypeID = -1
        Else
            profile.PersonalInfo_BodyTypeID = BodyType
        End If

        If (EyeColor < 1) Then
            profile.PersonalInfo_EyeColorID = -1
        Else
            profile.PersonalInfo_EyeColorID = EyeColor
        End If

        If (HairClr < 1) Then
            profile.PersonalInfo_HairColorID = -1
        Else
            profile.PersonalInfo_HairColorID = HairClr
        End If

        'If (cbRelationshipStatus.Items(0).Selected) Then
        profile.LookingFor_RelationshipStatusID = -1
        'Else
        '    profile.LookingFor_RelationshipStatusID = cbRelationshipStatus.SelectedItem.Value
        'End If

        profile.LookingFor_TypeOfDating_AdultDating_Casual = True
        profile.LookingFor_TypeOfDating_Friendship = True
        profile.LookingFor_TypeOfDating_LongTermRelationship = True
        profile.LookingFor_TypeOfDating_MarriedDating = True
        profile.LookingFor_TypeOfDating_MutuallyBeneficialArrangements = True
        profile.LookingFor_TypeOfDating_ShortTermRelationship = True

        'Try
        '    For Each itm As ListViewDataItem In lvTypeOfDate.Items

        '        Try

        '            Dim hdf As HiddenField = DirectCast(itm.FindControl("hdf"), HiddenField)
        '            Dim chk As DevExpress.Web.ASPxEditors.ASPxCheckBox = DirectCast(itm.FindControl("chk"), DevExpress.Web.ASPxEditors.ASPxCheckBox)

        '            Dim tmpTypeOfDating As TypeOfDatingEnum = DirectCast(System.Enum.Parse(GetType(TypeOfDatingEnum), hdf.Value), TypeOfDatingEnum)
        '            Select Case tmpTypeOfDating
        '                Case TypeOfDatingEnum.AdultDating_Casual
        '                    profile.LookingFor_TypeOfDating_AdultDating_Casual = chk.Checked

        '                Case TypeOfDatingEnum.Friendship
        '                    profile.LookingFor_TypeOfDating_Friendship = chk.Checked

        '                Case TypeOfDatingEnum.LongTermRelationship
        '                    profile.LookingFor_TypeOfDating_LongTermRelationship = chk.Checked

        '                Case TypeOfDatingEnum.MarriedDating
        '                    profile.LookingFor_TypeOfDating_MarriedDating = chk.Checked

        '                Case TypeOfDatingEnum.MutuallyBeneficialArrangements
        '                    profile.LookingFor_TypeOfDating_MutuallyBeneficialArrangements = chk.Checked

        '                Case TypeOfDatingEnum.ShortTermRelationship
        '                    profile.LookingFor_TypeOfDating_ShortTermRelationship = chk.Checked

        '            End Select
        '        Catch ex As Exception
        '            WebErrorMessageBox(Me, ex, "")
        '        End Try
        '    Next

        'Catch ex As Exception
        '    WebErrorMessageBox(Me, ex, "")
        'End Try


        profile.PersonalInfo_ChildrenID = Lists.gDSLists.EUS_LISTS_ChildrenNumber.Single(Function(itm As DSLists.EUS_LISTS_ChildrenNumberRow) itm.US = "No Children").ChildrenNumberId
        profile.PersonalInfo_EthnicityID = Lists.gDSLists.EUS_LISTS_Ethnicity.Single(Function(itm As DSLists.EUS_LISTS_EthnicityRow) itm.US = "White / Caucasian").EthnicityId

        'Agnostic / Non-religious ---	Χωρίς Θρησκεία
        profile.PersonalInfo_ReligionID = 2
        'Lists.gDSLists.EUS_LISTS_Religion.Single(Function(itm As DSLists.EUS_LISTS_ReligionRow) itm.US = "Agnostic / Non-religious").ReligionId

        profile.PersonalInfo_SmokingHabitID = Lists.gDSLists.EUS_LISTS_Smoking.Single(Function(itm As DSLists.EUS_LISTS_SmokingRow) itm.US = "Non-Smoker").SmokingId
        profile.PersonalInfo_DrinkingHabitID = Lists.gDSLists.EUS_LISTS_Drinking.Single(Function(itm As DSLists.EUS_LISTS_DrinkingRow) itm.US = "Non-Drinker").DrinkingId
        profile.LookingFor_RelationshipStatusID = Lists.gDSLists.EUS_LISTS_RelationshipStatus.Single(Function(itm As DSLists.EUS_LISTS_RelationshipStatusRow) itm.US = "Single").RelationshipStatusId


        ' type of dating
        'profile.LookingFor_TypeOfDating_ShortTermRelationship = True
        ' profile.LookingFor_TypeOfDating_MutuallyBeneficialArrangements = True


        ' set notification settings
        profile.NotificationsSettings_WhenLikeReceived = True
        profile.NotificationsSettings_WhenNewMembersNearMe = False
        profile.NotificationsSettings_WhenNewMessageReceived = True
        profile.NotificationsSettings_WhenNewOfferReceived = True


        ' set implicit data
        profile.Role = "MEMBER"
        profile.Status = Core.DLL.ProfileStatusEnum.NewProfile
        profile.IsMaster = False
        profile.MirrorProfileID = 0
        profile.LAGID = Lag

        profile.DateTimeToRegister = DateTime.UtcNow
        profile.RegisterIP = REMOTE_ADDR
        profile.RegisterGEOInfos = GEO_COUNTRY_CODE


        profile.LastLoginDateTime = DateTime.UtcNow
        profile.LastLoginIP = REMOTE_ADDR
        profile.LastLoginGEOInfos = ""

        profile.LastUpdateProfileDateTime = DateTime.UtcNow
        profile.LastUpdateProfileIP = REMOTE_ADDR
        profile.LastUpdateProfileGEOInfo = ""


        profile.CustomReferrer = CustomReferrer


        If (Not String.IsNullOrEmpty(LandingPage)) Then
            profile.LandingPage = LandingPage
            If (Not String.IsNullOrEmpty(profile.LandingPage)) Then
                If (profile.LandingPage.Length > 1023) Then
                    profile.LandingPage = profile.LandingPage.Remove(1023)
                End If
            End If
        End If


        profile.RegisterUserAgent = HTTP_USER_AGENT
        If (Not String.IsNullOrEmpty(profile.RegisterUserAgent)) Then
            If (profile.RegisterUserAgent.Length > 1023) Then
                profile.RegisterUserAgent = profile.RegisterUserAgent.Remove(1023)
            End If
        End If

        profile.Referrer = IIf(HTTP_REFERER IsNot Nothing, HTTP_REFERER, "")
        If (Not String.IsNullOrEmpty(profile.Referrer)) Then
            If (profile.Referrer.Length > 1023) Then
                profile.Referrer = profile.Referrer.Remove(1023)
            End If
        End If

        profile.SearchKeywords = IIf(SearchEngineKeywords IsNot Nothing, SearchEngineKeywords, "")
        If (Not String.IsNullOrEmpty(profile.SearchKeywords)) Then
            If (profile.SearchKeywords.Length > 249) Then
                profile.SearchKeywords = profile.SearchKeywords.Remove(249)
            End If
        End If

        'profile.ShowOnFrontPage = False
        'profile.ShowOnMosaic = False

        Dim countryCodeTmp As String = profile.Country
        Dim PostcodeTmp As String = profile.Zip
        Dim LAGIDTmp As String = profile.LAGID

        Try

            Dim dt As DataTable = clsGeoHelper.GetGEOByZip(countryCodeTmp, PostcodeTmp, LAGIDTmp)

            If (dt.Rows.Count > 0) Then

                profile.latitude = dt.Rows(0)("latitude")
                profile.longitude = dt.Rows(0)("longitude")

            Else

                dt = clsGeoHelper.GetCountryMinPostcodeDataTable(profile.Country, profile._Region, profile.City, profile.LAGID)

                If (dt.Rows.Count > 0) Then
                    If (Not IsDBNull(dt.Rows(0)("MinPostcode"))) Then PostcodeTmp = dt.Rows(0)("MinPostcode")
                    If (Not IsDBNull(dt.Rows(0)("countryCode"))) Then countryCodeTmp = dt.Rows(0)("countryCode")

                    dt = clsGeoHelper.GetGEOByZip(countryCodeTmp, PostcodeTmp, profile.LAGID)

                    profile.latitude = dt.Rows(0)("latitude")
                    profile.longitude = dt.Rows(0)("longitude")
                End If

            End If

        Catch ex As Exception
            ErrorsList.Add("Registering Profile: Failed to retrieve GEO info for specified zip, params (zip:" & profile.Zip & "). Excp message: " & ex.Message)
        End Try


        '    ds.EUS_Profiles.AddEUS_ProfilesRow(profile)
        '    adapter.Update(ds)

        '    Try
        '        If (Me.FBUser IsNot Nothing) Then
        '            DataHelpers.UpdateEUS_Profiles_FacebookData(profile.ProfileID, Me.FBUser.uid, Me.FBUser.name, Me.FBUser.username)
        '        End If
        '    Catch ex As Exception
        '        WebErrorMessageBox(Me, ex, "")
        '    End Try

        '    PerformLogin(ds, profile, False)
        'Catch ex As Exception
        '    WebErrorMessageBox(Me, ex, "")
        'End Try

    End Sub



#Region "Check"

    'Public Function ValidateInput() As Boolean

    '    Dim isValid As Boolean = True

    '    ' perform checks, in case user has bypass validation on client
    '    'serverValidationList.Items.Clear()

    '    If (Not cbAgreements) Then
    '        ErrorsList.Add("cbAgreements_Required_ErrorText")
    '        'Dim txt As String = CurrentPageData.GetCustomString("cbAgreements_Required_ErrorText")
    '        'txt = StripTags(txt)
    '        'Dim itm As ListItem = New ListItem(txt, "javascript:" & cbAgreements.ClientInstanceName & ".SetFocus();")
    '        'itm.Attributes.Add("class", "BulletedListItem")
    '        'serverValidationList.Items.Add(itm)
    '        'cbAgreements.IsValid = False
    '        isValid = False
    '    End If

    '    If (GenderId Is Nothing OrElse
    '        GenderId <> ProfileHelper.gMaleGender.GenderId.ToString() AndAlso
    '        GenderId <> ProfileHelper.gFemaleGender.GenderId.ToString()) Then
    '        ErrorsList.Add("rblGender_Required_ErrorText")
    '        'Dim txt As String = CurrentPageData.GetCustomString("rblGender_Required_ErrorText")

    '        'txt = StripTags(txt)
    '        'Dim itm As ListItem = New ListItem(txt, "javascript:" & rblGender.ClientInstanceName & ".SetFocus();")
    '        'itm.Attributes.Add("class", "BulletedListItem")
    '        'serverValidationList.Items.Add(itm)
    '        'rblGender.IsValid = False
    '        isValid = False

    '    End If


    '    Email = Email.Trim()
    '    If (Email.Length = 0) Then
    '        ErrorsList.Add("txtEmail_Required_ErrorText")
    '        'Dim txt As String = CurrentPageData.GetCustomString("txtEmail_Required_ErrorText")
    '        'txt = StripTags(txt)
    '        'Dim itm As ListItem = New ListItem(txt, "javascript:" & txtEmail.ClientInstanceName & ".SetFocus();")
    '        'itm.Attributes.Add("class", "BulletedListItem")
    '        'serverValidationList.Items.Add(itm)
    '        'txtEmail.IsValid = False
    '        isValid = False
    '    End If

    '    Login = Login.Trim()
    '    If (Login.Trim().Length = 0) Then
    '        ErrorsList.Add("txtLogin_Required_ErrorText")
    '        'Dim txt As String = CurrentPageData.GetCustomString("txtLogin_Required_ErrorText")
    '        'txt = StripTags(txt)
    '        'Dim itm As ListItem = New ListItem(txt, "javascript:" & txtLogin.ClientInstanceName & ".SetFocus();")
    '        'itm.Attributes.Add("class", "BulletedListItem")
    '        'serverValidationList.Items.Add(itm)
    '        'txtLogin.IsValid = False
    '        isValid = False
    '    End If

    '    If (Password.Trim().Length = 0) Then
    '        ErrorsList.Add("txtPasswrd_Required_ErrorText")
    '        'Dim txt As String = CurrentPageData.GetCustomString("txtPasswrd_Required_ErrorText")
    '        'txt = StripTags(txt)
    '        'Dim itm As ListItem = New ListItem(txt, "javascript:" & txtPasswrd.ClientInstanceName & ".SetFocus();")
    '        'itm.Attributes.Add("class", "BulletedListItem")
    '        'serverValidationList.Items.Add(itm)
    '        'txtPasswrd.IsValid = False
    '        isValid = False
    '    End If

    '    Try

    '        If (Birthday Is Nothing) Then
    '            ErrorsList.Add("ctlBirthday_Required_ErrorText")
    '            'Dim txt As String = CurrentPageData.GetCustomString("ctlBirthday_Required_ErrorText")
    '            'txt = StripTags(txt)
    '            'Dim itm As ListItem = New ListItem(txt, "javascript:void(0);")
    '            'itm.Attributes.Add("class", "BulletedListItem")
    '            'serverValidationList.Items.Add(itm)
    '            isValid = False
    '        Else
    '            Dim age = ProfileHelper.GetCurrentAge(Birthday)
    '            If (age < 18) Then
    '                ErrorsList.Add("ctlBirthday_AgeNotAllowed_ErrorText")
    '                'Dim txt As String = CurrentPageData.GetCustomString("ctlBirthday_AgeNotAllowed_ErrorText")
    '                'txt = StripTags(txt)
    '                'Dim itm As ListItem = New ListItem(txt, "javascript:void(0);")
    '                'itm.Attributes.Add("class", "BulletedListItem")
    '                'serverValidationList.Items.Add(itm)
    '                isValid = False
    '            End If
    '        End If

    '    Catch ex As Exception
    '        'WebErrorMessageBox(Me, ex, "")
    '        ErrorsList.Add("ctlBirthday_InvalidDate_ErrorText")

    '        'Dim txt As String = CurrentPageData.GetCustomString("ctlBirthday_InvalidDate_ErrorText")
    '        'txt = StripTags(txt)
    '        'Dim itm As ListItem = New ListItem(txt, "javascript:void(0);")
    '        'itm.Attributes.Add("class", "BulletedListItem")
    '        'serverValidationList.Items.Add(itm)
    '        isValid = False
    '    End Try


    '    If (Country IsNot Nothing AndAlso Country = "-1") Then
    '        ErrorsList.Add("lblCountryErr")

    '        'cbCountry.Focus()
    '        'Dim txt As String = CurrentPageData.GetCustomString("lblCountryErr")
    '        'txt = StripTags(txt)
    '        'Dim itm As ListItem = New ListItem(txt, "javascript:" & cbCountry.ClientInstanceName & ".SetFocus();")
    '        'itm.Attributes.Add("class", "BulletedListItem")
    '        'serverValidationList.Items.Add(itm)
    '        isValid = False

    '    ElseIf (Country IsNot Nothing AndAlso
    '            Country <> "-1" AndAlso
    '            clsGeoHelper.CheckCountryTable(Country)) Then

    '        If (isValid) Then
    '            Zip = AppUtils.GetZip(Zip)
    '            If (Zip.Trim() <> "") Then 'AndAlso hdfLocationStatus.Value = "zip"
    '                'Try

    '                '    Dim dt As DataTable = clsGeoHelper.GetGEOByZip(Country, Zip, Lag)
    '                '    If (dt.Rows.Count > 0) Then
    '                '        If (Not SelectComboItem(cbRegion, dt.Rows(0)("region1"))) Then
    '                '            cbRegion.Items.Add(dt.Rows(0)("region1"))
    '                '            cbRegion.Items(cbRegion.Items.Count - 1).Selected = True
    '                '        End If
    '                '        If (Not SelectComboItem(cbCity, dt.Rows(0)("city"))) Then
    '                '            cbCity.Items.Add(dt.Rows(0)("city"))
    '                '            cbCity.Items(cbCity.Items.Count - 1).Selected = True
    '                '        End If
    '                '    Else
    '                '        'txtZip.Focus()

    '                '        Dim txt As String = CurrentPageData.GetCustomString("lblZipErr")
    '                '        txt = StripTags(txt)
    '                '        Dim itm As ListItem = New ListItem(txt, "javascript:" & txtZip.ClientInstanceName & ".SetFocus();")
    '                '        itm.Attributes.Add("class", "BulletedListItem")
    '                '        serverValidationList.Items.Add(itm)
    '                '        isValid = False
    '                '    End If

    '                'Catch ex As Exception
    '                '    WebErrorMessageBox(Me, ex, "")

    '                '    txtZip.Focus()
    '                '    Dim txt As String = CurrentPageData.GetCustomString("lblZipErr")
    '                '    txt = StripTags(txt)
    '                '    Dim itm As ListItem = New ListItem(txt, "javascript:" & txtZip.ClientInstanceName & ".SetFocus();")
    '                '    itm.Attributes.Add("class", "BulletedListItem")
    '                '    serverValidationList.Items.Add(itm)
    '                '    isValid = False
    '                'Finally
    '                'End Try

    '            Else
    '                ErrorsList.Add("lblZipErr")
    '                'txtZip.Focus()
    '                'Dim txt As String = CurrentPageData.GetCustomString("lblZipErr")
    '                'txt = StripTags(txt)
    '                'Dim itm As ListItem = New ListItem(txt, "javascript:" & txtZip.ClientInstanceName & ".SetFocus();")
    '                'itm.Attributes.Add("class", "BulletedListItem")
    '                'serverValidationList.Items.Add(itm)
    '                isValid = False
    '            End If
    '        End If


    '        If (isValid) Then
    '            If (Region Is Nothing OrElse Region = "-1") Then
    '                ErrorsList.Add("lblRegionErr")
    '                'cbRegion.Focus()
    '                'Dim txt As String = CurrentPageData.GetCustomString("lblRegionErr")
    '                'txt = StripTags(txt)
    '                'Dim itm As ListItem = New ListItem(txt, "javascript:" & cbRegion.ClientInstanceName & ".SetFocus();")
    '                'itm.Attributes.Add("class", "BulletedListItem")
    '                'serverValidationList.Items.Add(itm)
    '                isValid = False
    '            End If
    '        End If


    '        If (isValid) Then
    '            If (City Is Nothing OrElse City = "-1") Then
    '                ErrorsList.Add("lblCityErr")
    '                'cbCity.Focus()
    '                'Dim txt As String = CurrentPageData.GetCustomString("lblCityErr")
    '                'txt = StripTags(txt)
    '                'Dim itm As ListItem = New ListItem(txt, "javascript:" & cbCity.ClientInstanceName & ".SetFocus();")
    '                'itm.Attributes.Add("class", "BulletedListItem")
    '                'serverValidationList.Items.Add(itm)
    '                isValid = False
    '            End If
    '        End If
    '    End If

    '    If (isValid) Then
    '        ' personal information
    '        If (isValid AndAlso (Height Is Nothing OrElse Height = "-1" OrElse Height = "0")) Then
    '            ErrorsList.Add("msg_ProvidePersonalInfo")
    '            'cbHeight.Focus()
    '            ''Dim txt As String = CurrentPageData.GetCustomString("cbHeightErr")
    '            'Dim txt As String = CurrentPageData.GetCustomString("msg_ProvidePersonalInfo")
    '            'txt = StripTags(txt)
    '            'Dim itm As ListItem = New ListItem(txt, "javascript:" & cbHeight.ClientInstanceName & ".SetFocus();")
    '            'itm.Attributes.Add("class", "BulletedListItem")
    '            'serverValidationList.Items.Add(itm)
    '            isValid = False
    '        End If


    '        If (isValid AndAlso (BodyType Is Nothing OrElse BodyType = "-1" OrElse BodyType = "0")) Then
    '            ErrorsList.Add("msg_ProvidePersonalInfo")
    '            'cbBodyType.Focus()
    '            '' Dim txt As String = CurrentPageData.GetCustomString("cbBodyTypeErr")
    '            'Dim txt As String = CurrentPageData.GetCustomString("msg_ProvidePersonalInfo")
    '            'txt = StripTags(txt)
    '            'Dim itm As ListItem = New ListItem(txt, "javascript:" & cbBodyType.ClientInstanceName & ".SetFocus();")
    '            'itm.Attributes.Add("class", "BulletedListItem")
    '            'serverValidationList.Items.Add(itm)
    '            isValid = False
    '        End If

    '        If (isValid AndAlso (EyeColor Is Nothing OrElse EyeColor = "-1" OrElse EyeColor = "0")) Then
    '            ErrorsList.Add("msg_ProvidePersonalInfo")
    '            'cbEyeColor.Focus()
    '            ''Dim txt As String = CurrentPageData.GetCustomString("cbEyeColorErr")
    '            'Dim txt As String = CurrentPageData.GetCustomString("msg_ProvidePersonalInfo")
    '            'txt = StripTags(txt)
    '            'Dim itm As ListItem = New ListItem(txt, "javascript:" & cbEyeColor.ClientInstanceName & ".SetFocus();")
    '            'itm.Attributes.Add("class", "BulletedListItem")
    '            'serverValidationList.Items.Add(itm)
    '            isValid = False
    '        End If


    '        If (isValid AndAlso (HairClr Is Nothing OrElse HairClr = "-1" OrElse HairClr = "0")) Then
    '            ErrorsList.Add("msg_ProvidePersonalInfo")
    '            'cbHairClr.Focus()
    '            ''Dim txt As String = CurrentPageData.GetCustomString("cbHairClrErr")
    '            'Dim txt As String = CurrentPageData.GetCustomString("msg_ProvidePersonalInfo")
    '            'txt = StripTags(txt)
    '            'Dim itm As ListItem = New ListItem(txt, "javascript:" & cbHairClr.ClientInstanceName & ".SetFocus();")
    '            'itm.Attributes.Add("class", "BulletedListItem")
    '            'serverValidationList.Items.Add(itm)
    '            isValid = False
    '        End If

    '    End If


    '    ' check user name and email availability
    '    If (isValid) Then
    '        'Dim profile As EUS_Profile

    '        '' check email
    '        'profile = (From itm In Me.CMSDBDataContext.EUS_Profiles
    '        '          Where itm.eMail.ToUpper() = txtEmail.Text.ToUpper() AndAlso _
    '        '              itm.Status <> ProfileStatusEnum.DeletedByUser
    '        '          Select itm).FirstOrDefault()

    '        'Me.CMSDBDataContext.EUS_Profiles.FirstOrDefault(Function(itm As EUS_Profile) itm.eMail.ToUpper() = txtEmail.Text.ToUpper())
    '        If (DataHelpers.EUS_Profile_CheckEmail(Email)) Then
    '            ErrorsList.Add("txtEmail_EmailAlreadyInUse_ErrorText")
    '            'Dim txt As String = CurrentPageData.GetCustomString("txtEmail_EmailAlreadyInUse_ErrorText")
    '            'txt = StripTags(txt)
    '            'Dim itm As ListItem = New ListItem(txt, "javascript:" & txtEmail.ClientInstanceName & ".SetFocus();")
    '            'itm.Attributes.Add("class", "BulletedListItem")
    '            'serverValidationList.Items.Add(itm)
    '            'txtEmail.IsValid = False
    '            isValid = False
    '        End If

    '        'profile = Nothing

    '        ' check user name
    '        'profile = Me.CMSDBDataContext.EUS_Profiles.FirstOrDefault(Function(itm As EUS_Profile) itm.LoginName.ToUpper() = txtLogin.Text.ToUpper())
    '        If (DataHelpers.EUS_Profile_CheckEmail(Login)) Then
    '            ErrorsList.Add("txtEmail_UserNameAlreadyInUse_ErrorText")
    '            'Dim txt As String = CurrentPageData.GetCustomString("txtEmail_UserNameAlreadyInUse_ErrorText")
    '            'txt = StripTags(txt)
    '            'Dim itm As ListItem = New ListItem(txt, "javascript:" & txtLogin.ClientInstanceName & ".SetFocus();")
    '            'itm.Attributes.Add("class", "BulletedListItem")
    '            'serverValidationList.Items.Add(itm)
    '            'txtLogin.IsValid = False
    '            isValid = False
    '        End If
    '    End If

    '    Return isValid
    'End Function


#End Region

End Class



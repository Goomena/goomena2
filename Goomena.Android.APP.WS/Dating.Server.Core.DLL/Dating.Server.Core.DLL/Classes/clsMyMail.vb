﻿Imports System.Net.Mail
Imports System.Net



Public Class clsMyMail

    Public Class SMTPSettings
        Public gSMTPServerName As String
        Public gSMTPLoginName As String
        Public gSMTPPassword As String
        Public gSMTPOutPort As String
    End Class

    Public Shared MySMTPSettings As SMTPSettings

    Shared Sub New()
        MySMTPSettings = New clsMyMail.SMTPSettings()
    End Sub


    ''' <summary>
    '''  Sends email using system.net/mailSettings configuration of web.config
    ''' </summary>
    ''' <param name="toAddress"></param>
    ''' <param name="subject"></param>
    ''' <param name="content"></param>
    ''' <remarks></remarks>
    Shared Sub SendMail(toAddress As String,
                        subject As String,
                        content As String,
                        Optional IsHtmlBody As Boolean = False,
                        Optional BCCRecipients As String = Nothing)

        '(1) Create the MailMessage instance
        Dim mm As New MailMessage()

        '(2) Assign the MailMessage's properties
        mm.Subject = subject
        mm.Body = content
        mm.IsBodyHtml = IsHtmlBody
        mm.To.Add(toAddress)

        If (Not String.IsNullOrEmpty(BCCRecipients)) Then
            If (BCCRecipients.Trim().Length > 5) Then
                Dim bccArr As String() = BCCRecipients.Split(";")
                For c1 = 0 To bccArr.Length - 1
                    mm.Bcc.Add(bccArr(c1))
                Next
            End If
        End If
        '(3) Create the SmtpClient object
        Dim smtp As New SmtpClient

        '(4) Send the MailMessage (will use the Web.config settings)
        smtp.Send(mm)
    End Sub


    Public Shared Function SendMail2(ByRef SMTPSettings As SMTPSettings,
                                     ByVal strFrom As String,
                                     ByVal strTo As String,
                                     ByVal strSubject As String,
                                     ByVal strBody As String,
                                     Optional IsBodyHtml As Boolean = False,
                                     Optional BCCRecipients As String = Nothing) As Boolean
        Try

            ' Dim mailMessage As New System.Net.Mail.MailMessage
            'Dim loginInfo As New NetworkCredential("viv.pisces@gmail.com", "vivekraj2484")
            'mailMessage.From = New Mail.MailAddress("viv.pisces@gmail.com")
            'mailMessage.To.Add(New Mail.MailAddress(MailTo))

            'mailMessage.Subject = Subject
            'mailMessage.Body = Body

            Dim loginInfo As New NetworkCredential(SMTPSettings.gSMTPLoginName, SMTPSettings.gSMTPPassword)
            Dim client As New Mail.SmtpClient()
            client.DeliveryMethod = Mail.SmtpDeliveryMethod.Network
            client.Host = SMTPSettings.gSMTPServerName
            client.Port = SMTPSettings.gSMTPOutPort
            client.EnableSsl = False
            client.Credentials = loginInfo


            Dim mm As New MailMessage()

            mm.From = New System.Net.Mail.MailAddress(strFrom)
            mm.To.Add(strTo)
            mm.Subject = strSubject
            mm.Body = strBody
            mm.IsBodyHtml = IsBodyHtml

            If (Not String.IsNullOrEmpty(BCCRecipients)) Then
                If (BCCRecipients.Trim().Length > 5) Then
                    Dim bccArr As String() = BCCRecipients.Split(";")
                    For c1 = 0 To bccArr.Length - 1
                        mm.Bcc.Add(bccArr(c1))
                    Next
                End If
            End If

            client.Send(mm)

            Return True
        Catch ex As Exception
            Throw
            'gER.ErrorMsgBox(ex, "")
        End Try

        Return False
    End Function

    Public Shared Function SendMail2(ByVal strFrom As String,
                                     ByVal strTo As String,
                                     ByVal strSubject As String,
                                     ByVal strBody As String,
                                     Optional IsBodyHtml As Boolean = False,
                                     Optional BCCRecipients As String = Nothing) As Boolean
        Try
            strSubject = AppUtils.StripTags(strSubject)
            Return SendMail2(MySMTPSettings, strFrom, strTo, strSubject, strBody, IsBodyHtml, BCCRecipients)

        Catch ex As Exception
            Throw
            'gER.ErrorMsgBox(ex, "")
        End Try

        Return False
    End Function


    Shared Sub SendMailFromQueue(toAddress As String,
                                 subject As String,
                                 content As String,
                                 ProfileID As Integer,
                                 EmailAction As String,
                                 IsHtmlBody As Boolean,
                                 SMTPServerCredentialsID As Integer?,
                                 BCCRecipients As String)

        Dim hdl As New Core.DLL.clsSendingEmailsHandler()
        Try
            hdl.AddEmail(toAddress, subject, content, ProfileID, IsHtmlBody, EmailAction, SMTPServerCredentialsID, BCCRecipients)
        Catch ex As Exception
            Throw
        Finally
            hdl.Dispose()
        End Try

    End Sub

End Class

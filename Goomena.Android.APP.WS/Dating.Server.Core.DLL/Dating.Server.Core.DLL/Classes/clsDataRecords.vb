﻿
Public Class clsDataRecordLogin

    Public LoginName As String
    Public Password As String
    'Public SiteName As String
    Public HardwareID As String
    Public HASH As String

End Class


Public Class clsDataRecordLoginMember

    Public LoginName As String
    Public Password As String
    'Public SiteName As String
    'Public HardwareID As String
    Public HASH As String
    Public RecordLastLogin As Integer = -10
    Public AllowReferral As Boolean
End Class

Public Class clsDataRecordHeader

    Public Token As String
    Public LoginName As String
    Public Password As String
    Public SiteName As String
    Public HardwareID As String
    Public HASH As String

End Class

<Serializable()> _
Public Class clsDataRecordLoginMemberReturn

    Public IsValid As Boolean
    Public HasErrors As Boolean
    Public ErrorCode As Integer
    Public Message As String
    Public ExtraMessage As String

    ' Public UsersRow As UniCMSDB.SYS_UsersRow

    ' Public AllowSiteManage As Boolean
    '  Public AllowCMS As Boolean
    Public ReferrerParentID As Integer

    Public Status As Integer
    Public ProfileID As Integer
    Public MirrorProfileID As Integer
    Public LoginName As String
    Public FirstName As String
    Public LastName As String
    Public GenderId As Integer
    Public Country As String
    Public Zip As String
    Public latitude As Double?
    Public longitude As Double?
    Public _Region As String
    Public Birthday As Date
    Public Role As String
    Public LAGID As String
    Public CustomReferrer As String
    Public LookingFor_ToMeetMaleID As Boolean
    Public LookingFor_ToMeetFemaleID As Boolean
    Public RegisterDate As DateTime

    'Public Token As String
    'Public HASH As String



    Public Sub Fill(EUS_MembersRow As DSMembers.EUS_ProfilesRow)

        Me.ProfileID = EUS_MembersRow.ProfileID
        Me.MirrorProfileID = EUS_MembersRow.MirrorProfileID
        Me.MirrorProfileID = IIf(Me.MirrorProfileID = 0, Me.ProfileID, Me.MirrorProfileID)


        Me.LoginName = EUS_MembersRow.LoginName
        If (Not EUS_MembersRow.IsDateTimeToRegisterNull()) Then Me.RegisterDate = EUS_MembersRow.DateTimeToRegister
        If (Not EUS_MembersRow.IsStatusNull()) Then Me.Status = EUS_MembersRow.Status
        If (Not EUS_MembersRow.IsReferrerParentIdNull()) Then Me.ReferrerParentID = EUS_MembersRow.ReferrerParentId
        If (Not EUS_MembersRow.IsMirrorProfileIDNull()) Then Me.MirrorProfileID = EUS_MembersRow.MirrorProfileID
        If (Not EUS_MembersRow.IsFirstNameNull()) Then Me.FirstName = EUS_MembersRow.FirstName
        If (Not EUS_MembersRow.IsLastNameNull()) Then Me.LastName = EUS_MembersRow.LastName
        If (Not EUS_MembersRow.IsGenderIdNull()) Then Me.GenderId = EUS_MembersRow.GenderId
        If (Not EUS_MembersRow.IsCountryNull()) Then Me.Country = EUS_MembersRow.Country
        If (Not EUS_MembersRow.IsZipNull()) Then Me.Zip = EUS_MembersRow.Zip
        If (Not EUS_MembersRow.IslatitudeNull()) Then Me.latitude = EUS_MembersRow.latitude
        If (Not EUS_MembersRow.IslongitudeNull()) Then Me.longitude = EUS_MembersRow.longitude
        If (Not EUS_MembersRow.Is_RegionNull()) Then Me._Region = EUS_MembersRow._Region
        If (Not EUS_MembersRow.IsLAGIDNull()) Then Me.LAGID = EUS_MembersRow.LAGID
        If (Not EUS_MembersRow.IsCustomReferrerNull()) Then Me.CustomReferrer = EUS_MembersRow.CustomReferrer
        If (Not EUS_MembersRow.IsLookingFor_ToMeetMaleIDNull()) Then Me.LookingFor_ToMeetMaleID = EUS_MembersRow.LookingFor_ToMeetMaleID
        If (Not EUS_MembersRow.IsLookingFor_ToMeetFemaleIDNull()) Then Me.LookingFor_ToMeetFemaleID = EUS_MembersRow.LookingFor_ToMeetFemaleID

        If (Not EUS_MembersRow.IsBirthdayNull()) Then
            If (EUS_MembersRow.Birthday.Year <> 1800) Then
                Me.Birthday = EUS_MembersRow.Birthday
            End If
        End If

        If (Not EUS_MembersRow.IsRoleNull()) Then
            If (Not String.IsNullOrEmpty(EUS_MembersRow.Role)) Then
                Me.Role = EUS_MembersRow.Role.ToUpper()
            Else
                Me.Role = "MEMBER"
            End If
        End If

        'Dim Token As String = Library.Public.clsHash.ComputeHash(Me.LoginName & "^Token$$$", "SHA1") 'DataRecordLogin.HardwareID & "^^" &
        'Me.Token = Token

        If EUS_MembersRow.Status > ProfileStatusEnum.None Then
            Me.HasErrors = False
            Me.Message = "OK"
            Me.IsValid = True
        Else
            Me.HasErrors = True
            Me.Message = "The user is not Active"
            Me.IsValid = False
        End If
    End Sub


End Class


Public Class clsDataRecordLoginReturn

    Public IsValid As Boolean
    Public HasErrors As Boolean
    Public ErrorCode As Integer
    Public Message As String
    Public ExtraMessage As String

    'Public UsersRow As DSSYSData.SYS_UsersRow

    ' Public AllowSiteManage As Boolean
    '  Public AllowCMS As Boolean

    Public UserID As Integer
    Public LoginName As String
    Public UserRole As Integer
    'Public LAGID As String

    'Public AdminRights As Boolean
    'Public ManagerRights As Boolean
    'Public ContextRights As Boolean

    ' Public CMSAllowAddContext As Boolean
    ' Public CMSAllowEditContext As Boolean
    ' Public CMSAllowDeleteContext As Boolean

    ' Public CMSAllowAddMenuItems As Boolean
    ' Public CMSAllowEditMenuItems As Boolean
    ' Public CMSAllowDeleteMenuItems As Boolean

    Public Token As String


    Public HASH As String
End Class


Public Class clsWSHeaders
    Public HardwareID As String
    Public LoginName As String
    Public Password As String
    Public Token As String
    Public UserID As Integer
    Public ServerStartUTCTime As DateTime?
    Public ServerEndUTCTime As DateTime?
End Class


Public Class clsDataRecordErrorsReturn

    Public HasErrors As Boolean
    Public ErrorCode As Integer
    Public Message As String
    Public ExtraMessage As String

End Class


'Public Class clsDataRecordIPN
'    Property PayProviderAmount As String
'    Property PaymentDateTime As String
'    Property PayTransactionID As String
'    Property CustomerID As String
'    Property SalesSiteProductID As String
'    Property SaleDescription As Object
'    Property PayProviderTransactionID As String
'    Property SaleQuantity As Integer
'    Property custopmerIP As String
'    Property CustomReferrer As String
'    Property PromoCode As Object
'    Property TransactionTypeID As Object
'    Property BuyerInfo As clsBuyerInfo
'    Property VerifyHASH As String

'    'Function VerifyHASH() As String
'    '    Dim sData As String
'    '    sData = Me.PayProviderAmount & "+" & Me.PaymentDateTime & "+" & Me.PayTransactionID & "+" & Me.CustomerID & "+" & Me.SalesSiteProductID & "Extr@Ded0men@"
'    '    Dim h As New Library.Public.clsHash
'    '    Dim Code As String = Library.Public.clsHash.ComputeHash(sData, "SHA512")
'    '    Return Code
'    'End Function

'End Class



'Public Class clsDataRecordIPNReturn
'    Property HasErrors As Boolean
'    Property ErrorCode As Integer
'    Property Message As String
'End Class

'Public Class clsBuyerInfo

'    Property PayerEmail As Object

'End Class

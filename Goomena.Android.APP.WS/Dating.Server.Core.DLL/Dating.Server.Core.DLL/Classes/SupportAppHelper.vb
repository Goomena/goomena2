﻿Imports System.Drawing
Imports System.Drawing.Drawing2D
Imports System.Drawing.Imaging
Imports System.IO


Public NotInheritable Class SupportAppHelper


    Private Sub New()
    End Sub



    Public Shared Function IsEmailSendRunning() As Boolean
        Dim result As Boolean
        Dim ctr As SYS_SupportAppMonitor = Nothing
        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try
            ctr = (From itm In _CMSDBDataContext.SYS_SupportAppMonitors
                                          Select itm).FirstOrDefault()


            If (ctr IsNot Nothing AndAlso ctr.LastEmailSentDate IsNot Nothing) Then
                result = (Date.UtcNow - ctr.LastEmailSentDate.Value).TotalMinutes < 4
            Else
                result = False
            End If
        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

        Return result
    End Function


    Public Shared Sub NotifyEmailSent(isRunning As Boolean)
        Dim ctr As SYS_SupportAppMonitor = Nothing
        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try
            ctr = (From itm In _CMSDBDataContext.SYS_SupportAppMonitors
                                          Select itm).FirstOrDefault()


            If (isRunning) Then
                If (ctr Is Nothing) Then
                    ctr = New SYS_SupportAppMonitor()
                    _CMSDBDataContext.SYS_SupportAppMonitors.InsertOnSubmit(ctr)
                End If

                ctr.LastEmailSentDate = Date.UtcNow
            Else
                ctr.LastEmailSentDate = Nothing
            End If

            _CMSDBDataContext.SubmitChanges()

        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

    End Sub

    Public Shared Function Get__NextDate_SendPaymentVisits_TR() As DateTime
        Dim result As DateTime?
        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try
            result = (From itm In _CMSDBDataContext.SYS_SupportAppMonitors
                                          Select itm.NextDate_SendPaymentVisits_TR).FirstOrDefault()

        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

        Return clsNullable.NullTo(result, DateTime.MinValue)
    End Function

    Public Shared Sub Set__NextDate_SendPaymentVisits_TR(NextDate_SendPaymentVisits_TR As DateTime)
        Dim ctr As SYS_SupportAppMonitor = Nothing
        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try
            ctr = (From itm In _CMSDBDataContext.SYS_SupportAppMonitors
                                          Select itm).FirstOrDefault()


            If (ctr IsNot Nothing) Then
                ctr.NextDate_SendPaymentVisits_TR = NextDate_SendPaymentVisits_TR
                _CMSDBDataContext.SubmitChanges()
            End If
        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try
    End Sub



    Public Shared Function Get__NextDate_CurrencyXMLCheck() As DateTime
        Dim result As DateTime?
        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try
            result = (From itm In _CMSDBDataContext.SYS_SupportAppMonitors
                                          Select itm.NextDate_CurrencyXMLCheck).FirstOrDefault()


        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

        Return clsNullable.NullTo(result, DateTime.MinValue)
    End Function

    Public Shared Sub Set__NextDate_CurrencyXMLCheck(NextDate_CurrencyXMLCheck As DateTime)
        Dim ctr As SYS_SupportAppMonitor = Nothing
        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try
            ctr = (From itm In _CMSDBDataContext.SYS_SupportAppMonitors
                                          Select itm).FirstOrDefault()


            If (ctr IsNot Nothing) Then
                ctr.NextDate_CurrencyXMLCheck = NextDate_CurrencyXMLCheck
                _CMSDBDataContext.SubmitChanges()
            End If
        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try
    End Sub


    Public Shared Function Get__NextDate_CheckOnlineWomen() As DateTime
        Dim result As DateTime?
        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try
            result = (From itm In _CMSDBDataContext.SYS_SupportAppMonitors
                                          Select itm.NextDate_CheckOnlineWomen).FirstOrDefault()

        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

        Return clsNullable.NullTo(result, DateTime.MinValue)
    End Function

    Public Shared Sub Set__NextDate_CheckOnlineWomen(NextDate_CheckOnlineWomen As DateTime)
        Dim ctr As SYS_SupportAppMonitor = Nothing
        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try
            ctr = (From itm In _CMSDBDataContext.SYS_SupportAppMonitors
                                          Select itm).FirstOrDefault()


            If (ctr IsNot Nothing) Then
                ctr.NextDate_CheckOnlineWomen = NextDate_CheckOnlineWomen
                _CMSDBDataContext.SubmitChanges()
            End If
        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try
    End Sub


    Public Shared Function Get__NextDate_SendMobileSMSForVirtualProfile() As DateTime
        Dim result As DateTime?
        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try
            result = (From itm In _CMSDBDataContext.SYS_SupportAppMonitors
                                          Select itm.NextDate_SendMobileSMSForVirtualProfile).FirstOrDefault()

        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

        Return clsNullable.NullTo(result, DateTime.MinValue)
    End Function

    Public Shared Sub Set__NextDate_SendMobileSMSForVirtualProfile(NextDate_SendMobileSMSForVirtualProfile As DateTime)
        Dim ctr As SYS_SupportAppMonitor = Nothing
        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try
            ctr = (From itm In _CMSDBDataContext.SYS_SupportAppMonitors
                                          Select itm).FirstOrDefault()


            If (ctr IsNot Nothing) Then
                ctr.NextDate_SendMobileSMSForVirtualProfile = NextDate_SendMobileSMSForVirtualProfile
                _CMSDBDataContext.SubmitChanges()
            End If
        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try
    End Sub


End Class




﻿Imports System.IO
Imports System.Net

Public Class RemotePost
    Private Inputs As System.Collections.Specialized.NameValueCollection = New System.Collections.Specialized.NameValueCollection

    Public Url As String = ""
    Public Method As String = "post"
    Public FormName As String = "form1"
    Public Target As String = "_self"

    Public Sub Add(ByVal name As String, ByVal value As String)
        Inputs.Add(name, value)
    End Sub

    Public Sub Post()
        System.Web.HttpContext.Current.Response.Clear()
        System.Web.HttpContext.Current.Response.Write("<html><head>")
        System.Web.HttpContext.Current.Response.Write(String.Format("</head><body onload=""document.{0}.submit()"">", FormName))
        System.Web.HttpContext.Current.Response.Write(String.Format("<form name=""{0}"" method=""{1}"" action=""{2}"" target=""{3}"">", FormName, Method, Url, Target))
        Dim i As Integer = 0
        Do While i < Inputs.Keys.Count
            System.Web.HttpContext.Current.Response.Write(String.Format("<input name=""{0}"" type=""hidden"" value=""{1}"">", Inputs.Keys(i), Inputs(Inputs.Keys(i))))
            i += 1
        Loop
        System.Web.HttpContext.Current.Response.Write("</form>")
        System.Web.HttpContext.Current.Response.Write("</body></html>")
        System.Web.HttpContext.Current.Response.End()
    End Sub

    Public Sub ReadParametersFromURL(url As String)
        Dim matches As System.Text.RegularExpressions.MatchCollection = System.Text.RegularExpressions.Regex.Matches(url, "[\?&](?<name>[^&=]+)=(?<value>[^&=]+)")

        For Each m As System.Text.RegularExpressions.Match In matches
            Dim name As String = m.Groups(1).Value
            Dim value As String = m.Groups(2).Value
            value = System.Web.HttpUtility.UrlDecode(value)
            Inputs.Add(name, value)
        Next

    End Sub



    Public Shared Function GetWebFile(urlDownloadFile As String) As String
        Dim localFilePath As String = ""

        Dim hwr As HttpWebRequest = DirectCast(System.Net.WebRequest.Create(urlDownloadFile), HttpWebRequest)
        hwr.Method = "GET"
        hwr.KeepAlive = True
        hwr.UserAgent = "Mozilla/4.0 (compatible; MSIE 10.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E; InfoPath.3)"


        Dim responseStream As Stream = hwr.GetResponse().GetResponseStream()
        Dim fstr As Stream = Nothing
        Try

            Dim fileName As String = Path.GetFileName(urlDownloadFile)
            fileName = DateTime.Now.ToFileTimeUtc().ToString() & "__" & fileName
            localFilePath = Path.Combine(Environment.GetEnvironmentVariable("Temp"), fileName)

            fstr = File.Create(localFilePath)

            ' Allocate a 1k buffer
            Dim buffer As Byte() = New Byte(1024) {}
            Dim bytesRead As Integer
            Dim bytesProcessed As Integer = 0


            ' Simple do/while loop to read from stream until
            ' no bytes are returned
            Do
                ' Read data (up to 1k) from the stream
                bytesRead = responseStream.Read(buffer, 0, buffer.Length)

                ' Write the data to the local file
                fstr.Write(buffer, 0, bytesRead)

                ' Increment total bytes processed
                bytesProcessed += bytesRead
            Loop While (bytesRead > 0)

        Catch ex As Exception
            Throw
        Finally
            responseStream.Close()
            fstr.Close()
        End Try

        Return localFilePath
    End Function


    Private Shared __ADMIN_CONFIG_FOLDER As String
    Public Shared ReadOnly Property ADMIN_CONFIG_FOLDER As String
        Get
            If (__ADMIN_CONFIG_FOLDER Is Nothing) Then
                ' Dim drive As String = New System.IO.DirectoryInfo(Environment.CurrentDirectory).Root
                Dim drive = "C:\CAS\GOOMENA\CONFIG"
                Try
                    System.IO.Directory.CreateDirectory(drive)
                Catch
                End Try

                If (Not System.IO.Directory.Exists(drive)) Then
                    drive = New System.IO.DirectoryInfo(Environment.CurrentDirectory).Root.FullName
                    drive = Path.Combine(drive, "CAS", "GOOMENA", "CONFIG")
                    System.IO.Directory.CreateDirectory(drive)
                End If

                __ADMIN_CONFIG_FOLDER = drive
            End If

            Return __ADMIN_CONFIG_FOLDER
        End Get
    End Property


    Private Shared __ADMIN_CACHE_FOLDER As String
    Public Shared ReadOnly Property ADMIN_CACHE_FOLDER As String
        Get
            If (__ADMIN_CACHE_FOLDER Is Nothing) Then
                ' Dim drive As String = New System.IO.DirectoryInfo(Environment.CurrentDirectory).Root
                Dim drive = "C:\CAS\GOOMENA\CACHE"
                Try
                    System.IO.Directory.CreateDirectory(drive)
                Catch
                End Try

                If (Not System.IO.Directory.Exists(drive)) Then
                    drive = New System.IO.DirectoryInfo(Environment.CurrentDirectory).Root.FullName
                    drive = Path.Combine(drive, "CAS", "GOOMENA", "CACHE")
                    System.IO.Directory.CreateDirectory(drive)
                End If

                __ADMIN_CACHE_FOLDER = drive


                'If (__TEMP_FOLDER Is Nothing) Then
                '    __TEMP_FOLDER = "CAS_GOOMENA_CACHE"
                '    Try
                '        System.IO.Directory.CreateDirectory(drive)
                '    Catch ex As Exception
                '        Try
                '            drive = New System.IO.DirectoryInfo(Environment.CurrentDirectory).Root.FullName
                '            drive = Path.Combine(drive, "CAS", "GOOMENA", "CACHE")
                '            System.IO.Directory.CreateDirectory(drive)
                '        Catch ex1 As Exception

                '        End Try
                '    End Try
                'End If

            End If

            Return __ADMIN_CACHE_FOLDER
        End Get
    End Property



    Public Shared Function RemoveCachedFoldersForPeriod(dateFrom As DateTime?, dateTo As DateTime?) As ADMIN_CACHE_FOLDER_Stats
        Return RemoveCachedFoldersForPeriodHelper(ADMIN_CACHE_FOLDER, dateFrom, dateTo)
    End Function


    Shared Function RemoveCachedFoldersForPeriodHelper(folderPath As String, dateFrom As DateTime?, dateTo As DateTime?) As ADMIN_CACHE_FOLDER_Stats
        Dim __ADMIN_CACHE_FOLDER_Stats As New ADMIN_CACHE_FOLDER_Stats()

        Dim directory As New IO.DirectoryInfo(folderPath)
        Dim __Files As IO.FileInfo() = directory.GetFiles()


        If (dateFrom IsNot Nothing AndAlso dateTo IsNot Nothing) Then

            For cnt = 0 To __Files.Length - 1
                Dim file As IO.FileInfo = __Files(cnt)
                If (file.CreationTime > dateFrom.Value) AndAlso (dateTo.Value > file.CreationTime) Then
                    file.Delete()
                    __ADMIN_CACHE_FOLDER_Stats.Files = __ADMIN_CACHE_FOLDER_Stats.Files + 1
                End If
            Next

        ElseIf (dateFrom IsNot Nothing) Then

            For cnt = 0 To __Files.Length - 1
                Dim file As IO.FileInfo = __Files(cnt)
                If (file.CreationTime > dateFrom.Value) Then
                    file.Delete()
                    __ADMIN_CACHE_FOLDER_Stats.Files = __ADMIN_CACHE_FOLDER_Stats.Files + 1
                End If
            Next

        ElseIf (dateTo IsNot Nothing) Then

            For cnt = 0 To __Files.Length - 1
                Dim file As IO.FileInfo = __Files(cnt)
                If (dateTo.Value > file.CreationTime) Then
                    file.Delete()
                    __ADMIN_CACHE_FOLDER_Stats.Files = __ADMIN_CACHE_FOLDER_Stats.Files + 1
                End If
            Next

        End If

        Dim __Folders As String() = System.IO.Directory.GetDirectories(folderPath)

        For cnt = 0 To __Folders.Length - 1
            Dim dir As String = __Folders(cnt)
            Dim tmp As ADMIN_CACHE_FOLDER_Stats = RemoveCachedFoldersForPeriodHelper(System.IO.Path.Combine(folderPath, dir), dateFrom, dateTo)

            __ADMIN_CACHE_FOLDER_Stats.Folders = __ADMIN_CACHE_FOLDER_Stats.Folders + tmp.Folders
            __ADMIN_CACHE_FOLDER_Stats.Files = __ADMIN_CACHE_FOLDER_Stats.Folders + tmp.Files
        Next

        Dim __FilesNames As String() = System.IO.Directory.GetFiles(folderPath)
        __Folders = System.IO.Directory.GetDirectories(folderPath)
        If (__FilesNames.Length = 0 AndAlso __Folders.Length = 0) Then
            directory.Delete()
            __ADMIN_CACHE_FOLDER_Stats.Folders = __ADMIN_CACHE_FOLDER_Stats.Folders + 1
        End If

        Return __ADMIN_CACHE_FOLDER_Stats
    End Function


    Public Shared Function RemoveGomTempFolder() As String
        If (__ADMIN_CACHE_FOLDER IsNot Nothing) Then
            Dim localFilePath As String
            'localFilePath = Path.Combine(Environment.GetEnvironmentVariable("Temp"), __TEMP_FOLDER)
            localFilePath = Path.Combine(__ADMIN_CACHE_FOLDER)
            If (System.IO.Directory.Exists(localFilePath)) Then
                System.IO.Directory.Delete(localFilePath, True)
            End If
        End If
        Return __ADMIN_CACHE_FOLDER
    End Function

    Public Shared Function GetLocalPhotoPathByURL(urlDownloadFile As String) As String
        Dim localFilePath As String = ""




        Dim fileName As String = Path.GetFileName(urlDownloadFile)

        If (fileName.IndexOf("?") > -1) Then
            fileName = fileName.Remove(fileName.IndexOf("?"))
        End If

        Dim thumbFolder As String = ""
        If (urlDownloadFile.ToUpper().Contains("/THUMBS/")) Then
            thumbFolder = "thumbs"
        End If

        Dim profileFolder As String = ""
        If (urlDownloadFile.ToUpper().Contains("/THUMBS/")) Then
            profileFolder = urlDownloadFile.Remove(urlDownloadFile.ToUpper().IndexOf("/THUMBS/"))
        Else
            profileFolder = urlDownloadFile.Remove(urlDownloadFile.IndexOf(fileName) - 1)
        End If
        profileFolder = profileFolder.Remove(0, profileFolder.LastIndexOf("/") + 1)


        'localFilePath = Path.Combine(Environment.GetEnvironmentVariable("Temp"), __TEMP_FOLDER, profileFolder, thumbFolder)
        'localFilePath = System.Reflection.Assembly.GetExecutingAssembly().Location

        localFilePath = Path.Combine(ADMIN_CACHE_FOLDER, profileFolder, thumbFolder)
        localFilePath = Path.Combine(localFilePath, fileName)

        Return localFilePath
    End Function

    Public Shared Function GetGomPhoto(urlDownloadFile As String) As String

        Dim localFilePath As String = ""
        Dim responseStream As Stream = Nothing
        Dim fstr As Stream = Nothing

        Try
            localFilePath = GetLocalPhotoPathByURL(urlDownloadFile)

            If (Not File.Exists(localFilePath)) Then

                If (Not Directory.Exists(System.IO.Path.GetDirectoryName(localFilePath))) Then
                    Directory.CreateDirectory(System.IO.Path.GetDirectoryName(localFilePath))
                End If

                Dim hwr As HttpWebRequest = DirectCast(System.Net.WebRequest.Create(urlDownloadFile), HttpWebRequest)
                hwr.Method = "GET"
                hwr.KeepAlive = True
                hwr.UserAgent = "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E; InfoPath.3)"


                responseStream = hwr.GetResponse().GetResponseStream()
                Try
                    fstr = File.Create(localFilePath)
                Catch ex As System.UnauthorizedAccessException

                    If (Directory.Exists(localFilePath)) Then
                        Try
                            Directory.Delete(localFilePath, True)
                        Catch
                        End Try

                        If (Directory.Exists(localFilePath)) Then
                            Try
                                Directory.Delete(localFilePath, True)
                            Catch
                            End Try
                        End If

                        fstr = File.Create(localFilePath)
                    End If

                End Try

                ' Allocate a 1k buffer
                Dim buffer As Byte() = New Byte(1024) {}
                Dim bytesRead As Integer
                Dim bytesProcessed As Integer = 0


                ' Simple do/while loop to read from stream until
                ' no bytes are returned
                Do
                    ' Read data (up to 1k) from the stream
                    bytesRead = responseStream.Read(buffer, 0, buffer.Length)

                    ' Write the data to the local file
                    fstr.Write(buffer, 0, bytesRead)

                    ' Increment total bytes processed
                    bytesProcessed += bytesRead
                Loop While (bytesRead > 0)

            End If

        Catch ex As Exception
            Throw
        Finally
            If (responseStream IsNot Nothing) Then responseStream.Close()
            If (fstr IsNot Nothing) Then fstr.Close()
        End Try

        Return localFilePath
    End Function


    Public Class ADMIN_CACHE_FOLDER_Stats
        Public Property Folders As Integer
        Public Property Files As Integer
    End Class

End Class

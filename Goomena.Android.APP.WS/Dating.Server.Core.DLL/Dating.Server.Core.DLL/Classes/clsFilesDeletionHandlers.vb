﻿Public Class clsFilesDeletionHandlers
    Implements IDisposable

    Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


    Public Sub AddFile(filePath As String)
        Try
            Dim file As New Core.DLL.SYS_FilesForDeletion

            file.Datecreated = Date.UtcNow
            file.Path = filePath

            _CMSDBDataContext.SYS_FilesForDeletions.InsertOnSubmit(file)
            _CMSDBDataContext.SubmitChanges()

        Catch ex As Exception
            Throw
        Finally
        End Try
    End Sub


    Public Function GetNextFile() As Core.DLL.SYS_FilesForDeletion
        Dim file As Core.DLL.SYS_FilesForDeletion
        Try
            file = (From itm In _CMSDBDataContext.SYS_FilesForDeletions
                   Where itm.Datedeleted Is Nothing
                   Select itm).FirstOrDefault()

            _CMSDBDataContext.SubmitChanges()

        Catch ex As Exception
            Throw
        Finally
        End Try

        Return file
    End Function


    Public Sub SetFileDeleted(file1 As Core.DLL.SYS_FilesForDeletion)
        Try
            Dim file As Core.DLL.SYS_FilesForDeletion = (From itm In _CMSDBDataContext.SYS_FilesForDeletions
                   Where itm.Id = file1.Id
                   Select itm).FirstOrDefault()
            file.Datedeleted = Date.UtcNow

            If (Len(file1.Exception) > 1023) Then file1.Exception = file1.Exception.Remove(1023)
            file.Exception = file1.Exception

            _CMSDBDataContext.SubmitChanges()
        Catch ex As Exception
            Throw
        Finally
        End Try

    End Sub


    Public Sub Dispose() Implements IDisposable.Dispose
        If (_CMSDBDataContext IsNot Nothing) Then _CMSDBDataContext.Dispose()
    End Sub

End Class

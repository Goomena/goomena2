﻿Public Class clsLogger
    Public Shared Sub InsertLog(Method As String)
        InsertLog(Method, Nothing, Nothing, Nothing)
    End Sub

    Public Shared Sub InsertLog(Method As String, ProfileId1 As Integer?)
        InsertLog(Method, ProfileId1, Nothing, Nothing)
    End Sub

    Public Shared Sub InsertLog(Method As String, ProfileId1 As Integer?, Duration As Double?)
        InsertLog(Method, ProfileId1, Nothing, Duration)
    End Sub

    Public Shared Sub InsertLog(Method As String, ProfileId1 As Integer?, ProfileId2 As Integer?, Duration As Double?)
        Try
            ' Total Milliseconds
            If (Duration <= 300) Then Exit Sub


            Dim sql As String = <sql><![CDATA[
INSERT INTO [dbo].[SYS_LogMethods]
           ([Method],[ProfileId1],[ProfileId2],[DateCreated],[Duration])
     VALUES
           (@Method,@ProfileId1,@ProfileId2,getutcdate(),@Duration)
]]></sql>.Value

            Dim cmd As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql)
            Dim par1 As New SqlClient.SqlParameter("@Method", SqlDbType.VarChar, 200)
            par1.Value = Method & "_ND"
            cmd.Parameters.Add(par1)

            If (ProfileId1 Is Nothing) Then
                cmd.Parameters.Add(New SqlClient.SqlParameter("@ProfileId1", System.DBNull.Value))
            Else
                cmd.Parameters.Add(New SqlClient.SqlParameter("@ProfileId1", ProfileId1))
            End If

            If (ProfileId2 Is Nothing) Then
                cmd.Parameters.Add(New SqlClient.SqlParameter("@ProfileId2", System.DBNull.Value))
            Else
                cmd.Parameters.Add(New SqlClient.SqlParameter("@ProfileId2", ProfileId2))
            End If

            If (Duration Is Nothing) Then
                cmd.Parameters.Add(New SqlClient.SqlParameter("@Duration", System.DBNull.Value))
            Else
                cmd.Parameters.Add(New SqlClient.SqlParameter("@Duration", Duration))
            End If

            DataHelpers.ExecuteNonQuery(cmd)
        Catch
        End Try
    End Sub

End Class

﻿Public Class clsMobile


    Public Shared Function SendSMSViaClickatell(ByVal CellPhoneNum As String, ByVal BodyMessageText As String, ByVal SMSLoginName As String, ByVal SMSPass As String, ByVal SMSGatewayAPIID As String) As Boolean
        ' CellPhoneNum Format +3069....
        ' SendSMS("+306974873354", "Test by jim", "Soundsoft", "jim2000", "3081457")

        Try
            Dim client As Net.WebClient = New Net.WebClient
            ' Add a user agent header in case the requested URI contains a query.

            ' BodyMessageText = "03B503BB03BB03B903BD03B903BA03B1000D000A" '"Ελλινικά Γράμματα"
            'Api = 3081457 

            client.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR1.0.3705;)")
            client.QueryString.Add("user", SMSLoginName)
            client.QueryString.Add("password", SMSPass)
            client.QueryString.Add("api_id", SMSGatewayAPIID)
            client.QueryString.Add("to", CellPhoneNum)
            client.QueryString.Add("text", BodyMessageText)
            ' client.QueryString.Add("unicode=1", SMSPass.EditValue)
            Dim baseurl As String = "http://api.clickatell.com/http/sendmsg"
            Dim data As System.IO.Stream = client.OpenRead(baseurl)
            Dim reader As System.IO.StreamReader = New System.IO.StreamReader(data)
            Dim s As String = reader.ReadToEnd()
            data.Close()
            reader.Close()
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

End Class

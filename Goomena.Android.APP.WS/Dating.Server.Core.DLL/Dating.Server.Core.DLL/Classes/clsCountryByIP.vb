﻿<Serializable()> _
Public Class clsCountryByIP
    Public Property CountryCode As String
    Public Property RegionCode As String
    Public Property city As String
    Public Property latitude As String
    Public Property longitude As String
    Public Property postalCode As String




    Public Shared Function GetCountryCodeFromIP(ip As String, connectionString As String) As clsCountryByIP
        Dim country As New clsCountryByIP()
        Dim output As String = Dot2LongIP(ip)
        Dim __tmp As String = "US"

        Dim sql As String = "EXEC GetCountryFromIP @ip=@ipIn"
        Dim con As New SqlClient.SqlConnection(connectionString)
        Dim cmd As New SqlClient.SqlCommand(sql, con)
        cmd.Parameters.AddWithValue("@ipIn", output)

        Dim rsRead As SqlClient.SqlDataReader = Nothing
        Try
            con.Open()
            rsRead = cmd.ExecuteReader()
            If (rsRead.Read()) Then

                __tmp = rsRead("country")
                __tmp = __tmp.Trim("""")
                country.CountryCode = __tmp

                If (__tmp = "US") Then
                    __tmp = rsRead("Region")
                    __tmp = __tmp.Trim("""")
                    country.RegionCode = __tmp
                Else
                    country.RegionCode = ""
                End If

                __tmp = rsRead("latitude")
                __tmp = __tmp.Trim("""")
                country.latitude = __tmp

                __tmp = rsRead("longitude")
                __tmp = __tmp.Trim("""")
                country.longitude = __tmp

                __tmp = rsRead("postalCode")
                __tmp = __tmp.Trim("""")
                country.postalCode = __tmp

                __tmp = rsRead("city")
                __tmp = __tmp.Trim("""")
                country.city = __tmp

            Else
                Throw New IPNotFoundException("Requested IP (" & ip & ") address not found in GEO Locations.")
            End If
        Catch ex As Exception
            Throw
        Finally
            If (rsRead IsNot Nothing) Then rsRead.Close()
            If (cmd IsNot Nothing) Then
                cmd.Connection.Close()
                cmd.Dispose()
            End If
        End Try

        Return country
    End Function

    Public Shared Function Dot2LongIP(ByVal DottedIP As String) As Double
        Dim arrDec() As String
        Dim i As Integer
        Dim intResult As Long
        If DottedIP = "" Then
            Dot2LongIP = 0
        Else
            arrDec = DottedIP.Split(".")
            For i = arrDec.Length - 1 To 0 Step -1
                intResult = intResult + ((Int(arrDec(i)) Mod 256) * Math.Pow(256, 3 - i))
            Next
            Dot2LongIP = intResult
        End If
    End Function

End Class

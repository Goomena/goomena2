﻿Public Class clsCreditsHelper

    Private Shared _gEUS_CreditsType As New Dictionary(Of UnlockType, EUS_CreditsType)

    Public Shared Sub ClearCache()
        Try
            If (_gEUS_CreditsType IsNot Nothing) Then
                _gEUS_CreditsType.Clear()
            End If
        Catch
        End Try

    End Sub



    Public Shared Function GetRequiredCredits(type As UnlockType) As EUS_CreditsType
        Dim creditsRequired As EUS_CreditsType = Nothing

        Try
            If (_gEUS_CreditsType.ContainsKey(type)) Then
                creditsRequired = _gEUS_CreditsType(type)
            End If
        Catch
        End Try

        If (creditsRequired Is Nothing) Then


            Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
            Try

                If (type = UnlockType.UNLOCK_CONVERSATION) Then
                    creditsRequired = (From itm In _CMSDBDataContext.EUS_CreditsTypes
                                 Where itm.CreditsType = "UNLOCK_CONVERSATION"
                                 Select itm).FirstOrDefault()

                ElseIf (type = UnlockType.UNLOCK_MESSAGE_READ) Then
                    creditsRequired = (From itm In _CMSDBDataContext.EUS_CreditsTypes
                                 Where itm.CreditsType = "UNLOCK_MESSAGE_READ"
                                 Select itm).FirstOrDefault()

                ElseIf (type = UnlockType.UNLOCK_MESSAGE_SEND) Then
                    creditsRequired = (From itm In _CMSDBDataContext.EUS_CreditsTypes
                                 Where itm.CreditsType = "UNLOCK_MESSAGE_SEND"
                                 Select itm).FirstOrDefault()

                ElseIf (type = UnlockType.UNLOCK_MESSAGE_SEND_FREE) Then
                    creditsRequired = (From itm In _CMSDBDataContext.EUS_CreditsTypes
                                 Where itm.CreditsType = "UNLOCK_MESSAGE_SEND_FREE"
                                 Select itm).FirstOrDefault()

                ElseIf (type = UnlockType.UNLOCK_MESSAGE_READ_FREE) Then
                    creditsRequired = (From itm In _CMSDBDataContext.EUS_CreditsTypes
                                 Where itm.CreditsType = "UNLOCK_MESSAGE_READ_FREE"
                                 Select itm).FirstOrDefault()
                End If

            Catch ex As Exception
                Throw
            Finally
                _CMSDBDataContext.Dispose()
            End Try

            Try
                _gEUS_CreditsType.Add(type, creditsRequired)
            Catch
            End Try
        End If


        Return creditsRequired
    End Function


End Class

﻿Imports Dating.Server.Core.DLL
Imports System.Data.SqlClient

<Serializable()> _
Public Class clsReferrerCredits


    Private _referrerId As Integer
    Private _affData As ReferrersRepository
    Private _loginName As String


    Private _creditsAvailable As Double
    Public ReadOnly Property CreditsAvailable As Double
        Get
            Return _creditsAvailable
        End Get
    End Property

    Private _creditsPending As Double
    Public ReadOnly Property CreditsPending As Double
        Get
            Return _creditsPending
        End Get
    End Property

    Private _creditsTotal As Double
    Public ReadOnly Property CreditsTotal As Double
        Get
            Return _creditsTotal
        End Get
    End Property


    Private _commissionCreditsAvailable As Double
    Public ReadOnly Property CommissionCreditsAvailable As Double
        Get
            Return _commissionCreditsAvailable
        End Get
    End Property

    Private _commissionCreditsPending As Double
    Public ReadOnly Property CommissionCreditsPending As Double
        Get
            Return _commissionCreditsPending
        End Get
    End Property

    Private _commissionCreditsTotal As Double
    Public ReadOnly Property CommissionCreditsTotal As Double
        Get
            Return _commissionCreditsTotal
        End Get
    End Property



    Private _moneyAvailable As Double
    Public ReadOnly Property MoneyAvailable As Double
        Get
            Return _moneyAvailable
        End Get
    End Property

    Private _moneyPending As Double
    Public ReadOnly Property MoneyPending As Double
        Get
            Return _moneyPending
        End Get
    End Property

    Private _moneyTotal As Double
    Public ReadOnly Property MoneyTotal As Double
        Get
            Return _moneyTotal
        End Get
    End Property



    Public Property DateCreated As DateTime

    Private _convRate As Double
    Public ReadOnly Property ConversionRate As Double
        Get
            Return _convRate
        End Get
    End Property


    Private _familyMembers As Integer
    Public ReadOnly Property FamilyMembers As Integer
        Get
            Return _familyMembers
        End Get
    End Property

    Private _rootFamilyMembers As Integer
    Public ReadOnly Property RootFamilyMembers As Integer
        Get
            Return _rootFamilyMembers
        End Get
    End Property


    Private _config As clsConfigValues
    Private Function GetConfig() As clsConfigValues
        If (_config Is Nothing) Then _config = New clsConfigValues()
        Return _config
    End Function


    Private _referrerCommissions As clsReferrerCommissions
    Public Function GetCurrentReferrerCommissions() As clsReferrerCommissions
        If (_referrerCommissions Is Nothing) Then
            _referrerCommissions = New clsReferrerCommissions()
            _referrerCommissions.Load(Me._referrerId)
        End If
        Return _referrerCommissions
    End Function


    Private _ReferrerCommissions_Admin As Core.DLL.DSReferrers.GetReferrerCommissions_AdminDataTable
    Public Function GetCommissionPercentsForAllReferrers() As Core.DLL.DSReferrers.GetReferrerCommissions_AdminDataTable
        If (_ReferrerCommissions_Admin Is Nothing) Then
            _ReferrerCommissions_Admin = DataHelpers.GetReferrerCommissions_Admin().GetReferrerCommissions_Admin
        End If
        Return _ReferrerCommissions_Admin
    End Function

    Public ReadOnly Property Datatable As DSCustom.ReferrersRepositoryDataDataTable
        Get
            Return _affData.DataTable
        End Get
    End Property

    Private _dateFrom As DateTime?
    Public ReadOnly Property DateFrom As DateTime?
        Get
            Return _dateFrom
        End Get
    End Property


    Private _dateTo As DateTime?
    Public ReadOnly Property DateTo As DateTime?
        Get
            Return _dateTo
        End Get
    End Property


    'Sub New()

    'End Sub

    Sub New(referrerID As Integer, loginName As String, dateFrom As DateTime?, dateTo As DateTime?)
        Me._referrerId = referrerID
        Me._loginName = loginName
        Me._dateFrom = dateFrom
        Me._dateTo = dateTo
    End Sub


    'Public Sub Load(referrerID As Integer, loginName As String, dateFrom As DateTime?, dateTo As DateTime?)
    '    Me._referrerId = referrerID
    '    Me._loginName = loginName
    '    Me._dateFrom = dateFrom
    '    Me._dateTo = dateTo
    '    Me.Load()
    'End Sub


    Public Sub Load()
        _affData = New ReferrersRepository()

        If (_referrerId > 1) Then

            Me._convRate = GetCurrentReferrerCommissions().REF_CommissionConvRate
            LoadData()
            _affData.CalculateData(_referrerId)

            Me._creditsTotal = _affData.GetCreditsSumLevelsTotal(_referrerId)
            Me._commissionCreditsTotal = _affData.GetCommissionCreditsLevelsTotal(_referrerId)
            Me._moneyTotal = Me._commissionCreditsTotal

            Me._creditsPending = _affData.GetCreditsPendingLevelsTotal(_referrerId)
            Me._commissionCreditsPending = _affData.GetCommissionCreditsPendingLevelsTotal(_referrerId)
            Me._moneyPending = Me._commissionCreditsPending

            Me._creditsAvailable = _affData.GetCreditsAvailableLevelsTotal(_referrerId)
            Me._commissionCreditsAvailable = _affData.GetCommissionCreditsAvailableLevelsTotal(_referrerId)
            Me._moneyAvailable = Me._commissionCreditsAvailable

            Me._familyMembers = _affData.DataTable.Select("Level>0").Length


            ' calc pending and available
            Dim calcCreditsPending As Double = Me._creditsPending * GetCurrentReferrerCommissions().REF_CommissionPendingPercent
            Me._creditsAvailable = Me._creditsAvailable + (Me._creditsPending - calcCreditsPending)
            Me._creditsPending = calcCreditsPending

            Dim calcCommCreditsPending As Double = Me._commissionCreditsPending * GetCurrentReferrerCommissions().REF_CommissionPendingPercent
            Me._commissionCreditsAvailable = Me._commissionCreditsAvailable + (Me._commissionCreditsPending - calcCommCreditsPending)
            Me._commissionCreditsPending = calcCommCreditsPending

            Dim calcMoneyPending As Double = Me._moneyPending * GetCurrentReferrerCommissions().REF_CommissionPendingPercent
            Me._moneyAvailable = Me._moneyAvailable + (Me._moneyPending - calcMoneyPending)
            Me._moneyPending = calcMoneyPending
            ' calc pending and available END


            Me.DateCreated = DateTime.UtcNow
        End If

    End Sub


    Public Sub LoadRoot()
        _affData = New ReferrersRepository()

        If (_referrerId = 1) Then

            LoadData()
            _affData.CalculateData(_referrerId)
            Me._rootFamilyMembers = _affData.DataTable.Rows.Count

            Me.DateCreated = DateTime.UtcNow
        End If

    End Sub


    Private Sub LoadData()
        ' Dim con As SqlClient.SqlConnection = DataHelpers.GetSqlCommand().Connection
        'Try


        'Dim CreditsSumForReferrers As Core.DLL.DSReferrers = DataHelpers.GetReferrersCreditsSum_Admin3(Me._referrerId, True, DateFrom, DateTo)
        Dim CommissionsForReferrers As Core.DLL.DSReferrers.GetReferrerCommissions_AdminDataTable = GetCommissionPercentsForAllReferrers()

        Dim level As Integer = 0
        Dim mySubReferrers As New List(Of Integer)
        Dim nodesCount As Integer
        Dim ds As New DSMembers

        If (Me._referrerId > 1) Then
            level = 1
            'Dim dict As Dictionary(Of String, Double) = GetCreditsSum(Me._referrerId, CreditsSumForReferrers.GetReferrersCreditsSum_Admin3, GetCurrentReferrerCommissions().REF_CommissionPendingPeriod, level)
            Dim dict As Dictionary(Of String, Double) = GetCreditsSum(Me._referrerId, GetCurrentReferrerCommissions().REF_CommissionPendingPeriod, level)
            Dim refBonus As DSReferrers = DataHelpers.GetReferrersBonus(DateFrom, DateTo, Me._referrerId)
            Dim drsBonus As Object = refBonus.REF_Bonus.Compute("SUM(Amount)", "ProfileID=" & Me._referrerId)
            If (drsBonus IsNot System.DBNull.Value) Then
                dict("available") = dict("available") + CType(drsBonus, Double)
                dict("total") = dict("total") + CType(drsBonus, Double)
            End If

            _affData.AddRecord(Me._referrerId, Me._loginName, 0, level, ProfileStatusEnum.Approved, dict("totalCost"), dict("total"), dict("availableCost"), dict("available"), dict("pendingCost"), dict("pending"))
            mySubReferrers.Add(Me._referrerId)
        Else
            level = 0
            _affData.AddRecord(Me._referrerId, Me._loginName, 0, level, ProfileStatusEnum.Approved, 0, 0, 0, 0, 0, 0)
            mySubReferrers.Add(Me._referrerId)
        End If


        ' next levels
        While (mySubReferrers.Count > 0 AndAlso (level < clsReferrer.StopLevel OrElse Me._referrerId = 1))
            level = level + 1

            Try

                Dim aff As New clsReferrer
                aff.GetProfilesForReferrerParentIdList(ds, mySubReferrers)

                If (ds.EUS_Profiles.Rows.Count > 0) Then

                    mySubReferrers.Clear()

                    Dim cmd As SqlClient.SqlCommand = DataHelpers.GetSqlCommand()
                    Dim sqlPattern As String = <sql><![CDATA[
exec [GetReferrersCreditsSum_ForLevel] 	@ReferrerID=@ReferrerID[COUNT], @ReferrerLevel=@ReferrerLevel[COUNT], @REF_CommissionPendingPeriod=@REF_CommissionPendingPeriod[COUNT], @dateFrom=@dateFrom[COUNT], @dateTo=@dateTo[COUNT];]]></sql>

                    Dim sb As New System.Text.StringBuilder()
                    Dim c As Integer
                    For c = 0 To ds.EUS_Profiles.Rows.Count - 1
                        Dim dr As DSMembers.EUS_ProfilesRow = ds.EUS_Profiles.Rows(c)
                        Dim profId As Integer = dr.ProfileID
                        Dim affComm As clsReferrerCommissions = clsReferrerCommissions.Load(profId, CommissionsForReferrers)


                        Dim sql2 As String = sqlPattern.Replace("[COUNT]", c.ToString())
                        sb.Append(sql2)

                        cmd.Parameters.AddWithValue("@ReferrerID" & c, profId)
                        cmd.Parameters.AddWithValue("@ReferrerLevel" & c, level)
                        cmd.Parameters.AddWithValue("@REF_CommissionPendingPeriod" & c, GetCurrentReferrerCommissions().REF_CommissionPendingPeriod)
                        If (DateFrom.HasValue) Then
                            cmd.Parameters.AddWithValue("@dateFrom" & c, DateFrom.Value)
                        Else
                            cmd.Parameters.AddWithValue("@dateFrom" & c, System.DBNull.Value)
                        End If
                        If (DateTo.HasValue) Then
                            cmd.Parameters.AddWithValue("@dateTo" & c, DateTo.Value)
                        Else
                            cmd.Parameters.AddWithValue("@dateTo" & c, System.DBNull.Value)
                        End If

                        ''Dim dict As Dictionary(Of String, Double) = GetCreditsSum(profId, CreditsSumForReferrers.GetReferrersCreditsSum_Admin3, affComm.REF_CommissionPendingPeriod, level)
                        'Dim dict As Dictionary(Of String, Double) = GetCreditsSum(profId, GetCurrentReferrerCommissions().REF_CommissionPendingPeriod, level, DateFrom, DateTo)

                        '_affData.AddRecord(profId, dr.LoginName, dr.ReferrerParentId, level, dr.Status, dict("totalCost"), dict("total"), dict("availableCost"), dict("available"), dict("pendingCost"), dict("pending"))

                        'mySubReferrers.Add(dr.ProfileID)
                        'nodesCount = nodesCount + 1
                    Next


                    cmd.CommandText = sb.ToString()
                    Dim ds2 As DataSet = DataHelpers.GetDataSet(cmd)

                    'Dim ds2 As New DataSet()
                    'Dim adapter As New SqlDataAdapter(cmd)

                    'Try
                    '    adapter.Fill(ds)
                    'Catch ex As Exception
                    '    Throw
                    'Finally
                    '    adapter.Dispose()
                    '    cmd.Parameters.Clear()
                    'End Try



                    For tbl = 0 To ds2.Tables.Count - 1
                        Dim dt As DataTable = ds2.Tables(tbl)

                        Dim dr As DSMembers.EUS_ProfilesRow = ds.EUS_Profiles.Select("Profileid=" & dt(0)("ReferrerID"))(0)
                        Dim profId As Integer = dr.ProfileID

                        Dim dict As New Dictionary(Of String, Double)
                        dict.Add("pending", dt(0)("pending"))
                        dict.Add("available", dt(0)("available"))
                        dict.Add("total", dt(0)("pending") + dt(0)("available"))
                        dict.Add("pendingCost", dt(0)("pendingCost"))
                        dict.Add("availableCost", dt(0)("availableCost"))
                        dict.Add("totalCost", dt(0)("pendingCost") + dt(0)("availableCost"))


                        _affData.AddRecord(profId, dr.LoginName, dr.ReferrerParentId, level, dr.Status, dict("totalCost"), dict("total"), dict("availableCost"), dict("available"), dict("pendingCost"), dict("pending"))

                        mySubReferrers.Add(dr.ProfileID)
                        nodesCount = nodesCount + 1
                    Next


                    'For c = 0 To ds.EUS_Profiles.Rows.Count - 1
                    '    Dim dr As DSMembers.EUS_ProfilesRow = ds.EUS_Profiles.Rows(c)
                    '    Dim profId As Integer = dr.ProfileID
                    '    Dim affComm As clsReferrerCommissions = clsReferrerCommissions.Load(profId, CommissionsForReferrers)
                    '    'Dim dict As Dictionary(Of String, Double) = GetCreditsSum(profId, CreditsSumForReferrers.GetReferrersCreditsSum_Admin3, affComm.REF_CommissionPendingPeriod, level)
                    '    Dim dict As Dictionary(Of String, Double) = GetCreditsSum(profId, GetCurrentReferrerCommissions().REF_CommissionPendingPeriod, level, DateFrom, DateTo)

                    '    _affData.AddRecord(profId, dr.LoginName, dr.ReferrerParentId, level, dr.Status, dict("totalCost"), dict("total"), dict("availableCost"), dict("available"), dict("pendingCost"), dict("pending"))

                    '    mySubReferrers.Add(dr.ProfileID)
                    '    nodesCount = nodesCount + 1
                    'Next
                End If

            Catch ex As Exception
                Throw
            End Try

        End While

        'Catch ex As Exception
        '    Throw
        'Finally
        '    If (con IsNot Nothing) Then con.Close()
        'End Try

    End Sub



    Private Function GetCreditsSum(profileID As Integer, dt As DSReferrers.GetReferrersCreditsSum_Admin3DataTable, REF_CommissionPendingPeriod As Integer, level As Integer) As Dictionary(Of String, Double)

        Dim pending As Double = 0
        Dim available As Double = 0
        Dim pendingCost As Double = 0
        Dim availableCost As Double = 0

        Dim drs As DSReferrers.GetReferrersCreditsSum_Admin3Row() = dt.Select("ReferrerID = " & profileID)
        Dim cnt As Integer
        For cnt = 0 To drs.Length - 1
            Dim DateTimeCreated As DateTime = drs(cnt).DateTimeToCreate
            Dim money As Double
            Dim moneyCost As Double

            Select Case level
                Case 1
                    If (Not drs(cnt).IsREF_L1_MoneyNull()) Then money = drs(cnt).REF_L1_Money
                Case 2
                    If (Not drs(cnt).IsREF_L2_MoneyNull()) Then money = drs(cnt).REF_L2_Money
                Case 3
                    If (Not drs(cnt).IsREF_L3_MoneyNull()) Then money = drs(cnt).REF_L3_Money
                Case 4
                    If (Not drs(cnt).IsREF_L4_MoneyNull()) Then money = drs(cnt).REF_L4_Money
                Case 5
                    If (Not drs(cnt).IsREF_L5_MoneyNull()) Then money = drs(cnt).REF_L5_Money
            End Select
            If (Not drs(cnt).IsCostNull()) Then moneyCost = drs(cnt).Cost

            If (DateTimeCreated.AddDays(REF_CommissionPendingPeriod) > DateTime.UtcNow) Then
                pending = pending + money
                pendingCost = pendingCost + moneyCost
            Else
                available = available + money
                availableCost = availableCost + moneyCost
            End If

        Next


        Dim dict As New Dictionary(Of String, Double)
        dict.Add("pending", pending)
        dict.Add("available", available)
        dict.Add("total", pending + available)
        dict.Add("pendingCost", pendingCost)
        dict.Add("availableCost", availableCost)
        dict.Add("totalCost", pendingCost + availableCost)


        Return dict
    End Function



    Private Function GetCreditsSum(profileID As Integer, REF_CommissionPendingPeriod As Integer, level As Integer) As Dictionary(Of String, Double)
        'ByRef con As SqlClient.SqlConnection,

        Dim sql As String = <sql><![CDATA[
exec [GetReferrersCreditsSum_ForLevel] 	@ReferrerID=@ReferrerID, @ReferrerLevel=@ReferrerLevel, @REF_CommissionPendingPeriod=@REF_CommissionPendingPeriod, @dateFrom=@dateFrom, @dateTo=@dateTo;
]]></sql>

        Dim cmd As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql)
        'cmd.Connection = con
        cmd.Parameters.AddWithValue("@ReferrerID", profileID)
        cmd.Parameters.AddWithValue("@ReferrerLevel", level)
        cmd.Parameters.AddWithValue("@REF_CommissionPendingPeriod", REF_CommissionPendingPeriod)
        If (DateFrom.HasValue) Then
            cmd.Parameters.AddWithValue("@dateFrom", DateFrom.Value)
        Else
            cmd.Parameters.AddWithValue("@dateFrom", System.DBNull.Value)
        End If
        If (DateTo.HasValue) Then
            cmd.Parameters.AddWithValue("@dateTo", DateTo.Value)
        Else
            cmd.Parameters.AddWithValue("@dateTo", System.DBNull.Value)
        End If


        'Dim dt As New DataTable()
        'Dim adapter As New SqlDataAdapter(cmd)

        'Try
        '    adapter.Fill(dt)
        'Catch ex As Exception
        '    Throw
        'Finally
        '    adapter.Dispose()
        '    cmd.Parameters.Clear()
        'End Try


        Dim dt As DataTable = DataHelpers.GetDataTable(cmd)

        Dim dict As New Dictionary(Of String, Double)
        dict.Add("pending", dt(0)("pending"))
        dict.Add("available", dt(0)("available"))
        dict.Add("total", dt(0)("pending") + dt(0)("available"))
        dict.Add("pendingCost", dt(0)("pendingCost"))
        dict.Add("availableCost", dt(0)("availableCost"))
        dict.Add("totalCost", dt(0)("pendingCost") + dt(0)("availableCost"))


        Return dict
    End Function



    Public Function GetReferrersForLevel(level As Integer) As DSCustom.ReferrersRepositoryDataRow()
        Return _affData.GetReferrersForLevel(level)
    End Function


    Public Function GetReferrerSales(referrerID As Integer) As Integer
        Return _affData.GetCreditsSum(referrerID)
    End Function


    Public Function GetReferrerCommissions(referrerID As Integer) As Double
        Return _affData.GetCommissionCreditsLevelsTotal(referrerID)
    End Function


    Public Function GetTotalPayment(referrerID As Integer) As Dictionary(Of String, Object)
        Dim data As New Dictionary(Of String, Object)
        data.Add("TotalDebitAmount", 0.0)
        data.Add("LastPaymentDate", DateTime.MinValue)
        data.Add("LastPaymentAmount", 0.0)

        Dim sql As String = <sql><![CDATA[
select * from (
			select 
				'TotalDebitAmount'=SUM(DebitAmount)
			from dbo.AFF_AffiliateTransaction
			where AffiliateID=@ReferrerID
) as t, (
			select top(1) 
					Date_Time as 'LastPaymentDate'
					,DebitAmount as 'LastPaymentAmount'
			from dbo.AFF_AffiliateTransaction
			where AffiliateID=@ReferrerID
			order by AffiliateTransactionID desc
) as t2
]]></sql>

        sql = sql.Replace("@ReferrerID", referrerID)
        Dim dt As DataTable = DataHelpers.GetDataTable(sql)
        If (dt.Rows.Count > 0) Then

            If (Not dt(0).IsNull("TotalDebitAmount")) Then data("TotalDebitAmount") = dt(0)("TotalDebitAmount")
            If (Not dt(0).IsNull("LastPaymentDate")) Then data("LastPaymentDate") = dt(0)("LastPaymentDate")
            If (Not dt(0).IsNull("LastPaymentAmount")) Then data("LastPaymentAmount") = dt(0)("LastPaymentAmount")
        End If

        Return data
    End Function



    Public Function GetTotalPaymentDS(referrerID As Integer, Optional dateFrom As DateTime? = Nothing, Optional dateTo As DateTime? = Nothing) As DataTable

        Dim sql As String = <sql><![CDATA[
select * from (
	select 
		'TotalDebitAmount'=SUM(DebitAmount)
	from dbo.AFF_AffiliateTransaction
	where AffiliateID=@ReferrerID
	and	(Date_Time >= @dateFrom or @dateFrom is null)
	and (Date_Time <= @dateTo or @dateTo is null)
) as t, (
	select top(1) 
			Date_Time as 'LastPaymentDate'
			,DebitAmount as 'LastPaymentAmount'
	from dbo.AFF_AffiliateTransaction
	where AffiliateID=@ReferrerID
	and	(Date_Time >= @dateFrom or @dateFrom is null)
	and (Date_Time <= @dateTo or @dateTo is null)
	order by AffiliateTransactionID desc
) as t2
]]></sql>

        sql = sql.Replace("@ReferrerID", referrerID)

        Dim command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql)
        If (dateFrom.HasValue) Then
            command.Parameters.Add(New SqlClient.SqlParameter("@dateFrom", dateFrom))
        Else
            command.Parameters.Add(New SqlClient.SqlParameter("@dateFrom", System.DBNull.Value))
        End If
        If (dateTo.HasValue) Then
            command.Parameters.Add(New SqlClient.SqlParameter("@dateTo", dateTo))
        Else
            command.Parameters.Add(New SqlClient.SqlParameter("@dateTo", System.DBNull.Value))
        End If

        Dim dt As DataTable = DataHelpers.GetDataTable(command)

        Return dt
    End Function


End Class


<Serializable()> _
Public Class ReferrersRepository

    Dim dtReferrersData As DSCustom.ReferrersRepositoryDataDataTable
    Public ReadOnly Property DataTable As DSCustom.ReferrersRepositoryDataDataTable
        Get
            Return dtReferrersData
        End Get
    End Property

    Sub New()
        dtReferrersData = New DSCustom.ReferrersRepositoryDataDataTable()
    End Sub


    Public Function GetCreditsSum(profileID As Integer) As Double
        Dim crd As Double = 0
        Dim drs As DataRow() = dtReferrersData.Select("ProfileID=" & profileID)
        If (drs.Length > 0) Then crd = drs(0)("CreditsSum")
        Return crd
    End Function


    Public Function GetCreditsSumLevelsTotal(profileID As Integer) As Double
        Dim crd As Double = 0
        Dim drs As DataRow() = dtReferrersData.Select("ProfileID=" & profileID)
        If (drs.Length > 0) Then crd = drs(0)("CreditsSumLevelsTotal")
        Return crd
    End Function


    Public Function GetCommissionCreditsLevelsTotal(profileID As Integer) As Double
        Dim crd As Double = 0
        Dim drs As DataRow() = dtReferrersData.Select("ProfileID=" & profileID)
        If (drs.Length > 0) Then crd = drs(0)("CommissionCreditsLevelsTotal")
        Return crd
    End Function


    Public Function GetCreditsAvailableLevelsTotal(profileID As Integer) As Double
        Dim crd As Double = 0
        Dim drs As DataRow() = dtReferrersData.Select("ProfileID=" & profileID)
        If (drs.Length > 0) Then crd = drs(0)("CreditsAvailableLevelsTotal")
        Return crd
    End Function


    Public Function GetCommissionCreditsAvailableLevelsTotal(profileID As Integer) As Double
        Dim crd As Double = 0
        Dim drs As DataRow() = dtReferrersData.Select("ProfileID=" & profileID)
        If (drs.Length > 0) Then crd = drs(0)("CommissionCreditsAvailableLevelsTotal")
        Return crd
    End Function


    Public Function GetCreditsPendingLevelsTotal(profileID As Integer) As Double
        Dim crd As Double = 0
        Dim drs As DataRow() = dtReferrersData.Select("ProfileID=" & profileID)
        If (drs.Length > 0) Then crd = drs(0)("CreditsPendingLevelsTotal")
        Return crd
    End Function


    Public Function GetCommissionCreditsPendingLevelsTotal(profileID As Integer) As Double
        Dim crd As Double = 0
        Dim drs As DataRow() = dtReferrersData.Select("ProfileID=" & profileID)
        If (drs.Length > 0) Then crd = drs(0)("CommissionCreditsPendingLevelsTotal")
        Return crd
    End Function


    Public Sub AddRecord(ProfileID As Integer,
                        LoginName As String,
                        ParentId As Integer,
                        level As Integer,
                        Status As Integer,
                        CreditsSum As Double,
                        CommissionTotal As Double,
                        CreditsAvailable As Double,
                        CommissionAvailable As Double,
                        CreditsPending As Double,
                        CommissionPending As Double)

        Dim dr As DSCustom.ReferrersRepositoryDataRow = dtReferrersData.NewReferrersRepositoryDataRow()
        dr.ProfileID = ProfileID
        dr.LoginName = LoginName
        dr.ParentId = ParentId
        dr.ChildsCount = 0
        dr.Level = level
        dr.Status = Status

        dr.CreditsSum = CreditsSum
        dr.CreditsAvailable = CreditsAvailable
        dr.CreditsPending = CreditsPending

        dr.CreditsSumLevelsTotal = 0
        dr.CreditsAvailableLevelsTotal = 0
        dr.CreditsPendingLevelsTotal = 0

        dr.CommissionCredits = CommissionTotal
        dr.CommissionCreditsAvailable = CommissionAvailable
        dr.CommissionCreditsPending = CommissionPending

        dr.CommissionCreditsLevelsTotal = 0
        dr.CommissionCreditsAvailableLevelsTotal = 0
        dr.CommissionCreditsPendingLevelsTotal = 0
        dtReferrersData.AddReferrersRepositoryDataRow(dr)

        'Dim dr As DataRow = dtReferrersData.NewRow()
        'dr("ProfileID") = ProfileID
        'dr("LoginName") = LoginName

        'dr("CreditsSum") = CreditsSum
        'dr("CreditsSumLevelsTotal") = 0
        'dr("CommissionCredits") = CommissionTotal
        'dr("CommissionCreditsLevelsTotal") = 0

        'dr("ParentId") = ParentId
        'dr("ChildsCount") = 0
        'dr("Level") = level

        'dr("CreditsAvailable") = CreditsAvailable
        'dr("CreditsAvailableLevelsTotal") = 0
        'dr("CommissionCreditsAvailable") = CommissionAvailable
        'dr("CommissionCreditsAvailableLevelsTotal") = 0

        'dr("CreditsPending") = CreditsPending
        'dr("CreditsPendingLevelsTotal") = 0
        'dr("CommissionCreditsPending") = CommissionPending
        'dr("CommissionCreditsPendingLevelsTotal") = 0

        'dtReferrersData.Rows.Add(dr)
    End Sub


    Public Sub CalculateData(profileId As Integer)
        Dim drs As DataRow() = dtReferrersData.Select("ProfileID=" & profileId)
        CalculateDataInner(drs)
    End Sub


    Sub CalculateDataInner(drs As DataRow())


        For Each dr As DataRow In drs
            'Dim level As Integer = dr("Level")
            'dr("Level") = level + 1

            Dim drChildren As DataRow() = dtReferrersData.Select("ParentId=" & dr("ProfileID"))
            If (drChildren.Length > 0) Then
                CalculateDataInner(drChildren)
            End If


            Try

                Dim childCount As Integer = drChildren.Length
                For Each drInner As DataRow In drChildren
                    Dim currCount As Integer = drInner("ChildsCount")
                    childCount = childCount + currCount
                Next
                If (childCount > 0) Then dr("ChildsCount") = childCount

            Catch ex As Exception
                Throw
            End Try



            ' total credits
            Dim creditsCount As Double = dr("CreditsSum")
            For Each drInner As DataRow In drChildren
                Dim currCount As Double = drInner("CreditsSumLevelsTotal")
                creditsCount = creditsCount + currCount
            Next
            If (creditsCount > 0) Then dr("CreditsSumLevelsTotal") = creditsCount



            creditsCount = dr("CommissionCredits")
            For Each drInner As DataRow In drChildren
                Dim currCount As Double = drInner("CommissionCreditsLevelsTotal")
                creditsCount = creditsCount + currCount
            Next
            If (creditsCount > 0) Then dr("CommissionCreditsLevelsTotal") = creditsCount



            ' available credits
            creditsCount = dr("CreditsAvailable")
            For Each drInner As DataRow In drChildren
                Dim currCount As Double = drInner("CreditsAvailableLevelsTotal")
                creditsCount = creditsCount + currCount
            Next
            If (creditsCount > 0) Then dr("CreditsAvailableLevelsTotal") = creditsCount



            creditsCount = dr("CommissionCreditsAvailable")
            For Each drInner As DataRow In drChildren
                Dim currCount As Double = drInner("CommissionCreditsAvailableLevelsTotal")
                creditsCount = creditsCount + currCount
            Next
            If (creditsCount > 0) Then dr("CommissionCreditsAvailableLevelsTotal") = creditsCount



            ' pending credits
            creditsCount = dr("CreditsPending")
            For Each drInner As DataRow In drChildren
                Dim currCount As Double = drInner("CreditsPendingLevelsTotal")
                creditsCount = creditsCount + currCount
            Next
            If (creditsCount > 0) Then dr("CreditsPendingLevelsTotal") = creditsCount



            creditsCount = dr("CommissionCreditsPending")
            For Each drInner As DataRow In drChildren
                Dim currCount As Double = drInner("CommissionCreditsPendingLevelsTotal")
                creditsCount = creditsCount + currCount
            Next
            If (creditsCount > 0) Then dr("CommissionCreditsPendingLevelsTotal") = creditsCount

        Next

    End Sub


    Public Function GetReferrersForLevel(level As Integer) As DSCustom.ReferrersRepositoryDataRow()
        Dim drs As DSCustom.ReferrersRepositoryDataRow() = dtReferrersData.Select("Level=" & level)
        Return drs
    End Function

End Class
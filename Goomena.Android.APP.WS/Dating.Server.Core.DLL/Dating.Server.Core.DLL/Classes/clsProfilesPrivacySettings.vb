﻿Imports System.Data.SqlClient

Public Class clsProfilesPrivacySettings





    Public Shared Function Update_SuppressWarning_WhenReadingMessageFromDifferentCountry(ProfileID As Integer, SuppressWarning As Boolean) As Integer
        Dim result As Integer = 0
        Try

            Dim sql As String = <sql><![CDATA[
update [EUS_ProfilesPrivacySettings]
set [SupressWarning_WhenReadingMessageFromDifferentCountry] = @SupressWarning_WhenReadingMessageFromDifferentCountry
    ,SupressWarning_WhenReadingMessageFromDifferentCountryDateSet=getutcdate()
where [ProfileID]=@ProfileID or [MirrorProfileID]=@ProfileID

if(@@ROWCOUNT = 0)
begin

    declare @MirrorProfileID int
    select @MirrorProfileID=MirrorProfileID from EUS_Profiles where ProfileID=@ProfileID
    
    Insert into [EUS_ProfilesPrivacySettings]([ProfileID],MirrorProfileID,[SupressWarning_WhenReadingMessageFromDifferentCountry],[SupressWarning_WhenReadingMessageFromDifferentCountryDateSet])
    values (@ProfileID,@MirrorProfileID, @SupressWarning_WhenReadingMessageFromDifferentCountry,getutcdate())

end

]]></sql>
            Dim cmd As SqlCommand = DataHelpers.GetSqlCommand(sql)
            cmd.Parameters.AddWithValue("@ProfileID", ProfileID)
            cmd.Parameters.AddWithValue("@SupressWarning_WhenReadingMessageFromDifferentCountry", SuppressWarning)
            result = DataHelpers.ExecuteNonQuery(cmd)

        Catch ex As Exception
            Throw
        Finally

        End Try
        Return result
    End Function


    Public Shared Function Update_PrivacySettings_ShowMeOffline(ProfileID As Integer, ShowMeOffline As Boolean) As Integer
        Dim result As Integer = 0
        Try

            Dim sql As String = <sql><![CDATA[
update [EUS_ProfilesPrivacySettings]
set [PrivacySettings_ShowMeOffline] = @PrivacySettings_ShowMeOffline
    ,PrivacySettings_ShowMeOfflineDateSet=getutcdate()
where [ProfileID]=@ProfileID or 
    [MirrorProfileID]=@ProfileID

if(@@ROWCOUNT = 0)
begin

    declare @MirrorProfileID int
    select @MirrorProfileID=MirrorProfileID from EUS_Profiles where ProfileID=@ProfileID
    
    Insert into [EUS_ProfilesPrivacySettings]([ProfileID],MirrorProfileID,[PrivacySettings_ShowMeOffline],[PrivacySettings_ShowMeOfflineDateSet])
    values (@ProfileID,@MirrorProfileID, @PrivacySettings_ShowMeOffline,getutcdate())

end

]]></sql>
            Dim cmd As SqlCommand = DataHelpers.GetSqlCommand(sql)
            cmd.Parameters.AddWithValue("@ProfileID", ProfileID)
            cmd.Parameters.AddWithValue("@PrivacySettings_ShowMeOffline", ShowMeOffline)
            result = DataHelpers.ExecuteNonQuery(cmd)

        Catch ex As Exception
            Throw
        Finally

        End Try
        Return result
    End Function



    Public Shared Function Update_ItemsPerPage(ProfileID As Integer, ItemsPerPage As Integer) As Integer
        Dim result As Integer = 0
        Try

            Dim sql As String = <sql><![CDATA[
update [EUS_ProfilesPrivacySettings]
set [ItemsPerPage] = @ItemsPerPage
    ,ItemsPerPageDateSet=getutcdate()
where [ProfileID]=@ProfileID or 
    [MirrorProfileID]=@ProfileID

if(@@ROWCOUNT = 0)
begin

    declare @MirrorProfileID int
    select @MirrorProfileID=MirrorProfileID from EUS_Profiles where ProfileID=@ProfileID
    
    Insert into [EUS_ProfilesPrivacySettings]([ProfileID],MirrorProfileID,[ItemsPerPage],[ItemsPerPageDateSet])
    values (@ProfileID,@MirrorProfileID, @ItemsPerPage,getutcdate())

end

]]></sql>
            Dim cmd As SqlCommand = DataHelpers.GetSqlCommand(sql)
            cmd.Parameters.AddWithValue("@ProfileID", ProfileID)
            cmd.Parameters.AddWithValue("@ItemsPerPage", ItemsPerPage)
            result = DataHelpers.ExecuteNonQuery(cmd)

        Catch ex As Exception
            Throw
        Finally

        End Try
        Return result
    End Function


    Public Shared Function GET_SuppressWarning_WhenReadingMessageFromDifferentCountry(ProfileID As Integer) As Boolean
        Dim SuppressWarning As Boolean
        Try

            Dim sql As String = <sql><![CDATA[
select SuppressWarning = cast ( ISNULL((
    select top(1) SupressWarning_WhenReadingMessageFromDifferentCountry 
    from [EUS_ProfilesPrivacySettings]
    where [ProfileID]=@ProfileID or [MirrorProfileID]=@ProfileID)
    ,0) as bit)

]]></sql>
            Dim cmd As SqlCommand = DataHelpers.GetSqlCommand(sql)
            cmd.Parameters.AddWithValue("@ProfileID", ProfileID)
            'cmd.Parameters.AddWithValue("@SupressWarning_WhenReadingMessageFromDifferentCountry", SuppressWarning)
            Dim result As DataTable = DataHelpers.GetDataTable(cmd)
            If (result.Rows.Count > 0) Then
                SuppressWarning = result.Rows(0)("SuppressWarning")
            End If

        Catch ex As Exception
            Throw
        Finally

        End Try
        Return SuppressWarning
    End Function


    Public Shared Function GET_DisableReceivingMessagesFromDifferentCountry(ProfileID As Integer) As Boolean
        Dim DisableReceivingMessagesFromDifferentCountry As Boolean
        Try

            Dim sql As String = <sql><![CDATA[
select DisableReceivingMessagesFromDifferentCountry = cast ( ISNULL((
    select top(1) DisableReceivingMessagesFromDifferentCountry 
    from [EUS_ProfilesPrivacySettings]
    where [ProfileID]=@ProfileID or [MirrorProfileID]=@ProfileID)
    ,0) as bit)

]]></sql>
            Dim cmd As SqlCommand = DataHelpers.GetSqlCommand(sql)
            cmd.Parameters.AddWithValue("@ProfileID", ProfileID)
            Dim result As DataTable = DataHelpers.GetDataTable(cmd)
            If (result.Rows.Count > 0) Then
                DisableReceivingMessagesFromDifferentCountry = result.Rows(0)("DisableReceivingMessagesFromDifferentCountry")
            End If

        Catch ex As Exception
            Throw
        Finally

        End Try
        Return DisableReceivingMessagesFromDifferentCountry
    End Function

    Public Shared Function GET_DisableReceivingLikesFromDifferentCountry(ProfileID As Integer) As Boolean
        Dim DisableReceivingLikesFromDifferentCountry As Boolean
        Try

            Dim sql As String = <sql><![CDATA[
select DisableReceivingLikesFromDifferentCountry = cast ( ISNULL((
    select top(1) DisableReceivingLikesFromDifferentCountry 
    from [EUS_ProfilesPrivacySettings]
    where [ProfileID]=@ProfileID or [MirrorProfileID]=@ProfileID)
    ,0) as bit)

]]></sql>
            Dim cmd As SqlCommand = DataHelpers.GetSqlCommand(sql)
            cmd.Parameters.AddWithValue("@ProfileID", ProfileID)
            Dim result As DataTable = DataHelpers.GetDataTable(cmd)
            If (result.Rows.Count > 0) Then
                DisableReceivingLikesFromDifferentCountry = result.Rows(0)("DisableReceivingLikesFromDifferentCountry")
            End If

        Catch ex As Exception
            Throw
        Finally

        End Try
        Return DisableReceivingLikesFromDifferentCountry
    End Function


    Public Shared Function GET_DisableReceivingLIKESFromDifferentCountryFromTo(ToProfileID As Integer, FromProfileID As Integer) As Boolean
        Dim DisableReceivingLikesFromDifferentCountry As Boolean
        Try

            Dim sql As String = <sql><![CDATA[
declare @DisableReceivingLikesFromDifferentCountry bit


select @DisableReceivingLikesFromDifferentCountry = cast ( ISNULL((
    select top(1) DisableReceivingLikesFromDifferentCountry 
    from [EUS_ProfilesPrivacySettings]
    where [ProfileID]=@ToProfileID or [MirrorProfileID]=@ToProfileID)
    ,0) as bit)


-- if receiving of likes from different country is disabled
-- then check profiles' countries

if(@DisableReceivingLikesFromDifferentCountry=1)
begin

    select @DisableReceivingLikesFromDifferentCountry =case
				when count(*)>0 then 0
				else 1
			end
    from (
        select Country
        from dbo.EUS_Profiles 
        where [ProfileID]=@ToProfileID
    ) as t1,
    (
        select Country
        from dbo.EUS_Profiles 
        where [ProfileID]=@FromProfileID
    ) as t2
    where t1.Country = t2.Country
    
end

select @DisableReceivingLikesFromDifferentCountry as DisableReceivingLikesFromDifferentCountry

]]></sql>
            Dim cmd As SqlCommand = DataHelpers.GetSqlCommand(sql)
            cmd.Parameters.AddWithValue("@ToProfileID", ToProfileID)
            cmd.Parameters.AddWithValue("@FromProfileID", FromProfileID)
            Dim result As DataTable = DataHelpers.GetDataTable(cmd)
            If (result.Rows.Count > 0) Then
                DisableReceivingLikesFromDifferentCountry = result.Rows(0)("DisableReceivingLikesFromDifferentCountry")
            End If

        Catch ex As Exception
            Throw
        Finally

        End Try
        Return DisableReceivingLikesFromDifferentCountry
    End Function



    Public Shared Function GET_DisableReceivingOFFERSFromDifferentCountryFromTo(ToProfileID As Integer, FromProfileID As Integer) As Boolean
        Dim DisableReceivingOffersFromDifferentCountry As Boolean
        Try

            Dim sql As String = <sql><![CDATA[
declare @DisableReceivingOffersFromDifferentCountry bit


select @DisableReceivingOffersFromDifferentCountry = cast ( ISNULL((
    select top(1) DisableReceivingOffersFromDifferentCountry 
    from [EUS_ProfilesPrivacySettings]
    where [ProfileID]=@ToProfileID or [MirrorProfileID]=@ToProfileID)
    ,0) as bit)


-- if receiving of offers from different country is disabled
-- then check profiles' countries

if(@DisableReceivingOffersFromDifferentCountry=1)
begin

    select @DisableReceivingOffersFromDifferentCountry =case
				when count(*)>0 then 0
				else 1
			end
    from (
        select Country
        from dbo.EUS_Profiles 
        where [ProfileID]=@ToProfileID
    ) as t1,
    (
        select Country
        from dbo.EUS_Profiles 
        where [ProfileID]=@FromProfileID
    ) as t2
    where t1.Country = t2.Country
    
end

select @DisableReceivingOffersFromDifferentCountry as DisableReceivingOffersFromDifferentCountry

]]></sql>
            Dim cmd As SqlCommand = DataHelpers.GetSqlCommand(sql)
            cmd.Parameters.AddWithValue("@ToProfileID", ToProfileID)
            cmd.Parameters.AddWithValue("@FromProfileID", FromProfileID)
            Dim result As DataTable = DataHelpers.GetDataTable(cmd)
            If (result.Rows.Count > 0) Then
                DisableReceivingOffersFromDifferentCountry = result.Rows(0)("DisableReceivingOffersFromDifferentCountry")
            End If

        Catch ex As Exception
            Throw
        Finally

        End Try
        Return DisableReceivingOffersFromDifferentCountry
    End Function


    Public Shared Function GET_DisableReceivingMESSAGESFromDifferentCountryFromTo(ToProfileID As Integer, FromProfileID As Integer) As Boolean
        Dim DisableReceivingMessagesFromDifferentCountry As Boolean
        Try

            Dim sql As String = <sql><![CDATA[
declare @DisableReceivingMessagesFromDifferentCountry bit


select @DisableReceivingMessagesFromDifferentCountry = cast ( ISNULL((
    select top(1) DisableReceivingMessagesFromDifferentCountry 
    from [EUS_ProfilesPrivacySettings]
	where [ProfileID]=@ToProfileID or [MirrorProfileID]=@ToProfileID)
    ,0) as bit)


-- if receiving of offers from different country is disabled
-- then check profiles' countries

if(@DisableReceivingMessagesFromDifferentCountry=1)
begin

    select @DisableReceivingMessagesFromDifferentCountry =case
				when count(*)>0 then 0
				else 1
			end
    from (
        select Country
        from dbo.EUS_Profiles 
        where [ProfileID]=@ToProfileID
    ) as t1,
    (
        select Country
        from dbo.EUS_Profiles 
        where [ProfileID]=@FromProfileID
    ) as t2
    where t1.Country = t2.Country
    
end

select @DisableReceivingMessagesFromDifferentCountry as DisableReceivingMessagesFromDifferentCountry

]]></sql>
            Dim cmd As SqlCommand = DataHelpers.GetSqlCommand(sql)
            cmd.Parameters.AddWithValue("@ToProfileID", ToProfileID)
            cmd.Parameters.AddWithValue("@FromProfileID", FromProfileID)
            Dim result As DataTable = DataHelpers.GetDataTable(cmd)
            If (result.Rows.Count > 0) Then
                DisableReceivingMessagesFromDifferentCountry = result.Rows(0)("DisableReceivingMessagesFromDifferentCountry")
            End If

        Catch ex As Exception
            Throw
        Finally

        End Try
        Return DisableReceivingMessagesFromDifferentCountry
    End Function




    Public Shared Sub Update_AdminSetting_BlockMessagesToBeReceivedByOther(_CMSDBDataContext As CMSDBDataContext, ProfileID As Integer, MirrorProfileID As Integer, Enable As Boolean)

        Try
            If _CMSDBDataContext IsNot Nothing Then
                ' exception thrown when disposed
                Dim a As System.Data.Common.DbConnection = _CMSDBDataContext.Connection
            End If
        Catch ex As Exception
            _CMSDBDataContext = Nothing
        End Try

        If (_CMSDBDataContext Is Nothing) Then _CMSDBDataContext = New CMSDBDataContext(DataHelpers.ConnectionString)

        Try

            Dim asstt As EUS_ProfilesPrivacySetting = Nothing

            asstt = (From itm In _CMSDBDataContext.EUS_ProfilesPrivacySettings
                     Where itm.ProfileID = ProfileID OrElse itm.MirrorProfileID = ProfileID
                     Select itm).FirstOrDefault()

            If (asstt Is Nothing) Then
                asstt = New EUS_ProfilesPrivacySetting()
            End If

            asstt.ProfileID = ProfileID
            asstt.MirrorProfileID = MirrorProfileID
            asstt.AdminSetting_BlockMessagesToBeReceivedByOther = Enable
            asstt.AdminSetting_BlockMessagesToBeReceivedByOtherDateSet = DateTime.UtcNow

            _CMSDBDataContext.EUS_ProfilesPrivacySettings.InsertOnSubmit(asstt)
            _CMSDBDataContext.SubmitChanges()

        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

    End Sub



    Public Shared Function GET_AdminSetting_BlockMessagesToBeReceivedByOther(ProfileID As Integer) As Boolean
        Dim AdminSetting_BlockMessagesToBeReceivedByOther As Boolean
        Try

            Dim sql As String = <sql><![CDATA[
select AdminSetting_BlockMessagesToBeReceivedByOther = cast (ISNULL((
    select top(1) AdminSetting_BlockMessagesToBeReceivedByOther 
    from [EUS_ProfilesPrivacySettings]
    where [ProfileID]=@ProfileID or [MirrorProfileID]=@ProfileID)
    ,0) as bit)

]]></sql>
            Dim cmd As SqlCommand = DataHelpers.GetSqlCommand(sql)
            cmd.Parameters.AddWithValue("@ProfileID", ProfileID)
            Dim result As DataTable = DataHelpers.GetDataTable(cmd)
            If (result.Rows.Count > 0) Then
                AdminSetting_BlockMessagesToBeReceivedByOther = result.Rows(0)("AdminSetting_BlockMessagesToBeReceivedByOther")
            End If

        Catch ex As Exception
            Throw
        Finally

        End Try
        Return AdminSetting_BlockMessagesToBeReceivedByOther
    End Function



    Public Shared Function GET_AdminSetting_IsVirtual(ProfileID As Integer, Optional CheckSpanishWoman As Boolean = False) As Boolean
        Dim AdminSetting_IsVirtual As Boolean
        Try
            Dim sql As String = ""
            If (CheckSpanishWoman) Then

                '                sql = <sql><![CDATA[
                'declare @AdminSetting_IsVirtual bit;

                'select @AdminSetting_IsVirtual=AdminSetting_IsVirtual 
                'from [EUS_ProfilesPrivacySettings]
                'where [ProfileID]=@ProfileID or [MirrorProfileID]=@ProfileID);

                'if(@AdminSetting_IsVirtual is null)
                'begin
                '    select @AdminSetting_IsVirtual=() 
                '    from [EUS_ProfilesPrivacySettings]
                '    where [ProfileID]=@ProfileID or [MirrorProfileID]=@ProfileID);
                'end

                'select AdminSetting_IsVirtual = cast (ISNULL((@AdminSetting_IsVirtual),0) as bit);
                ']]></sql>

                sql = <sql><![CDATA[
select AdminSetting_IsVirtual = cast (ISNULL((
    select top(1) AdminSetting_IsVirtual 
    from [EUS_ProfilesPrivacySettings]
    where [ProfileID]=@ProfileID or [MirrorProfileID]=@ProfileID)
    ,0) as bit)
]]></sql>

            Else

                sql = <sql><![CDATA[
select AdminSetting_IsVirtual = cast (ISNULL((
    select top(1) AdminSetting_IsVirtual 
    from [EUS_ProfilesPrivacySettings]
    where [ProfileID]=@ProfileID or [MirrorProfileID]=@ProfileID)
    ,0) as bit)
]]></sql>

            End If



            Dim cmd As SqlCommand = DataHelpers.GetSqlCommand(sql)
            cmd.Parameters.AddWithValue("@ProfileID", ProfileID)
            Dim result As DataTable = DataHelpers.GetDataTable(cmd)
            If (result.Rows.Count > 0) Then
                AdminSetting_IsVirtual = result.Rows(0)("AdminSetting_IsVirtual")
            End If

        Catch ex As Exception
            Throw
        Finally

        End Try
        Return AdminSetting_IsVirtual
    End Function



    Public Shared Function GET_PrivacySettings_ShowMeOffline(ProfileID As Integer) As Boolean
        Dim PrivacySettings_ShowMeOffline As Boolean
        Try

            Dim sql As String = <sql><![CDATA[
select PrivacySettings_ShowMeOffline = cast (ISNULL((
    select top(1) PrivacySettings_ShowMeOffline 
    from [EUS_ProfilesPrivacySettings]
    where (ProfileID=@ProfileID OR [MirrorProfileID]=@ProfileID)
	and [PrivacySettings_ShowMeOffline]=1)
,0) as bit)
]]></sql>
            Dim cmd As SqlCommand = DataHelpers.GetSqlCommand(sql)
            cmd.Parameters.AddWithValue("@ProfileID", ProfileID)
            Dim result As DataTable = DataHelpers.GetDataTable(cmd)
            If (result.Rows.Count > 0) Then
                PrivacySettings_ShowMeOffline = result.Rows(0)("PrivacySettings_ShowMeOffline")
            End If

        Catch ex As Exception
            Throw
        Finally

        End Try
        Return PrivacySettings_ShowMeOffline
    End Function



    Public Shared Function Update_NearestMembersOptions(ProfileID As Integer, NearestMembersOptions As Integer) As Integer
        Dim result As Integer = 0
        Try

            Dim sql As String = <sql><![CDATA[
    update [EUS_ProfilesPrivacySettings]
    set [NearestMembersOptions] = @NearestMembersOptions
        ,NearestMembersOptionsDateSet=getutcdate()
    where [ProfileID]=@ProfileID or 
        [MirrorProfileID]=@ProfileID

    if(@@ROWCOUNT = 0)
    begin

        declare @MirrorProfileID int
        select @MirrorProfileID=MirrorProfileID from EUS_Profiles where ProfileID=@ProfileID

        Insert into [EUS_ProfilesPrivacySettings]([ProfileID],MirrorProfileID,[NearestMembersOptions],[NearestMembersOptionsDateSet])
        values (@ProfileID,@MirrorProfileID, @NearestMembersOptions,getutcdate())

    end

    ]]></sql>
            Dim cmd As SqlCommand = DataHelpers.GetSqlCommand(sql)
            cmd.Parameters.AddWithValue("@ProfileID", ProfileID)
            cmd.Parameters.AddWithValue("@NearestMembersOptions", NearestMembersOptions)
            result = DataHelpers.ExecuteNonQuery(cmd)

        Catch ex As Exception
            Throw
        Finally

        End Try
        Return result
    End Function

    Public Shared Function GET_NearestMembersOptions(ProfileID As Integer) As Integer
        Dim NearestMembersOptions As Integer
        Try

            Dim sql As String = <sql><![CDATA[
select NearestMembersOptions = cast (ISNULL((
    select top(1) NearestMembersOptions 
    from [EUS_ProfilesPrivacySettings]
    where (ProfileID=@ProfileID OR [MirrorProfileID]=@ProfileID)),0) as int)
]]></sql>
            Dim cmd As SqlCommand = DataHelpers.GetSqlCommand(sql)
            cmd.Parameters.AddWithValue("@ProfileID", ProfileID)
            Dim result As DataTable = DataHelpers.GetDataTable(cmd)
            If (result.Rows.Count > 0) Then
                NearestMembersOptions = result.Rows(0)("NearestMembersOptions")
            End If

        Catch ex As Exception
            Throw
        Finally

        End Try
        Return NearestMembersOptions
    End Function


    Public Shared Function Update_ProfilesViewedOptions(ProfileID As Integer, ProfilesViewedOptions As Integer) As Integer
        Dim result As Integer = 0
        Try

            Dim sql As String = <sql><![CDATA[
    update [EUS_ProfilesPrivacySettings]
    set [ProfilesViewedOptions] = @ProfilesViewedOptions
        ,ProfilesViewedOptionsDateSet=getutcdate()
    where [ProfileID]=@ProfileID or 
        [MirrorProfileID]=@ProfileID

    if(@@ROWCOUNT = 0)
    begin

        declare @MirrorProfileID int
        select @MirrorProfileID=MirrorProfileID from EUS_Profiles where ProfileID=@ProfileID

        Insert into [EUS_ProfilesPrivacySettings]([ProfileID],MirrorProfileID,[ProfilesViewedOptions],[ProfilesViewedOptionsDateSet])
        values (@ProfileID,@MirrorProfileID, @ProfilesViewedOptions,getutcdate())

    end

    ]]></sql>
            Dim cmd As SqlCommand = DataHelpers.GetSqlCommand(sql)
            cmd.Parameters.AddWithValue("@ProfileID", ProfileID)
            cmd.Parameters.AddWithValue("@ProfilesViewedOptions", ProfilesViewedOptions)
            result = DataHelpers.ExecuteNonQuery(cmd)

        Catch ex As Exception
            Throw
        Finally

        End Try
        Return result
    End Function

    Public Shared Function GET_ProfilesViewedOptions(ProfileID As Integer) As Integer
        Dim ProfilesViewedOptions As Integer
        Try

            Dim sql As String = <sql><![CDATA[
select ProfilesViewedOptions = cast (ISNULL((
    select top(1) ProfilesViewedOptions 
    from [EUS_ProfilesPrivacySettings]
    where (ProfileID=@ProfileID OR [MirrorProfileID]=@ProfileID)),0) as int)
]]></sql>
            Dim cmd As SqlCommand = DataHelpers.GetSqlCommand(sql)
            cmd.Parameters.AddWithValue("@ProfileID", ProfileID)
            Dim result As DataTable = DataHelpers.GetDataTable(cmd)
            If (result.Rows.Count > 0) Then
                ProfilesViewedOptions = result.Rows(0)("ProfilesViewedOptions")
            End If

        Catch ex As Exception
            Throw
        Finally

        End Try
        Return ProfilesViewedOptions
    End Function

    Public Shared Function GET_ItemsPerPage(ProfileID As Integer) As Integer
        Dim ItemsPerPage As Integer
        Try

            Dim sql As String = <sql><![CDATA[
select ItemsPerPage = cast (ISNULL((
    select top(1) ItemsPerPage 
    from [EUS_ProfilesPrivacySettings]
    where (ProfileID=@ProfileID OR [MirrorProfileID]=@ProfileID)),0) as int)
]]></sql>
            Dim cmd As SqlCommand = DataHelpers.GetSqlCommand(sql)
            cmd.Parameters.AddWithValue("@ProfileID", ProfileID)
            Dim result As DataTable = DataHelpers.GetDataTable(cmd)
            If (result.Rows.Count > 0) Then
                ItemsPerPage = result.Rows(0)("ItemsPerPage")
            End If

        Catch ex As Exception
            Throw
        Finally

        End Try
        Return ItemsPerPage
    End Function


    Public Shared Function GET_NotificationsSettings_WhenMyProfileVisited(ProfileID As Integer) As Boolean
        Dim result As Boolean
        Try

            ' default value is TRUE, 
            ' NULL value => TRUE

            Dim sql As String = <sql><![CDATA[
select v = cast (ISNULL((
    select top(1) NotificationsSettings_WhenMyProfileVisited 
    from [EUS_ProfilesPrivacySettings]
    where (ProfileID=@ProfileID OR [MirrorProfileID]=@ProfileID))
,1) as bit)
]]></sql>
            Dim cmd As SqlCommand = DataHelpers.GetSqlCommand(sql)
            cmd.Parameters.AddWithValue("@ProfileID", ProfileID)
            Dim dt As DataTable = DataHelpers.GetDataTable(cmd)
            If (dt.Rows.Count > 0) Then
                result = dt.Rows(0)("v")
            End If

        Catch ex As Exception
            Throw
        Finally

        End Try
        Return result
    End Function


    Public Shared Function GET_NotificationsSettings_SendMeAboutUnvisitedProfiles(ProfileID As Integer) As Boolean
        Dim result As Boolean
        Try

            ' default value is TRUE, 
            ' NULL value => TRUE

            Dim sql As String = <sql><![CDATA[
select v = cast (ISNULL((
    select top(1) NotificationsSettings_SendMeAboutUnvisitedProfiles 
    from [EUS_ProfilesPrivacySettings]
    where (ProfileID=@ProfileID OR [MirrorProfileID]=@ProfileID))
,1) as bit)
]]></sql>
            Dim cmd As SqlCommand = DataHelpers.GetSqlCommand(sql)
            cmd.Parameters.AddWithValue("@ProfileID", ProfileID)
            Dim dt As DataTable = DataHelpers.GetDataTable(cmd)
            If (dt.Rows.Count > 0) Then
                result = dt.Rows(0)("v")
            End If

        Catch ex As Exception
            Throw
        Finally

        End Try
        Return result
    End Function

End Class

﻿Imports System
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.IO
Imports System.IO.Compression


Public Class AdminWSClient_SoapExtension
    Inherits SoapExtension

    Private OriginalStream As Stream
    Private NewStream As Stream

    Public Overloads Overrides Function GetInitializer(ByVal methodInfo As LogicalMethodInfo, ByVal attribute As SoapExtensionAttribute) As Object
        Return System.DBNull.Value
    End Function

    Public Overloads Overrides Function GetInitializer(ByVal WebServiceType As Type) As Object
        Return System.DBNull.Value
    End Function

    Public Overrides Sub Initialize(ByVal initializer As Object)
    End Sub


    'Private ApplyComression As Boolean
    'Private CurrentServiceURL As String

    Private Shared ServicesCompressionEnabled As Dictionary(Of String, Boolean)

    Shared Sub New()
        ServicesCompressionEnabled = New Dictionary(Of String, Boolean)
    End Sub

    Public Overrides Sub ProcessMessage(ByVal message As SoapMessage)
        'ApplyComression = False
        'CurrentServiceURL = DirectCast(message, System.Web.Services.Protocols.SoapClientMessage).Url
        'If (CurrentServiceURL.ToLower().EndsWith("admin.asmx") OrElse
        '    CurrentServiceURL.ToLower().EndsWith("adminreferrers.asmx")) Then

        '    ApplyComression = True

        Select Case message.Stage

            Case SoapMessageStage.BeforeSerialize

            Case SoapMessageStage.AfterSerialize
                AfterSerialize(message)

            Case SoapMessageStage.BeforeDeserialize
                BeforeDeserialize(message)

            Case SoapMessageStage.AfterDeserialize

                'Case Else
                'Throw New Exception("invalid stage")
        End Select
        'End If

    End Sub

    ' Save the stream representing the SOAP request or SOAP response into a
    ' local memory buffer.
    Public Overrides Function ChainStream(ByVal stream As Stream) As Stream
        OriginalStream = stream
        ' If (ApplyComression AndAlso CurrentServiceURL IsNot Nothing) Then
        NewStream = New MemoryStream()
        Return NewStream
        'Else
        'Return stream
        'End If
    End Function

    ' Write the compressed SOAP message out to a file at
    'the server's file system..
    Public Sub AfterSerialize(ByVal message As SoapMessage)
        'If (NewStream Is Nothing) Then NewStream = New MemoryStream()

        Dim ms As New MemoryStream()
        NewStream.Position = 0
        Copy(NewStream, ms)
        NewStream.Position = 0
        ms.Position = 0

        If (ServicesCompressionEnabled.ContainsKey(message.Url) AndAlso ServicesCompressionEnabled(message.Url) = True) Then
            ms = CompressData(ms)
            ms.Position = 0
            NewStream.Position = 0
        End If

        Copy(ms, OriginalStream)
        ms.Position = 0

        'NewStream.Position = 0
        'Dim fs As New FileStream("c:\temp\server_soap.txt", FileMode.Append, FileAccess.Write)
        'Dim w As New StreamWriter(fs)
        'w.WriteLine("-----Response at " + DateTime.Now.ToString())
        'w.Flush()
        ''Compress stream and save it to a file
        'Comp(NewStream, fs)
        'w.Close()
        'NewStream.Position = 0
        ''Compress stream and send it to the wire
        'NewStream = CompressData(networkStream)
        '' Comp(newStream, networkStream)
    End Sub

    ' Write the SOAP request message out to a file at the server's file system.
    Public Sub BeforeDeserialize(ByVal message As SoapMessage)

        Dim msLengthOnStart As Long = 0
        Dim ms As Stream = New MemoryStream()
        Copy(OriginalStream, ms)
        ms.Position = 0

        If (Not ServicesCompressionEnabled.ContainsKey(message.Url) OrElse ServicesCompressionEnabled(message.Url) = True) Then
            msLengthOnStart = ms.Length
            ms = DeCompressData(ms)
            ms.Position = 0
            NewStream.Position = 0
        End If

        Copy(ms, NewStream)
        ms.Position = 0
        NewStream.Position = 0
        'ms.Position = 0
        'NewStream.Position = 0

        'Dim originalLength As Long = OriginalStream.Length
        'Dim decompressedLength As Long = NewStream.Length
        If (Not ServicesCompressionEnabled.ContainsKey(message.Url)) Then
            If (msLengthOnStart <> NewStream.Length) Then
                ServicesCompressionEnabled.Add(message.Url, True)
            Else
                ServicesCompressionEnabled.Add(message.Url, False)
            End If
        End If

        'Copy(networkStream, NewStream)
        'Dim fs As New FileStream("c:\temp\server_soap.txt", _
        'FileMode.Create, FileAccess.Write)
        'Dim w As New StreamWriter(fs)
        'w.WriteLine("----- Request at " + DateTime.Now.ToString())
        'w.Flush()
        'NewStream.Position = 0
        'Copy(NewStream, fs)
        'w.Close()
        'NewStream.Position = 0
    End Sub

    Sub Copy(ByRef fromStream As Stream, ByRef toStream As Stream)
        'Dim reader As New StreamReader(fromStream)
        'Dim writer As New StreamWriter(toStream)
        'writer.WriteLine(reader.ReadToEnd())
        'writer.Flush()

        ' fromStream.Position = 0
        Dim bytesRead As Integer
        Dim buffer As Byte() = New Byte(1) {}
        Dim reader As New BinaryReader(fromStream)
        Dim writer As New BinaryWriter(toStream)
        Do
            bytesRead = reader.Read(buffer, 0, buffer.Length)
            writer.Write(buffer, 0, bytesRead)
        Loop While bytesRead > 0
        writer.Flush()

    End Sub

    'Sub Comp(ByRef fromStream As Stream, ByRef toStream As Stream)
    '    Dim reader As New StreamReader(fromStream)
    '    Dim writer As New StreamWriter(toStream)
    '    Dim test1 As String
    '    Dim test2 As String
    '    'test1 = reader.ReadToEnd
    '    'String compression using NZIPLIB
    '    test2 = CompressData(fromStream)
    '    writer.WriteLine(test2)
    '    writer.Flush()
    'End Sub


    Public Function CompressData(source As MemoryStream) As MemoryStream
        If source Is Nothing Then
            Return Nothing
        End If

        Dim ms As New MemoryStream()
        Dim compressedzipStream As GZipStream = Nothing
        Try
            Dim buffer As Byte() = New Byte(source.Length - 1) {}
            source.Position = 0
            source.Read(buffer, 0, buffer.Length)
            compressedzipStream = New GZipStream(ms, CompressionMode.Compress, True)
            compressedzipStream.Write(buffer, 0, buffer.Length)
        Finally
            compressedzipStream.Close()
        End Try

        Return ms
    End Function



    Public Function DeCompressData(inputStream As Stream) As MemoryStream
        If inputStream Is Nothing Then
            Return Nothing
        End If
        inputStream.Position = 0

        Dim gs As New GZipStream(inputStream, CompressionMode.Decompress)
        Dim reader As New BinaryReader(gs)
        Dim result As New MemoryStream()
        Dim writer As New BinaryWriter(result)
        Dim bytesRead As Integer
        Dim buffer As Byte() = New Byte(1) {}

        Try
            Do
                bytesRead = reader.Read(buffer, 0, buffer.Length)
                writer.Write(buffer, 0, bytesRead)
            Loop While bytesRead > 0
            writer.Flush()
            result.Position = 0
        Catch ex As System.IO.InvalidDataException
            Return inputStream
        Finally
        End Try

        Return result
    End Function


End Class



' Create a SoapExtensionAttribute for the SOAP extension that can be
' applied to an XML Web service method.
<AttributeUsage(AttributeTargets.Method)> _
Public Class AdminWSClient_SoapExtensionAttribute
    Inherits SoapExtensionAttribute

    Public Overrides ReadOnly Property ExtensionType() As Type
        Get
            Return GetType(AdminWSClient_SoapExtensionAttribute)
        End Get
    End Property

    Public Overrides Property Priority() As Integer
        Get
            Return 1
        End Get
        Set(ByVal Value As Integer)
        End Set
    End Property

End Class
﻿Imports System.Text.RegularExpressions
Imports System.Text


Public Class clsMessagesHelperParameters
    Public Property CurrentProfileId As Integer
    Public Property MessagesView As MessagesViewEnum
    Public Property cmdFilter As String = Nothing
    Public Property showActive As Boolean? = Nothing
    Public Property NumberOfRecordsToReturn As Integer = 0
    Public Property performCount As Boolean
    Public Property rowNumberMin As Integer = 0
    Public Property rowNumberMax As Integer = 0
    Public Property AdditionalWhereClause As String = ""
    Public Property MessagesSort As MessagesSortEnum

    'Public Property Country As String
    'Public Property zipstr As String
    'Public Property Distance As Integer
    'Public Property LookingFor_ToMeetMaleID As Boolean
    'Public Property LookingFor_ToMeetFemaleID As Boolean
    'Public Property latitudeIn As Double?
    'Public Property longitudeIn As Double?
End Class

Public Class clsMessagesHelper

    Enum enmMessagesSqlType
        None = 0
        Counter = 1
        Union = 2
        UnionCounter = 4

        CounterFast = 8
        UnionFast = 16

    End Enum


    Public Shared Function GetNewMessagesQuick2(CurrentProfileId As Integer, ReturnRecordsWithStatus As Integer, NumberOfRecordsToReturn As Integer) As DataSet
        Dim ds As New DataSet()
        Dim __logdate As DateTime = DateTime.UtcNow

        Try
            Dim sql As String = "EXEC GetNewMessagesQuick2 @CurrentProfileId=@CurrentProfileId, @ReturnRecordsWithStatus=@ReturnRecordsWithStatus, @NumberOfRecordsToReturn=@NumberOfRecordsToReturn"
            Dim cmd As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql)
            cmd.Parameters.AddWithValue("@CurrentProfileId", CurrentProfileId)
            cmd.Parameters.AddWithValue("@ReturnRecordsWithStatus", ReturnRecordsWithStatus)
            cmd.Parameters.AddWithValue("@NumberOfRecordsToReturn", NumberOfRecordsToReturn)
            ds = DataHelpers.GetDataSet(cmd)
        Catch ex As Exception
            Throw
        Finally

            clsLogger.InsertLog("GetNewMessagesQuick2", CurrentProfileId, (DateTime.UtcNow - __logdate).TotalMilliseconds)
        End Try

        Return ds
    End Function


    Public Shared Function GetConversationWithMessage(CurrentProfileId As Integer, messageID As Integer) As DataSet
        Dim ds As New DataSet()
        Dim __logdate As DateTime = DateTime.UtcNow

        Try
            Dim sql As String = "EXEC GetConversationWithMessage @ownerProfileID=@ownerProfileID, @messageID=@messageID"
            Dim cmd As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql)
            cmd.Parameters.AddWithValue("@ownerProfileID", CurrentProfileId)
            cmd.Parameters.AddWithValue("@messageID", messageID)
            ds = DataHelpers.GetDataSet(cmd)
        Catch ex As Exception
            Throw
        Finally

            clsLogger.InsertLog("GetConversationWithMessage", CurrentProfileId, (DateTime.UtcNow - __logdate).TotalMilliseconds)
        End Try

        Return ds
    End Function



    Public Shared Function GetMessagesViewEnum(IsReceived As Boolean, statusID As Integer) As MessagesViewEnum
        Dim MessagesView As MessagesViewEnum = MessagesViewEnum.NEWMESSAGES

        If (IsReceived = True AndAlso statusID = 0) Then
            MessagesView = MessagesViewEnum.NEWMESSAGES

        ElseIf (IsReceived = True AndAlso statusID = 1) Then
            MessagesView = MessagesViewEnum.INBOX

        ElseIf (IsReceived = False) Then
            MessagesView = MessagesViewEnum.SENT

        End If
        Return MessagesView
    End Function


    Shared Function GetINBOXMessagesSQL(MessagesSqlType As enmMessagesSqlType)
        Dim sql As String = ""
        If (MessagesSqlType = enmMessagesSqlType.Counter) Then

            sql = <sql><![CDATA[
-- INBOXMessages count

select 
    CountMsgs= count (distinct prof.ProfileID)
from EUS_Messages msg
    with (nolock)
inner join EUS_Profiles prof on prof.ProfileId = msg.FromProfileID
inner join (
	select MAX(msg1.DateTimeToCreate) as MAXDateTimeToCreate,msg1.FromProfileID
		from EUS_Messages  msg1
        with (nolock)
		where	msg1.ToProfileID=@CurrentProfileId
		and msg1.IsReceived=1   -- incoming
        and msg1.statusID=1     -- read
        and ISNULL(msg1.IsHidden,0)=0
		group by msg1.FromProfileID
	)  as msg1 on msg1.MAXDateTimeToCreate= msg.DateTimeToCreate and prof.ProfileId = msg1.FromProfileID
LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = prof.ProfileID AND phot.CustomerPhotosID = prof.DefPhotoID
where msg.ToProfileID=@CurrentProfileId
and msg.IsReceived=1 and msg.statusID=1  -- incoming, read
and ISNULL(msg.IsHidden,0)=0
AND  not exists(
    select * 
	from EUS_ProfilesBlocked bl 
        with (nolock)
	where (bl.FromProfileID =prof.ProfileID and bl.ToProfileID = @CurrentProfileId) or
		(bl.FromProfileID =@CurrentProfileId and bl.ToProfileID = prof.ProfileID)
)
and (
    (@showActive is null) or
    (@showActive=1 and prof.Status=4) or
    (@showActive=0 and prof.Status<>4) 
)

[AdditionalWhereClause]

]]></sql>.Value


        ElseIf (MessagesSqlType = enmMessagesSqlType.CounterFast) Then

            sql = <sql><![CDATA[
-- INBOXMessages CounterFast

select 
    CountMsgs= count (distinct prof.ProfileID)
from EUS_Messages msg
    with (nolock)
inner join EUS_Profiles prof on prof.ProfileId = msg.FromProfileID
inner join (
	select MAX(msg1.DateTimeToCreate) as MAXDateTimeToCreate,msg1.FromProfileID
		from EUS_Messages  msg1
        with (nolock)
		where	msg1.ToProfileID=@CurrentProfileId
		and msg1.IsReceived=1   -- incoming
        and msg1.statusID=1     -- read
        and ISNULL(msg1.IsHidden,0)=0
		group by msg1.FromProfileID
	)  as msg1 on msg1.MAXDateTimeToCreate= msg.DateTimeToCreate and prof.ProfileId = msg1.FromProfileID
where msg.ToProfileID=@CurrentProfileId
and msg.IsReceived=1 and msg.statusID=1  -- incoming, read
and ISNULL(msg.IsHidden,0)=0
AND  not exists(
    select * 
	from EUS_ProfilesBlocked bl 
        with (nolock)
	where (bl.FromProfileID =prof.ProfileID and bl.ToProfileID = @CurrentProfileId) or
		(bl.FromProfileID =@CurrentProfileId and bl.ToProfileID = prof.ProfileID)
)
and (
    (@showActive is null) or
    (@showActive=1 and prof.Status=4) or
    (@showActive=0 and prof.Status<>4) 
)

[AdditionalWhereClause]

]]></sql>.Value


        ElseIf (MessagesSqlType = enmMessagesSqlType.Union) Then


            sql = <sql><![CDATA[
-- INBOXMessages

select 
    tRowNumber.* 
	,messageData_StatusID = isnull((select top(1) StatusID from EUS_Messages where ISNULL(EUS_Messages.IsHidden,0)=0 and eus_messageid=tRowNumber.EUS_MessageID),0)

	,CommunicationStatus=ISNULL((
		select top(1) UnlockedConversationId
		from EUS_UnlockedConversations  con
        with (nolock)
		where	(con.ToProfileID = tRowNumber.ProfileId and con.FromProfileID =@CurrentProfileId) or
				(con.ToProfileID =@CurrentProfileId and con.FromProfileID = tRowNumber.ProfileId)
	),0)	 
	
	,ItemsCount=ISNULL((select COUNT(*) 
		from EUS_Messages  msg1
        with (nolock)
		where	msg1.ToProfileID=@CurrentProfileId
		and msg1.IsReceived=1	-- incoming
		and msg1.StatusID=1	-- read
		and msg1.FromProfileID=tRowNumber.ProfileId
        and ISNULL(msg1.IsHidden,0)=0
		group by msg1.FromProfileID 
	), 0)
	
	
	,OfferAmount=ISNULL((select  top(1)  Amount
		from eus_offers  ofr
        with (nolock)
		where ofr.StatusID=50 and (
			(ofr.ToProfileID = tRowNumber.ProfileId and ofr.FromProfileID =@CurrentProfileId) or
			(ofr.ToProfileID =@CurrentProfileId and ofr.FromProfileID = tRowNumber.ProfileId)
		)
	), 0)

from
(
        select distinct
	        msg.EUS_MessageID,
	        msg.Subject,
	        msg1.MAXDateTimeToCreate, 
	
	
	        ItemsCountRead = 0,
	
            IsReceived = 1,
            StatusID = 1,

	        prof.ProfileID, prof.IsMaster, prof.MirrorProfileID, prof.Status, prof.LoginName, 
            prof.Password, prof.FirstName, prof.LastName, prof.GenderId, prof.Country, prof.Region, 
	        prof.City, prof.Zip, prof.CityArea, prof.Address, prof.Telephone, prof.eMail, 
            prof.Cellular, prof.AreYouWillingToTravel, prof.AboutMe_Heading, 
	        prof.AboutMe_DescribeYourself, prof.AboutMe_DescribeAnIdealFirstDate, prof.OtherDetails_EducationID, 
            prof.OtherDetails_AnnualIncomeID, prof.OtherDetails_NetWorthID, prof.OtherDetails_Occupation, prof.PersonalInfo_HeightID, 
            prof.PersonalInfo_BodyTypeID, prof.PersonalInfo_EyeColorID, prof.PersonalInfo_HairColorID, prof.PersonalInfo_ChildrenID, 
            prof.PersonalInfo_EthnicityID, prof.PersonalInfo_ReligionID, prof.PersonalInfo_SmokingHabitID, 
	        prof.PersonalInfo_DrinkingHabitID, prof.LookingFor_ToMeetMaleID, prof.LookingFor_ToMeetFemaleID, 
            prof.LookingFor_RelationshipStatusID, 
	        prof.LookingFor_TypeOfDating_ShortTermRelationship, 
            prof.LookingFor_TypeOfDating_Friendship, 
            prof.LookingFor_TypeOfDating_LongTermRelationship, 
            prof.LookingFor_TypeOfDating_MutuallyBeneficialArrangements, 
            prof.LookingFor_TypeOfDating_MarriedDating, 
            prof.LookingFor_TypeOfDating_AdultDating_Casual, 
            prof.DateTimeToRegister, 
            prof.RegisterIP, 
            prof.LastLoginDateTime, 
            prof.LastUpdateProfileDateTime, 
            prof.LAGID, 
            prof.Birthday, 
            prof.IsOnline, 
            prof.LastActivityDateTime
	        ,[HasPhoto] = case 
			        when prof.PhotosPublic > 0  then 1
			        when prof.PhotosPrivate > 0  then 1
			        when phot.[CustomerID] IS NULL  then 0
			        else 1 
		        end
	        ,phot.CustomerID
            ,phot.FileName 
        from EUS_Messages msg
            with (nolock)
        inner join EUS_Profiles prof on prof.ProfileId = msg.FromProfileID
        inner join (
	        select MAX(msg1.DateTimeToCreate) as MAXDateTimeToCreate,msg1.FromProfileID
		        from EUS_Messages  msg1
                with (nolock)
		        where	msg1.ToProfileID=@CurrentProfileId
		        and msg1.IsReceived=1   -- incoming
                and msg1.statusID=1     -- read
                and ISNULL(msg1.IsHidden,0)=0
		        group by msg1.FromProfileID
	        )  as msg1 on msg1.MAXDateTimeToCreate= msg.DateTimeToCreate and prof.ProfileId = msg1.FromProfileID
        LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = prof.ProfileID AND phot.CustomerPhotosID = prof.DefPhotoID
        where msg.ToProfileID=@CurrentProfileId
        and msg.IsReceived=1 and msg.statusID=1  -- incoming, read
        and ISNULL(msg.IsHidden,0)=0
        AND  not exists(
            select * 
	        from EUS_ProfilesBlocked bl 
                with (nolock)
	        where (bl.FromProfileID =prof.ProfileID and bl.ToProfileID = @CurrentProfileId) or
		        (bl.FromProfileID =@CurrentProfileId and bl.ToProfileID = prof.ProfileID)
        )
        and (
            (@showActive is null) or
            (@showActive=1 and prof.Status=4) or
            (@showActive=0 and prof.Status<>4) 
        )

[AdditionalWhereClause]

) as tRowNumber 

]]></sql>.Value


        ElseIf (MessagesSqlType = enmMessagesSqlType.UnionFast) Then


            sql = <sql><![CDATA[
-- INBOXMessages UnionFast

select 
    tRowNumber.* 
	,messageData_StatusID = isnull((select top(1) StatusID from EUS_Messages where ISNULL(EUS_Messages.IsHidden,0)=0 and eus_messageid=tRowNumber.EUS_MessageID),0),
	prof.ProfileID, prof.IsMaster, prof.MirrorProfileID, prof.Status, prof.LoginName, 
    prof.GenderId, prof.Country, prof.Region, 
	prof.City, prof.Zip,
    prof.Birthday, 
    prof.IsOnline, 
    prof.LastActivityDateTime
	,[HasPhoto] = case 
			when prof.PhotosPublic > 0  then 1
			when prof.PhotosPrivate > 0  then 1
			when phot.[CustomerID] IS NULL  then 0
			else 1 
		end
	,phot.CustomerID
    ,phot.FileName 

	,CommunicationStatus=ISNULL((
		select top(1) UnlockedConversationId
		from EUS_UnlockedConversations  con
        with (nolock)
		where	(con.ToProfileID = prof.ProfileId and con.FromProfileID =@CurrentProfileId) or
				(con.ToProfileID =@CurrentProfileId and con.FromProfileID = prof.ProfileId)
	),0)	 
	
	,ItemsCount=ISNULL((select COUNT(*) 
		from EUS_Messages  msg1
        with (nolock)
		where	msg1.ToProfileID=@CurrentProfileId
		and msg1.IsReceived=1	-- incoming
		and msg1.StatusID=1	-- read
		and msg1.FromProfileID=prof.ProfileId
        and ISNULL(msg1.IsHidden,0)=0
		group by msg1.FromProfileID 
	), 0)
	
	
	,OfferAmount=ISNULL((select  top(1)  Amount
		from eus_offers  ofr
        with (nolock)
		where ofr.StatusID=50 and (
			(ofr.ToProfileID = prof.ProfileId and ofr.FromProfileID =@CurrentProfileId) or
			(ofr.ToProfileID =@CurrentProfileId and ofr.FromProfileID = prof.ProfileId)
		)
	), 0)

from
(
    select distinct
	    msg.EUS_MessageID,
	    msg.Subject,
	    msg.ToProfileID,
	    msg.FromProfileID,
	    msg1.MAXDateTimeToCreate, 
	    ItemsCountRead = 0,
        IsReceived = 1,
        StatusID = 1
    from EUS_Messages msg
        with (nolock)
    inner join EUS_Profiles prof on prof.ProfileId = msg.FromProfileID
    inner join (
	    select MAX(msg1.DateTimeToCreate) as MAXDateTimeToCreate,msg1.FromProfileID
		    from EUS_Messages  msg1
            with (nolock)
		    where	msg1.ToProfileID=@CurrentProfileId
		    and msg1.IsReceived=1   -- incoming
            and msg1.statusID=1     -- read
            and ISNULL(msg1.IsHidden,0)=0
		    group by msg1.FromProfileID
	    )  as msg1 on msg1.MAXDateTimeToCreate= msg.DateTimeToCreate and prof.ProfileId = msg1.FromProfileID
    where msg.ToProfileID=@CurrentProfileId
    and msg.IsReceived=1 and msg.statusID=1  -- incoming, read
    and ISNULL(msg.IsHidden,0)=0
    AND  not exists(
        select * 
	    from EUS_ProfilesBlocked bl 
            with (nolock)
	    where (bl.FromProfileID =prof.ProfileID and bl.ToProfileID = @CurrentProfileId) or
		    (bl.FromProfileID =@CurrentProfileId and bl.ToProfileID = prof.ProfileID)
    )
    and (
        (@showActive is null) or
        (@showActive=1 and prof.Status=4) or
        (@showActive=0 and prof.Status<>4) 
    )

[AdditionalWhereClause]

) as tRowNumber 
inner join EUS_Profiles prof on prof.ProfileId = tRowNumber.FromProfileID
LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = prof.ProfileID AND phot.CustomerPhotosID = prof.DefPhotoID

]]></sql>.Value


        Else


            sql = <sql><![CDATA[
-- INBOXMessages

	if(@NumberOfRecordsToReturn>0)
		SET ROWCOUNT @NumberOfRecordsToReturn;



select 
	tOutter.*
	,messageData_StatusID = isnull((select top(1) StatusID from EUS_Messages where ISNULL(EUS_Messages.IsHidden,0)=0 and eus_messageid=tOutter.EUS_MessageID),0)

	,CommunicationStatus=ISNULL((
		select top(1) UnlockedConversationId
		from EUS_UnlockedConversations  con
        with (nolock)
		where	(con.ToProfileID = tOutter.ProfileId and con.FromProfileID =@CurrentProfileId) or
				(con.ToProfileID =@CurrentProfileId and con.FromProfileID = tOutter.ProfileId)
	),0)	 
	
	,ItemsCount=ISNULL((select COUNT(*) 
		from EUS_Messages  msg1
        with (nolock)
		where	msg1.ToProfileID=@CurrentProfileId
		and msg1.IsReceived=1	-- incoming
		and msg1.StatusID=1	-- read
		and msg1.FromProfileID=tOutter.ProfileId
        and ISNULL(msg1.IsHidden,0)=0
		group by msg1.FromProfileID 
	), 0)
	
	
	,OfferAmount=ISNULL((select  top(1)  Amount
		from eus_offers  ofr
        with (nolock)
		where ofr.StatusID=50 and (
			(ofr.ToProfileID = tOutter.ProfileId and ofr.FromProfileID =@CurrentProfileId) or
			(ofr.ToProfileID =@CurrentProfileId and ofr.FromProfileID = tOutter.ProfileId)
		)
	), 0)

from
(
	select
		RowNumber = Row_Number() over([QUERY_ORDERING])
		,tRowNumber.* 
	from
	(
        select distinct
	        msg.EUS_MessageID,
	        msg.Subject,
	        msg1.MAXDateTimeToCreate, 
	
	
	        ItemsCountRead = 0,
	
            IsReceived = 1,
            StatusID = 1,

	        prof.ProfileID, prof.IsMaster, prof.MirrorProfileID, prof.Status, prof.LoginName, 
            prof.Password, prof.FirstName, prof.LastName, prof.GenderId, prof.Country, prof.Region, 
	        prof.City, prof.Zip, prof.CityArea, prof.Address, prof.Telephone, prof.eMail, 
            prof.Cellular, prof.AreYouWillingToTravel, prof.AboutMe_Heading, 
	        prof.AboutMe_DescribeYourself, prof.AboutMe_DescribeAnIdealFirstDate, prof.OtherDetails_EducationID, 
            prof.OtherDetails_AnnualIncomeID, prof.OtherDetails_NetWorthID, prof.OtherDetails_Occupation, prof.PersonalInfo_HeightID, 
            prof.PersonalInfo_BodyTypeID, prof.PersonalInfo_EyeColorID, prof.PersonalInfo_HairColorID, prof.PersonalInfo_ChildrenID, 
            prof.PersonalInfo_EthnicityID, prof.PersonalInfo_ReligionID, prof.PersonalInfo_SmokingHabitID, 
	        prof.PersonalInfo_DrinkingHabitID, prof.LookingFor_ToMeetMaleID, prof.LookingFor_ToMeetFemaleID, 
            prof.LookingFor_RelationshipStatusID, 
	        prof.LookingFor_TypeOfDating_ShortTermRelationship, 
            prof.LookingFor_TypeOfDating_Friendship, 
            prof.LookingFor_TypeOfDating_LongTermRelationship, 
            prof.LookingFor_TypeOfDating_MutuallyBeneficialArrangements, 
            prof.LookingFor_TypeOfDating_MarriedDating, 
            prof.LookingFor_TypeOfDating_AdultDating_Casual, 
            prof.DateTimeToRegister, 
            prof.RegisterIP, 
            prof.LastLoginDateTime, 
            prof.LastUpdateProfileDateTime, 
            prof.LAGID, 
            prof.Birthday, 
            prof.IsOnline, 
            prof.LastActivityDateTime	        
	        ,[HasPhoto] = case 
			        when prof.PhotosPublic > 0  then 1
			        when prof.PhotosPrivate > 0  then 1
			        when phot.[CustomerID] IS NULL  then 0
			        else 1 
		        end
	        ,phot.CustomerID
            ,phot.FileName 
        from EUS_Messages msg
            with (nolock)
        inner join EUS_Profiles prof on prof.ProfileId = msg.FromProfileID
        inner join (
	        select MAX(msg1.DateTimeToCreate) as MAXDateTimeToCreate,msg1.FromProfileID
		        from EUS_Messages  msg1
                with (nolock)
		        where	msg1.ToProfileID=@CurrentProfileId
		        and msg1.IsReceived=1   -- incoming
                and msg1.statusID=1     -- read
                and ISNULL(msg1.IsHidden,0)=0
		        group by msg1.FromProfileID
	        )  as msg1 on msg1.MAXDateTimeToCreate= msg.DateTimeToCreate and prof.ProfileId = msg1.FromProfileID
        LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = prof.ProfileID AND phot.CustomerPhotosID = prof.DefPhotoID
        where msg.ToProfileID=@CurrentProfileId
        and msg.IsReceived=1 and msg.statusID=1  -- incoming, read
        and ISNULL(msg.IsHidden,0)=0
        AND  not exists(
            select * 
	        from EUS_ProfilesBlocked bl 
                with (nolock)
	        where (bl.FromProfileID =prof.ProfileID and bl.ToProfileID = @CurrentProfileId) or
		        (bl.FromProfileID =@CurrentProfileId and bl.ToProfileID = prof.ProfileID)
        )
        and (
            (@showActive is null) or
            (@showActive=1 and prof.Status=4) or
            (@showActive=0 and prof.Status<>4) 
        )

[AdditionalWhereClause]

	) as tRowNumber 
) as tOutter
where 
	tOutter.RowNumber > @RowNumberMin and
	tOutter.RowNumber <= @RowNumberMax 
order by tOutter.RowNumber

]]></sql>.Value

        End If

        Return sql
    End Function


    Shared Function GetNEWMessagesSQL(MessagesSqlType As enmMessagesSqlType)
        Dim sql As String = ""
        If (MessagesSqlType = enmMessagesSqlType.Counter) Then

            sql = <sql><![CDATA[
-- NEWMessages count


select 
    @CountMsgs= count (distinct prof.ProfileID)
from EUS_Messages msg
        with (nolock)
inner join EUS_Profiles prof on prof.ProfileId = msg.FromProfileID
inner join (
	select MAX(msg1.DateTimeToCreate) as MAXDateTimeToCreate,msg1.FromProfileID
		from EUS_Messages  msg1
            with (nolock)
		where	msg1.ToProfileID=@CurrentProfileId
		and msg1.IsReceived=1 and msg1.statusID=0	-- incoming, unread
        and ISNULL(msg1.IsHidden,0)=0
		group by msg1.FromProfileID
	)  as msg1 on msg1.MAXDateTimeToCreate= msg.DateTimeToCreate and prof.ProfileId = msg1.FromProfileID
LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = prof.ProfileID AND phot.CustomerPhotosID = prof.DefPhotoID
where msg.ToProfileID=@CurrentProfileId
and msg.IsReceived=1 and msg.statusID=0	-- incoming, unread
and ISNULL(msg.IsHidden,0)=0
AND  not exists(
    select * 
	from EUS_ProfilesBlocked bl 
        with (nolock)
	where (bl.FromProfileID =prof.ProfileID and bl.ToProfileID = @CurrentProfileId) or
		(bl.FromProfileID =@CurrentProfileId and bl.ToProfileID = prof.ProfileID)
)
and (
    (@showActive is null) or
    (@showActive=1 and prof.Status=4) or
    (@showActive=0 and prof.Status<>4) 
)

[AdditionalWhereClause]

]]></sql>.Value


        ElseIf (MessagesSqlType = enmMessagesSqlType.CounterFast) Then

            sql = <sql><![CDATA[
-- NEWMessages CounterFasts


select 
    CountMsgs= count (distinct prof.ProfileID)
from EUS_Messages msg
        with (nolock)
inner join EUS_Profiles prof on prof.ProfileId = msg.FromProfileID
inner join (
	select MAX(msg1.DateTimeToCreate) as MAXDateTimeToCreate,msg1.FromProfileID
		from EUS_Messages  msg1
            with (nolock)
		where	msg1.ToProfileID=@CurrentProfileId
		and msg1.IsReceived=1 and msg1.statusID=0	-- incoming, unread
        and ISNULL(msg1.IsHidden,0)=0
		group by msg1.FromProfileID
	)  as msg1 on msg1.MAXDateTimeToCreate= msg.DateTimeToCreate and prof.ProfileId = msg1.FromProfileID
where msg.ToProfileID=@CurrentProfileId
and msg.IsReceived=1 and msg.statusID=0	-- incoming, unread
and ISNULL(msg.IsHidden,0)=0
AND  not exists(
    select * 
	from EUS_ProfilesBlocked bl 
        with (nolock)
	where (bl.FromProfileID =prof.ProfileID and bl.ToProfileID = @CurrentProfileId) or
		(bl.FromProfileID =@CurrentProfileId and bl.ToProfileID = prof.ProfileID)
)
and (
    (@showActive is null) or
    (@showActive=1 and prof.Status=4) or
    (@showActive=0 and prof.Status<>4) 
)

[AdditionalWhereClause]

]]></sql>.Value


        ElseIf (MessagesSqlType = enmMessagesSqlType.UnionCounter) Then

            sql = <sql><![CDATA[
-- NEWMessages count


select 
    CountMsgs= count (distinct prof.ProfileID)
from EUS_Messages msg
        with (nolock)
inner join EUS_Profiles prof on prof.ProfileId = msg.FromProfileID
inner join (
	select MAX(msg1.DateTimeToCreate) as MAXDateTimeToCreate,msg1.FromProfileID
		from EUS_Messages  msg1
            with (nolock)
		where	msg1.ToProfileID=@CurrentProfileId
		and msg1.IsReceived=1 and msg1.statusID=0	-- incoming, unread
        and ISNULL(msg1.IsHidden,0)=0
		group by msg1.FromProfileID
	)  as msg1 on msg1.MAXDateTimeToCreate= msg.DateTimeToCreate and prof.ProfileId = msg1.FromProfileID
LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = prof.ProfileID AND phot.CustomerPhotosID = prof.DefPhotoID
where msg.ToProfileID=@CurrentProfileId
and msg.IsReceived=1 and msg.statusID=0	-- incoming, unread
and ISNULL(msg.IsHidden,0)=0
AND  not exists(
    select * 
	from EUS_ProfilesBlocked bl 
        with (nolock)
	where (bl.FromProfileID =prof.ProfileID and bl.ToProfileID = @CurrentProfileId) or
		(bl.FromProfileID =@CurrentProfileId and bl.ToProfileID = prof.ProfileID)
)
and (
    (@showActive is null) or
    (@showActive=1 and prof.Status=4) or
    (@showActive=0 and prof.Status<>4) 
)

[AdditionalWhereClause]

]]></sql>.Value


        ElseIf (MessagesSqlType = enmMessagesSqlType.Union) Then

            sql = <sql><![CDATA[
-- NEWMessages


select 
	tRowNumber.*
	,messageData_StatusID = isnull((select top(1) StatusID from EUS_Messages where ISNULL(EUS_Messages.IsHidden,0)=0 and eus_messageid=tRowNumber.EUS_MessageID),0)

	,CommunicationStatus=ISNULL((
		select top(1) UnlockedConversationId
		from EUS_UnlockedConversations  con
        with (nolock)
		where	(con.ToProfileID = tRowNumber.ProfileId and con.FromProfileID =@CurrentProfileId) or
				(con.ToProfileID =@CurrentProfileId and con.FromProfileID = tRowNumber.ProfileId)
	),0) 
	
	,ItemsCount=ISNULL((select COUNT(*) 
		from EUS_Messages  msg1
        with (nolock)
		where	msg1.ToProfileID=@CurrentProfileId
		and msg1.IsReceived=1	-- incoming
		and msg1.StatusID=0	-- read
		and msg1.FromProfileID=tRowNumber.ProfileId
        and ISNULL(msg1.IsHidden,0)=0
		group by msg1.FromProfileID 
	), 0)
	
	
	,OfferAmount=ISNULL((select  top(1)  Amount
		from eus_offers  ofr
        with (nolock)
		where ofr.StatusID=50 and (
			(ofr.ToProfileID = tRowNumber.ProfileId and ofr.FromProfileID =@CurrentProfileId) or
			(ofr.ToProfileID =@CurrentProfileId and ofr.FromProfileID = tRowNumber.ProfileId)
		)
	), 0)
from
(
        select distinct
	        msg.EUS_MessageID,
	        msg.Subject,
	        msg1.MAXDateTimeToCreate, 
	
	        ItemsCountRead = 0,
	
            IsReceived = 0,
            StatusID = 0,

	        prof.ProfileID, prof.IsMaster, prof.MirrorProfileID, prof.Status, prof.LoginName, 
            prof.Password, prof.FirstName, prof.LastName, prof.GenderId, prof.Country, prof.Region, 
	        prof.City, prof.Zip, prof.CityArea, prof.Address, prof.Telephone, prof.eMail, 
            prof.Cellular, prof.AreYouWillingToTravel, prof.AboutMe_Heading, 
	        prof.AboutMe_DescribeYourself, prof.AboutMe_DescribeAnIdealFirstDate, prof.OtherDetails_EducationID, 
            prof.OtherDetails_AnnualIncomeID, prof.OtherDetails_NetWorthID, prof.OtherDetails_Occupation, prof.PersonalInfo_HeightID, 
            prof.PersonalInfo_BodyTypeID, prof.PersonalInfo_EyeColorID, prof.PersonalInfo_HairColorID, prof.PersonalInfo_ChildrenID, 
            prof.PersonalInfo_EthnicityID, prof.PersonalInfo_ReligionID, prof.PersonalInfo_SmokingHabitID, 
	        prof.PersonalInfo_DrinkingHabitID, prof.LookingFor_ToMeetMaleID, prof.LookingFor_ToMeetFemaleID, prof.LookingFor_RelationshipStatusID, 
	        prof.LookingFor_TypeOfDating_ShortTermRelationship, prof.LookingFor_TypeOfDating_Friendship, prof.LookingFor_TypeOfDating_LongTermRelationship, 
	        prof.LookingFor_TypeOfDating_MutuallyBeneficialArrangements, 
            prof.LookingFor_TypeOfDating_MarriedDating, 
            prof.LookingFor_TypeOfDating_AdultDating_Casual, 
            prof.DateTimeToRegister, 
            prof.RegisterIP, 
            prof.LastLoginDateTime, 
            prof.LastUpdateProfileDateTime, 
            prof.LAGID, 
            prof.Birthday, 
            prof.IsOnline, 
            prof.LastActivityDateTime        
            ,[HasPhoto] = case 
			        when prof.PhotosPublic > 0  then 1
			        when prof.PhotosPrivate > 0  then 1
			        when phot.[CustomerID] IS NULL  then 0
			        else 1 
		        end
		
	        ,phot.CustomerID
            ,phot.FileName 
        from EUS_Messages msg
                with (nolock)
        inner join EUS_Profiles prof on prof.ProfileId = msg.FromProfileID
        inner join (
	        select MAX(msg1.DateTimeToCreate) as MAXDateTimeToCreate,msg1.FromProfileID
		        from EUS_Messages  msg1
                    with (nolock)
		        where	msg1.ToProfileID=@CurrentProfileId
		        and msg1.IsReceived=1 and msg1.statusID=0	-- incoming, unread
                and ISNULL(msg1.IsHidden,0)=0
		        group by msg1.FromProfileID
	        )  as msg1 on msg1.MAXDateTimeToCreate= msg.DateTimeToCreate and prof.ProfileId = msg1.FromProfileID
        LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = prof.ProfileID AND phot.CustomerPhotosID = prof.DefPhotoID
        where msg.ToProfileID=@CurrentProfileId
        and msg.IsReceived=1 and msg.statusID=0	-- incoming, unread
        and ISNULL(msg.IsHidden,0)=0
        AND  not exists(
            select * 
	        from EUS_ProfilesBlocked bl 
                with (nolock)
	        where (bl.FromProfileID =prof.ProfileID and bl.ToProfileID = @CurrentProfileId) or
		        (bl.FromProfileID =@CurrentProfileId and bl.ToProfileID = prof.ProfileID)
        )
        and (
            (@showActive is null) or
            (@showActive=1 and prof.Status=4) or
            (@showActive=0 and prof.Status<>4) 
        )

[AdditionalWhereClause]

) as tRowNumber 

]]></sql>.Value


        ElseIf (MessagesSqlType = enmMessagesSqlType.UnionFast) Then

            sql = <sql><![CDATA[
-- NEWMessages UnionFast


select 
	tRowNumber.*
    ,messageData_StatusID = isnull((select top(1) StatusID from EUS_Messages where ISNULL(EUS_Messages.IsHidden,0)=0 and eus_messageid=tRowNumber.EUS_MessageID),0),
	prof.ProfileID, prof.IsMaster, prof.MirrorProfileID, prof.Status, prof.LoginName, 
    prof.GenderId, prof.Country, prof.Region, 
	prof.City, prof.Zip,
    prof.Birthday, 
    prof.IsOnline, 
    prof.LastActivityDateTime
    ,[HasPhoto] = case 
			when prof.PhotosPublic > 0  then 1
			when prof.PhotosPrivate > 0  then 1
			when phot.[CustomerID] IS NULL  then 0
			else 1 
		end
		
	,phot.CustomerID
    ,phot.FileName 

	,CommunicationStatus=ISNULL((
		select top(1) UnlockedConversationId
		from EUS_UnlockedConversations  con
        with (nolock)
		where	(con.ToProfileID = prof.ProfileId and con.FromProfileID =@CurrentProfileId) or
				(con.ToProfileID =@CurrentProfileId and con.FromProfileID = prof.ProfileId)
	),0) 
	
	,ItemsCount=ISNULL((select COUNT(*) 
		from EUS_Messages  msg1
        with (nolock)
		where	msg1.ToProfileID=@CurrentProfileId
		and msg1.IsReceived=1	-- incoming
		and msg1.StatusID=0	-- read
		and msg1.FromProfileID=prof.ProfileId
        and ISNULL(msg1.IsHidden,0)=0
		group by msg1.FromProfileID 
	), 0)
	
	
	,OfferAmount=ISNULL((select  top(1)  Amount
		from eus_offers  ofr
        with (nolock)
		where ofr.StatusID=50 and (
			(ofr.ToProfileID = prof.ProfileId and ofr.FromProfileID =@CurrentProfileId) or
			(ofr.ToProfileID =@CurrentProfileId and ofr.FromProfileID = prof.ProfileId)
		)
	), 0)
from
(
    select distinct
	    msg.EUS_MessageID,
	    msg.Subject,
	    msg.ToProfileID,
	    msg.FromProfileID,
	    msg1.MAXDateTimeToCreate, 
	    ItemsCountRead = 0,
        IsReceived = 0,
        StatusID = 0
    from EUS_Messages msg
            with (nolock)
    inner join EUS_Profiles prof on prof.ProfileId = msg.FromProfileID
    inner join (
	    select MAX(msg1.DateTimeToCreate) as MAXDateTimeToCreate,msg1.FromProfileID
		    from EUS_Messages  msg1
                with (nolock)
		    where	msg1.ToProfileID=@CurrentProfileId
		    and msg1.IsReceived=1 and msg1.statusID=0	-- incoming, unread
            and ISNULL(msg1.IsHidden,0)=0
		    group by msg1.FromProfileID
	    )  as msg1 on msg1.MAXDateTimeToCreate= msg.DateTimeToCreate and prof.ProfileId = msg1.FromProfileID
    where msg.ToProfileID=@CurrentProfileId
    and msg.IsReceived=1 and msg.statusID=0	-- incoming, unread
    and ISNULL(msg.IsHidden,0)=0
    AND  not exists(
        select * 
	    from EUS_ProfilesBlocked bl 
            with (nolock)
	    where (bl.FromProfileID =prof.ProfileID and bl.ToProfileID = @CurrentProfileId) or
		    (bl.FromProfileID =@CurrentProfileId and bl.ToProfileID = prof.ProfileID)
    )
    and (
        (@showActive is null) or
        (@showActive=1 and prof.Status=4) or
        (@showActive=0 and prof.Status<>4) 
    )

[AdditionalWhereClause]

) as tRowNumber 
inner join EUS_Profiles prof on prof.ProfileId = tRowNumber.FromProfileID
LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = prof.ProfileID AND phot.CustomerPhotosID = prof.DefPhotoID

]]></sql>.Value

        Else


            sql = <sql><![CDATA[
-- NEWMessages

	if(@NumberOfRecordsToReturn>0)
		SET ROWCOUNT @NumberOfRecordsToReturn;



select 
	tOutter.*
	,messageData_StatusID = isnull((select top(1) StatusID from EUS_Messages where ISNULL(EUS_Messages.IsHidden,0)=0 and eus_messageid=tOutter.EUS_MessageID),0)

	,CommunicationStatus=ISNULL((
		select top(1) UnlockedConversationId
		from EUS_UnlockedConversations  con
        with (nolock)
		where	(con.ToProfileID = tOutter.ProfileId and con.FromProfileID =@CurrentProfileId) or
				(con.ToProfileID =@CurrentProfileId and con.FromProfileID = tOutter.ProfileId)
	),0) 
	
	,ItemsCount=ISNULL((select COUNT(*) 
		from EUS_Messages  msg1
        with (nolock)
		where	msg1.ToProfileID=@CurrentProfileId
		and msg1.IsReceived=1	-- incoming
		and msg1.StatusID=0	-- read
		and msg1.FromProfileID=tOutter.ProfileId
        and ISNULL(msg1.IsHidden,0)=0
		group by msg1.FromProfileID 
	), 0)
	
	
	,OfferAmount=ISNULL((select  top(1)  Amount
		from eus_offers  ofr
        with (nolock)
		where ofr.StatusID=50 and (
			(ofr.ToProfileID = tOutter.ProfileId and ofr.FromProfileID =@CurrentProfileId) or
			(ofr.ToProfileID =@CurrentProfileId and ofr.FromProfileID = tOutter.ProfileId)
		)
	), 0)
from
(
	select
		RowNumber = Row_Number() over([QUERY_ORDERING])
		,tRowNumber.* 
	from
	(
        select distinct
	        msg.EUS_MessageID,
	        msg.Subject,
	        msg1.MAXDateTimeToCreate, 
	
	        ItemsCountRead = 0,
	
            IsReceived = 0,
            StatusID = 0,

	        prof.ProfileID, prof.IsMaster, prof.MirrorProfileID, prof.Status, prof.LoginName, 
            prof.Password, prof.FirstName, prof.LastName, prof.GenderId, prof.Country, prof.Region, 
	        prof.City, prof.Zip, prof.CityArea, prof.Address, prof.Telephone, prof.eMail, 
            prof.Cellular, prof.AreYouWillingToTravel, prof.AboutMe_Heading, 
	        prof.AboutMe_DescribeYourself, prof.AboutMe_DescribeAnIdealFirstDate, prof.OtherDetails_EducationID, 
            prof.OtherDetails_AnnualIncomeID, prof.OtherDetails_NetWorthID, prof.OtherDetails_Occupation, prof.PersonalInfo_HeightID, 
            prof.PersonalInfo_BodyTypeID, prof.PersonalInfo_EyeColorID, prof.PersonalInfo_HairColorID, prof.PersonalInfo_ChildrenID, 
            prof.PersonalInfo_EthnicityID, prof.PersonalInfo_ReligionID, prof.PersonalInfo_SmokingHabitID, 
	        prof.PersonalInfo_DrinkingHabitID, prof.LookingFor_ToMeetMaleID, prof.LookingFor_ToMeetFemaleID, prof.LookingFor_RelationshipStatusID, 
	        prof.LookingFor_TypeOfDating_ShortTermRelationship, prof.LookingFor_TypeOfDating_Friendship, prof.LookingFor_TypeOfDating_LongTermRelationship, 
	        prof.LookingFor_TypeOfDating_MutuallyBeneficialArrangements, 
            prof.LookingFor_TypeOfDating_MarriedDating, 
            prof.LookingFor_TypeOfDating_AdultDating_Casual, 
            prof.DateTimeToRegister, 
            prof.RegisterIP, 
            prof.LastLoginDateTime, 
            prof.LastUpdateProfileDateTime, 
            prof.LAGID, 
            prof.Birthday, 
            prof.IsOnline, 
            prof.LastActivityDateTime	        
            ,[HasPhoto] = case 
			        when prof.PhotosPublic > 0  then 1
			        when prof.PhotosPrivate > 0  then 1
			        when phot.[CustomerID] IS NULL  then 0
			        else 1 
		        end
		
	        ,phot.CustomerID
            ,phot.FileName 
        from EUS_Messages msg
                with (nolock)
        inner join EUS_Profiles prof on prof.ProfileId = msg.FromProfileID
        inner join (
	        select MAX(msg1.DateTimeToCreate) as MAXDateTimeToCreate,msg1.FromProfileID
		        from EUS_Messages  msg1
                    with (nolock)
		        where	msg1.ToProfileID=@CurrentProfileId
		        and msg1.IsReceived=1 and msg1.statusID=0	-- incoming, unread
                and ISNULL(msg1.IsHidden,0)=0
		        group by msg1.FromProfileID
	        )  as msg1 on msg1.MAXDateTimeToCreate= msg.DateTimeToCreate and prof.ProfileId = msg1.FromProfileID
        LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = prof.ProfileID AND phot.CustomerPhotosID = prof.DefPhotoID
        where msg.ToProfileID=@CurrentProfileId
        and msg.IsReceived=1 and msg.statusID=0	-- incoming, unread
        and ISNULL(msg.IsHidden,0)=0
        AND  not exists(
            select * 
	        from EUS_ProfilesBlocked bl 
                with (nolock)
	        where (bl.FromProfileID =prof.ProfileID and bl.ToProfileID = @CurrentProfileId) or
		        (bl.FromProfileID =@CurrentProfileId and bl.ToProfileID = prof.ProfileID)
        )
        and (
            (@showActive is null) or
            (@showActive=1 and prof.Status=4) or
            (@showActive=0 and prof.Status<>4) 
        )

[AdditionalWhereClause]

	) as tRowNumber 
) as tOutter
where 
	tOutter.RowNumber > @RowNumberMin and
	tOutter.RowNumber <= @RowNumberMax 
order by tOutter.RowNumber

]]></sql>.Value

        End If

        Return sql
    End Function


    Shared Function GetSENTMessagesSQL(MessagesSqlType As enmMessagesSqlType)
        Dim sql As String = ""
        If (MessagesSqlType = enmMessagesSqlType.Counter) Then

            sql = <sql><![CDATA[
-- SENTMessages count

select 
    CountMsgs= count (distinct prof.ProfileID)
from EUS_Messages msg
    with (nolock)
inner join EUS_Profiles prof on prof.ProfileId = msg.ToProfileID
inner join (
	select MAX(msg1.DateTimeToCreate) as MAXDateTimeToCreate,msg1.ToProfileID
		from EUS_Messages  msg1
        with (nolock)
		where	msg1.FromProfileID=@CurrentProfileId
		and msg1.IsReceived=0	-- outgoing
        and ISNULL(msg1.IsHidden,0)=0
		group by msg1.ToProfileID
	)  as msg1 on msg1.MAXDateTimeToCreate= msg.DateTimeToCreate and prof.ProfileId=msg1.ToProfileID
LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = prof.ProfileID AND phot.CustomerPhotosID = prof.DefPhotoID
where msg.FromProfileID=@CurrentProfileId
and msg.IsReceived=0
and ISNULL(msg.IsHidden,0)=0
AND  not exists(
    select * 
	from EUS_ProfilesBlocked bl 
        with (nolock)
	where (bl.FromProfileID =prof.ProfileID and bl.ToProfileID = @CurrentProfileId) or
		(bl.FromProfileID =@CurrentProfileId and bl.ToProfileID = prof.ProfileID)
)
and (
    (@showActive is null) or
    (@showActive=1 and prof.Status=4) or
    (@showActive=0 and prof.Status<>4) 
)

[AdditionalWhereClause]

]]></sql>.Value


        ElseIf (MessagesSqlType = enmMessagesSqlType.CounterFast) Then

            sql = <sql><![CDATA[
-- SENTMessages CounterFast

select 
    CountMsgs= count (distinct prof.ProfileID)
from EUS_Messages msg
    with (nolock)
inner join EUS_Profiles prof on prof.ProfileId = msg.ToProfileID
inner join (
	select MAX(msg1.DateTimeToCreate) as MAXDateTimeToCreate,msg1.ToProfileID
		from EUS_Messages  msg1
        with (nolock)
		where	msg1.FromProfileID=@CurrentProfileId
		and msg1.IsReceived=0	-- outgoing
        and ISNULL(msg1.IsHidden,0)=0
		group by msg1.ToProfileID
	)  as msg1 on msg1.MAXDateTimeToCreate= msg.DateTimeToCreate and prof.ProfileId=msg1.ToProfileID
where msg.FromProfileID=@CurrentProfileId
and msg.IsReceived=0
and ISNULL(msg.IsHidden,0)=0
AND  not exists(
    select * 
	from EUS_ProfilesBlocked bl 
        with (nolock)
	where (bl.FromProfileID =prof.ProfileID and bl.ToProfileID = @CurrentProfileId) or
		(bl.FromProfileID =@CurrentProfileId and bl.ToProfileID = prof.ProfileID)
)
and (
    (@showActive is null) or
    (@showActive=1 and prof.Status=4) or
    (@showActive=0 and prof.Status<>4) 
)

[AdditionalWhereClause]

]]></sql>.Value


        ElseIf (MessagesSqlType = enmMessagesSqlType.Union) Then


            sql = <sql><![CDATA[

-- SENTMessages

select 
	tRowNumber.* 
	,messageData_StatusID = isnull((select top(1) StatusID from EUS_Messages where ISNULL(EUS_Messages.IsHidden,0)=0 and eus_messageid=tRowNumber.EUS_MessageID),0)

	,CommunicationStatus=ISNULL((
		select top(1) UnlockedConversationId
		from EUS_UnlockedConversations  con
        with (nolock)
		where	(con.ToProfileID = tRowNumber.ProfileId and con.FromProfileID =@CurrentProfileId) or
				(con.ToProfileID =@CurrentProfileId and con.FromProfileID = tRowNumber.ProfileId)
	),0)	 
	
	
	,ItemsCount=(select COUNT(*) 
		from EUS_Messages  msg1
        with (nolock)
		where	msg1.FromProfileID=@CurrentProfileId
		and msg1.IsReceived=0	-- outgoing
		and msg1.ToProfileID=tRowNumber.ProfileId
        and ISNULL(msg1.IsHidden,0)=0
		group by msg1.FromProfileID 
	)
	
	,OfferAmount=(select  top(1)  Amount
		from eus_offers  ofr
        with (nolock)
		where ofr.StatusID=50 and (
			(ofr.ToProfileID = tRowNumber.ProfileId and ofr.FromProfileID =@CurrentProfileId) or
			(ofr.ToProfileID =@CurrentProfileId and ofr.FromProfileID = tRowNumber.ProfileId)
		)
	)
from
(
        select distinct
	        msg.EUS_MessageID,
	        msg.Subject,
	        msg1.MAXDateTimeToCreate, 
            ItemsCountRead = null,

            IsReceived = 0,
            StatusID = 1,

	        prof.ProfileID, prof.IsMaster, prof.MirrorProfileID, prof.Status, prof.LoginName, prof.Password, prof.FirstName, prof.LastName, prof.GenderId, prof.Country, prof.Region, 
	        prof.City, prof.Zip, prof.CityArea, prof.Address, prof.Telephone, prof.eMail, prof.Cellular, prof.AreYouWillingToTravel, prof.AboutMe_Heading, 
	        prof.AboutMe_DescribeYourself, prof.AboutMe_DescribeAnIdealFirstDate, prof.OtherDetails_EducationID, prof.OtherDetails_AnnualIncomeID, 
	        prof.OtherDetails_NetWorthID, prof.OtherDetails_Occupation, prof.PersonalInfo_HeightID, prof.PersonalInfo_BodyTypeID, prof.PersonalInfo_EyeColorID, 
	        prof.PersonalInfo_HairColorID, prof.PersonalInfo_ChildrenID, prof.PersonalInfo_EthnicityID, prof.PersonalInfo_ReligionID, prof.PersonalInfo_SmokingHabitID, 
	        prof.PersonalInfo_DrinkingHabitID, prof.LookingFor_ToMeetMaleID, prof.LookingFor_ToMeetFemaleID, prof.LookingFor_RelationshipStatusID, 
	        prof.LookingFor_TypeOfDating_ShortTermRelationship, prof.LookingFor_TypeOfDating_Friendship, prof.LookingFor_TypeOfDating_LongTermRelationship, 
	        prof.LookingFor_TypeOfDating_MutuallyBeneficialArrangements, prof.LookingFor_TypeOfDating_MarriedDating, prof.LookingFor_TypeOfDating_AdultDating_Casual, 
            prof.DateTimeToRegister, 
            prof.RegisterIP, 
            prof.LastLoginDateTime, 
            prof.LastUpdateProfileDateTime, 
            prof.LAGID, 
            prof.Birthday, 
            prof.IsOnline, 
            prof.LastActivityDateTime
	        ,[HasPhoto] = case 
			        when prof.PhotosPublic > 0  then 1
			        when prof.PhotosPrivate > 0  then 1
			        when phot.[CustomerID] IS NULL  then 0
			        else 1 
		        end
		
	        ,phot.CustomerID
            ,phot.FileName 
        from EUS_Messages msg
                with (nolock)
        inner join EUS_Profiles prof on prof.ProfileId = msg.ToProfileID
        inner join (
	        select MAX(msg1.DateTimeToCreate) as MAXDateTimeToCreate,msg1.ToProfileID
		        from EUS_Messages  msg1
                with (nolock)
		        where	msg1.FromProfileID=@CurrentProfileId
		        and msg1.IsReceived=0	-- outgoing
                and ISNULL(msg1.IsHidden,0)=0
		        group by msg1.ToProfileID
	        )  as msg1 on msg1.MAXDateTimeToCreate= msg.DateTimeToCreate and prof.ProfileId=msg1.ToProfileID
        LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = prof.ProfileID AND phot.CustomerPhotosID = prof.DefPhotoID
        where msg.FromProfileID=@CurrentProfileId
        and msg.IsReceived=0
        and ISNULL(msg.IsHidden,0)=0
        AND  not exists(
            select * 
	        from EUS_ProfilesBlocked bl 
                with (nolock)
	        where (bl.FromProfileID =prof.ProfileID and bl.ToProfileID = @CurrentProfileId) or
		        (bl.FromProfileID =@CurrentProfileId and bl.ToProfileID = prof.ProfileID)
        )
        and (
            (@showActive is null) or
            (@showActive=1 and prof.Status=4) or
            (@showActive=0 and prof.Status<>4) 
        )

[AdditionalWhereClause]

) as tRowNumber 

]]></sql>.Value


        ElseIf (MessagesSqlType = enmMessagesSqlType.UnionFast) Then


            sql = <sql><![CDATA[
-- SENTMessages UnionFast

select 
	tRowNumber.* 
	,messageData_StatusID = isnull((select top(1) StatusID from EUS_Messages where ISNULL(EUS_Messages.IsHidden,0)=0 and eus_messageid=tRowNumber.EUS_MessageID),0),
	prof.ProfileID, prof.IsMaster, prof.MirrorProfileID, prof.Status, prof.LoginName, 
    prof.GenderId, prof.Country, prof.Region, 
	prof.City, prof.Zip,
    prof.Birthday, 
    prof.IsOnline, 
    prof.LastActivityDateTime
	,[HasPhoto] = case 
			when prof.PhotosPublic > 0  then 1
			when prof.PhotosPrivate > 0  then 1
			when phot.[CustomerID] IS NULL  then 0
			else 1 
		end
	,phot.CustomerID
    ,phot.FileName 
	,CommunicationStatus=ISNULL((
		select top(1) UnlockedConversationId
		from EUS_UnlockedConversations  con
        with (nolock)
		where	(con.ToProfileID = prof.ProfileId and con.FromProfileID =@CurrentProfileId) or
				(con.ToProfileID =@CurrentProfileId and con.FromProfileID = prof.ProfileId)
	),0)	 
	,ItemsCount=(select COUNT(*) 
		from EUS_Messages  msg1
        with (nolock)
		where	msg1.FromProfileID=@CurrentProfileId
		and msg1.IsReceived=0	-- outgoing
		and msg1.ToProfileID=prof.ProfileId
        and ISNULL(msg1.IsHidden,0)=0
		group by msg1.FromProfileID 
	)
	,OfferAmount=(select  top(1)  Amount
		from eus_offers  ofr
        with (nolock)
		where ofr.StatusID=50 and (
			(ofr.ToProfileID = prof.ProfileId and ofr.FromProfileID =@CurrentProfileId) or
			(ofr.ToProfileID =@CurrentProfileId and ofr.FromProfileID = prof.ProfileId)
		)
	)
from
(
    select distinct
	    msg.EUS_MessageID,
	    msg.Subject,
	    msg.ToProfileID,
	    msg.FromProfileID,
	    msg1.MAXDateTimeToCreate, 
        ItemsCountRead = null,
        IsReceived = 0,
        StatusID = 1
    from EUS_Messages msg
            with (nolock)
    inner join EUS_Profiles prof on prof.ProfileId = msg.ToProfileID
    inner join (
	    select MAX(msg1.DateTimeToCreate) as MAXDateTimeToCreate,msg1.ToProfileID
		    from EUS_Messages  msg1
            with (nolock)
		    where	msg1.FromProfileID=@CurrentProfileId
		    and msg1.IsReceived=0	-- outgoing
            and ISNULL(msg1.IsHidden,0)=0
		    group by msg1.ToProfileID
	    )  as msg1 on msg1.MAXDateTimeToCreate= msg.DateTimeToCreate and prof.ProfileId=msg1.ToProfileID
    where msg.FromProfileID=@CurrentProfileId
    and msg.IsReceived=0
    and ISNULL(msg.IsHidden,0)=0
    AND  not exists(
        select * 
	    from EUS_ProfilesBlocked bl 
            with (nolock)
	    where (bl.FromProfileID =prof.ProfileID and bl.ToProfileID = @CurrentProfileId) or
		    (bl.FromProfileID =@CurrentProfileId and bl.ToProfileID = prof.ProfileID)
    )
    and (
        (@showActive is null) or
        (@showActive=1 and prof.Status=4) or
        (@showActive=0 and prof.Status<>4) 
    )

[AdditionalWhereClause]

) as tRowNumber 
inner join EUS_Profiles prof on prof.ProfileId = tRowNumber.ToProfileID
LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = prof.ProfileID AND phot.CustomerPhotosID = prof.DefPhotoID

]]></sql>.Value

        Else

            sql = <sql><![CDATA[

-- SENTMessages

	if(@NumberOfRecordsToReturn>0)
		SET ROWCOUNT @NumberOfRecordsToReturn;


select 
	tOutter.*
	,messageData_StatusID = isnull((select top(1) StatusID from EUS_Messages where ISNULL(EUS_Messages.IsHidden,0)=0 and eus_messageid=tOutter.EUS_MessageID),0)

	,CommunicationStatus=ISNULL((
		select top(1) UnlockedConversationId
		from EUS_UnlockedConversations  con
        with (nolock)
		where	(con.ToProfileID = tOutter.ProfileId and con.FromProfileID =@CurrentProfileId) or
				(con.ToProfileID =@CurrentProfileId and con.FromProfileID = tOutter.ProfileId)
	),0)	 
	
	
	,ItemsCount=(select COUNT(*) 
		from EUS_Messages  msg1
        with (nolock)
		where	msg1.FromProfileID=@CurrentProfileId
		and msg1.IsReceived=0	-- outgoing
		and msg1.ToProfileID=tOutter.ProfileId
        and ISNULL(msg1.IsHidden,0)=0
		group by msg1.FromProfileID 
	)
	
	,OfferAmount=(select  top(1)  Amount
		from eus_offers  ofr
        with (nolock)
		where ofr.StatusID=50 and (
			(ofr.ToProfileID = tOutter.ProfileId and ofr.FromProfileID =@CurrentProfileId) or
			(ofr.ToProfileID =@CurrentProfileId and ofr.FromProfileID = tOutter.ProfileId)
		)
	)

from
(
	select
		RowNumber = Row_Number() over([QUERY_ORDERING])
		,tRowNumber.* 
	from
	(
        select distinct
	        msg.EUS_MessageID,
	        msg.Subject,
	        msg1.MAXDateTimeToCreate, 
            ItemsCountRead = null,

            IsReceived = 0,
            StatusID = 1,

	        prof.ProfileID, prof.IsMaster, prof.MirrorProfileID, prof.Status, prof.LoginName, prof.Password, prof.FirstName, prof.LastName, prof.GenderId, prof.Country, prof.Region, 
	        prof.City, prof.Zip, prof.CityArea, prof.Address, prof.Telephone, prof.eMail, prof.Cellular, prof.AreYouWillingToTravel, prof.AboutMe_Heading, 
	        prof.AboutMe_DescribeYourself, prof.AboutMe_DescribeAnIdealFirstDate, prof.OtherDetails_EducationID, prof.OtherDetails_AnnualIncomeID, 
	        prof.OtherDetails_NetWorthID, prof.OtherDetails_Occupation, prof.PersonalInfo_HeightID, prof.PersonalInfo_BodyTypeID, prof.PersonalInfo_EyeColorID, 
	        prof.PersonalInfo_HairColorID, prof.PersonalInfo_ChildrenID, prof.PersonalInfo_EthnicityID, prof.PersonalInfo_ReligionID, prof.PersonalInfo_SmokingHabitID, 
	        prof.PersonalInfo_DrinkingHabitID, prof.LookingFor_ToMeetMaleID, prof.LookingFor_ToMeetFemaleID, prof.LookingFor_RelationshipStatusID, 
	        prof.LookingFor_TypeOfDating_ShortTermRelationship, prof.LookingFor_TypeOfDating_Friendship, prof.LookingFor_TypeOfDating_LongTermRelationship, 
	        prof.LookingFor_TypeOfDating_MutuallyBeneficialArrangements, prof.LookingFor_TypeOfDating_MarriedDating, prof.LookingFor_TypeOfDating_AdultDating_Casual, 
            prof.DateTimeToRegister, 
            prof.RegisterIP, 
            prof.LastLoginDateTime, 
            prof.LastUpdateProfileDateTime, 
            prof.LAGID, 
            prof.Birthday, 
            prof.IsOnline, 
            prof.LastActivityDateTime	        
	        ,[HasPhoto] = case 
			        when prof.PhotosPublic > 0  then 1
			        when prof.PhotosPrivate > 0  then 1
			        when phot.[CustomerID] IS NULL  then 0
			        else 1 
		        end
		
	        ,phot.CustomerID
            ,phot.FileName 
        from EUS_Messages msg
                with (nolock)
        inner join EUS_Profiles prof on prof.ProfileId = msg.ToProfileID
        inner join (
	        select MAX(msg1.DateTimeToCreate) as MAXDateTimeToCreate,msg1.ToProfileID
		        from EUS_Messages  msg1
                with (nolock)
		        where	msg1.FromProfileID=@CurrentProfileId
		        and msg1.IsReceived=0	-- outgoing
                and ISNULL(msg1.IsHidden,0)=0
		        group by msg1.ToProfileID
	        )  as msg1 on msg1.MAXDateTimeToCreate= msg.DateTimeToCreate and prof.ProfileId=msg1.ToProfileID
        LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = prof.ProfileID AND phot.CustomerPhotosID = prof.DefPhotoID
        where msg.FromProfileID=@CurrentProfileId
        and msg.IsReceived=0
        and ISNULL(msg.IsHidden,0)=0
        AND  not exists(
            select * 
	        from EUS_ProfilesBlocked bl 
                with (nolock)
	        where (bl.FromProfileID =prof.ProfileID and bl.ToProfileID = @CurrentProfileId) or
		        (bl.FromProfileID =@CurrentProfileId and bl.ToProfileID = prof.ProfileID)
        )
        and (
            (@showActive is null) or
            (@showActive=1 and prof.Status=4) or
            (@showActive=0 and prof.Status<>4) 
        )

[AdditionalWhereClause]

	) as tRowNumber 
) as tOutter
where 
	tOutter.RowNumber > @RowNumberMin and
	tOutter.RowNumber <= @RowNumberMax 
order by tOutter.RowNumber
]]></sql>.Value

        End If

        Return sql
    End Function



    Shared Function GetOWNED_NON_TRASHED_MessagesSQL(MessagesSqlType As enmMessagesSqlType)
        Dim sql As String = ""
        If (MessagesSqlType = enmMessagesSqlType.CounterFast) Then




            sql = <sql><![CDATA[
declare @CountMsgs int=0
select @CountMsgs=sum(CounterTbl.CountMsgs) 
from
(
    (
-- SENTMessages CounterFast

select 
    CountMsgs= count (distinct prof.ProfileID)
from EUS_Messages msg
    with (nolock)
inner join EUS_Profiles prof on prof.ProfileId = msg.ToProfileID
inner join (
	select MAX(msg1.DateTimeToCreate) as MAXDateTimeToCreate,msg1.ToProfileID
		from EUS_Messages  msg1
        with (nolock)
		where	msg1.FromProfileID=@CurrentProfileId
		and msg1.IsReceived=0	-- outgoing
        and ISNULL(msg1.IsHidden,0)=0
		group by msg1.ToProfileID
	)  as msg1 on msg1.MAXDateTimeToCreate= msg.DateTimeToCreate and prof.ProfileId=msg1.ToProfileID
where msg.FromProfileID=@CurrentProfileId
and msg.IsReceived=0
and ISNULL(msg.IsHidden,0)=0
AND  not exists(
    select * 
	from EUS_ProfilesBlocked bl 
        with (nolock)
	where (bl.FromProfileID =prof.ProfileID and bl.ToProfileID = @CurrentProfileId) or
		(bl.FromProfileID =@CurrentProfileId and bl.ToProfileID = prof.ProfileID)
)
and (
    (@showActive is null) or
    (@showActive=1 and prof.Status=4) or
    (@showActive=0 and prof.Status<>4) 
)

[AdditionalWhereClause]

    )
    UNION ALL(

-- NEWMessages and INBOXMessages CounterFast


select 
    CountMsgs= count (distinct prof.ProfileID)
from EUS_Messages msg
        with (nolock)
inner join EUS_Profiles prof on prof.ProfileId = msg.FromProfileID
inner join (
	select MAX(msg1.DateTimeToCreate) as MAXDateTimeToCreate,msg1.FromProfileID
		from EUS_Messages  msg1
            with (nolock)
		where	msg1.ToProfileID=@CurrentProfileId
		and msg1.IsReceived=1 -- incoming, unread or read
        and ISNULL(msg1.IsHidden,0)=0
		group by msg1.FromProfileID
	)  as msg1 on msg1.MAXDateTimeToCreate= msg.DateTimeToCreate and prof.ProfileId = msg1.FromProfileID
where msg.ToProfileID=@CurrentProfileId
and msg.IsReceived=1 -- incoming, unread or read
and ISNULL(msg.IsHidden,0)=0
AND  not exists(
    select * 
	from EUS_ProfilesBlocked bl 
        with (nolock)
	where (bl.FromProfileID =prof.ProfileID and bl.ToProfileID = @CurrentProfileId) or
		(bl.FromProfileID =@CurrentProfileId and bl.ToProfileID = prof.ProfileID)
)
and (
    (@showActive is null) or
    (@showActive=1 and prof.Status=4) or
    (@showActive=0 and prof.Status<>4) 
)

[AdditionalWhereClause]

    )
) as CounterTbl

select @CountMsgs as CountMsgs;
]]></sql>.Value


        ElseIf (MessagesSqlType = enmMessagesSqlType.UnionFast) Then


            sql = sql & <sql><![CDATA[

if(@NumberOfRecordsToReturn>0)
    SET ROWCOUNT @NumberOfRecordsToReturn;

select 
   tOutterUnion.* 
from(
    select
	    RowNumber = Row_Number() over([QUERY_ORDERING])
        ,tUnion.* 
    from
    (
        (
-- SENTMessages UnionFast

select 
	tRowNumber.* 
	,messageData_StatusID = isnull((select top(1) StatusID from EUS_Messages where ISNULL(EUS_Messages.IsHidden,0)=0 and eus_messageid=tRowNumber.EUS_MessageID),0),
	prof.ProfileID, prof.IsMaster, prof.MirrorProfileID, prof.Status, prof.LoginName, 
    prof.GenderId, prof.Country, prof.Region, 
	prof.City, prof.Zip,
    prof.Birthday, 
    prof.IsOnline, 
    prof.LastActivityDateTime
	,[HasPhoto] = case 
			when prof.PhotosPublic > 0  then 1
			when prof.PhotosPrivate > 0  then 1
			when phot.[CustomerID] IS NULL  then 0
			else 1 
		end
	,phot.CustomerID
    ,phot.FileName 
	,CommunicationStatus=ISNULL((
		select top(1) UnlockedConversationId
		from EUS_UnlockedConversations  con
        with (nolock)
		where	(con.ToProfileID = prof.ProfileId and con.FromProfileID =@CurrentProfileId) or
				(con.ToProfileID =@CurrentProfileId and con.FromProfileID = prof.ProfileId)
	),0)	 
	,ItemsCount=(select COUNT(*) 
		from EUS_Messages  msg1
        with (nolock)
		where	msg1.FromProfileID=@CurrentProfileId
		and msg1.IsReceived=0	-- outgoing
		and msg1.ToProfileID=prof.ProfileId
        and ISNULL(msg1.IsHidden,0)=0
		group by msg1.FromProfileID 
	)
	,OfferAmount=(select  top(1)  Amount
		from eus_offers  ofr
        with (nolock)
		where ofr.StatusID=50 and (
			(ofr.ToProfileID = prof.ProfileId and ofr.FromProfileID =@CurrentProfileId) or
			(ofr.ToProfileID =@CurrentProfileId and ofr.FromProfileID = prof.ProfileId)
		)
	)
from
(
    select distinct
	    msg.EUS_MessageID,
	    msg.Subject,
	    msg.ToProfileID,
	    msg.FromProfileID,
	    msg1.MAXDateTimeToCreate, 
        ItemsCountRead = null,
        IsReceived = 0,
        StatusID = 1
    from EUS_Messages msg
            with (nolock)
    inner join EUS_Profiles prof on prof.ProfileId = msg.ToProfileID
    inner join (
	    select MAX(msg1.DateTimeToCreate) as MAXDateTimeToCreate,msg1.ToProfileID
		    from EUS_Messages  msg1
            with (nolock)
		    where	msg1.FromProfileID=@CurrentProfileId
		    and msg1.IsReceived=0	-- outgoing
            and ISNULL(msg1.IsHidden,0)=0
		    group by msg1.ToProfileID
	    )  as msg1 on msg1.MAXDateTimeToCreate= msg.DateTimeToCreate and prof.ProfileId=msg1.ToProfileID
    where msg.FromProfileID=@CurrentProfileId
    and msg.IsReceived=0
    and ISNULL(msg.IsHidden,0)=0
    AND  not exists(
        select * 
	    from EUS_ProfilesBlocked bl 
            with (nolock)
	    where (bl.FromProfileID =prof.ProfileID and bl.ToProfileID = @CurrentProfileId) or
		    (bl.FromProfileID =@CurrentProfileId and bl.ToProfileID = prof.ProfileID)
    )
    and (
        (@showActive is null) or
        (@showActive=1 and prof.Status=4) or
        (@showActive=0 and prof.Status<>4) 
    )

[AdditionalWhereClause]

) as tRowNumber 
inner join EUS_Profiles prof on prof.ProfileId = tRowNumber.ToProfileID
LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = prof.ProfileID AND phot.CustomerPhotosID = prof.DefPhotoID

        )
        UNION (

-- NEWMessages and INBOXMessages UnionFast

select 
    tRowNumber.* 
	,messageData_StatusID = isnull((select top(1) StatusID from EUS_Messages where ISNULL(EUS_Messages.IsHidden,0)=0 and eus_messageid=tRowNumber.EUS_MessageID),0),
	prof.ProfileID, prof.IsMaster, prof.MirrorProfileID, prof.Status, prof.LoginName, 
    prof.GenderId, prof.Country, prof.Region, 
	prof.City, prof.Zip,
    prof.Birthday, 
    prof.IsOnline, 
    prof.LastActivityDateTime
	,[HasPhoto] = case 
			when prof.PhotosPublic > 0  then 1
			when prof.PhotosPrivate > 0  then 1
			when phot.[CustomerID] IS NULL  then 0
			else 1 
		end
	,phot.CustomerID
    ,phot.FileName 

	,CommunicationStatus=ISNULL((
		select top(1) UnlockedConversationId
		from EUS_UnlockedConversations  con
        with (nolock)
		where	(con.ToProfileID = prof.ProfileId and con.FromProfileID =@CurrentProfileId) or
				(con.ToProfileID =@CurrentProfileId and con.FromProfileID = prof.ProfileId)
	),0)	 
	
	,ItemsCount=ISNULL((select COUNT(*) 
		from EUS_Messages  msg1
        with (nolock)
		where	msg1.ToProfileID=@CurrentProfileId
		and msg1.IsReceived=1	-- incoming, unread or read
		and msg1.FromProfileID=prof.ProfileId
        and ISNULL(msg1.IsHidden,0)=0
		group by msg1.FromProfileID 
	), 0)
	
	
	,OfferAmount=ISNULL((select  top(1)  Amount
		from eus_offers  ofr
        with (nolock)
		where ofr.StatusID=50 and (
			(ofr.ToProfileID = prof.ProfileId and ofr.FromProfileID =@CurrentProfileId) or
			(ofr.ToProfileID =@CurrentProfileId and ofr.FromProfileID = prof.ProfileId)
		)
	), 0)

from
(
    select distinct
	    msg.EUS_MessageID,
	    msg.Subject,
	    msg.ToProfileID,
	    msg.FromProfileID,
	    msg1.MAXDateTimeToCreate, 
	    ItemsCountRead = 0,
        IsReceived = 1,
        StatusID = 1
    from EUS_Messages msg
        with (nolock)
    inner join EUS_Profiles prof on prof.ProfileId = msg.FromProfileID
    inner join (
	    select MAX(msg1.DateTimeToCreate) as MAXDateTimeToCreate,msg1.FromProfileID
		    from EUS_Messages  msg1
            with (nolock)
		    where	msg1.ToProfileID=@CurrentProfileId
		    and msg1.IsReceived=1   -- incoming, unread or read
            and ISNULL(msg1.IsHidden,0)=0
		    group by msg1.FromProfileID
	    )  as msg1 on msg1.MAXDateTimeToCreate= msg.DateTimeToCreate and prof.ProfileId = msg1.FromProfileID
    where msg.ToProfileID=@CurrentProfileId
    and msg.IsReceived=1  -- incoming, unread or read
    and ISNULL(msg.IsHidden,0)=0
    AND  not exists(
        select * 
	    from EUS_ProfilesBlocked bl 
            with (nolock)
	    where (bl.FromProfileID =prof.ProfileID and bl.ToProfileID = @CurrentProfileId) or
		    (bl.FromProfileID =@CurrentProfileId and bl.ToProfileID = prof.ProfileID)
    )
    and (
        (@showActive is null) or
        (@showActive=1 and prof.Status=4) or
        (@showActive=0 and prof.Status<>4) 
    )

[AdditionalWhereClause]

) as tRowNumber 
inner join EUS_Profiles prof on prof.ProfileId = tRowNumber.FromProfileID
LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = prof.ProfileID AND phot.CustomerPhotosID = prof.DefPhotoID

        )
    ) as tUnion
) as tOutterUnion
where 
	tOutterUnion.RowNumber > @RowNumberMin and
	tOutterUnion.RowNumber <= @RowNumberMax 
order by tOutterUnion.RowNumber
]]></sql>.Value

        End If

        Return sql
    End Function


    Public Shared Function GetMessagesDataTable(ByRef parms As clsMessagesHelperParameters) As DataSet

        Dim __logdate As DateTime = DateTime.UtcNow

        Dim CurrentProfileId As Integer = parms.CurrentProfileId
        Dim MessagesView As MessagesViewEnum = parms.MessagesView
        Dim cmdFilter As String = parms.cmdFilter
        Dim showActive As Boolean? = parms.showActive

        Dim sql As String = ""
        Dim dt As New DataSet()

        'This script is based on kilometer. If you use miles simply convert the radius to kilometer (radius=radius*1.609344)
        Try

            If (MessagesViewEnum.INBOX = MessagesView) Then
                ' incoming messages

                parms.AdditionalWhereClause = <sql><![CDATA[
and ISNULL(msg.IsDeleted, 0)=0
{0}
]]></sql>.Value


                If (parms.performCount) Then
                    sql = GetINBOXMessagesSQL(enmMessagesSqlType.CounterFast)
                Else
                    sql = ""
                End If

                sql = sql & GetINBOXMessagesSQL(enmMessagesSqlType.None)

            ElseIf (MessagesViewEnum.SENT = MessagesView) Then
                ' sent messages
                parms.AdditionalWhereClause = <sql><![CDATA[
and ISNULL(msg.IsDeleted, 0)=0
{0}
]]></sql>.Value

                If (parms.performCount) Then
                    sql = GetSENTMessagesSQL(enmMessagesSqlType.CounterFast)
                Else
                    sql = ""
                End If

                sql = sql & GetSENTMessagesSQL(enmMessagesSqlType.None)

            ElseIf (MessagesViewEnum.TRASH = MessagesView) Then
                ' trash messages -- start

                parms.AdditionalWhereClause = <sql><![CDATA[
and ISNULL(msg.IsDeleted, 0)=1
{0}
]]></sql>.Value

                If (parms.performCount) Then


                    sql = <sql><![CDATA[
select CountMsgs=sum(CounterTbl.CountMsgs) 
from
(
    (
]]></sql>.Value & _
             GetSENTMessagesSQL(enmMessagesSqlType.CounterFast) & _
    <sql><![CDATA[
    )
    UNION ALL(

]]></sql>.Value & _
            GetINBOXMessagesSQL(enmMessagesSqlType.CounterFast) & _
    <sql><![CDATA[
    )
    UNION ALL(

]]></sql>.Value & _
            GetNEWMessagesSQL(enmMessagesSqlType.CounterFast) & _
    <sql><![CDATA[
    )
) as CounterTbl
]]></sql>.Value

                Else
                    sql = ""
                End If




                sql = sql & <sql><![CDATA[

if(@NumberOfRecordsToReturn>0)
    SET ROWCOUNT @NumberOfRecordsToReturn;

select 
   tOutterUnion.* 
from(
    select
	    RowNumber = Row_Number() over([QUERY_ORDERING])
        ,tUnion.* 
    from
    (
        (
        -- sent messages
]]></sql>.Value & _
         GetSENTMessagesSQL(enmMessagesSqlType.UnionFast) & _
<sql><![CDATA[
        )
        UNION (

         -- received and read messages

]]></sql>.Value & _
        GetINBOXMessagesSQL(enmMessagesSqlType.UnionFast) & _
<sql><![CDATA[

        )
        UNION (

         -- new messages

]]></sql>.Value & _
        GetNEWMessagesSQL(enmMessagesSqlType.UnionFast) & _
<sql><![CDATA[
        )
    ) as tUnion
) as tOutterUnion
where 
	tOutterUnion.RowNumber > @RowNumberMin and
	tOutterUnion.RowNumber <= @RowNumberMax 
order by tOutterUnion.RowNumber
]]></sql>.Value

                ' trash messages -- end


            Else
                ' new messages


                parms.AdditionalWhereClause = <sql><![CDATA[
and ISNULL(msg.IsDeleted, 0)=0
{0}
]]></sql>.Value

                If (parms.performCount) Then
                    'sql = "declare @CountMsgs int=0" & vbCrLf
                    'sql = sql & GetNEWMessagesSQL(enmMessagesSqlType.Counter) & vbCrLf
                    'sql = sql & "select @CountMsgs as CountMsgs;" & vbCrLf
                    sql = "declare @CountMsgs int=0" & vbCrLf
                    sql = sql & "select @CountMsgs=(" & GetNEWMessagesSQL(enmMessagesSqlType.CounterFast) & ")" & vbCrLf
                    sql = sql & "select @CountMsgs as CountMsgs;" & vbCrLf
                Else
                    sql = ""
                End If


                sql = sql & GetNEWMessagesSQL(enmMessagesSqlType.None)
            End If


            If (Not String.IsNullOrEmpty(parms.AdditionalWhereClause)) Then

                If (cmdFilter = "UNREAD") Then
                    parms.AdditionalWhereClause = String.Format(parms.AdditionalWhereClause, "and StatusID = 0")
                ElseIf (cmdFilter = "READ") Then
                    parms.AdditionalWhereClause = String.Format(parms.AdditionalWhereClause, "and StatusID = 1")
                Else
                    parms.AdditionalWhereClause = String.Format(parms.AdditionalWhereClause, "")
                End If

                sql = sql.Replace("[AdditionalWhereClause]", vbCrLf & parms.AdditionalWhereClause)
                'prms.performCount = False
                'NumberOfRecordsToReturn = 0
            Else
                sql = sql.Replace("[AdditionalWhereClause]", "")
            End If


            Dim sqlOrderBy As String = ""
            If (parms.MessagesSort = MessagesSortEnum.Oldest) Then
                sqlOrderBy = vbCrLf & _
                    "Order By MAXDateTimeToCreate asc"
            Else
                sqlOrderBy = vbCrLf & _
                    "Order By MAXDateTimeToCreate desc"
            End If

            sql = sql.Replace("[QUERY_ORDERING]", sqlOrderBy)


            Dim command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql)
            command.Parameters.AddWithValue("@CurrentProfileId", CurrentProfileId)
            command.Parameters.AddWithValue("@NumberOfRecordsToReturn", parms.NumberOfRecordsToReturn)
            command.Parameters.AddWithValue("@RowNumberMin", parms.rowNumberMin)
            command.Parameters.AddWithValue("@RowNumberMax", parms.rowNumberMax)

            If (showActive.HasValue) Then
                command.Parameters.AddWithValue("@showActive", showActive)
            Else
                command.Parameters.AddWithValue("@showActive", System.DBNull.Value)
            End If

            dt = DataHelpers.GetDataSet(command)


        Catch ex As Exception
            Throw New System.Exception(ex.Message, ex)
            clsLogger.InsertLog("GetMessagesDataTable", CurrentProfileId, (DateTime.UtcNow - __logdate).TotalMilliseconds)
        End Try

        Return dt
    End Function



    Public Shared Function GetMessagesLeftDataTable(ByRef parms As clsMessagesHelperParameters) As DataSet

        Dim __logdate As DateTime = DateTime.UtcNow

        Dim CurrentProfileId As Integer = parms.CurrentProfileId
        Dim MessagesView As MessagesViewEnum = parms.MessagesView
        Dim cmdFilter As String = parms.cmdFilter
        Dim showActive As Boolean? = parms.showActive

        Dim sql As String = ""
        Dim dt As New DataSet()

        'This script is based on kilometer. If you use miles simply convert the radius to kilometer (radius=radius*1.609344)
        Try


            ' new messages


            parms.AdditionalWhereClause = <sql><![CDATA[
and ISNULL(msg.IsDeleted, 0)=0
{0}
]]></sql>.Value
            If (parms.MessagesView = MessagesViewEnum.NEWMESSAGES) Then

                If (parms.performCount) Then
                    sql = "declare @CountMsgs int=0" & vbCrLf
                    sql = sql & "select @CountMsgs=(" & GetNEWMessagesSQL(enmMessagesSqlType.CounterFast) & ")" & vbCrLf
                    sql = sql & "select @CountMsgs as CountMsgs;" & vbCrLf
                    sql = sql & vbCrLf & "select @RowNumberMax=@CountMsgs,@RowNumberMin=0; " & vbCrLf


                    '                sql = <sql><![CDATA[
                    'declare @CountMsgs int=0
                    'select @CountMsgs=sum(CounterTbl.CountMsgs) 
                    'from
                    '(
                    '    (
                    ']]></sql>.Value & _
                    '         GetSENTMessagesSQL(enmMessagesSqlType.CounterFast) & _
                    '<sql><![CDATA[
                    '    )
                    '    UNION ALL(

                    ']]></sql>.Value & _
                    '        GetINBOXMessagesSQL(enmMessagesSqlType.CounterFast) & _
                    '<sql><![CDATA[
                    '    )
                    '    UNION ALL(

                    ']]></sql>.Value & _
                    '        GetNEWMessagesSQL(enmMessagesSqlType.CounterFast) & _
                    '<sql><![CDATA[
                    '    )
                    ') as CounterTbl

                    'select @CountMsgs as CountMsgs;
                    ']]></sql>.Value


                    'sql = "declare @CountMsgs int=0" & vbCrLf
                    'sql = sql & GetNEWMessagesSQL(enmMessagesSqlType.Counter) & vbCrLf
                    'sql = sql & "select @CountMsgs as CountMsgs;" & vbCrLf

                Else
                    sql = ""
                End If
                sql = sql & GetNEWMessagesSQL(enmMessagesSqlType.UnionFast)

            ElseIf (parms.MessagesView = MessagesViewEnum.INBOX) Then

                If (parms.performCount) Then
                    sql = "declare @CountMsgs int=0" & vbCrLf
                    sql = sql & "select @CountMsgs=(" & GetINBOXMessagesSQL(enmMessagesSqlType.CounterFast) & ")" & vbCrLf
                    sql = sql & "select @CountMsgs as CountMsgs;" & vbCrLf
                    sql = sql & vbCrLf & "select @RowNumberMax=@CountMsgs,@RowNumberMin=0; " & vbCrLf
                Else
                    sql = ""
                End If
                sql = sql & GetINBOXMessagesSQL(enmMessagesSqlType.UnionFast)

            End If

            If (Not String.IsNullOrEmpty(parms.AdditionalWhereClause)) Then

                If (cmdFilter = "UNREAD") Then
                    parms.AdditionalWhereClause = String.Format(parms.AdditionalWhereClause, "and StatusID = 0")
                ElseIf (cmdFilter = "READ") Then
                    parms.AdditionalWhereClause = String.Format(parms.AdditionalWhereClause, "and StatusID = 1")
                Else
                    parms.AdditionalWhereClause = String.Format(parms.AdditionalWhereClause, "")
                End If

                sql = sql.Replace("[AdditionalWhereClause]", vbCrLf & parms.AdditionalWhereClause)
            Else
                sql = sql.Replace("[AdditionalWhereClause]", "")
            End If


            Dim sqlOrderBy As String = ""
            If (parms.MessagesSort = MessagesSortEnum.Oldest) Then
                sqlOrderBy = vbCrLf & _
                    "Order By MAXDateTimeToCreate asc"
            Else
                sqlOrderBy = vbCrLf & _
                    "Order By MAXDateTimeToCreate desc"
            End If

            sql = sql.Replace("[QUERY_ORDERING]", sqlOrderBy)


            Dim command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql)
            command.Parameters.AddWithValue("@CurrentProfileId", CurrentProfileId)
            command.Parameters.AddWithValue("@NumberOfRecordsToReturn", parms.NumberOfRecordsToReturn)
            command.Parameters.AddWithValue("@RowNumberMin", parms.rowNumberMin)
            command.Parameters.AddWithValue("@RowNumberMax", parms.rowNumberMax)

            If (showActive.HasValue) Then
                command.Parameters.AddWithValue("@showActive", showActive)
            Else
                command.Parameters.AddWithValue("@showActive", System.DBNull.Value)
            End If

            dt = DataHelpers.GetDataSet(command)


        Catch ex As Exception
            Throw New System.Exception(ex.Message, ex)
        Finally
            clsLogger.InsertLog("GetMessagesDataTable", CurrentProfileId, (DateTime.UtcNow - __logdate).TotalMilliseconds)
        End Try

        Return dt
    End Function


    Public Shared Function DeleteMessagesFromMemberInView(ProfileIdOwner As Integer, FromProfileID As Integer, ToProfileID As Integer, MessagesView As MessagesViewEnum, OnDelete_MakeHidden As Boolean) As Integer
        Dim __logdate As DateTime = DateTime.UtcNow

        Dim result As Integer = 0
        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try

            If (ProfileIdOwner > 0) Then

                result = _CMSDBDataContext.EUS_Messages_DeleteMessagesInView2(ProfileIdOwner,
                                                                              FromProfileID,
                                                                              ToProfileID,
                                                                              MessagesView.ToString(),
                                                                              OnDelete_MakeHidden,
                                                                              Nothing,
                                                                              Nothing,
                                                                              Nothing,
                                                                              Nothing)

            End If

        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
            clsLogger.InsertLog("DeleteMessagesFromMemberInView", ProfileIdOwner, (DateTime.UtcNow - __logdate).TotalMilliseconds)
        End Try

        Return result
    End Function


    Public Shared Function COUNT_DeleteMessagesFromMemberInView(ProfileIdOwner As Integer,
                                                                FromProfileID As Integer,
                                                                ToProfileID As Integer,
                                                                MessagesView As MessagesViewEnum,
                                                                OnDelete_MakeHidden As Boolean) As EUS_Messages_DeleteMessagesInView2_CountResult

        Dim __logdate As DateTime = DateTime.UtcNow

        Dim result As EUS_Messages_DeleteMessagesInView2_CountResult = Nothing
        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try

            If (ProfileIdOwner > 0) Then

                result = _CMSDBDataContext.EUS_Messages_DeleteMessagesInView2_Count(ProfileIdOwner,
                                                                              FromProfileID,
                                                                              ToProfileID,
                                                                              MessagesView.ToString(),
                                                                              OnDelete_MakeHidden,
                                                                              Nothing,
                                                                              Nothing,
                                                                              Nothing,
                                                                              Nothing).FirstOrDefault()

            End If

        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
            clsLogger.InsertLog("COUNT_DeleteMessagesFromMemberInView", ProfileIdOwner, (DateTime.UtcNow - __logdate).TotalMilliseconds)
        End Try

        Return result
    End Function

    Public Shared Function DeleteMessages_ClearHistory(ProfileIdOwner As Integer,
                                                       OnDelete_MakeHidden As Boolean,
                                                       ClearIncomingMessages As Boolean?,
                                                       ClearOutgoingMessages As Boolean?,
                                                       ClearMessagesDateUntil As DateTime) As Integer
        Dim __logdate As DateTime = DateTime.UtcNow

        Dim result As Integer = 0
        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try

            If (ProfileIdOwner > 0) Then

                result = _CMSDBDataContext.EUS_Messages_DeleteMessagesInView2(ProfileIdOwner,
                                                                              0,
                                                                              0,
                                                                              "",
                                                                              OnDelete_MakeHidden,
                                                                              ClearIncomingMessages,
                                                                              ClearOutgoingMessages,
                                                                              ClearMessagesDateUntil,
                                                                              Nothing)

            End If

        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
            clsLogger.InsertLog("DeleteMessages_ClearHistory", ProfileIdOwner, (DateTime.UtcNow - __logdate).TotalMilliseconds)
        End Try

        Return result
    End Function
    Public Shared Function COUNT_DeleteMessages_ClearHistory(ProfileIdOwner As Integer,
                                                           OnDelete_MakeHidden As Boolean,
                                                           ClearIncomingMessages As Boolean?,
                                                           ClearOutgoingMessages As Boolean?,
                                                           ClearMessagesDateUntil As DateTime) As EUS_Messages_DeleteMessagesInView2_CountResult
        Dim __logdate As DateTime = DateTime.UtcNow
        Dim result As EUS_Messages_DeleteMessagesInView2_CountResult = Nothing
        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try

            If (ProfileIdOwner > 0) Then

                result = _CMSDBDataContext.EUS_Messages_DeleteMessagesInView2_Count(ProfileIdOwner,
                                                                                      0,
                                                                                      0,
                                                                                      "",
                                                                                      OnDelete_MakeHidden,
                                                                                      ClearIncomingMessages,
                                                                                      ClearOutgoingMessages,
                                                                                      ClearMessagesDateUntil,
                                                                                      Nothing).FirstOrDefault()

            End If

        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
            clsLogger.InsertLog("COUNT_DeleteMessages_ClearHistory", ProfileIdOwner, (DateTime.UtcNow - __logdate).TotalMilliseconds)
        End Try

        Return result
    End Function

End Class

﻿Imports Dating.Server.Core.DLL
Imports System.Web


Public Class clsPricing

    Private Shared Property _PricesList As New List(Of EUS_Price)()

    Public Shared Property VATCountries As String() = {"AT", "BE", "BG", "CY", "CZ", "DK", "EE", "FI", "FR", "DE", "GR", "HU", "IE", "IT", "LV", "LT", "LU", "MT", "NL", "PL", "PT", "RO", "SK", "SI", "ES", "SE", "GB", "UK"}

    Public Shared Function GetPriceForDisplayIndex(index As Integer, Optional currency As String = "EUR") As EUS_Price
        Dim countryPrice As New EUS_Price()

        If (_PricesList.Count = 0) Then
            Load()
        End If

        Dim cnt2 As Integer = 0
        For cnt2 = 0 To _PricesList.Count - 1
            If (_PricesList(cnt2).Currency = currency AndAlso _PricesList(cnt2).DisplayIndex = index) Then
                countryPrice = _PricesList(cnt2)
                Exit For
            End If
        Next

        Return countryPrice
    End Function
    Public Shared Function GetPriceForProductCode(ProductCode As String) As EUS_Price
        Dim countryPrice As New EUS_Price()

        If (_PricesList.Count = 0) Then
            Load()
        End If
        ProductCode = ProductCode.ToUpper()
        Dim cnt2 As Integer = 0
        For cnt2 = 0 To _PricesList.Count - 1
            If (_PricesList(cnt2).ProductCode = ProductCode) Then
                countryPrice = _PricesList(cnt2)
                Exit For
            End If
        Next

        Return countryPrice
    End Function


    Public Shared Function GetPriceForProductCode_CopyResult(ProductCode As String) As EUS_Price
        Dim _Price As EUS_Price
        ProductCode = ProductCode.ToUpper()

        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try
            _Price = (From itm In _CMSDBDataContext.EUS_Prices
                    Where itm.ProductCode.ToUpper() = ProductCode AndAlso itm.DisplayIndex > 0
                    Select itm).FirstOrDefault()



        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        Finally
            _CMSDBDataContext.Dispose()
        End Try


        Return _Price
    End Function


    Public Shared Function GetPricesListForCurrency(Optional currency As String = "EUR") As List(Of EUS_Price)
        Dim countryPrice As New List(Of EUS_Price)()

        If (_PricesList.Count = 0) Then
            Load()
        End If

        Dim cnt2 As Integer = 0
        For cnt2 = 0 To _PricesList.Count - 1
            If (_PricesList(cnt2).Currency = currency) Then
                countryPrice.Add(_PricesList(cnt2))
            End If
        Next

        Return countryPrice
    End Function


    Shared Sub Load()

        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try
            Dim _Price As List(Of EUS_Price) = (From itm In _CMSDBDataContext.EUS_Prices
                                                Order By itm.Currency Ascending, itm.DisplayIndex Ascending
                                                Select itm).ToList()


            Dim cnt As Integer = 0
            For cnt = 0 To _Price.Count - 1
                If (_Price(cnt).DisplayIndex < 1) Then Continue For
                If (_Price(cnt).ProductCode IsNot Nothing) Then _Price(cnt).ProductCode = _Price(cnt).ProductCode.ToUpper()
                _PricesList.Add(_Price(cnt))
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        Finally
            _CMSDBDataContext.Dispose()
        End Try

    End Sub


    Public Shared Sub ClearCache()
        Try
            _PricesList.Clear()
        Catch ex As Exception

        End Try
    End Sub


    Public Shared Function GetProductCode(creditsAmount As String, type As String, CustomerCountry As String) As String
        Dim productCode As String = Nothing

        If (Not String.IsNullOrEmpty(creditsAmount) AndAlso Not String.IsNullOrEmpty(type)) Then
            productCode = "dd" & creditsAmount & type
            Select Case (CustomerCountry)
                Case "AL" : productCode = "dd" & creditsAmount & type & "LEK" '"EUR" '"LEK"
                Case "TR" : productCode = "dd" & creditsAmount & type & "TRY" '"EUR" '"LEK"
            End Select
        End If

        Return productCode
    End Function


    Public Shared Function GetCurrency(countryCode As String) As String
        Dim Currency As String = "EUR"

        Select Case (countryCode)
            Case "AL" : Currency = "LEK" ' "EUR"
            Case "TR" : Currency = "TRY" ' "EUR"
        End Select

        Return Currency
    End Function


    Public Shared Function GetRandomPaymentProviderPercent(country As String) As Integer

        Dim payment_provider_ratio As Integer
        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try
            Dim ctr As SYS_CountriesGEO = (From itm In _CMSDBDataContext.SYS_CountriesGEOs
                                          Where itm.Iso = country.ToUpper()
                                          Select itm).FirstOrDefault()

            If (ctr IsNot Nothing) Then
                If (ctr.PaymentCommisionMIN.HasValue AndAlso ctr.PaymentCommisionMAX.HasValue) Then
                    Dim _min As Integer = ctr.PaymentCommisionMIN
                    Dim _max As Integer = ctr.PaymentCommisionMAX

                    Dim rnd As New Random(System.DateTime.Now.Millisecond)
                    If _min > _max Then
                        Dim t As Integer = _min
                        _min = _max
                        _max = t
                    End If
                    payment_provider_ratio = rnd.Next(_min, _max)

                ElseIf (ctr.PaymentCommisionMIN.HasValue) Then
                    payment_provider_ratio = ctr.PaymentCommisionMIN

                ElseIf (ctr.PaymentCommisionMAX.HasValue) Then
                    payment_provider_ratio = ctr.PaymentCommisionMAX

                End If

            End If


        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        Finally
            _CMSDBDataContext.Dispose()
        End Try

        Return payment_provider_ratio
    End Function


End Class

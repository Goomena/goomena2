﻿Imports System.Text.RegularExpressions
Imports System.Text


Public Class clsSearchHelperParameters
    Public Property CurrentProfileId As Integer
    Public Property ReturnRecordsWithStatus As Integer
    Public Property SearchSort As SearchSortEnum
    Public Property zipstr As String
    Public Property Distance As Integer
    Public Property LookingFor_ToMeetMaleID As Boolean
    Public Property LookingFor_ToMeetFemaleID As Boolean
    Public Property NumberOfRecordsToReturn As Integer = 0
    Public Property AdditionalWhereClause As String = ""
    Public Property latitudeIn As Double?
    Public Property longitudeIn As Double?
    Public Property performCount As Boolean
    Public Property Country As String
    Public Property rowNumberMin As Integer = 0
    Public Property rowNumberMax As Integer = 0

    Public Property GenderId As Integer
    Public Property ShowOnFrontPageRequired As Boolean?

End Class



Public Class clsWinksHelperParameters

    Public Property CurrentProfileId As Integer
    Public Property ReturnRecordsWithStatus As Integer
    Public Property sorting As OffersSortEnum
    Public Property zipstr As String
    Public Property latitudeIn As Double?
    Public Property longitudeIn As Double?
    Public Property Distance As Integer = clsSearchHelper.DISTANCE_DEFAULT
    Public Property NumberOfRecordsToReturn As Integer = 0
    Public Property performCount As Boolean = False

    Public Property rowNumberMin As Integer = 0
    Public Property rowNumberMax As Integer = 0
    Public Property AdditionalWhereClause As String = ""

End Class


Public Class clsDatesHelperParameters

    Public Property CurrentProfileId As Integer
    Public Property ReturnRecordsWithStatus As Integer
    Public Property sorting As DatesSortEnum
    Public Property zipstr As String
    Public Property latitudeIn As Double?
    Public Property longitudeIn As Double?
    Public Property Distance As Integer = clsSearchHelper.DISTANCE_DEFAULT
    Public Property NumberOfRecordsToReturn As Integer
    Public Property performCount As Boolean

    Public Property rowNumberMin As Integer
    Public Property rowNumberMax As Integer
    Public Property AdditionalWhereClause As String = ""
    Public Property OtherProfileId As Integer
End Class


Public Class clsMyListsHelperParameters

    Public Property CurrentProfileId As Integer
    Public Property ReturnRecordsWithStatus As Integer
    Public Property sorting As MyListsSortEnum
    Public Property zipstr As String
    Public Property latitudeIn As Double?
    Public Property longitudeIn As Double?
    Public Property Distance As Integer = clsSearchHelper.DISTANCE_DEFAULT
    Public Property NumberOfRecordsToReturn As Integer = 0
    Public Property performCount As Boolean = False

    Public Property rowNumberMin As Integer = 0
    Public Property rowNumberMax As Integer = 0
    Public Property AdditionalWhereClause As String = ""
    Public Property isOnline As Boolean
End Class


Public Class clsSearchHelper

    Public Const DISTANCE_DEFAULT = 1000000


    Public Shared Function GetMembersToSearchDataTable(prms As clsSearchHelperParameters) As DataSet

        'clsLogger.InsertLog("GetMembersToSearchDataTable", prms.CurrentProfileId)
        Dim __logdate As DateTime = DateTime.UtcNow

        Dim CurrentProfileId As Integer = prms.CurrentProfileId
        Dim ReturnRecordsWithStatus As Integer = prms.ReturnRecordsWithStatus
        Dim SearchSort As SearchSortEnum = prms.SearchSort
        'Dim zipstr As String = prms.zipstr
        Dim Distance As Integer = prms.Distance
        Dim LookingFor_ToMeetMaleID As Boolean = prms.LookingFor_ToMeetMaleID
        Dim LookingFor_ToMeetFemaleID As Boolean = prms.LookingFor_ToMeetFemaleID
        Dim NumberOfRecordsToReturn As Integer = prms.NumberOfRecordsToReturn
        'Dim AdditionalWhereClause As String = prms.AdditionalWhereClause


        If (Distance = 0) Then
            Distance = DISTANCE_DEFAULT
        End If

        Dim sql As String = ""
        Dim dt As New DataSet()

        'This script is based on kilometer. If you use miles simply convert the radius to kilometer (radius=radius*1.609344)
        Try

            sql = <sql><![CDATA[

--fn:GetMembersToSearchDataTable
	--@CurrentProfileId int
	--,@ReturnRecordsWithStatus int
	--,@NumberOfRecordsToReturn int =0
	--,@Distance int=0

	SET NOCOUNT ON;


declare @Country varchar(10)
declare @LookingFor_ToMeetMaleID bit
declare @LookingFor_ToMeetFemaleID bit
declare @MirrorProfileID int

select 
    @MirrorProfileID=MirrorProfileID,
    @Country=Country, 
    @LookingFor_ToMeetMaleID=LookingFor_ToMeetMaleID, 
    @LookingFor_ToMeetFemaleID=LookingFor_ToMeetFemaleID 
from Eus_Profiles 
where ProfileID=@CurrentProfileId

if(@performCount=1)
begin

    select Count(*) from (
	    SELECT    
		    distance =isNull(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)), 1000)
	    FROM		
            dbo.EUS_Profiles AS EUS_Profiles 
		    with (nolock)
	    --LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.CustomerPhotosID = EUS_Profiles.DefPhotoID
		--LEFT OUTER JOIN dbo.EUS_ProfilesPrivacySettings AS pps ON (pps.ProfileID=EUS_Profiles.ProfileID) and (isnull(pps.HideMeFromUsersInDifferentCountry,0)=1)
        WHERE
		    EUS_Profiles.IsMaster = 1
	    AND EUS_Profiles.Profileid <> 1
	    AND EUS_Profiles.ProfileID <> @CurrentProfileId
	    AND EUS_Profiles.ProfileID <> @MirrorProfileID
	    AND EUS_Profiles.Status = @ReturnRecordsWithStatus
	    AND ISNULL(EUS_Profiles.PrivacySettings_HideMeFromSearchResults,0) = 0
	    AND (
			    (@LookingFor_ToMeetMaleID= 1    AND EUS_Profiles.GenderId = 1)
		    OR
			    (@LookingFor_ToMeetFemaleID = 1 AND EUS_Profiles.GenderId = 2)
	    )
	    AND  not exists(select * 
						from EUS_ProfilesBlocked bl 
                        with (nolock) 
						where (bl.FromProfileID =EUS_Profiles.ProfileID and bl.ToProfileID = @CurrentProfileId) or
							(bl.FromProfileID =@CurrentProfileId and bl.ToProfileID = EUS_Profiles.ProfileID)
	    )
	    AND (  
            EUS_Profiles.Country = @Country OR 
            (
                EUS_Profiles.Country <> @Country AND
                not exists(select * 
					        from EUS_ProfilesPrivacySettings bl 
                            with (nolock) 
					        where (bl.ProfileID =EUS_Profiles.ProfileID) and (isnull(HideMeFromUsersInDifferentCountry,0)=1)
                )
            )
        )
[AdditionalWhereClause]
    ) as EUS_Profiles
    where 
      ((distance <= @Distance and @Distance <@DISTANCE_DEFAULT)  OR ((distance <= @DISTANCE_DEFAULT) and @Distance =@DISTANCE_DEFAULT))
end

	--if(@NumberOfRecordsToReturn>0)
	--	SET ROWCOUNT @NumberOfRecordsToReturn;



select 
	tOutter.*
    ,CASE 
		WHEN ISNULL(tOutter.Birthday, '') = '' then null
		ELSE dbo.fn_GetAge(tOutter.Birthday, GETDATE())
	End as Age
    ,IsOnlineNow=CAST((CASE
            WHEN exists(select ProfileID 
                from [EUS_ProfilesPrivacySettings]
                where (ProfileID=tOutter.ProfileID OR [MirrorProfileID]=tOutter.ProfileID)
                and [PrivacySettings_ShowMeOffline]=1) THEN 0
            WHEN not @LastActivityUTCDate is null and (tOutter.IsOnline=1 and tOutter.LastActivityDateTime>=@LastActivityUTCDate) THEN 1
            ELSE 0
        END) as bit)
    ,HasFavoritedMe = case 
		when (exists (select *
						from 	dbo.EUS_ProfilesFavorite favMe
						with (nolock)
						where   favMe.ToProfileID = @CurrentProfileId AND favMe.FromProfileID = tOutter.ProfileID))
        then 1
        else 0
    end
    ,DidIFavorited = case 
		when (exists (select *
                        from 	dbo.EUS_ProfilesFavorite favMe
                        with (nolock)
                        where   favMe.ToProfileID = tOutter.ProfileID AND favMe.FromProfileID = @CurrentProfileId))
        then 1
        else 0
    end
    ,CommunicationUnl = case 
		when (exists (select *
                        from 	dbo.EUS_UnlockedConversations unl
                        with (nolock)
                        where   ( unl.FromProfileID = @CurrentProfileId AND unl.ToProfileID = tOutter.ProfileID) OR
	                            ( unl.FromProfileID = tOutter.ProfileID  AND unl.ToProfileID = @CurrentProfileId)))
        then 1
        else 0
    end
    ,WantJob = case 
        when tOutter.GenderID=2 then (select top(1) lt.JobID  from dbo.EUS_ProfileJobs pj inner join dbo.EUS_LISTS_Job lt on lt.JobID=pj.JobID where ProfileID=tOutter.ProfileID)
        else null
    end
    ,BreastSizeID = case 
        when tOutter.GenderID=2 then (select top(1) b.BreastSizeID from dbo.EUS_ProfileBreastSize b where b.ProfileID=tOutter.ProfileID)
        else null
    end
    ,IsAnyWinkOrIsAnyOffer = (SELECT top(1) [EUS_Offers].OfferID 
		FROM [dbo].[EUS_Offers]
		WHERE (([ToProfileID]=tOutter.ProfileID AND [FromProfileID]=@CurrentProfileId) OR ([ToProfileID]=@CurrentProfileId  AND [FromProfileID]=tOutter.ProfileID))
		AND ([OfferTypeID]=1 OR [OfferTypeID]=10  OR [OfferTypeID]= 11)
		--WINK,OFFERNEW,OFFERCOUNTER
	)
    ,ProfilesCommunication_IsAnySentMessage = (SELECT TOP (1) [SentMessage]
		FROM [dbo].[EUS_ProfilesCommunication]
		WHERE ([FromProfileID] = tOutter.ProfileID AND [ToProfileID] = @CurrentProfileId) or 
				([FromProfileID] = @CurrentProfileId AND [ToProfileID] = tOutter.ProfileID)
	)
    ,ProfilesCommunication_IsAnySentLike = (SELECT TOP (1) SentLike
		FROM [dbo].[EUS_ProfilesCommunication]
		WHERE ([FromProfileID] = tOutter.ProfileID AND [ToProfileID] = @CurrentProfileId) or 
				([FromProfileID] = @CurrentProfileId AND [ToProfileID] = tOutter.ProfileID)
	)
    ,ProfilesCommunication_IsAnySentOffer = (SELECT TOP (1) SentOffer
		FROM [dbo].[EUS_ProfilesCommunication]
		WHERE ([FromProfileID] = tOutter.ProfileID AND [ToProfileID] = @CurrentProfileId) or 
				([FromProfileID] = @CurrentProfileId AND [ToProfileID] = tOutter.ProfileID)
	)
    ,IsAnyLastOffer = (SELECT TOP (1) [OfferID]
        FROM [dbo].[EUS_Offers] 
        WHERE (([ToProfileID]=tOutter.ProfileID AND [FromProfileID]=@CurrentProfileId) OR ([ToProfileID]=@CurrentProfileId AND [FromProfileID]=tOutter.ProfileID)) 
        AND ([OfferTypeID]=10 OR [OfferTypeID]=11) 
        AND ([ChildOfferID]=0)
        --10=OFFERNEW, 11=OFFERCOUNTER
	)
from (
    select 
	    RowNumber = Row_Number() over([QueryOrdering])
	    ,tInner.*
    from(
        select 
            [SELECT-LIST]
        from (
	        SELECT    
		        distance    = cast(isNull(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)), 1000) as int)
  		        ,minutes1   =	datediff(minute,'1990-1-1',EUS_Profiles.DateTimeToRegister),
                EUS_Profiles.PointsBeauty,    
		        EUS_Profiles.PointsVerification,     
		        EUS_Profiles.PointsCredits,     
                EUS_Profiles.PointsUnlocks,
                EUS_Profiles.ProfileID, 
                EUS_Profiles.IsMaster, 
                EUS_Profiles.MirrorProfileID, 
                EUS_Profiles.Status,
                EUS_Profiles.LoginName, 
                EUS_Profiles.FirstName,
                EUS_Profiles.LastName, 
                EUS_Profiles.GenderId, 
                EUS_Profiles.Country, 
                EUS_Profiles.Region, 
                EUS_Profiles.City, 
                EUS_Profiles.Zip, 
                EUS_Profiles.CityArea, 
                EUS_Profiles.Address, 
                EUS_Profiles.Telephone, 
                EUS_Profiles.eMail, 
                EUS_Profiles.Cellular, 
                EUS_Profiles.AreYouWillingToTravel, 
                EUS_Profiles.AboutMe_Heading, 
                EUS_Profiles.AboutMe_DescribeYourself, 
                EUS_Profiles.AboutMe_DescribeAnIdealFirstDate, 
                EUS_Profiles.OtherDetails_EducationID, 
                EUS_Profiles.OtherDetails_AnnualIncomeID, 
                EUS_Profiles.OtherDetails_NetWorthID, 
                EUS_Profiles.OtherDetails_Occupation, 
                EUS_Profiles.PersonalInfo_HeightID, 
                EUS_Profiles.PersonalInfo_BodyTypeID, 
                EUS_Profiles.PersonalInfo_EyeColorID, 
                EUS_Profiles.PersonalInfo_HairColorID, 
                EUS_Profiles.PersonalInfo_ChildrenID, 
                EUS_Profiles.PersonalInfo_EthnicityID,
                EUS_Profiles.PersonalInfo_ReligionID, 
                EUS_Profiles.PersonalInfo_SmokingHabitID, 
                EUS_Profiles.PersonalInfo_DrinkingHabitID, 
                EUS_Profiles.LookingFor_ToMeetMaleID, 
                EUS_Profiles.LookingFor_ToMeetFemaleID, EUS_Profiles.LookingFor_RelationshipStatusID, 
                EUS_Profiles.LookingFor_TypeOfDating_ShortTermRelationship, 
                EUS_Profiles.LookingFor_TypeOfDating_Friendship,
                EUS_Profiles.LookingFor_TypeOfDating_LongTermRelationship, 
                EUS_Profiles.LookingFor_TypeOfDating_MutuallyBeneficialArrangements, 
                EUS_Profiles.LookingFor_TypeOfDating_MarriedDating, 
                EUS_Profiles.LookingFor_TypeOfDating_AdultDating_Casual, 
                EUS_Profiles.DateTimeToRegister, 
                EUS_Profiles.RegisterGEOInfos, 
                EUS_Profiles.LastLoginDateTime, 
                EUS_Profiles.LastLoginGEOInfos, 
                EUS_Profiles.LastUpdateProfileDateTime, 
                EUS_Profiles.LastUpdateProfileGEOInfo, 
                EUS_Profiles.LAGID, 
                EUS_Profiles.CustomReferrer, 
                EUS_Profiles.Birthday, 
                EUS_Profiles.Role, 
                EUS_Profiles.IsOnline, 
                EUS_Profiles.LastActivityDateTime, 
                EUS_Profiles.AvailableCredits, 
                EUS_Profiles.DefPhotoID, 
                EUS_Profiles.CelebratingBirth, 
                phot.CustomerPhotosID, 
                phot.CustomerID, 
                phot.DateTimeToUploading, 
                phot.FileName, 
                phot.DisplayLevel, 
                phot.HasAproved, phot.HasDeclined, 
                phot.IsDefault, 
	            [HasPhoto] = case 
			            when [EUS_Profiles].PhotosPublic > 0  then 1
			            when [EUS_Profiles].PhotosPrivate > 0  then 1
			            when phot.[CustomerID] IS NULL  then 0
			            else 1 
			        end
	        FROM		
                dbo.EUS_Profiles AS EUS_Profiles 
		        with (nolock)
	        LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.CustomerPhotosID = EUS_Profiles.DefPhotoID
            WHERE
		        EUS_Profiles.IsMaster = 1
	        AND EUS_Profiles.Profileid <> 1
	        AND EUS_Profiles.ProfileID <> @CurrentProfileId
	        AND EUS_Profiles.ProfileID <> @MirrorProfileID
	        AND EUS_Profiles.Status = @ReturnRecordsWithStatus
	        AND ISNULL(EUS_Profiles.PrivacySettings_HideMeFromSearchResults,0) = 0
	        AND (
			        (@LookingFor_ToMeetMaleID= 1    AND EUS_Profiles.GenderId = 1)
		        OR
			        (@LookingFor_ToMeetFemaleID = 1 AND EUS_Profiles.GenderId = 2)
	        )
	        AND  not exists(select * 
						    from EUS_ProfilesBlocked bl 
                            with (nolock) 
						    where (bl.FromProfileID =EUS_Profiles.ProfileID and bl.ToProfileID = @CurrentProfileId) or
							    (bl.FromProfileID =@CurrentProfileId and bl.ToProfileID = EUS_Profiles.ProfileID)
	        )
	        AND (  
                EUS_Profiles.Country = @Country OR 
                (
                    EUS_Profiles.Country <> @Country AND
                    not exists(select * 
					            from EUS_ProfilesPrivacySettings bl 
                                with (nolock) 
					            where (bl.ProfileID =EUS_Profiles.ProfileID) and (isnull(HideMeFromUsersInDifferentCountry,0)=1)
                    )
                )
            )
        [AdditionalWhereClause]
        ) as EUS_Profiles
        where 
          ((distance <= @Distance and @Distance <@DISTANCE_DEFAULT)  OR ((distance <= @DISTANCE_DEFAULT) and @Distance =@DISTANCE_DEFAULT))
    ) as tInner
) as tOutter
where 
	tOutter.RowNumber > @RowNumberMin and
	tOutter.RowNumber <= @RowNumberMax 
order by tOutter.RowNumber
]]></sql>.Value



            If (Not String.IsNullOrEmpty(prms.AdditionalWhereClause)) Then
                sql = sql.Replace("[AdditionalWhereClause]", vbCrLf & prms.AdditionalWhereClause)
            Else
                sql = sql.Replace("[AdditionalWhereClause]", "")
            End If



            Dim SELECT_LIST As String = "*"
            If (SearchSort = SearchSortEnum.NewestNearMember) Then

                SELECT_LIST = <sql><![CDATA[
	NewestNearMemberQualifier =  
		minutes1 / case 
						when distance=0 then 10
						when distance<10 then 10
						else isnull(distance, 1)
					end
	,* 
]]></sql>

            End If

            If (SearchSort <> SearchSortEnum.NewestNearMember) Then

                SELECT_LIST = <sql><![CDATA[
	CountryQualifier = case 
						    when Country=@Country then 1
						    else 0
					    end
	,* 
]]></sql>

            End If

            sql = sql.Replace("[SELECT-LIST]", SELECT_LIST)


            Dim sqlOrderBy As String = ""
            If (LookingFor_ToMeetFemaleID) Then
                If (SearchSort = SearchSortEnum.NewestMember) Then
                    sqlOrderBy = vbCrLf & _
                        "Order By HasPhoto desc, CountryQualifier desc, DateTimeToRegister Desc, distance asc, Birthday asc, PointsBeauty desc, PointsVerification desc, PointsUnlocks desc"
                ElseIf (SearchSort = SearchSortEnum.NewestNearMember) Then
                    sqlOrderBy = vbCrLf & _
                        "Order By HasPhoto desc, NewestNearMemberQualifier desc, DateTimeToRegister Desc, distance asc, Birthday asc, PointsBeauty desc, PointsVerification desc, PointsUnlocks desc"
                ElseIf (SearchSort = SearchSortEnum.OldestMember) Then
                    sqlOrderBy = vbCrLf & _
                        "Order By HasPhoto desc, CountryQualifier desc, DateTimeToRegister Asc, distance asc, Birthday asc, PointsBeauty desc, PointsVerification desc, PointsUnlocks desc"
                ElseIf (SearchSort = SearchSortEnum.RecentlyLoggedIn) Then
                    sqlOrderBy = vbCrLf & _
                        "Order By HasPhoto desc, CountryQualifier desc, LastLoginDateTime Desc, distance asc, Birthday asc, PointsBeauty desc, PointsVerification desc, PointsUnlocks desc"
                ElseIf (SearchSort = SearchSortEnum.Birthday) Then
                    sqlOrderBy = vbCrLf & _
                        "Order By CountryQualifier desc,HasPhoto desc,CelebratingBirth asc,LastLoginDateTime Desc, distance asc, Birthday asc, PointsBeauty desc, PointsVerification desc, PointsUnlocks desc"
                Else
                    'SearchSortEnum.NearestDistance
                    'SearchSortEnum.None
                    sqlOrderBy = vbCrLf & _
                        "order by HasPhoto desc, CountryQualifier desc, distance asc, Birthday asc, PointsBeauty desc, PointsVerification desc, PointsUnlocks desc"
                End If
            Else
                If (SearchSort = SearchSortEnum.NewestMember) Then
                    sqlOrderBy = vbCrLf & _
                        "Order By HasPhoto desc, CountryQualifier desc, DateTimeToRegister Desc, distance asc, PointsVerification desc, Birthday asc, PointsCredits desc, PointsUnlocks desc"
                ElseIf (SearchSort = SearchSortEnum.NewestNearMember) Then
                    sqlOrderBy = vbCrLf & _
                        "Order By HasPhoto desc, NewestNearMemberQualifier desc, DateTimeToRegister Desc, distance asc, PointsVerification desc, Birthday asc, PointsCredits desc, PointsUnlocks desc"
                ElseIf (SearchSort = SearchSortEnum.OldestMember) Then
                    sqlOrderBy = vbCrLf & _
                        "Order By HasPhoto desc, CountryQualifier desc, DateTimeToRegister Asc, distance asc, PointsVerification desc, Birthday asc, PointsCredits desc, PointsUnlocks desc"
                ElseIf (SearchSort = SearchSortEnum.RecentlyLoggedIn) Then
                    sqlOrderBy = vbCrLf & _
                        "Order By HasPhoto desc, CountryQualifier desc, LastLoginDateTime Desc, distance asc, PointsVerification desc, Birthday asc, PointsCredits desc, PointsUnlocks desc"
                ElseIf (SearchSort = SearchSortEnum.Birthday) Then
                    sqlOrderBy = vbCrLf & _
                        "Order By CountryQualifier desc,HasPhoto desc,CelebratingBirth asc,LastLoginDateTime Desc,distance asc, PointsVerification desc, Birthday asc, PointsCredits desc, PointsUnlocks desc"
                Else
                    'SearchSortEnum.NearestDistance
                    'SearchSortEnum.None
                    sqlOrderBy = vbCrLf & _
                        "order by HasPhoto desc, CountryQualifier desc, distance asc, PointsVerification desc, Birthday asc, PointsCredits desc, PointsUnlocks desc"
                End If
            End If


            sql = sql.Replace("[QueryOrdering]", sqlOrderBy)


            Dim command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql)
            command.Parameters.Add(New SqlClient.SqlParameter("@CurrentProfileId", CurrentProfileId))
            command.Parameters.Add(New SqlClient.SqlParameter("@ReturnRecordsWithStatus", ReturnRecordsWithStatus))
            command.Parameters.Add(New SqlClient.SqlParameter("@NumberOfRecordsToReturn", NumberOfRecordsToReturn))
            command.Parameters.Add(New SqlClient.SqlParameter("@Distance", Distance))
            command.Parameters.Add(New SqlClient.SqlParameter("@DISTANCE_DEFAULT", DISTANCE_DEFAULT))
            command.Parameters.Add(New SqlClient.SqlParameter("@performCount", prms.performCount))
            command.Parameters.Add(New SqlClient.SqlParameter("@RowNumberMin", prms.rowNumberMin))
            command.Parameters.Add(New SqlClient.SqlParameter("@RowNumberMax", prms.rowNumberMax))

            If (String.IsNullOrEmpty(prms.zipstr)) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@zip", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@zip", prms.zipstr))
            End If

            If (prms.latitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", prms.latitudeIn))
            End If

            If (prms.longitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", prms.longitudeIn))
            End If

            Try
                Dim mins As Integer = clsConfigValues.Get__members_online_minutes()
                Dim LastActivityUTCDate As DateTime = Date.UtcNow.AddMinutes(-mins)
                command.Parameters.Add(New SqlClient.SqlParameter("@LastActivityUTCDate", LastActivityUTCDate))
            Catch
                command.Parameters.Add(New SqlClient.SqlParameter("@LastActivityUTCDate", System.DBNull.Value))
            End Try

            dt = DataHelpers.GetDataSet(command)

        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        Finally
            clsLogger.InsertLog("GetMembersToSearchDataTable", prms.CurrentProfileId, (DateTime.UtcNow - __logdate).TotalMilliseconds)
        End Try


        Return dt
    End Function



    Public Shared Function GetMembersToSearchDataTable_MembersNear(prms As clsSearchHelperParameters) As DataSet

        'clsLogger.InsertLog("GetMembersToSearchDataTable", prms.CurrentProfileId)
        Dim __logdate As DateTime = DateTime.UtcNow

        Dim CurrentProfileId As Integer = prms.CurrentProfileId
        Dim ReturnRecordsWithStatus As Integer = prms.ReturnRecordsWithStatus
        Dim SearchSort As SearchSortEnum = prms.SearchSort
        'Dim zipstr As String = prms.zipstr
        Dim Distance As Integer = prms.Distance
        Dim LookingFor_ToMeetMaleID As Boolean = prms.LookingFor_ToMeetMaleID
        Dim LookingFor_ToMeetFemaleID As Boolean = prms.LookingFor_ToMeetFemaleID
        Dim NumberOfRecordsToReturn As Integer = prms.NumberOfRecordsToReturn
        'Dim AdditionalWhereClause As String = prms.AdditionalWhereClause


        If (Distance = 0) Then
            Distance = DISTANCE_DEFAULT
        End If

        Dim sql As String = ""
        Dim dt As New DataSet()

        'This script is based on kilometer. If you use miles simply convert the radius to kilometer (radius=radius*1.609344)
        Try

            sql = <sql><![CDATA[
--fn:GetMembersToSearchDataTable_MembersNear
SET NOCOUNT ON;

declare @Country varchar(10)
declare @LookingFor_ToMeetMaleID bit
declare @LookingFor_ToMeetFemaleID bit

select 
    @Country=Country, 
    @LookingFor_ToMeetMaleID=LookingFor_ToMeetMaleID, 
    @LookingFor_ToMeetFemaleID=LookingFor_ToMeetFemaleID 
from Eus_Profiles 
where ProfileID=@CurrentProfileId


declare @rows int=@NumberOfRecordsToReturn-1
declare @whileCount int=1

while(@rows<@NumberOfRecordsToReturn) and @whileCount<=6
begin

	if(@whileCount>1 and @whileCount<6 and @rows<@NumberOfRecordsToReturn)
	begin
		set @Distance = @Distance * @whileCount
	end
	else if(@whileCount=6 and @rows<@NumberOfRecordsToReturn)
	begin
		set @Distance=1000
	end
	--select newdistance = @Distance
	

	select @rows=count(*)
	from (
		select 
			top(@NumberOfRecordsToReturn)*
		from (
			SELECT    
				distance    = cast(isNull(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)), 1000) as int)
				,minutes1   =	datediff(minute,'1990-1-1',EUS_Profiles.DateTimeToRegister),
				[HasPhoto] = case 
						when [EUS_Profiles].PhotosPublic > 0  then 1
						when [EUS_Profiles].PhotosPrivate > 0  then 1
						when not exists(select * 
							from EUS_CustomerPhotos phot 
							with (nolock) 
							where phot.CustomerID = EUS_Profiles.ProfileID AND phot.CustomerPhotosID = EUS_Profiles.DefPhotoID
						)  then 0
						else 1 
					end
				,CountryQualifier = case 
										when Country=@Country then 1
										else 0
									end
				,Country
				,PointsVerification
				,Birthday
				,PointsCredits
				,PointsUnlocks
			FROM		
				dbo.EUS_Profiles AS EUS_Profiles 
				with (nolock)
			WHERE
				EUS_Profiles.IsMaster = 1
			AND EUS_Profiles.Profileid <> 1
			AND EUS_Profiles.ProfileID <> @CurrentProfileId
			AND EUS_Profiles.Status = @ReturnRecordsWithStatus
			AND ISNULL(EUS_Profiles.PrivacySettings_HideMeFromSearchResults,0) = 0
			AND (
					(@LookingFor_ToMeetMaleID= 1    AND EUS_Profiles.GenderId = 1)
				OR
					(@LookingFor_ToMeetFemaleID = 1 AND EUS_Profiles.GenderId = 2)
			)
			AND  not exists(select * 
							from EUS_ProfilesBlocked bl 
							with (nolock) 
							where (bl.FromProfileID =EUS_Profiles.ProfileID and bl.ToProfileID = @CurrentProfileId) or
								(bl.FromProfileID =@CurrentProfileId and bl.ToProfileID = EUS_Profiles.ProfileID)
			)
			AND (  
				EUS_Profiles.Country = @Country OR 
				(
					EUS_Profiles.Country <> @Country AND
					not exists(select * 
								from EUS_ProfilesPrivacySettings bl 
								with (nolock) 
								where (bl.ProfileID =EUS_Profiles.ProfileID) and (isnull(HideMeFromUsersInDifferentCountry,0)=1)
					)
				)
			)
            AND not exists(select * from EUS_ProfilesViewed where FromProfileID=@CurrentProfileId and ToProfileID=EUS_Profiles.ProfileID)
		) as EUS_Profiles
		where 
		  ((distance <= @Distance and @Distance <@DISTANCE_DEFAULT)  OR ((distance <= @DISTANCE_DEFAULT) and @Distance =@DISTANCE_DEFAULT))
		and (
			(
				@whileCount<4	
				and CountryQualifier=1 
				and	HasPhoto=1		
			)
			or(
				@whileCount>=4
			)
		)
	) as tCount
	
	if(@rows < @NumberOfRecordsToReturn)
	begin
		select @whileCount = @whileCount+1
		--select [Rows]=@rows, [NextTry]=@whileCount
	end
	else if(@rows = @NumberOfRecordsToReturn)
	begin
		select 
			tOutter.*,
            EUS_Profiles.PointsBeauty,    
            EUS_Profiles.IsMaster, 
            EUS_Profiles.MirrorProfileID, 
            EUS_Profiles.Status,
            EUS_Profiles.LoginName, 
            EUS_Profiles.FirstName,
            EUS_Profiles.LastName, 
            EUS_Profiles.GenderId, 
            EUS_Profiles.Region, 
            EUS_Profiles.City, 
            EUS_Profiles.Zip, 
            EUS_Profiles.CityArea, 
            EUS_Profiles.Address, 
            EUS_Profiles.Telephone, 
            EUS_Profiles.eMail, 
            EUS_Profiles.Cellular, 
            EUS_Profiles.AreYouWillingToTravel, 
            EUS_Profiles.AboutMe_Heading, 
            EUS_Profiles.AboutMe_DescribeYourself, 
            EUS_Profiles.AboutMe_DescribeAnIdealFirstDate, 
            EUS_Profiles.OtherDetails_EducationID, 
            EUS_Profiles.OtherDetails_AnnualIncomeID, 
            EUS_Profiles.OtherDetails_NetWorthID, 
            EUS_Profiles.OtherDetails_Occupation, 
            EUS_Profiles.PersonalInfo_HeightID, 
            EUS_Profiles.PersonalInfo_BodyTypeID, 
            EUS_Profiles.PersonalInfo_EyeColorID, 
            EUS_Profiles.PersonalInfo_HairColorID, 
            EUS_Profiles.PersonalInfo_ChildrenID, 
            EUS_Profiles.PersonalInfo_EthnicityID,
            EUS_Profiles.PersonalInfo_ReligionID, 
            EUS_Profiles.PersonalInfo_SmokingHabitID, 
            EUS_Profiles.PersonalInfo_DrinkingHabitID, 
            EUS_Profiles.LookingFor_ToMeetMaleID, 
            EUS_Profiles.LookingFor_ToMeetFemaleID, EUS_Profiles.LookingFor_RelationshipStatusID, 
            EUS_Profiles.LookingFor_TypeOfDating_ShortTermRelationship, 
            EUS_Profiles.LookingFor_TypeOfDating_Friendship,
            EUS_Profiles.LookingFor_TypeOfDating_LongTermRelationship, 
            EUS_Profiles.LookingFor_TypeOfDating_MutuallyBeneficialArrangements, 
            EUS_Profiles.LookingFor_TypeOfDating_MarriedDating, 
            EUS_Profiles.LookingFor_TypeOfDating_AdultDating_Casual, 
            EUS_Profiles.DateTimeToRegister, 
            EUS_Profiles.RegisterGEOInfos, 
            EUS_Profiles.LastLoginDateTime, 
            EUS_Profiles.LastLoginGEOInfos, 
            EUS_Profiles.LastUpdateProfileDateTime, 
            EUS_Profiles.LastUpdateProfileGEOInfo, 
            EUS_Profiles.LAGID, 
            EUS_Profiles.CustomReferrer, 
            EUS_Profiles.Role, 
            EUS_Profiles.IsOnline, 
            EUS_Profiles.LastActivityDateTime, 
            EUS_Profiles.AvailableCredits, 
            EUS_Profiles.DefPhotoID, 
            EUS_Profiles.CelebratingBirth, 
            phot.CustomerPhotosID, 
            phot.CustomerID, 
            phot.DateTimeToUploading, 
            phot.FileName, 
            phot.DisplayLevel, 
            phot.HasAproved, 
            phot.HasDeclined, 
            phot.IsDefault 
			,CASE 
				WHEN ISNULL(tOutter.Birthday, '') = '' then null
				ELSE dbo.fn_GetAge(tOutter.Birthday, GETDATE())
			End as Age
			,IsOnlineNow=CAST((CASE
					WHEN exists(select ProfileID 
						from [EUS_ProfilesPrivacySettings]
						where (ProfileID=tOutter.ProfileID OR [MirrorProfileID]=tOutter.ProfileID)
						and [PrivacySettings_ShowMeOffline]=1) THEN 0
					WHEN not @LastActivityUTCDate is null and 
							(EUS_Profiles.IsOnline=1 and 
							EUS_Profiles.LastActivityDateTime>=@LastActivityUTCDate) THEN 1
					ELSE 0
				END) as bit)
		from (
			select 
				top(@NumberOfRecordsToReturn)*
			from (
				SELECT
					RowNumber = Row_Number() over(order by newid())
					,distance=cast(isNull(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)), 1000) as int)
					,minutes1=datediff(minute,'1990-1-1',EUS_Profiles.DateTimeToRegister),
					[HasPhoto]=case 
							when [EUS_Profiles].PhotosPublic > 0  then 1
							when [EUS_Profiles].PhotosPrivate > 0  then 1
							when not exists(select * 
								from EUS_CustomerPhotos phot 
								with (nolock) 
								where phot.CustomerID = EUS_Profiles.ProfileID AND phot.CustomerPhotosID = EUS_Profiles.DefPhotoID
							)  then 0
							else 1 
						end
					,CountryQualifier = case 
											when Country=@Country then 1
											else 0
										end
					,ProfileID
					,Country
					,PointsVerification
					,Birthday
					,PointsCredits
					,PointsUnlocks
				FROM		
					dbo.EUS_Profiles AS EUS_Profiles 
					with (nolock)
				WHERE
					EUS_Profiles.IsMaster = 1
				AND EUS_Profiles.Profileid <> 1
				AND EUS_Profiles.ProfileID <> @CurrentProfileId
				AND EUS_Profiles.Status = @ReturnRecordsWithStatus
				AND ISNULL(EUS_Profiles.PrivacySettings_HideMeFromSearchResults,0) = 0
				AND (
						(@LookingFor_ToMeetMaleID= 1    AND EUS_Profiles.GenderId = 1)
					OR
						(@LookingFor_ToMeetFemaleID = 1 AND EUS_Profiles.GenderId = 2)
				)
				AND  not exists(select * 
								from EUS_ProfilesBlocked bl 
								with (nolock) 
								where (bl.FromProfileID =EUS_Profiles.ProfileID and bl.ToProfileID = @CurrentProfileId) or
									(bl.FromProfileID =@CurrentProfileId and bl.ToProfileID = EUS_Profiles.ProfileID)
				)
				AND (  
					EUS_Profiles.Country = @Country OR 
					(
						EUS_Profiles.Country <> @Country AND
						not exists(select * 
									from EUS_ProfilesPrivacySettings bl 
									with (nolock) 
									where (bl.ProfileID =EUS_Profiles.ProfileID) and (isnull(HideMeFromUsersInDifferentCountry,0)=1)
						)
					)
				)
                AND not exists(select * from EUS_ProfilesViewed where FromProfileID=@CurrentProfileId and ToProfileID=EUS_Profiles.ProfileID)
			) as EUS_Profiles
			where 
				((distance <= @Distance and @Distance <@DISTANCE_DEFAULT)  OR ((distance <= @DISTANCE_DEFAULT) and @Distance =@DISTANCE_DEFAULT))
			and (
				(
					@whileCount<4	
					and CountryQualifier=1 
					and	HasPhoto=1		
				)
				or(
					@whileCount>=4
				)
			)
		) as tOutter
		INNER JOIN dbo.EUS_Profiles AS EUS_Profiles ON tOutter.ProfileID = EUS_Profiles.ProfileID
		LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.CustomerPhotosID = EUS_Profiles.DefPhotoID
		order by tOutter.RowNumber
	end
end
]]></sql>.Value


            Dim command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql)
            command.Parameters.Add(New SqlClient.SqlParameter("@CurrentProfileId", CurrentProfileId))
            command.Parameters.Add(New SqlClient.SqlParameter("@ReturnRecordsWithStatus", ReturnRecordsWithStatus))
            command.Parameters.Add(New SqlClient.SqlParameter("@NumberOfRecordsToReturn", NumberOfRecordsToReturn))
            command.Parameters.Add(New SqlClient.SqlParameter("@Distance", Distance))
            command.Parameters.Add(New SqlClient.SqlParameter("@DISTANCE_DEFAULT", DISTANCE_DEFAULT))
            command.Parameters.Add(New SqlClient.SqlParameter("@performCount", prms.performCount))
            command.Parameters.Add(New SqlClient.SqlParameter("@RowNumberMin", prms.rowNumberMin))
            command.Parameters.Add(New SqlClient.SqlParameter("@RowNumberMax", prms.rowNumberMax))

            If (String.IsNullOrEmpty(prms.zipstr)) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@zip", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@zip", prms.zipstr))
            End If

            If (prms.latitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", prms.latitudeIn))
            End If

            If (prms.longitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", prms.longitudeIn))
            End If

            Try
                Dim mins As Integer = clsConfigValues.Get__members_online_minutes()
                Dim LastActivityUTCDate As DateTime = Date.UtcNow.AddMinutes(-mins)
                command.Parameters.Add(New SqlClient.SqlParameter("@LastActivityUTCDate", LastActivityUTCDate))
            Catch
                command.Parameters.Add(New SqlClient.SqlParameter("@LastActivityUTCDate", System.DBNull.Value))
            End Try

            dt = DataHelpers.GetDataSet(command)

        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        Finally
            clsLogger.InsertLog("GetMembersToSearchDataTable", prms.CurrentProfileId, (DateTime.UtcNow - __logdate).TotalMilliseconds)
        End Try


        Return dt
    End Function

    Public Shared Function GetMembersToSearchDataTable_Public(prms As clsSearchHelperParameters) As DataSet

        'clsLogger.InsertLog("GetMembersToSearchDataTable_Public")
        Dim __logdate As DateTime = DateTime.UtcNow


        Dim ReturnRecordsWithStatus As Integer = prms.ReturnRecordsWithStatus
        Dim SearchSort As SearchSortEnum = prms.SearchSort
        Dim Distance As Integer = prms.Distance
        Dim LookingFor_ToMeetMaleID As Boolean = prms.LookingFor_ToMeetMaleID
        Dim LookingFor_ToMeetFemaleID As Boolean = prms.LookingFor_ToMeetFemaleID
        Dim NumberOfRecordsToReturn As Integer = prms.NumberOfRecordsToReturn


        If (Distance = 0) Then
            Distance = DISTANCE_DEFAULT
        End If

        Dim sql As String = ""
        Dim dt As New DataSet()

        'This script is based on kilometer. If you use miles simply convert the radius to kilometer (radius=radius*1.609344)
        Try


            sql = <sql><![CDATA[

--fn:GetMembersToSearchDataTable_Public

	SET NOCOUNT ON;

if(@performCount=1)
begin

    select Count(*) 
    from (
	    SELECT  
            ProfileID,
		    distance =isNull(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)), 1000)
	    FROM		
            dbo.EUS_Profiles AS EUS_Profiles 
		    with (nolock)
        WHERE
		    EUS_Profiles.IsMaster = 1
	    AND EUS_Profiles.Profileid <> 1
	    AND (
			    (EUS_Profiles.GenderId = 1  AND  @LookingFor_ToMeetMaleID=1)
		    OR
			    (EUS_Profiles.GenderId = 2  AND  @LookingFor_ToMeetFemaleID=1)
	    )
	    AND EUS_Profiles.Status = @ReturnRecordsWithStatus
	    AND ISNULL(EUS_Profiles.PrivacySettings_HideMeFromSearchResults,0) = 0
	    AND (  
            EUS_Profiles.Country = @Country OR 
            (
                EUS_Profiles.Country <> @Country AND
                not exists(select * 
					        from EUS_ProfilesPrivacySettings bl 
					        where (bl.ProfileID =EUS_Profiles.ProfileID) and (isnull(HideMeFromUsersInDifferentCountry,0)=1)
                )
            )
        )
        [AdditionalWhereClause]
    ) as EUS_Profiles
    where 
        ((distance <= @Distance and @Distance <@DISTANCE_DEFAULT)  OR ((distance <= @DISTANCE_DEFAULT) and @Distance =@DISTANCE_DEFAULT))
end



if(@NumberOfRecordsToReturn>0)
	SET ROWCOUNT @NumberOfRecordsToReturn;


select 
	tOutter.*
    ,CASE 
		WHEN ISNULL(tOutter.Birthday, '') = '' then null
		ELSE dbo.fn_GetAge(tOutter.Birthday, GETDATE())
	End as Age
    ,IsOnlineNow=CAST((CASE
            WHEN exists(select ProfileID 
                from [EUS_ProfilesPrivacySettings]
                where (ProfileID=tOutter.ProfileID OR [MirrorProfileID]=tOutter.ProfileID)
                and [PrivacySettings_ShowMeOffline]=1) THEN 0
            WHEN not @LastActivityUTCDate is null and (tOutter.IsOnline=1 and tOutter.LastActivityDateTime>=@LastActivityUTCDate) THEN 1
            ELSE 0
        END) as bit)

from (
    select 
	    RowNumber = Row_Number() over([QueryOrdering])
	    ,tInner.*
    from(
        select 
            *
        from (
	        SELECT    
		        distance =isNull(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)), 1000)
	            ,CountryQualifier = case 
						            when Country=@Country then 1
						            else 0
					            end
		        ,EUS_Profiles.PointsBeauty,    
		        EUS_Profiles.PointsVerification,     
		        EUS_Profiles.PointsCredits,     
                EUS_Profiles.PointsUnlocks,
                EUS_Profiles.ProfileID, EUS_Profiles.IsMaster, EUS_Profiles.MirrorProfileID, EUS_Profiles.Status, EUS_Profiles.LoginName, 
                EUS_Profiles.FirstName, EUS_Profiles.LastName, EUS_Profiles.GenderId, EUS_Profiles.Country, EUS_Profiles.Region, 
                EUS_Profiles.City, EUS_Profiles.Zip, EUS_Profiles.CityArea, EUS_Profiles.Address, EUS_Profiles.Telephone, EUS_Profiles.eMail, 
                EUS_Profiles.Cellular, EUS_Profiles.AreYouWillingToTravel, EUS_Profiles.AboutMe_Heading, 
                EUS_Profiles.AboutMe_DescribeYourself, EUS_Profiles.AboutMe_DescribeAnIdealFirstDate, 
                EUS_Profiles.OtherDetails_EducationID, EUS_Profiles.OtherDetails_AnnualIncomeID, 
                EUS_Profiles.OtherDetails_NetWorthID, EUS_Profiles.OtherDetails_Occupation, EUS_Profiles.PersonalInfo_HeightID, 
                EUS_Profiles.PersonalInfo_BodyTypeID, EUS_Profiles.PersonalInfo_EyeColorID, 
                EUS_Profiles.PersonalInfo_HairColorID, EUS_Profiles.PersonalInfo_ChildrenID, EUS_Profiles.PersonalInfo_EthnicityID,
                EUS_Profiles.PersonalInfo_ReligionID, EUS_Profiles.PersonalInfo_SmokingHabitID, 
                EUS_Profiles.PersonalInfo_DrinkingHabitID, EUS_Profiles.LookingFor_ToMeetMaleID, 
                EUS_Profiles.LookingFor_ToMeetFemaleID, EUS_Profiles.LookingFor_RelationshipStatusID, 
                EUS_Profiles.LookingFor_TypeOfDating_ShortTermRelationship, EUS_Profiles.LookingFor_TypeOfDating_Friendship,
                EUS_Profiles.LookingFor_TypeOfDating_LongTermRelationship, 
                EUS_Profiles.LookingFor_TypeOfDating_MutuallyBeneficialArrangements, EUS_Profiles.LookingFor_TypeOfDating_MarriedDating, 
                EUS_Profiles.LookingFor_TypeOfDating_AdultDating_Casual, 
                EUS_Profiles.DateTimeToRegister, 
                EUS_Profiles.RegisterIP, 
                EUS_Profiles.LastLoginDateTime, 
                EUS_Profiles.LastUpdateProfileDateTime, 
                EUS_Profiles.LAGID, 
                EUS_Profiles.Birthday, 
                EUS_Profiles.IsOnline, 
                EUS_Profiles.LastActivityDateTime, 
                EUS_Profiles.DefPhotoID, 
                IsReferrer = case 
                    when ISNULL(EUS_Profiles.ReferrerParentId,0)>0 then 1
                    else 0
                end, 
                phot.CustomerPhotosID, 
                phot.CustomerID, 
                phot.DateTimeToUploading, 
                phot.FileName, 
                phot.DisplayLevel, 
                phot.HasAproved, phot.HasDeclined, 
                phot.CheckedContextID, 
                phot.IsDefault, 
                phot.ShowOnFrontPage, 
                phot.ShowOnGrid, 
                HasFavoritedMe = 0,
                DidIFavorited =0,
                CommunicationUnl = 0,
                VerID = ver.ID,
                VerPhoto = ver.Photo,
                VerTel = ver.Tel,
                VerComplete = ver.Complete,
                VerFacebook = ver.Facebook
	        FROM		
                dbo.EUS_Profiles AS EUS_Profiles 
		        with (nolock)
	        LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.CustomerPhotosID = EUS_Profiles.DefPhotoID
	        LEFT OUTER JOIN dbo.EUS_ProfilesVerification AS ver ON ver.ProfileID = EUS_Profiles.ProfileID
            WHERE
		        EUS_Profiles.IsMaster = 1
	        AND EUS_Profiles.Profileid <> 1
	        AND (
			        (EUS_Profiles.GenderId = 1  AND  @LookingFor_ToMeetMaleID=1)
		        OR
			        (EUS_Profiles.GenderId = 2  AND  @LookingFor_ToMeetFemaleID=1)
	        )
	        AND EUS_Profiles.Status = @ReturnRecordsWithStatus
	        AND ISNULL(EUS_Profiles.PrivacySettings_HideMeFromSearchResults,0) = 0
	        AND (  
                EUS_Profiles.Country = @Country OR 
                (
                    EUS_Profiles.Country <> @Country AND
                    not exists(select * 
					            from EUS_ProfilesPrivacySettings bl 
					            where (bl.ProfileID =EUS_Profiles.ProfileID) and (isnull(HideMeFromUsersInDifferentCountry,0)=1)
                    )
                )
            )
            [AdditionalWhereClause]
        ) as EUS_Profiles
        where 
          ((distance <= @Distance and @Distance <@DISTANCE_DEFAULT)  OR ((distance <= @DISTANCE_DEFAULT) and @Distance =@DISTANCE_DEFAULT))
    ) as tInner
) as tOutter
where 
	tOutter.RowNumber > @RowNumberMin and
	tOutter.RowNumber <= @RowNumberMax 
order by tOutter.RowNumber
]]></sql>.Value

            If (Not String.IsNullOrEmpty(prms.AdditionalWhereClause)) Then
                sql = sql.Replace("[AdditionalWhereClause]", vbCrLf & prms.AdditionalWhereClause)
                prms.performCount = False
                NumberOfRecordsToReturn = 0
            Else
                sql = sql.Replace("[AdditionalWhereClause]", "")
            End If


            Dim sqlOrderBy As String = ""
            If (LookingFor_ToMeetFemaleID) Then
                If (SearchSort = SearchSortEnum.NewestMember) Then
                    sqlOrderBy = vbCrLf & _
                        "Order By isdefault desc, CountryQualifier desc, DateTimeToRegister Desc, ShowOnGrid desc, ShowOnFrontPage desc, PointsBeauty desc, Birthday asc, IsReferrer desc, VerComplete desc, VerID desc, VerPhoto desc, VerTel desc, VerFacebook desc, distance asc, PointsVerification desc, PointsUnlocks desc"
                ElseIf (SearchSort = SearchSortEnum.OldestMember) Then
                    sqlOrderBy = vbCrLf & _
                        "Order By isdefault desc, CountryQualifier desc, DateTimeToRegister Asc, ShowOnGrid desc, ShowOnFrontPage desc, PointsBeauty desc, Birthday asc, IsReferrer desc, VerComplete desc, VerID desc, VerPhoto desc, VerTel desc, VerFacebook desc, distance asc, PointsVerification desc, PointsUnlocks desc"
                ElseIf (SearchSort = SearchSortEnum.RecentlyLoggedIn) Then
                    sqlOrderBy = vbCrLf & _
                        "Order By isdefault desc, CountryQualifier desc, LastLoginDateTime Desc, ShowOnGrid desc, ShowOnFrontPage desc, PointsBeauty desc, Birthday asc, IsReferrer desc, VerComplete desc, VerID desc, VerPhoto desc, VerTel desc, VerFacebook desc, distance asc, PointsVerification desc, PointsUnlocks desc"
                Else
                    'SearchSortEnum.NearestDistance
                    'SearchSortEnum.None
                    sqlOrderBy = vbCrLf & _
                        "order by isdefault desc, CountryQualifier desc, ShowOnGrid desc, ShowOnFrontPage desc, distance asc, PointsBeauty desc, Birthday asc, IsReferrer desc, VerComplete desc, VerID desc, VerPhoto desc, VerTel desc, VerFacebook desc, Age asc, PointsVerification desc, PointsUnlocks desc"
                End If
            Else
                If (SearchSort = SearchSortEnum.NewestMember) Then
                    sqlOrderBy = vbCrLf & _
                        "Order By isdefault desc, CountryQualifier desc, DateTimeToRegister Desc, ShowOnGrid desc, ShowOnFrontPage desc, PointsBeauty desc, Birthday asc, VerComplete desc, VerID desc, VerPhoto desc, VerTel desc, VerFacebook desc, distance asc, PointsVerification desc, PointsCredits desc, PointsUnlocks desc"
                ElseIf (SearchSort = SearchSortEnum.OldestMember) Then
                    sqlOrderBy = vbCrLf & _
                        "Order By isdefault desc, CountryQualifier desc, DateTimeToRegister Asc, ShowOnGrid desc, ShowOnFrontPage desc, PointsBeauty desc, Birthday asc, VerComplete desc, VerID desc, VerPhoto desc, VerTel desc, VerFacebook desc, distance asc, PointsVerification desc, PointsCredits desc, PointsUnlocks desc"
                ElseIf (SearchSort = SearchSortEnum.RecentlyLoggedIn) Then
                    sqlOrderBy = vbCrLf & _
                        "Order By isdefault desc,CountryQualifier desc, LastLoginDateTime Desc,  ShowOnGrid desc, ShowOnFrontPage desc, PointsBeauty desc, Birthday asc, VerComplete desc, VerID desc, VerPhoto desc, VerTel desc, VerFacebook desc, distance asc, PointsVerification desc, PointsCredits desc, PointsUnlocks desc"
                Else
                    'SearchSortEnum.NearestDistance
                    'SearchSortEnum.None
                    sqlOrderBy = vbCrLf & _
                        "order by isdefault desc, CountryQualifier desc, ShowOnGrid desc, ShowOnFrontPage desc, distance asc, PointsBeauty desc, Birthday asc, PointsVerification desc,  PointsCredits desc, PointsUnlocks desc"
                End If
            End If

            sql = sql.Replace("[QueryOrdering]", sqlOrderBy)



            Dim command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql)
            command.Parameters.Add(New SqlClient.SqlParameter("@ReturnRecordsWithStatus", ReturnRecordsWithStatus))
            command.Parameters.Add(New SqlClient.SqlParameter("@NumberOfRecordsToReturn", NumberOfRecordsToReturn))
            command.Parameters.Add(New SqlClient.SqlParameter("@Distance", Distance))
            command.Parameters.Add(New SqlClient.SqlParameter("@DISTANCE_DEFAULT", DISTANCE_DEFAULT))
            command.Parameters.Add(New SqlClient.SqlParameter("@performCount", prms.performCount))
            command.Parameters.Add(New SqlClient.SqlParameter("@RowNumberMin", prms.rowNumberMin))
            command.Parameters.Add(New SqlClient.SqlParameter("@RowNumberMax", prms.rowNumberMax))

            If (String.IsNullOrEmpty(prms.zipstr)) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@zip", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@zip", prms.zipstr))
            End If

            If (prms.latitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", prms.latitudeIn))
            End If

            If (prms.longitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", prms.longitudeIn))
            End If

            If (Not prms.LookingFor_ToMeetFemaleID AndAlso Not prms.LookingFor_ToMeetMaleID) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@LookingFor_ToMeetFemaleID", True))
                command.Parameters.Add(New SqlClient.SqlParameter("@LookingFor_ToMeetMaleID", False))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@LookingFor_ToMeetFemaleID", prms.LookingFor_ToMeetFemaleID))
                command.Parameters.Add(New SqlClient.SqlParameter("@LookingFor_ToMeetMaleID", prms.LookingFor_ToMeetMaleID))
            End If

            If (prms.Country Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@Country", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@Country", prms.Country))
            End If

            Try
                Dim mins As Integer = clsConfigValues.Get__members_online_minutes()
                Dim LastActivityUTCDate As DateTime = Date.UtcNow.AddMinutes(-mins)
                command.Parameters.Add(New SqlClient.SqlParameter("@LastActivityUTCDate", LastActivityUTCDate))
            Catch
                command.Parameters.Add(New SqlClient.SqlParameter("@LastActivityUTCDate", System.DBNull.Value))
            End Try

            dt = DataHelpers.GetDataSet(command)

        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        Finally
            clsLogger.InsertLog("GetMembersToSearchDataTable_Public", Nothing, (DateTime.UtcNow - __logdate).TotalMilliseconds)
        End Try

        Return dt
    End Function




    Public Shared Function GetMembersToSearchUsingGenderDataTable(prms As clsSearchHelperParameters) As DataSet

        Dim __logdate As DateTime = DateTime.UtcNow

        Dim CurrentProfileId As Integer = prms.CurrentProfileId
        Dim ReturnRecordsWithStatus As Integer = prms.ReturnRecordsWithStatus
        Dim SearchSort As SearchSortEnum = prms.SearchSort
        'Dim zipstr As String = prms.zipstr
        Dim Distance As Integer = prms.Distance
        Dim LookingFor_ToMeetMaleID As Boolean = prms.LookingFor_ToMeetMaleID
        Dim LookingFor_ToMeetFemaleID As Boolean = prms.LookingFor_ToMeetFemaleID
        Dim NumberOfRecordsToReturn As Integer = prms.NumberOfRecordsToReturn



        Dim sql As String = ""
        Dim dt As New DataSet()

        Try

            sql = <sql><![CDATA[
    --fn:GetMembersToSearchUsingGenderDataTable
        --@ReturnRecordsWithStatus int
        --,@Gender int
        --,@NumberOfRecordsToReturn int =0


if(@performCount=1)
begin

    select Count(*) from (
	    SELECT    
		    distance =isNull(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)), 1000)
        FROM		
        	dbo.EUS_Profiles AS EUS_Profiles 
        	with (nolock)
    	LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.CustomerPhotosID = EUS_Profiles.DefPhotoID
        WHERE
        	EUS_Profiles.IsMaster = 1
        AND EUS_Profiles.Profileid <> 1
        AND EUS_Profiles.Status = @ReturnRecordsWithStatus
        AND EUS_Profiles.GenderId=@Gender 
        AND (phot.ShowOnFrontPage=@ShowOnFrontPage OR @ShowOnFrontPage is null)
        AND (EUS_Profiles.PrivacySettings_HideMeFromSearchResults=0 or EUS_Profiles.PrivacySettings_HideMeFromSearchResults is null)
    ) as t
    where 
      ((distance <= @Distance and @Distance <@DISTANCE_DEFAULT)  OR ((distance <= @DISTANCE_DEFAULT) and @Distance =@DISTANCE_DEFAULT))
end




        SET NOCOUNT ON;
        if(@NumberOfRecordsToReturn>0)
        	SET ROWCOUNT @NumberOfRecordsToReturn;


select 
	tOutter.*
    ,CASE 
        WHEN ISNULL(tOutter.Birthday, '') = '' then null
        ELSE dbo.fn_GetAge(tOutter.Birthday, GETDATE())
    End as Age
from
(
    select 
	    RowNumber = Row_Number() over(ORDER BY  IsDefault desc, NEWID(),LastActivityDateTime Desc, PointsBeauty desc, DateTimeToRegister Desc)
		,tRowNumber.* 
    from (
        SELECT    
        	distance =isNull(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)), 1000),
            EUS_Profiles.ProfileID, 
            EUS_Profiles.IsMaster, 
            EUS_Profiles.MirrorProfileID, 
            EUS_Profiles.Status, 
            EUS_Profiles.LoginName, 
            City,
            Region,
            Genderid,
            AboutMe_Heading,
            PersonalInfo_HeightID,
            PersonalInfo_BodyTypeID,
            PersonalInfo_HairColorID,
            PersonalInfo_EyeColorID,
            PersonalInfo_EthnicityID,
            LastActivityDateTime,
            DateTimeToRegister,
            Birthday,
        	EUS_Profiles.PointsBeauty,    
        	EUS_Profiles.PointsVerification,     
        	EUS_Profiles.PointsCredits,     
            EUS_Profiles.PointsUnlocks,
            phot.CustomerPhotosID, 
            phot.CustomerID, 
            phot.DateTimeToUploading, phot.FileName, 
            phot.DisplayLevel, 
            phot.HasAproved, phot.HasDeclined, 
            phot.CheckedContextID, 
            phot.IsDefault
        FROM		
        	dbo.EUS_Profiles AS EUS_Profiles 
        	with (nolock)
    	LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.CustomerPhotosID = EUS_Profiles.DefPhotoID
        WHERE
        	EUS_Profiles.IsMaster = 1
        AND EUS_Profiles.Profileid <> 1
        AND EUS_Profiles.Status = @ReturnRecordsWithStatus
        AND EUS_Profiles.GenderId=@Gender 
        AND (phot.ShowOnFrontPage=@ShowOnFrontPage OR @ShowOnFrontPage is null)
        AND (EUS_Profiles.PrivacySettings_HideMeFromSearchResults=0 or EUS_Profiles.PrivacySettings_HideMeFromSearchResults is null)
    ) as tRowNumber
    where 
        ((distance <= @Distance and @Distance <@DISTANCE_DEFAULT)  OR ((distance <= @DISTANCE_DEFAULT) and @Distance =@DISTANCE_DEFAULT))
) as tOutter
where 
	tOutter.RowNumber > @RowNumberMin and
	tOutter.RowNumber <= @RowNumberMax 
order by tOutter.RowNumber
    ]]></sql>.Value

            Dim command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql)
            command.Parameters.Add(New SqlClient.SqlParameter("@ReturnRecordsWithStatus", ReturnRecordsWithStatus))
            command.Parameters.Add(New SqlClient.SqlParameter("@NumberOfRecordsToReturn", NumberOfRecordsToReturn))
            command.Parameters.Add(New SqlClient.SqlParameter("@Gender", prms.GenderId))
            command.Parameters.Add(New SqlClient.SqlParameter("@Distance", Distance))
            command.Parameters.Add(New SqlClient.SqlParameter("@DISTANCE_DEFAULT", DISTANCE_DEFAULT))
            'command.Parameters.Add(New SqlClient.SqlParameter("@zip", prms.zipstr))
            command.Parameters.Add(New SqlClient.SqlParameter("@performCount", prms.performCount))
            command.Parameters.Add(New SqlClient.SqlParameter("@RowNumberMin", prms.rowNumberMin))
            command.Parameters.Add(New SqlClient.SqlParameter("@RowNumberMax", prms.rowNumberMax))

            If (String.IsNullOrEmpty(prms.zipstr)) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@zip", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@zip", prms.zipstr))
            End If
            If (prms.ShowOnFrontPageRequired Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@ShowOnFrontPage", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@ShowOnFrontPage", prms.ShowOnFrontPageRequired))
            End If
            If (prms.latitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", prms.latitudeIn))
            End If
            If (prms.longitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", prms.longitudeIn))
            End If

            dt = DataHelpers.GetDataSet(command)
        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        Finally
            clsLogger.InsertLog("GetMembersToSearchUsingGenderDataTable", 0, (DateTime.UtcNow - __logdate).TotalMilliseconds)
        End Try


        Return dt
    End Function





    Public Shared Function GetWhoViewedMeMembersDataTable(parms As clsMyListsHelperParameters) As DataSet

        Dim __logdate As DateTime = DateTime.UtcNow

        Dim CurrentProfileId As Integer = parms.CurrentProfileId
        Dim ReturnRecordsWithStatus As Integer = parms.ReturnRecordsWithStatus
        Dim sorting As MyListsSortEnum = parms.sorting
        Dim zipstr As String = parms.zipstr
        Dim latitudeIn As Double? = parms.latitudeIn
        Dim longitudeIn As Double? = parms.longitudeIn
        Dim Distance As Integer = parms.Distance
        Dim NumberOfRecordsToReturn As Integer = parms.NumberOfRecordsToReturn
        Dim performCount As Boolean = parms.performCount
        Dim isOnline As Boolean = parms.isOnline


        If (Distance = 0) Then
            Distance = DISTANCE_DEFAULT
        End If

        Dim sql As String = ""
        Dim dt As New DataSet()

        'This script is based on kilometer. If you use miles simply convert the radius to kilometer (radius=radius*1.609344)
        Try

            sql = <sql><![CDATA[
--fn:GetWhoViewedMeMembersDataTable
	--@CurrentProfileId int
	--,@ReturnRecordsWithStatus int
	--,@NumberOfRecordsToReturn int =0
	--,@Distance int=0


if(@performCount=1)
begin

    select Count(*) from (
	    SELECT    
		    distance =isNull(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)), 1000)
	    FROM		dbo.EUS_Profiles AS EUS_Profiles 
		    with (nolock)
	    INNER JOIN
			      dbo.EUS_ProfilesViewed AS viewed ON viewed.FromProfileID = EUS_Profiles.ProfileID AND viewed.ToProfileID = @CurrentProfileId    AND isnull(viewed.ToProfileDeleted,0) = 0  
  	    LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.CustomerPhotosID = EUS_Profiles.DefPhotoID
	    WHERE
		    EUS_Profiles.IsMaster = 1
	     AND EUS_Profiles.[ProfileID] <> 1 
	    AND EUS_Profiles.Status = @ReturnRecordsWithStatus

	    /* blocked filter */
	    AND  not exists(select * 
						    from EUS_ProfilesBlocked bl 
		                    with (nolock)
						    where (bl.FromProfileID =EUS_Profiles.ProfileID and bl.ToProfileID = @CurrentProfileId) or
							    (bl.FromProfileID =@CurrentProfileId and bl.ToProfileID = EUS_Profiles.ProfileID)
					    )

	    /* hide me from other users' who viewed me list */
	    AND (		(EUS_Profiles.PrivacySettings_NotShowInOtherUsersViewedList = 1 and
					    not exists(select * 
						    from EUS_ProfilesViewed bl 
		                    with (nolock)
						    where bl.FromProfileID = EUS_Profiles.ProfileID and bl.ToProfileID = @CurrentProfileId
				    )
				    OR 
					    EUS_Profiles.PrivacySettings_NotShowInOtherUsersViewedList = 0
				    OR
					    EUS_Profiles.PrivacySettings_NotShowInOtherUsersViewedList is null
			    )
		    )
        AND (
            @LastActivityDateTime is null or
            (
                not @LastActivityDateTime is null and 
                EUS_Profiles.IsOnline=1 and 
                EUS_Profiles.LastActivityDateTime>=@LastActivityDateTime and
                not exists(select ProfileID 
                        from [EUS_ProfilesPrivacySettings]
                        where (ProfileID=EUS_Profiles.ProfileID OR [MirrorProfileID]=EUS_Profiles.ProfileID)
                        and [PrivacySettings_ShowMeOffline]=1)
            )
        )
    ) as t
    where 
      ((distance <= @Distance and @Distance <@DISTANCE_DEFAULT)  OR ((distance <= @DISTANCE_DEFAULT) and @Distance =@DISTANCE_DEFAULT))
end


	SET NOCOUNT ON;
	if(@NumberOfRecordsToReturn>0)
		SET ROWCOUNT @NumberOfRecordsToReturn;



select 
	tOutter.*

	,OffersCount = 
        ISNULL((select COUNT(*) 
	        from EUS_Offers 
	        where (ToProfileID=tOutter.ProfileID and  FromProfileID=@CurrentProfileId)
	        or (ToProfileID=@CurrentProfileId and FromProfileID=tOutter.ProfileID)
        ),0)
    ,HasFavoritedMe =
        ISNULL((select COUNT(*)
                from 	dbo.EUS_ProfilesFavorite favMe
                where   favMe.ToProfileID = @CurrentProfileId AND favMe.FromProfileID = tOutter.ProfileID 
        ),0)
    ,DidIFavorited = 
        ISNULL((select COUNT(*)
            from 	dbo.EUS_ProfilesFavorite favMe
            where   favMe.ToProfileID = tOutter.ProfileID AND favMe.FromProfileID = @CurrentProfileId
        ),0)
    ,CommunicationUnl =
        ISNULL((select COUNT(*)
                from 	dbo.EUS_UnlockedConversations unl
                where   ( unl.FromProfileID = @CurrentProfileId AND unl.ToProfileID = tOutter.ProfileID) OR
		                ( unl.FromProfileID = tOutter.ProfileID  AND unl.ToProfileID = @CurrentProfileId)
        ),0)
    ,IsOnlineNow=CAST((CASE
                        WHEN exists(select ProfileID 
                            from [EUS_ProfilesPrivacySettings]
                            where (ProfileID=tOutter.ProfileID OR [MirrorProfileID]=tOutter.ProfileID)
                            and [PrivacySettings_ShowMeOffline]=1) THEN 0
                        WHEN not @LastActivityDateTime is null and (tOutter.IsOnline=1 and tOutter.LastActivityDateTime>=@LastActivityDateTime) THEN 1
                        ELSE 0
                    END) as bit)
from
(
    select 
	    RowNumber = Row_Number() over([QueryOrdering])
		,tRowNumber.* 
    from (
	    SELECT    
		    distance =isNull(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)), 1000),
            EUS_Profiles.ProfileID, EUS_Profiles.IsMaster, EUS_Profiles.MirrorProfileID, EUS_Profiles.Status, EUS_Profiles.LoginName, 
            EUS_Profiles.FirstName, EUS_Profiles.LastName, EUS_Profiles.GenderId, EUS_Profiles.Country, EUS_Profiles.Region, 
            EUS_Profiles.City, EUS_Profiles.Zip, EUS_Profiles.CityArea, EUS_Profiles.Address, EUS_Profiles.Telephone, EUS_Profiles.eMail, 
            EUS_Profiles.Cellular, EUS_Profiles.AreYouWillingToTravel, EUS_Profiles.AboutMe_Heading, 
            EUS_Profiles.AboutMe_DescribeYourself, EUS_Profiles.AboutMe_DescribeAnIdealFirstDate, 
            EUS_Profiles.OtherDetails_EducationID, EUS_Profiles.OtherDetails_AnnualIncomeID, 
            EUS_Profiles.OtherDetails_NetWorthID, EUS_Profiles.OtherDetails_Occupation, EUS_Profiles.PersonalInfo_HeightID, 
            EUS_Profiles.PersonalInfo_BodyTypeID, EUS_Profiles.PersonalInfo_EyeColorID, 
            EUS_Profiles.PersonalInfo_HairColorID, EUS_Profiles.PersonalInfo_ChildrenID, EUS_Profiles.PersonalInfo_EthnicityID,
            EUS_Profiles.PersonalInfo_ReligionID, EUS_Profiles.PersonalInfo_SmokingHabitID, 
            EUS_Profiles.PersonalInfo_DrinkingHabitID, EUS_Profiles.LookingFor_ToMeetMaleID, 
            EUS_Profiles.LookingFor_ToMeetFemaleID, EUS_Profiles.LookingFor_RelationshipStatusID, 
            EUS_Profiles.LookingFor_TypeOfDating_ShortTermRelationship, EUS_Profiles.LookingFor_TypeOfDating_Friendship,
            EUS_Profiles.LookingFor_TypeOfDating_LongTermRelationship, 
            EUS_Profiles.LookingFor_TypeOfDating_MutuallyBeneficialArrangements, EUS_Profiles.LookingFor_TypeOfDating_MarriedDating, 
            EUS_Profiles.LookingFor_TypeOfDating_AdultDating_Casual, 
            EUS_Profiles.DateTimeToRegister, 
            EUS_Profiles.RegisterIP, 
            EUS_Profiles.LastLoginDateTime, 
            EUS_Profiles.LastUpdateProfileDateTime, 
            EUS_Profiles.LAGID, 
            EUS_Profiles.Birthday, 
            EUS_Profiles.IsOnline, 
            EUS_Profiles.LastActivityDateTime, 
            EUS_Profiles.CelebratingBirth, 
            phot.CustomerPhotosID, 
            phot.CustomerID, 
            phot.DateTimeToUploading, phot.FileName, 
            phot.DisplayLevel, 
            phot.HasAproved, phot.HasDeclined, 
            phot.CheckedContextID, 
            phot.IsDefault, 
	        [HasPhoto] = case 
			        when [EUS_Profiles].PhotosPublic > 0  then 1
			        when [EUS_Profiles].PhotosPrivate > 0  then 1
			        when phot.[CustomerID] IS NULL  then 0
			        else 1 
			    end,

	        viewed.EUS_ProfilesViewedID as ProfilesViewedID, 
	        viewed.DateTimeToCreate as ProfilesViewedDateTimeToCreate, 
	        viewed.FromProfileID as ProfilesViewedFromProfileID, 
	        viewed.ToProfileID as ProfilesViewedToProfileID

	    FROM		dbo.EUS_Profiles AS EUS_Profiles 
		    with (nolock)
	    INNER JOIN
			      dbo.EUS_ProfilesViewed AS viewed ON viewed.FromProfileID = EUS_Profiles.ProfileID AND viewed.ToProfileID = @CurrentProfileId    AND isnull(viewed.ToProfileDeleted,0) = 0  
  	    LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.CustomerPhotosID = EUS_Profiles.DefPhotoID
	    WHERE
		    EUS_Profiles.IsMaster = 1
	     AND EUS_Profiles.[ProfileID] <> 1 
	    AND EUS_Profiles.Status = @ReturnRecordsWithStatus

	    /* blocked filter */
	    AND  not exists(select * 
						    from EUS_ProfilesBlocked bl 
		                    with (nolock)
						    where (bl.FromProfileID =EUS_Profiles.ProfileID and bl.ToProfileID = @CurrentProfileId) or
							    (bl.FromProfileID =@CurrentProfileId and bl.ToProfileID = EUS_Profiles.ProfileID)
					    )

	    /* hide me from other users' who viewed me list */
	    AND (		(EUS_Profiles.PrivacySettings_NotShowInOtherUsersViewedList = 1 and
					    not exists(select * 
						    from EUS_ProfilesViewed bl 
		                    with (nolock)
						    where bl.FromProfileID = EUS_Profiles.ProfileID and bl.ToProfileID = @CurrentProfileId
				    )
				    OR 
					    EUS_Profiles.PrivacySettings_NotShowInOtherUsersViewedList = 0
				    OR
					    EUS_Profiles.PrivacySettings_NotShowInOtherUsersViewedList is null
			    )
		    )
        AND (
            @LastActivityDateTime is null or
            (
                not @LastActivityDateTime is null and 
                EUS_Profiles.IsOnline=1 and 
                EUS_Profiles.LastActivityDateTime>=@LastActivityDateTime and
                not exists(select ProfileID 
                        from [EUS_ProfilesPrivacySettings]
                        where (ProfileID=EUS_Profiles.ProfileID OR [MirrorProfileID]=EUS_Profiles.ProfileID)
                        and [PrivacySettings_ShowMeOffline]=1)
            )
        )
    ) as tRowNumber
    where 
        ((distance <= @Distance and @Distance <@DISTANCE_DEFAULT)  OR ((distance <= @DISTANCE_DEFAULT) and @Distance =@DISTANCE_DEFAULT))
) as tOutter
where 
	tOutter.RowNumber > @RowNumberMin and
	tOutter.RowNumber <= @RowNumberMax 
order by tOutter.RowNumber
]]></sql>.Value

            Dim sqlOrderBy As String = ""
            If (sorting = MyListsSortEnum.Recent) Then
                sqlOrderBy = vbCrLf & _
                    "Order By ProfilesViewedDateTimeToCreate desc"
            ElseIf (sorting = MyListsSortEnum.Oldest) Then
                sqlOrderBy = vbCrLf & _
                    "Order By ProfilesViewedDateTimeToCreate Asc"
            End If

            sql = sql.Replace("[QueryOrdering]", sqlOrderBy)

            Dim command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql)
            command.Parameters.Add(New SqlClient.SqlParameter("@CurrentProfileId", CurrentProfileId))
            command.Parameters.Add(New SqlClient.SqlParameter("@ReturnRecordsWithStatus", ReturnRecordsWithStatus))
            command.Parameters.Add(New SqlClient.SqlParameter("@NumberOfRecordsToReturn", NumberOfRecordsToReturn))
            command.Parameters.Add(New SqlClient.SqlParameter("@Distance", Distance))
            command.Parameters.Add(New SqlClient.SqlParameter("@DISTANCE_DEFAULT", DISTANCE_DEFAULT))
            'command.Parameters.Add(New SqlClient.SqlParameter("@zip", zipstr))
            command.Parameters.Add(New SqlClient.SqlParameter("@performCount", parms.performCount))
            command.Parameters.Add(New SqlClient.SqlParameter("@RowNumberMin", parms.rowNumberMin))
            command.Parameters.Add(New SqlClient.SqlParameter("@RowNumberMax", parms.rowNumberMax))

            If (String.IsNullOrEmpty(zipstr)) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@zip", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@zip", zipstr))
            End If
            If (latitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", latitudeIn))
            End If
            If (longitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", longitudeIn))
            End If

            If (isOnline) Then
                Dim mins As Integer = clsConfigValues.Get__members_online_minutes()
                Dim LastActivityUTCDate As DateTime = Date.UtcNow.AddMinutes(-mins)

                command.Parameters.Add(New SqlClient.SqlParameter("@LastActivityDateTime", LastActivityUTCDate))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@LastActivityDateTime", DBNull.Value))
            End If

            dt = DataHelpers.GetDataSet(command)

        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        Finally
            clsLogger.InsertLog("GetWhoViewedMeMembersDataTable", CurrentProfileId, (DateTime.UtcNow - __logdate).TotalMilliseconds)
        End Try

        Return dt
    End Function


    Public Shared Function GetMyViewedMembersDataTable(parms As clsMyListsHelperParameters) As DataSet

        Dim __logdate As DateTime = DateTime.UtcNow
        Dim CurrentProfileId As Integer = parms.CurrentProfileId
        Dim ReturnRecordsWithStatus As Integer = parms.ReturnRecordsWithStatus
        Dim sorting As MyListsSortEnum = parms.sorting
        Dim zipstr As String = parms.zipstr
        Dim latitudeIn As Double? = parms.latitudeIn
        Dim longitudeIn As Double? = parms.longitudeIn
        Dim Distance As Integer = parms.Distance
        Dim NumberOfRecordsToReturn As Integer = parms.NumberOfRecordsToReturn
        Dim performCount As Boolean = parms.performCount
        Dim isOnline As Boolean = parms.isOnline

        If (Distance = 0) Then
            Distance = DISTANCE_DEFAULT
        End If

        Dim sql As String = ""
        Dim dt As New DataSet()

        'This script is based on kilometer. If you use miles simply convert the radius to kilometer (radius=radius*1.609344)
        Try

            sql = <sql><![CDATA[
--fn:GetMyViewedMembersDataTable
	--@CurrentProfileId int
	--,@ReturnRecordsWithStatus int
	--,@NumberOfRecordsToReturn int =0
	--,@Distance int=0




if(@performCount=1)
begin

    select Count(*) from (
	    SELECT    
		    distance =isNull(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)), 1000)
	    FROM		dbo.EUS_Profiles AS EUS_Profiles 
		    with (nolock)
	    INNER JOIN
			      dbo.EUS_ProfilesViewed AS viewed ON viewed.ToProfileID = EUS_Profiles.ProfileID AND viewed.FromProfileID = @CurrentProfileId  AND isnull(viewed.FromProfileDeleted,0) = 0  
   	    LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.CustomerPhotosID = EUS_Profiles.DefPhotoID
	    WHERE
		    EUS_Profiles.IsMaster = 1
	     AND EUS_Profiles.[ProfileID] <> 1 
	    AND EUS_Profiles.Status = @ReturnRecordsWithStatus

	    /* blocked filter */
	    AND  not exists(select * 
						    from EUS_ProfilesBlocked bl 
		                    with (nolock)
						    where (bl.FromProfileID =EUS_Profiles.ProfileID and bl.ToProfileID = @CurrentProfileId) or
							    (bl.FromProfileID =@CurrentProfileId and bl.ToProfileID = EUS_Profiles.ProfileID)
					    )

	    /* hide me from other users' who viewed me list */
	    AND (		(EUS_Profiles.PrivacySettings_NotShowInOtherUsersViewedList = 1 and
					    not exists(select * 
						    from EUS_ProfilesViewed bl 
		                    with (nolock)
						    where bl.FromProfileID = EUS_Profiles.ProfileID and bl.ToProfileID = @CurrentProfileId
				    )
				    OR 
					    EUS_Profiles.PrivacySettings_NotShowInOtherUsersViewedList = 0
				    OR
					    EUS_Profiles.PrivacySettings_NotShowInOtherUsersViewedList is null
			    )
		    )
    ) as t
    where 
      ((distance <= @Distance and @Distance <@DISTANCE_DEFAULT)  OR ((distance <= @DISTANCE_DEFAULT) and @Distance =@DISTANCE_DEFAULT))
end


	SET NOCOUNT ON;
	if(@NumberOfRecordsToReturn>0)
		SET ROWCOUNT @NumberOfRecordsToReturn;



select 
	tOutter.*
    ,OffersCount = ISNULL((select COUNT(*) 
		                    from EUS_Offers 
		                    where (ToProfileID=tOutter.ProfileID and  FromProfileID=@CurrentProfileId)
		                    or (ToProfileID=@CurrentProfileId and FromProfileID=tOutter.ProfileID)
	                    ),0)
    ,HasFavoritedMe = ISNULL((select COUNT(*)
                            from 	dbo.EUS_ProfilesFavorite favMe
                            where   favMe.ToProfileID = @CurrentProfileId AND favMe.FromProfileID = tOutter.ProfileID 
                            ),0)
    ,DidIFavorited = ISNULL((select COUNT(*)
                            from 	dbo.EUS_ProfilesFavorite favMe
                            where   favMe.ToProfileID = tOutter.ProfileID AND favMe.FromProfileID = @CurrentProfileId
                            ),0)
    ,CommunicationUnl = ISNULL((select COUNT(*)
                            from 	dbo.EUS_UnlockedConversations unl
                            where   ( unl.FromProfileID = @CurrentProfileId AND unl.ToProfileID = tOutter.ProfileID) OR
		                            ( unl.FromProfileID = tOutter.ProfileID  AND unl.ToProfileID = @CurrentProfileId)
                            ),0)

from
(
    select 
	    RowNumber = Row_Number() over([QueryOrdering])
		,tRowNumber.* 
    from (
	    SELECT    
		    distance =isNull(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)), 1000),
            EUS_Profiles.ProfileID, EUS_Profiles.IsMaster, EUS_Profiles.MirrorProfileID, EUS_Profiles.Status, EUS_Profiles.LoginName, 
            EUS_Profiles.FirstName, EUS_Profiles.LastName, EUS_Profiles.GenderId, EUS_Profiles.Country, EUS_Profiles.Region, 
            EUS_Profiles.City, EUS_Profiles.Zip, EUS_Profiles.CityArea, EUS_Profiles.Address, EUS_Profiles.Telephone, EUS_Profiles.eMail, 
            EUS_Profiles.Cellular, EUS_Profiles.AreYouWillingToTravel, EUS_Profiles.AboutMe_Heading, 
            EUS_Profiles.AboutMe_DescribeYourself, EUS_Profiles.AboutMe_DescribeAnIdealFirstDate, 
            EUS_Profiles.OtherDetails_EducationID, EUS_Profiles.OtherDetails_AnnualIncomeID, 
            EUS_Profiles.OtherDetails_NetWorthID, EUS_Profiles.OtherDetails_Occupation, EUS_Profiles.PersonalInfo_HeightID, 
            EUS_Profiles.PersonalInfo_BodyTypeID, EUS_Profiles.PersonalInfo_EyeColorID, 
            EUS_Profiles.PersonalInfo_HairColorID, EUS_Profiles.PersonalInfo_ChildrenID, EUS_Profiles.PersonalInfo_EthnicityID,
            EUS_Profiles.PersonalInfo_ReligionID, EUS_Profiles.PersonalInfo_SmokingHabitID, 
            EUS_Profiles.PersonalInfo_DrinkingHabitID, EUS_Profiles.LookingFor_ToMeetMaleID, 
            EUS_Profiles.LookingFor_ToMeetFemaleID, EUS_Profiles.LookingFor_RelationshipStatusID, 
            EUS_Profiles.LookingFor_TypeOfDating_ShortTermRelationship, EUS_Profiles.LookingFor_TypeOfDating_Friendship,
            EUS_Profiles.LookingFor_TypeOfDating_LongTermRelationship, 
            EUS_Profiles.LookingFor_TypeOfDating_MutuallyBeneficialArrangements, EUS_Profiles.LookingFor_TypeOfDating_MarriedDating, 
            EUS_Profiles.LookingFor_TypeOfDating_AdultDating_Casual, 
            EUS_Profiles.DateTimeToRegister, 
            EUS_Profiles.RegisterIP, 
            EUS_Profiles.LastLoginDateTime, 
            EUS_Profiles.LastUpdateProfileDateTime, 
            EUS_Profiles.LAGID, 
            EUS_Profiles.Birthday, 
            EUS_Profiles.IsOnline, 
            EUS_Profiles.LastActivityDateTime, 
            EUS_Profiles.CelebratingBirth, 
            phot.CustomerPhotosID, 
            phot.CustomerID, 
            phot.DateTimeToUploading, phot.FileName, 
            phot.DisplayLevel, 
            phot.HasAproved, phot.HasDeclined, 
            phot.CheckedContextID, 
            phot.IsDefault, 
	        [HasPhoto] = case 
			        when [EUS_Profiles].PhotosPublic > 0  then 1
			        when [EUS_Profiles].PhotosPrivate > 0  then 1
			        when phot.[CustomerID] IS NULL  then 0
			        else 1 
			    end,

	        viewed.EUS_ProfilesViewedID as ProfilesViewedID, 
	        viewed.DateTimeToCreate as ProfilesViewedDateTimeToCreate, 
	        viewed.FromProfileID as ProfilesViewedFromProfileID, 
	        viewed.ToProfileID as ProfilesViewedToProfileID

	    FROM		dbo.EUS_Profiles AS EUS_Profiles 
		    with (nolock)
	    INNER JOIN
			      dbo.EUS_ProfilesViewed AS viewed ON viewed.ToProfileID = EUS_Profiles.ProfileID AND viewed.FromProfileID = @CurrentProfileId  AND isnull(viewed.FromProfileDeleted,0) = 0  
   	    LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.CustomerPhotosID = EUS_Profiles.DefPhotoID
	    WHERE
		    EUS_Profiles.IsMaster = 1
	     AND EUS_Profiles.[ProfileID] <> 1 
	    AND EUS_Profiles.Status = @ReturnRecordsWithStatus

	    /* blocked filter */
	    AND  not exists(select * 
						    from EUS_ProfilesBlocked bl 
		                    with (nolock)
						    where (bl.FromProfileID =EUS_Profiles.ProfileID and bl.ToProfileID = @CurrentProfileId) or
							    (bl.FromProfileID =@CurrentProfileId and bl.ToProfileID = EUS_Profiles.ProfileID)
					    )

	    /* hide me from other users' who viewed me list */
	    AND (		(EUS_Profiles.PrivacySettings_NotShowInOtherUsersViewedList = 1 and
					    not exists(select * 
						    from EUS_ProfilesViewed bl 
		                    with (nolock)
						    where bl.FromProfileID = EUS_Profiles.ProfileID and bl.ToProfileID = @CurrentProfileId
				    )
				    OR 
					    EUS_Profiles.PrivacySettings_NotShowInOtherUsersViewedList = 0
				    OR
					    EUS_Profiles.PrivacySettings_NotShowInOtherUsersViewedList is null
			    )
		    )
    ) as tRowNumber
    where 
        ((distance <= @Distance and @Distance <@DISTANCE_DEFAULT)  OR ((distance <= @DISTANCE_DEFAULT) and @Distance =@DISTANCE_DEFAULT))
) as tOutter
where 
	tOutter.RowNumber > @RowNumberMin and
	tOutter.RowNumber <= @RowNumberMax 
order by tOutter.RowNumber
]]></sql>.Value



            Dim sqlOrderBy As String = ""
            If (sorting = MyListsSortEnum.Recent) Then
                sqlOrderBy = vbCrLf & _
                    "Order By ProfilesViewedDateTimeToCreate desc"
            ElseIf (sorting = MyListsSortEnum.Oldest) Then
                sqlOrderBy = vbCrLf & _
                    "Order By ProfilesViewedDateTimeToCreate Asc"
            End If

            sql = sql.Replace("[QueryOrdering]", sqlOrderBy)

            Dim command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql)
            command.Parameters.Add(New SqlClient.SqlParameter("@CurrentProfileId", CurrentProfileId))
            command.Parameters.Add(New SqlClient.SqlParameter("@ReturnRecordsWithStatus", ReturnRecordsWithStatus))
            command.Parameters.Add(New SqlClient.SqlParameter("@NumberOfRecordsToReturn", NumberOfRecordsToReturn))
            command.Parameters.Add(New SqlClient.SqlParameter("@Distance", Distance))
            command.Parameters.Add(New SqlClient.SqlParameter("@DISTANCE_DEFAULT", DISTANCE_DEFAULT))
            'command.Parameters.Add(New SqlClient.SqlParameter("@zip", zipstr))
            command.Parameters.Add(New SqlClient.SqlParameter("@performCount", parms.performCount))
            command.Parameters.Add(New SqlClient.SqlParameter("@RowNumberMin", parms.rowNumberMin))
            command.Parameters.Add(New SqlClient.SqlParameter("@RowNumberMax", parms.rowNumberMax))

            If (String.IsNullOrEmpty(zipstr)) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@zip", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@zip", zipstr))
            End If
            If (latitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", latitudeIn))
            End If
            If (longitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", longitudeIn))
            End If


            dt = DataHelpers.GetDataSet(command)

        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        Finally
            clsLogger.InsertLog("GetMyViewedMembersDataTable", CurrentProfileId, (DateTime.UtcNow - __logdate).TotalMilliseconds)
        End Try

        Return dt
    End Function



    Public Shared Function GetMyBlockedMembersDataTable(parms As clsMyListsHelperParameters) As DataSet


        Dim __logdate As DateTime = DateTime.UtcNow

        Dim CurrentProfileId As Integer = parms.CurrentProfileId
        Dim ReturnRecordsWithStatus As Integer = parms.ReturnRecordsWithStatus
        Dim sorting As MyListsSortEnum = parms.sorting
        Dim zipstr As String = parms.zipstr
        Dim latitudeIn As Double? = parms.latitudeIn
        Dim longitudeIn As Double? = parms.longitudeIn
        Dim Distance As Integer = parms.Distance
        Dim NumberOfRecordsToReturn As Integer = parms.NumberOfRecordsToReturn
        Dim performCount As Boolean = parms.performCount
        Dim isOnline As Boolean = parms.isOnline

        If (Distance = 0) Then
            Distance = DISTANCE_DEFAULT
        End If

        Dim sql As String = ""
        Dim dt As New DataSet()

        'This script is based on kilometer. If you use miles simply convert the radius to kilometer (radius=radius*1.609344)
        Try

            sql = <sql><![CDATA[
--fn:GetMyBlockedMembersDataTable
	--@CurrentProfileId int
	--,@ReturnRecordsWithStatus int
	--,@NumberOfRecordsToReturn int =0
	--,@Distance int=0



if(@performCount=1)
begin

    select Count(*) from (
	    SELECT    
		    distance =isNull(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)), 1000)
	    FROM		dbo.EUS_Profiles AS EUS_Profiles 
		    with (nolock)
	    INNER JOIN
			      dbo.EUS_ProfilesBlocked AS blocked ON blocked.ToProfileID = EUS_Profiles.ProfileID AND blocked.FromProfileID = @CurrentProfileId  
   	    LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.CustomerPhotosID = EUS_Profiles.DefPhotoID
	    WHERE
		    EUS_Profiles.IsMaster = 1
	     AND EUS_Profiles.[ProfileID] <> 1 
	    AND EUS_Profiles.Status = @ReturnRecordsWithStatus
    ) as t
    where 
      ((distance <= @Distance and @Distance <@DISTANCE_DEFAULT)  OR ((distance <= @DISTANCE_DEFAULT) and @Distance =@DISTANCE_DEFAULT))
end


	SET NOCOUNT ON;
	if(@NumberOfRecordsToReturn>0)
		SET ROWCOUNT @NumberOfRecordsToReturn;



select 
	tOutter.*

	,OffersCount = ISNULL((select COUNT(*) 
		                from EUS_Offers 
		                where (ToProfileID=tOutter.ProfileID and  FromProfileID=@CurrentProfileId)
		                or (ToProfileID=@CurrentProfileId and FromProfileID=tOutter.ProfileID)
	                ),0)
    ,HasFavoritedMe = ISNULL((select COUNT(*)
                        from 	dbo.EUS_ProfilesFavorite favMe
                        where   favMe.ToProfileID = @CurrentProfileId AND favMe.FromProfileID = tOutter.ProfileID 
                        ),0)
    ,DidIFavorited = ISNULL((select COUNT(*)
                        from 	dbo.EUS_ProfilesFavorite favMe
                        where   favMe.ToProfileID = tOutter.ProfileID AND favMe.FromProfileID = @CurrentProfileId
                        ),0)
    ,CommunicationUnl = ISNULL((select COUNT(*)
                        from 	dbo.EUS_UnlockedConversations unl
                        where   ( unl.FromProfileID = @CurrentProfileId AND unl.ToProfileID = tOutter.ProfileID) OR
		                        ( unl.FromProfileID = tOutter.ProfileID  AND unl.ToProfileID = @CurrentProfileId)
                        ),0)
from
(
    select 
	    RowNumber = Row_Number() over([QueryOrdering])
		,tRowNumber.* 
    from (
	    SELECT    
		    distance =isNull(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)), 1000),
            EUS_Profiles.ProfileID, EUS_Profiles.IsMaster, EUS_Profiles.MirrorProfileID, EUS_Profiles.Status, EUS_Profiles.LoginName, 
            EUS_Profiles.FirstName, EUS_Profiles.LastName, EUS_Profiles.GenderId, EUS_Profiles.Country, EUS_Profiles.Region, 
            EUS_Profiles.City, EUS_Profiles.Zip, EUS_Profiles.CityArea, EUS_Profiles.Address, EUS_Profiles.Telephone, EUS_Profiles.eMail, 
            EUS_Profiles.Cellular, EUS_Profiles.AreYouWillingToTravel, EUS_Profiles.AboutMe_Heading, 
            EUS_Profiles.AboutMe_DescribeYourself, EUS_Profiles.AboutMe_DescribeAnIdealFirstDate, 
            EUS_Profiles.OtherDetails_EducationID, EUS_Profiles.OtherDetails_AnnualIncomeID, 
            EUS_Profiles.OtherDetails_NetWorthID, EUS_Profiles.OtherDetails_Occupation, EUS_Profiles.PersonalInfo_HeightID, 
            EUS_Profiles.PersonalInfo_BodyTypeID, EUS_Profiles.PersonalInfo_EyeColorID, 
            EUS_Profiles.PersonalInfo_HairColorID, EUS_Profiles.PersonalInfo_ChildrenID, EUS_Profiles.PersonalInfo_EthnicityID,
            EUS_Profiles.PersonalInfo_ReligionID, EUS_Profiles.PersonalInfo_SmokingHabitID, 
            EUS_Profiles.PersonalInfo_DrinkingHabitID, EUS_Profiles.LookingFor_ToMeetMaleID, 
            EUS_Profiles.LookingFor_ToMeetFemaleID, EUS_Profiles.LookingFor_RelationshipStatusID, 
            EUS_Profiles.LookingFor_TypeOfDating_ShortTermRelationship, EUS_Profiles.LookingFor_TypeOfDating_Friendship,
            EUS_Profiles.LookingFor_TypeOfDating_LongTermRelationship, 
            EUS_Profiles.LookingFor_TypeOfDating_MutuallyBeneficialArrangements, EUS_Profiles.LookingFor_TypeOfDating_MarriedDating, 
            EUS_Profiles.LookingFor_TypeOfDating_AdultDating_Casual, 
            EUS_Profiles.DateTimeToRegister, 
            EUS_Profiles.RegisterIP, 
            EUS_Profiles.LastLoginDateTime, 
            EUS_Profiles.LastUpdateProfileDateTime, 
            EUS_Profiles.LAGID, 
            EUS_Profiles.Birthday, 
            EUS_Profiles.IsOnline, 
            EUS_Profiles.LastActivityDateTime, 
            EUS_Profiles.CelebratingBirth, 
            phot.CustomerPhotosID, 
            phot.CustomerID, 
            phot.DateTimeToUploading, phot.FileName, 
            phot.DisplayLevel, 
            phot.HasAproved, phot.HasDeclined, 
            phot.CheckedContextID, 
            phot.IsDefault, 
	        [HasPhoto] = case 
			        when [EUS_Profiles].PhotosPublic > 0  then 1
			        when [EUS_Profiles].PhotosPrivate > 0  then 1
			        when phot.[CustomerID] IS NULL  then 0
			        else 1 
			    end,

	        blocked.EUS_ProfilesBlockedID as ProfilesBlockedID, 
	        blocked.DateTimeToCreate as ProfilesBlockedDateTimeToCreate, 
	        blocked.FromProfileID as ProfilesBlockedFromProfileID, 
	        blocked.ToProfileID as ProfilesBlockedToProfileID
	    FROM		dbo.EUS_Profiles AS EUS_Profiles 
		    with (nolock)
	    INNER JOIN
			      dbo.EUS_ProfilesBlocked AS blocked ON blocked.ToProfileID = EUS_Profiles.ProfileID AND blocked.FromProfileID = @CurrentProfileId  
   	    LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.CustomerPhotosID = EUS_Profiles.DefPhotoID
	    WHERE
		    EUS_Profiles.IsMaster = 1
	     AND EUS_Profiles.[ProfileID] <> 1 
	    AND EUS_Profiles.Status = @ReturnRecordsWithStatus
    ) as tRowNumber
    where 
        ((distance <= @Distance and @Distance <@DISTANCE_DEFAULT)  OR ((distance <= @DISTANCE_DEFAULT) and @Distance =@DISTANCE_DEFAULT))
) as tOutter
where 
	tOutter.RowNumber > @RowNumberMin and
	tOutter.RowNumber <= @RowNumberMax 
order by tOutter.RowNumber
]]></sql>.Value

            Dim sqlOrderBy As String = ""
            If (sorting = MyListsSortEnum.Recent) Then
                sqlOrderBy = vbCrLf & _
                    "Order By ProfilesBlockedDateTimeToCreate desc"
            ElseIf (sorting = MyListsSortEnum.Oldest) Then
                sqlOrderBy = vbCrLf & _
                    "Order By ProfilesBlockedDateTimeToCreate Asc"
            End If

            sql = sql.Replace("[QueryOrdering]", sqlOrderBy)

            Dim command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql)
            command.Parameters.Add(New SqlClient.SqlParameter("@CurrentProfileId", CurrentProfileId))
            command.Parameters.Add(New SqlClient.SqlParameter("@ReturnRecordsWithStatus", ReturnRecordsWithStatus))
            command.Parameters.Add(New SqlClient.SqlParameter("@NumberOfRecordsToReturn", NumberOfRecordsToReturn))
            command.Parameters.Add(New SqlClient.SqlParameter("@Distance", Distance))
            command.Parameters.Add(New SqlClient.SqlParameter("@DISTANCE_DEFAULT", DISTANCE_DEFAULT))
            'command.Parameters.Add(New SqlClient.SqlParameter("@zip", zipstr))
            command.Parameters.Add(New SqlClient.SqlParameter("@performCount", parms.performCount))
            command.Parameters.Add(New SqlClient.SqlParameter("@RowNumberMin", parms.rowNumberMin))
            command.Parameters.Add(New SqlClient.SqlParameter("@RowNumberMax", parms.rowNumberMax))

            If (String.IsNullOrEmpty(zipstr)) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@zip", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@zip", zipstr))
            End If
            If (latitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", latitudeIn))
            End If
            If (longitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", longitudeIn))
            End If

            dt = DataHelpers.GetDataSet(command)

        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        Finally
            clsLogger.InsertLog("GetMyBlockedMembersDataTable", CurrentProfileId, (DateTime.UtcNow - __logdate).TotalMilliseconds)
        End Try

        Return dt
    End Function


    Public Shared Function GetWinksDataTable(ByRef parms As clsWinksHelperParameters) As DataSet

        Dim __logdate As DateTime = DateTime.UtcNow

        Dim CurrentProfileId As Integer = parms.CurrentProfileId
        Dim ReturnRecordsWithStatus As Integer = parms.ReturnRecordsWithStatus
        Dim sorting As OffersSortEnum = parms.sorting
        Dim zipstr As String = parms.zipstr
        Dim latitudeIn As Double? = parms.latitudeIn
        Dim longitudeIn As Double? = parms.longitudeIn
        Dim Distance As Integer = parms.Distance
        Dim NumberOfRecordsToReturn As Integer = parms.NumberOfRecordsToReturn
        Dim performCount As Boolean = parms.performCount

        If (Distance = 0) Then
            Distance = DISTANCE_DEFAULT
        End If

        Dim sql As String = ""
        Dim dt As New DataSet()

        'This script is based on kilometer. If you use miles simply convert the radius to kilometer (radius=radius*1.609344)
        Try

            sql = <sql><![CDATA[
--fn:GetWinksDataTable
	--@CurrentProfileId int
	--,@ReturnRecordsWithStatus int
	--,@NumberOfRecordsToReturn int =0
	--,@Distance int=0


if(@performCount=1)
begin

    select Count(*) from (
	    SELECT    
		    distance =isNull(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)), 1000)
	    FROM
			    dbo.EUS_Profiles EUS_Profiles
	    INNER JOIN  dbo.EUS_Offers offr ON offr.FromProfileID = EUS_Profiles.ProfileID
   	    LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.CustomerPhotosID = EUS_Profiles.DefPhotoID
	    WHERE     
		    EUS_Profiles.IsMaster = 1
	     AND EUS_Profiles.[ProfileID] <> 1 
	    AND offr.StatusID IN (SELECT [EUS_OffersStatusID]  
						     FROM [dbo].[EUS_OffersStatus]  
						     WHERE [ConstantName]='PENDING')
	    AND offr.OfferTypeID IN (SELECT [EUS_OffersTypeID]  
							     FROM [dbo].[EUS_OffersTypes]  
							     WHERE [ConstantName]='WINK' OR [ConstantName]='POKE')
	    AND offr.ToProfileID = @CurrentProfileId
        AND isnull(offr.ToProfileDeleted,0) = 0
	    AND	EUS_Profiles.Status = @ReturnRecordsWithStatus
    ) as t
    where 
      ((distance <= @Distance and @Distance <@DISTANCE_DEFAULT)  OR ((distance <= @DISTANCE_DEFAULT) and @Distance =@DISTANCE_DEFAULT))
end


	SET NOCOUNT ON;
	if(@NumberOfRecordsToReturn>0)
		SET ROWCOUNT @NumberOfRecordsToReturn;

select 
	tOutter.*
    ,CommunicationUnl = 
        ISNULL((select COUNT(*)
		        from 	dbo.EUS_UnlockedConversations unl
		        where (
			        ( unl.FromProfileID = @CurrentProfileId AND unl.ToProfileID = tOutter.ProfileID) OR
			        ( unl.FromProfileID = tOutter.ProfileID  AND unl.ToProfileID = @CurrentProfileId)
		        )
		        AND unl.DateTimeCreated <= '2013-02-08'
        ),0)
    ,IsMessageSent = 
        ISNULL((	select sum(IsMessageSent)
	        from(
		        (select COUNT(*) as IsMessageSent
			        from 	dbo.EUS_Messages msg
			        where (
					        msg.FromProfileID = tOutter.ProfileID  AND msg.ToProfileID = @CurrentProfileId
			        )
			        and ISNULL(msg.IsHidden,0)=0)
		        union	
		        (select COUNT(*) as IsMessageSent
			        from 	dbo.EUS_ProfilesCommunication msg
			        where (
					        msg.FromProfileID = tOutter.ProfileID  AND msg.ToProfileID = @CurrentProfileId
			        )
			        and msg.SentMessage>0
		        )
	        ) as t11
        ),0)
    ,IsOnlineNow=CAST((CASE
                        WHEN exists(select ProfileID 
                            from [EUS_ProfilesPrivacySettings]
                            where (ProfileID=tOutter.ProfileID OR [MirrorProfileID]=tOutter.ProfileID)
                            and [PrivacySettings_ShowMeOffline]=1) THEN 0
                        WHEN not @LastActivityUTCDate is null and (tOutter.IsOnline=1 and tOutter.LastActivityDateTime>=@LastActivityUTCDate) THEN 1
                        ELSE 0
                    END) as bit)
from
(
    select 
	    RowNumber = Row_Number() over([QueryOrdering])
		,tRowNumber.* 
    from (
	    SELECT    
		    distance =isNull(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)), 1000),
            EUS_Profiles.ProfileID, EUS_Profiles.IsMaster, EUS_Profiles.MirrorProfileID, EUS_Profiles.Status, EUS_Profiles.LoginName, 
            EUS_Profiles.FirstName, EUS_Profiles.LastName, EUS_Profiles.GenderId, EUS_Profiles.Country, EUS_Profiles.Region, 
            EUS_Profiles.City, EUS_Profiles.Zip, EUS_Profiles.CityArea, EUS_Profiles.Address, EUS_Profiles.Telephone, EUS_Profiles.eMail, 
            EUS_Profiles.Cellular, EUS_Profiles.AreYouWillingToTravel, EUS_Profiles.AboutMe_Heading, 
            EUS_Profiles.AboutMe_DescribeYourself, EUS_Profiles.AboutMe_DescribeAnIdealFirstDate, 
            EUS_Profiles.OtherDetails_EducationID, EUS_Profiles.OtherDetails_AnnualIncomeID, 
            EUS_Profiles.OtherDetails_NetWorthID, EUS_Profiles.OtherDetails_Occupation, EUS_Profiles.PersonalInfo_HeightID, 
            EUS_Profiles.PersonalInfo_BodyTypeID, EUS_Profiles.PersonalInfo_EyeColorID, 
            EUS_Profiles.PersonalInfo_HairColorID, EUS_Profiles.PersonalInfo_ChildrenID, EUS_Profiles.PersonalInfo_EthnicityID,
            EUS_Profiles.PersonalInfo_ReligionID, EUS_Profiles.PersonalInfo_SmokingHabitID, 
            EUS_Profiles.PersonalInfo_DrinkingHabitID, EUS_Profiles.LookingFor_ToMeetMaleID, 
            EUS_Profiles.LookingFor_ToMeetFemaleID, EUS_Profiles.LookingFor_RelationshipStatusID, 
            EUS_Profiles.LookingFor_TypeOfDating_ShortTermRelationship, EUS_Profiles.LookingFor_TypeOfDating_Friendship,
            EUS_Profiles.LookingFor_TypeOfDating_LongTermRelationship, 
            EUS_Profiles.LookingFor_TypeOfDating_MutuallyBeneficialArrangements, EUS_Profiles.LookingFor_TypeOfDating_MarriedDating, 
            EUS_Profiles.LookingFor_TypeOfDating_AdultDating_Casual, 
            EUS_Profiles.DateTimeToRegister, 
            EUS_Profiles.RegisterIP, 
            EUS_Profiles.LastLoginDateTime, 
            EUS_Profiles.LastUpdateProfileDateTime, 
            EUS_Profiles.LAGID, 
            EUS_Profiles.Birthday, 
            EUS_Profiles.IsOnline, 
            EUS_Profiles.LastActivityDateTime, 
            phot.CustomerPhotosID, 
            phot.CustomerID, 
            phot.DateTimeToUploading, phot.FileName, 
            phot.DisplayLevel, 
            phot.HasAproved, phot.HasDeclined, 
            phot.CheckedContextID, 
            phot.IsDefault, 
	        [HasPhoto] = case 
			        when [EUS_Profiles].PhotosPublic > 0  then 1
			        when [EUS_Profiles].PhotosPrivate > 0  then 1
			        when phot.[CustomerID] IS NULL  then 0
			        else 1 
			    end,

		    offr.OfferID, 
		    offr.DateTimeToCreate as OffersDateTimeToCreate , 
		    offr.OfferTypeID as OffersOfferTypeID, 
		    offr.FromProfileID as OffersFromProfileID, 
		    offr.ToProfileID as OffersToProfileID, 
		    offr.Amount as OffersAmount, 
		    offr.StatusID as OffersStatusID
	    FROM
			    dbo.EUS_Profiles EUS_Profiles
	    INNER JOIN  dbo.EUS_Offers offr ON offr.FromProfileID = EUS_Profiles.ProfileID
   	    LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.CustomerPhotosID = EUS_Profiles.DefPhotoID
	    WHERE     
		    EUS_Profiles.IsMaster = 1
	     AND EUS_Profiles.[ProfileID] <> 1 
	    AND offr.StatusID IN (SELECT [EUS_OffersStatusID]  
						     FROM [dbo].[EUS_OffersStatus]  
						     WHERE [ConstantName]='PENDING')
	    AND offr.OfferTypeID IN (SELECT [EUS_OffersTypeID]  
							     FROM [dbo].[EUS_OffersTypes]  
							     WHERE [ConstantName]='WINK' OR [ConstantName]='POKE')
	    AND offr.ToProfileID = @CurrentProfileId
        AND isnull(offr.ToProfileDeleted,0) = 0
	    AND	EUS_Profiles.Status = @ReturnRecordsWithStatus
    ) as tRowNumber
    where 
        ((distance <= @Distance and @Distance <@DISTANCE_DEFAULT)  OR ((distance <= @DISTANCE_DEFAULT) and @Distance =@DISTANCE_DEFAULT))
) as tOutter
where 
	tOutter.RowNumber > @RowNumberMin and
	tOutter.RowNumber <= @RowNumberMax 
order by tOutter.RowNumber
]]></sql>.Value

            Dim sqlOrderBy As String = ""
            If (sorting = OffersSortEnum.OfferAmountHighest) Then
                sqlOrderBy = vbCrLf & _
                    "order by OffersAmount desc"
            ElseIf (sorting = OffersSortEnum.OfferAmountLowest) Then
                sqlOrderBy = vbCrLf & _
                    "Order By OffersAmount asc"
            ElseIf (sorting = OffersSortEnum.OldestOffers) Then
                sqlOrderBy = vbCrLf & _
                    "Order By OffersDateTimeToCreate Asc"
            ElseIf (sorting = OffersSortEnum.RecentOffers) Then
                sqlOrderBy = vbCrLf & _
                    "Order By OffersDateTimeToCreate Desc"
            End If

            sql = sql.Replace("[QueryOrdering]", sqlOrderBy)

            Dim command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql)
            command.Parameters.Add(New SqlClient.SqlParameter("@CurrentProfileId", CurrentProfileId))
            command.Parameters.Add(New SqlClient.SqlParameter("@ReturnRecordsWithStatus", ReturnRecordsWithStatus))
            command.Parameters.Add(New SqlClient.SqlParameter("@NumberOfRecordsToReturn", NumberOfRecordsToReturn))
            command.Parameters.Add(New SqlClient.SqlParameter("@Distance", Distance))
            command.Parameters.Add(New SqlClient.SqlParameter("@DISTANCE_DEFAULT", DISTANCE_DEFAULT))
            command.Parameters.Add(New SqlClient.SqlParameter("@performCount", parms.performCount))
            command.Parameters.Add(New SqlClient.SqlParameter("@RowNumberMin", parms.rowNumberMin))
            command.Parameters.Add(New SqlClient.SqlParameter("@RowNumberMax", parms.rowNumberMax))

            If (String.IsNullOrEmpty(zipstr)) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@zip", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@zip", zipstr))
            End If
            If (latitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", latitudeIn))
            End If
            If (longitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", longitudeIn))
            End If

            Try
                Dim mins As Integer = clsConfigValues.Get__members_online_minutes()
                Dim LastActivityUTCDate As DateTime = Date.UtcNow.AddMinutes(-mins)
                command.Parameters.Add(New SqlClient.SqlParameter("@LastActivityUTCDate", LastActivityUTCDate))
            Catch
                command.Parameters.Add(New SqlClient.SqlParameter("@LastActivityUTCDate", System.DBNull.Value))
            End Try


            dt = DataHelpers.GetDataSet(command)

        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        Finally
            clsLogger.InsertLog("GetWinksDataTable", CurrentProfileId, (DateTime.UtcNow - __logdate).TotalMilliseconds)
        End Try

        Return dt
    End Function



    Public Shared Function GetNewOffersDataTable(ByRef parms As clsWinksHelperParameters) As DataSet
        Dim CurrentProfileId As Integer = parms.CurrentProfileId
        Dim ReturnRecordsWithStatus As Integer = parms.ReturnRecordsWithStatus
        Dim sorting As OffersSortEnum = parms.sorting
        Dim zipstr As String = parms.zipstr
        Dim latitudeIn As Double? = parms.latitudeIn
        Dim longitudeIn As Double? = parms.longitudeIn
        Dim Distance As Integer = parms.Distance
        Dim NumberOfRecordsToReturn As Integer = parms.NumberOfRecordsToReturn

        Dim __logdate As DateTime = DateTime.UtcNow
        If (Distance = 0) Then
            Distance = DISTANCE_DEFAULT
        End If

        Dim sql As String = ""
        Dim dt As New DataSet()

        'This script is based on kilometer. If you use miles simply convert the radius to kilometer (radius=radius*1.609344)
        Try

            sql = <sql><![CDATA[
--fn:GetNewOffersDataTable
SET NOCOUNT ON;

if(@performCount=1)
begin
    select Count(*) from (
	    SELECT    
		    distance =isNull(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)), 1000)
	    FROM dbo.EUS_Profiles EUS_Profiles
            with(nolock)
	    INNER JOIN  dbo.EUS_Offers offr ON offr.FromProfileID = EUS_Profiles.ProfileID
	    WHERE     
		    EUS_Profiles.IsMaster = 1
	    AND EUS_Profiles.[ProfileID] <> 1 
	    AND	EUS_Profiles.Status = @ReturnRecordsWithStatus
	    AND offr.StatusID IN ([OFFERS_STATUSID_LIST])
	    AND offr.OfferTypeID IN ([OFFERS_TYPEID_LIST])
	    AND offr.ToProfileID = @CurrentProfileId
        AND isnull(offr.ToProfileDeleted,0) = 0
    ) as t
    where 
      ((distance <= @Distance and @Distance <@DISTANCE_DEFAULT)  OR ((distance <= @DISTANCE_DEFAULT) and @Distance =@DISTANCE_DEFAULT))
end


if(@NumberOfRecordsToReturn>0)
	SET ROWCOUNT @NumberOfRecordsToReturn;




select 
	tOutter.*
    ,CommunicationUnl = 
        ISNULL((select COUNT(*)
				from 	dbo.EUS_UnlockedConversations unl
				where (
					( unl.FromProfileID = @CurrentProfileId AND unl.ToProfileID = tOutter.ProfileID) OR
					( unl.FromProfileID = tOutter.ProfileID  AND unl.ToProfileID = @CurrentProfileId)
				)
				AND unl.DateTimeCreated <= '2013-02-08'
        ),0)
    ,IsMessageSent = 
        ISNULL((select sum(IsMessageSent)
				from(
					(select COUNT(*) as IsMessageSent
						from 	dbo.EUS_Messages msg
						where (
								msg.FromProfileID = tOutter.ProfileID  AND msg.ToProfileID = @CurrentProfileId
						)
						and ISNULL(msg.IsHidden,0)=0)
					union	
					(select COUNT(*) as IsMessageSent
						from 	dbo.EUS_ProfilesCommunication msg
						where (
								msg.FromProfileID = tOutter.ProfileID  AND msg.ToProfileID = @CurrentProfileId
						)
						and msg.SentMessage>0
					)
				) as t11
            ),0)
    ,IsOnlineNow=CAST((CASE
                        WHEN exists(select ProfileID 
                            from [EUS_ProfilesPrivacySettings]
                            where (ProfileID=tOutter.ProfileID OR [MirrorProfileID]=tOutter.ProfileID)
                            and [PrivacySettings_ShowMeOffline]=1) THEN 0
                        WHEN not @LastActivityUTCDate is null and (tOutter.IsOnline=1 and tOutter.LastActivityDateTime>=@LastActivityUTCDate) THEN 1
                        ELSE 0
                    END) as bit)
from
(
    select 
	    RowNumber = Row_Number() over([QueryOrdering])
		,tRowNumber.* 
    from (SELECT    
		    distance =isNull(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)), 1000),
            EUS_Profiles.ProfileID, EUS_Profiles.IsMaster, 
            EUS_Profiles.MirrorProfileID, EUS_Profiles.Status, 
            EUS_Profiles.LoginName, 
            EUS_Profiles.FirstName, EUS_Profiles.LastName, 
            EUS_Profiles.GenderId, EUS_Profiles.Country, EUS_Profiles.Region, 
            EUS_Profiles.City, EUS_Profiles.Zip, EUS_Profiles.CityArea, 
            EUS_Profiles.Address, EUS_Profiles.Telephone, EUS_Profiles.eMail, 
            EUS_Profiles.Cellular, EUS_Profiles.AreYouWillingToTravel, 
            EUS_Profiles.AboutMe_Heading, 
            EUS_Profiles.AboutMe_DescribeYourself, 
            EUS_Profiles.AboutMe_DescribeAnIdealFirstDate, 
            EUS_Profiles.OtherDetails_EducationID, 
            EUS_Profiles.OtherDetails_AnnualIncomeID, 
            EUS_Profiles.OtherDetails_NetWorthID, 
            EUS_Profiles.OtherDetails_Occupation, 
            EUS_Profiles.PersonalInfo_HeightID, 
            EUS_Profiles.PersonalInfo_BodyTypeID, 
            EUS_Profiles.PersonalInfo_EyeColorID, 
            EUS_Profiles.PersonalInfo_HairColorID,
            EUS_Profiles.PersonalInfo_ChildrenID, EUS_Profiles.PersonalInfo_EthnicityID,
            EUS_Profiles.PersonalInfo_ReligionID, 
            EUS_Profiles.PersonalInfo_SmokingHabitID, 
            EUS_Profiles.PersonalInfo_DrinkingHabitID, 
            EUS_Profiles.LookingFor_ToMeetMaleID, 
            EUS_Profiles.LookingFor_ToMeetFemaleID, 
            EUS_Profiles.LookingFor_RelationshipStatusID, 
            EUS_Profiles.LookingFor_TypeOfDating_ShortTermRelationship, 
            EUS_Profiles.LookingFor_TypeOfDating_Friendship,
            EUS_Profiles.LookingFor_TypeOfDating_LongTermRelationship, 
            EUS_Profiles.LookingFor_TypeOfDating_MutuallyBeneficialArrangements, 
            EUS_Profiles.LookingFor_TypeOfDating_MarriedDating, 
            EUS_Profiles.LookingFor_TypeOfDating_AdultDating_Casual, 
            EUS_Profiles.DateTimeToRegister, 
            EUS_Profiles.RegisterIP, 
            EUS_Profiles.LastLoginDateTime, 
            EUS_Profiles.LastUpdateProfileDateTime, 
            EUS_Profiles.LAGID, 
            EUS_Profiles.Birthday, 
            EUS_Profiles.IsOnline, 
            EUS_Profiles.LastActivityDateTime, 
            phot.CustomerPhotosID, 
            phot.CustomerID, 
            phot.DateTimeToUploading, phot.FileName, 
            phot.DisplayLevel, 
            phot.HasAproved, phot.HasDeclined, 
            phot.CheckedContextID, 
            phot.IsDefault, 
	        [HasPhoto] = case 
			        when [EUS_Profiles].PhotosPublic > 0  then 1
			        when [EUS_Profiles].PhotosPrivate > 0  then 1
			        when phot.[CustomerID] IS NULL  then 0
			        else 1 
			    end,

		    offr.OfferID, 
		    offr.DateTimeToCreate as OffersDateTimeToCreate , 
		    offr.OfferTypeID as OffersOfferTypeID, 
		    offr.FromProfileID as OffersFromProfileID, 
		    offr.ToProfileID as OffersToProfileID, 
		    offr.Amount as OffersAmount, 
		    offr.StatusID as OffersStatusID
	    FROM
			    dbo.EUS_Profiles EUS_Profiles
            with(nolock)
	    INNER JOIN  dbo.EUS_Offers offr ON offr.FromProfileID = EUS_Profiles.ProfileID
   	    LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.CustomerPhotosID = EUS_Profiles.DefPhotoID
	    WHERE     
		    EUS_Profiles.IsMaster = 1
	    AND EUS_Profiles.[ProfileID] <> 1 
	    AND	EUS_Profiles.Status = @ReturnRecordsWithStatus
	    AND offr.StatusID IN ([OFFERS_STATUSID_LIST])
	    AND offr.OfferTypeID IN ([OFFERS_TYPEID_LIST])
	    AND offr.ToProfileID = @CurrentProfileId
        AND isnull(offr.ToProfileDeleted,0) = 0
    ) as tRowNumber
    where 
        ((distance <= @Distance and @Distance <@DISTANCE_DEFAULT)  OR ((distance <= @DISTANCE_DEFAULT) and @Distance =@DISTANCE_DEFAULT))
) as tOutter
where 
	tOutter.RowNumber > @RowNumberMin and
	tOutter.RowNumber <= @RowNumberMax 
order by tOutter.RowNumber
]]></sql>.Value

            Dim sqlOrderBy As String = ""
            If (sorting = OffersSortEnum.OfferAmountHighest) Then
                sqlOrderBy = vbCrLf & _
                    "order by OffersAmount desc"
            ElseIf (sorting = OffersSortEnum.OfferAmountLowest) Then
                sqlOrderBy = vbCrLf & _
                    "Order By OffersAmount asc"
            ElseIf (sorting = OffersSortEnum.OldestOffers) Then
                sqlOrderBy = vbCrLf & _
                    "Order By OffersDateTimeToCreate Asc"
            ElseIf (sorting = OffersSortEnum.RecentOffers) Then
                sqlOrderBy = vbCrLf & _
                    "Order By OffersDateTimeToCreate Desc"
            End If

            sql = sql.Replace("[QueryOrdering]", sqlOrderBy)


            'AND offr.StatusID IN (SELECT [EUS_OffersStatusID]  
            'FROM[dbo].[EUS_OffersStatus]
            'WHERE [ConstantName]='PENDING' OR [ConstantName]='COUNTER')
            'AND offr.OfferTypeID IN (SELECT [EUS_OffersTypeID]  
            'FROM[dbo].[EUS_OffersTypes]
            'WHERE [ConstantName]='OFFERCOUNTER' OR  [ConstantName]='OFFERNEW')

            Dim OFFERS_STATUSID_LIST As String() = {ProfileHelper.OfferStatusID_PENDING,
                                                    ProfileHelper.OfferStatusID_COUNTER}

            Dim OFFERS_TYPEID_LIST As String() = {ProfileHelper.OfferTypeID_OFFERCOUNTER,
                                                  ProfileHelper.OfferTypeID_OFFERNEW}



            sql = sql.Replace("[OFFERS_STATUSID_LIST]", String.Join(",", OFFERS_STATUSID_LIST))
            sql = sql.Replace("[OFFERS_TYPEID_LIST]", String.Join(",", OFFERS_TYPEID_LIST))


            Dim command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql)
            command.Parameters.Add(New SqlClient.SqlParameter("@CurrentProfileId", CurrentProfileId))
            command.Parameters.Add(New SqlClient.SqlParameter("@ReturnRecordsWithStatus", ReturnRecordsWithStatus))
            command.Parameters.Add(New SqlClient.SqlParameter("@NumberOfRecordsToReturn", NumberOfRecordsToReturn))
            command.Parameters.Add(New SqlClient.SqlParameter("@Distance", Distance))
            command.Parameters.Add(New SqlClient.SqlParameter("@DISTANCE_DEFAULT", DISTANCE_DEFAULT))
            command.Parameters.Add(New SqlClient.SqlParameter("@performCount", parms.performCount))
            command.Parameters.Add(New SqlClient.SqlParameter("@RowNumberMin", parms.rowNumberMin))
            command.Parameters.Add(New SqlClient.SqlParameter("@RowNumberMax", parms.rowNumberMax))

            If (String.IsNullOrEmpty(zipstr)) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@zip", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@zip", zipstr))
            End If

            If (latitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", latitudeIn))
            End If
            If (longitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", longitudeIn))
            End If

            Try
                Dim mins As Integer = clsConfigValues.Get__members_online_minutes()
                Dim LastActivityUTCDate As DateTime = Date.UtcNow.AddMinutes(-mins)
                command.Parameters.Add(New SqlClient.SqlParameter("@LastActivityUTCDate", LastActivityUTCDate))
            Catch
                command.Parameters.Add(New SqlClient.SqlParameter("@LastActivityUTCDate", System.DBNull.Value))
            End Try

            dt = DataHelpers.GetDataSet(command)

        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        Finally
            clsLogger.InsertLog("GetNewOffersDataTable", CurrentProfileId, (DateTime.UtcNow - __logdate).TotalMilliseconds)
        End Try

        Return dt
    End Function




    Public Shared Function GetPendingOffersDataTable(ByRef parms As clsWinksHelperParameters) As DataSet
        Dim CurrentProfileId As Integer = parms.CurrentProfileId
        Dim ReturnRecordsWithStatus As Integer = parms.ReturnRecordsWithStatus
        Dim sorting As OffersSortEnum = parms.sorting
        Dim zipstr As String = parms.zipstr
        Dim latitudeIn As Double? = parms.latitudeIn
        Dim longitudeIn As Double? = parms.longitudeIn
        Dim Distance As Integer = parms.Distance
        Dim NumberOfRecordsToReturn As Integer = parms.NumberOfRecordsToReturn

        Dim __logdate As DateTime = DateTime.UtcNow
        If (Distance = 0) Then
            Distance = DISTANCE_DEFAULT
        End If

        Dim sql As String = ""
        Dim dt As New DataSet()

        'This script is based on kilometer. If you use miles simply convert the radius to kilometer (radius=radius*1.609344)
        Try

            sql = <sql><![CDATA[
--fn:GetPendingOffersDataTable
SET NOCOUNT ON;

if(@performCount=1)
begin
    select Count(*) from (
	    SELECT    
		    distance =isNull(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)), 1000)
	    FROM dbo.EUS_Profiles EUS_Profiles
            with(nolock)
	    INNER JOIN  dbo.EUS_Offers offr ON offr.FromProfileID = @CurrentProfileId AND offr.ToProfileID = EUS_Profiles.ProfileID and isnull(offr.FromProfileDeleted,0) = 0
   	    LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.CustomerPhotosID = EUS_Profiles.DefPhotoID
	    WHERE     
		    EUS_Profiles.IsMaster = 1
	    AND EUS_Profiles.[ProfileID] <> 1 
	    AND	EUS_Profiles.Status = @ReturnRecordsWithStatus
	    AND offr.StatusID IN ([OFFERS_STATUSID_LIST])
	    AND offr.OfferTypeID IN ([OFFERS_TYPEID_LIST])
    ) as t
    where 
      ((distance <= @Distance and @Distance <@DISTANCE_DEFAULT)  OR ((distance <= @DISTANCE_DEFAULT) and @Distance =@DISTANCE_DEFAULT))
end

if(@NumberOfRecordsToReturn>0)
	SET ROWCOUNT @NumberOfRecordsToReturn;


select 
	tOutter.*
    ,CommunicationUnl = 
        ISNULL((select COUNT(*)
				from 	dbo.EUS_UnlockedConversations unl
				where (
					( unl.FromProfileID = @CurrentProfileId AND unl.ToProfileID = tOutter.ProfileID) OR
					( unl.FromProfileID = tOutter.ProfileID  AND unl.ToProfileID = @CurrentProfileId)
				)
				AND unl.DateTimeCreated <= '2013-02-08'
        ),0)
    ,IsMessageSent = 
        ISNULL((select sum(IsMessageSent)
				from(
					(select COUNT(*) as IsMessageSent
						from 	dbo.EUS_Messages msg
						where (
								msg.FromProfileID = tOutter.ProfileID  AND msg.ToProfileID = @CurrentProfileId
						)
						and ISNULL(msg.IsHidden,0)=0)
					union	
					(select COUNT(*) as IsMessageSent
						from 	dbo.EUS_ProfilesCommunication msg
						where (
								msg.FromProfileID = tOutter.ProfileID  AND msg.ToProfileID = @CurrentProfileId
						)
						and msg.SentMessage>0
					)
				) as t11
            ),0)
    ,IsOnlineNow=CAST((CASE
                        WHEN exists(select ProfileID 
                            from [EUS_ProfilesPrivacySettings]
                            where (ProfileID=tOutter.ProfileID OR [MirrorProfileID]=tOutter.ProfileID)
                            and [PrivacySettings_ShowMeOffline]=1) THEN 0
                        WHEN not @LastActivityUTCDate is null and (tOutter.IsOnline=1 and tOutter.LastActivityDateTime>=@LastActivityUTCDate) THEN 1
                        ELSE 0
                    END) as bit)
from
(
    select 
	    RowNumber = Row_Number() over([QueryOrdering])
		,tRowNumber.* 
    from (SELECT    
		    distance =isNull(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)), 1000),
            EUS_Profiles.ProfileID, EUS_Profiles.IsMaster, 
            EUS_Profiles.MirrorProfileID, EUS_Profiles.Status, 
            EUS_Profiles.LoginName, 
            EUS_Profiles.FirstName, EUS_Profiles.LastName, 
            EUS_Profiles.GenderId, EUS_Profiles.Country, EUS_Profiles.Region, 
            EUS_Profiles.City, EUS_Profiles.Zip, EUS_Profiles.CityArea, 
            EUS_Profiles.Address, EUS_Profiles.Telephone, EUS_Profiles.eMail, 
            EUS_Profiles.Cellular, EUS_Profiles.AreYouWillingToTravel, 
            EUS_Profiles.AboutMe_Heading, 
            EUS_Profiles.AboutMe_DescribeYourself, 
            EUS_Profiles.AboutMe_DescribeAnIdealFirstDate, 
            EUS_Profiles.OtherDetails_EducationID, 
            EUS_Profiles.OtherDetails_AnnualIncomeID, 
            EUS_Profiles.OtherDetails_NetWorthID, 
            EUS_Profiles.OtherDetails_Occupation, 
            EUS_Profiles.PersonalInfo_HeightID, 
            EUS_Profiles.PersonalInfo_BodyTypeID, 
            EUS_Profiles.PersonalInfo_EyeColorID, 
            EUS_Profiles.PersonalInfo_HairColorID,
            EUS_Profiles.PersonalInfo_ChildrenID, EUS_Profiles.PersonalInfo_EthnicityID,
            EUS_Profiles.PersonalInfo_ReligionID, 
            EUS_Profiles.PersonalInfo_SmokingHabitID, 
            EUS_Profiles.PersonalInfo_DrinkingHabitID, 
            EUS_Profiles.LookingFor_ToMeetMaleID, 
            EUS_Profiles.LookingFor_ToMeetFemaleID, 
            EUS_Profiles.LookingFor_RelationshipStatusID, 
            EUS_Profiles.LookingFor_TypeOfDating_ShortTermRelationship, 
            EUS_Profiles.LookingFor_TypeOfDating_Friendship,
            EUS_Profiles.LookingFor_TypeOfDating_LongTermRelationship, 
            EUS_Profiles.LookingFor_TypeOfDating_MutuallyBeneficialArrangements, 
            EUS_Profiles.LookingFor_TypeOfDating_MarriedDating, 
            EUS_Profiles.LookingFor_TypeOfDating_AdultDating_Casual, 
            EUS_Profiles.DateTimeToRegister, 
            EUS_Profiles.RegisterIP, 
            EUS_Profiles.LastLoginDateTime, 
            EUS_Profiles.LastUpdateProfileDateTime, 
            EUS_Profiles.LAGID, 
            EUS_Profiles.Birthday, 
            EUS_Profiles.IsOnline, 
            EUS_Profiles.LastActivityDateTime, 
            phot.CustomerPhotosID, 
            phot.CustomerID, 
            phot.DateTimeToUploading, phot.FileName, 
            phot.DisplayLevel, 
            phot.HasAproved, phot.HasDeclined, 
            phot.CheckedContextID, 
            phot.IsDefault, 
	        [HasPhoto] = case 
			        when [EUS_Profiles].PhotosPublic > 0  then 1
			        when [EUS_Profiles].PhotosPrivate > 0  then 1
			        when phot.[CustomerID] IS NULL  then 0
			        else 1 
			    end,

		    offr.OfferID, 
		    offr.DateTimeToCreate as OffersDateTimeToCreate , 
		    offr.OfferTypeID as OffersOfferTypeID, 
		    offr.FromProfileID as OffersFromProfileID, 
		    offr.ToProfileID as OffersToProfileID, 
		    offr.Amount as OffersAmount, 
		    offr.StatusID as OffersStatusID
	    FROM
			    dbo.EUS_Profiles EUS_Profiles
            with(nolock)
	    INNER JOIN  dbo.EUS_Offers offr ON offr.FromProfileID = @CurrentProfileId AND offr.ToProfileID = EUS_Profiles.ProfileID and isnull(offr.FromProfileDeleted,0) = 0
   	    LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.CustomerPhotosID = EUS_Profiles.DefPhotoID
	    WHERE     
		    EUS_Profiles.IsMaster = 1
	    AND EUS_Profiles.[ProfileID] <> 1 
	    AND	EUS_Profiles.Status = @ReturnRecordsWithStatus
	    AND offr.StatusID IN ([OFFERS_STATUSID_LIST])
	    AND offr.OfferTypeID IN ([OFFERS_TYPEID_LIST])
    ) as tRowNumber
    where 
        ((distance <= @Distance and @Distance <@DISTANCE_DEFAULT)  OR ((distance <= @DISTANCE_DEFAULT) and @Distance =@DISTANCE_DEFAULT))
) as tOutter
where 
	tOutter.RowNumber > @RowNumberMin and
	tOutter.RowNumber <= @RowNumberMax 
order by tOutter.RowNumber
]]></sql>.Value



            Dim sqlOrderBy As String = ""
            If (sorting = OffersSortEnum.OfferAmountHighest) Then
                sqlOrderBy = vbCrLf & _
                    "order by OffersAmount desc"
            ElseIf (sorting = OffersSortEnum.OfferAmountLowest) Then
                sqlOrderBy = vbCrLf & _
                    "Order By OffersAmount asc"
            ElseIf (sorting = OffersSortEnum.OldestOffers) Then
                sqlOrderBy = vbCrLf & _
                    "Order By OffersDateTimeToCreate Asc"
            ElseIf (sorting = OffersSortEnum.RecentOffers) Then
                sqlOrderBy = vbCrLf & _
                    "Order By OffersDateTimeToCreate Desc"
            End If

            sql = sql.Replace("[QueryOrdering]", sqlOrderBy)


            'AND offr.StatusID IN (SELECT [EUS_OffersStatusID]  
            '					 FROM [dbo].[EUS_OffersStatus]  
            '					 WHERE [ConstantName]='PENDING' OR [ConstantName]='COUNTER')
            'AND offr.OfferTypeID IN (SELECT [EUS_OffersTypeID]
            '                           FROM [EUS_OffersTypes] 
            '					    WHERE [ConstantName]='OFFERNEW' OR [ConstantName]='OFFERCOUNTER')

            Dim OFFERS_STATUSID_LIST As String() = {ProfileHelper.OfferStatusID_PENDING,
                                                    ProfileHelper.OfferStatusID_COUNTER}

            Dim OFFERS_TYPEID_LIST As String() = {ProfileHelper.OfferTypeID_OFFERCOUNTER,
                                                  ProfileHelper.OfferTypeID_OFFERNEW}



            sql = sql.Replace("[OFFERS_STATUSID_LIST]", String.Join(",", OFFERS_STATUSID_LIST))
            sql = sql.Replace("[OFFERS_TYPEID_LIST]", String.Join(",", OFFERS_TYPEID_LIST))


            Dim command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql)
            command.Parameters.Add(New SqlClient.SqlParameter("@CurrentProfileId", CurrentProfileId))
            command.Parameters.Add(New SqlClient.SqlParameter("@ReturnRecordsWithStatus", ReturnRecordsWithStatus))
            command.Parameters.Add(New SqlClient.SqlParameter("@NumberOfRecordsToReturn", NumberOfRecordsToReturn))
            command.Parameters.Add(New SqlClient.SqlParameter("@Distance", Distance))
            command.Parameters.Add(New SqlClient.SqlParameter("@DISTANCE_DEFAULT", DISTANCE_DEFAULT))
            command.Parameters.Add(New SqlClient.SqlParameter("@performCount", parms.performCount))
            command.Parameters.Add(New SqlClient.SqlParameter("@RowNumberMin", parms.rowNumberMin))
            command.Parameters.Add(New SqlClient.SqlParameter("@RowNumberMax", parms.rowNumberMax))

            If (String.IsNullOrEmpty(zipstr)) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@zip", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@zip", zipstr))
            End If

            If (latitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", latitudeIn))
            End If
            If (longitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", longitudeIn))
            End If

            Try
                Dim mins As Integer = clsConfigValues.Get__members_online_minutes()
                Dim LastActivityUTCDate As DateTime = Date.UtcNow.AddMinutes(-mins)
                command.Parameters.Add(New SqlClient.SqlParameter("@LastActivityUTCDate", LastActivityUTCDate))
            Catch
                command.Parameters.Add(New SqlClient.SqlParameter("@LastActivityUTCDate", System.DBNull.Value))
            End Try

            dt = DataHelpers.GetDataSet(command)

        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        Finally
            clsLogger.InsertLog("GetPendingOffersDataTable", CurrentProfileId, (DateTime.UtcNow - __logdate).TotalMilliseconds)
        End Try


        Return dt
    End Function





    Public Shared Function GetPendingLikesDataTable(ByRef parms As clsWinksHelperParameters) As DataSet

        Dim __logdate As DateTime = DateTime.UtcNow

        Dim CurrentProfileId As Integer = parms.CurrentProfileId
        Dim ReturnRecordsWithStatus As Integer = parms.ReturnRecordsWithStatus
        Dim sorting As OffersSortEnum = parms.sorting
        Dim zipstr As String = parms.zipstr
        Dim latitudeIn As Double? = parms.latitudeIn
        Dim longitudeIn As Double? = parms.longitudeIn
        Dim Distance As Integer = parms.Distance
        Dim NumberOfRecordsToReturn As Integer = parms.NumberOfRecordsToReturn
        Dim performCount As Boolean = parms.performCount

        If (Distance = 0) Then
            Distance = DISTANCE_DEFAULT
        End If

        Dim sql As String = ""
        Dim dt As New DataSet()

        'This script is based on kilometer. If you use miles simply convert the radius to kilometer (radius=radius*1.609344)
        Try

            sql = <sql><![CDATA[
--fn:GetPendingLikesDataTable
	--@CurrentProfileId int
	--,@ReturnRecordsWithStatus int
	--,@NumberOfRecordsToReturn int =0
	--,@Distance int=0


if(@performCount=1)
begin

    select Count(*) from (
	    SELECT    
		    distance =isNull(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)), 1000)
	    FROM
			    dbo.EUS_Profiles EUS_Profiles
            with(nolock)
	    INNER JOIN  dbo.EUS_Offers offr ON offr.FromProfileID = @CurrentProfileId AND offr.ToProfileID = EUS_Profiles.ProfileID and isnull(offr.FromProfileDeleted,0) = 0
   	    LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.CustomerPhotosID = EUS_Profiles.DefPhotoID
	    WHERE     
		    EUS_Profiles.IsMaster = 1
	    AND EUS_Profiles.[ProfileID] <> 1 
	    AND offr.StatusID IN (SELECT [EUS_OffersStatusID]  
						     FROM [dbo].[EUS_OffersStatus]  
						     WHERE [ConstantName]='PENDING' OR [ConstantName]='COUNTER')
	    AND offr.OfferTypeID IN (SELECT [EUS_OffersTypeID]
                                FROM [EUS_OffersTypes] 
						        WHERE [ConstantName]='WINK' OR [ConstantName]='POKE')
	    AND	EUS_Profiles.Status = @ReturnRecordsWithStatus
    ) as t
    where 
      ((distance <= @Distance and @Distance <@DISTANCE_DEFAULT)  OR ((distance <= @DISTANCE_DEFAULT) and @Distance =@DISTANCE_DEFAULT))
end




	SET NOCOUNT ON;
	if(@NumberOfRecordsToReturn>0)
		SET ROWCOUNT @NumberOfRecordsToReturn;


select 
	tOutter.*
    ,CommunicationUnl = 
        ISNULL((select COUNT(*)
		        from 	dbo.EUS_UnlockedConversations unl
		        where (
			        ( unl.FromProfileID = @CurrentProfileId AND unl.ToProfileID = tOutter.ProfileID) OR
			        ( unl.FromProfileID = tOutter.ProfileID  AND unl.ToProfileID = @CurrentProfileId)
		        )
		        AND unl.DateTimeCreated <= '2013-02-08'
        ),0)
    ,IsMessageSent = 
            ISNULL((	select sum(IsMessageSent)
	            from(
		            (select COUNT(*) as IsMessageSent
			            from 	dbo.EUS_Messages msg
			            where (
					            msg.FromProfileID = tOutter.ProfileID  AND msg.ToProfileID = @CurrentProfileId
			            )
			            and ISNULL(msg.IsHidden,0)=0)
		            union	
		            (select COUNT(*) as IsMessageSent
			            from 	dbo.EUS_ProfilesCommunication msg
			            where (
					            msg.FromProfileID = tOutter.ProfileID  AND msg.ToProfileID = @CurrentProfileId
			            )
			            and msg.SentMessage>0
		            )
	            ) as t11
            ),0)
    ,IsOnlineNow=CAST((CASE
                        WHEN exists(select ProfileID 
                            from [EUS_ProfilesPrivacySettings]
                            where (ProfileID=tOutter.ProfileID OR [MirrorProfileID]=tOutter.ProfileID)
                            and [PrivacySettings_ShowMeOffline]=1) THEN 0
                        WHEN not @LastActivityUTCDate is null and (tOutter.IsOnline=1 and tOutter.LastActivityDateTime>=@LastActivityUTCDate) THEN 1
                        ELSE 0
                    END) as bit)
from
(
    select 
	    RowNumber = Row_Number() over([QueryOrdering])
		,tRowNumber.* 
    from (
	    SELECT    
		    distance =isNull(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)), 1000),
            EUS_Profiles.ProfileID, EUS_Profiles.IsMaster, EUS_Profiles.MirrorProfileID, EUS_Profiles.Status, EUS_Profiles.LoginName, 
            EUS_Profiles.FirstName, EUS_Profiles.LastName, EUS_Profiles.GenderId, EUS_Profiles.Country, EUS_Profiles.Region, 
            EUS_Profiles.City, EUS_Profiles.Zip, EUS_Profiles.CityArea, EUS_Profiles.Address, EUS_Profiles.Telephone, EUS_Profiles.eMail, 
            EUS_Profiles.Cellular, EUS_Profiles.AreYouWillingToTravel, EUS_Profiles.AboutMe_Heading, 
            EUS_Profiles.AboutMe_DescribeYourself, EUS_Profiles.AboutMe_DescribeAnIdealFirstDate, 
            EUS_Profiles.OtherDetails_EducationID, EUS_Profiles.OtherDetails_AnnualIncomeID, 
            EUS_Profiles.OtherDetails_NetWorthID, EUS_Profiles.OtherDetails_Occupation, EUS_Profiles.PersonalInfo_HeightID, 
            EUS_Profiles.PersonalInfo_BodyTypeID, EUS_Profiles.PersonalInfo_EyeColorID, 
            EUS_Profiles.PersonalInfo_HairColorID, EUS_Profiles.PersonalInfo_ChildrenID, EUS_Profiles.PersonalInfo_EthnicityID,
            EUS_Profiles.PersonalInfo_ReligionID, EUS_Profiles.PersonalInfo_SmokingHabitID, 
            EUS_Profiles.PersonalInfo_DrinkingHabitID, EUS_Profiles.LookingFor_ToMeetMaleID, 
            EUS_Profiles.LookingFor_ToMeetFemaleID, EUS_Profiles.LookingFor_RelationshipStatusID, 
            EUS_Profiles.LookingFor_TypeOfDating_ShortTermRelationship, EUS_Profiles.LookingFor_TypeOfDating_Friendship,
            EUS_Profiles.LookingFor_TypeOfDating_LongTermRelationship, 
            EUS_Profiles.LookingFor_TypeOfDating_MutuallyBeneficialArrangements, EUS_Profiles.LookingFor_TypeOfDating_MarriedDating, 
            EUS_Profiles.LookingFor_TypeOfDating_AdultDating_Casual, 
            EUS_Profiles.DateTimeToRegister, 
            EUS_Profiles.RegisterIP, 
            EUS_Profiles.LastLoginDateTime, 
            EUS_Profiles.LastUpdateProfileDateTime, 
            EUS_Profiles.LAGID, 
            EUS_Profiles.Birthday, 
            EUS_Profiles.IsOnline, 
            EUS_Profiles.LastActivityDateTime, 
            phot.CustomerPhotosID, 
            phot.CustomerID, 
            phot.DateTimeToUploading, phot.FileName, 
            phot.DisplayLevel, 
            phot.HasAproved, phot.HasDeclined, 
            phot.CheckedContextID, 
            phot.IsDefault, 
	        [HasPhoto] = case 
			        when [EUS_Profiles].PhotosPublic > 0  then 1
			        when [EUS_Profiles].PhotosPrivate > 0  then 1
			        when phot.[CustomerID] IS NULL  then 0
			        else 1 
			    end,

		    offr.OfferID, 
		    offr.DateTimeToCreate as OffersDateTimeToCreate , 
		    offr.OfferTypeID as OffersOfferTypeID, 
		    offr.FromProfileID as OffersFromProfileID, 
		    offr.ToProfileID as OffersToProfileID, 
		    offr.Amount as OffersAmount, 
		    offr.StatusID as OffersStatusID
	    FROM
			    dbo.EUS_Profiles EUS_Profiles
            with(nolock)
	    INNER JOIN  dbo.EUS_Offers offr ON offr.FromProfileID = @CurrentProfileId AND offr.ToProfileID = EUS_Profiles.ProfileID and isnull(offr.FromProfileDeleted,0) = 0
   	    LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.CustomerPhotosID = EUS_Profiles.DefPhotoID
	    WHERE     
		    EUS_Profiles.IsMaster = 1
	    AND EUS_Profiles.[ProfileID] <> 1 
	    AND offr.StatusID IN (SELECT [EUS_OffersStatusID]  
						     FROM [dbo].[EUS_OffersStatus]  
						     WHERE [ConstantName]='PENDING' OR [ConstantName]='COUNTER')
	    AND offr.OfferTypeID IN (SELECT [EUS_OffersTypeID]
                                FROM [EUS_OffersTypes] 
						        WHERE [ConstantName]='WINK' OR [ConstantName]='POKE')
	    AND	EUS_Profiles.Status = @ReturnRecordsWithStatus
    ) as tRowNumber
    where 
        ((distance <= @Distance and @Distance <@DISTANCE_DEFAULT)  OR ((distance <= @DISTANCE_DEFAULT) and @Distance =@DISTANCE_DEFAULT))
) as tOutter
where 
	tOutter.RowNumber > @RowNumberMin and
	tOutter.RowNumber <= @RowNumberMax 
order by tOutter.RowNumber
]]></sql>.Value


            Dim sqlOrderBy As String = ""
            If (sorting = OffersSortEnum.OfferAmountHighest) Then
                sqlOrderBy = vbCrLf & _
                    "order by OffersAmount desc"
            ElseIf (sorting = OffersSortEnum.OfferAmountLowest) Then
                sqlOrderBy = vbCrLf & _
                    "Order By OffersAmount asc"
            ElseIf (sorting = OffersSortEnum.OldestOffers) Then
                sqlOrderBy = vbCrLf & _
                    "Order By OffersDateTimeToCreate Asc"
            ElseIf (sorting = OffersSortEnum.RecentOffers) Then
                sqlOrderBy = vbCrLf & _
                    "Order By OffersDateTimeToCreate Desc"
            End If

            sql = sql.Replace("[QueryOrdering]", sqlOrderBy)

            Dim command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql)
            command.Parameters.Add(New SqlClient.SqlParameter("@CurrentProfileId", CurrentProfileId))
            command.Parameters.Add(New SqlClient.SqlParameter("@ReturnRecordsWithStatus", ReturnRecordsWithStatus))
            command.Parameters.Add(New SqlClient.SqlParameter("@NumberOfRecordsToReturn", NumberOfRecordsToReturn))
            command.Parameters.Add(New SqlClient.SqlParameter("@Distance", Distance))
            command.Parameters.Add(New SqlClient.SqlParameter("@DISTANCE_DEFAULT", DISTANCE_DEFAULT))
            'command.Parameters.Add(New SqlClient.SqlParameter("@zip", zipstr))
            command.Parameters.Add(New SqlClient.SqlParameter("@performCount", parms.performCount))
            command.Parameters.Add(New SqlClient.SqlParameter("@RowNumberMin", parms.rowNumberMin))
            command.Parameters.Add(New SqlClient.SqlParameter("@RowNumberMax", parms.rowNumberMax))

            If (String.IsNullOrEmpty(zipstr)) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@zip", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@zip", zipstr))
            End If
            If (latitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", latitudeIn))
            End If
            If (longitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", longitudeIn))
            End If

            Try
                Dim mins As Integer = clsConfigValues.Get__members_online_minutes()
                Dim LastActivityUTCDate As DateTime = Date.UtcNow.AddMinutes(-mins)
                command.Parameters.Add(New SqlClient.SqlParameter("@LastActivityUTCDate", LastActivityUTCDate))
            Catch
                command.Parameters.Add(New SqlClient.SqlParameter("@LastActivityUTCDate", System.DBNull.Value))
            End Try

            dt = DataHelpers.GetDataSet(command)

        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        Finally
            clsLogger.InsertLog("GetPendingLikesDataTable", CurrentProfileId, (DateTime.UtcNow - __logdate).TotalMilliseconds)
        End Try


        Return dt
    End Function



    Public Shared Function GetRejectedOffersDataTable(ByRef parms As clsWinksHelperParameters) As DataSet

        Dim CurrentProfileId As Integer = parms.CurrentProfileId
        Dim ReturnRecordsWithStatus As Integer = parms.ReturnRecordsWithStatus
        Dim sorting As DatesSortEnum = parms.sorting
        Dim zipstr As String = parms.zipstr
        Dim latitudeIn As Double? = parms.latitudeIn
        Dim longitudeIn As Double? = parms.longitudeIn
        Dim Distance As Integer = parms.Distance
        Dim NumberOfRecordsToReturn As Integer = parms.NumberOfRecordsToReturn
        Dim performCount As Boolean = parms.performCount

        Dim __logdate As DateTime = DateTime.UtcNow
        If (Distance = 0) Then
            Distance = DISTANCE_DEFAULT
        End If

        Dim sql As String = ""
        Dim dt As New DataSet()

        'This script is based on kilometer. If you use miles simply convert the radius to kilometer (radius=radius*1.609344)
        Try

            sql = <sql><![CDATA[
--fn:GetRejectedOffersDataTable
SET NOCOUNT ON;

if(@performCount=1)
begin
    select Count(*) from (
	    SELECT    
		    distance =isNull(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)), 1000),
		    OffersOfferTypeID= case	when offrFromMe.OfferID IS null then offrToMe.OfferTypeID	else offrFromMe.OfferTypeID	end, 
		    OffersStatusID= case	when offrFromMe.OfferID IS null then offrToMe.StatusID	else offrFromMe.StatusID	end
		FROM    dbo.EUS_Profiles EUS_Profiles
			with(nolock)
		LEFT OUTER JOIN	dbo.EUS_Offers offrFromMe on (offrFromMe.FromProfileID = @CurrentProfileId AND offrFromMe.ToProfileID = EUS_Profiles.ProfileID and isnull(offrFromMe.FromProfileDeleted,0) = 0 
												AND (offrFromMe.StatusID IN ([OFFERS_STATUSID_LIST1]) OR offrFromMe.StatusID IN ([OFFERS_STATUSID_LIST2])) 
												AND offrFromMe.OfferTypeID IN ([OFFERS_TYPEID_LIST]))
		LEFT OUTER JOIN	dbo.EUS_Offers offrToMe on (offrToMe.FromProfileID = EUS_Profiles.ProfileID AND offrToMe.ToProfileID =@CurrentProfileId and isnull(offrToMe.ToProfileDeleted,0) = 0 
												AND offrToMe.StatusID IN ([OFFERS_STATUSID_LIST1]) 
												AND offrToMe.OfferTypeID IN ([OFFERS_TYPEID_LIST]))
		WHERE     
			EUS_Profiles.IsMaster = 1
		AND EUS_Profiles.[ProfileID] <> 1 
		AND	EUS_Profiles.Status = @ReturnRecordsWithStatus
		AND (
			(
				(
					offrFromMe.StatusID IN ([OFFERS_STATUSID_LIST1]) 
					OR 
					offrFromMe.StatusID IN ([OFFERS_STATUSID_LIST2])
				)  
				AND offrFromMe.OfferTypeID IN ([OFFERS_TYPEID_LIST])
			)
			OR(
				offrToMe.StatusID IN ([OFFERS_STATUSID_LIST1]) 
				AND offrToMe.OfferTypeID IN ([OFFERS_TYPEID_LIST])
			)
		)
    ) as t
    where 
      ((distance <= @Distance and @Distance <@DISTANCE_DEFAULT)  OR ((distance <= @DISTANCE_DEFAULT) and @Distance =@DISTANCE_DEFAULT))
	AND (
        t.OffersStatusID IN ([OFFERS_STATUSID_LIST1])  OR 
		t.OffersStatusID IN ([OFFERS_STATUSID_LIST2])
    )
	AND t.OffersOfferTypeID IN ([OFFERS_TYPEID_LIST])
end


if(@NumberOfRecordsToReturn>0)
	SET ROWCOUNT @NumberOfRecordsToReturn;



select 
	tOutter.*
    ,CommunicationUnl = 
        ISNULL((select COUNT(*)
				from 	dbo.EUS_UnlockedConversations unl
				where (
					( unl.FromProfileID = @CurrentProfileId AND unl.ToProfileID = tOutter.ProfileID) OR
					( unl.FromProfileID = tOutter.ProfileID  AND unl.ToProfileID = @CurrentProfileId)
				)
				AND unl.DateTimeCreated <= '2013-02-08'
        ),0)
    ,IsMessageSent = 
        ISNULL((select sum(IsMessageSent)
				from(
					(select COUNT(*) as IsMessageSent
						from 	dbo.EUS_Messages msg
						where (
								msg.FromProfileID = tOutter.ProfileID  AND msg.ToProfileID = @CurrentProfileId
						)
						and ISNULL(msg.IsHidden,0)=0)
					union	
					(select COUNT(*) as IsMessageSent
						from 	dbo.EUS_ProfilesCommunication msg
						where (
								msg.FromProfileID = tOutter.ProfileID  AND msg.ToProfileID = @CurrentProfileId
						)
						and msg.SentMessage>0
					)
				) as t11
            ),0)
    ,IsOnlineNow=CAST((CASE
                        WHEN exists(select ProfileID 
                            from [EUS_ProfilesPrivacySettings]
                            where (ProfileID=tOutter.ProfileID OR [MirrorProfileID]=tOutter.ProfileID)
                            and [PrivacySettings_ShowMeOffline]=1) THEN 0
                        WHEN not @LastActivityUTCDate is null and (tOutter.IsOnline=1 and tOutter.LastActivityDateTime>=@LastActivityUTCDate) THEN 1
                        ELSE 0
                    END) as bit)
from
(
    select 
	    RowNumber = Row_Number() over([QueryOrdering])
		,tRowNumber.* 
    from (
	    SELECT    
		    distance =isNull(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)), 1000),
            EUS_Profiles.ProfileID, EUS_Profiles.IsMaster, 
            EUS_Profiles.MirrorProfileID, EUS_Profiles.Status, 
            EUS_Profiles.LoginName, 
            EUS_Profiles.FirstName, EUS_Profiles.LastName, 
            EUS_Profiles.GenderId, EUS_Profiles.Country, EUS_Profiles.Region, 
            EUS_Profiles.City, EUS_Profiles.Zip, EUS_Profiles.CityArea, 
            EUS_Profiles.Address, EUS_Profiles.Telephone, EUS_Profiles.eMail, 
            EUS_Profiles.Cellular, EUS_Profiles.AreYouWillingToTravel, 
            EUS_Profiles.AboutMe_Heading, 
            EUS_Profiles.AboutMe_DescribeYourself, 
            EUS_Profiles.AboutMe_DescribeAnIdealFirstDate, 
            EUS_Profiles.OtherDetails_EducationID, 
            EUS_Profiles.OtherDetails_AnnualIncomeID, 
            EUS_Profiles.OtherDetails_NetWorthID, 
            EUS_Profiles.OtherDetails_Occupation, 
            EUS_Profiles.PersonalInfo_HeightID, 
            EUS_Profiles.PersonalInfo_BodyTypeID, 
            EUS_Profiles.PersonalInfo_EyeColorID, 
            EUS_Profiles.PersonalInfo_HairColorID,
            EUS_Profiles.PersonalInfo_ChildrenID, EUS_Profiles.PersonalInfo_EthnicityID,
            EUS_Profiles.PersonalInfo_ReligionID, 
            EUS_Profiles.PersonalInfo_SmokingHabitID, 
            EUS_Profiles.PersonalInfo_DrinkingHabitID, 
            EUS_Profiles.LookingFor_ToMeetMaleID, 
            EUS_Profiles.LookingFor_ToMeetFemaleID, 
            EUS_Profiles.LookingFor_RelationshipStatusID, 
            EUS_Profiles.LookingFor_TypeOfDating_ShortTermRelationship, 
            EUS_Profiles.LookingFor_TypeOfDating_Friendship,
            EUS_Profiles.LookingFor_TypeOfDating_LongTermRelationship, 
            EUS_Profiles.LookingFor_TypeOfDating_MutuallyBeneficialArrangements, 
            EUS_Profiles.LookingFor_TypeOfDating_MarriedDating, 
            EUS_Profiles.LookingFor_TypeOfDating_AdultDating_Casual, 
            EUS_Profiles.DateTimeToRegister, 
            EUS_Profiles.RegisterIP, 
            EUS_Profiles.LastLoginDateTime, 
            EUS_Profiles.LastUpdateProfileDateTime, 
            EUS_Profiles.LAGID, 
            EUS_Profiles.Birthday, 
            EUS_Profiles.IsOnline, 
            EUS_Profiles.LastActivityDateTime, 
            phot.CustomerPhotosID, 
            phot.CustomerID, 
            phot.DateTimeToUploading, phot.FileName, 
            phot.DisplayLevel, 
            phot.HasAproved, phot.HasDeclined, 
            phot.CheckedContextID, 
            phot.IsDefault, 
	        [HasPhoto] = case 
			        when [EUS_Profiles].PhotosPublic > 0  then 1
			        when [EUS_Profiles].PhotosPrivate > 0  then 1
			        when phot.[CustomerID] IS NULL  then 0
			        else 1 
			    end,
		    OfferID= case when offrFromMe.OfferID IS null then offrToMe.OfferID	else offrFromMe.OfferID	end, 
		    OffersDateTimeToCreate= case	when offrFromMe.OfferID IS null then offrToMe.DateTimeToCreate	else offrFromMe.DateTimeToCreate	end, 
		    OffersOfferTypeID= case	when offrFromMe.OfferID IS null then offrToMe.OfferTypeID	else offrFromMe.OfferTypeID	end, 
		    OffersFromProfileID= case	when offrFromMe.OfferID IS null then offrToMe.FromProfileID	else offrFromMe.FromProfileID	end, 
		    OffersToProfileID= case	when offrFromMe.OfferID IS null then offrToMe.ToProfileID	else offrFromMe.ToProfileID end, 
		    OffersAmount= case	when offrFromMe.OfferID IS null then offrToMe.Amount	else offrFromMe.Amount	end, 
		    OffersStatusID= case	when offrFromMe.OfferID IS null then offrToMe.StatusID	else offrFromMe.StatusID	end,
		    OffersIsToProfileIDViewedDate= case	when offrFromMe.OfferID IS null then offrToMe.IsToProfileIDViewedDate	else offrFromMe.IsToProfileIDViewedDate	end,
		    OffersIsDate= case	when offrFromMe.OfferID IS null then offrToMe.IsDate	else offrFromMe.IsDate	end
		FROM    dbo.EUS_Profiles EUS_Profiles
			with(nolock)
		LEFT OUTER JOIN	dbo.EUS_Offers offrFromMe on (offrFromMe.FromProfileID = @CurrentProfileId AND offrFromMe.ToProfileID = EUS_Profiles.ProfileID and isnull(offrFromMe.FromProfileDeleted,0) = 0 
												AND (offrFromMe.StatusID IN ([OFFERS_STATUSID_LIST1]) OR offrFromMe.StatusID IN ([OFFERS_STATUSID_LIST2])) 
												AND offrFromMe.OfferTypeID IN ([OFFERS_TYPEID_LIST]))
		LEFT OUTER JOIN	dbo.EUS_Offers offrToMe on (offrToMe.FromProfileID = EUS_Profiles.ProfileID AND offrToMe.ToProfileID =@CurrentProfileId and isnull(offrToMe.ToProfileDeleted,0) = 0 
												AND offrToMe.StatusID IN ([OFFERS_STATUSID_LIST1]) 
												AND offrToMe.OfferTypeID IN ([OFFERS_TYPEID_LIST]))
		LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.CustomerPhotosID = EUS_Profiles.DefPhotoID
		WHERE     
			EUS_Profiles.IsMaster = 1
		AND EUS_Profiles.[ProfileID] <> 1 
		AND	EUS_Profiles.Status = @ReturnRecordsWithStatus
		AND (
			(
				(
					offrFromMe.StatusID IN ([OFFERS_STATUSID_LIST1]) 
					OR 
					offrFromMe.StatusID IN ([OFFERS_STATUSID_LIST2])
				)  
				AND offrFromMe.OfferTypeID IN ([OFFERS_TYPEID_LIST])
			)
			OR(
				offrToMe.StatusID IN ([OFFERS_STATUSID_LIST1]) 
				AND offrToMe.OfferTypeID IN ([OFFERS_TYPEID_LIST])
			)
		)
    ) as tRowNumber
    where 
        ((distance <= @Distance and @Distance <@DISTANCE_DEFAULT)  OR ((distance <= @DISTANCE_DEFAULT) and @Distance =@DISTANCE_DEFAULT))
	AND (
        tRowNumber.OffersStatusID IN ([OFFERS_STATUSID_LIST1])  OR 
		tRowNumber.OffersStatusID IN ([OFFERS_STATUSID_LIST2])
    )
	AND tRowNumber.OffersOfferTypeID IN ([OFFERS_TYPEID_LIST])
) as tOutter
where 
	tOutter.RowNumber > @RowNumberMin and
	tOutter.RowNumber <= @RowNumberMax 
order by tOutter.RowNumber
]]></sql>.Value
            Dim sqlOrderBy As String = ""
            If (sorting = OffersSortEnum.OfferAmountHighest) Then
                sqlOrderBy = vbCrLf & _
                    "order by OffersAmount desc"
            ElseIf (sorting = OffersSortEnum.OfferAmountLowest) Then
                sqlOrderBy = vbCrLf & _
                    "Order By OffersAmount asc"
            ElseIf (sorting = OffersSortEnum.OldestOffers) Then
                sqlOrderBy = vbCrLf & _
                    "Order By OffersDateTimeToCreate Asc"
            ElseIf (sorting = OffersSortEnum.RecentOffers) Then
                sqlOrderBy = vbCrLf & _
                    "Order By OffersDateTimeToCreate Desc"
            End If

            sql = sql.Replace("[QueryOrdering]", sqlOrderBy)


            'AND (
            '	(offr.StatusID IN (SELECT [EUS_OffersStatusID]  
            '						FROM [dbo].[EUS_OffersStatus]  
            '						WHERE [ConstantName] like 'REJECT%' )
            '	AND (offr.FromProfileID = @CurrentProfileId AND offr.ToProfileID = EUS_Profiles.ProfileID 
            '			OR offr.FromProfileID = EUS_Profiles.ProfileID AND offr.ToProfileID =@CurrentProfileId)
            '	) OR (
            '	offr.StatusID IN (SELECT [EUS_OffersStatusID]  
            '							FROM [dbo].[EUS_OffersStatus]  
            '							WHERE [ConstantName] like 'CANCELED' )
            '		AND offr.FromProfileID = @CurrentProfileId AND offr.ToProfileID = EUS_Profiles.ProfileID
            '	))
            'AND offr.OfferTypeID IN (SELECT [EUS_OffersTypeID]  
            '							FROM [dbo].[EUS_OffersTypes]  
            '							WHERE [ConstantName]='OFFERCOUNTER' OR  [ConstantName]='OFFERNEW')

            Dim OFFERS_STATUSID_LIST1 As String() = {ProfileHelper.OfferStatusID_REJECTED,
                                                    ProfileHelper.OfferStatusID_REJECTBAD,
                                                    ProfileHelper.OfferStatusID_REJECTEXPECTATIONS,
                                                    ProfileHelper.OfferStatusID_REJECTFAR,
                                                    ProfileHelper.OfferStatusID_REJECTTYPE}

            Dim OFFERS_STATUSID_LIST2 As String() = {ProfileHelper.OfferStatusID_CANCELED}


            Dim OFFERS_TYPEID_LIST As String() = {ProfileHelper.OfferTypeID_OFFERCOUNTER,
                                                  ProfileHelper.OfferTypeID_OFFERNEW}

            sql = sql.Replace("[OFFERS_STATUSID_LIST1]", String.Join(",", OFFERS_STATUSID_LIST1))
            sql = sql.Replace("[OFFERS_STATUSID_LIST2]", String.Join(",", OFFERS_STATUSID_LIST2))
            sql = sql.Replace("[OFFERS_TYPEID_LIST]", String.Join(",", OFFERS_TYPEID_LIST))


            Dim command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql)
            command.Parameters.Add(New SqlClient.SqlParameter("@CurrentProfileId", CurrentProfileId))
            command.Parameters.Add(New SqlClient.SqlParameter("@ReturnRecordsWithStatus", ReturnRecordsWithStatus))
            command.Parameters.Add(New SqlClient.SqlParameter("@NumberOfRecordsToReturn", NumberOfRecordsToReturn))
            command.Parameters.Add(New SqlClient.SqlParameter("@Distance", Distance))
            command.Parameters.Add(New SqlClient.SqlParameter("@DISTANCE_DEFAULT", DISTANCE_DEFAULT))
            command.Parameters.Add(New SqlClient.SqlParameter("@performCount", parms.performCount))
            command.Parameters.Add(New SqlClient.SqlParameter("@RowNumberMin", parms.rowNumberMin))
            command.Parameters.Add(New SqlClient.SqlParameter("@RowNumberMax", parms.rowNumberMax))

            If (String.IsNullOrEmpty(zipstr)) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@zip", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@zip", zipstr))
            End If
            If (latitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", latitudeIn))
            End If
            If (longitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", longitudeIn))
            End If

            Try
                Dim mins As Integer = clsConfigValues.Get__members_online_minutes()
                Dim LastActivityUTCDate As DateTime = Date.UtcNow.AddMinutes(-mins)
                command.Parameters.Add(New SqlClient.SqlParameter("@LastActivityUTCDate", LastActivityUTCDate))
            Catch
                command.Parameters.Add(New SqlClient.SqlParameter("@LastActivityUTCDate", System.DBNull.Value))
            End Try


            dt = DataHelpers.GetDataSet(command)

        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        Finally
            clsLogger.InsertLog("GetRejectedOffersDataTable", CurrentProfileId, (DateTime.UtcNow - __logdate).TotalMilliseconds)
        End Try

        Return dt
    End Function



    Public Shared Function GetRejectedLikesDataTable(ByRef parms As clsWinksHelperParameters) As DataSet

        Dim __logdate As DateTime = DateTime.UtcNow

        Dim CurrentProfileId As Integer = parms.CurrentProfileId
        Dim ReturnRecordsWithStatus As Integer = parms.ReturnRecordsWithStatus
        Dim sorting As OffersSortEnum = parms.sorting
        Dim zipstr As String = parms.zipstr
        Dim latitudeIn As Double? = parms.latitudeIn
        Dim longitudeIn As Double? = parms.longitudeIn
        Dim Distance As Integer = parms.Distance
        Dim NumberOfRecordsToReturn As Integer = parms.NumberOfRecordsToReturn
        Dim performCount As Boolean = parms.performCount

        If (Distance = 0) Then
            Distance = DISTANCE_DEFAULT
        End If

        Dim sql As String = ""
        Dim dt As New DataSet()

        'This script is based on kilometer. If you use miles simply convert the radius to kilometer (radius=radius*1.609344)
        Try

            sql = <sql><![CDATA[
--fn:GetRejectedLikesDataTable

if(@performCount=1)
begin

    select Count(distinct t.LoginName) from (
	    SELECT    
			EUS_Profiles.loginname,  
		    distance =isNull(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)), 1000),
		    OffersOfferTypeID= case 
						when offr.OfferID IS null then offr1.OfferTypeID
					else offr.OfferTypeID 
					end, 
		    OffersStatusID= case 
						when offr.OfferID IS null then offr1.StatusID
					else offr.StatusID 
					end
	    FROM
			    dbo.EUS_Profiles EUS_Profiles
            with(nolock)
	    LEFT OUTER JOIN	dbo.EUS_Offers offr on (offr.FromProfileID = @CurrentProfileId AND offr.ToProfileID = EUS_Profiles.ProfileID and isnull(offr.FromProfileDeleted,0) = 0)
	    LEFT OUTER JOIN	dbo.EUS_Offers offr1 on (offr1.FromProfileID = EUS_Profiles.ProfileID AND offr1.ToProfileID =@CurrentProfileId and isnull(offr1.ToProfileDeleted,0) = 0)
	    WHERE     
		    EUS_Profiles.IsMaster = 1
	    AND EUS_Profiles.[ProfileID]<>1 
	    AND	EUS_Profiles.Status = @ReturnRecordsWithStatus
	    AND (offr.StatusID IN ([OFFERS_STATUSID_LIST]) OR  offr1.StatusID IN ([OFFERS_STATUSID_LIST]))
	    AND (offr.OfferTypeID IN ([OFFERS_TYPEID_LIST]) OR offr1.OfferTypeID IN ([OFFERS_TYPEID_LIST]))
    ) as t
    where 
      ((distance <= @Distance and @Distance <@DISTANCE_DEFAULT)  OR ((distance <= @DISTANCE_DEFAULT) and @Distance =@DISTANCE_DEFAULT))
	AND t.OffersStatusID IN ([OFFERS_STATUSID_LIST])
	AND t.OffersOfferTypeID IN ([OFFERS_TYPEID_LIST])
end


	SET NOCOUNT ON;
	if(@NumberOfRecordsToReturn>0)
		SET ROWCOUNT @NumberOfRecordsToReturn;




select 
	tOutter.*
    ,CommunicationUnl = 
        ISNULL((select COUNT(*)
		        from 	dbo.EUS_UnlockedConversations unl
		        where (
			        ( unl.FromProfileID = @CurrentProfileId AND unl.ToProfileID = tOutter.ProfileID) OR
			        ( unl.FromProfileID = tOutter.ProfileID  AND unl.ToProfileID = @CurrentProfileId)
		        )
		        AND unl.DateTimeCreated <= '2013-02-08'
        ),0)
    ,IsMessageSent = 
        ISNULL((	select sum(IsMessageSent)
	        from(
		        (select COUNT(*) as IsMessageSent
			        from 	dbo.EUS_Messages msg
			        where (
					        msg.FromProfileID = tOutter.ProfileID  AND msg.ToProfileID = @CurrentProfileId
			        )
			        and ISNULL(msg.IsHidden,0)=0)
		        union	
		        (select COUNT(*) as IsMessageSent
			        from 	dbo.EUS_ProfilesCommunication msg
			        where (
					        msg.FromProfileID = tOutter.ProfileID  AND msg.ToProfileID = @CurrentProfileId
			        )
			        and msg.SentMessage>0
		        )
	        ) as t11
        ),0)
    ,IsOnlineNow=CAST((CASE
                        WHEN exists(select ProfileID 
                            from [EUS_ProfilesPrivacySettings]
                            where (ProfileID=tOutter.ProfileID OR [MirrorProfileID]=tOutter.ProfileID)
                            and [PrivacySettings_ShowMeOffline]=1) THEN 0
                        WHEN not @LastActivityUTCDate is null and (tOutter.IsOnline=1 and tOutter.LastActivityDateTime>=@LastActivityUTCDate) THEN 1
                        ELSE 0
                    END) as bit)
from
(
    select 
	    RowNumber = Row_Number() over([QueryOrdering])
		,tRowNumber.* 
    from (
	    SELECT	distinct 
		    distance =isNull(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)), 1000),
            EUS_Profiles.ProfileID, EUS_Profiles.IsMaster, EUS_Profiles.MirrorProfileID, EUS_Profiles.Status, EUS_Profiles.LoginName, 
            EUS_Profiles.FirstName, EUS_Profiles.LastName, EUS_Profiles.GenderId, EUS_Profiles.Country, EUS_Profiles.Region, 
            EUS_Profiles.City, EUS_Profiles.Zip, EUS_Profiles.CityArea, EUS_Profiles.Address, EUS_Profiles.Telephone, EUS_Profiles.eMail, 
            EUS_Profiles.Cellular, EUS_Profiles.AreYouWillingToTravel, EUS_Profiles.AboutMe_Heading, 
            EUS_Profiles.AboutMe_DescribeYourself, EUS_Profiles.AboutMe_DescribeAnIdealFirstDate, 
            EUS_Profiles.OtherDetails_EducationID, EUS_Profiles.OtherDetails_AnnualIncomeID, 
            EUS_Profiles.OtherDetails_NetWorthID, EUS_Profiles.OtherDetails_Occupation, EUS_Profiles.PersonalInfo_HeightID, 
            EUS_Profiles.PersonalInfo_BodyTypeID, EUS_Profiles.PersonalInfo_EyeColorID, 
            EUS_Profiles.PersonalInfo_HairColorID, EUS_Profiles.PersonalInfo_ChildrenID, EUS_Profiles.PersonalInfo_EthnicityID,
            EUS_Profiles.PersonalInfo_ReligionID, EUS_Profiles.PersonalInfo_SmokingHabitID, 
            EUS_Profiles.PersonalInfo_DrinkingHabitID, EUS_Profiles.LookingFor_ToMeetMaleID, 
            EUS_Profiles.LookingFor_ToMeetFemaleID, EUS_Profiles.LookingFor_RelationshipStatusID, 
            EUS_Profiles.LookingFor_TypeOfDating_ShortTermRelationship, EUS_Profiles.LookingFor_TypeOfDating_Friendship,
            EUS_Profiles.LookingFor_TypeOfDating_LongTermRelationship, 
            EUS_Profiles.LookingFor_TypeOfDating_MutuallyBeneficialArrangements, EUS_Profiles.LookingFor_TypeOfDating_MarriedDating, 
            EUS_Profiles.LookingFor_TypeOfDating_AdultDating_Casual, 
            EUS_Profiles.DateTimeToRegister, 
            EUS_Profiles.RegisterIP, 
            EUS_Profiles.LastLoginDateTime, 
            EUS_Profiles.LastUpdateProfileDateTime, 
            EUS_Profiles.LAGID, 
            EUS_Profiles.Birthday, 
            EUS_Profiles.IsOnline, 
            EUS_Profiles.LastActivityDateTime, 
            phot.CustomerPhotosID, 
            phot.CustomerID, 
            phot.DateTimeToUploading, phot.FileName, 
            phot.DisplayLevel, 
            phot.HasAproved, phot.HasDeclined, 
            phot.CheckedContextID, 
            phot.IsDefault, 
	        [HasPhoto] = case 
			        when [EUS_Profiles].PhotosPublic > 0  then 1
			        when [EUS_Profiles].PhotosPrivate > 0  then 1
			        when phot.[CustomerID] IS NULL  then 0
			        else 1 
			    end,
		    OfferID= case 
						when offr.OfferID IS null then offr1.OfferID
					else offr.OfferID
					end, 
		    OffersDateTimeToCreate= case 
						when offr.OfferID IS null then offr1.DateTimeToCreate
					else offr.DateTimeToCreate 
					end, 
		    OffersOfferTypeID= case 
						when offr.OfferID IS null then offr1.OfferTypeID
					else offr.OfferTypeID 
					end, 
		    OffersFromProfileID= case 
						when offr.OfferID IS null then offr1.FromProfileID
					else offr.FromProfileID 
					end, 
		    OffersToProfileID= case 
						when offr.OfferID IS null then offr1.ToProfileID
					else offr.ToProfileID 
					end, 
		    OffersAmount= case 
						when offr.OfferID IS null then offr1.Amount
					else offr.Amount
					end, 
		    OffersStatusID= case 
						when offr.OfferID IS null then offr1.StatusID
					else offr.StatusID 
					end

	    FROM
			    dbo.EUS_Profiles EUS_Profiles
            with(nolock)
	    LEFT OUTER JOIN	dbo.EUS_Offers offr on (offr.FromProfileID = @CurrentProfileId AND offr.ToProfileID = EUS_Profiles.ProfileID and isnull(offr.FromProfileDeleted,0) = 0)
	    LEFT OUTER JOIN	dbo.EUS_Offers offr1 on (offr1.FromProfileID = EUS_Profiles.ProfileID AND offr1.ToProfileID =@CurrentProfileId and isnull(offr1.ToProfileDeleted,0) = 0)
   	    LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.CustomerPhotosID = EUS_Profiles.DefPhotoID
	    WHERE     
		    EUS_Profiles.IsMaster = 1
	    AND EUS_Profiles.[ProfileID]<>1 
	    AND	EUS_Profiles.Status = @ReturnRecordsWithStatus
	    AND (offr.StatusID IN ([OFFERS_STATUSID_LIST]) OR  offr1.StatusID IN ([OFFERS_STATUSID_LIST]))
	    AND (offr.OfferTypeID IN ([OFFERS_TYPEID_LIST]) OR offr1.OfferTypeID IN ([OFFERS_TYPEID_LIST]))
    ) as tRowNumber
    where 
        ((distance<=@Distance and @Distance<@DISTANCE_DEFAULT)  OR ((distance<=@DISTANCE_DEFAULT) and @Distance=@DISTANCE_DEFAULT))
	AND tRowNumber.OffersStatusID IN ([OFFERS_STATUSID_LIST])
	AND tRowNumber.OffersOfferTypeID IN ([OFFERS_TYPEID_LIST])
) as tOutter
where 
	tOutter.RowNumber>@RowNumberMin and
	tOutter.RowNumber<=@RowNumberMax 
order by tOutter.RowNumber

]]></sql>.Value

            Dim sqlOrderBy As String = ""
            If (sorting = OffersSortEnum.OfferAmountHighest) Then
                sqlOrderBy = vbCrLf & _
                    "order by OffersAmount desc"
            ElseIf (sorting = OffersSortEnum.OfferAmountLowest) Then
                sqlOrderBy = vbCrLf & _
                    "Order By OffersAmount asc"
            ElseIf (sorting = OffersSortEnum.OldestOffers) Then
                sqlOrderBy = vbCrLf & _
                    "Order By OffersDateTimeToCreate Asc"
            ElseIf (sorting = OffersSortEnum.RecentOffers) Then
                sqlOrderBy = vbCrLf & _
                    "Order By OffersDateTimeToCreate Desc"
            End If

            sql = sql.Replace("[QueryOrdering]", sqlOrderBy)



            Dim OFFERS_STATUSID_LIST As String() = {ProfileHelper.OfferStatusID_CANCELED,
                                                  ProfileHelper.OfferStatusID_REJECTBAD,
                                                  ProfileHelper.OfferStatusID_REJECTED,
                                                  ProfileHelper.OfferStatusID_REJECTEXPECTATIONS,
                                                  ProfileHelper.OfferStatusID_REJECTFAR,
                                                  ProfileHelper.OfferStatusID_REJECTTYPE}

            Dim OFFERS_TYPEID_LIST As String() = {ProfileHelper.OfferTypeID_WINK,
                                                  ProfileHelper.OfferTypeID_POKE}


            sql = sql.Replace("[OFFERS_STATUSID_LIST]", String.Join(",", OFFERS_STATUSID_LIST))
            sql = sql.Replace("[OFFERS_TYPEID_LIST]", String.Join(",", OFFERS_TYPEID_LIST))


            Dim command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql)
            command.Parameters.Add(New SqlClient.SqlParameter("@CurrentProfileId", CurrentProfileId))
            command.Parameters.Add(New SqlClient.SqlParameter("@ReturnRecordsWithStatus", ReturnRecordsWithStatus))
            command.Parameters.Add(New SqlClient.SqlParameter("@NumberOfRecordsToReturn", NumberOfRecordsToReturn))
            command.Parameters.Add(New SqlClient.SqlParameter("@Distance", Distance))
            command.Parameters.Add(New SqlClient.SqlParameter("@DISTANCE_DEFAULT", DISTANCE_DEFAULT))
            'command.Parameters.Add(New SqlClient.SqlParameter("@zip", zipstr))
            command.Parameters.Add(New SqlClient.SqlParameter("@performCount", parms.performCount))
            command.Parameters.Add(New SqlClient.SqlParameter("@RowNumberMin", parms.rowNumberMin))
            command.Parameters.Add(New SqlClient.SqlParameter("@RowNumberMax", parms.rowNumberMax))

            If (String.IsNullOrEmpty(zipstr)) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@zip", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@zip", zipstr))
            End If
            If (latitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", latitudeIn))
            End If
            If (longitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", longitudeIn))
            End If

            Try
                Dim mins As Integer = clsConfigValues.Get__members_online_minutes()
                Dim LastActivityUTCDate As DateTime = Date.UtcNow.AddMinutes(-mins)
                command.Parameters.Add(New SqlClient.SqlParameter("@LastActivityUTCDate", LastActivityUTCDate))
            Catch
                command.Parameters.Add(New SqlClient.SqlParameter("@LastActivityUTCDate", System.DBNull.Value))
            End Try

            dt = DataHelpers.GetDataSet(command)

        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        Finally
            clsLogger.InsertLog("GetRejectedLikesDataTable", CurrentProfileId, (DateTime.UtcNow - __logdate).TotalMilliseconds)
        End Try


        Return dt
    End Function


    Public Shared Function GetNewDatesDataTable(ByRef parms As clsDatesHelperParameters) As DataSet

        Dim CurrentProfileId As Integer = parms.CurrentProfileId
        Dim ReturnRecordsWithStatus As Integer = parms.ReturnRecordsWithStatus
        Dim sorting As DatesSortEnum = parms.sorting
        Dim zipstr As String = parms.zipstr
        Dim latitudeIn As Double? = parms.latitudeIn
        Dim longitudeIn As Double? = parms.longitudeIn
        Dim Distance As Integer = parms.Distance
        Dim NumberOfRecordsToReturn As Integer = parms.NumberOfRecordsToReturn
        Dim performCount As Boolean = parms.performCount


        Dim __logdate As DateTime = DateTime.UtcNow
        If (Distance = 0) Then
            Distance = DISTANCE_DEFAULT
        End If

        Dim sql As String = ""
        Dim dt As New DataSet()

        'This script is based on kilometer. If you use miles simply convert the radius to kilometer (radius=radius*1.609344)
        Try

            sql = <sql><![CDATA[
--fn:GetNewDatesDataTable
	--@CurrentProfileId int
	--,@ReturnRecordsWithStatus int
	--,@NumberOfRecordsToReturn int =0
	--,@Distance int=0
	
declare @MirrorProfileID int 
select @MirrorProfileID=MirrorProfileID from EUS_Profiles where ProfileID=@CurrentProfileId

declare @EUS_OffersStatusID_UNLOCKED int,
	@EUS_OffersTypeID_NEWDATE int

SELECT @EUS_OffersStatusID_UNLOCKED = [EUS_OffersStatusID]  
FROM [dbo].[EUS_OffersStatus]  
with (nolock)    
WHERE [ConstantName]='UNLOCKED'

SELECT @EUS_OffersTypeID_NEWDATE=[EUS_OffersTypeID]  
FROM [dbo].[EUS_OffersTypes]  
with (nolock)    
WHERE  [ConstantName]='NEWDATE'


if(@performCount=1)
begin

    select Count(*) from (
	    SELECT    
		    distance =isNull(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)), 1000)
		FROM
				dbo.EUS_Profiles EUS_Profiles 
						with (nolock)    
	    LEFT OUTER JOIN	dbo.EUS_Offers offr on (offr.FromProfileID = @CurrentProfileId AND 
												offr.ToProfileID = EUS_Profiles.ProfileID and 
												isnull(offr.FromProfileDeleted,0) = 0 AND 
												offr.StatusID=@EUS_OffersStatusID_UNLOCKED	AND 
												offr.OfferTypeID=@EUS_OffersTypeID_NEWDATE)
	    LEFT OUTER JOIN	dbo.EUS_Offers offr1 on (offr1.FromProfileID = EUS_Profiles.ProfileID AND 
												offr1.ToProfileID =@CurrentProfileId and 
												isnull(offr1.ToProfileDeleted,0) = 0 AND 
												offr1.StatusID=@EUS_OffersStatusID_UNLOCKED	AND 
												offr1.OfferTypeID=@EUS_OffersTypeID_NEWDATE)
		WHERE     
			EUS_Profiles.IsMaster = 1
		AND EUS_Profiles.Profileid <> 1
		AND EUS_Profiles.Profileid <> @CurrentProfileId
		AND EUS_Profiles.Profileid <> @MirrorProfileID
  		AND	EUS_Profiles.Status=@ReturnRecordsWithStatus
  		AND(
  			(
				(
					offr.[IsDate]=1
					OR(
						offr.StatusID=@EUS_OffersStatusID_UNLOCKED
						AND offr.OfferTypeID=@EUS_OffersTypeID_NEWDATE
					)
				)
				and isnull(offr.FromProfileDeleted,0) = 0
				and isnull(offr.ToProfileDeleted,0) = 0
			)
			OR 
  			(
				(
					offr1.[IsDate]=1
					OR(
						offr1.StatusID=@EUS_OffersStatusID_UNLOCKED
						AND offr1.OfferTypeID=@EUS_OffersTypeID_NEWDATE
					)
				)
				and isnull(offr1.FromProfileDeleted,0) = 0
				and isnull(offr1.ToProfileDeleted,0) = 0
			)
		)
		AND  not exists(select * 
						from EUS_ProfilesBlocked bl 
						with (nolock)    
						where (bl.FromProfileID=EUS_Profiles.ProfileID and bl.ToProfileID = @CurrentProfileId) or
							(bl.FromProfileID=@CurrentProfileId and bl.ToProfileID = EUS_Profiles.ProfileID)
		)
        [AdditionalWhereClause]
    ) as EUS_Profiles
	where 
	  ((distance <= @Distance and @Distance <@DISTANCE_DEFAULT)  OR ((distance <= @DISTANCE_DEFAULT) and @Distance =@DISTANCE_DEFAULT))

end


	SET NOCOUNT ON;
	if(@NumberOfRecordsToReturn>0)
		SET ROWCOUNT @NumberOfRecordsToReturn;



select 
	tOutter.*
    ,IsOnlineNow=CAST((CASE
                        WHEN exists(select ProfileID 
                            from [EUS_ProfilesPrivacySettings]
                            where (ProfileID=tOutter.ProfileID OR [MirrorProfileID]=tOutter.ProfileID)
                            and [PrivacySettings_ShowMeOffline]=1) THEN 0
                        WHEN not @LastActivityUTCDate is null and (tOutter.IsOnline=1 and tOutter.LastActivityDateTime>=@LastActivityUTCDate) THEN 1
                        ELSE 0
                    END) as bit)
from
(
    select 
	    RowNumber = Row_Number() over([QueryOrdering])
		,tRowNumber.* 
    from (
	    SELECT    
		    distance =isNull(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)), 1000),
            EUS_Profiles.ProfileID, EUS_Profiles.IsMaster, 
            EUS_Profiles.MirrorProfileID, EUS_Profiles.Status, 
            EUS_Profiles.LoginName, 
            EUS_Profiles.FirstName, EUS_Profiles.LastName, 
            EUS_Profiles.GenderId, EUS_Profiles.Country, EUS_Profiles.Region, 
            EUS_Profiles.City, EUS_Profiles.Zip, EUS_Profiles.CityArea, 
            EUS_Profiles.Address, EUS_Profiles.Telephone, EUS_Profiles.eMail, 
            EUS_Profiles.Cellular, EUS_Profiles.AreYouWillingToTravel, 
            EUS_Profiles.AboutMe_Heading, 
            EUS_Profiles.AboutMe_DescribeYourself, 
            EUS_Profiles.AboutMe_DescribeAnIdealFirstDate, 
            EUS_Profiles.OtherDetails_EducationID, 
            EUS_Profiles.OtherDetails_AnnualIncomeID, 
            EUS_Profiles.OtherDetails_NetWorthID, 
            EUS_Profiles.OtherDetails_Occupation, 
            EUS_Profiles.PersonalInfo_HeightID, 
            EUS_Profiles.PersonalInfo_BodyTypeID, 
            EUS_Profiles.PersonalInfo_EyeColorID, 
            EUS_Profiles.PersonalInfo_HairColorID,
            EUS_Profiles.PersonalInfo_ChildrenID, EUS_Profiles.PersonalInfo_EthnicityID,
            EUS_Profiles.PersonalInfo_ReligionID, 
            EUS_Profiles.PersonalInfo_SmokingHabitID, 
            EUS_Profiles.PersonalInfo_DrinkingHabitID, 
            EUS_Profiles.LookingFor_ToMeetMaleID, 
            EUS_Profiles.LookingFor_ToMeetFemaleID, 
            EUS_Profiles.LookingFor_RelationshipStatusID, 
            EUS_Profiles.LookingFor_TypeOfDating_ShortTermRelationship, 
            EUS_Profiles.LookingFor_TypeOfDating_Friendship,
            EUS_Profiles.LookingFor_TypeOfDating_LongTermRelationship, 
            EUS_Profiles.LookingFor_TypeOfDating_MutuallyBeneficialArrangements, 
            EUS_Profiles.LookingFor_TypeOfDating_MarriedDating, 
            EUS_Profiles.LookingFor_TypeOfDating_AdultDating_Casual, 
            EUS_Profiles.DateTimeToRegister, 
            EUS_Profiles.RegisterIP, 
            EUS_Profiles.LastLoginDateTime, 
            EUS_Profiles.LastUpdateProfileDateTime, 
            EUS_Profiles.LAGID, 
            EUS_Profiles.Birthday, 
            EUS_Profiles.IsOnline, 
            EUS_Profiles.LastActivityDateTime, 
            phot.CustomerPhotosID, 
            phot.CustomerID, 
            phot.DateTimeToUploading, phot.FileName, 
            phot.DisplayLevel, 
            phot.HasAproved, phot.HasDeclined, 
            phot.CheckedContextID, 
            phot.IsDefault, 
	        [HasPhoto] = case 
			        when [EUS_Profiles].PhotosPublic > 0  then 1
			        when [EUS_Profiles].PhotosPrivate > 0  then 1
			        when phot.[CustomerID] IS NULL  then 0
			        else 1 
			    end,

		    OfferID= case when offr.OfferID IS null then offr1.OfferID	else offr.OfferID	end, 
		    OffersDateTimeToCreate= case	when offr.OfferID IS null then offr1.DateTimeToCreate	else offr.DateTimeToCreate	end, 
		    OffersOfferTypeID= case	when offr.OfferID IS null then offr1.OfferTypeID	else offr.OfferTypeID	end, 
		    OffersFromProfileID= case	when offr.OfferID IS null then offr1.FromProfileID	else offr.FromProfileID	end, 
		    OffersToProfileID= case	when offr.OfferID IS null then offr1.ToProfileID	else offr.ToProfileID end, 
		    OffersAmount= case	when offr.OfferID IS null then offr1.Amount	else offr.Amount	end, 
		    OffersStatusID= case	when offr.OfferID IS null then offr1.StatusID	else offr.StatusID	end,
		    OffersIsToProfileIDViewedDate= case	when offr.OfferID IS null then offr1.IsToProfileIDViewedDate	else offr.IsToProfileIDViewedDate	end,
		    OffersIsDate= case	when offr.OfferID IS null then offr1.IsDate	else offr.IsDate	end,

            CommunicationUnl = ISNULL((select COUNT(*)
                                from 	dbo.EUS_UnlockedConversations unl
                                with (nolock)    
                                where   ( unl.FromProfileID = @CurrentProfileId AND unl.ToProfileID = EUS_Profiles.ProfileID) OR
		                                ( unl.FromProfileID = EUS_Profiles.ProfileID  AND unl.ToProfileID = @CurrentProfileId)
                                ),0),

            LastMessageDate = (select top(1) DateTimeToCreate 
                                from 	dbo.EUS_Messages msgLast
                                with (nolock)    
                                where   (
                                            ( msgLast.FromProfileID = @CurrentProfileId AND msgLast.ToProfileID = EUS_Profiles.ProfileID) OR
		                                    ( msgLast.FromProfileID = EUS_Profiles.ProfileID  AND msgLast.ToProfileID = @CurrentProfileId)
                                        )
                                        and ISNULL(msgLast.IsHidden,0)=0
		                        order by  DateTimeToCreate desc )
	    FROM
			    dbo.EUS_Profiles EUS_Profiles 
                        with (nolock)    
	    LEFT OUTER JOIN	dbo.EUS_Offers offr on (offr.FromProfileID = @CurrentProfileId AND 
												offr.ToProfileID = EUS_Profiles.ProfileID and 
												isnull(offr.FromProfileDeleted,0) = 0 AND 
												offr.StatusID=@EUS_OffersStatusID_UNLOCKED	AND 
												offr.OfferTypeID=@EUS_OffersTypeID_NEWDATE)
	    LEFT OUTER JOIN	dbo.EUS_Offers offr1 on (offr1.FromProfileID = EUS_Profiles.ProfileID AND 
												offr1.ToProfileID =@CurrentProfileId and 
												isnull(offr1.ToProfileDeleted,0) = 0 AND 
												offr1.StatusID=@EUS_OffersStatusID_UNLOCKED	AND 
												offr1.OfferTypeID=@EUS_OffersTypeID_NEWDATE)
   	    LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.CustomerPhotosID = EUS_Profiles.DefPhotoID
		WHERE     
			EUS_Profiles.IsMaster = 1
		AND EUS_Profiles.Profileid <> 1
		AND EUS_Profiles.Profileid <> @CurrentProfileId
		AND EUS_Profiles.Profileid <> @MirrorProfileID
  		AND	EUS_Profiles.Status=@ReturnRecordsWithStatus
  		AND(
  			(
				(
					offr.[IsDate]=1
					OR(
						offr.StatusID=@EUS_OffersStatusID_UNLOCKED
						AND offr.OfferTypeID=@EUS_OffersTypeID_NEWDATE
					)
				)
				and isnull(offr.FromProfileDeleted,0) = 0
				and isnull(offr.ToProfileDeleted,0) = 0
			)
			OR 
  			(
				(
					offr1.[IsDate]=1
					OR(
						offr1.StatusID=@EUS_OffersStatusID_UNLOCKED
						AND offr1.OfferTypeID=@EUS_OffersTypeID_NEWDATE
					)
				)
				and isnull(offr1.FromProfileDeleted,0) = 0
				and isnull(offr1.ToProfileDeleted,0) = 0
			)
		)
		AND  not exists(select * 
						from EUS_ProfilesBlocked bl 
						with (nolock)    
						where (bl.FromProfileID=EUS_Profiles.ProfileID and bl.ToProfileID = @CurrentProfileId) or
							(bl.FromProfileID=@CurrentProfileId and bl.ToProfileID = EUS_Profiles.ProfileID)
		)
        [AdditionalWhereClause]
    ) as tRowNumber
    where 
        ((distance <= @Distance and @Distance <@DISTANCE_DEFAULT)  OR ((distance <= @DISTANCE_DEFAULT) and @Distance =@DISTANCE_DEFAULT))
) as tOutter
where 
	tOutter.RowNumber > @RowNumberMin and
	tOutter.RowNumber <= @RowNumberMax 
order by tOutter.RowNumber
]]></sql>.Value

            Dim sqlOrderBy As String = ""
            If (sorting = DatesSortEnum.NewDates) Then
                sqlOrderBy = vbCrLf & _
                    "order by OffersIsToProfileIDViewedDate asc, OffersDateTimeToCreate Desc"

            ElseIf (sorting = DatesSortEnum.ByStatus) Then
                sqlOrderBy = vbCrLf & _
                    "Order By CommunicationUnl desc"

            ElseIf (sorting = DatesSortEnum.ByLastDating) Then
                sqlOrderBy = vbCrLf & _
                    "Order By LastMessageDate Desc"

            Else
                sqlOrderBy = vbCrLf & _
                    "Order By OffersDateTimeToCreate Desc"

            End If

            sql = sql.Replace("[QueryOrdering]", sqlOrderBy)

            If (parms.OtherProfileId > 0) Then
                sql = sql.Replace("[AdditionalWhereClause]", "AND EUS_Profiles.ProfileId=@OtherProfileId")
            ElseIf (Not String.IsNullOrEmpty(parms.AdditionalWhereClause)) Then
                sql = sql.Replace("[AdditionalWhereClause]", vbCrLf & parms.AdditionalWhereClause)
            Else
                sql = sql.Replace("[AdditionalWhereClause]", "")
            End If


            Dim command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql)
            command.Parameters.Add(New SqlClient.SqlParameter("@CurrentProfileId", CurrentProfileId))
            command.Parameters.Add(New SqlClient.SqlParameter("@ReturnRecordsWithStatus", ReturnRecordsWithStatus))
            command.Parameters.Add(New SqlClient.SqlParameter("@NumberOfRecordsToReturn", NumberOfRecordsToReturn))
            command.Parameters.Add(New SqlClient.SqlParameter("@Distance", Distance))
            command.Parameters.Add(New SqlClient.SqlParameter("@DISTANCE_DEFAULT", DISTANCE_DEFAULT))
            'command.Parameters.Add(New SqlClient.SqlParameter("@zip", zipstr))
            command.Parameters.Add(New SqlClient.SqlParameter("@performCount", performCount))
            command.Parameters.Add(New SqlClient.SqlParameter("@RowNumberMin", parms.rowNumberMin))
            command.Parameters.Add(New SqlClient.SqlParameter("@RowNumberMax", parms.rowNumberMax))
            command.Parameters.Add(New SqlClient.SqlParameter("@OtherProfileId", parms.OtherProfileId))

            If (String.IsNullOrEmpty(zipstr)) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@zip", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@zip", zipstr))
            End If
            If (latitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", latitudeIn))
            End If
            If (longitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", longitudeIn))
            End If

            Try
                Dim mins As Integer = clsConfigValues.Get__members_online_minutes()
                Dim LastActivityUTCDate As DateTime = Date.UtcNow.AddMinutes(-mins)
                command.Parameters.Add(New SqlClient.SqlParameter("@LastActivityUTCDate", LastActivityUTCDate))
            Catch
                command.Parameters.Add(New SqlClient.SqlParameter("@LastActivityUTCDate", System.DBNull.Value))
            End Try

            dt = DataHelpers.GetDataSet(command)

        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        Finally
            clsLogger.InsertLog("GetNewDatesDataTable", CurrentProfileId, (DateTime.UtcNow - __logdate).TotalMilliseconds)
        End Try


        Return dt
    End Function



    '    Public Shared Function GetNewDatesDataTable(CurrentProfileId As Integer,
    '                                                OtherProfileId As Integer,
    '                                                ReturnRecordsWithStatus As Integer,
    '                                                sorting As DatesSortEnum,
    '                                                zipstr As String,
    '                                                latitudeIn As Double?,
    '                                                longitudeIn As Double?,
    '                                                Optional Distance As Integer = DISTANCE_DEFAULT,
    '                                                Optional NumberOfRecordsToReturn As Integer = 0) As DataTable

    '        Dim __logdate As DateTime = DateTime.UtcNow
    '        If (Distance = 0) Then
    '            Distance = DISTANCE_DEFAULT
    '        End If

    '        Dim sql As String = ""
    '        Dim dt As New DataTable()

    '        'This script is based on kilometer. If you use miles simply convert the radius to kilometer (radius=radius*1.609344)
    '        Try

    '            sql = <sql><![CDATA[
    '--fn:GetNewDatesDataTable
    '	--@CurrentProfileId int
    '	--,@ReturnRecordsWithStatus int
    '	--,@NumberOfRecordsToReturn int =0
    '	--,@Distance int=0


    'declare @MirrorProfileID int 
    'select @MirrorProfileID=MirrorProfileID from EUS_Profiles where ProfileID=@CurrentProfileId


    'declare @EUS_OffersStatusID_UNLOCKED int,
    '	@EUS_OffersTypeID_NEWDATE int

    'SELECT @EUS_OffersStatusID_UNLOCKED = [EUS_OffersStatusID]  
    'FROM [dbo].[EUS_OffersStatus]  
    'with (nolock)    
    'WHERE [ConstantName]='UNLOCKED'

    'SELECT @EUS_OffersTypeID_NEWDATE=[EUS_OffersTypeID]  
    'FROM [dbo].[EUS_OffersTypes]  
    'with (nolock)    
    'WHERE  [ConstantName]='NEWDATE'

    '	SET NOCOUNT ON;
    '	if(@NumberOfRecordsToReturn>0)
    '		SET ROWCOUNT @NumberOfRecordsToReturn;




    'select * from (
    '	SELECT    
    '		distance =isNull(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)), 1000),
    '        EUS_Profiles.ProfileID, EUS_Profiles.IsMaster, EUS_Profiles.MirrorProfileID, 
    '        EUS_Profiles.Status, EUS_Profiles.LoginName, 
    '        EUS_Profiles.FirstName, EUS_Profiles.LastName, EUS_Profiles.GenderId, 
    '        EUS_Profiles.Country, EUS_Profiles.Region, EUS_Profiles.City, 
    '        EUS_Profiles.Zip, EUS_Profiles.CityArea, EUS_Profiles.Address, 
    '        EUS_Profiles.Telephone, EUS_Profiles.eMail, EUS_Profiles.Cellular, 
    '        EUS_Profiles.AreYouWillingToTravel, EUS_Profiles.AboutMe_Heading, 
    '        EUS_Profiles.AboutMe_DescribeYourself, EUS_Profiles.AboutMe_DescribeAnIdealFirstDate, 
    '        EUS_Profiles.OtherDetails_EducationID, EUS_Profiles.OtherDetails_AnnualIncomeID, 
    '        EUS_Profiles.OtherDetails_NetWorthID, EUS_Profiles.OtherDetails_Occupation, 
    '        EUS_Profiles.PersonalInfo_HeightID, EUS_Profiles.PersonalInfo_BodyTypeID, 
    '        EUS_Profiles.PersonalInfo_EyeColorID, EUS_Profiles.PersonalInfo_HairColorID, 
    '        EUS_Profiles.PersonalInfo_ChildrenID, EUS_Profiles.PersonalInfo_EthnicityID,
    '        EUS_Profiles.PersonalInfo_ReligionID, EUS_Profiles.PersonalInfo_SmokingHabitID, 
    '        EUS_Profiles.PersonalInfo_DrinkingHabitID, EUS_Profiles.LookingFor_ToMeetMaleID, 
    '        EUS_Profiles.LookingFor_ToMeetFemaleID, EUS_Profiles.LookingFor_RelationshipStatusID, 
    '        EUS_Profiles.LookingFor_TypeOfDating_ShortTermRelationship, EUS_Profiles.LookingFor_TypeOfDating_Friendship,
    '        EUS_Profiles.LookingFor_TypeOfDating_LongTermRelationship, 
    '        EUS_Profiles.LookingFor_TypeOfDating_MutuallyBeneficialArrangements, 
    '        EUS_Profiles.LookingFor_TypeOfDating_MarriedDating, EUS_Profiles.LookingFor_TypeOfDating_AdultDating_Casual, 
    '        EUS_Profiles.DateTimeToRegister, 
    '        EUS_Profiles.RegisterIP, 
    '        EUS_Profiles.LastLoginDateTime, 
    '        EUS_Profiles.LastUpdateProfileDateTime, 
    '        EUS_Profiles.LAGID, 
    '        EUS_Profiles.Birthday, 
    '        EUS_Profiles.IsOnline, 
    '        EUS_Profiles.LastActivityDateTime, 
    '        phot.CustomerPhotosID, 
    '        phot.CustomerID, 
    '        phot.DateTimeToUploading, phot.FileName, 
    '        phot.DisplayLevel, 
    '        phot.HasAproved, phot.HasDeclined, 
    '        phot.CheckedContextID, 
    '        phot.IsDefault, 
    '	    [HasPhoto] = case 
    '			    when [EUS_Profiles].PhotosPublic > 0  then 1
    '			    when [EUS_Profiles].PhotosPrivate > 0  then 1
    '			    when phot.[CustomerID] IS NULL  then 0
    '			    else 1 
    '			end,

    '        offr.OfferID, 
    '        offr.DateTimeToCreate as OffersDateTimeToCreate , 
    '        offr.OfferTypeID as OffersOfferTypeID, 
    '        offr.FromProfileID as OffersFromProfileID, 
    '        offr.ToProfileID as OffersToProfileID, 
    '        offr.Amount as OffersAmount, 
    '        offr.StatusID as OffersStatusID,

    '        CommunicationUnl = ISNULL((select COUNT(*)
    '                            from 	dbo.EUS_UnlockedConversations unl
    '                            where   ( unl.FromProfileID = @CurrentProfileId AND unl.ToProfileID = EUS_Profiles.ProfileID) OR
    '		                            ( unl.FromProfileID = EUS_Profiles.ProfileID  AND unl.ToProfileID = @CurrentProfileId)
    '                            ),0),
    '        LastMessageDate = (select top(1) DateTimeToCreate 
    '                            from 	dbo.EUS_Messages msgLast
    '                            where   (
    '                                        ( msgLast.FromProfileID = @CurrentProfileId AND msgLast.ToProfileID = EUS_Profiles.ProfileID) OR
    '		                                ( msgLast.FromProfileID = EUS_Profiles.ProfileID  AND msgLast.ToProfileID = @CurrentProfileId)
    '                                    )
    '                                    and ISNULL(msgLast.IsHidden,0)=0
    '		                    order by  DateTimeToCreate desc )
    '	FROM
    '			dbo.EUS_Profiles EUS_Profiles 
    '	LEFT OUTER JOIN		
    '			dbo.EUS_Offers offr on (offr.FromProfileID = @CurrentProfileId AND offr.ToProfileID = EUS_Profiles.ProfileID and isnull(offr.FromProfileDeleted,0) = 0)
    '				                OR (offr.FromProfileID = EUS_Profiles.ProfileID AND offr.ToProfileID =@CurrentProfileId and isnull(offr.ToProfileDeleted,0) = 0)
    '   	LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.CustomerPhotosID = EUS_Profiles.DefPhotoID
    '	WHERE     
    '		EUS_Profiles.IsMaster = 1
    '	AND EUS_Profiles.Profileid <> 1
    '	AND EUS_Profiles.Profileid <> @CurrentProfileId
    '	AND EUS_Profiles.Profileid <> @MirrorProfileID
    '	AND (
    '        offr.[IsDate]=1
    '        OR(
    '		    offr.StatusID=@EUS_OffersStatusID_UNLOCKED
    '		    AND offr.OfferTypeID=@EUS_OffersTypeID_NEWDATE
    '        )
    '    )
    '	AND	EUS_Profiles.Status = @ReturnRecordsWithStatus
    '    AND EUS_Profiles.ProfileId = @OtherProfileId
    ') as t
    'where 
    '  ((distance <= @Distance and @Distance <@DISTANCE_DEFAULT)  OR ((distance <= @DISTANCE_DEFAULT) and @Distance =@DISTANCE_DEFAULT))
    ']]></sql>.Value


    '            Dim command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql)
    '            command.Parameters.Add(New SqlClient.SqlParameter("@CurrentProfileId", CurrentProfileId))
    '            command.Parameters.Add(New SqlClient.SqlParameter("@OtherProfileId", OtherProfileId))
    '            command.Parameters.Add(New SqlClient.SqlParameter("@ReturnRecordsWithStatus", ReturnRecordsWithStatus))
    '            command.Parameters.Add(New SqlClient.SqlParameter("@NumberOfRecordsToReturn", NumberOfRecordsToReturn))
    '            command.Parameters.Add(New SqlClient.SqlParameter("@Distance", Distance))
    '            command.Parameters.Add(New SqlClient.SqlParameter("@DISTANCE_DEFAULT", DISTANCE_DEFAULT))
    '            If (String.IsNullOrEmpty(zipstr)) Then
    '                command.Parameters.Add(New SqlClient.SqlParameter("@zip", System.DBNull.Value))
    '            Else
    '                command.Parameters.Add(New SqlClient.SqlParameter("@zip", zipstr))
    '            End If
    '            If (latitudeIn Is Nothing) Then
    '                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", System.DBNull.Value))
    '            Else
    '                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", latitudeIn))
    '            End If
    '            If (longitudeIn Is Nothing) Then
    '                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", System.DBNull.Value))
    '            Else
    '                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", longitudeIn))
    '            End If


    '            dt = DataHelpers.GetDataTable(command)

    '        Catch ex As Exception
    '            Throw New Exception(ex.Message, ex)
    '        Finally
    '            clsLogger.InsertLog("GetNewDatesDataTable", CurrentProfileId, (DateTime.UtcNow - __logdate).TotalMilliseconds)
    '        End Try

    '        Return dt
    '    End Function


    Public Shared Function GetAcceptedOffersDataTable(ByRef parms As clsWinksHelperParameters) As DataSet

        Dim CurrentProfileId As Integer = parms.CurrentProfileId
        Dim ReturnRecordsWithStatus As Integer = parms.ReturnRecordsWithStatus
        Dim sorting As OffersSortEnum = parms.sorting
        Dim zipstr As String = parms.zipstr
        Dim latitudeIn As Double? = parms.latitudeIn
        Dim longitudeIn As Double? = parms.longitudeIn
        Dim Distance As Integer = parms.Distance
        Dim NumberOfRecordsToReturn As Integer = parms.NumberOfRecordsToReturn

        Dim __logdate As DateTime = DateTime.UtcNow
        If (Distance = 0) Then
            Distance = DISTANCE_DEFAULT
        End If

        Dim sql As String = ""
        Dim dt As New DataSet()

        'This script is based on kilometer. If you use miles simply convert the radius to kilometer (radius=radius*1.609344)
        Try

            sql = <sql><![CDATA[
--fn:GetAcceptedOffersDataTable
SET NOCOUNT ON;

if(@performCount=1)
begin

    select Count(*) from (
	    SELECT    
		    distance =isNull(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)), 1000),
		    OffersOfferTypeID= case	when offr.OfferID IS null then offr1.OfferTypeID	else offr.OfferTypeID	end, 
		    OffersStatusID= case	when offr.OfferID IS null then offr1.StatusID	else offr.StatusID	end
	    FROM    dbo.EUS_Profiles EUS_Profiles
	    LEFT OUTER JOIN	dbo.EUS_Offers offr on (offr.FromProfileID = @CurrentProfileId AND offr.ToProfileID = EUS_Profiles.ProfileID and isnull(offr.FromProfileDeleted,0) = 0 and offr.StatusID IN ([OFFERS_STATUSID_LIST]) and offr.OfferTypeID IN ([OFFERS_TYPEID_LIST]))
	    LEFT OUTER JOIN	dbo.EUS_Offers offr1 on (offr1.FromProfileID = EUS_Profiles.ProfileID AND offr1.ToProfileID =@CurrentProfileId and isnull(offr1.ToProfileDeleted,0) = 0 and offr1.StatusID IN ([OFFERS_STATUSID_LIST]) and offr1.OfferTypeID IN ([OFFERS_TYPEID_LIST]))
	    WHERE     
		    EUS_Profiles.IsMaster = 1
	    AND EUS_Profiles.Profileid <> 1
	    AND	EUS_Profiles.Status = @ReturnRecordsWithStatus
	    AND (offr.StatusID IN ([OFFERS_STATUSID_LIST]) OR  offr1.StatusID IN ([OFFERS_STATUSID_LIST]))
	    AND (offr.OfferTypeID IN ([OFFERS_TYPEID_LIST]) OR offr1.OfferTypeID IN ([OFFERS_TYPEID_LIST]))
    ) as t
    where 
      ((distance <= @Distance and @Distance <@DISTANCE_DEFAULT)  OR ((distance <= @DISTANCE_DEFAULT) and @Distance =@DISTANCE_DEFAULT))
	AND t.OffersStatusID IN ([OFFERS_STATUSID_LIST])
	AND t.OffersOfferTypeID IN ([OFFERS_TYPEID_LIST])
end


if(@NumberOfRecordsToReturn>0)
	SET ROWCOUNT @NumberOfRecordsToReturn;


select 
	tOutter.*
    ,CommunicationUnl = 
        ISNULL((select COUNT(*)
				from 	dbo.EUS_UnlockedConversations unl
				where (
					( unl.FromProfileID = @CurrentProfileId AND unl.ToProfileID = tOutter.ProfileID) OR
					( unl.FromProfileID = tOutter.ProfileID  AND unl.ToProfileID = @CurrentProfileId)
				)
				AND unl.DateTimeCreated <= '2013-02-08'
        ),0)
    ,IsMessageSent = 
        ISNULL((select sum(IsMessageSent)
		        from(
			        (select COUNT(*) as IsMessageSent
				        from 	dbo.EUS_Messages msg
				        where (
						        msg.FromProfileID = @CurrentProfileId AND msg.ToProfileID = tOutter.ProfileID
				        )
				        and ISNULL(msg.IsHidden,0)=0)
			        union	
			        (select COUNT(*) as IsMessageSent
				        from 	dbo.EUS_ProfilesCommunication msg
				        where (
						        msg.FromProfileID = @CurrentProfileId AND msg.ToProfileID = tOutter.ProfileID
				        )
				        and msg.SentMessage>0
			        )
		        ) as t11
            ),0)
    ,IsOnlineNow=CAST((CASE
                        WHEN exists(select ProfileID 
                            from [EUS_ProfilesPrivacySettings]
                            where (ProfileID=tOutter.ProfileID OR [MirrorProfileID]=tOutter.ProfileID)
                            and [PrivacySettings_ShowMeOffline]=1) THEN 0
                        WHEN not @LastActivityUTCDate is null and (tOutter.IsOnline=1 and tOutter.LastActivityDateTime>=@LastActivityUTCDate) THEN 1
                        ELSE 0
                    END) as bit)
from
(
    select 
	    RowNumber = Row_Number() over([QueryOrdering])
		,tRowNumber.* 
    from (
	    SELECT    
		    distance =isNull(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)), 1000),
            EUS_Profiles.ProfileID, EUS_Profiles.IsMaster, 
            EUS_Profiles.MirrorProfileID, EUS_Profiles.Status, 
            EUS_Profiles.LoginName, 
            EUS_Profiles.FirstName, EUS_Profiles.LastName, 
            EUS_Profiles.GenderId, EUS_Profiles.Country, EUS_Profiles.Region, 
            EUS_Profiles.City, EUS_Profiles.Zip, EUS_Profiles.CityArea, 
            EUS_Profiles.Address, EUS_Profiles.Telephone, EUS_Profiles.eMail, 
            EUS_Profiles.Cellular, EUS_Profiles.AreYouWillingToTravel, 
            EUS_Profiles.AboutMe_Heading, 
            EUS_Profiles.AboutMe_DescribeYourself, 
            EUS_Profiles.AboutMe_DescribeAnIdealFirstDate, 
            EUS_Profiles.OtherDetails_EducationID, 
            EUS_Profiles.OtherDetails_AnnualIncomeID, 
            EUS_Profiles.OtherDetails_NetWorthID, 
            EUS_Profiles.OtherDetails_Occupation, 
            EUS_Profiles.PersonalInfo_HeightID, 
            EUS_Profiles.PersonalInfo_BodyTypeID, 
            EUS_Profiles.PersonalInfo_EyeColorID, 
            EUS_Profiles.PersonalInfo_HairColorID,
            EUS_Profiles.PersonalInfo_ChildrenID, EUS_Profiles.PersonalInfo_EthnicityID,
            EUS_Profiles.PersonalInfo_ReligionID, 
            EUS_Profiles.PersonalInfo_SmokingHabitID, 
            EUS_Profiles.PersonalInfo_DrinkingHabitID, 
            EUS_Profiles.LookingFor_ToMeetMaleID, 
            EUS_Profiles.LookingFor_ToMeetFemaleID, 
            EUS_Profiles.LookingFor_RelationshipStatusID, 
            EUS_Profiles.LookingFor_TypeOfDating_ShortTermRelationship, 
            EUS_Profiles.LookingFor_TypeOfDating_Friendship,
            EUS_Profiles.LookingFor_TypeOfDating_LongTermRelationship, 
            EUS_Profiles.LookingFor_TypeOfDating_MutuallyBeneficialArrangements, 
            EUS_Profiles.LookingFor_TypeOfDating_MarriedDating, 
            EUS_Profiles.LookingFor_TypeOfDating_AdultDating_Casual, 
            EUS_Profiles.DateTimeToRegister, 
            EUS_Profiles.RegisterIP, 
            EUS_Profiles.LastLoginDateTime, 
            EUS_Profiles.LastUpdateProfileDateTime, 
            EUS_Profiles.LAGID, 
            EUS_Profiles.Birthday, 
            EUS_Profiles.IsOnline, 
            EUS_Profiles.LastActivityDateTime, 
            phot.CustomerPhotosID, 
            phot.CustomerID, 
            phot.DateTimeToUploading, phot.FileName, 
            phot.DisplayLevel, 
            phot.HasAproved, phot.HasDeclined, 
            phot.CheckedContextID, 
            phot.IsDefault,
	        [HasPhoto] = case 
			        when [EUS_Profiles].PhotosPublic > 0  then 1
			        when [EUS_Profiles].PhotosPrivate > 0  then 1
			        when phot.[CustomerID] IS NULL  then 0
			        else 1 
			    end,

		    OfferID= case when offr.OfferID IS null then offr1.OfferID	else offr.OfferID	end, 
		    OffersDateTimeToCreate= case	when offr.OfferID IS null then offr1.DateTimeToCreate	else offr.DateTimeToCreate	end, 
		    OffersOfferTypeID= case	when offr.OfferID IS null then offr1.OfferTypeID	else offr.OfferTypeID	end, 
		    OffersFromProfileID= case	when offr.OfferID IS null then offr1.FromProfileID	else offr.FromProfileID	end, 
		    OffersToProfileID= case	when offr.OfferID IS null then offr1.ToProfileID	else offr.ToProfileID end, 
		    OffersAmount= case	when offr.OfferID IS null then offr1.Amount	else offr.Amount	end, 
		    OffersStatusID= case	when offr.OfferID IS null then offr1.StatusID	else offr.StatusID	end,
		    OffersIsToProfileIDViewedDate= case	when offr.OfferID IS null then offr1.IsToProfileIDViewedDate	else offr.IsToProfileIDViewedDate	end,
		    OffersIsDate= case	when offr.OfferID IS null then offr1.IsDate	else offr.IsDate	end

	    FROM    dbo.EUS_Profiles EUS_Profiles
	    LEFT OUTER JOIN	dbo.EUS_Offers offr on (offr.FromProfileID = @CurrentProfileId AND offr.ToProfileID = EUS_Profiles.ProfileID and isnull(offr.FromProfileDeleted,0) = 0 and offr.StatusID IN ([OFFERS_STATUSID_LIST]) and offr.OfferTypeID IN ([OFFERS_TYPEID_LIST]))
	    LEFT OUTER JOIN	dbo.EUS_Offers offr1 on (offr1.FromProfileID = EUS_Profiles.ProfileID AND offr1.ToProfileID =@CurrentProfileId and isnull(offr1.ToProfileDeleted,0) = 0 and offr1.StatusID IN ([OFFERS_STATUSID_LIST]) and offr1.OfferTypeID IN ([OFFERS_TYPEID_LIST]))
   	    LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.CustomerPhotosID = EUS_Profiles.DefPhotoID
	    WHERE     
		    EUS_Profiles.IsMaster = 1
	    AND EUS_Profiles.Profileid <> 1
	    AND	EUS_Profiles.Status = @ReturnRecordsWithStatus
	    AND (offr.StatusID IN ([OFFERS_STATUSID_LIST]) OR  offr1.StatusID IN ([OFFERS_STATUSID_LIST]))
	    AND (offr.OfferTypeID IN ([OFFERS_TYPEID_LIST]) OR offr1.OfferTypeID IN ([OFFERS_TYPEID_LIST]))
    ) as tRowNumber
    where 
        ((distance <= @Distance and @Distance <@DISTANCE_DEFAULT)  OR ((distance <= @DISTANCE_DEFAULT) and @Distance =@DISTANCE_DEFAULT))
	AND tRowNumber.OffersStatusID IN ([OFFERS_STATUSID_LIST])
	AND tRowNumber.OffersOfferTypeID IN ([OFFERS_TYPEID_LIST])
) as tOutter
where 
	tOutter.RowNumber > @RowNumberMin and
	tOutter.RowNumber <= @RowNumberMax 
order by tOutter.RowNumber
]]></sql>.Value

            Dim sqlOrderBy As String = ""
            If (sorting = OffersSortEnum.OfferAmountHighest) Then
                sqlOrderBy = vbCrLf & _
                    "order by OffersAmount desc"
            ElseIf (sorting = OffersSortEnum.OfferAmountLowest) Then
                sqlOrderBy = vbCrLf & _
                    "Order By OffersAmount asc"
            ElseIf (sorting = OffersSortEnum.OldestOffers) Then
                sqlOrderBy = vbCrLf & _
                    "Order By OffersDateTimeToCreate Asc"
            ElseIf (sorting = OffersSortEnum.RecentOffers) Then
                sqlOrderBy = vbCrLf & _
                    "Order By OffersDateTimeToCreate Desc"
            End If

            sql = sql.Replace("[QueryOrdering]", sqlOrderBy)


            'AND offr.StatusID IN (SELECT [EUS_OffersStatusID]  
            '       FROM[dbo].[EUS_OffersStatus]
            '	     WHERE [ConstantName]='ACCEPTED' OR  [ConstantName]='OFFER_ACCEEPTED_WITH_MESSAGE')
            'AND offr.OfferTypeID IN (SELECT [EUS_OffersTypeID]  
            '       FROM[dbo].[EUS_OffersTypes]
            '		     WHERE  [ConstantName]='OFFERCOUNTER' OR  [ConstantName]='OFFERNEW')

            Dim OFFERS_STATUSID_LIST As String() = {ProfileHelper.OfferStatusID_ACCEPTED,
                                                    ProfileHelper.OfferStatusID_OFFER_ACCEEPTED_WITH_MESSAGE}

            Dim OFFERS_TYPEID_LIST As String() = {ProfileHelper.OfferTypeID_OFFERCOUNTER,
                                                  ProfileHelper.OfferTypeID_OFFERNEW}



            sql = sql.Replace("[OFFERS_STATUSID_LIST]", String.Join(",", OFFERS_STATUSID_LIST))
            sql = sql.Replace("[OFFERS_TYPEID_LIST]", String.Join(",", OFFERS_TYPEID_LIST))


            Dim command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql)
            command.Parameters.Add(New SqlClient.SqlParameter("@CurrentProfileId", CurrentProfileId))
            command.Parameters.Add(New SqlClient.SqlParameter("@ReturnRecordsWithStatus", ReturnRecordsWithStatus))
            command.Parameters.Add(New SqlClient.SqlParameter("@NumberOfRecordsToReturn", NumberOfRecordsToReturn))
            command.Parameters.Add(New SqlClient.SqlParameter("@Distance", Distance))
            command.Parameters.Add(New SqlClient.SqlParameter("@DISTANCE_DEFAULT", DISTANCE_DEFAULT))
            command.Parameters.Add(New SqlClient.SqlParameter("@performCount", parms.performCount))
            command.Parameters.Add(New SqlClient.SqlParameter("@RowNumberMin", parms.rowNumberMin))
            command.Parameters.Add(New SqlClient.SqlParameter("@RowNumberMax", parms.rowNumberMax))

            If (String.IsNullOrEmpty(zipstr)) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@zip", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@zip", zipstr))
            End If
            If (latitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", latitudeIn))
            End If
            If (longitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", longitudeIn))
            End If

            Try
                Dim mins As Integer = clsConfigValues.Get__members_online_minutes()
                Dim LastActivityUTCDate As DateTime = Date.UtcNow.AddMinutes(-mins)
                command.Parameters.Add(New SqlClient.SqlParameter("@LastActivityUTCDate", LastActivityUTCDate))
            Catch
                command.Parameters.Add(New SqlClient.SqlParameter("@LastActivityUTCDate", System.DBNull.Value))
            End Try

            dt = DataHelpers.GetDataSet(command)

        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        Finally
            clsLogger.InsertLog("GetAcceptedOffersDataTable", CurrentProfileId, (DateTime.UtcNow - __logdate).TotalMilliseconds)
        End Try

        Return dt
    End Function



    Public Shared Function GetAcceptedLikesDataTable(ByRef parms As clsWinksHelperParameters) As DataSet

        Dim __logdate As DateTime = DateTime.UtcNow
        Dim CurrentProfileId As Integer = parms.CurrentProfileId
        Dim ReturnRecordsWithStatus As Integer = parms.ReturnRecordsWithStatus
        Dim sorting As OffersSortEnum = parms.sorting
        Dim zipstr As String = parms.zipstr
        Dim latitudeIn As Double? = parms.latitudeIn
        Dim longitudeIn As Double? = parms.longitudeIn
        Dim Distance As Integer = parms.Distance
        Dim NumberOfRecordsToReturn As Integer = parms.NumberOfRecordsToReturn
        Dim performCount As Boolean = parms.performCount


        If (Distance = 0) Then
            Distance = DISTANCE_DEFAULT
        End If

        Dim sql As String = ""
        Dim dt As New DataSet()

        'This script is based on kilometer. If you use miles simply convert the radius to kilometer (radius=radius*1.609344)
        Try

            sql = <sql><![CDATA[
--fn:GetAcceptedLikesDataTable
	--@CurrentProfileId int
	--,@ReturnRecordsWithStatus int
	--,@NumberOfRecordsToReturn int =0
	--,@Distance int=0




if(@performCount=1)
begin

    select Count(distinct t.LoginName) from (
	    SELECT    
			EUS_Profiles.loginname,  
		    distance =isNull(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)), 1000),
		    OffersOfferTypeID= case 
						when offr.OfferID IS null then offr1.OfferTypeID
					else offr.OfferTypeID 
					end, 
		    OffersStatusID= case 
						when offr.OfferID IS null then offr1.StatusID
					else offr.StatusID 
					end
	    FROM
			    dbo.EUS_Profiles EUS_Profiles
	    LEFT OUTER JOIN	dbo.EUS_Offers offr on (offr.FromProfileID = @CurrentProfileId AND offr.ToProfileID = EUS_Profiles.ProfileID and isnull(offr.FromProfileDeleted,0) = 0 AND offr.StatusID IN ([OFFERS_STATUSID_LIST]) AND offr.OfferTypeID IN ([OFFERS_TYPEID_LIST]))
	    LEFT OUTER JOIN	dbo.EUS_Offers offr1 on (offr1.FromProfileID = EUS_Profiles.ProfileID AND offr1.ToProfileID =@CurrentProfileId and isnull(offr1.ToProfileDeleted,0) = 0 AND offr1.StatusID IN ([OFFERS_STATUSID_LIST]) AND offr1.OfferTypeID IN ([OFFERS_TYPEID_LIST]))
	    WHERE     
		    EUS_Profiles.IsMaster = 1
	    AND EUS_Profiles.Profileid <> 1
	    AND	EUS_Profiles.Status = @ReturnRecordsWithStatus
	    AND (offr.StatusID IN ([OFFERS_STATUSID_LIST]) OR  offr1.StatusID IN ([OFFERS_STATUSID_LIST]))
	    AND (offr.OfferTypeID IN ([OFFERS_TYPEID_LIST]) OR offr1.OfferTypeID IN ([OFFERS_TYPEID_LIST]))
    ) as t
    where 
      ((distance <= @Distance and @Distance <@DISTANCE_DEFAULT)  OR ((distance <= @DISTANCE_DEFAULT) and @Distance =@DISTANCE_DEFAULT))
	AND t.OffersStatusID IN ([OFFERS_STATUSID_LIST])
	AND t.OffersOfferTypeID IN ([OFFERS_TYPEID_LIST])
end


	SET NOCOUNT ON;
	if(@NumberOfRecordsToReturn>0)
		SET ROWCOUNT @NumberOfRecordsToReturn;



select 
	tOutter.*
    ,CommunicationUnl = 
        ISNULL((select COUNT(*)
				from 	dbo.EUS_UnlockedConversations unl
				where (
					( unl.FromProfileID = @CurrentProfileId AND unl.ToProfileID = tOutter.ProfileID) OR
					( unl.FromProfileID = tOutter.ProfileID  AND unl.ToProfileID = @CurrentProfileId)
				)
				AND unl.DateTimeCreated <= '2013-02-08'
        ),0)
    ,IsMessageSent = 
        ISNULL((select sum(IsMessageSent)
	        from(
		        (select COUNT(*) as IsMessageSent
			        from 	dbo.EUS_Messages msg
			        where (
					        msg.FromProfileID = tOutter.ProfileID  AND msg.ToProfileID = @CurrentProfileId
			        )
			        and ISNULL(msg.IsHidden,0)=0)
		        union	
		        (select COUNT(*) as IsMessageSent
			        from 	dbo.EUS_ProfilesCommunication msg
			        where (
					        msg.FromProfileID = tOutter.ProfileID  AND msg.ToProfileID = @CurrentProfileId
			        )
			        and msg.SentMessage>0
		        )
	        ) as t11
        ),0)
    ,IsOnlineNow=CAST((CASE
                        WHEN exists(select ProfileID 
                            from [EUS_ProfilesPrivacySettings]
                            where (ProfileID=tOutter.ProfileID OR [MirrorProfileID]=tOutter.ProfileID)
                            and [PrivacySettings_ShowMeOffline]=1) THEN 0
                        WHEN not @LastActivityUTCDate is null and (tOutter.IsOnline=1 and tOutter.LastActivityDateTime>=@LastActivityUTCDate) THEN 1
                        ELSE 0
                    END) as bit)
from
(
    select 
	    RowNumber = Row_Number() over([QueryOrdering])
		,tRowNumber.* 
    from (
	    SELECT    distinct
		    distance =isNull(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)), 1000),
            EUS_Profiles.ProfileID, EUS_Profiles.IsMaster, EUS_Profiles.MirrorProfileID, EUS_Profiles.Status, EUS_Profiles.LoginName, 
            EUS_Profiles.FirstName, EUS_Profiles.LastName, EUS_Profiles.GenderId, EUS_Profiles.Country, EUS_Profiles.Region, 
            EUS_Profiles.City, EUS_Profiles.Zip, EUS_Profiles.CityArea, EUS_Profiles.Address, EUS_Profiles.Telephone, EUS_Profiles.eMail, 
            EUS_Profiles.Cellular, EUS_Profiles.AreYouWillingToTravel, EUS_Profiles.AboutMe_Heading, 
            EUS_Profiles.AboutMe_DescribeYourself, EUS_Profiles.AboutMe_DescribeAnIdealFirstDate, 
            EUS_Profiles.OtherDetails_EducationID, EUS_Profiles.OtherDetails_AnnualIncomeID, 
            EUS_Profiles.OtherDetails_NetWorthID, EUS_Profiles.OtherDetails_Occupation, EUS_Profiles.PersonalInfo_HeightID, 
            EUS_Profiles.PersonalInfo_BodyTypeID, EUS_Profiles.PersonalInfo_EyeColorID, 
            EUS_Profiles.PersonalInfo_HairColorID, EUS_Profiles.PersonalInfo_ChildrenID, EUS_Profiles.PersonalInfo_EthnicityID,
            EUS_Profiles.PersonalInfo_ReligionID, EUS_Profiles.PersonalInfo_SmokingHabitID, 
            EUS_Profiles.PersonalInfo_DrinkingHabitID, EUS_Profiles.LookingFor_ToMeetMaleID, 
            EUS_Profiles.LookingFor_ToMeetFemaleID, EUS_Profiles.LookingFor_RelationshipStatusID, 
            EUS_Profiles.LookingFor_TypeOfDating_ShortTermRelationship, EUS_Profiles.LookingFor_TypeOfDating_Friendship,
            EUS_Profiles.LookingFor_TypeOfDating_LongTermRelationship, 
            EUS_Profiles.LookingFor_TypeOfDating_MutuallyBeneficialArrangements, EUS_Profiles.LookingFor_TypeOfDating_MarriedDating, 
            EUS_Profiles.LookingFor_TypeOfDating_AdultDating_Casual, 
            EUS_Profiles.PrivacySettings_HideMeFromSearchResults, EUS_Profiles.PrivacySettings_HideMeFromMembersIHaveBlocked, 
            EUS_Profiles.PrivacySettings_NotShowInOtherUsersFavoritedList, 
            EUS_Profiles.PrivacySettings_NotShowInOtherUsersViewedList, EUS_Profiles.PrivacySettings_NotShowMyGifts, 
            EUS_Profiles.DateTimeToRegister, 
            EUS_Profiles.RegisterIP, 
            EUS_Profiles.LastLoginDateTime, 
            EUS_Profiles.LastUpdateProfileDateTime, 
            EUS_Profiles.LAGID, 
            EUS_Profiles.Birthday, 
            EUS_Profiles.IsOnline, 
            EUS_Profiles.LastActivityDateTime, 
            phot.CustomerPhotosID, 
            phot.CustomerID, 
            phot.DateTimeToUploading, phot.FileName, 
            phot.DisplayLevel, 
            phot.HasAproved, phot.HasDeclined, 
            phot.CheckedContextID, 
            phot.IsDefault, 
	        [HasPhoto] = case 
			        when [EUS_Profiles].PhotosPublic > 0  then 1
			        when [EUS_Profiles].PhotosPrivate > 0  then 1
			        when phot.[CustomerID] IS NULL  then 0
			        else 1 
			    end,

		    OfferID= case 
						when offr.OfferID IS null then offr1.OfferID
					else offr.OfferID
					end, 
		    OffersDateTimeToCreate= case 
						when offr.OfferID IS null then offr1.DateTimeToCreate
					else offr.DateTimeToCreate 
					end, 
		    OffersOfferTypeID= case 
						when offr.OfferID IS null then offr1.OfferTypeID
					else offr.OfferTypeID 
					end, 
		    OffersFromProfileID= case 
						when offr.OfferID IS null then offr1.FromProfileID
					else offr.FromProfileID 
					end, 
		    OffersToProfileID= case 
						when offr.OfferID IS null then offr1.ToProfileID
					else offr.ToProfileID 
					end, 
		    OffersAmount= case 
						when offr.OfferID IS null then offr1.Amount
					else offr.Amount
					end, 
		    OffersStatusID= case 
						when offr.OfferID IS null then offr1.StatusID
					else offr.StatusID 
					end

	    FROM
			    dbo.EUS_Profiles EUS_Profiles
	    LEFT OUTER JOIN	dbo.EUS_Offers offr on (offr.FromProfileID = @CurrentProfileId AND offr.ToProfileID = EUS_Profiles.ProfileID and isnull(offr.FromProfileDeleted,0) = 0 AND offr.StatusID IN ([OFFERS_STATUSID_LIST]) AND offr.OfferTypeID IN ([OFFERS_TYPEID_LIST]))
	    LEFT OUTER JOIN	dbo.EUS_Offers offr1 on (offr1.FromProfileID = EUS_Profiles.ProfileID AND offr1.ToProfileID =@CurrentProfileId and isnull(offr1.ToProfileDeleted,0) = 0 AND offr1.StatusID IN ([OFFERS_STATUSID_LIST]) AND offr1.OfferTypeID IN ([OFFERS_TYPEID_LIST]))
   	    LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.CustomerPhotosID = EUS_Profiles.DefPhotoID
	    WHERE     
		    EUS_Profiles.IsMaster = 1
	    AND EUS_Profiles.Profileid <> 1
	    AND	EUS_Profiles.Status = @ReturnRecordsWithStatus
	    AND (offr.StatusID IN ([OFFERS_STATUSID_LIST]) OR  offr1.StatusID IN ([OFFERS_STATUSID_LIST]))
	    AND (offr.OfferTypeID IN ([OFFERS_TYPEID_LIST]) OR offr1.OfferTypeID IN ([OFFERS_TYPEID_LIST]))
    ) as tRowNumber
    where 
        ((distance <= @Distance and @Distance <@DISTANCE_DEFAULT)  OR ((distance <= @DISTANCE_DEFAULT) and @Distance =@DISTANCE_DEFAULT))
	AND tRowNumber.OffersStatusID IN ([OFFERS_STATUSID_LIST])
	AND tRowNumber.OffersOfferTypeID IN ([OFFERS_TYPEID_LIST])
) as tOutter
where 
	tOutter.RowNumber > @RowNumberMin and
	tOutter.RowNumber <= @RowNumberMax 
order by tOutter.RowNumber
]]></sql>.Value

            Dim sqlOrderBy As String = ""
            If (sorting = OffersSortEnum.OfferAmountHighest) Then
                sqlOrderBy = vbCrLf & _
                    "order by OffersAmount desc"
            ElseIf (sorting = OffersSortEnum.OfferAmountLowest) Then
                sqlOrderBy = vbCrLf & _
                    "Order By OffersAmount asc"
            ElseIf (sorting = OffersSortEnum.OldestOffers) Then
                sqlOrderBy = vbCrLf & _
                    "Order By OffersDateTimeToCreate Asc"
            ElseIf (sorting = OffersSortEnum.RecentOffers) Then
                sqlOrderBy = vbCrLf & _
                    "Order By OffersDateTimeToCreate Desc"
            End If

            sql = sql.Replace("[QueryOrdering]", sqlOrderBy)


            '    (SELECT [EUS_OffersStatusID]  
            '    FROM([dbo].[EUS_OffersStatus])
            'WHERE [ConstantName]='LIKE_ACCEEPTED_WITH_OFFER' OR [ConstantName]='LIKE_ACCEEPTED_WITH_MESSAGE' OR [ConstantName]='LIKE_ACCEEPTED_WITH_POKE'
            '	OR [ConstantName]='POKE_ACCEEPTED_WITH_OFFER' OR [ConstantName]='POKE_ACCEEPTED_WITH_MESSAGE')
            Dim OFFERS_STATUSID_LIST As String() = {ProfileHelper.OfferStatusID_LIKE_ACCEEPTED_WITH_OFFER,
                                                  ProfileHelper.OfferStatusID_LIKE_ACCEEPTED_WITH_MESSAGE,
                                                  ProfileHelper.OfferStatusID_LIKE_ACCEEPTED_WITH_POKE,
                                                  ProfileHelper.OfferStatusID_POKE_ACCEEPTED_WITH_OFFER,
                                                  ProfileHelper.OfferStatusID_POKE_ACCEEPTED_WITH_MESSAGE}

            '(SELECT [EUS_OffersTypeID]  
            'FROM([dbo].[EUS_OffersTypes])
            'WHERE [ConstantName]='WINK' OR [ConstantName]='POKE')
            Dim OFFERS_TYPEID_LIST As String() = {ProfileHelper.OfferTypeID_WINK,
                                                  ProfileHelper.OfferTypeID_POKE}


            sql = sql.Replace("[OFFERS_STATUSID_LIST]", String.Join(",", OFFERS_STATUSID_LIST))
            sql = sql.Replace("[OFFERS_TYPEID_LIST]", String.Join(",", OFFERS_TYPEID_LIST))

            Dim command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql)
            command.Parameters.Add(New SqlClient.SqlParameter("@CurrentProfileId", CurrentProfileId))
            command.Parameters.Add(New SqlClient.SqlParameter("@ReturnRecordsWithStatus", ReturnRecordsWithStatus))
            command.Parameters.Add(New SqlClient.SqlParameter("@NumberOfRecordsToReturn", NumberOfRecordsToReturn))
            command.Parameters.Add(New SqlClient.SqlParameter("@Distance", Distance))
            command.Parameters.Add(New SqlClient.SqlParameter("@DISTANCE_DEFAULT", DISTANCE_DEFAULT))
            'command.Parameters.Add(New SqlClient.SqlParameter("@zip", zipstr))
            command.Parameters.Add(New SqlClient.SqlParameter("@performCount", parms.performCount))
            command.Parameters.Add(New SqlClient.SqlParameter("@RowNumberMin", parms.rowNumberMin))
            command.Parameters.Add(New SqlClient.SqlParameter("@RowNumberMax", parms.rowNumberMax))

            If (String.IsNullOrEmpty(zipstr)) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@zip", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@zip", zipstr))
            End If
            If (latitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", latitudeIn))
            End If
            If (longitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", longitudeIn))
            End If

            Try
                Dim mins As Integer = clsConfigValues.Get__members_online_minutes()
                Dim LastActivityUTCDate As DateTime = Date.UtcNow.AddMinutes(-mins)
                command.Parameters.Add(New SqlClient.SqlParameter("@LastActivityUTCDate", LastActivityUTCDate))
            Catch
                command.Parameters.Add(New SqlClient.SqlParameter("@LastActivityUTCDate", System.DBNull.Value))
            End Try

            dt = DataHelpers.GetDataSet(command)

        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        Finally
            clsLogger.InsertLog("GetAcceptedLikesDataTable", CurrentProfileId, (DateTime.UtcNow - __logdate).TotalMilliseconds)
        End Try

        Return dt
    End Function



    Public Shared Function GetNewDatesQuickDataTable(CurrentProfileId As Integer,
                                                      ReturnRecordsWithStatus As Integer,
                                                      sorting As OffersSortEnum,
                                                      zipstr As String,
                                                      latitudeIn As Double?,
                                                      longitudeIn As Double?,
                                                      Optional Distance As Integer = DISTANCE_DEFAULT,
                                                      Optional NumberOfRecordsToReturn As Integer = 0) As DataTable
        Dim __logdate As DateTime = DateTime.UtcNow
        If (Distance = 0) Then
            Distance = DISTANCE_DEFAULT
        End If

        Dim sql As String = ""
        Dim dt As New DataTable()

        'This script is based on kilometer. If you use miles simply convert the radius to kilometer (radius=radius*1.609344)
        Try

            sql = <sql><![CDATA[
--fn:GetNewDatesQuickDataTable
	--@CurrentProfileId int
	--,@ReturnRecordsWithStatus int
	--,@NumberOfRecordsToReturn int =0
	--,@Distance int=0

	SET NOCOUNT ON;
	if(@NumberOfRecordsToReturn>0)
		SET ROWCOUNT @NumberOfRecordsToReturn;


select * from (
	SELECT    
		distance =isNull(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)), 1000),
        EUS_Profiles.ProfileID, EUS_Profiles.IsMaster, 
        EUS_Profiles.MirrorProfileID, EUS_Profiles.Status, 
        EUS_Profiles.LoginName, 
        EUS_Profiles.FirstName, EUS_Profiles.LastName, 
        EUS_Profiles.GenderId, EUS_Profiles.Country, EUS_Profiles.Region, 
        EUS_Profiles.City, EUS_Profiles.Zip, EUS_Profiles.CityArea, 
        EUS_Profiles.Address, EUS_Profiles.Telephone, EUS_Profiles.eMail, 
        EUS_Profiles.Cellular, EUS_Profiles.AreYouWillingToTravel, 
        EUS_Profiles.AboutMe_Heading, 
        EUS_Profiles.AboutMe_DescribeYourself, 
        EUS_Profiles.AboutMe_DescribeAnIdealFirstDate, 
        EUS_Profiles.OtherDetails_EducationID, 
        EUS_Profiles.OtherDetails_AnnualIncomeID, 
        EUS_Profiles.OtherDetails_NetWorthID, 
        EUS_Profiles.OtherDetails_Occupation, 
        EUS_Profiles.PersonalInfo_HeightID, 
        EUS_Profiles.PersonalInfo_BodyTypeID, 
        EUS_Profiles.PersonalInfo_EyeColorID, 
        EUS_Profiles.PersonalInfo_HairColorID,
        EUS_Profiles.PersonalInfo_ChildrenID, EUS_Profiles.PersonalInfo_EthnicityID,
        EUS_Profiles.PersonalInfo_ReligionID, 
        EUS_Profiles.PersonalInfo_SmokingHabitID, 
        EUS_Profiles.PersonalInfo_DrinkingHabitID, 
        EUS_Profiles.LookingFor_ToMeetMaleID, 
        EUS_Profiles.LookingFor_ToMeetFemaleID, 
        EUS_Profiles.LookingFor_RelationshipStatusID, 
        EUS_Profiles.LookingFor_TypeOfDating_ShortTermRelationship, 
        EUS_Profiles.LookingFor_TypeOfDating_Friendship,
        EUS_Profiles.LookingFor_TypeOfDating_LongTermRelationship, 
        EUS_Profiles.LookingFor_TypeOfDating_MutuallyBeneficialArrangements, 
        EUS_Profiles.LookingFor_TypeOfDating_MarriedDating, 
        EUS_Profiles.LookingFor_TypeOfDating_AdultDating_Casual, 
        EUS_Profiles.DateTimeToRegister, 
        EUS_Profiles.RegisterIP, 
        EUS_Profiles.LastLoginDateTime, 
        EUS_Profiles.LastUpdateProfileDateTime, 
        EUS_Profiles.LAGID, 
        EUS_Profiles.Birthday, 
        EUS_Profiles.IsOnline, 
        EUS_Profiles.LastActivityDateTime, 
        phot.CustomerPhotosID, 
        phot.CustomerID, 
        phot.DateTimeToUploading, phot.FileName, 
        phot.DisplayLevel, 
        phot.HasAproved, phot.HasDeclined, 
        phot.CheckedContextID, 
        phot.IsDefault, 
	    [HasPhoto] = case 
			    when [EUS_Profiles].PhotosPublic > 0  then 1
			    when [EUS_Profiles].PhotosPrivate > 0  then 1
			    when phot.[CustomerID] IS NULL  then 0
			    else 1 
			end,

		offr.OfferID, 
		offr.DateTimeToCreate as OffersDateTimeToCreate , 
		offr.OfferTypeID as OffersOfferTypeID, 
		offr.FromProfileID as OffersFromProfileID, 
		offr.ToProfileID as OffersToProfileID, 
		ISNULL(offr.Amount, 0) as OffersAmount, 
		offr.StatusID as OffersStatusID
	FROM
			dbo.EUS_Profiles EUS_Profiles
	INNER JOIN	dbo.EUS_Offers offr	on offr.ToProfileID= @CurrentProfileId AND offr.FromProfileID= EUS_Profiles.ProfileID
   	LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.CustomerPhotosID = EUS_Profiles.DefPhotoID
	WHERE     
		EUS_Profiles.IsMaster = 1
	AND EUS_Profiles.Profileid <> 1
    AND (offr.StatusID=200) --UNLOCKED
    AND (offr.OfferTypeID=100) --NEWDATE
    AND (ISNULL(offr.IsToProfileIDViewedDate,0)=0)
	AND	EUS_Profiles.Status = @ReturnRecordsWithStatus
) as t
where 
  ((distance <= @Distance and @Distance <@DISTANCE_DEFAULT)  OR ((distance <= @DISTANCE_DEFAULT) and @Distance =@DISTANCE_DEFAULT))

]]></sql>.Value

            'sql = sql.Replace("###GetGEOLatLng###", GetGEOLatLng())
            'sql = sql.Replace("###GetGEOLeftJoin###", GetGEOLeftJoin())


            If (sorting = OffersSortEnum.OfferAmountHighest) Then
                sql = sql & vbCrLf & _
                    "order by OffersAmount desc"
            ElseIf (sorting = OffersSortEnum.OfferAmountLowest) Then
                sql = sql & vbCrLf & _
                    "Order By OffersAmount asc"
            ElseIf (sorting = OffersSortEnum.OldestOffers) Then
                sql = sql & vbCrLf & _
                    "Order By OffersDateTimeToCreate Asc"
            ElseIf (sorting = OffersSortEnum.RecentOffers) Then
                sql = sql & vbCrLf & _
                    "Order By OffersDateTimeToCreate Desc"
            End If

            Dim command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql)
            command.Parameters.Add(New SqlClient.SqlParameter("@CurrentProfileId", CurrentProfileId))
            command.Parameters.Add(New SqlClient.SqlParameter("@ReturnRecordsWithStatus", ReturnRecordsWithStatus))
            command.Parameters.Add(New SqlClient.SqlParameter("@NumberOfRecordsToReturn", NumberOfRecordsToReturn))
            command.Parameters.Add(New SqlClient.SqlParameter("@Distance", Distance))
            command.Parameters.Add(New SqlClient.SqlParameter("@DISTANCE_DEFAULT", DISTANCE_DEFAULT))
            'command.Parameters.Add(New SqlClient.SqlParameter("@zip", zipstr))
            If (String.IsNullOrEmpty(zipstr)) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@zip", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@zip", zipstr))
            End If
            If (latitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", latitudeIn))
            End If
            If (longitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", longitudeIn))
            End If


            dt = DataHelpers.GetDataTable(command)

        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        Finally
            clsLogger.InsertLog("GetNewDatesQuickDataTable", CurrentProfileId, (DateTime.UtcNow - __logdate).TotalMilliseconds)
        End Try

        Return dt
    End Function



    Public Shared Function GetNewOffersQuickDataTable(CurrentProfileId As Integer,
                                                      ReturnRecordsWithStatus As Integer,
                                                      sorting As OffersSortEnum,
                                                      zipstr As String,
                                                      latitudeIn As Double?,
                                                      longitudeIn As Double?,
                                                      Optional Distance As Integer = DISTANCE_DEFAULT,
                                                      Optional NumberOfRecordsToReturn As Integer = 0) As DataTable
        Dim __logdate As DateTime = DateTime.UtcNow
        If (Distance = 0) Then
            Distance = DISTANCE_DEFAULT
        End If

        Dim sql As String = ""
        Dim dt As New DataTable()

        'This script is based on kilometer. If you use miles simply convert the radius to kilometer (radius=radius*1.609344)
        Try

            sql = <sql><![CDATA[
--fn:GetNewOffersQuickDataTable
	--@CurrentProfileId int
	--,@ReturnRecordsWithStatus int
	--,@NumberOfRecordsToReturn int =0
	--,@Distance int=0

	SET NOCOUNT ON;
	if(@NumberOfRecordsToReturn>0)
		SET ROWCOUNT @NumberOfRecordsToReturn;



select * from (
	SELECT    
		distance =isNull(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)), 1000),
        EUS_Profiles.ProfileID, EUS_Profiles.IsMaster, 
        EUS_Profiles.MirrorProfileID, EUS_Profiles.Status, 
        EUS_Profiles.LoginName, 
        EUS_Profiles.FirstName, EUS_Profiles.LastName, 
        EUS_Profiles.GenderId, EUS_Profiles.Country, EUS_Profiles.Region, 
        EUS_Profiles.City, EUS_Profiles.Zip, EUS_Profiles.CityArea, 
        EUS_Profiles.Address, EUS_Profiles.Telephone, EUS_Profiles.eMail, 
        EUS_Profiles.Cellular, EUS_Profiles.AreYouWillingToTravel, 
        EUS_Profiles.AboutMe_Heading, 
        EUS_Profiles.AboutMe_DescribeYourself, 
        EUS_Profiles.AboutMe_DescribeAnIdealFirstDate, 
        EUS_Profiles.OtherDetails_EducationID, 
        EUS_Profiles.OtherDetails_AnnualIncomeID, 
        EUS_Profiles.OtherDetails_NetWorthID, 
        EUS_Profiles.OtherDetails_Occupation, 
        EUS_Profiles.PersonalInfo_HeightID, 
        EUS_Profiles.PersonalInfo_BodyTypeID, 
        EUS_Profiles.PersonalInfo_EyeColorID, 
        EUS_Profiles.PersonalInfo_HairColorID,
        EUS_Profiles.PersonalInfo_ChildrenID, EUS_Profiles.PersonalInfo_EthnicityID,
        EUS_Profiles.PersonalInfo_ReligionID, 
        EUS_Profiles.PersonalInfo_SmokingHabitID, 
        EUS_Profiles.PersonalInfo_DrinkingHabitID, 
        EUS_Profiles.LookingFor_ToMeetMaleID, 
        EUS_Profiles.LookingFor_ToMeetFemaleID, 
        EUS_Profiles.LookingFor_RelationshipStatusID, 
        EUS_Profiles.LookingFor_TypeOfDating_ShortTermRelationship, 
        EUS_Profiles.LookingFor_TypeOfDating_Friendship,
        EUS_Profiles.LookingFor_TypeOfDating_LongTermRelationship, 
        EUS_Profiles.LookingFor_TypeOfDating_MutuallyBeneficialArrangements, 
        EUS_Profiles.LookingFor_TypeOfDating_MarriedDating, 
        EUS_Profiles.LookingFor_TypeOfDating_AdultDating_Casual, 
        EUS_Profiles.DateTimeToRegister, 
        EUS_Profiles.RegisterIP, 
        EUS_Profiles.LastLoginDateTime, 
        EUS_Profiles.LastUpdateProfileDateTime, 
        EUS_Profiles.LAGID, 
        EUS_Profiles.Birthday, 
        EUS_Profiles.IsOnline, 
        EUS_Profiles.LastActivityDateTime, 
        phot.CustomerPhotosID, 
        phot.CustomerID, 
        phot.DateTimeToUploading, phot.FileName, 
        phot.DisplayLevel, 
        phot.HasAproved, phot.HasDeclined, 
        phot.CheckedContextID,
        phot.IsDefault, 
	    [HasPhoto] = case 
			    when [EUS_Profiles].PhotosPublic > 0  then 1
			    when [EUS_Profiles].PhotosPrivate > 0  then 1
			    when phot.[CustomerID] IS NULL  then 0
			    else 1 
			end,
		offr.OfferID, 
		offr.DateTimeToCreate as OffersDateTimeToCreate , 
		offr.OfferTypeID as OffersOfferTypeID, 
		offr.FromProfileID as OffersFromProfileID, 
		offr.ToProfileID as OffersToProfileID, 
		offr.Amount as OffersAmount, 
		offr.StatusID as OffersStatusID


	FROM
			dbo.EUS_Profiles EUS_Profiles
	INNER JOIN  dbo.EUS_Offers offr ON offr.FromProfileID = EUS_Profiles.ProfileID
   	LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.CustomerPhotosID = EUS_Profiles.DefPhotoID
	WHERE     
		EUS_Profiles.IsMaster = 1
	 AND EUS_Profiles.[ProfileID] <> 1 
	AND offr.StatusID IN (SELECT [EUS_OffersStatusID]  
						 FROM [dbo].[EUS_OffersStatus]  
						 WHERE [ConstantName]='PENDING' OR [ConstantName]='COUNTER')
	AND offr.OfferTypeID IN (SELECT [EUS_OffersTypeID]  
							 FROM [dbo].[EUS_OffersTypes]  
							 WHERE [ConstantName]='OFFERNEW' OR [ConstantName]='OFFERCOUNTER')
	AND offr.ToProfileID = @CurrentProfileId
    AND isnull(offr.ToProfileDeleted,0) = 0
	AND	EUS_Profiles.Status = @ReturnRecordsWithStatus
) as t
where 
  ((distance <= @Distance and @Distance <@DISTANCE_DEFAULT)  OR ((distance <= @DISTANCE_DEFAULT) and @Distance =@DISTANCE_DEFAULT))

]]></sql>.Value

            'sql = sql.Replace("###GetGEOLatLng###", GetGEOLatLng())
            'sql = sql.Replace("###GetGEOLeftJoin###", GetGEOLeftJoin())


            If (sorting = OffersSortEnum.OfferAmountHighest) Then
                sql = sql & vbCrLf & _
                    "order by OffersAmount desc"
            ElseIf (sorting = OffersSortEnum.OfferAmountLowest) Then
                sql = sql & vbCrLf & _
                    "Order By OffersAmount asc"
            ElseIf (sorting = OffersSortEnum.OldestOffers) Then
                sql = sql & vbCrLf & _
                    "Order By OffersDateTimeToCreate Asc"
            ElseIf (sorting = OffersSortEnum.RecentOffers) Then
                sql = sql & vbCrLf & _
                    "Order By OffersDateTimeToCreate Desc"
            End If

            Dim command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql)
            command.Parameters.Add(New SqlClient.SqlParameter("@CurrentProfileId", CurrentProfileId))
            command.Parameters.Add(New SqlClient.SqlParameter("@ReturnRecordsWithStatus", ReturnRecordsWithStatus))
            command.Parameters.Add(New SqlClient.SqlParameter("@NumberOfRecordsToReturn", NumberOfRecordsToReturn))
            command.Parameters.Add(New SqlClient.SqlParameter("@Distance", Distance))
            command.Parameters.Add(New SqlClient.SqlParameter("@DISTANCE_DEFAULT", DISTANCE_DEFAULT))
            'If (String.IsNullOrEmpty(zipstr)) Then
            '    command.Parameters.Add(New SqlClient.SqlParameter("@zip", System.DBNull.Value))
            'Else
            '    command.Parameters.Add(New SqlClient.SqlParameter("@zip", zipstr))
            'End If
            If (latitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", latitudeIn))
            End If
            If (longitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", longitudeIn))
            End If

            dt = DataHelpers.GetDataTable(command)

        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        Finally
            clsLogger.InsertLog("GetNewOffersQuickDataTable", CurrentProfileId, (DateTime.UtcNow - __logdate).TotalMilliseconds)
        End Try

        Return dt
    End Function



    Public Shared Function GetLikesQuickDataTable(CurrentProfileId As Integer,
                                                      ReturnRecordsWithStatus As Integer,
                                                      sorting As OffersSortEnum,
                                                      zipstr As String,
                                                      latitudeIn As Double?,
                                                      longitudeIn As Double?,
                                                      Optional Distance As Integer = DISTANCE_DEFAULT,
                                                      Optional NumberOfRecordsToReturn As Integer = 0) As DataTable
        Dim __logdate As DateTime = DateTime.UtcNow
        If (Distance = 0) Then
            Distance = DISTANCE_DEFAULT
        End If

        Dim sql As String = ""
        Dim dt As New DataTable()

        'This script is based on kilometer. If you use miles simply convert the radius to kilometer (radius=radius*1.609344)
        Try

            sql = <sql><![CDATA[
--fn:GetLikesQuickDataTable
	--@CurrentProfileId int
	--,@ReturnRecordsWithStatus int
	--,@NumberOfRecordsToReturn int =0
	--,@Distance int=0


	SET NOCOUNT ON;
	if(@NumberOfRecordsToReturn>0)
		SET ROWCOUNT @NumberOfRecordsToReturn;



select * from (
	SELECT    
        distance =isNull(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)), 1000),
        EUS_Profiles.ProfileID, EUS_Profiles.IsMaster, 
        EUS_Profiles.MirrorProfileID, EUS_Profiles.Status, 
        EUS_Profiles.LoginName, 
        EUS_Profiles.FirstName, EUS_Profiles.LastName, 
        EUS_Profiles.GenderId, EUS_Profiles.Country, EUS_Profiles.Region, 
        EUS_Profiles.City, EUS_Profiles.Zip, EUS_Profiles.CityArea, 
        EUS_Profiles.Address, EUS_Profiles.Telephone, EUS_Profiles.eMail, 
        EUS_Profiles.Cellular, EUS_Profiles.AreYouWillingToTravel, 
        EUS_Profiles.AboutMe_Heading, 
        EUS_Profiles.AboutMe_DescribeYourself, 
        EUS_Profiles.AboutMe_DescribeAnIdealFirstDate, 
        EUS_Profiles.OtherDetails_EducationID, 
        EUS_Profiles.OtherDetails_AnnualIncomeID, 
        EUS_Profiles.OtherDetails_NetWorthID, 
        EUS_Profiles.OtherDetails_Occupation, 
        EUS_Profiles.PersonalInfo_HeightID, 
        EUS_Profiles.PersonalInfo_BodyTypeID, 
        EUS_Profiles.PersonalInfo_EyeColorID, 
        EUS_Profiles.PersonalInfo_HairColorID,
        EUS_Profiles.PersonalInfo_ChildrenID, EUS_Profiles.PersonalInfo_EthnicityID,
        EUS_Profiles.PersonalInfo_ReligionID, 
        EUS_Profiles.PersonalInfo_SmokingHabitID, 
        EUS_Profiles.PersonalInfo_DrinkingHabitID, 
        EUS_Profiles.LookingFor_ToMeetMaleID, 
        EUS_Profiles.LookingFor_ToMeetFemaleID, 
        EUS_Profiles.LookingFor_RelationshipStatusID, 
        EUS_Profiles.LookingFor_TypeOfDating_ShortTermRelationship, 
        EUS_Profiles.LookingFor_TypeOfDating_Friendship,
        EUS_Profiles.LookingFor_TypeOfDating_LongTermRelationship, 
        EUS_Profiles.LookingFor_TypeOfDating_MutuallyBeneficialArrangements, 
        EUS_Profiles.LookingFor_TypeOfDating_MarriedDating, 
        EUS_Profiles.LookingFor_TypeOfDating_AdultDating_Casual, 
        EUS_Profiles.DateTimeToRegister, 
        EUS_Profiles.RegisterIP, 
        EUS_Profiles.LastLoginDateTime, 
        EUS_Profiles.LastUpdateProfileDateTime, 
        EUS_Profiles.LAGID, 
        EUS_Profiles.Birthday, 
        EUS_Profiles.IsOnline, 
        EUS_Profiles.LastActivityDateTime, 
        phot.CustomerPhotosID, 
        phot.CustomerID, 
        phot.DateTimeToUploading, phot.FileName, 
        phot.DisplayLevel, 
        phot.HasAproved, phot.HasDeclined, 
        phot.CheckedContextID, 
        phot.IsDefault, 
	    [HasPhoto] = case 
			    when [EUS_Profiles].PhotosPublic > 0  then 1
			    when [EUS_Profiles].PhotosPrivate > 0  then 1
			    when phot.[CustomerID] IS NULL  then 0
			    else 1 
			end,

		offr.OfferID, 
		offr.DateTimeToCreate as OffersDateTimeToCreate , 
		offr.OfferTypeID as OffersOfferTypeID, 
		offr.FromProfileID as OffersFromProfileID, 
		offr.ToProfileID as OffersToProfileID, 
		offr.Amount as OffersAmount, 
		offr.StatusID as OffersStatusID
	FROM
			dbo.EUS_Profiles EUS_Profiles
	INNER JOIN  dbo.EUS_Offers offr ON offr.FromProfileID = EUS_Profiles.ProfileID
   	LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.CustomerPhotosID = EUS_Profiles.DefPhotoID
	WHERE     
		EUS_Profiles.IsMaster = 1
	AND EUS_Profiles.[ProfileID] <> 1 
	AND	EUS_Profiles.Status = @ReturnRecordsWithStatus
	AND offr.StatusID IN (SELECT [EUS_OffersStatusID]  
						 FROM [dbo].[EUS_OffersStatus]  
						 WHERE [ConstantName]='PENDING')
	AND offr.OfferTypeID IN (SELECT [EUS_OffersTypeID]  
							 FROM [dbo].[EUS_OffersTypes]  
							 WHERE [ConstantName]='WINK' OR [ConstantName]='POKE')
	AND offr.ToProfileID = @CurrentProfileId
    AND isnull(offr.ToProfileDeleted,0) = 0
	AND  not exists(select * 
							from EUS_ProfilesBlocked bl 
							where (bl.FromProfileID =EUS_Profiles.ProfileID and bl.ToProfileID = @CurrentProfileId) or
								(bl.FromProfileID =@CurrentProfileId and bl.ToProfileID = EUS_Profiles.ProfileID)
	)
) as t
where 
  ((distance <= @Distance and @Distance <@DISTANCE_DEFAULT)  OR ((distance <= @DISTANCE_DEFAULT) and @Distance =@DISTANCE_DEFAULT))
]]></sql>.Value

            'sql = sql.Replace("###GetGEOLatLng###", GetGEOLatLng())
            'sql = sql.Replace("###GetGEOLeftJoin###", GetGEOLeftJoin())


            If (sorting = OffersSortEnum.OfferAmountHighest) Then
                sql = sql & vbCrLf & _
                    "ORDER BY NEWID(), OffersAmount desc"
            ElseIf (sorting = OffersSortEnum.OfferAmountLowest) Then
                sql = sql & vbCrLf & _
                    "ORDER BY NEWID(), OffersAmount asc"
            ElseIf (sorting = OffersSortEnum.OldestOffers) Then
                sql = sql & vbCrLf & _
                    "ORDER BY NEWID(), OffersDateTimeToCreate Asc"
            ElseIf (sorting = OffersSortEnum.RecentOffers) Then
                sql = sql & vbCrLf & _
                    "ORDER BY NEWID(), OffersDateTimeToCreate Desc"
            Else
                sql = sql & vbCrLf & _
                    "ORDER BY NEWID()"
            End If

            Dim command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql)
            command.Parameters.Add(New SqlClient.SqlParameter("@CurrentProfileId", CurrentProfileId))
            command.Parameters.Add(New SqlClient.SqlParameter("@ReturnRecordsWithStatus", ReturnRecordsWithStatus))
            command.Parameters.Add(New SqlClient.SqlParameter("@NumberOfRecordsToReturn", NumberOfRecordsToReturn))
            command.Parameters.Add(New SqlClient.SqlParameter("@Distance", Distance))
            command.Parameters.Add(New SqlClient.SqlParameter("@DISTANCE_DEFAULT", DISTANCE_DEFAULT))
            If (String.IsNullOrEmpty(zipstr)) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@zip", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@zip", zipstr))
            End If
            If (latitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", latitudeIn))
            End If
            If (longitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", longitudeIn))
            End If


            dt = DataHelpers.GetDataTable(command)

        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        Finally
            clsLogger.InsertLog("GetLikesQuickDataTable", CurrentProfileId, (DateTime.UtcNow - __logdate).TotalMilliseconds)
        End Try

        Return dt
    End Function



    Public Shared Function GetMembersThatShouldBeNotifiedForNewMember(NewProfileID As Integer) As DataTable

        Dim __logdate As DateTime = DateTime.UtcNow

        Dim sql As String = ""
        Dim dt As New DataTable()

        'This script is based on kilometer. If you use miles simply convert the radius to kilometer (radius=radius*1.609344)
        Try
            sql = "exec GetMembersThatShouldBeNotifiedForNewMember @CustomerID=@CustomerID;"
            Dim command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql)
            command.Parameters.Add(New SqlClient.SqlParameter("@CustomerID", NewProfileID))


            dt = DataHelpers.GetDataTable(command)

        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        Finally
            clsLogger.InsertLog("GetMembersThatShouldBeNotifiedForNewMember", NewProfileID, (DateTime.UtcNow - __logdate).TotalMilliseconds)
        End Try

        Return dt
    End Function


    Public Shared Function GetMembersDistance(CurrentProfileID As Integer, OtherProfileID As Integer) As Single
        Dim result As Single = DISTANCE_DEFAULT
        Dim sql As String = ""
        Dim dt As New DataTable()
        Dim __logdate As DateTime = DateTime.UtcNow

        'This script is based on kilometer. If you use miles simply convert the radius to kilometer (radius=radius*1.609344)
        Try

            sql = "exec GetMembersDistance @customerId=@profileId, @otherCustomerId=@otherProfileId;"
            Dim command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql)
            command.Parameters.Add(New SqlClient.SqlParameter("@otherProfileId", OtherProfileID))
            command.Parameters.Add(New SqlClient.SqlParameter("@profileId", CurrentProfileID))

            dt = DataHelpers.GetDataTable(command)

            If (dt.Rows.Count > 0) Then
                If (Not dt.Rows(0).IsNull("distance")) Then
                    result = dt.Rows(0)("distance")
                End If
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        Finally
            clsLogger.InsertLog("GetMembersDistance", CurrentProfileID, (DateTime.UtcNow - __logdate).TotalMilliseconds)
        End Try

        Return result
    End Function



    Public Shared Function GetWhoFavoritedMeMembersDataTable(parms As clsMyListsHelperParameters) As DataSet

        Dim __logdate As DateTime = DateTime.UtcNow

        Dim CurrentProfileId As Integer = parms.CurrentProfileId
        Dim ReturnRecordsWithStatus As Integer = parms.ReturnRecordsWithStatus
        Dim sorting As MyListsSortEnum = parms.sorting
        Dim zipstr As String = parms.zipstr
        Dim latitudeIn As Double? = parms.latitudeIn
        Dim longitudeIn As Double? = parms.longitudeIn
        Dim Distance As Integer = parms.Distance
        Dim NumberOfRecordsToReturn As Integer = parms.NumberOfRecordsToReturn
        Dim performCount As Boolean = parms.performCount
        Dim isOnline As Boolean = parms.isOnline

        Dim sql As String = ""
        Dim dt As New DataSet()

        'This script is based on kilometer. If you use miles simply convert the radius to kilometer (radius=radius*1.609344)
        Try

            sql = <sql><![CDATA[
--fn:GetWhoFavoritedMeMembersDataTable
	--@CurrentProfileId int
	--,@ReturnRecordsWithStatus int
	--,@NumberOfRecordsToReturn int =0
	--,@Distance int=0

declare @Country varchar(10)
declare @LookingFor_ToMeetMaleID bit
declare @LookingFor_ToMeetFemaleID bit
declare @MirrorProfileID int

select 
    @MirrorProfileID=MirrorProfileID,
    @Country=Country, 
    @LookingFor_ToMeetMaleID=LookingFor_ToMeetMaleID, 
    @LookingFor_ToMeetFemaleID=LookingFor_ToMeetFemaleID 
from Eus_Profiles 
where ProfileID=@CurrentProfileId


if(@performCount=1)
begin

    select Count(*) from (
	    SELECT    
		    distance =isNull(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)), 1000)
	    FROM		
            dbo.EUS_Profiles AS EUS_Profiles 
		    with (nolock)
	    INNER JOIN
			      dbo.EUS_ProfilesFavorite AS fav ON fav.FromProfileID = EUS_Profiles.ProfileID AND fav.ToProfileID = @CurrentProfileId  
   	    LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.CustomerPhotosID = EUS_Profiles.DefPhotoID
	    WHERE
		    EUS_Profiles.IsMaster = 1
	    AND EUS_Profiles.Profileid <> 1
	    AND (
		    EUS_Profiles.ProfileID <> @CurrentProfileId or 
		    EUS_Profiles.ProfileID <> @MirrorProfileID
	    )
	    AND EUS_Profiles.Status = @ReturnRecordsWithStatus
	    AND  not exists(select * 
							    from EUS_ProfilesBlocked bl 
							    where (bl.FromProfileID =EUS_Profiles.ProfileID and bl.ToProfileID = @CurrentProfileId) or
								    (bl.FromProfileID =@CurrentProfileId and bl.ToProfileID = EUS_Profiles.ProfileID)
	    )
    ) as t
    where 
      ((distance <= @Distance and @Distance <@DISTANCE_DEFAULT)  OR ((distance <= @DISTANCE_DEFAULT) and @Distance =@DISTANCE_DEFAULT))
end


	SET NOCOUNT ON;
	if(@NumberOfRecordsToReturn>0)
		SET ROWCOUNT @NumberOfRecordsToReturn;


select 
	tOutter.*
    ,Age=CASE 
		WHEN ISNULL(tOutter.Birthday, '') = '' then null
		ELSE dbo.fn_GetAge(tOutter.Birthday, GETDATE())
	End
	,OffersCount = ISNULL((select COUNT(*)
		                from EUS_Offers 
		                where (ToProfileID=tOutter.ProfileID and  FromProfileID=@CurrentProfileId)
			            or (ToProfileID=@CurrentProfileId and FromProfileID=tOutter.ProfileID)
                        ),0)
    ,HasFavoritedMe = ISNULL((select COUNT(*)
                        from 	dbo.EUS_ProfilesFavorite favMe
                        where   favMe.ToProfileID = @CurrentProfileId AND favMe.FromProfileID = tOutter.ProfileID 
                        ),0)
    ,DidIFavorited = ISNULL((select COUNT(*)
                        from 	dbo.EUS_ProfilesFavorite favMe
                        where   favMe.ToProfileID = tOutter.ProfileID AND favMe.FromProfileID = @CurrentProfileId
                        ),0)
    ,CommunicationUnl = ISNULL((select COUNT(*)
                        from 	dbo.EUS_UnlockedConversations unl
                        where   ( unl.FromProfileID = @CurrentProfileId AND unl.ToProfileID = tOutter.ProfileID) OR
		                        ( unl.FromProfileID = tOutter.ProfileID  AND unl.ToProfileID = @CurrentProfileId)
                        ),0)

from
(
    select 
	    RowNumber = Row_Number() over([QueryOrdering])
		,tRowNumber.* 
    from (
	    SELECT    
		    distance =isNull(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)), 1000),
		    EUS_Profiles.PointsBeauty,    
		    EUS_Profiles.PointsVerification,     
		    EUS_Profiles.PointsCredits,     
            EUS_Profiles.PointsUnlocks,
            EUS_Profiles.ProfileID, EUS_Profiles.IsMaster, EUS_Profiles.MirrorProfileID, EUS_Profiles.Status, EUS_Profiles.LoginName, 
            EUS_Profiles.FirstName, EUS_Profiles.LastName, EUS_Profiles.GenderId, EUS_Profiles.Country, EUS_Profiles.Region, 
            EUS_Profiles.City, EUS_Profiles.Zip, EUS_Profiles.CityArea, EUS_Profiles.Address, EUS_Profiles.Telephone, EUS_Profiles.eMail, 
            EUS_Profiles.Cellular, EUS_Profiles.AreYouWillingToTravel, EUS_Profiles.AboutMe_Heading, 
            EUS_Profiles.AboutMe_DescribeYourself, EUS_Profiles.AboutMe_DescribeAnIdealFirstDate, 
            EUS_Profiles.OtherDetails_EducationID, EUS_Profiles.OtherDetails_AnnualIncomeID, 
            EUS_Profiles.OtherDetails_NetWorthID, EUS_Profiles.OtherDetails_Occupation, EUS_Profiles.PersonalInfo_HeightID, 
            EUS_Profiles.PersonalInfo_BodyTypeID, EUS_Profiles.PersonalInfo_EyeColorID, 
            EUS_Profiles.PersonalInfo_HairColorID, EUS_Profiles.PersonalInfo_ChildrenID, EUS_Profiles.PersonalInfo_EthnicityID,
            EUS_Profiles.PersonalInfo_ReligionID, EUS_Profiles.PersonalInfo_SmokingHabitID, 
            EUS_Profiles.PersonalInfo_DrinkingHabitID, EUS_Profiles.LookingFor_ToMeetMaleID, 
            EUS_Profiles.LookingFor_ToMeetFemaleID, EUS_Profiles.LookingFor_RelationshipStatusID, 
            EUS_Profiles.LookingFor_TypeOfDating_ShortTermRelationship, EUS_Profiles.LookingFor_TypeOfDating_Friendship,
            EUS_Profiles.LookingFor_TypeOfDating_LongTermRelationship, 
            EUS_Profiles.LookingFor_TypeOfDating_MutuallyBeneficialArrangements, EUS_Profiles.LookingFor_TypeOfDating_MarriedDating, 
            EUS_Profiles.LookingFor_TypeOfDating_AdultDating_Casual, 
            EUS_Profiles.DateTimeToRegister, 
            EUS_Profiles.RegisterIP, 
            EUS_Profiles.LastLoginDateTime, 
            EUS_Profiles.LastUpdateProfileDateTime, 
            EUS_Profiles.LAGID, 
            EUS_Profiles.Birthday, 
            EUS_Profiles.IsOnline, 
            EUS_Profiles.LastActivityDateTime, 
            EUS_Profiles.CelebratingBirth, 
		    phot.CustomerPhotosID, 
		    phot.CustomerID, 
		    phot.DateTimeToUploading, 
		    phot.FileName, 
		    phot.DisplayLevel, 
		    phot.HasAproved, 
		    phot.HasDeclined, 
		    phot.CheckedContextID, 
		    phot.IsDefault, 
	        [HasPhoto] = case 
			        when [EUS_Profiles].PhotosPublic > 0  then 1
			        when [EUS_Profiles].PhotosPrivate > 0  then 1
			        when phot.[CustomerID] IS NULL  then 0
			        else 1 
			    end,

		    fav.EUS_ProfilesFavoriteID as ProfilesFavoriteID, 
		    fav.DateTimeToCreate as ProfilesFavoriteDateTimeToCreate, 
		    fav.FromProfileID as ProfilesFavoriteFromProfileID, 
		    fav.ToProfileID as ProfilesFavoriteToProfileID
	    FROM		
            dbo.EUS_Profiles AS EUS_Profiles 
		    with (nolock)
	    INNER JOIN
			        dbo.EUS_ProfilesFavorite AS fav ON fav.FromProfileID = EUS_Profiles.ProfileID AND fav.ToProfileID = @CurrentProfileId  
   	    LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.CustomerPhotosID = EUS_Profiles.DefPhotoID
	    WHERE
		    EUS_Profiles.IsMaster = 1
	    AND EUS_Profiles.Profileid <> 1
	    AND (
		    EUS_Profiles.ProfileID <> @CurrentProfileId or 
		    EUS_Profiles.ProfileID <> @MirrorProfileID
	    )
	    AND EUS_Profiles.Status = @ReturnRecordsWithStatus
	    AND  not exists(select * 
						from EUS_ProfilesBlocked bl 
		                with (nolock)
						where (bl.FromProfileID =EUS_Profiles.ProfileID and bl.ToProfileID = @CurrentProfileId) or
							(bl.FromProfileID =@CurrentProfileId and bl.ToProfileID = EUS_Profiles.ProfileID)
	    )
    ) as tRowNumber
    where 
        ((distance <= @Distance and @Distance <@DISTANCE_DEFAULT)  OR ((distance <= @DISTANCE_DEFAULT) and @Distance =@DISTANCE_DEFAULT))
) as tOutter
where 
	tOutter.RowNumber > @RowNumberMin and
	tOutter.RowNumber <= @RowNumberMax 
order by tOutter.RowNumber
]]></sql>.Value

            Dim sqlOrderBy As String = ""
            If (sorting = MyListsSortEnum.Recent) Then
                sqlOrderBy = vbCrLf & _
                    "Order By HasPhoto desc, ProfilesFavoriteDateTimeToCreate Desc"
            ElseIf (sorting = MyListsSortEnum.Oldest) Then
                sqlOrderBy = vbCrLf & _
                    "Order By HasPhoto desc, ProfilesFavoriteDateTimeToCreate Asc"
            End If

            sql = sql.Replace("[QueryOrdering]", sqlOrderBy)

            Dim command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql)
            command.Parameters.Add(New SqlClient.SqlParameter("@CurrentProfileId", CurrentProfileId))
            command.Parameters.Add(New SqlClient.SqlParameter("@ReturnRecordsWithStatus", ReturnRecordsWithStatus))
            command.Parameters.Add(New SqlClient.SqlParameter("@NumberOfRecordsToReturn", NumberOfRecordsToReturn))
            command.Parameters.Add(New SqlClient.SqlParameter("@Distance", DISTANCE_DEFAULT))
            command.Parameters.Add(New SqlClient.SqlParameter("@DISTANCE_DEFAULT", DISTANCE_DEFAULT))
            command.Parameters.Add(New SqlClient.SqlParameter("@performCount", parms.performCount))
            command.Parameters.Add(New SqlClient.SqlParameter("@RowNumberMin", parms.rowNumberMin))
            command.Parameters.Add(New SqlClient.SqlParameter("@RowNumberMax", parms.rowNumberMax))

            If (String.IsNullOrEmpty(zipstr)) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@zip", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@zip", zipstr))
            End If
            If (latitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", latitudeIn))
            End If
            If (longitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", longitudeIn))
            End If


            dt = DataHelpers.GetDataSet(command)

        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        Finally
            clsLogger.InsertLog("GetWhoFavoritedMeMembersDataTable", CurrentProfileId, (DateTime.UtcNow - __logdate).TotalMilliseconds)
        End Try

        Return dt
    End Function



    Public Shared Function GetMyFavoriteMembersDataTable(parms As clsMyListsHelperParameters) As DataSet

        Dim __logdate As DateTime = DateTime.UtcNow

        Dim CurrentProfileId As Integer = parms.CurrentProfileId
        Dim ReturnRecordsWithStatus As Integer = parms.ReturnRecordsWithStatus
        Dim sorting As MyListsSortEnum = parms.sorting
        Dim zipstr As String = parms.zipstr
        Dim latitudeIn As Double? = parms.latitudeIn
        Dim longitudeIn As Double? = parms.longitudeIn
        Dim Distance As Integer = parms.Distance
        Dim NumberOfRecordsToReturn As Integer = parms.NumberOfRecordsToReturn
        Dim performCount As Boolean = parms.performCount
        Dim isOnline As Boolean = parms.isOnline

        Dim sql As String = ""
        Dim dt As New DataSet()

        'This script is based on kilometer. If you use miles simply convert the radius to kilometer (radius=radius*1.609344)
        Try

            sql = <sql><![CDATA[
--fn:GetMyFavoriteMembersDataTable
	--@CurrentProfileId int
	--,@ReturnRecordsWithStatus int
	--,@NumberOfRecordsToReturn int =0
	--,@Distance int=0

declare @Country varchar(10)
declare @LookingFor_ToMeetMaleID bit
declare @LookingFor_ToMeetFemaleID bit
declare @MirrorProfileID int

select 
    @MirrorProfileID=MirrorProfileID,
    @Country=Country, 
    @LookingFor_ToMeetMaleID=LookingFor_ToMeetMaleID, 
    @LookingFor_ToMeetFemaleID=LookingFor_ToMeetFemaleID 
from Eus_Profiles 
where ProfileID=@CurrentProfileId


if(@performCount=1)
begin

    select Count(*) from (
	    SELECT    
		    distance =isNull(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)), 1000)
	    FROM		
            dbo.EUS_Profiles AS EUS_Profiles 
		    with (nolock)
	    INNER JOIN  dbo.EUS_ProfilesFavorite AS fav ON fav.ToProfileID = EUS_Profiles.ProfileID AND fav.FromProfileID = @CurrentProfileId  
   	    LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.CustomerPhotosID = EUS_Profiles.DefPhotoID
	    WHERE
		    EUS_Profiles.IsMaster = 1
	    AND EUS_Profiles.Profileid <> 1
	    AND (
		    EUS_Profiles.ProfileID <> @CurrentProfileId or 
		    EUS_Profiles.ProfileID <> @MirrorProfileID
	    )
	    AND EUS_Profiles.Status = @ReturnRecordsWithStatus
	    AND  not exists(select * 
					    from EUS_ProfilesBlocked bl 
					    where (bl.FromProfileID =EUS_Profiles.ProfileID and bl.ToProfileID = @CurrentProfileId) or
						    (bl.FromProfileID =@CurrentProfileId and bl.ToProfileID = EUS_Profiles.ProfileID)
	    )
        AND ISNULL(EUS_Profiles.PrivacySettings_NotShowInOtherUsersFavoritedList,0) = 0
        AND (
            @LastActivityDateTime is null or
            (
                not @LastActivityDateTime is null and 
                EUS_Profiles.IsOnline=1 and 
                EUS_Profiles.LastActivityDateTime>=@LastActivityDateTime and
                not exists(select ProfileID 
                        from [EUS_ProfilesPrivacySettings]
                        where (ProfileID=EUS_Profiles.ProfileID OR [MirrorProfileID]=EUS_Profiles.ProfileID)
                        and [PrivacySettings_ShowMeOffline]=1)
            )
        )
    ) as t
    where 
      ((distance <= @Distance and @Distance <@DISTANCE_DEFAULT)  OR ((distance <= @DISTANCE_DEFAULT) and @Distance =@DISTANCE_DEFAULT))
end


SET NOCOUNT ON;
if(@NumberOfRecordsToReturn>0)
	SET ROWCOUNT @NumberOfRecordsToReturn;






select 
	tOutter.*
    ,Age=CASE 
		WHEN ISNULL(tOutter.Birthday, '') = '' then null
		ELSE dbo.fn_GetAge(tOutter.Birthday, GETDATE())
	End
	,OffersCount = ISNULL((select COUNT(*)
		                from EUS_Offers 
		                where (ToProfileID=tOutter.ProfileID and  FromProfileID=@CurrentProfileId)
			            or (ToProfileID=@CurrentProfileId and FromProfileID=tOutter.ProfileID)
                        ),0)
    ,HasFavoritedMe = ISNULL((select COUNT(*)
                        from 	dbo.EUS_ProfilesFavorite favMe
                        where   favMe.ToProfileID = @CurrentProfileId AND favMe.FromProfileID = tOutter.ProfileID 
                        ),0)
    ,DidIFavorited = ISNULL((select COUNT(*)
                        from 	dbo.EUS_ProfilesFavorite favMe
                        where   favMe.ToProfileID = tOutter.ProfileID AND favMe.FromProfileID = @CurrentProfileId
                        ),0)
    ,CommunicationUnl = ISNULL((select COUNT(*)
                        from 	dbo.EUS_UnlockedConversations unl
                        where   ( unl.FromProfileID = @CurrentProfileId AND unl.ToProfileID = tOutter.ProfileID) OR
		                        ( unl.FromProfileID = tOutter.ProfileID  AND unl.ToProfileID = @CurrentProfileId)
                        ),0)
from
(
    select 
	    RowNumber = Row_Number() over([QueryOrdering])
		,tRowNumber.* 
    from (
	    SELECT    
		    distance =isNull(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)), 1000),
		    EUS_Profiles.PointsBeauty,    
		    EUS_Profiles.PointsVerification,     
		    EUS_Profiles.PointsCredits,     
            EUS_Profiles.PointsUnlocks,
            EUS_Profiles.ProfileID, EUS_Profiles.IsMaster, EUS_Profiles.MirrorProfileID, EUS_Profiles.Status, EUS_Profiles.LoginName, 
            EUS_Profiles.FirstName, EUS_Profiles.LastName, EUS_Profiles.GenderId, EUS_Profiles.Country, EUS_Profiles.Region, 
            EUS_Profiles.City, EUS_Profiles.Zip, EUS_Profiles.CityArea, EUS_Profiles.Address, EUS_Profiles.Telephone, EUS_Profiles.eMail, 
            EUS_Profiles.Cellular, EUS_Profiles.AreYouWillingToTravel, EUS_Profiles.AboutMe_Heading, 
            EUS_Profiles.AboutMe_DescribeYourself, EUS_Profiles.AboutMe_DescribeAnIdealFirstDate, 
            EUS_Profiles.OtherDetails_EducationID, EUS_Profiles.OtherDetails_AnnualIncomeID, 
            EUS_Profiles.OtherDetails_NetWorthID, EUS_Profiles.OtherDetails_Occupation, EUS_Profiles.PersonalInfo_HeightID, 
            EUS_Profiles.PersonalInfo_BodyTypeID, EUS_Profiles.PersonalInfo_EyeColorID, 
            EUS_Profiles.PersonalInfo_HairColorID, EUS_Profiles.PersonalInfo_ChildrenID, EUS_Profiles.PersonalInfo_EthnicityID,
            EUS_Profiles.PersonalInfo_ReligionID, EUS_Profiles.PersonalInfo_SmokingHabitID, 
            EUS_Profiles.PersonalInfo_DrinkingHabitID, EUS_Profiles.LookingFor_ToMeetMaleID, 
            EUS_Profiles.LookingFor_ToMeetFemaleID, EUS_Profiles.LookingFor_RelationshipStatusID, 
            EUS_Profiles.LookingFor_TypeOfDating_ShortTermRelationship, EUS_Profiles.LookingFor_TypeOfDating_Friendship,
            EUS_Profiles.LookingFor_TypeOfDating_LongTermRelationship, 
            EUS_Profiles.LookingFor_TypeOfDating_MutuallyBeneficialArrangements, EUS_Profiles.LookingFor_TypeOfDating_MarriedDating, 
            EUS_Profiles.LookingFor_TypeOfDating_AdultDating_Casual, 
            EUS_Profiles.DateTimeToRegister, 
            EUS_Profiles.RegisterIP, 
            EUS_Profiles.LastLoginDateTime, 
            EUS_Profiles.LastUpdateProfileDateTime, 
            EUS_Profiles.LAGID, 
            EUS_Profiles.Birthday, 
            EUS_Profiles.IsOnline, 
            EUS_Profiles.LastActivityDateTime, 
            EUS_Profiles.CelebratingBirth, 
		    phot.CustomerPhotosID, 
		    phot.CustomerID, 
		    phot.DateTimeToUploading, 
		    phot.FileName, 
		    phot.DisplayLevel, 
		    phot.HasAproved, 
		    phot.HasDeclined, 
		    phot.CheckedContextID, 
		    phot.IsDefault, 
	        [HasPhoto] = case 
			        when [EUS_Profiles].PhotosPublic > 0  then 1
			        when [EUS_Profiles].PhotosPrivate > 0  then 1
			        when phot.[CustomerID] IS NULL  then 0
			        else 1 
			    end,

		    fav.EUS_ProfilesFavoriteID as ProfilesFavoriteID, 
		    fav.DateTimeToCreate as ProfilesFavoriteDateTimeToCreate, 
		    fav.FromProfileID as ProfilesFavoriteFromProfileID, 
		    fav.ToProfileID as ProfilesFavoriteToProfileID
	    FROM		
            dbo.EUS_Profiles AS EUS_Profiles 
		    with (nolock)
	    INNER JOIN  dbo.EUS_ProfilesFavorite AS fav ON fav.ToProfileID = EUS_Profiles.ProfileID AND fav.FromProfileID = @CurrentProfileId  
   	    LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.CustomerPhotosID = EUS_Profiles.DefPhotoID
	    WHERE
		    EUS_Profiles.IsMaster = 1
	    AND EUS_Profiles.Profileid <> 1
	    AND (
		    EUS_Profiles.ProfileID <> @CurrentProfileId or 
		    EUS_Profiles.ProfileID <> @MirrorProfileID
	    )
	    AND EUS_Profiles.Status = @ReturnRecordsWithStatus
	    AND  not exists(select * 
					    from EUS_ProfilesBlocked bl 
					    where (bl.FromProfileID =EUS_Profiles.ProfileID and bl.ToProfileID = @CurrentProfileId) or
						    (bl.FromProfileID =@CurrentProfileId and bl.ToProfileID = EUS_Profiles.ProfileID)
	    )
        AND ISNULL(EUS_Profiles.PrivacySettings_NotShowInOtherUsersFavoritedList,0) = 0
        AND (
            @LastActivityDateTime is null or
            (
                not @LastActivityDateTime is null and 
                EUS_Profiles.IsOnline=1 and 
                EUS_Profiles.LastActivityDateTime>=@LastActivityDateTime and
                not exists(select ProfileID 
                        from [EUS_ProfilesPrivacySettings]
                        where (ProfileID=EUS_Profiles.ProfileID OR [MirrorProfileID]=EUS_Profiles.ProfileID)
                        and [PrivacySettings_ShowMeOffline]=1)
            )
        )
    ) as tRowNumber
    where 
        ((distance <= @Distance and @Distance <@DISTANCE_DEFAULT)  OR ((distance <= @DISTANCE_DEFAULT) and @Distance =@DISTANCE_DEFAULT))
) as tOutter
where 
	tOutter.RowNumber > @RowNumberMin and
	tOutter.RowNumber <= @RowNumberMax 
order by tOutter.RowNumber
]]></sql>.Value


            Dim sqlOrderBy As String = ""
            If (sorting = MyListsSortEnum.Recent) Then
                sqlOrderBy = vbCrLf & _
                    "Order By HasPhoto desc, ProfilesFavoriteDateTimeToCreate Desc"
            ElseIf (sorting = MyListsSortEnum.Oldest) Then
                sqlOrderBy = vbCrLf & _
                    "Order By HasPhoto desc, ProfilesFavoriteDateTimeToCreate Asc"
            End If

            sql = sql.Replace("[QueryOrdering]", sqlOrderBy)

            Dim command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql)
            command.Parameters.Add(New SqlClient.SqlParameter("@CurrentProfileId", CurrentProfileId))
            command.Parameters.Add(New SqlClient.SqlParameter("@ReturnRecordsWithStatus", ReturnRecordsWithStatus))
            command.Parameters.Add(New SqlClient.SqlParameter("@NumberOfRecordsToReturn", NumberOfRecordsToReturn))
            command.Parameters.Add(New SqlClient.SqlParameter("@Distance", DISTANCE_DEFAULT))
            command.Parameters.Add(New SqlClient.SqlParameter("@DISTANCE_DEFAULT", DISTANCE_DEFAULT))
            command.Parameters.Add(New SqlClient.SqlParameter("@performCount", parms.performCount))
            command.Parameters.Add(New SqlClient.SqlParameter("@RowNumberMin", parms.rowNumberMin))
            command.Parameters.Add(New SqlClient.SqlParameter("@RowNumberMax", parms.rowNumberMax))

            If (String.IsNullOrEmpty(zipstr)) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@zip", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@zip", zipstr))
            End If
            If (latitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", latitudeIn))
            End If
            If (longitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", longitudeIn))
            End If


            If (isOnline) Then
                Dim mins As Integer = clsConfigValues.Get__members_online_minutes()
                Dim LastActivityUTCDate As DateTime = Date.UtcNow.AddMinutes(-mins)

                command.Parameters.Add(New SqlClient.SqlParameter("@LastActivityDateTime", LastActivityUTCDate))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@LastActivityDateTime", DBNull.Value))
            End If

            dt = DataHelpers.GetDataSet(command)

        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        Finally
            clsLogger.InsertLog("GetMyFavoriteMembersDataTable", CurrentProfileId, (DateTime.UtcNow - __logdate).TotalMilliseconds)
        End Try

        Return dt
    End Function


    Public Shared Function GetWhoSharedPhotoMembersDataTable(parms As clsMyListsHelperParameters) As DataSet

        Dim __logdate As DateTime = DateTime.UtcNow

        Dim CurrentProfileId As Integer = parms.CurrentProfileId
        Dim ReturnRecordsWithStatus As Integer = parms.ReturnRecordsWithStatus
        Dim sorting As MyListsSortEnum = parms.sorting
        Dim zipstr As String = parms.zipstr
        Dim latitudeIn As Double? = parms.latitudeIn
        Dim longitudeIn As Double? = parms.longitudeIn
        Dim Distance As Integer = parms.Distance
        Dim NumberOfRecordsToReturn As Integer = parms.NumberOfRecordsToReturn
        Dim performCount As Boolean = parms.performCount
        Dim isOnline As Boolean = parms.isOnline

        Dim sql As String = ""
        Dim dt As New DataSet()

        'This script is based on kilometer. If you use miles simply convert the radius to kilometer (radius=radius*1.609344)
        Try

            sql = <sql><![CDATA[
--fn:GetWhoSharedPhotoMembersDataTable
	--@CurrentProfileId int
	--,@ReturnRecordsWithStatus int
	--,@NumberOfRecordsToReturn int =0
	--,@Distance int=0


declare @Country varchar(10)
declare @LookingFor_ToMeetMaleID bit
declare @LookingFor_ToMeetFemaleID bit
declare @MirrorProfileID int

select 
    @MirrorProfileID=MirrorProfileID,
    @Country=Country, 
    @LookingFor_ToMeetMaleID=LookingFor_ToMeetMaleID, 
    @LookingFor_ToMeetFemaleID=LookingFor_ToMeetFemaleID 
from Eus_Profiles 
where ProfileID=@CurrentProfileId




if(@performCount=1)
begin

    select Count(*) from (
	    SELECT    
		    distance =isNull(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)), 1000)
	    FROM		
            dbo.EUS_Profiles AS EUS_Profiles 
		    with (nolock)
	    WHERE
		    EUS_Profiles.IsMaster = 1
	    AND EUS_Profiles.Profileid <> 1
	    AND (
		    EUS_Profiles.ProfileID <> @CurrentProfileId or 
		    EUS_Profiles.ProfileID <> @MirrorProfileID
	    )
	    AND EUS_Profiles.Status = @ReturnRecordsWithStatus
	    AND  exists(select * 
					from EUS_ProfilePhotosLevel fav 
					where fav.FromProfileID = EUS_Profiles.ProfileID AND fav.ToProfileID = @CurrentProfileId   and fav.PhotoLevelID>0
	    )
	    AND  not exists(select * 
							    from EUS_ProfilesBlocked bl 
							    where (bl.FromProfileID =EUS_Profiles.ProfileID and bl.ToProfileID = @CurrentProfileId) or
								    (bl.FromProfileID =@CurrentProfileId and bl.ToProfileID = EUS_Profiles.ProfileID)
	    )
	    AND exists(
		    select CustomerPhotosID 
		    from EUS_CustomerPhotos p
		    where p.CustomerID = EUS_Profiles.ProfileID 
		    and p.DisplayLevel>0
		    and p.HasAproved=1
		    and ISNULL(p.IsDeleted,0)=0
	    )
    ) as t
    where 
      ((distance <= @Distance and @Distance <@DISTANCE_DEFAULT)  OR ((distance <= @DISTANCE_DEFAULT) and @Distance =@DISTANCE_DEFAULT))
end


	SET NOCOUNT ON;
	if(@NumberOfRecordsToReturn>0)
		SET ROWCOUNT @NumberOfRecordsToReturn;


select 
	tOutter.*
    ,Age=CASE 
		WHEN ISNULL(tOutter.Birthday, '') = '' then null
		ELSE dbo.fn_GetAge(tOutter.Birthday, GETDATE())
	End
    ,OffersCount = ISNULL((select COUNT(*)
		                    from EUS_Offers 
		                    where (ToProfileID=tOutter.ProfileID and  FromProfileID=@CurrentProfileId)
			                or (ToProfileID=@CurrentProfileId and FromProfileID=tOutter.ProfileID)
                            ),0)
    ,HasFavoritedMe = ISNULL((select COUNT(*)
                            from 	dbo.EUS_ProfilesFavorite favMe
                            where   favMe.ToProfileID = @CurrentProfileId AND favMe.FromProfileID = tOutter.ProfileID 
                            ),0)
    ,DidIFavorited = ISNULL((select COUNT(*)
                            from 	dbo.EUS_ProfilesFavorite favMe
                            where   favMe.ToProfileID = tOutter.ProfileID AND favMe.FromProfileID = @CurrentProfileId
                            ),0)
    ,CommunicationUnl = ISNULL((select COUNT(*)
                            from 	dbo.EUS_UnlockedConversations unl
                            where   ( unl.FromProfileID = @CurrentProfileId AND unl.ToProfileID = tOutter.ProfileID) OR
		                            ( unl.FromProfileID = tOutter.ProfileID  AND unl.ToProfileID = @CurrentProfileId)
                            ),0)

from
(
    select 
	    RowNumber = Row_Number() over([QueryOrdering])
		,tRowNumber.* 
    from (
	    SELECT    
		    distance =isNull(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)), 1000),
		    EUS_Profiles.PointsBeauty,    
		    EUS_Profiles.PointsVerification,     
		    EUS_Profiles.PointsCredits,     
            EUS_Profiles.PointsUnlocks,
            EUS_Profiles.ProfileID, EUS_Profiles.IsMaster, EUS_Profiles.MirrorProfileID, EUS_Profiles.Status, EUS_Profiles.LoginName, 
            EUS_Profiles.FirstName, EUS_Profiles.LastName, EUS_Profiles.GenderId, EUS_Profiles.Country, EUS_Profiles.Region, 
            EUS_Profiles.City, EUS_Profiles.Zip, EUS_Profiles.CityArea, EUS_Profiles.Address, EUS_Profiles.Telephone, EUS_Profiles.eMail, 
            EUS_Profiles.Cellular, EUS_Profiles.AreYouWillingToTravel, EUS_Profiles.AboutMe_Heading, 
            EUS_Profiles.AboutMe_DescribeYourself, EUS_Profiles.AboutMe_DescribeAnIdealFirstDate, 
            EUS_Profiles.OtherDetails_EducationID, EUS_Profiles.OtherDetails_AnnualIncomeID, 
            EUS_Profiles.OtherDetails_NetWorthID, EUS_Profiles.OtherDetails_Occupation, EUS_Profiles.PersonalInfo_HeightID, 
            EUS_Profiles.PersonalInfo_BodyTypeID, EUS_Profiles.PersonalInfo_EyeColorID, 
            EUS_Profiles.PersonalInfo_HairColorID, EUS_Profiles.PersonalInfo_ChildrenID, EUS_Profiles.PersonalInfo_EthnicityID,
            EUS_Profiles.PersonalInfo_ReligionID, EUS_Profiles.PersonalInfo_SmokingHabitID, 
            EUS_Profiles.PersonalInfo_DrinkingHabitID, EUS_Profiles.LookingFor_ToMeetMaleID, 
            EUS_Profiles.LookingFor_ToMeetFemaleID, EUS_Profiles.LookingFor_RelationshipStatusID, 
            EUS_Profiles.LookingFor_TypeOfDating_ShortTermRelationship, EUS_Profiles.LookingFor_TypeOfDating_Friendship,
            EUS_Profiles.LookingFor_TypeOfDating_LongTermRelationship, 
            EUS_Profiles.LookingFor_TypeOfDating_MutuallyBeneficialArrangements, 
            EUS_Profiles.LookingFor_TypeOfDating_MarriedDating, 
            EUS_Profiles.LookingFor_TypeOfDating_AdultDating_Casual, 
            EUS_Profiles.DateTimeToRegister, 
            EUS_Profiles.RegisterIP, 
            EUS_Profiles.LastLoginDateTime, 
            EUS_Profiles.LastUpdateProfileDateTime, 
            EUS_Profiles.LAGID, 
            EUS_Profiles.Birthday, 
            EUS_Profiles.IsOnline, 
            EUS_Profiles.LastActivityDateTime, 
            EUS_Profiles.CelebratingBirth, 
		    phot.CustomerPhotosID, 
		    phot.CustomerID, 
		    phot.DateTimeToUploading, 
		    phot.FileName, 
		    phot.DisplayLevel, 
		    phot.HasAproved, 
		    phot.HasDeclined, 
		    phot.CheckedContextID, 
		    phot.IsDefault, 
	        [HasPhoto] = case 
			        when [EUS_Profiles].PhotosPublic > 0  then 1
			        when [EUS_Profiles].PhotosPrivate > 0  then 1
			        when phot.[CustomerID] IS NULL  then 0
			        else 1 
			    end,

		    fav.ProfilePhotosLevelID as SharePhotosID, 
		    fav.DateTimeCreated as SharePhotosDateTimeCreated, 
		    fav.FromProfileID as SharePhotosFromProfileID, 
		    fav.ToProfileID as SharePhotosToProfileID
	    FROM		
            dbo.EUS_Profiles AS EUS_Profiles 
		    with (nolock)
	    INNER JOIN  dbo.EUS_ProfilePhotosLevel AS fav ON fav.FromProfileID = EUS_Profiles.ProfileID AND fav.ToProfileID = @CurrentProfileId   and fav.PhotoLevelID>0
   	    LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.CustomerPhotosID = EUS_Profiles.DefPhotoID
	    WHERE
		    EUS_Profiles.IsMaster = 1
	    AND EUS_Profiles.Profileid <> 1
	    AND (
		    EUS_Profiles.ProfileID <> @CurrentProfileId or 
		    EUS_Profiles.ProfileID <> @MirrorProfileID
	    )
	    AND EUS_Profiles.Status = @ReturnRecordsWithStatus
	    AND  not exists(select * 
							    from EUS_ProfilesBlocked bl 
							    where (bl.FromProfileID =EUS_Profiles.ProfileID and bl.ToProfileID = @CurrentProfileId) or
								    (bl.FromProfileID =@CurrentProfileId and bl.ToProfileID = EUS_Profiles.ProfileID)
	    )
	    AND exists(
		    select CustomerPhotosID 
		    from EUS_CustomerPhotos p
		    where p.CustomerID = EUS_Profiles.ProfileID 
		    and p.DisplayLevel>0
		    and p.HasAproved=1
		    and ISNULL(p.IsDeleted,0)=0
	    )
    ) as tRowNumber
    where 
        ((distance <= @Distance and @Distance <@DISTANCE_DEFAULT)  OR ((distance <= @DISTANCE_DEFAULT) and @Distance =@DISTANCE_DEFAULT))
) as tOutter
where 
	tOutter.RowNumber > @RowNumberMin and
	tOutter.RowNumber <= @RowNumberMax 
order by tOutter.RowNumber
]]></sql>.Value

            Dim sqlOrderBy As String = ""
            If (sorting = MyListsSortEnum.Recent) Then
                sqlOrderBy = vbCrLf & _
                    "Order By HasPhoto desc, SharePhotosDateTimeCreated Desc"
            ElseIf (sorting = MyListsSortEnum.Oldest) Then
                sqlOrderBy = vbCrLf & _
                    "Order By HasPhoto desc, SharePhotosDateTimeCreated Asc"
            End If

            sql = sql.Replace("[QueryOrdering]", sqlOrderBy)

            Dim command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql)
            command.Parameters.Add(New SqlClient.SqlParameter("@CurrentProfileId", CurrentProfileId))
            command.Parameters.Add(New SqlClient.SqlParameter("@ReturnRecordsWithStatus", ReturnRecordsWithStatus))
            command.Parameters.Add(New SqlClient.SqlParameter("@NumberOfRecordsToReturn", NumberOfRecordsToReturn))
            command.Parameters.Add(New SqlClient.SqlParameter("@Distance", DISTANCE_DEFAULT))
            command.Parameters.Add(New SqlClient.SqlParameter("@DISTANCE_DEFAULT", DISTANCE_DEFAULT))
            command.Parameters.Add(New SqlClient.SqlParameter("@performCount", parms.performCount))
            command.Parameters.Add(New SqlClient.SqlParameter("@RowNumberMin", parms.rowNumberMin))
            command.Parameters.Add(New SqlClient.SqlParameter("@RowNumberMax", parms.rowNumberMax))

            If (String.IsNullOrEmpty(zipstr)) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@zip", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@zip", zipstr))
            End If
            If (latitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", latitudeIn))
            End If
            If (longitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", longitudeIn))
            End If

            dt = DataHelpers.GetDataSet(command)

        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        Finally
            clsLogger.InsertLog("GetWhoSharedPhotoMembersDataTable", CurrentProfileId, (DateTime.UtcNow - __logdate).TotalMilliseconds)
        End Try

        Return dt
    End Function


    Public Shared Function GetSharedPhotosByMeToMembers_DataTable(parms As clsMyListsHelperParameters) As DataSet

        Dim __logdate As DateTime = DateTime.UtcNow

        Dim CurrentProfileId As Integer = parms.CurrentProfileId
        Dim ReturnRecordsWithStatus As Integer = parms.ReturnRecordsWithStatus
        Dim sorting As MyListsSortEnum = parms.sorting
        Dim zipstr As String = parms.zipstr
        Dim latitudeIn As Double? = parms.latitudeIn
        Dim longitudeIn As Double? = parms.longitudeIn
        Dim Distance As Integer = parms.Distance
        Dim NumberOfRecordsToReturn As Integer = parms.NumberOfRecordsToReturn
        Dim performCount As Boolean = parms.performCount
        Dim isOnline As Boolean = parms.isOnline

        'Public Shared Function GetSharedPhotosByMeToMembers_DataTable(sorting As MyListsSortEnum,
        '                                                  currentProfileId As Integer,
        '                                                  NumberOfRecordsToReturn As Integer,
        '                                                  ReturnRecordsWithStatus As Integer,
        '                                                  zipstr As String,
        '                                                  latitudeIn As Double?,
        '                                                  longitudeIn As Double?) As DataTable


        Dim sql As String = ""
        Dim dt As New DataSet()

        'This script is based on kilometer. If you use miles simply convert the radius to kilometer (radius=radius*1.609344)
        Try

            sql = <sql><![CDATA[
--fn:GetWhoSharedPhotoMembersDataTable
	--@CurrentProfileId int
	--,@ReturnRecordsWithStatus int
	--,@NumberOfRecordsToReturn int =0
	--,@Distance int=0

declare @Country varchar(10)
declare @LookingFor_ToMeetMaleID bit
declare @LookingFor_ToMeetFemaleID bit
declare @MirrorProfileID int

select 
    @MirrorProfileID=MirrorProfileID,
    @Country=Country, 
    @LookingFor_ToMeetMaleID=LookingFor_ToMeetMaleID, 
    @LookingFor_ToMeetFemaleID=LookingFor_ToMeetFemaleID 
from Eus_Profiles 
where ProfileID=@CurrentProfileId


if(@performCount=1)
begin

    select Count(*) from (
	    SELECT    
		    distance =isNull(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)), 1000)
	    FROM		
            dbo.EUS_Profiles AS EUS_Profiles 
		    with (nolock)
	    WHERE
		    EUS_Profiles.IsMaster = 1
	    AND EUS_Profiles.Profileid <>1
	    AND EUS_Profiles.ProfileID <>@CurrentProfileId 
		AND EUS_Profiles.ProfileID <>@MirrorProfileID
	    AND EUS_Profiles.Status = @ReturnRecordsWithStatus
	    AND  exists(select * 
					from EUS_ProfilePhotosLevel fav 
		            with (nolock)
					where fav.FromProfileID = @CurrentProfileId AND fav.ToProfileID = EUS_Profiles.ProfileID  /*and fav.PhotoLevelID>0*/
	    )
	    AND  not exists(select * 
						from EUS_ProfilesBlocked bl 
		                with (nolock)
						where (bl.FromProfileID =EUS_Profiles.ProfileID and bl.ToProfileID = @CurrentProfileId) or
							(bl.FromProfileID =@CurrentProfileId and bl.ToProfileID = EUS_Profiles.ProfileID)
	    )
        /*AND exists(
		    select CustomerPhotosID 
		    from EUS_CustomerPhotos p
		    with (nolock)
		    where p.CustomerID = @CurrentProfileId 
		    and p.DisplayLevel>0
		    and p.HasAproved=1
		    and p.HasDeclined=0
		    and ISNULL(p.IsDeleted,0)=0
	    )*/
    ) as t
    where 
      ((distance <= @Distance and @Distance <@DISTANCE_DEFAULT)  OR ((distance <= @DISTANCE_DEFAULT) and @Distance =@DISTANCE_DEFAULT))
end


	SET NOCOUNT ON;
	if(@NumberOfRecordsToReturn>0)
		SET ROWCOUNT @NumberOfRecordsToReturn;



select 
	tOutter.*
    ,Age=CASE 
		WHEN ISNULL(tOutter.Birthday, '') = '' then null
		ELSE dbo.fn_GetAge(tOutter.Birthday, GETDATE())
	End
    ,OffersCount = ISNULL((select COUNT(*)
		                    from EUS_Offers 
		                    where (ToProfileID=tOutter.ProfileID and  FromProfileID=@CurrentProfileId)
			                or (ToProfileID=@CurrentProfileId and FromProfileID=tOutter.ProfileID)
                            ),0)
    ,HasFavoritedMe = ISNULL((select COUNT(*)
                            from 	dbo.EUS_ProfilesFavorite favMe
                            where   favMe.ToProfileID = @CurrentProfileId AND favMe.FromProfileID = tOutter.ProfileID 
                            ),0)
    ,DidIFavorited = ISNULL((select COUNT(*)
                            from 	dbo.EUS_ProfilesFavorite favMe
                            where   favMe.ToProfileID = tOutter.ProfileID AND favMe.FromProfileID = @CurrentProfileId
                            ),0)
    ,CommunicationUnl = ISNULL((select COUNT(*)
                            from 	dbo.EUS_UnlockedConversations unl
                            where   ( unl.FromProfileID = @CurrentProfileId AND unl.ToProfileID = tOutter.ProfileID) OR
		                            ( unl.FromProfileID = tOutter.ProfileID  AND unl.ToProfileID = @CurrentProfileId)
                            ),0)

from
(
    select 
	    RowNumber = Row_Number() over([QueryOrdering])
		,tRowNumber.* 
    from (
	    SELECT    
		distance =isNull(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)), 1000),
		EUS_Profiles.PointsBeauty,    
		EUS_Profiles.PointsVerification,     
		EUS_Profiles.PointsCredits,     
        EUS_Profiles.PointsUnlocks,
        EUS_Profiles.ProfileID, EUS_Profiles.IsMaster, EUS_Profiles.MirrorProfileID, EUS_Profiles.Status, EUS_Profiles.LoginName, 
        EUS_Profiles.FirstName, EUS_Profiles.LastName, EUS_Profiles.GenderId, EUS_Profiles.Country, EUS_Profiles.Region, 
        EUS_Profiles.City, EUS_Profiles.Zip, EUS_Profiles.CityArea, EUS_Profiles.Address, EUS_Profiles.Telephone, EUS_Profiles.eMail, 
        EUS_Profiles.Cellular, EUS_Profiles.AreYouWillingToTravel, EUS_Profiles.AboutMe_Heading, 
        EUS_Profiles.AboutMe_DescribeYourself, EUS_Profiles.AboutMe_DescribeAnIdealFirstDate, 
        EUS_Profiles.OtherDetails_EducationID, EUS_Profiles.OtherDetails_AnnualIncomeID, 
        EUS_Profiles.OtherDetails_NetWorthID, EUS_Profiles.OtherDetails_Occupation, EUS_Profiles.PersonalInfo_HeightID, 
        EUS_Profiles.PersonalInfo_BodyTypeID, EUS_Profiles.PersonalInfo_EyeColorID, 
        EUS_Profiles.PersonalInfo_HairColorID, EUS_Profiles.PersonalInfo_ChildrenID, EUS_Profiles.PersonalInfo_EthnicityID,
        EUS_Profiles.PersonalInfo_ReligionID, EUS_Profiles.PersonalInfo_SmokingHabitID, 
        EUS_Profiles.PersonalInfo_DrinkingHabitID, EUS_Profiles.LookingFor_ToMeetMaleID, 
        EUS_Profiles.LookingFor_ToMeetFemaleID, EUS_Profiles.LookingFor_RelationshipStatusID, 
        EUS_Profiles.LookingFor_TypeOfDating_ShortTermRelationship, EUS_Profiles.LookingFor_TypeOfDating_Friendship,
        EUS_Profiles.LookingFor_TypeOfDating_LongTermRelationship, 
        EUS_Profiles.LookingFor_TypeOfDating_MutuallyBeneficialArrangements, 
        EUS_Profiles.LookingFor_TypeOfDating_MarriedDating, 
        EUS_Profiles.LookingFor_TypeOfDating_AdultDating_Casual, 
        EUS_Profiles.DateTimeToRegister, 
        EUS_Profiles.RegisterIP, 
        EUS_Profiles.LastLoginDateTime, 
        EUS_Profiles.LastUpdateProfileDateTime, 
        EUS_Profiles.LAGID, 
        EUS_Profiles.Birthday, 
        EUS_Profiles.IsOnline, 
        EUS_Profiles.LastActivityDateTime, 
		phot.CustomerPhotosID, 
		phot.CustomerID, 
		phot.DateTimeToUploading, 
		phot.FileName, 
		phot.DisplayLevel, 
		phot.HasAproved, 
		phot.HasDeclined, 
		phot.CheckedContextID, 
		phot.IsDefault, 
	    [HasPhoto] = case 
			    when [EUS_Profiles].PhotosPublic > 0  then 1
			    when [EUS_Profiles].PhotosPrivate > 0  then 1
			    when phot.[CustomerID] IS NULL  then 0
			    else 1 
			end,
		fav.ProfilePhotosLevelID as SharePhotosID, 
		fav.DateTimeCreated as SharePhotosDateTimeCreated, 
		fav.FromProfileID as SharePhotosFromProfileID, 
		fav.ToProfileID as SharePhotosToProfileID
	    FROM		
            dbo.EUS_Profiles AS EUS_Profiles 
		    with (nolock)
	    INNER JOIN      dbo.EUS_ProfilePhotosLevel AS fav ON fav.FromProfileID = @CurrentProfileId AND fav.ToProfileID = EUS_Profiles.ProfileID  /*and fav.PhotoLevelID>0*/
   	    LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.CustomerPhotosID = EUS_Profiles.DefPhotoID
	    WHERE
		    EUS_Profiles.IsMaster = 1
	    AND EUS_Profiles.Profileid <>1
	    AND EUS_Profiles.ProfileID <>@CurrentProfileId 
		AND EUS_Profiles.ProfileID <>@MirrorProfileID
	    AND EUS_Profiles.Status = @ReturnRecordsWithStatus
	    AND  not exists(select * 
						from EUS_ProfilesBlocked bl 
		                with (nolock)
						where (bl.FromProfileID =EUS_Profiles.ProfileID and bl.ToProfileID = @CurrentProfileId) or
							(bl.FromProfileID =@CurrentProfileId and bl.ToProfileID = EUS_Profiles.ProfileID)
	    )
	    /*AND exists(
		    select CustomerPhotosID 
		    from EUS_CustomerPhotos p
		    with (nolock)
		    where p.CustomerID = @CurrentProfileId 
		    and p.DisplayLevel>0
		    and p.HasAproved=1
		    and p.HasDeclined=0
		    and ISNULL(p.IsDeleted,0)=0
	    )*/
    ) as tRowNumber
    where 
        ((distance <= @Distance and @Distance <@DISTANCE_DEFAULT)  OR ((distance <= @DISTANCE_DEFAULT) and @Distance =@DISTANCE_DEFAULT))
) as tOutter
where 
	tOutter.RowNumber > @RowNumberMin and
	tOutter.RowNumber <= @RowNumberMax 
order by tOutter.RowNumber
]]></sql>.Value

           
            Dim sqlOrderBy As String = ""
            If (sorting = MyListsSortEnum.Recent) Then
                sqlOrderBy = vbCrLf & _
                    "Order By HasPhoto desc, SharePhotosDateTimeCreated Desc"
            ElseIf (sorting = MyListsSortEnum.Oldest) Then
                sqlOrderBy = vbCrLf & _
                    "Order By HasPhoto desc, SharePhotosDateTimeCreated Asc"
            End If

            sql = sql.Replace("[QueryOrdering]", sqlOrderBy)



            Dim command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql)
            command.Parameters.Add(New SqlClient.SqlParameter("@CurrentProfileId", CurrentProfileId))
            command.Parameters.Add(New SqlClient.SqlParameter("@ReturnRecordsWithStatus", ReturnRecordsWithStatus))
            command.Parameters.Add(New SqlClient.SqlParameter("@NumberOfRecordsToReturn", NumberOfRecordsToReturn))
            command.Parameters.Add(New SqlClient.SqlParameter("@Distance", DISTANCE_DEFAULT))
            command.Parameters.Add(New SqlClient.SqlParameter("@DISTANCE_DEFAULT", DISTANCE_DEFAULT))
            command.Parameters.Add(New SqlClient.SqlParameter("@performCount", parms.performCount))
            command.Parameters.Add(New SqlClient.SqlParameter("@RowNumberMin", parms.rowNumberMin))
            command.Parameters.Add(New SqlClient.SqlParameter("@RowNumberMax", parms.rowNumberMax))

            If (String.IsNullOrEmpty(zipstr)) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@zip", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@zip", zipstr))
            End If
            If (latitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", latitudeIn))
            End If
            If (longitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", longitudeIn))
            End If


            dt = DataHelpers.GetDataSet(command)

        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        Finally
            clsLogger.InsertLog("GetSharedPhotosByMeToMembers_DataTable", CurrentProfileId, (DateTime.UtcNow - __logdate).TotalMilliseconds)
        End Try

        Return dt
    End Function


End Class

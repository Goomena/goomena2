﻿Imports Dating.Server.Core.DLL
Imports System.Configuration
Imports System.Web
Imports System.Text.RegularExpressions

Public Class clsCustomer


    Public Shared Function GetPriceFromCredits(credits As Integer) As EUS_Price
        Dim _Price As EUS_Price = Nothing

        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try

            ' check
            _Price = _CMSDBDataContext.EUS_Prices.Where(Function(itm) itm.Credits = credits).FirstOrDefault()

            'If (rec IsNot Nothing) Then
            '    _Price = rec.Amount
            'End If


        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

        Return _Price
    End Function


    Public Shared Function GetEUS_PricesByAmount(amount As Integer) As EUS_Price
        Dim _Price As EUS_Price = Nothing

        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try

            ' check
            _Price = _CMSDBDataContext.EUS_Prices.Where(Function(itm) itm.Amount = amount).FirstOrDefault()

            'If (rec IsNot Nothing) Then
            '    _Price = rec.Amount
            'End If


        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

        Return _Price
    End Function



    Public Shared Sub sendPayment(ByVal amount As String,
                           ByVal type As String,
                           ByVal quantity As String,
                           ByVal loginName As String,
                           ByVal CustomerID As Integer,
                           ByVal CustomReferrer As String,
                           ByVal oURL As String,
                           ByVal paymentMethod As String,
                           ByVal promoCode As String,
                           ByVal SalesSiteID As Integer,
                           ByVal paymentProviderType As String,
                           ByVal productCode As String,
                           ByVal RegisterCountry As String)

        If (String.IsNullOrEmpty(productCode)) Then
            productCode = "dd" & quantity & type
        End If

        If (Not String.IsNullOrEmpty(paymentProviderType)) Then paymentProviderType = paymentProviderType.ToUpper()
        Dim useCase1 As Boolean = ((HttpContext.Current.Session("GEO_COUNTRY_CODE") = "GR" OrElse HttpContext.Current.Session("GEO_COUNTRY_CODE") = "AL") AndAlso paymentProviderType = "CC")
        Dim useCase_TR As Boolean = (paymentMethod.ToUpper() = "PW" OrElse paymentMethod.ToUpper() = "PAYMENTWALL") AndAlso RegisterCountry = "TR"

        If (Not String.IsNullOrEmpty(productCode)) Then
            If (Not productCode.EndsWith("TRY")) Then useCase_TR = False
        End If


        If (useCase_TR) Then

            clsCustomer.PaymentWall_TR_Payment(amount,
                            type,
                            quantity,
                            loginName,
                            CustomerID,
                            CustomReferrer,
                            oURL,
                            paymentMethod,
                            promoCode,
                            SalesSiteID,
                            paymentProviderType,
                            productCode)

        ElseIf (paymentMethod = "epoch") Then

            clsCustomer.Epoch_Payment(amount,
                           type,
                           quantity,
                           loginName,
                           CustomerID,
                           CustomReferrer,
                           oURL,
                           paymentMethod,
                           promoCode,
                           SalesSiteID,
                           paymentProviderType,
                           productCode)

        Else
            Dim qsInfo As String = ""
            Dim prof As EUS_Profile = Nothing
            Try
                prof = DataHelpers.GetEUS_ProfileByProfileID(CustomerID)
                qsInfo = clsCustomer.GetProfileInfoParameters(prof)
            Catch
            End Try

            Try
                Dim oRemotePost As New RemotePost()
                Dim postURL As String = ""
                Dim flag As Boolean = True

                If (useCase1 AndAlso False) Then

                    postURL = "https://www.putdrive.com/pay/loader.aspx"
                    paymentMethod = "adyen"

                Else


                    If paymentMethod = "AlertPay" Or paymentMethod = "paysafecard" Or paymentMethod = "epoch" Then
                        flag = False
                    End If
                    If flag Then
                        postURL = "http://www.paymix.net/pay/loader.aspx"
                    End If
                    If postURL = "" Then postURL = "http://www.soundsoft.com/pay/initPayment.ashx"
                    If productCode.ToLower = "zfreemoney" Then
                        '  postURL = "http://www.digi-store.net/pay/initPayment.ashx"
                    End If
                    If paymentMethod.ToLower = "paypal" Then
                        postURL = "http://www.digi-store.net/pay/initPayment.ashx"
                    End If
                    'If paymentMethod.ToLower = "epoch" Then

                    '    postURL = "https://www.goomena.com/pay/initPayment.ashx"

                    '    ' randomly send to dating-deals
                    '    Dim rnd As New Random(Date.UtcNow.Millisecond)
                    '    Dim rndVal = rnd.Next(100)
                    '    If (rndVal > 80) Then
                    '        postURL = "http://www.dating-deals.com/pay/initPayment.ashx"
                    '    End If

                    'End If

                End If
                oRemotePost.Url = postURL
                'oRemotePost.Url = "http://localhost:15837/initPayment.ashx"

                oRemotePost.Add("PaymentMethod", paymentMethod)
                If (paymentProviderType Is Nothing) Then
                    If (paymentMethod.ToUpper() = "PAYMENTWALL") Then
                        oRemotePost.Add("Widget", "p4_1")
                    End If
                End If
                oRemotePost.Add("ProductCode", productCode)
                oRemotePost.Add("CustomerID", CustomerID)
                oRemotePost.Add("LoginName", loginName)
                oRemotePost.Add("promoCode", promoCode)
                oRemotePost.Add("CustomReferrer", CustomReferrer)
                oRemotePost.Add("downloadURL", oURL)
                oRemotePost.Add("Amount", amount)
                oRemotePost.Add("type", paymentProviderType)
                oRemotePost.Add("SalesSiteID", SalesSiteID)
                If (prof IsNot Nothing) Then
                    Try
                        oRemotePost.Add("fn", prof.FirstName & " " & prof.LastName)
                        oRemotePost.Add("zip", prof.Zip)
                        oRemotePost.Add("cit", prof.City)
                        oRemotePost.Add("reg", prof.Region)
                        oRemotePost.Add("cntr", prof.Country)
                        oRemotePost.Add("eml", prof.eMail)
                        oRemotePost.Add("mob", prof.Cellular)
                    Catch ex As Exception

                    End Try
                End If
                oRemotePost.Post()
            Catch ex As Exception
                If (useCase1 AndAlso False) Then

                    Dim postURL As String = "https://www.putdrive.com/pay/loader.aspx"
                    postURL = postURL & _
                        "?PaymentMethod=" & paymentMethod & _
                        "&ProductCode=" & productCode & _
                        "&CustomerID=" & CustomerID & _
                        "&LoginName=" & loginName & _
                        "&promoCode=" & promoCode & _
                        "&CustomReferrer=" & CustomReferrer & _
                        "&downloadURL=" & oURL & _
                        "&amount=" & amount & _
                        "&SalesSiteID=" & SalesSiteID & _
                        "&type=" & paymentProviderType

                    HttpContext.Current.Response.Redirect(postURL)

                Else

                    Dim flag As Boolean = True
                    If paymentMethod.ToLower() = "alertpay" Or paymentMethod.ToLower() = "paysafecard" Or paymentMethod.ToLower() = "epoch" Then
                        flag = False
                    End If
                    Dim Widget As String = ""
                    If (paymentProviderType Is Nothing) Then
                        If (paymentMethod.ToUpper() = "PAYMENTWALL") Then
                            Widget = "&Widget=p4_1"
                        End If
                    End If

                    If productCode.ToLower = "zfreemoney" Then
                        '  HttpContext.Current.Response.Redirect("http://www.digi-store.net/pay/initPayment.ashx?PaymentMethod=" & paymentMethod & "&ProductCode=" & productCode & "&CustomerID=" & CustomerID & "&LoginName=" & loginName & "&promoCode=" & promoCode & "&CustomReferrer=" & CustomReferrer & "&downloadURL=" & oURL & "&amount=" & amount & "&SalesSiteID=" & SalesSiteID & "&type=" & paymentProviderType)

                    End If
                    If paymentMethod.ToLower = "paypal" Then
                        Dim url As String = "http://www.digi-store.net/pay/initPayment.ashx?PaymentMethod=" & paymentMethod & "&ProductCode=" & productCode & "&CustomerID=" & CustomerID & "&LoginName=" & loginName & "&promoCode=" & promoCode & "&CustomReferrer=" & CustomReferrer & "&downloadURL=" & oURL & "&amount=" & amount & "&SalesSiteID=" & SalesSiteID & "&type=" & paymentProviderType
                        HttpContext.Current.Response.Redirect(url)

                    End If
                    If flag Then
                        Dim url As String = "http://www.paymix.net/pay/loader.aspx?PaymentMethod=" & paymentMethod & "&ProductCode=" & productCode & "&CustomerID=" & CustomerID & "&LoginName=" & loginName & "&promoCode=" & promoCode & "&CustomReferrer=" & CustomReferrer & "&downloadURL=" & oURL & "&amount=" & amount & "&SalesSiteID=" & SalesSiteID & "&type=" & paymentProviderType & Widget
                        HttpContext.Current.Response.Redirect(url)

                    End If
                    'If paymentMethod.ToLower = "epoch" Then
                    '    Dim postURL As String = "https://www.goomena.com/pay/initPayment.ashx"

                    '    ' randomly send to dating-deals
                    '    Dim rnd As New Random(Date.UtcNow.Millisecond)
                    '    Dim rndVal = rnd.Next(100)
                    '    If (rndVal > 80) Then
                    '        postURL = "http://www.dating-deals.com/pay/initPayment.ashx"
                    '    End If

                    '    Dim url2 As String = postURL & "?PaymentMethod=" & paymentMethod & "&ProductCode=" & productCode & "&CustomerID=" & CustomerID & "&LoginName=" & loginName & "&promoCode=" & promoCode & "&CustomReferrer=" & CustomReferrer & "&downloadURL=" & oURL & "&amount=" & amount & "&SalesSiteID=" & SalesSiteID & "&type=" & paymentProviderType & "" & Widget & qsInfo
                    '    HttpContext.Current.Response.Redirect(url2)
                    'End If
                    Dim url1 As String = "http://www.soundsoft.com/pay/initPayment.ashx?PaymentMethod=" & paymentMethod & "&ProductCode=" & productCode & "&CustomerID=" & CustomerID & "&LoginName=" & loginName & "&promoCode=" & promoCode & "&CustomReferrer=" & CustomReferrer & "&downloadURL=" & oURL & "&amount=" & amount & "&SalesSiteID=" & SalesSiteID & "" & Widget
                    HttpContext.Current.Response.Redirect(url1)
                End If

                'HttpContext.Current.Response.Redirect("http://localhost:15837/initPayment.ashx?PaymentMethod=" & paymentMethod & "&ProductCode=" & productCode & "&CustomerID=" & CustomerID & "&LoginName=" & loginName & "&promoCode=" & promoCode & "&CustomReferrer=" & CustomReferrer & "&downloadURL=" & oURL & "&amount=" & amount & "&SalesSiteID=" & SalesSiteID & "")
            End Try


        End If

    End Sub

    Public Shared Sub Epoch_Payment(ByVal amount As String,
                           ByVal type As String,
                           ByVal quantity As String,
                           ByVal loginName As String,
                           ByVal CustomerID As Integer,
                           ByVal CustomReferrer As String,
                           ByVal oURL As String,
                           ByVal paymentMethod As String,
                           ByVal promoCode As String,
                           ByVal SalesSiteID As Integer,
                           ByVal paymentProviderType As String,
                           ByVal productCode As String)

        Dim camcharge As String = ""
        'If (paymentProviderType = "CC" AndAlso
        '    clsConfigValues.Get__autorebill_for_customers().Contains(CustomerID)) Then
        '''''''''''''''''''''''
        '' autorebill functionality
        ''  295=savvas, 269=savva82, 32128=dooma, 
        ''  4534=jim2000, 31492=dimkaravel, 23322=eyelove, 28974=john1976
        '' {295, 269, 32128, 4534, 31492, 23322, 28974}
        '''''''''''''''''''''''

        If (paymentProviderType = "CC") Then
            Dim ctx As New CMSDBDataContext(DataHelpers.ConnectionString)
            Dim tran As EUS_CustomerTransaction = Nothing
            Try

                tran = (From itm In ctx.EUS_CustomerTransactions
                        Where itm.CustomerID = CustomerID
                        Order By itm.Date_Time Descending
                        Select itm).FirstOrDefault()


                If (tran IsNot Nothing) Then
                    If (tran.PaymentMethods = "CreditCard") Then
                        paymentProviderType = tran.TransactionInfo
                        If (Not String.IsNullOrEmpty(tran.MemberID)) Then paymentProviderType = tran.MemberID
                        camcharge = "&camcharge=1"
                    End If
                End If

            Catch ex As Exception
            Finally
                ctx.Dispose()
            End Try
        End If

        Dim qsInfo As String = ""
        Dim prof As EUS_Profile = Nothing
        Try
            prof = DataHelpers.GetEUS_ProfileByProfileID(CustomerID)
            qsInfo = clsCustomer.GetProfileInfoParameters(prof)
        Catch
        End Try

        Dim postURL As String = ""
        postURL = "https://www.goomena.com/pay/initPayment.ashx"


        ' if it's euro then allow it to be sent from dating-deals
        If (Not productCode.EndsWith("TRY")) Then

            '' randomly send to dating-deals
            'Dim rnd As New Random(Date.UtcNow.Millisecond)
            'Dim rndVal = rnd.Next(100)
            'If (rndVal > 80) Then
            '    postURL = "http://www.dating-deals.com/pay/initPayment.ashx"
            '    paymentMethod = "epochzt"
            '    productCode = productCode & "zt"
            'End If

        End If


        Try
            Dim oRemotePost As New RemotePost()
            oRemotePost.Url = postURL
            oRemotePost.Add("PaymentMethod", paymentMethod)
            oRemotePost.Add("ProductCode", productCode)
            oRemotePost.Add("CustomerID", CustomerID)
            oRemotePost.Add("LoginName", loginName)
            oRemotePost.Add("promoCode", promoCode)
            oRemotePost.Add("CustomReferrer", CustomReferrer)
            oRemotePost.Add("downloadURL", oURL)
            oRemotePost.Add("Amount", amount)
            oRemotePost.Add("type", paymentProviderType)
            oRemotePost.Add("SalesSiteID", SalesSiteID)
            If (prof IsNot Nothing) Then
                Try
                    oRemotePost.Add("fn", prof.FirstName & " " & prof.LastName)
                    oRemotePost.Add("zip", prof.Zip)
                    oRemotePost.Add("cit", prof.City)
                    oRemotePost.Add("reg", prof.Region)
                    oRemotePost.Add("cntr", prof.Country)
                    oRemotePost.Add("eml", prof.eMail)
                    oRemotePost.Add("mob", prof.Cellular)
                Catch
                End Try
            End If
            oRemotePost.Post()
        Catch ex As Exception

            Dim Widget As String = ""

            Dim url2 As String = postURL & _
                "?PaymentMethod=" & paymentMethod & _
                "&ProductCode=" & productCode & _
                "&CustomerID=" & CustomerID & _
                "&LoginName=" & loginName & _
                "&promoCode=" & promoCode & _
                "&CustomReferrer=" & CustomReferrer & _
                "&downloadURL=" & oURL & _
                "&amount=" & amount & _
                "&SalesSiteID=" & SalesSiteID & _
                "&type=" & paymentProviderType & _
                "" & Widget & _
                qsInfo & _
                camcharge

            HttpContext.Current.Response.Redirect(url2)

        End Try


    End Sub


    Public Shared Sub RegNowPayment(productID As String, amount As String, payTransID As String)

        Dim pgUrl As String = "https://www.regnow.com/checkout/cart/new/42221-"

        'Dim productID As String = Request("itemName")
        'Dim amount As Decimal = Request("amount")
        'Dim payTransID As String = Request("TransID")

        Dim oRemotePost As New RemotePost()
        oRemotePost.Url = pgUrl & productID

        oRemotePost.Add("linkid", payTransID)
        oRemotePost.Add("currency", "EUR")
        oRemotePost.Post()

    End Sub


    Public Shared Sub PaymentWall_TR_Payment(ByVal amount As String,
                           ByVal type As String,
                           ByVal quantity As String,
                           ByVal loginName As String,
                           ByVal CustomerID As Integer,
                           ByVal CustomReferrer As String,
                           ByVal oURL As String,
                           ByVal paymentMethod As String,
                           ByVal promoCode As String,
                           ByVal SalesSiteID As Integer,
                           ByVal paymentProviderType As String,
                           ByVal productCode As String)

        If (String.IsNullOrEmpty(productCode)) Then productCode = "dd" & quantity & type
        If (Not String.IsNullOrEmpty(paymentProviderType)) Then paymentProviderType = paymentProviderType.ToUpper()

        Dim qsInfo As String = ""


        Dim prof As EUS_Profile = Nothing
        Try
            prof = DataHelpers.GetEUS_ProfileByProfileID(CustomerID)
            qsInfo = clsCustomer.GetProfileInfoParameters(prof)
        Catch
        End Try

        Try
            Dim oRemotePost As New RemotePost()
            Dim postURL As String = ""
            Dim flag As Boolean = True


            'postURL = "http://www.goomena.com/pay/initPayment.ashx?PaymentMethod=pw&ProductCode=dd1000Credits&CustomerID=295&LoginName=savvas&promoCode=&CustomReferrer=&downloadURL=&amount=0&SalesSiteID=5&type=PP&fn=Sav tEST&zip=14341&cit=Nea Filadelfeia&reg=Attica&cntr=GR&eml=savvas@000.gr&mob=&widget=p4_1"
            postURL = "http://www.dating-deals.com/pay/initPayment.ashx"
            paymentMethod = "pw"

            oRemotePost.Url = postURL
            oRemotePost.Add("PaymentMethod", paymentMethod)
            If (paymentProviderType Is Nothing) Then
                If (paymentMethod.ToUpper() = "PAYMENTWALL") Then
                    oRemotePost.Add("Widget", "p4_1")
                End If
            End If
            oRemotePost.Add("ProductCode", productCode)
            oRemotePost.Add("CustomerID", CustomerID)
            oRemotePost.Add("LoginName", loginName)
            oRemotePost.Add("promoCode", promoCode)
            oRemotePost.Add("CustomReferrer", CustomReferrer)
            oRemotePost.Add("downloadURL", oURL)
            oRemotePost.Add("Amount", amount)
            oRemotePost.Add("type", paymentProviderType)
            oRemotePost.Add("SalesSiteID", SalesSiteID)
            If (prof IsNot Nothing) Then
                Try
                    oRemotePost.Add("fn", prof.FirstName & " " & prof.LastName)
                    oRemotePost.Add("zip", prof.Zip)
                    oRemotePost.Add("cit", prof.City)
                    oRemotePost.Add("reg", prof.Region)
                    oRemotePost.Add("cntr", prof.Country)
                    oRemotePost.Add("eml", prof.eMail)
                    oRemotePost.Add("mob", prof.Cellular)
                Catch ex As Exception

                End Try
            End If
            oRemotePost.Post()
        Catch ex As Exception
            Dim Widget As String = ""
            If (paymentProviderType Is Nothing) Then
                If (paymentMethod.ToUpper() = "PAYMENTWALL" OrElse paymentMethod.ToUpper() = "PW") Then
                    Widget = "&Widget=p4_1"
                End If
            End If


            'postURL = "http://www.dating-deals.com/pay/initPayment.ashx?PaymentMethod=pw&ProductCode=dd1000Credits&CustomerID=295&LoginName=savvas&promoCode=&CustomReferrer=&downloadURL=&amount=0&SalesSiteID=5&type=PP&fn=Sav tEST&zip=14341&cit=Nea Filadelfeia&reg=Attica&cntr=GR&eml=savvas@000.gr&mob=&widget=p4_1"
            Dim postURL As String = "http://www.dating-deals.com/pay/initPayment.ashx"
            postURL = postURL & _
                "?PaymentMethod=" & HttpUtility.UrlEncode(paymentMethod) & _
                "&ProductCode=" & HttpUtility.UrlEncode(productCode) & _
                "&CustomerID=" & CustomerID & _
                "&LoginName=" & HttpUtility.UrlEncode(loginName) & _
                "&promoCode=" & HttpUtility.UrlEncode(promoCode) & _
                "&CustomReferrer=" & HttpUtility.UrlEncode(CustomReferrer) & _
                "&downloadURL=" & oURL & _
                "&amount=" & amount & _
                "&SalesSiteID=" & SalesSiteID & _
                "&type=" & paymentProviderType & _
                qsInfo & _
                Widget

            'Notify_SentToPaymentWall(postURL)
            HttpContext.Current.Response.Redirect(postURL)
        End Try
    End Sub

    Shared Function ResellerAddFunds(Money As Double, CustomerID As Integer) As Boolean
        Return True
    End Function


    Public Class AddTransactionParams
        Public Property Description As String
        Public Property Money As Double
        Public Property TransactionInfo As String
        Public Property p4 As String
        Public Property CreditsPurchased As Integer?
        Public Property REMOTE_ADDR As String
        Public Property HTTP_USER_AGENT As String
        Public Property HTTP_REFERER As String
        Public Property CustomerID As Integer
        Public Property CustomReferrer As String
        Public Property PayerEmail As String
        Public Property paymethod As PaymentMethods
        Public Property PromoCode As String
        Public Property TransactionTypeID As Integer
        Public Property productCode As String
        Public Property Currency As String
        Public Property cDataRecordIPN As clsDataRecordIPN
        Public Property freeUnlocks As Integer = 0
        Public Property useVoucherCredits As Boolean = False
        Public Property durationDays As Integer = 45
        Public Property voucherCredits As Integer = 0
        Public Property MemberID As String
    End Class



    Shared Function AddTransaction(Description As String,
                                   Money As Double,
                                   TransactionInfo As String,
                                   p4 As String,
                                   CreditsPurchased As Integer?,
                                   REMOTE_ADDR As String,
                                   HTTP_USER_AGENT As String,
                                   HTTP_REFERER As String,
                                   CustomerID As Integer,
                                   CustomReferrer As String,
                                   PayerEmail As String,
                                   paymethod As PaymentMethods,
                                   PromoCode As String,
                                   TransactionTypeID As Integer,
                                   productCode As String,
                                   Currency As String,
                                   cDataRecordIPN As clsDataRecordIPN,
                                   Optional freeUnlocks As Integer = 0,
                                   Optional useVoucherCredits As Boolean = False,
                                   Optional durationDays As Integer = 45,
                                   Optional voucherCredits As Integer = 0) As Integer



        Dim prms As New clsCustomer.AddTransactionParams()
        prms.Description = Description
        prms.Money = Money
        prms.TransactionInfo = TransactionInfo
        prms.p4 = p4
        prms.CreditsPurchased = CreditsPurchased
        prms.REMOTE_ADDR = REMOTE_ADDR
        prms.HTTP_USER_AGENT = HTTP_USER_AGENT
        prms.HTTP_REFERER = HTTP_REFERER
        prms.CustomerID = CustomerID
        prms.CustomReferrer = CustomReferrer
        prms.PayerEmail = PayerEmail
        prms.paymethod = paymethod
        prms.PromoCode = PromoCode
        prms.TransactionTypeID = TransactionTypeID
        prms.productCode = productCode
        prms.Currency = Currency
        prms.cDataRecordIPN = cDataRecordIPN
        prms.freeUnlocks = freeUnlocks
        prms.useVoucherCredits = useVoucherCredits
        prms.durationDays = durationDays
        prms.voucherCredits = voucherCredits

        Return AddTransaction(prms)
    End Function

    Shared Function AddTransaction(prms As AddTransactionParams) As Integer

        Dim Description As String = prms.Description
        Dim Money As Double = prms.Money
        Dim TransactionInfo As String = prms.TransactionInfo
        Dim p4 As String = prms.p4
        Dim CreditsPurchased As Integer? = prms.CreditsPurchased
        Dim REMOTE_ADDR As String = prms.REMOTE_ADDR
        Dim HTTP_USER_AGENT As String = prms.HTTP_USER_AGENT
        Dim HTTP_REFERER As String = prms.HTTP_REFERER
        Dim CustomerID As Integer = prms.CustomerID
        Dim CustomReferrer As String = prms.CustomReferrer
        Dim PayerEmail As String = prms.PayerEmail
        Dim paymethod As PaymentMethods = prms.paymethod
        Dim PromoCode As String = prms.PromoCode
        Dim TransactionTypeID As Integer = prms.TransactionTypeID
        Dim productCode As String = prms.productCode
        Dim Currency As String = prms.Currency
        Dim cDataRecordIPN As clsDataRecordIPN = prms.cDataRecordIPN
        Dim freeUnlocks As Integer = 0 = prms.freeUnlocks
        Dim useVoucherCredits As Boolean = prms.useVoucherCredits
        Dim durationDays As Integer = prms.durationDays
        Dim voucherCredits As Integer = prms.voucherCredits


        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Dim CustomerTransactionID As Integer = -1
        Dim credit As New EUS_CustomerCredit()

        Try
            ' the transction is from payment provider 
            'Not String.IsNullOrEmpty(TransactionInfo) AndAlso
            If (Not {"-1", "3333", "1111"}.Contains(TransactionInfo) AndAlso
                PromoCode <> "1000_CREDITS_GIFT") Then

                CustomerTransactionID = DataHelpers.EUS_CustomerTransaction_IsTransactionExists(CustomerID, TransactionInfo)
                If (CustomerTransactionID > -1) Then

                    clsMyMail.SendMail(ConfigurationManager.AppSettings("PaymentLogsEmail"),
                                           "Payment service (Payment exists)",
                                           "Payment already done for specified customer," & vbCrLf & _
                                           " TransactionID: " & TransactionInfo & vbCrLf & _
                                           " PayerEmail: " & PayerEmail & vbCrLf & _
                                           " CustomerID: " & CustomerID & vbCrLf & _
                                           " EUS_CustomerTransaction.CustomerTransactionID: " & CustomerTransactionID & vbCrLf & vbCrLf & _
                                           "Exiting...",
                                           False)
                    Return CustomerTransactionID

                End If
            End If


            If (Not String.IsNullOrEmpty(HTTP_REFERER)) Then
                If (HTTP_REFERER.Length > 1020) Then
                    HTTP_REFERER = HTTP_REFERER.Remove(1020)
                End If
            End If
            If (String.IsNullOrEmpty(Currency)) Then Currency = "EUR"

            Dim profile As DSMembers.EUS_ProfilesRow = DataHelpers.GetEUS_Profiles_ByProfileIDRow(CustomerID)
            Try

                If (productCode IsNot Nothing AndAlso productCode.ToUpper().EndsWith("LEK") AndAlso profile.Country = "TR") Then
                    productCode = productCode.Replace("LEK", "TRY")
                End If
            Catch
            End Try

            Dim payment_provider_value As Double = clsPricing.GetRandomPaymentProviderPercent(profile.Country)
            Dim payment_provider_ratio As Double = payment_provider_value / 100


            Dim transaction As New EUS_CustomerTransaction()
            transaction.CreditAmount = 0
            transaction.CustomerID = CustomerID
            transaction.CustomReferrer = CustomReferrer
            transaction.Date_Time = DateTime.UtcNow
            transaction.DebitReducePercent = payment_provider_value
            transaction.Description = Description
            transaction.IP = REMOTE_ADDR
            transaction.Referrer = HTTP_REFERER
            transaction.TransactionInfo = TransactionInfo
            transaction.TransactionType = TransactionTypeID
            transaction.Notes = ""
            transaction.ReceiptID = ""
            transaction.PaymentMethods = paymethod.ToString()
            transaction.MemberID = prms.MemberID


            Dim freeCredits As Integer
            If (freeUnlocks > 0) Then
                Dim creditsRequired As EUS_CreditsType = clsUserDoes.GetRequiredCredits(UnlockType.UNLOCK_CONVERSATION)
                freeCredits = freeUnlocks * creditsRequired.CreditsAmount
            End If


            Dim _Price As New EUS_Price()
            _Price.Currency = "EUR"

            If (Not String.IsNullOrEmpty(productCode)) Then
                _Price = clsPricing.GetPriceForProductCode_CopyResult(productCode)
                _Price.Credits = (_Price.Credits + freeCredits)

            ElseIf (Not String.IsNullOrEmpty(PromoCode)) Then

                _Price.Credits = CreditsPurchased
                _Price.duration = durationDays
                _Price.Amount = Money

            ElseIf (useVoucherCredits) Then
                _Price.Credits = voucherCredits + freeCredits
                _Price.duration = durationDays
                _Price.Amount = Money

            Else
                _Price = GetPriceFromCredits(CreditsPurchased)
                _Price.Credits = (CreditsPurchased + freeCredits)

            End If


            transaction.DebitAmountOriginal = Money
            transaction.DebitProductPrice = _Price.Amount

            ' currency defined by product package
            If (Currency = "EUR" AndAlso _Price.Currency <> "EUR") Then
                transaction.DebitAmount = Money - (Money * payment_provider_ratio)
                transaction.DebitAmountReceived = Money
            Else
                '' If (_Price.Currency = "EUR") Then
                transaction.DebitAmount = _Price.Amount - (_Price.Amount * payment_provider_ratio)
                transaction.DebitAmountReceived = _Price.Amount
            End If


            Try
                If (Currency <> "EUR") Then

                    ' convert to EURO
                    Dim cur As SYS_Currency = (From itm In _CMSDBDataContext.SYS_Currencies
                                               Where itm.CurrencyName = Currency
                                               Select itm).FirstOrDefault()

                    If (cur IsNot Nothing AndAlso cur.Rate > 0) Then
                        transaction.CurrencyRateToEuro = cur.Rate
                    End If


                    ' currecny defined by payment provider
                    If (_Price.Currency <> "EUR") Then

                        If (cur IsNot Nothing AndAlso cur.Rate > 0) Then
                            Dim newMoney = Money / cur.Rate
                            transaction.DebitAmount = newMoney - (newMoney * payment_provider_ratio)
                            transaction.DebitAmountReceived = newMoney
                        End If

                    End If

                End If

            Catch ex As Exception
                Try
                    Dim body As String = clsCustomer.GetMessageBody(Description, Money, TransactionInfo, p4,
                                                 CreditsPurchased, REMOTE_ADDR,
                                                 HTTP_USER_AGENT, HTTP_REFERER, CustomerID, CustomReferrer,
                                                 PayerEmail, paymethod, PromoCode, TransactionTypeID)

                    body = ex.ToString & vbCrLf & vbCrLf & body
                    'Dim toAddress As String = ConfigurationManager.AppSettings("gToEmail")
                    clsMyMail.SendMail(ConfigurationManager.AppSettings("PaymentLogsEmail"), "Payment service (CurrencyRateToEuro calc error, payment continues)", body, True)
                Catch
                End Try
            End Try


            credit.Credits = _Price.Credits
            credit.CreditstoBeExpired = _Price.Credits
            credit.CustomerId = CustomerID
            credit.DateTimeCreated = DateTime.UtcNow
            credit.DateTimeExpiration = DateTime.UtcNow.AddDays(_Price.duration)
            credit.ValidForDays = _Price.duration
            credit.EuroAmount = transaction.DebitAmount
            credit.Currency = Currency


            transaction.Currency = Currency

            transaction.IPNMetadata = GetIPNMetadata(prms)


            transaction.EUS_CustomerCredits.Add(credit)

            _CMSDBDataContext.EUS_CustomerTransactions.InsertOnSubmit(transaction)
            _CMSDBDataContext.EUS_CustomerCredits.InsertOnSubmit(credit)

            _CMSDBDataContext.SubmitChanges()


            If (credit.EuroAmount > 0) Then
                Dim REF_CRD2EURO_Rate As Double = 0
                If (credit.Credits > 0) Then REF_CRD2EURO_Rate = (credit.EuroAmount / credit.Credits)
                UpdateEUS_Profiles_REF_CRD2EURO_Rate(CustomerID, REF_CRD2EURO_Rate)
            End If


            CustomerTransactionID = transaction.CustomerTransactionID




            If (PromoCode = "REGISTRATION_GIFT") Then
                SendNotification_FreeRegistrationBonus(profile, Money, _Price, PayerEmail, CustomerID)

            ElseIf (PromoCode = "1000_CREDITS_GIFT") Then
                SendNotification_BonusCreditsGainedForPreviousBuy(profile, _Price, PayerEmail, CustomerID)

            ElseIf (Not String.IsNullOrEmpty(PromoCode)) Then
                SendNotification_BonusCreditsObtained(profile, Money, _Price, PayerEmail, CustomerID)

            Else
                SendNotification_PaymentSuccessful(profile, Money, _Price, PayerEmail, CustomerID)

            End If


        Catch ex As Exception
            Dim body As String = clsCustomer.GetMessageBody(Description, Money, TransactionInfo, p4,
                           CreditsPurchased, REMOTE_ADDR,
                           HTTP_USER_AGENT, HTTP_REFERER, CustomerID, CustomReferrer,
                           PayerEmail, paymethod, PromoCode, TransactionTypeID)

            body = ex.ToString & vbCrLf & vbCrLf & body
            'Dim toAddress As String = ConfigurationManager.AppSettings("gToEmail")
            clsMyMail.SendMail(ConfigurationManager.AppSettings("PaymentLogsEmail"), "Payment service", body, True)

            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try



        Try
            If (CustomerTransactionID > 0 AndAlso credit.EuroAmount > 0 AndAlso PromoCode <> "1000_CREDITS_GIFT") Then

                Dim dt As DataTable = DataHelpers.EUS_CustomerCredits_GetLastCreditsObtained(CustomerID, CustomerTransactionID)
                If (dt.Rows.Count > 0) Then
                    If (Not dt.Rows(0).IsNull("Credits") AndAlso dt.Rows(0)("Credits") = 10000) Then

                        ' add 1000 credits gift
                        ' TransactionTypeID: 4 = Bonus CustomerTransactionTypeID

                        Dim _prms1 As New clsCustomer.AddTransactionParams()
                        _prms1.Description = "Add 1000 credits gift (10000 credits was bought)"
                        _prms1.Money = 0
                        _prms1.TransactionInfo = "Add 1000 credits gift"
                        _prms1.CreditsPurchased = 1000
                        _prms1.REMOTE_ADDR = REMOTE_ADDR
                        _prms1.HTTP_USER_AGENT = HTTP_USER_AGENT
                        _prms1.HTTP_REFERER = HTTP_REFERER
                        _prms1.CustomerID = CustomerID
                        _prms1.CustomReferrer = CustomReferrer
                        _prms1.PayerEmail = PayerEmail
                        _prms1.paymethod = PaymentMethods.None
                        _prms1.PromoCode = "1000_CREDITS_GIFT"
                        _prms1.TransactionTypeID = 4
                        _prms1.productCode = Nothing
                        _prms1.Currency = "EUR"
                        _prms1.cDataRecordIPN = cDataRecordIPN
                        _prms1.freeUnlocks = 0
                        _prms1.useVoucherCredits = False
                        _prms1.durationDays = 45
                        _prms1.voucherCredits = 0

                        clsCustomer.AddTransaction(_prms1)
                    End If
                End If

            End If
        Catch ex As Exception
            Try

                Dim body As String = clsCustomer.GetMessageBody(Description, Money, TransactionInfo, p4,
                               CreditsPurchased, REMOTE_ADDR,
                               HTTP_USER_AGENT, HTTP_REFERER, CustomerID, CustomReferrer,
                               PayerEmail, paymethod, PromoCode, TransactionTypeID)

                body = ex.ToString & vbCrLf & vbCrLf & body
                clsMyMail.SendMail(ConfigurationManager.AppSettings("PaymentLogsEmail"), "Payment service (1000 credits gift error, payment continues)", body, True)
            Catch
            End Try
        End Try

        Return CustomerTransactionID
    End Function



    Public Shared Sub SendNotification_PaymentSuccessful(profile As DSMembers.EUS_ProfilesRow, Money As Double, _Price As EUS_Price, PayerEmail As String, CustomerID As Integer)

        Dim toAddress As String = ConfigurationManager.AppSettings("PaymentLogsEmail")
        Dim subject As String = "Payment successfull"

        ' default body content
        Dim body As String = <body><![CDATA[
Your payment at Goomena.com was successfull. <BR>You payed ###MONEYAMOUNT### and you gained an amount of ###CREDITS### credits. These credits are available during ###DURATION###. <BR><BR><BR>For any information don't hesitate to contact us at <A href="mailto:info@goomena.com">info@goomena.com</A>. <BR><BR>At your disposal, <BR>goomena.com website. 
]]></body>.Value

        Try
            Dim lagid As String = "US"
            If (Not profile.IsLAGIDNull()) Then
                lagid = profile.LAGID
            End If
            Dim o As New clsSiteLAG()
            subject = o.GetCustomStringFromPage(lagid, "GlobalStrings", "PaymentMessageSubject", DataHelpers.ConnectionString)
            body = o.GetCustomStringFromPage(lagid, "GlobalStrings", "PaymentMessageText", DataHelpers.ConnectionString)
            body = Regex.Replace(body, "###LOGINNAME###", profile.LoginName)
        Catch ex As Exception

        End Try


        If (body IsNot Nothing) Then
            body = body.Replace("###MONEYAMOUNT###", Money & "euro")
            body = body.Replace("###CREDITS###", _Price.Credits)

            Dim durStr As String = GetDurationString(_Price.duration)
            body = body.Replace("###DURATION###", durStr)
        End If

        Try
            clsMyMail.SendMail(PayerEmail, subject, body, True)
            clsMyMail.SendMail(toAddress, subject, body, True)
        Catch ex As Exception

        End Try

        clsUserDoes.SendMessageFromAdministrator(subject, body, CustomerID)
    End Sub


    Public Shared Sub SendNotification_BonusCreditsObtained(profile As DSMembers.EUS_ProfilesRow, Money As Double, _Price As EUS_Price, PayerEmail As String, CustomerID As Integer)

        Dim toAddress As String = ConfigurationManager.AppSettings("PaymentLogsEmail")
        Dim subject As String = "Payment successfull"

        ' default body content
        Dim body As String = <body><![CDATA[
Your payment at goomena.com was successfull. <BR>You payed ###MONEYAMOUNT### and you gained an amount of ###CREDITS### credits. These credits are available during ###DURATION###. <BR><BR><BR>For any information don't hesitate to contact us at <A href="mailto:info@goomena.com">info@goomena.com</A>. <BR><BR>At your disposal, <BR>goomena.com website. 
]]></body>.Value

        Try
            Dim lagid As String = "US"
            If (Not profile.IsLAGIDNull()) Then
                lagid = profile.LAGID
            End If
            Dim o As New clsSiteLAG()
            subject = o.GetCustomStringFromPage(lagid, "GlobalStrings", "BonusCreditsObtainedSubject", DataHelpers.ConnectionString)
            body = o.GetCustomStringFromPage(lagid, "GlobalStrings", "BonusCreditsObtainedText", DataHelpers.ConnectionString)
            body = Regex.Replace(body, "###LOGINNAME###", profile.LoginName)
        Catch ex As Exception
        End Try


        If (body IsNot Nothing) Then
            body = body.Replace("###MONEYAMOUNT###", Money & "euro")
            body = body.Replace("###CREDITS###", _Price.Credits)

            Dim durStr As String = GetDurationString(_Price.duration)
            body = body.Replace("###DURATION###", durStr)
        End If

        Try
            clsMyMail.SendMail(PayerEmail, subject, body, True)
            clsMyMail.SendMail(toAddress, subject, body, True)
        Catch ex As Exception

        End Try

        clsUserDoes.SendMessageFromAdministrator(subject, body, CustomerID)
    End Sub


    Public Shared Sub SendNotification_FreeRegistrationBonus(profile As DSMembers.EUS_ProfilesRow, Money As Double, _Price As EUS_Price, PayerEmail As String, CustomerID As Integer)

        Dim toAddress As String = ConfigurationManager.AppSettings("PaymentLogsEmail")
        Dim subject As String = "Payment successfull"

        ' default body content
        Dim body As String = ""

        Try
            Dim lagid As String = "US"
            If (Not profile.IsLAGIDNull()) Then
                lagid = profile.LAGID
            End If
            Dim o As New clsSiteLAG()
            subject = o.GetCustomStringFromPage(lagid, "GlobalStrings", "NotificationEmail_FreeRegisterationCreditsSubject", DataHelpers.ConnectionString)
            body = o.GetCustomStringFromPage(lagid, "GlobalStrings", "NotificationEmail_FreeRegisterationCreditsText", DataHelpers.ConnectionString)
            body = Regex.Replace(body, "###CREDITS_GIFT###", _Price.Credits.ToString())
        Catch
        End Try


        If (body IsNot Nothing) Then
            body = body.Replace("###MONEYAMOUNT###", Money & "euro")
            body = body.Replace("###CREDITS###", _Price.Credits)

            Dim durStr As String = GetDurationString(_Price.duration)
            body = body.Replace("###DURATION###", durStr)
        End If

        Try
            clsMyMail.SendMail(PayerEmail, subject, body, True)
        Catch
        End Try


        Try
            clsMyMail.SendMail(toAddress, subject, body, True)
        Catch
        End Try

        clsUserDoes.SendMessageFromAdministrator(subject, body, CustomerID)
    End Sub



    Public Shared Sub SendNotification_BonusCreditsGainedForPreviousBuy(profile As DSMembers.EUS_ProfilesRow, _Price As EUS_Price, PayerEmail As String, CustomerID As Integer)

        Dim toAddress As String = ConfigurationManager.AppSettings("PaymentLogsEmail")
        Dim subject As String = "Bonus Credits Gained"

        ' default body content
        Dim body As String = Nothing

        Try
            Dim lagid As String = "US"
            If (Not profile.IsLAGIDNull()) Then
                lagid = profile.LAGID
            End If
            Dim o As New clsSiteLAG()
            subject = o.GetCustomStringFromPage(lagid, "GlobalStrings", "NotificationEmail_BonusCreditsGainedForPreviousBuy_Subject", DataHelpers.ConnectionString)
            body = o.GetCustomStringFromPage(lagid, "GlobalStrings", "NotificationEmail_BonusCreditsGainedForPreviousBuy_Text", DataHelpers.ConnectionString)
        Catch
        End Try


        If (Not String.IsNullOrEmpty(body)) Then
            body = Regex.Replace(body, "###CREDITS_GIFT###", _Price.Credits.ToString())
            body = body.Replace("###MONEYAMOUNT###", 0 & "euro")
            body = body.Replace("###CREDITS###", _Price.Credits)

            Dim durStr As String = GetDurationString(_Price.duration)
            body = body.Replace("###DURATION###", durStr)

            clsUserDoes.SendMessageFromAdministrator(subject, body, CustomerID)
        End If
    End Sub


    Public Shared Function GetMessageBody(cDataRecordIPN As clsDataRecordIPN,
                                        HTTP_USER_AGENT As String,
                                        HTTP_REFERER As String,
                                        paymethod As PaymentMethods,
                                        productCode As String,
                                        MemberID As String)

        Dim body1 As String = <sql><![CDATA[
<table>
    <tr><td align="right"><strong>Login:</strong></td><td>[Login]</td></tr>
    <tr><td align="right"><strong>Money:</strong></td><td>[Money]</td></tr>
    <tr><td align="right"><strong>Currency:</strong></td><td>[Currency]</td></tr>
    <tr><td align="right"><strong>TransactionID:</strong></td><td>[TransactionID]</td></tr>
    <tr><td align="right"><strong>CreditsPurchased:</strong></td><td>[CreditsPurchased]</td></tr>
    <tr><td align="right"><strong>CustomerID:</strong></td><td>[CustomerID]</td></tr>
    <tr><td align="right"><strong>paymethod:</strong></td><td>[paymethod] ([paymethodCode])</td></tr>
    <tr><td align="right"><strong>TransactionTypeID:</strong></td><td>[TransactionTypeID]</td></tr>
    <tr><td align="right"><strong>CustomReferrer:</strong></td><td>[CustomReferrer]</td></tr>
    <tr><td align="right"><strong>Matched ProductCode:</strong></td><td>[ProductCode]</td></tr>
    <tr><td align="right"><strong>Matched MemberID:</strong></td><td>[MemberID]</td></tr>
    <tr><td colspan="2"><hr><h3>Goomena profile Info</h3></td></tr>
    <tr><td align="right"><strong>Country:</strong></td><td>[Country]</td></tr>
    <tr><td align="right"><strong>Region:</strong></td><td>[Region]</td></tr>
    <tr><td align="right"><strong>City:</strong></td><td>[City]</td></tr>
    <tr><td align="right"><strong>Zip:</strong></td><td>[Zip]</td></tr>
    <tr><td colspan="2"><hr><h3>Buyer Info</h3></td></tr>
    <tr><td align="right"><strong>PayerEmail:</strong></td><td>[BuyerInfoPayerEmail]</td></tr>
    <tr><td align="right"><strong>ResidenceCountry:</strong></td><td>[BuyerInfoResidenceCountry]</td></tr>
    <tr><td align="right"><strong>FirstName:</strong></td><td>[BuyerInfoFirstName]</td></tr>
    <tr><td align="right"><strong>LastName:</strong></td><td>[BuyerInfoLastName]</td></tr>
    <tr><td align="right"><strong>ContactPhone:</strong></td><td>[BuyerInfoContactPhone]</td></tr>
    <tr><td align="right"><strong>BusinessName:</strong></td><td>[BuyerInfoBusinessName]</td></tr>
    <tr><td align="right"><strong>Address:</strong></td><td>[BuyerInfoAddress]</td></tr>
    <tr><td align="right"><strong>PayerStatus:</strong></td><td>[BuyerInfoPayerStatus]</td></tr>
    <tr><td align="right"><strong>PayerID:</strong></td><td>[BuyerInfoPayerID]</td></tr>
    <tr><td colspan="2"><hr><h3>Metadata</h3></td></tr>
    <tr><td align="right"><strong>Remote Address:</strong></td><td>[REMOTE_ADDR]</td></tr>
    <tr><td align="right"><strong>User Agent:</strong></td><td>[HTTP_USER_AGENT]</td></tr>
    <tr><td align="right"><strong>HTTP Referer:</strong></td><td>[HTTP_REFERER]</td></tr>
    <tr><td colspan="2"><hr></td></tr>
    <tr><td align="right" font="small"><strong>Description:</strong></td><td>[Description]</td></tr>
    <tr><td colspan="2"><hr></td></tr>
    <tr><td align="right" font="small"><strong>Payment Started:</strong></td><td>[PaymentDateTime]</td></tr>
    <tr><td align="right" font="small"><strong>Payment Complete (Greece time):</strong></td><td>[PaymentCompleteDateTime]</td></tr>
    <tr><td align="right" font="small"><strong>Payment Complete (UTC time):</strong></td><td>[PaymentCompleteDateTimeUTC]</td></tr>
    <tr><td colspan="2"><hr><h3>Other info</h3></td></tr>
    <tr><td align="right" font="small"><strong>PayTransactionID:</strong></td><td>[PayTransactionID]</td></tr>
    <tr><td align="right" font="small"><strong>PayProviderID:</strong></td><td>[PayProviderID]</td></tr>
    <tr><td align="right" font="small"><strong>SalesSiteID:</strong></td><td>[SalesSiteID]</td></tr>
    <tr><td align="right" font="small"><strong>SalesSiteProductID:</strong></td><td>[SalesSiteProductID]</td></tr>
    <tr><td align="right" font="small"><strong>PromoCode:</strong></td><td>[PromoCode]</td></tr>
</table>
]]></sql>.Value

        body1 = body1.Replace("[Description]", IIf(cDataRecordIPN.SaleDescription IsNot Nothing, cDataRecordIPN.SaleDescription, "---"))
        body1 = body1.Replace("[Money]", cDataRecordIPN.PayProviderAmount)
        body1 = body1.Replace("[Currency]", cDataRecordIPN.Currency)
        body1 = body1.Replace("[TransactionID]", IIf(cDataRecordIPN.PayProviderTransactionID IsNot Nothing, cDataRecordIPN.PayProviderTransactionID, "---"))
        body1 = body1.Replace("[p4]", "---")
        body1 = body1.Replace("[CreditsPurchased]", cDataRecordIPN.SaleQuantity)
        body1 = body1.Replace("[CustomerID]", cDataRecordIPN.CustomerID)
        body1 = body1.Replace("[CustomReferrer]", IIf(cDataRecordIPN.CustomReferrer IsNot Nothing, cDataRecordIPN.CustomReferrer, "---"))
        body1 = body1.Replace("[paymethodCode]", CType(paymethod, Integer).ToString())
        body1 = body1.Replace("[paymethod]", paymethod.ToString())
        body1 = body1.Replace("[PromoCode]", IIf(cDataRecordIPN.PromoCode IsNot Nothing, cDataRecordIPN.PromoCode, "---"))
        body1 = body1.Replace("[TransactionTypeID]", cDataRecordIPN.TransactionTypeID)
        body1 = body1.Replace("[REMOTE_ADDR]", IIf(cDataRecordIPN.custopmerIP IsNot Nothing, cDataRecordIPN.custopmerIP, "---"))
        body1 = body1.Replace("[HTTP_USER_AGENT]", IIf(HTTP_USER_AGENT IsNot Nothing, HTTP_USER_AGENT, "---"))
        body1 = body1.Replace("[HTTP_REFERER]", IIf(HTTP_REFERER IsNot Nothing, HTTP_REFERER, "---"))
        body1 = body1.Replace("[PayTransactionID]", cDataRecordIPN.PayTransactionID)
        body1 = body1.Replace("[PayProviderID]", cDataRecordIPN.PayProviderID)
        body1 = body1.Replace("[SalesSiteID]", cDataRecordIPN.SalesSiteID)
        body1 = body1.Replace("[SalesSiteProductID]", cDataRecordIPN.SalesSiteProductID)

        body1 = body1.Replace("[ProductCode]", IIf(Not String.IsNullOrEmpty(productCode), productCode, "NOT MATCHED"))
        body1 = body1.Replace("[MemberID]", IIf(Not String.IsNullOrEmpty(MemberID), MemberID, "NOT MATCHED"))

        If (cDataRecordIPN.BuyerInfo IsNot Nothing) Then
            body1 = body1.Replace("[BuyerInfoAddress]", IIf(cDataRecordIPN.BuyerInfo.Address IsNot Nothing, cDataRecordIPN.BuyerInfo.Address, "---"))
            body1 = body1.Replace("[BuyerInfoBusinessName]", IIf(cDataRecordIPN.BuyerInfo.BusinessName IsNot Nothing, cDataRecordIPN.BuyerInfo.BusinessName, "---"))
            body1 = body1.Replace("[BuyerInfoContactPhone]", IIf(cDataRecordIPN.BuyerInfo.ContactPhone IsNot Nothing, cDataRecordIPN.BuyerInfo.ContactPhone, "---"))
            body1 = body1.Replace("[BuyerInfoFirstName]", IIf(cDataRecordIPN.BuyerInfo.FirstName IsNot Nothing, cDataRecordIPN.BuyerInfo.FirstName, "---"))
            body1 = body1.Replace("[BuyerInfoLastName]", IIf(cDataRecordIPN.BuyerInfo.LastName IsNot Nothing, cDataRecordIPN.BuyerInfo.LastName, "---"))
            body1 = body1.Replace("[BuyerInfoPayerEmail]", IIf(cDataRecordIPN.BuyerInfo.PayerEmail IsNot Nothing, cDataRecordIPN.BuyerInfo.PayerEmail, "---"))
            body1 = body1.Replace("[BuyerInfoPayerID]", IIf(cDataRecordIPN.BuyerInfo.PayerID IsNot Nothing, cDataRecordIPN.BuyerInfo.PayerID, "---"))
            body1 = body1.Replace("[BuyerInfoPayerStatus]", IIf(cDataRecordIPN.BuyerInfo.PayerStatus IsNot Nothing, cDataRecordIPN.BuyerInfo.PayerStatus, "---"))
            body1 = body1.Replace("[BuyerInfoResidenceCountry]", IIf(cDataRecordIPN.BuyerInfo.ResidenceCountry IsNot Nothing, cDataRecordIPN.BuyerInfo.ResidenceCountry, "---"))
        Else
            body1 = body1.Replace("[PayerEmail]", "---")
            body1 = body1.Replace("[BuyerInfoAddress]", "---")
            body1 = body1.Replace("[BuyerInfoBusinessName]", "---")
            body1 = body1.Replace("[BuyerInfoContactPhone]", "---")
            body1 = body1.Replace("[BuyerInfoFirstName]", "---")
            body1 = body1.Replace("[BuyerInfoLastName]", "---")
            body1 = body1.Replace("[BuyerInfoPayerEmail]", "---")
            body1 = body1.Replace("[BuyerInfoPayerID]", "---")
            body1 = body1.Replace("[BuyerInfoPayerStatus]", "---")
            body1 = body1.Replace("[BuyerInfoResidenceCountry]", "---")
        End If

        Try

            Dim UTCTime As DateTime = DateTime.UtcNow
            Dim GTBZone As TimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("GTB Standard Time")
            Dim GTBTime As DateTime = TimeZoneInfo.ConvertTime(UTCTime, TimeZoneInfo.Utc, GTBZone)
            Dim timeDiff As TimeSpan = GTBTime - UTCTime

            body1 = body1.Replace("[PaymentCompleteDateTimeUTC]", UTCTime.ToString("dd/MM/yyyy HH:mm:ss"))
            body1 = body1.Replace("[PaymentCompleteDateTime]", GTBTime.ToString("dd/MM/yyyy HH:mm:ss"))
            body1 = body1.Replace("[PaymentDateTime]", IIf(cDataRecordIPN.PaymentDateTime IsNot Nothing, cDataRecordIPN.PaymentDateTime, "---"))

        Catch
        End Try

        Try
            Dim cust As DSMembers.EUS_ProfilesRow = DataHelpers.GetEUS_Profiles_ByProfileIDRow(cDataRecordIPN.CustomerID)
            If (cust IsNot Nothing) Then
                body1 = body1.Replace("[Login]", cust.LoginName)

                Try
                    If (Not cust.IsCountryNull() AndAlso Not String.IsNullOrEmpty(cust.Country)) Then
                        body1 = body1.Replace("[Country]", ProfileHelper.GetCountryName(cust.Country))
                    Else
                        body1 = body1.Replace("[Country]", "Country not found in profile")
                    End If
                Catch ex As Exception
                End Try

                If (Not cust.Is_RegionNull() AndAlso Not String.IsNullOrEmpty(cust._Region)) Then
                    body1 = body1.Replace("[Region]", cust._Region)
                Else
                    body1 = body1.Replace("[Region]", "Region not found in profile")
                End If

                If (Not cust.IsCityNull() AndAlso Not String.IsNullOrEmpty(cust.City)) Then
                    body1 = body1.Replace("[City]", cust.City)
                Else
                    body1 = body1.Replace("[City]", "City not found in profile")
                End If

                If (Not cust.IsZipNull() AndAlso Not String.IsNullOrEmpty(cust.Zip)) Then
                    body1 = body1.Replace("[Zip]", cust.Zip)
                Else
                    body1 = body1.Replace("[Zip]", "Zip not found in profile")
                End If

            Else
                body1 = body1.Replace("[Login]", "Customer NOT FOUND")
                body1 = body1.Replace("[Country]", "Customer NOT FOUND")
                body1 = body1.Replace("[Region]", "Customer NOT FOUND")
                body1 = body1.Replace("[City]", "Customer NOT FOUND")
                body1 = body1.Replace("[Zip]", "Customer NOT FOUND")
            End If

        Catch ex As Exception
            body1 = body1.Replace("[Login]", ex.Message)
            body1 = body1.Replace("[Country]", "Exception")
            body1 = body1.Replace("[Region]", "Exception")
            body1 = body1.Replace("[City]", "Exception")
            body1 = body1.Replace("[Zip]", "Exception")
        End Try

        Return body1
    End Function


    Public Shared Function GetMessageBody(Description As String,
                                          Money As Double,
                                          TransactionID As String,
                                          p4 As String,
                                          CreditsPurchased As Integer,
                                          REMOTE_ADDR As String,
                                        HTTP_USER_AGENT As String,
                                        HTTP_REFERER As String,
                                        CustomerID As Integer,
                                        CustomReferrer As String,
                                        PayerEmail As Object,
                                        paymethod As PaymentMethods,
                                        PromoCode As Object,
                                        TransactionTypeID As Integer)

        Dim body1 As String = <sql><![CDATA[
<table>
    <tr><td align="right"><strong>Login:</strong></td><td>@Login</td></tr>
    <tr><td align="right"><strong>Money:</strong></td><td>@Money</td></tr>
    <tr><td align="right"><strong>TransactionID:</strong></td><td>@TransactionID</td></tr>
    <tr><td align="right"><strong>CreditsPurchased:</strong></td><td>@CreditsPurchased</td></tr>
    <tr><td align="right"><strong>CustomerID:</strong></td><td>@CustomerID</td></tr>
    <tr><td align="right"><strong>PayerEmail:</strong></td><td>@PayerEmail</td></tr>
    <tr><td align="right"><strong>paymethod:</strong></td><td>@paymethod (@paymethodCode)</td></tr>
    <tr><td align="right"><strong>PromoCode:</strong></td><td>@PromoCode</td></tr>
    <tr><td align="right"><strong>TransactionTypeID:</strong></td><td>@TransactionTypeID</td></tr>
    <tr><td align="right"><strong>CustomReferrer:</strong></td><td>@CustomReferrer</td></tr>
    <tr><td colspan="2"><hr></td></tr>
    <tr><td align="right"><strong>Country:</strong></td><td>[Country]</td></tr>
    <tr><td align="right"><strong>Region:</strong></td><td>[Region]</td></tr>
    <tr><td align="right"><strong>City:</strong></td><td>[City]</td></tr>
    <tr><td align="right"><strong>Zip:</strong></td><td>[Zip]</td></tr>
    <tr><td colspan="2"><hr></td></tr>
    <tr><td align="right"><strong>Remote Address:</strong></td><td>@REMOTE_ADDR</td></tr>
    <tr><td align="right"><strong>User Agent:</strong></td><td>@HTTP_USER_AGENT</td></tr>
    <tr><td align="right"><strong>HTTP Referer:</strong></td><td>@HTTP_REFERER</td></tr>
    <tr><td colspan="2"><hr></td></tr>
    <tr><td align="right" font="small"><strong>Description:</strong></td><td>@Description</td></tr>

    <!--<tr><td align="right" font="small"><strong>p4:</strong></td><td>@p4</td></tr>-->
</table>
]]></sql>.Value

        body1 = body1.Replace("@Description", IIf(Description IsNot Nothing, Description, ""))
        body1 = body1.Replace("@Money", Money)
        body1 = body1.Replace("@TransactionID", IIf(TransactionID IsNot Nothing, TransactionID, ""))
        body1 = body1.Replace("@p4", IIf(p4 IsNot Nothing, p4, ""))
        body1 = body1.Replace("@CreditsPurchased", CreditsPurchased)
        body1 = body1.Replace("@CustomerID", CustomerID)
        body1 = body1.Replace("@CustomReferrer", IIf(CustomReferrer IsNot Nothing, CustomReferrer, ""))
        body1 = body1.Replace("@PayerEmail", IIf(PayerEmail IsNot Nothing, PayerEmail, ""))
        body1 = body1.Replace("@paymethodCode", CType(paymethod, Integer).ToString())
        body1 = body1.Replace("@paymethod", paymethod.ToString())
        body1 = body1.Replace("@PromoCode", IIf(PromoCode IsNot Nothing, PromoCode, ""))
        body1 = body1.Replace("@TransactionTypeID", TransactionTypeID)
        body1 = body1.Replace("@REMOTE_ADDR", IIf(REMOTE_ADDR IsNot Nothing, REMOTE_ADDR, ""))
        body1 = body1.Replace("@HTTP_USER_AGENT", IIf(HTTP_USER_AGENT IsNot Nothing, HTTP_USER_AGENT, ""))
        body1 = body1.Replace("@HTTP_REFERER", IIf(HTTP_REFERER IsNot Nothing, HTTP_REFERER, ""))


        Try
            Dim cust As DSMembers.EUS_ProfilesRow = DataHelpers.GetEUS_Profiles_ByProfileIDRow(CustomerID)
            If (cust IsNot Nothing) Then
                body1 = body1.Replace("@Login", cust.LoginName)

                Try
                    If (Not cust.IsCountryNull() AndAlso Not String.IsNullOrEmpty(cust.Country)) Then
                        body1 = body1.Replace("[Country]", ProfileHelper.GetCountryName(cust.Country))
                    Else
                        body1 = body1.Replace("[Country]", "Country not found in profile")
                    End If
                Catch ex As Exception
                End Try

                If (Not cust.Is_RegionNull() AndAlso Not String.IsNullOrEmpty(cust._Region)) Then
                    body1 = body1.Replace("[Region]", cust._Region)
                Else
                    body1 = body1.Replace("[Region]", "Region not found in profile")
                End If

                If (Not cust.IsCityNull() AndAlso Not String.IsNullOrEmpty(cust.City)) Then
                    body1 = body1.Replace("[City]", cust.City)
                Else
                    body1 = body1.Replace("[City]", "City not found in profile")
                End If

                If (Not cust.IsZipNull() AndAlso Not String.IsNullOrEmpty(cust.Zip)) Then
                    body1 = body1.Replace("[Zip]", cust.Zip)
                Else
                    body1 = body1.Replace("[Zip]", "Zip not found in profile")
                End If

            Else
                body1 = body1.Replace("@Login", "Customer NOT FOUND")
                body1 = body1.Replace("[Country]", "Customer NOT FOUND")
                body1 = body1.Replace("[Region]", "Customer NOT FOUND")
                body1 = body1.Replace("[City]", "Customer NOT FOUND")
                body1 = body1.Replace("[Zip]", "Customer NOT FOUND")
            End If

        Catch ex As Exception
            body1 = body1.Replace("@Login", ex.Message)
            body1 = body1.Replace("[Country]", "Exception")
            body1 = body1.Replace("[Region]", "Exception")
            body1 = body1.Replace("[City]", "Exception")
            body1 = body1.Replace("[Zip]", "Exception")
        End Try

        'Dim body As String = "Description:" & vbCrLf & Description & vbCrLf & vbCrLf & _
        '    "Money:" & vbCrLf & Money & vbCrLf & vbCrLf & _
        '    "TransactionID:" & vbCrLf & TransactionID & vbCrLf & vbCrLf & _
        '    "p4:" & vbCrLf & p4 & vbCrLf & vbCrLf & _
        '    "CreditsPurchased:" & vbCrLf & CreditsPurchased & vbCrLf & vbCrLf & _
        '    "REMOTE_ADDR:" & vbCrLf & REMOTE_ADDR & vbCrLf & vbCrLf & _
        '    "HTTP_USER_AGENT:" & vbCrLf & HTTP_USER_AGENT & vbCrLf & vbCrLf & _
        '    "HTTP_REFERER:" & vbCrLf & HTTP_REFERER & vbCrLf & vbCrLf & _
        '    "CustomerID:" & vbCrLf & CustomerID & vbCrLf & vbCrLf & _
        '    "CustomReferrer:" & vbCrLf & CustomReferrer & vbCrLf & vbCrLf & _
        '    "PayerEmail:" & vbCrLf & PayerEmail & vbCrLf & vbCrLf & _
        '    "paymethod:" & vbCrLf & paymethod & vbCrLf & vbCrLf & _
        '    "PromoCode:" & vbCrLf & PromoCode & vbCrLf & vbCrLf & _
        '    "TransactionTypeID:" & vbCrLf & TransactionTypeID

        Return body1
    End Function


    Public Shared Function GetDurationString(duration As Integer, Optional LAGID As String = "US") As String
        Dim text As String = ""
        Dim o As New clsSiteLAG()

        If (duration = 120) Then
            text = "3 " & o.GetCustomStringFromPage(LAGID, "GlobalStrings", "Months", DataHelpers.ConnectionString)
        ElseIf (duration = 180) Then
            text = "6 " & o.GetCustomStringFromPage(LAGID, "GlobalStrings", "Months", DataHelpers.ConnectionString)
        ElseIf (duration = 270) Then
            text = "9 " & o.GetCustomStringFromPage(LAGID, "GlobalStrings", "Months", DataHelpers.ConnectionString)
        ElseIf (duration = 365) Then
            text = "1 " & o.GetCustomStringFromPage(LAGID, "GlobalStrings", "Year", DataHelpers.ConnectionString)
        ElseIf (duration = 548) Then
            text = "18 " & o.GetCustomStringFromPage(LAGID, "GlobalStrings", "Months", DataHelpers.ConnectionString)
        ElseIf (duration = 730) Then
            text = "2 " & o.GetCustomStringFromPage(LAGID, "GlobalStrings", "Years", DataHelpers.ConnectionString)
        Else
            text = duration & " " & o.GetCustomStringFromPage(LAGID, "GlobalStrings", "Days", DataHelpers.ConnectionString)
        End If

        Return text
    End Function


    Public Shared Function IsProfileActive(profileId As Integer) As Boolean
        Dim isactive As Boolean = DataHelpers.EUS_Profile_IsProfileActive(profileId)
        Return isactive
    End Function




    Public Shared Sub AddFreeRegistrationCredits_WithCheck(profileid As Integer, REMOTE_ADDR As String, HTTP_USER_AGENT As String, HTTP_REFERER As String)
        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try

            Dim profile = (From itm In _CMSDBDataContext.EUS_Profiles
                           Where
                               (itm.ProfileID = profileid OrElse itm.MirrorProfileID = profileid) AndAlso
                               (itm.Status = 4 OrElse itm.Status = 2 OrElse itm.Status = 1) AndAlso
                               (itm.StartingFreeCredits Is Nothing OrElse itm.StartingFreeCredits = False)
                           Select itm.GenderId, itm.eMail, itm.Country, itm.Region, itm.CustomReferrer, itm.StartingFreeCredits).FirstOrDefault()

            If ((profile Is Nothing) OrElse
                (Not ProfileHelper.IsMale(profile.GenderId))) Then Exit Sub


            Dim ctr As SYS_CountriesGEO = SYS_CountriesGEOHelper.SYS_CountriesGEO_GetForCountry(_CMSDBDataContext, profile.Country)
            Dim freeCredits As Boolean = clsNullable.NullTo(ctr.MembersBonusCredits) > 0

            If (freeCredits AndAlso Not String.IsNullOrEmpty(ctr.MembersBonusRegionsOrStates)) Then
                ctr.MembersBonusRegionsOrStates = ctr.MembersBonusRegionsOrStates.ToUpper()
                Dim _region As String = profile.Region.ToUpper()
                Dim regs As String() = ctr.MembersBonusRegionsOrStates.Split(vbCrLf)
                For cnt = 0 To regs.Length - 1
                    regs(cnt) = regs(cnt).Trim()
                Next
                Dim reg As String = (From itm In regs
                                     Where itm = _region
                                     Select itm).FirstOrDefault()

                If (reg IsNot Nothing) Then
                    freeCredits = True
                Else
                    freeCredits = False
                End If
            End If


            If (freeCredits) Then

                ' TransactionTypeID: 4 = Bonus CustomerTransactionTypeID



                clsCustomer.AddTransaction("Add free registration bonus credits",
                                            clsNullable.NullTo(ctr.MembersBonusCreditsAmount),
                                            "Free registration credits " & ctr.MembersBonusCredits,
                                            "",
                                            ctr.MembersBonusCredits,
                                            REMOTE_ADDR,
                                            HTTP_USER_AGENT,
                                            HTTP_REFERER,
                                            profileid,
                                            profile.CustomReferrer,
                                            profile.eMail,
                                            PaymentMethods.None,
                                            "REGISTRATION_GIFT",
                                            4,
                                            Nothing,
                                            "EUR",
                                            New clsDataRecordIPN(),
                                            0,
                                            False,
                                            45,
                                            0)

                DataHelpers.EUS_Profiles_Update_StartingFreeCredits(profileid)
            End If

        Catch ex As Exception
            Throw New System.Exception(ex.Message, ex)
        Finally
            _CMSDBDataContext.Dispose()
        End Try

    End Sub

    Public Shared Sub AddFreeRegistrationCredits(ByRef profile As DSMembers.EUS_ProfilesRow, REMOTE_ADDR As String, HTTP_USER_AGENT As String, HTTP_REFERER As String)

        Dim ctr As SYS_CountriesGEO = SYS_CountriesGEOHelper.SYS_CountriesGEO_GetForCountry(profile.Country)
        Dim freeCredits As Boolean = clsNullable.NullTo(ctr.MembersBonusCredits) > 0

        If (freeCredits AndAlso Not String.IsNullOrEmpty(ctr.MembersBonusRegionsOrStates)) Then
            ctr.MembersBonusRegionsOrStates = ctr.MembersBonusRegionsOrStates.ToUpper()
            Dim _region As String = profile._Region.ToUpper()
            Dim regs As String() = ctr.MembersBonusRegionsOrStates.Split(vbCrLf)
            For cnt = 0 To regs.Length - 1
                regs(cnt) = regs(cnt).Trim()
            Next
            Dim reg As String = (From itm In regs
                                 Where itm = _region
                                 Select itm).FirstOrDefault()

            If (reg IsNot Nothing) Then
                freeCredits = True
            Else
                freeCredits = False
            End If
        End If


        If (freeCredits) Then

            ' TransactionTypeID: 4 = Bonus CustomerTransactionTypeID
            clsCustomer.AddTransaction("Add free registration bonus credits",
                                        clsNullable.NullTo(ctr.MembersBonusCreditsAmount),
                                        "Free registration credits " & ctr.MembersBonusCredits,
                                        "",
                                        ctr.MembersBonusCredits,
                                        REMOTE_ADDR,
                                        HTTP_USER_AGENT,
                                        HTTP_REFERER,
                                        profile.ProfileID,
                                        profile.CustomReferrer,
                                        profile.eMail,
                                        PaymentMethods.None,
                                        "REGISTRATION_GIFT",
                                        4,
                                        Nothing,
                                        "EUR",
                                        New clsDataRecordIPN(),
                                        0,
                                        False,
                                        45,
                                        0)

            DataHelpers.EUS_Profiles_Update_StartingFreeCredits(profile.ProfileID)

            Try
                profile("StartingFreeCredits") = True
            Catch ex As Exception
            End Try
        End If

    End Sub



    '    Private Shared Function Notify_SentToPaymentWall(postURL As String)

    '        Try

    '            Dim __Subject As String
    '            Dim __Content As String


    '            If (HttpContext.Current.Request.Url.Host <> "localhost") Then
    '                Dim SessionVariables As clsSessionVariables = clsSessionVariables.GetCurrent()
    '                If (SessionVariables.MemberData.Country = "TR") Then
    '                    __Subject = "Loading Payment wall (Country:" & SessionVariables.MemberData.Country & ")"
    '                    __Content = <html><![CDATA[
    'Loading Payment wall (Country:[COUNTRY])
    'LoginName: [LOGINNAME]
    'Redirect to: [REDIR-URL]


    'URL: [URL]
    'IP: [IP]

    ']]></html>
    '                    __Content = __Content.Replace("[REDIR-URL]", postURL)
    '                    __Content = __Content.Replace("[COUNTRY]", SessionVariables.MemberData.Country)
    '                    __Content = __Content.Replace("[LOGINNAME]", SessionVariables.MemberData.LoginName)
    '                    __Content = __Content.Replace("[URL]", HttpContext.Current.Request.Url.ToString())
    '                    __Content = __Content.Replace("[IP]", HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"))



    '                    Dating.Server.Core.DLL.clsMyMail.SendMail(ConfigurationManager.AppSettings("PaymentLogsEmail"), __Subject, __Content, False)
    '                End If
    '            End If

    '        Catch
    '        End Try

    '    End Function

    '    Private Shared Function Notify_SentToEpoch(postURL As String)

    '        Try

    '            Dim __Subject As String
    '            Dim __Content As String


    '            If (HttpContext.Current.Request.Url.Host <> "localhost") Then
    '                Dim SessionVariables As clsSessionVariables = clsSessionVariables.GetCurrent()
    '                If (SessionVariables.MemberData.Country = "TR") Then
    '                    __Subject = "Loading Epoch (Country:" & SessionVariables.MemberData.Country & ")"
    '                    __Content = <html><![CDATA[
    'Loading Epoch (Country:[COUNTRY])
    'LoginName: [LOGINNAME]
    'Redirect to: [REDIR-URL]

    'URL: [URL]
    'IP: [IP]

    ']]></html>
    '                    __Content = __Content.Replace("[REDIR-URL]", postURL)
    '                    __Content = __Content.Replace("[COUNTRY]", SessionVariables.MemberData.Country)
    '                    __Content = __Content.Replace("[LOGINNAME]", SessionVariables.MemberData.LoginName)
    '                    __Content = __Content.Replace("[URL]", HttpContext.Current.Request.Url.ToString())
    '                    __Content = __Content.Replace("[IP]", HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"))



    '                    Dating.Server.Core.DLL.clsMyMail.SendMail(ConfigurationManager.AppSettings("PaymentLogsEmail"), __Subject, __Content, False)
    '                End If
    '            End If

    '        Catch
    '        End Try

    '    End Function

    Private Shared Function GetIPNMetadata(prms As AddTransactionParams) As String
        Dim IPNParams As String = Nothing
        Try

            Dim cDataRecordIPN As clsDataRecordIPN = prms.cDataRecordIPN


            Dim dblBr As String = vbCrLf & vbCrLf

            IPNParams = "" & _
                "Money: " & vbCrLf & cDataRecordIPN.PayProviderAmount & dblBr & _
                "Currency: " & vbCrLf & IIf(Not String.IsNullOrEmpty(cDataRecordIPN.Currency), cDataRecordIPN.Currency, "-") & dblBr & _
                "Credits: " & vbCrLf & cDataRecordIPN.SaleQuantity & dblBr & _
                "TransactionID: " & vbCrLf & IIf(Not String.IsNullOrEmpty(cDataRecordIPN.PayProviderTransactionID), cDataRecordIPN.PayProviderTransactionID, "-") & dblBr & _
                "MemberID: " & vbCrLf & IIf(Not String.IsNullOrEmpty(prms.MemberID), prms.MemberID, "-") & dblBr & _
                "TransactionTypeID: " & vbCrLf & cDataRecordIPN.TransactionTypeID & dblBr & _
                "CustomerID: " & vbCrLf & cDataRecordIPN.CustomerID & dblBr & _
                "CustomerIP: " & vbCrLf & IIf(Not String.IsNullOrEmpty(cDataRecordIPN.custopmerIP), cDataRecordIPN.custopmerIP, "-") & dblBr & _
                "SaleDescription: " & vbCrLf & IIf(Not String.IsNullOrEmpty(cDataRecordIPN.SaleDescription), cDataRecordIPN.SaleDescription, "-") & dblBr & _
                "CustomReferrer: " & vbCrLf & IIf(Not String.IsNullOrEmpty(cDataRecordIPN.CustomReferrer), cDataRecordIPN.CustomReferrer, "-") & dblBr & _
                "SalesSiteID: " & vbCrLf & cDataRecordIPN.SalesSiteID & dblBr & _
                "SalesSiteProductID: " & vbCrLf & cDataRecordIPN.SalesSiteProductID & dblBr & _
                "PayTransactionID: " & vbCrLf & IIf(Not String.IsNullOrEmpty(cDataRecordIPN.PayTransactionID), cDataRecordIPN.PayTransactionID, "-") & dblBr & _
                "PayProviderID: " & vbCrLf & cDataRecordIPN.PayProviderID & dblBr & _
                "ProductCode: " & vbCrLf & IIf(Not String.IsNullOrEmpty(prms.productCode), prms.productCode, "-") & dblBr & _
                "PromoCode: " & vbCrLf & IIf(Not String.IsNullOrEmpty(cDataRecordIPN.PromoCode), cDataRecordIPN.PromoCode, "-") & dblBr

            Try

                Dim UTCTime As DateTime = DateTime.UtcNow
                Dim GTBZone As TimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("GTB Standard Time")
                Dim GTBTime As DateTime = TimeZoneInfo.ConvertTime(UTCTime, TimeZoneInfo.Utc, GTBZone)
                Dim timeDiff As TimeSpan = GTBTime - UTCTime

                IPNParams = IPNParams & vbCrLf & _
                    "Payment Started: " & vbCrLf & IIf(Not String.IsNullOrEmpty(cDataRecordIPN.PaymentDateTime), cDataRecordIPN.PaymentDateTime, "-") & dblBr & _
                    "Payment Complete (Greece time): " & vbCrLf & GTBTime.ToString("dd/MM/yyyy HH:mm:ss") & dblBr & _
                    "Payment Complete (UTC time): " & vbCrLf & UTCTime.ToString("dd/MM/yyyy HH:mm:ss") & dblBr
            Catch
            End Try


            If (cDataRecordIPN.BuyerInfo IsNot Nothing) Then
                IPNParams = IPNParams & vbCrLf & _
                    "Buyer Info PayerEmail: " & vbCrLf & IIf(cDataRecordIPN.BuyerInfo.PayerEmail IsNot Nothing, cDataRecordIPN.BuyerInfo.PayerEmail, "-") & dblBr & _
                    "Buyer Info ResidenceCountry: " & vbCrLf & IIf(cDataRecordIPN.BuyerInfo.ResidenceCountry IsNot Nothing, cDataRecordIPN.BuyerInfo.ResidenceCountry, "-") & dblBr & _
                    "Buyer Info Address: " & vbCrLf & IIf(cDataRecordIPN.BuyerInfo.Address IsNot Nothing, cDataRecordIPN.BuyerInfo.Address, "-") & dblBr & _
                    "Buyer Info BusinessName: " & vbCrLf & IIf(cDataRecordIPN.BuyerInfo.BusinessName IsNot Nothing, cDataRecordIPN.BuyerInfo.BusinessName, "-") & dblBr & _
                    "Buyer Info ContactPhone: " & vbCrLf & IIf(cDataRecordIPN.BuyerInfo.ContactPhone IsNot Nothing, cDataRecordIPN.BuyerInfo.ContactPhone, "-") & dblBr & _
                    "Buyer Info FirstName: " & vbCrLf & IIf(cDataRecordIPN.BuyerInfo.FirstName IsNot Nothing, cDataRecordIPN.BuyerInfo.FirstName, "-") & dblBr & _
                    "Buyer Info LastName: " & vbCrLf & IIf(cDataRecordIPN.BuyerInfo.LastName IsNot Nothing, cDataRecordIPN.BuyerInfo.LastName, "-") & dblBr & _
                    "Buyer Info PayerID: " & vbCrLf & IIf(cDataRecordIPN.BuyerInfo.PayerID IsNot Nothing, cDataRecordIPN.BuyerInfo.PayerID, "-") & dblBr & _
                    "Buyer Info PayerStatus: " & vbCrLf & IIf(cDataRecordIPN.BuyerInfo.PayerStatus IsNot Nothing, cDataRecordIPN.BuyerInfo.PayerStatus, "-") & dblBr
            Else
                IPNParams = IPNParams & dblBr & _
                    "Buyer Info not available "
            End If

            If (Len(IPNParams) > 4000) Then IPNParams = IPNParams.Remove(4000)
        Catch
        End Try
        Return IPNParams
    End Function


    Private Shared Function GetProfileInfoParameters(ByRef prof As EUS_Profile) As String
        Dim qsInfo As String = ""
        Try
            qsInfo = qsInfo & "&fn="
            If (Not String.IsNullOrEmpty(prof.FirstName) AndAlso Not String.IsNullOrEmpty(prof.LastName)) Then
                qsInfo = qsInfo & HttpUtility.UrlEncode(prof.FirstName & " " & prof.LastName)
            ElseIf (Not String.IsNullOrEmpty(prof.FirstName)) Then
                qsInfo = qsInfo & HttpUtility.UrlEncode(prof.FirstName)
            ElseIf (Not String.IsNullOrEmpty(prof.LastName)) Then
                qsInfo = qsInfo & HttpUtility.UrlEncode(prof.LastName)
            End If

            qsInfo = qsInfo & "&zip="
            If (Not String.IsNullOrEmpty(prof.Zip)) Then qsInfo = qsInfo & HttpUtility.UrlEncode(prof.Zip)

            qsInfo = qsInfo & "&cit="
            If (Not String.IsNullOrEmpty(prof.City)) Then qsInfo = qsInfo & HttpUtility.UrlEncode(prof.City)

            qsInfo = qsInfo & "&reg="
            If (Not String.IsNullOrEmpty(prof.Region)) Then qsInfo = qsInfo & HttpUtility.UrlEncode(prof.Region)

            qsInfo = qsInfo & "&cntr="
            If (Not String.IsNullOrEmpty(prof.Country)) Then qsInfo = qsInfo & HttpUtility.UrlEncode(prof.Country)

            qsInfo = qsInfo & "&eml="
            If (Not String.IsNullOrEmpty(prof.eMail)) Then qsInfo = qsInfo & HttpUtility.UrlEncode(prof.eMail)

            qsInfo = qsInfo & "&mob="
            If (Not String.IsNullOrEmpty(prof.Cellular)) Then qsInfo = qsInfo & HttpUtility.UrlEncode(prof.Cellular)
        Catch
        End Try
        Return qsInfo
    End Function



End Class

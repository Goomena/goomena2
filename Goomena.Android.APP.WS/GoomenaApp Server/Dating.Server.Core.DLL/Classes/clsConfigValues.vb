﻿Public Class clsConfigValues

    Public Property auto_approve_new_profiles As String
    Public Property auto_approve_photos As String
    Public Property auto_approve_updating_profiles As String
    Public Property site_name As String
    Public Property image_thumb_url As String
    Public Property image_url As String
    Public Property image_thumb_server_path As String
    Public Property image_server_path As String
    Public Property view_profile_url As String
    Public Property zone_a As String
    Public Property zone_b As String
    Public Property zone_c As String
    Public Property members_online_minutes As String
    Public Property ip_to_map As String
    Public Property documents_server_path As String
    Public Property image_d150_server_path As String
    Public Property image_d150_url As String

    Public Property referrer_commission_conv_rate As Double
    Public Property referrer_commission_level_0 As Double
    Public Property referrer_commission_level_1 As Double
    Public Property referrer_commission_level_2 As Double
    Public Property referrer_commission_level_3 As Double
    Public Property referrer_commission_level_4 As Double
    Public Property referrer_commission_level_5 As Double
    Public Property referrer_commission_pending_percent As Double
    Public Property referrer_commission_pending_period As Integer

    Public Property ratio_eur_usd As Double
    Public Property ratio_eur_lek As Double

    Public Property credits_bonus_codes As Integer
    Public Property validate_email_address_normal As Boolean
    Public Property photos_max_approved As Integer


    Public Property admin_controler_phone1 As String
    Public Property admin_controler_phone2 As String
    Public Property admin_controler_phone3 As String
    Public Property admin_controler_phone4 As String
    Public Property admin_controler_phone5 As String
    Public Property admin_controler_email As String


    Public Sub New()
        Dim configValue As String = Nothing
        Dim _CMSDBDataContext As CMSDBDataContext = Nothing

        Try
            _CMSDBDataContext = New CMSDBDataContext(DataHelpers.ConnectionString)
            Dim access As List(Of Core.DLL.SYS_Config) = (From itm In _CMSDBDataContext.SYS_Configs
                                                 Select itm).ToList()

            For Each itm In access
                Select Case itm.ConfigName
                    Case "members_online_minutes" : members_online_minutes = itm.ConfigValue
                    Case "ip_to_map" : ip_to_map = itm.ConfigValue
                    Case "auto_approve_new_profiles" : auto_approve_new_profiles = itm.ConfigValue
                    Case "auto_approve_photos" : auto_approve_photos = itm.ConfigValue
                    Case "auto_approve_updating_profiles" : auto_approve_updating_profiles = itm.ConfigValue
                    Case "image_thumb_url" : image_thumb_url = itm.ConfigValue
                    Case "image_url" : image_url = itm.ConfigValue
                    Case "image_thumb_server_path" : image_thumb_server_path = itm.ConfigValue
                    Case "image_server_path" : image_server_path = itm.ConfigValue
                    Case "site_name" : site_name = itm.ConfigValue
                    Case "view_profile_url" : view_profile_url = itm.ConfigValue
                    Case "zone_a" : zone_a = itm.ConfigValue
                    Case "zone_b" : zone_b = itm.ConfigValue
                    Case "zone_c" : zone_c = itm.ConfigValue

                    Case "image_d150_server_path" : image_d150_server_path = itm.ConfigValue
                    Case "image_d150_url" : image_d150_url = itm.ConfigValue

                    Case "documents_server_path" : documents_server_path = itm.ConfigValue

                    Case "ratio_eur_usd" : ratio_eur_usd = itm.ConfigValueDouble
                    Case "ratio_eur_lek" : ratio_eur_lek = itm.ConfigValueDouble
                    Case "credits_bonus_codes" : credits_bonus_codes = itm.ConfigValueDouble
                    Case "photos_max_approved" : photos_max_approved = itm.ConfigValueDouble

                    Case "validate_email_address_normal" : validate_email_address_normal = itm.ConfigValueDouble

                    Case "referrer_commission_conv_rate"
                        If (itm.ConfigValueDouble.HasValue) Then
                            referrer_commission_conv_rate = itm.ConfigValueDouble.Value
                        End If

                    Case "referrer_commission_level_0"
                        If (itm.ConfigValueDouble.HasValue) Then
                            referrer_commission_level_0 = itm.ConfigValueDouble.Value
                        End If

                    Case "referrer_commission_level_1"
                        If (itm.ConfigValueDouble.HasValue) Then
                            referrer_commission_level_1 = itm.ConfigValueDouble.Value
                        End If

                    Case "referrer_commission_level_2"
                        If (itm.ConfigValueDouble.HasValue) Then
                            referrer_commission_level_2 = itm.ConfigValueDouble.Value
                        End If

                    Case "referrer_commission_level_3"
                        If (itm.ConfigValueDouble.HasValue) Then
                            referrer_commission_level_3 = itm.ConfigValueDouble.Value
                        End If

                    Case "referrer_commission_level_4"
                        If (itm.ConfigValueDouble.HasValue) Then
                            referrer_commission_level_4 = itm.ConfigValueDouble.Value
                        End If

                    Case "referrer_commission_level_5"
                        If (itm.ConfigValueDouble.HasValue) Then
                            referrer_commission_level_5 = itm.ConfigValueDouble.Value
                        End If

                    Case "referrer_commission_pending_percent"
                        If (itm.ConfigValueDouble.HasValue) Then
                            referrer_commission_pending_percent = itm.ConfigValueDouble.Value
                        End If

                    Case "referrer_commission_pending_period"
                        If (itm.ConfigValueDouble.HasValue) Then
                            referrer_commission_pending_period = itm.ConfigValueDouble.Value
                        End If


                    Case "admin_controler_phone1" : If (Not String.IsNullOrEmpty(itm.ConfigValue)) Then admin_controler_phone1 = itm.ConfigValue
                    Case "admin_controler_phone2" : If (Not String.IsNullOrEmpty(itm.ConfigValue)) Then admin_controler_phone2 = itm.ConfigValue
                    Case "admin_controler_phone3" : If (Not String.IsNullOrEmpty(itm.ConfigValue)) Then admin_controler_phone3 = itm.ConfigValue
                    Case "admin_controler_phone4" : If (Not String.IsNullOrEmpty(itm.ConfigValue)) Then admin_controler_phone4 = itm.ConfigValue
                    Case "admin_controler_phone5" : If (Not String.IsNullOrEmpty(itm.ConfigValue)) Then admin_controler_phone5 = itm.ConfigValue
                    Case "admin_controler_email" : If (Not String.IsNullOrEmpty(itm.ConfigValue)) Then admin_controler_email = itm.ConfigValue

                End Select
            Next

        Catch ex As Exception
            Throw
        Finally
            If (_CMSDBDataContext IsNot Nothing) Then _CMSDBDataContext.Dispose()
        End Try

    End Sub



    Public Shared Function GetSYS_ConfigValue(key As String) As String
        Dim configValue As String = Nothing
        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try
            Dim access As Core.DLL.SYS_Config = (From itm In _CMSDBDataContext.SYS_Configs
                                                 Where itm.ConfigName = key.ToLower()
                                                 Select itm).FirstOrDefault()

            configValue = access.ConfigValue
        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

        Return configValue
    End Function

    Public Shared Function GetSYS_ConfigValueDouble(ByVal key As String) As String
        Dim configValue As String = Nothing
        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try
            Dim access As Core.DLL.SYS_Config = (From itm In _CMSDBDataContext.SYS_Configs
                                                Where itm.ConfigName = key.ToLower()
                                                Select itm).FirstOrDefault()

            configValue = access.ConfigValueDouble
        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

        Return configValue
    End Function


    Public Shared Function GetSYS_ConfigValueDouble2(ByVal key As String) As Double
        Dim configValue As Double? = Nothing
        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try
            configValue = (From itm In _CMSDBDataContext.SYS_Configs
                            Where itm.ConfigName = key.ToLower()
                            Select itm.ConfigValueDouble).FirstOrDefault()
        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

        Return clsNullable.NullTo(configValue)
    End Function


    Private Shared __ratio_eur_lek As Double = -1
    Public Shared Function Get__ratio_eur_lek() As Double
        If (__ratio_eur_lek = -1) Then
            __ratio_eur_lek = clsConfigValues.GetSYS_ConfigValueDouble2("ratio_eur_lek")
        End If
        Return __ratio_eur_lek
    End Function


    Private Shared __ratio_eur_try As Double = -1
    Public Shared Function Get__ratio_eur_try() As Double
        If (__ratio_eur_try = -1) Then
            __ratio_eur_try = clsConfigValues.GetSYS_ConfigValueDouble2("ratio_eur_try")
        End If
        Return __ratio_eur_try
    End Function


    Private Shared __credits_bonus_codes As Integer = -1
    Public Shared Function Get__credits_bonus_codes() As Integer
        If (__credits_bonus_codes = -1) Then
            __credits_bonus_codes = clsConfigValues.GetSYS_ConfigValueDouble2("credits_bonus_codes")
        End If
        Return __credits_bonus_codes
    End Function


    Private Shared __validate_email_address_normal As Boolean?
    Public Shared Function Get__validate_email_address_normal() As Boolean
        If (Not __validate_email_address_normal.HasValue) Then
            __validate_email_address_normal = clsConfigValues.GetSYS_ConfigValueDouble("validate_email_address_normal")
        End If
        Return clsNullable.NullTo(__validate_email_address_normal)
    End Function


    Private Shared __photos_max_approved As Integer = -1
    Public Shared Function Get__photos_max_approved() As Integer
        If (__photos_max_approved = -1) Then
            __photos_max_approved = clsConfigValues.GetSYS_ConfigValueDouble2("photos_max_approved")
        End If
        Return __photos_max_approved
    End Function


    Private Shared __members_online_minutes As Integer = -1
    Public Shared Function Get__members_online_minutes() As Integer
        If (__members_online_minutes = -1) Then
            Dim tmp As String = clsConfigValues.GetSYS_ConfigValue("members_online_minutes")
            If (Not Integer.TryParse(tmp, __members_online_minutes)) Then __members_online_minutes = 20
        End If
        Return __members_online_minutes
    End Function


    'Private Shared __payment_provider_percents As Integer = -1
    'Public Shared Function Get__payment_provider_percents() As Integer
    '    If (__payment_provider_percents = -1) Then
    '        __payment_provider_percents = clsConfigValues.GetSYS_ConfigValueDouble("payment_provider_percents")
    '    End If
    '    Return __payment_provider_percents
    'End Function

    'Private Shared __payment_provider_percents_max As Integer = -1
    'Public Shared Function Get__payment_provider_percents_max() As Integer
    '    If (__payment_provider_percents_max = -1) Then
    '        __payment_provider_percents_max = clsConfigValues.GetSYS_ConfigValueDouble("payment_provider_percents_max")
    '    End If
    '    Return __payment_provider_percents_max
    'End Function


    'Public Shared Function GetRandomPaymentProviderPercent() As Integer
    '    Dim _min As Integer = clsConfigValues.Get__payment_provider_percents()
    '    Dim _max As Integer = clsConfigValues.Get__payment_provider_percents_max()

    '    Dim rnd As New Random(System.DateTime.Now.Millisecond)
    '    If _min > _max Then
    '        Dim t As Integer = _min
    '        _min = _max
    '        _max = t
    '    End If
    '    Dim payment_provider_ratio As Integer = rnd.Next(_min, _max)
    '    Return payment_provider_ratio
    'End Function


    Private Shared __message_check_forbidden_words_warning_threshold As Integer = -1
    Public Shared Function Get__message_check_forbidden_words_warning_threshold() As Integer
        If (__message_check_forbidden_words_warning_threshold = -1) Then
            __message_check_forbidden_words_warning_threshold = clsConfigValues.GetSYS_ConfigValueDouble2("message_check_forbidden_words_warning_threshold")
        End If
        Return __message_check_forbidden_words_warning_threshold
    End Function


    Private Shared __auto_approve_new_profiles As Boolean?
    Public Shared Function Get__auto_approve_new_profiles() As Boolean
        If (__auto_approve_new_profiles = -1) Then
            __auto_approve_new_profiles = False
            If (clsConfigValues.GetSYS_ConfigValue("auto_approve_new_profiles") = "1") Then __auto_approve_new_profiles = True
        End If
        Return __auto_approve_new_profiles
    End Function


    Private Shared __autorebill_for_customers As List(Of Integer)
    Public Shared Function Get__autorebill_for_customers() As List(Of Integer)
        If (__autorebill_for_customers Is Nothing) Then

            __autorebill_for_customers = New List(Of Integer)
            Try

                Dim tmp As String = clsConfigValues.GetSYS_ConfigValue("autorebill_for_customers")
                If (Not String.IsNullOrEmpty(tmp)) Then
                    Dim tmpArr As String() = tmp.Split({","c}, System.StringSplitOptions.RemoveEmptyEntries)
                    For c = 0 To tmpArr.Length - 1
                        Dim i As Integer
                        Integer.TryParse(tmpArr(c), i)
                        If (i > 1) Then
                            __autorebill_for_customers.Add(i)
                        End If
                    Next
                End If

            Catch
            End Try

        End If
        Return __autorebill_for_customers
    End Function



    Public Shared Sub ClearCache()
        __ratio_eur_lek = -1
        __credits_bonus_codes = -1
        __validate_email_address_normal = Nothing
        __photos_max_approved = -1
        __members_online_minutes = -1
        '__payment_provider_percents = -1
        '__payment_provider_percents_max = -1
        __autorebill_for_customers = Nothing
    End Sub

End Class

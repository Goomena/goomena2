﻿Imports System.Configuration
Imports System.Web
Imports System.Data
Imports System.Data.Sql
Imports System.Data.SqlClient

Imports Library.Public

Public Class clsSiteLAG

    'Public Class clsPageCustomsReturn
    '    Public UniqueKey As String
    '    Public Message As String
    'End Class

    Public Class clsPageBasicReturn
        Public PageTitle As String
        Public MetaDesciption As String
        Public MetaKeywords As String
        Public BodyHTM As String
        Public SitePageID As Integer
        Public MasterPage_FileName As String
        Public LastAccess As DateTime = Date.UtcNow

        Public ReadOnly Property BodyHTMSize As Integer
            Get
                If (BodyHTM Is Nothing) Then Return 0
                Return BodyHTM.Length
            End Get
        End Property
    End Class

    Dim m_ConnectionString As String
    Public ReadOnly Property ConnectionString As String
        Get
            Return m_ConnectionString
        End Get
    End Property


    Public Function Init(ByVal ServerName As String, ByVal Port As Integer, ByVal Database As String, ByVal UseNTSecurity As Boolean, ByVal Login As String, ByVal Password As String) As Boolean
        Try
            Dim sd As New Library.Public.SQLConnectionStringData
            sd.DB_Connect_Timeout = 120
            sd.DB_ServerName = ServerName
            sd.DB_SQLType = Library.Public.ProviderType.MsSQL
            sd.DB_DataBase = Database
            sd.DB_NTSecurity = UseNTSecurity
            sd.DB_Login = Login
            sd.DB_Password = Password
            m_ConnectionString = Library.Public.SQLFunctions.GetNETSQLConnectString(sd)
        Catch ex As Exception
            gER.ErrorMsgBox(ex, "")
        End Try

        Return Nothing
    End Function

    Public Function GetPageBasics(ByVal LagID As String, ByVal SitePageID As Long) As clsPageBasicReturn
        Dim cPageBasicReturn As New clsPageBasicReturn
        Try
            Dim cmd As SqlCommand
            'Dim szSQL As String = "SELECT * FROM SYS_SitePages WHERE SitePageID = " + SitePageID.ToString()
            Dim szSQL As String = <sql><![CDATA[
SELECT 
    SYS_SitePages.*,
    SYS_SiteMasterPages.[FileName]
FROM SYS_SitePages 
left join [SYS_SiteMasterPages] on SYS_SitePages.SYS_SiteMasterPageID=SYS_SiteMasterPages.SYS_SiteMasterPageID
WHERE SitePageID = @SitePageID
 ]]></sql>
            szSQL = szSQL.Replace("@SitePageID", SitePageID.ToString())
            cmd = New SqlCommand(szSQL, New SqlConnection(m_ConnectionString))
            cmd.Connection.Open()

            Dim rsRead As SqlDataReader = Nothing
            Dim cols As New List(Of String)
            Try
                rsRead = cmd.ExecuteReader
                Dim fieldsCount As Integer = (rsRead.FieldCount - 1)
                Dim cnt As Integer = 0
                For cnt = 0 To fieldsCount
                    cols.Add(rsRead.GetName(cnt).ToUpper)
                Next
            Catch
            End Try

            Try
                While rsRead.Read()

                    cPageBasicReturn.SitePageID = Library.Public.Common.CheckNULL(rsRead("SitePageID"), 0)

                    Dim FieldName As String = ("Title" & LagID)
                    If cols.Contains(FieldName.ToUpper) Then
                        Try
                            cPageBasicReturn.PageTitle = Common.CheckNULL(rsRead.Item(FieldName), "")
                        Catch
                        End Try
                        If "TITLEUS" <> FieldName.ToUpper AndAlso String.IsNullOrEmpty(cPageBasicReturn.PageTitle) Then
                            cPageBasicReturn.PageTitle = Common.CheckNULL(rsRead.Item("TitleUS"), "")
                        End If
                    End If


                    FieldName = ("metaDescription" & LagID)
                    If cols.Contains(FieldName.ToUpper) Then
                        Try
                            cPageBasicReturn.MetaDesciption = Common.CheckNULL(rsRead.Item(FieldName), "")
                        Catch
                        End Try
                        If "METADESCRIPTIONUS" <> FieldName.ToUpper AndAlso String.IsNullOrEmpty(cPageBasicReturn.MetaDesciption) Then
                            cPageBasicReturn.MetaDesciption = Common.CheckNULL(rsRead.Item("metaDescriptionUS"), "")
                        End If
                    End If


                    FieldName = ("Body" & LagID)
                    If cols.Contains(FieldName.ToUpper) Then
                        Try
                            cPageBasicReturn.BodyHTM = Common.CheckNULL(rsRead.Item(FieldName), "")
                        Catch
                        End Try
                        If "BODYUS" <> FieldName.ToUpper AndAlso String.IsNullOrEmpty(cPageBasicReturn.BodyHTM) Then
                            cPageBasicReturn.BodyHTM = Common.CheckNULL(rsRead.Item("BodyUS"), "")
                        End If
                    End If


                    FieldName = ("metaKeywords" & LagID)
                    If cols.Contains(FieldName.ToUpper) Then
                        Try
                            cPageBasicReturn.MetaKeywords = Common.CheckNULL(rsRead.Item(FieldName), "")
                        Catch
                        End Try
                        If "METAKEYWORDSUS" <> FieldName.ToUpper AndAlso String.IsNullOrEmpty(cPageBasicReturn.MetaKeywords) Then
                            cPageBasicReturn.MetaKeywords = Common.CheckNULL(rsRead.Item("metaKeywordsUS"), "")
                        End If
                    End If


                    FieldName = ("FileName")
                    If cols.Contains(FieldName.ToUpper) Then
                        Try
                            cPageBasicReturn.MasterPage_FileName = Common.CheckNULL(rsRead.Item(FieldName), "")
                        Catch
                        End Try
                    End If
                   
                End While
            Catch ex As Exception
                Throw
            Finally
                If (rsRead IsNot Nothing) Then rsRead.Close()
                cmd.Connection.Close()
                cmd.Connection.Dispose()
                cmd.Dispose()
            End Try


            Return cPageBasicReturn
        Catch ex As Exception
            gER.ErrorMsgBox(ex, "")
        End Try
        Return cPageBasicReturn
    End Function


    Public Function GetCustomStringFromPage(ByVal LagID As String, ByVal PageKey As String, ByVal LabelKey As String, ByVal connectionString As String) As String
        Dim text As String = ""


        Dim cPageBasicReturn As New clsPageBasicReturn
        Try
            m_ConnectionString = connectionString
            cPageBasicReturn = GetPageBasics(LagID, PageKey)
            text = GetCustomString(cPageBasicReturn.SitePageID, LagID, LabelKey, False)
        Catch ex As Exception
            gER.ErrorMsgBox(ex, "")
        End Try

        Return text
    End Function

    Public Function GetPageBasics(ByVal LagID As String, ByVal PageKey As String) As clsPageBasicReturn
        Dim cPageBasicReturn As New clsPageBasicReturn

        Dim cmd As SqlCommand = Nothing
        Dim rsRead As SqlDataReader = Nothing
        Try
            Dim szSQL As String = "EXEC [SYS_GetSitePages] @PageKey=N'" + PageKey + "'"
            '            Dim szSQL As String =
            '" IF EXISTS(SELECT * FROM SYS_SitePages WHERE PageFileName Like '~/" + PageKey + "') " & vbCrLf & _
            '" 	SELECT * FROM SYS_SitePages WHERE PageFileName Like '~/" + PageKey + "' " & vbCrLf & _
            '" ELSE  " & vbCrLf & _
            '" 	SELECT * FROM SYS_SitePages WHERE PageFileName Like '" + PageKey + "' "


            cmd = New SqlCommand(szSQL, New SqlConnection(m_ConnectionString))
            cmd.Connection.Open()

            rsRead = cmd.ExecuteReader
            Dim cols As New List(Of String)
            Try
                Dim fieldsCount As Integer = (rsRead.FieldCount - 1)
                Dim cnt As Integer = 0
                For cnt = 0 To fieldsCount
                    cols.Add(rsRead.GetName(cnt).ToUpper)
                Next
                'Do While (cnt <= fieldsCount)
                '    cols.Add(rsRead.GetName(cnt).ToUpper)
                '    cnt += 1
                'Loop
            Catch ex As Exception
            End Try

            While rsRead.Read()


                cPageBasicReturn.SitePageID = Library.Public.Common.CheckNULL(rsRead("SitePageID"), 0)


                Dim FieldName As String = ("Title" & LagID)
                If cols.Contains(FieldName.ToUpper()) Then
                    Try
                        cPageBasicReturn.PageTitle = Common.CheckNULL(rsRead.Item(FieldName), "")
                    Catch ex As Exception
                    End Try
                    If "TITLEUS" <> FieldName.ToUpper AndAlso String.IsNullOrEmpty(cPageBasicReturn.PageTitle) Then
                        cPageBasicReturn.PageTitle = Common.CheckNULL(rsRead.Item("TitleUS"), "")
                    End If
                End If


                FieldName = ("metaDescription" & LagID)
                If cols.Contains(FieldName.ToUpper) Then
                    Try
                        cPageBasicReturn.MetaDesciption = Common.CheckNULL(rsRead.Item(FieldName), "")
                    Catch ex As Exception
                    End Try
                    If "METADESCRIPTIONUS" <> FieldName.ToUpper AndAlso String.IsNullOrEmpty(cPageBasicReturn.MetaDesciption) Then
                        cPageBasicReturn.MetaDesciption = Common.CheckNULL(rsRead.Item("metaDescriptionUS"), "")
                    End If
                End If


                FieldName = ("Body" & LagID)
                If cols.Contains(FieldName.ToUpper) Then
                    Try
                        cPageBasicReturn.BodyHTM = Common.CheckNULL(rsRead.Item(FieldName), "")
                    Catch ex As Exception
                    End Try
                    If "BODYUS" <> FieldName.ToUpper AndAlso String.IsNullOrEmpty(cPageBasicReturn.BodyHTM) Then
                        cPageBasicReturn.BodyHTM = Common.CheckNULL(rsRead.Item("BodyUS"), "")
                    End If
                End If


                FieldName = ("metaKeywords" & LagID)
                If cols.Contains(FieldName.ToUpper) Then
                    Try
                        cPageBasicReturn.MetaKeywords = Common.CheckNULL(rsRead.Item(FieldName), "")
                    Catch
                    End Try
                    If "METAKEYWORDSUS" <> FieldName.ToUpper AndAlso String.IsNullOrEmpty(cPageBasicReturn.MetaKeywords) Then
                        cPageBasicReturn.MetaKeywords = Common.CheckNULL(rsRead.Item("metaKeywordsUS"), "")
                    End If
                End If

                '= Library.Public.Common.CheckNULL(rsRead("Body") & LagID, "")
                '                If Len(cPageBasicReturn.MetaDesciption) Then cPageBasicReturn.MetaDesciption = Library.Public.Common.CheckNULL(rsRead("BodyUS"), "")

                Exit While 'if we get two pages with this name only read one and exit
            End While

        Catch ex As Exception
            gER.ErrorMsgBox(ex, "")
        Finally
            If (rsRead IsNot Nothing) Then rsRead.Close()
            If (cmd IsNot Nothing) Then
                cmd.Connection.Close()
                cmd.Connection.Dispose()
                cmd.Dispose()
            End If
        End Try

        Return cPageBasicReturn
    End Function

    Public Function GetCustomString(ByVal SitePageID As Integer, ByVal LagID As String, ByVal LabelKey As String, NoCache As Boolean) As String

        Dim Ret As String = ""
        Dim cmd As SqlCommand = Nothing
        Dim rsRead As SqlDataReader = Nothing

        Try
            'Dim cmd As SqlCommand
            'Dim szSQL As String = "SELECT * FROM SYS_Messages WHERE SitePageID=" & SitePageID & " AND MessageKey Like '" & LabelKey & "'"
            Dim szSQL As String = "exec [GetCustomString] @SitePageID=@SitePageID,@MessageKey=@MessageKey"

            Try
                cmd = New SqlCommand(szSQL, New SqlConnection(m_ConnectionString))
                cmd.Parameters.AddWithValue("@SitePageID", SitePageID)
                cmd.Parameters.AddWithValue("@MessageKey", LabelKey)
                cmd.Connection.Open()

                rsRead = cmd.ExecuteReader()

                If rsRead.Read() Then
                    Dim FieldName As String = "" & LagID
                    Try
                        Ret = Library.Public.Common.CheckNULL(rsRead(FieldName), "")
                    Catch ex As Exception
                    End Try

                    If String.IsNullOrEmpty(Ret) Then
                        Ret = Common.CheckNULL(rsRead.Item("US"), "")
                    End If
                End If
            Catch ex As Exception

            Finally
                If (rsRead IsNot Nothing) Then rsRead.Close()
                If (cmd IsNot Nothing) Then
                    cmd.Connection.Close()
                    cmd.Connection.Dispose()
                    cmd.Dispose()
                End If
            End Try

        Catch ex As Exception
            gER.ErrorMsgBox(ex, "")
        End Try

        Return Ret
    End Function

    Public Function GetCustomStringDataTable(ByVal SitePageID As Integer, ByVal LabelKey As String) As DataTable

        Dim Ret As New DataTable()
        Dim rsRead As SqlDataReader = Nothing

        Try
            Dim szSQL As String = "exec [GetCustomString] @SitePageID=@SitePageID,@MessageKey=@MessageKey"
            Dim cmd As SqlCommand = New SqlCommand(szSQL, New SqlConnection(m_ConnectionString))
            cmd.Parameters.AddWithValue("@SitePageID", SitePageID)
            cmd.Parameters.AddWithValue("@MessageKey", LabelKey)
            Ret = DataHelpers.GetDataTable(cmd)

            'Dim cmd As SqlCommand
            'Dim szSQL As String = "SELECT * FROM SYS_Messages WHERE SitePageID=" & SitePageID & " AND MessageKey Like '" & LabelKey & "'"

            '    Try
            '        cmd = New SqlCommand(szSQL, New SqlConnection(m_ConnectionString))
            '        cmd.Parameters.AddWithValue("@SitePageID", SitePageID)
            '        cmd.Parameters.AddWithValue("@MessageKey", LabelKey)
            '        cmd.Connection.Open()

            '        rsRead = cmd.ExecuteReader()

            '        While rsRead.Read()
            '            Dim FieldName As String = "" & LagID
            '            Try
            '                Ret = Library.Public.Common.CheckNULL(rsRead(FieldName), "")
            '            Catch ex As Exception
            '            End Try

            '            If String.IsNullOrEmpty(Ret) Then
            '                Ret = Common.CheckNULL(rsRead.Item("US"), "")
            '            End If

            '        End While
            '    Catch ex As Exception

            '    Finally
            '        If (rsRead IsNot Nothing) Then rsRead.Close()
            '        If (cmd IsNot Nothing) Then
            '            cmd.Connection.Close()
            '            cmd.Connection.Dispose()
            '            cmd.Dispose()
            '        End If
            '    End Try

        Catch ex As Exception
            gER.ErrorMsgBox(ex, "")
        End Try

        Return Ret
    End Function


    Public Function GetCustomStrings(ByVal SitePageID As Integer, ByVal LagID As String) As DataTable
        Dim dt As New DataTable()

        Try
            Dim szSQL As String = "SELECT * FROM SYS_Messages WHERE SitePageID=" & SitePageID & " and not messagekey like '%***'"
            Dim cmd As SqlCommand = New SqlCommand(szSQL, New SqlConnection(m_ConnectionString))

            cmd.Connection.Open()
            Dim da As New SqlDataAdapter(cmd)
            Try
                da.Fill(dt)
            Catch ex As Exception
                gER.ErrorMsgBox(ex, "")
            Finally
                cmd.Connection.Close()
                cmd.Connection.Dispose()
                cmd.Dispose()
            End Try
        Catch ex As Exception
            gER.ErrorMsgBox(ex, "")
        End Try

        Return dt
    End Function


    'Public Function GetDataTable(ByVal szSQL As String) As DataTable
    '    Dim dt As New DataTable()

    '    Try
    '        'Dim szSQL As String = "SELECT * FROM SYS_Messages WHERE SitePageID=" & SitePageID
    '        Dim cmd As SqlCommand = New SqlCommand(szSQL, New SqlConnection(m_ConnectionString))

    '        cmd.Connection.Open()
    '        Dim da As New SqlDataAdapter(cmd)
    '        Try
    '            da.Fill(dt)
    '        Catch ex As Exception
    '            gER.ErrorMsgBox(ex, "")
    '        Finally
    '            cmd.Connection.Close()
    '            cmd.Dispose()
    '        End Try
    '    Catch ex As Exception
    '        gER.ErrorMsgBox(ex, "")
    '    End Try

    '    Return dt
    'End Function




    'Public Function RefreshFillAppStrings(ByVal pApp As HttpApplicationState, ByVal SiteID As Integer)
    '    Try
    '        Dim cmd As SqlCommand

    '        Dim szSQL As String = "SELECT * FROM SYS_LOC_MESSAGES WHERE [GROUP] Like '%site%'"
    '        cmd = New SqlCommand(szSQL, New SqlConnection(m_ConnectionString))
    '        Dim rsRead As SqlDataReader
    '        cmd.Connection.Open()
    '        rsRead = cmd.ExecuteReader()
    '        While rsRead.Read()
    '            Dim a As String
    '            a = rsRead("SiteID")
    '            Dim AppKey As String
    '            Dim PAGEKEY As String = Library.Public.Common.CheckNULL(rsRead("PageFileName"), "")
    '            Dim ITEMKEY As String = Library.Public.Common.CheckNULL(rsRead("PageFileName"), "")
    '            Dim TitleUS As String = Library.Public.Common.CheckNULL(rsRead("TitleUS"), "")

    '            AppKey = "LAG.US." & PAGEKEY & ".TITLE."


    '        End While
    '        rsRead.Close()
    '        cmd.Connection.Close()




    '        'Dim rs As New ClsMyADODB
    '        'If rs.Open(szSQL, ClsMyADODB.OpenMode.ReadOnlyMode) Then
    '        '    rs.MoveFirst()
    '        '    While Not rs.EOF
    '        '        Dim i As Integer
    '        '        For i = 0 To rs.rs.Fields.Count - 1
    '        '            Dim LAGID As String
    '        '            LAGID = CheckNULL(rs.GetFieldValue("LAGID", ""), "")
    '        '            Dim FieldLAG As String = rs.rs.Fields.Item(i).Name
    '        '            If FieldLAG <> "LAGID" And FieldLAG <> "GROUP" Then
    '        '                Dim RowString As String
    '        '                ' Dim FieldLAG As String = "US"
    '        '                RowString = CheckNULL(rs.GetFieldValue(FieldLAG, ""), "")
    '        '                If RowString.Length > 0 Then
    '        '                    Dim szAppKey As String
    '        '                    szAppKey = "LAG." & FieldLAG & "." & LAGID
    '        '                    pApp.Lock()
    '        '                    pApp(szAppKey) = RowString
    '        '                    pApp.UnLock()
    '        '                End If
    '        '            End If
    '        '        Next
    '        '        rs.MoveNext()
    '        '    End While
    '        'End If
    '        'rs.Close()
    '        'rs = Nothing

    '    Catch ex As Exception
    '        gER.ErrorMsgBox(ex, "")
    '    End Try
    'End Function



    ''Public Function FillAppStrings(ByVal pApp As System.Web.HttpApplication)
    ''    Try
    ''        Dim rs As New ClsMyADODB
    ''        Dim szSQL As String = "SELECT * FROM SYS_LOC_MESSAGES WHERE [GROUP] Like '%site%'"
    ''        If rs.Open(szSQL, ClsMyADODB.OpenMode.ReadOnlyMode) Then
    ''            rs.MoveFirst()
    ''            While Not rs.EOF
    ''                Dim i As Integer
    ''                For i = 0 To rs.rs.Fields.Count - 1
    ''                    Dim LAGID As String
    ''                    LAGID = CheckNULL(rs.GetFieldValue("LAGID", ""), "")
    ''                    Dim FieldLAG As String = rs.rs.Fields.Item(i).Name
    ''                    If FieldLAG <> "LAGID" And FieldLAG <> "GROUP" Then
    ''                        Dim RowString As String
    ''                        ' Dim FieldLAG As String = "US"
    ''                        RowString = CheckNULL(rs.GetFieldValue(FieldLAG, ""), "")
    ''                        If RowString.Length > 0 Then
    ''                            Dim szAppKey As String
    ''                            szAppKey = "LAG." & FieldLAG & "." & LAGID
    ''                            pApp.Application.Lock()
    ''                            pApp.Application.Add(szAppKey, RowString)
    ''                            pApp.Application.UnLock()
    ''                        End If
    ''                    End If
    ''                Next
    ''                rs.MoveNext()
    ''            End While
    ''        End If
    ''        rs.Close()
    ''        rs = Nothing
    ''        ReadStorageServerConfig()

    ''    Catch ex As Exception
    ''        ErrorMsgBox(ex, "")
    ''    End Try
    ''End Function

    'Public Function TranslateFormViewField(ByRef pApp As System.Web.HttpApplicationState, ByVal CurrLAG As String, ByVal mFormView As System.Web.UI.WebControls.FormView, ByVal szFieldName As String, ByVal szLAGID As String, ByVal szDefaultString As String)
    '    Try
    '        'Dim o As Object = mFormView.FindControl("lbLoginName")
    '        'If o IsNot Nothing Then
    '        '    Dim szValue = GetString(pApp, CurrLAG, szFieldName)
    '        '    o.text = szValue
    '        'End If
    '    Catch ex As Exception
    '    End Try
    'End Function

    'Public Function GetString(ByRef pApp As System.Web.HttpApplicationState, ByVal LagID As String, ByVal PageKey As String, ByVal LabelKey As String) As String
    '    Try
    '        'Dim szAppKey As String
    '        'szAppKey = "LAG." & CurrLAG & "." & LagID
    '        'Dim szString As String
    '        'pApp.Lock()
    '        'szString = pApp.Get(szAppKey)
    '        'pApp.UnLock()
    '        'If Len(szString) = 0 Then
    '        '    CurrLAG = "US"
    '        '    szAppKey = "LAG." & CurrLAG & "." & LagID
    '        '    pApp.Lock()
    '        '    szString = pApp.Get(szAppKey)
    '        '    pApp.UnLock()
    '        'End If
    '        'If IsNothing(szString) Then szString = ""
    '        'Return szString
    '    Catch ex As Exception
    '        gER.ErrorMsgBox(ex, "")
    '        Return ""
    '    End Try
    'End Function



End Class

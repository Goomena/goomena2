Imports DevExpress.Utils
Imports DevExpress.XtraEditors.Controls
Imports DevExpress.XtraGrid
Imports DevExpress.XtraGrid.Localization
Imports DevExpress.XtraBars.Localization
Imports DevExpress.XtraPrinting.Localization
Imports DevExpress.XtraReports.Localization
Imports DevExpress.XtraNavBar

Public Class clsLAGLocalization

    Dim LAGDBFileName As String
    Dim Cnn As New ADODB.Connection
    Dim rs As New ADODB.Recordset
    Dim ActiveLag As String = "US"
    Dim IsInit As Boolean = False

    Public Function Init(ByVal szFileName As String) As Boolean
        On Error GoTo er
        Init = False
        LAGDBFileName = szFileName
        If My.Computer.FileSystem.FileExists(LAGDBFileName) Then
            Cnn.Open(GetConnectString)
            rs.CursorLocation = ADODB.CursorLocationEnum.adUseClient
            rs.CursorType = ADODB.CursorTypeEnum.adOpenDynamic
            rs.LockType = ADODB.LockTypeEnum.adLockOptimistic '.adLockReadOnly
            rs.Open("SELECT * FROM LAG", Cnn)
            Init = True
            IsInit = True
        End If
        Exit Function
er:     gER.ErrorMsgBox("")
    End Function

    Public Function SetLag(ByVal szLagPrefix As String) As Boolean
        On Error GoTo er
        ActiveLag = szLagPrefix

        ' GridLocalizer.Active = New clsMyGridLocalizer
        ' Localizer.Active = New clsMyEditorsLocalizer

        ' BarLocalizer.Active = New clsMyBarLocalizer
        ' NavBarLocalizer.Active = New clsMyNavBarLocalizer
        ' PreviewLocalizer.Active = New clsMyPreviewLocalizer
        ' ReportLocalizer.Active = New clsMyReportLocalizer

        Exit Function
er:     gER.ErrorMsgBox("")
    End Function

    '
    'Returns the active lang 
    '***********************
    'last change 2010-11-23
    '***********************
    Public Function GetLangPrefix() As String
        If ActiveLag.Length > 0 Then
            Return ActiveLag
        Else
            Return ""
        End If
    End Function

    Private Function GetConnectString() As String
        GetConnectString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & LAGDBFileName & ";Persist Security Info=False"
    End Function

    Private Sub Class_Terminate()
        On Error GoTo er
        If rs.State = ADODB.ObjectStateEnum.adStateOpen Then rs.Close()
        If Cnn.State = ADODB.ObjectStateEnum.adStateOpen Then Cnn.Close()
        rs = Nothing
        Cnn = Nothing
        Exit Sub
er:     gER.ErrorMsgBox("")
    End Sub

    Public Sub SetLAGString(ByRef TextObject As Object, ByVal sKEY As String)
        Dim tmpString As String
        Try
            tmpString = GetString(sKEY, "")
            If Len(tmpString) Then
                TextObject = tmpString
            End If
        Catch ex As Exception
            gER.ErrorMsgBox("SetLAGString")
        End Try
    End Sub

    Public Function GetString(ByVal LagID As String, ByVal DefaultString As String) As String
        On Error GoTo er
        Dim tmpLag As String

        If Not IsInit Then Return DefaultString

        tmpLag = ActiveLag
        GetString = DefaultString
        If rs.State = ADODB.ObjectStateEnum.adStateOpen Then
            rs.MoveFirst()
            rs.Find("LAGID Like '" & LagID & "'")
            If rs.EOF Then Exit Function
GetUS:
            GetString = IIf(IsDBNull(rs.Fields.Item(tmpLag).Value), "", rs.Fields.Item(tmpLag).Value)

            ' Dim ttt As String
            ' ttt = IIf(IsDBNull(rs.Fields.Item("vv").Value), "", rs.Fields.Item("vv").Value)


            If Len(GetString) = 0 And tmpLag <> "US" Then tmpLag = "US" : GoTo GetUS
        End If

        Exit Function
er:     gER.ErrorMsgBox("")
        '  Resume Next
    End Function

    Public Function GetFormatingString(ByVal LagID As Long, ByVal DefaultString As String, _
    Optional ByVal Value1 As Object = "", _
    Optional ByVal Value2 As Object = "", _
    Optional ByVal Value3 As Object = "", _
    Optional ByVal Value4 As Object = "", _
    Optional ByVal Value5 As Object = "", _
    Optional ByVal Value6 As Object = "", _
    Optional ByVal Value7 As Object = "", _
    Optional ByVal Value8 As Object = "", _
    Optional ByVal Value9 As Object = "", _
    Optional ByVal Value10 As Object = "", _
    Optional ByVal Value11 As Object = "", _
    Optional ByVal Value12 As Object = "", _
    Optional ByVal Value13 As Object = "", _
    Optional ByVal Value14 As Object = "", _
    Optional ByVal Value15 As Object = "") As String

        On Error GoTo er
        Dim sz As String
        Dim szRet As String
        Dim i As Integer
        Dim tmpCh As String
        Dim nValue As Integer

        sz = GetString(LagID, DefaultString)
        nValue = 1
        For i = 1 To Len(sz)
            tmpCh = Mid(sz, i, 1)
            If tmpCh = "#" Then
                szRet = szRet & CStr(GetOptinalValue(nValue, Value1, Value2, Value3, Value4, Value5, Value6, Value7, Value8, Value9, Value10, Value11, Value12, Value13, Value14, Value15))
                nValue = nValue + 1
            Else
                szRet = szRet & tmpCh
            End If
        Next i
        Exit Function
er:     gER.ErrorMsgBox("")
    End Function

    Private Function GetOptinalValue(ByVal nValue As Object, _
    Optional ByVal Value1 As Object = "", _
        Optional ByVal Value2 As Object = "", _
        Optional ByVal Value3 As Object = "", _
        Optional ByVal Value4 As Object = "", _
        Optional ByVal Value5 As Object = "", _
        Optional ByVal Value6 As Object = "", _
        Optional ByVal Value7 As Object = "", _
        Optional ByVal Value8 As Object = "", _
        Optional ByVal Value9 As Object = "", _
        Optional ByVal Value10 As Object = "", _
        Optional ByVal Value11 As Object = "", _
        Optional ByVal Value12 As Object = "", _
        Optional ByVal Value13 As Object = "", _
        Optional ByVal Value14 As Object = "", _
        Optional ByVal Value15 As Object = "") As Object

        On Error GoTo er

        Select Case nValue
            Case 1 : GetOptinalValue = Value1
            Case 2 : GetOptinalValue = Value2
            Case 3 : GetOptinalValue = Value3
            Case 4 : GetOptinalValue = Value4
            Case 5 : GetOptinalValue = Value5
            Case 6 : GetOptinalValue = Value6
            Case 7 : GetOptinalValue = Value7
            Case 8 : GetOptinalValue = Value8
            Case 9 : GetOptinalValue = Value9
            Case 10 : GetOptinalValue = Value10
            Case 11 : GetOptinalValue = Value11
            Case 12 : GetOptinalValue = Value12
            Case 13 : GetOptinalValue = Value13
            Case 14 : GetOptinalValue = Value14
            Case 15 : GetOptinalValue = Value15
        End Select
        Exit Function
er:     gER.ErrorMsgBox("")
    End Function
End Class

﻿using Dating.Server.Core.DLL;
using Dating.Server.Datasets.DLL;
using Dating.Server.Datasets.DLL.DSMembersTableAdapters;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Web;
using System.Text;
using System.Web;


namespace GoomenaLib.Version4
{
    public class GoomenaServiceV4 : ServiceBase, IGoomenaServiceV4
    {


        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "register")]
        public string register(RegisterData data) {

            string returnMessage = "";
            DSMembers ds = new DSMembers();
            try {
                
                Dating.Server.Datasets.DLL.DSMembers.EUS_ProfilesRow profile = ds.EUS_Profiles.NewEUS_ProfilesRow();

                data.email = data.email.Trim();
                data.username = data.username.Trim();

                if (DataHelpers.EUS_Profile_CheckEmail(data.email)) {
                    returnMessage += "EmailAlready";
                }
                else if (DataHelpers.EUS_Profile_CheckLoginName(data.username)) {
                    returnMessage += "LoginNameAlready";
                }
                else {
                    clsCountryByIP country = null;
                    try {
                        country = clsCountryByIP.GetCountryCodeFromIP(getClientIP(), CurrentSettings.ConnectionString_GEODB);
                    }
                    catch (Exception) { }

                    using (EUS_ProfilesTableAdapter adapter = new EUS_ProfilesTableAdapter())
                    {
                        using (SqlConnection con=new SqlConnection(CurrentSettings.ConnectionString)){
                            adapter.Connection = con;
                            RegisterHelper helper = new RegisterHelper();

                            helper.cbAgreements = true;
                            helper.GenderId = Convert.ToInt32(data.gender);
                            helper.Email = data.email;
                            helper.Login = data.username;
                            helper.Password = data.password;
                            try
                            {
                                helper.Birthday = DateTime.Parse(data.birthday);
                            }
                            catch (System.FormatException)
                            {
                                throw new System.FormatException("Invalid date string (" + data.birthday + ")");
                            }
                            helper.Country = data.country; //to add country code here
                            helper.Region = data.region;
                            helper.City = data.city;
                            helper.Zip = getZipCode(data.country, data.region, data.city, data.lag);
                            helper.Lag = data.lag;

                            helper.Height = "-1";
                            helper.BodyType = "-1";
                            helper.EyeColor = "-1";
                            helper.HairClr = "-1";

                            if (country != null)
                                helper.GEO_COUNTRY_CODE = country.CountryCode;//getClientIP();
                            helper.REMOTE_ADDR = getClientIP();
                            helper.HTTP_USER_AGENT = "";
                            helper.HTTP_REFERER = "";
                            helper.CustomReferrer = "googleplay";
                            helper.LandingPage = "";
                            helper.SearchEngineKeywords = "";


                            helper.CreateNewProfile(ref profile);

                            ds.EUS_Profiles.AddEUS_ProfilesRow(profile);
                            adapter.Update(ds);
                        }
                    }
                   


                  

                    try {
                        if (!string.IsNullOrEmpty(data.fb_uid)) {
                            DataHelpers.UpdateEUS_Profiles_FacebookData(profile.ProfileID, data.fb_uid, data.fb_name, data.fb_username);
                        }
                        if (!string.IsNullOrEmpty(data.gp_uid)) {
                            DataHelpers.UpdateEUS_Profiles_GoogleUserData(profile.ProfileID, data.gp_uid, data.gp_name, data.gp_email);
                        }
                    }
                    catch (Exception ex) {
                        WebErrorSendEmail(ex, "Updating social data (handled)");
                    }


                    bool isAutoApproveOn = false;
                    try {
                        if (clsConfigValues.GetSYS_ConfigValue("auto_approve_new_profiles") == "1") {
                            isAutoApproveOn = true;
                        }

                        //string ConfigValue = clsConfigValues.GetSYS_ConfigValue("auto_approve_new_profiles");
                        //if (ConfigValue == "1")
                        //    isAutoApproveOn = true;
                        ////DataTable dt = DataHelpers.GetDataTable("select ISNULL(ConfigValue,0) as ConfigValue from SYS_Config where ConfigName='auto_approve_new_profiles'");
                        ////if ((dt.Rows.Count > 0 && (string)dt.Rows[0]["ConfigValue"] == "1")) {
                        ////    isAutoApproveOn = true;
                        ////}
                    }
                    catch (Exception ex) {
                        WebErrorSendEmail(ex, "GetSYS_ConfigValue->auto_approve_new_profiles (handled)");
                        if (CurrentSettings.AllowSendExceptions)
                            returnMessage += ex.ToString();
                    }


                    if (isAutoApproveOn) {
                        try {
                            AdminActions.ApproveProfile(ref ds, true);
                        }
                        catch (Exception ex) {
                            WebErrorSendEmail(ex, "ApproveProfile (handled)");
                            if (CurrentSettings.AllowSendExceptions)
                                returnMessage += ex.ToString();
                        }
                        try {
                            clsUserNotifications.SendEmailNotification_OnAdminApprove(profile, data.lag);
                        }
                        catch (Exception ex) {
                            WebErrorSendEmail(ex, "SendEmailNotification_OnAdminApprove (handled)");
                            if (CurrentSettings.AllowSendExceptions)
                                returnMessage += ex.ToString();
                        }
                    }

                    sendNotificationEmailToSupport(profile, isAutoApproveOn, country, data);
                    //LogRegisterData(data);
                }


                try {
                    if (profile.IsMirrorProfileIDNull()) profile.MirrorProfileID = 0;
                    clsProfilesPrivacySettings.CreateNew_EUS_ProfilesPrivacySetting(profile.ProfileID, profile.MirrorProfileID);
                }
                catch (Exception ex) {
                    WebErrorSendEmail(ex, "CreateNew_EUS_ProfilesPrivacySetting (handled)");
                    if (CurrentSettings.AllowSendExceptions)
                        returnMessage += ex.ToString();
                }


                //if (ProfileHelper.IsMale(Convert.ToInt32(data.gender)) && (data.country == "GR" )) {
                //    try {
                //        clsCustomer.AddFreeRegistrationCreditsGreek(ref profile, getClientIP(), "", "");
                //    }
                //    catch (Exception ex) {
                //        WebErrorSendEmail(ex, "AddFreeRegistrationCredits (handled). profile object is null:" + (profile == null).ToString());
                //        if (CurrentSettings.AllowSendExceptions)
                //            returnMessage += ex.ToString();
                //    }

                //}


                if (profile != null && profile.LoginName != null && profile.ProfileID > 1) {
                    DataHelpers.LogProfileAccess(Convert.ToInt32(profile.ProfileID), profile.LoginName, profile.Password, true, "Register", "Registration from device", "", getClientIP(), true, "Service4-register");
                }

            }
            catch (Exception ex) {
                WebErrorSendEmail(ex, "wcf-register");
                if (CurrentSettings.AllowSendExceptions)
                    returnMessage += ex.ToString();
            }
            finally { ds.Dispose(); }

            returnMessage += "ok";
            return returnMessage;
        }


        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "login_v2")]
        public UserProfile login_v2(LoginParameters lp) {
            UserProfile pInfo = new UserProfile();
            if (!string.IsNullOrEmpty(lp.fb_uid)) {

                LoginWithFacebookSuccessParams prms = ProfileHelper.PerfomReloginWithFacebookId(lp.fb_uid, lp.fb_name, lp.fb_username, lp.email);
                lp.email = prms.LoginName;
                lp.password = prms.Password;
                PerformLogin(lp, ref pInfo);

                if (pInfo.ProfileId != null &&
                    pInfo.UserName != null &&
                    prms.OnLoginSuccessSaveFBData) {
                    DataHelpers.UpdateEUS_Profiles_FacebookData(System.Convert.ToInt32(pInfo.ProfileId), lp.fb_uid, lp.fb_name, lp.fb_username);
                }
            }
            else {

                PerformLogin(lp, ref pInfo);

            }

            return pInfo;
        }

        public UserProfile PerformLogin(LoginParameters lp, ref UserProfile pInfo) {

            try {

                var pr = (from p in myContext.vw_EusProfile_Lights
                          where p.Password == lp.password && (p.eMail == lp.email || p.LoginName == lp.email) && p.IsMaster == true
                           && p.Status != (int)ProfileStatusEnum.Deleted && p.Status != (int)ProfileStatusEnum.DeletedByUser
                          select p).FirstOrDefault();

                //get client IP address
                OperationContext context = OperationContext.Current;
                MessageProperties messageProperties = context.IncomingMessageProperties;
                RemoteEndpointMessageProperty endpointProperty = (RemoteEndpointMessageProperty)messageProperties[RemoteEndpointMessageProperty.Name];

                if (pr != null) {
                    if (!ServiceBase.allowUserAction(pr.ProfileID, lp.logindetails)) {
                        throw new ActionNotAllowedException();
                    }


                    //login
                    DataHelpers.UpdateEUS_Profiles_LoginData(pr.ProfileID, endpointProperty.Address, getCountryCode(endpointProperty.Address), true, true);


                    //get profile photo
                    string fileName = "";
                    DSMembers.EUS_CustomerPhotosRow photo = DataHelpers.GetProfilesDefaultPhoto(Convert.ToInt32(pr.ProfileID));
                    if (photo != null)
                        fileName = photo.FileName;
                    //string path = ProfileHelper.GetProfileImageURL(Convert.ToInt32(q.ProfileID), fileName, Convert.ToInt32(q.GenderId), true, false);
                    string path = getImagePath(Convert.ToInt32(pr.ProfileID), fileName, Convert.ToInt32(pr.GenderId), true, false);

                    //update last activity
                    UpdateActivity(pr.ProfileID);

                    //get profile details
                    pInfo.FirstName = pr.FirstName;
                    pInfo.LastName = pr.LastName;
                    pInfo.ProfileId = Convert.ToString(pr.ProfileID);
                    pInfo.UserName = pr.LoginName;
                    pInfo.Password = pr.Password;
                    pInfo.ZipCode = pr.Zip;
                    pInfo.Latitude = clsNullable.NullTo(pr.latitude).ToString();
                    pInfo.Longtitude = clsNullable.NullTo(pr.longitude).ToString();
                    pInfo.GenderId = pr.GenderId.ToString();
                    pInfo.DateRegister = pr.DateTimeToRegister.Value.ToShortDateString();
                    if (pr.Birthday != null) pInfo.UserAge = Convert.ToString(ProfileHelper.GetCurrentAge(pr.Birthday.Value));
                    pInfo.UserCity = pr.City;
                    pInfo.UserRegion = pr.Region;
                    pInfo.ImageUrl = path;
                    pInfo.UserCountry = ProfileHelper.GetCountryName(pr.Country);

                    pInfo.ReferrerParentId = "";
                    if (pr.ReferrerParentId != null) {
                        pInfo.ReferrerParentId = Convert.ToString(pr.ReferrerParentId.Value);
                    }

                    if (pInfo.ProfileId != null && pInfo.UserName != null) {
                        DataHelpers.LogProfileAccess(Convert.ToInt32(pInfo.ProfileId), pInfo.UserName, lp.password, true, "Login", lp.logindetails, "", endpointProperty.Address, true, "Service4-PerformLogin");
                    }

                }
                else {
                    DataHelpers.LogProfileAccess(0, lp.email, lp.password, false, "Login", lp.logindetails, "", endpointProperty.Address, true, "Service4-PerformLogin");
                }
            }
            catch (Exception ex) {
                WebErrorSendEmail(ex, "");
            }
            finally {
                myContext.Dispose();
            }
            return pInfo;
        }


        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "userlastupdate/{id}")]
        public DateTime getUserProfileLastUpdate(string id) {
            DateTime? lastUpdate = null;
            using (myContext) {
                try {
                    //4 for Approved
                    lastUpdate = (from p in myContext.EUS_Profiles
                                  where p.ProfileID == Convert.ToInt32(id) && p.Status == (int)ProfileStatusEnum.Approved
                                  select p.LastUpdateProfileDateTime).Max();

                }
                catch (Exception ex) {
                    WebErrorSendEmail(ex, "");
                }
                finally {
                    myContext.Dispose();
                }
            }

            return (lastUpdate == null ? DateTime.MinValue : lastUpdate.Value);
        }


        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "userprofile/{id}")]
        public UserProfile getUserProfile(string id) {
            using (myContext) {
                //4 for Approved
                vw_EusProfile_Light pr = (from p in myContext.vw_EusProfile_Lights
                                          where p.ProfileID == Convert.ToInt32(id) && p.Status == (int)ProfileStatusEnum.Approved
                                          select p).FirstOrDefault();
                if (pr != null) {
                    string fileName = "";
                    DSMembers.EUS_CustomerPhotosRow photo = DataHelpers.GetProfilesDefaultPhoto(Convert.ToInt32(pr.ProfileID));
                    if (photo != null)
                        fileName = photo.FileName;
                    //string path = ProfileHelper.GetProfileImageURL(Convert.ToInt32(q.ProfileID), fileName, Convert.ToInt32(q.GenderId), true, false);
                    string path = getImagePath(Convert.ToInt32(pr.ProfileID), fileName, Convert.ToInt32(pr.GenderId), true, false);

                    DateTime LastActivityUTCDate = ServiceBase.getMinActivityDate();

                    UserProfile profile = new UserProfile();
                    profile.FirstName = pr.FirstName;
                    profile.LastName = pr.LastName;
                    profile.ProfileId = Convert.ToString(pr.ProfileID);
                    profile.FirstName = pr.FirstName;
                    profile.LastName = pr.LastName;
                    profile.DateRegister = pr.DateTimeToRegister.Value.ToShortDateString();
                    profile.GenderId = Convert.ToString(pr.GenderId);
                    profile.UserName = pr.LoginName;
                    profile.UserCity = pr.City;
                    profile.UserRegion = pr.Region;
                    profile.ZipCode = pr.Zip;
                    profile.Latitude = Convert.ToString(pr.latitude.Value);
                    profile.Longtitude = Convert.ToString(pr.longitude.Value);
                    profile.ImageUrl = path;
                    profile.UserCountry = ProfileHelper.GetCountryName(pr.Country);
                    profile.IsOnline = ServiceBase.IsProfileOnline(LastActivityUTCDate, pr).ToString();
                    profile.UserAge = ProfileHelper.GetCurrentAge(pr.Birthday.Value).ToString();
                    profile.UserCountry = ProfileHelper.GetCountryName(pr.Country);

                    //profile.IsVip = checkProfileVip(q.ProfileID) == true?"1":"0";


                    if (pr.ReferrerParentId != null) {
                        profile.ReferrerParentId = Convert.ToString(pr.ReferrerParentId.Value);
                    }
                    else {
                        profile.ReferrerParentId = "";
                    }

                    myContext.Dispose();

                    return profile;
                }
                else {
                    return null;
                }

            }
        }


        public UserProfile createProfile(EUS_Profile q, string path) {

            UserProfile profile = new UserProfile();
            profile.FirstName = q.FirstName;
            profile.LastName = q.LastName;
            profile.ProfileId = Convert.ToString(q.ProfileID);
            profile.FirstName = q.FirstName;
            profile.LastName = q.LastName;
            profile.DateRegister = q.DateTimeToRegister.Value.ToShortDateString();
            profile.GenderId = Convert.ToString(q.GenderId);
            profile.UserName = q.LoginName;
            profile.UserCity = q.City;
            profile.UserRegion = q.Region;
            profile.ZipCode = q.Zip;
            profile.Latitude = Convert.ToString(q.latitude.Value);
            profile.Longtitude = Convert.ToString(q.longitude);
            profile.ImageUrl = path;
            if (q.ReferrerParentId != null) {
                profile.ReferrerParentId = Convert.ToString(q.ReferrerParentId.Value);
            }
            else {
                profile.ReferrerParentId = "";
            }

            return profile;
        }

        public UserProfile createProfile(vw_EusProfile_Light q, string path) {

            UserProfile profile = new UserProfile();
            profile.FirstName = q.FirstName;
            profile.LastName = q.LastName;
            profile.ProfileId = Convert.ToString(q.ProfileID);
            profile.FirstName = q.FirstName;
            profile.LastName = q.LastName;
            profile.DateRegister = q.DateTimeToRegister.Value.ToShortDateString();
            profile.GenderId = Convert.ToString(q.GenderId);
            profile.UserName = q.LoginName;
            profile.UserCity = q.City;
            profile.UserRegion = q.Region;
            profile.ZipCode = q.Zip;
            profile.Latitude = Convert.ToString(q.latitude.Value);
            profile.Longtitude = Convert.ToString(q.longitude);
            profile.ImageUrl = path;
            if (q.ReferrerParentId != null) {
                profile.ReferrerParentId = Convert.ToString(q.ReferrerParentId.Value);
            }
            else {
                profile.ReferrerParentId = "";
            }

            return profile;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "userprofiledetails/{id}/{langId}")]
        public UserDetails getUserProfileDetails(string id, string langId) {
            UserDetails details = null;

            try {
                /*
                var pr = (from p in myContext.EUS_Profiles
                          where p.ProfileID == Convert.ToInt32(id)
                          select new
                          {
                              birth = p.Birthday,
                              bodytype = p.PersonalInfo_BodyTypeID,
                              hair = p.PersonalInfo_HairColorID,
                              eyes = p.PersonalInfo_EyeColorID,
                              edu = p.OtherDetails_EducationID,
                              child = p.PersonalInfo_ChildrenID,
                              nation = p.PersonalInfo_EthnicityID,
                              religion = p.PersonalInfo_ReligionID,
                              smoke = p.PersonalInfo_SmokingHabitID,
                              drink = p.PersonalInfo_DrinkingHabitID,
                              job = p.OtherDetails_Occupation,
                              income = p.OtherDetails_AnnualIncomeID,
                              family = p.LookingFor_RelationshipStatusID,
                              lookfor = 3 - p.GenderId,

                              dating_shorterm = p.LookingFor_TypeOfDating_ShortTermRelationship,
                              dating_married = p.LookingFor_TypeOfDating_MarriedDating,
                              dating_benefits = p.LookingFor_TypeOfDating_MutuallyBeneficialArrangements,
                              dating_longterm = p.LookingFor_TypeOfDating_LongTermRelationship,
                              dating_friensdhip = p.LookingFor_TypeOfDating_Friendship,
                              dating_adult = p.LookingFor_TypeOfDating_AdultDating_Casual,

                              aboutme_head = p.AboutMe_Heading,
                              aboutme_descr = p.AboutMe_DescribeYourself,
                              aboutme_date = p.AboutMe_DescribeAnIdealFirstDate,
                              availbale = Convert.ToString(getAvailableCredits(id, false)),
                              updated = p.LastUpdateProfileDateTime.Value.ToShortDateString()

                          }).FirstOrDefault();
                */

                EUS_Profile pr = (from p in myContext.EUS_Profiles
                                  where p.ProfileID == Convert.ToInt32(id)
                                  select p).FirstOrDefault();
                if (pr != null) {

                    details = new UserDetails();


                    clsCustomerAvailableCredits customerAvailableCredits = clsUserDoes.GetCustomerAvailableCredits(pr.ProfileID);

                    string fileName = "";
                    DSMembers.EUS_CustomerPhotosRow photo = DataHelpers.GetProfilesDefaultPhoto(Convert.ToInt32(pr.ProfileID));
                    if (photo != null)
                        fileName = photo.FileName;
                    //string path = ProfileHelper.GetProfileImageURL(Convert.ToInt32(q.ProfileID), fileName, Convert.ToInt32(q.GenderId), true, false);
                    string path = getImagePath(Convert.ToInt32(pr.ProfileID), fileName, Convert.ToInt32(pr.GenderId), true, false);

                    DateTime LastActivityUTCDate = ServiceBase.getMinActivityDate();

                    details.FirstName = pr.FirstName;
                    details.LastName = pr.LastName;
                    details.ProfileId = Convert.ToString(pr.ProfileID);
                    details.FirstName = pr.FirstName;
                    details.LastName = pr.LastName;
                    details.GenderId = Convert.ToString(pr.GenderId);
                    details.UserName = pr.LoginName;
                    details.UserCity = pr.City;
                    details.UserRegion = pr.Region;
                    details.ZipCode = pr.Zip;
                    details.ImageUrl = path;
                    details.UserCountry = ProfileHelper.GetCountryName(pr.Country);
                    details.IsOnline = ServiceBase.IsProfileOnline(LastActivityUTCDate, pr).ToString();
                    details.UserCountry = ProfileHelper.GetCountryName(pr.Country);

                    if (pr.DateTimeToRegister.HasValue) details.DateRegister = pr.DateTimeToRegister.Value.ToShortDateString();
                    if (pr.latitude.HasValue) details.Latitude = Convert.ToString(pr.latitude.Value);
                    if (pr.longitude.HasValue) details.Longtitude = Convert.ToString(pr.longitude.Value);
                    if (pr.Birthday.HasValue) details.UserAge = ProfileHelper.GetCurrentAge(pr.Birthday.Value).ToString();
                    if (pr.ReferrerParentId.HasValue) details.ReferrerParentId = Convert.ToString(pr.ReferrerParentId.Value);

                    //details.IsVip = checkProfileVip(q.ProfileID) == true?"1":"0";

                    details.IsVip = (
                        (pr.GenderId == 1) &&
                        ((pr.AvailableCredits >= ProfileHelper.Config_UNLOCK_MESSAGE_READ_CREDITS) || customerAvailableCredits.HasSubscription)
                        ).ToString();
                    details.IsVip = (details.IsVip == true.ToString() ? "1" : "0");




                    string datingInfo = "</br>";
                    for (int i = 0; i < Lists.gDSLists.EUS_LISTS_TypeOfDating.Rows.Count - 1; i++) {
                        switch ((TypeOfDatingEnum)Lists.gDSLists.EUS_LISTS_TypeOfDating.Rows[i]["TypeOfDatingId"]) {

                            case TypeOfDatingEnum.AdultDating_Casual:
                                if ((bool)pr.LookingFor_TypeOfDating_AdultDating_Casual) {
                                    datingInfo += (string)Lists.gDSLists.EUS_LISTS_TypeOfDating.Rows[i][langId] + "</br>";
                                }
                                break;

                            case TypeOfDatingEnum.Friendship:
                                if ((bool)pr.LookingFor_TypeOfDating_Friendship) {
                                    datingInfo += (string)Lists.gDSLists.EUS_LISTS_TypeOfDating.Rows[i][langId] + "</br>";

                                }
                                break;

                            case TypeOfDatingEnum.LongTermRelationship:
                                if ((bool)pr.LookingFor_TypeOfDating_LongTermRelationship) {
                                    datingInfo += (string)Lists.gDSLists.EUS_LISTS_TypeOfDating.Rows[i][langId] + "</br>";
                                }
                                break;

                            case TypeOfDatingEnum.MarriedDating:
                                if ((bool)pr.LookingFor_TypeOfDating_MarriedDating) {
                                    datingInfo += (string)Lists.gDSLists.EUS_LISTS_TypeOfDating.Rows[i][langId] + "</br>";
                                }
                                break;

                            case TypeOfDatingEnum.MutuallyBeneficialArrangements:
                                if ((bool)pr.LookingFor_TypeOfDating_MutuallyBeneficialArrangements) {
                                    datingInfo += (string)Lists.gDSLists.EUS_LISTS_TypeOfDating.Rows[i][langId] + "</br>";
                                }
                                break;

                            case TypeOfDatingEnum.ShortTermRelationship:
                                if ((bool)pr.LookingFor_TypeOfDating_ShortTermRelationship) {
                                    datingInfo += (string)Lists.gDSLists.EUS_LISTS_TypeOfDating.Rows[i][langId] + "</br>";

                                }
                                break;

                        }

                    }

                    details.birthdate = pr.Birthday.Value.ToShortDateString();
                    details.age = Convert.ToString(ProfileHelper.GetCurrentAge(pr.Birthday.Value));
                    details.bodytype = ProfileHelper.GetBodyTypeString(pr.PersonalInfo_BodyTypeID, langId);
                    details.haireyes = ProfileHelper.GetHairColorString(pr.PersonalInfo_HairColorID, langId) + "/" + ProfileHelper.GetEyeColorString(pr.PersonalInfo_EyeColorID, langId);
                    details.education = ProfileHelper.GetEducationString(pr.OtherDetails_EducationID, langId);
                    details.childs = ProfileHelper.GetChildrenNumberString(pr.OtherDetails_EducationID, langId);
                    details.nationality = ProfileHelper.GetEthnicityString(pr.PersonalInfo_EthnicityID, langId);
                    details.religion = ProfileHelper.GetReligionString(pr.PersonalInfo_ReligionID, langId);
                    details.smokedring = ProfileHelper.GetSmokingString(pr.PersonalInfo_SmokingHabitID, langId) + "/" + ProfileHelper.GetDrinkingString(pr.PersonalInfo_DrinkingHabitID, langId);
                    details.job = pr.OtherDetails_Occupation;
                    details.income = ProfileHelper.GetIncomeString(pr.OtherDetails_AnnualIncomeID, langId);
                    details.familystate = ProfileHelper.GetRelationshipStatusString(pr.LookingFor_RelationshipStatusID, langId);
                    details.lookfor = ProfileHelper.GetGenderString((3 - pr.GenderId), langId);
                    details.interest = datingInfo;
                    details.aboutme_describedate = pr.AboutMe_DescribeAnIdealFirstDate;
                    details.aboutme_describeyourself = pr.AboutMe_DescribeYourself;
                    details.aboutme_heading = pr.AboutMe_Heading;
                    details.profileupdated = pr.LastUpdateProfileDateTime.Value.ToShortDateString();
                    details.zodiac = ProfileHelper.ZodiacName(pr.Birthday.Value);

                    details.available_credits = Convert.ToString(customerAvailableCredits.AvailableCredits);
                    details.has_subscription = Convert.ToString(customerAvailableCredits.HasSubscription);
                    details.has_subscription = (details.has_subscription.ToLower() == "true" ? "1" : "0");
                }

            }
            catch (Exception ex) {
                WebErrorSendEmail(ex, "getUserProfileDetails, prms: id=" + id + " -- langId=" + langId);
            }
            finally {
                myContext.Dispose();
            }
            return details;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "newusers_v3/{myprofileid}/{offset}/{results}/{gender}/{isOnline}/{countrycode}")]
        public UserProfilesList getNewUsersV3(string myprofileid, string offset, string results, string gender, string isOnline, string countrycode) {

            using (myContext) {
                UpdateActivity(Convert.ToInt32(myprofileid));

                DateTime LastActivityUTCDate = ServiceBase.getMinActivityDate();
                List<UserProfile> profilesResult = new List<UserProfile>();

                if (isOnline == "0") {
                    int offsetInt = Convert.ToInt32(offset);
                    int resultInt = Convert.ToInt32(results);

                    var newProfiles = (from p in myContext.vw_EusProfile_Lights
                                       where p.IsMaster == true && p.GenderId == (3 - Convert.ToInt32(gender))
                                       && p.Status == (int)ProfileStatusEnum.Approved && p.Status != (int)ProfileStatusEnum.Deleted
                                       orderby
                                       (p.Country == countrycode ? 0 : 1) ascending,
                                       (p.DefPhotoID.Value > 0 ? 1 : 0) descending, p.DateTimeToRegister descending
                                       select p)
                                    .Skip(offsetInt)
                                    .Take(resultInt)
                                    .ToList();


                    for (int cnt = 0; cnt < newProfiles.Count; cnt++) {
                        vw_EusProfile_Light p = newProfiles[cnt];

                        string fileName = "";
                        DSMembers.EUS_CustomerPhotosRow photo = DataHelpers.GetProfilesDefaultPhoto(Convert.ToInt32(p.ProfileID));
                        if (photo != null)
                            fileName = photo.FileName;
                        //string path = ProfileHelper.GetProfileImageURL(Convert.ToInt32(p.ProfileID), fileName, Convert.ToInt32(p.GenderId), true, false);
                        string path = getImagePath(Convert.ToInt32(p.ProfileID), fileName, Convert.ToInt32(p.GenderId), true, false);

                        bool bIsOnline = ServiceBase.IsProfileOnline(LastActivityUTCDate, p);

                        UserProfile prof = new UserProfile()
                        {
                            FirstName = p.FirstName,
                            LastName = p.LastName,
                            UserRegion = p.Region,
                            UserCity = p.City,
                            UserAge = Convert.ToString(ProfileHelper.GetCurrentAge(p.Birthday.Value)),
                            ProfileId = Convert.ToString(p.ProfileID),
                            UserName = p.LoginName,
                            DateRegister = p.DateTimeToRegister.Value.ToShortDateString(),
                            GenderId = Convert.ToString(p.GenderId),
                            ImageUrl = path,
                            UserCountry = ProfileHelper.GetCountryName(p.Country),
                            Latitude = Convert.ToString(p.latitude.Value),
                            Longtitude = Convert.ToString(p.longitude.Value),
                            IsOnline = bIsOnline.ToString(),
                            PhotosCount = (p.PhotosApproved != null ? p.PhotosApproved : -1).ToString(),
                        };
                        profilesResult.Add(prof);

                    }

                }
                else if (isOnline == "1") {

                    int offsetInt = Convert.ToInt32(offset);
                    int resultInt = Convert.ToInt32(results);


                    var newProfiles = (from p in myContext.vw_EusProfile_Lights
                                       where p.IsMaster == true && p.GenderId == (3 - Convert.ToInt32(gender))
                                       && p.Status == (int)ProfileStatusEnum.Approved && p.Status != (int)ProfileStatusEnum.Deleted
                                       && p.IsOnline == true
                                       && p.LastActivityDateTime >= LastActivityUTCDate
                                       orderby
                                       (p.Country == countrycode ? 0 : 1) ascending,
                                       (p.DefPhotoID.Value > 0 ? 1 : 0) descending, p.DateTimeToRegister descending
                                       select p)
                                                .Skip(offsetInt)
                                                .Take(resultInt)
                                                .ToList();

                    //int offsetInt = Convert.ToInt32(offset);
                    //int resultInt = Convert.ToInt32(results);
                    //var newProfiles = profiles.Skip(offsetInt).Take(resultInt);

                    for (int cnt = 0; cnt < newProfiles.Count; cnt++) {
                        vw_EusProfile_Light p = newProfiles[cnt];
                        if (!clsProfilesPrivacySettings.GET_PrivacySettings_ShowMeOffline(p.ProfileID)) {
                            string fileName = "";
                            DSMembers.EUS_CustomerPhotosRow photo = DataHelpers.GetProfilesDefaultPhoto(Convert.ToInt32(p.ProfileID));
                            if (photo != null)
                                fileName = photo.FileName;
                            //string path = ProfileHelper.GetProfileImageURL(Convert.ToInt32(p.ProfileID), fileName, Convert.ToInt32(p.GenderId), true, false);
                            string path = getImagePath(Convert.ToInt32(p.ProfileID), fileName, Convert.ToInt32(p.GenderId), true, false);

                            UserProfile prof = new UserProfile()
                            {
                                FirstName = p.FirstName,
                                LastName = p.LastName,
                                UserRegion = p.Region,
                                UserCity = p.City,
                                UserAge = Convert.ToString(ProfileHelper.GetCurrentAge(p.Birthday.Value)),
                                ProfileId = Convert.ToString(p.ProfileID),
                                UserName = p.LoginName,
                                DateRegister = p.DateTimeToRegister.Value.ToShortDateString(),
                                GenderId = Convert.ToString(p.GenderId),
                                ImageUrl = path,
                                UserCountry = ProfileHelper.GetCountryName(p.Country),
                                Latitude = Convert.ToString(p.latitude.Value),
                                Longtitude = Convert.ToString(p.longitude.Value),
                                IsOnline = true.ToString(),
                                PhotosCount = (p.PhotosApproved != null ? p.PhotosApproved : -1).ToString(),
                            };
                            profilesResult.Add(prof);
                        }
                    }


                }

                UserProfilesList list = new UserProfilesList() { Profiles = profilesResult };

                myContext.Dispose();
                return list;
            }
        }


        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "profileimage/{profileId}/{genderId}")]
        public ImagePath getProfileImage(string profileId, string genderId) {
            string path = "";
            string fileName = "";
            DSMembers.EUS_CustomerPhotosRow photo = DataHelpers.GetProfilesDefaultPhoto(Convert.ToInt32(profileId));
            if (photo != null)
                fileName = photo.FileName;
            //string path = ProfileHelper.GetProfileImageURL(Convert.ToInt32(profileId), fileName, Convert.ToInt32(genderId), true, false);
            path = getImagePath(Convert.ToInt32(profileId), fileName, Convert.ToInt32(genderId), true, false);

            ImagePath imPath = new ImagePath() { Path = path, Level = (photo != null ? photo.DisplayLevel : -1) };
            return imPath;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "getuserstate/{userid}")]
        public override string getUserState(string userid) {
            return base.getUserState(userid);
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "getMinLevel/{myuserid}")]
        public string getMinLevel(string myuserid) {
            using (myContext) {

                Dating.Server.Datasets.DLL.DSMembers.EUS_CustomerPhotosDataTable dtPhotos = DataHelpers.GetEUS_CustomerPhotos_ByProfileOrMirrorID(Convert.ToInt32(myuserid)).EUS_CustomerPhotos;
                int DisplayLevel = clsNullable.DBNullToInteger(dtPhotos.Compute("Max(DisplayLevel)", ""));
                return DisplayLevel.ToString();

                //List<int> levels = new List<int>();

                //for (int i = 0; i < dtPhotos.Rows.Count; i++) {
                //    DataRow row = dtPhotos.Rows[i];
                //    levels.Add(Convert.ToInt32(row["DisplayLevel"]));
                //}

                //levels = levels.Distinct().ToList();
                //levels.Sort();

                //if (levels.Count > 1) {
                //    return Convert.ToString(levels[1]);
                //}
                //else {
                //    return "0";
                //}
            }

        }


        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "getuserphotos/{myuserid}/{userid}/{genderid}")]
        public ImagePathList getUserPhotos(string myuserid, string userid, string genderid) {
            using (myContext) {

                int userId = Convert.ToInt32(userid);
                int genderId = Convert.ToInt32(genderid);
                int myuserId = Convert.ToInt32(myuserid);


                ImagePathList imgPathList = new ImagePathList();
                imgPathList.PathList = new List<ImagePath>();

                if (userId != myuserId) {

                    EUS_ProfilePhotosLevel phtLvl = (from itm in myContext.EUS_ProfilePhotosLevels
                                                     where itm.FromProfileID == userId && itm.ToProfileID == myuserId
                                                     select itm).FirstOrDefault();


                    int allowDisplayLevel = 0;
                    if (phtLvl != null) {
                        allowDisplayLevel = phtLvl.PhotoLevelID.Value;
                    }

                    Dating.Server.Datasets.DLL.DSMembers.EUS_CustomerPhotosDataTable dtPhotos = DataHelpers.GetEUS_CustomerPhotos_ByProfileOrMirrorID(userId).EUS_CustomerPhotos;

                    foreach (DSMembers.EUS_CustomerPhotosRow row in dtPhotos) {
                        if (allowDisplayLevel >= row.DisplayLevel) {

                            bool isDeclined = (!row.IsHasDeclinedNull() && row.HasDeclined);
                            bool isDeleted = (!row.IsIsDeletedNull() && row.IsDeleted);

                            if (row.HasAproved && !isDeclined && !isDeleted) {
                                //string path = ProfileHelper.GetProfileImageURL(userId, (string)row["FileName"], genderId, false, false);
                                string path = getImagePath(userId, (string)row["FileName"], genderId, false, false);

                                ImagePath p = new ImagePath()
                                {
                                    Path = path,
                                    photoId = Convert.ToString(row["CustomerPhotosId"]),
                                    Level = row.DisplayLevel
                                };
                                imgPathList.PathList.Add(p);
                            }
                        }
                    }
                }
                else {
                    UpdateActivity(userId);
                    Dating.Server.Datasets.DLL.DSMembers.EUS_CustomerPhotosDataTable dtPhotos = DataHelpers.GetEUS_CustomerPhotos_ByProfileOrMirrorID(userId).EUS_CustomerPhotos;
                    foreach (DSMembers.EUS_CustomerPhotosRow row in dtPhotos) {

                        bool isDeclined = (!row.IsHasDeclinedNull() && row.HasDeclined);
                        bool isDeleted = (!row.IsIsDeletedNull() && row.IsDeleted);

                        if (!isDeleted) {

                            //string path = ProfileHelper.GetProfileImageURL(userId, (string)row["FileName"], genderId, true, false);
                            string path = getImagePathMyProfile(userId, (string)row["FileName"], genderId, false);
                            ImagePath p = new ImagePath()
                            {
                                Path = path,
                                photoId = Convert.ToString(row["CustomerPhotosId"]),
                                photoType = isDeclined ? 2 : 1, //2 for declined - 1 for approved
                                Level = row.DisplayLevel
                            };
                            imgPathList.PathList.Add(p);
                        }

                    }
                }

                myContext.Dispose();

                return imgPathList;

            }
        }



        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "getuserphotosThumb/{myuserid}/{userid}/{genderid}")]
        public ImagePathList getUserThumbPhotos(string myuserid, string userid, string genderid) {
            using (myContext) {

                int userId = Convert.ToInt32(userid);
                int genderId = Convert.ToInt32(genderid);
                int myuserId = Convert.ToInt32(myuserid);


                ImagePathList imgPathList = new ImagePathList();
                imgPathList.PathList = new List<ImagePath>();

                if (userId != myuserId) {

                    EUS_ProfilePhotosLevel phtLvl = (from itm in myContext.EUS_ProfilePhotosLevels
                                                     where itm.FromProfileID == userId && itm.ToProfileID == myuserId
                                                     select itm).FirstOrDefault();


                    int allowDisplayLevel = 0;
                    if (phtLvl != null) {
                        allowDisplayLevel = phtLvl.PhotoLevelID.Value;
                    }

                    Dating.Server.Datasets.DLL.DSMembers.EUS_CustomerPhotosDataTable dtPhotos = DataHelpers.GetEUS_CustomerPhotos_ByProfileOrMirrorID(userId).EUS_CustomerPhotos;


                    foreach (DSMembers.EUS_CustomerPhotosRow row in dtPhotos) {

                        bool isDeclined = (!row.IsHasDeclinedNull() && row.HasDeclined);
                        bool isDeleted = (!row.IsIsDeletedNull() && row.IsDeleted);

                        if (row.HasAproved && !isDeclined && !isDeleted) {

                            string path = "";
                            if (allowDisplayLevel < row.DisplayLevel) {
                                if (genderId == 2)
                                    path = "http://cdn.goomena.com" + "/Images2/private/list-womenlevel" + row.DisplayLevel + ".png";
                                else
                                    path = "http://cdn.goomena.com" + "/Images2/private/list-menlevel" + row.DisplayLevel + ".png";
                            }
                            else {
                                //string path = ProfileHelper.GetProfileImageURL(userId, (string)row["FileName"], genderId,true, false);
                                path = getImagePath(userId, (string)row["FileName"], genderId, true, false);
                            }

                            ImagePath p = new ImagePath()
                            {
                                Path = path,
                                photoId = Convert.ToString(row["CustomerPhotosId"]),
                                photoType = 1, //Approved
                                Level = row.DisplayLevel
                            };
                            imgPathList.PathList.Add(p);
                        }
                    }
                }
                else {
                    UpdateActivity(userId);
                    Dating.Server.Datasets.DLL.DSMembers.EUS_CustomerPhotosDataTable dtPhotos = DataHelpers.GetEUS_CustomerPhotos_ByProfileOrMirrorID(userId).EUS_CustomerPhotos;
                    foreach (DSMembers.EUS_CustomerPhotosRow row in dtPhotos) {

                        bool isDeclined = (!row.IsHasDeclinedNull() && row.HasDeclined);
                        bool isDeleted = (!row.IsIsDeletedNull() && row.IsDeleted);

                        if (!isDeleted) {

                            //string path = ProfileHelper.GetProfileImageURL(userId, (string)row["FileName"], genderId, true, false);
                            string path = getImagePathMyProfile(userId, (string)row["FileName"], genderId, false);
                            ImagePath p = new ImagePath()
                            {
                                Path = path,
                                photoId = Convert.ToString(row["CustomerPhotosId"]),
                                photoType = isDeclined ? 2 : 1, //2 for declined - 1 for approved
                                Level = row.DisplayLevel
                            };
                            imgPathList.PathList.Add(p);
                        }

                    }
                }

                myContext.Dispose();

                return imgPathList;

            }
        }


        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "deletephoto/{userid}/{photoId}")]
        public string deletePhoto(string userid, string photoId) {
            try {

                int userId = Convert.ToInt32(userid);
                UpdateActivity(userId);

                DSMembers ds = DataHelpers.GetEUS_CustomerPhotosByCustomerPhotosID(Convert.ToInt32(photoId));
                DSMembers.EUS_CustomerPhotosRow deletedPhoto = (DSMembers.EUS_CustomerPhotosRow)ds.EUS_CustomerPhotos.Rows[0];

                string fileName = ds.EUS_CustomerPhotos.Rows[0].Field<string>("FileName");
                bool isDefault = false;
                if ((!ds.EUS_CustomerPhotos.Rows[0].IsNull("IsDefault"))) {
                    isDefault = ds.EUS_CustomerPhotos.Rows[0].Field<bool>("IsDefault");
                }

                ds.EUS_CustomerPhotos.Rows[0].SetField("IsDeleted", true);
                DataHelpers.UpdateEUS_CustomerPhotos(ref ds);

                return "ok";
            }
            catch (Exception ex) {
                WebErrorSendEmail(ex, "");
                return "error";
            }
        }


        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "getsearchresultspublic/{sort}/{lat}/{lng}/{distance}/{gender}/{indexstart}/{results}/{countrycode}")]
        public UserProfilesList getSearchResultsPublic(
            string sort, string lat, string lng, string distance, string gender,
            string indexstart, string results, string countrycode) {

            int sortInt = 1;
            if (!string.IsNullOrEmpty(sort)) int.TryParse(sort, out sortInt);

            int iResults = 20;
            if (!string.IsNullOrEmpty(results)) int.TryParse(results, out iResults);

            SearchSortEnum sortEnum = SearchSortEnum.NewestMember;

            string sql = "";
            UserProfilesList pList = new UserProfilesList();
            pList.Profiles = new List<UserProfile>();

            try {

                int mins = clsConfigValues.Get__members_online_minutes();
                DateTime lastActivityUTCDate = DateTime.UtcNow.AddMinutes(-mins);

                switch (sortInt) {
                    case 0:
                        sortEnum = SearchSortEnum.None;
                        break;
                    case 1:
                        sortEnum = SearchSortEnum.NewestMember;
                        break;

                    case 2:
                        sortEnum = SearchSortEnum.OldestMember;
                        break;

                    case 3:
                        sortEnum = SearchSortEnum.RecentlyLoggedIn;
                        break;

                    case 4:
                        sortEnum = SearchSortEnum.NearestDistance;
                        break;
                }

                clsGlobalPositionWCF pos = new clsGlobalPositionWCF();
                try {
                    string ip =getClientIP();
                    clsCountryByIP country = clsCountryByIP.GetCountryCodeFromIP(ip, CurrentSettings.ConnectionString_GEODB);
                    countrycode = country.CountryCode;

                    if (!string.IsNullOrEmpty(lat) && !string.IsNullOrEmpty(lng))
                        pos = new clsGlobalPositionWCF(lat, lng);

                    if (pos.longitude == 0.0 && pos.latitude == 0.0)
                        pos = GetPositionForMinPostalCode(countrycode);

                }
                catch (Exception ex) {
                    WebErrorSendEmail(ex, "set clsGlobalPositionWCF (handled)");
                }

                clsSearchHelperParameters prms = new clsSearchHelperParameters();
                prms.ReturnRecordsWithStatus = (int)ProfileStatusEnum.Approved;
                prms.SearchSort = sortEnum;
                prms.Country = countrycode;
                prms.rowNumberMin = Convert.ToInt32(indexstart);
                prms.rowNumberMax = Convert.ToInt32(indexstart) + iResults;
                prms.AdditionalWhereClause = sql;
                prms.performCount = false;
                prms.ReturnRecordsWithStatus = (int)ProfileStatusEnum.Approved;

                if (pos != null) {
                    prms.latitudeIn = pos.latitude;
                    prms.longitudeIn = pos.longitude;
                }

                if (Convert.ToInt32(distance) > 0)
                    prms.Distance = Convert.ToInt32(distance);

                if (Convert.ToInt32(gender) == 1) {
                    prms.LookingFor_ToMeetFemaleID = true;
                    prms.LookingFor_ToMeetMaleID = false;
                }
                //else if (Convert.ToInt32(gender) == 2) {
                else {
                    prms.LookingFor_ToMeetFemaleID = false;
                    prms.LookingFor_ToMeetMaleID = true;
                }

                DataSet set = clsSearchHelper.GetMembersToSearchDataTable_Public(prms);
                DataTable dt = set.Tables[0];

                for (int i = 0; i < dt.Rows.Count; i++) {
                    DataRow dr = dt.Rows[i];

                    string path = "";
                    path = getImagePath_FromRow(dt, dr, Convert.ToInt32(dr["ProfileID"]), Convert.ToInt32(dr["GenderId"]), true);

                    if (string.IsNullOrEmpty(path)) {
                        string fileName = "";
                        DSMembers.EUS_CustomerPhotosRow photo = DataHelpers.GetProfilesDefaultPhoto(Convert.ToInt32(dr["ProfileID"]));
                        if (photo != null)
                            fileName = photo.FileName;
                        //string path = ProfileHelper.GetProfileImageURL(Convert.ToInt32(r["ProfileID"]), fileName, Convert.ToInt32(r["GenderId"]), true, false);
                        path = getImagePath(Convert.ToInt32(dr["ProfileID"]), fileName, Convert.ToInt32(dr["GenderId"]), true, false);
                    }

                    bool OtherMemberIsOnline = ServiceBase.GetIsOnline(lastActivityUTCDate, dt, dr);

                    int PhotosApproved = -1;
                    if (dt.Columns.Contains("PhotosApproved")) {
                        PhotosApproved = 0;
                        if (!dr.IsNull("PhotosApproved"))
                            PhotosApproved = (int)dr["PhotosApproved"];
                    }

                    UserProfile p = new UserProfile()
                    {
                        UserAge = dr["Age"].ToString(),
                        FirstName = dr["FirstName"].ToString(),
                        LastName = dr["LastName"].ToString(),
                        UserName = dr["LoginName"].ToString(),
                        UserRegion = dr["Region"].ToString(),
                        UserCity = dr["City"].ToString(),
                        ProfileId = dr["ProfileID"].ToString(),
                        GenderId = dr["GenderId"].ToString(),
                        ImageUrl = path,
                        Latitude = (dt.Columns.Contains("latitude") ? Convert.ToString(dr["latitude"]) : null),
                        Longtitude = (dt.Columns.Contains("longitude") ? Convert.ToString(dr["longitude"]) : null),
                        IsOnline = OtherMemberIsOnline.ToString(),
                        PhotosCount = PhotosApproved.ToString(),
                        UserCountry = ProfileHelper.GetCountryName(dr["Country"].ToString()),

                    };

                    p.IsVip = (p.GenderId == "1") ? GetIsVip(dt, dr) : "0";
                    pList.Profiles.Add(p);


                }
            }
            catch (Exception ex) {
                WebErrorSendEmail(ex, "");
            }

            return pList;
        }



        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "getsearchresults_v3/{userid}/{sort}/{zipstr}/{lat}/{lng}/{distance}/{gender}/{searchname}/{username}/{agemin}/{agemax}/{hasPhotos}/{isOnline}/{indexstart}/{results}/{isVip}/{countrycode}/{hasPrivatePhotos}/{travel}")]
        public UserProfilesList getSearchResultsV3(
            string userid, string sort, string zipstr,
            string lat, string lng, string distance,
            string gender, string searchname, string username, string agemin, string agemax,
            string hasPhotos, string isOnline, string indexstart, string results,
            string isVip, string countrycode, string hasPrivatePhotos, string travel) {

            int sortInt = 1;
            if (!string.IsNullOrEmpty(sort)) int.TryParse(sort, out sortInt);

            int iResults = 20;
            if (!string.IsNullOrEmpty(results)) int.TryParse(results, out iResults);

            SearchSortEnum sortEnum = SearchSortEnum.NewestMember;

            string sql = "";
            //bool sqlSet = false;
            UserProfilesList pList = new UserProfilesList();
            pList.Profiles = new List<UserProfile>();
            int iUserId = Convert.ToInt32(userid);

            try {
                UpdateActivity(iUserId);

                int mins = clsConfigValues.Get__members_online_minutes();
                DateTime lastActivityUTCDate = DateTime.UtcNow.AddMinutes(-mins);

                if (Convert.ToInt32(searchname) == 1) {

                    clsSQLInjectionClear.ClearString(ref username, 100);
                    if (username.Length > 0) {
                        sql = sql + "\r\n" + " and  EUS_Profiles.LoginName like '" + username + "%' ";

                    }
                }
                else {
                    if (Convert.ToInt32(agemin) != -1 && Convert.ToInt32(agemax) != -1) {
                        DateTime fromDate = DateTime.Now.AddYears((Convert.ToInt32(agemax) * -1) - 1).Date;
                        DateTime toDate = DateTime.Now.AddYears((Convert.ToInt32(agemin) * -1)).Date;

                        sql = sql + "\r\n" + " and  EUS_Profiles.Birthday between '" + fromDate.ToString("yyyy-MM-dd hh:mm:ss.mmm") + "' and '" + toDate.ToString("yyyy-MM-dd hh:mm:ss.mmm") + "'  ";
                        //sqlSet = true;
                    }
                    else if (Convert.ToInt32(agemin) != -1) {
                        DateTime toDate = DateTime.Now.AddYears((Convert.ToInt32(agemin) * -1)).Date;
                        sql = sql + "\r\n" + " and EUS_Profiles.Birthday < '" + toDate.ToString("yyyy-MM-dd hh:mm:ss.mmm") + "' ";
                        //sqlSet = true;
                    }
                    else if (Convert.ToInt32(agemax) != -1) {
                        DateTime fromDate = DateTime.Now.AddYears((Convert.ToInt32(agemax) * -1) - 1).Date;
                        sql = sql + "\r\n" + " and EUS_Profiles.Birthday between '" + fromDate.ToString("yyyy-MM-dd hh:mm:ss.mmm") + "' and getdate() ";
                        //sqlSet = true;
                    }

                    if (hasPhotos == "1") {
                        sql = sql + " and exists (select customerId from EUS_CustomerPhotos phot1 where phot1.customerId = EUS_Profiles.ProfileId and phot1.HasAproved = 1 and (phot1.HasDeclined is null or phot1.HasDeclined=0) and (phot1.IsDeleted is null or phot1.IsDeleted=0))";
                    }

                    if (isOnline == "1") {
                        sql = sql + " and (EUS_Profiles.IsOnline=1 and EUS_Profiles.LastActivityDateTime>= '" + lastActivityUTCDate.ToString("yyyy-MM-dd HH:mm") + "')";
                    }

                    if (isVip == "1" && gender == "2") {
                        // sql = sql + " and EUS_Profiles.AvailableCredits>=" + ProfileHelper.Config_UNLOCK_MESSAGE_READ_CREDITS;
                        sql = sql + " and ((EUS_Profiles.AvailableCredits >=" + ProfileHelper.Config_UNLOCK_MESSAGE_READ_CREDITS + ") or exists (select CustomerCreditsId from EUS_CustomerCredits cc where cc.CustomerId = EUS_Profiles.ProfileId and cc.IsSubscription=1 and cc.DateTimeExpiration>getutcdate()))";
                    }

                    if (hasPrivatePhotos == "true") {
                        sql = sql + " and exists (select customerId from EUS_CustomerPhotos phot1 where phot1.customerId = EUS_Profiles.ProfileId and phot1.HasAproved = 1 and (phot1.HasDeclined is null or phot1.HasDeclined=0) and (phot1.IsDeleted is null or phot1.IsDeleted=0) and phot1.DisplayLevel>0)";
                    }

                    if (travel == "true") {
                        sql = sql + " and (AreYouWillingToTravel=1)";
                    }


                    switch (sortInt) {
                        case 0:
                            sortEnum = SearchSortEnum.None;
                            break;
                        case 1:
                            sortEnum = SearchSortEnum.NewestMember;
                            break;

                        case 2:
                            sortEnum = SearchSortEnum.OldestMember;
                            break;

                        case 3:
                            sortEnum = SearchSortEnum.RecentlyLoggedIn;
                            break;

                        case 4:
                            sortEnum = SearchSortEnum.NearestDistance;
                            break;

                    }
                }

                clsSearchHelperParameters searchParams = new clsSearchHelperParameters();
                searchParams.CurrentProfileId = iUserId;
                searchParams.ReturnRecordsWithStatus = (int)ProfileStatusEnum.Approved;
                searchParams.SearchSort = sortEnum;
                searchParams.zipstr = zipstr;
                searchParams.latitudeIn = AppUtils.GetDoubleValue_FromStringWithPeriod(lat);// Convert.ToDouble(lat);
                searchParams.longitudeIn = AppUtils.GetDoubleValue_FromStringWithPeriod(lng);// Convert.ToDouble(lng);
                searchParams.Country = countrycode;

                if (Convert.ToInt32(distance) > 0) {
                    searchParams.Distance = Convert.ToInt32(distance);
                }

                if (Convert.ToInt32(gender) == 1) {
                    searchParams.LookingFor_ToMeetFemaleID = true;
                    searchParams.LookingFor_ToMeetMaleID = false;
                }
                else if (Convert.ToInt32(gender) == 2) {
                    searchParams.LookingFor_ToMeetFemaleID = false;
                    searchParams.LookingFor_ToMeetMaleID = true;
                }

                searchParams.rowNumberMin = Convert.ToInt32(indexstart);
                searchParams.rowNumberMax = Convert.ToInt32(indexstart) + iResults;
                //searchParams.NumberOfRecordsToReturn = 20;
                searchParams.AdditionalWhereClause = sql;
                searchParams.performCount = false;

                DataSet set = clsSearchHelper.GetMembersToSearchDataTable(searchParams);
                DataTable dt = set.Tables[0];

                for (int i = 0; i < dt.Rows.Count; i++) {
                    DataRow dr = dt.Rows[i];

                    string path = "";
                    path = getImagePath_FromRow(dt, dr, Convert.ToInt32(dr["ProfileID"]), Convert.ToInt32(dr["GenderId"]), true);

                    if (string.IsNullOrEmpty(path)) {
                        string fileName = "";
                        DSMembers.EUS_CustomerPhotosRow photo = DataHelpers.GetProfilesDefaultPhoto(Convert.ToInt32(dr["ProfileID"]));
                        if (photo != null)
                            fileName = photo.FileName;
                        //string path = ProfileHelper.GetProfileImageURL(Convert.ToInt32(r["ProfileID"]), fileName, Convert.ToInt32(r["GenderId"]), true, false);
                        path = getImagePath(Convert.ToInt32(dr["ProfileID"]), fileName, Convert.ToInt32(dr["GenderId"]), true, false);
                    }

                    bool OtherMemberIsOnline = ServiceBase.GetIsOnline(lastActivityUTCDate, dt, dr);

                    int PhotosApproved = -1;
                    if (dt.Columns.Contains("PhotosApproved")) {
                        PhotosApproved = 0;
                        if (!dr.IsNull("PhotosApproved"))
                            PhotosApproved = (int)dr["PhotosApproved"];
                    }

                    UserProfile p = new UserProfile()
                    {
                        UserAge = dr["Age"].ToString(),
                        FirstName = dr["FirstName"].ToString(),
                        LastName = dr["LastName"].ToString(),
                        UserName = dr["LoginName"].ToString(),
                        UserRegion = dr["Region"].ToString(),
                        UserCity = dr["City"].ToString(),
                        ProfileId = dr["ProfileID"].ToString(),
                        GenderId = dr["GenderId"].ToString(),
                        ImageUrl = path,
                        Latitude = (dt.Columns.Contains("latitude") ? Convert.ToString(dr["latitude"]) : null),
                        Longtitude = (dt.Columns.Contains("longitude") ? Convert.ToString(dr["longitude"]) : null),
                        IsOnline = OtherMemberIsOnline.ToString(),
                        PhotosCount = PhotosApproved.ToString(),
                        UserCountry = ProfileHelper.GetCountryName(dr["Country"].ToString()),

                    };

                    p.IsVip = (p.GenderId == "1") ? GetIsVip(dt, dr) : "0";
                    pList.Profiles.Add(p);


                }
            }
            catch (Exception ex) {
                WebErrorSendEmail(ex, "");
            }

            return pList;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "getsearchresults/{userid}/{sort}/{zipstr}/{lat}/{lng}/{distance}/{gender}/{searchname}/{username}/{agemin}/{agemax}/{hasPhotos}/{isOnline}/{indexstart}/{results}")]
        public UserProfilesList getSearchResults(string userid, string sort, string zipstr, string lat, string lng, string distance,
            string gender, string searchname, string username, string agemin, string agemax,
            string hasPhotos, string isOnline, string indexstart, string results) {
            int sortInt = Convert.ToInt32(sort);
            SearchSortEnum sortEnum = SearchSortEnum.NewestMember;

            string sql = "";
            //bool sqlSet = false;
            UserProfilesList pList = new UserProfilesList();
            pList.Profiles = new List<UserProfile>();
            int iUserId = Convert.ToInt32(userid);
            try {
                UpdateActivity(iUserId);

                int mins = clsConfigValues.Get__members_online_minutes();
                DateTime lastActivityUTCDate = DateTime.UtcNow.AddMinutes(-mins);

                if (Convert.ToInt32(searchname) == 1) {

                    clsSQLInjectionClear.ClearString(ref username, 100);
                    if (username.Length > 0) {
                        sql = sql + "\r\n" + " and  EUS_Profiles.LoginName like '" + username + "%' ";

                    }
                }
                else {
                    if (Convert.ToInt32(agemin) != -1 && Convert.ToInt32(agemax) != -1) {
                        DateTime fromDate = DateTime.Now.AddYears((Convert.ToInt32(agemax) * -1) - 1).Date;
                        DateTime toDate = DateTime.Now.AddYears((Convert.ToInt32(agemin) * -1)).Date;

                        sql = sql + "\r\n" + " and  EUS_Profiles.Birthday between '" + fromDate.ToString("yyyy-MM-dd hh:mm:ss.mmm") + "' and '" + toDate.ToString("yyyy-MM-dd hh:mm:ss.mmm") + "'  ";
                        //sqlSet = true;
                    }
                    else if (Convert.ToInt32(agemin) != -1) {
                        DateTime toDate = DateTime.Now.AddYears((Convert.ToInt32(agemin) * -1)).Date;
                        sql = sql + "\r\n" + " and EUS_Profiles.Birthday < '" + toDate.ToString("yyyy-MM-dd hh:mm:ss.mmm") + "' ";
                        //sqlSet = true;
                    }
                    else if (Convert.ToInt32(agemax) != -1) {
                        DateTime fromDate = DateTime.Now.AddYears((Convert.ToInt32(agemax) * -1) - 1).Date;
                        sql = sql + "\r\n" + " and EUS_Profiles.Birthday between '" + fromDate.ToString("yyyy-MM-dd hh:mm:ss.mmm") + "' and getdate() ";
                        //sqlSet = true;
                    }

                    if (hasPhotos == "1") {
                        sql = sql + " and exists (select customerId from EUS_CustomerPhotos phot1 where phot1.customerId=EUS_Profiles.ProfileId and phot1.HasAproved=1 and phot1.IsDeleted=0)";
                        //sqlSet = true;
                    }


                    if (isOnline == "1") {

                        sql = sql + " and (EUS_Profiles.IsOnline=1 and EUS_Profiles.LastActivityDateTime>= '" + lastActivityUTCDate.ToString("yyyy-MM-dd HH:mm") + "')";
                        //sqlSet = true;
                    }

                    switch (sortInt) {
                        case 0:
                            sortEnum = SearchSortEnum.None;
                            break;
                        case 1:
                            sortEnum = SearchSortEnum.NewestMember;
                            break;

                        case 2:
                            sortEnum = SearchSortEnum.OldestMember;
                            break;

                        case 3:
                            sortEnum = SearchSortEnum.RecentlyLoggedIn;
                            break;

                        case 4:
                            sortEnum = SearchSortEnum.NearestDistance;
                            break;

                    }
                }

                clsSearchHelperParameters searchParams = new clsSearchHelperParameters();
                searchParams.CurrentProfileId = iUserId;
                searchParams.ReturnRecordsWithStatus = (int)ProfileStatusEnum.Approved;
                searchParams.SearchSort = sortEnum;
                searchParams.zipstr = zipstr;
                searchParams.latitudeIn = Convert.ToDouble(lat);
                searchParams.longitudeIn = Convert.ToDouble(lng);
                searchParams.rowNumberMin = Convert.ToInt32(indexstart);
                searchParams.rowNumberMax = Convert.ToInt32(indexstart) + 20;

                if (Convert.ToInt32(distance) > 0) {
                    searchParams.Distance = Convert.ToInt32(distance);
                }

                if (Convert.ToInt32(gender) == 1) {
                    searchParams.LookingFor_ToMeetFemaleID = true;
                    searchParams.LookingFor_ToMeetMaleID = false;
                }
                else if (Convert.ToInt32(gender) == 2) {
                    searchParams.LookingFor_ToMeetFemaleID = false;
                    searchParams.LookingFor_ToMeetMaleID = true;
                }

                searchParams.NumberOfRecordsToReturn = Convert.ToInt32(indexstart) + Convert.ToInt32(results);
                searchParams.AdditionalWhereClause = sql;
                searchParams.performCount = false;

                DataSet set = clsSearchHelper.GetMembersToSearchDataTable(searchParams);
                DataTable dt = set.Tables[0];

                /*
                int start = Convert.ToInt32(indexstart);
                int result = Convert.ToInt32(results);
                int rowsmax;
                if (start + result < dataTable.Rows.Count)
                {
                    rowsmax = start + result;
                }
                else
                {
                    rowsmax = dataTable.Rows.Count;
                }

                 * */

                for (int i = 0; i < dt.Rows.Count; i++) {
                    DataRow dr = dt.Rows[i];

                    string path = "";
                    path = getImagePath_FromRow(dt, dr, Convert.ToInt32(dr["ProfileID"]), Convert.ToInt32(dr["GenderId"]), true);

                    if (string.IsNullOrEmpty(path)) {
                        string fileName = "";
                        DSMembers.EUS_CustomerPhotosRow photo = DataHelpers.GetProfilesDefaultPhoto(Convert.ToInt32(dr["ProfileID"]));
                        if (photo != null)
                            fileName = photo.FileName;
                        path = ProfileHelper.GetProfileImageURL(Convert.ToInt32(dr["ProfileID"]), fileName, Convert.ToInt32(dr["GenderId"]), true, false);
                    }

                    bool OtherMemberIsOnline = ServiceBase.GetIsOnline(lastActivityUTCDate, dt, dr);

                    int PhotosApproved = -1;
                    if (dt.Columns.Contains("PhotosApproved")) {
                        PhotosApproved = 0;
                        if (!dr.IsNull("PhotosApproved"))
                            PhotosApproved = (int)dr["PhotosApproved"];
                    }

                    UserProfile p = new UserProfile()
                    {
                        UserAge = Convert.ToString(dr["Age"]),
                        FirstName = (string)dr["FirstName"],
                        LastName = (string)dr["LastName"],
                        UserName = (string)dr["LoginName"],
                        UserRegion = (string)dr["Region"],
                        UserCity = (string)dr["City"],
                        ProfileId = Convert.ToString(dr["ProfileID"]),
                        GenderId = Convert.ToString(dr["GenderId"]),
                        ImageUrl = path,
                        Latitude = (dt.Columns.Contains("latitude") ? Convert.ToString(dr["latitude"]) : null),
                        Longtitude = (dt.Columns.Contains("longitude") ? Convert.ToString(dr["longitude"]) : null),
                        IsOnline = OtherMemberIsOnline.ToString(),
                        PhotosCount = PhotosApproved.ToString(),
                    };

                    p.IsVip = (p.GenderId == "1") ? GetIsVip(dt, dr) : "0";
                    pList.Profiles.Add(p);
                }


            }
            catch (Exception ex) {
                WebErrorSendEmail(ex, "");
            }

            return pList;
        }


        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "messagesquick/{userid}")]
        public MessageInfoList getMessagesQuick(string userid) {
            MessageInfoList list = new MessageInfoList();
            try {
                int iUserId = Convert.ToInt32(userid);
                UpdateActivity(iUserId);
                //DataHelpers.UpdateEUS_Profiles_Activity(Convert.ToInt32(userid), getClientIP(), true);

                // 4 = new messages
                // 20 results
                DataSet ds = clsMessagesHelper.GetNewMessagesQuick2(iUserId, 4, 20);

                list.msgList = new List<MessageInfo>();
                DataTable dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows) {

                    string path = "";
                    path = getImagePath_FromRow(dt, dr, Convert.ToInt32(dr["ProfileID"]), Convert.ToInt32(dr["GenderId"]), true);

                    if (string.IsNullOrEmpty(path)) {
                        string fileName = "";
                        DSMembers.EUS_CustomerPhotosRow photo = DataHelpers.GetProfilesDefaultPhoto(Convert.ToInt32(dr["ProfileID"]));
                        if (photo != null)
                            fileName = photo.FileName;
                        //string path = ProfileHelper.GetProfileImageURL(Convert.ToInt32(dr["ProfileID"]), fileName, Convert.ToInt32(dr["GenderId"]), true, false);
                        path = getImagePath(Convert.ToInt32(dr["ProfileID"]), fileName, Convert.ToInt32(dr["GenderId"]), true, false);
                    }

                    MessageInfo inf = new MessageInfo()
                    {
                        MsgCount = Convert.ToString(dr["ItemsCount"]),
                        MsgId = Convert.ToString(dr["EUS_MessageID"]),
                        ProfileId = Convert.ToString(dr["ProfileID"]),
                        UserName = (string)dr["LoginName"],
                        Age = Convert.ToString(ProfileHelper.GetCurrentAge((DateTime)dr["Birthday"])),
                        City = (string)dr["City"],
                        Region = (string)dr["Region"],
                        Country = (string)dr["Country"],
                        DateTimeSent = ((DateTime)dr["MAXDateTimeToCreate"]).ToString("dd MMM, yyyy H:mm"),
                        ProfileImg = path
                    };
                    list.msgList.Add(inf);

                }


            }
            catch (Exception ex) {
                WebErrorSendEmail(ex, "");
            }

            return list;
        }


        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "messageslist/{userid}/{view}/{indexstart}/{active}")]
        public MessageInfoList getMessagesList(string userid, string view, string indexstart, string active) {
            int iUserId = Convert.ToInt32(userid);
            UpdateActivity(iUserId);
            //DataHelpers.UpdateEUS_Profiles_Activity(Convert.ToInt32(userid), getClientIP(), true);

            int results = 20;

            MessagesViewEnum msgView = new MessagesViewEnum();
            switch (Convert.ToInt32(view)) {
                case 0:
                    msgView = MessagesViewEnum.None;
                    break;

                case 1:
                    msgView = MessagesViewEnum.INBOX;
                    break;

                case 2:
                    msgView = MessagesViewEnum.SENT;
                    break;

                case 4:
                    msgView = MessagesViewEnum.CONVERSATION;
                    break;

                case 8:
                    msgView = MessagesViewEnum.NEWMESSAGES;
                    break;

                case 16:
                    msgView = MessagesViewEnum.TRASH;
                    break;

                case 32:
                    msgView = MessagesViewEnum.SEND_NEW_MESSAGE;
                    break;

            }

            Boolean isActive;
            if (active == "1") {
                isActive = true;
            }
            else {
                isActive = false;
            }

            clsMessagesHelperParameters parms = new clsMessagesHelperParameters();
            parms.CurrentProfileId = iUserId;
            parms.MessagesView = msgView;
            parms.cmdFilter = "";
            parms.performCount = false;
            parms.showActive = isActive;
            parms.rowNumberMin = Convert.ToInt32(indexstart);
            parms.rowNumberMax = Convert.ToInt32(indexstart) + results;

            DataSet ds = clsMessagesHelper.GetMessagesDataTable(ref parms);
            MessageInfoList messageList = new MessageInfoList();
            messageList.msgList = new List<MessageInfo>();

            int mins = clsConfigValues.Get__members_online_minutes();
            DateTime lastActivityUTCDate = DateTime.UtcNow.AddMinutes(-mins);


            if (ds != null && ds.Tables.Count > 0) {

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                    DataRow dr = ds.Tables[0].Rows[i];
                    string path = "";
                    path = getImagePath_FromRow(ds.Tables[0], dr, Convert.ToInt32(dr["ProfileID"]), Convert.ToInt32(dr["GenderId"]), true);

                    if (string.IsNullOrEmpty(path)) {
                        string fileName = "";
                        DSMembers.EUS_CustomerPhotosRow photo = DataHelpers.GetProfilesDefaultPhoto(Convert.ToInt32(dr["ProfileID"]));
                        if (photo != null)
                            fileName = photo.FileName;
                        path = ProfileHelper.GetProfileImageURL(Convert.ToInt32(dr["ProfileID"]), fileName, Convert.ToInt32(dr["GenderId"]), true, false);
                    }

                    bool OtherMemberIsOnline = ServiceBase.GetIsOnline(lastActivityUTCDate, ds.Tables[0], dr);
                    MessageInfo info = new MessageInfo()
                    {
                        MsgCount = Convert.ToString(dr["ItemsCount"]),
                        MsgId = Convert.ToString(dr["EUS_MessageID"]),
                        ProfileId = Convert.ToString(dr["ProfileID"]),
                        UserName = (string)dr["LoginName"],
                        Age = Convert.ToString(ProfileHelper.GetCurrentAge((DateTime)dr["Birthday"])),
                        City = (string)dr["City"],
                        Region = (string)dr["Region"],
                        Country = (string)dr["Country"],
                        DateTimeSent = ((DateTime)dr["MAXDateTimeToCreate"]).ToString("dd MMM, yyyy H:mm"),
                        ProfileImg = path,
                        IsOnline = OtherMemberIsOnline.ToString()
                    };

                    messageList.msgList.Add(info);

                }


            }

            return messageList;

        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "conversation/{profileid}/{messageid}")]
        public MessageList getConversationMessages(string profileid, string messageid) {
            int iProfileid = Convert.ToInt32(profileid);
            int iMessageid = Convert.ToInt32(messageid);

            UpdateActivity(iProfileid);
            //DataHelpers.UpdateEUS_Profiles_Activity(iProfileid, getClientIP(), true);


            DataSet ds = clsMessagesHelper.GetConversationWithMessage(iProfileid, iMessageid);
            MessageList mList = new MessageList();
            mList.list = new List<Message>();

            foreach (DataRow dr in ds.Tables[0].Rows) {
                Message m = new Message()
                {
                    MessageId = Convert.ToString(dr["EUS_MessageID"]),
                    FromProfileId = Convert.ToString(dr["FromProfileId"]),
                    ToProfileId = Convert.ToString(dr["ToProfileId"]),
                    Subject = Convert.ToString(dr["subject"]),
                    Body = Convert.ToString(dr["Body"]),
                    DateTimeSent = Convert.ToDateTime(dr["DateTimeToCreate"]).ToString("dd MMM, yyyy H:mm"),
                    IsReceived = Convert.ToBoolean(dr["IsReceived"]),
                    IsSent = Convert.ToBoolean(dr["IsSent"]),
                    statusId = Convert.ToString(dr["StatusID"])
                };

                mList.list.Add(m);
            }


            return mList;
        }


        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "conversation_with_profile/{ownerprofileid}/{profileid2}")]
        public MessageList getConversationMessagesWithProfile(string ownerprofileid, string profileid2) {
            int iOwnerprofileid = Convert.ToInt32(ownerprofileid);
            int iProfileid2 = Convert.ToInt32(profileid2);


            MessageList mList = null;
            EUS_Message m = clsUserDoes.GetLastMessageForProfiles(iOwnerprofileid, iProfileid2);
            if (m != null && m.EUS_MessageID > 0) {
                mList = getConversationMessages(ownerprofileid, m.EUS_MessageID.ToString());
            }
            else {
                mList = new MessageList();
                UpdateActivity(iOwnerprofileid);
                //DataHelpers.UpdateEUS_Profiles_Activity(iOwnerprofileid, getClientIP(), true);
            }

            return mList;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "message/{messageid}")]
        public Message getMessageById(string messageid) {
            EUS_Message message = clsUserDoes.GetMessage(Convert.ToInt32(messageid));

            Message m = new Message()
            {
                Subject = message.Subject,
                Body = message.Body,
                MessageId = Convert.ToString(message.EUS_MessageID),
                FromProfileId = Convert.ToString(message.FromProfileID),
                ToProfileId = Convert.ToString(message.ToProfileID),
                DateTimeSent = message.DateTimeToCreate.Value.ToString("dd MMM, yyyy H:mm"),
                IsReceived = message.IsReceived,
                IsSent = message.IsSent

            };

            return m;

        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "unlockmessage/{userid}/{genderid}/{senderid}/{messageid}/{offerid}")]
        public string unlockMessage(string userid, string genderid, string senderid, string messageid, string offerid) {
            int iUserId = Convert.ToInt32(userid);
            UpdateActivity(iUserId);
            //DataHelpers.UpdateEUS_Profiles_Activity(Convert.ToInt32(userid), getClientIP(), true);

            if (genderid == "1") {

                bool hasCreditsToRead = clsUserDoes.HasRequiredCredits(iUserId, UnlockType.UNLOCK_MESSAGE_READ);
                if (hasCreditsToRead) {
                    double _REF_CRD2EURO_Rate = DataHelpers.GetEUS_Profiles_REF_CRD2EURO_Rate(Convert.ToInt32(senderid));
                    long customerCreditsId = clsUserDoes.UnlockMessageOnce(Convert.ToInt32(senderid), iUserId,
                                                                (int)ProfileHelper.Config_UNLOCK_MESSAGE_READ_CREDITS,
                                                                UnlockType.UNLOCK_MESSAGE_READ,
                                                                Convert.ToInt32(messageid),
                                                                _REF_CRD2EURO_Rate,
                                                                false);

                    clsUserDoes.MarkMessageRead(Convert.ToInt64(messageid));

                    int gid = Convert.ToInt32(genderid);
                    int ifromprofileid = iUserId;
                    int itoprofileid = Convert.ToInt32(senderid);
                    int iofferId = Convert.ToInt32(offerid);
                    clsUserDoes.MakeNewDate(ifromprofileid, itoprofileid, iofferId, (gid == 1));

                    //makeNewDate(userid, senderid, Convert.ToInt32(offerid), Convert.ToInt32(genderid));
                    return "ok";
                }
                else {
                    return "noceredits";
                }

            }
            else if (genderid == "2") {
                clsUserDoes.MarkMessageRead(Convert.ToInt64(messageid));
                return "ok";
            }

            return "";

        }

        //private void makeNewDate(string fromProfileId, string toProfileId, int offerId, int genderId)
        //{
        //    clsUserDoes.NewOfferParameters parms = new clsUserDoes.NewOfferParameters();
        //    bool anyOfferAccepted = false;

        //    if (offerId > 0) {
        //        EUS_Offer offer = clsUserDoes.GetOfferByOfferId(Convert.ToInt16(fromProfileId), offerId);
        //        if (offer.OfferTypeID == ProfileHelper.OfferTypeID_POKE) {
        //            clsUserDoes.AcceptPoke(offerId, OfferStatusEnum.POKE_ACCEEPTED_WITH_MESSAGE);
        //            anyOfferAccepted = true;

        //        }
        //        else if (offer.OfferTypeID == ProfileHelper.OfferTypeID_WINK) {
        //            clsUserDoes.AcceptLike(offerId, OfferStatusEnum.LIKE_ACCEEPTED_WITH_MESSAGE);
        //            anyOfferAccepted = true;

        //        }
        //        else if (offer.OfferTypeID == ProfileHelper.OfferTypeID_OFFERNEW || offer.OfferTypeID == ProfileHelper.OfferTypeID_OFFERCOUNTER) {
        //            clsUserDoes.AcceptOffer(offerId, OfferStatusEnum.OFFER_ACCEEPTED_WITH_MESSAGE);
        //            anyOfferAccepted = true;
        //        }

        //        parms.messageText1 = "";
        //        parms.messageText2 = "";
        //        parms.offerAmount = offer.Amount.Value;
        //        parms.parentOfferId = (int)offer.OfferID;
        //        parms.userIdReceiver = (int)offer.ToProfileID;
        //        parms.userIdWhoDid = (int)offer.FromProfileID;

        //    }
        //    else {
        //        parms.messageText1 = "";
        //        parms.messageText2 = "";
        //        parms.offerAmount = 0;
        //        parms.userIdReceiver = Convert.ToInt32(toProfileId);
        //        parms.userIdWhoDid = Convert.ToInt32(fromProfileId);
        //    }


        //    if (genderId == 1) {
        //        if (!clsUserDoes.HasDateOfferAlready(parms.userIdWhoDid, parms.userIdReceiver)) {
        //            clsUserDoes.MakeNewDate(parms);

        //        }

        //    }

        //    if (!anyOfferAccepted) {

        //        EUS_Offer rec = clsUserDoes.GetAnyWinkPending(Convert.ToInt32(fromProfileId), Convert.ToInt32(fromProfileId));
        //        if (rec != null) {

        //            clsUserDoes.AcceptLike((int)rec.OfferID, OfferStatusEnum.LIKE_ACCEEPTED_WITH_MESSAGE);
        //        }
        //        else {
        //            rec = clsUserDoes.GetAnyPokePending(Convert.ToInt32(fromProfileId), Convert.ToInt32(toProfileId));
        //            if (rec != null) {

        //                clsUserDoes.AcceptPoke((int)rec.OfferID, OfferStatusEnum.POKE_ACCEEPTED_WITH_MESSAGE);
        //            }
        //            else {
        //                rec = clsUserDoes.GetLastOffer(Convert.ToInt32(fromProfileId), Convert.ToInt32(toProfileId));
        //                if (rec != null) {
        //                    clsUserDoes.AcceptPoke((int)rec.OfferID, OfferStatusEnum.POKE_ACCEEPTED_WITH_MESSAGE);
        //                }
        //            }
        //        }

        //    }

        //}

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "sendmessage")]
        public string sendMessage(MessageParams mp) {

            int iUserId = Convert.ToInt32(mp.fromprofileid);
            int genderid = Convert.ToInt32(mp.genderid);
            bool sendOnce = true;
            //bool sendUnlimited = false;
            bool alreadyUnlockedUnlimited = false;
            bool messagePaid = false;
            EUS_Message messageReceived = null;


            UpdateActivity(iUserId);
            //DataHelpers.UpdateEUS_Profiles_Activity(iUserId, getClientIP(), true);

            bool blockReceiving = clsProfilesPrivacySettings.GET_AdminSetting_BlockMessagesToBeReceivedByOther(iUserId);
            if (genderid == 1) {

                bool maySendMessage = clsUserDoes.HasRequiredCredits(iUserId, UnlockType.UNLOCK_MESSAGE_SEND);

                if (maySendMessage) {

                    messageReceived = clsUserDoes.SendMessage(mp.subject, mp.body, iUserId, Convert.ToInt32(mp.toprofileid), false, 0, blockReceiving, false, genderid);

                    if (!alreadyUnlockedUnlimited) {
                        if (sendOnce) {
                            double _REF_CRD2EURO_Rate = DataHelpers.GetEUS_Profiles_REF_CRD2EURO_Rate(messageReceived.FromProfileID.Value);
                            long customerCreditsId = clsUserDoes.UnlockMessageOnce(Convert.ToInt32(mp.toprofileid),
                                iUserId,
                                (int)ProfileHelper.Config_UNLOCK_MESSAGE_SEND_CREDITS,
                                UnlockType.UNLOCK_MESSAGE_SEND,
                                messageReceived.EUS_MessageID,
                                _REF_CRD2EURO_Rate,
                                false);

                            try {

                                double costPerMessage = ProfileHelper.Config_UNLOCK_MESSAGE_SEND_CREDITS * _REF_CRD2EURO_Rate;
                                clsReferrer.SetCommissionForMessage((int)messageReceived.EUS_MessageID, (int)messageReceived.ToProfileID, costPerMessage, (int)ProfileHelper.Config_UNLOCK_MESSAGE_SEND_CREDITS, UnlockType.UNLOCK_MESSAGE_SEND, customerCreditsId);
                            }
                            catch (Exception ex) {
                                WebErrorSendEmail(ex, "send message");
                                return "-1";
                            }

                            messagePaid = true;

                        }
                    }

                }

            }
            else if (genderid == 2) {
                messageReceived = clsUserDoes.SendMessage(mp.subject, mp.body, iUserId, Convert.ToInt32(mp.toprofileid), true, 0, blockReceiving, false, genderid);
                messagePaid = true;
            }
            if (mp.toprofileid == "1") {
                try {
                    using (myContext) {

                        var pr = (from q in myContext.vw_EusProfile_Lights
                                 where q.ProfileID == iUserId
                                 select new
                                 {
                                     login = q.LoginName,
                                     email = q.eMail
                                 }).FirstOrDefault();

                        string newSubject = "Message sent to administrator of Goomena.com";
                        string newBody =
                            "<br>" +
                            "Message sent to administrator of Goomena.com" + "<br>" +
                            "<hr>" +
                            "Login Name : " + pr.login + "<br>" +
                                "E-mail : " + pr.email + "<br>" +
                            "<hr>" +
                            "Subject : " + mp.subject + "<br>" +
                            "Body : " + mp.body;

                        clsMyMail.SendMail(ConfigurationManager.AppSettings["gToEmail"], newSubject, newBody, true);

                    }
                }
                catch (Exception ex) {
                    WebErrorSendEmail(ex, "send message");
                    return "-1";
                }

            }



            if (messagePaid) {
                int ifromprofileid = iUserId;
                int itoprofileid = Convert.ToInt32(mp.toprofileid);
                int iofferId = Convert.ToInt32(mp.offerId);
                clsUserDoes.MakeNewDate(ifromprofileid, itoprofileid, iofferId, (genderid == 1));
            }

            string notifResult = sendNotificationToUser(mp.fromprofileid, mp.toprofileid, "3");

            if (messageReceived != null) {
                return Convert.ToString(messageReceived.EUS_MessageID + "notification sent: " + notifResult);
            }
            else {
                return "-1";
            }

        }



        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "deletemessage/{profileid}/{messageid}/{referrerparentid}")]
        public string deleteMessage(string profileid, string messageid, string referrerparentid) {
            int iUserId = Convert.ToInt32(profileid);
            UpdateActivity(iUserId);
            //DataHelpers.UpdateEUS_Profiles_Activity(iUserId, getClientIP(), true);

            string result = "";
            if (referrerparentid != null && Convert.ToInt32(referrerparentid) > 0) {
                clsUserDoes.DeleteMessage(iUserId, Convert.ToInt32(messageid), true);
                result = "deleted";
            }
            else {
                clsUserDoes.DeleteMessage(iUserId, Convert.ToInt32(messageid), false);
                result = "deleted";
            }
            return result;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "deletemessages/{profileId}/{messagesView}/{messageId}")]
        public String deleteMessagesInView(string profileId, string messagesView, string messageId) {
            int iUserId = Convert.ToInt32(profileId);
            UpdateActivity(iUserId);

            try {
                MessagesViewEnum msgEnum = MessagesViewEnum.NEWMESSAGES;

                if (messagesView == "8") {
                    msgEnum = MessagesViewEnum.NEWMESSAGES;
                }
                else if (messagesView == "1") {
                    msgEnum = MessagesViewEnum.INBOX;
                }
                else if (messagesView == "2") {
                    msgEnum = MessagesViewEnum.SENT;
                }
                else if (messagesView == "16") {
                    msgEnum = MessagesViewEnum.TRASH;
                }


                bool isReferrer = (from p in myContext.vw_EusProfile_Lights
                                   where p.ProfileID == iUserId
                                   select p.ReferrerParentId.HasValue && p.ReferrerParentId.Value > 0).First();


                //DataHelpers.EUS_Profile_GetIsReferrer(Me.CMSDBDataContext, OtherMemberProfileID)
                EUS_Message message = clsUserDoes.GetMessage(Convert.ToInt32(messageId));
                if ((message != null))
                    clsMessagesHelper.DeleteMessagesFromMemberInView(iUserId, message.FromProfileID.Value, message.ToProfileID.Value, msgEnum, isReferrer);
            }
            catch (Exception ex) {
                WebErrorSendEmail(ex, "");
                return ex.ToString();
            }
            return "ok";
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "favorite_v2/{userid}/{zip}/{lat}/{lng}/{isOnline}")]
        public UserProfilesList getFavoriteListV2(string userid, string zip, string lat, string lng, string isOnline) {
            int iUserId = Convert.ToInt32(userid);
            UpdateActivity(iUserId);
            //DataHelpers.UpdateEUS_Profiles_Activity(Convert.ToInt32(userid), getClientIP(), true);

            bool ison = isOnline == "1" ? true : false;

            UserProfilesList pList = new UserProfilesList();
            pList.Profiles = new List<UserProfile>();

            clsMyListsHelperParameters parms = new clsMyListsHelperParameters();
            parms.CurrentProfileId = iUserId;
            parms.sorting = MyListsSortEnum.Recent;
            parms.ReturnRecordsWithStatus = (int)ProfileStatusEnum.Approved;
            parms.zipstr = zip;
            parms.latitudeIn = Convert.ToDouble(lat);
            parms.longitudeIn = Convert.ToDouble(lng);
            parms.Distance = clsSearchHelper.DISTANCE_DEFAULT;
            parms.isOnline = ison;

            parms.performCount = true;
            parms.rowNumberMin = 0;
            parms.rowNumberMax = 0;

            int count = 0;

            DataSet dt = clsSearchHelper.GetMyFavoriteMembersDataTable(parms);

            if (dt != null && dt.Tables[0] != null) {
                count = (int)dt.Tables[0].Rows[0][0];

                parms.performCount = false;
                parms.rowNumberMin = 0;
                parms.rowNumberMax = count;

                dt = clsSearchHelper.GetMyFavoriteMembersDataTable(parms);

                if (dt != null) {
                    pList = createUserProfilesListfromDataset(dt);

                }

            }

            return pList;
        }

        private UserProfilesList createUserProfilesListfromDataset(DataSet set) {
            UserProfilesList pList = new UserProfilesList();
            pList.Profiles = new List<UserProfile>();

            if (set != null) {
                DataTable dt = set.Tables[0];
                foreach (DataRow dr in dt.Rows) {
                    string path = "";
                    path = getImagePath_FromRow(dt, dr, Convert.ToInt32(dr["ProfileID"]), Convert.ToInt32(dr["GenderId"]), true);

                    if (string.IsNullOrEmpty(path)) {
                        string fileName = "";
                        DSMembers.EUS_CustomerPhotosRow photo = DataHelpers.GetProfilesDefaultPhoto(Convert.ToInt32(dr["ProfileID"]));
                        if (photo != null)
                            fileName = photo.FileName;
                        path = getImagePath(Convert.ToInt32(dr["ProfileID"]), fileName, Convert.ToInt32(dr["GenderId"]), true, false);
                    }

                    int PhotosApproved = -1;
                    if (dt.Columns.Contains("PhotosApproved")) {
                        PhotosApproved = 0;
                        if (!dr.IsNull("PhotosApproved"))
                            PhotosApproved = (int)dr["PhotosApproved"];
                    }


                    UserProfile p = new UserProfile()
                    {
                        FirstName = (string)dr["FirstName"],
                        LastName = (string)dr["LastName"],
                        UserRegion = (string)dr["Region"],
                        UserCity = (string)dr["City"],
                        UserAge = Convert.ToString(ProfileHelper.GetCurrentAge((DateTime)dr["Birthday"])),
                        ProfileId = Convert.ToString(dr["ProfileID"]),
                        UserName = (string)dr["LoginName"],
                        DateRegister = ((DateTime)dr["DateTimeToRegister"]).ToShortDateString(),
                        ImageUrl = path,
                        GenderId = Convert.ToString(dr["GenderId"]),
                        Latitude = (dt.Columns.Contains("latitude") ? Convert.ToString(dr["latitude"]) : null),
                        Longtitude = (dt.Columns.Contains("longitude") ? Convert.ToString(dr["longitude"]) : null),
                        UserCountry = (dt.Columns.Contains("Country") ? ProfileHelper.GetCountryName(dr["Country"].ToString()) : null),
                        PhotosCount = PhotosApproved.ToString(),
                    };

                    p.IsVip = (p.GenderId == "1") ? GetIsVip(dt, dr) : "0";
                    pList.Profiles.Add(p);
                }
            }




            return pList;

        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "favorite/{userid}/{zip}/{lat}/{lng}")]
        public UserProfilesList getFavoriteList(string userid, string zip, string lat, string lng) {
            int iUserId = Convert.ToInt32(userid);
            UpdateActivity(iUserId);
            //DataHelpers.UpdateEUS_Profiles_Activity(Convert.ToInt32(userid), getClientIP(), true);

            UserProfilesList pList = new UserProfilesList();
            pList.Profiles = new List<UserProfile>();

            clsMyListsHelperParameters parms = new clsMyListsHelperParameters();
            parms.CurrentProfileId = iUserId;
            parms.sorting = MyListsSortEnum.Recent;
            parms.ReturnRecordsWithStatus = (int)ProfileStatusEnum.Approved;
            parms.zipstr = zip;
            parms.latitudeIn = Convert.ToDouble(lat);
            parms.longitudeIn = Convert.ToDouble(lng);
            parms.Distance = clsSearchHelper.DISTANCE_DEFAULT;

            parms.performCount = true;
            parms.rowNumberMin = 0;
            parms.rowNumberMax = 0;

            int count = 0;

            DataSet dt = clsSearchHelper.GetMyFavoriteMembersDataTable(parms);

            if (dt != null && dt.Tables[0] != null) {
                count = (int)dt.Tables[0].Rows[0][0];

                parms.performCount = false;
                parms.rowNumberMin = 0;
                parms.rowNumberMax = count;

                dt = clsSearchHelper.GetMyFavoriteMembersDataTable(parms);

                if (dt != null) {
                    pList = createUserProfilesListfromDataset(dt);
                }
            }


            /*
            List<Int32> ids = new List<int>();
            foreach (DataRow dr in dt.Rows)
            {
                ids.Add(Convert.ToInt32(dr["ProfileID"]));
            }

            using (myContext)
            {

                foreach (int id in ids)
                {
                    EUS_Profile p = (from pr in myContext.EUS_Profiles
                                     where pr.ProfileID == id && pr.IsMaster == true
                                     select pr).FirstOrDefault();


                    string fileName = "";
                    DSMembers.EUS_CustomerPhotosRow photo = DataHelpers.GetProfilesDefaultPhoto(Convert.ToInt32(p.ProfileID));
                    if (photo != null)
                        fileName = photo.FileName;
                    //string path = ProfileHelper.GetProfileImageURL(Convert.ToInt32(p.ProfileID), fileName, Convert.ToInt32(p.GenderId), true, false);
                    string path = getImagePath(Convert.ToInt32(p.ProfileID), fileName, Convert.ToInt32(p.GenderId), true, false);

                    UserProfile prof = new UserProfile()
                    {
                        FirstName = p.FirstName,
                        LastName = p.LastName,
                        UserRegion = p.Region,
                        UserCity = p.City,
                        UserAge = Convert.ToString(ProfileHelper.GetCurrentAge(p.Birthday.Value)),
                        ProfileId = Convert.ToString(p.ProfileID),
                        UserName = p.LoginName,
                        DateRegister = p.DateTimeToRegister.Value.ToShortDateString(),
                        GenderId = Convert.ToString(p.GenderId.Value),
                        ImageUrl = path
                    };

                    pList.Profiles.Add(prof);
                }

            }
            */
            return pList;
        }
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "whoviewedme/{userid}/{zip}/{lat}/{lng}/{indexstart}")]
        public UserProfilesList getWhoViewedMe(string userid, string zip, string lat, string lng, string indexstart) {
            int iUserId = Convert.ToInt32(userid);
            UpdateActivity(iUserId);
            //DataHelpers.UpdateEUS_Profiles_Activity(Convert.ToInt32(userid), getClientIP(), true);

            clsMyListsHelperParameters parms = new clsMyListsHelperParameters();
            parms.CurrentProfileId = iUserId;
            parms.sorting = MyListsSortEnum.Recent;
            parms.ReturnRecordsWithStatus = (int)ProfileStatusEnum.Approved;
            parms.zipstr = zip;
            parms.latitudeIn = Convert.ToDouble(lat);
            parms.longitudeIn = Convert.ToDouble(lng);
            parms.Distance = clsSearchHelper.DISTANCE_DEFAULT;

            parms.performCount = false;
            parms.rowNumberMin = Convert.ToInt32(indexstart);
            parms.rowNumberMax = Convert.ToInt32(indexstart) + 20;

            DataSet dt = clsSearchHelper.GetWhoViewedMeMembersDataTable(parms);



            UserProfilesList list = new UserProfilesList();
            list.Profiles = new List<UserProfile>();

            list = createUserProfilesListfromDataset(dt);

            /*
            int start = Convert.ToInt32(indexstart);
            int results = 20;
            int max;
            if (start + results < dt.Rows.Count)
            {
                max = start + results;
            }
            else
            {
                max = dt.Rows.Count;
            }
            for (int i = start; i < max; i++)
            {
                DataRow r = dt.Rows[i];
                string fileName = "";
                DSMembers.EUS_CustomerPhotosRow photo = DataHelpers.GetProfilesDefaultPhoto(Convert.ToInt32(r["ProfileID"]));
                if (photo != null)
                    fileName = photo.FileName;
                //string path = ProfileHelper.GetProfileImageURL(Convert.ToInt32(r["ProfileID"]), fileName, Convert.ToInt32(r["GenderId"]), true, false);
                string path = getImagePath(Convert.ToInt32(r["ProfileID"]), fileName, Convert.ToInt32(r["GenderId"]), true, false);

                UserProfile p = new UserProfile()
                {
                    FirstName = (string)r["FirstName"],
                    LastName = (string)r["LastName"],
                    UserRegion = (string)r["Region"],
                    UserCity = (string)r["City"],
                    UserAge = Convert.ToString(ProfileHelper.GetCurrentAge((DateTime)r["Birthday"])),
                    ProfileId = Convert.ToString(r["ProfileID"]),
                    UserName = (string)r["LoginName"],
                    DateRegister = ((DateTime)r["DateTimeToRegister"]).ToShortDateString(),
                    ImageUrl = path,
                    GenderId = Convert.ToString(r["GenderId"])

                };

                list.Profiles.Add(p);
            }

             * */

            return list;
        }
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "whoviewedme_v2/{userid}/{zip}/{lat}/{lng}/{indexstart}/{isOnline}")]
        public UserProfilesList getWhoViewedMeV2(string userid, string zip, string lat, string lng, string indexstart, string isOnline) {
            UpdateActivity(Convert.ToInt32(userid));
            //DataHelpers.UpdateEUS_Profiles_Activity(Convert.ToInt32(userid), getClientIP(), true);
            bool ison = isOnline == "1" ? true : false;

            UserProfilesList list = new UserProfilesList();
            list.Profiles = new List<UserProfile>();

            clsMyListsHelperParameters parms = new clsMyListsHelperParameters();
            parms.CurrentProfileId = Convert.ToInt32(userid);
            parms.sorting = MyListsSortEnum.Recent;
            parms.ReturnRecordsWithStatus = (int)ProfileStatusEnum.Approved;
            parms.zipstr = zip;
            parms.latitudeIn = Convert.ToDouble(lat);
            parms.longitudeIn = Convert.ToDouble(lng);
            parms.Distance = clsSearchHelper.DISTANCE_DEFAULT;

            parms.performCount = false;
            parms.rowNumberMin = Convert.ToInt32(indexstart);
            parms.rowNumberMax = Convert.ToInt32(indexstart) + 20;
            parms.isOnline = ison;

            DataSet dt = clsSearchHelper.GetWhoViewedMeMembersDataTable(parms);

            list = createUserProfilesListfromDataset(dt);



            return list;
        }


        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "viewprofile/{myprofileid}/{viewprofileid}")]
        public string viewProfile(string myprofileid, string viewprofileid) {
            int iMyprofileid = Convert.ToInt32(myprofileid);
            int iViewprofileid = Convert.ToInt32(viewprofileid);

            UpdateActivity(iMyprofileid);
            //DataHelpers.UpdateEUS_Profiles_Activity(iMyprofileid, getClientIP(), true);

            try {
                clsUserDoes.MarkAsViewed(myContext, iViewprofileid, iMyprofileid);
                clsUserDoes.MarkDateOfferAsViewed(myContext, iViewprofileid, iMyprofileid);


                // update EUS_ProfilesViewed records set IsToProfileIDViewed to true, where  ToProfileID is current member
                EUS_ProfilesViewed viewedMe = (from itm in myContext.EUS_ProfilesVieweds
                                               where itm.FromProfileID == iViewprofileid && itm.ToProfileID == iMyprofileid &&
                                               (itm.IsToProfileIDViewed == null || itm.IsToProfileIDViewed == false)
                                               select itm).FirstOrDefault();

                EUS_ProfilePhotosLevel sharedPhotosToMe = (from itm in myContext.EUS_ProfilePhotosLevels
                                                           where itm.FromProfileID == iViewprofileid && itm.ToProfileID == iMyprofileid &&
                                                              (itm.IsToProfileIDViewed == null || itm.IsToProfileIDViewed == false)
                                                           select itm).FirstOrDefault();


                if (viewedMe != null || sharedPhotosToMe != null) {
                    if (viewedMe != null)
                        viewedMe.IsToProfileIDViewed = true;
                    if (sharedPhotosToMe != null)
                        sharedPhotosToMe.IsToProfileIDViewed = true;

                    myContext.SubmitChanges();
                    clsUserDoes.ResetMemberCounter(MemberCountersEnum.WhoViewedMe | MemberCountersEnum.WhoSharedPhotos, iMyprofileid);
                }
            }
            catch (Exception ex) {
                WebErrorSendEmail(ex, "");
            }
            finally {
                myContext.Dispose();
            }
            return "ok";
        }


        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "favorite/{myuserid}/{userid}")]
        public string Favorite(string myuserid, string userid) {
            UpdateActivity(Convert.ToInt32(myuserid));
            //DataHelpers.UpdateEUS_Profiles_Activity(Convert.ToInt32(myuserid), getClientIP(), true);

            clsUserDoes.MarkAsFavorite(Convert.ToInt32(userid), Convert.ToInt32(myuserid));
            return "ok";
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "unfavorite/{myuserid}/{userid}")]
        public string UnFavorite(string myuserid, string userid) {
            UpdateActivity(Convert.ToInt32(myuserid));
            //DataHelpers.UpdateEUS_Profiles_Activity(Convert.ToInt32(myuserid), getClientIP(), true);

            clsUserDoes.MarkAsUnfavorite(Convert.ToInt32(userid), Convert.ToInt32(myuserid));
            return "ok";
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "checkfavorite/{myuserid}/{userid}")]
        public string checkFavorite(string myuserid, string userid) {
            if (clsUserDoes.IsFavorited(Convert.ToInt32(userid), Convert.ToInt32(myuserid))) {
                return "true";
            }
            else {
                return "false";
            }
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "checkhasphotos/{userid}")]
        public string checkHasPhotos(string userid) {
            bool result = false;
            int? mp = 0;
            using (myContext) {
                mp = (from p in myContext.vw_EusProfile_Lights
                      where p.ProfileID == Convert.ToInt32(userid) && p.IsMaster == true
                      select p.MirrorProfileID).FirstOrDefault();

                myContext.Dispose();
            }

            result = clsUserDoes.HasPhotos(Convert.ToInt32(userid), Convert.ToInt32(mp));
            if (result) {
                return "true";
            }
            else {
                return "false";
            }


        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "like/{myprofileid}/{otherprofileid}")]
        public string like(string myprofileid, string otherprofileid) {
            UpdateActivity(Convert.ToInt32(myprofileid));
            //DataHelpers.UpdateEUS_Profiles_Activity(Convert.ToInt32(myprofileid), getClientIP(), true);

            string result = "";

            bool res = false;
            int? mp = 0;
            using (myContext) {
                mp = (from p in myContext.vw_EusProfile_Lights
                      where p.ProfileID == Convert.ToInt32(myprofileid) && p.IsMaster == true
                      select p.MirrorProfileID).FirstOrDefault();

                myContext.Dispose();
            }
            res = clsUserDoes.HasPhotos(Convert.ToInt32(myprofileid), Convert.ToInt32(mp));
            bool currentUserHasPhotos = res;

            if (currentUserHasPhotos) {
                clsUserDoes.SendWink(Convert.ToInt32(otherprofileid), Convert.ToInt32(myprofileid));
                sendNotificationToUser(myprofileid, otherprofileid, "2");
                result = "done";
            }
            else {
                result = "nophotos";
            }
            return result;

        }


        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "poke/{myprofileid}/{otherprofileid}/{parentOffer}")]
        public string poke(string myprofileid, string otherprofileid, string parentOffer) {
            UpdateActivity(Convert.ToInt32(myprofileid));
            //DataHelpers.UpdateEUS_Profiles_Activity(Convert.ToInt32(myprofileid), getClientIP(), true);

            string result = "";

            bool res = false;
            int? mp = 0;
            using (myContext) {
                mp = (from p in myContext.vw_EusProfile_Lights
                      where p.ProfileID == Convert.ToInt32(myprofileid) && p.IsMaster == true
                      select p.MirrorProfileID).FirstOrDefault();

                myContext.Dispose();
            }

            res = clsUserDoes.HasPhotos(Convert.ToInt32(myprofileid), Convert.ToInt32(mp));
            bool currentUserHasPhotos = res; ;
            if (currentUserHasPhotos) {
                EUS_Offer offerRec = clsUserDoes.GetMyPendingPoke(Convert.ToInt32(otherprofileid), Convert.ToInt32(myprofileid));

                if (offerRec == null) {

                    clsUserDoes.NewOfferParameters parms = new clsUserDoes.NewOfferParameters();
                    parms.userIdWhoDid = Convert.ToInt32(myprofileid);
                    parms.userIdReceiver = -1;
                    parms.offerAmount = -1;
                    parms.parentOfferId = Convert.ToInt32(parentOffer);
                    parms.messageText1 = "";
                    parms.messageText2 = "";

                    clsUserDoes.SendPoke(parms);
                    clsUserDoes.AcceptLike(parms, OfferStatusEnum.LIKE_ACCEEPTED_WITH_POKE);
                    result = "done";
                }
                else {
                    result = "already";
                }
            }
            else {
                result = "nophotos";
            }
            return result;
        }


        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "checkactions/{myprofileid}/{otherprofileid}")]
        public UserActions getUserActions(string myprofileid, string otherprofileid) {
            UserActions ua = new UserActions();

            ua.viewprofile = viewProfile(myprofileid, otherprofileid);
            ua.like = checkLike(myprofileid, otherprofileid);
            ua.favorite = checkFavorite(myprofileid, otherprofileid);
            ua.photolevel = base.getProfilePhotoLevel(myprofileid, otherprofileid);
            ua.minlevel = getMinLevel(myprofileid);

            ua.like = (ua.like == "true" ? "1" : "0");
            ua.favorite = (ua.favorite == "true" ? "1" : "0");
            return ua;
        }


        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "checklike/{myprofileid}/{otherprofileid}")]
        public string checkLike(string myprofileid, string otherprofileid) {
            bool hasCommunication = clsUserDoes.HasCommunication(Convert.ToInt32(otherprofileid), Convert.ToInt32(myprofileid));
            bool IsAnyMessageExchanged = clsUserDoes.IsAnyMessageExchanged(Convert.ToInt32(myprofileid), Convert.ToInt32(myprofileid));
            bool hasAnyOffer = clsUserDoes.HasAnyOffer(Convert.ToInt32(otherprofileid), Convert.ToInt32(myprofileid));

            if (!hasCommunication && !IsAnyMessageExchanged && !hasAnyOffer) {
                return "true";
            }
            else {
                return "false";
            }
        }


        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "whofavme/{userid}/{zip}/{lat}/{lng}")]
        public UserProfilesList getWhoFavoritedMe(string userid, string zip, string lat, string lng) {
            UpdateActivity(Convert.ToInt32(userid));
            //DataHelpers.UpdateEUS_Profiles_Activity(Convert.ToInt32(userid), getClientIP(), true);

            UserProfilesList list = new UserProfilesList();
            list.Profiles = new List<UserProfile>();

            clsMyListsHelperParameters parms = new clsMyListsHelperParameters();
            parms.CurrentProfileId = Convert.ToInt32(userid);
            parms.sorting = MyListsSortEnum.Recent;
            parms.ReturnRecordsWithStatus = (int)ProfileStatusEnum.Approved;
            parms.zipstr = zip;
            parms.latitudeIn = Convert.ToDouble(lat);
            parms.longitudeIn = Convert.ToDouble(lng);
            parms.Distance = clsSearchHelper.DISTANCE_DEFAULT;

            parms.performCount = true;
            parms.rowNumberMin = 0;
            parms.rowNumberMax = 0;

            int count = 0;

            DataSet dt = clsSearchHelper.GetWhoFavoritedMeMembersDataTable(parms);

            if (dt != null && dt.Tables[0] != null) {
                count = (int)dt.Tables[0].Rows[0][0];

                parms.performCount = false;
                parms.rowNumberMin = 0;
                parms.rowNumberMax = count;

                dt = clsSearchHelper.GetWhoFavoritedMeMembersDataTable(parms);

                if (dt != null) {
                    list = createUserProfilesListfromDataset(dt);
                }
            }

            return list;
        }


        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "counters/{userid}")]
        public clsGetMemberActionsCounters getCounters(string userid) {
            int MasterProfileId = Convert.ToInt32(userid);
            clsGetMemberActionsCounters counters = clsUserDoes.GetMemberActionsCounters(MasterProfileId);
            return counters;
        }


        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "messagescount/{userid}")]
        public int getMessagesCount(string userid) {
            int MasterProfileId = Convert.ToInt32(userid);
            clsGetMemberActionsCounters counters = clsUserDoes.GetMemberActionsCounters(MasterProfileId);
            return counters.NewMessages;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "likescount/{userid}")]
        public int getLikesCount(string userid) {
            int MasterProfileId = Convert.ToInt32(userid);
            clsGetMemberActionsCounters counters = clsUserDoes.GetMemberActionsCounters(MasterProfileId);
            return counters.NewLikes;
        }


        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "likes/{mod}/{userid}/{zip}/{lat}/{lng}/{indexstart}")]
        public OfferList getLikes(string mod, string userid, string zip, string lat, string lng, string indexstart) {

            UpdateActivity(Convert.ToInt32(userid));
            //DataHelpers.UpdateEUS_Profiles_Activity(Convert.ToInt32(userid), getClientIP(), true);

            int mins = clsConfigValues.Get__members_online_minutes();
            DateTime lastActivityUTCDate = DateTime.UtcNow.AddMinutes(-mins);

            int mode = Convert.ToInt32(mod);
            DataSet ds = null;
            OfferList offerList = new OfferList();
            offerList.offers = new List<Offer>();


            clsWinksHelperParameters parms = new clsWinksHelperParameters();
            parms.CurrentProfileId = Convert.ToInt32(userid);
            parms.sorting = OffersSortEnum.RecentOffers;
            parms.ReturnRecordsWithStatus = (int)ProfileStatusEnum.Approved;
            parms.zipstr = zip;
            parms.latitudeIn = Convert.ToDouble(lat);
            parms.longitudeIn = Convert.ToDouble(lng);
            parms.Distance = clsSearchHelper.DISTANCE_DEFAULT;

            //parms.NumberOfRecordsToReturn = NumberOfRecordsToReturn;

            parms.performCount = false;
            parms.rowNumberMin = Convert.ToInt32(indexstart);
            parms.rowNumberMax = Convert.ToInt32(indexstart) + 20;


            switch (mode) {
                case 0:
                    ds = clsSearchHelper.GetWinksDataTable(ref parms);

                    break;

                case 1:
                    ds = clsSearchHelper.GetPendingLikesDataTable(ref parms);
                    break;

                case 2:
                    ds = clsSearchHelper.GetAcceptedLikesDataTable(ref parms);
                    break;

                case 3:
                    ds = clsSearchHelper.GetRejectedLikesDataTable(ref parms);
                    break;
            }


            DataTable dt = ds.Tables[0];
            for (int i = 0; i < dt.Rows.Count; i++) {
                DataRow dr = dt.Rows[i];

                string path = "";
                path = getImagePath_FromRow(ds.Tables[0], dr, Convert.ToInt32(dr["ProfileID"]), Convert.ToInt32(dr["GenderId"]), true);

                if (string.IsNullOrEmpty(path)) {
                    string fileName = "";

                    DSMembers.EUS_CustomerPhotosRow photo = DataHelpers.GetProfilesDefaultPhoto(Convert.ToInt32(dr["ProfileID"]));
                    if (photo != null)
                        fileName = photo.FileName;
                    //string path = ProfileHelper.GetProfileImageURL(Convert.ToInt32(r["ProfileID"]), fileName, Convert.ToInt32(r["GenderId"]), true, false);
                    path = getImagePath(Convert.ToInt32(dr["ProfileID"]), fileName, Convert.ToInt32(dr["GenderId"]), true, false);
                }

                bool OtherMemberIsOnline = ServiceBase.GetIsOnline(lastActivityUTCDate, ds.Tables[0], dr);

                int PhotosApproved = -1;
                if (ds.Tables[0].Columns.Contains("PhotosApproved")) {
                    PhotosApproved = 0;
                    if (!dr.IsNull("PhotosApproved"))
                        PhotosApproved = (int)dr["PhotosApproved"];
                }

                Offer offer = new Offer();
                UserProfile p = new UserProfile()
                {
                    FirstName = (string)dr["FirstName"],
                    LastName = (string)dr["LastName"],
                    UserRegion = (string)dr["Region"],
                    UserCity = (string)dr["City"],
                    UserCountry = ProfileHelper.GetCountryName((string)dr["Country"]),
                    UserAge = Convert.ToString(ProfileHelper.GetCurrentAge((DateTime)dr["Birthday"])),
                    ProfileId = Convert.ToString(dr["ProfileID"]),
                    UserName = (string)dr["LoginName"],
                    DateRegister = ((DateTime)dr["DateTimeToRegister"]).ToShortDateString(),
                    ImageUrl = path,
                    GenderId = Convert.ToString(dr["GenderId"]),
                    Latitude = (dt.Columns.Contains("latitude") ? Convert.ToString(dr["latitude"]) : null),
                    Longtitude = (dt.Columns.Contains("longitude") ? Convert.ToString(dr["longitude"]) : null),
                    IsOnline = OtherMemberIsOnline.ToString(),
                    PhotosCount = PhotosApproved.ToString()
                };

                offer.profile = p;
                offer.offerId = Convert.ToString(dr["OfferID"]);

                offerList.offers.Add(offer);
            }


            return offerList;
        }


        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "likeactions/{userid}/{action}/{offerid}")]
        public string likeActions(string userid, string action, string offerid) {
            UpdateActivity(Convert.ToInt32(userid));
            //DataHelpers.UpdateEUS_Profiles_Activity(Convert.ToInt32(userid), getClientIP(), true);

            int action_num = Convert.ToInt32(action);
            switch (action_num) {
                //cancel pending
                case 1:
                    clsUserDoes.CancelWinkOrOffer(Convert.ToInt32(offerid));
                    return "ok";
                //delete
                case 2:
                    clsUserDoes.DeleteOfferAndParents(Convert.ToInt32(offerid));
                    return "ok";
            }

            return "error";
        }


        [WebInvoke(Method = "GET", UriTemplate = "rejectlike/{offerid}/{reason}")]
        public void rejectLike(string offerid, string reason) {
            int reasonInt = Convert.ToInt32(reason);
            OfferControlCommandEnum reasonEnum = OfferControlCommandEnum.REJECTTYPE;
            switch (reasonInt) {
                case 5:
                    reasonEnum = OfferControlCommandEnum.REJECTTYPE;
                    break;
                case 6:
                    reasonEnum = OfferControlCommandEnum.REJECTFAR;
                    break;

                case 7:
                    reasonEnum = OfferControlCommandEnum.REJECTBAD;
                    break;

                case 8:
                    reasonEnum = OfferControlCommandEnum.REJECTEXPECTATIONS;
                    break;

            }

            clsUserDoes.RejectOffer(Convert.ToInt32(offerid), reasonEnum.ToString());
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "offers/{mod}/{userid}/{zip}/{lat}/{lng}/{indexstart}")]
        public OfferList getOffers(string mod, string userid, string zip, string lat, string lng, string indexstart) {
            UpdateActivity(Convert.ToInt32(userid));
            //DataHelpers.UpdateEUS_Profiles_Activity(Convert.ToInt32(userid), getClientIP(), true);

            int mode = Convert.ToInt32(mod);
            DataTable dt = null;
            OfferList offerList = new OfferList();
            offerList.offers = new List<Offer>();
            clsWinksHelperParameters parms = new clsWinksHelperParameters();
            parms.CurrentProfileId = Convert.ToInt32(userid);
            parms.sorting = OffersSortEnum.RecentOffers;
            parms.ReturnRecordsWithStatus = 4;
            parms.zipstr = zip;
            parms.latitudeIn = Convert.ToDouble(lat);
            parms.longitudeIn = Convert.ToDouble(lng);
            parms.Distance = clsSearchHelper.DISTANCE_DEFAULT;
            //parms.NumberOfRecordsToReturn = NumberOfRecordsToReturn
            parms.performCount = true;
            parms.rowNumberMin = Convert.ToInt32(indexstart);
            parms.rowNumberMax = Convert.ToInt32(indexstart) + 20;
            switch (mode) {   //new

                case 0:

                    dt = clsSearchHelper.GetNewOffersDataTable(ref parms).Tables[1];
                    //pending
                    break;

                case 1:
                    dt = clsSearchHelper.GetPendingOffersDataTable(ref parms).Tables[1];
                    break;

                //accepted
                case 2:
                    dt = clsSearchHelper.GetAcceptedOffersDataTable(ref parms).Tables[1];
                    break;
                //rejected
                case 3:
                    dt = clsSearchHelper.GetRejectedOffersDataTable(ref parms).Tables[1];
                    break;
            }

            if (dt != null) {

                int start = Convert.ToInt32(indexstart);
                int results = 20;
                int max;
                if (start + results < dt.Rows.Count) {
                    max = start + results;
                }
                else {
                    max = dt.Rows.Count;
                }

                for (int i = start; i < max; i++) {
                    DataRow dr = dt.Rows[i];

                    string path = "";
                    path = getImagePath_FromRow(dt, dr, Convert.ToInt32(dr["ProfileID"]), Convert.ToInt32(dr["GenderId"]), true);

                    if (string.IsNullOrEmpty(path)) {
                        string fileName = "";
                        DSMembers.EUS_CustomerPhotosRow photo = DataHelpers.GetProfilesDefaultPhoto(Convert.ToInt32(dr["ProfileID"]));
                        if (photo != null)
                            fileName = photo.FileName;
                        //string path = ProfileHelper.GetProfileImageURL(Convert.ToInt32(r["ProfileID"]), fileName, Convert.ToInt32(r["GenderId"]), true, false);
                        path = getImagePath(Convert.ToInt32(dr["ProfileID"]), fileName, Convert.ToInt32(dr["GenderId"]), true, false);
                    }

                    int PhotosApproved = -1;
                    if (dt.Columns.Contains("PhotosApproved")) {
                        PhotosApproved = 0;
                        if (!dr.IsNull("PhotosApproved"))
                            PhotosApproved = (int)dr["PhotosApproved"];
                    }

                    Offer offer = new Offer();
                    UserProfile p = new UserProfile()
                    {
                        FirstName = (string)dr["FirstName"],
                        LastName = (string)dr["LastName"],
                        UserRegion = (string)dr["Region"],
                        UserCity = (string)dr["City"],
                        UserCountry = ProfileHelper.GetCountryName((string)dr["Country"]),
                        UserAge = Convert.ToString(ProfileHelper.GetCurrentAge((DateTime)dr["Birthday"])),
                        ProfileId = Convert.ToString(dr["ProfileID"]),
                        UserName = (string)dr["LoginName"],
                        DateRegister = ((DateTime)dr["DateTimeToRegister"]).ToShortDateString(),
                        ImageUrl = path,
                        GenderId = Convert.ToString(dr["GenderId"]),
                        Latitude = (dt.Columns.Contains("latitude") ? Convert.ToString(dr["latitude"]) : null),
                        Longtitude = (dt.Columns.Contains("longitude") ? Convert.ToString(dr["longitude"]) : null),
                        PhotosCount = PhotosApproved.ToString()

                    };

                    offer.profile = p;
                    offer.offerId = Convert.ToString(dr["OfferID"]);
                    offer.rejectReason = Convert.ToString(dr["OffersStatusID"]);

                    offerList.offers.Add(offer);
                }

            }

            return offerList;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "makeoffer/{mode}/{amount}/{fromprofileid}/{toprofileid}/{genderid}/{offerid}")]
        public string makeOffer(string mode, string amount, string fromprofileid, string toprofileid, string genderid, string offerid) {
            UpdateActivity(Convert.ToInt32(fromprofileid));
            //DataHelpers.UpdateEUS_Profiles_Activity(Convert.ToInt32(fromprofileid), getClientIP(), true);

            try {
                int requestOfferId = Convert.ToInt32(offerid);

                clsUserDoes.NewOfferParameters parms = new clsUserDoes.NewOfferParameters();
                parms.messageText1 = "";
                parms.messageText2 = "";
                parms.offerAmount = Convert.ToInt32(amount);
                parms.userIdReceiver = Convert.ToInt32(toprofileid);
                parms.userIdWhoDid = Convert.ToInt32(fromprofileid);
                parms.parentOfferId = Convert.ToInt32(offerid);

                EUS_Offer offer = clsUserDoes.GetLastOfferAny(Convert.ToInt32(toprofileid), Convert.ToInt32(fromprofileid));

                int modeInt = Convert.ToInt32(mode);
                if (modeInt == 1)//new offer
                {

                    if (offer != null) {
                        if ((offer.OfferTypeID != ProfileHelper.OfferTypeID_OFFERNEW && offer.OfferTypeID != ProfileHelper.OfferTypeID_OFFERCOUNTER)) {

                            parms.parentOfferId = Convert.ToInt32(offer.OfferID);
                            int createdOfferId = parms.childOfferId = clsUserDoes.MakeOffer(parms);
                            if (offer.OfferTypeID == ProfileHelper.OfferTypeID_WINK) {
                                clsUserDoes.AcceptLike(parms, OfferStatusEnum.LIKE_ACCEEPTED_WITH_OFFER);
                            }
                            else if (offer.OfferTypeID == ProfileHelper.OfferTypeID_POKE) {
                                clsUserDoes.AcceptPoke(parms, OfferStatusEnum.POKE_ACCEEPTED_WITH_OFFER);

                            }

                            string notifResult = sendNotificationToUser(fromprofileid, toprofileid, "1");
                            return "new offer created: last offer id: " + offer.OfferID.ToString() + ", new offer id:" + createdOfferId.ToString();
                        }
                        else {
                            return "offer already";
                        }
                    }
                    else {
                        int createdOfferId = parms.childOfferId = clsUserDoes.MakeOffer(parms);
                        return "new offer created - last offer:null , new offer id:" + createdOfferId.ToString();

                    }

                } //counter offer              
                else if (modeInt == 2) {
                    if (offer != null) {
                        clsUserDoes.CounterOffer(Convert.ToInt32(offer.OfferID), Convert.ToInt32(amount));
                        return "counter offer created - last offer id: " + offer.OfferID.ToString();
                    }
                }

            }
            catch (Exception ex) {
                WebErrorSendEmail(ex, "");
                return "ERROR:\n" + ex.ToString();
            }

            return "";

        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "acceptoffer/{myprofileid}/{offerid}")]
        public string acceptOffer(string myprofileid, string offerid) {
            UpdateActivity(Convert.ToInt32(myprofileid));

            try {
                int id = Convert.ToInt32(offerid);
                clsUserDoes.AcceptOffer(id, OfferStatusEnum.ACCEPTED);

                return "ok";

            }
            catch (Exception ex) {
                WebErrorSendEmail(ex, "");
                return "Error";
            }
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "hash/{username}")]
        public string getHash(string username) {
            return AppUtils.MD5_GetString(username + "5686-android-1");
        }


        public long getLastOfferId(string fromprofile, string toprofile) {
            EUS_Offer offer = clsUserDoes.GetLastOfferAny(Convert.ToInt32(toprofile), Convert.ToInt32(fromprofile));
            if (offer != null) {
                return offer.OfferID;
            }
            else {
                return -1;
            }

        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "countries")]
        public CountryList getCountries() {
            CountryList list = new CountryList();
            list.countries = new List<Country>();

            DataTable dt = SYS_CountriesGEOHelper.gDSListsGEO__IsEnabled_PrintableNameASC.SYS_CountriesGEO;

            var q = (from row in dt.AsEnumerable()
                     select new
                     {

                         name = (string)row["PrintableName"],
                         iso = (string)row["Iso"]

                     }).ToList();

            for (int i = 0; i < q.Count; i++) {
                Country country = new Country()
                {
                    name = q[i].name,
                    iso = q[i].iso
                };
                list.countries.Add(country);
            }

            return list;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "regions/{countryIso}/{lagid}")]
        public Regions getCountryRegions(string countryIso, string lagid) {
            DataTable dt = clsGeoHelper.GetCountryRegions(countryIso, lagid);
            Regions regions = new Regions();
            regions.list = new List<string>();

            foreach (DataRow r in dt.AsEnumerable()) {
                regions.list.Add((string)r["region1"]);

            }

            return regions;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "cities/{countryIso}/{region}/{lagid}")]
        public Cities getRegionCities(string countryIso, string region, string lagid) {
            string regionDec = HttpUtility.UrlDecode(region);

            DataTable dt = clsGeoHelper.GetCountryRegionCities(countryIso, regionDec, lagid);
            Cities cities = new Cities();
            cities.list = new List<string>();

            foreach (DataRow r in dt.AsEnumerable()) {
                cities.list.Add((string)r["city"]);
            }

            return cities;
        }


        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "zip/{countryIso}/{region}/{city}/{lagid}")]
        public string getZipCode(string countryIso, string region, string city, string lagid) {
            string value = "0000";
            DataTable dt = clsGeoHelper.GetCountryCityPostalcodes(countryIso, region, city, lagid);
            if (dt.Rows.Count > 0) {
                value = (string)dt.Rows[0]["postcode"];
            }
            return value;
        }


        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "defaultphoto/{myprofileid}/{photoid}")]
        public string setDefaultPhoto(string myprofileid, string photoid) {
            try {
                int iPhotoId = Convert.ToInt32(photoid);
                int iMyprofileid = Convert.ToInt32(myprofileid);

                UpdateActivity(iMyprofileid);
                long CustomerPhotosID = DataHelpers.EUS_CustomerPhotos_SetDefault(iMyprofileid, iPhotoId, true, false);
                if (CustomerPhotosID > 0) {
                    clsCustomer.AddFreeRegistrationCredits_WithCheck(iMyprofileid, "", "", "");
                }

                //int photoId = Convert.ToInt32(photoid);

                //DSMembers ds = DataHelpers.GetEUS_CustomerPhotos_ByProfileOrMirrorID(Convert.ToInt32(myprofileid));
                //foreach (DSMembers.EUS_CustomerPhotosRow rawRecord in ds.EUS_CustomerPhotos.Rows) {
                //    if ((rawRecord.CustomerPhotosID == photoId && rawRecord.HasAproved == true)) {
                //        rawRecord.IsDefault = true;

                //    }
                //    else {
                //        rawRecord.IsDefault = false;
                //    }
                //}
                //DataHelpers.UpdateEUS_CustomerPhotos(ref ds);
                //ds.Dispose();
            }
            catch (Exception ex) {
                WebErrorSendEmail(ex, "");
                return "error";
            }
            return "ok";
        }


        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "send_notifications/{message}")]
        public override string sendNotificationsToAllUsers(string message) {
            return base.sendNotificationsToAllUsers(message);


        }

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "notification_tolist")]
        public string sendNotificationToUserList(NotificationParams p) {
            string response = base.sendNotificationToUserList1((GoomenaLib.NotificationParams)p);
            //try {
            //    using (myContext) {
            //        for (int i = 0; i < p.deviceIds.Count; i++) {
            //            string deviceId = p.deviceIds[i];
            //            response += SendNotification(deviceId, p.message);
            //        }

            //        myContext.Dispose();
            //    }
            //}
            //catch (Exception ex) {
            //    WebErrorSendEmail(ex, "");
            //    response += "exception: " + ex.ToString();
            //}
            return response;
        }


        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "notification/{fromProfileId}/{toProfileId}/{type}")]
        public override string sendNotificationToUser(string fromProfileId, string toProfileId, string type) {
            return base.sendNotificationToUser(fromProfileId, toProfileId, type);
        }


        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "storeGCMUser")]
        public string storeGCMUser(StoreUserParams p) {

            try {
                using (myContext) {


                    EUS_GCMProfile profile = new EUS_GCMProfile()
                    {
                        ProfileID = Convert.ToInt32(p.profileId),
                        GCM_RegID = p.regId,
                        LoginName = p.loginname,
                        Email = p.email,
                        CreatedDate = DateTime.Now,
                        Notif_Messages = 1,
                        Notif_Likes = 1,
                        Notif_Offers = 1
                    };
                    myContext.EUS_GCMProfiles.InsertOnSubmit(profile);
                    myContext.SubmitChanges();

                    myContext.Dispose();

                }

                return "OK";

            }
            catch (Exception ex) {
                WebErrorSendEmail(ex, "");
                return ex.ToString();
            }

        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "removeGCMUser/{regId}")]
        public string removeGCMUser(string regId) {
            string msg = "";

            try {

                using (myContext) {
                    EUS_GCMProfile profile = (from p in myContext.EUS_GCMProfiles
                                              where p.GCM_RegID == regId
                                              select p).FirstOrDefault();

                    if (profile != null) {
                        myContext.EUS_GCMProfiles.DeleteOnSubmit(profile);
                        myContext.SubmitChanges();
                        msg += "ok";
                    }
                    else {
                        msg += "not found";
                    }

                    myContext.Dispose();

                }

            }
            catch (Exception ex) {
                WebErrorSendEmail(ex, "");
                msg += "error: " + ex.ToString();
            }
            return msg;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "gcmusers")]
        public List<ProfileDeviceId> getGCMUsers() {
            List<ProfileDeviceId> pList = new List<ProfileDeviceId>();

            using (myContext) {

                List<EUS_GCMProfile> profiles = (from p in myContext.EUS_GCMProfiles
                                                 where p.GCM_RegID != null
                                                 select p).ToList();


                for (int i = 0; i < profiles.Count; i++) {
                    vw_EusProfile_Light pr = (from p in myContext.vw_EusProfile_Lights
                                              where p.ProfileID == Convert.ToInt32(profiles[i].ProfileID) && p.Status == (int)ProfileStatusEnum.Approved
                                              select p).FirstOrDefault();

                    UserProfile prof = createProfile(pr, "");

                    ProfileDeviceId item = new ProfileDeviceId();
                    item.deviceId = profiles[i].GCM_RegID;
                    item.profile = prof;
                    pList.Add(item);
                }

                myContext.Dispose();
            }

            return pList;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "savesettings/{profileId}/{notifMessages}/{notifLikes}/{notifOffers}")]
        public override string saveGCMUserSettings(string profileId, string notifMessages, string notifLikes, string notifOffers) {
            return base.saveGCMUserSettings(profileId, notifMessages, notifLikes, notifOffers);
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "settings/{profileId}")]
        public GCMUserSettings getSettings(string profileId) {
            GCMUserSettings settings = new GCMUserSettings();

            UpdateActivity(Convert.ToInt32(profileId));
            using (myContext) {

                EUS_GCMProfile p = (from s in myContext.EUS_GCMProfiles
                                    where s.ProfileID == Convert.ToInt32(profileId)
                                    select s).FirstOrDefault();

                if (p != null) {

                    settings.profileId = Convert.ToString(p.ProfileID);
                    settings.notifMessages = Convert.ToString(p.Notif_Messages);
                    settings.notifLikes = Convert.ToString(p.Notif_Likes);
                    settings.notifOffers = Convert.ToString(p.Notif_Offers);

                }
                myContext.Dispose();
            }
            return settings;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "photolevel/{fromProfileId}/{toProfileId}/{photoLevel}")]
        public string setProfilePhotoLevel(string fromProfileId, string toProfileId, string photoLevel) {
            string result = "";
            try {
                UpdateActivity(Convert.ToInt32(fromProfileId));
                clsUserDoes.SharePhoto(Convert.ToInt32(fromProfileId), Convert.ToInt32(toProfileId), Convert.ToInt32(photoLevel));
                result = "OK";

                //using (myContext) {

                //    EUS_ProfilePhotosLevel photosLevel = (from itm in myContext.EUS_ProfilePhotosLevels
                //                                          where itm.FromProfileID == Convert.ToInt32(fromProfileId)
                //                                          && itm.ToProfileID == Convert.ToInt32(toProfileId)
                //                                          select itm).FirstOrDefault();

                //    if (photosLevel != null) {
                //        photosLevel.PhotoLevelID = Convert.ToInt32(photoLevel);
                //    }
                //    else {
                //        photosLevel = new EUS_ProfilePhotosLevel();
                //        photosLevel.FromProfileID = Convert.ToInt32(fromProfileId);
                //        photosLevel.ToProfileID = Convert.ToInt32(toProfileId);
                //        photosLevel.DateTimeCreated = DateTime.UtcNow;
                //        photosLevel.PhotoLevelID = Convert.ToInt32(photoLevel);
                //        myContext.EUS_ProfilePhotosLevels.InsertOnSubmit(photosLevel);

                //    }

                //    myContext.SubmitChanges();
                //    result = "OK";
                //    //result = ""+photosLevel.PhotoLevelID.Value;

                //    myContext.Dispose();
                //}
            }
            catch (Exception ex) {
                WebErrorSendEmail(ex, "");
                result = "error" + ex.ToString();
            }
            return result;
        }


        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "getPhotoLevel/{fromProfileId}/{toProfileId}")]
        public override string getProfilePhotoLevel(string fromProfileId, string toProfileId) {
            return base.getProfilePhotoLevel(fromProfileId, toProfileId);
            //string result = "";
            //try {
            //    using (myContext) {
            //        EUS_ProfilePhotosLevel photosLevel = (from itm in myContext.EUS_ProfilePhotosLevels
            //                                              where itm.FromProfileID == Convert.ToInt32(fromProfileId)
            //                                              && itm.ToProfileID == Convert.ToInt32(toProfileId)
            //                                              select itm).FirstOrDefault();

            //        if (photosLevel != null) {
            //            result = Convert.ToString(photosLevel.PhotoLevelID);
            //        }
            //        else {
            //            result = "0";
            //        }

            //        myContext.Dispose();
            //    }
            //}
            //catch (Exception) {
            //    result = "error";
            //}
            //return result;
        }


        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "whoSharedMePhotos/{userid}/{zip}/{lat}/{lng}/{indexstart}")]
        public UserProfilesList getWhoSharedMePhotos(string userid, string zip, string lat, string lng, string indexstart) {
            UpdateActivity(Convert.ToInt32(userid));

            UserProfilesList list = new UserProfilesList();
            list.Profiles = new List<UserProfile>();

            clsMyListsHelperParameters parms = new clsMyListsHelperParameters();
            parms.CurrentProfileId = Convert.ToInt32(userid);
            parms.sorting = MyListsSortEnum.Recent;
            parms.ReturnRecordsWithStatus = (int)ProfileStatusEnum.Approved;
            parms.zipstr = zip;
            parms.latitudeIn = Convert.ToDouble(lat);
            parms.longitudeIn = Convert.ToDouble(lng);
            parms.Distance = clsSearchHelper.DISTANCE_DEFAULT;

            parms.performCount = false;
            parms.rowNumberMin = Convert.ToInt32(indexstart);
            parms.rowNumberMax = Convert.ToInt32(indexstart) + 20;


            DataSet dt = clsSearchHelper.GetWhoSharedPhotoMembersDataTable(parms);
            list = createUserProfilesListfromDataset(dt);



            return list;
        }


        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "getSharedByMePhotos/{userid}/{zip}/{lat}/{lng}/{indexstart}")]
        public UserProfilesList getSharedByMePhotos(string userid, string zip, string lat, string lng, string indexstart) {
            UpdateActivity(Convert.ToInt32(userid));

            clsMyListsHelperParameters parms = new clsMyListsHelperParameters();
            parms.CurrentProfileId = Convert.ToInt32(userid);
            parms.sorting = MyListsSortEnum.Recent;
            parms.ReturnRecordsWithStatus = 4;
            parms.zipstr = zip;
            parms.latitudeIn = Convert.ToDouble(lat);
            parms.longitudeIn = Convert.ToDouble(lng);
            parms.Distance = clsSearchHelper.DISTANCE_DEFAULT;
            //parms.NumberOfRecordsToReturn = NumberOfRecordsToReturn
            parms.performCount = true;
            parms.rowNumberMin = Convert.ToInt32(indexstart);
            parms.rowNumberMax = Convert.ToInt32(indexstart) + 20;
            DataTable dt = clsSearchHelper.GetSharedPhotosByMeToMembers_DataTable(parms).Tables[1];


            UserProfilesList list = new UserProfilesList();
            list.Profiles = new List<UserProfile>();

            int start = Convert.ToInt32(indexstart);
            int results = 20;
            int max;
            if (start + results < dt.Rows.Count) {
                max = start + results;
            }
            else {
                max = dt.Rows.Count;
            }
            for (int i = start; i < max; i++) {
                DataRow dr = dt.Rows[i];
                string path = "";
                path = getImagePath_FromRow(dt, dr, Convert.ToInt32(dr["ProfileID"]), Convert.ToInt32(dr["GenderId"]), true);

                if (string.IsNullOrEmpty(path)) {
                    string fileName = "";
                    DSMembers.EUS_CustomerPhotosRow photo = DataHelpers.GetProfilesDefaultPhoto(Convert.ToInt32(dr["ProfileID"]));
                    if (photo != null)
                        fileName = photo.FileName;
                    //string path = ProfileHelper.GetProfileImageURL(Convert.ToInt32(r["ProfileID"]), fileName, Convert.ToInt32(r["GenderId"]), true, false);
                    path = getImagePath(Convert.ToInt32(dr["ProfileID"]), fileName, Convert.ToInt32(dr["GenderId"]), true, false);
                }

                int PhotosApproved = -1;
                if (dt.Columns.Contains("PhotosApproved")) {
                    PhotosApproved = 0;
                    if (!dr.IsNull("PhotosApproved"))
                        PhotosApproved = (int)dr["PhotosApproved"];
                }


                UserProfile p = new UserProfile()
                {
                    FirstName = (string)dr["FirstName"],
                    LastName = (string)dr["LastName"],
                    UserRegion = (string)dr["Region"],
                    UserCity = (string)dr["City"],
                    UserAge = Convert.ToString(ProfileHelper.GetCurrentAge((DateTime)dr["Birthday"])),
                    ProfileId = Convert.ToString(dr["ProfileID"]),
                    UserName = (string)dr["LoginName"],
                    DateRegister = ((DateTime)dr["DateTimeToRegister"]).ToShortDateString(),
                    ImageUrl = path,
                    GenderId = Convert.ToString(dr["GenderId"]),
                    Latitude = (dt.Columns.Contains("latitude") ? Convert.ToString(dr["latitude"]) : null),
                    Longtitude = (dt.Columns.Contains("longitude") ? Convert.ToString(dr["longitude"]) : null),
                    PhotosCount = PhotosApproved.ToString(),
                };

                p.IsVip = (p.GenderId == "1") ? GetIsVip(dt, dr) : "0";
                list.Profiles.Add(p);
            }

            return list;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "checkCountry/{userid1}/{userid2}")]
        public override string checkDifferentCountry(string userid1, string userid2) {
            return base.checkDifferentCountry(userid1, userid2);
            //try {

            //    using (myContext) {
            //        EUS_Profile p1 = (from p in myContext.EUS_Profiles
            //                          where p.ProfileID == Convert.ToInt32(userid1)
            //                          select p).FirstOrDefault();

            //        EUS_Profile p2 = (from p in myContext.EUS_Profiles
            //                          where p.ProfileID == Convert.ToInt32(userid2)
            //                          select p).FirstOrDefault();


            //        if (p1.Country == p2.Country) {
            //            return "true";
            //        }
            //        else {
            //            return "false";
            //        }
            //    }

            //}
            //catch (Exception) {
            //    return "error";
            //}
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "hasPhotos/{userid}")]
        public override string hasPhotos(string userid) {
            return base.hasPhotos(userid);

            //try {
            //    Dating.Server.Datasets.DLL.DSMembers.EUS_CustomerPhotosDataTable dtPhotos = DataHelpers.GetEUS_CustomerPhotos_ByProfileOrMirrorID(Convert.ToInt32(userid)).EUS_CustomerPhotos;
            //    if (dtPhotos.Rows.Count > 0) {
            //        return "true";
            //    }
            //    else {
            //        return "false";
            //    }

            //}
            //catch (Exception) {
            //    return "error";
            //}
        }


        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "testException")]
        public string testException() {

            try {
                throw new NotImplementedException();
            }
            catch (Exception ex) {

                //if (base.CurrentSettings.AllowSendExceptions) {
                if (clsMyMail.MySMTPSettings.gSMTPServerName == null) ServiceBase.ReadSMTPSettings();

                string msg = ex.Message;
                if ((msg.Length > 200))
                    msg = msg.Remove(197) + "...";
                string subject = "Exception on " + "mobile service" + " (" + msg + ")";

                string contetn = "";
                contetn =
                    "CurrentSettings.gSMTPLoginName: " + CurrentSettings.gSMTPLoginName + "\r\n" + "\r\n" +
                    "CurrentSettings.ExceptionsEmail: " + CurrentSettings.ExceptionsEmail + "\r\n" + "\r\n" +
                    "clsMyMail.MySMTPSettings.gSMTPServerName: " + clsMyMail.MySMTPSettings.gSMTPServerName + "\r\n" + "\r\n" +
                    "clsMyMail.MySMTPSettings.gSMTPLoginName: " + clsMyMail.MySMTPSettings.gSMTPLoginName + "\r\n" + "\r\n" +
                    "clsMyMail.MySMTPSettings.gSMTPOutPort: " + clsMyMail.MySMTPSettings.gSMTPOutPort + "\r\n" + "\r\n" +
                    "clsMyMail.MySMTPSettings.gSMTPPassword: " + clsMyMail.MySMTPSettings.gSMTPPassword + "\r\n" + "\r\n" +
                    ex.Message + "\r\n" + "\r\n" +
                    ex.ToString();

                Dating.Server.Core.DLL.clsMyMail.SendMail2(CurrentSettings.gSMTPLoginName, CurrentSettings.ExceptionsEmail, subject, contetn);
                //}

                return "-1";
            }

            return "0";
        }

        private string getImagePath(int profileId, string fileName, int genderId, bool isThumb, bool isHTTPS) {
            string path = ProfileHelper.GetProfileImageURL(profileId, fileName, genderId, isThumb, isHTTPS);
            if (isThumb) {
                if (path.StartsWith("/")) {
                    path = "http://www.goomena.com" + path;
                }
                else {
                    path = path.Replace("thumbs", "d150");
                }

            }
            return path;
        }

        private string getImagePathMyProfile(int profileId, string fileName, int genderId, bool isHTTPS) {
            string path = ProfileHelper.GetProfileImageURL(profileId, fileName, genderId, false, isHTTPS);

            return path;
        }



        private string getImagePath_FromRow(DataTable dt, DataRow dr, int profileId, int genderId, bool isThumb) {
            string path = "";
            string fileName = "";

            try {

                if (!dr.IsNull("FileName")) fileName = dr["FileName"].ToString();
                if ((dt.Columns.Contains("HasPhoto"))) {
                    if ((!dr.IsNull("HasPhoto") && (dr["HasPhoto"].ToString() == "1" || dr["HasPhoto"].ToString() == "true"))) {
                        // user has photo
                        if ((fileName != null)) {
                            //has public photos
                            path = ProfileHelper.GetProfileImageURL(profileId, fileName, genderId, true, false, PhotoSize.Thumb);
                        }
                        else {
                            //has private photos
                            path = ProfileHelper.GetPrivateImageURL(genderId);
                        }
                    }
                    else {
                        // has no photo
                        path = ProfileHelper.GetDefaultImageURL(genderId);
                    }
                }
                else {
                    path = ProfileHelper.GetProfileImageURL(profileId, fileName, genderId, true, false, PhotoSize.Thumb);
                }

                if (isThumb) {
                    if (path.StartsWith("/")) {
                        path = "http://www.goomena.com" + path;
                    }
                    else {
                        path = path.Replace("thumbs", "d150");
                    }
                }
            }
            catch (Exception ex) {
                WebErrorSendEmail(ex, "");
            }
            return path;
        }





        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "getProfileEditPersonal")]
        public UserProfileEditData getProfileEditPersonal(LoginParameters lp) {
            UserProfileEditData pInfo = new UserProfileEditData();
            try {
                var pr = (from p in myContext.EUS_Profiles
                          where
                             p.Password == lp.password &&
                             (p.eMail == lp.email || p.LoginName == lp.email) &&
                             p.Status != (int)ProfileStatusEnum.Deleted &&
                             p.Status != (int)ProfileStatusEnum.DeletedByUser &&
                             p.IsMaster == false
                          select p).FirstOrDefault();

                //get client IP address
                OperationContext context = OperationContext.Current;
                MessageProperties messageProperties = context.IncomingMessageProperties;
                RemoteEndpointMessageProperty endpointProperty = (RemoteEndpointMessageProperty)messageProperties[RemoteEndpointMessageProperty.Name];

                if (pr != null) {
                    if (!ServiceBase.allowUserAction(pr.ProfileID, lp.logindetails)) {
                        throw new ActionNotAllowedException();
                    }


                    //update last activity
                    UpdateActivity(pr.ProfileID);

                    //get profile details
                    pInfo.FirstName = pr.FirstName;
                    pInfo.LastName = pr.LastName;
                    pInfo.EmailAddress = pr.eMail;
                    pInfo.Address = pr.Address;
                    pInfo.PhoneNumber = pr.Telephone;
                    pInfo.MobilePhone = pr.Cellular;


                    DataHelpers.LogProfileAccess(pr.ProfileID, pr.LoginName, lp.password, true, "Login-[Personal Data]", lp.logindetails, "", endpointProperty.Address, true, "Service4-getProfileEditPersonal");
                }
                else {
                    DataHelpers.LogProfileAccess(0, lp.email, lp.password, false, "Login-[Personal Data]", lp.logindetails, "", endpointProperty.Address, true, "Service4-getProfileEditPersonal");

                }

            }
            catch (Exception ex) {
                if (CurrentSettings.AllowSendExceptions)
                    pInfo.ErrorMessage = ex.ToString();
                else
                    pInfo.ErrorMessage = "exception";
                WebErrorSendEmail(ex, "");
            }
            finally {
                myContext.Dispose();
            }
            return pInfo;
        }

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "getProfileEditLookingFor")]
        public UserProfileEditLookingForData getProfileEditLookingFor(LoginParameters lp) {
            UserProfileEditLookingForData pInfo = new UserProfileEditLookingForData();
            try {

                var pr = (from p in myContext.EUS_Profiles
                          where
                             p.Password == lp.password &&
                             (p.eMail == lp.email || p.LoginName == lp.email) &&
                             p.Status != (int)ProfileStatusEnum.Deleted &&
                             p.Status != (int)ProfileStatusEnum.DeletedByUser &&
                             p.IsMaster == false
                          select p).FirstOrDefault();

                //get client IP address
                OperationContext context = OperationContext.Current;
                MessageProperties messageProperties = context.IncomingMessageProperties;
                RemoteEndpointMessageProperty endpointProperty = (RemoteEndpointMessageProperty)messageProperties[RemoteEndpointMessageProperty.Name];

                if (pr != null) {
                    if (!ServiceBase.allowUserAction(pr.ProfileID, lp.logindetails)) {
                        throw new ActionNotAllowedException();
                    }

                    //update last activity
                    UpdateActivity(pr.ProfileID);


                    //get profile details
                    pInfo.RelationshipStatus = clsNullable.NullTo(pr.LookingFor_RelationshipStatusID, -1).ToString();
                    pInfo.TODShort = clsNullable.NullTo(pr.LookingFor_TypeOfDating_ShortTermRelationship).ToString();
                    pInfo.TODFriendship = clsNullable.NullTo(pr.LookingFor_TypeOfDating_Friendship).ToString();
                    pInfo.TODLongTerm = clsNullable.NullTo(pr.LookingFor_TypeOfDating_LongTermRelationship).ToString();
                    pInfo.TODMutually = clsNullable.NullTo(pr.LookingFor_TypeOfDating_MutuallyBeneficialArrangements).ToString();
                    pInfo.TODMarried = clsNullable.NullTo(pr.LookingFor_TypeOfDating_MarriedDating).ToString();
                    pInfo.TODCasual = clsNullable.NullTo(pr.LookingFor_TypeOfDating_AdultDating_Casual).ToString();


                    DataHelpers.LogProfileAccess(pr.ProfileID, pr.LoginName, lp.password, true, "Login-[Personal Data]", lp.logindetails, "", endpointProperty.Address, true, "Service4-getProfileEditLookingFor");
                }
                else {
                    DataHelpers.LogProfileAccess(0, lp.email, lp.password, false, "Login-[Personal Data]", lp.logindetails, "", endpointProperty.Address, true, "Service4-getProfileEditLookingFor");

                }


            }
            catch (Exception ex) {
                if (CurrentSettings.AllowSendExceptions)
                    pInfo.ErrorMessage = ex.ToString();
                else
                    pInfo.ErrorMessage = "exception";
                WebErrorSendEmail(ex, "");
            }
            finally {
                myContext.Dispose();
            }
            return pInfo;
        }




        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "getProfileEditPersonalInfoLists")]
        public UserProfileEditData getProfileEditPersonalInfoLists(LoginParameters lp) {
            UserProfileEditData pInfo = new UserProfileEditData();
            try {

                var pr = (from p in myContext.EUS_Profiles
                          where
                             p.Password == lp.password &&
                             (p.eMail == lp.email || p.LoginName == lp.email) &&
                             p.Status != (int)ProfileStatusEnum.Deleted &&
                             p.Status != (int)ProfileStatusEnum.DeletedByUser &&
                             p.IsMaster == false
                          select p).FirstOrDefault();

                string ip = getClientIP();
                if (pr != null) {
                    if (!ServiceBase.allowUserAction(pr.ProfileID, lp.logindetails)) {
                        throw new ActionNotAllowedException();
                    }

                    UpdateActivity(pr.ProfileID);

                    pInfo.Height = clsNullable.NullTo(pr.PersonalInfo_HeightID, -1).ToString();
                    pInfo.BodyType = clsNullable.NullTo(pr.PersonalInfo_BodyTypeID, -1).ToString();
                    pInfo.EyeColor = clsNullable.NullTo(pr.PersonalInfo_EyeColorID, -1).ToString();
                    pInfo.HairColor = clsNullable.NullTo(pr.PersonalInfo_HairColorID, -1).ToString();
                    pInfo.Children = clsNullable.NullTo(pr.PersonalInfo_ChildrenID, -1).ToString();
                    pInfo.Ethnicity = clsNullable.NullTo(pr.PersonalInfo_EthnicityID, -1).ToString();
                    pInfo.Religion = clsNullable.NullTo(pr.PersonalInfo_ReligionID, -1).ToString();
                    pInfo.Smoking = clsNullable.NullTo(pr.PersonalInfo_SmokingHabitID, -1).ToString();
                    pInfo.Drinking = clsNullable.NullTo(pr.PersonalInfo_DrinkingHabitID, -1).ToString();


                    EUS_ProfileBreastSize pbs = (from itm in myContext.EUS_ProfileBreastSizes
                                                 where itm.ProfileID == pr.ProfileID || itm.ProfileID == pr.MirrorProfileID
                                                 select itm).FirstOrDefault();
                    if (pbs != null) {
                        pInfo.BreastSize = pbs.BreastSizeID.ToString();
                    }
                    else {
                        pInfo.BreastSize = "-1";
                    }

                    DataHelpers.LogProfileAccess(pr.ProfileID, pr.LoginName, lp.password, true, "Login-[Personal Data]", lp.logindetails, "", ip, true, "Service4-getProfileEditPersonalInfoLists");
                }
                else {
                    DataHelpers.LogProfileAccess(0, lp.email, lp.password, false, "Login-[Personal Data]", lp.logindetails, "", ip, true, "Service4-getProfileEditPersonalInfoLists");
                }


            }
            catch (Exception ex) {
                if (CurrentSettings.AllowSendExceptions)
                    pInfo.ErrorMessage = ex.ToString();
                else
                    pInfo.ErrorMessage = "exception";
                WebErrorSendEmail(ex, "");
            }
            finally {
                myContext.Dispose();
            }

            return pInfo;
        }



        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "getProfileEditLocation")]
        public UserProfileEditData getProfileEditLocation(LoginParameters lp) {
            UserProfileEditData pInfo = new UserProfileEditData();
            try {

                var pr = (from p in myContext.EUS_Profiles
                          where
                             p.Password == lp.password &&
                             (p.eMail == lp.email || p.LoginName == lp.email) &&
                             p.Status != (int)ProfileStatusEnum.Deleted &&
                             p.Status != (int)ProfileStatusEnum.DeletedByUser &&
                             p.IsMaster == false
                          select p).FirstOrDefault();

                string ip = getClientIP();
                if (pr != null) {
                    if (!ServiceBase.allowUserAction(pr.ProfileID, lp.logindetails)) {
                        throw new ActionNotAllowedException();
                    }

                    UpdateActivity(pr.ProfileID);

                    pInfo.Country = pr.Country;
                    pInfo.Region = pr.Region;
                    pInfo.City = pr.City;
                    pInfo.ZipCode = pr.Zip;

                    {
                        List<string> SelectedLangIdList = new List<string>();
                        DataTable dt = DataHelpers.GetEUS_LISTS_SpokenLanguages_Datatable(pr.ProfileID);
                        DataRow[] drs = dt.Select("IsSelected=1");
                        for (int i = 0; i < drs.Length; i++) {
                            SelectedLangIdList.Add(drs[i]["SpokenLangId"].ToString());
                        }
                        pInfo.SelectedLangIdList = SelectedLangIdList;
                    }


                    {
                        List<EUS_ProfileLocation> loc = (from itm in myContext.EUS_ProfileLocations where itm.ProfileID == pr.ProfileID select itm).ToList();
                        ItemsList list = new ItemsList();

                        for (int c1 = 0; c1 < loc.Count; c1++) {
                            EUS_ProfileLocation itm = loc[c1];
                            if (itm.LocalityType == "country") {
                                itm.Descr = itm.Loc_country;
                            }
                            else if (!string.IsNullOrEmpty(itm.Loc_city)) {
                                itm.Descr = itm.Loc_country + ", " + itm.Loc_city;
                            }
                            else if (!string.IsNullOrEmpty(itm.Loc_region)) {
                                itm.Descr = itm.Loc_country + ", " + itm.Loc_region;
                            }
                            itm.Descr = itm.Descr.Trim(new char[] { ' ', ',' });

                            list.items.Add(new ListItem()
                            {
                                id = itm.LocationID,
                                title = itm.Descr
                            });
                        }

                        pInfo.ProfileLocationList = list;
                    }


                    {
                        ItemsList list = new ItemsList();

                        string sql = @"
select lt.* 
from dbo.EUS_ProfileJobs pj
inner join dbo.EUS_LISTS_Job lt on lt.JobID=pj.JobID
where ProfileID=@Profileid
";
                        sql = sql.Replace("@Profileid", pr.ProfileID.ToString());
                        DataTable dt = DataHelpers.GetDataTable(sql);
                        dt.Columns.Add("Descr", typeof(string));

                        for (int c1 = 0; c1 < dt.Rows.Count; c1++) {
                            DataRow dr = dt.Rows[c1];
                            if ((dt.Columns.Contains(pr.LAGID) && dr[pr.LAGID].ToString().Trim().Length > 0)) {
                                dr["Descr"] = dr[pr.LAGID];
                            }
                            else {
                                dr["Descr"] = dr["US"];
                            }
                            list.items.Add(new ListItem()
                            {
                                id = dr["JobID"].ToString(),
                                title = dr["Descr"].ToString()
                            });
                        }

                        pInfo.JobsList = list;
                    }



                    DataHelpers.LogProfileAccess(pr.ProfileID, pr.LoginName, lp.password, true, "Login-[Personal Data]", lp.logindetails, "", ip, true, "Service4-getProfileEditLocation");
                }
                else {
                    DataHelpers.LogProfileAccess(0, lp.email, lp.password, false, "Login-[Personal Data]", lp.logindetails, "", ip, true, "Service4-getProfileEditLocation");
                }


            }
            catch (Exception ex) {
                if (CurrentSettings.AllowSendExceptions)
                    pInfo.ErrorMessage = ex.ToString();
                else
                    pInfo.ErrorMessage = "exception";
                WebErrorSendEmail(ex, "");
            }
            finally {
                myContext.Dispose();
            }

            return pInfo;
        }



        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "getProfileEditOtherDetails")]
        public UserProfileEditData getProfileEditOtherDetails(LoginParameters lp) {
            UserProfileEditData pInfo = new UserProfileEditData();
            try {

                var pr = (from p in myContext.EUS_Profiles
                          where
                             p.Password == lp.password &&
                             (p.eMail == lp.email || p.LoginName == lp.email) &&
                             p.Status != (int)ProfileStatusEnum.Deleted &&
                             p.Status != (int)ProfileStatusEnum.DeletedByUser &&
                             p.IsMaster == false
                          select p).FirstOrDefault();

                string ip = getClientIP();
                if (pr != null) {
                    if (!ServiceBase.allowUserAction(pr.ProfileID, lp.logindetails)) {
                        throw new ActionNotAllowedException();
                    }

                    UpdateActivity(pr.ProfileID);

                    pInfo.Occupation = pr.OtherDetails_Occupation;
                    pInfo.Education = clsNullable.NullTo(pr.OtherDetails_EducationID, -1).ToString();
                    pInfo.AnnualIncome = clsNullable.NullTo(pr.OtherDetails_AnnualIncomeID, -1).ToString();

                    DataHelpers.LogProfileAccess(pr.ProfileID, pr.LoginName, lp.password, true, "Login-[Personal Data]", lp.logindetails, "", ip, true, "Service4-getProfileEditOtherDetails");
                }
                else {
                    DataHelpers.LogProfileAccess(0, lp.email, lp.password, false, "Login-[Personal Data]", lp.logindetails, "", ip, true, "Service4-getProfileEditOtherDetails");
                }


            }
            catch (Exception ex) {
                if (CurrentSettings.AllowSendExceptions)
                    pInfo.ErrorMessage = ex.ToString();
                else
                    pInfo.ErrorMessage = "exception";
                WebErrorSendEmail(ex, "");
            }
            finally {
                myContext.Dispose();
            }

            return pInfo;
        }


        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "setProfileEditLocation")]
        public string setProfileEditLocation(UserProfileEditData lp) {
            string result = "";
            StringBuilder resultSb = new StringBuilder();
            try {

                var pr = (from p in myContext.EUS_Profiles
                          where
                             p.Password == lp.password &&
                             (p.eMail == lp.email || p.LoginName == lp.email) &&
                             p.Status != (int)ProfileStatusEnum.Deleted &&
                             p.Status != (int)ProfileStatusEnum.DeletedByUser &&
                             p.IsMaster == false
                          select p).FirstOrDefault();

                string ip = getClientIP();
                if (pr != null) {
                    if (!ServiceBase.allowUserAction(pr.ProfileID, lp.logindetails)) {
                        throw new ActionNotAllowedException();
                    }


                    bool dataOk = false;
                    lp.ZipCode = AppUtils.GetZip(lp.ZipCode);
                    if (!string.IsNullOrEmpty(lp.ZipCode.Trim())) {

                        DataTable dt = clsGeoHelper.GetGEOByZip(lp.Country, lp.ZipCode, pr.LAGID);
                        if (dt.Rows.Count > 0) {
                            lp.Region = dt.Rows[0]["region1"].ToString();
                            lp.City = dt.Rows[0]["city"].ToString();
                            dataOk = true;
                        }
                        else {
                            result = "invalid zip";
                        }
                    }


                    if (dataOk) {
                        pr.Country = lp.Country;
                        pr.Region = lp.Region;
                        pr.City = lp.City;
                        pr.Zip = lp.ZipCode;


                        try {
                            clsGlobalPosition pos = clsGeoHelper.GetPosition(pr.Country, pr.Region, pr.City, pr.Zip, pr.LAGID);
                            if (pos.latitude != 0 && pos.longitude != 0) {
                                pr.latitude = pos.latitude;
                                pr.longitude = pos.longitude;
                            }
                            else {
                                resultSb.AppendLine("position not found");
                            }
                        }
                        catch (Exception ex) {
                            resultSb.AppendLine(ex.ToString());
                            WebErrorSendEmail(ex, "Android Updating Profile: Failed to retrieve GEO info for specified zip, params (zip:" + pr.Zip + ")");
                        }


                        if (pr.MirrorProfileID > 1) {
                            // update master record
                            var masterR = (from p in myContext.EUS_Profiles
                                           where p.ProfileID == pr.MirrorProfileID
                                           select p).FirstOrDefault();

                            if (masterR != null) {
                                masterR.Country = pr.Country;
                                masterR.Region = pr.Region;
                                masterR.City = pr.City;
                                masterR.Zip = pr.Zip;
                                masterR.latitude = pr.latitude;
                                masterR.longitude = pr.longitude;
                            }
                        }

                        myContext.SubmitChanges();
                        //update last activity

                        result = "ok";
                    }

                    UpdateActivity(pr.ProfileID);
                    DataHelpers.LogProfileAccess(pr.ProfileID, pr.LoginName, lp.password, true, "Login-[Personal Data Save]", lp.logindetails, "", ip, true, "Service4-setProfileEditLocation");
                }
                else {
                    result = "failed";
                    DataHelpers.LogProfileAccess(0, lp.email, lp.password, false, "Login-[Personal Data Save]", lp.logindetails, "", ip, true, "Service4-setProfileEditLocation");

                }


            }
            catch (Exception ex) {
                resultSb.AppendLine(ex.ToString());

                if (CurrentSettings.AllowSendExceptions)
                    result = ex.ToString();
                else
                    result = "exception";

                WebErrorSendEmail(ex, "");
            }
            finally {
                myContext.Dispose();
            }
            //if (false && resultSb.Length > 0) {
            //    resultSb.Insert(0, result + "\r\n");
            //    result = resultSb.ToString();
            //}
            return result;
        }


        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "setProfileEditPersonal")]
        public string setProfileEditPersonal(UserProfileEditData lp) {
            string result = "";
            try {


                var pr = (from p in myContext.EUS_Profiles
                          where
                             p.Password == lp.password &&
                             (p.eMail == lp.email || p.LoginName == lp.email) &&
                             p.Status != (int)ProfileStatusEnum.Deleted &&
                             p.Status != (int)ProfileStatusEnum.DeletedByUser &&
                             p.IsMaster == false
                          select p).FirstOrDefault();


                string ip = getClientIP();

                if (pr != null) {
                    if (!ServiceBase.allowUserAction(pr.ProfileID, lp.logindetails)) {
                        throw new ActionNotAllowedException();
                    }


                    bool _DataChanged = false;


                    //get profile details
                    {
                        if (lp.FirstName == null) lp.FirstName = "";
                        if (lp.FirstName.Length > 50) lp.FirstName = lp.FirstName.Remove(50);
                        if (pr.FirstName == null) pr.FirstName = "";
                        if (pr.FirstName != lp.FirstName) _DataChanged = true;
                        pr.FirstName = lp.FirstName;
                    }

                    {
                        if (lp.LastName == null) lp.LastName = "";
                        if (lp.LastName.Length > 50) lp.LastName = lp.LastName.Remove(50);
                        if (pr.LastName == null) pr.LastName = "";
                        if (pr.LastName != lp.LastName) _DataChanged = true;
                        pr.LastName = lp.LastName;
                    }
                    {
                        if (lp.EmailAddress == null) lp.EmailAddress = "";
                        if (lp.EmailAddress.Length > 100) lp.EmailAddress = lp.EmailAddress.Remove(100);
                        if (pr.eMail == null) pr.eMail = "";
                        if (pr.eMail != lp.EmailAddress) _DataChanged = true;
                        pr.eMail = lp.EmailAddress;
                    }
                    {
                        if (lp.Address == null) lp.Address = "";
                        if (lp.Address.Length > 200) lp.Address = lp.Address.Remove(200);
                        if (pr.Address == null) pr.Address = "";
                        if (pr.Address != lp.Address) _DataChanged = true;
                        pr.Address = lp.Address;
                    }
                    {
                        if (lp.PhoneNumber == null) lp.PhoneNumber = "";
                        if (lp.PhoneNumber.Length > 30) lp.PhoneNumber = lp.PhoneNumber.Remove(30);
                        if (pr.Telephone == null) pr.Telephone = "";
                        if (pr.Telephone != lp.PhoneNumber) _DataChanged = true;
                        pr.Telephone = lp.PhoneNumber;
                    }
                    {
                        if (lp.MobilePhone == null) lp.MobilePhone = "";
                        if (lp.MobilePhone.Length > 30) lp.MobilePhone = lp.MobilePhone.Remove(30);
                        if (pr.Cellular == null) pr.Cellular = "";
                        if (pr.Cellular != lp.MobilePhone) _DataChanged = true;
                        pr.Cellular = lp.MobilePhone;
                    }

                    if (_DataChanged) {
                        pr.Status = (int)ProfileStatusEnum.Updating;
                        pr.LastUpdateProfileDateTime = DateTime.UtcNow;
                        pr.LastUpdateProfileIP = getClientIP();
                        try {
                            clsCountryByIP country = clsCountryByIP.GetCountryCodeFromIP(ip, CurrentSettings.ConnectionString_GEODB);
                            pr.LastUpdateProfileGEOInfo = country.CountryCode;
                        }
                        catch (Exception) { }
                    }

                    myContext.SubmitChanges();

                    result = "ok";

                    //update last activity
                    UpdateActivity(pr.ProfileID);
                    DataHelpers.LogProfileAccess(pr.ProfileID, pr.LoginName, lp.password, true, "Login-[Personal Data Save]", lp.logindetails, "", ip, true, "Service4-setProfileEditPersonal");
                }
                else {
                    result = "failed";
                    DataHelpers.LogProfileAccess(0, lp.email, lp.password, false, "Login-[Personal Data Save]", lp.logindetails, "", ip, true, "Service4-setProfileEditPersonal");

                }


            }
            catch (Exception ex) {
                if (CurrentSettings.AllowSendExceptions)
                    result = ex.ToString();
                else
                    result = "exception";

                WebErrorSendEmail(ex, "");
            }
            finally {
                myContext.Dispose();
            }
            return result;
        }


        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "setProfileEditAboutMe")]
        public string setProfileEditAboutMe(UserProfileEditData lp) {
            string result = "";
            try {

                var pr = (from p in myContext.EUS_Profiles
                          where
                             p.Password == lp.password &&
                             (p.eMail == lp.email || p.LoginName == lp.email) &&
                             p.Status != (int)ProfileStatusEnum.Deleted &&
                             p.Status != (int)ProfileStatusEnum.DeletedByUser &&
                             p.IsMaster == false
                          select p).FirstOrDefault();

                string ip = getClientIP();
                if (pr != null) {
                    if (!ServiceBase.allowUserAction(pr.ProfileID, lp.logindetails)) {
                        throw new ActionNotAllowedException();
                    }

                    bool _DataChanged = false;
                    {
                        if (lp.AboutMe_Heading == null) lp.AboutMe_Heading = "";
                        if (lp.AboutMe_Heading.Length > 200) lp.AboutMe_Heading = lp.AboutMe_Heading.Remove(200);
                        if (pr.AboutMe_Heading == null) pr.AboutMe_Heading = "";
                        if (pr.AboutMe_Heading != lp.AboutMe_Heading) _DataChanged = true;
                        pr.AboutMe_Heading = lp.AboutMe_Heading;
                    }
                    {
                        if (lp.AboutMe_DescribeYourself == null) lp.AboutMe_DescribeYourself = "";
                        if (pr.AboutMe_DescribeYourself == null) pr.AboutMe_DescribeYourself = "";
                        if (pr.AboutMe_DescribeYourself != lp.AboutMe_DescribeYourself) _DataChanged = true;
                        pr.AboutMe_DescribeYourself = lp.AboutMe_DescribeYourself;
                    }
                    {
                        if (lp.AboutMe_DescribeFirstDate == null) lp.AboutMe_DescribeFirstDate = "";
                        if (pr.AboutMe_DescribeAnIdealFirstDate == null) pr.AboutMe_DescribeAnIdealFirstDate = "";
                        if (pr.AboutMe_DescribeAnIdealFirstDate != lp.AboutMe_DescribeFirstDate) _DataChanged = true;
                        pr.AboutMe_DescribeAnIdealFirstDate = lp.AboutMe_DescribeFirstDate;
                    }


                    if (_DataChanged) {
                        pr.Status = (int)ProfileStatusEnum.Updating;
                        pr.LastUpdateProfileDateTime = DateTime.UtcNow;
                        pr.LastUpdateProfileIP = getClientIP();
                        try {
                            clsCountryByIP country = clsCountryByIP.GetCountryCodeFromIP(ip, CurrentSettings.ConnectionString_GEODB);
                            pr.LastUpdateProfileGEOInfo = country.CountryCode;
                        }
                        catch (Exception) { }
                    }

                    myContext.SubmitChanges();

                    result = "ok";

                    //update last activity
                    UpdateActivity(pr.ProfileID);
                    DataHelpers.LogProfileAccess(pr.ProfileID, pr.LoginName, lp.password, true, "Login-[Personal Data Save]", lp.logindetails, "", ip, true, "Service4-setProfileEditAboutMe");
                }
                else {
                    result = "failed";
                    DataHelpers.LogProfileAccess(0, lp.email, lp.password, false, "Login-[Personal Data Save]", lp.logindetails, "", ip, true, "Service4-setProfileEditAboutMe");

                }



            }
            catch (Exception ex) {
                if (CurrentSettings.AllowSendExceptions)
                    result = ex.ToString();
                else
                    result = "exception";
                WebErrorSendEmail(ex, "");
            }
            finally {
                myContext.Dispose();
            }
            return result;
        }


        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "setProfileEditOtherDetails")]
        public string setProfileEditOtherDetails(UserProfileEditData lp) {
            string result = "";
            try {

                var pr = (from p in myContext.EUS_Profiles
                          where
                             p.Password == lp.password &&
                             (p.eMail == lp.email || p.LoginName == lp.email) &&
                             p.Status != (int)ProfileStatusEnum.Deleted &&
                             p.Status != (int)ProfileStatusEnum.DeletedByUser &&
                             p.IsMaster == false
                          select p).FirstOrDefault();

                string ip = getClientIP();
                if (pr != null) {
                    if (!ServiceBase.allowUserAction(pr.ProfileID, lp.logindetails)) {
                        throw new ActionNotAllowedException();
                    }

                    bool _DataChanged = false;
                    {
                        if (lp.Occupation == null) lp.Occupation = "";
                        if (lp.Occupation.Length > 200) lp.Occupation = lp.Occupation.Remove(200);
                        if (pr.OtherDetails_Occupation == null) pr.OtherDetails_Occupation = "";
                        if (pr.OtherDetails_Occupation != lp.Occupation) _DataChanged = true;
                        pr.OtherDetails_Occupation = lp.Occupation;
                    }
                    {
                        int Id = 0;
                        if (lp.Education != null && int.TryParse(lp.Education, out Id))
                            pr.OtherDetails_EducationID = Id;
                    }
                    {
                        int Id = 0;
                        if (!string.IsNullOrEmpty(lp.AnnualIncome) && int.TryParse(lp.AnnualIncome, out Id))
                            pr.OtherDetails_AnnualIncomeID = Id;
                    }


                    if (_DataChanged) {
                        pr.Status = (int)ProfileStatusEnum.Updating;
                        pr.LastUpdateProfileDateTime = DateTime.UtcNow;
                        pr.LastUpdateProfileIP = ip;
                        try {
                            clsCountryByIP country = clsCountryByIP.GetCountryCodeFromIP(ip, CurrentSettings.ConnectionString_GEODB);
                            pr.LastUpdateProfileGEOInfo = country.CountryCode;
                        }
                        catch (Exception) { }
                    }


                    if (pr.MirrorProfileID > 1) {
                        // update master record
                        var masterR = (from p in myContext.EUS_Profiles
                                       where p.ProfileID == pr.MirrorProfileID
                                       select p).FirstOrDefault();

                        if (masterR != null) {
                            masterR.OtherDetails_AnnualIncomeID = pr.OtherDetails_AnnualIncomeID;
                            masterR.OtherDetails_EducationID = pr.OtherDetails_EducationID;
                        }
                    }

                    myContext.SubmitChanges();

                    //update last activity
                    UpdateActivity(pr.ProfileID);
                    //DataHelpers.UpdateEUS_Profiles_Activity(mr.ProfileID, ip, true);


                    result = "ok";
                    DataHelpers.LogProfileAccess(pr.ProfileID, pr.LoginName, lp.password, true, "Login-[Personal Data Save]", lp.logindetails, "", ip, true, "Service4-setProfileEditOtherDetails");
                }
                else {
                    result = "failed";
                    DataHelpers.LogProfileAccess(0, lp.email, lp.password, false, "Login-[Personal Data Save]", lp.logindetails, "", ip, true, "Service4-setProfileEditOtherDetails");

                }

            }
            catch (Exception ex) {
                if (CurrentSettings.AllowSendExceptions)
                    result = ex.ToString();
                else
                    result = "exception";

                WebErrorSendEmail(ex, "");
            }
            finally {
                myContext.Dispose();
            }
            return result;
        }


        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "setProfileEditPersonalInfoLists")]
        public string setProfileEditPersonalInfoLists(UserProfileEditData lp) {
            string result = "";
            try {

                var pr = (from p in myContext.EUS_Profiles
                          where
                             p.Password == lp.password &&
                             (p.eMail == lp.email || p.LoginName == lp.email) &&
                             p.Status != (int)ProfileStatusEnum.Deleted &&
                             p.Status != (int)ProfileStatusEnum.DeletedByUser &&
                             p.IsMaster == false
                          select p).FirstOrDefault();

                string ip = getClientIP();
                if (pr != null) {
                    if (!ServiceBase.allowUserAction(pr.ProfileID, lp.logindetails)) {
                        throw new ActionNotAllowedException();
                    }


                    {
                        int Id = -1;
                        if (lp.Height != null && int.TryParse(lp.Height, out Id))
                            pr.PersonalInfo_HeightID = Id;
                    }
                    {
                        int Id = -1;
                        if (lp.BodyType != null && int.TryParse(lp.BodyType, out Id))
                            pr.PersonalInfo_BodyTypeID = Id;
                    }
                    {
                        int Id = -1;
                        if (lp.EyeColor != null && int.TryParse(lp.EyeColor, out Id))
                            pr.PersonalInfo_EyeColorID = Id;
                    }
                    {
                        int Id = -1;
                        if (lp.HairColor != null && int.TryParse(lp.HairColor, out Id))
                            pr.PersonalInfo_HairColorID = Id;
                    }
                    {
                        int Id = -1;
                        if (lp.Children != null && int.TryParse(lp.Children, out Id))
                            pr.PersonalInfo_ChildrenID = Id;
                    }
                    {
                        int Id = 0;
                        if (lp.Ethnicity != null && int.TryParse(lp.Ethnicity, out Id))
                            pr.PersonalInfo_EthnicityID = Id;
                    }
                    {
                        int Id = -1;
                        if (lp.Religion != null && int.TryParse(lp.Religion, out Id))
                            pr.PersonalInfo_ReligionID = Id;
                    }
                    {
                        int Id = -1;
                        if (lp.Smoking != null && int.TryParse(lp.Smoking, out Id))
                            pr.PersonalInfo_SmokingHabitID = Id;
                    }
                    {
                        int Id = -1;
                        if (lp.Drinking != null && int.TryParse(lp.Drinking, out Id))
                            pr.PersonalInfo_DrinkingHabitID = Id;
                    }


                    {
                        int newBreastSize = 0;
                        if (ProfileHelper.IsFemale(pr.GenderId.Value) &&
                            !string.IsNullOrEmpty(lp.BreastSize) &&
                            int.TryParse(lp.BreastSize, out newBreastSize)) {

                            EUS_ProfileBreastSize pbs = (from itm in myContext.EUS_ProfileBreastSizes
                                                         where itm.ProfileID == pr.ProfileID || itm.ProfileID == pr.MirrorProfileID
                                                         select itm).FirstOrDefault();

                            if (newBreastSize > 1) {

                                if ((newBreastSize > 1) && (pbs != null) && (pbs.BreastSizeID != newBreastSize)) {
                                    myContext.EUS_ProfileBreastSizes.DeleteOnSubmit(pbs);
                                    pbs = null;
                                }

                                // save new value
                                if (pbs == null) {
                                    pbs = new EUS_ProfileBreastSize();
                                    pbs.BreastSizeID = newBreastSize;
                                    pbs.ProfileID = pr.MirrorProfileID.Value ; // set master profileid
                                    myContext.EUS_ProfileBreastSizes.InsertOnSubmit(pbs);
                                }
                                else if (newBreastSize <= 1) {
                                    if (pbs != null)
                                        myContext.EUS_ProfileBreastSizes.DeleteOnSubmit(pbs);
                                }

                            }

                        }
                    }


                    if (pr.MirrorProfileID > 1) {
                        // update master record
                        var masterR = (from p in myContext.EUS_Profiles
                                       where p.ProfileID == pr.MirrorProfileID
                                       select p).FirstOrDefault();

                        if (masterR != null) {
                            masterR.PersonalInfo_HeightID = pr.PersonalInfo_HeightID;
                            masterR.PersonalInfo_BodyTypeID = pr.PersonalInfo_BodyTypeID;
                            masterR.PersonalInfo_EyeColorID = pr.PersonalInfo_EyeColorID;
                            masterR.PersonalInfo_HairColorID = pr.PersonalInfo_HairColorID;
                            masterR.PersonalInfo_ChildrenID = pr.PersonalInfo_ChildrenID;
                            masterR.PersonalInfo_EthnicityID = pr.PersonalInfo_EthnicityID;
                            masterR.PersonalInfo_ReligionID = pr.PersonalInfo_ReligionID;
                            masterR.PersonalInfo_SmokingHabitID = pr.PersonalInfo_SmokingHabitID;
                            masterR.PersonalInfo_DrinkingHabitID = pr.PersonalInfo_DrinkingHabitID;
                        }
                    }

                    myContext.SubmitChanges();

                    result = "ok";

                    //update last activity
                    UpdateActivity(pr.ProfileID);
                    DataHelpers.LogProfileAccess(pr.ProfileID, pr.LoginName, lp.password, true, "Login-[Personal Data Save]", lp.logindetails, "", ip, true, "Service4-setProfileEditPersonalInfoLists");
                }
                else {
                    result = "failed";
                    DataHelpers.LogProfileAccess(0, lp.email, lp.password, false, "Login-[Personal Data Save]", lp.logindetails, "", ip, true, "Service4-setProfileEditPersonalInfoLists");

                }


            }
            catch (Exception ex) {
                if (CurrentSettings.AllowSendExceptions)
                    result = ex.ToString();
                else
                    result = "exception";
                WebErrorSendEmail(ex, "");
            }
            finally {
                myContext.Dispose();
            }
            return result;

        }


        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "setProfileEditLookingFor")]
        public string setProfileEditLookingFor(UserProfileEditLookingForData lp) {
            string result = "";
            try {

                var pr = (from p in myContext.EUS_Profiles
                          where
                             p.Password == lp.password &&
                             (p.eMail == lp.email || p.LoginName == lp.email) &&
                             p.Status != (int)ProfileStatusEnum.Deleted &&
                             p.Status != (int)ProfileStatusEnum.DeletedByUser &&
                             p.IsMaster == false
                          select p).FirstOrDefault();

                string ip = getClientIP();
                if (pr != null) {
                    if (!ServiceBase.allowUserAction(pr.ProfileID, lp.logindetails)) {
                        throw new ActionNotAllowedException();
                    }


                    {
                        int Id = 0;
                        if (lp.RelationshipStatus != null && int.TryParse(lp.RelationshipStatus, out Id))
                            pr.LookingFor_RelationshipStatusID = Id;
                    }

                    if (!string.IsNullOrEmpty(lp.TODShort))
                        pr.LookingFor_TypeOfDating_ShortTermRelationship = (lp.TODShort.ToLower() == "true");
                    else
                        pr.LookingFor_TypeOfDating_ShortTermRelationship = false;

                    if (!string.IsNullOrEmpty(lp.TODFriendship))
                        pr.LookingFor_TypeOfDating_Friendship = (lp.TODFriendship.ToLower() == "true");
                    else
                        pr.LookingFor_TypeOfDating_Friendship = false;

                    if (!string.IsNullOrEmpty(lp.TODLongTerm))
                        pr.LookingFor_TypeOfDating_LongTermRelationship = (lp.TODLongTerm.ToLower() == "true");
                    else
                        pr.LookingFor_TypeOfDating_LongTermRelationship = false;

                    if (!string.IsNullOrEmpty(lp.TODMutually))
                        pr.LookingFor_TypeOfDating_MutuallyBeneficialArrangements = (lp.TODMutually.ToLower() == "true");
                    else
                        pr.LookingFor_TypeOfDating_MutuallyBeneficialArrangements = false;

                    if (!string.IsNullOrEmpty(lp.TODMarried))
                        pr.LookingFor_TypeOfDating_MarriedDating = (lp.TODMarried.ToLower() == "true");
                    else
                        pr.LookingFor_TypeOfDating_MarriedDating = false;

                    if (!string.IsNullOrEmpty(lp.TODCasual))
                        pr.LookingFor_TypeOfDating_AdultDating_Casual = (lp.TODCasual.ToLower() == "true");
                    else
                        pr.LookingFor_TypeOfDating_AdultDating_Casual = false;


                    if (pr.MirrorProfileID > 1) {
                        // update master record
                        var masterR = (from p in myContext.EUS_Profiles
                                       where p.ProfileID == pr.MirrorProfileID
                                       select p).FirstOrDefault();

                        if (masterR != null) {
                            masterR.LookingFor_RelationshipStatusID = pr.LookingFor_RelationshipStatusID;
                            masterR.LookingFor_TypeOfDating_ShortTermRelationship = pr.LookingFor_TypeOfDating_ShortTermRelationship;
                            masterR.LookingFor_TypeOfDating_Friendship = pr.LookingFor_TypeOfDating_Friendship;
                            masterR.LookingFor_TypeOfDating_LongTermRelationship = pr.LookingFor_TypeOfDating_LongTermRelationship;
                            masterR.LookingFor_TypeOfDating_MutuallyBeneficialArrangements = pr.LookingFor_TypeOfDating_MutuallyBeneficialArrangements;
                            masterR.LookingFor_TypeOfDating_MarriedDating = pr.LookingFor_TypeOfDating_MarriedDating;
                            masterR.LookingFor_TypeOfDating_AdultDating_Casual = pr.LookingFor_TypeOfDating_AdultDating_Casual;
                        }
                    }


                    myContext.SubmitChanges();

                    result = "ok";

                    //update last activity
                    UpdateActivity(pr.ProfileID);
                    DataHelpers.LogProfileAccess(pr.ProfileID, pr.LoginName, lp.password, true, "Login-[Personal Data Save]", lp.logindetails, "", ip, true, "Service4-setProfileEditLookingFor");
                }
                else {
                    result = "failed";
                    DataHelpers.LogProfileAccess(0, lp.email, lp.password, false, "Login-[Personal Data Save]", lp.logindetails, "", ip, true, "Service4-setProfileEditLookingFor");

                }


            }
            catch (Exception ex) {
                if (CurrentSettings.AllowSendExceptions)
                    result = ex.ToString();
                else
                    result = "exception";
                WebErrorSendEmail(ex, "");
            }
            finally {
                myContext.Dispose();
            }
            return result;

        }


        private static string[] ListTypes = { "bodytype", "breastsize", "childrennumber", "drinking", "education", 
                                       "ethnicity", "eyecolor", "gender", "haircolor", "height", "income", 
                                       "job", "networth", "photosdisplaylevel",  "relationshipstatus", 
                                       "religion", "reportingreason", "smoking", "spokenlanguages", "typeofdating" };

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "getList/{listType}/{lang}")]
        public ItemsList getList(string listType, string lang) {
            ItemsList list = new ItemsList();
            list.items = new List<ListItem>();

            bool isvalid = !string.IsNullOrEmpty(lang) && !string.IsNullOrEmpty(listType) && ListTypes.Contains(listType.ToLower());

            if (isvalid) {
                lang = lang.ToUpper();
                string defLang = "US";
                string idColName = "";
                DataTable dt = null;
                switch (listType) {
                    case "bodytype":
                        dt = Lists.gDSLists.EUS_LISTS_BodyType;
                        idColName = "BodyTypeId";
                        break;
                    case "breastsize":
                        dt = Lists.gDSLists.EUS_LISTS_BreastSize;
                        idColName = "BreastSizeID";
                        break;
                    case "childrennumber":
                        dt = Lists.gDSLists.EUS_LISTS_ChildrenNumber;
                        idColName = "ChildrenNumberId";
                        break;
                    case "drinking":
                        dt = Lists.gDSLists.EUS_LISTS_Drinking;
                        idColName = "DrinkingId";
                        break;
                    case "education":
                        dt = Lists.gDSLists.EUS_LISTS_Education;
                        idColName = "EducationId";
                        break;
                    case "ethnicity":
                        dt = Lists.gDSLists.EUS_LISTS_Ethnicity;
                        idColName = "EthnicityId";
                        break;
                    case "eyecolor":
                        dt = Lists.gDSLists.EUS_LISTS_EyeColor;
                        idColName = "EyeColorId";
                        break;
                    case "gender":
                        dt = Lists.gDSLists.EUS_LISTS_Gender;
                        idColName = "GenderId";
                        break;
                    case "haircolor":
                        dt = Lists.gDSLists.EUS_LISTS_HairColor;
                        idColName = "HairColorId";
                        break;
                    case "height":
                        dt = Lists.gDSLists.EUS_LISTS_Height;
                        idColName = "HeightId";
                        break;
                    case "income":
                        dt = Lists.gDSLists.EUS_LISTS_Income;
                        idColName = "IncomeId";
                        break;
                    case "job":
                        dt = Lists.gDSLists.EUS_LISTS_Job;
                        idColName = "JobID";
                        break;
                    case "networth":
                        dt = Lists.gDSLists.EUS_LISTS_NetWorth;
                        idColName = "NetWorthId";
                        break;
                    case "photosdisplaylevel":
                        dt = Lists.gDSLists.EUS_LISTS_PhotosDisplayLevel;
                        idColName = "PhotosDisplayLevelId";
                        break;
                    case "relationshipstatus":
                        dt = Lists.gDSLists.EUS_LISTS_RelationshipStatus;
                        idColName = "RelationshipStatusId";
                        break;
                    case "religion":
                        dt = Lists.gDSLists.EUS_LISTS_Religion;
                        idColName = "ReligionId";
                        break;
                    case "reportingreason":
                        dt = Lists.gDSLists.EUS_LISTS_ReportingReason;
                        idColName = "ReportingReasonId";
                        break;
                    case "smoking":
                        dt = Lists.gDSLists.EUS_LISTS_Smoking;
                        idColName = "SmokingId";
                        break;
                    case "spokenlanguages":
                        dt = DataHelpers.GetEUS_LISTS_SpokenLanguages_Datatable();
                        idColName = "SpokenLangId";
                        lang = "LangName";
                        defLang = "";
                        break;
                    case "typeofdating":
                        dt = Lists.gDSLists.EUS_LISTS_TypeOfDating;
                        idColName = "TypeOfDatingId";
                        break;
                }

                if (dt != null) {
                    if (!dt.Columns.Contains(lang)) { lang = "US"; }

                    list.items = (from row in dt.AsEnumerable()
                                  select new ListItem
                                  {
                                      id = row[idColName].ToString(),
                                      title = row[lang].ToString() != "" ? row[lang].ToString() : (defLang != "" ? row[defLang].ToString() : "")
                                  }).ToList<ListItem>();
                }
            }
            return list;
        }



        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "blockedcount/{userid}/{lat}/{lng}")]
        public int getBlockedCount(string userid, string lat, string lng) {
            int counter = 0;
            int iUserid = Convert.ToInt32(userid);
            UpdateActivity(iUserid);

            clsMyListsHelperParameters parms = new clsMyListsHelperParameters();
            parms.CurrentProfileId = iUserid;
            parms.sorting = MyListsSortEnum.Recent;
            parms.ReturnRecordsWithStatus = (int)ProfileStatusEnum.Approved;
            parms.latitudeIn = Convert.ToDouble(lat);
            parms.longitudeIn = Convert.ToDouble(lng);
            parms.Distance = clsSearchHelper.DISTANCE_DEFAULT;

            parms.performCount = true;
            parms.rowNumberMin = 0;
            parms.rowNumberMax = 0;

            DataSet ds = clsSearchHelper.GetMyBlockedMembersDataTable(parms);

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0) {
                counter = (int)ds.Tables[0].Rows[0][0];
            }
            return counter;
        }


        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "blocked/{userid}/{lat}/{lng}/{indexstart}")]
        public UserProfilesList getBlocked(string userid, string lat, string lng, string indexstart) {

            UserProfilesList pList = new UserProfilesList();
            pList.Profiles = new List<UserProfile>();

            int iUserid = Convert.ToInt32(userid);
            UpdateActivity(iUserid);

            clsMyListsHelperParameters parms = new clsMyListsHelperParameters();
            parms.CurrentProfileId = iUserid;
            parms.sorting = MyListsSortEnum.Recent;
            parms.ReturnRecordsWithStatus = (int)ProfileStatusEnum.Approved;
            parms.latitudeIn = Convert.ToDouble(lat);
            parms.longitudeIn = Convert.ToDouble(lng);
            parms.Distance = clsSearchHelper.DISTANCE_DEFAULT;

            parms.performCount = false;
            parms.rowNumberMin = Convert.ToInt32(indexstart);
            parms.rowNumberMax = Convert.ToInt32(indexstart) + 20;

            DataSet ds = clsSearchHelper.GetMyBlockedMembersDataTable(parms);

            if (ds != null) {
                pList = createUserProfilesListfromDataset(ds);
            }

            return pList;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "blockaction/{fromprofileid}/{toprofileid}/{action}")]
        public override string performBlockAction(string fromprofileid, string toprofileid, string action) {
            return base.performBlockAction(fromprofileid, toprofileid, action);
        }



        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "getCredits/{userid}")]
        public int getAvailableCredits(string userid) {
            return base.getAvailableCredits(userid, true);
        }


        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "changePassword/{newPassword}")]
        public string changePassword(LoginParameters lp, string newPassword) {
            string result = "";
            if (string.IsNullOrEmpty(newPassword)) {
                result = "Empty pass";
                return result;
            }

            try {

                var pr = (from p in myContext.EUS_Profiles
                          where
                             p.Password == lp.password &&
                             (p.eMail == lp.email || p.LoginName == lp.email) &&
                             p.Status != (int)ProfileStatusEnum.Deleted &&
                             p.Status != (int)ProfileStatusEnum.DeletedByUser
                          select p);

                string ip = getClientIP();
                if (pr != null) {

                    int profileId = 0;
                    string LoginName = "";
                    EUS_Profile p = null;
                    IEnumerator<EUS_Profile> enm = pr.GetEnumerator();
                    while (enm.MoveNext()) {
                        p = enm.Current;
                        p.Password = newPassword;

                        if (p.IsMaster == true) {
                            profileId = p.ProfileID;
                            LoginName = p.LoginName;
                        }
                    }

                    if (profileId == 0 && p != null) {
                        profileId = p.ProfileID;
                        LoginName = p.LoginName;
                    }
                    if (!ServiceBase.allowUserAction(profileId, lp.logindetails)) {
                        throw new ActionNotAllowedException();
                    }

                    if (string.Compare(newPassword, LoginName, true) == 0) {
                        result = "failed. pass and login are same";
                        DataHelpers.LogProfileAccess(profileId, LoginName, lp.password, true, "Login-[Change Pass Failed (1)]", lp.logindetails, "", ip, true, "Service4-changePassword");
                    }
                    else {
                        myContext.SubmitChanges();

                        result = "ok";

                        //update last activity
                        UpdateActivity(profileId);
                        DataHelpers.LogProfileAccess(profileId, LoginName, lp.password, true, "Login-[Change Pass]", lp.logindetails, "", ip, true, "Service4-changePassword");
                    }
                }

                if (pr == null && string.IsNullOrEmpty(result)) {
                    result = "failed";
                    DataHelpers.LogProfileAccess(0, lp.email, lp.password, false, "Login-[Change Pass Failed (2)]", lp.logindetails, "", ip, true, "Service4-changePassword");
                }


            }
            catch (Exception ex) {
                if (CurrentSettings.AllowSendExceptions)
                    result = ex.ToString();
                else
                    result = "exception";
                WebErrorSendEmail(ex, "");
            }
            finally {
                myContext.Dispose();
            }
            return result;

        }


        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "deleteAccount")]
        public string deleteAccount(LoginParameters lp) {
            StringBuilder resultSb = new StringBuilder();
            string result = "";
            try {
                //if (lp.email == "" && lp.password == "") {
                //    lp.email = "savva82";
                //    lp.password = "savva123@";
                //}

                var pr = (from p in myContext.EUS_Profiles
                          where
                             p.Password == lp.password &&
                             (p.eMail == lp.email || p.LoginName == lp.email) &&
                             p.Status != (int)ProfileStatusEnum.Deleted &&
                             p.Status != (int)ProfileStatusEnum.DeletedByUser
                          select p).FirstOrDefault();

                string ip = getClientIP();
                if (pr != null) {
                    if (!ServiceBase.allowUserAction(pr.ProfileID, lp.logindetails)) {
                        throw new ActionNotAllowedException();
                    }

                    //result += " " + lp.email + " user found";
                    clsCountryByIP country = clsCountryByIP.GetCountryCodeFromIP(ip, CurrentSettings.ConnectionString_GEODB);
                    //result += " " + country.CountryCode + " CountryCode";

                    DeleteAccountParams parms = new DeleteAccountParams();
                    parms.IP = ip;
                    parms.GEO_COUNTRY_CODE = country.CountryCode;
                    parms.HTTP_USER_AGENT = "";
                    parms.Referrer = "";
                    parms.SupportTeamEmail = CurrentSettings.gToEmail;
                    //result += " " + CurrentSettings.gToEmail + " gToEmail";

                    ActionResult ActionResult1 = null;
                    if (pr.IsMaster == true) {
                        ActionResult1 = ProfileHelper.DeleteAccount(pr.ProfileID, pr.MirrorProfileID.Value, parms, sendEmailToSupport: false);
                    }
                    else {
                        ActionResult1 = ProfileHelper.DeleteAccount(pr.MirrorProfileID.Value , pr.ProfileID, parms, sendEmailToSupport: false);
                    }

                    if (!ActionResult1.HasErrors) {
                        result += "-ok-";

                        // send email to support
                        Dating.Server.Core.DLL.clsMyMail.SendMail2(
                            CurrentSettings.gSMTPLoginName,
                            ActionResult1.MailData.ToAddress,
                            ActionResult1.MailData.Subject,
                            ActionResult1.MailData.Content,
                            ActionResult1.MailData.IsHtmlBody,
                            ActionResult1.MailData.BCCRecipients);
                    }
                    else {
                        resultSb.AppendLine(ActionResult1.ExtraMessage);

                        if (CurrentSettings.AllowSendExceptions)
                            result += ActionResult1.ExtraMessage;
                        else
                            result += "-exception-";

                        WebErrorSendEmail(new System.Exception(ActionResult1.ExtraMessage), "");
                    }
                }

                if (pr == null) {
                    result += "-failed-";
                    DataHelpers.LogProfileAccess(0, lp.email, lp.password, false, "Login-[Delete account]", lp.logindetails, "", ip, true, "Service4-deleteAccount");
                }


            }
            catch (Exception ex) {
                resultSb.AppendLine(ex.ToString());

                if (CurrentSettings.AllowSendExceptions)
                    result += " " + ex.ToString();
                else
                    result += "-exception-";
                //WebErrorSendEmail(ex, "");
            }
            finally {
                myContext.Dispose();
            }


            //if (false && resultSb.Length > 0) {
            //    resultSb.Insert(0, result + "\r\n");
            //    result = resultSb.ToString();
            //}

            return result;
        }


        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "privatephoto/{profileid}")]
        public ImagePath getRandomPrivatePhoto(string profileid) {
            ImagePath imPath = null;
            try {
                int iProfileid = Convert.ToInt32(profileid);

                //4 for Approved
                var pr = (from p in myContext.EUS_Profiles
                          where p.ProfileID == iProfileid && p.Status == (int)ProfileStatusEnum.Approved
                          select new
                          {
                              ProfileID = p.ProfileID,
                              GenderId = p.GenderId
                          }).FirstOrDefault();

                if (pr != null) {
                    iProfileid = pr.ProfileID;

                    EUS_CustomerPhoto photo =
                            (from p in myContext.EUS_CustomerPhotos
                             where
                             p.CustomerID == iProfileid &&
                             p.DisplayLevel > 0 &&
                             p.HasAproved == true &&
                             p.HasDeclined == false &&
                             (p.IsDeleted == null || p.IsDeleted == false)
                             select p).FirstOrDefault();

                    if (photo != null) {
                        string path = getImagePath(iProfileid, photo.FileName, Convert.ToInt32(pr.GenderId), true, false);
                        imPath = new ImagePath()
                        {
                            Path = path,
                            Level = clsNullable.NullTo(photo.DisplayLevel),
                            photoId = photo.CustomerPhotosID.ToString()
                        };
                    }


                }
            }
            catch (Exception) { }
            finally {
                myContext.Dispose();
            }
            return imPath;
        }


        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "setphotolevel/{profileid}/{photoid}/{level}")]
        public string setPhotoLevel(string profileid, string photoid, string level) {
            string result = "-ok-";
            try {
                int iProfileid = Convert.ToInt32(profileid);
                long iPhotoid = Convert.ToInt64(photoid);
                int iLevel = Convert.ToInt32(level);

                UpdateActivity(iProfileid);

                ProfileHelper ph = new ProfileHelper();
                ph.OnExceptionThrowed += ProfileHelper_OnExceptionThrowed;
                bool ok = ph.SetPhotoLevel(iProfileid, iPhotoid, iLevel);
                if (!ok) {
                    result = "-failed-";
                }
            }
            catch (Exception ex) {
                result = "-exception-";
                WebErrorSendEmail(ex, "setPhotoLevel");
            }
            finally {
                myContext.Dispose();
            }
            return result;
        }




        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "restorepassword/{login}")]
        public string restorePassword(string login) {
            string result = "-ok-";
            try {


                if (!string.IsNullOrEmpty(login)) {

                    // retrieve user password
                    //EUS_Profile pr = (from itm in this.myContext.EUS_Profiles
                    //      where
                    //          (itm.LoginName.ToUpper() == login.ToUpper() || itm.eMail.ToUpper() == login.ToUpper()) &&
                    //          (itm.Status == (int)ProfileStatusEnum.Approved || itm.Status == (int)ProfileStatusEnum.NewProfile || itm.Status == (int)ProfileStatusEnum.Rejected)
                    //      select itm).FirstOrDefault();

                    EUS_Profile pr = DataHelpers.GetEUS_Profile_ForPasswordRestore(this.myContext, login);
                    if (pr != null) {

                        clsSiteLAG o = new clsSiteLAG();
                        string subject = o.GetCustomStringFromPage(pr.LAGID, "RestorePassword.aspx", "RecoveryPassword_EmailSubject", CurrentSettings.ConnectionString);
                        string body = o.GetCustomStringFromPage(pr.LAGID, "RestorePassword.aspx", "RecoveryPassword_EmailBody", CurrentSettings.ConnectionString);

                        string content = System.Environment.NewLine + body.Replace("[PASSWORD]", pr.Password);
                        Dating.Server.Core.DLL.clsMyMail.SendMail2(CurrentSettings.gSMTPLoginName, pr.eMail, subject, content);

                    }
                    else {
                        result = "-user not found-";
                    }


                }
                else {
                    result = "-empty login-";
                }


            }
            catch (Exception ex) {
                result = "-exception-";
                WebErrorSendEmail(ex, "restorePassword");
            }
            finally {
                myContext.Dispose();
            }
            return result;
        }


        public void ProfileHelper_OnExceptionThrowed(Exception ex, string ExtraMessage) {
            WebErrorSendEmail(ex, ExtraMessage);
        }




        private string GetIsVip(DataTable dt, DataRow dr) {
            string IsVip = "0";
            if (dt.Columns.Contains("AvailableCredits")) {
                IsVip = (
                            (clsNullable.DBNullToInteger(dr["AvailableCredits"]) >= ProfileHelper.Config_UNLOCK_MESSAGE_READ_CREDITS) ||
                            clsNullable.DBNullToBoolean(dr["HasSubscription"])
                        ).ToString();
                IsVip = (IsVip == true.ToString() ? "1" : "0");
            }
            return IsVip;
        }


    }// end class


}

﻿using Dating.Server.Core.DLL;
using Dating.Server.Datasets.DLL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Web;
using System.Text;
using System.Web;

namespace GoomenaLib
{
    public class ServiceBase
    {

        static ServiceBase() {
            DataHelpers.SetConnectionString(CurrentSettings.ConnectionString);
            ServiceBase.ReadSMTPSettings();
        }

        //private Properties.Settings __CurrentSettings;
        //internal Properties.Settings CurrentSettings {
        //    get {
        //        if (__CurrentSettings == null)
        //            __CurrentSettings = new Properties.Settings();
        //        return __CurrentSettings;
        //    }
        //}


        protected CMSDBDataContext _context;
        public CMSDBDataContext myContext {
            get {
                try {
                    if (_context != null) {
                        // exception thrown when disposed
                        System.Data.Common.DbConnection a = _context.Connection;
                    }
                }
                catch (Exception) { _context = null; }

                if (_context == null) {
                    _context = new CMSDBDataContext(CurrentSettings.ConnectionString);
                }
                return _context;
            }
            set { _context = value; }
        }


        protected bool checkProfileVip(int profileId) {
            EUS_Profile q = null;
            using (myContext) {
                q = (from p in myContext.EUS_Profiles
                     where p.ProfileID == profileId &&
                     p.IsMaster == true && p.GenderId == 1
                     select p).FirstOrDefault();

                myContext.Dispose();

            }

            if (q != null) {

                if (q.AvailableCredits.HasValue && q.AvailableCredits >= ProfileHelper.Config_UNLOCK_MESSAGE_READ_CREDITS) {
                    return true;
                }
                else {
                    return false;
                }
            }

            return false;

        }

        #region "EUS_GCMProfile"

        public virtual string sendNotificationsToAllUsers(string message) {

            string response = "";

            try {
                using (myContext) {

                    List<EUS_GCMProfile> list = (from cp in myContext.EUS_GCMProfiles
                                                 select cp).ToList();

                    for (int i = 0; i < list.Count; i++) {
                        EUS_GCMProfile itm = list[i];
                        if (itm.ProfileID.HasValue) {
                            bool IsActiveMember = DataHelpers.EUS_Profile_IsProfileActive(itm.ProfileID.Value);
                            if (IsActiveMember) {
                                response += "Device id: " + itm.GCM_RegID + "-----";
                                response += SendNotification(itm.GCM_RegID, message);
                            }
                        }
                    }
                    response += "\n--------------------------------------";
                    response += "Send to: " + list.Count() + " profiles";

                    myContext.Dispose();
                }

            }
            catch (Exception e) {
                response += "Exception: " + e.ToString();
            }

            return response;
        }

        public virtual string saveGCMUserSettings(string profileId, string notifMessages, string notifLikes, string notifOffers) {
            string result = "";
            try {
                using (myContext) {

                    EUS_GCMProfile profile = (from p in myContext.EUS_GCMProfiles
                                              where p.ProfileID == Convert.ToInt32(profileId)
                                              select p).FirstOrDefault();
                    if (profile != null) {

                        profile.Notif_Messages = Convert.ToInt32(notifMessages);
                        profile.Notif_Likes = Convert.ToInt32(notifLikes);
                        profile.Notif_Offers = Convert.ToInt32(notifOffers);
                        myContext.SubmitChanges();
                        result += "OK";
                    }
                    result += "profile not found";
                    myContext.Dispose();
                }
            }
            catch (Exception e) {
                result += e.ToString();
            }
            return result;
        }

        #endregion


        public string SendNotification(string deviceId, string message) {


            string GoogleAppID = "AIzaSyA0JGSfbeJjoGTdTEEJFBQPQM02VV266b8";
            //string authKey = "63851648440.apps.googleusercontent.com";
            var SENDER_ID = "63851648440";
            var value = message;
            WebRequest tRequest;
            tRequest = WebRequest.Create("https://android.googleapis.com/gcm/send");
            tRequest.Method = "POST";
            tRequest.ContentType = "application/x-www-form-urlencoded;charset=UTF-8";
            tRequest.Headers.Add("Authorization", "key=" + GoogleAppID);

            tRequest.Headers.Add("Sender", "id=" + SENDER_ID);

            string postData = "collapse_key=score_update&time_to_live=108&delay_while_idle=1&data.message=" + value + "&data.time=" + System.DateTime.Now.ToString() + "&registration_id=" + deviceId;


            Byte[] byteArray = Encoding.UTF8.GetBytes(postData);
            tRequest.ContentLength = byteArray.Length;

            Stream dataStream = tRequest.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();


            WebResponse tResponse = tRequest.GetResponse();

            Stream dataStreamResponse = tResponse.GetResponseStream();

            StreamReader tReader = new StreamReader(dataStreamResponse);

            String sResponseFromServer = tReader.ReadToEnd();


            tReader.Close();
            dataStreamResponse.Close();
            tResponse.Close();


            Debug.WriteLine(sResponseFromServer);
            return sResponseFromServer + deviceId;
        }

        public virtual string sendNotificationToUser(string fromProfileId, string toProfileId, string type) {
            string response = "";
            try {
                int iToProfileId = Convert.ToInt32(toProfileId);
                int iFromProfileId = Convert.ToInt32(fromProfileId);
                using (myContext) {
                    List<EUS_GCMProfile> list = (from cp in myContext.EUS_GCMProfiles
                                                 where cp.ProfileID == iToProfileId && cp.GCM_RegID != null
                                                 select cp)
                                                 .Take(1)
                                                 .ToList();



                    string fromLoginName = (from p in myContext.EUS_Profiles
                                            where p.ProfileID == iFromProfileId
                                            select p.LoginName).FirstOrDefault();


                    string toLoginName = (from p in myContext.EUS_Profiles
                                          where p.ProfileID == iToProfileId
                                          select p.LoginName).FirstOrDefault();

                    int typeInt = Convert.ToInt32(type);
                    string message = "";

                    bool hasPreviousAction = clsUserDoes.HasAnyOffer(iFromProfileId, iToProfileId);
                    bool isAnyMessage = clsUserDoes.IsAnyMessageExchanged(iFromProfileId, iToProfileId);


                    EUS_GCMProfile gcmProfile = null;
                    if (list.Count > 0) {
                        gcmProfile = list[0];
                    }
                    bool allowSend = false;


                    if (gcmProfile != null) {

                        string notifType = "";

                        switch (typeInt) {
                            //offers
                            case 1:
                                notifType = "OFFER";
                                if (gcmProfile.Notif_Offers != null) {
                                    allowSend = checkAllowSend(gcmProfile.Notif_Offers.Value, hasPreviousAction);
                                }

                                message = "You received an offer from " + fromLoginName;
                                break;
                            //likes
                            case 2:
                                notifType = "LIKE";
                                if (gcmProfile.Notif_Likes != null) {
                                    allowSend = checkAllowSend(gcmProfile.Notif_Likes.Value, hasPreviousAction);
                                }

                                message = "You received a like from " + fromLoginName;
                                break;
                            //messages
                            case 3:
                                notifType = "MESSAGE";
                                if (gcmProfile.Notif_Messages != null) {
                                    allowSend = checkAllowSend(gcmProfile.Notif_Messages.Value, isAnyMessage);
                                }

                                message = "You received a message from " + fromLoginName;
                                break;

                            //default photo changed
                            case 4:
                                notifType = "DEFAULT-PHOTO";
                                message = "Default photo changed " + fromLoginName;
                                break;

                        }

                        clsGetMemberActionsCounters counters = clsUserDoes.GetMemberActionsCounters(iToProfileId);

                        string regId = gcmProfile.GCM_RegID;
                        //string json = "{\"fromProfileId\":\"" + fromProfileId + "\" , \"message\":\"" + message + "\" }";
                        string json = "{\"fromProfileId\":\"" + fromProfileId + "\" , \"message\":\"" + message + "\" , \"newlikescount\":\"" + counters.NewLikes + "\" , \"newmessagescount\":\"" + counters.NewMessages + "\" , \"newofferscount\":\"" + counters.NewOffers + "\", \"type\":\"" + typeInt + "\"}";


                        bool IsActiveMember = DataHelpers.EUS_Profile_IsProfileActiveByDeviceID(regId);
                        if (allowSend && IsActiveMember) {
                            response += SendNotification(regId, json);
                            response += "sent notification type " + notifType + " profile id: " + fromProfileId + "-" + fromLoginName + " to profile id: " + toProfileId + "-" + toLoginName;
                        }
                        else {
                            response += "notification type " + notifType + " not sent due to user settings";
                        }

                    }
                    else {
                        response += " user not found";
                    }

                }

            }
            catch (Exception e) {
                response += " exception: " + e.ToString();
            }

            return response;
        }


        public bool checkAllowSend(int value, bool hasPreviousAction) {
            bool allowSend = false;
            if (value == 0) {
                allowSend = false;
            }
            else if (value == 1) {
                allowSend = true;
            }
            else if (value == 2 && hasPreviousAction) {
                allowSend = true;
            }
            else if (value == 3 && !hasPreviousAction) {
                allowSend = true;
            }
            return allowSend;

        }

        public static void WebErrorSendEmail(Exception ex, string ExtraMessage) {
            try {
                if (clsMyMail.MySMTPSettings.gSMTPServerName == null) ServiceBase.ReadSMTPSettings();

                string msg = ex.Message;
                if ((msg.Length > 200))
                    msg = msg.Remove(197) + "...";
                string subject = "Exception on " + "mobile service" + " (" + msg + ")";

                string contetn = "";
                contetn = ExtraMessage + "\r\n" + "\r\n" + ex.ToString();

                try {
                    contetn = contetn + "\r\n" + "\r\n";

                    string ip = getClientIP();
                    contetn = contetn + "IP: " + ip + "\r\n";

                    clsCountryByIP country = clsCountryByIP.GetCountryCodeFromIP(ip, CurrentSettings.ConnectionString_GEODB);
                    contetn = contetn + "IP GEO: " + country.CountryCode + "\r\n";
                }
                catch (Exception) { }

                Dating.Server.Core.DLL.clsMyMail.SendMail2(CurrentSettings.gSMTPLoginName, CurrentSettings.ExceptionsEmail, subject, contetn);
            }
            catch (Exception) { }
        }

        public static void ReadSMTPSettings() {
            try {
                clsMyMail.MySMTPSettings.gSMTPOutPort = CurrentSettings.gSMTPOutPort;
                clsMyMail.MySMTPSettings.gSMTPServerName = CurrentSettings.gSMTPServerName;
                clsMyMail.MySMTPSettings.gSMTPLoginName = CurrentSettings.gSMTPLoginName;
                clsMyMail.MySMTPSettings.gSMTPPassword = CurrentSettings.gSMTPPassword;
            }
            catch (Exception) { }
        }


        public static string getClientIP() {
            OperationContext context = OperationContext.Current;
            MessageProperties prop = context.IncomingMessageProperties;
            RemoteEndpointMessageProperty endpoint = prop[RemoteEndpointMessageProperty.Name] as RemoteEndpointMessageProperty;
            string ip = endpoint.Address;
            return ip;
        }


        public static clsGlobalPositionWCF GetPositionForMinPostalCode(string countryCode) {
            clsGlobalPositionWCF pos = null;
            try {
                string PostcodeTmp = null;
                DataTable dt = clsGeoHelper.GetCountryMinPostcodeDataTable(countryCode, null, null, null);
                if ((dt!=null) && (dt.Rows.Count > 0)) {
                    PostcodeTmp = dt.Rows[0]["MinPostcode"].ToString();
                    if (dt.Rows[0]["countryCode"].ToString() != "")
                        countryCode = dt.Rows[0]["countryCode"].ToString();

                    dt = clsGeoHelper.GetGEOByZip(countryCode, PostcodeTmp, null);
                    pos = new clsGlobalPositionWCF(dt.Rows[0]["latitude"].ToString(), dt.Rows[0]["longitude"].ToString());
                }

            }
            catch (Exception ex) {
                WebErrorSendEmail(ex, "GetPositionForMinPostalCode");
            }
            return pos;
        }


        public void UpdateActivity(int profileID) {
            try {
                DataHelpers.UpdateEUS_Profiles_Activity(profileID, getClientIP(), true, true);
            }
            catch (Exception ex) {
                WebErrorSendEmail(ex, "UpdateActivity");
            }
        }

        public string getCountryCode(string ip) {
            try {
                clsCountryByIP country = clsCountryByIP.GetCountryCodeFromIP(ip, CurrentSettings.ConnectionString_GEODB);

                if (country.CountryCode.Length == 2) {
                    return country.CountryCode;
                }
            }
            catch (Exception ex) {
                WebErrorSendEmail(ex, "");
            }
            return "";
        }


        public virtual string checkDifferentCountry(string userid1, string userid2) {
            try {

                using (myContext) {
                    EUS_Profile p1 = (from p in myContext.EUS_Profiles
                                      where p.ProfileID == Convert.ToInt32(userid1)
                                      select p).FirstOrDefault();

                    EUS_Profile p2 = (from p in myContext.EUS_Profiles
                                      where p.ProfileID == Convert.ToInt32(userid2)
                                      select p).FirstOrDefault();


                    if (p1.Country == p2.Country) {
                        return "true";
                    }
                    else {
                        return "false";
                    }
                }

            }
            catch (Exception) {
                return "error";
            }
        }


        public virtual string hasPhotos(string userid) {

            try {
                Dating.Server.Datasets.DLL.DSMembers.EUS_CustomerPhotosDataTable dtPhotos = DataHelpers.GetEUS_CustomerPhotos_ByProfileOrMirrorID(Convert.ToInt32(userid)).EUS_CustomerPhotos;
                if (dtPhotos.Rows.Count > 0) {
                    return "true";
                }
                else {
                    return "false";
                }

            }
            catch (Exception) {
                return "error";
            }
        }

        public virtual string getProfilePhotoLevel(string fromProfileId, string toProfileId) {
            string result = "";
            try {
                using (myContext) {
                    EUS_ProfilePhotosLevel photosLevel = (from itm in myContext.EUS_ProfilePhotosLevels
                                                          where itm.FromProfileID == Convert.ToInt32(fromProfileId)
                                                          && itm.ToProfileID == Convert.ToInt32(toProfileId)
                                                          select itm).FirstOrDefault();

                    if (photosLevel != null) {
                        result = Convert.ToString(photosLevel.PhotoLevelID);
                    }
                    else {
                        result = "0";
                    }

                    myContext.Dispose();
                }
            }
            catch (Exception) {
                result = "error";
            }
            return result;
        }


        public static bool GetIsOnline(DateTime lastActivityUTCDate, DataTable dt, DataRow dr) {
            DateTime? __LastActivityDateTime = (!dr.IsNull("LastActivityDateTime") ? (DateTime?)(System.Convert.ToDateTime(dr["LastActivityDateTime"])) : null);
            bool OtherMemberIsOnline = false;
            if (dt.Columns.Contains("IsOnlineNow")) {
                OtherMemberIsOnline = (!dr.IsNull("IsOnlineNow") ? System.Convert.ToBoolean(dr["IsOnlineNow"]) : false);
            }
            else {
                OtherMemberIsOnline = (!dr.IsNull("IsOnline") ? System.Convert.ToBoolean(dr["IsOnline"]) : false);
                if (OtherMemberIsOnline) {
                    if (__LastActivityDateTime.HasValue)
                        OtherMemberIsOnline = __LastActivityDateTime.Value >= lastActivityUTCDate;
                    else
                        OtherMemberIsOnline = false;
                }
            }
            return OtherMemberIsOnline;
        }

        public static DateTime getMinActivityDate() {
            int mins = 20;
            clsConfigValues config = new clsConfigValues();
            if (!int.TryParse(config.members_online_minutes, out mins)) mins = 20;
            DateTime result = DateTime.UtcNow.AddMinutes(-mins);
            return result;
        }

        public static bool IsProfileOnline(DateTime LastActivityUTCDate, EUS_Profile p) {
            bool bIsOnline = false;
            if (p.LastActivityDateTime != null && p.IsOnline != null) {
                if ((p.LastActivityDateTime.Value >= LastActivityUTCDate) && p.IsOnline.Value) {
                    bIsOnline = true;
                    if (clsProfilesPrivacySettings.GET_PrivacySettings_ShowMeOffline(p.ProfileID))
                        bIsOnline = false;
                }
            }
            return bIsOnline;
        }


        public static bool IsProfileOnline(DateTime LastActivityUTCDate, vw_EusProfile_Light p) {
            bool bIsOnline = false;
            if (p.LastActivityDateTime != null && p.IsOnline != null) {
                if ((p.LastActivityDateTime.Value >= LastActivityUTCDate) && p.IsOnline.Value) {
                    bIsOnline = true;
                    if (clsProfilesPrivacySettings.GET_PrivacySettings_ShowMeOffline(p.ProfileID))
                        bIsOnline = false;
                }
            }
            return bIsOnline;
        }


        public virtual string getUserState(string userid) {
            using (myContext) {
                string result = "Offline";

                if (!clsProfilesPrivacySettings.GET_PrivacySettings_ShowMeOffline(Convert.ToInt32(userid))) {

                    var currentProfile = (from p in myContext.EUS_Profiles
                                          where p.ProfileID == Convert.ToInt32(userid) && p.IsOnline == true
                                          select new
                                          {
                                              lastActivity = p.LastActivityDateTime,
                                              lastLogin = p.LastLoginDateTime
                                          }).FirstOrDefault();
                    if (currentProfile != null) {
                        DateTime before20mins = getMinActivityDate();
                        if (currentProfile.lastActivity >= before20mins)
                            result = "Online";
                    }
                }

                myContext.Dispose();
                return result;
            }
        }


        protected void sendNotificationEmailToSupport(Dating.Server.Datasets.DLL.DSMembers.EUS_ProfilesRow mirrorRow, bool isAutoApproveOn, clsCountryByIP country, GoomenaLib.RegisterData data) {
            try {
                clsSiteLAG o = new clsSiteLAG();
                string Content = o.GetCustomStringFromPage("US", "GlobalStrings", "EmailSendToSupport_MemberNew", CurrentSettings.ConnectionString_CMS);

                String toEmail = CurrentSettings.gToEmail;
                if (isAutoApproveOn) {
                    Content = Content.Replace("###YESNO###", "YES");
                }
                else {
                    Content = Content.Replace("###YESNO###", "NO");
                }

                Content = Content.Replace("###LOGINNAME###", mirrorRow.LoginName);
                Content = Content.Replace("###EMAIL###", mirrorRow.eMail);

                Content = Content.Replace("###APPROVEPROFILEURL###", "");
                Content = Content.Replace("###REJECTPROFILEURL###", "");

                try {

                    if (!mirrorRow.IslongitudeNull()) {
                        Content = Content.Replace("###LONGITUDE###", Convert.ToString(mirrorRow.longitude));
                    }
                    else {
                        Content = Content.Replace("###LONGITUDE###", "[Longitude not set]");
                    }

                }
                catch (Exception) { }

                try {
                    if (!mirrorRow.IslatitudeNull()) {
                        Content = Content.Replace("###LATITUDE###", Convert.ToString(mirrorRow.latitude));
                    }
                    else {
                        Content = Content.Replace("###LATITUDE###", "[Latitude not set]");
                    }
                }
                catch (Exception) { }

                try {
                    Content = Content.Replace("###BIRTHDATE###", Convert.ToString(mirrorRow.Birthday));
                }
                catch (Exception) { }

                try {
                    Content = Content.Replace("###AGE###", Convert.ToString(ProfileHelper.GetCurrentAge(mirrorRow.Birthday)));
                }
                catch (Exception) { }

                try {
                    Content = Content.Replace("###GENDER###", ProfileHelper.GetGenderString(mirrorRow.GenderId, "US"));
                }
                catch (Exception) { }
                try {
                    Content = Content.Replace("###SEX###", ProfileHelper.GetGenderString(mirrorRow.GenderId, "US"));
                }
                catch (Exception) { }

                try {
                    Content = Content.Replace("###COUNTRY###", ProfileHelper.GetCountryName(mirrorRow.Country));
                }
                catch (Exception) { }

                try {
                    Content = Content.Replace("###STATEREGION###", mirrorRow._Region);
                }
                catch (Exception) { }

                try {
                    Content = Content.Replace("###CITY###", mirrorRow.City);
                }
                catch (Exception) { }

                try {
                    Content = Content.Replace("###ZIP###", mirrorRow.Zip);
                }
                catch (Exception) { }

                try {
                    Content = Content.Replace("###DATETOREGISTER###", Convert.ToString(mirrorRow.DateTimeToRegister));
                }
                catch (Exception) { }

                try {
                    Content = Content.Replace("###PROFILEAPPROVEDYESNO###", mirrorRow.Status == 4 ? "YES" : "NO");
                }
                catch (Exception) { }

                try {
                    Content = Content.Replace("###APPROVEDPHOTOSYESNO###", "NO");
                }
                catch (Exception) { }

                try {
                    Content = Content.Replace("###AUTOAPPROVEDPHOTOYESNO###", "NO");
                }
                catch (Exception) { }

                try {
                    Content = Content.Replace("###IP###", getClientIP());
                }
                catch (Exception) { }

                try {
                    Content = Content.Replace("###CUSTOMERID###", Convert.ToString(mirrorRow.ProfileID));
                }
                catch (Exception) { }


                try {
                    if (country != null)
                        Content = Content.Replace("###GEOIP###", country.CountryCode);
                    else
                        Content = Content.Replace("###GEOIP###", "");
                }
                catch (Exception) {
                    Content = Content.Replace("###GEOIP###", "");
                }

                try {
                    if (data != null && data.logindetails != null)
                        Content = Content.Replace("###AGENT###", data.logindetails);
                    else
                        Content = Content.Replace("###AGENT###", "[Not Available]");
                }
                catch (Exception) {
                    Content = Content.Replace("###AGENT###", "");
                }


                string DateTimeToRegister = "";
                if (!mirrorRow.IsDateTimeToRegisterNull()) {
                    DateTimeToRegister = mirrorRow.DateTimeToRegister.ToLocalTime().ToString("dd/MM/yyyy HH:mm:ss");
                }

                if (Content != null) {

                    Content = Content
                        .Replace("Agent:", "Device:")
                        .Replace("###REFERRER###", "")
                        .Replace("###CUSTOMREFERRER###", mirrorRow.CustomReferrer)
                        .Replace("###LOGONCUSTOMER###", "")
                        .Replace("###SEARCHENGINEKEYWORDS###", "")
                        .Replace("###LANDINGPAGE###", "")
                        .Replace("###REGDATE###", DateTimeToRegister);
                }


                clsMyMail.SendMail2(CurrentSettings.gSMTPLoginName, CurrentSettings.gToEmail, "New user REGISTRATION on GOOMENA.", Content, true);
            }
            catch (Exception ex) {
                WebErrorSendEmail(ex, "sendNotificationEmailToSupport (handled)");

            }

        }

        /*
        protected void LogRegisterData(GoomenaLib.Version4.RegisterData data) {
            try {
                string extraMessage = "";
                //GoomenaLib.RegisterData data1 = new GoomenaLib.RegisterData();
                //data1.birthday = data.birthday;
                //data1.city = data.birthday;
                //data1.country = data.birthday;
                //data1.email = data.birthday;
                //data1.gender = data.birthday;
                //data1.lag = data.birthday;
                //data1.logindetails = data.birthday;
                //data1.password = data.birthday;
                //data1.region = data.birthday;
                //data1.username = data.birthday;

                extraMessage = AppUtils.ConvertObjectToXML(data, data.GetType());
                if (!string.IsNullOrEmpty(extraMessage)) {
                    extraMessage = extraMessage.Replace(@"<?xml version=""1.0"" encoding=""utf-16""?>", "");
                    extraMessage = "SERIALIAZED object follows " + "\r\n" + (new string('-', 30)) + extraMessage + "\r\n" + (new string('-', 30));
                }

                clsMyMail.SendMail2(CurrentSettings.gSMTPLoginName, CurrentSettings.ExceptionsEmail, "Adroid registration info", extraMessage, false);
            }
            catch (Exception ex) {
                WebErrorSendEmail(ex, "sendNotificationEmailToSupport-2 (handled)");
            }
        }


        protected void LogRegisterData(GoomenaLib.Version3.RegisterData data) {
            try {
                string extraMessage = "";
                //GoomenaLib.RegisterData data1 = new GoomenaLib.RegisterData();
                //data1.birthday = data.birthday;
                //data1.city = data.birthday;
                //data1.country = data.birthday;
                //data1.email = data.birthday;
                //data1.gender = data.birthday;
                //data1.lag = data.birthday;
                //data1.logindetails = data.birthday;
                //data1.password = data.birthday;
                //data1.region = data.birthday;
                //data1.username = data.birthday;

                extraMessage = AppUtils.ConvertObjectToXML(data, data.GetType());
                if (!string.IsNullOrEmpty(extraMessage)) {
                    extraMessage = extraMessage.Replace(@"<?xml version=""1.0"" encoding=""utf-16""?>", "");
                    extraMessage = "SERIALIAZED object follows " + "\r\n" + (new string('-', 30)) + extraMessage + "\r\n" + (new string('-', 30));
                }

                clsMyMail.SendMail2(CurrentSettings.gSMTPLoginName, CurrentSettings.ExceptionsEmail, "Adroid registration info", extraMessage, false);
            }
            catch (Exception ex) {
                WebErrorSendEmail(ex, "sendNotificationEmailToSupport-2 (handled)");
            }
        }


        protected void LogRegisterData(GoomenaLib.RegisterData data) {
            try {
                string extraMessage = "";
                //GoomenaLib.RegisterData data1 = new GoomenaLib.RegisterData();
                //data1.birthday = data.birthday;
                //data1.city = data.birthday;
                //data1.country = data.birthday;
                //data1.email = data.birthday;
                //data1.gender = data.birthday;
                //data1.lag = data.birthday;
                //data1.logindetails = data.birthday;
                //data1.password = data.birthday;
                //data1.region = data.birthday;
                //data1.username = data.birthday;

                extraMessage = AppUtils.ConvertObjectToXML(data, data.GetType());
                if (!string.IsNullOrEmpty(extraMessage)) {
                    extraMessage = extraMessage.Replace(@"<?xml version=""1.0"" encoding=""utf-16""?>", "");
                    extraMessage = "SERIALIAZED object follows " + "\r\n" + (new string('-', 30)) + extraMessage + "\r\n" + (new string('-', 30));
                }

                clsMyMail.SendMail2(CurrentSettings.gSMTPLoginName, CurrentSettings.ExceptionsEmail, "Adroid registration info", extraMessage, false);
            }
            catch (Exception ex) {
                WebErrorSendEmail(ex, "sendNotificationEmailToSupport-2 (handled)");
            }
        }
        */


        public int getAvailableCredits(string profileid, bool updateActivity) {
            int iMyprofileid = Convert.ToInt32(profileid);
            if (updateActivity) {
                DataHelpers.UpdateEUS_Profiles_Activity(iMyprofileid, getClientIP(), true);
            }
            //int AvailableCredits = 0;
            //string sqlProc = "EXEC [CustomerAvailableCredits] @CustomerId = " + profileid;
            //DataTable dt = DataHelpers.GetDataTable(sqlProc);
            //if (Convert.ToInt32(dt.Rows[0]["CreditsRecordsCount"]) > 0) {
            //    AvailableCredits = Convert.ToInt32(dt.Rows[0]["AvailableCredits"]);
            //}
            clsCustomerAvailableCredits customerAvailableCredits = clsUserDoes.GetCustomerAvailableCredits(iMyprofileid);
            int AvailableCredits = customerAvailableCredits.AvailableCredits;
            return AvailableCredits;
        }


        public virtual string performBlockAction(string fromprofileid, string toprofileid, string action) {
            try {
                int iFromprofileid = Convert.ToInt32(fromprofileid);
                int iToprofileid = Convert.ToInt32(toprofileid);
                if (iFromprofileid > 1 && iToprofileid > 1) {

                    if (action == "unblock")
                        clsUserDoes.MarkAsUnblocked(iToprofileid, iFromprofileid);
                    else if (action == "block")
                        clsUserDoes.MarkAsBlocked(iToprofileid, iFromprofileid);

                    return "ok";
                }
            }
            catch (Exception ex) {
                if (CurrentSettings.AllowSendExceptions)
                    return ex.ToString();
                else
                    return "exception";
            }
            return "failed";
        }


        public virtual string sendNotificationToUserList1(NotificationParams p) {
            string response = "";
            try {

                using (myContext) {
                    for (int i = 0; i < p.deviceIds.Count; i++) {
                        string deviceId = p.deviceIds[i];
                        bool IsActiveMember = DataHelpers.EUS_Profile_IsProfileActiveByDeviceID(deviceId);
                        if (IsActiveMember) {
                            response += SendNotification(deviceId, p.message);
                        }
                    }

                    myContext.Dispose();
                }

            }
            catch (Exception ex) {
                WebErrorSendEmail(ex, "");
                response += "exception: " + ex.ToString();
            }
            return response;
        }



        public static bool allowUserAction(int userid, string device) {
            bool? allow = null;

            if (CurrentSettings.LockToDeviceModel) {
                if (userid == 383 || userid == 384) {
                    allow = false;
                    if (!string.IsNullOrEmpty(device) && (device == "test" || device.EndsWith("Device Model ONE TOUCH 6012X")))
                        allow = true;
                }
            }

            if (allow == null)
                allow = true;

            return allow.Value;
        }
        public static bool allowUserAction(string login, string device) {
            bool? allow = null;

            if (login != null && login.ToLower() == "mariapa") {
                allow = allowUserAction(383, device);
            }
            if (allow == null) allow = true;

            return allow.Value;
        }
    }
}

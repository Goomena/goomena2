﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace GoomenaLib.Version3
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IGoomenaServiceV3" in both code and config file together.
    [ServiceContract]
    public interface IGoomenaServiceV3
    {
        // TODO: Add your service operations here
        [OperationContract]
        UserProfile getUserProfile(string id);

        [OperationContract]
        UserProfile login_v2(LoginParameters lp);


        [OperationContract]
        ImagePath getProfileImage(string profileId, string genderId);

        [OperationContract]
        UserDetails getUserProfileDetails(string id, string langId);

        [OperationContract]
        string getUserState(string userid);

        [OperationContract]
        ImagePathList getUserPhotos(string myuserid, string userid, string genderid);


        [OperationContract]
        MessageInfoList getMessagesQuick(string userid);

        [OperationContract]
        Message getMessageById(string messageid);

        [OperationContract]
        MessageList getConversationMessages(string profileid, string messageid);

        [OperationContract]
        string unlockMessage(string userid, string genderid, string senderid, string messageid, string offerid);

        [OperationContract]
        MessageInfoList getMessagesList(string userid, string view, string indexstart, string active);

        [OperationContract]
        string sendMessage(MessageParams mp);

        [OperationContract]
        string deleteMessage(string profileid, string messageid, string referrerparentid);

        [OperationContract]
        string deleteMessagesInView(string profileId, string messagesView, string messageId);

        [OperationContract]
        UserProfilesList getFavoriteList(string userid, string zip, string lat, string lng);

        [OperationContract]
        string viewProfile(string myprofileid, string viewprofileid);


        [OperationContract]
        string Favorite(string myuserid, string userid);

        [OperationContract]
        string UnFavorite(string myuserid, string userid);

        [OperationContract]
        string checkFavorite(string myuserid, string userid);

        [OperationContract]
        string checkHasPhotos(string userid);

        [OperationContract]
        string checkLike(string myprofileid, string otherprofileid);

        [OperationContract]
        string like(string myprofileid, string otherprofileid);

        [OperationContract]
        UserProfilesList getWhoFavoritedMe(string userid, string zip, string lat, string lng);

        [OperationContract]
        OfferList getLikes(string mod, string userid, string zip, string lat, string lng, string indexstart);

        [OperationContract]
        int getLikesCount(string userid);

        [OperationContract]
        int getMessagesCount(string userid);

        [OperationContract]
        OfferList getOffers(string mod, string userid, string zip, string lat, string lng, string indexstart);

        [OperationContract]
        string likeActions(string userid, string action, string offerid);

        [OperationContract]
        void rejectLike(string offerid, string reason);

        [OperationContract]
        string makeOffer(string mode, string amount, string fromprofileid, string toprofileid, string genderid, string offerid);

        [OperationContract]
        string acceptOffer(string myprofileid, string offerid);

        [OperationContract]
        string getHash(string username);

        [OperationContract]
        string poke(string myprofileid, string otherprofileid, string parentOffer);

        [OperationContract]
        string register(RegisterData data);

        [OperationContract]
        CountryList getCountries();

        [OperationContract]
        Regions getCountryRegions(string countryIso, string lagid);

        [OperationContract]
        Cities getRegionCities(string countryIso, string region, string lagid);

        [OperationContract]
        string setDefaultPhoto(string myprofileid, string photoid);

        [OperationContract]
        ImagePathList getUserThumbPhotos(string myuserid, string userid, string genderid);

        //-----------------notification functions-------------------------

        [OperationContract]
        string storeGCMUser(StoreUserParams p);

        [OperationContract]
        string sendNotificationsToAllUsers(string message);

        [OperationContract]
        string sendNotificationToUser(string fromProfileId, string toProfileId, string type);

        [OperationContract]
        string sendNotificationToUserList(NotificationParams p);

        [OperationContract]
        List<ProfileDeviceId> getGCMUsers();

        [OperationContract]
        string saveGCMUserSettings(string profileId, string notifMessages, string notifLikes, string notifOffers);

        [OperationContract]
        GCMUserSettings getSettings(string profileId);

        [OperationContract]
        string setProfilePhotoLevel(string fromProfileId, string toProfileId, string photoLevel);
        //----------------------------------------------------------------------
        [OperationContract]
        string getProfilePhotoLevel(string fromProfileId, string toProfileId);


        [OperationContract]
        UserProfilesList getWhoSharedMePhotos(string userid, string zip, string lat, string lng, string indexstart);

        [OperationContract]
        UserProfilesList getSharedByMePhotos(string userid, string zip, string lat, string lng, string indexstart);

        [OperationContract]
        string removeGCMUser(string regId);

        [OperationContract]
        UserProfilesList getSearchResults(string userid, string sort, string zipstr, string lat, string lng, string distance,
            string gender, string searchname, string username, string agemin, string agemax,
            string hasPhotos, string isOnline, string indexstart, string results);


        //        ------------------ v2 ----------------------------


        [OperationContract]
        UserProfilesList getSearchResultsV3(string userid, string sort, string zipstr, string lat, string lng, string distance,
            string gender, string searchname, string username, string agemin, string agemax,
            string hasPhotos, string isOnline, string indexstart, string results, string isVip, string countrycode);


        [OperationContract]
        UserProfilesList getNewUsersV3(string myprofileid, string offset, string results, string gender, string isOnline, string countrycode);

        [OperationContract]
        UserProfilesList getWhoViewedMeV2(string userid, string zip, string lat, string lng, string indexstart, string isOnline);
        [OperationContract]
        UserProfilesList getWhoViewedMe(string userid, string zip, string lat, string lng, string indexstart);

        [OperationContract]
        UserProfilesList getFavoriteListV2(string userid, string zip, string lat, string lng, string isOnline);


        [OperationContract]
        string deletePhoto(string userid, string photoId);

        [OperationContract]
        string getMinLevel(string myuserid);

        [OperationContract]
        string checkDifferentCountry(string userid1, string userid2);

        [OperationContract]
        string hasPhotos(string userid);
    }

    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    // You can add XSD files into the project. After building the project, you can directly use the data types defined there, with the namespace "GoomenaLib.ContractType".
    [DataContract]
    public class CompositeType
    {
        bool boolValue = true;
        string stringValue = "Hello ";

        [DataMember]
        public bool BoolValue {
            get { return boolValue; }
            set { boolValue = value; }
        }

        [DataMember]
        public string StringValue {
            get { return stringValue; }
            set { stringValue = value; }
        }
    }

    [DataContract]
    public class GCMUserSettings
    {
        [DataMember]
        public string profileId { get; set; }
        [DataMember]
        public string notifMessages { get; set; }
        [DataMember]
        public string notifLikes { get; set; }
        [DataMember]
        public string notifOffers { get; set; }
    }

    [DataContract]
    public class ProfileDeviceId
    {
        [DataMember]
        public string deviceId { get; set; }

        [DataMember]
        public UserProfile profile { get; set; }
    }

    [DataContract]
    public class NotificationParams : GoomenaLib.NotificationParams
    {
        //[DataMember]
        //public string message { get; set; }
        //[DataMember]
        //public List<string> deviceIds { get; set; }
    }

    [DataContract]
    public class StoreUserParams
    {
        [DataMember]
        public string regId { get; set; }
        [DataMember]
        public string profileId { get; set; }
        [DataMember]
        public string loginname { get; set; }
        [DataMember]
        public string email { get; set; }
    }

    [DataContract]
    public class LoginParameters
    {
        [DataMember]
        public string email { get; set; }
        [DataMember]
        public string password { get; set; }
        [DataMember]
        public string logindetails { get; set; }
    }

    [DataContract]
    public class RegisterData : GoomenaLib.RegisterData
    {
        //[DataMember]
        //public string email { get; set; }

        //[DataMember]
        //public string username { get; set; }

        //[DataMember]
        //public string password { get; set; }

        //[DataMember]
        //public string country { get; set; }

        //[DataMember]
        //public string region { get; set; }

        //[DataMember]
        //public string city { get; set; }

        //[DataMember]
        //public string birthday { get; set; }

        //[DataMember]
        //public string gender { get; set; }

        //[DataMember]
        //public string lag { get; set; }
    }

    [DataContract]
    public class UserProfilesList
    {
        List<UserProfile> profiles;

        [DataMember]
        public List<UserProfile> Profiles {
            get { return profiles; }
            set { profiles = value; }
        }
    }

    [DataContract]
    public class UserProfile : GoomenaLib.UserProfile
    {

        //[DataMember]
        //public string FirstName { get; set; }

        //[DataMember]
        //public string LastName { get; set; }

        //[DataMember]
        //public string ProfileId { get; set; }

        //[DataMember]
        //public string UserName { get; set; }

        //[DataMember]
        //public string UserAge { get; set; }

        //[DataMember]
        //public string UserCity { get; set; }

        //[DataMember]
        //public string UserRegion { get; set; }

        //[DataMember]
        //public string DateRegister { get; set; }

        //[DataMember]
        //public string GenderId { get; set; }

        //[DataMember]
        //public string Password { get; set; }


        //[DataMember]
        //public string ZipCode { get; set; }
        //[DataMember]
        //public string Latitude { get; set; }
        //[DataMember]
        //public string Longtitude { get; set; }

        //[DataMember]
        //public string ImageUrl { get; set; }

        //[DataMember]
        //public string ReferrerParentId { get; set; }

        //[DataMember]
        //public string IsVip { get; set; }

        //[DataMember]
        //public string UserCountry { get; set; }

    }

    [DataContract]
    public class ImagePath
    {
        private string path;

        [DataMember]
        public string Path {
            get { return path; }
            set { path = value; }
        }

        [DataMember]
        public String photoId { get; set; }

        [DataMember]
        public int photoType { get; set; }
    }

    [DataContract]
    public class ImagePathList
    {
        private List<ImagePath> pathList;

        [DataMember]
        public List<ImagePath> PathList {
            get { return pathList; }
            set { pathList = value; }
        }


    }

    [DataContract]
    public class UserDetails
    {
        [DataMember]
        public string birthdate { get; set; }

        [DataMember]
        public string age { get; set; }

        [DataMember]
        public string bodytype { get; set; }

        [DataMember]
        public string haireyes { get; set; }

        [DataMember]
        public string education { get; set; }

        [DataMember]
        public string childs { get; set; }

        [DataMember]
        public string nationality { get; set; }

        [DataMember]
        public string religion { get; set; }

        [DataMember]
        public string smokedring { get; set; }

        [DataMember]
        public string job { get; set; }

        [DataMember]
        public string income { get; set; }

        [DataMember]
        public string familystate { get; set; }

        [DataMember]
        public string lookfor { get; set; }

        [DataMember]
        public string interest { get; set; }

        [DataMember]
        public string profileupdated { get; set; }

        [DataMember]
        public string aboutme_heading { get; set; }
        [DataMember]
        public string aboutme_describeyourself { get; set; }
        [DataMember]
        public string aboutme_describedate { get; set; }
        [DataMember]
        public string available_credits { get; set; }
        [DataMember]
        public string zodiac { get; set; }

    }

    [DataContract]
    public class MessageInfo
    {
        [DataMember]
        public string ProfileId { get; set; }

        [DataMember]
        public string MsgId { get; set; }

        [DataMember]
        public string MsgCount { get; set; }

        [DataMember]
        public string UserName { get; set; }

        [DataMember]
        public string Age { get; set; }

        [DataMember]
        public string City { get; set; }
        [DataMember]
        public string Region { get; set; }
        [DataMember]
        public string Country { get; set; }

        [DataMember]
        public string ProfileImg { get; set; }

        [DataMember]
        public string DateTimeSent { get; set; }
    }

    [DataContract]
    public class MessageInfoList
    {
        [DataMember]
        public List<MessageInfo> msgList { get; set; }
    }

    [DataContract]
    public class Message
    {
        [DataMember]
        public string MessageId { get; set; }
        [DataMember]
        public string FromProfileId { get; set; }
        [DataMember]
        public string ToProfileId { get; set; }
        [DataMember]
        public string DateTimeSent { get; set; }
        [DataMember]
        public string Body { get; set; }
        [DataMember]
        public string Subject { get; set; }
        [DataMember]
        public bool IsSent { get; set; }
        [DataMember]
        public bool IsReceived { get; set; }
        [DataMember]
        public string statusId { get; set; }


    }

    [DataContract]
    public class MessageList
    {
        [DataMember]
        public List<Message> list { get; set; }

    }

    [DataContract]
    public class MessageParams
    {
        [DataMember]
        public string subject { get; set; }
        [DataMember]
        public string body { get; set; }
        [DataMember]
        public string fromprofileid { get; set; }
        [DataMember]
        public string toprofileid { get; set; }
        [DataMember]
        public string genderid { get; set; }
        [DataMember]
        public string offerId { get; set; }

    }

    [DataContract]
    public class UploadPhotoParams
    {
        [DataMember]
        public string strFileName { get; set; }

        [DataMember]
        public Stream streamFileContent { get; set; }

        [DataMember]
        public string profileid { get; set; }

        [DataMember]
        public string photoLevel { get; set; }

    }

    [DataContract]
    public class Offer
    {
        [DataMember]
        public UserProfile profile { get; set; }

        [DataMember]
        public string offerId { get; set; }

        [DataMember]
        public string rejectReason { get; set; }

    }

    [DataContract]
    public class OfferList
    {
        [DataMember]
        public List<Offer> offers { get; set; }
    }

    [DataContract]
    public class CountryList
    {
        [DataMember]
        public List<Country> countries { get; set; }
    }

    [DataContract]
    public class Country
    {
        [DataMember]
        public string name { get; set; }

        [DataMember]
        public string iso { get; set; }
    }

    [DataContract]
    public class Regions
    {
        [DataMember]
        public List<String> list { get; set; }
    }

    [DataContract]
    public class Cities
    {
        [DataMember]
        public List<String> list { get; set; }

    }

}

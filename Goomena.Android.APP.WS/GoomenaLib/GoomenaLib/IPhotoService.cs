﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace GoomenaLib
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IPhotoService" in both code and config file together.
    [ServiceContract]
    public interface IPhotoService
    {
        [OperationContract]
        void DoWork();

        [OperationContract]
        string SavePhoto(string strFileName, string profileid, string photoLevel, System.IO.Stream streamFileContent);

        //[OperationContract]
        //bool UploadStream(System.IO.Stream stream);

        //[OperationContract]
        //string SavePhoto(string strFileName, string profileid, string photoLevel);

        //[OperationContract]
        //string SavePhoto(Stream data);

        //[OperationContract]
        //void SavePhoto(FileUploadMessage request);

        //[OperationContract]
        //FileUploadMessageResult SavePhoto(FileUploadMessage request);

        //[OperationContract(Name = "Upload")]
        //[WebInvoke(Method = "POST", UriTemplate = "/Upload/{step}/{fileName}",            BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        //FileUploadMessageResult SavePhoto(string fileName, string step, Stream fileStream);
    }

    //[MessageContract]
    //public class FileUploadMessage
    //{
    //    [MessageHeader(MustUnderstand = true)]
    //    public string FileName;
    //    [MessageHeader(MustUnderstand = true)]
    //    public int ProfileID;
    //    [MessageHeader(MustUnderstand = true)]
    //    public int PhotoLevel;
    //    [MessageBodyMember(Order = 1)]
    //    public Stream FileByteStream;
    //}
    //[MessageContract]
    //public class FileUploadMessageResult
    //{
    //    [MessageBodyMember(Order = 1)]
    //    public string Message;
    //}
}

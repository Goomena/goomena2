﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoomenaLib
{
    [Serializable]
    public class ActionNotAllowedException : System.Exception
    {
        public ActionNotAllowedException()
            : base("Action not allowed for specified user.") {
        }

        public ActionNotAllowedException(string message)
            : base(message) {
        }

        public ActionNotAllowedException(string message, Exception inner)
            : base(message, inner) {
        }

        // A constructor is needed for serialization when an 
        // exception propagates from a remoting server to the client.  
        protected ActionNotAllowedException(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context) {

        }
    }
}

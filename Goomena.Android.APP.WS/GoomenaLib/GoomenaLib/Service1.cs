﻿using Dating.Server.Core.DLL;
using Dating.Server.Datasets.DLL;
using Dating.Server.Datasets.DLL.DSMembersTableAdapters;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Web;
using System.Text;
using System.Web;


namespace GoomenaLib
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    public class Service1 : ServiceBase, IService1
    {


        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "userprofile/{id}")]
        public UserProfile getUserProfile(string id) {
            using (myContext) {
                //4 for Approved
                EUS_Profile q = (from p in myContext.EUS_Profiles
                                 where p.ProfileID == Convert.ToInt32(id) && p.Status == (int)ProfileStatusEnum.Approved
                                 select p).FirstOrDefault();
                if (q != null) {
                    string fileName = "";
                    DSMembers.EUS_CustomerPhotosRow photo = DataHelpers.GetProfilesDefaultPhoto(Convert.ToInt32(q.ProfileID));
                    if (photo != null)
                        fileName = photo.FileName;
                    //string path = ProfileHelper.GetProfileImageURL(Convert.ToInt32(q.ProfileID), fileName, Convert.ToInt32(q.GenderId), true, false);
                    string path = getImagePath(Convert.ToInt32(q.ProfileID), fileName, Convert.ToInt32(q.GenderId), true, false);


                    UserProfile profile = new UserProfile();
                    profile.FirstName = q.FirstName;
                    profile.LastName = q.LastName;
                    profile.ProfileId = Convert.ToString(q.ProfileID);
                    profile.FirstName = q.FirstName;
                    profile.LastName = q.LastName;
                    profile.DateRegister = q.DateTimeToRegister.Value.ToShortDateString();
                    profile.GenderId = Convert.ToString(q.GenderId);
                    profile.UserName = q.LoginName;
                    profile.UserCity = q.City;
                    profile.UserRegion = q.Region;
                    profile.ZipCode = q.Zip;
                    profile.Latitude = Convert.ToString(q.latitude.Value);
                    profile.Longtitude = Convert.ToString(q.longitude);
                    profile.ImageUrl = path;
                    profile.UserCountry = q.Country;
                    //profile.IsVip = checkProfileVip(q.ProfileID) == true?"1":"0";


                    if (q.ReferrerParentId != null) {
                        profile.ReferrerParentId = Convert.ToString(q.ReferrerParentId.Value);
                    }
                    else {
                        profile.ReferrerParentId = "";
                    }

                    myContext.Dispose();

                    return profile;
                }
                else {
                    return null;
                }

            }
        }


        public UserProfile createProfile(EUS_Profile q, string path) {

            UserProfile profile = new UserProfile();
            profile.FirstName = q.FirstName;
            profile.LastName = q.LastName;
            profile.ProfileId = Convert.ToString(q.ProfileID);
            profile.FirstName = q.FirstName;
            profile.LastName = q.LastName;
            profile.DateRegister = q.DateTimeToRegister.Value.ToShortDateString();
            profile.GenderId = Convert.ToString(q.GenderId);
            profile.UserName = q.LoginName;
            profile.UserCity = q.City;
            profile.UserRegion = q.Region;
            profile.ZipCode = q.Zip;
            profile.Latitude = Convert.ToString(q.latitude.Value);
            profile.Longtitude = Convert.ToString(q.longitude);
            profile.ImageUrl = path;
            if (q.ReferrerParentId != null) {
                profile.ReferrerParentId = Convert.ToString(q.ReferrerParentId.Value);
            }
            else {
                profile.ReferrerParentId = "";
            }

            return profile;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "userprofiledetails/{id}/{langId}")]
        public UserDetails getUserProfileDetails(string id, string langId) {

            using (myContext) {

                var q = (from p in myContext.EUS_Profiles
                         where p.ProfileID == Convert.ToInt32(id)
                         select new
                         {
                             birth = p.Birthday,
                             bodytype = p.PersonalInfo_BodyTypeID,
                             hair = p.PersonalInfo_HairColorID,
                             eyes = p.PersonalInfo_EyeColorID,
                             edu = p.OtherDetails_EducationID,
                             child = p.PersonalInfo_ChildrenID,
                             nation = p.PersonalInfo_EthnicityID,
                             religion = p.PersonalInfo_ReligionID,
                             smoke = p.PersonalInfo_SmokingHabitID,
                             drink = p.PersonalInfo_DrinkingHabitID,
                             job = p.OtherDetails_Occupation,
                             income = p.OtherDetails_AnnualIncomeID,
                             family = p.LookingFor_RelationshipStatusID,
                             lookfor = 3 - p.GenderId,

                             dating_shorterm = p.LookingFor_TypeOfDating_ShortTermRelationship,
                             dating_married = p.LookingFor_TypeOfDating_MarriedDating,
                             dating_benefits = p.LookingFor_TypeOfDating_MutuallyBeneficialArrangements,
                             dating_longterm = p.LookingFor_TypeOfDating_LongTermRelationship,
                             dating_friensdhip = p.LookingFor_TypeOfDating_Friendship,
                             dating_adult = p.LookingFor_TypeOfDating_AdultDating_Casual,

                             aboutme_head = p.AboutMe_Heading,
                             aboutme_descr = p.AboutMe_DescribeYourself,
                             aboutme_date = p.AboutMe_DescribeAnIdealFirstDate,
                             availbale = Convert.ToString(getAvailableCredits(id)),
                             updated = p.LastUpdateProfileDateTime.Value.ToShortDateString()

                         }).FirstOrDefault();

                string datingInfo = "</br>";
                for (int i = 0; i < Lists.gDSLists.EUS_LISTS_TypeOfDating.Rows.Count - 1; i++) {
                    switch ((TypeOfDatingEnum)Lists.gDSLists.EUS_LISTS_TypeOfDating.Rows[i]["TypeOfDatingId"]) {

                        case TypeOfDatingEnum.AdultDating_Casual:
                            if ((bool)q.dating_adult) {
                                datingInfo += (string)Lists.gDSLists.EUS_LISTS_TypeOfDating.Rows[i][langId] + "</br>";
                            }
                            break;

                        case TypeOfDatingEnum.Friendship:
                            if ((bool)q.dating_friensdhip) {
                                datingInfo += (string)Lists.gDSLists.EUS_LISTS_TypeOfDating.Rows[i][langId] + "</br>";

                            }
                            break;

                        case TypeOfDatingEnum.LongTermRelationship:
                            if ((bool)q.dating_longterm) {
                                datingInfo += (string)Lists.gDSLists.EUS_LISTS_TypeOfDating.Rows[i][langId] + "</br>";
                            }
                            break;

                        case TypeOfDatingEnum.MarriedDating:
                            if ((bool)q.dating_married) {
                                datingInfo += (string)Lists.gDSLists.EUS_LISTS_TypeOfDating.Rows[i][langId] + "</br>";
                            }
                            break;

                        case TypeOfDatingEnum.MutuallyBeneficialArrangements:
                            if ((bool)q.dating_benefits) {
                                datingInfo += (string)Lists.gDSLists.EUS_LISTS_TypeOfDating.Rows[i][langId] + "</br>";
                            }
                            break;

                        case TypeOfDatingEnum.ShortTermRelationship:
                            if ((bool)q.dating_shorterm) {
                                datingInfo += (string)Lists.gDSLists.EUS_LISTS_TypeOfDating.Rows[i][langId] + "</br>";

                            }
                            break;

                    }

                }

                UserDetails details = new UserDetails();
                details.birthdate = q.birth.Value.ToShortDateString();
                details.age = Convert.ToString(ProfileHelper.GetCurrentAge(q.birth.Value));
                details.bodytype = ProfileHelper.GetBodyTypeString(q.bodytype, langId);
                details.haireyes = ProfileHelper.GetHairColorString(q.hair, langId) + "/" + ProfileHelper.GetEyeColorString(q.eyes, langId);
                details.education = ProfileHelper.GetEducationString(q.edu, langId);
                details.childs = ProfileHelper.GetChildrenNumberString(q.edu, langId);
                details.nationality = ProfileHelper.GetEthnicityString(q.nation, langId);
                details.religion = ProfileHelper.GetReligionString(q.religion, langId);
                details.smokedring = ProfileHelper.GetSmokingString(q.smoke, langId) + "/" + ProfileHelper.GetDrinkingString(q.drink, langId);
                details.job = q.job;
                details.income = ProfileHelper.GetIncomeString(q.income, langId);
                details.familystate = ProfileHelper.GetRelationshipStatusString(q.family, langId);
                details.lookfor = ProfileHelper.GetGenderString(q.lookfor, langId);
                details.interest = datingInfo;
                details.aboutme_describedate = q.aboutme_date;
                details.aboutme_describeyourself = q.aboutme_descr;
                details.aboutme_heading = q.aboutme_head;
                details.available_credits = Convert.ToString(q.availbale);
                details.profileupdated = q.updated;
                details.zodiac = ProfileHelper.ZodiacName(q.birth.Value);


                myContext.Dispose();
                return details;
            }
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "newusers_v2/{myprofileid}/{offset}/{results}/{gender}/{isOnline}")]
        public UserProfilesList getNewUsersV2(string myprofileid, string offset, string results, string gender, string isOnline) {

            using (myContext) {
                DataHelpers.UpdateEUS_Profiles_Activity(Convert.ToInt32(myprofileid), getClientIP(), true);

                List<UserProfile> profilesResult = new List<UserProfile>();

                if (isOnline == "0") {

                    var profiles = (from p in myContext.EUS_Profiles
                                    where p.IsMaster == true && p.GenderId == (3 - Convert.ToInt32(gender))
                                    && p.Status == (int)ProfileStatusEnum.Approved && p.Status != (int)ProfileStatusEnum.Deleted
                                    orderby
                                    (p.DefPhotoID.Value > 0 ? 1 : 0) descending, p.DateTimeToRegister descending
                                    select p).ToList();

                    int offsetInt = Convert.ToInt32(offset);
                    int resultInt = Convert.ToInt32(results);

                    var newProfiles = profiles.Skip(offsetInt).Take(resultInt);


                    foreach (EUS_Profile p in newProfiles) {
                        string fileName = "";
                        DSMembers.EUS_CustomerPhotosRow photo = DataHelpers.GetProfilesDefaultPhoto(Convert.ToInt32(p.ProfileID));
                        if (photo != null)
                            fileName = photo.FileName;
                        //string path = ProfileHelper.GetProfileImageURL(Convert.ToInt32(p.ProfileID), fileName, Convert.ToInt32(p.GenderId), true, false);
                        string path = getImagePath(Convert.ToInt32(p.ProfileID), fileName, Convert.ToInt32(p.GenderId), true, false);


                        UserProfile prof = new UserProfile()
                        {
                            FirstName = p.FirstName,
                            LastName = p.LastName,
                            UserRegion = p.Region,
                            UserCity = p.City,
                            UserAge = Convert.ToString(ProfileHelper.GetCurrentAge(p.Birthday.Value)),
                            ProfileId = Convert.ToString(p.ProfileID),
                            UserName = p.LoginName,
                            DateRegister = p.DateTimeToRegister.Value.ToShortDateString(),
                            GenderId = Convert.ToString(p.GenderId),
                            ImageUrl = path,
                            Latitude = Convert.ToString(p.latitude.Value),
                            Longtitude = Convert.ToString(p.longitude.Value),
                        };
                        profilesResult.Add(prof);

                    }

                }
                else if (isOnline == "1") {

                    var profiles = (from p in myContext.EUS_Profiles
                                    where p.IsMaster == true && p.GenderId == (3 - Convert.ToInt32(gender))
                                    && p.Status == (int)ProfileStatusEnum.Approved && p.Status != (int)ProfileStatusEnum.Deleted
                                    && p.IsOnline.HasValue && p.IsOnline.Value
                                    orderby (p.DefPhotoID.Value > 0 ? 1 : 0) descending, p.DateTimeToRegister descending
                                    select p).ToList();

                    int offsetInt = Convert.ToInt32(offset);
                    int resultInt = Convert.ToInt32(results);

                    var newProfiles = profiles.Skip(offsetInt).Take(resultInt);

                    foreach (EUS_Profile p in newProfiles) {
                        string fileName = "";
                        DSMembers.EUS_CustomerPhotosRow photo = DataHelpers.GetProfilesDefaultPhoto(Convert.ToInt32(p.ProfileID));
                        if (photo != null)
                            fileName = photo.FileName;
                        //string path = ProfileHelper.GetProfileImageURL(Convert.ToInt32(p.ProfileID), fileName, Convert.ToInt32(p.GenderId), true, false);
                        string path = getImagePath(Convert.ToInt32(p.ProfileID), fileName, Convert.ToInt32(p.GenderId), true, false);

                        UserProfile prof = new UserProfile()
                        {
                            FirstName = p.FirstName,
                            LastName = p.LastName,
                            UserRegion = p.Region,
                            UserCity = p.City,
                            UserAge = Convert.ToString(ProfileHelper.GetCurrentAge(p.Birthday.Value)),
                            ProfileId = Convert.ToString(p.ProfileID),
                            UserName = p.LoginName,
                            DateRegister = p.DateTimeToRegister.Value.ToShortDateString(),
                            GenderId = Convert.ToString(p.GenderId),
                            ImageUrl = path,
                            Latitude = Convert.ToString(p.latitude.Value),
                            Longtitude = Convert.ToString(p.longitude.Value),
                        };
                        profilesResult.Add(prof);

                    }


                }

                UserProfilesList list = new UserProfilesList() { Profiles = profilesResult };

                myContext.Dispose();
                return list;
            }
        }



        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "newusers/{myprofileid}/{offset}/{results}/{gender}")]
        public UserProfilesList getNewUsers(string myprofileid, string offset, string results, string gender) {

            using (myContext) {
                DataHelpers.UpdateEUS_Profiles_Activity(Convert.ToInt32(myprofileid), getClientIP(), true);

                var profiles = (from p in myContext.EUS_Profiles
                                where p.IsMaster == true && p.GenderId == (3 - Convert.ToInt32(gender))
                                && p.Status == (int)ProfileStatusEnum.Approved && p.Status != (int)ProfileStatusEnum.Deleted
                                orderby (p.DefPhotoID.Value > 0 ? 1 : 0) descending, p.DateTimeToRegister descending
                                select p).ToList();

                int offsetInt = Convert.ToInt32(offset);
                int resultInt = Convert.ToInt32(results);

                var newProfiles = profiles.Skip(offsetInt).Take(resultInt);

                List<UserProfile> profilesResult = new List<UserProfile>();
                foreach (EUS_Profile p in newProfiles) {
                    string fileName = "";
                    DSMembers.EUS_CustomerPhotosRow photo = DataHelpers.GetProfilesDefaultPhoto(Convert.ToInt32(p.ProfileID));
                    if (photo != null)
                        fileName = photo.FileName;
                    //string path = ProfileHelper.GetProfileImageURL(Convert.ToInt32(p.ProfileID), fileName, Convert.ToInt32(p.GenderId), true, false);
                    string path = getImagePath(Convert.ToInt32(p.ProfileID), fileName, Convert.ToInt32(p.GenderId), true, false);

                    UserProfile prof = new UserProfile()
                    {
                        FirstName = p.FirstName,
                        LastName = p.LastName,
                        UserRegion = p.Region,
                        UserCity = p.City,
                        UserAge = Convert.ToString(ProfileHelper.GetCurrentAge(p.Birthday.Value)),
                        ProfileId = Convert.ToString(p.ProfileID),
                        UserName = p.LoginName,
                        DateRegister = p.DateTimeToRegister.Value.ToShortDateString(),
                        GenderId = Convert.ToString(p.GenderId),
                        ImageUrl = path,
                        Latitude = Convert.ToString(p.latitude.Value),
                        Longtitude = Convert.ToString(p.longitude.Value),
                    };
                    profilesResult.Add(prof);
                }

                UserProfilesList list = new UserProfilesList() { Profiles = profilesResult };

                myContext.Dispose();
                return list;
            }
        }



        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "profileimage/{profileId}/{genderId}")]
        public ImagePath getProfileImage(string profileId, string genderId) {
            string fileName = "";
            DSMembers.EUS_CustomerPhotosRow photo = DataHelpers.GetProfilesDefaultPhoto(Convert.ToInt32(profileId));
            if (photo != null)
                fileName = photo.FileName;
            //string path = ProfileHelper.GetProfileImageURL(Convert.ToInt32(profileId), fileName, Convert.ToInt32(genderId), true, false);
            string path = getImagePath(Convert.ToInt32(profileId), fileName, Convert.ToInt32(genderId), true, false);
            ImagePath imPath = new ImagePath() { Path = path };
            return imPath;
        }


        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "login_v2")]
        public UserProfile login_v2(LoginParameters lp) {
            using (myContext) {

                var q = (from p in myContext.EUS_Profiles
                         where p.Password == lp.password && (p.eMail == lp.email || p.LoginName == lp.email) && p.IsMaster == true
                         && p.Status != (int)ProfileStatusEnum.Deleted && p.Status != (int)ProfileStatusEnum.DeletedByUser
                         select p).FirstOrDefault();

                //get client IP address
                OperationContext context = OperationContext.Current;
                MessageProperties messageProperties = context.IncomingMessageProperties;
                RemoteEndpointMessageProperty endpointProperty = (RemoteEndpointMessageProperty)messageProperties[RemoteEndpointMessageProperty.Name];

                UserProfile pInfo = new UserProfile();
                if (q != null) {

                    //login
                    DataHelpers.UpdateEUS_Profiles_LoginData(q.ProfileID, endpointProperty.Address, getCountryCode(endpointProperty.Address), true, true);


                    //get profile photo
                    string fileName = "";
                    DSMembers.EUS_CustomerPhotosRow photo = DataHelpers.GetProfilesDefaultPhoto(Convert.ToInt32(q.ProfileID));
                    if (photo != null)
                        fileName = photo.FileName;
                    //string path = ProfileHelper.GetProfileImageURL(Convert.ToInt32(q.ProfileID), fileName, Convert.ToInt32(q.GenderId), true, false);
                    string path = getImagePath(Convert.ToInt32(q.ProfileID), fileName, Convert.ToInt32(q.GenderId), true, false);

                    //update last activity
                    DataHelpers.UpdateEUS_Profiles_Activity(Convert.ToInt32(q.ProfileID), getClientIP(), true);

                    //get profile details
                    pInfo.FirstName = q.FirstName;
                    pInfo.LastName = q.LastName;
                    pInfo.ProfileId = Convert.ToString(q.ProfileID);
                    pInfo.UserName = q.LoginName;
                    //pInfo.Password = q.Password;
                    pInfo.ZipCode = q.Zip;
                    pInfo.Latitude = Convert.ToString(q.latitude.Value);
                    pInfo.Longtitude = Convert.ToString(q.longitude.Value);
                    pInfo.GenderId = Convert.ToString(q.GenderId);
                    pInfo.DateRegister = q.DateTimeToRegister.Value.ToShortDateString();
                    pInfo.UserAge = Convert.ToString(ProfileHelper.GetCurrentAge(q.Birthday.Value));
                    pInfo.UserCity = q.City;
                    pInfo.UserRegion = q.Region;
                    pInfo.ImageUrl = path;
                    pInfo.UserCountry = q.Country;

                    if (q.ReferrerParentId != null) {
                        pInfo.ReferrerParentId = Convert.ToString(q.ReferrerParentId.Value);
                    }
                    else {
                        pInfo.ReferrerParentId = "";
                    }

                    if (pInfo.ProfileId != null && pInfo.UserName != null) {

                        DataHelpers.LogProfileAccess(Convert.ToInt32(pInfo.ProfileId), pInfo.UserName, lp.password, true, "Login", lp.logindetails, "", endpointProperty.Address, true, "Service1-login_v2");
                    }

                }
                else {
                    DataHelpers.LogProfileAccess(0, lp.email, lp.password, false, "Login", lp.logindetails, "", endpointProperty.Address, true, "Service1-login_v2");

                }

                myContext.Dispose();
                return pInfo;
            }
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "login/{email}/{password}")]
        public UserProfile login(string email, string password) {
            using (myContext) {

                var q = (from p in myContext.EUS_Profiles
                         where p.Password == password && (p.eMail == email || p.LoginName == email) && p.IsMaster == true
                         select p).FirstOrDefault();

                //get client IP address
                OperationContext context = OperationContext.Current;
                MessageProperties messageProperties = context.IncomingMessageProperties;
                RemoteEndpointMessageProperty endpointProperty = (RemoteEndpointMessageProperty)messageProperties[RemoteEndpointMessageProperty.Name];

                UserProfile pInfo = new UserProfile();
                if (q != null) {


                    //login
                    DataHelpers.UpdateEUS_Profiles_LoginData(q.ProfileID, endpointProperty.Address, getCountryCode(endpointProperty.Address), true, true);


                    //get profile photo
                    string fileName = "";
                    DSMembers.EUS_CustomerPhotosRow photo = DataHelpers.GetProfilesDefaultPhoto(Convert.ToInt32(q.ProfileID));
                    if (photo != null)
                        fileName = photo.FileName;
                    string path = ProfileHelper.GetProfileImageURL(Convert.ToInt32(q.ProfileID), fileName, Convert.ToInt32(q.GenderId), true, false);

                    //update last activity
                    DataHelpers.UpdateEUS_Profiles_Activity(Convert.ToInt32(q.ProfileID), getClientIP(), true);

                    //get profile details
                    pInfo.FirstName = q.FirstName;
                    pInfo.LastName = q.LastName;
                    pInfo.ProfileId = Convert.ToString(q.ProfileID);
                    pInfo.UserName = q.LoginName;
                    //pInfo.Password = q.Password;
                    pInfo.ZipCode = q.Zip;
                    pInfo.Latitude = Convert.ToString(q.latitude.Value);
                    pInfo.Longtitude = Convert.ToString(q.longitude.Value);
                    pInfo.GenderId = Convert.ToString(q.GenderId);
                    pInfo.DateRegister = q.DateTimeToRegister.Value.ToShortDateString();
                    pInfo.UserAge = Convert.ToString(ProfileHelper.GetCurrentAge(q.Birthday.Value));
                    pInfo.UserCity = q.City;
                    pInfo.UserRegion = q.Region;
                    pInfo.ImageUrl = path;
                    pInfo.UserCountry = q.Country;
                    if (q.ReferrerParentId != null) {
                        pInfo.ReferrerParentId = Convert.ToString(q.ReferrerParentId.Value);
                    }
                    else {
                        pInfo.ReferrerParentId = "";
                    }

                    if (pInfo.ProfileId != null && pInfo.UserName != null) {

                        DataHelpers.LogProfileAccess(Convert.ToInt32(pInfo.ProfileId), pInfo.UserName, password, true, "Login", "Android, app version 1.0", "", endpointProperty.Address, true, "Service1-login");
                    }

                }
                else {
                    DataHelpers.LogProfileAccess(0, email, password, false, "Login", "Android, app version 1.0", "", endpointProperty.Address, true, "Service1-login");

                }

                myContext.Dispose();
                return pInfo;
            }
        }


        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "getuserstate/{userid}")]
        public override string getUserState(string userid) {
            return base.getUserState(userid);
        }
        //public string getUserState(string userid) {
        //    using (myContext) {
        //        string result = "";
        //        var currentProfile = (from p in myContext.EUS_Profiles
        //                              where p.ProfileID == Convert.ToInt32(userid) && p.IsOnline == true
        //                              select new
        //                              {
        //                                  lastActivity = p.LastActivityDateTime,
        //                                  lastLogin = p.LastLoginDateTime
        //                              }).FirstOrDefault();
        //        if (currentProfile != null) {

        //            int mins = 20;
        //            clsConfigValues config = new clsConfigValues();

        //            Int32.TryParse(config.members_online_minutes, out mins);
        //            DateTime before20mins = DateTime.UtcNow.AddMinutes(-mins);

        //            if (currentProfile.lastActivity >= before20mins) {
        //                result = "Online";
        //            }
        //            else {
        //                result = "Offline";
        //            }
        //        }
        //        else {
        //            result = "Offline";
        //        }

        //        myContext.Dispose();
        //        return result;
        //    }
        //}

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "getMinLevel/{myuserid}")]
        public string getMinLevel(string myuserid) {
            using (myContext) {

                Dating.Server.Datasets.DLL.DSMembers.EUS_CustomerPhotosDataTable dtPhotos = DataHelpers.GetEUS_CustomerPhotos_ByProfileOrMirrorID(Convert.ToInt32(myuserid)).EUS_CustomerPhotos;

                List<int> levels = new List<int>();

                for (int i = 0; i < dtPhotos.Rows.Count; i++) {
                    DataRow row = dtPhotos.Rows[i];
                    levels.Add(Convert.ToInt32(row["DisplayLevel"]));
                }

                levels = levels.Distinct().ToList();
                levels.Sort();

                if (levels.Count > 1) {
                    return Convert.ToString(levels[1]);
                }
                else {
                    return "0";
                }
            }

        }



        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "getuserphotos/{myuserid}/{userid}/{genderid}")]
        public ImagePathList getUserPhotos(string myuserid, string userid, string genderid) {
            using (myContext) {

                int userId = Convert.ToInt32(userid);
                int genderId = Convert.ToInt32(genderid);
                int myuserId = Convert.ToInt32(myuserid);


                ImagePathList imgPathList = new ImagePathList();
                imgPathList.PathList = new List<ImagePath>();

                if (userId != myuserId) {

                    EUS_ProfilePhotosLevel phtLvl = (from itm in myContext.EUS_ProfilePhotosLevels
                                                     where itm.FromProfileID == userId && itm.ToProfileID == myuserId
                                                     select itm).FirstOrDefault();


                    int allowDisplayLevel = 0;
                    if (phtLvl != null) {
                        allowDisplayLevel = phtLvl.PhotoLevelID.Value;
                    }

                    Dating.Server.Datasets.DLL.DSMembers.EUS_CustomerPhotosDataTable dtPhotos = DataHelpers.GetEUS_CustomerPhotos_ByProfileOrMirrorID(userId).EUS_CustomerPhotos;


                    foreach (DSMembers.EUS_CustomerPhotosRow row in dtPhotos) {
                        if (allowDisplayLevel >= row.DisplayLevel) {

                            bool isDeclined = (!row.IsHasDeclinedNull() && row.HasDeclined);
                            bool isDeleted = (!row.IsIsDeletedNull() && row.IsDeleted);

                            if (row.HasAproved && !isDeclined && !isDeleted) {
                                //string path = ProfileHelper.GetProfileImageURL(userId, (string)row["FileName"], genderId, false, false);
                                string path = getImagePath(userId, (string)row["FileName"], genderId, false, false);

                                ImagePath p = new ImagePath()
                                {
                                    Path = path,
                                    photoId = Convert.ToString(row["CustomerPhotosId"])
                                };
                                imgPathList.PathList.Add(p);
                            }
                        }
                    }
                }
                else {
                    Dating.Server.Datasets.DLL.DSMembers.EUS_CustomerPhotosDataTable dtPhotos = DataHelpers.GetEUS_CustomerPhotos_ByProfileOrMirrorID(userId).EUS_CustomerPhotos;
                    foreach (DSMembers.EUS_CustomerPhotosRow row in dtPhotos) {

                        bool isDeclined = (!row.IsHasDeclinedNull() && row.HasDeclined);
                        bool isDeleted = (!row.IsIsDeletedNull() && row.IsDeleted);

                        if (!isDeleted) {

                            //string path = ProfileHelper.GetProfileImageURL(userId, (string)row["FileName"], genderId, true, false);
                            string path = getImagePathMyProfile(userId, (string)row["FileName"], genderId, false);
                            ImagePath p = new ImagePath()
                            {
                                Path = path,
                                photoId = Convert.ToString(row["CustomerPhotosId"]),
                                photoType = isDeclined ? 2 : 1 //2 for declined - 1 for approved
                            };
                            imgPathList.PathList.Add(p);
                        }

                        /*
                        if (row.HasAproved && !isDeclined && !isDeleted)
                        {

                            //string path = ProfileHelper.GetProfileImageURL(userId, (string)row["FileName"], genderId, false, false);
                            string path = getImagePath(userId, (string)row["FileName"], genderId, false, false);
                            ImagePath p = new ImagePath()
                            {
                                Path = path,
                                photoId = Convert.ToString(row["CustomerPhotosId"])
                            };
                            imgPathList.PathList.Add(p);
                        }
                         * */

                    }
                }

                myContext.Dispose();

                return imgPathList;

            }
        }



        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "getuserphotosThumb/{myuserid}/{userid}/{genderid}")]
        public ImagePathList getUserThumbPhotos(string myuserid, string userid, string genderid) {
            using (myContext) {

                int userId = Convert.ToInt32(userid);
                int genderId = Convert.ToInt32(genderid);
                int myuserId = Convert.ToInt32(myuserid);


                ImagePathList imgPathList = new ImagePathList();
                imgPathList.PathList = new List<ImagePath>();

                if (userId != myuserId) {

                    EUS_ProfilePhotosLevel phtLvl = (from itm in myContext.EUS_ProfilePhotosLevels
                                                     where itm.FromProfileID == userId && itm.ToProfileID == myuserId
                                                     select itm).FirstOrDefault();


                    int allowDisplayLevel = 0;
                    if (phtLvl != null) {
                        allowDisplayLevel = phtLvl.PhotoLevelID.Value;
                    }

                    Dating.Server.Datasets.DLL.DSMembers.EUS_CustomerPhotosDataTable dtPhotos = DataHelpers.GetEUS_CustomerPhotos_ByProfileOrMirrorID(userId).EUS_CustomerPhotos;


                    foreach (DSMembers.EUS_CustomerPhotosRow row in dtPhotos) {
                        if (allowDisplayLevel >= row.DisplayLevel) {

                            bool isDeclined = (!row.IsHasDeclinedNull() && row.HasDeclined);
                            bool isDeleted = (!row.IsIsDeletedNull() && row.IsDeleted);

                            if (row.HasAproved && !isDeclined && !isDeleted) {

                                //string path = ProfileHelper.GetProfileImageURL(userId, (string)row["FileName"], genderId,true, false);
                                string path = getImagePath(userId, (string)row["FileName"], genderId, true, false);

                                ImagePath p = new ImagePath()
                                {
                                    Path = path,
                                    photoId = Convert.ToString(row["CustomerPhotosId"]),
                                    photoType = 1 //Approved
                                };
                                imgPathList.PathList.Add(p);
                            }
                        }
                    }
                }
                else {
                    Dating.Server.Datasets.DLL.DSMembers.EUS_CustomerPhotosDataTable dtPhotos = DataHelpers.GetEUS_CustomerPhotos_ByProfileOrMirrorID(userId).EUS_CustomerPhotos;
                    foreach (DSMembers.EUS_CustomerPhotosRow row in dtPhotos) {

                        bool isDeclined = (!row.IsHasDeclinedNull() && row.HasDeclined);
                        bool isDeleted = (!row.IsIsDeletedNull() && row.IsDeleted);

                        if (!isDeleted) {

                            //string path = ProfileHelper.GetProfileImageURL(userId, (string)row["FileName"], genderId, true, false);
                            string path = getImagePathMyProfile(userId, (string)row["FileName"], genderId, false);
                            ImagePath p = new ImagePath()
                            {
                                Path = path,
                                photoId = Convert.ToString(row["CustomerPhotosId"]),
                                photoType = isDeclined ? 2 : 1 //2 for declined - 1 for approved
                            };
                            imgPathList.PathList.Add(p);
                        }

                    }
                }

                myContext.Dispose();

                return imgPathList;

            }
        }


        //this version filters profiles with VIP parameter
        /*
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "getsearchresults_v2/{userid}/{sort}/{zipstr}/{lat}/{lng}/{distance}/{gender}/{searchname}/{username}/{agemin}/{agemax}/{hasPhotos}/{isOnline}/{indexstart}/{results}/{isVip}")]
        public UserProfilesList getSearchResultsV2(string userid, string sort, string zipstr, string lat, string lng, string distance,
            string gender, string searchname, string username, string agemin, string agemax,
            string hasPhotos, string isOnline, string indexstart, string results,string isVip)
        {
            int sortInt = Convert.ToInt32(sort);
            SearchSortEnum sortEnum = SearchSortEnum.NewestMember;

            string sql = "";
            bool sqlSet = false;
            UserProfilesList pList = new UserProfilesList();
            pList.Profiles = new List<UserProfile>();

            try
            {
                DataHelpers.UpdateEUS_Profiles_Activity(Convert.ToInt32(userid));


                if (Convert.ToInt32(searchname) == 1)
                {

                    ClsSQLInjectionClear.ClearString(ref username, 100);
                    if (username.Length > 0)
                    {
                        sql = sql + "\r\n" + " and  EUS_Profiles.LoginName like '" + username + "%' ";

                    }
                }
                else
                {
                    if (Convert.ToInt32(agemin) != -1 && Convert.ToInt32(agemax) != -1)
                    {
                        DateTime fromDate = DateTime.Now.AddYears((Convert.ToInt32(agemax) * -1) - 1).Date;
                        DateTime toDate = DateTime.Now.AddYears((Convert.ToInt32(agemin) * -1)).Date;

                        sql = sql + "\r\n" + " and  EUS_Profiles.Birthday between '" + fromDate.ToString("yyyy-MM-dd hh:mm:ss.mmm") + "' and '" + toDate.ToString("yyyy-MM-dd hh:mm:ss.mmm") + "'  ";
                        sqlSet = true;
                    }
                    else if (Convert.ToInt32(agemin) != -1)
                    {
                        DateTime toDate = DateTime.Now.AddYears((Convert.ToInt32(agemin) * -1)).Date;
                        sql = sql + "\r\n" + " and EUS_Profiles.Birthday < '" + toDate.ToString("yyyy-MM-dd hh:mm:ss.mmm") + "' ";
                        sqlSet = true;
                    }
                    else if (Convert.ToInt32(agemax) != -1)
                    {
                        DateTime fromDate = DateTime.Now.AddYears((Convert.ToInt32(agemax) * -1) - 1).Date;
                        sql = sql + "\r\n" + " and EUS_Profiles.Birthday between '" + fromDate.ToString("yyyy-MM-dd hh:mm:ss.mmm") + "' and getdate() ";
                        sqlSet = true;
                    }

                    if (hasPhotos == "1")
                    {
                        sql = sql + " and exists (select customerId from EUS_CustomerPhotos phot1 where phot1.customerId = EUS_Profiles.ProfileId and phot1.HasAproved = 1 and ISNULL(phot1.IsDeleted,0) = 0)";
                        sqlSet = true;
                    }


                    if (isOnline == "1")
                    {
                        int mins = clsConfigValues.Get__members_online_minutes();
                        DateTime lastActivityUTCDate = DateTime.UtcNow.AddMinutes(-mins);

                        sql = sql + " and (EUS_Profiles.IsOnline=1 and EUS_Profiles.LastActivityDateTime>= '" + lastActivityUTCDate.ToString("yyyy-MM-dd HH:mm") + "')";
                        sqlSet = true;
                    }

                    switch (sortInt)
                    {
                        case 0:
                            sortEnum = SearchSortEnum.None;
                            break;
                        case 1:
                            sortEnum = SearchSortEnum.NewestMember;
                            break;

                        case 2:
                            sortEnum = SearchSortEnum.OldestMember;
                            break;

                        case 3:
                            sortEnum = SearchSortEnum.RecentlyLoggedIn;
                            break;

                        case 4:
                            sortEnum = SearchSortEnum.NearestDistance;
                            break;

                    }
                }

                clsSearchHelperParameters searchParams = new clsSearchHelperParameters();
                searchParams.CurrentProfileId = Convert.ToInt32(userid);
                searchParams.ReturnRecordsWithStatus = (int)ProfileStatusEnum.Approved;
                searchParams.SearchSort = sortEnum;
                searchParams.zipstr = zipstr;
                searchParams.latitudeIn = Convert.ToDouble(lat);
                searchParams.longitudeIn = Convert.ToDouble(lng);

                if (Convert.ToInt32(distance) > 0)
                {
                    searchParams.Distance = Convert.ToInt32(distance);
                }

                if (Convert.ToInt32(gender) == 1)
                {
                    searchParams.LookingFor_ToMeetFemaleID = true;
                    searchParams.LookingFor_ToMeetMaleID = false;
                }
                else if (Convert.ToInt32(gender) == 2)
                {
                    searchParams.LookingFor_ToMeetFemaleID = false;
                    searchParams.LookingFor_ToMeetMaleID = true;
                }

                searchParams.NumberOfRecordsToReturn = Convert.ToInt32(indexstart) + Convert.ToInt32(results);
                searchParams.AdditionalWhereClause = sql;
                searchParams.performCount = false;

                DataSet set = clsSearchHelper.GetMembersToSearchDataTable(searchParams);
                DataTable dataTable = set.Tables[0];

                int start = Convert.ToInt32(indexstart);
                int result = Convert.ToInt32(results);
                int rowsmax;
                if (start + result < dataTable.Rows.Count)
                {
                    rowsmax = start + result;
                }
                else
                {
                    rowsmax = dataTable.Rows.Count;
                }

                for (int i = start; i < rowsmax; i++)
                {
                    if (i >= dataTable.Rows.Count)
                    {
                        break;
                    }

                    DataRow r = dataTable.Rows[i];

                    string fileName = "";
                    DSMembers.EUS_CustomerPhotosRow photo = DataHelpers.GetProfilesDefaultPhoto(Convert.ToInt32(r["ProfileID"]));
                    if (photo != null)
                        fileName = photo.FileName;
                    string path = ProfileHelper.GetProfileImageURL(Convert.ToInt32(r["ProfileID"]), fileName, Convert.ToInt32(r["GenderId"]), true, false);

                    

                    if (Convert.ToInt32(gender) == 2)
                    {
                        bool isvip = checkProfileVip(Convert.ToInt32(r["ProfileID"]) );

                        if (Convert.ToInt32(isVip) == 1 && isvip)
                        {

                            UserProfile p = new UserProfile()
                            {
                                UserAge = Convert.ToString(r["Age"]),
                                FirstName = (string)r["FirstName"],
                                LastName = (string)r["LastName"],
                                UserName = (string)r["LoginName"],
                                UserRegion = (string)r["Region"],
                                UserCity = (string)r["City"],
                                ProfileId = Convert.ToString(r["ProfileID"]),
                                GenderId = Convert.ToString(r["GenderId"]),
                                ImageUrl = path,
                                IsVip = isvip == true ? "1" : "0"
                            };

                            pList.Profiles.Add(p);

                        }
                        else if (Convert.ToInt32(isVip) == 0)
                        {
                            UserProfile p = new UserProfile()
                            {
                                UserAge = Convert.ToString(r["Age"]),
                                FirstName = (string)r["FirstName"],
                                LastName = (string)r["LastName"],
                                UserName = (string)r["LoginName"],
                                UserRegion = (string)r["Region"],
                                UserCity = (string)r["City"],
                                ProfileId = Convert.ToString(r["ProfileID"]),
                                GenderId = Convert.ToString(r["GenderId"]),
                                ImageUrl = path,
                                IsVip = isvip == true ? "1" : "0"
                            };

                            pList.Profiles.Add(p);
                        }

                    }
                    else if (gender == "1")
                    {
                        UserProfile p = new UserProfile()
                        {
                            UserAge = Convert.ToString(r["Age"]),
                            FirstName = (string)r["FirstName"],
                            LastName = (string)r["LastName"],
                            UserName = (string)r["LoginName"],
                            UserRegion = (string)r["Region"],
                            UserCity = (string)r["City"],
                            ProfileId = Convert.ToString(r["ProfileID"]),
                            GenderId = Convert.ToString(r["GenderId"]),
                            ImageUrl = path,
                            IsVip = "0"
                        };

                        pList.Profiles.Add(p);
                    }
                }


            }
            catch (Exception ex)
            {

            }

            return pList;
        }
        */

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "deletephoto/{userid}/{photoId}")]
        public string deletePhoto(string userid, string photoId) {
            try {
                DSMembers ds = DataHelpers.GetEUS_CustomerPhotosByCustomerPhotosID(Convert.ToInt32(photoId));
                DSMembers.EUS_CustomerPhotosRow deletedPhoto = (DSMembers.EUS_CustomerPhotosRow)ds.EUS_CustomerPhotos.Rows[0];

                string fileName = ds.EUS_CustomerPhotos.Rows[0].Field<string>("FileName");
                bool isDefault = false;
                if ((!ds.EUS_CustomerPhotos.Rows[0].IsNull("IsDefault"))) {
                    isDefault = ds.EUS_CustomerPhotos.Rows[0].Field<bool>("IsDefault");
                }

                ds.EUS_CustomerPhotos.Rows[0].SetField("IsDeleted", true);
                DataHelpers.UpdateEUS_CustomerPhotos(ref ds);

                return "ok";
            }
            catch (Exception) {
                return "error";
            }
        }



        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "getsearchresults_v2/{userid}/{sort}/{zipstr}/{lat}/{lng}/{distance}/{gender}/{searchname}/{username}/{agemin}/{agemax}/{hasPhotos}/{isOnline}/{indexstart}/{results}/{isVip}")]
        public UserProfilesList getSearchResultsV2(string userid, string sort, string zipstr, string lat, string lng, string distance,
            string gender, string searchname, string username, string agemin, string agemax,
            string hasPhotos, string isOnline, string indexstart, string results, string isVip) {
            int sortInt = Convert.ToInt32(sort);
            SearchSortEnum sortEnum = SearchSortEnum.NewestMember;

            string sql = "";
            //bool sqlSet = false;
            UserProfilesList pList = new UserProfilesList();
            pList.Profiles = new List<UserProfile>();

            try {
                DataHelpers.UpdateEUS_Profiles_Activity(Convert.ToInt32(userid), getClientIP(), true);


                if (Convert.ToInt32(searchname) == 1) {

                    clsSQLInjectionClear.ClearString(ref username, 100);
                    if (username.Length > 0) {
                        sql = sql + "\r\n" + " and  EUS_Profiles.LoginName like '" + username + "%' ";

                    }
                }
                else {
                    if (Convert.ToInt32(agemin) != -1 && Convert.ToInt32(agemax) != -1) {
                        DateTime fromDate = DateTime.Now.AddYears((Convert.ToInt32(agemax) * -1) - 1).Date;
                        DateTime toDate = DateTime.Now.AddYears((Convert.ToInt32(agemin) * -1)).Date;

                        sql = sql + "\r\n" + " and  EUS_Profiles.Birthday between '" + fromDate.ToString("yyyy-MM-dd hh:mm:ss.mmm") + "' and '" + toDate.ToString("yyyy-MM-dd hh:mm:ss.mmm") + "'  ";
                        //sqlSet = true;
                    }
                    else if (Convert.ToInt32(agemin) != -1) {
                        DateTime toDate = DateTime.Now.AddYears((Convert.ToInt32(agemin) * -1)).Date;
                        sql = sql + "\r\n" + " and EUS_Profiles.Birthday < '" + toDate.ToString("yyyy-MM-dd hh:mm:ss.mmm") + "' ";
                        //sqlSet = true;
                    }
                    else if (Convert.ToInt32(agemax) != -1) {
                        DateTime fromDate = DateTime.Now.AddYears((Convert.ToInt32(agemax) * -1) - 1).Date;
                        sql = sql + "\r\n" + " and EUS_Profiles.Birthday between '" + fromDate.ToString("yyyy-MM-dd hh:mm:ss.mmm") + "' and getdate() ";
                        //sqlSet = true;
                    }

                    if (hasPhotos == "1") {
                        sql = sql + " and exists (select customerId from EUS_CustomerPhotos phot1 where phot1.customerId=EUS_Profiles.ProfileId and phot1.HasAproved=1 and phot1.IsDeleted=0)";
                        //sqlSet = true;
                    }


                    if (isOnline == "1") {
                        int mins = clsConfigValues.Get__members_online_minutes();
                        DateTime lastActivityUTCDate = DateTime.UtcNow.AddMinutes(-mins);

                        sql = sql + " and (EUS_Profiles.IsOnline=1 and EUS_Profiles.LastActivityDateTime>= '" + lastActivityUTCDate.ToString("yyyy-MM-dd HH:mm") + "')";
                        //sqlSet = true;
                    }

                    if (isVip == "1" && gender == "2") {
                        sql = sql + " and isnull(EUS_Profiles.AvailableCredits,0)>=" + ProfileHelper.Config_UNLOCK_MESSAGE_READ_CREDITS;
                        //sqlSet = true;
                    }

                    switch (sortInt) {
                        case 0:
                            sortEnum = SearchSortEnum.None;
                            break;
                        case 1:
                            sortEnum = SearchSortEnum.NewestMember;
                            break;

                        case 2:
                            sortEnum = SearchSortEnum.OldestMember;
                            break;

                        case 3:
                            sortEnum = SearchSortEnum.RecentlyLoggedIn;
                            break;

                        case 4:
                            sortEnum = SearchSortEnum.NearestDistance;
                            break;

                    }
                }

                clsSearchHelperParameters searchParams = new clsSearchHelperParameters();
                searchParams.CurrentProfileId = Convert.ToInt32(userid);
                searchParams.ReturnRecordsWithStatus = (int)ProfileStatusEnum.Approved;
                searchParams.SearchSort = sortEnum;
                searchParams.zipstr = zipstr;
                searchParams.latitudeIn = Convert.ToDouble(lat);
                searchParams.longitudeIn = Convert.ToDouble(lng);


                if (Convert.ToInt32(distance) > 0) {
                    searchParams.Distance = Convert.ToInt32(distance);
                }

                if (Convert.ToInt32(gender) == 1) {
                    searchParams.LookingFor_ToMeetFemaleID = true;
                    searchParams.LookingFor_ToMeetMaleID = false;
                }
                else if (Convert.ToInt32(gender) == 2) {
                    searchParams.LookingFor_ToMeetFemaleID = false;
                    searchParams.LookingFor_ToMeetMaleID = true;
                }

                searchParams.rowNumberMin = Convert.ToInt32(indexstart);
                searchParams.rowNumberMax = Convert.ToInt32(indexstart) + 20;
                //searchParams.NumberOfRecordsToReturn = 20;
                searchParams.AdditionalWhereClause = sql;
                searchParams.performCount = false;

                DataSet set = clsSearchHelper.GetMembersToSearchDataTable(searchParams);
                /*
                int start = Convert.ToInt32(indexstart);
                int result = Convert.ToInt32(results);
                int rowsmax;
                if (start + result < dataTable.Rows.Count)
                {
                    rowsmax = start + result;
                }
                else
                {
                    rowsmax = dataTable.Rows.Count;
                }
                */

                DataTable dt = set.Tables[0];
                for (int i = 0; i < dt.Rows.Count; i++) {
                    DataRow dr = dt.Rows[i];


                    string fileName = "";
                    DSMembers.EUS_CustomerPhotosRow photo = DataHelpers.GetProfilesDefaultPhoto(Convert.ToInt32(dr["ProfileID"]));
                    if (photo != null)
                        fileName = photo.FileName;
                    //string path = ProfileHelper.GetProfileImageURL(Convert.ToInt32(r["ProfileID"]), fileName, Convert.ToInt32(r["GenderId"]), true, false);
                    string path = getImagePath(Convert.ToInt32(dr["ProfileID"]), fileName, Convert.ToInt32(dr["GenderId"]), true, false);

                    UserProfile p = new UserProfile()
                    {
                        UserAge = Convert.ToString(dr["Age"]),
                        FirstName = (string)dr["FirstName"],
                        LastName = (string)dr["LastName"],
                        UserName = (string)dr["LoginName"],
                        UserRegion = (string)dr["Region"],
                        UserCity = (string)dr["City"],
                        ProfileId = Convert.ToString(dr["ProfileID"]),
                        GenderId = Convert.ToString(dr["GenderId"]),
                        ImageUrl = path,
                        Latitude = (dt.Columns.Contains("latitude") ? Convert.ToString(dr["latitude"]) : null),
                        Longtitude = (dt.Columns.Contains("longitude") ? Convert.ToString(dr["longitude"]) : null),
                    };

                    pList.Profiles.Add(p);


                }
            }
            catch (Exception) {

            }

            return pList;
        }

        private string getImagePath(int profileId, string fileName, int genderId, bool isThumb, bool isHTTPS) {
            string path = ProfileHelper.GetProfileImageURL(profileId, fileName, genderId, isThumb, isHTTPS);
            if (isThumb) {
                if (path.StartsWith("/")) {
                    path = "http://www.goomena.com" + path;
                }
                else {
                    path = path.Replace("thumbs", "d150");
                }

            }
            return path;
        }

        private string getImagePathMyProfile(int profileId, string fileName, int genderId, bool isHTTPS) {
            string path = ProfileHelper.GetProfileImageURL(profileId, fileName, genderId, true, isHTTPS);

            return path;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "getsearchresults/{userid}/{sort}/{zipstr}/{lat}/{lng}/{distance}/{gender}/{searchname}/{username}/{agemin}/{agemax}/{hasPhotos}/{isOnline}/{indexstart}/{results}")]
        public UserProfilesList getSearchResults(string userid, string sort, string zipstr, string lat, string lng, string distance,
            string gender, string searchname, string username, string agemin, string agemax,
            string hasPhotos, string isOnline, string indexstart, string results) {
            int sortInt = Convert.ToInt32(sort);
            SearchSortEnum sortEnum = SearchSortEnum.NewestMember;

            string sql = "";
            //bool sqlSet = false;
            UserProfilesList pList = new UserProfilesList();
            pList.Profiles = new List<UserProfile>();

            try {
                DataHelpers.UpdateEUS_Profiles_Activity(Convert.ToInt32(userid), getClientIP(), true);


                if (Convert.ToInt32(searchname) == 1) {

                    clsSQLInjectionClear.ClearString(ref username, 100);
                    if (username.Length > 0) {
                        sql = sql + "\r\n" + " and  EUS_Profiles.LoginName like '" + username + "%' ";

                    }
                }
                else {
                    if (Convert.ToInt32(agemin) != -1 && Convert.ToInt32(agemax) != -1) {
                        DateTime fromDate = DateTime.Now.AddYears((Convert.ToInt32(agemax) * -1) - 1).Date;
                        DateTime toDate = DateTime.Now.AddYears((Convert.ToInt32(agemin) * -1)).Date;

                        sql = sql + "\r\n" + " and  EUS_Profiles.Birthday between '" + fromDate.ToString("yyyy-MM-dd hh:mm:ss.mmm") + "' and '" + toDate.ToString("yyyy-MM-dd hh:mm:ss.mmm") + "'  ";
                        //sqlSet = true;
                    }
                    else if (Convert.ToInt32(agemin) != -1) {
                        DateTime toDate = DateTime.Now.AddYears((Convert.ToInt32(agemin) * -1)).Date;
                        sql = sql + "\r\n" + " and EUS_Profiles.Birthday < '" + toDate.ToString("yyyy-MM-dd hh:mm:ss.mmm") + "' ";
                        //sqlSet = true;
                    }
                    else if (Convert.ToInt32(agemax) != -1) {
                        DateTime fromDate = DateTime.Now.AddYears((Convert.ToInt32(agemax) * -1) - 1).Date;
                        sql = sql + "\r\n" + " and EUS_Profiles.Birthday between '" + fromDate.ToString("yyyy-MM-dd hh:mm:ss.mmm") + "' and getdate() ";
                        //sqlSet = true;
                    }

                    if (hasPhotos == "1") {
                        sql = sql + " and exists (select customerId from EUS_CustomerPhotos phot1 where phot1.customerId=EUS_Profiles.ProfileId and phot1.HasAproved=1 and phot1.IsDeleted=0)";
                        //sqlSet = true;
                    }


                    if (isOnline == "1") {
                        int mins = clsConfigValues.Get__members_online_minutes();
                        DateTime lastActivityUTCDate = DateTime.UtcNow.AddMinutes(-mins);

                        sql = sql + " and (EUS_Profiles.IsOnline=1 and EUS_Profiles.LastActivityDateTime>= '" + lastActivityUTCDate.ToString("yyyy-MM-dd HH:mm") + "')";
                        //sqlSet = true;
                    }

                    switch (sortInt) {
                        case 0:
                            sortEnum = SearchSortEnum.None;
                            break;
                        case 1:
                            sortEnum = SearchSortEnum.NewestMember;
                            break;

                        case 2:
                            sortEnum = SearchSortEnum.OldestMember;
                            break;

                        case 3:
                            sortEnum = SearchSortEnum.RecentlyLoggedIn;
                            break;

                        case 4:
                            sortEnum = SearchSortEnum.NearestDistance;
                            break;

                    }
                }

                clsSearchHelperParameters searchParams = new clsSearchHelperParameters();
                searchParams.CurrentProfileId = Convert.ToInt32(userid);
                searchParams.ReturnRecordsWithStatus = (int)ProfileStatusEnum.Approved;
                searchParams.SearchSort = sortEnum;
                searchParams.zipstr = zipstr;
                searchParams.latitudeIn = Convert.ToDouble(lat);
                searchParams.longitudeIn = Convert.ToDouble(lng);
                searchParams.rowNumberMin = Convert.ToInt32(indexstart);
                searchParams.rowNumberMax = Convert.ToInt32(indexstart) + 20;

                if (Convert.ToInt32(distance) > 0) {
                    searchParams.Distance = Convert.ToInt32(distance);
                }

                if (Convert.ToInt32(gender) == 1) {
                    searchParams.LookingFor_ToMeetFemaleID = true;
                    searchParams.LookingFor_ToMeetMaleID = false;
                }
                else if (Convert.ToInt32(gender) == 2) {
                    searchParams.LookingFor_ToMeetFemaleID = false;
                    searchParams.LookingFor_ToMeetMaleID = true;
                }

                searchParams.NumberOfRecordsToReturn = Convert.ToInt32(indexstart) + Convert.ToInt32(results);
                searchParams.AdditionalWhereClause = sql;
                searchParams.performCount = false;

                DataSet set = clsSearchHelper.GetMembersToSearchDataTable(searchParams);
                DataTable dt = set.Tables[0];

                /*
                int start = Convert.ToInt32(indexstart);
                int result = Convert.ToInt32(results);
                int rowsmax;
                if (start + result < dataTable.Rows.Count)
                {
                    rowsmax = start + result;
                }
                else
                {
                    rowsmax = dataTable.Rows.Count;
                }

                 * */

                for (int i = 0; i < dt.Rows.Count; i++) {
                    DataRow dr = dt.Rows[i];

                    string fileName = "";
                    DSMembers.EUS_CustomerPhotosRow photo = DataHelpers.GetProfilesDefaultPhoto(Convert.ToInt32(dr["ProfileID"]));
                    if (photo != null)
                        fileName = photo.FileName;
                    string path = ProfileHelper.GetProfileImageURL(Convert.ToInt32(dr["ProfileID"]), fileName, Convert.ToInt32(dr["GenderId"]), true, false);

                    UserProfile p = new UserProfile()
                    {
                        UserAge = Convert.ToString(dr["Age"]),
                        FirstName = (string)dr["FirstName"],
                        LastName = (string)dr["LastName"],
                        UserName = (string)dr["LoginName"],
                        UserRegion = (string)dr["Region"],
                        UserCity = (string)dr["City"],
                        ProfileId = Convert.ToString(dr["ProfileID"]),
                        GenderId = Convert.ToString(dr["GenderId"]),
                        ImageUrl = path,
                        Latitude = (dt.Columns.Contains("latitude") ? Convert.ToString(dr["latitude"]) : null),
                        Longtitude = (dt.Columns.Contains("longitude") ? Convert.ToString(dr["longitude"]) : null),
                    };

                    pList.Profiles.Add(p);
                }


            }
            catch (Exception) {

            }

            return pList;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "messagesquick/{userid}")]
        public MessageInfoList getMessagesQuick(string userid) {
            MessageInfoList list = new MessageInfoList();
            try {
                DataHelpers.UpdateEUS_Profiles_Activity(Convert.ToInt32(userid), getClientIP(), true);

                // 4 = new messages
                // 20 results
                DataSet ds = clsMessagesHelper.GetNewMessagesQuick2(Convert.ToInt32(userid), 4, 20);

                list.msgList = new List<MessageInfo>();
                foreach (DataRow dr in ds.Tables[0].Rows) {

                    string fileName = "";
                    DSMembers.EUS_CustomerPhotosRow photo = DataHelpers.GetProfilesDefaultPhoto(Convert.ToInt32(dr["ProfileID"]));
                    if (photo != null)
                        fileName = photo.FileName;
                    //string path = ProfileHelper.GetProfileImageURL(Convert.ToInt32(dr["ProfileID"]), fileName, Convert.ToInt32(dr["GenderId"]), true, false);
                    string path = getImagePath(Convert.ToInt32(dr["ProfileID"]), fileName, Convert.ToInt32(dr["GenderId"]), true, false);

                    MessageInfo inf = new MessageInfo()
                    {
                        MsgCount = Convert.ToString(dr["ItemsCount"]),
                        MsgId = Convert.ToString(dr["EUS_MessageID"]),
                        ProfileId = Convert.ToString(dr["ProfileID"]),
                        UserName = (string)dr["LoginName"],
                        Age = Convert.ToString(ProfileHelper.GetCurrentAge((DateTime)dr["Birthday"])),
                        City = (string)dr["City"],
                        Region = (string)dr["Region"],
                        Country = (string)dr["Country"],
                        DateTimeSent = ((DateTime)dr["MAXDateTimeToCreate"]).ToString("dd MMM, yyyy H:mm"),
                        ProfileImg = path
                    };
                    list.msgList.Add(inf);

                }


            }
            catch (Exception) { }

            return list;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "messageslist/{userid}/{view}/{indexstart}/{active}")]
        public MessageInfoList getMessagesList(string userid, string view, string indexstart, string active) {
            DataHelpers.UpdateEUS_Profiles_Activity(Convert.ToInt32(userid), getClientIP(), true);

            int results = 20;

            MessagesViewEnum msgView = new MessagesViewEnum();
            switch (Convert.ToInt32(view)) {
                case 0:
                    msgView = MessagesViewEnum.None;
                    break;

                case 1:
                    msgView = MessagesViewEnum.INBOX;
                    break;

                case 2:
                    msgView = MessagesViewEnum.SENT;
                    break;

                case 4:
                    msgView = MessagesViewEnum.CONVERSATION;
                    break;

                case 8:
                    msgView = MessagesViewEnum.NEWMESSAGES;
                    break;

                case 16:
                    msgView = MessagesViewEnum.TRASH;
                    break;

                case 32:
                    msgView = MessagesViewEnum.SEND_NEW_MESSAGE;
                    break;

            }

            Boolean isActive;
            if (active == "1") {
                isActive = true;
            }
            else {
                isActive = false;
            }

            clsMessagesHelperParameters parms = new clsMessagesHelperParameters();
            parms.CurrentProfileId = Convert.ToInt32(userid);
            parms.MessagesView = msgView;
            parms.cmdFilter = "";
            parms.performCount = false;
            parms.showActive = isActive;
            parms.rowNumberMin = Convert.ToInt32(indexstart);
            parms.rowNumberMax = Convert.ToInt32(indexstart) + results;

            DataSet dt = clsMessagesHelper.GetMessagesDataTable(ref parms);
            MessageInfoList messageList = new MessageInfoList();
            messageList.msgList = new List<MessageInfo>();


            if (dt != null && dt.Tables.Count > 0) {

                for (int i = 0; i < dt.Tables[0].Rows.Count; i++) {
                    DataRow dr = dt.Tables[0].Rows[i];
                    string fileName = "";
                    DSMembers.EUS_CustomerPhotosRow photo = DataHelpers.GetProfilesDefaultPhoto(Convert.ToInt32(dr["ProfileID"]));
                    if (photo != null)
                        fileName = photo.FileName;
                    string path = ProfileHelper.GetProfileImageURL(Convert.ToInt32(dr["ProfileID"]), fileName, Convert.ToInt32(dr["GenderId"]), true, false);

                    MessageInfo info = new MessageInfo()
                    {
                        MsgCount = Convert.ToString(dr["ItemsCount"]),
                        MsgId = Convert.ToString(dr["EUS_MessageID"]),
                        ProfileId = Convert.ToString(dr["ProfileID"]),
                        UserName = (string)dr["LoginName"],
                        Age = Convert.ToString(ProfileHelper.GetCurrentAge((DateTime)dr["Birthday"])),
                        City = (string)dr["City"],
                        Region = (string)dr["Region"],
                        Country = (string)dr["Country"],
                        DateTimeSent = ((DateTime)dr["MAXDateTimeToCreate"]).ToString("dd MMM, yyyy H:mm"),
                        ProfileImg = path

                    };

                    messageList.msgList.Add(info);

                }


            }

            return messageList;

        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "conversation/{profileid}/{messageid}")]
        public MessageList getConversationMessages(string profileid, string messageid) {
            DataHelpers.UpdateEUS_Profiles_Activity(Convert.ToInt32(profileid), getClientIP(), true);


            DataSet ds = clsMessagesHelper.GetConversationWithMessage(Convert.ToInt32(profileid), Convert.ToInt32(messageid));
            MessageList mList = new MessageList();
            mList.list = new List<Message>();

            foreach (DataRow dr in ds.Tables[0].Rows) {
                Message m = new Message()
                {
                    MessageId = Convert.ToString(dr["EUS_MessageID"]),
                    FromProfileId = Convert.ToString(dr["FromProfileId"]),
                    ToProfileId = Convert.ToString(dr["ToProfileId"]),
                    Subject = Convert.ToString(dr["subject"]),
                    Body = Convert.ToString(dr["Body"]),
                    DateTimeSent = Convert.ToDateTime(dr["DateTimeToCreate"]).ToString("dd MMM, yyyy H:mm"),
                    IsReceived = Convert.ToBoolean(dr["IsReceived"]),
                    IsSent = Convert.ToBoolean(dr["IsSent"]),
                    statusId = Convert.ToString(dr["StatusID"])
                };

                mList.list.Add(m);
            }


            return mList;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "message/{messageid}")]
        public Message getMessageById(string messageid) {
            EUS_Message message = clsUserDoes.GetMessage(Convert.ToInt32(messageid));

            Message m = new Message()
            {
                Subject = message.Subject,
                Body = message.Body,
                MessageId = Convert.ToString(message.EUS_MessageID),
                FromProfileId = Convert.ToString(message.FromProfileID),
                ToProfileId = Convert.ToString(message.ToProfileID),
                DateTimeSent = message.DateTimeToCreate.Value.ToString("dd MMM, yyyy H:mm"),
                IsReceived = message.IsReceived,
                IsSent = message.IsSent

            };

            return m;

        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "unlockmessage/{userid}/{genderid}/{senderid}/{messageid}/{offerid}")]
        public string unlockMessage(string userid, string genderid, string senderid, string messageid, string offerid) {
            DataHelpers.UpdateEUS_Profiles_Activity(Convert.ToInt32(userid), getClientIP(), true);

            if (genderid == "1") {

                bool hasCreditsToRead = clsUserDoes.HasRequiredCredits(Convert.ToInt32(userid), UnlockType.UNLOCK_MESSAGE_READ);
                if (hasCreditsToRead) {
                    double _REF_CRD2EURO_Rate = DataHelpers.GetEUS_Profiles_REF_CRD2EURO_Rate(Convert.ToInt32(senderid));
                    long customerCreditsId = clsUserDoes.UnlockMessageOnce(Convert.ToInt32(senderid),
                                                                Convert.ToInt32(userid),
                                                                (int)ProfileHelper.Config_UNLOCK_MESSAGE_READ_CREDITS,
                                                                UnlockType.UNLOCK_MESSAGE_READ,
                                                                Convert.ToInt32(messageid),
                                                                _REF_CRD2EURO_Rate,
                                                                false);

                    clsUserDoes.MarkMessageRead(Convert.ToInt32(messageid));

                    int gid = Convert.ToInt32(genderid);
                    int ifromprofileid = Convert.ToInt32(userid);
                    int itoprofileid = Convert.ToInt32(senderid);
                    int iofferId = Convert.ToInt32(offerid);
                    clsUserDoes.MakeNewDate(ifromprofileid, itoprofileid, iofferId, (gid == 1));
                    //makeNewDate(userid, senderid, Convert.ToInt32(offerid), Convert.ToInt32(genderid));
                    return "ok";
                }
                else {
                    return "noceredits";
                }

            }
            else if (genderid == "2") {
                clsUserDoes.MarkMessageRead(Convert.ToInt32(messageid));
                return "ok";
            }

            return "";

        }

        //private void makeNewDate(string fromProfileId, string toProfileId, int offerId, int genderId) {
        //    clsUserDoes.NewOfferParameters parms = new clsUserDoes.NewOfferParameters();
        //    bool anyOfferAccepted = false;

        //    if (offerId > 0) {
        //        EUS_Offer offer = clsUserDoes.GetOfferByOfferId(Convert.ToInt16(fromProfileId), offerId);
        //        if (offer.OfferTypeID == ProfileHelper.OfferTypeID_POKE) {
        //            clsUserDoes.AcceptPoke(offerId, OfferStatusEnum.POKE_ACCEEPTED_WITH_MESSAGE);
        //            anyOfferAccepted = true;

        //        }
        //        else if (offer.OfferTypeID == ProfileHelper.OfferTypeID_WINK) {
        //            clsUserDoes.AcceptLike(offerId, OfferStatusEnum.LIKE_ACCEEPTED_WITH_MESSAGE);
        //            anyOfferAccepted = true;

        //        }
        //        else if (offer.OfferTypeID == ProfileHelper.OfferTypeID_OFFERNEW || offer.OfferTypeID == ProfileHelper.OfferTypeID_OFFERCOUNTER) {
        //            clsUserDoes.AcceptOffer(offerId, OfferStatusEnum.OFFER_ACCEEPTED_WITH_MESSAGE);
        //            anyOfferAccepted = true;
        //        }

        //        parms.messageText1 = "";
        //        parms.messageText2 = "";
        //        parms.offerAmount = offer.Amount.Value;
        //        parms.parentOfferId = (int)offer.OfferID;
        //        parms.userIdReceiver = (int)offer.ToProfileID;
        //        parms.userIdWhoDid = (int)offer.FromProfileID;

        //    }
        //    else {
        //        parms.messageText1 = "";
        //        parms.messageText2 = "";
        //        parms.offerAmount = 0;
        //        parms.userIdReceiver = Convert.ToInt32(toProfileId);
        //        parms.userIdWhoDid = Convert.ToInt32(fromProfileId);
        //    }


        //    if (genderId == 1) {
        //        if (!clsUserDoes.HasDateOfferAlready(parms.userIdWhoDid, parms.userIdReceiver)) {
        //            clsUserDoes.MakeNewDate(parms);

        //        }

        //    }

        //    if (!anyOfferAccepted) {

        //        EUS_Offer rec = clsUserDoes.GetAnyWinkPending(Convert.ToInt32(fromProfileId), Convert.ToInt32(fromProfileId));
        //        if (rec != null) {

        //            clsUserDoes.AcceptLike((int)rec.OfferID, OfferStatusEnum.LIKE_ACCEEPTED_WITH_MESSAGE);
        //        }
        //        else {
        //            rec = clsUserDoes.GetAnyPokePending(Convert.ToInt32(fromProfileId), Convert.ToInt32(toProfileId));
        //            if (rec != null) {

        //                clsUserDoes.AcceptPoke((int)rec.OfferID, OfferStatusEnum.POKE_ACCEEPTED_WITH_MESSAGE);
        //            }
        //            else {
        //                rec = clsUserDoes.GetLastOffer(Convert.ToInt32(fromProfileId), Convert.ToInt32(toProfileId));
        //                if (rec != null) {
        //                    clsUserDoes.AcceptPoke((int)rec.OfferID, OfferStatusEnum.POKE_ACCEEPTED_WITH_MESSAGE);
        //                }
        //            }
        //        }

        //    }

        //}

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "sendmessage")]
        public string sendMessage(MessageParams mp) {

            int genderid = Convert.ToInt32(mp.genderid);
            bool sendOnce = true;
            //bool sendUnlimited = false;
            bool alreadyUnlockedUnlimited = false;
            bool messagePaid = false;
            EUS_Message messageReceived = null;

            DataHelpers.UpdateEUS_Profiles_Activity(Convert.ToInt32(mp.fromprofileid), getClientIP(), true);

            bool blockReceiving = clsProfilesPrivacySettings.GET_AdminSetting_BlockMessagesToBeReceivedByOther(Convert.ToInt32(mp.fromprofileid));
            if (genderid == 1) {

                bool maySendMessage = clsUserDoes.HasRequiredCredits(Convert.ToInt32(mp.fromprofileid), UnlockType.UNLOCK_MESSAGE_SEND);

                if (maySendMessage) {

                    messageReceived = clsUserDoes.SendMessage(mp.subject, mp.body, Convert.ToInt32(mp.fromprofileid), Convert.ToInt32(mp.toprofileid), false, 0, blockReceiving, false, genderid);

                    if (!alreadyUnlockedUnlimited) {
                        if (sendOnce) {
                            double _REF_CRD2EURO_Rate = DataHelpers.GetEUS_Profiles_REF_CRD2EURO_Rate(Convert.ToInt32(messageReceived.FromProfileID.Value));
                            long customerCreditsId = clsUserDoes.UnlockMessageOnce(
                                                                    Convert.ToInt32(mp.toprofileid),
                                                                    Convert.ToInt32(mp.fromprofileid),
                                                                    (int)ProfileHelper.Config_UNLOCK_MESSAGE_SEND_CREDITS,
                                                                    UnlockType.UNLOCK_MESSAGE_SEND,
                                                                    messageReceived.EUS_MessageID,
                                                                    _REF_CRD2EURO_Rate,
                                                                    false);
                            try {

                                double costPerMessage = ProfileHelper.Config_UNLOCK_MESSAGE_SEND_CREDITS * _REF_CRD2EURO_Rate;
                                clsReferrer.SetCommissionForMessage((int)messageReceived.EUS_MessageID, (int)messageReceived.ToProfileID, costPerMessage, (int)ProfileHelper.Config_UNLOCK_MESSAGE_SEND_CREDITS, UnlockType.UNLOCK_MESSAGE_SEND, customerCreditsId);
                            }
                            catch (Exception ex) {
                                WebErrorSendEmail(ex, "send message");
                                return "-1";
                            }

                            messagePaid = true;

                        }
                    }

                }

            }
            else if (genderid == 2) {
                messageReceived = clsUserDoes.SendMessage(mp.subject, mp.body, Convert.ToInt32(mp.fromprofileid), Convert.ToInt32(mp.toprofileid), true, 0, blockReceiving, false, genderid);
                messagePaid = true;
            }
            if (mp.toprofileid == "1") {
                try {
                    using (myContext) {

                        var p = (from q in myContext.EUS_Profiles
                                 where q.ProfileID == Convert.ToInt32(mp.fromprofileid)
                                 select new
                                 {
                                     login = q.LoginName,
                                     email = q.eMail
                                 }).FirstOrDefault();

                        string newSubject = "Message sent to administrator of Goomena.com";
                        string newBody =
                            "<br>" +
                            "Message sent to administrator of Goomena.com" + "<br>" +
                            "<hr>" +
                            "Login Name : " + p.login + "<br>" +
                                "E-mail : " + p.email + "<br>" +
                            "<hr>" +
                            "Subject : " + mp.subject + "<br>" +
                            "Body : " + mp.body;

                        clsMyMail.SendMail(ConfigurationManager.AppSettings["gToEmail"], newSubject, newBody, true);

                    }
                }
                catch (Exception ex) {
                    WebErrorSendEmail(ex, "send message");
                    return "-1";
                }

            }



            if (messagePaid) {
                int ifromprofileid = Convert.ToInt32(mp.fromprofileid);
                int itoprofileid = Convert.ToInt32(mp.toprofileid);
                int iofferId = Convert.ToInt32(mp.offerId);
                clsUserDoes.MakeNewDate(ifromprofileid, itoprofileid, iofferId, (genderid == 1));
                //makeNewDate(mp.fromprofileid, mp.toprofileid, Convert.ToInt32(mp.offerId), Convert.ToInt32(mp.genderid));

            }

            string notifResult = sendNotificationToUser(mp.fromprofileid, mp.toprofileid, "3");

            if (messageReceived != null) {
                return Convert.ToString(messageReceived.EUS_MessageID + "notification sent: " + notifResult);
            }
            else {
                return "-1";
            }

        }



        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "deletemessage/{profileid}/{messageid}/{referrerparentid}")]
        public string deleteMessage(string profileid, string messageid, string referrerparentid) {
            DataHelpers.UpdateEUS_Profiles_Activity(Convert.ToInt32(profileid), getClientIP(), true);

            string result = "";
            if (referrerparentid != null && Convert.ToInt32(referrerparentid) > 0) {
                clsUserDoes.DeleteMessage(Convert.ToInt32(profileid), Convert.ToInt32(messageid), true);
                result = "deleted";
            }
            else {
                clsUserDoes.DeleteMessage(Convert.ToInt32(profileid), Convert.ToInt32(messageid), false);
                result = "deleted";
            }
            return result;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "deletemessages/{profileId}/{messagesView}/{messageId}")]
        public String deleteMessagesInView(string profileId, string messagesView, string messageId) {
            try {
                MessagesViewEnum msgEnum = MessagesViewEnum.NEWMESSAGES;

                if (messagesView == "8") {
                    msgEnum = MessagesViewEnum.NEWMESSAGES;
                }
                else if (messagesView == "1") {
                    msgEnum = MessagesViewEnum.INBOX;
                }
                else if (messagesView == "2") {
                    msgEnum = MessagesViewEnum.SENT;
                }
                else if (messagesView == "16") {
                    msgEnum = MessagesViewEnum.TRASH;
                }


                bool isReferrer = (from p in myContext.EUS_Profiles
                                   where p.ProfileID == Convert.ToInt32(profileId)
                                   select p.ReferrerParentId.HasValue && p.ReferrerParentId.Value > 0).First();


                //DataHelpers.EUS_Profile_GetIsReferrer(Me.CMSDBDataContext, OtherMemberProfileID)
                EUS_Message message = clsUserDoes.GetMessage(Convert.ToInt32(messageId));
                if ((message != null))
                    clsMessagesHelper.DeleteMessagesFromMemberInView(Convert.ToInt32(profileId), message.FromProfileID.Value, message.ToProfileID.Value, msgEnum, isReferrer);
            }
            catch (Exception ex) {
                return ex.ToString();
            }
            return "ok";
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "favorite_v2/{userid}/{zip}/{lat}/{lng}/{isOnline}")]
        public UserProfilesList getFavoriteListV2(string userid, string zip, string lat, string lng, string isOnline) {
            DataHelpers.UpdateEUS_Profiles_Activity(Convert.ToInt32(userid), getClientIP(), true);

            bool ison = isOnline == "1" ? true : false;

            UserProfilesList pList = new UserProfilesList();
            pList.Profiles = new List<UserProfile>();

            clsMyListsHelperParameters parms = new clsMyListsHelperParameters();
            parms.CurrentProfileId = Convert.ToInt32(userid);
            parms.sorting = MyListsSortEnum.Recent;
            parms.ReturnRecordsWithStatus = (int)ProfileStatusEnum.Approved;
            parms.zipstr = zip;
            parms.latitudeIn = Convert.ToDouble(lat);
            parms.longitudeIn = Convert.ToDouble(lng);
            parms.Distance = clsSearchHelper.DISTANCE_DEFAULT;
            parms.isOnline = ison;

            parms.performCount = true;
            parms.rowNumberMin = 0;
            parms.rowNumberMax = 0;

            int count = 0;

            DataSet dt = clsSearchHelper.GetMyFavoriteMembersDataTable(parms);

            if (dt != null && dt.Tables[0] != null) {
                count = (int)dt.Tables[0].Rows[0][0];

                parms.performCount = false;
                parms.rowNumberMin = 0;
                parms.rowNumberMax = count;

                dt = clsSearchHelper.GetMyFavoriteMembersDataTable(parms);

                if (dt != null) {
                    pList = createUserProfilesListfromDataset(dt);
                    /*
                    foreach(DataRow r in dt.Tables[0].Rows)
                    {
                        string fileName = "";
                         DSMembers.EUS_CustomerPhotosRow photo = DataHelpers.GetProfilesDefaultPhoto(Convert.ToInt32(r["ProfileID"]));
                        if (photo != null)
                            fileName = photo.FileName;
                        string path = getImagePath(Convert.ToInt32(r["ProfileID"]), fileName, Convert.ToInt32(r["GenderId"]), true, false);

                         UserProfile p = new UserProfile()
                        {
                            FirstName = (string)r["FirstName"],
                            LastName = (string)r["LastName"],
                            UserRegion = (string)r["Region"],
                            UserCity = (string)r["City"],
                            UserAge = Convert.ToString(ProfileHelper.GetCurrentAge((DateTime)r["Birthday"])),
                            ProfileId = Convert.ToString(r["ProfileID"]),
                            UserName = (string)r["LoginName"],
                            DateRegister = ((DateTime)r["DateTimeToRegister"]).ToShortDateString(),
                            ImageUrl = path,
                            GenderId = Convert.ToString(r["GenderId"])

                        };

                        pList.Profiles.Add(p);
                    }
                }
*/

                }

            }

            return pList;
        }

        private UserProfilesList createUserProfilesListfromDataset(DataSet set) {
            UserProfilesList pList = new UserProfilesList();
            pList.Profiles = new List<UserProfile>();

            if (set != null) {
                DataTable dt = set.Tables[0];
                foreach (DataRow dr in dt.Rows) {
                    string fileName = "";
                    DSMembers.EUS_CustomerPhotosRow photo = DataHelpers.GetProfilesDefaultPhoto(Convert.ToInt32(dr["ProfileID"]));
                    if (photo != null)
                        fileName = photo.FileName;
                    string path = getImagePath(Convert.ToInt32(dr["ProfileID"]), fileName, Convert.ToInt32(dr["GenderId"]), true, false);

                    UserProfile p = new UserProfile()
                    {
                        FirstName = (string)dr["FirstName"],
                        LastName = (string)dr["LastName"],
                        UserRegion = (string)dr["Region"],
                        UserCity = (string)dr["City"],
                        UserAge = Convert.ToString(ProfileHelper.GetCurrentAge((DateTime)dr["Birthday"])),
                        ProfileId = Convert.ToString(dr["ProfileID"]),
                        UserName = (string)dr["LoginName"],
                        DateRegister = ((DateTime)dr["DateTimeToRegister"]).ToShortDateString(),
                        ImageUrl = path,
                        GenderId = Convert.ToString(dr["GenderId"]),
                        Latitude = (dt.Columns.Contains("latitude") ? Convert.ToString(dr["latitude"]) : null),
                        Longtitude = (dt.Columns.Contains("longitude") ? Convert.ToString(dr["longitude"]) : null),
                    };

                    pList.Profiles.Add(p);
                }
            }




            return pList;

        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "favorite/{userid}/{zip}/{lat}/{lng}")]
        public UserProfilesList getFavoriteList(string userid, string zip, string lat, string lng) {
            DataHelpers.UpdateEUS_Profiles_Activity(Convert.ToInt32(userid), getClientIP(), true);

            UserProfilesList pList = new UserProfilesList();
            pList.Profiles = new List<UserProfile>();

            clsMyListsHelperParameters parms = new clsMyListsHelperParameters();
            parms.CurrentProfileId = Convert.ToInt32(userid);
            parms.sorting = MyListsSortEnum.Recent;
            parms.ReturnRecordsWithStatus = (int)ProfileStatusEnum.Approved;
            parms.zipstr = zip;
            parms.latitudeIn = Convert.ToDouble(lat);
            parms.longitudeIn = Convert.ToDouble(lng);
            parms.Distance = clsSearchHelper.DISTANCE_DEFAULT;

            parms.performCount = true;
            parms.rowNumberMin = 0;
            parms.rowNumberMax = 0;

            int count = 0;

            DataSet dt = clsSearchHelper.GetMyFavoriteMembersDataTable(parms);

            if (dt != null && dt.Tables[0] != null) {
                count = (int)dt.Tables[0].Rows[0][0];

                parms.performCount = false;
                parms.rowNumberMin = 0;
                parms.rowNumberMax = count;

                dt = clsSearchHelper.GetMyFavoriteMembersDataTable(parms);

                if (dt != null) {
                    pList = createUserProfilesListfromDataset(dt);
                }
            }


            /*
            List<Int32> ids = new List<int>();
            foreach (DataRow dr in dt.Rows)
            {
                ids.Add(Convert.ToInt32(dr["ProfileID"]));
            }

            using (myContext)
            {

                foreach (int id in ids)
                {
                    EUS_Profile p = (from pr in myContext.EUS_Profiles
                                     where pr.ProfileID == id && pr.IsMaster == true
                                     select pr).FirstOrDefault();


                    string fileName = "";
                    DSMembers.EUS_CustomerPhotosRow photo = DataHelpers.GetProfilesDefaultPhoto(Convert.ToInt32(p.ProfileID));
                    if (photo != null)
                        fileName = photo.FileName;
                    //string path = ProfileHelper.GetProfileImageURL(Convert.ToInt32(p.ProfileID), fileName, Convert.ToInt32(p.GenderId), true, false);
                    string path = getImagePath(Convert.ToInt32(p.ProfileID), fileName, Convert.ToInt32(p.GenderId), true, false);

                    UserProfile prof = new UserProfile()
                    {
                        FirstName = p.FirstName,
                        LastName = p.LastName,
                        UserRegion = p.Region,
                        UserCity = p.City,
                        UserAge = Convert.ToString(ProfileHelper.GetCurrentAge(p.Birthday.Value)),
                        ProfileId = Convert.ToString(p.ProfileID),
                        UserName = p.LoginName,
                        DateRegister = p.DateTimeToRegister.Value.ToShortDateString(),
                        GenderId = Convert.ToString(p.GenderId.Value),
                        ImageUrl = path
                    };

                    pList.Profiles.Add(prof);
                }

            }
            */
            return pList;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "whoviewedme_v2/{userid}/{zip}/{lat}/{lng}/{indexstart}/{isOnline}")]
        public UserProfilesList getWhoViewedMeV2(string userid, string zip, string lat, string lng, string indexstart, string isOnline) {
            DataHelpers.UpdateEUS_Profiles_Activity(Convert.ToInt32(userid), getClientIP(), true);
            bool ison = isOnline == "1" ? true : false;

            UserProfilesList list = new UserProfilesList();
            list.Profiles = new List<UserProfile>();

            clsMyListsHelperParameters parms = new clsMyListsHelperParameters();
            parms.CurrentProfileId = Convert.ToInt32(userid);
            parms.sorting = MyListsSortEnum.Recent;
            parms.ReturnRecordsWithStatus = (int)ProfileStatusEnum.Approved;
            parms.zipstr = zip;
            parms.latitudeIn = Convert.ToDouble(lat);
            parms.longitudeIn = Convert.ToDouble(lng);
            parms.Distance = clsSearchHelper.DISTANCE_DEFAULT;

            parms.performCount = false;
            parms.rowNumberMin = Convert.ToInt32(indexstart);
            parms.rowNumberMax = Convert.ToInt32(indexstart) + 20;
            parms.isOnline = ison;

            DataSet dt = clsSearchHelper.GetWhoViewedMeMembersDataTable(parms);

            list = createUserProfilesListfromDataset(dt);


            /*
            DataTable dt = clsSearchHelper.GetWhoViewedMeMembersDataTable(MyListsSortEnum.Recent,
                                                                                         Convert.ToInt32(userid), 0,
                                                                                         4,
                                                                                         zip,
                                                                                         Convert.ToDouble(lat),
                                                                                         Convert.ToDouble(lng),100000,
                                                                                         ison);



           

            int start = Convert.ToInt32(indexstart);
            int results = 20;
            int max;
            if (start + results < dt.Rows.Count)
            {
                max = start + results;
            }
            else
            {
                max = dt.Rows.Count;
            }

          
                for (int i = start; i < max; i++)
                {
                    DataRow r = dt.Rows[i];
                    string fileName = "";
                    DSMembers.EUS_CustomerPhotosRow photo = DataHelpers.GetProfilesDefaultPhoto(Convert.ToInt32(r["ProfileID"]));
                    if (photo != null)
                        fileName = photo.FileName;
                    //string path = ProfileHelper.GetProfileImageURL(Convert.ToInt32(r["ProfileID"]), fileName, Convert.ToInt32(r["GenderId"]), true, false);
                    string path = getImagePath(Convert.ToInt32(r["ProfileID"]), fileName, Convert.ToInt32(r["GenderId"]), true, false);

                    UserProfile p = new UserProfile()
                    {
                        FirstName = (string)r["FirstName"],
                        LastName = (string)r["LastName"],
                        UserRegion = (string)r["Region"],
                        UserCity = (string)r["City"],
                        UserAge = Convert.ToString(ProfileHelper.GetCurrentAge((DateTime)r["Birthday"])),
                        ProfileId = Convert.ToString(r["ProfileID"]),
                        UserName = (string)r["LoginName"],
                        DateRegister = ((DateTime)r["DateTimeToRegister"]).ToShortDateString(),
                        ImageUrl = path,
                        GenderId = Convert.ToString(r["GenderId"])

                    };

                    list.Profiles.Add(p);
                }
             * */


            return list;
        }


        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "whoviewedme/{userid}/{zip}/{lat}/{lng}/{indexstart}")]
        public UserProfilesList getWhoViewedMe(string userid, string zip, string lat, string lng, string indexstart) {
            DataHelpers.UpdateEUS_Profiles_Activity(Convert.ToInt32(userid), getClientIP(), true);

            clsMyListsHelperParameters parms = new clsMyListsHelperParameters();
            parms.CurrentProfileId = Convert.ToInt32(userid);
            parms.sorting = MyListsSortEnum.Recent;
            parms.ReturnRecordsWithStatus = (int)ProfileStatusEnum.Approved;
            parms.zipstr = zip;
            parms.latitudeIn = Convert.ToDouble(lat);
            parms.longitudeIn = Convert.ToDouble(lng);
            parms.Distance = clsSearchHelper.DISTANCE_DEFAULT;

            parms.performCount = false;
            parms.rowNumberMin = Convert.ToInt32(indexstart);
            parms.rowNumberMax = Convert.ToInt32(indexstart) + 20;

            DataSet dt = clsSearchHelper.GetWhoViewedMeMembersDataTable(parms);



            UserProfilesList list = new UserProfilesList();
            list.Profiles = new List<UserProfile>();

            list = createUserProfilesListfromDataset(dt);

            /*
            int start = Convert.ToInt32(indexstart);
            int results = 20;
            int max;
            if (start + results < dt.Rows.Count)
            {
                max = start + results;
            }
            else
            {
                max = dt.Rows.Count;
            }
            for (int i = start; i < max; i++)
            {
                DataRow r = dt.Rows[i];
                string fileName = "";
                DSMembers.EUS_CustomerPhotosRow photo = DataHelpers.GetProfilesDefaultPhoto(Convert.ToInt32(r["ProfileID"]));
                if (photo != null)
                    fileName = photo.FileName;
                //string path = ProfileHelper.GetProfileImageURL(Convert.ToInt32(r["ProfileID"]), fileName, Convert.ToInt32(r["GenderId"]), true, false);
                string path = getImagePath(Convert.ToInt32(r["ProfileID"]), fileName, Convert.ToInt32(r["GenderId"]), true, false);

                UserProfile p = new UserProfile()
                {
                    FirstName = (string)r["FirstName"],
                    LastName = (string)r["LastName"],
                    UserRegion = (string)r["Region"],
                    UserCity = (string)r["City"],
                    UserAge = Convert.ToString(ProfileHelper.GetCurrentAge((DateTime)r["Birthday"])),
                    ProfileId = Convert.ToString(r["ProfileID"]),
                    UserName = (string)r["LoginName"],
                    DateRegister = ((DateTime)r["DateTimeToRegister"]).ToShortDateString(),
                    ImageUrl = path,
                    GenderId = Convert.ToString(r["GenderId"])

                };

                list.Profiles.Add(p);
            }

             * */

            return list;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "viewprofile/{myprofileid}/{viewprofileid}")]
        public string viewProfile(string myprofileid, string viewprofileid) {
            DataHelpers.UpdateEUS_Profiles_Activity(Convert.ToInt32(myprofileid), getClientIP(), true);

            clsUserDoes.MarkAsViewed(Convert.ToInt32(viewprofileid), Convert.ToInt32(myprofileid));
            clsUserDoes.MarkDateOfferAsViewed(Convert.ToInt32(viewprofileid), Convert.ToInt32(myprofileid));

            return "ok";
        }


        private int getAvailableCredits(string profileid) {
            DataHelpers.UpdateEUS_Profiles_Activity(Convert.ToInt32(profileid), getClientIP(), true);

            int AvailableCredits = 0;
            string sqlProc = "EXEC [CustomerAvailableCredits] @CustomerId = " + profileid;
            DataTable dt = DataHelpers.GetDataTable(sqlProc);
            if (Convert.ToInt32(dt.Rows[0]["CreditsRecordsCount"]) > 0) {

                AvailableCredits = Convert.ToInt32(dt.Rows[0]["AvailableCredits"]);

            }
            return AvailableCredits;
        }

        /*
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "savephoto/{strFileName}/{profileid}/{photoLevel}")]
        public string SavePhoto(string strFileName, System.IO.Stream streamFileContent, string profileid, string photoLevel)
        {
            try
            {
                DataHelpers.UpdateEUS_Profiles_Activity(Convert.ToInt32(profileid));

                clsConfigValues config = new clsConfigValues();
                if (config.photos_max_approved <= DataHelpers.EUS_CustomerPhotos_GetVisiblePhotos(Convert.ToInt32(profileid)))
                {
                    return "error";
                }
                else
                {
                    return SavePostedFile(strFileName, streamFileContent, profileid, photoLevel);

                }

            }
            catch (Exception ex)
            {
                return ex.ToString();
            }

        }


        private string SavePostedFile(string strFileName, System.IO.Stream streamFileContent, string profileid, string photoLevel)
        {
            string returnMessage = "START UPLOADING";
            
            try
            {           

            string imgfilePath = "";
            string thumbfilePath = "";
            Guid _guid = Guid.NewGuid();
            string ext = strFileName;
            returnMessage += "ext: " + ext + "\n";
            ext = ext.Substring(ext.LastIndexOf(".") + 1);
            string filePath = _guid.ToString("N") + "." + ext;

            returnMessage += filePath + " start: ";

            clsConfigValues config = new clsConfigValues();

           
                string imgDir = config.image_server_path;
                returnMessage += imgDir + "\n";
                imgDir = imgDir.Remove(imgDir.LastIndexOf('/') - 1);
                imgDir = string.Format(imgDir, profileid);

                if (!System.IO.Directory.Exists(imgDir))
                {
                    System.IO.Directory.CreateDirectory(imgDir);
                    returnMessage += "Dir Created\n";
                }
                imgfilePath = Path.Combine(imgDir, filePath);
                returnMessage += "file path: " + imgfilePath;

                string thumbDir = config.image_thumb_server_path;
                thumbDir = thumbDir.Remove(thumbDir.LastIndexOf('/') - 1);
                thumbDir = string.Format(thumbDir, profileid);
                if (!System.IO.Directory.Exists(thumbDir))
                {
                    System.IO.Directory.CreateDirectory(thumbDir);
                }
                thumbfilePath = Path.Combine(thumbDir, filePath);


                using (Image original = Image.FromStream(streamFileContent))
                {
                    PhotoUtils.SaveToJpeg(original, imgfilePath);

                    // resize proportionaly
                    using (Image thumbnail = PhotoUtils.AutosizeImage(original, 250, 250))
                    {

                        // fill image to avoid streching
                        using (Image thumbnail2 = PhotoUtils.Inscribe(thumbnail, 250, 250))
                        {
                            PhotoUtils.SaveToJpeg(thumbnail2, thumbfilePath);
                            thumbnail2.Dispose();
                            returnMessage += " jpeg saved\n";
                        }

                        thumbnail.Dispose();
                    }
                    original.Dispose();
                }

                //Catch ex As Exception
                //    WebErrorMessageBox(Me, ex, "SavePostedFile failed (1).")
                //End Try


                //' save to database file reference
                //Try

                int displayLevel = Convert.ToInt32(photoLevel);


                //If (chkPrivePhoto.Checked) Then
                //    displayLevel = 1 'private
                //    chkPrivePhoto.Checked = False
                //End If

                DSMembers ds = DataHelpers.GetEUS_CustomerPhotos_ByProfileOrMirrorID(0);

                DSMembers.EUS_CustomerPhotosRow newRow = ds.EUS_CustomerPhotos.NewEUS_CustomerPhotosRow();
                newRow.CustomerPhotosID = -1;
                newRow.CustomerID = Convert.ToInt32(profileid);
                newRow.CheckedContextID = 1;
                newRow.DateTimeToUploading = DateTime.UtcNow;
                newRow.DisplayLevel = displayLevel;
                newRow.FileName = filePath;
                newRow.HasAproved = false;
                newRow.HasDeclined = false;
                newRow.IsDefault = false;
                newRow.IsAutoApproved = false;
                ds.EUS_CustomerPhotos.AddEUS_CustomerPhotosRow(newRow);

                DataHelpers.UpdateEUS_CustomerPhotos(ref ds);


                dynamic isAutoApproved = false;
                //photo auto approve
                try
                {

                    if ((config.auto_approve_photos == "1"))
                    {
                        isAutoApproved = true;
                        DataHelpers.ApprovePhoto((int)newRow.CustomerPhotosID, isAutoApproved);
                    }
                }
                catch (Exception ex)
                {
                    returnMessage += ex.ToString();
                }

                DataHelpers.UpdateEUS_Profiles_MirrorSetStatusUpdating(Convert.ToInt32(profileid), ProfileStatusEnum.Updating, ProfileModifiedEnum.UpdatingPhotos);
                clsUserNotifications.SendNotificationsToMembers_AboutNewMember(Convert.ToInt32(profileid), true);

                DSMembers dsmem = DataHelpers.GetEUS_Profiles_ByProfileOrMirrorID(Convert.ToInt32(profileid));

                DataRow mRow;
                foreach (DataRow r in dsmem.EUS_Profiles.Rows)
                {
                    if ((bool)r["IsMaster"] == false)
                    {
                        mRow = r;
                    }
                }

               

            }
            catch (Exception ex)
            {
                returnMessage += ex.ToString();
            }

            
            return returnMessage;
        }

        */

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "favorite/{myuserid}/{userid}")]
        public string Favorite(string myuserid, string userid) {
            DataHelpers.UpdateEUS_Profiles_Activity(Convert.ToInt32(myuserid), getClientIP(), true);

            clsUserDoes.MarkAsFavorite(Convert.ToInt32(userid), Convert.ToInt32(myuserid));
            return "ok";
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "unfavorite/{myuserid}/{userid}")]
        public string UnFavorite(string myuserid, string userid) {
            DataHelpers.UpdateEUS_Profiles_Activity(Convert.ToInt32(myuserid), getClientIP(), true);

            clsUserDoes.MarkAsUnfavorite(Convert.ToInt32(userid), Convert.ToInt32(myuserid));
            return "ok";
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "checkfavorite/{myuserid}/{userid}")]
        public string checkFavorite(string myuserid, string userid) {
            if (clsUserDoes.IsFavorited(Convert.ToInt32(userid), Convert.ToInt32(myuserid))) {
                return "true";
            }
            else {
                return "false";
            }
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "checkhasphotos/{userid}")]
        public string checkHasPhotos(string userid) {
            bool result = false;
            using (myContext) {
                var mp = (from p in myContext.EUS_Profiles
                          where p.ProfileID == Convert.ToInt32(userid) && p.IsMaster == true
                          select p.MirrorProfileID).FirstOrDefault();

                result = clsUserDoes.HasPhotos(Convert.ToInt32(userid), Convert.ToInt32(mp));
                myContext.Dispose();
            }

            if (result) {
                return "true";
            }
            else {
                return "false";
            }


        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "like/{myprofileid}/{otherprofileid}")]
        public string like(string myprofileid, string otherprofileid) {
            DataHelpers.UpdateEUS_Profiles_Activity(Convert.ToInt32(myprofileid), getClientIP(), true);

            string result = "";

            bool res = false;
            using (myContext) {
                var mp = (from p in myContext.EUS_Profiles
                          where p.ProfileID == Convert.ToInt32(myprofileid) && p.IsMaster == true
                          select p.MirrorProfileID).FirstOrDefault();

                res = clsUserDoes.HasPhotos(Convert.ToInt32(myprofileid), Convert.ToInt32(mp));
                myContext.Dispose();
            }

            bool currentUserHasPhotos = res;

            if ((currentUserHasPhotos)) {

                clsUserDoes.SendWink(Convert.ToInt32(otherprofileid), Convert.ToInt32(myprofileid));
                sendNotificationToUser(myprofileid, otherprofileid, "2");
                result = "done";
            }
            else {
                result = "nophotos";
            }
            return result;

        }


        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "poke/{myprofileid}/{otherprofileid}/{parentOffer}")]
        public string poke(string myprofileid, string otherprofileid, string parentOffer) {
            DataHelpers.UpdateEUS_Profiles_Activity(Convert.ToInt32(myprofileid), getClientIP(), true);

            string result = "";

            bool res = false;
            using (myContext) {
                var mp = (from p in myContext.EUS_Profiles
                          where p.ProfileID == Convert.ToInt32(myprofileid) && p.IsMaster == true
                          select p.MirrorProfileID).FirstOrDefault();

                res = clsUserDoes.HasPhotos(Convert.ToInt32(myprofileid), Convert.ToInt32(mp));
                myContext.Dispose();
            }

            bool currentUserHasPhotos = res; ;

            if ((currentUserHasPhotos)) {
                EUS_Offer offerRec = clsUserDoes.GetMyPendingPoke(Convert.ToInt32(otherprofileid), Convert.ToInt32(myprofileid));

                if (offerRec == null) {

                    clsUserDoes.NewOfferParameters parms = new clsUserDoes.NewOfferParameters();
                    parms.userIdWhoDid = Convert.ToInt32(myprofileid);
                    parms.userIdReceiver = -1;
                    parms.offerAmount = -1;
                    parms.parentOfferId = Convert.ToInt32(parentOffer);
                    parms.messageText1 = "";
                    parms.messageText2 = "";

                    clsUserDoes.SendPoke(parms);
                    clsUserDoes.AcceptLike(parms, OfferStatusEnum.LIKE_ACCEEPTED_WITH_POKE);
                    result = "done";
                }
                else {
                    result = "already";
                }
            }
            else {
                result = "nophotos";
            }
            return result;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "checklike/{myprofileid}/{otherprofileid}")]
        public string checkLike(string myprofileid, string otherprofileid) {
            bool hasCommunication = clsUserDoes.HasCommunication(Convert.ToInt32(otherprofileid), Convert.ToInt32(myprofileid));
            bool IsAnyMessageExchanged = clsUserDoes.IsAnyMessageExchanged(Convert.ToInt32(myprofileid), Convert.ToInt32(myprofileid));
            bool hasAnyOffer = clsUserDoes.HasAnyOffer(Convert.ToInt32(otherprofileid), Convert.ToInt32(myprofileid));

            if (!hasCommunication && !IsAnyMessageExchanged && !hasAnyOffer) {
                return "true";
            }
            else {
                return "false";
            }
        }


        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "whofavme/{userid}/{zip}/{lat}/{lng}")]
        public UserProfilesList getWhoFavoritedMe(string userid, string zip, string lat, string lng) {
            DataHelpers.UpdateEUS_Profiles_Activity(Convert.ToInt32(userid), getClientIP(), true);

            UserProfilesList list = new UserProfilesList();
            list.Profiles = new List<UserProfile>();

            clsMyListsHelperParameters parms = new clsMyListsHelperParameters();
            parms.CurrentProfileId = Convert.ToInt32(userid);
            parms.sorting = MyListsSortEnum.Recent;
            parms.ReturnRecordsWithStatus = (int)ProfileStatusEnum.Approved;
            parms.zipstr = zip;
            parms.latitudeIn = Convert.ToDouble(lat);
            parms.longitudeIn = Convert.ToDouble(lng);
            parms.Distance = clsSearchHelper.DISTANCE_DEFAULT;

            parms.performCount = true;
            parms.rowNumberMin = 0;
            parms.rowNumberMax = 0;

            int count = 0;

            DataSet dt = clsSearchHelper.GetWhoFavoritedMeMembersDataTable(parms);

            if (dt != null && dt.Tables[0] != null) {
                count = (int)dt.Tables[0].Rows[0][0];

                parms.performCount = false;
                parms.rowNumberMin = 0;
                parms.rowNumberMax = count;

                dt = clsSearchHelper.GetWhoFavoritedMeMembersDataTable(parms);

                if (dt != null) {
                    list = createUserProfilesListfromDataset(dt);
                }
            }

            /*

            UserProfilesList list = new UserProfilesList();
            list.Profiles = new List<UserProfile>();
            foreach (DataRow r in dt.Rows)
            {
                string fileName = "";
                DSMembers.EUS_CustomerPhotosRow photo = DataHelpers.GetProfilesDefaultPhoto(Convert.ToInt32(r["ProfileID"]));
                if (photo != null)
                    fileName = photo.FileName;
                //string path = ProfileHelper.GetProfileImageURL(Convert.ToInt32(r["ProfileID"]), fileName, Convert.ToInt32(r["GenderId"]), true, false);
                string path = getImagePath(Convert.ToInt32(r["ProfileID"]), fileName, Convert.ToInt32(r["GenderId"]), true, false);

                UserProfile p = new UserProfile()
                {
                    FirstName = (string)r["FirstName"],
                    LastName = (string)r["LastName"],
                    UserRegion = (string)r["Region"],
                    UserCity = (string)r["City"],
                    UserAge = Convert.ToString(ProfileHelper.GetCurrentAge((DateTime)r["Birthday"])),
                    ProfileId = Convert.ToString(r["ProfileID"]),
                    UserName = (string)r["LoginName"],
                    DateRegister = ((DateTime)r["DateTimeToRegister"]).ToShortDateString(),
                    ImageUrl = path,
                    GenderId = Convert.ToString(r["GenderId"])

                };

                list.Profiles.Add(p);
            }
             * 
             * */

            return list;
        }


        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "likes/{mod}/{userid}/{zip}/{lat}/{lng}/{indexstart}")]
        public OfferList getLikes(string mod, string userid, string zip, string lat, string lng, string indexstart) {
            DataHelpers.UpdateEUS_Profiles_Activity(Convert.ToInt32(userid), getClientIP(), true);

            int mode = Convert.ToInt32(mod);
            DataSet ds = null;
            OfferList offerList = new OfferList();
            offerList.offers = new List<Offer>();


            clsWinksHelperParameters parms = new clsWinksHelperParameters();
            parms.CurrentProfileId = Convert.ToInt32(userid);
            parms.sorting = OffersSortEnum.RecentOffers;
            parms.ReturnRecordsWithStatus = (int)ProfileStatusEnum.Approved;
            parms.zipstr = zip;
            parms.latitudeIn = Convert.ToDouble(lat);
            parms.longitudeIn = Convert.ToDouble(lng);
            parms.Distance = clsSearchHelper.DISTANCE_DEFAULT;

            //parms.NumberOfRecordsToReturn = NumberOfRecordsToReturn;

            parms.performCount = false;
            parms.rowNumberMin = Convert.ToInt32(indexstart);
            parms.rowNumberMax = Convert.ToInt32(indexstart) + 20;


            switch (mode) {
                case 0:
                    ds = clsSearchHelper.GetWinksDataTable(ref parms);

                    break;

                case 1:
                    ds = clsSearchHelper.GetPendingLikesDataTable(ref parms);
                    break;

                case 2:
                    ds = clsSearchHelper.GetAcceptedLikesDataTable(ref parms);
                    break;

                case 3:
                    ds = clsSearchHelper.GetRejectedLikesDataTable(ref parms);
                    break;
            }

            DataTable dt = ds.Tables[0];
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++) {
                DataRow dr = ds.Tables[0].Rows[i];

                string fileName = "";
                DSMembers.EUS_CustomerPhotosRow photo = DataHelpers.GetProfilesDefaultPhoto(Convert.ToInt32(dr["ProfileID"]));
                if (photo != null)
                    fileName = photo.FileName;
                //string path = ProfileHelper.GetProfileImageURL(Convert.ToInt32(r["ProfileID"]), fileName, Convert.ToInt32(r["GenderId"]), true, false);
                string path = getImagePath(Convert.ToInt32(dr["ProfileID"]), fileName, Convert.ToInt32(dr["GenderId"]), true, false);

                Offer offer = new Offer();
                UserProfile p = new UserProfile()
                {
                    FirstName = (string)dr["FirstName"],
                    LastName = (string)dr["LastName"],
                    UserRegion = (string)dr["Region"],
                    UserCity = (string)dr["City"],
                    UserAge = Convert.ToString(ProfileHelper.GetCurrentAge((DateTime)dr["Birthday"])),
                    ProfileId = Convert.ToString(dr["ProfileID"]),
                    UserName = (string)dr["LoginName"],
                    DateRegister = ((DateTime)dr["DateTimeToRegister"]).ToShortDateString(),
                    ImageUrl = path,
                    GenderId = Convert.ToString(dr["GenderId"]),
                    Latitude = (dt.Columns.Contains("latitude") ? Convert.ToString(dr["latitude"]) : null),
                    Longtitude = (dt.Columns.Contains("longitude") ? Convert.ToString(dr["longitude"]) : null),
                };

                offer.profile = p;
                offer.offerId = Convert.ToString(dr["OfferID"]);

                offerList.offers.Add(offer);
            }



            /*

                        if (dt != null)
                        {
                            int start = Convert.ToInt32(indexstart);
                            int results = 20;
                            int max;
                            if (start + results < dt.Rows.Count)
                            {
                                max = start + results;
                            }
                            else
                            {
                                max = dt.Rows.Count;
                            }
                            for (int i = start; i < max; i++)
                            {
                                DataRow r = dt.Rows[i];

                                string fileName = "";
                                DSMembers.EUS_CustomerPhotosRow photo = DataHelpers.GetProfilesDefaultPhoto(Convert.ToInt32(r["ProfileID"]));
                                if (photo != null)
                                    fileName = photo.FileName;
                                //string path = ProfileHelper.GetProfileImageURL(Convert.ToInt32(r["ProfileID"]), fileName, Convert.ToInt32(r["GenderId"]), true, false);
                                string path = getImagePath(Convert.ToInt32(r["ProfileID"]), fileName, Convert.ToInt32(r["GenderId"]), true, false);

                                Offer offer = new Offer();
                                UserProfile p = new UserProfile()
                                {
                                    FirstName = (string)r["FirstName"],
                                    LastName = (string)r["LastName"],
                                    UserRegion = (string)r["Region"],
                                    UserCity = (string)r["City"],
                                    UserAge = Convert.ToString(ProfileHelper.GetCurrentAge((DateTime)r["Birthday"])),
                                    ProfileId = Convert.ToString(r["ProfileID"]),
                                    UserName = (string)r["LoginName"],
                                    DateRegister = ((DateTime)r["DateTimeToRegister"]).ToShortDateString(),
                                    ImageUrl = path,
                                    GenderId = Convert.ToString(r["GenderId"])

                                };

                                offer.profile = p;
                                offer.offerId = Convert.ToString(r["OfferID"]);

                                offerList.offers.Add(offer);
                            }

                        }

                         * */

            return offerList;
        }


        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "likeactions/{userid}/{action}/{offerid}")]
        public string likeActions(string userid, string action, string offerid) {
            DataHelpers.UpdateEUS_Profiles_Activity(Convert.ToInt32(userid), getClientIP(), true);

            int action_num = Convert.ToInt32(action);
            switch (action_num) {
                //cancel pending
                case 1:
                    clsUserDoes.CancelWinkOrOffer(Convert.ToInt32(offerid));
                    return "ok";
                //delete
                case 2:
                    clsUserDoes.DeleteOfferAndParents(Convert.ToInt32(offerid));
                    return "ok";
            }

            return "error";
        }


        [WebInvoke(Method = "GET", UriTemplate = "rejectlike/{offerid}/{reason}")]
        public void rejectLike(string offerid, string reason) {
            int reasonInt = Convert.ToInt32(reason);
            OfferControlCommandEnum reasonEnum = OfferControlCommandEnum.REJECTTYPE;
            switch (reasonInt) {
                case 5:
                    reasonEnum = OfferControlCommandEnum.REJECTTYPE;
                    break;
                case 6:
                    reasonEnum = OfferControlCommandEnum.REJECTFAR;
                    break;

                case 7:
                    reasonEnum = OfferControlCommandEnum.REJECTBAD;
                    break;

                case 8:
                    reasonEnum = OfferControlCommandEnum.REJECTEXPECTATIONS;
                    break;

            }

            clsUserDoes.RejectOffer(Convert.ToInt32(offerid), reasonEnum.ToString());
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "offers/{mod}/{userid}/{zip}/{lat}/{lng}/{indexstart}")]
        public OfferList getOffers(string mod, string userid, string zip, string lat, string lng, string indexstart) {
            DataHelpers.UpdateEUS_Profiles_Activity(Convert.ToInt32(userid), getClientIP(), true);

            int mode = Convert.ToInt32(mod);
            DataTable dt = null;
            OfferList offerList = new OfferList();
            offerList.offers = new List<Offer>();
            clsWinksHelperParameters parms = new clsWinksHelperParameters();
            parms.CurrentProfileId = Convert.ToInt32(userid);
            parms.sorting = OffersSortEnum.RecentOffers;
            parms.ReturnRecordsWithStatus = 4;
            parms.zipstr = zip;
            parms.latitudeIn = Convert.ToDouble(lat);
            parms.longitudeIn = Convert.ToDouble(lng);
            parms.Distance = clsSearchHelper.DISTANCE_DEFAULT;
            //parms.NumberOfRecordsToReturn = NumberOfRecordsToReturn
            parms.performCount = true;
            parms.rowNumberMin = Convert.ToInt32(indexstart);
            parms.rowNumberMax = Convert.ToInt32(indexstart) + 20;
            switch (mode) {   //new

                case 0:

                    dt = clsSearchHelper.GetNewOffersDataTable(ref parms).Tables[1];
                    //pending
                    break;

                case 1:
                    dt = clsSearchHelper.GetPendingOffersDataTable(ref parms).Tables[1];
                    break;

                //accepted
                case 2:
                    dt = clsSearchHelper.GetAcceptedOffersDataTable(ref parms).Tables[1];
                    break;
                //rejected
                case 3:
                    dt = clsSearchHelper.GetRejectedOffersDataTable(ref parms).Tables[1];
                    break;
            }

            if (dt != null) {

                int start = Convert.ToInt32(indexstart);
                int results = 20;
                int max;
                if (start + results < dt.Rows.Count) {
                    max = start + results;
                }
                else {
                    max = dt.Rows.Count;
                }

                for (int i = start; i < max; i++) {
                    DataRow dr = dt.Rows[i];
                    string fileName = "";
                    DSMembers.EUS_CustomerPhotosRow photo = DataHelpers.GetProfilesDefaultPhoto(Convert.ToInt32(dr["ProfileID"]));
                    if (photo != null)
                        fileName = photo.FileName;
                    //string path = ProfileHelper.GetProfileImageURL(Convert.ToInt32(r["ProfileID"]), fileName, Convert.ToInt32(r["GenderId"]), true, false);
                    string path = getImagePath(Convert.ToInt32(dr["ProfileID"]), fileName, Convert.ToInt32(dr["GenderId"]), true, false);

                    Offer offer = new Offer();
                    UserProfile p = new UserProfile()
                    {
                        FirstName = (string)dr["FirstName"],
                        LastName = (string)dr["LastName"],
                        UserRegion = (string)dr["Region"],
                        UserCity = (string)dr["City"],
                        UserAge = Convert.ToString(ProfileHelper.GetCurrentAge((DateTime)dr["Birthday"])),
                        ProfileId = Convert.ToString(dr["ProfileID"]),
                        UserName = (string)dr["LoginName"],
                        DateRegister = ((DateTime)dr["DateTimeToRegister"]).ToShortDateString(),
                        ImageUrl = path,
                        GenderId = Convert.ToString(dr["GenderId"]),
                        Latitude = (dt.Columns.Contains("latitude") ? Convert.ToString(dr["latitude"]) : null),
                        Longtitude = (dt.Columns.Contains("longitude") ? Convert.ToString(dr["longitude"]) : null),
                    };

                    offer.profile = p;
                    offer.offerId = Convert.ToString(dr["OfferID"]);
                    offer.rejectReason = Convert.ToString(dr["OffersStatusID"]);

                    offerList.offers.Add(offer);
                }

            }

            return offerList;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "makeoffer/{mode}/{amount}/{fromprofileid}/{toprofileid}/{genderid}/{offerid}")]
        public string makeOffer(string mode, string amount, string fromprofileid, string toprofileid, string genderid, string offerid) {
            DataHelpers.UpdateEUS_Profiles_Activity(Convert.ToInt32(fromprofileid), getClientIP(), true);

            try {
                int requestOfferId = Convert.ToInt32(offerid);

                clsUserDoes.NewOfferParameters parms = new clsUserDoes.NewOfferParameters();
                parms.messageText1 = "";
                parms.messageText2 = "";
                parms.offerAmount = Convert.ToInt32(amount);
                parms.userIdReceiver = Convert.ToInt32(toprofileid);
                parms.userIdWhoDid = Convert.ToInt32(fromprofileid);
                parms.parentOfferId = Convert.ToInt32(offerid);

                EUS_Offer offer = clsUserDoes.GetLastOfferAny(Convert.ToInt32(toprofileid), Convert.ToInt32(fromprofileid));

                int modeInt = Convert.ToInt32(mode);
                if (modeInt == 1)//new offer
                {

                    if (offer != null) {
                        if ((offer.OfferTypeID != ProfileHelper.OfferTypeID_OFFERNEW && offer.OfferTypeID != ProfileHelper.OfferTypeID_OFFERCOUNTER)) {

                            parms.parentOfferId = Convert.ToInt32(offer.OfferID);
                            int createdOfferId = parms.childOfferId = clsUserDoes.MakeOffer(parms);
                            if (offer.OfferTypeID == ProfileHelper.OfferTypeID_WINK) {
                                clsUserDoes.AcceptLike(parms, OfferStatusEnum.LIKE_ACCEEPTED_WITH_OFFER);
                            }
                            else if (offer.OfferTypeID == ProfileHelper.OfferTypeID_POKE) {
                                clsUserDoes.AcceptPoke(parms, OfferStatusEnum.POKE_ACCEEPTED_WITH_OFFER);

                            }

                            string notifResult = sendNotificationToUser(fromprofileid, toprofileid, "1");
                            return "new offer created: last offer id: " + offer.OfferID.ToString() + ", new offer id:" + createdOfferId.ToString();
                        }
                        else {
                            return "offer already";
                        }
                    }
                    else {
                        int createdOfferId = parms.childOfferId = clsUserDoes.MakeOffer(parms);
                        return "new offer created - last offer:null , new offer id:" + createdOfferId.ToString();

                    }

                } //counter offer              
                else if (modeInt == 2) {
                    if (offer != null) {
                        clsUserDoes.CounterOffer(Convert.ToInt32(offer.OfferID), Convert.ToInt32(amount));
                        return "counter offer created - last offer id: " + offer.OfferID.ToString();
                    }
                }

            }
            catch (Exception ex) {
                return "ERROR:\n" + ex.ToString();
            }

            return "";

        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "acceptoffer/{myprofileid}/{offerid}")]
        public string acceptOffer(string myprofileid, string offerid) {
            DataHelpers.UpdateEUS_Profiles_Activity(Convert.ToInt32(myprofileid), getClientIP(), true);

            try {
                int id = Convert.ToInt32(offerid);
                clsUserDoes.AcceptOffer(id, OfferStatusEnum.ACCEPTED);

                DataHelpers.UpdateEUS_Profiles_Activity(Convert.ToInt32(myprofileid), getClientIP(), true);

                return "ok";

            }
            catch (Exception) {
                return "Error";
            }
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "hash/{username}")]
        public string getHash(string username) {
            return AppUtils.MD5_GetString(username + "5686-android");
        }


        public long getLastOfferId(string fromprofile, string toprofile) {
            EUS_Offer offer = clsUserDoes.GetLastOfferAny(Convert.ToInt32(toprofile), Convert.ToInt32(fromprofile));
            if (offer != null) {
                return offer.OfferID;
            }
            else {
                return -1;
            }

        }

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "register")]
        public string register(RegisterData data) {

            string returnMessage = "";
            DSMembers ds = new DSMembers();
            try {
             
                Dating.Server.Datasets.DLL.DSMembers.EUS_ProfilesRow profile = ds.EUS_Profiles.NewEUS_ProfilesRow();


                if (DataHelpers.EUS_Profile_CheckEmail(data.email)) {
                    returnMessage += "EmailAlready";
                }
                else if (DataHelpers.EUS_Profile_CheckLoginName(data.username)) {
                    returnMessage += "LoginNameAlready";
                }
                else {

                    clsCountryByIP country = clsCountryByIP.GetCountryCodeFromIP(getClientIP(), CurrentSettings.ConnectionString_GEODB);

                    using (EUS_ProfilesTableAdapter adapter = new EUS_ProfilesTableAdapter())
                    {
                        using (SqlConnection con=new SqlConnection(CurrentSettings.ConnectionString)){
                            adapter.Connection = con;

                            RegisterHelper helper = new RegisterHelper();

                            helper.cbAgreements = true;
                            helper.GenderId = Convert.ToInt32(data.gender);
                            helper.Email = data.email;
                            helper.Login = data.username;
                            helper.Password = data.password;
                            helper.Birthday = DateTime.Parse(data.birthday);
                            helper.Country = data.country; //to add country code here
                            helper.Region = data.region;
                            helper.City = data.city;
                            helper.Zip = getZipCode(data.country, data.region, data.city, data.lag);
                            helper.Lag = data.lag;

                            helper.Height = "-1";
                            helper.BodyType = "-1";
                            helper.EyeColor = "-1";
                            helper.HairClr = "-1";

                            helper.GEO_COUNTRY_CODE = country.CountryCode;
                            helper.REMOTE_ADDR = getClientIP();
                            helper.HTTP_USER_AGENT = "";
                            helper.HTTP_REFERER = "";
                            helper.CustomReferrer = "googleplay";
                            helper.LandingPage = "";
                            helper.SearchEngineKeywords = "";


                            helper.CreateNewProfile(ref profile);

                            ds.EUS_Profiles.AddEUS_ProfilesRow(profile);
                            adapter.Update(ds);
                        }
                    }
                 


                    bool isAutoApproveOn = false;
                    try {
                        DataTable dt = DataHelpers.GetDataTable("select ISNULL(ConfigValue,0) as ConfigValue from SYS_Config where ConfigName='auto_approve_new_profiles'");
                        if ((dt.Rows.Count > 0 && (string)dt.Rows[0]["ConfigValue"] == "1")) {
                            isAutoApproveOn = true;
                        }
                    }
                    catch (Exception ex) {
                        returnMessage += ex.ToString();
                    }

                    if ((isAutoApproveOn)) {
                        try {
                            AdminActions.ApproveProfile(ref ds, true);
                        }
                        catch (Exception ex) {
                            returnMessage += ex.ToString();
                        }
                        try {
                            clsUserNotifications.SendEmailNotification_OnAdminApprove(profile, data.lag);
                        }
                        catch (Exception ex) {
                            returnMessage += ex.ToString();
                        }
                    }


                }


                try {
                    if (profile.IsMirrorProfileIDNull()) profile.MirrorProfileID = 0;
                    clsProfilesPrivacySettings.CreateNew_EUS_ProfilesPrivacySetting(profile.ProfileID, profile.MirrorProfileID);
                }
                catch (Exception ex) {
                    WebErrorSendEmail(ex, "CreateNew_EUS_ProfilesPrivacySetting (handled)");
                    if (CurrentSettings.AllowSendExceptions)
                        returnMessage += ex.ToString();
                }


                if (ProfileHelper.IsMale(Convert.ToInt32(data.gender)) && (data.country == "GR" )) {
                    try {
                        clsCustomer.AddFreeRegistrationCreditsGreek(ref profile, getClientIP(), "", "");
                    }
                    catch (Exception ex) {
                        WebErrorSendEmail(ex, "AddFreeRegistrationCreditsGreek (handled). profile object is null:" + (profile == null).ToString());
                        if (CurrentSettings.AllowSendExceptions)
                            returnMessage += ex.ToString();
                    }
                }


                if (profile != null && profile.LoginName != null && profile.ProfileID > 1) {
                    DataHelpers.LogProfileAccess(Convert.ToInt32(profile.ProfileID), profile.LoginName, profile.Password, true, "Register", "Registration from device", "", getClientIP(), true, "Service1-register");
                }


            }
            catch (Exception ex) {
                WebErrorSendEmail(ex, "wcf-register");
                if (CurrentSettings.AllowSendExceptions)
                    returnMessage += ex.ToString();
            }
            finally
            {
                ds.Dispose();
            }



            returnMessage += "ok";
            return returnMessage;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "countries")]
        public CountryList getCountries() {
            CountryList list = new CountryList();
            list.countries = new List<Country>();

            DataTable dt = SYS_CountriesGEOHelper.gDSListsGEO__IsEnabled_PrintableNameASC.SYS_CountriesGEO;

            var q = (from row in dt.AsEnumerable()
                     select new
                     {

                         name = (string)row["PrintableName"],
                         iso = (string)row["Iso"]

                     }).ToList();

            for (int i = 0; i < q.Count; i++) {
                Country country = new Country()
                {
                    name = q[i].name,
                    iso = q[i].iso
                };
                list.countries.Add(country);
            }

            return list;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "regions/{countryIso}/{lagid}")]
        public Regions getCountryRegions(string countryIso, string lagid) {
            DataTable dt = clsGeoHelper.GetCountryRegions(countryIso, lagid);
            Regions regions = new Regions();
            regions.list = new List<string>();

            foreach (DataRow r in dt.AsEnumerable()) {
                regions.list.Add((string)r["region1"]);

            }

            return regions;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "cities/{countryIso}/{region}/{lagid}")]
        public Cities getRegionCities(string countryIso, string region, string lagid) {
            string regionDec = HttpUtility.UrlDecode(region);

            DataTable dt = clsGeoHelper.GetCountryRegionCities(countryIso, regionDec, lagid);
            Cities cities = new Cities();
            cities.list = new List<string>();

            foreach (DataRow r in dt.AsEnumerable()) {
                cities.list.Add((string)r["city"]);
            }

            return cities;
        }


        public string getZipCode(string country, string region, string city, string lag) {
            string value = "0000";
            DataTable dt = clsGeoHelper.GetCountryCityPostalcodes(country, region, city, lag);
            if (dt.Rows.Count > 0) {
                value = (string)dt.Rows[0]["postcode"];
            }
            return value;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "defaultphoto/{myprofileid}/{photoid}")]
        public string setDefaultPhoto(string myprofileid, string photoid) {
            try {
                int iPhotoId = Convert.ToInt32(photoid);
                int iMyprofileid = Convert.ToInt32(myprofileid);

                long CustomerPhotosID = DataHelpers.EUS_CustomerPhotos_SetDefault(iMyprofileid, iPhotoId, true, false);
                if (CustomerPhotosID > 0) {
                    clsCustomer.AddFreeRegistrationCredits_WithCheck(iMyprofileid, "", "", "");
                }

                //DSMembers ds = DataHelpers.GetEUS_CustomerPhotos_ByProfileOrMirrorID(Convert.ToInt32(myprofileid));
                //foreach (DSMembers.EUS_CustomerPhotosRow rawRecord in ds.EUS_CustomerPhotos.Rows) {
                //    if ((rawRecord.CustomerPhotosID == photoId && rawRecord.HasAproved == true)) {
                //        rawRecord.IsDefault = true;

                //    }
                //    else {
                //        rawRecord.IsDefault = false;
                //    }
                //}
                //DataHelpers.UpdateEUS_CustomerPhotos(ref ds);
                //ds.Dispose();
            }
            catch (Exception) {
                return "error";
            }
            return "ok";
        }


        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "send_notifications/{message}")]
        public override string sendNotificationsToAllUsers(string message) {
            return base.sendNotificationsToAllUsers(message);

        }

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "notification_tolist")]
        public string sendNotificationToUserList(NotificationParams p) {
            string response = "";
            try {

                using (myContext) {
                    for (int i = 0; i < p.deviceIds.Count; i++) {
                        string deviceId = p.deviceIds[i];
                        bool IsActiveMember = DataHelpers.EUS_Profile_IsProfileActiveByDeviceID(deviceId);
                        if (IsActiveMember) {
                            response += SendNotification(deviceId, p.message);
                        }
                    }

                    myContext.Dispose();
                }

            }
            catch (Exception e) {
                response += "exception: " + e.ToString();
            }
            return response;
        }


        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "notification/{fromProfileId}/{toProfileId}/{type}")]
        public override string sendNotificationToUser(string fromProfileId, string toProfileId, string type) {
            return base.sendNotificationToUser(fromProfileId, toProfileId, type);
        }


        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "storeGCMUser")]
        public string storeGCMUser(StoreUserParams p) {
            try {
                using (myContext) {


                    EUS_GCMProfile profile = new EUS_GCMProfile()
                    {
                        ProfileID = Convert.ToInt32(p.profileId),
                        GCM_RegID = p.regId,
                        LoginName = p.loginname,
                        Email = p.email,
                        CreatedDate = DateTime.Now,
                        Notif_Messages = 1,
                        Notif_Likes = 1,
                        Notif_Offers = 1
                    };
                    myContext.EUS_GCMProfiles.InsertOnSubmit(profile);
                    myContext.SubmitChanges();

                    myContext.Dispose();

                }

                return "OK";

            }
            catch (Exception e) {
                return e.ToString();
            }

        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "removeGCMUser/{regId}")]
        public string removeGCMUser(string regId) {
            string msg = "";

            try {

                using (myContext) {
                    EUS_GCMProfile profile = (from p in myContext.EUS_GCMProfiles
                                              where p.GCM_RegID == regId
                                              select p).FirstOrDefault();

                    if (profile != null) {
                        myContext.EUS_GCMProfiles.DeleteOnSubmit(profile);
                        myContext.SubmitChanges();
                        msg += "ok";
                    }
                    else {
                        msg += "not found";
                    }

                    myContext.Dispose();

                }

            }
            catch (Exception e) {
                msg += "error: " + e.ToString();
            }
            return msg;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "gcmusers")]
        public List<ProfileDeviceId> getGCMUsers() {
            List<ProfileDeviceId> pList = new List<ProfileDeviceId>();

            using (myContext) {

                List<EUS_GCMProfile> profiles = (from p in myContext.EUS_GCMProfiles
                                                 where p.GCM_RegID != null
                                                 select p).ToList();


                for (int i = 0; i < profiles.Count; i++) {
                    EUS_Profile q = (from p in myContext.EUS_Profiles
                                     where p.ProfileID == Convert.ToInt32(profiles[i].ProfileID) && p.Status == (int)ProfileStatusEnum.Approved
                                     select p).FirstOrDefault();

                    UserProfile prof = createProfile(q, "");

                    ProfileDeviceId item = new ProfileDeviceId();
                    item.deviceId = profiles[i].GCM_RegID;
                    item.profile = prof;
                    pList.Add(item);
                }

                myContext.Dispose();
            }

            return pList;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "savesettings/{profileId}/{notifMessages}/{notifLikes}/{notifOffers}")]
        public override string saveGCMUserSettings(string profileId, string notifMessages, string notifLikes, string notifOffers) {
            return base.saveGCMUserSettings(profileId, notifMessages, notifLikes, notifOffers);

        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "settings/{profileId}")]
        public GCMUserSettings getSettings(string profileId) {
            GCMUserSettings settings = new GCMUserSettings();

            using (myContext) {

                EUS_GCMProfile p = (from s in myContext.EUS_GCMProfiles
                                    where s.ProfileID == Convert.ToInt32(profileId)
                                    select s).FirstOrDefault();

                if (p != null) {

                    settings.profileId = Convert.ToString(p.ProfileID);
                    settings.notifMessages = Convert.ToString(p.Notif_Messages);
                    settings.notifLikes = Convert.ToString(p.Notif_Likes);
                    settings.notifOffers = Convert.ToString(p.Notif_Offers);

                }
                myContext.Dispose();
            }
            return settings;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "photolevel/{fromProfileId}/{toProfileId}/{photoLevel}")]
        public string setProfilePhotoLevel(string fromProfileId, string toProfileId, string photoLevel) {
            string result = "";
            try {
                using (myContext) {

                    EUS_ProfilePhotosLevel photosLevel = (from itm in myContext.EUS_ProfilePhotosLevels
                                                          where itm.FromProfileID == Convert.ToInt32(fromProfileId)
                                                          && itm.ToProfileID == Convert.ToInt32(toProfileId)
                                                          select itm).FirstOrDefault();

                    if (photosLevel != null) {
                        photosLevel.PhotoLevelID = Convert.ToInt32(photoLevel);
                    }
                    else {
                        photosLevel = new EUS_ProfilePhotosLevel();
                        photosLevel.FromProfileID = Convert.ToInt32(fromProfileId);
                        photosLevel.ToProfileID = Convert.ToInt32(toProfileId);
                        photosLevel.DateTimeCreated = DateTime.UtcNow;
                        photosLevel.PhotoLevelID = Convert.ToInt32(photoLevel);
                        myContext.EUS_ProfilePhotosLevels.InsertOnSubmit(photosLevel);

                    }

                    myContext.SubmitChanges();
                    result = "OK";
                    //result = ""+photosLevel.PhotoLevelID.Value;

                    myContext.Dispose();
                }
            }
            catch (Exception e) {
                result = "error" + e.ToString();
            }
            return result;
        }


        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "getPhotoLevel/{fromProfileId}/{toProfileId}")]
        public override string getProfilePhotoLevel(string fromProfileId, string toProfileId) {
            return base.getProfilePhotoLevel(fromProfileId, toProfileId);
            //string result = "";
            //try {
            //    using (myContext) {
            //        EUS_ProfilePhotosLevel photosLevel = (from itm in myContext.EUS_ProfilePhotosLevels
            //                                              where itm.FromProfileID == Convert.ToInt32(fromProfileId)
            //                                              && itm.ToProfileID == Convert.ToInt32(toProfileId)
            //                                              select itm).FirstOrDefault();

            //        if (photosLevel != null) {
            //            result = Convert.ToString(photosLevel.PhotoLevelID);
            //        }
            //        else {
            //            result = "0";
            //        }

            //        myContext.Dispose();
            //    }
            //}
            //catch (Exception) {
            //    result = "error";
            //}
            //return result;
        }


        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "whoSharedMePhotos/{userid}/{zip}/{lat}/{lng}/{indexstart}")]
        public UserProfilesList getWhoSharedMePhotos(string userid, string zip, string lat, string lng, string indexstart) {
            DataHelpers.UpdateEUS_Profiles_Activity(Convert.ToInt32(userid), getClientIP(), true);

            UserProfilesList list = new UserProfilesList();
            list.Profiles = new List<UserProfile>();

            clsMyListsHelperParameters parms = new clsMyListsHelperParameters();
            parms.CurrentProfileId = Convert.ToInt32(userid);
            parms.sorting = MyListsSortEnum.Recent;
            parms.ReturnRecordsWithStatus = (int)ProfileStatusEnum.Approved;
            parms.zipstr = zip;
            parms.latitudeIn = Convert.ToDouble(lat);
            parms.longitudeIn = Convert.ToDouble(lng);
            parms.Distance = clsSearchHelper.DISTANCE_DEFAULT;

            parms.performCount = false;
            parms.rowNumberMin = Convert.ToInt32(indexstart);
            parms.rowNumberMax = Convert.ToInt32(indexstart) + 20;


            DataSet dt = clsSearchHelper.GetWhoSharedPhotoMembersDataTable(parms);
            list = createUserProfilesListfromDataset(dt);

            /*

             DataTable dt = clsSearchHelper.GetWhoSharedPhotoMembersDataTable(MyListsSortEnum.Recent,
                                                                                          Convert.ToInt32(userid), 0,
                                                                                          4,
                                                                                          zip,
                                                                                          Convert.ToDouble(lat),
                                                                                          Convert.ToDouble(lng));
           

             int start = Convert.ToInt32(indexstart);
             int results = 20;
             int max;
             if (start + results < dt.Rows.Count)
             {
                 max = start + results;
             }
             else
             {
                 max = dt.Rows.Count;
             }
             for (int i = start; i < max; i++)
             {
                 DataRow r = dt.Rows[i];
                 string fileName = "";
                 DSMembers.EUS_CustomerPhotosRow photo = DataHelpers.GetProfilesDefaultPhoto(Convert.ToInt32(r["ProfileID"]));
                 if (photo != null)
                     fileName = photo.FileName;
                 //string path = ProfileHelper.GetProfileImageURL(Convert.ToInt32(r["ProfileID"]), fileName, Convert.ToInt32(r["GenderId"]), true, false);
                 string path = getImagePath(Convert.ToInt32(r["ProfileID"]), fileName, Convert.ToInt32(r["GenderId"]), true, false);

                 UserProfile p = new UserProfile()
                 {
                     FirstName = (string)r["FirstName"],
                     LastName = (string)r["LastName"],
                     UserRegion = (string)r["Region"],
                     UserCity = (string)r["City"],
                     UserAge = Convert.ToString(ProfileHelper.GetCurrentAge((DateTime)r["Birthday"])),
                     ProfileId = Convert.ToString(r["ProfileID"]),
                     UserName = (string)r["LoginName"],
                     DateRegister = ((DateTime)r["DateTimeToRegister"]).ToShortDateString(),
                     ImageUrl = path,
                     GenderId = Convert.ToString(r["GenderId"])

                 };

                 list.Profiles.Add(p);
             }
             
             * */


            return list;
        }


        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "getSharedByMePhotos/{userid}/{zip}/{lat}/{lng}/{indexstart}")]
        public UserProfilesList getSharedByMePhotos(string userid, string zip, string lat, string lng, string indexstart) {
            DataHelpers.UpdateEUS_Profiles_Activity(Convert.ToInt32(userid), getClientIP(), true);
            clsMyListsHelperParameters parms = new clsMyListsHelperParameters();
            parms.CurrentProfileId = Convert.ToInt32(userid);
            parms.sorting = MyListsSortEnum.Recent;
            parms.ReturnRecordsWithStatus = 4;
            parms.zipstr = zip;
            parms.latitudeIn = Convert.ToDouble(lat);
            parms.longitudeIn = Convert.ToDouble(lng);
            parms.Distance = clsSearchHelper.DISTANCE_DEFAULT;
            //parms.NumberOfRecordsToReturn = NumberOfRecordsToReturn
            parms.performCount = true;
            parms.rowNumberMin = Convert.ToInt32(indexstart);
            parms.rowNumberMax = Convert.ToInt32(indexstart) + 20;
            DataTable dt = clsSearchHelper.GetSharedPhotosByMeToMembers_DataTable(parms).Tables[1];

            UserProfilesList list = new UserProfilesList();
            list.Profiles = new List<UserProfile>();

            int start = Convert.ToInt32(indexstart);
            int results = 20;
            int max;
            if (start + results < dt.Rows.Count) {
                max = start + results;
            }
            else {
                max = dt.Rows.Count;
            }
            for (int i = start; i < max; i++) {
                DataRow dr = dt.Rows[i];
                string fileName = "";
                DSMembers.EUS_CustomerPhotosRow photo = DataHelpers.GetProfilesDefaultPhoto(Convert.ToInt32(dr["ProfileID"]));
                if (photo != null)
                    fileName = photo.FileName;
                //string path = ProfileHelper.GetProfileImageURL(Convert.ToInt32(r["ProfileID"]), fileName, Convert.ToInt32(r["GenderId"]), true, false);
                string path = getImagePath(Convert.ToInt32(dr["ProfileID"]), fileName, Convert.ToInt32(dr["GenderId"]), true, false);

                UserProfile p = new UserProfile()
                {
                    FirstName = (string)dr["FirstName"],
                    LastName = (string)dr["LastName"],
                    UserRegion = (string)dr["Region"],
                    UserCity = (string)dr["City"],
                    UserAge = Convert.ToString(ProfileHelper.GetCurrentAge((DateTime)dr["Birthday"])),
                    ProfileId = Convert.ToString(dr["ProfileID"]),
                    UserName = (string)dr["LoginName"],
                    DateRegister = ((DateTime)dr["DateTimeToRegister"]).ToShortDateString(),
                    ImageUrl = path,
                    GenderId = Convert.ToString(dr["GenderId"]),
                    Latitude = (dt.Columns.Contains("latitude") ? Convert.ToString(dr["latitude"]) : null),
                    Longtitude = (dt.Columns.Contains("longitude") ? Convert.ToString(dr["longitude"]) : null),
                };

                list.Profiles.Add(p);
            }

            return list;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "checkCountry/{userid1}/{userid2}")]
        public override string checkDifferentCountry(string userid1, string userid2) {
            return base.checkDifferentCountry(userid1, userid2);
            //try {

            //    using (myContext) {
            //        EUS_Profile p1 = (from p in myContext.EUS_Profiles
            //                          where p.ProfileID == Convert.ToInt32(userid1)
            //                          select p).FirstOrDefault();

            //        EUS_Profile p2 = (from p in myContext.EUS_Profiles
            //                          where p.ProfileID == Convert.ToInt32(userid2)
            //                          select p).FirstOrDefault();


            //        if (p1.Country == p2.Country) {
            //            return "true";
            //        }
            //        else {
            //            return "false";
            //        }
            //    }

            //}
            //catch (Exception) {
            //    return "error";
            //}
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "hasPhotos/{userid}")]
        public override string hasPhotos(string userid) {
            return base.hasPhotos(userid);

            //try {
            //    Dating.Server.Datasets.DLL.DSMembers.EUS_CustomerPhotosDataTable dtPhotos = DataHelpers.GetEUS_CustomerPhotos_ByProfileOrMirrorID(Convert.ToInt32(userid)).EUS_CustomerPhotos;
            //    if (dtPhotos.Rows.Count > 0) {
            //        return "true";
            //    }
            //    else {
            //        return "false";
            //    }

            //}
            //catch (Exception) {
            //    return "error";
            //}
        }

    }



}

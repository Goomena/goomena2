﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NotificationSender
{
    public class Profile
    {
        public string DateRegister { get; set; }
        public string FirstName { get; set; }
        public string GenderId { get; set; }
        public string ImageUrl { get; set; }
        public string LastName { get; set; }
        public string Latitude { get; set; }
        public string Longtitude { get; set; }
        public object Password { get; set; }
        public string ProfileId { get; set; }
        public string ReferrerParentId { get; set; }
        public object UserAge { get; set; }
        public string UserCity { get; set; }
        public string UserName { get; set; }
        public string UserRegion { get; set; }
        public string ZipCode { get; set; }
    }

    public class RootObject
    {
        public string deviceId { get; set; }
        public Profile profile { get; set; }
    }
}

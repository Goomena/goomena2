﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using System.IO;

namespace NotificationSender
{
    public partial class Form1 : Form
    {

        string base_address = "http://188.165.13.64/android/GoomenaLib.Service1.svc/json/";
        
        public Form1()
        {
            InitializeComponent();

                       
        }

        //send to all
        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "")
            {
                WebClient webClient = new WebClient();
                string result = webClient.DownloadString(base_address + "send_notifications/" + textBox1.Text);
                Debug.WriteLine("SERVER RESULT: " + result);
            }
            else
            {
                MessageBox.Show("Notification Message is empty!");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            WebClient webClient = new WebClient();
            string json = webClient.DownloadString(base_address + "gcmusers");

            List<RootObject> users = JsonConvert.DeserializeObject<List<RootObject>>(json);

            listView1.Items.Clear();

            for (int i = 0; i < users.Count; i++)
            {
                Profile user = users[i].profile;
                string gender = "";
                if(user.GenderId=="1"){
                    gender = "Male";
                }
                else{
                    gender = "Female";
                }
                string[] array = {Convert.ToString(i), Convert.ToString(user.ProfileId), user.UserName, gender,user.UserCity,user.UserRegion, users[i].deviceId};
                listView1.Items.Add(new ListViewItem(array));
            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (textBox2.Text != "")
            {
                List<string> deviceIds = new List<string>();
                for (int i = 0; i < listView1.SelectedItems.Count; i++)
                {
                    Debug.WriteLine("selected login name: "+listView1.SelectedItems[i].SubItems[2].Text);
                    string deviceId = listView1.SelectedItems[i].SubItems[3].Text;
                    deviceIds.Add(deviceId);
                }

                string json = JsonConvert.SerializeObject(new { textBox2.Text, deviceIds } );
                string url = base_address + "notification_tolist";
               
                var http = (HttpWebRequest)WebRequest.Create(new Uri(url));
                http.Accept = "application/json";
                http.ContentType = "application/json";
                http.Method = "POST";

                string parsedContent = json;
                ASCIIEncoding encoding = new ASCIIEncoding();
                Byte[] bytes = encoding.GetBytes(parsedContent);

                Stream newStream = http.GetRequestStream();
                newStream.Write(bytes, 0, bytes.Length);
                newStream.Close();

                var response = http.GetResponse();

                var stream = response.GetResponseStream();
                var sr = new StreamReader(stream);
                var content = sr.ReadToEnd();

                Debug.WriteLine("SERVER RESPONSE: "+ content);

            }
            else
            {
                MessageBox.Show("Notification Message is empty!");
            }
        }

       

    }
}

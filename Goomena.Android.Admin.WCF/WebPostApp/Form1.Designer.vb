﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnLoadURL = New System.Windows.Forms.Button()
        Me.txtOutput = New System.Windows.Forms.TextBox()
        Me.txtRequestData = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtRequestUrl = New System.Windows.Forms.TextBox()
        Me.btnPOSTData = New System.Windows.Forms.Button()
        Me.cbAction = New System.Windows.Forms.ComboBox()
        Me.lnkClearOutput = New System.Windows.Forms.LinkLabel()
        Me.btnGET = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'btnLoadURL
        '
        Me.btnLoadURL.Location = New System.Drawing.Point(239, 12)
        Me.btnLoadURL.Name = "btnLoadURL"
        Me.btnLoadURL.Size = New System.Drawing.Size(163, 23)
        Me.btnLoadURL.TabIndex = 0
        Me.btnLoadURL.Text = "Load Selected URL"
        Me.btnLoadURL.UseVisualStyleBackColor = True
        '
        'txtOutput
        '
        Me.txtOutput.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtOutput.Location = New System.Drawing.Point(13, 306)
        Me.txtOutput.Multiline = True
        Me.txtOutput.Name = "txtOutput"
        Me.txtOutput.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txtOutput.Size = New System.Drawing.Size(767, 144)
        Me.txtOutput.TabIndex = 1
        '
        'txtRequestData
        '
        Me.txtRequestData.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtRequestData.Location = New System.Drawing.Point(12, 130)
        Me.txtRequestData.Multiline = True
        Me.txtRequestData.Name = "txtRequestData"
        Me.txtRequestData.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txtRequestData.Size = New System.Drawing.Size(767, 144)
        Me.txtRequestData.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(10, 114)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(66, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "request data"
        '
        'Label2
        '
        Me.Label2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(13, 290)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(37, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "output"
        '
        'Label3
        '
        Me.Label3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(10, 72)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(56, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "request url"
        '
        'txtRequestUrl
        '
        Me.txtRequestUrl.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtRequestUrl.Location = New System.Drawing.Point(12, 88)
        Me.txtRequestUrl.Multiline = True
        Me.txtRequestUrl.Name = "txtRequestUrl"
        Me.txtRequestUrl.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txtRequestUrl.Size = New System.Drawing.Size(767, 23)
        Me.txtRequestUrl.TabIndex = 1
        '
        'btnPOSTData
        '
        Me.btnPOSTData.Location = New System.Drawing.Point(728, 12)
        Me.btnPOSTData.Name = "btnPOSTData"
        Me.btnPOSTData.Size = New System.Drawing.Size(52, 48)
        Me.btnPOSTData.TabIndex = 0
        Me.btnPOSTData.Text = "POST Data"
        Me.btnPOSTData.UseVisualStyleBackColor = True
        '
        'cbAction
        '
        Me.cbAction.FormattingEnabled = True
        Me.cbAction.Items.AddRange(New Object() {"login", "getupdating", "setUserProfileDetails", "approveProfile", "getnewphotos", "getTransactions"})
        Me.cbAction.Location = New System.Drawing.Point(13, 12)
        Me.cbAction.Name = "cbAction"
        Me.cbAction.Size = New System.Drawing.Size(209, 21)
        Me.cbAction.TabIndex = 3
        '
        'lnkClearOutput
        '
        Me.lnkClearOutput.AutoSize = True
        Me.lnkClearOutput.Location = New System.Drawing.Point(57, 290)
        Me.lnkClearOutput.Name = "lnkClearOutput"
        Me.lnkClearOutput.Size = New System.Drawing.Size(66, 13)
        Me.lnkClearOutput.TabIndex = 4
        Me.lnkClearOutput.TabStop = True
        Me.lnkClearOutput.Text = "Clear Output"
        '
        'btnGET
        '
        Me.btnGET.Location = New System.Drawing.Point(670, 12)
        Me.btnGET.Name = "btnGET"
        Me.btnGET.Size = New System.Drawing.Size(52, 48)
        Me.btnGET.TabIndex = 0
        Me.btnGET.Text = "GET Data"
        Me.btnGET.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(792, 462)
        Me.Controls.Add(Me.lnkClearOutput)
        Me.Controls.Add(Me.cbAction)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtRequestUrl)
        Me.Controls.Add(Me.txtRequestData)
        Me.Controls.Add(Me.txtOutput)
        Me.Controls.Add(Me.btnGET)
        Me.Controls.Add(Me.btnPOSTData)
        Me.Controls.Add(Me.btnLoadURL)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnLoadURL As System.Windows.Forms.Button
    Friend WithEvents txtOutput As System.Windows.Forms.TextBox
    Friend WithEvents txtRequestData As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtRequestUrl As System.Windows.Forms.TextBox
    Friend WithEvents btnPOSTData As System.Windows.Forms.Button
    Friend WithEvents cbAction As System.Windows.Forms.ComboBox
    Friend WithEvents lnkClearOutput As System.Windows.Forms.LinkLabel
    Friend WithEvents btnGET As System.Windows.Forms.Button

End Class

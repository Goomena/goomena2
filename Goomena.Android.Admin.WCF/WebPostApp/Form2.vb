﻿Imports System.Net
Imports System.IO
Imports System.Text

Public Class Form2

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim request As HttpWebRequest
        Dim response As HttpWebResponse = Nothing
        Dim reader As StreamReader
        Dim address As Uri
        'Dim data As StringBuilder
        Dim byteData() As Byte
        Dim postStream As Stream = Nothing

        address = New Uri("https://dev.goomena.com/IPN/epochIPNHandler.aspx")
        address = New Uri("http://localhost:19306/pweb/epochIPNHandler.aspx")

        ' Create the web request  
        request = DirectCast(WebRequest.Create(address), HttpWebRequest)
        ' Set type to POST  
        request.Method = "POST"
        request.ContentType = "application/x-www-form-urlencoded"


        'data = New StringBuilder()
        'data.Append("action=authandclose&ans=Y098014UM%2C771254500%2C+CAPTURED&auth_amount=19.94&" & _
        '            "auth_code=098014&auth_id=771254500&auth_localamount=15.00&currency=EUR&" & _
        '            "http_referer=https%3A%2F%2Fwww.goomena.com%2Fpay%2FinitPayment.ashx%3FPaymentMeth" & _
        '            "od%3Depoch%26ProductCode%3Ddd1000Credits%26CustomerID%3D269%26LoginName%3D" & _
        '            "xristosgol%40hotmail.com%26promoCode%3D%26CustomReferrer%3Dg3%26downloadURL%" & _
        '            "3D%26amount%3D0%26SalesSiteID%3D5%26type%3D1664806032%26fn%3D%26zip%3D21100%" & _
        '            "26cit%3DNaiplio%26reg%3DPeloponnese%26cntr%3DGR%26eml%3Dxristosgol%2540hotma" & _
        '            "il.com%26mob%3D%26camcharge%3D1&ip=2.84.243.94&mastercode=M-604633&member_id" & _
        '            "=1664806032&payment_type=VS&returnurl=http%3A%2F%2Fwww.goomena.com%2Fpay%2FO" & _
        '            "k.aspx%3FtransID%3D1403544216151.15&transaction_id=771254500&" & _
        '            "x_merchant_order_id=1403544216151.15&epoch_digest=766e8fe0d881d0bcf0b57b3f6d78d03c")

        Dim data As String = "action=authandclose&ans=Y410819UM%2C772193673%2C+CAPTURED&auth_amount=48.00&auth_code=410819&auth_id=772193673&auth_localamount=36.00&currency=EUR&http_referer=https%3A%2F%2Fwww.goomena.com%2FIPN%2FinitPayment.ashx%3FPaymentMethod%3Depoch%26ProductCode%3Ddd3000Credits%26CustomerID%3D84582%26LoginName%3Drigasp%40in.gr%26promoCode%3D%26CustomReferrer%3Dnone%26downloadURL%3D%26amount%3D0%26SalesSiteID%3D5%26type%3D1666130019%26fn%3D%26zip%3D13231%26cit%3DPetroupoli%26reg%3DAttica%26cntr%3DGR%26eml%3Drigasp%2540in.gr%26mob%3D%26camcharge%3D1&ip=92.119.51.196&mastercode=M-604633&member_id=1666130019&payment_type=VS&returnurl=https%3A%2F%2Fwww.goomena.com%2FIPN%2FOk.aspx%3FtransID%3D1403546505354.17&transaction_id=772193673&x_merchant_order_id=1403546505354.17&epoch_digest=b0b0664fbe6a6cd5059801fddf8acd1d"
        ' Create a byte array of the data we want to send  
        byteData = UTF8Encoding.UTF8.GetBytes(data.ToString())

        ' Set the content length in the request headers  
        request.ContentLength = byteData.Length

        ' Write data  
        Try
            postStream = request.GetRequestStream()
            postStream.Write(byteData, 0, byteData.Length)
        Catch ex As Exception
            TextBox1.Text = ex.ToString()
        Finally
            If Not postStream Is Nothing Then postStream.Close()
        End Try

        Try
            ' Get response  
            response = DirectCast(request.GetResponse(), HttpWebResponse)

            ' Get the response stream into a reader  
            reader = New StreamReader(response.GetResponseStream())

            ' Console application output  
            TextBox1.Text = reader.ReadToEnd()
        Catch ex As Exception
            TextBox1.Text = ex.ToString()
        Finally
            If Not response Is Nothing Then response.Close()
        End Try

    End Sub

    Private Sub btnAndroidDeleteAccount_Click(sender As Object, e As EventArgs) Handles btnAndroidDeleteAccount.Click
        ServicePointManager.Expect100Continue = False

        Dim request As HttpWebRequest
        Dim response As HttpWebResponse = Nothing
        Dim reader As StreamReader
        Dim address As Uri
        Dim data As StringBuilder
        Dim byteData() As Byte
        Dim postStream As Stream = Nothing

        address = New Uri("http://188.165.13.64/testwcf/GoomenaLib.Version4.GoomenaServiceV4.svc/json/deleteAccount")

        ' Create the web request  
        request = DirectCast(WebRequest.Create(address), HttpWebRequest)
        ' Set type to POST  
        request.Method = "POST"
        request.Accept = "application/json"
        request.ContentType = "application/json"


        data = New StringBuilder()
        data.Append("email=savva82&password=savva123@&logindetails=otinananai")

        ' Create a byte array of the data we want to send  
        byteData = UTF8Encoding.UTF8.GetBytes(data.ToString())

        ' Set the content length in the request headers  
        request.ContentLength = byteData.Length

        ' Write data  
        Try
            postStream = request.GetRequestStream()
            postStream.Write(byteData, 0, byteData.Length)
        Catch ex As Exception
            TextBox1.Text = ex.ToString()
        Finally
            If Not postStream Is Nothing Then postStream.Close()
        End Try

        Try
            ' Get response  
            response = DirectCast(request.GetResponse(), HttpWebResponse)

            ' Get the response stream into a reader  
            reader = New StreamReader(response.GetResponseStream())

            ' Console application output  
            TextBox1.Text = reader.ReadToEnd()
        Catch ex As Exception
            TextBox1.Text = ex.ToString()
        Finally
            If Not response Is Nothing Then response.Close()
        End Try
    End Sub


End Class

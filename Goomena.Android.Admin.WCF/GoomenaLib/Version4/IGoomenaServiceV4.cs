﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace GoomenaLib.Version4
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IGoomenaServiceV3" in both code and config file together.
    [ServiceContract]
    public interface IGoomenaServiceV4
    {
        [OperationContract]
        DateTime getUserProfileLastUpdate(string id);

        [OperationContract]
        string testException();

        [OperationContract]
        UserProfile getUserProfile(string id);

        [OperationContract]
        UserProfile login_v2(LoginParameters lp);


        [OperationContract]
        ImagePath getProfileImage(string profileId, string genderId);

        [OperationContract]
        UserDetails getUserProfileDetails(string id, string langId);

        [OperationContract]
        string getUserState(string userid);

        [OperationContract]
        ImagePathList getUserPhotos(string myuserid, string userid, string genderid);


        [OperationContract]
        MessageInfoList getMessagesQuick(string userid);

        [OperationContract]
        Message getMessageById(string messageid);

        [OperationContract]
        MessageList getConversationMessages(string profileid, string messageid);

        [OperationContract]
        MessageList getConversationMessagesWithProfile(string ownerprofileid, string profileid2);

        [OperationContract]
        string unlockMessage(string userid, string genderid, string senderid, string messageid, string offerid);

        [OperationContract]
        MessageInfoList getMessagesList(string userid, string view, string indexstart, string active);

        [OperationContract]
        string sendMessage(MessageParams mp);

        [OperationContract]
        string deleteMessage(string profileid, string messageid, string referrerparentid);

        [OperationContract]
        string deleteMessagesInView(string profileId, string messagesView, string messageId);

        [OperationContract]
        UserProfilesList getFavoriteList(string userid, string zip, string lat, string lng);

        [OperationContract]
        string viewProfile(string myprofileid, string viewprofileid);


        [OperationContract]
        string Favorite(string myuserid, string userid);

        [OperationContract]
        string UnFavorite(string myuserid, string userid);

        [OperationContract]
        string checkFavorite(string myuserid, string userid);

        [OperationContract]
        string checkHasPhotos(string userid);

        [OperationContract]
        string checkLike(string myprofileid, string otherprofileid);

        [OperationContract]
        string like(string myprofileid, string otherprofileid);

        [OperationContract]
        UserProfilesList getWhoFavoritedMe(string userid, string zip, string lat, string lng);

        [OperationContract]
        OfferList getLikes(string mod, string userid, string zip, string lat, string lng, string indexstart);

        [OperationContract]
        int getLikesCount(string userid);

        [OperationContract]
        int getMessagesCount(string userid);

        [OperationContract]
        Dating.Server.Core.DLL.clsGetMemberActionsCounters getCounters(string userid);

        [OperationContract]
        OfferList getOffers(string mod, string userid, string zip, string lat, string lng, string indexstart);

        [OperationContract]
        string likeActions(string userid, string action, string offerid);

        [OperationContract]
        void rejectLike(string offerid, string reason);

        [OperationContract]
        string makeOffer(string mode, string amount, string fromprofileid, string toprofileid, string genderid, string offerid);

        [OperationContract]
        string acceptOffer(string myprofileid, string offerid);

        [OperationContract]
        string getHash(string username);

        [OperationContract]
        string poke(string myprofileid, string otherprofileid, string parentOffer);

        [OperationContract]
        string register(RegisterData data);

        [OperationContract]
        CountryList getCountries();

        [OperationContract]
        Regions getCountryRegions(string countryIso, string lagid);

        [OperationContract]
        Cities getRegionCities(string countryIso, string region, string lagid);

        [OperationContract]
        string getZipCode(string countryIso, string region, string city, string lagid);

        [OperationContract]
        string setDefaultPhoto(string myprofileid, string photoid);

        [OperationContract]
        ImagePathList getUserThumbPhotos(string myuserid, string userid, string genderid);

        //-----------------notification functions-------------------------

        [OperationContract]
        string storeGCMUser(StoreUserParams p);

        [OperationContract]
        string sendNotificationsToAllUsers(string message);

        [OperationContract]
        string sendNotificationToUser(string fromProfileId, string toProfileId, string type);

        [OperationContract]
        string sendNotificationToUserList(NotificationParams p);

        [OperationContract]
        List<ProfileDeviceId> getGCMUsers();

        [OperationContract]
        string saveGCMUserSettings(string profileId, string notifMessages, string notifLikes, string notifOffers);

        [OperationContract]
        GCMUserSettings getSettings(string profileId);

        [OperationContract]
        string setProfilePhotoLevel(string fromProfileId, string toProfileId, string photoLevel);
        //----------------------------------------------------------------------
        [OperationContract]
        string getProfilePhotoLevel(string fromProfileId, string toProfileId);


        [OperationContract]
        UserProfilesList getWhoSharedMePhotos(string userid, string zip, string lat, string lng, string indexstart);

        [OperationContract]
        UserProfilesList getSharedByMePhotos(string userid, string zip, string lat, string lng, string indexstart);

        [OperationContract]
        string removeGCMUser(string regId);

        [OperationContract]
        UserProfilesList getSearchResults(string userid, string sort, string zipstr, string lat, string lng, string distance,
            string gender, string searchname, string username, string agemin, string agemax,
            string hasPhotos, string isOnline, string indexstart, string results);


        //        ------------------ v2 ----------------------------


        [OperationContract]
        UserProfilesList getSearchResultsV3(
            string userid, string sort, string zipstr,
            string lat, string lng, string distance,
            string gender, string searchname, string username, string agemin, string agemax,
            string hasPhotos, string isOnline, string indexstart, string results,
            string isVip, string countrycode, string hasPrivatePhotos, string travel);


        [OperationContract]
        UserProfilesList getNewUsersV3(string myprofileid, string offset, string results, string gender, string isOnline, string countrycode);

        [OperationContract]
        UserProfilesList getWhoViewedMeV2(string userid, string zip, string lat, string lng, string indexstart, string isOnline);
        [OperationContract]
        UserProfilesList getWhoViewedMe(string userid, string zip, string lat, string lng, string indexstart);

        [OperationContract]
        UserProfilesList getFavoriteListV2(string userid, string zip, string lat, string lng, string isOnline);


        [OperationContract]
        string deletePhoto(string userid, string photoId);

        [OperationContract]
        string getMinLevel(string myuserid);

        [OperationContract]
        string checkDifferentCountry(string userid1, string userid2);

        [OperationContract]
        string hasPhotos(string userid);

        [OperationContract]
        UserProfileEditData getProfileEditPersonal(LoginParameters lp);

        [OperationContract]
        UserProfileEditLookingForData getProfileEditLookingFor(LoginParameters lp);

        [OperationContract]
        UserProfileEditData getProfileEditPersonalInfoLists(LoginParameters lp);

        [OperationContract]
        UserProfileEditData getProfileEditLocation(LoginParameters lp);

        [OperationContract]
        UserProfileEditData getProfileEditOtherDetails(LoginParameters lp);

        [OperationContract]
        string setProfileEditLocation(UserProfileEditData lp);

        [OperationContract]
        string setProfileEditPersonal(UserProfileEditData lp);

        [OperationContract]
        string setProfileEditAboutMe(UserProfileEditData lp);

        [OperationContract]
        string setProfileEditOtherDetails(UserProfileEditData lp);

        [OperationContract]
        string setProfileEditPersonalInfoLists(UserProfileEditData lp);

        [OperationContract]
        string setProfileEditLookingFor(UserProfileEditLookingForData lp);

        [OperationContract]
        ItemsList getList(string listType, string lang);

        [OperationContract]
        int getBlockedCount(string userid, string lat, string lng);

        [OperationContract]
        UserProfilesList getBlocked(string userid, string lat, string lng, string indexstart);

        [OperationContract]
        string performBlockAction(string fromprofileid, string toprofileid, string action);

        [OperationContract]
        int getAvailableCredits(string userid);

        [OperationContract]
        string changePassword(LoginParameters lp, string newPassword);

        [OperationContract]
        string deleteAccount(LoginParameters lp);

        [OperationContract]
        ImagePath getPrivatePhoto(string profileid);

        [OperationContract]
        string setPhotoLevel(string profileid, string photoid, string level);

    }

    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    // You can add XSD files into the project. After building the project, you can directly use the data types defined there, with the namespace "GoomenaLib.ContractType".
    [DataContract]
    public class CompositeType : GoomenaLib.CompositeType
    {
    }

    [DataContract]
    public class GCMUserSettings : GoomenaLib.GCMUserSettings
    {
    }

    [DataContract]
    public class ProfileDeviceId
    {
        [DataMember]
        public string deviceId { get; set; }

        [DataMember]
        public UserProfile profile { get; set; }
    }

    [DataContract]
    public class NotificationParams
    {
        [DataMember]
        public string message { get; set; }
        [DataMember]
        public List<string> deviceIds { get; set; }
    }

    [DataContract]
    public class StoreUserParams : GoomenaLib.StoreUserParams
    {
    }

    [DataContract]
    public class LoginParameters : GoomenaLib.LoginParameters
    {
    }

    [DataContract]
    public class RegisterData : GoomenaLib.RegisterData
    {
    }

    [DataContract]
    public class UserProfilesList
    {
        List<UserProfile> profiles;

        [DataMember]
        public List<UserProfile> Profiles {
            get { return profiles; }
            set { profiles = value; }
        }
    }

    [DataContract]
    public class UserProfile : GoomenaLib.UserProfile
    {
        [DataMember]
        public string IsOnline { get; set; }

        [DataMember]
        public string PhotosCount { get; set; }
    }

    //[DataContract]
    //public class ProfileLocation 
    //{
    //    [DataMember]
    //    public string LocID { get; set; }

    //    [DataMember]
    //    public string Descr { get; set; }
    //}

    [DataContract]
    public class UserProfileEditData : GoomenaLib.LoginParameters
    {
        [DataMember]
        public string ErrorMessage { get; set; }



        [DataMember]
        public string Country { get; set; }

        [DataMember]
        public string Region { get; set; }

        [DataMember]
        public string City { get; set; }

        [DataMember]
        public string ZipCode { get; set; }

        [DataMember]
        public List<string> SelectedLangIdList { get; set; }

        [DataMember]
        public ItemsList ProfileLocationList { get; set; }

        [DataMember]
        public ItemsList JobsList { get; set; }





        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string LastName { get; set; }

        [DataMember]
        public string Address { get; set; }

        [DataMember]
        public string EmailAddress { get; set; }

        [DataMember]
        public string PhoneNumber { get; set; }

        [DataMember]
        public string MobilePhone { get; set; }


        [DataMember]
        public string AboutMe_Heading { get; set; }

        [DataMember]
        public string AboutMe_DescribeYourself { get; set; }

        [DataMember]
        public string AboutMe_DescribeFirstDate { get; set; }


        [DataMember]
        public string Occupation { get; set; }

        [DataMember]
        public string Education { get; set; }

        [DataMember]
        public string AnnualIncome { get; set; }


        [DataMember]
        public string Height { get; set; }

        [DataMember]
        public string BodyType { get; set; }

        [DataMember]
        public string EyeColor { get; set; }

        [DataMember]
        public string HairColor { get; set; }

        [DataMember]
        public string Children { get; set; }

        [DataMember]
        public string Ethnicity { get; set; }

        [DataMember]
        public string Religion { get; set; }

        [DataMember]
        public string Smoking { get; set; }

        [DataMember]
        public string Drinking { get; set; }

        [DataMember]
        public string BreastSize { get; set; }



    }


    [DataContract]
    public class UserProfileEditLookingForData : GoomenaLib.LoginParameters
    {
        [DataMember]
        public string ErrorMessage { get; set; }


        [DataMember]
        public string RelationshipStatus { get; set; }

        [DataMember]
        public string TODShort { get; set; }

        [DataMember]
        public string TODFriendship { get; set; }

        [DataMember]
        public string TODLongTerm { get; set; }

        [DataMember]
        public string TODMutually { get; set; }

        [DataMember]
        public string TODMarried { get; set; }

        [DataMember]
        public string TODCasual { get; set; }

    }


    [DataContract]
    public class ImagePath : GoomenaLib.ImagePath
    {
        [DataMember]
        public int Level { get; set; }
    }

    [DataContract]
    public class ImagePathList
    {
        private List<ImagePath> pathList;

        [DataMember]
        public List<ImagePath> PathList {
            get { return pathList; }
            set { pathList = value; }
        }

    }

    [DataContract]
    public class UserDetails : GoomenaLib.UserDetails
    {

    }

    [DataContract]
    public class MessageInfo : GoomenaLib.MessageInfo
    {
        [DataMember]
        public string IsOnline { get; set; }

    }

    [DataContract]
    public class MessageInfoList
    {
        [DataMember]
        public List<MessageInfo> msgList { get; set; }
    }

    [DataContract]
    public class Message : GoomenaLib.Message
    {
    }

    [DataContract]
    public class MessageList
    {
        [DataMember]
        public List<Message> list { get; set; }

    }

    [DataContract]
    public class MessageParams : GoomenaLib.MessageParams
    {
        //[DataMember]
        //public string subject { get; set; }
        //[DataMember]
        //public string body { get; set; }
        //[DataMember]
        //public string fromprofileid { get; set; }
        //[DataMember]
        //public string toprofileid { get; set; }
        //[DataMember]
        //public string genderid { get; set; }
        //[DataMember]
        //public string offerId { get; set; }

    }

    [DataContract]
    public class UploadPhotoParams : GoomenaLib.UploadPhotoParams
    {
        //[DataMember]
        //public string strFileName { get; set; }

        //[DataMember]
        //public Stream streamFileContent { get; set; }

        //[DataMember]
        //public string profileid { get; set; }

        //[DataMember]
        //public string photoLevel { get; set; }

    }

    [DataContract]
    public class Offer
    {
        [DataMember]
        public UserProfile profile { get; set; }

        [DataMember]
        public string offerId { get; set; }

        [DataMember]
        public string rejectReason { get; set; }

    }

    [DataContract]
    public class OfferList
    {
        [DataMember]
        public List<Offer> offers { get; set; }
    }

    [DataContract]
    public class CountryList
    {
        [DataMember]
        public List<Country> countries { get; set; }
    }

    [DataContract]
    public class Country : GoomenaLib.Country
    {

    }

    [DataContract]
    public class Regions
    {
        [DataMember]
        public List<string> list { get; set; }
    }

    [DataContract]
    public class Cities
    {
        [DataMember]
        public List<string> list { get; set; }

    }


    [DataContract]
    public class ListItem
    {
        [DataMember]
        public string id { get; set; }

        [DataMember]
        public string title { get; set; }

    }

    [DataContract]
    public class ItemsList
    {
        [DataMember]
        public List<ListItem> items { get; set; }

        public ItemsList() {
            items = new List<ListItem>();
        }
    }



}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoomenaLib
{
    public class ParseTo
    {
        public static bool? toBoolNull(string val) {
            bool? res = null;
            if (!string.IsNullOrEmpty(val)) {
                bool a = false;
                if (bool.TryParse(val, out a))
                    res = a;
            }
            return res;
        }

        public static int? toIntNull(string val) {
            int? res = null;
            if (!string.IsNullOrEmpty(val)) {
                int a = 0;
                if (int.TryParse(val, out a))
                    res = a;
            }
            return res;
        }

        public static DateTime? toDateNull(string val) {
            DateTime? res = null;
            if (!string.IsNullOrEmpty(val)) {
                System.Globalization.CultureInfo provider   = System.Globalization.CultureInfo.InvariantCulture;
                try {
                    DateTime a = DateTime.ParseExact(val, "yyyyMMdd HH:mm:ss", provider);
                    res = a;
                }
                catch (Exception) { }
            }
            return res;
        }

    }
}

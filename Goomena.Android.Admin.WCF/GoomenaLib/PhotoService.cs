﻿using Dating.Server.Core.DLL;
using Dating.Server.Datasets.DLL;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Channels;
using System.ServiceModel.Web;
using System.Text;
using System.Web;


namespace GoomenaLib
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "PhotoService" in both code and config file together.
    //[AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class PhotoService : ServiceBase, IPhotoService
    {
        public void DoWork() {

        }

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "savephoto/{strFileName}/{profileid}/{photoLevel}")]
        public string SavePhoto(string strFileName, string profileid, string photoLevel, System.IO.Stream streamFileContent) {
            try {
                DataHelpers.UpdateEUS_Profiles_Activity(Convert.ToInt32(profileid), getClientIP(), true);

                clsConfigValues config = new clsConfigValues();
                if (config.photos_max_approved <= DataHelpers.EUS_CustomerPhotos_GetVisiblePhotos(Convert.ToInt32(profileid))) {
                    return "error";
                }
                else {
                    return SavePostedFile(strFileName, streamFileContent, profileid, photoLevel);

                }

            }
            catch (Exception ex) {
                return ex.ToString();
            }

        }


        //[WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadStream")]
        //public bool UploadStream(System.IO.Stream stream) {
        //    //this implementation places the uploaded file
        //    //in the current directory and calls it "uploadedfile"
        //    //with no file extension


        //    //get the query string passed in
        //    OperationContext context = OperationContext.Current;
        //    MessageProperties properties = context.IncomingMessageProperties;
        //    HttpRequestMessageProperty requestProperty = properties[HttpRequestMessageProperty.Name] as HttpRequestMessageProperty;
        //    string queryString = requestProperty.QueryString;

        //    //put the values into a collection
        //    NameValueCollection coll = HttpUtility.ParseQueryString(queryString);
        //    string fname = coll["filename"];


        //    string filePath = Path.Combine(
        //        System.Environment.CurrentDirectory,
        //        "uploadedfile");
        //    try {
        //        Console.WriteLine("Saving to file {0}", filePath);
        //        FileStream outstream = File.Open(filePath, FileMode.Create, FileAccess.Write);
        //        //read from the input stream in 4K chunks
        //        //and save to output stream
        //        const int bufferLen = 4096;
        //        byte[] buffer = new byte[bufferLen];
        //        int count = 0;
        //        while ((count = stream.Read(buffer, 0, bufferLen)) > 0) {
        //            Console.Write(".");
        //            outstream.Write(buffer, 0, count);
        //        }
        //        outstream.Close();
        //        stream.Close();
        //        Console.WriteLine();
        //        Console.WriteLine("File {0} saved", filePath);

        //        return true;
        //    }
        //    catch (IOException ex) {
        //        Console.WriteLine(
        //            String.Format("An exception was thrown while opening or writing to file {0}", filePath));
        //        Console.WriteLine("Exception is: ");
        //        Console.WriteLine(ex.ToString());
        //        throw ex;
        //    }
        //}


        //[WebInvoke(Method = "POST", UriTemplate = "savephoto")]//ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
        //public void SavePhoto(FileUploadMessage request) {

        //    Stream fileStream = null;
        //    Stream outputStream = null;
        //    FileUploadMessageResult res = new FileUploadMessageResult();

        //    try {
        //        fileStream = request.FileByteStream;
        //        res.Message = SavePostedFile(request.FileName, request.FileByteStream, request.ProfileID, request.PhotoLevel);
        //        //string rootPath = ConfigurationManager.AppSettings["RootPath"].ToString();

        //        //DirectoryInfo dirInfo = new DirectoryInfo(rootPath);
        //        //if (!dirInfo.Exists) {
        //        //    dirInfo.Create();
        //        //}

        //        ////Create the file in the filesystem - change the extension if you wish, or use a passed in value from metadata ideally
        //        //string newFileName = Path.Combine(rootPath, Guid.NewGuid() + ".xml");

        //        //outputStream = new FileInfo(newFileName).OpenWrite();
        //        //const int bufferSize = 1024;
        //        //byte[] buffer = new byte[bufferSize];

        //        //int bytesRead = fileStream.Read(buffer, 0, bufferSize);

        //        //while (bytesRead > 0) {
        //        //    outputStream.Write(buffer, 0, bufferSize);
        //        //    bytesRead = fileStream.Read(buffer, 0, bufferSize);
        //        //}
        //    }
        //    catch (IOException ex) {
        //        throw new FaultException<IOException>(ex, new FaultReason(ex.Message));
        //    }
        //    finally {
        //        if (fileStream != null) {
        //            fileStream.Close();
        //        }
        //        if (outputStream != null) {
        //            outputStream.Close();
        //        }
        //    }
        //   // return res;
        //}


        //[WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, UriTemplate = "savephoto")]
        //public FileUploadMessageResult SavePhoto(FileUploadMessage request) {

        //    Stream fileStream = null;
        //    Stream outputStream = null;
        //        FileUploadMessageResult res = new FileUploadMessageResult();

        //    try {
        //        fileStream = request.FileByteStream;
        //        res.Message = SavePostedFile(request.FileName, request.FileByteStream, request.ProfileID, request.PhotoLevel);
        //        //string rootPath = ConfigurationManager.AppSettings["RootPath"].ToString();

        //        //DirectoryInfo dirInfo = new DirectoryInfo(rootPath);
        //        //if (!dirInfo.Exists) {
        //        //    dirInfo.Create();
        //        //}

        //        ////Create the file in the filesystem - change the extension if you wish, or use a passed in value from metadata ideally
        //        //string newFileName = Path.Combine(rootPath, Guid.NewGuid() + ".xml");

        //        //outputStream = new FileInfo(newFileName).OpenWrite();
        //        //const int bufferSize = 1024;
        //        //byte[] buffer = new byte[bufferSize];

        //        //int bytesRead = fileStream.Read(buffer, 0, bufferSize);

        //        //while (bytesRead > 0) {
        //        //    outputStream.Write(buffer, 0, bufferSize);
        //        //    bytesRead = fileStream.Read(buffer, 0, bufferSize);
        //        //}
        //    }
        //    catch (IOException ex) {
        //        throw new FaultException<IOException>(ex, new FaultReason(ex.Message));
        //    }
        //    finally {
        //        if (fileStream != null) {
        //            fileStream.Close();
        //        }
        //        if (outputStream != null) {
        //            outputStream.Close();
        //        }
        //    }
        //    return res;
        //}

        //[WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "savephoto/{strFileName}/{profileid}/{photoLevel}")]
        //public string SavePhoto(string strFileName, string profileid, string photoLevel) {
        //    try {
        //        DataHelpers.UpdateEUS_Profiles_Activity(Convert.ToInt32(profileid), getClientIP(), true);

        //        clsConfigValues config = new clsConfigValues();
        //        if (config.photos_max_approved <= DataHelpers.EUS_CustomerPhotos_GetVisiblePhotos(Convert.ToInt32(profileid))) {
        //            return "error";
        //        }
        //        else {
        //            //OperationContext context = OperationContext.Current;
        //            //MessageProperties prop = context.IncomingMessageProperties;

        //            System.Web.HttpPostedFile file = System.Web.HttpContext.Current.Request.Files[0];
        //            System.IO.Stream inputStream = file.InputStream;
        //            return SavePostedFile(strFileName, inputStream, profileid, photoLevel);
        //            // return "nothing";
        //        }

        //    }
        //    catch (Exception ex) {
        //        return ex.ToString();
        //    }

        //}



        //[WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "savephoto/{strFileName}/{profileid}/{photoLevel}")]
        //[WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped, Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "savephoto/{strFileName}/{profileid}/{photoLevel}")]
        //[WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "savephoto/{strFileName}/{profileid}/{photoLevel}")]
        //public string SavePhoto(string strFileName, string profileid, string photoLevel) {
        //    try {
        //        DataHelpers.UpdateEUS_Profiles_Activity(Convert.ToInt32(profileid), getClientIP(), true);

        //        clsConfigValues config = new clsConfigValues();
        //        if (config.photos_max_approved <= DataHelpers.EUS_CustomerPhotos_GetVisiblePhotos(Convert.ToInt32(profileid))) {
        //            return "error";
        //        }
        //        else {
        //            //OperationContext context = OperationContext.Current;
        //            //MessageProperties prop = context.IncomingMessageProperties;

        //            System.Web.HttpPostedFile file = System.Web.HttpContext.Current.Request.Files[0];
        //            System.IO.Stream inputStream = file.InputStream;
        //            return SavePostedFile(strFileName, inputStream, profileid, photoLevel);
        //            // return "nothing";
        //        }

        //    }
        //    catch (Exception ex) {
        //        return ex.ToString();
        //    }

        //}

        //[WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "SavePhoto")]
        //public string SavePhoto(Stream data) {
        //    //try {
        //    //DataHelpers.UpdateEUS_Profiles_Activity(Convert.ToInt32(profileid), getClientIP(), true);

        //    //clsConfigValues config = new clsConfigValues();
        //    //if (config.photos_max_approved <= DataHelpers.EUS_CustomerPhotos_GetVisiblePhotos(Convert.ToInt32(profileid))) {
        //    //    return "error";
        //    //}
        //    //else {
        //    ////OperationContext context = OperationContext.Current;
        //    ////MessageProperties prop = context.IncomingMessageProperties;

        //    //System.Web.HttpPostedFile file = System.Web.HttpContext.Current.Request.Files[0];
        //    //System.IO.Stream inputStream = file.InputStream;
        //    //return SavePostedFile(strFileName, inputStream, profileid, photoLevel);
        //    //// return "nothing";

        //    //get the query string passed in
        //    OperationContext context = OperationContext.Current;
        //    MessageProperties properties = context.IncomingMessageProperties;
        //    HttpRequestMessageProperty requestProperty = properties[HttpRequestMessageProperty.Name] as HttpRequestMessageProperty;
        //    string queryString = requestProperty.QueryString;

        //    //put the values into a collection
        //    NameValueCollection coll = HttpUtility.ParseQueryString(queryString);
        //    string fname = coll["filename"];

        //    //now read our data in chunks
        //    int bytesread = 0;
        //    int size = 8192;
        //    MemoryStream inputStream = new MemoryStream();
        //    byte[] chunk = new byte[size];
        //    bytesread = data.Read(chunk, 0, size);

        //    while (bytesread > 0) {
        //        inputStream.Write(chunk, 0, bytesread);
        //        bytesread = data.Read(chunk, 0, size);
        //    }
        //    string strFileName = fname;
        //    string result = SavePostedFile(strFileName, inputStream, "269", "0");
        //    inputStream.Dispose();
        //    return result;
        //    //    }

        //    //}
        //    //catch (Exception ex) {
        //    //    return ex.ToString();
        //    //}

        //}

        private string SavePostedFile(string strFileName, System.IO.Stream streamFileContent, string profileid, string photoLevel) {
            int iProfileid = Convert.ToInt32(profileid);
            int iPhotoLevel = Convert.ToInt32(photoLevel);
            return SavePostedFile(strFileName, streamFileContent, iProfileid, iPhotoLevel);
        }


        private string SavePostedFile(string strFileName, System.IO.Stream streamFileContent, int profileid, int photoLevel) {
            string returnMessage = "START UPLOADING";

            try {

                string imgfilePath = "";
                Guid _guid = Guid.NewGuid();
                string ext = strFileName;
                returnMessage += "ext: " + ext + "\n";
                ext = ext.Substring(ext.LastIndexOf(".") + 1);
                string filePath = _guid.ToString("N") + "." + ext;

                returnMessage += filePath + " start: ";

                clsConfigValues config = new clsConfigValues();


                string imgDir = config.image_server_path;
                returnMessage += imgDir + "\n";

                imgDir = imgDir.Replace("{0}", profileid.ToString());
                imgDir = imgDir.Replace("{1}", "");

                if (!System.IO.Directory.Exists(imgDir)) {
                    System.IO.Directory.CreateDirectory(imgDir);
                    returnMessage += "Dir Created\n";
                }
                imgfilePath = Path.Combine(imgDir, filePath);
                returnMessage += "file path: " + imgfilePath;




                using (Image original = Image.FromStream(streamFileContent)) {
                    try {

                        PhotoUtils.SaveToJpeg(original, imgfilePath);

                        string thumbfilePath = "";
                        {
                            string thumbDir = config.image_thumb_server_path;
                            thumbDir = thumbDir.Replace("{0}", profileid.ToString());
                            thumbDir = thumbDir.Replace("{1}", "");

                            if (!System.IO.Directory.Exists(thumbDir)) {
                                System.IO.Directory.CreateDirectory(thumbDir);
                            }
                            thumbfilePath = Path.Combine(thumbDir, filePath);
                        }

                        // fill image to avoid streching
                        using (Image thumbnail = PhotoUtils.AutosizeImageAndInscribe(original, 250, 250)) {
                            PhotoUtils.SaveToJpeg(thumbnail, thumbfilePath);
                            thumbnail.Dispose();
                            returnMessage += "250 jpeg saved\n";
                        }

                    }
                    catch (Exception) {
                        throw;
                    }
                    finally {
                        original.Dispose();
                    }
                }


                using (Image original = Image.FromFile(imgfilePath)) {
                    try {

                        string d150Path = "";
                        {
                            string d150 = config.image_d150_server_path;
                            d150 = d150.Replace("{0}", profileid.ToString());
                            d150 = d150.Replace("{1}", "");

                            if (!System.IO.Directory.Exists(d150)) {
                                System.IO.Directory.CreateDirectory(d150);
                            }
                            d150Path = Path.Combine(d150, filePath);
                        }

                        using (Image thumbnail = PhotoUtils.AutosizeImageAndInscribe(original, 150, 150)) {
                            PhotoUtils.SaveToJpeg(thumbnail, d150Path);
                            thumbnail.Dispose();
                            returnMessage += "150 jpeg saved\n";
                        }

                    }
                    catch (Exception) {
                        throw;
                    }
                    finally {
                        original.Dispose();
                    }
                }


                {
                    Image original = Image.FromFile(imgfilePath);
                    try {

                        string d350Path = "";
                        {
                            string d350 = config.image_d350_server_path;
                            d350 = d350.Replace("{0}", profileid.ToString());
                            d350 = d350.Replace("{1}", "");

                            if (!System.IO.Directory.Exists(d350)) {
                                System.IO.Directory.CreateDirectory(d350);
                            }
                            d350Path = Path.Combine(d350, filePath);
                        }

                        using (Image thumbnail = PhotoUtils.ResizeBitmap(ref original, 350, 350)) {
                            PhotoUtils.SaveToJpeg(thumbnail, d350Path);
                            thumbnail.Dispose();
                            returnMessage += "350 jpeg saved\n";
                        }

                    }
                    catch (Exception) {
                        throw;
                    }
                    finally {
                        original.Dispose();
                    }
                }


                //' save to database file reference
                int displayLevel = photoLevel;//Convert.ToInt32(photoLevel);


                //If (chkPrivePhoto.Checked) Then
                //    displayLevel = 1 'private
                //    chkPrivePhoto.Checked = False
                //End If

                DSMembers ds = DataHelpers.GetEUS_CustomerPhotos_ByProfileOrMirrorID(0);

                DSMembers.EUS_CustomerPhotosRow newRow = ds.EUS_CustomerPhotos.NewEUS_CustomerPhotosRow();
                newRow.CustomerPhotosID = -1;
                newRow.CustomerID = profileid;
                newRow.DateTimeToUploading = DateTime.UtcNow;
                newRow.DisplayLevel = displayLevel;
                newRow.FileName = filePath;
                newRow.HasAproved = false;
                newRow.HasDeclined = false;
                newRow.IsDefault = false;
                newRow.IsAutoApproved = false;
                ds.EUS_CustomerPhotos.AddEUS_CustomerPhotosRow(newRow);

                DataHelpers.UpdateEUS_CustomerPhotos(ref ds);
                DataHelpers.UpdateEUS_CustomerPhotos_IsUpdating(profileid, newRow.CustomerPhotosID, true);


                dynamic isAutoApprovedPhoto = false;
                //photo auto approve
                try {

                    if (config.auto_approve_photos == "1") {
                        isAutoApprovedPhoto = true;
                        AdminActions.ApprovePhoto((int)newRow.CustomerPhotosID, true, false, isAutoApprovedPhoto);
                    }
                }
                catch (Exception ex) {
                    returnMessage += ex.ToString();
                }

                DataHelpers.UpdateEUS_Profiles_MirrorSetStatusUpdating(profileid, ProfileStatusEnum.Updating, ProfileModifiedEnum.UpdatingPhotos, true);
                clsUserNotifications.SendNotificationsToMembers_AboutNewMember(profileid, true);

                /*

                if (setDefault == "1")
                {
                    DSMembers ds = DataHelpers.GetEUS_CustomerPhotos_ByProfileOrMirrorID(this.MasterProfileId);
                    foreach (DSMembers.EUS_CustomerPhotosRow rawRecord in ds.EUS_CustomerPhotos.Rows)
                    {
                        if ((rawRecord.CustomerPhotosID == photoId && rawRecord.HasAproved == true))
                        {
                            rawRecord.IsDefault = true;
                            hasDefaultChanged = true;
                        }
                        else
                        {
                            rawRecord.IsDefault = false;
                        }
                    }
                    DataHelpers.UpdateEUS_CustomerPhotos(ds);
                    ds.Dispose();
                }

                
                DSMembers dsmem = DataHelpers.GetEUS_Profiles_ByProfileOrMirrorID(Convert.ToInt32(profileid));

                DataRow mRow;
                foreach (DataRow r in dsmem.EUS_Profiles.Rows)
                {
                    if ((bool)r["IsMaster"] == false)
                    {
                        mRow = r;
                    }
                }

                
                try
                {
                    SendNotificationEmailToSupport(mRow, newRow, isAutoApproved);
                }
                catch (Exception ex)
                {
                }
                 
                */

            }
            catch (Exception ex) {
                returnMessage += ex.ToString();
            }

            //return returnMessage;
            return "ok";
        }

        //private string getClientIP()
        //{
        //    OperationContext context = OperationContext.Current;
        //    MessageProperties prop = context.IncomingMessageProperties;
        //    RemoteEndpointMessageProperty endpoint = prop[RemoteEndpointMessageProperty.Name] as RemoteEndpointMessageProperty;
        //    string ip = endpoint.Address;

        //    return ip;
        //}
    }
}

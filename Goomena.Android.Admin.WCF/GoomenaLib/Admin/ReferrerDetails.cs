﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.ServiceModel;
using Dating.Server.Core.DLL;

namespace GoomenaLib.Admin
{


    [DataContract]
    public class ReferrerDetails
    {
        [DataMember]
        public string profileid { get; set; }
        [DataMember]
        public string status { get; set; }
        [DataMember]
        public string email { get; set; }
        [DataMember]
        public string fullparentcode { get; set; }
        [DataMember]
        public string referrerparentid { get; set; }
        [DataMember]
        public string firstname { get; set; }
        [DataMember]
        public string lastname { get; set; }
        [DataMember]
        public string siteprofileid { get; set; }
        [DataMember]
        public string sitemirrorprofileid { get; set; }
        [DataMember]
        public string siteloginname { get; set; }
        [DataMember]
        public string customreferrer { get; set; }
        [DataMember]
        public string registerdate { get; set; }
        [DataMember]
        public string password { get; set; }
        [DataMember]
        public string loginname { get; set; }
        [DataMember]
        public string parentloginname { get; set; }


        public static ReferrerDetails GetUserDetails(int id, string langId) {
            Dating.Admin.Referrals.Core.DLL.CMSDBDataContext RefContext = new Dating.Admin.Referrals.Core.DLL.CMSDBDataContext(CurrentSettings.ConnectionString);

            ReferrerDetails details = new ReferrerDetails();
            try {
                var pr = (from p in RefContext.REF_ReferrerProfiles
                          where p.CustomerID == id
                          select new
                          {
                              profileid = p.CustomerID,
                              status = p.Status,
                              email = p.eMail,
                              fullparentcode = p.REF_FullParentCode,
                              referrerparentid = p.ReferrerParentId,
                              firstname = p.FirstName,
                              lastname = p.LastName,
                              siteprofileid = p.SiteProfileID,
                              sitemirrorprofileid = p.SiteMirrorProfileID,
                              siteloginname = p.SiteLoginName,
                              customreferrer = p.CustomReferrer,
                              registerdate = p.DateTimeToRegister,
                              password = p.Password,
                              loginname = p.LoginName,

                          }).FirstOrDefault();


                details.profileid = pr.profileid.ToString();
                details.status = pr.status.ToString();
                details.email = pr.email;
                details.fullparentcode = pr.fullparentcode;
                details.referrerparentid = (pr.referrerparentid.HasValue ? pr.referrerparentid : 0).ToString();
                details.firstname = pr.firstname;
                details.lastname = pr.lastname;
                details.siteprofileid = (pr.siteprofileid.HasValue ? pr.siteprofileid : 0).ToString();
                details.sitemirrorprofileid = (pr.sitemirrorprofileid.HasValue ? pr.sitemirrorprofileid : 0).ToString();
                details.siteloginname = pr.siteloginname;
                details.customreferrer = pr.customreferrer;
                if (pr.registerdate.HasValue) details.registerdate = Utils.GetDateWithOffset(pr.registerdate.Value).ToString("yyyyMMdd HH:mm");
                details.password = pr.password;
                details.loginname = pr.loginname;


                if (pr.referrerparentid == 1) {
                    details.parentloginname = "root";
                }
                else {
                    details.parentloginname = Dating.Server.Core.DLL.DataHelpers.GetEUS_Profile_LoginName_ByProfileID(pr.referrerparentid.Value);
                }

            }
            catch (Exception ex) {
                throw new Exception(ex.Message, ex);
            }
            finally {
                RefContext.Dispose();
            }
            return details;
        }

    }

}

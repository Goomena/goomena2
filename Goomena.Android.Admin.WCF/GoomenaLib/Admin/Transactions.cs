﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.ServiceModel;
using Dating.Server.Core.DLL;

namespace GoomenaLib.Admin
{

    [DataContract]
    public class Transactions
    {

        [DataMember]
        public string CustomerTransactionID { get; set; }
        [DataMember]
        public string LoginName { get; set; }
        [DataMember]
        public string Country { get; set; }
        [DataMember]
        public string RegisterGEOInfos { get; set; }
        [DataMember]
        public string LastLoginGEOInfos { get; set; }
        [DataMember]
        public string Date_Time { get; set; }
        [DataMember]
        public string CustomerID { get; set; }
        [DataMember]
        public string DebitAmount { get; set; }
        [DataMember]
        public string DebitAmountReceived { get; set; }
        [DataMember]
        public string DebitReducePercent { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string IP { get; set; }
        [DataMember]
        public string TransactionInfo { get; set; }
        [DataMember]
        public string PaymentMethods { get; set; }
        [DataMember]
        public string Currency { get; set; }
        [DataMember]
        public string IsSubscription { get; set; }


        public void ReadRow(Dating.Admin.Datasets.DLL.DSMembersViews.AllCustomerTransactionsRow row) {
            this.CustomerTransactionID = row.CustomerTransactionID.ToString();
            this.LoginName = (row.IsLoginNameNull() ? "[Profile not found]" : row.LoginName);
            this.Country = (row.IsCountryNull() ? "" : row.Country);
            this.RegisterGEOInfos = (row.IsRegisterGEOInfosNull() ? "" : row.RegisterGEOInfos);
            this.LastLoginGEOInfos = (row.IsLastLoginGEOInfosNull() ? "" : row.LastLoginGEOInfos);
            this.Date_Time = Utils.GetDateWithOffset(row.Date_Time).ToString("dd/MM/yyyy HH:mm");
            this.CustomerID = (row.IsCustomerIDNull() ? 0 : row.CustomerID).ToString();
            this.DebitAmount = (row.IsDebitAmountNull() ? 0 : row.DebitAmount).ToString("0.00");
            this.DebitAmountReceived = (row.IsDebitAmountReceivedNull() ? 0 : row.DebitAmountReceived).ToString("0.00");
            this.DebitReducePercent = (row.IsDebitReducePercentNull() ? 0 : row.DebitReducePercent).ToString("0");
            this.Description = (row.IsDescriptionNull() ? "" : row.Description);
            this.IP = (row.IsIPNull() ? "" : row.IP);
            this.TransactionInfo = (row.IsTransactionInfoNull() ? "" : row.TransactionInfo);
            this.PaymentMethods = (row.IsPaymentMethodsNull() ? "" : row.PaymentMethods);
            this.Currency = (row.IsCurrencyNull() ? "" : row.Currency);
            this.IsSubscription = (row.IsIsSubscriptionNull() ? "0" : (row.IsSubscription ? "1" : "0"));
        }


    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.ServiceModel;
using Dating.Server.Core.DLL;

namespace GoomenaLib.Admin
{


    [DataContract]
    public class UserDetails
    {
        [DataMember]
        public string mirrorprofileid { get; set; }
        [DataMember]
        public string ismaster { get; set; }
        [DataMember]
        public string status { get; set; }
        [DataMember]
        public string birthdate { get; set; }
        [DataMember]
        public string age { get; set; }
        [DataMember]
        public string bodytype { get; set; }
        [DataMember]
        public string haireyes { get; set; }
        [DataMember]
        public string education { get; set; }
        [DataMember]
        public string childs { get; set; }
        [DataMember]
        public string nationality { get; set; }
        [DataMember]
        public string religion { get; set; }
        [DataMember]
        public string smokedring { get; set; }
        [DataMember]
        public string job { get; set; }
        [DataMember]
        public string income { get; set; }
        [DataMember]
        public string familystate { get; set; }
        [DataMember]
        public string lookfor { get; set; }
        [DataMember]
        public string interest { get; set; }
        [DataMember]
        public string profileupdated { get; set; }
        [DataMember]
        public string aboutme_heading { get; set; }
        [DataMember]
        public string aboutme_describeyourself { get; set; }
        [DataMember]
        public string aboutme_describedate { get; set; }
        [DataMember]
        public string occupation { get; set; }
        [DataMember]
        public string available_credits { get; set; }
        [DataMember]
        public string zodiac { get; set; }
        [DataMember]
        public string referrerparentid { get; set; }


        public static UserDetails GetUserDetails(Dating.Server.Datasets.DLL.CMSDBDataContext myContext, int id, string langId) {
            UserDetails details = new UserDetails();
            var pr = (from p in myContext.EUS_Profiles
                      where p.ProfileID == id
                      select new
                      {
                          mirrorprofileid = p.MirrorProfileID,
                          ismaster = p.IsMaster,
                          status = p.Status,
                          birth = p.Birthday,
                          bodytype = p.PersonalInfo_BodyTypeID,
                          hair = p.PersonalInfo_HairColorID,
                          eyes = p.PersonalInfo_EyeColorID,
                          edu = p.OtherDetails_EducationID,
                          child = p.PersonalInfo_ChildrenID,
                          nation = p.PersonalInfo_EthnicityID,
                          religion = p.PersonalInfo_ReligionID,
                          smoke = p.PersonalInfo_SmokingHabitID,
                          drink = p.PersonalInfo_DrinkingHabitID,
                          job = p.OtherDetails_Occupation,
                          income = p.OtherDetails_AnnualIncomeID,
                          family = p.LookingFor_RelationshipStatusID,
                          lookfor = 3 - p.GenderId,

                          dating_shorterm = p.LookingFor_TypeOfDating_ShortTermRelationship,
                          dating_married = p.LookingFor_TypeOfDating_MarriedDating,
                          dating_benefits = p.LookingFor_TypeOfDating_MutuallyBeneficialArrangements,
                          dating_longterm = p.LookingFor_TypeOfDating_LongTermRelationship,
                          dating_friensdhip = p.LookingFor_TypeOfDating_Friendship,
                          dating_adult = p.LookingFor_TypeOfDating_AdultDating_Casual,

                          aboutme_head = p.AboutMe_Heading,
                          aboutme_descr = p.AboutMe_DescribeYourself,
                          aboutme_date = p.AboutMe_DescribeAnIdealFirstDate,
                          occupation = p.OtherDetails_Occupation,

                          availbale = Convert.ToString(ServiceBase.getAvailableCredits(id, false)),
                          updated = p.LastUpdateProfileDateTime.Value.ToShortDateString(),

                          referrerparentid = p.ReferrerParentId
                      }).FirstOrDefault();
            
            if (pr == null) {
                throw new ProfileNotFoundException(id);
            }

            string datingInfo = "</br>";
            try {
                // Dating.Server.Datasets.DLL.DSLists _gDSLists = Dating.Server.Datasets.DLL.DataHelpers.GetEUS_LISTS();
                for (int i = 0; i < Lists.gDSLists.EUS_LISTS_TypeOfDating.Rows.Count - 1; i++) {
                    TypeOfDatingEnum enm = (TypeOfDatingEnum)Lists.gDSLists.EUS_LISTS_TypeOfDating.Rows[i]["TypeOfDatingId"];
                    switch (enm) {
                        case TypeOfDatingEnum.AdultDating_Casual:
                            if (clsNullable.NullTo(pr.dating_adult)) {
                                datingInfo += Lists.gDSLists.EUS_LISTS_TypeOfDating.Rows[i][langId].ToString() + "</br>";
                            }
                            break;

                        case TypeOfDatingEnum.Friendship:
                            if (clsNullable.NullTo(pr.dating_friensdhip)) {
                                datingInfo += Lists.gDSLists.EUS_LISTS_TypeOfDating.Rows[i][langId].ToString() + "</br>";

                            }
                            break;

                        case TypeOfDatingEnum.LongTermRelationship:
                            if (clsNullable.NullTo(pr.dating_longterm)) {
                                datingInfo += Lists.gDSLists.EUS_LISTS_TypeOfDating.Rows[i][langId].ToString() + "</br>";
                            }
                            break;

                        case TypeOfDatingEnum.MarriedDating:
                            if (clsNullable.NullTo(pr.dating_married)) {
                                datingInfo += Lists.gDSLists.EUS_LISTS_TypeOfDating.Rows[i][langId].ToString() + "</br>";
                            }
                            break;

                        case TypeOfDatingEnum.MutuallyBeneficialArrangements:
                            if (clsNullable.NullTo(pr.dating_benefits)) {
                                datingInfo += Lists.gDSLists.EUS_LISTS_TypeOfDating.Rows[i][langId].ToString() + "</br>";
                            }
                            break;

                        case TypeOfDatingEnum.ShortTermRelationship:
                            if (clsNullable.NullTo(pr.dating_shorterm)) {
                                datingInfo += Lists.gDSLists.EUS_LISTS_TypeOfDating.Rows[i][langId].ToString() + "</br>";

                            }
                            break;

                    }
                }
            }
            catch (Exception ex) {
                ServiceBase.WebErrorSendEmail(ex, "GetUserDetails (ProfileID:" + id + ", lang:" + langId + ")");
            }

            details.mirrorprofileid = pr.mirrorprofileid.ToString();
            details.ismaster = pr.ismaster.ToString();
            details.status = pr.status.ToString();
            details.birthdate = pr.birth.Value.ToShortDateString();
            details.age = ProfileHelper.GetCurrentAge(pr.birth.Value).ToString();
            details.bodytype = ProfileHelper.GetBodyTypeString(pr.bodytype, langId);
            details.haireyes = ProfileHelper.GetHairColorString(pr.hair, langId) + "/" + ProfileHelper.GetEyeColorString(pr.eyes, langId);
            details.education = ProfileHelper.GetEducationString(pr.edu, langId);
            details.childs = ProfileHelper.GetChildrenNumberString(pr.child, langId);
            details.nationality = ProfileHelper.GetEthnicityString(pr.nation, langId);
            details.religion = ProfileHelper.GetReligionString(pr.religion, langId);
            details.smokedring = ProfileHelper.GetSmokingString(pr.smoke, langId) + "/" + ProfileHelper.GetDrinkingString(pr.drink, langId);
            details.job = pr.job;
            details.income = ProfileHelper.GetIncomeString(pr.income, langId);
            details.familystate = ProfileHelper.GetRelationshipStatusString(pr.family, langId);
            details.lookfor = ProfileHelper.GetGenderString(pr.lookfor, langId);
            details.interest = datingInfo;
            details.aboutme_describedate = pr.aboutme_date;
            details.aboutme_describeyourself = pr.aboutme_descr;
            details.aboutme_heading = pr.aboutme_head;
            details.occupation = pr.occupation;
            details.available_credits = Convert.ToString(pr.availbale);
            details.profileupdated = pr.updated;
            details.zodiac = ProfileHelper.ZodiacName(pr.birth.Value);
            details.referrerparentid = clsNullable.NullTo(pr.referrerparentid).ToString();

            return details;
        }

    }

}

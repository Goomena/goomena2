﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.ServiceModel;
using Dating.Server.Core.DLL;

namespace GoomenaLib.Admin
{

    [DataContract]
    public class UserProfile
    {

        [DataMember]
        public string FirstName { get; set; }
        [DataMember]
        public string LastName { get; set; }
        [DataMember]
        public string ProfileId { get; set; }
        [DataMember]
        public string UserName { get; set; }
        [DataMember]
        public string UserAge { get; set; }
        [DataMember]
        public string UserCity { get; set; }
        [DataMember]
        public string UserRegion { get; set; }
        [DataMember]
        public string DateRegister { get; set; }
        [DataMember]
        public string GenderId { get; set; }
        [DataMember]
        public string Password { get; set; }
        [DataMember]
        public string ZipCode { get; set; }
        [DataMember]
        public string Latitude { get; set; }
        [DataMember]
        public string Longtitude { get; set; }
        [DataMember]
        public string ImageUrl { get; set; }
        [DataMember]
        public string ReferrerParentId { get; set; }
        [DataMember]
        public string HasCredits { get; set; }
        [DataMember]
        public string UserCountry { get; set; }
        [DataMember]
        public string IsOnline { get; set; }
        [DataMember]
        public string PhotosCount { get; set; }
        [DataMember]
        public string LastActivityDateTime { get; set; }


        public void ReadRow(Dating.Admin.Datasets.DLL.DSMembersAdmin.EUS_ProfilesRow row) {
            this.PhotosCount = "0";
            this.ImageUrl = "";
            this.IsOnline = "0";
            if (!row.IsFirstNameNull()) this.FirstName = row.FirstName;
            if (!row.IsLastNameNull()) this.LastName = row.LastName;
            this.ProfileId = row.ProfileID.ToString();
            this.UserName = row.LoginName;
            if (!row.IsBirthdayNull()) this.UserAge = ProfileHelper.GetCurrentAge(row.Birthday).ToString();
            if (!row.IsCityNull()) this.UserCity = row.City;
            if (!row.Is_RegionNull()) this.UserRegion = row._Region;
            this.DateRegister = Utils.GetDateWithOffset(row.DateTimeToRegister).ToString("yyyyMMdd HH:mm");
            this.GenderId = row.GenderId.ToString();
            this.Password = "";// row.Password;
            this.ZipCode = row.Zip;

            this.ReferrerParentId = "0";
            if (!row.IsReferrerParentIdNull()) this.ReferrerParentId = row.ReferrerParentId.ToString();

            //this.HasCredits = "0";
            //if (!row.IsHasCreditsNull()) this.HasCredits = row.HasCredits.ToString().ToLower();
            this.HasCredits = (row.IsHasCreditsNull() ? "0" : (row.HasCredits ? "1" : "0"));

            if (!row.IsCountryNull()) this.UserCountry = ProfileHelper.GetCountryName(row.Country);
            if (!row.IsLastActivityDateTimeNull()) this.LastActivityDateTime = Utils.GetDateWithOffset(row.LastActivityDateTime).ToString("yyyyMMdd HH:mm:ss");
        }

        public void ReadRow(Dating.Admin.Datasets.DLL.DSMembersAdmin.EUS_Profiles_NewRow row) {
            this.PhotosCount = "0";
            this.ImageUrl = "";
            this.IsOnline = "0";
            if (!row.IsFirstNameNull()) this.FirstName = row.FirstName;
            if (!row.IsLastNameNull()) this.LastName = row.LastName;
            this.ProfileId = row.ProfileID.ToString();
            this.UserName = row.LoginName;
            if (!row.IsBirthdayNull()) this.UserAge = ProfileHelper.GetCurrentAge(row.Birthday).ToString();
            if (!row.IsCityNull()) this.UserCity = row.City;
            if (!row.Is_RegionNull()) this.UserRegion = row._Region;
            this.DateRegister = Utils.GetDateWithOffset(row.DateTimeToRegister).ToString("yyyyMMdd HH:mm");
            this.GenderId = row.GenderId.ToString();
            this.Password = "";// row.Password;
            this.ZipCode = row.Zip;

            this.ReferrerParentId = "0";
            if (!row.IsReferrerParentIdNull()) this.ReferrerParentId = row.ReferrerParentId.ToString();

            //this.HasCredits = "0";
            //if (!row.IsHasCreditsNull()) this.HasCredits = row.HasCredits.ToString().ToLower();
            this.HasCredits = (row.IsHasCreditsNull() ? "0" : (row.HasCredits ? "1" : "0"));

            if (!row.IsCountryNull()) this.UserCountry = ProfileHelper.GetCountryName(row.Country);
            if (!row.IsLastActivityDateTimeNull()) this.LastActivityDateTime = Utils.GetDateWithOffset(row.LastActivityDateTime).ToString("yyyyMMdd HH:mm:ss");
        }


        public static bool GetIsOnlineNow(int members_online_minutes, bool isOnline, DateTime LastActivityDateTime) {
            bool IsOnlineNow = false;
            DateTime utcDate = DateTime.UtcNow.AddMinutes(-members_online_minutes);

            // the user's last activity date should be greater or equal to datetime 20 minutes before, for example
            // so we could say user is online
            if (LastActivityDateTime >= utcDate && isOnline == true) {
                IsOnlineNow = true;
            }
            return IsOnlineNow;
        }
    }

}

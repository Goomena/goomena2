﻿using Dating.Server.Core.DLL;
using Dating.Server.Datasets.DLL;
using Dating.Server.Datasets.DLL.DSMembersTableAdapters;
using Dating.Server.Datasets.DLL.DSSYSDataTableAdapters;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Web;
using System.Text;
using System.Web;


namespace GoomenaLib.Admin
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    public class AdminService : AdminBaseService, IAdminService
    {

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "login")]
        public override LoginHeaders login(AdminUser user) {
            LoginHeaders headers = base.login(user);
            return headers;
        }


        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "getupdating")]
        public UserProfilesList getProfilesUpdating(LoginHeaders headers) {

            int iProfileID = 0;
            int AdminUserID = 0;

            UserProfilesList profiles = null;
            try {

                AdminUserID = Convert.ToInt32(headers.UserID);
                if (!CheckHeaders(ref headers))
                    return null;
                AdminUserID = Convert.ToInt32(headers.UserID);

                if (!UserRightsHelper.CheckIsAllowed(AdminUserID, "ProfilesUpdatingProfiles"))
                    return null;


                Dating.Server.WebServices.Web.ProfilesRetrieveParams prms = new Dating.Server.WebServices.Web.ProfilesRetrieveParams();
                prms.NonMasterRecords = true;
                prms.MasterRecords = false;
                prms.Status = ((int)ProfileStatusEnum.Updating);
                prms.UseParams = true;
                prms.RecordsToSelect = 500;
                prms.PreviousRowNumber = -1;

                Dating.Admin.Datasets.DLL.DSMembersAdmin ds = new Dating.Admin.Datasets.DLL.DSMembersAdmin();
                //Dating.Admin.Core.DLL.clsWSHeaders headersWS = (Dating.Admin.Core.DLL.clsWSHeaders)headers;
                Dating.Admin.Core.DLL.clsWSHeaders headersWS = headers.GetWSHeaders();
                Dating.Admin.Core.DLL.clsDataRecordErrorsReturn ret = gAdminWs.GetEUS_ProfilesWithParams_New2(ref headersWS, ref ds, ref prms);
                headers.ReadWSHeaders(headersWS);

                int mins = Utils.GetMemberOnlineMins();
                profiles = new UserProfilesList();
                for (int c = 0; c < ds.EUS_Profiles_New.Rows.Count; c++) {
                    Dating.Admin.Datasets.DLL.DSMembersAdmin.EUS_Profiles_NewRow row =
                        (Dating.Admin.Datasets.DLL.DSMembersAdmin.EUS_Profiles_NewRow)ds.EUS_Profiles_New.Rows[c];
                    UserProfile profile = new UserProfile();
                    profile.ReadRow(row);
                    if (!row.IsLastActivityDateTimeNull()) {
                        //profile.IsOnline = UserProfile.GetIsOnlineNow(mins, row.IsOnline, row.LastActivityDateTime).ToString();
                        bool IsOnline = UserProfile.GetIsOnlineNow(mins, row.IsOnline, row.LastActivityDateTime);
                        profile.IsOnline = (IsOnline ? 1 : 0).ToString();

                    }
                    profiles.Profiles.Add(profile);
                }


                profiles.Profiles = (from itm in profiles.Profiles
                                     orderby itm.LastActivityDateTime descending
                                     select itm).ToList();

            }
            catch (Exception ex) {
                WebErrorSendEmail(ex, "getUpdatingProfiles");
            }
            finally {
                try {
                    ServiceBase.LogAdminAction(AdminUserID, iProfileID, "Load profiles", "Load profiles");
                }
                catch (Exception ex) {
                    WebErrorSendEmail(ex, "getUpdatingProfiles->LOG");
                }
            }


            return profiles;
        }


        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "getonline")]
        public UserProfilesList getProfilesOnline(LoginHeaders headers) {

            int iProfileID = 0;
            int AdminUserID = 0;

            UserProfilesList profiles = null;
            try {

                AdminUserID = Convert.ToInt32(headers.UserID);
                if (!CheckHeaders(ref headers))
                    return null;
                AdminUserID = Convert.ToInt32(headers.UserID);

                if (!UserRightsHelper.CheckIsAllowed(AdminUserID, "ProfilesMembersOnline"))
                    return null;

                Dating.Server.WebServices.Web.ProfilesRetrieveParams prms = new Dating.Server.WebServices.Web.ProfilesRetrieveParams();
                prms.NonMasterRecords = true;
                prms.MasterRecords = false;
                prms.Status = ((int)ProfileStatusEnum.MembersOnline);
                prms.UseParams = true;
                prms.RecordsToSelect = 500;
                prms.PreviousRowNumber = -1;

                Dating.Admin.Datasets.DLL.DSMembersAdmin ds = new Dating.Admin.Datasets.DLL.DSMembersAdmin();
                //Dating.Admin.Core.DLL.clsWSHeaders headersWS = (Dating.Admin.Core.DLL.clsWSHeaders)headers;

                Dating.Admin.Core.DLL.clsWSHeaders headersWS = headers.GetWSHeaders();
                Dating.Admin.Core.DLL.clsDataRecordErrorsReturn ret = gAdminWs.GetEUS_ProfilesWithParams_New2(ref headersWS, ref ds, ref prms);
                headers.ReadWSHeaders(headersWS);


                int mins = Utils.GetMemberOnlineMins();
                profiles = new UserProfilesList();
                for (int c = 0; c < ds.EUS_Profiles_New.Rows.Count; c++) {
                    Dating.Admin.Datasets.DLL.DSMembersAdmin.EUS_Profiles_NewRow row =
                        (Dating.Admin.Datasets.DLL.DSMembersAdmin.EUS_Profiles_NewRow)ds.EUS_Profiles_New.Rows[c];
                    UserProfile profile = new UserProfile();
                    profile.ReadRow(row);
                    if (!row.IsLastActivityDateTimeNull()) {
                        bool IsOnline = UserProfile.GetIsOnlineNow(mins, row.IsOnline, row.LastActivityDateTime);
                        profile.IsOnline = (IsOnline ? 1 : 0).ToString();
                    }
                    profiles.Profiles.Add(profile);
                }


                profiles.Profiles = (from itm in profiles.Profiles
                                     orderby itm.LastActivityDateTime descending
                                     select itm).ToList();

            }
            catch (Exception ex) {
                WebErrorSendEmail(ex, "getOnlineProfiles");
            }
            finally {
                try {
                    ServiceBase.LogAdminAction(AdminUserID, iProfileID, "Load profiles", "Load profiles");
                }
                catch (Exception ex) {
                    WebErrorSendEmail(ex, "getOnlineProfiles->LOG");
                }
            }


            return profiles;
        }


        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "getwaitingreferrers")]
        public UserProfileReferrerList getReferrersWaiting(LoginHeaders headers) {

            int iProfileID = 0;
            int AdminUserID = 0;

            UserProfileReferrerList profiles = null;
            try {

                AdminUserID = Convert.ToInt32(headers.UserID);
                if (!CheckHeaders(ref headers))
                    return null;
                AdminUserID = Convert.ToInt32(headers.UserID);

                if (!UserRightsHelper.CheckIsAllowed(AdminUserID, "ReferrersWaitingReferrers"))
                    return null;


                Dating.Server.WebServices.Web.ProfilesRetrieveParams prms = new Dating.Server.WebServices.Web.ProfilesRetrieveParams();
                prms.UseParams = true;
                prms.NonMasterRecords = true;
                prms.MasterRecords = true;
                prms.Status = (int)Dating.Admin.Core.DLL.ProfileStatusEnum.NewProfile;
                prms.RecordsToSelect = 200;
                prms.PreviousRowNumber = -1;


                Dating.Admin.Referrals.Core.DLL.DSReferrers ds = new Dating.Admin.Referrals.Core.DLL.DSReferrers();
                Dating.Admin.Core.DLL.clsWSHeaders headersWS = headers.GetWSHeaders();
                Dating.Admin.Core.DLL.clsDataRecordErrorsReturn ret = gAdminReferrersWS.GetREF_ReferrerProfiles_New(ref headersWS, ref ds, prms);
                headers.ReadWSHeaders(headersWS);


                int mins = Utils.GetMemberOnlineMins();
                profiles = new UserProfileReferrerList();
                int recCount = ds.GetReferrerProfiles_ADMIN.Rows.Count;
                for (int c = 0; c < recCount; c++) {
                    Dating.Admin.Referrals.Core.DLL.DSReferrers.GetReferrerProfiles_ADMINRow row =
                        (Dating.Admin.Referrals.Core.DLL.DSReferrers.GetReferrerProfiles_ADMINRow)ds.GetReferrerProfiles_ADMIN.Rows[c];
                    UserProfileReferrer profile = new UserProfileReferrer();
                    profile.ReadRow(row);
                    profiles.Profiles.Add(profile);
                }


                profiles.Profiles = (from itm in profiles.Profiles
                                     orderby itm.DateRegister descending
                                     select itm).ToList();

            }
            catch (Exception ex) {
                WebErrorSendEmail(ex, "getReferrersWaiting");
            }
            finally {
                try {
                    ServiceBase.LogAdminAction(AdminUserID, iProfileID, "Load waiting referrers", "Load waiting referrers");
                }
                catch (Exception ex) {
                    WebErrorSendEmail(ex, "getReferrersWaiting->LOG");
                }
            }


            return profiles;
        }


        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "referreramount/{profileid}")]
        public decimal getReferrerAmount(LoginHeaders headers, string profileid) {
            decimal amount = 0;
            int iProfileID = 0;
            string ProfileLoginName = "";
            int AdminUserID = 0;
            try {

                AdminUserID = Convert.ToInt32(headers.UserID);
                iProfileID = Convert.ToInt32(profileid);

                if (!CheckHeaders(ref headers))
                    return amount;
                AdminUserID = Convert.ToInt32(headers.UserID);

                if (!UserRightsHelper.CheckIsAllowed(AdminUserID, "ProfileCartTabAccountReferrerPayments"))
                    return amount;

                int refParrentID = Dating.Server.Core.DLL.DataHelpers.GetEUS_Profiles_ReferrerParentId(iProfileID);
                if (refParrentID < 1)
                    return amount;


                ProfileLoginName = Dating.Server.Core.DLL.DataHelpers.GetEUS_Profile_LoginName_ByProfileID(iProfileID);
                DataSet referrerCredits = new DataSet();

                Dating.Server.WebServices.Web.ReferrersRetrieveParams prms = new Dating.Server.WebServices.Web.ReferrersRetrieveParams();
                prms.AffiliateId = iProfileID;
                prms.LoginName = ProfileLoginName;
                prms.DateFrom = new DateTime(2008, 1, 1);
                prms.DateTo = DateTime.UtcNow.AddDays(1);


                Dating.Admin.Core.DLL.clsWSHeaders headersWS = headers.GetWSHeaders();
                gAdminWs.GetReferrerCredits(ref headersWS, ref referrerCredits, prms);
                headers.ReadWSHeaders(headersWS);



                decimal AvailableAmount = 0;
                if ((referrerCredits.Tables.Count > 0)) {
                    DataTable dt = referrerCredits.Tables[0];

                    if (dt.Rows.Count > 0) {
                        decimal LastPaymentAmount = clsNullable.DBNullToDecimal(dt.Rows[0]["LastPaymentAmount"]);
                        decimal TotalDebitAmount = clsNullable.DBNullToDecimal(dt.Rows[0]["TotalDebitAmount"]);
                        decimal totalMoneyToBePaid = clsNullable.DBNullToDecimal(dt.Rows[0]["totalMoneyToBePaid"]);
                        AvailableAmount = clsNullable.DBNullToDecimal(dt.Rows[0]["AvailableAmount"]);
                        decimal PendingAmount = clsNullable.DBNullToDecimal(dt.Rows[0]["PendingAmount"]);
                        if (AvailableAmount < 0) {
                            PendingAmount = PendingAmount + AvailableAmount;
                            AvailableAmount = 0;
                        }

                    }
                }

                amount = Math.Round(AvailableAmount, 2);
            }
            catch (Exception ex) {
                WebErrorSendEmail(ex, "getReferrerAmount (profileid:" + profileid + ")");
            }
            finally {
                myContext.Dispose();

                try {
                    ServiceBase.LogAdminAction(AdminUserID, iProfileID, "Referrer Amount", "Referrer Amount" + Environment.NewLine + "Profile login name: " + ProfileLoginName);
                }
                catch (Exception ex) {
                    WebErrorSendEmail(ex, "getReferrerAmount->LOG");
                }
            }


            return amount;
        }

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "userprofiledetails/{profileid}")]
        public UserDetails getUserProfileDetails(LoginHeaders headers, string profileid) {
            string langId = "US";
            UserDetails details = null;
            int iProfileID = 0;
            string ProfileLoginName = "";
            int AdminUserID = 0;
            try {

                AdminUserID = Convert.ToInt32(headers.UserID);
                iProfileID = Convert.ToInt32(profileid);

                if (!CheckHeaders(ref headers))
                    return null;
                AdminUserID = Convert.ToInt32(headers.UserID);

                if (!UserRightsHelper.CheckIsAllowed(AdminUserID, "ProfilesManagerEdit"))
                    return null;


                ProfileLoginName = DataHelpers.GetEUS_Profile_LoginName_ByProfileID(iProfileID);
                details = UserDetails.GetUserDetails(myContext, iProfileID, langId);

            }
            catch (Exception ex) {
                WebErrorSendEmail(ex, "getUserProfileDetails (profileid:" + profileid + ")");
            }
            finally {
                myContext.Dispose();

                try {
                    ServiceBase.LogAdminAction(AdminUserID, iProfileID, "Load profile", "Load profile" + Environment.NewLine + "Profile login name: " + ProfileLoginName);
                }
                catch (Exception ex) {
                    WebErrorSendEmail(ex, "getUserProfileDetails->LOG");
                }
            }


            return details;
        }


        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "setuserprofiledetails/{profileid}", BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public string setUserProfileDetails(UserProfileDetailsParams prms, string profileid) {
            LoginHeaders headers = prms.LoginHeaders;
            UserDetails lp = prms.UserDetails;

            string result = "-ok-";
            int iProfileID = 0;
            string ProfileLoginName = "";
            int AdminUserID = 0;
            try {

                AdminUserID = Convert.ToInt32(headers.UserID);
                iProfileID = Convert.ToInt32(profileid);

                if (!CheckHeaders(ref headers))
                    return "unauthorized";
                AdminUserID = Convert.ToInt32(headers.UserID);

                if (!UserRightsHelper.CheckIsAllowed(AdminUserID, "ProfilesManagerEdit"))
                    return "unauthorized";

                Dating.Server.Datasets.DLL.EUS_Profile pr = (from p in myContext.EUS_Profiles
                                                             where p.ProfileID == iProfileID
                                                             select p).FirstOrDefault();

                Dating.Server.Datasets.DLL.EUS_Profile prMirror = (from p in myContext.EUS_Profiles
                                                                   where p.MirrorProfileID == iProfileID
                                                                   select p).FirstOrDefault();

                if (pr != null || prMirror != null) {

                    if (pr != null)
                        ProfileLoginName = pr.LoginName;
                    else if (prMirror != null)
                        ProfileLoginName = prMirror.LoginName;


                    Dating.Server.Datasets.DLL.EUS_Profile prUpdating = null;
                    //if is updating record, then save data only to mirror (updating) record
                    if (prMirror != null && prMirror.Status == (int)ProfileStatusEnum.Updating)
                        prUpdating = prMirror;
                    else if (pr != null && pr.Status == (int)ProfileStatusEnum.Updating)
                        prUpdating = pr;


                    if (prUpdating != null) {
                        setEUS_ProfileFields(ref  prUpdating, ref lp);
                    }
                    else {
                        // none record is updating, so update both recors

                        if (pr != null)
                            setEUS_ProfileFields(ref  pr, ref lp);

                        if (prMirror != null)
                            setEUS_ProfileFields(ref  prMirror, ref lp);

                    }


                    //if (pr.MirrorProfileID > 1) {
                    //    // update master record
                    //    var masterR = (from p in myContext.EUS_Profiles
                    //                   where p.ProfileID == pr.MirrorProfileID
                    //                   select p).FirstOrDefault();

                    //    if (masterR != null) {
                    //        masterR.OtherDetails_AnnualIncomeID = pr.OtherDetails_AnnualIncomeID;
                    //        masterR.OtherDetails_EducationID = pr.OtherDetails_EducationID;
                    //    }
                    //}

                    myContext.SubmitChanges();

                    result = "-ok-";
                }
                else {
                    result = "-failed-";
                }

            }
            catch (Exception ex) {
                if (CurrentSettings.AllowSendExceptions)
                    result = ex.ToString();
                else
                    result = "-exception-";

                WebErrorSendEmail(ex, "setUserProfileDetails");
            }
            finally {
                myContext.Dispose();

                try {
                    ServiceBase.LogAdminAction(AdminUserID, iProfileID, "Saving profile changes", "Saving profile changes" + Environment.NewLine + "Profile login name: " + ProfileLoginName);
                }
                catch (Exception ex) {
                    WebErrorSendEmail(ex, "setUserProfileDetails->LOG");
                }
            }
            return result;
        }


        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "getnewphotos/{profileid}")]
        public ImagePathList getNewPhotosD350(LoginHeaders headers, string profileid) {
            ImagePathList imgPathList = new ImagePathList();
            int iProfileID = 0;
            string ProfileLoginName = "";
            int AdminUserID = 0;
            try {

                AdminUserID = Convert.ToInt32(headers.UserID);
                iProfileID = Convert.ToInt32(profileid);

                if (!CheckHeaders(ref headers))
                    return null;
                AdminUserID = Convert.ToInt32(headers.UserID);

                if (!UserRightsHelper.CheckIsAllowed(AdminUserID, "Photos"))
                    return null;

                ProfileLoginName = DataHelpers.GetEUS_Profile_LoginName_ByProfileID(iProfileID);

                Dating.Server.Datasets.DLL.DSMembers.EUS_CustomerPhotosDataTable dtPhotos = DataHelpers.GetEUS_CustomerPhotos_ByProfileOrMirrorID(iProfileID).EUS_CustomerPhotos;
                foreach (DSMembers.EUS_CustomerPhotosRow row in dtPhotos) {

                    bool isDeclined = (!row.IsHasDeclinedNull() && row.HasDeclined);
                    bool isApproved = (!row.IsHasAprovedNull() && row.HasAproved);
                    bool isDeleted = (!row.IsIsDeletedNull() && row.IsDeleted);
                    bool isAutoApprovedPhoto = (!row.IsIsAutoApprovedNull() && row.IsAutoApproved);
                    bool isUpdating = (!row.IsIsUpdatingNull() && row.IsUpdating);

                    if (isDeleted) continue;
                    if ((!isDeclined && !isApproved) || isAutoApprovedPhoto || isUpdating) {

                        string path = ServiceBase.getImagePathMyProfile((int)row["CustomerID"], row["FileName"].ToString(), 0, false, PhotoSize.D350);
                        ImagePath p = new ImagePath()
                        {
                            Path = path,
                            PhotoId = row["CustomerPhotosId"].ToString(),
                            Level = row.DisplayLevel.ToString()
                        };
                        p.HasAproved = (row.IsHasAprovedNull() ? false : row.HasAproved).ToString();
                        p.HasDeclined = (row.IsHasDeclinedNull() ? false : row.HasDeclined).ToString();
                        p.IsDefault = (row.IsIsDefaultNull() ? false : row.IsDefault).ToString();
                        p.IsDeleted = (row.IsIsDeletedNull() ? false : row.IsDeleted).ToString();
                        p.ShowOnFrontPage = (row.IsShowOnFrontPageNull() ? false : row.ShowOnFrontPage).ToString();
                        p.ShowOnGrid = (row.IsShowOnGridNull() ? false : row.ShowOnGrid).ToString();
                        p.IsUpdating = (row.IsIsUpdatingNull() ? false : row.IsUpdating).ToString();

                        imgPathList.PathList.Add(p);
                    }

                }

                imgPathList.PathList = imgPathList.PathList.OrderByDescending(itm => itm.IsUpdating).ToList();
            }
            catch (Exception ex) {
                WebErrorSendEmail(ex, "getNewPhotosD350");
            }
            finally {
                myContext.Dispose();

                try {
                    ServiceBase.LogAdminAction(AdminUserID, iProfileID, "Load profile photos", "Load profile photos" + Environment.NewLine + "Profile login name: " + ProfileLoginName);
                }
                catch (Exception ex) {
                    WebErrorSendEmail(ex, "getNewPhotosD350->LOG");
                }
            }


            return imgPathList;
        }


        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "approveprofile/{profileid}")]
        public string approveProfile(LoginHeaders headers, string profileid) {
            StringBuilder sb = new StringBuilder();
            int iProfileID = 0;
            string ProfileLoginName = "";
            int AdminUserID = 0;
            clsApprovedProfileResult result1 = new clsApprovedProfileResult();
            try {

                AdminUserID = Convert.ToInt32(headers.UserID);
                iProfileID = Convert.ToInt32(profileid);

                if (!CheckHeaders(ref headers))
                    return "unauthorized";
                AdminUserID = Convert.ToInt32(headers.UserID);

                if (!UserRightsHelper.CheckIsAllowed(AdminUserID, "ProfileCartActionsApprove"))
                    return "unauthorized";

                ProfileLoginName = DataHelpers.GetEUS_Profile_LoginName_ByProfileID(iProfileID);
                result1 = AdminActions.ApproveProfile(iProfileID);

                if (result1.IsSuccessful) {

                    //sb.AppendLine("Profile approved.");

                    if (result1.IsNewProfile) {
                        clsUserNotifications.SendNotificationsToMembers_AboutNewMember(iProfileID, false);
                    }


                    //sb.AppendLine("Checking new photos....");
                    // check for new photos, and approve them
                    ///''''''''''''''''''''''''''''''''''''''
                    DSMembersViews ds = new DSMembersViews();
                    AdminCounters.GetAdminCountersPhotos(ref ds, iProfileID);

                    if (ds.GetAdminCountersPhotos.Rows.Count > 0) {
                        DSMembersViews.GetAdminCountersPhotosRow row =
                            (DSMembersViews.GetAdminCountersPhotosRow)ds.GetAdminCountersPhotos.Rows[0];
                        if (row.NewPhotos > 0) {

                            DataHelpers.EUS_CustomerPhotos_ApproveNewPhotos(iProfileID);
                            //sb.AppendLine("New photos approved.");

                            clsSetDefaultPhotoResult result2 = AdminActions.SetDefaultEUS_CustomerPhotos(-1, iProfileID);
                            //if (result2.IsSuccessful)
                            //    sb.AppendLine("New default photo set.");
                            //else
                            //    sb.AppendLine("New default photo NOT set.");


                        }
                    }
                    sb.AppendLine("-ok-");
                }
                else {
                    sb.AppendLine("-failed-");
                }





            }
            catch (Exception ex) {
                if (CurrentSettings.AllowSendExceptions)
                    sb.AppendLine(ex.ToString());
                else
                    sb.AppendLine("-exception-");

                WebErrorSendEmail(ex, "approveProfile");
            }
            finally {
                myContext.Dispose();

                try {
                    if (result1.IsNewProfile) {
                        ServiceBase.LogAdminAction(AdminUserID, iProfileID, "Approve New Profile", "Approve New Profile" + Environment.NewLine + "Profile login name: " + ProfileLoginName);
                    }
                    else {
                        ServiceBase.LogAdminAction(AdminUserID, iProfileID, "Approve Profile changes", "Approve Profile changes" + Environment.NewLine + "Profile login name: " + ProfileLoginName);
                    }
                }
                catch (Exception ex) {
                    WebErrorSendEmail(ex, "approveProfile->LOG");
                }
            }


            return sb.ToString().Trim();
        }

        //[WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "rejectprofile/{profileid}")]
        //public string rejectProfile(LoginHeaders headers, string profileid)
        //{
        //    string result = "-ok-";
        //    int iProfileID = 0;
        //    string ProfileLoginName = "";
        //    int AdminUserID = 0;
        //    clsRejectedProfileResult result1 = new clsRejectedProfileResult();
        //    try
        //    {

        //        AdminUserID = Convert.ToInt32(headers.UserID);
        //        iProfileID = Convert.ToInt32(profileid);

        //        if (!CheckHeaders(ref headers))
        //            return "unauthorized";
        //        AdminUserID = Convert.ToInt32(headers.UserID);

        //        if (!UserRightsHelper.CheckIsAllowed(AdminUserID, "ProfileCartActionsReject"))
        //            return "unauthorized";


        //        ProfileLoginName = DataHelpers.GetEUS_Profile_LoginName_ByProfileID(iProfileID);

        //        bool isAnyNewPhoto = false;
        //        Dating.Server.Datasets.DLL.DSMembers.EUS_CustomerPhotosDataTable dtPhotos = DataHelpers.GetEUS_CustomerPhotos_ByProfileOrMirrorID(iProfileID).EUS_CustomerPhotos;
        //        foreach (DSMembers.EUS_CustomerPhotosRow row in dtPhotos)
        //        {

        //            bool isDeclined = (!row.IsHasDeclinedNull() && row.HasDeclined);
        //            bool isApproved = (!row.IsHasAprovedNull() && row.HasAproved);
        //            bool isDeleted = (!row.IsIsDeletedNull() && row.IsDeleted);
        //            bool isAutoApprovedPhoto = (!row.IsIsAutoApprovedNull() && row.IsAutoApproved);
        //            bool isUpdating = (!row.IsIsUpdatingNull() && row.IsUpdating);

        //            if (isDeleted) continue;
        //            if (isDeclined) continue;

        //            if ((!isDeclined && !isApproved) || isAutoApprovedPhoto || isUpdating)
        //            {
        //                isAnyNewPhoto = true;
        //                break;
        //            }

        //        }
        //        if (!isAnyNewPhoto)
        //        {
        //            result1 = AdminActions.RejectProfile(iProfileID);
        //            if (!result1.IsSuccessful)
        //                result = "-failed-";
        //        }
        //        else
        //        {
        //            result = "-failed-check new photos-";
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        if (CurrentSettings.AllowSendExceptions)
        //            result = ex.ToString();
        //        else
        //            result = "-exception-";
        //        WebErrorSendEmail(ex, "rejectProfile");
        //    }
        //    finally
        //    {
        //        myContext.Dispose();

        //        try
        //        {
        //            if (result1.IsNewProfile)
        //            {
        //                ServiceBase.LogAdminAction(AdminUserID, iProfileID, "Reject new profile", "Reject new profile" + Environment.NewLine + "Profile login name: " + ProfileLoginName);
        //            }
        //            else
        //            {
        //                ServiceBase.LogAdminAction(AdminUserID, iProfileID, "Reject profile changes", "Reject profile changes" + Environment.NewLine + "Profile login name: " + ProfileLoginName);
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            WebErrorSendEmail(ex, "rejectProfile->LOG");
        //        }
        //    }


        //    return result;
        //}
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "rejectprofile/{profileid}")]
        public string rejectProfile(LoginHeaders headers, string profileid) {
            string result = "-ok-";
            int iProfileID = 0;
            string ProfileLoginName = "";
            int AdminUserID = 0;
            clsRejectedProfileResult result1 = new clsRejectedProfileResult();
            try {

                AdminUserID = Convert.ToInt32(headers.UserID);
                iProfileID = Convert.ToInt32(profileid);

                if (!CheckHeaders(ref headers))
                    return "unauthorized";
                AdminUserID = Convert.ToInt32(headers.UserID);

                if (!UserRightsHelper.CheckIsAllowed(AdminUserID, "ProfileCartActionsReject"))
                    return "unauthorized";


                ProfileLoginName = DataHelpers.GetEUS_Profile_LoginName_ByProfileID(iProfileID);
                
                bool ProblemRejecting = false;
                Dating.Server.Datasets.DLL.DSMembers.EUS_CustomerPhotosDataTable dtPhotos = DataHelpers.GetEUS_CustomerPhotos_ByProfileOrMirrorID(iProfileID).EUS_CustomerPhotos;
                foreach (DSMembers.EUS_CustomerPhotosRow row in dtPhotos) {

                    bool isDeclined = (!row.IsHasDeclinedNull() && row.HasDeclined);
                    bool isApproved = (!row.IsHasAprovedNull() && row.HasAproved);
                    bool isDeleted = (!row.IsIsDeletedNull() && row.IsDeleted);
                    bool isAutoApprovedPhoto = (!row.IsIsAutoApprovedNull() && row.IsAutoApproved);
                    bool isUpdating = (!row.IsIsUpdatingNull() && row.IsUpdating);
                    
                    if (isDeleted) continue;
                    if (isDeclined) continue;

                    if ((!isDeclined && !isApproved)  || isUpdating) {
                        
                        try
                        {
                            if (AdminActions.RejectPhoto(row.CustomerPhotosID) == false)
                            {
                                try
                                {
                                    if (AdminActions.RejectPhoto(row.CustomerPhotosID) == false)
                                    {
                                        ProblemRejecting = true;
                                        break;
                                   }
                                } catch (Exception ex)
                                {
                                    ProblemRejecting = true;
                                    break;
                                }
                            }

                        }
                        catch (Exception ex)
                        {
                            ProblemRejecting = true;
                            break;
                        }
                      //  break;
                    }

                }
                if (!ProblemRejecting) {
                    result1 = AdminActions.RejectProfile(iProfileID);
                    if (!result1.IsSuccessful)
                        result = "-failed-";
                }
                else {
                    result = "-failed-To Reject Photos-";
                }

            }
            catch (Exception ex) {
                if (CurrentSettings.AllowSendExceptions)
                    result = ex.ToString();
                else
                    result = "-exception-";
                WebErrorSendEmail(ex, "rejectProfile");
            }
            finally {
                myContext.Dispose();

                try {
                    if (result1.IsNewProfile) {
                        ServiceBase.LogAdminAction(AdminUserID, iProfileID, "Reject new profile", "Reject new profile" + Environment.NewLine + "Profile login name: " + ProfileLoginName);
                    }
                    else {
                        ServiceBase.LogAdminAction(AdminUserID, iProfileID, "Reject profile changes", "Reject profile changes" + Environment.NewLine + "Profile login name: " + ProfileLoginName);
                    }
                }
                catch (Exception ex) {
                    WebErrorSendEmail(ex, "rejectProfile->LOG");
                }
            }


            return result;
        }


        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "deleteprofile/{profileid}")]
        public string deleteProfile(LoginHeaders headers, string profileid) {

            bool deletePermanently = false;
            int iProfileID = 0;
            string ProfileLoginName = "";
            int AdminUserID = 0;
            string result = "";

            try {

                AdminUserID = Convert.ToInt32(headers.UserID);
                iProfileID = Convert.ToInt32(profileid);

                if (!CheckHeaders(ref headers))
                    return "unauthorized";
                AdminUserID = Convert.ToInt32(headers.UserID);

                if (!UserRightsHelper.CheckIsAllowed_ProfilesManagerDelete(AdminUserID))
                    return "unauthorized";

                DSMembers dsMembers = DataHelpers.GetEUS_Profiles_ByProfileID(iProfileID);
                if (dsMembers.EUS_Profiles.Rows.Count == 0) {
                    result = "-not found-";
                }
                else {
                    DSMembers.EUS_ProfilesRow dr = (DSMembers.EUS_ProfilesRow)dsMembers.EUS_Profiles.Rows[0];
                    ProfileLoginName = dr.LoginName;

                    if (dr.Status == (int)ProfileStatusEnum.Deleted || dr.Status == (int)ProfileStatusEnum.DeletedByUser)
                        deletePermanently = true;


                    if (deletePermanently) {
                        DataHelpers.UpdateEUS_Profiles_DeletePermanently(iProfileID);
                        try {
                            clsDataRecordErrorsReturn ReturnErrors = AdminActions.DeleteAllCustomerPhotos(iProfileID);
                        }
                        catch (Exception ex) {
                            WebErrorSendEmail(ex, "deleteProfile->DeleteAllCustomerPhotos");
                        }
                        result = "-deleted permanently-";
                    }
                    else {
                        DataHelpers.UpdateEUS_Profiles_MarkDeleted(iProfileID);
                        result = "-marked deleted-";
                    }


                    Dating.Admin.Core.DLL.clsWSHeaders headersWS = headers.GetWSHeaders();
                    gAdminWs.RemoveFromSpammersWhenDeleting(ref headersWS, dr.ProfileID);
                    headers.ReadWSHeaders(headersWS);
                    if (dr.MirrorProfileID > 0)
                        gAdminWs.RemoveFromSpammersWhenDeleting(ref headersWS, dr.MirrorProfileID);

                }

            }
            catch (Exception ex) {
                if (CurrentSettings.AllowSendExceptions)
                    result = ex.ToString();
                else
                    result = "-exception-";
                WebErrorSendEmail(ex, "deleteProfile");
            }
            finally {

                try {
                    if (deletePermanently) {
                        ServiceBase.LogAdminAction(AdminUserID, iProfileID, "Deleting permanently profile", "Profile deleted permanently" + Environment.NewLine + "Profile login name: " + ProfileLoginName);
                    }
                    else {
                        ServiceBase.LogAdminAction(AdminUserID, iProfileID, "Deleting profile", "Profile marked as deleted" + Environment.NewLine + "Profile login name: " + ProfileLoginName);
                    }
                }
                catch (Exception ex) {
                    WebErrorSendEmail(ex, "deleteProfile->Log");
                }
            }
            return result;
        }


        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "approvephoto/{profileid}/{photoid}")]
        public string approveProfilePhoto(LoginHeaders headers, string profileid, string photoid) {
            string result = "-ok-";
            int iProfileID = 0;
            long iPhotoid = 0;
            string ProfileLoginName = "";
            int AdminUserID = 0;
            try {

                AdminUserID = Convert.ToInt32(headers.UserID);
                iProfileID = Convert.ToInt32(profileid);
                iPhotoid = Convert.ToInt64(photoid);

                if (!CheckHeaders(ref headers))
                    return "unauthorized";
                AdminUserID = Convert.ToInt32(headers.UserID);

                if (!UserRightsHelper.CheckIsAllowed(AdminUserID, "PhotosManagerApprovePhoto"))
                    return "unauthorized";

                ProfileLoginName = DataHelpers.GetEUS_Profile_LoginName_ByProfileID(iProfileID);
                bool isok = AdminActions.ApprovePhoto(iPhotoid, false, true);
                if (!isok) result = "-failed-";

            }
            catch (PhotoNotFoundException) {
                result = "-photo not found-";
            }
            catch (Exception ex) {
                if (CurrentSettings.AllowSendExceptions)
                    result = ex.ToString();
                else
                    result = "-exception-";
                WebErrorSendEmail(ex, "approveProfilePhoto");
            }
            finally {
                myContext.Dispose();

                try {
                    ServiceBase.LogAdminAction(AdminUserID, iProfileID, "Approved Photo", "Approved Photo" + Environment.NewLine + "Profile login name: " + ProfileLoginName + Environment.NewLine + "Photo ID: " + iPhotoid);
                }
                catch (Exception ex) {
                    WebErrorSendEmail(ex, "approveProfilePhoto->LOG");
                }
            }


            return result;
        }


        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "rejectphoto/{profileid}/{photoid}")]
        public string rejectProfilePhoto(LoginHeaders headers, string profileid, string photoid) {
            string result = "-ok-";
            int iProfileID = 0;
            long iPhotoid = 0;
            string ProfileLoginName = "";
            int AdminUserID = 0;
            try {


                AdminUserID = Convert.ToInt32(headers.UserID);
                iProfileID = Convert.ToInt32(profileid);
                iPhotoid = Convert.ToInt64(photoid);

                if (!CheckHeaders(ref headers))
                    return "unauthorized";
                AdminUserID = Convert.ToInt32(headers.UserID);

                if (!UserRightsHelper.CheckIsAllowed(AdminUserID, "PhotosManagerDeclinePhoto"))
                    return "unauthorized";

                ProfileLoginName = DataHelpers.GetEUS_Profile_LoginName_ByProfileID(iProfileID);
                bool isok = AdminActions.RejectPhoto(iPhotoid);
                if (!isok) {
                    result = "-failed-";
                }

            }
            catch (PhotoNotFoundException) {
                result = "-photo not found-";
            }
            catch (Exception ex) {
                if (CurrentSettings.AllowSendExceptions)
                    result = ex.ToString();
                else
                    result = "-exception-";
                WebErrorSendEmail(ex, "rejectProfilePhoto");
            }
            finally {
                myContext.Dispose();

                try {
                    ServiceBase.LogAdminAction(AdminUserID, iProfileID, "Decline Photo",
                        "Declined Photo" + Environment.NewLine +
                        "Profile login name: " + ProfileLoginName + Environment.NewLine +
                        "Photo ID: " + iPhotoid);
                }
                catch (Exception ex) {
                    WebErrorSendEmail(ex, "rejectProfilePhoto->LOG");
                }
            }

            return result;
        }


        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "deletephoto/{profileid}/{photoid}")]
        public string deletePhoto(LoginHeaders headers, string profileid, string photoid) {
            string result = "-ok-";
            int iProfileID = 0;
            long iPhotoid = 0;
            string ProfileLoginName = "";
            int AdminUserID = 0;
            string photoName = "";

            try {

                AdminUserID = Convert.ToInt32(headers.UserID);
                iProfileID = Convert.ToInt32(profileid);
                iPhotoid = Convert.ToInt64(photoid);

                if (!CheckHeaders(ref headers))
                    return "unauthorized";
                AdminUserID = Convert.ToInt32(headers.UserID);

                if (!UserRightsHelper.CheckIsAllowed(AdminUserID, "PhotosManagerDeletePhoto"))
                    return "unauthorized";

                ProfileLoginName = DataHelpers.GetEUS_Profile_LoginName_ByProfileID(iProfileID);
                DSMembers.EUS_CustomerPhotosRow dbRow = DataHelpers.GetEUS_CustomerPhotosRow(iPhotoid);
                photoName = dbRow.FileName;
                AdminActions.DeletePhoto(iProfileID, iPhotoid);

            }
            catch (Exception ex) {
                if (CurrentSettings.AllowSendExceptions)
                    result = ex.ToString();
                else
                    result = "-exception-";
                WebErrorSendEmail(ex, "deletePhoto");
            }
            finally {
                myContext.Dispose();

                try {
                    ServiceBase.LogAdminAction(AdminUserID, iProfileID, "Deleting photo",
                        "Deleting photo" + Environment.NewLine +
                        "Profile login name: " + ProfileLoginName + Environment.NewLine +
                        "Photo ID: " + iPhotoid + Environment.NewLine +
                        "File name: " + photoName);
                }
                catch (Exception ex) {
                    WebErrorSendEmail(ex, "deletePhoto->LOG");
                }
            }

            return result;
        }


        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "gettransactions/{datefrom}/{dateto}")]
        public TransactionsList getTransactions(LoginHeaders headers, string datefrom, string dateto) {
            string result = "-ok-";
            DateTime dtDateFrom = DateTime.Now.Date.AddDays(1).AddSeconds(-1);
            DateTime dtDateTo = DateTime.Now.Date.AddSeconds(-1);
            int AdminUserID = 0;

            TransactionsList tl = null;
            try {

                AdminUserID = Convert.ToInt32(headers.UserID);
                if (!string.IsNullOrEmpty(datefrom)) dtDateFrom = DateTime.ParseExact(datefrom, "yyyyMMddHHmmss", System.Globalization.CultureInfo.InvariantCulture);
                if (!string.IsNullOrEmpty(dateto)) dtDateTo = DateTime.ParseExact(dateto, "yyyyMMddHHmmss", System.Globalization.CultureInfo.InvariantCulture);

                if (!CheckHeaders(ref headers))
                    return tl;
                AdminUserID = Convert.ToInt32(headers.UserID);

                if (!UserRightsHelper.CheckIsAllowed(AdminUserID, "TransactionsTransactions"))
                    return tl;

                Dating.Admin.Datasets.DLL.DSMembersViews ds = new Dating.Admin.Datasets.DLL.DSMembersViews();
                Dating.Server.WebServices.Web.TransactionsRetrieveParams prms = new Dating.Server.WebServices.Web.TransactionsRetrieveParams();
                prms.DateFrom = Utils.GetDateUtc(dtDateFrom);
                prms.DateTo = Utils.GetDateUtc(dtDateTo);

                Dating.Admin.Core.DLL.clsWSHeaders headersWS = headers.GetWSHeaders();
                gAdminWs.GetAllCustomerTransactions(ref headersWS, ref ds, prms);
                headers.ReadWSHeaders(headersWS);

                tl = new TransactionsList();
                for (int c = 0; c < ds.AllCustomerTransactions.Rows.Count; c++) {
                    Dating.Admin.Datasets.DLL.DSMembersViews.AllCustomerTransactionsRow row =
                        (Dating.Admin.Datasets.DLL.DSMembersViews.AllCustomerTransactionsRow)ds.AllCustomerTransactions.Rows[c];
                    Transactions tran = new Transactions();
                    tran.ReadRow(row);
                    tl.list.Add(tran);
                    // allow only one transaction
                    if (headers.UserID == "21") break;
                }

                if (headers.UserID != "21") {
                    tl.list = (from itm in tl.list
                               orderby itm.Date_Time descending
                               select itm).ToList();
                }

            }
            catch (Exception ex) {
                if (CurrentSettings.AllowSendExceptions)
                    result = ex.ToString();
                else
                    result = "-exception-";
                WebErrorSendEmail(ex, "getTransactions (params: datefrom=" + datefrom + ",dateto=" + dateto + ")");
            }
            finally {
                myContext.Dispose();

                try {
                    ServiceBase.LogAdminAction(AdminUserID, -1, "Load transactions", "Load transactions");
                }
                catch (Exception ex) {
                    WebErrorSendEmail(ex, "getTransactions->LOG");
                }
            }

            return tl;
        }

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "approvereferrer/{referrerid}/{siteprofileid}")]
        public string approveReferrer(LoginHeaders headers, string referrerid, string siteprofileid) {
            StringBuilder sb = new StringBuilder();
            int iReferrerID = 0;
            string ProfileLoginName = "";
            int AdminUserID = 0;
            int SiteProfileID = 0;
            int ReferrerParentId = 0;

            try {

                AdminUserID = Convert.ToInt32(headers.UserID);
                iReferrerID = Convert.ToInt32(referrerid);
                SiteProfileID = Convert.ToInt32(siteprofileid);

                if (!CheckHeaders(ref headers))
                    return "unauthorized";
                AdminUserID = Convert.ToInt32(headers.UserID);

                if (!UserRightsHelper.CheckIsAllowed(AdminUserID, "ReferrersWaitingReferrers"))
                    return "unauthorized";

                if (SiteProfileID == 0)
                    return "site profile not connected";

                Dating.Admin.Referrals.Core.DLL.DSReferrers members = Dating.Admin.Referrals.Core.DLL.DataHelpers.GetREF_ReferrerProfiles_ByCustomerID(iReferrerID);
                if (members.REF_ReferrerProfiles.Rows.Count == 0)
                    return "referrer not found";


                Dating.Admin.Referrals.Core.DLL.DSReferrers.REF_ReferrerProfilesRow dr =
                    (Dating.Admin.Referrals.Core.DLL.DSReferrers.REF_ReferrerProfilesRow)members.REF_ReferrerProfiles.Rows[0];
             
                if (dr.Status == (int)ProfileStatusEnum.NewProfile || dr.Status == (int)ProfileStatusEnum.Updating) {
                    if (!dr.IsReferrerParentIdNull() && dr.ReferrerParentId > 0) {
                        ReferrerParentId = dr.ReferrerParentId;
                                           }

                    Dating.Admin.Datasets.DLL.DSMembers dsMembers = new Dating.Admin.Datasets.DLL.DSMembers();
                    dsMembers = Dating.Admin.Core.DLL.DataHelpers.GetEUS_Profiles_ByProfileOrMirrorID(SiteProfileID);

                    Dating.Server.Core.DLL.clsConfigValues configs = null;

                    Dating.Admin.Datasets.DLL.DSMembers.EUS_ProfilesRow mirrorRow = null;
                    Dating.Admin.Datasets.DLL.DSMembers.EUS_ProfilesRow masterRow = null;
                    Dating.Admin.Datasets.DLL.DSMembers.EUS_ProfilesRow mirrorRowOld = null;
                    Dating.Admin.Datasets.DLL.DSMembers.EUS_ProfilesRow masterRowOld = null;
                    Dating.Admin.Datasets.DLL.DSMembers.EUS_ProfilesRow[] _drs = null;

                    _drs = (Dating.Admin.Datasets.DLL.DSMembers.EUS_ProfilesRow[])dsMembers.EUS_Profiles.Select("IsMaster=0");
                    if (_drs.Length > 0) {
                        mirrorRow = (Dating.Admin.Datasets.DLL.DSMembers.EUS_ProfilesRow)_drs[0];
                    }

                    _drs = (Dating.Admin.Datasets.DLL.DSMembers.EUS_ProfilesRow[])dsMembers.EUS_Profiles.Select("IsMaster=1");
                    if (_drs.Length > 0) {
                        masterRow = (Dating.Admin.Datasets.DLL.DSMembers.EUS_ProfilesRow)_drs[0];
                    }

                    Dating.Admin.Datasets.DLL.DSMembers dsOld = (Dating.Admin.Datasets.DLL.DSMembers)dsMembers.Copy();
                    _drs = (Dating.Admin.Datasets.DLL.DSMembers.EUS_ProfilesRow[])dsOld.EUS_Profiles.Select("IsMaster=0");
                    if (_drs.Length > 0) {
                        mirrorRowOld = (Dating.Admin.Datasets.DLL.DSMembers.EUS_ProfilesRow)_drs[0];
                    }
                    _drs = (Dating.Admin.Datasets.DLL.DSMembers.EUS_ProfilesRow[])dsOld.EUS_Profiles.Select("IsMaster=1");
                    if (_drs.Length > 0) {
                        masterRowOld = (Dating.Admin.Datasets.DLL.DSMembers.EUS_ProfilesRow)_drs[0];
                    }


                    for (int cnt = 0; cnt < dsMembers.EUS_Profiles.Rows.Count; cnt++) {
                        dsMembers.EUS_Profiles.Rows[cnt]["ReferrerParentId"] = ReferrerParentId;

                        string refCalcCurrency = dsMembers.EUS_Profiles.Rows[cnt]["REF_CalculateCurrency"].ToString();
                        if (string.IsNullOrEmpty(refCalcCurrency)) {
                            if (configs == null) {
                                configs = new Dating.Server.Core.DLL.clsConfigValues();
                            }
                            dsMembers.EUS_Profiles.Rows[cnt]["REF_CalculateCurrency"] = configs.referrer_calculate_currency;
                        }

                        if (dsMembers.EUS_Profiles.Rows[cnt].RowState == DataRowState.Unchanged)
                            dsMembers.EUS_Profiles.Rows[cnt].SetModified();
                    }


                    if (masterRow != null) {
                        Dating.Admin.Core.DLL.DataHelpers.UpdateEUS_Profiles_ModifiedFields(masterRow, masterRowOld);
                    }
                    Dating.Admin.Core.DLL.DataHelpers.UpdateEUS_Profiles_ModifiedFields(mirrorRow, mirrorRowOld);


                    dr.Status = (int)Dating.Admin.Core.DLL.ProfileStatusEnum.Approved;
                    Dating.Admin.Referrals.Core.DLL.DataHelpers.UpdateREF_ReferrerProfiles(ref members);
                    try
                    {
                      Dating.Server.Core.DLL.DataHelpers.RemoveFromQuarntineandSendMessage(mirrorRow.ProfileID, mirrorRow.MirrorProfileID);
                                         }
                    catch (Exception ex)
                    {
                    }



                    //// send email to user
                    //System.Diagnostics.Debug.Print(System.DateTime.Now.ToString("HH:mm.fff") + " " + this.Name + "->" + " SendEmailNotification_OnAdminApprove");
                    //GlbAdminReferrersWS.CheckResult(GlbAdminReferrersWS.Proxies.SendEmailNotification_OnAdminApprove(GlbAdminReferrersWS.Headers, CustomerID));
                    //System.Diagnostics.Debug.Print(System.DateTime.Now.ToString("HH:mm.fff") + " " + this.Name + "->" + " END SendEmailNotification_OnAdminApprove");

                    sb.AppendLine("ok");
                }
                else {
                    sb.AppendLine("not updating");
                }




            }
            catch (Exception ex) {
                if (CurrentSettings.AllowSendExceptions)
                    sb.AppendLine(ex.ToString());
                else
                    sb.AppendLine("-exception-");

                WebErrorSendEmail(ex, "approveProfile");
            }
            finally {
                myContext.Dispose();

                try {
                    if (SiteProfileID > 0) {
                        ServiceBase.LogAdminAction(AdminUserID, SiteProfileID,
                            "Approve Referrer",
                            "Approve Referrer" + Environment.NewLine +
                            "Referrer customer id: " + iReferrerID + Environment.NewLine +
                            "Profile login name: " + ProfileLoginName + Environment.NewLine +
                            "SiteProfileID: " + SiteProfileID + Environment.NewLine +
                            "ReferrerParentId: " + ReferrerParentId);
                    }
                    else {
                        ServiceBase.LogAdminAction(AdminUserID, -1,
                            "Approve Referrer",
                            "Approve Referrer" + Environment.NewLine +
                            "Referrer customer id: " + iReferrerID + Environment.NewLine +
                            "Profile login name: " + ProfileLoginName + Environment.NewLine +
                            "SiteProfileID: " + SiteProfileID + Environment.NewLine +
                            "ReferrerParentId: " + ReferrerParentId);
                    }
                }
                catch (Exception ex) {
                    WebErrorSendEmail(ex, "approveReferrer->LOG");
                }
            }


            return sb.ToString().Trim();
        }

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "rejectreferrer/{referrerid}")]
        public string rejectReferrer(LoginHeaders headers, string referrerid) {
            StringBuilder sb = new StringBuilder();
            int iReferrerID = 0;
            string ProfileLoginName = "";
            int AdminUserID = 0;
            int SiteProfileID = 0;
            int ReferrerParentId = 0;

            try {

                AdminUserID = Convert.ToInt32(headers.UserID);
                iReferrerID = Convert.ToInt32(referrerid);

                if (!CheckHeaders(ref headers))
                    return "unauthorized";
                AdminUserID = Convert.ToInt32(headers.UserID);

                if (!UserRightsHelper.CheckIsAllowed(AdminUserID, "ReferrersWaitingReferrers"))
                    return "unauthorized";

                Dating.Admin.Referrals.Core.DLL.DSReferrers members = Dating.Admin.Referrals.Core.DLL.DataHelpers.GetREF_ReferrerProfiles_ByCustomerID(iReferrerID);
                if (members.REF_ReferrerProfiles.Rows.Count == 0)
                    return "referrer not found";


                Dating.Admin.Referrals.Core.DLL.DSReferrers.REF_ReferrerProfilesRow dr =
                    (Dating.Admin.Referrals.Core.DLL.DSReferrers.REF_ReferrerProfilesRow)members.REF_ReferrerProfiles.Rows[0];

                if (!dr.IsSiteProfileIDNull() && dr.SiteProfileID > 0) {
                    SiteProfileID = dr.SiteProfileID;
                }
                if (!dr.IsReferrerParentIdNull() && dr.ReferrerParentId > 0) {
                    ReferrerParentId = dr.ReferrerParentId;
                }

                dr.Status = (int)Dating.Admin.Core.DLL.ProfileStatusEnum.Rejected;
                Dating.Admin.Referrals.Core.DLL.DataHelpers.UpdateREF_ReferrerProfiles(ref members);

                sb.AppendLine("ok");
            }
            catch (Exception ex) {
                if (CurrentSettings.AllowSendExceptions)
                    sb.AppendLine(ex.ToString());
                else
                    sb.AppendLine("-exception-");

                WebErrorSendEmail(ex, "rejectReferrer");
            }
            finally {
                myContext.Dispose();

                try {
                    if (SiteProfileID > 0) {
                        ServiceBase.LogAdminAction(AdminUserID, SiteProfileID,
                             "Reject Referrer",
                             "Reject Referrer" + Environment.NewLine +
                             "Referrer customer id: " + iReferrerID + Environment.NewLine +
                             "Site Profile login name: " + ProfileLoginName + Environment.NewLine +
                             "SiteProfileID: " + SiteProfileID + Environment.NewLine + "ReferrerParentId: " + ReferrerParentId);
                    }
                    else {
                        ServiceBase.LogAdminAction(AdminUserID, -1,
                             "Reject Referrer",
                             "Reject Referrer" + Environment.NewLine +
                             "Referrer customer id: " + iReferrerID + Environment.NewLine +
                             "Site Profile login name: " + ProfileLoginName + Environment.NewLine +
                             "SiteProfileID: " + SiteProfileID + Environment.NewLine +
                             "ReferrerParentId: " + ReferrerParentId);
                    }
                }
                catch (Exception ex) {
                    WebErrorSendEmail(ex, "rejectReferrer->LOG");
                }
            }


            return sb.ToString().Trim();
        }


        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "deletereferrer/{referrerid}")]
        public string deleteReferrer(LoginHeaders headers, string referrerid) {
            StringBuilder sb = new StringBuilder();
            int iReferrerID = 0;
            string ProfileLoginName = "";
            int AdminUserID = 0;
            int SiteProfileID = 0;
            bool Permanently = false;

            try {

                AdminUserID = Convert.ToInt32(headers.UserID);
                iReferrerID = Convert.ToInt32(referrerid);

                if (!CheckHeaders(ref headers))
                    return "unauthorized";
                AdminUserID = Convert.ToInt32(headers.UserID);

                if (!UserRightsHelper.CheckIsAllowed(AdminUserID, "ReferrersWaitingReferrers"))
                    return "unauthorized";


                Dating.Admin.Referrals.Core.DLL.DSReferrers members = Dating.Admin.Referrals.Core.DLL.DataHelpers.GetREF_ReferrerProfiles_ByCustomerID(iReferrerID);
                if (members.REF_ReferrerProfiles.Rows.Count > 0) {
                    Dating.Admin.Referrals.Core.DLL.DSReferrers.REF_ReferrerProfilesRow dr =
                        (Dating.Admin.Referrals.Core.DLL.DSReferrers.REF_ReferrerProfilesRow)members.REF_ReferrerProfiles.Rows[0];

                    ProfileLoginName = dr["LoginName"].ToString();
                    if (!dr.IsNull("SiteProfileID")) SiteProfileID = dr.SiteProfileID;

                    if (dr.Status == (int)ProfileStatusEnum.Deleted)
                        Permanently = true;

                    if (Permanently) {
                        int cnt = 0;
                        for (cnt = 0; cnt < members.REF_ReferrerProfiles.Rows.Count; cnt++) {
                            members.REF_ReferrerProfiles.Rows[cnt].Delete();
                        }

                    }
                    else {
                        foreach (Dating.Admin.Referrals.Core.DLL.DSReferrers.REF_ReferrerProfilesRow dr1 in members.REF_ReferrerProfiles.Rows) {
                            dr1.Status = (int)Dating.Admin.Core.DLL.ProfileStatusEnum.Deleted;
                        }
                    }

                    Dating.Admin.Referrals.Core.DLL.DataHelpers.UpdateREF_ReferrerProfiles(ref members);
                }

                sb.AppendLine("ok");
            }
            catch (Exception ex) {
                if (CurrentSettings.AllowSendExceptions)
                    sb.AppendLine(ex.ToString());
                else
                    sb.AppendLine("-exception-");

                WebErrorSendEmail(ex, "deleteReferrer");
            }
            finally {
                myContext.Dispose();

                try {
                    if (Permanently) {
                        if (SiteProfileID > 0) {
                            ServiceBase.LogAdminAction(AdminUserID, SiteProfileID,
                                "Deleting permanently Referrer",
                                "Deleting permanently Referrer" + Environment.NewLine +
                                "Referrer customer id: " + iReferrerID + Environment.NewLine +
                                "Site Profile login name: " + ProfileLoginName + Environment.NewLine +
                                "SiteProfileID: " + SiteProfileID);
                        }
                        else {
                            ServiceBase.LogAdminAction(AdminUserID, -1,
                                "Deleting permanently Referrer",
                                "Deleting permanently Referrer" + Environment.NewLine +
                                "Referrer customer id: " + iReferrerID + Environment.NewLine +
                                "Site Profile login name: " + ProfileLoginName + Environment.NewLine +
                                "SiteProfileID: " + SiteProfileID);
                        }
                    }
                    else {
                        if ((SiteProfileID > 0)) {
                            ServiceBase.LogAdminAction(AdminUserID, SiteProfileID,
                                "Deleting Referrer",
                                "Referrer marked as deleted" + Environment.NewLine +
                                "Referrer customer id: " + iReferrerID + Environment.NewLine +
                                "Site Profile login name: " + ProfileLoginName + Environment.NewLine +
                                "SiteProfileID: " + SiteProfileID);
                        }
                        else {
                            ServiceBase.LogAdminAction(AdminUserID, -1,
                                "Deleting Referrer",
                                "Referrer marked as deleted" + Environment.NewLine +
                                "Referrer customer id: " + iReferrerID + Environment.NewLine +
                                "Site Profile login name: " + ProfileLoginName + Environment.NewLine +
                                "SiteProfileID: " + SiteProfileID);
                        }
                    }

                }
                catch (Exception ex) {
                    WebErrorSendEmail(ex, "deleteReferrer->LOG");
                }
            }


            return sb.ToString().Trim();
        }


        //[WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "userprofiledetails/{profileid}")]
        //public ReferrerDetails getUserReferrerDetails(LoginHeaders headers, string profileid) {
        //    string langId = "US";
        //    ReferrerDetails details = null;
        //    int iProfileID = 0;
        //    string ProfileLoginName = "";
        //    int AdminUserID = 0;

        //    try {

        //        AdminUserID = Convert.ToInt32(headers.UserID);
        //        iProfileID = Convert.ToInt32(profileid);

        //        if (!CheckHeaders(ref headers))
        //            return null;

        //        if (!UserRightsHelper.CheckIsAllowed(AdminUserID, "ProfilesManagerEdit"))
        //            return null;


        //        ProfileLoginName = "";
        //        details = ReferrerDetails.GetUserDetails(iProfileID, langId);

        //        //Dating.Server.WebServices.Web.ProfilesRetrieveParams prms = new Dating.Server.WebServices.Web.ProfilesRetrieveParams();
        //        //prms.UseParams = true;
        //        //prms.NonMasterRecords = true;
        //        //prms.MasterRecords = true;
        //        //prms.Status = (int)Dating.Admin.Core.DLL.ProfileStatusEnum.NewProfile;
        //        //prms.RecordsToSelect = 200;
        //        //prms.PreviousRowNumber = -1;


        //        //Dating.Admin.Referrals.Core.DLL.DSReferrers ds = new Dating.Admin.Referrals.Core.DLL.DSReferrers();
        //        //Dating.Admin.Core.DLL.clsWSHeaders headersWS = headers.GetWSHeaders();
        //        //Dating.Admin.Core.DLL.clsDataRecordErrorsReturn ret = gAdminReferrersWS.GetREF_ReferrerProfiles(ref headersWS, ref ds, iProfileID);
        //        //headers.ReadWSHeaders(headersWS);

        //        //string ParentRefLoginName = "root";
        //        //if (ds.REF_ReferrerProfiles.Rows.Count > 0) {
        //        //    Dating.Admin.Referrals.Core.DLL.DSReferrers.REF_ReferrerProfilesRow dr = (Dating.Admin.Referrals.Core.DLL.DSReferrers.REF_ReferrerProfilesRow)ds.REF_ReferrerProfiles.Rows[0];
        //        //    if (!dr.IsReferrerParentIdNull())
        //        //        ParentRefLoginName = Dating.Server.Core.DLL.DataHelpers.GetEUS_Profile_LoginName_ByProfileID(dr.ReferrerParentId);
        //        //}



        //    }
        //    catch (Exception ex) {
        //        WebErrorSendEmail(ex, "getUserProfileDetails");
        //    }
        //    finally {
        //        myContext.Dispose();

        //        try {
        //            ServiceBase.LogAdminAction(AdminUserID, iProfileID, "Load profile", "Load profile" + Environment.NewLine + "Profile login name: " + ProfileLoginName);
        //        }
        //        catch (Exception ex) {
        //            WebErrorSendEmail(ex, "getUserProfileDetails->LOG");
        //        }
        //    }


        //    return details;
        //}



    }// end class AdminService
}

﻿using Dating.Admin.Datasets.DLL;
using Dating.Admin.Datasets.DLL.DSMembersTableAdapters;
using Dating.Admin.Datasets.DLL.DSSYSDataTableAdapters;
using Dating.Server.Core.DLL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Web;
using System.Text;
using System.Web;


namespace GoomenaLib.Admin
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    public class AdminBaseService : ServiceBase
    {
        internal static Dating.Server.WebServices.Web.Admin gAdminWs = new Dating.Server.WebServices.Web.Admin();
        internal static Dating.Server.WebServices.Web.AdminReferrers gAdminReferrersWS = new Dating.Server.WebServices.Web.AdminReferrers();

        public static List<string> gTokensList;
        static AdminBaseService() {
            gTokensList = new List<string>();
            UserRightsHelper.gSYS_Users = new DSSYSData.SYS_UsersDataTable();

            gAdminWs.SqlConnectionString = CurrentSettings.ConnectionString;
            gAdminReferrersWS.SqlConnectionString = CurrentSettings.ConnectionString;
        }


        public virtual LoginHeaders login(AdminUser user) {

            LoginHeaders result = new LoginHeaders()
            {
                UserID = "-1",
                Token = "",
                LoginName = user.Login

            };

            try {
                Dating.Admin.Core.DLL.clsDataRecordLogin loginRec = new Dating.Admin.Core.DLL.clsDataRecordLogin()
                {
                    HardwareID = "",
                    LoginName = user.Login,
                    Password = user.Password
                };
                // Dating.Admin.Core.DLL.clsWSHeaders headers = new Dating.Admin.Core.DLL.clsWSHeaders();
                Dating.Admin.Core.DLL.clsDataRecordLoginReturn loginRecReturn = new Dating.Admin.Core.DLL.clsDataRecordLoginReturn();

                Dating.Admin.Core.DLL.clsWSHeaders headersWS = result.GetWSHeaders();
                loginRecReturn = gAdminWs.Login(ref headersWS, loginRec);
                result.ReadWSHeaders(headersWS);


                for (int c = 0; c < UserRightsHelper.gSYS_Users.Rows.Count; c++) {
                    if ((int)UserRightsHelper.gSYS_Users.Rows[c]["UsersID"] == headersWS.UserID) {
                        UserRightsHelper.gSYS_Users.Rows.RemoveAt(c);
                        break;
                    }
                }

                DSSYSData.SYS_UsersDataTable SYS_UsersDataTable = new DSSYSData.SYS_UsersDataTable();
                SYS_UsersTableAdapter ta = new SYS_UsersTableAdapter();
                ta.Connection = new SqlConnection(CurrentSettings.ConnectionString);
                ta.FillBy_UsersID(SYS_UsersDataTable, headersWS.UserID);
                if (SYS_UsersDataTable.Rows.Count > 0) {
                    DSSYSData.SYS_UsersRow SYS_UsersRow = (DSSYSData.SYS_UsersRow)SYS_UsersDataTable.Rows[0];
                    UserRightsHelper.gSYS_Users.ImportRow(SYS_UsersRow);
                }



                //if (!loginRecReturn.HasErrors) {
                //    result.LoginName = headers.LoginName;
                //    result.Password = headers.Password;
                //    result.UserID = headers.UserID.ToString();
                //    result.Token = headers.Token;
                //}

                //DSSYSData.SYS_UsersDataTable SYS_UsersDataTable = new DSSYSData.SYS_UsersDataTable();
                //SYS_UsersTableAdapter ta = new SYS_UsersTableAdapter();
                //ta.Connection = new SqlConnection(CurrentSettings.ConnectionString);
                //ta.FillBy_SYS_Users_GetUsers(SYS_UsersDataTable, null, user.Login, user.Password);

                //if (SYS_UsersDataTable.Rows.Count > 0) {
                //    int rowIndex = 0;


                //    while (rowIndex < SYS_UsersDataTable.Rows.Count) {
                //        DSSYSData.SYS_UsersRow SYS_UsersRow = (DSSYSData.SYS_UsersRow)SYS_UsersDataTable.Rows[rowIndex];

                //        for (int c = 0; c < UserRightsHelper.gSYS_Users.Rows.Count; c++) {
                //            if ((int)UserRightsHelper.gSYS_Users.Rows[c]["UsersID"] == SYS_UsersRow.UsersID) {
                //                UserRightsHelper.gSYS_Users.Rows.RemoveAt(c);
                //                break;
                //            }
                //        }
                //        UserRightsHelper.gSYS_Users.ImportRow(SYS_UsersRow);

                //        if (SYS_UsersRow.IsActive) {
                //            string Token = Library.Public.clsHash.ComputeHash(user.Login + "^Token$$$!!", "SHA1");
                //            if (!gTokensList.Contains(Token)) {
                //                gTokensList.Add(Token);
                //            }

                //            Library.Public.clsCrypoPassword crypt = new Library.Public.clsCrypoPassword();
                //            string LoginName = crypt.CrypPassword(SYS_UsersRow.LoginName);
                //            string Password = crypt.CrypPassword(SYS_UsersRow.Password);


                //            result.UserID = SYS_UsersRow.UsersID.ToString();
                //            result.LoginName = LoginName;
                //            result.Password = Password;
                //            result.Token = Token;

                //        }

                //        rowIndex += 1;
                //    }
                //}
            }
            catch (Exception ex) {
                WebErrorSendEmail(ex, "admin-login");
            }

            return result;
        }


        protected bool CheckHeaders(ref LoginHeaders headers) {
            try {
                Dating.Admin.Core.DLL.clsWSHeaders headersWS = headers.GetWSHeaders();
                bool ret = gAdminWs.CheckHeaders(ref headersWS);
                headers.ReadWSHeaders(headersWS);

                if (UserRightsHelper.GetUserRow(headersWS.UserID) == null) {
                    DSSYSData.SYS_UsersDataTable SYS_UsersDataTable = new DSSYSData.SYS_UsersDataTable();
                    SYS_UsersTableAdapter ta = new SYS_UsersTableAdapter();
                    ta.Connection = new SqlConnection(CurrentSettings.ConnectionString);
                    ta.FillBy_UsersID(SYS_UsersDataTable, headersWS.UserID);
                    if (SYS_UsersDataTable.Rows.Count > 0) {
                        DSSYSData.SYS_UsersRow SYS_UsersRow = (DSSYSData.SYS_UsersRow)SYS_UsersDataTable.Rows[0];
                        UserRightsHelper.gSYS_Users.ImportRow(SYS_UsersRow);
                    }
                }

                return ret;
            }
            catch (Exception ex) {
                WebErrorSendEmail(ex, "admin-CheckHeaders");
            }
            return false;
        }


        public void setEUS_ProfileFields(ref Dating.Server.Datasets.DLL.EUS_Profile pr, ref UserDetails lp) {
            {
                if (lp.occupation == null) lp.occupation = "";
                if (lp.occupation.Length > 200) lp.occupation = lp.occupation.Remove(200);
                pr.OtherDetails_Occupation = lp.occupation;
            }
            {
                if (lp.aboutme_heading == null) lp.aboutme_heading = "";
                if (lp.aboutme_heading.Length > 200) lp.aboutme_heading = lp.aboutme_heading.Remove(200);
                pr.AboutMe_Heading = lp.aboutme_heading;
            }
            {
                if (lp.aboutme_describeyourself == null) lp.aboutme_describeyourself = "";
                if (pr.AboutMe_DescribeYourself == null) pr.AboutMe_DescribeYourself = "";
                pr.AboutMe_DescribeYourself = lp.aboutme_describeyourself;
            }
            {
                if (lp.aboutme_describedate == null) lp.aboutme_describedate = "";
                if (pr.AboutMe_DescribeAnIdealFirstDate == null) pr.AboutMe_DescribeAnIdealFirstDate = "";
                pr.AboutMe_DescribeAnIdealFirstDate = lp.aboutme_describedate;
            }

        }

        /*
        protected UserProfilesList GetEUS_ProfilesWithParams_New(ProfilesRetrieveParams parms) {

            Dating.Admin.Datasets.DLL.DSMembersAdmin ds = new Dating.Admin.Datasets.DLL.DSMembersAdmin();
            UserProfilesList profiles = new UserProfilesList();

            Dating.Admin.Datasets.DLL.DSMembersAdminTableAdapters.EUS_ProfilesTableAdapter ta =
                new Dating.Admin.Datasets.DLL.DSMembersAdminTableAdapters.EUS_ProfilesTableAdapter();
            ta.Connection = new SqlConnection(CurrentSettings.ConnectionString);

            clsConfigValues config = new clsConfigValues();

            int mins = 20;
            if ((!int.TryParse(config.members_online_minutes, out mins)))
                mins = 20;
            DateTime utcDate = System.DateTime.UtcNow.AddMinutes(-mins);


            ProfileStatusEnum parmStatus = (ProfileStatusEnum)int.Parse(parms.Status);
            bool? parmMasterRecords = ParseTo.toBoolNull(parms.MasterRecords);
            bool? parmNonMasterRecords = ParseTo.toBoolNull(parms.NonMasterRecords);
            int? parmRecordsToSelect = ParseTo.toIntNull(parms.RecordsToSelect);
            int? parmPreviousRowNumber = ParseTo.toIntNull(parms.PreviousRowNumber);
            bool? parmQuickSelect = ParseTo.toBoolNull(parms.QuickSelect);
            DateTime? parmDateFrom = ParseTo.toDateNull(parms.DateFrom);
            DateTime? parmDateTo = ParseTo.toDateNull(parms.DateTo);
            bool? parmLoadAffiliateProfiles = ParseTo.toBoolNull(parms.LoadAffiliateProfiles);
            bool? parmLoadReferrersProfiles = ParseTo.toBoolNull(parms.LoadReferrersProfiles);


            if (parmStatus == ProfileStatusEnum.SearchProfile) {
                if (parmMasterRecords == true && parmNonMasterRecords == true) {
                    ta.FillBy_Search(ds.EUS_Profiles, null, parms.SearchLogin, parms.SearchEmail, parmPreviousRowNumber, parmRecordsToSelect, parmQuickSelect);
                }
                else if (parmNonMasterRecords == true) {
                    ta.FillBy_Search(ds.EUS_Profiles, false, parms.SearchLogin, parms.SearchEmail, parmPreviousRowNumber, parmRecordsToSelect, parmQuickSelect);
                }
                else if (parmMasterRecords == true) {
                    ta.FillBy_Search(ds.EUS_Profiles, true, parms.SearchLogin, parms.SearchEmail, parmPreviousRowNumber, parmRecordsToSelect, parmQuickSelect);
                }

            }
            else if (parmStatus == ProfileStatusEnum.AutoApprovedProfile) {
                if (parmMasterRecords == true && parmNonMasterRecords == true) {
                    ta.FillBy_IsAutoApproved(ds.EUS_Profiles, null, parmDateFrom, parmDateTo, null, parmLoadAffiliateProfiles, parmPreviousRowNumber, parmRecordsToSelect, parmQuickSelect);
                }
                else if (parmNonMasterRecords == true) {
                    ta.FillBy_IsAutoApproved(ds.EUS_Profiles, false, parmDateFrom, parmDateTo, null, parmLoadAffiliateProfiles, parmPreviousRowNumber, parmRecordsToSelect, parmQuickSelect);
                }
                else if (parmMasterRecords == true) {
                    ta.FillBy_IsAutoApproved(ds.EUS_Profiles, true, parmDateFrom, parmDateTo, null, parmLoadAffiliateProfiles, parmPreviousRowNumber, parmRecordsToSelect, parmQuickSelect);
                }



            }
            else if ((parmStatus == ProfileStatusEnum.ReferrerWithBonus)) {
                if ((parmMasterRecords == true && parmNonMasterRecords == true)) {
                    ta.FillBy_Referrals_WithBonus(ds.EUS_Profiles, null, parmDateFrom, parmDateTo, parmPreviousRowNumber, parmRecordsToSelect, parmQuickSelect);
                }
                else if (parmNonMasterRecords == true) {
                    ta.FillBy_Referrals_WithBonus(ds.EUS_Profiles, false, parmDateFrom, parmDateTo, parmPreviousRowNumber, parmRecordsToSelect, parmQuickSelect);
                }
                else if (parmMasterRecords == true) {
                    ta.FillBy_Referrals_WithBonus(ds.EUS_Profiles, true, parmDateFrom, parmDateTo, parmPreviousRowNumber, parmRecordsToSelect, parmQuickSelect);
                }


            }
            else if ((parmStatus == ProfileStatusEnum.ProfilesOnQuarantine)) {
                if ((parmMasterRecords == true && parmNonMasterRecords == true)) {
                    ta.FillBy_MembersOnQuarantine(ds.EUS_Profiles, null, parmDateFrom, parmDateTo, parmLoadReferrersProfiles, parmLoadAffiliateProfiles, parmPreviousRowNumber, parmRecordsToSelect, parmQuickSelect);
                }
                else if (parmNonMasterRecords == true) {
                    ta.FillBy_MembersOnQuarantine(ds.EUS_Profiles, false, parmDateFrom, parmDateTo, parmLoadReferrersProfiles, parmLoadAffiliateProfiles, parmPreviousRowNumber, parmRecordsToSelect, parmQuickSelect);
                }
                else if (parmMasterRecords == true) {
                    ta.FillBy_MembersOnQuarantine(ds.EUS_Profiles, true, parmDateFrom, parmDateTo, parmLoadReferrersProfiles, parmLoadAffiliateProfiles, parmPreviousRowNumber, parmRecordsToSelect, parmQuickSelect);
                }



            }
            else if ((parmStatus == ProfileStatusEnum.VirtualProfiles)) {
                if ((parmMasterRecords == true && parmNonMasterRecords == true)) {
                    ta.FillBy_IsVirtual(ds.EUS_Profiles, null, parmDateFrom, parmDateTo, parmLoadReferrersProfiles, parmLoadAffiliateProfiles, parmPreviousRowNumber, parmRecordsToSelect, parmQuickSelect);
                }
                else if (parmNonMasterRecords == true) {
                    ta.FillBy_IsVirtual(ds.EUS_Profiles, false, parmDateFrom, parmDateTo, parmLoadReferrersProfiles, parmLoadAffiliateProfiles, parmPreviousRowNumber, parmRecordsToSelect, parmQuickSelect);
                }
                else if (parmMasterRecords == true) {
                    ta.FillBy_IsVirtual(ds.EUS_Profiles, true, parmDateFrom, parmDateTo, parmLoadReferrersProfiles, parmLoadAffiliateProfiles, parmPreviousRowNumber, parmRecordsToSelect, parmQuickSelect);
                }



            }
            else if ((parmStatus == ProfileStatusEnum.ApprovedProfileWithPhotos || parmStatus == ProfileStatusEnum.ApprovedProfileWithoutPhotos)) {
                bool hasPhotos = (parmStatus == ProfileStatusEnum.ApprovedProfileWithPhotos);

                if ((parmMasterRecords == true && parmNonMasterRecords == true)) {
                    ta.FillBy_Approved_HasOrNotPhotos(ds.EUS_Profiles, hasPhotos, null, parmDateFrom, parmDateTo, parmLoadReferrersProfiles, parmLoadAffiliateProfiles, parmPreviousRowNumber, parmRecordsToSelect, parmQuickSelect);
                }
                else if (parmNonMasterRecords == true) {
                    ta.FillBy_Approved_HasOrNotPhotos(ds.EUS_Profiles, hasPhotos, false, parmDateFrom, parmDateTo, parmLoadReferrersProfiles, parmLoadAffiliateProfiles, parmPreviousRowNumber, parmRecordsToSelect, parmQuickSelect);
                }
                else if (parmMasterRecords == true) {
                    ta.FillBy_Approved_HasOrNotPhotos(ds.EUS_Profiles, hasPhotos, true, parmDateFrom, parmDateTo, parmLoadReferrersProfiles, parmLoadAffiliateProfiles, parmPreviousRowNumber, parmRecordsToSelect, parmQuickSelect);
                }


            }
            else if ((parmStatus == ProfileStatusEnum.ApprovedOrUpdatingProfileWithPhotos ||
                        parmStatus == ProfileStatusEnum.ApprovedOrUpdatingProfileWithoutPhotos)) {
                bool hasPhotos = (parmStatus == ProfileStatusEnum.ApprovedOrUpdatingProfileWithPhotos);

                if ((parmMasterRecords == true && parmNonMasterRecords == true)) {
                    ta.FillBy_ApprovedOrUpdating_HasOrNotPhotos(ds.EUS_Profiles, hasPhotos, null, parmDateFrom, parmDateTo, parmLoadReferrersProfiles, parmLoadAffiliateProfiles, parmPreviousRowNumber, parmRecordsToSelect, parmQuickSelect);
                }
                else if (parmNonMasterRecords == true) {
                    ta.FillBy_ApprovedOrUpdating_HasOrNotPhotos(ds.EUS_Profiles, hasPhotos, false, parmDateFrom, parmDateTo, parmLoadReferrersProfiles, parmLoadAffiliateProfiles, parmPreviousRowNumber, parmRecordsToSelect, parmQuickSelect);
                }
                else if (parmMasterRecords == true) {
                    ta.FillBy_ApprovedOrUpdating_HasOrNotPhotos(ds.EUS_Profiles, hasPhotos, true, parmDateFrom, parmDateTo, parmLoadReferrersProfiles, parmLoadAffiliateProfiles, parmPreviousRowNumber, parmRecordsToSelect, parmQuickSelect);
                }


            }
            else if ((parmStatus == ProfileStatusEnum.ApprovedOrUpdating)) {
                if ((parmMasterRecords == true && parmNonMasterRecords == true)) {
                    ta.FillBy_ApprovedOrUpdating_IsMaster(ds.EUS_Profiles, null, parmDateFrom, parmDateTo, parmLoadReferrersProfiles, parmLoadAffiliateProfiles, parmPreviousRowNumber, parmRecordsToSelect, parmQuickSelect);
                }
                else if (parmNonMasterRecords == true) {
                    ta.FillBy_ApprovedOrUpdating_IsMaster(ds.EUS_Profiles, false, parmDateFrom, parmDateTo, parmLoadReferrersProfiles, parmLoadAffiliateProfiles, parmPreviousRowNumber, parmRecordsToSelect, parmQuickSelect);
                }
                else if (parmMasterRecords == true) {
                    ta.FillBy_ApprovedOrUpdating_IsMaster(ds.EUS_Profiles, true, parmDateFrom, parmDateTo, parmLoadReferrersProfiles, parmLoadAffiliateProfiles, parmPreviousRowNumber, parmRecordsToSelect, parmQuickSelect);
                }


            }
            else if (parmStatus == ProfileStatusEnum.Approved) {
                if (parmMasterRecords == true && parmNonMasterRecords == true) {
                    ta.FillBy_IsMasterAndStatus(ds.EUS_Profiles, null, (int?)parmStatus, parmDateFrom, parmDateTo, parmLoadReferrersProfiles, parmLoadAffiliateProfiles, parmPreviousRowNumber, parmRecordsToSelect, parmQuickSelect);
                }
                else if (parmNonMasterRecords == true) {
                    ta.FillBy_IsMasterAndStatus(ds.EUS_Profiles, false, (int?)parmStatus, parmDateFrom, parmDateTo, parmLoadReferrersProfiles, parmLoadAffiliateProfiles, parmPreviousRowNumber, parmRecordsToSelect, parmQuickSelect);
                }
                else if (parmMasterRecords == true) {
                    ta.FillBy_IsMasterAndStatus(ds.EUS_Profiles, true, (int?)parmStatus, parmDateFrom, parmDateTo, parmLoadReferrersProfiles, parmLoadAffiliateProfiles, parmPreviousRowNumber, parmRecordsToSelect, parmQuickSelect);
                }



            }
            else if (parmStatus == ProfileStatusEnum.MembersOnline) {
                // the user's last activity date should be greater or equal to datetime 20 minutes before, for example
                // so we could say user is online

                // pass datetime that was 20 mins before
                if ((parmMasterRecords == true && parmNonMasterRecords == true)) {
                    ta.FillBy_MembersOnline(ds.EUS_Profiles, null, utcDate, parmLoadReferrersProfiles, null, parmLoadAffiliateProfiles, parmPreviousRowNumber, parmRecordsToSelect, null, parmQuickSelect);
                }
                else if (parmNonMasterRecords == true) {
                    ta.FillBy_MembersOnline(ds.EUS_Profiles, false, utcDate, parmLoadReferrersProfiles, null, parmLoadAffiliateProfiles, parmPreviousRowNumber, parmRecordsToSelect, null, parmQuickSelect);
                }
                else if (parmMasterRecords == true) {
                    ta.FillBy_MembersOnline(ds.EUS_Profiles, true, utcDate, parmLoadReferrersProfiles, null, parmLoadAffiliateProfiles, parmPreviousRowNumber, parmRecordsToSelect, null, parmQuickSelect);
                }

            }
            else if (parmStatus == ProfileStatusEnum.RegisteredFromMobile) {
                ta.FillBy_RegistersFromMobile(ds.EUS_Profiles, parmDateFrom, parmDateTo, parmPreviousRowNumber, parmRecordsToSelect, parmQuickSelect);
            }
            else if (parmStatus > ProfileStatusEnum.None) {
                if (parmMasterRecords == true && parmNonMasterRecords == true) {
                    ta.FillBy_Status(ds.EUS_Profiles, (int?)parmStatus);

                }
                else if (parmMasterRecords == true) {
                    ta.FillBy_IsMasterAndStatus(ds.EUS_Profiles, true, (int?)parmStatus, parmDateFrom, parmDateTo, parmLoadReferrersProfiles, parmLoadAffiliateProfiles, parmPreviousRowNumber, parmRecordsToSelect, parmQuickSelect);

                }
                else if (parmNonMasterRecords == true) {
                    ta.FillBy_IsMasterAndStatus(ds.EUS_Profiles, false, (int?)parmStatus, parmDateFrom, parmDateTo, parmLoadReferrersProfiles, parmLoadAffiliateProfiles, parmPreviousRowNumber, parmRecordsToSelect, parmQuickSelect);

                }


            }
            else {
                if (parmMasterRecords == true) {
                    ta.FillBy_IsMaster(ds.EUS_Profiles, true, parmLoadReferrersProfiles, parmLoadAffiliateProfiles, parmDateFrom, parmDateTo, parmPreviousRowNumber, parmRecordsToSelect, parmQuickSelect);

                }
                else if (parmNonMasterRecords == true) {
                    ta.FillBy_IsMaster(ds.EUS_Profiles, false, parmLoadReferrersProfiles, parmLoadAffiliateProfiles, parmDateFrom, parmDateTo, parmPreviousRowNumber, parmRecordsToSelect, parmQuickSelect);
                }
            }



            for (int c = 0; c < ds.EUS_Profiles.Rows.Count; c++) {
                DSMembersAdmin.EUS_ProfilesRow row = (DSMembersAdmin.EUS_ProfilesRow)ds.EUS_Profiles.Rows[c];
                UserProfile profile = new UserProfile();
                profile.ReadRow(row);
                if (!row.IsLastActivityDateTimeNull())
                    profile.IsOnline = UserProfile.GetIsOnlineNow(mins, row.IsOnline, row.LastActivityDateTime).ToString();
                profiles.Profiles.Add(profile);
            }


            if (parmStatus == ProfileStatusEnum.MembersOnline) {
                //profiles sort
                profiles.Profiles = (from itm in profiles.Profiles
                                     orderby itm.LastActivityDateTime descending
                                     select itm).ToList();
            }

            return profiles;
        }

        */
    }// end class AdminService
}

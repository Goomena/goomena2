﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.ServiceModel;
using Dating.Server.Core.DLL;

namespace GoomenaLib.Admin
{

    [DataContract]
    public class UserProfileReferrer : UserProfile
    {

        [DataMember]
        public string SiteProfileID { get; set; }
        [DataMember]
        public string SiteMirrorProfileID { get; set; }
        [DataMember]
        public string ParentLoginName { get; set; }
        //[DataMember]
        //public string REF_FullParentCode { get; set; }


        public void ReadRow(Dating.Admin.Referrals.Core.DLL.DSReferrers.GetReferrerProfiles_ADMINRow row) {
            this.PhotosCount = "0";
            this.ImageUrl = "";
            this.IsOnline = "0";
            this.HasCredits = "0";
            this.ReferrerParentId = "0";
            this.FirstName = "";
            this.LastName = "";
            this.ZipCode = "";
            this.UserCountry = "";
            this.SiteProfileID = "0";
            this.SiteMirrorProfileID = "0";
            this.ParentLoginName = "[no parent]";

            if (!row.IsFirstNameNull()) this.FirstName = row.FirstName;
            if (!row.IsLastNameNull()) this.LastName = row.LastName;
            this.ProfileId = row.CustomerID.ToString();
            this.UserName = row.LoginName;
            if (!row.IsCityNull()) this.UserCity = row.City;
            if (!row.Is_RegionNull()) this.UserRegion = row._Region;
            this.DateRegister = Utils.GetDateWithOffset(row.DateTimeToRegister).ToString("yyyyMMdd HH:mm");
            this.GenderId = row.GenderId.ToString();
            this.Password = row.Password;
            if (!row.IsZipNull()) this.ZipCode = row.Zip;

            if (!row.IsReferrerParentIdNull()) {
                this.ReferrerParentId = row.ReferrerParentId.ToString();
               
                if (row.ReferrerParentId == 1) 
                    this.ParentLoginName = "root";
                else 
                    this.ParentLoginName = Dating.Server.Core.DLL.DataHelpers.GetEUS_Profile_LoginName_ByProfileID(row.ReferrerParentId);
            }

            if (!row.IsCountryNull()) this.UserCountry = ProfileHelper.GetCountryName(row.Country);
            if (!row.IsSiteProfileIDNull()) this.SiteProfileID = row.SiteProfileID.ToString();
            if (!row.IsSiteMirrorProfileIDNull()) this.SiteMirrorProfileID = row.SiteMirrorProfileID.ToString();


        }


    }

}

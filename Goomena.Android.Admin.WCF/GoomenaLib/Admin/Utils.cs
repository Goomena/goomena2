﻿using Dating.Server.Core.DLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoomenaLib.Admin
{
    public class Utils
    {
        //public string BoolToString(System.Data.DataRow row, string col) {
        //    string result = "0";
        //    if (row[col])
        //    return result;
        //}


        public static DateTime GetDateWithOffset(DateTime inp) {
            TimeZoneInfo GTBZone = TimeZoneInfo.FindSystemTimeZoneById("GTB Standard Time");
            DateTime GTBTime = TimeZoneInfo.ConvertTime(inp, TimeZoneInfo.Utc, GTBZone);
            return GTBTime;
        }


        public static DateTime GetDateUtc(DateTime inp) {
            TimeZoneInfo GTBZone = TimeZoneInfo.FindSystemTimeZoneById("GTB Standard Time");
            DateTime GTBTime = TimeZoneInfo.ConvertTime(inp, GTBZone, TimeZoneInfo.Utc);
            return GTBTime;
        }


        public static int GetMemberOnlineMins() {
            clsConfigValues config = new clsConfigValues();

            int mins = 20;
            if ((!int.TryParse(config.members_online_minutes, out mins)))
                mins = 20;
            DateTime utcDate = System.DateTime.UtcNow.AddMinutes(-mins);
            return mins;

        }


    }
}

﻿
using Dating.Admin.Datasets.DLL;

public class UserRightsHelper
{
    public static DSSYSData.SYS_UsersDataTable gSYS_Users;

    public static DSSYSData.SYS_UsersRow GetUserRow(int adminUserId) {
        DSSYSData.SYS_UsersRow[] dr1 = (DSSYSData.SYS_UsersRow[])UserRightsHelper.gSYS_Users.Select("UsersID=" + adminUserId);
        if (dr1.Length > 0)
            return dr1[0];
        return null;
    }

    public static bool CheckIsAllowed_ProfilesManagerEdit(int adminUserId) {
        try {
            DSSYSData.SYS_UsersRow dr1 = GetUserRow(adminUserId);
            return (!dr1.IsProfilesManagerEditNull() ? dr1.ProfilesManagerEdit : false);
        }
        catch { }
        return false;
    }

    public static bool CheckIsAllowed_ProfilesManagerDelete(int adminUserId) {
        try {
            DSSYSData.SYS_UsersRow dr1 = GetUserRow(adminUserId);
            return (!dr1.IsProfilesManagerDeleteNull() ? dr1.ProfilesManagerDelete : false);
        }
        catch { }
        return false;
    }

    public static bool CheckIsAllowed(int adminUserId, string propertyName) {
        try {
            DSSYSData.SYS_UsersRow dr1 = GetUserRow(adminUserId);
            return (bool)dr1[propertyName];
        }
        catch { }
        return false;
    }


}

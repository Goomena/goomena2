﻿using Dating.Server.Core.DLL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace GoomenaLib.Admin
{
    [ServiceContract]
    public interface IAdminService
    {
        [OperationContract]
        LoginHeaders login(AdminUser user);

        [OperationContract]
        UserProfilesList getProfilesUpdating(LoginHeaders headers);

        [OperationContract]
        UserProfilesList getProfilesOnline(LoginHeaders headers);

        [OperationContract]
        UserProfileReferrerList getReferrersWaiting(LoginHeaders headers);

        [OperationContract]
        ImagePathList getNewPhotosD350(LoginHeaders headers, string profileid);

        [OperationContract]
        UserDetails getUserProfileDetails(LoginHeaders headers, string profileid);

        [OperationContract]
        string approveProfile(LoginHeaders headers, string profileid);

        [OperationContract]
        string rejectProfile(LoginHeaders headers, string profileid);

        [OperationContract]
        string deleteProfile(LoginHeaders headers, string profileid);

        [OperationContract]
        string approveProfilePhoto(LoginHeaders headers, string profileid, string photoid);

        [OperationContract]
        string rejectProfilePhoto(LoginHeaders headers, string profileid, string photoid);

        [OperationContract]
        string deletePhoto(LoginHeaders headers, string profileid, string photoid);

        [OperationContract]
        string setUserProfileDetails(UserProfileDetailsParams prms, string profileid);

        [OperationContract]
        TransactionsList getTransactions(LoginHeaders headers, string datefrom, string dateto);

        [OperationContract]
        string approveReferrer(LoginHeaders headers, string referrerid, string siteprofileid);

        [OperationContract]
        string rejectReferrer(LoginHeaders headers, string referrerid);

        [OperationContract]
        string deleteReferrer(LoginHeaders headers, string referrerid);

        [OperationContract]
        decimal getReferrerAmount(LoginHeaders headers, string profileid);

    }


    [DataContract]
    public class AdminUser
    {
        [DataMember]
        public string Login { get; set; }
        [DataMember]
        public string Password { get; set; }
        [DataMember]
        public string logindetails { get; set; }
    }

    [DataContract]
    public class LoginHeaders
    {
        [DataMember]
        public string LoginName { get; set; }

        [DataMember]
        public string Password { get; set; }

        [DataMember]
        public string Token { get; set; }

        [DataMember]
        public string UserID { get; set; }

        public Dating.Admin.Core.DLL.clsWSHeaders GetWSHeaders() {
            Dating.Admin.Core.DLL.clsWSHeaders headersWS = new Dating.Admin.Core.DLL.clsWSHeaders()
            {
                HardwareID = "",
                LoginName = this.LoginName,
                Password = this.Password,
                Token = this.Token
            };

            int iud = 0;
            if (!string.IsNullOrEmpty(this.UserID) && int.TryParse(this.UserID, out iud))
                headersWS.UserID = iud;

            return headersWS;
        }


        public void ReadWSHeaders(Dating.Admin.Core.DLL.clsWSHeaders headers) {
            this.LoginName = headers.LoginName;
            this.Password = headers.Password;
            this.Token = headers.Token;
            this.UserID = headers.UserID.ToString();
        }

    }


    [DataContract]
    public class DateRetrieveParams
    {
        public string DateFrom { get; set; }
        public string DateTo { get; set; }
        public string PreviousRowNumber { get; set; }
        public string RecordsToSelect { get; set; }
    }

    [DataContract]
    public class ProfilesRetrieveParams : DateRetrieveParams
    {
        public string Status { get; set; }
        public string MasterRecords { get; set; }
        public string NonMasterRecords { get; set; }
        public string QuickSelect { get; set; }

        public string SearchLogin { get; set; }
        public string SearchEmail { get; set; }

        public string LoadReferrersProfiles { get; set; }
        public string LoadAffiliateProfiles { get; set; }
    }


    [DataContract]
    public class UserProfilesList
    {
        List<UserProfile> profiles;

        public UserProfilesList() {
            profiles = new List<UserProfile>();
        }

        [DataMember]
        public List<UserProfile> Profiles {
            get { return profiles; }
            set { profiles = value; }
        }
    }



    [DataContract]
    public class UserProfileReferrerList
    {
        List<UserProfileReferrer> profiles;

        public UserProfileReferrerList() {
            profiles = new List<UserProfileReferrer>();
        }

        [DataMember]
        public List<UserProfileReferrer> Profiles {
            get { return profiles; }
            set { profiles = value; }
        }
    }


    [DataContract]
    public class ImagePath
    {

        [DataMember]
        public string Path { get; set; }
        [DataMember]
        public string PhotoId { get; set; }
        [DataMember]
        public string Level { get; set; }
        [DataMember]
        public string HasAproved { get; set; }
        [DataMember]
        public string HasDeclined { get; set; }
        [DataMember]
        public string IsDefault { get; set; }
        [DataMember]
        public string IsDeleted { get; set; }
        [DataMember]
        public string ShowOnFrontPage { get; set; }
        [DataMember]
        public string ShowOnGrid { get; set; }
        [DataMember]
        public string IsUpdating { get; set; }
    }

    [DataContract]
    public class ImagePathList
    {
        private List<ImagePath> pathList;

        public ImagePathList() {
            pathList = new List<ImagePath>();
        }

        [DataMember]
        public List<ImagePath> PathList {
            get { return pathList; }
            set { pathList = value; }
        }

    }

    [DataContract]
    public class UserProfileDetailsParams
    {
        [DataMember]
        public LoginHeaders LoginHeaders { get; set; }
        [DataMember]
        public UserDetails UserDetails { get; set; }
    }


    [DataContract]
    public class TransactionsList
    {
        public TransactionsList() {
            list = new List<Transactions>();
        }

        [DataMember]
        public List<Transactions> list { get; set; }
    }


    /*
    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    // You can add XSD files into the project. After building the project, you can directly use the data types defined there, with the namespace "GoomenaLib.ContractType".
    [DataContract]
    public class CompositeType
    {
        bool boolValue = true;
        string stringValue = "Hello ";

        [DataMember]
        public bool BoolValue {
            get { return boolValue; }
            set { boolValue = value; }
        }

        [DataMember]
        public string StringValue {
            get { return stringValue; }
            set { stringValue = value; }
        }
    }

    [DataContract]
    public class GCMUserSettings
    {
        [DataMember]
        public string profileId { get; set; }
        [DataMember]
        public string notifMessages { get; set; }
        [DataMember]
        public string notifLikes { get; set; }
        [DataMember]
        public string notifOffers { get; set; }
    }

    [DataContract]
    public class ProfileDeviceId
    {
        [DataMember]
        public string deviceId { get; set; }

        [DataMember]
        public UserProfile profile { get; set; }
    }

    [DataContract]
    public class NotificationParams
    {
        [DataMember]
        public string message { get; set; }
        [DataMember]
        public List<string> deviceIds { get; set; }
    }

    [DataContract]
    public class StoreUserParams
    {
        [DataMember]
        public string regId { get; set; }
        [DataMember]
        public string profileId { get; set; }
        [DataMember]
        public string loginname { get; set; }
        [DataMember]
        public string email { get; set; }
    }

    [DataContract]
    public class LoginParameters
    {
        [DataMember]
        public string email { get; set; }
        [DataMember]
        public string password { get; set; }
        [DataMember]
        public string logindetails { get; set; }
    }

    [DataContract]
    public class RegisterData
    {
        [DataMember]
        public string email { get; set; }

        [DataMember]
        public string username { get; set; }

        [DataMember]
        public string password { get; set; }

        [DataMember]
        public string country { get; set; }

        [DataMember]
        public string region { get; set; }

        [DataMember]
        public string city { get; set; }

        [DataMember]
        public string birthday { get; set; }

        [DataMember]
        public string gender { get; set; }

        [DataMember]
        public string lag { get; set; }
    }

    [DataContract]
    public class ImagePath
    {
        private string path;

        [DataMember]
        public string Path {
            get { return path; }
            set { path = value; }
        }

        [DataMember]
        public String photoId { get; set; }

        [DataMember]
        public int photoType { get; set; }
    }

    [DataContract]
    public class ImagePathList
    {
        private List<ImagePath> pathList;

        [DataMember]
        public List<ImagePath> PathList {
            get { return pathList; }
            set { pathList = value; }
        }


    }

    [DataContract]
    public class MessageInfo
    {
        [DataMember]
        public string ProfileId { get; set; }

        [DataMember]
        public string MsgId { get; set; }

        [DataMember]
        public string MsgCount { get; set; }

        [DataMember]
        public string UserName { get; set; }

        [DataMember]
        public string Age { get; set; }

        [DataMember]
        public string City { get; set; }
        [DataMember]
        public string Region { get; set; }
        [DataMember]
        public string Country { get; set; }

        [DataMember]
        public string ProfileImg { get; set; }

        [DataMember]
        public string DateTimeSent { get; set; }
    }

    [DataContract]
    public class MessageInfoList
    {
        [DataMember]
        public List<MessageInfo> msgList { get; set; }
    }

    [DataContract]
    public class Message
    {
        [DataMember]
        public string MessageId { get; set; }
        [DataMember]
        public string FromProfileId { get; set; }
        [DataMember]
        public string ToProfileId { get; set; }
        [DataMember]
        public string DateTimeSent { get; set; }
        [DataMember]
        public string Body { get; set; }
        [DataMember]
        public string Subject { get; set; }
        [DataMember]
        public bool IsSent { get; set; }
        [DataMember]
        public bool IsReceived { get; set; }
        [DataMember]
        public string statusId { get; set; }


    }

    [DataContract]
    public class MessageList
    {
        [DataMember]
        public List<Message> list { get; set; }

    }

    [DataContract]
    public class MessageParams
    {
        [DataMember]
        public string subject { get; set; }
        [DataMember]
        public string body { get; set; }
        [DataMember]
        public string fromprofileid { get; set; }
        [DataMember]
        public string toprofileid { get; set; }
        [DataMember]
        public string genderid { get; set; }
        [DataMember]
        public string offerId { get; set; }

    }

    [DataContract]
    public class UploadPhotoParams
    {
        [DataMember]
        public string strFileName { get; set; }

        [DataMember]
        public Stream streamFileContent { get; set; }

        [DataMember]
        public string profileid { get; set; }

        [DataMember]
        public string photoLevel { get; set; }

    }

    [DataContract]
    public class Offer
    {
        [DataMember]
        public UserProfile profile { get; set; }

        [DataMember]
        public string offerId { get; set; }

        [DataMember]
        public string rejectReason { get; set; }

    }

    [DataContract]
    public class OfferList
    {
        [DataMember]
        public List<Offer> offers { get; set; }
    }

    [DataContract]
    public class CountryList
    {
        [DataMember]
        public List<Country> countries { get; set; }
    }

    [DataContract]
    public class Country
    {
        [DataMember]
        public string name { get; set; }

        [DataMember]
        public string iso { get; set; }
    }

    [DataContract]
    public class Regions
    {
        [DataMember]
        public List<String> list { get; set; }
    }

    [DataContract]
    public class Cities
    {
        [DataMember]
        public List<String> list { get; set; }


    }

     * */
}

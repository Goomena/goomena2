﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;

namespace GoomenaLib
{
    public class CurrentSettings
    {


        public static string ConnectionString {
            get {
                return System.Configuration.ConfigurationManager.ConnectionStrings["GoomenaLib.Properties.Settings.CMSGoomenaConnectionString"].ConnectionString;
            }
        }

        public static string ConnectionString_GEODB {
            get {
                return System.Configuration.ConfigurationManager.ConnectionStrings["GeoDBconnectionString"].ConnectionString;
            }
        }


        public static string gToEmail {
            get {
                return System.Configuration.ConfigurationManager.AppSettings["gToEmail"];
            }
        }

        public static bool AllowSendExceptions {
            get {
                return (System.Configuration.ConfigurationManager.AppSettings["AllowSendExceptions"] == "True");
            }
        }

        public static string gSMTPOutPort {
            get {
                return System.Configuration.ConfigurationManager.AppSettings["gSMTPOutPort"];
            }
        }

        public static string gSMTPServerName {
            get {
                return System.Configuration.ConfigurationManager.AppSettings["gSMTPServerName"];
            }
        }

        public static string gSMTPLoginName {
            get {
                return System.Configuration.ConfigurationManager.AppSettings["gSMTPLoginName"];
            }
        }

        public static string gSMTPPassword {
            get {
                return System.Configuration.ConfigurationManager.AppSettings["gSMTPPassword"];
            }
        }

        public static string ExceptionsEmail {
            get {
                return System.Configuration.ConfigurationManager.AppSettings["ExceptionsEmail"];
            }
        }


    }
}

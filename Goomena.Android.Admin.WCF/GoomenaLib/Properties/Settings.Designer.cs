﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34014
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GoomenaLib.Properties {
    
    
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "12.0.0.0")]
    internal sealed partial class Settings : global::System.Configuration.ApplicationSettingsBase {
        
        private static Settings defaultInstance = ((Settings)(global::System.Configuration.ApplicationSettingsBase.Synchronized(new Settings())));
        
        public static Settings Default {
            get {
                return defaultInstance;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("info@goomena.com")]
        public string gToEmail {
            get {
                return ((string)(this["gToEmail"]));
            }
            set {
                this["gToEmail"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("mail.goomena.com")]
        public string gSMTPServerName {
            get {
                return ((string)(this["gSMTPServerName"]));
            }
            set {
                this["gSMTPServerName"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("support@goomena.com")]
        public string gSMTPLoginName {
            get {
                return ((string)(this["gSMTPLoginName"]));
            }
            set {
                this["gSMTPLoginName"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("Greece2012")]
        public string gSMTPPassword {
            get {
                return ((string)(this["gSMTPPassword"]));
            }
            set {
                this["gSMTPPassword"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("25")]
        public string gSMTPOutPort {
            get {
                return ((string)(this["gSMTPOutPort"]));
            }
            set {
                this["gSMTPOutPort"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("exceptions@zizina.com")]
        public string ExceptionsEmail {
            get {
                return ((string)(this["ExceptionsEmail"]));
            }
            set {
                this["ExceptionsEmail"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool AllowSendExceptions {
            get {
                return ((bool)(this["AllowSendExceptions"]));
            }
            set {
                this["AllowSendExceptions"] = value;
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.ConnectionString)]
        [global::System.Configuration.DefaultSettingValueAttribute("Data Source=KOUKOS-PC\\SERVER;Initial Catalog=Goomena__Local;Persist Security Info" +
            "=True;User ID=koukos;Password=ko5022@;Application Name=android_app_vs")]
        public string CMSGoomenaConnectionString {
            get {
                return ((string)(this["CMSGoomenaConnectionString"]));
            }
        }
    }
}

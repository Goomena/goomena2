﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Data.SqlClient

<System.Web.Services.WebService(Namespace:="http://www.000.gr/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class CMSWebServices
    Inherits System.Web.Services.WebService

    <WebMethod()> _
    Public Function Hello() As String
        Return "OK"
    End Function

    Private Function CheckToken(Token As String)
        Try
            Dim ret = gTokenList.Contains(Token)
            Return ret
        Catch ex As Exception
        End Try
    End Function

#Region "Login"

    <WebMethod()> _
    Public Function Login(DataRecordLogin As clsDataRecordLogin) As clsDataRecordLoginReturn
        Dim DataRecordLoginReturn As New clsDataRecordLoginReturn
        DataRecordLoginReturn.IsValid = False
        DataRecordLoginReturn.HasErrors = False

        Try
            Dim SYS_UsersDataTable As New UniCMSDB.SYS_UsersDataTable
            Dim ta As New UniCMSDBTableAdapters.SYS_UsersTableAdapter
            ta.FillByLoginPass(SYS_UsersDataTable, DataRecordLogin.LoginName, DataRecordLogin.Password)
            If SYS_UsersDataTable.Rows.Count > 0 Then

                Dim SYS_UsersRow As UniCMSDB.SYS_UsersRow = SYS_UsersDataTable.Rows(0)
                If SYS_UsersRow.IsActive Then
                    Dim Token As String = Library.Public.clsHash.ComputeHash(DataRecordLogin.HardwareID & "^^" & DataRecordLogin.LoginName & "^Token$$$", "SHA1")
                    gTokenList.Add(Token)
                    DataRecordLoginReturn.Token = Token
                    DataRecordLoginReturn.HasErrors = False
                    DataRecordLoginReturn.Message = "OK"
                    DataRecordLoginReturn.IsValid = True
                Else
                    DataRecordLoginReturn.HasErrors = True
                    DataRecordLoginReturn.Message = "This user not Active"
                End If
            Else
                DataRecordLoginReturn.Message = "Error Login Or Password"
            End If
        Catch ex As Exception
            DataRecordLoginReturn.HasErrors = True
            DataRecordLoginReturn.Message = ex.Message
        End Try
        Return DataRecordLoginReturn
    End Function

#End Region

    <WebMethod()> _
    Public Function GetUsers(Token As String, ByRef SYS_Users As UniCMSDB) As clsDataRecordErrorsReturn
        Dim ReturnErrors As New clsDataRecordErrorsReturn
        Try
            If CheckToken(Token) Then
                Dim ta As New UniCMSDBTableAdapters.SYS_UsersTableAdapter
                ta.Fill(SYS_Users.SYS_Users)
                ReturnErrors.HasErrors = False
                ReturnErrors.Message = "OK"
            Else
                ReturnErrors.HasErrors = True
                ReturnErrors.Message = "Invalid Token!"
                ReturnErrors.ErrorCode = "999"
            End If
        Catch ex As Exception
            ReturnErrors.HasErrors = True
            ReturnErrors.Message = ex.Message
            ReturnErrors.ErrorCode = "101"
            ReturnErrors.ExtraMessage = ""
        End Try
        Return ReturnErrors
    End Function


#Region "SYS_SiteBasic"

    <WebMethod()> _
    Public Function GetSYS_SiteBasic(Token As String, ByRef SYS_SiteBasic As UniCMSDB) As clsDataRecordErrorsReturn
        Dim ReturnErrors As New clsDataRecordErrorsReturn
        Try
            If CheckToken(Token) Then
                Dim ta As New UniCMSDBTableAdapters.SYS_SiteBasicTableAdapter
                ta.Fill(SYS_SiteBasic.SYS_SiteBasic)
                ReturnErrors.HasErrors = False
                ReturnErrors.Message = "OK"
            Else
                ReturnErrors.HasErrors = True
                ReturnErrors.Message = "Invalid Token!"
                ReturnErrors.ErrorCode = "999"
            End If
        Catch ex As Exception
            ReturnErrors.HasErrors = True
            ReturnErrors.Message = ex.Message
            ReturnErrors.ErrorCode = "101"
            ReturnErrors.ExtraMessage = ""
        End Try
        Return ReturnErrors
    End Function

    <WebMethod()> _
    Public Function UpdateSYS_SiteBasic(Token As String, ByRef SYS_SiteBasic As UniCMSDB) As clsDataRecordErrorsReturn
        Dim ReturnErrors As New clsDataRecordErrorsReturn
        Try
            If CheckToken(Token) Then
                Dim ta As New UniCMSDBTableAdapters.SYS_SiteBasicTableAdapter

                'Dim cbCust As New SqlCommandBuilder(ta)


                ta.Update(SYS_SiteBasic.SYS_SiteBasic)
                ReturnErrors.HasErrors = False
                ReturnErrors.Message = "OK"
            Else
                ReturnErrors.HasErrors = True
                ReturnErrors.Message = "Invalid Token!"
                ReturnErrors.ErrorCode = "999"
            End If
        Catch ex As Exception
            ReturnErrors.HasErrors = True
            ReturnErrors.Message = ex.Message
            ReturnErrors.ErrorCode = "101"
            ReturnErrors.ExtraMessage = ""
        End Try
        Return ReturnErrors
    End Function


#End Region


#Region "SYS_SitePages"

    <WebMethod()> _
    Public Function GetSYS_SitePages(Token As String, ByRef SitePages As UniCMSDB) As clsDataRecordErrorsReturn
        Dim ReturnErrors As New clsDataRecordErrorsReturn
        Try
            If CheckToken(Token) Then
                Dim ta As New UniCMSDBTableAdapters.SYS_SitePagesTableAdapter
                ta.Fill(SitePages.SYS_SitePages)
                ReturnErrors.HasErrors = False
                ReturnErrors.Message = "OK"
            Else
                ReturnErrors.HasErrors = True
                ReturnErrors.Message = "Invalid Token!"
                ReturnErrors.ErrorCode = "999"
            End If
        Catch ex As Exception
            ReturnErrors.HasErrors = True
            ReturnErrors.Message = ex.Message
            ReturnErrors.ErrorCode = "101"
            ReturnErrors.ExtraMessage = ""
        End Try
        Return ReturnErrors
    End Function

    <WebMethod()> _
    Public Function GetSYS_SitePages_FillBySitePageID(Token As String, SitePageID As Integer, ByRef SYS_SitePages As UniCMSDB) As clsDataRecordErrorsReturn
        Dim ReturnErrors As New clsDataRecordErrorsReturn
        Try
            If CheckToken(Token) Then
                Dim ta As New UniCMSDBTableAdapters.SYS_SitePagesTableAdapter
                ta.FillBySitePageID(SYS_SitePages.SYS_SitePages, SitePageID)
                ReturnErrors.HasErrors = False
                ReturnErrors.Message = "OK"
            Else
                ReturnErrors.HasErrors = True
                ReturnErrors.Message = "Invalid Token!"
                ReturnErrors.ErrorCode = "999"
            End If
        Catch ex As Exception
            ReturnErrors.HasErrors = True
            ReturnErrors.Message = ex.Message
            ReturnErrors.ErrorCode = "101"
            ReturnErrors.ExtraMessage = ""
        End Try
        Return ReturnErrors
    End Function

    <WebMethod()> _
    Public Function GetSYS_SitePages_FillByPageFileName(Token As String, PageFileName As String, ByRef SYS_SitePages As UniCMSDB) As clsDataRecordErrorsReturn
        Dim ReturnErrors As New clsDataRecordErrorsReturn
        Try
            If CheckToken(Token) Then
                Dim ta As New UniCMSDBTableAdapters.SYS_SitePagesTableAdapter
                ta.FillByPageFileName(SYS_SitePages.SYS_SitePages, PageFileName)
                ReturnErrors.HasErrors = False
                ReturnErrors.Message = "OK Read By PageFileName:" & PageFileName
            Else
                ReturnErrors.HasErrors = True
                ReturnErrors.Message = "Invalid Token!"
                ReturnErrors.ErrorCode = "999"
            End If
        Catch ex As Exception
            ReturnErrors.HasErrors = True
            ReturnErrors.Message = ex.Message
            ReturnErrors.ErrorCode = "101"
            ReturnErrors.ExtraMessage = ""
        End Try
        Return ReturnErrors
    End Function

    <WebMethod()> _
    Public Function GetSYS_SitePages_FillByArea(Token As String, Area As String, ByRef SYS_SitePages As UniCMSDB) As clsDataRecordErrorsReturn
        Dim ReturnErrors As New clsDataRecordErrorsReturn
        Try
            If CheckToken(Token) Then
                Dim ta As New UniCMSDBTableAdapters.SYS_SitePagesTableAdapter
                ta.FillByArea(SYS_SitePages.SYS_SitePages, Area)
                ReturnErrors.HasErrors = False
                ReturnErrors.Message = "OK"
            Else
                ReturnErrors.HasErrors = True
                ReturnErrors.Message = "Invalid Token!"
                ReturnErrors.ErrorCode = "999"
            End If
        Catch ex As Exception
            ReturnErrors.HasErrors = True
            ReturnErrors.Message = ex.Message
            ReturnErrors.ErrorCode = "101"
            ReturnErrors.ExtraMessage = ""
        End Try
        Return ReturnErrors
    End Function

    <WebMethod()> _
    Public Function Update_SYS_SitePages(Token As String, SYS_SitePages As UniCMSDB) As clsDataRecordErrorsReturn
        Dim ReturnErrors As New clsDataRecordErrorsReturn
        Try
            If CheckToken(Token) Then
                Dim ta As New UniCMSDBTableAdapters.SYS_SitePagesTableAdapter
                ta.Update(SYS_SitePages.SYS_SitePages)
                ReturnErrors.HasErrors = False
                ReturnErrors.Message = "OK"
            Else
                ReturnErrors.HasErrors = True
                ReturnErrors.Message = "Invalid Token!"
                ReturnErrors.ErrorCode = "999"
            End If
        Catch ex As Exception
            ReturnErrors.HasErrors = True
            ReturnErrors.Message = ex.Message
            ReturnErrors.ErrorCode = "101"
            ReturnErrors.ExtraMessage = ""
        End Try
        Return ReturnErrors
    End Function
#End Region

#Region "SYS_MessagesGRID"

    <WebMethod()> _
    Public Function GetSYS_MessagesGRID(Token As String, ByRef SYS_MessagesGRID As UniCMSDB) As clsDataRecordErrorsReturn
        Dim ReturnErrors As New clsDataRecordErrorsReturn
        Try
            If CheckToken(Token) Then
                Dim ta As New UniCMSDBTableAdapters.SYS_MessagesGRIDTableAdapter
                ta.Fill(SYS_MessagesGRID.SYS_MessagesGRID)
                ReturnErrors.HasErrors = False
                ReturnErrors.Message = "OK"
            Else
                ReturnErrors.HasErrors = True
                ReturnErrors.Message = "Invalid Token!"
                ReturnErrors.ErrorCode = "999"
            End If
        Catch ex As Exception
            ReturnErrors.HasErrors = True
            ReturnErrors.Message = ex.Message
            ReturnErrors.ErrorCode = "101"
            ReturnErrors.ExtraMessage = ""
        End Try
        Return ReturnErrors
    End Function

    <WebMethod()> _
    Public Function GetSYS_MessagesGRID_FillBySitePageID(Token As String, ByRef SYS_MessagesGRID As UniCMSDB, FillBySitePageID As Long) As clsDataRecordErrorsReturn
        Dim ReturnErrors As New clsDataRecordErrorsReturn
        Try
            If CheckToken(Token) Then
                Dim ta As New UniCMSDBTableAdapters.SYS_MessagesGRIDTableAdapter
                ta.FillBySitePageID(SYS_MessagesGRID.SYS_MessagesGRID, FillBySitePageID)
                ReturnErrors.HasErrors = False
                ReturnErrors.Message = "OK"
            Else
                ReturnErrors.HasErrors = True
                ReturnErrors.Message = "Invalid Token!"
                ReturnErrors.ErrorCode = "999"
            End If
        Catch ex As Exception
            ReturnErrors.HasErrors = True
            ReturnErrors.Message = ex.Message
            ReturnErrors.ErrorCode = "101"
            ReturnErrors.ExtraMessage = ""
        End Try
        Return ReturnErrors
    End Function


#End Region

#Region "SYS_SiteMenuBase"

    <WebMethod()> _
    Public Function GetSYS_SiteMenuBase(Token As String, ByRef SYS_SiteMenuBase As UniCMSDB) As clsDataRecordErrorsReturn
        Dim ReturnErrors As New clsDataRecordErrorsReturn
        Try
            If CheckToken(Token) Then
                Dim ta As New UniCMSDBTableAdapters.SYS_SiteMenuBaseTableAdapter
                ta.Fill(SYS_SiteMenuBase.SYS_SiteMenuBase)
                ReturnErrors.HasErrors = False
                ReturnErrors.Message = "OK"
            Else
                ReturnErrors.HasErrors = True
                ReturnErrors.Message = "Invalid Token!"
                ReturnErrors.ErrorCode = "999"
            End If
        Catch ex As Exception
            ReturnErrors.HasErrors = True
            ReturnErrors.Message = ex.Message
            ReturnErrors.ErrorCode = "101"
            ReturnErrors.ExtraMessage = ""
        End Try
        Return ReturnErrors
    End Function

    <WebMethod()> _
    Public Function UpdateSYS_SiteMenuBase(Token As String, SYS_SiteMenuBase As UniCMSDB) As clsDataRecordErrorsReturn
        Dim ReturnErrors As New clsDataRecordErrorsReturn
        Try
            If CheckToken(Token) Then
                Dim ta As New UniCMSDBTableAdapters.SYS_SiteMenuBaseTableAdapter
                ta.Update(SYS_SiteMenuBase.SYS_SiteMenuBase)
                ReturnErrors.HasErrors = False
                ReturnErrors.Message = "OK"
            Else
                ReturnErrors.HasErrors = True
                ReturnErrors.Message = "Invalid Token!"
                ReturnErrors.ErrorCode = "999"
            End If
        Catch ex As Exception
            ReturnErrors.HasErrors = True
            ReturnErrors.Message = ex.Message
            ReturnErrors.ErrorCode = "101"
            ReturnErrors.ExtraMessage = ""
        End Try
        Return ReturnErrors
    End Function
#End Region

#Region "SYS_Messages"

    <WebMethod()> _
    Public Function GetSYS_Messages(Token As String, ByRef SYS_Messages As UniCMSDB) As clsDataRecordErrorsReturn
        Dim ReturnErrors As New clsDataRecordErrorsReturn
        Try
            If CheckToken(Token) Then
                Dim ta As New UniCMSDBTableAdapters.SYS_MessagesTableAdapter
                ta.Fill(SYS_Messages.SYS_Messages)
                ReturnErrors.HasErrors = False
                ReturnErrors.Message = "OK"
            Else
                ReturnErrors.HasErrors = True
                ReturnErrors.Message = "Invalid Token!"
                ReturnErrors.ErrorCode = "999"
            End If
        Catch ex As Exception
            ReturnErrors.HasErrors = True
            ReturnErrors.Message = ex.Message
            ReturnErrors.ErrorCode = "101"
            ReturnErrors.ExtraMessage = ""
        End Try
        Return ReturnErrors
    End Function

    <WebMethod()> _
    Public Function GetSYS_Messages_FillBySitePageID(Token As String, SitePageID As Long, ByRef SYS_Messages As UniCMSDB) As clsDataRecordErrorsReturn
        Dim ReturnErrors As New clsDataRecordErrorsReturn
        Try
            If CheckToken(Token) Then
                Dim ta As New UniCMSDBTableAdapters.SYS_MessagesTableAdapter
                ta.FillBySitePageID(SYS_Messages.SYS_Messages, SitePageID)
                ReturnErrors.HasErrors = False
                ReturnErrors.Message = "OK"
            Else
                ReturnErrors.HasErrors = True
                ReturnErrors.Message = "Invalid Token!"
                ReturnErrors.ErrorCode = "999"
            End If
        Catch ex As Exception
            ReturnErrors.HasErrors = True
            ReturnErrors.Message = ex.Message
            ReturnErrors.ErrorCode = "101"
            ReturnErrors.ExtraMessage = ""
        End Try
        Return ReturnErrors
    End Function

    <WebMethod()> _
    Public Function GetSYS_Messages_FillByMessageID(Token As String, FillByMessageID As Long, ByRef SYS_Messages As UniCMSDB) As clsDataRecordErrorsReturn
        Dim ReturnErrors As New clsDataRecordErrorsReturn
        Try
            If CheckToken(Token) Then
                Dim ta As New UniCMSDBTableAdapters.SYS_MessagesTableAdapter
                ta.FillByMessageID(SYS_Messages.SYS_Messages, FillByMessageID)
                ReturnErrors.HasErrors = False
                ReturnErrors.Message = "OK"
            Else
                ReturnErrors.HasErrors = True
                ReturnErrors.Message = "Invalid Token!"
                ReturnErrors.ErrorCode = "999"
            End If
        Catch ex As Exception
            ReturnErrors.HasErrors = True
            ReturnErrors.Message = ex.Message
            ReturnErrors.ErrorCode = "101"
            ReturnErrors.ExtraMessage = ""
        End Try
        Return ReturnErrors
    End Function

    <WebMethod()> _
    Public Function UpdateSYS_Messages(Token As String, SYS_Messages As UniCMSDB) As clsDataRecordErrorsReturn
        Dim ReturnErrors As New clsDataRecordErrorsReturn
        Try
            If CheckToken(Token) Then
                Dim ta As New UniCMSDBTableAdapters.SYS_MessagesTableAdapter
                ta.Update(SYS_Messages.SYS_Messages)
                ReturnErrors.HasErrors = False
                ReturnErrors.Message = "OK"
            Else
                ReturnErrors.HasErrors = True
                ReturnErrors.Message = "Invalid Token!"
                ReturnErrors.ErrorCode = "999"
            End If
        Catch ex As Exception
            ReturnErrors.HasErrors = True
            ReturnErrors.Message = ex.Message
            ReturnErrors.ErrorCode = "101"
            ReturnErrors.ExtraMessage = ""
        End Try
        Return ReturnErrors
    End Function

#End Region

#Region "SYS_SiteMenuItems"

    <WebMethod()> _
    Public Function GetSYS_SiteMenuItems(Token As String, ByRef SYS_SiteMenuItems As UniCMSDB) As clsDataRecordErrorsReturn
        Dim ReturnErrors As New clsDataRecordErrorsReturn
        Try
            If CheckToken(Token) Then
                Dim ta As New UniCMSDBTableAdapters.SYS_SiteMenuItemsTableAdapter
                ta.Fill(SYS_SiteMenuItems.SYS_SiteMenuItems)
                ReturnErrors.HasErrors = False
                ReturnErrors.Message = "OK"
            Else
                ReturnErrors.HasErrors = True
                ReturnErrors.Message = "Invalid Token!"
                ReturnErrors.ErrorCode = "999"
            End If
        Catch ex As Exception
            ReturnErrors.HasErrors = True
            ReturnErrors.Message = ex.Message
            ReturnErrors.ErrorCode = "101"
            ReturnErrors.ExtraMessage = ""
        End Try
        Return ReturnErrors
    End Function

    <WebMethod()> _
    Public Function GetSYS_SiteMenuItemsFillBySiteMenuBaseID(Token As String, SiteMenuBaseID As Integer, ByRef SYS_SiteMenuItems As UniCMSDB) As clsDataRecordErrorsReturn
        Dim ReturnErrors As New clsDataRecordErrorsReturn
        Try
            If CheckToken(Token) Then
                Dim ta As New UniCMSDBTableAdapters.SYS_SiteMenuItemsTableAdapter
                ta.FillBySiteMenuBaseID(SYS_SiteMenuItems.SYS_SiteMenuItems, SiteMenuBaseID)
                ReturnErrors.HasErrors = False
                ReturnErrors.Message = "OK"
            Else
                ReturnErrors.HasErrors = True
                ReturnErrors.Message = "Invalid Token!"
                ReturnErrors.ErrorCode = "999"
            End If
        Catch ex As Exception
            ReturnErrors.HasErrors = True
            ReturnErrors.Message = ex.Message
            ReturnErrors.ErrorCode = "101"
            ReturnErrors.ExtraMessage = ""
        End Try
        Return ReturnErrors
    End Function

    <WebMethod()> _
    Public Function UpdateSYS_SiteMenuItems(Token As String, SYS_SiteMenuItems As UniCMSDB) As clsDataRecordErrorsReturn
        Dim ReturnErrors As New clsDataRecordErrorsReturn
        Try
            If CheckToken(Token) Then
                Dim ta As New UniCMSDBTableAdapters.SYS_SiteMenuItemsTableAdapter
                ta.Update(SYS_SiteMenuItems.SYS_SiteMenuItems)
                ReturnErrors.HasErrors = False
                ReturnErrors.Message = "OK"
            Else
                ReturnErrors.HasErrors = True
                ReturnErrors.Message = "Invalid Token!"
                ReturnErrors.ErrorCode = "999"
            End If
        Catch ex As Exception
            ReturnErrors.HasErrors = True
            ReturnErrors.Message = ex.Message
            ReturnErrors.ErrorCode = "101"
            ReturnErrors.ExtraMessage = ""
        End Try
        Return ReturnErrors
    End Function


#End Region



End Class
﻿namespace GoogleTranslate
{
    partial class frmTranslate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtResults = new System.Windows.Forms.TextBox();
            this.txtInput = new System.Windows.Forms.TextBox();
            this.cbTranslateToLang = new System.Windows.Forms.ComboBox();
            this.cbTranslateFromLang = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.picLoadingPhoto = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.picLoadingPhoto)).BeginInit();
            this.SuspendLayout();
            // 
            // txtResults
            // 
            this.txtResults.Location = new System.Drawing.Point(346, 56);
            this.txtResults.Multiline = true;
            this.txtResults.Name = "txtResults";
            this.txtResults.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtResults.Size = new System.Drawing.Size(256, 343);
            this.txtResults.TabIndex = 1;
            // 
            // txtInput
            // 
            this.txtInput.Location = new System.Drawing.Point(12, 56);
            this.txtInput.Multiline = true;
            this.txtInput.Name = "txtInput";
            this.txtInput.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtInput.Size = new System.Drawing.Size(234, 343);
            this.txtInput.TabIndex = 2;
            this.txtInput.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // cbTranslateToLang
            // 
            this.cbTranslateToLang.FormattingEnabled = true;
            this.cbTranslateToLang.Items.AddRange(new object[] {
            "Greek",
            "Spanish",
            "English",
            "Turkish"});
            this.cbTranslateToLang.Location = new System.Drawing.Point(417, 12);
            this.cbTranslateToLang.Name = "cbTranslateToLang";
            this.cbTranslateToLang.Size = new System.Drawing.Size(121, 21);
            this.cbTranslateToLang.TabIndex = 3;
            this.cbTranslateToLang.SelectedValueChanged += new System.EventHandler(this.comboBox1_SelectedValueChanged);
            // 
            // cbTranslateFromLang
            // 
            this.cbTranslateFromLang.FormattingEnabled = true;
            this.cbTranslateFromLang.Items.AddRange(new object[] {
            "Greek",
            "Spanish",
            "English",
            "Turkish"});
            this.cbTranslateFromLang.Location = new System.Drawing.Point(65, 13);
            this.cbTranslateFromLang.Name = "cbTranslateFromLang";
            this.cbTranslateFromLang.Size = new System.Drawing.Size(121, 21);
            this.cbTranslateFromLang.TabIndex = 4;
            this.cbTranslateFromLang.SelectedValueChanged += new System.EventHandler(this.comboBox2_SelectedValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(383, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(23, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "To:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(29, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "From";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(262, 202);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 7;
            this.button2.Text = "Translate";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // picLoadingPhoto
            // 
            this.picLoadingPhoto.Image = global::GoogleTranslate.Properties.Resources.loading_anim2;
            this.picLoadingPhoto.InitialImage = global::GoogleTranslate.Properties.Resources.loading_anim2;
            this.picLoadingPhoto.Location = new System.Drawing.Point(286, 110);
            this.picLoadingPhoto.Name = "picLoadingPhoto";
            this.picLoadingPhoto.Size = new System.Drawing.Size(26, 23);
            this.picLoadingPhoto.TabIndex = 452;
            this.picLoadingPhoto.TabStop = false;
            // 
            // frmTranslate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(614, 411);
            this.Controls.Add(this.picLoadingPhoto);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbTranslateFromLang);
            this.Controls.Add(this.cbTranslateToLang);
            this.Controls.Add(this.txtInput);
            this.Controls.Add(this.txtResults);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "frmTranslate";
            this.Text = "Translate";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Shown += new System.EventHandler(this.Form1_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.picLoadingPhoto)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtResults;
        private System.Windows.Forms.TextBox txtInput;
        private System.Windows.Forms.ComboBox cbTranslateToLang;
        private System.Windows.Forms.ComboBox cbTranslateFromLang;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.PictureBox picLoadingPhoto;
    }
}


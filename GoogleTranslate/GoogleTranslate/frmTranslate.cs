﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Net;

using System.Text;
using System.Windows.Forms;
using Google.API.Translate;
using FilesProxy.DownloadSites.Web;

namespace GoogleTranslate
{
    public partial class frmTranslate : Form
    {

        string fromLanguage = "English";
        string toLanguage = "Turkish";
        string content = "Thank You";
        string result = string.Empty;
        // bool useGreek = false;
        List<string> lstr = new List<string>();

        public string TextToTranslate;
        public string TextFromCountry;
        Dictionary<string, string> languageMap = new Dictionary<string, string>();
        Dictionary<string, string> languageMapReversed = new Dictionary<string, string>();

        public frmTranslate() {
            InitializeComponent();

        }

        //private void button1_Click(object sender, EventArgs e)
        //{
        //    if (string.IsNullOrWhiteSpace(textBox2.Text))
        //    {
        //        return;
        //    }
        //    var languageMap = new Dictionary<string, string>();
        //    InitLanguageMap(languageMap);
        //    Uri address = new Uri("http://translate.google.com/translate_t");
        //    WebClient wc = new WebClient();
        //    wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
        //    wc.Headers[HttpRequestHeader.UserAgent] = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.66 Safari/537.36";
        //    //wc.Headers[HttpRequestHeader.ContentLength] = ( textBox2.Text.Length + 30 ).ToString();
        //    wc.UploadStringCompleted += new UploadStringCompletedEventHandler(wc_UploadStringCompleted);
        //    if (useGreek)
        //    {
        //        wc.Encoding = System.Text.Encoding.GetEncoding("iso-8859-7"); 
        //    }
        //    else
        //    {
        //        wc.Encoding = System.Text.Encoding.UTF8;
        //    }
        //    //

        //    // Async Upload to the specified source i.e http://translate.google.com/translate_t for handling the translation.
        //    wc.UploadStringAsync(address, GetPostData(languageMap[fromLanguage], languageMap[toLanguage], content));

        //}

        private string GetPostData(string fromLanguage, string toLanguage, string content) {
            // Set the language translation. All we need is the language pair, from and to.
            string strPostData = string.Format("hl=en&ie=UTF8&oe=UTF8submit=Translate&langpair={0}|{1}",
                                                 fromLanguage,
                                                 toLanguage);

            // Encode the content and set the text query string param
            return strPostData += "&text=" + HttpUtility.UrlEncode(content);
        }

        private string getResult(string htmlSource) {
            int indexOfResult = -1;
            indexOfResult = htmlSource.IndexOf("result_box");
            if (indexOfResult == -1)
                return "";
            string tempResult = "";
            bool isOK = false;
            for (int i = indexOfResult; i < htmlSource.Length; i++) {

                string tempString = htmlSource.Substring(i, 7);
                if (htmlSource.Substring(i, 13) == "</span></div>")
                    break;


                if (tempString == "</span>") {
                    tempResult = "";
                    for (int j = i; j > 0; j--) {

                        tempString = htmlSource.Substring(j, 1);
                        if (tempString == ">") {
                            isOK = true;
                            break;
                        }
                        else
                            tempResult += htmlSource.Substring(j, 1);
                    }
                }

                if (isOK) {
                    char[] charArray = tempResult.ToCharArray();
                    Array.Reverse(charArray);
                    result += new string(charArray);
                    result = result.Replace(">", "");
                    result = result.Replace("<", "");
                    isOK = false;
                    //return HttpUtility.HtmlDecode(result);

                }


            }


            return result;
        }



        //private void wc_UploadStringCompleted(object sender, UploadStringCompletedEventArgs e)
        //{
        //    if (e.Result != null)
        //    {
        //        result = "";
        //        string htmlSource = e.Result;
        //        int indexOfResult = -1;
        //        indexOfResult = htmlSource.IndexOf("result_box");
        //        if (indexOfResult == -1)
        //            return;

        //        bool isOK = false;
        //        for (int i = indexOfResult; i < htmlSource.Length; i++)
        //        {
        //            string tempString = htmlSource.Substring(i, 7);
        //            if (tempString == "</span>")
        //            {
        //                for (int j = i; j > 0; j--)
        //                {
        //                    tempString = htmlSource.Substring(j, 1);
        //                    if (tempString == ">")
        //                    {
        //                        isOK = true;
        //                        break;
        //                    }
        //                    else
        //                        result += htmlSource.Substring(j, 1);
        //                }
        //            }
        //            if (isOK)
        //            {
        //                char[] charArray = result.ToCharArray();
        //                Array.Reverse(charArray);
        //                result = new string(charArray);
        //                result = result.Replace(">", "");
        //                result = result.Replace("<", "");
        //                textBox1.Text = HttpUtility.HtmlDecode( result);
        //                break;
        //            }


        //        }
        //    }
        //}  

        private void InitLanguageMap() {
            languageMap.Clear();
            languageMap.Add("English", "en");
            languageMap.Add("Spanish", "es");
            languageMap.Add("Greek", "el");
            languageMap.Add("Turkish", "tr");


            languageMapReversed.Clear();
            languageMapReversed.Add("en", "English");
            languageMapReversed.Add("es", "Spanish");
            languageMapReversed.Add("el", "Greek");
            languageMapReversed.Add("tr", "Turkish");

        }

        private void textBox2_TextChanged(object sender, EventArgs e) {
            content = txtInput.Text;
        }

        private void comboBox2_SelectedValueChanged(object sender, EventArgs e) {
            fromLanguage = cbTranslateFromLang.SelectedItem.ToString();
        }

        private void comboBox1_SelectedValueChanged(object sender, EventArgs e) {
            toLanguage = cbTranslateToLang.SelectedItem.ToString();
            //if (toLanguage == "Greek")
            //{
            //    useGreek = true;
            //}
            //else
            //    useGreek = false;
        }

        private void button2_Click(object sender, EventArgs e) {
            __PerformTranslation();
        }



        private void __PerformTranslation() {
            InitLanguageMap();

            picLoadingPhoto.Visible = true;
            button2.Enabled = false;
            try {

                SimpleBrowser sb = new SimpleBrowser();
                WebPageUrl page = new WebPageUrl("http://translate.google.com/translate_t?" + GetPostData(languageMap[fromLanguage], languageMap[toLanguage], content));
                page.Headers.Add("ContentType", "application/x-www-form-urlencoded");

                Random r = new Random(DateTime.UtcNow.Millisecond);
                int ndx = r.Next(0, lstr.Count - 1);


                sb.UserAgent = lstr[ndx];
                sb.Navigate(page);
                string kati = getResult(sb.LastPageText);
                txtResults.Text = "";
                txtResults.Text = HttpUtility.HtmlDecode(kati);
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
            finally {
                picLoadingPhoto.Visible = false;
                button2.Enabled = true;
            }

            if (txtResults.Text.Trim().Length == 0)
                txtResults.Text = content;
        }



        private void Form1_Shown(object sender, EventArgs e) {


            if (!string.IsNullOrEmpty(TextToTranslate)) {
                txtInput.Text = content = TextToTranslate;
                __PerformTranslation();
                TextToTranslate = "";
            }
        }

        private void Form1_Load(object sender, EventArgs e) {
            InitLanguageMap();

            txtInput.Text = "";
            txtResults.Text = "";
            cbTranslateToLang.SelectedIndex = 0;
            cbTranslateFromLang.SelectedIndex = 0;
            picLoadingPhoto.Visible = false;

            lstr.Add("Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.57 Safari/537.36");
            lstr.Add("Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.66 Safari/537.36");
            lstr.Add("Opera/9.80 (Windows NT 6.1; WOW64) Presto/2.12.388 Version/12.16");

            cbTranslateToLang.SelectedItem = "Greek";
            if (!string.IsNullOrEmpty(TextToTranslate)) {
                txtInput.Text = TextToTranslate;
            }

            if (!string.IsNullOrEmpty(TextFromCountry)) {
                TextFromCountry = TextFromCountry.ToLower();
                if (languageMapReversed.ContainsKey(TextFromCountry)) {
                    cbTranslateFromLang.SelectedItem = languageMapReversed[TextFromCountry];
                }
                //else { 

                //    if (TextFromCountry == "TR") {
                //        cbTranslateFromLang.SelectedItem = "Turkish";
                //    }
                //    else if (TextFromCountry == "GR") {
                //        cbTranslateFromLang.SelectedItem = "Greek";
                //    }
                //    else if (TextFromCountry == "ES") {
                //        cbTranslateFromLang.SelectedItem = "Spanish";
                //    }
                //}
            }

        }


    }
}

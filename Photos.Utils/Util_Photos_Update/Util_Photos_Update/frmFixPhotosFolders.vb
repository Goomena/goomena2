﻿

Public Class frmFixPhotosFolders

    Dim tmrTaskInterval As Integer

    Private Sub frmSendingEmails_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        GroupControl1.Text = Me.Text
        BarStaticItemStatus.Caption = "Not running"

        StartTimer()
    End Sub


    Private Sub frmSendingEmails_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Try

            bw.CancelAsync()
            bw.Dispose()
            bw = Nothing
            tmrTask.Enabled = False
            Me.Dispose()

        Catch ex As Exception
        End Try
    End Sub


    Private Sub bw_DoWork(sender As System.Object, e As System.ComponentModel.DoWorkEventArgs) Handles bw.DoWork
        Try
            Dim topPath As String = DirectCast(e.Argument, String)
            Dim Directories As String() = System.IO.Directory.GetDirectories(topPath)
            bw.ReportProgress(0, "Starting...")
            bw.ReportProgress(0, "Directories " & Directories.Length)


            Dim r As New Random(System.DateTime.Now.Millisecond)
            Dim continueDate As DateTime = DateTime.UtcNow.AddMinutes(-1)

            For cnt = 0 To Directories.Length - 1
                System.Threading.Thread.Sleep(50)
                If bw.CancellationPending = True Then
                    e.Cancel = True
                    Exit Sub
                End If

                If bw.CancellationPending = True Then
                    e.Cancel = True
                    Exit For
                Else

                    Dim checkDir As String = Directories(cnt)
                    Try


                        Dim Files As String() = System.IO.Directory.GetFiles(checkDir)
                        Dim checkDir_D150 As String = Nothing
                        Dim checkDir_D250 As String = Nothing
                        Dim checkDir_D350 As String = Nothing

                        bw.ReportProgress(-1, checkDir)

                        ' check if last folder is number, 
                        ' to verify that this is a user photo folder
                        Dim drInfo As New System.IO.DirectoryInfo(checkDir)
                        Dim lastFolderName As String = drInfo.Name
                        Dim lastFolderNameInt As Long
                        If (Not Int64.TryParse(lastFolderName, lastFolderNameInt) OrElse lastFolderNameInt < 0) Then
                            Continue For
                        End If

                        For cnt1 = 0 To Files.Length - 1
                            If (checkDir_D150 Is Nothing) Then
                                checkDir_D150 = checkDir & "/d150"
                                checkDir_D250 = checkDir & "/thumbs"
                                checkDir_D350 = checkDir & "/d350"

                                If Not System.IO.Directory.Exists(checkDir_D150) Then System.IO.Directory.CreateDirectory(checkDir_D150)
                                If Not System.IO.Directory.Exists(checkDir_D250) Then System.IO.Directory.CreateDirectory(checkDir_D250)
                                If Not System.IO.Directory.Exists(checkDir_D350) Then System.IO.Directory.CreateDirectory(checkDir_D350)

                            End If

                        
                            Dim fn As String = System.IO.Path.GetFileName(Files(cnt1))

                            bw.ReportProgress(-1, Files(cnt1))

                            Dim photoPath As String = checkDir & "\" & fn
                            Dim D250Path As String = checkDir_D250 & "\" & fn
                            Dim D150Path As String = checkDir_D150 & "\" & fn
                            Dim D350Path As String = checkDir_D350 & "\" & fn

                            Dim original As Image = Nothing

                            If Not System.IO.File.Exists(D250Path) Then
                                Try
                                    original = Image.FromFile(photoPath)
                                    Using thumbnail2 As Image = PhotoUtils.AutosizeImageAndInscribe(original, 250, 250)
                                        PhotoUtils.SaveToJpeg(thumbnail2, D250Path)
                                        thumbnail2.Dispose()

                                        bw.ReportProgress(0, "OK - " & D250Path)

                                    End Using
                                    original.Dispose()
                                Catch ex As Exception
                                    bw.ReportProgress(0, " Exc. " & ex.Message & " - Path -> " & D250Path)
                                Finally
                                End Try
                            End If
                            If (original IsNot Nothing) Then
                                original.Dispose()
                            End If


                            If Not System.IO.File.Exists(D150Path) Then
                                Try
                                    original = Image.FromFile(photoPath)
                                    Using thumbnail150_2 As Image = PhotoUtils.AutosizeImageAndInscribe(original, 150, 150)
                                        PhotoUtils.SaveToJpeg(thumbnail150_2, D150Path)
                                        thumbnail150_2.Dispose()

                                        bw.ReportProgress(0, "OK - " & D150Path)

                                    End Using
                                Catch ex As Exception
                                    bw.ReportProgress(0, " Exc. " & ex.Message & " - Path -> " & D150Path)
                                Finally
                                End Try
                            End If
                            If (original IsNot Nothing) Then
                                original.Dispose()
                            End If

                            If Not System.IO.File.Exists(D350Path) Then
                                Try
                                    original = Image.FromFile(photoPath)
                                    Using thumbnail350_2 As Image = PhotoUtils.ResizeBitmap(original, 350, 350)
                                        PhotoUtils.SaveToJpeg(thumbnail350_2, D350Path)
                                        thumbnail350_2.Dispose()

                                        bw.ReportProgress(0, "OK - " & D350Path)

                                    End Using
                                Catch ex As Exception
                                    bw.ReportProgress(0, " Exc. " & ex.Message & " - Path -> " & D350Path)
                                Finally
                                End Try
                            End If
                            If (original IsNot Nothing) Then
                                original.Dispose()
                            End If

                        Next


                    Catch ex As Exception
                        bw.ReportProgress(0, " Exc. " & ex.Message & " - Path -> " & checkDir)
                    End Try
                End If


            Next

        Catch ex As Exception
            bw.ReportProgress(0, ex.Message)
        Finally

        End Try
    End Sub

    Private Sub AddMessage(message As String)
        'txOutput.Text = "[" & Date.Now.ToLocalTime() & "] " & message & vbCrLf & txOutput.Text
        If (message IsNot Nothing) Then
            txtOutput.MaskBox.AppendText("[" & Date.Now.ToLocalTime() & "] " & message & vbCrLf)
        End If
    End Sub

    Private Sub bw_RunWorkerCompleted(sender As System.Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bw.RunWorkerCompleted
        If e.Cancelled = True Then
            Me.BarStaticItemStatus.Caption = "Canceled"
        ElseIf e.Error IsNot Nothing Then
            Me.BarStaticItemStatus.Caption = "Error: " & e.Error.Message
        Else
            Me.BarStaticItemStatus.Caption = "Not running"
        End If
    End Sub



    Private Sub BarButtonItemClear_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemClear.ItemClick
        txtOutput.Text = ""
    End Sub

    Private Sub BarButtonItemRun_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemRun.ItemClick
        Try
            ' tmrTask.Enabled = False

            If Not bw.IsBusy = True Then
                If (Not String.IsNullOrEmpty(txPath.Text) AndAlso System.IO.Directory.Exists(txPath.Text)) Then
                    BarStaticItemStatus.Caption = "Running"
                    bw.RunWorkerAsync(txPath.Text)
                    'BarButtonItemRun.Enabled = False
                Else
                    AddMessage("empty path")
                End If
            Else
                Try
                    If (Not String.IsNullOrEmpty(txPath.Text) AndAlso System.IO.Directory.Exists(txPath.Text)) Then
                        bw.CancelAsync()
                        bw.RunWorkerAsync(txPath.Text)
                        BarStaticItemStatus.Caption = "Running"
                    Else
                        AddMessage("empty path")
                    End If
                Catch ex As Exception
                    AddMessage(Me.Name & " -> BarButtonItemRun_ItemClick -> bw.IsBusy" & vbCrLf & ex.Message)
                End Try
            End If

        Catch ex As Exception
            AddMessage(Me.Name & " -> BarButtonItemRun_ItemClick" & vbCrLf & ex.Message)
        Finally
            ' tmrTask.Enabled = True
            ' tmrTask.Interval = tmrTaskInterval
        End Try
    End Sub


    Private Sub BarButtonItemCancelJob_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemCancelJob.ItemClick
        bw.CancelAsync()
        tmrTask.Enabled = False
        BarButtonItemRun.Enabled = True
    End Sub

    Private Sub bw_ProgressChanged(sender As System.Object, e As System.ComponentModel.ProgressChangedEventArgs) Handles bw.ProgressChanged
        If (e.ProgressPercentage = -1) Then
            Me.BarStaticItemStatus.Caption = Mid(CStr(e.UserState), 1, 50) & "..."
        Else
            AddMessage(CStr(e.UserState))
        End If
    End Sub



    Private Sub tmrTask_Tick(sender As System.Object, e As System.EventArgs) Handles tmrTask.Tick
        If Not bw.IsBusy = True Then
            'If (Not String.IsNullOrEmpty(txPath.Text) AndAlso System.IO.Directory.Exists(txPath.Text)) Then
            '    BarStaticItemStatus.Caption = "Running"
            '    bw.RunWorkerAsync(txPath.Text)
            '    'BarButtonItemRun.Enabled = False
            'Else
            '    AddMessage("empty path")
            'End If
        End If
        'tmrTask.Interval = tmrTaskInterval
    End Sub


    Private Sub StartTimer()
        'Dim r As New Random(System.DateTime.Now.Millisecond)
        'Dim interval As Integer = r.Next(30, 60)

        'If (tmrTaskInterval = 0) Then tmrTaskInterval = tmrTask.Interval
        'tmrTask.Interval = interval * 1000
        'tmrTask.Enabled = True

        'tmrTask.Enabled = True
        'tmrTask.Start()

        'BarStaticItemStatus.Caption = "First run in " & tmrTask.Interval / 1000 & " sec"
    End Sub


End Class
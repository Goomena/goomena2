﻿Imports Dating.Server.Core.DLL
Imports System.Xml
Imports System.Globalization
Imports System.Text
Imports System.Linq

Public Class frmCheckPhotosWithGoogle

    Dim tmrTaskInterval As Integer
    Dim isBusy_PerformImageComparison As Boolean

    Private Sub frmTasks_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Dim sqlConn As New SqlClient.SqlConnection(My.Settings.AppDBconnectionString)
        'Me.Text = m_SiteURL & " Goomena CPanel Administrator - Ver: " & My.Application.Info.Version.Major & "." & My.Application.Info.Version.Minor & " WS:" & gAdminWS.Url & " - APP:" & RegistryAPP & " Site Name:" & m_SiteName
        Me.Text = " Goomena Photos Checker App - Ver: " & _
            My.Application.Info.Version.ToString() & _
            " Connecting to SQL Server : " & sqlConn.DataSource & " Database=" & sqlConn.Database

        Dating.Server.Core.DLL.DataHelpers.SetConnectionString(My.Settings.AppDBconnectionString)

        GroupControl1.Text = Me.Text
        BarStaticItemStatus.Caption = "Not running"
        StartTimer()
    End Sub


    Private Sub frmTasks_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Try

            bw.CancelAsync()
            bw.Dispose()
            bw = Nothing
            tmrTask.Enabled = False
            Me.Dispose()

        Catch ex As Exception
        End Try
    End Sub


    Private Sub bw_DoWork(sender As System.Object, e As System.ComponentModel.DoWorkEventArgs) Handles bw.DoWork
        'Dim hdl As New Core.DLL.clsSendingEmailsHandler()
        Try
            'Dim emailForSending As Core.DLL.SYS_EmailsQueue = hdl.GetNextEmailToSend()

            'Dim emailCount As Integer
            Dim r As New Random(System.DateTime.Now.Millisecond)
            Dim continueDate As DateTime = DateTime.UtcNow.AddMinutes(-1)

            While True
                While (continueDate > DateTime.UtcNow)
                    System.Threading.Thread.Sleep(200)
                    If bw.CancellationPending = True Then
                        e.Cancel = True
                        Exit Sub
                    End If
                End While

                If bw.CancellationPending = True Then
                    e.Cancel = True
                    Exit Sub
                End If



                If (Not isBusy_PerformImageComparison) Then
                    Try
                        PerformImageComparison()
                    Catch ex As Exception
                        bw.ReportProgress(0, ex.Message)
                    End Try
                End If




                If (isBusy_PerformImageComparison) Then
                    Dim interval As Integer = r.Next(60, 120)
                    ' bw.ReportProgress(0, " Waiting " & interval & " seconds")
                    continueDate = DateTime.UtcNow.AddMilliseconds(interval * 1000)
                End If

            End While

        Catch ex As Exception
            bw.ReportProgress(0, ex.Message)
        Finally
            'hdl.Dispose()
        End Try
    End Sub

    Private Sub AddMessage(message As String)
        If (message IsNot Nothing) Then
            txtOutput.MaskBox.AppendText("[" & Date.Now.ToLocalTime().ToString("dd/MM/yyyy HH:mm:ss") & "] " & message & vbCrLf)
        End If
    End Sub

    Private Sub bw_RunWorkerCompleted(sender As System.Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bw.RunWorkerCompleted
        If e.Cancelled = True Then

            Me.BarStaticItemStatus.Caption = "Canceled"
            tmrTask.Enabled = False

        ElseIf e.Error IsNot Nothing Then
            Me.BarStaticItemStatus.Caption = "Error: " & e.Error.Message
        Else
            Me.BarStaticItemStatus.Caption = "Not running"
        End If
    End Sub



    Private Sub BarButtonItemClear_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemClear.ItemClick
        txtOutput.Text = ""
    End Sub

    Private Sub BarButtonItemRun_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemRun.ItemClick
        Try
            tmrTask.Enabled = False

            If Not bw.IsBusy = True Then
                BarStaticItemStatus.Caption = "Running"
                bw.RunWorkerAsync()
                BarButtonItemRun.Enabled = False
            Else
                Try
                    bw.CancelAsync()
                    bw.RunWorkerAsync()
                    BarStaticItemStatus.Caption = "Running"
                Catch ex As Exception
                    AddMessage(Me.Name & " -> BarButtonItemRun_ItemClick -> bw.IsBusy" & vbCrLf & ex.Message)
                End Try
            End If

        Catch ex As Exception
            AddMessage(Me.Name & " -> BarButtonItemRun_ItemClick" & vbCrLf & ex.Message)
        Finally
            tmrTask.Enabled = True
            tmrTask.Interval = tmrTaskInterval
        End Try
    End Sub


    Private Sub BarButtonItemCancelJob_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemCancelJob.ItemClick
        bw.CancelAsync()
        tmrTask.Enabled = False
        BarButtonItemRun.Enabled = True
    End Sub

    Private Sub bw_ProgressChanged(sender As System.Object, e As System.ComponentModel.ProgressChangedEventArgs) Handles bw.ProgressChanged
        If (e.ProgressPercentage = -1) Then
            Me.BarStaticItemStatus.Caption = Mid(CStr(e.UserState), 1, 200) & "..."
        Else
            AddMessage(CStr(e.UserState))
        End If
    End Sub



    Private Sub tmrTask_Tick(sender As System.Object, e As System.EventArgs) Handles tmrTask.Tick
        If Not bw.IsBusy = True Then
            BarStaticItemStatus.Caption = "Running"
            bw.RunWorkerAsync()
            BarButtonItemRun.Enabled = False
        End If
        tmrTask.Interval = tmrTaskInterval
    End Sub


    Private Sub StartTimer()
        Dim r As New Random(System.DateTime.Now.Millisecond)
        Dim interval As Integer = r.Next(30, 60)

        If (tmrTaskInterval = 0) Then tmrTaskInterval = tmrTask.Interval
        tmrTask.Interval = interval * 1000
        tmrTask.Enabled = True

        tmrTask.Enabled = True
        tmrTask.Start()

        BarStaticItemStatus.Caption = "First run in " & tmrTask.Interval / 1000 & " sec"
    End Sub


#Region "PHOTOS COMPARER using Google API"



    Private Sub PerformImageComparison_ProgressChanged(sender As Object, e As GoomenaImageCompare.ProgressChangedEventArgs)
        bw.ReportProgress(0, "Image Comparison-->" & e.CurrentUrl)
        bw.ReportProgress(-1, "Image Comparison-->" & e.CurrentUrl)
    End Sub


    Private Sub PerformImageComparison_Complete(sender As Object, e As GoomenaImageCompare.AlertEventArgs)
        bw.ReportProgress(0, "Image Comparison-->Complete")
        bw.ReportProgress(-1, "Image Comparison-->Complete")

        If (customerIDList.Count = 0) Then Return
        Dim command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand()

        Try

            If (e.uuiListData.Count > 0) Then
                Dim gToEmail As String = My.Settings.gToEmail
                clsMyMail.MySMTPSettings.gSMTPOutPort = My.Settings.gSMTPOutPort
                clsMyMail.MySMTPSettings.gSMTPServerName = My.Settings.gSMTPServerName
                clsMyMail.MySMTPSettings.gSMTPLoginName = My.Settings.gSMTPLoginName
                clsMyMail.MySMTPSettings.gSMTPPassword = My.Settings.gSMTPPassword


                Dim uniqueInputPhotos As New List(Of String)
                For cnt = 0 To e.uuiListData.Count - 1
                    Dim itm As GoomenaImageCompare.uuiSimilarUrlItem = e.uuiListData(cnt)
                    If (Not uniqueInputPhotos.Contains(itm.InputUrl)) Then
                        uniqueInputPhotos.Add(itm.InputUrl)
                    End If
                Next


                For cnt = 0 To uniqueInputPhotos.Count - 1

                    Dim urlPhoto As String = uniqueInputPhotos(cnt)
                    If (Not customerIDList.ContainsKey(urlPhoto)) Then Continue For
                    Dim dr As System.Data.DataRow = customerIDList(urlPhoto)
                    Dim loginName As String = DataHelpers.GetEUS_Profile_LoginName_ByProfileID(dr("CustomerID"))

                    Try


                        Dim toEmail As String = gToEmail
                        Dim Subject As String = "Users Found to have the same cookie"
                        Dim Content As String = <h><![CDATA[
Found similar photos with photo on Goomena.com<br/>
<br/>
<div><a href="[GOOMENA-PHOTO]" target="_blank">[LOGINNAME]</a></div>
<div><img src="[GOOMENA-PHOTO]"  style="width:250px" width="250" /></div>
<br/> 
<br/>
<h3 style="margin-bottom:7px;">Similar photo(s) found </h3>

]]></h>

                        Dim Content_Similar As String = <h><![CDATA[
<div>Similar [SIMILARITY]%</div>
<div><img src="[PHOTO-URL]"  style="width:250px" width="250"/></div>
<div><a href="[PHOTO-URL]" target="_blank">[PHOTO-URL]</a></div>
<br/> 
<br/>
]]></h>

                        Content = Content.Replace("[GOOMENA-PHOTO]", urlPhoto)
                        Content = Content.Replace("[LOGINNAME]", loginName)

                        Dim htmlTable As New StringBuilder()
                        Dim prevLogin As String = ""

                        Dim itemsSimilar As List(Of GoomenaImageCompare.uuiSimilarUrlItem) = (From f In e.uuiListData Order By f.Similarity Descending).ToList()
                        For f1 = 0 To itemsSimilar.Count - 1
                            Dim itm As GoomenaImageCompare.uuiSimilarUrlItem = itemsSimilar(f1)
                            If (itm.InputUrl = urlPhoto) Then
                                Dim simPhoto As String = Content_Similar.Replace("[PHOTO-URL]", itm.FoundUrl)
                                simPhoto = simPhoto.Replace("[SIMILARITY]", Math.Round(itm.Similarity * 100, 2))
                                htmlTable.AppendLine(simPhoto)
                            End If
                        Next

                        htmlTable.Insert(0, "<html><head>" & "</head><body>")
                        htmlTable.AppendLine("</body></html>")
                        Content = Content & htmlTable.ToString()

                        bw.ReportProgress(0, "Image Comparison-->Sending message for " & loginName)
                        clsMyMail.SendMail2(My.Settings.gToEmail, My.Settings.gToEmail, "Found similar images", Content, True)
                    Catch
                    End Try
                Next



                Dim _CMSDBDataContext As New CMSDBDataContext(command.Connection.ConnectionString)
                Try

                    For cnt = 0 To e.uuiListData.Count - 1
                        Dim itm As GoomenaImageCompare.uuiSimilarUrlItem = e.uuiListData(cnt)
                        If (Not customerIDList.ContainsKey(itm.InputUrl)) Then Continue For
                        Dim dr As System.Data.DataRow = customerIDList(itm.InputUrl)

                        Dim ps As New EUS_CustomerPhotosSimilar()
                        ps.CustomerID = dr("CustomerID")
                        ps.CustomerPhotosID = dr("CustomerPhotosID")
                        ps.DateTimeCreated = Date.UtcNow
                        ps.PhotoSimilarity = itm.Similarity
                        ps.PhotoURL = itm.FoundUrl
                        _CMSDBDataContext.EUS_CustomerPhotosSimilars.InsertOnSubmit(ps)

                    Next

                    _CMSDBDataContext.SubmitChanges()
                Catch ex As Exception
                    bw.ReportProgress(0, "Image Comparison-->" & ex.ToString())
                    Throw New System.Exception(ex.Message, ex)
                Finally
                    _CMSDBDataContext.Dispose()
                End Try
                bw.ReportProgress(0, "Image Comparison-->EUS_CustomerPhotosSimilars updated")
            End If


            Dim sb As New System.Text.StringBuilder()
            For cnt = 0 To e.uuiListData.Count - 1
                Dim itm As GoomenaImageCompare.uuiSimilarUrlItem = e.uuiListData(cnt)

                If (customerIDList.ContainsKey(itm.InputUrl)) Then
                    Dim dr As System.Data.DataRow = customerIDList(itm.InputUrl)
                    Dim CustomerPhotosID As Integer = dr("CustomerPhotosID")

                    Dim sql As String = "update EUS_CustomerPhotos set PhotoSimilarity=@PhotoSimilarity[COUNT] where CustomerPhotosID=@CustomerPhotosID[COUNT];"
                    sql = sql.Replace("[COUNT]", cnt)
                    sb.AppendLine(sql)
                    command.Parameters.AddWithValue("@PhotoSimilarity" & cnt, 1)
                    command.Parameters.AddWithValue("@CustomerPhotosID" & cnt, CustomerPhotosID)
                    customerIDList.Remove(itm.InputUrl)
                End If
            Next


            Dim keys As String() = New String(customerIDList.Keys.Count - 1) {}
            customerIDList.Keys.CopyTo(keys, 0)
            For cnt = 0 To keys.Length - 1
                Dim dr As System.Data.DataRow = customerIDList(keys(cnt))
                Dim CustomerPhotosID As Integer = dr("CustomerPhotosID")

                Dim index As Integer = e.uuiListData.Count + cnt + 10
                Dim sql As String = "update EUS_CustomerPhotos set PhotoSimilarity=@PhotoSimilarity[COUNT] where CustomerPhotosID=@CustomerPhotosID[COUNT];"
                sql = sql.Replace("[COUNT]", index)
                sb.AppendLine(sql)
                command.Parameters.AddWithValue("@PhotoSimilarity" & index, 0)
                command.Parameters.AddWithValue("@CustomerPhotosID" & index, CustomerPhotosID)
            Next

            command.CommandText = sb.ToString()
            DataHelpers.ExecuteNonQuery(command)
            bw.ReportProgress(0, "Image Comparison-->Database updated")

        Catch ex As Exception
            bw.ReportProgress(0, "Image Comparison-->" & ex.ToString())
            Throw New System.Exception(ex.Message, ex)
        Finally
            isBusy_PerformImageComparison = False
        End Try

    End Sub



    Private customerIDList As Dictionary(Of String, System.Data.DataRow)
    Private Sub PerformImageComparison()
        Try
            Dim DirectoryName As String = System.IO.Path.GetDirectoryName(Application.ExecutablePath)
            Dim logFile As String = System.IO.Path.Combine(DirectoryName, "ImageComparison.log")
            If (customerIDList Is Nothing) Then bw.ReportProgress(0, "Image Comparison-->log file " & vbTab & logFile)
            customerIDList = New Dictionary(Of String, System.Data.DataRow)

            Try
                System.IO.File.Delete(logFile)
            Catch
            End Try

            Dim sql As String = "EXEC [GetView_EUS_CustomerPhotos_ImageComparison] @recordsToSelect=10;"
            Dim dt As DataTable = DataHelpers.GetDataTable(sql)
            Dim urlList As New List(Of String)

            bw.ReportProgress(0, "Image Comparison-->photos count:" & dt.Rows.Count)
            For cnt = 0 To dt.Rows.Count - 1
                Dim fn As String = dt.Rows(cnt)("FileName")
                Dim customerid As Integer = dt.Rows(cnt)("CustomerID")
                Dim CustomerPhotosID As Long = dt.Rows(cnt)("CustomerPhotosID")
                Dim path As String = ProfileHelper.GetProfileImageURL(customerid, fn, 0, False, False)

                urlList.Add(path)
                customerIDList.Add(path, dt.Rows(cnt))
            Next

            bw.ReportProgress(0, "Image Comparison-->starting search...")

            Dim sd As New GoomenaImageCompare.ImageComparison(10000, urlList, logFile)
            AddHandler sd.CallAlert, AddressOf PerformImageComparison_Complete
            AddHandler sd.ProgressChanged, AddressOf PerformImageComparison_ProgressChanged
            sd.getResults()

            isBusy_PerformImageComparison = True

        Catch ex As Exception
            Throw New System.Exception(ex.Message, ex)
        End Try
    End Sub


#End Region


End Class
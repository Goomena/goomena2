USE [Goomena WaterMark]
GO
/****** Object:  Table [dbo].[WatermarkConfiguration]    Script Date: 11/13/2013 16:29:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WatermarkConfiguration](
	[width] [int] NOT NULL,
	[height] [int] NOT NULL,
	[position] [float] NULL,
	[resize] [float] NULL,
	[shifting] [int] NULL,
	[letter_pos] [int] NULL,
	[letter_size] [int] NULL,
 CONSTRAINT [PK_WatermarkConfiguration] PRIMARY KEY CLUSTERED 
(
	[width] ASC,
	[height] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace watermarktest3
{
    public partial class Form1 : Form
    {

        string connString = Properties.Settings.Default.Goomena_WaterMarkConnectionString;
        string pathToSave = "";
        double position = .77;
        string imageName = "";
        int letter_size = 14;
        int letter_pos = 120;
        double resize = 1;
        Image tempIm;
        string file = "";
        float opacity = 0.5f;
        int pos = 300;


        public Form1()
        {
            InitializeComponent();


        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (pictureBox1.Image == null)
                return;

            // pernoume tis suntetagmenes tis eikonas
            int width = pictureBox1.Image.Width;
            int height = pictureBox1.Image.Height;
            string goomenaLink = txtGoomenaLink.Text;
            int timesFit = (width / 318) + 1;
            Bitmap strip = new Bitmap(318 * timesFit, 52);

            //string WorkingDirectory = "";

            //define a string of text to use as the Copyright message
            //string Copyright = "SKATAasdmd4m3f";

            //create a image object containing the photograph to watermark
            Image imgPhoto = Image.FromFile("logos.jpg");
            int phWidth = imgPhoto.Width;
            int phHeight = imgPhoto.Height;

            //create a Bitmap the Size of the original photograph
            //Bitmap bmPhoto = new Bitmap(phWidth, phHeight, PixelFormat.Format24bppRgb);


            Bitmap myBitmap = new Bitmap(pictureBox3.Image);
            Graphics g = Graphics.FromImage(myBitmap);
            SolidBrush transBrush = new SolidBrush(Color.FromArgb(113, 170, 170, 170));
            //g.DrawString("Goomena   Goomena   Goomena   Goomena   Goomena   Goomena   Goomena   Goomena   ", new Font("Tahoma", 16), transBrush, new PointF(10, 5));
            //TextRenderer.DrawText(Graphics.FromImage(myBitmap), "G♀♂mena", new Font("Tahoma", 16), new Point(100, 100), Color.Black);
            g.DrawString(goomenaLink + "     " + goomenaLink + "     " + goomenaLink + "     " + goomenaLink + "     ", new Font("Tahoma", 8), transBrush, new PointF(10, 85));
            pictureBox3.Image.Dispose();
            pictureBox3.Image = myBitmap;

            Image im = SetImageOpacity(myBitmap, 0.5f);
            im.Save("asdasd.png", ImageFormat.Png);

            Bitmap bit = new Bitmap(im);

            using (Graphics grfx = Graphics.FromImage(pictureBox1.Image))
            {
                grfx.DrawImage(bit, 10, 100);
            }

            pictureBox1.Image.Dispose();
            pictureBox1.Image = bit;

            Image rotim = RotateImage(im, new PointF(0, 0), -3f);
            rotim.Save("rotim.png", ImageFormat.Png);
        }

        public Image SetImageOpacity(Bitmap image, float opacity)
        {
            try
            {
                //create a Bitmap the size of the image provided  
                Bitmap bmp = new Bitmap(image.Width, image.Height);

                //create a graphics object from the image  
                using (Graphics gfx = Graphics.FromImage(bmp))
                {

                    //create a color matrix object  
                    ColorMatrix matrix = new ColorMatrix();

                    //set the opacity  
                    matrix.Matrix33 = opacity;

                    //create image attributes  
                    ImageAttributes attributes = new ImageAttributes();

                    //set the color(opacity) of the image  
                    attributes.SetColorMatrix(matrix, ColorMatrixFlag.Default, ColorAdjustType.Bitmap);

                    //now draw the image  
                    gfx.DrawImage(image, new Rectangle(0, 0, bmp.Width, bmp.Height), 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, attributes);
                }
                return bmp;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }
        }

        private void writeWaterMark(Graphics graphicsHandle, int x, int y, Font ourFont, String text)
        {
            StringFormat strFormatter = new StringFormat();
            SolidBrush transBrush = new SolidBrush(Color.FromArgb(113, 255, 255, 255));
            // Drawstring the transparent text to the Graphics context at x,y position.
            graphicsHandle.DrawString(text,
                 ourFont,
                 transBrush,
                 new PointF(x, y),
                 strFormatter);
        }

        public Bitmap RotateImage(Image image, PointF offset, float angle)
        {
            if (image == null)
                throw new ArgumentNullException("image");

            //create a new empty bitmap to hold rotated image
            Bitmap rotatedBmp = new Bitmap(image.Width + 50, image.Height + 100);
            rotatedBmp.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            //make a graphics object from the empty bitmap
            Graphics g = Graphics.FromImage(rotatedBmp);

            //Put the rotation point in the center of the image
            g.TranslateTransform(offset.X, offset.Y);

            //rotate the image
            g.RotateTransform(angle);

            //move the image back
            g.TranslateTransform(-offset.X, -offset.Y);

            //draw passed in image onto graphics object
            g.DrawImage(image, new PointF(0, 0));

            return rotatedBmp;
        }

        public Image RotateImage(Image img, float rotationAngle)
        {
            //create an empty Bitmap image
            Bitmap bmp = new Bitmap(img.Width, img.Height);

            //turn the Bitmap into a Graphics object
            Graphics gfx = Graphics.FromImage(bmp);

            //now we set the rotation point to the center of our image
            gfx.TranslateTransform((float)bmp.Width / 2, (float)bmp.Height / 2);

            //now rotate the image
            gfx.RotateTransform(rotationAngle);

            gfx.TranslateTransform(-(float)bmp.Width / 2, -(float)bmp.Height / 2);

            //set the InterpolationMode to HighQualityBicubic so to ensure a high
            //quality image once it is transformed to the specified size
            gfx.InterpolationMode = InterpolationMode.HighQualityBicubic;

            //now draw our new image onto the graphics object
            gfx.DrawImage(img, new Point(0, 0));

            //dispose of our Graphics object
            gfx.Dispose();

            //return the image
            return bmp;
        }

        private void pictureBox2_Paint(object sender, PaintEventArgs e)
        {
            // writeWaterMark(e.Graphics, 100, 100, new Font("Arial", 14, FontStyle.Bold), "Copyright Dream.In.Code.net");
        }

        public void setWatermark(Bitmap srcImage, string pathtosave)
        {
            pictureBox1.Image = srcImage;
            button2.PerformClick();
        }

        private void button2_Click(object sender, EventArgs e)
        {

            if (pictureBox1.Image == null)
                return;

            string goomenaLink = txtGoomenaLink.Text;

            if (pictureBox1.Image.Height < pictureBox1.Image.Width)
            {
                // prwta kanoume ena resize gia na tairiazei stis analogies tis eikonas
                //double percentage = 0.15;
                //int logoImageheight = (int)(pictureBox1.Image.Height * percentage);
                //int logoImageWidth = (logoImageheight * 318) / 156;
                //pictureBox3.Image = new Bitmap(pictureBox3.Image, new Size(logoImageWidth, logoImageheight));
                if (pictureBox1.Image.Height <= 305)
                {
                    pictureBox3.Image = pictureBox6.Image;
                }

                Bitmap logoImage = new Bitmap(2500, 156);
                Image image = pictureBox3.Image;
                Graphics gp = Graphics.FromImage(logoImage);
                // kanoume repeat tin eikona mexri na gemisei 2500 pixels
                for (int x = 0; x <= logoImage.Width - image.Width; x += image.Width)
                {
                    gp.DrawImage(image, new Point(x, 0));
                }


                // meta grafoume to link katw apo to logotupo
                Graphics g = Graphics.FromImage(logoImage);
                SolidBrush transBrush = new SolidBrush(Color.FromArgb(180, 150, 150, 150));

                if (pictureBox1.Image.Height <= 305)
                {
                    g.DrawString(goomenaLink + "     " + goomenaLink + "     " + goomenaLink + "     " + goomenaLink + "     " + goomenaLink + "     " + goomenaLink + "     " + goomenaLink, new Font("Tahoma", 4), transBrush, new PointF(15, 32));
                }
                else
                {
                    g.DrawString(goomenaLink + "     " + goomenaLink + "     " + goomenaLink + "     " + goomenaLink + "     " + goomenaLink + "     " + goomenaLink + "     " + goomenaLink, new Font("Tahoma", 10), transBrush, new PointF(25, 90));
                }
                // kanoume panw k katw apo ta grammata ta panta full transparent

                g.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceCopy;

                if (pictureBox1.Image.Height <= 305)
                {
                    using (var br = new SolidBrush(Color.FromArgb(0, 255, 255, 255)))
                    {
                        g.FillRectangle(br, new Rectangle(0, 0, 1920, 20));
                        g.FillRectangle(br, new Rectangle(0, 40, 1920, 60));
                    }
                }
                else
                {
                    using (var br = new SolidBrush(Color.FromArgb(0, 255, 255, 255)))
                    {
                        g.FillRectangle(br, new Rectangle(0, 0, 1920, 55));
                        g.FillRectangle(br, new Rectangle(0, 110, 1920, 156));
                    }
                }


                // twra tin kanoume rotate
                Image rotim = RotateImage(logoImage, new PointF(0, 0), 1f);


                // tin kanoume transparent
                Image im = SetImageOpacity(new Bitmap(rotim), 0.5f);



                // prepei na tin kanoume crop..
                int y = -1;
                if (pictureBox1.Image.Width > 700 && pictureBox1.Image.Width < 800 && pictureBox1.Image.Height > 300 && pictureBox1.Image.Height < 350)
                {
                    y = (int)(pictureBox1.Image.Height * 0.70);
                }
                else if (pictureBox1.Image.Width > 2500 && pictureBox1.Image.Width < 2600 && pictureBox1.Image.Height > 1800 && pictureBox1.Image.Height < 2000)
                {
                    y = (int)(pictureBox1.Image.Height * 0.80);
                }
                else if (pictureBox1.Image.Width > 620 && pictureBox1.Image.Width < 650 && pictureBox1.Image.Height > 340 && pictureBox1.Image.Height < 370)
                {
                    y = (int)(pictureBox1.Image.Height * 0.70);
                }
                else if (pictureBox1.Image.Width > 2200 && pictureBox1.Image.Width < 2260 && pictureBox1.Image.Height > 1900 && pictureBox1.Image.Height < 2000)
                {
                    y = (int)(pictureBox1.Image.Height * 0.81);
                }
                else if (pictureBox1.Image.Width > 850 && pictureBox1.Image.Width < 900 && pictureBox1.Image.Height > 300 && pictureBox1.Image.Height < 350)
                {
                    y = (int)(pictureBox1.Image.Height * 0.70);
                }
                else if (pictureBox1.Image.Width > 390 && pictureBox1.Image.Width < 410 && pictureBox1.Image.Height > 300 && pictureBox1.Image.Height < 310)
                {
                    y = (int)(pictureBox1.Image.Height * 0.82);
                }
                else if (pictureBox1.Image.Height > 1000)
                {
                    y = (int)(pictureBox1.Image.Height * 0.87);
                }
                else if (pictureBox1.Image.Height > 700)
                {
                    y = (int)(pictureBox1.Image.Height * 0.85);
                }
                else if (pictureBox1.Image.Height > 500)
                {
                    y = (int)(pictureBox1.Image.Height * 0.80);
                }
                else if (pictureBox1.Image.Height > 300)
                {
                    y = (int)(pictureBox1.Image.Height * 0.77);
                }
                else
                {
                    y = (int)(pictureBox1.Image.Height * 0.77);
                }

                using (Graphics grfx = Graphics.FromImage(pictureBox1.Image))
                {
                    grfx.DrawImage(im, -10, (int)(pictureBox1.Image.Height * 0.77));
                }
                Image immm = pictureBox1.Image;
                pictureBox1.Image = immm;
                if (String.IsNullOrWhiteSpace(pathToSave))
                    immm.Save("foo.jpg", ImageFormat.Jpeg);
                else
                    immm.Save(pathToSave + "\\foo.jpg", ImageFormat.Jpeg);
            }
            else
            { // an to upsos einai megalitero apo to platos

                Image im = pictureBox5.Image;

                Bitmap logoImage = new Bitmap(2500, 156);
                Image image = pictureBox3.Image;
                Graphics gp = Graphics.FromImage(logoImage);
                // kanoume repeat tin eikona mexri na gemisei 1920 pixels
                for (int x = 0; x <= logoImage.Width - image.Width; x += image.Width)
                {
                    gp.DrawImage(image, new Point(x, 0));
                }
                // meta grafoume to link katw apo to logotupo
                Graphics g = Graphics.FromImage(logoImage);
                SolidBrush transBrush = new SolidBrush(Color.FromArgb(180, 150, 150, 150));

                g.DrawString(goomenaLink + "     " + goomenaLink + "     " + goomenaLink + "     " + goomenaLink + "     " + goomenaLink + "     " + goomenaLink + "     " + goomenaLink + "     " + goomenaLink + "     " + goomenaLink, new Font("Tahoma", 10), transBrush, new PointF(25, 90));
                g.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceCopy;
                using (var br = new SolidBrush(Color.FromArgb(0, 255, 255, 255)))
                {
                    g.FillRectangle(br, new Rectangle(0, 0, 2500, 55));
                    g.FillRectangle(br, new Rectangle(0, 110, 2500, 156));
                }
                // tis vazoume transparent deksia kai aristera apo ta grammaa

                Bitmap bitmapPicturebox1 = new Bitmap(logoImage);
                bitmapPicturebox1.RotateFlip(RotateFlipType.Rotate270FlipNone);

                // tin kanoume transparent
                Image im2 = SetImageOpacity(new Bitmap(bitmapPicturebox1), 0.5f);

                Image rotim = RotateImage(im2, new PointF(0, 0), -2f);

                using (Graphics grfx = Graphics.FromImage(pictureBox1.Image))
                {
                    grfx.DrawImage(rotim, -20, -20);
                }
            }
        }

        private Image ResizeImage(Image image, Size size, bool preserveAspectRatio = true)
        {
            int newWidth;
            int newHeight;
            if (preserveAspectRatio)
            {
                int originalWidth = image.Width;
                int originalHeight = image.Height;
                float percentWidth = (float)size.Width / (float)originalWidth;
                float percentHeight = (float)size.Height / (float)originalHeight;
                float percent = percentHeight < percentWidth ? percentHeight : percentWidth;
                newWidth = (int)(originalWidth * percent);
                newHeight = (int)(originalHeight * percent);
            }
            else
            {
                newWidth = size.Width;
                newHeight = size.Height;
            }
            Image newImage = new Bitmap(newWidth, newHeight);
            using (Graphics graphicsHandle = Graphics.FromImage(newImage))
            {
                graphicsHandle.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphicsHandle.DrawImage(image, 0, 0, newWidth, newHeight);
            }
            return newImage;
        }

        void DrawDigonalString(Graphics G, string S, Font F, Brush B, PointF P, int Angle)
        {
            SizeF MySize = G.MeasureString(S, F);
            G.TranslateTransform(P.X + MySize.Width / 2, P.Y + MySize.Height / 2);
            G.RotateTransform(Angle);
            G.DrawString(S, F, B, new PointF(-MySize.Width / 2, -MySize.Height / 2));
            G.RotateTransform(-Angle);
            G.TranslateTransform(-P.X - MySize.Width / 2, -P.Y - MySize.Height / 2);
        }

        private void btnOpenImage_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult result = openFileDialog1.ShowDialog(); // Show the dialog.
                if (result == DialogResult.OK) // Test result.
                {
                    file = openFileDialog1.FileName;
                    string[] imageNameValues = file.Split(new string[] { "\\" }, StringSplitOptions.RemoveEmptyEntries);
                    imageName = "watermarked_" + imageNameValues[imageNameValues.Length - 1];
                    try
                    {
                        Bitmap bmp = new Bitmap(file);
                        tempIm = new Bitmap(bmp);
                        pictureBox2.Image = bmp;

                    }
                    catch (IOException)
                    {
                    }
                }
                // retrieve from DB
                if (String.IsNullOrWhiteSpace(file))
                    return;
                var ds = new DataSet1();
                var dt = new DataSet1TableAdapters.WatermarkConfigurationTableAdapter();
                dt.Connection = new SqlConnection(connString);
                DataSet1.WatermarkConfigurationDataTable firstTable = new DataSet1.WatermarkConfigurationDataTable();
                dt.Fill(firstTable, pictureBox2.Image.Width, pictureBox2.Image.Height);

                try
                {
                    DataRow row = firstTable.Rows[0];

                    position = Convert.ToDouble(row["position"]);
                    resize = Convert.ToDouble(row["resize"]);
                    pos = Convert.ToInt32(row["shifting"]);
                    letter_size = Convert.ToInt32(row["letter_size"]);
                    letter_pos = Convert.ToInt32(row["letter_pos"]);

                    trackBarPosition.Value = (int)(position * 100);
                    trackBarResize.Value = (int)(resize * 100);
                    trackBarShifting.Value = pos;
                    trackBarLinkPos.Value = letter_pos;
                    trackBarLetterSize.Value = letter_size;

                    btnKeep.Text = "Update";
                    label6.Text = "There is already a record for this";
                    label6.Visible = true;
                    AddWatermarkToPicture();
                }
                catch (Exception)
                {
                    // den eixe kati na diavasei vazoume tis default times

                    position = .77;
                    resize = 1;
                    pos = 300;
                    letter_size = 14;
                    letter_pos = 120;
                    label6.Text = "There is no record for this";
                    btnKeep.Text = "Keep";
                    label6.Visible = true;
                    AddWatermarkToPicture();
                }
            }
            catch (Exception) { }
            /*
            using (SqlConnection connection = new SqlConnection(Properties.Settings.Default.Goomena_WaterMarkConnectionString))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;            // <== lacking
                    command.CommandType = CommandType.Text;
                    command.CommandText = "SELECT * FROM WatermarkConfiguration where(width between " + (pictureBox2.Image.Width - 5) + " and " + (pictureBox2.Image.Width + 5) + ") and (height between " + (pictureBox2.Image.Height - 5) + " and " + (pictureBox2.Image.Height + 5) + ")";


                    try
                    {
                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        try
                        {
                            reader.Read();
                        }
                        catch (Exception ex)
                        { }

                        if (reader.HasRows)
                        {
                            position = reader.GetDouble(2);
                            resize = reader.GetDouble(3);
                            pos = reader.GetInt32(4);
                            aaa();
                        }
                    }
                    catch (SqlException)
                    {
                        // error ..
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
             */
        }

        private Image cropImage(Image img, Rectangle cropArea)
        {
            Bitmap bmpImage = new Bitmap(img);
            Bitmap bmpCrop = bmpImage.Clone(cropArea, bmpImage.PixelFormat);
            return (Image)(bmpCrop);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            label6.Visible = false;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Graphics g = Graphics.FromImage(pictureBox1.Image);


        }

        private void button4_Click_1(object sender, EventArgs e)
        {

            pictureBox1.Dispose();

            //pictureBox1.Image = pictureBox2.Image;
            string goomenaLink = txtGoomenaLink.Text;

            Bitmap logoImage = new Bitmap(2500, 156);
            Image image = pictureBox3.Image;
            Graphics gp = Graphics.FromImage(logoImage);
            // kanoume repeat tin eikona mexri na gemisei 2500 pixels
            for (int x = 0; x <= logoImage.Width - image.Width; x += image.Width)
            {
                gp.DrawImage(image, new Point(x, 0));
            }
            SolidBrush transBrush = new SolidBrush(Color.FromArgb(180, 150, 150, 150));
            gp.DrawString(goomenaLink + "     " + goomenaLink + "     " + goomenaLink + "     " + goomenaLink + "     " + goomenaLink + "     " + goomenaLink + "     " + goomenaLink, new Font("Tahoma", 10), transBrush, new PointF(25, 90));

            gp.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceCopy;

            using (var br = new SolidBrush(Color.FromArgb(0, 255, 255, 255)))
            {
                gp.FillRectangle(br, new Rectangle(0, 0, 2500, 55));
                gp.FillRectangle(br, new Rectangle(0, 110, 2500, 156));
            }
            gp.Dispose();
            Image rotim = RotateImage(logoImage, new PointF(0, 0), 1f);

            Image im = SetImageOpacity(new Bitmap(rotim), 0.5f);

            using (Graphics grfx = Graphics.FromImage(pictureBox1.Image))
            {
                grfx.DrawImage(im, -10, (int)(pictureBox1.Image.Height * position));
            }
            Image immm = pictureBox1.Image;
            //pictureBox1.Image.Dispose();

            if (String.IsNullOrWhiteSpace(pathToSave))
                immm.Save("foo.jpg", ImageFormat.Jpeg);
            else
                immm.Save(pathToSave + "\\foo.jpg", ImageFormat.Jpeg);

        }

        private void trackBarPosition_Scroll(object sender, EventArgs e)
        {
            position = (double)trackBarPosition.Value / 100;
            AddWatermarkToPicture();
        }

        private void AddWatermarkToPicture()
        {
            if (string.IsNullOrEmpty(file)) return;

            pictureBox1.Image = new Bitmap(file);
            string goomenaLink = txtGoomenaLink.Text;

            if (pictureBox1.Image.Height < pictureBox1.Image.Width)
            {
                int height = (int)(170 * resize);
                int width = (int)(3000 * resize);
                Bitmap logoImage = new Bitmap(3000, height);
                Image image = new Bitmap(watermarktest3.Properties.Resources.logossss22);

                Graphics gp = Graphics.FromImage(logoImage);
                // kanoume repeat tin eikona mexri na gemisei 2500 pixels
                for (int x = 0; x <= logoImage.Width - image.Width; x += image.Width)
                {
                    gp.DrawImage(image, new Point(x, 0));
                }
                SolidBrush transBrush = new SolidBrush(Color.FromArgb(180, 150, 150, 150));
                gp.DrawString(goomenaLink + "     " + goomenaLink + "     " + goomenaLink + "     " + goomenaLink + "     " + goomenaLink + "     " + goomenaLink + "     " + goomenaLink, new Font("Tahoma", letter_size), transBrush, new PointF(25, letter_pos));

                gp.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceCopy;

                using (var br = new SolidBrush(Color.FromArgb(0, 255, 255, 255)))
                {
                    gp.FillRectangle(br, new Rectangle(0, 0, 3000, 52));
                    gp.FillRectangle(br, new Rectangle(0, 152, 3000, 170));
                }
                gp.Dispose();
                Image rotim = RotateImage(logoImage, new PointF(0, 0), 1f);

                Image im = SetImageOpacity(new Bitmap(rotim), opacity);
                Image image23 = new Bitmap(im, new Size(width, height));
                Image picBoxIm = pictureBox1.Image;
                try
                {
                    using (Graphics grfx = Graphics.FromImage(picBoxIm))
                    {
                        grfx.DrawImage(image23, -10, (int)(pictureBox1.Image.Height * position));
                    }
                }
                catch (Exception) { label6.Text = "an error occured with this image.."; }
                pictureBox1.Image = picBoxIm;
            }
            else
            {
                int height = (int)(170 * resize);
                int width = (int)(3000 * resize);
                Bitmap logoImage = new Bitmap(3000, height);
                Image image = new Bitmap(watermarktest3.Properties.Resources.logossss22);
                Graphics gp = Graphics.FromImage(logoImage);
                // kanoume repeat tin eikona mexri na gemisei 1920 pixels
                for (int x = 0; x <= logoImage.Width - image.Width; x += image.Width)
                {
                    gp.DrawImage(image, new Point(x, 0));
                }
                // meta grafoume to link katw apo to logotupo
                Graphics g = Graphics.FromImage(logoImage);
                SolidBrush transBrush = new SolidBrush(Color.FromArgb(180, 150, 150, 150));

                g.DrawString(goomenaLink + "     " + goomenaLink + "     " + goomenaLink + "     " + goomenaLink + "     " + goomenaLink + "     " + goomenaLink + "     " + goomenaLink + "     " + goomenaLink + "     " + goomenaLink, new Font("Tahoma", letter_size), transBrush, new PointF(25, letter_pos));
                g.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceCopy;
                using (var br = new SolidBrush(Color.FromArgb(0, 255, 255, 255)))
                {
                    g.FillRectangle(br, new Rectangle(0, 0, 3000, 52));
                    g.FillRectangle(br, new Rectangle(0, 152, 3000, 170));
                }
                // tis vazoume transparent deksia kai aristera apo ta grammaa

                Bitmap bitmapPicturebox1 = new Bitmap(logoImage);
                bitmapPicturebox1.RotateFlip(RotateFlipType.Rotate270FlipNone);

                // tin kanoume transparent
                Image im2 = SetImageOpacity(new Bitmap(bitmapPicturebox1), opacity);

                Image rotim = RotateImage(im2, new PointF(0, 0), -2f);
                Image image23 = new Bitmap(rotim, new Size(height, width));
                Image picBoxIm = pictureBox1.Image;
                using (Graphics grfx = Graphics.FromImage(picBoxIm))
                {
                    grfx.DrawImage(image23, -(int)(position * 100), -pos);
                }
                pictureBox1.Image = picBoxIm;
            }
        }

        private void trackBarResize_Scroll(object sender, EventArgs e)
        {
            resize = (double)trackBarResize.Value / 100;
            AddWatermarkToPicture();
        }

        private void btnKeep_Click(object sender, EventArgs e)
        {
            // swzoume tis rithmiseis
            if (btnKeep.Text == "Keep")
            {
                try
                {
                    var ds = new DataSet1();
                    var dt = new DataSet1TableAdapters.WatermarkConfiguration1TableAdapter();
                    dt.Connection = new SqlConnection(connString);
                    DataSet1.WatermarkConfiguration1DataTable firstTable = new DataSet1.WatermarkConfiguration1DataTable();
                    dt.GetData(pictureBox1.Image.Width, pictureBox1.Image.Height, position, resize, pos, letter_pos, letter_size);
                    btnKeep.Text = "Update";
                }
                catch (Exception) { }

            }
            else if (btnKeep.Text == "Update")
            {
                var ds = new DataSet1();
                var dt = new DataSet1TableAdapters.WatermarkConfiguration2TableAdapter();
                dt.Connection = new SqlConnection(connString);
                DataSet1.WatermarkConfiguration2DataTable firstTable = new DataSet1.WatermarkConfiguration2DataTable();
                dt.GetData(position, resize, pos, letter_size, letter_pos, pictureBox1.Image.Width, pictureBox1.Image.Height);

            }

            /*
            using (SqlConnection connection = new SqlConnection(Properties.Settings.Default.Goomena_WaterMarkConnectionString))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;            // <== lacking
                    command.CommandType = CommandType.Text;
                    command.CommandText = "INSERT into WatermarkConfiguration (width, height, position, resize, shifting) VALUES (@w, @h, @p, @r, @s)";
                    command.Parameters.AddWithValue("@w", pictureBox1.Image.Width);
                    command.Parameters.AddWithValue("@h", pictureBox1.Image.Height);
                    command.Parameters.AddWithValue("@p", position);
                    command.Parameters.AddWithValue("@r", resize);
                    command.Parameters.AddWithValue("@s", pos);


                    try
                    {
                        connection.Open();
                        int recordsAffected = command.ExecuteNonQuery();
                    }
                    catch (SqlException)
                    {
                        // error ..
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            */
            Image immm = pictureBox1.Image;
            //pictureBox1.Image.Dispose();

            if (checkBox1.Checked)
            {
                try
                {
                    if (String.IsNullOrWhiteSpace(pathToSave))
                        immm.Save(imageName, ImageFormat.Jpeg);
                    else
                        immm.Save(pathToSave + "\\" + imageName + ".jpg", ImageFormat.Jpeg);
                }
                catch (Exception)
                {
                    label6.Text = "cannot save file..";
                }
            }
            else
            {

            }

        }

        private void trackBarShifting_Scroll(object sender, EventArgs e)
        {
            pos = trackBarShifting.Value;
            AddWatermarkToPicture();
        }

        private void trackBarOpacity_Scroll(object sender, EventArgs e)
        {
            opacity = (float)((double)trackBarOpacity.Value / 100);
            AddWatermarkToPicture();
        }

        private void btnSaveLocation_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            DialogResult result = fbd.ShowDialog();

            txtSavePath.Text = pathToSave = fbd.SelectedPath;
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {

        }

        private void trackBarLinkPos_Scroll(object sender, EventArgs e)
        {
            letter_pos = trackBarLinkPos.Value;
            AddWatermarkToPicture();
        }

        private void trackBarLetterSize_Scroll(object sender, EventArgs e)
        {
            letter_size = trackBarLetterSize.Value;
            AddWatermarkToPicture();
        }
    }
}

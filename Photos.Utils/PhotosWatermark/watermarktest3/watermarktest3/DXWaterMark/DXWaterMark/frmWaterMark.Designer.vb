﻿Namespace CsWinFormsBlackApp
    Partial Public Class frmAddWatermark
        ''' <summary>
        ''' Required designer variable.
        ''' </summary>
        Private components As System.ComponentModel.IContainer = Nothing

        ''' <summary>
        ''' Clean up any resources being used.
        ''' </summary>
        ''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso (components IsNot Nothing) Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

#Region "Windows Form Designer generated code"

        ''' <summary>
        ''' Required method for Designer support - do not modify
        ''' the contents of this method with the code editor.
        ''' </summary>
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAddWatermark))
            Me.label9 = New DevExpress.XtraEditors.LabelControl()
            Me.trackBarLetterSize = New DevExpress.XtraEditors.TrackBarControl()
            Me.label8 = New DevExpress.XtraEditors.LabelControl()
            Me.trackBarLinkPos = New DevExpress.XtraEditors.TrackBarControl()
            Me.label3 = New DevExpress.XtraEditors.LabelControl()
            Me.label2 = New DevExpress.XtraEditors.LabelControl()
            Me.trackBarOpacity = New DevExpress.XtraEditors.TrackBarControl()
            Me.trackBarShifting = New DevExpress.XtraEditors.TrackBarControl()
            Me.label5 = New DevExpress.XtraEditors.LabelControl()
            Me.label4 = New DevExpress.XtraEditors.LabelControl()
            Me.trackBarResize = New DevExpress.XtraEditors.TrackBarControl()
            Me.trackBarPosition = New DevExpress.XtraEditors.TrackBarControl()
            Me.txtSavePath = New DevExpress.XtraEditors.TextEdit()
            Me.lblPathtoSave = New DevExpress.XtraEditors.LabelControl()
            Me.label1 = New DevExpress.XtraEditors.LabelControl()
            Me.txtGoomenaLink = New DevExpress.XtraEditors.TextEdit()
            Me.picMain = New System.Windows.Forms.PictureBox()
            Me.pictureBox6 = New System.Windows.Forms.PictureBox()
            Me.pictureBox5 = New System.Windows.Forms.PictureBox()
            Me.pictureBox4 = New System.Windows.Forms.PictureBox()
            Me.pictureBox3 = New System.Windows.Forms.PictureBox()
            Me.lblErrors = New DevExpress.XtraEditors.LabelControl()
            Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
            Me.chkPreserve = New DevExpress.XtraEditors.CheckEdit()
            Me.prgPanel1 = New DevExpress.XtraWaitForm.ProgressPanel()
            Me.BarManager1 = New DevExpress.XtraBars.BarManager()
            Me.Bar1 = New DevExpress.XtraBars.Bar()
            Me.Bar2 = New DevExpress.XtraBars.Bar()
            Me.Bar3 = New DevExpress.XtraBars.Bar()
            Me.btnStoreSettings = New DevExpress.XtraBars.BarButtonItem()
            Me.btnOpenImage = New DevExpress.XtraBars.BarButtonItem()
            Me.btnSaveLocation = New DevExpress.XtraBars.BarButtonItem()
            Me.btnSaveImage = New DevExpress.XtraBars.BarButtonItem()
            Me.btnClose = New DevExpress.XtraBars.BarButtonItem()
            Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl()
            Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl()
            Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl()
            Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl()
            Me.picError = New System.Windows.Forms.PictureBox()
            CType(Me.trackBarLetterSize, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.trackBarLetterSize.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.trackBarLinkPos, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.trackBarLinkPos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.trackBarOpacity, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.trackBarOpacity.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.trackBarShifting, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.trackBarShifting.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.trackBarResize, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.trackBarResize.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.trackBarPosition, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.trackBarPosition.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.txtSavePath.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.txtGoomenaLink.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.picMain, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.pictureBox6, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.pictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.pictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.pictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.chkPreserve.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.picError, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'label9
            '
            Me.label9.Location = New System.Drawing.Point(878, 23)
            Me.label9.Name = "label9"
            Me.label9.Size = New System.Drawing.Size(47, 13)
            Me.label9.TabIndex = 37
            Me.label9.Text = "letter size"
            '
            'trackBarLetterSize
            '
            Me.trackBarLetterSize.EditValue = 15
            Me.trackBarLetterSize.Location = New System.Drawing.Point(881, 39)
            Me.trackBarLetterSize.Name = "trackBarLetterSize"
            Me.trackBarLetterSize.Properties.Maximum = 20
            Me.trackBarLetterSize.Properties.Minimum = 10
            Me.trackBarLetterSize.Properties.Orientation = System.Windows.Forms.Orientation.Vertical
            Me.trackBarLetterSize.Size = New System.Drawing.Size(45, 317)
            Me.trackBarLetterSize.TabIndex = 36
            Me.trackBarLetterSize.Value = 15
            '
            'label8
            '
            Me.label8.Location = New System.Drawing.Point(809, 23)
            Me.label8.Name = "label8"
            Me.label8.Size = New System.Drawing.Size(55, 13)
            Me.label8.TabIndex = 35
            Me.label8.Text = "link position"
            '
            'trackBarLinkPos
            '
            Me.trackBarLinkPos.EditValue = 120
            Me.trackBarLinkPos.Location = New System.Drawing.Point(819, 39)
            Me.trackBarLinkPos.Name = "trackBarLinkPos"
            Me.trackBarLinkPos.Properties.Maximum = 150
            Me.trackBarLinkPos.Properties.Minimum = 30
            Me.trackBarLinkPos.Properties.Orientation = System.Windows.Forms.Orientation.Vertical
            Me.trackBarLinkPos.Size = New System.Drawing.Size(45, 317)
            Me.trackBarLinkPos.TabIndex = 34
            Me.trackBarLinkPos.Value = 120
            '
            'label3
            '
            Me.label3.Location = New System.Drawing.Point(941, 23)
            Me.label3.Name = "label3"
            Me.label3.Size = New System.Drawing.Size(35, 13)
            Me.label3.TabIndex = 33
            Me.label3.Text = "opacity"
            Me.label3.Visible = False
            '
            'label2
            '
            Me.label2.Location = New System.Drawing.Point(759, 23)
            Me.label2.Name = "label2"
            Me.label2.Size = New System.Drawing.Size(38, 13)
            Me.label2.TabIndex = 32
            Me.label2.Text = "shifting "
            '
            'trackBarOpacity
            '
            Me.trackBarOpacity.EditValue = 50
            Me.trackBarOpacity.Location = New System.Drawing.Point(938, 39)
            Me.trackBarOpacity.Name = "trackBarOpacity"
            Me.trackBarOpacity.Properties.Maximum = 100
            Me.trackBarOpacity.Properties.Orientation = System.Windows.Forms.Orientation.Vertical
            Me.trackBarOpacity.Size = New System.Drawing.Size(45, 317)
            Me.trackBarOpacity.TabIndex = 31
            Me.trackBarOpacity.Value = 50
            Me.trackBarOpacity.Visible = False
            '
            'trackBarShifting
            '
            Me.trackBarShifting.EditValue = 300
            Me.trackBarShifting.Location = New System.Drawing.Point(759, 39)
            Me.trackBarShifting.Name = "trackBarShifting"
            Me.trackBarShifting.Properties.Maximum = 500
            Me.trackBarShifting.Properties.Orientation = System.Windows.Forms.Orientation.Vertical
            Me.trackBarShifting.Size = New System.Drawing.Size(45, 317)
            Me.trackBarShifting.TabIndex = 30
            Me.trackBarShifting.Value = 300
            '
            'label5
            '
            Me.label5.Location = New System.Drawing.Point(754, 394)
            Me.label5.Name = "label5"
            Me.label5.Size = New System.Drawing.Size(28, 13)
            Me.label5.TabIndex = 41
            Me.label5.Text = "resize"
            '
            'label4
            '
            Me.label4.Location = New System.Drawing.Point(754, 454)
            Me.label4.Name = "label4"
            Me.label4.Size = New System.Drawing.Size(37, 13)
            Me.label4.TabIndex = 40
            Me.label4.Text = "position"
            '
            'trackBarResize
            '
            Me.trackBarResize.EditValue = 100
            Me.trackBarResize.Location = New System.Drawing.Point(745, 362)
            Me.trackBarResize.Name = "trackBarResize"
            Me.trackBarResize.Properties.Maximum = 120
            Me.trackBarResize.Properties.Minimum = 40
            Me.trackBarResize.Size = New System.Drawing.Size(317, 45)
            Me.trackBarResize.TabIndex = 39
            Me.trackBarResize.Value = 100
            '
            'trackBarPosition
            '
            Me.trackBarPosition.EditValue = 77
            Me.trackBarPosition.Location = New System.Drawing.Point(745, 422)
            Me.trackBarPosition.Name = "trackBarPosition"
            Me.trackBarPosition.Properties.Maximum = 100
            Me.trackBarPosition.Size = New System.Drawing.Size(317, 45)
            Me.trackBarPosition.TabIndex = 38
            Me.trackBarPosition.Value = 77
            '
            'txtSavePath
            '
            Me.txtSavePath.Location = New System.Drawing.Point(96, 542)
            Me.txtSavePath.Name = "txtSavePath"
            Me.txtSavePath.Size = New System.Drawing.Size(453, 20)
            Me.txtSavePath.TabIndex = 48
            '
            'lblPathtoSave
            '
            Me.lblPathtoSave.Location = New System.Drawing.Point(12, 540)
            Me.lblPathtoSave.Name = "lblPathtoSave"
            Me.lblPathtoSave.Size = New System.Drawing.Size(65, 13)
            Me.lblPathtoSave.TabIndex = 46
            Me.lblPathtoSave.Text = "Path to save:"
            '
            'label1
            '
            Me.label1.Location = New System.Drawing.Point(12, 518)
            Me.label1.Name = "label1"
            Me.label1.Size = New System.Drawing.Size(78, 13)
            Me.label1.TabIndex = 43
            Me.label1.Text = "Watermark Link:"
            '
            'txtGoomenaLink
            '
            Me.txtGoomenaLink.EditValue = "http://goomena.com/asfgdfh sjfgjfbsd"
            Me.txtGoomenaLink.Location = New System.Drawing.Point(96, 515)
            Me.txtGoomenaLink.Name = "txtGoomenaLink"
            Me.txtGoomenaLink.Size = New System.Drawing.Size(453, 20)
            Me.txtGoomenaLink.TabIndex = 42
            '
            'picMain
            '
            Me.picMain.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
            Me.picMain.Location = New System.Drawing.Point(15, 32)
            Me.picMain.Name = "picMain"
            Me.picMain.Size = New System.Drawing.Size(727, 468)
            Me.picMain.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
            Me.picMain.TabIndex = 52
            Me.picMain.TabStop = False
            '
            'pictureBox6
            '
            Me.pictureBox6.Image = CType(resources.GetObject("pictureBox6.Image"), System.Drawing.Image)
            Me.pictureBox6.Location = New System.Drawing.Point(989, 142)
            Me.pictureBox6.Name = "pictureBox6"
            Me.pictureBox6.Size = New System.Drawing.Size(100, 50)
            Me.pictureBox6.TabIndex = 56
            Me.pictureBox6.TabStop = False
            Me.pictureBox6.Visible = False
            '
            'pictureBox5
            '
            Me.pictureBox5.Image = CType(resources.GetObject("pictureBox5.Image"), System.Drawing.Image)
            Me.pictureBox5.Location = New System.Drawing.Point(989, 198)
            Me.pictureBox5.Name = "pictureBox5"
            Me.pictureBox5.Size = New System.Drawing.Size(100, 50)
            Me.pictureBox5.TabIndex = 55
            Me.pictureBox5.TabStop = False
            Me.pictureBox5.Visible = False
            '
            'pictureBox4
            '
            Me.pictureBox4.Image = CType(resources.GetObject("pictureBox4.Image"), System.Drawing.Image)
            Me.pictureBox4.Location = New System.Drawing.Point(1009, 169)
            Me.pictureBox4.Name = "pictureBox4"
            Me.pictureBox4.Size = New System.Drawing.Size(333, 156)
            Me.pictureBox4.TabIndex = 54
            Me.pictureBox4.TabStop = False
            Me.pictureBox4.Visible = False
            '
            'pictureBox3
            '
            Me.pictureBox3.Image = CType(resources.GetObject("pictureBox3.Image"), System.Drawing.Image)
            Me.pictureBox3.Location = New System.Drawing.Point(1009, 57)
            Me.pictureBox3.Name = "pictureBox3"
            Me.pictureBox3.Size = New System.Drawing.Size(370, 172)
            Me.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
            Me.pictureBox3.TabIndex = 53
            Me.pictureBox3.TabStop = False
            Me.pictureBox3.Visible = False
            '
            'lblErrors
            '
            Me.lblErrors.Location = New System.Drawing.Point(39, 13)
            Me.lblErrors.Name = "lblErrors"
            Me.lblErrors.Size = New System.Drawing.Size(39, 13)
            Me.lblErrors.TabIndex = 57
            Me.lblErrors.Text = "[Status]"
            '
            'OpenFileDialog1
            '
            Me.OpenFileDialog1.FileName = "OpenFileDialog1"
            '
            'chkPreserve
            '
            Me.chkPreserve.Location = New System.Drawing.Point(555, 542)
            Me.chkPreserve.Name = "chkPreserve"
            Me.chkPreserve.Properties.Caption = "Preserve file name"
            Me.chkPreserve.Size = New System.Drawing.Size(110, 19)
            Me.chkPreserve.TabIndex = 58
            '
            'prgPanel1
            '
            Me.prgPanel1.Appearance.BackColor = System.Drawing.Color.Transparent
            Me.prgPanel1.Appearance.Options.UseBackColor = True
            Me.prgPanel1.AppearanceCaption.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
            Me.prgPanel1.AppearanceCaption.Options.UseFont = True
            Me.prgPanel1.AppearanceDescription.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
            Me.prgPanel1.AppearanceDescription.Options.UseFont = True
            Me.prgPanel1.Location = New System.Drawing.Point(475, 239)
            Me.prgPanel1.LookAndFeel.SkinName = "McSkin"
            Me.prgPanel1.LookAndFeel.UseDefaultLookAndFeel = False
            Me.prgPanel1.Name = "prgPanel1"
            Me.prgPanel1.Size = New System.Drawing.Size(246, 66)
            Me.prgPanel1.TabIndex = 59
            Me.prgPanel1.Text = "ProgressPanel1"
            '
            'BarManager1
            '
            Me.BarManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar1, Me.Bar2, Me.Bar3})
            Me.BarManager1.DockControls.Add(Me.barDockControlTop)
            Me.BarManager1.DockControls.Add(Me.barDockControlBottom)
            Me.BarManager1.DockControls.Add(Me.barDockControlLeft)
            Me.BarManager1.DockControls.Add(Me.barDockControlRight)
            Me.BarManager1.Form = Me
            Me.BarManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.btnStoreSettings, Me.btnOpenImage, Me.btnSaveLocation, Me.btnSaveImage, Me.btnClose})
            Me.BarManager1.MainMenu = Me.Bar2
            Me.BarManager1.MaxItemId = 5
            Me.BarManager1.StatusBar = Me.Bar3
            '
            'Bar1
            '
            Me.Bar1.BarName = "Tools"
            Me.Bar1.DockCol = 0
            Me.Bar1.DockRow = 1
            Me.Bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
            Me.Bar1.Text = "Tools"
            Me.Bar1.Visible = False
            '
            'Bar2
            '
            Me.Bar2.BarName = "Main menu"
            Me.Bar2.DockCol = 0
            Me.Bar2.DockRow = 0
            Me.Bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
            Me.Bar2.OptionsBar.MultiLine = True
            Me.Bar2.OptionsBar.UseWholeRow = True
            Me.Bar2.Text = "Main menu"
            Me.Bar2.Visible = False
            '
            'Bar3
            '
            Me.Bar3.BarName = "Status bar"
            Me.Bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom
            Me.Bar3.DockCol = 0
            Me.Bar3.DockRow = 0
            Me.Bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom
            Me.Bar3.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnStoreSettings, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnOpenImage, "", True, True, True, 0, Nothing, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(Me.btnSaveLocation, True), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnSaveImage, "", True, True, True, 0, Nothing, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnClose, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)})
            Me.Bar3.OptionsBar.AllowQuickCustomization = False
            Me.Bar3.OptionsBar.DrawDragBorder = False
            Me.Bar3.OptionsBar.UseWholeRow = True
            Me.Bar3.Text = "Status bar"
            '
            'btnStoreSettings
            '
            Me.btnStoreSettings.Caption = "Save Settings"
            Me.btnStoreSettings.Glyph = Global.DXWaterMark.My.Resources.Resources.Save_Settings
            Me.btnStoreSettings.Id = 0
            Me.btnStoreSettings.Name = "btnStoreSettings"
            '
            'btnOpenImage
            '
            Me.btnOpenImage.Caption = "Open image"
            Me.btnOpenImage.Glyph = Global.DXWaterMark.My.Resources.Resources.Open_file
            Me.btnOpenImage.Id = 1
            Me.btnOpenImage.Name = "btnOpenImage"
            '
            'btnSaveLocation
            '
            Me.btnSaveLocation.Caption = "Set image new location"
            Me.btnSaveLocation.Id = 2
            Me.btnSaveLocation.Name = "btnSaveLocation"
            Me.btnSaveLocation.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
            '
            'btnSaveImage
            '
            Me.btnSaveImage.Caption = "Save Image"
            Me.btnSaveImage.Glyph = Global.DXWaterMark.My.Resources.Resources.filesave
            Me.btnSaveImage.Id = 3
            Me.btnSaveImage.Name = "btnSaveImage"
            '
            'btnClose
            '
            Me.btnClose.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right
            Me.btnClose.Caption = "Close"
            Me.btnClose.Glyph = Global.DXWaterMark.My.Resources.Resources.Button_Close_icon
            Me.btnClose.Id = 4
            Me.btnClose.Name = "btnClose"
            '
            'barDockControlTop
            '
            Me.barDockControlTop.CausesValidation = False
            Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
            Me.barDockControlTop.Location = New System.Drawing.Point(0, 0)
            Me.barDockControlTop.Size = New System.Drawing.Size(1066, 49)
            '
            'barDockControlBottom
            '
            Me.barDockControlBottom.CausesValidation = False
            Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
            Me.barDockControlBottom.Location = New System.Drawing.Point(0, 574)
            Me.barDockControlBottom.Size = New System.Drawing.Size(1066, 34)
            '
            'barDockControlLeft
            '
            Me.barDockControlLeft.CausesValidation = False
            Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
            Me.barDockControlLeft.Location = New System.Drawing.Point(0, 49)
            Me.barDockControlLeft.Size = New System.Drawing.Size(0, 525)
            '
            'barDockControlRight
            '
            Me.barDockControlRight.CausesValidation = False
            Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
            Me.barDockControlRight.Location = New System.Drawing.Point(1066, 49)
            Me.barDockControlRight.Size = New System.Drawing.Size(0, 525)
            '
            'picError
            '
            Me.picError.Image = Global.DXWaterMark.My.Resources.Resources.exclamation_2
            Me.picError.Location = New System.Drawing.Point(15, 9)
            Me.picError.Name = "picError"
            Me.picError.Size = New System.Drawing.Size(20, 20)
            Me.picError.TabIndex = 64
            Me.picError.TabStop = False
            '
            'frmAddWatermark
            '
            Me.ClientSize = New System.Drawing.Size(1066, 608)
            Me.Controls.Add(Me.picError)
            Me.Controls.Add(Me.prgPanel1)
            Me.Controls.Add(Me.chkPreserve)
            Me.Controls.Add(Me.lblErrors)
            Me.Controls.Add(Me.pictureBox6)
            Me.Controls.Add(Me.pictureBox5)
            Me.Controls.Add(Me.pictureBox4)
            Me.Controls.Add(Me.pictureBox3)
            Me.Controls.Add(Me.picMain)
            Me.Controls.Add(Me.txtSavePath)
            Me.Controls.Add(Me.lblPathtoSave)
            Me.Controls.Add(Me.label1)
            Me.Controls.Add(Me.txtGoomenaLink)
            Me.Controls.Add(Me.label5)
            Me.Controls.Add(Me.label4)
            Me.Controls.Add(Me.trackBarResize)
            Me.Controls.Add(Me.trackBarPosition)
            Me.Controls.Add(Me.label9)
            Me.Controls.Add(Me.trackBarLetterSize)
            Me.Controls.Add(Me.label8)
            Me.Controls.Add(Me.trackBarLinkPos)
            Me.Controls.Add(Me.label3)
            Me.Controls.Add(Me.label2)
            Me.Controls.Add(Me.trackBarOpacity)
            Me.Controls.Add(Me.trackBarShifting)
            Me.Controls.Add(Me.barDockControlLeft)
            Me.Controls.Add(Me.barDockControlRight)
            Me.Controls.Add(Me.barDockControlBottom)
            Me.Controls.Add(Me.barDockControlTop)
            Me.Name = "frmAddWatermark"
            Me.Text = "Add watermark "
            CType(Me.trackBarLetterSize.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.trackBarLetterSize, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.trackBarLinkPos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.trackBarLinkPos, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.trackBarOpacity.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.trackBarOpacity, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.trackBarShifting.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.trackBarShifting, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.trackBarResize.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.trackBarResize, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.trackBarPosition.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.trackBarPosition, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.txtSavePath.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.txtGoomenaLink.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.picMain, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.pictureBox6, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.pictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.pictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.pictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.chkPreserve.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.picError, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Private WithEvents label9 As DevExpress.XtraEditors.LabelControl
        Private WithEvents trackBarLetterSize As DevExpress.XtraEditors.TrackBarControl
        Private WithEvents label8 As DevExpress.XtraEditors.LabelControl
        Private WithEvents trackBarLinkPos As DevExpress.XtraEditors.TrackBarControl
        Private WithEvents label3 As DevExpress.XtraEditors.LabelControl
        Private WithEvents label2 As DevExpress.XtraEditors.LabelControl
        Private WithEvents trackBarOpacity As DevExpress.XtraEditors.TrackBarControl
        Private WithEvents trackBarShifting As DevExpress.XtraEditors.TrackBarControl
        Private WithEvents label5 As DevExpress.XtraEditors.LabelControl
        Private WithEvents label4 As DevExpress.XtraEditors.LabelControl
        Private WithEvents trackBarResize As DevExpress.XtraEditors.TrackBarControl
        Private WithEvents trackBarPosition As DevExpress.XtraEditors.TrackBarControl
        Private WithEvents txtSavePath As DevExpress.XtraEditors.TextEdit
        Private WithEvents lblPathtoSave As DevExpress.XtraEditors.LabelControl
        Private WithEvents label1 As DevExpress.XtraEditors.LabelControl
        Private WithEvents txtGoomenaLink As DevExpress.XtraEditors.TextEdit
        Private WithEvents picMain As System.Windows.Forms.PictureBox
        Private WithEvents pictureBox6 As System.Windows.Forms.PictureBox
        Private WithEvents pictureBox5 As System.Windows.Forms.PictureBox
        Private WithEvents pictureBox4 As System.Windows.Forms.PictureBox
        Private WithEvents pictureBox3 As System.Windows.Forms.PictureBox
        Friend WithEvents lblErrors As DevExpress.XtraEditors.LabelControl
        Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
        Friend WithEvents chkPreserve As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents prgPanel1 As DevExpress.XtraWaitForm.ProgressPanel
        Friend WithEvents BarManager1 As DevExpress.XtraBars.BarManager
        Friend WithEvents Bar1 As DevExpress.XtraBars.Bar
        Friend WithEvents Bar2 As DevExpress.XtraBars.Bar
        Friend WithEvents Bar3 As DevExpress.XtraBars.Bar
        Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
        Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
        Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
        Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
        Friend WithEvents btnStoreSettings As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents btnOpenImage As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents btnSaveLocation As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents btnSaveImage As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents btnClose As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents picError As System.Windows.Forms.PictureBox

#End Region

    End Class
End Namespace


﻿
Namespace CsWinFormsBlackApp

    Public Class SavingSettingsEventArgs
        Inherits System.EventArgs

        Public Property Settings As WatermarkSettings

        Public Sub New(ByVal data As WatermarkSettings)
            MyBase.New()
            Settings = data
        End Sub

    End Class

End Namespace


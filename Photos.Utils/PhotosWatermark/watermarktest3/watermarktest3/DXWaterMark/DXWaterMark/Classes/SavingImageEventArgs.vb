﻿
Namespace CsWinFormsBlackApp

    Public Class SavingImageEventArgs
        Inherits System.EventArgs

        Public Property PhotoID As Long
        Public Property ImagePath As String
        Public Property BinaryData As Image

        Public Sub New(PhID As Long, path As String, ByRef data As Image)
            MyBase.New()
            PhotoID = PhID
            ImagePath = path
            BinaryData = data
        End Sub

    End Class

End Namespace

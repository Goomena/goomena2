﻿Public Class clsNullable

    Public Shared Function NullTo(val As Boolean?, Optional defValue As Boolean = False) As Boolean
        If (val Is Nothing) Then Return defValue
        Return val.Value
    End Function


    Public Shared Function NullTo(val As Integer?, Optional defValue As Integer = 0) As Integer
        If (val Is Nothing) Then Return defValue
        Return val.Value
    End Function


    Public Shared Function NullTo(val As Double?, Optional defValue As Double = 0) As Double
        If (val Is Nothing) Then Return defValue
        Return val.Value
    End Function

    Public Shared Function NullTo(val As DateTime?, defValue As DateTime) As DateTime
        If (val Is Nothing) Then Return defValue
        Return val.Value
    End Function


    Public Shared Function DBNullToInteger(val As Object, Optional defValue As Integer = 0) As Integer
        If (Not IsDBNull(val)) Then defValue = Convert.ToInt32(val)
        Return defValue
    End Function


    Public Shared Function DBNullToDouble(val As Object, Optional defValue As Double = 0) As Double
        If (Not IsDBNull(val)) Then defValue = Convert.ToDouble(val)
        Return defValue
    End Function


    Public Shared Function DBNullToDecimal(val As Object, Optional defValue As Decimal = 0) As Decimal
        If (Not IsDBNull(val)) Then
            If (val.GetType() Is GetType(String)) Then
                Dim dbl As Double = 0
                Dim culture As System.Globalization.CultureInfo = System.Globalization.CultureInfo.CreateSpecificCulture("el-GR")
                Double.TryParse(val.ToString().Replace(".", ","), System.Globalization.NumberStyles.Any, culture, dbl)
                defValue = Convert.ToDecimal(dbl)
            Else
                defValue = Convert.ToDecimal(val)
            End If
        End If
        Return defValue
    End Function


End Class

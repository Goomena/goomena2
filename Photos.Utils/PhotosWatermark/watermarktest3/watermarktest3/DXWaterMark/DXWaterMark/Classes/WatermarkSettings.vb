﻿

Public Class WatermarkSettings
    Private __row As DataRow

    Public Property SettingsAlreadyExists As Boolean

    Private __Width As Integer
    Public Property Width As Integer
        Get
            Return __Width
        End Get
        Set(value As Integer)
            __Width = value
        End Set
    End Property


    Private __Height As Integer
    Public Property Height As Integer
        Get
            Return __Height
        End Get
        Set(value As Integer)
            __Height = value
        End Set
    End Property


    Private __position As Double = 0.77
    Public Property Position As Double
        Get
            Return __position
        End Get
        Set(value As Double)
            __position = value
        End Set
    End Property

    Private __letter_size As Integer = 14
    Public Property letter_size As Integer
        Get
            Return __letter_size
        End Get
        Set(value As Integer)
            __letter_size = value
        End Set
    End Property

    Private __letter_pos As Integer = 120
    Public Property letter_pos As Integer
        Get
            Return __letter_pos
        End Get
        Set(value As Integer)
            __letter_pos = value
        End Set
    End Property

    Private __resize As Double = 1
    Public Property resize As Double
        Get
            Return __resize
        End Get
        Set(value As Double)
            __resize = value
        End Set
    End Property


    Private __opacity As Single = 0.5F
    Public Property opacity As Single
        Get
            Return __opacity
        End Get
        Set(value As Single)
            __opacity = value
        End Set
    End Property


    Private __ShiftingPos As Integer = 300
    Public Property ShiftingPos As Integer
        Get
            Return __ShiftingPos
        End Get
        Set(value As Integer)
            __ShiftingPos = value
        End Set
    End Property


    Private __url As String
    Public Property URL As String
        Get
            Return __url
        End Get
        Set(value As String)
            __url = value
        End Set
    End Property


    Public Sub New()
    End Sub


    Public Sub New(row As DataRow)
        __row = row

        If (__row IsNot Nothing) Then
            SettingsAlreadyExists = True
            __position = clsNullable.DBNullToDecimal(__row("position"))
            __resize = clsNullable.DBNullToDecimal(__row("resize"))
            __ShiftingPos = clsNullable.DBNullToInteger(__row("shifting"))
            __letter_size = clsNullable.DBNullToInteger(__row("letter_size"))
            __letter_pos = clsNullable.DBNullToInteger(__row("letter_pos"))
        End If

    End Sub

End Class

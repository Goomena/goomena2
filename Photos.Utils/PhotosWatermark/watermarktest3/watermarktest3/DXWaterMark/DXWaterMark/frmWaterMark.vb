﻿Imports System.ComponentModel
Imports System.Text
Imports System.Data.SqlClient
Imports System.IO
Imports System.Drawing.Imaging
Imports System.Drawing.Drawing2D

Namespace CsWinFormsBlackApp
    Partial Public Class frmAddWatermark
        Inherits DevExpress.XtraEditors.XtraForm


        Private __connString As String = My.Settings.Goomena__LocalConnectionString
        Private __PhotoID As Long
        Private __pathToSave As String = ""
        Private __position As Double = 0.77
        Private __imageName As String = ""
        Private __letter_size As Integer = 14
        Private __letter_pos As Integer = 120
        Private __resize As Double = 1
        Private __tempIm As Image
        Private __filePath As String = ""
        Private __opacity As Single = 0.5F
        Private __ShiftingPos As Integer = 300

        Private __CurrentSettings As WatermarkSettings
        Private __IsDelayedLoad As Boolean
        Private __SaveSettingsUsingEvent As Boolean
        Private __SaveImageUsingEvent As Boolean
        Private listOfDisabledControls As List(Of String)

        'Public Event SavingImage As EventHandler
        'Public Event SavingSettings As SavingSettingsEventHandler
        Public Event SavingImage(ByVal sender As Object, ByVal Args As SavingImageEventArgs)
        Public Event SavingSettings(ByVal sender As Object, ByVal Args As SavingSettingsEventArgs)
        Public Event FormLoaded(ByVal sender As Object, ByVal Args As EventArgs)

        Public Sub New()
            listOfDisabledControls = New List(Of String)
            InitializeComponent()
            prgPanel1.Visible = False
            btnStoreSettings.Enabled = False
        End Sub

        Public Sub New(delayedLoad As Boolean)
            listOfDisabledControls = New List(Of String)
            InitializeComponent()
            __IsDelayedLoad = delayedLoad
        End Sub


        Private Sub frmAddWatermark_Shown(sender As Object, e As System.EventArgs) Handles Me.Shown
            HideError()
            onDelayedShown_SetUI()
            RaiseEvent FormLoaded(Me, New EventArgs())
        End Sub

        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")> Private Sub button1_Click(sender As Object, e As EventArgs)
            If picMain.Image Is Nothing Then
                Return
            End If

            ' pernoume tis suntetagmenes tis eikonas
            Dim width As Integer = picMain.Image.Width
            Dim height As Integer = picMain.Image.Height
            Dim goomenaLink As String = txtGoomenaLink.Text
            Dim timesFit As Integer = (width \ 318) + 1
            '   Dim strip As New Bitmap(318 * timesFit, 52)

            'string WorkingDirectory = "";

            'define a string of text to use as the Copyright message
            'string Copyright = "SKATAasdmd4m3f";

            'create a image object containing the photograph to watermark
            Using imgPhoto As Image = Image.FromFile("logos.jpg")


                Dim phWidth As Integer = imgPhoto.Width
                Dim phHeight As Integer = imgPhoto.Height

                'create a Bitmap the Size of the original photograph
                'Bitmap bmPhoto = new Bitmap(phWidth, phHeight, PixelFormat.Format24bppRgb);


                Using myBitmap As New Bitmap(pictureBox3.Image)


                    Using g As Graphics = Graphics.FromImage(myBitmap)


                        g.InterpolationMode = Drawing2D.InterpolationMode.HighQualityBicubic
                        Using transBrush As New SolidBrush(Color.FromArgb(113, 170, 170, 170))


                            'g.DrawString("Goomena   Goomena   Goomena   Goomena   Goomena   Goomena   Goomena   Goomena   ", new Font("Tahoma", 16), transBrush, new PointF(10, 5));
                            'TextRenderer.DrawText(Graphics.FromImage(myBitmap), "G♀♂mena", new Font("Tahoma", 16), new Point(100, 100), Color.Black);
                            g.DrawString(goomenaLink & "     " & goomenaLink & "     " & goomenaLink & "     " & goomenaLink & "     ", New Font("Tahoma", 8), transBrush, New PointF(10, 85))
                            pictureBox3.Image.Dispose()
                            pictureBox3.Image = myBitmap
                        End Using
                        Using im As Image = SetImageOpacity(myBitmap, 0.5F)
                            im.Save("asdasd.png", ImageFormat.Png)
                            Dim bit As New Bitmap(im)
                            Using grfx As Graphics = Graphics.FromImage(picMain.Image)
                                grfx.InterpolationMode = Drawing2D.InterpolationMode.HighQualityBicubic
                                grfx.DrawImage(bit, 10, 100)
                            End Using

                            picMain.Image.Dispose()
                            picMain.Image = bit

                            Using rotim As Image = RotateImage(im, New PointF(0, 0), -3.0F)
                                rotim.Save("rotim.png", ImageFormat.Png)
                            End Using
                            'End Using
                        End Using
                    End Using
                End Using
            End Using
        End Sub

        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")> Public Function SetImageOpacity(image As Bitmap, opacity As Single) As Image
            Try
                'create a Bitmap the size of the image provided  
                Dim bmp As New Bitmap(image.Width, image.Height)



                'create a graphics object from the image  
                Using gfx As Graphics = Graphics.FromImage(bmp)
                    gfx.InterpolationMode = Drawing2D.InterpolationMode.HighQualityBicubic

                    'create a color matrix object  
                    Dim matrix As New ColorMatrix()

                    'set the opacity  
                    matrix.Matrix33 = opacity

                    'create image attributes  
                    Using attributes As New ImageAttributes()



                        'set the color(opacity) of the image  
                        attributes.SetColorMatrix(matrix, ColorMatrixFlag.[Default], ColorAdjustType.Bitmap)

                        'now draw the image  
                        gfx.DrawImage(image, New Rectangle(0, 0, bmp.Width, bmp.Height), 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, attributes)
                    End Using
                End Using
                Return bmp
                ' End Using
            Catch ex As Exception
                MessageBox.Show(ex.Message)
                Return Nothing
            End Try
        End Function

        Private Sub writeWaterMark(graphicsHandle As Graphics, x As Integer, y As Integer, ourFont As Font, text As String)
            Using strFormatter As New StringFormat()


                Using transBrush As New SolidBrush(Color.FromArgb(113, 255, 255, 255))
                    ' Drawstring the transparent text to the Graphics context at x,y position.
                    graphicsHandle.DrawString(text, ourFont, transBrush, New PointF(x, y), strFormatter)
                End Using


            End Using
        End Sub

        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")> Public Function RotateImage(image As Image, offset As PointF, angle As Single) As Bitmap
            If image Is Nothing Then
                Throw New ArgumentNullException("image")
            End If

            'create a new empty bitmap to hold rotated image
            Dim rotatedBmp As New Bitmap(image.Width + 50, image.Height + 100, image.PixelFormat)
            rotatedBmp.SetResolution(image.HorizontalResolution, image.VerticalResolution)

            'make a graphics object from the empty bitmap
            Using g As Graphics = Graphics.FromImage(rotatedBmp)


                g.InterpolationMode = Drawing2D.InterpolationMode.HighQualityBicubic

                'Put the rotation point in the center of the image
                g.TranslateTransform(offset.X, offset.Y)

                'rotate the image
                g.RotateTransform(angle)

                'move the image back
                g.TranslateTransform(-offset.X, -offset.Y)

                'draw passed in image onto graphics object
                g.DrawImage(image, New PointF(0, 0))
            End Using
            Return rotatedBmp
        End Function


        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")> Public Function RotateImage(img As Image, rotationAngle As Single) As Image
            'create an empty Bitmap image
            Dim bmp As New Bitmap(img.Width, img.Height, img.PixelFormat)

            'turn the Bitmap into a Graphics object
            Using gfx As Graphics = Graphics.FromImage(bmp)


                gfx.InterpolationMode = Drawing2D.InterpolationMode.HighQualityBicubic

                'now we set the rotation point to the center of our image
                gfx.TranslateTransform(CSng(bmp.Width) / 2, CSng(bmp.Height) / 2)

                'now rotate the image
                gfx.RotateTransform(rotationAngle)

                gfx.TranslateTransform(-CSng(bmp.Width) / 2, -CSng(bmp.Height) / 2)

                'set the InterpolationMode to HighQualityBicubic so to ensure a high
                'quality image once it is transformed to the specified size
                gfx.InterpolationMode = InterpolationMode.HighQualityBicubic

                'now draw our new image onto the graphics object
                gfx.DrawImage(img, New Point(0, 0))

                'dispose of our Graphics object

            End Using
            'return the image
            Return bmp
        End Function

        Private Sub pictureBox2_Paint(sender As Object, e As PaintEventArgs)
            ' writeWaterMark(e.Graphics, 100, 100, new Font("Arial", 14, FontStyle.Bold), "Copyright Dream.In.Code.net");
        End Sub

        Public Sub setWatermark(srcImage As Bitmap, pathtosave As String)
            picMain.Image = srcImage
            button2_Click()
        End Sub

        Private Sub button2_Click()

            If picMain.Image Is Nothing Then
                Return
            End If

            Dim goomenaLink As String = txtGoomenaLink.Text

            If picMain.Image.Height < picMain.Image.Width Then
                ' prwta kanoume ena resize gia na tairiazei stis analogies tis eikonas
                'double percentage = 0.15;
                'int logoImageheight = (int)(pictureBox1.Image.Height * percentage);
                'int logoImageWidth = (logoImageheight * 318) / 156;
                'pictureBox3.Image = new Bitmap(pictureBox3.Image, new Size(logoImageWidth, logoImageheight));
                If picMain.Image.Height <= 305 Then
                    pictureBox3.Image = pictureBox6.Image
                End If

                Using logoImage As New Bitmap(2500, 156)
                    Using image As Image = pictureBox3.Image
                        Using gp As Graphics = Graphics.FromImage(logoImage)
                            gp.InterpolationMode = Drawing2D.InterpolationMode.HighQualityBicubic
                            ' kanoume repeat tin eikona mexri na gemisei 2500 pixels
                            Dim x As Integer = 0
                            While x <= logoImage.Width - image.Width
                                gp.DrawImage(image, New Point(x, 0))
                                x += image.Width
                            End While
                        End Using
                 
              
                
               


                ' meta grafoume to link katw apo to logotupo
                        Using g As Graphics = Graphics.FromImage(logoImage)


                            g.InterpolationMode = Drawing2D.InterpolationMode.HighQualityBicubic
                            Using transBrush As New SolidBrush(Color.FromArgb(180, 150, 150, 150))


                                Using f As New Font("Tahoma", 4)


                                    If picMain.Image.Height <= 305 Then
                                        g.DrawString(goomenaLink & "     " & goomenaLink & "     " & goomenaLink & "     " & goomenaLink & "     " & goomenaLink & "     " & goomenaLink & "     " & goomenaLink, f, transBrush, New PointF(15, 32))
                                    Else
                                        g.DrawString(goomenaLink & "     " & goomenaLink & "     " & goomenaLink & "     " & goomenaLink & "     " & goomenaLink & "     " & goomenaLink & "     " & goomenaLink, f, transBrush, New PointF(25, 90))
                                    End If
                                End Using
                                ' kanoume panw k katw apo ta grammata ta panta full transparent

                                g.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceCopy

                                If picMain.Image.Height <= 305 Then
                                    Using br = New SolidBrush(Color.FromArgb(0, 255, 255, 255))
                                        g.FillRectangle(br, New Rectangle(0, 0, 1920, 20))
                                        g.FillRectangle(br, New Rectangle(0, 40, 1920, 60))
                                    End Using
                                Else
                                    Using br = New SolidBrush(Color.FromArgb(0, 255, 255, 255))
                                        g.FillRectangle(br, New Rectangle(0, 0, 1920, 55))
                                        g.FillRectangle(br, New Rectangle(0, 110, 1920, 156))
                                    End Using
                                End If
                            End Using
                        End Using

                        ' twra tin kanoume rotate
                        Using rotim As Image = RotateImage(logoImage, New PointF(0, 0), 1.0F)
                            Using o As New Bitmap(rotim)


                                Using im As Image = SetImageOpacity(o, 0.5F)
                                    ' prepei na tin kanoume crop..
                                    Dim y As Integer = -1
                                    If picMain.Image.Width > 700 AndAlso picMain.Image.Width < 800 AndAlso picMain.Image.Height > 300 AndAlso picMain.Image.Height < 350 Then
                                        y = CInt(Math.Truncate(picMain.Image.Height * 0.7))
                                    ElseIf picMain.Image.Width > 2500 AndAlso picMain.Image.Width < 2600 AndAlso picMain.Image.Height > 1800 AndAlso picMain.Image.Height < 2000 Then
                                        y = CInt(Math.Truncate(picMain.Image.Height * 0.8))
                                    ElseIf picMain.Image.Width > 620 AndAlso picMain.Image.Width < 650 AndAlso picMain.Image.Height > 340 AndAlso picMain.Image.Height < 370 Then
                                        y = CInt(Math.Truncate(picMain.Image.Height * 0.7))
                                    ElseIf picMain.Image.Width > 2200 AndAlso picMain.Image.Width < 2260 AndAlso picMain.Image.Height > 1900 AndAlso picMain.Image.Height < 2000 Then
                                        y = CInt(Math.Truncate(picMain.Image.Height * 0.81))
                                    ElseIf picMain.Image.Width > 850 AndAlso picMain.Image.Width < 900 AndAlso picMain.Image.Height > 300 AndAlso picMain.Image.Height < 350 Then
                                        y = CInt(Math.Truncate(picMain.Image.Height * 0.7))
                                    ElseIf picMain.Image.Width > 390 AndAlso picMain.Image.Width < 410 AndAlso picMain.Image.Height > 300 AndAlso picMain.Image.Height < 310 Then
                                        y = CInt(Math.Truncate(picMain.Image.Height * 0.82))
                                    ElseIf picMain.Image.Height > 1000 Then
                                        y = CInt(Math.Truncate(picMain.Image.Height * 0.87))
                                    ElseIf picMain.Image.Height > 700 Then
                                        y = CInt(Math.Truncate(picMain.Image.Height * 0.85))
                                    ElseIf picMain.Image.Height > 500 Then
                                        y = CInt(Math.Truncate(picMain.Image.Height * 0.8))
                                    ElseIf picMain.Image.Height > 300 Then
                                        y = CInt(Math.Truncate(picMain.Image.Height * 0.77))
                                    Else
                                        y = CInt(Math.Truncate(picMain.Image.Height * 0.77))
                                    End If

                                    Using grfx As Graphics = Graphics.FromImage(picMain.Image)
                                        grfx.InterpolationMode = Drawing2D.InterpolationMode.HighQualityBicubic
                                        grfx.DrawImage(im, -10, CInt(Math.Truncate(picMain.Image.Height * 0.77)))
                                    End Using
                                End Using
                            End Using
                        End Using


                        ' tin kanoume transparent





                        Using immm As Image = picMain.Image


                            picMain.Image = immm
                            If String.IsNullOrWhiteSpace(__pathToSave) Then
                                immm.Save("foo.jpg", ImageFormat.Png)
                            Else
                                immm.Save(__pathToSave & "\foo.jpg", ImageFormat.Png)
                            End If
                        End Using
                    End Using
                End Using
            Else
                ' an to upsos einai megalitero apo to platos


                Using logoImage As New Bitmap(2500, 156)
                    Using image As Image = pictureBox3.Image
                        Using gp As Graphics = Graphics.FromImage(logoImage)
                            gp.InterpolationMode = Drawing2D.InterpolationMode.HighQualityBicubic

                            ' kanoume repeat tin eikona mexri na gemisei 1920 pixels
                            Dim x As Integer = 0
                            While x <= logoImage.Width - image.Width
                                gp.DrawImage(image, New Point(x, 0))
                                x += image.Width
                            End While
                        End Using




                        ' meta grafoume to link katw apo to logotupo
                        Using g As Graphics = Graphics.FromImage(logoImage)
                            g.InterpolationMode = Drawing2D.InterpolationMode.HighQualityBicubic
                            Using transBrush As New SolidBrush(Color.FromArgb(180, 150, 150, 150))
                                Using o As New Font("Tahoma", 10)


                                    g.DrawString(goomenaLink & "     " & goomenaLink & "     " & goomenaLink & "     " & goomenaLink & "     " & goomenaLink & "     " & goomenaLink & "     " & goomenaLink & "     " & goomenaLink & "     " & goomenaLink, o, transBrush, New PointF(25, 90))
                                    g.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceCopy
                                    Using br = New SolidBrush(Color.FromArgb(0, 255, 255, 255))
                                        g.FillRectangle(br, New Rectangle(0, 0, 2500, 55))
                                        g.FillRectangle(br, New Rectangle(0, 110, 2500, 156))
                                    End Using
                                End Using
                            End Using
                        End Using



                        ' tis vazoume transparent deksia kai aristera apo ta grammaa

                        Using bitmapPicturebox1 As New Bitmap(logoImage)
                            bitmapPicturebox1.RotateFlip(RotateFlipType.Rotate270FlipNone)
                            ' tin kanoume transparent
                            Using o As New Bitmap(bitmapPicturebox1)


                                Using im2 As Image = SetImageOpacity(o, 0.5F)
                                    Using rotim As Image = RotateImage(im2, New PointF(0, 0), -2.0F)
                                        Using grfx As Graphics = Graphics.FromImage(picMain.Image)
                                            grfx.InterpolationMode = Drawing2D.InterpolationMode.HighQualityBicubic
                                            grfx.DrawImage(rotim, -20, -20)
                                        End Using
                                    End Using
                                End Using
                            End Using
                        End Using
                    End Using
                End Using
            End If
        End Sub

        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")> Private Function ResizeImage(image As Image, size As Size, Optional preserveAspectRatio As Boolean = True) As Image
            Dim newWidth As Integer
            Dim newHeight As Integer
            If preserveAspectRatio Then
                Dim originalWidth As Integer = image.Width
                Dim originalHeight As Integer = image.Height
                Dim percentWidth As Single = CSng(size.Width) / CSng(originalWidth)
                Dim percentHeight As Single = CSng(size.Height) / CSng(originalHeight)
                Dim percent As Single = If(percentHeight < percentWidth, percentHeight, percentWidth)
                newWidth = CInt(Math.Truncate(originalWidth * percent))
                newHeight = CInt(Math.Truncate(originalHeight * percent))
            Else
                newWidth = size.Width
                newHeight = size.Height
            End If
            Dim newImage As Image = New Bitmap(newWidth, newHeight, image.PixelFormat)
            Using graphicsHandle As Graphics = Graphics.FromImage(newImage)
                graphicsHandle.InterpolationMode = Drawing2D.InterpolationMode.HighQualityBicubic
                graphicsHandle.InterpolationMode = InterpolationMode.HighQualityBicubic
                graphicsHandle.DrawImage(image, 0, 0, newWidth, newHeight)
            End Using
            Return newImage
        End Function

        Private Sub DrawDigonalString(G As Graphics, S As String, F As Font, B As Brush, P As PointF, Angle As Integer)
            Dim MySize As SizeF = G.MeasureString(S, F)
            G.TranslateTransform(P.X + MySize.Width / 2, P.Y + MySize.Height / 2)
            G.RotateTransform(Angle)
            G.DrawString(S, F, B, New PointF(-MySize.Width / 2, -MySize.Height / 2))
            G.RotateTransform(-Angle)
            G.TranslateTransform(-P.X - MySize.Width / 2, -P.Y - MySize.Height / 2)
        End Sub

        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")> Private Sub btnOpenImage_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnOpenImage.ItemClick
            Try
                Dim result As DialogResult = OpenFileDialog1.ShowDialog()
                ' Show the dialog.
                If result = DialogResult.OK Then
                    ' Test result.
                    __filePath = OpenFileDialog1.FileName
                    Dim imageNameValues As String() = __filePath.Split(New String() {"\"}, StringSplitOptions.RemoveEmptyEntries)
                    __imageName = "watermarked_" & imageNameValues(imageNameValues.Length - 1)
                    Try
                        Dim bmp As New Bitmap(__filePath)
                        __tempIm = New Bitmap(bmp)

                        picMain.Image = bmp
                    Catch ex As IOException
                    End Try
                End If
                ' retrieve from DB
                If String.IsNullOrWhiteSpace(__filePath) Then
                    Return
                End If

                Using ds = New DataSet1()
                    Using dt = New DataSet1TableAdapters.WatermarkConfigurationTableAdapter()
                        Using con As New SqlConnection(__connString)
                            dt.Connection = con
                            Using firstTable As New DataSet1.WatermarkConfigurationDataTable()
                                dt.Fill(firstTable, picMain.Image.Width, picMain.Image.Height)
                                Try
                                    Dim row As DataRow = firstTable.Rows(0)

                                    __position = Convert.ToDouble(row("position"))
                                    __resize = Convert.ToDouble(row("resize"))
                                    __ShiftingPos = Convert.ToInt32(row("shifting"))
                                    __letter_size = Convert.ToInt32(row("letter_size"))
                                    __letter_pos = Convert.ToInt32(row("letter_pos"))

                                    trackBarPosition.Value = CInt(Math.Truncate(__position * 100))
                                    trackBarResize.Value = CInt(Math.Truncate(__resize * 100))
                                    trackBarShifting.Value = __ShiftingPos
                                    trackBarLinkPos.Value = __letter_pos
                                    trackBarLetterSize.Value = __letter_size

                                    'btnStoreSettings.Text = "Update"
                                    ShowError("There is already a record for this")
                                    lblErrors.Visible = True
                                    SetWatermarkOnPicture()
                                Catch ex As Exception
                                    ' den eixe kati na diavasei vazoume tis default times

                                    __position = 0.77
                                    __resize = 1
                                    __ShiftingPos = 300
                                    __letter_size = 14
                                    __letter_pos = 120
                                    ShowError("There is no record for this")
                                    'btnStoreSettings.Text = "Keep"
                                    lblErrors.Visible = True
                                    SetWatermarkOnPicture()
                                End Try
         
                            End Using
                        End Using
                    End Using
                End Using
            Catch ex As Exception
            End Try







        End Sub

        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")> Private Function cropImage(img As Image, cropArea As Rectangle) As Image
            Using bmpImage As New Bitmap(img)
                Dim bmpCrop As Bitmap = bmpImage.Clone(cropArea, bmpImage.PixelFormat)
                Return DirectCast(bmpCrop, Image)
            End Using
        End Function

        Private Sub Form1_Load(sender As Object, e As EventArgs)
            lblErrors.Visible = False
        End Sub

        'Private Sub button4_Click(sender As Object, e As EventArgs)
        '    Dim g As Graphics = Graphics.FromImage(picMain.Image)


        'End Sub

        Private Sub button4_Click_1(sender As Object, e As EventArgs)

            picMain.Dispose()

            'pictureBox1.Image = pictureBox2.Image;
            Dim goomenaLink As String = txtGoomenaLink.Text

            Using logoImage As New Bitmap(2500, 156)
                Using image As Image = pictureBox3.Image
                    Using gp As Graphics = Graphics.FromImage(logoImage)
                        gp.InterpolationMode = Drawing2D.InterpolationMode.HighQualityBicubic

                        ' kanoume repeat tin eikona mexri na gemisei 2500 pixels
                        Dim x As Integer = 0
                        While x <= logoImage.Width - image.Width
                            gp.DrawImage(image, New Point(x, 0))
                            x += image.Width
                        End While

                        Using transBrush As New SolidBrush(Color.FromArgb(180, 150, 150, 150))
                            Using o As New Font("Tahoma", 10)



                                gp.DrawString(goomenaLink & "     " & goomenaLink & "     " & goomenaLink & "     " & goomenaLink & "     " & goomenaLink & "     " & goomenaLink & "     " & goomenaLink, o, transBrush, New PointF(25, 90))

                                gp.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceCopy

                                Using br = New SolidBrush(Color.FromArgb(0, 255, 255, 255))
                                    gp.FillRectangle(br, New Rectangle(0, 0, 2500, 55))
                                    gp.FillRectangle(br, New Rectangle(0, 110, 2500, 156))
                                End Using
                            End Using
                        End Using
                    End Using
                End Using
                Using rotim As Image = RotateImage(logoImage, New PointF(0, 0), 1.0F)
                    Using o As New Bitmap(rotim)


                        Using im As Image = SetImageOpacity(o, 0.5F)
                            Using grfx As Graphics = Graphics.FromImage(picMain.Image)
                                grfx.InterpolationMode = Drawing2D.InterpolationMode.HighQualityBicubic
                                grfx.DrawImage(im, -10, CInt(Math.Truncate(picMain.Image.Height * __position)))
                            End Using
                        End Using
                    End Using
                End Using
                Using immm As Image = picMain.Image
                    'pictureBox1.Image.Dispose();

                    If String.IsNullOrWhiteSpace(__pathToSave) Then
                        immm.Save("foo.jpg", ImageFormat.Png)
                    Else
                        immm.Save(__pathToSave & "\foo.jpg", ImageFormat.Png)
                    End If
                End Using
            End Using










        End Sub

        Private Sub trackBarPosition_Scroll(sender As Object, e As EventArgs) Handles trackBarPosition.EditValueChanged
            __position = CDbl(trackBarPosition.Value) / 100
            SetWatermarkOnPicture()
            btnStoreSettings.Enabled = True
        End Sub

        Private Sub SetWatermarkOnPicture()

            If (String.IsNullOrEmpty(__filePath)) Then Return

            picMain.Image = New Bitmap(__filePath)
            Dim goomenaLink As String = txtGoomenaLink.Text

            If picMain.Image.Height < picMain.Image.Width Then
                Dim height As Integer = CInt(Math.Truncate(170 * __resize))
                Dim width As Integer = CInt(Math.Truncate(3000 * __resize))
                Using logoImage As New Bitmap(3000, height)
                    Using image As Image = New Bitmap(My.Resources.logossss22)

                  
               

                Using gp As Graphics = Graphics.FromImage(logoImage)
                    gp.InterpolationMode = Drawing2D.InterpolationMode.HighQualityBicubic
                    ' kanoume repeat tin eikona mexri na gemisei 2500 pixels
                    Dim x As Integer = 0
                    While x <= logoImage.Width - Image.Width
                        gp.DrawImage(Image, New Point(x, 0))
                        x += Image.Width
                    End While
                            Using transBrush As New SolidBrush(Color.FromArgb(180, 150, 150, 150))
                                Using o As New Font("Tahoma", __letter_size)


                                    gp.DrawString(goomenaLink & "     " & goomenaLink & "     " & goomenaLink & "     " & goomenaLink & "     " & goomenaLink & "     " & goomenaLink & "     " & goomenaLink, o, transBrush, New PointF(25, __letter_pos))
                                    gp.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceCopy

                                    Using br = New SolidBrush(Color.FromArgb(0, 255, 255, 255))
                                        gp.FillRectangle(br, New Rectangle(0, 0, 3000, 52))
                                        gp.FillRectangle(br, New Rectangle(0, 152, 3000, 170))
                                    End Using
                                End Using
                            End Using
                            Using rotim As Image = RotateImage(logoImage, New PointF(0, 0), 1.0F)
                                Using o As New Bitmap(rotim)
                                    Using im As Image = SetImageOpacity(o, __opacity)


                                        Using image23 As Image = New Bitmap(im, New Size(width, height))
                                            Dim picBoxIm As Image = picMain.Image
                                            Try
                                                If ({System.Drawing.Imaging.PixelFormat.Format1bppIndexed,
                                                     System.Drawing.Imaging.PixelFormat.Format4bppIndexed,
                                                     System.Drawing.Imaging.PixelFormat.Format8bppIndexed,
                                                     System.Drawing.Imaging.PixelFormat.Indexed}.Contains(picBoxIm.PixelFormat)) Then
                                                    picBoxIm = GetUnindexedImage(picBoxIm)
                                                End If

                                                Using grfx As Graphics = Graphics.FromImage(picBoxIm)
                                                    grfx.InterpolationMode = Drawing2D.InterpolationMode.HighQualityBicubic
                                                    grfx.DrawImage(image23, -10, CInt(Math.Truncate(picMain.Image.Height * __position)))
                                                End Using
                                            Catch ex As Exception
                                                ShowError("an error occured with this image..")
                                            End Try
                                            picMain.Image = picBoxIm
                                            'End Using
                                        End Using
                                    End Using
                                End Using
                            End Using
                        End Using
                    End Using
                End Using






            Else
                Dim height As Integer = CInt(Math.Truncate(170 * __resize))
                Dim width As Integer = CInt(Math.Truncate(3000 * __resize))
                Using logoImage As New Bitmap(3000, height)
                    Using image As Image = New Bitmap(My.Resources.logossss22)
                        Using gp As Graphics = Graphics.FromImage(logoImage)
                            gp.InterpolationMode = Drawing2D.InterpolationMode.HighQualityBicubic
                            ' kanoume repeat tin eikona mexri na gemisei 1920 pixels
                            Dim x As Integer = 0
                            While x <= logoImage.Width - image.Width
                                gp.DrawImage(image, New Point(x, 0))
                                x += image.Width
                            End While
                            ' meta grafoume to link katw apo to logotupo
                            Using g As Graphics = Graphics.FromImage(logoImage)
                                g.InterpolationMode = Drawing2D.InterpolationMode.HighQualityBicubic
                                Using transBrush As New SolidBrush(Color.FromArgb(180, 150, 150, 150))

                                    Using o As New Font("Tahoma", __letter_size)



                                        g.DrawString(goomenaLink & "     " & goomenaLink & "     " & goomenaLink & "     " & goomenaLink & "     " & goomenaLink & "     " & goomenaLink & "     " & goomenaLink & "     " & goomenaLink & "     " & goomenaLink, o, transBrush, New PointF(25, __letter_pos))
                                        g.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceCopy
                                        Using br = New SolidBrush(Color.FromArgb(0, 255, 255, 255))
                                            g.FillRectangle(br, New Rectangle(0, 0, 3000, 52))
                                            g.FillRectangle(br, New Rectangle(0, 152, 3000, 170))
                                        End Using
                                    End Using
                                End Using
                            End Using
                        End Using
                    End Using
                    ' tis vazoume transparent deksia kai aristera apo ta grammaa

                    Using bitmapPicturebox1 As New Bitmap(logoImage)


                        bitmapPicturebox1.RotateFlip(RotateFlipType.Rotate270FlipNone)

                        ' tin kanoume transparent
                        Using o As New Bitmap(bitmapPicturebox1)


                            Using im2 As Image = SetImageOpacity(o, __opacity)


                                ' picMain.Image = im2
                                Using rotim As Image = RotateImage(im2, New PointF(0, 0), -2.0F)
                                    Using image23 As Image = New Bitmap(rotim, New Size(height, width))
                                        Dim picBoxIm As Image = picMain.Image


                                        '  picMain.Image = rotim
                                        Using p As New Bitmap(picMain.Image.Size.Width, picMain.Image.Size.Height)
                                            '  picBoxIm = p
                                            Using grfx As Graphics = Graphics.FromImage(picBoxIm)
                                                grfx.InterpolationMode = Drawing2D.InterpolationMode.HighQualityBicubic

                                                grfx.DrawImage(image23, -CInt(Math.Truncate(__position * 100)), -__ShiftingPos)
                                            End Using
                                        End Using

                                        picMain.Image = picBoxIm
                                        picMain.Invalidate()
                                    End Using
                                End Using

                            End Using
                        End Using
                    End Using
                End Using






            End If
        End Sub

        Private Sub trackBarResize_Scroll(sender As Object, e As EventArgs) Handles trackBarResize.EditValueChanged
            __resize = CDbl(trackBarResize.Value) / 100
            SetWatermarkOnPicture()
            btnStoreSettings.Enabled = True
        End Sub

        Private Sub btnStoreSettings_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnStoreSettings.ItemClick
            ' swzoume tis rithmiseis

            If (__SaveSettingsUsingEvent = True) Then

                prgPanel1.Visible = True
                prgPanel1.Enabled = True

                __CurrentSettings.Position = __position
                __CurrentSettings.resize = __resize
                __CurrentSettings.ShiftingPos = __ShiftingPos
                __CurrentSettings.letter_size = __letter_size
                __CurrentSettings.letter_pos = __letter_pos

                Dim args As New SavingSettingsEventArgs(__CurrentSettings)
                RaiseEvent SavingSettings(Me, args)
                prgPanel1.Visible = False

            Else

                If btnStoreSettings.Caption = "Keep" Then
                    Try
                        Using dt = New DataSet1TableAdapters.WatermarkConfiguration1TableAdapter()
                            Using con As New SqlConnection(__connString)
                                dt.Connection = con
                                Using firstTable As New DataSet1.WatermarkConfiguration1DataTable()
                                    dt.GetData(picMain.Image.Width, picMain.Image.Height, __position, __resize, __ShiftingPos, __letter_pos, __letter_size)
                                    btnStoreSettings.Caption = "Update"
                                End Using
                            End Using
                        End Using
                       
                        
                    Catch ex As Exception

                    End Try
                ElseIf btnStoreSettings.Caption = "Update" Then

                    Using dt = New DataSet1TableAdapters.WatermarkConfiguration2TableAdapter()
                        Using con As New SqlConnection(__connString)
                            dt.Connection = con
                            Using firstTable As New DataSet1.WatermarkConfiguration2DataTable()
                                dt.GetData(__position, __resize, __ShiftingPos, __letter_size, __letter_pos, picMain.Image.Width, picMain.Image.Height)
                            End Using
                        End Using
                    End Using
                End If


                Using immm As Image = picMain.Image


                    'pictureBox1.Image.Dispose();

                    If chkPreserve.Checked Then
                        Try
                            If String.IsNullOrWhiteSpace(__pathToSave) Then
                                immm.Save(__imageName, ImageFormat.Png)
                            Else
                                immm.Save(__pathToSave & "\" & __imageName & ".jpg", ImageFormat.Png)
                            End If
                        Catch ex As Exception
                            ShowError("cannot save file..")
                        End Try

                    Else
                    End If
                End Using
            End If

            btnStoreSettings.Enabled = False

        End Sub

        Private Sub trackBarShifting_Scroll(sender As Object, e As EventArgs) Handles trackBarShifting.EditValueChanged
            __ShiftingPos = trackBarShifting.Value
            SetWatermarkOnPicture()
            btnStoreSettings.Enabled = True
        End Sub

        Private Sub trackBarOpacity_Scroll(sender As Object, e As EventArgs) Handles trackBarOpacity.EditValueChanged
            __opacity = CSng(CDbl(trackBarOpacity.Value) / 100)
            SetWatermarkOnPicture()
            btnStoreSettings.Enabled = True
        End Sub

        Private Sub btnSaveLocation_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnSaveLocation.ItemClick
            Using fbd As New FolderBrowserDialog()

                Dim result As DialogResult = fbd.ShowDialog()

                txtSavePath.Text = InlineAssignHelper(__pathToSave, fbd.SelectedPath)

            End Using
        End Sub

        Private Sub pictureBox4_Click(sender As Object, e As EventArgs)

        End Sub

        Private Sub trackBarLinkPos_Scroll(sender As Object, e As EventArgs) Handles trackBarLinkPos.EditValueChanged
            __letter_pos = trackBarLinkPos.Value
            SetWatermarkOnPicture()
            btnStoreSettings.Enabled = True
        End Sub

        Private Sub trackBarLetterSize_Scroll(sender As Object, e As EventArgs) Handles trackBarLetterSize.EditValueChanged
            __letter_size = trackBarLetterSize.Value
            SetWatermarkOnPicture()
            btnStoreSettings.Enabled = True
        End Sub
        Private Shared Function InlineAssignHelper(Of T)(ByRef target As T, value As T) As T
            target = value
            Return value
        End Function


        Private Sub btnSaveImage_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnSaveImage.ItemClick
            Dim Args As New SavingImageEventArgs(__PhotoID, __filePath, picMain.Image)
            prgPanel1.Visible = True
            prgPanel1.Enabled = True
            RaiseEvent SavingImage(Me, Args)
            prgPanel1.Visible = False
            CloseWin()
        End Sub

        Public Sub CloseWin()
            Try
                picMain.Visible = False
                picMain.Image.Dispose()
                picMain.Image = Nothing
            Catch
            End Try

            Try
                __tempIm.Dispose()
                __tempIm = Nothing
            Catch
            End Try


            Try
                Me.Close()
                Me.Dispose()
            Catch
            End Try
        End Sub
        '        Sub CenterChild(Parent As Form, Child As Control)
        '            Dim iTop As Integer
        '            Dim iLeft As Integer
        '            If Parent.WindowState <> 0 Then Exit Sub
        'iTop = ((Parent.Height - Child.Height) 2)
        'iLeft = ((Parent.Width - Child.Width) 2)
        '            Child.Move(iLeft, iTop)
        '        End Sub

        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")> Public Sub LoadImage(path As String, ByRef settings As WatermarkSettings, photoID As Long)
            Try

                __PhotoID = photoID
                __SaveSettingsUsingEvent = True
                __SaveImageUsingEvent = True
                SetUI_OnLoad_FromExternalApp()
                txtGoomenaLink.Text = settings.URL

                __filePath = path
                Dim imageNameValues As String() = __filePath.Split(New String() {"\"}, StringSplitOptions.RemoveEmptyEntries)
                __imageName = "watermarked_" & imageNameValues(imageNameValues.Length - 1)
                Try

                    'Bitmap orig = new Bitmap(@"c:\temp\24bpp.bmp");
                    'Bitmap clone = new Bitmap(orig.Width, orig.Height, System.Drawing.Imaging.PixelFormat.Format32bppPArgb);
                    'using (Graphics gr = Graphics.FromImage(clone)) {
                    '    gr.DrawImage(orig, new Rectangle(0, 0, clone.Width, clone.Height));
                    '}

                    Dim orig As New Bitmap(__filePath)
                    If ({System.Drawing.Imaging.PixelFormat.Format1bppIndexed,
                         System.Drawing.Imaging.PixelFormat.Format4bppIndexed,
                         System.Drawing.Imaging.PixelFormat.Format8bppIndexed,
                         System.Drawing.Imaging.PixelFormat.Indexed}.Contains(orig.PixelFormat)) Then
                        __tempIm = GetUnindexedImage(orig)
                    Else
                        __tempIm = orig
                    End If

                    ''fix intended for indexed images
                    '__tempIm = New Bitmap(orig.Width, orig.Height, System.Drawing.Imaging.PixelFormat.Format32bppPArgb)
                    'Using grf As Graphics = Graphics.FromImage(__tempIm)
                    '    grf.InterpolationMode = Drawing2D.InterpolationMode.HighQualityBicubic
                    '    grf.DrawImage(orig, New Rectangle(0, 0, __tempIm.Width, __tempIm.Height))
                    '    'grf.DrawImage(__tempIm, 0, 0)
                    'End Using

                    picMain.Image = __tempIm
                Catch ex As IOException
                End Try

                ' retrieve from DB
                If String.IsNullOrWhiteSpace(__filePath) Then
                    Return
                End If


                Try
                    __CurrentSettings = settings
                    __position = settings.Position
                    __resize = settings.resize
                    __ShiftingPos = settings.ShiftingPos
                    __letter_size = settings.letter_size
                    __letter_pos = settings.letter_pos

                    trackBarPosition.Value = CInt(Math.Truncate(__position * 100))
                    trackBarResize.Value = CInt(Math.Truncate(__resize * 100))
                    trackBarShifting.Value = __ShiftingPos
                    trackBarLinkPos.Value = __letter_pos
                    trackBarLetterSize.Value = __letter_size

                    'btnStoreSettings.Text = "Update"
                    'lblErrors.Text = "There is already a record for this"
                    'lblErrors.Visible = True
                Catch ex As Exception
                    ' den eixe kati na diavasei vazoume tis default times

                    __position = 0.77
                    __resize = 1
                    __ShiftingPos = 300
                    __letter_size = 14
                    __letter_pos = 120
                    'lblErrors.Text = "There is no record for this"
                    'btnStoreSettings.Text = "Keep"
                    'lblErrors.Visible = True
                End Try

                SetWatermarkOnPicture()
            Catch ex As Exception
                ShowError(ex.Message)
            End Try

        End Sub

        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")> Function GetUnindexedImage(ByRef imgSource As Bitmap) As Bitmap
            'fix intended for indexed images
            Dim newImg = New Bitmap(imgSource.Width, imgSource.Height, System.Drawing.Imaging.PixelFormat.Format32bppPArgb)
            Using grf As Graphics = Graphics.FromImage(newImg)
                grf.InterpolationMode = Drawing2D.InterpolationMode.HighQualityBicubic
                grf.DrawImage(imgSource, New Rectangle(0, 0, newImg.Width, newImg.Height))
                'grf.DrawImage(__tempIm, 0, 0)
            End Using
            Return newImg
        End Function


        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")> Function GetUnindexedImage(ByRef imgSource As Image) As Image
            'fix intended for indexed images
            Dim newImg As Image = New Bitmap(imgSource.Width, imgSource.Height, System.Drawing.Imaging.PixelFormat.Format32bppPArgb)
            Using grf As Graphics = Graphics.FromImage(newImg)
                grf.InterpolationMode = Drawing2D.InterpolationMode.HighQualityBicubic
                grf.DrawImage(imgSource, New Rectangle(0, 0, newImg.Width, newImg.Height))
                'grf.DrawImage(__tempIm, 0, 0)
            End Using
            Return newImg
        End Function

        Private Sub SetUI_OnLoad_FromExternalApp()
            lblPathtoSave.Visible = False
            txtSavePath.Visible = False
            chkPreserve.Visible = False
            btnOpenImage.Visibility = DevExpress.XtraBars.BarItemVisibility.Never

            onDelayedShown_ResetUI()
        End Sub



        Private Sub onDelayedShown_SetUI()
            If (__IsDelayedLoad) Then

                For Each ctl As Control In Me.Controls
                    If (ctl.Enabled) Then
                        ctl.Enabled = False
                        listOfDisabledControls.Add(ctl.Name)
                        If (ctl.HasChildren) Then
                            For Each ctl1 As Control In ctl.Controls
                                If (ctl1.Enabled) Then
                                    ctl1.Enabled = False
                                    listOfDisabledControls.Add(ctl1.Name)
                                End If
                            Next
                        End If
                    End If
                Next

                prgPanel1.Enabled = True
                prgPanel1.Visible = True
            End If
        End Sub


        Private Sub onDelayedShown_ResetUI()
            If (__IsDelayedLoad) Then
                prgPanel1.Enabled = False
                prgPanel1.Visible = False

                For Each ctl As Control In Me.Controls
                    If (listOfDisabledControls.Contains(ctl.Name)) Then
                        ctl.Enabled = True

                        If (ctl.HasChildren) Then
                            For Each ctl1 As Control In ctl.Controls
                                If (listOfDisabledControls.Contains(ctl1.Name)) Then
                                    ctl1.Enabled = True
                                End If
                            Next
                        End If

                    End If
                Next

            End If
        End Sub




        Private Sub btnClose_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnClose.ItemClick
            Me.CloseWin()
        End Sub


        Sub ShowError(msg As String)
            lblErrors.Text = msg
            lblErrors.Visible = True
            picError.Visible = True
        End Sub

        Sub HideError()
            lblErrors.Text = ""
            lblErrors.Visible = False
            picError.Visible = False
        End Sub

    End Class
End Namespace

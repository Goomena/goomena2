﻿using AForge.Imaging;
using AForge.Imaging.Filters;
using FilesProxy.DownloadSites.Web;
using Google.API.Search;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace GoomenaImageCompare
{
    class URLCrawlerThread
    {

        public delegate void AlertEventHandler(Object sender, AlertEventArgs e);
        public event AlertEventHandler CallAlert;


        public delegate void ProgressChangedEventHandler(Object sender, ProgressChangedEventArgs e);
        public event ProgressChangedEventHandler ProgressChanged;

        Profil profil = Profil.Default;
        String html = String.Empty;
        int concurrentNulls = 0; // poses sunexomenes fores mas epistrepsei null, prepei na stamatisoume gia ligo
        int nullThreshold = 5; // sta 5 null kanoume pause


        public URLCrawlerThread() {

        }

        public void run() // edw kanoume parse to url kai 
        {

            try {

                int counter = profil.getprofilCount();
                foreach (String profilUrl in profil.returnAllProfilUrls()) {
                    if (ProgressChanged != null)
                        ProgressChanged(this, new ProgressChangedEventArgs() { CurrentUrl = profilUrl });

                    WriteToLOG("PROFIL: " + profilUrl); // grafoume to PROFIL
                    try {
                        ImagesStorage.Default.clearProfilImages();
                        ImagesStorage.Default.clearDictionary();
                        ImagesStorage.Default.clearGoogleImagesURL();
                        ImagesStorage.Default.clearAllGoogleImage();
                        profil.clearprofilImageURLs();
                        getImagesForTheProfil(profilUrl); // pernoume san bitmaps tis eikones profil kai ta url tis kathe mias
                        Thread.Sleep(5000);

                    }
                    catch (Exception) {

                    }
                    foreach (String profilImageURL in profil.returnAllprofilImageURLs()) { // gia kathe mia eikona profil


                        Globals.Default.setCanContinue(false);
                        getSimilarImageFromURL(profilImageURL); // pernoume tis paromoies eikones apo tin google analoga me to URL


                        ////int __counter = 0;
                        //do
                        //{
                        //    //System.Threading.Thread.Sleep(1000);
                        //    //__counter += 1000;
                        //    //if (__counter > 90000) Globals.Default.setCanContinue(true);
                        //} while (!Globals.Default.getCanContinue());


                    }


                    Thread.Sleep(1000);
                    // edw ginetai i sugkrish kathe mias eikonas profil me kathe eikona apo to dictionary
                    Thread.Sleep(500);
                    Dictionary<Bitmap, string> tempD = ImagesStorage.Default.returnDictionary();

                    // afairoume ta diplwtima
                    Dictionary<Bitmap, string> uniqDictionary = new Dictionary<Bitmap, string>();
                    Thread.Sleep(5000);
                    foreach (KeyValuePair<Bitmap, string> entry in tempD) {
                        if (!uniqDictionary.ContainsValue(entry.Value))
                            uniqDictionary.Add(entry.Key, entry.Value);
                    }

                    Globals.Default.setmaxCompares(profil.getCountprofilImageURL() * uniqDictionary.Count);
                    Globals.Default.setcurrentCompares(0);
                    Globals.Default.setcompareStarted(true);
                    Globals.Default.setcurrentProfilCompared(profilUrl);
                    int count = 0;
                    foreach (String profilImageUrl in profil.returnAllprofilImageURLs()) // twra gia kathe mia eikona profil
                    {
                        // gia kathe mia eikona paromoia apo ti google



                        Thread.Sleep(5000);
                        foreach (KeyValuePair<Bitmap, string> entry in uniqDictionary) {
                            count++;
                            Globals.Default.setcurrentCompares(count);
                            float similarity = templateMatching(ImagesStorage.Default.BitmapFromWeb(profilImageUrl), entry.Key, entry.Value);
                            if (similarity > 0.7) {
                                this.addlastData(new uuiSimilarUrlItem
                                {
                                    InputUrl = profilImageUrl,
                                    FoundUrl = entry.Value,
                                    Similarity = similarity
                                });


                                String temp = String.Empty;
                                temp = "profilURL: " + profilImageUrl + " similarImageURL: " + entry.Value + " similarity: " + similarity.ToString();
                                WriteToLOG(temp);
                            }

                        }
                    }
                    Globals.Default.setcompareStarted(false);
                    // telos me auto to profil pame se alla
                    WriteLineToLOG();
                }

                WriteEndOfLOG(null);
            }
            catch (Exception ex) {
                WriteEndOfLOG(ex.ToString());
            }
            finally {
                Globals.Default.setEND(true);

                if (CallAlert != null) {
                    AlertEventArgs alertEventArgs = new AlertEventArgs();
                    alertEventArgs.uuiData = true;
                    alertEventArgs.uuiListData = this.getlastData();
                    CallAlert(this, alertEventArgs);
                }
            }
        }


        private void WriteToLOG(string text) {
            if (!string.IsNullOrEmpty(text))
                try {
                    using (StreamWriter sw = File.AppendText(Globals.Default.getoutputFilePath())) {
                        sw.WriteLine(text);
                        sw.Flush();
                        sw.Close();
                        sw.Dispose();
                    }
                }
                catch (Exception) { }
        }

        private void WriteLineToLOG() {
            try {
                using (StreamWriter sw = File.AppendText(Globals.Default.getoutputFilePath())) {
                    sw.WriteLine();
                    sw.WriteLine("_______________________________________________________________________");
                    sw.WriteLine();
                    sw.Flush();
                    sw.Close();
                    sw.Dispose();
                }
            }
            catch (Exception) { }
        }
        private void WriteEndOfLOG(string exceptionString) {
            try {
                using (StreamWriter sw = File.AppendText(Globals.Default.getoutputFilePath())) {
                    if (!string.IsNullOrEmpty(exceptionString))
                        sw.WriteLine(exceptionString);
                    sw.WriteLine();
                    sw.WriteLine("######################################### END OF LOG ############################################################");
                    sw.WriteLine();
                    sw.Flush();
                    sw.Close();
                    sw.Dispose();
                }

            }
            catch (Exception) { }
        }

        List<uuiSimilarUrlItem> lastData = new List<uuiSimilarUrlItem>();
        public List<uuiSimilarUrlItem> getlastData() {
            return lastData;
        }

        public void setlastData(List<uuiSimilarUrlItem> var) {
            lastData = var;
        }
        public void addlastData(uuiSimilarUrlItem var) {
            lastData.Add(var);
        }


        private float templateMatching(Bitmap im1, Bitmap im2, String imUrl) {
            try {
                long size1 = im1.Height * im1.Width;
                long size2 = im2.Height * im2.Width;
                if (size1 < size2) {
                    im2 = resizeImage(im2, im1.Width, im1.Height);
                }
                else {
                    im1 = resizeImage(im1, im2.Width, im2.Height);
                }
                //Histogram hist = new Histogram();

                Grayscale filter = new Grayscale(0.2125, 0.7154, 0.0721);
                // apply the filter
                im1 = filter.Apply(im1);
                im2 = filter.Apply(im2);
                //histCompare(im1, im2);
                // create template matching algorithm's instance

                ExhaustiveTemplateMatching tm = new ExhaustiveTemplateMatching(0.75f);
                // find all matchings with specified above similarity

                TemplateMatch[] matchings = tm.ProcessImage(im1, im2);
                if (matchings != null && matchings.Length > 0) {
                    return matchings[0].Similarity;
                }
            }
            catch (Exception) { }
            return 0;
        }


        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        private Bitmap resizeImage(Bitmap srcImage, int newWidth, int newHeight) {
            Bitmap newImage = new Bitmap(newWidth, newHeight);
            using (Graphics gr = Graphics.FromImage(newImage)) {
                gr.SmoothingMode = SmoothingMode.HighQuality;
                gr.InterpolationMode = InterpolationMode.HighQualityBicubic;
                gr.PixelOffsetMode = PixelOffsetMode.HighQuality;
                gr.DrawImage(srcImage, new Rectangle(0, 0, newWidth, newHeight));

            }
            return newImage;
        }

        private int histCompare(Bitmap image1, Bitmap image2) // einai thread p sugkrinei tin mia eikona profil kai mia allh apo ti google
        {
            //Histogram hist = new Histogram();
            //Bitmap image1 = ImagesStorage.Default.BitmapFromWeb(imUrl);

            //int percentage = hist.compare(image1, image2);

            //if (percentage >= 50) // ta vazoume sto log file
            //{
            //    return percentage;
            //}
            //else if (percentage >= 30)
            // {
            //return percentage;
            // }
            return 1;
        }

        private void histCompare(String imUrl, Bitmap image2) // einai thread p sugkrinei tin mia eikona profil kai mia allh apo ti google
        {
            //Histogram hist = new Histogram();
            //Bitmap image1 = ImagesStorage.Default.BitmapFromWeb(imUrl);

            //int percentage = hist.compare(image1, image2);

            //if (percentage > 60) // ta vazoume sto log file
            //{

            //}
        }
        private void getSimilarImageFromURL(string profilImageURL) {
            Thread.Sleep(Globals.Default.getDelayForGoogleRequest()); // delay gia to request sti google
            String keyword = SBISharp.SBIHelper.SearchByImage(profilImageURL);
            if (String.IsNullOrWhiteSpace(keyword)) {
                concurrentNulls++;
                if (concurrentNulls == nullThreshold) {
                    Thread.Sleep(10000);
                }
                Globals.Default.setCanContinue(true);
                return;
            }



            GimageSearchClient client = new GimageSearchClient("http://www.google.com");

            IList<IImageResult> results = null;
            try {
                results = client.Search(keyword, 20);
            }
            catch (Exception ex) {
                if (ex.Message == "[response status:503]qps rate exceeded") {
                    System.Threading.Thread.Sleep(30000);
                    try {
                        results = client.Search(keyword, 20);
                    }
                    catch (Exception) { }
                }
            }

            if (client == null || results == null) {
                Globals.Default.setCanContinue(true);
                return;
            }

            foreach (IImageResult result in results) {
                //Console.WriteLine("[{0}] {1} => {2}", result.Title, result.Content, result.Url);
                ImagesStorage.Default.setGoogleImagesURL(result.Url);
                ImagesStorage.Default.push(result.Url); // vazoume stin oura ta url twn eikonwn 
                //ImagesStorage.Default.setGoogleImage(ImagesStorage.Default.BitmapFromWeb(result.Url));
            }
            Thread.Sleep(300);
            if (ImagesStorage.Default.queueSize() > 0) {
                Globals.Default.setCanContinue(false);
                Globals.Default.setGoogleImagesLoaded(false);
                if (ImagesStorage.Default.queueSize() == 1) {
                    for (int threadNumber = 0; threadNumber < 1; threadNumber++) { // dhmiourgoume threads p tha katevasoun tis paromoies eikones apo tin oura
                        ParseGoogleImages imageParser = new ParseGoogleImages();
                        imageParser.getImages();
                        //Thread tid = new Thread(new ThreadStart(imageParser.getImages));

                        //try
                        //{
                        //    tid.Start();
                        //    Thread.Sleep(200);
                        //}
                        //catch (Exception ex)
                        //{ }
                    }
                }
                else {
                    for (int threadNumber = 0; threadNumber < ImagesStorage.Default.queueSize() / 2; threadNumber++) { // dhmiourgoume threads p tha katevasoun tis paromoies eikones apo tin oura
                        ParseGoogleImages imageParser = new ParseGoogleImages();
                        imageParser.getImages();
                        //Thread tid = new Thread(new ThreadStart(imageParser.getImages));

                        //try
                        //{
                        //    tid.Start();
                        //    Thread.Sleep(200);
                        //}
                        //catch (Exception ex)
                        //{ }
                    }
                }
            }
            else {
                Globals.Default.setCanContinue(true);
            }

        }

        private void getImagesForTheProfil(String url) {
            try {
                //ImagesStorage imStorage = new ImagesStorage();
                SimpleBrowser browser = new SimpleBrowser();
                browser.Navigate(new WebPageUrl(url));
                if (browser.ResponseContentType.Contains("image/")) {
                    profil.setProfilImageURL(url);
                    ImagesStorage.Default.setProfilImage(ImagesStorage.Default.BitmapFromWeb(url));
                }
                else {
                    html = browser.LastPageText;
                    getImagesFromHtml();
                    //Thread t = new Thread(new ThreadStart(getImagesFromHtml)); // h sunarthsh auth kanei parse ta url twn eikonwn apo to html source
                    //t.Start();
                    //t.Join();
                    Globals.Default.setProfilImagesloaded(true);
                }
            }
            catch (Exception) { }
            //HtmlAgilityPack.HtmlDocument htmlDoc = new HtmlAgilityPack.HtmlDocument();
            //htmlDoc.Load(html);
        }

        private void getImagesFromHtml() // edw pernoume tin html kai prosthetoume tis eikones p vriskoume
        {
            try {
                //ImagesStorage imStorage = new ImagesStorage();

                int initialIndex = -1;
                initialIndex = html.IndexOf("/thumbs/");

                if (initialIndex == -1) // den vrikame url..
                {
                    return;
                }

                for (int i = initialIndex; i < html.Length; i++) {

                    String tempString = html.Substring(i, 8);
                    String url = "";
                    Boolean isOK = false;
                    if (tempString == "/thumbs/") // simainei oti vrikame url
                    {
                        int left = -1;
                        int right = i;
                        for (left = i; left > 1; left--) {

                            if (html.Substring(left, 1) == "\"") // pianoume ta aristera oria tou url
                            {
                                for (right = left + 1; right < html.Length; right++) // pianoume ta deksia oria tou url
                                {
                                    if (html.Substring(right, 1) == "\"") // pianoume ta aristera oria tou url
                                    {
                                        break;
                                    }
                                    else {
                                        url += html.Substring(right, 1);
                                        isOK = true;
                                    }
                                }
                            }
                            if (isOK)
                                break;
                        }

                        url = url.Replace("/thumbs", "");
                        profil.setProfilImageURL(url);
                        ImagesStorage.Default.setProfilImage(ImagesStorage.Default.BitmapFromWeb(url));
                    }
                }
            }
            catch (Exception) { }
        }



    }
}

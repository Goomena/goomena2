﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoomenaImageCompare
{
    class Profil
    {


        static Profil __Default;
        public static Profil Default
        {
            get
            {
                if (__Default == null) __Default = new Profil();
                return __Default;
            }
        }


        LinkedList<String> profilURLs = new LinkedList<string>();
        LinkedList<String> profilImageUrl = new LinkedList<String>();

        #region "profilURLs"
        public void setProfilURL(String url)
        {
            profilURLs.AddFirst(url);
        }

        public String getAndRemoveProfilURL()
        {
            String first = profilURLs.First();
            profilURLs.RemoveFirst();
            return first;
        }
        public LinkedList<String> returnAllProfilUrls()
        {
            return profilURLs;
        }
        public int getprofilCount()
        {
            return profilURLs.Count;
        }
        public void clearProfilUrls()
        {
            try
            {
                profilURLs.Clear();
            }
            catch (Exception) { }
        }
        #endregion "profilURLs"



        #region "profilImageUrl"
        public int getCountprofilImageURL()
        {
            return profilImageUrl.Count();
        }

        public void setProfilImageURL(String url)
        {
            profilImageUrl.AddFirst(url);
        }
        public String getAndRemoveProfilImageURL()
        {
            String first = profilImageUrl.First();
            profilImageUrl.RemoveFirst();
            return first;
        }
        public LinkedList<String> returnAllprofilImageURLs()
        {
            return profilImageUrl;
        }
        public int getProfilImageURLCount()
        {
            return profilImageUrl.Count();
        }

        public void clearprofilImageURLs()
        {
            try
            {
                profilImageUrl.Clear();
            }
            catch (Exception) { }
        }
        #endregion "profilImageUrl"

    }
}

﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoomenaImageCompare
{
    class ParseGoogleImages
    {
        public ParseGoogleImages() {
        }

        public void getImages() // afairoun ena ena ta url apo tin oura kai gemizoun to linked list me tis eikones
        {
            while (ImagesStorage.Default.queueSize() != 0)
            {
                int count = 0;
            again:
                try
                {
                    String url = ImagesStorage.Default.pop();
                    Bitmap tempIm = ImagesStorage.Default.BitmapFromWeb(url);
                    if (tempIm != null)
                    {
                        ImagesStorage.Default.setGoogleImage(tempIm);
                        ImagesStorage.Default.insertIntoDictionary(tempIm, url);
                    }
                }
                catch (Exception)
                {
                    if (count != 2)
                    {
                        count++;
                        goto again;
                    }

                }
            }
            Globals.Default.setCanContinue(true);
            Globals.Default.setGoogleImagesLoaded(true);
        }
    }
}

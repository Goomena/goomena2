﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace GoomenaImageCompare
{
    class ImagesStorage
    {
        static ImagesStorage __Default;
        public static ImagesStorage Default
        {
            get
            {
                if (__Default == null) __Default = new ImagesStorage();
                return __Default;
            }
        }

        LinkedList<Bitmap> profilImages = new LinkedList<Bitmap>();
        LinkedList<String> queue = new LinkedList<String>(); // tha mpoun prosorina ta urls twn eikonwn apo ti google
        LinkedList<String> googleImagesURL = new LinkedList<string>();
        Dictionary<Bitmap, String> googleImageDictionary = new Dictionary<Bitmap, String>(); // googleImage, url tis eikonas
        LinkedList<Bitmap> googleSimilarImages = new LinkedList<Bitmap>();





        #region "profilImages"
        public LinkedList<Bitmap> returnAllprofilimages()
        {
            return profilImages;
        }

        public void setProfilImage(Bitmap image)
        {
            if (image != null)
            {
                profilImages.AddLast(image);
            }
        }
        public int getProfilImagesCount()
        {
            return profilImages.Count();
        }
        public void clearProfilImages()
        {
            try
            {
                profilImages.Clear();
            }
            catch (Exception) { }
        }
        #endregion




        #region "queue"

        public void push(String var)
        {
            queue.AddFirst(var);
        }

        public String pop()
        {

            if (queue.Count > 0)
            {
                String first = queue.First();
                queue.RemoveFirst();
                return first;
            }
            return null;
        }

        public int queueSize()
        {
            return queue.Count();
        }
        #endregion "queue"



        #region "googleImagesURL"
        public void setGoogleImagesURL(String url)
        {
            googleImagesURL.AddFirst(url);
        }

        public void clearGoogleImagesURL()
        {
            try
            {
                googleImagesURL.Clear();
            }
            catch (Exception ) { }
        }
        #endregion "googleImagesURL"



        #region "googleImagesURL"
        public void insertIntoDictionary(Bitmap im, String url)
        {
            googleImageDictionary.Add(im, url);
        }

        public Dictionary<Bitmap, String> returnDictionary()
        {
            return googleImageDictionary;
        }

        public Boolean clearDictionary()
        {
            try
            {
                googleImageDictionary.Clear();
                return true;
            }
            catch (Exception) { return false; }
        }
        #endregion "googleImagesURL"



        #region "googleImagesURL"
        public void setGoogleImage(Bitmap image)
        {
            googleSimilarImages.AddFirst(image);
        }

        public LinkedList<Bitmap> returnAllGoogleImage()
        {
            return googleSimilarImages;
        }

        public void clearAllGoogleImage()
        {
            try
            {
                googleSimilarImages.Clear();
            }
            catch (Exception) { }
        }
        #endregion "googleImagesURL"


        public Bitmap BitmapFromWeb(string URL)
        {
            try
            {
                string filename = URL.Remove(0, URL.LastIndexOf("/") + 1);
                // create a web request to the url of the image
                HttpWebRequest myRequest = (HttpWebRequest)WebRequest.Create(URL);
                // set the method to GET to get the image
                myRequest.Method = "GET";
                // get the response from the webpage
                HttpWebResponse myResponse = (HttpWebResponse)myRequest.GetResponse();
                // create a bitmap from the stream of the response
                Bitmap bmp = new Bitmap(myResponse.GetResponseStream());
                // close off the stream and the response
                myResponse.Close();
                return bmp;
            }
            catch (Exception)
            {
                return null; // if for some reason we couldn't get to image, we return null
            }
        }


    }
}

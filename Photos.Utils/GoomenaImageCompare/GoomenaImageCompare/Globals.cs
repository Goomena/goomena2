﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace GoomenaImageCompare
{
    class Globals
    {
        static Globals __Default;
        public static Globals Default
        {
            get
            {
                if (__Default == null) __Default = new Globals();
                return __Default;
            }
        }

        Boolean END = false;
        public Boolean getEND()
        {
            return END;
        }
        public void setEND(Boolean var)
        {
            END = var;
        }

        String outputFilePath = "";
        public String getoutputFilePath()
        {
            return outputFilePath;
        }
        public void setoutputFilePath(String var)
        {
            outputFilePath = var;
        }

        Boolean STOP = false; // an einai true stamataei to programma
        public Boolean getSTOP()
        {
            return STOP;
        }
        public void setSTOP(Boolean var)
        {
            STOP = var;
        }


        int googleRequestDelay = 5000; // default timh
        public int getDelayForGoogleRequest()
        {
            return googleRequestDelay;
        }
        public void setDelayForGoogleRequest(int var)
        {
            googleRequestDelay = var;
        }


        String currentProfilCompared = "";
        public void setcurrentProfilCompared(String var)
        {
            currentProfilCompared = var;
        }
        public String getcurrentProfilCompared()
        {
            return currentProfilCompared;
        }


        Boolean compareStarted = false;
        public void setcompareStarted(Boolean var)
        {
            compareStarted = var;
        }
        public Boolean getcompareStarted()
        {
            return compareStarted;
        }

        int maxCompares = 0;
        public int getmaxCompares()
        {
            return maxCompares;
        }
        public void setmaxCompares(int var)
        {
            maxCompares = var;
        }

        int currentCompares = 0;
        public int getcurrentCompares()
        {
            return currentCompares;
        }
        public void setcurrentCompares(int var)
        {
            currentCompares = var;
        }



        Boolean profilImagesloaded = false;
        public void setProfilImagesloaded(Boolean var)
        {

            profilImagesloaded = var;
        }
        public Boolean getProfilImagesloaded()
        {
            return profilImagesloaded;
        }


        Boolean similarImagesLoaded = false; // otan fortosoume apo ti google paromoies eikones tote true
        public void setSimilarImagesLoaded(Boolean var)
        {

            similarImagesLoaded = var;
        }
        public Boolean getSimilarImagesLoaded()
        {
            return similarImagesLoaded;
        }


        Boolean googleImagesLoaded = false;
        public Boolean getGoogleImagesLoaded()
        {
            return googleImagesLoaded;
        }
        public void setGoogleImagesLoaded(Boolean var)
        {
            googleImagesLoaded = var;
        }


        Boolean canContinue = false;
        public Boolean getCanContinue()
        {
            return canContinue;
        }
        public void setCanContinue(Boolean var)
        {
            canContinue = var;
        }

        Boolean canCompare = false;
        public Boolean getCanCompare()
        {
            return canCompare;
        }
        public void setCanCompare(Boolean var)
        {
            canCompare = var;
        }

    }
}

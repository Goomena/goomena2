﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GoomenaImageCompare
{
    public class ImageComparison
    {

        //Globals glbs = new Globals();
        //Profil profil = new Profil();
        System.Windows.Forms.Timer t = new System.Windows.Forms.Timer();
        // ################## overload contructors ########################

        object m_syncUserState = null;

        private void initTimer()
        {

            t.Interval = 5000; // specify interval time as you want
            t.Tick += new EventHandler(timer_Tick);
            t.Start();
        }
        public ImageComparison(String profilURLsFilePath)
        {
            Profil.Default.clearProfilUrls();
            Profil.Default.clearprofilImageURLs();

            string[] lines = System.IO.File.ReadAllLines(profilURLsFilePath);
            foreach (String line in lines)
            {
                Profil.Default.setProfilURL(line);
            }

            initTimer();
        }
        public ImageComparison(int googleRequestDelay, String profilURLsFilePath)
        {
            Profil.Default.clearProfilUrls();
            Profil.Default.clearprofilImageURLs();
            Globals.Default.setDelayForGoogleRequest(googleRequestDelay);

            string[] lines = System.IO.File.ReadAllLines(profilURLsFilePath);
            foreach (String line in lines)
            {
                Profil.Default.setProfilURL(line);

            }
            initTimer();
        }
        public ImageComparison(int googleRequestDelay, String profilURLsFilePath, String outputFilePath)
        {
            Profil.Default.clearProfilUrls();
            Profil.Default.clearprofilImageURLs();
            Globals.Default.setDelayForGoogleRequest(googleRequestDelay);

            string[] lines = System.IO.File.ReadAllLines(profilURLsFilePath);
            foreach (String line in lines)
            {
                Profil.Default.setProfilURL(line);

            }
            Globals.Default.setoutputFilePath(outputFilePath);
            initTimer();
        }
        public ImageComparison(int googleRequestDelay, List<String> profilURLsList, String outputFilePath)
        {
            Profil.Default.clearProfilUrls();
            Profil.Default.clearprofilImageURLs();
            Globals.Default.setDelayForGoogleRequest(googleRequestDelay);

            foreach (String line in profilURLsList)
            {
                Profil.Default.setProfilURL(line);

            }
            Globals.Default.setoutputFilePath(outputFilePath);
            initTimer();
        }
        //public ImageComparison(int googleRequestDelay, List<String> profilURLsList)
        //{
        //    Globals.Default.setDelayForGoogleRequest(googleRequestDelay);

        //    foreach (String line in profilURLsList)
        //    {
        //        Profil.Default.setProfilURL(line);

        //    }
        //    initTimer();
        //}
        // ============================ END overload contructors =====================

        // ============================ events =======================================

        void timer_Tick(object source, EventArgs e)
        {
            if (Globals.Default.getEND())
            {
                isRinging();
                Globals.Default.setEND(false);
            }
        }
        public delegate void AlertEventHandler(Object sender, AlertEventArgs e);
        public event AlertEventHandler CallAlert;

        public delegate void ProgressChangedEventHandler(Object sender, ProgressChangedEventArgs e);
        public event ProgressChangedEventHandler ProgressChanged;



        public void isRinging()
        {
            //AlertEventArgs alertEventArgs = new AlertEventArgs();
            //alertEventArgs.uuiData = true;
            //alertEventArgs.uuiListData = Globals.Default.getlastData();
            //CallAlert(this, alertEventArgs);
        }


        public void crawler_CallAlert(object sender, AlertEventArgs e)
        {
            //AlertEventArgs alertEventArgs = new AlertEventArgs();
            //alertEventArgs.uuiData = true;
            //alertEventArgs.uuiListData = Globals.Default.getlastData();
            t.Enabled = false;
            isRinging();
            e.UserState = m_syncUserState;
            if (CallAlert != null)
                CallAlert(this, e);
            Globals.Default.setEND(false);
        }


        public void crawler_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (ProgressChanged != null)
                ProgressChanged(this, e);
        }

        public void getResults()
        {
            getResults(null);
        }

        public void getResults(object userState)
        {
            m_syncUserState = userState;
            Globals.Default.setEND(false);
            URLCrawlerThread crawler = new URLCrawlerThread();
            crawler.CallAlert += new URLCrawlerThread.AlertEventHandler(crawler_CallAlert);
            crawler.ProgressChanged += new URLCrawlerThread.ProgressChangedEventHandler(crawler_ProgressChanged);
            crawler.run();
            //Thread tid = new Thread(new ThreadStart(crawler.run));

            //try
            //{
            //    tid.Start();
            //}
            //catch (Exception) { }
        }

        public void stop()
        {

        }
    }


    public class AlertEventArgs : EventArgs
    {
        #region AlertEventArgs Properties
        private Boolean _uui = false;
        List<uuiSimilarUrlItem> data = new List<uuiSimilarUrlItem>();
        #endregion

        #region Get/Set Properties
        public Boolean uuiData
        {
            get { return _uui; }
            set { _uui = value; }
        }

        public List<uuiSimilarUrlItem> uuiListData
        {
            get { return data; }
            set { data = value; }
        }

        public object UserState { get; internal set; }

        #endregion
    }


    public class ProgressChangedEventArgs : EventArgs
    {
        public string CurrentUrl { get; set; }
    }

    public class uuiSimilarUrlItem
    {
        public string InputUrl { get; set; }
        public string FoundUrl { get; set; }
        public float Similarity { get; set; }

    }
}

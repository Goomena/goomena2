﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtConnection = New System.Windows.Forms.TextBox()
        Me.txtHdPath = New System.Windows.Forms.TextBox()
        Me.txtOriginalPath = New System.Windows.Forms.TextBox()
        Me.txtThumbPath = New System.Windows.Forms.TextBox()
        Me.txtd150 = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.prb = New DevExpress.XtraEditors.ProgressBarControl()
        Me.lblTotal = New DevExpress.XtraEditors.LabelControl()
        Me.cmdStart = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdStop = New DevExpress.XtraEditors.SimpleButton()
        Me.BackgroundWorker1 = New System.ComponentModel.BackgroundWorker()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtd350 = New System.Windows.Forms.TextBox()
        CType(Me.prb.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(5, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(91, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "ConnectionString:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(29, 42)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(67, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "OriginalPath:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Enabled = False
        Me.Label3.Location = New System.Drawing.Point(48, 68)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(48, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "HDPath:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(31, 94)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(65, 13)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "ThumbPath:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(40, 120)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(56, 13)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "d150Path:"
        '
        'txtConnection
        '
        Me.txtConnection.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtConnection.Location = New System.Drawing.Point(102, 13)
        Me.txtConnection.Name = "txtConnection"
        Me.txtConnection.Size = New System.Drawing.Size(841, 20)
        Me.txtConnection.TabIndex = 5
        Me.txtConnection.Text = "Data Source=KOUKOS-PC\SERVER;Initial Catalog=CMSGoomena;Integrated Security=True"
        '
        'txtHdPath
        '
        Me.txtHdPath.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtHdPath.Enabled = False
        Me.txtHdPath.Location = New System.Drawing.Point(102, 65)
        Me.txtHdPath.Name = "txtHdPath"
        Me.txtHdPath.Size = New System.Drawing.Size(841, 20)
        Me.txtHdPath.TabIndex = 6
        Me.txtHdPath.Text = "C:\inetpub\photos.goomena.com\{0}\HD\{1}"
        '
        'txtOriginalPath
        '
        Me.txtOriginalPath.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtOriginalPath.Location = New System.Drawing.Point(102, 39)
        Me.txtOriginalPath.Name = "txtOriginalPath"
        Me.txtOriginalPath.Size = New System.Drawing.Size(841, 20)
        Me.txtOriginalPath.TabIndex = 7
        Me.txtOriginalPath.Text = "C:\inetpub\photos.goomena.com\{0}\{1}"
        '
        'txtThumbPath
        '
        Me.txtThumbPath.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtThumbPath.Location = New System.Drawing.Point(102, 91)
        Me.txtThumbPath.Name = "txtThumbPath"
        Me.txtThumbPath.Size = New System.Drawing.Size(841, 20)
        Me.txtThumbPath.TabIndex = 8
        Me.txtThumbPath.Text = "C:\inetpub\photos.goomena.com\{0}\thumbs\{1}"
        '
        'txtd150
        '
        Me.txtd150.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtd150.Location = New System.Drawing.Point(102, 117)
        Me.txtd150.Name = "txtd150"
        Me.txtd150.Size = New System.Drawing.Size(841, 20)
        Me.txtd150.TabIndex = 9
        Me.txtd150.Text = "C:\inetpub\photos.goomena.com\{0}\d150\{1}"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.Label6.Location = New System.Drawing.Point(7, 226)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(103, 13)
        Me.Label6.TabIndex = 10
        Me.Label6.Text = "Total Processed:"
        '
        'prb
        '
        Me.prb.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.prb.Location = New System.Drawing.Point(0, 253)
        Me.prb.Name = "prb"
        Me.prb.Properties.ShowTitle = True
        Me.prb.Size = New System.Drawing.Size(955, 33)
        Me.prb.TabIndex = 11
        Me.prb.Visible = False
        '
        'lblTotal
        '
        Me.lblTotal.Location = New System.Drawing.Point(116, 226)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(66, 13)
        Me.lblTotal.TabIndex = 12
        Me.lblTotal.Text = "LabelControl1"
        '
        'cmdStart
        '
        Me.cmdStart.Location = New System.Drawing.Point(481, 224)
        Me.cmdStart.Name = "cmdStart"
        Me.cmdStart.Size = New System.Drawing.Size(75, 23)
        Me.cmdStart.TabIndex = 13
        Me.cmdStart.Text = "Start Hashing"
        '
        'cmdStop
        '
        Me.cmdStop.Location = New System.Drawing.Point(790, 224)
        Me.cmdStop.Name = "cmdStop"
        Me.cmdStop.Size = New System.Drawing.Size(75, 23)
        Me.cmdStop.TabIndex = 14
        Me.cmdStop.Text = "StopHashing"
        Me.cmdStop.Visible = False
        '
        'BackgroundWorker1
        '
        Me.BackgroundWorker1.WorkerReportsProgress = True
        Me.BackgroundWorker1.WorkerSupportsCancellation = True
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(40, 146)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(56, 13)
        Me.Label7.TabIndex = 4
        Me.Label7.Text = "d350Path:"
        '
        'txtd350
        '
        Me.txtd350.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtd350.Location = New System.Drawing.Point(102, 143)
        Me.txtd350.Name = "txtd350"
        Me.txtd350.Size = New System.Drawing.Size(841, 20)
        Me.txtd350.TabIndex = 9
        Me.txtd350.Text = "C:\inetpub\photos.goomena.com\{0}\d350\{1}"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(955, 286)
        Me.Controls.Add(Me.cmdStop)
        Me.Controls.Add(Me.cmdStart)
        Me.Controls.Add(Me.lblTotal)
        Me.Controls.Add(Me.prb)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.txtd350)
        Me.Controls.Add(Me.txtd150)
        Me.Controls.Add(Me.txtThumbPath)
        Me.Controls.Add(Me.txtOriginalPath)
        Me.Controls.Add(Me.txtHdPath)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.txtConnection)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Name = "Form1"
        Me.Text = "frmMain"
        CType(Me.prb.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtConnection As System.Windows.Forms.TextBox
    Friend WithEvents txtHdPath As System.Windows.Forms.TextBox
    Friend WithEvents txtOriginalPath As System.Windows.Forms.TextBox
    Friend WithEvents txtThumbPath As System.Windows.Forms.TextBox
    Friend WithEvents txtd150 As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents prb As DevExpress.XtraEditors.ProgressBarControl
    Friend WithEvents lblTotal As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cmdStart As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdStop As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents BackgroundWorker1 As System.ComponentModel.BackgroundWorker
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtd350 As System.Windows.Forms.TextBox

End Class

﻿Imports System.Security.Cryptography

Public Class Form1
    Private ori_path As String = ""
    Private Hd_path As String = ""
    Private thumb_path As String = ""
    Private d150_path As String = ""
    Private d350_path As String = ""
    Private con As String
    Private _LastId As Integer = -1
    Private Event UpdateProgress(ByVal e_ As Integer)
    Private Delegate Sub delUpdateProgressMAx(ByVal _e As Integer)
    Private Sub UpdateProgressMAx(ByVal e As Integer)
        If Me.InvokeRequired Then
            Dim d As New delUpdateProgressMAx(AddressOf UpdateProgressMAx)
            Me.Invoke(d, {e})
        Else
            _LastId = -1
            prb.Properties.Maximum = e
            prb.Position = 0
            lblTotal.Text = "0/" & e
        End If
    End Sub
    Private Sub UpdateProgressCurrent(ByVal e As Integer) Handles Me.UpdateProgress
        If Me.InvokeRequired Then
            Dim d As New delUpdateProgressMAx(AddressOf UpdateProgressCurrent)
            Me.Invoke(d, {e})
        Else
            ' _LastId = -1
            ' prb.Properties.Maximum = e
            prb.Position = e
            lblTotal.Text = e & "/" & e
        End If
    End Sub
    Private Function ReadValues() As Boolean
        Dim re As Boolean = False
        Try
            ori_path = txtOriginalPath.Text
            Hd_path = txtHdPath.Text
            thumb_path = txtThumbPath.Text
            d150_path = txtd150.Text
            d350_path = txtd350.Text
            con = txtConnection.Text
            re = True
        Catch ex As Exception
        End Try
        Return re
    End Function
    Private Function getFileHash(ByVal filePath As String) As String
        Dim s As String = Nothing
        Try


            ' get all the file contents
            Dim File() As Byte = System.IO.File.ReadAllBytes(filePath)

            ' create a new md5 object
            Dim Md5 As New MD5CryptoServiceProvider()

            ' compute the hash
            Dim byteHash() As Byte = Md5.ComputeHash(File)
            s = Convert.ToBase64String(byteHash)
            ' return the value in base 64 
        Catch ex As Exception
            s = Nothing
        End Try
        Return s
    End Function
    Private Sub BackgroundWorker1_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork
        Try
            Dim _continue As Boolean = False
            If ReadValues() Then
                Using ta As New dsTableAdapters.EUS_CustomerPhotosTableAdapter
                    ta.Connection.ConnectionString = con
                    Dim o As Integer = ta.Count
                    UpdateProgressMAx(o)
                    If o = 0 Then Return

                    Dim i As Integer = 0
                    Dim lock As Boolean = False
                    Using c_connect As New SqlClient.SqlConnection(con)
                        c_connect.Open()
                        Using exe As New SqlClient.SqlCommand
                            exe.Connection = c_connect
                            exe.CommandTimeout = 360
                            exe.CommandType = CommandType.Text
                            Do Until lock = True
                                If BackgroundWorker1.CancellationPending Then
                                    lock = True
                                    Exit Sub
                                End If
                                Using ds1 As New ds
                                    ta.FillBy(ds1.EUS_CustomerPhotos, _LastId)
                                    If ds1.EUS_CustomerPhotos.Rows.Count > 0 Then
                                        If ds1.EUS_CustomerPhotos.Rows.Count = 100 Then
                                            Dim dr As ds.EUS_CustomerPhotosRow = ds1.EUS_CustomerPhotos.Rows(99)
                                            _LastId = dr.CustomerPhotosID
                                        Else
                                            lock = True
                                            BackgroundWorker1.ReportProgress(100)
                                        End If

                                        For Each r As ds.EUS_CustomerPhotosRow In ds1.EUS_CustomerPhotos.Rows
                                            If BackgroundWorker1.CancellationPending Then
                                                lock = True
                                                Exit Sub
                                            End If

                                            Dim UpdateFields As String = ""
                                            Try
                                                Dim tmp_ori_path As String = String.Format(ori_path, r.CustomerID, r.FileName)
                                                If tmp_ori_path <> "" AndAlso IO.File.Exists(tmp_ori_path) Then
                                                    ' r.HashOriginal = getFileHash(tmp_ori_path)
                                                    If UpdateFields <> "" Then UpdateFields = UpdateFields & ", " & vbNewLine
                                                    UpdateFields &= "[HashOriginal] ='" & getFileHash(tmp_ori_path) & "'"
                                                End If
                                            Catch ex As Exception
                                            End Try

                                            'Try
                                            '    Dim tmp_HD_path As String = String.Format(Hd_path, r.CustomerID, r.FileName)
                                            '    If tmp_HD_path <> "" AndAlso IO.File.Exists(tmp_HD_path) Then
                                            '        If UpdateFields <> "" Then UpdateFields = UpdateFields & ", " & vbNewLine
                                            '        UpdateFields = "[HashHD] ='" & getFileHash(tmp_HD_path) & "'"
                                            '    End If
                                            'Catch ex As Exception
                                            'End Try

                                            Try
                                                Dim tmp_thumb_path As String = String.Format(thumb_path, r.CustomerID, r.FileName)
                                                If tmp_thumb_path <> "" AndAlso IO.File.Exists(tmp_thumb_path) Then
                                                    If UpdateFields <> "" Then UpdateFields = UpdateFields & ", " & vbNewLine
                                                    UpdateFields = UpdateFields & "[HashThumb] ='" & getFileHash(tmp_thumb_path) & "'"
                                                End If
                                            Catch ex As Exception
                                            End Try

                                            Try
                                                Dim tmp_d150_path As String = String.Format(d150_path, r.CustomerID, r.FileName)
                                                If tmp_d150_path <> "" AndAlso IO.File.Exists(tmp_d150_path) Then
                                                    '    r.Hashd150 = getFileHash(tmp_d150_path)
                                                    If UpdateFields <> "" Then UpdateFields = UpdateFields & ", " & vbNewLine
                                                    UpdateFields = UpdateFields & "[Hashd150] ='" & getFileHash(tmp_d150_path) & "'"
                                                End If
                                            Catch ex As Exception
                                            End Try


                                            Try
                                                Dim tmp_d350_path As String = String.Format(d350_path, r.CustomerID, r.FileName)
                                                If tmp_d350_path <> "" AndAlso IO.File.Exists(tmp_d350_path) Then
                                                    '    r.Hashd150 = getFileHash(tmp_d150_path)
                                                    If UpdateFields <> "" Then UpdateFields = UpdateFields & ", " & vbNewLine
                                                    UpdateFields = UpdateFields & "[Hashd350] ='" & getFileHash(tmp_d350_path) & "'"
                                                End If
                                            Catch ex As Exception
                                            End Try

                                            If BackgroundWorker1.CancellationPending Then
                                                lock = True
                                                Exit Sub
                                            End If
                                            If UpdateFields <> "" Then
                                                Dim sql As String = " UPDATE [dbo].[EUS_CustomerPhotos] " & _
                                                                    " SET " & _
                                                                          UpdateFields & _
                                                                             "WHERE CustomerPhotosID=" & r.CustomerPhotosID
                                                exe.CommandText = sql
                                                exe.ExecuteNonQuery()
                                            End If
                                            i += 1
                                            RaiseEvent UpdateProgress(i)
                                        Next
                                    End If
                                End Using
                            Loop
                        End Using
                    End Using
                End Using
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub cmdStart_Click(sender As Object, e As EventArgs) Handles cmdStart.Click
        cmdStart.Visible = False
        cmdStop.Visible = True
        prb.Visible = True
        BackgroundWorker1.RunWorkerAsync()
    End Sub

    Private Sub cmdStop_Click(sender As Object, e As EventArgs) Handles cmdStop.Click
        cmdStart.Visible = True
        cmdStop.Visible = False
        prb.Visible = False
        BackgroundWorker1.CancelAsync()
    End Sub
    Private Sub Completed()
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New MethodInvoker(AddressOf Completed))
            Else
                cmdStart.Visible = True
                cmdStop.Visible = False
                prb.Visible = False
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub BackgroundWorker1_ProgressChanged(sender As Object, e As System.ComponentModel.ProgressChangedEventArgs) Handles BackgroundWorker1.ProgressChanged
        If e.ProgressPercentage = 100 Then
            Completed()
        End If
    End Sub
End Class

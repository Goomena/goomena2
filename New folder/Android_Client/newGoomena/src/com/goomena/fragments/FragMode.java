package com.goomena.fragments;

import com.goomena.app.R;
import com.goomena.app.tools.Log;
import com.goomena.app.tools.Utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;


@SuppressLint("NewApi") 
public class FragMode extends Fragment
{
	Activity act;
	SharedPreferences settings;
	
	RelativeLayout relativeMode1,relativeMode2,relativeMode3,relativeMode4;
	RelativeLayout relativeModeSettings1,relativeModeSettings2,relativeModeSettings3,relativeModeSettings4;
	RelativeLayout relativeModeSettings_save;
	ImageView new_menuItemImageMode;
	TextView new_menuItemTextMode;
	int selectedMode = 1;
	
	LinearLayout LinearLayout1,LinearLayout2,LinearLayout3,LinearLayout4;
	
	
	
	
	Boolean L1_SoundOn = true;
	Boolean L2_SoundOn = true;
	Boolean L3_SoundOn = true;
	Boolean L4_SoundOn = true;
	ImageView L1_modesettingsImageOnOff,L2_modesettingsImageOnOff,L3_modesettingsImageOnOff,L4_modesettingsImageOnOff;
	RelativeLayout L1_modesettingsRelativeBall_1,L1_modesettingsRelativeBall_2,L1_modesettingsRelativeBall_3;
	RelativeLayout L2_modesettingsRelativeBall_1,L2_modesettingsRelativeBall_2,L2_modesettingsRelativeBall_3;
	RelativeLayout L3_modesettingsRelativeBall_1,L3_modesettingsRelativeBall_2,L3_modesettingsRelativeBall_3;
	RelativeLayout L4_modesettingsRelativeBall_1,L4_modesettingsRelativeBall_2,L4_modesettingsRelativeBall_3;
	ImageView L1_modesettingsImageball_1,L1_modesettingsImageball_2,L1_modesettingsImageball_3;
	ImageView L2_modesettingsImageball_1,L2_modesettingsImageball_2,L2_modesettingsImageball_3;
	ImageView L3_modesettingsImageball_1,L3_modesettingsImageball_2,L3_modesettingsImageball_3;
	ImageView L4_modesettingsImageball_1,L4_modesettingsImageball_2,L4_modesettingsImageball_3;
	RelativeLayout L1_modesettingsRelativeLogoG,L1_modesettingsRelativeLogoT;
	RelativeLayout L2_modesettingsRelativeLogoG,L2_modesettingsRelativeLogoT;
	RelativeLayout L3_modesettingsRelativeLogoG,L3_modesettingsRelativeLogoT;
	RelativeLayout L4_modesettingsRelativeLogoG,L4_modesettingsRelativeLogoT;
	ImageView L1_modesettingsImageBallG,L1_modesettingsImageBallT;
	ImageView L2_modesettingsImageBallG,L2_modesettingsImageBallT;
	ImageView L3_modesettingsImageBallG,L3_modesettingsImageBallT;
	ImageView L4_modesettingsImageBallG,L4_modesettingsImageBallT;
	RelativeLayout L1_relativeSave,L2_relativeSave,L3_relativeSave,L4_relativeSave;
	
	int L1_Tone=-1;
	int L1_Notif=-1;
	int L1_Appl=-1;
	
	int L2_Tone=-1;
	int L2_Notif=-1;
	int L2_Appl=-1;
	
	int L3_Tone=-1;
	int L3_Notif=-1;
	int L3_Appl=-1;
	
	int L4_Tone=-1;
	int L4_Notif=-1;
	int L4_Appl=-1;
	
	
	
	@SuppressWarnings("unused")
	private final static String TAG_FRAGMENT = "TAG_FRAGMENT";
	
	Animation AnimLinearLayout1,AnimLinearLayout2,AnimLinearLayout3,AnimLinearLayout4;
	
	@Override
	 public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState) 
	 {
		 return inflater.inflate(R.layout.act_include_mode, container, false);
	 }
	
	public void onActivityCreated (Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);
		act = getActivity();
		
		relativeMode1 = (RelativeLayout) act.findViewById(R.id.relativeMode1);
		relativeMode2 = (RelativeLayout) act.findViewById(R.id.relativeMode2);
		relativeMode3 = (RelativeLayout) act.findViewById(R.id.relativeMode3);
		relativeMode4 = (RelativeLayout) act.findViewById(R.id.relativeMode4);
		
		relativeModeSettings1 = (RelativeLayout) act.findViewById(R.id.relativeModeSettings1);
		relativeModeSettings2 = (RelativeLayout) act.findViewById(R.id.relativeModeSettings2);
		relativeModeSettings3 = (RelativeLayout) act.findViewById(R.id.relativeModeSettings3);
		relativeModeSettings4 = (RelativeLayout) act.findViewById(R.id.relativeModeSettings4);
		relativeModeSettings_save = (RelativeLayout) act.findViewById(R.id.relativeModeSettings_save);
		
		LinearLayout1 = (LinearLayout) act.findViewById(R.id.LinearLayout1);
		LinearLayout2 = (LinearLayout) act.findViewById(R.id.LinearLayout2);
		LinearLayout3 = (LinearLayout) act.findViewById(R.id.LinearLayout3);
		LinearLayout4 = (LinearLayout) act.findViewById(R.id.LinearLayout4);
		
		
		
		
	
		new_menuItemImageMode = (ImageView) act.findViewById(R.id.new_menuItemImageMode);
		new_menuItemTextMode = (TextView) act.findViewById(R.id.new_menuItemTextMode);
		
		relativeMode1.setOnClickListener(clickListener);
		relativeMode2.setOnClickListener(clickListener);
		relativeMode3.setOnClickListener(clickListener);
		relativeMode4.setOnClickListener(clickListener);
		
		relativeModeSettings1.setOnClickListener(clickListener);
		relativeModeSettings2.setOnClickListener(clickListener);
		relativeModeSettings3.setOnClickListener(clickListener);
		relativeModeSettings4.setOnClickListener(clickListener);
		relativeModeSettings_save.setOnClickListener(clickListener);
		
		settings = act.getSharedPreferences(Utils.SharedPrefsKey, 0);
		int edit = settings.getInt("Mode",0);
		//Log.v("APP","aaaaaaaaaa" +edit);
		selectMode(edit);
		
		
		
		
		AnimLinearLayout1 = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_up);
		AnimLinearLayout1.setAnimationListener(new AnimationListener() { 
			@Override public void onAnimationStart(Animation animation) {}
			@Override public void onAnimationRepeat(Animation animation) {}
			@Override public void onAnimationEnd(Animation animation) 
			{
				LinearLayout1.setVisibility(LinearLayout.GONE);
				LinearLayout1.clearAnimation();
			}});
		
		
		AnimLinearLayout2 = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_up);
		AnimLinearLayout2.setAnimationListener(new AnimationListener() { 
			@Override public void onAnimationStart(Animation animation) {}
			@Override public void onAnimationRepeat(Animation animation) {}
			@Override public void onAnimationEnd(Animation animation) 
			{
				LinearLayout2.setVisibility(LinearLayout.GONE);
				LinearLayout2.clearAnimation();
			}});
		
		AnimLinearLayout3 = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_up);
		AnimLinearLayout3.setAnimationListener(new AnimationListener() { 
			@Override public void onAnimationStart(Animation animation) {}
			@Override public void onAnimationRepeat(Animation animation) {}
			@Override public void onAnimationEnd(Animation animation) 
			{
				LinearLayout3.setVisibility(LinearLayout.GONE);
				LinearLayout3.clearAnimation();
			}});
		
		AnimLinearLayout4 = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_up);
		AnimLinearLayout4.setAnimationListener(new AnimationListener() { 
			@Override public void onAnimationStart(Animation animation) {}
			@Override public void onAnimationRepeat(Animation animation) {}
			@Override public void onAnimationEnd(Animation animation) 
			{
				LinearLayout4.setVisibility(LinearLayout.GONE);
				LinearLayout4.clearAnimation();
			}});
		
		
		
		
		
		
		
		L1_modesettingsImageOnOff = (ImageView) act.findViewById(R.id.L1_modesettingsImageOnOff);
		L2_modesettingsImageOnOff = (ImageView) act.findViewById(R.id.L2_modesettingsImageOnOff);
		L3_modesettingsImageOnOff = (ImageView) act.findViewById(R.id.L3_modesettingsImageOnOff);
		L4_modesettingsImageOnOff = (ImageView) act.findViewById(R.id.L4_modesettingsImageOnOff);
		
		L1_modesettingsRelativeBall_1 = (RelativeLayout) act.findViewById(R.id.L1_modesettingsRelativeBall_1);
		L1_modesettingsRelativeBall_2 = (RelativeLayout) act.findViewById(R.id.L1_modesettingsRelativeBall_2);
		L1_modesettingsRelativeBall_3 = (RelativeLayout) act.findViewById(R.id.L1_modesettingsRelativeBall_3);
		L2_modesettingsRelativeBall_1 = (RelativeLayout) act.findViewById(R.id.L2_modesettingsRelativeBall_1);
		L2_modesettingsRelativeBall_2 = (RelativeLayout) act.findViewById(R.id.L2_modesettingsRelativeBall_2);
		L2_modesettingsRelativeBall_3 = (RelativeLayout) act.findViewById(R.id.L2_modesettingsRelativeBall_3);
		L3_modesettingsRelativeBall_1 = (RelativeLayout) act.findViewById(R.id.L3_modesettingsRelativeBall_1);
		L3_modesettingsRelativeBall_2 = (RelativeLayout) act.findViewById(R.id.L3_modesettingsRelativeBall_2);
		L3_modesettingsRelativeBall_3 = (RelativeLayout) act.findViewById(R.id.L3_modesettingsRelativeBall_3);
		L4_modesettingsRelativeBall_1 = (RelativeLayout) act.findViewById(R.id.L4_modesettingsRelativeBall_1);
		L4_modesettingsRelativeBall_2 = (RelativeLayout) act.findViewById(R.id.L4_modesettingsRelativeBall_2);
		L4_modesettingsRelativeBall_3 = (RelativeLayout) act.findViewById(R.id.L4_modesettingsRelativeBall_3);
		
		L1_modesettingsImageball_1 = (ImageView) act.findViewById(R.id.L1_modesettingsImageball_1);
		L1_modesettingsImageball_2 = (ImageView) act.findViewById(R.id.L1_modesettingsImageball_2);
		L1_modesettingsImageball_3 = (ImageView) act.findViewById(R.id.L1_modesettingsImageball_3);
		L2_modesettingsImageball_1 = (ImageView) act.findViewById(R.id.L2_modesettingsImageball_1);
		L2_modesettingsImageball_2 = (ImageView) act.findViewById(R.id.L2_modesettingsImageball_2);
		L2_modesettingsImageball_3 = (ImageView) act.findViewById(R.id.L2_modesettingsImageball_3);
		L3_modesettingsImageball_1 = (ImageView) act.findViewById(R.id.L3_modesettingsImageball_1);
		L3_modesettingsImageball_2 = (ImageView) act.findViewById(R.id.L3_modesettingsImageball_2);
		L3_modesettingsImageball_3 = (ImageView) act.findViewById(R.id.L3_modesettingsImageball_3);
		L4_modesettingsImageball_1 = (ImageView) act.findViewById(R.id.L4_modesettingsImageball_1);
		L4_modesettingsImageball_2 = (ImageView) act.findViewById(R.id.L4_modesettingsImageball_2);
		L4_modesettingsImageball_3 = (ImageView) act.findViewById(R.id.L4_modesettingsImageball_3);
		
		L1_modesettingsRelativeLogoG = (RelativeLayout) act.findViewById(R.id.L1_modesettingsRelativeLogoG);
		L1_modesettingsRelativeLogoT = (RelativeLayout) act.findViewById(R.id.L1_modesettingsRelativeLogoT);
		L2_modesettingsRelativeLogoG = (RelativeLayout) act.findViewById(R.id.L2_modesettingsRelativeLogoG);
		L2_modesettingsRelativeLogoT = (RelativeLayout) act.findViewById(R.id.L2_modesettingsRelativeLogoT);
		L3_modesettingsRelativeLogoG = (RelativeLayout) act.findViewById(R.id.L3_modesettingsRelativeLogoG);
		L3_modesettingsRelativeLogoT = (RelativeLayout) act.findViewById(R.id.L3_modesettingsRelativeLogoT);
		L4_modesettingsRelativeLogoG = (RelativeLayout) act.findViewById(R.id.L4_modesettingsRelativeLogoG);
		L4_modesettingsRelativeLogoT = (RelativeLayout) act.findViewById(R.id.L4_modesettingsRelativeLogoT);
		
		
		L1_modesettingsImageBallG = (ImageView) act.findViewById(R.id.L1_modesettingsImageBallG);
		L1_modesettingsImageBallT = (ImageView) act.findViewById(R.id.L1_modesettingsImageBallT);
		L2_modesettingsImageBallG = (ImageView) act.findViewById(R.id.L2_modesettingsImageBallG);
		L2_modesettingsImageBallT = (ImageView) act.findViewById(R.id.L2_modesettingsImageBallT);
		L3_modesettingsImageBallG = (ImageView) act.findViewById(R.id.L3_modesettingsImageBallG);
		L3_modesettingsImageBallT = (ImageView) act.findViewById(R.id.L3_modesettingsImageBallT);
		L4_modesettingsImageBallG = (ImageView) act.findViewById(R.id.L4_modesettingsImageBallG);
		L4_modesettingsImageBallT = (ImageView) act.findViewById(R.id.L4_modesettingsImageBallT);
		L1_relativeSave = (RelativeLayout) act.findViewById(R.id.L1_relativeSave);
		L2_relativeSave = (RelativeLayout) act.findViewById(R.id.L2_relativeSave);
		L3_relativeSave = (RelativeLayout) act.findViewById(R.id.L3_relativeSave);
		L4_relativeSave = (RelativeLayout) act.findViewById(R.id.L4_relativeSave);
		
		
		
		
		L1_modesettingsImageOnOff.setOnClickListener(clickListenersettings);
		L2_modesettingsImageOnOff.setOnClickListener(clickListenersettings);
		L3_modesettingsImageOnOff.setOnClickListener(clickListenersettings);
		L4_modesettingsImageOnOff.setOnClickListener(clickListenersettings);
		L1_modesettingsRelativeBall_1.setOnClickListener(clickListenersettings);
		L1_modesettingsRelativeBall_2.setOnClickListener(clickListenersettings);
		L1_modesettingsRelativeBall_3.setOnClickListener(clickListenersettings);
		L2_modesettingsRelativeBall_1.setOnClickListener(clickListenersettings);
		L2_modesettingsRelativeBall_2.setOnClickListener(clickListenersettings);
		L2_modesettingsRelativeBall_3.setOnClickListener(clickListenersettings);
		L3_modesettingsRelativeBall_1.setOnClickListener(clickListenersettings);
		L3_modesettingsRelativeBall_2.setOnClickListener(clickListenersettings);
		L3_modesettingsRelativeBall_3.setOnClickListener(clickListenersettings);
		L4_modesettingsRelativeBall_1.setOnClickListener(clickListenersettings);
		L4_modesettingsRelativeBall_2.setOnClickListener(clickListenersettings);
		L4_modesettingsRelativeBall_3.setOnClickListener(clickListenersettings);
		L1_modesettingsRelativeLogoG.setOnClickListener(clickListenersettings);
		L1_modesettingsRelativeLogoT.setOnClickListener(clickListenersettings);
		L2_modesettingsRelativeLogoG.setOnClickListener(clickListenersettings);
		L2_modesettingsRelativeLogoT.setOnClickListener(clickListenersettings);
		L3_modesettingsRelativeLogoG.setOnClickListener(clickListenersettings);
		L3_modesettingsRelativeLogoT.setOnClickListener(clickListenersettings);
		L4_modesettingsRelativeLogoG.setOnClickListener(clickListenersettings);
		L4_modesettingsRelativeLogoT.setOnClickListener(clickListenersettings);
		L1_relativeSave.setOnClickListener(clickListenersettings);
		L2_relativeSave.setOnClickListener(clickListenersettings);
		L3_relativeSave.setOnClickListener(clickListenersettings);
		L4_relativeSave.setOnClickListener(clickListenersettings);
		
		LoadInfo();
	}
	
	
	
	
	OnClickListener clickListenersettings = new OnClickListener() 
	{
		public void onClick(View v) 
		{
			if(v == L1_relativeSave)
			{
				if(L1_Tone!=-1)
				{
					Editor edit = settings.edit();
					edit.putInt("Mode0Tone", L1_Tone);
					edit.commit();
				}
				if(L1_Notif!=-1)
				{
					Editor edit = settings.edit();
					edit.putInt("Mode0Notif", L1_Notif);
					edit.commit();
				}
				if(L1_Appl!=-1)
				{
					Editor edit = settings.edit();
					edit.putInt("Mode0Appl", L1_Appl);
					edit.commit();
				}
				LinearLayout1.startAnimation(AnimLinearLayout1);
				
				selectMode(1);
				new_menuItemImageMode.setImageResource(R.drawable.nm_mode_nice_free);
				new_menuItemTextMode.setText("("+getResources().getString(R.string.normal)+")");
				Editor edit = settings.edit();
				edit.putInt("Mode", 1);
				edit.commit();
			}
			else if(v == L2_relativeSave)
			{
				if(L2_Tone!=-1)
				{
					Editor edit = settings.edit();
					edit.putInt("Mode1Tone", L2_Tone);
					edit.commit();
				}
				if(L2_Notif!=-1)
				{
					Editor edit = settings.edit();
					edit.putInt("Mode1Notif", L2_Notif);
					edit.commit();
				}
				if(L2_Appl!=-1)
				{
					Editor edit = settings.edit();
					edit.putInt("Mode1Appl", L2_Appl);
					edit.commit();
				}
				LinearLayout2.startAnimation(AnimLinearLayout2);
				
				selectMode(2);
				new_menuItemImageMode.setImageResource(R.drawable.nm_mode_bussiness);
				new_menuItemTextMode.setText("("+getResources().getString(R.string.discreet)+")"); 
				Editor edit = settings.edit();
				edit.putInt("Mode", 2);
				edit.commit();
			}
			else if(v == L3_relativeSave)
			{
				if(L3_Tone!=-1)
				{
					Editor edit = settings.edit();
					edit.putInt("Mode2Tone", L3_Tone);
					edit.commit();
				}
				if(L3_Notif!=-1)
				{
					Editor edit = settings.edit();
					edit.putInt("Mode2Notif", L3_Notif);
					edit.commit();
				}
				if(L3_Appl!=-1)
				{
					Editor edit = settings.edit();
					edit.putInt("Mode2Appl", L3_Appl);
					edit.commit();
				}
				LinearLayout3.startAnimation(AnimLinearLayout3);
				
				selectMode(3);
				new_menuItemImageMode.setImageResource(R.drawable.nm_mode_with_wife);
				new_menuItemTextMode.setText("("+getResources().getString(R.string.dangerous_situation)+")");
				Editor edit = settings.edit();
				edit.putInt("Mode", 3);
				edit.commit();
			}
			else if(v == L4_relativeSave)
			{
				if(L4_Tone!=-1)
				{
					Editor edit = settings.edit();
					edit.putInt("Mode3Tone", L4_Tone);
					edit.commit();
				}
				if(L4_Notif!=-1)
				{
					Editor edit = settings.edit();
					edit.putInt("Mode3Notif", L4_Notif);
					edit.commit();
				}
				if(L4_Appl!=-1)
				{
					Editor edit = settings.edit();
					edit.putInt("Mode3Appl", L4_Appl);
					edit.commit();
				}
				LinearLayout4.startAnimation(AnimLinearLayout4);
				
				selectMode(4);
				new_menuItemImageMode.setImageResource(R.drawable.nm_mode_spy);
				new_menuItemTextMode.setText("("+getResources().getString(R.string.masquerading)+")");
				Editor edit = settings.edit();
				edit.putInt("Mode", 4);
				edit.commit();
			}
			else if(v == L1_modesettingsImageOnOff)
			{
				if(L1_SoundOn)
				{
					L1_modesettingsImageOnOff.setImageResource(R.drawable.off);
					L1_SoundOn=false;
					L1_Tone=2;
				}
				else
				{
					L1_modesettingsImageOnOff.setImageResource(R.drawable.on);
					L1_SoundOn = true;
					L1_Tone=1;
				}
			}
			else if(v == L2_modesettingsImageOnOff)
			{
				if(L2_SoundOn)
				{
					L2_modesettingsImageOnOff.setImageResource(R.drawable.off);
					L2_SoundOn=false;
					L2_Tone=2;
				}
				else
				{
					L2_modesettingsImageOnOff.setImageResource(R.drawable.on);
					L2_SoundOn = true;
					L2_Tone=1;
				}
			}
			else if(v == L3_modesettingsImageOnOff)
			{
				if(L3_SoundOn)
				{
					L3_modesettingsImageOnOff.setImageResource(R.drawable.off);
					L3_SoundOn=false;
					L3_Tone=2;
				}
				else
				{
					L3_modesettingsImageOnOff.setImageResource(R.drawable.on);
					L3_SoundOn = true;
					L3_Tone=1;
				}
			}
			else if(v == L4_modesettingsImageOnOff)
			{
				if(L4_SoundOn)
				{
					L4_modesettingsImageOnOff.setImageResource(R.drawable.off);
					L4_SoundOn=false;
					L4_Tone=2;
				}
				else
				{
					L4_modesettingsImageOnOff.setImageResource(R.drawable.on);
					L4_SoundOn = true;
					L4_Tone=1;
				}
			}
			else if(v == L1_modesettingsRelativeBall_1)
			{
				L1_modesettingsImageball_1.setImageResource(R.drawable.ds_enable);
				L1_modesettingsImageball_2.setImageResource(R.drawable.ds_disable);
				L1_modesettingsImageball_3.setImageResource(R.drawable.ds_disable);
				L1_Notif = 1;
			}
			else if(v == L1_modesettingsRelativeBall_2)
			{
				L1_modesettingsImageball_1.setImageResource(R.drawable.ds_disable);
				L1_modesettingsImageball_2.setImageResource(R.drawable.ds_enable);
				L1_modesettingsImageball_3.setImageResource(R.drawable.ds_disable);
				L1_Notif = 2;
			}
			else if(v == L1_modesettingsRelativeBall_3)
			{
				L1_modesettingsImageball_1.setImageResource(R.drawable.ds_disable);
				L1_modesettingsImageball_2.setImageResource(R.drawable.ds_disable);
				L1_modesettingsImageball_3.setImageResource(R.drawable.ds_enable);
				L1_Notif = 3;
			}
			else if(v == L2_modesettingsRelativeBall_1)
			{
				L2_modesettingsImageball_1.setImageResource(R.drawable.ds_enable);
				L2_modesettingsImageball_2.setImageResource(R.drawable.ds_disable);
				L2_modesettingsImageball_3.setImageResource(R.drawable.ds_disable);
				L2_Notif = 1;
			}
			else if(v == L2_modesettingsRelativeBall_2)
			{
				L2_modesettingsImageball_1.setImageResource(R.drawable.ds_disable);
				L2_modesettingsImageball_2.setImageResource(R.drawable.ds_enable);
				L2_modesettingsImageball_3.setImageResource(R.drawable.ds_disable);
				L2_Notif = 2;
			}
			else if(v == L2_modesettingsRelativeBall_3)
			{
				L2_modesettingsImageball_1.setImageResource(R.drawable.ds_disable);
				L2_modesettingsImageball_2.setImageResource(R.drawable.ds_disable);
				L2_modesettingsImageball_3.setImageResource(R.drawable.ds_enable);
				L2_Notif = 3;
			}
			else if(v == L3_modesettingsRelativeBall_1)
			{
				L3_modesettingsImageball_1.setImageResource(R.drawable.ds_enable);
				L3_modesettingsImageball_2.setImageResource(R.drawable.ds_disable);
				L3_modesettingsImageball_3.setImageResource(R.drawable.ds_disable);
				L3_Notif = 1;
			}
			else if(v == L3_modesettingsRelativeBall_2)
			{
				L3_modesettingsImageball_1.setImageResource(R.drawable.ds_disable);
				L3_modesettingsImageball_2.setImageResource(R.drawable.ds_enable);
				L3_modesettingsImageball_3.setImageResource(R.drawable.ds_disable);
				L3_Notif = 2;
			}
			else if(v == L3_modesettingsRelativeBall_3)
			{
				L3_modesettingsImageball_1.setImageResource(R.drawable.ds_disable);
				L3_modesettingsImageball_2.setImageResource(R.drawable.ds_disable);
				L3_modesettingsImageball_3.setImageResource(R.drawable.ds_enable);
				L3_Notif = 3;
			}
			else if(v == L4_modesettingsRelativeBall_1)
			{
				L4_modesettingsImageball_1.setImageResource(R.drawable.ds_enable);
				L4_modesettingsImageball_2.setImageResource(R.drawable.ds_disable);
				L4_modesettingsImageball_3.setImageResource(R.drawable.ds_disable);
				L4_Notif = 1;
			}
			else if(v == L4_modesettingsRelativeBall_2)
			{
				L4_modesettingsImageball_1.setImageResource(R.drawable.ds_disable);
				L4_modesettingsImageball_2.setImageResource(R.drawable.ds_enable);
				L4_modesettingsImageball_3.setImageResource(R.drawable.ds_disable);
				L4_Notif = 2;
			}
			else if(v == L4_modesettingsRelativeBall_3)
			{
				L4_modesettingsImageball_1.setImageResource(R.drawable.ds_disable);
				L4_modesettingsImageball_2.setImageResource(R.drawable.ds_disable);
				L4_modesettingsImageball_3.setImageResource(R.drawable.ds_enable);
				L4_Notif = 3;
			}
			else if(v == L1_modesettingsRelativeLogoG)
			{
				L1_modesettingsRelativeLogoG.setBackgroundResource(R.drawable.rectangle_search_white_corners);
				L1_modesettingsRelativeLogoT.setBackgroundResource(R.drawable.rectangle_white_alpha_corners);
				L1_modesettingsImageBallG.setImageResource(R.drawable.ds_enable);
				L1_modesettingsImageBallT.setImageResource(R.drawable.ds_disable);
				L1_Appl=1;
			}
			else if(v == L1_modesettingsRelativeLogoT)
			{
				L1_modesettingsRelativeLogoG.setBackgroundResource(R.drawable.rectangle_white_alpha_corners);
				L1_modesettingsRelativeLogoT.setBackgroundResource(R.drawable.rectangle_search_white_corners);
				L1_modesettingsImageBallG.setImageResource(R.drawable.ds_disable);
				L1_modesettingsImageBallT.setImageResource(R.drawable.ds_enable);
				L1_Appl=2;
			}	
			else if(v == L2_modesettingsRelativeLogoG)
			{
				L2_modesettingsRelativeLogoG.setBackgroundResource(R.drawable.rectangle_search_white_corners);
				L2_modesettingsRelativeLogoT.setBackgroundResource(R.drawable.rectangle_white_alpha_corners);
				L2_modesettingsImageBallG.setImageResource(R.drawable.ds_enable);
				L2_modesettingsImageBallT.setImageResource(R.drawable.ds_disable);
				L2_Appl=1;
			}
			else if(v == L2_modesettingsRelativeLogoT)
			{
				L2_modesettingsRelativeLogoG.setBackgroundResource(R.drawable.rectangle_white_alpha_corners);
				L2_modesettingsRelativeLogoT.setBackgroundResource(R.drawable.rectangle_search_white_corners);
				L2_modesettingsImageBallG.setImageResource(R.drawable.ds_disable);
				L2_modesettingsImageBallT.setImageResource(R.drawable.ds_enable);
				L2_Appl=2;
			}	
			else if(v == L3_modesettingsRelativeLogoG)
			{
				L3_modesettingsRelativeLogoG.setBackgroundResource(R.drawable.rectangle_search_white_corners);
				L3_modesettingsRelativeLogoT.setBackgroundResource(R.drawable.rectangle_white_alpha_corners);
				L3_modesettingsImageBallG.setImageResource(R.drawable.ds_enable);
				L3_modesettingsImageBallT.setImageResource(R.drawable.ds_disable);
				L3_Appl=1;
			}
			else if(v == L3_modesettingsRelativeLogoT)
			{
				L3_modesettingsRelativeLogoG.setBackgroundResource(R.drawable.rectangle_white_alpha_corners);
				L3_modesettingsRelativeLogoT.setBackgroundResource(R.drawable.rectangle_search_white_corners);
				L3_modesettingsImageBallG.setImageResource(R.drawable.ds_disable);
				L3_modesettingsImageBallT.setImageResource(R.drawable.ds_enable);
				L3_Appl=2;
			}	
			else if(v == L4_modesettingsRelativeLogoG)
			{
				L4_modesettingsRelativeLogoG.setBackgroundResource(R.drawable.rectangle_search_white_corners);
				L4_modesettingsRelativeLogoT.setBackgroundResource(R.drawable.rectangle_white_alpha_corners);
				L4_modesettingsImageBallG.setImageResource(R.drawable.ds_enable);
				L4_modesettingsImageBallT.setImageResource(R.drawable.ds_disable);
				L4_Appl=1;
			}
			else if(v == L4_modesettingsRelativeLogoT)
			{
				L4_modesettingsRelativeLogoG.setBackgroundResource(R.drawable.rectangle_white_alpha_corners);
				L4_modesettingsRelativeLogoT.setBackgroundResource(R.drawable.rectangle_search_white_corners);
				L4_modesettingsImageBallG.setImageResource(R.drawable.ds_disable);
				L4_modesettingsImageBallT.setImageResource(R.drawable.ds_enable);
				L4_Appl=2;
			}	
			
			
			
			
			
		}
	};
	
	
	OnClickListener clickListener = new OnClickListener() 
	{
		public void onClick(View v) 
		{
			if(v == relativeMode1)
			{
				selectMode(1);
			}
			else if(v == relativeMode2)
			{
				selectMode(2);
			}
			else if(v == relativeMode3)
			{
				selectMode(3);
			}
			else if(v == relativeMode4)
			{
				selectMode(4);
			}
			else if(v == relativeModeSettings1)
			{
				if(LinearLayout1.getVisibility() == LinearLayout.GONE)
				{
					LinearLayout1.setVisibility(LinearLayout.VISIBLE);
					LinearLayout1.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.mode_anim));
				}
				else
				{
					LinearLayout1.startAnimation(AnimLinearLayout1);
				}
			}
			else if(v == relativeModeSettings2)
			{
				if(LinearLayout2.getVisibility() == LinearLayout.GONE)
				{
					LinearLayout2.setVisibility(LinearLayout.VISIBLE);
					LinearLayout2.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.mode_anim));
				}
				else
				{
					LinearLayout2.startAnimation(AnimLinearLayout2);
				}
			}
			else if(v == relativeModeSettings3)
			{
				if(LinearLayout3.getVisibility() == LinearLayout.GONE)
				{
					LinearLayout3.setVisibility(LinearLayout.VISIBLE);
					LinearLayout3.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.mode_anim));
				}
				else
				{
					LinearLayout3.startAnimation(AnimLinearLayout3);
				}
			}
			else if(v == relativeModeSettings4)
			{
				if(LinearLayout4.getVisibility() == LinearLayout.GONE)
				{
					LinearLayout4.setVisibility(LinearLayout.VISIBLE);
					LinearLayout4.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.mode_anim));
				}
				else
				{
					LinearLayout4.startAnimation(AnimLinearLayout4);
				}
			}
			else if(v == relativeModeSettings_save)
			{
				if(selectedMode == 1)
				{
					new_menuItemImageMode.setImageResource(R.drawable.nm_mode_nice_free);
					new_menuItemTextMode.setText("("+getResources().getString(R.string.normal)+")");
					
					Editor edit = settings.edit();
					edit.putInt("Mode", 1);
					edit.commit();
				}
				else if(selectedMode == 2)
				{
					new_menuItemImageMode.setImageResource(R.drawable.nm_mode_bussiness);
					new_menuItemTextMode.setText("("+getResources().getString(R.string.discreet)+")"); 
					
					Editor edit = settings.edit();
					edit.putInt("Mode", 2);
					edit.commit();
				}
				else if(selectedMode == 3)
				{
					new_menuItemImageMode.setImageResource(R.drawable.nm_mode_with_wife);
					new_menuItemTextMode.setText("("+getResources().getString(R.string.dangerous_situation)+")");
					
					Editor edit = settings.edit();
					edit.putInt("Mode", 3);
					edit.commit();
				}
				else if(selectedMode == 4)
				{
					new_menuItemImageMode.setImageResource(R.drawable.nm_mode_spy);
					new_menuItemTextMode.setText("("+getResources().getString(R.string.masquerading)+")");
					
					Editor edit = settings.edit();
					edit.putInt("Mode", 4);
					edit.commit();
				}
				FragmentManager fm = getActivity().getFragmentManager();
				fm.popBackStack();
			}
		}
	};
	
	void selectMode(int mode)
	{
		if(mode == 1)
		{
			relativeMode1.setBackgroundResource(R.drawable.d_enable);
			relativeMode2.setBackgroundResource(R.drawable.d_regular);
			relativeMode3.setBackgroundResource(R.drawable.d_regular);
			relativeMode4.setBackgroundResource(R.drawable.d_regular);
			selectedMode = 1;
		}
		else if(mode == 2)
		{
			relativeMode1.setBackgroundResource(R.drawable.d_regular);
			relativeMode2.setBackgroundResource(R.drawable.d_enable);
			relativeMode3.setBackgroundResource(R.drawable.d_regular);
			relativeMode4.setBackgroundResource(R.drawable.d_regular);
			selectedMode = 2;
		}
		else if(mode == 3)
		{
			relativeMode1.setBackgroundResource(R.drawable.d_regular);
			relativeMode2.setBackgroundResource(R.drawable.d_regular);
			relativeMode3.setBackgroundResource(R.drawable.d_enable);
			relativeMode4.setBackgroundResource(R.drawable.d_regular);
			selectedMode = 3;
		}
		else if(mode == 4)
		{
			relativeMode1.setBackgroundResource(R.drawable.d_regular);
			relativeMode2.setBackgroundResource(R.drawable.d_regular);
			relativeMode3.setBackgroundResource(R.drawable.d_regular);
			relativeMode4.setBackgroundResource(R.drawable.d_enable);
			selectedMode = 4;
		}
	}
	
	
	
	public void  LoadInfo()
	{
		if(settings.getInt("Mode0Tone",1)==1)
		{
			L1_modesettingsImageOnOff.setImageResource(R.drawable.on);
			L1_SoundOn = true;
		}
		else
		{
			L1_modesettingsImageOnOff.setImageResource(R.drawable.off);
			L1_SoundOn=false;
		}
			
		if(settings.getInt("Mode0Notif",1)==1)
		{
			L1_modesettingsImageball_1.setImageResource(R.drawable.ds_enable);
			L1_modesettingsImageball_2.setImageResource(R.drawable.ds_disable);
			L1_modesettingsImageball_3.setImageResource(R.drawable.ds_disable);
		}
		else if(settings.getInt("Mode0Notif",1)==2)
		{
			L1_modesettingsImageball_1.setImageResource(R.drawable.ds_disable);
			L1_modesettingsImageball_2.setImageResource(R.drawable.ds_enable);
			L1_modesettingsImageball_3.setImageResource(R.drawable.ds_disable);
		}
		else if(settings.getInt("Mode0Notif",1)==3)
		{
			L1_modesettingsImageball_1.setImageResource(R.drawable.ds_disable);
			L1_modesettingsImageball_2.setImageResource(R.drawable.ds_disable);
			L1_modesettingsImageball_3.setImageResource(R.drawable.ds_enable);
		}
			
			
		if(settings.getInt("Mode0Appl",1)==1)
		{
			L1_modesettingsRelativeLogoG.setBackgroundResource(R.drawable.rectangle_search_white_corners);
			L1_modesettingsRelativeLogoT.setBackgroundResource(R.drawable.rectangle_white_alpha_corners);
			L1_modesettingsImageBallG.setImageResource(R.drawable.ds_enable);
			L1_modesettingsImageBallT.setImageResource(R.drawable.ds_disable);
		}
		else
		{
			L1_modesettingsRelativeLogoG.setBackgroundResource(R.drawable.rectangle_white_alpha_corners);
			L1_modesettingsRelativeLogoT.setBackgroundResource(R.drawable.rectangle_search_white_corners);
			L1_modesettingsImageBallG.setImageResource(R.drawable.ds_disable);
			L1_modesettingsImageBallT.setImageResource(R.drawable.ds_enable);
		}
		
		
		if(settings.getInt("Mode1Tone",1)==1)
		{
			L2_modesettingsImageOnOff.setImageResource(R.drawable.on);
			L2_SoundOn = true;
		}
		else
		{
			L2_modesettingsImageOnOff.setImageResource(R.drawable.off);
			L2_SoundOn=false;
		}
			
		
		if(settings.getInt("Mode1Notif",2)==1)
		{
			L2_modesettingsImageball_1.setImageResource(R.drawable.ds_enable);
			L2_modesettingsImageball_2.setImageResource(R.drawable.ds_disable);
			L2_modesettingsImageball_3.setImageResource(R.drawable.ds_disable);
		}
		else if(settings.getInt("Mode1Notif",2)==2)
		{
			L2_modesettingsImageball_1.setImageResource(R.drawable.ds_disable);
			L2_modesettingsImageball_2.setImageResource(R.drawable.ds_enable);
			L2_modesettingsImageball_3.setImageResource(R.drawable.ds_disable);
		}
		else if(settings.getInt("Mode1Notif",2)==3)
		{
			L2_modesettingsImageball_1.setImageResource(R.drawable.ds_disable);
			L2_modesettingsImageball_2.setImageResource(R.drawable.ds_disable);
			L2_modesettingsImageball_3.setImageResource(R.drawable.ds_enable);
		}
			
		if(settings.getInt("Mode1Appl",1)==1)
		{
			L2_modesettingsRelativeLogoG.setBackgroundResource(R.drawable.rectangle_search_white_corners);
			L2_modesettingsRelativeLogoT.setBackgroundResource(R.drawable.rectangle_white_alpha_corners);
			L2_modesettingsImageBallG.setImageResource(R.drawable.ds_enable);
			L2_modesettingsImageBallT.setImageResource(R.drawable.ds_disable);
		}
		else
		{
			L2_modesettingsRelativeLogoG.setBackgroundResource(R.drawable.rectangle_white_alpha_corners);
			L2_modesettingsRelativeLogoT.setBackgroundResource(R.drawable.rectangle_search_white_corners);
			L2_modesettingsImageBallG.setImageResource(R.drawable.ds_disable);
			L2_modesettingsImageBallT.setImageResource(R.drawable.ds_enable);
		}
		
		
		if(settings.getInt("Mode2Tone",0)==1)
		{
			L3_modesettingsImageOnOff.setImageResource(R.drawable.on);
			L3_SoundOn = true;
		}
		else
		{
			L3_modesettingsImageOnOff.setImageResource(R.drawable.off);
			L3_SoundOn=false;
		}
		
		
		if(settings.getInt("Mode2Notif",3)==1)
		{
			L3_modesettingsImageball_1.setImageResource(R.drawable.ds_enable);
			L3_modesettingsImageball_2.setImageResource(R.drawable.ds_disable);
			L3_modesettingsImageball_3.setImageResource(R.drawable.ds_disable);
		}
		else if(settings.getInt("Mode2Notif",3)==2)
		{
			L3_modesettingsImageball_1.setImageResource(R.drawable.ds_disable);
			L3_modesettingsImageball_2.setImageResource(R.drawable.ds_enable);
			L3_modesettingsImageball_3.setImageResource(R.drawable.ds_disable);
		}
		else if(settings.getInt("Mode2Notif",3)==3)
		{
			L3_modesettingsImageball_1.setImageResource(R.drawable.ds_disable);
			L3_modesettingsImageball_2.setImageResource(R.drawable.ds_disable);
			L3_modesettingsImageball_3.setImageResource(R.drawable.ds_enable);
		}
		
		if(settings.getInt("Mode2Appl",2)==1)
		{
			L3_modesettingsRelativeLogoG.setBackgroundResource(R.drawable.rectangle_search_white_corners);
			L3_modesettingsRelativeLogoT.setBackgroundResource(R.drawable.rectangle_white_alpha_corners);
			L3_modesettingsImageBallG.setImageResource(R.drawable.ds_enable);
			L3_modesettingsImageBallT.setImageResource(R.drawable.ds_disable);
		}
		else
		{
			L3_modesettingsRelativeLogoG.setBackgroundResource(R.drawable.rectangle_white_alpha_corners);
			L3_modesettingsRelativeLogoT.setBackgroundResource(R.drawable.rectangle_search_white_corners);
			L3_modesettingsImageBallG.setImageResource(R.drawable.ds_disable);
			L3_modesettingsImageBallT.setImageResource(R.drawable.ds_enable);
		}
	
		
		if(settings.getInt("Mode3Tone",0)==1)
		{
			L4_modesettingsImageOnOff.setImageResource(R.drawable.on);
			L4_SoundOn = true;
		}
		else
		{
			L4_modesettingsImageOnOff.setImageResource(R.drawable.off);
			L4_SoundOn=false;
		}
		
		if(settings.getInt("Mode3Notif",3)==1)
		{
			L4_modesettingsImageball_1.setImageResource(R.drawable.ds_enable);
			L4_modesettingsImageball_2.setImageResource(R.drawable.ds_disable);
			L4_modesettingsImageball_3.setImageResource(R.drawable.ds_disable);
		}
		else if(settings.getInt("Mode3Notif",3)==2)
		{
			L4_modesettingsImageball_1.setImageResource(R.drawable.ds_disable);
			L4_modesettingsImageball_2.setImageResource(R.drawable.ds_enable);
			L4_modesettingsImageball_3.setImageResource(R.drawable.ds_disable);
		}
		else if(settings.getInt("Mode3Notif",3)==3)
		{
			L4_modesettingsImageball_1.setImageResource(R.drawable.ds_disable);
			L4_modesettingsImageball_2.setImageResource(R.drawable.ds_disable);
			L4_modesettingsImageball_3.setImageResource(R.drawable.ds_enable);
		}
		
		if(settings.getInt("Mode3Appl",2)==1)
		{
			L4_modesettingsRelativeLogoG.setBackgroundResource(R.drawable.rectangle_search_white_corners);
			L4_modesettingsRelativeLogoT.setBackgroundResource(R.drawable.rectangle_white_alpha_corners);
			L4_modesettingsImageBallG.setImageResource(R.drawable.ds_enable);
			L4_modesettingsImageBallT.setImageResource(R.drawable.ds_disable);
		}
		else
		{
			L4_modesettingsRelativeLogoG.setBackgroundResource(R.drawable.rectangle_white_alpha_corners);
			L4_modesettingsRelativeLogoT.setBackgroundResource(R.drawable.rectangle_search_white_corners);
			L4_modesettingsImageBallG.setImageResource(R.drawable.ds_disable);
			L4_modesettingsImageBallT.setImageResource(R.drawable.ds_enable);
		}
		
	}

}

package com.goomena.fragments;

import java.util.ArrayList;

import com.goomena.app.ActHome;
import com.goomena.app.ActionBarChange;
import com.goomena.app.R;
import com.goomena.app.adapters.ItemGridProfileAdapter;
import com.goomena.app.adapters.LikesAdapters;
import com.goomena.app.custom.StaggeredGridView;
import com.goomena.app.data.ProfileManager;
import com.goomena.app.data.ServiceClient;
import com.goomena.app.global.Global;
import com.goomena.app.models.UserProfile;
import com.goomena.app.models.UserProfilesList;
import com.goomena.app.tools.Log;
import com.goomena.app.tools.Utils;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView.OnItemClickListener;

@SuppressLint({ "NewApi", "ValidFragment" })
public class FragGridProfile extends Fragment
{
	int _mode;
	private ItemGridProfileAdapter adapter;
	static UserProfilesList profilesListFull;
	String onString = "0";
	boolean onlineUser = false;
	
	StaggeredGridView profileGridView;
	FragLoading fragLoading;
	Animation animFadein;
	
	boolean allowCall = true;
	private final static String TAG_FRAGMENT = "TAG_FRAGMENT";
	UserProfile Myprofile;
	
	RelativeLayout include_view_no_result;
	TextView view_noresult_textTitle;
	
	//for Search
	String _name="";
	int _bitOnline;
	int _bitPhoto;
	int _bitVip;
	int _bitHasPrivatePhotos;
	int _bitTravel;
	String _distance;
	String _agemin;
	String _agemax;
	Handler handler;
	
	
	static Boolean firstSee = false;
	
	static UserProfilesList profilesListFullFavorite=null;
	
	static UserProfilesList profilesListFullWhoViewedMe=null;
	
	static UserProfilesList profilesListFullWhoFavedMe=null;
	
	static UserProfilesList profilesListFullWhomAccess=null;
	
	static UserProfilesList profilesListFullWhoAccessMe=null;
	
	static UserProfilesList profilesListFullBlockedList=null;
	
	
	
	public FragGridProfile(int mode)
	{
		this._mode = mode;
		profilesListFull = new UserProfilesList();
		profilesListFull.Profiles = new ArrayList<UserProfile>();
		
		
		if(mode==1)
		{
			if(profilesListFullFavorite==null)
			{
				firstSee=true;
				profilesListFullFavorite = new UserProfilesList();
				profilesListFullFavorite.Profiles = new ArrayList<UserProfile>();
				profilesListFull = profilesListFullFavorite;
			}
			else
			{
				firstSee=false;
				profilesListFull = profilesListFullFavorite;
			}
		}
		
		if(mode==2)
		{
			if(profilesListFullWhoViewedMe==null)
			{
				firstSee=true;
				profilesListFullWhoViewedMe = new UserProfilesList();
				profilesListFullWhoViewedMe.Profiles = new ArrayList<UserProfile>();
			}
			else
			{
				firstSee=false;
				profilesListFull.Profiles.addAll(profilesListFullWhoViewedMe.Profiles);
				profilesListFullWhoViewedMe.Profiles.clear();
			}
		}
		
		if(mode==3)
		{
			if(profilesListFullWhoFavedMe==null)
			{
				firstSee=true;
				profilesListFullWhoFavedMe = new UserProfilesList();
				profilesListFullWhoFavedMe.Profiles = new ArrayList<UserProfile>();
				profilesListFull = profilesListFullWhoFavedMe;
			}
			else
			{
				firstSee=false;
				profilesListFull = profilesListFullWhoFavedMe;
			}
		}
		
		if(mode==4)
		{
			if(profilesListFullWhomAccess==null)
			{
				firstSee=true;
				profilesListFullWhomAccess = new UserProfilesList();
				profilesListFullWhomAccess.Profiles = new ArrayList<UserProfile>();
			}
			else
			{
				firstSee=false;
				profilesListFull.Profiles.addAll(profilesListFullWhomAccess.Profiles);
				profilesListFullWhomAccess.Profiles.clear();
			}
		}
		
		if(mode==5)
		{
			if(profilesListFullWhoAccessMe==null)
			{
				firstSee=true;
				profilesListFullWhoAccessMe = new UserProfilesList();
				profilesListFullWhoAccessMe.Profiles = new ArrayList<UserProfile>();
			}
			else
			{
				firstSee=false;
				profilesListFull.Profiles.addAll(profilesListFullWhoAccessMe.Profiles);
				profilesListFullWhoAccessMe.Profiles.clear();
			}
		}
		
		if(mode==6)
		{
			if(profilesListFullBlockedList==null)
			{
				firstSee=true;
				profilesListFullBlockedList = new UserProfilesList();
				profilesListFullBlockedList.Profiles = new ArrayList<UserProfile>();
			}
			else
			{
				firstSee=false;
				profilesListFull.Profiles.addAll(profilesListFullBlockedList.Profiles);
				profilesListFullBlockedList.Profiles.clear();
			}
		}
			
		
		
		
		
		//new getFavoritesTask().execute();
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup group, Bundle saved)
	{
		return inflater.inflate(R.layout.fr_grid_profile, group, false);
	}
	
	@Override
	public void onActivityCreated (Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);

		//ActionBarChange.changeActionBar(getActivity(), 1);
		Myprofile = ProfileManager.loadProfile(getActivity());
		
		fragLoading = new FragLoading(getActivity());
		
		
		include_view_no_result = (RelativeLayout) getActivity().findViewById(R.id.include_view_no_result);
		view_noresult_textTitle = (TextView) getActivity().findViewById(R.id.view_noresult_textTitle);
		profileGridView = (StaggeredGridView) getActivity().findViewById(R.id.profileGrid);
		adapter = new ItemGridProfileAdapter(getActivity(),_mode);
		
		profileGridView.setAdapter(adapter);
		profileGridView.setOnScrollListener(myOnScrollListener);
		profileGridView.setOnItemClickListener(myOnItemClickListener);
		
		
		
		
		if(_mode==1)
		{
			new getFavoritesTask().execute();
			adapter.setItems(profilesListFullFavorite.Profiles);
			adapter.notifyDataSetChanged();
		}
		else if(_mode==2)
		{
			new getFavoritesTask().execute();
			adapter.setItems(profilesListFull.Profiles);
			adapter.notifyDataSetChanged();
		}
		else if(_mode==3)
		{
			new getFavoritesTask().execute();
			adapter.setItems(profilesListFullWhoFavedMe.Profiles);
			adapter.notifyDataSetChanged();
		}
		else if(_mode==4)
		{
			new getFavoritesTask().execute();
			adapter.setItems(profilesListFull.Profiles);
			adapter.notifyDataSetChanged();
		}
		else if(_mode==5)
		{
			new getFavoritesTask().execute();
			adapter.setItems(profilesListFull.Profiles);
			adapter.notifyDataSetChanged();
		}
		else if(_mode==6)
		{
			new getFavoritesTask().execute();
			adapter.setItems(profilesListFull.Profiles);
			adapter.notifyDataSetChanged();
		}
		else
		{
			if(profilesListFull.Profiles.size()==0)
			{
				new getFavoritesTask().execute();
			}
			adapter.setItems(profilesListFull.Profiles);
			adapter.notifyDataSetChanged();
		}
		
	}
	
	
	
	
	class getFavoritesTask extends AsyncTask<Void,Integer,UserProfilesList>{

		@Override
		protected void onPreExecute()
		{
			if(_mode==1 || _mode==2 || _mode==3 || _mode==4 || _mode==5 || _mode==6)
			{
				if(firstSee)
				{
					fragLoading.Show();
				}
			}
			else
			{
				fragLoading.Show(); 
			}
		}
		
		@Override
		protected UserProfilesList doInBackground(Void... params) 
		{
			UserProfilesList profilesList = null;
			if(_mode == 0) //Who is online evala thn allh giati vgazei perisotera apotelesmata
			{
				//profilesList = ServiceClient.getNewMembers(Myprofile.ProfileId ,String.valueOf(adapter.getCount()), "20" ,Myprofile.GenderId, "1", Utils.getSharedPreferences(getActivity()).getString("UserCountry",""));
				profilesList = ServiceClient.getSearchResults_v2(Myprofile.ProfileId, "1", Myprofile.ZipCode, Myprofile.Latitude, Myprofile.Longtitude, "9000", 
						Myprofile.GenderId, "0" ,"%20", "18", "103",
						"0","1", "0", String.valueOf(adapter.getCount()), "20", Utils.getSharedPreferences(getActivity()).getString("UserCountry",""),"0","0");
			}
			else if(_mode == 1)
			{
				profilesList = ServiceClient.getFavoriteList_v2(Myprofile.ProfileId,Myprofile.ZipCode,Myprofile.Latitude,Myprofile.Longtitude,onString);
			}
			else if(_mode == 2)
			{
				profilesList = ServiceClient.getWhoViewedMe(Myprofile.ProfileId,Myprofile.ZipCode,Myprofile.Latitude,Myprofile.Longtitude,String.valueOf(profilesListFullWhoViewedMe.Profiles.size()));
			}
			else if(_mode == 3)
			{
				profilesList = ServiceClient.getWhoFavedMe(Myprofile.ProfileId,Myprofile.ZipCode,Myprofile.Latitude,Myprofile.Longtitude);
			}
			else if(_mode == 4)
			{
				profilesList = ServiceClient.getWhomAccess(Myprofile.ProfileId,Myprofile.ZipCode,Myprofile.Latitude,Myprofile.Longtitude, profilesListFullWhomAccess.Profiles.size());
			}
			else if(_mode == 5)
			{
				profilesList = ServiceClient.getWhoAccessMe(Myprofile.ProfileId,Myprofile.ZipCode,Myprofile.Latitude,Myprofile.Longtitude, profilesListFullWhoAccessMe.Profiles.size());
			}
			else if(_mode == 6) // BlockedList
			{
				profilesList = ServiceClient.getBlockedList(Myprofile.ProfileId,Myprofile.Latitude,Myprofile.Longtitude, profilesListFullBlockedList.Profiles.size());
			}
			else if(_mode == 11)
			{
				profilesList = ServiceClient.getSearchResults(Myprofile.ProfileId, "4", Myprofile.ZipCode, Myprofile.Latitude, Myprofile.Longtitude, "100", Myprofile.GenderId,
						"0", "a", "-1", "-1", "1", "0",String.valueOf(adapter.getCount()),"20" );
			}
			else if(_mode == 12)
			{
				profilesList = ServiceClient.getSearchResults(Myprofile.ProfileId, "1", Myprofile.ZipCode, Myprofile.Latitude, Myprofile.Longtitude, "100", Myprofile.GenderId,
						"0", "a", "-1", "-1", "1", "0",String.valueOf(adapter.getCount()),"20" );
			}
			else if(_mode == 13)
			{
				profilesList = ServiceClient.getSearchResults(Myprofile.ProfileId, "2", Myprofile.ZipCode, Myprofile.Latitude, Myprofile.Longtitude, "100", Myprofile.GenderId,
						"0", "a", "-1", "-1", "1", "0",String.valueOf(adapter.getCount()),"20" );
			}
			else if(_mode == 14)
			{
				profilesList = ServiceClient.getSearchResults(Myprofile.ProfileId, "3", Myprofile.ZipCode, Myprofile.Latitude, Myprofile.Longtitude, "100", Myprofile.GenderId,
						"0", "a", "-1", "-1", "1", "0",String.valueOf(adapter.getCount()),"20" );
			}
			else if(_mode == 15)
			{
				profilesList = ServiceClient.getSearchResults_v2(Myprofile.ProfileId, "1", Myprofile.ZipCode, Myprofile.Latitude, Myprofile.Longtitude, "1200", 
						Myprofile.GenderId, "0" ,"%20", "18", "115",
						"0", "0", "1", String.valueOf(adapter.getCount()), "20", Utils.getSharedPreferences(getActivity()).getString("UserCountry",""),"0","0");
			}
			else if(_mode == 21)
			{
				_name = _name.replace(" ", "%20");
				profilesList = ServiceClient.getSearchResults_v2(Myprofile.ProfileId, "1", Myprofile.ZipCode, Myprofile.Latitude, Myprofile.Longtitude, "1200", 
						Myprofile.GenderId, "1" ,_name, "-1", "-1",
						"0", "0", "0", String.valueOf(adapter.getCount()), "20", Utils.getSharedPreferences(getActivity()).getString("UserCountry",""),"0","0");
			}
			else if(_mode == 22)
			{
				profilesList = ServiceClient.getSearchResults_v2(Myprofile.ProfileId, "1", Myprofile.ZipCode, Myprofile.Latitude, Myprofile.Longtitude, _distance, 
						Myprofile.GenderId, "0" ,"%20", _agemin, _agemax,
						String.valueOf(_bitPhoto), String.valueOf(_bitOnline), String.valueOf(_bitVip), String.valueOf(adapter.getCount()), "20", Utils.getSharedPreferences(getActivity()).getString("UserCountry",""),String.valueOf(_bitHasPrivatePhotos),String.valueOf(_bitTravel));
			}
			return profilesList;
		}
		
		@Override
		protected void onPostExecute(UserProfilesList result){
			if(result!=null)
			{
				if(_mode == 1 ||_mode == 3)
				{
					
					if(_mode==1)
					{
						profilesListFullFavorite.Profiles = result.Profiles;
						adapter.setItems(profilesListFullFavorite.Profiles);
						adapter.notifyDataSetChanged();
						
						if(profilesListFullFavorite.Profiles.size()==0)
						{
							view_noresult_textTitle.setText(getActivity().getResources().getString(R.string.noresult_title_my_favorites));
							include_view_no_result.setVisibility(View.VISIBLE);
						}
						
						allowCall = false;
					}
					else if(_mode == 3)
					{
						profilesListFullWhoFavedMe.Profiles = result.Profiles;
						adapter.setItems(profilesListFullWhoFavedMe.Profiles);
						adapter.notifyDataSetChanged();
						
						if(profilesListFullWhoFavedMe.Profiles.size()==0)
						{
							view_noresult_textTitle.setText(getActivity().getResources().getString(R.string.noresult_title_who_added_me_as_a_favorite));
							include_view_no_result.setVisibility(View.VISIBLE);
						}
					
						allowCall = false;
					}
				}
				else
				{
					if(_mode==2)
					{
						profilesListFullWhoViewedMe.Profiles.addAll(result.Profiles);
						adapter.setItems(profilesListFullWhoViewedMe.Profiles);
						adapter.notifyDataSetChanged();
						profilesListFull = profilesListFullWhoViewedMe;
						
						
						if(profilesListFullWhoViewedMe.Profiles.size()==0)
						{
							view_noresult_textTitle.setText(getActivity().getResources().getString(R.string.noresult_title_who_view_me));
							include_view_no_result.setVisibility(View.VISIBLE);
						}
					}
					else if(_mode==4)
					{
						profilesListFullWhomAccess.Profiles.addAll(result.Profiles);
						adapter.setItems(profilesListFullWhomAccess.Profiles);
						adapter.notifyDataSetChanged();
						profilesListFull = profilesListFullWhomAccess;
						
						if(profilesListFullWhomAccess.Profiles.size()==0)
						{
							view_noresult_textTitle.setText(getActivity().getResources().getString(R.string.noresult_title_you_havent_given_access));
							include_view_no_result.setVisibility(View.VISIBLE);
						}
					}
					else if(_mode==5)
					{
						profilesListFullWhoAccessMe.Profiles.addAll(result.Profiles);
						adapter.setItems(profilesListFullWhoAccessMe.Profiles);
						adapter.notifyDataSetChanged();
						profilesListFull = profilesListFullWhoAccessMe;
						
						if(profilesListFullWhoAccessMe.Profiles.size()==0)
						{
							view_noresult_textTitle.setText(getActivity().getResources().getString(R.string.noresult_title_who_shered_photos));
							include_view_no_result.setVisibility(View.VISIBLE);
						}
					}
					else if(_mode==6)
					{
						profilesListFullBlockedList.Profiles.addAll(result.Profiles);
						adapter.setItems(profilesListFullBlockedList.Profiles);
						adapter.notifyDataSetChanged();
						profilesListFull = profilesListFullBlockedList;
						
						if(profilesListFullBlockedList.Profiles.size()==0)
						{
							view_noresult_textTitle.setText(getActivity().getResources().getString(R.string.noresult_title_who_blocked_list));
							include_view_no_result.setVisibility(View.VISIBLE);
						}
					}
					else
					{
						profilesListFull.Profiles.addAll(result.Profiles);
						adapter.setItems(profilesListFull.Profiles);
						adapter.notifyDataSetChanged();
					}
					
					if(result.Profiles.size()<20)
					{
						allowCall = false;
					}
					else
					{
						allowCall = true;
						firstSee=true;
					}
				}
			}
			fragLoading.Close();
		}
		
	}
	
	
	
	
	OnScrollListener myOnScrollListener = new OnScrollListener() 
	{
		@Override
		public void onScrollStateChanged(AbsListView view, int scrollState) 
		{
			//Log.v("APP", "rtrtrtyy "+scrollState+" "+view);
			if(scrollState==1)
			{
				adapter.setAnim(true);
			}
			else if(scrollState==0)
			{
				adapter.setAnim(false);
			}
		}

		@Override
		public void onScroll(AbsListView view, int firstVisibleItem,int visibleItemCount, int totalItemCount) {
			
			//Log.v("APP", "rtrtrtyy "+firstVisibleItem+" "+visibleItemCount+" "+totalItemCount);
			
			if(firstVisibleItem+visibleItemCount==totalItemCount && visibleItemCount!=0 && totalItemCount!=0 && allowCall)
			{
				allowCall = false;
				new getFavoritesTask().execute();
			}
		}
	};
	
	OnItemClickListener myOnItemClickListener = new OnItemClickListener() 
	{
		@Override
		public void onItemClick(AdapterView<?> parent, final View view,int position, long id) 
		{
			firstSee=false;
			Fragment fr = new FragProfile(2,profilesListFull.Profiles,position);
			FragmentManager fm = getActivity().getFragmentManager();
		    FragmentTransaction fragmentTransaction = fm.beginTransaction();
		    fragmentTransaction.setCustomAnimations(R.anim.left_slidein_list_animator, R.anim.left_slideout_list_animator,R.anim.right_slidein_list_animator, R.anim.right_slideout_list_animator);
		    fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
		    fragmentTransaction.addToBackStack(null);
		    fragmentTransaction.commit();
		}
	};
	
	
	
	
	
	
	//for search
	
	public void searchByName(String name)
	{
		this._name = name;
	}
	
	public void searchByFilter(String agemin,String agemax,int bitOnline,int bitPhoto,int bitVip,int bitHasPrivatePhotos,int bitTravel,String distance)
	{
		this._agemin = agemin;
		this._agemax = agemax;
		this._bitOnline = bitOnline;
		this._bitPhoto = bitPhoto;
		this._bitVip = bitVip;
		this._bitHasPrivatePhotos = bitHasPrivatePhotos;
		this._bitTravel = bitTravel;
		this._distance = distance;
		
	}

}

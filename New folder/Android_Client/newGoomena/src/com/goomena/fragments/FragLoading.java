package com.goomena.fragments;

import com.goomena.app.R;
import com.goomena.app.tools.Utils;

import android.app.Activity;
import android.os.Handler;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class FragLoading 
{
	Activity _activity;
	
	RelativeLayout home_loading;
	TextView dialog_waiting_textWaiting;
	ImageView dialog_waiting_bgLogo,dialog_waiting_FrLogo;
	Handler handler = new Handler();
	
	public FragLoading(Activity activity)
	{
		this._activity = activity;
		
		home_loading = (RelativeLayout) activity.findViewById(R.id.home_loading);
				
		dialog_waiting_textWaiting = (TextView) activity.findViewById(R.id.dialog_waiting_textWaiting);
		dialog_waiting_bgLogo = (ImageView) activity.findViewById(R.id.dialog_waiting_bgLogo);
		dialog_waiting_FrLogo = (ImageView) activity.findViewById(R.id.dialog_waiting_FrLogo);
		
		if(Utils.getSharedPreferences(activity).getString("genderid","").contains("2"))
		{
			dialog_waiting_FrLogo.setBackgroundResource(R.drawable.woman_loading);
		}
	}
	
	
	public void Show()
	{
		if(home_loading.getVisibility() == View.GONE)
		{
			home_loading.setVisibility(View.VISIBLE);
			home_loading.startAnimation(AnimationUtils.loadAnimation(_activity, R.anim.slide_up_dialog));
			dialog_waiting_textWaiting.startAnimation(AnimationUtils.loadAnimation(_activity, R.anim.d_wait_fr_load));
			dialog_waiting_bgLogo.startAnimation(AnimationUtils.loadAnimation(_activity, R.anim.d_wait_bg_logo));
		
			handler.postDelayed(new Runnable() 
			{ 
				public void run() 
				{ 
					if(Utils.getSharedPreferences(_activity).getString("genderid","").contains("2"))
					{
						dialog_waiting_textWaiting.setText(_activity.getResources().getString(R.string.never_on_time_classic));
					}
					else
					{
						dialog_waiting_textWaiting.setText(_activity.getResources().getString(R.string.the_ladies_always_late_patience));
					}
				} 
			}, 6000); 
		}
	}
	
	public void Close()
	{
		if(home_loading.getVisibility() == View.VISIBLE)
		{
			home_loading.setVisibility(View.GONE);
			home_loading.startAnimation(AnimationUtils.loadAnimation(_activity, R.anim.slide_out_down));
			dialog_waiting_textWaiting.clearAnimation();
			dialog_waiting_bgLogo.clearAnimation();
			dialog_waiting_textWaiting.setText("Loading");
		}
	}

}

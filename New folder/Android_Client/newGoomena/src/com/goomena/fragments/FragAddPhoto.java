package com.goomena.fragments;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

















import com.goomena.app.ActHome;
import com.goomena.app.ActionBarChange;
import com.goomena.app.MultiPhotoSellect;
import com.goomena.app.R;
import com.goomena.app.adapters.ExpandableHeightGridView;
import com.goomena.app.data.DataFetcher;
import com.goomena.app.models.ImagePath;
import com.goomena.app.models.ImagePathList;
import com.goomena.app.tools.Log;
import com.goomena.app.tools.Utils;
import com.goomena.fragments.FragMyPhoto.setPhotoLevelTask;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
 
@SuppressLint("NewApi") 
public class FragAddPhoto extends Fragment
{
	public final static int CAMERA_REQUEST = 1888;	
	public final static int GALLERY_REQUEST = 1889;	
	public final static int CHANGE_DEFAULT_REQUEST = 1890;
	
	private Bitmap bitmap;
	private String imageToByte ="";
	private Uri imageUri=null;
	private String pathOfImage = null;
	
	int levelFinal;
	byte[] byteArray;
	
	Bitmap bmpToUpload = null;
	Bitmap bmpProfile = null;
	
	boolean changeProfilePicture = false;
	ImagePath defaultImagePath = null;
	String notif;
	
	ImagePath a;
	ImagePathList b;
	
	RelativeLayout fr_add_photo_relativeTake,fr_add_photo_relativeAlbum,fr_add_photo_relativeStorage;
	RelativeLayout fr_add_photo_relativeMyprofile,fr_add_photo_relativeMyphoto,fr_add_photo_relativeEditProfile;
	
	MyImageAdapter adapter;
	
	Dialog dPhotos,d;
	private int progressStatus = 0;
	ProgressBar dialog_photos_ProgressBar,dialog_photos_ProgressBar2;
	private Handler handler = new Handler();
	
	private final static String TAG_FRAGMENT = "TAG_FRAGMENT";

	public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState) 
	{
		return inflater.inflate(R.layout.fr_add_photo, container, false);
	}
	
	public void onActivityCreated (Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);
		
		
		adapter = new MyImageAdapter(getActivity());
		
		b = new ImagePathList();
		b.PathList = new ArrayList<ImagePath>();
		
		fr_add_photo_relativeTake = (RelativeLayout) getActivity().findViewById(R.id.fr_add_photo_relativeTake);
		fr_add_photo_relativeAlbum = (RelativeLayout) getActivity().findViewById(R.id.fr_add_photo_relativeAlbum);
		fr_add_photo_relativeStorage = (RelativeLayout) getActivity().findViewById(R.id.fr_add_photo_relativeStorage);
		
		fr_add_photo_relativeMyprofile = (RelativeLayout) getActivity().findViewById(R.id.fr_add_photo_relativeMyprofile);
		fr_add_photo_relativeMyphoto = (RelativeLayout) getActivity().findViewById(R.id.fr_add_photo_relativeMyphoto);
		fr_add_photo_relativeEditProfile = (RelativeLayout) getActivity().findViewById(R.id.fr_add_photo_relativeEditProfile);
		
		
		fr_add_photo_relativeTake.setOnClickListener(click);
		fr_add_photo_relativeAlbum.setOnClickListener(click);
		fr_add_photo_relativeStorage.setOnClickListener(click);
		
		fr_add_photo_relativeMyprofile.setOnClickListener(click);
		fr_add_photo_relativeMyphoto.setOnClickListener(click);
		fr_add_photo_relativeEditProfile.setOnClickListener(click);
		
		fr_add_photo_relativeMyprofile.setBackgroundColor(getActivity().getResources().getColor(R.color.alphaGrey));
	}
	
	
	public void onActivityResult(int requestCode, int resultCode, Intent data) 
	{
		super.onActivityResult(requestCode,resultCode,data);

		if(requestCode==CAMERA_REQUEST)
		{
			bitmap = null;
			Uri selectedImageUri = imageUri;		
			
			if(resultCode!=0)
			{
				a = new ImagePath();
				a.Path = selectedImageUri.toString();
				a.photoType = "0";
				a.Level = "0";
					
				b.PathList.add(a);
				showDialogPhotos(b);
			}	
		}
		else if(requestCode==GALLERY_REQUEST)
		{
			bitmap = null;
			if(data!=null)
			{
				ArrayList<String> result = data.getStringArrayListExtra("result");
	            
	            if(result.size()!=0)
	            {
	            	for(int i=0;i<result.size();i++)
	            	{
	            		a = new ImagePath();
	            		a.Path = "file://"+result.get(i);
	            		a.photoType = "0";
	            		a.Level = "0";
	            		b.PathList.add(a);
	            	}
	            }
	            if(b.PathList.size()!=0)
	            {
	            	showDialogPhotos(b);
	            }
			}
		}
		else if(requestCode==CHANGE_DEFAULT_REQUEST)
		{
			if(data!=null)
			{
	        	//circle.setVisibility(View.GONE);	        	
	        	defaultImagePath = (ImagePath) data.getExtras().get("imagePath");
	    		//Utils.getImageLoader(getActivity()).displayImage(defaultImagePath.Path,profileimg,Utils.getImageOptions());
	        	//change.setText(getResources().getString(R.string.upload));
	        	changeProfilePicture = true;
			}
		}
	}
	
	
	
	OnClickListener click = new OnClickListener() 
	{
		@Override
		public void onClick(View v) 
		{
			if(v==fr_add_photo_relativeTake)
			{
				String name =   dateToString(new Date(),"yyyy-MM-dd-hh-mm-ss"); 
                ContentValues values = new ContentValues();
                values.put(MediaStore.Images.Media.TITLE, name);
                values.put(MediaStore.Images.Media.DESCRIPTION,"Image capture by camera");
                imageUri = getActivity().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
                startActivityForResult(intent, CAMERA_REQUEST);
			}
			else if(v==fr_add_photo_relativeAlbum)
			{
				Intent i = new Intent();
                i.setClass(getActivity(), MultiPhotoSellect.class);
                startActivityForResult(i,GALLERY_REQUEST);
			}
			else if(v==fr_add_photo_relativeStorage)
			{
				
			}
			else if(v == fr_add_photo_relativeMyprofile)
			{
				/*
				Fragment fr = new FragMyProfile();
				FragmentManager fm = getActivity().getFragmentManager();
				
		        FragmentTransaction fragmentTransaction = fm.beginTransaction();
		        fragmentTransaction.setCustomAnimations(R.anim.left_slidein_list_animator, R.anim.left_slideout_list_animator,R.anim.right_slidein_list_animator, R.anim.right_slideout_list_animator);
		        fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
		        fragmentTransaction.addToBackStack(null);
		
		        fragmentTransaction.commit();
		        */
			}
			else if(v == fr_add_photo_relativeMyphoto)
			{
				Fragment fr = new FragMyPhoto();
				FragmentManager fm = getActivity().getFragmentManager();
				
		        FragmentTransaction fragmentTransaction = fm.beginTransaction();
		        fragmentTransaction.setCustomAnimations(R.anim.left_slidein_list_animator, R.anim.left_slideout_list_animator,R.anim.right_slidein_list_animator, R.anim.right_slideout_list_animator);
		        fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
		        fragmentTransaction.addToBackStack(null);
		
		        fragmentTransaction.commit();
			}
			else if(v == fr_add_photo_relativeEditProfile)
			{
				Fragment fr = new FragEditProfile();
				FragmentManager fm = getActivity().getFragmentManager();
				
		        FragmentTransaction fragmentTransaction = fm.beginTransaction();
		        fragmentTransaction.setCustomAnimations(R.anim.left_slidein_list_animator, R.anim.left_slideout_list_animator,R.anim.right_slidein_list_animator, R.anim.right_slideout_list_animator);
		        fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
		        fragmentTransaction.addToBackStack(null);
		
		        fragmentTransaction.commit();
			}
		}
	};
	
	
	
	private void showDialogPhotos(ImagePathList list) 
	{
		dPhotos = new Dialog(getActivity(), R.style.DialogSlideAnim);
		dPhotos.setContentView(R.layout.dialog_photos);
		
		ExpandableHeightGridView dialog_photos_imageGrid = new ExpandableHeightGridView(getActivity());
		dialog_photos_ProgressBar = (ProgressBar) dPhotos.findViewById(R.id.dialog_photos_ProgressBar);
		dialog_photos_ProgressBar2 = (ProgressBar) dPhotos.findViewById(R.id.dialog_photos_ProgressBar2);
		dialog_photos_imageGrid = (ExpandableHeightGridView) dPhotos.findViewById(R.id.dialog_photos_imageGrid);
		final RelativeLayout dialog_photos_relativeComplete = (RelativeLayout) dPhotos.findViewById(R.id.dialog_photos_relativeComplete);
		RelativeLayout dialog_photos_relativeCancel = (RelativeLayout) dPhotos.findViewById(R.id.dialog_photos_relativeCancel);
	
		dialog_photos_imageGrid.setNumColumns(3);
		dialog_photos_imageGrid.setFocusable(false);
		dialog_photos_imageGrid.setExpanded(true);
        
		adapter.setItems(list);
		dialog_photos_imageGrid.setAdapter(adapter);
		
		dialog_photos_imageGrid.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,int position, long id) {
				showCategory(position);
			}
		});
		
		
		dialog_photos_relativeComplete.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				dialog_photos_relativeComplete.setBackgroundColor(getActivity().getResources().getColor(R.color.dialogPhoto));
				dialog_photos_relativeComplete.setOnClickListener(null);
				
				new uploadImagesTask().execute();
			}
		});
		
		dialog_photos_relativeCancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dPhotos.cancel();
			}
		});
		
		dPhotos.show();
	}
	

	Boolean Npublic1 = true;
	Boolean Npr1 = false;
	Boolean Npr2 = false;
	Boolean Npr3 = false;
	Boolean Npr4 = false;
	Boolean Npr5 = false;
	Boolean Npr6 = false;
	Boolean Npr7 = false;
	Boolean Npr8 = false;
    Boolean Npr9 = false;
	Boolean Npr10 = false;
	
	private void showCategory(final int position) 
	{

		final Dialog d = new Dialog(getActivity(), R.style.DialogSlideAnim);
		d.setContentView(R.layout.level_view_pro);
		Button cancelButton = (Button) d.findViewById(R.id.cancel_btn);
		Button okButton = (Button) d.findViewById(R.id.ok_btn);
		final LinearLayout public1 = (LinearLayout) d.findViewById(R.id.public1);
	    final LinearLayout pr1 = (LinearLayout) d.findViewById(R.id.private1);
	    final LinearLayout pr2 = (LinearLayout) d.findViewById(R.id.private2);
	    final LinearLayout pr3 = (LinearLayout) d.findViewById(R.id.private3);
		final LinearLayout pr4 = (LinearLayout) d.findViewById(R.id.private4);
	    final LinearLayout pr5 = (LinearLayout) d.findViewById(R.id.private5);
	    final LinearLayout pr6 = (LinearLayout) d.findViewById(R.id.private6);
		final LinearLayout pr7 = (LinearLayout) d.findViewById(R.id.private7);
	    final LinearLayout pr8 = (LinearLayout) d.findViewById(R.id.private8);
	    final LinearLayout pr9 = (LinearLayout) d.findViewById(R.id.private9);
		final LinearLayout pr10 = (LinearLayout) d.findViewById(R.id.private10);
		
		
		final ImageView impublic1 = (ImageView) d.findViewById(R.id.imageCheckpublic1);
	    final ImageView impr1 = (ImageView) d.findViewById(R.id.imageCheckprivate1);
	    final ImageView impr2 = (ImageView) d.findViewById(R.id.imageCheckprivate2);
	    final ImageView impr3 = (ImageView) d.findViewById(R.id.imageCheckprivate3);
		final ImageView impr4 = (ImageView) d.findViewById(R.id.imageCheckprivate4);
	    final ImageView impr5 = (ImageView) d.findViewById(R.id.imageCheckprivate5);
	    final ImageView impr6 = (ImageView) d.findViewById(R.id.imageCheckprivate6);
		final ImageView impr7 = (ImageView) d.findViewById(R.id.imageCheckprivate7);
	    final ImageView impr8 = (ImageView) d.findViewById(R.id.imageCheckprivate8);
	    final ImageView impr9 = (ImageView) d.findViewById(R.id.imageCheckprivate9);
		final ImageView impr10 = (ImageView) d.findViewById(R.id.imageCheckprivate10);
		
		Npublic1 = true;
		Npr1 = false;
		Npr2 = false;
		Npr3 = false;
		Npr4 = false;
		Npr5 = false;
		Npr6 = false;
		Npr7 = false;
		Npr8 = false;
	    Npr9 = false;
		Npr10 = false;
	    
		OnClickListener ffff = new OnClickListener()
		{

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				if(v == public1)
				{
					public1.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr1.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr2.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr3.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr4.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr5.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr6.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr7.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr8.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr9.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr10.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					
					impublic1.setBackgroundResource(R.drawable.search_check);
					impr1.setBackgroundResource(R.drawable.search_unckeck);
					impr2.setBackgroundResource(R.drawable.search_unckeck);
					impr3.setBackgroundResource(R.drawable.search_unckeck);
					impr4.setBackgroundResource(R.drawable.search_unckeck);
					impr5.setBackgroundResource(R.drawable.search_unckeck);
					impr6.setBackgroundResource(R.drawable.search_unckeck);
					impr7.setBackgroundResource(R.drawable.search_unckeck);
					impr8.setBackgroundResource(R.drawable.search_unckeck);
					impr9.setBackgroundResource(R.drawable.search_unckeck);
					impr10.setBackgroundResource(R.drawable.search_unckeck);
					
					Npublic1 = true;
					Npr1 = false;
					Npr2 = false;
					Npr3 = false;
					Npr4 = false;
					Npr5 = false;
					Npr6 = false;
					Npr7 = false;
					Npr8 = false;
				    Npr9 = false;
					Npr10 = false;
				}
				else if(v == pr1)
				{
					public1.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr1.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr2.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr3.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr4.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr5.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr6.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr7.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr8.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr9.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr10.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					
					impublic1.setBackgroundResource(R.drawable.search_unckeck);
					impr1.setBackgroundResource(R.drawable.search_check);
					impr2.setBackgroundResource(R.drawable.search_unckeck);
					impr3.setBackgroundResource(R.drawable.search_unckeck);
					impr4.setBackgroundResource(R.drawable.search_unckeck);
					impr5.setBackgroundResource(R.drawable.search_unckeck);
					impr6.setBackgroundResource(R.drawable.search_unckeck);
					impr7.setBackgroundResource(R.drawable.search_unckeck);
					impr8.setBackgroundResource(R.drawable.search_unckeck);
					impr9.setBackgroundResource(R.drawable.search_unckeck);
					impr10.setBackgroundResource(R.drawable.search_unckeck);
					
					Npublic1 = false;
					Npr1 = true;
					Npr2 = false;
					Npr3 = false;
					Npr4 = false;
					Npr5 = false;
					Npr6 = false;
					Npr7 = false;
					Npr8 = false;
				    Npr9 = false;
					Npr10 = false;
				}
				else if(v == pr2)
				{
					public1.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr1.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr2.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr3.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr4.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr5.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr6.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr7.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr8.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr9.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr10.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					
					impublic1.setBackgroundResource(R.drawable.search_unckeck);
					impr1.setBackgroundResource(R.drawable.search_unckeck);
					impr2.setBackgroundResource(R.drawable.search_check);
					impr3.setBackgroundResource(R.drawable.search_unckeck);
					impr4.setBackgroundResource(R.drawable.search_unckeck);
					impr5.setBackgroundResource(R.drawable.search_unckeck);
					impr6.setBackgroundResource(R.drawable.search_unckeck);
					impr7.setBackgroundResource(R.drawable.search_unckeck);
					impr8.setBackgroundResource(R.drawable.search_unckeck);
					impr9.setBackgroundResource(R.drawable.search_unckeck);
					impr10.setBackgroundResource(R.drawable.search_unckeck);
					
					Npublic1 = false;
					Npr1 = false;
					Npr2 = true;
					Npr3 = false;
					Npr4 = false;
					Npr5 = false;
					Npr6 = false;
					Npr7 = false;
					Npr8 = false;
				    Npr9 = false;
					Npr10 = false;
				}
				else if(v == pr3)
				{
					public1.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr1.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr2.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr3.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr4.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr5.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr6.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr7.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr8.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr9.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr10.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					
					impublic1.setBackgroundResource(R.drawable.search_unckeck);
					impr1.setBackgroundResource(R.drawable.search_unckeck);
					impr2.setBackgroundResource(R.drawable.search_unckeck);
					impr3.setBackgroundResource(R.drawable.search_check);
					impr4.setBackgroundResource(R.drawable.search_unckeck);
					impr5.setBackgroundResource(R.drawable.search_unckeck);
					impr6.setBackgroundResource(R.drawable.search_unckeck);
					impr7.setBackgroundResource(R.drawable.search_unckeck);
					impr8.setBackgroundResource(R.drawable.search_unckeck);
					impr9.setBackgroundResource(R.drawable.search_unckeck);
					impr10.setBackgroundResource(R.drawable.search_unckeck);
					
					Npublic1 = false;
					Npr1 = false;
					Npr2 = false;
					Npr3 = true;
					Npr4 = false;
					Npr5 = false;
					Npr6 = false;
					Npr7 = false;
					Npr8 = false;
				    Npr9 = false;
					Npr10 = false;
				}
				else if(v == pr4)
				{
					public1.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr1.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr2.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr3.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr4.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr5.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr6.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr7.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr8.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr9.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr10.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					
					impublic1.setBackgroundResource(R.drawable.search_unckeck);
					impr1.setBackgroundResource(R.drawable.search_unckeck);
					impr2.setBackgroundResource(R.drawable.search_unckeck);
					impr3.setBackgroundResource(R.drawable.search_unckeck);
					impr4.setBackgroundResource(R.drawable.search_check);
					impr5.setBackgroundResource(R.drawable.search_unckeck);
					impr6.setBackgroundResource(R.drawable.search_unckeck);
					impr7.setBackgroundResource(R.drawable.search_unckeck);
					impr8.setBackgroundResource(R.drawable.search_unckeck);
					impr9.setBackgroundResource(R.drawable.search_unckeck);
					impr10.setBackgroundResource(R.drawable.search_unckeck);
					
					Npublic1 = false;
					Npr1 = false;
					Npr2 = false;
					Npr3 = false;
					Npr4 = true;
					Npr5 = false;
					Npr6 = false;
					Npr7 = false;
					Npr8 = false;
				    Npr9 = false;
					Npr10 = false;
				}
				else if(v == pr5)
				{
					public1.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr1.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr2.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr3.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr4.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr5.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr6.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr7.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr8.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr9.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr10.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					
					impublic1.setBackgroundResource(R.drawable.search_unckeck);
					impr1.setBackgroundResource(R.drawable.search_unckeck);
					impr2.setBackgroundResource(R.drawable.search_unckeck);
					impr3.setBackgroundResource(R.drawable.search_unckeck);
					impr4.setBackgroundResource(R.drawable.search_unckeck);
					impr5.setBackgroundResource(R.drawable.search_check);
					impr6.setBackgroundResource(R.drawable.search_unckeck);
					impr7.setBackgroundResource(R.drawable.search_unckeck);
					impr8.setBackgroundResource(R.drawable.search_unckeck);
					impr9.setBackgroundResource(R.drawable.search_unckeck);
					impr10.setBackgroundResource(R.drawable.search_unckeck);
					
					Npublic1 = false;
					Npr1 = false;
					Npr2 = false;
					Npr3 = false;
					Npr4 = false;
					Npr5 = true;
					Npr6 = false;
					Npr7 = false;
					Npr8 = false;
				    Npr9 = false;
					Npr10 = false;
				}
				else if(v == pr6)
				{
					public1.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr1.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr2.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr3.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr4.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr5.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr6.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr7.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr8.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr9.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr10.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					
					impublic1.setBackgroundResource(R.drawable.search_unckeck);
					impr1.setBackgroundResource(R.drawable.search_unckeck);
					impr2.setBackgroundResource(R.drawable.search_unckeck);
					impr3.setBackgroundResource(R.drawable.search_unckeck);
					impr4.setBackgroundResource(R.drawable.search_unckeck);
					impr5.setBackgroundResource(R.drawable.search_unckeck);
					impr6.setBackgroundResource(R.drawable.search_check);
					impr7.setBackgroundResource(R.drawable.search_unckeck);
					impr8.setBackgroundResource(R.drawable.search_unckeck);
					impr9.setBackgroundResource(R.drawable.search_unckeck);
					impr10.setBackgroundResource(R.drawable.search_unckeck);
					
					Npublic1 = false;
					Npr1 = false;
					Npr2 = false;
					Npr3 = false;
					Npr4 = false;
					Npr5 = false;
					Npr6 = true;
					Npr7 = false;
					Npr8 = false;
				    Npr9 = false;
					Npr10 = false;
				}
				else if(v == pr7)
				{
					public1.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr1.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr2.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr3.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr4.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr5.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr6.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr7.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr8.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr9.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr10.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					
					impublic1.setBackgroundResource(R.drawable.search_unckeck);
					impr1.setBackgroundResource(R.drawable.search_unckeck);
					impr2.setBackgroundResource(R.drawable.search_unckeck);
					impr3.setBackgroundResource(R.drawable.search_unckeck);
					impr4.setBackgroundResource(R.drawable.search_unckeck);
					impr5.setBackgroundResource(R.drawable.search_unckeck);
					impr6.setBackgroundResource(R.drawable.search_unckeck);
					impr7.setBackgroundResource(R.drawable.search_check);
					impr8.setBackgroundResource(R.drawable.search_unckeck);
					impr9.setBackgroundResource(R.drawable.search_unckeck);
					impr10.setBackgroundResource(R.drawable.search_unckeck);
					
					Npublic1 = false;
					Npr1 = false;
					Npr2 = false;
					Npr3 = false;
					Npr4 = false;
					Npr5 = false;
					Npr6 = false;
					Npr7 = true;
					Npr8 = false;
				    Npr9 = false;
					Npr10 = false;
				}
				else if(v == pr8)
				{
					public1.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr1.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr2.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr3.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr4.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr5.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr6.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr7.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr8.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr9.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr10.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					
					impublic1.setBackgroundResource(R.drawable.search_unckeck);
					impr1.setBackgroundResource(R.drawable.search_unckeck);
					impr2.setBackgroundResource(R.drawable.search_unckeck);
					impr3.setBackgroundResource(R.drawable.search_unckeck);
					impr4.setBackgroundResource(R.drawable.search_unckeck);
					impr5.setBackgroundResource(R.drawable.search_unckeck);
					impr6.setBackgroundResource(R.drawable.search_unckeck);
					impr7.setBackgroundResource(R.drawable.search_unckeck);
					impr8.setBackgroundResource(R.drawable.search_check);
					impr9.setBackgroundResource(R.drawable.search_unckeck);
					impr10.setBackgroundResource(R.drawable.search_unckeck);
					
					Npublic1 = false;
					Npr1 = false;
					Npr2 = false;
					Npr3 = false;
					Npr4 = false;
					Npr5 = false;
					Npr6 = false;
					Npr7 = false;
					Npr8 = true;
				    Npr9 = false;
					Npr10 = false;
				}
				else if(v == pr9)
				{
					public1.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr1.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr2.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr3.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr4.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr5.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr6.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr7.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr8.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr9.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr10.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					
					impublic1.setBackgroundResource(R.drawable.search_unckeck);
					impr1.setBackgroundResource(R.drawable.search_unckeck);
					impr2.setBackgroundResource(R.drawable.search_unckeck);
					impr3.setBackgroundResource(R.drawable.search_unckeck);
					impr4.setBackgroundResource(R.drawable.search_unckeck);
					impr5.setBackgroundResource(R.drawable.search_unckeck);
					impr6.setBackgroundResource(R.drawable.search_unckeck);
					impr7.setBackgroundResource(R.drawable.search_unckeck);
					impr8.setBackgroundResource(R.drawable.search_unckeck);
					impr9.setBackgroundResource(R.drawable.search_check);
					impr10.setBackgroundResource(R.drawable.search_unckeck);
					
					Npublic1 = false;
					Npr1 = false;
					Npr2 = false;
					Npr3 = false;
					Npr4 = false;
					Npr5 = false;
					Npr6 = false;
					Npr7 = false;
					Npr8 = false;
				    Npr9 = true;
					Npr10 = false;
				}
				else if(v == pr10)
				{
					public1.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr1.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr2.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr3.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr4.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr5.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr6.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr7.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr8.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr9.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr10.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					
					impublic1.setBackgroundResource(R.drawable.search_unckeck);
					impr1.setBackgroundResource(R.drawable.search_unckeck);
					impr2.setBackgroundResource(R.drawable.search_unckeck);
					impr3.setBackgroundResource(R.drawable.search_unckeck);
					impr4.setBackgroundResource(R.drawable.search_unckeck);
					impr5.setBackgroundResource(R.drawable.search_unckeck);
					impr6.setBackgroundResource(R.drawable.search_unckeck);
					impr7.setBackgroundResource(R.drawable.search_unckeck);
					impr8.setBackgroundResource(R.drawable.search_unckeck);
					impr9.setBackgroundResource(R.drawable.search_unckeck);
					impr10.setBackgroundResource(R.drawable.search_check);
					
					Npublic1 = false;
					Npr1 = false;
					Npr2 = false;
					Npr3 = false;
					Npr4 = false;
					Npr5 = false;
					Npr6 = false;
					Npr7 = false;
					Npr8 = false;
				    Npr9 = false;
					Npr10 = true;
				}
				
			}
			
		};
		
		public1.setOnClickListener(ffff);
		pr1.setOnClickListener(ffff);
		pr2.setOnClickListener(ffff);
		pr3.setOnClickListener(ffff);
		pr4.setOnClickListener(ffff);
		pr5.setOnClickListener(ffff);
		pr6.setOnClickListener(ffff);
		pr7.setOnClickListener(ffff);
		pr8.setOnClickListener(ffff);
		pr9.setOnClickListener(ffff);
		pr10.setOnClickListener(ffff);
		
		
		
		cancelButton.setOnClickListener(new OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				d.cancel();
			}
		});
		
		okButton.setOnClickListener(new OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				 if (Npr10) {
                     levelFinal = 10;
					 //leveltext.setText(R.string.p10);
				 } else if (Npr1) {
					 levelFinal = 1;
					 //leveltext.setText(R.string.p1);
				 } else if (Npr2) {
					 levelFinal = 2;
					 //leveltext.setText(R.string.p2);
				 } else if (Npr3) {
					 levelFinal = 3;
					 //leveltext.setText(R.string.p3);
				 } else if (Npr4) {
					 levelFinal = 4;
					 //leveltext.setText(R.string.p4);
				 } else if (Npr5) {
					 levelFinal = 5;
					 //leveltext.setText(R.string.p5);
				 } else if (Npr6) {
					 levelFinal = 6;
					 //leveltext.setText(R.string.p6);
				 } else if (Npr7) {
					 levelFinal = 7;
					 //leveltext.setText(R.string.p7);
				 } else if (Npr8) {
					 levelFinal = 8;
					 //leveltext.setText(R.string.p8);
				 } else if (Npr9) {
					 levelFinal = 9;
					 //leveltext.setText(R.string.p9);
				 } else if (Npublic1) {
					 levelFinal = 0;
					 //leveltext.setText(R.string.p0);
				 } else {

				 }
				 b.PathList.get(position).Level = Integer.toString(levelFinal);
				 adapter.notifyDataSetChanged();
				d.cancel();
			}
		});
		d.show();
	}
	
	class uploadImagesTask extends AsyncTask<Void,Void,Void>
	{
		@Override
		protected void onPreExecute()
		{
			dialog_photos_ProgressBar2.setVisibility(View.VISIBLE);
		}
		
		@Override
		protected Void doInBackground(Void... params) 
		{
			return null;
		}
		
		@Override
		protected void onPostExecute(Void r)
		{
			for(int i=0;i<b.PathList.size();i++)
			{
		        new uploadImageTask(b.PathList.get(i),i,b.PathList.size()).execute();
			}
		}
	}
	
	
	class uploadImageTask extends AsyncTask<Void,Void,Void>
	{
		byte[] _byteArray;
		ImagePath _imPath;
		int _position;
		int _sizelist;
		
		uploadImageTask(ImagePath imPath,int position,int sizelist)
		{
			this._imPath = imPath;
			this._position = position;
			this._sizelist = sizelist;
		}
		@Override
		protected void onPreExecute()
		{
			if(_imPath.Path.indexOf("file://") > -1)
			{
				pathOfImage = _imPath.Path.replace("file://", "");
			}
			else
			{
				pathOfImage = getPath(Uri.parse(_imPath.Path));
			}
			
			ManagerOfImage manIma = new ManagerOfImage(); 
			bitmap = manIma.down_scale(pathOfImage,  800, 480);
			
			int ROTATION_DEGREE =90;
		    Matrix matrix = new Matrix();
		    int orientation = 1;
		    try 
		    {
		    	ExifInterface exif = new ExifInterface(pathOfImage);
		        orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);                  
		    } 
		    catch (IOException e) 
		    {
		    	e.printStackTrace();
		    }
		                 
		    matrix.setRotate(0);
		                  
		    switch (orientation)
		    {
		    	case ExifInterface.ORIENTATION_ROTATE_90:
		    		matrix.setRotate(ROTATION_DEGREE);
		    		break;
		                  
		        case ExifInterface.ORIENTATION_ROTATE_180:
		        	matrix.setRotate(ROTATION_DEGREE * 2); 
		        	break;
		                  
		        case ExifInterface.ORIENTATION_ROTATE_270:
		        	matrix.setRotate(-ROTATION_DEGREE);
		        	break;
		                  
		        default:
		        	break;
		    }
		    int x = bitmap.getWidth();
		    int y = bitmap.getHeight();

		    bitmap = Bitmap.createBitmap(bitmap, 0, 0, x, y, matrix, true);
			
			ByteArrayOutputStream stream = new ByteArrayOutputStream();
	        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
	        _byteArray = stream.toByteArray();
		}
				
		@Override
		protected Void doInBackground(Void... params) 
		{
			notif = DataFetcher.postImage("profileimg.jpg",_byteArray ,Utils.getSharedPreferences(getActivity()).getString("userid",""),Integer.parseInt(_imPath.Level) );
			return null;
		}
		
		@Override
		protected void onPostExecute(Void r)
		{
			if(isAdded())
			{
				Toast toast = Toast.makeText(getActivity(), notif, Toast.LENGTH_LONG);
				toast.show();
				
				//double tisekato = ((100/(_sizelist))*_position)+(100/(_sizelist));
				
				//Log.v("APP", "jkjkjk " +_position +" " +_sizelist + " "+tisekato);
				
				
				
				dialog_photos_ProgressBar.setProgress((int) ((100/(_sizelist))*_position)+(100/(_sizelist)));
				
				if(_position == (_sizelist-1))
				{
					dialog_photos_ProgressBar2.setVisibility(View.GONE);
					dPhotos.cancel();
					b.PathList.clear();
					
					Fragment fr = new FragMyPhoto();
					FragmentManager fm = getActivity().getFragmentManager();
					
			        FragmentTransaction fragmentTransaction = fm.beginTransaction();
			        fragmentTransaction.setCustomAnimations(R.anim.left_slidein_list_animator, R.anim.left_slideout_list_animator,R.anim.right_slidein_list_animator, R.anim.right_slideout_list_animator);
			        fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
			        fragmentTransaction.addToBackStack(null);
			
			        fragmentTransaction.commit();
				}
			}
		}
	}
	
	
	
	
	public String getPath(Uri uri) 
	{
		Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null); 
		if(cursor!=null)
		{
			cursor.moveToFirst(); 
			int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
			return cursor.getString(idx); 
		}
		else
		{
			return null;
		}
	}
	
	
	public Context getApplicationContext() 
	{
		return null;
	}

	
	
	public String dateToString(Date date, String format) 
	{ 
		SimpleDateFormat df = new SimpleDateFormat(format); 
	   	return df.format(date); 
	}  
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public class MyImageAdapter extends BaseAdapter 
	{	
		private Activity _activity;
		public ImagePathList imageList;
		
		
		 
		    public MyImageAdapter(Activity activity)
		    {
		        this._activity = activity;
		    }
		    
		    public void setItems(ImagePathList list)
		    {
		    	this.imageList = list;
		    }
		 
		    @Override
		    public int getCount() 
		    {
		        return imageList.PathList.size();
		    }
		 
		    @Override
		    public Object getItem(int position) 
		    {
		        return imageList.PathList.get(position);
		    }
		 
		    @Override
		    public long getItemId(int position) 
		    {
		        return 0;
		    }
		 
		    @Override
		    public View getView(final int position, View convertView, ViewGroup parent) 
		    {
		    	RelativeLayout layout = (RelativeLayout) _activity.getLayoutInflater().inflate(R.layout.item_my_image, null);		    	
		        ImageView imageView = (ImageView) layout.findViewById(R.id.image);
		       // TextView item_image_imageDelete = (TextView) layout.findViewById(R.id.item_image_imageDelete);
		        RelativeLayout item_image_relativeDenied = (RelativeLayout) layout.findViewById(R.id.item_image_relativeDenied);
		        TextView item_image_textdecline = (TextView) layout.findViewById(R.id.item_image_textdecline);
		        RelativeLayout item_image_relativeLevel = (RelativeLayout) layout.findViewById(R.id.item_image_relativeLevel);
		        TextView item_image_textLevel = (TextView) layout.findViewById(R.id.item_image_textLevel);
		        
		        Utils.getImageLoader(_activity).displayImage(imageList.PathList.get(position).Path, imageView, Utils.getImageOptions());
		        
		        if(imageList.PathList.get(position).photoType.contains("2"))
		        {
		        	item_image_relativeDenied.setVisibility(View.VISIBLE);
		        	item_image_textdecline.setText(R.string.photodecline);
		        }
		        
		        
		        if(!imageList.PathList.get(position).Level.equals("0"))
		        {
		        	item_image_relativeLevel.setVisibility(View.VISIBLE);
		        	item_image_textLevel.setText(imageList.PathList.get(position).Level);
		        }
		        
		        
		        
		        /*
		        item_image_imageDelete.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						
						b.PathList.remove(position);
						adapter.notifyDataSetChanged();
						//new getDeletePhotosTask(imageList.PathList.get(position).photoId).execute();
					}
				});
		        */
		        
		        
		        
		        return layout;
		    }

	}

}

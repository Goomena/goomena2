package com.goomena.fragments;


import com.goomena.app.R;
import com.goomena.app.adapters.EditProfileAdapter;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.RelativeLayout;
import android.widget.TextView;

@SuppressLint("NewApi") 
public class FragEditProfile extends Fragment
{
	private ViewPager viewPager;
	private EditProfileAdapter adapter;
	
	RelativeLayout fr_editPr_change_relativeLayout,fr_editPr_settings_relativeLayout;
	TextView fr_editPr_change_text,fr_editPr_settings_text;
	View fr_editPr_viewChange;
	//fr_editPr_viewSettings;
	int w;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup group, Bundle saved)
	{
		return inflater.inflate(R.layout.fr_edit_profile, group, false);
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public void onActivityCreated (Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);
		
		
		w = getActivity().getWindowManager().getDefaultDisplay().getWidth();
		
		fr_editPr_change_relativeLayout = (RelativeLayout) getActivity().findViewById(R.id.fr_editPr_change_relativeLayout);
		fr_editPr_settings_relativeLayout = (RelativeLayout) getActivity().findViewById(R.id.fr_editPr_settings_relativeLayout);
		fr_editPr_change_text = (TextView) getActivity().findViewById(R.id.fr_editPr_change_text);
		fr_editPr_settings_text = (TextView) getActivity().findViewById(R.id.fr_editPr_settings_text);
		fr_editPr_viewChange = (View) getActivity().findViewById(R.id.fr_editPr_viewChange);
		//fr_editPr_viewSettings = (View) getActivity().findViewById(R.id.fr_editPr_viewSettings);
		viewPager = (ViewPager) getActivity().findViewById(R.id.view_pager_editPr);
		
		fr_editPr_change_relativeLayout.setOnClickListener(clickListener);
		fr_editPr_settings_relativeLayout.setOnClickListener(clickListener);
		
		adapter = new EditProfileAdapter(getActivity());
		
		viewPager.setAdapter(adapter);
		viewPager.setPageMargin(50);
		viewPager.setOnPageChangeListener(pageChangeListener);
		
		setTab(0);
		viewPager.setCurrentItem(0);
	}
	
	OnClickListener clickListener = new OnClickListener() 
	{
		public void onClick(View v) 
		{
			if(v == fr_editPr_change_relativeLayout)
			{
				setTab(0);
				viewPager.setCurrentItem(0);
			}
			else if(v == fr_editPr_settings_relativeLayout)
			{
				setTab(1);
				viewPager.setCurrentItem(1);
			}
		}
	};
	
	OnPageChangeListener pageChangeListener = new OnPageChangeListener()
	{
		@Override
		public void onPageScrollStateChanged(int arg0) {}
		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2) 
		{
			if(arg0==0)
			{
				fr_editPr_viewChange.setTranslationX(arg1*(w/(1.86f)));
			}
			
			//Log.v("APP", "ererer "+arg0+" "+ arg1+" "+arg2);
		}
		@Override
		public void onPageSelected(int arg0) { setTab(arg0); }
	};
	
	private void setTab(int select)
	{
		if(select==0)
		{
			fr_editPr_change_text.setTextColor(getActivity().getResources().getColor(R.color.colorPanelLine));
			fr_editPr_settings_text.setTextColor(getActivity().getResources().getColor(R.color.white));
			
			
			//fr_editPr_viewChange.setBackgroundColor(getActivity().getResources().getColor(R.color.colorPanelLine));
			//fr_editPr_viewSettings.setBackgroundColor(getActivity().getResources().getColor(R.color.white));
		}
		else if(select==1)
		{
			fr_editPr_change_text.setTextColor(getActivity().getResources().getColor(R.color.white));
			fr_editPr_settings_text.setTextColor(getActivity().getResources().getColor(R.color.colorPanelLine));
			
			
			//fr_editPr_viewChange.setBackgroundColor(getActivity().getResources().getColor(R.color.white));
			//fr_editPr_viewSettings.setBackgroundColor(getActivity().getResources().getColor(R.color.colorPanelLine));
		}
	}

}

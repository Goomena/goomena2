package com.goomena.fragments;

import com.edmodo.rangebar.RangeBar;
import com.goomena.app.ActHome;
import com.goomena.app.ActionBarChange;
import com.goomena.app.R;
import com.goomena.app.tools.Log;
import com.goomena.app.tools.Utils;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

@SuppressLint("NewApi") 
public class FragPreSearch extends Fragment
{
	Boolean flagDA = true;
	int MyAge;
	int Dt = 25;
	int Lt = 25;
	int Rt = 0;
	int MinAge=18;
	int MaxAge=103;
	String Distance="50";
	
	RelativeLayout fr_pre_search_relativeTextAge;
	TextView fr_pre_search_textTextAge;
	TextView fr_search_textMinDifAge,fr_search_textMaxDifAge;
	RangeBar fr_search_rangebarDifAge;
	
	SeekBar fr_search_rangebarDistance;
	ImageView fr_pre_search_imageOval_1,fr_pre_search_imageOval_2,fr_pre_search_imageOval_3,fr_pre_search_imageOval_4,fr_pre_search_imageOval_5;
	LinearLayout fr_pre_search_linearOval_1,fr_pre_search_linearOval_2,fr_pre_search_linearOval_3,fr_pre_search_linearOval_4,fr_pre_search_linearOval_5;
	
	RelativeLayout fr_pre_search_relativeSearch;
	RelativeLayout fr_pre_search_relativeSearchAdvance;
	RelativeLayout fr_pre_search_relativeNearestMembers;
	RelativeLayout fr_pre_search_relativeNewestMembers;
	RelativeLayout fr_pre_search_relativeRecentLogin;
	
	TextView fr_pre_search_textRecentLogin;
	
	
	private final static String TAG_FRAGMENT = "TAG_FRAGMENT";
	
	public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState) 
	{
		 return inflater.inflate(R.layout.fr_pre_search, container, false);
	}
	
	public void onActivityCreated (Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);
		
		//ActionBarChange.changeActionBar(getActivity(), 1);
		
		MyAge = Integer.parseInt(Utils.getSharedPreferences(getActivity()).getString("userage",""));
		
		fr_pre_search_relativeTextAge = (RelativeLayout) getActivity().findViewById(R.id.fr_pre_search_relativeTextAge);
		fr_pre_search_textTextAge = (TextView) getActivity().findViewById(R.id.fr_pre_search_textTextAge);
		
		fr_search_textMinDifAge = (TextView) getActivity().findViewById(R.id.fr_search_textMinDifAge);
		fr_search_textMaxDifAge = (TextView) getActivity().findViewById(R.id.fr_search_textMaxDifAge);
		fr_search_rangebarDifAge = (RangeBar) getActivity().findViewById(R.id.fr_search_rangebarDifAge);
		setDifferenceAge();
		
		fr_search_rangebarDistance = (SeekBar) getActivity().findViewById(R.id.fr_search_rangebarDistance);
		fr_search_rangebarDistance.setProgress(50);
		
		fr_pre_search_imageOval_1 = (ImageView) getActivity().findViewById(R.id.fr_pre_search_imageOval_1);
		fr_pre_search_imageOval_2 = (ImageView) getActivity().findViewById(R.id.fr_pre_search_imageOval_2);
		fr_pre_search_imageOval_3 = (ImageView) getActivity().findViewById(R.id.fr_pre_search_imageOval_3);
		fr_pre_search_imageOval_4 = (ImageView) getActivity().findViewById(R.id.fr_pre_search_imageOval_4);
		fr_pre_search_imageOval_5 = (ImageView) getActivity().findViewById(R.id.fr_pre_search_imageOval_5);
		
		fr_pre_search_linearOval_1 = (LinearLayout) getActivity().findViewById(R.id.fr_pre_search_linearOval_1);
		fr_pre_search_linearOval_2 = (LinearLayout) getActivity().findViewById(R.id.fr_pre_search_linearOval_2);
		fr_pre_search_linearOval_3 = (LinearLayout) getActivity().findViewById(R.id.fr_pre_search_linearOval_3);
		fr_pre_search_linearOval_4 = (LinearLayout) getActivity().findViewById(R.id.fr_pre_search_linearOval_4);
		fr_pre_search_linearOval_5 = (LinearLayout) getActivity().findViewById(R.id.fr_pre_search_linearOval_5);
		
		fr_pre_search_relativeSearch = (RelativeLayout) getActivity().findViewById(R.id.fr_pre_search_relativeSearch);
		fr_pre_search_relativeSearchAdvance = (RelativeLayout) getActivity().findViewById(R.id.fr_pre_search_relativeSearchAdvance);
		fr_pre_search_relativeNearestMembers = (RelativeLayout) getActivity().findViewById(R.id.fr_pre_search_relativeNearestMembers);
		fr_pre_search_relativeNewestMembers = (RelativeLayout) getActivity().findViewById(R.id.fr_pre_search_relativeNewestMembers);
		fr_pre_search_relativeRecentLogin = (RelativeLayout) getActivity().findViewById(R.id.fr_pre_search_relativeRecentLogin);
		fr_pre_search_textRecentLogin = (TextView) getActivity().findViewById(R.id.fr_pre_search_textRecentLogin);
		
		
		if(Utils.getSharedPreferences(getActivity()).getString("genderid","").contains("2"))
		{
			fr_pre_search_relativeSearchAdvance.setBackgroundResource(R.drawable.pre_search_wih_men);
			fr_pre_search_relativeNearestMembers.setBackgroundResource(R.drawable.pre_nearest_icon);
			fr_pre_search_relativeNewestMembers.setBackgroundResource(R.drawable.pre_new_icon);
			fr_pre_search_relativeRecentLogin.setBackgroundResource(R.drawable.pre_vip_icon);
			fr_pre_search_textRecentLogin.setText(R.string.show_me_the_vip_members);
		}
		
		fr_search_rangebarDistance.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) 
			{
				if(fr_search_rangebarDistance.getProgress() <=20 )
				{
					setOvalcolor(0);
					fr_search_rangebarDistance.setProgress(0);
				}
				else if(fr_search_rangebarDistance.getProgress() >20 && fr_search_rangebarDistance.getProgress() <=40)
				{
					setOvalcolor(1);
					fr_search_rangebarDistance.setProgress(25);
				}
				else if(fr_search_rangebarDistance.getProgress() >40 && fr_search_rangebarDistance.getProgress() <=60)
				{
					setOvalcolor(2);
					fr_search_rangebarDistance.setProgress(50);
				}
				else if(fr_search_rangebarDistance.getProgress() >60 && fr_search_rangebarDistance.getProgress() <=80)
				{
					setOvalcolor(3);
					fr_search_rangebarDistance.setProgress(75);
				}
				else if(fr_search_rangebarDistance.getProgress() >80 && fr_search_rangebarDistance.getProgress() <=99)
				{
					setOvalcolor(4);
					fr_search_rangebarDistance.setProgress(99);
				}
			}
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {}
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {}
		});
		
		fr_pre_search_relativeSearch.setOnClickListener(clickListener);
		fr_pre_search_relativeTextAge.setOnClickListener(clickListener);
		fr_pre_search_relativeSearchAdvance.setOnClickListener(clickListener);
		fr_pre_search_relativeNearestMembers.setOnClickListener(clickListener);
		fr_pre_search_relativeNewestMembers.setOnClickListener(clickListener);
		fr_pre_search_relativeRecentLogin.setOnClickListener(clickListener);
	}
	
	
	
	
	
	
	
	
	
	OnClickListener clickListener = new OnClickListener() 
	{
		public void onClick(View v) 
		{
			if(v == fr_pre_search_relativeTextAge)
			{
				if(flagDA)
				{
					setMinMaxAge();
				}
				else
				{
					setDifferenceAge();
				}
			}
			else if(v == fr_pre_search_relativeSearch)
			{
				FragGridProfile a = new FragGridProfile(22);
				a.searchByFilter(String.valueOf(MinAge),String.valueOf(MaxAge),0, 0,0,0,0, Distance);
				
				Fragment fr = a;			
				FragmentManager fm = getActivity().getFragmentManager();
				
		        FragmentTransaction fragmentTransaction = fm.beginTransaction();
		        fragmentTransaction.setCustomAnimations(R.anim.left_slidein_list_animator, R.anim.left_slideout_list_animator,R.anim.right_slidein_list_animator, R.anim.right_slideout_list_animator);
		        fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
		        fragmentTransaction.addToBackStack(null);
		
		        fragmentTransaction.commit();
			}
			else if(v == fr_pre_search_relativeSearchAdvance)
			{
				Fragment fr = new FragSearch();
				 
				FragmentManager fm = getActivity().getFragmentManager();

				FragmentTransaction fragmentTransaction = fm.beginTransaction();
				fragmentTransaction.setCustomAnimations(R.anim.left_slidein_list_animator, R.anim.left_slideout_list_animator,R.anim.right_slidein_list_animator, R.anim.right_slideout_list_animator);
				fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
				fragmentTransaction.addToBackStack(null);

				fragmentTransaction.commit();
			}
			else if(v == fr_pre_search_relativeNearestMembers)
			{
				Fragment fr = new FragGridProfile(11);				
				FragmentManager fm = getActivity().getFragmentManager();
				
		        FragmentTransaction fragmentTransaction = fm.beginTransaction();
		        fragmentTransaction.setCustomAnimations(R.anim.left_slidein_list_animator, R.anim.left_slideout_list_animator,R.anim.right_slidein_list_animator, R.anim.right_slideout_list_animator);
		        fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
		        fragmentTransaction.addToBackStack(null);
		
		        fragmentTransaction.commit();
			}
			else if(v == fr_pre_search_relativeNewestMembers)
			{
				Fragment fr = new FragGridProfile(12);				
				FragmentManager fm = getActivity().getFragmentManager();
				
		        FragmentTransaction fragmentTransaction = fm.beginTransaction();
		        fragmentTransaction.setCustomAnimations(R.anim.left_slidein_list_animator, R.anim.left_slideout_list_animator,R.anim.right_slidein_list_animator, R.anim.right_slideout_list_animator);
		        fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
		        fragmentTransaction.addToBackStack(null);
		
		        fragmentTransaction.commit();
			}
			else if(v == fr_pre_search_relativeRecentLogin)
			{
				if(Utils.getSharedPreferences(getActivity()).getString("genderid","").contains("1"))
				{
					Fragment fr = new FragGridProfile(14);				
					FragmentManager fm = getActivity().getFragmentManager();
				
					FragmentTransaction fragmentTransaction = fm.beginTransaction();
					fragmentTransaction.setCustomAnimations(R.anim.left_slidein_list_animator, R.anim.left_slideout_list_animator,R.anim.right_slidein_list_animator, R.anim.right_slideout_list_animator);
					fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
					fragmentTransaction.addToBackStack(null);
		
					fragmentTransaction.commit();
				}
				else
				{
					Fragment fr = new FragGridProfile(15);				
					FragmentManager fm = getActivity().getFragmentManager();
					
			        FragmentTransaction fragmentTransaction = fm.beginTransaction();
			        fragmentTransaction.setCustomAnimations(R.anim.left_slidein_list_animator, R.anim.left_slideout_list_animator,R.anim.right_slidein_list_animator, R.anim.right_slideout_list_animator);
			        fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
			        fragmentTransaction.addToBackStack(null);
			
			        fragmentTransaction.commit();
				}
			}
		}
	};
	
	
	public void setDifferenceAge()
	{
		flagDA = true;
		
		if(MyAge == 18) {Dt=5;Lt=0;Rt=Dt-6;}
		if(MyAge == 19) {Dt=6;Lt=1;Rt=Dt-6;}
		if(MyAge == 20) {Dt=7;Lt=2;Rt=Dt-6;}
		if(MyAge == 21) {Dt=8;Lt=3;Rt=Dt-6;}
		if(MyAge == 22) {Dt=9;Lt=4;Rt=Dt-6;}
		if(MyAge == 23) {Dt=10;Lt=5;Rt=Dt-6;}
		if(MyAge == 24) {Dt=11;Lt=6;Rt=Dt-6;}
		if(MyAge == 25) {Dt=12;Lt=7;Rt=Dt-6;}
		if(MyAge >= 26 && MyAge <= 29) {Dt=12;Lt=7;Rt=Dt-6;}
		if(MyAge >= 30 && MyAge <= 34) {Dt=5;Lt=5;Rt=Dt-1;}
		if(MyAge >= 35 && MyAge <= 39) {Dt=8;Lt=8;Rt=Dt-1;}
		if(MyAge >= 40 && MyAge <= 44) {Dt=10;Lt=10;Rt=Dt-1;}
		if(MyAge >= 45 && MyAge <= 49) {Dt=13;Lt=13;Rt=Dt-1;}
		if(MyAge >= 50 && MyAge <= 54) {Dt=16;Lt=16;Rt=Dt-1;}
		if(MyAge >= 55 && MyAge <= 59) {Dt=20;Lt=20;Rt=Dt-1;}
		if(MyAge >= 60) {Dt=25;Lt=25;Rt=Dt-1;}
		
		
		fr_pre_search_textTextAge.setText(getActivity().getResources().getString(R.string.age_variance));
		fr_search_rangebarDifAge.setTickCount(Dt);
		fr_search_rangebarDifAge.setThumbIndices(0, Dt-1);
		
		fr_search_textMinDifAge.setText(Integer.toString(fr_search_rangebarDifAge.getLeftIndex()-Lt) +" "+getActivity().getResources().getString(R.string.age_years));
		fr_search_textMaxDifAge.setText(Integer.toString(fr_search_rangebarDifAge.getRightIndex()-Rt) +" "+getActivity().getResources().getString(R.string.age_years));
		MinAge = MyAge + (fr_search_rangebarDifAge.getLeftIndex()-Lt);
		MaxAge = MyAge + (fr_search_rangebarDifAge.getRightIndex()-Rt);
		//Log.v("APP","dfdfdf "+MinAge+" "+MaxAge);
		
		fr_search_rangebarDifAge.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onIndexChangeListener(RangeBar rangeBar, int leftThumbIndex, int rightThumbIndex) {

            	fr_search_textMinDifAge.setText(Integer.toString(fr_search_rangebarDifAge.getLeftIndex()-Lt) +" " + getActivity().getResources().getString(R.string.age_years));
        		fr_search_textMaxDifAge.setText(Integer.toString(fr_search_rangebarDifAge.getRightIndex()-Rt) +" "+ getActivity().getResources().getString(R.string.age_years));
        		MinAge = MyAge + (fr_search_rangebarDifAge.getLeftIndex()-Lt);
        		MaxAge = MyAge + (fr_search_rangebarDifAge.getRightIndex()-Rt);
        		
        		//Log.v("APP","dfdfdf "+MinAge+" "+MaxAge);
            }
        });
	}
	
	public void setMinMaxAge()
	{
		flagDA = false;
		fr_pre_search_textTextAge.setText(getActivity().getResources().getString(R.string.range_of_values));
		fr_search_rangebarDifAge.setTickCount(100);
		fr_search_rangebarDifAge.setThumbIndices(0, 85);
		
		fr_search_textMinDifAge.setText( Integer.toString(fr_search_rangebarDifAge.getLeftIndex()+18) +" "+ getActivity().getResources().getString(R.string.years));
		fr_search_textMaxDifAge.setText(Integer.toString(fr_search_rangebarDifAge.getRightIndex()+18) +" "+ getActivity().getResources().getString(R.string.years));
		MinAge = fr_search_rangebarDifAge.getLeftIndex()+18;
		MaxAge = fr_search_rangebarDifAge.getRightIndex()+18;
		//Log.v("APP","dfdfdf "+MinAge+" "+MaxAge);
		
		fr_search_rangebarDifAge.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onIndexChangeListener(RangeBar rangeBar, int leftThumbIndex, int rightThumbIndex) {

            	fr_search_textMinDifAge.setText( Integer.toString(fr_search_rangebarDifAge.getLeftIndex()+18) +" "+ getActivity().getResources().getString(R.string.years));
            	fr_search_textMaxDifAge.setText(Integer.toString(fr_search_rangebarDifAge.getRightIndex()+18) +" "+ getActivity().getResources().getString(R.string.years));
            	MinAge = fr_search_rangebarDifAge.getLeftIndex()+18;
        		MaxAge = fr_search_rangebarDifAge.getRightIndex()+18;
        		//Log.v("APP","dfdfdf "+MinAge+" "+MaxAge);
            }
        });
		
	}
	
	public void setOvalcolor(int selc)
	{
		if(selc==0)
		{
			Distance = "1";
			fr_pre_search_imageOval_1.setImageResource(R.drawable.pre_cycle_color);
			fr_pre_search_imageOval_2.setImageResource(R.drawable.pre_cycle_white);
			fr_pre_search_imageOval_3.setImageResource(R.drawable.pre_cycle_white);
			fr_pre_search_imageOval_4.setImageResource(R.drawable.pre_cycle_white);
			fr_pre_search_imageOval_5.setImageResource(R.drawable.pre_cycle_white);
			
			fr_pre_search_linearOval_1.setVisibility(View.VISIBLE);
			fr_pre_search_linearOval_2.setVisibility(View.GONE);
			fr_pre_search_linearOval_3.setVisibility(View.GONE);
			fr_pre_search_linearOval_4.setVisibility(View.GONE);
			fr_pre_search_linearOval_5.setVisibility(View.GONE);
		}
		else if(selc==1)
		{
			Distance = "5";
			fr_pre_search_imageOval_1.setImageResource(R.drawable.pre_cycle_white);
			fr_pre_search_imageOval_2.setImageResource(R.drawable.pre_cycle_color);
			fr_pre_search_imageOval_3.setImageResource(R.drawable.pre_cycle_white);
			fr_pre_search_imageOval_4.setImageResource(R.drawable.pre_cycle_white);
			fr_pre_search_imageOval_5.setImageResource(R.drawable.pre_cycle_white);
			
			fr_pre_search_linearOval_1.setVisibility(View.GONE);
			fr_pre_search_linearOval_2.setVisibility(View.VISIBLE);
			fr_pre_search_linearOval_3.setVisibility(View.GONE);
			fr_pre_search_linearOval_4.setVisibility(View.GONE);
			fr_pre_search_linearOval_5.setVisibility(View.GONE);
		}
		else if(selc==2)
		{
			Distance = "50";
			fr_pre_search_imageOval_1.setImageResource(R.drawable.pre_cycle_white);
			fr_pre_search_imageOval_2.setImageResource(R.drawable.pre_cycle_white);
			fr_pre_search_imageOval_3.setImageResource(R.drawable.pre_cycle_color);
			fr_pre_search_imageOval_4.setImageResource(R.drawable.pre_cycle_white);
			fr_pre_search_imageOval_5.setImageResource(R.drawable.pre_cycle_white);
			
			fr_pre_search_linearOval_1.setVisibility(View.GONE);
			fr_pre_search_linearOval_2.setVisibility(View.GONE);
			fr_pre_search_linearOval_3.setVisibility(View.VISIBLE);
			fr_pre_search_linearOval_4.setVisibility(View.GONE);
			fr_pre_search_linearOval_5.setVisibility(View.GONE);
		}
		else if(selc==3)
		{
			Distance = "1200";
			fr_pre_search_imageOval_1.setImageResource(R.drawable.pre_cycle_white);
			fr_pre_search_imageOval_2.setImageResource(R.drawable.pre_cycle_white);
			fr_pre_search_imageOval_3.setImageResource(R.drawable.pre_cycle_white);
			fr_pre_search_imageOval_4.setImageResource(R.drawable.pre_cycle_color);
			fr_pre_search_imageOval_5.setImageResource(R.drawable.pre_cycle_white);
			
			fr_pre_search_linearOval_1.setVisibility(View.GONE);
			fr_pre_search_linearOval_2.setVisibility(View.GONE);
			fr_pre_search_linearOval_3.setVisibility(View.GONE);
			fr_pre_search_linearOval_4.setVisibility(View.VISIBLE);
			fr_pre_search_linearOval_5.setVisibility(View.GONE);
		}
		else if(selc==4)
		{
			Distance = "10000";
			fr_pre_search_imageOval_1.setImageResource(R.drawable.pre_cycle_white);
			fr_pre_search_imageOval_2.setImageResource(R.drawable.pre_cycle_white);
			fr_pre_search_imageOval_3.setImageResource(R.drawable.pre_cycle_white);
			fr_pre_search_imageOval_4.setImageResource(R.drawable.pre_cycle_white);
			fr_pre_search_imageOval_5.setImageResource(R.drawable.pre_cycle_color);
			
			fr_pre_search_linearOval_1.setVisibility(View.GONE);
			fr_pre_search_linearOval_2.setVisibility(View.GONE);
			fr_pre_search_linearOval_3.setVisibility(View.GONE);
			fr_pre_search_linearOval_4.setVisibility(View.GONE);
			fr_pre_search_linearOval_5.setVisibility(View.VISIBLE);
		}
		
	}
	
	

}





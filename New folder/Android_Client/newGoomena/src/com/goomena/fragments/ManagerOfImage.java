package com.goomena.fragments;

import java.io.ByteArrayOutputStream;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class ManagerOfImage {

//gia na kanoume alagi megethous tin ikona, afou einai megaliteri apta oria pou dilonoume
	public Bitmap down_scale(String imagePath, int maxWidth, int maxHeight) {
	  
		  BitmapFactory.Options options = new BitmapFactory.Options();
		  options.inJustDecodeBounds = true;
		  BitmapFactory.decodeFile(imagePath,options);
		  int scale=1;
		  if(options.outWidth>maxWidth || options.outHeight>maxHeight){
			  // here maxWidth and maxHeight are the desired width and height
			  scale = Math.max(options.outWidth/maxWidth, options.outHeight/maxHeight);	  
		  }
		  
		  BitmapFactory.Options r_bit = new BitmapFactory.Options();
		  r_bit.inSampleSize = scale;
          Bitmap resizedBitmap = BitmapFactory.decodeFile(imagePath, r_bit);	  
		  return resizedBitmap;
	}
	
	//metatrepoume tin ikona se Byte
	public String convertImageToByte(Bitmap bitmap, String imagePath){
		
		ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
        
		if(imagePath.endsWith("jpeg")){
			bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteStream);
		}
		else if(imagePath.endsWith("png")){
			bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteStream);
		}
		else{
			bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteStream);
		}
        
		byte [] byteImage = byteStream.toByteArray();
        String stringByteImage=Base64.encodeBytes(byteImage);
	
        return stringByteImage;
	
	}
	
	
	
}


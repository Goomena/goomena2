package com.goomena.app.adapters;

import com.goomena.app.R;
import com.goomena.app.models.ImagePathList;
import com.goomena.app.tools.Utils;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class ImageAdapter extends BaseAdapter 
{	
	private Activity _activity;
	public ImagePathList imageList;
	
	
	 
	    public ImageAdapter(Activity activity)
	    {
	        this._activity = activity;
	    }
	    
	    public void setItems(ImagePathList list)
	    {
	    	this.imageList = list;
	    }
	 
	    @Override
	    public int getCount() 
	    {
	        return imageList.PathList.size();
	    }
	 
	    @Override
	    public Object getItem(int position) 
	    {
	        return imageList.PathList.get(position);
	    }
	 
	    @Override
	    public long getItemId(int position) 
	    {
	        return 0;
	    }
	 
	    @SuppressLint("WrongViewCast") 
	    @Override
	    public View getView(int position, View convertView, ViewGroup parent) 
	    {
	    	RelativeLayout layout = (RelativeLayout) _activity.getLayoutInflater().inflate(R.layout.item_image, null);		    	
	        ImageView imageView = (ImageView) layout.findViewById(R.id.image);
	        ImageView item_image_imageDelete = (ImageView) layout.findViewById(R.id.item_image_imageDelete);
	        RelativeLayout item_image_relativeDenied = (RelativeLayout) layout.findViewById(R.id.item_image_relativeDenied);
	        
	        Utils.getImageLoader(_activity).displayImage(imageList.PathList.get(position).Path, imageView, Utils.getImageOptions());
	        
	        if(imageList.PathList.get(position).photoType.contains("2"))
	        {
	        	item_image_relativeDenied.setVisibility(View.VISIBLE);
	        }
	        
	        
	        
	        
	        
	        return layout;
	    }

}

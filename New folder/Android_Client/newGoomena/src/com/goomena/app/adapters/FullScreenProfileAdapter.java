package com.goomena.app.adapters;

import java.util.ArrayList;
import java.util.HashMap;

import com.goomena.app.ActGoogleMap;
import com.goomena.app.ActShowPhoto;
import com.goomena.app.R;
import com.goomena.app.adapters.ObservableScrollView.ScrollViewListener;
import com.goomena.app.data.ServiceClient;
import com.goomena.app.global.Global;
import com.goomena.app.models.CheckActions;
import com.goomena.app.models.ImagePathList;
import com.goomena.app.models.UserDetails;
import com.goomena.app.models.UserProfile;
import com.goomena.app.tools.Log;
import com.goomena.app.tools.Utils;
import com.goomena.fragments.FragLoading;
import com.goomena.fragments.FragMessageView;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

@SuppressLint("UseSparseArrays") 
public class FullScreenProfileAdapter extends PagerAdapter
{
	private Activity _activity;
	//private ArrayList<String> _imagePaths;
	private ArrayList<UserProfile> _userProfiles;
	private LayoutInflater inflater;
	Animation animRotate,animFadein;
	private final static String TAG_FRAGMENT = "TAG_FRAGMENT";
	
	FragLoading fragLoading;
	String privatePhoto;
	public final static int IMAGE_REQUEST = 1999;
	
	String resultLevel;
	int levelint;
	
	@SuppressWarnings("rawtypes")
	HashMap<Integer, AsyncTask> ARPhotosTask = new HashMap<Integer, AsyncTask>();
	@SuppressWarnings("rawtypes")
	HashMap<Integer, AsyncTask> ARUserStateTask = new HashMap<Integer, AsyncTask>();
	@SuppressWarnings("rawtypes")
	HashMap<Integer, AsyncTask> ARgetDetailsTask = new HashMap<Integer, AsyncTask>();
	@SuppressWarnings("rawtypes")
	HashMap<Integer, AsyncTask> ARgetCheckActionsTask = new HashMap<Integer, AsyncTask>();
	
	
	Boolean photoOpen = false;
	int hScreen;
	int hActin_bar;
	int topMenu;
	int bottomMenu;


	// constructor
	@SuppressWarnings("deprecation")
	public FullScreenProfileAdapter(Activity activity, ArrayList<UserProfile> userProfiles)
	{
		this._activity = activity;
		//this._imagePaths = imagePaths;
		this._userProfiles = userProfiles;
		animRotate = AnimationUtils.loadAnimation(_activity, R.anim.d_wait_bg_logo);
		animFadein = AnimationUtils.loadAnimation(activity, R.anim.fade_in);
		fragLoading = new FragLoading(activity);
		
		LinearLayout actin_bar_bg_menu = (LinearLayout) activity.findViewById(R.id.actin_bar_bg_menu);
		
		
		actin_bar_bg_menu.measure(0, 0);
		hActin_bar = actin_bar_bg_menu.getMeasuredHeight();
		
		hScreen = activity.getWindowManager().getDefaultDisplay().getHeight();
		topMenu = Math.round(activity.getWindowManager().getDefaultDisplay().getHeight()/(1.67f));   
		///////bottomMenu = Math.round(activity.getWindowManager().getDefaultDisplay().getHeight()/(19.2f));
		
		
		//Log.v("APP", "sdfwfwf "+topMenu +" "+bottomMenu);
	}
	
	

	@Override
	public int getCount() 
	{		
		//Log.v("APP","aaaaaaaaaa size "+_userProfiles.size());
		return _userProfiles.size();
	}
	
	

	@Override
    public boolean isViewFromObject(View view, Object object) 
	{
        return view == ((RelativeLayout) object);
    }
	
	
	public void setPrimaryItem(ViewGroup container, int position, Object object) 
	{	
		
	}
	
	
	@SuppressLint("NewApi") 
	@Override
    public Object instantiateItem(ViewGroup container, final int position) 
	{
		final LinearLayout fr_profile_linearMenu;
		LinearLayout fr_profile_linearMenudd;
		final ObservableScrollView fr_profile_scroll;
		final ImageView profile_image_shadow;
		RelativeLayout Profile_relative;
		ImageView Profile_imageOval_yellow;
		TextView Profile_textOnline,Profile_textNumPhotos;
        final ImageView imgDisplay;
        final RelativeLayout topMenu_Profile;
        final ImageView topMenu_imageProfile;
        final TextView topMenu_textName,topMenu_textAge;
        //,topMenu_textLocation;
        
        ImageView profile_imageLike,profile_imageFavorite,profile_imageBids,profile_imageMessage;
        //final TextView profile_textLike;
		//final TextView profile_textFavorite;
		//final TextView profile_textBids;
		//final TextView profile_textMessage;
        
		ImageView p_imageView1,p_imageView2,p_imageView3,p_imageView4,p_imageView5;
		
        TextView Profile_textName,Profile_textAge,Profile_textLocation;
        ExpandableHeightGridView imageGrid = new ExpandableHeightGridView(_activity);
        LinearLayout Profile_linearAccess;
        final TextView Profile_textAccess;
        TextView Profile_textabout,Profile_textage3,Profile_textzodiac3,Profile_textbodytype,Profile_texthaireyes,Profile_texteducation,Profile_textchilds,Profile_textnation,Profile_textreligion,Profile_textsmoke,Profile_textdrink,Profile_textjob;
        ImageView Profile_imageStatus,Profile_imageMeet;
        TextView Profile_textStatus,Profile_textMeet,Profile_textInterested1,Profile_textInterested2,Profile_textInterested3,Profile_textInterested4,Profile_textInterested5,Profile_textInterested6;
        TextView Profile_textTitleaboutme,Profile_textaboutme,Profile_textTitleaboutimaginefirst,Profile_textaboutimaginefirst;
        ImageView Profile_imageMap;
        TextView Profile_textMap;
        
        inflater = (LayoutInflater) _activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewLayout = inflater.inflate(R.layout.fullscreen_profile, container, false);
        
        fr_profile_linearMenu = (LinearLayout) viewLayout.findViewById(R.id.fr_profile_linearMenu);
        fr_profile_linearMenudd = (LinearLayout) viewLayout.findViewById(R.id.fr_profile_linearMenudd);
        fr_profile_scroll = (ObservableScrollView) viewLayout.findViewById(R.id.fr_profile_scroll);
        profile_image_shadow = (ImageView) viewLayout.findViewById(R.id.profile_image_shadow);
 
        Profile_relative  = (RelativeLayout) viewLayout.findViewById(R.id.Profile_relative);
        Profile_imageOval_yellow = (ImageView) viewLayout.findViewById(R.id.Profile_imageOval_yellow);
        Profile_textOnline = (TextView) viewLayout.findViewById(R.id.Profile_textOnline);
        Profile_textNumPhotos = (TextView) viewLayout.findViewById(R.id.Profile_textNumPhotos);
        imgDisplay = (ImageView) viewLayout.findViewById(R.id.Profile_imageid);
        topMenu_Profile  = (RelativeLayout) viewLayout.findViewById(R.id.topMenu_Profile);
        
        topMenu_imageProfile = (ImageView) viewLayout.findViewById(R.id.topMenu_imageProfile);
        topMenu_textName = (TextView) viewLayout.findViewById(R.id.topMenu_textName);
        topMenu_textAge = (TextView) viewLayout.findViewById(R.id.topMenu_textAge);
        //topMenu_textLocation = (TextView) viewLayout.findViewById(R.id.topMenu_textLocation);
        
        ///+++++
        
        //topMenu_textLocation.setText(_activity.getResources().getText(R.string.from)+" "+_userProfiles.get(position).UserCity+", "+_userProfiles.get(position).UserRegion+", "+_userProfiles.get(position).UserCountry);

        //------
        
        profile_imageLike = (ImageView) viewLayout.findViewById(R.id.profile_imageLike);
        profile_imageFavorite = (ImageView) viewLayout.findViewById(R.id.profile_imageFavorite);
        profile_imageBids = (ImageView) viewLayout.findViewById(R.id.profile_imageBids);
        profile_imageMessage = (ImageView) viewLayout.findViewById(R.id.profile_imageMessage);
        
        //profile_textLike = (TextView) viewLayout.findViewById(R.id.profile_textLike);
        //profile_textFavorite = (TextView) viewLayout.findViewById(R.id.profile_textFavorite);
        //profile_textBids = (TextView) viewLayout.findViewById(R.id.profile_textBids);
        //profile_textMessage = (TextView) viewLayout.findViewById(R.id.profile_textMessage);
        
        p_imageView1 = (ImageView) viewLayout.findViewById(R.id.p_imageView1);
        p_imageView2 = (ImageView) viewLayout.findViewById(R.id.p_imageView2);
        p_imageView3 = (ImageView) viewLayout.findViewById(R.id.p_imageView3);
        p_imageView4 = (ImageView) viewLayout.findViewById(R.id.p_imageView4);
        p_imageView5 = (ImageView) viewLayout.findViewById(R.id.p_imageView5);
        
        Profile_textName = (TextView) viewLayout.findViewById(R.id.Profile_textName);
        Profile_textAge = (TextView) viewLayout.findViewById(R.id.Profile_textAge);
        Profile_textLocation = (TextView) viewLayout.findViewById(R.id.Profile_textLocation);
        
        
        imageGrid = (ExpandableHeightGridView) viewLayout.findViewById(R.id.imageGrid);
        Profile_linearAccess = (LinearLayout) viewLayout.findViewById(R.id.Profile_linearAccess);
        Profile_textAccess = (TextView) viewLayout.findViewById(R.id.Profile_textAccess);
        
        Profile_textabout = (TextView) viewLayout.findViewById(R.id.Profile_textabout);
        Profile_textage3 = (TextView) viewLayout.findViewById(R.id.Profile_textage3);
        Profile_textzodiac3 = (TextView) viewLayout.findViewById(R.id.Profile_textzodiac3);
        Profile_textbodytype = (TextView) viewLayout.findViewById(R.id.Profile_textbodytype);
        Profile_texthaireyes = (TextView) viewLayout.findViewById(R.id.Profile_texthaireyes);
        Profile_texteducation = (TextView) viewLayout.findViewById(R.id.Profile_texteducation);
        Profile_textchilds = (TextView) viewLayout.findViewById(R.id.Profile_textchilds);
        Profile_textnation = (TextView) viewLayout.findViewById(R.id.Profile_textnation);
        Profile_textreligion = (TextView) viewLayout.findViewById(R.id.Profile_textreligion);
        Profile_textsmoke = (TextView) viewLayout.findViewById(R.id.Profile_textsmoke);
        Profile_textdrink = (TextView) viewLayout.findViewById(R.id.Profile_textdrink);
        Profile_textjob = (TextView) viewLayout.findViewById(R.id.Profile_textjob);
        
        Profile_imageStatus = (ImageView) viewLayout.findViewById(R.id.Profile_imageStatus);
        Profile_imageMeet = (ImageView) viewLayout.findViewById(R.id.Profile_imageMeet);
        Profile_textStatus = (TextView) viewLayout.findViewById(R.id.Profile_textStatus);
        Profile_textMeet = (TextView) viewLayout.findViewById(R.id.Profile_textMeet);
        Profile_textInterested1 = (TextView) viewLayout.findViewById(R.id.Profile_textInterested1);
        Profile_textInterested2 = (TextView) viewLayout.findViewById(R.id.Profile_textInterested2);
        Profile_textInterested3 = (TextView) viewLayout.findViewById(R.id.Profile_textInterested3);
        Profile_textInterested4 = (TextView) viewLayout.findViewById(R.id.Profile_textInterested4);
        Profile_textInterested5 = (TextView) viewLayout.findViewById(R.id.Profile_textInterested5);
        Profile_textInterested6 = (TextView) viewLayout.findViewById(R.id.Profile_textInterested6);
        Profile_textTitleaboutme = (TextView) viewLayout.findViewById(R.id.Profile_textTitleaboutme);
        Profile_textaboutme = (TextView) viewLayout.findViewById(R.id.Profile_textaboutme);
        Profile_textTitleaboutimaginefirst = (TextView) viewLayout.findViewById(R.id.Profile_textTitleaboutimaginefirst);
        Profile_textaboutimaginefirst = (TextView) viewLayout.findViewById(R.id.Profile_textaboutimaginefirst);
        
        Profile_imageMap = (ImageView) viewLayout.findViewById(R.id.Profile_imageMap);
        Profile_textMap = (TextView) viewLayout.findViewById(R.id.Profile_textMap);
        
        ARPhotosTask.put(position, new getPhotosTask(_userProfiles.get(position),Profile_textNumPhotos,imageGrid,imgDisplay).execute());
        ARgetCheckActionsTask.put(position, new getCheckActionsTask(_userProfiles.get(position), profile_imageFavorite,profile_imageLike,Profile_linearAccess,Profile_textAccess).execute());
        
        String imageurld150 = _userProfiles.get(position).ImageUrl;
        _userProfiles.get(position).ImageUrl = _userProfiles.get(position).ImageUrl.replace("/d150", "");
        Utils.getImageLoader(_activity);
        
        if(_userProfiles.get(position).ImageUrl.contains("Images2/woman"))
        {
        	_userProfiles.get(position).ImageUrl = "drawable://" + R.drawable.private_for_woman;
        	imageurld150 = "drawable://" + R.drawable.private_for_woman;
        }
        else if(_userProfiles.get(position).ImageUrl.contains("Images2/men.png"))
        {
        	_userProfiles.get(position).ImageUrl = "drawable://" + R.drawable.private_for_men;
        	imageurld150 = "drawable://" + R.drawable.private_for_men;
        }
        
        final ImageView spinner = (ImageView) viewLayout.findViewById(R.id.loading_rotate);
        final ImageView spinnerFr = (ImageView) viewLayout.findViewById(R.id.loading_rotateFR);
        if(Utils.getSharedPreferences(_activity).getString("genderid","").contains("2"))
		{
        	spinnerFr.setImageResource(R.drawable.woman_loading);
		}
        
		ImageLoader.getInstance().displayImage(_userProfiles.get(position).ImageUrl, imgDisplay,Utils.getImageOptions(),new SimpleImageLoadingListener() {
			public void onLoadingStarted(String imageUri, View view) {
				spinner.setVisibility(View.VISIBLE);
				spinnerFr.setVisibility(View.VISIBLE);
				spinner.startAnimation(animRotate);
				imgDisplay.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						photoOpen = true;
						/*
							ArrayList<String> viewpagerImages = new ArrayList<String>();
							viewpagerImages.add(_userProfiles.get(position).ImageUrl);
							Intent i = new Intent();
							i.setClass(_activity, ActShowPhoto.class);
							i.putExtra("ImageUrls", viewpagerImages);
							i.putExtra("ImageUserProfile", _userProfiles.get(position));
							i.putExtra("Index", 0);
							_activity.startActivityForResult(i,IMAGE_REQUEST);
							
							*/
					}
				});
			}
			@SuppressWarnings("unused")
			public void onLoadingFailed(String imageUri, View view) {
				spinner.setVisibility(View.GONE);
				spinnerFr.setVisibility(View.GONE);
			}
			public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage){
				spinner.setVisibility(View.GONE);
				spinnerFr.setVisibility(View.GONE);
				spinner.clearAnimation();
			}
		});
        
		ImageLoader.getInstance().displayImage(imageurld150, topMenu_imageProfile,Utils.getImageOptions());
        topMenu_textName.setText(""+_userProfiles.get(position).UserName);
        topMenu_textAge.setText(" - "+_userProfiles.get(position).UserAge+" "+_activity.getResources().getText(R.string.years));
       
        
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////
        fr_profile_linearMenudd.measure(0,0);  
       //// Log.v("APP", "dfbhdehb "+fr_profile_linearMenudd.getMeasuredHeight());
        LayoutParams params = (LayoutParams) fr_profile_linearMenudd.getLayoutParams();
        
       
        Profile_relative.measure(0,0);
       //// Log.v("APP", "dfbhdehb image"+Profile_relative.getMeasuredHeight());
        
       
       //// Log.v("APP", "dfbhdehb hActin_bar "+hActin_bar);
        
       //// Log.v("APP", "dfbhdehb hScreen "+hScreen);
    
        
     imgDisplay.setLayoutParams(new android.widget.RelativeLayout.LayoutParams(android.widget.RelativeLayout.LayoutParams.MATCH_PARENT, Math.round((hScreen-hActin_bar)*0.86f)));
     
   
     
     params.height = Math.round((hScreen-hActin_bar)*0.17f)+12;
     
     fr_profile_linearMenu.measure(0,0);  
     bottomMenu = Math.round(fr_profile_linearMenu.getMeasuredHeight()/(2.85f));
     
     
   ////  Log.v("APP", "dfbhdehb hScreen "+bottomMenu);
     //////////////////////////////////////////////////////////////////////////////////////////////////////
     
        fr_profile_scroll.setScrollViewListener(new ScrollViewListener() {
        	Boolean flag = false;
			@Override
			public void onScrollStopped() {}
			@SuppressLint("NewApi") 
			@Override
			public void onScrollChanged(ScrollView scrollView, int x, int y, int oldx,int oldy) {
				// TODO Auto-generated method stub
				
				if(y < bottomMenu && y >0)
				{
					fr_profile_linearMenu.setTranslationY(y);
					profile_image_shadow.setVisibility(View.VISIBLE);
				}
				else if(y > bottomMenu)
				{
					fr_profile_linearMenu.setTranslationY(bottomMenu);
					profile_image_shadow.setVisibility(View.VISIBLE);
				}
				else if(y<=0)
				{
					fr_profile_linearMenu.setTranslationY(0);
					profile_image_shadow.setVisibility(View.GONE);
				}
				//include1.setTranslationX(-y);
				if(y>=topMenu)
				{
					if(flag==false)
					{
						topMenu_Profile.setVisibility(View.VISIBLE);
						///////Profile_info.setVisibility(View.GONE);
						//topMenu_imageProfile.setAnimation(AnimationUtils.loadAnimation(_activity, R.anim.gridview_anm));
						//topMenu_textName.setAnimation(AnimationUtils.loadAnimation(_activity, R.anim.gridview_anm));
						topMenu_Profile.setAnimation(AnimationUtils.loadAnimation(_activity, R.anim.topmenu_anim));
						flag=true;
					}
				}
				else
				{
					if(flag==true)
					{
						topMenu_Profile.setVisibility(View.GONE);
						////////Profile_info.setVisibility(View.VISIBLE);
						flag=false;
					}
				}
				//Log.v("APP", "dfdfdf "+y);
			}
		});
        
        
        if(_userProfiles.get(position).IsVip!=null)
        {
        	if(_userProfiles.get(position).IsVip.equals("1"))
        	{
        		p_imageView1.setBackgroundResource(R.drawable.p_vip);
        	}
        }
        
        if(_userProfiles.get(position).GenderId.contains("1"))
        {
        	p_imageView3.setVisibility(View.GONE);
        	p_imageView4.setBackgroundResource(R.drawable.p_men_travel);
        	p_imageView5.setBackgroundResource(R.drawable.p_men_single);
        }
        else
        {
        	p_imageView3.setBackgroundResource(R.drawable.p_woman_work);
        	p_imageView4.setBackgroundResource(R.drawable.p_woman_travel);
        	p_imageView5.setBackgroundResource(R.drawable.p_woman_single);
        }
        
        
        if(_userProfiles.get(position).IsOnline == true)
        {
        	Profile_textOnline.setText("online");
			Profile_imageOval_yellow.setBackgroundResource(R.drawable.oval_yellow);
			//Profile_imageOval_yellow.startAnimation(animFadein);
        }
        else if(_userProfiles.get(position).IsOnline == false)
        {
        	Profile_textOnline.setText("offline");
			Profile_imageOval_yellow.setBackgroundResource(R.drawable.oval_red);
			//Profile_imageOval_yellow.clearAnimation();
        }
        else
        {
        	//new getUserStateTask(_userProfiles.get(position).ProfileId,Profile_textOnline,Profile_imageOval_yellow).execute();
        	ARUserStateTask.put(position, new getUserStateTask(_userProfiles.get(position).ProfileId,Profile_textOnline,Profile_imageOval_yellow).execute());
        }
        
        if(_userProfiles.get(position).PhotosCount!=0)
        {
        	Profile_textNumPhotos.setText(""+_userProfiles.get(position).PhotosCount+" "+_activity.getResources().getString(R.string.photos));
        }
        
        
        profile_imageBids.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				createOfferDialog(_userProfiles.get(position));
				offerDialog.show();
			}
		});
        profile_imageMessage.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Fragment fr;
				fr = new FragMessageView(null,_userProfiles.get(position),0,null);
				FragmentManager fm = _activity.getFragmentManager();
				
		        FragmentTransaction fragmentTransaction = fm.beginTransaction();
		        fragmentTransaction.setCustomAnimations(R.anim.left_slidein_list_animator, R.anim.left_slideout_list_animator,R.anim.right_slidein_list_animator, R.anim.right_slideout_list_animator);
		        fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
		        fragmentTransaction.addToBackStack(null);
		        fragmentTransaction.commit();
				
			}
		});
        
        
        
        Profile_textName.setText(""+_userProfiles.get(position).UserName);
        Profile_textAge.setText(_activity.getResources().getText(R.string.age)+" "+_userProfiles.get(position).UserAge+" "+_activity.getResources().getText(R.string.years));
        Profile_textLocation.setText(_activity.getResources().getText(R.string.from)+" "+_userProfiles.get(position).UserCity+", "+_userProfiles.get(position).UserRegion+", "+_userProfiles.get(position).UserCountry);

      
      
        imageGrid.setNumColumns(3);
        imageGrid.setFocusable(false);
        imageGrid.setExpanded(true);
        
        
        ARgetDetailsTask.put(position, new getDetailsTask(_userProfiles.get(position),p_imageView2,Profile_textabout,Profile_textage3,Profile_textzodiac3,Profile_textbodytype,Profile_texthaireyes,Profile_texteducation,Profile_textchilds,Profile_textnation,Profile_textreligion,Profile_textsmoke,Profile_textdrink,Profile_textjob,
        		Profile_imageStatus,Profile_imageMeet,
				Profile_textStatus,Profile_textMeet,Profile_textInterested1,Profile_textInterested2,Profile_textInterested3,Profile_textInterested4,Profile_textInterested5,Profile_textInterested6,Profile_textTitleaboutme,Profile_textaboutme,Profile_textTitleaboutimaginefirst,Profile_textaboutimaginefirst,Profile_imageMap).execute());
        
        
        
        Profile_textMap.setText(_activity.getResources().getString(R.string.view)+" "+_userProfiles.get(position).UserName + " " + _activity.getResources().getString(R.string.on_map));
        
        
        ((ViewPager) container).addView(viewLayout);
        return viewLayout;
	}
	
	@Override
    public void destroyItem(ViewGroup container, int position, Object object) 
	{
		ARPhotosTask.get(position).cancel(true);
    	ARgetDetailsTask.get(position).cancel(true);
    	ARgetCheckActionsTask.get(position).cancel(true);
        
        ((ViewPager) container).removeView((RelativeLayout) object);
    }
	
	
	class getPhotosTask extends AsyncTask<Void,Integer,ImagePathList>
	{
		TextView _Profile_textNumPhotos;
		ExpandableHeightGridView _imageGrid = new ExpandableHeightGridView(_activity);
		UserProfile _userprofile;
		ImageView _imgDisplay;
		public getPhotosTask(UserProfile userprofile,TextView Profile_textNumPhotos,ExpandableHeightGridView imageGrid,ImageView imgDisplay)
		{
			this._Profile_textNumPhotos = Profile_textNumPhotos;
			this._imageGrid = imageGrid;
			this._userprofile = userprofile;
			this._imgDisplay = imgDisplay;
		}
		
		@Override
		protected void onPreExecute()
		{
			photoOpen = false;
		}
		
		@Override
		protected ImagePathList doInBackground(Void... params)
		{
			ImagePathList list;
			list = ServiceClient.getUserPhotosThumbs( Utils.getSharedPreferences(_activity).getString("userid", ""),
					_userprofile.ProfileId,_userprofile.GenderId);
			return list;
		}
		 
		@Override
		protected void onPostExecute(final ImagePathList list)
		{
			if(photoOpen)
			{
				photoOpen = false;
				if(list!=null)
				{
					ArrayList<String> viewpagerImages = new ArrayList<String>();
					for(int i=0;i<list.PathList.size();i++)
					{
						if(_userprofile.ImageUrl.equals(list.PathList.get(i).Path.replace("/d150", "")))
						{
							//posit=i;
							viewpagerImages.add(0,list.PathList.get(i).Path);
						}
						else
						{
							viewpagerImages.add(list.PathList.get(i).Path);
						}
					}
					if(viewpagerImages.size()==0)
					{
						viewpagerImages.add(_userprofile.ImageUrl);
					}
					Intent i = new Intent(_activity,ActShowPhoto.class);
					i.putExtra("ImageUrls", viewpagerImages);
					i.putExtra("ImageUserProfile", _userprofile);
					i.putExtra("Index", 0);
					_activity.startActivityForResult(i,IMAGE_REQUEST);
				}
			}
			
			if(list!=null)
			{
				_Profile_textNumPhotos.setText(""+list.PathList.size()+" "+_activity.getResources().getString(R.string.photos));
				ImageAdapter adapter = new ImageAdapter(_activity);
				adapter.setItems(list);
				_imageGrid.setAdapter(adapter);
				
				_imgDisplay.setOnClickListener(new OnClickListener() {
					int posit=0;
					@Override
					public void onClick(View v) 
					{
						// TODO Auto-generated method stub
						if(list!=null)
						{
							ArrayList<String> viewpagerImages = new ArrayList<String>();
							
							
							for(int i=0;i<list.PathList.size();i++)
							{
								if(_userprofile.ImageUrl.equals(list.PathList.get(i).Path.replace("/d150", "")))
								{
									//posit=i;
									viewpagerImages.add(0,list.PathList.get(i).Path);
								}
								else
								{
									viewpagerImages.add(list.PathList.get(i).Path);
								}
							}
							if(viewpagerImages.size()==0)
							{
								viewpagerImages.add(_userprofile.ImageUrl);
							}
							Intent i = new Intent(_activity,ActShowPhoto.class);
							i.putExtra("ImageUrls", viewpagerImages);
							i.putExtra("ImageUserProfile", _userprofile);
							i.putExtra("Index", posit);
							_activity.startActivityForResult(i,IMAGE_REQUEST);
						}
					}
				});
				
				if(_userProfiles.size()==1)
				{
					_imgDisplay.setOnTouchListener(new OnTouchListener() {
						
						Float old = 0.0f;
						Boolean flagOpen = false;
						@SuppressLint("ClickableViewAccessibility") 
						@Override
						public boolean onTouch(View v, MotionEvent event) 
						{
							// TODO Auto-generated method stub	
							switch(event.getAction())
							{
								case MotionEvent.ACTION_DOWN:
					               old = event.getX();
					               flagOpen = false;
					                return true;
					           
					            case MotionEvent.ACTION_MOVE:
					            	if(Math.abs(event.getX()-old)>20 && !flagOpen)
					            	{
					            		int posit = 0;
						            	if(list!=null)
										{
											ArrayList<String> viewpagerImages = new ArrayList<String>();
											
											for(int i=0;i<list.PathList.size();i++)
											{
												if(_userprofile.ImageUrl.equals(list.PathList.get(i).Path.replace("/d150", "")))
												{
													//posit=i;
													viewpagerImages.add(0,list.PathList.get(i).Path);
												}
												else
												{
													viewpagerImages.add(list.PathList.get(i).Path);
												}	
											}
											if(viewpagerImages.size()==0)
											{
												viewpagerImages.add(_userprofile.ImageUrl);
											}
											Intent i = new Intent(_activity,ActShowPhoto.class);
											i.putExtra("ImageUrls", viewpagerImages);
											i.putExtra("ImageUserProfile", _userprofile);
											i.putExtra("Index", posit);
											_activity.startActivityForResult(i,IMAGE_REQUEST);
										}
						            	flagOpen = true;
					            	}
					                return true;
					            case MotionEvent.ACTION_UP:
					            	int posit = 0;
					            	if(list!=null)
									{
										ArrayList<String> viewpagerImages = new ArrayList<String>();
										for(int i=0;i<list.PathList.size();i++)
										{
											if(_userprofile.ImageUrl.equals(list.PathList.get(i).Path.replace("/d150", "")))
											{
												//posit=i;
												viewpagerImages.add(0,list.PathList.get(i).Path);
											}
											else
											{
												viewpagerImages.add(list.PathList.get(i).Path);
											}
										}
										if(viewpagerImages.size()==0)
										{
											viewpagerImages.add(_userprofile.ImageUrl);
										}
										Intent i = new Intent(_activity,ActShowPhoto.class);
										i.putExtra("ImageUrls", viewpagerImages);
										i.putExtra("ImageUserProfile", _userprofile);
										i.putExtra("Index", posit);
										_activity.startActivityForResult(i,IMAGE_REQUEST);
									}
					                return true;
					            }
					            return false;
						}
					});
				}
				
				_imageGrid.setOnItemClickListener(new OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1, int index,long arg3) 
					{
						if(list!=null){
							ArrayList<String> viewpagerImages = new ArrayList<String>();
							for(int i=0;i<list.PathList.size();i++)
							{
								viewpagerImages.add(list.PathList.get(i).Path);
							}
							
							if(viewpagerImages.size()==0)
							{
								viewpagerImages.add(_userprofile.ImageUrl);
							}
							Intent i = new Intent(_activity,ActShowPhoto.class);
							i.putExtra("ImageUrls", viewpagerImages);
							i.putExtra("ImageUserProfile", _userprofile);
							if(index >= viewpagerImages.size())
							{
								i.putExtra("Index", 0);
							}
							else
							{
								i.putExtra("Index", index);
							}
							_activity.startActivityForResult(i,IMAGE_REQUEST);
						}
					}
				});
			}
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	@SuppressLint("NewApi") 
	class getDetailsTask extends AsyncTask<Void, Integer, UserDetails>
	{
		UserProfile _userprofile;
		ImageView _p_imageView2;
		TextView Profile_textabout,Profile_textage3,Profile_textzodiac3;
		TextView Profile_textbodytype,Profile_texthaireyes,Profile_texteducation,Profile_textchilds,Profile_textnation,Profile_textreligion,Profile_textsmoke,Profile_textdrink,Profile_textjob;
		ImageView Profile_imageStatus,Profile_imageMeet;
        TextView Profile_textStatus,Profile_textMeet,Profile_textInterested1,Profile_textInterested2,Profile_textInterested3,Profile_textInterested4,Profile_textInterested5,Profile_textInterested6;
        TextView Profile_textTitleaboutme;
        TextView Profile_textaboutme;
        TextView Profile_textTitleaboutimaginefirst;
        TextView Profile_textaboutimaginefirst;
        ImageView Profile_imageMap;
        
        
		public getDetailsTask(UserProfile userprofile,ImageView p_imageView2,TextView Profile_textabout,TextView Profile_textage3,TextView Profile_textzodiac3,TextView Profile_textbodytype,TextView Profile_texthaireyes,TextView Profile_texteducation,TextView Profile_textchilds,TextView Profile_textnation,TextView Profile_textreligion,TextView Profile_textsmoke,TextView Profile_textdrink,TextView Profile_textjob,
				ImageView Profile_imageStatus,ImageView Profile_imageMeet,
				TextView Profile_textStatus,TextView Profile_textMeet,TextView Profile_textInterested1,TextView Profile_textInterested2,TextView Profile_textInterested3,TextView Profile_textInterested4,TextView Profile_textInterested5,TextView Profile_textInterested6,
				TextView Profile_textTitleaboutme,
				TextView Profile_textaboutme,
				TextView Profile_textTitleaboutimaginefirst,
				TextView Profile_textaboutimaginefirst,
				ImageView Profile_imageMap)
		{
			this._userprofile = userprofile;
			this._p_imageView2 = p_imageView2;
			this.Profile_textabout = Profile_textabout;
			this.Profile_textage3 = Profile_textage3;
			this.Profile_textzodiac3 = Profile_textzodiac3;
			this.Profile_textbodytype = Profile_textbodytype;
			this.Profile_texthaireyes = Profile_texthaireyes;
			this.Profile_texteducation = Profile_texteducation;
			this.Profile_textchilds = Profile_textchilds;
			this.Profile_textnation = Profile_textnation;
			this.Profile_textreligion = Profile_textreligion;
			this.Profile_textsmoke = Profile_textsmoke;
			this.Profile_textdrink = Profile_textdrink;
			this.Profile_textjob = Profile_textjob;
			
			this.Profile_imageStatus = Profile_imageStatus;
			this.Profile_imageMeet = Profile_imageMeet;
			this.Profile_textStatus = Profile_textStatus;
			this.Profile_textMeet = Profile_textMeet;
			this.Profile_textInterested1 = Profile_textInterested1;
			this.Profile_textInterested2 = Profile_textInterested2;
			this.Profile_textInterested3 = Profile_textInterested3;
			this.Profile_textInterested4 = Profile_textInterested4;
			this.Profile_textInterested5 = Profile_textInterested5;
			this.Profile_textInterested6 = Profile_textInterested6;
			this.Profile_textTitleaboutme = Profile_textTitleaboutme;
			this.Profile_textaboutme = Profile_textaboutme;
			this.Profile_textTitleaboutimaginefirst = Profile_textTitleaboutimaginefirst;
			this.Profile_textaboutimaginefirst = Profile_textaboutimaginefirst;
			this.Profile_imageMap = Profile_imageMap;
		}
		
		@Override
		protected void onPreExecute()
		{
		}

		@Override
		protected UserDetails doInBackground(Void... params) 
		{		// Global.Lang
			UserDetails details;
			details = ServiceClient.getUserDetails(_userprofile.ProfileId,Global.Lang);			
			return details;
		}
		@Override
		protected void onPostExecute(final UserDetails details)
		{
			if(details!=null)
			{
				try 
				{
					if(details.zodiac.contains("Gemini"))
					{
						_p_imageView2.setBackgroundResource(R.drawable.p_gemini);
					}
					else if(details.zodiac.contains("Aries"))
					{
						_p_imageView2.setBackgroundResource(R.drawable.p_aries);
					}
					else if(details.zodiac.contains("Aquarius"))
					{
						_p_imageView2.setBackgroundResource(R.drawable.p_aruarius);
					}
					else if(details.zodiac.contains("Cancer"))
					{
						_p_imageView2.setBackgroundResource(R.drawable.p_cancer);
					}
					else if(details.zodiac.contains("Capricorn"))
					{
						_p_imageView2.setBackgroundResource(R.drawable.p_capricorn);
					}
					else if(details.zodiac.contains("Leo"))
					{
						_p_imageView2.setBackgroundResource(R.drawable.p_leo);
					}
					else if(details.zodiac.contains("Libra"))
					{
						_p_imageView2.setBackgroundResource(R.drawable.p_libra);
					}
					else if(details.zodiac.contains("Pisces"))
					{
						_p_imageView2.setBackgroundResource(R.drawable.p_pisces);
					}
					else if(details.zodiac.contains("Sagittarius"))
					{
						_p_imageView2.setBackgroundResource(R.drawable.p_sagittarius);
					}
					else if(details.zodiac.contains("Scorpio"))
					{
						_p_imageView2.setBackgroundResource(R.drawable.p_scorpio);
					}
					else if(details.zodiac.contains("Taurus"))
					{
						_p_imageView2.setBackgroundResource(R.drawable.p_taurus);
					}
					else if(details.zodiac.contains("Virgo"))
					{
						_p_imageView2.setBackgroundResource(R.drawable.p_virgo);
					}
					else
					{
						_p_imageView2.setVisibility(View.GONE);
					}
					
					Profile_textabout.setText(_userprofile.UserName);
					Profile_textage3.setText(details.age);
					Profile_textzodiac3.setText("("+details.zodiac+")");	
					Profile_textbodytype.setText(details.bodytype);
					Profile_texthaireyes.setText(details.haireyes);
					Profile_texteducation.setText(details.education);
					Profile_textchilds.setText(details.childs);
					Profile_textnation.setText(details.nationality);
					Profile_textreligion.setText(details.religion);
					String[] result = details.smokedring.split("/");
					if(result.length == 1)
					{
						Profile_textsmoke.setText(result[0]);
					}
					if(result.length == 2)
					{
						Profile_textsmoke.setText(result[0]);
						Profile_textdrink.setText(result[1]);
					}
					
					Profile_textjob.setText(details.job);
					
					Profile_textStatus.setText(details.familystate);
					Profile_textMeet.setText(details.lookfor);
					
					if(details.familystate.equals("Μόνος/η") || details.familystate.equals("Single") || details.familystate.equals("Solo/a"))
					{
						if(_userprofile.GenderId.equals("1"))
						{
							Profile_imageStatus.setImageResource(R.drawable.single_men_big);
						}
						else
						{
							//na mou to steilei o andreas
							Profile_imageStatus.setImageResource(R.drawable.famele_icon);
						}
					}
					else
					{
						Profile_imageStatus.setImageResource(R.drawable.couple_big);
					}
					
					if(details.lookfor.equals("Άντ�?ας") || details.lookfor.equals("Male") || details.lookfor.equals("Erkek") || details.lookfor.equals("Masculino"))
					{
						Profile_imageMeet.setImageResource(R.drawable.male_icon);
					}
					else
					{
						Profile_imageMeet.setImageResource(R.drawable.famele_icon);
					}
					String[] result2 = details.interest.split("</br>");
					if(result2.length == 1)
					{
						Profile_textInterested1.setText("");
						Profile_textInterested2.setVisibility(View.GONE);
						Profile_textInterested3.setVisibility(View.GONE);
						Profile_textInterested4.setVisibility(View.GONE);
						Profile_textInterested5.setVisibility(View.GONE);
						Profile_textInterested6.setVisibility(View.GONE);
					}
					else if(result2.length == 2)
					{
						Profile_textInterested1.setText(result2[1]);
						Profile_textInterested2.setText("");
						Profile_textInterested3.setVisibility(View.GONE);
						Profile_textInterested4.setVisibility(View.GONE);
						Profile_textInterested5.setVisibility(View.GONE);
						Profile_textInterested6.setVisibility(View.GONE);
					}
					else if(result2.length == 3)
					{
						Profile_textInterested1.setText(result2[1]);
						Profile_textInterested2.setText(result2[2]);
						Profile_textInterested3.setText("");
						Profile_textInterested4.setVisibility(View.GONE);
						Profile_textInterested5.setVisibility(View.GONE);
						Profile_textInterested6.setVisibility(View.GONE);
					}
					else if(result2.length == 4)
					{
						Profile_textInterested1.setText(result2[1]);
						Profile_textInterested2.setText(result2[2]);
						Profile_textInterested3.setText(result2[3]);
						Profile_textInterested4.setText("");
						Profile_textInterested5.setVisibility(View.GONE);
						Profile_textInterested6.setVisibility(View.GONE);
					}
					else if(result2.length == 5)
					{
						Profile_textInterested1.setText(result2[1]);
						Profile_textInterested2.setText(result2[2]);
						Profile_textInterested3.setText(result2[3]);
						Profile_textInterested4.setText(result2[4]);
						Profile_textInterested5.setText("");
						Profile_textInterested6.setVisibility(View.GONE);
					}
					else if(result2.length == 6)
					{
						Profile_textInterested1.setText(result2[1]);
						Profile_textInterested2.setText(result2[2]);
						Profile_textInterested3.setText(result2[3]);
						Profile_textInterested4.setText(result2[4]);
						Profile_textInterested5.setText(result2[5]);
						Profile_textInterested6.setText("");
					}
					
					
					if(details.aboutme_describeyourself.length()!=0)
					{
						Profile_textTitleaboutme.setVisibility(View.VISIBLE);
						Profile_textaboutme.setVisibility(View.VISIBLE);
						Profile_textaboutme.setText(details.aboutme_describeyourself);
					}
					
					
					if(details.aboutme_describedate.length()!=0)
					{
						Profile_textTitleaboutimaginefirst.setVisibility(View.VISIBLE);
						Profile_textaboutimaginefirst.setVisibility(View.VISIBLE);
						Profile_textaboutimaginefirst.setText(details.aboutme_describedate);
					}
					
					
				} catch (Exception e) {
					// TODO: handle exception
					
					//Log.v("APP", "dghdgjdeh ");
				}
				
				Profile_imageMap.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						Intent intent = new Intent();
						intent.setClass(_activity, ActGoogleMap.class);
						intent.putExtra("UserName", details.UserName);
						intent.putExtra("Lat", Double.parseDouble(details.Latitude));
						intent.putExtra("Lng",Double.parseDouble( details.Longtitude));
					
						_activity.startActivity(intent);
						
						
					}
				});
				
				
			}	
		}
	}
	
	
	
	
	
	@SuppressLint("NewApi") 
	class getUserStateTask extends AsyncTask<Void, Integer, String>
	{
		String _userID;
		TextView _Profile_textOnline;
		ImageView _Profile_imageOval_yellow;
		public getUserStateTask(String userID,TextView Profile_textOnline,ImageView Profile_imageOval_yellow)
		{
			this._userID = userID;
			this._Profile_textOnline = Profile_textOnline;
			this._Profile_imageOval_yellow = Profile_imageOval_yellow;
		}
		@Override
		protected String doInBackground(Void... params)
		{
			String result = ServiceClient.getUserState(_userID);
			return result;
		}
	
		@Override
		protected void onPostExecute(String res)
		{
			if(res.contains("Online"))
			{
				_Profile_textOnline.setText("online");
				_Profile_imageOval_yellow.setBackgroundResource(R.drawable.oval_yellow);
				//_Profile_imageOval_yellow.startAnimation(animFadein);
			}
			else
			{
				_Profile_textOnline.setText("offline");
				_Profile_imageOval_yellow.setBackgroundResource(R.drawable.oval_red);
				//_Profile_imageOval_yellow.clearAnimation();
			}
		}
	}
	
	
	@SuppressLint("NewApi") 
	class checkLike extends AsyncTask<Void,Void,String>
	{
		String myprofileid,otherprofileid;
		TextView textLike;
		
		UserProfile _userprofile;
		ImageView _profile_imageLike;
		
		public checkLike(UserProfile userprofile,ImageView profile_imageLike)
		{
			this._userprofile = userprofile;
			this._profile_imageLike = profile_imageLike;
		}
		
		@Override
		protected String doInBackground(Void... params)
		{
			String res = ServiceClient.checkLike(Utils.getSharedPreferences(_activity).getString("userid", ""), _userprofile.ProfileId);
			return res;
		}
		
		@Override
		protected void onPostExecute(String res)
		{
			if(res.contains("true"))
			{
				_profile_imageLike.setBackgroundResource(R.drawable.p_like);
				_profile_imageLike.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						new likeTask(_userprofile, _profile_imageLike).execute();
					}
				});
			}
			else
			{
				_profile_imageLike.setBackgroundResource(R.drawable.p_like_enable);
				_profile_imageLike.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						Toast.makeText(_activity, _activity.getResources().getString(R.string.you_have_already_made_like), Toast.LENGTH_LONG).show();
					}
				});
			}
		}		
	}
	
	@SuppressLint("NewApi") 
	class checkFavorite extends AsyncTask<Void,Void,String>
	{
		UserProfile _userprofile;
		ImageView _profile_imageFavorite;
		public checkFavorite(UserProfile userprofile,ImageView profile_imageFavorite)
		{
			this._userprofile = userprofile;
			this._profile_imageFavorite = profile_imageFavorite;
		}
		@Override
		protected String doInBackground(Void... params) 
		{
			String resutl = ServiceClient.checkFavorite(Utils.getSharedPreferences(_activity).getString("userid", ""), _userprofile.ProfileId);
			return resutl;
		}
		
		@Override
		protected void onPostExecute(String res)
		{
			if(res.contains("true"))
			{
				_profile_imageFavorite.setBackgroundResource(R.drawable.p_favorite_enable);
				_profile_imageFavorite.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						new unFavoriteTask(_userprofile, _profile_imageFavorite).execute();
						
					}
				});
			}
			else
			{
				_profile_imageFavorite.setBackgroundResource(R.drawable.p_favorite);
				
				_profile_imageFavorite.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						new favoriteTask(_userprofile, _profile_imageFavorite).execute();
						
					}
				});
			}

		}
	}
	
	
	class getCheckActionsTask extends AsyncTask<Void,Void,CheckActions>
	{
		UserProfile _userprofile;
		ImageView _profile_imageFavorite;
		ImageView _profile_imageLike;
		LinearLayout _Profile_linearAccess;
		TextView _Profile_textAccess;
		
		public getCheckActionsTask(UserProfile userprofile,ImageView profile_imageFavorite,ImageView profile_imageLike,LinearLayout Profile_linearAccess,TextView Profile_textAccess)
		{
			this._userprofile = userprofile;
			this._profile_imageFavorite = profile_imageFavorite;
			this._profile_imageLike = profile_imageLike;
			this._Profile_linearAccess = Profile_linearAccess;
			this._Profile_textAccess = Profile_textAccess;
		}
		@Override
		protected CheckActions doInBackground(Void... params) 
		{
			CheckActions resutl = ServiceClient.getCheckActions(Utils.getSharedPreferences(_activity).getString("userid", ""), _userprofile.ProfileId);
			return resutl;
		}
		
		@Override
		protected void onPostExecute(final CheckActions res)
		{
			if(res!=null)
			{
				if(res.favorite.contains("1"))
				{
					_profile_imageFavorite.setBackgroundResource(R.drawable.p_favorite_enable);
					_profile_imageFavorite.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							new unFavoriteTask(_userprofile, _profile_imageFavorite).execute();
							
						}
					});
				}
				else
				{
					_profile_imageFavorite.setBackgroundResource(R.drawable.p_favorite);
					_profile_imageFavorite.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							new favoriteTask(_userprofile, _profile_imageFavorite).execute();
							
						}
					});
				}
				//////////////////////////////////////////////
				if(res.like.contains("1"))
				{
					_profile_imageLike.setBackgroundResource(R.drawable.p_like);
					_profile_imageLike.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							new likeTask(_userprofile, _profile_imageLike).execute();
						}
					});
				}
				else
				{
					_profile_imageLike.setBackgroundResource(R.drawable.p_like_enable);
					_profile_imageLike.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							Toast.makeText(_activity, _activity.getResources().getString(R.string.you_have_already_made_like), Toast.LENGTH_LONG).show();
						}
					});
				}
				/////////////////////////////////////////////////////////
				if ( res.photolevel.length() < 6) 
				{
					if ( res.photolevel.indexOf("10") != -1 ) 
					{
						_Profile_textAccess.setText(R.string.p10);
					} 
					else if ( res.photolevel.indexOf("9") != -1 ) 
					{
						_Profile_textAccess.setText(R.string.p9);
					} 
					else if ( res.photolevel.indexOf("8") != -1 ) 
					{
						_Profile_textAccess.setText(R.string.p8);
					}
					else if ( res.photolevel.indexOf("7") != -1 )
					{
						_Profile_textAccess.setText(R.string.p7);
					} 
					else if ( res.photolevel.indexOf("6") != -1 ) 
					{
						_Profile_textAccess.setText(R.string.p6);
					}
					else if ( res.photolevel.indexOf("5") != -1 ) 
					{
						_Profile_textAccess.setText(R.string.p5);
					}
					else if ( res.photolevel.indexOf("4") != -1 ) 
					{
						_Profile_textAccess.setText(R.string.p4);
					} 
					else if ( res.photolevel.indexOf("3") != -1 )
					{
						_Profile_textAccess.setText(R.string.p3);
					} 
					else if ( res.photolevel.indexOf("2") != -1 ) 
					{
						_Profile_textAccess.setText(R.string.p2);
					} 
					else if ( res.photolevel.indexOf("1") != -1 ) 
					{
						_Profile_textAccess.setText(R.string.p1);
					} 
					else if ( res.photolevel.indexOf("0") != -1 ) 
					{
						_Profile_textAccess.setText(R.string.p0);
					}
					else {}
				}
				////////////////////////////////////
				
				Log.v("APP", "adgegegegg "+res.minlevel);
				if(res.minlevel!=null)
				{
					
					_Profile_linearAccess.setOnClickListener(new OnClickListener() 
					{	
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							showCategory(_Profile_textAccess, _userprofile.ProfileId,res.minlevel);
						}
					});
				}
			}
		}
	}
	
	
	
	
	
	@SuppressLint("NewApi") 
	class favoriteTask extends AsyncTask<Void,Void,String>
	{
		UserProfile _userprofile;
		ImageView _profile_imageFavorite;
		@Override
		protected void onPreExecute()
		{
			fragLoading.Show();
		}
		public favoriteTask(UserProfile userprofile,ImageView profile_imageFavorite)
		{
			this._userprofile = userprofile;
			this._profile_imageFavorite = profile_imageFavorite;
		}
		
		@Override
		protected String doInBackground(Void... params) 
		{
			ServiceClient.favorite(Utils.getSharedPreferences(_activity).getString("userid", ""), _userprofile.ProfileId);
			return null;
		}
		
		@Override
		protected void onPostExecute(String res)
		{
			fragLoading.Close();
			new checkFavorite(_userprofile, _profile_imageFavorite).execute();
			_profile_imageFavorite.setBackgroundResource(R.drawable.p_favorite_enable);
		}
	}
	
	@SuppressLint("NewApi") 
	class unFavoriteTask extends AsyncTask<Void,Void,String>
	{
		UserProfile _userprofile;
		ImageView _profile_imageFavorite;
		
		@Override
		protected void onPreExecute()
		{
			fragLoading.Show();
		}
		public unFavoriteTask(UserProfile userprofile,ImageView profile_imageFavorite)
		{
			this._userprofile = userprofile;
			this._profile_imageFavorite = profile_imageFavorite;
		}
		
		@Override
		protected String doInBackground(Void... params) 
		{
			ServiceClient.unfavorite(Utils.getSharedPreferences(_activity).getString("userid", ""), _userprofile.ProfileId);
			return null;
		}
		
		@Override
		protected void onPostExecute(String res)
		{
			fragLoading.Close();
			new checkFavorite(_userprofile, _profile_imageFavorite).execute();
			_profile_imageFavorite.setBackgroundResource(R.drawable.p_favorite);
		}
	}
	
	
	@SuppressLint("NewApi") 
	class likeTask extends AsyncTask<Void,Void,String>
	{
		UserProfile _userprofile;
		ImageView _profile_imageLike;
		
		@Override
		protected void onPreExecute()
		{
			fragLoading.Show();
		}
		public likeTask(UserProfile userprofile,ImageView profile_imageLike)
		{
			this._userprofile = userprofile;
			this._profile_imageLike = profile_imageLike;
		}
		
		@Override
		protected String doInBackground(Void... params) 
		{
			String res = ServiceClient.like(Utils.getSharedPreferences(_activity).getString("userid", ""), _userprofile.ProfileId);			 
			return res;
		}
		
		@Override
		protected void onPostExecute(String p)
		{
			fragLoading.Close();
			if(p.contains("done"))
			{
				//textLike.setTextColor(getResources().getColor(R.color.grey));
				//actionLike.setBackgroundResource(R.drawable.like_button_1_hover);
				//like = true;
				_profile_imageLike.setBackgroundResource(R.drawable.p_like_enable);
			}	
			else if(p.contains("nophotos"))
			{
				Toast.makeText(_activity, _activity.getResources().getString(R.string.plzaddphoto), Toast.LENGTH_LONG).show();			
			}
		}		
	}
	
	
	
	
	
	
	
	Dialog offerDialog;
	int offerAmount = 35;
	private void createOfferDialog(final UserProfile user)
	{
		offerAmount = 35;
		offerDialog = new Dialog(_activity, R.style.DialogSlideAnim);
		offerDialog.setContentView(R.layout.dialog_offer);
		
		final Animation fade_diallog_offerUp = AnimationUtils.loadAnimation(_activity, R.anim.fade_dialog_offer);
		final Animation fade_diallog_offerDown = AnimationUtils.loadAnimation(_activity, R.anim.fade_dialog_offer);
		final Animation fade_in = AnimationUtils.loadAnimation(_activity, R.anim.fade_in_dialog_offer);
		final Animation fade_out = AnimationUtils.loadAnimation(_activity, R.anim.fade_out_dialog_offer);
		
		TextView dialog_offer_textUsername = (TextView) offerDialog.findViewById(R.id.dialog_offer_textUsername);
		ImageView dialog_offer_imageProfile = (ImageView) offerDialog.findViewById(R.id.dialog_offer_imageProfile);
		final TextView dialog_offer_textPrice1 = (TextView) offerDialog.findViewById(R.id.dialog_offer_textPrice1);
		final TextView dialog_offer_textPrice2 = (TextView) offerDialog.findViewById(R.id.dialog_offer_textPrice2);
		RelativeLayout dialog_offer_relativeLayoutDown = (RelativeLayout) offerDialog.findViewById(R.id.dialog_offer_relativeLayoutDown);
		final RelativeLayout dialog_offer_relativeLayoutColorDown = (RelativeLayout) offerDialog.findViewById(R.id.dialog_offer_relativeLayoutColorDown);
		dialog_offer_relativeLayoutColorDown.startAnimation(fade_diallog_offerDown);
		final RelativeLayout dialog_offer_relativeLayoutUp = (RelativeLayout) offerDialog.findViewById(R.id.dialog_offer_relativeLayoutUp);
		final RelativeLayout dialog_offer_relativeLayoutColorUp = (RelativeLayout) offerDialog.findViewById(R.id.dialog_offer_relativeLayoutColorUp);
		dialog_offer_relativeLayoutColorUp.startAnimation(fade_diallog_offerUp);
		RelativeLayout dialog_offer_relativeLayoutSend = (RelativeLayout) offerDialog.findViewById(R.id.dialog_offer_relativeLayoutSend);
		
		dialog_offer_textUsername.setText(user.UserName);
		Utils.getImageLoader(_activity).displayImage(user.ImageUrl.replace("d150", "thumbs"), dialog_offer_imageProfile, Utils.getImageOptions());
		
		dialog_offer_relativeLayoutDown.setOnClickListener(new OnClickListener() {	
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(offerAmount>1)
				{
					dialog_offer_relativeLayoutColorDown.startAnimation(fade_diallog_offerDown);
					offerAmount--;
					dialog_offer_textPrice2.startAnimation(fade_in);
					dialog_offer_textPrice2.setText("€ "+offerAmount);
					dialog_offer_textPrice1.startAnimation(fade_out);
					dialog_offer_textPrice1.setText("€ "+(offerAmount+1));
				}
			}
		});
		
		dialog_offer_relativeLayoutUp.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(offerAmount<40)
				{
					dialog_offer_relativeLayoutColorUp.startAnimation(fade_diallog_offerUp);
					offerAmount++;
					dialog_offer_textPrice2.startAnimation(fade_in);
					dialog_offer_textPrice2.setText("€ "+offerAmount);
					dialog_offer_textPrice1.startAnimation(fade_out);
					dialog_offer_textPrice1.setText("€ "+(offerAmount-1));
				}
				
			}
		});
		dialog_offer_relativeLayoutSend.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//Log.v("APP", "rtrtrt "+price);
				new offerTask(user.ProfileId,String.valueOf(offerAmount)).execute();
				offerDialog.cancel();
			}
		});
	}
	
	
	
	class offerTask extends AsyncTask<String,Void,String>
	{
        String result;
        String _userid,_offerAmount;
        public offerTask(String userid,String offerAmount)
        {
        	this._userid = userid;
        	this._offerAmount = offerAmount;
        }
        @Override
		protected void onPreExecute()
        {}
        
		@Override
		protected String doInBackground(String... params) 
		{
			result = ServiceClient.makeOffer("1", _offerAmount, Utils.getSharedPreferences(_activity).getString("userid", ""), _userid, Utils.getSharedPreferences(_activity).getString("genderid",""),"0");
			return result;
		}
		
		@Override
		protected void onPostExecute(String res)
		{
			if(result.contains("created"))
			{
				Toast.makeText(_activity, _activity.getResources().getString(R.string.offersent), Toast.LENGTH_LONG).show();	
			}
			else if(result.contains("already"))
			{
				Toast.makeText(_activity, _activity.getString(R.string.offeralready), Toast.LENGTH_LONG).show();
			}
			else if(result.contains("ERROR"))
			{
				Toast.makeText(_activity, _activity.getString(R.string.errorsent), Toast.LENGTH_LONG).show();			
			}
		}
	}
	
	
	class viewProfileTask extends AsyncTask<Void,Void,Void>
	{
		String _userid;
		
		public viewProfileTask(String userid)
		{
			this._userid = userid;
		}
		
		@Override
		protected Void doInBackground(Void... params) 
		{
			ServiceClient.viewProfile(Utils.getSharedPreferences(_activity).getString("userid", ""), _userid);
			return null;
		}		
	}
	
	
	
	
	class getLevelTask extends AsyncTask<String, Integer, String>
	{
		TextView Profile_textAccess;
		public getLevelTask(TextView Profile_textAccess)
		{
			this.Profile_textAccess = Profile_textAccess;
		}
		@Override
		protected String doInBackground(String... params) 
		{
			String result = ServiceClient.getLevel(Utils.getSharedPreferences(_activity).getString("userid", ""),params[0]);
			return result;
		}
	
		@Override
		protected void onPostExecute(String res)
		{
				if ( res.length() < 6) 
				{
					if ( res.indexOf("10") != -1 ) 
					{
						Profile_textAccess.setText(R.string.p10);
					} 
					else if ( res.indexOf("9") != -1 ) 
					{
						Profile_textAccess.setText(R.string.p9);
					} 
					else if ( res.indexOf("8") != -1 ) 
					{
						Profile_textAccess.setText(R.string.p8);
					}
					else if ( res.indexOf("7") != -1 )
					{
						Profile_textAccess.setText(R.string.p7);
					} 
					else if ( res.indexOf("6") != -1 ) 
					{
						Profile_textAccess.setText(R.string.p6);
					}
					else if ( res.indexOf("5") != -1 ) 
					{
						Profile_textAccess.setText(R.string.p5);
					}
					else if ( res.indexOf("4") != -1 ) 
					{
						Profile_textAccess.setText(R.string.p4);
					} 
					else if ( res.indexOf("3") != -1 )
					{
						Profile_textAccess.setText(R.string.p3);
					} 
					else if ( res.indexOf("2") != -1 ) 
					{
						Profile_textAccess.setText(R.string.p2);
					} else if ( res.indexOf("1") != -1 ) 
					{
						Profile_textAccess.setText(R.string.p1);
					} 
					else if ( res.indexOf("0") != -1 ) 
					{
						Profile_textAccess.setText(R.string.p0);
					}
					else {}
				}
			
		}
	}
	
	
	Boolean Npublic1 = true;
	Boolean Npr1 = false;
	Boolean Npr2 = false;
	Boolean Npr3 = false;
	Boolean Npr4 = false;
	Boolean Npr5 = false;
	Boolean Npr6 = false;
	Boolean Npr7 = false;
	Boolean Npr8 = false;
    Boolean Npr9 = false;
	Boolean Npr10 = false;
	
	private void showCategory(final TextView Profile_textAccess,final String Userid,final String minlevel) 
	{
		resultLevel = minlevel;
		
		
		final Dialog d = new Dialog(_activity, R.style.DialogSlideAnim);
		d.setContentView(R.layout.level_view_pro);
		Button cancelButton = (Button) d.findViewById(R.id.cancel_btn);
		Button okButton = (Button) d.findViewById(R.id.ok_btn);
		final LinearLayout public1 = (LinearLayout) d.findViewById(R.id.public1);
	    final LinearLayout pr1 = (LinearLayout) d.findViewById(R.id.private1);
	    final LinearLayout pr2 = (LinearLayout) d.findViewById(R.id.private2);
	    final LinearLayout pr3 = (LinearLayout) d.findViewById(R.id.private3);
		final LinearLayout pr4 = (LinearLayout) d.findViewById(R.id.private4);
	    final LinearLayout pr5 = (LinearLayout) d.findViewById(R.id.private5);
	    final LinearLayout pr6 = (LinearLayout) d.findViewById(R.id.private6);
		final LinearLayout pr7 = (LinearLayout) d.findViewById(R.id.private7);
	    final LinearLayout pr8 = (LinearLayout) d.findViewById(R.id.private8);
	    final LinearLayout pr9 = (LinearLayout) d.findViewById(R.id.private9);
		final LinearLayout pr10 = (LinearLayout) d.findViewById(R.id.private10);
		
		
		final ImageView impublic1 = (ImageView) d.findViewById(R.id.imageCheckpublic1);
	    final ImageView impr1 = (ImageView) d.findViewById(R.id.imageCheckprivate1);
	    final ImageView impr2 = (ImageView) d.findViewById(R.id.imageCheckprivate2);
	    final ImageView impr3 = (ImageView) d.findViewById(R.id.imageCheckprivate3);
		final ImageView impr4 = (ImageView) d.findViewById(R.id.imageCheckprivate4);
	    final ImageView impr5 = (ImageView) d.findViewById(R.id.imageCheckprivate5);
	    final ImageView impr6 = (ImageView) d.findViewById(R.id.imageCheckprivate6);
		final ImageView impr7 = (ImageView) d.findViewById(R.id.imageCheckprivate7);
	    final ImageView impr8 = (ImageView) d.findViewById(R.id.imageCheckprivate8);
	    final ImageView impr9 = (ImageView) d.findViewById(R.id.imageCheckprivate9);
		final ImageView impr10 = (ImageView) d.findViewById(R.id.imageCheckprivate10);
		
		Npublic1 = true;
		Npr1 = false;
		Npr2 = false;
		Npr3 = false;
		Npr4 = false;
		Npr5 = false;
		Npr6 = false;
		Npr7 = false;
		Npr8 = false;
	    Npr9 = false;
		Npr10 = false;
	    
		OnClickListener ffff = new OnClickListener()
		{

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				if(v == public1)
				{
					public1.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr1.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr2.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr3.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr4.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr5.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr6.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr7.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr8.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr9.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr10.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					
					impublic1.setBackgroundResource(R.drawable.search_check);
					impr1.setBackgroundResource(R.drawable.search_unckeck);
					impr2.setBackgroundResource(R.drawable.search_unckeck);
					impr3.setBackgroundResource(R.drawable.search_unckeck);
					impr4.setBackgroundResource(R.drawable.search_unckeck);
					impr5.setBackgroundResource(R.drawable.search_unckeck);
					impr6.setBackgroundResource(R.drawable.search_unckeck);
					impr7.setBackgroundResource(R.drawable.search_unckeck);
					impr8.setBackgroundResource(R.drawable.search_unckeck);
					impr9.setBackgroundResource(R.drawable.search_unckeck);
					impr10.setBackgroundResource(R.drawable.search_unckeck);
					
					Npublic1 = true;
					Npr1 = false;
					Npr2 = false;
					Npr3 = false;
					Npr4 = false;
					Npr5 = false;
					Npr6 = false;
					Npr7 = false;
					Npr8 = false;
				    Npr9 = false;
					Npr10 = false;
				}
				else if(v == pr1)
				{
					public1.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr1.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr2.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr3.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr4.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr5.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr6.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr7.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr8.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr9.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr10.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					
					impublic1.setBackgroundResource(R.drawable.search_check);
					impr1.setBackgroundResource(R.drawable.search_check);
					impr2.setBackgroundResource(R.drawable.search_unckeck);
					impr3.setBackgroundResource(R.drawable.search_unckeck);
					impr4.setBackgroundResource(R.drawable.search_unckeck);
					impr5.setBackgroundResource(R.drawable.search_unckeck);
					impr6.setBackgroundResource(R.drawable.search_unckeck);
					impr7.setBackgroundResource(R.drawable.search_unckeck);
					impr8.setBackgroundResource(R.drawable.search_unckeck);
					impr9.setBackgroundResource(R.drawable.search_unckeck);
					impr10.setBackgroundResource(R.drawable.search_unckeck);
					
					Npublic1 = false;
					Npr1 = true;
					Npr2 = false;
					Npr3 = false;
					Npr4 = false;
					Npr5 = false;
					Npr6 = false;
					Npr7 = false;
					Npr8 = false;
				    Npr9 = false;
					Npr10 = false;
				}
				else if(v == pr2)
				{
					public1.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr1.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr2.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr3.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr4.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr5.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr6.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr7.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr8.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr9.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr10.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					
					impublic1.setBackgroundResource(R.drawable.search_check);
					impr1.setBackgroundResource(R.drawable.search_check);
					impr2.setBackgroundResource(R.drawable.search_check);
					impr3.setBackgroundResource(R.drawable.search_unckeck);
					impr4.setBackgroundResource(R.drawable.search_unckeck);
					impr5.setBackgroundResource(R.drawable.search_unckeck);
					impr6.setBackgroundResource(R.drawable.search_unckeck);
					impr7.setBackgroundResource(R.drawable.search_unckeck);
					impr8.setBackgroundResource(R.drawable.search_unckeck);
					impr9.setBackgroundResource(R.drawable.search_unckeck);
					impr10.setBackgroundResource(R.drawable.search_unckeck);
					
					Npublic1 = false;
					Npr1 = false;
					Npr2 = true;
					Npr3 = false;
					Npr4 = false;
					Npr5 = false;
					Npr6 = false;
					Npr7 = false;
					Npr8 = false;
				    Npr9 = false;
					Npr10 = false;
				}
				else if(v == pr3)
				{
					public1.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr1.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr2.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr3.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr4.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr5.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr6.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr7.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr8.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr9.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr10.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					
					impublic1.setBackgroundResource(R.drawable.search_check);
					impr1.setBackgroundResource(R.drawable.search_check);
					impr2.setBackgroundResource(R.drawable.search_check);
					impr3.setBackgroundResource(R.drawable.search_check);
					impr4.setBackgroundResource(R.drawable.search_unckeck);
					impr5.setBackgroundResource(R.drawable.search_unckeck);
					impr6.setBackgroundResource(R.drawable.search_unckeck);
					impr7.setBackgroundResource(R.drawable.search_unckeck);
					impr8.setBackgroundResource(R.drawable.search_unckeck);
					impr9.setBackgroundResource(R.drawable.search_unckeck);
					impr10.setBackgroundResource(R.drawable.search_unckeck);
					
					Npublic1 = false;
					Npr1 = false;
					Npr2 = false;
					Npr3 = true;
					Npr4 = false;
					Npr5 = false;
					Npr6 = false;
					Npr7 = false;
					Npr8 = false;
				    Npr9 = false;
					Npr10 = false;
				}
				else if(v == pr4)
				{
					public1.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr1.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr2.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr3.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr4.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr5.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr6.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr7.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr8.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr9.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr10.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					
					impublic1.setBackgroundResource(R.drawable.search_check);
					impr1.setBackgroundResource(R.drawable.search_check);
					impr2.setBackgroundResource(R.drawable.search_check);
					impr3.setBackgroundResource(R.drawable.search_check);
					impr4.setBackgroundResource(R.drawable.search_check);
					impr5.setBackgroundResource(R.drawable.search_unckeck);
					impr6.setBackgroundResource(R.drawable.search_unckeck);
					impr7.setBackgroundResource(R.drawable.search_unckeck);
					impr8.setBackgroundResource(R.drawable.search_unckeck);
					impr9.setBackgroundResource(R.drawable.search_unckeck);
					impr10.setBackgroundResource(R.drawable.search_unckeck);
					
					Npublic1 = false;
					Npr1 = false;
					Npr2 = false;
					Npr3 = false;
					Npr4 = true;
					Npr5 = false;
					Npr6 = false;
					Npr7 = false;
					Npr8 = false;
				    Npr9 = false;
					Npr10 = false;
				}
				else if(v == pr5)
				{
					public1.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr1.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr2.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr3.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr4.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr5.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr6.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr7.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr8.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr9.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr10.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					
					impublic1.setBackgroundResource(R.drawable.search_check);
					impr1.setBackgroundResource(R.drawable.search_check);
					impr2.setBackgroundResource(R.drawable.search_check);
					impr3.setBackgroundResource(R.drawable.search_check);
					impr4.setBackgroundResource(R.drawable.search_check);
					impr5.setBackgroundResource(R.drawable.search_check);
					impr6.setBackgroundResource(R.drawable.search_unckeck);
					impr7.setBackgroundResource(R.drawable.search_unckeck);
					impr8.setBackgroundResource(R.drawable.search_unckeck);
					impr9.setBackgroundResource(R.drawable.search_unckeck);
					impr10.setBackgroundResource(R.drawable.search_unckeck);
					
					Npublic1 = false;
					Npr1 = false;
					Npr2 = false;
					Npr3 = false;
					Npr4 = false;
					Npr5 = true;
					Npr6 = false;
					Npr7 = false;
					Npr8 = false;
				    Npr9 = false;
					Npr10 = false;
				}
				else if(v == pr6)
				{
					public1.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr1.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr2.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr3.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr4.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr5.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr6.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr7.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr8.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr9.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr10.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					
					impublic1.setBackgroundResource(R.drawable.search_check);
					impr1.setBackgroundResource(R.drawable.search_check);
					impr2.setBackgroundResource(R.drawable.search_check);
					impr3.setBackgroundResource(R.drawable.search_check);
					impr4.setBackgroundResource(R.drawable.search_check);
					impr5.setBackgroundResource(R.drawable.search_check);
					impr6.setBackgroundResource(R.drawable.search_check);
					impr7.setBackgroundResource(R.drawable.search_unckeck);
					impr8.setBackgroundResource(R.drawable.search_unckeck);
					impr9.setBackgroundResource(R.drawable.search_unckeck);
					impr10.setBackgroundResource(R.drawable.search_unckeck);
					
					Npublic1 = false;
					Npr1 = false;
					Npr2 = false;
					Npr3 = false;
					Npr4 = false;
					Npr5 = false;
					Npr6 = true;
					Npr7 = false;
					Npr8 = false;
				    Npr9 = false;
					Npr10 = false;
				}
				else if(v == pr7)
				{
					public1.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr1.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr2.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr3.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr4.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr5.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr6.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr7.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr8.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr9.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr10.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					
					impublic1.setBackgroundResource(R.drawable.search_check);
					impr1.setBackgroundResource(R.drawable.search_check);
					impr2.setBackgroundResource(R.drawable.search_check);
					impr3.setBackgroundResource(R.drawable.search_check);
					impr4.setBackgroundResource(R.drawable.search_check);
					impr5.setBackgroundResource(R.drawable.search_check);
					impr6.setBackgroundResource(R.drawable.search_check);
					impr7.setBackgroundResource(R.drawable.search_check);
					impr8.setBackgroundResource(R.drawable.search_unckeck);
					impr9.setBackgroundResource(R.drawable.search_unckeck);
					impr10.setBackgroundResource(R.drawable.search_unckeck);
					
					Npublic1 = false;
					Npr1 = false;
					Npr2 = false;
					Npr3 = false;
					Npr4 = false;
					Npr5 = false;
					Npr6 = false;
					Npr7 = true;
					Npr8 = false;
				    Npr9 = false;
					Npr10 = false;
				}
				else if(v == pr8)
				{
					public1.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr1.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr2.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr3.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr4.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr5.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr6.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr7.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr8.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr9.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					pr10.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					
					impublic1.setBackgroundResource(R.drawable.search_check);
					impr1.setBackgroundResource(R.drawable.search_check);
					impr2.setBackgroundResource(R.drawable.search_check);
					impr3.setBackgroundResource(R.drawable.search_check);
					impr4.setBackgroundResource(R.drawable.search_check);
					impr5.setBackgroundResource(R.drawable.search_check);
					impr6.setBackgroundResource(R.drawable.search_check);
					impr7.setBackgroundResource(R.drawable.search_check);
					impr8.setBackgroundResource(R.drawable.search_check);
					impr9.setBackgroundResource(R.drawable.search_unckeck);
					impr10.setBackgroundResource(R.drawable.search_unckeck);
					
					Npublic1 = false;
					Npr1 = false;
					Npr2 = false;
					Npr3 = false;
					Npr4 = false;
					Npr5 = false;
					Npr6 = false;
					Npr7 = false;
					Npr8 = true;
				    Npr9 = false;
					Npr10 = false;
				}
				else if(v == pr9)
				{
					public1.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr1.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr2.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr3.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr4.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr5.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr6.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr7.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr8.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr9.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr10.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
					
					impublic1.setBackgroundResource(R.drawable.search_check);
					impr1.setBackgroundResource(R.drawable.search_check);
					impr2.setBackgroundResource(R.drawable.search_check);
					impr3.setBackgroundResource(R.drawable.search_check);
					impr4.setBackgroundResource(R.drawable.search_check);
					impr5.setBackgroundResource(R.drawable.search_check);
					impr6.setBackgroundResource(R.drawable.search_check);
					impr7.setBackgroundResource(R.drawable.search_check);
					impr8.setBackgroundResource(R.drawable.search_check);
					impr9.setBackgroundResource(R.drawable.search_check);
					impr10.setBackgroundResource(R.drawable.search_unckeck);
					
					Npublic1 = false;
					Npr1 = false;
					Npr2 = false;
					Npr3 = false;
					Npr4 = false;
					Npr5 = false;
					Npr6 = false;
					Npr7 = false;
					Npr8 = false;
				    Npr9 = true;
					Npr10 = false;
				}
				else if(v == pr10)
				{
					public1.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr1.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr2.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr3.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr4.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr5.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr6.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr7.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr8.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr9.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					pr10.setBackgroundResource(R.drawable.rectangle_search_white_corners);
					
					impublic1.setBackgroundResource(R.drawable.search_check);
					impr1.setBackgroundResource(R.drawable.search_check);
					impr2.setBackgroundResource(R.drawable.search_check);
					impr3.setBackgroundResource(R.drawable.search_check);
					impr4.setBackgroundResource(R.drawable.search_check);
					impr5.setBackgroundResource(R.drawable.search_check);
					impr6.setBackgroundResource(R.drawable.search_check);
					impr7.setBackgroundResource(R.drawable.search_check);
					impr8.setBackgroundResource(R.drawable.search_check);
					impr9.setBackgroundResource(R.drawable.search_check);
					impr10.setBackgroundResource(R.drawable.search_check);
					
					Npublic1 = false;
					Npr1 = false;
					Npr2 = false;
					Npr3 = false;
					Npr4 = false;
					Npr5 = false;
					Npr6 = false;
					Npr7 = false;
					Npr8 = false;
				    Npr9 = false;
					Npr10 = true;
				}
				
			}
			
		};
		
		public1.setOnClickListener(ffff);
		pr1.setOnClickListener(ffff);
		pr2.setOnClickListener(ffff);
		pr3.setOnClickListener(ffff);
		pr4.setOnClickListener(ffff);
		pr5.setOnClickListener(ffff);
		pr6.setOnClickListener(ffff);
		pr7.setOnClickListener(ffff);
		pr8.setOnClickListener(ffff);
		pr9.setOnClickListener(ffff);
		pr10.setOnClickListener(ffff);
		
		
		
		cancelButton.setOnClickListener(new OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				d.cancel();
			}
		});
		
		okButton.setOnClickListener(new OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
                levelint = Integer.parseInt(minlevel);
				//levelint = ((Number)NumberFormat.getInstance().parse(y)).intValue();
				
				if (Npr10) 
				{
					if (levelint>10) 
					{
						Toast.makeText(_activity, _activity.getResources().getString(R.string.dialoglevel), Toast.LENGTH_LONG).show();		
					}
					else if (levelint ==0) 
					{
						Toast.makeText(_activity, _activity.getResources().getString(R.string.dialoglevel), Toast.LENGTH_LONG).show();	
					}
					else 
					{
						Profile_textAccess.setText(R.string.p10);
						levelint=10;
						new levelTask().execute(Userid);
					}
				} 
				else if (Npr1) 
				{
					if (levelint>1) 
					{
						Toast.makeText(_activity, _activity.getResources().getString(R.string.dialoglevel), Toast.LENGTH_LONG).show();		 
					}
					else if (levelint ==0) 
					{
						Toast.makeText(_activity, _activity.getResources().getString(R.string.dialoglevel), Toast.LENGTH_LONG).show();	
				    }
					else 
					{
						Profile_textAccess.setText(R.string.p1);
						levelint=1;
						new levelTask().execute(Userid);
				   }
				} 
				else if (Npr2) 
				{
					if (levelint>2) 
					{
						Toast.makeText(_activity, _activity.getResources().getString(R.string.dialoglevel), Toast.LENGTH_LONG).show();		
					}
					else if (levelint ==0) 
					{
				    	Toast.makeText(_activity, _activity.getResources().getString(R.string.dialoglevel), Toast.LENGTH_LONG).show();	
				    }
					else 
					{
						Profile_textAccess.setText(R.string.p2);
						levelint=2;
						new levelTask().execute(Userid);
					}
				} 
				else if (Npr3) 
				{
					if (levelint>3) 
					{
						Toast.makeText(_activity, _activity.getResources().getString(R.string.dialoglevel), Toast.LENGTH_LONG).show();		
					}  
					else if (levelint ==0) 
					{
						Toast.makeText(_activity, _activity.getResources().getString(R.string.dialoglevel), Toast.LENGTH_LONG).show();	
					}
					else 
					{
						Profile_textAccess.setText(R.string.p3);
						levelint=3;
						new levelTask().execute(Userid);
					}
				} 
				else if (Npr4) 
				{
					if (levelint>4) 
					{
						Toast.makeText(_activity, _activity.getResources().getString(R.string.dialoglevel), Toast.LENGTH_LONG).show();		
					}  
					else if (levelint ==0) 
					{
						Toast.makeText(_activity, _activity.getResources().getString(R.string.dialoglevel), Toast.LENGTH_LONG).show();	
					}
					else 
					{
						Profile_textAccess.setText(R.string.p4);
						levelint=4;
						new levelTask().execute(Userid);
					}
				} 
				else if (Npr5) 
				{
					if (levelint>5) 
					{
						Toast.makeText(_activity, _activity.getResources().getString(R.string.dialoglevel), Toast.LENGTH_LONG).show();		
					}  
					else if (levelint ==0) 
					{
						Toast.makeText(_activity, _activity.getResources().getString(R.string.dialoglevel), Toast.LENGTH_LONG).show();	
					}
					else 
					{
						Profile_textAccess.setText(R.string.p5);
						levelint=5;
						new levelTask().execute(Userid);
					} 
				} 
				else if (Npr6) 
				{
					if (levelint>6)
					{
						Toast.makeText(_activity, _activity.getResources().getString(R.string.dialoglevel), Toast.LENGTH_LONG).show();		 
					}  
					else if (levelint ==0) 
					{
						Toast.makeText(_activity, _activity.getResources().getString(R.string.dialoglevel), Toast.LENGTH_LONG).show();	
					}
					else
					{
						Profile_textAccess.setText(R.string.p6);
						levelint=6;
						new levelTask().execute(Userid);
					}
				} 
				else if (Npr7)
				{
					if (levelint>7) 
					{
						Toast.makeText(_activity, _activity.getResources().getString(R.string.dialoglevel), Toast.LENGTH_LONG).show();		
					} 	
					else if (levelint ==0) 
					{
				  	 Toast.makeText(_activity, _activity.getResources().getString(R.string.dialoglevel), Toast.LENGTH_LONG).show();	
					}
					else
					{
						Profile_textAccess.setText(R.string.p7);
						levelint=7;
						new levelTask().execute(Userid);
				   }
				} 
				else if (Npr8) 
				{
					if (levelint>8) 
					{
						Toast.makeText(_activity, _activity.getResources().getString(R.string.dialoglevel), Toast.LENGTH_LONG).show();		
					}  
					else if (levelint ==0) 
					{
						Toast.makeText(_activity, _activity.getResources().getString(R.string.dialoglevel), Toast.LENGTH_LONG).show();	
					}
					else 
					{
						Profile_textAccess.setText(R.string.p8);
						levelint=8;
						new levelTask().execute(Userid);
					}
				} 
				else if (Npr9) 
				{
					if (levelint>9 ) 
					{
						Toast.makeText(_activity, _activity.getResources().getString(R.string.dialoglevel), Toast.LENGTH_LONG).show();		  
					}  
					else if (levelint ==0) 
					{
						Toast.makeText(_activity, _activity.getResources().getString(R.string.dialoglevel), Toast.LENGTH_LONG).show();	
					}
					else 
					{
						Profile_textAccess.setText(R.string.p9);
						levelint=9;
						new levelTask().execute(Userid);
					}
				} 
				else if (Npublic1)
				{
					levelint = 0;
					Profile_textAccess.setText(R.string.p0);
					new levelTask().execute(Userid);
				}
				d.cancel();
			}
		});
		d.show();
	}
	
	class levelTask extends AsyncTask<String,Void,String>
	{
		@Override
		protected void onPreExecute()
		{
			fragLoading.Show();
		}
		@Override
		protected String doInBackground(String... params) 
		{
			String result = ServiceClient.level(Utils.getSharedPreferences(_activity).getString("userid", ""), params[0], levelint);
			return result;
		}	
		@Override
		protected void onPostExecute(String res)
		{
			fragLoading.Close();
		}

	}
	
	
	
}











	
		 
	 
	
	
	
	



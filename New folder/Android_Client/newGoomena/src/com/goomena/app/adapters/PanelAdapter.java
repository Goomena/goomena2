package com.goomena.app.adapters;

import java.util.ArrayList;

import com.goomena.app.Controller;
import com.goomena.app.Panel;
import com.goomena.app.R;
import com.goomena.app.data.ServiceClient;
import com.goomena.app.models.MessageInfo;
import com.goomena.app.models.MessageInfoList;
import com.goomena.app.models.Offer;
import com.goomena.app.models.OfferList;
import com.goomena.app.models.UserProfile;
import com.goomena.app.tools.Log;
import com.goomena.app.tools.Utils;
import com.goomena.fragments.FragMessageView;
import com.goomena.fragments.FragOffers;
import com.goomena.fragments.FragProfile;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

@SuppressLint("NewApi")
public class PanelAdapter extends PagerAdapter
{
	private Activity _activity;
	private LayoutInflater inflater;
	//Handler _handlerPanel;
	RelativeLayout credits_layout_relativeLayoutGetMore,credits_layout_relativeLayoutGetIt;
	TextView credits_layout_textCredits;
	ListView panelList;
	ItemPanelAdapter adapterPanelLike,adapterPanelOffer,adapterPanelMessage;
	OfferList likeList;
	MessageInfoList messageInfosList;
	OfferList offerList;
	String userid,genderid,zip,lat,lng;
	
	TextView panel_adaptes_textTitle,panel_adaptes_textCaseCount;
	
	String rejectReason;
	String currentOfferid;
	
	Panel _panel;
	private final static String TAG_FRAGMENT = "TAG_FRAGMENT";
	
	Handler handler_likes,handler_offers;
	
	public PanelAdapter(Activity activity,Panel panel)
	{
		this._activity = activity;
		this._panel = panel;
		SharedPreferences settings = Utils.getSharedPreferences(activity);
		userid = settings.getString("userid", "");
		genderid = settings.getString("genderid", "");
		zip = settings.getString("zipcode", "");
		lat = settings.getString("lat", "");
		lng = settings.getString("lng", "");
		
		likeList = new OfferList();
		likeList.offers = new ArrayList<Offer>();
		//
		offerList = new OfferList();
		offerList.offers = new ArrayList<Offer>();
		
		messageInfosList = new MessageInfoList();
		messageInfosList.msgList = new ArrayList<MessageInfo>();
		
		adapterPanelLike = new ItemPanelAdapter(_activity,likeList,messageInfosList,0);
		//adapterPanelMessage = new ItemPanelAdapter(_activity,likeList,messageInfosList,1);
		adapterPanelOffer = new ItemPanelAdapter(_activity,offerList,messageInfosList,2);
		
		
		createRejectDialog();
		
		new getLikesTask().execute();
		new getOffersTask().execute();
		//new getNewMessagesTask().execute();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		if(genderid.contains("1"))
		{
			return 3;
		}
		else
		{
			return 2;
		}
	}

	
	@Override
	public boolean isViewFromObject(View view, Object object) {
		// TODO Auto-generated method stub
		return view == ((LinearLayout) object);
	}
	
	
	public void refresh()
	{
		new getLikesTask().execute();
		new getOffersTask().execute();
		//new getNewMessagesTask().execute();
	}
	
	@Override
	public void setPrimaryItem(ViewGroup container, int position, Object object) 
	{}
	
	public Object instantiateItem(ViewGroup container, int position) 
	{
		inflater = (LayoutInflater) _activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View viewLayout = inflater.inflate(R.layout.panel_adapter, container, false);
		
		final RelativeLayout include_view_no_result = (RelativeLayout) viewLayout.findViewById(R.id.include_view_no_result);
		final TextView view_noresult_textTitle = (TextView) viewLayout.findViewById(R.id.view_noresult_textTitle);
		
		panel_adaptes_textTitle = (TextView) viewLayout.findViewById(R.id.panel_adaptes_textTitle);
		//panel_adaptes_textCaseCount = (TextView) viewLayout.findViewById(R.id.panel_adaptes_textCaseCount);
		
		panelList = (ListView) viewLayout.findViewById(R.id.panelList);
		
		
		
		
		if(position==0)
		{
			//new getLikesTask().execute();
			panel_adaptes_textTitle.setText("Likes");
			//new getNumberLikesTask(panel_adaptes_textCaseCount).execute();
			panelList.setAdapter(adapterPanelLike);
			panelList.setOnItemClickListener(itemCLickLike);
			//likeGridView.setOnScrollListener(myOnScrollListener_new);
			//likeGridView.setOnItemClickListener(myOnItemClickListener_new);
			
			if(likeList.offers.size()==0)
			{
				view_noresult_textTitle.setText(_activity.getResources().getString(R.string.noresult_title_you_have_no_likes));
				include_view_no_result.setVisibility(RelativeLayout.VISIBLE);
			}
			handler_likes = new Handler()
			{
				public void handleMessage(Message msg) 
				{	
					if(msg.obj.toString().equals("0"))
					{
						view_noresult_textTitle.setText(_activity.getResources().getString(R.string.noresult_title_you_have_no_likes));
						include_view_no_result.setVisibility(RelativeLayout.VISIBLE);
					}
					else
					{
						include_view_no_result.setVisibility(RelativeLayout.GONE);
					}
				}
			};
		}
		/*
		else if(position==1)
		{
			panel_adaptes_textTitle.setText(_activity.getResources().getString(R.string.messages));
			new getNumberMessagesTask(panel_adaptes_textCaseCount).execute();
			panelList.setAdapter(adapterPanelMessage);
			panelList.setOnItemClickListener(itemCLickMessage);
		}
		*/
		else if(position==1)
		{
			panel_adaptes_textTitle.setText(_activity.getResources().getString(R.string.bids));
			//new getNumberOffersTask(panel_adaptes_textCaseCount).execute();
			panelList.setAdapter(adapterPanelOffer);
			panelList.setOnItemClickListener(itemCLickOffer);
			//likeGridView.setOnScrollListener(myOnScrollListener_new);
			//likeGridView.setOnItemClickListener(myOnItemClickListener_new);
			
			if(offerList.offers.size()==0)
			{
				view_noresult_textTitle.setText(_activity.getResources().getString(R.string.noresult_title_you_currently_have_no_offers));
				include_view_no_result.setVisibility(RelativeLayout.VISIBLE);
			}
			handler_offers = new Handler()
			{
				public void handleMessage(Message msg) 
				{	
					if(msg.obj.toString().equals("0"))
					{
						view_noresult_textTitle.setText(_activity.getResources().getString(R.string.noresult_title_you_currently_have_no_offers));
						include_view_no_result.setVisibility(RelativeLayout.VISIBLE);
					}
					else
					{
						include_view_no_result.setVisibility(RelativeLayout.GONE);
					}
				}
			};
		}
		/*
		else if(position==3)
		{
			panel_adaptes_textTitle.setText("Dates");
		}
		*/
		else if(position==2)
		{
			View viewLayoutCredits = inflater.inflate(R.layout.credits_layout, container, false);
			credits_layout_relativeLayoutGetMore = (RelativeLayout) viewLayoutCredits.findViewById(R.id.credits_layout_relativeLayoutGetMore); 
			credits_layout_relativeLayoutGetIt = (RelativeLayout) viewLayoutCredits.findViewById(R.id.credits_layout_relativeLayoutGetIt);
			credits_layout_textCredits = (TextView) viewLayoutCredits.findViewById(R.id.credits_layout_textCredits);
			
			new getCredits(credits_layout_textCredits).execute();
			
			credits_layout_relativeLayoutGetMore.setOnClickListener(clickListener);
			credits_layout_relativeLayoutGetIt.setOnClickListener(clickListener);
			
			((ViewPager) container).addView(viewLayoutCredits);
			return viewLayoutCredits;
		}
		
		
		((ViewPager) container).addView(viewLayout);
		 
        return viewLayout;
	}
	@Override
    public void destroyItem(ViewGroup container, int position, Object object) 
	{
        ((ViewPager) container).removeView((LinearLayout) object);
    }

	
	
	
	OnItemClickListener itemCLickLike = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, final int index, long arg3) {
			_panel.Close();
			
			/*
			Fragment fr = new FragLikes();				
			FragmentManager fm = _activity.getFragmentManager();
			
	        FragmentTransaction fragmentTransaction = fm.beginTransaction();
	        fragmentTransaction.setCustomAnimations(R.anim.left_slidein_list_animator, R.anim.left_slideout_list_animator,R.anim.right_slidein_list_animator, R.anim.right_slideout_list_animator);
	        fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
	        fragmentTransaction.addToBackStack(null);
	        fragmentTransaction.commit();
	        */
			
			final Dialog d = new Dialog(_activity, R.style.DialogSlideAnim);
			d.setContentView(R.layout.dialog_like);
			
			TextView dialog_textTitle = (TextView) d.findViewById(R.id.dialog_textTitle);
			ImageView dialog_imageProfile = (ImageView) d.findViewById(R.id.dialog_imageProfile);
			TextView dialog_textUsername = (TextView) d.findViewById(R.id.dialog_textUsername);
			TextView dialog_textAge = (TextView) d.findViewById(R.id.dialog_textAge);
			TextView dialog_textLocation = (TextView) d.findViewById(R.id.dialog_textLocation);
			RelativeLayout dialog_relativeLayoutSendMessage = (RelativeLayout) d.findViewById(R.id.dialog_relativeLayoutSendMessage);
			RelativeLayout dialog_relativeLayoutBid = (RelativeLayout) d.findViewById(R.id.dialog_relativeLayoutBid);
			RelativeLayout dialog_relativeLayoutPoke = (RelativeLayout) d.findViewById(R.id.dialog_relativeLayoutPoke);
			RelativeLayout dialog_relativeLayoutReject = (RelativeLayout) d.findViewById(R.id.dialog_relativeLayoutReject);
			TextView dialog_textReject = (TextView) d.findViewById(R.id.dialog_textReject);
			RelativeLayout dialog_relativeLayoutProfile = (RelativeLayout) d.findViewById(R.id.dialog_relativeLayoutProfile);
			
			dialog_textTitle.setText(_activity.getResources().getString(R.string.you_received_a_like_from));
			
			Utils.getImageLoader(_activity).displayImage(likeList.offers.get(index).profile.ImageUrl.replace("d150", "thumbs"), dialog_imageProfile, Utils.getImageOptions()); 	    
			dialog_textUsername.setText(likeList.offers.get(index).profile.UserName);
			dialog_textAge.setText(_activity.getResources().getString(R.string.age)+" "+likeList.offers.get(index).profile.UserAge+" "+_activity.getResources().getString(R.string.years));
			dialog_textLocation.setText(likeList.offers.get(index).profile.UserCity+", "+likeList.offers.get(index).profile.UserRegion);
			dialog_textReject.setText(_activity.getResources().getString(R.string.reject_the_like));
			
			
			if(likeList.offers.get(index).profile.GenderId.equals("1"))
			{
				dialog_relativeLayoutPoke.setVisibility(View.VISIBLE);
			}
			
			dialog_imageProfile.setOnClickListener(new OnClickListener() {
				@SuppressLint("NewApi") 
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					ArrayList<UserProfile> _userProfilesFull_ = new ArrayList<UserProfile>();
					_userProfilesFull_.add(likeList.offers.get(index).profile);
					
					Fragment fr = new FragProfile(2,_userProfilesFull_,0);				
					FragmentManager fm = _activity.getFragmentManager();
				    FragmentTransaction fragmentTransaction = fm.beginTransaction();
				    fragmentTransaction.setCustomAnimations(R.anim.left_slidein_list_animator, R.anim.left_slideout_list_animator,R.anim.right_slidein_list_animator, R.anim.right_slideout_list_animator);
				    fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
				    fragmentTransaction.addToBackStack(null);
				    fragmentTransaction.commit();
					d.cancel();
				}
			});
			
			dialog_relativeLayoutSendMessage.setOnClickListener(new OnClickListener() {
				
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Fragment fr;
					fr = new FragMessageView(null,likeList.offers.get(index).profile,Integer.parseInt(likeList.offers.get(index).offerId),null);
					FragmentManager fm = _activity.getFragmentManager();
					
			        FragmentTransaction fragmentTransaction = fm.beginTransaction();
			        fragmentTransaction.setCustomAnimations(R.anim.left_slidein_list_animator, R.anim.left_slideout_list_animator,R.anim.right_slidein_list_animator, R.anim.right_slideout_list_animator);
			        fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
			        fragmentTransaction.addToBackStack(null);
			        fragmentTransaction.commit();
			        d.cancel();
				}
			});
			
			dialog_relativeLayoutBid.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					createOfferDialog(likeList.offers.get(index).profile);
					offerDialog.show();
					d.cancel();
				}
			});
			
			
			dialog_relativeLayoutPoke.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					currentOfferid = likeList.offers.get(index).offerId;
					new sendPokeTask(likeList.offers.get(index).profile.ProfileId,currentOfferid).execute();
					d.cancel();
				}
			});
			
			dialog_relativeLayoutProfile.setOnClickListener(new OnClickListener() {
				@SuppressLint("NewApi") 
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					ArrayList<UserProfile> _userProfilesFull_ = new ArrayList<UserProfile>();
					_userProfilesFull_.add(likeList.offers.get(index).profile);
					
					Fragment fr = new FragProfile(2,_userProfilesFull_,0);			
					FragmentManager fm = _activity.getFragmentManager();
				    FragmentTransaction fragmentTransaction = fm.beginTransaction();
				    fragmentTransaction.setCustomAnimations(R.anim.left_slidein_list_animator, R.anim.left_slideout_list_animator,R.anim.right_slidein_list_animator, R.anim.right_slideout_list_animator);
				    fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
				    fragmentTransaction.addToBackStack(null);
				    fragmentTransaction.commit();
					d.cancel();
				}
			});
			
			dialog_relativeLayoutReject.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					rejectDialog.show();
					currentOfferid = likeList.offers.get(index).offerId;
					d.cancel();
				}
			});
			d.show();
		}
	};
	
	OnItemClickListener itemCLickMessage = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, final int index, long arg3) {
			_panel.Close();
			
			
			
			
			Fragment fr;
			fr = new FragMessageView(messageInfosList.msgList.get(index),null,0,null);
			FragmentManager fm = _activity.getFragmentManager();
			
	        FragmentTransaction fragmentTransaction = fm.beginTransaction();
	        fragmentTransaction.setCustomAnimations(R.anim.left_slidein_list_animator, R.anim.left_slideout_list_animator,R.anim.right_slidein_list_animator, R.anim.right_slideout_list_animator);
	        fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
	        fragmentTransaction.addToBackStack(null);
	        fragmentTransaction.commit();
	        
	        
		}
	};
	
	OnItemClickListener itemCLickOffer = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, final int index, long arg3) {
			_panel.Close();
			
			
			final Dialog d = new Dialog(_activity, R.style.DialogSlideAnim);
			d.setContentView(R.layout.dialog_like);
			
			TextView dialog_textTitle = (TextView) d.findViewById(R.id.dialog_textTitle);
			ImageView dialog_imageProfile = (ImageView) d.findViewById(R.id.dialog_imageProfile);
			TextView dialog_textUsername = (TextView) d.findViewById(R.id.dialog_textUsername);
			TextView dialog_textAge = (TextView) d.findViewById(R.id.dialog_textAge);
			TextView dialog_textLocation = (TextView) d.findViewById(R.id.dialog_textLocation);
			RelativeLayout dialog_relativeLayoutSendMessage = (RelativeLayout) d.findViewById(R.id.dialog_relativeLayoutSendMessage);
			RelativeLayout dialog_relativeLayoutBid = (RelativeLayout) d.findViewById(R.id.dialog_relativeLayoutBid);
			TextView dialog_textLayoutBid = (TextView) d.findViewById(R.id.dialog_textLayoutBid);
			RelativeLayout dialog_relativeLayoutReject = (RelativeLayout) d.findViewById(R.id.dialog_relativeLayoutReject);
			TextView dialog_textReject = (TextView) d.findViewById(R.id.dialog_textReject);
			RelativeLayout dialog_relativeLayoutProfile = (RelativeLayout) d.findViewById(R.id.dialog_relativeLayoutProfile);
			
			dialog_textTitle.setText(_activity.getResources().getString(R.string.you_received_a_offer_from));
			
			Utils.getImageLoader(_activity).displayImage(offerList.offers.get(index).profile.ImageUrl.replace("d150", "thumbs"), dialog_imageProfile, Utils.getImageOptions()); 	    
			dialog_textUsername.setText(offerList.offers.get(index).profile.UserName);
			dialog_textAge.setText(_activity.getResources().getString(R.string.age)+" "+offerList.offers.get(index).profile.UserAge+" "+_activity.getResources().getString(R.string.years));
			dialog_textLocation.setText(offerList.offers.get(index).profile.UserCity+", "+offerList.offers.get(index).profile.UserRegion);
			dialog_textReject.setText(_activity.getResources().getString(R.string.reject_the_offer));
			dialog_textLayoutBid.setText(_activity.getResources().getString(R.string.accept));
			
			
			dialog_relativeLayoutSendMessage.setOnClickListener(new OnClickListener() {
				
				@SuppressLint("NewApi") 
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Fragment fr;
					fr = new FragMessageView(null,offerList.offers.get(index).profile,Integer.parseInt(offerList.offers.get(index).offerId),null);
					FragmentManager fm = _activity.getFragmentManager();
					
			        FragmentTransaction fragmentTransaction = fm.beginTransaction();
			        fragmentTransaction.setCustomAnimations(R.anim.left_slidein_list_animator, R.anim.left_slideout_list_animator,R.anim.right_slidein_list_animator, R.anim.right_slideout_list_animator);
			        fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
			        fragmentTransaction.addToBackStack(null);
			        fragmentTransaction.commit();
			        d.cancel();
				}
			});
			
			dialog_relativeLayoutBid.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					currentOfferid =offerList.offers.get(index).offerId;
					new acceptOfferTask().execute(currentOfferid);
					d.cancel();
				}
			});
			
			
			dialog_imageProfile.setOnClickListener(new OnClickListener() {
				@SuppressLint("NewApi") 
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					ArrayList<UserProfile> _userProfilesFull_ = new ArrayList<UserProfile>();
					_userProfilesFull_.add(offerList.offers.get(index).profile);
					
					Fragment fr = new FragProfile(2,_userProfilesFull_,0);				
					FragmentManager fm = _activity.getFragmentManager();
				    FragmentTransaction fragmentTransaction = fm.beginTransaction();
				    fragmentTransaction.setCustomAnimations(R.anim.left_slidein_list_animator, R.anim.left_slideout_list_animator,R.anim.right_slidein_list_animator, R.anim.right_slideout_list_animator);
				    fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
				    fragmentTransaction.addToBackStack(null);
				    fragmentTransaction.commit();
					d.cancel();
				}
			});
			
			//dialog_relativeLayoutBid.setVisibility(View.GONE);
			
			dialog_relativeLayoutProfile.setOnClickListener(new OnClickListener() {
				@SuppressLint("NewApi") 
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					ArrayList<UserProfile> _userProfilesFull_ = new ArrayList<UserProfile>();
					_userProfilesFull_.add(offerList.offers.get(index).profile);
					
					Fragment fr = new FragProfile(2,_userProfilesFull_,0);				
					FragmentManager fm = _activity.getFragmentManager();
				    FragmentTransaction fragmentTransaction = fm.beginTransaction();
				    fragmentTransaction.setCustomAnimations(R.anim.left_slidein_list_animator, R.anim.left_slideout_list_animator,R.anim.right_slidein_list_animator, R.anim.right_slideout_list_animator);
				    fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
				    fragmentTransaction.addToBackStack(null);
				    fragmentTransaction.commit();
					d.cancel();
				}
			});
			
			dialog_relativeLayoutReject.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					rejectDialog.show();
					currentOfferid = offerList.offers.get(index).offerId;
					d.cancel();
				}
			});
			d.show();
			/*
			Fragment fr = new FragOffers();				
			FragmentManager fm = _activity.getFragmentManager();
			
	        FragmentTransaction fragmentTransaction = fm.beginTransaction();
	        fragmentTransaction.setCustomAnimations(R.anim.left_slidein_list_animator, R.anim.left_slideout_list_animator,R.anim.right_slidein_list_animator, R.anim.right_slideout_list_animator);
	        fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
	        fragmentTransaction.addToBackStack(null);
	        fragmentTransaction.commit();
	        */
		}
	};
	
	
	
	
	class getLikesTask extends AsyncTask<Void,Void,OfferList>
	{
		@Override
		protected OfferList doInBackground(Void... params) 
		{
			//mode: 0 for new
			likeList = ServiceClient.getLikes(String.valueOf(0), userid, zip, lat, lng,"0");
			return likeList;
		}
		
		@Override
		protected void onPostExecute(OfferList result)
		{
			if(result!=null &&  result.offers!=null)
			{
				adapterPanelLike.setItems(result,null);
				//panelList.setAdapter(adapterPanel);
				
				if(handler_likes!=null)
				{
					if(result.offers.size()==0)
					{
						Message msg = handler_likes.obtainMessage();
						msg.obj = "0";
						handler_likes.sendMessage(msg);
					}
					else
					{
						Message msg = handler_likes.obtainMessage();
						msg.obj = "1";
						handler_likes.sendMessage(msg);
					}
				}
			}			
		}
	}
	
	
	
	
	class getNumberLikesTask extends AsyncTask<Void,Void,String>
	{
		TextView _textCaseCount;
		public getNumberLikesTask(TextView textCaseCount)
		{
			this._textCaseCount = textCaseCount;
		}
		@Override
		protected String doInBackground(Void... params) 
		{
			//mode: 0 for new
			String NumberLikes;
			NumberLikes = ServiceClient.getNumberLikes(userid);
			return NumberLikes;
		}
		
		@Override
		protected void onPostExecute(String Number)
		{
			if(Number!=null)
			{
				_textCaseCount.setText(Number);
			}			
		}
	}
	
	
	
	
	class getNewMessagesTask extends AsyncTask<Void,Void,MessageInfoList>
	{
		@Override
		protected MessageInfoList doInBackground(Void... params) 
		{
			//mode: 0 for new
			messageInfosList = ServiceClient.getNewMessages(userid);
			return messageInfosList;
		}
		
		@Override
		protected void onPostExecute(MessageInfoList result)
		{
			if(result!=null &&  result.msgList!=null)
			{
				adapterPanelMessage.setItems(null,result);
				//panel_adaptes_textCaseCount.setText(""+offerList.offers.size());
				//panelList.setAdapter(adapterPanel);
			}			
		}
	}
	
	class getNumberMessagesTask extends AsyncTask<Void,Void,String>
	{
		TextView _textCaseCount;
		public getNumberMessagesTask(TextView textCaseCount)
		{
			this._textCaseCount = textCaseCount;
		}
		@Override
		protected String doInBackground(Void... params) 
		{
			//mode: 0 for new
			String NumberMessages;
			NumberMessages = ServiceClient.getNumberMessages(userid);
			return NumberMessages;
		}
		
		@Override
		protected void onPostExecute(String Number)
		{
			if(Number!=null)
			{
				_textCaseCount.setText(Number);
			}			
		}
	}
	
	
	
	class getOffersTask extends AsyncTask<Void,Void,OfferList>
	{
		@Override
		protected OfferList doInBackground(Void... params) 
		{
			//mode: 0 for new
			offerList = ServiceClient.getOffers(String.valueOf(0), userid, zip, lat, lng,"0");
			return offerList;
		}
		
		@Override
		protected void onPostExecute(OfferList result)
		{
			if(result!=null &&  result.offers!=null)
			{
				adapterPanelOffer.setItems(result,null);
				//panel_adaptes_textCaseCount.setText(""+offerList.offers.size());
				//panelList.setAdapter(adapterPanel);
				
				if(handler_offers!=null)
				{
					if(result.offers.size()==0)
					{
						Message msg = handler_offers.obtainMessage();
						msg.obj = "0";
						handler_offers.sendMessage(msg);
					}
					else
					{
						Message msg = handler_offers.obtainMessage();
						msg.obj = "1";
						handler_offers.sendMessage(msg);
					}
				}
			}			
		}
	}
	
	class getNumberOffersTask extends AsyncTask<Void,Void,String>
	{
		TextView _textCaseCount;
		public getNumberOffersTask(TextView textCaseCount)
		{
			this._textCaseCount = textCaseCount;
		}
		@Override
		protected String doInBackground(Void... params) 
		{
			//mode: 0 for new
			String NumberOffers;
			NumberOffers = Integer.toString(offerList.offers.size());
			//NumberLikes = ServiceClient.getNumberLikes(userid);
			return NumberOffers;
		}
		
		@Override
		protected void onPostExecute(String Number)
		{
			if(Number.length()!=0)
			{
				_textCaseCount.setText(Number);
			}			
		}
	}
	
	
	class getCredits extends AsyncTask<Void,Void,String>
	{
		TextView _textCredits;
		public getCredits(TextView textCredits)
		{
			this._textCredits = textCredits;
		}
		@Override
		protected String doInBackground(Void... params) 
		{
			String Credits;
			Credits = ServiceClient.getCredits(userid);
			return Credits;
		}
		
		@Override
		protected void onPostExecute(String NCredits)
		{
			if(NCredits!=null)
			{
				_textCredits.setText(NCredits+" Credits");
			}			
		}
	}
	
	
	
	OnClickListener clickListener = new OnClickListener() 
	{
		@Override
		public void onClick(View v) 
		{
			if(v == credits_layout_relativeLayoutGetMore)
			{
				new getHashTask().execute();
			}
			else if(v ==credits_layout_relativeLayoutGetIt)
			{
				new getHashTask().execute();
			}
		}
	};
	
	
	class getHashTask extends AsyncTask<Void,Void,String>{
		@Override
		protected String doInBackground(Void... params) {
			String hash;
			hash = ServiceClient.getHashTask(Utils.getSharedPreferences(_activity).getString("username",""));
			return hash;
		}
		@Override
		protected void onPostExecute(String p){
			p = p.replace("\"", "");
			String uri = "http://www.goomena.com/Login.aspx?hash="+p+"&login="+Utils.getSharedPreferences(_activity).getString("username","")+"&ReturnUrl=%2fMembers%2fSelectProduct.aspx";
			Log.v("APP", uri);
			Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
			_activity.startActivity(browserIntent);
		}  
	}
	
	
	
	
	
	Dialog rejectDialog;
	
	private void createRejectDialog(){
		rejectDialog = new Dialog(_activity, R.style.DialogSlideAnim);
		rejectDialog.setContentView(R.layout.menu_reject);
		rejectDialog.setTitle(R.string.reject);
		
		final LinearLayout[] texts = new LinearLayout[4];
		texts[0] = (LinearLayout) rejectDialog.findViewById(R.id.txtReject1);
		texts[0].setTag("7");
		texts[1] = (LinearLayout) rejectDialog.findViewById(R.id.txtReject2);
		texts[1].setTag("6");
		texts[2] = (LinearLayout) rejectDialog.findViewById(R.id.txtReject3);
		texts[2].setTag("5");
		texts[3] = (LinearLayout) rejectDialog.findViewById(R.id.txtReject4);
		texts[3].setTag("8");
		
		final ImageView[] imagevs = new ImageView[4];
		imagevs[0] = (ImageView) rejectDialog.findViewById(R.id.imagetxtReject1);
		imagevs[1] = (ImageView) rejectDialog.findViewById(R.id.imagetxtReject2);
		imagevs[2] = (ImageView) rejectDialog.findViewById(R.id.imagetxtReject3);
		imagevs[3] = (ImageView) rejectDialog.findViewById(R.id.imagetxtReject4);
		
		OnClickListener clck = new OnClickListener() {
			@Override
			public void onClick(View v) {
				for(LinearLayout t:texts){
					t.setBackgroundResource(R.drawable.rectangle_search_white_lines_corners);
				}
				for(ImageView t:imagevs){
					t.setBackgroundResource(R.drawable.search_unckeck);
				}
				v.setBackgroundResource(R.drawable.rectangle_search_white_corners);
				
				if(v.getTag().toString().equals("7"))
					imagevs[0].setBackgroundResource(R.drawable.search_check);
				if(v.getTag().toString().equals("6"))
					imagevs[1].setBackgroundResource(R.drawable.search_check);
				if(v.getTag().toString().equals("5"))
					imagevs[2].setBackgroundResource(R.drawable.search_check);
				if(v.getTag().toString().equals("8"))
					imagevs[3].setBackgroundResource(R.drawable.search_check);
				
				rejectReason = (String) v.getTag();
			}
		};
		
		for(LinearLayout t:texts){			
			t.setOnClickListener(clck);
		}
		
		TextView cancel = (TextView)rejectDialog.findViewById(R.id.cancel);
		TextView ok = (TextView)rejectDialog.findViewById(R.id.ok);

		cancel.setOnClickListener(new OnClickListener() {	
			@Override
			public void onClick(View v) {
				rejectDialog.dismiss();
			}
		});
		
		ok.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				rejectDialog.dismiss();
				new rejectLikeTask().execute(currentOfferid);				
			}
		});
	}
	
	
	
	Dialog offerDialog;
	int offerAmount = 35;
	private void createOfferDialog(final UserProfile user)
	{
		offerAmount = 35;
		offerDialog = new Dialog(_activity, R.style.DialogSlideAnim);
		offerDialog.setContentView(R.layout.dialog_offer);
		
		final Animation fade_diallog_offerUp = AnimationUtils.loadAnimation(_activity, R.anim.fade_dialog_offer);
		final Animation fade_diallog_offerDown = AnimationUtils.loadAnimation(_activity, R.anim.fade_dialog_offer);
		final Animation fade_in = AnimationUtils.loadAnimation(_activity, R.anim.fade_in_dialog_offer);
		final Animation fade_out = AnimationUtils.loadAnimation(_activity, R.anim.fade_out_dialog_offer);
		
		TextView dialog_offer_textUsername = (TextView) offerDialog.findViewById(R.id.dialog_offer_textUsername);
		ImageView dialog_offer_imageProfile = (ImageView) offerDialog.findViewById(R.id.dialog_offer_imageProfile);
		final TextView dialog_offer_textPrice1 = (TextView) offerDialog.findViewById(R.id.dialog_offer_textPrice1);
		final TextView dialog_offer_textPrice2 = (TextView) offerDialog.findViewById(R.id.dialog_offer_textPrice2);
		RelativeLayout dialog_offer_relativeLayoutDown = (RelativeLayout) offerDialog.findViewById(R.id.dialog_offer_relativeLayoutDown);
		final RelativeLayout dialog_offer_relativeLayoutColorDown = (RelativeLayout) offerDialog.findViewById(R.id.dialog_offer_relativeLayoutColorDown);
		dialog_offer_relativeLayoutColorDown.startAnimation(fade_diallog_offerDown);
		final RelativeLayout dialog_offer_relativeLayoutUp = (RelativeLayout) offerDialog.findViewById(R.id.dialog_offer_relativeLayoutUp);
		final RelativeLayout dialog_offer_relativeLayoutColorUp = (RelativeLayout) offerDialog.findViewById(R.id.dialog_offer_relativeLayoutColorUp);
		dialog_offer_relativeLayoutColorUp.startAnimation(fade_diallog_offerUp);
		RelativeLayout dialog_offer_relativeLayoutSend = (RelativeLayout) offerDialog.findViewById(R.id.dialog_offer_relativeLayoutSend);
		
		dialog_offer_textUsername.setText(user.UserName);
		Utils.getImageLoader(_activity).displayImage(user.ImageUrl.replace("d150", "thumbs"), dialog_offer_imageProfile, Utils.getImageOptions());
		
		dialog_offer_relativeLayoutDown.setOnClickListener(new OnClickListener() {	
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(offerAmount>1)
				{
					dialog_offer_relativeLayoutColorDown.startAnimation(fade_diallog_offerDown);
					offerAmount--;
					dialog_offer_textPrice2.startAnimation(fade_in);
					dialog_offer_textPrice2.setText("€ "+offerAmount);
					dialog_offer_textPrice1.startAnimation(fade_out);
					dialog_offer_textPrice1.setText("€ "+(offerAmount+1));
				}
			}
		});
		
		dialog_offer_relativeLayoutUp.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(offerAmount<40)
				{
					dialog_offer_relativeLayoutColorUp.startAnimation(fade_diallog_offerUp);
					offerAmount++;
					dialog_offer_textPrice2.startAnimation(fade_in);
					dialog_offer_textPrice2.setText("€ "+offerAmount);
					dialog_offer_textPrice1.startAnimation(fade_out);
					dialog_offer_textPrice1.setText("€ "+(offerAmount-1));
				}
				
			}
		});
		dialog_offer_relativeLayoutSend.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//Log.v("APP", "rtrtrt "+price);
				new offerTask(user.ProfileId,String.valueOf(offerAmount)).execute();
				offerDialog.cancel();
				
				
			}
		});
	}
	
	class rejectLikeTask extends AsyncTask<String,Void,Void>{
		@Override
		protected Void doInBackground(String... params) {
			String res = ServiceClient.rejectLike(params[0], rejectReason);
			return null;
		}
		@Override
		protected void onPostExecute(Void p){
			likeList.offers.clear();
			new getLikesTask().execute();
			
			Panel.refresh();
			Controller.refreshCounters(_activity);
		}
	}
	
	
	class sendPokeTask extends AsyncTask<Void,Void,Void>
	{

		String otherProfileid;
		String parentOffer;
		public sendPokeTask(String otherProfileid,String parentOffer)
		{
			this.otherProfileid = otherProfileid;
			this.parentOffer = parentOffer;
		}
		
		@Override
		protected Void doInBackground(Void... params) 
		{
			ServiceClient.poke(Utils.getSharedPreferences(_activity).getString("userid", ""), otherProfileid, parentOffer);
			return null;
		}

		@Override
		protected void onPostExecute(Void p)
		{
			likeList.offers.clear();
			new getLikesTask().execute();
			
			
			Panel.refresh();
			Controller.refreshCounters(_activity);
		}	
	}
	class offerTask extends AsyncTask<String,Void,String>
	{
        String result;
        String _userid,_offerAmount;
        public offerTask(String userid,String offerAmount)
        {
        	this._userid = userid;
        	this._offerAmount = offerAmount;
        }
        @Override
		protected void onPreExecute()
        {}
        
		@Override
		protected String doInBackground(String... params) 
		{
			result = ServiceClient.makeOffer("1", _offerAmount, Utils.getSharedPreferences(_activity).getString("userid", ""), _userid, Utils.getSharedPreferences(_activity).getString("genderid",""),"0");
			return result;
		}
		
		@Override
		protected void onPostExecute(String res)
		{
			if(result.contains("created"))
			{
				Toast.makeText(_activity, _activity.getResources().getString(R.string.offersent), Toast.LENGTH_LONG).show();	
			}
			else if(result.contains("already"))
			{
				Toast.makeText(_activity, _activity.getString(R.string.offeralready), Toast.LENGTH_LONG).show();
			}
			else if(result.contains("ERROR"))
			{
				Toast.makeText(_activity, _activity.getString(R.string.errorsent), Toast.LENGTH_LONG).show();			
			}
			
			
			
			////////////
			likeList.offers.clear();
			new getLikesTask().execute();
			
			
			Panel.refresh();
			Controller.refreshCounters(_activity);
		}
	}
	
	
	class acceptOfferTask extends AsyncTask<String,Void,Void>{

		@Override
		protected Void doInBackground(String... args) {			
			String result = ServiceClient.acceptOffer(Utils.getSharedPreferences(_activity).getString("userid", "") ,args[0]);
			return null;
		}
		
		@Override
		protected void onPostExecute(Void p){
			offerList.offers.clear();
			new getOffersTask().execute();
			
			Panel.refresh();
			Controller.refreshCounters(_activity);
		}
		
	}
}

package com.goomena.app.adapters;

import java.util.ArrayList;

import com.goomena.app.R;
import com.goomena.app.models.UserProfile;
import com.goomena.app.tools.Log;
import com.goomena.app.tools.Utils;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ItemGridProfileAdapter extends BaseAdapter
{
	Activity _activity;
	private ArrayList<UserProfile> profileList;
	int _mode;
	Boolean flagAnim=false;
	String privatePhoto;
	
	public ItemGridProfileAdapter(Activity activity,int mode)
	{
		this._activity = activity;
		this._mode = mode;
		profileList = new ArrayList<UserProfile>();
	}
	
	public void setItems(ArrayList<UserProfile> profiles){
		this.profileList = profiles;			
	}	
	

	public void setAnim(Boolean flag){
		flagAnim = flag;
	}	
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return profileList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return profileList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) 
	{
		// TODO Auto-generated method stub
		View rowView = null;
		LayoutInflater inflater = _activity.getLayoutInflater();
		final UserProfile profile = profileList.get(position);
		
		if(rowView==null)
		{			
			rowView = inflater.inflate(R.layout.item_grid_profile, null);
			ViewHolder vholder = new ViewHolder();
			vholder.item_grid_profile_textTitle = (TextView)rowView.findViewById(R.id.item_grid_profile_textTitle);	
			vholder.item_grid_profile_imageProfile = (ImageView)rowView.findViewById(R.id.item_grid_profile_imageProfile);	
			vholder.item_grid_profile_textNumPho = (TextView)rowView.findViewById(R.id.item_grid_profile_textNumPho);	
			vholder.item_grid_profile_textUsername = (TextView)rowView.findViewById(R.id.item_grid_profile_textUsername);	
			vholder.item_grid_profile_imageOval_yellow = (ImageView)rowView.findViewById(R.id.item_grid_profile_imageOval_yellow);
			vholder.item_grid_profile_imageVIP = (ImageView)rowView.findViewById(R.id.item_grid_profile_imageVIP);
			vholder.item_grid_profile_textAge = (TextView)rowView.findViewById(R.id.item_grid_profile_textAge);	
			vholder.item_grid_profile_textLocation = (TextView)rowView.findViewById(R.id.item_grid_profile_textLocation);		
			rowView.setTag(vholder);
		}
		ViewHolder viewHolder = (ViewHolder)rowView.getTag();
		
		if(flagAnim)
		{
			rowView.setAnimation(AnimationUtils.loadAnimation(_activity, R.anim.gridview_anm));//////////////////////////////
		}
		
		if(position == 1)
		{
			if(_mode == 0)
			{
				viewHolder.item_grid_profile_textTitle.setText(_activity.getResources().getString(R.string.online));
				viewHolder.item_grid_profile_textTitle.setVisibility(View.VISIBLE);
			}
			else if(_mode == 1)
			{
				viewHolder.item_grid_profile_textTitle.setText(_activity.getResources().getString(R.string.myfavlist));
				viewHolder.item_grid_profile_textTitle.setVisibility(View.VISIBLE);
			}
			else if(_mode == 2)
			{
				viewHolder.item_grid_profile_textTitle.setText(_activity.getResources().getString(R.string.seenmyprofile));
				viewHolder.item_grid_profile_textTitle.setVisibility(View.VISIBLE);
			}
			else if(_mode == 3)
			{
				viewHolder.item_grid_profile_textTitle.setText(_activity.getResources().getString(R.string.favme));
				viewHolder.item_grid_profile_textTitle.setVisibility(View.VISIBLE);
			}
			else if(_mode == 4)
			{
				viewHolder.item_grid_profile_textTitle.setText(_activity.getResources().getString(R.string.accesswhomphoto));
				viewHolder.item_grid_profile_textTitle.setVisibility(View.VISIBLE);
			}
			else if(_mode == 5)
			{
				viewHolder.item_grid_profile_textTitle.setText(_activity.getResources().getString(R.string.accessmephoto));
				viewHolder.item_grid_profile_textTitle.setVisibility(View.VISIBLE);
			}
			else if(_mode == 6)
			{
				viewHolder.item_grid_profile_textTitle.setText("Block List");
				viewHolder.item_grid_profile_textTitle.setVisibility(View.VISIBLE);
			}
			else if(_mode == 11)
			{
				viewHolder.item_grid_profile_textTitle.setText(_activity.getResources().getString(R.string.nearest_members));
				viewHolder.item_grid_profile_textTitle.setVisibility(View.VISIBLE);
			}
			else if(_mode == 12)
			{
				viewHolder.item_grid_profile_textTitle.setText(_activity.getResources().getString(R.string.newest_members));
				viewHolder.item_grid_profile_textTitle.setVisibility(View.VISIBLE);
			}
			else if(_mode == 13)
			{
				viewHolder.item_grid_profile_textTitle.setText(_activity.getResources().getString(R.string.oldest_members));
				viewHolder.item_grid_profile_textTitle.setVisibility(View.VISIBLE);
			}
			else if(_mode == 14)
			{
				viewHolder.item_grid_profile_textTitle.setText(_activity.getResources().getString(R.string.recent_login));
				viewHolder.item_grid_profile_textTitle.setVisibility(View.VISIBLE);
			}
			else if(_mode == 15)
			{
				viewHolder.item_grid_profile_textTitle.setText(_activity.getResources().getString(R.string.vip_members));
				viewHolder.item_grid_profile_textTitle.setVisibility(View.VISIBLE);
			}
			else if(_mode == 21)
			{
				viewHolder.item_grid_profile_textTitle.setText(_activity.getResources().getString(R.string.serachbyname));
				viewHolder.item_grid_profile_textTitle.setVisibility(View.VISIBLE);
			}
			else if(_mode == 22)
			{
				viewHolder.item_grid_profile_textTitle.setText(_activity.getResources().getString(R.string.serachbyfilter));
				viewHolder.item_grid_profile_textTitle.setVisibility(View.VISIBLE);
			}
		}
			
		
		if(profile.ImageUrl.contains("Images2/woman"))
        {
        	privatePhoto = "drawable://" + R.drawable.private_for_woman;
        }
        else if(profile.ImageUrl.contains("Images2/men.png"))
        {
        	privatePhoto = "drawable://" + R.drawable.private_for_men;
        }
        else
        {
        	privatePhoto = profile.ImageUrl.replace("d150", "thumbs");
        }
		Utils.getImageLoader(_activity).displayImage(privatePhoto, viewHolder.item_grid_profile_imageProfile, Utils.getImageOptions()); 	    
		
		viewHolder.item_grid_profile_textNumPho.setText(""+profile.PhotosCount+" "+_activity.getResources().getString(R.string.photos));
		viewHolder.item_grid_profile_textUsername.setText(profile.UserName);
		
		
		if(profile.IsVip!=null)
		{
			if(profile.IsVip.equals("1"))
			{
				viewHolder.item_grid_profile_imageVIP.setVisibility(View.VISIBLE);
			}
		}
		
		if(profile.IsOnline)
		{
			viewHolder.item_grid_profile_imageOval_yellow.setVisibility(View.VISIBLE);
		}
		else
		{
			viewHolder.item_grid_profile_imageOval_yellow.setVisibility(View.GONE);
		}
			
		
		if(profile.UserCountry==null)
		{
			viewHolder.item_grid_profile_textLocation.setText(profile.UserCity+", "+profile.UserRegion);
		}
		else
		{
			viewHolder.item_grid_profile_textLocation.setText(profile.UserCity+", "+profile.UserRegion+", " +profile.UserCountry);
		}
		
		
		viewHolder.item_grid_profile_textAge.setText(_activity.getResources().getText(R.string.age)+" "+profile.UserAge+" "+_activity.getResources().getText(R.string.years));
		
    
		return rowView;	
	}

	
	class ViewHolder 
	{
		public ImageView item_grid_profile_imageProfile,item_grid_profile_imageOval_yellow,item_grid_profile_imageVIP;	
		public TextView item_grid_profile_textAge,item_grid_profile_textLocation,item_grid_profile_textUsername,location,item_grid_profile_textTitle,item_grid_profile_textNumPho;

	}
}

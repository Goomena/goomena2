package com.goomena.app.adapters;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import com.goomena.app.Controller;
import com.goomena.app.Panel;
import com.goomena.app.R;
import com.goomena.app.data.ServiceClient;
import com.goomena.app.global.GlobalLikes;
import com.goomena.app.global.GlobalMessages;
import com.goomena.app.global.GlobalOffers;
import com.goomena.app.models.Message;
import com.goomena.app.models.MessageInfo;
import com.goomena.app.models.MessageList;
import com.goomena.app.models.UserProfile;
import com.goomena.app.tools.Log;
import com.goomena.app.tools.Utils;
import com.goomena.fragments.FragMessageView;
import com.goomena.fragments.FragProfile;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class ConversationAdapter  extends BaseAdapter
{
	private Activity _activity;
	MessageList list;
	MessageInfo _minfo;
	UserProfile _userprofile;
	int currentGenderId;
	String hash = "";
	
	private HashMap<String, Integer> emoticons = new HashMap<String, Integer>();
	private ArrayList<String> arrayListSmileys = new ArrayList<String>();
	
	private final static String TAG_FRAGMENT = "TAG_FRAGMENT";
	
	Boolean openCheck=false;
	
	SparseBooleanArray mSparseBooleanArray;
	
	public ConversationAdapter(Activity activity,MessageInfo minfo,UserProfile userprofile)
	{
		this._activity = activity;
		this._minfo = minfo;
		this._userprofile = userprofile;
		currentGenderId = Integer.valueOf(Utils.getSharedPreferences(activity).getString("genderid", ""));
		mSparseBooleanArray = new SparseBooleanArray();
		
		emoticons.put(":)", R.drawable.emotic_smile);
		emoticons.put(":-(", R.drawable.emotic_sadsmile);
		emoticons.put(":-D", R.drawable.emotic_bigsmile);
		emoticons.put("8-)", R.drawable.emotic_cool);
		emoticons.put(":-o", R.drawable.emotic_surprised);
		emoticons.put(";-)", R.drawable.emotic_wink);
		emoticons.put(";-(", R.drawable.emotic_crying);
		emoticons.put("(sweat)", R.drawable.emotic_sweating);
		emoticons.put(":-|", R.drawable.emotic_speechless);
		emoticons.put(":-*", R.drawable.emotic_kiss);
		emoticons.put(":-p", R.drawable.emotic_tongueout);
		emoticons.put("(blush)", R.drawable.emotic_blush);
		emoticons.put(":^)", R.drawable.emotic_wondering);
		emoticons.put("(snooze)", R.drawable.emotic_sleepy);
		emoticons.put("|-(", R.drawable.emotic_dull);
		emoticons.put("(inlove)", R.drawable.emotic_inlove);
		emoticons.put("(grin)", R.drawable.emotic_evilgrin);
		emoticons.put("(talk)", R.drawable.emotic_talking);
		emoticons.put("(yawn)", R.drawable.emotic_yawning);
		emoticons.put("(puke)", R.drawable.emotic_puke);
		emoticons.put("(doh)", R.drawable.emotic_doh);
		emoticons.put(":-@", R.drawable.emotic_angry);
		emoticons.put("(wasntme)", R.drawable.emotic_itwasntme);
		emoticons.put("(party)", R.drawable.emotic_party);
		emoticons.put(":-s", R.drawable.emotic_worried);
		emoticons.put("(mm)", R.drawable.emotic_mmm);
		emoticons.put("(nerd)", R.drawable.emotic_nerd);
		emoticons.put(":-x", R.drawable.emotic_lipssealed);
		emoticons.put("(hi)", R.drawable.emotic_hi);
		emoticons.put("(call)", R.drawable.emotic_call);
		emoticons.put("(devil)", R.drawable.emotic_devil);
		emoticons.put("(angel)", R.drawable.emotic_angel);
		emoticons.put("(envy)", R.drawable.emotic_envy);
		emoticons.put("(wait)", R.drawable.emotic_wait);
		emoticons.put("(hug)", R.drawable.emotic_hug);
		emoticons.put("(makeup)", R.drawable.emotic_makeup);
		emoticons.put("(chuckle)", R.drawable.emotic_giggle);
		emoticons.put("(clap)", R.drawable.emotic_clapping);
		emoticons.put("(think)", R.drawable.emotic_thinking);
		emoticons.put("(bow)", R.drawable.emotic_bow);
		emoticons.put("(ok)", R.drawable.emotic_yes);
		emoticons.put("(N)", R.drawable.emotic_no);
		emoticons.put("(handshake)", R.drawable.emotic_handshake);
		emoticons.put("(h)", R.drawable.emotic_heart);
		emoticons.put("(u)", R.drawable.emotic_brokenheart);
		emoticons.put("(m)", R.drawable.emotic_mail);
		emoticons.put("(F)", R.drawable.emotic_flower);
		emoticons.put("(rain)", R.drawable.emotic_rain);
		emoticons.put("(sun)", R.drawable.emotic_sunshine);
		emoticons.put("(time)", R.drawable.emotic_time);
		emoticons.put("(music)", R.drawable.emotic_music);
		emoticons.put("(movie)", R.drawable.emotic_movie);
		emoticons.put("(ph)", R.drawable.emotic_phone);
		emoticons.put("(coffee)", R.drawable.emotic_coffee);
		emoticons.put("(pizza)", R.drawable.emotic_pizza);
		emoticons.put("(cash)", R.drawable.emotic_cash);
		emoticons.put("(flex)", R.drawable.emotic_muscle);
		emoticons.put("(cake)", R.drawable.emotic_cake);
		emoticons.put("(beer)", R.drawable.emotic_beer);
		emoticons.put("(d)", R.drawable.emotic_drink);
		emoticons.put("(dance)", R.drawable.emotic_dancing);
		emoticons.put("(ninja)", R.drawable.emotic_ninja);
		emoticons.put("(*)", R.drawable.emotic_star);
		emoticons.put("(mooning)", R.drawable.emotic_mooning);
		emoticons.put("(finger)", R.drawable.emotic_finger);
		emoticons.put("(bandit)", R.drawable.emotic_bandit);
		emoticons.put("(drunk)", R.drawable.emotic_drunk);
		emoticons.put("(smoking)", R.drawable.emotic_smoking);
		emoticons.put("(toivo)", R.drawable.emotic_toivo);
		emoticons.put("(rock)", R.drawable.emotic_rock);
		emoticons.put("(headbang)", R.drawable.emotic_headbang);
		
		
	}
	
	public ArrayList<String> getCheckedItemsIDs() {
		ArrayList<String> mTempArry = new ArrayList<String>();

		for(int i=0;i<list.list.size();i++) {
			if(mSparseBooleanArray.get(i)) {
				mTempArry.add(list.list.get((list.list.size() - 1) - i).MessageId);
			}
		}
		
		mSparseBooleanArray.clear();

		return mTempArry;
	}
	
	@Override
	public int getCount() 
	{
		// TODO Auto-generated method stub
		return list.list.size();
	}
	
	public void setItems(MessageList list)
	{
		this.list = list;
	}
	
	public void setOpenCheck(Boolean opck)
	{
		openCheck = opck;
	}
	
	public Boolean getOpenCheck()
	{
		return openCheck;
	}

	@Override
	public Object getItem(int position) 
	{
		// TODO Auto-generated method stub
		return list.list.get(position);
	}
	
	@Override
	public long getItemId(int position) 
	{
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, View row, ViewGroup parent) 
	{
		// TODO Auto-generated method stub
		final Message m = list.list.get( (list.list.size() - 1) - position);
		row = null;
		
		
		
		
		if(row==null)
		{
			LayoutInflater inflater = _activity.getLayoutInflater();
			row = inflater.inflate(R.layout.item_conversation_send , null);
		}
			ViewHolder holder = new ViewHolder();
			holder.conv_receive_relativeLayoutOpen = (RelativeLayout) row.findViewById(R.id.conv_receive_relativeLayoutOpen);
			holder.conv_receive_textPayToRead = (TextView)row.findViewById(R.id.conv_receive_textPayToRead);
			holder.conv_receive_imageProfile = (ImageView)row.findViewById(R.id.conv_receive_imageProfile);
			holder.conv_receive_textUsername = (TextView)row.findViewById(R.id.conv_receive_textUsername);
			holder.conv_receive_textDate = (TextView) row.findViewById(R.id.conv_receive_textDate);
			holder.conv_receive_textbudy = (TextView)row.findViewById(R.id.conv_receive_textbudy);
			//holder.trash = (ImageView)row.findViewById(R.id.trashicon);					
			holder.conv_send_relativeLayout = (RelativeLayout)row.findViewById(R.id.conv_send_relativeLayout);
			holder.conv_recieve_relativeLayout = (RelativeLayout)row.findViewById(R.id.conv_recieve_relativeLayout);
			holder.conv_send_imageProfile = (ImageView)row.findViewById(R.id.conv_send_imageProfile);
			holder.conv_send_textUsername = (TextView)row.findViewById(R.id.conv_send_textUsername);	
			holder.conv_send_textDate = (TextView) row.findViewById(R.id.conv_send_textDate);
			holder.conv_send_textbudy = (TextView)row.findViewById(R.id.conv_send_textbudy);
			
			holder.checkBoxDelete = (CheckBox)row.findViewById(R.id.checkBoxDelete);
			
			//holder.trash = (ImageView)row.findViewById(R.id.trashicon);
			
			row.setTag(holder);
		
		
		//Log.v("APP","ttt "+m.statusId);
		if(row!=null)
		{
			ViewHolder holder1 = (ViewHolder) row.getTag();
			/*
			holder.trash.setOnClickListener(new OnClickListener() 
			{
				@Override
				public void onClick(View v) 
				{
					//new deleteMessageTask().execute(m.MessageId);
				}
			});
			*/
			
			//holder1.conv_send_relativeLayout.setAnimation(AnimationUtils.loadAnimation(_activity, R.anim.listview_anim));//////////////
			//holder1.conv_recieve_relativeLayout.setAnimation(AnimationUtils.loadAnimation(_activity, R.anim.listview_anim));//////////////
			
			if(openCheck)
			{
				holder1.checkBoxDelete.setVisibility(View.VISIBLE);
			}
			else
			{
				holder1.checkBoxDelete.setVisibility(View.GONE);
			}
			
			holder1.checkBoxDelete.setTag(position);
			holder1.checkBoxDelete.setChecked(mSparseBooleanArray.get(position));
			holder1.checkBoxDelete.setOnCheckedChangeListener(mCheckedChangeListener);
			
			if(m.IsReceived)
			{
				holder1.conv_send_relativeLayout.setVisibility(View.GONE);
				holder1.conv_recieve_relativeLayout.setVisibility(View.VISIBLE);
				if(_minfo!=null)
				{
					Utils.getImageLoader(_activity).displayImage(_minfo.ProfileImg, holder1.conv_receive_imageProfile,Utils.getImageOptions());
					holder1.conv_receive_textUsername.setText(_minfo.UserName);
				}
				else
				{
					Utils.getImageLoader(_activity).displayImage(_userprofile.ImageUrl, holder1.conv_receive_imageProfile,Utils.getImageOptions());
					holder1.conv_receive_textUsername.setText(_userprofile.UserName);
					
					
					holder1.conv_receive_imageProfile.setOnClickListener(new OnClickListener() {
						
						@SuppressLint("NewApi") 
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							InputMethodManager inputManager = (InputMethodManager) _activity.getSystemService(Context.INPUT_METHOD_SERVICE);
						    inputManager.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
						    
						    if (FragMessageView.popupWindow!=null)
						    {
						    	FragMessageView.popupWindow.dismiss();	
						    }
						    
							ArrayList<UserProfile> _userProfilesFull_ = new ArrayList<UserProfile>();
							_userProfilesFull_.add(_userprofile);
							Fragment fr = new FragProfile(2,_userProfilesFull_,0);				
							FragmentManager fm = _activity.getFragmentManager();
							FragmentTransaction fragmentTransaction = fm.beginTransaction();
							fragmentTransaction.setCustomAnimations(R.anim.left_slidein_list_animator, R.anim.left_slideout_list_animator,R.anim.right_slidein_list_animator, R.anim.right_slideout_list_animator);
							fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
							fragmentTransaction.addToBackStack(null);
							fragmentTransaction.commit();	
						}
					});
				}
				
				
				holder1.conv_receive_textDate.setText(m.DateTimeSent);
				
				if(m.statusId.equals("0"))
				{
					holder1.conv_receive_relativeLayoutOpen.setTag(m);
					holder1.conv_receive_relativeLayoutOpen.setOnClickListener(new OnClickListener() 
					{
						@Override
						public void onClick(final View v) 
						{		
							new getCheckCountryTask(m,v).execute();			
						}
					});	
					
					if (!m.isRead && currentGenderId==1 && m.FromProfileId.equals("1")) 
					{
						holder1.conv_receive_relativeLayoutOpen.setVisibility(View.GONE);
						holder1.conv_receive_textbudy.setText(getSmiledText(_activity,m.Body));  
						
					} 
					else if(!m.isRead && currentGenderId==1)
					{
						holder1.conv_receive_relativeLayoutOpen.setVisibility(View.VISIBLE);
						
					}
					else
					{
						holder1.conv_receive_relativeLayoutOpen.setVisibility(View.GONE);
						holder1.conv_receive_textbudy.setText(getSmiledText(_activity,m.Body));
						if (m != null) 
						{
							new unlockMessageTask(m,holder.conv_receive_relativeLayoutOpen).execute();
						}
					}	
					/////////messagePanel.refresh();	
				}//read
				else{
					holder1.conv_receive_relativeLayoutOpen.setVisibility(View.GONE);
					holder1.conv_receive_textbudy.setText(getSmiledText(_activity,m.Body));
				}
			
			}
			else if(m.IsSent)
			{
				holder1.conv_recieve_relativeLayout.setVisibility(View.GONE);
				holder1.conv_send_relativeLayout.setVisibility(View.VISIBLE);
				Utils.getImageLoader(_activity).displayImage(Utils.getSharedPreferences(_activity).getString("imageurl", ""), holder.conv_send_imageProfile, Utils.getImageOptions());
				holder1.conv_send_textUsername.setText((Utils.getSharedPreferences(_activity).getString("username", "")));
				holder1.conv_send_textDate.setText(m.DateTimeSent);
				holder1.conv_send_textbudy.setText(getSmiledText(_activity,m.Body));
			}
			
		}
		
		return row;
	}
	
	OnCheckedChangeListener mCheckedChangeListener = new OnCheckedChangeListener() {

		@Override
		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
			// TODO Auto-generated method stub
			mSparseBooleanArray.put((Integer) buttonView.getTag(), isChecked);
		}
	};
	
	class ViewHolder
	{
		ImageView conv_send_imageProfile;
		TextView conv_send_textUsername,conv_send_textDate,conv_send_textbudy;
		
		ImageView conv_receive_imageProfile;
		TextView conv_receive_textUsername,conv_receive_textDate,conv_receive_textbudy,conv_receive_textPayToRead;
		RelativeLayout conv_receive_relativeLayoutOpen,layoutmessage;
		RelativeLayout conv_send_relativeLayout,conv_recieve_relativeLayout;
		
		CheckBox checkBoxDelete;
	}
	
	
	class getCheckCountryTask extends AsyncTask<Void,Integer, String>{

		Message m;
		View v;
		public getCheckCountryTask(Message m,View v){
			this.m = m;
			this.v = v;
		}
		
		@Override
		protected String doInBackground(Void... params) {
			String result = ServiceClient.getCheckCountry(Utils.getSharedPreferences(_activity).getString("userid", "") ,
					m.FromProfileId);
			return result;
		}
		
		@Override
		protected void onPostExecute(String s){
			if ( s.indexOf("true")>-1) {
			new unlockMessageTask(m,v).execute();
			} else if ( s.indexOf("false")>-1) {
				AlertDialog.Builder builder1 = new AlertDialog.Builder(_activity);
	            builder1.setMessage(R.string.dialogcountry);
	            builder1.setCancelable(true);
	            builder1.setPositiveButton(R.string.buttonOk,
	                    new DialogInterface.OnClickListener() {
	                public void onClick(DialogInterface dialog, int id) {
	                	new unlockMessageTask(m,v).execute();		
	                    dialog.cancel();
	                }
	            });
	            builder1.setNegativeButton(R.string.buttonCancel,
	                    new DialogInterface.OnClickListener() {
	                public void onClick(DialogInterface dialog, int id) {
	                    dialog.cancel();
	                }
	            });

	            AlertDialog alert11 = builder1.create();
	            alert11.show();
			} else {
				Toast.makeText(_activity, "error", Toast.LENGTH_LONG).show();
			}
		}
		
	}
		
	class unlockMessageTask extends AsyncTask<Void,Integer, String>{

		Message m;
		View v;
		public unlockMessageTask(Message m,View v){
			this.m = m;
			this.v = v;
		}
		
		@Override
		protected String doInBackground(Void... params) {
			String result = ServiceClient.unlockMessage(Utils.getSharedPreferences(_activity).getString("userid", "") ,
					Utils.getSharedPreferences(_activity).getString("genderid", ""),
					m.FromProfileId, m.MessageId, "0");
			return result;
		}
		
		@Override
		protected void onPostExecute(String s){
			if(s.contains("ok"))
			{
				v.setVisibility(View.GONE);				
				m.isRead = true;
				ConversationAdapter.this.notifyDataSetChanged();
				
				Panel.refresh();
				Controller.refreshCredits(_activity);
				Controller.refreshCounters(_activity);
			}
			else if(s.contains("noceredits")){
				new getHashTask().execute();			
			}
		}
		
	}
	
	class getHashTask extends AsyncTask<Void,Void,Void>{

		@Override
		protected Void doInBackground(Void... params) {
			hash = ServiceClient.getHashTask(Utils.getSharedPreferences(_activity).getString("username",""));
			return null;
		}
		  
		@Override
		protected void onPostExecute(Void p){
			hash = hash.replace("\"", "");
			String uri = "http://www.goomena.com/Login.aspx?hash="+hash+"&login="+Utils.getSharedPreferences(_activity).getString("username","")+"&ReturnUrl=%2fMembers%2fSelectProduct.aspx";
			Log.v("APP", uri);
			Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
			_activity.startActivity(browserIntent);
		}
		
	}
	
	
	public Spannable getSmiledText(Context context, String text) {
        SpannableStringBuilder builder = new SpannableStringBuilder(text);
        int index;
        for (index = 0; index < builder.length(); index++) {
            for (Entry<String, Integer> entry : emoticons.entrySet()) {
                int length = entry.getKey().length();
                if (index + length > builder.length())
                    continue;
                if (builder.subSequence(index, index + length).toString().equals(entry.getKey())) {
                    builder.setSpan(new ImageSpan(context, entry.getValue()), index, index + length,
                            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    index += length - 1;
                    break;
                }
            }
        }
        return builder;
    }

}

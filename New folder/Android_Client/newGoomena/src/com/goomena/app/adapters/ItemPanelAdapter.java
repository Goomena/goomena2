package com.goomena.app.adapters;

import java.util.ArrayList;

import com.goomena.app.R;
import com.goomena.app.models.MessageInfoList;
import com.goomena.app.models.Offer;
import com.goomena.app.models.OfferList;
import com.goomena.app.models.UserProfile;
import com.goomena.app.tools.Log;
import com.goomena.app.tools.Utils;

import android.app.Activity;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ItemPanelAdapter extends BaseAdapter
{
	private Activity _activity;
	private OfferList list;
	private MessageInfoList list_message;
	String userid,genderid;
	int _modeCase;
	
	public ItemPanelAdapter(Activity activity,OfferList list,MessageInfoList list_message,int modeCase)
	{
		this._activity = activity;
		this.list = list;
		this.list_message = list_message;
		this._modeCase = modeCase;
		
		SharedPreferences settings = Utils.getSharedPreferences(activity);
		userid = settings.getString("userid", "");
		genderid = settings.getString("genderid", "");
		userid = settings.getString("userid", "");
	}
	
	public void setItems(OfferList list,MessageInfoList list_message){
		if(_modeCase==1)
		{
			this.list_message = list_message;
		}
		else
		{
		 	this.list = list;		
		}
	}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		if(_modeCase==1)
		{
			return list_message.msgList.size();
		}
		else
		{
			return list.offers.size();
		}
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		if(_modeCase==1)
		{
			return list_message.msgList.get(position);
		}
		else
		{
			return list.offers.get(position);
		}
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) 
	{
		// TODO Auto-generated method stub
		
		View rowView = null;
		LayoutInflater inflater = _activity.getLayoutInflater();
		
		if(_modeCase==0 || _modeCase==2)  //likes and offers
		{
			final UserProfile profile = list.offers.get(position).profile;
		
			if(rowView==null)
			{			
				rowView = inflater.inflate(R.layout.item_panel, null);			
				ViewHolder vholder = new ViewHolder();
				vholder.item_panel_imageProfilePic = (ImageView)rowView.findViewById(R.id.item_panel_imageProfilePic);						
				vholder.item_panel_textUsername = (TextView)rowView.findViewById(R.id.item_panel_textUsername);	
				vholder.item_panel_textAge = (TextView)rowView.findViewById(R.id.item_panel_textAge);
				vholder.item_panel_textCase = (TextView)rowView.findViewById(R.id.item_panel_textCase);
				vholder.item_panel_textName = (TextView)rowView.findViewById(R.id.item_panel_textName);			
				rowView.setTag(vholder);
			}
			ViewHolder viewHolder = (ViewHolder)rowView.getTag();
    
			//Log.v("APP", "sfhfshshsfdhdh "+profile.ImageUrl);
			
			
			if(profile.ImageUrl.contains("Images2/woman"))
	        {
				profile.ImageUrl = "drawable://" + R.drawable.private_for_woman;
	        }
	        else if(profile.ImageUrl.contains("Images2/men.png"))
	        {
	        	profile.ImageUrl = "drawable://" + R.drawable.private_for_men;
	        }
	        
	        
			Utils.getImageLoader(_activity).displayImage(profile.ImageUrl, viewHolder.item_panel_imageProfilePic, Utils.getImageOptions());
		
			viewHolder.item_panel_textUsername.setText(profile.UserName);
			viewHolder.item_panel_textAge.setText(_activity.getResources().getString(R.string.age)+" "+profile.UserAge+" "+_activity.getResources().getString(R.string.years));
		
			if(_modeCase==0)
			{
				viewHolder.item_panel_textCase.setText(" Like ");
			}
			else if(_modeCase==2)
			{
				viewHolder.item_panel_textCase.setText(" "+_activity.getResources().getString(R.string.offer)+" ");
			}
			viewHolder.item_panel_textName.setText(" "+profile.UserName);
		}
		else if(_modeCase==1) //message
		{
			if(rowView==null)
			{			
				rowView = inflater.inflate(R.layout.item_panel, null);			
				ViewHolder vholder = new ViewHolder();
				vholder.item_panel_imageProfilePic = (ImageView)rowView.findViewById(R.id.item_panel_imageProfilePic);						
				vholder.item_panel_textUsername = (TextView)rowView.findViewById(R.id.item_panel_textUsername);	
				vholder.item_panel_textAge = (TextView)rowView.findViewById(R.id.item_panel_textAge);
				vholder.item_panel_textCase = (TextView)rowView.findViewById(R.id.item_panel_textCase);
				vholder.item_panel_textName = (TextView)rowView.findViewById(R.id.item_panel_textName);	
				vholder.item_panel_relativeNumMessages = (RelativeLayout)rowView.findViewById(R.id.item_panel_relativeNumMessages);	
				vholder.item_panel_textNumMessages = (TextView)rowView.findViewById(R.id.item_panel_textNumMessages);	
				rowView.setTag(vholder);
			}
			ViewHolder viewHolder = (ViewHolder)rowView.getTag();
    
			Utils.getImageLoader(_activity).displayImage(list_message.msgList.get(position).ProfileImg.replace("d150", "thumbs"), viewHolder.item_panel_imageProfilePic, Utils.getImageOptions());
		
			viewHolder.item_panel_textUsername.setText(list_message.msgList.get(position).UserName);
			viewHolder.item_panel_textAge.setText(_activity.getResources().getString(R.string.age)+" "+list_message.msgList.get(position).Age+" "+_activity.getResources().getString(R.string.years));
		
			if(_modeCase==1)
			{
				viewHolder.item_panel_textCase.setText(" "+_activity.getResources().getString(R.string.message)+" ");
				viewHolder.item_panel_relativeNumMessages.setVisibility(View.VISIBLE);
				viewHolder.item_panel_textNumMessages.setText(list_message.msgList.get(position).MsgCount);
			}
			
			viewHolder.item_panel_textName.setText(" "+list_message.msgList.get(position).UserName);
		}
		
		return rowView;	
	}
	
	
	class ViewHolder 
	{
		public ImageView item_panel_imageProfilePic;	
		public TextView item_panel_textUsername,item_panel_textAge,item_panel_textCase,item_panel_textName;
		public RelativeLayout item_panel_relativeNew,item_panel_relativeNumMessages;
		public TextView item_panel_textNumMessages;
	}

}

package com.goomena.app.adapters;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.goomena.app.R;
import com.goomena.app.adapters.ItemLikeAdapter.ViewHolder;
import com.goomena.app.models.Offer;
import com.goomena.app.models.UserProfile;
import com.goomena.app.tools.Utils;

public class ItemOfferAdapter extends BaseAdapter
{
	Activity _activity;
	private ArrayList<Offer> offerList;
	int _viewMode;
	Boolean flagAnim=false;
	
	public ItemOfferAdapter(Activity activity,int viewMode)
	{
		this._activity = activity;
		this._viewMode = viewMode;
		offerList = new ArrayList<Offer>();
	}
	
	public void setItems(ArrayList<Offer> offers){
		this.offerList = offers;			
	}	
	
	public void setAnim(Boolean flag){
		flagAnim = flag;
	}	
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return offerList.size();
	}
	
	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return offerList.get(position);
	}
	
	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	
	@SuppressLint("NewApi") 
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View rowView = null;
		LayoutInflater inflater = _activity.getLayoutInflater();
		final UserProfile profile = offerList.get(position).profile;
		
		//accepted
		//if(viewMode==2)
		//{	
			if(rowView==null)
			{			
				rowView = inflater.inflate(R.layout.item_offer, null);			
				ViewHolder vholder = new ViewHolder();
				vholder.profileImg = (ImageView)rowView.findViewById(R.id.item_like_imageProfile);						
				vholder.userName = (TextView)rowView.findViewById(R.id.item_like_textUsername);	
				vholder.item_like_imageOval_yellow = (ImageView)rowView.findViewById(R.id.item_like_imageOval_yellow);
				vholder.location = (TextView)rowView.findViewById(R.id.item_like_textLocation);	
				vholder.item_offer_imageAnswers = (ImageView)rowView.findViewById(R.id.item_offer_imageAnswers);	
				vholder.item_offer_textAnswers = (TextView)rowView.findViewById(R.id.item_offer_textAnswers);
				vholder.item_offer_textReason = (TextView)rowView.findViewById(R.id.item_offer_textReason);
				//vholder.button2 = (TextView)rowView.findViewById(R.id.send);
				//vholder.button1 = (TextView)rowView.findViewById(R.id.delete);			
				rowView.setTag(vholder);
			}
			ViewHolder viewHolder = (ViewHolder)rowView.getTag();
			
			if(flagAnim)
			{
				rowView.setAnimation(AnimationUtils.loadAnimation(_activity, R.anim.listview_anim));//////////////////////////////
			}   
			
			if(profile.ImageUrl.contains("Images2/woman"))
	        {
				profile.ImageUrl = "drawable://" + R.drawable.private_for_woman;
	        }
	        else if(profile.ImageUrl.contains("Images2/men.png"))
	        {
	        	profile.ImageUrl = "drawable://" + R.drawable.private_for_men;
	        }
	        else
	        {
	        	profile.ImageUrl = profile.ImageUrl.replace("d150", "thumbs");
	        }
			Utils.getImageLoader(_activity).displayImage(profile.ImageUrl.replace("d150", "thumbs"), viewHolder.profileImg, Utils.getImageOptions()); 	    
			//viewHolder.profileImg.setTag(profile);
			//viewHolder.button2.setTag(profile);
			//viewHolder.button1.setTag(offerList.get(position).offerId);
	    
			viewHolder.userName.setText(profile.UserName);
			if(profile.IsOnline)
			{
				viewHolder.item_like_imageOval_yellow.setVisibility(View.VISIBLE);
			}
			else
			{
				viewHolder.item_like_imageOval_yellow.setVisibility(View.GONE);
			}
			viewHolder.location.setText(profile.UserCity+", "+profile.UserRegion);
	    
		//}
		
		if(_viewMode==0)
		{
			viewHolder.item_offer_imageAnswers.setBackgroundResource(R.drawable.new_bid);
			viewHolder.item_offer_textAnswers.setText(_activity.getResources().getString(R.string.newoffers));
			viewHolder.item_offer_textReason.setText(_activity.getResources().getString(R.string.newoffersanswers));
		}
		else if(_viewMode==1)
		{
			viewHolder.item_offer_imageAnswers.setBackgroundResource(R.drawable.pending_bid);
			viewHolder.item_offer_textAnswers.setText(_activity.getResources().getString(R.string.offersent));
			viewHolder.item_offer_textReason.setText(_activity.getResources().getString(R.string.offersentanswers));
		}
		else if(_viewMode==2)
		{
			viewHolder.item_offer_imageAnswers.setBackgroundResource(R.drawable.accepted_bid);
			viewHolder.item_offer_textAnswers.setText(_activity.getResources().getString(R.string.acceptedoffers));
			viewHolder.item_offer_textReason.setText(_activity.getResources().getString(R.string.offersacceptedReason));
		}
		else if(_viewMode==3)
		{
			viewHolder.item_offer_imageAnswers.setBackgroundResource(R.drawable.reject_bid);
			viewHolder.item_offer_textAnswers.setText(_activity.getResources().getString(R.string.rejectoffer));
			viewHolder.item_offer_textReason.setText(_activity.getResources().getString(R.string.thereason)+" "+getRejectReason(Integer.valueOf(offerList.get(position).rejectReason)));
		}
		
		
		return rowView;	
	}
	
	
	class ViewHolder 
	{
		public TextView button1,button2;		
		public ImageView profileImg,item_like_imageOval_yellow,item_offer_imageAnswers;	
		public TextView infoText,userName,location;
		public TextView item_offer_textAnswers,item_offer_textReason;

	}
	
	
	private String getRejectReason(int rejectId){
		switch (rejectId) {
		case 100:
			return _activity.getResources().getString(R.string.noinfo);
			
		case 101:
			return _activity.getResources().getString(R.string.norandevu);
						
		case 102:
			return _activity.getResources().getString(R.string.toofar);
			
		case 103:
			return _activity.getResources().getString(R.string.notinterest);
			
		default:
			return "";
		}
	}

}

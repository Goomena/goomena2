package com.goomena.app.adapters;

import java.util.ArrayList;

import com.goomena.app.R;
import com.goomena.app.models.Offer;
import com.goomena.app.models.UserProfile;
import com.goomena.app.tools.Log;
import com.goomena.app.tools.Utils;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ItemLikeAdapter extends BaseAdapter
{
	Activity _activity;
	private ArrayList<Offer> offerList;
	int _viewMode;
	Boolean flagAnim=false;
	
	public ItemLikeAdapter(Activity activity,int viewMode)
	{
		this._activity = activity;
		this._viewMode = viewMode;
		offerList = new ArrayList<Offer>();
	}
	
	public void setItems(ArrayList<Offer> offers){
		this.offerList = offers;			
	}	
	
	public void setAnim(Boolean flag){
		flagAnim = flag;
	}	

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return offerList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return offerList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View rowView = null;
		LayoutInflater inflater = _activity.getLayoutInflater();
		final UserProfile profile = offerList.get(position).profile;
		
		//accepted
		//if(viewMode==2)
		{	
			if(rowView==null)
			{			
				rowView = inflater.inflate(R.layout.item_like, null);
				ViewHolder vholder = new ViewHolder();
				vholder.item_like_textTitle = (TextView)rowView.findViewById(R.id.item_like_textTitle);	
				vholder.profileImg = (ImageView)rowView.findViewById(R.id.item_like_imageProfile);						
				vholder.userName = (TextView)rowView.findViewById(R.id.item_like_textUsername);	
				vholder.item_like_imageOval_yellow = (ImageView)rowView.findViewById(R.id.item_like_imageOval_yellow);
				vholder.location = (TextView)rowView.findViewById(R.id.item_like_textLocation);	
				//vholder.button2 = (TextView)rowView.findViewById(R.id.send);
				//vholder.button1 = (TextView)rowView.findViewById(R.id.delete);			
				rowView.setTag(vholder);
			}
			ViewHolder viewHolder = (ViewHolder)rowView.getTag();
		
			if(flagAnim)
			{
				rowView.setAnimation(AnimationUtils.loadAnimation(_activity, R.anim.gridview_anm));//////////////////////////////
			}

			if(position==1)
			{
				if(_viewMode==0)
				{	
					viewHolder.item_like_textTitle.setText("New likes");
					viewHolder.item_like_textTitle.setVisibility(View.VISIBLE);
				}
				else if(_viewMode==1)
				{
					viewHolder.item_like_textTitle.setText("Panding likes");
					viewHolder.item_like_textTitle.setVisibility(View.VISIBLE);
				}
				else if(_viewMode==2)
				{
					viewHolder.item_like_textTitle.setText("Accepted likes");
					viewHolder.item_like_textTitle.setVisibility(View.VISIBLE);
				}
				else if(_viewMode==3)
				{
					viewHolder.item_like_textTitle.setText("Rejeced likes");
					viewHolder.item_like_textTitle.setVisibility(View.VISIBLE);
				}
			}
			
			
			if(profile.ImageUrl.contains("Images2/woman"))
	        {
				profile.ImageUrl = "drawable://" + R.drawable.private_for_woman;
	        }
	        else if(profile.ImageUrl.contains("Images2/men.png"))
	        {
	        	profile.ImageUrl = "drawable://" + R.drawable.private_for_men;
	        }
	        else
	        {
	        	profile.ImageUrl = profile.ImageUrl.replace("d150", "thumbs");
	        }
			Utils.getImageLoader(_activity).displayImage(profile.ImageUrl, viewHolder.profileImg, Utils.getImageOptions()); 	    
			//viewHolder.profileImg.setTag(profile);
			//viewHolder.button2.setTag(profile);
			//viewHolder.button1.setTag(offerList.get(position).offerId);
	    
			viewHolder.userName.setText(profile.UserName);
			
			
			
			if(profile.IsOnline)
			{
				viewHolder.item_like_imageOval_yellow.setVisibility(View.VISIBLE);
			}
			else
			{
				viewHolder.item_like_imageOval_yellow.setVisibility(View.GONE);
			}
			
			
			
			viewHolder.location.setText(profile.UserCity+", "+profile.UserRegion);
	    
	    
		

	    
	    
		}
		
		
		return rowView;	
	}
	
	
	
	class ViewHolder 
	{
		public TextView button1,button2;		
		public ImageView profileImg,item_like_imageOval_yellow;	
		public TextView infoText,userName,location,item_like_textTitle;

	}
	
	class ViewHolderNew
	{
		public TextView buttonMsg, buttonReject, buttonPoke;
		public ImageView profileImg;
		public TextView userName;
	}

}

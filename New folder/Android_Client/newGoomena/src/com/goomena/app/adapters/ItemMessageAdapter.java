package com.goomena.app.adapters;


import java.util.ArrayList;

import com.goomena.app.Controller;
import com.goomena.app.Panel;
import com.goomena.app.R;
import com.goomena.app.data.ServiceClient;
import com.goomena.app.global.GlobalLikes;
import com.goomena.app.global.GlobalMessages;
import com.goomena.app.global.GlobalOffers;
import com.goomena.app.models.MessageInfo;
import com.goomena.app.models.MessageInfoList;
import com.goomena.app.models.Offer;
import com.goomena.app.tools.Log;
import com.goomena.app.tools.Utils;
import com.goomena.fragments.FragLoading;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class ItemMessageAdapter extends BaseAdapter
{
	private Activity _activity;
	private ArrayList<MessageInfo> MessageInfoList;
	private LayoutInflater inflater;
	private int _viewMode;
	Boolean flagAnim=false;
	
	ViewHolder holder;
	//MessageInfo item;
	
	String userid,genderid;
	
	
	FragLoading fragLoading;
	Animation animFadein;

	public ItemMessageAdapter(Activity activity,int viewMode)
	{
		this._activity = activity;
		this._viewMode = viewMode;
		
		MessageInfoList = new ArrayList<MessageInfo>();
		
		animFadein = AnimationUtils.loadAnimation(activity, R.anim.fade_in);
		SharedPreferences settings = Utils.getSharedPreferences(activity);
		userid = settings.getString("userid", "");
		
		fragLoading = new FragLoading(activity);
	}
	
	public void setItems(ArrayList<MessageInfo> MessageInfos)
	{
		this.MessageInfoList = MessageInfos;
	}
	
	public void setAnim(Boolean flag){
		flagAnim = flag;
	}	
	
	@Override
	public int getCount() 
	{
		// TODO Auto-generated method stub
		return MessageInfoList.size();
	}

	@Override
	public Object getItem(int position) 
	{
		// TODO Auto-generated method stub
		return MessageInfoList.get(position);
	}

	@Override
	public long getItemId(int position) 
	{
		// TODO Auto-generated method stub
		return 0;
	}

	
	@SuppressLint("NewApi") @Override
	public View getView(final int position, View view, ViewGroup parent) 
	{
		
		View rowView = null;
		//inflater = (LayoutInflater) _activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		//View viewLayout = inflater.inflate(R.layout.item_message, parent, false);
		final MessageInfo item = MessageInfoList.get(position);
		//item = MessageInfoList.get(position);
		
		if(rowView==null)
		{
			rowView = _activity.getLayoutInflater().inflate(R.layout.item_message, null);
			
			holder = new ViewHolder();
			holder.username = (TextView) rowView.findViewById(R.id.userName1);
			holder.imageOval_yellow = (ImageView)rowView.findViewById(R.id.imageOval_yellow);
			holder.sentdate = (TextView) rowView.findViewById(R.id.sentDate);
			//holder.received = (TextView) rowView.findViewById(R.id.received);
			holder.msgcount = (TextView) rowView.findViewById(R.id.msgNum);
			holder.status = (TextView) rowView.findViewById(R.id.textstatus);
			
			holder.userimg = (ImageView)rowView.findViewById(R.id.profileImg);
			holder.statusicon = (ImageView)rowView.findViewById(R.id.messageicon);
					
			holder.trash = (ImageView)rowView.findViewById(R.id.trash);
			
			rowView.setTag(holder);
		}
		
		
		ViewHolder holder = (ViewHolder)rowView.getTag();
		
		if(flagAnim)
		{
			rowView.setAnimation(AnimationUtils.loadAnimation(_activity, R.anim.listview_anim));//////////////////////////////
		}
		
		holder.username.setText(item.UserName);
		
		
		
		if(item.IsOnline)
		{
			holder.imageOval_yellow.setVisibility(View.VISIBLE);
		}
		else
		{
			holder.imageOval_yellow.setVisibility(View.GONE);
		}
		
		
		//new getUserStateTask(item.ProfileId,holder.imageOval_yellow).execute();
		//Log.v("APP", "tttt "+item.);
		holder.sentdate.setText(_activity.getResources().getString(R.string.msent)+" "+ item.DateTimeSent);
		holder.trash.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
					Log.v("APP", "----deleting messages---");
				
				AlertDialog dialog = new AlertDialog.Builder(_activity).create();
				dialog.setTitle(R.string.rusure);
				dialog.setMessage(_activity.getResources().getString(R.string.suretodelete)+" "+item.UserName+
						" "+_activity.getResources().getString(R.string.messageview)+
						" "+_activity.getResources().getString(getViewString()));
				
				dialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) 
					{
						dialog.dismiss();
						new deleteMessagesTask(position).execute(item.MsgId);
						
					}
				});
				dialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {								
						dialog.dismiss();
					}
				});
				dialog.show();
			}
		});
		

		/*
		if(_viewMode==8 || _viewMode==1){
			holder.received.setText(_activity.getResources().getString(R.string.you_received_a_message_from)+item.UserName);
		}
		*/
		
		holder.msgcount.setText(_activity.getResources().getString(R.string.msgs)+" ("+item.MsgCount+")");
		//to check from server!!!
		if(_viewMode==1){
			holder.status.setText(_activity.getResources().getString(R.string.read));
			holder.statusicon.setImageResource(R.drawable.folder_open);
		}
		else if(_viewMode==8){
			holder.status.setText(_activity.getResources().getString(R.string.commlocked));
			holder.statusicon.setImageResource(R.drawable.folder_close);
		}
		else if(_viewMode==2){
			holder.status.setText(_activity.getResources().getString(R.string.read));
			holder.statusicon.setImageResource(R.drawable.folder_open);
		}
		
		
		if(item.ProfileImg.contains("Images2/woman"))
        {
			item.ProfileImg = "drawable://" + R.drawable.private_for_woman;
        }
        else if(item.ProfileImg.contains("Images2/men.png"))
        {
        	item.ProfileImg = "drawable://" + R.drawable.private_for_men;
        }
        else
        {
        	item.ProfileImg = item.ProfileImg;
        }
		
	    Utils.getImageLoader(_activity).displayImage(item.ProfileImg, holder.userimg, Utils.getImageOptions());			    
					    
	    
	    
	    
	  
	    /*
	    holder.username.setAnimation(AnimationUtils.loadAnimation(_activity, R.anim.listview_anim));
	    holder.imageOval_yellow.setAnimation(AnimationUtils.loadAnimation(_activity, R.anim.listview_anim));
	    holder.sentdate.setAnimation(AnimationUtils.loadAnimation(_activity, R.anim.listview_anim));
	    holder.received.setAnimation(AnimationUtils.loadAnimation(_activity, R.anim.listview_anim));
	    holder.msgcount.setAnimation(AnimationUtils.loadAnimation(_activity, R.anim.listview_anim));
	    holder.status.setAnimation(AnimationUtils.loadAnimation(_activity, R.anim.listview_anim));
		holder.userimg.setAnimation(AnimationUtils.loadAnimation(_activity, R.anim.listview_anim));
		holder.statusicon.setAnimation(AnimationUtils.loadAnimation(_activity, R.anim.listview_anim));
		holder.trash.setAnimation(AnimationUtils.loadAnimation(_activity, R.anim.listview_anim));
		*/
		
	    
	    
	    
		return rowView;
	}
	
	
	
	
	
	class ViewHolder
	{
		TextView username,sentdate,msgcount/*,received*/,status;
		ImageView imageOval_yellow,userimg,statusicon,trash;
	}
	
	
	private int getViewString(){
		switch (_viewMode) {
			case 8:					
				return R.string.newmsg;
			case 1:
				return R.string.inboxmsg;
			case 2:
				return R.string.sentmsg;
			case 16:
				return R.string.trashmsg;
		
		}
		return R.string.newmsg;
	}
	

	class deleteMessagesTask extends AsyncTask<String,Void,String>
	{
		int _position;
		public deleteMessagesTask(int position)
		{
			this._position = position;
		}
		@Override
		protected void onPreExecute()
		{
			fragLoading.Show();
		}
		
		@Override
		protected String doInBackground(String... params) 
		{
			String result;
			result = ServiceClient.deleteMessagesInView(userid, String.valueOf(_viewMode),params[0] );
			return result;
		}
		
		@Override
		protected void onPostExecute(String res)
		{
			if(res.contains("ok"))
			{
				MessageInfoList.remove(_position);
				notifyDataSetChanged();
				fragLoading.Close();
				
				
				Panel.refresh();
				Controller.refreshCounters(_activity);
			}
		}
		
	}
	
	
	
	@SuppressLint("NewApi") 
	class getUserStateTask extends AsyncTask<Void, Integer, String>
	{
		String _userID;
		ImageView _imageOval_yellow;
		public getUserStateTask(String userID ,ImageView imageOval_yellow)
		{
			this._userID = userID;
			this._imageOval_yellow = imageOval_yellow;
		}
		@Override
		protected String doInBackground(Void... params)
		{
			String result = ServiceClient.getUserState(_userID);
			return result;
		}
	
		@Override
		protected void onPostExecute(String res)
		{
			if(res.contains("Online"))
			{
				_imageOval_yellow.setBackground(_activity.getResources().getDrawable(R.drawable.oval_yellow));
				_imageOval_yellow.startAnimation(animFadein);
			}
			else
			{
				_imageOval_yellow.setBackground(_activity.getResources().getDrawable(R.drawable.oval_red));
				_imageOval_yellow.clearAnimation();
			}
		}
	}
}

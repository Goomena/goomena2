package com.goomena.app;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;

import org.json.JSONException;
import org.json.JSONObject;

import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.FacebookError;
import com.facebook.android.AsyncFacebookRunner.RequestListener;
import com.facebook.android.Facebook.DialogListener;
import com.google.analytics.tracking.android.EasyTracker;
import com.goomena.app.R;
import com.goomena.app.ActMeet3.LoginTask;
import com.goomena.app.data.ProfileManager;
import com.goomena.app.data.ServiceClient;
import com.goomena.app.models.UserProfile;
import com.goomena.app.tools.Log;
import com.goomena.app.tools.Utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.InputType;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class ActLogin extends Activity
{
	//Login is relativeLayout5
	RelativeLayout relativeLayout1,relativeLayout2,relativeLayout3,relativeLayout4,relativeLayout5,relativeLayout6,relativeLayout7,relativeLayout8,relativeLayout9;
	EditText editEmail,editPass;
	ImageView onOff,imageEye;
	
	SharedPreferences settings;
	boolean rememberMe = true;
	boolean remember = false;
	boolean flag=false;
	
	ProgressBar progressBar;
	Dialog Dwaiting;
	
	// Your Facebook APP ID
	private static String APP_ID = "1458937241031502"; // Replace with your App ID
	
	// Instance of Facebook Class
	private Facebook facebook = new Facebook(APP_ID);
	private AsyncFacebookRunner mAsyncRunner;
	String FILENAME = "AndroidSSO_data";
	private SharedPreferences mPrefs;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_login);
		
		///-----+
		Dwaiting = new Dialog(ActLogin.this, R.style.DialogSlideAnim);
		Dwaiting.setContentView(R.layout.dialog_waiting);
		Animation animFadein = AnimationUtils.loadAnimation(ActLogin.this, R.anim.d_wait_fr_load);
		Animation animRotScal = AnimationUtils.loadAnimation(ActLogin.this, R.anim.d_wait_bg_logo);
		final TextView dialog_waiting_textWaiting = (TextView) Dwaiting.findViewById(R.id.dialog_waiting_textWaiting);
		ImageView dialog_waiting_bgLogo = (ImageView) Dwaiting.findViewById(R.id.dialog_waiting_bgLogo);
		dialog_waiting_textWaiting.startAnimation(animFadein);
		dialog_waiting_bgLogo.startAnimation(animRotScal);
		/////----
		
		editEmail = (EditText)findViewById(R.id.editEmail);
		editPass = (EditText)findViewById(R.id.editPass);
		onOff = (ImageView)findViewById(R.id.image_on_off);
		imageEye = (ImageView)findViewById(R.id.imageEye);
		relativeLayout4 = (RelativeLayout) findViewById(R.id.relativeLayout4);
		relativeLayout5 = (RelativeLayout) findViewById(R.id.relativeLayout5);
		relativeLayout7 = (RelativeLayout) findViewById(R.id.relativeLayout7);
		relativeLayout9 = (RelativeLayout) findViewById(R.id.relativeLayout9);
		
		editEmail.setOnClickListener(clickListener);
		editPass.setOnClickListener(clickListener);
		onOff.setOnClickListener(clickListener);
		relativeLayout5.setOnClickListener(clickListener);
		relativeLayout7.setOnClickListener(clickListener);
		relativeLayout9.setOnClickListener(clickListener);
		progressBar = (ProgressBar) findViewById(R.id.progressbar_Horizontal);
		
		settings = getSharedPreferences(Utils.SharedPrefsKey, 0);
		
		remember = settings.getBoolean("rememberMe",false);
		
		if(remember)
		{
			editEmail.setText(settings.getString("Email", ""));
			editPass.setText(settings.getString("Pass", ""));
		}
		

		imageEye.setOnTouchListener(new OnTouchListener() 
     	{
			public boolean onTouch(View v, MotionEvent event) 
			{
				// TODO Auto-generated method stub
				switch (event.getAction())
				{
					case MotionEvent.ACTION_DOWN: 
					{
						editPass.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
						break;
					}
					case MotionEvent.ACTION_UP:
					{
						editPass.setInputType(129);
					}
                }
                return true;
			}
        });
		
		mAsyncRunner = new AsyncFacebookRunner(facebook);
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) 
	{
		super.onActivityResult(requestCode, resultCode, data);
		facebook.authorizeCallback(requestCode, resultCode, data);
	}
	
	OnClickListener clickListener = new OnClickListener() 
	{
		@Override
		public void onClick(View v) 
		{
			if(v == editEmail)
			{
				//relativeLayout9.setVisibility(View.GONE);
			}
			else if(v == editPass)
			{

			}
			else if(v == onOff)
			{
				if(rememberMe)
				{
					onOff.setImageResource(R.drawable.l_off);
					rememberMe = false;
				}
				else
				{
					onOff.setImageResource(R.drawable.l_on);
					rememberMe = true;
				}
			}
			else if(v == relativeLayout5)
			{
				InputMethodManager imm = (InputMethodManager)getSystemService( Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(editEmail.getWindowToken(), 0);
				
				if(Utils.isNetworkConnected(ActLogin.this))
				{
					new LoginTask(editEmail.getText().toString(), editPass.getText().toString(),null,null,null).execute();
					new BackgroundAsyncTask().execute();
				}
				else
				{
					Utils.showNoInternet(ActLogin.this);
				}
			}
			else if(v == relativeLayout7)
			{
				dialogForgotPass();
			}
			else if(v == relativeLayout9)
			{
				showRateDialog();
			}
		}
	};
	
	class LoginTask extends AsyncTask<Void, Integer, UserProfile>
	{
		int myProgressCount;
		String _email,_password,_fbid,_fbname,_fbusername;
		public LoginTask(String email, String password, String fbid, String fbname, String fbusername)
		{
			this._email = email;
			this._password = password;
			this._fbid = fbid;
			this._fbname = fbname;
			this._fbusername = fbusername;
		}
		
		@Override
		protected void onPreExecute()
		{	
			Dwaiting.show();
		}
		
		@Override
		protected UserProfile doInBackground(Void... params) 
		{
			UserProfile result = ServiceClient.loginUser_v2(_email, _password, Utils.getLoginDetails(ActLogin.this),_fbid,_fbname,_fbusername);
			return result;
		}
		
		@SuppressLint("InlinedApi")
		@Override
		protected void onPostExecute(UserProfile result)
		{	
			if(result!=null && checkProfile(result))
			{				
				ProfileManager.saveProfile(ActLogin.this, result);
				if(rememberMe)
				{
					Editor edit = settings.edit();
					edit.putBoolean("rememberMe", true);
					edit.putString("Email", result.UserName);
					edit.putString("Pass", result.Password);
					edit.commit();
				}
				else
				{
					Editor edit = settings.edit();
					edit.putBoolean("rememberMe", false);
					edit.commit();
				}
				Intent intent = new Intent(ActLogin.this,ActHome.class);
		        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
		        ActLogin.this.startActivity(intent);
				
				//ActLogin.this.finish();
			}
			else
			{
				Log.v("APP","Error in login!");
				Toast.makeText(ActLogin.this, getResources().getString(R.string.msg_err_login), Toast.LENGTH_LONG).show();
			}
			Dwaiting.cancel();
			//imgLoading.setVisibility(View.GONE);
			//loadingAnimation.stop();
		}
		
		
	}
	
	@Override
	protected void onResume()
	{
		super.onResume();
		if(Utils.getSharedPreferences(this).getBoolean("isLoggedIn", false))
		{
			this.finish();
		}
	}
	
	private boolean checkProfile(UserProfile profile)
	{
		if(profile.ProfileId==null)
		{
			return false;
		}
		else if(profile.UserName==null)
		{
			return false;
		}
		return true;
	}
	
	@Override
	public void onStart() 
	{
		super.onStart();
		// The rest of your onStart() code.
	    EasyTracker.getInstance(this).activityStart(this);  // Add this method.
	}

	  
	@Override
	public void onStop()
	{
		super.onStop();
	    // The rest of your onStop() code.
	    EasyTracker.getInstance(this).activityStop(this);  // Add this method.
	}
	
	
	public class BackgroundAsyncTask extends AsyncTask<Void, Integer, Void> {

		int myProgressCount;

		@Override
		protected void onPreExecute() {
			progressBar.setProgress(0);
			myProgressCount = 0;
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			while (myProgressCount < 100) {
				myProgressCount++;
				/**
				 * Runs on the UI thread after publishProgress(Progress...) is
				 * invoked. The specified values are the values passed to
				 * publishProgress(Progress...).
				 * 
				 * Parameters values The values indicating progress.
				 */

				publishProgress(myProgressCount);
				SystemClock.sleep(10);
			}
			return null;
		}

		/**
		 * This method can be invoked from doInBackground(Params...) to publish
		 * updates on the UI thread while the background computation is still
		 * running. Each call to this method will trigger the execution of
		 * onProgressUpdate(Progress...) on the UI thread.
		 * 
		 * onProgressUpdate(Progress...) will not be called if the task has been
		 * canceled.
		 * 
		 * Parameters values The progress values to update the UI with.
		 */
		@Override
		protected void onProgressUpdate(Integer... values) {
			// TODO Auto-generated method stub
			progressBar.setProgress(values[0]);
		}

		@Override
		protected void onPostExecute(Void result) {
		}
	}
	
	
	public void dialogForgotPass()
	{
		final Dialog d = new Dialog(ActLogin.this, R.style.DialogSlideAnim);
		d.setContentView(R.layout.dialog_forgot_pass);
		d.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
		
		final EditText editForgotPass = (EditText) d.findViewById(R.id.editForgotPass);
		TextView TextSendPass = (TextView) d.findViewById(R.id.TextSendPass);

		TextSendPass.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(editForgotPass.getText().toString().equals(""))
				{
					Toast.makeText(ActLogin.this, getResources().getString(R.string.username_or_email_required), Toast.LENGTH_LONG).show();
				}
				else
				{
					new ForgorPassAsyncTask().execute(editForgotPass.getText().toString());
				}
				
				d.cancel();
			}
		});
		
		
		d.show();
	}
	
	
	
	public class ForgorPassAsyncTask extends AsyncTask<String, Void, String> 
	{
		@Override
		protected void onPreExecute() {
			
		}

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			return ServiceClient.SendForgotPass(params[0]);
			
		}

		@Override
		protected void onPostExecute(String result) 
		{
			if(result.contains("-ok-"))
			{
				Toast.makeText(ActLogin.this, getResources().getString(R.string.message_sent_to_your_email_containing), Toast.LENGTH_LONG).show();
			}
			else
			{
				Toast.makeText(ActLogin.this, getResources().getString(R.string.email_or_username_not_found), Toast.LENGTH_LONG).show();
			}
		}
	}
	
	
	
	 public void showRateDialog() 
	 {
        final Dialog dialog = new Dialog(ActLogin.this,R.style.DialogSlideAnim);
        dialog.setContentView(R.layout.dialog_login);
        
        TextView TextFacebook = (TextView) dialog.findViewById(R.id.TextFacebook);
        TextView TextGoogle = (TextView) dialog.findViewById(R.id.TextGoogle);
        TextView TextMail = (TextView) dialog.findViewById(R.id.TextMail);
        
        TextFacebook.setOnClickListener(new OnClickListener() {
            public void onClick(View v) 
            {
            	loginToFacebook();
                dialog.dismiss();
            }
        });        
      
        TextGoogle.setOnClickListener(new OnClickListener() {
            public void onClick(View v) 
            {
                dialog.dismiss();
                Toast.makeText(ActLogin.this, getResources().getString(R.string.under_construction), Toast.LENGTH_LONG).show();
            }
        });
        
        TextMail.setOnClickListener(new OnClickListener() {
            public void onClick(View v) 
            {   
                dialog.dismiss();
                Toast.makeText(ActLogin.this, getResources().getString(R.string.insert_email_username_password_here), Toast.LENGTH_LONG).show();
            }
        });     
        dialog.show();        
    }
	 
	 
	 
	 /**
	 * Function to login into facebook
	 * */
	@SuppressWarnings("deprecation")
	public void loginToFacebook() 
	{
		mPrefs = getPreferences(MODE_PRIVATE);
		String access_token = mPrefs.getString("access_token", null);
		long expires = mPrefs.getLong("access_expires", 0);

		if (access_token != null) 
		{
			facebook.setAccessToken(access_token);
			Log.d("FB Sessions", "" + facebook.isSessionValid());
		}

		if (expires != 0) 
		{
			facebook.setAccessExpires(expires);
		}

		if (!facebook.isSessionValid()) 
		{
			facebook.authorize(this, new String[] { "email", "publish_stream" }, new DialogListener() 
			{
				@Override
				public void onCancel() { }

				@Override
				public void onComplete(Bundle values) 
				{
					getProfileInformation();
				}
				@Override
				public void onError(DialogError error) { }

				@Override
				public void onFacebookError(FacebookError fberror) { }
			});
		}
	}
	
	/**
	 * Get Profile information by making request to Facebook Graph API
	 * */
	@SuppressWarnings("deprecation")
	public void getProfileInformation() 
	{
		mAsyncRunner.request("me", new RequestListener() 
		{
			@SuppressWarnings("unused")
			@Override
			public void onComplete(String response, Object state) 
			{
				Log.d("Profile", response);
				String json = response;
				try 
				{
					JSONObject profile = new JSONObject(response);
					final String email = profile.getString("email");
					final String id = profile.getString("id");
					final String name = profile.getString("name");
					
					runOnUiThread(new Runnable() 
					{
						@Override
						public void run() 
						{
							new LoginTask(email,null,id,name,null).execute();
						}

					});
				} 
				catch (JSONException e) 
				{
					e.printStackTrace();
				}
			}

			@Override
			public void onIOException(IOException e, Object state) { }
			@Override
			public void onFileNotFoundException(FileNotFoundException e, Object state) { }
			@Override
			public void onMalformedURLException(MalformedURLException e, Object state) { }
			@Override
			public void onFacebookError(FacebookError e, Object state) { }
		});
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
	    super.onConfigurationChanged(newConfig);


	    // Checks whether a hardware keyboard is available
	    if (newConfig.hardKeyboardHidden == Configuration.HARDKEYBOARDHIDDEN_NO) {
	        Toast.makeText(this, "keyboard visible", Toast.LENGTH_SHORT).show();
	    } else if (newConfig.hardKeyboardHidden == Configuration.HARDKEYBOARDHIDDEN_YES) {
	        Toast.makeText(this, "keyboard hidden", Toast.LENGTH_SHORT).show();
	    }
	}

}

package com.goomena.app;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import com.google.android.gms.maps.model.LatLng;
import com.goomena.app.R;
import com.goomena.app.data.DataFetcher;
import com.goomena.app.data.ProfileManager;
import com.goomena.app.data.ServiceClient;
import com.goomena.app.models.Country;
import com.goomena.app.models.CountryList;
import com.goomena.app.models.UserProfile;
import com.goomena.app.tools.GPSTracker;
import com.goomena.app.tools.Log;
import com.goomena.app.tools.Utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class ActRegister3 extends Activity
{
	private String la1 = null;
	private String lo2 = null;
	
	GPSTracker gps;
	
	ImageView imgMale, imgFemale;
	TextView text_i_am_male,text_i_am_female;
	boolean isMale = true;
	boolean isFemale = false;
	RelativeLayout relativeLayout1,relativeLayout2,relativeLayout3,relativeLayout4,relativeLayout6,relativeLayout10;
	RelativeLayout relativeLayout31,relativeLayout32;
	
	LinearLayout relativeLayout5;
	Spinner spinnerCountry,spinnerRegion,spinnerCity;
	String email,username,password,confirmPass,birthday, region,country,city,lag,gender;
	HashMap<String, String> mapCountries;
	ArrayList<String> countries,regions,cities;
	CountryList countryList;
	
	public static final int Date_dialog_id = 1;
	// date and time
	private int mYear;
	private int mMonth;
	private int mDay;
	
	Calendar c = null;
	
	TextView textday,textmonth,textyear;
	
	
	String fbMail,fbId,fbName,fbUsername,fbGender;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_register3);
		
		gps = new GPSTracker(ActRegister3.this);
		
		
		
		relativeLayout5 = (LinearLayout) findViewById(R.id.relativeLayout5);
     	relativeLayout10 = (RelativeLayout) findViewById(R.id.relativeLayout10);
     	
     	relativeLayout31 = (RelativeLayout) findViewById(R.id.relativeLayout31);
     	relativeLayout32 = (RelativeLayout) findViewById(R.id.relativeLayout32);
     	
     	textday = (TextView) findViewById(R.id.textday);
     	textmonth = (TextView) findViewById(R.id.textmonth);
     	textyear = (TextView) findViewById(R.id.textyear);
     	
     	
     	imgMale = (ImageView) findViewById(R.id.check1);
		imgFemale = (ImageView) findViewById(R.id.check2);
		text_i_am_male = (TextView) findViewById(R.id.text_i_am_male);
		text_i_am_female = (TextView) findViewById(R.id.text_i_am_female);
		
		spinnerCountry = (Spinner)findViewById(R.id.spin1);
		spinnerRegion = (Spinner)findViewById(R.id.spin2);
		spinnerCity = (Spinner)findViewById(R.id.spin3);
     	
     	relativeLayout10.setOnClickListener(clickListener);
     	relativeLayout31.setOnClickListener(clickListener);
     	relativeLayout32.setOnClickListener(clickListener);
     	relativeLayout5.setOnClickListener(clickListener);
     	
     	
     	initUi();
     	
     	
     	//actualizarPosicion();
     	
     	
     	
     	
     	
     	
     	
     	if(Utils.isNetworkConnected(this))
     	{
			new getCountriesTask().execute();
		}
		else
		{
			Utils.showNoInternet(this);
		}
     	
     	
				
		
            
       c = Calendar.getInstance();
       mYear = c.get(Calendar.YEAR);
       mMonth = c.get(Calendar.MONTH);
       mDay = c.get(Calendar.DAY_OF_MONTH);

    		//updateDisplay();
       
       Intent intent = getIntent();
       fbMail = intent.getStringExtra("fbMail");
       fbId = intent.getStringExtra("fbId");
       fbName = intent.getStringExtra("fbName");
       fbUsername = intent.getStringExtra("fbUsername");
       fbGender = intent.getStringExtra("fbGender");
       
       /*
       if(fbGender.contains("male"))
       {
    	   if(!isMale)
			{
				imgMale.setImageResource(R.drawable.l_male_icon_enable);
				text_i_am_male.setTextColor(getResources().getColor(R.color.mRed));
				isMale = true;
				imgFemale.setImageResource(R.drawable.l_female_icon);
				text_i_am_female.setTextColor(getResources().getColor(R.color.white));
				isFemale = false;
			}
       }
       else
       {
    	   if(!isFemale)
			{
				imgFemale.setImageResource(R.drawable.l_female_enable);
				text_i_am_female.setTextColor(getResources().getColor(R.color.mRed));
				isFemale = true;
				imgMale.setImageResource(R.drawable.l_male_icon);
				text_i_am_male.setTextColor(getResources().getColor(R.color.white));
				isMale = false;
			}
       }
       */

	}
	
	private void initUi()
	{
		initSpinners();		
		relativeLayout10.setOnClickListener(new OnClickListener() 
		{
			
			@Override
			public void onClick(View v) {
				
				email = GlobalRegister.Email;
				username = GlobalRegister.UserName;
				password = GlobalRegister.Pass;
				
				
				new registerTask().execute();				
			}
		});
	}
	
	
	
	class registerTask extends AsyncTask<Void,Void,String>
	{
		@Override
		protected void onPreExecute()
		{
			//loadingAnimation.start();
			//imgLoading.setVisibility(View.VISIBLE);
		}
		
		@Override
		protected String doInBackground(Void... params) 
		{	
			if (validateDate(c.get(Calendar.YEAR), c.get(Calendar.MONTH)+1,c.get(Calendar.DAY_OF_MONTH)))
			{
				if(isMale)
				{
					gender = "1";
				}
				else if(isFemale)
				{
					gender = "2";
				}
				DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd", Locale.US);
				birthday = dateFormat.format(c.getTime());
				//Log.v("APP","birthday selected: "+birthday);
					
				lag = "GR";
					
								
				username = username.replace("\t\r\n", "").replace("\n", "").replace(" ", "");
				//Log.v("APP","aiaiai ="+username);
						
				Log.v("APP", "ayayay "+birthday);
								
				String countryIso = mapCountries.get( spinnerCountry.getSelectedItem() );
				String result = DataFetcher.postRegisterData(email, username, password.trim(), birthday, gender, region,countryIso, city, lag,fbId,fbName,fbUsername);
				
				
				Log.v("APP", "ayayay "+result);
					
				return result;
			}
			else 
			{
				return "5";
			}
		}
		
		@Override
		protected void onPostExecute(String res)
		{
			//imgLoading.setVisibility(View.GONE);
			//loadingAnimation.stop();
			if(res.contains("ok") && res.contains("EmailAlready")){
				Toast.makeText(ActRegister3.this, getResources().getString(R.string.emailalready), Toast.LENGTH_LONG).show();
			}
			else if(res.contains("ok") && res.contains("LoginNameAlready")){
				Toast.makeText(ActRegister3.this, getResources().getString(R.string.loginalready), Toast.LENGTH_LONG).show();
			}
			else if(res.contains("ok")){
				new LoginTask(email,password,fbId,fbName,fbUsername).execute();
			}
			else if(res.equals("5")){
				Toast.makeText(ActRegister3.this, getResources().getString(R.string.anilikos), Toast.LENGTH_LONG).show();

			}
			else{
				Toast.makeText(ActRegister3.this, "An error occured!", Toast.LENGTH_LONG).show();
			}
		}
		}
	
	class LoginTask extends AsyncTask<Void, Void, UserProfile>{
		String _email,_password,_fbid,_fbname,_fbusername;
		public LoginTask(String email, String password, String fbid, String fbname, String fbusername){
			this._email = email;
			this._password = password;
			this._fbid = fbid;
			this._fbname = fbname;
			this._fbusername = fbusername;
		}
		
		@Override
		protected void onPreExecute(){
			//loadingAnimation.start();
			//imgLoading.setVisibility(View.VISIBLE);
		}
		
		@Override
		protected UserProfile doInBackground(Void... params) {
			UserProfile result = ServiceClient.loginUser_v2(_email, _password, Utils.getLoginDetails(ActRegister3.this),_fbid,_fbname,_fbusername);			
			return result;
		}
		
		@Override
		protected void onPostExecute(UserProfile result){
			if(result!=null && checkProfile(result))
			{				
				ProfileManager.saveProfile(ActRegister3.this, result);	
				SharedPreferences.Editor editor = Utils.getSharedPreferences(ActRegister3.this).edit();
				editor.putString("Email", result.UserName);
				editor.putString("Pass", result.Password);
				editor.commit();
				startActivity(new Intent(ActRegister3.this,ActRegister35.class));
				ActRegister3.this.finish();
			}
			else{
				Log.v("APP","Error in login!");
				Toast.makeText(ActRegister3.this, getResources().getString(R.string.msg_err_login), Toast.LENGTH_LONG).show();
			}
			//imgLoading.setVisibility(View.GONE);
			//loadingAnimation.stop();
		}
		
	}
	
	private boolean checkProfile(UserProfile profile){
		if(profile.ProfileId==null){
			return false;
		}
		else if(profile.UserName==null){
			return false;
		}
		return true;
	}
	
	@Override
	@Deprecated
	protected void onPrepareDialog(int id, Dialog dialog) {
		// TODO Auto-generated method stub
		super.onPrepareDialog(id, dialog);

		((DatePickerDialog) dialog).updateDate(mYear, mMonth, mDay);

	}

	private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {

		public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
			mYear = year;
			mMonth = monthOfYear;
			mDay = dayOfMonth;
			updateDisplay();
		}
	};

	private void updateDisplay() 
	{
		// TODO Auto-generated method stub
		
		textday.setText(""+mDay);
		textmonth.setText(""+(mMonth + 1));
		textyear.setText(""+mYear);
		
		if (!validateDate(mYear, mMonth + 1,mDay))
		{
			Toast.makeText(ActRegister3.this, getResources().getString(R.string.anilikos), Toast.LENGTH_LONG).show();
		}
		c.set(mYear, mMonth, mDay);
	}
	 
	
	private void initSpinners(){
		spinnerCountry = (Spinner)findViewById(R.id.spin1);
		spinnerRegion = (Spinner)findViewById(R.id.spin2);
		spinnerCity = (Spinner)findViewById(R.id.spin3);
		
		spinnerCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int index, long arg3) {
				if(countries!=null){
					country = countries.get(index);
					new getRegionsTask().execute();
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {				
			}
		});
		
		spinnerRegion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int index, long arg3) {
				if(regions!=null){
					region = regions.get(index);
					new getCitiesTask().execute();
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
			}
			
		});
		
		spinnerCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int index, long arg3) {
				if(cities!=null)
					city = cities.get(index);
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
			}
			
		});
	}
	
	
	OnClickListener clickListener = new OnClickListener() 
	{
		@SuppressLint("InlinedApi") 
		@Override
		public void onClick(View v) 
		{
			
			if(v == relativeLayout10)
			{
				Intent intent = new Intent();
				intent.setClass(ActRegister3.this, ActRegister4.class);
				startActivity(intent);
				overridePendingTransition(R.anim.right_in, R.anim.left_out);
			}
			else if(v == relativeLayout5)
			{
				DatePickerDialog dpd = new DatePickerDialog(ActRegister3.this,android.R.style.Theme_Holo_Dialog, mDateSetListener, mYear, mMonth, mDay);
				dpd.show();
			}
			else if(v == relativeLayout31)
			{
				if(!isMale)
				{
					imgMale.setImageResource(R.drawable.l_male_icon_enable);
					text_i_am_male.setTextColor(getResources().getColor(R.color.mRed));
					isMale = true;
					imgFemale.setImageResource(R.drawable.l_female_icon);
					text_i_am_female.setTextColor(getResources().getColor(R.color.white));
					isFemale = false;
				}
			}
			else if(v == relativeLayout32)
			{
				if(!isFemale)
				{
					imgFemale.setImageResource(R.drawable.l_female_enable);
					text_i_am_female.setTextColor(getResources().getColor(R.color.mRed));
					isFemale = true;
					imgMale.setImageResource(R.drawable.l_male_icon);
					text_i_am_male.setTextColor(getResources().getColor(R.color.white));
					isMale = false;
				}
			}
		}
	};
	
	private boolean validateDate(int YEAR, int month, int day)
	{
		Calendar c = Calendar.getInstance(); 
		int xronia = c.get(Calendar.YEAR) - 18;
		int mera = c.get(Calendar.DAY_OF_MONTH)-1;
		int mhnas = c.get(Calendar.MONTH)+1;
		
		if(YEAR < xronia)
		{
			return true;
		}
		else if(YEAR == xronia)
		{
			if(month < mhnas)
			{
				return true;
			}
			else if(month == mhnas)
			{
				if(day < mera)
				{
					return true;
				}
				else if(day == mera)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	 
	List<Address> addresses;
	class getCountriesTask extends AsyncTask<Void,Void,Void>
	{

		@Override
		protected Void doInBackground(Void... params) {
			countryList = ServiceClient.getCountries();
			region = "";
			city = "";
			if(countryList!=null)
			{
				mapCountries = new HashMap<String, String>();
				countries = new ArrayList<String>();
				
				
				 Log.v("APP", "dfdfdfdferer " +gps.getLatitude() +" "+ gps.getLongitude());
				
				Geocoder geoCoder = new Geocoder(getBaseContext(), Locale.getDefault());
				try 
				{
				  addresses = geoCoder.getFromLocation(gps.getLatitude(), gps.getLongitude(), 1);

				  
				   
				   
				   Log.v("APP", "dfdfdfdferer " +addresses.get(0).getLocality());
				   
				   Log.v("APP", "dfdfdfdferer sss " +addresses.get(0).getCountryCode());
				   
				   
				   mapCountries.put(addresses.get(0).getCountryName(), addresses.get(0).getCountryCode());
				   countries.add(addresses.get(0).getCountryName());
				}
				catch (Exception e1) 
				{
					Log.v("APP", "dfdfdfdferer NOP" );
					mapCountries.put("Greece", "GR");
					countries.add("Greece");
					e1.printStackTrace(); 
				}
				
				
				for(Country c:countryList.countries)
				{
					mapCountries.put(c.name, c.iso);
					countries.add(c.name);
				}
				
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(Void p){
			if(countryList!=null){
				ArrayAdapter<String> adapter = new ArrayAdapter<String>(ActRegister3.this, android.R.layout.simple_spinner_dropdown_item, countries);
				spinnerCountry.setAdapter(adapter);
				spinnerCountry.setSelection(0); //20 = Greece
				
				
			
				new getRegionsTask().execute();
				
			}
		}
		
	}
	class getRegionsTask extends AsyncTask<Void,Void,Void>{

		@Override
		protected Void doInBackground(Void... params) {
			String countryIso = mapCountries.get( spinnerCountry.getSelectedItem() );
			String lagid = "EN";
			if(countryIso.equals("GR"))
				lagid = "GR";
			regions = ServiceClient.getCountryRegions(countryIso, lagid);
			
			return null;
		}
		
		@Override
		protected void onPostExecute(Void p){
			if(regions!=null){
				ArrayAdapter<String> adapter = new ArrayAdapter<String>(ActRegister3.this, android.R.layout.simple_spinner_dropdown_item, regions);
				spinnerRegion.setAdapter(adapter);
				spinnerRegion.setSelection(0);
				
			}
			city = "";
			new getCitiesTask().execute();
		}		
	}
	class getCitiesTask extends AsyncTask<Void,Void,Void>{

		@Override
		protected Void doInBackground(Void... params) {
			String countryIso = mapCountries.get( spinnerCountry.getSelectedItem() );
			String lagid = "EN";
			if(countryIso.equals("GR"))
				lagid = "GR";
			region = region.replace(" ", "%20");
			cities = ServiceClient.getRegionCities(countryIso, region,lagid);
			
			region = region.replace("%20", " ");
			return null;
		}
		
		@Override
		protected void onPostExecute(Void p){
			if(cities!=null){
				ArrayAdapter<String> adapter = new ArrayAdapter<String>(ActRegister3.this, android.R.layout.simple_spinner_dropdown_item, cities);
				spinnerCity.setAdapter(adapter);
				if(cities.size() >= 17)
				{
					spinnerCity.setSelection(17);
				}
				else
				{
					spinnerCity.setSelection(0);
				}
				
			}
		}
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	 private String provider;
	
	private void actualizarPosicion()
	{
		
		 try {
		        LocationManager locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
		       // boolean gps_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
		      //LocationManager locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
		        Criteria criteria = new Criteria();
		        provider = locationManager.getBestProvider(criteria, false);
		        Location location = locationManager.getLastKnownLocation(provider);
		       // muestraPosicion(location);
		        LocationListener locationListener = new LocationListener() 
		        {
		        	public void onLocationChanged(Location location) 
		        	{
		        		 muestraPosicion(location);
		        	}
		        	public void onProviderDisabled(String provider){}
		        	public void onProviderEnabled(String provider){}
		        	public void onStatusChanged(String provider, int status, Bundle extras){}
		        };
		        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
		    } catch (Exception e) {

		    }
		
	    

	   
	}
	
	
	
	
	
	private void muestraPosicion(Location loc)
	{
	   if(loc != null) 
	   {
		   
	      la1 = String.valueOf(loc.getLatitude());
	      lo2 = String.valueOf(loc.getLongitude());   
	      Log.e("gps", la1+" "+lo2); 
	      
	      Log.v("APP","sdsdsdc e "+ la1+" "+lo2);
	     
	    } 
	   else 
	   {
		   Log.v("APP","sdsdsdc r "+ la1+" "+lo2);
			 startService();
	    }
	} 
	
	
	
	public boolean startService() 
	{
	   try 
	   {
	       FetchCordinates fetchCordinates = new FetchCordinates();
	       fetchCordinates.execute();
	         return true;
	   } 
	   catch (Exception error) 
	   {
	    return false;
	   }
	}
	
	
	public double lati = 0.0;
	public double longi = 0.0;
	public class FetchCordinates extends AsyncTask<String, Integer, String> 
	{
		
		public LocationManager mLocationManager;
		public VeggsterLocationListener mVeggsterLocationListener;
		@Override
		protected void onPreExecute()
		{
			mVeggsterLocationListener = new VeggsterLocationListener();
			mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
			mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, mVeggsterLocationListener);
		}
			
		@Override
		protected String doInBackground(String... params) 
		{
			while (lati == 0.0) 
			{}
			return null;
		}
	
		public class VeggsterLocationListener implements LocationListener 
		{
			@Override
			public void onLocationChanged(Location location) 
			{
				try 
				{
					lati = location.getLatitude();
					longi = location.getLongitude();                
					la1=""+lati;
					lo2=""+longi;
					Log.e("wifi or 3g", la1+" "+lo2); 

				} 
				catch (Exception e) 
				{
					Log.e("error wifi or 3g",""+e);
				}
				
				
				
				
				Geocoder geoCoder = new Geocoder(getBaseContext(), Locale.getDefault());
				try {
				   List<Address> addresses = geoCoder.getFromLocation(lati, longi, 1);

				  
				   
				   
				   Log.v("APP", "dfdfdfdferer " +addresses.get(0).getLocality());
				   
				   Log.v("APP", "dfdfdfdferer sss " +addresses.get(0).getCountryCode());
				}
				catch (Exception e1) {                
				   e1.printStackTrace();
				}   
				
				
				
				
				
			}
	     
			@Override
			public void onProviderDisabled(String provider) 
			{
				Log.i("OnProviderDisabled", "OnProviderDisabled "+provider);
			}
	     
			@Override
			public void onProviderEnabled(String provider) 
			{
				Log.i("onProviderEnabled", "onProviderEnabled "+provider);
			}
			@Override
			public void onStatusChanged(String provider, int status, Bundle extras) 
			{
				Log.i("onStatusChanged", "onStatusChanged "+provider);
			}
		}
	}

}
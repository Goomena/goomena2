package com.goomena.app;

import java.util.ArrayList;

import com.goomena.app.data.ServiceClient;
import com.goomena.app.global.Global;
import com.goomena.app.global.GlobalLikes;
import com.goomena.app.global.GlobalMessages;
import com.goomena.app.global.GlobalOffers;
import com.goomena.app.models.Counters;
import com.goomena.app.models.Offer;
import com.goomena.app.models.OfferList;
import com.goomena.app.tools.Log;
import com.goomena.app.tools.Utils;
import com.qs.samsungbadger.Badge;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class Controller 
{
	
	
	static TextView new_menuItemTextMessages;
	static TextView new_menuItemTextLikes;
	static TextView panel_adaptes_textLikesCount;
	static TextView new_menuItemTextBids;
	static TextView panel_adaptes_textOffersCount;
	static TextView new_menuItemTextCredits;
	
	static TextView action_bar_text_numMessages;
	static ImageView action_bar_image_numMessages;
	static TextView action_bar_text_notifications;
	static ImageView action_bar_image_notifications;
	
	public static int numNotification = 0;
	
	static Handler	handlerCounters;
	
	
	public Controller(Activity activity)
	{
		//this._activity = activity;
	}
	

	
	
	/*
	public static void refreshMessages(Activity activity)
	{
		action_bar_text_numMessages = (TextView) activity.findViewById(R.id.action_bar_text_numMessages);
		action_bar_image_numMessages = (ImageView) activity.findViewById(R.id.action_bar_image_numMessages);
		new_menuItemTextMessages = (TextView) activity.findViewById(R.id.new_menuItemTextMessages);
		
		new getMessagesCountTask(activity,action_bar_text_numMessages,action_bar_image_numMessages,new_menuItemTextMessages).execute();
	}
	*/
	
	/*
	public static void refreshLikes(Activity activity)
	{
		new_menuItemTextLikes = (TextView) activity.findViewById(R.id.new_menuItemTextLikes);
		
		new getLikesCountTask(activity,new_menuItemTextLikes).execute();
	}
	*/
	
	/*
	public static void refreshOffers(Activity activity)
	{
		new_menuItemTextBids = (TextView) activity.findViewById(R.id.new_menuItemTextBids);
		
		new getOffersCountTask(activity,new_menuItemTextBids).execute();
	}
	*/
	
	/*
	public static void refreshNotification(Activity activity)
	{
		action_bar_text_notifications = (TextView) activity.findViewById(R.id.action_bar_text_notifications);
		action_bar_image_notifications = (ImageView) activity.findViewById(R.id.action_bar_image_notifications);
		
		if(numNotification==0)
		{
			action_bar_text_notifications.setVisibility(View.GONE);
			action_bar_image_notifications.setVisibility(View.GONE);
		}
		else
		{
			action_bar_text_notifications.setText(""+numNotification);
		}
	}
	*/

	
	public static void refreshWithHandler()
	{
		try
		{
			Message msg = Controller.handlerCounters.obtainMessage();
			msg.obj = "";
			Controller.handlerCounters.sendMessage(msg);
		}
		catch (Exception e) {}
	}
	
	public static void refreshCounters(Activity activity)
	{
		action_bar_text_numMessages = (TextView) activity.findViewById(R.id.action_bar_text_numMessages);
		action_bar_image_numMessages = (ImageView) activity.findViewById(R.id.action_bar_image_numMessages);
		new_menuItemTextMessages = (TextView) activity.findViewById(R.id.new_menuItemTextMessages);
		
		new_menuItemTextLikes = (TextView) activity.findViewById(R.id.new_menuItemTextLikes);
		panel_adaptes_textLikesCount = (TextView) activity.findViewById(R.id.panel_adaptes_textLikesCount);
		
		new_menuItemTextBids = (TextView) activity.findViewById(R.id.new_menuItemTextBids);
		panel_adaptes_textOffersCount = (TextView) activity.findViewById(R.id.panel_adaptes_textOffersCount);
		
		action_bar_text_notifications = (TextView) activity.findViewById(R.id.action_bar_text_notifications);
		action_bar_image_notifications = (ImageView) activity.findViewById(R.id.action_bar_image_notifications);
		
		new getCounters(activity,action_bar_text_numMessages,action_bar_image_numMessages,new_menuItemTextMessages,new_menuItemTextLikes,panel_adaptes_textLikesCount,new_menuItemTextBids,panel_adaptes_textOffersCount,action_bar_text_notifications,action_bar_image_notifications).execute();
	}
	
	public static void refreshCredits(Activity activity)
	{
		new_menuItemTextCredits = (TextView) activity.findViewById(R.id.new_menuItemTextCredits);
		
		new getCredits(activity,new_menuItemTextCredits).execute();
	}
	
	
	

}







/*
class getMessagesCountTask extends AsyncTask<String,Void,String>
{
	TextView _textMessages;
	TextView _action_bar_textMessages;
	ImageView _action_bar_image_numMessages;
	Activity _activity;
	public getMessagesCountTask(Activity activity,TextView action_bar_textMessages,ImageView action_bar_image_numMessages,TextView textMessages)
	{
		this._action_bar_textMessages = action_bar_textMessages;
		this._action_bar_image_numMessages = action_bar_image_numMessages;
		this._textMessages = textMessages;
		this._activity = activity;
	}
	@Override
	protected String doInBackground(String... params) 
	{
		String result = ServiceClient.getNumberMessages(Utils.getSharedPreferences(_activity).getString("userid",""));		
		return result;
	}
	
	@Override
	protected void onPostExecute(String result)
	{
		if(!result.equals(""))
		{
			if(result.equals("0"))
			{
				_textMessages.setVisibility(View.GONE);
				_action_bar_textMessages.setVisibility(View.GONE);
				_action_bar_image_numMessages.setVisibility(View.GONE);
			}
			else
			{
				_textMessages.setText(result);
				_action_bar_textMessages.setText(result);
			}
		}
	}
}

*/
/*
class getLikesCountTask extends AsyncTask<String,Void,String>
{
	TextView _textLikes;
	Activity _activity;
	public getLikesCountTask(Activity activity,TextView textLikes)
	{
		this._textLikes = textLikes;
		this._activity = activity;
	}
	@Override
	protected String doInBackground(String... params) 
	{
		
		String result = ServiceClient.getNumberLikes(Utils.getSharedPreferences(_activity).getString("userid",""));
			
		return result;
	}
	
	@Override
	protected void onPostExecute(String result)
	{
		if(!result.equals(""))
		{
			if(result.equals("0"))
			{
				_textLikes.setVisibility(View.GONE);
			}
			else
			{
				_textLikes.setText(result);
			}
			
			Controller.numNotification = Integer.parseInt(result);
		}
	}
}
*/

/*
class getOffersCountTask extends AsyncTask<Void,Void,OfferList>
{
	TextView _textOffers;
	Activity _activity;
	getOffersCountTask(Activity activity,TextView textOffers)
	{
		this._activity = activity;
		this._textOffers = textOffers;
	}
	
	SharedPreferences settings = Utils.getSharedPreferences(_activity);
	String userid = settings.getString("userid", "");
	String genderid = settings.getString("genderid", "");
	String zip = settings.getString("zipcode", "");
	String lat = settings.getString("lat", "");
	String lng = settings.getString("lng", "");
	
	@Override
	protected void onPreExecute()
	{}
	
	@Override
	protected OfferList doInBackground(Void... params) 
	{
		return ServiceClient.getOffers(String.valueOf(0), userid, zip, lat, lng,"0");
	}

	@Override
	protected void onPostExecute(OfferList result)
	{
		if(result!=null &&  result.offers!=null)
		{
			if(result.offers.size()==0)
			{
				_textOffers.setVisibility(View.GONE);
			}
			else
			{
				_textOffers.setText(""+result.offers.size());
			}
			
			Controller.numNotification += result.offers.size();
			Controller.refreshNotification(_activity);
		}
	}
}

*/



class getCounters extends AsyncTask<Void,Void,Counters>
{
	Activity _activity;
	
	TextView _action_bar_text_numMessages;
	ImageView _action_bar_image_numMessages;
	TextView _new_menuItemTextMessages;
	
	TextView _new_menuItemTextLikes;
	TextView _panel_adaptes_textLikesCount;
	
	TextView _new_menuItemTextBids;
	TextView _panel_adaptes_textOffersCount;
	
	TextView _action_bar_text_notifications;
	ImageView _action_bar_image_notifications;
	
	public getCounters(Activity activity,TextView action_bar_text_numMessages,ImageView action_bar_image_numMessages,TextView new_menuItemTextMessages,TextView new_menuItemTextLikes,TextView panel_adaptes_textLikesCount,TextView new_menuItemTextBids,TextView panel_adaptes_textOffersCount,TextView action_bar_text_notifications,ImageView action_bar_image_notifications)
	{
		this._activity = activity;
		
		this._action_bar_text_numMessages = action_bar_text_numMessages;
		this._action_bar_image_numMessages = action_bar_image_numMessages;
		this._new_menuItemTextMessages = new_menuItemTextMessages;
		
		this._new_menuItemTextLikes = new_menuItemTextLikes;
		this._panel_adaptes_textLikesCount = panel_adaptes_textLikesCount;
		
		this._new_menuItemTextBids = new_menuItemTextBids;
		this._panel_adaptes_textOffersCount = panel_adaptes_textOffersCount;
		
		this._action_bar_text_notifications = action_bar_text_notifications;
		this._action_bar_image_notifications = action_bar_image_notifications;
	}
	@Override
	protected Counters doInBackground(Void... params) 
	{
		Counters counters;
		counters = ServiceClient.getCounters(Utils.getSharedPreferences(_activity).getString("userid",""));
		return counters;
	}
	
	@Override
	protected void onPostExecute(Counters NCounters)
	{
		if(NCounters!=null)
		{
			if(NCounters.NewMessages.equals("0"))
			{
				_new_menuItemTextMessages.setVisibility(View.GONE);
				_action_bar_text_numMessages.setVisibility(View.GONE);
				_action_bar_image_numMessages.setVisibility(View.GONE);
			}
			else
			{
				_new_menuItemTextMessages.setVisibility(View.VISIBLE);
				_action_bar_text_numMessages.setVisibility(View.VISIBLE);
				_action_bar_image_numMessages.setVisibility(View.VISIBLE);
				
				_new_menuItemTextMessages.setText(NCounters.NewMessages);
				_action_bar_text_numMessages.setText(NCounters.NewMessages);
			}
			
			if(NCounters.NewLikes.equals("0"))
			{
				_new_menuItemTextLikes.setVisibility(View.GONE);
				_panel_adaptes_textLikesCount.setVisibility(View.GONE);
			}
			else
			{
				_new_menuItemTextLikes.setVisibility(View.VISIBLE);
				_panel_adaptes_textLikesCount.setVisibility(View.VISIBLE);
				
				_new_menuItemTextLikes.setText(NCounters.NewLikes);
				_panel_adaptes_textLikesCount.setText(NCounters.NewLikes);
			}
			
			if(NCounters.NewOffers.equals("0"))
			{
				_new_menuItemTextBids.setVisibility(View.GONE);
				_panel_adaptes_textOffersCount.setVisibility(View.GONE);
			}
			else
			{
				_new_menuItemTextBids.setVisibility(View.VISIBLE);
				_panel_adaptes_textOffersCount.setVisibility(View.VISIBLE);
				
				_new_menuItemTextBids.setText(NCounters.NewOffers);
				_panel_adaptes_textOffersCount.setText(NCounters.NewOffers);
			}
			
			int numNotification = Integer.parseInt(NCounters.NewLikes)+Integer.parseInt(NCounters.NewOffers);
			
			if(numNotification==0)
			{
				_action_bar_text_notifications.setVisibility(View.GONE);
				_action_bar_image_notifications.setVisibility(View.GONE);
			}
			else
			{
				_action_bar_text_notifications.setVisibility(View.VISIBLE);
				_action_bar_image_notifications.setVisibility(View.VISIBLE); 
				
				_action_bar_text_notifications.setText(""+numNotification);
			}
			
			int allNotification = Integer.parseInt(NCounters.NewMessages)+Integer.parseInt(NCounters.NewLikes)+Integer.parseInt(NCounters.NewOffers);
			
			Context context = _activity.getApplicationContext();
			if (Badge.isBadgingSupported(context)) 
			{
			    Badge badge = new Badge();
			    badge.mPackage = "com.goomena.app";
			    badge.mClass = "com.goomena.app.ActSplash"; // This should point to Activity declared as android.intent.action.MAIN
			    
			    if(allNotification!=0)
			    {
			    	badge.mBadgeCount = allNotification;
			    }
			    else
			    {
			    	Badge.deleteAllBadges(context);
			    }
			    
			
			    //Log.v("APP", "ethetheth " +badge.getIcon());
			    //badge.mIcon = R.drawable.bid;
			    badge.save(context);
			}
		}			
	}
}


class getCredits extends AsyncTask<Void,Void,String>
{
	TextView _textCredits;
	Activity _activity;
	public getCredits(Activity activity,TextView textCredits)
	{
		this._textCredits = textCredits;
		this._activity = activity;
	}
	@Override
	protected String doInBackground(Void... params) 
	{
		String Credits;
		Credits = ServiceClient.getCredits(Utils.getSharedPreferences(_activity).getString("userid",""));
		return Credits;
	}
	
	@Override
	protected void onPostExecute(String NCredits)
	{
		if(NCredits!=null)
		{
			_textCredits.setText("Credits  " +"( "+NCredits+" )");
		}			
	}
}

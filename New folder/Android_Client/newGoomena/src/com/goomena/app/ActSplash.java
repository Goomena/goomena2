package com.goomena.app;

import com.goomena.app.R;
import com.goomena.app.tools.Utils;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

public class ActSplash extends Activity
{
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_splash);
		
		redirect();
	}
	
	private void redirect()
	{
		SharedPreferences settings = getSharedPreferences(Utils.SharedPrefsKey,0 );
		if(settings.getBoolean("isLoggedIn", false))
		{	
			startActivity(new Intent(ActSplash.this,ActHome.class));
			ActSplash.this.finish();
		}
		else
		{
			startActivity(new Intent(ActSplash.this,ActFirst.class));
			ActSplash.this.finish();
		}
	}
		
	@Override
	protected void onResume() 
	{	
		super.onResume();
		this.finish();
	}
}

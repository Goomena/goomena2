package com.goomena.app;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.goomena.app.tools.Log;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.widget.Toast;

@SuppressLint("NewApi")
public class ActGoogleMap extends Activity
{
	// Google Map
    private GoogleMap googleMap;
    String userName;
    Double LAT,LNG;
    
    LatLng UsreLocation;
    static final LatLng KIEL = new LatLng(53.551, 9.993);
    
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_google_map);
		
		userName = (String) getIntent().getExtras().getSerializable("UserName");	
		LAT = (Double) getIntent().getExtras().getSerializable("Lat");
		LNG = (Double) getIntent().getExtras().getSerializable("Lng");
		
		Log.v("APP", "yyytr "+LAT.toString());
		
		if(LAT!=null && LNG!=null)
		{
			UsreLocation = new LatLng(LAT, LNG);
		}
		else
		{
			UsreLocation = new LatLng(38.73154893335188, 22.204045385742184);
		}
		
		try 
		{
			// Loading map
	        initilizeMap();
	            
	        //Marker hamburg = googleMap.addMarker(new MarkerOptions().position(HAMBURG).title("Hamburg"));
	            
	        if(LAT!=null && LNG!=null)
	        {
	        	@SuppressWarnings("unused")
				Marker kiel = googleMap.addMarker(new MarkerOptions()
	            .position(UsreLocation)
		        .title(userName)
		        .snippet("I am here"));
		        //.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_launcher)));
	        	googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(UsreLocation, 17.0f));
	        }
	        else
	        {
	        	googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(UsreLocation, 7.0f));
	        }
	            
	        } catch (Exception e) { e.printStackTrace(); }
	}
	
	/**
     * function to load map. If map is not created it will create it for you
     * */
    private void initilizeMap() 
    {
        if (googleMap == null) 
        {
            googleMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
 
            // check if map is created successfully or not
            if (googleMap == null) 
            {
                Toast.makeText(getApplicationContext(),"Sorry! unable to create maps", Toast.LENGTH_SHORT).show();
            }
        }
    }
    
    
    @Override
    protected void onResume() {
        super.onResume();
        initilizeMap();
    }

}

package com.goomena.app;

import java.util.ArrayList;

import com.goomena.app.data.ServiceClient;
import com.goomena.app.models.UserProfile;
import com.goomena.app.tools.Log;
import com.goomena.app.tools.Utils;
import com.goomena.fragments.FragMessageView;
import com.goomena.fragments.FragProfile;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

@SuppressLint("NewApi") 
public class LiveNotification 
{
	Activity _activity;
	static ImageView live_notificImageProfile;
	static ImageView live_notificImagefinger;
	ImageView live_notificImageType;
	static RelativeLayout live_notification,live_notificRelativeProfile;
	RelativeLayout relativefragment_place;
	RelativeLayout eeeee;
	Dialog d;
	
	static Animation animMoveFinger,animZoomImage,animBack;
	
	private final static String TAG_FRAGMENT = "TAG_FRAGMENT";

	public LiveNotification(Activity activity)
	{
		this._activity = activity;
		
		relativefragment_place = (RelativeLayout) activity.findViewById(R.id.relativefragment_place);
		
		//relativefragment_placeInBox = (RelativeLayout) activity.findViewById(R.id.relativefragment_placeInBox);
		live_notification = (RelativeLayout) activity.findViewById(R.id.live_notification);
		live_notificImageProfile = (ImageView) activity.findViewById(R.id.live_notificImageProfile);
		live_notificImagefinger = (ImageView) activity.findViewById(R.id.live_notificImagefinger);
		live_notificImageType = (ImageView) activity.findViewById(R.id.live_notificImageType);
		animMoveFinger = AnimationUtils.loadAnimation(activity, R.anim.move_notification_finger);
		animZoomImage = AnimationUtils.loadAnimation(activity, R.anim.zoom_notivication_image);
		animBack = AnimationUtils.loadAnimation(activity, R.anim.back_notification);
		
		
		
		new getProfileTask("73576").execute();
		
		Utils.getImageLoader(_activity).displayImage("drawable://" + R.drawable.baby_icon, live_notificImageProfile, Utils.getImageOptions3());
	
	
		live_notification.setOnTouchListener(new OnTouchListener() {
			
			@SuppressLint("NewApi") 
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				
			    switch (event.getAction() & MotionEvent.ACTION_MASK) {
			        case MotionEvent.ACTION_DOWN:
			            
			            break;
			        case MotionEvent.ACTION_UP:
			        	live_notificImageProfile.animate().setDuration(300).translationXBy(event.getX()).translationYBy(event.getY()).translationX(0).translationY(0);
			        	live_notificImageProfile.startAnimation(animBack);
			        	
			        	d.show();
			        	
			        	eeeee.setTranslationX(1);
			        	eeeee.setTranslationY(1);
			        	break;
			        case MotionEvent.ACTION_POINTER_DOWN:
			            break;
			        case MotionEvent.ACTION_POINTER_UP:
			            break;
			        case MotionEvent.ACTION_MOVE:
			        	live_notificImageProfile.setTranslationX(event.getX()-150);
			        	live_notificImageProfile.setTranslationY(event.getY()-150);
			        	
			        	d.show();
			        	if(eeeee!=null)
			        	{
			        	eeeee.setVisibility(View.VISIBLE);
			        	eeeee.setTranslationX(event.getX()+800);
			        	eeeee.setTranslationY(event.getY()+1024);
			        	}
			        	
			        	break;
			    }
				return true;
			}
		});
	
	}
	
	
	
	public static void startAnimNotification()
	{
		live_notification.setVisibility(View.VISIBLE);
		live_notification.startAnimation(animMoveFinger);
		
		live_notificImageProfile.startAnimation(animZoomImage);
		
		
	}
	
	
	
	
	//73576
	
	
	class getProfileTask extends AsyncTask<Void,UserProfile,UserProfile>
	{
		String _userID;
		
		getProfileTask(String userID)
		{
			this._userID = userID;
		}
		
		@Override
		protected UserProfile doInBackground(Void... params) {
			// TODO Auto-generated method stub
			return ServiceClient.getProfile(_userID);
		}
		
		@Override
		protected void onPostExecute(UserProfile userprofile)
		{
			if(userprofile!=null)
			{
				
				Utils.getImageLoader(_activity).displayImage(userprofile.ImageUrl, live_notificImageProfile, Utils.getImageOptions3());
				/*
				Fragment fr;
				fr = new FragMessageView(null,userprofile,0);
				FragmentManager fm = _activity.getFragmentManager();
				
		        FragmentTransaction fragmentTransaction = fm.beginTransaction();
		        fragmentTransaction.setCustomAnimations(R.anim.left_slidein_list_animator, R.anim.left_slideout_list_animator,R.anim.right_slidein_list_animator, R.anim.right_slideout_list_animator);
		        fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
		        fragmentTransaction.addToBackStack(null);
		        fragmentTransaction.commit();
		        */
				
				dialogLike(userprofile);
		        
			}
		}
	}
	
	
	public void dialogLike(final UserProfile userprofile)
	{
		d = new Dialog(_activity, R.style.DialogSlideAnim2);
		d.setContentView(R.layout.dialog_like);
		
		eeeee = (RelativeLayout) d.findViewById(R.id.eeeee);
		TextView dialog_textTitle = (TextView) d.findViewById(R.id.dialog_textTitle);
		ImageView dialog_imageProfile = (ImageView) d.findViewById(R.id.dialog_imageProfile);
		TextView dialog_textUsername = (TextView) d.findViewById(R.id.dialog_textUsername);
		TextView dialog_textAge = (TextView) d.findViewById(R.id.dialog_textAge);
		TextView dialog_textLocation = (TextView) d.findViewById(R.id.dialog_textLocation);
		RelativeLayout dialog_relativeLayoutSendMessage = (RelativeLayout) d.findViewById(R.id.dialog_relativeLayoutSendMessage);
		RelativeLayout dialog_relativeLayoutBid = (RelativeLayout) d.findViewById(R.id.dialog_relativeLayoutBid);
		RelativeLayout dialog_relativeLayoutReject = (RelativeLayout) d.findViewById(R.id.dialog_relativeLayoutReject);
		TextView dialog_textReject = (TextView) d.findViewById(R.id.dialog_textReject);
		RelativeLayout dialog_relativeLayoutProfile = (RelativeLayout) d.findViewById(R.id.dialog_relativeLayoutProfile);
		
		dialog_textTitle.setText("You received a Like From:");
		
		Utils.getImageLoader(_activity).displayImage(userprofile.ImageUrl.replace("d150", "thumbs"), dialog_imageProfile, Utils.getImageOptions()); 	    
		dialog_textUsername.setText(userprofile.UserName);
		dialog_textAge.setText(_activity.getResources().getString(R.string.age)+" "+userprofile.UserAge+" "+_activity.getResources().getString(R.string.years));
		dialog_textLocation.setText(userprofile.UserCity+", "+userprofile.UserRegion);
		dialog_textReject.setText("Reject the like");
		
		eeeee.setVisibility(View.GONE);
		
		dialog_imageProfile.setOnClickListener(new OnClickListener() {
			@SuppressLint("NewApi") 
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				ArrayList<UserProfile> _userProfilesFull_ = new ArrayList<UserProfile>();
				_userProfilesFull_.add(userprofile);
				
				Fragment fr = new FragProfile(2,_userProfilesFull_,0);				
				FragmentManager fm = _activity.getFragmentManager();
			    FragmentTransaction fragmentTransaction = fm.beginTransaction();
			    fragmentTransaction.setCustomAnimations(R.anim.left_slidein_list_animator, R.anim.left_slideout_list_animator,R.anim.right_slidein_list_animator, R.anim.right_slideout_list_animator);
			    fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
			    fragmentTransaction.addToBackStack(null);
			    fragmentTransaction.commit();
				d.cancel();
			}
		});
		
		dialog_relativeLayoutSendMessage.setOnClickListener(new OnClickListener() {
			
			@SuppressLint("NewApi")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Fragment fr;
				fr = new FragMessageView(null,userprofile,Integer.parseInt("0"),null);
				FragmentManager fm = _activity.getFragmentManager();
				
		        FragmentTransaction fragmentTransaction = fm.beginTransaction();
		        fragmentTransaction.setCustomAnimations(R.anim.left_slidein_list_animator, R.anim.left_slideout_list_animator,R.anim.right_slidein_list_animator, R.anim.right_slideout_list_animator);
		        fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
		        fragmentTransaction.addToBackStack(null);
		        fragmentTransaction.commit();
		        d.cancel();
			}
		});
		
		dialog_relativeLayoutBid.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//////createOfferDialog(userprofile);
				//////offerDialog.show();
				d.cancel();
			}
		});
		
		dialog_relativeLayoutProfile.setOnClickListener(new OnClickListener() {
			@SuppressLint("NewApi") 
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				ArrayList<UserProfile> _userProfilesFull_ = new ArrayList<UserProfile>();
				_userProfilesFull_.add(userprofile);
				
				Fragment fr = new FragProfile(2,_userProfilesFull_,0);			
				FragmentManager fm = _activity.getFragmentManager();
			    FragmentTransaction fragmentTransaction = fm.beginTransaction();
			    fragmentTransaction.setCustomAnimations(R.anim.left_slidein_list_animator, R.anim.left_slideout_list_animator,R.anim.right_slidein_list_animator, R.anim.right_slideout_list_animator);
			    fragmentTransaction.replace(R.id.fragment_place, fr,TAG_FRAGMENT);
			    fragmentTransaction.addToBackStack(null);
			    fragmentTransaction.commit();
				d.cancel();
			}
		});
		
		dialog_relativeLayoutReject.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//rejectDialog.show();
				//currentOfferid = profilesListFull_new.offers.get(position).offerId;
				d.cancel();
			}
		});
		
	}
	
	
}

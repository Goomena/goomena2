package com.goomena.app;

import com.goomena.app.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class ActRegister2 extends Activity
{
	RelativeLayout relativeLayout1,relativeLayout2,relativeLayout3,relativeLayout4,relativeLayout5,relativeLayout6,relativeLayout7;
	EditText editEmail,editUserName,editPass,editConfPass;
	
	String fbMail,fbId,fbName,fbUsername,fbGender;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_register2);
		
		relativeLayout1 = (RelativeLayout) findViewById(R.id.relativeLayout1);
     	relativeLayout2 = (RelativeLayout) findViewById(R.id.relativeLayout2);
     	relativeLayout3 = (RelativeLayout) findViewById(R.id.relativeLayout3);
     	relativeLayout4 = (RelativeLayout) findViewById(R.id.relativeLayout4);
     	relativeLayout5 = (RelativeLayout) findViewById(R.id.relativeLayout5);
     	relativeLayout6 = (RelativeLayout) findViewById(R.id.relativeLayout6);
     	relativeLayout7 = (RelativeLayout) findViewById(R.id.relativeLayout7);
     	
     	editEmail = (EditText) findViewById(R.id.editEmail);
     	editUserName = (EditText) findViewById(R.id.editUserName);
     	editPass = (EditText) findViewById(R.id.editPass);
     	editConfPass = (EditText) findViewById(R.id.editConfPass);
     	
     	relativeLayout7.setOnClickListener(clickListener);
     	
     	Intent intent = getIntent();
     	fbMail = intent.getStringExtra("fbMail");
     	fbId = intent.getStringExtra("fbId");
     	fbName = intent.getStringExtra("fbName");
     	fbUsername = intent.getStringExtra("fbUsername");
     	fbGender = intent.getStringExtra("fbGender");
     	
     	
     	
     	if(fbMail!=null)
     	{
     		editEmail.setText(fbMail);
     	}
     	
	}
	
	OnClickListener clickListener = new OnClickListener() 
	{
		@Override
		public void onClick(View v) 
		{
			if(v == relativeLayout2)
			{
				
			}
			else if(v == relativeLayout3)
			{
				
			}
			else if(v == relativeLayout4)
			{
				
			}
			else if(v == relativeLayout5)
			{
				
			}
			else if(v == relativeLayout7)
			{
				GlobalRegister.Email = editEmail.getText().toString();
				GlobalRegister.UserName = editUserName.getText().toString();
				GlobalRegister.Pass = editPass.getText().toString();
				GlobalRegister.ConfPass = editConfPass.getText().toString();
				
				
				if(isValidEmail(GlobalRegister.Email))
				{
					if (GlobalRegister.UserName.length()> 2 && GlobalRegister.UserName.length()< 11) 
					{
						if(GlobalRegister.Pass.length()>2)
						{
							if(GlobalRegister.Pass.equals(GlobalRegister.ConfPass))
							{
								Intent intent = new Intent();
								intent.setClass(ActRegister2.this, ActRegister3.class);

								intent.putExtra("fbMail", fbMail);
								intent.putExtra("fbId", fbId);
								intent.putExtra("fbName", fbName);
								intent.putExtra("fbUsername", fbUsername);
								intent.putExtra("fbGender", fbGender);
								
								startActivity(intent);
								overridePendingTransition(R.anim.right_in, R.anim.left_out);
							}
							else
							{
								Toast.makeText(ActRegister2.this, getResources().getString(R.string.passnotmatch), Toast.LENGTH_LONG).show();
							}
						}
						else
						{
							Toast.makeText(ActRegister2.this, getResources().getString(R.string.passtooshort), Toast.LENGTH_LONG).show();
						}
					}
					else
					{
						Toast.makeText(ActRegister2.this, getResources().getString(R.string.invalidusername), Toast.LENGTH_LONG).show();
					}
				}
				else
				{
					Toast.makeText(ActRegister2.this, getResources().getString(R.string.novalidmail), Toast.LENGTH_LONG).show();
				}
			}
		}
	};
	
	private boolean isValidEmail(CharSequence target) 
	{
	    if (target == null) 
	    {
	        return false;
	    } 
	    else 
	    {
	    	//Toast.makeText(ActRegister2.this, getResources().getString(R.string.novalidmail), Toast.LENGTH_LONG).show();
	        return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
	    }
	}

}
package com.goomena.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RelativeLayout;

public class ActMeet extends Activity
{
	RelativeLayout relativeWoman,relativeMan,relativeRegister;
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_meet);
		
		relativeWoman = (RelativeLayout) findViewById(R.id.relativeWoman);
		relativeMan = (RelativeLayout) findViewById(R.id.relativeMan);
		relativeRegister = (RelativeLayout) findViewById(R.id.relativeRegister);
		
		relativeWoman.setOnClickListener(clickListener);
		relativeMan.setOnClickListener(clickListener);
		relativeRegister.setOnClickListener(clickListener);
	}
	
	
	OnClickListener clickListener = new OnClickListener() 
	{
		@Override
		public void onClick(View v) 
		{
			if(v == relativeWoman)
			{
				// TODO Auto-generated method stub
				Intent intent = new Intent();
				intent.setClass(ActMeet.this, ActMeet2.class);
				intent.putExtra("YourChoice", "1");
				startActivity(intent);
				overridePendingTransition(R.anim.right_in, R.anim.left_out);
			}
			else if(v == relativeMan)
			{
				// TODO Auto-generated method stub
				Intent intent = new Intent();
				intent.setClass(ActMeet.this, ActMeet2.class);
				intent.putExtra("YourChoice", "2");
				startActivity(intent);
				overridePendingTransition(R.anim.right_in, R.anim.left_out);
			}
			else if(v == relativeRegister)
			{
				// TODO Auto-generated method stub
				Intent intent = new Intent();
				intent.setClass(ActMeet.this, ActRegister1.class);
				startActivity(intent);
				overridePendingTransition(R.anim.right_in, R.anim.left_out);
			}
		}
	};

}

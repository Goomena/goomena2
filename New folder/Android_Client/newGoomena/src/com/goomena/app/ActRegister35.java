package com.goomena.app;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import com.goomena.app.data.DataEditProfile;
import com.goomena.app.data.ServiceClient;
import com.goomena.app.global.Global;
import com.goomena.app.models.ItemList;
import com.goomena.app.tools.Utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class ActRegister35 extends Activity
{
	Spinner spin1,spin2,spin3,spin4;
	RelativeLayout relativeLayout10;
	
	Dialog Dwaiting;
	Animation animFadein;
	Animation animRotScal;
	TextView dialog_waiting_textWaiting;
	ImageView dialog_waiting_bgLogo,dialog_waiting_FrLogo;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_register35);
		
		
		///-----+
		Dwaiting = new Dialog(this, R.style.DialogSlideAnim);	
		Dwaiting.setContentView(R.layout.dialog_waiting);
		animFadein = AnimationUtils.loadAnimation(this, R.anim.d_wait_fr_load);
		animRotScal = AnimationUtils.loadAnimation(this, R.anim.d_wait_bg_logo);
		dialog_waiting_textWaiting = (TextView) Dwaiting.findViewById(R.id.dialog_waiting_textWaiting);
		dialog_waiting_bgLogo = (ImageView) Dwaiting.findViewById(R.id.dialog_waiting_bgLogo);
		dialog_waiting_FrLogo = (ImageView) Dwaiting.findViewById(R.id.dialog_waiting_FrLogo);
		dialog_waiting_textWaiting.startAnimation(animFadein);
		dialog_waiting_bgLogo.startAnimation(animRotScal);
		/////----
		
		if(Utils.getSharedPreferences(ActRegister35.this).getString("genderid","").contains("2"))
		{
			dialog_waiting_FrLogo.setImageResource(R.drawable.woman_loading);
		}
		
		
		spin1 = (Spinner) findViewById(R.id.spin1);
		spin2 = (Spinner) findViewById(R.id.spin2);
		spin3 = (Spinner) findViewById(R.id.spin3);
		spin4 = (Spinner) findViewById(R.id.spin4);
		relativeLayout10 = (RelativeLayout) findViewById(R.id.relativeLayout10);
		
		
		new getPersonalInfoLists(spin1,"height").execute();
		new getPersonalInfoLists(spin2,"bodytype").execute();
		new getPersonalInfoLists(spin3,"eyecolor").execute();
		new getPersonalInfoLists(spin4,"haircolor").execute();
		
		
		
		relativeLayout10.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				new setProfileEditPersonalInfoLists().execute();
			}
		});
		
		
	}

	
	
	
	class getPersonalInfoLists extends AsyncTask<String,String,ItemList>
	{
		Spinner _a;
		String _b;
		@Override
		protected void onPreExecute() 
		{
			
		}
		public getPersonalInfoLists(Spinner a,String b)
		{
			this._a = a;
			this._b = b;
		}
		@Override
		protected ItemList doInBackground(String... params) 
		{
			if(Locale.getDefault().getLanguage().equals("el"))
		    {
				Global.Lang = "GR";
		    }
		    else if(Locale.getDefault().getLanguage().equals("en"))
		    {
		    	Global.Lang = "US";
		    }
				
				
			ItemList result = ServiceClient.getItems(_b, Global.Lang);
			return result;
		}
		
		@Override
		protected void onPostExecute(ItemList s)
		{
			
			if(s!=null)
			{
				
				ArrayList<String> items = new ArrayList<String>();
				for (int i=0; i<s.items.size(); i++) 
				{
					//HashMap hm = new HashMap<String,String>();
					//hm.put(s.items.get(i).id, s.items.get(i).title.replace("&lt;", "<").replace("&gt;", ">").replace("&euro;", "€"));
					items.add(s.items.get(i).title.replace("&lt;", "<").replace("&gt;", ">").replace("&euro;", "€"));
				}
				ArrayAdapter <String> adapter2 = new ArrayAdapter<String>(ActRegister35.this,R.layout.spinner_item,items);
				adapter2.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
				_a.setAdapter(adapter2);
			}
		}		
	}
	
	
	
	class setProfileEditPersonalInfoLists extends AsyncTask<String,String,String>
	{
		@Override
		protected void onPreExecute() { Dwaiting.show(); }
		@Override
		protected String doInBackground(String... params) 
		{	
			String result = DataEditProfile.setProfileEditPersonalInfoLists(spin1.getSelectedItemId()+28, spin2.getSelectedItemId()+6,
					spin3.getSelectedItemId()+8,spin4.getSelectedItemId()+10,
					1,1,1,1,1,1,ActRegister35.this );
			return result;
		}
		
		@Override
		protected void onPostExecute(String s)
		{
			if(s!=null)
			{
				if(s.contains("ok") && spin1.getSelectedItemId()!=0 && spin2.getSelectedItemId()!=0 && spin3.getSelectedItemId()!=0 && spin4.getSelectedItemId()!=0)
				{
					startActivity(new Intent(ActRegister35.this,ActRegister4.class));
					ActRegister35.this.finish();
				}
				else
				{
					Toast.makeText(ActRegister35.this, getResources().getString(R.string.please_provide_your_pesonal_info), Toast.LENGTH_LONG).show();
				}
			}
			Dwaiting.cancel();
		}		
	}
}

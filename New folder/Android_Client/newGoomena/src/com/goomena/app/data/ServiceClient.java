package com.goomena.app.data;

import java.net.URLEncoder;
import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.goomena.app.models.CheckActions;
import com.goomena.app.models.Cities;
import com.goomena.app.models.Counters;
import com.goomena.app.models.CountryList;
import com.goomena.app.models.GCMUserSettings;
import com.goomena.app.models.ImagePathList;
import com.goomena.app.models.ItemList;
import com.goomena.app.models.Message;
import com.goomena.app.models.MessageInfoList;
import com.goomena.app.models.MessageList;
import com.goomena.app.models.OfferList;
import com.goomena.app.models.Regions;
import com.goomena.app.models.UserDetails;
import com.goomena.app.models.UserProfile;
import com.goomena.app.models.UserProfilesList;
import com.goomena.app.tools.Log;




public class ServiceClient {

	static String base_url = "http://188.165.13.64/testwcf/GoomenaLib.Version4.GoomenaServiceV4.svc/json/";

	
	
	private static Gson gson;
	
	private static Gson getGson(){
		if(gson==null){
			gson = new Gson();
		}
		return gson;
	}
	
	
	public static UserProfile getProfile(String userid){
		
		Log.v("APP","asasas userprofile= "+userid);
		
		String url = base_url+"userprofile/"+userid;
		String result = DataFetcher.getData(url);
		Log.v("APP", "Server result:"+result);
		try{
			UserProfile profile = getGson().fromJson(result, UserProfile.class);
			return profile;
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	
	public static UserProfile loginUser_v2(String email, String password,String loginDetails, String fbuid , String fbname , String fbusername){
		String result = DataFetcher.postLoginData(email, password, loginDetails, fbuid, fbname, fbusername);
		
		try{
			UserProfile pinfo = getGson().fromJson(result, UserProfile.class);
			return pinfo;
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	public static UserProfilesList getNewMembers(String userid,String offset,String results,String gender, String onlineUser, String country){
		Log.v("APP","asasas newusers_v3= "+userid);
		String url = base_url + "newusers_v3/"+userid+"/"+offset+"/"+results+"/"+gender+"/"+onlineUser+"/"+country;
		String result = DataFetcher.getData(url);
		UserProfilesList list = getGson().fromJson(result, UserProfilesList.class);
		return list;
	}
	

	
	public static UserDetails getUserDetails(String userid,String langid){
		Log.v("APP","asasas userprofiledetails= "+userid+" "+langid);
		String url = base_url +"userprofiledetails/"+userid+"/"+langid;
		String result = DataFetcher.getData(url);
		UserDetails details = getGson().fromJson(result, UserDetails.class);
		return details;
	}
	
	public static String getUserProfileImage(String userid,String gender){
		
		String url = base_url +"profileimage/"+userid+"/"+gender;
		
		Log.v("APP","asasas profileimage= "+userid);
		
		String result = DataFetcher.getData(url);
		String imgurl= null;
		try {
			JSONObject obj = new JSONObject(result);
			imgurl = obj.getString("Path");
			
		} catch (JSONException e) {			
			e.printStackTrace();
		}
		return imgurl;
	}
	
	public static String getUserState(String userid){
		
		Log.v("APP","asasas getuserstate= "+userid);
		String url = base_url+"getuserstate/"+userid;
		String result = DataFetcher.getData(url);
		return result;
	}
	
	public static String getLevel(String userid, String profileid){
		Log.v("APP","asasas getPhotoLevel= "+userid+" "+profileid);
		String url = base_url+"getPhotoLevel/"+userid+"/"+profileid;
		String result = DataFetcher.getData(url);
		return result;

	}
	
	public static ImagePathList getUserPhotos(String myuserid,String userid,String genderid){
		
		String url = base_url+"getuserphotos/"+myuserid+"/"+userid+"/"+genderid;
		String result = DataFetcher.getData(url);
		ImagePathList list = new ImagePathList();
		list = getGson().fromJson(result, ImagePathList.class);
		return list;
	}
	
	public static ImagePathList getUserPhotosThumbs(String myuserid,String userid,String genderid){
		
		String url = base_url+"getuserphotosThumb/"+myuserid+"/"+userid+"/"+genderid;
		String result = DataFetcher.getData(url);
		ImagePathList list = new ImagePathList();
		list = getGson().fromJson(result, ImagePathList.class);
		return list;
	}
	
	public static UserProfilesList getSearchResults_v2(String userid,String sort,String zipstr,String lat,String lng,String distance,
            String gender,String searchname, String username,String agemin,String agemax,
            String hasPhotos,String isOnline,String isVip, String start,String results, String country,String hasPrivatePhotos,String travel){
		
		try{
			UserProfilesList profileList = new UserProfilesList();
			String url = base_url + "getsearchresults_v3/"+userid+"/"+sort+"/"+zipstr+"/"+lat+"/"+lng+"/"+distance+"/"+gender+"/"+
						searchname+"/"+username+"/"+agemin+"/"+agemax+"/"+hasPhotos+"/"+isOnline+"/"+start+"/"+results+"/"+isVip+"/"+country+"/"+hasPrivatePhotos+"/"+travel;
			String result = DataFetcher.getData(url);
			Log.v("APP","SEARCH RESULT: \n"+result);
			profileList = getGson().fromJson(result, UserProfilesList.class);
							
			return profileList;
		}
		catch(Exception e){
			return null;
		}
	}
	
	public static UserProfilesList getSearchResults(String userid,String sort,String zipstr,String lat,String lng,String distance,
            String gender,String searchname, String username,String agemin,String agemax,
            String hasPhotos,String isOnline,String start,String results){
		
		try{
			UserProfilesList profileList = new UserProfilesList();
			String url = base_url + "getsearchresults/"+userid+"/"+sort+"/"+zipstr+"/"+lat+"/"+lng+"/"+distance+"/"+gender+"/"+
						searchname+"/"+username+"/"+agemin+"/"+agemax+"/"+hasPhotos+"/"+isOnline+"/"+start+"/"+results;
			String result = DataFetcher.getData(url);
			Log.v("APP","SEARCH RESULT: \n"+result);
			profileList = getGson().fromJson(result, UserProfilesList.class);
							
			return profileList;
		}
		catch(Exception e){
			return null;
		}
		
	}
	
	
	public static UserProfilesList getSearchResultsPublic(String sort,String lat,String lng,String distance,String gender,String start,String results, String country){
		try{
			UserProfilesList profileList = new UserProfilesList();
			String url = base_url + "getsearchresultspublic/"+sort+"/"+lat+"/"+lng+"/"+distance+"/"+gender+"/"+start+"/"+results+"/"+country;
			String result = DataFetcher.getData(url);
			Log.v("APP","SEARCH RESULT: \n"+result);
			profileList = getGson().fromJson(result, UserProfilesList.class);
							
			return profileList;
		}
		catch(Exception e){
			return null;
		}
	}
	
	
	public static MessageInfoList getMessagesList(String userid,String view,String indexstart,String active){
		String url = base_url + "messageslist/"+userid+"/"+view+"/"+indexstart+"/"+active;
		String result = DataFetcher.getData(url);
		MessageInfoList list = new MessageInfoList();
		list = getGson().fromJson(result, MessageInfoList.class);
		return list;
	}
	
	public static MessageInfoList getNewMessages(String userid){
		
		String url = base_url + "messagesquick/"+userid;
		String result = DataFetcher.getData(url);
		MessageInfoList list = getGson().fromJson(result, MessageInfoList.class);
		return list;		
	}
	
	public static Message getMessage(String messageId){
		String url = base_url +"message/"+messageId;
		String result = DataFetcher.getData(url);
		Message m = getGson().fromJson(result, Message.class);
		return m;
	}
	
	public static MessageList getConversation(String userid,String messageid){
		
		Log.v("APP","asasas conversation= "+userid+" "+messageid);
		
		String url = base_url + "conversation/"+userid+"/"+messageid;
		String result = DataFetcher.getData(url);
		MessageList list = getGson().fromJson(result, MessageList.class);
		return list;
	}
	
	public static MessageList getConversation_with_profile(String Myuserid,String userid){
		
		Log.v("APP","asasas conversation= "+Myuserid+" "+userid);
		
		String url = base_url + "conversation_with_profile/"+Myuserid+"/"+userid;
		String result = DataFetcher.getData(url);
		MessageList list = getGson().fromJson(result, MessageList.class);
		return list;
	}
	
	
	
	public static String unlockMessage(String userid,String genderId,String senderid, String messageid,String offerid){
		String url = base_url + "unlockmessage/"+userid+"/"+genderId+"/"+senderid+"/"+messageid+"/"+offerid;
		//Global.credits=Global.credits-50;
		String result = DataFetcher.getData(url);
		return result;
	}
	
	/*
	public static String sendMessage(String subject, String body,String fromProfileId,String toProfileId, String genderId){
		String url = base_url+"sendmessage/"+subject+"/"+body+"/"+fromProfileId+"/"+toProfileId+"/"+genderId;
		String result = DataFetcher.getData(url);
		return result;
	}
	*/
	
	public static String deleteMessage(String userid,String messageid,String referrerId){
		String url = base_url + "deletemessage/"+userid+"/"+messageid+"/"+referrerId;
		String result = DataFetcher.getData(url);
		return result;
	}
	
	public static UserProfilesList getFavoriteList(String userid,String zip,String lat,String lng){
		
		Log.v("APP","asasas favorite= "+userid);
		
		String url = base_url+"favorite/"+userid+"/"+zip+"/"+lat+"/"+lng;
		String result = DataFetcher.getData(url);
		UserProfilesList list = getGson().fromJson(result, UserProfilesList.class);
		return list;
	}
	
	public static UserProfilesList getFavoriteList_v2(String userid,String zip,String lat,String lng, String onlineUse){
		
		Log.v("APP","asasas favorite_v2= "+userid);
		
		String url = base_url+"favorite_v2/"+userid+"/"+zip+"/"+lat+"/"+lng+"/"+onlineUse;
		String result = DataFetcher.getData(url);
		UserProfilesList list = getGson().fromJson(result, UserProfilesList.class);
		return list;
	}
	
	
	public static String viewProfile(String myProfileId,String viewProfileId){
		Log.v("APP", "asasas viewprofile= "+myProfileId+" "+viewProfileId);
		
		String url=base_url+"viewprofile/"+myProfileId+"/"+viewProfileId;
		String result = DataFetcher.getData(url);
		return result;
	}
	
	public static UserProfilesList getWhoViewedMe(String userid,String zip,String lat,String lng,String indexstart){
		String url = base_url+"whoviewedme/"+userid+"/"+zip+"/"+lat+"/"+lng+"/"+indexstart;
		String result = DataFetcher.getData(url);
		UserProfilesList list = getGson().fromJson(result, UserProfilesList.class);
		return list;
	}
	
	public static UserProfilesList getWhoViewedMe_v2(String userid,String zip,String lat,String lng,String indexstart, String onlineUser){
		String url = base_url+"whoviewedme_v2/"+userid+"/"+zip+"/"+lat+"/"+lng+"/"+indexstart+"/"+onlineUser;
		String result = DataFetcher.getData(url);
		UserProfilesList list = getGson().fromJson(result, UserProfilesList.class);
		return list;
	}
	
	
	public static String favorite(String myuserid,String userid){
		
		Log.v("APP", "asasas favorite= "+myuserid+" "+userid);
		
		String url = base_url+"favorite/"+myuserid+"/"+userid;
		String result = DataFetcher.getData(url);
		return result;
	}
	
	public static String level(String myuserid,String userid, int level){
		String url = base_url+"photolevel/"+myuserid+"/"+userid+"/"+level;
		String result = DataFetcher.getData(url);
		return result;
	}
	
	public static String unfavorite(String myuserid,String userid){
		String url = base_url+"unfavorite/"+myuserid+"/"+userid;
		String result = DataFetcher.getData(url);
		return result;
	}
	
	public static String checkFavorite(String myuserid,String userid){
		
		Log.v("APP", "asasas checkfavorite= "+myuserid+" "+userid);
		
		String url = base_url+"checkfavorite/"+myuserid+"/"+userid;
		String result = DataFetcher.getData(url);
		return result;
	}
	
	public static String checkHasPhoto(String userid){
		String url = base_url+"checkhasphoto/"+userid;
		String result = DataFetcher.getData(url);
		return result;				
				
	}
	
	public static String checkLike(String myprofileid,String otherprofileid)
	{
		Log.v("APP", "asasas checklike= "+myprofileid+" "+otherprofileid);
		
		String url = base_url+"checklike/"+myprofileid+"/"+otherprofileid;
		String result = DataFetcher.getData(url);
		return result;
	}
	
	public static String like(String myprofileid,String otherprofileid){
		String url = base_url+"like/"+myprofileid+"/"+otherprofileid;
		String result = DataFetcher.getData(url);
		return result;
	}
	
	public static String poke(String myprofileid,String otherprofileid,String parentOffer){
		String url = base_url+"poke/"+myprofileid+"/"+otherprofileid+"/"+parentOffer;
		String result = DataFetcher.getData(url);
		return result;
	}
	
	
	public static CheckActions getCheckActions(String myid,String userid){
		String url = base_url+"checkactions/"+myid+"/"+userid;
		String result = DataFetcher.getData(url);
		CheckActions checkactions = getGson().fromJson(result, CheckActions.class);
		return checkactions;
	}
	
	
	
	public static UserProfilesList getWhoFavedMe(String userid,String zip,String lat,String lng){
		String url = base_url+"whofavme/"+userid+"/"+zip+"/"+lat+"/"+lng;
		String result = DataFetcher.getData(url);
		UserProfilesList list = getGson().fromJson(result, UserProfilesList.class);
		return list;
	}
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public static UserProfilesList getWhoAccessMe(String userid,String zip,String lat,String lng, int index){
		String url = base_url+"whoSharedMePhotos/"+userid+"/"+zip+"/"+lat+"/"+lng+"/"+index;
		String result = DataFetcher.getData(url);
		UserProfilesList list = getGson().fromJson(result, UserProfilesList.class);
		return list;
	}
	
	public static UserProfilesList getWhomAccess(String userid,String zip,String lat,String lng, int index){
		String url = base_url+"getSharedByMePhotos/"+userid+"/"+zip+"/"+lat+"/"+lng+"/"+index;
		String result = DataFetcher.getData(url);
		UserProfilesList list = getGson().fromJson(result, UserProfilesList.class);
		return list;
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	
	//For Block
	public static UserProfilesList getBlockedList(String userid,String lat,String lng, int index){
		String url = base_url+"blocked/"+userid+"/"+"/"+lat+"/"+lng+"/"+index;
		String result = DataFetcher.getData(url);
		UserProfilesList list = getGson().fromJson(result, UserProfilesList.class);
		return list;
	}
	public static String getNumberBlocked(String userid,String lat,String lng){
		String url = base_url+"blockedcount/"+userid+"/"+"/"+lat+"/"+lng;
		String result = DataFetcher.getData(url);
		return result;
	}
	public static String blockAction(String myprofileid,String otherprofileid,String action){
		String url = base_url+"blockaction/"+myprofileid+"/"+otherprofileid+"/"+action;
		String result = DataFetcher.getData(url);
		return result;
	}
	
	public static OfferList getLikes(String mode,String userid,String zip,String lat,String lng,String indexstart){
		String url = base_url+"likes/"+mode+"/"+userid+"/"+zip+"/"+lat+"/"+lng+"/"+indexstart;
		String result = DataFetcher.getData(url);
		OfferList list = getGson().fromJson(result, OfferList.class);
		return list;
	}
	
	public static String getNumberLikes(String userid){
		String url = base_url+"likescount/"+userid;
		String result = DataFetcher.getData(url);
		
		return result;
	}
	
	public static String getCredits(String userid){
		String url = base_url+"getCredits/"+userid;
		String result = DataFetcher.getData(url);
		
		return result;
	}
	
	public static String getNumberMessages(String userid){
		String url = base_url+"messagescount/"+userid;
		String result = DataFetcher.getData(url);
		
		return result;
	}
	
	
	public static Counters getCounters(String userid){
		
		String url = base_url + "counters/"+userid;
		String result = DataFetcher.getData(url);
		Counters counters = getGson().fromJson(result, Counters.class);
		return counters;		
	}

	public static String SendForgotPass(String email){
	
		String url = base_url + "restorepassword/"+email;
		String result = DataFetcher.getData(url);
	
		return result;		
	}
	
	public static OfferList getOffers(String mode,String userid,String zip,String lat,String lng,String indexstart){
		String url = base_url+"offers/"+mode+"/"+userid+"/"+zip+"/"+lat+"/"+lng+"/"+indexstart;
		String result = DataFetcher.getData(url);
		OfferList list = getGson().fromJson(result, OfferList.class);
		return list;
	}
	
	public static String likeActions(String userid,String action,String offerId){
		String url = base_url+"likeactions/"+userid+"/"+action+"/"+offerId;
		String result = DataFetcher.getData(url);
		return result;
	}
	
	public static String rejectLike(String offerid,String reason){
		String url = base_url+"rejectlike/"+offerid+"/"+reason;
		String result = DataFetcher.getData(url);
		return result;
	}
	
	public static String makeOffer(String mode,String amount,String fromprofileid,String toprofileid,String genderid,String offerid){
		String url = base_url+"makeoffer/"+mode+"/"+amount+"/"+fromprofileid+"/"+toprofileid+"/"+genderid+"/"+offerid;
		String result = DataFetcher.getData(url);
		return result;
	}
	
	public static String acceptOffer(String userid,String offerid){
		String url = base_url+"acceptoffer/"+userid+"/"+offerid;
		String result = DataFetcher.getData(url);
		return result;
	}
	
	public static String getHashTask(String username){
		String url = base_url+"hash/"+username;
		String result = DataFetcher.getData(url);
		return result;
	}
	
	public static String register(String email,String username,String password,String birthday, String gender, String country,String region, String city,String lag ){
		String url = base_url+"register/"+email+"/"+username+"/"+password+"/"+URLEncoder.encode(birthday)+"/"+gender+"/"+country+"/"+region+"/"+city+"/"+lag;
		String result = DataFetcher.getData(url);
		return result;
	}
	
	public static CountryList getCountries(){
		String url = base_url+"countries";
		String json = DataFetcher.getData(url);
		try{
			CountryList list = getGson().fromJson(json, CountryList.class);
			return list;
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	public static ItemList getItems(String mode,String lanquage){
		String url = base_url+"getList/"+mode+"/"+lanquage;
		String json = DataFetcher.getData(url);
		try{
			ItemList list = getGson().fromJson(json, ItemList.class);
			return list;
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	public static ArrayList<String> getCountryRegions(String countryIso,String lagId){
		String url = base_url + "regions/"+countryIso+"/"+lagId;
		String json = DataFetcher.getData(url);
		try{
			Regions regoins = getGson().fromJson(json, Regions.class ); 
			return regoins.list;
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	public static ArrayList<String> getRegionCities(String countryIso,String region, String lagId){
		String url = base_url + "cities/"+countryIso+"/"+region+"/"+lagId;
		String json = DataFetcher.getData(url);
		try{
			Cities cities = getGson().fromJson(json, Cities.class); 
			return cities.list;
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	public static String getRegionZip(String countryIso,String region,String city, String lagId){
		String url = base_url + "zip/"+countryIso+"/"+region+"/"+city+"/"+lagId;
		String json = DataFetcher.getData(url);
		return json;
	}
	
	public static String deleteMessagesInView(String profileId,String messagesView,String messageId){
		String url = base_url +"deletemessages/"+profileId+"/"+messagesView+"/"+messageId;
		String json = DataFetcher.getData(url);
		return json;
	}
	
	public static String setDeafultPhoto(String myprofileId,String photoId){
		
		Log.v("APP", "asasas defaultphoto= "+myprofileId+" "+photoId);
		
		String url = base_url+"defaultphoto/"+myprofileId+"/"+photoId;
		String res = DataFetcher.getData(url);
		return res;
	}
	
	public static String saveGCMsettings(String profileId,String notifMessages,String notifLikes,String notifOffers){
		String url = base_url+"savesettings/"+profileId+"/"+notifMessages+"/"+notifLikes+"/"+notifOffers;
		String res = DataFetcher.getData(url);
		return res;
	}
	
	public static GCMUserSettings getGCMSettings(String profileId){
		String url = base_url + "settings/"+profileId;
		String res = DataFetcher.getData(url);
		try{
			GCMUserSettings settings = getGson().fromJson(res,GCMUserSettings.class);
			return settings;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	public static String disconectGCM(String regId){
		String url = base_url+"removeGCMUser/"+regId;
		String res = DataFetcher.getData(url);
		return res;
	}
	
	
	
	public static String deletePhoto(String profileid, String photoid){
		String url = base_url+"deletephoto/"+profileid+"/"+photoid;
		String result = DataFetcher.getData(url);
		return result;

	}
	
	public static String setPhotoLevel(String profileid, String photoid,String level){
		String url = base_url+"setphotolevel/"+profileid+"/"+photoid+"/"+level;
		String result = DataFetcher.getData(url);
		return result;

	}
	
	public static String getMinLevel(String userid){
		String url = base_url+"getMinLevel/"+userid;
		String result = DataFetcher.getData(url);
		return result;

	}
	
	public static String getCheckCountry(String userid, String profileid){
		
		
		
		String url = base_url+"checkCountry/"+userid+"/"+profileid;
		String result = DataFetcher.getData(url);
		return result;

	}
	

	public static String getHasPhotos(String userid){
		String url = base_url+"hasphotos/"+userid;
		String result = DataFetcher.getData(url);
		return result;

	}

}

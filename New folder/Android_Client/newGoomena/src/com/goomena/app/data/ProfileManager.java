package com.goomena.app.data;

import android.app.Activity;
import android.content.SharedPreferences;

import com.goomena.app.models.UserProfile;
import com.goomena.app.tools.Utils;

public class ProfileManager {

	static String registrationId;
	
	public static void saveProfile(Activity activity,UserProfile result){
		
		SharedPreferences.Editor editor = Utils.getSharedPreferences(activity).edit();
		
		editor.putString("userid", result.ProfileId);
		editor.putString("genderid", result.GenderId);
		editor.putString("lat", result.Latitude);
		editor.putString("lng", result.Longtitude);
		editor.putString("zipcode", result.ZipCode);		
		editor.putString("imageurl", result.ImageUrl);
		editor.putString("username",result.UserName);
		editor.putString("userregion", result.UserRegion);
		editor.putString("usercity", result.UserCity);
		editor.putString("userage", result.UserAge);
		editor.putString("referal", result.ReferrerParentId);
		editor.putString("UserCountry", result.UserCountry);
		editor.putBoolean("isLoggedIn", true);
		editor.commit();		
		
	}
	
	public static UserProfile loadProfile(Activity activity){
		
		SharedPreferences settings = Utils.getSharedPreferences(activity);
		UserProfile profile = new UserProfile();
		profile.GenderId = settings.getString("genderid", "");
		profile.ProfileId = settings.getString("userid", "");
		profile.Latitude = settings.getString("lat", "");
		profile.Longtitude = settings.getString("lng", "");
		profile.ZipCode = settings.getString("zipcode", "");
		profile.ImageUrl = settings.getString("imageurl", "");
		profile.UserName = settings.getString("username", "");
		profile.UserRegion = settings.getString("userregion", "");
		profile.UserCity = settings.getString("usercity","");
		profile.UserAge = settings.getString("userage","");
		profile.ReferrerParentId = settings.getString("referal","");
		profile.UserCountry = settings.getString("UserCountry","");
		return profile;
	}

	public static void disconnect(Activity activity) 
	{
		String email,pass;
		boolean remeberme;
		SharedPreferences.Editor editor = Utils.getSharedPreferences(activity).edit();
		
		remeberme = Utils.getSharedPreferences(activity).getBoolean("rememberMe",false);
		email = Utils.getSharedPreferences(activity).getString("Email", "");
		pass = Utils.getSharedPreferences(activity).getString("Pass", "");
		
		editor.clear();
		editor.putBoolean("isLoggedIn", false);
		
		editor.putBoolean("rememberMe", remeberme);
		editor.putString("Email", email);
		editor.putString("Pass", pass);
		
		editor.commit();		
	}
	

	
}

package com.goomena.app.global;

import java.util.ArrayList;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.goomena.app.ActHome;
import com.goomena.app.adapters.ItemMessageAdapter;
import com.goomena.app.data.ServiceClient;
import com.goomena.app.models.MessageInfo;
import com.goomena.app.models.MessageInfoList;

public class GlobalMessages 
{
	public static String messagescount = "0";
	public static MessageInfoList messageInfos = null;
	public static MessageInfoList messageInfosFull = null;
	public static MessageInfoList messageInfos1 = null;
	public static MessageInfoList messageInfosFull1 = null;
	public static MessageInfoList messageInfos2 = null;
	public static MessageInfoList messageInfosFull2 = null;
	public static MessageInfoList messageInfos3 = null;
	public static MessageInfoList messageInfosFull3 = null;
	public static Handler handler_messages = null;
	//public static int max = 0;
	

	public static void getMessagesCount()
	{
		new getMessagesCountTask().execute(Global.MyId);
	}
	
	public static void getAllMessages()
	{
		//new getNewMessagesTask().execute();
		
	}
}







class getNewMessagesTask extends AsyncTask<Void,Integer,Void>{

	@Override
	protected void onPreExecute()
	{
		//imgLoading.setVisibility(View.VISIBLE);
		//loadingAnimation.start();
		GlobalMessages.messageInfosFull = new MessageInfoList();
		GlobalMessages.messageInfosFull.msgList = new ArrayList<MessageInfo>();
		
		GlobalMessages.messageInfosFull1 = new MessageInfoList();
		GlobalMessages.messageInfosFull1.msgList = new ArrayList<MessageInfo>();
		
		GlobalMessages.messageInfosFull2 = new MessageInfoList();
		GlobalMessages.messageInfosFull2.msgList = new ArrayList<MessageInfo>();
		
		GlobalMessages.messageInfosFull3 = new MessageInfoList();
		GlobalMessages.messageInfosFull3.msgList = new ArrayList<MessageInfo>();
		
	}
	
	@Override
	protected Void doInBackground(Void... params)
	{
		GlobalMessages.messageInfos = ServiceClient.getMessagesList(Global.MyId,String.valueOf(8),String.valueOf(GlobalMessages.messageInfosFull.msgList.size() ),"1");
		GlobalMessages.messageInfos1 = ServiceClient.getMessagesList(Global.MyId,String.valueOf(1),String.valueOf(GlobalMessages.messageInfosFull.msgList.size() ),"1");
		GlobalMessages.messageInfos2 = ServiceClient.getMessagesList(Global.MyId,String.valueOf(2),String.valueOf(GlobalMessages.messageInfosFull.msgList.size() ),"1");
		GlobalMessages.messageInfos3 = ServiceClient.getMessagesList(Global.MyId,String.valueOf(16),String.valueOf(GlobalMessages.messageInfosFull.msgList.size() ),"1");
		return null;
	}
	
	@Override
	protected void onPostExecute(Void v)
	{
		//if(isAdded())
		{
			
			if(GlobalMessages.messageInfos!=null)
			{
				GlobalMessages.messageInfosFull.msgList.addAll(GlobalMessages.messageInfos.msgList);
				
				
				if(GlobalMessages.handler_messages != null)
				{
					Message msg2 = GlobalMessages.handler_messages.obtainMessage();
					GlobalMessages.handler_messages.sendMessage(msg2);
				}
				
				
			}
			if(GlobalMessages.messageInfos1!=null)
			{
				GlobalMessages.messageInfosFull1.msgList.addAll(GlobalMessages.messageInfos1.msgList);
			}
			if(GlobalMessages.messageInfos2!=null)
			{
				GlobalMessages.messageInfosFull2.msgList.addAll(GlobalMessages.messageInfos2.msgList);
			}
			if(GlobalMessages.messageInfos3!=null)
			{
				GlobalMessages.messageInfosFull3.msgList.addAll(GlobalMessages.messageInfos3.msgList);
			}
		}
		
	}
	
}







class getMessagesCountTask extends AsyncTask<String,Void,String>
{
	@Override
	protected String doInBackground(String... params) 
	{
		String result = ServiceClient.getNumberMessages(params[0]);		
		return result;
	}
	
	@Override
	protected void onPostExecute(String result)
	{
		if(!result.equals(""))
		{
			GlobalMessages.messagescount = result;
			if(Global.handler1!=null && Global.handlerSumNotifications!=null)
  	  		{
				Message msg = Global.handler1.obtainMessage();
				Global.handler1.sendMessage(msg);
			
				Message msg1 = Global.handlerSumNotifications.obtainMessage();
				Global.handlerSumNotifications.sendMessage(msg1);
				
				//ActHome.initHandler
  	  		}
		}
	}
}
package com.goomena.app.global;

import java.util.ArrayList;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Message;

import com.goomena.app.data.ServiceClient;
import com.goomena.app.models.Offer;
import com.goomena.app.models.OfferList;
import com.goomena.app.tools.Utils;

public class GlobalOffers {
	static Activity _activity;
	public static String offercount = "0";
	public static OfferList offerList;
	
	
	
	public static void getOffersCount(Activity activity)
	{
		new getOffersCountTask(activity).execute();
	
	}
}


class getOffersCountTask extends AsyncTask<Void,Void,OfferList>
{
	
	Activity _activity;
	getOffersCountTask(Activity activity)
	{
		this._activity = activity;
	}
	
	SharedPreferences settings = Utils.getSharedPreferences(_activity);
	String userid = settings.getString("userid", "");
	String genderid = settings.getString("genderid", "");
	String zip = settings.getString("zipcode", "");
	String lat = settings.getString("lat", "");
	String lng = settings.getString("lng", "");
	
	@Override
	protected void onPreExecute()
	{
		GlobalOffers.offerList = new OfferList();
		GlobalOffers.offerList.offers = new ArrayList<Offer>();
		
		
	}
	@Override
	protected OfferList doInBackground(Void... params) 
	{
		//mode: 0 for new
		
		return ServiceClient.getOffers(String.valueOf(0), userid, zip, lat, lng,"0");
	}
	
	
	
	@Override
	protected void onPostExecute(OfferList result)
	{
		if(result!=null &&  result.offers!=null)
		{
			
			GlobalOffers.offercount = Integer.toString(result.offers.size());
		if(Global.handler1!=null && Global.handlerSumNotifications!=null)
  	  	{
			Message msg = Global.handler1.obtainMessage();
			Global.handler1.sendMessage(msg);
			
			Message msg1 = Global.handlerSumNotifications.obtainMessage();
			Global.handlerSumNotifications.sendMessage(msg1);
			
  	  	}
		}
	}
}
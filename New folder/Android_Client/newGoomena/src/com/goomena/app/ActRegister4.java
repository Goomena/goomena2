package com.goomena.app;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.goomena.app.R;
import com.goomena.app.data.ProfileManager;
import com.goomena.app.models.ImagePath;
import com.goomena.app.models.UserProfile;
import com.goomena.app.tools.Log;
import com.goomena.app.tools.Utils;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RelativeLayout;

public class ActRegister4 extends Activity
{
	public final static int CAMERA_REQUEST = 1888;	
	public final static int GALLERY_REQUEST = 1889;	
	public final static int CHANGE_DEFAULT_REQUEST = 1890;
	
	RelativeLayout relativeLayout1,relativeLayout2,relativeLayout3,relativeLayout4,relativeLayout5,relativeLayout6,relativeLayout7;
	
	private Bitmap bitmap;
	private Uri imageUri=null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_register4);
		
		relativeLayout4 = (RelativeLayout) findViewById(R.id.relativeLayout4);
		relativeLayout6 = (RelativeLayout) findViewById(R.id.relativeLayout6);
		relativeLayout7 = (RelativeLayout) findViewById(R.id.relativeLayout7);
     	
     	relativeLayout4.setOnClickListener(clickListener);
     	relativeLayout6.setOnClickListener(clickListener);
     	relativeLayout7.setOnClickListener(clickListener);
	}
	
	public void onActivityResult(int requestCode, int resultCode, Intent data) 
	{
		super.onActivityResult(requestCode,resultCode,data);

		if(requestCode==CAMERA_REQUEST)
		{
			bitmap = null;
			Uri selectedImageUri = imageUri;		
			
			if(resultCode!=0)
			{	
				Intent intent = new Intent();
				
				intent.setClass(ActRegister4.this, ActRegister5.class);
				intent.putExtra("MYimage", selectedImageUri.toString());
				startActivity(intent);
				overridePendingTransition(R.anim.right_in, R.anim.left_out);
			}	
		}
		else if(requestCode==GALLERY_REQUEST)
		{
			bitmap = null;
			//Log.v("APP", "rtrtrtrt "+data);
			if(data!=null)
			{
				String result = data.getStringExtra("result");
				//Log.v("APP", "rtrtrtrt "+result);
				//Utils.getSharedPreferences(this).edit().putString("imageurl", "file://"+result).commit();
				
				Intent intent = new Intent();
				intent.setClass(ActRegister4.this, ActRegister5.class);
				intent.putExtra("MYimage", "file://"+result);
				startActivity(intent);
				overridePendingTransition(R.anim.right_in, R.anim.left_out);
			}
		}
	}
	
	OnClickListener clickListener = new OnClickListener() 
	{
		@Override
		public void onClick(View v) 
		{
			if(v == relativeLayout4)
			{
				Intent i = new Intent();
                i.setClass(ActRegister4.this, MultiPhotoSellectOne.class);
                startActivityForResult(i,GALLERY_REQUEST);
			}
			else if(v == relativeLayout6)
			{
				String name =   dateToString(new Date(),"yyyy-MM-dd-hh-mm-ss"); 
                ContentValues values = new ContentValues();
                values.put(MediaStore.Images.Media.TITLE, name);
                values.put(MediaStore.Images.Media.DESCRIPTION,"Image capture by camera");
                imageUri = ActRegister4.this.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
                startActivityForResult(intent, CAMERA_REQUEST);
			}
			else if(v == relativeLayout7)
			{
				
				Intent intent = new Intent();
				intent.setClass(ActRegister4.this, ActWelcome.class);
				intent.putExtra("MYimage", "NOT");
				startActivity(intent);
				overridePendingTransition(R.anim.right_in, R.anim.left_out);
			}
		}
	};
	
	public String dateToString(Date date, String format) 
	{ 
		SimpleDateFormat df = new SimpleDateFormat(format); 
	   	return df.format(date); 
	}  

}
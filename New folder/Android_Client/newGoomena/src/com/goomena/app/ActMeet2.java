package com.goomena.app;


import java.util.ArrayList;

import com.goomena.app.adapters.FullScreenProfileAdapter;
import com.goomena.app.data.ServiceClient;
import com.goomena.app.models.UserProfile;
import com.goomena.app.models.UserProfilesList;
import com.goomena.app.tools.Log;
import com.goomena.app.tools.Utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

public class ActMeet2 extends Activity
{
	private ViewPager viewPager;
	private ActMeet1 adapter;
	
	ArrayList<UserProfile> userProfiles;
	ArrayList<UserProfile> userProfilesFull;
	
	Dialog Dwaiting;
	Animation animFadein;
	Animation animRotScal;
	TextView dialog_waiting_textWaiting;
	ImageView dialog_waiting_bgLogo,dialog_waiting_FrLogo;
	
	String Gender;
	
	@SuppressLint("NewApi") 
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_meet2);
		
		///-----+
		Dwaiting = new Dialog(this, R.style.DialogSlideAnim);
		Dwaiting.setContentView(R.layout.dialog_waiting);
		animFadein = AnimationUtils.loadAnimation(this, R.anim.d_wait_fr_load);
		animRotScal = AnimationUtils.loadAnimation(this, R.anim.d_wait_bg_logo);
		dialog_waiting_textWaiting = (TextView) Dwaiting.findViewById(R.id.dialog_waiting_textWaiting);
		dialog_waiting_bgLogo = (ImageView) Dwaiting.findViewById(R.id.dialog_waiting_bgLogo);
		dialog_waiting_FrLogo = (ImageView) Dwaiting.findViewById(R.id.dialog_waiting_FrLogo);
		dialog_waiting_textWaiting.startAnimation(animFadein);
		dialog_waiting_bgLogo.startAnimation(animRotScal);
		/////----
		
		userProfilesFull = new ArrayList<UserProfile>();
		
		viewPager = (ViewPager) findViewById(R.id.view_pager);
		//viewPager.setAccessibilityLiveRegion(MODE_MULTI_PROCESS);
		viewPager.setPageMargin(-(getWindowManager().getDefaultDisplay().getWidth()/22));
		
		//adapter = new ActMeet1(this,null);
		//viewPager.setAdapter(adapter);
		Gender = getIntent().getStringExtra("YourChoice");
		
		
		if(!Gender.equals("1"))
		{
			dialog_waiting_FrLogo.setImageResource(R.drawable.woman_loading);
		}
		
		new getProfilesTask().execute();
		
		adapter = new ActMeet1(ActMeet2.this,userProfilesFull);
		viewPager.setAdapter(adapter);
		
		
		viewPager.setOnPageChangeListener(new OnPageChangeListener() {
			
			@Override
			public void onPageSelected(int arg0) {
				// TODO Auto-generated method stub
				Log.v("APP", "wwwwec "+arg0+" "+adapter.getCount());
				
				if(arg0==adapter.getCount()-1)
				{
					new getProfilesTask().execute();
				}
			}
			
			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// TODO Auto-generated method stub
			}
			
			@Override
			public void onPageScrollStateChanged(int arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		
	}
	
	
	
	class getProfilesTask extends AsyncTask<Void, Void, Integer>
	{
		@Override
		protected void onPreExecute()
		{
			Dwaiting.show();
			dialog_waiting_textWaiting.startAnimation(animFadein);
			dialog_waiting_bgLogo.startAnimation(animRotScal);
		}
	
		@Override
		protected Integer doInBackground(Void... params)
		{
			UserProfilesList list;
			if(Gender.equals("1"))
			{
				list = ServiceClient.getSearchResultsPublic("1", null, null, "100", Gender, String.valueOf(userProfilesFull.size()), "10" , "GR");
				//list = ServiceClient.getNewMembers("67884" ,String.valueOf(userProfilesFull.size()),"15",Gender, "0", "GR");
			}
			else
			{
				list = ServiceClient.getSearchResultsPublic("1", null, null, "100", Gender, String.valueOf(userProfilesFull.size()), "10" , "GR");
				//list = ServiceClient.getNewMembers("73576" ,String.valueOf(userProfilesFull.size()),"15",Gender, "0", "GR");
			}
	 
			if(list!=null && list.Profiles!=null)
			{
				userProfiles = list.Profiles;
				return 1;
			}
			return 0;
		}

		@Override
		protected void onPostExecute(Integer result)
		{
			
				if(result==1)
				{
					userProfilesFull.addAll(userProfiles);
					//adapter.setItems(userProfilesFull);	
					adapter.notifyDataSetChanged();
					
				}
				Dwaiting.cancel();
			
		}
	}

}

package com.goomena.app.models;

public class Message
{    
    public String MessageId;     
    public String FromProfileId;     
    public String ToProfileId;       
    public String DateTimeSent;     
    public String Body;     
    public String Subject;     
    public boolean IsSent;     
    public boolean IsReceived;
    
    public boolean isRead;
    
    public String statusId;

}
package com.goomena.app.models;

public class GetEditProfile {
	
	public class ProfileEditPersonal 
	{
		public String FirstName;
		public String LastName;	        
	    public String EmailAddress;	        
	    public String Address;	        
	    public String PhoneNumber;	        
	    public String MobilePhone;
	};
	
	public class ProfileEditLookingFor 
	{
		public int RelationshipStatus;
		public String TODShort;	        
	    public String TODFriendship;	        
	    public String TODLongTerm;	        
	    public String TODMutually;	        
	    public String TODMarried;
	    public String TODCasual;
	};
	
	public class ProfileEditPersonalInfoLists 
	{
		public int Height;
		public int BodyType;	        
	    public int EyeColor;	        
	    public int HairColor;	        
	    public int Children;	        
	    public int Ethnicity;
	    public int Religion;
	    public int Smoking;	        
	    public int Drinking;
	    public int BreastSize;
	};
	
	public class ProfileEditOtherDetails 
	{
		public String Occupation;
		public int Education;	        
	    public int AnnualIncome;
	};
	
	public class ProfileEditLocation 
	{
		public String Country;	        
	    public String Region;	        
	    public String City;	        
	    public String ZipCode;	          
	};

}




package com.goomena.app.models;

import java.io.Serializable;

public class UserProfile implements Serializable {
	
	public String FirstName;
    public String LastName;
    public String ProfileId;
    public String UserName;
    public String UserAge;
    public String UserCity;
    public String UserRegion;
    public String DateRegister;
    public String GenderId;
    public boolean IsOnline;
    public int PhotosCount;
    public String IsVip;
    public String ZipCode;
    public String Latitude;
    public String Longtitude;
    public String UserCountry;
    public String Password;
    public String ImageUrl;
    public String ReferrerParentId;
}

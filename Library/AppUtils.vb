﻿Imports Dating.Client.Admin.WSReferences.DLL.AdminWS
Imports AdminWS = Dating.Client.Admin.WSReferences.DLL.AdminWS

Public Class AppUtils
    Inherits Dating.Server.Core.DLL.AppUtils


    Public Shared Function ViewIPOnMap(ipAddr As String) As Boolean
        Try
            ShowProgressWin("Loading config please wait...", 50 / 100)

            Dim UniCMSDB As New AdminWS.UniCMSDB()

            System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " GetSYS_Config")
            GlbAdminWS.CheckResult(GlbAdminWS.Proxies.GetSYS_Config(GlbAdminWS.Headers, UniCMSDB))
            System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " END GetSYS_Config")

            Dim url As String = ""
            Dim cnt As Integer
            For cnt = 0 To UniCMSDB.SYS_Config.Rows.Count - 1
                Dim row As AdminWS.UniCMSDB.SYS_ConfigRow = UniCMSDB.SYS_Config.Rows(cnt)
                If (row.ConfigName = "ip_to_map") Then
                    If (Not row.IsConfigValueNull()) Then url = row.ConfigValue
                    Exit For
                End If
            Next

            If (String.IsNullOrEmpty(url)) Then
                MsgBox("Please configure a ""URL view IP on MAP"" setting, to enable viewing IP on map!", MsgBoxStyle.Exclamation)
                Return False
            End If

            ShowProgressWin("Openning browser ...", 50 / 100)
            url = String.Format(url, ipAddr)
            System.Diagnostics.Process.Start(url)
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        Finally
            HideWaitWin()
        End Try

        Return True
    End Function


    Public Shared Function ViewOnMap(latitude As Double, longitude As Double, Optional radius As Integer = 10, Optional zoom As Integer = 6) As Boolean
        Try
            'ShowProgressWin("Loading config please wait...", 50 / 100)

            'Dim UniCMSDB As New AdminWS.UniCMSDB()

            'System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " " & Me.Name & "->"  & " GetSYS_Config")
            'CheckWebServiceCallResult(GlbAdminWS.gWS.GetSYS_Config(GlbAdminWS.gHeaders, UniCMSDB))
            'System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " " & Me.Name & "->"  & " END GetSYS_Config")

            'Dim url As String = ""
            'Dim cnt As Integer
            'For cnt = 0 To UniCMSDB.SYS_Config.Rows.Count - 1
            '    Dim row As AdminWS.UniCMSDB.SYS_ConfigRow = UniCMSDB.SYS_Config.Rows(cnt)
            '    If (row.ConfigName = "ip_to_map") Then
            '        If (Not row.IsConfigValueNull()) Then url = row.ConfigValue
            '        Exit For
            '    End If
            'Next

            'If (String.IsNullOrEmpty(url)) Then
            '    MsgBox("Please configure a ""URL view IP on MAP"" setting, to enable viewing IP on map!", MsgBoxStyle.Exclamation)
            '    Return False
            'End If

            ShowProgressWin("Openning browser ...", 50 / 100)
            Dim mapPath As String = "http://www.goomena.com/map.aspx?lat=" & latitude & "&lng=" & longitude & "&radius=" & radius & "&zoom=" & zoom
            System.Diagnostics.Process.Start(mapPath)
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        Finally
            HideWaitWin()
        End Try

        Return True
    End Function



    Public Shared Sub CopyEUS_ProfilesRowsData(ByRef EUS_Profiles As AdminWS.DSMembers.EUS_ProfilesDataTable, ByRef TargetRowIndex As Integer, ByRef SourceRowIndex As Integer)

        ''''''''''''''''''''''''''''''''''''''''''''
        ' copy data from mirror record to master, overwrite old user data with new
        ''''''''''''''''''''''''''''''''''''''''''''

        ' copy data that is not read only
        Dim cnt As Integer = 0
        Dim drTarget As AdminWS.DSMembers.EUS_ProfilesRow = EUS_Profiles.Rows(TargetRowIndex)
        Dim drSource As AdminWS.DSMembers.EUS_ProfilesRow = EUS_Profiles.Rows(SourceRowIndex)
        For cnt = 0 To EUS_Profiles.Columns.Count - 1
            Dim dc As DataColumn = EUS_Profiles.Columns(cnt)
            If (dc.ReadOnly OrElse dc.ColumnName = "IsMaster" OrElse dc.ColumnName = "MirrorProfileID") Then
                Continue For
            End If

            If (drTarget.RowState = DataRowState.Unchanged) Then
                Dim modified As Boolean = drTarget.IsNull(cnt) AndAlso Not drSource.IsNull(cnt)
                modified = modified OrElse Not drTarget.IsNull(cnt) AndAlso drSource.IsNull(cnt)
                modified = modified OrElse Not drTarget.IsNull(cnt) AndAlso Not drSource.IsNull(cnt) AndAlso (drTarget(cnt) <> drSource(cnt))

                If modified Then
                    drTarget.SetModified()
                End If
            End If

            drTarget(cnt) = drSource(cnt)
        Next

    End Sub


    Public Shared Sub CopyFromEUS_ProfilesToEUS_Profiles_AdminUpdater(ByRef ds As AdminWS.DSMembers)

        ''''''''''''''''''''''''''''''''''''''''''''
        ' copy data from mirror record to master, overwrite old user data with new
        ''''''''''''''''''''''''''''''''''''''''''''
        Dim drTargetMirror As AdminWS.DSMembers.EUS_Profiles_AdminUpdaterRow = Nothing
        Dim drTargetMaster As AdminWS.DSMembers.EUS_Profiles_AdminUpdaterRow = Nothing

        Dim drSourceMirror As AdminWS.DSMembers.EUS_ProfilesRow = Nothing
        Dim drSourceMaster As AdminWS.DSMembers.EUS_ProfilesRow = Nothing

        Dim drsTarget As AdminWS.DSMembers.EUS_Profiles_AdminUpdaterRow() = ds.EUS_Profiles_AdminUpdater.Select("IsMaster=0")
        If (drsTarget.Length > 0) Then
            drTargetMirror = drsTarget(0)
        End If

        drsTarget = ds.EUS_Profiles_AdminUpdater.Select("IsMaster=1")
        If (drsTarget.Length > 0) Then
            drTargetMaster = drsTarget(0)
        End If


        Dim drsSource As AdminWS.DSMembers.EUS_ProfilesRow() = ds.EUS_Profiles.Select("IsMaster=0")
        If (drsSource.Length > 0) Then
            drSourceMirror = drsSource(0)
        End If

        drsSource = ds.EUS_Profiles.Select("IsMaster=1")
        If (drsSource.Length > 0) Then
            drSourceMaster = drsSource(0)
        End If


        If (drSourceMirror IsNot Nothing AndAlso drTargetMirror Is Nothing) Then
            drTargetMirror = ds.EUS_Profiles_AdminUpdater.NewEUS_Profiles_AdminUpdaterRow()
        End If

        If (drSourceMaster IsNot Nothing AndAlso drTargetMaster Is Nothing) Then
            drTargetMaster = ds.EUS_Profiles_AdminUpdater.NewEUS_Profiles_AdminUpdaterRow()
        End If


        Dim continueProcessing As Boolean = True
        ' case of deleted profile 
        If (drSourceMaster Is Nothing AndAlso drSourceMirror Is Nothing) Then
            drTargetMaster.Delete()
            drTargetMirror.Delete()
            continueProcessing = False
        End If


        If (continueProcessing) Then

            ' copy data that is not read only
            Dim cnt As Integer = 0
            Dim columnName As String = ""
            For cnt = 0 To ds.EUS_Profiles_AdminUpdater.Columns.Count - 1
                Dim dc As DataColumn = ds.EUS_Profiles_AdminUpdater.Columns(cnt)
                If (dc.ReadOnly) Then
                    Continue For
                End If

                columnName = dc.ColumnName

                If (drTargetMirror IsNot Nothing AndAlso drSourceMirror IsNot Nothing) Then
                    Dim modifiedMirror As Boolean = drTargetMirror.IsNull(columnName) AndAlso Not drSourceMirror.IsNull(columnName)
                    modifiedMirror = modifiedMirror OrElse Not drTargetMirror.IsNull(columnName) AndAlso drSourceMirror.IsNull(columnName)
                    modifiedMirror = modifiedMirror OrElse Not drTargetMirror.IsNull(columnName) AndAlso Not drSourceMirror.IsNull(columnName) AndAlso (drTargetMirror(columnName) <> drSourceMirror(columnName))

                    If modifiedMirror Then
                        drTargetMirror(columnName) = drSourceMirror(columnName)
                    End If
                End If


                If (drTargetMaster IsNot Nothing AndAlso drSourceMaster IsNot Nothing) Then
                    Dim modifiedMaster As Boolean = drTargetMaster.IsNull(columnName) AndAlso Not drSourceMaster.IsNull(columnName)
                    modifiedMaster = modifiedMaster OrElse Not drTargetMaster.IsNull(columnName) AndAlso drSourceMaster.IsNull(columnName)
                    modifiedMaster = modifiedMaster OrElse Not drTargetMaster.IsNull(columnName) AndAlso Not drSourceMaster.IsNull(columnName) AndAlso (drTargetMaster(columnName) <> drSourceMaster(columnName))

                    If modifiedMaster Then
                        drTargetMaster(columnName) = drSourceMaster(columnName)
                    End If
                End If

                'If drTargetMaster.IsNull(columnName) AndAlso Not drSourceMaster.IsNull(columnName) Then
                '    drTargetMaster(columnName) = drSourceMaster(columnName)
                'ElseIf Not drTargetMaster.IsNull(columnName) AndAlso drSourceMaster.IsNull(columnName) Then
                '    drTargetMaster.setnull(null(columnName))
                'ElseIf drTargetMaster(columnName) <> drSourceMaster(columnName) Then
                '    drTargetMaster(columnName) = drSourceMaster(columnName)
                'End If

            Next

        End If

    End Sub





    'Public Shared Sub ConvertDateToLocal(ByRef dt As System.Data.DataRowView)
    '    'Dim _dt As DataTable = dt.DataView.Table
    '    'For Each col As DataColumn In _dt.Columns
    '    '    If col.DataType.FullName = "System.DateTime" Then
    '    '        col.DateTimeMode = DataSetDateTime.UnspecifiedLocal
    '    '        'For cnt = 0 To dt.DataView.Count - 1
    '    '        If (dt.Item(col.ColumnName) IsNot System.DBNull.Value) Then
    '    '            dt.Item(col.ColumnName) = DateControlsHandler.GetLocalDate(dt.Item(col.ColumnName))
    '    '        End If
    '    '        'Next
    '    '    End If
    '    'Next
    'End Sub


    'Public Shared Sub ConvertDateToUTC(ByRef dt As DataTable)
    '    'For Each col As DataColumn In dt.Columns
    '    '    If col.DataType.FullName = "System.DateTime" Then
    '    '        For cnt = 0 To dt.Rows.Count - 1
    '    '            If (Not dt.Rows(cnt).IsNull((col.ColumnName))) Then
    '    '                dt.Rows(cnt)(col.ColumnName) = DirectCast(dt.Rows(cnt)(col.ColumnName), DateTime).ToUniversalTime()
    '    '            End If
    '    '        Next
    '    '    End If
    '    'Next
    'End Sub





    Public Shared Sub SetDateEditControls_DisplayText(Ctls As System.Windows.Forms.Control.ControlCollection)
        For cnt1 = 0 To Ctls.Count - 1
            If (Ctls(cnt1).GetType() Is GetType(DevExpress.XtraEditors.DateEdit)) Then

                Dim ctl As DevExpress.XtraEditors.DateEdit = Ctls(cnt1)
                ctl.Properties.ExportMode = DevExpress.XtraEditors.Repository.ExportMode.DisplayText
                'ctl.Enabled = False
                AddHandler ctl.CustomDisplayText, AddressOf GridHelper.dateedit_CustomDisplayText
            Else
                If (Ctls(cnt1).Controls.Count > 0) Then
                    SetDateEditControls_DisplayText(Ctls(cnt1).Controls)
                End If
            End If
        Next
    End Sub

    Public Shared Function IdealTextColor(bg As Color) As Color
        Dim foreColor As Color = Nothing
        Try


            Dim nThreshold As Integer = 105
            Dim bgDelta As Integer = Convert.ToInt32(Math.Round((bg.R * 0.299) + (bg.G * 0.587) + (bg.B * 0.114), 0))

            foreColor = If((255 - bgDelta < nThreshold), Color.Black, Color.White)
        Catch ex As Exception

        End Try
        Return foreColor
    End Function


End Class

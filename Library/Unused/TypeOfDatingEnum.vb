﻿

Public Enum TypeOfDatingEnum
    None = 0
    ShortTermRelationship = 1
    Friendship = 2
    LongTermRelationship = 3
    MutuallyBeneficialArrangements = 4
    MarriedDating = 5
    AdultDating_Casual = 6
End Enum

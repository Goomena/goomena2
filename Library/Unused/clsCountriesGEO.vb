﻿Imports Dating.Client.Admin.WSReferences.DLL.AdminWS
Imports AdminWS = Dating.Client.Admin.WSReferences.DLL.AdminWS

Public Class clsCountriesGEO
    Private Shared ds As AdminWS.DSLists

    Public Sub New()
    End Sub


    Public Shared Sub Load()

        ds = New AdminWS.DSLists()
        Dim Params As New AdminWS.CountriesGEORetrieveParams()

        System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " GetSYS_CountriesGEO")
        CheckWebServiceCallResult(gAdminWS.GetSYS_CountriesGEO(gAdminWSHeaders, ds, Params))
        System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " END GetSYS_CountriesGEO")
    End Sub


    Public Shared Function GetData() As AdminWS.DSLists.SYS_CountriesGEODataTable
        Try
            If (ds Is Nothing) Then
                Load()
            End If
            Return ds.SYS_CountriesGEO
        Catch ex As Exception
            ErrorMsgBox(ex, "clsCountriesGEO->LoadGrid")
        End Try
        Return Nothing
    End Function


    Public Shared Function GetCountryName(countryCode As String) As String
        GetData()

        Dim txt As String = ""
        If (Not String.IsNullOrEmpty(countryCode) AndAlso countryCode <> "-1") Then
            Try
                'txt = ds.SYS_CountriesGEO.Single(Function(itm As ds.SYS_CountriesGEORow) itm.Iso = countryCode).PrintableName
                Dim cnt As Integer
                For cnt = 0 To ds.SYS_CountriesGEO.Rows.Count - 1
                    If (ds.SYS_CountriesGEO.Rows(cnt)("Iso") = countryCode) Then
                        txt = ds.SYS_CountriesGEO.Rows(cnt)("PrintableName")
                        Exit For
                    End If
                Next
            Catch ex As Exception

            End Try
        End If
        Return txt
    End Function



    Public Shared Function GetCountryCode(countryName As String) As String
        GetData()

        Dim txt As String = ""
        If (Not String.IsNullOrEmpty(countryName)) Then
            Try
                'txt = Lists.gDSLists.SYS_CountriesGEO.Single(Function(itm As DSLists.SYS_CountriesGEORow) itm.Name = countryName.ToUpper()).Iso
                Dim cnt As Integer
                For cnt = 0 To ds.SYS_CountriesGEO.Rows.Count - 1
                    If (ds.SYS_CountriesGEO.Rows(cnt)("Name") = countryName.ToUpper()) Then
                        txt = ds.SYS_CountriesGEO.Rows(cnt)("Iso")
                        Exit For
                    End If
                Next
            Catch ex As Exception

            End Try
        End If
        Return txt
    End Function


End Class

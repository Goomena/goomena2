﻿Imports Dating.Client.Admin.WSReferences.DLL.AdminWS
Imports AdminWS = Dating.Client.Admin.WSReferences.DLL.AdminWS


Public Class UCPhotosListHelper
    Public Enum Allow_IsDefaultEnum
        NotSet = 0
        Allowed = 1
        Reset = 2
        NotAllowed = 4
    End Enum


    Public Shared Function Check_IsDefault(dr As AdminWS.DSMembersViews.CustomerPhotosViewRow) As Boolean
        Dim result As Boolean
        If (dr.IsIsDeletedNull() OrElse dr.IsDeleted = False) Then
            If (Not dr.IsIsDefaultNull() AndAlso dr.IsDefault = True) Then
                If (Not dr.IsHasAprovedNull() AndAlso dr.HasAproved = True) Then
                    If (dr.IsHasDeclinedNull() OrElse dr.HasDeclined = False) Then
                        If (Not dr.IsDisplayLevelNull() AndAlso dr.DisplayLevel = 0) Then
                            result = True
                        End If
                    End If
                End If
            End If
        End If
        Return result
    End Function


    Public Shared Function Permit_IsDefault(ByRef dr As AdminWS.DSMembersViews.CustomerPhotosViewRow) As Allow_IsDefaultEnum
        Dim result As Allow_IsDefaultEnum = Allow_IsDefaultEnum.NotAllowed

        If (dr.IsDefault = True) Then

            Dim IsNotDeleted As Boolean = dr.IsIsDeletedNull() OrElse dr.IsDeleted = False
            Dim HasNotDeclined As Boolean = dr.IsHasDeclinedNull() OrElse dr.HasDeclined = False
            Dim isDisplayLevel As Boolean = Not dr.IsHasDeclinedNull() AndAlso dr.DisplayLevel = 0
            Dim HasAproved As Boolean = Not dr.IsHasDeclinedNull() AndAlso dr.HasAproved = True

            If (IsNotDeleted AndAlso HasNotDeclined AndAlso isDisplayLevel AndAlso HasAproved) Then

                If (dr.IsDefault = True AndAlso dr.DefaultPhotoByUser > 0 AndAlso dr.DefaultPhotoByUser <> dr.CustomerPhotosID) Then
                    If (MsgBox("The member has already defined his default photo." & vbCrLf & "Continue changing default photo?", MsgBoxStyle.YesNo, "Default Photo") = MsgBoxResult.No) Then
                        result = Allow_IsDefaultEnum.Reset
                        'dr.IsDefault = False
                        'layoutViewPhotos.SetFocusedRowCellValue("IsDefault", False)
                    Else
                        result = Allow_IsDefaultEnum.Allowed

                        'dbRowPhotoChanged = DirectCast(layoutViewPhotos.GetRow(layoutViewPhotos.FocusedRowHandle), System.Data.DataRowView).Row
                    End If
                Else
                    result = Allow_IsDefaultEnum.Allowed
                    'dbRowPhotoChanged = DirectCast(layoutViewPhotos.GetRow(layoutViewPhotos.FocusedRowHandle), System.Data.DataRowView).Row
                End If

            Else

                MsgBox("Sorry, current photo cannot be default.", MsgBoxStyle.OkOnly, "Default Photo")
                'dr.IsDefault = False
                ''layoutViewPhotos.SetFocusedRowCellValue("IsDefault", False)
            End If
        Else
            result = Allow_IsDefaultEnum.Allowed
            ' dbRowPhotoChanged = DirectCast(layoutViewPhotos.GetRow(layoutViewPhotos.FocusedRowHandle), System.Data.DataRowView).Row
        End If

        Return result
    End Function


    Public Shared Function Allow_HasAproved_IsDefault(dr As AdminWS.DSMembersViews.CustomerPhotosViewRow) As Boolean
        Dim result As Boolean

        If (dr.IsIsDeletedNull() OrElse dr.IsDeleted = False) Then

            'HasAproved part
            If (dr.IsHasAprovedNull() OrElse dr.HasAproved = False) Then
                result = True
            End If

            'IsDefault part
            If (Not dr.IsDisplayLevelNull() AndAlso dr.DisplayLevel = 0) Then
                If (dr.IsIsDefaultNull() OrElse dr.IsDefault = False) Then
                    result = result OrElse True
                End If
            End If

        End If

        Return result
    End Function




    Public Shared Sub SetPhotoApprovedAndDefault(dbRow As AdminWS.DSMembersViews.CustomerPhotosViewRow)

        Dim DSChangedPhoto As New AdminWS.DSMembers()

        System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " UCPhotosListHelper->" & " GetEUS_CustomerPhotos_ByCustomerPhotosID")
        CheckWebServiceCallResult(gAdminWS.GetEUS_CustomerPhotos_ByCustomerPhotosID(gAdminWSHeaders, DSChangedPhoto, dbRow.CustomerPhotosID))
        System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " UCPhotosListHelper->" & " END GetEUS_CustomerPhotos_ByCustomerPhotosID")

        Dim dr As AdminWS.DSMembers.EUS_CustomerPhotosRow = DSChangedPhoto.EUS_CustomerPhotos.Rows(0)
        dr.HasAproved = True
        dr.HasDeclined = False

        System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " UCPhotosListHelper->" & " UpdateEUS_CustomerPhotos")
        CheckWebServiceCallResult(gAdminWS.UpdateEUS_CustomerPhotos(gAdminWSHeaders, DSChangedPhoto))
        System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " UCPhotosListHelper->" & " END UpdateEUS_CustomerPhotos")


        Dim parms As New AdminWS.DefaultPhotoParams()
        parms.CustomerID = dbRow.CustomerID
        parms.PhotoID = dbRow.CustomerPhotosID

        System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " UCPhotosListHelper->" & " SetDefaultEUS_CustomerPhotos")
        CheckWebServiceCallResult(gAdminWS.SetDefaultEUS_CustomerPhotos(gAdminWSHeaders, parms))
        System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " UCPhotosListHelper->" & " END SetDefaultEUS_CustomerPhotos")

    End Sub


End Class

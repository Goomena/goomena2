﻿Imports Dating.Client.Admin.WSReferences.DLL.AdminWS
Imports AdminWS = Dating.Client.Admin.WSReferences.DLL.AdminWS
Imports Dating.Client.Admin.WSReferences.DLL.AdminReferrersWS
Imports AdminReferrersWS = Dating.Client.Admin.WSReferences.DLL.AdminReferrersWS

Public Class clsUserLog

    Dim _rs As Library.Public.ClsMyADODB

    Private Property RS As Library.Public.ClsMyADODB
        Get
            If (_rs Is Nothing) Then
                'G.MasterConn
                'Dim o1 As New Library.Public.ClsMyConnectionString()
                'o1.m_ConnectionString = G.MasterConn.
                '_rs = New Library.Public.ClsMyADODB(
            End If
            Return _rs
        End Get
        Set(value As Library.Public.ClsMyADODB)
            _rs = value
        End Set
    End Property

    Dim StilData As Boolean

    Public Sub BeginWork(ByVal WorkID As Long, ByVal WorkTitle1 As String, ByVal WorkTitle2 As String)
        On Error GoTo er
        RS.AddNew()
        RS.SetFieldValue("UsersID", gUserID)
        ' rs.SetFieldValue("TerminalID", gTerminalID)
        RS.SetFieldValue("WorkID", WorkID)
        RS.SetFieldValue("DTWorkBegin", Now)
        RS.SetFieldValue("IsWorkEndOK", False)
        RS.SetFieldValue("WorkTitle1", WorkTitle1)
        RS.SetFieldValue("WorkTitle2", WorkTitle2)
        StilData = True
        Exit Sub
er:
        ErrorMsgBox("")
    End Sub

    Public Sub EndWork(ByVal WorkEndOK As Boolean, ByVal RecordID As Long, ByVal TableName As String, ByVal RecordADD As Boolean, ByVal RecordUPDATE As Boolean, ByVal RecordDELETE As Boolean)
        On Error GoTo er
        RS.SetFieldValue("DTWorkEnd", Now)
        RS.SetFieldValue("RecordID", RecordID)
        RS.SetFieldValue("SQLTableName", TableName)
        RS.SetFieldValue("RecordADD", RecordADD)
        RS.SetFieldValue("RecordUPDATE", RecordUPDATE)
        RS.SetFieldValue("RecordDELETE", RecordDELETE)
        RS.SetFieldValue("IsWorkEndOK", WorkEndOK)
        If StilData Then RS.Update()
        StilData = False
        Exit Sub
er:
        ErrorMsgBox("")
    End Sub

    Public Sub New()
        On Error GoTo er
        Dim sz As String = "SELECT * FROM UsersWorks WHERE UsersWorksID = 0"
        RS.Open(sz, Library.Public.ClsMyADODB.OpenMode.ReadWriteMode)
        StilData = False
        Exit Sub
er:
        ErrorMsgBox("")
    End Sub

    Protected Overrides Sub Finalize()
        Try
            If StilData Then EndWork(0, 0, "Closed dialog from X", 0, 0, 0)
            If (RS IsNot Nothing) Then
                RS.Close()
                RS = Nothing
            End If
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
        MyBase.Finalize()
    End Sub


    Public Shared Property MyAdminWS As AdminWS.Admin
    Public Shared Sub LogAdminAction(ProfileIdAffected As Integer, ActionDescription As String, NotesFromUser As String, Optional MakeAsyncWSCall As Boolean = True)

        If (gLoginRec Is Nothing) Then Return

        Try
            If (MyAdminWS Is Nothing) Then
                MyAdminWS = New AdminWS.Admin()
                MyAdminWS.Url = gAdminWS.Url
                AddHandler MyAdminWS.UpdateSYS_AdminActionsCompleted, AddressOf UpdateSYS_AdminActionsCompleted
            End If

            Dim dsSec As New AdminWS.dsSecurity
            Dim row As AdminWS.dsSecurity.SYS_AdminActionsRow = dsSec.SYS_AdminActions.NewSYS_AdminActionsRow()
            row.ActionDescription = ActionDescription
            row.Date_Time = DateTime.UtcNow
            If (Not String.IsNullOrEmpty(NotesFromUser)) Then
                row.NotesFromUser = NotesFromUser & vbCrLf & "Ver: " & My.Application.Info.Version.ToString()
            Else
                row.NotesFromUser = "Ver: " & My.Application.Info.Version.ToString()
            End If
            row.ProfileIdAffected = ProfileIdAffected
            row.SystemUserId = gLoginRec.UserID
            dsSec.SYS_AdminActions.AddSYS_AdminActionsRow(row)

            If (MakeAsyncWSCall) Then
                MyAdminWS.UpdateSYS_AdminActionsAsync(gAdminWSHeaders, dsSec, New With {Key ProfileIdAffected, ActionDescription, row.Date_Time})
            Else
                System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " UpdateSYS_AdminActions")
                CheckWebServiceCallResult(MyAdminWS.UpdateSYS_AdminActions(gAdminWSHeaders, dsSec))
                System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " END UpdateSYS_AdminActions")
            End If
        Catch ex As Exception
            ErrorMsgBox(ex, "Submitting user action")
        End Try
    End Sub

    Public Shared Sub UpdateSYS_AdminActionsCompleted(sender As Object, e As AdminWS.UpdateSYS_AdminActionsCompletedEventArgs)

    End Sub


    Public Shared Sub Dispose()
        If (MyAdminWS IsNot Nothing) Then
            MyAdminWS.CancelAsync(Nothing)
            MyAdminWS = Nothing
        End If
    End Sub

End Class

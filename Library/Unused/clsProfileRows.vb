﻿Imports Dating.Client.Admin.WSReferences.DLL.AdminWS
Imports AdminWS = Dating.Client.Admin.WSReferences.DLL.AdminWS

Public Class clsProfileRows

    Public Property MasterProfileID As Integer
    Public Property MirrorProfileID As Integer
    Public Property MasterRowIndex As Integer
    Public Property MirrorRowIndex As Integer
    Public Property DSMembers As DSMembers

    Private _profileId As Integer
    Private _masterRow As EUS_ProfilesRow
    Private _mirrorRow As EUS_ProfilesRow

    Private _isDataLoaded As Boolean



    Public Sub New(profileId As Integer)
        _profileId = profileId
    End Sub


    Public Function GetMirrorRow() As DSMembers.EUS_ProfilesRow
        _LoadData()
        Return _mirrorRow
    End Function


    Public Function GetMasterRow() As DSMembers.EUS_ProfilesRow
        _LoadData()
        Return _masterRow
    End Function


    Private Sub _LoadData()
        If (Not _isDataLoaded) Then
            Dim _ds As New DSMembers()
            Dim results As clsDataRecordErrorsReturn = gAdminWS.GetEUS_Profiles_ByProfileOrMirrorID(gLoginRec.Token, _ds, _profileId)

            Dim cnt As Integer = 0
            For cnt = 0 To _ds.EUS_Profiles.Rows.Count - 1
                Dim dr As DataRow = _ds.EUS_Profiles.Rows(cnt)
                If (dr("Status") = ProfileStatusEnum.NewProfile) Then
                    MasterProfileID = dr("ProfileID")
                    MasterRowIndex = cnt
                    MirrorProfileID = dr("ProfileID")
                    MirrorRowIndex = cnt
                Else

                    If (dr("IsMaster") = True) Then
                        MasterProfileID = dr("ProfileID")
                        MasterRowIndex = cnt
                    ElseIf (dr("IsMaster") = False) Then
                        MirrorProfileID = dr("ProfileID")
                        MirrorRowIndex = cnt
                    End If

                End If
            Next

            ' search for changed fields and show related indication buttons
            _masterRow = _ds.EUS_Profiles(MasterRowIndex)
            _mirrorRow = _ds.EUS_Profiles(MirrorRowIndex)

            DSMembers = _ds

            _isDataLoaded = True
        End If
    End Sub

End Class

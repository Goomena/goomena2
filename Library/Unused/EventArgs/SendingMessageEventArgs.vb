﻿Imports Dating.Client.Admin.WSReferences.DLL.AdminWS
Imports AdminWS = Dating.Client.Admin.WSReferences.DLL.AdminWS
Imports Dating.Client.Admin.WSReferences.DLL.AdminReferrersWS
Imports AdminReferrersWS = Dating.Client.Admin.WSReferences.DLL.AdminReferrersWS

Public Class SendingMessageEventArgs
    Inherits System.EventArgs

    Public Property EmailMessagesRow As AdminWS.UniCMSDB.SYS_EmailMessagesRow
    Public Property SendApplicationMessage As Boolean
    Public Property SendEmail As Boolean
    Public Property SMTPServerID As Integer?
    Public Property ToRecipients As String
    Public Property BCCRecipients As String

    Public Sub New()
        MyBase.New()
    End Sub

    Public Sub New(ByVal _EmailMessagesRow As AdminWS.UniCMSDB.SYS_EmailMessagesRow,
                   ByVal _SendApplicationMessage As Boolean,
                   ByVal _SendEmail As Boolean,
                   ByVal _SMTPServerID As Integer?,
                   ByVal _ToRecipients As String,
                   ByVal _BCCRecipients As String)

        MyBase.New()

        EmailMessagesRow = _EmailMessagesRow
        SendApplicationMessage = _SendApplicationMessage
        SendEmail = _SendEmail
        SMTPServerID = _SMTPServerID
        ToRecipients = _ToRecipients
        BCCRecipients = _BCCRecipients

    End Sub

End Class

﻿Imports Dating.Client.Admin.WSReferences.DLL.AdminWS
Imports AdminWS = Dating.Client.Admin.WSReferences.DLL.AdminWS
Imports Dating.Client.Admin.WSReferences.DLL.AdminReferrersWS
Imports AdminReferrersWS = Dating.Client.Admin.WSReferences.DLL.AdminReferrersWS
Imports Dating.Server.Core.DLL

Public Class clsConfigValues2

    Public Property auto_approve_new_profiles As String
    Public Property auto_approve_photos As String
    Public Property auto_approve_updating_profiles As String
    Public Property site_name As String
    Public Property image_thumb_url As String
    Public Property image_url As String
    Public Property image_thumb_server_path As String
    Public Property image_server_path As String
    Public Property view_profile_url As String
    Public Property zone_a As String
    Public Property zone_b As String
    Public Property zone_c As String
    Public Property members_online_minutes As String
    Public Property ip_to_map As String

    Public Property documents_server_path As String
    Public Property documents_url_path As String

    Public Property gref_documents_server_path As String
    Public Property gref_documents_url_path As String

    Public Property referrer_commission_conv_rate As Double
    Public Property referrer_commission_level_0 As Double
    Public Property referrer_commission_level_1 As Double
    Public Property referrer_commission_level_2 As Double
    Public Property referrer_commission_level_3 As Double
    Public Property referrer_commission_level_4 As Double
    Public Property referrer_commission_level_5 As Double
    Public Property referrer_commission_pending_percent As Double
    Public Property referrer_commission_pending_period As Double

    Public Property affiliate_commission_percent As Double

    Public Property supporter_inactivity_period_mins As Integer
    Public Property supporter_on_login_disable_auto_login As Boolean
    Public Property supporter_monitor_refresh_time_mins As Integer


    Public Property admin_controler_phone1 As String
    Public Property admin_controler_phone2 As String
    Public Property admin_controler_phone3 As String
    Public Property admin_controler_phone4 As String
    Public Property admin_controler_phone5 As String
    Public Property admin_controler_email As String

    Public Property fb_open_user_url As String
    Public Property fb_search_by_email_url As String

    Dim UniCMSDB As New AdminWS.UniCMSDB()

    Public Sub New()
    End Sub


    Public Sub Load()


        System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " GetSYS_Config")
        CheckWebServiceCallResult(gAdminWS.GetSYS_Config(gAdminWSHeaders, UniCMSDB))
        System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " END GetSYS_Config")

        Dim cnt As Integer
        For cnt = 0 To UniCMSDB.SYS_Config.Rows.Count - 1
            Dim row As AdminWS.UniCMSDB.SYS_ConfigRow = UniCMSDB.SYS_Config.Rows(cnt)
            Select Case row.ConfigName
                Case "members_online_minutes" : If (Not row.IsConfigValueNull()) Then members_online_minutes = row.ConfigValue
                Case "ip_to_map" : If (Not row.IsConfigValueNull()) Then ip_to_map = row.ConfigValue
                Case "auto_approve_new_profiles" : If (Not row.IsConfigValueNull()) Then auto_approve_new_profiles = row.ConfigValue
                Case "auto_approve_photos" : If (Not row.IsConfigValueNull()) Then auto_approve_photos = row.ConfigValue
                Case "auto_approve_updating_profiles" : If (Not row.IsConfigValueNull()) Then auto_approve_updating_profiles = row.ConfigValue
                Case "image_thumb_url" : If (Not row.IsConfigValueNull()) Then image_thumb_url = row.ConfigValue
                Case "image_url" : If (Not row.IsConfigValueNull()) Then image_url = row.ConfigValue
                Case "site_name" : If (Not row.IsConfigValueNull()) Then site_name = row.ConfigValue
                Case "view_profile_url" : If (Not row.IsConfigValueNull()) Then view_profile_url = row.ConfigValue
                Case "zone_a" : If (Not row.IsConfigValueNull()) Then zone_a = row.ConfigValue
                Case "zone_b" : If (Not row.IsConfigValueNull()) Then zone_b = row.ConfigValue
                Case "zone_c" : If (Not row.IsConfigValueNull()) Then zone_c = row.ConfigValue

                Case "image_thumb_server_path" : If (Not row.IsConfigValueNull()) Then image_thumb_server_path = row.ConfigValue
                Case "image_server_path" : If (Not row.IsConfigValueNull()) Then image_server_path = row.ConfigValue
                Case "documents_url_path" : If (Not row.IsConfigValueNull()) Then documents_url_path = row.ConfigValue
                Case "documents_server_path" : If (Not row.IsConfigValueNull()) Then documents_server_path = row.ConfigValue

                Case "gref_documents_url_path" : If (Not row.IsConfigValueNull()) Then gref_documents_url_path = row.ConfigValue
                Case "gref_documents_server_path" : If (Not row.IsConfigValueNull()) Then gref_documents_server_path = row.ConfigValue

                Case "referrer_commission_conv_rate" : If (Not row.IsConfigValueDoubleNull()) Then referrer_commission_conv_rate = row.ConfigValueDouble
                Case "referrer_commission_level_0" : If (Not row.IsConfigValueDoubleNull()) Then referrer_commission_level_0 = row.ConfigValueDouble
                Case "referrer_commission_level_1" : If (Not row.IsConfigValueDoubleNull()) Then referrer_commission_level_1 = row.ConfigValueDouble
                Case "referrer_commission_level_2" : If (Not row.IsConfigValueDoubleNull()) Then referrer_commission_level_2 = row.ConfigValueDouble
                Case "referrer_commission_level_3" : If (Not row.IsConfigValueDoubleNull()) Then referrer_commission_level_3 = row.ConfigValueDouble
                Case "referrer_commission_level_4" : If (Not row.IsConfigValueDoubleNull()) Then referrer_commission_level_4 = row.ConfigValueDouble
                Case "referrer_commission_level_5" : If (Not row.IsConfigValueDoubleNull()) Then referrer_commission_level_5 = row.ConfigValueDouble
                Case "referrer_commission_pending_percent" : If (Not row.IsConfigValueDoubleNull()) Then referrer_commission_pending_percent = row.ConfigValueDouble
                Case "referrer_commission_pending_period" : If (Not row.IsConfigValueDoubleNull()) Then referrer_commission_pending_period = row.ConfigValueDouble

                Case "affiliate_commission_percent" : If (Not row.IsConfigValueDoubleNull()) Then affiliate_commission_percent = row.ConfigValueDouble

                Case "supporter_inactivity_period_mins" : If (Not row.IsConfigValueDoubleNull()) Then supporter_inactivity_period_mins = row.ConfigValueDouble
                Case "supporter_on_login_disable_auto_login" : If (Not row.IsConfigValueDoubleNull()) Then supporter_on_login_disable_auto_login = (row.ConfigValueDouble <> 0)
                Case "supporter_monitor_refresh_time_mins" : If (Not row.IsConfigValueDoubleNull()) Then supporter_monitor_refresh_time_mins = row.ConfigValueDouble


                Case "admin_controler_phone1" : If (Not row.IsConfigValueNull()) Then admin_controler_phone1 = row.ConfigValue
                Case "admin_controler_phone2" : If (Not row.IsConfigValueNull()) Then admin_controler_phone2 = row.ConfigValue
                Case "admin_controler_phone3" : If (Not row.IsConfigValueNull()) Then admin_controler_phone3 = row.ConfigValue
                Case "admin_controler_phone4" : If (Not row.IsConfigValueNull()) Then admin_controler_phone4 = row.ConfigValue
                Case "admin_controler_phone5" : If (Not row.IsConfigValueNull()) Then admin_controler_phone5 = row.ConfigValue
                Case "admin_controler_email" : If (Not row.IsConfigValueNull()) Then admin_controler_email = row.ConfigValue

                Case "fb_open_user_url" : If (Not row.IsConfigValueNull()) Then fb_open_user_url = row.ConfigValue
                Case "fb_search_by_email_url" : If (Not row.IsConfigValueNull()) Then fb_search_by_email_url = row.ConfigValue

            End Select
        Next
    End Sub




    Public Function GetSYS_ConfigValue(key As String) As String
        Dim configValue As String = Nothing
        Try

            If (UniCMSDB.SYS_Config.Rows.Count = 0) Then
                System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " GetSYS_Config")
                CheckWebServiceCallResult(gAdminWS.GetSYS_Config(gAdminWSHeaders, UniCMSDB))
                System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " END GetSYS_Config")
            End If

            Dim row As AdminWS.UniCMSDB.SYS_ConfigRow() = UniCMSDB.SYS_Config.Select("ConfigName='" & key & "'")
            Dim cnt As Integer
            For cnt = 0 To row.Length - 1
                If (Not row(cnt).IsConfigValueNull()) Then configValue = row(cnt).ConfigValue
            Next

        Catch ex As Exception
            Throw
        End Try

        Return configValue
    End Function


    Public Function GetSYS_ConfigValueDouble2(ByVal key As String) As Double
        Dim configValue As Double? = Nothing
        Try
            If (UniCMSDB.SYS_Config.Rows.Count = 0) Then
                System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " GetSYS_Config")
                CheckWebServiceCallResult(gAdminWS.GetSYS_Config(gAdminWSHeaders, UniCMSDB))
                System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " END GetSYS_Config")
            End If

            Dim row As AdminWS.UniCMSDB.SYS_ConfigRow() = UniCMSDB.SYS_Config.Select("ConfigName='" & key & "'")
            Dim cnt As Integer
            For cnt = 0 To row.Length - 1
                If (Not row(cnt).IsConfigValueDoubleNull()) Then configValue = row(cnt).ConfigValueDouble
            Next

        Catch ex As Exception
            Throw
        End Try

        Return clsNullable.NullTo(configValue)
    End Function


    Public Sub Save()


        Try
            Dim rows As AdminWS.UniCMSDB.SYS_ConfigRow() = Me.UniCMSDB.SYS_Config.Select("ConfigName='members_online_minutes'")
            If (rows.Length = 1) Then
                rows(0).ConfigValue = members_online_minutes
            Else
                Dim row As AdminWS.UniCMSDB.SYS_ConfigRow = Me.UniCMSDB.SYS_Config.NewSYS_ConfigRow()
                row.ConfigName = "members_online_minutes"
                row.ConfigValue = members_online_minutes
                Me.UniCMSDB.SYS_Config.Rows.Add(row)
            End If

        Catch ex As Exception
            Throw
        End Try


        Try
            Dim rows As AdminWS.UniCMSDB.SYS_ConfigRow() = Me.UniCMSDB.SYS_Config.Select("ConfigName='ip_to_map'")
            If (rows.Length = 1) Then
                rows(0).ConfigValue = ip_to_map
            Else
                Dim row As AdminWS.UniCMSDB.SYS_ConfigRow = Me.UniCMSDB.SYS_Config.NewSYS_ConfigRow()
                row.ConfigName = "ip_to_map"
                row.ConfigValue = ip_to_map
                Me.UniCMSDB.SYS_Config.Rows.Add(row)
            End If

        Catch ex As Exception
            Throw
        End Try


        Try
            Dim rows As AdminWS.UniCMSDB.SYS_ConfigRow() = Me.UniCMSDB.SYS_Config.Select("ConfigName='auto_approve_new_profiles'")
            If (rows.Length = 1) Then
                rows(0).ConfigValue = auto_approve_new_profiles
            Else
                Dim row As AdminWS.UniCMSDB.SYS_ConfigRow = Me.UniCMSDB.SYS_Config.NewSYS_ConfigRow()
                row.ConfigName = "auto_approve_new_profiles"
                row.ConfigValue = auto_approve_new_profiles
                Me.UniCMSDB.SYS_Config.Rows.Add(row)
            End If

        Catch ex As Exception
            Throw
        End Try

        Try
            Dim rows As AdminWS.UniCMSDB.SYS_ConfigRow() = Me.UniCMSDB.SYS_Config.Select("ConfigName='auto_approve_photos'")
            If (rows.Length = 1) Then
                rows(0).ConfigValue = auto_approve_photos
            Else
                Dim row As AdminWS.UniCMSDB.SYS_ConfigRow = Me.UniCMSDB.SYS_Config.NewSYS_ConfigRow()
                row.ConfigName = "auto_approve_photos"
                row.ConfigValue = auto_approve_photos
                Me.UniCMSDB.SYS_Config.Rows.Add(row)
            End If

        Catch ex As Exception
            Throw
        End Try


        Try
            Dim rows As AdminWS.UniCMSDB.SYS_ConfigRow() = Me.UniCMSDB.SYS_Config.Select("ConfigName='auto_approve_updating_profiles'")
            If (rows.Length = 1) Then
                rows(0).ConfigValue = auto_approve_updating_profiles
            Else
                Dim row As AdminWS.UniCMSDB.SYS_ConfigRow = Me.UniCMSDB.SYS_Config.NewSYS_ConfigRow()
                row.ConfigName = "auto_approve_updating_profiles"
                row.ConfigValue = auto_approve_updating_profiles
                Me.UniCMSDB.SYS_Config.Rows.Add(row)
            End If

        Catch ex As Exception
            Throw
        End Try


        Try
            Dim rows As AdminWS.UniCMSDB.SYS_ConfigRow() = Me.UniCMSDB.SYS_Config.Select("ConfigName='image_thumb_url'")
            If (rows.Length = 1) Then
                rows(0).ConfigValue = image_thumb_url
            Else
                Dim row As AdminWS.UniCMSDB.SYS_ConfigRow = Me.UniCMSDB.SYS_Config.NewSYS_ConfigRow()
                row.ConfigName = "image_thumb_url"
                row.ConfigValue = image_thumb_url
                Me.UniCMSDB.SYS_Config.Rows.Add(row)
            End If

        Catch ex As Exception
            Throw
        End Try


        Try
            Dim rows As AdminWS.UniCMSDB.SYS_ConfigRow() = Me.UniCMSDB.SYS_Config.Select("ConfigName='image_url'")
            If (rows.Length = 1) Then
                rows(0).ConfigValue = image_url
            Else
                Dim row As AdminWS.UniCMSDB.SYS_ConfigRow = Me.UniCMSDB.SYS_Config.NewSYS_ConfigRow()
                row.ConfigName = "image_url"
                row.ConfigValue = image_url
                Me.UniCMSDB.SYS_Config.Rows.Add(row)
            End If

        Catch ex As Exception
            Throw
        End Try


        Try
            Dim rows As AdminWS.UniCMSDB.SYS_ConfigRow() = Me.UniCMSDB.SYS_Config.Select("ConfigName='site_name'")
            If (rows.Length = 1) Then
                rows(0).ConfigValue = site_name
            Else
                Dim row As AdminWS.UniCMSDB.SYS_ConfigRow = Me.UniCMSDB.SYS_Config.NewSYS_ConfigRow()
                row.ConfigName = "site_name"
                row.ConfigValue = site_name
                Me.UniCMSDB.SYS_Config.Rows.Add(row)
            End If

        Catch ex As Exception
            Throw
        End Try


        Try
            Dim rows As AdminWS.UniCMSDB.SYS_ConfigRow() = Me.UniCMSDB.SYS_Config.Select("ConfigName='view_profile_url'")
            If (rows.Length = 1) Then
                rows(0).ConfigValue = view_profile_url
            Else
                Dim row As AdminWS.UniCMSDB.SYS_ConfigRow = Me.UniCMSDB.SYS_Config.NewSYS_ConfigRow()
                row.ConfigName = "view_profile_url"
                row.ConfigValue = view_profile_url
                Me.UniCMSDB.SYS_Config.Rows.Add(row)
            End If

        Catch ex As Exception
            Throw
        End Try


        Try
            Dim rows As AdminWS.UniCMSDB.SYS_ConfigRow() = Me.UniCMSDB.SYS_Config.Select("ConfigName='zone_a'")
            If (rows.Length = 1) Then
                rows(0).ConfigValue = zone_a
            Else
                Dim row As AdminWS.UniCMSDB.SYS_ConfigRow = Me.UniCMSDB.SYS_Config.NewSYS_ConfigRow()
                row.ConfigName = "zone_a"
                row.ConfigValue = zone_a
                Me.UniCMSDB.SYS_Config.Rows.Add(row)
            End If

        Catch ex As Exception
            Throw
        End Try


        Try
            Dim rows As AdminWS.UniCMSDB.SYS_ConfigRow() = Me.UniCMSDB.SYS_Config.Select("ConfigName='zone_b'")
            If (rows.Length = 1) Then
                rows(0).ConfigValue = zone_b
            Else
                Dim row As AdminWS.UniCMSDB.SYS_ConfigRow = Me.UniCMSDB.SYS_Config.NewSYS_ConfigRow()
                row.ConfigName = "zone_b"
                row.ConfigValue = zone_b
                Me.UniCMSDB.SYS_Config.Rows.Add(row)
            End If

        Catch ex As Exception
            Throw
        End Try


        Try
            Dim rows As AdminWS.UniCMSDB.SYS_ConfigRow() = Me.UniCMSDB.SYS_Config.Select("ConfigName='zone_c'")
            If (rows.Length = 1) Then
                rows(0).ConfigValue = zone_c
            Else
                Dim row As AdminWS.UniCMSDB.SYS_ConfigRow = Me.UniCMSDB.SYS_Config.NewSYS_ConfigRow()
                row.ConfigName = "zone_c"
                row.ConfigValue = zone_c
                Me.UniCMSDB.SYS_Config.Rows.Add(row)
            End If

        Catch ex As Exception
            Throw
        End Try


        ''''
        Try
            Dim rows As AdminWS.UniCMSDB.SYS_ConfigRow() = Me.UniCMSDB.SYS_Config.Select("ConfigName='referrer_commission_conv_rate'")
            If (rows.Length = 1) Then
                rows(0).ConfigValueDouble = referrer_commission_conv_rate
            Else
                Dim row As AdminWS.UniCMSDB.SYS_ConfigRow = Me.UniCMSDB.SYS_Config.NewSYS_ConfigRow()
                row.ConfigName = "referrer_commission_conv_rate"
                row.ConfigValueDouble = referrer_commission_conv_rate
                Me.UniCMSDB.SYS_Config.Rows.Add(row)
            End If

        Catch ex As Exception
            Throw
        End Try


        Try
            Dim rows As AdminWS.UniCMSDB.SYS_ConfigRow() = Me.UniCMSDB.SYS_Config.Select("ConfigName='referrer_commission_level_0'")
            If (rows.Length = 1) Then
                rows(0).ConfigValueDouble = referrer_commission_level_0
            Else
                Dim row As AdminWS.UniCMSDB.SYS_ConfigRow = Me.UniCMSDB.SYS_Config.NewSYS_ConfigRow()
                row.ConfigName = "referrer_commission_level_0"
                row.ConfigValueDouble = referrer_commission_level_0
                Me.UniCMSDB.SYS_Config.Rows.Add(row)
            End If

        Catch ex As Exception
            Throw
        End Try


        Try
            Dim rows As AdminWS.UniCMSDB.SYS_ConfigRow() = Me.UniCMSDB.SYS_Config.Select("ConfigName='referrer_commission_level_1'")
            If (rows.Length = 1) Then
                rows(0).ConfigValueDouble = referrer_commission_level_1
            Else
                Dim row As AdminWS.UniCMSDB.SYS_ConfigRow = Me.UniCMSDB.SYS_Config.NewSYS_ConfigRow()
                row.ConfigName = "referrer_commission_level_1"
                row.ConfigValueDouble = referrer_commission_level_1
                Me.UniCMSDB.SYS_Config.Rows.Add(row)
            End If

        Catch ex As Exception
            Throw
        End Try

        Try
            Dim rows As AdminWS.UniCMSDB.SYS_ConfigRow() = Me.UniCMSDB.SYS_Config.Select("ConfigName='referrer_commission_level_2'")
            If (rows.Length = 1) Then
                rows(0).ConfigValueDouble = referrer_commission_level_2
            Else
                Dim row As AdminWS.UniCMSDB.SYS_ConfigRow = Me.UniCMSDB.SYS_Config.NewSYS_ConfigRow()
                row.ConfigName = "referrer_commission_level_2"
                row.ConfigValueDouble = referrer_commission_level_2
                Me.UniCMSDB.SYS_Config.Rows.Add(row)
            End If

        Catch ex As Exception
            Throw
        End Try


        Try
            Dim rows As AdminWS.UniCMSDB.SYS_ConfigRow() = Me.UniCMSDB.SYS_Config.Select("ConfigName='referrer_commission_level_3'")
            If (rows.Length = 1) Then
                rows(0).ConfigValueDouble = referrer_commission_level_3
            Else
                Dim row As AdminWS.UniCMSDB.SYS_ConfigRow = Me.UniCMSDB.SYS_Config.NewSYS_ConfigRow()
                row.ConfigName = "referrer_commission_level_3"
                row.ConfigValueDouble = referrer_commission_level_3
                Me.UniCMSDB.SYS_Config.Rows.Add(row)
            End If

        Catch ex As Exception
            Throw
        End Try


        Try
            Dim rows As AdminWS.UniCMSDB.SYS_ConfigRow() = Me.UniCMSDB.SYS_Config.Select("ConfigName='referrer_commission_level_4'")
            If (rows.Length = 1) Then
                rows(0).ConfigValueDouble = referrer_commission_level_4
            Else
                Dim row As AdminWS.UniCMSDB.SYS_ConfigRow = Me.UniCMSDB.SYS_Config.NewSYS_ConfigRow()
                row.ConfigName = "referrer_commission_level_4"
                row.ConfigValueDouble = referrer_commission_level_4
                Me.UniCMSDB.SYS_Config.Rows.Add(row)
            End If

        Catch ex As Exception
            Throw
        End Try


        Try
            Dim rows As AdminWS.UniCMSDB.SYS_ConfigRow() = Me.UniCMSDB.SYS_Config.Select("ConfigName='referrer_commission_level_5'")
            If (rows.Length = 1) Then
                rows(0).ConfigValueDouble = referrer_commission_level_5
            Else
                Dim row As AdminWS.UniCMSDB.SYS_ConfigRow = Me.UniCMSDB.SYS_Config.NewSYS_ConfigRow()
                row.ConfigName = "referrer_commission_level_5"
                row.ConfigValueDouble = referrer_commission_level_5
                Me.UniCMSDB.SYS_Config.Rows.Add(row)
            End If

        Catch ex As Exception
            Throw
        End Try


        Try
            Dim rows As AdminWS.UniCMSDB.SYS_ConfigRow() = Me.UniCMSDB.SYS_Config.Select("ConfigName='referrer_commission_pending_percent'")
            If (rows.Length = 1) Then
                rows(0).ConfigValueDouble = referrer_commission_pending_percent
            Else
                Dim row As AdminWS.UniCMSDB.SYS_ConfigRow = Me.UniCMSDB.SYS_Config.NewSYS_ConfigRow()
                row.ConfigName = "referrer_commission_pending_percent"
                row.ConfigValueDouble = referrer_commission_pending_percent
                Me.UniCMSDB.SYS_Config.Rows.Add(row)
            End If

        Catch ex As Exception
            Throw
        End Try


        Try
            Dim rows As AdminWS.UniCMSDB.SYS_ConfigRow() = Me.UniCMSDB.SYS_Config.Select("ConfigName='referrer_commission_pending_period'")
            If (rows.Length = 1) Then
                rows(0).ConfigValueDouble = referrer_commission_pending_period
            Else
                Dim row As AdminWS.UniCMSDB.SYS_ConfigRow = Me.UniCMSDB.SYS_Config.NewSYS_ConfigRow()
                row.ConfigName = "referrer_commission_pending_period"
                row.ConfigValueDouble = referrer_commission_pending_period
                Me.UniCMSDB.SYS_Config.Rows.Add(row)
            End If

        Catch ex As Exception
            Throw
        End Try


        Try
            Dim rows As AdminWS.UniCMSDB.SYS_ConfigRow() = Me.UniCMSDB.SYS_Config.Select("ConfigName='affiliate_commission_percent'")
            If (rows.Length = 1) Then
                rows(0).ConfigValueDouble = affiliate_commission_percent
            Else
                Dim row As AdminWS.UniCMSDB.SYS_ConfigRow = Me.UniCMSDB.SYS_Config.NewSYS_ConfigRow()
                row.ConfigName = "affiliate_commission_percent"
                row.ConfigValueDouble = referrer_commission_pending_period
                Me.UniCMSDB.SYS_Config.Rows.Add(row)
            End If

        Catch ex As Exception
            Throw
        End Try



        System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " UpdateSYS_Config")
        CheckWebServiceCallResult(gAdminWS.UpdateSYS_Config(gAdminWSHeaders, Me.UniCMSDB))
        System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " END UpdateSYS_Config")


    End Sub


    Public Shared Function SetAutoApprove_forSupporter(enabled_AutoApproveForProfiles As Boolean, enabled_AutoApproveForPhotos As Boolean) As Boolean
        Dim result As Boolean
        Dim UniCMSDB As New AdminWS.UniCMSDB()

        System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " SetProfilesAutoApprove->GetSYS_Config")
        CheckWebServiceCallResult(gAdminWS.GetSYS_Config(gAdminWSHeaders, UniCMSDB))
        System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " SetProfilesAutoApprove->END GetSYS_Config")

        Dim rows As AdminWS.UniCMSDB.SYS_ConfigRow()


        Dim auto_approve_new_profiles As String = IIf(enabled_AutoApproveForProfiles, "1", "0")
        rows = UniCMSDB.SYS_Config.Select("ConfigName='auto_approve_new_profiles'")
        If (rows.Length = 1) Then
            rows(0).ConfigValue = auto_approve_new_profiles
        Else
            Dim row As AdminWS.UniCMSDB.SYS_ConfigRow = UniCMSDB.SYS_Config.NewSYS_ConfigRow()
            row.ConfigName = "auto_approve_new_profiles"
            row.ConfigValue = auto_approve_new_profiles
            UniCMSDB.SYS_Config.Rows.Add(row)
        End If


        Dim auto_approve_photos As String = IIf(enabled_AutoApproveForPhotos, "1", "0")
        rows = UniCMSDB.SYS_Config.Select("ConfigName='auto_approve_photos'")
        If (rows.Length = 1) Then
            rows(0).ConfigValue = auto_approve_photos
        Else
            Dim row As AdminWS.UniCMSDB.SYS_ConfigRow = UniCMSDB.SYS_Config.NewSYS_ConfigRow()
            row.ConfigName = "auto_approve_photos"
            row.ConfigValue = auto_approve_photos
            UniCMSDB.SYS_Config.Rows.Add(row)
        End If



        System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " SetProfilesAutoApprove->UpdateSYS_Config")
        CheckWebServiceCallResult(gAdminWS.UpdateSYS_Config(gAdminWSHeaders, UniCMSDB))
        System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " SetProfilesAutoApprove->END UpdateSYS_Config")
        result = True
        '  End If

        Return result
    End Function



End Class

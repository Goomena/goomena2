﻿

Public Enum TypeOfDatingEnum
    None = 0
    ShortTermRelationship = 1
    Friendship = 2
    LongTermRelationship = 3
    MutuallyBeneficialArrangements = 4
    MarriedDating = 5
    AdultDating_Casual = 6
End Enum


Public Enum PaymentForEnum
    Referrers
    Affiliates
End Enum



Public Enum FormActionEnum
    None = 0
    ADD = 1
    EDIT = 10
    DELETE = 100
End Enum


Public Enum UserRoleEnum
    None = 0
    Admin = 90
    Manager = 100
    Supporter = 101
End Enum


Public Enum UserMessagePositionEnum
    None = 0
    Limited = 1
    PhotoDisplayLevelChanged = 2
End Enum


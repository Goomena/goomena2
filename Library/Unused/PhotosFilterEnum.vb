﻿


Public Enum PhotosFilterEnum
    None = 0
    AllPhotos = 1
    NewPhotos = 2
    ApprovedPhotos = 3
    RejectedPhotos = 4
    DeletedPhotos = 5
    AutoApprovedPhotos = 6
End Enum



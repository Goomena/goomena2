﻿' Developer Express Code Central Example:
' How to use an unbound check box column to select grid rows
' 
' 
Imports Microsoft.VisualBasic
Imports System
Imports System.Drawing
Imports System.Collections
Imports System.Windows.Forms
Imports DevExpress.XtraGrid
Imports DevExpress.XtraGrid.Columns
Imports DevExpress.XtraEditors.Controls
Imports DevExpress.XtraEditors.Repository
Imports DevExpress.XtraGrid.Views.Base
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid.Views.Grid.ViewInfo

Public Class GridCheckMarksSelection
    Protected view_Renamed As GridView
    Protected selection As ArrayList
    Private column As GridColumn
    Private edit As RepositoryItemCheckEdit


    Public Sub New()
        MyBase.New()
        selection = New ArrayList()
    End Sub

    Public Sub New(ByVal view As GridView)
        Me.New()
        Me.View = view
    End Sub

    Protected Overridable Sub Attach(ByVal view As GridView)
        If view Is Nothing Then
            Return
        End If
        selection.Clear()
        Me.view_Renamed = view
        edit = TryCast(view.GridControl.RepositoryItems.Add("CheckEdit"), RepositoryItemCheckEdit)
        AddHandler edit.EditValueChanged, AddressOf edit_EditValueChanged

        column = view.Columns.Add()
        column.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False
        column.VisibleIndex = Integer.MaxValue
        column.FieldName = "CheckMarkSelection"
        column.Caption = "Mark"
        column.OptionsColumn.ShowCaption = False
        column.UnboundType = DevExpress.Data.UnboundColumnType.Boolean
        column.ColumnEdit = edit

        AddHandler view.CustomDrawColumnHeader, AddressOf View_CustomDrawColumnHeader
        AddHandler view.CustomDrawGroupRow, AddressOf View_CustomDrawGroupRow
        AddHandler view.CustomUnboundColumnData, AddressOf view_CustomUnboundColumnData
        AddHandler view.RowStyle, AddressOf view_RowStyle
        AddHandler view.MouseDown, AddressOf view_MouseDown ' clear selection
        AddHandler view.SelectionChanged, AddressOf View_SelectionChanged ' clear selection
    End Sub

    Protected Overridable Sub Detach()
        If view_Renamed Is Nothing Then
            Return
        End If
        If column IsNot Nothing Then
            column.Dispose()
        End If
        If edit IsNot Nothing Then
            view_Renamed.GridControl.RepositoryItems.Remove(edit)
            edit.Dispose()
        End If

        RemoveHandler view_Renamed.CustomDrawColumnHeader, AddressOf View_CustomDrawColumnHeader
        RemoveHandler view_Renamed.CustomDrawGroupRow, AddressOf View_CustomDrawGroupRow
        RemoveHandler view_Renamed.CustomUnboundColumnData, AddressOf view_CustomUnboundColumnData
        RemoveHandler view_Renamed.RowStyle, AddressOf view_RowStyle
        RemoveHandler view_Renamed.MouseDown, AddressOf view_MouseDown
        RemoveHandler view_Renamed.SelectionChanged, AddressOf View_SelectionChanged

        view_Renamed = Nothing
    End Sub

    Protected Sub DrawCheckBox(ByVal g As Graphics, ByVal r As Rectangle, ByVal Checked As Boolean)
        Dim info As DevExpress.XtraEditors.ViewInfo.CheckEditViewInfo
        Dim painter As DevExpress.XtraEditors.Drawing.CheckEditPainter
        Dim args As DevExpress.XtraEditors.Drawing.ControlGraphicsInfoArgs
        info = TryCast(edit.CreateViewInfo(), DevExpress.XtraEditors.ViewInfo.CheckEditViewInfo)
        painter = TryCast(edit.CreatePainter(), DevExpress.XtraEditors.Drawing.CheckEditPainter)
        info.EditValue = Checked
        info.Bounds = r
        info.CalcViewInfo(g)
        args = New DevExpress.XtraEditors.Drawing.ControlGraphicsInfoArgs(info, New DevExpress.Utils.Drawing.GraphicsCache(g), r)
        painter.Draw(args)
        args.Cache.Dispose()
    End Sub

    Private Sub view_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs)
        If e.Clicks = 1 AndAlso e.Button = MouseButtons.Left Then
            Dim info As GridHitInfo
            Dim pt As Point = view_Renamed.GridControl.PointToClient(Control.MousePosition)
            info = view_Renamed.CalcHitInfo(pt)
            If info.InRow AndAlso info.Column IsNot column AndAlso view_Renamed.IsDataRow(info.RowHandle) Then
                ClearSelection()
                SelectRow(info.RowHandle, True)
            End If

            If info.InColumn AndAlso info.Column Is column Then
                If SelectedCount = view_Renamed.DataRowCount Then
                    ClearSelection()
                Else
                    SelectAll()
                End If
            End If

            If info.InRow AndAlso view_Renamed.IsGroupRow(info.RowHandle) AndAlso info.HitTest <> GridHitTest.RowGroupButton Then
                Dim selected As Boolean = IsGroupRowSelected(info.RowHandle)
                SelectGroup(info.RowHandle, (Not selected))
            End If
            If info.InRow And info.Column Is CheckMarkColumn Then
                If CType(view_Renamed.GetRowCellValue(info.RowHandle, CheckMarkColumn), Boolean) = True Then
                    view_Renamed.SetRowCellValue(info.RowHandle, CheckMarkColumn, False)
                Else
                    view_Renamed.SetRowCellValue(info.RowHandle, CheckMarkColumn, True)
                End If
            End If
        End If
    End Sub

    Private Sub View_CustomDrawColumnHeader(ByVal sender As Object, ByVal e As ColumnHeaderCustomDrawEventArgs)
        If e.Column Is column Then
            e.Info.InnerElements.Clear()
            e.Painter.DrawObject(e.Info)
            DrawCheckBox(e.Graphics, e.Bounds, SelectedCount = view_Renamed.DataRowCount)
            e.Handled = True
        End If
    End Sub

    Private Sub View_CustomDrawGroupRow(ByVal sender As Object, ByVal e As RowObjectCustomDrawEventArgs)
        Dim info As DevExpress.XtraGrid.Views.Grid.ViewInfo.GridGroupRowInfo
        info = TryCast(e.Info, DevExpress.XtraGrid.Views.Grid.ViewInfo.GridGroupRowInfo)

        info.GroupText = "         " & info.GroupText.TrimStart()
        e.Info.Paint.FillRectangle(e.Graphics, e.Appearance.GetBackBrush(e.Cache), e.Bounds)
        e.Painter.DrawObject(e.Info)

        Dim r As Rectangle = info.ButtonBounds
        r.Offset(r.Width * 2, 0)
        DrawCheckBox(e.Graphics, r, IsGroupRowSelected(e.RowHandle))
        e.Handled = True
    End Sub

    Private Sub view_RowStyle(ByVal sender As Object, ByVal e As RowStyleEventArgs)
        If IsRowSelected(e.RowHandle) Then
            e.Appearance.BackColor = SystemColors.Highlight
            e.Appearance.ForeColor = SystemColors.HighlightText
        End If
    End Sub

    Public Property View() As GridView
        Get
            Return view_Renamed
        End Get
        Set(ByVal value As GridView)
            If view_Renamed IsNot value Then
                Detach()
                Attach(value)
            End If
        End Set
    End Property

    Public ReadOnly Property CheckMarkColumn() As GridColumn
        Get
            Return column
        End Get
    End Property

    Public ReadOnly Property SelectedCount() As Integer
        Get
            Return selection.Count
        End Get
    End Property

    Public Function GetSelectedRow(ByVal index As Integer) As Object
        Return selection(index)
    End Function

    Public Function GetSelectedIndex(ByVal row As Object) As Integer
        Return selection.IndexOf(row)
    End Function

    Public Sub ClearSelection()
        selection.Clear()
        Invalidate()
    End Sub

    Private Sub Invalidate()
        view_Renamed.CloseEditor()
        view_Renamed.BeginUpdate()
        view_Renamed.EndUpdate()
    End Sub

    Public Sub SelectAll()
        selection.Clear()
        Dim dataSource As ICollection = TryCast(view_Renamed.DataSource, ICollection)
        If dataSource IsNot Nothing AndAlso dataSource.Count = view_Renamed.DataRowCount Then
            selection.AddRange(dataSource) ' fast
        Else
            For i As Integer = 0 To view_Renamed.DataRowCount - 1 ' slow
                selection.Add(view_Renamed.GetRow(i))
            Next i
        End If
        Invalidate()
    End Sub

    Public Sub SelectGroup(ByVal rowHandle As Integer, ByVal [select] As Boolean)
        If IsGroupRowSelected(rowHandle) AndAlso [select] Then
            Return
        End If
        For i As Integer = 0 To view_Renamed.GetChildRowCount(rowHandle) - 1
            Dim childRowHandle As Integer = view_Renamed.GetChildRowHandle(rowHandle, i)
            If view_Renamed.IsGroupRow(childRowHandle) Then
                SelectGroup(childRowHandle, [select])
            Else
                SelectRow(childRowHandle, [select], False)
            End If
        Next i
        Invalidate()
    End Sub

    Public Sub SelectRow(ByVal rowHandle As Integer, ByVal [select] As Boolean)
        SelectRow(rowHandle, [select], True)
    End Sub

    Private Sub SelectRow(ByVal rowHandle As Integer, ByVal [select] As Boolean, ByVal invalidate As Boolean)
        If IsRowSelected(rowHandle) = [select] Then
            Return
        End If
        Dim row As Object = view_Renamed.GetRow(rowHandle)
        If [select] Then
            selection.Add(row)
        Else
            selection.Remove(row)
        End If
        If invalidate Then
            Me.Invalidate()
        End If
    End Sub

    Public Function IsGroupRowSelected(ByVal rowHandle As Integer) As Boolean
        For i As Integer = 0 To view_Renamed.GetChildRowCount(rowHandle) - 1
            Dim row As Integer = view_Renamed.GetChildRowHandle(rowHandle, i)
            If view_Renamed.IsGroupRow(row) Then
                If (Not IsGroupRowSelected(row)) Then
                    Return False
                End If
            Else
                If (Not IsRowSelected(row)) Then
                    Return False
                End If
            End If
        Next i
        Return True
    End Function

    Public Function IsRowSelected(ByVal rowHandle As Integer) As Boolean
        If view_Renamed.IsGroupRow(rowHandle) Then
            Return IsGroupRowSelected(rowHandle)
        End If

        Dim row As Object = view_Renamed.GetRow(rowHandle)
        Return GetSelectedIndex(row) <> -1
    End Function

    Private Sub view_CustomUnboundColumnData(ByVal sender As Object, ByVal e As CustomColumnDataEventArgs)
        If e.Column Is CheckMarkColumn Then
            If e.IsGetData Then
                e.Value = IsRowSelected(View.GetRowHandle(e.ListSourceRowIndex))
            Else
                SelectRow(View.GetRowHandle(e.ListSourceRowIndex), CBool(e.Value))
            End If
        End If
    End Sub

    Private Sub edit_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs)
        view_Renamed.PostEditor()
    End Sub


    Private Sub View_SelectionChanged(ByVal sender As Object, ByVal e As DevExpress.Data.SelectionChangedEventArgs)
        If (e.Action = System.ComponentModel.CollectionChangeAction.Remove) Then
            View.SetRowCellValue(e.ControllerRow, column, False)
        ElseIf (e.Action = System.ComponentModel.CollectionChangeAction.Add) Then
            View.SetRowCellValue(e.ControllerRow, column, True)
        End If
    End Sub


End Class

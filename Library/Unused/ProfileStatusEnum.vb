﻿

Public Enum ProfileStatusEnum
    None = 0
    NewProfile = 1
    Updating = 2
    Approved = 4
    Rejected = 8
    Deleted = 16
    DeletedByUser = 18
    AutoApprovedProfile = -1
End Enum

﻿Public Class DateControlsHandler


    Public Shared Function GetLocalDate(datePart As DateTime) As DateTime
        Dim fullDate As DateTime
        fullDate = New Date(datePart.Ticks, DateTimeKind.Utc)
        fullDate = fullDate.ToLocalTime()
        Return fullDate
    End Function


    Public Shared Function GetUTCDate(datePart As DateTime?) As DateTime?
        If (datePart Is Nothing) Then
            Return datePart
        End If
        Return GetUTCDate(datePart, datePart)
    End Function


    Public Shared Function GetUTCDate(datePart As DateTime?, timePart As DateTime?) As DateTime?
        If (datePart Is Nothing) Then
            Return datePart
        End If

        Dim fullDate As DateTime
        fullDate = datePart.Value.Date
        fullDate = New Date(fullDate.Ticks, DateTimeKind.Local)
        If (timePart IsNot Nothing) Then
            fullDate = fullDate.AddHours(timePart.Value.Hour)
            fullDate = fullDate.AddMinutes(timePart.Value.Minute)
            fullDate = fullDate.AddSeconds(timePart.Value.Second)
        End If
        fullDate = New Date(fullDate.ToUniversalTime().Ticks, DateTimeKind.Utc)

        Return fullDate
    End Function




    Public Shared Sub SetToday(ByRef DateFrom As DevExpress.XtraBars.BarEditItem,
                             ByRef DateTo As DevExpress.XtraBars.BarEditItem)

        DateFrom.EditValue = Date.UtcNow.Date
        DateTo.EditValue = Date.UtcNow.Date.AddDays(1).AddMilliseconds(-1)
    End Sub


    Public Shared Sub SetToday(ByRef DateFrom As DevExpress.XtraBars.BarEditItem,
                                   ByRef DateTo As DevExpress.XtraBars.BarEditItem,
                                   ByRef TimeFrom As DevExpress.XtraBars.BarEditItem,
                                   ByRef TimeTo As DevExpress.XtraBars.BarEditItem)

        DateFrom.EditValue = Date.UtcNow.Date
        DateTo.EditValue = Date.UtcNow.Date.AddDays(1).AddMilliseconds(-1)
        TimeFrom.EditValue = DateFrom.EditValue
        TimeTo.EditValue = DateTo.EditValue

    End Sub




    Public Shared Sub SetTwoDaysAgo(ByRef DateFrom As DevExpress.XtraBars.BarEditItem,
                           ByRef DateTo As DevExpress.XtraBars.BarEditItem)

        DateFrom.EditValue = Date.UtcNow.Date.AddDays(-2)
        DateTo.EditValue = Date.UtcNow.Date.AddDays(-1).AddMilliseconds(-1)
    End Sub


    Public Shared Sub SetTwoDaysAgo(ByRef DateFrom As DevExpress.XtraBars.BarEditItem,
                                     ByRef DateTo As DevExpress.XtraBars.BarEditItem,
                                     ByRef TimeFrom As DevExpress.XtraBars.BarEditItem,
                                     ByRef TimeTo As DevExpress.XtraBars.BarEditItem)

        DateFrom.EditValue = Date.UtcNow.Date.AddDays(-2)
        DateTo.EditValue = Date.UtcNow.Date.AddDays(-1).AddMilliseconds(-1)
        TimeFrom.EditValue = DateFrom.EditValue
        TimeTo.EditValue = DateTo.EditValue

    End Sub



    Public Shared Sub Set3DaysAgo(ByRef DateFrom As DevExpress.XtraBars.BarEditItem,
                           ByRef DateTo As DevExpress.XtraBars.BarEditItem)

        DateFrom.EditValue = Date.UtcNow.Date.AddDays(-3)
        DateTo.EditValue = Date.UtcNow.Date.AddDays(-2).AddMilliseconds(-1)
    End Sub



    Public Shared Sub SetYesterday(ByRef DateFrom As DevExpress.XtraBars.BarEditItem,
                           ByRef DateTo As DevExpress.XtraBars.BarEditItem)

        DateFrom.EditValue = Date.UtcNow.Date.AddDays(-1)
        DateTo.EditValue = Date.UtcNow.Date.AddMilliseconds(-1)
    End Sub

    Public Shared Sub SetYesterday(ByRef DateFrom As DevExpress.XtraBars.BarEditItem,
                                     ByRef DateTo As DevExpress.XtraBars.BarEditItem,
                                     ByRef TimeFrom As DevExpress.XtraBars.BarEditItem,
                                     ByRef TimeTo As DevExpress.XtraBars.BarEditItem)

        DateFrom.EditValue = Date.UtcNow.Date.AddDays(-1)
        DateTo.EditValue = Date.UtcNow.Date.AddMilliseconds(-1)
        TimeFrom.EditValue = DateFrom.EditValue
        TimeTo.EditValue = DateTo.EditValue

    End Sub



    Public Shared Sub SetLastSevenDays(ByRef DateFrom As DevExpress.XtraBars.BarEditItem,
                           ByRef DateTo As DevExpress.XtraBars.BarEditItem)

        DateFrom.EditValue = Date.UtcNow.Date.AddDays(-7)
        DateTo.EditValue = Date.UtcNow.Date.AddDays(1).AddMilliseconds(-1)
    End Sub

    Public Shared Sub SetLastSevenDays(ByRef DateFrom As DevExpress.XtraBars.BarEditItem,
                                      ByRef DateTo As DevExpress.XtraBars.BarEditItem,
                                      ByRef TimeFrom As DevExpress.XtraBars.BarEditItem,
                                      ByRef TimeTo As DevExpress.XtraBars.BarEditItem)

        DateFrom.EditValue = Date.UtcNow.Date.AddDays(-7)
        DateTo.EditValue = Date.UtcNow.Date.AddDays(1).AddMilliseconds(-1)
        TimeFrom.EditValue = DateFrom.EditValue
        TimeTo.EditValue = DateTo.EditValue

    End Sub


    Public Shared Sub SetLast15Days(ByRef DateFrom As DevExpress.XtraBars.BarEditItem,
                           ByRef DateTo As DevExpress.XtraBars.BarEditItem)

        DateFrom.EditValue = Date.UtcNow.Date.AddDays(-15)
        DateTo.EditValue = Date.UtcNow.Date.AddDays(1).AddMilliseconds(-1)
    End Sub



    Public Shared Function GetDefaultToDate() As Date
        Return Date.Now.Date.ToUniversalTime().AddDays(1).AddMilliseconds(-1)
    End Function


    Public Shared Function GetDefaultDateFrom() As Date
        Return Date.Now.Date.ToUniversalTime()
    End Function

End Class


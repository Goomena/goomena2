﻿Imports System.Data

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports Dating.Server.Core.DLL



'''<summary>
'''This is a test class for clsGeoHelperTest and is intended
'''to contain all clsGeoHelperTest Unit Tests
'''</summary>
<TestClass()> _
Public Class clsGeoHelperTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    'You can use the following additional attributes as you write your tests:
    '
    'Use ClassInitialize to run code before running the first test in the class
    <ClassInitialize()> _
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        DataHelpers.SetConnectionString(My.Settings.AppDBconnectionString)
    End Sub
    '
    'Use ClassCleanup to run code after all tests in a class have run
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize to run code before running each test
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup to run code after each test has run
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''A test for GetCountryMinPostcodeDataTable
    '''</summary>
    <TestMethod()> _
    Public Sub GetCountryMinPostcodeDataTableTest()
        Dim countryCode As String = "GL" ' TODO: Initialize to an appropriate value
        Dim region As String = String.Empty ' TODO: Initialize to an appropriate value
        Dim city As String = String.Empty ' TODO: Initialize to an appropriate value
        Dim lagid As String = String.Empty ' TODO: Initialize to an appropriate value
        Dim actual As DataTable
        actual = clsGeoHelper.GetCountryMinPostcodeDataTable(countryCode, region, city, lagid)
        Assert.AreNotEqual(0, actual.Rows.Count)
    End Sub


    '''<summary>
    '''A test for GetGEOByZip
    '''</summary>
    <TestMethod()> _
    Public Sub GetGEOByZipTest()
        Dim countryCode As String = "GR" ' TODO: Initialize to an appropriate value
        Dim zipstr As String = "11146" ' TODO: Initialize to an appropriate value
        Dim lagid As String = String.Empty ' TODO: Initialize to an appropriate value
        Dim expected As DataTable = Nothing ' TODO: Initialize to an appropriate value
        Dim actual As DataTable
        actual = clsGeoHelper.GetGEOByZip(countryCode, zipstr, lagid)
        Assert.AreNotEqual(0, actual.Rows.Count)
    End Sub
End Class

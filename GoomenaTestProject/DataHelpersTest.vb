﻿Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports Dating.Server.Core.DLL



'''<summary>
'''This is a test class for DataHelpersTest and is intended
'''to contain all DataHelpersTest Unit Tests
'''</summary>
<TestClass()> _
Public Class DataHelpersTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    'You can use the following additional attributes as you write your tests:
    '
    'Use ClassInitialize to run code before running the first test in the class
    <ClassInitialize()> _
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        DataHelpers.SetConnectionString(My.Settings.AppDBconnectionString)
    End Sub
    '
    'Use ClassCleanup to run code after all tests in a class have run
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize to run code before running each test
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup to run code after each test has run
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''A test for UpdateEUS_Profiles_Activity
    '''</summary>
    <TestMethod()> _
    Public Sub UpdateEUS_Profiles_ActivityTest()
        Dim profileid As Integer = 269 ' TODO: Initialize to an appropriate value
        DataHelpers.UpdateEUS_Profiles_Activity(profileid)
        Assert.Inconclusive("A method that does not return a value cannot be verified.")
    End Sub

    '''<summary>
    '''A test for UpdateEUS_Profiles_FacebookData
    '''</summary>
    <TestMethod()> _
    Public Sub UpdateEUS_Profiles_FacebookDataTest()
        Dim profileid As Integer = 269 ' TODO: Initialize to an appropriate value
        Dim fbUid As String = "1234567890" ' TODO: Initialize to an appropriate value
        Dim fbName As String = "slava" ' TODO: Initialize to an appropriate value
        Dim fbUserName As String = "slava1" ' TODO: Initialize to an appropriate value
        DataHelpers.UpdateEUS_Profiles_FacebookData(profileid, fbUid, fbName, fbUserName)
        Assert.Inconclusive("A method that does not return a value cannot be verified.")
    End Sub

   

    '''<summary>
    '''A test for UpdateEUS_Profiles_LoginData
    '''</summary>
    <TestMethod()> _
    Public Sub UpdateEUS_Profiles_LoginDataTest1()
        Dim profileid As Integer = 269 ' TODO: Initialize to an appropriate value
        Dim REMOTE_ADDR As String = String.Empty ' TODO: Initialize to an appropriate value
        Dim GEO_COUNTRY_CODE As String = String.Empty ' TODO: Initialize to an appropriate value
        Dim IsOnline As Boolean = False ' TODO: Initialize to an appropriate value
        DataHelpers.UpdateEUS_Profiles_LoginData(profileid, REMOTE_ADDR, GEO_COUNTRY_CODE, IsOnline)
        Assert.Inconclusive("A method that does not return a value cannot be verified.")
    End Sub
End Class

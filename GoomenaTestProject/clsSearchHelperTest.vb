﻿Imports System.Data

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports Dating.Server.Core.DLL



'''<summary>
'''This is a test class for clsSearchHelperTest and is intended
'''to contain all clsSearchHelperTest Unit Tests
'''</summary>
<TestClass()> _
Public Class clsSearchHelperTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    'You can use the following additional attributes as you write your tests:
    '
    'Use ClassInitialize to run code before running the first test in the class
    <ClassInitialize()> _
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        DataHelpers.SetConnectionString(My.Settings.AppDBconnectionString)
    End Sub
    '
    'Use ClassCleanup to run code after all tests in a class have run
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize to run code before running each test
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup to run code after each test has run
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''A test for GetMembersDistance
    '''</summary>
    <TestMethod()> _
    Public Sub GetMembersDistanceTest()

        Dim CurrentProfileID As Integer = 269 ' TODO: Initialize to an appropriate value
        Dim OtherProfileID As Integer = 8183 ' TODO: Initialize to an appropriate value
        Dim expected As String = "2.82" '7.0260283505761603 ' TODO: Initialize to an appropriate value
        Dim actual As Double
        actual = clsSearchHelper.GetMembersDistance(CurrentProfileID, OtherProfileID)
        Assert.AreEqual(expected, actual)
    End Sub

    '''<summary>
    '''A test for GetMembersToSearchDataTable
    '''</summary>
    <TestMethod()> _
    Public Sub GetMembersToSearchDataTableTest()
        Dim prms As New clsSearchHelperParameters() ' TODO: Initialize to an appropriate value


        prms.CurrentProfileId = 269
        prms.ReturnRecordsWithStatus = ProfileStatusEnum.Approved
        prms.SearchSort = SearchSortEnum.NewestMember
        prms.zipstr = "111 47"
        prms.Distance = 650
        prms.LookingFor_ToMeetMaleID = False
        prms.LookingFor_ToMeetFemaleID = True
        prms.NumberOfRecordsToReturn = 0

        'prms.AdditionalWhereClause = Sql

        Dim expected As New DataTable() ' TODO: Initialize to an appropriate value
        Dim actual As DataSet
        actual = clsSearchHelper.GetMembersToSearchDataTable(prms)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Verify the correctness of this test method.")
    End Sub

    '''<summary>
    '''A test for GetMembersDistance
    '''</summary>
    <TestMethod()> _
    Public Sub GetMembersDistanceTest1()
        Dim CurrentProfileID As Integer = 0 ' TODO: Initialize to an appropriate value
        Dim OtherProfileID As Integer = 0 ' TODO: Initialize to an appropriate value
        Dim expected As Single = 0.0! ' TODO: Initialize to an appropriate value
        Dim actual As Single
        actual = clsSearchHelper.GetMembersDistance(CurrentProfileID, OtherProfileID)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Verify the correctness of this test method.")
    End Sub
End Class

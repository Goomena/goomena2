﻿Imports Microsoft.VisualStudio.TestTools.UnitTesting.Web

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports Dating.Server.Site.Web



'''<summary>
'''This is a test class for GeneralFunctionsTest and is intended
'''to contain all GeneralFunctionsTest Unit Tests
'''</summary>
<TestClass()> _
Public Class GeneralFunctionsTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    'You can use the following additional attributes as you write your tests:
    '
    'Use ClassInitialize to run code before running the first test in the class
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup to run code after all tests in a class have run
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize to run code before running each test
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup to run code after each test has run
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    'TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
    ' http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
    ' whether you are testing a page, web service, or a WCF service.
    '''<summary>
    '''A test for IsSupportedLAG
    '''</summary>
    <TestMethod(), _
     HostType("ASP.NET"), _
     AspNetDevelopmentServerHost("C:\Development\CMSProject\Goomena\Dating.Server.Site.Web", "/"), _
     UrlToTest("http://localhost:44444/")> _
    Public Sub IsSupportedLAGTest()
        Dim lag As String = "US" ' TODO: Initialize to an appropriate value
        Dim expected As Boolean = True ' TODO: Initialize to an appropriate value
        Dim actual As Boolean
        actual = GeneralFunctions.IsSupportedLAG(lag)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Verify the correctness of this test method.")
    End Sub

    'TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
    ' http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
    ' whether you are testing a page, web service, or a WCF service.
    '''<summary>
    '''A test for CheckDomainAndRedirect
    '''</summary>
    <TestMethod(), _
     HostType("ASP.NET"), _
     AspNetDevelopmentServerHost("C:\Development\CMSProject\Goomena\Dating.Server.Site.Web", "/"), _
     UrlToTest("http://localhost:44444/")> _
    Public Sub CheckDomainAndRedirectTest()
        Dim host As String = "goomena.com" ' TODO: Initialize to an appropriate value
        GeneralFunctions.CheckDomainAndRedirect(host)
        Assert.Inconclusive("A method that does not return a value cannot be verified.")
    End Sub
End Class

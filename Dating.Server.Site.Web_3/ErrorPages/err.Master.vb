﻿Imports System.Data.SqlClient
Imports Dating.Server.Core.DLL
Imports Dating.Server.Datasets.DLL.DSMembers
Imports DevExpress.Web.ASPxEditors



Public Class _errMaster
    Inherits BaseMasterPageMembers


    Public Property HideBottom_MemberLeftPanel As Boolean
    Public Property ifrUserMap_src As String
    Public Property RedirectURL As String


    'Dim _pageData As clsPageData
    'Protected ReadOnly Property CurrentPageData As clsPageData
    '    Get
    '        If (_pageData Is Nothing) Then _pageData = New clsPageData("master", Context)
    '        Return _pageData
    '    End Get
    'End Property





    Private Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        Try
            ''If Not clsCurrentContext.VerifyLogin() OrElse Request.Form("__EVENTTARGET") = LoginStatus1.UniqueID & "$ctl00" Then
            ''    '"ctl00%24SettingsPopup%24LoginStatus1%24ctl00" Then
            ''    clsCurrentContext.ClearSession(Session)
            ''    Response.Redirect(ResolveUrl("~/Login.aspx"), False)
            ''    HttpContext.Current.ApplicationInstance.CompleteRequest()
            ''    Session("LoginRedirectToDefault") = 1
            ''End If


            If Not Me.IsPostBack Then

                If clsCurrentContext.VerifyLogin() = True Then
                    lnkGoomenaLogo.NavigateUrl = "/Members/default.aspx"
                    lnkHome.NavigateUrl = "/Members/default.aspx"
                    RedirectURL = "/Members/default.aspx"
                Else
                    lnkGoomenaLogo.NavigateUrl = "/Default.aspx"
                    lnkHome.NavigateUrl = "/Default.aspx"
                    RedirectURL = "/Default.aspx"
                End If

                If (Session("NoRedirectToMembersDefault") Is Nothing) Then Session("NoRedirectToMembersDefault") = True

                Dim TimeOffset As Double
                If (hdfTimeOffset.Value.Trim() <> "" AndAlso Double.TryParse(hdfTimeOffset.Value, TimeOffset)) Then
                    Session("TimeOffset") = TimeOffset
                End If

                If (clsLanguageHelper.IsSupportedLAG(Request.QueryString("lag"), Session("GEO_COUNTRY_CODE"))) Then
                    Session("LagID") = Request.QueryString("lag").ToUpper()

                    ' update lagid info
                    Try
                        If (Me.Session("ProfileID") > 0) Then
                            DataHelpers.UpdateEUS_Profiles_LAGID(Me.Session("ProfileID"), Me.Session("LagID"))
                        End If
                    Catch ex As Exception
                        WebErrorMessageBox(Me, ex, "")
                    End Try
                End If


                If (Not clsLanguageHelper.IsSupportedLAG(Session("LagID"), Session("GEO_COUNTRY_CODE"))) Then
                    clsLanguageHelper.SetLAGID()
                End If

                If (clsLanguageHelper.IsSupportedLAG(Session("LagID"), Session("GEO_COUNTRY_CODE"))) Then
                    cmbLag.Items(clsLanguageHelper.GetIndexFromLagID(Session("LagID"))).Selected = True
                End If
                SetLanguageLinks()
            End If


            If (cmbLag.SelectedItem Is Nothing) Then
                cmbLag.Items(clsLanguageHelper.GetIndexFromLagID(Session("LagID"))).Selected = True
            End If


            Try
                ' otan to session exei diaforetiki glwsa apo tin epilegmeni glwssa
                If (clsLanguageHelper.GetIndexFromLagID(Session("LagID")) = 0 AndAlso Session("LagID") <> "US") Then
                    Session("LagID") = gDomainCOM_DefaultLAG
                End If
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Dim a As New _RootMaster
        'If (a.IsFemale) AndAlso (Session("LagID") = "GR") Then
        '    Me.womenbanner.Attributes.Add("src", "https://cdn.goomena.com/goomena-kostas/gifts/banner-home-page.jpg")
        '    Me.womenbannerhref.Attributes.Add("href", "http://www.goomena.com/cmspage.aspx?pageid=276&title=Gifts.aspx")
        'End If

        Try
            '    BindMembersList() 'set popup controls enabled/disabled first
            '    LoadLAG()
            '    ShowUserMap()

            '    If (Not Page.IsPostBack) Then
            '        content_bottom.Visible = False

            '        If (Me.Request.RawUrl.ToUpper().StartsWith("/MEMBERS/")) Then
            '            content_bottom.Visible = True
            '            'SetReferrerButtonVisible()
            '        End If

            '    End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        Try
            LoadLAG()
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Protected Sub cmbLag_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbLag.SelectedIndexChanged
        Try

            clsLanguageHelper.SetLagCookie(cmbLag.SelectedItem.Value)

            ' update lagid info
            Try

                DataHelpers.UpdateEUS_Profiles_LAGID(Me.Session("ProfileID"), Me.Session("LagID"))
                LanguageHelper.RedirectToNewLAG(cmbLag.SelectedIndex)
            Catch ex As System.Threading.ThreadAbortException
                Return
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try



            Try

                Me._pageData = Nothing
                LoadLAG()

            Catch ex As System.Threading.ThreadAbortException
                Return
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try


            Try
                ModGlobals.UpdateUserControls(Me.Page, True)
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try


        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Protected Sub LoadLAG()
        Try
            clsMenuHelper.PopulateMenuData(mnuFooter1, "mnuFooter1")
            clsMenuHelper.PopulateMenuData(mnuFooter2, "mnuFooter2")
            clsMenuHelper.PopulateMenuData(mnuFooter3, "mnuFooter3")
            clsMenuHelper.PopulateMenuData(mnuFooter4, "mnuFooter4")
            clsMenuHelper.PopulateMenuData(mnuFooter5, "mnuFooter5")

            clsMenuHelper.CheckPublicMenuLinks(mnuFooter1, GetLag())
            clsMenuHelper.CheckPublicMenuLinks(mnuFooter2, GetLag())
            clsMenuHelper.CheckPublicMenuLinks(mnuFooter3, GetLag())
            clsMenuHelper.CheckPublicMenuLinks(mnuFooter4, GetLag())
            clsMenuHelper.CheckPublicMenuLinks(mnuFooter5, GetLag())

            lblFooterMenuTitle1.Text = CurrentPageData.GetCustomString("lblFooterMenuTitle1")
            lblFooterMenuTitle2.Text = CurrentPageData.GetCustomString("lblFooterMenuTitle2")
            lblFooterMenuTitle3.Text = CurrentPageData.GetCustomString("lblFooterMenuTitle3")
            lblFooterMenuTitle4.Text = CurrentPageData.GetCustomString("lblFooterMenuTitle4")
            lblFooterMenuTitle5.Text = CurrentPageData.GetCustomString("lblFooterMenuTitle5")

            'clsMenuHelper.PopulateMenuData(mnuFooter, "bottom")
            ''AddModifyMenuNavigateUrl_AddMembersMaster(mnuFooter.Items)

            'clsMenuHelper.PopulateMenuData(mnuFooter1, "mnuFooter1")
            'clsMenuHelper.PopulateMenuData(mnuFooter2, "mnuFooter2")
            'clsMenuHelper.PopulateMenuData(mnuFooter3, "mnuFooter3")
            'clsMenuHelper.PopulateMenuData(mnuFooter4, "mnuFooter4")
            'clsMenuHelper.PopulateMenuData(mnuFooter5, "mnuFooter5")


            'lblFooterMenuTitle1.Text = CurrentPageData.GetCustomString("lblFooterMenuTitle1")
            'lblFooterMenuTitle2.Text = CurrentPageData.GetCustomString("lblFooterMenuTitle2")
            'lblFooterMenuTitle3.Text = CurrentPageData.GetCustomString("lblFooterMenuTitle3")
            'lblFooterMenuTitle4.Text = CurrentPageData.GetCustomString("lblFooterMenuTitle4")
            'lblFooterMenuTitle5.Text = CurrentPageData.GetCustomString("lblFooterMenuTitle5")

            'tdFooterMenu1.Visible = (mnuFooter1.Items.Count > 0)
            'tdFooterMenu2.Visible = (mnuFooter2.Items.Count > 0)
            'tdFooterMenu3.Visible = (mnuFooter3.Items.Count > 0)
            'tdFooterMenu4.Visible = (mnuFooter4.Items.Count > 0)
            'tdFooterMenu5.Visible = (mnuFooter5.Items.Count > 0)

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Try
            lblFooterSiteTitle.Text = CurrentPageData.GetCustomString("lblFooterSiteTitle")
            lblFooterSiteDescription.Text = CurrentPageData.GetCustomString("lblFooterSiteDescription")
            lblFooterCopyRight.Text = CurrentPageData.GetCustomString("lblFooterCopyRight")
            lnkSiteMap.Text = CurrentPageData.GetCustomString("lnkSiteMap")
            lnkHome.Text = CurrentPageData.GetCustomString("lnkHome_ND")


            lblEscortsWarning.Text = CurrentPageData.GetCustomString("lblEscortsWarning")


            Dim lblEscortsWarningTooltip As Label = lblEscortsWarningPopup.FindControl("lblEscortsWarningTooltip")
            If (lblEscortsWarningTooltip IsNot Nothing) Then
                lblEscortsWarningTooltip.Text = CurrentPageData.GetCustomString("lblEscortsWarningTooltip")
                lblEscortsWarningPopup.Enabled = True
            Else
                lblEscortsWarningPopup.Enabled = False
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Try
            LoadLAG_LoginControl()
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Protected Sub LoadLAG_LoginControl()
        Try
            Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            Dim MainLogin As System.Web.UI.WebControls.Login = ucLogin1.LoginControl
            'MainLogin.UserNameLabelText = CurrentPageData.GetCustomString("txtUserNameLabelText")
            'MainLogin.PasswordLabelText = CurrentPageData.GetCustomString("txtPasswordLabelText")
            'MainLogin.RememberMeText = CurrentPageData.GetCustomString("txtRememberMeText")
            'MainLogin.LoginButtonText = CurrentPageData.GetCustomString("txtLoginButtonText")
            'MainLogin.UserNameRequiredErrorMessage = CurrentPageData.GetCustomString("txtUserNameRequiredErrorMessage")
            MainLogin.TitleText = CurrentPageData.GetCustomString("txtLoginTitleText")

            Dim lbForgotPassword As ASPxHyperLink = MainLogin.FindControl("lbForgotPassword")
            If (lbForgotPassword IsNot Nothing) Then
                lbForgotPassword.Text = CurrentPageData.GetCustomString("lbForgotPassword")
            End If

            Dim lbLoginFB As ASPxHyperLink = MainLogin.FindControl("lbLoginFB")
            If (lbLoginFB IsNot Nothing) Then
                lbLoginFB.Text = CurrentPageData.GetCustomString("lbLoginFB")
            End If

            Dim btnJoinToday As ASPxButton = MainLogin.FindControl("btnJoinToday")
            If (btnJoinToday IsNot Nothing) Then
                btnJoinToday.Text = CurrentPageData.GetCustomString("btnJoinToday")
                btnJoinToday.PostBackUrl = "~/Register.aspx"
                If (Not String.IsNullOrEmpty(Request.QueryString("ReturnUrl"))) Then
                    btnJoinToday.PostBackUrl = btnJoinToday.PostBackUrl & "?ReturnUrl=" & HttpUtility.UrlEncode(Request.QueryString("ReturnUrl"))
                End If
            End If

            Dim UserName As ASPxTextBox = MainLogin.FindControl("UserName")
            If (UserName IsNot Nothing) Then
                'UserName.NullText = CurrentPageData.GetCustomString("UserName.NullText")
                UserName.ValidationSettings.RequiredField.ErrorText = CurrentPageData.GetCustomString("UserName.ValidationSettings.RequiredField.ErrorText")
            End If

            Dim Password As ASPxTextBox = MainLogin.FindControl("Password")
            If (Password IsNot Nothing) Then
                'Password.NullText = CurrentPageData.GetCustomString("Password.NullText")
                Password.ValidationSettings.RequiredField.ErrorText = CurrentPageData.GetCustomString("Password.ValidationSettings.RequiredField.ErrorText")
            End If

            Dim lblUsernameTop As Label = MainLogin.FindControl("lblUsernameTop")
            If (lblUsernameTop IsNot Nothing) Then
                lblUsernameTop.Text = CurrentPageData.GetCustomString("UserName.NullText")
            End If

            Dim lblPasswordTop As Label = MainLogin.FindControl("lblPasswordTop")
            If (lblPasswordTop IsNot Nothing) Then
                lblPasswordTop.Text = CurrentPageData.GetCustomString("Password.NullText")
            End If


            Dim btnLogin As ASPxButton = MainLogin.FindControl("btnLogin")
            If (btnLogin IsNot Nothing) Then
                btnLogin.Text = CurrentPageData.GetCustomString("btnLogin")
            End If

            Dim RememberMe As CheckBox = MainLogin.FindControl("RememberMe")
            If (RememberMe IsNot Nothing) Then
                RememberMe.Text = CurrentPageData.GetCustomString("RememberMe")
            End If


            Dim chkRememberMe As ASPxCheckBox = MainLogin.FindControl("chkRememberMe")
            If (chkRememberMe IsNot Nothing) Then
                chkRememberMe.Text = CurrentPageData.GetCustomString("RememberMe")
            End If


            '' controls found on login.aspx page
            Dim lblUserName As Literal = MainLogin.FindControl("lblUserName")
            If (lblUserName IsNot Nothing) Then
                lblUserName.Text = CurrentPageData.GetCustomString("lblUserName")
            End If


            Dim lblPassword As Literal = MainLogin.FindControl("lblPassword")
            If (lblPassword IsNot Nothing) Then
                lblPassword.Text = CurrentPageData.GetCustomString("lblPassword")
            End If

            Dim btnLoginNow As ASPxButton = MainLogin.FindControl("btnLoginNow")
            If (btnLoginNow IsNot Nothing) Then
                btnLoginNow.Text = CurrentPageData.GetCustomString("btnLoginNow")
                btnLoginNow.Text = btnLoginNow.Text.Replace("&nbsp;", " ")
            End If


        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Private Sub SetLanguageLinks()
        Try
            If (clsLanguageHelper.HasCountryConfig(Session("GEO_COUNTRY_CODE"))) Then
                Dim cc As clsLanguageHelper.CountryConfig = clsLanguageHelper.GetCountryConfig(Session("GEO_COUNTRY_CODE"))
                For itms = cmbLag.Items.Count - 1 To 0 Step -1
                    If (cc.ShowLangs.Contains(cmbLag.Items(itms).Value)) Then
                        Continue For
                    Else
                        cmbLag.Items.RemoveAt(itms)
                    End If
                Next
            End If

            'lnkEnglish
            'lnkGerman
            'lnkGreek
            'lnkHungarian

            Dim url As String = Request.Url.AbsolutePath
            url = url & "?"

            For Each itm As String In Request.QueryString.AllKeys
                If (Not String.IsNullOrEmpty(itm) AndAlso itm.ToUpper() <> "LAG") Then
                    url = url & itm & "=" & HttpUtility.UrlEncode(Request.QueryString(itm)) & "&"
                End If
            Next
            'url = url.TrimEnd("&"c)



            'lnkEnglish.NavigateUrl = url & "lag=US"
            'lnkGerman.NavigateUrl = url & "lag=DE"
            'lnkGreek.NavigateUrl = url & "lag=GR"
            'lnkHungarian.NavigateUrl = url & "lag=HU"
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try


        Try

            'lnkEnglish
            'lnkGerman
            'lnkGreek
            'lnkHungarian

            'lnkEnglish.Visible = AppUtils.IsSupportedLAG("US")
            'lnkGerman.Visible = AppUtils.IsSupportedLAG("DE")
            'lnkGreek.Visible = AppUtils.IsSupportedLAG("GR")
            'lnkHungarian.Visible = AppUtils.IsSupportedLAG("HU")

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Protected Sub LoginStatus1_LoggingOut(sender As Object, e As System.Web.UI.WebControls.LoginCancelEventArgs) 'Handles LoginStatus1.LoggingOut
        Try
            FormsAuthentication.SignOut()
            clsCurrentContext.ClearSession(Session)

            Response.Redirect(ResolveUrl("~/login.aspx"))

        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub
End Class
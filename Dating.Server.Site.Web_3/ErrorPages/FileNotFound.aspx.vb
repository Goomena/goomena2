﻿Public Class FileNotFound
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            Dim errorInfo As String = ""
            Dim sendMessage As Boolean = True
            If (Request("aspxerrorpath") IsNot Nothing) Then
                'lbHTMLBody.Text = lbHTMLBody.Text & "<br/>" & _ " Requested URL: <b>" & Request("aspxerrorpath") & "</b><br/>"
                errorInfo = Request("aspxerrorpath")
            End If
            If (Len(errorInfo) = 0) Then
                errorInfo = Request.Url.ToString()
                If (errorInfo.IndexOf("?") > -1) Then
                    errorInfo = errorInfo.Remove(0, errorInfo.IndexOf("?")).Replace("?404;", "")
                Else
                    errorInfo = ""
                End If
            End If

            If (Len(errorInfo) > 0) Then
                UrlUtils.CheckURLAndRedirectPermanent(errorInfo, True, isFromFileNotFoundPage:=True)
            End If

            Try
                If (errorInfo.IndexOf("MicrosoftAjax", StringComparison.OrdinalIgnoreCase) > -1) Then
                    'just skip
                    sendMessage = False
                ElseIf (errorInfo.IndexOf(".php", StringComparison.OrdinalIgnoreCase) > -1) Then
                    'just skip
                    sendMessage = False
                ElseIf (errorInfo.IndexOf("DXR.axd", StringComparison.OrdinalIgnoreCase) > -1) Then
                    'just skip
                    sendMessage = False
                ElseIf (errorInfo.IndexOf("ScriptResource.axd", StringComparison.OrdinalIgnoreCase) > -1) Then
                    'just skip
                    sendMessage = False
                ElseIf (errorInfo.IndexOf(").html(", StringComparison.OrdinalIgnoreCase) > -1) Then
                    'just skip
                    sendMessage = False
                ElseIf (errorInfo.IndexOf("browserconfig.xml", StringComparison.OrdinalIgnoreCase) > -1) Then
                    'just skip
                    sendMessage = False
                Else
                    Dim ref As String = Request.ServerVariables("HTTP_REFERER")
                    If (Not String.IsNullOrEmpty(ref)) Then
                        If (ref.IndexOf("DXR.axd", StringComparison.OrdinalIgnoreCase) > -1) Then
                            sendMessage = False
                        ElseIf (ref.IndexOf("ScriptResource.axd", StringComparison.OrdinalIgnoreCase) > -1) Then
                            sendMessage = False
                        ElseIf (ref.IndexOf(".php", StringComparison.OrdinalIgnoreCase) > -1) Then
                            sendMessage = False
                        ElseIf (ref.IndexOf(").html(", StringComparison.OrdinalIgnoreCase) > -1) Then
                            sendMessage = False
                        ElseIf (ref.IndexOf("browserconfig.xml", StringComparison.OrdinalIgnoreCase) > -1) Then
                            sendMessage = False
                        End If
                    End If
                End If
            Catch
            End Try

            If (sendMessage) Then
                Try
                    WebInfoSendEmail("FileNotFound page loaded." & vbCrLf & "Error path: " & errorInfo)
                Catch
                End Try
            End If

        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
        End Try
    End Sub


End Class
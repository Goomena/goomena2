﻿Imports Dating.Server.Core.DLL

Public Class _2coSetPayment
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            'lnkBack.NavigateUrl = "http://www.paymix.net/pay/notOk.aspx?transID=" & Request("TransID")
            Dim pgUrl As String = "https://www.2checkout.com/checkout/spurchase"
            Dim productID As String = Request("itemName")
            Dim amt As Decimal = Request("amount")
            Dim payTransID As String = Request("TransID")
            Dim currency As String = Request("currency")
            Dim type As String = Request("type")

            Dim oRemotePost As New RemotePost()
            oRemotePost.Url = pgUrl

            oRemotePost.Add("sid", "1832556")
            oRemotePost.Add("quantity", 1)
            Try
                oRemotePost.Add("pay_method", type)
            Catch ex As Exception
                oRemotePost.Add("pay_method", "CC")
            End Try

            oRemotePost.Add("product_id", productID)
            oRemotePost.Add("merchant_order_id", payTransID)

            Dim siteUrl As String = Request.Url.AbsoluteUri
            If (siteUrl.IndexOf(Request.Url.PathAndQuery) > -1) Then siteUrl = siteUrl.Remove(siteUrl.IndexOf(Request.Url.PathAndQuery))
            siteUrl = siteUrl & "/Members/paymentfail.aspx?transID=" & payTransID

            oRemotePost.Add("return_url", siteUrl)
            oRemotePost.Post()

        Catch ex As Threading.ThreadAbortException

        Catch ex As Exception

        End Try
    End Sub

End Class
﻿Imports System.IO
Imports System.Web.HttpUtility
Imports System.Net
Imports System.Data.SqlClient
Imports System.Security.Cryptography
Imports Dating.Server.Core.DLL

Public Class epochIPNHandler
    Inherits System.Web.UI.Page
    Shared cmd As New SqlCommand
    Dim sqlcon As New SqlConnection(ModGlobals.ConnectionString)
    Shared sqlStr As String = ""


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete

        Response.Clear()

        Dim reader As StreamReader = New StreamReader(Page.Request.InputStream)
        'Dim mail As New clsMyMail

        Dim ret As String = reader.ReadToEnd()
        'Try
        '    mail.SendMailUseGmail("info@zevera.com", "savvas@000.gr", "epoch ipn", ret)
        'Catch
        'End Try
        clsMyMail.TrySendMail(clsCurrentContext.ExceptionsEmail, "epoch ipn", ret)

        Dim siteIPN As UniPAYIPN = New UniPAYIPN()
        Dim buyer As New clsDataRecordBuyerInfo
        Dim ipnData As New clsDataRecordIPN
        Dim UniPayTransactionID As String = UrlDecode(Request("x_merchant_order_id"))
        Dim allok As Boolean = False
        ''Dim Merchant As String = UrlDecode(Request("vendor_id"))
        ''Dim Securitycode As String = UrlDecode(Request("md5_hash"))
        Dim CustomerEmaiAddress As String = UrlDecode(Request("email"))
        Dim ReferenceNumber As String = UrlDecode(Request("order_id"))
        Dim amount As String = UrlDecode(Request("localamount"))
        Dim status As String = UrlDecode(Request("ans"))
        Dim custFirstName As String = UrlDecode(Request("name"))
        Dim custLastName As String = UrlDecode(Request("name"))
        Dim custAddress As String = UrlDecode(Request("address"))
        Dim custCity As String = UrlDecode(Request("city"))
        Dim custState As String = UrlDecode(Request("state"))
        Dim custCountry As String = UrlDecode(Request("country"))
        Dim custZip As String = UrlDecode(Request("zip"))
        Dim customerIP As String = UrlDecode(Request("ipaddress"))
        Dim feeAmount As String = 0 'UrlDecode(Request("ap_feeamount"))
        Dim netAmount As String = UrlDecode(Request("amount")) 'UrlDecode(Request("ap_netamount"))
        Dim currency As String = UrlDecode(Request("currency"))
        Dim memberID As String = UrlDecode(Request("member_id"))
        Dim epochProduct As clsEpochProduct

        If Len(ReferenceNumber) = 0 Then
            ReferenceNumber = UrlDecode(Request("transaction_id"))
        End If
        If Len(amount) = 0 Then
            amount = UrlDecode(Request("auth_localamount"))
        End If
        ''Dim message_type As String = UrlDecode(Request("message_type"))
        ''Dim fraud_status As String = UrlDecode(Request("fraud_status"))
        Dim productCode As String = ""

        Dim paymentSubType As String = ""
        Try
            paymentSubType = UrlDecode(Request("payment_subtype"))
        Catch
        End Try
        If (String.IsNullOrEmpty(paymentSubType)) Then
            paymentSubType = UrlDecode(Request("payment_type"))
        End If


        Try
            sqlStr = "select count(*) from UNI_PayTransactions WHERE UniqueID = '" & UniPayTransactionID & "'"
            Dim count As Integer = DataHelpers.ExecuteScalar(sqlStr, ModGlobals.ConnectionString)

            If count = 0 Then UniPayTransactionID = ""
            ''  If Trim(Merchant.ToLower()) = "1832556" Then
            Try
                If UniPayTransactionID = "" Or UniPayTransactionID = Nothing Then

                    sqlStr = "select top(1) UniqueID from UNI_PayTransactions WHERE LoginName = '" & CustomerEmaiAddress & "' order by paymentdatetime desc"
                    UniPayTransactionID = DataHelpers.ExecuteScalar(sqlStr, ModGlobals.ConnectionString)

                    If UniPayTransactionID = "" Or UniPayTransactionID = Nothing Then

                        sqlStr = "select top(1) UniqueID from UNI_PayTransactions WHERE CustomerIP = '" & customerIP & "' order by paymentdatetime desc"
                        UniPayTransactionID = DataHelpers.ExecuteScalar(sqlStr, ModGlobals.ConnectionString)

                        If UniPayTransactionID = "" Or UniPayTransactionID = Nothing Then
                            'Mail.SendMailUseGmail("info@zevera.com", "savvas@000.gr", "IMPORTANT!: ERROR on EPOCH ipn", "No ORDER ID " & vbCrLf & "Email: " & CustomerEmaiAddress & vbCrLf & "TransactionID: " & ReferenceNumber)
                            clsMyMail.TrySendMail(clsCurrentContext.ExceptionsEmail, "IMPORTANT!: ERROR on EPOCH ipn",
                                               "No ORDER ID " & vbCrLf & _
                                               "Email: " & CustomerEmaiAddress & vbCrLf & _
                                               "TransactionID: " & ReferenceNumber)

                            Exit Sub
                        End If
                    End If


                End If
            Catch ex2 As Exception

            End Try

            If Trim(status.ToLower).StartsWith("y") Then
                ''  If Trim(Securitycode) = Trim(GenerateHash(UrlDecode(Request("sale_id")) & Merchant & ReferenceNumber & "@Ek0R1G!aL21")) Then
                'sqlStr = "select * from UNI_PayTransactions INNER JOIN SalesSitesProducts ON SalesSitesProducts.SalesSiteProductID =  UNI_PayTransactions.SalesSiteProductID inner join salessites on salessites.SalesSiteID = UNI_PayTransactions.SalesSiteID WHERE UniqueID = '" & UniPayTransactionID & "'"
                sqlStr = "select * from UNI_PayTransactions WHERE UniqueID = '" & UniPayTransactionID & "'"
                Dim rs As SqlDataReader
                sqlcon = New SqlConnection(ModGlobals.ConnectionString)
                cmd = New SqlCommand(sqlStr, sqlcon)
                cmd.Connection.Open()
                rs = cmd.ExecuteReader()

                Dim originalTransactionID As String = ""
                While rs.Read

                    If (String.IsNullOrEmpty(CustomerEmaiAddress)) Then
                        CustomerEmaiAddress = rs("LoginName")
                    End If

                    siteIPN = New UniPAYIPN()
                    ipnData.PayProviderTransactionID = ReferenceNumber
                    ipnData.PayProviderAmount = amount

                    buyer.PayerEmail = CustomerEmaiAddress
                    ipnData.CustomerID = rs("CustomerID")
                    ipnData.PaymentDateTime = rs("PaymentDateTime")
                    ipnData.PayTransactionID = UniPayTransactionID
                    ipnData.SalesSiteID = rs("SalesSiteID")
                    ipnData.SalesSiteProductID = rs("SalesSiteProductID")
                    ipnData.TransactionTypeID = rs("SaleTypeID")
                    ipnData.Currency = currency
                    ipnData.custopmerIP = customerIP
                    ''ipnData.message_type = message_type
                    ''ipnData.fraud_status = fraud_status

                    Dim extraData As String = rs("SalesSiteExtraData")
                    For Each Data As String In extraData.Split(";")
                        If Data.Contains("customReferrer") Then
                            ipnData.CustomReferrer = Data.Split("=")(1)
                        End If
                        If Data.Contains("promoCode") Then
                            ipnData.PromoCode = Data.Split("=")(1)
                        End If

                    Next


                    epochProduct = clsEpochProduct.GetProductBySalesSiteProductID(ipnData.SalesSiteProductID)
                    productCode = epochProduct.SiteProductCode
                    originalTransactionID = ReferenceNumber
                    ReferenceNumber = ReferenceNumber & ". ProductCode:[" & productCode & "]. PaymentType:[" & paymentSubType & "]. MemberID:[" & memberID & "]"



                    Dim ProductDescription As String = ""
                    ProductDescription = epochProduct.Description
                    'ProductDescription = rs("ProductDescription").ToString()

                    If ProductDescription.Contains("Days") Or ProductDescription.Contains("Lifetime") Or ProductDescription.Contains("Unlimited") Then

                        ipnData.SaleDescription = "Add Days with Epoch. Reference: " & ReferenceNumber
                    ElseIf ProductDescription.Contains("Giga") Then

                        If ProductDescription.Contains("Oron") Then
                            ipnData.SaleDescription = "Add oronGiga with Epoch. Reference: " & ReferenceNumber
                        ElseIf ProductDescription.Contains("Uploaded.to") Then
                            ipnData.SaleDescription = "Add ulGiga with Epoch. Reference: " & ReferenceNumber
                        Else

                            ipnData.SaleDescription = "Add Giga with epoch. Reference: " & ReferenceNumber
                        End If
                    ElseIf ProductDescription.Contains("Credits") Then
                        ipnData.SaleDescription = "Add Credits with Epoch. Reference: " & ReferenceNumber

                    Else
                        ipnData.SaleDescription = "Reseller Add Money"

                    End If


                    'ipnData.SaleQuantity = rs("Quantity")
                    ipnData.SaleQuantity = epochProduct.Quantity
                End While
                rs.Close()
                cmd.Connection.Close()

                Try

                    sqlStr = "UPDATE UNI_PayTransactions  SET IPN_OriginalCleanData = '" & ret & "', IsCompleted = 1, Currency = '" & currency & "', BuyerInfo_PayerEmail = '" & buyer.PayerEmail & "', BuyerInfo_FirstName = '" & custFirstName & "', BuyerInfo_LastName = '" & custLastName & "', BuyerInfo_Address = '" & custAddress & "', BuyerInfo_ResidenceCountry = '" & custCountry & "', IPN_NotifierIP = '" & Request.ServerVariables("REMOTE_ADDR") & "', IPN_TransactionID = '" & originalTransactionID & "', Amount = " & amount & ", Fee = " & feeAmount & ", Gross = " & amount - feeAmount & " WHERE UniqueID = '" & UniPayTransactionID & "'"
                    DataHelpers.ExecuteNonQuery(sqlStr, ModGlobals.ConnectionString)
                Catch
                End Try

                Dim sData As String
                sData = ipnData.PayProviderAmount & "+" & ipnData.PaymentDateTime & "+" & ipnData.PayTransactionID & "+" & ipnData.CustomerID & "+" & ipnData.SalesSiteProductID & "Extr@Ded0men@"
                Dim h As New Library.Public.clsHash
                Dim Code As String = Library.Public.clsHash.ComputeHash(sData, "SHA512")
                ipnData.VerifyHASH = Code
                ipnData.BuyerInfo = buyer
                Dim ipnReturn As New clsDataRecordIPNReturn
                ipnReturn = UniPAYIPN.SendIPN(ipnData)

                Try
                    If Not ipnReturn.HasErrors Then
                        sqlStr = "UPDATE UNI_PayTransactions SET IsSendingToSalesSiteIPN = 1, ResponceFromSalesSite = 'Customer Updated' WHERE UniqueID = '" & UniPayTransactionID & "'"
                        DataHelpers.ExecuteNonQuery(sqlStr, ModGlobals.ConnectionString)
                        allok = True
                    Else
                        sqlStr = "UPDATE UNI_PayTransactions SET IsSendingToSalesSiteIPN = 0, ResponceFromSalesSite = '" & ipnReturn.Message.Replace("'", "_") & "' WHERE UniqueID = '" & UniPayTransactionID & "'"
                        DataHelpers.ExecuteNonQuery(sqlStr, ModGlobals.ConnectionString)
                        If ipnReturn.Message.ToLower.Contains("already open") Then
                            allok = True
                        End If
                    End If
                Catch ex As Exception
                    cmd.Connection.Close()
                    sqlStr = "UPDATE UNI_PayTransactions SET IsSendingToSalesSiteIPN = 1, ResponceFromSalesSite = 'Customer Updated' WHERE UniqueID = '" & UniPayTransactionID & "'"
                    DataHelpers.ExecuteNonQuery(sqlStr, ModGlobals.ConnectionString)
                    allok = True
                End Try

            Else
                sqlStr = "UPDATE UNI_PayTransactions SET IPN_OriginalCleanData = 'Error On Status' WHERE UniqueID = '" & UniPayTransactionID & "'"
                DataHelpers.ExecuteNonQuery(sqlStr, ModGlobals.ConnectionString)
                allok = False
            End If

        Catch ex As Exception
            'Mail.SendMailUseGmail("info@zevera.com", "savvas@000.gr", "IMPORTANT!: ERROR on EPOCH ipn", "No ORDER ID " & vbCrLf & "Email: " & CustomerEmaiAddress & vbCrLf & "TransactionID: " & ReferenceNumber)
            clsMyMail.TrySendMail(clsCurrentContext.ExceptionsEmail, "IMPORTANT!: ERROR on EPOCH ipn",
                                  "No ORDER ID " & vbCrLf & _
                                  "Email: " & CustomerEmaiAddress & vbCrLf & _
                                  "TransactionID: " & ReferenceNumber)

            sqlStr = "UPDATE UNI_PayTransactions SET IPN_OriginalCleanData = '" & ex.Message.ToString & "' WHERE UniqueID = '" & UniPayTransactionID & "'"
            DataHelpers.ExecuteNonQuery(sqlStr, ModGlobals.ConnectionString)

            allok = False
        End Try
        If allok Then
            'And Request.ServerVariables("REMOTE_ADDR") = "188.165.25.51" Then

            Dim LoginName As String = DataHelpers.GetEUS_Profile_LoginName_ByProfileID(ipnData.CustomerID)

            Dim count As Integer = DataHelpers.EUS_CustomerTransaction_CountTransactions(ipnData.CustomerID)
            Dim isRebill As Boolean = (count > 1)

            Dim ipnNotifier As Goomena.Server.PaymentIPNWS.DLL.IPNNotifier = Nothing
            Dim ipnNotifierResult As String = ""


            Try
                Dim epochipn_ws_url As String = ConfigurationManager.AppSettings("epochipn_ws_url")

                ipnNotifier = New Goomena.Server.PaymentIPNWS.DLL.IPNNotifier()
                ipnNotifier.UniPayTransactionID = UniPayTransactionID
                ipnNotifier.CustomerEmaiAddress = CustomerEmaiAddress
                ipnNotifier.RefereceNumber = ReferenceNumber
                ipnNotifier.amount = amount
                ipnNotifier.status = status
                ipnNotifier.custFirstName = custFirstName
                ipnNotifier.custLastName = custLastName
                ipnNotifier.custAddress = custAddress
                ipnNotifier.custCity = custCity
                ipnNotifier.custState = custState
                ipnNotifier.custCountry = custCountry
                ipnNotifier.custZip = custZip
                ipnNotifier.customerIP = customerIP
                ipnNotifier.feeAmount = feeAmount
                ipnNotifier.netAmount = netAmount
                ipnNotifier.currency = currency
                ipnNotifier.memberID = memberID
                ipnNotifier.STRproductCode = productCode
                ipnNotifier.paymentMethod = "epoch"
                ipnNotifier.paymentSubType = paymentSubType
                ipnNotifier.siteid = ipnData.SalesSiteID
                ipnNotifier.loginname = LoginName
                ipnNotifier.customerid = ipnData.CustomerID
                ipnNotifier.customReferrer = ipnData.CustomReferrer
                ipnNotifier.isRebill = isRebill

                ipnNotifierResult = ipnNotifier.SendResults(epochipn_ws_url)
                If (ipnNotifierResult = "OK") Then
                    sqlStr = "UPDATE UNI_PayTransactions SET UnipaySideVerified=1, SerializedIPNNotifier=null WHERE UniqueID = '" & UniPayTransactionID & "'"
                    DataHelpers.ExecuteNonQuery(sqlStr, ModGlobals.ConnectionString)
                Else
                    '' we have to retry next time
                    Try

                        Dim xmlresult As New StringWriter()
                        Dim x As New System.Xml.Serialization.XmlSerializer(ipnNotifier.GetType)
                        x.Serialize(xmlresult, ipnNotifier)

                        sqlStr = "UPDATE UNI_PayTransactions SET UnipaySideVerified=0, SerializedIPNNotifier=@SerializedIPNNotifier WHERE UniqueID='" & UniPayTransactionID & "'"
                        Dim cmd As New SqlCommand(sqlStr)
                        cmd.Parameters.AddWithValue("@SerializedIPNNotifier", xmlresult.ToString())
                        DataHelpers.ExecuteNonQuery(cmd, ModGlobals.ConnectionString)

                    Catch ex As Exception
                        WebErrorSendEmail(ex, "serializing ipnNotifier for retry")
                    End Try
                End If


                Response.Write(ipnNotifierResult)
                Response.Write(vbCrLf)

            Catch ex As Exception
                WebErrorSendEmail(ex, "epochIPNHandler")
            Finally
                If (ipnNotifier IsNot Nothing) Then ipnNotifier.Dispose()
            End Try

            Response.Write("!!!ALLOK!!!")
            Response.Write(vbCrLf)
        End If

    End Sub


    Private Function GenerateHash(ByVal SourceText As String) As String
        'Create an encoding object to ensure the encoding standard for the source text
        Dim Ue As New UnicodeEncoding()
        'Retrieve a byte array based on the source text
        Dim ByteSourceText() As Byte = Ue.GetBytes(SourceText)
        'Instantiate an MD5 Provider object
        Dim Md5 As New MD5CryptoServiceProvider()
        'Compute the hash value from the source
        Dim ByteHash() As Byte = Md5.ComputeHash(ByteSourceText)
        'And convert it to String format for return
        Return Convert.ToBase64String(ByteHash)
    End Function

End Class
﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports Dating.Server.Core.DLL

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class UniPAYIPN
    Inherits System.Web.Services.WebService



    Public Shared Function SendIPN(ByVal ipnData As clsDataRecordIPN) As clsDataRecordIPNReturn ', ByVal loc_url As String
        Dim ret As New clsDataRecordIPNReturn
        ret.HasErrors = False
        ret.ErrorCode = 0
        ret.Message = ""
        ret.ExtraMessage = ""

        Try

            Dim ipn As New UniPAYIPN() 'loc_url
            ret = ipn.IPNEventRaised(ipnData)

        Catch ex As Exception
            ret.HasErrors = True
            ret.ErrorCode = 1
            ret.Message = ex.Message
            ret.ExtraMessage = ex.ToString()
        End Try

        Return ret
    End Function



    Dim Description As String
    Dim Money As Double
    Dim TransactionID As String
    Dim CreditsPurchased As Integer
    Dim PurchaseGigabytes As Integer
    Dim REMOTE_ADDR As String
    Dim HTTP_USER_AGENT As String
    Dim HTTP_REFERER As String
    Dim CustomerID As Integer
    Dim CustomReferrer As String

    ''' <summary>
    ''' Regex to read ProductCode from description
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared Regex_ProductCode As Regex

    ''' <summary>
    ''' Regex to read MemberID from description
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared Regex_MemberID As Regex

    ''' <summary>
    ''' Regex to read PaymentType from description
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared Regex_PaymentType As Regex
    Private Shared Regex_BonusCode As Regex

    Shared Sub New()
        Regex_ProductCode = New Regex("ProductCode:\[(?<ProductCode>[^\]]*)\]", RegexOptions.IgnoreCase Or RegexOptions.Compiled)
        Regex_MemberID = New Regex("MemberID:\[(?<MemberID>[^\]]*)\]", RegexOptions.IgnoreCase Or RegexOptions.Compiled)
        Regex_PaymentType = New Regex("PaymentType:\[(?<PaymentType>[^\]]*)\]", RegexOptions.IgnoreCase Or RegexOptions.Compiled)
        Regex_BonusCode = New Regex("BonusCode:\[(?<BonusCode>[^\]]*)\]", RegexOptions.IgnoreCase Or RegexOptions.Compiled)
    End Sub

    <WebMethod()> _
    Public Function HelloWorld() As String
        Return "Hello World"
    End Function
    ' Exaples of call UniPAY
    ' http://www.soundsoft.com/PAY/pay.ashx?pid=12&pp=1&cid=1234&promo=ddddd&exd=dddddddd
    ' pid = to product ID apo tin DB tou UniPAY
    ' pp = Use ton PayProvider me ID 1 px Paypal alios tha xrisimophisi ton default
    ' cid = Customer ID
    ' promo= Promo Code
    ' exd = (optionasl) extra data
    ' Me krifo to soundsoft....
    ' Kanoume to paymix.com na kani redirect sto soundsoft 


    <WebMethod()> _
    Public Function IPNEventRaised(ByVal cDataRecordIPN As clsDataRecordIPN) As clsDataRecordIPNReturn
        Dim cDataRecordIPNReturn As New clsDataRecordIPNReturn
        Dim toAddress As String = ConfigurationManager.AppSettings("gToEmail")

        Try
            If (cDataRecordIPN IsNot Nothing) Then cDataRecordIPN.PromoCode = ""
            LogInput(cDataRecordIPN)

            If VerifyHASH(cDataRecordIPN) Then

                Dim paymethod As PaymentMethods = GetPayMethod(cDataRecordIPN.SaleDescription)


                ' Ine OK Proxora stin analisi....
                Dim productCode As String = Nothing
                Try
                    'Dim r As New Regex("ProductCode:\[(?<ProductCode>.*)\][.]|ProductCode:\[(?<ProductCode>.*)\]", RegexOptions.IgnoreCase)
                    Dim m As Match = Regex_ProductCode.Match(cDataRecordIPN.SaleDescription)
                    If (m.Success) Then
                        productCode = m.Groups("ProductCode").Value
                        If (productCode.EndsWith("zt")) Then productCode = productCode.Remove(productCode.Length - 2)
                    End If
                Catch ex As Exception
                End Try


                ' Ine OK Proxora stin analisi....
                Dim MemberID As String = Nothing
                Try
                    'Dim r As New Regex("MemberID:\[(?<MemberID>.*)\][.]|MemberID:\[(?<MemberID>.*)\]", RegexOptions.IgnoreCase)
                    Dim m As Match = Regex_MemberID.Match(cDataRecordIPN.SaleDescription)
                    If (m.Success) Then
                        MemberID = m.Groups("MemberID").Value
                    End If
                Catch ex As Exception
                End Try



                Dim bonusCode As String = Nothing
                Try
                    Dim m As Match = Regex_BonusCode.Match(cDataRecordIPN.SaleDescription)
                    If (m.Success) Then
                        bonusCode = m.Groups("BonusCode").Value
                        If (Not String.IsNullOrEmpty(bonusCode) AndAlso Not DataHelpers.EUS_CreditsBonusCodes_IsCodeAvailable(cDataRecordIPN.CustomerID, bonusCode)) Then
                            bonusCode = Nothing

                            clsMyMail.TrySendMail(ConfigurationManager.AppSettings("PaymentLogsEmail"), "no purch", cDataRecordIPN.SaleDescription)
                            cDataRecordIPNReturn.HasErrors = True
                            cDataRecordIPNReturn.ErrorCode = 100
                            cDataRecordIPNReturn.Message = "no purch"
                            Return cDataRecordIPNReturn
                        End If
                    End If
                Catch ex As Exception
                End Try

                If Not String.IsNullOrEmpty(productCode) Then

                    FillTransInfo(cDataRecordIPN.SaleDescription,
                                  cDataRecordIPN.PayProviderAmount,
                                  cDataRecordIPN.PayProviderTransactionID,
                                  cDataRecordIPN.SaleQuantity, 0,
                                  cDataRecordIPN.custopmerIP,
                                  HttpContext.Current.Request.ServerVariables("HTTP_USER_AGENT"),
                                  HttpContext.Current.Request.ServerVariables("HTTP_REFERER"),
                                  cDataRecordIPN.CustomerID,
                                  cDataRecordIPN.CustomReferrer)

                ElseIf Not String.IsNullOrEmpty(bonusCode) Then

                    FillTransInfo(cDataRecordIPN.SaleDescription,
                                      cDataRecordIPN.PayProviderAmount,
                                      cDataRecordIPN.PayProviderTransactionID,
                                      cDataRecordIPN.SaleQuantity, 0,
                                      cDataRecordIPN.custopmerIP,
                                      HttpContext.Current.Request.ServerVariables("HTTP_USER_AGENT"),
                                      HttpContext.Current.Request.ServerVariables("HTTP_REFERER"),
                                      cDataRecordIPN.CustomerID,
                                      cDataRecordIPN.CustomReferrer)

                    cDataRecordIPN.PromoCode = bonusCode


                ElseIf cDataRecordIPN.SaleDescription.ToUpper().Contains("DAYS") OrElse _
                    cDataRecordIPN.SaleDescription.ToUpper().Contains("CREDITS") OrElse _
                    cDataRecordIPN.SaleDescription.Contains("Lifetime") OrElse _
                    cDataRecordIPN.SaleDescription.Contains("Unlimited") OrElse _
                    cDataRecordIPN.SaleDescription.Contains("Reseller") Then

                    FillTransInfo(cDataRecordIPN.SaleDescription,
                                  cDataRecordIPN.PayProviderAmount,
                                  cDataRecordIPN.PayProviderTransactionID,
                                  cDataRecordIPN.SaleQuantity, 0,
                                  cDataRecordIPN.custopmerIP,
                                  HttpContext.Current.Request.ServerVariables("HTTP_USER_AGENT"),
                                  HttpContext.Current.Request.ServerVariables("HTTP_REFERER"),
                                  cDataRecordIPN.CustomerID,
                                  cDataRecordIPN.CustomReferrer)

                    'ElseIf cDataRecordIPN.SaleDescription.Contains("Giga") Then
                    '    FillTransInfo(cDataRecordIPN.SaleDescription,
                    '                  cDataRecordIPN.PayProviderAmount,
                    '                  cDataRecordIPN.PayProviderTransactionID, 0,
                    '                  cDataRecordIPN.SaleQuantity,
                    '                  cDataRecordIPN.custopmerIP,
                    '                  HttpContext.Current.Request.ServerVariables("HTTP_USER_AGENT"),
                    '                  HttpContext.Current.Request.ServerVariables("HTTP_REFERER"),
                    '                  cDataRecordIPN.CustomerID,
                    '                  cDataRecordIPN.CustomReferrer)

                Else
                    clsMyMail.TrySendMail(ConfigurationManager.AppSettings("PaymentLogsEmail"), "no purch", "CustomerID " & CustomerID)
                    cDataRecordIPNReturn.HasErrors = True
                    cDataRecordIPNReturn.ErrorCode = 100
                    cDataRecordIPNReturn.Message = "no purch"
                    Return cDataRecordIPNReturn

                End If


                Dim prms As New clsCustomer.AddTransactionParams()
                prms.Description = Description
                prms.Money = Money
                prms.TransactionInfo = TransactionID
                prms.p4 = ""
                prms.CreditsPurchased = CreditsPurchased
                prms.REMOTE_ADDR = REMOTE_ADDR
                prms.HTTP_USER_AGENT = HTTP_USER_AGENT
                prms.HTTP_REFERER = HTTP_REFERER
                prms.CustomerID = CustomerID
                prms.CustomReferrer = CustomReferrer
                prms.PayerEmail = cDataRecordIPN.BuyerInfo.PayerEmail
                prms.paymethod = paymethod
                prms.PromoCode = cDataRecordIPN.PromoCode
                prms.TransactionTypeID = cDataRecordIPN.TransactionTypeID
                prms.productCode = productCode
                prms.Currency = cDataRecordIPN.Currency
                prms.cDataRecordIPN = cDataRecordIPN
                prms.MemberID = MemberID


                clsCustomer.AddTransaction(prms)

                'clsCustomer.AddTransaction(Description,
                '                           Money,
                '                           TransactionID, "",
                '                           CreditsPurchased,
                '                           REMOTE_ADDR,
                '                           HTTP_USER_AGENT,
                '                           HTTP_REFERER,
                '                           CustomerID,
                '                           CustomReferrer,
                '                           cDataRecordIPN.BuyerInfo.PayerEmail,
                '                           paymethod,
                '                           cDataRecordIPN.PromoCode,
                '                           cDataRecordIPN.TransactionTypeID,
                '                           productCode,
                '                           cDataRecordIPN.Currency,
                '                           cDataRecordIPN)
                cDataRecordIPNReturn.HasErrors = False
                cDataRecordIPNReturn.ErrorCode = 0
            Else
                cDataRecordIPNReturn.HasErrors = True
                cDataRecordIPNReturn.ErrorCode = 101
                cDataRecordIPNReturn.Message = "Errors on HASH"
            End If
        Catch ex As Exception
            cDataRecordIPNReturn.HasErrors = True
            cDataRecordIPNReturn.ErrorCode = 100
            cDataRecordIPNReturn.Message = ex.Message
            clsMyMail.TrySendMail(ConfigurationManager.AppSettings("PaymentLogsEmail"), "Payment service exceptions", ex.ToString(), False)
        End Try
        Return cDataRecordIPNReturn
    End Function

    Private Sub FillTransInfo(ByVal tDesc As String,
                              ByVal tMoney As Double,
                              ByVal tTransID As String,
                              ByVal tCredits As Integer,
                              ByVal tPGiga As Integer,
                              ByVal tRemAddr As String,
                              ByVal tUserAgt As String,
                              ByVal tHttpRef As String,
                              ByVal tcustID As Integer,
                              ByVal tCustRef As String)
        Description = tDesc
        Money = tMoney
        TransactionID = tTransID
        CreditsPurchased = tCredits
        PurchaseGigabytes = tPGiga
        REMOTE_ADDR = tRemAddr
        HTTP_USER_AGENT = tUserAgt
        HTTP_REFERER = tHttpRef
        CustomerID = tcustID
        CustomReferrer = tCustRef

    End Sub

    Private Function VerifyHASH(ByVal cDataRecordIPN As clsDataRecordIPN) As Boolean
        Try
            Dim sData As String
            sData = cDataRecordIPN.PayProviderAmount & "+" & cDataRecordIPN.PaymentDateTime & "+" & cDataRecordIPN.PayTransactionID & "+" & cDataRecordIPN.CustomerID & "+" & cDataRecordIPN.SalesSiteProductID & "Extr@Ded0men@"
            Dim h As New Library.Public.clsHash
            Dim Code As String = Library.Public.clsHash.ComputeHash(sData, "SHA512")
            If Code = cDataRecordIPN.VerifyHASH Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            Return False
        End Try
    End Function


    Public Shared Function GetPayMethod(SaleDescription As String)
        Dim paymethod As PaymentMethods = PaymentMethods.None


        Try
            Dim m As Match = Regex_PaymentType.Match(SaleDescription)
            If (m.Success) Then
                Dim str As String = m.Groups("PaymentType").Value
                str = str.ToLower()

                Select Case (str)
                    Case "pp", "paypal"
                        paymethod = PaymentMethods.PayPal

                    Case "pv", "ps"
                        paymethod = PaymentMethods.PaySafe

                    Case "cc", "vs", "mc"
                        paymethod = PaymentMethods.CreditCard

                End Select
            End If
        Catch ex As Exception
        End Try


        If (paymethod = PaymentMethods.None) Then
            Dim salesUp As String = SaleDescription.ToUpper()
            If salesUp.Contains("CREDITCARD") Then
                paymethod = PaymentMethods.CreditCard

            ElseIf salesUp.Contains("EPOCH") Then
                paymethod = PaymentMethods.CreditCard

            ElseIf salesUp.Contains("ALERTPAY") Then
                paymethod = PaymentMethods.AlertPay

            ElseIf salesUp.Contains("PAYSAFECARD") Then
                paymethod = PaymentMethods.PaySafe

            ElseIf salesUp.Contains("2CO") Then
                paymethod = PaymentMethods.CreditCard

            ElseIf salesUp.Contains("WEBMONEY") Then
                paymethod = PaymentMethods.WebMoney
            Else
                paymethod = PaymentMethods.PayPal
            End If

        End If

        Return paymethod
    End Function

    Sub LogInput(cDataRecordIPN As clsDataRecordIPN)
        Try

            Dim paymethod As PaymentMethods
            Try
                paymethod = GetPayMethod(cDataRecordIPN.SaleDescription)
            Catch ex As Exception
            End Try


            ' Ine OK Proxora stin analisi....
            Dim productCode As String = Nothing
            Try
                Dim m As Match = Regex_ProductCode.Match(cDataRecordIPN.SaleDescription)
                If (m.Success) Then
                    productCode = m.Groups("ProductCode").Value
                    If (productCode.EndsWith("zt")) Then productCode = productCode.Remove(productCode.Length - 2)
                End If
            Catch ex As Exception
            End Try


            ' Ine OK Proxora stin analisi....
            Dim MemberID As String = Nothing
            Try
                Dim m As Match = Regex_MemberID.Match(cDataRecordIPN.SaleDescription)
                If (m.Success) Then
                    MemberID = m.Groups("MemberID").Value
                End If
            Catch ex As Exception
            End Try


            Dim body As String = clsCustomer.GetMessageBody(
                                  cDataRecordIPN,
                                  HttpContext.Current.Request.ServerVariables("HTTP_USER_AGENT"),
                                  HttpContext.Current.Request.ServerVariables("HTTP_REFERER"),
                                  paymethod,
                                  productCode,
                                  MemberID)

            'Dim body As String = clsCustomer.GetMessageBody(
            '    cDataRecordIPN.SaleDescription,
            '    cDataRecordIPN.PayProviderAmount,
            '    cDataRecordIPN.PayProviderTransactionID,
            '    "",
            '    cDataRecordIPN.SaleQuantity,
            '    cDataRecordIPN.custopmerIP,
            '    HttpContext.Current.Request.ServerVariables("HTTP_USER_AGENT"),
            '    HttpContext.Current.Request.ServerVariables("HTTP_REFERER"),
            '    cDataRecordIPN.CustomerID,
            '    cDataRecordIPN.CustomReferrer,
            '    cDataRecordIPN.BuyerInfo.PayerEmail,
            '    paymethod,
            '    cDataRecordIPN.PromoCode,
            '    cDataRecordIPN.TransactionTypeID)


            clsMyMail.TrySendMail(ConfigurationManager.AppSettings("PaymentLogsEmail"), "Payment service log", body, True)
        Catch ex As Exception

        End Try

    End Sub
End Class
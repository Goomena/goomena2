﻿Imports Library.Public
Imports DevExpress.Web.ASPxMenu
Imports Dating.Server.Core.DLL
Imports DevExpress.Web.ASPxEditors
Imports System.Data.SqlClient

Public Class _RootLandingMaster
    Inherits BaseMasterPagePublic


    'Dim _pageData As clsPageData
    'Protected ReadOnly Property CurrentPageData As clsPageData
    '    Get
    '        If (_pageData Is Nothing) Then _pageData = New clsPageData("master", Context)
    '        Return _pageData
    '    End Get
    'End Property


    Private Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        Try
            AddHandler ucLogin1.LoggingInFailed, AddressOf ucLogin1_LoggingInFailed

            If Not Me.IsPostBack Then
                Dim lagIdOnStart As String = Session("LagID")
                If (Session("SessionStarted_RedirectToCountryLAG") = True) Then
                    Session("SessionStarted_RedirectToCountryLAG") = Nothing

                    Try

                        If (LanguageHelper.SessionStarted_RedirectToCountryLAG(True) = True) Then
                            Return
                        End If

                    Catch ex As System.Threading.ThreadAbortException
                    Catch ex As Exception
                        WebErrorMessageBox(Me, ex, "")
                    End Try
                End If

                If (Not String.IsNullOrEmpty(Me.RequestedLAGID)) Then
                    Session("LagID") = Me.RequestedLAGID
                Else
                    Session("LagID") = clsLanguageHelper.GetLagCookie()
                End If

                If (Not clsLanguageHelper.IsSupportedLAG(Session("LagID"), Session("GEO_COUNTRY_CODE"))) Then
                    clsLanguageHelper.SetLAGID()
                End If

                If (clsLanguageHelper.IsSupportedLAG(Session("LagID"), Session("GEO_COUNTRY_CODE"))) Then
                    cmbLag.Items(clsLanguageHelper.GetIndexFromLagID(Session("LagID"))).Selected = True
                End If
                clsLanguageHelper.SetLagCookie(Session("LagID"))
                SetLanguageLinks()

                ' update lagid info
                Try
                    If (lagIdOnStart <> Session("LagID")) Then
                        If (Me.Session("ProfileID") > 0) Then
                            DataHelpers.UpdateEUS_Profiles_LAGID(Me.Session("ProfileID"), Me.Session("LagID"))
                        End If
                    End If
                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "")
                End Try
            End If


            If (cmbLag.SelectedItem Is Nothing) Then
                If (clsLanguageHelper.IsSupportedLAG(Session("LagID"), Session("GEO_COUNTRY_CODE"))) Then
                    cmbLag.Items(clsLanguageHelper.GetIndexFromLagID(Session("LagID"))).Selected = True
                End If
                clsLanguageHelper.SetLagCookie(Session("LagID"))
            End If


            Try
                ' otan to session exei diaforetiki glwsa apo tin epilegmeni glwssa
                If (clsLanguageHelper.GetIndexFromLagID(Session("LagID")) = 0 AndAlso Session("LagID") <> "US") Then
                    Session("LagID") = gDomainCOM_DefaultLAG
                    clsLanguageHelper.SetLagCookie(Session("LagID"))
                End If
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try


            If Not Me.IsPostBack OrElse Me.VerifyLagProps() Then
                LoadLAG()
            End If

            If (Session("MAX_LOGIN_RETRIES") = 1 OrElse Session("MAX_LOGIN_RETRIES") = 2) Then
                Dim MainLogin As System.Web.UI.WebControls.Login = ucLogin1.LoginControl

                Dim btnLogin As ASPxButton = MainLogin.FindControl("btnLogin")
                If (btnLogin IsNot Nothing) Then btnLogin.PostBackUrl = "~/Login.aspx"

                Dim lblUsernameTopTD As HtmlControl = MainLogin.FindControl("lblUsernameTopTD")
                If (lblUsernameTopTD IsNot Nothing) Then lblUsernameTopTD.Visible = False

                Dim lblPasswordTopTD As HtmlControl = MainLogin.FindControl("lblPasswordTopTD")
                If (lblPasswordTopTD IsNot Nothing) Then lblPasswordTopTD.Visible = False

                Dim UserNameTD As HtmlControl = MainLogin.FindControl("UserNameTD")
                If (UserNameTD IsNot Nothing) Then UserNameTD.Visible = False

                Dim PasswordTD As HtmlControl = MainLogin.FindControl("PasswordTD")
                If (PasswordTD IsNot Nothing) Then PasswordTD.Visible = False

                Dim chkRememberMeTD As HtmlControl = MainLogin.FindControl("chkRememberMeTD")
                If (chkRememberMeTD IsNot Nothing) Then chkRememberMeTD.Visible = False

                Dim lbForgotPasswordTD As HtmlControl = MainLogin.FindControl("lbForgotPasswordTD")
                If (lbForgotPasswordTD IsNot Nothing) Then lbForgotPasswordTD.Visible = False

                If (Request.Url.LocalPath = "/Login.aspx") Then
                    Dim btnLoginTD As HtmlControl = MainLogin.FindControl("btnLoginTD")
                    If (btnLoginTD IsNot Nothing) Then btnLoginTD.Visible = False

                    Dim btnLoginTopTD As HtmlControl = MainLogin.FindControl("btnLoginTopTD")
                    If (btnLoginTopTD IsNot Nothing) Then btnLoginTopTD.Visible = False

                    Dim btnLoginBotTD As HtmlControl = MainLogin.FindControl("btnLoginBotTD")
                    If (btnLoginBotTD IsNot Nothing) Then btnLoginBotTD.Visible = False
                End If
            End If

        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Try
            LoadPhotos()
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            ' ShowUserMap()

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Private Sub ModifyApplicationForLanguageChanges()
        Try
            If (Not Page.IsPostBack) Then
                If (Not String.IsNullOrEmpty(Me.RequestedLAGID)) Then
                    cmbLag.SelectedIndex = clsLanguageHelper.GetIndexFromLagID(Me.RequestedLAGID)
                End If
            End If

            'If (Session("LagID") <> cmbLag.SelectedItem.Value) Then
            clsLanguageHelper.SetLagCookie(cmbLag.SelectedItem.Value)
            'End If

        Catch ex As Exception
            Session("LagID") = cmbLag.SelectedItem.Value
        End Try
    End Sub



    Protected Sub cmbLag_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbLag.SelectedIndexChanged
        ModifyApplicationForLanguageChanges()
        Try

            Me._pageData = Nothing
            LoadLAG()

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try


        Try
            ModGlobals.UpdateUserControls(Me.Page, True)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


    Protected Sub LoadLAG()
        Try
            lnkLogo.NavigateUrl = MyBase.LanguageHelper.GetPublicURL("/Default.aspx", MyBase.GetLag())
            imgLogo.Alt = CurrentPageData.GetCustomString("imgLogo.Alt.Text")
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Try
            'PopulateMenuData(mnuHeaderLeft, "topLeft")
            'PopulateMenuData(mnuHeaderRight, "topRight")
            'PopulateMenuData(mnuFooter, "bottom")
            'PopulateMenuData(mnuHeader, "bottom")

            clsMenuHelper.PopulateMenuData(mnuNavi, "topLeft")
            clsMenuHelper.CheckPublicMenuLinks(mnuNavi, GetLag())

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Try
            ltrMenMessage.Text = CurrentPageData.GetCustomString("ltrMenMessage2")
            ltrLadiesMessage.Text = CurrentPageData.GetCustomString("ltrLadiesMessage2")
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        'Try
        '    pmb1.LoadLAG()
        'Catch ex As Exception
        '    WebErrorMessageBox(Me, ex, "")
        'End Try


        Try
            LoadLAG_LoginControl()
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


    Protected Sub LoadLAG_LoginControl()
        Try
            Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            Dim MainLogin As System.Web.UI.WebControls.Login = ucLogin1.LoginControl
            'MainLogin.UserNameLabelText = CurrentPageData.GetCustomString("txtUserNameLabelText")
            'MainLogin.PasswordLabelText = CurrentPageData.GetCustomString("txtPasswordLabelText")
            'MainLogin.RememberMeText = CurrentPageData.GetCustomString("txtRememberMeText")
            'MainLogin.LoginButtonText = CurrentPageData.GetCustomString("txtLoginButtonText")
            'MainLogin.UserNameRequiredErrorMessage = CurrentPageData.GetCustomString("txtUserNameRequiredErrorMessage")
            MainLogin.TitleText = CurrentPageData.GetCustomString("txtLoginTitleText")

            Dim lbForgotPassword As ASPxHyperLink = MainLogin.FindControl("lbForgotPassword")
            If (lbForgotPassword IsNot Nothing) Then
                lbForgotPassword.Text = CurrentPageData.GetCustomString("lbForgotPassword")
            End If

            Dim lbLoginFB As ASPxHyperLink = MainLogin.FindControl("lbLoginFB")
            If (lbLoginFB IsNot Nothing) Then
                lbLoginFB.Text = CurrentPageData.GetCustomString("lbLoginFB")
            End If

            Dim btnJoinToday As ASPxButton = MainLogin.FindControl("btnJoinToday")
            If (btnJoinToday IsNot Nothing) Then
                btnJoinToday.Text = CurrentPageData.GetCustomString("btnJoinToday")
                btnJoinToday.PostBackUrl = "~/Register.aspx"
                If (Not String.IsNullOrEmpty(Request.QueryString("ReturnUrl"))) Then
                    btnJoinToday.PostBackUrl = btnJoinToday.PostBackUrl & "?ReturnUrl=" & HttpUtility.UrlEncode(Request.QueryString("ReturnUrl"))
                End If
            End If

            Dim UserName As ASPxTextBox = MainLogin.FindControl("UserName")
            If (UserName IsNot Nothing) Then
                'UserName.NullText = CurrentPageData.GetCustomString("UserName.NullText")
                UserName.ValidationSettings.RequiredField.ErrorText = CurrentPageData.GetCustomString("UserName.ValidationSettings.RequiredField.ErrorText")
            End If

            Dim Password As ASPxTextBox = MainLogin.FindControl("Password")
            If (Password IsNot Nothing) Then
                'Password.NullText = CurrentPageData.GetCustomString("Password.NullText")
                Password.ValidationSettings.RequiredField.ErrorText = CurrentPageData.GetCustomString("Password.ValidationSettings.RequiredField.ErrorText")
            End If

            Dim lblUsernameTop As Label = MainLogin.FindControl("lblUsernameTop")
            If (lblUsernameTop IsNot Nothing) Then
                lblUsernameTop.Text = CurrentPageData.GetCustomString("UserName.NullText")
            End If

            Dim lblPasswordTop As Label = MainLogin.FindControl("lblPasswordTop")
            If (lblPasswordTop IsNot Nothing) Then
                lblPasswordTop.Text = CurrentPageData.GetCustomString("Password.NullText")
            End If


            Dim btnLogin As ASPxButton = MainLogin.FindControl("btnLogin")
            If (btnLogin IsNot Nothing) Then
                btnLogin.Text = CurrentPageData.GetCustomString("btnLogin")
            End If

            Dim RememberMe As CheckBox = MainLogin.FindControl("RememberMe")
            If (RememberMe IsNot Nothing) Then
                RememberMe.Text = CurrentPageData.GetCustomString("RememberMe")
            End If


            Dim chkRememberMe As ASPxCheckBox = MainLogin.FindControl("chkRememberMe")
            If (chkRememberMe IsNot Nothing) Then
                chkRememberMe.Text = CurrentPageData.GetCustomString("RememberMe")
            End If


            '' controls found on login.aspx page
            Dim lblUserName As Literal = MainLogin.FindControl("lblUserName")
            If (lblUserName IsNot Nothing) Then
                lblUserName.Text = CurrentPageData.GetCustomString("lblUserName")
            End If


            Dim lblPassword As Literal = MainLogin.FindControl("lblPassword")
            If (lblPassword IsNot Nothing) Then
                lblPassword.Text = CurrentPageData.GetCustomString("lblPassword")
            End If

            Dim btnLoginNow As ASPxButton = MainLogin.FindControl("btnLoginNow")
            If (btnLoginNow IsNot Nothing) Then
                btnLoginNow.Text = CurrentPageData.GetCustomString("btnLoginNow")
                btnLoginNow.Text = btnLoginNow.Text.Replace("&nbsp;", " ")
            End If


        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Public Sub ShowUserMap()
        Try

            Dim dt As New DataTable()
            Dim zoom As Integer = 9
            Dim radius As Integer = 10



            If (Not String.IsNullOrEmpty(Session("GEO_COUNTRY_POSTALCODE"))) Then
                dt = clsGeoHelper.GetGEOByZip(Session("GEO_COUNTRY_CODE"), Session("GEO_COUNTRY_POSTALCODE"), Session("LAGID"))

            ElseIf (Not String.IsNullOrEmpty(Session("GEO_COUNTRY_LATITUDE")) AndAlso Not String.IsNullOrEmpty(Session("GEO_COUNTRY_LONGITUDE"))) Then
                Dim sLAT As String = Session("GEO_COUNTRY_LATITUDE")
                Dim sLON As String = Session("GEO_COUNTRY_LONGITUDE")

                Dim periodLAT As Integer = sLAT.IndexOf(".")
                If (Len(sLAT) > periodLAT + 3) Then sLAT = sLAT.Remove(periodLAT + 3)

                Dim periodLON As Integer = sLON.IndexOf(".")
                If (Len(sLON) > periodLON + 3) Then sLON = sLON.Remove(periodLON + 3)

                dt = clsGeoHelper.GetGEOWithLatitudeAndLongitude(Session("GEO_COUNTRY_CODE"), sLAT, sLON, Session("LAGID"))

            ElseIf (Not String.IsNullOrEmpty(Session("GEO_COUNTRY_CITY"))) Then
                dt = clsGeoHelper.GetGEOWithCity(Session("GEO_COUNTRY_CODE"), Session("GEO_COUNTRY_CITY"))

            End If

            If (dt.Rows.Count = 0) Then
                dt = clsGeoHelper.GetGEOByZip("GR", "10431", Session("LAGID"))
                radius = 60
                zoom = 6
            End If

            Dim lat As Double = dt.Rows(0)("latitude")
            Dim lng As Double = dt.Rows(0)("longitude")
            'ShowUserMap(lat, lng, radius, zoom)

            Session("GEO_COUNTRY_CITY") = dt.Rows(0)("city")
            Session("GEO_COUNTRY_LATITUDE") = lat
            Session("GEO_COUNTRY_LONGITUDE") = lng
            Session("GEO_COUNTRY_POSTALCODE") = dt.Rows(0)("postcode")

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        Finally
        End Try

    End Sub



    Private Sub SetLanguageLinks()
        Try
            If (clsLanguageHelper.HasCountryConfig(Session("GEO_COUNTRY_CODE"))) Then
                Dim cc As clsLanguageHelper.CountryConfig = clsLanguageHelper.GetCountryConfig(Session("GEO_COUNTRY_CODE"))
                For itms = cmbLag.Items.Count - 1 To 0 Step -1
                    If (cc.ShowLangs.Contains(cmbLag.Items(itms).Value)) Then
                        Continue For
                    Else
                        cmbLag.Items.RemoveAt(itms)
                    End If
                Next
            End If

            Dim url As String = Request.Url.AbsolutePath
            Dim attributes As New Dictionary(Of String, String)
            For Each itm As ListEditItem In cmbLag.Items
                Select Case itm.Value
                    Case "US"
                        If (Not itm.Text.StartsWith("<a")) Then
                            url = LanguageHelper.GetNewLAGUrl(itm.Index)
                            If (Not Regex.IsMatch(url, "[?&]lang=1", RegexOptions.IgnoreCase)) Then
                                url = If(url IsNot Nothing, url, "") & If(url.IndexOf("?") > -1, "&", "?") & "lang=1"
                            End If
                            attributes.Clear()
                            attributes.Add("class", itm.Value)
                            itm.Text = clsHTMLHelper.RenderLinkHtml(url, itm.Text, attributes)
                            itm.ImageUrl = Me.Request.Url.Scheme & "://cdn.goomena.com/Images/spacer10.png"
                        End If
                    Case "ES"
                        If (Not itm.Text.StartsWith("<a")) Then
                            url = LanguageHelper.GetNewLAGUrl(itm.Index)
                            If (Not Regex.IsMatch(url, "[?&]lang=1", RegexOptions.IgnoreCase)) Then
                                url = If(url IsNot Nothing, url, "") & If(url.IndexOf("?") > -1, "&", "?") & "lang=1"
                            End If
                            attributes.Clear()
                            attributes.Add("class", itm.Value)
                            itm.Text = clsHTMLHelper.RenderLinkHtml(url, itm.Text, attributes)
                            itm.ImageUrl = Me.Request.Url.Scheme & "://cdn.goomena.com/Images/spacer10.png"
                        End If
                    Case "GR"
                        If (Not itm.Text.StartsWith("<a")) Then
                            url = LanguageHelper.GetNewLAGUrl(itm.Index)
                            If (Not Regex.IsMatch(url, "[?&]lang=1", RegexOptions.IgnoreCase)) Then
                                url = If(url IsNot Nothing, url, "") & If(url.IndexOf("?") > -1, "&", "?") & "lang=1"
                            End If
                            attributes.Clear()
                            attributes.Add("class", itm.Value)
                            itm.Text = clsHTMLHelper.RenderLinkHtml(url, itm.Text, attributes)
                            itm.ImageUrl = Me.Request.Url.Scheme & "://cdn.goomena.com/Images/spacer10.png"
                        End If
                    Case "TR"
                        If (Not itm.Text.StartsWith("<a")) Then
                            url = LanguageHelper.GetNewLAGUrl(itm.Index)
                            If (Not Regex.IsMatch(url, "[?&]lang=1", RegexOptions.IgnoreCase)) Then
                                url = If(url IsNot Nothing, url, "") & If(url.IndexOf("?") > -1, "&", "?") & "lang=1"
                            End If
                            attributes.Clear()
                            attributes.Add("class", itm.Value)
                            itm.Text = clsHTMLHelper.RenderLinkHtml(url, itm.Text, attributes)
                            itm.ImageUrl = Me.Request.Url.Scheme & "://cdn.goomena.com/Images/spacer10.png"
                        End If
                End Select
            Next

            'url = url.TrimEnd("&"c)

            'lnkEnglish.NavigateUrl = url & "lag=US"
            'lnkGerman.NavigateUrl = url & "lag=DE"
            'lnkGreek.NavigateUrl = url & "lag=GR"
            'lnkHungarian.NavigateUrl = url & "lag=HU"
            'cmbLag.DisplayFormatString = "<a href=""/Default.aspx?lag=US"">English</a>"


        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        'Try

        '    'lnkEnglish()
        '    'lnkGerman
        '    'lnkGreek()
        '    'lnkHungarian()

        '    'lnkEnglish.Visible = AppUtils.IsSupportedLAG("US")
        '    'lnkGerman.Visible = AppUtils.IsSupportedLAG("DE")
        '    'lnkGreek.Visible = AppUtils.IsSupportedLAG("GR")
        '    'lnkHungarian.Visible = AppUtils.IsSupportedLAG("HU")

        'Catch ex As Exception
        '    WebErrorMessageBox(Me, ex, "")
        'End Try
    End Sub


    Private __IsLagChanged As Integer = -1
    Public Function VerifyLagProps() As Boolean
        If (Request.Form("__EVENTTARGET") = cmbLag.UniqueID) Then

            __IsLagChanged = IIf((Request.Form("__EVENTTARGET") = cmbLag.UniqueID), 1, 0)
            ModifyApplicationForLanguageChanges()

        ElseIf (Not Page.IsPostBack AndAlso Not String.IsNullOrEmpty(Me.RequestedLAGID)) Then

            __IsLagChanged = IIf((Me.RequestedLAGID <> Session("LAGID")), 1, 0)
            ModifyApplicationForLanguageChanges()

        End If
        Return (__IsLagChanged = 1)
    End Function



    Private Sub LoadPhotos()

        'Dim myConn As SqlConnection
        'Dim AppDBconnectionString As SqlConnection
        'Dim myCmd As SqlCommand
        'Dim myCmd2 As SqlCommand
        Dim photoid As String
        Dim photoname As String
        Dim address As String = Me.Request.Url.Scheme & "://photos.goomena.com/"
        Dim address2 As String
        Dim address3 As String
        Dim i As Integer
        Dim picture As New HtmlGenericControl
        Dim imageforwomen As New HtmlImage
        Dim loginname As HtmlGenericControl
        Dim profilename As String
        Dim memberctr As HyperLink
        Dim checkList As New List(Of Integer)
        Dim pnlBottomPhotos1 As Panel = pnlBottomPhotos
        Dim pnlMosaic1 As Panel = pnlMosaic

        'Dim lgnfrm As HtmlContainerControl
        'If (HttpContext.Current.User.Identity.IsAuthenticated) Then
        '    lgnfrm = Master.FindControl("Content").FindControl("regformcontainer").FindControl("regmidcontainer").FindControl("registerform")
        '    lgnfrm.Style("display") = "none"
        'End If

        Dim latitudeIn As Double? = Nothing
        Dim longitudeIn As Double? = Nothing

        Try

            'If (Session("GEO_COUNTRY_INFO") IsNot Nothing) Then
            '    Dim country As clsCountryByIP = Session("GEO_COUNTRY_INFO")
            Dim country As clsCountryByIP = clsCurrentContext.GetCountryByIP()
            If (country IsNot Nothing) Then

                Try
                    Dim pos As New clsGlobalPositionWCF(country.latitude, country.longitude)
                    latitudeIn = pos.latitude
                    longitudeIn = pos.longitude

                    'If (Not String.IsNullOrEmpty(country.latitude)) Then
                    '    Dim __latitudeIn As Double = 0
                    '    'country.latitude = country.latitude.Replace(".", ",")
                    '    If (Double.TryParse(country.latitude, __latitudeIn)) Then latitudeIn = __latitudeIn
                    '    If (__latitudeIn > 180 OrElse __latitudeIn < -180) Then
                    '        country.latitude = country.latitude.Replace(".", ",")
                    '        If (Double.TryParse(country.latitude, __latitudeIn)) Then latitudeIn = __latitudeIn
                    '    End If
                    'End If
                    'If (Not String.IsNullOrEmpty(country.longitude)) Then
                    '    Dim __longitudeIn As Double = 0
                    '    'country.longitude = country.longitude.Replace(".", ",")
                    '    If (Double.TryParse(country.longitude, __longitudeIn)) Then longitudeIn = __longitudeIn
                    '    If (__longitudeIn > 180 OrElse __longitudeIn < -180) Then
                    '        country.longitude = country.longitude.Replace(".", ",")
                    '        If (Double.TryParse(country.longitude, __longitudeIn)) Then longitudeIn = __longitudeIn
                    '    End If
                    'End If
                Catch ex As Exception
                    WebErrorSendEmail(ex, "Get lat-lon from ip.")
                End Try

            End If
            'End If


            If (Not latitudeIn.HasValue AndAlso Not longitudeIn.HasValue) Then
                Try

                    If (MasterProfileId > 0 AndAlso clsCurrentContext.VerifyLogin()) Then
                        If (SessionVariables.MemberData.latitude.HasValue) Then latitudeIn = SessionVariables.MemberData.latitude
                        If (SessionVariables.MemberData.longitude.HasValue) Then longitudeIn = SessionVariables.MemberData.longitude
                    End If
                Catch ex As Exception
                    WebErrorSendEmail(ex, "Get lat-lon from profile.")
                End Try
            End If

            If (Not latitudeIn.HasValue AndAlso Not longitudeIn.HasValue) Then
                Try

                    Dim pos As clsGlobalPositionWCF = clsGeoHelper.GetCountryMinPostcodeDataTable_Position(MyBase.GetGeoCountry(), Nothing, Nothing)
                    latitudeIn = pos.latitude
                    longitudeIn = pos.longitude

                    'Dim dtGEO As DataTable = clsGeoHelper.GetCountryMinPostcodeDataTable(MyBase.GetGeoCountry(), Nothing, Nothing)

                    'If (dtGEO.Rows.Count > 0 AndAlso Not IsDBNull(dtGEO.Rows(0)("latitude")) AndAlso Not IsDBNull(dtGEO.Rows(0)("longitude"))) Then
                    '    latitudeIn = clsNullable.DBNullToDecimal(dtGEO.Rows(0)("latitude"))
                    '    longitudeIn = clsNullable.DBNullToDecimal(dtGEO.Rows(0)("longitude"))
                    'End If

                Catch ex As Exception
                    WebErrorSendEmail(ex, "Get lat-lon from geo data.")
                End Try
            End If

        Catch ex As Exception
            WebErrorSendEmail(ex, "Get lat-lon info.")
        End Try

        Dim sql As String = "EXEC [CustomerPhotos_Grid2]  @latitudeIn=@latitudeIn, @longitudeIn=@longitudeIn,@Country=@Country"
        Dim cmd As SqlCommand = DataHelpers.GetSqlCommand(sql)
        cmd.Parameters.AddWithValue("@Country", MyBase.GetGeoCountry())
        Dim prm1 As SqlClient.SqlParameter = DataHelpers.GetSqlParam_LatitubeOrLongitude("@latitudeIn", latitudeIn)
        cmd.Parameters.Add(prm1)

        Dim prm2 As SqlClient.SqlParameter = DataHelpers.GetSqlParam_LatitubeOrLongitude("@longitudeIn", longitudeIn)
        cmd.Parameters.Add(prm2)

        'If (Not latitudeIn.HasValue AndAlso Not longitudeIn.HasValue) Then
        '    cmd.Parameters.AddWithValue("@latitudeIn", DBNull.Value)
        '    cmd.Parameters.AddWithValue("@longitudeIn", DBNull.Value)
        'Else
        '    cmd.Parameters.AddWithValue("@latitudeIn", latitudeIn.Value)
        '    cmd.Parameters.AddWithValue("@longitudeIn", longitudeIn.Value)
        'End If

        Dim dt As DataTable = DataHelpers.GetDataTable(cmd)
        dt.Columns.Add("InUse", GetType(Boolean))

        Try

            Dim women As New List(Of Integer)
            Dim men As New List(Of Integer)

            For cnt = 0 To dt.Rows.Count - 1
                Dim dr As DataRow = dt.Rows(cnt)

                Dim CustomerID As Integer = dr("CustomerID")
                If (checkList.Contains(CustomerID)) Then
                    Continue For
                End If
                Dim gender As Integer = dr("GenderId")
                If (gender = 2) Then
                    If (women.Count > 59) Then
                        Continue For
                    Else
                        women.Add(cnt)
                    End If
                ElseIf (gender = 1) Then
                    If (men.Count > 27) Then
                        Continue For
                    Else
                        men.Add(cnt)
                    End If
                End If

                checkList.Add(CustomerID)


                'stis 88 photos exit
                If (checkList.Count > 87) Then Exit For

            Next

            Dim currentPhotosCount As Integer = 0
            i = 0
            For cnt = 0 To checkList.Count - 1
                If (currentPhotosCount > 87) Then Exit For
                Dim drs As DataRow() = dt.Select("CustomerId=" & checkList(cnt) & "and (InUse is null)")
                If (drs.Length = 0) Then Continue For

                i += 1
                picture = pnlMosaic1.FindControl("pic" & i)
                While (i < 87 AndAlso (picture Is Nothing OrElse Not picture.Visible))
                    i += 1
                    picture = pnlMosaic1.FindControl("pic" & i)
                End While
                If (picture Is Nothing) Then Exit For


                Dim dr As DataRow = drs(0)
                dr("InUse") = True
                currentPhotosCount = currentPhotosCount + 1

                photoid = dr("CustomerID").ToString
                photoname = dr("FileName")
                profilename = dr("LoginName")
                Dim country As String = dr("Country")
                Dim region As String = dr("Region")
                Dim birthday As Date = dr("Birthday")
                Dim city As String = dr("City")
                Dim age As Integer = ProfileHelper.GetCurrentAge(birthday)
                Dim gender As Integer = dr("GenderId")
                address2 = address + photoid
                address3 = address2 + "/thumbs/" + photoname
                If Me.IsHTTPS AndAlso address3.StartsWith("http:") Then address3 = "https" & address3.Remove(0, "http".Length)

                picture.Attributes.Add("style", "background-image:url(" + address3 + ")")
                picture.Attributes.Add("data-URL", address3)
                picture.Attributes.Add("data-ID", photoid)
                picture.Attributes.Add("data-LoginName", profilename)
                picture.Attributes.Add("data-Country", dr("CountryName")) 'ProfileHelper.GetCountryName(country))
                picture.Attributes.Add("data-Region", region)
                picture.Attributes.Add("data-Age", age)
                picture.Attributes.Add("data-City", city)
                picture.Attributes.Add("data-Gender", gender)

                If (cnt = checkList.Count - 1 AndAlso currentPhotosCount < 87) Then
                    cnt = 0
                End If
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        Finally
            cmd.Dispose()
        End Try



        checkList.Clear()


        checkList.Clear()
        Dim sql1 As String = "EXEC [CustomerPhotos_FrontPage2]  @latitudeIn=@latitudeIn, @longitudeIn=@longitudeIn,@Country=@Country"
        cmd = DataHelpers.GetSqlCommand(sql1)
        cmd.Parameters.AddWithValue("@Country", MyBase.GetGeoCountry())
        cmd.Parameters.AddWithValue("@latitudeIn", DBNull.Value)
        cmd.Parameters.AddWithValue("@longitudeIn", DBNull.Value)
        dt = DataHelpers.GetDataTable(cmd)
        dt.Columns.Add("InUse", GetType(Boolean))

        Try

            Dim women As New List(Of Integer)
            Dim men As New List(Of Integer)

            For cnt = 0 To dt.Rows.Count - 1
                Dim dr As DataRow = dt.Rows(cnt)

                Dim CustomerID As Integer = dr("CustomerID")
                If (checkList.Contains(CustomerID)) Then
                    Continue For
                End If
                Dim gender As Integer = dr("GenderId")
                If (gender = 2) Then
                    If (women.Count > 3) Then
                        Continue For
                    Else
                        women.Add(CustomerID)
                    End If
                ElseIf (gender = 1) Then
                    If (men.Count > 3) Then
                        Continue For
                    Else
                        men.Add(CustomerID)
                    End If
                End If

                checkList.Add(CustomerID)


                'stis 7 photos exit
                If (checkList.Count > 7) Then Exit For

            Next


            i = 0
            For cnt = 0 To men.Count - 1

                Dim drs As DataRow() = dt.Select("CustomerId=" & men(cnt) & "and (InUse is null)")
                If (drs.Length = 0) Then Continue For

                Dim dr As DataRow = drs(0)
                dr("InUse") = True

                i += 1


                imageforwomen = pnlBottomPhotos1.FindControl("imgtag" & i)
                loginname = pnlBottomPhotos1.FindControl("imagename" & i)
                photoid = dr("CustomerID").ToString
                photoname = dr("FileName")
                profilename = dr("LoginName")
                address2 = address + photoid
                address3 = address2 + "/thumbs/" + photoname
                If Me.IsHTTPS AndAlso address3.StartsWith("http:") Then address3 = "https" & address3.Remove(0, "http".Length)

                imageforwomen.Attributes.Add("src", address3)
                loginname.InnerHtml = profilename.ToString()
                memberctr = pnlBottomPhotos1.FindControl("anchornum" & i)
                If (HttpContext.Current.User.Identity.IsAuthenticated) Then
                    memberctr.NavigateUrl = "Members/Profile.aspx?p=" & HttpUtility.UrlEncode(profilename)
                    memberctr.Attributes.Add("onclick", "ShowLoading();")
                Else
                    'memberctr.NavigateUrl = "profile/" & HttpUtility.UrlEncode(profilename)
                    'memberctr.NavigateUrl = MyBase.LanguageHelper.GetPublicURL("/profile/" & HttpUtility.UrlEncode(profilename), MyBase.GetLag())
                    Dim url As String = UrlUtils.GetValidRoutingProfileURI(HttpUtility.UrlEncode(profilename)) 'HttpUtility.UrlEncode(profilename).Replace("+", " ")
                    url = MyBase.LanguageHelper.GetPublicURL("/profile/" & url, MyBase.GetLag())
                    memberctr.NavigateUrl = url
                    memberctr.Attributes.Add("onclick", "ShowLoading();")
                End If
            Next


            ' i = 0
            For cnt = 0 To women.Count - 1

                Dim drs As DataRow() = dt.Select("CustomerId=" & women(cnt) & "and (InUse is null)")
                If (drs.Length = 0) Then Continue For

                Dim dr As DataRow = drs(0)
                dr("InUse") = True

                i += 1

                imageforwomen = pnlBottomPhotos1.FindControl("imgtag" & i)
                loginname = pnlBottomPhotos1.FindControl("imagename" & i)
                photoid = dr(0).ToString
                photoname = dr(1)
                profilename = dr(2)
                address2 = address + photoid
                address3 = address2 + "/thumbs/" + photoname
                If Me.IsHTTPS AndAlso address3.StartsWith("http:") Then address3 = "https" & address3.Remove(0, "http".Length)

                imageforwomen.Attributes.Add("src", address3)
                loginname.InnerHtml = profilename.ToString()
                memberctr = pnlBottomPhotos1.FindControl("anchornum" & i)
                If (HttpContext.Current.User.Identity.IsAuthenticated) Then
                    memberctr.NavigateUrl = "Members/Profile.aspx?p=" & HttpUtility.UrlEncode(profilename)
                    memberctr.Attributes.Add("onclick", "ShowLoading();")
                Else
                    'memberctr.NavigateUrl = "profile/" & HttpUtility.UrlEncode(profilename)
                    'memberctr.NavigateUrl = MyBase.LanguageHelper.GetPublicURL("/profile/" & HttpUtility.UrlEncode(profilename), MyBase.GetLag())
                    Dim url As String = UrlUtils.GetValidRoutingProfileURI(HttpUtility.UrlEncode(profilename)) 'HttpUtility.UrlEncode(profilename).Replace("+", " ")
                    url = MyBase.LanguageHelper.GetPublicURL("/profile/" & url, MyBase.GetLag())
                    memberctr.NavigateUrl = url
                    memberctr.Attributes.Add("onclick", "ShowLoading();")
                End If

            Next


        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        Finally
            cmd.Dispose()
        End Try

        'Try

        '    i = 0
        '    Dim women As New List(Of Integer)
        '    Dim men As New List(Of Integer)

        '    Do While myReader2.Read()

        '        Dim CustomerID As Integer = myReader2.GetInt32(0)
        '        If (checkList.Contains(CustomerID)) Then
        '            Continue Do
        '        End If

        '        Dim gender As Integer = myReader2("GenderId")
        '        If (gender = 2) Then
        '            If (women.Count > 3) Then
        '                Continue Do
        '            Else
        '                women.Add(i)
        '            End If
        '        ElseIf (gender = 1) Then
        '            If (men.Count > 3) Then
        '                Continue Do
        '            Else
        '                men.Add(i)
        '            End If
        '        End If

        '        checkList.Add(CustomerID)

        '        i = i + 1

        '        'stis 8 photos exit
        '        If (i > 7) Then Exit Do

        '    Loop


        '    imageforwomen = New HtmlImage
        '    imageforwomen = Master.FindControl("Content").FindControl("imgtag" & i)
        '    loginname = New HtmlGenericControl
        '    loginname = Master.FindControl("Content").FindControl("imagename" & i)
        '    photoid = myReader2.GetValue(0).ToString
        '    photoname = myReader2.GetString(1)
        '    profilename = myReader2.GetString(2)
        '    address2 = address + photoid
        '    address3 = address2 + "/thumbs/" + photoname
        '    If Me.IsHTTPS AndAlso address3.StartsWith("http:") Then address3 = "https" & address3.Remove(0, "http".Length)

        '    imageforwomen.Attributes.Add("src", address3)
        '    loginname.InnerHtml = profilename.ToString()
        '    memberctr = Master.FindControl("Content").FindControl("anchornum" & i)
        '    If (HttpContext.Current.User.Identity.IsAuthenticated) Then
        '        memberctr.NavigateUrl = "Members/Profile.aspx?p=" & profilename
        '    Else
        '        memberctr.NavigateUrl = "Register.aspx"
        '    End If


        'Catch ex As Exception
        '    Throw New Exception(ex.Message, ex)
        'Finally

        '    'myReader2.Close()
        '    myConn.Close()
        '    myCmd2.Dispose()

        'End Try

    End Sub





    Private Sub ucLogin1_LoggingInFailed(ByRef sender As Object, ByRef e As LoggingInFailedEventArgs)
        Dim loginctl As Dating.Server.Site.Web.ucLogin = sender
        If (loginctl.LogginFailReason = LogginFailReasonEnum.UserDeleted) Then
            e.Handled = True

            Dim MainLogin As System.Web.UI.WebControls.Login = ucLogin1.LoginControl

            Dim btnLogin As ASPxButton = MainLogin.FindControl("btnLogin")
            If (btnLogin IsNot Nothing) Then btnLogin.Visible = False

            Dim lblUsernameTopTD As HtmlControl = MainLogin.FindControl("lblUsernameTopTD")
            If (lblUsernameTopTD IsNot Nothing) Then lblUsernameTopTD.Visible = False

            Dim lblPasswordTopTD As HtmlControl = MainLogin.FindControl("lblPasswordTopTD")
            If (lblPasswordTopTD IsNot Nothing) Then lblPasswordTopTD.Visible = False

            Dim UserNameTD As HtmlControl = MainLogin.FindControl("UserNameTD")
            If (UserNameTD IsNot Nothing) Then UserNameTD.Visible = False

            Dim PasswordTD As HtmlControl = MainLogin.FindControl("PasswordTD")
            If (PasswordTD IsNot Nothing) Then PasswordTD.Visible = False

            Dim chkRememberMeTD As HtmlControl = MainLogin.FindControl("chkRememberMeTD")
            If (chkRememberMeTD IsNot Nothing) Then chkRememberMeTD.Visible = False

            Dim lbForgotPasswordTD As HtmlControl = MainLogin.FindControl("lbForgotPasswordTD")
            If (lbForgotPasswordTD IsNot Nothing) Then lbForgotPasswordTD.Visible = False

            Dim lblErrorUserDeleted As Label = MainLogin.FindControl("lblErrorUserDeleted")
            If (lblErrorUserDeleted IsNot Nothing) Then
                lblErrorUserDeleted.Text = e.ErrorMessage
                lblErrorUserDeleted.Visible = True
            End If

        End If
    End Sub


End Class



#Region "Comments"
'Private Sub LoadPhotos()

'    'Dim myConn As SqlConnection
'    'Dim AppDBconnectionString As SqlConnection
'    'Dim myCmd As SqlCommand
'    'Dim myCmd2 As SqlCommand
'    Dim photoid As String
'    Dim photoname As String
'    Dim address As String = Me.Request.Url.Scheme & "://photos.goomena.com/"
'    Dim address2 As String
'    Dim address3 As String
'    Dim i As Integer
'    'Dim pic As String
'    Dim picture As New HtmlGenericControl
'    Dim imageforwomen As New HtmlImage
'    Dim loginname As HtmlGenericControl
'    Dim profilename As String
'    Dim memberctr As HyperLink
'    'Dim lgnfrm As HtmlContainerControl
'    Dim checkList As New List(Of Integer)


'    'If (HttpContext.Current.User.Identity.IsAuthenticated) Then
'    '    lgnfrm = me.FindControl("Content").FindControl("regformcontainer").FindControl("regmidcontainer").FindControl("registerform")
'    '    lgnfrm.Style("display") = "none"
'    'End If


'    Dim latitudeIn As Decimal? = Nothing
'    Dim longitudeIn As Decimal? = Nothing
'    Try

'        If (Session("GEO_COUNTRY_INFO") IsNot Nothing) Then
'            Dim country As clsCountryByIP = Session("GEO_COUNTRY_INFO")
'            If (country IsNot Nothing) Then

'                Try
'                    If (Not String.IsNullOrEmpty(country.latitude)) Then
'                        Dim __latitudeIn As Double = 0
'                        'country.latitude = country.latitude.Replace(".", ",")
'                        If (Double.TryParse(country.latitude, __latitudeIn)) Then latitudeIn = __latitudeIn
'                        If (__latitudeIn > 180) Then
'                            country.latitude = country.latitude.Replace(".", ",")
'                            If (Double.TryParse(country.latitude, __latitudeIn)) Then latitudeIn = __latitudeIn
'                        End If
'                    End If
'                    If (Not String.IsNullOrEmpty(country.longitude)) Then
'                        Dim __longitudeIn As Double = 0
'                        'country.longitude = country.longitude.Replace(".", ",")
'                        If (Double.TryParse(country.longitude, __longitudeIn)) Then longitudeIn = __longitudeIn
'                        If (__longitudeIn > 180) Then
'                            country.longitude = country.longitude.Replace(".", ",")
'                            If (Double.TryParse(country.longitude, __longitudeIn)) Then longitudeIn = __longitudeIn
'                        End If
'                    End If
'                Catch ex As Exception
'                    WebErrorSendEmail(ex, "Get lat-lon from ip.")
'                End Try

'            End If
'        End If


'        If (Not latitudeIn.HasValue AndAlso Not longitudeIn.HasValue) Then
'            Try

'                If (MasterProfileId > 0 AndAlso clsCurrentContext.VerifyLogin()) Then
'                    If (SessionVariables.MemberData.latitude.HasValue) Then latitudeIn = SessionVariables.MemberData.latitude
'                    If (SessionVariables.MemberData.longitude.HasValue) Then longitudeIn = SessionVariables.MemberData.longitude
'                End If
'            Catch ex As Exception
'                WebErrorSendEmail(ex, "Get lat-lon from profile.")
'            End Try
'        End If

'        If (Not latitudeIn.HasValue AndAlso Not longitudeIn.HasValue) Then
'            Try
'                Dim dtGEO As DataTable = clsGeoHelper.GetCountryMinPostcodeDataTable(Session("GEO_COUNTRY_CODE"), Nothing, Nothing)

'                If (dtGEO.Rows.Count > 0) Then
'                    latitudeIn = dtGEO.Rows(0)("latitude")
'                    longitudeIn = dtGEO.Rows(0)("longitude")
'                End If

'            Catch ex As Exception
'                WebErrorSendEmail(ex, "Get lat-lon from geo data.")
'            End Try
'        End If

'    Catch ex As Exception
'        WebErrorSendEmail(ex, "Get lat-lon info.")
'    End Try

'    Dim sql As String = "EXEC [CustomerPhotos_Grid2]  @latitudeIn=@latitudeIn, @longitudeIn=@longitudeIn,@Country=@Country"
'    Dim cmd As SqlCommand = DataHelpers.GetSqlCommand(sql)
'    cmd.Parameters.AddWithValue("@Country", MyBase.GetGeoCountry())
'    If (Not latitudeIn.HasValue AndAlso Not longitudeIn.HasValue) Then
'        cmd.Parameters.AddWithValue("@latitudeIn", DBNull.Value)
'        cmd.Parameters.AddWithValue("@longitudeIn", DBNull.Value)
'    Else
'        cmd.Parameters.AddWithValue("@latitudeIn", latitudeIn.Value)
'        cmd.Parameters.AddWithValue("@longitudeIn", longitudeIn.Value)
'    End If
'    Dim dt As DataTable = DataHelpers.GetDataTable(cmd)
'    dt.Columns.Add("InUse", GetType(Boolean))


'    Try

'        Dim women As New List(Of Integer)
'        Dim men As New List(Of Integer)

'        For cnt = 0 To dt.Rows.Count - 1
'            Dim dr As DataRow = dt.Rows(cnt)

'            Dim CustomerID As Integer = dr(0)
'            If (checkList.Contains(CustomerID)) Then
'                Continue For
'            End If
'            Dim gender As Integer = dr("GenderId")
'            If (gender = 2) Then
'                If (women.Count > 59) Then
'                    Continue For
'                Else
'                    women.Add(cnt)
'                End If
'            ElseIf (gender = 1) Then
'                If (men.Count > 27) Then
'                    Continue For
'                Else
'                    men.Add(cnt)
'                End If
'            End If

'            checkList.Add(CustomerID)


'            'stis 88 photos exit
'            If (checkList.Count > 87) Then Exit For

'        Next

'        i = 0
'        For cnt = 0 To checkList.Count - 1
'            Dim dr As DataRow = dt.Select("CustomerId=" & checkList(cnt))(0)
'            i += 1
'            If (i > 87) Then Exit For


'            picture = New HtmlGenericControl
'            picture = Me.FindControl("pnlMosaic").FindControl("pic" & i)
'            While (i < 87 AndAlso (picture Is Nothing OrElse Not picture.Visible))
'                i += 1
'                picture = Me.FindControl("pnlMosaic").FindControl("pic" & i)
'            End While

'            photoid = dr(0).ToString
'            photoname = dr(1)
'            profilename = dr(2)
'            Dim country As String = dr(3)
'            Dim region As String = dr(4)
'            Dim birthday As Date = dr(5)
'            Dim city As String = dr(6)
'            Dim today As Date = Date.Today
'            Dim age As Integer = today.Year - birthday.Year
'            Dim gender As Integer = dr("GenderId")
'            address2 = address + photoid
'            address3 = address2 + "/thumbs/" + photoname
'            If Me.IsHTTPS AndAlso address3.StartsWith("http:") Then address3 = "https" & address3.Remove(0, "http".Length)

'            picture.Attributes.Add("style", "background-image:url(" + address3 + ")")
'            picture.Attributes.Add("data-URL", address3)
'            picture.Attributes.Add("data-ID", photoid)
'            picture.Attributes.Add("data-LoginName", profilename)
'            picture.Attributes.Add("data-Country", ProfileHelper.GetCountryName(country))
'            picture.Attributes.Add("data-Region", region)
'            picture.Attributes.Add("data-Age", age)
'            picture.Attributes.Add("data-City", city)
'            picture.Attributes.Add("data-Gender", gender)

'        Next

'    Catch ex As Exception
'        Throw New Exception(ex.Message, ex)
'    Finally
'        cmd.Dispose()
'    End Try


'    checkList.Clear()



'    checkList.Clear()
'    Dim sql1 As String = "EXEC [CustomerPhotos_FrontPage]  @latitudeIn=@latitudeIn, @longitudeIn=@longitudeIn"
'    cmd = DataHelpers.GetSqlCommand(sql1)
'    cmd.Parameters.AddWithValue("@latitudeIn", DBNull.Value)
'    cmd.Parameters.AddWithValue("@longitudeIn", DBNull.Value)
'    dt = DataHelpers.GetDataTable(cmd)

'    Try

'        Dim women As New List(Of Integer)
'        Dim men As New List(Of Integer)

'        For cnt = 0 To dt.Rows.Count - 1
'            Dim dr As DataRow = dt.Rows(cnt)

'            Dim CustomerID As Integer = dr(0)
'            If (checkList.Contains(CustomerID)) Then
'                Continue For
'            End If
'            Dim gender As Integer = dr("GenderId")
'            If (gender = 2) Then
'                If (women.Count > 3) Then
'                    Continue For
'                Else
'                    women.Add(CustomerID)
'                End If
'            ElseIf (gender = 1) Then
'                If (men.Count > 3) Then
'                    Continue For
'                Else
'                    men.Add(CustomerID)
'                End If
'            End If

'            checkList.Add(CustomerID)


'            'stis 7 photos exit
'            If (checkList.Count > 7) Then Exit For
'        Next


'        i = 0
'        For cnt = 0 To men.Count - 1
'            Dim dr As DataRow = dt.Select("CustomerId=" & men(cnt))(0)
'            i += 1


'            imageforwomen = New HtmlImage
'            imageforwomen = Me.FindControl("pnlBottomPhotos").FindControl("imgtag" & i)
'            loginname = New HtmlGenericControl
'            loginname = Me.FindControl("pnlBottomPhotos").FindControl("imagename" & i)
'            photoid = dr(0).ToString
'            photoname = dr(1)
'            profilename = dr(2)
'            address2 = address + photoid
'            address3 = address2 + "/thumbs/" + photoname
'            If Me.IsHTTPS AndAlso address3.StartsWith("http:") Then address3 = "https" & address3.Remove(0, "http".Length)

'            imageforwomen.Attributes.Add("src", address3)
'            loginname.InnerHtml = profilename.ToString()
'            memberctr = Me.FindControl("pnlBottomPhotos").FindControl("anchornum" & i)
'            If (HttpContext.Current.User.Identity.IsAuthenticated) Then
'                memberctr.NavigateUrl = "Members/Profile.aspx?p=" & HttpUtility.UrlEncode(profilename)
'                memberctr.Attributes.Add("onclick", "ShowLoading();")
'            Else
'                memberctr.NavigateUrl = "Register.aspx"
'                memberctr.Attributes.Add("onclick", "ShowLoading();")
'            End If
'        Next


'        For cnt = 0 To women.Count - 1
'            Dim dr As DataRow = dt.Select("CustomerId=" & women(cnt))(0)
'            i += 1

'            imageforwomen = New HtmlImage
'            imageforwomen = Me.FindControl("pnlBottomPhotos").FindControl("imgtag" & i)
'            loginname = New HtmlGenericControl
'            loginname = Me.FindControl("pnlBottomPhotos").FindControl("imagename" & i)
'            photoid = dr(0).ToString
'            photoname = dr(1)
'            profilename = dr(2)
'            address2 = address + photoid
'            address3 = address2 + "/thumbs/" + photoname
'            If Me.IsHTTPS AndAlso address3.StartsWith("http:") Then address3 = "https" & address3.Remove(0, "http".Length)

'            imageforwomen.Attributes.Add("src", address3)
'            loginname.InnerHtml = profilename.ToString()
'            memberctr = Me.FindControl("pnlBottomPhotos").FindControl("anchornum" & i)
'            If (HttpContext.Current.User.Identity.IsAuthenticated) Then
'                memberctr.NavigateUrl = "Members/Profile.aspx?p=" & HttpUtility.UrlEncode(profilename)
'                memberctr.Attributes.Add("onclick", "ShowLoading();")
'            Else
'                memberctr.NavigateUrl = "Register.aspx"
'                memberctr.Attributes.Add("onclick", "ShowLoading();")
'            End If

'        Next


'    Catch ex As Exception
'        Throw New Exception(ex.Message, ex)
'    Finally
'        cmd.Dispose()
'    End Try



'End Sub

#End Region

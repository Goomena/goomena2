﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="LoginWithGoogle.aspx.vb" Inherits="Dating.Server.Site.Web.LoginWithGoogle" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Button ID="Button1" runat="server" Text="Go to google" OnClientClick="OpenGoogleLoginPopup();return false;"  />
        </div>
    </form>

    <script type="text/javascript">


        function OpenGoogleLoginPopup() {


            var url = "https://accounts.google.com/o/oauth2/auth?";
            url += "scope=https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email&";
            url += "state=%2Fprofile&"
            url += "redirect_uri=<%=Return_url %>&"
             url += "response_type=token&"
             url += "client_id=<%=Client_ID %>";

            window.location = url;
        }
        //OpenGoogleLoginPopup();

    </script>
</body>
</html>

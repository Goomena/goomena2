﻿Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Core.DLL
Imports System.Globalization

Public Class Browse
    Inherits BasePage

    Private Property ItemsPerPage As Integer = 10

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If (Request.QueryString("ps") = "generous") Then
                Response.Redirect(ResolveUrl("~/PubSearch.aspx?show=1"))
            ElseIf (Request.QueryString("ps") = "attractive") Then
                Response.Redirect(ResolveUrl("~/PubSearch.aspx?show=2"))
            Else
                Response.Redirect(ResolveUrl("~/PubSearchSelect.aspx"))
            End If

            AddHandler Me.browseMembers.Repeater.ItemCommand, AddressOf Me.offersSearchRepeater_ItemCommand


            'Try
            '    Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            '    AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
            'Catch ex As Exception
            '    WebErrorMessageBox(Me, ex, "Page_Load")
            'End Try


            If (Not Me.Page.IsPostBack) Then
                Me.SessionVariables.WinkBeforeLogin = Nothing
                Me.SessionVariables.FavoriteBeforeLogin = Nothing
                BindSearchResults()
            End If

            If (Not Me.IsPostBack) Then
                LoadLAG()
            End If
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try
    End Sub



    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
    End Sub


    Protected Sub LoadLAG()
        Try
            lnkJoin.Text = Me.CurrentPageData.GetCustomString("lnkJoin")
            'SetControlsValue(Me, CurrentPageData)

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Protected Sub offersSearchRepeater_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs)
        Try
            If (e.CommandName = OfferControlCommandEnum.WINK.ToString()) Then

                If (Me.MasterProfileId = 0) Then
                    'Me.SessionVariables.WinkBeforeLogin = e.CommandArgument
                    Response.Redirect(Me.ResolveUrl("~/Register.aspx"), False)
                    HttpContext.Current.ApplicationInstance.CompleteRequest()
                End If

            ElseIf (e.CommandName = OfferControlCommandEnum.FAVORITE.ToString()) Then

                If (Me.MasterProfileId = 0) Then
                    'Me.SessionVariables.FavoriteBeforeLogin = e.CommandArgument
                    Response.Redirect(Me.ResolveUrl("~/Register.aspx"), False)
                    HttpContext.Current.ApplicationInstance.CompleteRequest()
                End If

            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    'Private Sub BindSearchResults()
    '    Try

    '        'If (profileRow IsNot Nothing) Then


    '        Dim dc As CMSDBDataContext = Me.CMSDBDataContext

    '        ' bind new offers
    '        Try
    '            Dim dt As DataTable = DataHelpers.GetMembersToSearchDataTable(SearchSortEnum.NewestMember, Me.MasterProfileId)

    '            If (dt.Rows.Count > 0) Then
    '                FillOfferControlList(dt, offersSearch)
    '            End If

    '            offersSearch.DataBind()
    '            SetPager(dt.Rows.Count)

    '        Catch ex As Exception
    '            WebErrorMessageBox(Me, ex, "")

    '            SetPager(0)
    '            offersSearch.UsersList = Nothing
    '            offersSearch.DataBind()
    '        End Try
    '        'End If


    '    Catch ex As Exception
    '        WebErrorMessageBox(Me, ex, "")
    '    End Try

    'End Sub


    Private Sub BindSearchResults()

        ' Dim dc As CMSDBDataContext = Me.CMSDBDataContext

        '' set pagination
        'Dim startingIndex As Integer = 0
        'Dim endIndex As Integer = Me.ItemsPerPage - 1
        'If (Me.Pager.PageIndex > 0) Then
        '    startingIndex = (Me.ItemsPerPage * (Me.Pager.PageIndex))
        '    endIndex = (startingIndex + (Me.ItemsPerPage * (Me.Pager.PageIndex))) - 1
        'End If


        ' bind offersSearch
        Dim dt As New DataTable()
        Dim sql As String = ""
        Dim countRows As Integer

        Try
            Dim prms As New clsSearchHelperParameters()

            Dim NumberOfRecordsToReturn As Integer = Me.GetRecordsToRetrieve()
            'Dim zipStr As String = clsGeoHelper.GetCountryMinPostcode(Session("GEO_COUNTRY_CODE"))
            Dim _dt1 As DataTable = clsGeoHelper.GetCountryMinPostcodeDataTable(Session("GEO_COUNTRY_CODE"), Nothing, Nothing, GetLag())
            Try
                Dim culture As CultureInfo = CultureInfo.CreateSpecificCulture("el-GR")
                If (_dt1.Rows.Count > 0) Then
                    If (Not IsDBNull(_dt1.Rows(0)("minpostcode"))) Then prms.zipstr = _dt1.Rows(0)("minpostcode")
                    If (Not IsDBNull(_dt1.Rows(0)("latitude"))) Then
                        prms.latitudeIn = 0
                        Double.TryParse(_dt1.Rows(0)("latitude").ToString().Replace(".", ","), System.Globalization.NumberStyles.Any, culture, prms.latitudeIn)
                    End If
                    If (Not IsDBNull(_dt1.Rows(0)("longitude"))) Then
                        prms.longitudeIn = 0
                        Double.TryParse(_dt1.Rows(0)("longitude").ToString().Replace(".", ","), System.Globalization.NumberStyles.Any, culture, prms.longitudeIn)
                    End If
                End If
            Catch
            End Try

            prms.ReturnRecordsWithStatus = ProfileStatusEnum.Approved
            prms.performCount = True
            prms.rowNumberMin = NumberOfRecordsToReturn - Me.ItemsPerPage
            prms.rowNumberMax = NumberOfRecordsToReturn



            If (String.IsNullOrEmpty(Request.QueryString("ps")) OrElse Request.QueryString("ps").ToUpper() = "GENEROUS") Then
                prms.GenderId = ProfileHelper.gMaleGender.GenderId
                'ds = clsSearchHelper.GetMembersToSearchUsingGenderDataTable(ProfileStatusEnum.Approved, ProfileHelper.gMaleGender.GenderId, zipStr, Nothing, Nothing, Nothing)
            Else
                prms.GenderId = ProfileHelper.gFemaleGender.GenderId
                '' If (Request.QueryString("ps").ToUpper() = "ATTRACTIVE") Then
                'ds = clsSearchHelper.GetMembersToSearchUsingGenderDataTable(ProfileStatusEnum.Approved, ProfileHelper.gFemaleGender.GenderId, zipStr, Nothing, Nothing, Nothing)
            End If

            Dim ds As DataSet = clsSearchHelper.GetMembersToSearchUsingGenderDataTable(prms)
            countRows = ds.Tables(0).Rows(0)(0)
            If (countRows > 0) Then
                dt = ds.Tables(1)
            End If

            ' If (prms.performCount) Then
            'Else
            '    countRows = ds.Tables(0).Rows.Count
            '    If (countRows > 0) Then
            '        dt = ds.Tables(0)
            '    End If
            'End If

            'Dim rowsCount As Integer = 0
            For Each dr As DataRow In dt.Rows

                ' If (rowsCount >= startingIndex AndAlso rowsCount <= endIndex) Then

                Try

                    Dim uli As New clsWinkUserListItem()
                    uli.LAGID = Session("LAGID")
                    uli.OtherMemberBirthday = If(Not dr.IsNull("Birthday"), dr("Birthday"), Nothing)
                    uli.OtherMemberLoginName = dr("LoginName")
                    uli.OtherMemberProfileID = dr("ProfileID")
                    uli.OtherMemberMirrorProfileID = dr("MirrorProfileID")
                    uli.OtherMemberCity = IIf(Not dr.IsNull("City"), dr("City"), String.Empty)
                    uli.OtherMemberRegion = IIf(Not dr.IsNull("Region"), dr("Region"), String.Empty)
                    uli.OtherMemberGenderid = dr("Genderid")
                    uli.OtherMemberHeading = IIf(Not dr.IsNull("AboutMe_Heading"), dr("AboutMe_Heading"), String.Empty)


                    If (Not dr.IsNull("PersonalInfo_HeightID")) Then
                        uli.OtherMemberHeight = ProfileHelper.GetHeightString(dr("PersonalInfo_HeightID"), MyBase.GetLag())
                    End If


                    If (Not dr.IsNull("PersonalInfo_BodyTypeID")) Then
                        uli.OtherMemberPersonType = ProfileHelper.GetBodyTypeString(dr("PersonalInfo_BodyTypeID"), MyBase.GetLag())
                    End If


                    If (Not dr.IsNull("PersonalInfo_HairColorID")) Then
                        uli.OtherMemberHair = ProfileHelper.GetHairColorString(dr("PersonalInfo_HairColorID"), MyBase.GetLag())
                    End If


                    If (Not dr.IsNull("PersonalInfo_EyeColorID")) Then
                        uli.OtherMemberEyes = ProfileHelper.GetEyeColorString(dr("PersonalInfo_EyeColorID"), MyBase.GetLag())
                    End If


                    If (Not dr.IsNull("PersonalInfo_EthnicityID")) Then
                        uli.OtherMemberEthnicity = ProfileHelper.GetEthnicityString(dr("PersonalInfo_EthnicityID"), MyBase.GetLag())
                    End If


                    If (Not dr.IsNull("Birthday")) Then
                        uli.OtherMemberAge = ProfileHelper.GetCurrentAge(dr("Birthday"))
                    Else
                        uli.OtherMemberAge = "020"
                    End If

                    'If (Not dr.IsNull("FileName")) Then
                    '    uli.OtherMemberImageUrl = ProfileHelper.GetProfileImageURL(dr("CustomerID"), dr("FileName"), uli.OtherMemberGenderid, True, Me.IsHTTPS)
                    '    uli.OtherMemberImageFileName = dr("FileName")
                    '    uli.OtherMemberImageThumbUrl = uli.OtherMemberImageUrl
                    'Else
                    '    uli.OtherMemberImageUrl = ProfileHelper.GetDefaultImageURL(uli.OtherMemberGenderid)
                    '    uli.OtherMemberImageThumbUrl = uli.OtherMemberImageUrl
                    '    uli.OtherMemberImageFileName = System.IO.Path.GetFileName(uli.OtherMemberImageUrl)
                    'End If
                    If (Not dr.IsNull("FileName")) Then uli.OtherMemberImageFileName = dr("FileName")
                    uli.OtherMemberImageUrl = ProfileHelper.GetProfileImageURL(uli.OtherMemberProfileID, uli.OtherMemberImageFileName, uli.OtherMemberGenderid, True, Me.IsHTTPS)
                    uli.OtherMemberImageThumbUrl = uli.OtherMemberImageUrl


                    uli.OtherMemberProfileViewUrl = Me.ResolveUrl("~/Register.aspx") 'ResolveUrl("~/Members/Profile.aspx?p=" & dr("LoginName"))
                    uli.AllowWink = True
                    uli.AllowFavorite = True

                    uli.Distance = dr("distance")

                    If (uli.Distance < gCarDistance) Then
                        uli.DistanceCss = "distance_car"
                    Else
                        uli.DistanceCss = "distance_plane"
                    End If


                    browseMembers.UsersList.Add(uli)

                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "")
                End Try
                ' End If

                'rowsCount += 1
            Next
            ' End If

            browseMembers.DataBind()
            SetPager(countRows)

            'If (dt.Rows.Count > GetItemsPerPageNumber()) Then

            'End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")

            browseMembers.UsersList = Nothing
            browseMembers.DataBind()
            SetPager(0)

        Finally
            dt.Dispose()
        End Try

    End Sub


    Sub SetPager(itemsCount As Integer)
        Me.Pager.ItemCount = itemsCount
        Me.Pager.ItemsPerPage = Me.ItemsPerPage
        Me.Pager.Visible = (itemsCount > Me.ItemsPerPage)

        If (Me.Pager.PageIndex = -1) AndAlso (itemsCount > 0) Then
            Me.Pager.PageIndex = 0
        End If
    End Sub

    Function GetRecordsToRetrieve() As Integer
        Me.Pager.ItemsPerPage = Me.ItemsPerPage

        If (Me.Pager.PageIndex = -1) AndAlso (Me.Pager.ItemCount > 0) Then
            Me.Pager.PageIndex = 0
        End If

        If (Me.Pager.PageIndex > 0) Then
            Return (Me.Pager.ItemsPerPage * (Me.Pager.PageIndex + 1))
        End If
        Return Me.Pager.ItemsPerPage
    End Function


    Protected Sub Pager_PageIndexChanged(sender As Object, e As EventArgs) Handles Pager.PageIndexChanged
        If (Pager.PageIndex < 2) Then
            BindSearchResults()
        Else
            Response.Redirect("Register.aspx", False)
            HttpContext.Current.ApplicationInstance.CompleteRequest()
        End If
    End Sub

    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender

        Try
            Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub


End Class
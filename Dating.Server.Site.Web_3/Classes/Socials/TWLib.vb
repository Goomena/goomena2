﻿Imports System.IO
Imports System.Net
Imports System.Security.Cryptography
Imports System.Collections.Specialized
Imports System.Xml


Public Class TWLib

    Public Class OAuthBase

        ''' <summary>
        ''' Provides a predefined set of algorithms that are supported officially by the protocol
        ''' </summary>
        Public Enum SignatureTypes
            HMACSHA1
            PLAINTEXT
            RSASHA1
        End Enum

        ''' <summary>
        ''' Provides an internal structure to sort the query parameter
        ''' </summary>
        Protected Class QueryParameter
            Private m_name As String = Nothing
            Private m_value As String = Nothing

            Public Sub New(name As String, value As String)
                Me.m_name = name
                Me.m_value = value
            End Sub

            Public ReadOnly Property Name() As String
                Get
                    Return m_name
                End Get
            End Property

            Public ReadOnly Property Value() As String
                Get
                    Return m_value
                End Get
            End Property
        End Class

        ''' <summary>
        ''' Comparer class used to perform the sorting of the query parameters
        ''' </summary>
        Protected Class QueryParameterComparer
            Implements IComparer(Of OAuthBase.QueryParameter)

#Region "IComparer<QueryParameter> Members"

            Public Function Compare(x As OAuthBase.QueryParameter, y As OAuthBase.QueryParameter) As Integer Implements IComparer(Of OAuthBase.QueryParameter).Compare
                If x.Name = y.Name Then
                    Return String.Compare(x.Value, y.Value)
                Else
                    Return String.Compare(x.Name, y.Name)
                End If
            End Function

#End Region
        End Class

        Protected Const OAuthVersion As String = "1.0"
        Protected Const OAuthParameterPrefix As String = "oauth_"

        '
        ' List of know and used oauth parameters' names
        '        
        Protected Const OAuthConsumerKeyKey As String = "oauth_consumer_key"
        Protected Const OAuthCallbackKey As String = "oauth_callback"
        Protected Const OAuthVersionKey As String = "oauth_version"
        Protected Const OAuthSignatureMethodKey As String = "oauth_signature_method"
        Protected Const OAuthSignatureKey As String = "oauth_signature"
        Protected Const OAuthTimestampKey As String = "oauth_timestamp"
        Protected Const OAuthNonceKey As String = "oauth_nonce"
        Protected Const OAuthTokenKey As String = "oauth_token"
        Protected Const OAuthTokenSecretKey As String = "oauth_token_secret"
        Protected Const OAuthVerifierKey As String = "oauth_verifier"

        Protected Const HMACSHA1SignatureType As String = "HMAC-SHA1"
        Protected Const PlainTextSignatureType As String = "PLAINTEXT"
        Protected Const RSASHA1SignatureType As String = "RSA-SHA1"

        Protected random As New Random()

        Protected unreservedChars As String = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_.~"

        ''' <summary>
        ''' Helper function to compute a hash value
        ''' </summary>
        ''' <param name="hashAlgorithm">The hashing algoirhtm used. If that algorithm needs some initialization, like HMAC and its derivatives, they should be initialized prior to passing it to this function</param>
        ''' <param name="data">The data to hash</param>
        ''' <returns>a Base64 string of the hash value</returns>
        Private Function ComputeHash(hashAlgorithm As HashAlgorithm, data As String) As String
            If hashAlgorithm Is Nothing Then
                Throw New ArgumentNullException("hashAlgorithm")
            End If

            If String.IsNullOrEmpty(data) Then
                Throw New ArgumentNullException("data")
            End If

            Dim dataBuffer As Byte() = System.Text.Encoding.ASCII.GetBytes(data)
            Dim hashBytes As Byte() = hashAlgorithm.ComputeHash(dataBuffer)

            Return Convert.ToBase64String(hashBytes)
        End Function

        ''' <summary>
        ''' Internal function to cut out all non oauth query string parameters (all parameters not begining with "oauth_")
        ''' </summary>
        ''' <param name="parameters">The query string part of the Url</param>
        ''' <returns>A list of QueryParameter each containing the parameter name and value</returns>
        Private Function GetQueryParameters(parameters As String) As List(Of QueryParameter)
            If parameters.StartsWith("?") Then
                parameters = parameters.Remove(0, 1)
            End If

            Dim result As New List(Of QueryParameter)()

            If Not String.IsNullOrEmpty(parameters) Then
                Dim p As String() = parameters.Split("&"c)
                For Each s As String In p
                    If Not String.IsNullOrEmpty(s) AndAlso Not s.StartsWith(OAuthParameterPrefix) Then
                        If s.IndexOf("="c) > -1 Then
                            Dim temp As String() = s.Split("="c)
                            result.Add(New QueryParameter(temp(0), temp(1)))
                        Else
                            result.Add(New QueryParameter(s, String.Empty))
                        End If
                    End If
                Next
            End If

            Return result
        End Function

        ''' <summary>
        ''' This is a different Url Encode implementation since the default .NET one outputs the percent encoding in lower case.
        ''' While this is not a problem with the percent encoding spec, it is used in upper case throughout OAuth
        ''' </summary>
        ''' <param name="value">The value to Url encode</param>
        ''' <returns>Returns a Url encoded string</returns>
        Public Function UrlEncode(value As String) As String
            Dim result As New StringBuilder()

            For Each symbol As Char In value
                If unreservedChars.IndexOf(symbol) <> -1 Then
                    result.Append(symbol)
                Else
                    result.Append("%"c & [String].Format("{0:X2}", AscW(symbol)))
                End If
            Next

            Return result.ToString()
        End Function

        ''' <summary>
        ''' Normalizes the request parameters according to the spec
        ''' </summary>
        ''' <param name="parameters">The list of parameters already sorted</param>
        ''' <returns>a string representing the normalized parameters</returns>
        Protected Function NormalizeRequestParameters(parameters As IList(Of QueryParameter)) As String
            Dim sb As New StringBuilder()
            Dim p As QueryParameter = Nothing
            For i As Integer = 0 To parameters.Count - 1
                p = parameters(i)
                sb.AppendFormat("{0}={1}", p.Name, p.Value)

                If i < parameters.Count - 1 Then
                    sb.Append("&")
                End If
            Next

            Return sb.ToString()
        End Function

        ''' <summary>
        ''' Generate the signature base that is used to produce the signature
        ''' </summary>
        ''' <param name="url">The full url that needs to be signed including its non OAuth url parameters</param>
        ''' <param name="consumerKey">The consumer key</param>        
        ''' <param name="token">The token, if available. If not available pass null or an empty string</param>
        ''' <param name="tokenSecret">The token secret, if available. If not available pass null or an empty string</param>
        ''' <param name="callBackUrl">The callback URL (for OAuth 1.0a).If your client cannot accept callbacks, the value MUST be 'oob' </param>
        ''' <param name="oauthVerifier">This value MUST be included when exchanging Request Tokens for Access Tokens. Otherwise pass a null or an empty string</param>
        ''' <param name="httpMethod">The http method used. Must be a valid HTTP method verb (POST,GET,PUT, etc)</param>
        ''' <param name="signatureType">The signature type. To use the default values use <see cref="OAuthBase.SignatureTypes">OAuthBase.SignatureTypes</see>.</param>
        ''' <returns>The signature base</returns>
        Public Function GenerateSignatureBase(url As Uri, consumerKey As String, token As String, tokenSecret As String, callBackUrl As String, oauthVerifier As String, _
         httpMethod As String, timeStamp As String, nonce As String, signatureType As String, ByRef normalizedUrl As String, ByRef normalizedRequestParameters As String) As String
            If token Is Nothing Then
                token = String.Empty
            End If

            If tokenSecret Is Nothing Then
                tokenSecret = String.Empty
            End If

            If String.IsNullOrEmpty(consumerKey) Then
                Throw New ArgumentNullException("TWconsumerKey")
            End If

            If String.IsNullOrEmpty(httpMethod) Then
                Throw New ArgumentNullException("httpMethod")
            End If

            If String.IsNullOrEmpty(signatureType) Then
                Throw New ArgumentNullException("signatureType")
            End If

            normalizedUrl = Nothing
            normalizedRequestParameters = Nothing

            Dim parameters As List(Of QueryParameter) = GetQueryParameters(url.Query)
            parameters.Add(New QueryParameter(OAuthVersionKey, OAuthVersion))
            parameters.Add(New QueryParameter(OAuthNonceKey, nonce))
            parameters.Add(New QueryParameter(OAuthTimestampKey, timeStamp))
            parameters.Add(New QueryParameter(OAuthSignatureMethodKey, signatureType))
            parameters.Add(New QueryParameter(OAuthConsumerKeyKey, consumerKey))

            If Not String.IsNullOrEmpty(callBackUrl) Then
                parameters.Add(New QueryParameter(OAuthCallbackKey, UrlEncode(callBackUrl)))
            End If


            If Not String.IsNullOrEmpty(oauthVerifier) Then
                parameters.Add(New QueryParameter(OAuthVerifierKey, oauthVerifier))
            End If

            If Not String.IsNullOrEmpty(token) Then
                parameters.Add(New QueryParameter(OAuthTokenKey, token))
            End If

            parameters.Sort(New QueryParameterComparer())

            normalizedUrl = String.Format("{0}://{1}", url.Scheme, url.Host)
            If Not ((url.Scheme = "http" AndAlso url.Port = 80) OrElse (url.Scheme = "https" AndAlso url.Port = 443)) Then
                normalizedUrl += ":" & Convert.ToString(url.Port)
            End If
            normalizedUrl += url.AbsolutePath
            normalizedRequestParameters = NormalizeRequestParameters(parameters)

            Dim signatureBase As New StringBuilder()
            signatureBase.AppendFormat("{0}&", httpMethod.ToUpper())
            signatureBase.AppendFormat("{0}&", UrlEncode(normalizedUrl))
            signatureBase.AppendFormat("{0}", UrlEncode(normalizedRequestParameters))

            Return signatureBase.ToString()
        End Function

        ''' <summary>
        ''' Generate the signature value based on the given signature base and hash algorithm
        ''' </summary>
        ''' <param name="signatureBase">The signature based as produced by the GenerateSignatureBase method or by any other means</param>
        ''' <param name="hash">The hash algorithm used to perform the hashing. If the hashing algorithm requires initialization or a key it should be set prior to calling this method</param>
        ''' <returns>A base64 string of the hash value</returns>
        Public Function GenerateSignatureUsingHash(signatureBase As String, hash As HashAlgorithm) As String
            Return ComputeHash(hash, signatureBase)
        End Function

        ''' <summary>
        ''' Generates a signature using the HMAC-SHA1 algorithm
        ''' </summary>		
        ''' <param name="url">The full url that needs to be signed including its non OAuth url parameters</param>
        ''' <param name="consumerKey">The consumer key</param>
        ''' <param name="consumerSecret">The consumer seceret</param>
        ''' <param name="token">The token, if available. If not available pass null or an empty string</param>
        ''' <param name="tokenSecret">The token secret, if available. If not available pass null or an empty string</param>
        ''' <param name="callBackUrl">The callback URL (for OAuth 1.0a).If your client cannot accept callbacks, the value MUST be 'oob' </param>
        ''' <param name="oauthVerifier">This value MUST be included when exchanging Request Tokens for Access Tokens. Otherwise pass a null or an empty string</param>
        ''' <param name="httpMethod">The http method used. Must be a valid HTTP method verb (POST,GET,PUT, etc)</param>
        ''' <returns>A base64 string of the hash value</returns>
        Public Function GenerateSignature(url As Uri, consumerKey As String, consumerSecret As String, token As String, tokenSecret As String, callBackUrl As String, _
         oauthVerifier As String, httpMethod As String, timeStamp As String, nonce As String, ByRef normalizedUrl As String, ByRef normalizedRequestParameters As String) As String
            Return GenerateSignature(url, consumerKey, consumerSecret, token, tokenSecret, callBackUrl, _
             oauthVerifier, httpMethod, timeStamp, nonce, SignatureTypes.HMACSHA1, normalizedUrl, _
             normalizedRequestParameters)
        End Function

        ''' <summary>
        ''' Generates a signature using the specified signatureType 
        ''' </summary>		
        ''' <param name="url">The full url that needs to be signed including its non OAuth url parameters</param>
        ''' <param name="consumerKey">The consumer key</param>
        ''' <param name="consumerSecret">The consumer seceret</param>
        ''' <param name="token">The token, if available. If not available pass null or an empty string</param>
        ''' <param name="tokenSecret">The token secret, if available. If not available pass null or an empty string</param>
        ''' <param name="callBackUrl">The callback URL (for OAuth 1.0a).If your client cannot accept callbacks, the value MUST be 'oob' </param>
        ''' <param name="oauthVerifier">This value MUST be included when exchanging Request Tokens for Access Tokens. Otherwise pass a null or an empty string</param>
        ''' <param name="httpMethod">The http method used. Must be a valid HTTP method verb (POST,GET,PUT, etc)</param>
        ''' <param name="signatureType">The type of signature to use</param>
        ''' <returns>A base64 string of the hash value</returns>
        Public Function GenerateSignature(url As Uri, consumerKey As String, consumerSecret As String, token As String, tokenSecret As String, callBackUrl As String, _
         oauthVerifier As String, httpMethod As String, timeStamp As String, nonce As String, signatureType As SignatureTypes, ByRef normalizedUrl As String, _
         ByRef normalizedRequestParameters As String) As String
            normalizedUrl = Nothing
            normalizedRequestParameters = Nothing

            Select Case signatureType
                Case SignatureTypes.PLAINTEXT
                    Return HttpUtility.UrlEncode(String.Format("{0}&{1}", consumerSecret, tokenSecret))
                Case SignatureTypes.HMACSHA1
                    Dim signatureBase As String = GenerateSignatureBase(url, consumerKey, token, tokenSecret, callBackUrl, oauthVerifier, _
                     httpMethod, timeStamp, nonce, HMACSHA1SignatureType, normalizedUrl, normalizedRequestParameters)

                    Dim hmacsha1 As New System.Security.Cryptography.HMACSHA1()
                    hmacsha1.Key = Encoding.ASCII.GetBytes(String.Format("{0}&{1}", UrlEncode(consumerSecret), If(String.IsNullOrEmpty(tokenSecret), "", UrlEncode(tokenSecret))))

                    Return GenerateSignatureUsingHash(signatureBase, hmacsha1)
                Case SignatureTypes.RSASHA1
                    Throw New NotImplementedException()
                Case Else
                    Throw New ArgumentException("Unknown signature type", "signatureType")
            End Select
        End Function

        ''' <summary>
        ''' Generate the timestamp for the signature        
        ''' </summary>
        ''' <returns></returns>
        Public Overridable Function GenerateTimeStamp() As String
            ' Default implementation of UNIX time of the current UTC time
            Dim ts As TimeSpan = DateTime.UtcNow - New DateTime(1970, 1, 1, 0, 0, 0, _
             0)
            Return Convert.ToInt64(ts.TotalSeconds).ToString()
        End Function

        ''' <summary>
        ''' Generate a nonce
        ''' </summary>
        ''' <returns></returns>
        Public Overridable Function GenerateNonce() As String
            ' Just a simple implementation of a random number between 123400 and 9999999
            Return random.[Next](123400, 9999999).ToString()
        End Function

    End Class



    Public Class oAuthTwitter
        Inherits OAuthBase
        Public Enum Method
            [GET]
            POST
            DELETE
        End Enum
        Public Const REQUEST_TOKEN As String = "http://twitter.com/oauth/request_token"
        Public Const AUTHORIZE As String = "http://twitter.com/oauth/authorize"
        Public Const ACCESS_TOKEN As String = "http://twitter.com/oauth/access_token"

        Private _consumerKey As String = ""
        Private _consumerSecret As String = ""
        Private _token As String = ""
        Private _tokenSecret As String = ""
        Private _callBackUrl As String = "oob"
        Private _oauthVerifier As String = ""


#Region "Properties"
        Public Property ConsumerKey() As String
            Get
                If _consumerKey.Length = 0 Then
                    _consumerKey = ConfigurationManager.AppSettings("TWconsumerKey")
                End If
                Return _consumerKey
            End Get
            Set(value As String)
                _consumerKey = value
            End Set
        End Property

        Public Property ConsumerSecret() As String
            Get
                If _consumerSecret.Length = 0 Then
                    _consumerSecret = ConfigurationManager.AppSettings("TWconsumerSecret")
                End If
                Return _consumerSecret
            End Get
            Set(value As String)
                _consumerSecret = value
            End Set
        End Property

        Public Property Token() As String
            Get
                Return _token
            End Get
            Set(value As String)
                _token = value
            End Set
        End Property
        Public Property TokenSecret() As String
            Get
                Return _tokenSecret
            End Get
            Set(value As String)
                _tokenSecret = value
            End Set
        End Property
        Public Property CallBackUrl() As String
            Get
                Return _callBackUrl
            End Get
            Set(value As String)
                _callBackUrl = value
            End Set
        End Property
        Public Property OAuthVerifier() As String
            Get
                Return _oauthVerifier
            End Get
            Set(value As String)
                _oauthVerifier = value
            End Set
        End Property

#End Region

        ''' <summary>
        ''' Get the link to Twitter's authorization page for this application.
        ''' </summary>
        ''' <returns>The url with a valid request token, or a null string.</returns>
        Public Function AuthorizationLinkGet() As String
            Dim ret As String = Nothing

            Dim response As String = oAuthWebRequest(Method.[GET], REQUEST_TOKEN, [String].Empty)
            If response.Length > 0 Then
                'response contains token and token secret.  We only need the token.
                Dim qs As NameValueCollection = HttpUtility.ParseQueryString(response)

                If qs("oauth_callback_confirmed") IsNot Nothing Then
                    If qs("oauth_callback_confirmed") <> "true" Then
                        Throw New Exception("OAuth callback not confirmed.")
                    End If
                End If

                If qs("oauth_token") IsNot Nothing Then
                    ret = (AUTHORIZE & "?oauth_token=") + qs("oauth_token")
                End If
            End If
            Return ret
        End Function

        ''' <summary>
        ''' Exchange the request token for an access token.
        ''' </summary>
        ''' <param name="authToken">The oauth_token is supplied by Twitter's authorization page following the callback.</param>
        ''' <param name="oauthVerifier">An oauth_verifier parameter is provided to the client either in the pre-configured callback URL</param>
        Public Sub AccessTokenGet(authToken As String, oauthVerifier As String)
            Me.Token = authToken
            Me.OAuthVerifier = oauthVerifier

            Dim response As String = oAuthWebRequest(Method.[GET], ACCESS_TOKEN, [String].Empty)

            If response.Length > 0 Then
                'Store the Token and Token Secret
                Dim qs As NameValueCollection = HttpUtility.ParseQueryString(response)
                If qs("oauth_token") IsNot Nothing Then
                    Me.Token = qs("oauth_token")
                End If
                If qs("oauth_token_secret") IsNot Nothing Then
                    Me.TokenSecret = qs("oauth_token_secret")
                End If
            End If
        End Sub

        ''' <summary>
        ''' Submit a web request using oAuth.
        ''' </summary>
        ''' <param name="method__1">GET or POST</param>
        ''' <param name="url">The full url, including the querystring.</param>
        ''' <param name="postData">Data to post (querystring format)</param>
        ''' <returns>The web server response.</returns>
        Public Function oAuthWebRequest(method__1 As Method, url As String, postData As String) As String
            Dim outUrl As String = ""
            Dim querystring As String = ""
            Dim ret As String = ""


            'Setup postData for signing.
            'Add the postData to the querystring.
            If method__1 = Method.POST OrElse method__1 = Method.DELETE Then
                If postData.Length > 0 Then
                    'Decode the parameters and re-encode using the oAuth UrlEncode method.
                    Dim qs As NameValueCollection = HttpUtility.ParseQueryString(postData)
                    postData = ""
                    For Each key As String In qs.AllKeys
                        If postData.Length > 0 Then
                            postData += "&"
                        End If
                        qs(key) = HttpUtility.UrlDecode(qs(key))
                        qs(key) = Me.UrlEncode(qs(key))

                        postData += (key & "=") + qs(key)
                    Next
                    If url.IndexOf("?") > 0 Then
                        url += "&"
                    Else
                        url += "?"
                    End If
                    url += postData
                End If
            End If

            Dim uri As New Uri(url)

            Dim nonce As String = Me.GenerateNonce()
            Dim timeStamp As String = Me.GenerateTimeStamp()

            'Generate Signature
            Dim sig As String = Me.GenerateSignature(uri, Me.ConsumerKey, Me.ConsumerSecret, Me.Token, Me.TokenSecret, Me.CallBackUrl, _
             Me.OAuthVerifier, method__1.ToString(), timeStamp, nonce, outUrl, querystring)

            querystring += "&oauth_signature=" & Me.UrlEncode(sig)

            'Convert the querystring to postData
            If method__1 = Method.POST OrElse method__1 = Method.DELETE Then
                postData = querystring
                querystring = ""
            End If

            If querystring.Length > 0 Then
                outUrl += "?"
            End If

            ret = WebRequest(method__1, outUrl & querystring, postData)

            Return ret
        End Function

        ''' <summary>
        ''' Web Request Wrapper
        ''' </summary>
        ''' <param name="method__1">Http Method</param>
        ''' <param name="url">Full url to the web resource</param>
        ''' <param name="postData">Data to post in querystring format</param>
        ''' <returns>The web server response.</returns>
        Public Function WebRequest(method__1 As Method, url As String, postData As String) As String
            Dim webRequest__2 As HttpWebRequest = Nothing
            Dim requestWriter As StreamWriter = Nothing
            Dim responseData As String = ""

            webRequest__2 = TryCast(System.Net.WebRequest.Create(url), HttpWebRequest)
            webRequest__2.Method = method__1.ToString()
            webRequest__2.ServicePoint.Expect100Continue = False
            'webRequest.UserAgent  = "Identify your application please.";
            'webRequest.Timeout = 20000;

            If method__1 = Method.POST OrElse method__1 = Method.DELETE Then
                webRequest__2.ContentType = "application/x-www-form-urlencoded"

                'POST the data.
                requestWriter = New StreamWriter(webRequest__2.GetRequestStream())
                Try
                    requestWriter.Write(postData)
                Catch
                    Throw
                Finally
                    requestWriter.Close()
                    requestWriter = Nothing
                End Try
            End If

            responseData = WebResponseGet(webRequest__2)

            webRequest__2 = Nothing

            Return responseData

        End Function

        ''' <summary>
        ''' Process the web response.
        ''' </summary>
        ''' <param name="webRequest">The request object.</param>
        ''' <returns>The response data.</returns>
        Public Function WebResponseGet(webRequest As HttpWebRequest) As String
            Dim responseReader As StreamReader = Nothing
            Dim responseData As String = ""

            Try
                responseReader = New StreamReader(webRequest.GetResponse().GetResponseStream())
                responseData = responseReader.ReadToEnd()
            Catch
                Throw
            Finally
                webRequest.GetResponse().GetResponseStream().Close()
                responseReader.Close()
                responseReader = Nothing
            End Try

            Return responseData
        End Function
    End Class



    <Serializable()>
    Public Class TWUserInfo

        Public Property uid As String

        Public Property name As String

        Public Property username As String

        Public Property location As String

        Public Property email As String

        Public Shared Function GetInfo(twitter_info_xml As String) As TWUserInfo
            Dim tw As New TWUserInfo()

            Dim settings As New XmlReaderSettings()
            Using reader As XmlReader = XmlReader.Create(New System.IO.StringReader(twitter_info_xml), settings)

                While (reader.Read())
                    Select Case reader.NodeType
                        Case XmlNodeType.Element
                            Select Case reader.Name
                                Case "id"
                                    reader.Read()
                                    tw.uid = reader.Value

                                Case "email"
                                    reader.Read()
                                    tw.email = reader.Value

                                Case "name"
                                    reader.Read()
                                    tw.name = reader.Value

                                Case "screen_name"
                                    reader.Read()
                                    tw.username = reader.Value

                                Case "location"
                                    reader.Read()
                                    tw.location = reader.Value

                                    'Case "meeting_sex"
                                    '    If (reader.MoveToAttribute("xsi:nil") AndAlso reader.Read() = True) Then
                                    '        Continue While
                                    '    End If
                                    '    'If (reader.ReadToFollowing("city")) Then
                                    '    '    reader.Read()
                                    '    '    tw.current_location_City = reader.Value
                                    '    'End If
                                    '    'reader.Read()
                                    '    'tw.meeting_sex = reader.Value

                                    'Case "first_name"
                                    '    reader.Read()
                                    '    tw.first_name = reader.Value

                                    'Case "middle_name"
                                    '    reader.Read()
                                    '    tw.middle_name = reader.Value

                                    'Case "last_name"
                                    '    reader.Read()
                                    '    tw.last_name = reader.Value

                                    'Case "name"
                                    '    reader.Read()
                                    '    tw.name = reader.Value

                                    'Case "birthday"
                                    '    reader.Read()
                                    '    tw.birthday = reader.Value

                                    'Case "birthday_date"
                                    '    reader.Read()
                                    '    tw.birthday_date = reader.Value

                                    'Case "current_location"
                                    '    If (reader.MoveToAttribute("xsi:nil") AndAlso reader.Read() = True) Then
                                    '        Continue While
                                    '    End If

                                    '    If (reader.ReadToFollowing("city")) Then
                                    '        reader.Read()
                                    '        tw.current_location_City = reader.Value
                                    '    End If

                                    '    If (reader.ReadToFollowing("state")) Then
                                    '        reader.Read()
                                    '        tw.current_location_State = reader.Value
                                    '    End If

                                    '    If (reader.ReadToFollowing("country")) Then
                                    '        reader.Read()
                                    '        tw.current_location_Country = reader.Value
                                    '    End If

                                    '    If (reader.ReadToFollowing("zip")) Then
                                    '        reader.Read()
                                    '        tw.current_location_Zip = reader.Value
                                    '    End If

                                    '    If (reader.ReadToFollowing("id")) Then
                                    '        reader.Read()
                                    '        tw.current_location_Id = reader.Value
                                    '    End If

                                    '    If (reader.ReadToFollowing("name")) Then
                                    '        reader.Read()
                                    '        tw.current_location_Name = reader.Value
                                    '    End If
                                    'Case "hometown_location"
                                    '    If (reader.MoveToAttribute("xsi:nil") AndAlso reader.Read() = True) Then
                                    '        Continue While
                                    '    End If

                                    '    If (reader.ReadToFollowing("city")) Then
                                    '        reader.Read()
                                    '        tw.hometown_location_City = reader.Value
                                    '    End If

                                    '    If (reader.ReadToFollowing("state")) Then
                                    '        reader.Read()
                                    '        tw.hometown_location_State = reader.Value
                                    '    End If

                                    '    If (reader.ReadToFollowing("country")) Then
                                    '        reader.Read()
                                    '        tw.hometown_location_Country = reader.Value
                                    '    End If

                                    '    If (reader.ReadToFollowing("zip")) Then
                                    '        reader.Read()
                                    '        tw.hometown_location_Zip = reader.Value
                                    '    End If

                                    '    If (reader.ReadToFollowing("id")) Then
                                    '        reader.Read()
                                    '        tw.hometown_location_Id = reader.Value
                                    '    End If

                                    '    If (reader.ReadToFollowing("name")) Then
                                    '        reader.Read()
                                    '        tw.hometown_location_Name = reader.Value
                                    '    End If

                            End Select
                    End Select

                End While

            End Using

            Return tw
        End Function

    End Class

End Class

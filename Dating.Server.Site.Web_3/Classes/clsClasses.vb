﻿Imports Dating.Server.Core.DLL

Public Class clsDataRecordBuyerInfo
    Public PayerID As String
    Public BusinessName As String
    Public FirstName As String
    Public LastName As String
    Public PayerEmail As String
    Public Address As String
    Public ContactPhone As String
    Public ResidenceCountry As String
    Public PayerStatus As String
End Class

Public Class clsDataRecordIPN
    Public PayTransactionID As String
    Public PayProviderID As Long
    Public SalesSiteID As Long
    Public SalesSiteProductID As Long
    Public CustomerID As Long
    Public custopmerIP As String = ""
    Public PaymentDateTime As String ' Gia na min exoume thema me to HASH
    Public TransactionTypeID As Integer '1=Sale,2=Refund,3=Subscription,4=Cancel Sub
    Public PayProviderTransactionID As String
    Public PayProviderAmount As Integer ' Gia na min exoume thema me to HASH
    Public PromoCode As String
    Public SaleDescription As String
    Public SaleQuantity As Integer
    Public CustomReferrer As String
    Public BuyerInfo As clsDataRecordBuyerInfo

    Public VerifyHASH As String

End Class

Public Class clsDataRecordIPNReturn

    Public HasErrors As Boolean
    Public ErrorCode As Integer
    Public Message As String
    Public ExtraMessage As String

End Class
Public Class clsDataRecordJetCustomerDataReturn

    Public HasErrors As Boolean
    Public ErrorCode As Integer
    Public hosterName As String
    Public downloadedSoFar As Long
    Public DailyLimit As Long
    Public visualStatus As String
    Public status As Boolean
    Public ExtraNotes As String
    Public imagePath As String
    Public progress As Integer
    Public emptyText As List(Of String)
End Class

Public Class clsDataRecordJetCustomerDataListReturn
    Public Property RapidHosters As New List(Of clsDataRecordJetCustomerDataReturn)

End Class




Public Class clsSessionVariables

    Public Property DataRecordLoginMemberReturn As New clsDataRecordLoginMemberReturn()
    Public Property myTheme As String = ""
    Public Property myCulture As String = "US"
    Public Property Page As String = ""
    Public Property LoginName As String = ""
    'Public Property Referrer As String = ""
    'Public Property CustomReferrer As String = ""
    'Public Property IP As String = ""
    'Public Property Agent As String = ""
    'Public Property StatCookie As String = ""
    'Public Property StaticalCustomAction As String = ""
    'Public Property GEO_COUNTRY_CODE As String = ""
    'Public Property LagID As String
    'Public Property GlobalStrings As clsPageData

    Public Property WinkBeforeLogin As String
    Public Property FavoriteBeforeLogin As String

    Public Property UserUnlockInfo As New clsUserUnlockInfo()


    Public Shared Function GetCurrent() As clsSessionVariables
        Dim _sessVars As clsSessionVariables
        _sessVars = HttpContext.Current.Session("SessionVariables")
        Return _sessVars
    End Function

End Class


Public Class clsUserUnlockInfo
    Public Property OtherLoginName As String
    Public Property OfferId As Integer
    Public Property ReturnUrl As String
    Public Property IsUnlocked As Boolean

    Public Sub Reset()
        OtherLoginName = Nothing
        OfferId = 0
        ReturnUrl = Nothing
        IsUnlocked = False
    End Sub

    Public Function IsValid() As Boolean
        Return (OfferId > 0)
    End Function
End Class
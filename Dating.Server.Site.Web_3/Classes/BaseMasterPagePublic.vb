﻿Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Core.DLL

Public Class BaseMasterPagePublic
    Inherits BaseMasterPage


    Dim _RequestedLAGID As String
    Protected ReadOnly Property RequestedLAGID As String
        Get
            If (_RequestedLAGID Is Nothing) Then
                _RequestedLAGID = ""

                Dim currentUrl As String = Request.Url.ToString()
                Dim requestedLag As String = clsLanguageHelper.GetLAGFromURL(currentUrl)

                If (String.IsNullOrEmpty(requestedLag) AndAlso clsLanguageHelper.IsSupportedLAG(Request.QueryString("LAG"), Session("GEO_COUNTRY_CODE"))) Then
                    requestedLag = Request.QueryString("LAG")
                End If
                If (Not String.IsNullOrEmpty(requestedLag)) Then
                    _RequestedLAGID = requestedLag
                End If

                'If (Not String.IsNullOrEmpty(Request.QueryString("lag"))) Then
                '    _RequestedLAGID = Request.QueryString("lag")
                '    If (clsLanguage.IsSupportedLAG(_RequestedLAGID)) Then
                '        _RequestedLAGID = _RequestedLAGID.ToUpper()
                '    Else
                '        _RequestedLAGID = ""
                '    End If
                'End If
            End If
            Return _RequestedLAGID
        End Get
    End Property


    Dim _mineMasterProfile As EUS_Profile
    Protected Function GetCurrentMasterProfile() As EUS_Profile
        Try

            If (_mineMasterProfile Is Nothing) AndAlso Me.MasterProfileId > 0 Then
                _mineMasterProfile = Me.CMSDBDataContext.EUS_Profiles.Where(Function(itm) (itm.ProfileID = Me.MirrorProfileId OrElse itm.ProfileID = Me.MasterProfileId) AndAlso itm.IsMaster = True).SingleOrDefault()
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Return _mineMasterProfile
    End Function


    Dim _mineMirrorProfile As EUS_Profile
    Protected Function GetCurrentMirrorProfile() As EUS_Profile
        Try

            If (_mineMirrorProfile Is Nothing) AndAlso Me.MirrorProfileId > 0 Then
                _mineMirrorProfile = Me.CMSDBDataContext.EUS_Profiles.Where(Function(itm) (itm.ProfileID = Me.MirrorProfileId OrElse itm.ProfileID = Me.MasterProfileId) AndAlso itm.IsMaster = False).SingleOrDefault()
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Return _mineMirrorProfile
    End Function




    Dim _currentProfile As EUS_Profile
    Protected Function GetCurrentProfile(Optional ByVal CheckOnly As Boolean = False) As EUS_Profile
        Try

            If (_currentProfile Is Nothing) Then
                _currentProfile = Me.GetCurrentMasterProfile()
                If (_currentProfile Is Nothing) Then
                    _currentProfile = Me.GetCurrentMirrorProfile()
                End If

                ' not approved member
                If (_currentProfile Is Nothing) Then
                    _currentProfile = Me.CMSDBDataContext.EUS_Profiles.Where(Function(itm) itm.ProfileID = Me.MasterProfileId AndAlso itm.IsMaster = False).SingleOrDefault()
                End If
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        'If (_currentProfile Is Nothing AndAlso Not CheckOnly) Then Throw New ProfileNotFoundException()
        Return _currentProfile
    End Function


  
End Class

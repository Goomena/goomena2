﻿'Imports Dating.Server.Site.Web.Security
Imports Dating.Server.Core.DLL
Imports Dating.Server.Datasets.DLL

Public Class clsWebStatistics

    Public Property SessionID As String
    Public Property Referrer As String
    Public Property CustomReferrer As String
    Public Property IP As String
    Public Property Agent As String
    Public Property Page As String
    Public Property LoginName As String
    Public Property ProfileID As Integer
    Public Property StatCookie As String
    Public Property CustomAction As String
    Public Property GEO_COUNTRY_CODE As String
    Public Property RequestedUrl As String
    Private Property __startTime As DateTime


    Public Sub New()
        __startTime = DateTime.UtcNow
    End Sub


    Public Function AddNew() As Boolean
        Return AddNew(SessionID, Referrer, CustomReferrer, IP, Agent, Page, LoginName, StatCookie, CustomAction, GEO_COUNTRY_CODE, RequestedUrl, ProfileID)
    End Function


    Public Function AddNew(ByVal _SessionID As String, _
        ByVal _Referrer As String, _
        ByVal _CustomReferrer As String, _
        ByVal _IP As String, _
        ByVal _Agent As String, _
        ByVal _Page As String, _
        ByVal _LoginName As String, _
        ByVal _StatCookie As String, _
        ByVal _CustomAction As String, _
        ByVal _GEO_COUNTRY_CODE As String, _
        ByVal _RequestedUrl As String, _
        ByVal _ProfileID As Integer) As Boolean
        ', _
        '        ByVal MemberType As String
        Try
            ' min kratas statical for local
            '    If IP = "127.0.0.1" Then Return True

            Dim ta As New DSWebStatisticsTableAdapters.SYS_WebStatisticsTableAdapter
            ta.Connection = New Data.SqlClient.SqlConnection(ModGlobals.ConnectionString)
            Dim ds As New DSWebStatistics.SYS_WebStatisticsDataTable
            Dim row As DSWebStatistics.SYS_WebStatisticsRow
            row = ds.NewSYS_WebStatisticsRow
            row.DateTime = Date.UtcNow
            row.SessionID = CheckString(_SessionID, 100, "")
            row.Referrer = CheckString(_Referrer, 200, "")
            row.CustomReferrer = CheckString(_CustomReferrer, 100, "")
            row.IP = CheckString(_IP, 30, "")
            row.Agent = CheckString(_Agent, 1024, "")
            row.Page = CheckString(_Page, 100, "")
            row.LoginName = CheckString(_LoginName, 30, "")
            row.CustomAction = CheckString(_CustomAction, 100, "")
            row.StatCookie = CheckString(_StatCookie, 100, "")
            row.GEO_COUNTRY_CODE = CheckString(_GEO_COUNTRY_CODE, 2, "NN")
            Dim SearchEngineKeywords As String = GetKeywords(_Referrer)
            row.SearchEngineKeywords = CheckString(SearchEngineKeywords, 100, "")
            row.RequestedUrl = CheckString(_RequestedUrl, 3000, "")
            row.LoadTimeMSec = (DateTime.UtcNow - __startTime).TotalMilliseconds
            If (_ProfileID > 0) Then row.ProfileID = _ProfileID
            ds.AddSYS_WebStatisticsRow(row)

            ta.Update(ds)

            Return True
        Catch ex As Exception
            WebErrorMessageBox("clsWebStatistics->AddNew", ex, "")
            Return False
        End Try
    End Function

    Private Function CheckString(ByVal szString As String, ByVal iMax As Integer, ByVal DefaultString As String) As String
        Try
            If Len(szString) > 0 Then
                Return Mid(szString, 1, iMax)
            Else
                Return DefaultString
            End If
        Catch ex As Exception
            WebErrorMessageBox("clsWebStatistics->CheckString", ex, "")
        End Try

        Return Nothing
    End Function

    Public Function GetReferrer(httpReferrer As String, host As String) As String

        Dim referrer As String = ""
        Try
            If (Not String.IsNullOrEmpty(httpReferrer)) Then
                Dim u As New Uri(httpReferrer)
                referrer = httpReferrer
                If (u.Host = host) Then
                    referrer = ""
                End If
            End If
        Catch ex As Exception

        End Try

        Return referrer
    End Function

    'Public Function AddNew(ByVal mBasePage As BasePage, ByVal SessionID, ByVal Referrer, ByVal CustomReferrer, ByVal IP, ByVal Agency, ByVal Page, ByVal LoginName, ByVal CustomAction, ByVal SpyCokkie)
    Public Function AddNew(ByRef mPage As System.Web.HttpApplication)
        Try

            mPage.Application.Lock()

            Dim sessVars As clsSessionVariables = DirectCast(mPage.Session("SessionVariables"), clsSessionVariables)

            Dim refferer As String = GetReferrer(mPage.Request("HTTP_REFERER"), mPage.Request.Url.Host)

            AddNew(
             mPage.Session.SessionID,
             refferer,
             mPage.Session("CustomReferrer"),
             mPage.Session("IP"),
             mPage.Session("Agent"),
             sessVars.Page,
             sessVars.IdentityLoginName,
             mPage.Session("StatCookie"),
             mPage.Session("StaticalCustomAction"),
             mPage.Session("GEO_COUNTRY_CODE"),
             mPage.Request.Url.ToString(),
             mPage.Session("ProfileID"))
            mPage.Application.UnLock()
        Catch ex As Exception
            WebErrorMessageBox("clsWebStatistics->AddNew", ex, "")
            mPage.Application.UnLock()
        End Try

        Return Nothing
    End Function

    Public Function AddNew(ByVal mPage As BasePage)
        Try
            mPage.Application.Lock()

            Dim sessVars As clsSessionVariables = DirectCast(mPage.Session("SessionVariables"), clsSessionVariables)

            'Dim refferer As String = mPage.Request("HTTP_REFERER")
            'If (Not String.IsNullOrEmpty(refferer) AndAlso refferer.Contains(mPage.Request.Url.Host)) Then
            '    refferer = ""
            'End If
            Dim refferer As String = GetReferrer(mPage.Request("HTTP_REFERER"), mPage.Request.Url.Host)

            AddNew(
                     mPage.Session.SessionID,
                     refferer,
                     mPage.Session("CustomReferrer"),
                     mPage.Session("IP"),
                     mPage.Session("Agent"),
                     sessVars.Page,
                     HttpContext.Current.User.Identity.Name,
                     mPage.Session("StatCookie"),
                     mPage.Session("StaticalCustomAction"),
                     mPage.Session("GEO_COUNTRY_CODE"),
                     mPage.Request.Url.ToString(),
                     mPage.Session("ProfileID"))

            mPage.Application.UnLock()
        Catch ex As Exception
            WebErrorMessageBox("clsWebStatistics->AddNew2", ex, "")
            mPage.Application.UnLock()
        End Try

        Return Nothing
    End Function

    'Public Function AddNew(ByVal mPage As BasePageMobileAdmin)
    '    Try
    '        mPage.Application.Lock()

    '        Dim sessVars As clsSessionVariables = DirectCast(mPage.Session("SessionVariables"), clsSessionVariables)

    '        'Dim refferer As String = mPage.Request("HTTP_REFERER")
    '        'If (Not String.IsNullOrEmpty(refferer) AndAlso refferer.Contains(mPage.Request.Url.Host)) Then
    '        '    refferer = ""
    '        'End If

    '        Dim refferer As String = GetReferrer(mPage.Request("HTTP_REFERER"), mPage.Request.Url.Host)

    '        AddNew(
    '         mPage.Session.SessionID,
    '         refferer,
    '         mPage.Session("CustomReferrer"),
    '         mPage.Session("IP"),
    '         mPage.Session("Agent"),
    '         sessVars.Page,
    '         HttpContext.Current.User.Identity.Name,
    '         mPage.Session("StatCookie"),
    '         mPage.Session("StaticalCustomAction"),
    '         mPage.Session("GEO_COUNTRY_CODE"),
    '         mPage.Request.Url.ToString(),
    '         mPage.Session("ProfileID"))

    '        mPage.Application.UnLock()
    '    Catch ex As Exception
    '        WebErrorMessageBox("clsWebStatistics->AddNew2", ex, "")
    '        mPage.Application.UnLock()
    '    End Try

    '    Return Nothing
    'End Function

    Public Shared Function GetKeywords(ByVal urlReferrer As String) As String
        If String.IsNullOrEmpty(urlReferrer) Then Return ""

        Try
            'Dim searchQuery = String.Empty
            'Dim query = HttpUtility.ParseQueryString(urlReferrer)
            'Dim url = New Uri(urlReferrer)
            'Dim hostName As String = url.Host

            Dim searchQuery = String.Empty
            Dim url = New Uri(urlReferrer)
            Dim hostName As String = url.Host
            Dim query = HttpUtility.ParseQueryString(url.Query)

            Try
                hostName = hostName.ToLower().Replace("www.", "")
                If (hostName.IndexOf("."c) > -1) Then hostName = hostName.Remove(hostName.IndexOf("."c))
            Catch ex As Exception
            End Try

            Select Case hostName
                Case "google", "daum", "msn", "bing", "ask", "altavista", _
                 "alltheweb", "live", "najdi", "aol", "seznam", "search", _
                 "szukacz", "pchome", "kvasir", "sesam", "ozu", "mynet", _
                 "ekolay"
                    searchQuery = query("q")
                    Exit Select
                Case "naver", "netscape", "mama", "mamma", "terra", "cnn"
                    searchQuery = query("query")
                    Exit Select
                Case "virgilio", "alice"
                    searchQuery = query("qs")
                    Exit Select
                Case "yahoo"
                    searchQuery = query("p")
                    Exit Select
                Case "onet"
                    searchQuery = query("qt")
                    Exit Select
                Case "eniro"
                    searchQuery = query("search_word")
                    Exit Select
                Case "about"
                    searchQuery = query("terms")
                    Exit Select
                Case "voila"
                    searchQuery = query("rdata")
                    Exit Select
                Case "baidu"
                    searchQuery = query("wd")
                    Exit Select
                Case "yandex"
                    searchQuery = query("text")
                    Exit Select
                Case "szukaj"
                    searchQuery = query("wp")
                    Exit Select
                Case "yam"
                    searchQuery = query("k")
                    Exit Select
                Case "rambler"
                    searchQuery = query("words")
                    Exit Select
                Case Else
                    searchQuery = query("q")
                    Exit Select
            End Select

            If Not String.IsNullOrEmpty(searchQuery) Then
                searchQuery = searchQuery.Replace("+", " ")
            End If

            Return searchQuery
        Catch ex As Exception
            WebErrorMessageBox("clsWebStatistics->GetKeywords", ex, "")
            Return ""
        End Try
    End Function

End Class

﻿Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Core.DLL

Public Class BaseUserControl
    Inherits System.Web.UI.UserControl
    Implements ILanguageDependableContent

    Public ReadOnly Property IsPublicRootMaster As Boolean
        Get
            Return (Me.Page.Master IsNot Nothing AndAlso Me.Page.Master.GetType().Name = "root_master")
        End Get
    End Property


    Public ReadOnly Property IsPublicRootMaster2 As Boolean
        Get
            Return (Me.Page.Master IsNot Nothing AndAlso Me.Page.Master.GetType().Name = "root2_master")
        End Get
    End Property



    Private _IsMale As Boolean?
    Public ReadOnly Property IsMale As Boolean
        Get
            If (Not _IsMale.HasValue) Then
                '_IsMale = (Me.GetCurrentProfile().GenderId = ProfileHelper.gMaleGender.GenderId)
                _IsMale = (Me.SessionVariables.MemberData.GenderId = ProfileHelper.gMaleGender.GenderId)
            End If
            Return _IsMale
        End Get
    End Property


    Private _IsFemale As Boolean?
    Public ReadOnly Property IsFemale As Boolean
        Get
            If (Not _IsFemale.HasValue) Then
                '_IsFemale = (Me.GetCurrentProfile().GenderId = ProfileHelper.gFemaleGender.GenderId)
                _IsFemale = (Me.SessionVariables.MemberData.GenderId = ProfileHelper.gFemaleGender.GenderId)
            End If
            Return _IsFemale
        End Get
    End Property


    Private __isReferrer As Boolean?
    Protected ReadOnly Property IsReferrer As Boolean
        Get
            If (__isReferrer Is Nothing) Then
                __isReferrer = (Me.GetCurrentProfile().ReferrerParentId.HasValue AndAlso Me.GetCurrentProfile().ReferrerParentId > 0)
            End If
            Return __isReferrer
        End Get
    End Property


    Public ReadOnly Property IsHTTPS As Boolean
        Get
            Return (Me.Request.Url.Scheme = "https")
        End Get
    End Property

    Protected ReadOnly Property MasterProfileId As Integer
        Get
            Return Me.Session("ProfileID")
        End Get
    End Property


    Protected ReadOnly Property LagID As String
        Get
            Return Me.Session("LagID")
        End Get
    End Property


    Protected ReadOnly Property MirrorProfileId As Integer
        Get
            Return Me.Session("MirrorProfileID")
        End Get
    End Property

    Private __Language As clsLanguageHelper
    Protected ReadOnly Property LanguageHelper As clsLanguageHelper
        Get
            If (__Language Is Nothing) Then __Language = New clsLanguageHelper(Context)
            Return __Language
        End Get
    End Property

    Private __ProfileCountry As SYS_CountriesGEO
    Protected ReadOnly Property ProfileCountry As SYS_CountriesGEO
        Get
            If (__ProfileCountry Is Nothing) Then
                __ProfileCountry = SYS_CountriesGEOHelper.SYS_CountriesGEO_GetForCountry(Me.CMSDBDataContext, Me.SessionVariables.MemberData.Country)
            End If
            Return __ProfileCountry
        End Get
    End Property

    Protected ReadOnly Property ProfileLoginName As String
        Get
            Return Me.SessionVariables.MemberData.LoginName
        End Get
    End Property

    Dim _sessVars As clsSessionVariables
    Protected ReadOnly Property SessionVariables As clsSessionVariables
        Get
            If (_sessVars Is Nothing) Then _sessVars = clsSessionVariables.GetCurrent()
            Return _sessVars
        End Get
    End Property


    Private _globalStrings As clsPageData
    Public ReadOnly Property globalStrings As clsPageData
        Get
            If (_globalStrings Is Nothing) Then
                _globalStrings = New clsPageData("GlobalStrings", HttpContext.Current)
                AddHandler _globalStrings.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _globalStrings
        End Get
    End Property

    Protected _pageData As clsPageData
    Protected ReadOnly Property CurrentPageData As clsPageData
        Get
            ' let it read custom strings from container page
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData(Context, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property



    Dim _CMSDBDataContext As CMSDBDataContext
    Protected ReadOnly Property CMSDBDataContext As CMSDBDataContext
        Get
            Try
                If _CMSDBDataContext IsNot Nothing Then
                    ' exception thrown when disposed
                    Dim a As System.Data.Common.DbConnection = _CMSDBDataContext.Connection
                End If
            Catch ex As Exception
                _CMSDBDataContext = Nothing
            End Try

            If (_CMSDBDataContext Is Nothing) Then 'OrElse _DataContext.Connection.State = ConnectionState.Broken OrElse _DataContext.Connection.State = ConnectionState.Closed) Then
                _CMSDBDataContext = New CMSDBDataContext(ModGlobals.ConnectionString)
            End If
            Return _CMSDBDataContext
        End Get
    End Property



    Dim _mineMasterProfile As vw_EusProfile_Light
    Protected Function GetCurrentMasterProfile() As vw_EusProfile_Light
        Try

            If (_mineMasterProfile Is Nothing) Then
                _mineMasterProfile = DataHelpers.GetCurrentMasterProfile(Me.CMSDBDataContext, Me.MasterProfileId, Me.MirrorProfileId)
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Return _mineMasterProfile
    End Function


    Dim _mineMirrorProfile As vw_EusProfile_Light
    Protected Function GetCurrentMirrorProfile() As vw_EusProfile_Light
        Try

            If (_mineMirrorProfile Is Nothing) Then
                _mineMirrorProfile = DataHelpers.GetCurrentMirrorProfile(Me.CMSDBDataContext, Me.MasterProfileId, Me.MirrorProfileId)
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Return _mineMirrorProfile
    End Function


    Dim _currentProfile As vw_EusProfile_Light
    Protected Function GetCurrentProfile(Optional ByVal CheckOnly As Boolean = False) As vw_EusProfile_Light
        Try

            If (_currentProfile Is Nothing) Then
                _currentProfile = Me.GetCurrentMasterProfile()
                If (_currentProfile Is Nothing) Then
                    _currentProfile = Me.GetCurrentMirrorProfile()
                End If

                ' not approved member
                If (_currentProfile Is Nothing) Then
                    _currentProfile = Me.CMSDBDataContext.vw_EusProfile_Lights.Where(Function(itm) itm.ProfileID = Me.MasterProfileId AndAlso itm.IsMaster = False).SingleOrDefault()
                End If
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        If (_currentProfile Is Nothing AndAlso Not CheckOnly) Then Throw New ProfileNotFoundException()
        Return _currentProfile
    End Function

    Public Function GetCurrency() As String
        Dim currency As String = Nothing
        If (Me.MasterProfileId > 1 AndAlso Me.GetCurrentProfile(True) IsNot Nothing) Then
            currency = clsPricing.GetCurrency(Me.GetCurrentProfile().Country)
        Else
            currency = clsPricing.GetCurrency(HttpContext.Current.Session("GEO_COUNTRY_CODE"))
        End If
        Return currency
    End Function

    Public Overridable Sub Master_LanguageChanged() Implements ILanguageDependableContent.Master_LanguageChanged

    End Sub


    Protected Overridable Sub Page_Unload(sender As Object, e As System.EventArgs) Handles Me.Unload
        If (Not _CMSDBDataContext Is Nothing) Then
            _CMSDBDataContext.Dispose()
        End If
    End Sub



    Public Function GetLag() As String
        Dim str As String = Nothing
        Try
            If (clsLanguageHelper.IsSupportedLAG(HttpContext.Current.Session("LAGID"), Session("GEO_COUNTRY_CODE"))) Then
                str = HttpContext.Current.Session("LAGID")
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        If (str Is Nothing) Then
            If Request.Url.Host.EndsWith(".gr", StringComparison.OrdinalIgnoreCase) Then
                'goomena.gr
                str = gDomainGR_DefaultLAG
            Else

                If Request.Url.Host.EndsWith(".com", StringComparison.OrdinalIgnoreCase) Then
                    'goomena.com
                    If (str Is Nothing) Then str = gDomainCOM_DefaultLAG
                End If

            End If
        End If

        If (str Is Nothing) Then str = gDomainCOM_DefaultLAG

        Return str
    End Function





    Public Function GetHasCommunication(OtherProfileID As Integer) As Boolean
        Dim _HasCommunication As Boolean = False
        If (OtherProfileID = 1 OrElse Me.IsFemale) Then
            _HasCommunication = True
        Else
            _HasCommunication = clsUserDoes.HasCommunication(OtherProfileID, Me.MasterProfileId)
        End If
        Return _HasCommunication
    End Function



    Private _UnlimitedMsgID As Integer?
    Public ReadOnly Property UnlimitedMsgID As Integer
        Get
            If (Not _UnlimitedMsgID.HasValue) Then
                _UnlimitedMsgID = 0
                If (Not String.IsNullOrEmpty(Request.QueryString("unmsg"))) Then
                    Long.TryParse(Request.QueryString("unmsg"), _UnlimitedMsgID)
                End If
            End If
            Return _UnlimitedMsgID
        End Get
    End Property


    Public Shared Function OnMoreInfoClickFunc(input As String, contentUrl As String, headerText As String) As String
        input = WhatIsIt.OnMoreInfoClickFunc(input, contentUrl, headerText)
        Return input
    End Function




    Private _CustomerAvailableCredits As clsCustomerAvailableCredits
    Protected Function HasRequiredCredits(type As UnlockType) As Boolean
        Dim success = False

        If (_CustomerAvailableCredits Is Nothing) Then
            _CustomerAvailableCredits = BasePage.GetCustomerAvailableCredits(Me.MasterProfileId)
        End If

        Try
            Dim creditsRequired As Integer = 10000000

            If (type = UnlockType.UNLOCK_CONVERSATION) Then
                creditsRequired = ProfileHelper.Config_UNLOCK_CONVERSATION_CREDITS

            ElseIf (type = UnlockType.UNLOCK_MESSAGE_READ) Then
                creditsRequired = ProfileHelper.Config_UNLOCK_MESSAGE_READ_CREDITS

            ElseIf (type = UnlockType.UNLOCK_MESSAGE_SEND) Then
                creditsRequired = ProfileHelper.Config_UNLOCK_MESSAGE_SEND_CREDITS
            End If

            If (_CustomerAvailableCredits.AvailableCredits >= creditsRequired) Then success = True
        Catch ex As Exception

        End Try


        Return success
    End Function



    Protected ReadOnly Property BlockMessagesToBeReceivedByOther As Boolean
        Get
            If (ViewState("IsEnabled_BlockMessagesToBeReceivedByOther") Is Nothing) Then
                ViewState("IsEnabled_BlockMessagesToBeReceivedByOther") = clsProfilesPrivacySettings.GET_AdminSetting_BlockMessagesToBeReceivedByOther(Me.MasterProfileId)
            End If
            Return ViewState("IsEnabled_BlockMessagesToBeReceivedByOther")
        End Get
    End Property


    Public Sub Page_CustomStringRetrievalComplete(ByRef sender As clsPageData, ByRef Args As CustomStringRetrievalCompleteEventArgs)
        Dim __pageData As clsPageData = sender
        Dim result As String = Args.CustomString

        If (HttpContext.Current.Request.Url.Scheme.ToUpper() = "HTTPS") Then
            If (result.Contains("http://www.goomena.")) Then
                result = result.Replace("http://www.goomena.", "https://www.goomena.")
                Args.CustomString = result
            End If
            If (result.Contains("http://cdn.goomena.com")) Then
                result = result.Replace("http://cdn.goomena.com", "https://cdn.goomena.com")
                Args.CustomString = result
            End If
        End If

        If (Args.LagIDparam <> "US") Then
            Dim ancMatch As Match = clsWebFormHelper.AnchorRegex.Match(result, 0)
            If (ancMatch.Success) Then
                Args.CustomString = LanguageHelper.FixHTMLBodyAnchors(result, GetLag(), Page.RouteData)
            End If
        End If
    End Sub


End Class

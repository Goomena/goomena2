﻿Imports Dating.Server.Core.DLL
Imports System.Threading

Public Class PageOnUnloadTask
    Public Property NotificationType As NotificationType
    Public Property FromProfileID As Integer
    Public Property ToProfileID As Integer
End Class



Public Class PageOnUnloadTasksList
    Private _list As List(Of PageOnUnloadTask)

    Public Sub Add(itm As PageOnUnloadTask)
        If (_list Is Nothing) Then _list = New List(Of PageOnUnloadTask)
        _list.Add(itm)
    End Sub


    Public Sub RunTasks()
        If (_list IsNot Nothing) Then
            For cnt = 0 To _list.Count - 1
                Dim itm As PageOnUnloadTask = _list(cnt)
                If (itm.NotificationType > 0) Then
                    clsUserNotifications.SendEmailNotification(itm.NotificationType, itm.FromProfileID, itm.ToProfileID)
                End If
            Next
        End If
    End Sub

End Class



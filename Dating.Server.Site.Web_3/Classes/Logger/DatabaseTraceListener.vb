﻿Imports System.Data.SqlClient

'Imports System
'Imports System.Data
'Imports System.Configuration
'Imports System.Web
'Imports System.Web.Security
'Imports System.Web.UI
'Imports System.Web.UI.WebControls
'Imports System.Web.UI.WebControls.WebParts
'Imports System.Web.UI.HtmlControls
'Imports System.Diagnostics
'Imports System.Data.SqlClient
'Imports System.Collections.Specialized

Public Class DatabaseTraceListener
    Inherits TraceListener

    Private Const COLUMN_SEPARATOR As String = "|"
    Private m_strConnectionString As String
    Private m_iMaximumRequests As Integer
    Private m_objCollection As StringCollection

    Public Sub New()
        InitializeListener()
    End Sub

    Public Sub New(r_strListenerName As String)
        MyBase.New(r_strListenerName)
        InitializeListener()
    End Sub

    Private Sub InitializeListener()
        m_strConnectionString = ConfigurationManager.ConnectionStrings("AppDBconnectionString").ConnectionString
        m_iMaximumRequests = Convert.ToInt32(ConfigurationManager.AppSettings("TRACE_MaximumRequests"))
        m_objCollection = New StringCollection()
    End Sub

    Private Sub SaveErrors()
        Dim objConnection As New SqlConnection(m_strConnectionString)
        Dim objCommand As New SqlCommand()
        Try
            objCommand.Connection = objConnection
            objCommand.CommandText = "usp_AddTrace"
            objCommand.CommandType = CommandType.StoredProcedure
            objConnection.Open()

            For Each m_strError As String In m_objCollection
                CreateParameters(objCommand, m_strError)
                objCommand.ExecuteNonQuery()
            Next
            m_objCollection.Clear()

        Catch e As Exception
        Finally
            If objConnection IsNot Nothing Then
                If objConnection.State = ConnectionState.Open Then
                    objConnection.Close()
                End If
            End If
            objConnection = Nothing
            objCommand = Nothing
        End Try
    End Sub

    Private Sub AddToCollection(r_strTraceDateTime As String, r_strTraceCategory As String, r_strTraceDescription As String, r_strStackTrace As String, r_strDetailedErrorDescription As String)
        Dim strError As String = r_strTraceDateTime & COLUMN_SEPARATOR & r_strTraceCategory & COLUMN_SEPARATOR & r_strTraceDescription & COLUMN_SEPARATOR & r_strStackTrace & COLUMN_SEPARATOR & r_strDetailedErrorDescription
        m_objCollection.Add(strError)
        If m_objCollection.Count = m_iMaximumRequests Then
            SaveErrors()
        End If
    End Sub

    Private Sub CreateParameters(r_objCommand As SqlCommand, r_strError As String)
        If (r_objCommand IsNot Nothing) AndAlso (Not r_strError.Equals("")) Then
            Dim strColumns As String()
            Dim objParameters As SqlParameterCollection = r_objCommand.Parameters

            strColumns = r_strError.Split(COLUMN_SEPARATOR.ToCharArray())
            objParameters.Clear()

            objParameters.Add(New SqlParameter("@r_dtTraceDateTime", SqlDbType.DateTime))
            objParameters.Add(New SqlParameter("@r_vcTraceCategory", SqlDbType.VarChar, 50))
            objParameters.Add(New SqlParameter("@r_vcTraceDescription", SqlDbType.VarChar, 1024))
            objParameters.Add(New SqlParameter("@r_vcStackTrace", SqlDbType.VarChar, 2048))
            objParameters.Add(New SqlParameter("@r_vcDetailedErrorDescription", SqlDbType.VarChar, 2048))

            Dim iCount As Integer = strColumns.GetLength(0)
            For i As Integer = 0 To iCount - 1
                objParameters(i).IsNullable = True
                objParameters(i).Direction = ParameterDirection.Input
                objParameters(i).Value = strColumns.GetValue(i).ToString().Trim()
            Next
        End If
    End Sub


    Public Overrides Sub Write(message As String)
        Dim objTrace As New StackTrace(True)
        AddToCollection(DateTime.Now.ToString(), "", message, objTrace.ToString(), "")
    End Sub

    Public Overrides Sub Write(o As Object)
        Dim objTrace As New StackTrace(True)
        AddToCollection(DateTime.Now.ToString(), "", o.ToString(), objTrace.ToString(), "")
    End Sub

    Public Overrides Sub Write(message As String, category As String)
        Dim objTrace As New StackTrace(True)
        AddToCollection(DateTime.Now.ToString(), category, message, objTrace.ToString(), "")
    End Sub

    Public Overrides Sub Write(o As Object, category As String)
        Dim objTrace As New StackTrace(True)
        AddToCollection(DateTime.Now.ToString(), category, o.ToString(), objTrace.ToString(), "")
    End Sub


    Public Overrides Sub WriteLine(message As String)
        Write(message & vbLf)
    End Sub

    Public Overrides Sub WriteLine(o As Object)
        Write(o.ToString() & vbLf)
    End Sub

    Public Overrides Sub WriteLine(message As String, category As String)
        Write((message & vbLf), category)
    End Sub

    Public Overrides Sub WriteLine(o As Object, category As String)
        Write((o.ToString() & vbLf), category)
    End Sub


    Public Overrides Sub Fail(message As String)
        Dim objTrace As New StackTrace(True)
        AddToCollection(DateTime.Now.ToString(), "Fail", message, objTrace.ToString(), "")
    End Sub

    Public Overrides Sub Fail(message As String, detailMessage As String)
        Dim objTrace As New StackTrace(True)
        AddToCollection(DateTime.Now.ToString(), "Fail", message, objTrace.ToString(), detailMessage)
    End Sub

    Public Overrides Sub Close()
        SaveErrors()
    End Sub

    Public Overrides Sub Flush()
        SaveErrors()
    End Sub

End Class

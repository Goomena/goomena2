﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Members/Members2Inner.master" CodeBehind="Photos.aspx.vb" Inherits="Dating.Server.Site.Web.Photos" %>

<%@ Register Assembly="DevExpress.Web.v13.1, Version=13.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxUploadControl" TagPrefix="dx" %>

<%@ Register src="../UserControls/MemberLeftPanel.ascx" tagname="MemberLeftPanel" tagprefix="uc3" %>
<%--<%@ Register src="../UserControls/PhotosEditIfr.ascx" tagname="PhotosEditIfr" tagprefix="uc2" %>--%>
<%@ Register src="../UserControls/PhotosEdit.ascx" tagname="PhotosEdit" tagprefix="uc2" %>
<%@ Register src="../UserControls/WhatIsIt.ascx" tagname="WhatIsIt" tagprefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<%--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
	<script type="text/javascript">
	    !window.jQuery && document.write('<script src="../Scripts/jquery-1.4.3.min.js"><\/script>');
	</script>
	<script type="text/javascript" src="../Scripts/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
	<script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
	<link rel="stylesheet" type="text/css" href="../Scripts/fancybox/jquery.fancybox-1.3.4.css" media="screen" />--%>
	<script type="text/javascript" src="//cdn.goomena.com/Scripts/fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>
	<script type="text/javascript" src="//cdn.goomena.com/Scripts/fancybox/source/jquery.fancybox.pack.js?v=2.1.4"></script>
	<script type="text/javascript" src="//cdn.goomena.com/Scripts/fancybox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
	<script type="text/javascript" src="//cdn.goomena.com/Scripts/fancybox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
	<script type="text/javascript" src="//cdn.goomena.com/Scripts/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.5"></script>
	<link rel="stylesheet" type="text/css" href="//cdn.goomena.com/Scripts/fancybox/source/jquery.fancybox.css?v=2.1.4" media="screen" />
	<link rel="stylesheet" type="text/css" href="//cdn.goomena.com/Scripts/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" />
	<link rel="stylesheet" type="text/css" href="//cdn.goomena.com/Scripts/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />
    <style type="text/css">
        .uploadCtlText { width: 80px; }
        .dxucEditArea{/* devexpress css class hack*/width:100px;}
    </style>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
<div id="ph-edit">
    <div class="m_wrap" id="divNoPhoto" runat="server" visible="false" enableviewstate="false">
	    <div class="items_none items_hard">
            <asp:Literal ID="msg_HasNoPhotosText" runat="server"/>
	    </div>
    </div>
    <h3 id="h3Dashboard" class="page-title">
        <asp:Literal ID="lblItemsName" runat="server" />
        <asp:LinkButton ID="lnkViewDescription" runat="server" CssClass="more_info2" OnClientClick="OnMoreInfoClick('[contentUrl]', this, '[headerText]');return false;" />
    </h3>
	<uc2:PhotosEdit ID="PhotosEdit1" runat="server" Visible="true" />
	<%--<uc2:PhotosEditIfr ID="PhotosEdit2" runat="server" Visible="false" />--%>
	<div class="clear"></div>

<%--
        <div class="clear"></div>
        <div class="container_12" id="tabs-outter">
            <div class="grid_12 top_tabs">
            </div>
        </div>
        <div class="clear"></div>
		<div class="container_12" id="content_top">


		</div>
		<div class="container_12" id="content">
        <div class="grid_3 top_sidebar" id="sidebar-outter">
            <div class="left_tab" id="left_tab" runat="server">
                <div class="top_info">
                </div>
                <div class="middle" id="search">
                    <div class="submit">
                        <h3><asp:Literal ID="lblItemsName" runat="server" /></h3>
                        <div class="clear"></div>
                        <asp:LinkButton ID="lnkViewDescription" runat="server" CssClass="more_info2" OnClientClick="OnMoreInfoClick('[contentUrl]', this, '[headerText]');return false;" />
                    </div>
                </div>
                <div class="bottom">
                    &nbsp;</div></div>
		    </div>
            <div class="grid_9 body">
                <div class="middle" id="content-outter">
                </div>
                <div class="bottom"></div>
		    </div>
		    <div class="clear"></div>
	    </div>

--%>
        <input id="up1Refresh" type="submit" value="submit"  style="display:none;"/>



    <script type="text/javascript">
// <![CDATA[
        function loadFancybox() {
            $("a[rel=public]").fancybox({
                'transitionIn': 'elastic',
                'transitionOut': 'elastic',
                'titlePosition': 'over',
                'overlayColor' : '#000'
            });
            $("a[rel=prive]").fancybox({
                'transitionIn': 'elastic',
                'transitionOut': 'elastic',
                'titlePosition': 'over',
                'overlayColor': '#000'
            });
        }


        function pageLoad(sender, args) {
            HideLoading();
            $('.progress-wrap', '.mp_info').hide();
            loadFancybox();
            if (typeof runPhotosCollapsible === 'function') runPhotosCollapsible();
            if (typeof CallBackPanelModifyControls === 'function') CallBackPanelModifyControls();
        }

        var fileNumber = 0;
        var fileName = "";
        var startDate = null;
        function Uploader_OnFileUploadStart(s, e) {
            startDate = new Date();
            btnUpload.SetEnabled(false);
            ClearProgressInfo();
            $('.progress-wrap', '.mp_info').show();
            ShowLoading();
        }
        function Uploader_OnFileUploadComplete(s, e) {
            $('.progress-wrap', '.mp_info').hide();
            HideLoading();
            if (e.errorText && $.trim(e.errorText).length > 0) {
                s.UpdateErrorMessageCell(0, e.errorText, false);
            }
            else {
                // refresh update panel-old method
                var o = document.getElementById("up1Refresh");
                if (o) {
                    o.click();
                    ShowLoading();
                }
            }
        }
        function Uploader_OnFilesUploadComplete(s, e) {
            UpdateUploadButton(s, e);
            $('.progress-wrap', '.mp_info').hide();
            HideLoading();
        }
        function UpdateUploadButton(s, e) {
            btnUpload.SetEnabled(uploader.GetText(0) != "");
        }
        function getPreviewImageElement() {
            return document.getElementById("previewImage");
        }

        function ProgressChanged(s, args) {
            if (args.currentFileName != fileName) {
                fileName = args.currentFileName;
                fileNumber++;
            }
            progress2.SetPosition(args.progress);
//UpdateProgressStatus(args.uploadedContentLength, args.totalContentLength);
        }
        function ClearProgressInfo() {
            progress2.SetPosition(0);
            fileNumber = 0;
            fileName = "";
        }
//function UpdateProgressStatus(uploadedLength, totalLength) {
//    var currentDate = new Date();
//    var elapsedDateMilliseconds = currentDate - startDate;
//    var speed = uploadedLength / (elapsedDateMilliseconds / 1000);
//    var elapsedDate = new Date(elapsedDateMilliseconds);
//    var elapsedTime = GetTimeString(elapsedDate);
//    var estimatedMilliseconds = Math.floor((totalLength - uploadedLength) / speed) * 1000;
//    var estimatedDate = new Date(estimatedMilliseconds);
//    var estimatedTime = GetTimeString(estimatedDate);
//    lblProgressStatus.SetText('Elapsed time: ' + elapsedTime + ' &ensp; Estimated time: ' + estimatedTime + ' &ensp; Speed: ' + GetContentLengthString(speed) + '/s');
//}
//function GetContentLengthString(contentLength) {
//    var sizeDimensions = ['bytes', 'KB', 'MB', 'GB', 'TB'];
//    var index = 0;
//    var length = contentLength;
//    var postfix = sizeDimensions[index];
//    while (length > 1024) {
//        length = length / 1024;
//        postfix = sizeDimensions[++index];
//    }
//    var numberRegExpPattern = /[-+]?[0-9]*(?:\.|\,)[0-9]{0,2}|[0-9]{0,2}/;
//    var results = numberRegExpPattern.exec(length);
//    length = results ? results[0] : Math.floor(length);
//    return length.toString() + ' ' + postfix;
//}
//function GetTimeString(date) {
//    var timeRegExpPattern = /\d{1,2}:\d{1,2}:\d{1,2}/;
//    var results = timeRegExpPattern.exec(date.toUTCString());
//    return results ? results[0] : "00:00:00";
//}
// ]]> 
        $('.progress-wrap', '.mp_info').hide();
    </script>

     <!--[if IE]>
    <script type="text/javascript">
    $(".ImageUpload").removeAttr("multiple");
    </script>
    <script src="../Scripts/jquery.MultiFile.js" type="text/javascript"></script>
    <![endif]-->

    <script type="text/javascript">
        if (navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1) {
            $(".ImageUpload").removeAttr("multiple");
            $.getScript("../Scripts/jquery.MultiFile.js");
        }
    </script>

	<uc2:whatisit ID="WhatIsIt1" runat="server" />
    
</div>
</asp:Content>


﻿Imports Dating.Server.Core.DLL
Imports Dating.Server.Datasets.DLL

Public Class AutoNotificationsSettings
    Inherits BasePage


    'Dim _pageData As clsPageData
    'Protected ReadOnly Property CurrentPageData As clsPageData
    '    Get
    '        If (_pageData Is Nothing) Then
    '            _pageData = New clsPageData(Context)
    '            AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
    '        End If
    '        Return _pageData
    '    End Get
    'End Property


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            'Try
            '    Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            '    AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
            'Catch ex As Exception
            '    WebErrorMessageBox(Me, ex, "Page_Load")
            'End Try

            If (Not Me.IsPostBack) Then
                LoadLAG()
                LoadSettings()
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub


    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender

        Try
            Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub


    Public Overrides Sub Master_LanguageChanged()

        If (Me.Visible) Then
            Me._pageData = Nothing
            LoadLAG()
        End If

    End Sub


    Protected Sub LoadLAG()
        Try
            Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            SetControlsValue(Me, CurrentPageData)

            btnSave.Text = CurrentPageData.GetCustomString("btnSave")
            LoadAdvahcedSearch()

            msg_FromWord2.Text = msg_FromWord.Text
            msg_ToWord2.Text = msg_ToWord.Text
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub



    Private Sub LoadAdvahcedSearch()
        'If (cbCountry.Items.Count = 0) Then

        'Web.ClsCombos.FillCombo(gLAG.ConnectionString, "SYS_CountriesGEO", "PrintableName", "Iso", cbCountry, True, False, "PrintableName", "ASC")
        Web.ClsCombos.FillComboUsingDatatable(SYS_CountriesGEOHelper.gDSListsGEO__IsEnabled_PrintableNameASC.SYS_CountriesGEO, "PrintableName", "Iso", cbCountry, True, "PrintableName")
        Dim def As DevExpress.Web.ASPxEditors.ListEditItem = Nothing 'cbCountry.Items.FindByValue(Session("GEO_COUNTRY_CODE"))
        If (def IsNot Nothing) Then
            def.Selected = True
        Else
            def = New DevExpress.Web.ASPxEditors.ListEditItem(CurrentPageData.GetCustomString("cbCountry_default"), "-1")
            def.Selected = True
            cbCountry.Items.Insert(0, def)
        End If


        Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_Height, Me.Session("LagID"), "HeightId", cbHeightMin, True, "US")
        cbHeightMin.Items(0).Selected = True
        'cbHeightMin.Items(1).Selected = True

        Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_Height, Me.Session("LagID"), "HeightId", cbHeightMax, True, "US")
        cbHeightMax.Items(0).Selected = True
        'cbHeightMax.Items(cbHeightMax.Items.Count - 1).Selected = True

        Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_BodyType, Me.Session("LagID"), "BodyTypeId", cbBodyType, True, False, "US")
        cbBodyType.Items.RemoveAt(0)
        'SelectAllCheckBoxes(cbBodyType, True)

        Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_Ethnicity, Me.Session("LagID"), "EthnicityId", cbEthnicity, True, False, "US")
        cbEthnicity.Items.RemoveAt(0)
        'SelectAllCheckBoxes(cbEthnicity, True)

        Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_EyeColor, Me.Session("LagID"), "EyeColorId", cbEyeColor, True, False, "US")
        cbEyeColor.Items.RemoveAt(0)
        'SelectAllCheckBoxes(cbEyeColor, True)

        Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_HairColor, Me.Session("LagID"), "HairColorId", cbHairColor, True, False, "US")
        cbHairColor.Items.RemoveAt(0)
        'If (Me.IsMale) Then
        '    ' remove shaven value
        '    Dim li As ListItem = cbHairColor.Items.FindByValue("21")
        '    cbHairColor.Items.Remove(li)
        'End If
        'SelectAllCheckBoxes(cbHairColor, True)

        Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_RelationshipStatus, Me.Session("LagID"), "RelationshipStatusId", cbRelationshipStatus, True, False, "US")
        cbRelationshipStatus.Items.RemoveAt(0)
        'SelectAllCheckBoxes(cbRelationshipStatus, True)

        Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_ChildrenNumber, Me.Session("LagID"), "ChildrenNumberId", cbChildren, True, False, "US")
        cbChildren.Items.RemoveAt(0)
        'SelectAllCheckBoxes(cbChildren, True)

        'Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_TypeOfDating, Me.Session("LagID"), "TypeOfDatingId", cbDatingType, True)
        'SelectAllCheckBoxes(cbDatingType, True)

        'Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_Education, Me.Session("LagID"), "EducationId", cbEducation, True)
        'cbEducation.Items.RemoveAt(0)
        'SelectAllCheckBoxes(cbEducation, True)

        'Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_Income, Me.Session("LagID"), "IncomeId", cbIncome, True)
        'cbIncome.Items.RemoveAt(0)
        'cbIncome.Items.RemoveAt(0)
        'SelectAllCheckBoxes(cbIncome, True)

        'Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_NetWorth, Me.Session("LagID"), "NetWorthId", cbNetWorth, True)
        'cbNetWorth.Items.RemoveAt(0)
        'cbNetWorth.Items.RemoveAt(0)
        'SelectAllCheckBoxes(cbNetWorth, True)

        Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_Religion, Me.Session("LagID"), "ReligionId", cbReligion, True, False, "US")
        cbReligion.Items.RemoveAt(0)
        'SelectAllCheckBoxes(cbReligion, True)

        Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_Smoking, Me.Session("LagID"), "SmokingId", cbSmoking, True, False, "US")
        cbSmoking.Items.RemoveAt(0)
        'SelectAllCheckBoxes(cbSmoking, True)


        Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_Drinking, Me.Session("LagID"), "DrinkingId", cbDrinking, True, False, "US")
        cbDrinking.Items.RemoveAt(0)
        'SelectAllCheckBoxes(cbDrinking, True)

        'Web.ClsCombos.FillComboUsingDatatable(Lists.gDSLists.EUS_LISTS_Gender, Me.Session("LagID"), "GenderId", cbLooking, True)
        'SelectAllCheckBoxes(cbLooking, True)


        ''cbLooking.Items.add(New ListItem(
        'If (Not Me.GetCurrentProfile().LookingFor_ToMeetFemaleID) Then
        '    cbLooking.Items.Remove(cbLooking.Items.FindByValue(ProfileHelper.gFemaleGender.GenderId))
        'End If
        'If (Not Me.GetCurrentProfile().LookingFor_ToMeetMaleID) Then
        '    cbLooking.Items.Remove(cbLooking.Items.FindByValue(ProfileHelper.gMaleGender.GenderId))
        'End If
        ''End If


        'If (cbCountry.SelectedItem IsNot Nothing) Then
        '    FillRegionCombo(cbCountry.SelectedItem.Value)
        '    ClsCombos.SelectComboItem(cbRegion, cbRegion.Text)

        '    If (cbRegion.SelectedItem IsNot Nothing) Then
        '        FillCityCombo(cbRegion.SelectedItem.Value)
        '        ClsCombos.SelectComboItem(cbCity, cbCity.Text)
        '    End If
        'End If


        'If (hdfLocationStatus.Value = "zip") Then
        '    liLocationsRegion.Attributes.CssStyle.Add("display", "none")
        '    liLocationsCity.Attributes.CssStyle.Add("display", "none")
        '    msg_SelectRegion.Attributes.CssStyle.Remove("display")
        'End If

        'If (cbRegion.Items.Count = 0) Then cbRegion.Text = CurrentPageData.GetCustomString("msg_NotFound")
        'If (cbCity.Items.Count = 0) Then cbCity.Text = CurrentPageData.GetCustomString("msg_NotFound")

    End Sub


    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        SaveSettings()
        If (Not String.IsNullOrEmpty(Request.QueryString("popup"))) Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, GetType(String), "closeWin", "top.window[""" & Request.QueryString("popup") & """].DoHideWindow(0);", True)
        End If
    End Sub

    Private Sub SaveSettings()
        Try

            Dim settings As EUS_AutoNotificationSetting = (From itm In Me.CMSDBDataContext.EUS_AutoNotificationSettings
                                                          Where itm.CustomerID = Me.MasterProfileId
                                                          Select itm).FirstOrDefault()

            If (settings Is Nothing) Then
                settings = New EUS_AutoNotificationSetting
                settings.CustomerID = Me.MasterProfileId

                If (ProfileHelper.IsMale(Me.SessionVariables.MemberData.GenderId)) Then
                    settings.crGenderId = ProfileHelper.gFemaleGender.GenderId
                Else
                    settings.crGenderId = ProfileHelper.gMaleGender.GenderId
                End If

                Me.CMSDBDataContext.EUS_AutoNotificationSettings.InsertOnSubmit(settings)
            End If


            settings.crHeightIdMin = -1
            settings.crHeightIdMax = -1

            If (cbHeightMin.SelectedIndex > 0 AndAlso cbHeightMax.SelectedIndex > 0) Then
                Integer.TryParse(cbHeightMin.SelectedItem.Value, settings.crHeightIdMin)
                Integer.TryParse(cbHeightMax.SelectedItem.Value, settings.crHeightIdMax)

            ElseIf (cbHeightMin.SelectedIndex > 0) Then
                Integer.TryParse(cbHeightMin.SelectedItem.Value, settings.crHeightIdMin)

            ElseIf (cbHeightMax.SelectedIndex > 0) Then
                Integer.TryParse(cbHeightMax.SelectedItem.Value, settings.crHeightIdMax)

            End If

            Dim cbHeights As String = ""
            For Each itm As Dating.Server.Datasets.DLL.DSLists.EUS_LISTS_HeightRow In Lists.gDSLists.EUS_LISTS_Height.Rows
                If (itm.HeightId >= settings.crHeightIdMin AndAlso itm.HeightId <= settings.crHeightIdMax) Then
                    cbHeights = cbHeights & itm.HeightId & ","
                End If
            Next
            If (cbHeights <> "") Then
                cbHeights = cbHeights.TrimEnd(",")
                settings.crHeightId = cbHeights
            ElseIf (settings.crHeightIdMin > -1) Then
                settings.crHeightId = settings.crHeightIdMin
            ElseIf (settings.crHeightIdMax > -1) Then
                settings.crHeightId = settings.crHeightIdMax
            Else
                settings.crHeightId = Nothing
            End If

            settings.crBodyTypeId = GetListSubquery(cbBodyType)
            settings.crEthnicityId = GetListSubquery(cbEthnicity)
            settings.crEyeColorId = GetListSubquery(cbEyeColor)
            settings.crHairColorId = GetListSubquery(cbHairColor)


            settings.crAgeMin = -1
            settings.crAgeMax = -1
            If (ageMin2.SelectedValue <> "-1" AndAlso ageMax2.SelectedValue <> "-1") Then
                Integer.TryParse(ageMin2.SelectedValue, settings.crAgeMin)
                Integer.TryParse(ageMax2.SelectedValue, settings.crAgeMax)

            ElseIf (ageMin2.SelectedValue <> "-1") Then
                Integer.TryParse(ageMin2.SelectedValue, settings.crAgeMin)

            ElseIf (ageMax2.SelectedValue <> "-1") Then
                Integer.TryParse(ageMax2.SelectedValue, settings.crAgeMax)

            End If

            settings.crRelationshipStatusId = GetListSubquery(cbRelationshipStatus)
            settings.crChildrenId = GetListSubquery(cbChildren)


            If (cbCountry.SelectedItem IsNot Nothing AndAlso cbCountry.SelectedItem.Index > 0) Then
                settings.crCountry = cbCountry.SelectedItem.Value
            Else
                settings.crCountry = Nothing
            End If


            settings.crRegion = GetRegionName()
            settings.crCity = GetCityName()

            If (ddldistanceAdv.SelectedValue > 0) Then
                settings.crDistance = -1
                Integer.TryParse(ddldistanceAdv.SelectedValue, settings.crDistance)
            Else
                settings.crDistance = Nothing
            End If

            settings.crReligionId = GetListSubquery(cbReligion)
            settings.crSmokingId = GetListSubquery(cbSmoking)
            settings.crDrinkingId = GetListSubquery(cbDrinking)

            Me.CMSDBDataContext.SubmitChanges()

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub




    Private Sub LoadSettings()
        Try

            Dim settings As EUS_AutoNotificationSetting = (From itm In Me.CMSDBDataContext.EUS_AutoNotificationSettings
                                                          Where itm.CustomerID = Me.MasterProfileId
                                                          Select itm).FirstOrDefault()

            If (settings IsNot Nothing) Then

                ClsCombos.SelectComboItem(cbHeightMin, settings.crHeightIdMin)
                ClsCombos.SelectComboItem(cbHeightMax, settings.crHeightIdMax)

                If (settings.crBodyTypeId IsNot Nothing) Then
                    Dim cbBodyTypes As String() = settings.crBodyTypeId.Split(","c)
                    For Each li As ListItem In cbBodyType.Items
                        li.Selected = cbBodyTypes.Contains(li.Value)
                    Next
                End If


                If (settings.crEthnicityId IsNot Nothing) Then
                    Dim cbEthnicitys As String() = settings.crEthnicityId.Split(","c)
                    For Each li As ListItem In cbEthnicity.Items
                        li.Selected = cbEthnicitys.Contains(li.Value)
                    Next
                End If


                If (settings.crEyeColorId IsNot Nothing) Then
                    Dim cbEyeColors As String() = settings.crEyeColorId.Split(","c)
                    For Each li As ListItem In cbEyeColor.Items
                        li.Selected = cbEyeColors.Contains(li.Value)
                    Next
                End If


                If (settings.crHairColorId IsNot Nothing) Then
                    Dim cbHairColors As String() = settings.crHairColorId.Split(","c)
                    For Each li As ListItem In cbHairColor.Items
                        li.Selected = cbHairColors.Contains(li.Value)
                    Next
                End If


                ageMin2.SelectedValue = settings.crAgeMin
                ageMax2.SelectedValue = settings.crAgeMax


                If (settings.crRelationshipStatusId IsNot Nothing) Then
                    Dim cbRelationshipStatuses As String() = settings.crRelationshipStatusId.Split(","c)
                    For Each li As ListItem In cbRelationshipStatus.Items
                        li.Selected = cbRelationshipStatuses.Contains(li.Value)
                    Next
                End If


                If (settings.crChildrenId IsNot Nothing) Then
                    Dim cbChildrens As String() = settings.crChildrenId.Split(","c)
                    For Each li As ListItem In cbChildren.Items
                        li.Selected = cbChildrens.Contains(li.Value)
                    Next
                End If


                ClsCombos.SelectComboItem(cbCountry, settings.crCountry)

                FillRegionCombo()
                ClsCombos.SelectComboItem(cbRegion, settings.crRegion)

                FillCityCombo()
                ClsCombos.SelectComboItem(cbCity, settings.crCity)

                ClsCombos.SelectComboItem(cbCountry, settings.crCountry)


                If (settings.crDistance IsNot Nothing) Then ddldistanceAdv.SelectedValue = settings.crDistance

                If (settings.crReligionId IsNot Nothing) Then
                    Dim cbReligions As String() = settings.crReligionId.Split(","c)
                    For Each li As ListItem In cbReligion.Items
                        li.Selected = cbReligions.Contains(li.Value)
                    Next
                End If


                If (settings.crSmokingId IsNot Nothing) Then
                    Dim cbSmokings As String() = settings.crSmokingId.Split(","c)
                    For Each li As ListItem In cbSmoking.Items
                        li.Selected = cbSmokings.Contains(li.Value)
                    Next
                End If


                If (settings.crDrinkingId IsNot Nothing) Then
                    Dim cbDrinkings As String() = settings.crDrinkingId.Split(","c)
                    For Each li As ListItem In cbDrinking.Items
                        li.Selected = cbDrinkings.Contains(li.Value)
                    Next
                End If

            End If


        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub



    Protected Sub cbCountry_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbCountry.SelectedIndexChanged
        FillRegionCombo()
    End Sub

    Protected Sub cbRegion_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbRegion.SelectedIndexChanged
        FillCityCombo()
    End Sub



    Protected Sub FillRegionCombo()

        Try
            cbRegion.DataBind()
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try


        If (cbRegion.Items.Count = 0) Then
            Dim li As New DevExpress.Web.ASPxEditors.ListEditItem(CurrentPageData.GetCustomString("msg_NotFound"))
            cbRegion.Items.Insert(0, li)

            Dim li2 As New DevExpress.Web.ASPxEditors.ListEditItem(CurrentPageData.GetCustomString("msg_NotFound"))
            cbCity.Items.Insert(0, li2)

            'cbRegion.Text = CurrentPageData.GetCustomString("msg_NotFound")
            'cbCity.Text = CurrentPageData.GetCustomString("msg_NotFound")
        Else
            Dim li As New DevExpress.Web.ASPxEditors.ListEditItem(CurrentPageData.GetCustomString(msg_SelectRegionText.ID))
            cbRegion.Items.Insert(0, li)
            cbRegion.Items(0).Selected = True

            Dim li2 As New DevExpress.Web.ASPxEditors.ListEditItem(CurrentPageData.GetCustomString("msg_NotFound"))
            cbCity.Items.Insert(0, li2)
            cbCity.Items(0).Selected = True

            'cbRegion.Text = CurrentPageData.GetCustomString(msg_SelectRegionText.ID)
            'cbCity.Text = CurrentPageData.GetCustomString("msg_NotFound")
        End If


    End Sub


    Protected Sub FillCityCombo()

        Try
            cbCity.DataBind()
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        If (cbCity.Items.Count = 0) Then
            Dim li2 As New DevExpress.Web.ASPxEditors.ListEditItem(CurrentPageData.GetCustomString("msg_NotFound"))
            cbCity.Items.Insert(0, li2)
        Else
            Dim li2 As New DevExpress.Web.ASPxEditors.ListEditItem(CurrentPageData.GetCustomString(msg_SelectCity.ID))
            cbCity.Items.Insert(0, li2)
            cbCity.Items(0).Selected = True
        End If
        'If (cbCity.Items.Count = 0) Then
        '    cbCity.Text = CurrentPageData.GetCustomString("msg_NotFound")
        'End If

    End Sub


    Private Function GetCityName() As String
        Dim city As String = Nothing
        If (cbCity.SelectedItem IsNot Nothing) Then
            city = cbCity.SelectedItem.Value
        Else
            city = cbCity.Text.Trim()
        End If

        If (city = CurrentPageData.GetCustomString(msg_SelectCity.ID)) Then
            city = Nothing
        End If

        If (city = CurrentPageData.GetCustomString("msg_NotFound")) Then
            city = Nothing
        End If

        Return city
    End Function



    Private Function GetRegionName() As String
        Dim region As String = Nothing
        If (cbRegion.SelectedItem IsNot Nothing) Then
            region = cbRegion.SelectedItem.Value
        Else
            region = cbRegion.Text.Trim()
        End If

        If (region = CurrentPageData.GetCustomString(msg_SelectRegionText.ID)) Then
            region = Nothing
        End If

        If (region = CurrentPageData.GetCustomString("msg_NotFound")) Then
            region = Nothing
        End If

        Return region
    End Function


    Public Function GetListSubquery(cb As CheckBoxList) As String
        Dim cbItems As String = ""
        For Each li As ListItem In cb.Items
            If (li.Selected) Then
                cbItems = cbItems & li.Value & ","
            End If
        Next
        If (cbItems <> "") Then
            cbItems = cbItems.TrimEnd(",")
        Else
            cbItems = Nothing
        End If

        Return cbItems
    End Function


End Class
﻿<%@ Page Language="vb" AutoEventWireup="false" 
CodeBehind="AutoNotificationsSettings.aspx.vb" 
Inherits="Dating.Server.Site.Web.AutoNotificationsSettings" 
MasterPageFile="~/Members/Members2Inner.Master" %>

<%@ Register src="~/UserControls/AgeDropDown.ascx" tagname="AgeDropDown" tagprefix="uc1" %>
<%@ Register src="~/UserControls/DistanceDropDown.ascx" tagname="DistanceDropDown" tagprefix="uc4" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<style type="text/css">
    body{background-image:none;}
</style>
    <script type="text/javascript">
        function SelectCheckBoxes(scope) {
            $('input[type="checkbox"]', $(scope)).prop( "checked", true );
        }
        function ClearCheckBoxes(scope) {
            $('input[type="checkbox"]', $(scope)).prop("checked", false);
        }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">

<asp:UpdatePanel ID="updCnt" runat="server" RenderMode="Inline">
<ContentTemplate>
    <div class="container_12" id="content" style="width:450px;background-image:none;margin-top:10px;margin-bottom:20px;">
        <div class="grid_9 body" style="width:auto;background-color:#fff;">
            <div class="middle" id="content-outter" style="padding-left:15px;" >
    <div class="form-horizontal adv_search">
		<%--<fieldset>
		<legend><asp:Literal ID="msg_AppearanceSectionTitle" runat="server" Visible="false"/></legend>--%>
		
		
        <div class="control-group " id="divLocationsCountry" runat="server">
			<!-- Country  -->
			<asp:Label ID="msg_SelectCountry" runat="server" Text="" CssClass="psLabel lfloat" AssociatedControlID="cbCountry"/>
			<div class="controls">
                <dx:ASPxComboBox ID="cbCountry" runat="server" 
                            Height="18px" EncodeHtml="False" 
                            IncrementalFilteringMode="StartsWith" AutoPostBack="True">
                </dx:ASPxComboBox>	
			</div>
        </div>

		<!-- region  -->
		<!-- region  -->
        <div class="control-group" id="liLocationsRegion" runat="server" style="margin-top:10px;">
            <!--<div>
				<span class="zip_list"><asp:LinkButton ID="msg_SelectZip" runat="server" OnClientClick="showZip();return false;"/></span>
            </div>-->
            <asp:Label ID="msg_SelectRegionText" runat="server" Text="" class="psLabel lfloat" AssociatedControlID="cbRegion"/>
			<div class="controls">
                <dx:ASPxComboBox ID="cbRegion" runat="server" EncodeHtml="False" 
                    EnableSynchronization="False"  ClientInstanceName="cbRegion"
                        IncrementalFilteringMode="StartsWith" DataSourceID="sdsRegion" 
                        TextField="region1" ValueField="region1" AutoPostBack="True">
                    </dx:ASPxComboBox>
					<asp:SqlDataSource ID="sdsRegion" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:AppDBconnectionString %>" 
                        SelectCommand="SELECT DISTINCT region1 FROM SYS_GEO_GR WHERE (countrycode = @countrycode) AND (language = @language)">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="cbCountry" DefaultValue="GR" 
                                Name="countrycode" PropertyName="Value" />
                            <asp:Parameter DefaultValue="EN" Name="language" />
                        </SelectParameters>
                    </asp:SqlDataSource>
			</div>
        </div>

		<!-- city  -->
        <div  class="control-group" id="liLocationsCity" runat="server" style="margin-top:10px;">
            <asp:Label ID="msg_SelectCity" runat="server" Text="" class="psLabel lfloat" AssociatedControlID="cbCity"/>
			<div class="controls">
                <dx:ASPxComboBox ID="cbCity" runat="server" 
                    Height="18px" EncodeHtml="False" EnableSynchronization="False"  ClientInstanceName="cbCity"
                    IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" 
                    DataSourceID="sdsCity" TextField="city" ValueField="city">
                </dx:ASPxComboBox>	
                <asp:SqlDataSource ID="sdsCity" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:AppDBconnectionString %>" 
                    SelectCommand="SELECT DISTINCT city FROM SYS_GEO_GR WHERE (region1 = @region1) AND (language = @language)">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="cbRegion" Name="region1" 
                            PropertyName="Value" />
                        <asp:Parameter DefaultValue="EN" Name="language" />
                    </SelectParameters>
                </asp:SqlDataSource>
			</div>
        </div>

        <div class="control-group" style="margin-top:10px;">
			<asp:Label ID="msg_SelectDistance" runat="server" Text="" CssClass="psLabel lfloat" AssociatedControlID="cbReligion"/>
			<div class="controls">
	            <uc4:DistanceDropDown ID="ddldistanceAdv" runat="server" AutoPostBack="false"  />
			</div>
        </div>



		<div class="control-group ">
            <asp:Label ID="msg_AgeWord" class="psLabel lfloat" runat="server" AssociatedControlID="msg_FromWord"/>
            <div class="controls agelist">
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td><asp:Label ID="msg_FromWord2" class="help-inline" runat="server" AssociatedControlID="ageMin2"/></td>
                    <td><uc1:agedropdown ID="ageMin2" runat="server" CssClass="input-small" 
                            DefaultValue="Minimum" /></td>
                    <td>&nbsp;</td>
                    <td><asp:Label ID="msg_ToWord2" class="help-inline" runat="server" AssociatedControlID="ageMax2"/></td>
                    <td><uc1:agedropdown ID="ageMax2" runat="server" CssClass="input-small" 
                            DefaultValue="Maximum" /></td>
                </tr>
            </table>
            </div>
		</div>

		<!-- height -->
		<div class="control-group ">
			<asp:Label ID="msg_PersonalHeight" runat="server" Text="" CssClass="psLabel lfloat" AssociatedControlID="cbHeightMin"/>
			<div class="controls">
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td><asp:Label class="help-inline" ID="msg_FromWord" runat="server"/></td>
                    <td><dx:ASPxComboBox ID="cbHeightMin" runat="server" EncodeHtml="False" CssClass="input-small" Width="90px">
                    </dx:ASPxComboBox></td>
                    <td>&nbsp;</td>
                    <td><span class="help-inline"><asp:Literal ID="msg_ToWord" runat="server"/></span></td>
                    <td><dx:ASPxComboBox ID="cbHeightMax" runat="server" EncodeHtml="False" CssClass="input-small" Width="90px">
                    </dx:ASPxComboBox></td>
                </tr>
            </table>
			</div>
		</div>

		<div class="control-group ">
			<asp:Label ID="msg_PersonalEyeClr" runat="server" Text="" CssClass="psLabel lfloat" AssociatedControlID="cbEyeColor"/>
			<div class="controls">
                <asp:CheckBoxList ID="cbEyeColor" runat="server"
                                EncodeHtml="False">
                            </asp:CheckBoxList>
			</div>
            <table class="center-table">
                <tr>
                    <td><asp:HyperLink ID="lnkEyeClr" runat="server" NavigateUrl="javascript:void(0);">Clear</asp:HyperLink></td>
                    <td>&nbsp;&nbsp;&nbsp;</td>
                    <td><asp:HyperLink ID="lnkEyeAll" runat="server" NavigateUrl="javascript:void(0);">Select All</asp:HyperLink></td>
                </tr>
            </table>
            <script type="text/javascript">
                $("#<%= lnkEyeClr.ClientID%>").click(function () { ClearCheckBoxes("#<%= cbEyeColor.ClientID%>") });
                $("#<%= lnkEyeAll.ClientID%>").click(function () { SelectCheckBoxes("#<%= cbEyeColor.ClientID%>") });
            </script>
		</div>

		<div class="control-group ">
			<asp:Label ID="msg_PersonalHairClr" runat="server" Text="" CssClass="psLabel lfloat" AssociatedControlID="cbHairColor"/>
			<div class="controls">
                <asp:CheckBoxList ID="cbHairColor" runat="server"
                                EncodeHtml="False">
                            </asp:CheckBoxList>
            </div>
            <table class="center-table">
                <tr>
                    <td><asp:HyperLink ID="lnkHairClr" runat="server" NavigateUrl="javascript:void(0);">Clear</asp:HyperLink></td>
                    <td>&nbsp;&nbsp;&nbsp;</td>
                    <td><asp:HyperLink ID="lnkHairAll" runat="server" NavigateUrl="javascript:void(0);">Select All</asp:HyperLink></td>
                </tr>
            </table>
            <script type="text/javascript">
                $("#<%= lnkHairClr.ClientID%>").click(function () { ClearCheckBoxes("#<%= cbHairColor.ClientID%>") });
                $("#<%= lnkHairAll.ClientID%>").click(function () { SelectCheckBoxes("#<%= cbHairColor.ClientID%>") });
            </script>
		</div>

		<!-- body type -->
        <div class="control-group">
			<asp:Label ID="msg_PersonalBodyType" runat="server" Text="" CssClass="psLabel lfloat" AssociatedControlID="cbBodyType"/>
            <div class="controls">
                <asp:CheckBoxList  ID="cbBodyType" runat="server"
                    EncodeHtml="False">
                </asp:CheckBoxList >	
            </div>
            <table class="center-table">
                <tr>
                    <td><asp:HyperLink ID="lnkBodyClr" runat="server" NavigateUrl="javascript:void(0);">Clear</asp:HyperLink></td>
                    <td>&nbsp;&nbsp;&nbsp;</td>
                    <td><asp:HyperLink ID="lnkBodyAll" runat="server" NavigateUrl="javascript:void(0);">Select All</asp:HyperLink></td>
                </tr>
            </table>
            <script type="text/javascript">
                $("#<%= lnkBodyClr.ClientID%>").click(function () { ClearCheckBoxes("#<%= cbBodyType.ClientID%>") });
                $("#<%= lnkBodyAll.ClientID%>").click(function () { SelectCheckBoxes("#<%= cbBodyType.ClientID%>") });
            </script>
        </div>

		<div class="control-group ">
			<asp:Label ID="msg_RelationshipStatus" runat="server" Text="" CssClass="psLabel lfloat" AssociatedControlID="cbRelationshipStatus"/>
            <div class="controls">
                <asp:CheckBoxList ID="cbRelationshipStatus" runat="server" Width="310px" 
                                EncodeHtml="False">
                            </asp:CheckBoxList >
            </div>
            <table class="center-table">
                <tr>
                    <td><asp:HyperLink ID="lnkRelStatClr" runat="server" NavigateUrl="javascript:void(0);">Clear</asp:HyperLink></td>
                    <td>&nbsp;&nbsp;&nbsp;</td>
                    <td><asp:HyperLink ID="lnkRelStatAll" runat="server" NavigateUrl="javascript:void(0);">Select All</asp:HyperLink></td>
                </tr>
            </table>
            <script type="text/javascript">
                $("#<%= lnkRelStatClr.ClientID%>").click(function () { ClearCheckBoxes("#<%= cbRelationshipStatus.ClientID%>") });
                $("#<%= lnkRelStatAll.ClientID%>").click(function () { SelectCheckBoxes("#<%= cbRelationshipStatus.ClientID%>") });
            </script>
		</div>
        <div class="clear"></div>

        <div class="control-group ">
			<asp:Label ID="msg_PersonalReligion" runat="server" Text="" CssClass="psLabel lfloat" AssociatedControlID="cbReligion"/>
			<div class="controls">
                <asp:CheckBoxList  ID="cbReligion" runat="server"
                                EncodeHtml="False">
                            </asp:CheckBoxList >
			</div>
            <table class="center-table">
                <tr>
                    <td><asp:HyperLink ID="lnkReligClr" runat="server" NavigateUrl="javascript:void(0);">Clear</asp:HyperLink></td>
                    <td>&nbsp;&nbsp;&nbsp;</td>
                    <td><asp:HyperLink ID="lnkReligAll" runat="server" NavigateUrl="javascript:void(0);">Select All</asp:HyperLink></td>
                </tr>
            </table>
            <script type="text/javascript">
                $("#<%= lnkReligClr.ClientID%>").click(function () { ClearCheckBoxes("#<%= cbReligion.ClientID%>") });
                $("#<%= lnkReligAll.ClientID%>").click(function () { SelectCheckBoxes("#<%= cbReligion.ClientID%>") });
            </script>
		</div>

		<!-- body type -->
		<div class="control-group ">
						<asp:Label ID="msg_PersonalEthnicity" runat="server" Text="" CssClass="psLabel lfloat" AssociatedControlID="cbEthnicity"/>
            <div class="controls">
                            <asp:CheckBoxList ID="cbEthnicity" runat="server"
                                EncodeHtml="False">
                            </asp:CheckBoxList>	
            </div>
            <table class="center-table">
                <tr>
                    <td><asp:HyperLink ID="lnkEthnClr" runat="server" NavigateUrl="javascript:void(0);">Clear</asp:HyperLink></td>
                    <td>&nbsp;&nbsp;&nbsp;</td>
                    <td><asp:HyperLink ID="lnkEthnAll" runat="server" NavigateUrl="javascript:void(0);">Select All</asp:HyperLink></td>
                </tr>
            </table>
            <script type="text/javascript">
                $("#<%= lnkEthnClr.ClientID%>").click(function () { ClearCheckBoxes("#<%= cbEthnicity.ClientID%>") });
                $("#<%= lnkEthnAll.ClientID%>").click(function () { SelectCheckBoxes("#<%= cbEthnicity.ClientID%>") });
            </script>
		</div>

		<div class="control-group ">
			<asp:Label ID="msg_PersonalChildren" runat="server" Text="" CssClass="psLabel lfloat" AssociatedControlID="cbChildren"/>
			<div class="controls">
                <asp:CheckBoxList  ID="cbChildren" runat="server"
                                EncodeHtml="False">
                            </asp:CheckBoxList >
    		</div>
            <table class="center-table">
                <tr>
                    <td><asp:HyperLink ID="lnkChildClr" runat="server" NavigateUrl="javascript:void(0);">Clear</asp:HyperLink></td>
                    <td>&nbsp;&nbsp;&nbsp;</td>
                    <td><asp:HyperLink ID="lnkChildAll" runat="server" NavigateUrl="javascript:void(0);">Select All</asp:HyperLink></td>
                </tr>
            </table>
            <script type="text/javascript">
                $("#<%= lnkChildClr.ClientID%>").click(function () { ClearCheckBoxes("#<%= cbChildren.ClientID%>") });
                $("#<%= lnkChildAll.ClientID%>").click(function () { SelectCheckBoxes("#<%= cbChildren.ClientID%>") });
            </script>
		</div>

		<div class="control-group ">
			<asp:Label ID="msg_PersonalSmokingHabit" runat="server" Text="" CssClass="psLabel lfloat" AssociatedControlID="cbSmoking"/>
			<div class="controls">
                <asp:CheckBoxList  ID="cbSmoking" runat="server"
                                EncodeHtml="False">
                            </asp:CheckBoxList >
			</div>
            <table class="center-table">
                <tr>
                    <td><asp:HyperLink ID="lnkSmokeClr" runat="server" NavigateUrl="javascript:void(0);">Clear</asp:HyperLink></td>
                    <td>&nbsp;&nbsp;&nbsp;</td>
                    <td><asp:HyperLink ID="lnkSmokeAll" runat="server" NavigateUrl="javascript:void(0);">Select All</asp:HyperLink></td>
                </tr>
            </table>
            <script type="text/javascript">
                $("#<%= lnkSmokeClr.ClientID%>").click(function () { ClearCheckBoxes("#<%= cbSmoking.ClientID%>") });
                $("#<%= lnkSmokeAll.ClientID%>").click(function () { SelectCheckBoxes("#<%= cbSmoking.ClientID%>") });
            </script>
		</div>

		<div class="control-group ">
			<asp:Label ID="msg_PersonalDrinkingHabit" runat="server" Text="" CssClass="psLabel lfloat" AssociatedControlID="cbDrinking"/>
			<div class="controls">
                <asp:CheckBoxList  ID="cbDrinking" runat="server"
                                EncodeHtml="False">
                            </asp:CheckBoxList >
			</div>
            <table class="center-table">
                <tr>
                    <td><asp:HyperLink ID="lnkDrinkClr" runat="server" NavigateUrl="javascript:void(0);">Clear</asp:HyperLink></td>
                    <td>&nbsp;&nbsp;&nbsp;</td>
                    <td><asp:HyperLink ID="lnkDrinkAll" runat="server" NavigateUrl="javascript:void(0);">Select All</asp:HyperLink></td>
                </tr>
            </table>
            <script type="text/javascript">
                $("#<%= lnkDrinkClr.ClientID%>").click(function () { ClearCheckBoxes("#<%= cbDrinking.ClientID%>") });
                $("#<%= lnkDrinkAll.ClientID%>").click(function () { SelectCheckBoxes("#<%= cbDrinking.ClientID%>") });
            </script>
		</div>

		<%--</fieldset>--%>
        <div class="clear"></div>
		<div class="form-actions" style="padding-left:0px;width:auto;">
            <dx:ASPxButton ID="btnSave" runat="server" CausesValidation="True" CommandName="Insert"
                CssClass="btn btn-primary" Text="Save Criterias" Native="True"  EncodeHtml="False" />
        </div>
    </div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </div>
</ContentTemplate>
</asp:UpdatePanel>

</asp:Content>

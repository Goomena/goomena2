﻿Imports Dating.Server.Core.DLL

'Imports Sites.SharedSiteCode

Public Class payment
    Inherits BasePage


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Me.IsPostBack Then
            Dim SalesSiteID As Integer = ConfigurationManager.AppSettings("siteID")

            Dim ppType As String = ""
            Try
                ppType = Request.QueryString("type")
            Catch ex As Exception
                ppType = ""
            End Try
            Dim paymentMethod As String = Request.QueryString("method")
            Dim amount As Long
            Dim product As String = Request.QueryString("product")

            Try

                If Not String.IsNullOrEmpty(Request.QueryString("amount")) Then Long.TryParse(Request.QueryString("amount"), amount)

                If (product Is Nothing) Then product = ""

                If (paymentMethod = "regnow") Then
                    Dim type As String = ""
                    Dim quantity As String = ""

                    If product.Contains("credits") Then
                        type = "Credits"
                        quantity = product.Replace("credits", "")
                    End If

                    Dim productCode As String = clsPricing.GetProductCode(quantity, type, Me.GetCurrentProfile().Country)

                    clsCustomer.sendPayment(amount, type, quantity,
                                            Me.SessionVariables.MemberData.Email,
                                            Me.MasterProfileId,
                                            Me.SessionVariables.MemberData.CustomReferrer,
                                            Trim(Request("ourl")),
                                            paymentMethod,
                                            Session("PromoCode"),
                                            SalesSiteID,
                                            ppType,
                                            productCode,
                                            Me.SessionVariables.MemberData.Country)

                ElseIf amount > 0 Then
                    Dim productCode As String = Nothing
                    Select Case Request.QueryString("method")
                        Case "PayPal"
                            clsCustomer.sendPayment(amount, "Money", "Free", Me.SessionVariables.MemberData.Email,
                                                    Me.MasterProfileId,
                                                    Me.SessionVariables.MemberData.CustomReferrer, Trim(Request("ourl")), "PayPal", Session("PromoCode"), SalesSiteID, ppType, productCode,
                                            Me.SessionVariables.MemberData.Country)
                        Case "AlertPay"
                            clsCustomer.sendPayment(amount, "Money", "Free", Me.SessionVariables.MemberData.Email,
                                                    Me.MasterProfileId,
                                                    Me.SessionVariables.MemberData.CustomReferrer, Trim(Request("ourl")), "AlertPay", Session("PromoCode"), SalesSiteID, ppType, productCode,
                                            Me.SessionVariables.MemberData.Country)
                        Case "paysafecard"
                            clsCustomer.sendPayment(amount, "Money", "Free", Me.SessionVariables.MemberData.Email,
                                                    Me.MasterProfileId,
                                                    Me.SessionVariables.MemberData.CustomReferrer, Trim(Request("ourl")), "paysafecard", Session("promoCode"), SalesSiteID, ppType, productCode,
                                            Me.SessionVariables.MemberData.Country)
                        Case "paymentWall"
                            clsCustomer.sendPayment(amount, "Money", "Free", Me.SessionVariables.MemberData.Email,
                                                    Me.MasterProfileId,
                                                    Me.SessionVariables.MemberData.CustomReferrer, Trim(Request("ourl")), "PaymentWall", Session("PromoCode"), SalesSiteID, ppType, productCode,
                                            Me.SessionVariables.MemberData.Country)
                        Case "epoch"
                            clsCustomer.sendPayment(amount, "Money", "Free", Me.SessionVariables.MemberData.Email,
                                                    Me.MasterProfileId,
                                                    Me.SessionVariables.MemberData.CustomReferrer, Trim(Request("ourl")), "epoch", Session("PromoCode"), SalesSiteID, ppType, productCode,
                                            Me.SessionVariables.MemberData.Country)

                    End Select
                Else

                    If Not Request.QueryString("price") Is Nothing Or Not Request.QueryString("price") = "" Then
                        If Request.QueryString("product").Contains("oron") Then
                            ' If Request.QueryString("code") <> "" Then Session("PromoCode") = Request.QueryString("code")
                            'If Me.MasterProfileId Is Nothing Then

                            'Response.Redirect("~/login.aspx")
                            'Else
                            'Dim amount As String = 0
                            amount = 0
                            Dim type As String = ""
                            Dim quantity As String = ""
                            type = "OronGigabytes"
                            quantity = Request.QueryString("product").Replace("orongiga", "")

                            Dim productCode As String = clsPricing.GetProductCode(quantity, type, Me.GetCurrentProfile().Country)

                            Select Case Request.QueryString("method")
                                Case "paypal"
                                    clsCustomer.sendPayment(amount, type, quantity, Me.SessionVariables.MemberData.Email,
                                                            Me.MasterProfileId,
                                                            Me.SessionVariables.MemberData.CustomReferrer, Trim(Request("ourl")), "PayPal", Session("PromoCode"), SalesSiteID, ppType, productCode,
                                            Me.SessionVariables.MemberData.Country)
                                Case "credit"
                                    ' If Me.MasterProfileId = 30882 Then
                                    clsCustomer.sendPayment(amount, type, quantity, Me.SessionVariables.MemberData.Email,
                                                            Me.MasterProfileId,
                                                            Me.SessionVariables.MemberData.CustomReferrer, Trim(Request("ourl")), "2CO", Session("PromoCode"), SalesSiteID, ppType, productCode,
                                            Me.SessionVariables.MemberData.Country)
                                    'Else
                                    'clsCustomer.sendPayment(amount, type, quantity, Me.SessionVariables.LoginName, Me.MasterProfileId,  Me.SessionVariables.DataRecordLoginMemberReturn.CustomReferrer, Trim(Request("ourl")), "PayPal", Session("PromoCode"),SalesSiteID)

                                    'End If

                                    'clsCustomer.sendPayment(amount, type, quantity, Me.SessionVariables.LoginName, Me.MasterProfileId,  Me.SessionVariables.DataRecordLoginMemberReturn.CustomReferrer, Trim(Request("ourl")), "AlertPay", Session("PromoCode"),SalesSiteID)
                                Case "paysafe"
                                    clsCustomer.sendPayment(amount, type, quantity, Me.SessionVariables.MemberData.Email,
                                                            Me.MasterProfileId,
                                                            Me.SessionVariables.MemberData.CustomReferrer, Trim(Request("ourl")), "paysafecard", Session("promoCode"), SalesSiteID, ppType, productCode,
                                            Me.SessionVariables.MemberData.Country)
                                Case "phone"
                                    Response.Redirect("/member/DaoPayPayment.aspx?ourl=" & Trim(Request("ourl")) & "&prodCode=" & getDaoPayProdCode(Request.QueryString("product")))
                                Case "webmoney"
                                    clsCustomer.sendPayment(amount, type, quantity, Me.SessionVariables.MemberData.Email,
                                                            Me.MasterProfileId,
                                                            Me.SessionVariables.MemberData.CustomReferrer, Trim(Request("ourl")), "webmoney", Session("promoCode"), SalesSiteID, ppType, productCode,
                                            Me.SessionVariables.MemberData.Country)
                                Case "paymentWall"
                                    clsCustomer.sendPayment(amount, type, quantity, Me.SessionVariables.MemberData.Email,
                                                            Me.MasterProfileId,
                                                            Me.SessionVariables.MemberData.CustomReferrer, Trim(Request("ourl")), "PaymentWall", Session("PromoCode"), SalesSiteID, ppType, productCode,
                                            Me.SessionVariables.MemberData.Country)
                                Case "alertpay"
                                    clsCustomer.sendPayment(amount, type, quantity, Me.SessionVariables.MemberData.Email,
                                                            Me.MasterProfileId,
                                                            Me.SessionVariables.MemberData.CustomReferrer, Trim(Request("ourl")), "AlertPay", Session("PromoCode"), SalesSiteID, ppType, productCode,
                                            Me.SessionVariables.MemberData.Country)
                                Case "adyen"
                                    clsCustomer.sendPayment(amount, type, quantity, Me.SessionVariables.MemberData.Email,
                                                            Me.MasterProfileId,
                                                            Me.SessionVariables.MemberData.CustomReferrer, Trim(Request("ourl")), "adyen", Session("PromoCode"), SalesSiteID, ppType, productCode,
                                            Me.SessionVariables.MemberData.Country)
                                Case "epoch"
                                    clsCustomer.sendPayment(amount, type, quantity, Me.SessionVariables.MemberData.Email,
                                                            Me.MasterProfileId,
                                                            Me.SessionVariables.MemberData.CustomReferrer, Trim(Request("ourl")), "epoch", Session("PromoCode"), SalesSiteID, ppType, productCode,
                                            Me.SessionVariables.MemberData.Country)

                            End Select
                        ElseIf Request.QueryString("product").Contains("ul") Then
                            ' If Request.QueryString("code") <> "" Then Session("PromoCode") = Request.QueryString("code")
                            'If Me.MasterProfileId Is Nothing Then

                            'Response.Redirect("~/login.aspx")
                            'Else
                            'Dim amount As String = 0
                            amount = 0
                            Dim type As String = ""
                            Dim quantity As String = ""
                            type = "UploadedGigabytes"
                            quantity = Request.QueryString("product").Replace("ulgiga", "")

                            Dim productCode As String = clsPricing.GetProductCode(quantity, type, Me.GetCurrentProfile().Country)

                            Select Case Request.QueryString("method")
                                Case "paypal"
                                    clsCustomer.sendPayment(amount, type, quantity, Me.SessionVariables.MemberData.Email,
                                                            Me.MasterProfileId,
                                                            Me.SessionVariables.MemberData.CustomReferrer, Trim(Request("ourl")), "PayPal", Session("PromoCode"), SalesSiteID, ppType, productCode,
                                            Me.SessionVariables.MemberData.Country)
                                Case "credit"
                                    ' If Me.MasterProfileId = 30882 Then
                                    clsCustomer.sendPayment(amount, type, quantity, Me.SessionVariables.MemberData.Email,
                                                            Me.MasterProfileId,
                                                            Me.SessionVariables.MemberData.CustomReferrer, Trim(Request("ourl")), "2CO", Session("PromoCode"), SalesSiteID, ppType, productCode,
                                            Me.SessionVariables.MemberData.Country)
                                    'Else
                                    'clsCustomer.sendPayment(amount, type, quantity, Me.SessionVariables.LoginName, Me.MasterProfileId,  Me.SessionVariables.DataRecordLoginMemberReturn.CustomReferrer, Trim(Request("ourl")), "PayPal", Session("PromoCode"),SalesSiteID)

                                    'End If

                                    'clsCustomer.sendPayment(amount, type, quantity, Me.SessionVariables.LoginName, Me.MasterProfileId,  Me.SessionVariables.DataRecordLoginMemberReturn.CustomReferrer, Trim(Request("ourl")), "AlertPay", Session("PromoCode"),SalesSiteID)
                                Case "paysafe"
                                    clsCustomer.sendPayment(amount, type, quantity, Me.SessionVariables.MemberData.Email,
                                                            Me.MasterProfileId,
                                                            Me.SessionVariables.MemberData.CustomReferrer, Trim(Request("ourl")), "paysafecard", Session("promoCode"), SalesSiteID, ppType, productCode,
                                            Me.SessionVariables.MemberData.Country)
                                Case "phone"
                                    Response.Redirect("/member/DaoPayPayment.aspx?ourl=" & Trim(Request("ourl")) & "&prodCode=" & getDaoPayProdCode(Request.QueryString("product")))
                                Case "webmoney"
                                    clsCustomer.sendPayment(amount, type, quantity, Me.SessionVariables.MemberData.Email,
                                                            Me.MasterProfileId,
                                                            Me.SessionVariables.MemberData.CustomReferrer, Trim(Request("ourl")), "webmoney", Session("promoCode"), SalesSiteID, ppType, productCode,
                                            Me.SessionVariables.MemberData.Country)
                                Case "paymentWall"
                                    clsCustomer.sendPayment(amount, type, quantity, Me.SessionVariables.MemberData.Email,
                                                            Me.MasterProfileId,
                                                            Me.SessionVariables.MemberData.CustomReferrer, Trim(Request("ourl")), "PaymentWall", Session("PromoCode"), SalesSiteID, ppType, productCode,
                                            Me.SessionVariables.MemberData.Country)
                                Case "adyen"
                                    clsCustomer.sendPayment(amount, type, quantity, Me.SessionVariables.MemberData.Email,
                                                            Me.MasterProfileId,
                                                            Me.SessionVariables.MemberData.CustomReferrer, Trim(Request("ourl")), "adyen", Session("PromoCode"), SalesSiteID, ppType, productCode,
                                            Me.SessionVariables.MemberData.Country)
                                Case "epoch"
                                    clsCustomer.sendPayment(amount, type, quantity, Me.SessionVariables.MemberData.Email,
                                                            Me.MasterProfileId,
                                                            Me.SessionVariables.MemberData.CustomReferrer, Trim(Request("ourl")), "epoch", Session("PromoCode"), SalesSiteID, ppType, productCode,
                                            Me.SessionVariables.MemberData.Country)

                            End Select
                        Else

                            'Dim amount As String = 0
                            amount = 0
                            Dim type As String = ""
                            Dim quantity As String = ""
                            product = Request.QueryString("product")

                            If product.Contains("credits") Then
                                type = "Credits"
                                quantity = Request.QueryString("product").Replace("credits", "")
                            ElseIf product.EndsWith("days", StringComparison.OrdinalIgnoreCase) Then
                                type = "Days"
                                quantity = Request.QueryString("product").ToLower().Replace("days", "").Replace("dd", "")
                            End If

                            Dim productCode As String = clsPricing.GetProductCode(quantity, type, Me.GetCurrentProfile().Country)

                            Select Case Request.QueryString("method")
                                Case "paypal"
                                    clsCustomer.sendPayment(amount, type, quantity, Me.SessionVariables.MemberData.Email,
                                                            Me.MasterProfileId,
                                                            Me.SessionVariables.MemberData.CustomReferrer, Trim(Request("ourl")), "PayPal", Session("PromoCode"), SalesSiteID, ppType, productCode,
                                            Me.SessionVariables.MemberData.Country)
                                Case "credit"
                                    ' If Me.MasterProfileId = 30882 Then
                                    clsCustomer.sendPayment(amount, type, quantity, Me.SessionVariables.MemberData.Email,
                                                            Me.MasterProfileId,
                                                            Me.SessionVariables.MemberData.CustomReferrer, Trim(Request("ourl")), "2CO", Session("PromoCode"), SalesSiteID, ppType, productCode,
                                            Me.SessionVariables.MemberData.Country)
                                    'Else
                                    'clsCustomer.sendPayment(amount, type, quantity, Me.SessionVariables.LoginName, Me.MasterProfileId,  Me.SessionVariables.DataRecordLoginMemberReturn.CustomReferrer, Trim(Request("ourl")), "PayPal", Session("PromoCode"),SalesSiteID)

                                    'End If
                                    ' clsCustomer.sendPayment(amount, type, quantity, Me.SessionVariables.LoginName, Me.MasterProfileId,  Me.SessionVariables.DataRecordLoginMemberReturn.CustomReferrer, Trim(Request("ourl")), "AlertPay", Session("PromoCode"),SalesSiteID)
                                Case "paysafe"
                                    clsCustomer.sendPayment(amount, type, quantity, Me.SessionVariables.MemberData.Email,
                                                            Me.MasterProfileId,
                                                            Me.SessionVariables.MemberData.CustomReferrer, Trim(Request("ourl")), "paysafecard", Session("promoCode"), SalesSiteID, ppType, productCode,
                                            Me.SessionVariables.MemberData.Country)
                                Case "phone"
                                    Response.Redirect("/member/DaoPayPayment.aspx?ourl=" & Trim(Request("ourl")) & "&prodCode=" & getDaoPayProdCode(Request.QueryString("product")))
                                Case "alertpay"
                                    clsCustomer.sendPayment(amount, type, quantity, Me.SessionVariables.MemberData.Email,
                                                            Me.MasterProfileId,
                                                            Me.SessionVariables.MemberData.CustomReferrer, Trim(Request("ourl")), "AlertPay", Session("PromoCode"), SalesSiteID, ppType, productCode,
                                            Me.SessionVariables.MemberData.Country)
                                Case "webmoney"
                                    clsCustomer.sendPayment(amount, type, quantity, Me.SessionVariables.MemberData.Email,
                                                            Me.MasterProfileId,
                                                            Me.SessionVariables.MemberData.CustomReferrer, Trim(Request("ourl")), "WebMoney", Session("promoCode"), SalesSiteID, ppType, productCode,
                                            Me.SessionVariables.MemberData.Country)
                                Case "paymentWall"
                                    clsCustomer.sendPayment(amount, type, quantity, Me.SessionVariables.MemberData.Email,
                                                            Me.MasterProfileId,
                                                            Me.SessionVariables.MemberData.CustomReferrer,
                                                            Trim(Request("ourl")), "PaymentWall",
                                                            Session("PromoCode"), SalesSiteID,
                                                            ppType,
                                                            productCode,
                                                            Me.SessionVariables.MemberData.Country)
                                Case "adyen"
                                    clsCustomer.sendPayment(amount, type, quantity, Me.SessionVariables.MemberData.Email,
                                                            Me.MasterProfileId,
                                                            Me.SessionVariables.MemberData.CustomReferrer, Trim(Request("ourl")), "adyen", Session("PromoCode"), SalesSiteID, ppType, productCode,
                                            Me.SessionVariables.MemberData.Country)
                                Case "epoch"
                                    clsCustomer.sendPayment(amount, type, quantity, Me.SessionVariables.MemberData.Email,
                                                            Me.MasterProfileId,
                                                            Me.SessionVariables.MemberData.CustomReferrer, Trim(Request("ourl")), "epoch", Session("PromoCode"), SalesSiteID, ppType, productCode,
                                            Me.SessionVariables.MemberData.Country)

                            End Select
                            'End If
                        End If

                    End If
                End If

            Catch ex As Exception

            End Try
        End If

    End Sub
    Public Function getDaoPayProdCode(ByVal selection As String) As Integer

        If selection.Contains("days") Then
            Dim quantity As Integer = CInt(selection.Replace("days", ""))

            Select Case quantity
                Case 30
                    Return 101
                Case 90
                    Return 102
                Case 180
                    Return 103
                Case 365
                    Return 104

            End Select
        ElseIf selection.Contains("giga") Then
            Dim quantity As Integer = CInt(selection.Replace("gigabytes", ""))

            Select Case quantity
                Case 50
                    Return 201
                Case 150
                    Return 202
                Case 300
                    Return 203
                Case 600
                    Return 204
            End Select
        End If
        Return -1
    End Function

End Class


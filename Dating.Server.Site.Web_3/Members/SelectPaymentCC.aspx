﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Members/Members2Inner.Master" 
    CodeBehind="SelectPaymentCC.aspx.vb" Inherits="Dating.Server.Site.Web.SelectPaymentCC" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        body { background-image: none; }
        a.popup-warning-continue, a.popup-warning-cancel{border: none;display:inline-block;padding-left:10px;padding-right:10px;height:47px;font-size:14px;cursor:pointer;color:#fff;line-height:45px;}
        a.popup-warning-continue{background:#0098CE;}
        a.popup-warning-cancel{background:#575555;}
        a.popup-warning-continue:hover{background:#242424;}
        a.popup-warning-cancel:hover{background:#ADADAD;}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">

<div style="width:460px;height:220px;position:relative;margin:50px auto 0;">
    <asp:Label ID="lblInfoTitle" runat="server"></asp:Label>
    <asp:Label ID="lblInfo" runat="server" CssClass="info-content">
    </asp:Label>  

    <span style="font-size:14px;line-height:21px;">
        We see that you already have used a Credit Card for credits' purchasing.<br />
        You can use the same card for new purchasing.<br /><br />
        <h4 style="text-align:center;">
        Do you want to continue with the same Credit Card?
        </h4>
    <br />
    </span>
    <table align="center">
        <tr>
            <td><asp:LinkButton ID="btnContinueWithSameCard" runat="server" OnClientClick="CCUseTheSame();return false;" CssClass="popup-warning-continue">Yes, use the same Credit Card</asp:LinkButton></td>
            <td>&nbsp;</td>
            <td><asp:LinkButton ID="btnUseDifferentCard" runat="server" OnClientClick="CCUseDifferent();return false;" CssClass="popup-warning-cancel">No, use other Credit Card</asp:LinkButton></td>
        </tr>
    </table>
</div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cntBodyBottom" runat="server">
   <script type="text/javascript">
       var popupName = '<%= Request.QueryString("popup") %>';
//function resizePopup1() {
//    var $j = jQuery;
//    var h = $('html').height();
//    var w = $('html').width();
//    try {
//        top.window[popupName].SetSize(w + 50, h + 77);
//        top.window[popupName].UpdatePosition();
//        var title = '<%= Dating.Server.Site.Web.AppUtils.JS_PrepareString (PopupTitle) %>';
//        if (title != null && title.length > 0)
//            top.window[popupName].SetHeaderText(title);
//    }
//    catch (e) { }
//}
       function setFancyboxTitle() {
           try {
               var title = '<%= Dating.Server.Site.Web.AppUtils.JS_PrepareString (PopupTitle) %>';
               if (title != null && title.length > 0) {
                   //alert(title + ' 1')
                   window.top.setFancyboxTitle(title);
                   //alert(title + ' 2')
               }
           }
           catch (e) { }
       }

       function CCUseTheSame() {
           window.top.CCUseTheSame();
       }

       function CCUseDifferent() {
           window.top.CCUseDifferent();
       }

       jQuery(document).ready(function ($) {

//(function () {
//    var availableLevel = parseInt('< %= MyBase.AvailableLevel % >');

//    if (availableLevel == 0) {
//        $('div#private-footer-top-line').addClass('level1');
//        setInterval(toogleLevel1, 500)
//    }
//    else if (availableLevel > 0) {
//        $('div#private-footer-top-line').addClass('level' + availableLevel);
//        for (var i = 1; i <= availableLevel; i++)
//            $('.private-footer-level' + i, '#private-footer').addClass('on')
//    }

//    var isOn = false;
//    function toogleLevel1() {
//        if (isOn) 
//            $('.private-footer-level1', '#private-footer').removeClass('on');
//        else 
//            $('.private-footer-level1', '#private-footer').addClass('on');
//        isOn = !isOn;
//    }
//})();

//setTimeout(setFancyboxTitle, 1500);

       });
    </script>

</asp:Content>

﻿Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Core.DLL
Imports Dating.Server.Datasets.DLL.DSMembers

Public Class MyLists_sharedphotosbyme
    Inherits BasePage


#Region "Props"


    Public Property MyListsSorting As MyListsSortEnum

        Get
            Dim sorting As MyListsSortEnum = MyListsSortEnum.Recent

            Dim index As Integer = cbSort.SelectedIndex
            If (index = 0) Then
                sorting = MyListsSortEnum.Recent
            ElseIf (index = 1) Then
                sorting = MyListsSortEnum.Oldest
            End If

            Return sorting
        End Get

        Set(value As MyListsSortEnum)
            Dim index As Integer = 0

            If (value = MyListsSortEnum.Recent) Then
                index = 0
            ElseIf (value = MyListsSortEnum.Oldest) Then
                index = 1
            End If
            cbSort.SelectedIndex = index
        End Set

    End Property

    Public Property ItemsPerPage As Integer

        Get
            Dim perPageNumber As Integer = 10

            Dim index As Integer = cbPerPage.SelectedIndex
            If (index = 0) Then
                perPageNumber = 10
            ElseIf (index = 1) Then
                perPageNumber = 25
            ElseIf (index = 2) Then
                perPageNumber = 50
            End If

            Return perPageNumber
        End Get

        Set(value As Integer)
            Dim index As Integer = 0

            If (value = 25) Then
                index = 1
            ElseIf (value = 50) Then
                index = 2
            Else
                index = 0
            End If

            cbPerPage.SelectedIndex = index
        End Set

    End Property



    Protected Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            'If (_pageData Is Nothing) Then _pageData = New clsPageData("~/Members/MyLists.aspx", Context)
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData("~/Members/MyLists.aspx", Context, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property


    Private ReadOnly Property CurrentOffersControl As SearchControl
        Get

            'If (mvMyListsMain.GetActiveView().Equals(vwWhoViewedMe)) Then
            Return sharedPhotosByMeList

            'ElseIf (mvMyListsMain.GetActiveView().Equals(vwWhoFavoritedMe)) Then
            '    Return whoFavoritedMeList

            'ElseIf (mvMyListsMain.GetActiveView().Equals(vwWhoSharedPhoto)) Then
            '    Return whoSharedPhotoList

            'ElseIf (mvMyListsMain.GetActiveView().Equals(vwMyFavoriteList)) Then
            '    Return myFavoriteList

            'ElseIf (mvMyListsMain.GetActiveView().Equals(vwMyBlockedList)) Then
            '    Return myBlockedList

            'ElseIf (mvMyListsMain.GetActiveView().Equals(vwMyViewedList)) Then
            '    Return myViewedList

            'End If

            Return Nothing
        End Get
    End Property

#End Region

    Protected Overrides Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        SetMasterPage(CurrentPageData)

        Try
            If clsCurrentContext.VerifyLogin() = True Then
                Me.MasterPageFile = "~/Members/Members2.Master"
                'If (Request("master") = "inner") Then
                '    Me.MasterPageFile = "~/Members/Members2Inner.Master"
                'Else
                'End If
            Else
                clsCurrentContext.ClearSession(Session)
                If (Not Me.IsCallback) Then
                    Response.Redirect(ResolveUrl("~/Login.aspx"), False)
                End If
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        Try
            If (Not Page.IsPostBack) Then
                Try
                    Me.ItemsPerPage = clsProfilesPrivacySettings.GET_ItemsPerPage(Me.MasterProfileId)
                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "GET_ItemsPerPage")
                End Try

                Dim req As String = Request.QueryString("itemsPerPage")
                Dim _itemsPerPage As Integer
                If (Not String.IsNullOrEmpty(req)) Then
                    Integer.TryParse(req, _itemsPerPage)
                    Me.ItemsPerPage = _itemsPerPage
                End If


                req = Request.QueryString("sorting")
                Dim _cbSort As Integer
                If (Not String.IsNullOrEmpty(req)) Then
                    Integer.TryParse(req, _cbSort)
                    If (_cbSort >= 0 AndAlso _cbSort < cbSort.Items.Count) Then
                        cbSort.SelectedIndex = _cbSort
                    End If
                End If


                req = Request.QueryString("seo" & Me.Pager.ClientID)
                Dim currentPage As Integer
                If (Not String.IsNullOrEmpty(req)) Then
                    Try
                        req = req.Replace("page", "")
                        Integer.TryParse(req, currentPage)
                        Me.Pager.ItemCount = Me.ItemsPerPage * currentPage
                        Me.Pager.PageIndex = currentPage - 1
                    Catch ex As Exception
                    End Try
                End If

            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try


        Try

            If (Not Page.IsPostBack) Then

                LoadLAG()
                LoadViews()
            End If


            AddHandler Me.CurrentOffersControl.Repeater.ItemCommand, AddressOf Me.offersSearchRepeater_ItemCommand
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub

    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender

        Try
            Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

        Try

            Dim url As String = Request.Url.AbsolutePath
            url = url & "?"

            Dim seo As String = ("seo" & Me.Pager.ClientID).ToUpper()
            For Each itm As String In Request.QueryString.AllKeys
                If (Not String.IsNullOrEmpty(itm)) Then

                    Dim s As String = itm.ToUpper()
                    If (s <> seo AndAlso s <> "ITEMSPERPAGE" AndAlso s <> "SORTING") Then
                        url = url & itm & "=" & HttpUtility.UrlEncode(Request.QueryString(itm)) & "&"
                    End If

                End If
            Next

            Me.Pager.SeoFriendly = True
            Me.Pager.SeoNavigateUrlFormatString = url & "seo" & Me.Pager.ClientID & "={0}&itemsPerPage=" & Me.ItemsPerPage & "&sorting=" & cbSort.SelectedIndex
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub


    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
        LoadViews()
    End Sub


    Protected Sub LoadLAG()
        Try

            'lnkWhoViewedMe.Text = "<span></span>" & CurrentPageData.GetCustomString("lnkWhoViewedMe")
            'lnkWhoFavoritedMe.Text = "<span></span>" & CurrentPageData.GetCustomString("lnkWhoFavoritedMe")
            'lnkWhoSharedPhotos.Text = "<span></span>" & CurrentPageData.GetCustomString("lnkWhoSharedPhotos")
            'lnkMyFavoriteList.Text = "<span></span>" & CurrentPageData.GetCustomString("lnkMyFavoriteList")
            'lnkMyBlockedList.Text = "<span></span>" & CurrentPageData.GetCustomString("lnkMyBlockedList")
            'lnkMyViewedList.Text = "<span></span>" & CurrentPageData.GetCustomString("lnkMyViewedList")

            cbSort.Items.Clear()
            cbSort.Items.Add(CurrentPageData.GetCustomString("cbSort_Recent"), "Recent")
            cbSort.Items(cbSort.Items.Count - 1).Selected = True
            cbSort.Items.Add(CurrentPageData.GetCustomString("cbSort_Oldest"), "Oldest")

            Try
                If (Not Page.IsPostBack) Then
                    Me.MyListsSorting = MyListsSortEnum.Recent

                    Dim req As String = Request.QueryString("sorting")
                    Dim _cbSort As Integer
                    If (Not String.IsNullOrEmpty(req)) Then
                        Integer.TryParse(req, _cbSort)
                        If (_cbSort >= 0 AndAlso _cbSort < cbSort.Items.Count) Then
                            cbSort.SelectedIndex = _cbSort
                        End If
                    End If

                End If
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try

            lblSortByText.Text = CurrentPageData.GetCustomString("lblSortByText")
            lblResultsPerPageText.Text = CurrentPageData.GetCustomString("lblResultsPerPageText")

            lblItemsName.Text = Me.CurrentPageData.GetCustomString("CartMyListsView_SharedPhotosWithWhom")
            lnkViewDescription.Text = Me.CurrentPageData.GetCustomString("CartMyListsWhatIsIt_SharedPhotosWithWhom")
            WhatIsIt1.PopupControl.HeaderText = Me.CurrentPageData.GetCustomString("CartMyListsView_SharedPhotosWithWhom")
            lnkViewDescription.OnClientClick = OnMoreInfoClickFunc(
                lnkViewDescription.OnClientClick,
                ResolveUrl("~/Members/InfoWin.aspx?info=mylists"),
                Me.CurrentPageData.GetCustomString("CartMyListsView"))


        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Private Sub LoadViews()

        Try

            Me.MyListsSorting = MyListsSortEnum.Recent
            Me.ItemsPerPage = 10
            BindSearchResults()

            'If (Request.QueryString("vw") IsNot Nothing) Then
            '    If (Request.QueryString("vw").ToUpper() = "WHOFAVORITEDME") Then
            '        mvMyListsMain.SetActiveView(vwWhoFavoritedMe)

            '    ElseIf (Request.QueryString("vw").ToUpper() = "MYFAVORITELIST") Then
            '        mvMyListsMain.SetActiveView(vwMyFavoriteList)

            '    ElseIf (Request.QueryString("vw").ToUpper() = "WHOSHAREDPHOTOS") Then
            '        mvMyListsMain.SetActiveView(vwWhoSharedPhoto)

            '    ElseIf (Request.QueryString("vw").ToUpper() = "MYBLOCKEDLIST") Then
            '        mvMyListsMain.SetActiveView(vwMyBlockedList)

            '    ElseIf (Request.QueryString("vw").ToUpper() = "MYVIEWEDLIST") Then
            '        mvMyListsMain.SetActiveView(vwMyViewedList)

            '    ElseIf (Request.QueryString("vw").ToUpper() = "WHOVIEWEDME") Then
            '        mvMyListsMain.SetActiveView(vwWhoViewedMe)

            '    End If
            'Else
            '    mvMyListsMain.SetActiveView(vwWhoViewedMe)
            'End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


    Protected Sub offersSearchRepeater_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs)
        Dim success = False
        Try
            'Dim drDefaultPhoto As EUS_CustomerPhotosRow = DataHelpers.GetProfilesDefaultPhoto(Me.MasterProfileId)
            Dim currentUserHasPhotos As Boolean = clsUserDoes.HasPhotos(Me.MasterProfileId, Me.MirrorProfileId)

            If (currentUserHasPhotos) Then
                If (e.CommandName = OfferControlCommandEnum.WINK.ToString()) Then

                    If (Not String.IsNullOrEmpty(e.CommandArgument)) Then

                        Try
                            Dim ToProfileID As Integer
                            Integer.TryParse(e.CommandArgument.ToString(), ToProfileID)

                            Dim MaySendWink As Boolean = True
                            Dim MaySendWink_CheckSetting As Boolean = True


                            MaySendWink_CheckSetting = Not clsProfilesPrivacySettings.GET_DisableReceivingLIKESFromDifferentCountryFromTo(ToProfileID, Me.MasterProfileId)
                            If (MaySendWink_CheckSetting = False) Then
                                MaySendWink = False
                            End If


                            If (MaySendWink AndAlso Me.IsFemale) Then
                                If (Me.IsReferrer) Then
                                    MaySendWink = clsUserDoes.IsReferrerAllowedSendWink(ToProfileID, Me.MasterProfileId)
                                End If
                            End If

                            If (MaySendWink) Then
                                clsUserDoes.SendWink(ToProfileID, Me.MasterProfileId)
                            Else
                                If (MaySendWink_CheckSetting = False) Then
                                    Dim popupMessage As String = WhatIsIt.OnMoreInfoClickFunc(ResolveUrl("~/Members/InfoWin.aspx?info=Error_Sending_Receiver_Disabled_Likes_From_Diff_Country&popup=popupWhatIs"), "Error! Can't send", 650, 350)
                                    popupMessage = "function fnError_Sending_Receiver_DoNot_Have_Photo(){" & popupMessage & "};"
                                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Error_Sending_Receiver_DoNot_Have_Photo", popupMessage, True)
                                Else
                                    Dim popupMessage As String = WhatIsIt.OnMoreInfoClickFunc(ResolveUrl("~/Members/InfoWin.aspx?info=Error_Sending_Receiver_DoNot_Have_Photo&popup=popupWhatIs"), "Error! Can't send", 650, 350)
                                    popupMessage = "function fnError_Sending_Receiver_DoNot_Have_Photo(){" & popupMessage & "};"
                                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Error_Sending_Receiver_DoNot_Have_Photo", popupMessage, True)
                                End If
                            End If
                            success = MaySendWink

                        Catch ex As Exception
                            WebErrorMessageBox(Me, ex, "")
                        End Try

                    End If

                ElseIf (e.CommandName = OfferControlCommandEnum.FAVORITE.ToString()) Then

                    Dim _userId As Integer = CType(e.CommandArgument, Integer)
                    If (_userId > 0) Then
                        Try
                            clsUserDoes.MarkAsFavorite(_userId, Me.MasterProfileId)
                            success = True
                        Catch ex As Exception
                            WebErrorMessageBox(Me, ex, "")
                        End Try
                    End If

                ElseIf (e.CommandName = OfferControlCommandEnum.UNFAVORITE.ToString()) Then

                    Dim _userId As Integer = CType(e.CommandArgument, Integer)
                    If (_userId > 0) Then
                        Try
                            clsUserDoes.MarkAsUnfavorite(_userId, Me.MasterProfileId)

                            success = True
                        Catch ex As Exception
                            WebErrorMessageBox(Me, ex, "")
                        End Try
                    End If


                ElseIf (e.CommandName = OfferControlCommandEnum.UNBLOCK.ToString()) Then

                    Dim _userId As Integer = CType(e.CommandArgument, Integer)
                    If (_userId > 0) Then
                        Try
                            clsUserDoes.MarkAsUnblocked(_userId, Me.MasterProfileId)
                            success = True
                        Catch ex As Exception
                            WebErrorMessageBox(Me, ex, "")
                        End Try
                    End If

                End If
            Else
                Response.Redirect("~/Members/Photos.aspx", False)
                HttpContext.Current.ApplicationInstance.CompleteRequest()
            End If



        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try


        Try
            If (success) Then
                BindSearchResults()
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub



    'Protected Sub mvMyListsMain_ActiveViewChanged(sender As Object, e As EventArgs) Handles mvMyListsMain.ActiveViewChanged
    '    BindSearchResults()
    'End Sub



    Protected Sub Pager_PageIndexChanged(sender As Object, e As EventArgs) Handles Pager.PageIndexChanged
        BindSearchResults()
    End Sub


    Protected Sub ddlSort_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbSort.SelectedIndexChanged
        Me.Pager.PageIndex = 0
        BindSearchResults()
    End Sub

    Protected Sub ddlPerPage_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbPerPage.SelectedIndexChanged
        'Me.Pager.PageIndex = 0
        'BindSearchResults()
        Try
            clsProfilesPrivacySettings.Update_ItemsPerPage(Me.MasterProfileId, Me.ItemsPerPage)
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorSendEmail(ex, "")
        End Try
        Try
            Me.Pager.PageIndex = 0
            BindSearchResults()
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(ex, "")
        End Try
    End Sub


    Sub SetPager(itemsCount As Integer)
        Me.Pager.ItemCount = itemsCount
        Me.Pager.ItemsPerPage = Me.ItemsPerPage
        Me.Pager.Visible = (itemsCount > Me.ItemsPerPage)

        If (Me.Pager.PageIndex = -1) AndAlso (itemsCount > 0) Then
            Me.Pager.PageIndex = 0
        End If
    End Sub

    Function GetRecordsToRetrieve() As Integer
        Me.Pager.ItemsPerPage = Me.ItemsPerPage

        If (Me.Pager.PageIndex = -1) AndAlso (Me.Pager.ItemCount > 0) Then
            Me.Pager.PageIndex = 0
        End If

        If (Me.Pager.PageIndex > 0) Then
            Return (Me.Pager.ItemsPerPage * (Me.Pager.PageIndex + 1))
        End If
        Return Me.Pager.ItemsPerPage
    End Function


    Private Sub BindSearchResults()

        'If (mvMyListsMain.GetActiveView().Equals(vwWhoViewedMe)) Then

        '    liWhoViewedMe.Attributes.Add("class", "down")
        '    liWhoFavoritedMe.Attributes.Remove("class")
        '    liMyFavoriteList.Attributes.Remove("class")
        '    liMyBlockedList.Attributes.Remove("class")
        '    liMyViewedList.Attributes.Remove("class")
        '    liWhoSharedPhotos.Attributes.Remove("class")

        '    whoViewedMeList.InfoWinHeaderText = CurrentPageData.GetCustomString("lnkWhoViewedMe")
        '    whoViewedMeList.InfoWinUrl = ResolveUrl("~/Members/InfoWin.aspx?info=WhoViewedMeCommands")

        '    BindWhoViewedMe()

        'ElseIf (mvMyListsMain.GetActiveView().Equals(vwWhoFavoritedMe)) Then

        '    liWhoFavoritedMe.Attributes.Add("class", "down")
        '    liWhoViewedMe.Attributes.Remove("class")
        '    liMyFavoriteList.Attributes.Remove("class")
        '    liMyBlockedList.Attributes.Remove("class")
        '    liMyViewedList.Attributes.Remove("class")
        '    liWhoSharedPhotos.Attributes.Remove("class")

        '    whoFavoritedMeList.InfoWinHeaderText = CurrentPageData.GetCustomString("lnkWhoFavoritedMe")
        '    whoFavoritedMeList.InfoWinUrl = ResolveUrl("~/Members/InfoWin.aspx?info=WhoFavoritedMeCommands")

        '    BindWhoFavoritedMe()

        'ElseIf (mvMyListsMain.GetActiveView().Equals(vwWhoSharedPhoto)) Then


        'liWhoSharedPhotos.Attributes.Add("class", "down")
        'liMyFavoriteList.Attributes.Remove("class")
        'liWhoFavoritedMe.Attributes.Remove("class")
        'liWhoViewedMe.Attributes.Remove("class")
        'liMyBlockedList.Attributes.Remove("class")
        'liMyViewedList.Attributes.Remove("class")

        sharedPhotosByMeList.InfoWinHeaderText = CurrentPageData.GetCustomString("lnkSharedPhotosWithWhom")
        sharedPhotosByMeList.InfoWinUrl = ResolveUrl("~/Members/InfoWin.aspx?info=sharedphotosbyme")

        BindSharedPhotosByMeList()

        'ElseIf (mvMyListsMain.GetActiveView().Equals(vwMyFavoriteList)) Then

        '    liMyFavoriteList.Attributes.Add("class", "down")
        '    liWhoFavoritedMe.Attributes.Remove("class")
        '    liWhoViewedMe.Attributes.Remove("class")
        '    liMyBlockedList.Attributes.Remove("class")
        '    liMyViewedList.Attributes.Remove("class")
        '    liWhoSharedPhotos.Attributes.Remove("class")

        '    myFavoriteList.InfoWinHeaderText = CurrentPageData.GetCustomString("lnkMyFavoriteList")
        '    myFavoriteList.InfoWinUrl = ResolveUrl("~/Members/InfoWin.aspx?info=MyFavoriteListCommands")

        '    BindMyFavoriteList()

        'ElseIf (mvMyListsMain.GetActiveView().Equals(vwMyBlockedList)) Then

        '    liMyBlockedList.Attributes.Add("class", "down")
        '    liWhoFavoritedMe.Attributes.Remove("class")
        '    liMyFavoriteList.Attributes.Remove("class")
        '    liWhoViewedMe.Attributes.Remove("class")
        '    liMyViewedList.Attributes.Remove("class")
        '    liWhoSharedPhotos.Attributes.Remove("class")

        '    myBlockedList.InfoWinHeaderText = CurrentPageData.GetCustomString("lnkMyBlockedList")
        '    myBlockedList.InfoWinUrl = ResolveUrl("~/Members/InfoWin.aspx?info=MyBlockedListCommands")

        '    BindMyBlockedList()

        'ElseIf (mvMyListsMain.GetActiveView().Equals(vwMyViewedList)) Then

        '    liMyViewedList.Attributes.Add("class", "down")
        '    liWhoFavoritedMe.Attributes.Remove("class")
        '    liMyFavoriteList.Attributes.Remove("class")
        '    liMyBlockedList.Attributes.Remove("class")
        '    liWhoViewedMe.Attributes.Remove("class")
        '    liWhoSharedPhotos.Attributes.Remove("class")

        '    myViewedList.InfoWinHeaderText = CurrentPageData.GetCustomString("lnkMyViewedList")
        '    myViewedList.InfoWinUrl = ResolveUrl("~/Members/InfoWin.aspx?info=MyViewedListCommands")

        '    BindMyViewedList()

        'End If

    End Sub


    Private Sub BindSharedPhotosByMeList()
        Try

            divFilter.Visible = False
            sharedPhotosByMeList.ShowEmptyListText = True

            If (Me.GetCurrentProfile(True) IsNot Nothing) Then


                Try
                    'Dim dt As DataTable = clsSearchHelper.GetSharedPhotosByMeToMembers_DataTable(Me.MyListsSorting,
                    '                                                                        Me.MasterProfileId, 0,
                    '                                                                        ProfileStatusEnum.Approved,
                    '                                                                        Me.SessionVariables.MemberData.Zip,
                    '                                                                        Me.SessionVariables.MemberData.latitude,
                    '                                                                        Me.SessionVariables.MemberData.longitude)

                    'If (dt.Rows.Count > 0) Then
                    '    FillOfferControlList(dt, sharedPhotosByMeList)
                    '    sharedPhotosByMeList.ShowEmptyListText = False
                    '    divFilter.Visible = True
                    'End If


                    Dim countRows As Integer
                    Dim performCount As Boolean = True
                    Dim NumberOfRecordsToReturn As Integer = Me.GetRecordsToRetrieve()

                    Dim parms As New clsMyListsHelperParameters()
                    parms.CurrentProfileId = Me.MasterProfileId
                    parms.sorting = Me.MyListsSorting
                    parms.ReturnRecordsWithStatus = ProfileStatusEnum.Approved
                    parms.zipstr = Me.SessionVariables.MemberData.Zip
                    parms.latitudeIn = Me.SessionVariables.MemberData.latitude
                    parms.longitudeIn = Me.SessionVariables.MemberData.longitude
                    parms.Distance = clsSearchHelper.DISTANCE_DEFAULT
                    'parms.NumberOfRecordsToReturn = NumberOfRecordsToReturn
                    parms.performCount = True
                    parms.rowNumberMin = NumberOfRecordsToReturn - Me.ItemsPerPage
                    parms.rowNumberMax = NumberOfRecordsToReturn

                    Dim ds As DataSet = clsSearchHelper.GetSharedPhotosByMeToMembers_DataTable(parms)

                    If (performCount) Then
                        countRows = ds.Tables(0).Rows(0)(0)
                        If (countRows > 0) Then
                            Dim dt As DataTable = ds.Tables(1)
                            FillOfferControlList(dt, sharedPhotosByMeList)
                            sharedPhotosByMeList.ShowEmptyListText = False
                            divFilter.Visible = True
                        End If
                    Else
                        countRows = ds.Tables(0).Rows.Count
                        If (countRows > 0) Then
                            Dim dt As DataTable = ds.Tables(0)
                            FillOfferControlList(dt, sharedPhotosByMeList)
                            sharedPhotosByMeList.ShowEmptyListText = False
                            divFilter.Visible = True
                        End If
                    End If

                    sharedPhotosByMeList.DataBind()
                    SetPager(countRows)

                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "")

                    SetPager(0)
                    sharedPhotosByMeList.UsersList = Nothing
                    sharedPhotosByMeList.DataBind()
                End Try

            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Private Sub FillOfferControlList(dt As DataTable, offersControl As Dating.Server.Site.Web.SearchControl)
        '' set pagination
        'Dim startingIndex As Integer = 0
        'Dim endIndex As Integer = Me.ItemsPerPage - 1
        'If (Me.Pager.PageIndex > 0) Then
        '    startingIndex = Me.ItemsPerPage * Me.Pager.PageIndex
        '    endIndex = (startingIndex + Me.ItemsPerPage) - 1
        'End If


        'Dim _profileRows As New clsProfileRows(Me.MasterProfileId)
        'Dim masterRow As EUS_ProfilesRow = _profileRows.GetMasterRow()

        Dim drDefaultPhoto As EUS_CustomerPhotosRow = DataHelpers.GetProfilesDefaultPhoto(Me.MasterProfileId)
        Dim uliCurrentMember As New clsWinkUserListItem()
        uliCurrentMember.ProfileID = Me.MasterProfileId ' 
        uliCurrentMember.MirrorProfileID = Me.MirrorProfileId
        uliCurrentMember.LoginName = Me.GetCurrentProfile().LoginName
        uliCurrentMember.Genderid = Me.GetCurrentProfile().GenderId
        uliCurrentMember.ProfileViewUrl = ResolveUrl("~/Members/Profile.aspx?p=" & HttpUtility.UrlEncode(Me.GetCurrentProfile().LoginName))

        'If (drDefaultPhoto IsNot Nothing) Then
        '    uliCurrentMember.ImageFileName = drDefaultPhoto.FileName
        '    uliCurrentMember.ImageUrl = ProfileHelper.GetProfileImageURL(drDefaultPhoto.CustomerID, drDefaultPhoto.FileName, Me.GetCurrentProfile().GenderId, True, Me.IsHTTPS)
        '    uliCurrentMember.ImageThumbUrl = ProfileHelper.GetProfileImageURL(drDefaultPhoto.CustomerID, drDefaultPhoto.FileName, Me.GetCurrentProfile().GenderId, True, Me.IsHTTPS)
        '    uliCurrentMember.ImageUploadDate = drDefaultPhoto.DateTimeToUploading.ToLocalTime().ToString("d MMM, yyyy H:mm")
        'Else
        '    uliCurrentMember.ImageUrl = ProfileHelper.GetDefaultImageURL(Me.GetCurrentProfile().GenderId)
        '    uliCurrentMember.ImageThumbUrl = uliCurrentMember.ImageUrl
        '    uliCurrentMember.ImageFileName = System.IO.Path.GetFileName(uliCurrentMember.ImageUrl)
        '    uliCurrentMember.ImageUploadDate = Nothing
        'End If

        If (drDefaultPhoto IsNot Nothing) Then
            uliCurrentMember.ImageFileName = drDefaultPhoto.FileName
            uliCurrentMember.ImageUploadDate = drDefaultPhoto.DateTimeToUploading.ToLocalTime().ToString("d MMM, yyyy H:mm")
        Else
            uliCurrentMember.ImageUploadDate = Nothing
            uliCurrentMember.ImageFileName = ""
        End If

        uliCurrentMember.ImageUrl = ProfileHelper.GetProfileImageURL(Me.MasterProfileId, uliCurrentMember.ImageFileName, Me.SessionVariables.MemberData.GenderId, True, Me.IsHTTPS, PhotoSize.D150)
        uliCurrentMember.ImageThumbUrl = uliCurrentMember.ImageUrl




        'Dim rowsCount As Integer
        'For rowsCount = startingIndex To dt.Rows.Count - 1
        '    If (rowsCount > endIndex) Then Exit For
        '    If (rowsCount >= startingIndex AndAlso rowsCount <= endIndex) Then

        Dim rowsCount As Integer
        For rowsCount = 0 To dt.Rows.Count - 1
            Try
                Dim dr As DataRow = dt.Rows(rowsCount)

                Dim uli As New clsWinkUserListItem()
                uli.LAGID = Session("LAGID")
                uli.ProfileID = uliCurrentMember.ProfileID
                uli.MirrorProfileID = uliCurrentMember.MirrorProfileID
                uli.LoginName = uliCurrentMember.LoginName
                uli.Genderid = uliCurrentMember.Genderid
                uli.ProfileViewUrl = uliCurrentMember.ProfileViewUrl
                uli.ImageFileName = uliCurrentMember.ImageFileName
                uli.ImageUrl = uliCurrentMember.ImageUrl
                uli.ImageThumbUrl = uliCurrentMember.ImageThumbUrl
                uli.ImageUploadDate = uliCurrentMember.ImageUploadDate


                If (dt.Columns.Contains("Birthday")) Then uli.OtherMemberBirthday = If(Not dr.IsNull("Birthday"), dr("Birthday"), Nothing)
                uli.OtherMemberLoginName = dr("LoginName")
                uli.OtherMemberProfileID = dr("ProfileID")
                uli.OtherMemberMirrorProfileID = dr("MirrorProfileID")
                uli.OtherMemberCity = IIf(Not dr.IsNull("City"), dr("City"), String.Empty)
                uli.OtherMemberRegion = IIf(Not dr.IsNull("Region"), dr("Region"), String.Empty)
                uli.OtherMemberCountry = IIf(Not dr.IsNull("Country"), dr("Country"), "")
                uli.OtherMemberGenderid = dr("Genderid")
                'uli.OtherMemberHeading = IIf(Not dr.IsNull("AboutMe_Heading"), dr("AboutMe_Heading"), String.Empty)


                If (Not dr.IsNull("PersonalInfo_HeightID")) Then
                    uli.OtherMemberHeight = ProfileHelper.GetHeightString(dr("PersonalInfo_HeightID"), Me.Session("LagID"))
                End If


                If (Not dr.IsNull("PersonalInfo_BodyTypeID")) Then
                    uli.OtherMemberPersonType = ProfileHelper.GetBodyTypeString(dr("PersonalInfo_BodyTypeID"), Me.Session("LagID"))
                End If


                If (Not dr.IsNull("PersonalInfo_HairColorID")) Then
                    uli.OtherMemberHair = ProfileHelper.GetHairColorString(dr("PersonalInfo_HairColorID"), Me.Session("LagID"))
                End If


                If (Not dr.IsNull("PersonalInfo_EyeColorID")) Then
                    uli.OtherMemberEyes = ProfileHelper.GetEyeColorString(dr("PersonalInfo_EyeColorID"), Me.Session("LagID"))
                End If


                If (Not dr.IsNull("PersonalInfo_EthnicityID")) Then
                    uli.OtherMemberEthnicity = ProfileHelper.GetEthnicityString(dr("PersonalInfo_EthnicityID"), Me.Session("LagID"))
                End If


                If (Not dr.IsNull("Birthday")) Then
                    uli.OtherMemberAge = ProfileHelper.GetCurrentAge(dr("Birthday"))
                Else
                    uli.OtherMemberAge = 20
                End If


                If (Not dr.IsNull("FileName")) Then
                    uli.OtherMemberImageFileName = dr("FileName")
                End If
                'uli.OtherMemberImageUrl = ProfileHelper.GetProfileImageURL(dr("ProfileID"), uli.OtherMemberImageFileName, uli.OtherMemberGenderid, True, Me.IsHTTPS, PhotoSize.D150)
                If (dt.Columns.Contains("HasPhoto")) Then
                    If (Not dr.IsNull("HasPhoto") AndAlso (dr("HasPhoto").ToString() = "1" OrElse dr("HasPhoto").ToString() = "True")) Then
                        ' user has photo
                        If (uli.OtherMemberImageFileName IsNot Nothing) Then
                            'has public photos
                            uli.OtherMemberImageUrl = ProfileHelper.GetProfileImageURL(uli.OtherMemberProfileID, uli.OtherMemberImageFileName, uli.OtherMemberGenderid, True, Me.IsHTTPS, PhotoSize.D150)

                        Else
                            'has private photos
                            uli.OtherMemberImageUrl = ProfileHelper.GetPrivateImageURL(uli.OtherMemberGenderid)

                        End If
                    Else
                        ' has no photo
                        uli.OtherMemberImageUrl = ProfileHelper.GetDefaultImageURL(uli.OtherMemberGenderid)

                    End If
                    If IsHTTPS AndAlso uli.OtherMemberImageUrl.StartsWith("http:") Then uli.OtherMemberImageUrl = "https" & uli.OtherMemberImageUrl.Remove(0, "http".Length)
                Else
                    uli.OtherMemberImageUrl = ProfileHelper.GetProfileImageURL(uli.OtherMemberProfileID, uli.OtherMemberImageFileName, uli.OtherMemberGenderid, True, Me.IsHTTPS, PhotoSize.D150)
                End If
                uli.OtherMemberImageThumbUrl = uli.OtherMemberImageUrl


                uli.OtherMemberProfileViewUrl = ResolveUrl("~/Members/Profile.aspx?p=" & HttpUtility.UrlEncode(uli.OtherMemberLoginName))

                uli.Distance = dr("distance")

                If (uli.Distance < gCarDistance) Then
                    uli.DistanceCss = "distance_car"
                Else
                    uli.DistanceCss = "distance_plane"
                End If

                'If (mvMyListsMain.GetActiveView().Equals(vwMyFavoriteList)) Then

                '    uli.AllowUnfavorite = True
                '    'uli.AllowFavorite = False

                'ElseIf (mvMyListsMain.GetActiveView().Equals(vwMyBlockedList)) Then

                '    uli.AllowUnblock = True

                'Else

                If (dt.Columns.Contains("OfferID")) Then
                    uli.OfferID = IIf(Not dr.IsNull("OfferID"), dr("OfferID"), 0)
                    Dim OfferTypeID = IIf(Not dr.IsNull("OffersOfferTypeID"), dr("OffersOfferTypeID"), 0)
                    Dim OffersStatusID = IIf(Not dr.IsNull("OffersStatusID"), dr("OffersStatusID"), 0)
                    Dim OffersFromProfileID = IIf(Not dr.IsNull("OffersFromProfileID"), dr("OffersFromProfileID"), 0)
                    Dim OffersToProfileID = IIf(Not dr.IsNull("OffersToProfileID"), dr("OffersToProfileID"), 0)

                    uli.OfferAmount = IIf(Not dr.IsNull("OffersAmount"), dr("OffersAmount"), 0)

                    If (uli.OfferID = 0) Then
                        uli.AllowWink = True
                    End If

                ElseIf (dr("OffersCount") = 0) Then
                    uli.AllowWink = True
                End If

                'End If


                Dim handleLikeBtn As Boolean = False
                Dim handleFavoriteBtn As Boolean = False

                If (offersControl.Equals(sharedPhotosByMeList)) Then

                    handleLikeBtn = True

                End If


                If (offersControl.Equals(sharedPhotosByMeList)) Then

                    'handleFavoriteBtn = True
                    Dim allowFavorite As Boolean = True
                    If (dr("HasFavoritedMe") > 0 AndAlso dr("DidIFavorited") > 0) Then
                        ''''''''''''''''''''''''
                        ' both users has favorited, each other
                        ''''''''''''''''''''''''
                        allowFavorite = False

                    ElseIf (dr("HasFavoritedMe") = 0 AndAlso dr("DidIFavorited") > 0) Then
                        ''''''''''''''''''''''''
                        ' current user has favorited, but the other not
                        ''''''''''''''''''''''''

                        allowFavorite = False
                    ElseIf (dr("HasFavoritedMe") > 0 AndAlso dr("DidIFavorited") = 0) Then
                        ''''''''''''''''''''''''
                        ' current user has not favorited, but the other did
                        ''''''''''''''''''''''''

                        allowFavorite = True

                    Else
                        ''''''''''''''''''''''''
                        ' none user has favorited, the other user
                        ''''''''''''''''''''''''

                        allowFavorite = True
                    End If


                    uli.AllowFavorite = allowFavorite
                    uli.AllowUnfavorite = Not allowFavorite
                End If


                If (handleLikeBtn) Then
                    uli.AllowWink = True

                    Try
                        Dim rec As Boolean = clsUserDoes.IsAnyWinkOrIsAnyOffer(uli.OtherMemberProfileID, Me.MasterProfileId)
                        uli.AllowWink = (Not rec)
                    Catch ex As Exception

                    End Try

                    ' if like enabled and users have exchanged at least one message, then disable like
                    If (uli.AllowWink) Then
                        Try
                            Dim rec As Boolean = clsUserDoes.IsAnyMessageExchanged(uli.OtherMemberProfileID, Me.MasterProfileId)
                            uli.AllowWink = (Not rec)
                        Catch ex As Exception
                        End Try
                    End If

                    If (Not uli.AllowWink) Then uli.AllowUnWink = True
                End If





                uli.SendMessageUrl = ResolveUrl("~/Members/Conversation.aspx?p=" & HttpUtility.UrlEncode(uli.OtherMemberLoginName) & "&offerid=" & uli.OfferID)
                uli.SendMessageUrlOnce = ResolveUrl("~/Members/Conversation.aspx?send=once&p=" & HttpUtility.UrlEncode(uli.OtherMemberLoginName) & "&offerid=" & uli.OfferID)
                uli.SendMessageUrlMany = ResolveUrl("~/Members/Conversation.aspx?send=unl&p=" & HttpUtility.UrlEncode(uli.OtherMemberLoginName) & "&offerid=" & uli.OfferID)


                If (Me.IsMale AndAlso dr("CommunicationUnl") = 0) Then
                    Dim rec As EUS_Offer = clsUserDoes.GetLastOffer(uli.OtherMemberProfileID, Me.MasterProfileId)

                    If (rec Is Nothing) Then
                        uli.CreateOfferUrl = ResolveUrl("~/Members/CreateOffer.aspx?offerto=" & HttpUtility.UrlEncode(uli.OtherMemberLoginName))
                    End If

                    uli.AllowActionsMenu = True
                    uli.AllowActionsCreateOffer = True
                    uli.AllowActionsSendMessageOnce = True
                    uli.AllowActionsSendMessageMany = True
                Else
                    uli.AllowSendMessage = True
                End If



                uli.AllowTooltipPopupSearch = True


                If (Me.SessionVariables.MemberData.Status = ProfileStatusEnum.LIMITED) Then
                    uli.AllowUnblock = False
                    uli.AllowUnfavorite = False
                    uli.AllowFavorite = False
                    uli.AllowActionsMenu = False
                    uli.AllowSendMessage = False
                    uli.AllowWink = False
                    uli.AllowUnWink = False
                End If

                offersControl.UsersList.Add(uli)
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try

            'End If
            ''rowsCount += 1
        Next


    End Sub


End Class

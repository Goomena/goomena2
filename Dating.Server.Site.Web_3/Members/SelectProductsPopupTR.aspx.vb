﻿Imports Library.Public
Imports Dating.Server.Core.DLL


Public Class SelectProductsPopupTR
    Inherits BasePage



    Protected Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            'If (_pageData Is Nothing) Then _pageData = New clsPageData("control.UCSelectProductMembers", Context)
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData("control.UCSelectProductMembers", Context, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property


    Dim _control As String = ""
    Public Property control As String
        Get
            Return _control
        End Get
        Set(ByVal value As String)
            _control = value
        End Set
    End Property


    Protected Overrides Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        SetMasterPage(CurrentPageData)

        Try
            If clsCurrentContext.VerifyLogin() = True Then
                ' Me.MasterPageFile = "~/Members/Members2.Master"
                'If (Request("master") = "inner") Then
                '    Me.MasterPageFile = "~/Members/Members2Inner.Master"
                'Else
                'End If
            Else
                clsCurrentContext.ClearSession(Session)
            End If
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            Me.Form.Target = "_top"
            If (Not Me.IsPostBack) Then
                LoadLAG()

                If (Me.SessionVariables.MemberData.Country = "TR") Then
                    UCPrice1.CssClass = UCPrice1.CssClass & " tr"
                    UCPrice2.CssClass = UCPrice2.CssClass & " tr"
                    UCPrice3.CssClass = UCPrice3.CssClass & " tr"
                    payment_2.Attributes("class") = payment_2.Attributes("class") & " tr"
                Else
                    ' ok
                End If

                Dim currency As String = Me.GetCurrency()
                UCPrice1.SetPricing(clsPricing.GetPriceForDisplayIndex(1, currency))
                UCPrice2.SetPricing(clsPricing.GetPriceForDisplayIndex(2, currency))
                UCPrice3.SetPricing(clsPricing.GetPriceForDisplayIndex(3, currency))

            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub

    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender

        Try
            Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub




    Public Overrides Sub Master_LanguageChanged()

        If (Me.Visible) Then
            Me._pageData = Nothing
            LoadLAG()
        End If

    End Sub


    Protected Sub LoadLAG()
        Try

            Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic


            lblInfo.Text = CurrentPageData.GetCustomString("Select.Products.Popup.TR.Text")
            'lnkBack.Text = CurrentPageData.GetCustomString(lnkBack.ID)


            'lblItemsName.Text = Me.CurrentPageData.GetCustomString("CartSelectPaymentView")
            'lnkViewDescription.Text = Me.CurrentPageData.GetCustomString("CartSelectPaymentWhatIsIt")
            'lnkViewDescription.OnClientClick = OnMoreInfoClickFunc(
            '    lnkViewDescription.OnClientClick,
            '    ResolveUrl("~/Members/InfoWin.aspx?info=selectpayment"),
            '    Me.CurrentPageData.GetCustomString("CartSelectPaymentView"))
            'WhatIsIt1.PopupControl.HeaderText = Me.CurrentPageData.GetCustomString("CartSelectPaymentView")

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Private Sub img30Days_Command(ByVal sender As Object, ByVal e As PriceSelectedEventArgs) Handles UCPrice1.PriceSelected, UCPrice2.PriceSelected, UCPrice3.PriceSelected
        Dim amount As String = ""
        Dim days As String = ""
        Dim credits As String = ""

        'credits = e.CommandArgument.ToString.Split(",")(0)
        'days = e.CommandArgument.ToString.Split(",")(1)
        'amount = e.CommandArgument.ToString.Split(",")(2)
        credits = e.Credits
        days = e.Duration
        amount = e.Price

        Session("credits") = credits.Replace("Credits", "")
        Session("purDesc") = days
        Session("amount") = amount

        Dim product As String = credits
        Dim url As String = ResolveUrl("~/Members/selectPayment.aspx?choice=" & product)
        If (Me.SessionVariables.MemberData.Country = "TR") Then
            url = ResolveUrl("~/Members/selectPaymentTR.aspx?choice=" & product)
        End If


        If _control = "new" Then
            url = HttpUtility.UrlEncode(url)
            Dim registerUrl As String = ResolveUrl("~/register.aspx?ReturnUrl=" & url)
            Response.Redirect(registerUrl)

        ElseIf Me.SessionVariables.MemberData Is Nothing OrElse Me.Session("ProfileID") = 0 Then
            url = HttpUtility.UrlEncode(url)
            Dim loginUrl As String = ResolveUrl("~/login.aspx?ReturnUrl=" & url)
            Response.Redirect(loginUrl)
            'Response.Redirect("http://" & gSiteName & "/login.aspx")

        Else
            'Response.Redirect("selectPayment2.aspx?choice=" & product)
            Response.Redirect(url)

        End If

    End Sub



End Class
﻿Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Core.DLL
Imports System.Web.Script.Serialization
Imports System.IO
Imports System.Web.Script.Services

Public Class JSON
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If (Request("option") IsNot Nothing AndAlso
                Request("option").ToLower() = "getjobslist") Then
                Dim result As String = GetJobsList(Request("term"))
                Response.Clear()
                Response.Write(result)
                Response.End()
            End If

        Catch ex As System.Threading.ThreadAbortException
        End Try
    End Sub

    <System.Web.Services.WebMethod()> _
    Public Shared Sub SetOffsetTime(ByVal time As String)
        Dim TimeOffset As Double
        If (time.Trim() <> "" AndAlso
            Double.TryParse(time, TimeOffset) AndAlso
            HttpContext.Current.Session("TimeOffset") <> TimeOffset) Then

            HttpContext.Current.Session("TimeOffset") = TimeOffset

        End If
    End Sub


    <System.Web.Services.WebMethod(enablesession:=True)> _
    Public Shared Sub SetIsMobile(ByVal isMobile As String)
        If (Not String.IsNullOrEmpty(isMobile)) Then
            isMobile = isMobile.ToUpper()
            Dim Session As HttpSessionState = HttpContext.Current.Session

            If (isMobile = Boolean.TrueString.ToUpper()) Then
                Session("IsMobileAccess") = True

                Dim MasterProfileId As Integer = Session("ProfileID")
                UpdateEUS_Profiles_LoginData(MasterProfileId, True, True)

                If (Session("MobileAccessIDList") IsNot Nothing) Then
                    Dim MobileAccessIDList As List(Of Long) = Session("MobileAccessIDList")
                    DataHelpers.SYS_ProfilesAccess_Update_IsMobile(MobileAccessIDList)
                    Session("MobileAccessIDList") = Nothing
                End If

            ElseIf (isMobile = Boolean.FalseString.ToUpper()) Then
                Session("IsMobileAccess") = False
            End If

        End If
    End Sub

    <System.Web.Services.WebMethod()> _
    Public Shared Sub SuppressWarning_WhenReadingMessageFromDifferentCountry(ByVal SuppressWarning)
        Try

            Dim ProfileID As Integer = HttpContext.Current.Session("ProfileID")
            clsProfilesPrivacySettings.Update_SuppressWarning_WhenReadingMessageFromDifferentCountry(ProfileID, SuppressWarning)

        Catch ex As Exception
            WebErrorSendEmail(ex, ex.Message)
        End Try
    End Sub



    <System.Web.Services.WebMethod()> _
    Public Shared Function loadMemberActionCounters(ByVal data As String) As String
        Try
            Dim session As System.Web.SessionState.HttpSessionState = HttpContext.Current.Session
            Dim MasterProfileId As Integer = session("ProfileID")
            Dim leftPanelCounters As clsGetMemberActionsCounters = clsUserDoes.GetMemberActionsCounters(MasterProfileId)
            Dim counters As New MemberTotalActionCounters

            Dim AvailableCredits As Integer
            Dim __cac As clsCustomerAvailableCredits = BasePage.GetCustomerAvailableCredits(MasterProfileId, True)
            If (__cac.CreditsRecordsCount > 0) Then

                AvailableCredits = __cac.AvailableCredits
                'lnkCredits.Visible = True
                'lnkCredits.Text = CurrentPageData.GetCustomString("lnkCredits")
                If (__cac.AvailableCredits > 0) Then
                    'lnkCredits.Text = textPattern.Replace("#TEXT#", lnkCredits.Text).Replace("#COUNT#", __CustomerAvailableCredits.AvailableCredits)
                End If
                'lnkCredits.Attributes.Add("onclick", "ShowLoading();")
            Else
                'lnkCredits.Visible = False
                AvailableCredits = -1
            End If

            counters.HasSubscription = __cac.HasSubscription
            counters.CreditsCounter = AvailableCredits
            counters.WhoViewedMeCounter = leftPanelCounters.CountWhoViewedMe
            counters.WhoFavoritedMeCounter = leftPanelCounters.CountWhoFavoritedMe
            counters.WhoSharedPhotosCounter = leftPanelCounters.CountWhoSharedPhotos


            'Dim sqlProc As String = "EXEC GetNewCounters2 @CurrentProfileId= " & MasterProfileId
            'Dim dt As DataTable = DataHelpers.GetDataTable(sqlProc)
            'If (dt.Rows.Count > 0) Then
            '    If (dt.Rows(0)("NewDates2") > 0) Then counters.NewDates = dt.Rows(0)("NewDates2")
            '    If (dt.Rows(0)("NewWinks") > 0) Then counters.NewWinks = dt.Rows(0)("NewWinks")
            '    If (dt.Rows(0)("NewOffers") > 0) Then counters.NewOffers = dt.Rows(0)("NewOffers")
            '    If (dt.Rows(0)("NewMessages") > 0) Then counters.NewMessages = dt.Rows(0)("NewMessages")
            'End If

            Dim result As Dating.Server.Core.DLL.clsGetMemberActionsCounters =
                clsUserDoes.GetMemberActionsCounters(MasterProfileId)

            counters.NewDates = result.NewDates
            counters.NewWinks = result.NewLikes
            counters.NewOffers = result.NewOffers
            counters.NewMessages = result.NewMessages


            Dim serializer As New JavaScriptSerializer()
            Return serializer.Serialize(counters)

        Catch ex As Exception
            'WebErrorMessageBox(Me, ex, "")
            Return False
        End Try
    End Function




    <System.Web.Services.WebMethod()> _
    Public Shared Function GetMessagesList() As String
        Dim result As String = ""
        Try
            Dim ProfileID As Integer = HttpContext.Current.Session("ProfileID")
            If (ProfileID > 0) Then

                Dim writer As New StringWriter()
                HttpContext.Current.Server.Execute("~/Members/jsonMessagesList.aspx", writer, False)
                result = writer.ToString()


                If (Not result.StartsWith("{""redirect""")) Then
                    Dim form_ndx = result.IndexOf("<form")
                    If (form_ndx > -1) Then
                        form_ndx = (result.IndexOf(">", form_ndx)) + 1
                        Dim form_close_ndx = result.IndexOf("</form>", form_ndx)
                        result = result.Substring(form_ndx, form_close_ndx - form_ndx)
                    End If

                    form_ndx = result.IndexOf("<input type=""hidden"" name=""__VIEWSTATE""")
                    If (form_ndx > -1) Then
                        Dim form_close_ndx = result.IndexOf(">", form_ndx) + 1
                        result = result.Remove(form_ndx, form_close_ndx - form_ndx)
                    End If
                Else
                    If (result.IndexOf(vbCrLf) > 0) Then
                        result = result.Remove(result.IndexOf(vbCrLf))
                    End If
                End If


            End If
        Catch ex As Exception
            WebErrorSendEmail(ex, ex.Message)
        End Try
        Return result
    End Function


    <System.Web.Services.WebMethod()> _
    Public Shared Function GetConversationMessagesList(otherProfile As String, rowFrom As String, rowTo As String, olderMsgIdAvailable As String, freeMessages As String) As String
        Dim result As String = ""
        result = GetConversationMessagesList2(otherProfile, rowFrom, rowTo, olderMsgIdAvailable, freeMessages, Nothing)
        Return result
    End Function


    <System.Web.Services.WebMethod()> _
    Public Shared Function GetConversationMessagesList2(otherProfile As String, rowFrom As String, rowTo As String, olderMsgIdAvailable As String, freeMessages As String, dateUntil As String) As String
        Dim result As String = ""
        Try
            Dim ProfileID As Integer = HttpContext.Current.Session("ProfileID")
            If (ProfileID > 0) Then

                If (otherProfile Is Nothing) Then otherProfile = ""
                If (rowFrom Is Nothing) Then rowFrom = ""
                If (rowTo Is Nothing) Then rowTo = ""
                If (olderMsgIdAvailable Is Nothing) Then olderMsgIdAvailable = ""
                If (dateUntil Is Nothing) Then dateUntil = ""

                Dim url As String = "~/Members/jsonConversationOlder.aspx" & "?vw=" & otherProfile & "&rowFrom=" & rowFrom & "&rowTo=" & rowTo & "&olderMsgIdAvailable=" & olderMsgIdAvailable & "&freeMessages=" & freeMessages & "&dateUntil=" & dateUntil
                Dim writer As New StringWriter()
                HttpContext.Current.Server.Execute(url, writer, False)
                result = writer.ToString()

                If (Not result.StartsWith("{""redirect""")) Then
                    Dim form_ndx = result.IndexOf("<form")
                    If (form_ndx > -1) Then
                        form_ndx = (result.IndexOf(">", form_ndx)) + 1
                        Dim form_close_ndx = result.IndexOf("</form>", form_ndx)
                        result = result.Substring(form_ndx, form_close_ndx - form_ndx)
                    End If

                    form_ndx = result.IndexOf("<input type=""hidden"" name=""__VIEWSTATE""")
                    If (form_ndx > -1) Then
                        Dim form_close_ndx = result.IndexOf(">", form_ndx) + 1
                        result = result.Remove(form_ndx, form_close_ndx - form_ndx)
                    End If
                Else
                    If (result.IndexOf(vbCrLf) > 0) Then
                        result = result.Remove(result.IndexOf(vbCrLf))
                    End If
                End If

            End If
        Catch ex As Exception
            WebErrorSendEmail(ex, ex.Message)
        End Try

        Return result
    End Function


    <System.Web.Services.WebMethod()> _
    Public Shared Function MessageUnlock2(otherProfile As String, read As String, unmsg As String, onemsg As String, free As String, subscription As String) As String
        Dim result As String = ""
        Try
            Dim ProfileID As Integer = HttpContext.Current.Session("ProfileID")
            If (ProfileID > 0) Then

                If (otherProfile Is Nothing) Then otherProfile = ""
                If (read Is Nothing) Then read = ""
                If (unmsg Is Nothing) Then unmsg = ""
                If (onemsg Is Nothing) Then onemsg = ""

                Dim url As String = "~/Members/jsonConversationUnlockMessage.aspx?free=" & free & "&read=" & read & "&onemsg=" & onemsg & "&vw=" & otherProfile & "&unmsg=" & unmsg & "&subscription=" & subscription
                Dim writer As New StringWriter()
                HttpContext.Current.Server.Execute(url, writer, False)
                result = writer.ToString()

                If (Not result.StartsWith("{""redirect""")) Then
                    Dim form_ndx = result.IndexOf("<form")
                    If (form_ndx > -1) Then
                        form_ndx = (result.IndexOf(">", form_ndx)) + 1
                        Dim form_close_ndx = result.IndexOf("</form>", form_ndx)
                        result = result.Substring(form_ndx, form_close_ndx - form_ndx)
                    End If

                    form_ndx = result.IndexOf("<input type=""hidden"" name=""__VIEWSTATE""")
                    If (form_ndx > -1) Then
                        Dim form_close_ndx = result.IndexOf(">", form_ndx) + 1
                        result = result.Remove(form_ndx, form_close_ndx - form_ndx)
                    End If
                Else
                    If (result.IndexOf(vbCrLf) > 0) Then
                        result = result.Remove(result.IndexOf(vbCrLf))
                    End If
                End If

            End If
        Catch ex As Exception
            WebErrorSendEmail(ex, ex.Message)
        End Try

        Return result
    End Function


    <System.Web.Services.WebMethod()> _
    Public Shared Function MessageUnlock(otherProfile As String, read As String, unmsg As String, onemsg As String, free As String) As String
        Dim result As String = ""
        result = MessageUnlock2(otherProfile, read, unmsg, onemsg, free, "")
        Return result
    End Function


    <System.Web.Services.WebMethod()> _
    Public Shared Function MessageDelete(otherProfile As String, msg As String, onemsg As String) As String
        Dim result As String = ""
        Try
            Dim ProfileID As Integer = HttpContext.Current.Session("ProfileID")
            If (ProfileID > 0) Then

                If (otherProfile Is Nothing) Then otherProfile = ""
                If (msg Is Nothing) Then msg = ""
                If (onemsg Is Nothing) Then onemsg = ""

                Dim MsgID As Integer
                Integer.TryParse(msg, MsgID)
                Dim prof As EUS_Profile = Nothing
                Dim currentprof As EUS_Profile = Nothing
                Dim ctx As New CMSDBDataContext(ModGlobals.ConnectionString)
                Try
                    currentprof = DataHelpers.GetEUS_ProfileByProfileID(ctx, ProfileID)
                    'prof = DataHelpers.GetEUS_ProfileMasterByLoginName(ctx, otherProfile, ProfileStatusEnum.Approved, ProfileStatusEnum.LIMITED)
                Catch ex As Exception
                    Throw
                Finally
                    ctx.Dispose()
                End Try

                If (currentprof IsNot Nothing) Then
                    If (clsNullable.NullTo(currentprof.ReferrerParentId) > 0) Then
                        clsUserDoes.DeleteMessage(ProfileID, MsgID, True)
                    Else
                        clsUserDoes.DeleteMessage(ProfileID, MsgID, False)
                    End If

                    Dim m As EUS_Message = clsUserDoes.GetMessage(MsgID)
                    If (m IsNot Nothing AndAlso m.IsDeleted = True) Then
                        result = "{""action"":""trash""}"
                    ElseIf (m Is Nothing) Then
                        result = "{""action"":""remove""}"
                    End If
                End If
            End If
        Catch ex As Exception
            WebErrorSendEmail(ex, ex.Message)
        End Try

        Return result
    End Function

    <System.Web.Services.WebMethod()> _
    Public Shared Function GetCredits() As String
        Dim result As String = ""
        Try
            Dim ProfileID As Integer = HttpContext.Current.Session("ProfileID")
            If (ProfileID > 0) Then
                Dim sesVars As clsSessionVariables = clsSessionVariables.GetCurrent()
                If (sesVars.MemberData.GenderId = 1) Then
                    Dim Session As System.Web.SessionState.HttpSessionState = HttpContext.Current.Session
                    Session("CustomerAvailableCredits") = Nothing
                    Dim _CustomerAvailableCredits As clsCustomerAvailableCredits = BasePage.GetCustomerAvailableCredits(ProfileID)
                    result = _CustomerAvailableCredits.AvailableCredits
                End If
            End If
        Catch ex As Exception
            WebErrorSendEmail(ex, ex.Message)
        End Try

        Return result
    End Function



    '<System.Web.Services.WebMethod()> _
    'Public Shared Function UserMessages_Check() As String
    '    '''''''''''''''
    '    '' check if any ready message available for user action made  recently
    '    '''''''''''''''
    '    Dim arrayJson As String = ""
    '    Try
    '        Dim ProfileID As Integer = HttpContext.Current.Session("ProfileID")
    '        If (ProfileID > 0) Then
    '            Dim sesVars As clsSessionVariables = clsSessionVariables.GetCurrent()
    '            If (sesVars.MemberData.GenderId = 1) Then
    '                Dim Session As System.Web.SessionState.HttpSessionState = HttpContext.Current.Session
    '                Dim dt As DataTable = clsMemberActionResponseHandler.GetReadyMessageForProfileID(ProfileID)
    '                arrayJson = AppUtils.DataTableToJSON(dt)
    '            End If
    '        End If
    '    Catch ex As Exception
    '        WebErrorSendEmail(ex, ex.Message)
    '    End Try

    '    Return arrayJson
    'End Function


    '<System.Web.Services.WebMethod()> _
    'Public Shared Function UserMessages_Read(itemid As String) As String
    '    '''''''''''''''
    '    '' check if any ready message available for user action made  recently
    '    '''''''''''''''
    '    Dim arrayJson As String = ""
    '    Try
    '        Dim QueueID As Long
    '        If (Not String.IsNullOrEmpty(itemid)) Then
    '            Long.TryParse(itemid, QueueID)
    '        End If

    '        Dim ProfileID As Integer = HttpContext.Current.Session("ProfileID")
    '        If (ProfileID > 0 AndAlso QueueID > 0) Then
    '            Dim sesVars As clsSessionVariables = clsSessionVariables.GetCurrent()
    '            If (sesVars.MemberData.GenderId = 1) Then
    '                Dim Session As System.Web.SessionState.HttpSessionState = HttpContext.Current.Session
    '                clsMemberActionResponseHandler.SetReadyMessageRead(QueueID, ProfileID)
    '            End If
    '        End If
    '    Catch ex As Exception
    '        WebErrorSendEmail(ex, ex.Message)
    '    End Try

    '    Return "OK"
    'End Function


    <System.Web.Services.WebMethod()> _
    Public Shared Function GetRandomPredefinedMessage() As String
        Dim result As String = ""
        Try
            Dim ProfileID As Integer = HttpContext.Current.Session("ProfileID")
            Dim LAGID As String = HttpContext.Current.Session("LAGID")
            If (ProfileID > 0) Then
                Dim sesVars As clsSessionVariables = clsSessionVariables.GetCurrent()
                If (sesVars.MemberData.GenderId = 1) Then
                    result = DataHelpers.GetRandomPredefinedMessage(2, LAGID)
                Else
                    result = DataHelpers.GetRandomPredefinedMessage(1, LAGID)
                End If
            End If
        Catch ex As Exception
            WebErrorSendEmail(ex, ex.Message)
        End Try

        Return result
    End Function


    <System.Web.Services.WebMethod()> _
    Public Shared Function GetAllPredefinedMessage() As String
        Dim arrayJson As String = ""
        Try
            Dim result As New DataTable()
            Dim ProfileID As Integer = HttpContext.Current.Session("ProfileID")
            Dim LAGID As String = HttpContext.Current.Session("LAGID")
            If (ProfileID > 0) Then
                Dim sesVars As clsSessionVariables = clsSessionVariables.GetCurrent()
                If (sesVars.MemberData.GenderId = 1) Then
                    result = DataHelpers.GetAllPredefinedMessages(2, LAGID)
                Else
                    result = DataHelpers.GetAllPredefinedMessages(1, LAGID)
                End If
            End If


            arrayJson = AppUtils.DataTableToJSON(result)

        Catch ex As Exception
            WebErrorSendEmail(ex, ex.Message)
        End Try

        Return arrayJson
    End Function


    <System.Web.Services.WebMethod()> _
    Public Shared Function WantVisitDelete(location_id As String) As String
        Dim arrayJson As String = ""
        Try
            Dim ProfileID As Integer = HttpContext.Current.Session("ProfileID")
            If (ProfileID > 0) Then

                Dim ctx As New CMSDBDataContext(ConnectionString)
                Try

                    ' check
                    Dim loc As EUS_ProfileLocation = (From itm In ctx.EUS_ProfileLocations
                                                     Where itm.ProfileID = ProfileID AndAlso itm.LocationID = location_id
                                                     Select itm).FirstOrDefault()

                    If (loc IsNot Nothing) Then
                        ctx.EUS_ProfileLocations.DeleteOnSubmit(loc)
                        ctx.SubmitChanges()
                    End If

                    Dim counter As Integer = (From itm In ctx.EUS_ProfileLocations
                                                Where itm.ProfileID = ProfileID
                                                Select itm).Count()

                    If (counter = 0) Then
                        DataHelpers.UpdateEUS_Profiles_AreYouWillingToTravel(ProfileID, False)
                    End If

                Catch ex As Exception
                    Throw
                Finally
                    ctx.Dispose()
                End Try

            End If

        Catch ex As Exception
            WebErrorSendEmail(ex, ex.Message)
        End Try

        Return arrayJson
    End Function


    <System.Web.Services.WebMethod()> _
    Public Shared Function WantVisitSave(location_id As String, locality_type As String,
                                         desc As String, ref As String,
                                         loc_country As String, loc_country_short As String,
                                         loc_region As String, loc_subregion As String,
                                         loc_city As String, loc_subcity As String) As String
        Dim arrayJson As String = ""
        Try
            Dim ProfileID As Integer = HttpContext.Current.Session("ProfileID")
            If (ProfileID > 0) Then

                Dim ctx As New CMSDBDataContext(ConnectionString)
                Try

                    ' check
                    Dim loc As EUS_ProfileLocation = (From itm In ctx.EUS_ProfileLocations
                                                     Where itm.ProfileID = ProfileID AndAlso itm.LocationID = location_id
                                                     Select itm).FirstOrDefault()

                    If (loc Is Nothing) Then
                        loc = New EUS_ProfileLocation()
                        loc.LocationID = location_id
                        loc.LocalityType = locality_type
                        loc.Descr = desc
                        loc.Ref = ref
                        loc.Loc_country = loc_country
                        loc.Loc_country_short = loc_country_short
                        loc.Loc_region = loc_region
                        loc.Loc_subregion = loc_subregion
                        loc.Loc_city = loc_city
                        loc.Loc_subcity = loc_subcity
                        loc.ProfileID = ProfileID

                        ctx.EUS_ProfileLocations.InsertOnSubmit(loc)
                        ctx.SubmitChanges()
                    End If
                    Dim counter As Integer = (From itm In ctx.EUS_ProfileLocations
                                                     Where itm.ProfileID = ProfileID
                                                     Select itm).Count()

                    If (counter > 0) Then
                        DataHelpers.UpdateEUS_Profiles_AreYouWillingToTravel(ProfileID, True)
                    End If
                Catch ex As Exception
                    Throw
                Finally
                    ctx.Dispose()
                End Try

            End If

        Catch ex As Exception
            WebErrorSendEmail(ex, ex.Message)
        End Try

        Return arrayJson
    End Function



    <System.Web.Services.WebMethod()> _
    Public Shared Function GetJobsList(term As String) As String
        Dim arrayJson As String = ""
        Try
            Dim ProfileID As Integer = HttpContext.Current.Session("ProfileID")
            Dim LAGID As String = HttpContext.Current.Session("LAGID")
            If (ProfileID > 0 AndAlso Not String.IsNullOrEmpty(term)) Then

                Dim ctx As New CMSDBDataContext(ConnectionString)
                Try
                    term = term.ToLower()
                    ' check
                    Dim loc As New List(Of EUS_LISTS_Job)()
                    Select Case LAGID
                        Case "GR"
                            loc = (From itm In ctx.EUS_LISTS_Jobs
                            Where itm.GR.ToLower().Contains(term) OrElse itm.US.ToLower().Contains(term)
                            Select itm).ToList()
                        Case "ES"
                            loc = (From itm In ctx.EUS_LISTS_Jobs
                            Where itm.ES.ToLower().Contains(term) OrElse itm.US.ToLower().Contains(term)
                            Select itm).ToList()
                        Case "TR"
                            loc = (From itm In ctx.EUS_LISTS_Jobs
                            Where itm.TR.ToLower().Contains(term) OrElse itm.US.ToLower().Contains(term)
                            Select itm).ToList()
                        Case "HU"
                            loc = (From itm In ctx.EUS_LISTS_Jobs
                            Where itm.HU.ToLower().Contains(term) OrElse itm.US.ToLower().Contains(term)
                            Select itm).ToList()
                        Case "DE"
                            loc = (From itm In ctx.EUS_LISTS_Jobs
                            Where itm.DE.ToLower().Contains(term) OrElse itm.US.ToLower().Contains(term)
                            Select itm).ToList()
                        Case "AL"
                            loc = (From itm In ctx.EUS_LISTS_Jobs
                            Where itm.AL.ToLower().Contains(term) OrElse itm.US.ToLower().Contains(term)
                            Select itm).ToList()
                        Case "SR"
                            loc = (From itm In ctx.EUS_LISTS_Jobs
                            Where itm.SR.ToLower().Contains(term) OrElse itm.US.ToLower().Contains(term)
                            Select itm).ToList()
                        Case "RU"
                            loc = (From itm In ctx.EUS_LISTS_Jobs
                            Where itm.RU.ToLower().Contains(term) OrElse itm.US.ToLower().Contains(term)
                            Select itm).ToList()
                        Case "RO"
                            loc = (From itm In ctx.EUS_LISTS_Jobs
                            Where itm.RO.ToLower().Contains(term) OrElse itm.US.ToLower().Contains(term)
                            Select itm).ToList()
                        Case "BG"
                            loc = (From itm In ctx.EUS_LISTS_Jobs
                            Where itm.BG.ToLower().Contains(term) OrElse itm.US.ToLower().Contains(term)
                            Select itm).ToList()
                        Case "FR"
                            loc = (From itm In ctx.EUS_LISTS_Jobs
                            Where itm.FR.ToLower().Contains(term) OrElse itm.US.ToLower().Contains(term)
                            Select itm).ToList()
                        Case "IT"
                            loc = (From itm In ctx.EUS_LISTS_Jobs
                            Where itm.IT.ToLower().Contains(term) OrElse itm.US.ToLower().Contains(term)
                            Select itm).ToList()

                    End Select

                    If (loc.Count = 0) Then
                        loc = (From itm In ctx.EUS_LISTS_Jobs
                        Where itm.US.ToLower().Contains(term)
                        Select itm).ToList()
                    End If

                    If (loc.Count > 0) Then
                        Dim sb As New StringBuilder()
                        sb.Append("[")
                        For c = 0 To loc.Count - 1
                            Dim text As String = Nothing
                            Select Case LAGID
                                Case "GR"
                                    text = loc(c).GR
                                Case "ES"
                                    text = loc(c).ES
                                Case "TR"
                                    text = loc(c).TR
                                Case "HU"
                                    text = loc(c).HU
                                Case "DE"
                                    text = loc(c).DE
                                Case "AL"
                                    text = loc(c).AL
                                Case "SR"
                                    text = loc(c).SR
                                Case "RU"
                                    text = loc(c).RU
                                Case "RO"
                                    text = loc(c).RO
                                Case "BG"
                                    text = loc(c).BG
                                Case "FR"
                                    text = loc(c).FR
                                Case "IT"
                                    text = loc(c).IT
                            End Select
                            If (String.IsNullOrEmpty(text)) Then
                                text = loc(c).US
                            End If
                            sb.Append("{""id"":""" & loc(c).JobID & """,""value"":""" & text & """}")

                            If (c < loc.Count - 1) Then sb.Append(",")
                        Next
                        sb.Append("]")
                        arrayJson = sb.ToString()
                    End If

                Catch ex As Exception
                    Throw
                Finally
                    ctx.Dispose()
                End Try

            End If

        Catch ex As Exception
            WebErrorSendEmail(ex, ex.Message)
        End Try

        Return arrayJson
    End Function



    <System.Web.Services.WebMethod()> _
    Public Shared Function JobsSave(job_id As String) As String
        Dim arrayJson As String = ""
        Try
            Dim ProfileID As Integer = HttpContext.Current.Session("ProfileID")
            Dim ijob_id As Integer = 0
            If (ProfileID > 0 AndAlso job_id IsNot Nothing AndAlso Integer.TryParse(job_id, ijob_id) AndAlso ijob_id > 0) Then

                Dim ctx As New CMSDBDataContext(ConnectionString)
                Try

                    ' check
                    Dim loc As EUS_ProfileJob = (From itm In ctx.EUS_ProfileJobs
                                                     Where itm.ProfileID = ProfileID AndAlso itm.JobID = ijob_id
                                                     Select itm).FirstOrDefault()

                    If (loc Is Nothing) Then
                        loc = New EUS_ProfileJob()
                        loc.JobID = ijob_id
                        loc.ProfileID = ProfileID

                        ctx.EUS_ProfileJobs.InsertOnSubmit(loc)
                        ctx.SubmitChanges()
                    End If
                Catch ex As Exception
                    Throw
                Finally
                    ctx.Dispose()
                End Try

            End If

        Catch ex As Exception
            WebErrorSendEmail(ex, ex.Message)
        End Try

        Return arrayJson
    End Function


    <System.Web.Services.WebMethod()> _
    Public Shared Function JobsDelete(job_id As String) As String
        Dim arrayJson As String = ""
        Try
            Dim ProfileID As Integer = HttpContext.Current.Session("ProfileID")
            Dim ijob_id As Integer = 0
            If (ProfileID > 0 AndAlso job_id IsNot Nothing AndAlso Integer.TryParse(job_id, ijob_id) AndAlso ijob_id > 0) Then

                Dim ctx As New CMSDBDataContext(ConnectionString)
                Try

                    ' check
                    Dim loc As EUS_ProfileJob = (From itm In ctx.EUS_ProfileJobs
                                                     Where itm.ProfileID = ProfileID AndAlso itm.JobID = ijob_id
                                                     Select itm).FirstOrDefault()

                    If (loc IsNot Nothing) Then
                        ctx.EUS_ProfileJobs.DeleteOnSubmit(loc)
                        ctx.SubmitChanges()
                    End If
                Catch ex As Exception
                    Throw
                Finally
                    ctx.Dispose()
                End Try

            End If

        Catch ex As Exception
            WebErrorSendEmail(ex, ex.Message)
        End Try

        Return arrayJson
    End Function

    <System.Web.Services.WebMethod()> _
    Public Shared Function GiftsPopupShown(ByVal val As Boolean) As Boolean
        Dim re As Boolean = False
        Try
            Dim ProfileID As Integer = HttpContext.Current.Session("ProfileID")
            re = ProfileHelper.SetHasSeenPopup(ProfileID, val)
            HttpContext.Current.Session("HasSeenGiftPopUp") = val
        Catch ex As Exception
            WebErrorSendEmail(ex, ex.Message)
        End Try
        Return re
    End Function
    <System.Web.Services.WebMethod()> _
    Public Shared Function NewTermsAcceptedShown(ByVal val As Boolean) As Boolean
        Dim re As Boolean = False
        Try
            Dim ProfileID As Integer = HttpContext.Current.Session("ProfileID")
            Dim MirrorProfileID As Integer = HttpContext.Current.Session("MirrorProfileID")
            re = ProfileHelper.ApproveUserNewTOS(ProfileID, MirrorProfileID, val, clsCurrentContext.GetCurrentIP, HttpContext.Current.Session.SessionID)
            HttpContext.Current.Session("HasNewTermsPopUp") = True
        Catch ex As Exception
            WebErrorSendEmail(ex, ex.Message)
        End Try
        Return re
    End Function

    '<System.Web.Services.WebMethod()> _
    'Public Shared Function GetEmoticonsList() As String
    '    Dim result As String = ""
    '    Try
    '        Dim ProfileID As Integer = HttpContext.Current.Session("ProfileID")
    '        If (ProfileID > 0) Then

    '            Dim writer As New StringWriter()
    '            HttpContext.Current.Server.Execute("~/Members/ShowControl.aspx?c=EmoticonsList", writer, False)
    '            result = writer.ToString()

    '            Dim form_ndx = result.IndexOf("<form")
    '            If (form_ndx > -1) Then
    '                form_ndx = (result.IndexOf(">", form_ndx)) + 1
    '                Dim form_close_ndx = result.IndexOf("</form>", form_ndx)
    '                result = result.Substring(form_ndx, form_close_ndx - form_ndx)
    '            End If

    '            form_ndx = result.IndexOf("<input type=""hidden"" name=""__VIEWSTATE""")
    '            If (form_ndx > -1) Then
    '                Dim form_close_ndx = result.IndexOf(">", form_ndx) + 1
    '                result = result.Remove(form_ndx, form_close_ndx - form_ndx)
    '            End If

    '        End If
    '    Catch ex As Exception
    '        WebErrorSendEmail(ex, ex.Message)
    '    End Try

    '    Return result
    'End Function


#Region "Types"

    Public Class MemberTotalActionCounters
        Private _whoViewedMeCounter As Integer
        Public Property WhoViewedMeCounter As Integer
            Get
                Return _whoViewedMeCounter
            End Get
            Set(ByVal value As Integer)
                _whoViewedMeCounter = value
            End Set
        End Property

        Private _whoFavoritedMeCounter As Integer
        Public Property WhoFavoritedMeCounter As Integer
            Get
                Return _whoFavoritedMeCounter
            End Get
            Set(ByVal value As Integer)
                _whoFavoritedMeCounter = value
            End Set
        End Property

        Private _whoSharedPhotosCounter As Integer
        Public Property WhoSharedPhotosCounter As Integer
            Get
                Return _whoSharedPhotosCounter
            End Get
            Set(ByVal value As Integer)
                _whoSharedPhotosCounter = value
            End Set
        End Property

        Public Property HasSubscription As Boolean

        Private _creditsCounter As Integer
        Public Property CreditsCounter As Integer
            Get
                Return _creditsCounter
            End Get
            Set(ByVal value As Integer)
                _creditsCounter = value
            End Set
        End Property

        Private _newDates As Integer
        Public Property NewDates As Integer
            Get
                Return _newDates
            End Get
            Set(ByVal value As Integer)
                _newDates = value
            End Set
        End Property


        Private _newMessages As Integer
        Public Property NewMessages As Integer
            Get
                Return _newMessages
            End Get
            Set(ByVal value As Integer)
                _newMessages = value
            End Set
        End Property



        Private _newOffers As Integer
        Public Property NewOffers As Integer
            Get
                Return _newOffers
            End Get
            Set(ByVal value As Integer)
                _newOffers = value
            End Set
        End Property


        Private _newWinks As Integer
        Public Property NewWinks As Integer
            Get
                Return _newWinks
            End Get
            Set(ByVal value As Integer)
                _newWinks = value
            End Set
        End Property

    End Class

    Public Class FormlessPage
        Inherits Page

        Public Overrides Sub VerifyRenderingInServerForm(control As Control)
        End Sub

    End Class

#End Region


End Class
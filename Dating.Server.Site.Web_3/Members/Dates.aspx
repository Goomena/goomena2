﻿<%@ Page Title="" Language="vb" AutoEventWireup="true" MasterPageFile="~/Members/Members2Inner.Master" 
    MaintainScrollPositionOnPostback="true"
    CodeBehind="Dates.aspx.vb" Inherits="Dating.Server.Site.Web.Dates" %>

<%@ Register src="~/UserControls/DatesControl.ascx" tagname="DatesControl" tagprefix="uc2" %>
<%@ Register src="~/UserControls/MemberLeftPanel.ascx" tagname="MemberLeftPanel" tagprefix="uc1" %>
<%@ Register src="~/UserControls/WhatIsIt.ascx" tagname="WhatIsIt" tagprefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
<div id="dates-list">
                

<asp:UpdatePanel ID="updOffers" runat="server">
    <ContentTemplate>
<div id="likes-list-top">
    <div class="list-title">
        <div class="lfloat">
            <asp:Literal ID="lblItemsName" runat="server" />
            <asp:LinkButton ID="lnkViewDescription" runat="server" CssClass="more_info2" OnClientClick="OnMoreInfoClick('[contentUrl]', this, '[headerText]');return false;" />
        </div>
        <asp:Panel ID="pnlQUserName" runat="server" DefaultButton="imgSearchByUserName" CssClass="rfloat input-user">
            <%--<div class="input-title lfloat"><asp:Label ID="msg_UserNameSearchQ" runat="server" Text="" AssociatedControlID="txtUserNameQ" /></div>--%>
            <div class="search_textbox_container lfloat"><dx:ASPxTextBox ID="txtUserNameQ" runat="server" Width="191" Size="191"></dx:ASPxTextBox></div>
            <div class="search_button_container lfloat"><asp:ImageButton ID="imgSearchByUserName" runat="server" ImageUrl="//cdn.goomena.com/Images/spacer10.png" CssClass="btn imgSearchByUserName" /></div>
            <div class="clear"></div>
        </asp:Panel>
        <div class="clear"></div>
   </div>


    <div id="divFilter" runat="server" clientidmode="Static" class="likes-filters">
        <div class="lfloat cr-word">
            <dx:ASPxLabel ID="lblSortByText" runat="server" Text="" EncodeHtml="False">
            </dx:ASPxLabel>
        </div>
        <div class="lfloat">
            <dx:ASPxComboBox ID="cbSort" runat="server" class="update_uri" 
                AutoPostBack="True" EncodeHtml="False">
                <ClientSideEvents SelectedIndexChanged="ShowLoadingOnMenu" />
            </dx:ASPxComboBox>
        </div>
        <div class="rfloat">
            <dx:ASPxComboBox ID="cbPerPage" runat="server" class="update_uri input-small" style="width: 60px;" 
                AutoPostBack="True" EncodeHtml="False">
                <ClientSideEvents SelectedIndexChanged="ShowLoadingOnMenu" />
                <Items>
                    <dx:ListEditItem Text="10" Value="10" Selected="true"  />
                    <dx:ListEditItem Text="25" Value="25" />
                    <dx:ListEditItem Text="50" Value="50" />
                </Items>
            </dx:ASPxComboBox>
        </div>
        <div class="rfloat cr-word">
            <dx:ASPxLabel ID="lblResultsPerPageText" runat="server" Text="" EncodeHtml="False">
            </dx:ASPxLabel>
        </div>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>
</div>

<script type="text/javascript">
    function setUI() {
    var itms = $("a", "#top-links").css("width", "auto");
    for (var c = 0; c < itms.length; c++) {
        var w = $(itms[c]).width();
        $(itms[c]).css("width", "").removeClass("lnk83").removeClass("lnk112").removeClass("lnk127").removeClass("lnk161")
        if (w < 73)
            $(itms[c]).addClass("lnk83");
        else if (w < 100)
            $(itms[c]).addClass("lnk112");
        else if (w < 115)
            $(itms[c]).addClass("lnk127");
        else
            $(itms[c]).addClass("lnk161");
    }

    try {
        var el = $("a", "#top-links .down");
        var arr = $(".list-arrow", "#top-links-wrap");
        var p1 = el.position().left;
        p1 = (p1 + (el.width() / 2)) - (arr.width() / 2);
        arr.css("left", p1 + "px");
    }
    catch (e) { }

    }
    (function () {
        setUI();
        Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(setUI);
    })();
</script>
                
    
    <div class="s_wrap">
        <div class="items_none no-results" id="divNoResultsChangeCriteria" runat="server" visible="false">
            <asp:Literal ID="msg_divNoResultsTryChangeCriteria_ND" runat="server"/>
	        <div class="clear"></div>
        </div>
    </div>

    <div class="l_wrap">
    <asp:MultiView ID="mvOfferMain" runat="server" ActiveViewIndex="-1">

    <asp:View ID="vwDatesOffers" runat="server"><uc2:DatesControl ID="newDates" runat="server" 
                            UsersListView="DatesOffers" EnableViewState="true"/></asp:View>

    </asp:MultiView>
    </div>

    <div class="pagination">
        <table align="center">
            <tr>
                <td><dx:ASPxPager ID="Pager" runat="server"
                        ItemCount="3" 
                        ItemsPerPage="1" 
                        RenderMode="Lightweight" CssFilePath="~/App_Themes/Aqua/{0}/styles.css" 
                        CssPostfix="Aqua" 
                        CurrentPageNumberFormat="{0}" 
                        PageNumberFormat="<span onclick='ShowLoading();'>{0}</span>"
                        EncodeHtml="false"
                        SpriteCssFilePath="~/App_Themes/Aqua/{0}/sprite.css"
                        SeoFriendly="Enabled">
                        <lastpagebutton visible="True"> 
                        </lastpagebutton>
                        <firstpagebutton visible="True">
                        </firstpagebutton>
                        <Summary Position="Left" Text="Page {0} of {1} ({2} members)" Visible="false" />
                </dx:ASPxPager></td>
            </tr>
        </table>
    </div>

    </ContentTemplate>
</asp:UpdatePanel>


    <%--<div class="pagination">
        <div class="float-right">
            <dx:ASPxPager ID="Pager" runat="server" ItemCount="3" ItemsPerPage="1" 
                RenderMode="Lightweight" CssFilePath="~/App_Themes/Aqua/{0}/styles.css" 
                CssPostfix="Aqua" CurrentPageNumberFormat="{0}" 
                SpriteCssFilePath="~/App_Themes/Aqua/{0}/sprite.css" 
                PageNumberFormat="<span onclick='ShowLoading();'>{0}</span>"
                EncodeHtml="false"
                SeoFriendly="Enabled">
                <lastpagebutton visible="True">
                </lastpagebutton>
                <firstpagebutton visible="True">
                </firstpagebutton>
                <Summary Position="Left" Text="Page {0} of {1} ({2} members)" Visible="false" />
            </dx:ASPxPager>
        </div>
    </div>--%>
    
        
    
	<uc2:whatisit ID="WhatIsIt1" runat="server" />
</div>

<script type="text/javascript">
    jQuery(function ($) {
        $(window).ready(function () {
            scrollToLogin();
        });
    });
</script>
</asp:Content>


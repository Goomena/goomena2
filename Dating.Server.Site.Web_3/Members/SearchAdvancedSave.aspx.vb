﻿Imports System.Globalization

Public Class SearchAdvancedSave
    Inherits BasePage



#Region "Props"

#End Region


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If (Not Me.IsPostBack) Then
                LoadLAG()

                If (Session("AdvancedSearch_Dont_Save_Dont_Ask") Is Nothing) Then
                    ' do nothing
                ElseIf (Session("AdvancedSearch_Dont_Save_Dont_Ask") = True) Then
                    chkDontAsk.Checked = True
                End If
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try
    End Sub


    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender

        Try
            Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub



    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
    End Sub


    Protected Sub LoadLAG()
        Try

            Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = Me.CurrentPageData.cPageBasic
            lblSearchName.Text = Me.CurrentPageData.GetCustomString("lblSearchName")
            btnSave.Text = Me.CurrentPageData.GetCustomString("btnSave")
            lblTitle.Text = Me.CurrentPageData.GetCustomString("lblTitle")
            chkDontAsk.Text = Me.CurrentPageData.GetCustomString("chkDontAsk")
            txtSearchName.Text = Date.Now.ToString("dddd dd MMMM, HH:mm", CultureInfo.CreateSpecificCulture("en-US"))

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    'Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
    '    Try
    '    Catch ex As Exception
    '        WebErrorMessageBox(Me, ex, "")
    '    End Try
    'End Sub

    Protected Sub btnDontSave_Click(sender As Object, e As EventArgs) Handles btnDontSave.Click
        Try
            If (chkDontAsk.Checked) Then
                Session("AdvancedSearch_Dont_Save_Dont_Ask") = True
            End If
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "notifyParentNoSave", "notifyParentNoSave()", True)

            'Dim b As Boolean = Dating.Server.Core.DLL.DataHelpers.EUS_Profiles_CheckPassword(Me.MasterProfileId, txtSearchName.Text)
            'If (b) Then
            '    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "notifyParentSuccess", "notifyParentSuccess()", True)
            'Else
            '    pnlPasswordVerifyFailed.Visible = True
            '    lblPasswordVerifyFailed.Text = Me.CurrentPageData.GetCustomString("lblPasswordVerifyFailed")
            'End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


End Class
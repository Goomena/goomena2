﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Members/Members2Inner.master" CodeBehind="PaymentCoupon.aspx.vb" Inherits="Dating.Server.Site.Web.PaymentCoupon" %>

<%@ Register Src="../UserControls/MemberLeftPanel.ascx" TagName="MemberLeftPanel"
    TagPrefix="uc1" %>
<%@ Register src="../UserControls/WhatIsIt.ascx" tagname="WhatIsIt" tagprefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <div class="clear">
    </div>
    <div class="container_12" id="tabs-outter">
        <div class="grid_12 top_tabs">
            <div>
            </div>
        </div>
    </div>
    <div class="clear">
    </div>
    <div class="container_12" id="content_top">
    </div>
    <div class="container_12" id="content">
        <div class="grid_3 top_sidebar" id="sidebar-outter">
            <div class="left_tab" id="left_tab" runat="server">
                <div class="top_info">
                </div>
                <div class="middle" id="search">
                    <div class="submit">
                        <h3><asp:Literal ID="lblItemsName" runat="server" /></h3>
                        <div class="clear"></div>
                        <asp:LinkButton ID="lnkViewDescription" runat="server" CssClass="more_info2" OnClientClick="OnMoreInfoClick('[contentUrl]', this, '[headerText]');return false;" />
                    </div>
                </div>
                <div class="bottom">
                    &nbsp;</div></div>
            <%--<uc1:MemberLeftPanel ID="leftPanel" runat="server" ShowBottomPanel="false" />--%>
        </div>
        <div class="grid_9 body">
                <asp:Panel ID="content_outter" runat="server" CssClass="middle" ClientIDMode="Static">


            <div id="filter" class="m_filter t_filter msg_filter">
	            <div class="lfloat">
		            <h2><asp:Label ID="lblPageTitle" runat="server" Text="Reseller Voucher Activation"/></h2>
	            </div>
	            <div class="rfloat">
                    <dx:ASPxHyperLink ID="lnkBack" runat="server" Text="Back" CssClass="btn" 
                        Visible="False">
                    </dx:ASPxHyperLink>
	            </div>

	            <div class="clear"></div>
            </div>

            <div class="t_wrap">
                <asp:Panel ID="pnlActivate" runat="server">
                    <div class="send_msg t_to msg">
                        <div class="message voucher_form">
                            <p style="margin-bottom:10px;">
                                <dx:ASPxLabel ID="lblTopDescription" 
                                    runat="server" Text="Redeem Voucher description" EncodeHtml="False">
                                </dx:ASPxLabel>
                            </p>
                            <div class="offer_input">
                                <asp:Panel ID="pnlErr" runat="server" CssClass="alert alert-danger" Visible="False">
                                    <asp:Label ID="lblErr" runat="server" Text=""></asp:Label>
                                </asp:Panel>
                                <div class="input-prepend">
                                    <div class="lfloat" style="padding-top:8px;padding-left:10px;margin-right:5px;">
                                        <dx:ASPxLabel ID="lblInsertVoucher" runat="server" Text="Insert your voucher" 
                                            EncodeHtml="False">
                                        </dx:ASPxLabel>
                                    </div>
                                    <div class="lfloat">
                                        <dx:ASPxTextBox ID="txtVoucher" runat="server" Width="170px">
                                        </dx:ASPxTextBox>
                                    </div>
                                    <div class="clear">
                                    </div>
                                </div>
                                <div class="clear">
                                </div>
                                <div id="divWarning" runat="server" class="alert alert-info" style="margin-top:15px;" visible="false">
                                    <dx:ASPxLabel ID="lblNote" runat="server" Text="" 
                                        EncodeHtml="False">
                                    </dx:ASPxLabel>
                                </div>
                                <div class="clear">
                                </div>
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                    </div>
                    <div class="form-actions">
                        <dx:ASPxButton ID="btnProceed" runat="server" 
                            CssClass="btn btn-large btn-primary" Text="Proceed" Native="True" EncodeHtml="False" 
                            UseSubmitBehavior="False"  />
                        <dx:ASPxButton ID="btnCreateTest" runat="server" 
                            CssClass="btn btn-large btn-primary" Text="Create Test" Native="True" EncodeHtml="False" 
                            UseSubmitBehavior="False" Visible="False" />
                    </div>
                </asp:Panel>
	        </div>
            <asp:Panel ID="pnlActivateDone" runat="server" Visible="False" style="margin:20px;" CssClass="alert alert-success">
                <dx:ASPxLabel ID="lblResult" runat="server" Text="" 
                                    EncodeHtml="False">
                                </dx:ASPxLabel>
            </asp:Panel>


                <%--<table id="ctl00_tblTitle" cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tbody>
                        <tr>
                            <td valign="middle" class="DetailsPageHeader">
                                <table cellpadding="0" cellspacing="0" border="0" class="Title" width="100%">
                                    <tbody>
                                        <tr>
                                            <td style="width: 1%;" rowspan="2">
                                                <dx:ASPxImage ID="iTitleImage" runat="server" Style="height: 50px; width: 45px; border-width: 0px;
                                                    border-width: 0px;" ImageUrl="../Images/IconImages/agt_update_critical.png" AlternateText="Transaction Cancelled - Your credit card was NOT charged">
                                                </dx:ASPxImage>
                                            </td>
                                            <td>
                                                &nbsp;&nbsp;<dx:ASPxLabel ID="lGroupName" runat="server" Text="Transaction Cancelled"
                                                    Style="font-size: X-Large; font-weight: bold;" EncodeHtml="False">
                                                </dx:ASPxLabel>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;&nbsp;<dx:ASPxLabel ID="lbSubTitle" runat="server" 
                                                    Text="Your credit card was NOT charged" EncodeHtml="False">
                                                </dx:ASPxLabel>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table id="tblContent" cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tbody>
                        <tr>
                            <td valign="top">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tbody>
                                        <tr>
                                            <td class="DetailsPageLeftEdge">
                                                <div class="Spacer DetailsPageEdge">
                                                </div>
                                            </td>
                                            <td valign="top" class="DetailsPageContent">
                                                <div class="ContentMargin">
                                                    <dx:ASPxLabel ID="lDescription" runat="server" 
                                                        Text="Redeem Voucher description" EncodeHtml="False">
                                                    </dx:ASPxLabel>
                                                </div>
                                            </td>
                                            <td class="DetailsPageRightEdge">
                                                <div class="Spacer DetailsPageEdge">
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table id="ctl00_tblFooter" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                        <tr>
                            <td class="DetailsPageFooter">
                                <div class="Spacer">
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>--%>

                </asp:Panel>
            <div class="bottom">
            </div>
        </div>
        <div class="clear">
        </div>
    </div>

	<uc2:whatisit ID="WhatIsIt1" runat="server" />
    
</asp:Content>

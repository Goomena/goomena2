﻿'Imports Sites.SharedSiteCode
Imports Google.Contacts
Imports Google.GData.Client
Imports Google.GData.Contacts
Imports Google.GData.Extensions

Imports Library.Public
Imports Dating.Server.Core.DLL
Imports System.Data.SqlClient
Imports System.Net
Imports System.IO
Imports System.Web.Script.Serialization
Imports System.Data
Imports System.Xml
Imports System.Web.Script.Services


Public Class ImportContacts
    Inherits BasePage


    Protected Overrides Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        SetMasterPage(CurrentPageData)

        Try
            If clsCurrentContext.VerifyLogin() = True Then
                Me.MasterPageFile = "~/Members/Members2.Master"
                'If (Request("master") = "inner") Then
                '    Me.MasterPageFile = "~/Members/Members2Inner.Master"
                'Else
                'End If
            Else
                clsCurrentContext.ClearSession(Session)
                If (Not Me.IsCallback) Then
                    Response.Redirect(ResolveUrl("~/Login.aspx"), False)
                End If
                HttpContext.Current.ApplicationInstance.CompleteRequest()
            End If
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            'clsContactManager.Count_All_UserContacts(Me.MasterProfileId)
            If (clsCurrentContext.VerifyLogin() = False) Then
                FormsAuthentication.SignOut()
                Response.Redirect(Page.ResolveUrl("~/Login.aspx"))
            End If

            If (Not Me.IsPostBack) Then
                LoadLAG()
            End If

            'clearSessionTokens()

            'Session.Remove("Gcode")
            'Session.Remove("OauthRequestToken")
            'Session.Remove("OauthRequestTokenSecret")
            'Session.Remove("oauth_verifier")
            'Session.Remove("OauthAccessToken")
            'Session.Remove("OauthAccessTokenSecret")
            'Session.Remove("OauthSessionHandle")
            'Session.Remove("OauthYahooGuid")

            '************Gmail Contacts************

            If Request("code") IsNot Nothing Then   'Αν το Gmail popup είναι ανοιχτό...

                Session.Add("Gcode", DirectCast(Request("code"), String)) 'Αποθηκεύουμε στο Session τον μοναδικό κωδικό που μας έχει δώσει η Google για να είναι διαθέσιμος και μετά την επαναφόρτωση
                ScriptManager.RegisterClientScriptBlock(Me.Page, GetType(String), "refreshGmail", "window.close();", True)
                ScriptManager.RegisterClientScriptBlock(Me.Page, GetType(String), "refreshGmail2", "window.opener.location.reload(true);", True)  'Κλείνουμε το Gmail Popup και κάνουμε επαναφόρτωση
            Else
                'Αν το Gmail popup έχει ήδη κλείσει...
                If Session("Gcode") IsNot Nothing Then

                    Dim gmailContacts As List(Of clsContact) = RetrieveGmailContacts(Session("Gcode"))

                    'Εισαγωγή των επαφών Gmail στη βάση δεδομένων

                    If gmailContacts IsNot Nothing AndAlso gmailContacts.Count > 0 Then
                        Dim contactsInserted As Integer = clsContactManager.Insert_UserContacts(Me.MasterProfileId, gmailContacts)
                        If contactsInserted > 0 Then
                            'Εισήχθησαν επαφές
                        Else
                            'Δεν είχαμε νέες επαφές

                        End If
                    Else
                        'Σφάλμα - δεν εισήχθησαν επαφές (μπορεί ο χρήστης να μην έχει επαφές στο Gmail του)

                    End If

                    gvContacts.DataSource = gmailContacts
                    gvContacts.DataBind()
                    'Session.Remove("Gcode") 'Διαγράφουμε τον Gcode από το session έτσι ώστε να μην ξαναγίνει η ίδια αίτηση με το ίδιο Gcode (θα πεταχτεί exception-bad request)
                    clearSessionTokens()
                End If

                '************Yahoo Contacts************

                'Ελέγχουμε αν έχουν εισαχθεί σωστά οι επαφές από το Yahoo (θα ελέγχουμε το Session για την ύπαρξη ή όχι της μεταβλητής για σωστή εισαγωγή των επαφών από το Yahoo)...

                If Request("oauth_token") IsNot Nothing Then 'Αν το Yahoo popup είναι ανοιχτό...
                    Dim oauth_request_token As String = Request("oauth_token")
                    Dim oauth_verifier As String = Request("oauth_verifier")

                    Session("OauthRequestToken") = oauth_request_token

                    Session("Oauth_verifier") = oauth_verifier

                    ScriptManager.RegisterClientScriptBlock(Me.Page, GetType(String), "refreshYahoo", "window.close();", True)
                    ScriptManager.RegisterClientScriptBlock(Me.Page, GetType(String), "refreshYahoo1", "window.opener.location.reload(true);", True)  'Κλείνουμε το Yahoo Popup και κάνουμε επαναφόρτωση
                Else

                    'Αν το Yahoo popup έχει ήδη κλείσει...
                    If Session("OauthRequestToken") IsNot Nothing Then
                        'Σε αυτό το σημείο, έχουμε ήδη πάρει Request Token και θα το ανταλλάξουμε με Access Token. Μόλις πάρουμε το Access Token, μπορούμε να διαβάσουμε τις επαφές...
                        Dim oauth_request_token As String = Session("OauthRequestToken")
                        Dim oauth_verifier As String = Session("Oauth_verifier")

                        GetYahooAccessToken(oauth_request_token, oauth_verifier)

                        Dim yahooContacts As List(Of clsContact)
                        yahooContacts = RetriveYahooContacts()

                        If yahooContacts IsNot Nothing AndAlso yahooContacts.Count > 0 Then
                            Dim contactsInserted As Integer = clsContactManager.Insert_UserContacts(Me.MasterProfileId, yahooContacts)
                            If contactsInserted > 0 Then
                                'Εισήχθησαν επαφές
                            Else
                                'Δεν είχαμε νέες επαφές

                            End If
                        Else
                            'Σφάλμα - δεν εισήχθησαν επαφές (μπορεί ο χρήστης να μην έχει επαφές στο Yahoo Mail του)

                        End If

                        gvContacts.DataSource = yahooContacts
                        gvContacts.DataBind()
                        clearSessionTokens()
                    End If
                End If

            End If

        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            clearSessionTokens()
            WebErrorSendEmail(ex, "")
        End Try
    End Sub



    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender

        Try
            Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
        Catch ex As Exception
            clearSessionTokens()
            WebErrorMessageBox(Me, ex, "PreRender")
        End Try

    End Sub



    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
    End Sub


    Protected Sub LoadLAG()
        Try
            Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            'lblHeader.Text = CurrentPageData.GetCustomString("lblHeader")
            'lblContent.Text = CurrentPageData.GetCustomString("lblContent")
            'btnSearchMen.Text = CurrentPageData.GetCustomString("btnSearchMen")
        Catch ex As Exception
            clearSessionTokens()
            WebErrorSendEmail(ex, "")
        End Try
    End Sub


    Public Function RetrieveGmailContacts(ByVal code As String) As List(Of clsContact)
        Dim contacts As New List(Of clsContact)()
        Try
            Dim postcontents As String = String.Format("code={0}&client_id={1}&client_secret={2}&redirect_uri={3}&grant_type=authorization_code",
                                                       System.Web.HttpUtility.UrlEncode(code),
                                                       System.Web.HttpUtility.UrlEncode(ConfigurationManager.AppSettings("gmailclientid")),
                                                       System.Web.HttpUtility.UrlEncode(ConfigurationManager.AppSettings("gmailsecret")),
                                                       System.Web.HttpUtility.UrlEncode(Dating.Server.Site.Web.UrlUtils.GetURLWithScheme(ConfigurationManager.AppSettings("gmailreturnurl"))))
            Dim request As HttpWebRequest = DirectCast(HttpWebRequest.Create("https://accounts.google.com/o/oauth2/token"), HttpWebRequest)
            request.Method = "POST"
            Dim postcontentsArray As Byte() = System.Text.Encoding.UTF8.GetBytes(postcontents)
            request.ContentType = "application/x-www-form-urlencoded"
            request.ContentLength = postcontentsArray.Length
            Dim token As GoogleOAuthToken
            Using requestStream As Stream = request.GetRequestStream
                requestStream.Write(postcontentsArray, 0, postcontentsArray.Length)
                requestStream.Close()
                Dim response As WebResponse = request.GetResponse()
                Using responseStream As Stream = response.GetResponseStream()
                    Using reader As New StreamReader(responseStream)
                        Dim responseFromServer As String = reader.ReadToEnd()
                        reader.Close()
                        responseStream.Close()
                        response.Close()
                        Dim ser As New JavaScriptSerializer()
                        token = ser.Deserialize(Of GoogleOAuthToken)(responseFromServer)
                    End Using
                End Using
            End Using
            Dim contactrequest As New RequestSettings("import contact", token.access_token)
            contactrequest.AutoPaging = True
            Dim req As New ContactsRequest(contactrequest)
            For Each contact As Google.Contacts.Contact In req.GetContacts().Entries
                Dim userContact As New clsContact
                userContact.ContactEmail = contact.PrimaryEmail.Address
                userContact.ContactFirstName = contact.Name.GivenName 'Προσοχή...Υπάρχουν διάφορα πεδία για το όνομα. Τα επικρατέστερα είναι το GivenName και το FamilyName...
                userContact.ContactLastName = contact.Name.FamilyName
                'userContact.AssociatedGoomenaProfileID = Me.MasterProfileId

                contacts.Add(userContact)
            Next

            Return contacts
        Catch ex As Exception
            clearSessionTokens()
            WebErrorSendEmail(ex, "")
            Return contacts
        End Try
    End Function

    Public Class GoogleOAuthToken
        Public access_token As String
        Public expires_in As String
        Public token_type As String
        Public refresh_token As String
    End Class


    Private Function GetYahooRequestToken() As String

        clearSessionTokens()
        Dim authorizationUrl As String = String.Empty

        Dim consumerKey As String = ConfigurationManager.AppSettings("yahooconsumerkey")
        Dim consumerSecret As String = ConfigurationManager.AppSettings("yahooconsumersecret")

        Dim oauth As New OAuthBase()

        Dim uri As New Uri("https://api.login.yahoo.com/oauth/v2/get_request_token")
        Dim nonce As String = oauth.GenerateNonce()
        Dim timeStamp As String = oauth.GenerateTimeStamp()
        Dim normalizedUrl As String = ""
        Dim normalizedRequestParameters As String = ""
        Dim sig As String = oauth.GenerateSignature(uri, consumerKey, consumerSecret, String.Empty, String.Empty, "GET", _
         timeStamp, nonce, OAuthBase.SignatureTypes.PLAINTEXT, normalizedUrl, normalizedRequestParameters)
        'OAuthBase.SignatureTypes.HMACSHA1
        Dim sbRequestToken As New StringBuilder(uri.ToString())
        sbRequestToken.AppendFormat("?oauth_nonce={0}&", nonce)
        sbRequestToken.AppendFormat("oauth_timestamp={0}&", timeStamp)
        sbRequestToken.AppendFormat("oauth_consumer_key={0}&", consumerKey)
        sbRequestToken.AppendFormat("oauth_signature_method={0}&", "PLAINTEXT")
        'HMAC-SHA1
        sbRequestToken.AppendFormat("oauth_signature={0}&", sig)
        sbRequestToken.AppendFormat("oauth_version={0}&", "1.0")
        sbRequestToken.AppendFormat("oauth_callback={0}", HttpUtility.UrlEncode(Dating.Server.Site.Web.UrlUtils.GetURLWithScheme(ConfigurationManager.AppSettings("yahooreturnurl"))))
        'Response.Write(sbRequestToken.ToString());
        'Response.End();

        Try
            Dim returnStr As String = String.Empty
            Dim returnData As String()

            Dim req As HttpWebRequest = DirectCast(WebRequest.Create(sbRequestToken.ToString()), HttpWebRequest)
            Dim res As HttpWebResponse = DirectCast(req.GetResponse(), HttpWebResponse)
            Dim streamReader As New StreamReader(res.GetResponseStream())
            returnStr = streamReader.ReadToEnd()
            returnData = returnStr.Split(New [Char]() {"&"c})
            'Response.Write(returnStr);

            Dim index As Integer
            If returnData.Length > 0 Then
                'index = returnData[0].IndexOf("=");
                'string oauth_token = returnData[0].Substring(index + 1);
                'Session["Oauth_Token"] = oauth_token;

                index = returnData(1).IndexOf("=")
                Dim oauthRequestToken_secret As String = returnData(1).Substring(index + 1)
                Session("OauthRequestTokenSecret") = oauthRequestToken_secret

                'index = returnData[2].IndexOf("=");
                'int oauth_expires_in;
                'Int32.TryParse(returnData[2].Substring(index + 1), out oauth_expires_in);
                'Session["Oauth_Expires_In"] = oauth_expires_in;

                index = returnData(3).IndexOf("=")
                Dim oauth_request_auth_url As String = returnData(3).Substring(index + 1)
                authorizationUrl = HttpUtility.UrlDecode(oauth_request_auth_url)
            End If
            'Response.Write(ex.Message);
        Catch ex As WebException
            clearSessionTokens()
            WebErrorSendEmail(ex, "")
        End Try
        Return authorizationUrl
    End Function

    Private Sub GetYahooAccessToken(ByVal oauth_token As String, ByVal oauth_verifier As String)
        Dim consumerKey As String = ConfigurationManager.AppSettings("yahooconsumerkey")
        Dim consumerSecret As String = ConfigurationManager.AppSettings("yahooconsumersecret")

        Dim oauth As New OAuthBase()

        Dim oauthRequestToken_secret As String = Session("OauthRequestTokenSecret")

        Dim uri As New Uri("https://api.login.yahoo.com/oauth/v2/get_token")
        Dim nonce As String = oauth.GenerateNonce()
        Dim timeStamp As String = oauth.GenerateTimeStamp()
        Dim sig As String = consumerSecret & "%26" & oauthRequestToken_secret

        Dim sbAccessToken As New StringBuilder(uri.ToString())
        sbAccessToken.AppendFormat("?oauth_consumer_key={0}&", consumerKey)
        sbAccessToken.AppendFormat("oauth_signature_method={0}&", "PLAINTEXT")
        'HMAC-SHA1
        sbAccessToken.AppendFormat("oauth_signature={0}&", sig)
        sbAccessToken.AppendFormat("oauth_timestamp={0}&", timeStamp)
        sbAccessToken.AppendFormat("oauth_version={0}&", "1.0")
        sbAccessToken.AppendFormat("oauth_token={0}&", oauth_token)
        sbAccessToken.AppendFormat("oauth_nonce={0}&", nonce)
        sbAccessToken.AppendFormat("oauth_verifier={0}", oauth_verifier)
        'Response.Write(sbAccessToken.ToString());
        'Response.End();

        Try
            Dim returnStr As String = String.Empty
            Dim returnData As String()

            Dim req As HttpWebRequest = DirectCast(WebRequest.Create(sbAccessToken.ToString()), HttpWebRequest)
            lblLog.Text = sbAccessToken.ToString()

            Dim res As HttpWebResponse = DirectCast(req.GetResponse(), HttpWebResponse)
            Dim streamReader As New StreamReader(res.GetResponseStream())
            returnStr = streamReader.ReadToEnd()
            returnData = returnStr.Split(New [Char]() {"&"c})
            'Response.Write(returnStr);
            'Response.End();

            Dim index As Integer
            If returnData.Length > 0 Then
                index = returnData(0).IndexOf("=")
                Dim oauthAccessToken As String = returnData(0).Substring(index + 1)

                Session("OauthAccessToken") = oauthAccessToken

                index = returnData(1).IndexOf("=")
                Dim oauthAccessToken_secret As String = returnData(1).Substring(index + 1)

                Session("OauthAccessTokenSecret") = oauthAccessToken_secret

                'index = returnData[2].IndexOf("=");
                'int oauth_expires_in;
                'Int32.TryParse(returnData[2].Substring(index + 1), out oauth_expires_in);

                index = returnData(3).IndexOf("=")
                Dim oauth_session_handle As String = returnData(3).Substring(index + 1)

                Session("OauthSessionHandle") = oauth_session_handle

                'OauthSessionHandle = oauth_session_handle

                'index = returnData[4].IndexOf("=");
                'int oauth_authorization_expires_in;
                'Int32.TryParse(returnData[4].Substring(index + 1), out oauth_authorization_expires_in);

                index = returnData(5).IndexOf("=")
                Dim xoauth_yahoo_guid As String = returnData(5).Substring(index + 1)

                Session("OauthYahooGuid") = xoauth_yahoo_guid

                'OauthYahooGuid = xoauth_yahoo_guid
            End If

            'Response.Write(ex.Message);
        Catch ex As WebException
            clearSessionTokens()
            WebErrorSendEmail(ex, "")
        End Try
    End Sub


    Private Function RetriveYahooContacts() As List(Of clsContact)

        Dim consumerKey As String = ConfigurationManager.AppSettings("yahooconsumerkey")
        Dim consumerSecret As String = ConfigurationManager.AppSettings("yahooconsumersecret")
        Dim oauthAccessToken As String = Session("OauthAccessToken")
        Dim oauthAccessToken_secret As String = Session("OauthAccessTokenSecret")
        Dim xoauth_yahoo_guid As String = Session("OauthYahooGuid")

        Dim oauth As New OAuthBase()

        Dim uri As New Uri("https://social.yahooapis.com/v1/user/" & xoauth_yahoo_guid & "/contacts?format=XML")
        'Dim uri As New Uri("https://query.yahooapis.com/v1/yql?q=SELECT * from social.contacts WHERE guid=me")

        Dim nonce As String = oauth.GenerateNonce()
        Dim timeStamp As String = oauth.GenerateTimeStamp()
        Dim normalizedUrl As String = ""
        Dim normalizedRequestParameters As String = ""
        Dim sig As String = oauth.GenerateSignature(uri, consumerKey, consumerSecret, oauthAccessToken, oauthAccessToken_secret, "GET", _
         timeStamp, nonce, OAuthBase.SignatureTypes.HMACSHA1, normalizedUrl, normalizedRequestParameters)

        Dim sbGetContacts As New StringBuilder(uri.ToString())

        'Response.Write("URL: " + sbGetContacts.ToString());
        'Response.End();

        Try
            Dim returnStr As String = String.Empty
            Dim req As HttpWebRequest = DirectCast(WebRequest.Create(sbGetContacts.ToString()), HttpWebRequest)
            req.Method = "GET"

            Dim authHeader As String = "Authorization: OAuth " & "realm=""yahooapis.com""" & ",oauth_consumer_key=""" & consumerKey & """" & ",oauth_nonce=""" & nonce & """" & ",oauth_signature_method=""HMAC-SHA1""" & ",oauth_timestamp=""" & timeStamp & """" & ",oauth_token=""" & oauthAccessToken & """" & ",oauth_version=""1.0""" & ",oauth_signature=""" & HttpUtility.UrlEncode(sig) & """"

            'Response.Write("</br>Headers: " + authHeader);

            req.Headers.Add(authHeader)

            Dim res As HttpWebResponse = DirectCast(req.GetResponse(), HttpWebResponse)
            Dim streamReader As New StreamReader(res.GetResponseStream())
            returnStr = streamReader.ReadToEnd()
            Dim xmldoc As New XmlDocument()
            xmldoc.LoadXml(returnStr)

            Dim elemList As XmlNodeList = xmldoc.DocumentElement.GetElementsByTagName("contact")

            Dim contacts As New List(Of clsContact)()

            For i As Integer = 0 To elemList.Count - 1
                Dim userContact As New clsContact
                Dim currentContact As XmlElement = elemList(i)
                Dim fieldList As XmlNodeList = currentContact.GetElementsByTagName("fields") 'Τα πεδία της επαφής δεν θα έρθουν πάντα με την ίδια σειρά και δε θα είναι πάντα τα ίδια. Εξαρτάται από τις πληροφορίες που έχει ορίσει ο χρήστης για την κάθε επαφή.
                For j As Integer = 0 To fieldList.Count - 1
                    Dim currentFieldElement As XmlElement = fieldList(j)
                    Dim typeElement As XmlNodeList = currentFieldElement.GetElementsByTagName("type") 'Αυτή η λίστα θα έχει μόνο 1 στοιχείο. Κάθε στοιχείο field έχει μόνο 1 στοιχείο type ως παιδί (αλλά δεν ξέρουμε αν είναι πρώτο, δεύτερο κλπ ώστε να πάμε κατευθείαν σε αυτό).
                    'Οι πληροφορίες αλλάζουν ανάλογα με την επαφή και τα πεδία που έχει ορίσει ο χρήστης για την κάθε επαφή. Έτσι, αλλάζει και το xml δέντρο αντίστοιχα. Άρα, ψάχνουμε το στοιχείο που μας ενδιαφέρει με επαναληπτικό βρόχο.                             

                    If typeElement(0).ChildNodes(0).InnerText = "email" Then
                        Dim valueElement As XmlNodeList = currentFieldElement.GetElementsByTagName("value") 'Και αυτή η λίστα θα έχει μόνο 1 στοιχείο, την τιμή που θέλουμε.
                        Dim emailAddress = valueElement(0).ChildNodes(0).InnerText
                        userContact.ContactEmail = emailAddress
                    End If

                    If typeElement(0).ChildNodes(0).InnerText = "name" Then
                        'Το στοιχείο name, έχει ως παιδί το στοιχείο value και εκείνο έχει ως παιδιά τους διάφορους τύπους ονόματος, όπως Given Name, Middle Name, Family Name Κλπ. Εμείς, θα πάρουμε το Given Name και το Family Name.
                        Dim valueElement As XmlNodeList = currentFieldElement.GetElementsByTagName("value") 'Και αυτή η λίστα θα έχει μόνο 1 στοιχείο.
                        Dim nameTypes As XmlElement = valueElement(0)

                        Dim givenNameElement As XmlNodeList = nameTypes.GetElementsByTagName("givenName") 'Και αυτή η λίστα θα έχει μόνο 1 στοιχείο.
                        Dim givenNameValue As String = givenNameElement(0).InnerText
                        userContact.ContactFirstName = givenNameValue

                        Dim familyNameElement As XmlNodeList = nameTypes.GetElementsByTagName("familyName") 'Και αυτή η λίστα θα έχει μόνο 1 στοιχείο.
                        Dim familyNameValue As String = familyNameElement(0).InnerText
                        userContact.ContactLastName = familyNameValue

                        'Θα μπορούσαμε να πάρουμε και άλλους τύπους ονόματος, αν ήταν απαραίτητο
                    End If

                    'Θα μπορούσαμε να πάρουμε και άλλα πεδία για την κάθε επαφή, αν ήταν απαραίτητο
                Next

                'userContact.AssociatedGoomenaProfileID = Me.MasterProfileId
                contacts.Add(userContact)
            Next

            Return contacts

        Catch ex As WebException
            clearSessionTokens()
            WebErrorSendEmail(ex, "")
            Return Nothing
        End Try
    End Function


    Protected Sub btnImportYahooContacts_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnImportYahooContacts.Click
        clearSessionTokens()
        Dim authorizationUrl As String
        authorizationUrl = String.Empty
        authorizationUrl = GetYahooRequestToken()
        Dim s As String = "window.open('" & authorizationUrl + "', 'yahoo_popup_window', 'width=600,height=600,left=100,top=100,resizable=no');"
        ClientScript.RegisterStartupScript(Me.GetType(), "yahoo_redirect", s, True)
    End Sub


    <System.Web.Services.WebMethod()> _
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Shared Function RetrieveHotmailContacts(HotmailContacts As List(Of clsContact))
        Try
            'Εισαγωγή των επαφών Hotmail στη βάση δεδομένων

            If HotmailContacts.Count > 0 Then
                Dim currentProfileID As Integer = HttpContext.Current.Session("ProfileID")
                'For Each contact As clsContact In HotmailContacts
                '    contact.AssociatedGoomenaProfileID = currentProfileID
                'Next

                Dim contactsInserted As Integer = clsContactManager.Insert_UserContacts(currentProfileID, HotmailContacts)
                If contactsInserted > 0 Then
                    'Εισήχθησαν επαφές
                Else
                    'Δεν είχαμε νέες επαφές

                End If

            Else
                'Σφάλμα - δεν εισήχθησαν επαφές (μπορεί ο χρήστης να μην έχει επαφές στο Hotmail του)

            End If

            Return True
        Catch ex As Exception
            'Error Message ? WebErrorMessageBox(Me, ex, "")
            Return False
        End Try
    End Function

    Private Sub clearSessionTokens()
        If Session("Gcode") IsNot Nothing Then
            Session("Gcode") = Nothing
            Session.Remove("Gcode")
        End If

        If Session("OauthRequestToken") IsNot Nothing Then
            Session("OauthRequestToken") = Nothing
            Session.Remove("OauthRequestToken")
        End If

        If Session("OauthRequestTokenSecret") IsNot Nothing Then
            Session("OauthRequestTokenSecret") = Nothing
            Session.Remove("OauthRequestTokenSecret")
        End If

        If Session("Oauth_verifier") IsNot Nothing Then
            Session("Oauth_verifier") = Nothing
            Session.Remove("Oauth_verifier")
        End If

        If Session("OauthAccessToken") IsNot Nothing Then
            Session("OauthAccessToken") = Nothing
            Session.Remove("OauthAccessToken")
        End If

        If Session("OauthAccessTokenSecret") IsNot Nothing Then
            Session("OauthAccessTokenSecret") = Nothing
            Session.Remove("OauthAccessTokenSecret")
        End If

        If Session("OauthSessionHandle") IsNot Nothing Then
            Session("OauthSessionHandle") = Nothing
            Session.Remove("OauthSessionHandle")
        End If

        If Session("OauthYahooGuid") IsNot Nothing Then
            Session("OauthYahooGuid") = Nothing
            Session.Remove("OauthYahooGuid")
        End If

        'Session("Gcode") = Nothing
        'Session("OauthRequestToken") = Nothing
        'Session("OauthRequestTokenSecret") = Nothing
        'Session("OauthAccessToken") = Nothing
        'Session("OauthAccessTokenSecret") = Nothing
        'Session("OauthSessionHandle") = Nothing
        'Session("OauthYahooGuid") = Nothing
    End Sub


    Private Sub btnCountContacts_Click(sender As Object, e As EventArgs) Handles btnCountContacts.Click
        Dim contacts As Integer = clsContactManager.Count_All_UserContacts(Me.MasterProfileId)
        lblCountContacts.Text = "Imported contacts: " & contacts
        gvContacts.DataSource = clsContactManager.Get_All_UserContacts(Me.MasterProfileId)
        gvContacts.DataBind()
    End Sub

End Class
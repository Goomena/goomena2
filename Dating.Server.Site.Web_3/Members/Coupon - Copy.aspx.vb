﻿Imports Dating.Server.Core.DLL
Imports Dating.Server.Datasets.DLL

Public Class Coupon
    Inherits BasePage

    Private affCredits As clsReferrerCredits

    Protected Overrides Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        SetMasterPage(CurrentPageData)

        Try
            If clsCurrentContext.VerifyLogin() = True Then
                ''''  Me.MasterPageFile = "~/Members/Members2.Master"
            Else
                clsCurrentContext.ClearSession(Session)
                If (Not Me.IsCallback) Then
                    Response.Redirect(ResolveUrl("~/Login.aspx"), False)
                End If
                HttpContext.Current.ApplicationInstance.CompleteRequest()
            End If
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Protected Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData(Context, Me.Page, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If (Not Me.IsPostBack) Then
                Dim rpp As Boolean = ProfileHelper.GetRewardProgramParticipate(Me.MasterProfileId)
                If Not (rpp) Then
                    Response.Redirect("GiftsInfo.aspx")
                    Return
                End If
            End If
            LoadLAG()
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try


    End Sub


    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender

        Try
            Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try
        'Try
        '    If (Not Page.IsPostBack) Then
        '        If (ucCredits.HasCredits) Then
        '            CouponCreate.Visible = True
        '            CouponPreview.Visible = True
        '        End If
        '    End If
        'Catch ex As Exception
        '    WebErrorMessageBox(Me, ex, "Page_Load")
        'End Try
    End Sub

    Protected Sub LoadLAG()
        Try
            If (String.IsNullOrEmpty(CurrentPageData.GetCustomString("lnkNew"))) Then
                clsPageData.ClearCache()
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Private Sub ucCredits_CreateCouponEvent() Handles ucCredits.CreateCouponEvent
        Dim credits As Double = 0

        affCredits = GetReferrerCreditsObject(New DateTime(2010, 1, 1), DateTime.UtcNow.AddDays(1))

        Dim amount As Dictionary(Of String, Object) = affCredits.GetTotalPayment(Me.MasterProfileId)

        Dim LastPaymentAmount As Double = amount("LastPaymentAmount")
        Dim LastPaymentDate As DateTime = amount("LastPaymentDate")
        Dim TotalDebitAmount As Double = amount("TotalDebitAmount")
        Dim totalMoneyToBePaid As Double = affCredits.CommissionCreditsTotal - TotalDebitAmount
        Dim AvailableAmount As Double = affCredits.CommissionCreditsAvailable - TotalDebitAmount
        Dim PendingAmount As Double = affCredits.CommissionCreditsPending
        If (AvailableAmount < 0) Then
            PendingAmount = PendingAmount + AvailableAmount
            AvailableAmount = 0
        End If
        credits = AvailableAmount


        Dim referrer_payout_min_balance As Integer = -1
        Dim profile As DSMembers.EUS_ProfilesRow = DataHelpers.GetEUS_Profiles_ByProfileIDRow(Me.MasterProfileId)
        If (Not profile.IsREF_Payout_MinBalanceNull()) Then
            referrer_payout_min_balance = profile.REF_Payout_MinBalance
        Else
            referrer_payout_min_balance = clsConfigValues.Get__referrer_payout_min_balance()
        End If
        If (referrer_payout_min_balance > 0) Then
            credits = referrer_payout_min_balance
            '   min = referrer_payout_min_balance
        End If
        If referrer_payout_min_balance > AvailableAmount Then
            CouponCreate.MySpinEdit.Enabled = False
        End If



        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "AddOnchange", "$(function(){lnkGetCreditsID2 = '" & CouponCreate.lblClientId & "';lnkGetCreditsText2 = '';});", True)
        ucCredits.ShowCreditsAnalysis = False
        ucCredits.ShowCreditsHeader = False
        CouponPreview.Visible = False
        CouponCreate.MySpinEdit.MinValue = Math.Floor(referrer_payout_min_balance)
        CouponCreate.MySpinEdit.MaxValue = If(referrer_payout_min_balance > Math.Floor(AvailableAmount), referrer_payout_min_balance, Math.Floor(AvailableAmount))
        CouponCreate.MySpinEdit.Number = credits
        CouponCreate.Visible = True
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "UpdateMsg", "$(function(){  scrollWin('body', -200, 500);});", True)
    End Sub

    Protected Function GetReferrerCreditsObject(dateFrom As DateTime?, dateTo As DateTime?)
        If (affCredits Is Nothing) Then

            Try
                If (affCredits Is Nothing) Then
                    Dim _dtFrom As DateTime?
                    Dim _dtTo As DateTime?
                    If (dateFrom.HasValue) Then _dtFrom = dateFrom.Value.Date
                    If (dateTo.HasValue) Then _dtTo = dateTo.Value.Date.AddDays(1).AddMilliseconds(-1)

                    affCredits = New clsReferrerCredits(Me.MasterProfileId, Me.SessionVariables.MemberData.LoginName, _dtFrom, _dtTo)
                    affCredits.Load()
                End If

            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try
        End If

        Return affCredits
    End Function

    Private Sub CouponCreate_CreateCouponComplete(_credits As Integer, _name As String) Handles CouponCreate.CreateCouponComplete
        Try
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "UpdateMsg", "$(function(){ ShowLoading({ delay: 10, text: ''});});", True)
            Dim affCredits As clsReferrerCredits = New clsReferrerCredits(Me.MasterProfileId, Me.SessionVariables.MemberData.LoginName, New DateTime(2010, 1, 1), DateTime.UtcNow.AddDays(1))
            affCredits.Load()
            Dim amount As Dictionary(Of String, Object) = affCredits.GetTotalPayment(Me.MasterProfileId)
            Dim TotalDebitAmount As Double = amount("TotalDebitAmount")
            Dim AvailableAmount As Double = affCredits.CommissionCreditsAvailable - TotalDebitAmount
            If AvailableAmount >= _credits Then
                Dim _country As String = ""
                Try
                    Dim c As clsCountryByIP = clsCurrentContext.GetCountryByIP
                    If c IsNot Nothing Then
                        _country = c.RegionCode
                    End If
                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "")
                End Try

                Dim re As clsCreateCouponReturn = clsReferrerCredits.CreateCoupon(CInt(_credits), _name, Me.GetCurrentMasterProfile, clsCurrentContext.GetCurrentIP, _country, Session.SessionID)
                If re.Success Then
                    CouponPreview.Visible = True

                    CouponPreview.Credits = re.CreditAmmount
                    CouponPreview.CouponName = re.CouponName
                    CouponPreview.CouponCode = re.CouponCode
                    ucCredits.RefreshData()
                    CouponCreate.Visible = False
                    ucCredits.ShowCreditsAnalysis = True
                Else

                End If
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "UpdateMsg", "$(function(){ HideLoading();});", True)

            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


End Class

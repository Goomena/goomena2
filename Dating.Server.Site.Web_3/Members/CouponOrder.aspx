﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Members/Members2.Master" CodeBehind="CouponOrder.aspx.vb" Inherits="Dating.Server.Site.Web.CouponOrder" %>

<%@ Register Src="~/UserControls/gifts/ucRewardGiftsCards.ascx" TagPrefix="uc1" TagName="ucRewardGiftsCards" %>
<%@ Register Src="~/UserControls/gifts/ucRewardGiftsAvaliable.ascx" TagPrefix="uc1" TagName="ucRewardGiftsAvaliable" %>
<%@ Register Src="~/UserControls/gifts/ucRewardGiftCardConfirm.ascx" TagPrefix="uc1" TagName="ucRewardGiftCardConfirm" %>




<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../v1/CSS/GiftsStyle.css?v4" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="content" runat="server">

    <asp:Panel ID="pnlSelectGiftCard" runat="server">
        <div id="SelectGiftCardContainer">
               <div class="crtlConfirm">
                   <uc1:ucRewardGiftCardConfirm runat="server" ID="ucRewardGiftCardConfirm" />
            </div>
            

            <div class="crtlSelectGiftCard">
                <uc1:ucRewardGiftsAvaliable runat="server" ID="ucRewardGiftsAvaliable" />
             <%--    <uc1:ucRewardGiftsCards runat="server" ID="ucRewardGiftsCards" />--%>
              <script type="text/javascript" >


                  $('body').on("click", "#SelectGiftCardContainer .ReadMoreLink", function () {
                      $(".Bigtext").hide();
                      $(this).siblings(".Bigtext").fadeIn(200);
                  });
                  $('body').on("click", "#SelectGiftCardContainer .ClosebtnBigText", function () {
                      $(".Bigtext").fadeOut(100);
                  });

                  var OnPopup = false;
                  var OnButton = false;
                  $('body').on("mouseover", "#SelectGiftCardContainer .btnSelectGiftCard.Disabled", function () {
                      OnButton = true;
                      console.log("Mouseover Button");
                      if ($(this).siblings(".SmallPopupNotEnoughCreditsContainer").is(":visible")!=true){
                          $(".SmallPopupNotEnoughCreditsContainer").hide();
                      
                      $(this).siblings(".SmallPopupNotEnoughCreditsContainer").fadeIn(200);}
                  });
                  $('body').on("mouseover", "#SelectGiftCardContainer .SmallPopupNotEnoughCreditsContainer", function () {
                      OnPopup = true;
                      console.log("Mouseover Popup");
                  });
                  $('body').on("mouseleave", "#SelectGiftCardContainer .btnSelectGiftCard.Disabled", function () {
                      OnButton = false;
                      console.log("MouseLeave button");
                      setTimeout(function () {
                      if (OnPopup == false && OnButton==false) {
                          $(".SmallPopupNotEnoughCreditsContainer").fadeOut(100);
                      }
                  }, 100);
                  });
                  $('body').on("mouseleave", "#SelectGiftCardContainer .SmallPopupNotEnoughCreditsContainer", function () {
                      OnPopup = false;
                      console.log("MouseLeave Popup");
                      setTimeout(function () {
                          if (OnPopup == false && OnButton == false) {
                              $(".SmallPopupNotEnoughCreditsContainer").fadeOut(100)
                          }
                      }, 100);
                  });


                </script>
            </div>
           
        </div>
        
    </asp:Panel>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cntBodyBottom" runat="server">
</asp:Content>

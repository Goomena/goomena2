﻿Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Core.DLL
Imports Dating.Server.Datasets.DLL.DSMembers

Public Class MyLists
    Inherits BasePage


#Region "Props"


    Public Property MyListsSorting As MyListsSortEnum

        Get
            Dim sorting As MyListsSortEnum = MyListsSortEnum.Recent

            Dim index As Integer = cbSort.SelectedIndex
            If (index = 0) Then
                sorting = MyListsSortEnum.Recent
            ElseIf (index = 1) Then
                sorting = MyListsSortEnum.Oldest
            End If

            Return sorting
        End Get

        Set(value As MyListsSortEnum)
            Dim index As Integer = 0

            If (value = MyListsSortEnum.Recent) Then
                index = 0
            ElseIf (value = MyListsSortEnum.Oldest) Then
                index = 1
            End If
            cbSort.SelectedIndex = index
        End Set

    End Property

    Public Property ItemsPerPage As Integer

        Get
            Dim perPageNumber As Integer = 25

            Dim index As Integer = cbPerPage.SelectedIndex
            If (index = 0) Then
                perPageNumber = 10
            ElseIf (index = 1) Then
                perPageNumber = 25
            ElseIf (index = 2) Then
                perPageNumber = 50
            End If

            Return perPageNumber
        End Get

        Set(value As Integer)
            Dim index As Integer = 1

            If (value = 10) Then
                index = 0
            ElseIf (value = 50) Then
                index = 2
            Else
                index = 1
            End If

            cbPerPage.SelectedIndex = index
        End Set

    End Property



    'Dim _pageData As clsPageData
    'Protected ReadOnly Property CurrentPageData As clsPageData
    '    Get
    '        If (_pageData Is Nothing) Then _pageData = New clsPageData(Context)
    '        Return _pageData
    '    End Get
    'End Property


    Private ReadOnly Property CurrentOffersControl As SearchControl
        Get

            If (mvMyListsMain.GetActiveView().Equals(vwWhoViewedMe)) Then
                Return whoViewedMeList

            ElseIf (mvMyListsMain.GetActiveView().Equals(vwWhoFavoritedMe)) Then
                Return whoFavoritedMeList

            ElseIf (mvMyListsMain.GetActiveView().Equals(vwWhoSharedPhoto)) Then
                Return whoSharedPhotoList

            ElseIf (mvMyListsMain.GetActiveView().Equals(vwMyFavoriteList)) Then
                Return myFavoriteList

            ElseIf (mvMyListsMain.GetActiveView().Equals(vwMyBlockedList)) Then
                Return myBlockedList

            ElseIf (mvMyListsMain.GetActiveView().Equals(vwMyViewedList)) Then
                Return myViewedList

            End If

            Return Nothing
        End Get
    End Property

#End Region

    Protected Overrides Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        SetMasterPage(CurrentPageData)

        Try
            If clsCurrentContext.VerifyLogin() = True Then
                Me.MasterPageFile = "~/Members/Members2.Master"
                'If (Request("master") = "inner") Then
                '    Me.MasterPageFile = "~/Members/Members2Inner.Master"
                'Else
                'End If
            Else
                clsCurrentContext.ClearSession(Session)
                If (Not Me.IsCallback) Then
                    Response.Redirect(ResolveUrl("~/Login.aspx"), False)
                End If
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If (Not Page.IsPostBack) Then
                Try
                    Me.ItemsPerPage = clsProfilesPrivacySettings.GET_ItemsPerPage(Me.MasterProfileId)
                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "GET_ItemsPerPage")
                End Try

                Dim req As String = Request.QueryString("itemsPerPage")
                Dim _itemsPerPage As Integer
                If (Not String.IsNullOrEmpty(req)) Then
                    Integer.TryParse(req, _itemsPerPage)
                    Me.ItemsPerPage = _itemsPerPage
                End If


                req = Request.QueryString("sorting")
                Dim _cbSort As Integer
                If (Not String.IsNullOrEmpty(req)) Then
                    Integer.TryParse(req, _cbSort)
                    If (_cbSort >= 0 AndAlso _cbSort < cbSort.Items.Count) Then
                        cbSort.SelectedIndex = _cbSort
                    End If
                End If


                req = Request.QueryString("seo" & Me.Pager.ClientID)
                Dim currentPage As Integer
                If (Not String.IsNullOrEmpty(req)) Then
                    Try
                        req = req.Replace("page", "")
                        Integer.TryParse(req, currentPage)
                        Me.Pager.ItemCount = Me.ItemsPerPage * currentPage
                        Me.Pager.PageIndex = currentPage - 1
                    Catch ex As Exception
                    End Try
                End If

            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Try

            'Try
            '    Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            '    AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
            'Catch ex As Exception
            '    WebErrorMessageBox(Me, ex, "Page_Load")
            'End Try


            If (Not Page.IsPostBack) Then
                LoadLAG()
                LoadViews()
            End If


            'If (Not Page.IsPostBack) Then
            '                BindSearchResults()
            'End If

            AddHandler Me.CurrentOffersControl.Repeater.ItemCommand, AddressOf Me.offersSearchRepeater_ItemCommand
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender

        Try
            Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try


        Try

            Dim url As String = Request.Url.AbsolutePath
            url = url & "?"

            Dim seo As String = ("seo" & Me.Pager.ClientID).ToUpper()
            For Each itm As String In Request.QueryString.AllKeys
                If (Not String.IsNullOrEmpty(itm)) Then

                    Dim s As String = itm.ToUpper()
                    If (s <> seo AndAlso s <> "ITEMSPERPAGE" AndAlso s <> "SORTING") Then
                        url = url & itm & "=" & HttpUtility.UrlEncode(Request.QueryString(itm)) & "&"
                    End If

                End If
            Next

            Me.Pager.SeoFriendly = True
            Me.Pager.SeoNavigateUrlFormatString = url & "seo" & Me.Pager.ClientID & "={0}&itemsPerPage=" & Me.ItemsPerPage & "&sorting=" & cbSort.SelectedIndex
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub


    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
        LoadViews()
    End Sub


    Protected Sub LoadLAG()
        Try

            lnkWhoViewedMe.Text = "<span></span>" & CurrentPageData.GetCustomString("lnkWhoViewedMe")
            lnkWhoFavoritedMe.Text = "<span></span>" & CurrentPageData.GetCustomString("lnkWhoFavoritedMe")
            lnkWhoSharedPhotos.Text = "<span></span>" & CurrentPageData.GetCustomString("lnkWhoSharedPhotos")
            lnkMyFavoriteList.Text = "<span></span>" & CurrentPageData.GetCustomString("lnkMyFavoriteList")
            lnkMyBlockedList.Text = "<span></span>" & CurrentPageData.GetCustomString("lnkMyBlockedList")
            lnkMyViewedList.Text = "<span></span>" & CurrentPageData.GetCustomString("lnkMyViewedList")

            lnkWhoViewedMe.ToolTip = CurrentPageData.GetCustomString("lnkWhoViewedMe")
            lnkWhoFavoritedMe.ToolTip = CurrentPageData.GetCustomString("lnkWhoFavoritedMe")
            lnkWhoSharedPhotos.ToolTip = CurrentPageData.GetCustomString("lnkWhoSharedPhotos")
            lnkMyFavoriteList.ToolTip = CurrentPageData.GetCustomString("lnkMyFavoriteList")
            lnkMyBlockedList.ToolTip = CurrentPageData.GetCustomString("lnkMyBlockedList")
            lnkMyViewedList.ToolTip = CurrentPageData.GetCustomString("lnkMyViewedList")

            cbSort.Items.Clear()
            cbSort.Items.Add(CurrentPageData.GetCustomString("cbSort_Recent"), "Recent")
            cbSort.Items(cbSort.Items.Count - 1).Selected = True
            cbSort.Items.Add(CurrentPageData.GetCustomString("cbSort_Oldest"), "Oldest")

            Try
                If (Not Page.IsPostBack) Then
                    Me.MyListsSorting = MyListsSortEnum.Recent

                    Dim req As String = Request.QueryString("sorting")
                    Dim _cbSort As Integer
                    If (Not String.IsNullOrEmpty(req)) Then
                        Integer.TryParse(req, _cbSort)
                        If (_cbSort >= 0 AndAlso _cbSort < cbSort.Items.Count) Then
                            cbSort.SelectedIndex = _cbSort
                        End If
                    End If

                End If
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try

            lblSortByText.Text = CurrentPageData.GetCustomString("lblSortByText")
            lblResultsPerPageText.Text = CurrentPageData.GetCustomString("lblResultsPerPageText")

            lblItemsName.Text = Me.CurrentPageData.GetCustomString("CartMyListsView")
            lnkViewDescription.Text = Me.CurrentPageData.GetCustomString("CartMyListsWhatIsIt")
            WhatIsIt1.PopupControl.HeaderText = Me.CurrentPageData.GetCustomString("CartMyListsView")
            lnkViewDescription.OnClientClick = OnMoreInfoClickFunc(
                lnkViewDescription.OnClientClick,
                ResolveUrl("~/Members/InfoWin.aspx?info=mylists"),
                Me.CurrentPageData.GetCustomString("CartMyListsView"))


        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Private Sub LoadViews()

        Try

            If (Request.QueryString("vw") IsNot Nothing) Then
                If (Request.QueryString("vw").ToUpper() = "WHOFAVORITEDME") Then
                    mvMyListsMain.SetActiveView(vwWhoFavoritedMe)

                ElseIf (Request.QueryString("vw").ToUpper() = "MYFAVORITELIST") Then
                    mvMyListsMain.SetActiveView(vwMyFavoriteList)

                ElseIf (Request.QueryString("vw").ToUpper() = "WHOSHAREDPHOTOS") Then
                    mvMyListsMain.SetActiveView(vwWhoSharedPhoto)

                ElseIf (Request.QueryString("vw").ToUpper() = "MYBLOCKEDLIST") Then
                    mvMyListsMain.SetActiveView(vwMyBlockedList)

                ElseIf (Request.QueryString("vw").ToUpper() = "MYVIEWEDLIST") Then
                    mvMyListsMain.SetActiveView(vwMyViewedList)

                ElseIf (Request.QueryString("vw").ToUpper() = "WHOVIEWEDME") Then
                    mvMyListsMain.SetActiveView(vwWhoViewedMe)

                End If
            Else
                mvMyListsMain.SetActiveView(vwWhoViewedMe)
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


    Protected Sub offersSearchRepeater_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs)
        Dim success = False
        Try
            'Dim drDefaultPhoto As EUS_CustomerPhotosRow = DataHelpers.GetProfilesDefaultPhoto(Me.MasterProfileId)
            Dim currentUserHasPhotos As Boolean = clsUserDoes.HasPhotos(Me.MasterProfileId, Me.MirrorProfileId)

            If (currentUserHasPhotos) Then
                If (e.CommandName = OfferControlCommandEnum.WINK.ToString()) Then

                    If (Not String.IsNullOrEmpty(e.CommandArgument)) Then

                        Try
                            Dim ToProfileID As Integer
                            Integer.TryParse(e.CommandArgument.ToString(), ToProfileID)

                            ''clsUserDoes.SendWink(ToProfileID, Me.MasterProfileId)
                            ''success = True
                            'Dim MaySendWink As Boolean = True

                            'If (Me.IsFemale) Then
                            '    If (Me.IsReferrer) Then
                            '        MaySendWink = clsUserDoes.IsReferrerAllowedSendWink(ToProfileID, Me.MasterProfileId)
                            '    End If
                            'End If

                            'If (MaySendWink) Then
                            '    clsUserDoes.SendWink(ToProfileID, Me.MasterProfileId)
                            'Else
                            '    Dim popupMessage As String = WhatIsIt.OnMoreInfoClickFunc(ResolveUrl("~/Members/InfoWin.aspx?info=Error_Sending_Receiver_DoNot_Have_Photo"), "Error! Can't send", 650, 350)
                            '    popupMessage = "function fnError_Sending_Receiver_DoNot_Have_Photo(){" & popupMessage & "};"
                            '    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Error_Sending_Receiver_DoNot_Have_Photo", popupMessage, True)
                            'End If
                            Dim MaySendWink As Boolean = True
                            Dim MaySendWink_CheckSetting As Boolean = True


                            MaySendWink_CheckSetting = Not clsProfilesPrivacySettings.GET_DisableReceivingLIKESFromDifferentCountryFromTo(ToProfileID, Me.MasterProfileId)
                            If (MaySendWink_CheckSetting = False) Then
                                MaySendWink = False
                            End If


                            If (MaySendWink AndAlso Me.IsFemale) Then
                                If (Me.IsReferrer) Then
                                    MaySendWink = clsUserDoes.IsReferrerAllowedSendWink(ToProfileID, Me.MasterProfileId)
                                End If
                            End If

                            If (MaySendWink) Then
                                clsUserDoes.SendWink(ToProfileID, Me.MasterProfileId)
                            Else
                                If (MaySendWink_CheckSetting = False) Then
                                    Dim popupMessage As String = WhatIsIt.OnMoreInfoClickFunc(ResolveUrl("~/Members/InfoWin.aspx?info=Error_Sending_Receiver_Disabled_Likes_From_Diff_Country&popup=popupWhatIs"), "Error! Can't send", 650, 350)
                                    popupMessage = "function fnError_Sending_Receiver_DoNot_Have_Photo(){" & popupMessage & "};"
                                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Error_Sending_Receiver_DoNot_Have_Photo", popupMessage, True)
                                Else
                                    Dim popupMessage As String = WhatIsIt.OnMoreInfoClickFunc(ResolveUrl("~/Members/InfoWin.aspx?info=Error_Sending_Receiver_DoNot_Have_Photo&popup=popupWhatIs"), "Error! Can't send", 650, 350)
                                    popupMessage = "function fnError_Sending_Receiver_DoNot_Have_Photo(){" & popupMessage & "};"
                                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Error_Sending_Receiver_DoNot_Have_Photo", popupMessage, True)
                                End If
                            End If
                            success = MaySendWink

                        Catch ex As Exception
                            WebErrorMessageBox(Me, ex, "")
                        End Try

                    End If

                ElseIf (e.CommandName = OfferControlCommandEnum.FAVORITE.ToString()) Then

                    Dim _userId As Integer = CType(e.CommandArgument, Integer)
                    If (_userId > 0) Then
                        Try
                            clsUserDoes.MarkAsFavorite(_userId, Me.MasterProfileId)
                            success = True
                        Catch ex As Exception
                            WebErrorMessageBox(Me, ex, "")
                        End Try
                    End If

                ElseIf (e.CommandName = OfferControlCommandEnum.UNFAVORITE.ToString()) Then

                    Dim _userId As Integer = CType(e.CommandArgument, Integer)
                    If (_userId > 0) Then
                        Try
                            clsUserDoes.MarkAsUnfavorite(_userId, Me.MasterProfileId)

                            success = True
                        Catch ex As Exception
                            WebErrorMessageBox(Me, ex, "")
                        End Try
                    End If


                ElseIf (e.CommandName = OfferControlCommandEnum.UNBLOCK.ToString()) Then

                    Dim _userId As Integer = CType(e.CommandArgument, Integer)
                    If (_userId > 0) Then
                        Try
                            clsUserDoes.MarkAsUnblocked(_userId, Me.MasterProfileId)
                            success = True
                        Catch ex As Exception
                            WebErrorMessageBox(Me, ex, "")
                        End Try
                    End If

                End If
            Else
                Response.Redirect("~/Members/Photos.aspx", False)
                HttpContext.Current.ApplicationInstance.CompleteRequest()
            End If



        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try


        Try
            If (success) Then
                BindSearchResults()
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


    'Protected Sub lnkWhoViewedMe_Click(sender As Object, e As EventArgs) Handles lnkWhoViewedMe.Click
    '    mvMyListsMain.SetActiveView(vwWhoViewedMe)
    'End Sub


    'Protected Sub lnkWhoFavoritedMe_Click(sender As Object, e As EventArgs) Handles lnkWhoFavoritedMe.Click
    '    mvMyListsMain.SetActiveView(vwWhoFavoritedMe)
    'End Sub

    'Protected Sub lnkWhoSharedPhotos_Click(sender As Object, e As EventArgs) Handles lnkWhoSharedPhotos.Click
    '    mvMyListsMain.SetActiveView(vwWhoSharedPhoto)
    'End Sub

    'Protected Sub lnkMyFavoriteList_Click(sender As Object, e As EventArgs) Handles lnkMyFavoriteList.Click
    '    mvMyListsMain.SetActiveView(vwMyFavoriteList)
    'End Sub


    'Protected Sub lnkMyBlockedList_Click(sender As Object, e As EventArgs) Handles lnkMyBlockedList.Click
    '    mvMyListsMain.SetActiveView(vwMyBlockedList)
    'End Sub


    'Protected Sub lnkMyViewedList_Click(sender As Object, e As EventArgs) Handles lnkMyViewedList.Click
    '    mvMyListsMain.SetActiveView(vwMyViewedList)
    'End Sub



    Protected Sub mvMyListsMain_ActiveViewChanged(sender As Object, e As EventArgs) Handles mvMyListsMain.ActiveViewChanged
        BindSearchResults()
    End Sub



    Protected Sub Pager_PageIndexChanged(sender As Object, e As EventArgs) Handles Pager.PageIndexChanged
        BindSearchResults()
    End Sub


    Protected Sub ddlSort_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbSort.SelectedIndexChanged
        Me.Pager.PageIndex = 0
        BindSearchResults()
    End Sub

    Protected Sub ddlPerPage_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbPerPage.SelectedIndexChanged
        'Me.Pager.PageIndex = 0
        'BindSearchResults()
        Try
            clsProfilesPrivacySettings.Update_ItemsPerPage(Me.MasterProfileId, Me.ItemsPerPage)
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorSendEmail(ex, "")
        End Try
        Try
            Me.Pager.PageIndex = 0
            BindSearchResults()
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(ex, "")
        End Try
    End Sub



    Sub SetPager(itemsCount As Integer)
        Me.Pager.ItemCount = itemsCount
        Me.Pager.ItemsPerPage = Me.ItemsPerPage
        Me.Pager.Visible = (itemsCount > Me.ItemsPerPage)

        If (Me.Pager.PageIndex = -1) AndAlso (itemsCount > 0) Then
            Me.Pager.PageIndex = 0
        End If
    End Sub

    Function GetRecordsToRetrieve() As Integer
        Me.Pager.ItemsPerPage = Me.ItemsPerPage

        If (Me.Pager.PageIndex = -1) AndAlso (Me.Pager.ItemCount > 0) Then
            Me.Pager.PageIndex = 0
        End If

        If (Me.Pager.PageIndex > 0) Then
            Return (Me.Pager.ItemsPerPage * (Me.Pager.PageIndex + 1))
        End If
        Return Me.Pager.ItemsPerPage
    End Function


    Private Sub BindWhoViewedMe()
        Try

            divFilter.Visible = False
            whoViewedMeList.ShowEmptyListText = True

            If (Me.GetCurrentProfile(True) IsNot Nothing) Then


                Dim dc As CMSDBDataContext = Me.CMSDBDataContext

                ' bind new offers
                Try

                    'Dim dt As DataTable = clsSearchHelper.GetWhoViewedMeMembersDataTable(Me.MyListsSorting,
                    '                                                                     Me.MasterProfileId, 0,
                    '                                                                     ProfileStatusEnum.Approved,
                    '                                                                     Me.SessionVariables.MemberData.Zip,
                    '                                                                     Me.SessionVariables.MemberData.latitude,
                    '                                                                     Me.SessionVariables.MemberData.longitude)

                    'If (dt.Rows.Count > 0) Then
                    '    FillOfferControlList(dt, whoViewedMeList)
                    '    whoViewedMeList.ShowEmptyListText = False
                    '    divFilter.Visible = True
                    'End If


                    'whoViewedMeList.DataBind()
                    'SetPager(dt.Rows.Count)


                    Dim countRows As Integer
                    Dim performCount As Boolean = True
                    Dim NumberOfRecordsToReturn As Integer = Me.GetRecordsToRetrieve()

                    Dim parms As New clsMyListsHelperParameters()
                    parms.CurrentProfileId = Me.MasterProfileId
                    parms.sorting = Me.MyListsSorting
                    parms.ReturnRecordsWithStatus = ProfileStatusEnum.Approved
                    parms.zipstr = Me.SessionVariables.MemberData.Zip
                    parms.latitudeIn = Me.SessionVariables.MemberData.latitude
                    parms.longitudeIn = Me.SessionVariables.MemberData.longitude
                    parms.Distance = clsSearchHelper.DISTANCE_DEFAULT
                    'parms.NumberOfRecordsToReturn = NumberOfRecordsToReturn
                    parms.performCount = True
                    parms.rowNumberMin = NumberOfRecordsToReturn - Me.ItemsPerPage
                    parms.rowNumberMax = NumberOfRecordsToReturn

                    Dim ds As DataSet = clsSearchHelper.GetWhoViewedMeMembersDataTable(parms)

                    If (performCount) Then
                        countRows = ds.Tables(0).Rows(0)(0)
                        If (countRows > 0) Then
                            Dim dt As DataTable = ds.Tables(1)
                            FillOfferControlList(dt, whoViewedMeList)
                            whoViewedMeList.ShowEmptyListText = False
                            divFilter.Visible = True
                        End If
                    Else
                        countRows = ds.Tables(0).Rows.Count
                        If (countRows > 0) Then
                            Dim dt As DataTable = ds.Tables(0)
                            FillOfferControlList(dt, whoViewedMeList)
                            whoViewedMeList.ShowEmptyListText = False
                            divFilter.Visible = True
                        End If
                    End If

                    whoViewedMeList.DataBind()
                    SetPager(countRows)

                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "")

                    SetPager(0)
                    whoViewedMeList.UsersList = Nothing
                    whoViewedMeList.DataBind()
                End Try


                Try

                    ' update EUS_ProfilesViewed records set IsToProfileIDViewed to true, where  ToProfileID is current member
                    Dim usersInList As List(Of Integer) = (From itm In whoViewedMeList.UsersList
                                                          Select itm.OtherMemberProfileID).ToList()
                    If (usersInList.Count > 0) Then
                        Dim recs = From itm In Me.CMSDBDataContext.EUS_ProfilesVieweds
                                   Where itm.ToProfileID = Me.MasterProfileId AndAlso usersInList.Contains(itm.FromProfileID) AndAlso _
                                   (itm.IsToProfileIDViewed Is Nothing OrElse itm.IsToProfileIDViewed = False)
                                   Select itm

                        For Each itm As EUS_ProfilesViewed In recs
                            itm.IsToProfileIDViewed = True
                        Next
                        Me.CMSDBDataContext.SubmitChanges()

                        clsUserDoes.ResetMemberCounter(MemberCountersEnum.WhoViewedMe, Me.MasterProfileId)
                    End If

                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "")
                End Try

            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Private Sub BindWhoFavoritedMe()
        Try

            divFilter.Visible = False
            whoFavoritedMeList.ShowEmptyListText = True

            If (Me.GetCurrentProfile(True) IsNot Nothing) Then


                Dim dc As CMSDBDataContext = Me.CMSDBDataContext
                Try
                    'Dim dt As DataTable = clsSearchHelper.GetWhoFavoritedMeMembersDataTable(Me.MyListsSorting,
                    '                                                                        Me.MasterProfileId, 0,
                    '                                                                        ProfileStatusEnum.Approved,
                    '                                                                        Me.SessionVariables.MemberData.Zip,
                    '                                                                        Me.SessionVariables.MemberData.latitude,
                    '                                                                        Me.SessionVariables.MemberData.longitude)

                    'If (dt.Rows.Count > 0) Then
                    '    FillOfferControlList(dt, whoFavoritedMeList)
                    '    whoFavoritedMeList.ShowEmptyListText = False
                    '    divFilter.Visible = True
                    'End If

                    'whoFavoritedMeList.DataBind()
                    'SetPager(dt.Rows.Count)


                    Dim countRows As Integer
                    Dim performCount As Boolean = True
                    Dim NumberOfRecordsToReturn As Integer = Me.GetRecordsToRetrieve()

                    Dim parms As New clsMyListsHelperParameters()
                    parms.CurrentProfileId = Me.MasterProfileId
                    parms.sorting = Me.MyListsSorting
                    parms.ReturnRecordsWithStatus = ProfileStatusEnum.Approved
                    parms.zipstr = Me.SessionVariables.MemberData.Zip
                    parms.latitudeIn = Me.SessionVariables.MemberData.latitude
                    parms.longitudeIn = Me.SessionVariables.MemberData.longitude
                    parms.Distance = clsSearchHelper.DISTANCE_DEFAULT
                    'parms.NumberOfRecordsToReturn = NumberOfRecordsToReturn
                    parms.performCount = True
                    parms.rowNumberMin = NumberOfRecordsToReturn - Me.ItemsPerPage
                    parms.rowNumberMax = NumberOfRecordsToReturn

                    Dim ds As DataSet = clsSearchHelper.GetWhoFavoritedMeMembersDataTable(parms)

                    If (performCount) Then
                        countRows = ds.Tables(0).Rows(0)(0)
                        If (countRows > 0) Then
                            Dim dt As DataTable = ds.Tables(1)
                            FillOfferControlList(dt, whoFavoritedMeList)
                            whoFavoritedMeList.ShowEmptyListText = False
                            divFilter.Visible = True
                        End If
                    Else
                        countRows = ds.Tables(0).Rows.Count
                        If (countRows > 0) Then
                            Dim dt As DataTable = ds.Tables(0)
                            FillOfferControlList(dt, whoFavoritedMeList)
                            whoFavoritedMeList.ShowEmptyListText = False
                            divFilter.Visible = True
                        End If
                    End If

                    whoFavoritedMeList.DataBind()
                    SetPager(countRows)


                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "")

                    SetPager(0)
                    whoFavoritedMeList.UsersList = Nothing
                    whoFavoritedMeList.DataBind()
                End Try


                Try

                    ' update EUS_ProfilesViewed records set IsToProfileIDViewed to true, where  ToProfileID is current member
                    Dim usersInList As List(Of Integer) = (From itm In whoFavoritedMeList.UsersList
                                                          Select itm.OtherMemberProfileID).ToList()
                    If (usersInList.Count > 0) Then
                        Dim recs = From itm In Me.CMSDBDataContext.EUS_ProfilesFavorites
                                   Where itm.ToProfileID = Me.MasterProfileId AndAlso usersInList.Contains(itm.FromProfileID) AndAlso _
                                   (itm.IsToProfileIDViewed Is Nothing OrElse itm.IsToProfileIDViewed = False)
                                   Select itm

                        For Each itm As EUS_ProfilesFavorite In recs
                            itm.IsToProfileIDViewed = True
                        Next
                        Me.CMSDBDataContext.SubmitChanges()

                        clsUserDoes.ResetMemberCounter(MemberCountersEnum.WhoFavoritedMe, Me.MasterProfileId)
                    End If

                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "")
                End Try


            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Private Sub BindWhoSharedPhoto()
        Try

            divFilter.Visible = False
            whoSharedPhotoList.ShowEmptyListText = True

            If (Me.GetCurrentProfile(True) IsNot Nothing) Then


                Dim dc As CMSDBDataContext = Me.CMSDBDataContext
                Try
                    'Dim dt As DataTable = clsSearchHelper.GetWhoSharedPhotoMembersDataTable(Me.MyListsSorting,
                    '                                                                        Me.MasterProfileId, 0,
                    '                                                                        ProfileStatusEnum.Approved,
                    '                                                                        Me.SessionVariables.MemberData.Zip,
                    '                                                                        Me.SessionVariables.MemberData.latitude,
                    '                                                                        Me.SessionVariables.MemberData.longitude)

                    'If (dt.Rows.Count > 0) Then
                    '    FillOfferControlList(dt, whoSharedPhotoList)
                    '    whoSharedPhotoList.ShowEmptyListText = False
                    '    divFilter.Visible = True
                    'End If

                    'whoSharedPhotoList.DataBind()
                    'SetPager(dt.Rows.Count)



                    Dim countRows As Integer
                    Dim performCount As Boolean = True
                    Dim NumberOfRecordsToReturn As Integer = Me.GetRecordsToRetrieve()

                    Dim parms As New clsMyListsHelperParameters()
                    parms.CurrentProfileId = Me.MasterProfileId
                    parms.sorting = Me.MyListsSorting
                    parms.ReturnRecordsWithStatus = ProfileStatusEnum.Approved
                    parms.zipstr = Me.SessionVariables.MemberData.Zip
                    parms.latitudeIn = Me.SessionVariables.MemberData.latitude
                    parms.longitudeIn = Me.SessionVariables.MemberData.longitude
                    parms.Distance = clsSearchHelper.DISTANCE_DEFAULT
                    'parms.NumberOfRecordsToReturn = NumberOfRecordsToReturn
                    parms.performCount = True
                    parms.rowNumberMin = NumberOfRecordsToReturn - Me.ItemsPerPage
                    parms.rowNumberMax = NumberOfRecordsToReturn

                    Dim ds As DataSet = clsSearchHelper.GetWhoSharedPhotoMembersDataTable(parms)

                    If (performCount) Then
                        countRows = ds.Tables(0).Rows(0)(0)
                        If (countRows > 0) Then
                            Dim dt As DataTable = ds.Tables(1)
                            FillOfferControlList(dt, whoSharedPhotoList)
                            whoSharedPhotoList.ShowEmptyListText = False
                            divFilter.Visible = True
                        End If
                    Else
                        countRows = ds.Tables(0).Rows.Count
                        If (countRows > 0) Then
                            Dim dt As DataTable = ds.Tables(0)
                            FillOfferControlList(dt, whoSharedPhotoList)
                            whoSharedPhotoList.ShowEmptyListText = False
                            divFilter.Visible = True
                        End If
                    End If

                    whoSharedPhotoList.DataBind()
                    SetPager(countRows)


                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "")

                    SetPager(0)
                    whoSharedPhotoList.UsersList = Nothing
                    whoSharedPhotoList.DataBind()
                End Try


                Try

                    ' update EUS_ProfilesViewed records set IsToProfileIDViewed to true, where  ToProfileID is current member
                    Dim usersInList As List(Of Integer) = (From itm In whoSharedPhotoList.UsersList
                                                          Select itm.OtherMemberProfileID).ToList()
                    If (usersInList.Count > 0) Then
                        Dim ctx As New CMSDBDataContext(ModGlobals.ConnectionString)
                        Try
                            'Dim sql As String = <sq><![CDATA[
                            'SELECT [ProfilePhotosLevelID], [FromProfileID], [ToProfileID], [PhotoLevelID], [DateTimeCreated], [IsToProfileIDViewed]
                            'FROM([dbo].[EUS_ProfilePhotosLevel])
                            'WHERE ([ToProfileID] = [@ToProfileID]) 
                            'AND ISNULL([IsToProfileIDViewed],0)=0
                            'AND [FromProfileID] IN ([@FromProfileID])
                            ']]></sq>
                            'Dim sb As New System.Text.StringBuilder()
                            'For cnt = 0 To usersInList.Count - 1
                            '    sb.Append(usersInList(cnt) & ",")
                            'Next
                            'sb.Remove(sb.Length - 1, 1)
                            'sql = sql.Replace("[@ToProfileID]", Me.MasterProfileId)
                            'sql = sql.Replace("[@FromProfileID]", sb.ToString())

                            'Dim recs1 As List(Of EUS_ProfilePhotosLevel) = ctx.ExecuteQuery(Of EUS_ProfilePhotosLevel)(sql, {}).ToList()
                            ''(From itm In ctx.EUS_ProfilePhotosLevels
                            ''Where itm.ToProfileID = Me.MasterProfileId AndAlso (itm.IsToProfileIDViewed Is Nothing OrElse itm.IsToProfileIDViewed = False) AndAlso usersInList.Contains(itm.FromProfileID)
                            ''Select itm).ToList()
                            Dim recs As List(Of EUS_ProfilePhotosLevel) = (From itm In ctx.EUS_ProfilePhotosLevels
                                                                            Where itm.ToProfileID = Me.MasterProfileId AndAlso (itm.IsToProfileIDViewed Is Nothing OrElse itm.IsToProfileIDViewed = False) AndAlso usersInList.Contains(itm.FromProfileID)
                                                                            Select itm).ToList()

                            For Each itm As EUS_ProfilePhotosLevel In recs
                                itm.IsToProfileIDViewed = True
                            Next
                            ctx.SubmitChanges()

                            clsUserDoes.ResetMemberCounter(MemberCountersEnum.WhoSharedPhotos, Me.MasterProfileId)
                        Catch ex As Exception
                            WebErrorMessageBox(Me, ex, "Updating shared photos")
                        Finally
                            ctx.Dispose()
                        End Try
                    End If

                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "")
                End Try

            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Private Sub BindMyFavoriteList()
        Try

            divFilter.Visible = False
            myFavoriteList.ShowEmptyListText = True

            If (Me.GetCurrentProfile(True) IsNot Nothing) Then


                Dim dc As CMSDBDataContext = Me.CMSDBDataContext
                Try
                    'Dim dt As DataTable = clsSearchHelper.GetMyFavoriteMembersDataTable(Me.MyListsSorting,
                    '                                                                    Me.MasterProfileId, 0,
                    '                                                                    ProfileStatusEnum.Approved,
                    '                                                                    Me.SessionVariables.MemberData.Zip,
                    '                                                                    Me.SessionVariables.MemberData.latitude,
                    '                                                                    Me.SessionVariables.MemberData.longitude)

                    'If (dt.Rows.Count > 0) Then
                    '    FillOfferControlList(dt, myFavoriteList)
                    '    myFavoriteList.ShowEmptyListText = False
                    '    divFilter.Visible = True
                    'End If

                    'myFavoriteList.DataBind()
                    'SetPager(dt.Rows.Count)




                    Dim countRows As Integer
                    Dim performCount As Boolean = True
                    Dim NumberOfRecordsToReturn As Integer = Me.GetRecordsToRetrieve()

                    Dim parms As New clsMyListsHelperParameters()
                    parms.CurrentProfileId = Me.MasterProfileId
                    parms.sorting = Me.MyListsSorting
                    parms.ReturnRecordsWithStatus = ProfileStatusEnum.Approved
                    parms.zipstr = Me.SessionVariables.MemberData.Zip
                    parms.latitudeIn = Me.SessionVariables.MemberData.latitude
                    parms.longitudeIn = Me.SessionVariables.MemberData.longitude
                    parms.Distance = clsSearchHelper.DISTANCE_DEFAULT
                    'parms.NumberOfRecordsToReturn = NumberOfRecordsToReturn
                    parms.performCount = True
                    parms.rowNumberMin = NumberOfRecordsToReturn - Me.ItemsPerPage
                    parms.rowNumberMax = NumberOfRecordsToReturn

                    Dim ds As DataSet = clsSearchHelper.GetMyFavoriteMembersDataTable(parms)

                    If (performCount) Then
                        countRows = ds.Tables(0).Rows(0)(0)
                        If (countRows > 0) Then
                            Dim dt As DataTable = ds.Tables(1)
                            FillOfferControlList(dt, myFavoriteList)
                            myFavoriteList.ShowEmptyListText = False
                            divFilter.Visible = True
                        End If
                    Else
                        countRows = ds.Tables(0).Rows.Count
                        If (countRows > 0) Then
                            Dim dt As DataTable = ds.Tables(0)
                            FillOfferControlList(dt, myFavoriteList)
                            myFavoriteList.ShowEmptyListText = False
                            divFilter.Visible = True
                        End If
                    End If

                    myFavoriteList.DataBind()
                    SetPager(countRows)


                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "")

                    SetPager(0)
                    myFavoriteList.UsersList = Nothing
                    myFavoriteList.DataBind()
                End Try
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Private Sub BindMyBlockedList()
        Try

            divFilter.Visible = False
            myBlockedList.ShowEmptyListText = True

            If (Me.GetCurrentProfile(True) IsNot Nothing) Then


                Dim dc As CMSDBDataContext = Me.CMSDBDataContext
                Try
                    'Dim dt As DataTable = clsSearchHelper.GetMyBlockedMembersDataTable(Me.MyListsSorting, Me.MasterProfileId, 0,
                    '                                                                   ProfileStatusEnum.Approved,
                    '                                                                   Me.SessionVariables.MemberData.Zip,
                    '                                                                    Me.SessionVariables.MemberData.latitude,
                    '                                                                    Me.SessionVariables.MemberData.longitude)

                    'If (dt.Rows.Count > 0) Then
                    '    FillOfferControlList(dt, myBlockedList)
                    '    myBlockedList.ShowEmptyListText = False
                    '    divFilter.Visible = True
                    'End If

                    'myBlockedList.DataBind()
                    'SetPager(dt.Rows.Count)



                    Dim countRows As Integer
                    Dim performCount As Boolean = True
                    Dim NumberOfRecordsToReturn As Integer = Me.GetRecordsToRetrieve()

                    Dim parms As New clsMyListsHelperParameters()
                    parms.CurrentProfileId = Me.MasterProfileId
                    parms.sorting = Me.MyListsSorting
                    parms.ReturnRecordsWithStatus = ProfileStatusEnum.Approved
                    parms.zipstr = Me.SessionVariables.MemberData.Zip
                    parms.latitudeIn = Me.SessionVariables.MemberData.latitude
                    parms.longitudeIn = Me.SessionVariables.MemberData.longitude
                    parms.Distance = clsSearchHelper.DISTANCE_DEFAULT
                    'parms.NumberOfRecordsToReturn = NumberOfRecordsToReturn
                    parms.performCount = True
                    parms.rowNumberMin = NumberOfRecordsToReturn - Me.ItemsPerPage
                    parms.rowNumberMax = NumberOfRecordsToReturn

                    Dim ds As DataSet = clsSearchHelper.GetMyBlockedMembersDataTable(parms)

                    If (performCount) Then
                        countRows = ds.Tables(0).Rows(0)(0)
                        If (countRows > 0) Then
                            Dim dt As DataTable = ds.Tables(1)
                            FillOfferControlList(dt, myBlockedList)
                            myBlockedList.ShowEmptyListText = False
                            divFilter.Visible = True
                        End If
                    Else
                        countRows = ds.Tables(0).Rows.Count
                        If (countRows > 0) Then
                            Dim dt As DataTable = ds.Tables(0)
                            FillOfferControlList(dt, myBlockedList)
                            myBlockedList.ShowEmptyListText = False
                            divFilter.Visible = True
                        End If
                    End If

                    myBlockedList.DataBind()
                    SetPager(countRows)


                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "")

                    SetPager(0)
                    myBlockedList.UsersList = Nothing
                    myBlockedList.DataBind()
                End Try
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Private Sub BindMyViewedList()
        Try

            divFilter.Visible = False
            myViewedList.ShowEmptyListText = True

            If (Me.GetCurrentProfile(True) IsNot Nothing) Then


                Dim dc As CMSDBDataContext = Me.CMSDBDataContext
                Try
                    'Dim dt As DataTable = clsSearchHelper.GetMyViewedMembersDataTable(Me.MyListsSorting,
                    '                                                                  Me.MasterProfileId, 0,
                    '                                                                  ProfileStatusEnum.Approved,
                    '                                                                  Me.SessionVariables.MemberData.Zip,
                    '                                                                  Me.SessionVariables.MemberData.latitude,
                    '                                                                  Me.SessionVariables.MemberData.longitude)

                    'If (dt.Rows.Count > 0) Then
                    '    FillOfferControlList(dt, myViewedList)
                    '    myViewedList.ShowEmptyListText = False
                    '    divFilter.Visible = True
                    'End If

                    'myViewedList.DataBind()
                    'SetPager(dt.Rows.Count)



                    Dim countRows As Integer
                    Dim performCount As Boolean = True
                    Dim NumberOfRecordsToReturn As Integer = Me.GetRecordsToRetrieve()

                    Dim parms As New clsMyListsHelperParameters()
                    parms.CurrentProfileId = Me.MasterProfileId
                    parms.sorting = Me.MyListsSorting
                    parms.ReturnRecordsWithStatus = ProfileStatusEnum.Approved
                    parms.zipstr = Me.SessionVariables.MemberData.Zip
                    parms.latitudeIn = Me.SessionVariables.MemberData.latitude
                    parms.longitudeIn = Me.SessionVariables.MemberData.longitude
                    parms.Distance = clsSearchHelper.DISTANCE_DEFAULT
                    'parms.NumberOfRecordsToReturn = NumberOfRecordsToReturn
                    parms.performCount = True
                    parms.rowNumberMin = NumberOfRecordsToReturn - Me.ItemsPerPage
                    parms.rowNumberMax = NumberOfRecordsToReturn

                    Dim ds As DataSet = clsSearchHelper.GetMyViewedMembersDataTable(parms)

                    If (performCount) Then
                        countRows = ds.Tables(0).Rows(0)(0)
                        If (countRows > 0) Then
                            Dim dt As DataTable = ds.Tables(1)
                            FillOfferControlList(dt, myViewedList)
                            myViewedList.ShowEmptyListText = False
                            divFilter.Visible = True
                        End If
                    Else
                        countRows = ds.Tables(0).Rows.Count
                        If (countRows > 0) Then
                            Dim dt As DataTable = ds.Tables(0)
                            FillOfferControlList(dt, myViewedList)
                            myViewedList.ShowEmptyListText = False
                            divFilter.Visible = True
                        End If
                    End If

                    myViewedList.DataBind()
                    SetPager(countRows)


                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "")

                    SetPager(0)
                    myViewedList.UsersList = Nothing
                    myViewedList.DataBind()
                End Try
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Private Sub BindSearchResults()

        'liWhoFavoritedMe.Visible = True
        'liWhoViewedMe.Visible = True
        'liWhoSharedPhotos.Visible = False
        'liMyFavoriteList.Visible = True
        'liMyBlockedList.Visible = True
        'liMyViewedList.Visible = True

        If (mvMyListsMain.GetActiveView().Equals(vwWhoViewedMe)) Then

            liWhoViewedMe.Attributes.Add("class", "down")
            liWhoFavoritedMe.Attributes.Remove("class")
            liMyFavoriteList.Attributes.Remove("class")
            liMyBlockedList.Attributes.Remove("class")
            liMyViewedList.Attributes.Remove("class")
            liWhoSharedPhotos.Attributes.Remove("class")

            whoViewedMeList.InfoWinHeaderText = CurrentPageData.GetCustomString("lnkWhoViewedMe")
            whoViewedMeList.InfoWinUrl = ResolveUrl("~/Members/InfoWin.aspx?info=WhoViewedMeCommands")

            BindWhoViewedMe()

        ElseIf (mvMyListsMain.GetActiveView().Equals(vwWhoFavoritedMe)) Then

            liWhoFavoritedMe.Attributes.Add("class", "down")
            liWhoViewedMe.Attributes.Remove("class")
            liMyFavoriteList.Attributes.Remove("class")
            liMyBlockedList.Attributes.Remove("class")
            liMyViewedList.Attributes.Remove("class")
            liWhoSharedPhotos.Attributes.Remove("class")

            whoFavoritedMeList.InfoWinHeaderText = CurrentPageData.GetCustomString("lnkWhoFavoritedMe")
            whoFavoritedMeList.InfoWinUrl = ResolveUrl("~/Members/InfoWin.aspx?info=WhoFavoritedMeCommands")

            BindWhoFavoritedMe()

        ElseIf (mvMyListsMain.GetActiveView().Equals(vwWhoSharedPhoto)) Then

            'liWhoFavoritedMe.Visible = False
            'liWhoViewedMe.Visible = False
            'liMyFavoriteList.Visible = False
            'liMyBlockedList.Visible = False
            'liMyViewedList.Visible = False

            'liWhoSharedPhotos.Visible = True
            liWhoSharedPhotos.Attributes.Add("class", "down")
            liMyFavoriteList.Attributes.Remove("class")
            liWhoFavoritedMe.Attributes.Remove("class")
            liWhoViewedMe.Attributes.Remove("class")
            liMyBlockedList.Attributes.Remove("class")
            liMyViewedList.Attributes.Remove("class")

            whoSharedPhotoList.InfoWinHeaderText = CurrentPageData.GetCustomString("lnkWhoSharedPhotos")
            whoSharedPhotoList.InfoWinUrl = ResolveUrl("~/Members/InfoWin.aspx?info=WhoSharedPhotosCommands")

            BindWhoSharedPhoto()

        ElseIf (mvMyListsMain.GetActiveView().Equals(vwMyFavoriteList)) Then

            liMyFavoriteList.Attributes.Add("class", "down")
            liWhoFavoritedMe.Attributes.Remove("class")
            liWhoViewedMe.Attributes.Remove("class")
            liMyBlockedList.Attributes.Remove("class")
            liMyViewedList.Attributes.Remove("class")
            liWhoSharedPhotos.Attributes.Remove("class")

            myFavoriteList.InfoWinHeaderText = CurrentPageData.GetCustomString("lnkMyFavoriteList")
            myFavoriteList.InfoWinUrl = ResolveUrl("~/Members/InfoWin.aspx?info=MyFavoriteListCommands")

            BindMyFavoriteList()

        ElseIf (mvMyListsMain.GetActiveView().Equals(vwMyBlockedList)) Then

            liMyBlockedList.Attributes.Add("class", "down")
            liWhoFavoritedMe.Attributes.Remove("class")
            liMyFavoriteList.Attributes.Remove("class")
            liWhoViewedMe.Attributes.Remove("class")
            liMyViewedList.Attributes.Remove("class")
            liWhoSharedPhotos.Attributes.Remove("class")

            myBlockedList.InfoWinHeaderText = CurrentPageData.GetCustomString("lnkMyBlockedList")
            myBlockedList.InfoWinUrl = ResolveUrl("~/Members/InfoWin.aspx?info=MyBlockedListCommands")

            BindMyBlockedList()

        ElseIf (mvMyListsMain.GetActiveView().Equals(vwMyViewedList)) Then

            liMyViewedList.Attributes.Add("class", "down")
            liWhoFavoritedMe.Attributes.Remove("class")
            liMyFavoriteList.Attributes.Remove("class")
            liMyBlockedList.Attributes.Remove("class")
            liWhoViewedMe.Attributes.Remove("class")
            liWhoSharedPhotos.Attributes.Remove("class")

            myViewedList.InfoWinHeaderText = CurrentPageData.GetCustomString("lnkMyViewedList")
            myViewedList.InfoWinUrl = ResolveUrl("~/Members/InfoWin.aspx?info=MyViewedListCommands")

            BindMyViewedList()

        End If

    End Sub


    Private Sub FillOfferControlList(dt As DataTable, offersControl As Dating.Server.Site.Web.SearchControl)
        '' set pagination
        'Dim startingIndex As Integer = 0
        'Dim endIndex As Integer = Me.ItemsPerPage - 1
        'If (Me.Pager.PageIndex > 0) Then
        '    startingIndex = Me.ItemsPerPage * Me.Pager.PageIndex
        '    endIndex = (startingIndex + Me.ItemsPerPage) - 1
        'End If


        'Dim _profileRows As New clsProfileRows(Me.MasterProfileId)
        'Dim masterRow As EUS_ProfilesRow = _profileRows.GetMasterRow()

        Dim drDefaultPhoto As EUS_CustomerPhotosRow = DataHelpers.GetProfilesDefaultPhoto(Me.MasterProfileId)
        Dim uliCurrentMember As New clsWinkUserListItem()
        uliCurrentMember.ProfileID = Me.MasterProfileId ' 
        uliCurrentMember.MirrorProfileID = Me.MirrorProfileId
        uliCurrentMember.LoginName = Me.GetCurrentProfile().LoginName
        uliCurrentMember.Genderid = Me.GetCurrentProfile().GenderId
        uliCurrentMember.ProfileViewUrl = ResolveUrl("~/Members/Profile.aspx?p=" & HttpUtility.UrlEncode(Me.GetCurrentProfile().LoginName))

        'If (drDefaultPhoto IsNot Nothing) Then
        '    uliCurrentMember.ImageFileName = drDefaultPhoto.FileName
        '    uliCurrentMember.ImageUrl = ProfileHelper.GetProfileImageURL(drDefaultPhoto.CustomerID, drDefaultPhoto.FileName, Me.GetCurrentProfile().GenderId, True, Me.IsHTTPS)
        '    uliCurrentMember.ImageThumbUrl = ProfileHelper.GetProfileImageURL(drDefaultPhoto.CustomerID, drDefaultPhoto.FileName, Me.GetCurrentProfile().GenderId, True, Me.IsHTTPS)
        '    uliCurrentMember.ImageUploadDate = drDefaultPhoto.DateTimeToUploading.ToLocalTime().ToString("d MMM, yyyy H:mm")
        'Else
        '    uliCurrentMember.ImageUrl = ProfileHelper.GetDefaultImageURL(Me.GetCurrentProfile().GenderId)
        '    uliCurrentMember.ImageThumbUrl = uliCurrentMember.ImageUrl
        '    uliCurrentMember.ImageFileName = System.IO.Path.GetFileName(uliCurrentMember.ImageUrl)
        '    uliCurrentMember.ImageUploadDate = Nothing
        'End If

        If (drDefaultPhoto IsNot Nothing) Then
            uliCurrentMember.ImageFileName = drDefaultPhoto.FileName
            uliCurrentMember.ImageUploadDate = drDefaultPhoto.DateTimeToUploading.ToLocalTime().ToString("d MMM, yyyy H:mm")
        End If

        uliCurrentMember.ImageUrl = ProfileHelper.GetProfileImageURL(Me.MasterProfileId, uliCurrentMember.ImageFileName, Me.SessionVariables.MemberData.GenderId, True, Me.IsHTTPS, PhotoSize.D150)
        uliCurrentMember.ImageThumbUrl = uliCurrentMember.ImageUrl



        'Dim rowsCount As Integer
        'For rowsCount = startingIndex To dt.Rows.Count - 1
        '    If (rowsCount > endIndex) Then Exit For
        '    If (rowsCount >= startingIndex AndAlso rowsCount <= endIndex) Then

        Dim rowsCount As Integer
        For rowsCount = 0 To dt.Rows.Count - 1
            Try
                Dim dr As DataRow = dt.Rows(rowsCount)

                Dim uli As New clsWinkUserListItem()
                uli.LAGID = Session("LAGID")
                uli.ProfileID = uliCurrentMember.ProfileID
                uli.MirrorProfileID = uliCurrentMember.MirrorProfileID
                uli.LoginName = uliCurrentMember.LoginName
                uli.Genderid = uliCurrentMember.Genderid
                uli.ProfileViewUrl = uliCurrentMember.ProfileViewUrl
                uli.ImageFileName = uliCurrentMember.ImageFileName
                uli.ImageUrl = uliCurrentMember.ImageUrl
                uli.ImageThumbUrl = uliCurrentMember.ImageThumbUrl
                uli.ImageUploadDate = uliCurrentMember.ImageUploadDate


                If (dt.Columns.Contains("Birthday")) Then uli.OtherMemberBirthday = If(Not dr.IsNull("Birthday"), dr("Birthday"), Nothing)
                uli.OtherMemberLoginName = dr("LoginName")
                uli.OtherMemberProfileID = dr("ProfileID")
                uli.OtherMemberMirrorProfileID = dr("MirrorProfileID")
                uli.OtherMemberCity = IIf(Not dr.IsNull("City"), dr("City"), String.Empty)
                uli.OtherMemberRegion = IIf(Not dr.IsNull("Region"), dr("Region"), String.Empty)
                uli.OtherMemberCountry = IIf(Not dr.IsNull("Country"), dr("Country"), "")
                uli.OtherMemberGenderid = dr("Genderid")
                'uli.OtherMemberHeading = IIf(Not dr.IsNull("AboutMe_Heading"), dr("AboutMe_Heading"), String.Empty)


                If (Not dr.IsNull("PersonalInfo_HeightID")) Then
                    uli.OtherMemberHeight = ProfileHelper.GetHeightString(dr("PersonalInfo_HeightID"), Me.Session("LagID"))
                End If


                If (Not dr.IsNull("PersonalInfo_BodyTypeID")) Then
                    uli.OtherMemberPersonType = ProfileHelper.GetBodyTypeString(dr("PersonalInfo_BodyTypeID"), Me.Session("LagID"))
                End If


                If (Not dr.IsNull("PersonalInfo_HairColorID")) Then
                    uli.OtherMemberHair = ProfileHelper.GetHairColorString(dr("PersonalInfo_HairColorID"), Me.Session("LagID"))
                End If


                If (Not dr.IsNull("PersonalInfo_EyeColorID")) Then
                    uli.OtherMemberEyes = ProfileHelper.GetEyeColorString(dr("PersonalInfo_EyeColorID"), Me.Session("LagID"))
                End If


                If (Not dr.IsNull("PersonalInfo_EthnicityID")) Then
                    uli.OtherMemberEthnicity = ProfileHelper.GetEthnicityString(dr("PersonalInfo_EthnicityID"), Me.Session("LagID"))
                End If


                If (Not dr.IsNull("Birthday")) Then
                    uli.OtherMemberAge = ProfileHelper.GetCurrentAge(dr("Birthday"))
                Else
                    uli.OtherMemberAge = 20
                End If


                'If (Not dr.IsNull("FileName")) Then
                '    uli.OtherMemberImageFileName = dr("FileName")
                '    uli.OtherMemberImageUrl = ProfileHelper.GetProfileImageURL(dr("CustomerID"), uli.OtherMemberImageFileName, uli.OtherMemberGenderid, True, Me.IsHTTPS)
                '    uli.OtherMemberImageThumbUrl = ProfileHelper.GetProfileImageURL(dr("CustomerID"), uli.OtherMemberImageFileName, uli.OtherMemberGenderid, True, Me.IsHTTPS)
                '    'ElseIf (Not dr.IsNull("FileName2")) Then
                '    '    uli.OtherMemberImageFileName = dr("FileName2")
                '    '    uli.OtherMemberImageUrl = ProfileHelper.GetProfileImageURL(dr("CustomerID2"), uli.OtherMemberImageFileName, uli.OtherMemberGenderid, False)
                '    '    uli.OtherMemberImageThumbUrl = ProfileHelper.GetProfileImageURL(dr("CustomerID2"), uli.OtherMemberImageFileName, uli.OtherMemberGenderid, True)
                'Else
                '    uli.OtherMemberImageUrl = ProfileHelper.GetDefaultImageURL(uli.OtherMemberGenderid)
                '    uli.OtherMemberImageThumbUrl = uli.OtherMemberImageUrl
                '    uli.OtherMemberImageFileName = System.IO.Path.GetFileName(uli.OtherMemberImageUrl)
                'End If
                If (Not dr.IsNull("FileName")) Then
                    uli.OtherMemberImageFileName = dr("FileName")
                End If
                'uli.OtherMemberImageUrl = ProfileHelper.GetProfileImageURL(dr("ProfileID"), uli.OtherMemberImageFileName, uli.OtherMemberGenderid, True, Me.IsHTTPS, PhotoSize.D150)
                If (dt.Columns.Contains("HasPhoto")) Then
                    If (Not dr.IsNull("HasPhoto") AndAlso (dr("HasPhoto").ToString() = "1" OrElse dr("HasPhoto").ToString() = "True")) Then
                        ' user has photo
                        If (uli.OtherMemberImageFileName IsNot Nothing) Then
                            'has public photos
                            uli.OtherMemberImageUrl = ProfileHelper.GetProfileImageURL(uli.OtherMemberProfileID, uli.OtherMemberImageFileName, uli.OtherMemberGenderid, True, Me.IsHTTPS, PhotoSize.D150)

                        Else
                            'has private photos
                            uli.OtherMemberImageUrl = ProfileHelper.GetPrivateImageURL(uli.OtherMemberGenderid)

                        End If
                    Else
                        ' has no photo
                        uli.OtherMemberImageUrl = ProfileHelper.GetDefaultImageURL(uli.OtherMemberGenderid)

                    End If
                    If IsHTTPS AndAlso uli.OtherMemberImageUrl.StartsWith("http:") Then uli.OtherMemberImageUrl = "https" & uli.OtherMemberImageUrl.Remove(0, "http".Length)
                Else
                    uli.OtherMemberImageUrl = ProfileHelper.GetProfileImageURL(uli.OtherMemberProfileID, uli.OtherMemberImageFileName, uli.OtherMemberGenderid, True, Me.IsHTTPS, PhotoSize.D150)
                End If
                uli.OtherMemberImageThumbUrl = uli.OtherMemberImageUrl


                uli.OtherMemberProfileViewUrl = ResolveUrl("~/Members/Profile.aspx?p=" & HttpUtility.UrlEncode(dr("LoginName").ToString()))

                uli.Distance = dr("distance")

                If (uli.Distance < gCarDistance) Then
                    uli.DistanceCss = "distance_car"
                Else
                    uli.DistanceCss = "distance_plane"
                End If

                If (mvMyListsMain.GetActiveView().Equals(vwMyFavoriteList)) Then

                    uli.AllowUnfavorite = True
                    'uli.AllowFavorite = False

                ElseIf (mvMyListsMain.GetActiveView().Equals(vwMyBlockedList)) Then

                    uli.AllowUnblock = True

                Else

                    If (dt.Columns.Contains("OfferID")) Then
                        uli.OfferID = IIf(Not dr.IsNull("OfferID"), dr("OfferID"), 0)
                        Dim OfferTypeID = IIf(Not dr.IsNull("OffersOfferTypeID"), dr("OffersOfferTypeID"), 0)
                        Dim OffersStatusID = IIf(Not dr.IsNull("OffersStatusID"), dr("OffersStatusID"), 0)
                        Dim OffersFromProfileID = IIf(Not dr.IsNull("OffersFromProfileID"), dr("OffersFromProfileID"), 0)
                        Dim OffersToProfileID = IIf(Not dr.IsNull("OffersToProfileID"), dr("OffersToProfileID"), 0)

                        uli.OfferAmount = IIf(Not dr.IsNull("OffersAmount"), dr("OffersAmount"), 0)

                        If (uli.OfferID = 0) Then
                            uli.AllowWink = True
                        End If

                    ElseIf (dr("OffersCount") = 0) Then
                        uli.AllowWink = True
                    End If

                End If


                Dim handleLikeBtn As Boolean = False
                Dim handleFavoriteBtn As Boolean = False

                If (offersControl.Equals(myViewedList)) Then
                    handleLikeBtn = True
                    handleFavoriteBtn = False
                End If


                If (offersControl.Equals(whoViewedMeList)) Then
                    handleLikeBtn = True
                    handleFavoriteBtn = False
                End If


                If (offersControl.Equals(whoFavoritedMeList) OrElse _
                    offersControl.Equals(whoSharedPhotoList) OrElse _
                    offersControl.Equals(myFavoriteList)) Then

                    handleLikeBtn = True

                End If


                If (offersControl.Equals(whoFavoritedMeList) OrElse _
                    offersControl.Equals(whoSharedPhotoList) OrElse _
                    offersControl.Equals(whoViewedMeList) OrElse _
                    offersControl.Equals(myViewedList)) Then

                    'handleFavoriteBtn = True
                    Dim allowFavorite As Boolean = True
                    If (dr("HasFavoritedMe") > 0 AndAlso dr("DidIFavorited") > 0) Then
                        ''''''''''''''''''''''''
                        ' both users has favorited, each other
                        ''''''''''''''''''''''''
                        allowFavorite = False

                    ElseIf (dr("HasFavoritedMe") = 0 AndAlso dr("DidIFavorited") > 0) Then
                        ''''''''''''''''''''''''
                        ' current user has favorited, but the other not
                        ''''''''''''''''''''''''

                        allowFavorite = False
                    ElseIf (dr("HasFavoritedMe") > 0 AndAlso dr("DidIFavorited") = 0) Then
                        ''''''''''''''''''''''''
                        ' current user has not favorited, but the other did
                        ''''''''''''''''''''''''

                        allowFavorite = True

                    Else
                        ''''''''''''''''''''''''
                        ' none user has favorited, the other user
                        ''''''''''''''''''''''''

                        allowFavorite = True
                    End If


                    uli.AllowFavorite = allowFavorite
                    uli.AllowUnfavorite = Not allowFavorite
                End If


                If (handleLikeBtn) Then
                    uli.AllowWink = True

                    Try
                        Dim rec As Boolean = clsUserDoes.IsAnyWinkOrIsAnyOffer(uli.OtherMemberProfileID, Me.MasterProfileId)
                        uli.AllowWink = (Not rec)
                    Catch ex As Exception

                    End Try

                    ' if like enabled and users have exchanged at least one message, then disable like
                    If (uli.AllowWink) Then
                        Try
                            Dim rec As Boolean = clsUserDoes.IsAnyMessageExchanged(uli.OtherMemberProfileID, Me.MasterProfileId)
                            uli.AllowWink = (Not rec)
                        Catch ex As Exception
                        End Try
                    End If

                    If (Not uli.AllowWink) Then uli.AllowUnWink = True

                    'If (Not uli.AllowWink) Then
                    '    Try
                    '        Dim rec As EUS_Offer = clsUserDoes.GetMyPendingWink(uli.OtherMemberProfileID, Me.MasterProfileId)
                    '        uli.AllowUnWink = (rec IsNot Nothing)
                    '    Catch ex As Exception

                    '    End Try
                    'End If
                End If



                'If (handleFavoriteBtn) Then

                '    Dim allowFavorite As Boolean = True
                '    If (Not dr.IsNull("FavoriteID") AndAlso Not dr.IsNull("FavoriteID2")) Then
                '        ''''''''''''''''''''''''
                '        ' both users has favorited, each other
                '        ''''''''''''''''''''''''
                '        allowFavorite = False

                '    ElseIf (Not dr.IsNull("FavoriteID") AndAlso dr.IsNull("FavoriteID2")) Then
                '        ''''''''''''''''''''''''
                '        ' current user has favorited, but the other not
                '        ''''''''''''''''''''''''

                '        allowFavorite = False
                '    ElseIf (dr.IsNull("FavoriteID") AndAlso Not dr.IsNull("FavoriteID2")) Then
                '        ''''''''''''''''''''''''
                '        ' current user has not favorited, but the other did
                '        ''''''''''''''''''''''''

                '        allowFavorite = True

                '    Else
                '        ''''''''''''''''''''''''
                '        ' none user has favorited, the other user
                '        ''''''''''''''''''''''''

                '        allowFavorite = True
                '    End If

                '    uli.AllowFavorite = allowFavorite
                '    uli.AllowUnfavorite = Not uli.AllowFavorite
                'End If

                If (Not offersControl.Equals(myBlockedList)) Then

                    uli.SendMessageUrl = ResolveUrl("~/Members/Conversation.aspx?p=" & HttpUtility.UrlEncode(uli.OtherMemberLoginName) & "&offerid=" & uli.OfferID)
                    uli.SendMessageUrlOnce = ResolveUrl("~/Members/Conversation.aspx?send=once&p=" & HttpUtility.UrlEncode(uli.OtherMemberLoginName) & "&offerid=" & uli.OfferID)
                    uli.SendMessageUrlMany = ResolveUrl("~/Members/Conversation.aspx?send=unl&p=" & HttpUtility.UrlEncode(uli.OtherMemberLoginName) & "&offerid=" & uli.OfferID)


                    If (Me.IsMale AndAlso dr("CommunicationUnl") = 0) Then
                        Dim rec As EUS_Offer = clsUserDoes.GetLastOffer(uli.OtherMemberProfileID, Me.MasterProfileId)

                        If (rec Is Nothing) Then
                            uli.CreateOfferUrl = ResolveUrl("~/Members/CreateOffer.aspx?offerto=" & HttpUtility.UrlEncode(uli.OtherMemberLoginName))
                        End If

                        uli.AllowActionsMenu = True
                        uli.AllowActionsCreateOffer = True
                        uli.AllowActionsSendMessageOnce = True
                        uli.AllowActionsSendMessageMany = True
                    Else
                        uli.AllowSendMessage = True
                    End If

                End If


                uli.AllowTooltipPopupSearch = True


                If (Me.SessionVariables.MemberData.Status = ProfileStatusEnum.LIMITED) Then
                    uli.AllowUnblock = False
                    uli.AllowUnfavorite = False
                    uli.AllowFavorite = False
                    uli.AllowActionsMenu = False
                    uli.AllowSendMessage = False
                    uli.AllowWink = False
                    uli.AllowUnWink = False
                End If

                offersControl.UsersList.Add(uli)
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try

            'rowsCount += 1
        Next


    End Sub


End Class
